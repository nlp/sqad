<s>
Simone	Simon	k1gMnSc5	Simon
de	de	k?	de
Beauvoir	Beauvoir	k1gInSc1	Beauvoir
[	[	kIx(	[
<g/>
výslovnost	výslovnost	k1gFnSc1	výslovnost
si	se	k3xPyFc3	se
<g/>
:	:	kIx,	:
<g/>
ˈ	ˈ	k?	ˈ
də	də	k?	də
bo	bo	k?	bo
<g/>
:	:	kIx,	:
<g/>
ˈ	ˈ	k?	ˈ
<g/>
:	:	kIx,	:
<g/>
ʀ	ʀ	k?	ʀ
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
celým	celý	k2eAgNnSc7d1	celé
jménem	jméno	k1gNnSc7	jméno
Simone-Lucie-Ernestine-Marie	Simone-Lucie-Ernestine-Marie	k1gFnSc2	Simone-Lucie-Ernestine-Marie
Bertrand	Bertranda	k1gFnPc2	Bertranda
de	de	k?	de
Beauvoir	Beauvoira	k1gFnPc2	Beauvoira
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
též	též	k9	též
Simone	Simon	k1gMnSc5	Simon
de	de	k?	de
Beauvoirová	Beauvoirová	k1gFnSc1	Beauvoirová
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Simone	Simon	k1gMnSc5	Simon
Beauvoirová	Beauvoirová	k1gFnSc1	Beauvoirová
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1908	[number]	k4	1908
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
-	-	kIx~	-
14	[number]	k4	14
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
významná	významný	k2eAgFnSc1d1	významná
francouzská	francouzský	k2eAgFnSc1d1	francouzská
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
<g/>
,	,	kIx,	,
myslitelka	myslitelka	k1gFnSc1	myslitelka
<g/>
,	,	kIx,	,
filozofka	filozofka	k1gFnSc1	filozofka
a	a	k8xC	a
existencialistka	existencialistka	k1gFnSc1	existencialistka
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
jméno	jméno	k1gNnSc1	jméno
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
spojováno	spojovat	k5eAaImNgNnS	spojovat
s	s	k7c7	s
přispěním	přispění	k1gNnSc7	přispění
k	k	k7c3	k
emancipačním	emancipační	k2eAgFnPc3d1	emancipační
snahám	snaha	k1gFnPc3	snaha
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
feministickou	feministický	k2eAgFnSc7d1	feministická
teorií	teorie	k1gFnSc7	teorie
<g/>
,	,	kIx,	,
svobodomyslným	svobodomyslný	k2eAgInSc7d1	svobodomyslný
způsobem	způsob	k1gInSc7	způsob
života	život	k1gInSc2	život
a	a	k8xC	a
jejím	její	k3xOp3gInSc7	její
dlouholetým	dlouholetý	k2eAgInSc7d1	dlouholetý
vztahem	vztah	k1gInSc7	vztah
s	s	k7c7	s
francouzským	francouzský	k2eAgMnSc7d1	francouzský
filozofem	filozof	k1gMnSc7	filozof
Jeanem-Paulem	Jeanem-Paul	k1gMnSc7	Jeanem-Paul
Sartrem	Sartr	k1gMnSc7	Sartr
<g/>
.	.	kIx.	.
</s>
<s>
Simone	Simon	k1gMnSc5	Simon
de	de	k?	de
Beauvoir	Beauvoir	k1gInSc1	Beauvoir
(	(	kIx(	(
<g/>
celým	celý	k2eAgNnSc7d1	celé
jménem	jméno	k1gNnSc7	jméno
Simone-Lucie-Ernestine-Marie	Simone-Lucie-Ernestine-Marie	k1gFnSc2	Simone-Lucie-Ernestine-Marie
Bertrand	Bertranda	k1gFnPc2	Bertranda
de	de	k?	de
Beauvoir	Beauvoir	k1gInSc1	Beauvoir
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
v	v	k7c6	v
zámožné	zámožný	k2eAgFnSc6d1	zámožná
měšťanské	měšťanský	k2eAgFnSc6d1	měšťanská
rodině	rodina	k1gFnSc6	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnSc1	její
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
právník	právník	k1gMnSc1	právník
s	s	k7c7	s
uměleckými	umělecký	k2eAgInPc7d1	umělecký
sklony	sklon	k1gInPc7	sklon
<g/>
,	,	kIx,	,
její	její	k3xOp3gInPc4	její
matka	matka	k1gFnSc1	matka
byla	být	k5eAaImAgFnS	být
katolicky	katolicky	k6eAd1	katolicky
založená	založený	k2eAgFnSc1d1	založená
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
životní	životní	k2eAgFnSc1d1	životní
úroveň	úroveň	k1gFnSc1	úroveň
rodiny	rodina	k1gFnSc2	rodina
poklesla	poklesnout	k5eAaPmAgFnS	poklesnout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přesto	přesto	k8xC	přesto
si	se	k3xPyFc3	se
matka	matka	k1gFnSc1	matka
prosadila	prosadit	k5eAaPmAgFnS	prosadit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
Simone	Simon	k1gMnSc5	Simon
i	i	k9	i
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
sestrou	sestra	k1gFnSc7	sestra
poslána	poslat	k5eAaPmNgFnS	poslat
do	do	k7c2	do
prestižní	prestižní	k2eAgFnSc2d1	prestižní
klášterní	klášterní	k2eAgFnSc2d1	klášterní
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Rodinné	rodinný	k2eAgNnSc1d1	rodinné
zázemí	zázemí	k1gNnSc1	zázemí
s	s	k7c7	s
konzervativním	konzervativní	k2eAgInSc7d1	konzervativní
stylem	styl	k1gInSc7	styl
výchovy	výchova	k1gFnSc2	výchova
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
naplnění	naplnění	k1gNnSc1	naplnění
tradiční	tradiční	k2eAgFnSc2d1	tradiční
ženské	ženský	k2eAgFnSc2d1	ženská
role	role	k1gFnSc2	role
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
vdát	vdát	k5eAaPmF	vdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
porodit	porodit	k5eAaPmF	porodit
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
zasvětit	zasvětit	k5eAaPmF	zasvětit
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
rodinnému	rodinný	k2eAgInSc3d1	rodinný
krbu	krb	k1gInSc3	krb
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
14	[number]	k4	14
letech	léto	k1gNnPc6	léto
však	však	k9	však
prošla	projít	k5eAaPmAgFnS	projít
krizí	krize	k1gFnSc7	krize
víry	víra	k1gFnSc2	víra
<g/>
,	,	kIx,	,
obrátila	obrátit	k5eAaPmAgFnS	obrátit
se	se	k3xPyFc4	se
k	k	k7c3	k
přírodě	příroda	k1gFnSc3	příroda
a	a	k8xC	a
socialismu	socialismus	k1gInSc3	socialismus
(	(	kIx(	(
<g/>
komunismus	komunismus	k1gInSc1	komunismus
odmítala	odmítat	k5eAaImAgFnS	odmítat
<g/>
)	)	kIx)	)
a	a	k8xC	a
po	po	k7c4	po
zbytek	zbytek	k1gInSc4	zbytek
života	život	k1gInSc2	život
zastávala	zastávat	k5eAaImAgFnS	zastávat
ateistické	ateistický	k2eAgInPc4d1	ateistický
názory	názor	k1gInPc4	názor
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
jako	jako	k8xC	jako
dítě	dítě	k1gNnSc4	dítě
byla	být	k5eAaImAgFnS	být
Simone	Simon	k1gMnSc5	Simon
de	de	k?	de
Beauvoir	Beauvoir	k1gMnSc1	Beauvoir
intelektuálně	intelektuálně	k6eAd1	intelektuálně
velmi	velmi	k6eAd1	velmi
zdatná	zdatný	k2eAgFnSc1d1	zdatná
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
také	také	k9	také
vystudovala	vystudovat	k5eAaPmAgFnS	vystudovat
matematiku	matematika	k1gFnSc4	matematika
a	a	k8xC	a
poté	poté	k6eAd1	poté
filosofii	filosofie	k1gFnSc3	filosofie
na	na	k7c6	na
prestižní	prestižní	k2eAgFnSc6d1	prestižní
pařížské	pařížský	k2eAgFnSc6d1	Pařížská
univerzitě	univerzita	k1gFnSc6	univerzita
-	-	kIx~	-
Sorbonně	Sorbonna	k1gFnSc6	Sorbonna
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
snem	sen	k1gInSc7	sen
bylo	být	k5eAaImAgNnS	být
zasvětit	zasvětit	k5eAaPmF	zasvětit
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
psaní	psaní	k1gNnSc2	psaní
a	a	k8xC	a
studiu	studio	k1gNnSc6	studio
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
svých	svůj	k3xOyFgNnPc2	svůj
studií	studio	k1gNnPc2	studio
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
se	se	k3xPyFc4	se
připojila	připojit	k5eAaPmAgFnS	připojit
ke	k	k7c3	k
skupině	skupina	k1gFnSc3	skupina
myslitelů	myslitel	k1gMnPc2	myslitel
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgMnPc7	jenž
byl	být	k5eAaImAgMnS	být
kromě	kromě	k7c2	kromě
Paula	Paul	k1gMnSc2	Paul
Nizana	Nizan	k1gMnSc2	Nizan
a	a	k8xC	a
Andrého	André	k1gMnSc4	André
Hermaida	Hermaid	k1gMnSc4	Hermaid
také	také	k9	také
Jean-Paul	Jean-Paul	k1gInSc4	Jean-Paul
Sartre	Sartr	k1gMnSc5	Sartr
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgInSc7	jenž
Simone	Simon	k1gMnSc5	Simon
de	de	k?	de
Beauvior	Beauvior	k1gInSc4	Beauvior
nejdříve	dříve	k6eAd3	dříve
pojilo	pojit	k5eAaImAgNnS	pojit
hluboké	hluboký	k2eAgNnSc4d1	hluboké
přátelství	přátelství	k1gNnSc4	přátelství
a	a	k8xC	a
sdílené	sdílený	k2eAgInPc4d1	sdílený
zájmy	zájem	k1gInPc4	zájem
o	o	k7c4	o
filozofii	filozofie	k1gFnSc4	filozofie
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
milostný	milostný	k2eAgInSc1d1	milostný
svazek	svazek	k1gInSc1	svazek
-	-	kIx~	-
bez	bez	k7c2	bez
závazku	závazek	k1gInSc2	závazek
věrnosti	věrnost	k1gFnSc2	věrnost
-	-	kIx~	-
až	až	k9	až
do	do	k7c2	do
Sartrovy	Sartrův	k2eAgFnSc2d1	Sartrova
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
dokončila	dokončit	k5eAaPmAgFnS	dokončit
Simone	Simon	k1gMnSc5	Simon
de	de	k?	de
Beauvoir	Beauvoira	k1gFnPc2	Beauvoira
úspěšně	úspěšně	k6eAd1	úspěšně
studia	studio	k1gNnSc2	studio
na	na	k7c6	na
Sorbonně	Sorbonna	k1gFnSc6	Sorbonna
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
se	se	k3xPyFc4	se
věnovat	věnovat	k5eAaPmF	věnovat
učitelské	učitelský	k2eAgFnSc3d1	učitelská
profesi	profes	k1gFnSc3	profes
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1931	[number]	k4	1931
<g/>
-	-	kIx~	-
<g/>
1943	[number]	k4	1943
působila	působit	k5eAaImAgFnS	působit
jako	jako	k9	jako
pedagožka	pedagožka	k1gFnSc1	pedagožka
filozofie	filozofie	k1gFnSc1	filozofie
na	na	k7c6	na
lyceích	lyceum	k1gNnPc6	lyceum
v	v	k7c6	v
Marseille	Marseille	k1gFnSc6	Marseille
<g/>
,	,	kIx,	,
Rouenu	Roueno	k1gNnSc6	Roueno
a	a	k8xC	a
Paříži	Paříž	k1gFnSc6	Paříž
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
si	se	k3xPyFc3	se
otevřela	otevřít	k5eAaPmAgFnS	otevřít
vlastní	vlastní	k2eAgInSc4d1	vlastní
kurz	kurz	k1gInSc4	kurz
na	na	k7c6	na
Sorbonně	Sorbonna	k1gFnSc6	Sorbonna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
se	se	k3xPyFc4	se
vzdala	vzdát	k5eAaPmAgFnS	vzdát
profesorského	profesorský	k2eAgNnSc2d1	profesorské
povolání	povolání	k1gNnSc2	povolání
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
věnovala	věnovat	k5eAaImAgFnS	věnovat
jen	jen	k9	jen
spisovatelské	spisovatelský	k2eAgFnPc4d1	spisovatelská
činnosti	činnost	k1gFnPc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
připojila	připojit	k5eAaPmAgFnS	připojit
k	k	k7c3	k
hnutí	hnutí	k1gNnSc3	hnutí
za	za	k7c4	za
emancipaci	emancipace	k1gFnSc4	emancipace
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Zemřela	zemřít	k5eAaPmAgFnS	zemřít
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
78	[number]	k4	78
let	léto	k1gNnPc2	léto
na	na	k7c4	na
zápal	zápal	k1gInSc4	zápal
plic	plíce	k1gFnPc2	plíce
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
pohřbena	pohřbít	k5eAaPmNgFnS	pohřbít
vedle	vedle	k7c2	vedle
svého	svůj	k3xOyFgMnSc2	svůj
dlouholetého	dlouholetý	k2eAgMnSc2d1	dlouholetý
druha	druh	k1gMnSc2	druh
Jean-Paula	Jean-Paul	k1gMnSc2	Jean-Paul
Sartra	Sartr	k1gMnSc2	Sartr
na	na	k7c6	na
hřbitově	hřbitov	k1gInSc6	hřbitov
Montparnasse	Montparnasse	k1gFnSc2	Montparnasse
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Kritikové	kritik	k1gMnPc1	kritik
a	a	k8xC	a
biografové	biografový	k2eAgInPc1d1	biografový
vidí	vidět	k5eAaImIp3nP	vidět
kořeny	kořen	k1gInPc1	kořen
svobodomyslného	svobodomyslný	k2eAgNnSc2d1	svobodomyslné
a	a	k8xC	a
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
dobu	doba	k1gFnSc4	doba
odvážného	odvážný	k2eAgInSc2d1	odvážný
společenského	společenský	k2eAgInSc2d1	společenský
života	život	k1gInSc2	život
Simone	Simon	k1gMnSc5	Simon
de	de	k?	de
Beauvior	Beauvior	k1gMnSc1	Beauvior
v	v	k7c6	v
rodinném	rodinný	k2eAgNnSc6d1	rodinné
nastavení	nastavení	k1gNnSc6	nastavení
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
a	a	k8xC	a
filozofka	filozofka	k1gFnSc1	filozofka
chycena	chycen	k2eAgFnSc1d1	chycena
v	v	k7c6	v
pasti	past	k1gFnSc6	past
mezi	mezi	k7c7	mezi
otcovými	otcův	k2eAgFnPc7d1	otcova
pohanskými	pohanský	k2eAgFnPc7d1	pohanská
hodnotami	hodnota	k1gFnPc7	hodnota
a	a	k8xC	a
matčinými	matčin	k2eAgInPc7d1	matčin
rigidními	rigidní	k2eAgInPc7d1	rigidní
katolickými	katolický	k2eAgInPc7d1	katolický
standardy	standard	k1gInPc7	standard
<g/>
.	.	kIx.	.
</s>
<s>
Odloučení	odloučení	k1gNnSc1	odloučení
od	od	k7c2	od
Boha	bůh	k1gMnSc2	bůh
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Simone	Simon	k1gMnSc5	Simon
de	de	k?	de
Beauvoir	Beauvoir	k1gInSc1	Beauvoir
doprovázeno	doprovázen	k2eAgNnSc1d1	doprovázeno
vědomím	vědomí	k1gNnSc7	vědomí
<g/>
,	,	kIx,	,
že	že	k8xS	že
život	život	k1gInSc1	život
je	být	k5eAaImIp3nS	být
smrtelný	smrtelný	k2eAgMnSc1d1	smrtelný
a	a	k8xC	a
jedinec	jedinec	k1gMnSc1	jedinec
by	by	kYmCp3nS	by
si	se	k3xPyFc3	se
měl	mít	k5eAaImAgMnS	mít
užít	užít	k5eAaPmF	užít
všechny	všechen	k3xTgFnPc4	všechen
radosti	radost	k1gFnPc4	radost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mu	on	k3xPp3gNnSc3	on
pozemský	pozemský	k2eAgInSc4d1	pozemský
život	život	k1gInSc4	život
nabízí	nabízet	k5eAaImIp3nS	nabízet
<g/>
.	.	kIx.	.
</s>
<s>
Sama	sám	k3xTgFnSc1	sám
se	se	k3xPyFc4	se
tímto	tento	k3xDgInSc7	tento
životním	životní	k2eAgInSc7d1	životní
stylem	styl	k1gInSc7	styl
inspirovaným	inspirovaný	k2eAgInSc7d1	inspirovaný
přírodou	příroda	k1gFnSc7	příroda
a	a	k8xC	a
levicovými	levicový	k2eAgInPc7d1	levicový
názory	názor	k1gInPc7	názor
řídila	řídit	k5eAaImAgFnS	řídit
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
také	také	k9	také
determinovalo	determinovat	k5eAaBmAgNnS	determinovat
její	její	k3xOp3gFnSc4	její
spisovatelskou	spisovatelský	k2eAgFnSc4d1	spisovatelská
a	a	k8xC	a
filozofickou	filozofický	k2eAgFnSc4d1	filozofická
tvorbu	tvorba	k1gFnSc4	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Vzdělání	vzdělání	k1gNnSc1	vzdělání
a	a	k8xC	a
silné	silný	k2eAgNnSc1d1	silné
osobní	osobní	k2eAgNnSc1d1	osobní
přesvědčení	přesvědčení	k1gNnSc1	přesvědčení
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
výše	vysoce	k6eAd2	vysoce
zmíněný	zmíněný	k2eAgInSc1d1	zmíněný
odklon	odklon	k1gInSc1	odklon
od	od	k7c2	od
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
,	,	kIx,	,
významným	významný	k2eAgInSc7d1	významný
způsobem	způsob	k1gInSc7	způsob
ovlivnily	ovlivnit	k5eAaPmAgFnP	ovlivnit
životní	životní	k2eAgInSc4d1	životní
styl	styl	k1gInSc4	styl
Simone	Simon	k1gMnSc5	Simon
de	de	k?	de
Beauvior	Beauviora	k1gFnPc2	Beauviora
<g/>
.	.	kIx.	.
</s>
<s>
Sama	sám	k3xTgFnSc1	sám
sebe	sebe	k3xPyFc4	sebe
vnímala	vnímat	k5eAaImAgFnS	vnímat
jako	jako	k9	jako
nezávislou	závislý	k2eNgFnSc4d1	nezávislá
<g/>
,	,	kIx,	,
silnou	silný	k2eAgFnSc4d1	silná
osobnost	osobnost	k1gFnSc4	osobnost
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
je	být	k5eAaImIp3nS	být
submitivita	submitivita	k1gFnSc1	submitivita
a	a	k8xC	a
přizpůsobivost	přizpůsobivost	k1gFnSc1	přizpůsobivost
cizí	cizí	k2eAgFnSc1d1	cizí
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
projevilo	projevit	k5eAaPmAgNnS	projevit
i	i	k9	i
na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
vztahu	vztah	k1gInSc6	vztah
s	s	k7c7	s
Jean-Paulem	Jean-Paul	k1gMnSc7	Jean-Paul
Sartrem	Sartr	k1gMnSc7	Sartr
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
hlubokou	hluboký	k2eAgFnSc4d1	hluboká
úctu	úcta	k1gFnSc4	úcta
jednoho	jeden	k4xCgMnSc4	jeden
k	k	k7c3	k
druhému	druhý	k4xOgInSc3	druhý
vytvořily	vytvořit	k5eAaPmAgFnP	vytvořit
tyto	tento	k3xDgFnPc1	tento
dvě	dva	k4xCgFnPc1	dva
významné	významný	k2eAgFnPc1d1	významná
osobnosti	osobnost	k1gFnPc1	osobnost
francouzského	francouzský	k2eAgInSc2d1	francouzský
intelektuálního	intelektuální	k2eAgInSc2d1	intelektuální
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	let	k1gInPc6	let
otevřený	otevřený	k2eAgInSc4d1	otevřený
svazek	svazek	k1gInSc4	svazek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
jim	on	k3xPp3gMnPc3	on
dovoloval	dovolovat	k5eAaImAgInS	dovolovat
milostné	milostný	k2eAgInPc4d1	milostný
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
jinými	jiný	k2eAgMnPc7d1	jiný
lidmi	člověk	k1gMnPc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Simone	Simon	k1gMnSc5	Simon
de	de	k?	de
Beauvoir	Beauvoir	k1gInSc4	Beauvoir
nechtěla	chtít	k5eNaImAgFnS	chtít
vztah	vztah	k1gInSc4	vztah
institucionalizovat	institucionalizovat	k5eAaImF	institucionalizovat
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
také	také	k9	také
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
Sartrovu	Sartrův	k2eAgFnSc4d1	Sartrova
nabídku	nabídka	k1gFnSc4	nabídka
k	k	k7c3	k
sňatku	sňatek	k1gInSc3	sňatek
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
proti	proti	k7c3	proti
manželskému	manželský	k2eAgInSc3d1	manželský
svazku	svazek	k1gInSc3	svazek
nikdy	nikdy	k6eAd1	nikdy
otevřeně	otevřeně	k6eAd1	otevřeně
nebrojila	brojit	k5eNaImAgFnS	brojit
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
svým	svůj	k3xOyFgInSc7	svůj
upřímným	upřímný	k2eAgInSc7d1	upřímný
a	a	k8xC	a
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
dobu	doba	k1gFnSc4	doba
velmi	velmi	k6eAd1	velmi
moderně	moderně	k6eAd1	moderně
pojatým	pojatý	k2eAgInSc7d1	pojatý
a	a	k8xC	a
pokrokovým	pokrokový	k2eAgInSc7d1	pokrokový
vztahem	vztah	k1gInSc7	vztah
s	s	k7c7	s
Jean-Paulem	Jean-Paul	k1gMnSc7	Jean-Paul
Sartrem	Sartr	k1gMnSc7	Sartr
(	(	kIx(	(
<g/>
krátce	krátce	k6eAd1	krátce
dokonce	dokonce	k9	dokonce
utvořili	utvořit	k5eAaPmAgMnP	utvořit
milostný	milostný	k2eAgInSc4d1	milostný
trojúhelník	trojúhelník	k1gInSc4	trojúhelník
s	s	k7c7	s
mladičkou	mladička	k1gFnSc7	mladička
studentkou	studentka	k1gFnSc7	studentka
Olgou	Olga	k1gFnSc7	Olga
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bourala	bourat	k5eAaImAgFnS	bourat
Simone	Simon	k1gMnSc5	Simon
de	de	k?	de
Beauvoir	Beauvoira	k1gFnPc2	Beauvoira
tehdejší	tehdejší	k2eAgFnSc1d1	tehdejší
společenská	společenský	k2eAgFnSc1d1	společenská
tabu	tabu	k1gNnSc7	tabu
a	a	k8xC	a
narušovala	narušovat	k5eAaImAgFnS	narušovat
tradiční	tradiční	k2eAgInPc4d1	tradiční
stereotypy	stereotyp	k1gInPc4	stereotyp
ohledně	ohledně	k7c2	ohledně
ženské	ženský	k2eAgFnSc2d1	ženská
role	role	k1gFnSc2	role
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
životní	životní	k2eAgInSc4d1	životní
styl	styl	k1gInSc4	styl
sklízela	sklízet	k5eAaImAgFnS	sklízet
jak	jak	k8xS	jak
kritiku	kritika	k1gFnSc4	kritika
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
obdiv	obdiv	k1gInSc4	obdiv
mnoha	mnoho	k4c2	mnoho
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
přimělo	přimět	k5eAaPmAgNnS	přimět
k	k	k7c3	k
veřejnému	veřejný	k2eAgNnSc3d1	veřejné
prosazování	prosazování	k1gNnSc3	prosazování
emancipace	emancipace	k1gFnSc2	emancipace
<g/>
.	.	kIx.	.
</s>
<s>
Simone	Simon	k1gMnSc5	Simon
de	de	k?	de
Beauvoir	Beauvoira	k1gFnPc2	Beauvoira
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
významnou	významný	k2eAgFnSc7d1	významná
obhájkyní	obhájkyně	k1gFnSc7	obhájkyně
ženských	ženský	k2eAgNnPc2d1	ženské
práv	právo	k1gNnPc2	právo
a	a	k8xC	a
hodnot	hodnota	k1gFnPc2	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Brojila	brojit	k5eAaImAgFnS	brojit
proti	proti	k7c3	proti
způsobu	způsob	k1gInSc3	způsob
<g/>
,	,	kIx,	,
jakým	jaký	k3yIgInSc7	jaký
je	být	k5eAaImIp3nS	být
společností	společnost	k1gFnSc7	společnost
nahlíženo	nahlížen	k2eAgNnSc4d1	nahlíženo
na	na	k7c4	na
chudé	chudý	k2eAgFnPc4d1	chudá
nevdané	vdaný	k2eNgFnPc4d1	nevdaná
matky	matka	k1gFnPc4	matka
<g/>
,	,	kIx,	,
a	a	k8xC	a
kritizovala	kritizovat	k5eAaImAgFnS	kritizovat
způsob	způsob	k1gInSc4	způsob
<g/>
,	,	kIx,	,
jakým	jaký	k3yQgInSc7	jaký
náboženská	náboženský	k2eAgFnSc1d1	náboženská
morálka	morálka	k1gFnSc1	morálka
svazuje	svazovat	k5eAaImIp3nS	svazovat
svobodu	svoboda	k1gFnSc4	svoboda
jedince	jedinec	k1gMnSc2	jedinec
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
ženy	žena	k1gFnPc1	žena
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgInPc4	svůj
postřehy	postřeh	k1gInPc4	postřeh
o	o	k7c6	o
životě	život	k1gInSc6	život
a	a	k8xC	a
roli	role	k1gFnSc3	role
žen	žena	k1gFnPc2	žena
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
zpracovala	zpracovat	k5eAaPmAgFnS	zpracovat
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Le	Le	k1gFnSc2	Le
deuxiè	deuxiè	k?	deuxiè
sexe	sex	k1gInSc5	sex
(	(	kIx(	(
<g/>
Druhé	druhý	k4xOgFnSc2	druhý
pohlaví	pohlaví	k1gNnPc2	pohlaví
<g/>
,	,	kIx,	,
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
poukazuje	poukazovat	k5eAaImIp3nS	poukazovat
na	na	k7c4	na
tehdy	tehdy	k6eAd1	tehdy
mnoha	mnoho	k4c7	mnoho
ženami	žena	k1gFnPc7	žena
sdílený	sdílený	k2eAgInSc4d1	sdílený
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
mužskou	mužský	k2eAgFnSc7d1	mužská
částí	část	k1gFnSc7	část
společnosti	společnost	k1gFnSc2	společnost
považovány	považován	k2eAgFnPc1d1	považována
za	za	k7c4	za
ty	ten	k3xDgMnPc4	ten
druhé	druhý	k4xOgMnPc4	druhý
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
roli	role	k1gFnSc3	role
ve	v	k7c4	v
společnosti	společnost	k1gFnPc4	společnost
utvářeny	utvářen	k2eAgFnPc4d1	utvářena
<g/>
.	.	kIx.	.
</s>
<s>
Myšlenku	myšlenka	k1gFnSc4	myšlenka
široce	široko	k6eAd1	široko
rozvedla	rozvést	k5eAaPmAgFnS	rozvést
a	a	k8xC	a
zasadila	zasadit	k5eAaPmAgFnS	zasadit
ji	on	k3xPp3gFnSc4	on
do	do	k7c2	do
historického	historický	k2eAgInSc2d1	historický
kontextu	kontext	k1gInSc2	kontext
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejucelenějších	ucelený	k2eAgFnPc2d3	nejucelenější
deklarací	deklarace	k1gFnPc2	deklarace
nezávislosti	nezávislost	k1gFnSc2	nezávislost
žen	žena	k1gFnPc2	žena
na	na	k7c6	na
mužích	muž	k1gMnPc6	muž
<g/>
.	.	kIx.	.
</s>
<s>
Vztah	vztah	k1gInSc1	vztah
s	s	k7c7	s
Jean-Paulem	Jean-Paul	k1gMnSc7	Jean-Paul
Sartrem	Sartr	k1gMnSc7	Sartr
ovlivnil	ovlivnit	k5eAaPmAgMnS	ovlivnit
také	také	k9	také
její	její	k3xOp3gFnSc4	její
filozofickou	filozofický	k2eAgFnSc4d1	filozofická
dráhu	dráha	k1gFnSc4	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
druhem	druh	k1gInSc7	druh
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
vyznavačkou	vyznavačka	k1gFnSc7	vyznavačka
moderního	moderní	k2eAgInSc2d1	moderní
směru	směr	k1gInSc2	směr
existencialismu	existencialismus	k1gInSc2	existencialismus
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
rámci	rámec	k1gInSc6	rámec
formulovala	formulovat	k5eAaImAgFnS	formulovat
hned	hned	k6eAd1	hned
několik	několik	k4yIc4	několik
důležitých	důležitý	k2eAgFnPc2d1	důležitá
myšlenek	myšlenka	k1gFnPc2	myšlenka
<g/>
.	.	kIx.	.
</s>
<s>
Existencialistický	existencialistický	k2eAgInSc1d1	existencialistický
směr	směr	k1gInSc1	směr
obecně	obecně	k6eAd1	obecně
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
předpokladu	předpoklad	k1gInSc2	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
jedinou	jediný	k2eAgFnSc7d1	jediná
daností	danost	k1gFnSc7	danost
je	být	k5eAaImIp3nS	být
samotná	samotný	k2eAgFnSc1d1	samotná
existence	existence	k1gFnSc1	existence
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Jedinec	jedinec	k1gMnSc1	jedinec
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
existencialistů	existencialista	k1gMnPc2	existencialista
stává	stávat	k5eAaImIp3nS	stávat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
co	co	k9	co
sám	sám	k3xTgMnSc1	sám
se	s	k7c7	s
sebou	se	k3xPyFc7	se
dělá	dělat	k5eAaImIp3nS	dělat
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemu	co	k3yQnSc3	co
se	se	k3xPyFc4	se
sám	sám	k3xTgMnSc1	sám
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
chová	chovat	k5eAaImIp3nS	chovat
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
si	se	k3xPyFc3	se
přeje	přát	k5eAaImIp3nS	přát
a	a	k8xC	a
jak	jak	k8xC	jak
formuje	formovat	k5eAaImIp3nS	formovat
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
vlastní	vlastní	k2eAgInSc4d1	vlastní
aktivitou	aktivita	k1gFnSc7	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
sám	sám	k3xTgMnSc1	sám
odpovědný	odpovědný	k2eAgMnSc1d1	odpovědný
za	za	k7c4	za
svá	svůj	k3xOyFgNnPc4	svůj
rozhodnutí	rozhodnutí	k1gNnPc4	rozhodnutí
a	a	k8xC	a
podobu	podoba	k1gFnSc4	podoba
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Simone	Simon	k1gMnSc5	Simon
de	de	k?	de
Beauvoir	Beauvoir	k1gInSc4	Beauvoir
přispěla	přispět	k5eAaPmAgFnS	přispět
k	k	k7c3	k
existencialismu	existencialismus	k1gInSc3	existencialismus
ženskými	ženský	k2eAgNnPc7d1	ženské
tématy	téma	k1gNnPc7	téma
a	a	k8xC	a
(	(	kIx(	(
<g/>
sebe	sebe	k3xPyFc4	sebe
<g/>
)	)	kIx)	)
<g/>
reflexí	reflexe	k1gFnPc2	reflexe
ženy	žena	k1gFnSc2	žena
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
shrnula	shrnout	k5eAaPmAgFnS	shrnout
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
nejslavnějším	slavný	k2eAgNnSc6d3	nejslavnější
esejistickém	esejistický	k2eAgNnSc6d1	esejistické
díle	dílo	k1gNnSc6	dílo
Druhé	druhý	k4xOgNnSc4	druhý
pohlaví	pohlaví	k1gNnSc4	pohlaví
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
dílo	dílo	k1gNnSc1	dílo
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
a	a	k8xC	a
svým	svůj	k3xOyFgNnSc7	svůj
poselstvím	poselství	k1gNnSc7	poselství
šokovalo	šokovat	k5eAaBmAgNnS	šokovat
tehdejší	tehdejší	k2eAgFnSc4d1	tehdejší
veřejnost	veřejnost	k1gFnSc4	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Publikace	publikace	k1gFnSc1	publikace
kritizuje	kritizovat	k5eAaImIp3nS	kritizovat
mužské	mužský	k2eAgFnPc4d1	mužská
představy	představa	k1gFnPc4	představa
o	o	k7c4	o
roli	role	k1gFnSc4	role
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterých	který	k3yRgFnPc2	který
jsou	být	k5eAaImIp3nP	být
jejich	jejich	k3xOp3gInPc4	jejich
osobní	osobní	k2eAgInPc4d1	osobní
úspěchy	úspěch	k1gInPc4	úspěch
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
jejich	jejich	k3xOp3gNnSc7	jejich
ženstvím	ženství	k1gNnSc7	ženství
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
"	"	kIx"	"
<g/>
pravá	pravý	k2eAgFnSc1d1	pravá
žena	žena	k1gFnSc1	žena
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
objektem	objekt	k1gInSc7	objekt
mužského	mužský	k2eAgInSc2d1	mužský
plánu	plán	k1gInSc2	plán
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
druhořadým	druhořadý	k2eAgNnSc7d1	druhořadé
stvořením	stvoření	k1gNnSc7	stvoření
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
povinno	povinen	k2eAgNnSc1d1	povinno
přispívat	přispívat	k5eAaImF	přispívat
k	k	k7c3	k
uspokojování	uspokojování	k1gNnSc3	uspokojování
mužských	mužský	k2eAgFnPc2d1	mužská
potřeb	potřeba	k1gFnPc2	potřeba
a	a	k8xC	a
k	k	k7c3	k
uskutečňování	uskutečňování	k1gNnSc3	uskutečňování
jejich	jejich	k3xOp3gInPc2	jejich
životních	životní	k2eAgInPc2d1	životní
plánů	plán	k1gInPc2	plán
<g/>
.	.	kIx.	.
</s>
<s>
Simone	Simon	k1gMnSc5	Simon
de	de	k?	de
Beauvoir	Beauvoir	k1gInSc4	Beauvoir
s	s	k7c7	s
takovým	takový	k3xDgInSc7	takový
rozšířeným	rozšířený	k2eAgInSc7d1	rozšířený
stereotypem	stereotyp	k1gInSc7	stereotyp
společenského	společenský	k2eAgMnSc2d1	společenský
"	"	kIx"	"
<g/>
utváření	utváření	k1gNnSc1	utváření
žen	žena	k1gFnPc2	žena
<g/>
"	"	kIx"	"
na	na	k7c6	na
základě	základ	k1gInSc6	základ
předsudků	předsudek	k1gInPc2	předsudek
<g/>
,	,	kIx,	,
konvencí	konvence	k1gFnPc2	konvence
a	a	k8xC	a
očekávání	očekávání	k1gNnSc2	očekávání
mužského	mužský	k2eAgInSc2d1	mužský
světa	svět	k1gInSc2	svět
nesouhlasila	souhlasit	k5eNaImAgFnS	souhlasit
a	a	k8xC	a
brojila	brojit	k5eAaImAgFnS	brojit
proti	proti	k7c3	proti
němu	on	k3xPp3gMnSc3	on
nejen	nejen	k6eAd1	nejen
svou	svůj	k3xOyFgFnSc7	svůj
esejistickou	esejistický	k2eAgFnSc7d1	esejistická
tvorbou	tvorba	k1gFnSc7	tvorba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
vlastními	vlastní	k2eAgInPc7d1	vlastní
životními	životní	k2eAgInPc7d1	životní
rozhodnutími	rozhodnutí	k1gNnPc7	rozhodnutí
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
přestože	přestože	k8xS	přestože
jako	jako	k9	jako
filozofka	filozofka	k1gFnSc1	filozofka
přispěla	přispět	k5eAaPmAgFnS	přispět
k	k	k7c3	k
několika	několik	k4yIc3	několik
tezím	teze	k1gFnPc3	teze
existencialistického	existencialistický	k2eAgNnSc2d1	existencialistické
smýšlení	smýšlení	k1gNnSc2	smýšlení
<g/>
,	,	kIx,	,
sama	sám	k3xTgFnSc1	sám
se	se	k3xPyFc4	se
za	za	k7c4	za
existencialistu	existencialista	k1gMnSc4	existencialista
nepovažovala	považovat	k5eNaImAgFnS	považovat
<g/>
.	.	kIx.	.
</s>
<s>
Vnímala	vnímat	k5eAaImAgFnS	vnímat
sebe	sebe	k3xPyFc4	sebe
samu	sám	k3xTgFnSc4	sám
spíše	spíše	k9	spíše
jako	jako	k9	jako
bojovnici	bojovnice	k1gFnSc4	bojovnice
za	za	k7c4	za
sociálně	sociálně	k6eAd1	sociálně
spravedlivější	spravedlivý	k2eAgInSc4d2	spravedlivější
svět	svět	k1gInSc4	svět
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
dokazovala	dokazovat	k5eAaImAgFnS	dokazovat
i	i	k8xC	i
svým	svůj	k3xOyFgNnSc7	svůj
působením	působení	k1gNnSc7	působení
v	v	k7c6	v
redakci	redakce	k1gFnSc6	redakce
časopisu	časopis	k1gInSc2	časopis
Les	les	k1gInSc4	les
Temps	Temps	k1gInSc1	Temps
Modernes	Modernes	k1gInSc1	Modernes
<g/>
.	.	kIx.	.
</s>
<s>
Veřejně	veřejně	k6eAd1	veřejně
kritizovala	kritizovat	k5eAaImAgFnS	kritizovat
zvláště	zvláště	k6eAd1	zvláště
válku	válka	k1gFnSc4	válka
v	v	k7c6	v
Alžírsku	Alžírsko	k1gNnSc6	Alžírsko
<g/>
,	,	kIx,	,
bojovala	bojovat	k5eAaImAgFnS	bojovat
za	za	k7c4	za
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
práce	práce	k1gFnSc2	práce
v	v	k7c6	v
továrnách	továrna	k1gFnPc6	továrna
a	a	k8xC	a
současně	současně	k6eAd1	současně
obhajovala	obhajovat	k5eAaImAgNnP	obhajovat
práva	právo	k1gNnPc1	právo
starých	starý	k2eAgMnPc2d1	starý
občanů	občan	k1gMnPc2	občan
a	a	k8xC	a
marxisticky	marxisticky	k6eAd1	marxisticky
orientované	orientovaný	k2eAgNnSc4d1	orientované
socialistické	socialistický	k2eAgNnSc4d1	socialistické
smýšlení	smýšlení	k1gNnSc4	smýšlení
<g/>
.	.	kIx.	.
</s>
<s>
Katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
zařadila	zařadit	k5eAaPmAgFnS	zařadit
její	její	k3xOp3gFnPc4	její
knihy	kniha	k1gFnPc4	kniha
Druhé	druhý	k4xOgNnSc1	druhý
pohlaví	pohlaví	k1gNnSc1	pohlaví
(	(	kIx(	(
<g/>
Le	Le	k1gMnSc1	Le
Deuxiè	Deuxiè	k1gMnSc1	Deuxiè
sexe	sex	k1gInSc5	sex
<g/>
)	)	kIx)	)
a	a	k8xC	a
Mandaríni	mandarín	k1gMnPc1	mandarín
(	(	kIx(	(
<g/>
Les	les	k1gInSc1	les
Mandarins	Mandarins	k1gInSc1	Mandarins
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
na	na	k7c4	na
Index	index	k1gInSc4	index
zakázaných	zakázaný	k2eAgFnPc2d1	zakázaná
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Měla	mít	k5eAaImAgFnS	mít
mnoho	mnoho	k4c4	mnoho
milenců	milenec	k1gMnPc2	milenec
a	a	k8xC	a
milenek	milenka	k1gFnPc2	milenka
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
několik	několik	k4yIc1	několik
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
bylo	být	k5eAaImAgNnS	být
nezletilých	nezletilých	k2eAgNnSc1d1	nezletilých
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
diskusím	diskuse	k1gFnPc3	diskuse
o	o	k7c6	o
pedofilních	pedofilní	k2eAgInPc6d1	pedofilní
sklonech	sklon	k1gInPc6	sklon
spisovatelky	spisovatelka	k1gFnSc2	spisovatelka
<g/>
.	.	kIx.	.
</s>
<s>
Společenský	společenský	k2eAgInSc4d1	společenský
rozruch	rozruch	k1gInSc4	rozruch
způsobila	způsobit	k5eAaPmAgFnS	způsobit
roku	rok	k1gInSc2	rok
1941	[number]	k4	1941
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
jako	jako	k9	jako
učitelka	učitelka	k1gFnSc1	učitelka
suspendována	suspendován	k2eAgFnSc1d1	suspendována
po	po	k7c6	po
kontroverzní	kontroverzní	k2eAgFnSc6d1	kontroverzní
aféře	aféra	k1gFnSc6	aféra
se	s	k7c7	s
sedmnáctiletou	sedmnáctiletý	k2eAgFnSc7d1	sedmnáctiletá
studentkou	studentka	k1gFnSc7	studentka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
této	tento	k3xDgFnSc6	tento
události	událost	k1gFnSc6	událost
Simone	Simon	k1gMnSc5	Simon
de	de	k?	de
Beauvoir	Beauvoir	k1gInSc1	Beauvoir
prosazovala	prosazovat	k5eAaImAgFnS	prosazovat
zrušení	zrušení	k1gNnSc4	zrušení
veškerých	veškerý	k3xTgInPc2	veškerý
věkových	věkový	k2eAgInPc2d1	věkový
zákonných	zákonný	k2eAgInPc2d1	zákonný
limitů	limit	k1gInPc2	limit
pro	pro	k7c4	pro
pohlavní	pohlavní	k2eAgInSc4d1	pohlavní
styk	styk	k1gInSc4	styk
<g/>
.	.	kIx.	.
</s>
<s>
Ráda	rád	k2eAgFnSc1d1	ráda
cestovala	cestovat	k5eAaImAgFnS	cestovat
a	a	k8xC	a
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
pracovně	pracovně	k6eAd1	pracovně
působila	působit	k5eAaImAgFnS	působit
či	či	k8xC	či
se	se	k3xPyFc4	se
rekreovala	rekreovat	k5eAaBmAgFnS	rekreovat
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
,	,	kIx,	,
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
,	,	kIx,	,
Izraeli	Izrael	k1gInSc6	Izrael
<g/>
,	,	kIx,	,
Brazílii	Brazílie	k1gFnSc6	Brazílie
a	a	k8xC	a
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
<g/>
.	.	kIx.	.
</s>
<s>
Podepsala	podepsat	k5eAaPmAgFnS	podepsat
mj.	mj.	kA	mj.
Manifest	manifest	k1gInSc4	manifest
343	[number]	k4	343
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
sdružoval	sdružovat	k5eAaImAgInS	sdružovat
slavné	slavný	k2eAgFnPc4d1	slavná
ženy	žena	k1gFnPc4	žena
prodělavší	prodělavší	k2eAgInSc4d1	prodělavší
potrat	potrat	k1gInSc4	potrat
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
nezákonný	zákonný	k2eNgInSc1d1	nezákonný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Americký	americký	k2eAgInSc1d1	americký
překlad	překlad	k1gInSc1	překlad
jejího	její	k3xOp3gNnSc2	její
stěžejního	stěžejní	k2eAgNnSc2d1	stěžejní
díla	dílo	k1gNnSc2	dílo
Druhé	druhý	k4xOgNnSc4	druhý
pohlaví	pohlaví	k1gNnSc4	pohlaví
(	(	kIx(	(
<g/>
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
nepřesný	přesný	k2eNgInSc1d1	nepřesný
<g/>
,	,	kIx,	,
vinou	vinou	k7c2	vinou
čehož	což	k3yQnSc2	což
se	se	k3xPyFc4	se
ztratila	ztratit	k5eAaPmAgFnS	ztratit
hlavní	hlavní	k2eAgFnSc1d1	hlavní
myšlenka	myšlenka	k1gFnSc1	myšlenka
autorky	autorka	k1gFnSc2	autorka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
ještě	ještě	k6eAd1	ještě
slavnější	slavný	k2eAgFnSc1d2	slavnější
než	než	k8xS	než
za	za	k7c2	za
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
byla	být	k5eAaImAgFnS	být
nazývána	nazývat	k5eAaImNgFnS	nazývat
matkou	matka	k1gFnSc7	matka
feminismu	feminismus	k1gInSc2	feminismus
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
její	její	k3xOp3gFnSc4	její
počest	počest	k1gFnSc4	počest
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
most	most	k1gInSc1	most
pro	pro	k7c4	pro
pěší	pěší	k2eAgInSc4d1	pěší
Passerelle	Passerelle	k1gInSc4	Passerelle
Simone-de-Beauvoir	Simonee-Beauvoira	k1gFnPc2	Simone-de-Beauvoira
přes	přes	k7c4	přes
řeku	řeka	k1gFnSc4	řeka
Seine	Sein	k1gMnSc5	Sein
<g/>
.	.	kIx.	.
</s>
<s>
Literární	literární	k2eAgFnSc1d1	literární
tvorba	tvorba	k1gFnSc1	tvorba
Simone	Simon	k1gMnSc5	Simon
de	de	k?	de
Beauvoir	Beauvoir	k1gInSc1	Beauvoir
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
filozofickým	filozofický	k2eAgInSc7d1	filozofický
základem	základ	k1gInSc7	základ
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
existencialistickými	existencialistický	k2eAgInPc7d1	existencialistický
impulzy	impulz	k1gInPc7	impulz
<g/>
,	,	kIx,	,
studiem	studio	k1gNnSc7	studio
postavení	postavení	k1gNnSc2	postavení
žen	žena	k1gFnPc2	žena
ve	v	k7c6	v
francouzské	francouzský	k2eAgFnSc6d1	francouzská
společnosti	společnost	k1gFnSc6	společnost
a	a	k8xC	a
vlastními	vlastní	k2eAgInPc7d1	vlastní
zážitky	zážitek	k1gInPc7	zážitek
<g/>
.	.	kIx.	.
</s>
<s>
Autorka	autorka	k1gFnSc1	autorka
do	do	k7c2	do
svých	svůj	k3xOyFgNnPc2	svůj
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
bývají	bývat	k5eAaImIp3nP	bývat
charakterizována	charakterizován	k2eAgNnPc1d1	charakterizováno
jako	jako	k8xC	jako
tzv.	tzv.	kA	tzv.
angažovaná	angažovaný	k2eAgFnSc1d1	angažovaná
literatura	literatura	k1gFnSc1	literatura
(	(	kIx(	(
<g/>
žánr	žánr	k1gInSc1	žánr
na	na	k7c4	na
pomezí	pomezí	k1gNnSc4	pomezí
filozofie	filozofie	k1gFnSc2	filozofie
a	a	k8xC	a
beletrie	beletrie	k1gFnSc2	beletrie
<g/>
)	)	kIx)	)
často	často	k6eAd1	často
projektovala	projektovat	k5eAaBmAgFnS	projektovat
vlastní	vlastní	k2eAgFnPc4d1	vlastní
životní	životní	k2eAgFnPc4d1	životní
zkušenosti	zkušenost	k1gFnPc4	zkušenost
<g/>
,	,	kIx,	,
názory	názor	k1gInPc4	názor
a	a	k8xC	a
postoje	postoj	k1gInPc4	postoj
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
dohromady	dohromady	k6eAd1	dohromady
s	s	k7c7	s
jejím	její	k3xOp3gNnSc7	její
racionálním	racionální	k2eAgNnSc7d1	racionální
uvažováním	uvažování	k1gNnSc7	uvažování
a	a	k8xC	a
objektivním	objektivní	k2eAgNnSc7d1	objektivní
realistickým	realistický	k2eAgNnSc7d1	realistické
nazíráním	nazírání	k1gNnSc7	nazírání
na	na	k7c4	na
svět	svět	k1gInSc4	svět
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
unikátní	unikátní	k2eAgFnSc4d1	unikátní
kombinaci	kombinace	k1gFnSc4	kombinace
obsahu	obsah	k1gInSc2	obsah
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
tématem	téma	k1gNnSc7	téma
jejích	její	k3xOp3gFnPc2	její
prací	práce	k1gFnPc2	práce
esejistických	esejistický	k2eAgMnPc2d1	esejistický
<g/>
,	,	kIx,	,
filozofických	filozofický	k2eAgInPc2d1	filozofický
či	či	k8xC	či
beletristických	beletristický	k2eAgInPc2d1	beletristický
je	být	k5eAaImIp3nS	být
morálka	morálka	k1gFnSc1	morálka
společnosti	společnost	k1gFnSc2	společnost
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
postavení	postavení	k1gNnSc3	postavení
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
všeobecná	všeobecný	k2eAgFnSc1d1	všeobecná
emancipace	emancipace	k1gFnSc1	emancipace
a	a	k8xC	a
sociální	sociální	k2eAgFnSc1d1	sociální
spravedlnost	spravedlnost	k1gFnSc1	spravedlnost
<g/>
.	.	kIx.	.
1943	[number]	k4	1943
<g/>
,	,	kIx,	,
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Invitée	Invitée	k1gInSc1	Invitée
<g/>
,	,	kIx,	,
Paris	Paris	k1gMnSc1	Paris
<g/>
:	:	kIx,	:
Gallimard	Gallimard	k1gMnSc1	Gallimard
<g/>
.	.	kIx.	.
</s>
<s>
Pozvaná	pozvaný	k2eAgFnSc1d1	pozvaná
-	-	kIx~	-
první	první	k4xOgInSc1	první
román	román	k1gInSc1	román
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
existencialismu	existencialismus	k1gInSc2	existencialismus
hovořící	hovořící	k2eAgFnPc1d1	hovořící
o	o	k7c6	o
beletrizovaném	beletrizovaný	k2eAgInSc6d1	beletrizovaný
trojúhelníkovém	trojúhelníkový	k2eAgInSc6d1	trojúhelníkový
vztahu	vztah	k1gInSc6	vztah
mezi	mezi	k7c7	mezi
autorkou	autorka	k1gFnSc7	autorka
<g/>
,	,	kIx,	,
Saartrem	Saartr	k1gInSc7	Saartr
a	a	k8xC	a
studentkou	studentka	k1gFnSc7	studentka
Olgou	Olga	k1gFnSc7	Olga
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
odhaluje	odhalovat	k5eAaImIp3nS	odhalovat
autorka	autorka	k1gFnSc1	autorka
nechuť	nechuť	k1gFnSc1	nechuť
k	k	k7c3	k
tradičnímu	tradiční	k2eAgInSc3d1	tradiční
buržoaznímu	buržoazní	k2eAgInSc3d1	buržoazní
způsobu	způsob	k1gInSc3	způsob
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
vyrůstala	vyrůstat	k5eAaImAgFnS	vyrůstat
<g/>
,	,	kIx,	,
a	a	k8xC	a
touhu	touha	k1gFnSc4	touha
po	po	k7c6	po
partnerské	partnerský	k2eAgFnSc6d1	partnerská
harmonii	harmonie	k1gFnSc6	harmonie
<g/>
.	.	kIx.	.
</s>
<s>
Dochází	docházet	k5eAaImIp3nS	docházet
ale	ale	k9	ale
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
naprosté	naprostý	k2eAgNnSc1d1	naprosté
sjednocení	sjednocení	k1gNnSc1	sjednocení
dvou	dva	k4xCgMnPc2	dva
jedinců	jedinec	k1gMnPc2	jedinec
je	být	k5eAaImIp3nS	být
nemožné	možný	k2eNgNnSc1d1	nemožné
-	-	kIx~	-
existují	existovat	k5eAaImIp3nP	existovat
chvíle	chvíle	k1gFnPc1	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
každý	každý	k3xTgMnSc1	každý
z	z	k7c2	z
nás	my	k3xPp1nPc2	my
osamocen	osamotit	k5eAaPmNgInS	osamotit
<g/>
.	.	kIx.	.
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
Pyrrhus	Pyrrhus	k1gMnSc1	Pyrrhus
et	et	k?	et
Cinéas	Cinéas	k1gMnSc1	Cinéas
<g/>
,	,	kIx,	,
Paris	Paris	k1gMnSc1	Paris
<g/>
:	:	kIx,	:
Gallimard	Gallimard	k1gMnSc1	Gallimard
<g/>
.	.	kIx.	.
</s>
<s>
Pyrrhus	Pyrrhus	k1gMnSc1	Pyrrhus
rozmlouvá	rozmlouvat	k5eAaImIp3nS	rozmlouvat
s	s	k7c7	s
Kineou	Kinea	k1gFnSc7	Kinea
-	-	kIx~	-
dílem	dílo	k1gNnSc7	dílo
začalo	začít	k5eAaPmAgNnS	začít
období	období	k1gNnSc1	období
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
sama	sám	k3xTgFnSc1	sám
autorka	autorka	k1gFnSc1	autorka
nazývá	nazývat	k5eAaImIp3nS	nazývat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
morální	morální	k2eAgFnSc1d1	morální
fáze	fáze	k1gFnSc1	fáze
života	život	k1gInSc2	život
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
La	la	k1gNnSc2	la
Phénoménologie	Phénoménologie	k1gFnSc2	Phénoménologie
de	de	k?	de
la	la	k1gNnPc2	la
perception	perception	k1gInSc1	perception
de	de	k?	de
Maurice	Maurika	k1gFnSc3	Maurika
Merleau-Ponty	Merleau-Ponta	k1gFnSc2	Merleau-Ponta
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Les	les	k1gInSc4	les
Temps	Tempsa	k1gFnPc2	Tempsa
modernes	modernesa	k1gFnPc2	modernesa
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
363	[number]	k4	363
<g/>
-	-	kIx~	-
<g/>
67	[number]	k4	67
<g/>
.	.	kIx.	.
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
Le	Le	k1gMnSc1	Le
Sang	Sang	k1gMnSc1	Sang
des	des	k1gNnSc2	des
autres	autres	k1gMnSc1	autres
<g/>
,	,	kIx,	,
Paris	Paris	k1gMnSc1	Paris
<g/>
:	:	kIx,	:
Gallimard	Gallimard	k1gMnSc1	Gallimard
<g/>
.	.	kIx.	.
</s>
<s>
Krev	krev	k1gFnSc1	krev
těch	ten	k3xDgMnPc2	ten
druhých	druhý	k4xOgMnPc2	druhý
-	-	kIx~	-
román	román	k1gInSc1	román
<g/>
.	.	kIx.	.
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
Les	les	k1gInSc1	les
Bouches	Bouches	k1gMnSc1	Bouches
inutiles	inutiles	k1gMnSc1	inutiles
<g/>
,	,	kIx,	,
Paris	Paris	k1gMnSc1	Paris
<g/>
:	:	kIx,	:
Gallimard	Gallimard	k1gMnSc1	Gallimard
<g/>
.	.	kIx.	.
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Littérature	Littératur	k1gMnSc5	Littératur
et	et	k?	et
métaphysique	métaphysique	k1gFnPc6	métaphysique
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Les	les	k1gInSc4	les
Temps	Tempsa	k1gFnPc2	Tempsa
modernes	modernesa	k1gFnPc2	modernesa
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
:	:	kIx,	:
1153	[number]	k4	1153
<g/>
-	-	kIx~	-
<g/>
63	[number]	k4	63
<g/>
.	.	kIx.	.
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
Tous	Tous	k1gInSc1	Tous
les	les	k1gInSc1	les
homes	homes	k1gInSc1	homes
sont	sont	k2eAgInSc1d1	sont
mortels	mortels	k1gInSc1	mortels
<g/>
,	,	kIx,	,
Paris	Paris	k1gMnSc1	Paris
<g/>
:	:	kIx,	:
Gallimard	Gallimard	k1gMnSc1	Gallimard
<g/>
.	.	kIx.	.
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
Pour	Pour	k1gMnSc1	Pour
une	une	k?	une
morale	morala	k1gFnSc6	morala
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
ambigüité	ambigüitý	k2eAgFnSc2d1	ambigüitý
<g/>
,	,	kIx,	,
Paris	Paris	k1gMnSc1	Paris
<g/>
:	:	kIx,	:
Gallimard	Gallimard	k1gMnSc1	Gallimard
<g/>
.	.	kIx.	.
</s>
<s>
Etika	etika	k1gFnSc1	etika
dvojznačnosti	dvojznačnost	k1gFnSc2	dvojznačnost
-	-	kIx~	-
filozofická	filozofický	k2eAgFnSc1d1	filozofická
práce	práce	k1gFnSc1	práce
představující	představující	k2eAgFnSc1d1	představující
vrchol	vrchol	k1gInSc4	vrchol
jejího	její	k3xOp3gNnSc2	její
moralistického	moralistický	k2eAgNnSc2d1	moralistické
období	období	k1gNnSc2	období
<g/>
.	.	kIx.	.
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Amerique	Ameriqu	k1gMnSc2	Ameriqu
au	au	k0	au
jour	jour	k?	jour
le	le	k?	le
jour	jour	k?	jour
<g/>
,	,	kIx,	,
Paris	Paris	k1gMnSc1	Paris
<g/>
:	:	kIx,	:
Editions	Editions	k1gInSc1	Editions
Paul	Paul	k1gMnSc1	Paul
Marihein	Marihein	k1gMnSc1	Marihein
<g/>
.	.	kIx.	.
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Existentialisme	Existentialismus	k1gInSc5	Existentialismus
et	et	k?	et
la	la	k1gNnSc3	la
sagesse	sagesse	k1gFnSc2	sagesse
des	des	k1gNnSc2	des
nations	nationsa	k1gFnPc2	nationsa
<g/>
,	,	kIx,	,
Paris	Paris	k1gMnSc1	Paris
<g/>
:	:	kIx,	:
Nagel	Nagel	k1gMnSc1	Nagel
<g/>
.	.	kIx.	.
1949	[number]	k4	1949
<g/>
,	,	kIx,	,
Le	Le	k1gFnSc1	Le
Deuxiè	Deuxiè	k1gFnSc1	Deuxiè
sexe	sex	k1gInSc5	sex
<g/>
,	,	kIx,	,
Paris	Paris	k1gMnSc1	Paris
<g/>
:	:	kIx,	:
Gallimard	Gallimard	k1gMnSc1	Gallimard
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgNnSc1	druhý
pohlaví	pohlaví	k1gNnSc1	pohlaví
-	-	kIx~	-
esejistická	esejistický	k2eAgFnSc1d1	esejistická
kniha	kniha	k1gFnSc1	kniha
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc7	její
nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
a	a	k8xC	a
světově	světově	k6eAd1	světově
nejproslulejší	proslulý	k2eAgNnSc4d3	nejproslulejší
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
(	(	kIx(	(
<g/>
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
existencialistické	existencialistický	k2eAgFnSc2d1	existencialistická
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
)	)	kIx)	)
problémem	problém	k1gInSc7	problém
postavení	postavení	k1gNnSc2	postavení
žen	žena	k1gFnPc2	žena
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Publikace	publikace	k1gFnSc1	publikace
kritizuje	kritizovat	k5eAaImIp3nS	kritizovat
fakt	fakt	k1gInSc4	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byly	být	k5eAaImAgFnP	být
ženy	žena	k1gFnPc1	žena
považovány	považován	k2eAgFnPc1d1	považována
za	za	k7c4	za
něco	něco	k3yInSc4	něco
nenormálního	normální	k2eNgNnSc2d1	nenormální
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
muž	muž	k1gMnSc1	muž
byl	být	k5eAaImAgMnS	být
ideálem	ideál	k1gInSc7	ideál
<g/>
.	.	kIx.	.
</s>
<s>
Autorka	autorka	k1gFnSc1	autorka
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
dokázat	dokázat	k5eAaPmF	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
takové	takový	k3xDgNnSc1	takový
myšlení	myšlení	k1gNnSc1	myšlení
je	být	k5eAaImIp3nS	být
špatné	špatný	k2eAgNnSc1d1	špatné
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
implikuje	implikovat	k5eAaImIp3nS	implikovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ženy	žena	k1gFnPc1	žena
jsou	být	k5eAaImIp3nP	být
nějakou	nějaký	k3yIgFnSc7	nějaký
odchylkou	odchylka	k1gFnSc7	odchylka
od	od	k7c2	od
normálu	normál	k1gInSc2	normál
a	a	k8xC	a
normalitu	normalita	k1gFnSc4	normalita
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
měly	mít	k5eAaImAgFnP	mít
snažit	snažit	k5eAaImF	snažit
napodobovat	napodobovat	k5eAaImF	napodobovat
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
sehrála	sehrát	k5eAaPmAgFnS	sehrát
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
při	při	k7c6	při
utváření	utváření	k1gNnSc6	utváření
emancipačního	emancipační	k2eAgNnSc2d1	emancipační
hnutí	hnutí	k1gNnSc2	hnutí
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
60	[number]	k4	60
<g/>
.	.	kIx.	.
a	a	k8xC	a
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
1951	[number]	k4	1951
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Faut-il	Fautl	k1gInSc1	Faut-il
brû	brû	k?	brû
Sade	sad	k1gInSc5	sad
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Les	les	k1gInSc4	les
Temps	Tempsa	k1gFnPc2	Tempsa
modernes	modernesa	k1gFnPc2	modernesa
<g/>
,	,	kIx,	,
74	[number]	k4	74
<g/>
:	:	kIx,	:
1002	[number]	k4	1002
<g/>
-	-	kIx~	-
<g/>
33	[number]	k4	33
<g/>
.	.	kIx.	.
1952	[number]	k4	1952
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Faut-il	Fautl	k1gInSc1	Faut-il
brû	brû	k?	brû
Sade	sad	k1gInSc5	sad
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Les	les	k1gInSc4	les
Temps	Tempsa	k1gFnPc2	Tempsa
modernes	modernesa	k1gFnPc2	modernesa
<g/>
,	,	kIx,	,
75	[number]	k4	75
<g/>
:	:	kIx,	:
1197	[number]	k4	1197
<g/>
-	-	kIx~	-
<g/>
230	[number]	k4	230
<g/>
.	.	kIx.	.
1954	[number]	k4	1954
<g/>
,	,	kIx,	,
Les	les	k1gInSc4	les
Mandarins	Mandarinsa	k1gFnPc2	Mandarinsa
<g/>
,	,	kIx,	,
Paris	Paris	k1gMnSc1	Paris
<g/>
:	:	kIx,	:
Gallimard	Gallimard	k1gMnSc1	Gallimard
<g/>
.	.	kIx.	.
</s>
<s>
Mandaríni	mandarín	k1gMnPc1	mandarín
-	-	kIx~	-
románová	románový	k2eAgFnSc1d1	románová
kronika	kronika	k1gFnSc1	kronika
levicové	levicový	k2eAgFnSc2d1	levicová
inteligence	inteligence	k1gFnSc2	inteligence
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
1955	[number]	k4	1955
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Merleau-Ponty	Merleau-Ponta	k1gFnPc1	Merleau-Ponta
et	et	k?	et
le	le	k?	le
pseudo-sartrisme	pseudoartrismus	k1gInSc5	pseudo-sartrismus
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Les	les	k1gInSc4	les
Temps	Tempsa	k1gFnPc2	Tempsa
modernes	modernesa	k1gFnPc2	modernesa
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
:	:	kIx,	:
2072	[number]	k4	2072
<g/>
-	-	kIx~	-
<g/>
122	[number]	k4	122
<g/>
.	.	kIx.	.
1955	[number]	k4	1955
<g/>
,	,	kIx,	,
Privilè	Privilè	k1gMnSc1	Privilè
<g/>
,	,	kIx,	,
Paris	Paris	k1gMnSc1	Paris
<g/>
:	:	kIx,	:
Gallimard	Gallimard	k1gMnSc1	Gallimard
<g/>
.	.	kIx.	.
1957	[number]	k4	1957
<g/>
,	,	kIx,	,
La	la	k1gNnSc1	la
Longue	Longue	k1gFnPc2	Longue
marche	marche	k1gNnSc2	marche
<g/>
,	,	kIx,	,
essai	essai	k1gNnSc2	essai
sur	sur	k?	sur
la	la	k0	la
Chine	Chin	k1gMnSc5	Chin
<g/>
,	,	kIx,	,
Paris	Paris	k1gMnSc1	Paris
<g/>
:	:	kIx,	:
Gallimard	Gallimard	k1gMnSc1	Gallimard
<g/>
.	.	kIx.	.
1958	[number]	k4	1958
<g/>
,	,	kIx,	,
Mémoires	Mémoires	k1gInSc1	Mémoires
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
une	une	k?	une
jeune	jeunout	k5eAaImIp3nS	jeunout
fille	fille	k1gFnSc1	fille
rangée	rangée	k1gFnSc1	rangée
<g/>
,	,	kIx,	,
Paris	Paris	k1gMnSc1	Paris
<g/>
:	:	kIx,	:
Gallimard	Gallimard	k1gMnSc1	Gallimard
<g/>
.	.	kIx.	.
</s>
<s>
Paměti	paměť	k1gFnPc1	paměť
spořádané	spořádaný	k2eAgFnSc2d1	spořádaná
dívky	dívka	k1gFnSc2	dívka
<g/>
.	.	kIx.	.
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
La	la	k1gNnSc1	la
Force	force	k1gFnSc2	force
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
âge	âge	k?	âge
<g/>
,	,	kIx,	,
Paris	Paris	k1gMnSc1	Paris
<g/>
:	:	kIx,	:
Gallimard	Gallimard	k1gMnSc1	Gallimard
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nejlepších	dobrý	k2eAgNnPc6d3	nejlepší
letech	léto	k1gNnPc6	léto
-	-	kIx~	-
román	román	k1gInSc1	román
inspirovaný	inspirovaný	k2eAgInSc1d1	inspirovaný
vztahem	vztah	k1gInSc7	vztah
se	s	k7c7	s
Sartrem	Sartr	k1gInSc7	Sartr
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
autorka	autorka	k1gFnSc1	autorka
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
vztahové	vztahový	k2eAgNnSc4d1	vztahové
myšlení	myšlení	k1gNnSc4	myšlení
typu	typ	k1gInSc2	typ
"	"	kIx"	"
<g/>
já	já	k3xPp1nSc1	já
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
my	my	k3xPp1nPc1	my
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Preface	preface	k1gFnSc1	preface
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
in	in	k?	in
Djamila	Djamila	k1gMnSc1	Djamila
Boupacha	Boupacha	k1gMnSc1	Boupacha
<g/>
,	,	kIx,	,
S.	S.	kA	S.
de	de	k?	de
Beauvoir	Beauvoir	k1gInSc1	Beauvoir
and	and	k?	and
G.	G.	kA	G.
Halimi	Hali	k1gFnPc7	Hali
<g/>
,	,	kIx,	,
Paris	Paris	k1gMnSc1	Paris
<g/>
:	:	kIx,	:
Gallimard	Gallimard	k1gMnSc1	Gallimard
<g/>
.	.	kIx.	.
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
La	la	k1gNnSc1	la
Force	force	k1gFnPc2	force
des	des	k1gNnSc1	des
choses	choses	k1gMnSc1	choses
<g/>
,	,	kIx,	,
Paris	Paris	k1gMnSc1	Paris
<g/>
:	:	kIx,	:
Gallimard	Gallimard	k1gMnSc1	Gallimard
<g/>
.	.	kIx.	.
</s>
<s>
Síla	síla	k1gFnSc1	síla
okolností	okolnost	k1gFnPc2	okolnost
-	-	kIx~	-
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
líčí	líčit	k5eAaImIp3nS	líčit
svůj	svůj	k3xOyFgInSc4	svůj
a	a	k8xC	a
Sartrův	Sartrův	k2eAgInSc4d1	Sartrův
myšlenkový	myšlenkový	k2eAgInSc4d1	myšlenkový
vývoj	vývoj	k1gInSc4	vývoj
<g/>
.	.	kIx.	.
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Preface	preface	k1gFnSc1	preface
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
in	in	k?	in
La	la	k0	la
Bâtarde	Bâtard	k1gMnSc5	Bâtard
<g/>
,	,	kIx,	,
V.	V.	kA	V.
Leduc	Leduc	k1gInSc1	Leduc
<g/>
,	,	kIx,	,
Paris	Paris	k1gMnSc1	Paris
<g/>
:	:	kIx,	:
Gallimard	Gallimard	k1gMnSc1	Gallimard
<g/>
.	.	kIx.	.
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
Une	Une	k1gMnSc1	Une
Mort	Mort	k1gMnSc1	Mort
trè	trè	k?	trè
douce	douce	k1gMnSc1	douce
<g/>
,	,	kIx,	,
Paris	Paris	k1gMnSc1	Paris
<g/>
:	:	kIx,	:
Gallimard	Gallimard	k1gMnSc1	Gallimard
<g/>
.	.	kIx.	.
</s>
<s>
Velice	velice	k6eAd1	velice
lehká	lehký	k2eAgFnSc1d1	lehká
smrt	smrt	k1gFnSc1	smrt
-	-	kIx~	-
román	román	k1gInSc1	román
popisující	popisující	k2eAgFnSc2d1	popisující
poslední	poslední	k2eAgFnSc2d1	poslední
dny	dna	k1gFnSc2	dna
její	její	k3xOp3gFnSc2	její
matky	matka	k1gFnSc2	matka
<g/>
.	.	kIx.	.
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Que	Que	k1gMnSc1	Que
peut	peut	k1gMnSc1	peut
la	la	k1gNnPc2	la
littérature	littératur	k1gInSc5	littératur
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Le	Le	k1gMnSc5	Le
Monde	Mond	k1gMnSc5	Mond
<g/>
,	,	kIx,	,
249	[number]	k4	249
<g/>
:	:	kIx,	:
73	[number]	k4	73
<g/>
-	-	kIx~	-
<g/>
92	[number]	k4	92
<g/>
.	.	kIx.	.
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Preface	preface	k1gFnSc1	preface
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
in	in	k?	in
Tréblinka	Tréblinka	k1gFnSc1	Tréblinka
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Steiner	Steiner	k1gMnSc1	Steiner
<g/>
,	,	kIx,	,
Paris	Paris	k1gMnSc1	Paris
<g/>
:	:	kIx,	:
Fayard	Fayard	k1gMnSc1	Fayard
<g/>
.	.	kIx.	.
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
Les	les	k1gInSc1	les
Belles	Belles	k1gMnSc1	Belles
images	images	k1gMnSc1	images
<g/>
,	,	kIx,	,
Paris	Paris	k1gMnSc1	Paris
<g/>
:	:	kIx,	:
Gallimard	Gallimard	k1gMnSc1	Gallimard
<g/>
.	.	kIx.	.
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
La	la	k1gNnSc1	la
Femme	Femm	k1gMnSc5	Femm
rompue	rompuus	k1gMnSc5	rompuus
<g/>
,	,	kIx,	,
Paris	Paris	k1gMnSc1	Paris
<g/>
:	:	kIx,	:
Gallimard	Gallimard	k1gMnSc1	Gallimard
<g/>
.	.	kIx.	.
1970	[number]	k4	1970
<g/>
,	,	kIx,	,
La	la	k1gNnSc1	la
Vieillesse	Vieillesse	k1gFnSc2	Vieillesse
<g/>
,	,	kIx,	,
Paris	Paris	k1gMnSc1	Paris
<g/>
:	:	kIx,	:
Gallimard	Gallimard	k1gMnSc1	Gallimard
<g/>
.	.	kIx.	.
1972	[number]	k4	1972
<g/>
,	,	kIx,	,
Tout	Tout	k1gInSc1	Tout
compte	compat	k5eAaPmRp2nP	compat
fait	fait	k1gInSc4	fait
<g/>
,	,	kIx,	,
Paris	Paris	k1gMnSc1	Paris
<g/>
:	:	kIx,	:
Gallimard	Gallimard	k1gMnSc1	Gallimard
<g/>
.	.	kIx.	.
</s>
<s>
Zúčtování	zúčtování	k1gNnSc1	zúčtování
-	-	kIx~	-
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
podává	podávat	k5eAaImIp3nS	podávat
bilanci	bilance	k1gFnSc4	bilance
svého	svůj	k3xOyFgInSc2	svůj
spisovatelského	spisovatelský	k2eAgInSc2d1	spisovatelský
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
1979	[number]	k4	1979
<g/>
,	,	kIx,	,
Quand	Quand	k1gInSc1	Quand
prime	prim	k1gInSc5	prim
le	le	k?	le
spirituel	spirituel	k1gMnSc1	spirituel
<g/>
,	,	kIx,	,
Paris	Paris	k1gMnSc1	Paris
<g/>
:	:	kIx,	:
Gallimard	Gallimard	k1gMnSc1	Gallimard
<g/>
.	.	kIx.	.
1979	[number]	k4	1979
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Mon	Mon	k1gFnSc1	Mon
expérience	expérience	k1gFnSc2	expérience
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
écrivain	écrivain	k1gMnSc1	écrivain
(	(	kIx(	(
<g/>
September	September	k1gInSc1	September
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
in	in	k?	in
Les	les	k1gInSc1	les
Écrits	Écrits	k1gInSc1	Écrits
de	de	k?	de
Simone	Simon	k1gMnSc5	Simon
de	de	k?	de
Beauvoir	Beauvoir	k1gMnSc1	Beauvoir
<g/>
,	,	kIx,	,
C.	C.	kA	C.
Francis	Francis	k1gFnSc1	Francis
and	and	k?	and
F.	F.	kA	F.
Gontier	Gontier	k1gMnSc1	Gontier
<g/>
,	,	kIx,	,
Paris	Paris	k1gMnSc1	Paris
<g/>
:	:	kIx,	:
Gallimard	Gallimard	k1gMnSc1	Gallimard
<g/>
.	.	kIx.	.
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
La	la	k1gNnSc1	la
Cérémonie	Cérémonie	k1gFnSc2	Cérémonie
des	des	k1gNnSc1	des
adieux	adieux	k1gInSc4	adieux
<g/>
,	,	kIx,	,
suivi	suiev	k1gFnSc3	suiev
de	de	k?	de
Entretiens	Entretiens	k1gInSc1	Entretiens
avec	avec	k1gInSc1	avec
Jean-Paul	Jean-Paul	k1gInSc1	Jean-Paul
Sartre	Sartr	k1gInSc5	Sartr
<g/>
,	,	kIx,	,
Aoû	Aoû	k1gMnSc7	Aoû
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
Paris	Paris	k1gMnSc1	Paris
<g/>
:	:	kIx,	:
Gallimard	Gallimard	k1gMnSc1	Gallimard
<g/>
.	.	kIx.	.
</s>
<s>
Obřad	obřad	k1gInSc1	obřad
odloučení	odloučení	k1gNnSc2	odloučení
-	-	kIx~	-
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
jakousi	jakýsi	k3yIgFnSc7	jakýsi
beletristickou	beletristický	k2eAgFnSc7d1	beletristická
vzpomínkou	vzpomínka	k1gFnSc7	vzpomínka
na	na	k7c4	na
Sartra	Sartrum	k1gNnPc4	Sartrum
<g/>
.	.	kIx.	.
1985	[number]	k4	1985
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Préface	Préface	k1gFnSc1	Préface
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
in	in	k?	in
Shoah	Shoah	k1gInSc1	Shoah
<g/>
,	,	kIx,	,
C.	C.	kA	C.
Lanzmann	Lanzmann	k1gMnSc1	Lanzmann
<g/>
,	,	kIx,	,
Paris	Paris	k1gMnSc1	Paris
<g/>
:	:	kIx,	:
Fayard	Fayard	k1gMnSc1	Fayard
<g/>
.	.	kIx.	.
</s>
<s>
Jean-Paul	Jean-Paul	k1gInSc1	Jean-Paul
Sartre	Sartr	k1gMnSc5	Sartr
a	a	k8xC	a
Simone	Simon	k1gMnSc5	Simon
de	de	k?	de
Beauvoir	Beauvoir	k1gInSc4	Beauvoir
jsou	být	k5eAaImIp3nP	být
hlavními	hlavní	k2eAgFnPc7d1	hlavní
postavami	postava	k1gFnPc7	postava
rozhlasové	rozhlasový	k2eAgFnSc2d1	rozhlasová
hry	hra	k1gFnSc2	hra
Benjamina	Benjamin	k1gMnSc2	Benjamin
Kurase	Kuras	k1gInSc6	Kuras
Sebeklamy	sebeklam	k1gInPc1	sebeklam
<g/>
:	:	kIx,	:
lehce	lehko	k6eAd1	lehko
absurdní	absurdní	k2eAgFnSc2d1	absurdní
rozhlasové	rozhlasový	k2eAgFnSc2d1	rozhlasová
komedie	komedie	k1gFnSc2	komedie
s	s	k7c7	s
francouzsko-levičáckými	francouzskoevičácký	k2eAgInPc7d1	francouzsko-levičácký
nápěvy	nápěv	k1gInPc7	nápěv
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Michal	Michal	k1gMnSc1	Michal
Bureš	Bureš	k1gMnSc1	Bureš
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavních	hlavní	k2eAgFnPc6d1	hlavní
rolích	role	k1gFnPc6	role
Hana	Hana	k1gFnSc1	Hana
Maciuchová	Maciuchová	k1gFnSc1	Maciuchová
a	a	k8xC	a
Igor	Igor	k1gMnSc1	Igor
Bareš	Bareš	k1gMnSc1	Bareš
<g/>
,	,	kIx,	,
zpracováno	zpracován	k2eAgNnSc1d1	zpracováno
v	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
rozhlasu	rozhlas	k1gInSc6	rozhlas
-	-	kIx~	-
Vltava	Vltava	k1gFnSc1	Vltava
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
</s>
