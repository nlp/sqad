<s>
Simone	Simon	k1gMnSc5	Simon
de	de	k?	de
Beauvoir	Beauvoir	k1gInSc1	Beauvoir
[	[	kIx(	[
<g/>
výslovnost	výslovnost	k1gFnSc1	výslovnost
si	se	k3xPyFc3	se
<g/>
:	:	kIx,	:
<g/>
ˈ	ˈ	k?	ˈ
də	də	k?	də
bo	bo	k?	bo
<g/>
:	:	kIx,	:
<g/>
ˈ	ˈ	k?	ˈ
<g/>
:	:	kIx,	:
<g/>
ʀ	ʀ	k?	ʀ
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
celým	celý	k2eAgNnSc7d1	celé
jménem	jméno	k1gNnSc7	jméno
Simone-Lucie-Ernestine-Marie	Simone-Lucie-Ernestine-Marie	k1gFnSc2	Simone-Lucie-Ernestine-Marie
Bertrand	Bertranda	k1gFnPc2	Bertranda
de	de	k?	de
Beauvoir	Beauvoira	k1gFnPc2	Beauvoira
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
též	též	k9	též
Simone	Simon	k1gMnSc5	Simon
de	de	k?	de
Beauvoirová	Beauvoirová	k1gFnSc1	Beauvoirová
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Simone	Simon	k1gMnSc5	Simon
Beauvoirová	Beauvoirová	k1gFnSc1	Beauvoirová
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1908	[number]	k4	1908
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
-	-	kIx~	-
14	[number]	k4	14
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
významná	významný	k2eAgFnSc1d1	významná
francouzská	francouzský	k2eAgFnSc1d1	francouzská
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
<g/>
,	,	kIx,	,
myslitelka	myslitelka	k1gFnSc1	myslitelka
<g/>
,	,	kIx,	,
filozofka	filozofka	k1gFnSc1	filozofka
a	a	k8xC	a
existencialistka	existencialistka	k1gFnSc1	existencialistka
<g/>
.	.	kIx.	.
</s>
