<s>
Předseda	předseda	k1gMnSc1	předseda
Evropské	evropský	k2eAgFnSc2d1	Evropská
rady	rada	k1gFnSc2	rada
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
President	president	k1gMnSc1	president
of	of	k?	of
the	the	k?	the
European	European	k1gMnSc1	European
Council	Council	k1gMnSc1	Council
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
také	také	k9	také
nepřesně	přesně	k6eNd1	přesně
jako	jako	k8xS	jako
prezident	prezident	k1gMnSc1	prezident
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
prezident	prezident	k1gMnSc1	prezident
EU	EU	kA	EU
<g/>
,	,	kIx,	,
evropský	evropský	k2eAgMnSc1d1	evropský
prezident	prezident	k1gMnSc1	prezident
nebo	nebo	k8xC	nebo
prezident	prezident	k1gMnSc1	prezident
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
osoba	osoba	k1gFnSc1	osoba
odpovědná	odpovědný	k2eAgFnSc1d1	odpovědná
za	za	k7c4	za
předsedání	předsedání	k1gNnSc4	předsedání
Evropské	evropský	k2eAgFnSc2d1	Evropská
radě	rada	k1gFnSc3	rada
a	a	k8xC	a
vedení	vedení	k1gNnSc4	vedení
jejích	její	k3xOp3gInPc2	její
summitů	summit	k1gInPc2	summit
<g/>
.	.	kIx.	.
</s>
<s>
Předseda	předseda	k1gMnSc1	předseda
Evropské	evropský	k2eAgFnSc2d1	Evropská
rady	rada	k1gFnSc2	rada
rovněž	rovněž	k9	rovněž
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
Evropskou	evropský	k2eAgFnSc4d1	Evropská
unii	unie	k1gFnSc4	unie
na	na	k7c6	na
světové	světový	k2eAgFnSc6d1	světová
scéně	scéna	k1gFnSc6	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2014	[number]	k4	2014
je	být	k5eAaImIp3nS	být
předsedou	předseda	k1gMnSc7	předseda
Evropské	evropský	k2eAgFnSc2d1	Evropská
rady	rada	k1gFnSc2	rada
bývalý	bývalý	k2eAgMnSc1d1	bývalý
polský	polský	k2eAgMnSc1d1	polský
premiér	premiér	k1gMnSc1	premiér
Donald	Donald	k1gMnSc1	Donald
Tusk	Tusk	k1gMnSc1	Tusk
<g/>
.	.	kIx.	.
</s>
<s>
Lisabonská	lisabonský	k2eAgFnSc1d1	Lisabonská
smlouva	smlouva	k1gFnSc1	smlouva
<g/>
,	,	kIx,	,
platná	platný	k2eAgFnSc1d1	platná
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Evropská	evropský	k2eAgFnSc1d1	Evropská
rada	rada	k1gFnSc1	rada
volí	volit	k5eAaImIp3nS	volit
svého	svůj	k3xOyFgMnSc4	svůj
stálého	stálý	k2eAgMnSc4d1	stálý
předsedu	předseda	k1gMnSc4	předseda
kvalifikovanou	kvalifikovaný	k2eAgFnSc7d1	kvalifikovaná
většinou	většina	k1gFnSc7	většina
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
a	a	k8xC	a
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
s	s	k7c7	s
jednou	jeden	k4xCgFnSc7	jeden
možností	možnost	k1gFnSc7	možnost
znovuzvolení	znovuzvolení	k1gNnSc2	znovuzvolení
<g/>
.	.	kIx.	.
</s>
<s>
Odvolání	odvolání	k1gNnSc1	odvolání
předsedy	předseda	k1gMnSc2	předseda
Evropské	evropský	k2eAgFnSc2d1	Evropská
rady	rada	k1gFnSc2	rada
(	(	kIx(	(
<g/>
v	v	k7c6	v
případě	případ	k1gInSc6	případ
překážky	překážka	k1gFnSc2	překážka
nebo	nebo	k8xC	nebo
závažného	závažný	k2eAgNnSc2d1	závažné
pochybení	pochybení	k1gNnSc2	pochybení
<g/>
)	)	kIx)	)
rovněž	rovněž	k9	rovněž
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
kvalifikovanou	kvalifikovaný	k2eAgFnSc4d1	kvalifikovaná
většinu	většina	k1gFnSc4	většina
<g/>
.	.	kIx.	.
</s>
<s>
Předseda	předseda	k1gMnSc1	předseda
Evropské	evropský	k2eAgFnSc2d1	Evropská
rady	rada	k1gFnSc2	rada
nesmí	smět	k5eNaImIp3nS	smět
současně	současně	k6eAd1	současně
zastávat	zastávat	k5eAaImF	zastávat
žádnou	žádný	k3yNgFnSc4	žádný
vnitrostátní	vnitrostátní	k2eAgFnSc4d1	vnitrostátní
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
summitu	summit	k1gInSc6	summit
Evropské	evropský	k2eAgFnSc2d1	Evropská
rady	rada	k1gFnSc2	rada
v	v	k7c6	v
Bruselu	Brusel	k1gInSc6	Brusel
19	[number]	k4	19
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2009	[number]	k4	2009
byl	být	k5eAaImAgInS	být
historicky	historicky	k6eAd1	historicky
prvním	první	k4xOgMnSc7	první
stálým	stálý	k2eAgMnSc7d1	stálý
předsedou	předseda	k1gMnSc7	předseda
Evropské	evropský	k2eAgFnSc2d1	Evropská
rady	rada	k1gFnSc2	rada
zvolen	zvolen	k2eAgMnSc1d1	zvolen
belgický	belgický	k2eAgMnSc1d1	belgický
premiér	premiér	k1gMnSc1	premiér
Herman	Herman	k1gMnSc1	Herman
Van	van	k1gInSc4	van
Rompuy	Rompua	k1gFnSc2	Rompua
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
svého	svůj	k3xOyFgInSc2	svůj
úřadu	úřad	k1gInSc2	úřad
ujal	ujmout	k5eAaPmAgInS	ujmout
1	[number]	k4	1
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
byl	být	k5eAaImAgMnS	být
předseda	předseda	k1gMnSc1	předseda
Evropské	evropský	k2eAgFnSc2d1	Evropská
rady	rada	k1gFnSc2	rada
neoficiální	oficiální	k2eNgFnSc1d1	neoficiální
pozicí	pozice	k1gFnSc7	pozice
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
vykonával	vykonávat	k5eAaImAgMnS	vykonávat
šéf	šéf	k1gMnSc1	šéf
exekutivy	exekutiva	k1gFnSc2	exekutiva
členského	členský	k2eAgInSc2d1	členský
státu	stát	k1gInSc2	stát
řídící	řídící	k2eAgFnPc4d1	řídící
v	v	k7c6	v
daném	daný	k2eAgNnSc6d1	dané
období	období	k1gNnSc6	období
Evropskou	evropský	k2eAgFnSc4d1	Evropská
radu	rada	k1gFnSc4	rada
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
každého	každý	k3xTgInSc2	každý
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
rotujícího	rotující	k2eAgNnSc2d1	rotující
Předsednictví	předsednictví	k1gNnSc2	předsednictví
Evropské	evropský	k2eAgFnSc2d1	Evropská
rady	rada	k1gFnSc2	rada
a	a	k8xC	a
Rady	rada	k1gFnSc2	rada
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
se	se	k3xPyFc4	se
předsednictví	předsednictví	k1gNnSc1	předsednictví
Evropské	evropský	k2eAgFnSc2d1	Evropská
rady	rada	k1gFnSc2	rada
vystřídalo	vystřídat	k5eAaPmAgNnS	vystřídat
do	do	k7c2	do
1	[number]	k4	1
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2009	[number]	k4	2009
71	[number]	k4	71
krát	krát	k6eAd1	krát
<g/>
.	.	kIx.	.
</s>
