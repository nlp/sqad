<s>
Britské	britský	k2eAgNnSc1d1	Britské
muzeum	muzeum	k1gNnSc1	muzeum
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
londýnském	londýnský	k2eAgInSc6d1	londýnský
obvodu	obvod	k1gInSc6	obvod
Camden	Camdna	k1gFnPc2	Camdna
je	být	k5eAaImIp3nS	být
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgNnPc2d3	veliký
a	a	k8xC	a
nejvýznamnějších	významný	k2eAgNnPc2d3	nejvýznamnější
muzeí	muzeum	k1gNnPc2	muzeum
historie	historie	k1gFnSc2	historie
a	a	k8xC	a
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
roku	rok	k1gInSc2	rok
1753	[number]	k4	1753
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc7	jeho
základem	základ	k1gInSc7	základ
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
sbírka	sbírka	k1gFnSc1	sbírka
lékaře	lékař	k1gMnSc2	lékař
a	a	k8xC	a
vědce	vědec	k1gMnSc2	vědec
sira	sir	k1gMnSc2	sir
Hanse	Hans	k1gMnSc2	Hans
Sloana	Sloan	k1gMnSc2	Sloan
<g/>
.	.	kIx.	.
</s>
<s>
Muzeum	muzeum	k1gNnSc1	muzeum
bylo	být	k5eAaImAgNnS	být
zpřístupněno	zpřístupnit	k5eAaPmNgNnS	zpřístupnit
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
15	[number]	k4	15
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1759	[number]	k4	1759
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
umístěno	umístit	k5eAaPmNgNnS	umístit
v	v	k7c6	v
Montagu	Montag	k1gInSc6	Montag
House	house	k1gNnSc4	house
v	v	k7c6	v
Bloomsbury	Bloomsbura	k1gFnSc2	Bloomsbura
<g/>
,	,	kIx,	,
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
současná	současný	k2eAgFnSc1d1	současná
budova	budova	k1gFnSc1	budova
muzea	muzeum	k1gNnSc2	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Britské	britský	k2eAgNnSc1d1	Britské
muzeum	muzeum	k1gNnSc1	muzeum
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
sbírky	sbírka	k1gFnPc4	sbírka
obsahující	obsahující	k2eAgFnPc4d1	obsahující
více	hodně	k6eAd2	hodně
než	než	k8xS	než
sedm	sedm	k4xCc1	sedm
miliónů	milión	k4xCgInPc2	milión
předmětů	předmět	k1gInPc2	předmět
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
kontinentů	kontinent	k1gInPc2	kontinent
dokumentujících	dokumentující	k2eAgInPc2d1	dokumentující
historii	historie	k1gFnSc4	historie
lidstva	lidstvo	k1gNnSc2	lidstvo
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
až	až	k6eAd1	až
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgInPc1d1	mnohý
objekty	objekt	k1gInPc1	objekt
jsou	být	k5eAaImIp3nP	být
uschovány	uschovat	k5eAaPmNgInP	uschovat
mimo	mimo	k7c4	mimo
budovu	budova	k1gFnSc4	budova
muzea	muzeum	k1gNnSc2	muzeum
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
nedostatku	nedostatek	k1gInSc2	nedostatek
místa	místo	k1gNnSc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Vstup	vstup	k1gInSc1	vstup
do	do	k7c2	do
muzea	muzeum	k1gNnSc2	muzeum
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
do	do	k7c2	do
mnoha	mnoho	k4c2	mnoho
dalších	další	k2eAgNnPc2d1	další
britských	britský	k2eAgNnPc2d1	Britské
muzeí	muzeum	k1gNnPc2	muzeum
je	být	k5eAaImIp3nS	být
volný	volný	k2eAgInSc1d1	volný
<g/>
,	,	kIx,	,
platba	platba	k1gFnSc1	platba
vstupného	vstupné	k1gNnSc2	vstupné
je	být	k5eAaImIp3nS	být
požadována	požadovat	k5eAaImNgFnS	požadovat
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
některé	některý	k3yIgFnPc4	některý
specializované	specializovaný	k2eAgFnPc4d1	specializovaná
akce	akce	k1gFnPc4	akce
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
byly	být	k5eAaImAgFnP	být
obsahem	obsah	k1gInSc7	obsah
sbírek	sbírka	k1gFnPc2	sbírka
muzea	muzeum	k1gNnSc2	muzeum
umělecké	umělecký	k2eAgInPc1d1	umělecký
předměty	předmět	k1gInPc1	předmět
a	a	k8xC	a
starožitnosti	starožitnost	k1gFnPc1	starožitnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
muzeum	muzeum	k1gNnSc1	muzeum
zaměřeno	zaměřit	k5eAaPmNgNnS	zaměřit
jako	jako	k8xC	jako
všeobecné	všeobecný	k2eAgNnSc1d1	všeobecné
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
kolekce	kolekce	k1gFnSc1	kolekce
věnovaná	věnovaný	k2eAgFnSc1d1	věnovaná
Hansem	Hans	k1gMnSc7	Hans
Sloanem	Sloan	k1gMnSc7	Sloan
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
asi	asi	k9	asi
40	[number]	k4	40
000	[number]	k4	000
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,	,
7	[number]	k4	7
000	[number]	k4	000
rukopisů	rukopis	k1gInPc2	rukopis
<g/>
,	,	kIx,	,
tisky	tisk	k1gInPc1	tisk
Albrechta	Albrecht	k1gMnSc2	Albrecht
Dürera	Dürer	k1gMnSc2	Dürer
a	a	k8xC	a
mnohé	mnohý	k2eAgFnSc2d1	mnohá
starožitnosti	starožitnost	k1gFnSc2	starožitnost
z	z	k7c2	z
Egypta	Egypt	k1gInSc2	Egypt
<g/>
,	,	kIx,	,
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
,	,	kIx,	,
středního	střední	k2eAgInSc2d1	střední
a	a	k8xC	a
dalekého	daleký	k2eAgInSc2d1	daleký
východu	východ	k1gInSc2	východ
a	a	k8xC	a
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Zakládací	zakládací	k2eAgFnSc1d1	zakládací
listina	listina	k1gFnSc1	listina
z	z	k7c2	z
7	[number]	k4	7
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1753	[number]	k4	1753
zařadila	zařadit	k5eAaPmAgFnS	zařadit
do	do	k7c2	do
sbírek	sbírka	k1gFnPc2	sbírka
muzea	muzeum	k1gNnSc2	muzeum
i	i	k8xC	i
další	další	k2eAgFnPc1d1	další
dvě	dva	k4xCgFnPc1	dva
knihovny	knihovna	k1gFnPc1	knihovna
–	–	k?	–
Cottonian	Cottonian	k1gInSc1	Cottonian
Library	Librara	k1gFnSc2	Librara
sira	sir	k1gMnSc2	sir
Roberta	Robert	k1gMnSc2	Robert
Cottona	Cotton	k1gMnSc2	Cotton
a	a	k8xC	a
Harleian	Harleian	k1gMnSc1	Harleian
Library	Librara	k1gFnSc2	Librara
–	–	k?	–
prvního	první	k4xOgMnSc2	první
a	a	k8xC	a
druhého	druhý	k4xOgMnSc2	druhý
hraběte	hrabě	k1gMnSc2	hrabě
z	z	k7c2	z
Oxfordu	Oxford	k1gInSc2	Oxford
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1757	[number]	k4	1757
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
sbírek	sbírka	k1gFnPc2	sbírka
přidána	přidán	k2eAgFnSc1d1	přidána
i	i	k8xC	i
Royal	Royal	k1gInSc4	Royal
Library	Librara	k1gFnSc2	Librara
<g/>
.	.	kIx.	.
</s>
<s>
Správní	správní	k2eAgFnSc1d1	správní
rada	rada	k1gFnSc1	rada
muzea	muzeum	k1gNnSc2	muzeum
(	(	kIx(	(
<g/>
jejímiž	jejíž	k3xOyRp3gMnPc7	jejíž
členy	člen	k1gMnPc7	člen
byli	být	k5eAaImAgMnP	být
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1693	[number]	k4	1693
a	a	k8xC	a
Arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
Canterburský	Canterburský	k2eAgMnSc1d1	Canterburský
<g/>
,	,	kIx,	,
lord	lord	k1gMnSc1	lord
kancléř	kancléř	k1gMnSc1	kancléř
a	a	k8xC	a
mluvčí	mluvčí	k1gMnSc1	mluvčí
Dolní	dolní	k2eAgFnSc2d1	dolní
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
)	)	kIx)	)
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
o	o	k7c4	o
umístění	umístění	k1gNnSc4	umístění
muzea	muzeum	k1gNnSc2	muzeum
v	v	k7c6	v
Montagu	Montag	k1gInSc6	Montag
House	house	k1gNnSc1	house
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
odkoupen	odkoupit	k5eAaPmNgInS	odkoupit
od	od	k7c2	od
rodiny	rodina	k1gFnSc2	rodina
Montaguových	Montaguův	k2eAgMnPc2d1	Montaguův
za	za	k7c4	za
20	[number]	k4	20
000	[number]	k4	000
£	£	k?	£
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc1	návrh
na	na	k7c4	na
umístění	umístění	k1gNnSc4	umístění
do	do	k7c2	do
Buckingham	Buckingham	k1gInSc1	Buckingham
House	house	k1gNnSc1	house
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
dobudovaný	dobudovaný	k2eAgInSc1d1	dobudovaný
na	na	k7c4	na
Buckinghamský	buckinghamský	k2eAgInSc4d1	buckinghamský
palác	palác	k1gInSc4	palác
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
vysokých	vysoký	k2eAgInPc2d1	vysoký
nákladů	náklad	k1gInPc2	náklad
a	a	k8xC	a
nevhodného	vhodný	k2eNgNnSc2d1	nevhodné
umístění	umístění	k1gNnSc2	umístění
odmítnuto	odmítnut	k2eAgNnSc1d1	odmítnuto
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
založení	založení	k1gNnSc1	založení
muzeum	muzeum	k1gNnSc4	muzeum
obdrželo	obdržet	k5eAaPmAgNnS	obdržet
další	další	k2eAgInPc4d1	další
dary	dar	k1gInPc4	dar
včetně	včetně	k7c2	včetně
Thomason	Thomasona	k1gFnPc2	Thomasona
Library	Librara	k1gFnSc2	Librara
a	a	k8xC	a
David	David	k1gMnSc1	David
Garrick	Garrick	k1gMnSc1	Garrick
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Library	Librar	k1gMnPc7	Librar
obsahující	obsahující	k2eAgNnSc4d1	obsahující
1000	[number]	k4	1000
tištěných	tištěný	k2eAgFnPc2d1	tištěná
her	hra	k1gFnPc2	hra
ale	ale	k8xC	ale
ve	v	k7c6	v
sbírkách	sbírka	k1gFnPc6	sbírka
bylo	být	k5eAaImAgNnS	být
pouze	pouze	k6eAd1	pouze
málo	málo	k4c1	málo
starověkých	starověký	k2eAgInPc2d1	starověký
předmětů	předmět	k1gInPc2	předmět
a	a	k8xC	a
nebylo	být	k5eNaImAgNnS	být
veřejností	veřejnost	k1gFnPc2	veřejnost
vnímáno	vnímat	k5eAaImNgNnS	vnímat
jako	jako	k8xS	jako
moderní	moderní	k2eAgNnSc1d1	moderní
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
významným	významný	k2eAgMnSc7d1	významný
rozšířením	rozšíření	k1gNnSc7	rozšíření
sbírek	sbírka	k1gFnPc2	sbírka
o	o	k7c4	o
předměty	předmět	k1gInPc4	předmět
staršího	starý	k2eAgNnSc2d2	starší
data	datum	k1gNnSc2	datum
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1782	[number]	k4	1782
nákup	nákup	k1gInSc4	nákup
sbírek	sbírka	k1gFnPc2	sbírka
řeckých	řecký	k2eAgMnPc2d1	řecký
a	a	k8xC	a
římských	římský	k2eAgMnPc2d1	římský
artefaktů	artefakt	k1gInPc2	artefakt
od	od	k7c2	od
sira	sir	k1gMnSc2	sir
Williama	William	k1gMnSc2	William
Hamiltona	Hamilton	k1gMnSc2	Hamilton
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
Francie	Francie	k1gFnSc2	Francie
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Abukiru	Abukir	k1gInSc2	Abukir
roku	rok	k1gInSc2	rok
1801	[number]	k4	1801
získalo	získat	k5eAaPmAgNnS	získat
muzeum	muzeum	k1gNnSc1	muzeum
mnoho	mnoho	k6eAd1	mnoho
egyptských	egyptský	k2eAgFnPc2d1	egyptská
soch	socha	k1gFnPc2	socha
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1805	[number]	k4	1805
se	se	k3xPyFc4	se
sbírky	sbírka	k1gFnPc1	sbírka
rozšířily	rozšířit	k5eAaPmAgFnP	rozšířit
i	i	k9	i
o	o	k7c4	o
řecké	řecký	k2eAgFnPc4d1	řecká
sochy	socha	k1gFnPc4	socha
především	především	k6eAd1	především
z	z	k7c2	z
Towneley	Townelea	k1gFnSc2	Townelea
collection	collection	k1gInSc1	collection
<g/>
.	.	kIx.	.
</s>
<s>
Rozsah	rozsah	k1gInSc1	rozsah
sbírek	sbírka	k1gFnPc2	sbírka
narůstal	narůstat	k5eAaImAgInS	narůstat
a	a	k8xC	a
stal	stát	k5eAaPmAgInS	stát
se	s	k7c7	s
kritickou	kritický	k2eAgFnSc7d1	kritická
poté	poté	k6eAd1	poté
co	co	k3yQnSc4	co
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
1822	[number]	k4	1822
muzeu	muzeum	k1gNnSc6	muzeum
darována	darován	k2eAgFnSc1d1	darována
osobní	osobní	k2eAgFnSc1d1	osobní
knihovna	knihovna	k1gFnSc1	knihovna
Jiřího	Jiří	k1gMnSc2	Jiří
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgFnSc1d1	obsahující
65	[number]	k4	65
000	[number]	k4	000
svazků	svazek	k1gInPc2	svazek
<g/>
,	,	kIx,	,
19	[number]	k4	19
000	[number]	k4	000
spisů	spis	k1gInPc2	spis
<g/>
,	,	kIx,	,
map	mapa	k1gFnPc2	mapa
a	a	k8xC	a
topografických	topografický	k2eAgInPc2d1	topografický
nákresů	nákres	k1gInPc2	nákres
<g/>
.	.	kIx.	.
</s>
<s>
Zchátralý	zchátralý	k2eAgInSc1d1	zchátralý
starý	starý	k2eAgInSc1d1	starý
Montagu	Montag	k1gInSc6	Montag
House	house	k1gNnSc1	house
byl	být	k5eAaImAgMnS	být
roku	rok	k1gInSc2	rok
1845	[number]	k4	1845
zbořen	zbořit	k5eAaPmNgInS	zbořit
a	a	k8xC	a
nahrazen	nahradit	k5eAaPmNgInS	nahradit
neoklasicistní	neoklasicistní	k2eAgFnSc7d1	neoklasicistní
stavbou	stavba	k1gFnSc7	stavba
sira	sir	k1gMnSc2	sir
Roberta	Robert	k1gMnSc2	Robert
Smirka	Smirek	k1gMnSc2	Smirek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
ředitelem	ředitel	k1gMnSc7	ředitel
muzea	muzeum	k1gNnSc2	muzeum
Antonio	Antonio	k1gMnSc1	Antonio
Panizzi	Panizh	k1gMnPc1	Panizh
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rozsah	rozsah	k1gInSc1	rozsah
knihovny	knihovna	k1gFnSc2	knihovna
značně	značně	k6eAd1	značně
rozrostl	rozrůst	k5eAaPmAgMnS	rozrůst
a	a	k8xC	a
zlepšila	zlepšit	k5eAaPmAgFnS	zlepšit
se	se	k3xPyFc4	se
její	její	k3xOp3gFnSc2	její
organizace	organizace	k1gFnSc2	organizace
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
na	na	k7c4	na
úroveň	úroveň	k1gFnSc4	úroveň
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
nazývána	nazývat	k5eAaImNgFnS	nazývat
národní	národní	k2eAgFnSc7d1	národní
knihovnou	knihovna	k1gFnSc7	knihovna
<g/>
.	.	kIx.	.
</s>
<s>
Zastřešený	zastřešený	k2eAgInSc1d1	zastřešený
čtvercový	čtvercový	k2eAgInSc1d1	čtvercový
dvůr	dvůr	k1gInSc1	dvůr
uprostřed	uprostřed	k7c2	uprostřed
nové	nový	k2eAgFnSc2d1	nová
budovy	budova	k1gFnSc2	budova
muzea	muzeum	k1gNnSc2	muzeum
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
Panizziho	Panizzi	k1gMnSc4	Panizzi
požadavek	požadavek	k1gInSc4	požadavek
upraven	upravit	k5eAaPmNgMnS	upravit
na	na	k7c4	na
čítárnu	čítárna	k1gFnSc4	čítárna
<g/>
.	.	kIx.	.
</s>
<s>
Přírodopisné	přírodopisný	k2eAgFnPc1d1	přírodopisná
sbírky	sbírka	k1gFnPc1	sbírka
byly	být	k5eAaImAgFnP	být
součástí	součást	k1gFnSc7	součást
muzea	muzeum	k1gNnSc2	muzeum
do	do	k7c2	do
jejich	jejich	k3xOp3gNnSc2	jejich
vyčlenění	vyčlenění	k1gNnSc2	vyčlenění
do	do	k7c2	do
Natural	Natural	k?	Natural
History	Histor	k1gInPc4	Histor
Museum	museum	k1gNnSc4	museum
roku	rok	k1gInSc2	rok
1887	[number]	k4	1887
<g/>
.	.	kIx.	.
</s>
<s>
Etnografická	etnografický	k2eAgFnSc1d1	etnografická
kolekce	kolekce	k1gFnSc1	kolekce
byla	být	k5eAaImAgFnS	být
přechodně	přechodně	k6eAd1	přechodně
převedena	převést	k5eAaPmNgFnS	převést
do	do	k7c2	do
Museum	museum	k1gNnSc4	museum
of	of	k?	of
Mankind	Mankind	k1gInSc1	Mankind
na	na	k7c4	na
Piccadilly	Piccadilla	k1gFnPc4	Piccadilla
ale	ale	k8xC	ale
nyní	nyní	k6eAd1	nyní
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
Oddělení	oddělení	k1gNnSc2	oddělení
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
Oceánie	Oceánie	k1gFnSc2	Oceánie
a	a	k8xC	a
Ameriky	Amerika	k1gFnSc2	Amerika
Britského	britský	k2eAgNnSc2d1	Britské
muzea	muzeum	k1gNnSc2	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Speciální	speciální	k2eAgFnSc1d1	speciální
výstava	výstava	k1gFnSc1	výstava
Tutanchámonovy	Tutanchámonův	k2eAgFnSc2d1	Tutanchámonova
poklady	poklad	k1gInPc1	poklad
pořádaná	pořádaný	k2eAgFnSc1d1	pořádaná
muzeem	muzeum	k1gNnSc7	muzeum
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
byla	být	k5eAaImAgFnS	být
nejúspěšnější	úspěšný	k2eAgFnSc1d3	nejúspěšnější
výstavní	výstavní	k2eAgFnSc7d1	výstavní
akcí	akce	k1gFnSc7	akce
s	s	k7c7	s
počtem	počet	k1gInSc7	počet
1	[number]	k4	1
694	[number]	k4	694
000	[number]	k4	000
návštěvníků	návštěvník	k1gMnPc2	návštěvník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomtéž	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
byl	být	k5eAaImAgInS	být
vydán	vydán	k2eAgInSc1d1	vydán
zákon	zákon	k1gInSc1	zákon
ustavující	ustavující	k2eAgFnSc4d1	ustavující
Britskou	britský	k2eAgFnSc4d1	britská
knihovnu	knihovna	k1gFnSc4	knihovna
do	do	k7c2	do
níž	jenž	k3xRgFnSc2	jenž
byly	být	k5eAaImAgFnP	být
z	z	k7c2	z
Britského	britský	k2eAgNnSc2d1	Britské
muzea	muzeum	k1gNnSc2	muzeum
převedeny	převeden	k2eAgFnPc1d1	převedena
sbírky	sbírka	k1gFnPc1	sbírka
rukopisů	rukopis	k1gInPc2	rukopis
a	a	k8xC	a
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Fasáda	fasáda	k1gFnSc1	fasáda
budovy	budova	k1gFnSc2	budova
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Great	Great	k2eAgInSc4d1	Great
Russell	Russell	k1gInSc4	Russell
Street	Streeta	k1gFnPc2	Streeta
je	být	k5eAaImIp3nS	být
typickým	typický	k2eAgInSc7d1	typický
příkladem	příklad	k1gInSc7	příklad
stylu	styl	k1gInSc2	styl
Roberta	Robert	k1gMnSc4	Robert
Smirka	Smirek	k1gMnSc4	Smirek
<g/>
.	.	kIx.	.
</s>
<s>
Průčelí	průčelí	k1gNnSc1	průčelí
budovy	budova	k1gFnSc2	budova
<g/>
,	,	kIx,	,
inspirované	inspirovaný	k2eAgNnSc1d1	inspirované
řeckým	řecký	k2eAgInSc7d1	řecký
chrámem	chrám	k1gInSc7	chrám
zasvěceným	zasvěcený	k2eAgInSc7d1	zasvěcený
bohyni	bohyně	k1gFnSc4	bohyně
Athéně	Athéna	k1gFnSc3	Athéna
<g/>
,	,	kIx,	,
zdobí	zdobit	k5eAaImIp3nS	zdobit
44	[number]	k4	44
iónských	iónský	k2eAgInPc2d1	iónský
sloupů	sloup	k1gInPc2	sloup
<g/>
.	.	kIx.	.
</s>
<s>
Trojúhelníkový	trojúhelníkový	k2eAgInSc1d1	trojúhelníkový
štít	štít	k1gInSc1	štít
nad	nad	k7c7	nad
vchodem	vchod	k1gInSc7	vchod
je	být	k5eAaImIp3nS	být
zdoben	zdoben	k2eAgMnSc1d1	zdoben
plastikami	plastika	k1gFnPc7	plastika
Richarda	Richard	k1gMnSc2	Richard
Westmacotta	Westmacott	k1gMnSc2	Westmacott
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1852	[number]	k4	1852
<g/>
,	,	kIx,	,
zobrazujícími	zobrazující	k2eAgInPc7d1	zobrazující
růst	růst	k1gInSc4	růst
civilizace	civilizace	k1gFnSc2	civilizace
<g/>
.	.	kIx.	.
</s>
<s>
Duveen	Duveen	k2eAgInSc4d1	Duveen
Gallery	Galler	k1gInPc4	Galler
byla	být	k5eAaImAgFnS	být
navržena	navržen	k2eAgFnSc1d1	navržena
architektem	architekt	k1gMnSc7	architekt
Johnem	John	k1gMnSc7	John
Russellem	Russell	k1gMnSc7	Russell
Pope	pop	k1gInSc5	pop
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byla	být	k5eAaImAgFnS	být
poškozena	poškodit	k5eAaPmNgFnS	poškodit
při	při	k7c6	při
bombardování	bombardování	k1gNnSc6	bombardování
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
zpřístupněna	zpřístupněn	k2eAgFnSc1d1	zpřístupněna
po	po	k7c6	po
opravě	oprava	k1gFnSc6	oprava
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
dvorana	dvorana	k1gFnSc1	dvorana
Alžběty	Alžběta	k1gFnSc2	Alžběta
II	II	kA	II
<g/>
.	.	kIx.	.
je	být	k5eAaImIp3nS	být
zastřešená	zastřešený	k2eAgFnSc1d1	zastřešená
plocha	plocha	k1gFnSc1	plocha
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
budovy	budova	k1gFnSc2	budova
muzea	muzeum	k1gNnSc2	muzeum
<g/>
,	,	kIx,	,
navržená	navržený	k2eAgFnSc1d1	navržená
společností	společnost	k1gFnSc7	společnost
Foster	Foster	k1gMnSc1	Foster
and	and	k?	and
Partners	Partners	k1gInSc1	Partners
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2000	[number]	k4	2000
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgInSc7d3	veliký
zastřešeným	zastřešený	k2eAgInSc7d1	zastřešený
prostorem	prostor	k1gInSc7	prostor
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Střecha	střecha	k1gFnSc1	střecha
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
kovovou	kovový	k2eAgFnSc7d1	kovová
konstrukcí	konstrukce	k1gFnSc7	konstrukce
vyplněnou	vyplněná	k1gFnSc7	vyplněná
1	[number]	k4	1
656	[number]	k4	656
skleněných	skleněný	k2eAgInPc2d1	skleněný
panelů	panel	k1gInPc2	panel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
velké	velký	k2eAgFnSc2d1	velká
dvorany	dvorana	k1gFnSc2	dvorana
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
čítárna	čítárna	k1gFnSc1	čítárna
uvolněná	uvolněný	k2eAgFnSc1d1	uvolněná
Britskou	britský	k2eAgFnSc7d1	britská
knihovnou	knihovna	k1gFnSc7	knihovna
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
její	její	k3xOp3gFnSc4	její
funkci	funkce	k1gFnSc4	funkce
zabezpečuje	zabezpečovat	k5eAaImIp3nS	zabezpečovat
v	v	k7c6	v
St	St	kA	St
Pancras	Pancras	k1gInSc1	Pancras
<g/>
.	.	kIx.	.
</s>
<s>
Starověké	starověký	k2eAgInPc1d1	starověký
kamenné	kamenný	k2eAgInPc1d1	kamenný
monumenty	monument	k1gInPc1	monument
ze	z	k7c2	z
střední	střední	k2eAgFnSc2d1	střední
Asie	Asie	k1gFnSc2	Asie
perské	perský	k2eAgNnSc1d1	perské
kamenné	kamenný	k2eAgInPc1d1	kamenný
artefakty	artefakt	k1gInPc1	artefakt
z	z	k7c2	z
Aššurbanipalovy	Aššurbanipalův	k2eAgFnSc2d1	Aššurbanipalův
knihovny	knihovna	k1gFnSc2	knihovna
Antické	antický	k2eAgNnSc4d1	antické
Řecko	Řecko	k1gNnSc4	Řecko
Elginovy	Elginův	k2eAgInPc4d1	Elginův
mramory	mramor	k1gInPc4	mramor
–	–	k?	–
mramorové	mramorový	k2eAgFnPc1d1	mramorová
sochy	socha	k1gFnPc1	socha
a	a	k8xC	a
plastiky	plastika	k1gFnPc1	plastika
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
přivezl	přivézt	k5eAaPmAgMnS	přivézt
lord	lord	k1gMnSc1	lord
Elgin	Elgin	k1gMnSc1	Elgin
z	z	k7c2	z
aténského	aténský	k2eAgInSc2d1	aténský
Parthenónu	Parthenón	k1gInSc2	Parthenón
karyatida	karyatida	k1gFnSc1	karyatida
z	z	k7c2	z
Erechtheionu	Erechtheion	k1gInSc2	Erechtheion
v	v	k7c6	v
Athénách	Athéna	k1gFnPc6	Athéna
Portlandská	portlandský	k2eAgFnSc1d1	Portlandská
váza	váza	k1gFnSc1	váza
-	-	kIx~	-
antická	antický	k2eAgFnSc1d1	antická
helénská	helénský	k2eAgFnSc1d1	helénská
váza	váza	k1gFnSc1	váza
z	z	k7c2	z
černého	černý	k2eAgNnSc2d1	černé
skla	sklo	k1gNnSc2	sklo
s	s	k7c7	s
bílým	bílý	k2eAgInSc7d1	bílý
reliéfem	reliéf	k1gInSc7	reliéf
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g />
.	.	kIx.	.
</s>
<s>
<g/>
stol.	stol.	k?	stol.
<g/>
n.	n.	k?	n.
<g/>
l.	l.	k?	l.
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgNnSc4d3	nejstarší
evropské	evropský	k2eAgNnSc4d1	Evropské
sklo	sklo	k1gNnSc4	sklo
Starověký	starověký	k2eAgInSc1d1	starověký
Egypt	Egypt	k1gInSc1	Egypt
Rosettská	rosettský	k2eAgFnSc1d1	Rosettská
deska	deska	k1gFnSc1	deska
-	-	kIx~	-
několikajazyčná	několikajazyčný	k2eAgFnSc1d1	několikajazyčná
nápisová	nápisový	k2eAgFnSc1d1	nápisová
deska	deska	k1gFnSc1	deska
<g/>
,	,	kIx,	,
klíč	klíč	k1gInSc1	klíč
k	k	k7c3	k
rozluštění	rozluštění	k1gNnSc3	rozluštění
egyptských	egyptský	k2eAgInPc2d1	egyptský
hieroglyfů	hieroglyf	k1gInPc2	hieroglyf
egyptské	egyptský	k2eAgFnSc2d1	egyptská
mumie	mumie	k1gFnSc2	mumie
6	[number]	k4	6
těl	tělo	k1gNnPc2	tělo
z	z	k7c2	z
předdynastického	předdynastický	k2eAgNnSc2d1	předdynastický
období	období	k1gNnSc2	období
<g/>
,	,	kIx,	,
z	z	k7c2	z
Abydosu	Abydos	k1gInSc2	Abydos
a	a	k8xC	a
Geleionu	Geleion	k1gInSc2	Geleion
<g/>
,	,	kIx,	,
cca	cca	kA	cca
3400	[number]	k4	3400
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Mumie	mumie	k1gFnSc1	mumie
<g />
.	.	kIx.	.
</s>
<s>
královny	královna	k1gFnPc1	královna
Kleopatry	Kleopatra	k1gFnSc2	Kleopatra
kolosální	kolosální	k2eAgFnSc1d1	kolosální
socha	socha	k1gFnSc1	socha
egyptského	egyptský	k2eAgMnSc2d1	egyptský
faraóna	faraón	k1gMnSc2	faraón
Amenhotepa	Amenhotep	k1gMnSc2	Amenhotep
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
červený	červený	k2eAgInSc1d1	červený
granit	granit	k1gInSc1	granit
kolosální	kolosální	k2eAgFnSc1d1	kolosální
hlava	hlava	k1gFnSc1	hlava
sochy	socha	k1gFnSc2	socha
egyptského	egyptský	k2eAgMnSc4d1	egyptský
faraóna	faraón	k1gMnSc4	faraón
Amenhotepa	Amenhotep	k1gMnSc2	Amenhotep
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
černý	černý	k2eAgInSc4d1	černý
granit	granit	k1gInSc4	granit
kolosální	kolosální	k2eAgNnSc4d1	kolosální
poprsí	poprsí	k1gNnSc4	poprsí
egyptského	egyptský	k2eAgMnSc2d1	egyptský
faraóna	faraón	k1gMnSc2	faraón
Ramesse	Ramesse	k1gFnSc2	Ramesse
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
šedá	šedý	k2eAgFnSc1d1	šedá
žula	žula	k1gFnSc1	žula
Kamenná	kamenný	k2eAgFnSc1d1	kamenná
architektura	architektura	k1gFnSc1	architektura
Malé	Malé	k2eAgFnSc2d1	Malé
Asie	Asie	k1gFnSc2	Asie
Mausoleum	mausoleum	k1gNnSc1	mausoleum
z	z	k7c2	z
Halikarnassu	Halikarnass	k1gInSc2	Halikarnass
Fragmenty	fragment	k1gInPc1	fragment
z	z	k7c2	z
chrámu	chrám	k1gInSc2	chrám
bohyně	bohyně	k1gFnSc2	bohyně
<g />
.	.	kIx.	.
</s>
<s>
Artemis	Artemis	k1gFnSc1	Artemis
v	v	k7c6	v
Efesu	Efes	k1gInSc6	Efes
Středověká	středověký	k2eAgFnSc1d1	středověká
Evropa	Evropa	k1gFnSc1	Evropa
Anglosaský	anglosaský	k2eAgInSc4d1	anglosaský
zlatý	zlatý	k2eAgInSc4d1	zlatý
poklad	poklad	k1gInSc4	poklad
z	z	k7c2	z
pohřebiště	pohřebiště	k1gNnSc2	pohřebiště
v	v	k7c4	v
Sutton	Sutton	k1gInSc4	Sutton
Hoo	Hoo	k1gFnSc2	Hoo
Lewisova	Lewisův	k2eAgFnSc1d1	Lewisova
souprava	souprava	k1gFnSc1	souprava
raně	raně	k6eAd1	raně
středověkých	středověký	k2eAgFnPc2d1	středověká
šachových	šachový	k2eAgFnPc2d1	šachová
figurek	figurka	k1gFnPc2	figurka
z	z	k7c2	z
mrožího	mroží	k2eAgInSc2d1	mroží
klu	kel	k1gInSc2	kel
a	a	k8xC	a
kosti	kost	k1gFnSc2	kost
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
Wiltonský	Wiltonský	k2eAgInSc1d1	Wiltonský
diptych	diptych	k1gInSc1	diptych
-	-	kIx~	-
dvojdílný	dvojdílný	k2eAgInSc1d1	dvojdílný
malovaný	malovaný	k2eAgInSc1d1	malovaný
oltář	oltář	k1gInSc1	oltář
krále	král	k1gMnSc2	král
Richarda	Richard	k1gMnSc2	Richard
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
manželky	manželka	k1gFnPc1	manželka
Anny	Anna	k1gFnSc2	Anna
České	český	k2eAgFnPc1d1	Česká
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
1380	[number]	k4	1380
<g />
.	.	kIx.	.
</s>
<s>
Zlatý	zlatý	k2eAgInSc1d1	zlatý
královský	královský	k2eAgInSc1d1	královský
pohár	pohár	k1gInSc1	pohár
s	s	k7c7	s
gotickými	gotický	k2eAgFnPc7d1	gotická
scénami	scéna	k1gFnPc7	scéna
v	v	k7c6	v
pestrobarevném	pestrobarevný	k2eAgInSc6d1	pestrobarevný
emailu	email	k1gInSc6	email
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
1370	[number]	k4	1370
Renesance	renesance	k1gFnSc2	renesance
obrazy	obraz	k1gInPc1	obraz
Tiziana	Tizian	k1gMnSc2	Tizian
<g/>
,	,	kIx,	,
kresby	kresba	k1gFnPc1	kresba
Albrechta	Albrecht	k1gMnSc2	Albrecht
Dürera	Dürer	k1gMnSc2	Dürer
<g/>
,	,	kIx,	,
Leonarda	Leonardo	k1gMnSc2	Leonardo
da	da	k?	da
Vinci	Vinca	k1gMnSc2	Vinca
<g/>
,	,	kIx,	,
Michelangela	Michelangel	k1gMnSc2	Michelangel
<g/>
,	,	kIx,	,
Raffaela	Raffael	k1gMnSc2	Raffael
Santi	Sanť	k1gFnSc2	Sanť
Mimoevropské	mimoevropský	k2eAgNnSc1d1	mimoevropské
umění	umění	k1gNnSc1	umění
bronzové	bronzový	k2eAgFnSc2d1	bronzová
sochy	socha	k1gFnSc2	socha
z	z	k7c2	z
královského	královský	k2eAgInSc2d1	královský
paláce	palác	k1gInSc2	palác
v	v	k7c6	v
Beninu	Benin	k1gInSc6	Benin
socha	socha	k1gFnSc1	socha
Moai	Moai	k1gNnSc2	Moai
z	z	k7c2	z
Velikonočních	velikonoční	k2eAgInPc2d1	velikonoční
ostrovů	ostrov	k1gInPc2	ostrov
Zlatý	zlatý	k2eAgInSc1d1	zlatý
ceremoniální	ceremoniální	k2eAgInSc1d1	ceremoniální
límec	límec	k1gInSc1	límec
z	z	k7c2	z
Doby	doba	k1gFnSc2	doba
bronzové	bronzový	k2eAgInPc4d1	bronzový
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Britské	britský	k2eAgFnSc2d1	britská
muzeum	muzeum	k1gNnSc4	muzeum
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Galerie	galerie	k1gFnSc1	galerie
Britské	britský	k2eAgNnSc4d1	Britské
muzeum	muzeum	k1gNnSc4	muzeum
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
