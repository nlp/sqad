<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Íránu	Írán	k1gInSc2	Írán
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgInPc4	tři
vodorovné	vodorovný	k2eAgInPc4d1	vodorovný
pruhy	pruh	k1gInPc4	pruh
v	v	k7c6	v
tradičních	tradiční	k2eAgFnPc6d1	tradiční
íránských	íránský	k2eAgFnPc6d1	íránská
barvách	barva	k1gFnPc6	barva
<g/>
,	,	kIx,	,
zelené	zelený	k2eAgFnPc1d1	zelená
<g/>
,	,	kIx,	,
bílé	bílý	k2eAgFnPc1d1	bílá
a	a	k8xC	a
červené	červená	k1gFnPc1	červená
<g/>
,	,	kIx,	,
bílý	bílý	k2eAgInSc1d1	bílý
pruh	pruh	k1gInSc1	pruh
je	být	k5eAaImIp3nS	být
však	však	k9	však
o	o	k7c4	o
něco	něco	k3yInSc4	něco
širší	široký	k2eAgFnSc1d2	širší
než	než	k8xS	než
dva	dva	k4xCgInPc1	dva
ostatní	ostatní	k2eAgInPc1d1	ostatní
a	a	k8xC	a
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
do	do	k7c2	do
nich	on	k3xPp3gMnPc2	on
úzkými	úzký	k2eAgInPc7d1	úzký
obdélníkovými	obdélníkový	k2eAgInPc7d1	obdélníkový
zuby	zub	k1gInPc7	zub
<g/>
.	.	kIx.	.
</s>
