<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Íránu	Írán	k1gInSc2	Írán
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgInPc4	tři
vodorovné	vodorovný	k2eAgInPc4d1	vodorovný
pruhy	pruh	k1gInPc4	pruh
v	v	k7c6	v
tradičních	tradiční	k2eAgFnPc6d1	tradiční
íránských	íránský	k2eAgFnPc6d1	íránská
barvách	barva	k1gFnPc6	barva
<g/>
,	,	kIx,	,
zelené	zelený	k2eAgFnPc1d1	zelená
<g/>
,	,	kIx,	,
bílé	bílý	k2eAgFnPc1d1	bílá
a	a	k8xC	a
červené	červená	k1gFnPc1	červená
<g/>
,	,	kIx,	,
bílý	bílý	k2eAgInSc1d1	bílý
pruh	pruh	k1gInSc1	pruh
je	být	k5eAaImIp3nS	být
však	však	k9	však
o	o	k7c4	o
něco	něco	k3yInSc4	něco
širší	široký	k2eAgFnSc1d2	širší
než	než	k8xS	než
dva	dva	k4xCgInPc1	dva
ostatní	ostatní	k2eAgInPc1d1	ostatní
a	a	k8xC	a
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
do	do	k7c2	do
nich	on	k3xPp3gMnPc2	on
úzkými	úzký	k2eAgInPc7d1	úzký
obdélníkovými	obdélníkový	k2eAgInPc7d1	obdélníkový
zuby	zub	k1gInPc7	zub
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dolním	dolní	k2eAgInSc6d1	dolní
okraji	okraj	k1gInSc6	okraj
pruhu	pruh	k1gInSc2	pruh
červeného	červený	k2eAgInSc2d1	červený
se	s	k7c7	s
vždy	vždy	k6eAd1	vždy
jedenáctkrát	jedenáctkrát	k6eAd1	jedenáctkrát
opakuje	opakovat	k5eAaImIp3nS	opakovat
stylizovaným	stylizovaný	k2eAgNnSc7d1	stylizované
arabským	arabský	k2eAgNnSc7d1	arabské
písmem	písmo	k1gNnSc7	písmo
bíle	bíle	k6eAd1	bíle
psané	psaný	k2eAgNnSc4d1	psané
heslo	heslo	k1gNnSc4	heslo
Bůh	bůh	k1gMnSc1	bůh
je	být	k5eAaImIp3nS	být
veliký	veliký	k2eAgInSc4d1	veliký
(	(	kIx(	(
<g/>
Alláhu	Alláh	k1gMnSc3	Alláh
akbar	akbara	k1gFnPc2	akbara
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k7c2	uprostřed
bílého	bílý	k2eAgInSc2d1	bílý
pruhu	pruh	k1gInSc2	pruh
je	být	k5eAaImIp3nS	být
umístěn	umístit	k5eAaPmNgInS	umístit
červený	červený	k2eAgInSc1d1	červený
íránský	íránský	k2eAgInSc1d1	íránský
znak	znak	k1gInSc1	znak
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
tvoří	tvořit	k5eAaImIp3nS	tvořit
čtyři	čtyři	k4xCgInPc4	čtyři
malé	malý	k2eAgInPc4d1	malý
půlměsíce	půlměsíc	k1gInPc4	půlměsíc
<g/>
,	,	kIx,	,
obklopující	obklopující	k2eAgInSc4d1	obklopující
svisle	svisle	k6eAd1	svisle
ležící	ležící	k2eAgInSc4d1	ležící
čepel	čepel	k1gInSc4	čepel
meče	meč	k1gInSc2	meč
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
kterou	který	k3yRgFnSc7	který
je	být	k5eAaImIp3nS	být
umístěn	umístěn	k2eAgInSc1d1	umístěn
znak	znak	k1gInSc1	znak
arabského	arabský	k2eAgNnSc2d1	arabské
písma	písmo	k1gNnSc2	písmo
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
at-tašdíd	atašdíd	k1gInSc1	at-tašdíd
<g/>
.	.	kIx.	.
<g/>
Kresba	kresba	k1gFnSc1	kresba
znaku	znak	k1gInSc2	znak
je	být	k5eAaImIp3nS	být
stylizace	stylizace	k1gFnSc1	stylizace
Alláhova	Alláhův	k2eAgNnSc2d1	Alláhovo
jména	jméno	k1gNnSc2	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Pět	pět	k4xCc1	pět
hlavních	hlavní	k2eAgInPc2d1	hlavní
prvků	prvek	k1gInPc2	prvek
znaku	znak	k1gInSc2	znak
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
pět	pět	k4xCc1	pět
základních	základní	k2eAgInPc2d1	základní
pilířů	pilíř	k1gInPc2	pilíř
islámu	islám	k1gInSc2	islám
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
svislá	svislý	k2eAgFnSc1d1	svislá
poloha	poloha	k1gFnSc1	poloha
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
cestu	cesta	k1gFnSc4	cesta
k	k	k7c3	k
jedinému	jediný	k2eAgMnSc3d1	jediný
bohu	bůh	k1gMnSc3	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Čtyři	čtyři	k4xCgInPc1	čtyři
půlměsíce	půlměsíc	k1gInPc1	půlměsíc
představují	představovat	k5eAaImIp3nP	představovat
fázi	fáze	k1gFnSc4	fáze
jeho	on	k3xPp3gNnSc2	on
narůstání	narůstání	k1gNnSc2	narůstání
<g/>
,	,	kIx,	,
tak	tak	k9	tak
jak	jak	k8xC	jak
roste	růst	k5eAaImIp3nS	růst
islámská	islámský	k2eAgFnSc1d1	islámská
víra	víra	k1gFnSc1	víra
<g/>
.	.	kIx.	.
</s>
<s>
Meč	meč	k1gInSc1	meč
znamená	znamenat	k5eAaImIp3nS	znamenat
sílu	síla	k1gFnSc4	síla
a	a	k8xC	a
statečnost	statečnost	k1gFnSc4	statečnost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
navíc	navíc	k6eAd1	navíc
zesílena	zesílit	k5eAaPmNgFnS	zesílit
znakem	znak	k1gInSc7	znak
at-tašdíd	atašdíd	k6eAd1	at-tašdíd
<g/>
.	.	kIx.	.
</s>
<s>
Podoba	podoba	k1gFnSc1	podoba
znaku	znak	k1gInSc2	znak
zároveň	zároveň	k6eAd1	zároveň
evokuje	evokovat	k5eAaBmIp3nS	evokovat
představu	představa	k1gFnSc4	představa
zemského	zemský	k2eAgInSc2d1	zemský
glóbu	glóbus	k1gInSc2	glóbus
<g/>
.	.	kIx.	.
</s>
<s>
Červená	červený	k2eAgFnSc1d1	červená
barva	barva	k1gFnSc1	barva
vlajky	vlajka	k1gFnSc2	vlajka
znamená	znamenat	k5eAaImIp3nS	znamenat
revoluci	revoluce	k1gFnSc4	revoluce
<g/>
,	,	kIx,	,
bílá	bílý	k2eAgFnSc1d1	bílá
mír	mír	k1gInSc1	mír
<g/>
,	,	kIx,	,
zelená	zelenat	k5eAaImIp3nS	zelenat
islám	islám	k1gInSc1	islám
<g/>
.	.	kIx.	.
</s>
<s>
Dvaadvacetkrát	dvaadvacetkrát	k6eAd1	dvaadvacetkrát
se	se	k3xPyFc4	se
opakující	opakující	k2eAgInSc4d1	opakující
text	text	k1gInSc4	text
na	na	k7c6	na
okrajích	okraj	k1gInPc6	okraj
barevných	barevný	k2eAgInPc2d1	barevný
pruhů	pruh	k1gInPc2	pruh
připomíná	připomínat	k5eAaImIp3nS	připomínat
22	[number]	k4	22
<g/>
.	.	kIx.	.
bahmán	bahmán	k2eAgInSc1d1	bahmán
revolučního	revoluční	k2eAgInSc2d1	revoluční
roku	rok	k1gInSc2	rok
1357	[number]	k4	1357
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
únor	únor	k1gInSc1	únor
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
oslavován	oslavovat	k5eAaImNgInS	oslavovat
jako	jako	k9	jako
den	den	k1gInSc4	den
vítězství	vítězství	k1gNnSc2	vítězství
íránské	íránský	k2eAgFnSc2d1	íránská
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
I	i	k9	i
když	když	k8xS	když
republika	republika	k1gFnSc1	republika
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Íránu	Írán	k1gInSc6	Írán
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
již	již	k6eAd1	již
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1979	[number]	k4	1979
<g/>
,	,	kIx,	,
novou	nový	k2eAgFnSc4d1	nová
íránskou	íránský	k2eAgFnSc4d1	íránská
vlajku	vlajka	k1gFnSc4	vlajka
schválil	schválit	k5eAaPmAgInS	schválit
parlament	parlament	k1gInSc1	parlament
až	až	k6eAd1	až
29	[number]	k4	29
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
byla	být	k5eAaImAgFnS	být
používána	používán	k2eAgFnSc1d1	používána
íránská	íránský	k2eAgFnSc1d1	íránská
trikolóra	trikolóra	k1gFnSc1	trikolóra
bez	bez	k7c2	bez
císařského	císařský	k2eAgMnSc2d1	císařský
lva	lev	k1gMnSc2	lev
nebo	nebo	k8xC	nebo
se	s	k7c7	s
lvem	lev	k1gMnSc7	lev
bez	bez	k7c2	bez
císařské	císařský	k2eAgFnSc2d1	císařská
koruny	koruna	k1gFnSc2	koruna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
Íránu	Írán	k1gInSc2	Írán
</s>
</p>
<p>
<s>
Íránská	íránský	k2eAgFnSc1d1	íránská
hymna	hymna	k1gFnSc1	hymna
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Íránu	Írán	k1gInSc2	Írán
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Íránská	íránský	k2eAgFnSc1d1	íránská
vlajka	vlajka	k1gFnSc1	vlajka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
