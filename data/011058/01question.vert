<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
chráněné	chráněný	k2eAgNnSc1d1	chráněné
území	území	k1gNnSc1	území
v	v	k7c6	v
Ekvádoru	Ekvádor	k1gInSc6	Ekvádor
<g/>
,	,	kIx,	,
nesoucí	nesoucí	k2eAgFnSc7d1	nesoucí
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
status	status	k1gInSc1	status
národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
<g/>
?	?	kIx.	?
</s>
