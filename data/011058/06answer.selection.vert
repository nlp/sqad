<s>
Národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
Llanganates	Llanganatesa	k1gFnPc2	Llanganatesa
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
Parque	Parque	k1gInSc1	Parque
nacional	nacionat	k5eAaImAgInS	nacionat
Llanganates	Llanganates	k1gInSc4	Llanganates
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
chráněné	chráněný	k2eAgNnSc4d1	chráněné
území	území	k1gNnSc4	území
v	v	k7c6	v
Ekvádoru	Ekvádor	k1gInSc6	Ekvádor
<g/>
,	,	kIx,	,
nesoucí	nesoucí	k2eAgFnSc7d1	nesoucí
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
status	status	k1gInSc1	status
národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
<g/>
.	.	kIx.	.
</s>
