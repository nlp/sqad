<p>
<s>
Národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
Llanganates	Llanganatesa	k1gFnPc2	Llanganatesa
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
Parque	Parque	k1gInSc1	Parque
nacional	nacionat	k5eAaPmAgInS	nacionat
Llanganates	Llanganates	k1gInSc4	Llanganates
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
chráněné	chráněný	k2eAgNnSc4d1	chráněné
území	území	k1gNnSc4	území
v	v	k7c6	v
Ekvádoru	Ekvádor	k1gInSc6	Ekvádor
<g/>
,	,	kIx,	,
nesoucí	nesoucí	k2eAgFnSc7d1	nesoucí
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
status	status	k1gInSc1	status
národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
2199	[number]	k4	2199
km2	km2	k4	km2
a	a	k8xC	a
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
na	na	k7c6	na
území	území	k1gNnSc6	území
provincií	provincie	k1gFnPc2	provincie
Cotopaxi	Cotopaxe	k1gFnSc4	Cotopaxe
<g/>
,	,	kIx,	,
Tungurahua	Tungurahua	k1gFnSc1	Tungurahua
<g/>
,	,	kIx,	,
Napo	napa	k1gFnSc5	napa
a	a	k8xC	a
Pastaza	Pastaz	k1gMnSc4	Pastaz
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgFnSc1d1	západní
část	část	k1gFnSc1	část
národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
andské	andský	k2eAgFnSc6d1	andská
krajině	krajina	k1gFnSc6	krajina
nazývané	nazývaný	k2eAgInPc1d1	nazývaný
páramo	páramo	k6eAd1	páramo
<g/>
,	,	kIx,	,
dosahující	dosahující	k2eAgFnPc1d1	dosahující
nadmořské	nadmořský	k2eAgFnPc1d1	nadmořská
výšky	výška	k1gFnPc1	výška
až	až	k9	až
4571	[number]	k4	4571
m	m	kA	m
<g/>
,	,	kIx,	,
východní	východní	k2eAgFnSc1d1	východní
část	část	k1gFnSc1	část
se	se	k3xPyFc4	se
svažuje	svažovat	k5eAaImIp3nS	svažovat
do	do	k7c2	do
údolí	údolí	k1gNnSc2	údolí
Amazonky	Amazonka	k1gFnSc2	Amazonka
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
porostlá	porostlý	k2eAgNnPc4d1	porostlé
hustým	hustý	k2eAgInSc7d1	hustý
tropickým	tropický	k2eAgInSc7d1	tropický
lesem	les	k1gInSc7	les
<g/>
.	.	kIx.	.
</s>
<s>
Typickými	typický	k2eAgMnPc7d1	typický
živočichy	živočich	k1gMnPc7	živočich
jsou	být	k5eAaImIp3nP	být
lama	lama	k1gFnSc1	lama
alpaka	alpaka	k1gFnSc1	alpaka
<g/>
,	,	kIx,	,
vikuňa	vikuňa	k1gFnSc1	vikuňa
<g/>
,	,	kIx,	,
puma	puma	k1gFnSc1	puma
americká	americký	k2eAgFnSc1d1	americká
<g/>
,	,	kIx,	,
medvěd	medvěd	k1gMnSc1	medvěd
brýlatý	brýlatý	k2eAgMnSc1d1	brýlatý
<g/>
,	,	kIx,	,
tapír	tapír	k1gMnSc1	tapír
horský	horský	k2eAgMnSc1d1	horský
<g/>
,	,	kIx,	,
aguti	agut	k1gMnPc1	agut
tmavý	tmavý	k2eAgMnSc1d1	tmavý
<g/>
,	,	kIx,	,
kondor	kondor	k1gMnSc1	kondor
andský	andský	k2eAgMnSc1d1	andský
<g/>
,	,	kIx,	,
tangara	tangara	k1gFnSc1	tangara
Wetmoreova	Wetmoreův	k2eAgFnSc1d1	Wetmoreův
a	a	k8xC	a
skalňák	skalňák	k1gMnSc1	skalňák
andský	andský	k2eAgMnSc1d1	andský
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
parku	park	k1gInSc2	park
znamená	znamenat	k5eAaImIp3nS	znamenat
v	v	k7c6	v
kečuánštině	kečuánština	k1gFnSc6	kečuánština
"	"	kIx"	"
<g/>
krásná	krásný	k2eAgFnSc1d1	krásná
hora	hora	k1gFnSc1	hora
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
Llanganates	Llanganatesa	k1gFnPc2	Llanganatesa
je	být	k5eAaImIp3nS	být
známá	známá	k1gFnSc1	známá
díky	díky	k7c3	díky
legendám	legenda	k1gFnPc3	legenda
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
nichž	jenž	k3xRgInPc2	jenž
ve	v	k7c6	v
zdejších	zdejší	k2eAgFnPc6d1	zdejší
nepřístupných	přístupný	k2eNgFnPc6d1	nepřístupná
horách	hora	k1gFnPc6	hora
ukryli	ukrýt	k5eAaPmAgMnP	ukrýt
Inkové	Ink	k1gMnPc1	Ink
před	před	k7c7	před
španělskými	španělský	k2eAgInPc7d1	španělský
conquistadory	conquistador	k1gMnPc7	conquistador
svůj	svůj	k3xOyFgInSc4	svůj
zlatý	zlatý	k2eAgInSc4d1	zlatý
poklad	poklad	k1gInSc4	poklad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
