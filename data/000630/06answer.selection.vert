<s>
Fridrich	Fridrich	k1gMnSc1	Fridrich
I.	I.	kA	I.
Barbarossa	Barbarossa	k1gMnSc1	Barbarossa
(	(	kIx(	(
<g/>
Rudovous	Rudovous	k1gMnSc1	Rudovous
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
asi	asi	k9	asi
prosinec	prosinec	k1gInSc1	prosinec
1122	[number]	k4	1122
-	-	kIx~	-
10	[number]	k4	10
<g/>
.	.	kIx.	.
červen	červen	k1gInSc1	červen
1190	[number]	k4	1190
<g/>
,	,	kIx,	,
řeka	řeka	k1gFnSc1	řeka
Salef	Salef	k1gInSc1	Salef
v	v	k7c6	v
dnešním	dnešní	k2eAgNnSc6d1	dnešní
Turecku	Turecko	k1gNnSc6	Turecko
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
římský	římský	k2eAgMnSc1d1	římský
král	král	k1gMnSc1	král
(	(	kIx(	(
<g/>
1152	[number]	k4	1152
<g/>
-	-	kIx~	-
<g/>
1190	[number]	k4	1190
<g/>
)	)	kIx)	)
a	a	k8xC	a
císař	císař	k1gMnSc1	císař
(	(	kIx(	(
<g/>
1155	[number]	k4	1155
<g/>
-	-	kIx~	-
<g/>
1190	[number]	k4	1190
<g/>
)	)	kIx)	)
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Štaufů	Štauf	k1gMnPc2	Štauf
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
Fridricha	Fridrich	k1gMnSc2	Fridrich
II	II	kA	II
<g/>
.	.	kIx.	.
Švábského	švábský	k2eAgNnSc2d1	švábské
a	a	k8xC	a
Judity	Judita	k1gFnSc2	Judita
Welfské	Welfský	k2eAgFnSc2d1	Welfský
<g/>
,	,	kIx,	,
dcery	dcera	k1gFnSc2	dcera
bavorského	bavorský	k2eAgMnSc2d1	bavorský
vévody	vévoda	k1gMnSc2	vévoda
Jindřicha	Jindřich	k1gMnSc2	Jindřich
Černého	Černý	k1gMnSc2	Černý
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Welfů	Welf	k1gInPc2	Welf
<g/>
.	.	kIx.	.
</s>
