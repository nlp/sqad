<s>
Fridrich	Fridrich	k1gMnSc1	Fridrich
I.	I.	kA	I.
Barbarossa	Barbarossa	k1gMnSc1	Barbarossa
(	(	kIx(	(
<g/>
Rudovous	Rudovous	k1gMnSc1	Rudovous
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
asi	asi	k9	asi
prosinec	prosinec	k1gInSc1	prosinec
1122	[number]	k4	1122
-	-	kIx~	-
10	[number]	k4	10
<g/>
.	.	kIx.	.
červen	červen	k1gInSc1	červen
1190	[number]	k4	1190
<g/>
,	,	kIx,	,
řeka	řeka	k1gFnSc1	řeka
Salef	Salef	k1gInSc1	Salef
v	v	k7c6	v
dnešním	dnešní	k2eAgNnSc6d1	dnešní
Turecku	Turecko	k1gNnSc6	Turecko
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
římský	římský	k2eAgMnSc1d1	římský
král	král	k1gMnSc1	král
(	(	kIx(	(
<g/>
1152	[number]	k4	1152
<g/>
-	-	kIx~	-
<g/>
1190	[number]	k4	1190
<g/>
)	)	kIx)	)
a	a	k8xC	a
císař	císař	k1gMnSc1	císař
(	(	kIx(	(
<g/>
1155	[number]	k4	1155
<g/>
-	-	kIx~	-
<g/>
1190	[number]	k4	1190
<g/>
)	)	kIx)	)
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Štaufů	Štauf	k1gMnPc2	Štauf
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
Fridricha	Fridrich	k1gMnSc2	Fridrich
II	II	kA	II
<g/>
.	.	kIx.	.
Švábského	švábský	k2eAgNnSc2d1	švábské
a	a	k8xC	a
Judity	Judita	k1gFnSc2	Judita
Welfské	Welfský	k2eAgFnSc2d1	Welfský
<g/>
,	,	kIx,	,
dcery	dcera	k1gFnSc2	dcera
bavorského	bavorský	k2eAgMnSc2d1	bavorský
vévody	vévoda	k1gMnSc2	vévoda
Jindřicha	Jindřich	k1gMnSc2	Jindřich
Černého	Černý	k1gMnSc2	Černý
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Welfů	Welf	k1gInPc2	Welf
<g/>
.	.	kIx.	.
</s>
<s>
Fridrich	Fridrich	k1gMnSc1	Fridrich
byl	být	k5eAaImAgMnS	být
významný	významný	k2eAgMnSc1d1	významný
panovník	panovník	k1gMnSc1	panovník
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
většinu	většina	k1gFnSc4	většina
života	život	k1gInSc2	život
prožil	prožít	k5eAaPmAgInS	prožít
v	v	k7c6	v
konfliktu	konflikt	k1gInSc6	konflikt
s	s	k7c7	s
městy	město	k1gNnPc7	město
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Podmanit	podmanit	k5eAaPmF	podmanit
si	se	k3xPyFc3	se
zcela	zcela	k6eAd1	zcela
Itálii	Itálie	k1gFnSc4	Itálie
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
ale	ale	k9	ale
nikdy	nikdy	k6eAd1	nikdy
nepodařilo	podařit	k5eNaPmAgNnS	podařit
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
řešil	řešit	k5eAaImAgMnS	řešit
problémy	problém	k1gInPc7	problém
ve	v	k7c6	v
vyjasňování	vyjasňování	k1gNnSc6	vyjasňování
pozic	pozice	k1gFnPc2	pozice
císařství	císařství	k1gNnSc2	císařství
ke	k	k7c3	k
kléru	klér	k1gInSc3	klér
a	a	k8xC	a
papežství	papežství	k1gNnSc6	papežství
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
prožívalo	prožívat	k5eAaImAgNnS	prožívat
schizma	schizma	k1gNnSc4	schizma
(	(	kIx(	(
<g/>
1159	[number]	k4	1159
<g/>
-	-	kIx~	-
<g/>
1177	[number]	k4	1177
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
řešení	řešení	k1gNnSc6	řešení
vztahů	vztah	k1gInPc2	vztah
mezi	mezi	k7c7	mezi
císařem	císař	k1gMnSc7	císař
a	a	k8xC	a
šlechtou	šlechta	k1gFnSc7	šlechta
(	(	kIx(	(
<g/>
např.	např.	kA	např.
sesazení	sesazení	k1gNnSc1	sesazení
bavorského	bavorský	k2eAgMnSc2d1	bavorský
a	a	k8xC	a
saského	saský	k2eAgMnSc2d1	saský
vévody	vévoda	k1gMnSc2	vévoda
Jindřicha	Jindřich	k1gMnSc2	Jindřich
Lva	Lev	k1gMnSc2	Lev
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1180	[number]	k4	1180
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
získali	získat	k5eAaPmAgMnP	získat
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
zkušení	zkušený	k2eAgMnPc1d1	zkušený
ministeriálové	ministeriál	k1gMnPc1	ministeriál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
otázkách	otázka	k1gFnPc6	otázka
pojetí	pojetí	k1gNnSc1	pojetí
vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
obrátil	obrátit	k5eAaPmAgMnS	obrátit
k	k	k7c3	k
tradicím	tradice	k1gFnPc3	tradice
antické	antický	k2eAgFnSc2d1	antická
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
využívat	využívat	k5eAaImF	využívat
římské	římský	k2eAgNnSc4d1	římské
právo	právo	k1gNnSc4	právo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
rozvíjelo	rozvíjet	k5eAaImAgNnS	rozvíjet
na	na	k7c6	na
území	území	k1gNnSc6	území
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
a	a	k8xC	a
obnovil	obnovit	k5eAaPmAgInS	obnovit
jeho	jeho	k3xOp3gNnSc4	jeho
studium	studium	k1gNnSc4	studium
<g/>
.	.	kIx.	.
1147	[number]	k4	1147
-	-	kIx~	-
sňatek	sňatek	k1gInSc1	sňatek
s	s	k7c7	s
Adélou	Adéla	k1gFnSc7	Adéla
z	z	k7c2	z
Vohburgu	Vohburg	k1gInSc2	Vohburg
4	[number]	k4	4
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1152	[number]	k4	1152
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
<g />
.	.	kIx.	.
</s>
<s>
římským	římský	k2eAgMnSc7d1	římský
králem	král	k1gMnSc7	král
březen	březno	k1gNnPc2	březno
1153	[number]	k4	1153
-	-	kIx~	-
rozvod	rozvod	k1gInSc1	rozvod
s	s	k7c7	s
Adélou	Adéla	k1gFnSc7	Adéla
z	z	k7c2	z
Vohburgu	Vohburg	k1gInSc2	Vohburg
17	[number]	k4	17
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1156	[number]	k4	1156
-	-	kIx~	-
ve	v	k7c6	v
Würzburgu	Würzburg	k1gInSc6	Würzburg
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Beatricí	Beatrice	k1gFnSc7	Beatrice
Burgundskou	burgundský	k2eAgFnSc7d1	burgundská
1158	[number]	k4	1158
<g/>
-	-	kIx~	-
<g/>
1168	[number]	k4	1168
boj	boj	k1gInSc1	boj
s	s	k7c7	s
Milánem	Milán	k1gInSc7	Milán
1159	[number]	k4	1159
boj	boj	k1gInSc4	boj
proti	proti	k7c3	proti
Cremě	Crema	k1gFnSc3	Crema
1162	[number]	k4	1162
Milán	Milán	k1gInSc1	Milán
se	se	k3xPyFc4	se
vzdal	vzdát	k5eAaPmAgInS	vzdát
císaři	císař	k1gMnSc3	císař
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
vyklizen	vyklizen	k2eAgMnSc1d1	vyklizen
a	a	k8xC	a
zničen	zničen	k2eAgMnSc1d1	zničen
1165	[number]	k4	1165
za	za	k7c2	za
podpory	podpora	k1gFnSc2	podpora
vzdoropapeže	vzdoropapež	k1gMnSc2	vzdoropapež
<g />
.	.	kIx.	.
</s>
<s>
Paschala	Paschat	k5eAaImAgFnS	Paschat
III	III	kA	III
<g/>
.	.	kIx.	.
nechal	nechat	k5eAaPmAgMnS	nechat
kanonizovat	kanonizovat	k5eAaBmF	kanonizovat
císaře	císař	k1gMnSc4	císař
Karla	Karel	k1gMnSc4	Karel
Velikého	veliký	k2eAgNnSc2d1	veliké
1167	[number]	k4	1167
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
obsadil	obsadit	k5eAaPmAgMnS	obsadit
Fridrich	Fridrich	k1gMnSc1	Fridrich
I.	I.	kA	I.
Řím	Řím	k1gInSc1	Řím
a	a	k8xC	a
vzdoropapež	vzdoropapež	k1gMnSc1	vzdoropapež
Paschala	Paschala	k1gFnSc2	Paschala
III	III	kA	III
<g/>
.	.	kIx.	.
ho	on	k3xPp3gNnSc4	on
korunoval	korunovat	k5eAaBmAgMnS	korunovat
císařem	císař	k1gMnSc7	císař
1167	[number]	k4	1167
císař	císař	k1gMnSc1	císař
byl	být	k5eAaImAgMnS	být
nucen	nutit	k5eAaImNgMnS	nutit
z	z	k7c2	z
Itálie	Itálie	k1gFnSc2	Itálie
uprchnout	uprchnout	k5eAaPmF	uprchnout
1176	[number]	k4	1176
poražen	porazit	k5eAaPmNgMnS	porazit
lombardskou	lombardský	k2eAgFnSc7d1	Lombardská
ligou	liga	k1gFnSc7	liga
severoitalských	severoitalský	k2eAgNnPc2d1	severoitalské
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
spojenou	spojený	k2eAgFnSc4d1	spojená
s	s	k7c7	s
papežem	papež	k1gMnSc7	papež
<g/>
,	,	kIx,	,
u	u	k7c2	u
Legnana	Legnan	k1gMnSc2	Legnan
1177	[number]	k4	1177
ponižující	ponižující	k2eAgInSc1d1	ponižující
smíření	smíření	k1gNnSc3	smíření
<g />
.	.	kIx.	.
</s>
<s>
s	s	k7c7	s
papežem	papež	k1gMnSc7	papež
Alexandrem	Alexandr	k1gMnSc7	Alexandr
III	III	kA	III
<g/>
.	.	kIx.	.
v	v	k7c6	v
Benátkách	Benátky	k1gFnPc6	Benátky
1177	[number]	k4	1177
<g/>
-	-	kIx~	-
<g/>
1180	[number]	k4	1180
spor	spor	k1gInSc1	spor
s	s	k7c7	s
vévodou	vévoda	k1gMnSc7	vévoda
Jindřichem	Jindřich	k1gMnSc7	Jindřich
Lvem	Lev	k1gMnSc7	Lev
1184	[number]	k4	1184
uzavření	uzavření	k1gNnSc6	uzavření
míru	míra	k1gFnSc4	míra
se	s	k7c7	s
severoitalskými	severoitalský	k2eAgNnPc7d1	severoitalské
městy	město	k1gNnPc7	město
v	v	k7c6	v
Kostnici	Kostnice	k1gFnSc6	Kostnice
1184	[number]	k4	1184
mohutná	mohutný	k2eAgFnSc1d1	mohutná
slavnost	slavnost	k1gFnSc1	slavnost
o	o	k7c6	o
Svatodušních	svatodušní	k2eAgInPc6d1	svatodušní
svátcích	svátek	k1gInPc6	svátek
v	v	k7c6	v
Mohuči	Mohuč	k1gFnSc6	Mohuč
1184	[number]	k4	1184
<g/>
-	-	kIx~	-
<g/>
1186	[number]	k4	1186
tažení	tažení	k1gNnSc2	tažení
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
1184	[number]	k4	1184
císař	císař	k1gMnSc1	císař
přislíbil	přislíbit	k5eAaPmAgMnS	přislíbit
účast	účast	k1gFnSc4	účast
na	na	k7c6	na
křížovém	křížový	k2eAgNnSc6d1	křížové
tažení	tažení	k1gNnSc6	tažení
do	do	k7c2	do
Svaté	svatý	k2eAgFnSc2d1	svatá
země	zem	k1gFnSc2	zem
27	[number]	k4	27
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1188	[number]	k4	1188
oficiálně	oficiálně	k6eAd1	oficiálně
přebírá	přebírat	k5eAaImIp3nS	přebírat
křižácký	křižácký	k2eAgInSc4d1	křižácký
závazek	závazek	k1gInSc4	závazek
v	v	k7c6	v
Mohuči	Mohuč	k1gFnSc6	Mohuč
1189	[number]	k4	1189
ujal	ujmout	k5eAaPmAgMnS	ujmout
se	se	k3xPyFc4	se
vedení	vedení	k1gNnSc3	vedení
německé	německý	k2eAgFnSc2d1	německá
části	část	k1gFnSc2	část
třetí	třetí	k4xOgFnSc2	třetí
křížové	křížový	k2eAgFnSc2d1	křížová
výpravy	výprava	k1gFnSc2	výprava
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yQgInPc4	který
zahynul	zahynout	k5eAaPmAgMnS	zahynout
nešťastnou	šťastný	k2eNgFnSc7d1	nešťastná
náhodou	náhoda	k1gFnSc7	náhoda
10	[number]	k4	10
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1190	[number]	k4	1190
při	při	k7c6	při
přesunu	přesun	k1gInSc6	přesun
přes	přes	k7c4	přes
řeku	řeka	k1gFnSc4	řeka
Calycadnus	Calycadnus	k1gMnSc1	Calycadnus
<g/>
.	.	kIx.	.
</s>
<s>
Fridrich	Fridrich	k1gMnSc1	Fridrich
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
roku	rok	k1gInSc2	rok
1122	[number]	k4	1122
jako	jako	k8xC	jako
syn	syn	k1gMnSc1	syn
švábského	švábský	k2eAgMnSc2d1	švábský
vévody	vévoda	k1gMnSc2	vévoda
Fridricha	Fridrich	k1gMnSc2	Fridrich
Štaufského	Štaufský	k2eAgMnSc2d1	Štaufský
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
manželky	manželka	k1gFnSc2	manželka
Judity	Judita	k1gFnSc2	Judita
<g/>
,	,	kIx,	,
dcery	dcera	k1gFnSc2	dcera
bavorského	bavorský	k2eAgMnSc2d1	bavorský
vévody	vévoda	k1gMnSc2	vévoda
Jindřicha	Jindřich	k1gMnSc2	Jindřich
Černého	Černý	k1gMnSc2	Černý
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Welfů	Welf	k1gInPc2	Welf
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
jeho	jeho	k3xOp3gNnSc6	jeho
dětství	dětství	k1gNnSc6	dětství
prakticky	prakticky	k6eAd1	prakticky
nic	nic	k3yNnSc4	nic
nevíme	vědět	k5eNaImIp1nP	vědět
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
svého	svůj	k3xOyFgNnSc2	svůj
mládí	mládí	k1gNnSc2	mládí
byl	být	k5eAaImAgMnS	být
Fridrich	Fridrich	k1gMnSc1	Fridrich
velmi	velmi	k6eAd1	velmi
silně	silně	k6eAd1	silně
vázán	vázat	k5eAaImNgInS	vázat
na	na	k7c4	na
rod	rod	k1gInSc4	rod
své	svůj	k3xOyFgFnSc2	svůj
matky	matka	k1gFnSc2	matka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
roku	rok	k1gInSc2	rok
1147	[number]	k4	1147
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
po	po	k7c6	po
svém	svůj	k3xOyFgMnSc6	svůj
zemřelém	zemřelý	k1gMnSc6	zemřelý
otci	otec	k1gMnSc6	otec
švábským	švábský	k2eAgMnSc7d1	švábský
vévodou	vévoda	k1gMnSc7	vévoda
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
štaufským	štaufský	k2eAgMnSc7d1	štaufský
strýcem	strýc	k1gMnSc7	strýc
králem	král	k1gMnSc7	král
Konrádem	Konrád	k1gMnSc7	Konrád
III	III	kA	III
<g/>
.	.	kIx.	.
vydal	vydat	k5eAaPmAgInS	vydat
na	na	k7c4	na
křížovou	křížový	k2eAgFnSc4d1	křížová
výpravu	výprava	k1gFnSc4	výprava
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
i	i	k9	i
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
pěstoval	pěstovat	k5eAaImAgInS	pěstovat
blízký	blízký	k2eAgInSc1d1	blízký
vztah	vztah	k1gInSc1	vztah
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
strýcem	strýc	k1gMnSc7	strýc
z	z	k7c2	z
matčiny	matčin	k2eAgFnSc2d1	matčina
strany	strana	k1gFnSc2	strana
Welfem	Welf	k1gMnSc7	Welf
VI	VI	kA	VI
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
výpravy	výprava	k1gFnSc2	výprava
se	se	k3xPyFc4	se
Fridrich	Fridrich	k1gMnSc1	Fridrich
účastnil	účastnit	k5eAaImAgMnS	účastnit
různých	různý	k2eAgInPc2d1	různý
diplomatických	diplomatický	k2eAgInPc2d1	diplomatický
jednání	jednání	k1gNnPc1	jednání
a	a	k8xC	a
"	"	kIx"	"
<g/>
udržoval	udržovat	k5eAaImAgInS	udržovat
pozici	pozice	k1gFnSc4	pozice
<g/>
"	"	kIx"	"
mezi	mezi	k7c7	mezi
znepřátelenými	znepřátelený	k2eAgInPc7d1	znepřátelený
Štaufy	Štauf	k1gInPc7	Štauf
a	a	k8xC	a
Welfy	Welf	k1gInPc7	Welf
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
křížové	křížový	k2eAgFnSc2d1	křížová
výpravy	výprava	k1gFnSc2	výprava
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
neslavila	slavit	k5eNaImAgFnS	slavit
žádný	žádný	k3yNgInSc4	žádný
podstatný	podstatný	k2eAgInSc4d1	podstatný
úspěch	úspěch	k1gInSc4	úspěch
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
konflikt	konflikt	k1gInSc1	konflikt
mezi	mezi	k7c7	mezi
Štaufy	Štauf	k1gMnPc7	Štauf
a	a	k8xC	a
Welfy	Welf	k1gMnPc7	Welf
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
dále	daleko	k6eAd2	daleko
vystupňoval	vystupňovat	k5eAaPmAgMnS	vystupňovat
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Konrád	Konrád	k1gMnSc1	Konrád
15	[number]	k4	15
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1152	[number]	k4	1152
v	v	k7c6	v
Bamberku	Bamberk	k1gInSc6	Bamberk
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
jen	jen	k9	jen
Fridrich	Fridrich	k1gMnSc1	Fridrich
a	a	k8xC	a
bamberský	bamberský	k2eAgMnSc1d1	bamberský
biskup	biskup	k1gMnSc1	biskup
byli	být	k5eAaImAgMnP	být
u	u	k7c2	u
jeho	on	k3xPp3gNnSc2	on
smrtelného	smrtelný	k2eAgNnSc2d1	smrtelné
lože	lože	k1gNnSc2	lože
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
poté	poté	k6eAd1	poté
tvrdili	tvrdit	k5eAaImAgMnP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Konrád	Konrád	k1gMnSc1	Konrád
za	za	k7c2	za
plného	plný	k2eAgNnSc2d1	plné
vědomí	vědomí	k1gNnSc2	vědomí
předal	předat	k5eAaPmAgMnS	předat
královské	královský	k2eAgInPc4d1	královský
klenoty	klenot	k1gInPc4	klenot
Fridrichovi	Fridrich	k1gMnSc3	Fridrich
a	a	k8xC	a
řekl	říct	k5eAaPmAgInS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
Fridrich	Fridrich	k1gMnSc1	Fridrich
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
jeho	jeho	k3xOp3gMnSc1	jeho
královský	královský	k2eAgMnSc1d1	královský
nástupce	nástupce	k1gMnSc1	nástupce
namísto	namísto	k7c2	namísto
Konrádova	Konrádův	k2eAgNnSc2d1	Konrádovo
tehdy	tehdy	k6eAd1	tehdy
šestiletého	šestiletý	k2eAgMnSc2d1	šestiletý
syna	syn	k1gMnSc2	syn
<g/>
,	,	kIx,	,
budoucího	budoucí	k2eAgMnSc2d1	budoucí
Fridricha	Fridrich	k1gMnSc2	Fridrich
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Fridrich	Fridrich	k1gMnSc1	Fridrich
energicky	energicky	k6eAd1	energicky
usiloval	usilovat	k5eAaImAgMnS	usilovat
o	o	k7c4	o
korunu	koruna	k1gFnSc4	koruna
a	a	k8xC	a
ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
čtvrtého	čtvrtý	k4xOgNnSc2	čtvrtý
března	březno	k1gNnSc2	březno
jej	on	k3xPp3gMnSc4	on
kurfiřti	kurfiřt	k1gMnPc1	kurfiřt
zvolili	zvolit	k5eAaPmAgMnP	zvolit
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
korunován	korunovat	k5eAaBmNgInS	korunovat
v	v	k7c6	v
Cáchách	Cáchy	k1gFnPc6	Cáchy
o	o	k7c4	o
několik	několik	k4yIc4	několik
dnů	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
zvolení	zvolení	k1gNnSc6	zvolení
Fridrich	Fridrich	k1gMnSc1	Fridrich
dále	daleko	k6eAd2	daleko
posiloval	posilovat	k5eAaImAgMnS	posilovat
své	svůj	k3xOyFgInPc4	svůj
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
Welfy	Welf	k1gMnPc7	Welf
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1152	[number]	k4	1152
obdržel	obdržet	k5eAaPmAgMnS	obdržet
Welf	Welf	k1gMnSc1	Welf
VI	VI	kA	VI
<g/>
.	.	kIx.	.
říšské	říšský	k2eAgFnSc2d1	říšská
državy	država	k1gFnSc2	država
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
(	(	kIx(	(
<g/>
mj.	mj.	kA	mj.
markrabství	markrabství	k1gNnSc1	markrabství
toskánské	toskánský	k2eAgInPc1d1	toskánský
<g/>
,	,	kIx,	,
vévodství	vévodství	k1gNnSc1	vévodství
spoletské	spoletský	k2eAgFnSc2d1	spoletský
a	a	k8xC	a
principát	principát	k1gInSc1	principát
sardinský	sardinský	k2eAgInSc1d1	sardinský
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
pak	pak	k6eAd1	pak
jeho	jeho	k3xOp3gMnSc1	jeho
synovec	synovec	k1gMnSc1	synovec
Jindřich	Jindřich	k1gMnSc1	Jindřich
Lev	Lev	k1gMnSc1	Lev
získal	získat	k5eAaPmAgMnS	získat
právo	právo	k1gNnSc4	právo
investitury	investitura	k1gFnSc2	investitura
v	v	k7c6	v
biskupství	biskupství	k1gNnSc6	biskupství
oldenburském	oldenburský	k2eAgNnSc6d1	oldenburský
<g/>
,	,	kIx,	,
retzenburském	retzenburský	k2eAgNnSc6d1	retzenburský
a	a	k8xC	a
meklenburském	meklenburský	k2eAgMnSc6d1	meklenburský
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
nově	nově	k6eAd1	nově
vzniklých	vzniklý	k2eAgFnPc6d1	vzniklá
diecézích	diecéze	k1gFnPc6	diecéze
na	na	k7c4	na
východ	východ	k1gInSc4	východ
od	od	k7c2	od
Labe	Labe	k1gNnSc2	Labe
<g/>
.	.	kIx.	.
</s>
<s>
Postavení	postavení	k1gNnSc1	postavení
Jindřicha	Jindřich	k1gMnSc2	Jindřich
Lva	Lev	k1gMnSc2	Lev
se	se	k3xPyFc4	se
tak	tak	k9	tak
přiblížilo	přiblížit	k5eAaPmAgNnS	přiblížit
postavení	postavení	k1gNnSc1	postavení
krále	král	k1gMnSc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
18	[number]	k4	18
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1155	[number]	k4	1155
byl	být	k5eAaImAgMnS	být
Barbarossa	Barbarossa	k1gMnSc1	Barbarossa
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
korunován	korunován	k2eAgInSc4d1	korunován
císařem	císař	k1gMnSc7	císař
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
ceremoniálu	ceremoniál	k1gInSc2	ceremoniál
Fridrich	Fridrich	k1gMnSc1	Fridrich
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
vzít	vzít	k5eAaPmF	vzít
za	za	k7c4	za
uzdu	uzda	k1gFnSc4	uzda
papežova	papežův	k2eAgMnSc2d1	papežův
koně	kůň	k1gMnSc2	kůň
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
toto	tento	k3xDgNnSc4	tento
gesto	gesto	k1gNnSc4	gesto
chápal	chápat	k5eAaImAgInS	chápat
jako	jako	k9	jako
nepřijatelný	přijatelný	k2eNgInSc1d1	nepřijatelný
projev	projev	k1gInSc1	projev
podřízenosti	podřízenost	k1gFnSc2	podřízenost
papeži	papež	k1gMnSc3	papež
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
po	po	k7c4	po
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
<g/>
,	,	kIx,	,
že	že	k8xS	že
toto	tento	k3xDgNnSc4	tento
"	"	kIx"	"
<g/>
gesto	gesto	k1gNnSc4	gesto
<g/>
"	"	kIx"	"
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
císaře	císař	k1gMnSc2	císař
Konstantina	Konstantin	k1gMnSc2	Konstantin
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zaběhlému	zaběhlý	k2eAgInSc3d1	zaběhlý
rituálu	rituál	k1gInSc3	rituál
podvolil	podvolit	k5eAaPmAgMnS	podvolit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1156	[number]	k4	1156
se	se	k3xPyFc4	se
Fridrich	Fridrich	k1gMnSc1	Fridrich
Barbarossa	Barbarossa	k1gMnSc1	Barbarossa
podruhé	podruhé	k6eAd1	podruhé
oženil	oženit	k5eAaPmAgMnS	oženit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
manželkou	manželka	k1gFnSc7	manželka
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
dědička	dědička	k1gFnSc1	dědička
burgundského	burgundský	k2eAgNnSc2d1	burgundské
hrabství	hrabství	k1gNnSc2	hrabství
Beatrice	Beatrice	k1gFnSc2	Beatrice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
využil	využít	k5eAaPmAgMnS	využít
úmrtí	úmrtí	k1gNnSc4	úmrtí
rýnského	rýnský	k2eAgMnSc2d1	rýnský
falckraběte	falckrabě	k1gMnSc2	falckrabě
Hermana	Herman	k1gMnSc2	Herman
ze	z	k7c2	z
Stahlecka	Stahlecko	k1gNnSc2	Stahlecko
a	a	k8xC	a
novým	nový	k2eAgMnSc7d1	nový
falckrabětem	falckrabě	k1gMnSc7	falckrabě
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
svého	svůj	k1gMnSc4	svůj
nevlastního	vlastní	k2eNgMnSc4d1	nevlastní
bratra	bratr	k1gMnSc4	bratr
Konráda	Konrád	k1gMnSc4	Konrád
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Obléhání	obléhání	k1gNnSc2	obléhání
Milána	Milán	k1gInSc2	Milán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1167	[number]	k4	1167
porazilo	porazit	k5eAaPmAgNnS	porazit
císařské	císařský	k2eAgNnSc1d1	císařské
vojsko	vojsko	k1gNnSc1	vojsko
<g/>
,	,	kIx,	,
vedené	vedený	k2eAgNnSc1d1	vedené
jeho	jeho	k3xOp3gNnPc2	jeho
legáty	legát	k1gInPc1	legát
kolínským	kolínský	k2eAgMnSc7d1	kolínský
arcibiskupem	arcibiskup	k1gMnSc7	arcibiskup
Rainaldem	Rainald	k1gMnSc7	Rainald
z	z	k7c2	z
Dasselu	Dassel	k1gInSc2	Dassel
a	a	k8xC	a
mohučským	mohučský	k2eAgMnSc7d1	mohučský
arcibiskupem	arcibiskup	k1gMnSc7	arcibiskup
Christianem	Christian	k1gMnSc7	Christian
<g/>
,	,	kIx,	,
Římany	Říman	k1gMnPc4	Říman
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
dorazil	dorazit	k5eAaPmAgMnS	dorazit
k	k	k7c3	k
Římu	Řím	k1gInSc3	Řím
i	i	k9	i
Fridrich	Fridrich	k1gMnSc1	Fridrich
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
vzdoropapežem	vzdoropapež	k1gMnSc7	vzdoropapež
Paschalem	Paschal	k1gMnSc7	Paschal
III	III	kA	III
<g/>
.	.	kIx.	.
a	a	k8xC	a
dalším	další	k2eAgNnSc7d1	další
vojskem	vojsko	k1gNnSc7	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Spojenému	spojený	k2eAgNnSc3d1	spojené
vojsku	vojsko	k1gNnSc3	vojsko
nemohl	moct	k5eNaImAgMnS	moct
vzdorovat	vzdorovat	k5eAaImF	vzdorovat
ani	ani	k8xC	ani
papež	papež	k1gMnSc1	papež
Alexandr	Alexandr	k1gMnSc1	Alexandr
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
Římané	Říman	k1gMnPc1	Říman
<g/>
.	.	kIx.	.
</s>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
otec	otec	k1gMnSc1	otec
uprchl	uprchnout	k5eAaPmAgMnS	uprchnout
do	do	k7c2	do
Beneventa	Benevento	k1gNnSc2	Benevento
a	a	k8xC	a
Římané	Říman	k1gMnPc1	Říman
se	se	k3xPyFc4	se
podrobili	podrobit	k5eAaPmAgMnP	podrobit
císaři	císař	k1gMnPc1	císař
a	a	k8xC	a
uzavřeli	uzavřít	k5eAaPmAgMnP	uzavřít
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
dohodu	dohoda	k1gFnSc4	dohoda
<g/>
,	,	kIx,	,
zajišťující	zajišťující	k2eAgFnSc4d1	zajišťující
jim	on	k3xPp3gMnPc3	on
další	další	k2eAgInSc4d1	další
existenci	existence	k1gFnSc4	existence
senátu	senát	k1gInSc2	senát
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
30	[number]	k4	30
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
byl	být	k5eAaImAgMnS	být
Paschal	Paschal	k1gFnSc4	Paschal
v	v	k7c6	v
chrámu	chrám	k1gInSc6	chrám
svatého	svatý	k2eAgMnSc2d1	svatý
Petra	Petr	k1gMnSc2	Petr
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
slavnostně	slavnostně	k6eAd1	slavnostně
nastolen	nastolit	k5eAaPmNgMnS	nastolit
a	a	k8xC	a
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
korunoval	korunovat	k5eAaBmAgMnS	korunovat
Fridrichovu	Fridrichův	k2eAgFnSc4d1	Fridrichova
manželku	manželka	k1gFnSc4	manželka
císařovnou	císařovna	k1gFnSc7	císařovna
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
Barbarossův	Barbarossův	k2eAgInSc1d1	Barbarossův
triumf	triumf	k1gInSc1	triumf
byl	být	k5eAaImAgInS	být
vystřídán	vystřídán	k2eAgInSc4d1	vystřídán
katastrofou	katastrofa	k1gFnSc7	katastrofa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
císařově	císařův	k2eAgNnSc6d1	císařovo
vojsku	vojsko	k1gNnSc6	vojsko
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
malárie	malárie	k1gFnSc1	malárie
<g/>
,	,	kIx,	,
které	který	k3yIgFnSc3	který
padla	padnout	k5eAaImAgFnS	padnout
za	za	k7c4	za
oběť	oběť	k1gFnSc4	oběť
nejen	nejen	k6eAd1	nejen
většina	většina	k1gFnSc1	většina
vojska	vojsko	k1gNnSc2	vojsko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
přední	přední	k2eAgMnPc1d1	přední
představitelé	představitel	k1gMnPc1	představitel
štaufské	štaufský	k2eAgFnSc2d1	štaufská
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
zemřelými	zemřelý	k1gMnPc7	zemřelý
byli	být	k5eAaImAgMnP	být
kolínský	kolínský	k2eAgMnSc1d1	kolínský
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
Rainald	Rainald	k1gMnSc1	Rainald
z	z	k7c2	z
Dasselu	Dassel	k1gInSc2	Dassel
<g/>
,	,	kIx,	,
biskupové	biskup	k1gMnPc1	biskup
Konrád	Konrád	k1gMnSc1	Konrád
z	z	k7c2	z
Augsburgu	Augsburg	k1gInSc2	Augsburg
<g/>
,	,	kIx,	,
Alexandr	Alexandr	k1gMnSc1	Alexandr
z	z	k7c2	z
Lutychu	Lutych	k1gInSc2	Lutych
<g/>
,	,	kIx,	,
Daniel	Daniel	k1gMnSc1	Daniel
Pražský	pražský	k2eAgMnSc1d1	pražský
<g/>
,	,	kIx,	,
Eberhard	Eberhard	k1gMnSc1	Eberhard
z	z	k7c2	z
Řezna	Řezno	k1gNnSc2	Řezno
<g/>
,	,	kIx,	,
Gottfried	Gottfried	k1gInSc1	Gottfried
ze	z	k7c2	z
Špýru	Špýr	k1gInSc2	Špýr
a	a	k8xC	a
Hermann	Hermann	k1gMnSc1	Hermann
z	z	k7c2	z
Verdenu	Verden	k1gInSc2	Verden
<g/>
.	.	kIx.	.
</s>
<s>
Zemřeli	zemřít	k5eAaPmAgMnP	zemřít
i	i	k9	i
vévodové	vévoda	k1gMnPc1	vévoda
Fridrich	Fridrich	k1gMnSc1	Fridrich
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Švábský	švábský	k2eAgMnSc1d1	švábský
<g/>
,	,	kIx,	,
Welf	Welf	k1gMnSc1	Welf
VII	VII	kA	VII
<g/>
.	.	kIx.	.
a	a	k8xC	a
Děpolt	Děpolt	k1gMnSc1	Děpolt
<g/>
,	,	kIx,	,
bratr	bratr	k1gMnSc1	bratr
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
Vladislava	Vladislav	k1gMnSc2	Vladislav
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgNnPc2d1	další
hrabat	hrabě	k1gNnPc2	hrabě
a	a	k8xC	a
šlechticů	šlechtic	k1gMnPc2	šlechtic
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc3	tento
situace	situace	k1gFnSc2	situace
se	se	k3xPyFc4	se
Fridrich	Fridrich	k1gMnSc1	Fridrich
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
zdecimovaným	zdecimovaný	k2eAgNnSc7d1	zdecimované
vojskem	vojsko	k1gNnSc7	vojsko
stáhl	stáhnout	k5eAaPmAgMnS	stáhnout
na	na	k7c4	na
sever	sever	k1gInSc4	sever
<g/>
.	.	kIx.	.
</s>
<s>
Oslabení	oslabení	k1gNnSc1	oslabení
císařovy	císařův	k2eAgFnSc2d1	císařova
moci	moc	k1gFnSc2	moc
využila	využít	k5eAaPmAgFnS	využít
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Itálii	Itálie	k1gFnSc6	Itálie
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
aktivizaci	aktivizace	k1gFnSc3	aktivizace
Lombardská	lombardský	k2eAgFnSc1d1	Lombardská
liga	liga	k1gFnSc1	liga
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
rostoucímu	rostoucí	k2eAgNnSc3d1	rostoucí
nepřátelství	nepřátelství	k1gNnSc3	nepřátelství
a	a	k8xC	a
slabosti	slabost	k1gFnSc3	slabost
císařského	císařský	k2eAgNnSc2d1	císařské
vojska	vojsko	k1gNnSc2	vojsko
musel	muset	k5eAaImAgMnS	muset
nakonec	nakonec	k9	nakonec
císař	císař	k1gMnSc1	císař
prchat	prchat	k5eAaImF	prchat
z	z	k7c2	z
Itálie	Itálie	k1gFnSc2	Itálie
v	v	k7c6	v
přestrojení	přestrojení	k1gNnSc6	přestrojení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1169	[number]	k4	1169
donutil	donutit	k5eAaPmAgMnS	donutit
Fridrich	Fridrich	k1gMnSc1	Fridrich
k	k	k7c3	k
rezignaci	rezignace	k1gFnSc3	rezignace
salcburského	salcburský	k2eAgMnSc2d1	salcburský
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
rezignaci	rezignace	k1gFnSc6	rezignace
císař	císař	k1gMnSc1	císař
nedosadil	dosadit	k5eNaPmAgMnS	dosadit
nového	nový	k2eAgMnSc4d1	nový
metropolitu	metropolita	k1gMnSc4	metropolita
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nechal	nechat	k5eAaPmAgMnS	nechat
arcibiskupství	arcibiskupství	k1gNnSc4	arcibiskupství
pod	pod	k7c7	pod
přímou	přímý	k2eAgFnSc7d1	přímá
správou	správa	k1gFnSc7	správa
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
15	[number]	k4	15
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1169	[number]	k4	1169
nechal	nechat	k5eAaPmAgMnS	nechat
v	v	k7c6	v
Cáchách	Cáchy	k1gFnPc6	Cáchy
korunovat	korunovat	k5eAaBmF	korunovat
králem	král	k1gMnSc7	král
svého	svůj	k3xOyFgNnSc2	svůj
syna	syn	k1gMnSc4	syn
Jindřicha	Jindřich	k1gMnSc2	Jindřich
VI	VI	kA	VI
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Legnana	Legnan	k1gMnSc2	Legnan
<g/>
.	.	kIx.	.
</s>
<s>
Tradičně	tradičně	k6eAd1	tradičně
dobré	dobrý	k2eAgInPc1d1	dobrý
vztahy	vztah	k1gInPc1	vztah
císaře	císař	k1gMnSc2	císař
k	k	k7c3	k
Jindřichu	Jindřich	k1gMnSc3	Jindřich
Lvovi	Lev	k1gMnSc3	Lev
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgInP	začít
horšit	horšit	k5eAaImF	horšit
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
byly	být	k5eAaImAgFnP	být
nejenom	nejenom	k6eAd1	nejenom
Jindřichovy	Jindřichův	k2eAgInPc1d1	Jindřichův
spory	spor	k1gInPc1	spor
s	s	k7c7	s
jeho	jeho	k3xOp3gMnPc7	jeho
knížecími	knížecí	k2eAgMnPc7d1	knížecí
protivníky	protivník	k1gMnPc7	protivník
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
především	především	k9	především
jeho	jeho	k3xOp3gFnSc1	jeho
neochota	neochota	k1gFnSc1	neochota
poskytnout	poskytnout	k5eAaPmF	poskytnout
Fridrichovi	Fridrichův	k2eAgMnPc1d1	Fridrichův
vojsko	vojsko	k1gNnSc1	vojsko
k	k	k7c3	k
bojům	boj	k1gInPc3	boj
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Fridrich	Fridrich	k1gMnSc1	Fridrich
pak	pak	k6eAd1	pak
logicky	logicky	k6eAd1	logicky
považoval	považovat	k5eAaImAgMnS	považovat
Jindřichovu	Jindřichův	k2eAgFnSc4d1	Jindřichova
nedostatečnou	dostatečný	k2eNgFnSc4d1	nedostatečná
podporu	podpora	k1gFnSc4	podpora
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
příčin	příčina	k1gFnPc2	příčina
svého	svůj	k3xOyFgInSc2	svůj
neúspěchu	neúspěch	k1gInSc2	neúspěch
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
začal	začít	k5eAaPmAgInS	začít
postupně	postupně	k6eAd1	postupně
činit	činit	k5eAaImF	činit
kroky	krok	k1gInPc4	krok
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
měly	mít	k5eAaImAgInP	mít
vést	vést	k5eAaImF	vést
ke	k	k7c3	k
zmenšení	zmenšení	k1gNnSc3	zmenšení
jeho	jeho	k3xOp3gFnSc2	jeho
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
krokem	krok	k1gInSc7	krok
bylo	být	k5eAaImAgNnS	být
uzavření	uzavření	k1gNnSc1	uzavření
dohody	dohoda	k1gFnSc2	dohoda
s	s	k7c7	s
papežem	papež	k1gMnSc7	papež
a	a	k8xC	a
následné	následný	k2eAgNnSc4d1	následné
sesazení	sesazení	k1gNnSc4	sesazení
schizmatických	schizmatický	k2eAgMnPc2d1	schizmatický
biskupů	biskup	k1gMnPc2	biskup
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
byli	být	k5eAaImAgMnP	být
věrní	věrný	k2eAgMnPc1d1	věrný
Jindřichovi	Jindřichův	k2eAgMnPc1d1	Jindřichův
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jejich	jejich	k3xOp3gNnPc4	jejich
místa	místo	k1gNnPc4	místo
se	se	k3xPyFc4	se
vrátili	vrátit	k5eAaPmAgMnP	vrátit
Jindřichovi	Jindřichův	k2eAgMnPc1d1	Jindřichův
nepřátelé	nepřítel	k1gMnPc1	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
Jindřichově	Jindřichův	k2eAgFnSc3d1	Jindřichova
oslabení	oslabení	k1gNnSc4	oslabení
vedlo	vést	k5eAaImAgNnS	vést
i	i	k9	i
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gMnSc1	jeho
strýc	strýc	k1gMnSc1	strýc
Welf	Welf	k1gMnSc1	Welf
VI	VI	kA	VI
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
jenž	jenž	k3xRgMnSc1	jenž
ale	ale	k8xC	ale
byl	být	k5eAaImAgMnS	být
zároveň	zároveň	k6eAd1	zároveň
i	i	k8xC	i
strýcem	strýc	k1gMnSc7	strýc
Barbarossy	Barbarossa	k1gMnSc2	Barbarossa
<g/>
)	)	kIx)	)
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
jiné	jiný	k2eAgFnPc4d1	jiná
výhody	výhoda	k1gFnPc4	výhoda
převedl	převést	k5eAaPmAgMnS	převést
na	na	k7c4	na
Fridricha	Fridrich	k1gMnSc4	Fridrich
své	svůj	k3xOyFgFnSc2	svůj
dědičné	dědičný	k2eAgInPc4d1	dědičný
statky	statek	k1gInPc7	statek
ve	v	k7c6	v
Švábsku	Švábsko	k1gNnSc6	Švábsko
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
Jindřich	Jindřich	k1gMnSc1	Jindřich
Lev	Lev	k1gMnSc1	Lev
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
dostavit	dostavit	k5eAaPmF	dostavit
na	na	k7c4	na
říšský	říšský	k2eAgInSc4d1	říšský
sněm	sněm	k1gInSc4	sněm
ve	v	k7c6	v
Wormsu	Worms	k1gInSc6	Worms
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1179	[number]	k4	1179
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
měly	mít	k5eAaImAgInP	mít
projednávat	projednávat	k5eAaImF	projednávat
jeho	jeho	k3xOp3gInPc1	jeho
spory	spor	k1gInPc1	spor
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
knížaty	kníže	k1gMnPc7wR	kníže
a	a	k8xC	a
církevními	církevní	k2eAgMnPc7d1	církevní
představiteli	představitel	k1gMnPc7	představitel
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
měsících	měsíc	k1gInPc6	měsíc
se	se	k3xPyFc4	se
Jindřich	Jindřich	k1gMnSc1	Jindřich
odmítal	odmítat	k5eAaImAgMnS	odmítat
dostavit	dostavit	k5eAaPmF	dostavit
ke	k	k7c3	k
dvoru	dvůr	k1gInSc3	dvůr
a	a	k8xC	a
spor	spor	k1gInSc1	spor
řešit	řešit	k5eAaImF	řešit
<g/>
.	.	kIx.	.
</s>
<s>
Následkem	následkem	k7c2	následkem
toho	ten	k3xDgNnSc2	ten
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
něho	on	k3xPp3gMnSc4	on
uvalena	uvalen	k2eAgMnSc4d1	uvalen
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1179	[number]	k4	1179
klatba	klatba	k1gFnSc1	klatba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
na	na	k7c6	na
sněmu	sněm	k1gInSc6	sněm
ve	v	k7c6	v
Würzburgu	Würzburg	k1gInSc6	Würzburg
byla	být	k5eAaImAgFnS	být
Jindřichu	Jindřich	k1gMnSc3	Jindřich
Lvovi	Lev	k1gMnSc3	Lev
pro	pro	k7c4	pro
urážku	urážka	k1gFnSc4	urážka
císařského	císařský	k2eAgInSc2d1	císařský
majestátu	majestát	k1gInSc2	majestát
odňata	odňat	k2eAgNnPc1d1	odňato
říšská	říšský	k2eAgNnPc1d1	říšské
léna	léno	k1gNnPc1	léno
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
vévodství	vévodství	k1gNnSc1	vévodství
saské	saský	k2eAgNnSc1d1	Saské
a	a	k8xC	a
bavorské	bavorský	k2eAgNnSc1d1	bavorské
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
bylo	být	k5eAaImAgNnS	být
Sasko	Sasko	k1gNnSc1	Sasko
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
na	na	k7c4	na
Vestfálsko	Vestfálsko	k1gNnSc4	Vestfálsko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
připadlo	připadnout	k5eAaPmAgNnS	připadnout
kolínskému	kolínský	k2eAgNnSc3d1	kolínské
arcibiskupství	arcibiskupství	k1gNnSc3	arcibiskupství
<g/>
,	,	kIx,	,
a	a	k8xC	a
Engern	Engern	k1gInSc1	Engern
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
získal	získat	k5eAaPmAgMnS	získat
Bernard	Bernard	k1gMnSc1	Bernard
pocházející	pocházející	k2eAgFnSc2d1	pocházející
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Askánců	Askánec	k1gMnPc2	Askánec
<g/>
.	.	kIx.	.
</s>
<s>
Bavorsko	Bavorsko	k1gNnSc1	Bavorsko
obdržel	obdržet	k5eAaPmAgMnS	obdržet
osvědčený	osvědčený	k2eAgMnSc1d1	osvědčený
Fridrichův	Fridrichův	k2eAgMnSc1d1	Fridrichův
spojenec	spojenec	k1gMnSc1	spojenec
Ota	Ota	k1gMnSc1	Ota
z	z	k7c2	z
Wittelsbachu	Wittelsbach	k1gInSc2	Wittelsbach
a	a	k8xC	a
Štýrsko	Štýrsko	k1gNnSc4	Štýrsko
<g/>
,	,	kIx,	,
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
formálně	formálně	k6eAd1	formálně
závislé	závislý	k2eAgInPc1d1	závislý
na	na	k7c6	na
Bavorsku	Bavorsko	k1gNnSc6	Bavorsko
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
povýšeno	povýšit	k5eAaPmNgNnS	povýšit
na	na	k7c4	na
samostatné	samostatný	k2eAgNnSc4d1	samostatné
vévodství	vévodství	k1gNnSc4	vévodství
pod	pod	k7c7	pod
vládou	vláda	k1gFnSc7	vláda
dosavadního	dosavadní	k2eAgMnSc2d1	dosavadní
markraběte	markrabě	k1gMnSc2	markrabě
Otakara	Otakar	k1gMnSc2	Otakar
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
hrabata	hrabě	k1gNnPc1	hrabě
z	z	k7c2	z
Andechsu	Andechs	k1gInSc2	Andechs
byla	být	k5eAaImAgNnP	být
povýšena	povýšit	k5eAaPmNgNnP	povýšit
do	do	k7c2	do
hodnosti	hodnost	k1gFnSc2	hodnost
vévodů	vévoda	k1gMnPc2	vévoda
meránských	meránský	k2eAgInPc2d1	meránský
<g/>
,	,	kIx,	,
chorvatských	chorvatský	k2eAgInPc2d1	chorvatský
a	a	k8xC	a
dalmatských	dalmatský	k2eAgInPc2d1	dalmatský
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Třetí	třetí	k4xOgFnSc1	třetí
křížová	křížový	k2eAgFnSc1d1	křížová
výprava	výprava	k1gFnSc1	výprava
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
katastrofické	katastrofický	k2eAgFnSc6d1	katastrofická
porážce	porážka	k1gFnSc6	porážka
vojska	vojsko	k1gNnSc2	vojsko
Jeruzalémského	jeruzalémský	k2eAgNnSc2d1	Jeruzalémské
království	království	k1gNnSc2	království
u	u	k7c2	u
Hattínu	Hattín	k1gInSc2	Hattín
a	a	k8xC	a
následujícím	následující	k2eAgNnSc6d1	následující
obsazení	obsazení	k1gNnSc6	obsazení
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
Turky	Turek	k1gMnPc4	Turek
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
znovu	znovu	k6eAd1	znovu
aktuální	aktuální	k2eAgFnSc1d1	aktuální
otázka	otázka	k1gFnSc1	otázka
křížové	křížový	k2eAgFnSc2d1	křížová
výpravy	výprava	k1gFnSc2	výprava
<g/>
.	.	kIx.	.
</s>
<s>
Fridrich	Fridrich	k1gMnSc1	Fridrich
i	i	k9	i
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
pečlivě	pečlivě	k6eAd1	pečlivě
sledoval	sledovat	k5eAaImAgMnS	sledovat
vývoj	vývoj	k1gInSc4	vývoj
ve	v	k7c6	v
Svaté	svatý	k2eAgFnSc6d1	svatá
zemi	zem	k1gFnSc6	zem
a	a	k8xC	a
už	už	k6eAd1	už
dříve	dříve	k6eAd2	dříve
(	(	kIx(	(
<g/>
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
1184	[number]	k4	1184
<g/>
)	)	kIx)	)
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
na	na	k7c6	na
schůzce	schůzka	k1gFnSc6	schůzka
s	s	k7c7	s
papežem	papež	k1gMnSc7	papež
Luciem	Lucius	k1gMnSc7	Lucius
III	III	kA	III
<g/>
.	.	kIx.	.
připravenost	připravenost	k1gFnSc4	připravenost
podílet	podílet	k5eAaImF	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
přípravě	příprava	k1gFnSc6	příprava
nové	nový	k2eAgFnSc2d1	nová
kruciáty	kruciáta	k1gFnSc2	kruciáta
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
ale	ale	k8xC	ale
už	už	k6eAd1	už
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
navázal	navázat	k5eAaPmAgMnS	navázat
císař	císař	k1gMnSc1	císař
(	(	kIx(	(
<g/>
byť	byť	k8xS	byť
epizodní	epizodní	k2eAgInPc1d1	epizodní
<g/>
)	)	kIx)	)
styky	styk	k1gInPc1	styk
i	i	k9	i
se	s	k7c7	s
sultánem	sultán	k1gMnSc7	sultán
Saladinem	Saladin	k1gMnSc7	Saladin
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
použít	použít	k5eAaPmF	použít
ho	on	k3xPp3gMnSc4	on
jako	jako	k8xC	jako
protiváhu	protiváha	k1gFnSc4	protiváha
při	při	k7c6	při
jednáních	jednání	k1gNnPc6	jednání
s	s	k7c7	s
Byzancí	Byzanc	k1gFnSc7	Byzanc
<g/>
.	.	kIx.	.
</s>
<s>
Konečné	Konečné	k2eAgNnSc1d1	Konečné
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
o	o	k7c6	o
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
výpravy	výprava	k1gFnSc2	výprava
však	však	k9	však
Fridrich	Fridrich	k1gMnSc1	Fridrich
odložil	odložit	k5eAaPmAgMnS	odložit
až	až	k9	až
na	na	k7c4	na
dvorský	dvorský	k2eAgInSc4d1	dvorský
sněm	sněm	k1gInSc4	sněm
plánovaný	plánovaný	k2eAgInSc4d1	plánovaný
na	na	k7c4	na
Velikonoce	Velikonoce	k1gFnPc4	Velikonoce
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
v	v	k7c6	v
Mohuči	Mohuč	k1gFnSc6	Mohuč
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
1188	[number]	k4	1188
se	se	k3xPyFc4	se
císař	císař	k1gMnSc1	císař
snažil	snažit	k5eAaImAgMnS	snažit
o	o	k7c4	o
zajištění	zajištění	k1gNnSc4	zajištění
budoucího	budoucí	k2eAgInSc2d1	budoucí
klidného	klidný	k2eAgInSc2d1	klidný
průběhu	průběh	k1gInSc2	průběh
tažení	tažení	k1gNnSc1	tažení
výpravy	výprava	k1gFnSc2	výprava
až	až	k9	až
do	do	k7c2	do
Svaté	svatý	k2eAgFnSc2d1	svatá
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Jednal	jednat	k5eAaImAgInS	jednat
proto	proto	k8xC	proto
s	s	k7c7	s
posly	posel	k1gMnPc7	posel
z	z	k7c2	z
Byzance	Byzanc	k1gFnSc2	Byzanc
<g/>
,	,	kIx,	,
z	z	k7c2	z
Ikonia	Ikonium	k1gNnSc2	Ikonium
<g/>
,	,	kIx,	,
Uher	Uhry	k1gFnPc2	Uhry
i	i	k8xC	i
Srbska	Srbsko	k1gNnSc2	Srbsko
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
bylo	být	k5eAaImAgNnS	být
zejména	zejména	k9	zejména
byzantské	byzantský	k2eAgNnSc1d1	byzantské
poselstvo	poselstvo	k1gNnSc1	poselstvo
ujištěno	ujistit	k5eAaPmNgNnS	ujistit
o	o	k7c6	o
veskrze	veskrze	k6eAd1	veskrze
mírových	mírový	k2eAgInPc6d1	mírový
úmyslech	úmysl	k1gInPc6	úmysl
výpravy	výprava	k1gFnSc2	výprava
a	a	k8xC	a
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
byly	být	k5eAaImAgFnP	být
odpřisáhnuty	odpřisáhnout	k5eAaPmNgFnP	odpřisáhnout
císařovým	císařův	k2eAgMnSc7d1	císařův
synem	syn	k1gMnSc7	syn
vévodou	vévoda	k1gMnSc7	vévoda
Fridrichem	Fridrich	k1gMnSc7	Fridrich
Švábským	švábský	k2eAgMnSc7d1	švábský
<g/>
,	,	kIx,	,
rakouským	rakouský	k2eAgMnSc7d1	rakouský
vévodou	vévoda	k1gMnSc7	vévoda
Leopoldem	Leopold	k1gMnSc7	Leopold
V.	V.	kA	V.
a	a	k8xC	a
würzbuským	würzbuský	k2eAgMnSc7d1	würzbuský
biskupem	biskup	k1gMnSc7	biskup
Gottfriedem	Gottfried	k1gMnSc7	Gottfried
<g/>
,	,	kIx,	,
přislíbilo	přislíbit	k5eAaPmAgNnS	přislíbit
poselstvo	poselstvo	k1gNnSc1	poselstvo
podporu	podpor	k1gInSc2	podpor
a	a	k8xC	a
zásobování	zásobování	k1gNnSc2	zásobování
výpravy	výprava	k1gFnSc2	výprava
na	na	k7c6	na
území	území	k1gNnSc6	území
Byzance	Byzanc	k1gFnSc2	Byzanc
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
před	před	k7c7	před
svým	svůj	k3xOyFgInSc7	svůj
odchodem	odchod	k1gInSc7	odchod
na	na	k7c4	na
křížovou	křížový	k2eAgFnSc4d1	křížová
výpravu	výprava	k1gFnSc4	výprava
vyřešil	vyřešit	k5eAaPmAgMnS	vyřešit
Fridrich	Fridrich	k1gMnSc1	Fridrich
otázky	otázka	k1gFnSc2	otázka
související	související	k2eAgFnPc4d1	související
s	s	k7c7	s
regentskou	regentský	k2eAgFnSc7d1	regentská
vládou	vláda	k1gFnSc7	vláda
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
pověřil	pověřit	k5eAaPmAgMnS	pověřit
svého	svůj	k3xOyFgMnSc4	svůj
syna	syn	k1gMnSc4	syn
Jindřicha	Jindřich	k1gMnSc4	Jindřich
<g/>
,	,	kIx,	,
a	a	k8xC	a
vymezil	vymezit	k5eAaPmAgInS	vymezit
přesný	přesný	k2eAgInSc4d1	přesný
okruh	okruh	k1gInSc4	okruh
úkolů	úkol	k1gInPc2	úkol
pro	pro	k7c4	pro
každého	každý	k3xTgMnSc4	každý
ze	z	k7c2	z
svých	svůj	k3xOyFgMnPc2	svůj
synů	syn	k1gMnPc2	syn
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
Klement	Klement	k1gMnSc1	Klement
III	III	kA	III
<g/>
.	.	kIx.	.
mu	on	k3xPp3gNnSc3	on
slíbil	slíbit	k5eAaPmAgMnS	slíbit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vykoná	vykonat	k5eAaPmIp3nS	vykonat
Jindřichovu	Jindřichův	k2eAgFnSc4d1	Jindřichova
císařskou	císařský	k2eAgFnSc4d1	císařská
korunovaci	korunovace	k1gFnSc4	korunovace
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
svým	svůj	k3xOyFgInSc7	svůj
odchodem	odchod	k1gInSc7	odchod
Barbarossa	Barbarossa	k1gMnSc1	Barbarossa
rovněž	rovněž	k6eAd1	rovněž
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
nové	nový	k2eAgNnSc4d1	nové
nařízení	nařízení	k1gNnSc4	nařízení
o	o	k7c4	o
míru	míra	k1gFnSc4	míra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1189	[number]	k4	1189
vyrazilo	vyrazit	k5eAaPmAgNnS	vyrazit
shromážděné	shromážděný	k2eAgNnSc1d1	shromážděné
vojsko	vojsko	k1gNnSc1	vojsko
z	z	k7c2	z
Řezna	Řezno	k1gNnSc2	Řezno
do	do	k7c2	do
Svaté	svatý	k2eAgFnSc2d1	svatá
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
dobové	dobový	k2eAgInPc4d1	dobový
prameny	pramen	k1gInPc4	pramen
odhadovaly	odhadovat	k5eAaImAgFnP	odhadovat
počet	počet	k1gInSc1	počet
bojovníků	bojovník	k1gMnPc2	bojovník
na	na	k7c4	na
100	[number]	k4	100
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
současní	současný	k2eAgMnPc1d1	současný
historikové	historik	k1gMnPc1	historik
předpokládají	předpokládat	k5eAaImIp3nP	předpokládat
spíše	spíše	k9	spíše
12	[number]	k4	12
000-15	[number]	k4	000-15
000	[number]	k4	000
bojovníků	bojovník	k1gMnPc2	bojovník
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
začátkem	začátkem	k7c2	začátkem
června	červen	k1gInSc2	červen
byl	být	k5eAaImAgMnS	být
císař	císař	k1gMnSc1	císař
Fridrich	Fridrich	k1gMnSc1	Fridrich
v	v	k7c6	v
Ostřihomi	Ostřiho	k1gFnPc7	Ostřiho
přivítán	přivítat	k5eAaPmNgInS	přivítat
uherským	uherský	k2eAgMnSc7d1	uherský
králem	král	k1gMnSc7	král
Bélou	Béla	k1gMnSc7	Béla
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Dobré	dobrý	k2eAgInPc1d1	dobrý
vztahy	vztah	k1gInPc1	vztah
obou	dva	k4xCgFnPc6	dva
panovníků	panovník	k1gMnPc2	panovník
byly	být	k5eAaImAgInP	být
stvrzeny	stvrzen	k2eAgInPc1d1	stvrzen
zásnubami	zásnuba	k1gFnPc7	zásnuba
císařova	císařův	k2eAgMnSc2d1	císařův
syna	syn	k1gMnSc2	syn
Fridricha	Fridrich	k1gMnSc2	Fridrich
s	s	k7c7	s
dcerou	dcera	k1gFnSc7	dcera
Bély	Béla	k1gMnSc2	Béla
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Konstancií	Konstancie	k1gFnSc7	Konstancie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
června	červen	k1gInSc2	červen
dorazil	dorazit	k5eAaPmAgMnS	dorazit
císař	císař	k1gMnSc1	císař
už	už	k6eAd1	už
do	do	k7c2	do
Bělehradu	Bělehrad	k1gInSc2	Bělehrad
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
už	už	k9	už
dříve	dříve	k6eAd2	dříve
byzantský	byzantský	k2eAgMnSc1d1	byzantský
císař	císař	k1gMnSc1	císař
Izák	Izák	k1gMnSc1	Izák
přislíbil	přislíbit	k5eAaPmAgMnS	přislíbit
hladký	hladký	k2eAgInSc4d1	hladký
průchod	průchod	k1gInSc4	průchod
křižáckého	křižácký	k2eAgNnSc2d1	křižácké
vojska	vojsko	k1gNnSc2	vojsko
Byzancí	Byzanc	k1gFnPc2	Byzanc
a	a	k8xC	a
také	také	k9	také
dostatečné	dostatečný	k2eAgNnSc4d1	dostatečné
zásobování	zásobování	k1gNnSc4	zásobování
vojska	vojsko	k1gNnSc2	vojsko
během	během	k7c2	během
cesty	cesta	k1gFnSc2	cesta
<g/>
,	,	kIx,	,
výprava	výprava	k1gFnSc1	výprava
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
cestu	cesta	k1gFnSc4	cesta
trpěla	trpět	k5eAaImAgFnS	trpět
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
nezajištěným	zajištěný	k2eNgNnSc7d1	nezajištěné
zásobováním	zásobování	k1gNnSc7	zásobování
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
také	také	k9	také
neustále	neustále	k6eAd1	neustále
napadána	napadán	k2eAgFnSc1d1	napadána
<g/>
.	.	kIx.	.
</s>
<s>
Byzantský	byzantský	k2eAgMnSc1d1	byzantský
císař	císař	k1gMnSc1	císař
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
zajal	zajmout	k5eAaPmAgInS	zajmout
císařské	císařský	k2eAgMnPc4d1	císařský
vyslance	vyslanec	k1gMnPc4	vyslanec
<g/>
,	,	kIx,	,
navázal	navázat	k5eAaPmAgInS	navázat
kontakt	kontakt	k1gInSc4	kontakt
se	s	k7c7	s
sultánem	sultán	k1gMnSc7	sultán
Saladinem	Saladin	k1gMnSc7	Saladin
a	a	k8xC	a
obvinil	obvinit	k5eAaPmAgInS	obvinit
Barbarossu	Barbarossa	k1gMnSc4	Barbarossa
<g/>
,	,	kIx,	,
že	že	k8xS	že
plánuje	plánovat	k5eAaImIp3nS	plánovat
dobýt	dobýt	k5eAaPmF	dobýt
Konstantinopol	Konstantinopol	k1gInSc4	Konstantinopol
a	a	k8xC	a
dosadit	dosadit	k5eAaPmF	dosadit
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
svého	svůj	k3xOyFgMnSc2	svůj
syna	syn	k1gMnSc2	syn
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
této	tento	k3xDgFnSc2	tento
situace	situace	k1gFnSc2	situace
zastavil	zastavit	k5eAaPmAgInS	zastavit
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
Barbarossa	Barbarossa	k1gMnSc1	Barbarossa
další	další	k2eAgInSc4d1	další
postup	postup	k1gInSc4	postup
výpravy	výprava	k1gFnSc2	výprava
<g/>
,	,	kIx,	,
vybudoval	vybudovat	k5eAaPmAgMnS	vybudovat
si	se	k3xPyFc3	se
tábor	tábor	k1gInSc4	tábor
ve	v	k7c6	v
Filippopoli	Filippopole	k1gFnSc6	Filippopole
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Plovdiv	Plovdiv	k1gInSc1	Plovdiv
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
měsících	měsíc	k1gInPc6	měsíc
vyplenil	vyplenit	k5eAaPmAgMnS	vyplenit
velkou	velká	k1gFnSc4	velká
část	část	k1gFnSc4	část
Byzantské	byzantský	k2eAgFnSc2d1	byzantská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
počátkem	počátkem	k7c2	počátkem
listopadu	listopad	k1gInSc2	listopad
přesunul	přesunout	k5eAaPmAgMnS	přesunout
svůj	svůj	k3xOyFgInSc4	svůj
tábor	tábor	k1gInSc4	tábor
do	do	k7c2	do
Adrianopole	Adrianopole	k1gFnSc2	Adrianopole
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Edirne	Edirn	k1gInSc5	Edirn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
vybudoval	vybudovat	k5eAaPmAgInS	vybudovat
zimní	zimní	k2eAgInSc4d1	zimní
tábor	tábor	k1gInSc4	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
přesunem	přesun	k1gInSc7	přesun
blíže	blízce	k6eAd2	blízce
ke	k	k7c3	k
Konstantinopoli	Konstantinopol	k1gInSc3	Konstantinopol
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
zvýšit	zvýšit	k5eAaPmF	zvýšit
tlak	tlak	k1gInSc4	tlak
na	na	k7c4	na
byzantského	byzantský	k2eAgMnSc4d1	byzantský
císaře	císař	k1gMnSc4	císař
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
začínal	začínat	k5eAaImAgMnS	začínat
zvažovat	zvažovat	k5eAaImF	zvažovat
útok	útok	k1gInSc4	útok
na	na	k7c4	na
Konstantinopol	Konstantinopol	k1gInSc4	Konstantinopol
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
roku	rok	k1gInSc2	rok
1190	[number]	k4	1190
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
jeho	jeho	k3xOp3gInSc7	jeho
hlavním	hlavní	k2eAgInSc7d1	hlavní
cílem	cíl	k1gInSc7	cíl
stále	stále	k6eAd1	stále
zůstával	zůstávat	k5eAaImAgInS	zůstávat
úspěch	úspěch	k1gInSc1	úspěch
křížové	křížový	k2eAgFnSc2d1	křížová
výpravy	výprava	k1gFnSc2	výprava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
došlo	dojít	k5eAaPmAgNnS	dojít
také	také	k9	také
k	k	k7c3	k
jednání	jednání	k1gNnSc3	jednání
se	se	k3xPyFc4	se
zástupci	zástupce	k1gMnPc1	zástupce
Bulharů	Bulhar	k1gMnPc2	Bulhar
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
byli	být	k5eAaImAgMnP	být
ochotni	ochoten	k2eAgMnPc1d1	ochoten
Fridricha	Fridrich	k1gMnSc2	Fridrich
při	při	k7c6	při
útoku	útok	k1gInSc6	útok
na	na	k7c4	na
Konstantinopol	Konstantinopol	k1gInSc4	Konstantinopol
podpořit	podpořit	k5eAaPmF	podpořit
<g/>
.	.	kIx.	.
</s>
<s>
Rostoucímu	rostoucí	k2eAgInSc3d1	rostoucí
tlaku	tlak	k1gInSc3	tlak
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
byzantský	byzantský	k2eAgMnSc1d1	byzantský
císař	císař	k1gMnSc1	císař
už	už	k6eAd1	už
dále	daleko	k6eAd2	daleko
odolávat	odolávat	k5eAaImF	odolávat
a	a	k8xC	a
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1190	[number]	k4	1190
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
Barbarossovi	Barbarossa	k1gMnSc3	Barbarossa
dohodu	dohoda	k1gFnSc4	dohoda
a	a	k8xC	a
zavázal	zavázat	k5eAaPmAgMnS	zavázat
se	se	k3xPyFc4	se
plně	plně	k6eAd1	plně
podporovat	podporovat	k5eAaImF	podporovat
křížovou	křížový	k2eAgFnSc4d1	křížová
výpravu	výprava	k1gFnSc4	výprava
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
března	březen	k1gInSc2	březen
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
celé	celý	k2eAgNnSc1d1	celé
vojsko	vojsko	k1gNnSc1	vojsko
konečně	konečně	k6eAd1	konečně
přeplavilo	přeplavit	k5eAaPmAgNnS	přeplavit
přes	přes	k7c4	přes
Hellespont	Hellespont	k1gInSc4	Hellespont
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
přeplavení	přeplavení	k1gNnSc6	přeplavení
do	do	k7c2	do
Anatolie	Anatolie	k1gFnSc2	Anatolie
však	však	k9	však
s	s	k7c7	s
rostoucí	rostoucí	k2eAgFnSc7d1	rostoucí
vzdáleností	vzdálenost	k1gFnSc7	vzdálenost
od	od	k7c2	od
Konstantinopole	Konstantinopol	k1gInSc2	Konstantinopol
slíbená	slíbený	k2eAgFnSc1d1	slíbená
byzantská	byzantský	k2eAgFnSc1d1	byzantská
podpora	podpora	k1gFnSc1	podpora
slábla	slábnout	k5eAaImAgFnS	slábnout
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
byzantská	byzantský	k2eAgFnSc1d1	byzantská
pomoc	pomoc	k1gFnSc1	pomoc
byla	být	k5eAaImAgFnS	být
pochybná	pochybný	k2eAgFnSc1d1	pochybná
<g/>
,	,	kIx,	,
po	po	k7c4	po
opuštění	opuštění	k1gNnSc4	opuštění
byzantského	byzantský	k2eAgNnSc2d1	byzantské
území	území	k1gNnSc2	území
se	se	k3xPyFc4	se
situace	situace	k1gFnSc1	situace
ještě	ještě	k6eAd1	ještě
zhoršila	zhoršit	k5eAaPmAgFnS	zhoršit
<g/>
.	.	kIx.	.
</s>
<s>
Ukázalo	ukázat	k5eAaPmAgNnS	ukázat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
dohody	dohoda	k1gFnPc1	dohoda
dříve	dříve	k6eAd2	dříve
uzavřené	uzavřený	k2eAgInPc4d1	uzavřený
s	s	k7c7	s
Arslanem	Arslan	k1gMnSc7	Arslan
Kiličem	Kilič	k1gMnSc7	Kilič
mají	mít	k5eAaImIp3nP	mít
prakticky	prakticky	k6eAd1	prakticky
nulovou	nulový	k2eAgFnSc4d1	nulová
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
sultán	sultán	k1gMnSc1	sultán
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1188	[number]	k4	1188
<g/>
-	-	kIx~	-
<g/>
1189	[number]	k4	1189
rozdělil	rozdělit	k5eAaPmAgInS	rozdělit
svou	svůj	k3xOyFgFnSc4	svůj
říši	říše	k1gFnSc4	říše
mezi	mezi	k7c7	mezi
devět	devět	k4xCc1	devět
svých	svůj	k3xOyFgMnPc2	svůj
synů	syn	k1gMnPc2	syn
a	a	k8xC	a
ještě	ještě	k9	ještě
i	i	k9	i
další	další	k2eAgMnPc4d1	další
příbuzné	příbuzný	k1gMnPc4	příbuzný
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
rozpoutali	rozpoutat	k5eAaPmAgMnP	rozpoutat
boj	boj	k1gInSc4	boj
<g/>
.	.	kIx.	.
</s>
<s>
Křižáci	křižák	k1gMnPc1	křižák
se	se	k3xPyFc4	se
museli	muset	k5eAaImAgMnP	muset
bránit	bránit	k5eAaImF	bránit
neustálým	neustálý	k2eAgInPc3d1	neustálý
tureckým	turecký	k2eAgInPc3d1	turecký
útokům	útok	k1gInPc3	útok
a	a	k8xC	a
když	když	k8xS	když
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
13	[number]	k4	13
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
sultanátu	sultanát	k1gInSc2	sultanát
Ikonium	Ikonium	k1gNnSc1	Ikonium
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Konya	Kony	k2eAgFnSc1d1	Kony
<g/>
)	)	kIx)	)
postavilo	postavit	k5eAaPmAgNnS	postavit
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
do	do	k7c2	do
cesty	cesta	k1gFnSc2	cesta
silné	silný	k2eAgNnSc1d1	silné
turecké	turecký	k2eAgNnSc1d1	turecké
vojsko	vojsko	k1gNnSc1	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Unaveným	unavený	k2eAgInPc3d1	unavený
křižákům	křižák	k1gInPc3	křižák
strženým	stržený	k2eAgMnSc7d1	stržený
Barbarossovým	Barbarossův	k2eAgInSc7d1	Barbarossův
příkladem	příklad	k1gInSc7	příklad
se	se	k3xPyFc4	se
však	však	k9	však
podařilo	podařit	k5eAaPmAgNnS	podařit
Turky	Turek	k1gMnPc4	Turek
porazit	porazit	k5eAaPmF	porazit
a	a	k8xC	a
Ikonium	Ikonium	k1gNnSc4	Ikonium
dobýt	dobýt	k5eAaPmF	dobýt
<g/>
.	.	kIx.	.
</s>
<s>
Římský	římský	k2eAgMnSc1d1	římský
císař	císař	k1gMnSc1	císař
Fridrich	Fridrich	k1gMnSc1	Fridrich
I.	I.	kA	I.
Barbarossa	Barbarossa	k1gMnSc1	Barbarossa
zemřel	zemřít	k5eAaPmAgMnS	zemřít
10	[number]	k4	10
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1190	[number]	k4	1190
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
třetí	třetí	k4xOgFnSc2	třetí
křížové	křížový	k2eAgFnSc2d1	křížová
výpravy	výprava	k1gFnSc2	výprava
<g/>
.	.	kIx.	.
</s>
<s>
Severně	severně	k6eAd1	severně
od	od	k7c2	od
města	město	k1gNnSc2	město
Silifke	Silifk	k1gFnSc2	Silifk
se	se	k3xPyFc4	se
pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
zkrátit	zkrátit	k5eAaPmF	zkrátit
si	se	k3xPyFc3	se
cestu	cesta	k1gFnSc4	cesta
přechodem	přechod	k1gInSc7	přechod
přes	přes	k7c4	přes
brod	brod	k1gInSc4	brod
řeky	řeka	k1gFnSc2	řeka
Salef	Salef	k1gInSc1	Salef
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
známa	známo	k1gNnSc2	známo
jako	jako	k8xS	jako
Göksu	Göks	k1gInSc2	Göks
<g/>
)	)	kIx)	)
a	a	k8xC	a
při	při	k7c6	při
tomto	tento	k3xDgInSc6	tento
pokusu	pokus	k1gInSc6	pokus
se	se	k3xPyFc4	se
utopil	utopit	k5eAaPmAgMnS	utopit
<g/>
.	.	kIx.	.
</s>
<s>
Císařova	Císařův	k2eAgFnSc1d1	Císařova
smrt	smrt	k1gFnSc1	smrt
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
pro	pro	k7c4	pro
část	část	k1gFnSc4	část
rytířů	rytíř	k1gMnPc2	rytíř
vhodnou	vhodný	k2eAgFnSc7d1	vhodná
záminkou	záminka	k1gFnSc7	záminka
pro	pro	k7c4	pro
ukončení	ukončení	k1gNnSc4	ukončení
výpravy	výprava	k1gFnSc2	výprava
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
menší	malý	k2eAgFnSc1d2	menší
část	část	k1gFnSc1	část
vojska	vojsko	k1gNnSc2	vojsko
vedená	vedený	k2eAgFnSc1d1	vedená
vedená	vedený	k2eAgFnSc1d1	vedená
Barbarossovým	Barbarossův	k2eAgMnSc7d1	Barbarossův
synem	syn	k1gMnSc7	syn
Fridrichem	Fridrich	k1gMnSc7	Fridrich
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
dále	daleko	k6eAd2	daleko
do	do	k7c2	do
Svaté	svatý	k2eAgFnSc2d1	svatá
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
císařova	císařův	k2eAgNnSc2d1	císařovo
mrtvého	mrtvý	k2eAgNnSc2d1	mrtvé
těla	tělo	k1gNnSc2	tělo
byly	být	k5eAaImAgFnP	být
vyjmuty	vyjmut	k2eAgFnPc1d1	vyjmuta
vnitřnosti	vnitřnost	k1gFnPc1	vnitřnost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
pohřbeny	pohřbít	k5eAaPmNgFnP	pohřbít
blízko	blízko	k7c2	blízko
místa	místo	k1gNnSc2	místo
nešťastného	šťastný	k2eNgNnSc2d1	nešťastné
úmrtí	úmrtí	k1gNnSc2	úmrtí
<g/>
,	,	kIx,	,
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Tarsu	Tars	k1gInSc2	Tars
<g/>
.	.	kIx.	.
</s>
<s>
Nabalzamované	nabalzamovaný	k2eAgNnSc1d1	nabalzamované
mrtvé	mrtvý	k2eAgNnSc1d1	mrtvé
tělo	tělo	k1gNnSc1	tělo
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
dále	daleko	k6eAd2	daleko
s	s	k7c7	s
výpravou	výprava	k1gFnSc7	výprava
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
křižáci	křižák	k1gMnPc1	křižák
dorazili	dorazit	k5eAaPmAgMnP	dorazit
do	do	k7c2	do
Antiochie	Antiochie	k1gFnSc2	Antiochie
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
balzamace	balzamace	k1gFnSc1	balzamace
je	být	k5eAaImIp3nS	být
nedostatečná	dostatečný	k2eNgFnSc1d1	nedostatečná
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
bylo	být	k5eAaImAgNnS	být
maso	maso	k1gNnSc1	maso
odděleno	oddělit	k5eAaPmNgNnS	oddělit
od	od	k7c2	od
kostí	kost	k1gFnPc2	kost
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
pohřbeno	pohřbít	k5eAaPmNgNnS	pohřbít
v	v	k7c6	v
Antiochii	Antiochie	k1gFnSc6	Antiochie
<g/>
.	.	kIx.	.
</s>
<s>
Kosti	kost	k1gFnPc1	kost
pak	pak	k6eAd1	pak
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
pohřbeny	pohřbít	k5eAaPmNgInP	pohřbít
v	v	k7c6	v
osvobozeném	osvobozený	k2eAgInSc6d1	osvobozený
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
a	a	k8xC	a
alespoň	alespoň	k9	alespoň
takto	takto	k6eAd1	takto
symbolicky	symbolicky	k6eAd1	symbolicky
měl	mít	k5eAaImAgMnS	mít
Barbarossa	Barbarossa	k1gMnSc1	Barbarossa
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
svého	svůj	k3xOyFgInSc2	svůj
cíle	cíl	k1gInSc2	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
byly	být	k5eAaImAgInP	být
ale	ale	k9	ale
pohřbeny	pohřbít	k5eAaPmNgInP	pohřbít
v	v	k7c6	v
hrobce	hrobka	k1gFnSc6	hrobka
v	v	k7c6	v
Tyru	Tyrus	k1gInSc6	Tyrus
<g/>
.	.	kIx.	.
</s>
<s>
Fridrich	Fridrich	k1gMnSc1	Fridrich
V.	V.	kA	V.
Švábský	švábský	k2eAgInSc1d1	švábský
zemřel	zemřít	k5eAaPmAgMnS	zemřít
roku	rok	k1gInSc2	rok
1191	[number]	k4	1191
v	v	k7c6	v
Akkonu	Akkon	k1gInSc6	Akkon
a	a	k8xC	a
hrob	hrob	k1gInSc1	hrob
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgFnP	být
pohřbeny	pohřbít	k5eAaPmNgFnP	pohřbít
kosti	kost	k1gFnPc1	kost
jeho	on	k3xPp3gMnSc2	on
otce	otec	k1gMnSc2	otec
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
nezvěstný	zvěstný	k2eNgInSc1d1	nezvěstný
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
Fridricha	Fridrich	k1gMnSc2	Fridrich
Barbarosy	Barbarosa	k1gFnSc2	Barbarosa
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
zaujal	zaujmout	k5eAaPmAgInS	zaujmout
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
český	český	k2eAgMnSc1d1	český
kníže	kníže	k1gMnSc1	kníže
Vladislav	Vladislav	k1gMnSc1	Vladislav
II	II	kA	II
<g/>
.	.	kIx.	.
zdrženlivý	zdrženlivý	k2eAgInSc4d1	zdrženlivý
postoj	postoj	k1gInSc4	postoj
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vzájemnému	vzájemný	k2eAgNnSc3d1	vzájemné
smíření	smíření	k1gNnSc3	smíření
došlo	dojít	k5eAaPmAgNnS	dojít
až	až	k9	až
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1156	[number]	k4	1156
na	na	k7c6	na
svatbě	svatba	k1gFnSc6	svatba
Fridricha	Fridrich	k1gMnSc2	Fridrich
Barbarossy	Barbarossa	k1gMnSc2	Barbarossa
s	s	k7c7	s
Beatricí	Beatrice	k1gFnSc7	Beatrice
Burgundskou	burgundský	k2eAgFnSc7d1	burgundská
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
smíření	smíření	k1gNnSc6	smíření
měl	mít	k5eAaImAgInS	mít
velký	velký	k2eAgInSc4d1	velký
podíl	podíl	k1gInSc4	podíl
pražský	pražský	k2eAgMnSc1d1	pražský
biskup	biskup	k1gMnSc1	biskup
Daniel	Daniel	k1gMnSc1	Daniel
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
právě	právě	k6eAd1	právě
český	český	k2eAgMnSc1d1	český
kníže	kníže	k1gMnSc1	kníže
Vladislav	Vladislav	k1gMnSc1	Vladislav
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
v	v	k7c4	v
roli	role	k1gFnSc4	role
mluvčího	mluvčí	k1gMnSc2	mluvčí
shromážděných	shromážděný	k2eAgMnPc2d1	shromážděný
knížat	kníže	k1gMnPc2wR	kníže
zveřejnil	zveřejnit	k5eAaPmAgInS	zveřejnit
na	na	k7c6	na
shromáždění	shromáždění	k1gNnSc6	shromáždění
v	v	k7c6	v
Řezně	Řezno	k1gNnSc6	Řezno
povýšení	povýšení	k1gNnSc2	povýšení
bavorské	bavorský	k2eAgFnPc4d1	bavorská
východní	východní	k2eAgFnPc4d1	východní
marky	marka	k1gFnPc4	marka
na	na	k7c4	na
rakouské	rakouský	k2eAgNnSc4d1	rakouské
vévodství	vévodství	k1gNnSc4	vévodství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
pak	pak	k6eAd1	pak
pomáhal	pomáhat	k5eAaImAgMnS	pomáhat
císaři	císař	k1gMnSc3	císař
v	v	k7c6	v
bojích	boj	k1gInPc6	boj
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
však	však	k9	však
Vladislav	Vladislav	k1gMnSc1	Vladislav
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
Barbarossovi	Barbarossa	k1gMnSc3	Barbarossa
přislíbil	přislíbit	k5eAaPmAgMnS	přislíbit
pomoc	pomoc	k1gFnSc4	pomoc
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
plánovaném	plánovaný	k2eAgInSc6d1	plánovaný
boji	boj	k1gInSc6	boj
se	s	k7c7	s
severoitalskými	severoitalský	k2eAgNnPc7d1	severoitalské
městy	město	k1gNnPc7	město
a	a	k8xC	a
především	především	k9	především
s	s	k7c7	s
Milánem	Milán	k1gInSc7	Milán
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
pomoc	pomoc	k1gFnSc1	pomoc
byla	být	k5eAaImAgFnS	být
odměněna	odměnit	k5eAaPmNgFnS	odměnit
na	na	k7c6	na
sněmu	sněm	k1gInSc6	sněm
v	v	k7c6	v
Řezně	Řezno	k1gNnSc6	Řezno
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1158	[number]	k4	1158
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1158	[number]	k4	1158
české	český	k2eAgNnSc1d1	české
vojsko	vojsko	k1gNnSc1	vojsko
vytáhlo	vytáhnout	k5eAaPmAgNnS	vytáhnout
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
a	a	k8xC	a
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
se	se	k3xPyFc4	se
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
bojů	boj	k1gInPc2	boj
o	o	k7c4	o
Milán	Milán	k1gInSc4	Milán
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
proslavilo	proslavit	k5eAaPmAgNnS	proslavit
hrdinskými	hrdinský	k2eAgInPc7d1	hrdinský
skutky	skutek	k1gInPc7	skutek
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
8	[number]	k4	8
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1158	[number]	k4	1158
v	v	k7c6	v
Miláně	Milán	k1gInSc6	Milán
císař	císař	k1gMnSc1	císař
slavnostně	slavnostně	k6eAd1	slavnostně
znovu	znovu	k6eAd1	znovu
ozdobil	ozdobit	k5eAaPmAgMnS	ozdobit
Vladislava	Vladislav	k1gMnSc4	Vladislav
II	II	kA	II
<g/>
.	.	kIx.	.
korunou	koruna	k1gFnSc7	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Samotná	samotný	k2eAgFnSc1d1	samotná
hodnota	hodnota	k1gFnSc1	hodnota
Vladislavova	Vladislavův	k2eAgInSc2d1	Vladislavův
"	"	kIx"	"
<g/>
královského	královský	k2eAgInSc2d1	královský
titulu	titul	k1gInSc2	titul
<g/>
"	"	kIx"	"
vzbuzuje	vzbuzovat	k5eAaImIp3nS	vzbuzovat
do	do	k7c2	do
dneška	dnešek	k1gInSc2	dnešek
otazníky	otazník	k1gInPc1	otazník
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
chybějící	chybějící	k2eAgFnSc2d1	chybějící
domácí	domácí	k2eAgFnSc2d1	domácí
korunovace	korunovace	k1gFnSc2	korunovace
nevíme	vědět	k5eNaImIp1nP	vědět
také	také	k9	také
nic	nic	k3yNnSc4	nic
o	o	k7c6	o
"	"	kIx"	"
<g/>
pomazání	pomazání	k1gNnSc6	pomazání
na	na	k7c4	na
krále	král	k1gMnSc4	král
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
tvořilo	tvořit	k5eAaImAgNnS	tvořit
důležitou	důležitý	k2eAgFnSc4d1	důležitá
součást	součást	k1gFnSc4	součást
rituálu	rituál	k1gInSc2	rituál
korunovace	korunovace	k1gFnSc2	korunovace
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
udělený	udělený	k2eAgInSc4d1	udělený
"	"	kIx"	"
<g/>
královský	královský	k2eAgInSc4d1	královský
titul	titul	k1gInSc4	titul
<g/>
"	"	kIx"	"
totiž	totiž	k9	totiž
Vladislav	Vladislav	k1gMnSc1	Vladislav
stále	stále	k6eAd1	stále
zůstával	zůstávat	k5eAaImAgMnS	zůstávat
jen	jen	k9	jen
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
říšských	říšský	k2eAgMnPc2d1	říšský
knížat	kníže	k1gMnPc2wR	kníže
<g/>
.	.	kIx.	.
</s>
<s>
Přijetím	přijetí	k1gNnSc7	přijetí
koruny	koruna	k1gFnSc2	koruna
z	z	k7c2	z
rukou	ruka	k1gFnPc2	ruka
císaře	císař	k1gMnSc2	císař
se	se	k3xPyFc4	se
Vladislav	Vladislav	k1gMnSc1	Vladislav
také	také	k9	také
do	do	k7c2	do
budoucna	budoucno	k1gNnSc2	budoucno
jednoznačně	jednoznačně	k6eAd1	jednoznačně
svázal	svázat	k5eAaPmAgInS	svázat
se	s	k7c7	s
štaufskou	štaufský	k2eAgFnSc7d1	štaufská
politikou	politika	k1gFnSc7	politika
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
přinášelo	přinášet	k5eAaImAgNnS	přinášet
mnohé	mnohý	k2eAgInPc4d1	mnohý
problémy	problém	k1gInPc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Zprostředkovaně	zprostředkovaně	k6eAd1	zprostředkovaně
se	se	k3xPyFc4	se
týkal	týkat	k5eAaImAgMnS	týkat
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
i	i	k9	i
spor	spor	k1gInSc4	spor
o	o	k7c4	o
salcburské	salcburský	k2eAgNnSc4d1	salcburské
arcibiskupství	arcibiskupství	k1gNnSc4	arcibiskupství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1168	[number]	k4	1168
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
arcibiskupem	arcibiskup	k1gMnSc7	arcibiskup
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
Vladislava	Vladislav	k1gMnSc2	Vladislav
<g/>
.	.	kIx.	.
</s>
<s>
Barbarossa	Barbarossa	k1gMnSc1	Barbarossa
jeho	jeho	k3xOp3gFnSc4	jeho
volbu	volba	k1gFnSc4	volba
neuznal	uznat	k5eNaPmAgMnS	uznat
a	a	k8xC	a
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1169	[number]	k4	1169
donutil	donutit	k5eAaPmAgMnS	donutit
Vojtěcha	Vojtěch	k1gMnSc4	Vojtěch
k	k	k7c3	k
rezignaci	rezignace	k1gFnSc3	rezignace
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
Vladislav	Vladislav	k1gMnSc1	Vladislav
nabízel	nabízet	k5eAaImAgMnS	nabízet
císaři	císař	k1gMnSc3	císař
za	za	k7c2	za
jeho	jeho	k3xOp3gNnSc2	jeho
uznání	uznání	k1gNnSc2	uznání
větší	veliký	k2eAgFnSc4d2	veliký
peněžní	peněžní	k2eAgFnSc4d1	peněžní
částku	částka	k1gFnSc4	částka
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1165	[number]	k4	1165
přestavěl	přestavět	k5eAaPmAgInS	přestavět
hrad	hrad	k1gInSc4	hrad
v	v	k7c6	v
Chebu	Cheb	k1gInSc6	Cheb
na	na	k7c4	na
falc	falc	k1gFnSc4	falc
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
1179	[number]	k4	1179
vyřešen	vyřešen	k2eAgInSc1d1	vyřešen
spor	spor	k1gInSc1	spor
knížete	kníže	k1gMnSc2	kníže
Bedřicha	Bedřich	k1gMnSc2	Bedřich
s	s	k7c7	s
rakouským	rakouský	k2eAgMnSc7d1	rakouský
vévodou	vévoda	k1gMnSc7	vévoda
Leopoldem	Leopold	k1gMnSc7	Leopold
o	o	k7c4	o
Vitorazsko	Vitorazsko	k1gNnSc4	Vitorazsko
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
císaře	císař	k1gMnSc2	císař
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
tam	tam	k6eAd1	tam
pobýval	pobývat	k5eAaImAgMnS	pobývat
o	o	k7c6	o
Vánocích	Vánoce	k1gFnPc6	Vánoce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1188	[number]	k4	1188
<g/>
.	.	kIx.	.
</s>
<s>
I.	I.	kA	I.
manželka	manželka	k1gFnSc1	manželka
Adéla	Adéla	k1gFnSc1	Adéla
z	z	k7c2	z
Vohburgu	Vohburg	k1gInSc2	Vohburg
bez	bez	k7c2	bez
potomků	potomek	k1gMnPc2	potomek
II	II	kA	II
<g/>
.	.	kIx.	.
manželka	manželka	k1gFnSc1	manželka
Beatrix	Beatrix	k1gInSc1	Beatrix
Burgundská	burgundský	k2eAgFnSc1d1	burgundská
Beatrix	Beatrix	k1gInSc1	Beatrix
(	(	kIx(	(
<g/>
asi	asi	k9	asi
1162	[number]	k4	1162
<g/>
-	-	kIx~	-
<g/>
před	před	k7c7	před
1174	[number]	k4	1174
<g/>
)	)	kIx)	)
Fridrich	Fridrich	k1gMnSc1	Fridrich
(	(	kIx(	(
<g/>
1164	[number]	k4	1164
<g/>
-	-	kIx~	-
<g/>
1169	[number]	k4	1169
<g/>
/	/	kIx~	/
<g/>
1170	[number]	k4	1170
<g/>
)	)	kIx)	)
Jindřich	Jindřich	k1gMnSc1	Jindřich
VI	VI	kA	VI
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1165	[number]	k4	1165
<g/>
-	-	kIx~	-
<g/>
1197	[number]	k4	1197
<g/>
)	)	kIx)	)
∞	∞	k?	∞
Konstancie	Konstancie	k1gFnSc1	Konstancie
Sicilská	sicilský	k2eAgFnSc1d1	sicilská
(	(	kIx(	(
<g/>
1154	[number]	k4	1154
<g/>
-	-	kIx~	-
<g/>
1198	[number]	k4	1198
<g/>
)	)	kIx)	)
Fridrich	Fridrich	k1gMnSc1	Fridrich
(	(	kIx(	(
<g/>
1167	[number]	k4	1167
<g/>
-	-	kIx~	-
<g/>
1191	[number]	k4	1191
<g/>
)	)	kIx)	)
Gisela	Gisela	k1gFnSc1	Gisela
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
1168	[number]	k4	1168
<g/>
-	-	kIx~	-
<g/>
1184	[number]	k4	1184
<g/>
)	)	kIx)	)
Ota	Ota	k1gMnSc1	Ota
(	(	kIx(	(
<g/>
1179	[number]	k4	1179
<g/>
-	-	kIx~	-
<g/>
1200	[number]	k4	1200
<g/>
)	)	kIx)	)
∞	∞	k?	∞
Markéta	Markéta	k1gFnSc1	Markéta
z	z	k7c2	z
Blois	Blois	k1gFnSc2	Blois
(	(	kIx(	(
<g/>
†	†	k?	†
1230	[number]	k4	1230
<g/>
)	)	kIx)	)
Konrád	Konrád	k1gMnSc1	Konrád
(	(	kIx(	(
<g/>
†	†	k?	†
1196	[number]	k4	1196
<g/>
)	)	kIx)	)
Rainald	Rainald	k1gInSc1	Rainald
(	(	kIx(	(
<g/>
*	*	kIx~	*
1173	[number]	k4	1173
<g/>
)	)	kIx)	)
Vilém	Vilém	k1gMnSc1	Vilém
(	(	kIx(	(
<g/>
*	*	kIx~	*
1176	[number]	k4	1176
<g/>
)	)	kIx)	)
Filip	Filip	k1gMnSc1	Filip
(	(	kIx(	(
<g/>
1177	[number]	k4	1177
<g/>
-	-	kIx~	-
<g/>
1208	[number]	k4	1208
<g/>
)	)	kIx)	)
∞	∞	k?	∞
1197	[number]	k4	1197
Irena	Irena	k1gFnSc1	Irena
Angelovna	Angelovna	k1gFnSc1	Angelovna
(	(	kIx(	(
<g/>
1181	[number]	k4	1181
<g/>
-	-	kIx~	-
<g/>
1208	[number]	k4	1208
<g/>
)	)	kIx)	)
Anežka	Anežka	k1gFnSc1	Anežka
(	(	kIx(	(
<g/>
†	†	k?	†
1184	[number]	k4	1184
<g/>
)	)	kIx)	)
</s>
