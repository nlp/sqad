<s>
Králický	králický	k2eAgInSc1d1	králický
Sněžník	Sněžník	k1gInSc1	Sněžník
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
s	s	k7c7	s
krácením	krácení	k1gNnSc7	krácení
kořenové	kořenový	k2eAgFnSc2d1	kořenová
samohlásky	samohláska	k1gFnSc2	samohláska
Kralický	kralický	k2eAgInSc1d1	kralický
Sněžník	Sněžník	k1gInSc1	Sněžník
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bývá	bývat	k5eAaImIp3nS	bývat
považováno	považován	k2eAgNnSc1d1	považováno
někdy	někdy	k6eAd1	někdy
za	za	k7c4	za
nesprávné	správný	k2eNgNnSc4d1	nesprávné
<g/>
,	,	kIx,	,
jindy	jindy	k6eAd1	jindy
za	za	k7c4	za
správné	správný	k2eAgInPc4d1	správný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zastarale	zastarale	k6eAd1	zastarale
také	také	k9	také
Kladský	kladský	k2eAgInSc4d1	kladský
Sněžník	Sněžník	k1gInSc4	Sněžník
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
Śnieżnik	Śnieżnik	k1gInSc1	Śnieżnik
Kłodzki	Kłodzk	k1gFnSc2	Kłodzk
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Glatzer	Glatzer	k1gMnSc1	Glatzer
Schneeberg	Schneeberg	k1gMnSc1	Schneeberg
nebo	nebo	k8xC	nebo
Grulicher	Grulichra	k1gFnPc2	Grulichra
Schneeberg	Schneeberg	k1gMnSc1	Schneeberg
<g/>
,	,	kIx,	,
zastarale	zastarale	k6eAd1	zastarale
také	také	k9	také
Spieglitzer	Spieglitzer	k1gMnSc1	Spieglitzer
Schneeberg	Schneeberg	k1gMnSc1	Schneeberg
nebo	nebo	k8xC	nebo
Grosser	Grosser	k1gMnSc1	Grosser
Schneeberg	Schneeberg	k1gMnSc1	Schneeberg
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
vrchol	vrchol	k1gInSc4	vrchol
(	(	kIx(	(
<g/>
1424	[number]	k4	1424
m	m	kA	m
<g/>
)	)	kIx)	)
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
třetího	třetí	k4xOgNnSc2	třetí
nejvyššího	vysoký	k2eAgNnSc2d3	nejvyšší
pohoří	pohoří	k1gNnSc2	pohoří
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
nacházejícího	nacházející	k2eAgMnSc2d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c4	na
státní	státní	k2eAgFnSc4d1	státní
hranici	hranice	k1gFnSc4	hranice
s	s	k7c7	s
Polskem	Polsko	k1gNnSc7	Polsko
<g/>
,	,	kIx,	,
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
zhruba	zhruba	k6eAd1	zhruba
16	[number]	k4	16
km	km	kA	km
táhnoucí	táhnoucí	k2eAgInPc4d1	táhnoucí
se	se	k3xPyFc4	se
od	od	k7c2	od
města	město	k1gNnSc2	město
Králíky	Králík	k1gMnPc4	Králík
po	po	k7c4	po
Kladské	kladský	k2eAgNnSc4d1	Kladské
sedlo	sedlo	k1gNnSc4	sedlo
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
Králický	králický	k2eAgInSc4d1	králický
Sněžník	Sněžník	k1gInSc4	Sněžník
<g/>
.	.	kIx.	.
</s>
