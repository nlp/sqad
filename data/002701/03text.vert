<s>
Králický	králický	k2eAgInSc1d1	králický
Sněžník	Sněžník	k1gInSc1	Sněžník
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
s	s	k7c7	s
krácením	krácení	k1gNnSc7	krácení
kořenové	kořenový	k2eAgFnSc2d1	kořenová
samohlásky	samohláska	k1gFnSc2	samohláska
Kralický	kralický	k2eAgInSc1d1	kralický
Sněžník	Sněžník	k1gInSc1	Sněžník
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bývá	bývat	k5eAaImIp3nS	bývat
považováno	považován	k2eAgNnSc1d1	považováno
někdy	někdy	k6eAd1	někdy
za	za	k7c4	za
nesprávné	správný	k2eNgNnSc4d1	nesprávné
<g/>
,	,	kIx,	,
jindy	jindy	k6eAd1	jindy
za	za	k7c4	za
správné	správný	k2eAgInPc4d1	správný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zastarale	zastarale	k6eAd1	zastarale
také	také	k9	také
Kladský	kladský	k2eAgInSc4d1	kladský
Sněžník	Sněžník	k1gInSc4	Sněžník
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
Śnieżnik	Śnieżnik	k1gInSc1	Śnieżnik
Kłodzki	Kłodzk	k1gFnSc2	Kłodzk
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Glatzer	Glatzer	k1gMnSc1	Glatzer
Schneeberg	Schneeberg	k1gMnSc1	Schneeberg
nebo	nebo	k8xC	nebo
Grulicher	Grulichra	k1gFnPc2	Grulichra
Schneeberg	Schneeberg	k1gMnSc1	Schneeberg
<g/>
,	,	kIx,	,
zastarale	zastarale	k6eAd1	zastarale
také	také	k9	také
Spieglitzer	Spieglitzer	k1gMnSc1	Spieglitzer
Schneeberg	Schneeberg	k1gMnSc1	Schneeberg
nebo	nebo	k8xC	nebo
Grosser	Grosser	k1gMnSc1	Grosser
Schneeberg	Schneeberg	k1gMnSc1	Schneeberg
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
vrchol	vrchol	k1gInSc4	vrchol
(	(	kIx(	(
<g/>
1424	[number]	k4	1424
m	m	kA	m
<g/>
)	)	kIx)	)
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
třetího	třetí	k4xOgNnSc2	třetí
nejvyššího	vysoký	k2eAgNnSc2d3	nejvyšší
pohoří	pohoří	k1gNnSc2	pohoří
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
nacházejícího	nacházející	k2eAgMnSc2d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c4	na
státní	státní	k2eAgFnSc4d1	státní
hranici	hranice	k1gFnSc4	hranice
s	s	k7c7	s
Polskem	Polsko	k1gNnSc7	Polsko
<g/>
,	,	kIx,	,
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
zhruba	zhruba	k6eAd1	zhruba
16	[number]	k4	16
km	km	kA	km
táhnoucí	táhnoucí	k2eAgInPc4d1	táhnoucí
se	se	k3xPyFc4	se
od	od	k7c2	od
města	město	k1gNnSc2	město
Králíky	Králík	k1gMnPc4	Králík
po	po	k7c4	po
Kladské	kladský	k2eAgNnSc4d1	Kladské
sedlo	sedlo	k1gNnSc4	sedlo
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
Králický	králický	k2eAgInSc4d1	králický
Sněžník	Sněžník	k1gInSc4	Sněžník
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
vrchol	vrchol	k1gInSc4	vrchol
Koruny	koruna	k1gFnSc2	koruna
hor	hora	k1gFnPc2	hora
Polska	Polsko	k1gNnSc2	Polsko
a	a	k8xC	a
o	o	k7c4	o
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
bod	bod	k1gInSc4	bod
okresu	okres	k1gInSc2	okres
Ústí	ústit	k5eAaImIp3nP	ústit
nad	nad	k7c7	nad
Orlicí	Orlice	k1gFnSc7	Orlice
i	i	k8xC	i
celého	celý	k2eAgInSc2d1	celý
Pardubického	pardubický	k2eAgInSc2d1	pardubický
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Vrcholový	vrcholový	k2eAgInSc1d1	vrcholový
geodetický	geodetický	k2eAgInSc1d1	geodetický
bod	bod	k1gInSc1	bod
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
souřadnicích	souřadnice	k1gFnPc6	souřadnice
50	[number]	k4	50
<g/>
°	°	k?	°
<g/>
12	[number]	k4	12
<g/>
'	'	kIx"	'
<g/>
26,675	[number]	k4	26,675
<g/>
0	[number]	k4	0
<g/>
''	''	k?	''
s.	s.	k?	s.
š.	š.	k?	š.
<g/>
,	,	kIx,	,
16	[number]	k4	16
<g/>
°	°	k?	°
<g/>
50	[number]	k4	50
<g/>
'	'	kIx"	'
<g/>
50,702	[number]	k4	50,702
<g/>
5	[number]	k4	5
<g/>
''	''	k?	''
v.	v.	k?	v.
d.	d.	k?	d.
Z	z	k7c2	z
vrcholové	vrcholový	k2eAgFnSc2d1	vrcholová
kupole	kupole	k1gFnSc2	kupole
výrazně	výrazně	k6eAd1	výrazně
modelované	modelovaný	k2eAgFnSc2d1	modelovaná
mrazovým	mrazový	k2eAgNnSc7d1	mrazové
zvětráváním	zvětrávání	k1gNnSc7	zvětrávání
vybíhá	vybíhat	k5eAaImIp3nS	vybíhat
pět	pět	k4xCc1	pět
dílčích	dílčí	k2eAgFnPc2d1	dílčí
rozsoch	rozsocha	k1gFnPc2	rozsocha
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
hory	hora	k1gFnSc2	hora
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
z	z	k7c2	z
dlouhého	dlouhý	k2eAgNnSc2d1	dlouhé
zimního	zimní	k2eAgNnSc2d1	zimní
období	období	k1gNnSc2	období
-	-	kIx~	-
sněhová	sněhový	k2eAgFnSc1d1	sněhová
pokrývka	pokrývka	k1gFnSc1	pokrývka
vydrží	vydržet	k5eAaPmIp3nS	vydržet
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
až	až	k9	až
8	[number]	k4	8
měsíců	měsíc	k1gInPc2	měsíc
v	v	k7c6	v
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
desítek	desítka	k1gFnPc2	desítka
metrů	metr	k1gInPc2	metr
pod	pod	k7c7	pod
vrcholem	vrchol	k1gInSc7	vrchol
na	na	k7c6	na
jižním	jižní	k2eAgInSc6d1	jižní
svahu	svah	k1gInSc6	svah
hory	hora	k1gFnSc2	hora
pramení	pramenit	k5eAaImIp3nS	pramenit
Morava	Morava	k1gFnSc1	Morava
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
vyvinut	vyvinout	k5eAaPmNgInS	vyvinout
kar	kar	k1gInSc4	kar
s	s	k7c7	s
lavinovou	lavinový	k2eAgFnSc7d1	lavinová
drahou	draha	k1gFnSc7	draha
<g/>
.	.	kIx.	.
</s>
<s>
Padající	padající	k2eAgFnPc1d1	padající
laviny	lavina	k1gFnPc1	lavina
zde	zde	k6eAd1	zde
zabraňují	zabraňovat	k5eAaImIp3nP	zabraňovat
vzniku	vznik	k1gInSc2	vznik
lesa	les	k1gInSc2	les
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
klín	klín	k1gInSc1	klín
bezlesí	bezlesí	k1gNnSc2	bezlesí
sahá	sahat	k5eAaImIp3nS	sahat
hluboko	hluboko	k6eAd1	hluboko
do	do	k7c2	do
pásma	pásmo	k1gNnSc2	pásmo
smrčin	smrčina	k1gFnPc2	smrčina
<g/>
.	.	kIx.	.
</s>
<s>
Niže	Niže	k6eAd1	Niže
pod	pod	k7c7	pod
vrcholem	vrchol	k1gInSc7	vrchol
(	(	kIx(	(
<g/>
trojmezní	trojmezní	k2eAgInSc1d1	trojmezní
kámen	kámen	k1gInSc1	kámen
<g/>
)	)	kIx)	)
počíná	počínat	k5eAaImIp3nS	počínat
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
končí	končit	k5eAaImIp3nS	končit
<g/>
"	"	kIx"	"
historická	historický	k2eAgFnSc1d1	historická
česko-moravská	českooravský	k2eAgFnSc1d1	česko-moravská
hranice	hranice	k1gFnSc1	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Hora	Hora	k1gMnSc1	Hora
Králický	králický	k2eAgInSc1d1	králický
Sněžník	Sněžník	k1gInSc1	Sněžník
tvoří	tvořit	k5eAaImIp3nS	tvořit
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
pohoří	pohořet	k5eAaPmIp3nS	pohořet
samostatný	samostatný	k2eAgInSc1d1	samostatný
geomorfologický	geomorfologický	k2eAgInSc1d1	geomorfologický
okrsek	okrsek	k1gInSc1	okrsek
zvaný	zvaný	k2eAgInSc1d1	zvaný
Hornomoravská	hornomoravský	k2eAgFnSc1d1	hornomoravský
hornatina	hornatina	k1gFnSc1	hornatina
<g/>
.	.	kIx.	.
</s>
<s>
Hranice	hranice	k1gFnSc1	hranice
se	s	k7c7	s
sousedními	sousední	k2eAgInPc7d1	sousední
okrsky	okrsek	k1gInPc7	okrsek
je	být	k5eAaImIp3nS	být
vymezena	vymezen	k2eAgFnSc1d1	vymezena
přilehlými	přilehlý	k2eAgNnPc7d1	přilehlé
údolími	údolí	k1gNnPc7	údolí
a	a	k8xC	a
sedly	sedlo	k1gNnPc7	sedlo
<g/>
.	.	kIx.	.
</s>
<s>
Sedlo	sedlo	k1gNnSc1	sedlo
mezi	mezi	k7c7	mezi
Králickým	králický	k2eAgInSc7d1	králický
a	a	k8xC	a
Malým	malý	k2eAgInSc7d1	malý
Sněžníkem	Sněžník	k1gInSc7	Sněžník
je	být	k5eAaImIp3nS	být
hranicí	hranice	k1gFnSc7	hranice
s	s	k7c7	s
Malosněžnickým	Malosněžnický	k2eAgInSc7d1	Malosněžnický
hřbetem	hřbet	k1gInSc7	hřbet
<g/>
,	,	kIx,	,
sedlo	sedlo	k1gNnSc4	sedlo
mezi	mezi	k7c7	mezi
Králickým	králický	k2eAgInSc7d1	králický
Sněžníkem	Sněžník	k1gInSc7	Sněžník
a	a	k8xC	a
Stříbrnickou	stříbrnický	k2eAgFnSc7d1	Stříbrnická
je	být	k5eAaImIp3nS	být
hranicí	hranice	k1gFnSc7	hranice
s	s	k7c7	s
Podbělským	Podbělský	k2eAgInSc7d1	Podbělský
hřbetem	hřbet	k1gInSc7	hřbet
<g/>
.	.	kIx.	.
</s>
<s>
Hora	hora	k1gFnSc1	hora
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
hlavním	hlavní	k2eAgNnSc6d1	hlavní
evropském	evropský	k2eAgNnSc6d1	Evropské
rozvodí	rozvodí	k1gNnSc6	rozvodí
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	řeka	k1gFnSc1	řeka
Morava	Morava	k1gFnSc1	Morava
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
několika	několik	k4yIc7	několik
přítoky	přítok	k1gInPc7	přítok
odvádí	odvádět	k5eAaImIp3nP	odvádět
vody	voda	k1gFnSc2	voda
z	z	k7c2	z
jižní	jižní	k2eAgFnSc2d1	jižní
části	část	k1gFnSc2	část
hory	hora	k1gFnSc2	hora
do	do	k7c2	do
Černého	Černého	k2eAgNnSc2d1	Černého
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgFnSc1d1	severní
(	(	kIx(	(
<g/>
polská	polský	k2eAgFnSc1d1	polská
<g/>
)	)	kIx)	)
část	část	k1gFnSc1	část
hory	hora	k1gFnSc2	hora
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
úmoří	úmoří	k1gNnSc2	úmoří
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
hory	hora	k1gFnSc2	hora
pramení	pramenit	k5eAaImIp3nS	pramenit
několik	několik	k4yIc1	několik
potoků	potok	k1gInPc2	potok
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Kamienica	Kamienica	k1gFnSc1	Kamienica
<g/>
,	,	kIx,	,
Kleśnica	Kleśnica	k1gFnSc1	Kleśnica
<g/>
,	,	kIx,	,
Wilczka	Wilczka	k1gFnSc1	Wilczka
aj.	aj.	kA	aj.
Horní	horní	k2eAgFnSc1d1	horní
partie	partie	k1gFnSc1	partie
Králického	králický	k2eAgInSc2d1	králický
Sněžníku	Sněžník	k1gInSc2	Sněžník
(	(	kIx(	(
<g/>
nad	nad	k7c4	nad
1350	[number]	k4	1350
m	m	kA	m
n.	n.	k?	n.
<g/>
m.	m.	k?	m.
<g/>
,	,	kIx,	,
v	v	k7c6	v
lavinové	lavinový	k2eAgFnSc6d1	lavinová
dráze	dráha	k1gFnSc6	dráha
i	i	k9	i
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
tvoří	tvořit	k5eAaImIp3nP	tvořit
hole	hole	k6eAd1	hole
s	s	k7c7	s
typickou	typický	k2eAgFnSc7d1	typická
subalpínskou	subalpínský	k2eAgFnSc7d1	subalpínská
vegetací	vegetace	k1gFnSc7	vegetace
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
500	[number]	k4	500
m	m	kA	m
od	od	k7c2	od
vrcholu	vrchol	k1gInSc2	vrchol
se	se	k3xPyFc4	se
nalézají	nalézat	k5eAaImIp3nP	nalézat
Vlaštovčí	vlaštovčí	k2eAgInPc1d1	vlaštovčí
kameny	kámen	k1gInPc1	kámen
-	-	kIx~	-
soustava	soustava	k1gFnSc1	soustava
skalních	skalní	k2eAgInPc2d1	skalní
srubů	srub	k1gInPc2	srub
s	s	k7c7	s
kamenným	kamenný	k2eAgNnSc7d1	kamenné
mořem	moře	k1gNnSc7	moře
na	na	k7c6	na
úpatí	úpatí	k1gNnSc6	úpatí
<g/>
.	.	kIx.	.
</s>
<s>
Nižší	nízký	k2eAgFnSc1d2	nižší
partie	partie	k1gFnSc1	partie
hory	hora	k1gFnSc2	hora
(	(	kIx(	(
<g/>
pod	pod	k7c4	pod
1350	[number]	k4	1350
m	m	kA	m
n.	n.	k?	n.
<g/>
m.	m.	k?	m.
<g/>
)	)	kIx)	)
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
převážně	převážně	k6eAd1	převážně
horské	horský	k2eAgFnPc1d1	horská
třtinové	třtinový	k2eAgFnPc1d1	třtinová
smrčiny	smrčina	k1gFnPc1	smrčina
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
o	o	k7c4	o
vegetaci	vegetace	k1gFnSc4	vegetace
a	a	k8xC	a
zoologii	zoologie	k1gFnSc4	zoologie
hory	hora	k1gFnSc2	hora
viz	vidět	k5eAaImRp2nS	vidět
NPR	NPR	kA	NPR
Králický	králický	k2eAgInSc4d1	králický
Sněžník	Sněžník	k1gInSc4	Sněžník
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
Králického	králický	k2eAgInSc2d1	králický
Sněžníku	Sněžník	k1gInSc2	Sněžník
nejsou	být	k5eNaImIp3nP	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
žádné	žádný	k3yNgFnSc2	žádný
stavby	stavba	k1gFnSc2	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
samém	samý	k3xTgInSc6	samý
vrcholu	vrchol	k1gInSc6	vrchol
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
stála	stát	k5eAaImAgFnS	stát
rozhledna	rozhledna	k1gFnSc1	rozhledna
(	(	kIx(	(
<g/>
postavena	postaven	k2eAgFnSc1d1	postavena
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1899	[number]	k4	1899
<g/>
)	)	kIx)	)
a	a	k8xC	a
několik	několik	k4yIc4	několik
set	sto	k4xCgNnPc2	sto
metrů	metr	k1gInPc2	metr
pod	pod	k7c7	pod
vrcholem	vrchol	k1gInSc7	vrchol
také	také	k6eAd1	také
Lichtenštejnova	Lichtenštejnův	k2eAgFnSc1d1	Lichtenštejnova
chata	chata	k1gFnSc1	chata
(	(	kIx(	(
<g/>
postavena	postaven	k2eAgFnSc1d1	postavena
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
stavby	stavba	k1gFnPc1	stavba
však	však	k9	však
byly	být	k5eAaImAgFnP	být
začátkem	začátkem	k7c2	začátkem
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
zbořeny	zbořit	k5eAaPmNgInP	zbořit
a	a	k8xC	a
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
zde	zde	k6eAd1	zde
spatříme	spatřit	k5eAaPmIp1nP	spatřit
jen	jen	k9	jen
ruiny	ruina	k1gFnPc1	ruina
rozhledny	rozhledna	k1gFnPc1	rozhledna
(	(	kIx(	(
<g/>
pahorek	pahorek	k1gInSc1	pahorek
kamenů	kámen	k1gInPc2	kámen
<g/>
)	)	kIx)	)
a	a	k8xC	a
základy	základ	k1gInPc4	základ
boudy	bouda	k1gFnSc2	bouda
<g/>
.	.	kIx.	.
</s>
<s>
Nalézá	nalézat	k5eAaImIp3nS	nalézat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
trojmezní	trojmezní	k2eAgInSc1d1	trojmezní
kámen	kámen	k1gInSc1	kámen
Českého	český	k2eAgNnSc2d1	české
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
Moravského	moravský	k2eAgNnSc2d1	Moravské
markrabství	markrabství	k1gNnSc2	markrabství
a	a	k8xC	a
Kladského	kladský	k2eAgNnSc2d1	Kladské
hrabství	hrabství	k1gNnSc2	hrabství
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
kousek	kousek	k1gInSc1	kousek
pod	pod	k7c7	pod
vrcholem	vrchol	k1gInSc7	vrchol
<g/>
,	,	kIx,	,
a	a	k8xC	a
u	u	k7c2	u
základů	základ	k1gInPc2	základ
zbořené	zbořený	k2eAgFnSc2d1	zbořená
chaty	chata	k1gFnSc2	chata
je	být	k5eAaImIp3nS	být
soška	soška	k1gFnSc1	soška
slůněte	slůně	k1gNnSc2	slůně
<g/>
.	.	kIx.	.
</s>
<s>
Žulová	žulový	k2eAgFnSc1d1	Žulová
plastika	plastika	k1gFnSc1	plastika
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
roku	rok	k1gInSc2	rok
1932	[number]	k4	1932
jako	jako	k8xC	jako
upomínka	upomínka	k1gFnSc1	upomínka
na	na	k7c4	na
desáté	desátý	k4xOgNnSc4	desátý
výročí	výročí	k1gNnSc4	výročí
vzniku	vznik	k1gInSc2	vznik
uměleckého	umělecký	k2eAgInSc2d1	umělecký
spolku	spolek	k1gInSc2	spolek
Jescher	Jeschra	k1gFnPc2	Jeschra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
o	o	k7c4	o
znovupostavení	znovupostavení	k1gNnSc4	znovupostavení
rozhledny	rozhledna	k1gFnSc2	rozhledna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
polské	polský	k2eAgFnSc6d1	polská
straně	strana	k1gFnSc6	strana
hory	hora	k1gFnSc2	hora
(	(	kIx(	(
<g/>
nikoliv	nikoliv	k9	nikoliv
však	však	k9	však
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
<g/>
)	)	kIx)	)
stojí	stát	k5eAaImIp3nS	stát
bouda	bouda	k1gFnSc1	bouda
zvaná	zvaný	k2eAgFnSc1d1	zvaná
Schronisko	Schronisko	k1gNnSc4	Schronisko
PTTK	PTTK	kA	PTTK
"	"	kIx"	"
<g/>
Na	na	k7c6	na
Śnieżniku	Śnieżnik	k1gInSc6	Śnieżnik
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
české	český	k2eAgFnSc6d1	Česká
straně	strana	k1gFnSc6	strana
stojí	stát	k5eAaImIp3nS	stát
v	v	k7c6	v
nižších	nízký	k2eAgFnPc6d2	nižší
polohách	poloha	k1gFnPc6	poloha
hory	hora	k1gFnSc2	hora
2	[number]	k4	2
malé	malý	k2eAgFnSc2d1	malá
dřevěné	dřevěný	k2eAgFnSc2d1	dřevěná
boudy	bouda	k1gFnSc2	bouda
Sněžná	sněžný	k2eAgFnSc1d1	sněžná
chata	chata	k1gFnSc1	chata
a	a	k8xC	a
Františkova	Františkův	k2eAgFnSc1d1	Františkova
chata	chata	k1gFnSc1	chata
<g/>
.	.	kIx.	.
</s>
<s>
Níže	nízce	k6eAd2	nízce
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
890	[number]	k4	890
<g/>
m	m	kA	m
n.	n.	k?	n.
<g/>
m.	m.	k?	m.
byla	být	k5eAaImAgFnS	být
postavena	postavit	k5eAaPmNgFnS	postavit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
panem	pan	k1gMnSc7	pan
Jungmannem	Jungmann	k1gMnSc7	Jungmann
Chata	chata	k1gFnSc1	chata
Návrší	návrší	k1gNnPc1	návrší
<g/>
,	,	kIx,	,
původní	původní	k2eAgInSc1d1	původní
název	název	k1gInSc1	název
Sennhüttenbaude	Sennhüttenbaud	k1gInSc5	Sennhüttenbaud
(	(	kIx(	(
<g/>
U	u	k7c2	u
salašních	salašní	k2eAgFnPc2d1	salašní
bud	bouda	k1gFnPc2	bouda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
česká	český	k2eAgFnSc1d1	Česká
část	část	k1gFnSc1	část
hory	hora	k1gFnSc2	hora
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
NPR	NPR	kA	NPR
Králický	králický	k2eAgInSc1d1	králický
Sněžník	Sněžník	k1gInSc1	Sněžník
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
1694,67	[number]	k4	1694,67
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
součástí	součást	k1gFnSc7	součást
navrhované	navrhovaný	k2eAgFnSc2d1	navrhovaná
evropsky	evropsky	k6eAd1	evropsky
významné	významný	k2eAgFnSc2d1	významná
lokality	lokalita	k1gFnSc2	lokalita
Králický	králický	k2eAgInSc1d1	králický
Sněžník	Sněžník	k1gInSc1	Sněžník
soustavy	soustava	k1gFnSc2	soustava
Natury	Natura	k1gFnSc2	Natura
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
hora	hora	k1gFnSc1	hora
nepatří	patřit	k5eNaImIp3nS	patřit
do	do	k7c2	do
Ptačí	ptačí	k2eAgFnSc2d1	ptačí
oblasti	oblast	k1gFnSc2	oblast
Králický	králický	k2eAgInSc1d1	králický
Sněžník	Sněžník	k1gInSc1	Sněžník
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
polské	polský	k2eAgFnSc6d1	polská
straně	strana	k1gFnSc6	strana
je	být	k5eAaImIp3nS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
rezerwat	rezerwat	k1gInSc1	rezerwat
przyrody	przyroda	k1gFnSc2	przyroda
Śnieżnik	Śnieżnik	k1gMnSc1	Śnieżnik
Kłodzki	Kłodzk	k1gFnSc2	Kłodzk
a	a	k8xC	a
také	také	k9	také
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
navržena	navržen	k2eAgFnSc1d1	navržena
evropsky	evropsky	k6eAd1	evropsky
významná	významný	k2eAgFnSc1d1	významná
lokalita	lokalita	k1gFnSc1	lokalita
Natury	Natura	k1gFnSc2	Natura
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Rozhledna	rozhledna	k1gFnSc1	rozhledna
na	na	k7c6	na
Králickém	králický	k2eAgInSc6d1	králický
Sněžníku	Sněžník	k1gInSc6	Sněžník
Schronisko	Schronisko	k1gNnSc1	Schronisko
PTTK	PTTK	kA	PTTK
"	"	kIx"	"
<g/>
Na	na	k7c6	na
Śnieżniku	Śnieżnik	k1gInSc6	Śnieżnik
<g/>
"	"	kIx"	"
Lichtenštejnova	Lichtenštejnův	k2eAgFnSc1d1	Lichtenštejnova
chata	chata	k1gFnSc1	chata
na	na	k7c6	na
Sněžníku	Sněžník	k1gInSc6	Sněžník
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Králický	králický	k2eAgInSc4d1	králický
Sněžník	Sněžník	k1gInSc4	Sněžník
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
NPR	NPR	kA	NPR
Králický	králický	k2eAgInSc4d1	králický
Sněžník	Sněžník	k1gInSc4	Sněžník
na	na	k7c4	na
Sneznik	Sneznik	k1gInSc4	Sneznik
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
NPR	NPR	kA	NPR
Králický	králický	k2eAgInSc4d1	králický
Sněžník	Sněžník	k1gInSc4	Sněžník
na	na	k7c6	na
Env	Env	k1gFnSc6	Env
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
galerie	galerie	k1gFnSc2	galerie
fotografií	fotografia	k1gFnPc2	fotografia
z	z	k7c2	z
NPR	NPR	kA	NPR
Králický	králický	k2eAgInSc1d1	králický
Sněžník	Sněžník	k1gInSc1	Sněžník
Geodetické	geodetický	k2eAgInPc1d1	geodetický
údaje	údaj	k1gInPc1	údaj
trigonometrického	trigonometrický	k2eAgInSc2d1	trigonometrický
bodu	bod	k1gInSc2	bod
Popis	popis	k1gInSc1	popis
turistického	turistický	k2eAgInSc2d1	turistický
přechodu	přechod	k1gInSc2	přechod
masivu	masiv	k1gInSc2	masiv
Králického	králický	k2eAgInSc2d1	králický
Sněžníku	Sněžník	k1gInSc2	Sněžník
</s>
