<p>
<s>
Václav	Václav	k1gMnSc1	Václav
Kural	Kural	k1gMnSc1	Kural
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1928	[number]	k4	1928
Plzeň	Plzeň	k1gFnSc1	Plzeň
–	–	k?	–
25	[number]	k4	25
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2011	[number]	k4	2011
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
historik	historik	k1gMnSc1	historik
<g/>
.	.	kIx.	.
</s>
<s>
Odborně	odborně	k6eAd1	odborně
se	se	k3xPyFc4	se
zaměřoval	zaměřovat	k5eAaImAgMnS	zaměřovat
na	na	k7c4	na
česko-německé	českoěmecký	k2eAgInPc4d1	česko-německý
vztahy	vztah	k1gInPc4	vztah
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
protifašistický	protifašistický	k2eAgInSc1d1	protifašistický
odboj	odboj	k1gInSc1	odboj
a	a	k8xC	a
pražské	pražský	k2eAgNnSc1d1	Pražské
jaro	jaro	k1gNnSc1	jaro
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
absolutoriu	absolutorium	k1gNnSc6	absolutorium
Vojenské	vojenský	k2eAgFnSc2d1	vojenská
politické	politický	k2eAgFnSc2d1	politická
akademie	akademie	k1gFnSc2	akademie
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
do	do	k7c2	do
Vojenského	vojenský	k2eAgInSc2d1	vojenský
historického	historický	k2eAgInSc2d1	historický
ústavu	ústav	k1gInSc2	ústav
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
zastával	zastávat	k5eAaImAgInS	zastávat
post	post	k1gInSc1	post
výkonného	výkonný	k2eAgMnSc2d1	výkonný
místopředsedy	místopředseda	k1gMnSc2	místopředseda
pro	pro	k7c4	pro
ve	v	k7c6	v
Výboru	výbor	k1gInSc6	výbor
pro	pro	k7c4	pro
dějiny	dějiny	k1gFnPc4	dějiny
protifašistického	protifašistický	k2eAgInSc2d1	protifašistický
odboje	odboj	k1gInSc2	odboj
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
také	také	k9	také
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
Janem	Jan	k1gMnSc7	Jan
Kuklíkem	kuklík	k1gMnSc7	kuklík
<g/>
,	,	kIx,	,
Robertem	Robert	k1gMnSc7	Robert
Kvačkem	Kvaček	k1gMnSc7	Kvaček
<g/>
,	,	kIx,	,
Josefem	Josef	k1gMnSc7	Josef
Novotným	Novotný	k1gMnSc7	Novotný
<g/>
,	,	kIx,	,
Janem	Jan	k1gMnSc7	Jan
Křenem	křen	k1gInSc7	křen
a	a	k8xC	a
Alenou	Alena	k1gFnSc7	Alena
Hájkovou	Hájková	k1gFnSc7	Hájková
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
angažoval	angažovat	k5eAaBmAgInS	angažovat
v	v	k7c6	v
obrodném	obrodný	k2eAgInSc6d1	obrodný
procesu	proces	k1gInSc6	proces
tzv.	tzv.	kA	tzv.
pražského	pražský	k2eAgNnSc2d1	Pražské
jara	jaro	k1gNnSc2	jaro
<g/>
,	,	kIx,	,
musel	muset	k5eAaImAgMnS	muset
po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
normalizace	normalizace	k1gFnSc2	normalizace
akademickou	akademický	k2eAgFnSc4d1	akademická
dráhu	dráha	k1gFnSc4	dráha
opustit	opustit	k5eAaPmF	opustit
a	a	k8xC	a
následujících	následující	k2eAgNnPc2d1	následující
dvacet	dvacet	k4xCc4	dvacet
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
živil	živit	k5eAaImAgMnS	živit
jako	jako	k9	jako
čerpač	čerpač	k1gMnSc1	čerpač
a	a	k8xC	a
topič	topič	k1gMnSc1	topič
<g/>
.	.	kIx.	.
</s>
<s>
Publikoval	publikovat	k5eAaBmAgMnS	publikovat
v	v	k7c6	v
samizdatu	samizdat	k1gInSc6	samizdat
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
díky	díky	k7c3	díky
intervenci	intervence	k1gFnSc3	intervence
západoněmeckých	západoněmecký	k2eAgMnPc2d1	západoněmecký
politiků	politik	k1gMnPc2	politik
uskutečnit	uskutečnit	k5eAaPmF	uskutečnit
jeho	jeho	k3xOp3gNnSc4	jeho
hostování	hostování	k1gNnSc4	hostování
na	na	k7c6	na
Univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Brémách	Brémy	k1gFnPc6	Brémy
<g/>
.	.	kIx.	.
</s>
<s>
Patřil	patřit	k5eAaImAgMnS	patřit
k	k	k7c3	k
čelným	čelný	k2eAgMnPc3d1	čelný
představitelům	představitel	k1gMnPc3	představitel
opozičního	opoziční	k2eAgInSc2d1	opoziční
Klubu	klub	k1gInSc2	klub
Obroda	obroda	k1gFnSc1	obroda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
vedoucí	vedoucí	k1gMnSc1	vedoucí
pracovník	pracovník	k1gMnSc1	pracovník
v	v	k7c6	v
Ústavu	ústav	k1gInSc6	ústav
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
vztahů	vztah	k1gInPc2	vztah
při	při	k7c6	při
Ministerstvu	ministerstvo	k1gNnSc6	ministerstvo
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
<g/>
,	,	kIx,	,
zasedal	zasedat	k5eAaImAgInS	zasedat
v	v	k7c6	v
Komisi	komise	k1gFnSc6	komise
vlády	vláda	k1gFnSc2	vláda
ČSFR	ČSFR	kA	ČSFR
pro	pro	k7c4	pro
analýzu	analýza	k1gFnSc4	analýza
období	období	k1gNnPc2	období
1967	[number]	k4	1967
<g/>
–	–	k?	–
<g/>
1970	[number]	k4	1970
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
Česko-německé	českoěmecký	k2eAgFnSc2d1	česko-německá
komise	komise	k1gFnSc2	komise
historiků	historik	k1gMnPc2	historik
<g/>
.	.	kIx.	.
</s>
<s>
Též	též	k6eAd1	též
se	se	k3xPyFc4	se
angažoval	angažovat	k5eAaBmAgMnS	angažovat
jako	jako	k9	jako
předseda	předseda	k1gMnSc1	předseda
Kruhu	kruh	k1gInSc2	kruh
občanů	občan	k1gMnPc2	občan
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
vyhnaných	vyhnaný	k2eAgMnPc2d1	vyhnaný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
z	z	k7c2	z
pohraničí	pohraničí	k1gNnSc2	pohraničí
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
práci	práce	k1gFnSc4	práce
v	v	k7c6	v
žánru	žánr	k1gInSc6	žánr
literatury	literatura	k1gFnSc2	literatura
faktu	fakt	k1gInSc2	fakt
obdržel	obdržet	k5eAaPmAgMnS	obdržet
Cenu	cena	k1gFnSc4	cena
Miroslava	Miroslav	k1gMnSc2	Miroslav
Ivanova	Ivanův	k2eAgMnSc2d1	Ivanův
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
a	a	k8xC	a
jako	jako	k9	jako
člen	člen	k1gInSc1	člen
kolektivu	kolektiv	k1gInSc2	kolektiv
autorů	autor	k1gMnPc2	autor
knihy	kniha	k1gFnSc2	kniha
Rozumět	rozumět	k5eAaImF	rozumět
dějinám	dějiny	k1gFnPc3	dějiny
je	být	k5eAaImIp3nS	být
držitelem	držitel	k1gMnSc7	držitel
též	též	k9	též
Ceny	cena	k1gFnPc1	cena
Egona	Egon	k1gMnSc2	Egon
Erwina	Erwin	k1gMnSc2	Erwin
Kische	Kisch	k1gMnSc2	Kisch
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výběr	výběr	k1gInSc1	výběr
z	z	k7c2	z
díla	dílo	k1gNnSc2	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Integration	Integration	k1gInSc1	Integration
oder	odra	k1gFnPc2	odra
Ausgrenzung	Ausgrenzung	k1gInSc1	Ausgrenzung
<g/>
,	,	kIx,	,
Deutschen	Deutschen	k2eAgInSc1d1	Deutschen
und	und	k?	und
Tschechen	Tschechen	k2eAgInSc1d1	Tschechen
1890	[number]	k4	1890
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Bremen	Bremen	k1gInSc1	Bremen
:	:	kIx,	:
Donat	Donat	k1gInSc1	Donat
und	und	k?	und
Temmen	Temmen	k1gInSc1	Temmen
<g/>
,	,	kIx,	,
1986	[number]	k4	1986
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
s	s	k7c7	s
J.	J.	kA	J.
Křenem	křen	k1gInSc7	křen
a	a	k8xC	a
D.	D.	kA	D.
Brandesem	Brandes	k1gInSc7	Brandes
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Konflikt	konflikt	k1gInSc1	konflikt
místo	místo	k7c2	místo
společenství	společenství	k1gNnSc2	společenství
<g/>
?	?	kIx.	?
</s>
<s>
Češi	Čech	k1gMnPc1	Čech
a	a	k8xC	a
Němci	Němec	k1gMnPc1	Němec
v	v	k7c6	v
československém	československý	k2eAgInSc6d1	československý
státě	stát	k1gInSc6	stát
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
–	–	k?	–
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
R	R	kA	R
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Ústavem	ústav	k1gInSc7	ústav
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
vztahů	vztah	k1gInPc2	vztah
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Místo	místo	k7c2	místo
společenství	společenství	k1gNnSc2	společenství
-	-	kIx~	-
konflikt	konflikt	k1gInSc1	konflikt
<g/>
!	!	kIx.	!
</s>
<s>
Češi	Čech	k1gMnPc1	Čech
a	a	k8xC	a
Němci	Němec	k1gMnPc1	Němec
ve	v	k7c6	v
Velkoněmecké	velkoněmecký	k2eAgFnSc6d1	Velkoněmecká
říši	říš	k1gFnSc6	říš
a	a	k8xC	a
cesta	cesta	k1gFnSc1	cesta
k	k	k7c3	k
odsunu	odsun	k1gInSc3	odsun
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Ústav	ústav	k1gInSc1	ústav
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
vztahů	vztah	k1gInPc2	vztah
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Die	Die	k?	Die
SED	sed	k1gInSc1	sed
und	und	k?	und
der	drát	k5eAaImRp2nS	drát
"	"	kIx"	"
<g/>
Prager	Prager	k1gInSc1	Prager
Frühling	Frühling	k1gInSc1	Frühling
<g/>
"	"	kIx"	"
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
Politik	politik	k1gMnSc1	politik
gegen	gegna	k1gFnPc2	gegna
einen	einen	k1gInSc1	einen
"	"	kIx"	"
<g/>
Sozialismus	Sozialismus	k1gInSc1	Sozialismus
mit	mit	k?	mit
menschlichem	menschlich	k1gInSc7	menschlich
Antlitz	Antlitz	k1gMnSc1	Antlitz
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Berlin	berlina	k1gFnPc2	berlina
:	:	kIx,	:
Akademie-Verlag	Akademie-Verlag	k1gMnSc1	Akademie-Verlag
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
(	(	kIx(	(
<g/>
s	s	k7c7	s
L.	L.	kA	L.
Prießem	Prießem	k1gInSc1	Prießem
a	a	k8xC	a
M.	M.	kA	M.
Wilke	Wilke	k1gInSc1	Wilke
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vlastenci	vlastenec	k1gMnPc1	vlastenec
proti	proti	k7c3	proti
okupaci	okupace	k1gFnSc3	okupace
<g/>
.	.	kIx.	.
</s>
<s>
Ústřední	ústřední	k2eAgNnSc1d1	ústřední
vedení	vedení	k1gNnSc1	vedení
odboje	odboj	k1gInSc2	odboj
domácího	domácí	k2eAgInSc2d1	domácí
1940	[number]	k4	1940
<g/>
–	–	k?	–
<g/>
1943	[number]	k4	1943
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Karolinum	Karolinum	k1gNnSc1	Karolinum
:	:	kIx,	:
Ústav	ústav	k1gInSc1	ústav
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
vztahů	vztah	k1gInPc2	vztah
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Sudety	Sudety	k1gInPc4	Sudety
<g/>
"	"	kIx"	"
pod	pod	k7c7	pod
hákovým	hákový	k2eAgInSc7d1	hákový
křížem	kříž	k1gInSc7	kříž
<g/>
.	.	kIx.	.
</s>
<s>
Albis	Albis	k1gFnSc1	Albis
International	International	k1gFnSc1	International
<g/>
,	,	kIx,	,
Ústí	ústí	k1gNnSc1	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
se	s	k7c7	s
Z.	Z.	kA	Z.
Radvanovským	Radvanovský	k2eAgNnSc7d1	Radvanovský
a	a	k8xC	a
kolektivem	kolektivum	k1gNnSc7	kolektivum
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Češi	Čech	k1gMnPc1	Čech
<g/>
,	,	kIx,	,
Němci	Němec	k1gMnPc1	Němec
a	a	k8xC	a
mnichovská	mnichovský	k2eAgFnSc1d1	Mnichovská
křižovatka	křižovatka	k1gFnSc1	křižovatka
(	(	kIx(	(
<g/>
stručné	stručný	k2eAgNnSc1d1	stručné
čtení	čtení	k1gNnSc1	čtení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Karolinum	Karolinum	k1gNnSc1	Karolinum
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hitlerova	Hitlerův	k2eAgFnSc1d1	Hitlerova
odložená	odložený	k2eAgFnSc1d1	odložená
válka	válka	k1gFnSc1	válka
za	za	k7c4	za
zničení	zničení	k1gNnSc4	zničení
ČSR	ČSR	kA	ČSR
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
s	s	k7c7	s
F.	F.	kA	F.
Vaškem	Vašek	k1gMnSc7	Vašek
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
České	český	k2eAgNnSc1d1	české
národní	národní	k2eAgNnSc1d1	národní
povstání	povstání	k1gNnSc1	povstání
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Karolinum	Karolinum	k1gNnSc1	Karolinum
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
se	s	k7c7	s
Z.	Z.	kA	Z.
Štěpánkem	Štěpánek	k1gMnSc7	Štěpánek
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BENČÍK	BENČÍK	kA	BENČÍK
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Kural	Kural	k1gMnSc1	Kural
-	-	kIx~	-
75	[number]	k4	75
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
Zpravodaj	zpravodaj	k1gInSc1	zpravodaj
Historického	historický	k2eAgInSc2d1	historický
klubu	klub	k1gInSc2	klub
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
s.	s.	k?	s.
86	[number]	k4	86
<g/>
-	-	kIx~	-
<g/>
88	[number]	k4	88
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
0	[number]	k4	0
<g/>
862	[number]	k4	862
<g/>
-	-	kIx~	-
<g/>
8513	[number]	k4	8513
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BENČÍK	BENČÍK	kA	BENČÍK
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
odejde	odejít	k5eAaPmIp3nS	odejít
přítel	přítel	k1gMnSc1	přítel
nejbližší	blízký	k2eAgMnSc1d3	nejbližší
<g/>
...	...	k?	...
In	In	k1gMnSc1	In
<g/>
:	:	kIx,	:
Přísně	přísně	k6eAd1	přísně
tajné	tajný	k2eAgFnPc1d1	tajná
<g/>
!	!	kIx.	!
</s>
<s>
Literatura	literatura	k1gFnSc1	literatura
faktu	fakt	k1gInSc2	fakt
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
/	/	kIx~	/
<g/>
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
s.	s.	k?	s.
128	[number]	k4	128
<g/>
-	-	kIx~	-
<g/>
131	[number]	k4	131
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1212	[number]	k4	1212
<g/>
-	-	kIx~	-
<g/>
7620	[number]	k4	7620
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KŘEN	křen	k1gInSc1	křen
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Kural	Kural	k1gMnSc1	Kural
(	(	kIx(	(
<g/>
*	*	kIx~	*
25	[number]	k4	25
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
1928	[number]	k4	1928
-	-	kIx~	-
†	†	k?	†
25	[number]	k4	25
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
Český	český	k2eAgInSc1d1	český
časopis	časopis	k1gInSc1	časopis
historický	historický	k2eAgInSc1d1	historický
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
/	/	kIx~	/
<g/>
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
s.	s.	k?	s.
889	[number]	k4	889
<g/>
-	-	kIx~	-
<g/>
891	[number]	k4	891
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
0	[number]	k4	0
<g/>
862	[number]	k4	862
<g/>
-	-	kIx~	-
<g/>
6111	[number]	k4	6111
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
REXOVÁ	REXOVÁ	kA	REXOVÁ
<g/>
,	,	kIx,	,
Kristina	Kristina	k1gFnSc1	Kristina
(	(	kIx(	(
<g/>
ed	ed	k?	ed
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výběrová	výběrový	k2eAgFnSc1d1	výběrová
bibliografie	bibliografie	k1gFnSc1	bibliografie
(	(	kIx(	(
<g/>
monografie	monografie	k1gFnSc2	monografie
<g/>
)	)	kIx)	)
Václava	Václav	k1gMnSc2	Václav
Kurala	Kural	k1gMnSc2	Kural
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
Český	český	k2eAgInSc1d1	český
časopis	časopis	k1gInSc1	časopis
historický	historický	k2eAgInSc1d1	historický
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
/	/	kIx~	/
<g/>
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
s.	s.	k?	s.
891	[number]	k4	891
<g/>
-	-	kIx~	-
<g/>
894	[number]	k4	894
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
0	[number]	k4	0
<g/>
862	[number]	k4	862
<g/>
-	-	kIx~	-
<g/>
6111	[number]	k4	6111
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
DEJMEK	Dejmek	k1gMnSc1	Dejmek
<g/>
,	,	kIx,	,
Jindřich	Jindřich	k1gMnSc1	Jindřich
<g/>
.	.	kIx.	.
</s>
<s>
Vzpomínka	vzpomínka	k1gFnSc1	vzpomínka
na	na	k7c4	na
Václava	Václav	k1gMnSc4	Václav
Kurala	Kural	k1gMnSc4	Kural
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
1928	[number]	k4	1928
<g/>
-	-	kIx~	-
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
Moderní	moderní	k2eAgFnPc1d1	moderní
dějiny	dějiny	k1gFnPc1	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Časopis	časopis	k1gInSc1	časopis
pro	pro	k7c4	pro
dějiny	dějiny	k1gFnPc4	dějiny
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
s.	s.	k?	s.
243	[number]	k4	243
<g/>
-	-	kIx~	-
<g/>
247	[number]	k4	247
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1210	[number]	k4	1210
<g/>
-	-	kIx~	-
<g/>
6860	[number]	k4	6860
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Václav	Václav	k1gMnSc1	Václav
Kural	Kural	k1gMnSc1	Kural
</s>
</p>
<p>
<s>
Mut	Mut	k?	Mut
der	drát	k5eAaImRp2nS	drát
DDR-Bürger	DDR-Bürger	k1gMnSc1	DDR-Bürger
unterschätzt	unterschätzt	k1gMnSc1	unterschätzt
(	(	kIx(	(
<g/>
rozhovor	rozhovor	k1gInSc1	rozhovor
pro	pro	k7c4	pro
Berliner	Berliner	k1gInSc4	Berliner
Zeitung	Zeitunga	k1gFnPc2	Zeitunga
<g/>
,	,	kIx,	,
18	[number]	k4	18
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
Kural	Kural	k1gMnSc1	Kural
<g/>
:	:	kIx,	:
Češi	Čech	k1gMnPc1	Čech
a	a	k8xC	a
Němci	Němec	k1gMnPc1	Němec
spolu	spolu	k6eAd1	spolu
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
žít	žít	k5eAaImF	žít
nemohli	moct	k5eNaImAgMnP	moct
(	(	kIx(	(
<g/>
rozhovor	rozhovor	k1gInSc4	rozhovor
pro	pro	k7c4	pro
deník	deník	k1gInSc4	deník
Právo	právo	k1gNnSc4	právo
<g/>
,	,	kIx,	,
11	[number]	k4	11
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
příloha	příloha	k1gFnSc1	příloha
Salon	salon	k1gInSc1	salon
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Historik	historik	k1gMnSc1	historik
<g/>
:	:	kIx,	:
Hitler	Hitler	k1gMnSc1	Hitler
před	před	k7c7	před
Mnichovem	Mnichov	k1gInSc7	Mnichov
často	často	k6eAd1	často
váhal	váhat	k5eAaImAgMnS	váhat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
vyzrát	vyzrát	k5eAaPmF	vyzrát
na	na	k7c4	na
Československo	Československo	k1gNnSc4	Československo
(	(	kIx(	(
<g/>
článek	článek	k1gInSc1	článek
na	na	k7c4	na
iDnes	iDnes	k1gInSc4	iDnes
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
30	[number]	k4	30
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
</s>
</p>
