<s>
Václav	Václav	k1gMnSc1	Václav
Kural	Kural	k1gMnSc1	Kural
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1928	[number]	k4	1928
Plzeň	Plzeň	k1gFnSc1	Plzeň
–	–	k?	–
25	[number]	k4	25
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2011	[number]	k4	2011
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
historik	historik	k1gMnSc1	historik
<g/>
.	.	kIx.	.
</s>
<s>
Odborně	odborně	k6eAd1	odborně
se	se	k3xPyFc4	se
zaměřoval	zaměřovat	k5eAaImAgMnS	zaměřovat
na	na	k7c4	na
česko-německé	českoěmecký	k2eAgInPc4d1	česko-německý
vztahy	vztah	k1gInPc4	vztah
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
protifašistický	protifašistický	k2eAgInSc1d1	protifašistický
odboj	odboj	k1gInSc1	odboj
a	a	k8xC	a
pražské	pražský	k2eAgNnSc1d1	Pražské
jaro	jaro	k1gNnSc1	jaro
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
