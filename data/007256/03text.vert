<s>
Háje	háj	k1gInPc1	háj
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
HA	ha	kA	ha
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stanice	stanice	k1gFnPc4	stanice
metra	metro	k1gNnSc2	metro
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c6	na
trase	trasa	k1gFnSc6	trasa
C	C	kA	C
(	(	kIx(	(
<g/>
úsek	úsek	k1gInSc1	úsek
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
sídlišti	sídliště	k1gNnSc6	sídliště
s	s	k7c7	s
názvem	název	k1gInSc7	název
Jižní	jižní	k2eAgNnSc1d1	jižní
Město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
největším	veliký	k2eAgNnSc6d3	veliký
panelovém	panelový	k2eAgNnSc6d1	panelové
sídlišti	sídliště	k1gNnSc6	sídliště
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
v	v	k7c6	v
katastrálním	katastrální	k2eAgNnSc6d1	katastrální
území	území	k1gNnSc6	území
Háje	háj	k1gInSc2	háj
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Kosmonautů	kosmonaut	k1gMnPc2	kosmonaut
byla	být	k5eAaImAgFnS	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
</s>
<s>
Stanice	stanice	k1gFnSc1	stanice
Háje	háj	k1gInSc2	háj
je	být	k5eAaImIp3nS	být
hloubená	hloubený	k2eAgFnSc1d1	hloubená
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
v	v	k7c6	v
jámě	jáma	k1gFnSc6	jáma
a	a	k8xC	a
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
koncová	koncový	k2eAgFnSc1d1	koncová
stanice	stanice	k1gFnSc1	stanice
na	na	k7c6	na
trase	trasa	k1gFnSc6	trasa
C.	C.	kA	C.
Konstrukce	konstrukce	k1gFnSc1	konstrukce
je	být	k5eAaImIp3nS	být
monolitická	monolitický	k2eAgFnSc1d1	monolitická
<g/>
,	,	kIx,	,
železobetonová	železobetonový	k2eAgFnSc1d1	železobetonová
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
515	[number]	k4	515
m	m	kA	m
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
dvou	dva	k4xCgInPc2	dva
obratových	obratový	k2eAgInPc2d1	obratový
a	a	k8xC	a
dvou	dva	k4xCgFnPc2	dva
odstavných	odstavný	k2eAgFnPc2d1	odstavná
kolejí	kolej	k1gFnPc2	kolej
<g/>
)	)	kIx)	)
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
11	[number]	k4	11
m	m	kA	m
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
náměstí	náměstí	k1gNnSc2	náměstí
Kosmonautů	kosmonaut	k1gMnPc2	kosmonaut
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgInPc4	dva
vestibuly	vestibul	k1gInPc4	vestibul
–	–	k?	–
západní	západní	k2eAgInSc1d1	západní
<g/>
,	,	kIx,	,
spojený	spojený	k2eAgInSc1d1	spojený
s	s	k7c7	s
nástupištěm	nástupiště	k1gNnSc7	nástupiště
pevným	pevný	k2eAgNnSc7d1	pevné
schodištěm	schodiště	k1gNnSc7	schodiště
<g/>
,	,	kIx,	,
a	a	k8xC	a
východní	východní	k2eAgFnSc1d1	východní
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
vedou	vést	k5eAaImIp3nP	vést
k	k	k7c3	k
nástupišti	nástupiště	k1gNnSc3	nástupiště
tři	tři	k4xCgInPc4	tři
eskalátory	eskalátor	k1gInPc4	eskalátor
<g/>
.	.	kIx.	.
</s>
<s>
Nástupiště	nástupiště	k1gNnSc1	nástupiště
stanice	stanice	k1gFnSc2	stanice
je	být	k5eAaImIp3nS	být
za	za	k7c7	za
kolejištěm	kolejiště	k1gNnSc7	kolejiště
obloženo	obložit	k5eAaPmNgNnS	obložit
mramorovými	mramorový	k2eAgFnPc7d1	mramorová
deskami	deska	k1gFnPc7	deska
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
všechny	všechen	k3xTgFnPc4	všechen
ostatní	ostatní	k2eAgFnPc4d1	ostatní
tehdejší	tehdejší	k2eAgFnPc4d1	tehdejší
stanice	stanice	k1gFnPc4	stanice
trasy	trasa	k1gFnSc2	trasa
C.	C.	kA	C.
Pro	pro	k7c4	pro
symbolické	symbolický	k2eAgNnSc4d1	symbolické
sjednocení	sjednocení	k1gNnSc4	sjednocení
s	s	k7c7	s
trasou	trasa	k1gFnSc7	trasa
A	a	k9	a
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
navíc	navíc	k6eAd1	navíc
použito	použít	k5eAaPmNgNnS	použít
několik	několik	k4yIc1	několik
hnědých	hnědý	k2eAgInPc2d1	hnědý
hliníkových	hliníkový	k2eAgInPc2d1	hliníkový
eloxovaných	eloxovaný	k2eAgInPc2d1	eloxovaný
výlisků	výlisek	k1gInPc2	výlisek
jako	jako	k8xS	jako
na	na	k7c6	na
trase	trasa	k1gFnSc6	trasa
A.	A.	kA	A.
Tato	tento	k3xDgFnSc1	tento
stanice	stanice	k1gFnSc1	stanice
je	být	k5eAaImIp3nS	být
koncipována	koncipovat	k5eAaBmNgFnS	koncipovat
jako	jako	k8xC	jako
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
koncová	koncový	k2eAgFnSc1d1	koncová
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
stanicí	stanice	k1gFnSc7	stanice
a	a	k8xC	a
obratovými	obratový	k2eAgFnPc7d1	obratová
kolejemi	kolej	k1gFnPc7	kolej
jsou	být	k5eAaImIp3nP	být
technologické	technologický	k2eAgInPc4d1	technologický
prostory	prostor	k1gInPc4	prostor
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
dříve	dříve	k6eAd2	dříve
sloužily	sloužit	k5eAaImAgInP	sloužit
kromě	kromě	k7c2	kromě
vlakových	vlakový	k2eAgFnPc2d1	vlaková
čet	četa	k1gFnPc2	četa
také	také	k9	také
pracovníkům	pracovník	k1gMnPc3	pracovník
údržby	údržba	k1gFnSc2	údržba
vozů	vůz	k1gInPc2	vůz
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
zde	zde	k6eAd1	zde
zřízena	zřídit	k5eAaPmNgFnS	zřídit
tzv.	tzv.	kA	tzv.
Stanice	stanice	k1gFnSc1	stanice
provozního	provozní	k2eAgNnSc2d1	provozní
ošetření	ošetření	k1gNnSc2	ošetření
(	(	kIx(	(
<g/>
SPO	SPO	kA	SPO
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Probíhaly	probíhat	k5eAaImAgFnP	probíhat
zde	zde	k6eAd1	zde
za	za	k7c2	za
provozu	provoz	k1gInSc2	provoz
základní	základní	k2eAgFnSc2d1	základní
prohlídky	prohlídka	k1gFnSc2	prohlídka
souprav	souprava	k1gFnPc2	souprava
metra	metro	k1gNnSc2	metro
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
prohlídky	prohlídka	k1gFnPc1	prohlídka
probíhaly	probíhat	k5eAaImAgFnP	probíhat
vždy	vždy	k6eAd1	vždy
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
obratových	obratový	k2eAgFnPc2d1	obratová
kolejí	kolej	k1gFnPc2	kolej
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
prostory	prostora	k1gFnSc2	prostora
slouží	sloužit	k5eAaImIp3nS	sloužit
převážně	převážně	k6eAd1	převážně
pro	pro	k7c4	pro
vlakové	vlakový	k2eAgFnPc4d1	vlaková
čety	četa	k1gFnPc4	četa
(	(	kIx(	(
<g/>
nástupní	nástupní	k2eAgNnSc4d1	nástupní
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výstavba	výstavba	k1gFnSc1	výstavba
stanice	stanice	k1gFnSc2	stanice
během	během	k7c2	během
konce	konec	k1gInSc2	konec
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
stála	stát	k5eAaImAgFnS	stát
338	[number]	k4	338
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
odstavných	odstavný	k2eAgFnPc2d1	odstavná
kolejí	kolej	k1gFnPc2	kolej
k	k	k7c3	k
požáru	požár	k1gInSc3	požár
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yQgInSc6	který
však	však	k9	však
nikdo	nikdo	k3yNnSc1	nikdo
nebyl	být	k5eNaImAgMnS	být
zraněn	zranit	k5eAaPmNgMnS	zranit
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
západního	západní	k2eAgInSc2d1	západní
vestibulu	vestibul	k1gInSc2	vestibul
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
terminál	terminál	k1gInSc1	terminál
autobusů	autobus	k1gInPc2	autobus
městské	městský	k2eAgFnSc2d1	městská
hromadné	hromadný	k2eAgFnSc2d1	hromadná
plochy	plocha	k1gFnSc2	plocha
s	s	k7c7	s
odstavnou	odstavný	k2eAgFnSc7d1	odstavná
plochou	plocha	k1gFnSc7	plocha
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
i	i	k9	i
jako	jako	k9	jako
obratiště	obratiště	k1gNnSc1	obratiště
linek	linka	k1gFnPc2	linka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
zde	zde	k6eAd1	zde
končí	končit	k5eAaImIp3nP	končit
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
14	[number]	k4	14
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2008	[number]	k4	2008
zde	zde	k6eAd1	zde
končí	končit	k5eAaImIp3nS	končit
i	i	k9	i
příměstské	příměstský	k2eAgFnPc4d1	příměstská
linky	linka	k1gFnPc4	linka
PID	PID	kA	PID
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Černokostelecko	Černokostelecko	k1gNnSc4	Černokostelecko
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
původně	původně	k6eAd1	původně
končily	končit	k5eAaImAgInP	končit
u	u	k7c2	u
stanice	stanice	k1gFnSc2	stanice
metra	metro	k1gNnSc2	metro
Depo	depo	k1gNnSc1	depo
Hostivař	Hostivař	k1gFnSc1	Hostivař
v	v	k7c6	v
Malešicích	Malešice	k1gFnPc6	Malešice
<g/>
;	;	kIx,	;
tato	tento	k3xDgFnSc1	tento
změna	změna	k1gFnSc1	změna
i	i	k9	i
způsob	způsob	k1gInSc4	způsob
jejího	její	k3xOp3gNnSc2	její
zdůvodnění	zdůvodnění	k1gNnSc2	zdůvodnění
byly	být	k5eAaImAgFnP	být
hojně	hojně	k6eAd1	hojně
kritizovány	kritizovat	k5eAaImNgFnP	kritizovat
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
přestupu	přestup	k1gInSc3	přestup
do	do	k7c2	do
stanice	stanice	k1gFnSc2	stanice
metra	metro	k1gNnSc2	metro
západním	západní	k2eAgInSc7d1	západní
vestibulem	vestibul	k1gInSc7	vestibul
slouží	sloužit	k5eAaImIp3nS	sloužit
též	též	k9	též
jeden	jeden	k4xCgInSc1	jeden
směr	směr	k1gInSc1	směr
zastávky	zastávka	k1gFnSc2	zastávka
Modrá	modrý	k2eAgFnSc1d1	modrá
škola	škola	k1gFnSc1	škola
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
stanice	stanice	k1gFnSc2	stanice
připomání	připomání	k1gNnSc2	připomání
také	také	k9	také
Sousoší	sousoší	k1gNnSc1	sousoší
kosmonautů	kosmonaut	k1gMnPc2	kosmonaut
nacházející	nacházející	k2eAgInPc4d1	nacházející
se	se	k3xPyFc4	se
poblíž	poblíž	k7c2	poblíž
autobusového	autobusový	k2eAgInSc2d1	autobusový
terminálu	terminál	k1gInSc2	terminál
na	na	k7c6	na
Hájích	háj	k1gInPc6	háj
a	a	k8xC	a
západního	západní	k2eAgInSc2d1	západní
výlezu	výlez	k1gInSc2	výlez
metra	metro	k1gNnSc2	metro
<g/>
.	.	kIx.	.
</s>
<s>
Symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
kosmonauty	kosmonaut	k1gMnPc4	kosmonaut
Vladimíra	Vladimír	k1gMnSc4	Vladimír
Remka	Remek	k1gMnSc4	Remek
a	a	k8xC	a
Alexeje	Alexej	k1gMnSc4	Alexej
Gubareva	Gubarev	k1gMnSc4	Gubarev
<g/>
.	.	kIx.	.
</s>
<s>
Východní	východní	k2eAgInSc1d1	východní
vestibul	vestibul	k1gInSc1	vestibul
je	být	k5eAaImIp3nS	být
nadzemní	nadzemní	k2eAgMnSc1d1	nadzemní
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
úrovní	úroveň	k1gFnSc7	úroveň
okolního	okolní	k2eAgInSc2d1	okolní
terénu	terén	k1gInSc2	terén
<g/>
,	,	kIx,	,
a	a	k8xC	a
bezprostředně	bezprostředně	k6eAd1	bezprostředně
na	na	k7c4	na
něj	on	k3xPp3gNnSc4	on
navazuje	navazovat	k5eAaImIp3nS	navazovat
tzv.	tzv.	kA	tzv.
pochozí	pochozí	k2eAgFnSc1d1	pochozí
obchodní	obchodní	k2eAgFnSc1d1	obchodní
zóna	zóna	k1gFnSc1	zóna
–	–	k?	–
nákupní	nákupní	k2eAgInSc4d1	nákupní
areál	areál	k1gInSc4	areál
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vede	vést	k5eAaImIp3nS	vést
až	až	k6eAd1	až
k	k	k7c3	k
zastávce	zastávka	k1gFnSc3	zastávka
Horčičkova	Horčičkův	k2eAgFnSc1d1	Horčičkova
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sousedství	sousedství	k1gNnSc6	sousedství
východního	východní	k2eAgInSc2d1	východní
vestibulu	vestibul	k1gInSc2	vestibul
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
také	také	k9	také
multikino	multikino	k1gNnSc1	multikino
Cinema	Cinem	k1gMnSc2	Cinem
City	City	k1gFnSc1	City
Galaxie	galaxie	k1gFnSc1	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
úrovně	úroveň	k1gFnSc2	úroveň
vestibulu	vestibul	k1gInSc2	vestibul
a	a	k8xC	a
obchodní	obchodní	k2eAgFnSc2d1	obchodní
zóny	zóna	k1gFnSc2	zóna
vede	vést	k5eAaImIp3nS	vést
pěší	pěší	k2eAgFnSc1d1	pěší
lávka	lávka	k1gFnSc1	lávka
přes	přes	k7c4	přes
Opatovskou	Opatovský	k2eAgFnSc4d1	Opatovská
ulici	ulice	k1gFnSc4	ulice
k	k	k7c3	k
věžovému	věžový	k2eAgInSc3d1	věžový
panelovému	panelový	k2eAgInSc3d1	panelový
domu	dům	k1gInSc3	dům
v	v	k7c6	v
Bajkonurské	Bajkonurský	k2eAgFnSc6d1	Bajkonurská
ulici	ulice	k1gFnSc6	ulice
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Bajkonur	Bajkonura	k1gFnPc2	Bajkonura
<g/>
)	)	kIx)	)
a	a	k8xC	a
k	k	k7c3	k
zastávce	zastávka	k1gFnSc3	zastávka
autobusů	autobus	k1gInPc2	autobus
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
zastávce	zastávka	k1gFnSc3	zastávka
Horčičkova	Horčičkův	k2eAgFnSc1d1	Horčičkova
<g/>
.	.	kIx.	.
</s>
<s>
Dům	dům	k1gInSc1	dům
"	"	kIx"	"
<g/>
Bajkonur	Bajkonur	k1gMnSc1	Bajkonur
<g/>
"	"	kIx"	"
původně	původně	k6eAd1	původně
měl	mít	k5eAaImAgInS	mít
z	z	k7c2	z
východní	východní	k2eAgFnSc2d1	východní
strany	strana	k1gFnSc2	strana
vybudované	vybudovaný	k2eAgFnPc1d1	vybudovaná
vstupní	vstupní	k2eAgFnPc1d1	vstupní
dveře	dveře	k1gFnPc1	dveře
plánované	plánovaný	k2eAgFnPc1d1	plánovaná
jako	jako	k8xC	jako
hlavní	hlavní	k2eAgInSc1d1	hlavní
vchod	vchod	k1gInSc1	vchod
ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
patře	patro	k1gNnSc6	patro
<g/>
,	,	kIx,	,
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
úrovni	úroveň	k1gFnSc6	úroveň
byly	být	k5eAaImAgFnP	být
i	i	k8xC	i
poštovní	poštovní	k2eAgFnPc1d1	poštovní
schránky	schránka	k1gFnPc1	schránka
pro	pro	k7c4	pro
všechny	všechen	k3xTgInPc4	všechen
byty	byt	k1gInPc4	byt
a	a	k8xC	a
jedno	jeden	k4xCgNnSc1	jeden
rameno	rameno	k1gNnSc1	rameno
lávky	lávka	k1gFnSc2	lávka
mělo	mít	k5eAaImAgNnS	mít
vést	vést	k5eAaImF	vést
přímo	přímo	k6eAd1	přímo
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
vchodu	vchod	k1gInSc3	vchod
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
záměru	záměr	k1gInSc2	záměr
bylo	být	k5eAaImAgNnS	být
však	však	k9	však
upuštěno	upustit	k5eAaPmNgNnS	upustit
a	a	k8xC	a
vchod	vchod	k1gInSc1	vchod
zrušen	zrušit	k5eAaPmNgInS	zrušit
a	a	k8xC	a
nahrazen	nahradit	k5eAaPmNgInS	nahradit
okny	okno	k1gNnPc7	okno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
stanice	stanice	k1gFnSc2	stanice
metra	metro	k1gNnSc2	metro
Háje	háj	k1gInSc2	háj
stojí	stát	k5eAaImIp3nS	stát
hlavní	hlavní	k2eAgFnSc1d1	hlavní
dominanta	dominanta	k1gFnSc1	dominanta
sídliště	sídliště	k1gNnSc2	sídliště
Háje	háj	k1gInSc2	háj
i	i	k8xC	i
celého	celý	k2eAgNnSc2d1	celé
Jižního	jižní	k2eAgNnSc2d1	jižní
Města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
ubytovna	ubytovna	k1gFnSc1	ubytovna
Kupa	kupa	k1gFnSc1	kupa
<g/>
,	,	kIx,	,
dvojice	dvojice	k1gFnSc1	dvojice
panelových	panelový	k2eAgInPc2d1	panelový
výškových	výškový	k2eAgInPc2d1	výškový
domů	dům	k1gInPc2	dům
spojená	spojený	k2eAgNnPc4d1	spojené
u	u	k7c2	u
vrcholu	vrchol	k1gInSc2	vrchol
mostním	mostní	k2eAgInSc7d1	mostní
přechodem	přechod	k1gInSc7	přechod
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Obchodní	obchodní	k2eAgFnSc1d1	obchodní
zóna	zóna	k1gFnSc1	zóna
Háje	háj	k1gInSc2	háj
<g/>
.	.	kIx.	.
</s>
<s>
Městská	městský	k2eAgFnSc1d1	městská
část	část	k1gFnSc1	část
Praha	Praha	k1gFnSc1	Praha
11	[number]	k4	11
již	již	k6eAd1	již
několik	několik	k4yIc1	několik
let	léto	k1gNnPc2	léto
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
2010	[number]	k4	2010
usilovala	usilovat	k5eAaImAgFnS	usilovat
o	o	k7c4	o
rekonstrukci	rekonstrukce	k1gFnSc4	rekonstrukce
pochozí	pochozí	k2eAgFnSc2d1	pochozí
obchodní	obchodní	k2eAgFnSc2d1	obchodní
zóny	zóna	k1gFnSc2	zóna
u	u	k7c2	u
východního	východní	k2eAgInSc2d1	východní
vestibulu	vestibul	k1gInSc2	vestibul
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
přípravu	příprava	k1gFnSc4	příprava
zdržely	zdržet	k5eAaPmAgInP	zdržet
roztříštěné	roztříštěný	k2eAgInPc1d1	roztříštěný
a	a	k8xC	a
sporné	sporný	k2eAgInPc1d1	sporný
majetkové	majetkový	k2eAgInPc1d1	majetkový
vztahy	vztah	k1gInPc1	vztah
k	k	k7c3	k
pozemkům	pozemek	k1gInPc3	pozemek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2009	[number]	k4	2009
představila	představit	k5eAaPmAgFnS	představit
společnost	společnost	k1gFnSc1	společnost
Melit	Melita	k1gFnPc2	Melita
městskému	městský	k2eAgMnSc3d1	městský
zastupitelstvu	zastupitelstvo	k1gNnSc6	zastupitelstvo
vizi	vize	k1gFnSc4	vize
přestavby	přestavba	k1gFnSc2	přestavba
komplexu	komplex	k1gInSc2	komplex
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
výstavbou	výstavba	k1gFnSc7	výstavba
čtyř	čtyři	k4xCgInPc2	čtyři
dvanáctipatrových	dvanáctipatrový	k2eAgInPc2d1	dvanáctipatrový
domů	dům	k1gInPc2	dům
s	s	k7c7	s
překlenutím	překlenutí	k1gNnSc7	překlenutí
Opatovské	Opatovský	k2eAgFnSc2d1	Opatovská
ulice	ulice	k1gFnSc2	ulice
nebo	nebo	k8xC	nebo
s	s	k7c7	s
úpravou	úprava	k1gFnSc7	úprava
nákupní	nákupní	k2eAgFnSc2d1	nákupní
zóny	zóna	k1gFnSc2	zóna
na	na	k7c4	na
zeleň	zeleň	k1gFnSc4	zeleň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2010	[number]	k4	2010
informovala	informovat	k5eAaBmAgFnS	informovat
společnost	společnost	k1gFnSc1	společnost
Melit	Melita	k1gFnPc2	Melita
o	o	k7c6	o
svém	svůj	k3xOyFgInSc6	svůj
záměru	záměr	k1gInSc6	záměr
vybudovat	vybudovat	k5eAaPmF	vybudovat
nad	nad	k7c7	nad
stanicí	stanice	k1gFnSc7	stanice
metra	metro	k1gNnSc2	metro
<g/>
,	,	kIx,	,
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
nynějšího	nynější	k2eAgInSc2d1	nynější
východního	východní	k2eAgInSc2d1	východní
vestibulu	vestibul	k1gInSc2	vestibul
stanice	stanice	k1gFnSc2	stanice
a	a	k8xC	a
navazující	navazující	k2eAgFnSc2d1	navazující
pochozí	pochozí	k2eAgFnSc2d1	pochozí
obchodní	obchodní	k2eAgFnSc2d1	obchodní
zóny	zóna	k1gFnSc2	zóna
<g/>
,	,	kIx,	,
komplex	komplex	k1gInSc4	komplex
administrativních	administrativní	k2eAgFnPc2d1	administrativní
<g/>
,	,	kIx,	,
komerčních	komerční	k2eAgFnPc2d1	komerční
a	a	k8xC	a
obytných	obytný	k2eAgFnPc2d1	obytná
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
součástí	součást	k1gFnSc7	součást
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
i	i	k9	i
120	[number]	k4	120
m	m	kA	m
vysoký	vysoký	k2eAgInSc4d1	vysoký
31	[number]	k4	31
<g/>
podlažní	podlažní	k2eAgInSc4d1	podlažní
mrakodrap	mrakodrap	k1gInSc4	mrakodrap
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nS	by
tak	tak	k9	tak
byl	být	k5eAaImAgMnS	být
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
budovou	budova	k1gFnSc7	budova
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Komplex	komplex	k1gInSc1	komplex
by	by	kYmCp3nS	by
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
zahrnoval	zahrnovat	k5eAaImAgMnS	zahrnovat
obchodní	obchodní	k2eAgNnSc4d1	obchodní
centrum	centrum	k1gNnSc4	centrum
<g/>
,	,	kIx,	,
dvojici	dvojice	k1gFnSc4	dvojice
administrativních	administrativní	k2eAgFnPc2d1	administrativní
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
bytový	bytový	k2eAgInSc1d1	bytový
dům	dům	k1gInSc1	dům
<g/>
,	,	kIx,	,
tři	tři	k4xCgNnPc1	tři
podzemní	podzemní	k2eAgNnPc1d1	podzemní
parkoviště	parkoviště	k1gNnPc1	parkoviště
s	s	k7c7	s
kapacitou	kapacita	k1gFnSc7	kapacita
asi	asi	k9	asi
1600	[number]	k4	1600
míst	místo	k1gNnPc2	místo
a	a	k8xC	a
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
komplexu	komplex	k1gInSc2	komplex
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
nacházelo	nacházet	k5eAaImAgNnS	nacházet
zčásti	zčásti	k6eAd1	zčásti
zastřešené	zastřešený	k2eAgNnSc1d1	zastřešené
náměstí	náměstí	k1gNnSc1	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
16	[number]	k4	16
<g/>
podlažní	podlažní	k2eAgInSc1d1	podlažní
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
podle	podle	k7c2	podle
vizualice	vizualice	k1gFnSc2	vizualice
překlenout	překlenout	k5eAaPmF	překlenout
Opatovskou	Opatovský	k2eAgFnSc4d1	Opatovská
ulici	ulice	k1gFnSc4	ulice
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
nynější	nynější	k2eAgFnSc2d1	nynější
lávky	lávka	k1gFnSc2	lávka
<g/>
.	.	kIx.	.
</s>
<s>
Komplex	komplex	k1gInSc1	komplex
byl	být	k5eAaImAgInS	být
prezentován	prezentovat	k5eAaBmNgInS	prezentovat
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Porto	porto	k1gNnSc1	porto
Háje	háj	k1gInSc2	háj
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
záměrů	záměr	k1gInPc2	záměr
investora	investor	k1gMnSc2	investor
by	by	kYmCp3nS	by
celý	celý	k2eAgInSc1d1	celý
komplex	komplex	k1gInSc1	komplex
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
v	v	k7c6	v
letech	let	k1gInPc6	let
2013	[number]	k4	2013
<g/>
–	–	k?	–
<g/>
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Žádný	žádný	k3yNgInSc1	žádný
orgán	orgán	k1gInSc1	orgán
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
zatím	zatím	k6eAd1	zatím
výstavbu	výstavba	k1gFnSc4	výstavba
neschválil	schválit	k5eNaPmAgMnS	schválit
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
záměru	záměr	k1gInSc3	záměr
protestovali	protestovat	k5eAaBmAgMnP	protestovat
někteří	některý	k3yIgMnPc1	některý
návštěvníci	návštěvník	k1gMnPc1	návštěvník
prezentace	prezentace	k1gFnSc2	prezentace
v	v	k7c6	v
Komunitním	komunitní	k2eAgNnSc6d1	komunitní
centru	centrum	k1gNnSc6	centrum
Matky	matka	k1gFnSc2	matka
Terezy	Tereza	k1gFnSc2	Tereza
i	i	k8xC	i
bývalá	bývalý	k2eAgFnSc1d1	bývalá
starostka	starostka	k1gFnSc1	starostka
Marta	Marta	k1gFnSc1	Marta
Šorfová	Šorfový	k2eAgFnSc1d1	Šorfová
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Háje	háj	k1gInSc2	háj
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
