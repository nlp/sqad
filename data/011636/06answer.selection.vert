<s>
Necyklopedie	Necyklopedie	k1gFnSc1	Necyklopedie
<g/>
,	,	kIx,	,
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
originále	originál	k1gInSc6	originál
Uncyclopedia	Uncyclopedium	k1gNnSc2	Uncyclopedium
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
satirická	satirický	k2eAgFnSc1d1	satirická
"	"	kIx"	"
<g/>
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
<g/>
"	"	kIx"	"
parodující	parodující	k2eAgFnSc3d1	parodující
Wikipedii	Wikipedie	k1gFnSc3	Wikipedie
(	(	kIx(	(
<g/>
i	i	k9	i
když	když	k8xS	když
sama	sám	k3xTgMnSc4	sám
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
opak	opak	k1gInSc1	opak
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
