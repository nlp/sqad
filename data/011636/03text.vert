<p>
<s>
Necyklopedie	Necyklopedie	k1gFnSc1	Necyklopedie
<g/>
,	,	kIx,	,
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
originále	originál	k1gInSc6	originál
Uncyclopedia	Uncyclopedium	k1gNnSc2	Uncyclopedium
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
satirická	satirický	k2eAgFnSc1d1	satirická
"	"	kIx"	"
<g/>
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
<g/>
"	"	kIx"	"
parodující	parodující	k2eAgFnSc3d1	parodující
Wikipedii	Wikipedie	k1gFnSc3	Wikipedie
(	(	kIx(	(
<g/>
i	i	k9	i
když	když	k8xS	když
sama	sám	k3xTgMnSc4	sám
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
opak	opak	k1gInSc1	opak
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Uncyclopedia	Uncyclopedium	k1gNnPc1	Uncyclopedium
vznikla	vzniknout	k5eAaPmAgNnP	vzniknout
jako	jako	k8xS	jako
anglofonní	anglofonní	k2eAgMnPc1d1	anglofonní
projekt	projekt	k1gInSc4	projekt
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
však	však	k9	však
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
několik	několik	k4yIc4	několik
jejích	její	k3xOp3gFnPc2	její
jazykových	jazykový	k2eAgFnPc2d1	jazyková
mutací	mutace	k1gFnPc2	mutace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Necyklopedie	Necyklopedie	k1gFnSc1	Necyklopedie
je	být	k5eAaImIp3nS	být
sbírkou	sbírka	k1gFnSc7	sbírka
absurdních	absurdní	k2eAgNnPc2d1	absurdní
hesel	heslo	k1gNnPc2	heslo
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
nemají	mít	k5eNaImIp3nP	mít
zdánlivě	zdánlivě	k6eAd1	zdánlivě
nic	nic	k3yNnSc1	nic
společného	společný	k2eAgNnSc2d1	společné
se	s	k7c7	s
skutečností	skutečnost	k1gFnSc7	skutečnost
–	–	k?	–
například	například	k6eAd1	například
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
definována	definovat	k5eAaBmNgFnS	definovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
satirická	satirický	k2eAgFnSc1d1	satirická
'	'	kIx"	'
<g/>
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
<g/>
'	'	kIx"	'
parodující	parodující	k2eAgFnSc3d1	parodující
Necyklopedii	Necyklopedie	k1gFnSc3	Necyklopedie
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgInPc2d1	hlavní
principů	princip	k1gInPc2	princip
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
satirical	satiricat	k5eAaPmAgMnS	satiricat
point	pointa	k1gFnPc2	pointa
of	of	k?	of
view	view	k?	view
(	(	kIx(	(
<g/>
SPOV	SPOV	kA	SPOV
<g/>
,	,	kIx,	,
satirický	satirický	k2eAgInSc1d1	satirický
úhel	úhel	k1gInSc1	úhel
pohledu	pohled	k1gInSc2	pohled
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
parodie	parodie	k1gFnSc1	parodie
na	na	k7c4	na
wikipedistické	wikipedistický	k2eAgNnSc4d1	wikipedistický
"	"	kIx"	"
<g/>
neutral	utrat	k5eNaPmAgMnS	utrat
point	pointa	k1gFnPc2	pointa
of	of	k?	of
view	view	k?	view
(	(	kIx(	(
<g/>
NPOV	NPOV	kA	NPOV
<g/>
,	,	kIx,	,
nezaujatý	zaujatý	k2eNgInSc1d1	nezaujatý
úhel	úhel	k1gInSc1	úhel
pohledu	pohled	k1gInSc2	pohled
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Česká	český	k2eAgFnSc1d1	Česká
Necyklopedie	Necyklopedie	k1gFnSc1	Necyklopedie
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2005	[number]	k4	2005
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
verzi	verze	k1gFnSc6	verze
(	(	kIx(	(
<g/>
Uncyclopedia	Uncyclopedium	k1gNnSc2	Uncyclopedium
<g/>
)	)	kIx)	)
založen	založit	k5eAaPmNgInS	založit
první	první	k4xOgInSc1	první
článek	článek	k1gInSc1	článek
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
,	,	kIx,	,
věnovaný	věnovaný	k2eAgInSc1d1	věnovaný
Rossovi	Ross	k1gMnSc6	Ross
Hedvíčkovi	Hedvíček	k1gMnSc6	Hedvíček
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
srpna	srpen	k1gInSc2	srpen
2005	[number]	k4	2005
běží	běžet	k5eAaImIp3nS	běžet
česká	český	k2eAgFnSc1d1	Česká
Necyklopedie	Necyklopedie	k1gFnSc1	Necyklopedie
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
verze	verze	k1gFnSc1	verze
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
v	v	k7c6	v
září	září	k1gNnSc6	září
2005	[number]	k4	2005
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
bylo	být	k5eAaImAgNnS	být
přibližně	přibližně	k6eAd1	přibližně
14	[number]	k4	14
000	[number]	k4	000
hesel	heslo	k1gNnPc2	heslo
<g/>
,	,	kIx,	,
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
září	září	k1gNnSc2	září
2006	[number]	k4	2006
18	[number]	k4	18
000	[number]	k4	000
hesel	heslo	k1gNnPc2	heslo
<g/>
,	,	kIx,	,
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2007	[number]	k4	2007
přes	přes	k7c4	přes
23	[number]	k4	23
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
měla	mít	k5eAaImAgFnS	mít
přes	přes	k7c4	přes
30	[number]	k4	30
000	[number]	k4	000
článků	článek	k1gInPc2	článek
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ji	on	k3xPp3gFnSc4	on
činilo	činit	k5eAaImAgNnS	činit
druhou	druhý	k4xOgFnSc7	druhý
největší	veliký	k2eAgFnSc7d3	veliký
po	po	k7c6	po
verzi	verze	k1gFnSc6	verze
portugalské	portugalský	k2eAgFnSc6d1	portugalská
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2016	[number]	k4	2016
má	mít	k5eAaImIp3nS	mít
Necyklopedie	Necyklopedie	k1gFnSc1	Necyklopedie
3	[number]	k4	3
374	[number]	k4	374
hesel	heslo	k1gNnPc2	heslo
<g/>
,	,	kIx,	,
prakticky	prakticky	k6eAd1	prakticky
bez	bez	k7c2	bez
pahýlů	pahýl	k1gMnPc2	pahýl
<g/>
;	;	kIx,	;
případné	případný	k2eAgInPc4d1	případný
pahýly	pahýl	k1gInPc4	pahýl
totiž	totiž	k9	totiž
tamější	tamější	k2eAgMnSc1d1	tamější
správci	správce	k1gMnPc1	správce
likvidují	likvidovat	k5eAaBmIp3nP	likvidovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
25	[number]	k4	25
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2018	[number]	k4	2018
Necyklopedie	Necyklopedie	k1gFnSc1	Necyklopedie
měla	mít	k5eAaImAgFnS	mít
i	i	k9	i
obdobný	obdobný	k2eAgInSc4d1	obdobný
vzhled	vzhled	k1gInSc4	vzhled
jako	jako	k8xS	jako
Wikipedie	Wikipedie	k1gFnPc4	Wikipedie
<g/>
,	,	kIx,	,
používala	používat	k5eAaImAgFnS	používat
stejný	stejný	k2eAgInSc4d1	stejný
software	software	k1gInSc4	software
MediaWiki	MediaWik	k1gFnSc2	MediaWik
<g/>
,	,	kIx,	,
běžěla	běžít	k5eAaPmAgFnS	běžít
na	na	k7c6	na
webhostingu	webhosting	k1gInSc6	webhosting
pro	pro	k7c4	pro
internetové	internetový	k2eAgFnPc4d1	internetová
encyklopedie	encyklopedie	k1gFnPc4	encyklopedie
Wikia	Wikium	k1gNnSc2	Wikium
<g/>
,	,	kIx,	,
s	s	k7c7	s
Wikipedií	Wikipedie	k1gFnSc7	Wikipedie
však	však	k9	však
organizačně	organizačně	k6eAd1	organizačně
spojena	spojit	k5eAaPmNgFnS	spojit
nebyla	být	k5eNaImAgFnS	být
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Na	na	k7c6	na
Necyklopedii	Necyklopedie	k1gFnSc6	Necyklopedie
je	být	k5eAaImIp3nS	být
uplatňován	uplatňován	k2eAgInSc4d1	uplatňován
podobný	podobný	k2eAgInSc4d1	podobný
systém	systém	k1gInSc4	systém
jako	jako	k8xC	jako
na	na	k7c4	na
Wikipedii	Wikipedie	k1gFnSc4	Wikipedie
<g/>
,	,	kIx,	,
fungují	fungovat	k5eAaImIp3nP	fungovat
tam	tam	k6eAd1	tam
správci	správce	k1gMnPc1	správce
a	a	k8xC	a
edituje	editovat	k5eAaImIp3nS	editovat
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
velmi	velmi	k6eAd1	velmi
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
na	na	k7c6	na
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
článků	článek	k1gInPc2	článek
a	a	k8xC	a
zmínek	zmínka	k1gFnPc2	zmínka
je	být	k5eAaImIp3nS	být
inspirovaných	inspirovaný	k2eAgFnPc2d1	inspirovaná
i	i	k9	i
osobními	osobní	k2eAgInPc7d1	osobní
či	či	k8xC	či
věcnými	věcný	k2eAgInPc7d1	věcný
spory	spor	k1gInPc7	spor
na	na	k7c6	na
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
jelikož	jelikož	k8xS	jelikož
mnozí	mnohý	k2eAgMnPc1d1	mnohý
editoři	editor	k1gMnPc1	editor
Necyklopedie	Necyklopedie	k1gFnSc2	Necyklopedie
editovali	editovat	k5eAaImAgMnP	editovat
či	či	k8xC	či
editují	editovat	k5eAaImIp3nP	editovat
též	též	k9	též
na	na	k7c4	na
Wikipedii	Wikipedie	k1gFnSc4	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ke	k	k7c3	k
dni	den	k1gInSc3	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2018	[number]	k4	2018
hostitelský	hostitelský	k2eAgInSc1d1	hostitelský
portál	portál	k1gInSc1	portál
FANDOM	FANDOM	kA	FANDOM
(	(	kIx(	(
<g/>
Wikia	Wikius	k1gMnSc2	Wikius
<g/>
)	)	kIx)	)
s	s	k7c7	s
odvoláním	odvolání	k1gNnSc7	odvolání
na	na	k7c4	na
směrnici	směrnice	k1gFnSc4	směrnice
GDPR	GDPR	kA	GDPR
zrušil	zrušit	k5eAaPmAgMnS	zrušit
stránkami	stránka	k1gFnPc7	stránka
typu	typ	k1gInSc3	typ
Uncyclopedia	Uncyclopedium	k1gNnSc2	Uncyclopedium
dosud	dosud	k6eAd1	dosud
užívaný	užívaný	k2eAgInSc1d1	užívaný
"	"	kIx"	"
<g/>
wikipedický	wikipedický	k2eAgInSc1d1	wikipedický
<g/>
"	"	kIx"	"
vzhled	vzhled	k1gInSc1	vzhled
(	(	kIx(	(
<g/>
skin	skin	k1gMnSc1	skin
<g/>
)	)	kIx)	)
Monobook	Monobook	k1gInSc1	Monobook
<g/>
,	,	kIx,	,
a	a	k8xC	a
nahradila	nahradit	k5eAaPmAgFnS	nahradit
jej	on	k3xPp3gMnSc4	on
svým	svůj	k3xOyFgMnSc7	svůj
implicitním	implicitní	k2eAgMnSc7d1	implicitní
skinem	skin	k1gMnSc7	skin
Oasis	Oasis	k1gFnSc2	Oasis
<g/>
.	.	kIx.	.
</s>
<s>
Výrazně	výrazně	k6eAd1	výrazně
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
změnil	změnit	k5eAaPmAgInS	změnit
celkový	celkový	k2eAgInSc4d1	celkový
vzhled	vzhled	k1gInSc4	vzhled
všech	všecek	k3xTgFnPc2	všecek
tímto	tento	k3xDgInSc7	tento
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
postižených	postižený	k1gMnPc2	postižený
***	***	k?	***
<g/>
pedií	pedie	k1gFnPc2	pedie
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
i	i	k9	i
Necyclopedie	Necyclopedie	k1gFnSc1	Necyclopedie
<g/>
.	.	kIx.	.
</s>
<s>
Ohlas	ohlas	k1gInSc1	ohlas
tímto	tento	k3xDgNnSc7	tento
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
postižených	postižený	k2eAgMnPc2d1	postižený
byl	být	k5eAaImAgMnS	být
převážně	převážně	k6eAd1	převážně
negativní	negativní	k2eAgMnSc1d1	negativní
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Postižena	postižen	k2eAgFnSc1d1	postižena
tak	tak	k8xS	tak
byla	být	k5eAaImAgFnS	být
i	i	k9	i
na	na	k7c6	na
webhostingu	webhosting	k1gInSc6	webhosting
FANDOM	FANDOM	kA	FANDOM
<g/>
/	/	kIx~	/
<g/>
wikia	wikium	k1gNnPc1	wikium
provozovaná	provozovaný	k2eAgNnPc1d1	provozované
anglofonní	anglofonní	k2eAgNnPc1d1	anglofonní
Uncyclopedia	Uncyclopedium	k1gNnPc1	Uncyclopedium
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
samostatná	samostatný	k2eAgFnSc1d1	samostatná
<g/>
,	,	kIx,	,
na	na	k7c4	na
wikia	wikium	k1gNnPc4	wikium
nezávislá	závislý	k2eNgNnPc4d1	nezávislé
Uncyclopedia	Uncyclopedium	k1gNnPc4	Uncyclopedium
si	se	k3xPyFc3	se
svůj	svůj	k3xOyFgInSc4	svůj
"	"	kIx"	"
<g/>
wikipedický	wikipedický	k2eAgInSc4d1	wikipedický
<g/>
"	"	kIx"	"
vzhled	vzhled	k1gInSc4	vzhled
zachovala	zachovat	k5eAaPmAgFnS	zachovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
26	[number]	k4	26
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
2019	[number]	k4	2019
oznámil	oznámit	k5eAaPmAgMnS	oznámit
FANDOM	FANDOM	kA	FANDOM
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
31	[number]	k4	31
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
2019	[number]	k4	2019
ukončí	ukončit	k5eAaPmIp3nS	ukončit
provoz	provoz	k1gInSc1	provoz
projektů	projekt	k1gInPc2	projekt
z	z	k7c2	z
rodiny	rodina	k1gFnSc2	rodina
Uncyclopedia	Uncyclopedium	k1gNnSc2	Uncyclopedium
<g/>
.	.	kIx.	.
</s>
<s>
Těm	ten	k3xDgMnPc3	ten
pak	pak	k6eAd1	pak
nezbylo	zbýt	k5eNaPmAgNnS	zbýt
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
přesunout	přesunout	k5eAaPmF	přesunout
na	na	k7c4	na
jiné	jiný	k2eAgInPc4d1	jiný
hostingy	hosting	k1gInPc4	hosting
<g/>
,	,	kIx,	,
což	což	k9	což
různé	různý	k2eAgFnPc1d1	různá
jazykové	jazykový	k2eAgFnPc1d1	jazyková
komunity	komunita	k1gFnPc1	komunita
řešily	řešit	k5eAaImAgFnP	řešit
různým	různý	k2eAgInSc7d1	různý
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
však	však	k9	však
propojení	propojení	k1gNnSc4	propojení
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
31	[number]	k4	31
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
2019	[number]	k4	2019
byla	být	k5eAaImAgFnS	být
Necyklopedie	Necyklopedie	k1gFnSc1	Necyklopedie
přesunuta	přesunout	k5eAaPmNgFnS	přesunout
na	na	k7c4	na
adresu	adresa	k1gFnSc4	adresa
http://necyklopedie.org	[url]	k1gInSc1	http://necyklopedie.org
(	(	kIx(	(
<g/>
hosting	hosting	k1gInSc1	hosting
uncyclomedia	uncyclomedium	k1gNnSc2	uncyclomedium
<g/>
.	.	kIx.	.
<g/>
co	co	k9	co
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
dubna	duben	k1gInSc2	duben
2019	[number]	k4	2019
tak	tak	k6eAd1	tak
i	i	k9	i
česká	český	k2eAgFnSc1d1	Česká
Necyklopedie	Necyklopedie	k1gFnSc1	Necyklopedie
běží	běžet	k5eAaImIp3nS	běžet
na	na	k7c6	na
novém	nový	k2eAgInSc6d1	nový
hostingu	hosting	k1gInSc6	hosting
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
2019	[number]	k4	2019
byla	být	k5eAaImAgFnS	být
Necyklopedie	Necyklopedie	k1gFnSc1	Necyklopedie
ze	z	k7c2	z
stránek	stránka	k1gFnPc2	stránka
FANDOM	FANDOM	kA	FANDOM
odstraněna	odstranit	k5eAaPmNgFnS	odstranit
<g/>
.	.	kIx.	.
</s>
<s>
Nadále	nadále	k6eAd1	nadále
tedy	tedy	k9	tedy
už	už	k6eAd1	už
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
dostupná	dostupný	k2eAgFnSc1d1	dostupná
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
hostingu	hosting	k1gInSc6	hosting
uncyclopedia	uncyclopedium	k1gNnSc2	uncyclopedium
<g/>
.	.	kIx.	.
<g/>
co	co	k9	co
<g/>
.	.	kIx.	.
<g/>
Česká	český	k2eAgFnSc1d1	Česká
Necyklopedie	Necyklopedie	k1gFnSc1	Necyklopedie
se	se	k3xPyFc4	se
relativně	relativně	k6eAd1	relativně
hodně	hodně	k6eAd1	hodně
věnuje	věnovat	k5eAaPmIp3nS	věnovat
známým	známý	k2eAgFnPc3d1	známá
antipatiím	antipatie	k1gFnPc3	antipatie
mezi	mezi	k7c7	mezi
Prahou	Praha	k1gFnSc7	Praha
a	a	k8xC	a
Brnem	Brno	k1gNnSc7	Brno
či	či	k8xC	či
Maďary	Maďar	k1gMnPc4	Maďar
a	a	k8xC	a
Slováky	Slovák	k1gMnPc4	Slovák
a	a	k8xC	a
konspiračním	konspirační	k2eAgFnPc3d1	konspirační
teoriím	teorie	k1gFnPc3	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
desítek	desítka	k1gFnPc2	desítka
českých	český	k2eAgMnPc2d1	český
politiků	politik	k1gMnPc2	politik
či	či	k8xC	či
evropských	evropský	k2eAgInPc2d1	evropský
států	stát	k1gInPc2	stát
má	mít	k5eAaImIp3nS	mít
též	též	k9	též
své	svůj	k3xOyFgInPc4	svůj
články	článek	k1gInPc4	článek
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
také	také	k9	také
mnoho	mnoho	k4c1	mnoho
méně	málo	k6eAd2	málo
kontroverzních	kontroverzní	k2eAgNnPc2d1	kontroverzní
pojednání	pojednání	k1gNnPc2	pojednání
o	o	k7c6	o
zajímavých	zajímavý	k2eAgMnPc6d1	zajímavý
živočiších	živočich	k1gMnPc6	živočich
<g/>
,	,	kIx,	,
vynálezech	vynález	k1gInPc6	vynález
<g/>
,	,	kIx,	,
otázkách	otázka	k1gFnPc6	otázka
filosofických	filosofický	k2eAgFnPc6d1	filosofická
historických	historický	k2eAgFnPc6d1	historická
osobnostech	osobnost	k1gFnPc6	osobnost
<g/>
,	,	kIx,	,
aktuálním	aktuální	k2eAgNnSc6d1	aktuální
dění	dění	k1gNnSc6	dění
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
Necyklopedie	Necyklopedie	k1gFnSc1	Necyklopedie
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
své	svůj	k3xOyFgInPc4	svůj
podprojekty	podprojekt	k1gInPc4	podprojekt
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
Výkladový	výkladový	k2eAgInSc1d1	výkladový
slovník	slovník	k1gInSc1	slovník
<g/>
,	,	kIx,	,
Necitáty	Necitát	k1gInPc1	Necitát
<g/>
,	,	kIx,	,
Necyklokrám	Necyklokra	k1gFnPc3	Necyklokra
nebo	nebo	k8xC	nebo
Necykloverzitu	Necykloverzit	k1gInSc6	Necykloverzit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Také	také	k9	také
názvy	název	k1gInPc1	název
odkazů	odkaz	k1gInPc2	odkaz
jsou	být	k5eAaImIp3nP	být
jiné	jiný	k2eAgInPc1d1	jiný
než	než	k8xS	než
na	na	k7c4	na
Wikipedii	Wikipedie	k1gFnSc4	Wikipedie
<g/>
,	,	kIx,	,
např.	např.	kA	např.
<g/>
:	:	kIx,	:
příspěvky	příspěvek	k1gInPc1	příspěvek
=	=	kIx~	=
spraseno	sprasen	k2eAgNnSc4d1	sprasen
<g/>
,	,	kIx,	,
editovat	editovat	k5eAaImF	editovat
=	=	kIx~	=
zvandalizuj	zvandalizovat	k5eAaPmRp2nS	zvandalizovat
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
lípou	lípa	k1gFnSc7	lípa
=	=	kIx~	=
pod	pod	k7c7	pod
parou	para	k1gFnSc7	para
<g/>
,	,	kIx,	,
diskuze	diskuze	k1gFnSc1	diskuze
=	=	kIx~	=
flamewar	flamewar	k1gMnSc1	flamewar
<g/>
,	,	kIx,	,
odhlásit	odhlásit	k5eAaPmF	odhlásit
se	se	k3xPyFc4	se
=	=	kIx~	=
čus	čus	k?	čus
bus	bus	k1gInSc1	bus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kontroverze	kontroverze	k1gFnSc2	kontroverze
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
byla	být	k5eAaImAgFnS	být
česká	český	k2eAgFnSc1d1	Česká
Necyklopedie	Necyklopedie	k1gFnSc1	Necyklopedie
vyloučena	vyloučit	k5eAaPmNgFnS	vyloučit
ze	z	k7c2	z
soutěže	soutěž	k1gFnSc2	soutěž
Křišťálová	křišťálový	k2eAgFnSc1d1	Křišťálová
Lupa	lupa	k1gFnSc1	lupa
z	z	k7c2	z
kategorie	kategorie	k1gFnSc2	kategorie
Zájmové	zájmový	k2eAgInPc4d1	zájmový
weby	web	k1gInPc4	web
kvůli	kvůli	k7c3	kvůli
rasismu	rasismus	k1gInSc3	rasismus
a	a	k8xC	a
homofobii	homofobie	k1gFnSc3	homofobie
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
článků	článek	k1gInPc2	článek
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
Romové	Rom	k1gMnPc1	Rom
<g/>
,	,	kIx,	,
Homosexualita	homosexualita	k1gFnSc1	homosexualita
<g/>
,	,	kIx,	,
Gay	gay	k1gMnSc1	gay
a	a	k8xC	a
Stát	stát	k1gInSc1	stát
Izrael	Izrael	k1gInSc1	Izrael
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
organizátorů	organizátor	k1gMnPc2	organizátor
soutěže	soutěž	k1gFnSc2	soutěž
nelze	lze	k6eNd1	lze
některé	některý	k3yIgFnPc4	některý
formulace	formulace	k1gFnPc4	formulace
obhájit	obhájit	k5eAaPmF	obhájit
ani	ani	k8xC	ani
vysokou	vysoký	k2eAgFnSc7d1	vysoká
mírou	míra	k1gFnSc7	míra
nadsázky	nadsázka	k1gFnSc2	nadsázka
a	a	k8xC	a
překročily	překročit	k5eAaPmAgFnP	překročit
hranici	hranice	k1gFnSc4	hranice
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
jsou	být	k5eAaImIp3nP	být
organizátoři	organizátor	k1gMnPc1	organizátor
ochotni	ochoten	k2eAgMnPc1d1	ochoten
akceptovat	akceptovat	k5eAaBmF	akceptovat
a	a	k8xC	a
zaštiťovat	zaštiťovat	k5eAaImF	zaštiťovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Jazykové	jazykový	k2eAgFnSc2d1	jazyková
verze	verze	k1gFnSc2	verze
Necyklopedie	Necyklopedie	k1gFnSc2	Necyklopedie
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Necyklopedie	Necyklopedie	k1gFnSc2	Necyklopedie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Uncyclopedia	Uncyclopedium	k1gNnSc2	Uncyclopedium
–	–	k?	–
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
strana	strana	k1gFnSc1	strana
anglické	anglický	k2eAgFnSc2d1	anglická
verze	verze	k1gFnSc2	verze
</s>
</p>
<p>
<s>
Necyklopedie	Necyklopedie	k1gFnSc1	Necyklopedie
–	–	k?	–
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
strana	strana	k1gFnSc1	strana
české	český	k2eAgFnSc2d1	Česká
verze	verze	k1gFnSc2	verze
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
české	český	k2eAgFnSc2d1	Česká
Necyklopedie	Necyklopedie	k1gFnSc2	Necyklopedie
sepsané	sepsaný	k2eAgFnSc2d1	sepsaná
jejími	její	k3xOp3gNnPc7	její
autory	autor	k1gMnPc7	autor
</s>
</p>
<p>
<s>
31	[number]	k4	31
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2007	[number]	k4	2007
vyšel	vyjít	k5eAaPmAgInS	vyjít
v	v	k7c6	v
deníku	deník	k1gInSc6	deník
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
DNES	dnes	k6eAd1	dnes
článek	článek	k1gInSc4	článek
o	o	k7c6	o
české	český	k2eAgFnSc6d1	Česká
Necyklopedii	Necyklopedie	k1gFnSc6	Necyklopedie
(	(	kIx(	(
<g/>
přepis	přepis	k1gInSc1	přepis
<g/>
)	)	kIx)	)
</s>
</p>
