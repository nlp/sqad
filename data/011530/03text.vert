<p>
<s>
Tank	tank	k1gInSc1	tank
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
obrněné	obrněný	k2eAgNnSc1d1	obrněné
bojové	bojový	k2eAgNnSc1d1	bojové
vozidlo	vozidlo	k1gNnSc1	vozidlo
s	s	k7c7	s
pásy	pás	k1gInPc7	pás
<g/>
,	,	kIx,	,
věží	věž	k1gFnSc7	věž
a	a	k8xC	a
kanónem	kanón	k1gInSc7	kanón
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
primárním	primární	k2eAgInSc7d1	primární
účelem	účel	k1gInSc7	účel
je	být	k5eAaImIp3nS	být
likvidace	likvidace	k1gFnSc1	likvidace
nepřátelských	přátelský	k2eNgFnPc2d1	nepřátelská
pozemních	pozemní	k2eAgFnPc2d1	pozemní
sil	síla	k1gFnPc2	síla
přímou	přímý	k2eAgFnSc7d1	přímá
palbou	palba	k1gFnSc7	palba
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgInPc1d1	moderní
tanky	tank	k1gInPc1	tank
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
velkou	velký	k2eAgFnSc7d1	velká
palebnou	palebný	k2eAgFnSc7d1	palebná
silou	síla	k1gFnSc7	síla
(	(	kIx(	(
<g/>
přičemž	přičemž	k6eAd1	přičemž
jejich	jejich	k3xOp3gFnSc4	jejich
nejobvyklejší	obvyklý	k2eAgFnSc4d3	nejobvyklejší
hlavní	hlavní	k2eAgFnSc4d1	hlavní
výzbroj	výzbroj	k1gFnSc4	výzbroj
představuje	představovat	k5eAaImIp3nS	představovat
kanón	kanón	k1gInSc1	kanón
<g/>
)	)	kIx)	)
a	a	k8xC	a
vysokou	vysoký	k2eAgFnSc7d1	vysoká
úrovní	úroveň	k1gFnSc7	úroveň
pancéřové	pancéřový	k2eAgFnSc2d1	pancéřová
ochrany	ochrana	k1gFnSc2	ochrana
a	a	k8xC	a
mobility	mobilita	k1gFnSc2	mobilita
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
vysokou	vysoký	k2eAgFnSc7d1	vysoká
rychlostí	rychlost	k1gFnSc7	rychlost
pohybovat	pohybovat	k5eAaImF	pohybovat
terénem	terén	k1gInSc7	terén
<g/>
,	,	kIx,	,
nejsou	být	k5eNaImIp3nP	být
ale	ale	k8xC	ale
určeny	určit	k5eAaPmNgFnP	určit
pro	pro	k7c4	pro
nepřetržitý	přetržitý	k2eNgInSc4d1	nepřetržitý
boj	boj	k1gInSc4	boj
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
pravidelnou	pravidelný	k2eAgFnSc4d1	pravidelná
a	a	k8xC	a
náročnou	náročný	k2eAgFnSc4d1	náročná
údržbu	údržba	k1gFnSc4	údržba
a	a	k8xC	a
časté	častý	k2eAgNnSc1d1	časté
doplňování	doplňování	k1gNnSc1	doplňování
munice	munice	k1gFnSc2	munice
a	a	k8xC	a
paliva	palivo	k1gNnSc2	palivo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tank	tank	k1gInSc1	tank
je	být	k5eAaImIp3nS	být
nejlépe	dobře	k6eAd3	dobře
pancéřovaný	pancéřovaný	k2eAgInSc4d1	pancéřovaný
bojový	bojový	k2eAgInSc4d1	bojový
prostředek	prostředek	k1gInSc4	prostředek
ze	z	k7c2	z
všech	všecek	k3xTgNnPc2	všecek
pozemních	pozemní	k2eAgNnPc2d1	pozemní
bojových	bojový	k2eAgNnPc2d1	bojové
vozidel	vozidlo	k1gNnPc2	vozidlo
a	a	k8xC	a
nese	nést	k5eAaImIp3nS	nést
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
nejefektivnější	efektivní	k2eAgFnSc1d3	nejefektivnější
protitankové	protitankový	k2eAgFnPc4d1	protitanková
zbraně	zbraň	k1gFnPc4	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
charakteristiky	charakteristika	k1gFnPc1	charakteristika
společně	společně	k6eAd1	společně
s	s	k7c7	s
intenzívním	intenzívní	k2eAgInSc7d1	intenzívní
hlukem	hluk	k1gInSc7	hluk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
vydává	vydávat	k5eAaImIp3nS	vydávat
pohybující	pohybující	k2eAgFnSc1d1	pohybující
se	se	k3xPyFc4	se
tanková	tankový	k2eAgFnSc1d1	tanková
formace	formace	k1gFnSc1	formace
<g/>
,	,	kIx,	,
dělají	dělat	k5eAaImIp3nP	dělat
z	z	k7c2	z
tanku	tank	k1gInSc2	tank
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejobávanějších	obávaný	k2eAgFnPc2d3	nejobávanější
zbraní	zbraň	k1gFnPc2	zbraň
na	na	k7c6	na
bitevním	bitevní	k2eAgNnSc6d1	bitevní
poli	pole	k1gNnSc6	pole
<g/>
,	,	kIx,	,
ceněnou	ceněný	k2eAgFnSc4d1	ceněná
pro	pro	k7c4	pro
šok	šok	k1gInSc4	šok
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
působí	působit	k5eAaImIp3nS	působit
na	na	k7c4	na
jiné	jiný	k2eAgInPc4d1	jiný
typy	typ	k1gInPc4	typ
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Tanky	tank	k1gInPc1	tank
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
nasazeny	nasadit	k5eAaPmNgInP	nasadit
v	v	k7c6	v
boji	boj	k1gInSc6	boj
samostatně	samostatně	k6eAd1	samostatně
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
přehledném	přehledný	k2eAgInSc6d1	přehledný
terénu	terén	k1gInSc6	terén
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
s	s	k7c7	s
ostatními	ostatní	k2eAgInPc7d1	ostatní
typy	typ	k1gInPc7	typ
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
dělostřelectva	dělostřelectvo	k1gNnSc2	dělostřelectvo
a	a	k8xC	a
pěchoty	pěchota	k1gFnSc2	pěchota
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
podpory	podpora	k1gFnSc2	podpora
pěchoty	pěchota	k1gFnSc2	pěchota
se	se	k3xPyFc4	se
tanky	tank	k1gInPc7	tank
zpravidla	zpravidla	k6eAd1	zpravidla
neobejdou	obejít	k5eNaPmIp3nP	obejít
při	při	k7c6	při
boji	boj	k1gInSc6	boj
v	v	k7c6	v
málo	málo	k6eAd1	málo
přehledných	přehledný	k2eAgInPc6d1	přehledný
terénech	terén	k1gInPc6	terén
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
les	les	k1gInSc1	les
<g/>
,	,	kIx,	,
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
hustá	hustý	k2eAgFnSc1d1	hustá
vysoká	vysoký	k2eAgFnSc1d1	vysoká
tráva	tráva	k1gFnSc1	tráva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tank	tank	k1gInSc1	tank
je	být	k5eAaImIp3nS	být
však	však	k9	však
také	také	k9	také
výbornou	výborný	k2eAgFnSc7d1	výborná
defenzívní	defenzívní	k2eAgFnSc7d1	defenzívní
zbraní	zbraň	k1gFnSc7	zbraň
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
jej	on	k3xPp3gMnSc4	on
použít	použít	k5eAaPmF	použít
ve	v	k7c6	v
statické	statický	k2eAgFnSc6d1	statická
obraně	obrana	k1gFnSc6	obrana
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
okopu	okop	k1gInSc6	okop
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
případě	případ	k1gInSc6	případ
potřeby	potřeba	k1gFnSc2	potřeba
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
provést	provést	k5eAaPmF	provést
protiútok	protiútok	k1gInSc4	protiútok
a	a	k8xC	a
zase	zase	k9	zase
se	se	k3xPyFc4	se
stáhnout	stáhnout	k5eAaPmF	stáhnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vojenská	vojenský	k2eAgNnPc1d1	vojenské
cvičiště	cvičiště	k1gNnPc1	cvičiště
pro	pro	k7c4	pro
tanky	tank	k1gInPc1	tank
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
tankodromy	tankodrom	k1gInPc1	tankodrom
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
jejich	jejich	k3xOp3gInSc3	jejich
specifickému	specifický	k2eAgInSc3d1	specifický
povrchu	povrch	k1gInSc3	povrch
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
slovo	slovo	k1gNnSc1	slovo
někdy	někdy	k6eAd1	někdy
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
přeneseném	přenesený	k2eAgInSc6d1	přenesený
významu	význam	k1gInSc6	význam
i	i	k9	i
pro	pro	k7c4	pro
nekvalitní	kvalitní	k2eNgFnPc4d1	nekvalitní
komunikace	komunikace	k1gFnPc4	komunikace
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
silnice	silnice	k1gFnPc1	silnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vývoj	vývoj	k1gInSc1	vývoj
a	a	k8xC	a
vznik	vznik	k1gInSc1	vznik
tanku	tank	k1gInSc2	tank
==	==	k?	==
</s>
</p>
<p>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
historických	historický	k2eAgFnPc2d1	historická
součástí	součást	k1gFnPc2	součást
tanku	tank	k1gInSc2	tank
-	-	kIx~	-
housenkové	housenkový	k2eAgInPc1d1	housenkový
pásy	pás	k1gInPc1	pás
-	-	kIx~	-
byly	být	k5eAaImAgFnP	být
vymyšleny	vymyslet	k5eAaPmNgFnP	vymyslet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1770	[number]	k4	1770
Richardem	Richard	k1gMnSc7	Richard
Edgeworthem	Edgeworth	k1gInSc7	Edgeworth
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tyto	tento	k3xDgInPc1	tento
pásy	pás	k1gInPc1	pás
byly	být	k5eAaImAgInP	být
použity	použít	k5eAaPmNgInP	použít
na	na	k7c6	na
relativně	relativně	k6eAd1	relativně
malém	malý	k2eAgInSc6d1	malý
počtu	počet	k1gInSc6	počet
traktorů	traktor	k1gInPc2	traktor
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnPc3	který
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
tomu	ten	k3xDgMnSc3	ten
značně	značně	k6eAd1	značně
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
průchodivost	průchodivost	k1gFnSc1	průchodivost
v	v	k7c6	v
blátivém	blátivý	k2eAgInSc6d1	blátivý
terénu	terén	k1gInSc6	terén
během	během	k7c2	během
krymské	krymský	k2eAgFnSc2d1	Krymská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dalším	další	k2eAgInSc7d1	další
důležitým	důležitý	k2eAgInSc7d1	důležitý
mezníkem	mezník	k1gInSc7	mezník
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
tanků	tank	k1gInPc2	tank
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1899	[number]	k4	1899
první	první	k4xOgInSc1	první
"	"	kIx"	"
<g/>
válečný	válečný	k2eAgInSc1d1	válečný
motorový	motorový	k2eAgInSc1d1	motorový
vůz	vůz	k1gInSc1	vůz
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
vozidlo	vozidlo	k1gNnSc1	vozidlo
bylo	být	k5eAaImAgNnS	být
vybaveno	vybavit	k5eAaPmNgNnS	vybavit
motorem	motor	k1gInSc7	motor
Daimler	Daimler	k1gInSc1	Daimler
<g/>
,	,	kIx,	,
pancéřováním	pancéřování	k1gNnSc7	pancéřování
a	a	k8xC	a
dvěma	dva	k4xCgInPc7	dva
kulomety	kulomet	k1gInPc7	kulomet
revolverového	revolverový	k2eAgInSc2d1	revolverový
systému	systém	k1gInSc2	systém
od	od	k7c2	od
Hirama	Hiramum	k1gNnSc2	Hiramum
Maxima	maximum	k1gNnSc2	maximum
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
vozidla	vozidlo	k1gNnPc1	vozidlo
byla	být	k5eAaImAgNnP	být
použita	použít	k5eAaPmNgNnP	použít
v	v	k7c6	v
anglické	anglický	k2eAgFnSc6d1	anglická
armádě	armáda	k1gFnSc6	armáda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
vzniku	vznik	k1gInSc6	vznik
prvního	první	k4xOgInSc2	první
tanku	tank	k1gInSc2	tank
se	se	k3xPyFc4	se
také	také	k9	také
podílel	podílet	k5eAaImAgInS	podílet
While	While	k1gInSc1	While
Hankey	Hankea	k1gFnSc2	Hankea
26	[number]	k4	26
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1914	[number]	k4	1914
v	v	k7c6	v
oběžníku	oběžník	k1gInSc6	oběžník
"	"	kIx"	"
<g/>
Memorandum	memorandum	k1gNnSc1	memorandum
o	o	k7c6	o
'	'	kIx"	'
<g/>
speciálním	speciální	k2eAgNnSc6d1	speciální
zařízení	zařízení	k1gNnSc6	zařízení
<g/>
'	'	kIx"	'
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
memorandum	memorandum	k1gNnSc1	memorandum
on	on	k3xPp3gMnSc1	on
'	'	kIx"	'
<g/>
special	special	k1gMnSc1	special
devices	devices	k1gMnSc1	devices
<g/>
'	'	kIx"	'
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1915	[number]	k4	1915
předveden	předvést	k5eAaPmNgInS	předvést
stroj	stroj	k1gInSc1	stroj
Killen-Straita	Killen-Straitum	k1gNnSc2	Killen-Straitum
-	-	kIx~	-
to	ten	k3xDgNnSc1	ten
už	už	k6eAd1	už
ale	ale	k8xC	ale
uběhl	uběhnout	k5eAaPmAgMnS	uběhnout
téměř	téměř	k6eAd1	téměř
rok	rok	k1gInSc4	rok
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
S	s	k7c7	s
prvním	první	k4xOgInSc7	první
návrhem	návrh	k1gInSc7	návrh
tanku	tank	k1gInSc2	tank
přišel	přijít	k5eAaPmAgMnS	přijít
vynálezce	vynálezce	k1gMnSc1	vynálezce
Leonardo	Leonardo	k1gMnSc1	Leonardo
Da	Da	k1gMnSc1	Da
Vinci	Vinca	k1gMnSc2	Vinca
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tanky	tank	k1gInPc1	tank
byly	být	k5eAaImAgInP	být
poprvé	poprvé	k6eAd1	poprvé
použity	použít	k5eAaPmNgInP	použít
v	v	k7c6	v
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Samotné	samotný	k2eAgNnSc1d1	samotné
slovo	slovo	k1gNnSc1	slovo
tank	tank	k1gInSc1	tank
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
nádrž	nádrž	k1gFnSc1	nádrž
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
krycí	krycí	k2eAgInSc1d1	krycí
název	název	k1gInSc1	název
projektu	projekt	k1gInSc2	projekt
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
tank	tank	k1gInSc1	tank
se	se	k3xPyFc4	se
zvolilo	zvolit	k5eAaPmAgNnS	zvolit
a	a	k8xC	a
vžilo	vžít	k5eAaPmAgNnS	vžít
pro	pro	k7c4	pro
technologickou	technologický	k2eAgFnSc4d1	technologická
podobnost	podobnost	k1gFnSc4	podobnost
výroby	výroba	k1gFnSc2	výroba
s	s	k7c7	s
nádržemi	nádrž	k1gFnPc7	nádrž
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
tanky	tank	k1gInPc7	tank
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInPc1d1	původní
tanky	tank	k1gInPc1	tank
byly	být	k5eAaImAgInP	být
vyvinuty	vyvinout	k5eAaPmNgInP	vyvinout
pro	pro	k7c4	pro
rozhodující	rozhodující	k2eAgFnSc4d1	rozhodující
změnu	změna	k1gFnSc4	změna
patové	patový	k2eAgFnSc2d1	patová
situace	situace	k1gFnSc2	situace
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
po	po	k7c6	po
hromadném	hromadný	k2eAgNnSc6d1	hromadné
zavedení	zavedení	k1gNnSc6	zavedení
kulometů	kulomet	k1gInPc2	kulomet
<g/>
,	,	kIx,	,
ostnatého	ostnatý	k2eAgInSc2d1	ostnatý
drátu	drát	k1gInSc2	drát
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
zákopové	zákopový	k2eAgFnSc2d1	zákopová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dnešní	dnešní	k2eAgFnSc1d1	dnešní
koncepce	koncepce	k1gFnSc1	koncepce
tanků	tank	k1gInPc2	tank
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
v	v	k7c6	v
drtivé	drtivý	k2eAgFnSc6d1	drtivá
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
věže	věž	k1gFnSc2	věž
a	a	k8xC	a
jednoho	jeden	k4xCgInSc2	jeden
kanónu	kanón	k1gInSc2	kanón
<g/>
.	.	kIx.	.
</s>
<s>
Ne	ne	k9	ne
vždy	vždy	k6eAd1	vždy
tomu	ten	k3xDgNnSc3	ten
ale	ale	k9	ale
tak	tak	k6eAd1	tak
bylo	být	k5eAaImAgNnS	být
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
přelomu	přelom	k1gInSc2	přelom
dvacátých	dvacátý	k4xOgNnPc2	dvacátý
a	a	k8xC	a
třicátých	třicátý	k4xOgNnPc2	třicátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
uváděna	uvádět	k5eAaImNgFnS	uvádět
ve	v	k7c4	v
skutečnost	skutečnost	k1gFnSc4	skutečnost
i	i	k8xC	i
myšlenka	myšlenka	k1gFnSc1	myšlenka
vícevěžových	vícevěžový	k2eAgNnPc2d1	vícevěžový
<g/>
.	.	kIx.	.
</s>
<s>
Výhoda	výhoda	k1gFnSc1	výhoda
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
první	první	k4xOgInSc4	první
pohled	pohled	k1gInSc4	pohled
zřejmá	zřejmý	k2eAgFnSc1d1	zřejmá
<g/>
:	:	kIx,	:
Možnost	možnost	k1gFnSc1	možnost
vést	vést	k5eAaImF	vést
palbu	palba	k1gFnSc4	palba
na	na	k7c4	na
více	hodně	k6eAd2	hodně
cílů	cíl	k1gInPc2	cíl
zároveň	zároveň	k6eAd1	zároveň
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
ale	ale	k9	ale
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nevýhody	nevýhoda	k1gFnPc4	nevýhoda
toto	tento	k3xDgNnSc4	tento
uspořádání	uspořádání	k1gNnSc4	uspořádání
značně	značně	k6eAd1	značně
toto	tento	k3xDgNnSc4	tento
pozitivum	pozitivum	k1gNnSc4	pozitivum
převyšují	převyšovat	k5eAaImIp3nP	převyšovat
(	(	kIx(	(
<g/>
především	především	k6eAd1	především
technická	technický	k2eAgFnSc1d1	technická
poruchovost	poruchovost	k1gFnSc1	poruchovost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vrcholem	vrchol	k1gInSc7	vrchol
vícevěžových	vícevěžový	k2eAgInPc2d1	vícevěžový
tanků	tank	k1gInPc2	tank
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
ruský	ruský	k2eAgMnSc1d1	ruský
pětivěžový	pětivěžový	k2eAgMnSc1d1	pětivěžový
T-	T-	k1gMnSc1	T-
<g/>
35	[number]	k4	35
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
dočkal	dočkat	k5eAaPmAgMnS	dočkat
nasazení	nasazení	k1gNnSc2	nasazení
ještě	ještě	k6eAd1	ještě
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
specifickým	specifický	k2eAgFnPc3d1	specifická
podmínkám	podmínka	k1gFnPc3	podmínka
boje	boj	k1gInSc2	boj
ve	v	k7c6	v
městech	město	k1gNnPc6	město
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
nevyhnutelnou	vyhnutelný	k2eNgFnSc7d1	nevyhnutelná
součástí	součást	k1gFnSc7	součást
současného	současný	k2eAgNnSc2d1	současné
bojového	bojový	k2eAgNnSc2d1	bojové
nasazení	nasazení	k1gNnSc2	nasazení
tanků	tank	k1gInPc2	tank
<g/>
,	,	kIx,	,
i	i	k8xC	i
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
vícevěžová	vícevěžový	k2eAgFnSc1d1	vícevěžový
koncepce	koncepce	k1gFnSc1	koncepce
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
vlastně	vlastně	k9	vlastně
každá	každý	k3xTgFnSc1	každý
dálkově	dálkově	k6eAd1	dálkově
ovládaná	ovládaný	k2eAgFnSc1d1	ovládaná
stanice	stanice	k1gFnSc1	stanice
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
vyzbrojená	vyzbrojený	k2eAgFnSc1d1	vyzbrojená
kulometem	kulomet	k1gInSc7	kulomet
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
nic	nic	k3yNnSc1	nic
jiného	jiný	k2eAgMnSc4d1	jiný
než	než	k8xS	než
další	další	k2eAgFnSc4d1	další
věž	věž	k1gFnSc4	věž
tanku	tank	k1gInSc2	tank
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
koncepce	koncepce	k1gFnSc1	koncepce
nabízí	nabízet	k5eAaImIp3nS	nabízet
prakticky	prakticky	k6eAd1	prakticky
jen	jen	k9	jen
výhody	výhoda	k1gFnSc2	výhoda
-	-	kIx~	-
posádka	posádka	k1gFnSc1	posádka
je	být	k5eAaImIp3nS	být
chráněná	chráněný	k2eAgFnSc1d1	chráněná
uvnitř	uvnitř	k7c2	uvnitř
tanku	tank	k1gInSc2	tank
<g/>
,	,	kIx,	,
stanice	stanice	k1gFnSc1	stanice
slouží	sloužit	k5eAaImIp3nS	sloužit
nejen	nejen	k6eAd1	nejen
k	k	k7c3	k
likvidaci	likvidace	k1gFnSc3	likvidace
nepřítele	nepřítel	k1gMnSc2	nepřítel
ale	ale	k8xC	ale
i	i	k9	i
k	k	k7c3	k
pozorování	pozorování	k1gNnSc3	pozorování
okolí	okolí	k1gNnSc2	okolí
<g/>
,	,	kIx,	,
stanice	stanice	k1gFnSc1	stanice
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
osadit	osadit	k5eAaPmF	osadit
poměrně	poměrně	k6eAd1	poměrně
pestrou	pestrý	k2eAgFnSc7d1	pestrá
paletou	paleta	k1gFnSc7	paleta
výzbroje	výzbroj	k1gFnSc2	výzbroj
(	(	kIx(	(
<g/>
kulomety	kulomet	k1gInPc1	kulomet
<g/>
,	,	kIx,	,
granátomety	granátomet	k1gInPc1	granátomet
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jediná	jediný	k2eAgFnSc1d1	jediná
nevýhoda	nevýhoda	k1gFnSc1	nevýhoda
je	být	k5eAaImIp3nS	být
samozřejmě	samozřejmě	k6eAd1	samozřejmě
vyšší	vysoký	k2eAgFnSc1d2	vyšší
cena	cena	k1gFnSc1	cena
oproti	oproti	k7c3	oproti
prosté	prostý	k2eAgFnSc3d1	prostá
lafetaci	lafetace	k1gFnSc3	lafetace
standardního	standardní	k2eAgInSc2d1	standardní
kulometu	kulomet	k1gInSc2	kulomet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
při	při	k7c6	při
současném	současný	k2eAgInSc6d1	současný
rychlém	rychlý	k2eAgInSc6d1	rychlý
rozvoji	rozvoj	k1gInSc6	rozvoj
elektroniky	elektronika	k1gFnSc2	elektronika
a	a	k8xC	a
optiky	optika	k1gFnSc2	optika
budou	být	k5eAaImBp3nP	být
zřejmě	zřejmě	k6eAd1	zřejmě
náklady	náklad	k1gInPc4	náklad
klesat	klesat	k5eAaImF	klesat
a	a	k8xC	a
tento	tento	k3xDgInSc1	tento
trend	trend	k1gInSc1	trend
pokračovat	pokračovat	k5eAaImF	pokračovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
tanky	tank	k1gInPc7	tank
staly	stát	k5eAaPmAgFnP	stát
rozhodující	rozhodující	k2eAgFnSc7d1	rozhodující
zbraní	zbraň	k1gFnSc7	zbraň
pozemních	pozemní	k2eAgFnPc2d1	pozemní
bitev	bitva	k1gFnPc2	bitva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
konci	konec	k1gInSc6	konec
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
ještě	ještě	k9	ještě
konstruktéři	konstruktér	k1gMnPc1	konstruktér
tanků	tank	k1gInPc2	tank
hledali	hledat	k5eAaImAgMnP	hledat
směr	směr	k1gInSc4	směr
<g/>
,	,	kIx,	,
jakým	jaký	k3yQgFnPc3	jaký
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
vývoj	vývoj	k1gInSc1	vývoj
ubírat	ubírat	k5eAaImF	ubírat
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
se	se	k3xPyFc4	se
ubíral	ubírat	k5eAaImAgInS	ubírat
cestou	cestou	k7c2	cestou
vylepšování	vylepšování	k1gNnSc2	vylepšování
tanků	tank	k1gInPc2	tank
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
SSSR	SSSR	kA	SSSR
byla	být	k5eAaImAgFnS	být
vylepšena	vylepšit	k5eAaPmNgFnS	vylepšit
konstrukce	konstrukce	k1gFnSc1	konstrukce
T-44	T-44	k1gFnSc1	T-44
z	z	k7c2	z
konce	konec	k1gInSc2	konec
války	válka	k1gFnSc2	válka
a	a	k8xC	a
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
T-	T-	k1gFnSc4	T-
<g/>
55	[number]	k4	55
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
USA	USA	kA	USA
vyvinuli	vyvinout	k5eAaPmAgMnP	vyvinout
z	z	k7c2	z
tanku	tank	k1gInSc2	tank
M26	M26	k1gFnSc2	M26
Pershing	Pershing	k1gInSc1	Pershing
varianty	varianta	k1gFnSc2	varianta
M	M	kA	M
<g/>
47	[number]	k4	47
<g/>
,	,	kIx,	,
M48	M48	k1gFnSc1	M48
a	a	k8xC	a
M60	M60	k1gFnSc1	M60
Patton	Patton	k1gInSc1	Patton
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
stále	stále	k6eAd1	stále
koexistovaly	koexistovat	k5eAaImAgInP	koexistovat
tanky	tank	k1gInPc1	tank
lehké	lehký	k2eAgInPc1d1	lehký
<g/>
,	,	kIx,	,
střední	střední	k2eAgInPc1d1	střední
a	a	k8xC	a
těžké	těžký	k2eAgInPc1d1	těžký
(	(	kIx(	(
<g/>
zastoupené	zastoupený	k2eAgInPc1d1	zastoupený
např.	např.	kA	např.
americkým	americký	k2eAgMnSc7d1	americký
M	M	kA	M
<g/>
103	[number]	k4	103
<g/>
,	,	kIx,	,
britským	britský	k2eAgNnSc7d1	Britské
Conqueror	Conqueror	k1gInSc4	Conqueror
nebo	nebo	k8xC	nebo
ruskými	ruský	k2eAgFnPc7d1	ruská
IS-2	IS-2	k1gFnPc7	IS-2
a	a	k8xC	a
T-	T-	k1gFnSc7	T-
<g/>
10	[number]	k4	10
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Časem	čas	k1gInSc7	čas
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
kategorie	kategorie	k1gFnSc1	kategorie
těžkých	těžký	k2eAgInPc2d1	těžký
tanků	tank	k1gInPc2	tank
a	a	k8xC	a
vyvíjely	vyvíjet	k5eAaImAgFnP	vyvíjet
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
kategorie	kategorie	k1gFnSc1	kategorie
středních	střední	k2eAgInPc2d1	střední
tanků	tank	k1gInPc2	tank
<g/>
,	,	kIx,	,
v	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
kategorie	kategorie	k1gFnPc1	kategorie
tanků	tank	k1gInPc2	tank
lehkých	lehký	k2eAgInPc2d1	lehký
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
60	[number]	k4	60
<g/>
.	.	kIx.	.
a	a	k8xC	a
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
zavedl	zavést	k5eAaPmAgInS	zavést
SSSR	SSSR	kA	SSSR
novou	nový	k2eAgFnSc4d1	nová
řadu	řada	k1gFnSc4	řada
tanků	tank	k1gInPc2	tank
-	-	kIx~	-
T-	T-	k1gFnPc2	T-
<g/>
62	[number]	k4	62
<g/>
,	,	kIx,	,
T-64	T-64	k1gFnSc1	T-64
a	a	k8xC	a
T-	T-	k1gFnSc1	T-
<g/>
72	[number]	k4	72
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
předčily	předčit	k5eAaBmAgFnP	předčit
tehdejší	tehdejší	k2eAgInPc4d1	tehdejší
americké	americký	k2eAgInPc4d1	americký
stroje	stroj	k1gInPc4	stroj
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
ohledech	ohled	k1gInPc6	ohled
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
tehdy	tehdy	k6eAd1	tehdy
existovaly	existovat	k5eAaImAgFnP	existovat
jen	jen	k9	jen
2	[number]	k4	2
tanky	tank	k1gInPc1	tank
schopné	schopný	k2eAgInPc1d1	schopný
se	se	k3xPyFc4	se
těmto	tento	k3xDgMnPc3	tento
postavit	postavit	k5eAaPmF	postavit
-	-	kIx~	-
německý	německý	k2eAgMnSc1d1	německý
Leopard	leopard	k1gMnSc1	leopard
a	a	k8xC	a
hlavně	hlavně	k6eAd1	hlavně
britský	britský	k2eAgInSc1d1	britský
Chieftain	Chieftain	k1gInSc1	Chieftain
<g/>
.	.	kIx.	.
</s>
<s>
Chieftain	Chieftain	k1gInSc1	Chieftain
byl	být	k5eAaImAgInS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
nejlépe	dobře	k6eAd3	dobře
vyzbrojený	vyzbrojený	k2eAgInSc4d1	vyzbrojený
a	a	k8xC	a
pancéřovaný	pancéřovaný	k2eAgInSc4d1	pancéřovaný
tank	tank	k1gInSc4	tank
NATO	NATO	kA	NATO
než	než	k8xS	než
jej	on	k3xPp3gMnSc4	on
začal	začít	k5eAaPmAgMnS	začít
nahrazovat	nahrazovat	k5eAaImF	nahrazovat
nový	nový	k2eAgInSc4d1	nový
typ	typ	k1gInSc4	typ
Challenger	Challenger	k1gInSc4	Challenger
1	[number]	k4	1
se	s	k7c7	s
speciálním	speciální	k2eAgNnSc7d1	speciální
pancéřováním	pancéřování	k1gNnSc7	pancéřování
Chobham	Chobham	k1gInSc1	Chobham
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
USA	USA	kA	USA
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
hledaly	hledat	k5eAaImAgFnP	hledat
cestu	cesta	k1gFnSc4	cesta
k	k	k7c3	k
novému	nový	k2eAgInSc3d1	nový
tanku	tank	k1gInSc3	tank
<g/>
,	,	kIx,	,
a	a	k8xC	a
po	po	k7c6	po
zrušení	zrušení	k1gNnSc6	zrušení
společného	společný	k2eAgInSc2d1	společný
projektu	projekt	k1gInSc2	projekt
přetechnizovaného	přetechnizovaný	k2eAgInSc2d1	přetechnizovaný
tanku	tank	k1gInSc2	tank
MBT-70	MBT-70	k1gMnPc2	MBT-70
se	se	k3xPyFc4	se
oba	dva	k4xCgInPc1	dva
státy	stát	k1gInPc1	stát
vydaly	vydat	k5eAaPmAgInP	vydat
cestou	cestou	k7c2	cestou
vlastního	vlastní	k2eAgInSc2d1	vlastní
vývoje	vývoj	k1gInSc2	vývoj
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
v	v	k7c6	v
USA	USA	kA	USA
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
typ	typ	k1gInSc1	typ
M1	M1	k1gFnSc2	M1
Abrams	Abramsa	k1gFnPc2	Abramsa
a	a	k8xC	a
v	v	k7c6	v
západním	západní	k2eAgNnSc6d1	západní
Německu	Německo	k1gNnSc6	Německo
typ	typ	k1gInSc1	typ
Leopard	leopard	k1gMnSc1	leopard
2	[number]	k4	2
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
oba	dva	k4xCgInPc1	dva
tanky	tank	k1gInPc1	tank
(	(	kIx(	(
<g/>
americký	americký	k2eAgMnSc1d1	americký
od	od	k7c2	od
verze	verze	k1gFnSc2	verze
A	a	k9	a
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
používali	používat	k5eAaImAgMnP	používat
stejnou	stejný	k2eAgFnSc4d1	stejná
hlavní	hlavní	k2eAgFnSc4d1	hlavní
zbraň	zbraň	k1gFnSc4	zbraň
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
120	[number]	k4	120
<g/>
mm	mm	kA	mm
kanón	kanón	k1gInSc4	kanón
Rheinmetall	Rheinmetalla	k1gFnPc2	Rheinmetalla
Rh-	Rh-	k1gMnPc2	Rh-
<g/>
120	[number]	k4	120
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rusové	Rus	k1gMnPc1	Rus
samozřejmě	samozřejmě	k6eAd1	samozřejmě
nemohli	moct	k5eNaImAgMnP	moct
nereagovat	reagovat	k5eNaBmF	reagovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
tanky	tank	k1gInPc1	tank
T-72B	T-72B	k1gMnPc2	T-72B
a	a	k8xC	a
T-	T-	k1gFnPc2	T-
<g/>
80	[number]	k4	80
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
měly	mít	k5eAaImAgFnP	mít
zesílený	zesílený	k2eAgInSc4d1	zesílený
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
T-80	T-80	k1gFnSc2	T-80
také	také	k9	také
kompozitní	kompozitní	k2eAgInSc1d1	kompozitní
pancíř	pancíř	k1gInSc1	pancíř
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Všeobecně	všeobecně	k6eAd1	všeobecně
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
platilo	platit	k5eAaImAgNnS	platit
<g/>
,	,	kIx,	,
sovětské	sovětský	k2eAgInPc1d1	sovětský
tanky	tank	k1gInPc1	tank
spoléhaly	spoléhat	k5eAaImAgInP	spoléhat
na	na	k7c4	na
manévrovatelnost	manévrovatelnost	k1gFnSc4	manévrovatelnost
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
americké	americký	k2eAgFnPc1d1	americká
M1	M1	k1gFnPc1	M1
spoléhaly	spoléhat	k5eAaImAgFnP	spoléhat
na	na	k7c4	na
zepředu	zepředu	k6eAd1	zepředu
neprorazitelný	prorazitelný	k2eNgInSc4d1	neprorazitelný
pancíř	pancíř	k1gInSc4	pancíř
<g/>
,	,	kIx,	,
doplněný	doplněný	k2eAgInSc4d1	doplněný
ve	v	k7c4	v
verzi	verze	k1gFnSc4	verze
M1A1HA	M1A1HA	k1gFnSc1	M1A1HA
vrstvou	vrstva	k1gFnSc7	vrstva
ochuzeného	ochuzený	k2eAgInSc2d1	ochuzený
uranu	uran	k1gInSc2	uran
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yQgMnSc7	který
měl	mít	k5eAaImAgMnS	mít
pancíř	pancíř	k1gInSc4	pancíř
RHA	RHA	kA	RHA
ekvivalent	ekvivalent	k1gInSc4	ekvivalent
1,3	[number]	k4	1,3
m	m	kA	m
silného	silný	k2eAgInSc2d1	silný
pancíře	pancíř	k1gInSc2	pancíř
z	z	k7c2	z
homogenní	homogenní	k2eAgFnSc2d1	homogenní
válcované	válcovaný	k2eAgFnSc2d1	válcovaná
oceli	ocel	k1gFnSc2	ocel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vlivem	vliv	k1gInSc7	vliv
zvyšování	zvyšování	k1gNnSc2	zvyšování
pancéřové	pancéřový	k2eAgFnSc2d1	pancéřová
ochrany	ochrana	k1gFnSc2	ochrana
tanků	tank	k1gInPc2	tank
(	(	kIx(	(
<g/>
více	hodně	k6eAd2	hodně
pancíře	pancíř	k1gInPc1	pancíř
=	=	kIx~	=
větší	veliký	k2eAgFnSc1d2	veliký
hmotnost	hmotnost	k1gFnSc1	hmotnost
<g/>
,	,	kIx,	,
potřeba	potřeba	k1gFnSc1	potřeba
zachování	zachování	k1gNnSc2	zachování
stejné	stejný	k2eAgFnSc2d1	stejná
rychlosti	rychlost	k1gFnSc2	rychlost
těžšího	těžký	k2eAgInSc2d2	těžší
tanku	tank	k1gInSc2	tank
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
instalaci	instalace	k1gFnSc4	instalace
výkonnějšího	výkonný	k2eAgInSc2d2	výkonnější
=	=	kIx~	=
těžšího	těžký	k2eAgInSc2d2	těžší
motoru	motor	k1gInSc2	motor
<g/>
)	)	kIx)	)
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
hmotnost	hmotnost	k1gFnSc1	hmotnost
středních	střední	k2eAgInPc2d1	střední
tanků	tank	k1gInPc2	tank
na	na	k7c4	na
úroveň	úroveň	k1gFnSc4	úroveň
těžkých	těžký	k2eAgInPc2d1	těžký
tanků	tank	k1gInPc2	tank
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vedlo	vést	k5eAaImAgNnS	vést
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
nové	nový	k2eAgFnSc2d1	nová
třídy	třída	k1gFnSc2	třída
tanků	tank	k1gInPc2	tank
nazvané	nazvaný	k2eAgFnSc2d1	nazvaná
hlavní	hlavní	k2eAgFnSc2d1	hlavní
bojový	bojový	k2eAgInSc4d1	bojový
tank	tank	k1gInSc4	tank
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
Main	Main	k1gMnSc1	Main
Battle	Battle	k1gFnSc2	Battle
Tank	tank	k1gInSc1	tank
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
MBT	MBT	kA	MBT
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nahradila	nahradit	k5eAaPmAgFnS	nahradit
kategorii	kategorie	k1gFnSc4	kategorie
středních	střední	k2eAgInPc2d1	střední
tanků	tank	k1gInPc2	tank
<g/>
.	.	kIx.	.
</s>
<s>
Těch	ten	k3xDgInPc2	ten
pár	pár	k4xCyI	pár
typů	typ	k1gInPc2	typ
třídy	třída	k1gFnSc2	třída
lehkých	lehký	k2eAgInPc2d1	lehký
tanků	tank	k1gInPc2	tank
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
poté	poté	k6eAd1	poté
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
označovány	označovat	k5eAaImNgFnP	označovat
jako	jako	k8xC	jako
stíhač	stíhač	k1gMnSc1	stíhač
tanků	tank	k1gInPc2	tank
<g/>
,	,	kIx,	,
či	či	k8xC	či
tanky	tank	k1gInPc1	tank
pro	pro	k7c4	pro
výsadkové	výsadkový	k2eAgNnSc4d1	výsadkové
vojsko	vojsko	k1gNnSc4	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Specifická	specifický	k2eAgFnSc1d1	specifická
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
pohledu	pohled	k1gInSc2	pohled
je	být	k5eAaImIp3nS	být
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
provozuje	provozovat	k5eAaImIp3nS	provozovat
vedle	vedle	k7c2	vedle
hlavních	hlavní	k2eAgInPc2d1	hlavní
bojových	bojový	k2eAgInPc2d1	bojový
tanků	tank	k1gInPc2	tank
4	[number]	k4	4
<g/>
.	.	kIx.	.
generace	generace	k1gFnSc1	generace
Leclerc	Leclerc	k1gFnSc1	Leclerc
<g/>
,	,	kIx,	,
také	také	k9	také
značné	značný	k2eAgNnSc4d1	značné
množství	množství	k1gNnSc4	množství
lehkých	lehký	k2eAgInPc2d1	lehký
kolových	kolový	k2eAgInPc2d1	kolový
tanků	tank	k1gInPc2	tank
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
všechny	všechen	k3xTgInPc1	všechen
moderní	moderní	k2eAgInPc1d1	moderní
tanky	tank	k1gInPc1	tank
spadají	spadat	k5eAaPmIp3nP	spadat
právě	právě	k9	právě
do	do	k7c2	do
kategorie	kategorie	k1gFnSc2	kategorie
hlavních	hlavní	k2eAgInPc2d1	hlavní
bojových	bojový	k2eAgInPc2d1	bojový
tanků	tank	k1gInPc2	tank
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
vývoj	vývoj	k1gInSc1	vývoj
zpomalil	zpomalit	k5eAaPmAgInS	zpomalit
-	-	kIx~	-
Rusko	Rusko	k1gNnSc1	Rusko
zavedlo	zavést	k5eAaPmAgNnS	zavést
nové	nový	k2eAgFnPc4d1	nová
verze	verze	k1gFnPc4	verze
T-	T-	k1gFnSc4	T-
<g/>
72	[number]	k4	72
<g/>
/	/	kIx~	/
<g/>
80	[number]	k4	80
<g/>
,	,	kIx,	,
zvané	zvaný	k2eAgFnSc2d1	zvaná
T-	T-	k1gFnSc2	T-
<g/>
90	[number]	k4	90
<g/>
S	s	k7c7	s
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
disponují	disponovat	k5eAaBmIp3nP	disponovat
systémem	systém	k1gInSc7	systém
řízení	řízení	k1gNnSc2	řízení
palby	palba	k1gFnSc2	palba
srovnatelným	srovnatelný	k2eAgFnPc3d1	srovnatelná
se	s	k7c7	s
soudobými	soudobý	k2eAgInPc7d1	soudobý
západními	západní	k2eAgInPc7d1	západní
tanky	tank	k1gInPc7	tank
<g/>
.	.	kIx.	.
</s>
<s>
Leopard	leopard	k1gMnSc1	leopard
2	[number]	k4	2
byl	být	k5eAaImAgMnS	být
modernizován	modernizovat	k5eAaBmNgMnS	modernizovat
až	až	k9	až
na	na	k7c4	na
standard	standard	k1gInSc4	standard
Leopard	leopard	k1gMnSc1	leopard
2A7	[number]	k4	2A7
a	a	k8xC	a
M1	M1	k1gFnSc4	M1
Abrams	Abrams	k1gInSc1	Abrams
byl	být	k5eAaImAgInS	být
modernizován	modernizovat	k5eAaBmNgInS	modernizovat
na	na	k7c6	na
M	M	kA	M
<g/>
1	[number]	k4	1
<g/>
A	a	k9	a
<g/>
2	[number]	k4	2
<g/>
SEP	SEP	kA	SEP
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
či	či	k8xC	či
teprve	teprve	k6eAd1	teprve
vyvíjejí	vyvíjet	k5eAaImIp3nP	vyvíjet
tanky	tank	k1gInPc1	tank
i	i	k9	i
zdánlivě	zdánlivě	k6eAd1	zdánlivě
průmyslově	průmyslově	k6eAd1	průmyslově
zaostalé	zaostalý	k2eAgFnSc2d1	zaostalá
země	zem	k1gFnSc2	zem
jako	jako	k8xC	jako
Indie	Indie	k1gFnSc2	Indie
<g/>
,	,	kIx,	,
Pákistán	Pákistán	k1gInSc4	Pákistán
či	či	k8xC	či
Jihoafrická	jihoafrický	k2eAgFnSc1d1	Jihoafrická
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
zmínku	zmínka	k1gFnSc4	zmínka
také	také	k9	také
stojí	stát	k5eAaImIp3nS	stát
izraelský	izraelský	k2eAgInSc4d1	izraelský
tank	tank	k1gInSc4	tank
Merkava	Merkava	k1gFnSc1	Merkava
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
kompletně	kompletně	k6eAd1	kompletně
vyvinut	vyvinout	k5eAaPmNgInS	vyvinout
v	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
koncepce	koncepce	k1gFnSc1	koncepce
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
odlišná	odlišný	k2eAgFnSc1d1	odlišná
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
hlavní	hlavní	k2eAgInSc4d1	hlavní
bojový	bojový	k2eAgInSc4d1	bojový
tank	tank	k1gInSc4	tank
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
motor	motor	k1gInSc1	motor
umístěný	umístěný	k2eAgInSc1d1	umístěný
vpředu	vpředu	k6eAd1	vpředu
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
ochranu	ochrana	k1gFnSc4	ochrana
posádky	posádka	k1gFnSc2	posádka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
to	ten	k3xDgNnSc1	ten
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
vznik	vznik	k1gInSc4	vznik
prostoru	prostor	k1gInSc2	prostor
pro	pro	k7c4	pro
pěchotu	pěchota	k1gFnSc4	pěchota
v	v	k7c6	v
zadní	zadní	k2eAgFnSc6d1	zadní
části	část	k1gFnSc6	část
tanku	tank	k1gInSc2	tank
<g/>
.	.	kIx.	.
</s>
<s>
Tank	tank	k1gInSc1	tank
tak	tak	k9	tak
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
plnohodnotný	plnohodnotný	k2eAgInSc4d1	plnohodnotný
tankový	tankový	k2eAgInSc4d1	tankový
boj	boj	k1gInSc4	boj
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
sloužit	sloužit	k5eAaImF	sloužit
jako	jako	k8xC	jako
bojové	bojový	k2eAgNnSc4d1	bojové
vozidlo	vozidlo	k1gNnSc4	vozidlo
pěchoty	pěchota	k1gFnSc2	pěchota
<g/>
,	,	kIx,	,
či	či	k8xC	či
bezpečně	bezpečně	k6eAd1	bezpečně
odvážet	odvážet	k5eAaImF	odvážet
zraněné	zraněný	k2eAgMnPc4d1	zraněný
vojáky	voják	k1gMnPc4	voják
<g/>
.	.	kIx.	.
</s>
<s>
Komerčně	komerčně	k6eAd1	komerčně
nejúspěšnějším	úspěšný	k2eAgInSc7d3	nejúspěšnější
tankem	tank	k1gInSc7	tank
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
vyvezený	vyvezený	k2eAgInSc4d1	vyvezený
do	do	k7c2	do
nejvíce	hodně	k6eAd3	hodně
zemí	zem	k1gFnPc2	zem
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
však	však	k9	však
německý	německý	k2eAgMnSc1d1	německý
Leopard	leopard	k1gMnSc1	leopard
2	[number]	k4	2
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
k	k	k7c3	k
nejlépe	dobře	k6eAd3	dobře
pancéřovaným	pancéřovaný	k2eAgInPc3d1	pancéřovaný
a	a	k8xC	a
vyzbrojeným	vyzbrojený	k2eAgInPc3d1	vyzbrojený
tankům	tank	k1gInPc3	tank
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc1	popis
současného	současný	k2eAgInSc2d1	současný
tanku	tank	k1gInSc2	tank
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Korba	korba	k1gFnSc1	korba
===	===	k?	===
</s>
</p>
<p>
<s>
Korba	korba	k1gFnSc1	korba
je	být	k5eAaImIp3nS	být
část	část	k1gFnSc4	část
tanku	tank	k1gInSc2	tank
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
palivo	palivo	k1gNnSc1	palivo
a	a	k8xC	a
motor	motor	k1gInSc1	motor
a	a	k8xC	a
ke	k	k7c3	k
které	který	k3yRgFnSc3	který
je	být	k5eAaImIp3nS	být
připevněn	připevněn	k2eAgInSc1d1	připevněn
podvozek	podvozek	k1gInSc1	podvozek
a	a	k8xC	a
věž	věž	k1gFnSc1	věž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
přední	přední	k2eAgFnSc6d1	přední
části	část	k1gFnSc6	část
korby	korba	k1gFnSc2	korba
sedává	sedávat	k5eAaImIp3nS	sedávat
řidič	řidič	k1gMnSc1	řidič
<g/>
,	,	kIx,	,
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
části	část	k1gFnSc6	část
je	být	k5eAaImIp3nS	být
prostor	prostor	k1gInSc4	prostor
pro	pro	k7c4	pro
věž	věž	k1gFnSc4	věž
<g/>
,	,	kIx,	,
v	v	k7c6	v
zadní	zadní	k2eAgFnSc6d1	zadní
části	část	k1gFnSc6	část
je	být	k5eAaImIp3nS	být
umístěn	umístěn	k2eAgInSc1d1	umístěn
motor	motor	k1gInSc1	motor
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
výjimek	výjimka	k1gFnPc2	výjimka
od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
koncepce	koncepce	k1gFnSc2	koncepce
je	být	k5eAaImIp3nS	být
izraelský	izraelský	k2eAgInSc4d1	izraelský
tank	tank	k1gInSc4	tank
Merkava	Merkava	k1gFnSc1	Merkava
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterého	který	k3yIgInSc2	který
je	být	k5eAaImIp3nS	být
motor	motor	k1gInSc1	motor
umístěn	umístit	k5eAaPmNgInS	umístit
vedle	vedle	k7c2	vedle
řidiče	řidič	k1gMnSc2	řidič
(	(	kIx(	(
<g/>
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
u	u	k7c2	u
bojového	bojový	k2eAgNnSc2d1	bojové
vozidla	vozidlo	k1gNnSc2	vozidlo
pěchoty	pěchota	k1gFnSc2	pěchota
<g/>
)	)	kIx)	)
a	a	k8xC	a
zadní	zadní	k2eAgFnSc1d1	zadní
část	část	k1gFnSc1	část
opatřená	opatřený	k2eAgFnSc1d1	opatřená
vraty	vrat	k1gInPc4	vrat
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
využít	využít	k5eAaPmF	využít
k	k	k7c3	k
přepravě	přeprava	k1gFnSc3	přeprava
další	další	k2eAgFnSc2d1	další
munice	munice	k1gFnSc2	munice
<g/>
,	,	kIx,	,
pěchoty	pěchota	k1gFnSc2	pěchota
či	či	k8xC	či
k	k	k7c3	k
evakuaci	evakuace	k1gFnSc3	evakuace
posádky	posádka	k1gFnSc2	posádka
jiného	jiný	k2eAgInSc2d1	jiný
poškozeného	poškozený	k2eAgInSc2d1	poškozený
tanku	tank	k1gInSc2	tank
z	z	k7c2	z
bojiště	bojiště	k1gNnSc2	bojiště
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pásy	pás	k1gInPc1	pás
===	===	k?	===
</s>
</p>
<p>
<s>
Jsou	být	k5eAaImIp3nP	být
dva	dva	k4xCgMnPc1	dva
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
nejslabším	slabý	k2eAgInSc7d3	nejslabší
prvkem	prvek	k1gInSc7	prvek
tanku	tank	k1gInSc2	tank
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
jeho	jeho	k3xOp3gFnSc7	jeho
největší	veliký	k2eAgFnSc7d3	veliký
výhodou	výhoda	k1gFnSc7	výhoda
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
pásy	pás	k1gInPc1	pás
jsou	být	k5eAaImIp3nP	být
kritický	kritický	k2eAgInSc4d1	kritický
prvek	prvek	k1gInSc4	prvek
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
je	být	k5eAaImIp3nS	být
jimi	on	k3xPp3gInPc7	on
tank	tank	k1gInSc4	tank
řízen	řídit	k5eAaImNgMnS	řídit
(	(	kIx(	(
<g/>
zatáčení	zatáčení	k1gNnSc6	zatáčení
se	se	k3xPyFc4	se
děje	dít	k5eAaImIp3nS	dít
udělováním	udělování	k1gNnSc7	udělování
rozdílných	rozdílný	k2eAgFnPc2d1	rozdílná
rychlostí	rychlost	k1gFnPc2	rychlost
pásům	pás	k1gInPc3	pás
<g/>
)	)	kIx)	)
a	a	k8xC	a
zničení	zničení	k1gNnSc2	zničení
byť	byť	k8xS	byť
jediného	jediný	k2eAgInSc2d1	jediný
pásu	pás	k1gInSc2	pás
tedy	tedy	k9	tedy
znamená	znamenat	k5eAaImIp3nS	znamenat
zastavení	zastavení	k1gNnSc1	zastavení
tanku	tank	k1gInSc2	tank
(	(	kIx(	(
<g/>
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
v	v	k7c6	v
boji	boj	k1gInSc6	boj
prakticky	prakticky	k6eAd1	prakticky
rovná	rovnat	k5eAaImIp3nS	rovnat
zničení	zničení	k1gNnSc1	zničení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
udělují	udělovat	k5eAaImIp3nP	udělovat
tanku	tank	k1gInSc2	tank
nepřekonatelnou	překonatelný	k2eNgFnSc4d1	nepřekonatelná
přilnavost	přilnavost	k1gFnSc4	přilnavost
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
velmi	velmi	k6eAd1	velmi
nízké	nízký	k2eAgNnSc1d1	nízké
měrné	měrný	k2eAgNnSc1d1	měrné
zatížení	zatížení	k1gNnSc1	zatížení
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
kterému	který	k3yIgMnSc3	který
dokáže	dokázat	k5eAaPmIp3nS	dokázat
tank	tank	k1gInSc4	tank
vyšplhat	vyšplhat	k5eAaPmF	vyšplhat
i	i	k9	i
kopec	kopec	k1gInSc1	kopec
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
byť	byť	k8xS	byť
mnohem	mnohem	k6eAd1	mnohem
lehčí	lehčit	k5eAaImIp3nS	lehčit
kolové	kolový	k2eAgNnSc1d1	kolové
vozidlo	vozidlo	k1gNnSc1	vozidlo
nevyjelo	vyjet	k5eNaPmAgNnS	vyjet
(	(	kIx(	(
<g/>
většina	většina	k1gFnSc1	většina
dnešních	dnešní	k2eAgInPc2d1	dnešní
tanků	tank	k1gInPc2	tank
dokáže	dokázat	k5eAaPmIp3nS	dokázat
vyjet	vyjet	k5eAaPmF	vyjet
suchý	suchý	k2eAgInSc4d1	suchý
kopec	kopec	k1gInSc4	kopec
o	o	k7c6	o
sklonu	sklon	k1gInSc6	sklon
60	[number]	k4	60
<g/>
%	%	kIx~	%
(	(	kIx(	(
<g/>
cca	cca	kA	cca
31	[number]	k4	31
<g/>
°	°	k?	°
<g/>
,	,	kIx,	,
max	max	kA	max
<g/>
.	.	kIx.	.
podélný	podélný	k2eAgInSc1d1	podélný
sklon	sklon	k1gInSc1	sklon
pro	pro	k7c4	pro
např.	např.	kA	např.
M1A1	M1A1	k1gFnSc4	M1A1
Abrams	Abramsa	k1gFnPc2	Abramsa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rychlostí	rychlost	k1gFnSc7	rychlost
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Otočná	otočný	k2eAgFnSc1d1	otočná
věž	věž	k1gFnSc1	věž
===	===	k?	===
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
poháněna	poháněn	k2eAgFnSc1d1	poháněna
hydraulikou	hydraulika	k1gFnSc7	hydraulika
a	a	k8xC	a
ovládána	ovládat	k5eAaImNgFnS	ovládat
elektronicky	elektronicky	k6eAd1	elektronicky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
vepředu	vepředu	k6eAd1	vepředu
zkosená	zkosený	k2eAgFnSc1d1	zkosená
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zlepšila	zlepšit	k5eAaPmAgFnS	zlepšit
pasivní	pasivní	k2eAgFnSc1d1	pasivní
ochrana	ochrana	k1gFnSc1	ochrana
<g/>
.	.	kIx.	.
</s>
<s>
Sedí	sedit	k5eAaImIp3nS	sedit
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
střelec	střelec	k1gMnSc1	střelec
a	a	k8xC	a
velitel	velitel	k1gMnSc1	velitel
<g/>
,	,	kIx,	,
u	u	k7c2	u
tanků	tank	k1gInPc2	tank
s	s	k7c7	s
manuálním	manuální	k2eAgNnSc7d1	manuální
nabíjením	nabíjení	k1gNnSc7	nabíjení
kanónu	kanón	k1gInSc2	kanón
i	i	k8xC	i
nabíječ	nabíječ	k1gInSc4	nabíječ
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
věži	věž	k1gFnSc6	věž
je	být	k5eAaImIp3nS	být
také	také	k9	také
umístěna	umístěn	k2eAgFnSc1d1	umístěna
munice	munice	k1gFnSc1	munice
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
buď	buď	k8xC	buď
vedle	vedle	k7c2	vedle
nabíječe	nabíječ	k1gInSc2	nabíječ
a	a	k8xC	a
střelce	střelec	k1gMnPc4	střelec
(	(	kIx(	(
<g/>
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
nebezpečné	bezpečný	k2eNgNnSc1d1	nebezpečné
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
při	při	k7c6	při
zásahu	zásah	k1gInSc6	zásah
tato	tento	k3xDgFnSc1	tento
munice	munice	k1gFnSc1	munice
může	moct	k5eAaImIp3nS	moct
explodovat	explodovat	k5eAaBmF	explodovat
-	-	kIx~	-
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
příčinou	příčina	k1gFnSc7	příčina
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
zásahu	zásah	k1gInSc6	zásah
odletí	odletět	k5eAaPmIp3nS	odletět
věž	věž	k1gFnSc1	věž
starších	starý	k2eAgInPc2d2	starší
ruských	ruský	k2eAgInPc2d1	ruský
tanků	tank	k1gInPc2	tank
mnoho	mnoho	k4c4	mnoho
desítek	desítka	k1gFnPc2	desítka
metrů	metr	k1gInPc2	metr
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
<g />
.	.	kIx.	.
</s>
<s>
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
odděleném	oddělený	k2eAgInSc6d1	oddělený
pancéřovými	pancéřový	k2eAgFnPc7d1	pancéřová
dveřmi	dveře	k1gFnPc7	dveře
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
navíc	navíc	k6eAd1	navíc
deflekční	deflekční	k2eAgInPc1d1	deflekční
panely	panel	k1gInPc1	panel
(	(	kIx(	(
<g/>
slabá	slabý	k2eAgNnPc1d1	slabé
místa	místo	k1gNnPc1	místo
na	na	k7c6	na
stropě	strop	k1gInSc6	strop
věže	věž	k1gFnSc2	věž
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
při	při	k7c6	při
výbuchu	výbuch	k1gInSc6	výbuch
munice	munice	k1gFnSc2	munice
odletí	odletět	k5eAaPmIp3nP	odletět
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
usměrní	usměrnit	k5eAaPmIp3nS	usměrnit
sílu	síla	k1gFnSc4	síla
exploze	exploze	k1gFnSc2	exploze
nahoru	nahoru	k6eAd1	nahoru
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
prostor	prostor	k1gInSc4	prostor
posádky	posádka	k1gFnSc2	posádka
<g/>
)	)	kIx)	)
-	-	kIx~	-
deflekční	deflekční	k2eAgInPc4d1	deflekční
panely	panel	k1gInPc4	panel
má	mít	k5eAaImIp3nS	mít
např.	např.	kA	např.
americký	americký	k2eAgInSc1d1	americký
M1	M1	k1gFnSc7	M1
Abrams	Abramsa	k1gFnPc2	Abramsa
nebo	nebo	k8xC	nebo
ruský	ruský	k2eAgInSc4d1	ruský
tank	tank	k1gInSc4	tank
T-	T-	k1gFnSc2	T-
<g/>
80	[number]	k4	80
<g/>
UM	um	k1gInSc1	um
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1	ostatní
tanky	tank	k1gInPc1	tank
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
T-72	T-72	k1gFnSc2	T-72
mají	mít	k5eAaImIp3nP	mít
munici	munice	k1gFnSc4	munice
uloženou	uložený	k2eAgFnSc4d1	uložená
volně	volně	k6eAd1	volně
ve	v	k7c6	v
věži	věž	k1gFnSc6	věž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vedle	vedle	k7c2	vedle
kanónu	kanón	k1gInSc2	kanón
je	být	k5eAaImIp3nS	být
instalován	instalován	k2eAgInSc1d1	instalován
spřažený	spřažený	k2eAgInSc1d1	spřažený
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
též	též	k9	též
koaxiální	koaxiální	k2eAgInSc1d1	koaxiální
<g/>
)	)	kIx)	)
kulomet	kulomet	k1gInSc1	kulomet
<g/>
,	,	kIx,	,
sloužící	sloužící	k2eAgMnSc1d1	sloužící
ke	k	k7c3	k
střelbě	střelba	k1gFnSc3	střelba
na	na	k7c4	na
pěchotu	pěchota	k1gFnSc4	pěchota
či	či	k8xC	či
nepancéřované	pancéřovaný	k2eNgInPc4d1	pancéřovaný
cíle	cíl	k1gInPc4	cíl
(	(	kIx(	(
<g/>
u	u	k7c2	u
exportních	exportní	k2eAgInPc2d1	exportní
francouzských	francouzský	k2eAgInPc2d1	francouzský
tanků	tank	k1gInPc2	tank
řady	řada	k1gFnSc2	řada
AMX	AMX	kA	AMX
3	[number]	k4	3
<g/>
x	x	k?	x
byl	být	k5eAaImAgMnS	být
místo	místo	k7c2	místo
kulometu	kulomet	k1gInSc2	kulomet
osazen	osadit	k5eAaPmNgInS	osadit
automatický	automatický	k2eAgInSc1d1	automatický
kanón	kanón	k1gInSc1	kanón
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
kanónu	kanón	k1gInSc2	kanón
tanku	tank	k1gInSc2	tank
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
optický	optický	k2eAgInSc4d1	optický
zaměřovač	zaměřovač	k1gInSc4	zaměřovač
střelce	střelec	k1gMnSc2	střelec
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
moderních	moderní	k2eAgInPc2d1	moderní
tanků	tank	k1gInPc2	tank
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
stropě	strop	k1gInSc6	strop
věže	věž	k1gFnSc2	věž
umístěn	umístěn	k2eAgInSc4d1	umístěn
nezávisle	závisle	k6eNd1	závisle
pohyblivý	pohyblivý	k2eAgInSc4d1	pohyblivý
zaměřovač	zaměřovač	k1gInSc4	zaměřovač
velitele	velitel	k1gMnSc2	velitel
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
během	během	k7c2	během
palby	palba	k1gFnSc2	palba
střelce	střelec	k1gMnPc4	střelec
na	na	k7c4	na
cíl	cíl	k1gInSc4	cíl
vyhledává	vyhledávat	k5eAaImIp3nS	vyhledávat
cíl	cíl	k1gInSc4	cíl
nový	nový	k2eAgInSc4d1	nový
a	a	k8xC	a
po	po	k7c6	po
zničení	zničení	k1gNnSc6	zničení
předešlého	předešlý	k2eAgInSc2d1	předešlý
cíle	cíl	k1gInSc2	cíl
(	(	kIx(	(
<g/>
v	v	k7c6	v
případě	případ	k1gInSc6	případ
zjištění	zjištění	k1gNnSc2	zjištění
nebezpečnějšího	bezpečný	k2eNgInSc2d2	nebezpečnější
cíle	cíl	k1gInSc2	cíl
<g/>
,	,	kIx,	,
např.	např.	kA	např.
tanku	tank	k1gInSc2	tank
místo	místo	k7c2	místo
"	"	kIx"	"
<g/>
obyčejného	obyčejný	k2eAgInSc2d1	obyčejný
<g/>
"	"	kIx"	"
obrněného	obrněný	k2eAgInSc2d1	obrněný
transportéru	transportér	k1gInSc2	transportér
<g/>
,	,	kIx,	,
i	i	k8xC	i
před	před	k7c7	před
jeho	jeho	k3xOp3gNnSc7	jeho
zničením	zničení	k1gNnSc7	zničení
<g/>
)	)	kIx)	)
stiskem	stisk	k1gInSc7	stisk
jednoho	jeden	k4xCgNnSc2	jeden
tlačítka	tlačítko	k1gNnSc2	tlačítko
navede	navést	k5eAaPmIp3nS	navést
kanón	kanón	k1gInSc4	kanón
na	na	k7c4	na
nový	nový	k2eAgInSc4d1	nový
cíl	cíl	k1gInSc4	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
nalezený	nalezený	k2eAgInSc1d1	nalezený
cíl	cíl	k1gInSc1	cíl
předá	předat	k5eAaPmIp3nS	předat
střelci	střelec	k1gMnSc3	střelec
<g/>
,	,	kIx,	,
a	a	k8xC	a
opět	opět	k6eAd1	opět
vyhledává	vyhledávat	k5eAaImIp3nS	vyhledávat
cíl	cíl	k1gInSc4	cíl
nový	nový	k2eAgInSc4d1	nový
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
stropě	strop	k1gInSc6	strop
věže	věž	k1gFnSc2	věž
bývá	bývat	k5eAaImIp3nS	bývat
lafetován	lafetován	k2eAgInSc1d1	lafetován
obvykle	obvykle	k6eAd1	obvykle
velkorážový	velkorážový	k2eAgInSc1d1	velkorážový
protiletadlový	protiletadlový	k2eAgInSc1d1	protiletadlový
kulomet	kulomet	k1gInSc1	kulomet
obsluhovaný	obsluhovaný	k2eAgInSc1d1	obsluhovaný
většinou	většinou	k6eAd1	většinou
velitelem	velitel	k1gMnSc7	velitel
tanku	tank	k1gInSc2	tank
(	(	kIx(	(
<g/>
u	u	k7c2	u
slovenského	slovenský	k2eAgInSc2d1	slovenský
modernizovaného	modernizovaný	k2eAgInSc2d1	modernizovaný
tanku	tank	k1gInSc2	tank
T-72	T-72	k1gFnSc1	T-72
Moderna	Moderna	k1gFnSc1	Moderna
je	být	k5eAaImIp3nS	být
místo	místo	k1gNnSc4	místo
protiletadlového	protiletadlový	k2eAgInSc2d1	protiletadlový
kulometu	kulomet	k1gInSc2	kulomet
osazen	osadit	k5eAaPmNgInS	osadit
opět	opět	k6eAd1	opět
automatický	automatický	k2eAgInSc1d1	automatický
kanón	kanón	k1gInSc1	kanón
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
americký	americký	k2eAgInSc4d1	americký
tank	tank	k1gInSc4	tank
M1	M1	k1gFnSc1	M1
Abrams	Abramsa	k1gFnPc2	Abramsa
má	mít	k5eAaImIp3nS	mít
navíc	navíc	k6eAd1	navíc
na	na	k7c6	na
střeše	střecha	k1gFnSc6	střecha
lafetován	lafetován	k2eAgInSc1d1	lafetován
kulomet	kulomet	k1gInSc1	kulomet
ráže	ráže	k1gFnSc2	ráže
7,62	[number]	k4	7,62
mm	mm	kA	mm
obsluhovaný	obsluhovaný	k2eAgInSc4d1	obsluhovaný
nabíječem	nabíječ	k1gInSc7	nabíječ
<g/>
,	,	kIx,	,
izraelská	izraelský	k2eAgFnSc1d1	izraelská
Merkava	Merkava	k1gFnSc1	Merkava
60	[number]	k4	60
mm	mm	kA	mm
minomet	minomet	k1gInSc4	minomet
určený	určený	k2eAgInSc4d1	určený
pro	pro	k7c4	pro
boj	boj	k1gInSc4	boj
s	s	k7c7	s
nepřítelem	nepřítel	k1gMnSc7	nepřítel
na	na	k7c6	na
střechách	střecha	k1gFnPc6	střecha
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
po	po	k7c6	po
zkušenostech	zkušenost	k1gFnPc6	zkušenost
z	z	k7c2	z
bojů	boj	k1gInPc2	boj
ve	v	k7c6	v
městech	město	k1gNnPc6	město
také	také	k9	také
zkoušejí	zkoušet	k5eAaImIp3nP	zkoušet
dálkově	dálkově	k6eAd1	dálkově
ovládané	ovládaný	k2eAgFnPc1d1	ovládaná
zbraňové	zbraňový	k2eAgFnPc1d1	zbraňová
stanice	stanice	k1gFnPc1	stanice
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
obvykle	obvykle	k6eAd1	obvykle
kulomety	kulomet	k1gInPc4	kulomet
spřažené	spřažený	k2eAgInPc4d1	spřažený
se	se	k3xPyFc4	se
zaměřovací	zaměřovací	k2eAgFnSc7d1	zaměřovací
optikou	optika	k1gFnSc7	optika
a	a	k8xC	a
ovládané	ovládaný	k2eAgNnSc1d1	ovládané
z	z	k7c2	z
vnitřku	vnitřek	k1gInSc2	vnitřek
tanku	tank	k1gInSc2	tank
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
není	být	k5eNaImIp3nS	být
posádka	posádka	k1gFnSc1	posádka
tak	tak	k6eAd1	tak
zranitelná	zranitelný	k2eAgFnSc1d1	zranitelná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Věž	věž	k1gFnSc1	věž
tanku	tank	k1gInSc2	tank
je	být	k5eAaImIp3nS	být
také	také	k6eAd1	také
vybavena	vybavit	k5eAaPmNgFnS	vybavit
zadýmovacími	zadýmovací	k2eAgInPc7d1	zadýmovací
granátomety	granátomet	k1gInPc7	granátomet
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
slouží	sloužit	k5eAaImIp3nP	sloužit
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
dýmové	dýmový	k2eAgFnSc2d1	dýmová
clony	clona	k1gFnSc2	clona
kolem	kolem	k7c2	kolem
tanku	tank	k1gInSc2	tank
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
znesnadnění	znesnadnění	k1gNnSc4	znesnadnění
zaměření	zaměření	k1gNnSc1	zaměření
tanku	tank	k1gInSc2	tank
nepřítelem	nepřítel	k1gMnSc7	nepřítel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
však	však	k9	však
některé	některý	k3yIgInPc1	některý
příklady	příklad	k1gInPc1	příklad
neortodoxní	ortodoxní	k2eNgFnSc2d1	neortodoxní
konstrukce	konstrukce	k1gFnSc2	konstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
Švédsko	Švédsko	k1gNnSc1	Švédsko
vyvinulo	vyvinout	k5eAaPmAgNnS	vyvinout
a	a	k8xC	a
provozovalo	provozovat	k5eAaImAgNnS	provozovat
takzvaný	takzvaný	k2eAgInSc4d1	takzvaný
S	s	k7c7	s
tank	tank	k1gInSc1	tank
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
měl	mít	k5eAaImAgInS	mít
bezvěžovou	bezvěžový	k2eAgFnSc4d1	bezvěžový
koncepci	koncepce	k1gFnSc4	koncepce
a	a	k8xC	a
střelec	střelec	k1gMnSc1	střelec
mířil	mířit	k5eAaImAgMnS	mířit
celým	celý	k2eAgNnSc7d1	celé
vozidlem	vozidlo	k1gNnSc7	vozidlo
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
však	však	k9	však
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
slepou	slepý	k2eAgFnSc4d1	slepá
uličku	ulička	k1gFnSc4	ulička
vývoje	vývoj	k1gInSc2	vývoj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kanón	kanón	k1gInSc4	kanón
===	===	k?	===
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
výzbroji	výzbroj	k1gFnSc6	výzbroj
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
nacházejí	nacházet	k5eAaImIp3nP	nacházet
tanky	tank	k1gInPc1	tank
starších	starý	k2eAgInPc2d2	starší
typů	typ	k1gInPc2	typ
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
kanóny	kanón	k1gInPc1	kanón
s	s	k7c7	s
drážkovaným	drážkovaný	k2eAgInSc7d1	drážkovaný
vývrtem	vývrt	k1gInSc7	vývrt
mají	mít	k5eAaImIp3nP	mít
ráži	ráže	k1gFnSc4	ráže
105	[number]	k4	105
mm	mm	kA	mm
v	v	k7c6	v
případě	případ	k1gInSc6	případ
"	"	kIx"	"
<g/>
západních	západní	k2eAgFnPc2d1	západní
<g/>
"	"	kIx"	"
konstrukcí	konstrukce	k1gFnPc2	konstrukce
<g/>
,	,	kIx,	,
tanky	tank	k1gInPc1	tank
sovětského	sovětský	k2eAgInSc2d1	sovětský
původu	původ	k1gInSc2	původ
mají	mít	k5eAaImIp3nP	mít
kanóny	kanón	k1gInPc1	kanón
ráže	ráže	k1gFnSc2	ráže
100	[number]	k4	100
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgInPc1d1	moderní
tanky	tank	k1gInPc1	tank
jsou	být	k5eAaImIp3nP	být
osazeny	osazen	k2eAgInPc1d1	osazen
kanóny	kanón	k1gInPc1	kanón
s	s	k7c7	s
hladkým	hladký	k2eAgInSc7d1	hladký
vývrtem	vývrt	k1gInSc7	vývrt
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Západní	západní	k2eAgMnSc1d1	západní
<g/>
"	"	kIx"	"
typy	typ	k1gInPc1	typ
mají	mít	k5eAaImIp3nP	mít
ráži	ráže	k1gFnSc4	ráže
120	[number]	k4	120
mm	mm	kA	mm
<g/>
,	,	kIx,	,
ruské	ruský	k2eAgFnSc2d1	ruská
125	[number]	k4	125
mm	mm	kA	mm
(	(	kIx(	(
<g/>
tank	tank	k1gInSc1	tank
T-62	T-62	k1gFnSc2	T-62
má	mít	k5eAaImIp3nS	mít
ráži	ráže	k1gFnSc4	ráže
115	[number]	k4	115
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zajímavostmi	zajímavost	k1gFnPc7	zajímavost
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
<g/>
,	,	kIx,	,
anglická	anglický	k2eAgFnSc1d1	anglická
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
i	i	k9	i
nadále	nadále	k6eAd1	nadále
upřednostňuje	upřednostňovat	k5eAaImIp3nS	upřednostňovat
drážkovaný	drážkovaný	k2eAgInSc4d1	drážkovaný
vývrt	vývrt	k1gInSc4	vývrt
hlavně	hlavně	k9	hlavně
a	a	k8xC	a
sovětské	sovětský	k2eAgNnSc1d1	sovětské
hlavně	hlavně	k6eAd1	hlavně
schopné	schopný	k2eAgNnSc1d1	schopné
odpalovat	odpalovat	k5eAaImF	odpalovat
protitankové	protitankový	k2eAgFnSc2d1	protitanková
řízené	řízený	k2eAgFnSc2d1	řízená
střely	střela	k1gFnSc2	střela
(	(	kIx(	(
<g/>
PTŘS	PTŘS	kA	PTŘS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
kompenzací	kompenzace	k1gFnSc7	kompenzace
méně	málo	k6eAd2	málo
výkonných	výkonný	k2eAgMnPc2d1	výkonný
balistických	balistický	k2eAgMnPc2d1	balistický
počítačů	počítač	k1gMnPc2	počítač
oproti	oproti	k7c3	oproti
západním	západní	k2eAgFnPc3d1	západní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kanon	kanon	k1gInSc1	kanon
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
střílet	střílet	k5eAaImF	střílet
několik	několik	k4yIc1	několik
druhů	druh	k1gInPc2	druh
munice	munice	k1gFnSc2	munice
<g/>
,	,	kIx,	,
a	a	k8xC	a
nabíjí	nabíjet	k5eAaImIp3nS	nabíjet
ho	on	k3xPp3gInSc4	on
buďto	buďto	k8xC	buďto
nabíječ	nabíječ	k1gInSc4	nabíječ
nebo	nebo	k8xC	nebo
nabíjecí	nabíjecí	k2eAgInSc4d1	nabíjecí
automat	automat	k1gInSc4	automat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
moderních	moderní	k2eAgInPc2d1	moderní
tanků	tank	k1gInPc2	tank
je	být	k5eAaImIp3nS	být
kanon	kanon	k1gInSc1	kanon
stabilizovaný	stabilizovaný	k2eAgInSc1d1	stabilizovaný
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
že	že	k8xS	že
stále	stále	k6eAd1	stále
míří	mířit	k5eAaImIp3nS	mířit
na	na	k7c4	na
určený	určený	k2eAgInSc4d1	určený
cíl	cíl	k1gInSc4	cíl
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
terénní	terénní	k2eAgFnPc4d1	terénní
nerovnosti	nerovnost	k1gFnPc4	nerovnost
či	či	k8xC	či
směr	směr	k1gInSc4	směr
jízdy	jízda	k1gFnSc2	jízda
tanku	tank	k1gInSc2	tank
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stabilizaci	stabilizace	k1gFnSc4	stabilizace
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
výkonné	výkonný	k2eAgInPc1d1	výkonný
gyroskopy	gyroskop	k1gInPc1	gyroskop
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Motor	motor	k1gInSc1	motor
===	===	k?	===
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
typů	typ	k1gInPc2	typ
tanků	tank	k1gInPc2	tank
používá	používat	k5eAaImIp3nS	používat
dieselový	dieselový	k2eAgInSc1d1	dieselový
motor	motor	k1gInSc1	motor
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
americký	americký	k2eAgMnSc1d1	americký
M1	M1	k1gMnSc1	M1
Abrams	Abrams	k1gInSc1	Abrams
a	a	k8xC	a
ruský	ruský	k2eAgMnSc1d1	ruský
T-	T-	k1gMnSc1	T-
<g/>
80	[number]	k4	80
<g/>
/	/	kIx~	/
<g/>
T-	T-	k1gFnSc2	T-
<g/>
90	[number]	k4	90
používají	používat	k5eAaImIp3nP	používat
plynovou	plynový	k2eAgFnSc4d1	plynová
turbínu	turbína	k1gFnSc4	turbína
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výhodou	výhoda	k1gFnSc7	výhoda
dieselového	dieselový	k2eAgInSc2d1	dieselový
motoru	motor	k1gInSc2	motor
je	být	k5eAaImIp3nS	být
nízká	nízký	k2eAgFnSc1d1	nízká
spotřeba	spotřeba	k1gFnSc1	spotřeba
a	a	k8xC	a
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
plynoucí	plynoucí	k2eAgInSc1d1	plynoucí
vysoký	vysoký	k2eAgInSc4d1	vysoký
dojezd	dojezd	k1gInSc4	dojezd
a	a	k8xC	a
relativně	relativně	k6eAd1	relativně
malá	malý	k2eAgFnSc1d1	malá
infračervená	infračervený	k2eAgFnSc1d1	infračervená
stopa	stopa	k1gFnSc1	stopa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oproti	oproti	k7c3	oproti
tomu	ten	k3xDgMnSc3	ten
plynová	plynový	k2eAgFnSc1d1	plynová
turbína	turbína	k1gFnSc1	turbína
dává	dávat	k5eAaImIp3nS	dávat
tanku	tank	k1gInSc2	tank
podstatně	podstatně	k6eAd1	podstatně
lepší	dobrý	k2eAgFnSc4d2	lepší
akceleraci	akcelerace	k1gFnSc4	akcelerace
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgFnSc1d2	menší
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
má	mít	k5eAaImIp3nS	mít
ovšem	ovšem	k9	ovšem
obrovskou	obrovský	k2eAgFnSc4d1	obrovská
spotřebu	spotřeba	k1gFnSc4	spotřeba
<g/>
,	,	kIx,	,
velkou	velký	k2eAgFnSc4d1	velká
infračervenou	infračervený	k2eAgFnSc4d1	infračervená
stopu	stopa	k1gFnSc4	stopa
a	a	k8xC	a
hlavně	hlavně	k6eAd1	hlavně
konzumuje	konzumovat	k5eAaBmIp3nS	konzumovat
palivo	palivo	k1gNnSc4	palivo
stejně	stejně	k6eAd1	stejně
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
výkonu	výkon	k1gInSc6	výkon
-	-	kIx~	-
když	když	k8xS	když
tank	tank	k1gInSc4	tank
stojí	stát	k5eAaImIp3nS	stát
tak	tak	k6eAd1	tak
plynová	plynový	k2eAgFnSc1d1	plynová
turbína	turbína	k1gFnSc1	turbína
spotřebovává	spotřebovávat	k5eAaImIp3nS	spotřebovávat
palivo	palivo	k1gNnSc4	palivo
úplně	úplně	k6eAd1	úplně
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
když	když	k8xS	když
jede	jet	k5eAaImIp3nS	jet
na	na	k7c4	na
plný	plný	k2eAgInSc4d1	plný
výkon	výkon	k1gInSc4	výkon
<g/>
.	.	kIx.	.
</s>
<s>
Plynová	plynový	k2eAgFnSc1d1	plynová
turbína	turbína	k1gFnSc1	turbína
je	být	k5eAaImIp3nS	být
také	také	k9	také
náročnější	náročný	k2eAgNnSc1d2	náročnější
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
podstatně	podstatně	k6eAd1	podstatně
dražší	drahý	k2eAgMnSc1d2	dražší
a	a	k8xC	a
také	také	k9	také
složitější	složitý	k2eAgMnSc1d2	složitější
na	na	k7c4	na
údržbu	údržba	k1gFnSc4	údržba
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
složitý	složitý	k2eAgInSc1d1	složitý
systém	systém	k1gInSc1	systém
čištění	čištění	k1gNnSc2	čištění
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
přiváděného	přiváděný	k2eAgInSc2d1	přiváděný
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
je	být	k5eAaImIp3nS	být
eliminována	eliminován	k2eAgFnSc1d1	eliminována
výhoda	výhoda	k1gFnSc1	výhoda
menších	malý	k2eAgInPc2d2	menší
rozměrů	rozměr	k1gInPc2	rozměr
turbíny	turbína	k1gFnSc2	turbína
samotné	samotný	k2eAgNnSc1d1	samotné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Motory	motor	k1gInPc1	motor
mají	mít	k5eAaImIp3nP	mít
výkon	výkon	k1gInSc4	výkon
většinou	většinou	k6eAd1	většinou
v	v	k7c6	v
řádu	řád	k1gInSc6	řád
stovek	stovka	k1gFnPc2	stovka
kW	kW	kA	kW
a	a	k8xC	a
udělují	udělovat	k5eAaImIp3nP	udělovat
tankům	tank	k1gInPc3	tank
rychlost	rychlost	k1gFnSc4	rychlost
okolo	okolo	k7c2	okolo
70	[number]	k4	70
<g/>
–	–	k?	–
<g/>
80	[number]	k4	80
km	km	kA	km
na	na	k7c6	na
pevném	pevný	k2eAgInSc6d1	pevný
podkladu	podklad	k1gInSc6	podklad
a	a	k8xC	a
30	[number]	k4	30
<g/>
–	–	k?	–
<g/>
50	[number]	k4	50
km	km	kA	km
v	v	k7c6	v
terénu	terén	k1gInSc6	terén
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
nejmodernějších	moderní	k2eAgInPc2d3	nejmodernější
typů	typ	k1gInPc2	typ
tanků	tank	k1gInPc2	tank
je	být	k5eAaImIp3nS	být
motor	motor	k1gInSc1	motor
spojen	spojit	k5eAaPmNgInS	spojit
s	s	k7c7	s
převodovkou	převodovka	k1gFnSc7	převodovka
do	do	k7c2	do
jednoho	jeden	k4xCgInSc2	jeden
bloku	blok	k1gInSc2	blok
(	(	kIx(	(
<g/>
Power	Power	k1gMnSc1	Power
Pack	Pack	k1gMnSc1	Pack
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
v	v	k7c6	v
případě	případ	k1gInSc6	případ
jakékoliv	jakýkoliv	k3yIgFnSc2	jakýkoliv
poruchy	porucha	k1gFnSc2	porucha
vyměnit	vyměnit	k5eAaPmF	vyměnit
i	i	k9	i
v	v	k7c6	v
polních	polní	k2eAgFnPc6d1	polní
podmínkách	podmínka	k1gFnPc6	podmínka
v	v	k7c6	v
řádu	řád	k1gInSc6	řád
desítek	desítka	k1gFnPc2	desítka
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ochrana	ochrana	k1gFnSc1	ochrana
===	===	k?	===
</s>
</p>
<p>
<s>
U	u	k7c2	u
tanků	tank	k1gInPc2	tank
byl	být	k5eAaImAgInS	být
vždy	vždy	k6eAd1	vždy
kladen	klást	k5eAaImNgInS	klást
důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
co	co	k3yInSc4	co
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
odolnost	odolnost	k1gFnSc1	odolnost
proti	proti	k7c3	proti
působení	působení	k1gNnSc3	působení
protivníka	protivník	k1gMnSc2	protivník
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
základem	základ	k1gInSc7	základ
je	být	k5eAaImIp3nS	být
pancéřování	pancéřování	k1gNnSc1	pancéřování
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
historických	historický	k2eAgInPc2d1	historický
tanků	tank	k1gInPc2	tank
až	až	k9	až
do	do	k7c2	do
typů	typ	k1gInPc2	typ
používaných	používaný	k2eAgInPc2d1	používaný
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
v	v	k7c6	v
drtivé	drtivý	k2eAgFnSc6d1	drtivá
většině	většina	k1gFnSc6	většina
o	o	k7c4	o
homogenní	homogenní	k2eAgInSc4d1	homogenní
ocelový	ocelový	k2eAgInSc4d1	ocelový
pancíř	pancíř	k1gInSc4	pancíř
s	s	k7c7	s
různou	různý	k2eAgFnSc7d1	různá
tloušťkou	tloušťka	k1gFnSc7	tloušťka
a	a	k8xC	a
tedy	tedy	k9	tedy
i	i	k9	i
odolností	odolnost	k1gFnSc7	odolnost
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
s	s	k7c7	s
nástupem	nástup	k1gInSc7	nástup
protitankových	protitankový	k2eAgFnPc2d1	protitanková
řízených	řízený	k2eAgFnPc2d1	řízená
střel	střela	k1gFnPc2	střela
a	a	k8xC	a
jaderných	jaderný	k2eAgFnPc2d1	jaderná
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
vyvinuty	vyvinout	k5eAaPmNgInP	vyvinout
nové	nový	k2eAgInPc1d1	nový
typy	typ	k1gInPc1	typ
pancéřování	pancéřování	k1gNnSc2	pancéřování
<g/>
,	,	kIx,	,
takzvané	takzvaný	k2eAgNnSc4d1	takzvané
vrstvené	vrstvený	k2eAgNnSc4d1	vrstvené
nebo	nebo	k8xC	nebo
kompozitní	kompozitní	k2eAgNnSc4d1	kompozitní
pancéřování	pancéřování	k1gNnSc4	pancéřování
<g/>
,	,	kIx,	,
označované	označovaný	k2eAgFnPc1d1	označovaná
také	také	k9	také
jako	jako	k9	jako
pancéřovaní	pancéřovaný	k2eAgMnPc1d1	pancéřovaný
typu	typ	k1gInSc2	typ
Chobham	Chobham	k1gInSc1	Chobham
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
základem	základ	k1gInSc7	základ
je	být	k5eAaImIp3nS	být
kovová	kovový	k2eAgFnSc1d1	kovová
matrice	matrice	k1gFnSc1	matrice
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yIgFnSc2	který
jsou	být	k5eAaImIp3nP	být
vlisovány	vlisovat	k5eAaPmNgFnP	vlisovat
keramické	keramický	k2eAgFnPc1d1	keramická
destičky	destička	k1gFnPc1	destička
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
dobře	dobře	k6eAd1	dobře
odolávají	odolávat	k5eAaImIp3nP	odolávat
jak	jak	k6eAd1	jak
kumulativním	kumulativní	k2eAgFnPc3d1	kumulativní
hlavicím	hlavice	k1gFnPc3	hlavice
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
kinetickým	kinetický	k2eAgInPc3d1	kinetický
projektilům	projektil	k1gInPc3	projektil
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
základ	základ	k1gInSc1	základ
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
jak	jak	k8xS	jak
z	z	k7c2	z
vnější	vnější	k2eAgFnSc2d1	vnější
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
i	i	k9	i
z	z	k7c2	z
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
strany	strana	k1gFnSc2	strana
obložen	obložen	k2eAgInSc1d1	obložen
několika	několik	k4yIc7	několik
různými	různý	k2eAgFnPc7d1	různá
vrstvami	vrstva	k1gFnPc7	vrstva
tak	tak	k9	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
chránily	chránit	k5eAaImAgFnP	chránit
keramické	keramický	k2eAgFnPc4d1	keramická
destičky	destička	k1gFnPc4	destička
proti	proti	k7c3	proti
poškození	poškození	k1gNnSc3	poškození
méně	málo	k6eAd2	málo
výkonnými	výkonný	k2eAgFnPc7d1	výkonná
zbraněmi	zbraň	k1gFnPc7	zbraň
a	a	k8xC	a
také	také	k9	také
posádku	posádka	k1gFnSc4	posádka
a	a	k8xC	a
vnitřní	vnitřní	k2eAgNnSc4d1	vnitřní
zařízení	zařízení	k1gNnSc4	zařízení
před	před	k7c7	před
úlomky	úlomek	k1gInPc7	úlomek
odštípnutými	odštípnutý	k2eAgInPc7d1	odštípnutý
z	z	k7c2	z
pancíře	pancíř	k1gInSc2	pancíř
při	při	k7c6	při
zásahu	zásah	k1gInSc6	zásah
tanku	tank	k1gInSc2	tank
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
většiny	většina	k1gFnSc2	většina
typů	typ	k1gInPc2	typ
tyto	tento	k3xDgFnPc1	tento
vrstvy	vrstva	k1gFnPc1	vrstva
slouží	sloužit	k5eAaImIp3nP	sloužit
také	také	k9	také
jako	jako	k9	jako
antiradiační	antiradiační	k2eAgInSc1d1	antiradiační
štít	štít	k1gInSc1	štít
pro	pro	k7c4	pro
odstínění	odstínění	k1gNnSc4	odstínění
pronikavé	pronikavý	k2eAgFnSc2d1	pronikavá
radiace	radiace	k1gFnSc2	radiace
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
při	při	k7c6	při
jaderném	jaderný	k2eAgInSc6d1	jaderný
výbuchu	výbuch	k1gInSc6	výbuch
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
takovéto	takovýto	k3xDgNnSc1	takovýto
pancéřování	pancéřování	k1gNnSc1	pancéřování
tvoří	tvořit	k5eAaImIp3nS	tvořit
podstatnou	podstatný	k2eAgFnSc4d1	podstatná
část	část	k1gFnSc4	část
váhy	váha	k1gFnSc2	váha
tanku	tank	k1gInSc2	tank
<g/>
,	,	kIx,	,
hledají	hledat	k5eAaImIp3nP	hledat
se	se	k3xPyFc4	se
cesty	cesta	k1gFnPc1	cesta
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
optimalizaci	optimalizace	k1gFnSc3	optimalizace
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
trendů	trend	k1gInPc2	trend
je	být	k5eAaImIp3nS	být
modularita	modularita	k1gFnSc1	modularita
a	a	k8xC	a
přídavné	přídavný	k2eAgFnPc1d1	přídavná
sady	sada	k1gFnPc1	sada
pancéřování	pancéřování	k1gNnSc2	pancéřování
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
v	v	k7c6	v
případě	případ	k1gInSc6	případ
potřeby	potřeba	k1gFnSc2	potřeba
dále	daleko	k6eAd2	daleko
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
odolnost	odolnost	k1gFnSc4	odolnost
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
jak	jak	k6eAd1	jak
o	o	k7c4	o
klasické	klasický	k2eAgInPc4d1	klasický
přídavné	přídavný	k2eAgInPc4d1	přídavný
pláty	plát	k1gInPc4	plát
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
o	o	k7c4	o
speciální	speciální	k2eAgNnSc4d1	speciální
mřížové	mřížový	k2eAgNnSc4d1	mřížové
pancéřování	pancéřování	k1gNnSc4	pancéřování
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
dokáže	dokázat	k5eAaPmIp3nS	dokázat
podstatně	podstatně	k6eAd1	podstatně
snížit	snížit	k5eAaPmF	snížit
efekt	efekt	k1gInSc4	efekt
kumulativních	kumulativní	k2eAgFnPc2d1	kumulativní
hlavic	hlavice	k1gFnPc2	hlavice
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
lehké	lehký	k2eAgNnSc4d1	lehké
a	a	k8xC	a
laciné	laciný	k2eAgNnSc4d1	laciné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Moderní	moderní	k2eAgInPc1d1	moderní
a	a	k8xC	a
modernizované	modernizovaný	k2eAgInPc1d1	modernizovaný
tanky	tank	k1gInPc1	tank
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
běžně	běžně	k6eAd1	běžně
opatřované	opatřovaný	k2eAgNnSc1d1	opatřovaný
reaktivním	reaktivní	k2eAgNnSc7d1	reaktivní
pancéřováním	pancéřování	k1gNnSc7	pancéřování
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
představuje	představovat	k5eAaImIp3nS	představovat
sada	sada	k1gFnSc1	sada
menších	malý	k2eAgFnPc2d2	menší
schránek	schránka	k1gFnPc2	schránka
s	s	k7c7	s
trhavinou	trhavina	k1gFnSc7	trhavina
uloženou	uložený	k2eAgFnSc7d1	uložená
mezi	mezi	k7c4	mezi
kovové	kovový	k2eAgFnPc4d1	kovová
desky	deska	k1gFnPc4	deska
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
odpálena	odpálit	k5eAaPmNgFnS	odpálit
v	v	k7c6	v
případě	případ	k1gInSc6	případ
zásahu	zásah	k1gInSc2	zásah
<g/>
.	.	kIx.	.
</s>
<s>
Energie	energie	k1gFnSc1	energie
výbuchu	výbuch	k1gInSc2	výbuch
a	a	k8xC	a
posun	posun	k1gInSc1	posun
desek	deska	k1gFnPc2	deska
pak	pak	k6eAd1	pak
efektivně	efektivně	k6eAd1	efektivně
narušuje	narušovat	k5eAaImIp3nS	narušovat
plazmový	plazmový	k2eAgInSc1d1	plazmový
paprsek	paprsek	k1gInSc1	paprsek
kumulativních	kumulativní	k2eAgFnPc2d1	kumulativní
hlavic	hlavice	k1gFnPc2	hlavice
nebo	nebo	k8xC	nebo
už	už	k6eAd1	už
méně	málo	k6eAd2	málo
efektivně	efektivně	k6eAd1	efektivně
rozrušuje	rozrušovat	k5eAaImIp3nS	rozrušovat
kinetické	kinetický	k2eAgInPc4d1	kinetický
penetrátory	penetrátor	k1gInPc4	penetrátor
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc4	první
reaktivní	reaktivní	k2eAgNnSc4d1	reaktivní
pancéřování	pancéřování	k1gNnSc4	pancéřování
zaváděly	zavádět	k5eAaImAgFnP	zavádět
armády	armáda	k1gFnPc1	armáda
Izraele	Izrael	k1gInSc2	Izrael
a	a	k8xC	a
Ruska	Rusko	k1gNnSc2	Rusko
(	(	kIx(	(
<g/>
SSSR	SSSR	kA	SSSR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
nás	my	k3xPp1nPc2	my
je	být	k5eAaImIp3nS	být
použito	použít	k5eAaPmNgNnS	použít
na	na	k7c6	na
modernizovaných	modernizovaný	k2eAgInPc6d1	modernizovaný
tancích	tanec	k1gInPc6	tanec
T-	T-	k1gFnSc2	T-
<g/>
72	[number]	k4	72
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
jsou	být	k5eAaImIp3nP	být
do	do	k7c2	do
výzbroje	výzbroj	k1gFnSc2	výzbroj
zaváděny	zaváděn	k2eAgInPc1d1	zaváděn
také	také	k9	také
aktivní	aktivní	k2eAgInPc1d1	aktivní
systémy	systém	k1gInPc1	systém
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
systémy	systém	k1gInPc4	systém
typu	typ	k1gInSc2	typ
soft-kill	softillum	k1gNnPc2	soft-killum
a	a	k8xC	a
hard-kill	hardilla	k1gFnPc2	hard-killa
<g/>
.	.	kIx.	.
</s>
<s>
Systémy	systém	k1gInPc1	systém
typu	typ	k1gInSc2	typ
soft-kill	softill	k1gInSc4	soft-kill
mají	mít	k5eAaImIp3nP	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
zabránit	zabránit	k5eAaPmF	zabránit
zásahu	zásah	k1gInSc3	zásah
tanku	tank	k1gInSc2	tank
řízenou	řízený	k2eAgFnSc7d1	řízená
střelou	střela	k1gFnSc7	střela
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
zaruší	zarušit	k5eAaPmIp3nS	zarušit
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
zničí	zničit	k5eAaPmIp3nS	zničit
její	její	k3xOp3gInSc1	její
naváděcí	naváděcí	k2eAgInSc1d1	naváděcí
systém	systém	k1gInSc1	systém
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
systémy	systém	k1gInPc1	systém
typu	typ	k1gInSc3	typ
hard-kill	hardill	k1gInSc1	hard-kill
ničí	ničit	k5eAaImIp3nS	ničit
samotnou	samotný	k2eAgFnSc4d1	samotná
přilétající	přilétající	k2eAgFnSc4d1	přilétající
střelu	střela	k1gFnSc4	střela
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
na	na	k7c4	na
větší	veliký	k2eAgFnSc4d2	veliký
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
antiraketou	antiraketa	k1gFnSc7	antiraketa
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
na	na	k7c4	na
kratší	krátký	k2eAgNnSc4d2	kratší
odpálením	odpálení	k1gNnSc7	odpálení
kovového	kovový	k2eAgInSc2d1	kovový
projektilu	projektil	k1gInSc2	projektil
proti	proti	k7c3	proti
střele	střela	k1gFnSc3	střela
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
zde	zde	k6eAd1	zde
jsou	být	k5eAaImIp3nP	být
průkopníky	průkopník	k1gMnPc4	průkopník
Izrael	Izrael	k1gInSc1	Izrael
a	a	k8xC	a
Rusko	Rusko	k1gNnSc1	Rusko
se	s	k7c7	s
systémy	systém	k1gInPc7	systém
Trophy	Tropha	k1gFnSc2	Tropha
<g/>
,	,	kIx,	,
Iron	iron	k1gInSc4	iron
Fist	Fista	k1gFnPc2	Fista
<g/>
,	,	kIx,	,
Štora	Štoro	k1gNnSc2	Štoro
a	a	k8xC	a
Arena	Areno	k1gNnSc2	Areno
<g/>
.	.	kIx.	.
</s>
<s>
Dlužno	dlužno	k6eAd1	dlužno
zmínit	zmínit	k5eAaPmF	zmínit
<g/>
,	,	kIx,	,
že	že	k8xS	že
systém	systém	k1gInSc1	systém
aktivní	aktivní	k2eAgFnSc2d1	aktivní
ochrany	ochrana	k1gFnSc2	ochrana
pancéřových	pancéřový	k2eAgNnPc2d1	pancéřové
vozidel	vozidlo	k1gNnPc2	vozidlo
<g/>
,	,	kIx,	,
používající	používající	k2eAgFnSc1d1	používající
jako	jako	k9	jako
akční	akční	k2eAgInSc4d1	akční
prvek	prvek	k1gInSc4	prvek
tzv.	tzv.	kA	tzv.
explozí	exploze	k1gFnSc7	exploze
formovanou	formovaný	k2eAgFnSc7d1	formovaná
sekeru	sekera	k1gFnSc4	sekera
(	(	kIx(	(
<g/>
EFA	EFA	kA	EFA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vyvíjen	vyvíjet	k5eAaImNgInS	vyvíjet
i	i	k9	i
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ochranu	ochrana	k1gFnSc4	ochrana
obvykle	obvykle	k6eAd1	obvykle
doplňuje	doplňovat	k5eAaImIp3nS	doplňovat
ještě	ještě	k9	ještě
sada	sada	k1gFnSc1	sada
multispektrálních	multispektrální	k2eAgInPc2d1	multispektrální
zadýmovacích	zadýmovací	k2eAgInPc2d1	zadýmovací
granátů	granát	k1gInPc2	granát
a	a	k8xC	a
také	také	k9	také
senzory	senzor	k1gInPc4	senzor
detekující	detekující	k2eAgInPc4d1	detekující
různé	různý	k2eAgInPc4d1	různý
druhy	druh	k1gInPc4	druh
hrozby	hrozba	k1gFnSc2	hrozba
<g/>
.	.	kIx.	.
</s>
<s>
Trendem	trend	k1gInSc7	trend
je	být	k5eAaImIp3nS	být
zapojovat	zapojovat	k5eAaImF	zapojovat
všechny	všechen	k3xTgFnPc4	všechen
tyto	tento	k3xDgFnPc4	tento
části	část	k1gFnPc4	část
do	do	k7c2	do
jednoho	jeden	k4xCgInSc2	jeden
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
detekuje	detekovat	k5eAaImIp3nS	detekovat
hrozbu	hrozba	k1gFnSc4	hrozba
<g/>
,	,	kIx,	,
identifikuje	identifikovat	k5eAaBmIp3nS	identifikovat
jí	on	k3xPp3gFnSc7	on
a	a	k8xC	a
provede	provést	k5eAaPmIp3nS	provést
adekvátní	adekvátní	k2eAgNnSc1d1	adekvátní
protiopatření	protiopatření	k1gNnSc1	protiopatření
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Posádka	posádka	k1gFnSc1	posádka
tanku	tank	k1gInSc2	tank
==	==	k?	==
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
tanků	tank	k1gInPc2	tank
obsluhuje	obsluhovat	k5eAaImIp3nS	obsluhovat
3	[number]	k4	3
až	až	k9	až
5	[number]	k4	5
členů	člen	k1gInPc2	člen
posádky	posádka	k1gFnSc2	posádka
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
•	•	k?	•
Velitel	velitel	k1gMnSc1	velitel
-	-	kIx~	-
Přijímá	přijímat	k5eAaImIp3nS	přijímat
rozkazy	rozkaz	k1gInPc4	rozkaz
od	od	k7c2	od
velení	velení	k1gNnSc2	velení
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zodpovědný	zodpovědný	k2eAgMnSc1d1	zodpovědný
jak	jak	k8xC	jak
za	za	k7c4	za
samotný	samotný	k2eAgInSc4d1	samotný
tank	tank	k1gInSc4	tank
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
za	za	k7c4	za
celou	celý	k2eAgFnSc4d1	celá
posádku	posádka	k1gFnSc4	posádka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
sám	sám	k3xTgMnSc1	sám
uděluje	udělovat	k5eAaImIp3nS	udělovat
vlastní	vlastní	k2eAgInPc4d1	vlastní
rozkazy	rozkaz	k1gInPc4	rozkaz
<g/>
.	.	kIx.	.
</s>
<s>
Vybírá	vybírat	k5eAaImIp3nS	vybírat
cíle	cíl	k1gInPc4	cíl
a	a	k8xC	a
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
o	o	k7c4	o
další	další	k2eAgFnPc4d1	další
činnosti	činnost	k1gFnPc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
tanky	tank	k1gInPc7	tank
či	či	k8xC	či
pěchotou	pěchota	k1gFnSc7	pěchota
<g/>
.	.	kIx.	.
</s>
<s>
Sedí	sedit	k5eAaImIp3nS	sedit
ve	v	k7c6	v
věži	věž	k1gFnSc6	věž
pod	pod	k7c7	pod
velitelským	velitelský	k2eAgInSc7d1	velitelský
poklopem	poklop	k1gInSc7	poklop
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
byl	být	k5eAaImAgInS	být
poklop	poklop	k1gInSc1	poklop
opatřen	opatřit	k5eAaPmNgInS	opatřit
neprůstřelnými	průstřelný	k2eNgInPc7d1	neprůstřelný
průzory	průzor	k1gInPc7	průzor
umožňující	umožňující	k2eAgFnSc2d1	umožňující
360	[number]	k4	360
<g/>
°	°	k?	°
rozhled	rozhled	k1gInSc4	rozhled
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
to	ten	k3xDgNnSc1	ten
nahrazují	nahrazovat	k5eAaImIp3nP	nahrazovat
moderní	moderní	k2eAgInPc1d1	moderní
digitální	digitální	k2eAgInPc1d1	digitální
pozorovací	pozorovací	k2eAgInPc1d1	pozorovací
přístroje	přístroj	k1gInPc1	přístroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
•	•	k?	•
Řidič	řidič	k1gMnSc1	řidič
-	-	kIx~	-
řídí	řídit	k5eAaImIp3nS	řídit
vozidlo	vozidlo	k1gNnSc1	vozidlo
<g/>
,	,	kIx,	,
v	v	k7c6	v
bojových	bojový	k2eAgFnPc6d1	bojová
podmínkách	podmínka	k1gFnPc6	podmínka
dle	dle	k7c2	dle
pokynů	pokyn	k1gInPc2	pokyn
velitele	velitel	k1gMnSc2	velitel
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
funguje	fungovat	k5eAaImIp3nS	fungovat
jako	jako	k9	jako
mechanik	mechanika	k1gFnPc2	mechanika
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
starosti	starost	k1gFnPc4	starost
správné	správný	k2eAgNnSc4d1	správné
fungování	fungování	k1gNnSc4	fungování
vozidla	vozidlo	k1gNnSc2	vozidlo
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
motoru	motor	k1gInSc2	motor
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
místo	místo	k1gNnSc1	místo
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
přední	přední	k2eAgFnSc6d1	přední
části	část	k1gFnSc6	část
korby	korba	k1gFnSc2	korba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
•	•	k?	•
Střelec	Střelec	k1gMnSc1	Střelec
-	-	kIx~	-
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
starosti	starost	k1gFnSc6	starost
tankový	tankový	k2eAgInSc4d1	tankový
kanón	kanón	k1gInSc4	kanón
a	a	k8xC	a
na	na	k7c4	na
rozkazy	rozkaz	k1gInPc4	rozkaz
velitele	velitel	k1gMnSc2	velitel
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
cíle	cíl	k1gInSc2	cíl
a	a	k8xC	a
ničí	ničí	k3xOyNgNnSc1	ničí
je	být	k5eAaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
taktéž	taktéž	k?	taktéž
hlásit	hlásit	k5eAaImF	hlásit
další	další	k2eAgInPc4d1	další
možné	možný	k2eAgInPc4d1	možný
cíle	cíl	k1gInPc4	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Sedí	sedit	k5eAaImIp3nS	sedit
ve	v	k7c6	v
věži	věž	k1gFnSc6	věž
pod	pod	k7c7	pod
velitelem	velitel	k1gMnSc7	velitel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
•	•	k?	•
Nabíječ	nabíječ	k1gInSc1	nabíječ
-	-	kIx~	-
Nabíjí	nabíjet	k5eAaImIp3nS	nabíjet
kanón	kanón	k1gInSc4	kanón
požadovanou	požadovaný	k2eAgFnSc7d1	požadovaná
municí	munice	k1gFnSc7	munice
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
mu	on	k3xPp3gMnSc3	on
velitel	velitel	k1gMnSc1	velitel
přikáže	přikázat	k5eAaPmIp3nS	přikázat
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
moderních	moderní	k2eAgNnPc2d1	moderní
vozidel	vozidlo	k1gNnPc2	vozidlo
tuto	tento	k3xDgFnSc4	tento
funkci	funkce	k1gFnSc4	funkce
někdy	někdy	k6eAd1	někdy
nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
automatický	automatický	k2eAgInSc1d1	automatický
nabíjecí	nabíjecí	k2eAgInSc1d1	nabíjecí
systém	systém	k1gInSc1	systém
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
pozice	pozice	k1gFnSc1	pozice
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
věži	věž	k1gFnSc6	věž
na	na	k7c6	na
opačné	opačný	k2eAgFnSc6d1	opačná
straně	strana	k1gFnSc6	strana
(	(	kIx(	(
<g/>
od	od	k7c2	od
kanónu	kanón	k1gInSc2	kanón
<g/>
)	)	kIx)	)
než	než	k8xS	než
střelec	střelec	k1gMnSc1	střelec
s	s	k7c7	s
velitelem	velitel	k1gMnSc7	velitel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
•	•	k?	•
Radista	radista	k1gMnSc1	radista
-	-	kIx~	-
udržuje	udržovat	k5eAaImIp3nS	udržovat
komunikaci	komunikace	k1gFnSc4	komunikace
s	s	k7c7	s
ostatními	ostatní	k2eAgNnPc7d1	ostatní
vozidly	vozidlo	k1gNnPc7	vozidlo
a	a	k8xC	a
často	často	k6eAd1	často
obsluhuje	obsluhovat	k5eAaImIp3nS	obsluhovat
kulomet	kulomet	k1gInSc1	kulomet
umístěný	umístěný	k2eAgInSc1d1	umístěný
v	v	k7c6	v
korbě	korba	k1gFnSc6	korba
<g/>
.	.	kIx.	.
</s>
<s>
Sedí	sedit	k5eAaImIp3nS	sedit
v	v	k7c6	v
přední	přední	k2eAgFnSc6d1	přední
části	část	k1gFnSc6	část
korby	korba	k1gFnSc2	korba
hned	hned	k6eAd1	hned
vedle	vedle	k7c2	vedle
řidiče	řidič	k1gMnSc2	řidič
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
člen	člen	k1gInSc1	člen
ještě	ještě	k9	ještě
více	hodně	k6eAd2	hodně
nahrazován	nahrazovat	k5eAaImNgInS	nahrazovat
než	než	k8xS	než
nabíječ	nabíječ	k1gInSc1	nabíječ
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
funkci	funkce	k1gFnSc4	funkce
může	moct	k5eAaImIp3nS	moct
snadno	snadno	k6eAd1	snadno
plnit	plnit	k5eAaImF	plnit
velitel	velitel	k1gMnSc1	velitel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Galerie	galerie	k1gFnSc1	galerie
umístění	umístění	k1gNnSc2	umístění
posádky	posádka	k1gFnSc2	posádka
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Tanky	tank	k1gInPc7	tank
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Německo	Německo	k1gNnSc1	Německo
===	===	k?	===
</s>
</p>
<p>
<s>
lehké	lehký	k2eAgNnSc1d1	lehké
<g/>
:	:	kIx,	:
Pz	Pz	k1gFnSc1	Pz
I	i	k9	i
<g/>
,	,	kIx,	,
Pz	Pz	k1gMnSc1	Pz
II	II	kA	II
<g/>
,	,	kIx,	,
Pz	Pz	k1gFnSc1	Pz
35	[number]	k4	35
<g/>
(	(	kIx(	(
<g/>
t	t	k?	t
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pz	Pz	k1gFnSc1	Pz
38	[number]	k4	38
<g/>
(	(	kIx(	(
<g/>
t	t	k?	t
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
střední	střední	k2eAgFnSc3d1	střední
<g/>
:	:	kIx,	:
Pz	Pz	k1gFnSc3	Pz
III	III	kA	III
<g/>
,	,	kIx,	,
Pz	Pz	k1gFnSc1	Pz
IV	Iva	k1gFnPc2	Iva
<g/>
,	,	kIx,	,
Panther	Panthra	k1gFnPc2	Panthra
(	(	kIx(	(
<g/>
Pz	Pz	k1gFnSc1	Pz
V	v	k7c6	v
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
těžké	těžký	k2eAgNnSc1d1	těžké
<g/>
:	:	kIx,	:
Tiger	Tiger	k1gInSc1	Tiger
I	I	kA	I
(	(	kIx(	(
<g/>
Pz	Pz	k1gMnSc1	Pz
VI	VI	kA	VI
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tiger	Tiger	k1gMnSc1	Tiger
II	II	kA	II
(	(	kIx(	(
<g/>
Königstiger	Königstiger	k1gMnSc1	Königstiger
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
SSSR	SSSR	kA	SSSR
===	===	k?	===
</s>
</p>
<p>
<s>
lehké	lehký	k2eAgNnSc1d1	lehké
<g/>
:	:	kIx,	:
BT	BT	kA	BT
<g/>
,	,	kIx,	,
T-	T-	k1gFnSc1	T-
<g/>
26	[number]	k4	26
<g/>
,	,	kIx,	,
T-	T-	k1gFnSc1	T-
<g/>
50	[number]	k4	50
<g/>
,	,	kIx,	,
T-	T-	k1gFnSc1	T-
<g/>
60	[number]	k4	60
<g/>
,	,	kIx,	,
<g/>
T-	T-	k1gFnSc1	T-
<g/>
40	[number]	k4	40
<g/>
,	,	kIx,	,
T-	T-	k1gFnSc1	T-
<g/>
46	[number]	k4	46
<g/>
,	,	kIx,	,
T-	T-	k1gFnSc1	T-
<g/>
30	[number]	k4	30
<g/>
,	,	kIx,	,
<g/>
T-	T-	k1gFnSc1	T-
<g/>
27	[number]	k4	27
<g/>
,	,	kIx,	,
<g/>
T-	T-	k1gFnSc1	T-
<g/>
38	[number]	k4	38
<g/>
,	,	kIx,	,
<g/>
T-	T-	k1gFnSc1	T-
<g/>
37	[number]	k4	37
<g/>
,	,	kIx,	,
<g/>
T-	T-	k1gFnSc1	T-
<g/>
18	[number]	k4	18
</s>
</p>
<p>
<s>
střední	střední	k2eAgFnSc1d1	střední
<g/>
:	:	kIx,	:
T-	T-	k1gFnSc1	T-
<g/>
28	[number]	k4	28
<g/>
,	,	kIx,	,
T-	T-	k1gFnSc1	T-
<g/>
29	[number]	k4	29
<g/>
,	,	kIx,	,
<g/>
T-	T-	k1gFnSc1	T-
<g/>
34	[number]	k4	34
</s>
</p>
<p>
<s>
těžké	těžký	k2eAgInPc1d1	těžký
<g/>
:	:	kIx,	:
IS	IS	kA	IS
<g/>
,	,	kIx,	,
IS-	IS-	k1gFnSc1	IS-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
KV-	KV-	k1gFnSc1	KV-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
KV-	KV-	k1gFnSc1	KV-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
KV-	KV-	k1gFnSc1	KV-
<g/>
85	[number]	k4	85
<g/>
,	,	kIx,	,
T-	T-	k1gFnSc1	T-
<g/>
35	[number]	k4	35
<g/>
,	,	kIx,	,
IS-	IS-	k1gFnSc1	IS-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
===	===	k?	===
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
===	===	k?	===
</s>
</p>
<p>
<s>
Pěchotní	pěchotní	k2eAgFnSc1d1	pěchotní
<g/>
:	:	kIx,	:
Matilda	Matilda	k1gFnSc1	Matilda
Mk	Mk	k1gFnSc1	Mk
I	i	k9	i
<g/>
,	,	kIx,	,
Matilda	Matilda	k1gMnSc1	Matilda
Mk	Mk	k1gFnSc2	Mk
II	II	kA	II
<g/>
,	,	kIx,	,
Valentine	Valentin	k1gMnSc5	Valentin
Mk	Mk	k1gMnSc5	Mk
III	III	kA	III
<g/>
,	,	kIx,	,
Churchill	Churchill	k1gMnSc1	Churchill
Mk	Mk	k1gFnSc2	Mk
IV	IV	kA	IV
</s>
</p>
<p>
<s>
Lehké	Lehké	k2eAgInSc1d1	Lehké
<g/>
:	:	kIx,	:
Vickers	Vickers	k1gInSc1	Vickers
Mark	Mark	k1gMnSc1	Mark
E	E	kA	E
<g/>
,	,	kIx,	,
Light	Light	k2eAgInSc1d1	Light
Tank	tank	k1gInSc1	tank
Mk	Mk	k1gFnSc2	Mk
II	II	kA	II
<g/>
,	,	kIx,	,
Light	Light	k2eAgInSc1d1	Light
Tank	tank	k1gInSc1	tank
Mk	Mk	k1gFnSc2	Mk
III	III	kA	III
<g/>
,	,	kIx,	,
Light	Light	k2eAgInSc1d1	Light
Tank	tank	k1gInSc1	tank
Mk	Mk	k1gFnSc2	Mk
IV	IV	kA	IV
<g/>
,	,	kIx,	,
Light	Light	k2eAgInSc1d1	Light
Tank	tank	k1gInSc1	tank
Mk	Mk	k1gFnSc2	Mk
V	V	kA	V
<g/>
,	,	kIx,	,
Light	Light	k2eAgInSc1d1	Light
Tank	tank	k1gInSc1	tank
Mk	Mk	k1gFnSc2	Mk
VI	VI	kA	VI
<g/>
,	,	kIx,	,
Mk	Mk	k1gMnSc1	Mk
VII	VII	kA	VII
Tetrarch	tetrarch	k1gMnSc1	tetrarch
<g/>
,	,	kIx,	,
Mk	Mk	k1gMnSc1	Mk
VIII	VIII	kA	VIII
Harry	Harra	k1gFnPc1	Harra
Hopkins	Hopkins	k1gInSc1	Hopkins
</s>
</p>
<p>
<s>
Střední	střední	k2eAgFnPc1d1	střední
<g/>
:	:	kIx,	:
Vickers	Vickers	k1gInSc1	Vickers
Mk	Mk	k1gFnSc2	Mk
II	II	kA	II
<g/>
,	,	kIx,	,
Sherman	Sherman	k1gMnSc1	Sherman
Firefly	Firefly	k1gMnSc1	Firefly
<g/>
,	,	kIx,	,
M3	M3	k1gMnSc1	M3
Grant	grant	k1gInSc1	grant
<g/>
,	,	kIx,	,
Comet	Comet	k1gInSc1	Comet
I	I	kA	I
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Křížníkové	Křížník	k1gMnPc1	Křížník
<g/>
:	:	kIx,	:
Cruiser	Cruiser	k1gMnSc1	Cruiser
Mk	Mk	k1gMnSc1	Mk
I	i	k9	i
<g/>
,	,	kIx,	,
Cruiser	Cruiser	k1gMnSc1	Cruiser
Mk	Mk	k1gFnSc2	Mk
II	II	kA	II
<g/>
,	,	kIx,	,
Cruiser	Cruiser	k1gMnSc1	Cruiser
Mk	Mk	k1gFnSc2	Mk
III	III	kA	III
<g/>
,	,	kIx,	,
Cruiser	Cruiser	k1gMnSc1	Cruiser
Mk	Mk	k1gFnSc2	Mk
IV	IV	kA	IV
<g/>
,	,	kIx,	,
Covenanter	Covenanter	k1gMnSc1	Covenanter
<g/>
,	,	kIx,	,
Crusader	Crusader	k1gMnSc1	Crusader
<g/>
,	,	kIx,	,
Cavalier	Cavalier	k1gMnSc1	Cavalier
<g/>
,	,	kIx,	,
Centaur	Centaur	k1gMnSc1	Centaur
<g/>
,	,	kIx,	,
Cromwell	Cromwell	k1gMnSc1	Cromwell
</s>
</p>
<p>
<s>
===	===	k?	===
USA	USA	kA	USA
===	===	k?	===
</s>
</p>
<p>
<s>
Lehké	Lehké	k2eAgFnSc1d1	Lehké
<g/>
:	:	kIx,	:
M22	M22	k1gFnSc1	M22
Locust	Locust	k1gFnSc1	Locust
<g/>
,	,	kIx,	,
M24	M24	k1gFnSc1	M24
Chaffee	Chaffee	k1gFnSc1	Chaffee
<g/>
,	,	kIx,	,
M-3	M-3	k1gFnSc1	M-3
Stuart	Stuart	k1gInSc1	Stuart
<g/>
,	,	kIx,	,
M-5	M-5	k1gFnSc1	M-5
Stuart	Stuarta	k1gFnPc2	Stuarta
</s>
</p>
<p>
<s>
Střední	střední	k2eAgNnSc1d1	střední
<g/>
:	:	kIx,	:
M3	M3	k1gMnSc4	M3
Lee	Lea	k1gFnSc6	Lea
<g/>
,	,	kIx,	,
M4	M4	k1gMnSc1	M4
Sherman	Sherman	k1gMnSc1	Sherman
<g/>
,	,	kIx,	,
M26	M26	k1gMnSc1	M26
Pershing	Pershing	k1gInSc1	Pershing
</s>
</p>
<p>
<s>
===	===	k?	===
Francie	Francie	k1gFnSc2	Francie
===	===	k?	===
</s>
</p>
<p>
<s>
Pěchotní	pěchotní	k2eAgInSc1d1	pěchotní
<g/>
:	:	kIx,	:
Renault	renault	k1gInSc1	renault
FT-	FT-	k1gFnSc2	FT-
<g/>
17	[number]	k4	17
<g/>
,	,	kIx,	,
FCM-	FCM-	k1gFnSc1	FCM-
<g/>
36	[number]	k4	36
<g/>
,	,	kIx,	,
Hotchkiss	Hotchkiss	k1gInSc1	Hotchkiss
H-	H-	k1gFnSc1	H-
<g/>
35	[number]	k4	35
<g/>
/	/	kIx~	/
<g/>
38	[number]	k4	38
<g/>
/	/	kIx~	/
<g/>
39	[number]	k4	39
<g/>
,	,	kIx,	,
Renault	renault	k1gInSc1	renault
B-	B-	k1gFnSc1	B-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
Renault	renault	k1gInSc1	renault
D-	D-	k1gFnSc1	D-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
Renault	renault	k1gInSc1	renault
D-	D-	k1gFnSc1	D-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
Renault	renault	k1gInSc1	renault
R-	R-	k1gFnSc1	R-
<g/>
35	[number]	k4	35
<g/>
/	/	kIx~	/
<g/>
39	[number]	k4	39
<g/>
/	/	kIx~	/
<g/>
40	[number]	k4	40
</s>
</p>
<p>
<s>
Jezdecké	jezdecký	k2eAgFnPc1d1	jezdecká
<g/>
:	:	kIx,	:
Renault	renault	k1gInSc1	renault
AMC-	AMC-	k1gFnSc2	AMC-
<g/>
34	[number]	k4	34
<g/>
/	/	kIx~	/
<g/>
35	[number]	k4	35
<g/>
,	,	kIx,	,
Renault	renault	k1gInSc1	renault
AMR-	AMR-	k1gFnSc1	AMR-
<g/>
33	[number]	k4	33
<g/>
/	/	kIx~	/
<g/>
35	[number]	k4	35
<g/>
,	,	kIx,	,
SOMUA	SOMUA	kA	SOMUA
S-35	S-35	k1gFnSc1	S-35
</s>
</p>
<p>
<s>
===	===	k?	===
Itálie	Itálie	k1gFnSc2	Itálie
===	===	k?	===
</s>
</p>
<p>
<s>
Lehké	Lehká	k1gFnPc1	Lehká
<g/>
:	:	kIx,	:
CV	CV	kA	CV
29	[number]	k4	29
<g/>
,	,	kIx,	,
Fiat	fiat	k1gInSc1	fiat
3000	[number]	k4	3000
<g/>
,	,	kIx,	,
L	L	kA	L
<g/>
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
33	[number]	k4	33
<g/>
,	,	kIx,	,
L	L	kA	L
<g/>
6	[number]	k4	6
<g/>
/	/	kIx~	/
<g/>
40	[number]	k4	40
</s>
</p>
<p>
<s>
Střední	střední	k1gMnSc1	střední
<g/>
:	:	kIx,	:
M	M	kA	M
<g/>
11	[number]	k4	11
<g/>
/	/	kIx~	/
<g/>
39	[number]	k4	39
<g/>
,	,	kIx,	,
M	M	kA	M
<g/>
13	[number]	k4	13
<g/>
/	/	kIx~	/
<g/>
40	[number]	k4	40
<g/>
,	,	kIx,	,
<g/>
M	M	kA	M
<g/>
14	[number]	k4	14
<g/>
/	/	kIx~	/
<g/>
41	[number]	k4	41
<g/>
,	,	kIx,	,
M	M	kA	M
<g/>
15	[number]	k4	15
<g/>
/	/	kIx~	/
<g/>
42	[number]	k4	42
</s>
</p>
<p>
<s>
Těžké	těžký	k2eAgInPc1d1	těžký
<g/>
:	:	kIx,	:
P	P	kA	P
<g/>
26	[number]	k4	26
<g/>
/	/	kIx~	/
<g/>
40	[number]	k4	40
</s>
</p>
<p>
<s>
===	===	k?	===
Japonsko	Japonsko	k1gNnSc1	Japonsko
===	===	k?	===
</s>
</p>
<p>
<s>
Plovoucí	plovoucí	k2eAgInSc1d1	plovoucí
<g/>
:	:	kIx,	:
Typ	typ	k1gInSc1	typ
2	[number]	k4	2
Ka-Mi	Ka-Mi	k1gNnSc2	Ka-Mi
<g/>
,	,	kIx,	,
Typ	typ	k1gInSc1	typ
3	[number]	k4	3
Ka-Chi	Ka-Ch	k1gFnSc2	Ka-Ch
</s>
</p>
<p>
<s>
Lehké	Lehké	k2eAgInSc4d1	Lehké
<g/>
:	:	kIx,	:
Typ	typ	k1gInSc4	typ
2	[number]	k4	2
Ke-To	Ke-To	k1gNnSc4	Ke-To
<g/>
,	,	kIx,	,
Typ	typ	k1gInSc1	typ
4	[number]	k4	4
Ke-Nu	Ke-Nus	k1gInSc2	Ke-Nus
<g/>
,	,	kIx,	,
Typ	typ	k1gInSc1	typ
92	[number]	k4	92
Džú-sókóša	Džúókóšum	k1gNnSc2	Džú-sókóšum
<g/>
,	,	kIx,	,
Typ	typ	k1gInSc1	typ
94	[number]	k4	94
Te-Ke	Te-Ke	k1gFnPc2	Te-Ke
<g/>
,	,	kIx,	,
Typ	typ	k1gInSc4	typ
95	[number]	k4	95
Ha-Go	Ha-Go	k1gNnSc4	Ha-Go
<g/>
,	,	kIx,	,
Typ	typ	k1gInSc1	typ
97	[number]	k4	97
Te-Ke	Te-K	k1gInSc2	Te-K
</s>
</p>
<p>
<s>
Střední	střední	k2eAgFnPc1d1	střední
<g/>
:	:	kIx,	:
Typ	typ	k1gInSc1	typ
3	[number]	k4	3
Chi-Nu	Chi-Nus	k1gInSc2	Chi-Nus
<g/>
,	,	kIx,	,
Typ	typ	k1gInSc1	typ
4	[number]	k4	4
Chi-To	Chi-To	k6eAd1	Chi-To
<g/>
,	,	kIx,	,
Typ	typ	k1gInSc4	typ
89	[number]	k4	89
Chi-Ro	Chi-Ro	k1gNnSc4	Chi-Ro
<g/>
,	,	kIx,	,
Typ	typ	k1gInSc1	typ
97	[number]	k4	97
Chi-Ha	Chi-H	k1gInSc2	Chi-H
</s>
</p>
<p>
<s>
===	===	k?	===
Polsko	Polsko	k1gNnSc1	Polsko
===	===	k?	===
</s>
</p>
<p>
<s>
Lehké	Lehká	k1gFnPc1	Lehká
<g/>
:	:	kIx,	:
7	[number]	k4	7
<g/>
TP	TP	kA	TP
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
TP	TP	kA	TP
<g/>
,	,	kIx,	,
14TP	[number]	k4	14TP
</s>
</p>
<p>
<s>
===	===	k?	===
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
===	===	k?	===
</s>
</p>
<p>
<s>
Tančíky	tančík	k1gInPc1	tančík
<g/>
:	:	kIx,	:
Fiat	fiat	k1gInSc1	fiat
Ansaldo	Ansaldo	k1gNnSc1	Ansaldo
</s>
</p>
<p>
<s>
Lehké	Lehké	k2eAgMnPc1d1	Lehké
<g/>
:	:	kIx,	:
Toldi	Told	k1gMnPc1	Told
III	III	kA	III
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Střední	střední	k1gMnSc1	střední
<g/>
:	:	kIx,	:
Turán	Turán	k1gMnSc1	Turán
III	III	kA	III
prototípus	prototípus	k1gMnSc1	prototípus
</s>
</p>
<p>
<s>
===	===	k?	===
Kanada	Kanada	k1gFnSc1	Kanada
===	===	k?	===
</s>
</p>
<p>
<s>
Křížníkové	Křížníkové	k2eAgFnSc1d1	Křížníkové
<g/>
:	:	kIx,	:
Ram	Ram	k1gFnSc1	Ram
2	[number]	k4	2
</s>
</p>
<p>
<s>
===	===	k?	===
Austrálie	Austrálie	k1gFnSc2	Austrálie
===	===	k?	===
</s>
</p>
<p>
<s>
Křížníkové	Křížníkové	k2eAgInSc1d1	Křížníkové
<g/>
:	:	kIx,	:
Sentinel	sentinel	k1gInSc1	sentinel
</s>
</p>
<p>
<s>
===	===	k?	===
Švédsko	Švédsko	k1gNnSc1	Švédsko
===	===	k?	===
</s>
</p>
<p>
<s>
Lehké	Lehké	k2eAgInSc1d1	Lehké
<g/>
:	:	kIx,	:
Landsverk	Landsverk	k1gInSc1	Landsverk
L-60	L-60	k1gFnSc2	L-60
</s>
</p>
<p>
<s>
==	==	k?	==
Tanky	tank	k1gInPc7	tank
ve	v	k7c6	v
výzbroji	výzbroj	k1gInSc6	výzbroj
české	český	k2eAgFnSc2d1	Česká
a	a	k8xC	a
slovenské	slovenský	k2eAgFnSc2d1	slovenská
armády	armáda	k1gFnSc2	armáda
==	==	k?	==
</s>
</p>
<p>
<s>
Renault	renault	k1gInSc1	renault
FT-	FT-	k1gFnSc1	FT-
<g/>
17	[number]	k4	17
<g/>
,	,	kIx,	,
Carden-Loyd	Carden-Loyd	k1gMnSc1	Carden-Loyd
Mk	Mk	k1gMnSc1	Mk
<g/>
.	.	kIx.	.
<g/>
VI	VI	kA	VI
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Tančík	tančík	k1gInSc1	tančík
vz.	vz.	k?	vz.
33	[number]	k4	33
<g/>
,	,	kIx,	,
LT-	LT-	k1gFnSc1	LT-
<g/>
34	[number]	k4	34
<g/>
,	,	kIx,	,
LT-	LT-	k1gFnSc1	LT-
<g/>
35	[number]	k4	35
<g/>
,	,	kIx,	,
LT-	LT-	k1gFnSc1	LT-
<g/>
38	[number]	k4	38
<g/>
,	,	kIx,	,
LLT	LLT	kA	LLT
(	(	kIx(	(
<g/>
LT-	LT-	k1gFnSc1	LT-
<g/>
40	[number]	k4	40
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
M5	M5	k1gFnSc1	M5
Stuart	Stuart	k1gInSc1	Stuart
<g/>
,	,	kIx,	,
Crusader	Crusader	k1gInSc1	Crusader
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Challenger	Challenger	k1gMnSc1	Challenger
<g/>
,	,	kIx,	,
Cromwell	Cromwell	k1gMnSc1	Cromwell
<g/>
,	,	kIx,	,
Sherman	Sherman	k1gMnSc1	Sherman
Firefly	Firefly	k1gMnSc1	Firefly
<g/>
,	,	kIx,	,
T-	T-	k1gFnSc1	T-
<g/>
70	[number]	k4	70
<g/>
,	,	kIx,	,
T-	T-	k1gFnSc1	T-
<g/>
34	[number]	k4	34
<g/>
/	/	kIx~	/
<g/>
76	[number]	k4	76
<g/>
,	,	kIx,	,
T-	T-	k1gFnSc1	T-
<g/>
34	[number]	k4	34
<g/>
/	/	kIx~	/
<g/>
85	[number]	k4	85
<g/>
,	,	kIx,	,
IS-	IS-	k1gFnSc1	IS-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
Pz-III	Pz-III	k1gMnPc3	Pz-III
<g/>
,	,	kIx,	,
Pz-IV	Pz-IV	k1gMnPc3	Pz-IV
<g/>
,	,	kIx,	,
Panther	Panthra	k1gFnPc2	Panthra
<g/>
,	,	kIx,	,
Tiger	Tiger	k1gInSc1	Tiger
I	I	kA	I
<g/>
,	,	kIx,	,
IS-	IS-	k1gFnSc1	IS-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
T-	T-	k1gFnSc1	T-
<g/>
54	[number]	k4	54
<g/>
,	,	kIx,	,
T-	T-	k1gFnSc1	T-
<g/>
55	[number]	k4	55
<g/>
,	,	kIx,	,
T-72	T-72	k1gFnSc1	T-72
a	a	k8xC	a
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
modifikace	modifikace	k1gFnSc2	modifikace
T-72M4CZ	T-72M4CZ	k1gFnSc2	T-72M4CZ
</s>
</p>
<p>
<s>
==	==	k?	==
Speciální	speciální	k2eAgInPc1d1	speciální
varianty	variant	k1gInPc1	variant
tanku	tank	k1gInSc2	tank
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Velitelský	velitelský	k2eAgInSc4d1	velitelský
tank	tank	k1gInSc4	tank
===	===	k?	===
</s>
</p>
<p>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
o	o	k7c4	o
běžný	běžný	k2eAgInSc4d1	běžný
bojový	bojový	k2eAgInSc4d1	bojový
tank	tank	k1gInSc4	tank
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
dovybavený	dovybavený	k2eAgInSc1d1	dovybavený
výkonnějšími	výkonný	k2eAgInPc7d2	výkonnější
prostředky	prostředek	k1gInPc7	prostředek
spojení	spojení	k1gNnSc2	spojení
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
umožnil	umožnit	k5eAaPmAgInS	umožnit
veliteli	velitel	k1gMnSc3	velitel
komunikovat	komunikovat	k5eAaImF	komunikovat
s	s	k7c7	s
podřízenými	podřízený	k2eAgInPc7d1	podřízený
tanky	tank	k1gInPc7	tank
i	i	k8xC	i
nadřízenými	nadřízený	k2eAgInPc7d1	nadřízený
stupni	stupeň	k1gInPc7	stupeň
velení	velení	k1gNnSc2	velení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Odminovací	odminovací	k2eAgInSc4d1	odminovací
tank	tank	k1gInSc4	tank
===	===	k?	===
</s>
</p>
<p>
<s>
Tank	tank	k1gInSc4	tank
určený	určený	k2eAgInSc4d1	určený
k	k	k7c3	k
vytváření	vytváření	k1gNnSc3	vytváření
průchodu	průchod	k1gInSc2	průchod
v	v	k7c6	v
minových	minový	k2eAgNnPc6d1	minové
polích	pole	k1gNnPc6	pole
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
standardní	standardní	k2eAgInSc4d1	standardní
tank	tank	k1gInSc4	tank
vybavený	vybavený	k2eAgInSc1d1	vybavený
přídavným	přídavný	k2eAgNnSc7d1	přídavné
zařízením	zařízení	k1gNnSc7	zařízení
<g/>
,	,	kIx,	,
sloužícímu	sloužící	k1gMnSc3	sloužící
k	k	k7c3	k
zneškodnění	zneškodnění	k1gNnSc3	zneškodnění
min	mina	k1gFnPc2	mina
buď	buď	k8xC	buď
jejich	jejich	k3xOp3gNnSc7	jejich
rozbitím	rozbití	k1gNnSc7	rozbití
či	či	k8xC	či
přivedením	přivedení	k1gNnSc7	přivedení
k	k	k7c3	k
předčasnému	předčasný	k2eAgInSc3d1	předčasný
výbuchu	výbuch	k1gInSc3	výbuch
<g/>
.	.	kIx.	.
</s>
<s>
Zařízení	zařízení	k1gNnSc1	zařízení
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
podobu	podoba	k1gFnSc4	podoba
pluhu	pluh	k1gInSc2	pluh
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
miny	mina	k1gFnSc2	mina
vyorává	vyorávat	k5eAaImIp3nS	vyorávat
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
podobu	podoba	k1gFnSc4	podoba
válce	válec	k1gInSc2	válec
s	s	k7c7	s
řetězy	řetěz	k1gInPc7	řetěz
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
účelem	účel	k1gInSc7	účel
je	být	k5eAaImIp3nS	být
bušit	bušit	k5eAaImF	bušit
do	do	k7c2	do
země	zem	k1gFnSc2	zem
a	a	k8xC	a
přivádět	přivádět	k5eAaImF	přivádět
miny	mina	k1gFnPc4	mina
k	k	k7c3	k
výbuchu	výbuch	k1gInSc3	výbuch
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
může	moct	k5eAaImIp3nS	moct
vytvářet	vytvářet	k5eAaImF	vytvářet
silné	silný	k2eAgNnSc4d1	silné
elektromagnetické	elektromagnetický	k2eAgNnSc4d1	elektromagnetické
pole	pole	k1gNnSc4	pole
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
aktivuje	aktivovat	k5eAaBmIp3nS	aktivovat
miny	mina	k1gFnPc1	mina
reagující	reagující	k2eAgFnPc1d1	reagující
na	na	k7c4	na
průjezd	průjezd	k1gInSc4	průjezd
kovových	kovový	k2eAgInPc2d1	kovový
(	(	kIx(	(
<g/>
magnetických	magnetický	k2eAgInPc2d1	magnetický
<g/>
)	)	kIx)	)
prostředků	prostředek	k1gInPc2	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Výjimečně	výjimečně	k6eAd1	výjimečně
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
setkat	setkat	k5eAaPmF	setkat
i	i	k9	i
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
tankovým	tankový	k2eAgInSc7d1	tankový
podvozkem	podvozek	k1gInSc7	podvozek
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
věže	věž	k1gFnSc2	věž
<g/>
)	)	kIx)	)
vybaveným	vybavený	k2eAgMnPc3d1	vybavený
některým	některý	k3yIgNnPc3	některý
z	z	k7c2	z
výše	vysoce	k6eAd2	vysoce
uvedených	uvedený	k2eAgNnPc2d1	uvedené
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Plamenometný	plamenometný	k2eAgInSc4d1	plamenometný
tank	tank	k1gInSc4	tank
===	===	k?	===
</s>
</p>
<p>
<s>
Tank	tank	k1gInSc1	tank
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterého	který	k3yQgNnSc2	který
plamenomet	plamenomet	k1gInSc1	plamenomet
doplňuje	doplňovat	k5eAaImIp3nS	doplňovat
či	či	k8xC	či
nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
kanónovou	kanónový	k2eAgFnSc4d1	kanónová
výzbroj	výzbroj	k1gFnSc4	výzbroj
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
tanky	tank	k1gInPc1	tank
byly	být	k5eAaImAgInP	být
ve	v	k7c6	v
výzbroji	výzbroj	k1gFnSc6	výzbroj
hlavních	hlavní	k2eAgFnPc2d1	hlavní
bojujících	bojující	k2eAgFnPc2d1	bojující
armád	armáda	k1gFnPc2	armáda
za	za	k7c4	za
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ženijní	ženijní	k2eAgInSc4d1	ženijní
tank	tank	k1gInSc4	tank
(	(	kIx(	(
<g/>
ozbrojený	ozbrojený	k2eAgMnSc1d1	ozbrojený
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Tank	tank	k1gInSc1	tank
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
standardní	standardní	k2eAgInSc1d1	standardní
kanón	kanón	k1gInSc1	kanón
je	být	k5eAaImIp3nS	být
nahrazen	nahradit	k5eAaPmNgInS	nahradit
krátkým	krátký	k2eAgInSc7d1	krátký
kanónem	kanón	k1gInSc7	kanón
větší	veliký	k2eAgFnSc2d2	veliký
ráže	ráže	k1gFnSc2	ráže
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
účelem	účel	k1gInSc7	účel
je	být	k5eAaImIp3nS	být
boření	boření	k1gNnSc1	boření
polních	polní	k2eAgNnPc2d1	polní
opevnění	opevnění	k1gNnPc2	opevnění
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k6eAd1	mimo
tohoto	tento	k3xDgInSc2	tento
úkolu	úkol	k1gInSc2	úkol
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
přepravě	přeprava	k1gFnSc3	přeprava
pomocných	pomocný	k2eAgInPc2d1	pomocný
prostředků	prostředek	k1gInPc2	prostředek
k	k	k7c3	k
překonávání	překonávání	k1gNnSc3	překonávání
těžkého	těžký	k2eAgInSc2d1	těžký
terénu	terén	k1gInSc2	terén
<g/>
:	:	kIx,	:
svazků	svazek	k1gInPc2	svazek
hatí	hať	k1gFnPc2	hať
nebo	nebo	k8xC	nebo
klád	kláda	k1gFnPc2	kláda
(	(	kIx(	(
<g/>
k	k	k7c3	k
zasypání	zasypání	k1gNnSc3	zasypání
okopu-vytvoření	okopuytvoření	k1gNnSc2	okopu-vytvoření
přejezdu	přejezd	k1gInSc2	přejezd
pro	pro	k7c4	pro
jinou	jiný	k2eAgFnSc4d1	jiná
techniku	technika	k1gFnSc4	technika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
různých	různý	k2eAgFnPc2d1	různá
rohoží	rohož	k1gFnPc2	rohož
(	(	kIx(	(
<g/>
ke	k	k7c3	k
zpevnění	zpevnění	k1gNnSc3	zpevnění
příliš	příliš	k6eAd1	příliš
měkkého	měkký	k2eAgInSc2d1	měkký
povrchu	povrch	k1gInSc2	povrch
půdy	půda	k1gFnSc2	půda
<g/>
)	)	kIx)	)
apod.	apod.	kA	apod.
Tento	tento	k3xDgInSc4	tento
typ	typ	k1gInSc4	typ
ženijního	ženijní	k2eAgInSc2d1	ženijní
tanku	tank	k1gInSc2	tank
je	být	k5eAaImIp3nS	být
využíván	využívat	k5eAaPmNgInS	využívat
hlavně	hlavně	k9	hlavně
Britskou	britský	k2eAgFnSc7d1	britská
armádou	armáda	k1gFnSc7	armáda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Další	další	k2eAgNnSc4d1	další
využití	využití	k1gNnSc4	využití
tankových	tankový	k2eAgInPc2d1	tankový
podvozků	podvozek	k1gInPc2	podvozek
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Mostní	mostní	k2eAgInSc4d1	mostní
tank	tank	k1gInSc4	tank
===	===	k?	===
</s>
</p>
<p>
<s>
Mostní	mostní	k2eAgInSc1d1	mostní
tank	tank	k1gInSc1	tank
je	být	k5eAaImIp3nS	být
samohybné	samohybný	k2eAgInPc1d1	samohybný
obrněné	obrněný	k2eAgInPc1d1	obrněný
transportní	transportní	k2eAgInPc1d1	transportní
vozidlo-nosič	vozidloosič	k1gInSc1	vozidlo-nosič
<g/>
,	,	kIx,	,
schopné	schopný	k2eAgFnPc1d1	schopná
nést	nést	k5eAaImF	nést
a	a	k8xC	a
pomocí	pomocí	k7c2	pomocí
zabudovaného	zabudovaný	k2eAgInSc2d1	zabudovaný
mechanismu	mechanismus	k1gInSc2	mechanismus
pokládat	pokládat	k5eAaImF	pokládat
a	a	k8xC	a
zvedat	zvedat	k5eAaImF	zvedat
mostní	mostní	k2eAgFnSc4d1	mostní
konstrukci	konstrukce	k1gFnSc4	konstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgNnSc1	takový
vozidlo	vozidlo	k1gNnSc1	vozidlo
s	s	k7c7	s
mostní	mostní	k2eAgFnSc7d1	mostní
konstrukcí	konstrukce	k1gFnSc7	konstrukce
pracuje	pracovat	k5eAaImIp3nS	pracovat
jako	jako	k9	jako
integrovaný	integrovaný	k2eAgInSc1d1	integrovaný
systém	systém	k1gInSc1	systém
<g/>
.	.	kIx.	.
<g/>
Tento	tento	k3xDgInSc1	tento
tank	tank	k1gInSc1	tank
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
již	již	k6eAd1	již
za	za	k7c4	za
2	[number]	k4	2
sv.	sv.	kA	sv.
v.	v.	k?	v.
při	při	k7c6	při
vylodění	vylodění	k1gNnSc6	vylodění
v	v	k7c6	v
Normandii	Normandie	k1gFnSc6	Normandie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Obrněné	obrněný	k2eAgNnSc1d1	obrněné
vyprošťovací	vyprošťovací	k2eAgNnSc1d1	vyprošťovací
vozidlo	vozidlo	k1gNnSc1	vozidlo
(	(	kIx(	(
<g/>
Vyprošťovací	vyprošťovací	k2eAgInSc4d1	vyprošťovací
tank	tank	k1gInSc4	tank
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Vyprošťovací	vyprošťovací	k2eAgInSc4d1	vyprošťovací
tank	tank	k1gInSc4	tank
je	být	k5eAaImIp3nS	být
samohybné	samohybný	k2eAgNnSc1d1	samohybné
obrněné	obrněný	k2eAgNnSc1d1	obrněné
transportní	transportní	k2eAgNnSc1d1	transportní
vozidlo	vozidlo	k1gNnSc1	vozidlo
určené	určený	k2eAgNnSc1d1	určené
k	k	k7c3	k
vlečení	vlečení	k1gNnSc3	vlečení
havarované	havarovaný	k2eAgFnSc2d1	havarovaná
a	a	k8xC	a
poškozené	poškozený	k2eAgFnSc2d1	poškozená
techniky	technika	k1gFnSc2	technika
<g/>
,	,	kIx,	,
k	k	k7c3	k
vyprošťování	vyprošťování	k1gNnSc3	vyprošťování
techniky	technika	k1gFnSc2	technika
<g/>
,	,	kIx,	,
ke	k	k7c3	k
zdvíhání	zdvíhání	k1gNnSc3	zdvíhání
břemen	břemeno	k1gNnPc2	břemeno
(	(	kIx(	(
<g/>
např.	např.	kA	např.
tankových	tankový	k2eAgInPc2d1	tankový
motorů	motor	k1gInPc2	motor
při	při	k7c6	při
jejich	jejich	k3xOp3gFnSc6	jejich
opravě	oprava	k1gFnSc6	oprava
či	či	k8xC	či
výměně	výměna	k1gFnSc6	výměna
<g/>
)	)	kIx)	)
a	a	k8xC	a
přepravě	přeprava	k1gFnSc3	přeprava
materiálu	materiál	k1gInSc2	materiál
na	na	k7c6	na
plošině	plošina	k1gFnSc6	plošina
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
výbavě	výbava	k1gFnSc3	výbava
patřívá	patřívat	k5eAaImIp3nS	patřívat
i	i	k9	i
elektrická	elektrický	k2eAgFnSc1d1	elektrická
svářečka	svářečka	k1gFnSc1	svářečka
<g/>
,	,	kIx,	,
či	či	k8xC	či
různé	různý	k2eAgNnSc4d1	různé
nářadí	nářadí	k1gNnSc4	nářadí
k	k	k7c3	k
opravám	oprava	k1gFnPc3	oprava
techniky	technika	k1gFnSc2	technika
v	v	k7c6	v
polním	polní	k2eAgNnSc6d1	polní
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ženijní	ženijní	k2eAgInSc4d1	ženijní
tank	tank	k1gInSc4	tank
===	===	k?	===
</s>
</p>
<p>
<s>
Tankový	tankový	k2eAgInSc1d1	tankový
podvozek	podvozek	k1gInSc1	podvozek
vybavený	vybavený	k2eAgInSc1d1	vybavený
např.	např.	kA	např.
dozerovou	dozerův	k2eAgFnSc7d1	dozerův
radlicí	radlice	k1gFnSc7	radlice
<g/>
,	,	kIx,	,
rypadlovým	rypadlový	k2eAgNnSc7d1	rypadlový
ramenem	rameno	k1gNnSc7	rameno
apod.	apod.	kA	apod.
Současně	současně	k6eAd1	současně
převáží	převážit	k5eAaPmIp3nS	převážit
spoustu	spousta	k1gFnSc4	spousta
ručního	ruční	k2eAgNnSc2d1	ruční
nářadí	nářadí	k1gNnSc2	nářadí
(	(	kIx(	(
<g/>
lopaty	lopata	k1gFnPc1	lopata
<g/>
,	,	kIx,	,
krumpáče	krumpáč	k1gInPc1	krumpáč
<g/>
,	,	kIx,	,
sekery	sekera	k1gFnPc1	sekera
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Taktéž	Taktéž	k?	Taktéž
může	moct	k5eAaImIp3nS	moct
plnit	plnit	k5eAaImF	plnit
úlohy	úloha	k1gFnPc4	úloha
vyprošťovacího	vyprošťovací	k2eAgInSc2d1	vyprošťovací
tanku	tank	k1gInSc2	tank
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Těžký	těžký	k2eAgInSc1d1	těžký
obrněný	obrněný	k2eAgInSc1d1	obrněný
transportér	transportér	k1gInSc1	transportér
===	===	k?	===
</s>
</p>
<p>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
využití	využití	k1gNnPc4	využití
silně	silně	k6eAd1	silně
pancéřovaného	pancéřovaný	k2eAgInSc2d1	pancéřovaný
tankového	tankový	k2eAgInSc2d1	tankový
podvozku	podvozek	k1gInSc2	podvozek
k	k	k7c3	k
přepravě	přeprava	k1gFnSc3	přeprava
pěchoty	pěchota	k1gFnSc2	pěchota
na	na	k7c4	na
bojiště	bojiště	k1gNnSc4	bojiště
<g/>
.	.	kIx.	.
</s>
<s>
Průkopníkem	průkopník	k1gMnSc7	průkopník
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
je	být	k5eAaImIp3nS	být
Izrael	Izrael	k1gInSc1	Izrael
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
nejdříve	dříve	k6eAd3	dříve
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
účelu	účel	k1gInSc3	účel
přestavoval	přestavovat	k5eAaImAgMnS	přestavovat
ukořistěné	ukořistěný	k2eAgInPc4d1	ukořistěný
tanky	tank	k1gInPc4	tank
sovětského	sovětský	k2eAgInSc2d1	sovětský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yQgFnPc2	který
odstranil	odstranit	k5eAaPmAgMnS	odstranit
věž	věž	k1gFnSc4	věž
(	(	kIx(	(
<g/>
čímž	což	k3yRnSc7	což
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
prostor	prostor	k1gInSc4	prostor
pro	pro	k7c4	pro
výsadek	výsadek	k1gInSc4	výsadek
<g/>
)	)	kIx)	)
a	a	k8xC	a
původní	původní	k2eAgInSc1d1	původní
motor	motor	k1gInSc1	motor
s	s	k7c7	s
převodovkou	převodovka	k1gFnSc7	převodovka
vyměnil	vyměnit	k5eAaPmAgMnS	vyměnit
za	za	k7c4	za
menší	malý	k2eAgInSc4d2	menší
motor-převodový	motorřevodový	k2eAgInSc4d1	motor-převodový
blok	blok	k1gInSc4	blok
(	(	kIx(	(
<g/>
což	což	k3yRnSc1	což
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
vytvořit	vytvořit	k5eAaPmF	vytvořit
vedle	vedle	k7c2	vedle
tohoto	tento	k3xDgInSc2	tento
bloku	blok	k1gInSc2	blok
tunel	tunel	k1gInSc1	tunel
pro	pro	k7c4	pro
výstup	výstup	k1gInSc4	výstup
výsadku	výsadka	k1gFnSc4	výsadka
zadkem	zadek	k1gInSc7	zadek
tanku	tank	k1gInSc2	tank
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
Izrael	Izrael	k1gInSc1	Izrael
zavádí	zavádět	k5eAaImIp3nS	zavádět
do	do	k7c2	do
výzbroje	výzbroj	k1gFnSc2	výzbroj
těžké	těžký	k2eAgFnSc2d1	těžká
OT	ot	k1gMnSc1	ot
Namer	Namer	k1gInSc1	Namer
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
tanků	tank	k1gInPc2	tank
Merkava	Merkava	k1gFnSc1	Merkava
(	(	kIx(	(
<g/>
díky	díky	k7c3	díky
umístění	umístění	k1gNnSc3	umístění
motoru	motor	k1gInSc2	motor
vepředu	vepředu	k6eAd1	vepředu
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgFnPc4	tento
přestavby	přestavba	k1gFnPc4	přestavba
jednodušší	jednoduchý	k2eAgFnPc4d2	jednodušší
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nějaké	nějaký	k3yIgFnPc4	nějaký
konstrukce	konstrukce	k1gFnPc4	konstrukce
těžkých	těžký	k2eAgInPc2d1	těžký
OT	ot	k1gMnSc1	ot
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
i	i	k9	i
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
u	u	k7c2	u
těch	ten	k3xDgMnPc2	ten
ale	ale	k9	ale
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
výměně	výměna	k1gFnSc3	výměna
motoru	motor	k1gInSc2	motor
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
pěší	pěší	k2eAgInSc1d1	pěší
výsadek	výsadek	k1gInSc1	výsadek
je	být	k5eAaImIp3nS	být
odkázán	odkázat	k5eAaPmNgInS	odkázat
na	na	k7c4	na
poklopy	poklop	k1gInPc4	poklop
ve	v	k7c6	v
stropě-	stropě-	k?	stropě-
tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
boji	boj	k1gInSc6	boj
velmi	velmi	k6eAd1	velmi
zranitelný	zranitelný	k2eAgInSc1d1	zranitelný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Jiný	jiný	k2eAgInSc1d1	jiný
bojový	bojový	k2eAgInSc1d1	bojový
prostředek	prostředek	k1gInSc1	prostředek
===	===	k?	===
</s>
</p>
<p>
<s>
Místo	místo	k7c2	místo
tankové	tankový	k2eAgFnSc2d1	tanková
střelecké	střelecký	k2eAgFnSc2d1	střelecká
věže	věž	k1gFnSc2	věž
se	se	k3xPyFc4	se
na	na	k7c4	na
podvozek	podvozek	k1gInSc4	podvozek
osadí	osadit	k5eAaPmIp3nS	osadit
věž	věž	k1gFnSc1	věž
s	s	k7c7	s
jinou	jiná	k1gFnSc7	jiná
výzbrojí-	výzbrojí-	k?	výzbrojí-
např.	např.	kA	např.
s	s	k7c7	s
houfnicí	houfnice	k1gFnSc7	houfnice
(	(	kIx(	(
<g/>
Samohybná	samohybný	k2eAgFnSc1d1	samohybná
kanónová	kanónový	k2eAgFnSc1d1	kanónová
houfnice	houfnice	k1gFnSc1	houfnice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
protiletadlovými	protiletadlový	k2eAgInPc7d1	protiletadlový
kanóny	kanón	k1gInPc7	kanón
(	(	kIx(	(
<g/>
Samohybný	samohybný	k2eAgInSc1d1	samohybný
protiletadlový	protiletadlový	k2eAgInSc1d1	protiletadlový
systém	systém	k1gInSc1	systém
<g/>
)	)	kIx)	)
apod.	apod.	kA	apod.
Výhodou	výhoda	k1gFnSc7	výhoda
oproti	oproti	k7c3	oproti
konstrukci	konstrukce	k1gFnSc3	konstrukce
speciálního	speciální	k2eAgInSc2d1	speciální
podvozku	podvozek	k1gInSc2	podvozek
je	být	k5eAaImIp3nS	být
snížení	snížení	k1gNnSc1	snížení
vývojových	vývojový	k2eAgInPc2d1	vývojový
a	a	k8xC	a
výrobních	výrobní	k2eAgInPc2d1	výrobní
nákladů	náklad	k1gInPc2	náklad
<g/>
.	.	kIx.	.
</s>
<s>
Nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgFnSc1d2	veliký
hmotnost	hmotnost	k1gFnSc1	hmotnost
podvozku	podvozek	k1gInSc2	podvozek
(	(	kIx(	(
<g/>
díky	díky	k7c3	díky
silnějšímu	silný	k2eAgNnSc3d2	silnější
pancéřování	pancéřování	k1gNnSc3	pancéřování
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
u	u	k7c2	u
spec.	spec.	k?	spec.
podvozků	podvozek	k1gInPc2	podvozek
nepoužívá	používat	k5eNaImIp3nS	používat
<g/>
)	)	kIx)	)
a	a	k8xC	a
většinou	většinou	k6eAd1	většinou
také	také	k9	také
větší	veliký	k2eAgFnSc1d2	veliký
spotřeba	spotřeba	k1gFnSc1	spotřeba
pohonných	pohonný	k2eAgFnPc2d1	pohonná
hmot	hmota	k1gFnPc2	hmota
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hasicí	hasicí	k2eAgInSc4d1	hasicí
tank	tank	k1gInSc4	tank
===	===	k?	===
</s>
</p>
<p>
<s>
Hasicí	hasicí	k2eAgInSc4d1	hasicí
tank	tank	k1gInSc4	tank
představuje	představovat	k5eAaImIp3nS	představovat
využití	využití	k1gNnSc1	využití
podvozků	podvozek	k1gInPc2	podvozek
vyřazovaných	vyřazovaný	k2eAgInPc2d1	vyřazovaný
tanků	tank	k1gInPc2	tank
pro	pro	k7c4	pro
civilní	civilní	k2eAgNnSc4d1	civilní
použití	použití	k1gNnSc4	použití
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
určen	určit	k5eAaPmNgInS	určit
k	k	k7c3	k
likvidaci	likvidace	k1gFnSc3	likvidace
všech	všecek	k3xTgInPc2	všecek
druhů	druh	k1gInPc2	druh
požárů	požár	k1gInPc2	požár
ve	v	k7c6	v
složitých	složitý	k2eAgFnPc6d1	složitá
terénních	terénní	k2eAgFnPc6d1	terénní
podmínkách	podmínka	k1gFnPc6	podmínka
a	a	k8xC	a
průmyslových	průmyslový	k2eAgFnPc6d1	průmyslová
aglomeracích	aglomerace	k1gFnPc6	aglomerace
(	(	kIx(	(
<g/>
např.	např.	kA	např.
lesy	les	k1gInPc1	les
<g/>
,	,	kIx,	,
elektrárny	elektrárna	k1gFnPc1	elektrárna
<g/>
,	,	kIx,	,
ropné	ropný	k2eAgFnPc1d1	ropná
rafinérie	rafinérie	k1gFnPc1	rafinérie
<g/>
,	,	kIx,	,
plynovody	plynovod	k1gInPc1	plynovod
<g/>
,	,	kIx,	,
povrchové	povrchový	k2eAgInPc1d1	povrchový
doly	dol	k1gInPc1	dol
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
účelu	účel	k1gInSc3	účel
je	být	k5eAaImIp3nS	být
místo	místo	k1gNnSc4	místo
střelecké	střelecký	k2eAgFnSc2d1	střelecká
věže	věž	k1gFnSc2	věž
osazena	osazen	k2eAgFnSc1d1	osazena
nová	nový	k2eAgFnSc1d1	nová
velká	velký	k2eAgFnSc1d1	velká
věž	věž	k1gFnSc1	věž
s	s	k7c7	s
nádržemi	nádrž	k1gFnPc7	nádrž
na	na	k7c4	na
vodu	voda	k1gFnSc4	voda
a	a	k8xC	a
pěnidlo	pěnidlo	k1gNnSc1	pěnidlo
<g/>
,	,	kIx,	,
osazená	osazený	k2eAgFnSc1d1	osazená
vysokotlakými	vysokotlaký	k2eAgFnPc7d1	vysokotlaká
proudnicemi	proudnice	k1gFnPc7	proudnice
ovládanými	ovládaný	k2eAgFnPc7d1	ovládaná
z	z	k7c2	z
vozidla	vozidlo	k1gNnSc2	vozidlo
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
vysokým	vysoký	k2eAgFnPc3d1	vysoká
teplotám	teplota	k1gFnPc3	teplota
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
použití	použití	k1gNnSc2	použití
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
chráněno	chránit	k5eAaImNgNnS	chránit
vodní	vodní	k2eAgFnSc7d1	vodní
mlhou	mlha	k1gFnSc7	mlha
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
vodou	voda	k1gFnSc7	voda
hnanou	hnaný	k2eAgFnSc4d1	hnaná
pod	pod	k7c7	pod
vysokým	vysoký	k2eAgInSc7d1	vysoký
tlakem	tlak	k1gInSc7	tlak
tryskami	tryska	k1gFnPc7	tryska
umístěnými	umístěný	k2eAgInPc7d1	umístěný
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
vozidla	vozidlo	k1gNnSc2	vozidlo
<g/>
.	.	kIx.	.
</s>
<s>
Hašení	hašení	k1gNnSc1	hašení
požáru	požár	k1gInSc2	požár
lze	lze	k6eAd1	lze
provádět	provádět	k5eAaImF	provádět
také	také	k9	také
ručními	ruční	k2eAgFnPc7d1	ruční
proudnicemi	proudnice	k1gFnPc7	proudnice
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
připojit	připojit	k5eAaPmF	připojit
k	k	k7c3	k
vozidlu	vozidlo	k1gNnSc3	vozidlo
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
hašení	hašení	k1gNnSc6	hašení
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
průběžné	průběžný	k2eAgNnSc1d1	průběžné
doplňovat	doplňovat	k5eAaImF	doplňovat
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
vybaven	vybavit	k5eAaPmNgMnS	vybavit
měřením	měření	k1gNnSc7	měření
teploty	teplota	k1gFnSc2	teplota
povrchu	povrch	k1gInSc2	povrch
vozidla	vozidlo	k1gNnSc2	vozidlo
<g/>
,	,	kIx,	,
kamerovým	kamerový	k2eAgInSc7d1	kamerový
systémem	systém	k1gInSc7	systém
<g/>
,	,	kIx,	,
radiostanicí	radiostanice	k1gFnSc7	radiostanice
a	a	k8xC	a
dálkovým	dálkový	k2eAgNnSc7d1	dálkové
ovládáním	ovládání	k1gNnSc7	ovládání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
V	v	k7c6	v
přísloví	přísloví	k1gNnSc6	přísloví
==	==	k?	==
</s>
</p>
<p>
<s>
Tank	tank	k1gInSc1	tank
je	být	k5eAaImIp3nS	být
i	i	k9	i
v	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
přísloví	přísloví	k1gNnSc6	přísloví
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Bordel	bordel	k1gInSc1	bordel
jako	jako	k8xS	jako
v	v	k7c6	v
tanku	tank	k1gInSc6	tank
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
zajímavý	zajímavý	k2eAgInSc1d1	zajímavý
protimluv	protimluv	k1gInSc1	protimluv
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
tanku	tank	k1gInSc6	tank
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
pořádek	pořádek	k1gInSc4	pořádek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
používá	používat	k5eAaImIp3nS	používat
"	"	kIx"	"
<g/>
Bordel	bordel	k1gInSc4	bordel
jako	jako	k8xS	jako
v	v	k7c6	v
převráceným	převrácený	k2eAgFnPc3d1	převrácená
tanku	tank	k1gInSc2	tank
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
teorií	teorie	k1gFnPc2	teorie
praví	pravit	k5eAaBmIp3nS	pravit
<g/>
,	,	kIx,	,
že	že	k8xS	že
toto	tento	k3xDgNnSc1	tento
přísloví	přísloví	k1gNnSc1	přísloví
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
díky	díky	k7c3	díky
prázdným	prázdný	k2eAgFnPc3d1	prázdná
nábojnicím	nábojnice	k1gFnPc3	nábojnice
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
výstřelu	výstřel	k1gInSc6	výstřel
volně	volně	k6eAd1	volně
pohybovaly	pohybovat	k5eAaImAgFnP	pohybovat
po	po	k7c6	po
podlaze	podlaha	k1gFnSc6	podlaha
korby	korba	k1gFnSc2	korba
ve	v	k7c6	v
vnitřním	vnitřní	k2eAgInSc6d1	vnitřní
prostoru	prostor	k1gInSc6	prostor
tanku	tank	k1gInSc2	tank
a	a	k8xC	a
představovaly	představovat	k5eAaImAgFnP	představovat
poměrné	poměrný	k2eAgNnSc4d1	poměrné
velké	velký	k2eAgNnSc4d1	velké
bezpečnostní	bezpečnostní	k2eAgNnSc4d1	bezpečnostní
riziko	riziko	k1gNnSc4	riziko
pro	pro	k7c4	pro
posádku	posádka	k1gFnSc4	posádka
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
v	v	k7c6	v
případě	případ	k1gInSc6	případ
obdrženého	obdržený	k2eAgInSc2d1	obdržený
zásahu	zásah	k1gInSc2	zásah
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
prázdné	prázdný	k2eAgFnPc1d1	prázdná
nábojnice	nábojnice	k1gFnPc1	nábojnice
mohly	moct	k5eAaImAgFnP	moct
rozletět	rozletět	k5eAaPmF	rozletět
na	na	k7c4	na
všechny	všechen	k3xTgFnPc4	všechen
strany	strana	k1gFnPc4	strana
a	a	k8xC	a
ve	v	k7c6	v
stísněném	stísněný	k2eAgInSc6d1	stísněný
prostoru	prostor	k1gInSc6	prostor
uvnitř	uvnitř	k7c2	uvnitř
tanku	tank	k1gInSc2	tank
vážně	vážně	k6eAd1	vážně
zranit	zranit	k5eAaPmF	zranit
či	či	k8xC	či
usmrtit	usmrtit	k5eAaPmF	usmrtit
posádku	posádka	k1gFnSc4	posádka
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
situace	situace	k1gFnSc1	situace
nastávala	nastávat	k5eAaImAgFnS	nastávat
především	především	k9	především
v	v	k7c6	v
ruských	ruský	k2eAgInPc6d1	ruský
tancích	tanec	k1gInPc6	tanec
během	během	k7c2	během
náročných	náročný	k2eAgInPc2d1	náročný
bojů	boj	k1gInPc2	boj
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgInPc6	který
nebyl	být	k5eNaImAgInS	být
vyřešen	vyřešen	k2eAgInSc1d1	vyřešen
postup	postup	k1gInSc1	postup
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
nakládat	nakládat	k5eAaImF	nakládat
s	s	k7c7	s
prázdnými	prázdný	k2eAgFnPc7d1	prázdná
nábojnicemi	nábojnice	k1gFnPc7	nábojnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Ota	Ota	k1gMnSc1	Ota
Holub	Holub	k1gMnSc1	Holub
<g/>
,	,	kIx,	,
Československé	československý	k2eAgInPc1d1	československý
tanky	tank	k1gInPc1	tank
a	a	k8xC	a
tankisté	tankista	k1gMnPc1	tankista
<g/>
,	,	kIx,	,
Naše	náš	k3xOp1gNnSc1	náš
vojsko	vojsko	k1gNnSc1	vojsko
<g/>
,	,	kIx,	,
1980	[number]	k4	1980
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
tanku	tank	k1gInSc2	tank
</s>
</p>
<p>
<s>
Tanky	tank	k1gInPc4	tank
během	během	k7c2	během
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
</s>
</p>
<p>
<s>
Tanky	tank	k1gInPc4	tank
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
</s>
</p>
<p>
<s>
Stíhač	stíhač	k1gMnSc1	stíhač
tanků	tank	k1gInPc2	tank
</s>
</p>
<p>
<s>
Tančík	tančík	k1gInSc1	tančík
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
tank	tank	k1gInSc4	tank
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
tank	tank	k1gInSc4	tank
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
tank	tank	k1gInSc1	tank
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Weapons	Weapons	k6eAd1	Weapons
of	of	k?	of
War	War	k1gFnSc1	War
<g/>
:	:	kIx,	:
Tanks	Tanks	k1gInSc1	Tanks
</s>
</p>
<p>
<s>
Tanky	tank	k1gInPc1	tank
na	na	k7c6	na
serveru	server	k1gInSc6	server
military	militara	k1gFnSc2	militara
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Tanky	tank	k1gInPc1	tank
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tanky	tank	k1gInPc1	tank
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sovětské	sovětský	k2eAgInPc1d1	sovětský
tanky	tank	k1gInPc1	tank
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g/>
rus	rus	k1gMnSc1	rus
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Obrněná	obrněný	k2eAgFnSc1d1	obrněná
technika	technika	k1gFnSc1	technika
Československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
</s>
</p>
<p>
<s>
Moderní	moderní	k2eAgInPc1d1	moderní
tanky	tank	k1gInPc1	tank
</s>
</p>
<p>
<s>
Článek	článek	k1gInSc1	článek
o	o	k7c6	o
historii	historie	k1gFnSc6	historie
tanků	tank	k1gInPc2	tank
</s>
</p>
<p>
<s>
Moderní	moderní	k2eAgFnSc1d1	moderní
vojenská	vojenský	k2eAgFnSc1d1	vojenská
technika	technika	k1gFnSc1	technika
<g/>
:	:	kIx,	:
Neviditelný	viditelný	k2eNgInSc1d1	neviditelný
tank	tank	k1gInSc1	tank
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
časopis	časopis	k1gInSc1	časopis
ABC	ABC	kA	ABC
<g/>
,	,	kIx,	,
18	[number]	k4	18
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2011	[number]	k4	2011
</s>
</p>
