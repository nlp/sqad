<s>
Tank	tank	k1gInSc1	tank
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
obrněné	obrněný	k2eAgNnSc1d1	obrněné
bojové	bojový	k2eAgNnSc1d1	bojové
vozidlo	vozidlo	k1gNnSc1	vozidlo
s	s	k7c7	s
pásy	pás	k1gInPc7	pás
<g/>
,	,	kIx,	,
věží	věž	k1gFnSc7	věž
a	a	k8xC	a
kanónem	kanón	k1gInSc7	kanón
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
primárním	primární	k2eAgInSc7d1	primární
účelem	účel	k1gInSc7	účel
je	být	k5eAaImIp3nS	být
likvidace	likvidace	k1gFnSc1	likvidace
nepřátelských	přátelský	k2eNgFnPc2d1	nepřátelská
pozemních	pozemní	k2eAgFnPc2d1	pozemní
sil	síla	k1gFnPc2	síla
přímou	přímý	k2eAgFnSc7d1	přímá
palbou	palba	k1gFnSc7	palba
<g/>
.	.	kIx.	.
</s>
