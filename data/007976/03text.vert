<s>
Pablo	Pablo	k1gNnSc1	Pablo
Ruiz	Ruiza	k1gFnPc2	Ruiza
Picasso	Picassa	k1gFnSc5	Picassa
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1881	[number]	k4	1881
–	–	k?	–
8	[number]	k4	8
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
španělský	španělský	k2eAgMnSc1d1	španělský
malíř	malíř	k1gMnSc1	malíř
a	a	k8xC	a
sochař	sochař	k1gMnSc1	sochař
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
celé	celý	k2eAgNnSc1d1	celé
jméno	jméno	k1gNnSc1	jméno
je	být	k5eAaImIp3nS	být
Pablo	Pablo	k1gNnSc4	Pablo
Diego	Diego	k6eAd1	Diego
José	Josý	k2eAgNnSc4d1	José
Francisco	Francisco	k1gNnSc4	Francisco
de	de	k?	de
Paula	Paula	k1gFnSc1	Paula
Juan	Juan	k1gMnSc1	Juan
Nepomuceno	Nepomucen	k2eAgNnSc4d1	Nepomuceno
María	Maríum	k1gNnSc2	Maríum
de	de	k?	de
los	los	k1gMnSc1	los
Remedios	Remedios	k1gMnSc1	Remedios
Cipriano	Cipriana	k1gFnSc5	Cipriana
de	de	k?	de
la	la	k1gNnSc2	la
Santísima	Santísima	k1gNnSc1	Santísima
Trinidad	Trinidad	k1gInSc1	Trinidad
Ruiz	Ruiz	k1gMnSc1	Ruiz
y	y	k?	y
Picasso	Picassa	k1gFnSc5	Picassa
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
osobností	osobnost	k1gFnPc2	osobnost
umění	umění	k1gNnSc1	umění
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
Georgesem	Georges	k1gMnSc7	Georges
Braquem	Braqu	k1gMnSc7	Braqu
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
zakladatele	zakladatel	k1gMnSc4	zakladatel
kubismu	kubismus	k1gInSc2	kubismus
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
Picasso	Picassa	k1gFnSc5	Picassa
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
asi	asi	k9	asi
13	[number]	k4	13
500	[number]	k4	500
obrazů	obraz	k1gInPc2	obraz
a	a	k8xC	a
skic	skica	k1gFnPc2	skica
<g/>
,	,	kIx,	,
100	[number]	k4	100
000	[number]	k4	000
rytin	rytina	k1gFnPc2	rytina
a	a	k8xC	a
tisků	tisk	k1gInPc2	tisk
<g/>
,	,	kIx,	,
34	[number]	k4	34
000	[number]	k4	000
ilustrací	ilustrace	k1gFnPc2	ilustrace
a	a	k8xC	a
300	[number]	k4	300
skulptur	skulptura	k1gFnPc2	skulptura
a	a	k8xC	a
keramických	keramický	k2eAgNnPc2d1	keramické
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
obraz	obraz	k1gInSc1	obraz
Alžírské	alžírský	k2eAgFnSc2d1	alžírská
ženy	žena	k1gFnSc2	žena
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2015	[number]	k4	2015
v	v	k7c6	v
newyorské	newyorský	k2eAgFnSc6d1	newyorská
aukční	aukční	k2eAgFnSc6d1	aukční
síni	síň	k1gFnSc6	síň
Christie	Christie	k1gFnSc2	Christie
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
vydražen	vydražit	k5eAaPmNgInS	vydražit
za	za	k7c4	za
179,4	[number]	k4	179,4
milionu	milion	k4xCgInSc2	milion
dolarů	dolar	k1gInPc2	dolar
(	(	kIx(	(
<g/>
v	v	k7c6	v
přepočtu	přepočet	k1gInSc6	přepočet
4,4	[number]	k4	4,4
miliardy	miliarda	k4xCgFnSc2	miliarda
korun	koruna	k1gFnPc2	koruna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
historicky	historicky	k6eAd1	historicky
nejdražším	drahý	k2eAgInSc7d3	nejdražší
výtvarným	výtvarný	k2eAgInSc7d1	výtvarný
dílem	díl	k1gInSc7	díl
prodaným	prodaný	k2eAgInSc7d1	prodaný
v	v	k7c6	v
aukci	aukce	k1gFnSc6	aukce
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Málaze	Málaha	k1gFnSc6	Málaha
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
dítětem	dítě	k1gNnSc7	dítě
José	Josý	k2eAgFnSc2d1	Josý
Ruize	Ruize	k1gFnSc2	Ruize
y	y	k?	y
Blasco	Blasco	k1gMnSc1	Blasco
a	a	k8xC	a
Maríe	Maríe	k1gFnSc1	Maríe
Picasso	Picassa	k1gFnSc5	Picassa
y	y	k?	y
López	Lópeza	k1gFnPc2	Lópeza
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
pokřtěn	pokřtít	k5eAaPmNgInS	pokřtít
jako	jako	k8xS	jako
Pablo	Pablo	k1gNnSc1	Pablo
<g/>
,	,	kIx,	,
Diego	Diego	k1gNnSc1	Diego
<g/>
,	,	kIx,	,
José	José	k1gNnSc1	José
<g/>
,	,	kIx,	,
Francisco	Francisco	k1gNnSc1	Francisco
de	de	k?	de
Paula	Paula	k1gFnSc1	Paula
<g/>
,	,	kIx,	,
Juan	Juan	k1gMnSc1	Juan
Nepomuceno	Nepomucen	k2eAgNnSc1d1	Nepomuceno
<g/>
,	,	kIx,	,
Maria	Maria	k1gFnSc1	Maria
de	de	k?	de
los	los	k1gInSc1	los
Remedios	Remedios	k1gInSc1	Remedios
<g/>
,	,	kIx,	,
a	a	k8xC	a
Cipriano	Cipriana	k1gFnSc5	Cipriana
de	de	k?	de
la	la	k1gNnSc2	la
Santísima	Santísima	k1gNnSc1	Santísima
Trinidad	Trinidad	k1gInSc1	Trinidad
<g/>
.	.	kIx.	.
</s>
<s>
Picassův	Picassův	k2eAgMnSc1d1	Picassův
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
José	José	k1gNnSc1	José
Ruiz	Ruiza	k1gFnPc2	Ruiza
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
malířem	malíř	k1gMnSc7	malíř
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
specialitou	specialita	k1gFnSc7	specialita
bylo	být	k5eAaImAgNnS	být
realistické	realistický	k2eAgNnSc1d1	realistické
zobrazení	zobrazení	k1gNnSc1	zobrazení
ptáků	pták	k1gMnPc2	pták
a	a	k8xC	a
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
většinu	většina	k1gFnSc4	většina
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
profesorem	profesor	k1gMnSc7	profesor
umění	umění	k1gNnSc2	umění
na	na	k7c6	na
škole	škola	k1gFnSc6	škola
řemesel	řemeslo	k1gNnPc2	řemeslo
a	a	k8xC	a
kurátorem	kurátor	k1gMnSc7	kurátor
v	v	k7c6	v
muzeu	muzeum	k1gNnSc6	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Picasso	Picassa	k1gFnSc5	Picassa
už	už	k9	už
jako	jako	k8xC	jako
chlapec	chlapec	k1gMnSc1	chlapec
prokazoval	prokazovat	k5eAaImAgMnS	prokazovat
vášeň	vášeň	k1gFnSc4	vášeň
i	i	k9	i
cit	cit	k1gInSc4	cit
pro	pro	k7c4	pro
kresbu	kresba	k1gFnSc4	kresba
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jeho	jeho	k3xOp3gFnSc2	jeho
matky	matka	k1gFnSc2	matka
bylo	být	k5eAaImAgNnS	být
jeho	jeho	k3xOp3gNnSc1	jeho
první	první	k4xOgNnSc1	první
slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
piz	piz	k?	piz
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
zkrácenina	zkrácenina	k1gFnSc1	zkrácenina
od	od	k7c2	od
lápiz	lápiza	k1gFnPc2	lápiza
<g/>
,	,	kIx,	,
španělsky	španělsky	k6eAd1	španělsky
tužka	tužka	k1gFnSc1	tužka
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
Picassa	Picass	k1gMnSc4	Picass
různým	různý	k2eAgFnPc3d1	různá
technikám	technika	k1gFnPc3	technika
<g/>
,	,	kIx,	,
např.	např.	kA	např.
kresbě	kresba	k1gFnSc3	kresba
a	a	k8xC	a
olejové	olejový	k2eAgFnSc3d1	olejová
malbě	malba	k1gFnSc3	malba
<g/>
.	.	kIx.	.
</s>
<s>
Picasso	Picassa	k1gFnSc5	Picassa
studoval	studovat	k5eAaImAgMnS	studovat
i	i	k9	i
na	na	k7c4	na
Akademii	akademie	k1gFnSc4	akademie
umění	umění	k1gNnSc2	umění
(	(	kIx(	(
<g/>
Academia	academia	k1gFnSc1	academia
de	de	k?	de
San	San	k1gFnSc1	San
Fernando	Fernanda	k1gFnSc5	Fernanda
<g/>
)	)	kIx)	)
v	v	k7c6	v
Madridu	Madrid	k1gInSc6	Madrid
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
studia	studio	k1gNnPc4	studio
nedokončil	dokončit	k5eNaPmAgMnS	dokončit
a	a	k8xC	a
po	po	k7c6	po
roce	rok	k1gInSc6	rok
ji	on	k3xPp3gFnSc4	on
opustil	opustit	k5eAaPmAgMnS	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
studiích	studio	k1gNnPc6	studio
v	v	k7c6	v
Madridu	Madrid	k1gInSc6	Madrid
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgMnS	vydat
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
centra	centrum	k1gNnSc2	centrum
umění	umění	k1gNnSc2	umění
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
bydlel	bydlet	k5eAaImAgMnS	bydlet
s	s	k7c7	s
Maxem	Max	k1gMnSc7	Max
Jacobem	Jacob	k1gMnSc7	Jacob
(	(	kIx(	(
<g/>
novinář	novinář	k1gMnSc1	novinář
a	a	k8xC	a
básník	básník	k1gMnSc1	básník
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
mu	on	k3xPp3gMnSc3	on
pomohl	pomoct	k5eAaPmAgMnS	pomoct
naučit	naučit	k5eAaPmF	naučit
se	se	k3xPyFc4	se
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
.	.	kIx.	.
</s>
<s>
Picasso	Picassa	k1gFnSc5	Picassa
pracoval	pracovat	k5eAaImAgMnS	pracovat
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Max	Max	k1gMnSc1	Max
spal	spát	k5eAaImAgMnS	spát
<g/>
.	.	kIx.	.
</s>
<s>
Prošli	projít	k5eAaPmAgMnP	projít
spolu	spolu	k6eAd1	spolu
časy	čas	k1gInPc1	čas
chudoby	chudoba	k1gFnSc2	chudoba
a	a	k8xC	a
beznaděje	beznaděj	k1gFnSc2	beznaděj
<g/>
,	,	kIx,	,
hodně	hodně	k6eAd1	hodně
z	z	k7c2	z
Picassova	Picassův	k2eAgNnSc2d1	Picassovo
díla	dílo	k1gNnSc2	dílo
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
muselo	muset	k5eAaImAgNnS	muset
spálit	spálit	k5eAaPmF	spálit
kvůli	kvůli	k7c3	kvůli
teplu	teplo	k1gNnSc3	teplo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1901	[number]	k4	1901
založil	založit	k5eAaPmAgMnS	založit
Picasso	Picassa	k1gFnSc5	Picassa
v	v	k7c6	v
Madridu	Madrid	k1gInSc6	Madrid
společně	společně	k6eAd1	společně
se	s	k7c7	s
Solerem	Solero	k1gNnSc7	Solero
časopis	časopis	k1gInSc1	časopis
Arte	Arte	k1gInSc1	Arte
Joven	Joven	k2eAgInSc1d1	Joven
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
číslo	číslo	k1gNnSc1	číslo
celé	celá	k1gFnSc2	celá
ilustroval	ilustrovat	k5eAaBmAgMnS	ilustrovat
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
Picasso	Picassa	k1gFnSc5	Picassa
podepisuje	podepisovat	k5eAaImIp3nS	podepisovat
jen	jen	k9	jen
Picasso	Picassa	k1gFnSc5	Picassa
<g/>
,	,	kIx,	,
už	už	k9	už
ne	ne	k9	ne
Pablo	Pablo	k1gNnSc1	Pablo
Ruiz	Ruiza	k1gFnPc2	Ruiza
y	y	k?	y
Picasso	Picassa	k1gFnSc5	Picassa
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnPc4	první
léta	léto	k1gNnPc4	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
trávil	trávit	k5eAaImAgMnS	trávit
Picasso	Picassa	k1gFnSc5	Picassa
mezi	mezi	k7c7	mezi
Barcelonou	Barcelona	k1gFnSc7	Barcelona
a	a	k8xC	a
Paříží	Paříž	k1gFnSc7	Paříž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1904	[number]	k4	1904
začal	začít	k5eAaPmAgInS	začít
jeho	jeho	k3xOp3gInSc1	jeho
dlouholetý	dlouholetý	k2eAgInSc1d1	dlouholetý
vztah	vztah	k1gInSc1	vztah
s	s	k7c7	s
Fernande	Fernand	k1gInSc5	Fernand
Olivierovou	Olivierový	k2eAgFnSc4d1	Olivierový
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
ona	onen	k3xDgFnSc1	onen
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
obrazech	obraz	k1gInPc6	obraz
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
získal	získat	k5eAaPmAgMnS	získat
slávu	sláva	k1gFnSc4	sláva
a	a	k8xC	a
nějaký	nějaký	k3yIgInSc4	nějaký
ten	ten	k3xDgInSc4	ten
majetek	majetek	k1gInSc4	majetek
<g/>
,	,	kIx,	,
odešel	odejít	k5eAaPmAgMnS	odejít
Picasso	Picassa	k1gFnSc5	Picassa
od	od	k7c2	od
Olivierové	Olivierové	k2eAgMnSc2d1	Olivierové
k	k	k7c3	k
Marcelle	Marcelle	k1gNnSc3	Marcelle
Humbertové	Humbertová	k1gFnSc2	Humbertová
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
říkal	říkat	k5eAaImAgMnS	říkat
Eva	Eva	k1gFnSc1	Eva
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
lásku	láska	k1gFnSc4	láska
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
Picasso	Picassa	k1gFnSc5	Picassa
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
kubistických	kubistický	k2eAgInPc6d1	kubistický
obrazech	obraz	k1gInPc6	obraz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
poznal	poznat	k5eAaPmAgMnS	poznat
lidi	člověk	k1gMnPc4	člověk
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
André	André	k1gMnSc1	André
Breton	Breton	k1gMnSc1	Breton
<g/>
,	,	kIx,	,
Guillaume	Guillaum	k1gInSc5	Guillaum
Apollinaire	Apollinair	k1gInSc5	Apollinair
a	a	k8xC	a
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
Gertrude	Gertrud	k1gInSc5	Gertrud
Stein	Stein	k1gMnSc1	Stein
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
manželky	manželka	k1gFnSc2	manželka
měl	mít	k5eAaImAgInS	mít
i	i	k9	i
množství	množství	k1gNnSc4	množství
milenek	milenka	k1gFnPc2	milenka
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
dvakrát	dvakrát	k6eAd1	dvakrát
ženatý	ženatý	k2eAgMnSc1d1	ženatý
a	a	k8xC	a
měl	mít	k5eAaImAgMnS	mít
čtyři	čtyři	k4xCgFnPc4	čtyři
děti	dítě	k1gFnPc4	dítě
se	s	k7c7	s
třemi	tři	k4xCgFnPc7	tři
ženami	žena	k1gFnPc7	žena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
si	se	k3xPyFc3	se
Picasso	Picassa	k1gFnSc5	Picassa
vzal	vzít	k5eAaPmAgMnS	vzít
balerínu	balerína	k1gFnSc4	balerína
Olgu	Olga	k1gFnSc4	Olga
Chochlovou	Chochlův	k2eAgFnSc7d1	Chochlova
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
mu	on	k3xPp3gMnSc3	on
představila	představit	k5eAaPmAgFnS	představit
život	život	k1gInSc4	život
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
vrstvách	vrstva	k1gFnPc6	vrstva
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Měli	mít	k5eAaImAgMnP	mít
spolu	spolu	k6eAd1	spolu
syna	syn	k1gMnSc2	syn
Paula	Paul	k1gMnSc2	Paul
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yIgInSc2	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stal	stát	k5eAaPmAgMnS	stát
motocyklový	motocyklový	k2eAgMnSc1d1	motocyklový
závodník	závodník	k1gMnSc1	závodník
a	a	k8xC	a
také	také	k9	také
řidič	řidič	k1gInSc4	řidič
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
<g/>
.	.	kIx.	.
</s>
<s>
Picasso	Picassa	k1gFnSc5	Picassa
se	se	k3xPyFc4	se
s	s	k7c7	s
Chochlovou	Chochlův	k2eAgFnSc7d1	Chochlova
často	často	k6eAd1	často
hádal	hádat	k5eAaImAgMnS	hádat
<g/>
.	.	kIx.	.
</s>
<s>
Ona	onen	k3xDgFnSc1	onen
si	se	k3xPyFc3	se
trvala	trvat	k5eAaImAgFnS	trvat
na	na	k7c6	na
společenském	společenský	k2eAgNnSc6d1	společenské
chování	chování	k1gNnSc6	chování
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
on	on	k3xPp3gMnSc1	on
měl	mít	k5eAaImAgMnS	mít
spíše	spíše	k9	spíše
sklony	sklon	k1gInPc4	sklon
k	k	k7c3	k
chování	chování	k1gNnSc3	chování
bohémskému	bohémský	k2eAgNnSc3d1	bohémské
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1927	[number]	k4	1927
Picasso	Picassa	k1gFnSc5	Picassa
poznal	poznat	k5eAaPmAgMnS	poznat
sedmnáctiletou	sedmnáctiletý	k2eAgFnSc7d1	sedmnáctiletá
Marie-Thérè	Marie-Thérè	k1gFnSc7	Marie-Thérè
Walterovou	Walterův	k2eAgFnSc7d1	Walterova
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
si	se	k3xPyFc3	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
tajnou	tajný	k2eAgFnSc7d1	tajná
aféru	aféra	k1gFnSc4	aféra
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
manželství	manželství	k1gNnSc1	manželství
s	s	k7c7	s
Chochlovou	Chochlův	k2eAgFnSc7d1	Chochlova
brzy	brzy	k6eAd1	brzy
skončilo	skončit	k5eAaPmAgNnS	skončit
rozloučením	rozloučení	k1gNnSc7	rozloučení
<g/>
.	.	kIx.	.
</s>
<s>
Rozvod	rozvod	k1gInSc1	rozvod
Picasso	Picassa	k1gFnSc5	Picassa
nechtěl	chtít	k5eNaImAgMnS	chtít
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
podle	podle	k7c2	podle
francouzského	francouzský	k2eAgNnSc2d1	francouzské
práva	právo	k1gNnSc2	právo
by	by	kYmCp3nS	by
Chochlové	Chochol	k1gMnPc1	Chochol
připadla	připadnout	k5eAaPmAgFnS	připadnout
polovina	polovina	k1gFnSc1	polovina
jeho	on	k3xPp3gInSc2	on
majetku	majetek	k1gInSc2	majetek
<g/>
.	.	kIx.	.
</s>
<s>
Zůstali	zůstat	k5eAaPmAgMnP	zůstat
tedy	tedy	k8xC	tedy
svoji	svůj	k3xOyFgFnSc4	svůj
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Chochlová	Chochlová	k1gFnSc1	Chochlová
zemřela	zemřít	k5eAaPmAgFnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Picasso	Picassa	k1gFnSc5	Picassa
dlouho	dlouho	k6eAd1	dlouho
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
s	s	k7c7	s
Walterovou	Walterův	k2eAgFnSc7d1	Walterova
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
dceru	dcera	k1gFnSc4	dcera
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
Maia	Maia	k1gMnSc1	Maia
<g/>
.	.	kIx.	.
</s>
<s>
Marie-Thérè	Marie-Thérè	k?	Marie-Thérè
po	po	k7c4	po
zbytek	zbytek	k1gInSc4	zbytek
života	život	k1gInSc2	život
žila	žít	k5eAaImAgFnS	žít
v	v	k7c4	v
naději	naděje	k1gFnSc4	naděje
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
Picasso	Picassa	k1gFnSc5	Picassa
jednou	jeden	k4xCgFnSc7	jeden
vezme	vzít	k5eAaPmIp3nS	vzít
<g/>
,	,	kIx,	,
oběsila	oběsit	k5eAaPmAgFnS	oběsit
se	s	k7c7	s
4	[number]	k4	4
roky	rok	k1gInPc7	rok
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Dora	Dora	k1gFnSc1	Dora
Maar	Maar	k1gInSc1	Maar
<g/>
,	,	kIx,	,
fotografka	fotografka	k1gFnSc1	fotografka
a	a	k8xC	a
malířka	malířka	k1gFnSc1	malířka
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
také	také	k9	také
po	po	k7c4	po
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
Picassovou	Picassův	k2eAgFnSc7d1	Picassova
milenkou	milenka	k1gFnSc7	milenka
<g/>
.	.	kIx.	.
</s>
<s>
Nejblíž	blízce	k6eAd3	blízce
si	se	k3xPyFc3	se
byli	být	k5eAaImAgMnP	být
na	na	k7c6	na
konci	konec	k1gInSc6	konec
30	[number]	k4	30
a	a	k8xC	a
začátkem	začátkem	k7c2	začátkem
40	[number]	k4	40
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
a	a	k8xC	a
byla	být	k5eAaImAgNnP	být
to	ten	k3xDgNnSc4	ten
ona	onen	k3xDgFnSc1	onen
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
zdokumentoval	zdokumentovat	k5eAaPmAgInS	zdokumentovat
obraz	obraz	k1gInSc4	obraz
Guernica	Guernic	k1gInSc2	Guernic
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
osvobození	osvobození	k1gNnSc6	osvobození
Paříže	Paříž	k1gFnSc2	Paříž
roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
se	se	k3xPyFc4	se
Picasso	Picassa	k1gFnSc5	Picassa
dal	dát	k5eAaPmAgInS	dát
dohromady	dohromady	k6eAd1	dohromady
s	s	k7c7	s
mladou	mladý	k2eAgFnSc7d1	mladá
studentkou	studentka	k1gFnSc7	studentka
umění	umění	k1gNnSc2	umění
Françoise	Françoise	k1gFnSc2	Françoise
Gilotovou	Gilotův	k2eAgFnSc7d1	Gilotův
<g/>
.	.	kIx.	.
</s>
<s>
Měli	mít	k5eAaImAgMnP	mít
spolu	spolu	k6eAd1	spolu
dvě	dva	k4xCgFnPc4	dva
děti	dítě	k1gFnPc4	dítě
–	–	k?	–
Clauda	Claudo	k1gNnSc2	Claudo
a	a	k8xC	a
Palomu	Palom	k1gInSc2	Palom
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
jediná	jediný	k2eAgFnSc1d1	jediná
z	z	k7c2	z
Picassových	Picassův	k2eAgFnPc2d1	Picassova
žen	žena	k1gFnPc2	žena
Gilotová	Gilotový	k2eAgFnSc1d1	Gilotový
Picassa	Picassa	k1gFnSc1	Picassa
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
opustila	opustit	k5eAaPmAgFnS	opustit
<g/>
,	,	kIx,	,
údajně	údajně	k6eAd1	údajně
kvůli	kvůli	k7c3	kvůli
hrubému	hrubý	k2eAgNnSc3d1	hrubé
zacházení	zacházení	k1gNnSc3	zacházení
a	a	k8xC	a
nevěře	nevěra	k1gFnSc3	nevěra
<g/>
.	.	kIx.	.
</s>
<s>
Picasso	Picassa	k1gFnSc5	Picassa
tím	ten	k3xDgNnSc7	ten
byl	být	k5eAaImAgMnS	být
zdrcen	zdrtit	k5eAaPmNgMnS	zdrtit
<g/>
.	.	kIx.	.
</s>
<s>
Prošel	projít	k5eAaPmAgInS	projít
těžkým	těžký	k2eAgNnSc7d1	těžké
obdobím	období	k1gNnSc7	období
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
uvědomil	uvědomit	k5eAaPmAgMnS	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
70	[number]	k4	70
letech	léto	k1gNnPc6	léto
už	už	k6eAd1	už
není	být	k5eNaImIp3nS	být
pro	pro	k7c4	pro
mladé	mladý	k2eAgFnPc4d1	mladá
ženy	žena	k1gFnPc4	žena
tak	tak	k9	tak
atraktivní	atraktivní	k2eAgMnSc1d1	atraktivní
a	a	k8xC	a
vypadá	vypadat	k5eAaPmIp3nS	vypadat
vedle	vedle	k7c2	vedle
nich	on	k3xPp3gMnPc2	on
spíš	spíš	k9	spíš
směšně	směšně	k6eAd1	směšně
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
kreseb	kresba	k1gFnPc2	kresba
z	z	k7c2	z
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
to	ten	k3xDgNnSc4	ten
jasně	jasně	k6eAd1	jasně
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
–	–	k?	–
znázorňují	znázorňovat	k5eAaImIp3nP	znázorňovat
ošklivého	ošklivý	k2eAgMnSc4d1	ošklivý
trpaslíka	trpaslík	k1gMnSc4	trpaslík
a	a	k8xC	a
krásnou	krásný	k2eAgFnSc4d1	krásná
mladou	mladý	k2eAgFnSc4d1	mladá
ženu	žena	k1gFnSc4	žena
<g/>
.	.	kIx.	.
</s>
<s>
Geneviè	Geneviè	k?	Geneviè
Laporte	Laport	k1gInSc5	Laport
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
s	s	k7c7	s
Picassem	Picass	k1gInSc7	Picass
zažila	zažít	k5eAaPmAgFnS	zažít
krátký	krátký	k2eAgInSc4d1	krátký
milostný	milostný	k2eAgInSc4d1	milostný
románek	románek	k1gInSc4	románek
<g/>
,	,	kIx,	,
vydražila	vydražit	k5eAaPmAgFnS	vydražit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
právě	právě	k6eAd1	právě
takové	takový	k3xDgFnPc1	takový
Picassovy	Picassův	k2eAgFnPc1d1	Picassova
kresby	kresba	k1gFnPc1	kresba
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgInPc6	jenž
je	být	k5eAaImIp3nS	být
i	i	k9	i
ona	onen	k3xDgFnSc1	onen
sama	sám	k3xTgFnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
Picassovou	Picassův	k2eAgFnSc7d1	Picassova
láskou	láska	k1gFnSc7	láska
byla	být	k5eAaImAgFnS	být
Jacqueline	Jacquelin	k1gInSc5	Jacquelin
Roque	Roque	k1gFnSc1	Roque
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
pracovala	pracovat	k5eAaImAgFnS	pracovat
v	v	k7c6	v
hrnčířské	hrnčířský	k2eAgFnSc6d1	Hrnčířská
dílně	dílna	k1gFnSc6	dílna
Madoura	Madour	k1gMnSc2	Madour
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Picasso	Picassa	k1gFnSc5	Picassa
vytvářel	vytvářet	k5eAaImAgMnS	vytvářet
a	a	k8xC	a
maloval	malovat	k5eAaImAgMnS	malovat
keramiku	keramika	k1gFnSc4	keramika
<g/>
.	.	kIx.	.
</s>
<s>
Vzal	vzít	k5eAaPmAgMnS	vzít
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
roku	rok	k1gInSc2	rok
1961	[number]	k4	1961
a	a	k8xC	a
byli	být	k5eAaImAgMnP	být
spolu	spolu	k6eAd1	spolu
až	až	k6eAd1	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
Picassova	Picassův	k2eAgInSc2d1	Picassův
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
sňatek	sňatek	k1gInSc1	sňatek
také	také	k9	také
představoval	představovat	k5eAaImAgInS	představovat
část	část	k1gFnSc4	část
pomsty	pomsta	k1gFnSc2	pomsta
Gilotové	Gilotový	k2eAgFnSc2d1	Gilotový
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
snažila	snažit	k5eAaImAgFnS	snažit
najít	najít	k5eAaPmF	najít
způsob	způsob	k1gInSc4	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
by	by	kYmCp3nS	by
legitimizovala	legitimizovat	k5eAaBmAgFnS	legitimizovat
své	svůj	k3xOyFgFnPc4	svůj
dvě	dva	k4xCgFnPc4	dva
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
měla	mít	k5eAaImAgFnS	mít
s	s	k7c7	s
Picassem	Picass	k1gMnSc7	Picass
<g/>
.	.	kIx.	.
</s>
<s>
Picasso	Picassa	k1gFnSc5	Picassa
ji	on	k3xPp3gFnSc4	on
podporoval	podporovat	k5eAaImAgInS	podporovat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
rozvedla	rozvést	k5eAaPmAgFnS	rozvést
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
manželem	manžel	k1gMnSc7	manžel
<g/>
,	,	kIx,	,
a	a	k8xC	a
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
jí	on	k3xPp3gFnSc3	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
poté	poté	k6eAd1	poté
vezme	vzít	k5eAaPmIp3nS	vzít
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
si	se	k3xPyFc3	se
ale	ale	k8xC	ale
vzal	vzít	k5eAaPmAgMnS	vzít
Roqueovou	Roqueův	k2eAgFnSc7d1	Roqueův
<g/>
.	.	kIx.	.
</s>
<s>
Picasso	Picassa	k1gFnSc5	Picassa
byl	být	k5eAaImAgInS	být
známá	známý	k2eAgFnSc1d1	známá
osobnost	osobnost	k1gFnSc1	osobnost
a	a	k8xC	a
lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
zajímali	zajímat	k5eAaImAgMnP	zajímat
jak	jak	k8xC	jak
o	o	k7c4	o
jeho	jeho	k3xOp3gNnSc4	jeho
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
o	o	k7c4	o
jeho	on	k3xPp3gInSc4	on
osobní	osobní	k2eAgInSc4d1	osobní
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
všestranným	všestranný	k2eAgMnSc7d1	všestranný
umělcem	umělec	k1gMnSc7	umělec
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
účinkoval	účinkovat	k5eAaImAgMnS	účinkovat
i	i	k9	i
ve	v	k7c6	v
filmu	film	k1gInSc6	film
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Jeana	Jean	k1gMnSc2	Jean
Cocteaua	Cocteauus	k1gMnSc2	Cocteauus
Orfeova	Orfeův	k2eAgFnSc1d1	Orfeova
závěť	závěť	k1gFnSc1	závěť
<g/>
.	.	kIx.	.
</s>
<s>
Pokaždé	pokaždé	k6eAd1	pokaždé
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
Picasso	Picassa	k1gFnSc5	Picassa
objevil	objevit	k5eAaPmAgInS	objevit
ve	v	k7c6	v
filmu	film	k1gInSc6	film
<g/>
,	,	kIx,	,
hrál	hrát	k5eAaImAgMnS	hrát
sám	sám	k3xTgMnSc1	sám
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgInS	podílet
na	na	k7c6	na
filmu	film	k1gInSc6	film
Le	Le	k1gFnSc2	Le
Mystè	Mystè	k1gFnSc2	Mystè
Picasso	Picassa	k1gFnSc5	Picassa
(	(	kIx(	(
<g/>
Picassovo	Picassův	k2eAgNnSc4d1	Picassovo
tajemství	tajemství	k1gNnSc4	tajemství
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
režíroval	režírovat	k5eAaImAgMnS	režírovat
Henri-Georges	Henri-Georges	k1gMnSc1	Henri-Georges
Clouzot	Clouzot	k1gMnSc1	Clouzot
<g/>
.	.	kIx.	.
</s>
<s>
Pablo	Pabla	k1gFnSc5	Pabla
Picasso	Picassa	k1gFnSc5	Picassa
zemřel	zemřít	k5eAaPmAgMnS	zemřít
8	[number]	k4	8
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1973	[number]	k4	1973
v	v	k7c4	v
Mougins	Mougins	k1gInSc4	Mougins
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
během	během	k7c2	během
oběda	oběd	k1gInSc2	oběd
s	s	k7c7	s
přáteli	přítel	k1gMnPc7	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnPc1	jeho
poslední	poslední	k2eAgNnPc1d1	poslední
slova	slovo	k1gNnPc1	slovo
byla	být	k5eAaImAgNnP	být
"	"	kIx"	"
<g/>
Připijte	připít	k5eAaPmRp2nP	připít
si	se	k3xPyFc3	se
na	na	k7c4	na
mě	já	k3xPp1nSc4	já
<g/>
,	,	kIx,	,
na	na	k7c4	na
mé	můj	k3xOp1gNnSc4	můj
zdraví	zdraví	k1gNnSc4	zdraví
<g/>
,	,	kIx,	,
víte	vědět	k5eAaImIp2nP	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
já	já	k3xPp1nSc1	já
už	už	k6eAd1	už
pít	pít	k5eAaImF	pít
nemohu	moct	k5eNaImIp1nS	moct
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Byl	být	k5eAaImAgMnS	být
pochován	pochován	k2eAgMnSc1d1	pochován
na	na	k7c6	na
zahradě	zahrada	k1gFnSc6	zahrada
zámku	zámek	k1gInSc2	zámek
Vauvenargues	Vauvenarguesa	k1gFnPc2	Vauvenarguesa
<g/>
,	,	kIx,	,
ve	v	k7c4	v
Vauvenargues	Vauvenargues	k1gInSc4	Vauvenargues
<g/>
,	,	kIx,	,
Bouches-du-Rhône	Bouchesu-Rhôn	k1gMnSc5	Bouches-du-Rhôn
<g/>
.	.	kIx.	.
</s>
<s>
Jacqueline	Jacquelin	k1gInSc5	Jacquelin
Roqueová	Roqueový	k2eAgFnSc1d1	Roqueový
nedovolila	dovolit	k5eNaPmAgFnS	dovolit
svým	svůj	k3xOyFgFnPc3	svůj
dětem	dítě	k1gFnPc3	dítě
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
zúčastnily	zúčastnit	k5eAaPmAgFnP	zúčastnit
pohřbu	pohřeb	k1gInSc6	pohřeb
<g/>
.	.	kIx.	.
</s>
<s>
Picasso	Picassa	k1gFnSc5	Picassa
zůstal	zůstat	k5eAaPmAgInS	zůstat
během	během	k7c2	během
španělské	španělský	k2eAgFnSc2d1	španělská
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
první	první	k4xOgInPc4	první
světové	světový	k2eAgInPc4d1	světový
války	válek	k1gInPc4	válek
i	i	k8xC	i
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
nestranný	nestranný	k2eAgMnSc1d1	nestranný
a	a	k8xC	a
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
bojovat	bojovat	k5eAaImF	bojovat
za	za	k7c4	za
jakoukoli	jakýkoli	k3yIgFnSc4	jakýkoli
stranu	strana	k1gFnSc4	strana
nebo	nebo	k8xC	nebo
zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Picasso	Picassa	k1gFnSc5	Picassa
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
nikdy	nikdy	k6eAd1	nikdy
přímo	přímo	k6eAd1	přímo
nehovořil	hovořit	k5eNaImAgMnS	hovořit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
podporoval	podporovat	k5eAaImAgMnS	podporovat
domněnku	domněnka	k1gFnSc4	domněnka
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
pacifista	pacifista	k1gMnSc1	pacifista
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
z	z	k7c2	z
jeho	jeho	k3xOp3gMnPc2	jeho
současníků	současník	k1gMnPc2	současník
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Braque	Braqu	k1gMnSc2	Braqu
<g/>
)	)	kIx)	)
měli	mít	k5eAaImAgMnP	mít
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
postoj	postoj	k1gInSc1	postoj
vychází	vycházet	k5eAaImIp3nS	vycházet
spíš	spíš	k9	spíš
ze	z	k7c2	z
zbabělosti	zbabělost	k1gFnSc2	zbabělost
než	než	k8xS	než
z	z	k7c2	z
principu	princip	k1gInSc2	princip
<g/>
.	.	kIx.	.
</s>
<s>
Jakožto	jakožto	k8xS	jakožto
španělský	španělský	k2eAgMnSc1d1	španělský
občan	občan	k1gMnSc1	občan
žijící	žijící	k2eAgMnSc1d1	žijící
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
nepociťoval	pociťovat	k5eNaImAgMnS	pociťovat
Piccaso	Piccasa	k1gFnSc5	Piccasa
touhu	touha	k1gFnSc4	touha
bojovat	bojovat	k5eAaImF	bojovat
proti	proti	k7c3	proti
Němcům	Němec	k1gMnPc3	Němec
v	v	k7c6	v
první	první	k4xOgFnSc6	první
ani	ani	k8xC	ani
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
španělské	španělský	k2eAgFnSc6d1	španělská
občanské	občanský	k2eAgFnSc6d1	občanská
válce	válka	k1gFnSc6	válka
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
Španěly	Španěl	k1gMnPc4	Španěl
žijící	žijící	k2eAgMnPc1d1	žijící
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
služba	služba	k1gFnSc1	služba
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
dobrovolná	dobrovolný	k2eAgFnSc1d1	dobrovolná
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
by	by	kYmCp3nS	by
vyžadovala	vyžadovat	k5eAaImAgFnS	vyžadovat
dobrovolný	dobrovolný	k2eAgInSc4d1	dobrovolný
návrat	návrat	k1gInSc4	návrat
do	do	k7c2	do
vlasti	vlast	k1gFnSc2	vlast
<g/>
.	.	kIx.	.
</s>
<s>
Picasso	Picassa	k1gFnSc5	Picassa
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
svůj	svůj	k3xOyFgInSc4	svůj
nesouhlas	nesouhlas	k1gInSc4	nesouhlas
s	s	k7c7	s
Frankem	frank	k1gInSc7	frank
a	a	k8xC	a
fašismem	fašismus	k1gInSc7	fašismus
pomocí	pomocí	k7c2	pomocí
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nikdy	nikdy	k6eAd1	nikdy
proti	proti	k7c3	proti
nim	on	k3xPp3gMnPc3	on
nešel	jít	k5eNaImAgMnS	jít
bojovat	bojovat	k5eAaImF	bojovat
přímo	přímo	k6eAd1	přímo
<g/>
.	.	kIx.	.
</s>
<s>
Neúčastnil	účastnit	k5eNaImAgMnS	účastnit
se	se	k3xPyFc4	se
ani	ani	k8xC	ani
Katalánského	katalánský	k2eAgNnSc2d1	katalánské
hnutí	hnutí	k1gNnSc2	hnutí
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
i	i	k9	i
přes	přes	k7c4	přes
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
vyjadřoval	vyjadřovat	k5eAaImAgMnS	vyjadřovat
souhlas	souhlas	k1gInSc4	souhlas
a	a	k8xC	a
sám	sám	k3xTgMnSc1	sám
se	se	k3xPyFc4	se
přátelil	přátelit	k5eAaImAgMnS	přátelit
s	s	k7c7	s
aktivisty	aktivista	k1gMnPc7	aktivista
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
byli	být	k5eAaImAgMnP	být
<g/>
.	.	kIx.	.
</s>
<s>
Žádné	žádný	k3yNgNnSc1	žádný
politické	politický	k2eAgNnSc1d1	politické
hnutí	hnutí	k1gNnSc1	hnutí
si	se	k3xPyFc3	se
nezískalo	získat	k5eNaPmAgNnS	získat
jeho	jeho	k3xOp3gFnSc4	jeho
podporu	podpora	k1gFnSc4	podpora
v	v	k7c6	v
nějakém	nějaký	k3yIgNnSc6	nějaký
větším	veliký	k2eAgNnSc6d2	veliký
měřítku	měřítko	k1gNnSc6	měřítko
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
pravda	pravda	k1gFnSc1	pravda
<g/>
,	,	kIx,	,
že	že	k8xS	že
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
zůstal	zůstat	k5eAaPmAgInS	zůstat
Picasso	Picassa	k1gFnSc5	Picassa
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
i	i	k8xC	i
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ji	on	k3xPp3gFnSc4	on
okupovali	okupovat	k5eAaBmAgMnP	okupovat
Němci	Němec	k1gMnPc1	Němec
<g/>
.	.	kIx.	.
</s>
<s>
Nacisté	nacista	k1gMnPc1	nacista
jeho	on	k3xPp3gInSc4	on
styl	styl	k1gInSc4	styl
malování	malování	k1gNnPc2	malování
nesnášeli	snášet	k5eNaImAgMnP	snášet
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
nemohl	moct	k5eNaImAgMnS	moct
vystavovat	vystavovat	k5eAaImF	vystavovat
<g/>
.	.	kIx.	.
</s>
<s>
Zavřel	Zavřel	k1gMnSc1	Zavřel
se	se	k3xPyFc4	se
však	však	k9	však
v	v	k7c6	v
ateliéru	ateliér	k1gInSc6	ateliér
a	a	k8xC	a
dál	daleko	k6eAd2	daleko
tvořil	tvořit	k5eAaImAgInS	tvořit
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nacisté	nacista	k1gMnPc1	nacista
zakázali	zakázat	k5eAaPmAgMnP	zakázat
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
odlévání	odlévání	k1gNnSc2	odlévání
bronzu	bronz	k1gInSc2	bronz
<g/>
,	,	kIx,	,
Piccaso	Piccasa	k1gFnSc5	Piccasa
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
s	s	k7c7	s
bronzem	bronz	k1gInSc7	bronz
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgInS	být
propašován	propašován	k2eAgInSc1d1	propašován
francouzským	francouzský	k2eAgNnSc7d1	francouzské
hnutím	hnutí	k1gNnSc7	hnutí
odporu	odpor	k1gInSc2	odpor
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
Picasso	Picassa	k1gFnSc5	Picassa
znovu	znovu	k6eAd1	znovu
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgMnS	účastnit
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
mírové	mírový	k2eAgFnSc2d1	mírová
konference	konference	k1gFnSc2	konference
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
komunismus	komunismus	k1gInSc4	komunismus
ochladl	ochladnout	k5eAaPmAgMnS	ochladnout
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
byl	být	k5eAaImAgMnS	být
kritizován	kritizovat	k5eAaImNgMnS	kritizovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInSc1	jeho
portrét	portrét	k1gInSc1	portrét
Stalina	Stalin	k1gMnSc2	Stalin
není	být	k5eNaImIp3nS	být
dostatečně	dostatečně	k6eAd1	dostatečně
realistický	realistický	k2eAgInSc1d1	realistický
<g/>
.	.	kIx.	.
</s>
<s>
Loajálním	loajální	k2eAgMnSc7d1	loajální
členem	člen	k1gMnSc7	člen
strany	strana	k1gFnSc2	strana
ale	ale	k8xC	ale
zůstal	zůstat	k5eAaPmAgInS	zůstat
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
myšlenkách	myšlenka	k1gFnPc6	myšlenka
se	se	k3xPyFc4	se
přibližoval	přibližovat	k5eAaImAgInS	přibližovat
spíš	spíš	k9	spíš
anarchistickému	anarchistický	k2eAgInSc3d1	anarchistický
komunismu	komunismus	k1gInSc3	komunismus
<g/>
.	.	kIx.	.
</s>
<s>
Picassovo	Picassův	k2eAgNnSc1d1	Picassovo
dílo	dílo	k1gNnSc1	dílo
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
do	do	k7c2	do
několika	několik	k4yIc2	několik
období	období	k1gNnPc2	období
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
názvy	název	k1gInPc1	název
období	období	k1gNnSc2	období
Picassovy	Picassův	k2eAgFnSc2d1	Picassova
tvorby	tvorba	k1gFnSc2	tvorba
jsou	být	k5eAaImIp3nP	být
předmětem	předmět	k1gInSc7	předmět
sporů	spor	k1gInPc2	spor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
asi	asi	k9	asi
neuznávanější	uznávaný	k2eNgMnPc1d2	uznávaný
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
modré	modrý	k2eAgNnSc1d1	modré
období	období	k1gNnSc1	období
(	(	kIx(	(
<g/>
1901	[number]	k4	1901
<g/>
–	–	k?	–
<g/>
1904	[number]	k4	1904
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
růžové	růžový	k2eAgNnSc4d1	růžové
období	období	k1gNnSc4	období
(	(	kIx(	(
<g/>
1905	[number]	k4	1905
<g/>
–	–	k?	–
<g/>
1907	[number]	k4	1907
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
období	období	k1gNnSc4	období
ovlivněné	ovlivněný	k2eAgFnSc2d1	ovlivněná
Afrikou	Afrika	k1gFnSc7	Afrika
(	(	kIx(	(
<g/>
1908	[number]	k4	1908
<g/>
–	–	k?	–
<g/>
1909	[number]	k4	1909
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
analytický	analytický	k2eAgInSc1d1	analytický
kubismus	kubismus	k1gInSc1	kubismus
(	(	kIx(	(
<g/>
1909	[number]	k4	1909
<g/>
–	–	k?	–
<g/>
1912	[number]	k4	1912
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
syntetický	syntetický	k2eAgInSc1d1	syntetický
kubismus	kubismus	k1gInSc1	kubismus
(	(	kIx(	(
<g/>
1912	[number]	k4	1912
<g/>
–	–	k?	–
<g/>
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1939	[number]	k4	1939
<g/>
–	–	k?	–
<g/>
40	[number]	k4	40
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
v	v	k7c6	v
Muzeu	muzeum	k1gNnSc6	muzeum
moderního	moderní	k2eAgNnSc2d1	moderní
umění	umění	k1gNnSc2	umění
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
pod	pod	k7c7	pod
záštitou	záštita	k1gFnSc7	záštita
jeho	on	k3xPp3gMnSc2	on
ředitele	ředitel	k1gMnSc2	ředitel
Alfreda	Alfred	k1gMnSc2	Alfred
Barra	Barr	k1gMnSc2	Barr
<g/>
,	,	kIx,	,
velice	velice	k6eAd1	velice
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
retrospektiva	retrospektiva	k1gFnSc1	retrospektiva
Picassova	Picassův	k2eAgNnSc2d1	Picassovo
díla	dílo	k1gNnSc2	dílo
až	až	k9	až
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
výstava	výstava	k1gFnSc1	výstava
Picassa	Picass	k1gMnSc2	Picass
proslavila	proslavit	k5eAaPmAgFnS	proslavit
a	a	k8xC	a
ukázala	ukázat	k5eAaPmAgFnS	ukázat
americké	americký	k2eAgFnPc4d1	americká
veřejnosti	veřejnost	k1gFnPc4	veřejnost
celý	celý	k2eAgInSc4d1	celý
rozsah	rozsah	k1gInSc4	rozsah
jeho	on	k3xPp3gNnSc2	on
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
historikové	historik	k1gMnPc1	historik
umění	umění	k1gNnSc4	umění
tehdy	tehdy	k6eAd1	tehdy
právě	právě	k9	právě
díky	díky	k7c3	díky
této	tento	k3xDgFnSc3	tento
výstavě	výstava	k1gFnSc6	výstava
svůj	svůj	k3xOyFgInSc4	svůj
názor	názor	k1gInSc4	názor
na	na	k7c4	na
Picassa	Picass	k1gMnSc4	Picass
přehodnotili	přehodnotit	k5eAaPmAgMnP	přehodnotit
<g/>
.	.	kIx.	.
</s>
<s>
Picasso	Picassa	k1gFnSc5	Picassa
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
učit	učit	k5eAaImF	učit
u	u	k7c2	u
svého	svůj	k3xOyFgMnSc4	svůj
otce	otec	k1gMnSc4	otec
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1890	[number]	k4	1890
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
vývoj	vývoj	k1gInSc1	vývoj
můžeme	moct	k5eAaImIp1nP	moct
sledovat	sledovat	k5eAaImF	sledovat
díky	díky	k7c3	díky
kolekci	kolekce	k1gFnSc3	kolekce
jeho	jeho	k3xOp3gFnPc2	jeho
raných	raný	k2eAgFnPc2d1	raná
prací	práce	k1gFnPc2	práce
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
teď	teď	k6eAd1	teď
v	v	k7c6	v
Picassově	Picassův	k2eAgNnSc6d1	Picassovo
muzeu	muzeum	k1gNnSc6	muzeum
v	v	k7c6	v
Barceloně	Barcelona	k1gFnSc6	Barcelona
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
existujících	existující	k2eAgFnPc2d1	existující
sbírek	sbírka	k1gFnPc2	sbírka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
dokládá	dokládat	k5eAaImIp3nS	dokládat
vývoj	vývoj	k1gInSc4	vývoj
díla	dílo	k1gNnSc2	dílo
jednoho	jeden	k4xCgMnSc2	jeden
umělce	umělec	k1gMnSc2	umělec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1893	[number]	k4	1893
se	se	k3xPyFc4	se
z	z	k7c2	z
jeho	jeho	k3xOp3gNnPc2	jeho
děl	dělo	k1gNnPc2	dělo
vytrácí	vytrácet	k5eAaImIp3nS	vytrácet
jakási	jakýsi	k3yIgFnSc1	jakýsi
mladistvost	mladistvost	k1gFnSc1	mladistvost
a	a	k8xC	a
můžeme	moct	k5eAaImIp1nP	moct
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
právě	právě	k9	právě
tímto	tento	k3xDgInSc7	tento
rokem	rok	k1gInSc7	rok
začíná	začínat	k5eAaImIp3nS	začínat
kariéra	kariéra	k1gFnSc1	kariéra
Picassa	Picass	k1gMnSc2	Picass
jako	jako	k8xS	jako
malíře	malíř	k1gMnSc2	malíř
<g/>
.	.	kIx.	.
</s>
<s>
Akademický	akademický	k2eAgInSc1d1	akademický
realismus	realismus	k1gInSc1	realismus
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
patrný	patrný	k2eAgInSc1d1	patrný
na	na	k7c6	na
jeho	jeho	k3xOp3gInPc6	jeho
dílech	díl	k1gInPc6	díl
z	z	k7c2	z
poloviny	polovina	k1gFnSc2	polovina
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
můžeme	moct	k5eAaImIp1nP	moct
vidět	vidět	k5eAaImF	vidět
např.	např.	kA	např.
na	na	k7c6	na
obraze	obraz	k1gInSc6	obraz
První	první	k4xOgNnSc1	první
přijímání	přijímání	k1gNnSc1	přijímání
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1896	[number]	k4	1896
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
velký	velký	k2eAgInSc4d1	velký
obraz	obraz	k1gInSc4	obraz
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgInSc6	který
je	být	k5eAaImIp3nS	být
Picassova	Picassův	k2eAgFnSc1d1	Picassova
sestra	sestra	k1gFnSc1	sestra
Lola	Lola	k1gFnSc1	Lola
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1897	[number]	k4	1897
namaloval	namalovat	k5eAaPmAgMnS	namalovat
několik	několik	k4yIc1	několik
obrazů	obraz	k1gInPc2	obraz
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgInP	být
ovlivněny	ovlivnit	k5eAaPmNgInP	ovlivnit
symbolismem	symbolismus	k1gInSc7	symbolismus
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
to	ten	k3xDgNnSc1	ten
krajiny	krajina	k1gFnPc1	krajina
vyvedené	vyvedený	k2eAgFnPc1d1	vyvedená
v	v	k7c6	v
nepřírodních	přírodní	k2eNgInPc6d1	nepřírodní
fialových	fialový	k2eAgInPc6d1	fialový
a	a	k8xC	a
zelených	zelený	k2eAgInPc6d1	zelený
tónech	tón	k1gInPc6	tón
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
následovalo	následovat	k5eAaImAgNnS	následovat
období	období	k1gNnSc1	období
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
nazýváno	nazývat	k5eAaImNgNnS	nazývat
jako	jako	k8xC	jako
modernistické	modernistický	k2eAgNnSc1d1	modernistické
(	(	kIx(	(
<g/>
léta	léto	k1gNnSc2	léto
1899	[number]	k4	1899
<g/>
–	–	k?	–
<g/>
1900	[number]	k4	1900
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Picasso	Picassa	k1gFnSc5	Picassa
znal	znát	k5eAaImAgMnS	znát
díla	dílo	k1gNnSc2	dílo
Rossettiho	Rossetti	k1gMnSc4	Rossetti
<g/>
,	,	kIx,	,
Steinlena	Steinlen	k2eAgMnSc4d1	Steinlen
<g/>
,	,	kIx,	,
Toulouse-Lautreca	Toulouse-Lautrec	k2eAgMnSc4d1	Toulouse-Lautrec
a	a	k8xC	a
Muncha	Munch	k1gMnSc4	Munch
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
působila	působit	k5eAaImAgFnS	působit
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
i	i	k9	i
tvorba	tvorba	k1gFnSc1	tvorba
starých	starý	k2eAgMnPc2d1	starý
mistrů	mistr	k1gMnPc2	mistr
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
byl	být	k5eAaImAgInS	být
například	například	k6eAd1	například
El	Ela	k1gFnPc2	Ela
Greco	Greco	k6eAd1	Greco
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
modrém	modrý	k2eAgNnSc6d1	modré
období	období	k1gNnSc6	období
(	(	kIx(	(
<g/>
1901	[number]	k4	1901
<g/>
–	–	k?	–
<g/>
1904	[number]	k4	1904
<g/>
)	)	kIx)	)
tvořil	tvořit	k5eAaImAgInS	tvořit
Picasso	Picassa	k1gFnSc5	Picassa
tmavé	tmavý	k2eAgInPc4d1	tmavý
obrazy	obraz	k1gInPc4	obraz
vyvedené	vyvedený	k2eAgInPc4d1	vyvedený
v	v	k7c6	v
modrých	modrý	k2eAgFnPc6d1	modrá
a	a	k8xC	a
modrozelených	modrozelený	k2eAgFnPc6d1	modrozelená
barvách	barva	k1gFnPc6	barva
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
zřídka	zřídka	k6eAd1	zřídka
použil	použít	k5eAaPmAgMnS	použít
teplejší	teplý	k2eAgFnPc4d2	teplejší
barvy	barva	k1gFnPc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
přesně	přesně	k6eAd1	přesně
toto	tento	k3xDgNnSc4	tento
Picassovo	Picassův	k2eAgNnSc4d1	Picassovo
období	období	k1gNnSc4	období
začalo	začít	k5eAaPmAgNnS	začít
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stalo	stát	k5eAaPmAgNnS	stát
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1901	[number]	k4	1901
nebo	nebo	k8xC	nebo
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Strohá	strohý	k2eAgFnSc1d1	strohá
barevnost	barevnost	k1gFnSc1	barevnost
a	a	k8xC	a
smutné	smutný	k2eAgInPc1d1	smutný
náměty	námět	k1gInPc1	námět
(	(	kIx(	(
<g/>
prostitutka	prostitutka	k1gFnSc1	prostitutka
<g/>
,	,	kIx,	,
žebrák	žebrák	k1gMnSc1	žebrák
<g/>
)	)	kIx)	)
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
za	za	k7c4	za
příčinu	příčina	k1gFnSc4	příčina
Picassovu	Picassův	k2eAgFnSc4d1	Picassova
cestu	cesta	k1gFnSc4	cesta
po	po	k7c6	po
Španělsku	Španělsko	k1gNnSc6	Španělsko
a	a	k8xC	a
také	také	k9	také
sebevraždu	sebevražda	k1gFnSc4	sebevražda
jeho	on	k3xPp3gMnSc2	on
přítele	přítel	k1gMnSc2	přítel
Carlose	Carlosa	k1gFnSc6	Carlosa
Casagemase	Casagemas	k1gInSc6	Casagemas
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
podzimu	podzim	k1gInSc2	podzim
roku	rok	k1gInSc2	rok
1901	[number]	k4	1901
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
několik	několik	k4yIc4	několik
posmrtných	posmrtný	k2eAgInPc2d1	posmrtný
portrétů	portrét	k1gInPc2	portrét
Casagemase	Casagemasa	k1gFnSc6	Casagemasa
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1903	[number]	k4	1903
namaloval	namalovat	k5eAaPmAgInS	namalovat
temný	temný	k2eAgInSc1d1	temný
alegorický	alegorický	k2eAgInSc1d1	alegorický
obraz	obraz	k1gInSc1	obraz
La	la	k1gNnSc2	la
Vie	Vie	k1gFnSc2	Vie
–	–	k?	–
Život	život	k1gInSc1	život
<g/>
.	.	kIx.	.
</s>
<s>
Stejná	stejný	k2eAgFnSc1d1	stejná
nálada	nálada	k1gFnSc1	nálada
panuje	panovat	k5eAaImIp3nS	panovat
i	i	k9	i
na	na	k7c6	na
rytině	rytina	k1gFnSc6	rytina
s	s	k7c7	s
názvem	název	k1gInSc7	název
Skromný	skromný	k2eAgInSc4d1	skromný
pokrm	pokrm	k1gInSc4	pokrm
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1904	[number]	k4	1904
<g/>
,	,	kIx,	,
na	na	k7c6	na
které	který	k3yRgFnSc6	který
je	být	k5eAaImIp3nS	být
žena	žena	k1gFnSc1	žena
a	a	k8xC	a
slepý	slepý	k2eAgMnSc1d1	slepý
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
oba	dva	k4xCgMnPc1	dva
jsou	být	k5eAaImIp3nP	být
vyhublí	vyhublý	k2eAgMnPc1d1	vyhublý
a	a	k8xC	a
sedí	sedit	k5eAaImIp3nP	sedit
u	u	k7c2	u
skoro	skoro	k6eAd1	skoro
prázdného	prázdný	k2eAgInSc2d1	prázdný
stolu	stol	k1gInSc2	stol
<g/>
.	.	kIx.	.
</s>
<s>
Téma	téma	k1gNnSc1	téma
slepoty	slepota	k1gFnSc2	slepota
se	se	k3xPyFc4	se
v	v	k7c6	v
Picassových	Picassův	k2eAgInPc6d1	Picassův
obrazech	obraz	k1gInPc6	obraz
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
objevuje	objevovat	k5eAaImIp3nS	objevovat
častěji	často	k6eAd2	často
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
časté	častý	k2eAgNnSc1d1	časté
téma	téma	k1gNnSc1	téma
jsou	být	k5eAaImIp3nP	být
umělci	umělec	k1gMnPc1	umělec
<g/>
,	,	kIx,	,
akrobati	akrobat	k1gMnPc1	akrobat
a	a	k8xC	a
harlekýni	harlekýn	k1gMnPc1	harlekýn
<g/>
.	.	kIx.	.
</s>
<s>
Harlekýn	harlekýn	k1gMnSc1	harlekýn
–	–	k?	–
obvykle	obvykle	k6eAd1	obvykle
vyobrazený	vyobrazený	k2eAgInSc4d1	vyobrazený
v	v	k7c6	v
oblečení	oblečení	k1gNnSc6	oblečení
se	s	k7c7	s
čtvercovým	čtvercový	k2eAgInSc7d1	čtvercový
vzorem	vzor	k1gInSc7	vzor
–	–	k?	–
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Picassovým	Picassův	k2eAgInSc7d1	Picassův
osobním	osobní	k2eAgInSc7d1	osobní
symbolem	symbol	k1gInSc7	symbol
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
růžové	růžový	k2eAgNnSc4d1	růžové
období	období	k1gNnSc4	období
(	(	kIx(	(
<g/>
léta	léto	k1gNnSc2	léto
1905	[number]	k4	1905
<g/>
–	–	k?	–
<g/>
1907	[number]	k4	1907
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
veselejší	veselý	k2eAgInSc1d2	veselejší
nádech	nádech	k1gInSc1	nádech
s	s	k7c7	s
oranžovými	oranžový	k2eAgFnPc7d1	oranžová
a	a	k8xC	a
růžovými	růžový	k2eAgFnPc7d1	růžová
barvami	barva	k1gFnPc7	barva
<g/>
.	.	kIx.	.
</s>
<s>
Opět	opět	k6eAd1	opět
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
často	často	k6eAd1	často
objevuje	objevovat	k5eAaImIp3nS	objevovat
harlekýn	harlekýn	k1gMnSc1	harlekýn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1904	[number]	k4	1904
se	se	k3xPyFc4	se
Picasso	Picassa	k1gFnSc5	Picassa
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
Fernande	Fernand	k1gInSc5	Fernand
Olivierovou	Olivierův	k2eAgFnSc7d1	Olivierova
<g/>
.	.	kIx.	.
</s>
<s>
Vztah	vztah	k1gInSc1	vztah
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
a	a	k8xC	a
také	také	k9	také
vliv	vliv	k1gInSc4	vliv
francouzských	francouzský	k2eAgMnPc2d1	francouzský
malířů	malíř	k1gMnPc2	malíř
se	se	k3xPyFc4	se
značně	značně	k6eAd1	značně
podepsaly	podepsat	k5eAaPmAgInP	podepsat
na	na	k7c6	na
Picassově	Picassův	k2eAgFnSc6d1	Picassova
tvorbě	tvorba	k1gFnSc6	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1906	[number]	k4	1906
strávil	strávit	k5eAaPmAgMnS	strávit
Picasso	Picassa	k1gFnSc5	Picassa
s	s	k7c7	s
Olivierovou	Olivierová	k1gFnSc4	Olivierová
deset	deset	k4xCc4	deset
týdnů	týden	k1gInPc2	týden
v	v	k7c6	v
odlehlé	odlehlý	k2eAgFnSc6d1	odlehlá
katalánské	katalánský	k2eAgFnSc6d1	katalánská
horské	horský	k2eAgFnSc6d1	horská
vesničce	vesnička	k1gFnSc6	vesnička
Gosol	Gosola	k1gFnPc2	Gosola
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
silně	silně	k6eAd1	silně
zapůsobilo	zapůsobit	k5eAaPmAgNnS	zapůsobit
primitivní	primitivní	k2eAgNnSc1d1	primitivní
románské	románský	k2eAgNnSc1d1	románské
umění	umění	k1gNnSc1	umění
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
nastal	nastat	k5eAaPmAgInS	nastat
radikální	radikální	k2eAgInSc1d1	radikální
zlom	zlom	k1gInSc1	zlom
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
malbě	malba	k1gFnSc6	malba
(	(	kIx(	(
<g/>
Dva	dva	k4xCgInPc4	dva
akty	akt	k1gInPc4	akt
<g/>
,	,	kIx,	,
1906	[number]	k4	1906
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
později	pozdě	k6eAd2	pozdě
označen	označit	k5eAaPmNgInS	označit
jako	jako	k8xC	jako
africké	africký	k2eAgNnSc1d1	africké
období	období	k1gNnSc1	období
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
Picassova	Picassův	k2eAgNnSc2d1	Picassovo
afrického	africký	k2eAgNnSc2d1	africké
období	období	k1gNnSc2	období
(	(	kIx(	(
<g/>
léta	léto	k1gNnSc2	léto
1907	[number]	k4	1907
<g/>
–	–	k?	–
<g/>
1909	[number]	k4	1909
<g/>
)	)	kIx)	)
stojí	stát	k5eAaImIp3nS	stát
dvě	dva	k4xCgFnPc4	dva
postavy	postava	k1gFnPc4	postava
na	na	k7c6	na
obraze	obraz	k1gInSc6	obraz
Avignonské	avignonský	k2eAgFnSc2d1	avignonská
slečny	slečna	k1gFnSc2	slečna
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgInP	být
inspirovány	inspirovat	k5eAaBmNgInP	inspirovat
předměty	předmět	k1gInPc7	předmět
přivezenými	přivezený	k2eAgInPc7d1	přivezený
z	z	k7c2	z
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Nápady	nápad	k1gInPc4	nápad
vzniklé	vzniklý	k2eAgInPc4d1	vzniklý
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
vedou	vést	k5eAaImIp3nP	vést
přímo	přímo	k6eAd1	přímo
k	k	k7c3	k
následujícímu	následující	k2eAgNnSc3d1	následující
období	období	k1gNnSc3	období
–	–	k?	–
kubismu	kubismus	k1gInSc3	kubismus
<g/>
.	.	kIx.	.
</s>
<s>
Analytický	analytický	k2eAgInSc1d1	analytický
kubismus	kubismus	k1gInSc1	kubismus
(	(	kIx(	(
<g/>
léta	léto	k1gNnSc2	léto
1909	[number]	k4	1909
<g/>
–	–	k?	–
<g/>
1912	[number]	k4	1912
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
styl	styl	k1gInSc4	styl
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
Picasso	Picassa	k1gFnSc5	Picassa
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
společně	společně	k6eAd1	společně
s	s	k7c7	s
Braquem	Braqu	k1gInSc7	Braqu
<g/>
.	.	kIx.	.
</s>
<s>
Obrazy	obraz	k1gInPc4	obraz
obou	dva	k4xCgFnPc2	dva
umělců	umělec	k1gMnPc2	umělec
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
temné	temný	k2eAgNnSc1d1	temné
<g/>
,	,	kIx,	,
vyvedené	vyvedený	k2eAgNnSc1d1	vyvedené
v	v	k7c6	v
hnědých	hnědý	k2eAgFnPc6d1	hnědá
barvách	barva	k1gFnPc6	barva
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
umělci	umělec	k1gMnPc1	umělec
malovali	malovat	k5eAaImAgMnP	malovat
věci	věc	k1gFnPc4	věc
tak	tak	k9	tak
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
by	by	kYmCp3nP	by
byly	být	k5eAaImAgFnP	být
viděny	vidět	k5eAaImNgFnP	vidět
z	z	k7c2	z
více	hodně	k6eAd2	hodně
úhlů	úhel	k1gInPc2	úhel
najednou	najednou	k6eAd1	najednou
<g/>
.	.	kIx.	.
</s>
<s>
Obrazy	obraz	k1gInPc1	obraz
Picassa	Picass	k1gMnSc2	Picass
a	a	k8xC	a
Braqua	Braquus	k1gMnSc2	Braquus
z	z	k7c2	z
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
jsou	být	k5eAaImIp3nP	být
si	se	k3xPyFc3	se
velice	velice	k6eAd1	velice
podobné	podobný	k2eAgInPc1d1	podobný
<g/>
.	.	kIx.	.
</s>
<s>
Syntetický	syntetický	k2eAgInSc1d1	syntetický
kubismus	kubismus	k1gInSc1	kubismus
(	(	kIx(	(
<g/>
léta	léto	k1gNnSc2	léto
1912	[number]	k4	1912
<g/>
–	–	k?	–
<g/>
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dalším	další	k2eAgInSc7d1	další
stupněm	stupeň	k1gInSc7	stupeň
kubismu	kubismus	k1gInSc2	kubismus
<g/>
.	.	kIx.	.
</s>
<s>
Umělec	umělec	k1gMnSc1	umělec
na	na	k7c4	na
obraz	obraz	k1gInSc4	obraz
lepí	lepit	k5eAaImIp3nP	lepit
i	i	k9	i
kusy	kus	k1gInPc1	kus
papíru	papír	k1gInSc2	papír
–	–	k?	–
často	často	k6eAd1	často
tapety	tapeta	k1gFnPc1	tapeta
nebo	nebo	k8xC	nebo
noviny	novina	k1gFnPc1	novina
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
první	první	k4xOgNnSc4	první
použití	použití	k1gNnSc4	použití
koláže	koláž	k1gFnSc2	koláž
v	v	k7c6	v
umění	umění	k1gNnSc6	umění
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Po	po	k7c6	po
První	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
tvořil	tvořit	k5eAaImAgInS	tvořit
Picasso	Picassa	k1gFnSc5	Picassa
v	v	k7c6	v
neoklasicistním	neoklasicistní	k2eAgInSc6d1	neoklasicistní
stylu	styl	k1gInSc6	styl
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
"	"	kIx"	"
<g/>
návrat	návrat	k1gInSc1	návrat
k	k	k7c3	k
pořádku	pořádek	k1gInSc3	pořádek
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
viditelný	viditelný	k2eAgInSc1d1	viditelný
v	v	k7c6	v
tvorbě	tvorba	k1gFnSc6	tvorba
mnoha	mnoho	k4c2	mnoho
umělců	umělec	k1gMnPc2	umělec
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Picassovy	Picassův	k2eAgInPc1d1	Picassův
obrazy	obraz	k1gInPc1	obraz
a	a	k8xC	a
kresby	kresba	k1gFnPc1	kresba
z	z	k7c2	z
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
připomínají	připomínat	k5eAaImIp3nP	připomínat
díla	dílo	k1gNnPc4	dílo
Ingrese	ingrese	k1gFnSc2	ingrese
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vystřídal	vystřídat	k5eAaPmAgMnS	vystřídat
harlekýna	harlekýn	k1gMnSc4	harlekýn
jako	jako	k9	jako
hlavní	hlavní	k2eAgInSc4d1	hlavní
motiv	motiv	k1gInSc4	motiv
Picassových	Picassův	k2eAgInPc2d1	Picassův
obrazů	obraz	k1gInPc2	obraz
minotaurus	minotaurus	k1gInSc4	minotaurus
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Picasso	Picassa	k1gFnSc5	Picassa
používal	používat	k5eAaImAgMnS	používat
motiv	motiv	k1gInSc4	motiv
minotaura	minotaur	k1gMnSc2	minotaur
<g/>
,	,	kIx,	,
můžeme	moct	k5eAaImIp1nP	moct
zčásti	zčásti	k6eAd1	zčásti
přičíst	přičíst	k5eAaPmF	přičíst
jeho	on	k3xPp3gInSc2	on
kontaktu	kontakt	k1gInSc2	kontakt
se	s	k7c7	s
surrealisty	surrealista	k1gMnPc7	surrealista
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
minotaura	minotaur	k1gMnSc4	minotaur
často	často	k6eAd1	často
používali	používat	k5eAaImAgMnP	používat
jako	jako	k9	jako
svůj	svůj	k3xOyFgInSc4	svůj
symbol	symbol	k1gInSc4	symbol
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Picassa	Picass	k1gMnSc2	Picass
ho	on	k3xPp3gMnSc4	on
můžeme	moct	k5eAaImIp1nP	moct
najít	najít	k5eAaPmF	najít
např.	např.	kA	např.
v	v	k7c6	v
obraze	obraz	k1gInSc6	obraz
Guernica	Guernic	k1gInSc2	Guernic
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gInPc2	jeho
nejznámějších	známý	k2eAgInPc2d3	nejznámější
obrazů	obraz	k1gInPc2	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
bombardování	bombardování	k1gNnSc1	bombardování
španělského	španělský	k2eAgNnSc2d1	španělské
města	město	k1gNnSc2	město
Guernica	Guernic	k1gInSc2	Guernic
Němci	Němec	k1gMnPc7	Němec
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
bylo	být	k5eAaImAgNnS	být
toto	tento	k3xDgNnSc1	tento
plátno	plátno	k1gNnSc1	plátno
v	v	k7c6	v
newyorském	newyorský	k2eAgNnSc6d1	newyorské
Muzeu	muzeum	k1gNnSc6	muzeum
moderního	moderní	k2eAgNnSc2d1	moderní
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
se	se	k3xPyFc4	se
vrátilo	vrátit	k5eAaPmAgNnS	vrátit
do	do	k7c2	do
Španělska	Španělsko	k1gNnSc2	Španělsko
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Madridu	Madrid	k1gInSc6	Madrid
<g/>
.	.	kIx.	.
</s>
<s>
Picasso	Picassa	k1gFnSc5	Picassa
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
250	[number]	k4	250
sochařů	sochař	k1gMnPc2	sochař
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
vystavovali	vystavovat	k5eAaImAgMnP	vystavovat
ve	v	k7c6	v
Filadelfii	Filadelfie	k1gFnSc6	Filadelfie
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
1949	[number]	k4	1949
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
Picassův	Picassův	k2eAgInSc1d1	Picassův
styl	styl	k1gInSc1	styl
opět	opět	k6eAd1	opět
změnil	změnit	k5eAaPmAgInS	změnit
<g/>
,	,	kIx,	,
když	když	k8xS	když
začal	začít	k5eAaPmAgInS	začít
tvořit	tvořit	k5eAaImF	tvořit
interpretace	interpretace	k1gFnPc4	interpretace
děl	dělo	k1gNnPc2	dělo
starých	starý	k2eAgMnPc2d1	starý
mistrů	mistr	k1gMnPc2	mistr
<g/>
.	.	kIx.	.
</s>
<s>
Vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
skupinu	skupina	k1gFnSc4	skupina
obrazů	obraz	k1gInPc2	obraz
založených	založený	k2eAgInPc2d1	založený
na	na	k7c6	na
Velazquezových	Velazquezová	k1gFnPc6	Velazquezová
Las	laso	k1gNnPc2	laso
Meninas	Meninasa	k1gFnPc2	Meninasa
–	–	k?	–
Dvorní	dvorní	k2eAgFnSc2d1	dvorní
dámy	dáma	k1gFnSc2	dáma
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnPc4d1	další
díla	dílo	k1gNnPc4	dílo
založil	založit	k5eAaPmAgMnS	založit
na	na	k7c6	na
obrazech	obraz	k1gInPc6	obraz
od	od	k7c2	od
Goyi	Goy	k1gFnSc2	Goy
<g/>
,	,	kIx,	,
Poussina	Poussin	k2eAgMnSc2d1	Poussin
<g/>
,	,	kIx,	,
Maneta	manet	k1gInSc2	manet
<g/>
,	,	kIx,	,
Courbeta	Courbeto	k1gNnSc2	Courbeto
a	a	k8xC	a
Delacroixe	Delacroixe	k1gFnSc2	Delacroixe
<g/>
.	.	kIx.	.
</s>
<s>
Dostal	dostat	k5eAaPmAgMnS	dostat
zakázku	zakázka	k1gFnSc4	zakázka
na	na	k7c4	na
vytvoření	vytvoření	k1gNnSc4	vytvoření
makety	maketa	k1gFnSc2	maketa
pro	pro	k7c4	pro
patnáctimetrovou	patnáctimetrový	k2eAgFnSc4d1	patnáctimetrová
sochu	socha	k1gFnSc4	socha
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
projektu	projekt	k1gInSc2	projekt
se	se	k3xPyFc4	se
vrhl	vrhnout	k5eAaPmAgMnS	vrhnout
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
zápalem	zápal	k1gInSc7	zápal
a	a	k8xC	a
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
skulpturu	skulptura	k1gFnSc4	skulptura
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
dvojsmyslná	dvojsmyslný	k2eAgFnSc1d1	dvojsmyslná
a	a	k8xC	a
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
i	i	k8xC	i
kontroverzní	kontroverzní	k2eAgMnSc1d1	kontroverzní
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
odhalena	odhalen	k2eAgFnSc1d1	odhalena
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgFnPc2d3	nejznámější
pamětihodností	pamětihodnost	k1gFnPc2	pamětihodnost
Chicaga	Chicago	k1gNnSc2	Chicago
<g/>
.	.	kIx.	.
</s>
<s>
Picasso	Picassa	k1gFnSc5	Picassa
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
odměnu	odměna	k1gFnSc4	odměna
100	[number]	k4	100
000	[number]	k4	000
dolarů	dolar	k1gInPc2	dolar
a	a	k8xC	a
věnoval	věnovat	k5eAaPmAgMnS	věnovat
je	být	k5eAaImIp3nS	být
občanům	občan	k1gMnPc3	občan
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Picassova	Picassův	k2eAgNnPc1d1	Picassovo
pozdní	pozdní	k2eAgNnPc1d1	pozdní
díla	dílo	k1gNnPc1	dílo
byla	být	k5eAaImAgNnP	být
směsicí	směsice	k1gFnSc7	směsice
stylů	styl	k1gInPc2	styl
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInPc4	jeho
prostředky	prostředek	k1gInPc4	prostředek
vyjadřování	vyjadřování	k1gNnSc2	vyjadřování
v	v	k7c6	v
neustálé	neustálý	k2eAgFnSc6d1	neustálá
proměně	proměna	k1gFnSc6	proměna
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
jeho	on	k3xPp3gInSc2	on
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Picasso	Picassa	k1gFnSc5	Picassa
se	se	k3xPyFc4	se
cele	cele	k6eAd1	cele
věnoval	věnovat	k5eAaImAgMnS	věnovat
práci	práce	k1gFnSc4	práce
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
odvážnější	odvážný	k2eAgNnSc4d2	odvážnější
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc1	jeho
dílo	dílo	k1gNnSc1	dílo
barevnější	barevný	k2eAgNnSc1d2	barevnější
a	a	k8xC	a
expresivnější	expresivní	k2eAgNnSc1d2	expresivnější
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1968	[number]	k4	1968
a	a	k8xC	a
1971	[number]	k4	1971
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
množství	množství	k1gNnSc4	množství
obrazů	obraz	k1gInPc2	obraz
a	a	k8xC	a
stovky	stovka	k1gFnPc4	stovka
leptů	lept	k1gInPc2	lept
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
obrazy	obraz	k1gInPc1	obraz
nebyly	být	k5eNaImAgInP	být
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
době	doba	k1gFnSc6	doba
nijak	nijak	k6eAd1	nijak
dobře	dobře	k6eAd1	dobře
přijímány	přijímat	k5eAaImNgInP	přijímat
<g/>
,	,	kIx,	,
teprve	teprve	k6eAd1	teprve
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
Picasso	Picassa	k1gFnSc5	Picassa
uznán	uznat	k5eAaPmNgInS	uznat
za	za	k7c4	za
umělce	umělec	k1gMnPc4	umělec
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
často	často	k6eAd1	často
daleko	daleko	k6eAd1	daleko
před	před	k7c7	před
svou	svůj	k3xOyFgFnSc7	svůj
dobou	doba	k1gFnSc7	doba
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1955	[number]	k4	1955
fotograf	fotograf	k1gMnSc1	fotograf
Lucien	Lucien	k2eAgInSc4d1	Lucien
Clergue	Clergue	k1gInSc4	Clergue
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Picassa	Picass	k1gMnSc4	Picass
v	v	k7c6	v
Cannes	Cannes	k1gNnSc6	Cannes
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
však	však	k9	však
setkali	setkat	k5eAaPmAgMnP	setkat
několik	několik	k4yIc1	několik
let	léto	k1gNnPc2	léto
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
na	na	k7c6	na
koridě	korida	k1gFnSc6	korida
v	v	k7c4	v
Arles	Arles	k1gInSc4	Arles
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Lucien	Lucien	k2eAgInSc1d1	Lucien
Clergue	Clergue	k1gInSc1	Clergue
ukázal	ukázat	k5eAaPmAgInS	ukázat
své	svůj	k3xOyFgFnPc4	svůj
fotografie	fotografia	k1gFnPc4	fotografia
Picassovi	Picassův	k2eAgMnPc1d1	Picassův
a	a	k8xC	a
ten	ten	k3xDgInSc1	ten
chtěl	chtít	k5eAaImAgInS	chtít
vidět	vidět	k5eAaImF	vidět
i	i	k9	i
ostatní	ostatní	k2eAgInPc4d1	ostatní
snímky	snímek	k1gInPc4	snímek
<g/>
.	.	kIx.	.
</s>
<s>
Mladý	mladý	k2eAgInSc1d1	mladý
Clergue	Clergue	k1gInSc1	Clergue
mu	on	k3xPp3gMnSc3	on
pak	pak	k6eAd1	pak
léta	léto	k1gNnSc2	léto
své	svůj	k3xOyFgFnSc2	svůj
snímky	snímka	k1gFnSc2	snímka
posílal	posílat	k5eAaImAgMnS	posílat
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
přátelství	přátelství	k1gNnSc1	přátelství
trvalo	trvat	k5eAaImAgNnS	trvat
30	[number]	k4	30
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
až	až	k9	až
do	do	k7c2	do
mistrovy	mistrův	k2eAgFnSc2d1	Mistrova
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
Můj	můj	k1gMnSc1	můj
přítel	přítel	k1gMnSc1	přítel
Picasso	Picassa	k1gFnSc5	Picassa
(	(	kIx(	(
<g/>
Picasso	Picassa	k1gFnSc5	Picassa
my	my	k3xPp1nPc1	my
friend	friend	k1gInSc1	friend
<g/>
)	)	kIx)	)
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
důležité	důležitý	k2eAgInPc4d1	důležitý
okamžiky	okamžik	k1gInPc4	okamžik
jejich	jejich	k3xOp3gInSc2	jejich
vztahu	vztah	k1gInSc2	vztah
<g/>
.	.	kIx.	.
</s>
