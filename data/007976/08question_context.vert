<s>
Pablo	Pablo	k1gNnSc1	Pablo
Ruiz	Ruiza	k1gFnPc2	Ruiza
Picasso	Picassa	k1gFnSc5	Picassa
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1881	[number]	k4	1881
–	–	k?	–
8	[number]	k4	8
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
španělský	španělský	k2eAgMnSc1d1	španělský
malíř	malíř	k1gMnSc1	malíř
a	a	k8xC	a
sochař	sochař	k1gMnSc1	sochař
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
celé	celý	k2eAgNnSc1d1	celé
jméno	jméno	k1gNnSc1	jméno
je	být	k5eAaImIp3nS	být
Pablo	Pablo	k1gNnSc4	Pablo
Diego	Diego	k6eAd1	Diego
José	Josý	k2eAgNnSc4d1	José
Francisco	Francisco	k1gNnSc4	Francisco
de	de	k?	de
Paula	Paula	k1gFnSc1	Paula
Juan	Juan	k1gMnSc1	Juan
Nepomuceno	Nepomucen	k2eAgNnSc4d1	Nepomuceno
María	Maríum	k1gNnSc2	Maríum
de	de	k?	de
los	los	k1gMnSc1	los
Remedios	Remedios	k1gMnSc1	Remedios
Cipriano	Cipriana	k1gFnSc5	Cipriana
de	de	k?	de
la	la	k1gNnSc2	la
Santísima	Santísima	k1gNnSc1	Santísima
Trinidad	Trinidad	k1gInSc1	Trinidad
Ruiz	Ruiz	k1gMnSc1	Ruiz
y	y	k?	y
Picasso	Picassa	k1gFnSc5	Picassa
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
osobností	osobnost	k1gFnPc2	osobnost
umění	umění	k1gNnSc1	umění
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
Georgesem	Georges	k1gMnSc7	Georges
Braquem	Braqu	k1gMnSc7	Braqu
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
zakladatele	zakladatel	k1gMnSc4	zakladatel
kubismu	kubismus	k1gInSc2	kubismus
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
Picasso	Picassa	k1gFnSc5	Picassa
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
asi	asi	k9	asi
13	[number]	k4	13
500	[number]	k4	500
obrazů	obraz	k1gInPc2	obraz
a	a	k8xC	a
skic	skica	k1gFnPc2	skica
<g/>
,	,	kIx,	,
100	[number]	k4	100
000	[number]	k4	000
rytin	rytina	k1gFnPc2	rytina
a	a	k8xC	a
tisků	tisk	k1gInPc2	tisk
<g/>
,	,	kIx,	,
34	[number]	k4	34
000	[number]	k4	000
ilustrací	ilustrace	k1gFnPc2	ilustrace
a	a	k8xC	a
300	[number]	k4	300
skulptur	skulptura	k1gFnPc2	skulptura
a	a	k8xC	a
keramických	keramický	k2eAgNnPc2d1	keramické
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
obraz	obraz	k1gInSc1	obraz
Alžírské	alžírský	k2eAgFnSc2d1	alžírská
ženy	žena	k1gFnSc2	žena
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2015	[number]	k4	2015
v	v	k7c6	v
newyorské	newyorský	k2eAgFnSc6d1	newyorská
aukční	aukční	k2eAgFnSc6d1	aukční
síni	síň	k1gFnSc6	síň
Christie	Christie	k1gFnSc2	Christie
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
vydražen	vydražit	k5eAaPmNgInS	vydražit
za	za	k7c4	za
179,4	[number]	k4	179,4
milionu	milion	k4xCgInSc2	milion
dolarů	dolar	k1gInPc2	dolar
(	(	kIx(	(
<g/>
v	v	k7c6	v
přepočtu	přepočet	k1gInSc6	přepočet
4,4	[number]	k4	4,4
miliardy	miliarda	k4xCgFnSc2	miliarda
korun	koruna	k1gFnPc2	koruna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
historicky	historicky	k6eAd1	historicky
nejdražším	drahý	k2eAgInSc7d3	nejdražší
výtvarným	výtvarný	k2eAgInSc7d1	výtvarný
dílem	díl	k1gInSc7	díl
prodaným	prodaný	k2eAgInSc7d1	prodaný
v	v	k7c6	v
aukci	aukce	k1gFnSc6	aukce
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Málaze	Málaha	k1gFnSc6	Málaha
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
dítětem	dítě	k1gNnSc7	dítě
José	Josý	k2eAgFnSc2d1	Josý
Ruize	Ruize	k1gFnSc2	Ruize
y	y	k?	y
Blasco	Blasco	k1gMnSc1	Blasco
a	a	k8xC	a
Maríe	Maríe	k1gFnSc1	Maríe
Picasso	Picassa	k1gFnSc5	Picassa
y	y	k?	y
López	Lópeza	k1gFnPc2	Lópeza
<g/>
.	.	kIx.	.
</s>
