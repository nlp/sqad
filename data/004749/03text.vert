<s>
26	[number]	k4	26
<g/>
.	.	kIx.	.
květen	květen	k1gInSc4	květen
je	být	k5eAaImIp3nS	být
146	[number]	k4	146
<g/>
.	.	kIx.	.
den	den	k1gInSc4	den
roku	rok	k1gInSc2	rok
podle	podle	k7c2	podle
gregoriánského	gregoriánský	k2eAgInSc2d1	gregoriánský
kalendáře	kalendář	k1gInSc2	kalendář
(	(	kIx(	(
<g/>
147	[number]	k4	147
<g/>
.	.	kIx.	.
v	v	k7c6	v
přestupném	přestupný	k2eAgInSc6d1	přestupný
roce	rok	k1gInSc6	rok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
zbývá	zbývat	k5eAaImIp3nS	zbývat
219	[number]	k4	219
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Česko	Česko	k1gNnSc1	Česko
1725	[number]	k4	1725
–	–	k?	–
Zřízení	zřízení	k1gNnSc3	zřízení
prvního	první	k4xOgInSc2	první
záchodu	záchod	k1gInSc2	záchod
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
určen	určit	k5eAaPmNgInS	určit
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
chození	chození	k1gNnSc4	chození
na	na	k7c4	na
"	"	kIx"	"
<g/>
menší	malý	k2eAgFnSc4d2	menší
úlevu	úleva	k1gFnSc4	úleva
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
1872	[number]	k4	1872
–	–	k?	–
Emil	Emil	k1gMnSc1	Emil
Holub	Holub	k1gMnSc1	Holub
se	se	k3xPyFc4	se
vydává	vydávat	k5eAaImIp3nS	vydávat
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Afriky	Afrika	k1gFnSc2	Afrika
do	do	k7c2	do
země	zem	k1gFnSc2	zem
divokých	divoký	k2eAgInPc2d1	divoký
Mašukulumbů	Mašukulumb	k1gInPc2	Mašukulumb
<g/>
.	.	kIx.	.
1916	[number]	k4	1916
–	–	k?	–
Premiéra	premiéra	k1gFnSc1	premiéra
Janáčkovy	Janáčkův	k2eAgFnSc2d1	Janáčkova
opery	opera	k1gFnSc2	opera
Její	její	k3xOp3gFnSc1	její
pastorkyňa	pastorkyňa	k1gFnSc1	pastorkyňa
v	v	k7c6	v
Národním	národní	k2eAgNnSc6d1	národní
divadle	divadlo	k1gNnSc6	divadlo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
1928	[number]	k4	1928
–	–	k?	–
Otevření	otevření	k1gNnSc2	otevření
nově	nově	k6eAd1	nově
vybudovaného	vybudovaný	k2eAgNnSc2d1	vybudované
Brněnského	brněnský	k2eAgNnSc2d1	brněnské
výstaviště	výstaviště	k1gNnSc2	výstaviště
<g/>
.	.	kIx.	.
1946	[number]	k4	1946
–	–	k?	–
Konaly	konat	k5eAaImAgFnP	konat
se	se	k3xPyFc4	se
československé	československý	k2eAgFnPc1d1	Československá
parlamentní	parlamentní	k2eAgFnPc1d1	parlamentní
volby	volba	k1gFnPc1	volba
<g/>
.	.	kIx.	.
</s>
<s>
Svět	svět	k1gInSc1	svět
1800	[number]	k4	1800
–	–	k?	–
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
revoluční	revoluční	k2eAgFnSc2d1	revoluční
války	válka	k1gFnSc2	válka
<g/>
:	:	kIx,	:
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Chiuselly	Chiusella	k1gFnSc2	Chiusella
<g/>
.	.	kIx.	.
1897	[number]	k4	1897
–	–	k?	–
Byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
román	román	k1gInSc1	román
Dracula	Draculum	k1gNnSc2	Draculum
Brama	brama	k1gFnSc1	brama
Stokera	Stokera	k1gFnSc1	Stokera
<g/>
.	.	kIx.	.
1986	[number]	k4	1986
–	–	k?	–
Evropské	evropský	k2eAgNnSc1d1	Evropské
hospodářské	hospodářský	k2eAgNnSc1d1	hospodářské
společenství	společenství	k1gNnSc1	společenství
přijalo	přijmout	k5eAaPmAgNnS	přijmout
Evropskou	evropský	k2eAgFnSc4d1	Evropská
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
.	.	kIx.	.
1994	[number]	k4	1994
–	–	k?	–
Svatebním	svatební	k2eAgInSc7d1	svatební
obřadem	obřad	k1gInSc7	obřad
v	v	k7c6	v
Dominikánské	dominikánský	k2eAgFnSc6d1	Dominikánská
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
utajeným	utajený	k2eAgInSc7d1	utajený
před	před	k7c7	před
médii	médium	k1gNnPc7	médium
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
<g />
.	.	kIx.	.
</s>
<s>
manžely	manžel	k1gMnPc4	manžel
Michael	Michael	k1gMnSc1	Michael
Jackson	Jackson	k1gMnSc1	Jackson
a	a	k8xC	a
Lisa	Lisa	k1gFnSc1	Lisa
Marie	Marie	k1gFnSc1	Marie
Pressleyová	Pressleyová	k1gFnSc1	Pressleyová
<g/>
.	.	kIx.	.
1728	[number]	k4	1728
–	–	k?	–
Anna	Anna	k1gFnSc1	Anna
Františka	Františka	k1gFnSc1	Františka
Hatašová	Hatašová	k1gFnSc1	Hatašová
<g/>
,	,	kIx,	,
operní	operní	k2eAgFnSc1d1	operní
pěvkyně	pěvkyně	k1gFnSc1	pěvkyně
(	(	kIx(	(
<g/>
†	†	k?	†
15	[number]	k4	15
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1781	[number]	k4	1781
<g/>
)	)	kIx)	)
1782	[number]	k4	1782
–	–	k?	–
Josef	Josef	k1gMnSc1	Josef
Drechsler	Drechsler	k1gMnSc1	Drechsler
<g/>
,	,	kIx,	,
česko-rakouský	českoakouský	k2eAgMnSc1d1	česko-rakouský
kapelník	kapelník	k1gMnSc1	kapelník
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
pedagog	pedagog	k1gMnSc1	pedagog
pocházející	pocházející	k2eAgMnSc1d1	pocházející
z	z	k7c2	z
Čech	Čechy	k1gFnPc2	Čechy
(	(	kIx(	(
<g/>
†	†	k?	†
27	[number]	k4	27
<g/>
<g />
.	.	kIx.	.
</s>
<s>
února	únor	k1gInSc2	únor
1852	[number]	k4	1852
<g/>
)	)	kIx)	)
1840	[number]	k4	1840
–	–	k?	–
Ignác	Ignác	k1gMnSc1	Ignác
Šechtl	Šechtl	k1gMnSc1	Šechtl
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
fotograf	fotograf	k1gMnSc1	fotograf
a	a	k8xC	a
průkopník	průkopník	k1gMnSc1	průkopník
kinematografie	kinematografie	k1gFnSc2	kinematografie
(	(	kIx(	(
<g/>
†	†	k?	†
6	[number]	k4	6
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1911	[number]	k4	1911
<g/>
)	)	kIx)	)
1853	[number]	k4	1853
–	–	k?	–
Bohumil	Bohumil	k1gMnSc1	Bohumil
Bečka	Bečka	k1gMnSc1	Bečka
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
astronom	astronom	k1gMnSc1	astronom
(	(	kIx(	(
<g/>
†	†	k?	†
25	[number]	k4	25
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1908	[number]	k4	1908
<g/>
)	)	kIx)	)
1855	[number]	k4	1855
–	–	k?	–
Anton	Anton	k1gMnSc1	Anton
Rzehak	Rzehak	k1gMnSc1	Rzehak
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
moravský	moravský	k2eAgMnSc1d1	moravský
geolog	geolog	k1gMnSc1	geolog
a	a	k8xC	a
archeolog	archeolog	k1gMnSc1	archeolog
(	(	kIx(	(
<g/>
†	†	k?	†
31	[number]	k4	31
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
1879	[number]	k4	1879
–	–	k?	–
Svatý	svatý	k2eAgMnSc1d1	svatý
Gorazd	Gorazd	k1gMnSc1	Gorazd
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
pravoslavný	pravoslavný	k2eAgMnSc1d1	pravoslavný
biskup	biskup	k1gMnSc1	biskup
(	(	kIx(	(
<g/>
†	†	k?	†
4	[number]	k4	4
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
1880	[number]	k4	1880
–	–	k?	–
Dominik	Dominik	k1gMnSc1	Dominik
Nejezchleb-Marcha	Nejezchleb-Marcha	k1gMnSc1	Nejezchleb-Marcha
<g/>
,	,	kIx,	,
československý	československý	k2eAgMnSc1d1	československý
politik	politik	k1gMnSc1	politik
(	(	kIx(	(
<g/>
†	†	k?	†
20	[number]	k4	20
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
<g />
.	.	kIx.	.
</s>
<s>
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
1881	[number]	k4	1881
–	–	k?	–
Karl	Karl	k1gMnSc1	Karl
Čermak	Čermak	k1gMnSc1	Čermak
<g/>
,	,	kIx,	,
československý	československý	k2eAgMnSc1d1	československý
politik	politik	k1gMnSc1	politik
německé	německý	k2eAgFnSc2d1	německá
národnosti	národnost	k1gFnSc2	národnost
(	(	kIx(	(
<g/>
†	†	k?	†
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1924	[number]	k4	1924
<g/>
)	)	kIx)	)
1885	[number]	k4	1885
–	–	k?	–
Jan	Jan	k1gMnSc1	Jan
Opočenský	opočenský	k2eAgMnSc1d1	opočenský
<g/>
,	,	kIx,	,
československý	československý	k2eAgMnSc1d1	československý
diplomat	diplomat	k1gMnSc1	diplomat
a	a	k8xC	a
historik	historik	k1gMnSc1	historik
(	(	kIx(	(
<g/>
†	†	k?	†
4	[number]	k4	4
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
1886	[number]	k4	1886
–	–	k?	–
Václav	Václav	k1gMnSc1	Václav
Jiřina	Jiřina	k1gFnSc1	Jiřina
<g/>
,	,	kIx,	,
český	český	k2eAgInSc1d1	český
<g />
.	.	kIx.	.
</s>
<s>
překladatel	překladatel	k1gMnSc1	překladatel
(	(	kIx(	(
<g/>
†	†	k?	†
26	[number]	k4	26
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
1893	[number]	k4	1893
Karel	Karel	k1gMnSc1	Karel
Klapálek	Klapálek	k?	Klapálek
<g/>
,	,	kIx,	,
legionář	legionář	k1gMnSc1	legionář
<g/>
,	,	kIx,	,
odbojář	odbojář	k1gMnSc1	odbojář
<g/>
,	,	kIx,	,
generál	generál	k1gMnSc1	generál
<g/>
,	,	kIx,	,
oběť	oběť	k1gFnSc4	oběť
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
(	(	kIx(	(
<g/>
†	†	k?	†
18	[number]	k4	18
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
Otakar	Otakar	k1gMnSc1	Otakar
Wünsch	Wünsch	k1gInSc4	Wünsch
<g/>
,	,	kIx,	,
člen	člen	k1gInSc4	člen
Petičního	petiční	k2eAgInSc2d1	petiční
výboru	výbor	k1gInSc2	výbor
Věrni	věren	k2eAgMnPc1d1	věren
zůstaneme	zůstat	k5eAaPmIp1nP	zůstat
a	a	k8xC	a
vydavatel	vydavatel	k1gMnSc1	vydavatel
časopisu	časopis	k1gInSc2	časopis
<g />
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
boj	boj	k1gInSc4	boj
(	(	kIx(	(
<g/>
†	†	k?	†
6	[number]	k4	6
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
1900	[number]	k4	1900
–	–	k?	–
Vítězslav	Vítězslav	k1gMnSc1	Vítězslav
Nezval	Nezval	k1gMnSc1	Nezval
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
(	(	kIx(	(
<g/>
†	†	k?	†
6	[number]	k4	6
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
1901	[number]	k4	1901
–	–	k?	–
Bohumil	Bohumil	k1gMnSc1	Bohumil
Turek	Turek	k1gMnSc1	Turek
<g/>
,	,	kIx,	,
motocyklový	motocyklový	k2eAgMnSc1d1	motocyklový
a	a	k8xC	a
automobilový	automobilový	k2eAgMnSc1d1	automobilový
závodník	závodník	k1gMnSc1	závodník
(	(	kIx(	(
<g/>
†	†	k?	†
18	[number]	k4	18
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
1910	[number]	k4	1910
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
Oldřich	Oldřich	k1gMnSc1	Oldřich
Mikulášek	Mikulášek	k1gMnSc1	Mikulášek
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
(	(	kIx(	(
<g/>
†	†	k?	†
13	[number]	k4	13
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
1913	[number]	k4	1913
–	–	k?	–
Otto	Otto	k1gMnSc1	Otto
Slabý	Slabý	k1gMnSc1	Slabý
<g/>
,	,	kIx,	,
lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
histolog	histolog	k1gMnSc1	histolog
<g/>
,	,	kIx,	,
embryolog	embryolog	k1gMnSc1	embryolog
a	a	k8xC	a
entomolog	entomolog	k1gMnSc1	entomolog
(	(	kIx(	(
<g/>
†	†	k?	†
18	[number]	k4	18
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
1920	[number]	k4	1920
–	–	k?	–
Jan	Jan	k1gMnSc1	Jan
Wiener	Wiener	k1gMnSc1	Wiener
<g/>
,	,	kIx,	,
letec	letec	k1gMnSc1	letec
RAF	raf	k0	raf
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
politický	politický	k2eAgMnSc1d1	politický
vězeň	vězeň	k1gMnSc1	vězeň
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
(	(	kIx(	(
<g/>
†	†	k?	†
24	[number]	k4	24
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
1922	[number]	k4	1922
–	–	k?	–
Miroslav	Miroslav	k1gMnSc1	Miroslav
Havel	Havel	k1gMnSc1	Havel
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
sklář	sklář	k1gMnSc1	sklář
(	(	kIx(	(
<g/>
†	†	k?	†
5	[number]	k4	5
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
1924	[number]	k4	1924
–	–	k?	–
Miroslav	Miroslav	k1gMnSc1	Miroslav
Kůra	kůra	k1gFnSc1	kůra
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
tanečník	tanečník	k1gMnSc1	tanečník
a	a	k8xC	a
choreograf	choreograf	k1gMnSc1	choreograf
1928	[number]	k4	1928
–	–	k?	–
Josef	Josef	k1gMnSc1	Josef
Vágner	Vágner	k1gMnSc1	Vágner
<g/>
,	,	kIx,	,
přírodovědec	přírodovědec	k1gMnSc1	přírodovědec
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
cestovatel	cestovatel	k1gMnSc1	cestovatel
<g/>
,	,	kIx,	,
lovec	lovec	k1gMnSc1	lovec
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
(	(	kIx(	(
<g/>
†	†	k?	†
6	[number]	k4	6
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
1931	[number]	k4	1931
–	–	k?	–
Helena	Helena	k1gFnSc1	Helena
Stachová	Stachová	k1gFnSc1	Stachová
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
překladatelka	překladatelka	k1gFnSc1	překladatelka
z	z	k7c2	z
polštiny	polština	k1gFnSc2	polština
1935	[number]	k4	1935
Radim	Radim	k1gMnSc1	Radim
Vašinka	Vašinka	k1gFnSc1	Vašinka
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
divadelní	divadelní	k2eAgMnSc1d1	divadelní
režisér	režisér	k1gMnSc1	režisér
a	a	k8xC	a
dramatik	dramatik	k1gMnSc1	dramatik
(	(	kIx(	(
<g/>
†	†	k?	†
10	[number]	k4	10
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Lubomír	Lubomír	k1gMnSc1	Lubomír
Nácovský	Nácovský	k1gMnSc1	Nácovský
<g/>
,	,	kIx,	,
československý	československý	k2eAgMnSc1d1	československý
olympionik	olympionik	k1gMnSc1	olympionik
<g/>
,	,	kIx,	,
sportovní	sportovní	k2eAgMnSc1d1	sportovní
střelec	střelec	k1gMnSc1	střelec
(	(	kIx(	(
<g/>
†	†	k?	†
10	[number]	k4	10
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
1939	[number]	k4	1939
–	–	k?	–
Karel	Karel	k1gMnSc1	Karel
Raška	Raška	k1gMnSc1	Raška
<g/>
,	,	kIx,	,
česko-americký	českomerický	k2eAgMnSc1d1	česko-americký
lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
biochemik	biochemik	k1gMnSc1	biochemik
<g/>
,	,	kIx,	,
molekulární	molekulární	k2eAgMnSc1d1	molekulární
virolog	virolog	k1gMnSc1	virolog
a	a	k8xC	a
genetik	genetik	k1gMnSc1	genetik
1946	[number]	k4	1946
–	–	k?	–
Drahomír	Drahomír	k1gMnSc1	Drahomír
Koudelka	Koudelka	k1gMnSc1	Koudelka
<g/>
,	,	kIx,	,
československý	československý	k2eAgMnSc1d1	československý
volejbalista	volejbalista	k1gMnSc1	volejbalista
<g/>
,	,	kIx,	,
mistr	mistr	k1gMnSc1	mistr
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
†	†	k?	†
19	[number]	k4	19
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
1947	[number]	k4	1947
Jiří	Jiří	k1gMnSc1	Jiří
Drda	Drda	k1gMnSc1	Drda
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
Lída	Lída	k1gFnSc1	Lída
Rakušanová	Rakušanová	k1gFnSc1	Rakušanová
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
a	a	k8xC	a
novinářka	novinářka	k1gFnSc1	novinářka
1949	[number]	k4	1949
–	–	k?	–
Tomáš	Tomáš	k1gMnSc1	Tomáš
Husák	Husák	k1gMnSc1	Husák
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
diplomat	diplomat	k1gMnSc1	diplomat
1950	[number]	k4	1950
–	–	k?	–
Jiří	Jiří	k1gMnSc1	Jiří
Kubový	Kubový	k2eAgMnSc1d1	Kubový
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
1951	[number]	k4	1951
Anna	Anna	k1gFnSc1	Anna
Röschová	Röschová	k1gFnSc1	Röschová
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
politička	politička	k1gFnSc1	politička
Josef	Josef	k1gMnSc1	Josef
Žáček	Žáček	k1gMnSc1	Žáček
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
<g />
.	.	kIx.	.
</s>
<s>
malíř	malíř	k1gMnSc1	malíř
1953	[number]	k4	1953
–	–	k?	–
Stanislava	Stanislava	k1gFnSc1	Stanislava
Nopová	nopový	k2eAgFnSc1d1	Nopová
<g/>
,	,	kIx,	,
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
<g/>
,	,	kIx,	,
básnířka	básnířka	k1gFnSc1	básnířka
<g/>
,	,	kIx,	,
publicistka	publicistka	k1gFnSc1	publicistka
a	a	k8xC	a
vydavatelka	vydavatelka	k1gFnSc1	vydavatelka
1967	[number]	k4	1967
–	–	k?	–
Martin	Martin	k1gMnSc1	Martin
Zounar	Zounar	k1gMnSc1	Zounar
<g/>
,	,	kIx,	,
filmový	filmový	k2eAgMnSc1d1	filmový
a	a	k8xC	a
televizní	televizní	k2eAgMnSc1d1	televizní
herec	herec	k1gMnSc1	herec
1973	[number]	k4	1973
–	–	k?	–
Magdalena	Magdalena	k1gFnSc1	Magdalena
Kožená	kožený	k2eAgFnSc1d1	kožená
<g/>
,	,	kIx,	,
operní	operní	k2eAgFnSc1d1	operní
pěvkyně	pěvkyně	k1gFnSc1	pěvkyně
<g/>
,	,	kIx,	,
mezzosopranistka	mezzosopranistka	k1gFnSc1	mezzosopranistka
1974	[number]	k4	1974
–	–	k?	–
Vendula	Vendula	k1gFnSc1	Vendula
Vartová-Eliášová	Vartová-Eliášová	k1gFnSc1	Vartová-Eliášová
<g/>
,	,	kIx,	,
básnířka	básnířka	k1gFnSc1	básnířka
<g/>
,	,	kIx,	,
publicistka	publicistka	k1gFnSc1	publicistka
<g/>
,	,	kIx,	,
překladatelka	překladatelka	k1gFnSc1	překladatelka
1989	[number]	k4	1989
–	–	k?	–
Tomáš	Tomáš	k1gMnSc1	Tomáš
Pekhart	Pekhart	k1gInSc1	Pekhart
<g/>
,	,	kIx,	,
fotbalista	fotbalista	k1gMnSc1	fotbalista
1991	[number]	k4	1991
–	–	k?	–
Martin	Martin	k1gMnSc1	Martin
Donutil	donutit	k5eAaPmAgMnS	donutit
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
1478	[number]	k4	1478
–	–	k?	–
Klement	Klement	k1gMnSc1	Klement
VII	VII	kA	VII
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Giulio	Giulio	k1gMnSc1	Giulio
di	di	k?	di
Guiliano	Guiliana	k1gFnSc5	Guiliana
de	de	k?	de
Medici	medik	k1gMnPc1	medik
<g/>
,	,	kIx,	,
papež	papež	k1gMnSc1	papež
(	(	kIx(	(
<g/>
†	†	k?	†
25	[number]	k4	25
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1534	[number]	k4	1534
<g/>
)	)	kIx)	)
1566	[number]	k4	1566
–	–	k?	–
Mehmed	Mehmed	k1gMnSc1	Mehmed
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Spravedlivý	spravedlivý	k2eAgMnSc1d1	spravedlivý
<g/>
,	,	kIx,	,
turecký	turecký	k2eAgMnSc1d1	turecký
sultán	sultán	k1gMnSc1	sultán
(	(	kIx(	(
<g/>
†	†	k?	†
22	[number]	k4	22
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1603	[number]	k4	1603
<g/>
)	)	kIx)	)
1602	[number]	k4	1602
–	–	k?	–
Philippe	Philipp	k1gInSc5	Philipp
de	de	k?	de
Champaigne	Champaign	k1gMnSc5	Champaign
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
malíř	malíř	k1gMnSc1	malíř
(	(	kIx(	(
<g/>
†	†	k?	†
12	[number]	k4	12
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1674	[number]	k4	1674
<g/>
)	)	kIx)	)
1650	[number]	k4	1650
–	–	k?	–
John	John	k1gMnSc1	John
Churchill	Churchill	k1gMnSc1	Churchill
<g/>
,	,	kIx,	,
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Marlborough	Marlborougha	k1gFnPc2	Marlborougha
<g/>
,	,	kIx,	,
anglický	anglický	k2eAgMnSc1d1	anglický
vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
(	(	kIx(	(
<g/>
†	†	k?	†
<g />
.	.	kIx.	.
</s>
<s>
16	[number]	k4	16
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1722	[number]	k4	1722
<g/>
)	)	kIx)	)
1667	[number]	k4	1667
–	–	k?	–
Abraham	Abraham	k1gMnSc1	Abraham
de	de	k?	de
Moivre	Moivr	k1gMnSc5	Moivr
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
matematik	matematik	k1gMnSc1	matematik
žijící	žijící	k2eAgFnSc4d1	žijící
větší	veliký	k2eAgFnSc4d2	veliký
část	část	k1gFnSc4	část
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
(	(	kIx(	(
<g/>
†	†	k?	†
27	[number]	k4	27
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1754	[number]	k4	1754
<g/>
)	)	kIx)	)
1689	[number]	k4	1689
–	–	k?	–
Mary	Mary	k1gFnSc1	Mary
Wortley	Wortlea	k1gFnSc2	Wortlea
Montagu	Montag	k1gInSc2	Montag
<g/>
,	,	kIx,	,
anglická	anglický	k2eAgFnSc1d1	anglická
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
(	(	kIx(	(
<g/>
†	†	k?	†
21	[number]	k4	21
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1762	[number]	k4	1762
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
1700	[number]	k4	1700
–	–	k?	–
Nikolaus	Nikolaus	k1gInSc1	Nikolaus
Ludwig	Ludwig	k1gInSc1	Ludwig
von	von	k1gInSc1	von
Zinzendorf	Zinzendorf	k1gInSc4	Zinzendorf
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
náboženský	náboženský	k2eAgMnSc1d1	náboženský
a	a	k8xC	a
sociální	sociální	k2eAgMnSc1d1	sociální
reformátor	reformátor	k1gMnSc1	reformátor
(	(	kIx(	(
<g/>
†	†	k?	†
9	[number]	k4	9
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1760	[number]	k4	1760
<g/>
)	)	kIx)	)
1733	[number]	k4	1733
–	–	k?	–
Pieter	Pieter	k1gMnSc1	Pieter
Boddaert	Boddaert	k1gMnSc1	Boddaert
<g/>
,	,	kIx,	,
nizozemský	nizozemský	k2eAgMnSc1d1	nizozemský
lékař	lékař	k1gMnSc1	lékař
a	a	k8xC	a
zoolog	zoolog	k1gMnSc1	zoolog
(	(	kIx(	(
<g/>
†	†	k?	†
6	[number]	k4	6
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1795	[number]	k4	1795
<g/>
)	)	kIx)	)
1796	[number]	k4	1796
–	–	k?	–
Alois	Alois	k1gMnSc1	Alois
II	II	kA	II
<g/>
<g />
.	.	kIx.	.
</s>
<s>
z	z	k7c2	z
Lichtenštejna	Lichtenštejn	k1gInSc2	Lichtenštejn
<g/>
,	,	kIx,	,
lichtenštejnský	lichtenštejnský	k2eAgMnSc1d1	lichtenštejnský
kníže	kníže	k1gMnSc1	kníže
(	(	kIx(	(
<g/>
†	†	k?	†
12	[number]	k4	12
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1858	[number]	k4	1858
<g/>
)	)	kIx)	)
1799	[number]	k4	1799
–	–	k?	–
Felipe	Felip	k1gInSc5	Felip
Poey	Poea	k1gFnSc2	Poea
<g/>
,	,	kIx,	,
kubánský	kubánský	k2eAgMnSc1d1	kubánský
zoolog	zoolog	k1gMnSc1	zoolog
(	(	kIx(	(
<g/>
†	†	k?	†
28	[number]	k4	28
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1891	[number]	k4	1891
<g/>
)	)	kIx)	)
1807	[number]	k4	1807
–	–	k?	–
Anton	anton	k1gInSc1	anton
von	von	k1gInSc1	von
Hye	Hye	k1gFnSc1	Hye
<g/>
,	,	kIx,	,
ministr	ministr	k1gMnSc1	ministr
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
a	a	k8xC	a
ministr	ministr	k1gMnSc1	ministr
kultu	kult	k1gInSc2	kult
a	a	k8xC	a
vyučování	vyučování	k1gNnSc4	vyučování
Předlitavska	Předlitavsko	k1gNnSc2	Předlitavsko
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
†	†	k?	†
8	[number]	k4	8
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1894	[number]	k4	1894
<g/>
)	)	kIx)	)
1814	[number]	k4	1814
–	–	k?	–
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Engerth	Engerth	k1gMnSc1	Engerth
<g/>
,	,	kIx,	,
rakouský	rakouský	k2eAgMnSc1d1	rakouský
stavitel	stavitel	k1gMnSc1	stavitel
železnic	železnice	k1gFnPc2	železnice
a	a	k8xC	a
konstruktér	konstruktér	k1gMnSc1	konstruktér
lokomotiv	lokomotiva	k1gFnPc2	lokomotiva
(	(	kIx(	(
<g/>
†	†	k?	†
4	[number]	k4	4
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1884	[number]	k4	1884
<g/>
)	)	kIx)	)
1863	[number]	k4	1863
–	–	k?	–
Bob	bob	k1gInSc1	bob
Fitzsimmons	Fitzsimmons	k1gInSc1	Fitzsimmons
<g/>
,	,	kIx,	,
britský	britský	k2eAgInSc1d1	britský
boxer	boxer	k1gInSc1	boxer
(	(	kIx(	(
<g/>
†	†	k?	†
22	[number]	k4	22
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
1865	[number]	k4	1865
–	–	k?	–
Robert	Robert	k1gMnSc1	Robert
William	William	k1gInSc1	William
Chambers	Chambers	k1gInSc4	Chambers
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
malíř	malíř	k1gMnSc1	malíř
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
(	(	kIx(	(
<g/>
†	†	k?	†
16	[number]	k4	16
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
1867	[number]	k4	1867
Marie	Marie	k1gFnSc1	Marie
z	z	k7c2	z
Tecku	Teck	k1gInSc2	Teck
<g/>
,	,	kIx,	,
britská	britský	k2eAgFnSc1d1	britská
královna	královna	k1gFnSc1	královna
<g/>
,	,	kIx,	,
manželka	manželka	k1gFnSc1	manželka
Jiřího	Jiří	k1gMnSc2	Jiří
V.	V.	kA	V.
(	(	kIx(	(
<g/>
†	†	k?	†
24	[number]	k4	24
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
Ernst	Ernst	k1gMnSc1	Ernst
Sellin	Sellin	k2eAgMnSc1d1	Sellin
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
luterský	luterský	k2eAgMnSc1d1	luterský
<g />
.	.	kIx.	.
</s>
<s>
profesor	profesor	k1gMnSc1	profesor
Starého	Starého	k2eAgInSc2d1	Starého
zákona	zákon	k1gInSc2	zákon
a	a	k8xC	a
archeologie	archeologie	k1gFnSc2	archeologie
(	(	kIx(	(
<g/>
†	†	k?	†
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
1871	[number]	k4	1871
–	–	k?	–
Camille	Camille	k1gNnSc2	Camille
Huysmans	Huysmansa	k1gFnPc2	Huysmansa
<g/>
,	,	kIx,	,
belgický	belgický	k2eAgMnSc1d1	belgický
politik	politik	k1gMnSc1	politik
(	(	kIx(	(
<g/>
†	†	k?	†
23	[number]	k4	23
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
1872	[number]	k4	1872
–	–	k?	–
Stanisław	Stanisław	k1gMnSc1	Stanisław
Haller	Haller	k1gMnSc1	Haller
de	de	k?	de
Hallenburg	Hallenburg	k1gMnSc1	Hallenburg
<g/>
,	,	kIx,	,
polský	polský	k2eAgMnSc1d1	polský
generál	generál	k1gMnSc1	generál
(	(	kIx(	(
<g/>
†	†	k?	†
květen	květen	k1gInSc4	květen
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
1874	[number]	k4	1874
–	–	k?	–
Laura	Laura	k1gFnSc1	Laura
Montoya	Montoy	k1gInSc2	Montoy
Upegui	Upegu	k1gFnSc2	Upegu
<g/>
,	,	kIx,	,
kolumbijská	kolumbijský	k2eAgFnSc1d1	kolumbijská
římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
řeholnice	řeholnice	k1gFnSc1	řeholnice
a	a	k8xC	a
světice	světice	k1gFnSc1	světice
(	(	kIx(	(
<g/>
†	†	k?	†
21	[number]	k4	21
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
1878	[number]	k4	1878
–	–	k?	–
Isadora	Isadora	k1gFnSc1	Isadora
Duncanová	Duncanová	k1gFnSc1	Duncanová
<g/>
,	,	kIx,	,
americká	americký	k2eAgFnSc1d1	americká
tanečnice	tanečnice	k1gFnSc1	tanečnice
(	(	kIx(	(
<g/>
†	†	k?	†
14	[number]	k4	14
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1927	[number]	k4	1927
<g/>
)	)	kIx)	)
1881	[number]	k4	1881
–	–	k?	–
Valerian	Valeriany	k1gInPc2	Valeriany
Albanov	Albanovo	k1gNnPc2	Albanovo
<g/>
,	,	kIx,	,
ruský	ruský	k2eAgMnSc1d1	ruský
polárník	polárník	k1gMnSc1	polárník
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
†	†	k?	†
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
1883	[number]	k4	1883
–	–	k?	–
Peter	Peter	k1gMnSc1	Peter
Kürten	Kürten	k2eAgMnSc1d1	Kürten
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
sériový	sériový	k2eAgMnSc1d1	sériový
vrah	vrah	k1gMnSc1	vrah
(	(	kIx(	(
<g/>
†	†	k?	†
2	[number]	k4	2
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
1885	[number]	k4	1885
–	–	k?	–
Maurice	Maurika	k1gFnSc3	Maurika
Dekobra	Dekobra	k1gMnSc1	Dekobra
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
spisovatel	spisovatel	k1gMnSc1	spisovatel
(	(	kIx(	(
<g/>
†	†	k?	†
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
1886	[number]	k4	1886
–	–	k?	–
Al	ala	k1gFnPc2	ala
Jolson	Jolson	k1gMnSc1	Jolson
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
zpěvák	zpěvák	k1gMnSc1	zpěvák
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
kabaretní	kabaretní	k2eAgMnSc1d1	kabaretní
herec	herec	k1gMnSc1	herec
(	(	kIx(	(
<g/>
†	†	k?	†
23	[number]	k4	23
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
1890	[number]	k4	1890
–	–	k?	–
Samuil	Samuil	k1gMnSc1	Samuil
Fejnberg	Fejnberg	k1gMnSc1	Fejnberg
<g/>
,	,	kIx,	,
ruský	ruský	k2eAgMnSc1d1	ruský
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
klavírista	klavírista	k1gMnSc1	klavírista
(	(	kIx(	(
<g/>
†	†	k?	†
22	[number]	k4	22
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
1891	[number]	k4	1891
Janez	Janez	k1gInSc1	Janez
Jalen	Jalen	k2eAgInSc1d1	Jalen
<g/>
,	,	kIx,	,
slovinský	slovinský	k2eAgMnSc1d1	slovinský
kněz	kněz	k1gMnSc1	kněz
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
(	(	kIx(	(
<g/>
†	†	k?	†
12	[number]	k4	12
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1966	[number]	k4	1966
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Paul	Paul	k1gMnSc1	Paul
Lukas	Lukas	k1gMnSc1	Lukas
<g/>
,	,	kIx,	,
maďarsko-americký	maďarskomerický	k2eAgMnSc1d1	maďarsko-americký
filmový	filmový	k2eAgMnSc1d1	filmový
herec	herec	k1gMnSc1	herec
(	(	kIx(	(
<g/>
†	†	k?	†
15	[number]	k4	15
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
1895	[number]	k4	1895
–	–	k?	–
Dorothea	Dorothe	k2eAgFnSc1d1	Dorothea
Langeová	Langeová	k1gFnSc1	Langeová
<g/>
,	,	kIx,	,
americká	americký	k2eAgFnSc1d1	americká
fotografka	fotografka	k1gFnSc1	fotografka
(	(	kIx(	(
<g/>
†	†	k?	†
11	[number]	k4	11
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
1907	[number]	k4	1907
–	–	k?	–
John	John	k1gMnSc1	John
Wayne	Wayn	k1gInSc5	Wayn
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
herec	herec	k1gMnSc1	herec
(	(	kIx(	(
<g/>
†	†	k?	†
11	[number]	k4	11
<g/>
<g />
.	.	kIx.	.
</s>
<s>
června	červen	k1gInSc2	červen
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
1910	[number]	k4	1910
–	–	k?	–
Imi	Imi	k1gMnSc1	Imi
Lichtenfeld	Lichtenfeld	k1gMnSc1	Lichtenfeld
<g/>
,	,	kIx,	,
tvůrce	tvůrce	k1gMnSc1	tvůrce
izraelského	izraelský	k2eAgInSc2d1	izraelský
bojového	bojový	k2eAgInSc2d1	bojový
systému	systém	k1gInSc2	systém
Krav	kráva	k1gFnPc2	kráva
maga	maga	k1gFnSc1	maga
(	(	kIx(	(
<g/>
†	†	k?	†
9	[number]	k4	9
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
1912	[number]	k4	1912
–	–	k?	–
János	János	k1gMnSc1	János
Kádár	Kádár	k1gMnSc1	Kádár
<g/>
,	,	kIx,	,
premiér	premiér	k1gMnSc1	premiér
Maďarské	maďarský	k2eAgFnSc2d1	maďarská
lidové	lidový	k2eAgFnSc2d1	lidová
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
†	†	k?	†
6	[number]	k4	6
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
1913	[number]	k4	1913
–	–	k?	–
Peter	Peter	k1gMnSc1	Peter
<g />
.	.	kIx.	.
</s>
<s>
Cushing	Cushing	k1gInSc1	Cushing
<g/>
,	,	kIx,	,
britský	britský	k2eAgMnSc1d1	britský
herec	herec	k1gMnSc1	herec
(	(	kIx(	(
<g/>
†	†	k?	†
11	[number]	k4	11
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
1919	[number]	k4	1919
–	–	k?	–
Rubén	Rubén	k1gInSc1	Rubén
González	González	k1gInSc1	González
<g/>
,	,	kIx,	,
kubánský	kubánský	k2eAgMnSc1d1	kubánský
pianista	pianista	k1gMnSc1	pianista
(	(	kIx(	(
<g/>
†	†	k?	†
8	[number]	k4	8
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
1920	[number]	k4	1920
–	–	k?	–
Peggy	Pegga	k1gFnSc2	Pegga
Lee	Lea	k1gFnSc3	Lea
<g/>
,	,	kIx,	,
americká	americký	k2eAgFnSc1d1	americká
jazzová	jazzový	k2eAgFnSc1d1	jazzová
a	a	k8xC	a
popová	popový	k2eAgFnSc1d1	popová
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
(	(	kIx(	(
<g/>
†	†	k?	†
21	[number]	k4	21
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
ledna	leden	k1gInSc2	leden
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
1922	[number]	k4	1922
–	–	k?	–
Marian	Marian	k1gMnSc1	Marian
Reniak	Reniak	k1gMnSc1	Reniak
<g/>
,	,	kIx,	,
polský	polský	k2eAgMnSc1d1	polský
spisovatel	spisovatel	k1gMnSc1	spisovatel
(	(	kIx(	(
<g/>
†	†	k?	†
8	[number]	k4	8
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
1923	[number]	k4	1923
–	–	k?	–
Harry	Harra	k1gFnSc2	Harra
Gordon	Gordon	k1gMnSc1	Gordon
Johnson	Johnson	k1gMnSc1	Johnson
<g/>
,	,	kIx,	,
kanadský	kanadský	k2eAgMnSc1d1	kanadský
ekonom	ekonom	k1gMnSc1	ekonom
(	(	kIx(	(
<g/>
†	†	k?	†
9	[number]	k4	9
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
1924	[number]	k4	1924
–	–	k?	–
Mike	Mike	k1gFnPc2	Mike
Bongiorno	Bongiorno	k1gNnSc1	Bongiorno
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
a	a	k8xC	a
italský	italský	k2eAgMnSc1d1	italský
<g />
.	.	kIx.	.
</s>
<s>
televizní	televizní	k2eAgInSc1d1	televizní
moderátor	moderátor	k1gInSc1	moderátor
(	(	kIx(	(
<g/>
†	†	k?	†
8	[number]	k4	8
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
1926	[number]	k4	1926
–	–	k?	–
Miles	Miles	k1gInSc1	Miles
Davis	Davis	k1gInSc1	Davis
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
jazzový	jazzový	k2eAgMnSc1d1	jazzový
trumpetista	trumpetista	k1gMnSc1	trumpetista
<g/>
,	,	kIx,	,
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
kapelník	kapelník	k1gMnSc1	kapelník
(	(	kIx(	(
<g/>
†	†	k?	†
28	[number]	k4	28
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
1927	[number]	k4	1927
–	–	k?	–
Endel	Endel	k1gInSc1	Endel
Tulving	Tulving	k1gInSc1	Tulving
<g/>
,	,	kIx,	,
estonsko-kanadský	estonskoanadský	k2eAgMnSc1d1	estonsko-kanadský
a	a	k8xC	a
psycholog	psycholog	k1gMnSc1	psycholog
1928	[number]	k4	1928
–	–	k?	–
Jack	Jack	k1gMnSc1	Jack
Kevorkian	Kevorkian	k1gMnSc1	Kevorkian
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
propagátor	propagátor	k1gMnSc1	propagátor
eutanazie	eutanazie	k1gFnSc2	eutanazie
(	(	kIx(	(
<g/>
†	†	k?	†
3	[number]	k4	3
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
1931	[number]	k4	1931
–	–	k?	–
Sven	Sven	k1gInSc1	Sven
Delblanc	Delblanc	k1gInSc1	Delblanc
<g/>
,	,	kIx,	,
švédský	švédský	k2eAgMnSc1d1	švédský
spisovatel	spisovatel	k1gMnSc1	spisovatel
(	(	kIx(	(
<g/>
†	†	k?	†
15	[number]	k4	15
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
1934	[number]	k4	1934
−	−	k?	−
Bernard	Bernard	k1gMnSc1	Bernard
Vitet	Vitet	k1gMnSc1	Vitet
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
trumpetista	trumpetista	k1gMnSc1	trumpetista
(	(	kIx(	(
<g/>
†	†	k?	†
3	[number]	k4	3
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g />
.	.	kIx.	.
</s>
<s>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
1936	[number]	k4	1936
–	–	k?	–
Natalja	Natalja	k1gFnSc1	Natalja
Gorbaněvská	Gorbaněvský	k2eAgFnSc1d1	Gorbaněvská
<g/>
,	,	kIx,	,
ruská	ruský	k2eAgFnSc1d1	ruská
básnířka	básnířka	k1gFnSc1	básnířka
<g/>
,	,	kIx,	,
disidentka	disidentka	k1gFnSc1	disidentka
(	(	kIx(	(
<g/>
†	†	k?	†
29	[number]	k4	29
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
1938	[number]	k4	1938
–	–	k?	–
Ivan	Ivan	k1gMnSc1	Ivan
Pop	pop	k1gMnSc1	pop
<g/>
,	,	kIx,	,
rusínský	rusínský	k2eAgMnSc1d1	rusínský
historik	historik	k1gMnSc1	historik
<g/>
,	,	kIx,	,
bohemista	bohemista	k1gMnSc1	bohemista
<g/>
,	,	kIx,	,
rusinista	rusinista	k1gMnSc1	rusinista
<g/>
,	,	kIx,	,
kulturolog	kulturolog	k1gMnSc1	kulturolog
1939	[number]	k4	1939
–	–	k?	–
Merab	Merab	k1gInSc1	Merab
Kostava	Kostava	k1gFnSc1	Kostava
<g/>
,	,	kIx,	,
gruzínský	gruzínský	k2eAgMnSc1d1	gruzínský
disident	disident	k1gMnSc1	disident
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
hudebník	hudebník	k1gMnSc1	hudebník
(	(	kIx(	(
<g/>
†	†	k?	†
13	[number]	k4	13
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
1940	[number]	k4	1940
−	−	k?	−
Levon	Levon	k1gMnSc1	Levon
Helm	Helm	k1gMnSc1	Helm
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
hudebník	hudebník	k1gMnSc1	hudebník
(	(	kIx(	(
<g/>
†	†	k?	†
19	[number]	k4	19
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
1942	[number]	k4	1942
–	–	k?	–
Dušan	Dušan	k1gMnSc1	Dušan
Grúň	Grúň	k?	Grúň
<g/>
,	,	kIx,	,
slovenský	slovenský	k2eAgMnSc1d1	slovenský
zpěvák	zpěvák	k1gMnSc1	zpěvák
1944	[number]	k4	1944
–	–	k?	–
Verden	Verdna	k1gFnPc2	Verdna
Allen	Allen	k1gMnSc1	Allen
<g/>
,	,	kIx,	,
britský	britský	k2eAgMnSc1d1	britský
varhaník	varhaník	k1gMnSc1	varhaník
1946	[number]	k4	1946
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
Mick	Mick	k1gMnSc1	Mick
Ronson	Ronson	k1gMnSc1	Ronson
<g/>
,	,	kIx,	,
britský	britský	k2eAgMnSc1d1	britský
rockový	rockový	k2eAgMnSc1d1	rockový
kytarista	kytarista	k1gMnSc1	kytarista
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
skladatel	skladatel	k1gMnSc1	skladatel
(	(	kIx(	(
<g/>
†	†	k?	†
29	[number]	k4	29
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
1948	[number]	k4	1948
–	–	k?	–
Stevie	Stevie	k1gFnSc2	Stevie
Nicks	Nicksa	k1gFnPc2	Nicksa
<g/>
,	,	kIx,	,
americká	americký	k2eAgFnSc1d1	americká
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
a	a	k8xC	a
skladatelka	skladatelka	k1gFnSc1	skladatelka
1949	[number]	k4	1949
–	–	k?	–
Ward	Ward	k1gInSc1	Ward
Cunningham	Cunningham	k1gInSc1	Cunningham
<g/>
,	,	kIx,	,
americký	americký	k2eAgInSc1d1	americký
počítačový	počítačový	k2eAgInSc1d1	počítačový
programátor	programátor	k1gInSc1	programátor
1951	[number]	k4	1951
−	−	k?	−
Sally	Salla	k1gFnSc2	Salla
Rideová	Rideový	k2eAgFnSc1d1	Rideový
<g/>
,	,	kIx,	,
americká	americký	k2eAgFnSc1d1	americká
astronautka	astronautka	k1gFnSc1	astronautka
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
†	†	k?	†
23	[number]	k4	23
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
1953	[number]	k4	1953
−	−	k?	−
Richard	Richard	k1gMnSc1	Richard
Sohl	Sohl	k1gMnSc1	Sohl
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
hudebník	hudebník	k1gMnSc1	hudebník
(	(	kIx(	(
<g/>
†	†	k?	†
3	[number]	k4	3
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
1954	[number]	k4	1954
–	–	k?	–
Alan	Alan	k1gMnSc1	Alan
Hollinghurst	Hollinghurst	k1gMnSc1	Hollinghurst
<g/>
,	,	kIx,	,
anglický	anglický	k2eAgMnSc1d1	anglický
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
překladatel	překladatel	k1gMnSc1	překladatel
1955	[number]	k4	1955
–	–	k?	–
Adam	Adam	k1gMnSc1	Adam
Curtis	Curtis	k1gFnSc2	Curtis
<g/>
,	,	kIx,	,
britský	britský	k2eAgMnSc1d1	britský
televizní	televizní	k2eAgMnSc1d1	televizní
dokumentarista	dokumentarista	k1gMnSc1	dokumentarista
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
1964	[number]	k4	1964
–	–	k?	–
Lenny	Lenny	k?	Lenny
Kravitz	Kravitz	k1gMnSc1	Kravitz
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
rockový	rockový	k2eAgMnSc1d1	rockový
zpěvák	zpěvák	k1gMnSc1	zpěvák
1966	[number]	k4	1966
−	−	k?	−
Helena	Helena	k1gFnSc1	Helena
Bonham	Bonham	k1gInSc1	Bonham
Carter	Carter	k1gInSc1	Carter
<g/>
,	,	kIx,	,
anglická	anglický	k2eAgFnSc1d1	anglická
herečka	herečka	k1gFnSc1	herečka
1812	[number]	k4	1812
–	–	k?	–
Johann	Johann	k1gMnSc1	Johann
Zoph	Zoph	k1gMnSc1	Zoph
<g/>
,	,	kIx,	,
rakouský	rakouský	k2eAgMnSc1d1	rakouský
císařsko-královský	císařskorálovský	k2eAgMnSc1d1	císařsko-královský
podmaršál	podmaršál	k1gMnSc1	podmaršál
(	(	kIx(	(
<g/>
*	*	kIx~	*
15	[number]	k4	15
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1738	[number]	k4	1738
<g/>
)	)	kIx)	)
1866	[number]	k4	1866
–	–	k?	–
Ignác	Ignác	k1gMnSc1	Ignác
Leopold	Leopold	k1gMnSc1	Leopold
Kober	kobra	k1gFnPc2	kobra
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
nakladatel	nakladatel	k1gMnSc1	nakladatel
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
*	*	kIx~	*
6	[number]	k4	6
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1825	[number]	k4	1825
<g/>
)	)	kIx)	)
1876	[number]	k4	1876
–	–	k?	–
František	František	k1gMnSc1	František
Palacký	Palacký	k1gMnSc1	Palacký
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
(	(	kIx(	(
<g/>
*	*	kIx~	*
14	[number]	k4	14
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1798	[number]	k4	1798
<g/>
)	)	kIx)	)
1887	[number]	k4	1887
–	–	k?	–
Jindřich	Jindřich	k1gMnSc1	Jindřich
Blažej	Blažej	k1gMnSc1	Blažej
Vávra	Vávra	k1gMnSc1	Vávra
<g/>
,	,	kIx,	,
lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
cestovatel	cestovatel	k1gMnSc1	cestovatel
a	a	k8xC	a
botanik	botanik	k1gMnSc1	botanik
(	(	kIx(	(
<g/>
*	*	kIx~	*
2	[number]	k4	2
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
<g />
.	.	kIx.	.
</s>
<s>
1831	[number]	k4	1831
<g/>
)	)	kIx)	)
1899	[number]	k4	1899
–	–	k?	–
Karel	Karel	k1gMnSc1	Karel
Gerber	gerbera	k1gFnPc2	gerbera
<g/>
,	,	kIx,	,
odborník	odborník	k1gMnSc1	odborník
na	na	k7c6	na
pojišťovnictví	pojišťovnictví	k1gNnSc6	pojišťovnictví
(	(	kIx(	(
<g/>
*	*	kIx~	*
1839	[number]	k4	1839
<g/>
)	)	kIx)	)
1900	[number]	k4	1900
–	–	k?	–
Anton	Anton	k1gMnSc1	Anton
Schobloch	Schobloch	k1gMnSc1	Schobloch
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
podnikatel	podnikatel	k1gMnSc1	podnikatel
(	(	kIx(	(
<g/>
*	*	kIx~	*
5	[number]	k4	5
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1835	[number]	k4	1835
<g/>
)	)	kIx)	)
1911	[number]	k4	1911
–	–	k?	–
Alois	Alois	k1gMnSc1	Alois
Strnad	Strnad	k1gMnSc1	Strnad
<g/>
,	,	kIx,	,
matematik	matematik	k1gMnSc1	matematik
a	a	k8xC	a
geometr	geometr	k1gMnSc1	geometr
(	(	kIx(	(
<g/>
*	*	kIx~	*
1	[number]	k4	1
<g/>
<g />
.	.	kIx.	.
</s>
<s>
října	říjen	k1gInSc2	říjen
1852	[number]	k4	1852
<g/>
)	)	kIx)	)
1925	[number]	k4	1925
–	–	k?	–
Peter	Peter	k1gMnSc1	Peter
Riedl	Riedl	k1gMnSc1	Riedl
<g/>
,	,	kIx,	,
pražský	pražský	k2eAgMnSc1d1	pražský
německý	německý	k2eAgMnSc1d1	německý
novinář	novinář	k1gMnSc1	novinář
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
(	(	kIx(	(
<g/>
*	*	kIx~	*
19	[number]	k4	19
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1853	[number]	k4	1853
<g/>
)	)	kIx)	)
1926	[number]	k4	1926
–	–	k?	–
Josef	Josef	k1gMnSc1	Josef
Kuchař	Kuchař	k1gMnSc1	Kuchař
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
(	(	kIx(	(
<g/>
*	*	kIx~	*
14	[number]	k4	14
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1847	[number]	k4	1847
<g/>
)	)	kIx)	)
1930	[number]	k4	1930
–	–	k?	–
Josef	Josef	k1gMnSc1	Josef
Vančura	Vančura	k1gMnSc1	Vančura
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
<g />
.	.	kIx.	.
</s>
<s>
právník	právník	k1gMnSc1	právník
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
římského	římský	k2eAgNnSc2d1	římské
práva	právo	k1gNnSc2	právo
(	(	kIx(	(
<g/>
*	*	kIx~	*
21	[number]	k4	21
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1870	[number]	k4	1870
<g/>
)	)	kIx)	)
1937	[number]	k4	1937
Jan	Jan	k1gMnSc1	Jan
Minařík	Minařík	k1gMnSc1	Minařík
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
malíř	malíř	k1gMnSc1	malíř
(	(	kIx(	(
<g/>
*	*	kIx~	*
15	[number]	k4	15
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1862	[number]	k4	1862
<g/>
)	)	kIx)	)
Karel	Karel	k1gMnSc1	Karel
Kramář	kramář	k1gMnSc1	kramář
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
(	(	kIx(	(
<g/>
*	*	kIx~	*
27	[number]	k4	27
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1860	[number]	k4	1860
<g/>
)	)	kIx)	)
1943	[number]	k4	1943
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
Pospíšilová	Pospíšilová	k1gFnSc1	Pospíšilová
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
(	(	kIx(	(
<g/>
*	*	kIx~	*
23	[number]	k4	23
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1862	[number]	k4	1862
<g/>
)	)	kIx)	)
1946	[number]	k4	1946
–	–	k?	–
Václav	Václav	k1gMnSc1	Václav
Jiřina	Jiřina	k1gFnSc1	Jiřina
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
překladatel	překladatel	k1gMnSc1	překladatel
(	(	kIx(	(
<g/>
*	*	kIx~	*
26	[number]	k4	26
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1886	[number]	k4	1886
<g/>
)	)	kIx)	)
1947	[number]	k4	1947
–	–	k?	–
Romuald	Romuald	k1gInSc1	Romuald
Rudolf	Rudolf	k1gMnSc1	Rudolf
Perlík	perlík	k1gInSc1	perlík
<g/>
,	,	kIx,	,
archivář	archivář	k1gMnSc1	archivář
na	na	k7c6	na
Strahově	Strahov	k1gInSc6	Strahov
a	a	k8xC	a
historik	historik	k1gMnSc1	historik
církevní	církevní	k2eAgFnSc2d1	církevní
hudby	hudba	k1gFnSc2	hudba
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
*	*	kIx~	*
11	[number]	k4	11
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1882	[number]	k4	1882
<g/>
)	)	kIx)	)
1952	[number]	k4	1952
–	–	k?	–
Jindřich	Jindřich	k1gMnSc1	Jindřich
Ladislav	Ladislav	k1gMnSc1	Ladislav
Barvíř	barvíř	k1gMnSc1	barvíř
<g/>
,	,	kIx,	,
geolog	geolog	k1gMnSc1	geolog
(	(	kIx(	(
<g/>
*	*	kIx~	*
15	[number]	k4	15
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1863	[number]	k4	1863
<g/>
)	)	kIx)	)
1955	[number]	k4	1955
–	–	k?	–
Ján	Ján	k1gMnSc1	Ján
Halla	Halla	k1gMnSc1	Halla
<g/>
,	,	kIx,	,
slovenský	slovenský	k2eAgMnSc1d1	slovenský
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
československý	československý	k2eAgMnSc1d1	československý
meziválečný	meziválečný	k2eAgMnSc1d1	meziválečný
politik	politik	k1gMnSc1	politik
(	(	kIx(	(
<g/>
*	*	kIx~	*
14	[number]	k4	14
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1885	[number]	k4	1885
<g/>
)	)	kIx)	)
1964	[number]	k4	1964
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
Pala	Pala	k1gMnSc1	Pala
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
vědec	vědec	k1gMnSc1	vědec
(	(	kIx(	(
<g/>
*	*	kIx~	*
9	[number]	k4	9
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1887	[number]	k4	1887
<g/>
)	)	kIx)	)
1965	[number]	k4	1965
–	–	k?	–
Václav	Václav	k1gMnSc1	Václav
Machek	Machek	k1gMnSc1	Machek
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
jazykovědec	jazykovědec	k1gMnSc1	jazykovědec
(	(	kIx(	(
<g/>
*	*	kIx~	*
8	[number]	k4	8
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1894	[number]	k4	1894
<g/>
)	)	kIx)	)
1968	[number]	k4	1968
–	–	k?	–
Vlasta	Vlasta	k1gFnSc1	Vlasta
Hilská	Hilská	k1gFnSc1	Hilská
<g/>
,	,	kIx,	,
profesorka	profesorka	k1gFnSc1	profesorka
japonské	japonský	k2eAgFnSc2d1	japonská
filologie	filologie	k1gFnSc2	filologie
a	a	k8xC	a
dějin	dějiny	k1gFnPc2	dějiny
(	(	kIx(	(
<g/>
*	*	kIx~	*
22	[number]	k4	22
<g/>
<g />
.	.	kIx.	.
</s>
<s>
června	červen	k1gInSc2	červen
1909	[number]	k4	1909
<g/>
)	)	kIx)	)
1970	[number]	k4	1970
–	–	k?	–
Ladislav	Ladislav	k1gMnSc1	Ladislav
Lábek	Lábek	k1gMnSc1	Lábek
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
historik	historik	k1gMnSc1	historik
a	a	k8xC	a
muzeolog	muzeolog	k1gMnSc1	muzeolog
(	(	kIx(	(
<g/>
*	*	kIx~	*
27	[number]	k4	27
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1882	[number]	k4	1882
<g/>
)	)	kIx)	)
1973	[number]	k4	1973
–	–	k?	–
Ladislav	Ladislav	k1gMnSc1	Ladislav
Žák	Žák	k1gMnSc1	Žák
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
malíř	malíř	k1gMnSc1	malíř
a	a	k8xC	a
architekt	architekt	k1gMnSc1	architekt
(	(	kIx(	(
<g/>
*	*	kIx~	*
25	[number]	k4	25
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1900	[number]	k4	1900
<g/>
)	)	kIx)	)
1982	[number]	k4	1982
–	–	k?	–
Bohumil	Bohumil	k1gMnSc1	Bohumil
Janda	Janda	k1gMnSc1	Janda
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
nakladatel	nakladatel	k1gMnSc1	nakladatel
a	a	k8xC	a
lexikograf	lexikograf	k1gMnSc1	lexikograf
(	(	kIx(	(
<g/>
*	*	kIx~	*
20	[number]	k4	20
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1900	[number]	k4	1900
<g/>
)	)	kIx)	)
1991	[number]	k4	1991
–	–	k?	–
Konrád	Konrád	k1gMnSc1	Konrád
Babraj	Babraj	k?	Babraj
<g/>
,	,	kIx,	,
sochař	sochař	k1gMnSc1	sochař
(	(	kIx(	(
<g/>
*	*	kIx~	*
22	[number]	k4	22
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
2001	[number]	k4	2001
–	–	k?	–
Josef	Josef	k1gMnSc1	Josef
Kratochvil	kratochvíle	k1gFnPc2	kratochvíle
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
skautský	skautský	k2eAgMnSc1d1	skautský
činitel	činitel	k1gMnSc1	činitel
a	a	k8xC	a
vysokoškolský	vysokoškolský	k2eAgMnSc1d1	vysokoškolský
pedagog	pedagog	k1gMnSc1	pedagog
(	(	kIx(	(
<g/>
*	*	kIx~	*
1	[number]	k4	1
<g />
.	.	kIx.	.
</s>
<s>
<g/>
ledna	leden	k1gInSc2	leden
1915	[number]	k4	1915
<g/>
)	)	kIx)	)
2002	[number]	k4	2002
–	–	k?	–
Eduard	Eduard	k1gMnSc1	Eduard
Tomáš	Tomáš	k1gMnSc1	Tomáš
<g/>
,	,	kIx,	,
mystik	mystik	k1gMnSc1	mystik
<g/>
,	,	kIx,	,
jogín	jogín	k1gMnSc1	jogín
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
(	(	kIx(	(
<g/>
*	*	kIx~	*
25	[number]	k4	25
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1908	[number]	k4	1908
<g/>
)	)	kIx)	)
2005	[number]	k4	2005
–	–	k?	–
Roman	Roman	k1gMnSc1	Roman
Hemala	Hemala	k1gFnSc1	Hemala
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
(	(	kIx(	(
<g/>
*	*	kIx~	*
11	[number]	k4	11
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
2006	[number]	k4	2006
–	–	k?	–
Štěpán	Štěpán	k1gMnSc1	Štěpán
Koníček	Koníček	k1gMnSc1	Koníček
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
dirigent	dirigent	k1gMnSc1	dirigent
a	a	k8xC	a
hudební	hudební	k2eAgMnSc1d1	hudební
dramaturg	dramaturg	k1gMnSc1	dramaturg
(	(	kIx(	(
<g/>
*	*	kIx~	*
7	[number]	k4	7
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1928	[number]	k4	1928
<g/>
)	)	kIx)	)
2007	[number]	k4	2007
–	–	k?	–
Antonín	Antonín	k1gMnSc1	Antonín
Lauterbach	Lauterbach	k1gMnSc1	Lauterbach
<g/>
,	,	kIx,	,
pedagog	pedagog	k1gMnSc1	pedagog
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
divadelník	divadelník	k1gMnSc1	divadelník
(	(	kIx(	(
<g/>
*	*	kIx~	*
2	[number]	k4	2
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1912	[number]	k4	1912
<g/>
)	)	kIx)	)
2012	[number]	k4	2012
−	−	k?	−
Aleš	Aleš	k1gMnSc1	Aleš
Zimolka	Zimolka	k1gFnSc1	Zimolka
<g/>
,	,	kIx,	,
rockový	rockový	k2eAgMnSc1d1	rockový
bubeník	bubeník	k1gMnSc1	bubeník
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
*	*	kIx~	*
7	[number]	k4	7
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
2013	[number]	k4	2013
–	–	k?	–
Dušan	Dušan	k1gMnSc1	Dušan
Cvek	cvek	k1gInSc1	cvek
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
lékař	lékař	k1gMnSc1	lékař
a	a	k8xC	a
básník	básník	k1gMnSc1	básník
(	(	kIx(	(
<g/>
*	*	kIx~	*
8	[number]	k4	8
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
0946	[number]	k4	0946
–	–	k?	–
Edmund	Edmund	k1gMnSc1	Edmund
I.	I.	kA	I.
<g/>
,	,	kIx,	,
anglický	anglický	k2eAgMnSc1d1	anglický
král	král	k1gMnSc1	král
(	(	kIx(	(
<g/>
*	*	kIx~	*
cca	cca	kA	cca
921	[number]	k4	921
<g/>
)	)	kIx)	)
1339	[number]	k4	1339
–	–	k?	–
Aldona	Aldona	k1gFnSc1	Aldona
Anna	Anna	k1gFnSc1	Anna
Litevská	litevský	k2eAgFnSc1d1	Litevská
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
polská	polský	k2eAgFnSc1d1	polská
královna	královna	k1gFnSc1	královna
<g/>
,	,	kIx,	,
manželka	manželka	k1gFnSc1	manželka
Kazimíra	Kazimír	k1gMnSc2	Kazimír
III	III	kA	III
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
*	*	kIx~	*
asi	asi	k9	asi
1309	[number]	k4	1309
<g/>
)	)	kIx)	)
1512	[number]	k4	1512
–	–	k?	–
Bajezid	Bajezida	k1gFnPc2	Bajezida
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
turecký	turecký	k2eAgMnSc1d1	turecký
sultán	sultán	k1gMnSc1	sultán
(	(	kIx(	(
<g/>
*	*	kIx~	*
3	[number]	k4	3
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1447	[number]	k4	1447
<g/>
)	)	kIx)	)
1552	[number]	k4	1552
–	–	k?	–
Sebastian	Sebastian	k1gMnSc1	Sebastian
Münster	Münster	k1gMnSc1	Münster
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
učenec	učenec	k1gMnSc1	učenec
(	(	kIx(	(
<g/>
*	*	kIx~	*
20	[number]	k4	20
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
ledna	leden	k1gInSc2	leden
1488	[number]	k4	1488
<g/>
)	)	kIx)	)
1595	[number]	k4	1595
–	–	k?	–
Filip	Filip	k1gMnSc1	Filip
Neri	Ner	k1gMnPc1	Ner
<g/>
,	,	kIx,	,
italský	italský	k2eAgMnSc1d1	italský
kněz	kněz	k1gMnSc1	kněz
a	a	k8xC	a
světec	světec	k1gMnSc1	světec
(	(	kIx(	(
<g/>
*	*	kIx~	*
1515	[number]	k4	1515
<g/>
)	)	kIx)	)
1696	[number]	k4	1696
–	–	k?	–
Albertina	Albertin	k2eAgFnSc1d1	Albertina
Agnes	Agnesa	k1gFnPc2	Agnesa
Oranžská	oranžský	k2eAgFnSc1d1	Oranžská
<g/>
,	,	kIx,	,
místodržitelka	místodržitelka	k1gFnSc1	místodržitelka
Fríska	Frísko	k1gNnSc2	Frísko
<g/>
,	,	kIx,	,
Drentska	Drentsko	k1gNnSc2	Drentsko
a	a	k8xC	a
Groningenu	Groningen	k1gInSc2	Groningen
(	(	kIx(	(
<g/>
*	*	kIx~	*
9	[number]	k4	9
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1634	[number]	k4	1634
<g/>
)	)	kIx)	)
1679	[number]	k4	1679
–	–	k?	–
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Maria	Mario	k1gMnSc2	Mario
Bavorský	bavorský	k2eAgInSc1d1	bavorský
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
bavorský	bavorský	k2eAgMnSc1d1	bavorský
kurfiřt	kurfiřt	k1gMnSc1	kurfiřt
(	(	kIx(	(
<g/>
*	*	kIx~	*
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1636	[number]	k4	1636
<g/>
)	)	kIx)	)
1683	[number]	k4	1683
–	–	k?	–
Jan	Jan	k1gMnSc1	Jan
Adolf	Adolf	k1gMnSc1	Adolf
I.	I.	kA	I.
ze	z	k7c2	z
Schwarzenbergu	Schwarzenberg	k1gInSc2	Schwarzenberg
<g/>
,	,	kIx,	,
rakouský	rakouský	k2eAgInSc1d1	rakouský
a	a	k8xC	a
český	český	k2eAgMnSc1d1	český
šlechtic	šlechtic	k1gMnSc1	šlechtic
a	a	k8xC	a
diplomat	diplomat	k1gMnSc1	diplomat
(	(	kIx(	(
<g/>
*	*	kIx~	*
20	[number]	k4	20
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1615	[number]	k4	1615
<g/>
)	)	kIx)	)
1703	[number]	k4	1703
–	–	k?	–
Louis-Hector	Louis-Hector	k1gMnSc1	Louis-Hector
de	de	k?	de
Calliè	Calliè	k1gMnSc1	Calliè
<g/>
,	,	kIx,	,
generální	generální	k2eAgMnSc1d1	generální
guvernér	guvernér	k1gMnSc1	guvernér
Nové	Nové	k2eAgFnSc2d1	Nové
Francie	Francie	k1gFnSc2	Francie
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
*	*	kIx~	*
12	[number]	k4	12
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1648	[number]	k4	1648
<g/>
)	)	kIx)	)
1707	[number]	k4	1707
–	–	k?	–
Madame	madame	k1gFnSc1	madame
de	de	k?	de
Montespan	Montespan	k1gInSc1	Montespan
<g/>
,	,	kIx,	,
milenka	milenka	k1gFnSc1	milenka
francouzského	francouzský	k2eAgMnSc2d1	francouzský
krále	král	k1gMnSc2	král
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
*	*	kIx~	*
5	[number]	k4	5
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1640	[number]	k4	1640
<g/>
)	)	kIx)	)
1774	[number]	k4	1774
–	–	k?	–
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Reinhard	Reinhard	k1gMnSc1	Reinhard
von	von	k1gInSc4	von
Neipperg	Neipperg	k1gMnSc1	Neipperg
<g/>
,	,	kIx,	,
rakouský	rakouský	k2eAgMnSc1d1	rakouský
polní	polní	k2eAgMnSc1d1	polní
maršál	maršál	k1gMnSc1	maršál
(	(	kIx(	(
<g/>
*	*	kIx~	*
27	[number]	k4	27
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
května	květen	k1gInSc2	květen
1684	[number]	k4	1684
<g/>
)	)	kIx)	)
1816	[number]	k4	1816
–	–	k?	–
Adrian	Adrian	k1gMnSc1	Adrian
Zingg	Zingg	k1gMnSc1	Zingg
<g/>
,	,	kIx,	,
sasko-švýcarský	sasko-švýcarský	k2eAgMnSc1d1	sasko-švýcarský
malíř	malíř	k1gMnSc1	malíř
(	(	kIx(	(
<g/>
*	*	kIx~	*
15	[number]	k4	15
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1734	[number]	k4	1734
<g/>
)	)	kIx)	)
1818	[number]	k4	1818
–	–	k?	–
Michail	Michail	k1gMnSc1	Michail
Bogdanovič	Bogdanovič	k1gMnSc1	Bogdanovič
Barclay	Barclaa	k1gFnSc2	Barclaa
de	de	k?	de
Tolly	Tolla	k1gFnSc2	Tolla
<g/>
,	,	kIx,	,
ruský	ruský	k2eAgMnSc1d1	ruský
generál	generál	k1gMnSc1	generál
(	(	kIx(	(
<g/>
*	*	kIx~	*
27	[number]	k4	27
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1761	[number]	k4	1761
<g/>
)	)	kIx)	)
1864	[number]	k4	1864
–	–	k?	–
Charles	Charles	k1gMnSc1	Charles
Sealsfield	Sealsfield	k1gMnSc1	Sealsfield
<g/>
,	,	kIx,	,
rakouský	rakouský	k2eAgMnSc1d1	rakouský
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
americký	americký	k2eAgMnSc1d1	americký
spisovatel	spisovatel	k1gMnSc1	spisovatel
(	(	kIx(	(
<g/>
*	*	kIx~	*
3	[number]	k4	3
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1793	[number]	k4	1793
<g/>
)	)	kIx)	)
1870	[number]	k4	1870
–	–	k?	–
Johann	Johann	k1gMnSc1	Johann
Heinrich	Heinrich	k1gMnSc1	Heinrich
Blasius	Blasius	k1gMnSc1	Blasius
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
ornitolog	ornitolog	k1gMnSc1	ornitolog
(	(	kIx(	(
<g/>
*	*	kIx~	*
7	[number]	k4	7
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1809	[number]	k4	1809
<g/>
)	)	kIx)	)
1871	[number]	k4	1871
–	–	k?	–
Aimé	Aimé	k1gNnSc2	Aimé
Maillart	Maillarta	k1gFnPc2	Maillarta
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
(	(	kIx(	(
<g/>
*	*	kIx~	*
24	[number]	k4	24
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1817	[number]	k4	1817
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
1883	[number]	k4	1883
–	–	k?	–
Abd	Abd	k1gMnSc1	Abd
al-Kádir	al-Kádir	k1gMnSc1	al-Kádir
<g/>
,	,	kIx,	,
alžírský	alžírský	k2eAgMnSc1d1	alžírský
islámský	islámský	k2eAgMnSc1d1	islámský
učenec	učenec	k1gMnSc1	učenec
(	(	kIx(	(
<g/>
*	*	kIx~	*
6	[number]	k4	6
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1808	[number]	k4	1808
<g/>
)	)	kIx)	)
1888	[number]	k4	1888
–	–	k?	–
Ascanio	Ascanio	k1gMnSc1	Ascanio
Sobrero	Sobrero	k1gNnSc1	Sobrero
<g/>
,	,	kIx,	,
italský	italský	k2eAgMnSc1d1	italský
chemik	chemik	k1gMnSc1	chemik
<g/>
,	,	kIx,	,
objevitel	objevitel	k1gMnSc1	objevitel
nitroglycerinu	nitroglycerin	k1gInSc2	nitroglycerin
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1812	[number]	k4	1812
<g/>
)	)	kIx)	)
1902	[number]	k4	1902
Almon	Almon	k1gMnSc1	Almon
Brown	Brown	k1gMnSc1	Brown
Strowger	Strowger	k1gMnSc1	Strowger
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
<g />
.	.	kIx.	.
</s>
<s>
vynálezce	vynálezce	k1gMnSc1	vynálezce
(	(	kIx(	(
<g/>
*	*	kIx~	*
1839	[number]	k4	1839
<g/>
)	)	kIx)	)
Anton	anton	k1gInSc1	anton
von	von	k1gInSc1	von
Banhans	Banhans	k1gInSc4	Banhans
<g/>
,	,	kIx,	,
předlitavský	předlitavský	k2eAgMnSc1d1	předlitavský
politik	politik	k1gMnSc1	politik
(	(	kIx(	(
<g/>
*	*	kIx~	*
8	[number]	k4	8
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1825	[number]	k4	1825
<g/>
)	)	kIx)	)
1904	[number]	k4	1904
–	–	k?	–
Georges	Georges	k1gMnSc1	Georges
Gilles	Gilles	k1gMnSc1	Gilles
de	de	k?	de
la	la	k0	la
Tourette	Tourett	k1gMnSc5	Tourett
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
neurolog	neurolog	k1gMnSc1	neurolog
(	(	kIx(	(
<g/>
*	*	kIx~	*
30	[number]	k4	30
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1857	[number]	k4	1857
<g/>
)	)	kIx)	)
1907	[number]	k4	1907
Ida	Ida	k1gFnSc1	Ida
Saxton	Saxton	k1gInSc1	Saxton
McKinleyová	McKinleyová	k1gFnSc1	McKinleyová
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
manželka	manželka	k1gFnSc1	manželka
25	[number]	k4	25
<g/>
.	.	kIx.	.
prezidenta	prezident	k1gMnSc4	prezident
USA	USA	kA	USA
Williama	William	k1gMnSc4	William
McKinleye	McKinley	k1gMnSc4	McKinley
(	(	kIx(	(
<g/>
*	*	kIx~	*
8	[number]	k4	8
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1847	[number]	k4	1847
<g/>
)	)	kIx)	)
Emil	Emil	k1gMnSc1	Emil
Steinbach	Steinbach	k1gMnSc1	Steinbach
<g/>
,	,	kIx,	,
předlitavský	předlitavský	k2eAgMnSc1d1	předlitavský
státní	státní	k2eAgMnSc1d1	státní
úředník	úředník	k1gMnSc1	úředník
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
(	(	kIx(	(
<g/>
*	*	kIx~	*
11	[number]	k4	11
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1846	[number]	k4	1846
<g/>
)	)	kIx)	)
1910	[number]	k4	1910
–	–	k?	–
Marian	Marian	k1gMnSc1	Marian
Gawalewicz	Gawalewicz	k1gMnSc1	Gawalewicz
<g/>
,	,	kIx,	,
polský	polský	k2eAgMnSc1d1	polský
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
novinář	novinář	k1gMnSc1	novinář
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
divadelní	divadelní	k2eAgMnSc1d1	divadelní
kritik	kritik	k1gMnSc1	kritik
(	(	kIx(	(
<g/>
*	*	kIx~	*
21	[number]	k4	21
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1852	[number]	k4	1852
<g/>
)	)	kIx)	)
1912	[number]	k4	1912
–	–	k?	–
Amélie	Amélie	k1gFnSc1	Amélie
Bavorská	bavorský	k2eAgFnSc1d1	bavorská
<g/>
,	,	kIx,	,
bavorská	bavorský	k2eAgFnSc1d1	bavorská
princezna	princezna	k1gFnSc1	princezna
(	(	kIx(	(
<g/>
*	*	kIx~	*
24	[number]	k4	24
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1865	[number]	k4	1865
<g/>
)	)	kIx)	)
1914	[number]	k4	1914
–	–	k?	–
Jacob	Jacoba	k1gFnPc2	Jacoba
Augustus	Augustus	k1gMnSc1	Augustus
Riis	Riis	k1gInSc1	Riis
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
fotograf	fotograf	k1gMnSc1	fotograf
<g/>
,	,	kIx,	,
sociolog	sociolog	k1gMnSc1	sociolog
<g/>
,	,	kIx,	,
novinář	novinář	k1gMnSc1	novinář
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g />
.	.	kIx.	.
</s>
<s>
3	[number]	k4	3
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1849	[number]	k4	1849
<g/>
)	)	kIx)	)
1927	[number]	k4	1927
–	–	k?	–
Béla	Béla	k1gMnSc1	Béla
Szenes	Szenes	k1gMnSc1	Szenes
<g/>
,	,	kIx,	,
maďarský	maďarský	k2eAgMnSc1d1	maďarský
spisovatel	spisovatel	k1gMnSc1	spisovatel
(	(	kIx(	(
<g/>
*	*	kIx~	*
18	[number]	k4	18
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1894	[number]	k4	1894
<g/>
)	)	kIx)	)
1933	[number]	k4	1933
–	–	k?	–
Jimmie	Jimmie	k1gFnSc2	Jimmie
Rodgers	Rodgersa	k1gFnPc2	Rodgersa
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
country	country	k2eAgMnSc1d1	country
zpěvák	zpěvák	k1gMnSc1	zpěvák
a	a	k8xC	a
skladatel	skladatel	k1gMnSc1	skladatel
(	(	kIx(	(
<g/>
*	*	kIx~	*
8	[number]	k4	8
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1897	[number]	k4	1897
<g/>
)	)	kIx)	)
1938	[number]	k4	1938
Rafael	Rafael	k1gMnSc1	Rafael
Arnáiz	Arnáiz	k1gMnSc1	Arnáiz
Barón	Barón	k1gMnSc1	Barón
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
španělský	španělský	k2eAgMnSc1d1	španělský
trapista	trapista	k1gMnSc1	trapista
<g/>
,	,	kIx,	,
architekt	architekt	k1gMnSc1	architekt
a	a	k8xC	a
katolický	katolický	k2eAgMnSc1d1	katolický
světec	světec	k1gMnSc1	světec
(	(	kIx(	(
<g/>
*	*	kIx~	*
9	[number]	k4	9
<g/>
.	.	kIx.	.
kvetna	kvetn	k1gInSc2	kvetn
1911	[number]	k4	1911
<g/>
)	)	kIx)	)
John	John	k1gMnSc1	John
Jacob	Jacoba	k1gFnPc2	Jacoba
Abel	Abel	k1gMnSc1	Abel
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
biochemik	biochemik	k1gMnSc1	biochemik
a	a	k8xC	a
farmakolog	farmakolog	k1gMnSc1	farmakolog
(	(	kIx(	(
<g/>
*	*	kIx~	*
19	[number]	k4	19
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1857	[number]	k4	1857
<g/>
)	)	kIx)	)
1944	[number]	k4	1944
–	–	k?	–
Christian	Christian	k1gMnSc1	Christian
Wirth	Wirth	k1gMnSc1	Wirth
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
SS	SS	kA	SS
Sturmbannführer	Sturmbannführer	k1gInSc4	Sturmbannführer
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
*	*	kIx~	*
24	[number]	k4	24
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1885	[number]	k4	1885
<g/>
)	)	kIx)	)
1946	[number]	k4	1946
–	–	k?	–
Fridrich	Fridrich	k1gMnSc1	Fridrich
Waldecko-Pyrmontský	Waldecko-Pyrmontský	k2eAgMnSc1d1	Waldecko-Pyrmontský
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgMnSc1d1	poslední
waldecko-pyrmontský	waldeckoyrmontský	k2eAgMnSc1d1	waldecko-pyrmontský
kníže	kníže	k1gMnSc1	kníže
(	(	kIx(	(
<g/>
*	*	kIx~	*
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1865	[number]	k4	1865
<g/>
)	)	kIx)	)
1947	[number]	k4	1947
–	–	k?	–
Theodor	Theodor	k1gMnSc1	Theodor
Morell	Morell	k1gMnSc1	Morell
<g/>
,	,	kIx,	,
osobní	osobní	k2eAgMnSc1d1	osobní
lékař	lékař	k1gMnSc1	lékař
Adolfa	Adolf	k1gMnSc2	Adolf
Hitlera	Hitler	k1gMnSc2	Hitler
(	(	kIx(	(
<g/>
*	*	kIx~	*
22	[number]	k4	22
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1886	[number]	k4	1886
<g/>
)	)	kIx)	)
1951	[number]	k4	1951
–	–	k?	–
Lincoln	Lincoln	k1gMnSc1	Lincoln
<g />
.	.	kIx.	.
</s>
<s>
Ellsworth	Ellsworth	k1gMnSc1	Ellsworth
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
podnikatel	podnikatel	k1gMnSc1	podnikatel
<g/>
,	,	kIx,	,
<g/>
letec	letec	k1gMnSc1	letec
a	a	k8xC	a
polární	polární	k2eAgMnSc1d1	polární
badatel	badatel	k1gMnSc1	badatel
(	(	kIx(	(
<g/>
*	*	kIx~	*
12	[number]	k4	12
<g/>
.	.	kIx.	.
<g/>
května	květen	k1gInSc2	květen
1880	[number]	k4	1880
<g/>
)	)	kIx)	)
1955	[number]	k4	1955
Alberto	Alberta	k1gFnSc5	Alberta
Ascari	Ascari	k1gNnPc6	Ascari
<g/>
,	,	kIx,	,
italský	italský	k2eAgMnSc1d1	italský
automobilový	automobilový	k2eAgMnSc1d1	automobilový
závodník	závodník	k1gMnSc1	závodník
(	(	kIx(	(
<g/>
*	*	kIx~	*
13	[number]	k4	13
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
Ivan	Ivan	k1gMnSc1	Ivan
Gall	Gall	k1gMnSc1	Gall
<g/>
,	,	kIx,	,
slovenský	slovenský	k2eAgMnSc1d1	slovenský
básník	básník	k1gMnSc1	básník
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g />
.	.	kIx.	.
</s>
<s>
1885	[number]	k4	1885
<g/>
)	)	kIx)	)
1956	[number]	k4	1956
–	–	k?	–
Sergej	Sergej	k1gMnSc1	Sergej
Melgunov	Melgunov	k1gInSc1	Melgunov
<g/>
,	,	kIx,	,
ruský	ruský	k2eAgMnSc1d1	ruský
historik	historik	k1gMnSc1	historik
(	(	kIx(	(
<g/>
*	*	kIx~	*
25	[number]	k4	25
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1879	[number]	k4	1879
<g/>
)	)	kIx)	)
1966	[number]	k4	1966
–	–	k?	–
Áron	áron	k1gInSc1	áron
Tamási	Tamáse	k1gFnSc3	Tamáse
<g/>
,	,	kIx,	,
maďarský	maďarský	k2eAgMnSc1d1	maďarský
spisovatel	spisovatel	k1gMnSc1	spisovatel
(	(	kIx(	(
<g/>
*	*	kIx~	*
20	[number]	k4	20
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1897	[number]	k4	1897
<g/>
)	)	kIx)	)
1968	[number]	k4	1968
–	–	k?	–
Little	Little	k1gFnSc2	Little
Willie	Willie	k1gFnSc2	Willie
John	John	k1gMnSc1	John
<g/>
,	,	kIx,	,
americký	americký	k2eAgInSc1d1	americký
R	R	kA	R
<g/>
&	&	k?	&
<g />
.	.	kIx.	.
</s>
<s>
<g/>
B	B	kA	B
zpěvák	zpěvák	k1gMnSc1	zpěvák
(	(	kIx(	(
<g/>
*	*	kIx~	*
15	[number]	k4	15
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
1971	[number]	k4	1971
–	–	k?	–
Július	Július	k1gMnSc1	Július
Adamiš	Adamiš	k1gMnSc1	Adamiš
<g/>
,	,	kIx,	,
slovenský	slovenský	k2eAgMnSc1d1	slovenský
evangelický	evangelický	k2eAgMnSc1d1	evangelický
kněz	kněz	k1gMnSc1	kněz
a	a	k8xC	a
církevní	církevní	k2eAgMnSc1d1	církevní
historik	historik	k1gMnSc1	historik
(	(	kIx(	(
<g/>
*	*	kIx~	*
28	[number]	k4	28
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1885	[number]	k4	1885
<g/>
)	)	kIx)	)
1976	[number]	k4	1976
–	–	k?	–
Martin	Martin	k1gMnSc1	Martin
Heidegger	Heidegger	k1gMnSc1	Heidegger
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
filozof	filozof	k1gMnSc1	filozof
(	(	kIx(	(
<g/>
*	*	kIx~	*
1889	[number]	k4	1889
<g/>
)	)	kIx)	)
1978	[number]	k4	1978
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
Jan	Jan	k1gMnSc1	Jan
Schwarzenberg	Schwarzenberg	k1gMnSc1	Schwarzenberg
<g/>
,	,	kIx,	,
rakouský	rakouský	k2eAgMnSc1d1	rakouský
diplomat	diplomat	k1gMnSc1	diplomat
(	(	kIx(	(
<g/>
*	*	kIx~	*
31	[number]	k4	31
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1903	[number]	k4	1903
<g/>
)	)	kIx)	)
1987	[number]	k4	1987
–	–	k?	–
Jerucham	Jerucham	k1gInSc1	Jerucham
Zeisel	Zeisel	k1gInSc1	Zeisel
<g/>
,	,	kIx,	,
izraelský	izraelský	k2eAgMnSc1d1	izraelský
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
starosta	starosta	k1gMnSc1	starosta
města	město	k1gNnSc2	město
Haifa	Haifa	k1gFnSc1	Haifa
(	(	kIx(	(
<g/>
*	*	kIx~	*
1909	[number]	k4	1909
<g/>
)	)	kIx)	)
1989	[number]	k4	1989
−	−	k?	−
Phineas	Phineas	k1gMnSc1	Phineas
Newborn	Newborn	k1gMnSc1	Newborn
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
klavírista	klavírista	k1gMnSc1	klavírista
(	(	kIx(	(
<g/>
*	*	kIx~	*
14	[number]	k4	14
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
<g />
.	.	kIx.	.
</s>
<s>
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
1997	[number]	k4	1997
–	–	k?	–
Manfred	Manfred	k1gInSc1	Manfred
von	von	k1gInSc1	von
Ardenne	Ardenn	k1gInSc5	Ardenn
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
fyzik	fyzik	k1gMnSc1	fyzik
(	(	kIx(	(
<g/>
*	*	kIx~	*
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1907	[number]	k4	1907
<g/>
)	)	kIx)	)
1999	[number]	k4	1999
–	–	k?	–
Paul	Paul	k1gMnSc1	Paul
Sacher	Sachra	k1gFnPc2	Sachra
<g/>
,	,	kIx,	,
švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
dirigent	dirigent	k1gMnSc1	dirigent
(	(	kIx(	(
<g/>
*	*	kIx~	*
28	[number]	k4	28
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1906	[number]	k4	1906
<g/>
)	)	kIx)	)
2002	[number]	k4	2002
–	–	k?	–
Mamo	mama	k1gFnSc5	mama
Wolde	Wold	k1gInSc5	Wold
<g/>
,	,	kIx,	,
etiopský	etiopský	k2eAgMnSc1d1	etiopský
olympijský	olympijský	k2eAgMnSc1d1	olympijský
vítěz	vítěz	k1gMnSc1	vítěz
v	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
maratonu	maraton	k1gInSc2	maraton
(	(	kIx(	(
<g/>
*	*	kIx~	*
12	[number]	k4	12
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
2005	[number]	k4	2005
–	–	k?	–
Eddie	Eddie	k1gFnSc2	Eddie
Albert	Albert	k1gMnSc1	Albert
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
herec	herec	k1gMnSc1	herec
(	(	kIx(	(
<g/>
*	*	kIx~	*
22	[number]	k4	22
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1906	[number]	k4	1906
<g/>
)	)	kIx)	)
2008	[number]	k4	2008
–	–	k?	–
Sydney	Sydney	k1gNnSc4	Sydney
Pollack	Pollacko	k1gNnPc2	Pollacko
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
filmový	filmový	k2eAgMnSc1d1	filmový
režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
producent	producent	k1gMnSc1	producent
(	(	kIx(	(
<g/>
*	*	kIx~	*
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g />
.	.	kIx.	.
</s>
<s>
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
2009	[number]	k4	2009
–	–	k?	–
Peter	Peter	k1gMnSc1	Peter
Zezel	Zezel	k1gMnSc1	Zezel
<g/>
,	,	kIx,	,
kanadský	kanadský	k2eAgMnSc1d1	kanadský
hokejista	hokejista	k1gMnSc1	hokejista
(	(	kIx(	(
<g/>
*	*	kIx~	*
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
Česko	Česko	k1gNnSc1	Česko
Filip	Filip	k1gMnSc1	Filip
<g/>
,	,	kIx,	,
Filipa	Filip	k1gMnSc2	Filip
Svět	svět	k1gInSc1	svět
Slovensko	Slovensko	k1gNnSc4	Slovensko
–	–	k?	–
Dušan	Dušan	k1gMnSc1	Dušan
Polsko	Polsko	k1gNnSc1	Polsko
–	–	k?	–
Den	dno	k1gNnPc2	dno
matek	matka	k1gFnPc2	matka
Guyana	Guyana	k1gFnSc1	Guyana
–	–	k?	–
Den	den	k1gInSc4	den
nezávislosti	nezávislost	k1gFnSc2	nezávislost
Gruzie	Gruzie	k1gFnSc1	Gruzie
–	–	k?	–
Den	den	k1gInSc4	den
nezávislosti	nezávislost	k1gFnSc2	nezávislost
Katolický	katolický	k2eAgInSc4d1	katolický
kalendář	kalendář	k1gInSc4	kalendář
Svatý	svatý	k2eAgMnSc1d1	svatý
Eleutherus	Eleutherus	k1gMnSc1	Eleutherus
<g/>
,	,	kIx,	,
13	[number]	k4	13
<g/>
.	.	kIx.	.
papež	papež	k1gMnSc1	papež
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
(	(	kIx(	(
<g/>
†	†	k?	†
189	[number]	k4	189
<g/>
)	)	kIx)	)
Galerie	galerie	k1gFnSc1	galerie
26	[number]	k4	26
<g/>
.	.	kIx.	.
květen	květen	k1gInSc4	květen
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
