<s>
Za	za	k7c4	za
úspěšný	úspěšný	k2eAgInSc4d1	úspěšný
boj	boj	k1gInSc4	boj
o	o	k7c4	o
demokracii	demokracie	k1gFnSc4	demokracie
a	a	k8xC	a
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
<g/>
,	,	kIx,	,
za	za	k7c4	za
usmíření	usmíření	k1gNnSc4	usmíření
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
sjednocení	sjednocení	k1gNnSc6	sjednocení
kontinentu	kontinent	k1gInSc2	kontinent
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
Evropské	evropský	k2eAgFnSc6d1	Evropská
unii	unie	k1gFnSc6	unie
udělena	udělen	k2eAgFnSc1d1	udělena
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
mír	mír	k1gInSc4	mír
<g/>
.	.	kIx.	.
</s>
