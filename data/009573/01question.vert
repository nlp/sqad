<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
ploché	plochý	k2eAgFnPc4d1	plochá
součásti	součást	k1gFnPc4	součást
kruhového	kruhový	k2eAgInSc2d1	kruhový
tvaru	tvar	k1gInSc2	tvar
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
otáčet	otáčet	k5eAaImF	otáčet
kolem	kolem	k7c2	kolem
svého	svůj	k3xOyFgInSc2	svůj
středu	střed	k1gInSc2	střed
<g/>
?	?	kIx.	?
</s>
