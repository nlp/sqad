<p>
<s>
Kolo	kolo	k1gNnSc1	kolo
je	být	k5eAaImIp3nS	být
plochá	plochý	k2eAgFnSc1d1	plochá
součást	součást	k1gFnSc1	součást
kruhového	kruhový	k2eAgInSc2d1	kruhový
tvaru	tvar	k1gInSc2	tvar
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
otáčet	otáčet	k5eAaImF	otáčet
kolem	kolem	k7c2	kolem
svého	svůj	k3xOyFgInSc2	svůj
středu	střed	k1gInSc2	střed
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
volně	volně	k6eAd1	volně
otočné	otočný	k2eAgFnPc4d1	otočná
na	na	k7c6	na
pevné	pevný	k2eAgFnSc6d1	pevná
hřídeli	hřídel	k1gFnSc6	hřídel
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
pevně	pevně	k6eAd1	pevně
spojeno	spojit	k5eAaPmNgNnS	spojit
s	s	k7c7	s
otáčivou	otáčivý	k2eAgFnSc7d1	otáčivá
hřídelí	hřídel	k1gFnSc7	hřídel
<g/>
.	.	kIx.	.
</s>
<s>
Kolo	kolo	k1gNnSc1	kolo
je	být	k5eAaImIp3nS	být
podstatnou	podstatný	k2eAgFnSc7d1	podstatná
součástí	součást	k1gFnSc7	součást
vozidel	vozidlo	k1gNnPc2	vozidlo
a	a	k8xC	a
dopravních	dopravní	k2eAgNnPc2d1	dopravní
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
valivý	valivý	k2eAgInSc1d1	valivý
odpor	odpor	k1gInSc1	odpor
je	být	k5eAaImIp3nS	být
podstatně	podstatně	k6eAd1	podstatně
nižší	nízký	k2eAgNnSc4d2	nižší
než	než	k8xS	než
smykové	smykový	k2eAgNnSc4d1	smykové
tření	tření	k1gNnSc4	tření
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kola	Kola	k1gFnSc1	Kola
se	se	k3xPyFc4	se
však	však	k9	však
užívají	užívat	k5eAaImIp3nP	užívat
i	i	k9	i
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
jiných	jiný	k2eAgFnPc6d1	jiná
aplikacích	aplikace	k1gFnPc6	aplikace
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
jako	jako	k9	jako
mlýnská	mlýnský	k2eAgNnPc4d1	mlýnské
kola	kolo	k1gNnPc4	kolo
nebo	nebo	k8xC	nebo
kolotoče	kolotoč	k1gInPc4	kolotoč
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
součásti	součást	k1gFnPc1	součást
strojů	stroj	k1gInPc2	stroj
<g/>
,	,	kIx,	,
převodů	převod	k1gInPc2	převod
a	a	k8xC	a
pohonů	pohon	k1gInPc2	pohon
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
kladky	kladka	k1gFnPc4	kladka
a	a	k8xC	a
setrvačníky	setrvačník	k1gInPc4	setrvačník
nebo	nebo	k8xC	nebo
ozubená	ozubený	k2eAgNnPc4d1	ozubené
kola	kolo	k1gNnPc4	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Vynález	vynález	k1gInSc1	vynález
kola	kolo	k1gNnSc2	kolo
<g/>
,	,	kIx,	,
starý	starý	k2eAgInSc1d1	starý
více	hodně	k6eAd2	hodně
než	než	k8xS	než
7	[number]	k4	7
tisíc	tisíc	k4xCgInSc4	tisíc
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgInPc2d3	nejvýznamnější
v	v	k7c6	v
celých	celý	k2eAgFnPc6d1	celá
dějinách	dějiny	k1gFnPc6	dějiny
techniky	technika	k1gFnSc2	technika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ložisko	ložisko	k1gNnSc1	ložisko
spojuje	spojovat	k5eAaImIp3nS	spojovat
otáčivou	otáčivý	k2eAgFnSc4d1	otáčivá
a	a	k8xC	a
neotáčivou	otáčivý	k2eNgFnSc4d1	neotáčivá
část	část	k1gFnSc4	část
(	(	kIx(	(
<g/>
kolo	kolo	k1gNnSc1	kolo
a	a	k8xC	a
vůz	vůz	k1gInSc1	vůz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
tedy	tedy	k9	tedy
archeologicky	archeologicky	k6eAd1	archeologicky
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
například	například	k6eAd1	například
kluzné	kluzný	k2eAgNnSc4d1	kluzné
ložisko	ložisko	k1gNnSc4	ložisko
či	či	k8xC	či
valivé	valivý	k2eAgNnSc4d1	valivé
ložisko	ložisko	k1gNnSc4	ložisko
je	být	k5eAaImIp3nS	být
či	či	k8xC	či
není	být	k5eNaImIp3nS	být
starší	starý	k2eAgInSc4d2	starší
vynález	vynález	k1gInSc4	vynález
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Nejstarší	starý	k2eAgInSc1d3	nejstarší
zachovalý	zachovalý	k2eAgInSc1d1	zachovalý
nález	nález	k1gInSc1	nález
představuje	představovat	k5eAaImIp3nS	představovat
dřevěné	dřevěný	k2eAgNnSc4d1	dřevěné
kolo	kolo	k1gNnSc4	kolo
staré	stará	k1gFnSc2	stará
asi	asi	k9	asi
5	[number]	k4	5
200	[number]	k4	200
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
nalezené	nalezený	k2eAgFnSc6d1	nalezená
v	v	k7c6	v
bažině	bažina	k1gFnSc6	bažina
u	u	k7c2	u
Vrhniki	Vrhnik	k1gFnSc2	Vrhnik
<g/>
,	,	kIx,	,
asi	asi	k9	asi
15	[number]	k4	15
km	km	kA	km
JZ	JZ	kA	JZ
od	od	k7c2	od
Lublaně	Lublaň	k1gFnSc2	Lublaň
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
pozůstatky	pozůstatek	k1gInPc4	pozůstatek
dokonale	dokonale	k6eAd1	dokonale
zpracovaného	zpracovaný	k2eAgNnSc2d1	zpracované
dřevěného	dřevěný	k2eAgNnSc2d1	dřevěné
kola	kolo	k1gNnSc2	kolo
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
72	[number]	k4	72
cm	cm	kA	cm
a	a	k8xC	a
osy	osa	k1gFnSc2	osa
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
bylo	být	k5eAaImAgNnS	být
zřejmě	zřejmě	k6eAd1	zřejmě
pevně	pevně	k6eAd1	pevně
nasazeno	nasazen	k2eAgNnSc1d1	nasazeno
a	a	k8xC	a
tvořilo	tvořit	k5eAaImAgNnS	tvořit
tak	tak	k9	tak
součást	součást	k1gFnSc4	součást
dvoukolí	dvoukolí	k1gNnSc2	dvoukolí
<g/>
.	.	kIx.	.
</s>
<s>
Podobné	podobný	k2eAgNnSc1d1	podobné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mladší	mladý	k2eAgInPc1d2	mladší
nálezy	nález	k1gInPc1	nález
pocházejí	pocházet	k5eAaImIp3nP	pocházet
ze	z	k7c2	z
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
a	a	k8xC	a
jihozápadního	jihozápadní	k2eAgNnSc2d1	jihozápadní
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
<g/>
Nejstarší	starý	k2eAgNnSc1d3	nejstarší
vyobrazení	vyobrazení	k1gNnSc1	vyobrazení
vozu	vůz	k1gInSc2	vůz
se	s	k7c7	s
čtyřmi	čtyři	k4xCgNnPc7	čtyři
koly	kolo	k1gNnPc7	kolo
se	se	k3xPyFc4	se
našlo	najít	k5eAaPmAgNnS	najít
na	na	k7c6	na
zvoncovitém	zvoncovitý	k2eAgInSc6d1	zvoncovitý
poháru	pohár	k1gInSc6	pohár
z	z	k7c2	z
Bronocic	Bronocice	k1gFnPc2	Bronocice
<g/>
,	,	kIx,	,
asi	asi	k9	asi
40	[number]	k4	40
km	km	kA	km
SV	sv	kA	sv
od	od	k7c2	od
Krakova	Krakov	k1gInSc2	Krakov
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
,	,	kIx,	,
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
kolem	kolem	k7c2	kolem
3400	[number]	k4	3400
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Ve	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
kolo	kolo	k1gNnSc1	kolo
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
po	po	k7c6	po
velké	velký	k2eAgFnSc6d1	velká
části	část	k1gFnSc6	část
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
na	na	k7c6	na
Středním	střední	k2eAgInSc6d1	střední
východě	východ	k1gInSc6	východ
a	a	k8xC	a
na	na	k7c6	na
Kavkazu	Kavkaz	k1gInSc6	Kavkaz
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1500	[number]	k4	1500
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
byl	být	k5eAaImAgMnS	být
vůz	vůz	k1gInSc4	vůz
s	s	k7c7	s
koly	kolo	k1gNnPc7	kolo
znám	znát	k5eAaImIp1nS	znát
i	i	k9	i
ve	v	k7c6	v
Skandinávii	Skandinávie	k1gFnSc6	Skandinávie
a	a	k8xC	a
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Americké	americký	k2eAgFnPc1d1	americká
předkolumbovské	předkolumbovský	k2eAgFnPc1d1	předkolumbovská
kultury	kultura	k1gFnPc1	kultura
vozy	vůz	k1gInPc4	vůz
neznaly	neznat	k5eAaImAgFnP	neznat
<g/>
,	,	kIx,	,
snad	snad	k9	snad
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
neměly	mít	k5eNaImAgFnP	mít
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
vhodná	vhodný	k2eAgNnPc4d1	vhodné
tažná	tažný	k2eAgNnPc4d1	tažné
zvířata	zvíře	k1gNnPc4	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Nalezla	nalézt	k5eAaBmAgFnS	nalézt
se	se	k3xPyFc4	se
však	však	k9	však
olmécká	olmécký	k2eAgFnSc1d1	olmécká
hračka	hračka	k1gFnSc1	hračka
s	s	k7c7	s
malými	malý	k2eAgInPc7d1	malý
kolečky	koleček	k1gInPc7	koleček
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
kolem	kolem	k7c2	kolem
1500	[number]	k4	1500
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Mezi	mezi	k7c4	mezi
nejstarší	starý	k2eAgNnSc4d3	nejstarší
využití	využití	k1gNnSc4	využití
kola	kolo	k1gNnSc2	kolo
patří	patřit	k5eAaImIp3nS	patřit
hrnčířský	hrnčířský	k2eAgInSc1d1	hrnčířský
kruh	kruh	k1gInSc1	kruh
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
4500	[number]	k4	4500
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Kola	Kola	k1gFnSc1	Kola
byla	být	k5eAaImAgFnS	být
zprvu	zprvu	k6eAd1	zprvu
dřevěná	dřevěný	k2eAgFnSc1d1	dřevěná
<g/>
,	,	kIx,	,
z	z	k7c2	z
desek	deska	k1gFnPc2	deska
spojovaných	spojovaný	k2eAgInPc2d1	spojovaný
svlaky	svlak	k1gInPc7	svlak
<g/>
,	,	kIx,	,
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
době	doba	k1gFnSc6	doba
bronzové	bronzový	k2eAgNnSc1d1	bronzové
(	(	kIx(	(
<g/>
2200	[number]	k4	2200
až	až	k9	až
1500	[number]	k4	1500
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
lehčí	lehký	k2eAgNnPc4d2	lehčí
loukoťová	loukoťový	k2eAgNnPc4d1	loukoťové
kola	kolo	k1gNnPc4	kolo
válečných	válečný	k2eAgInPc2d1	válečný
vozů	vůz	k1gInPc2	vůz
<g/>
,	,	kIx,	,
skládaná	skládaný	k2eAgFnSc1d1	skládaná
z	z	k7c2	z
částí	část	k1gFnPc2	část
a	a	k8xC	a
opásaná	opásaný	k2eAgFnSc1d1	opásaná
kovovou	kovový	k2eAgFnSc7d1	kovová
obručí	obruč	k1gFnSc7	obruč
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
drobnými	drobný	k2eAgNnPc7d1	drobné
zdokonaleními	zdokonalení	k1gNnPc7	zdokonalení
se	se	k3xPyFc4	se
používala	používat	k5eAaImAgFnS	používat
až	až	k9	až
do	do	k7c2	do
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
keltských	keltský	k2eAgInPc6d1	keltský
hrobech	hrob	k1gInPc6	hrob
se	se	k3xPyFc4	se
nalezly	nalézt	k5eAaBmAgInP	nalézt
pohřební	pohřební	k2eAgInPc1d1	pohřební
vozy	vůz	k1gInPc1	vůz
s	s	k7c7	s
bronzovými	bronzový	k2eAgFnPc7d1	bronzová
loukoťovými	loukoťový	k2eAgFnPc7d1	loukoťová
koly	kola	k1gFnPc4	kola
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
1500	[number]	k4	1500
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Větrné	větrný	k2eAgInPc1d1	větrný
i	i	k8xC	i
vodní	vodní	k2eAgInPc1d1	vodní
mlýny	mlýn	k1gInPc1	mlýn
potřebovaly	potřebovat	k5eAaImAgInP	potřebovat
kola	kolo	k1gNnPc4	kolo
a	a	k8xC	a
převody	převod	k1gInPc4	převod
<g/>
,	,	kIx,	,
ozubená	ozubený	k2eAgNnPc4d1	ozubené
kola	kolo	k1gNnPc4	kolo
byla	být	k5eAaImAgFnS	být
známa	znám	k2eAgFnSc1d1	známa
už	už	k6eAd1	už
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
<g/>
,	,	kIx,	,
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
se	se	k3xPyFc4	se
však	však	k9	však
až	až	k9	až
s	s	k7c7	s
objevem	objev	k1gInSc7	objev
mechanických	mechanický	k2eAgFnPc2d1	mechanická
hodin	hodina	k1gFnPc2	hodina
v	v	k7c6	v
pozdním	pozdní	k2eAgInSc6d1	pozdní
středověku	středověk	k1gInSc6	středověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pronikavé	pronikavý	k2eAgFnPc1d1	pronikavá
změny	změna	k1gFnPc1	změna
v	v	k7c6	v
konstrukci	konstrukce	k1gFnSc6	konstrukce
kola	kolo	k1gNnSc2	kolo
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
s	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
strojírenského	strojírenský	k2eAgInSc2d1	strojírenský
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
železniční	železniční	k2eAgFnSc2d1	železniční
i	i	k8xC	i
silniční	silniční	k2eAgFnSc2d1	silniční
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Parní	parní	k2eAgInSc1d1	parní
stroj	stroj	k1gInSc1	stroj
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
setrvačník	setrvačník	k1gInSc4	setrvačník
a	a	k8xC	a
převody	převod	k1gInPc4	převod
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
další	další	k2eAgInPc4d1	další
stroje	stroj	k1gInPc4	stroj
<g/>
,	,	kIx,	,
a	a	k8xC	a
železniční	železniční	k2eAgFnSc1d1	železniční
doprava	doprava	k1gFnSc1	doprava
si	se	k3xPyFc3	se
vyžádala	vyžádat	k5eAaPmAgFnS	vyžádat
ocelová	ocelový	k2eAgFnSc1d1	ocelová
kola	kola	k1gFnSc1	kola
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
pevně	pevně	k6eAd1	pevně
spojená	spojený	k2eAgFnSc1d1	spojená
do	do	k7c2	do
dvojkolí	dvojkolí	k1gNnSc2	dvojkolí
<g/>
.	.	kIx.	.
</s>
<s>
Lepší	dobrý	k2eAgFnPc1d2	lepší
cesty	cesta	k1gFnPc1	cesta
a	a	k8xC	a
silnice	silnice	k1gFnPc1	silnice
umožnily	umožnit	k5eAaPmAgFnP	umožnit
i	i	k8xC	i
silniční	silniční	k2eAgFnSc4d1	silniční
dopravu	doprava	k1gFnSc4	doprava
<g/>
,	,	kIx,	,
jízdní	jízdní	k2eAgNnPc4d1	jízdní
kola	kolo	k1gNnPc4	kolo
potřebovala	potřebovat	k5eAaImAgFnS	potřebovat
lehčí	lehký	k2eAgFnSc1d2	lehčí
drátová	drátový	k2eAgFnSc1d1	drátová
kola	kola	k1gFnSc1	kola
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
dodnes	dodnes	k6eAd1	dodnes
užívají	užívat	k5eAaImIp3nP	užívat
i	i	k9	i
u	u	k7c2	u
motocyklů	motocykl	k1gInPc2	motocykl
<g/>
.	.	kIx.	.
</s>
<s>
Vynález	vynález	k1gInSc1	vynález
dalších	další	k2eAgInPc2d1	další
motorů	motor	k1gInPc2	motor
a	a	k8xC	a
pohonů	pohon	k1gInPc2	pohon
si	se	k3xPyFc3	se
vynutil	vynutit	k5eAaPmAgMnS	vynutit
i	i	k9	i
nové	nový	k2eAgFnPc4d1	nová
konstrukce	konstrukce	k1gFnPc4	konstrukce
kol	kolo	k1gNnPc2	kolo
<g/>
,	,	kIx,	,
u	u	k7c2	u
automobilů	automobil	k1gInPc2	automobil
lisovaných	lisovaný	k2eAgInPc2d1	lisovaný
z	z	k7c2	z
ocelového	ocelový	k2eAgInSc2d1	ocelový
plechu	plech	k1gInSc2	plech
s	s	k7c7	s
gumovými	gumový	k2eAgFnPc7d1	gumová
obručemi	obruč	k1gFnPc7	obruč
a	a	k8xC	a
pneumatikami	pneumatika	k1gFnPc7	pneumatika
<g/>
.	.	kIx.	.
</s>
<s>
Kola	Kola	k1gFnSc1	Kola
z	z	k7c2	z
lehkých	lehký	k2eAgFnPc2d1	lehká
slitin	slitina	k1gFnPc2	slitina
začala	začít	k5eAaPmAgFnS	začít
u	u	k7c2	u
letadel	letadlo	k1gNnPc2	letadlo
a	a	k8xC	a
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
se	se	k3xPyFc4	se
i	i	k9	i
u	u	k7c2	u
sportovních	sportovní	k2eAgInPc2d1	sportovní
automobilů	automobil	k1gInPc2	automobil
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kola	Kola	k1gFnSc1	Kola
moderních	moderní	k2eAgInPc2d1	moderní
dopravních	dopravní	k2eAgInPc2d1	dopravní
prostředků	prostředek	k1gInPc2	prostředek
jsou	být	k5eAaImIp3nP	být
složité	složitý	k2eAgInPc1d1	složitý
systémy	systém	k1gInPc1	systém
s	s	k7c7	s
pohonem	pohon	k1gInSc7	pohon
<g/>
,	,	kIx,	,
odpružením	odpružení	k1gNnSc7	odpružení
a	a	k8xC	a
brzdami	brzda	k1gFnPc7	brzda
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
strojírenství	strojírenství	k1gNnSc6	strojírenství
se	se	k3xPyFc4	se
užívají	užívat	k5eAaImIp3nP	užívat
velmi	velmi	k6eAd1	velmi
rozmanité	rozmanitý	k2eAgInPc1d1	rozmanitý
druhy	druh	k1gInPc1	druh
kol	kolo	k1gNnPc2	kolo
včetně	včetně	k7c2	včetně
ozubených	ozubený	k2eAgFnPc2d1	ozubená
a	a	k8xC	a
malá	malý	k2eAgNnPc4d1	malé
i	i	k8xC	i
velká	velký	k2eAgNnPc4d1	velké
kola	kolo	k1gNnPc4	kolo
lze	lze	k6eAd1	lze
najít	najít	k5eAaPmF	najít
téměř	téměř	k6eAd1	téměř
všude	všude	k6eAd1	všude
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc1d1	poslední
vývoj	vývoj	k1gInSc1	vývoj
se	se	k3xPyFc4	se
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c4	na
monolitická	monolitický	k2eAgNnPc4d1	monolitické
kola	kolo	k1gNnPc4	kolo
z	z	k7c2	z
umělých	umělý	k2eAgFnPc2d1	umělá
hmot	hmota	k1gFnPc2	hmota
(	(	kIx(	(
<g/>
Tweel	Tweel	k1gInSc1	Tweel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
sama	sám	k3xTgMnSc4	sám
pruží	pružit	k5eAaImIp3nS	pružit
a	a	k8xC	a
tlumí	tlumit	k5eAaImIp3nS	tlumit
nárazy	náraz	k1gInPc4	náraz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Důležité	důležitý	k2eAgInPc1d1	důležitý
druhy	druh	k1gInPc1	druh
kol	kola	k1gFnPc2	kola
==	==	k?	==
</s>
</p>
<p>
<s>
Kolo	kolo	k1gNnSc1	kolo
na	na	k7c6	na
hřídeli	hřídel	k1gFnSc6	hřídel
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
jednoduché	jednoduchý	k2eAgInPc4d1	jednoduchý
stroje	stroj	k1gInPc4	stroj
</s>
</p>
<p>
<s>
Kladka	kladka	k1gFnSc1	kladka
je	být	k5eAaImIp3nS	být
kolo	kolo	k1gNnSc4	kolo
s	s	k7c7	s
drážkou	drážka	k1gFnSc7	drážka
po	po	k7c6	po
obvodě	obvod	k1gInSc6	obvod
pro	pro	k7c4	pro
vedení	vedení	k1gNnSc4	vedení
lana	lano	k1gNnSc2	lano
</s>
</p>
<p>
<s>
Řemenice	řemenice	k1gFnSc1	řemenice
je	být	k5eAaImIp3nS	být
kolo	kolo	k1gNnSc4	kolo
pro	pro	k7c4	pro
přenos	přenos	k1gInSc4	přenos
síly	síla	k1gFnSc2	síla
řemenem	řemen	k1gInSc7	řemen
</s>
</p>
<p>
<s>
Loukoťové	loukoťový	k2eAgNnSc1d1	loukoťové
kolo	kolo	k1gNnSc1	kolo
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
náboje	náboj	k1gInSc2	náboj
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
s	s	k7c7	s
ložiskem	ložisko	k1gNnSc7	ložisko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
paprsků	paprsek	k1gInPc2	paprsek
a	a	k8xC	a
z	z	k7c2	z
ohýbaných	ohýbaný	k2eAgInPc2d1	ohýbaný
segmentů	segment	k1gInPc2	segment
čili	čili	k8xC	čili
loukotí	loukoť	k1gFnPc2	loukoť
<g/>
,	,	kIx,	,
obepnutých	obepnutý	k2eAgFnPc2d1	obepnutá
kovovou	kovový	k2eAgFnSc7d1	kovová
obručí	obruč	k1gFnSc7	obruč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vodní	vodní	k2eAgNnSc1d1	vodní
kolo	kolo	k1gNnSc1	kolo
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgInPc2d3	nejstarší
pohonů	pohon	k1gInPc2	pohon
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
dřevěné	dřevěný	k2eAgNnSc1d1	dřevěné
<g/>
,	,	kIx,	,
po	po	k7c6	po
obvodu	obvod	k1gInSc6	obvod
opatřené	opatřený	k2eAgFnSc2d1	opatřená
lopatkami	lopatka	k1gFnPc7	lopatka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Železniční	železniční	k2eAgNnSc1d1	železniční
dvojkolí	dvojkolí	k1gNnSc1	dvojkolí
tvoří	tvořit	k5eAaImIp3nS	tvořit
pevný	pevný	k2eAgInSc4d1	pevný
celek	celek	k1gInSc4	celek
hřídele	hřídel	k1gFnSc2	hřídel
a	a	k8xC	a
dvou	dva	k4xCgNnPc2	dva
kol	kolo	k1gNnPc2	kolo
s	s	k7c7	s
nalisovanými	nalisovaný	k2eAgInPc7d1	nalisovaný
okolky	okolek	k1gInPc7	okolek
</s>
</p>
<p>
<s>
Ozubené	ozubený	k2eAgNnSc1d1	ozubené
kolo	kolo	k1gNnSc1	kolo
je	být	k5eAaImIp3nS	být
součást	součást	k1gFnSc4	součást
převodů	převod	k1gInPc2	převod
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
zaručují	zaručovat	k5eAaImIp3nP	zaručovat
stálý	stálý	k2eAgInSc4d1	stálý
převodový	převodový	k2eAgInSc4d1	převodový
poměr	poměr	k1gInSc4	poměr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Drátové	drátový	k2eAgNnSc1d1	drátové
kolo	kolo	k1gNnSc1	kolo
nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
paprsky	paprsek	k1gInPc7	paprsek
napjatými	napjatý	k2eAgInPc7d1	napjatý
ocelovými	ocelový	k2eAgInPc7d1	ocelový
dráty	drát	k1gInPc7	drát
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
namáhány	namáhat	k5eAaImNgInP	namáhat
tahem	tah	k1gInSc7	tah
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Automobilové	automobilový	k2eAgNnSc1d1	automobilové
kolo	kolo	k1gNnSc1	kolo
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
lisované	lisovaný	k2eAgNnSc1d1	lisované
z	z	k7c2	z
ocelového	ocelový	k2eAgInSc2d1	ocelový
plechu	plech	k1gInSc2	plech
<g/>
,	,	kIx,	,
s	s	k7c7	s
ráfkem	ráfek	k1gInSc7	ráfek
pro	pro	k7c4	pro
pneumatiku	pneumatika	k1gFnSc4	pneumatika
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
kola	kolo	k1gNnSc2	kolo
bývá	bývat	k5eAaImIp3nS	bývat
i	i	k9	i
brzdový	brzdový	k2eAgInSc4d1	brzdový
buben	buben	k1gInSc4	buben
nebo	nebo	k8xC	nebo
disk	disk	k1gInSc4	disk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kolo	kolo	k1gNnSc1	kolo
motocyklu	motocykl	k1gInSc2	motocykl
je	být	k5eAaImIp3nS	být
nejčastěji	často	k6eAd3	často
drátové	drátový	k2eAgNnSc1d1	drátové
<g/>
,	,	kIx,	,
při	při	k7c6	při
menším	malý	k2eAgInSc6d2	menší
průměru	průměr	k1gInSc6	průměr
také	také	k9	také
lisované	lisovaný	k2eAgNnSc1d1	lisované
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
z	z	k7c2	z
umělé	umělý	k2eAgFnSc2d1	umělá
hmoty	hmota	k1gFnSc2	hmota
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kolotoč	kolotoč	k1gInSc1	kolotoč
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
vodorovná	vodorovný	k2eAgFnSc1d1	vodorovná
kruhová	kruhový	k2eAgFnSc1d1	kruhová
plošina	plošina	k1gFnSc1	plošina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
otáčí	otáčet	k5eAaImIp3nS	otáčet
kolem	kolem	k7c2	kolem
svislé	svislý	k2eAgFnSc2d1	svislá
osy	osa	k1gFnSc2	osa
a	a	k8xC	a
vozí	vozit	k5eAaImIp3nS	vozit
návštěvníky	návštěvník	k1gMnPc4	návštěvník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ruské	ruský	k2eAgFnPc1d1	ruská
nebo	nebo	k8xC	nebo
také	také	k9	také
obří	obří	k2eAgNnSc4d1	obří
kolo	kolo	k1gNnSc4	kolo
je	být	k5eAaImIp3nS	být
zábavní	zábavní	k2eAgNnSc4d1	zábavní
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
,	,	kIx,	,
veliké	veliký	k2eAgNnSc4d1	veliké
drátové	drátový	k2eAgNnSc4d1	drátové
kolo	kolo	k1gNnSc4	kolo
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
50	[number]	k4	50
až	až	k9	až
200	[number]	k4	200
m	m	kA	m
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
otáčí	otáčet	k5eAaImIp3nP	otáčet
kolem	kolem	k7c2	kolem
vodorovné	vodorovný	k2eAgFnSc2d1	vodorovná
osy	osa	k1gFnSc2	osa
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
obvodu	obvod	k1gInSc6	obvod
gondoly	gondola	k1gFnSc2	gondola
pro	pro	k7c4	pro
návštěvníky	návštěvník	k1gMnPc4	návštěvník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Ottův	Ottův	k2eAgInSc1d1	Ottův
slovník	slovník	k1gInSc1	slovník
naučný	naučný	k2eAgInSc1d1	naučný
<g/>
,	,	kIx,	,
svazek	svazek	k1gInSc1	svazek
14	[number]	k4	14
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
582	[number]	k4	582
–	–	k?	–
heslo	heslo	k1gNnSc1	heslo
Kolo	kolo	k1gNnSc1	kolo
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Hřídel	hřídel	k1gFnSc1	hřídel
</s>
</p>
<p>
<s>
Ráfek	ráfek	k1gInSc1	ráfek
</s>
</p>
<p>
<s>
Pneumatika	pneumatika	k1gFnSc1	pneumatika
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
kolo	kolo	k1gNnSc4	kolo
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
kolo	kolo	k1gNnSc4	kolo
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Nejstarší	starý	k2eAgNnSc1d3	nejstarší
kolo	kolo	k1gNnSc1	kolo
v	v	k7c6	v
Lublani	Lublaň	k1gFnSc6	Lublaň
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
Kola	Kola	k1gFnSc1	Kola
vysokorychlostních	vysokorychlostní	k2eAgInPc2d1	vysokorychlostní
vlaků	vlak	k1gInPc2	vlak
</s>
</p>
