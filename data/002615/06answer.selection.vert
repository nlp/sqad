<s>
Dukelský	dukelský	k2eAgInSc1d1	dukelský
průsmyk	průsmyk	k1gInSc1	průsmyk
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
kdy	kdy	k6eAd1	kdy
zde	zde	k6eAd1	zde
probíhala	probíhat	k5eAaImAgFnS	probíhat
Karpatsko-dukelská	karpatskoukelský	k2eAgFnSc1d1	karpatsko-dukelská
vojenská	vojenský	k2eAgFnSc1d1	vojenská
operace	operace	k1gFnSc1	operace
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yIgFnSc6	který
v	v	k7c6	v
období	období	k1gNnSc6	období
od	od	k7c2	od
8	[number]	k4	8
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1944	[number]	k4	1944
do	do	k7c2	do
27	[number]	k4	27
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1944	[number]	k4	1944
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
nebo	nebo	k8xC	nebo
bylo	být	k5eAaImAgNnS	být
zraněno	zranit	k5eAaPmNgNnS	zranit
až	až	k9	až
85000	[number]	k4	85000
sovětských	sovětský	k2eAgMnPc2d1	sovětský
a	a	k8xC	a
6500	[number]	k4	6500
československých	československý	k2eAgMnPc2d1	československý
bojovníků	bojovník	k1gMnPc2	bojovník
<g/>
.	.	kIx.	.
</s>
