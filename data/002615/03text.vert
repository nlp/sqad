<s>
Dukelský	dukelský	k2eAgInSc1d1	dukelský
průsmyk	průsmyk	k1gInSc1	průsmyk
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
Dukliansky	Dukliansky	k1gMnSc1	Dukliansky
priesmyk	priesmyk	k1gMnSc1	priesmyk
<g/>
,	,	kIx,	,
polsky	polsky	k6eAd1	polsky
Przełęcz	Przełęcz	k1gMnSc1	Przełęcz
Dukielska	Dukielsk	k1gInSc2	Dukielsk
<g/>
,	,	kIx,	,
502	[number]	k4	502
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
průsmyk	průsmyk	k1gInSc1	průsmyk
(	(	kIx(	(
<g/>
sedlo	sedlo	k1gNnSc1	sedlo
<g/>
)	)	kIx)	)
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
Laborecké	Laborecký	k2eAgFnSc2d1	Laborecká
vrchoviny	vrchovina	k1gFnSc2	vrchovina
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
vrchem	vrch	k1gInSc7	vrch
Kýčera	Kýčero	k1gNnSc2	Kýčero
(	(	kIx(	(
<g/>
579,0	[number]	k4	579,0
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
a	a	k8xC	a
Porubským	Porubská	k1gFnPc3	Porubská
sedlem	sedlo	k1gNnSc7	sedlo
<g/>
.	.	kIx.	.
</s>
<s>
Sedlem	sedlo	k1gNnSc7	sedlo
probíhá	probíhat	k5eAaImIp3nS	probíhat
slovensko-polská	slovenskoolský	k2eAgFnSc1d1	slovensko-polská
státní	státní	k2eAgFnSc1d1	státní
hranice	hranice	k1gFnSc1	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
slovenském	slovenský	k2eAgNnSc6d1	slovenské
území	území	k1gNnSc6	území
přímo	přímo	k6eAd1	přímo
pod	pod	k7c7	pod
sedlem	sedlo	k1gNnSc7	sedlo
pramení	pramenit	k5eAaImIp3nS	pramenit
na	na	k7c4	na
jih	jih	k1gInSc4	jih
tekoucí	tekoucí	k2eAgFnSc1d1	tekoucí
Ladomirka	Ladomirka	k1gFnSc1	Ladomirka
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
nejnižší	nízký	k2eAgInSc4d3	nejnižší
a	a	k8xC	a
nejschůdnější	schůdný	k2eAgInSc4d3	nejschůdnější
přechod	přechod	k1gInSc4	přechod
přes	přes	k7c4	přes
pohraniční	pohraniční	k2eAgInSc4d1	pohraniční
hřeben	hřeben	k1gInSc4	hřeben
Laborecké	Laborecký	k2eAgFnSc2d1	Laborecká
vrchoviny	vrchovina	k1gFnSc2	vrchovina
nad	nad	k7c7	nad
obcí	obec	k1gFnSc7	obec
Vyšný	vyšný	k2eAgInSc1d1	vyšný
Komárnik	Komárnik	k1gInSc1	Komárnik
<g/>
.	.	kIx.	.
</s>
<s>
Průsmykem	průsmyk	k1gInSc7	průsmyk
vede	vést	k5eAaImIp3nS	vést
důležitá	důležitý	k2eAgFnSc1d1	důležitá
silnice	silnice	k1gFnSc1	silnice
mezi	mezi	k7c7	mezi
Slovenskem	Slovensko	k1gNnSc7	Slovensko
a	a	k8xC	a
Polskem	Polsko	k1gNnSc7	Polsko
<g/>
,	,	kIx,	,
silnice	silnice	k1gFnSc1	silnice
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
73	[number]	k4	73
spojující	spojující	k2eAgFnSc1d1	spojující
slovenské	slovenský	k2eAgNnSc4d1	slovenské
město	město	k1gNnSc4	město
Svidník	Svidník	k1gInSc1	Svidník
s	s	k7c7	s
polským	polský	k2eAgNnSc7d1	polské
městem	město	k1gNnSc7	město
Dukla	Dukla	k1gFnSc1	Dukla
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterého	který	k3yIgInSc2	který
byl	být	k5eAaImAgInS	být
průsmyk	průsmyk	k1gInSc1	průsmyk
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
významnou	významný	k2eAgFnSc4d1	významná
křižovatku	křižovatka	k1gFnSc4	křižovatka
turistických	turistický	k2eAgFnPc2d1	turistická
cest	cesta	k1gFnPc2	cesta
<g/>
:	:	kIx,	:
červeně	červeně	k6eAd1	červeně
značená	značený	k2eAgFnSc1d1	značená
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
trasa	trasa	k1gFnSc1	trasa
E3	E3	k1gFnSc1	E3
z	z	k7c2	z
Porubského	Porubského	k2eAgNnSc2d1	Porubského
sedla	sedlo	k1gNnSc2	sedlo
po	po	k7c6	po
státní	státní	k2eAgFnSc6d1	státní
hranici	hranice	k1gFnSc6	hranice
dál	daleko	k6eAd2	daleko
na	na	k7c4	na
východ	východ	k1gInSc4	východ
<g/>
,	,	kIx,	,
vychází	vycházet	k5eAaImIp3nS	vycházet
odtud	odtud	k6eAd1	odtud
červeně	červeně	k6eAd1	červeně
značená	značený	k2eAgFnSc1d1	značená
Cesta	cesta	k1gFnSc1	cesta
hrdinů	hrdina	k1gMnPc2	hrdina
SNP	SNP	kA	SNP
(	(	kIx(	(
<g/>
součást	součást	k1gFnSc1	součást
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
trasy	trasa	k1gFnSc2	trasa
E	E	kA	E
<g/>
8	[number]	k4	8
<g/>
)	)	kIx)	)
do	do	k7c2	do
obce	obec	k1gFnSc2	obec
Medvedie	Medvedie	k1gFnSc2	Medvedie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
polské	polský	k2eAgFnSc6d1	polská
straně	strana	k1gFnSc6	strana
se	se	k3xPyFc4	se
připojují	připojovat	k5eAaImIp3nP	připojovat
další	další	k2eAgFnPc1d1	další
dvě	dva	k4xCgFnPc1	dva
turistické	turistický	k2eAgFnPc1d1	turistická
stezky	stezka	k1gFnPc1	stezka
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
průsmykem	průsmyk	k1gInSc7	průsmyk
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
velké	velký	k2eAgNnSc4d1	velké
parkoviště	parkoviště	k1gNnSc4	parkoviště
<g/>
,	,	kIx,	,
bufet	bufet	k1gNnSc4	bufet
a	a	k8xC	a
hotel	hotel	k1gInSc4	hotel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
přešlo	přejít	k5eAaPmAgNnS	přejít
u	u	k7c2	u
Dukelského	dukelský	k2eAgInSc2d1	dukelský
průsmyku	průsmyk	k1gInSc2	průsmyk
do	do	k7c2	do
ruského	ruský	k2eAgNnSc2d1	ruské
zajetí	zajetí	k1gNnSc2	zajetí
1800	[number]	k4	1800
mužů	muž	k1gMnPc2	muž
z	z	k7c2	z
28	[number]	k4	28
<g/>
.	.	kIx.	.
pěšího	pěší	k2eAgInSc2d1	pěší
pluku	pluk	k1gInSc2	pluk
<g/>
.	.	kIx.	.
</s>
<s>
Dukelský	dukelský	k2eAgInSc1d1	dukelský
průsmyk	průsmyk	k1gInSc1	průsmyk
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
kdy	kdy	k6eAd1	kdy
zde	zde	k6eAd1	zde
probíhala	probíhat	k5eAaImAgFnS	probíhat
Karpatsko-dukelská	karpatskoukelský	k2eAgFnSc1d1	karpatsko-dukelská
vojenská	vojenský	k2eAgFnSc1d1	vojenská
operace	operace	k1gFnSc1	operace
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yQgFnSc6	který
v	v	k7c6	v
období	období	k1gNnSc6	období
od	od	k7c2	od
8	[number]	k4	8
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1944	[number]	k4	1944
do	do	k7c2	do
27	[number]	k4	27
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1944	[number]	k4	1944
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
nebo	nebo	k8xC	nebo
bylo	být	k5eAaImAgNnS	být
zraněno	zranit	k5eAaPmNgNnS	zranit
až	až	k9	až
85000	[number]	k4	85000
sovětských	sovětský	k2eAgMnPc2d1	sovětský
a	a	k8xC	a
6500	[number]	k4	6500
československých	československý	k2eAgMnPc2d1	československý
bojovníků	bojovník	k1gMnPc2	bojovník
<g/>
.	.	kIx.	.
</s>
<s>
Památník	památník	k1gInSc1	památník
a	a	k8xC	a
hřbitov	hřbitov	k1gInSc1	hřbitov
příslušníků	příslušník	k1gMnPc2	příslušník
1	[number]	k4	1
<g/>
.	.	kIx.	.
československého	československý	k2eAgInSc2d1	československý
armádního	armádní	k2eAgInSc2d1	armádní
sboru	sbor	k1gInSc2	sbor
je	být	k5eAaImIp3nS	být
národní	národní	k2eAgFnSc7d1	národní
kulturní	kulturní	k2eAgFnSc7d1	kulturní
památkou	památka	k1gFnSc7	památka
<g/>
,	,	kIx,	,
na	na	k7c6	na
hřbitově	hřbitov	k1gInSc6	hřbitov
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
hroby	hrob	k1gInPc1	hrob
se	s	k7c7	s
jmény	jméno	k1gNnPc7	jméno
1265	[number]	k4	1265
padlých	padlý	k2eAgMnPc2d1	padlý
bojovníků	bojovník	k1gMnPc2	bojovník
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
sedlem	sedlo	k1gNnSc7	sedlo
stojí	stát	k5eAaImIp3nS	stát
kamenný	kamenný	k2eAgInSc1d1	kamenný
pylon	pylon	k1gInSc1	pylon
(	(	kIx(	(
<g/>
výška	výška	k1gFnSc1	výška
28	[number]	k4	28
m	m	kA	m
<g/>
)	)	kIx)	)
s	s	k7c7	s
monumentálním	monumentální	k2eAgNnSc7d1	monumentální
sousoším	sousoší	k1gNnSc7	sousoší
(	(	kIx(	(
<g/>
slovenský	slovenský	k2eAgInSc4d1	slovenský
název	název	k1gInSc4	název
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Vďaka	Vďaka	k1gFnSc1	Vďaka
matky	matka	k1gFnSc2	matka
sovietskemu	sovietskemat	k5eAaPmIp1nS	sovietskemat
vojakovi	vojak	k1gMnSc3	vojak
za	za	k7c2	za
oslobodenie	oslobodenie	k1gFnSc2	oslobodenie
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
obřadní	obřadní	k2eAgFnSc7d1	obřadní
síní	síň	k1gFnSc7	síň
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
památníkem	památník	k1gInSc7	památník
byla	být	k5eAaImAgFnS	být
vysázena	vysázen	k2eAgFnSc1d1	vysázena
"	"	kIx"	"
<g/>
Alej	alej	k1gFnSc1	alej
hrdinů	hrdina	k1gMnPc2	hrdina
Dukly	Dukla	k1gFnSc2	Dukla
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
státní	státní	k2eAgFnSc6d1	státní
hranici	hranice	k1gFnSc6	hranice
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
bývalé	bývalý	k2eAgFnSc2d1	bývalá
"	"	kIx"	"
<g/>
Pozorovatelny	pozorovatelna	k1gFnSc2	pozorovatelna
generála	generál	k1gMnSc2	generál
Ludvíka	Ludvík	k1gMnSc2	Ludvík
Svobody	Svoboda	k1gMnSc2	Svoboda
<g/>
"	"	kIx"	"
postavena	postaven	k2eAgFnSc1d1	postavena
49	[number]	k4	49
metrů	metr	k1gInPc2	metr
vysoká	vysoký	k2eAgFnSc1d1	vysoká
věž	věž	k1gFnSc1	věž
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Dukelského	dukelský	k2eAgInSc2d1	dukelský
průsmyku	průsmyk	k1gInSc2	průsmyk
až	až	k9	až
po	po	k7c4	po
obec	obec	k1gFnSc4	obec
Krajná	krajný	k2eAgFnSc1d1	Krajná
Poľana	Poľana	k1gFnSc1	Poľana
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
areál	areál	k1gInSc1	areál
Vojenského	vojenský	k2eAgNnSc2d1	vojenské
přírodního	přírodní	k2eAgNnSc2d1	přírodní
muzea	muzeum	k1gNnSc2	muzeum
na	na	k7c6	na
Dukle	Dukla	k1gFnSc6	Dukla
a	a	k8xC	a
Památník	památník	k1gInSc1	památník
pyrotechniků	pyrotechnik	k1gMnPc2	pyrotechnik
(	(	kIx(	(
<g/>
čtyři	čtyři	k4xCgInPc4	čtyři
metry	metr	k1gInPc4	metr
vysoký	vysoký	k2eAgInSc4d1	vysoký
žulový	žulový	k2eAgInSc4d1	žulový
kvádr	kvádr	k1gInSc4	kvádr
s	s	k7c7	s
rukama	ruka	k1gFnPc7	ruka
<g/>
,	,	kIx,	,
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
ženistu	ženista	k1gMnSc4	ženista
zneškodňujícího	zneškodňující	k2eAgInSc2d1	zneškodňující
miny	mina	k1gFnSc2	mina
nastražené	nastražený	k2eAgFnPc1d1	nastražená
Němci	Němec	k1gMnPc7	Němec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
FÁBERA	Fábera	k1gMnSc1	Fábera
<g/>
,	,	kIx,	,
Miloslav	Miloslav	k1gMnSc1	Miloslav
<g/>
.	.	kIx.	.
</s>
<s>
Dukla	Dukla	k1gFnSc1	Dukla
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Tiskové	tiskový	k2eAgNnSc1d1	tiskové
oddělení	oddělení	k1gNnSc1	oddělení
Hlavní	hlavní	k2eAgFnSc2d1	hlavní
správy	správa	k1gFnSc2	správa
výchovy	výchova	k1gFnSc2	výchova
a	a	k8xC	a
osvěty	osvěta	k1gFnSc2	osvěta
při	při	k7c6	při
M.	M.	kA	M.
<g/>
N.	N.	kA	N.
<g/>
O.	O.	kA	O.
<g/>
,	,	kIx,	,
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
47	[number]	k4	47
s.	s.	k?	s.
1	[number]	k4	1
<g/>
.	.	kIx.	.
československý	československý	k2eAgInSc1d1	československý
armádní	armádní	k2eAgInSc1d1	armádní
sbor	sbor	k1gInSc1	sbor
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
národní	národní	k2eAgFnSc2d1	národní
povstání	povstání	k1gNnSc2	povstání
Dukla	Dukla	k1gFnSc1	Dukla
(	(	kIx(	(
<g/>
rozcestník	rozcestník	k1gInSc1	rozcestník
<g/>
)	)	kIx)	)
Dukelské	dukelský	k2eAgNnSc1d1	Dukelské
bojiště	bojiště	k1gNnSc1	bojiště
a	a	k8xC	a
památníky	památník	k1gInPc1	památník
Karpatsko-dukelské	karpatskoukelský	k2eAgFnSc2d1	karpatsko-dukelská
operace	operace	k1gFnSc2	operace
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Dukelský	dukelský	k2eAgInSc4d1	dukelský
průsmyk	průsmyk	k1gInSc4	průsmyk
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Hlásí	hlásit	k5eAaImIp3nS	hlásit
se	se	k3xPyFc4	se
Polní	polní	k2eAgFnSc1d1	polní
pošta	pošta	k1gFnSc1	pošta
<g/>
!	!	kIx.	!
</s>
<s>
-	-	kIx~	-
pořad	pořad	k1gInSc1	pořad
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
Plus	plus	k1gInSc1	plus
o	o	k7c6	o
bývalém	bývalý	k2eAgNnSc6d1	bývalé
armádním	armádní	k2eAgNnSc6d1	armádní
vysílání	vysílání	k1gNnSc6	vysílání
<g/>
;	;	kIx,	;
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
unikátní	unikátní	k2eAgFnSc4d1	unikátní
ukázku	ukázka	k1gFnSc4	ukázka
z	z	k7c2	z
pamětí	paměť	k1gFnPc2	paměť
majora	major	k1gMnSc2	major
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Tyrka	Tyrek	k1gMnSc2	Tyrek
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
jako	jako	k8xC	jako
první	první	k4xOgMnSc1	první
vztyčil	vztyčit	k5eAaPmAgMnS	vztyčit
vlajku	vlajka	k1gFnSc4	vlajka
na	na	k7c6	na
československém	československý	k2eAgNnSc6d1	Československé
území	území	k1gNnSc6	území
<g/>
.	.	kIx.	.
</s>
