<s>
Smetana	Smetana	k1gMnSc1	Smetana
je	být	k5eAaImIp3nS	být
mléčný	mléčný	k2eAgInSc4d1	mléčný
výrobek	výrobek	k1gInSc4	výrobek
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nejtučnější	tučný	k2eAgFnSc4d3	nejtučnější
část	část	k1gFnSc4	část
mléka	mléko	k1gNnSc2	mléko
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
usazuje	usazovat	k5eAaImIp3nS	usazovat
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
povrchu	povrch	k1gInSc6	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Smetana	Smetana	k1gMnSc1	Smetana
se	se	k3xPyFc4	se
získává	získávat	k5eAaImIp3nS	získávat
sbíráním	sbírání	k1gNnSc7	sbírání
nebo	nebo	k8xC	nebo
odstřeďováním	odstřeďování	k1gNnSc7	odstřeďování
a	a	k8xC	a
v	v	k7c6	v
obchodě	obchod	k1gInSc6	obchod
se	se	k3xPyFc4	se
prodává	prodávat	k5eAaImIp3nS	prodávat
sladká	sladký	k2eAgFnSc1d1	sladká
nebo	nebo	k8xC	nebo
kysaná	kysaný	k2eAgFnSc1d1	kysaná
(	(	kIx(	(
<g/>
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
jazyků	jazyk	k1gInPc2	jazyk
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
ruský	ruský	k2eAgInSc1d1	ruský
výraz	výraz	k1gInSc1	výraz
smetana	smetana	k1gFnSc1	smetana
užívá	užívat	k5eAaImIp3nS	užívat
právě	právě	k9	právě
pro	pro	k7c4	pro
zakysanou	zakysaný	k2eAgFnSc4d1	zakysaná
smetanu	smetana	k1gFnSc4	smetana
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
nejméně	málo	k6eAd3	málo
10	[number]	k4	10
%	%	kIx~	%
tuku	tuk	k1gInSc2	tuk
<g/>
.	.	kIx.	.
</s>
<s>
Smetana	smetana	k1gFnSc1	smetana
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
alespoň	alespoň	k9	alespoň
30	[number]	k4	30
%	%	kIx~	%
tuku	tuk	k1gInSc2	tuk
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
smetana	smetana	k1gFnSc1	smetana
ke	k	k7c3	k
šlehání	šlehání	k1gNnSc3	šlehání
a	a	k8xC	a
slouží	sloužit	k5eAaImIp3nP	sloužit
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
šlehačky	šlehačka	k1gFnSc2	šlehačka
<g/>
.	.	kIx.	.
</s>
<s>
Podmnožinou	podmnožina	k1gFnSc7	podmnožina
je	být	k5eAaImIp3nS	být
smetana	smetana	k1gFnSc1	smetana
vysokotučná	vysokotučný	k2eAgFnSc1d1	vysokotučná
s	s	k7c7	s
nejméně	málo	k6eAd3	málo
35	[number]	k4	35
%	%	kIx~	%
tuku	tuk	k1gInSc2	tuk
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgInPc1d1	jiný
právní	právní	k2eAgInPc1d1	právní
řády	řád	k1gInPc1	řád
než	než	k8xS	než
český	český	k2eAgInSc1d1	český
mohou	moct	k5eAaImIp3nP	moct
smetanu	smetana	k1gFnSc4	smetana
a	a	k8xC	a
její	její	k3xOp3gInPc1	její
druhy	druh	k1gInPc1	druh
definovat	definovat	k5eAaBmF	definovat
jiným	jiný	k2eAgInSc7d1	jiný
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
