<s>
Smetana	Smetana	k1gMnSc1	Smetana
je	být	k5eAaImIp3nS	být
mléčný	mléčný	k2eAgInSc4d1	mléčný
výrobek	výrobek	k1gInSc4	výrobek
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nejtučnější	tučný	k2eAgFnSc4d3	nejtučnější
část	část	k1gFnSc4	část
mléka	mléko	k1gNnSc2	mléko
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
usazuje	usazovat	k5eAaImIp3nS	usazovat
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
povrchu	povrch	k1gInSc6	povrch
<g/>
.	.	kIx.	.
</s>
