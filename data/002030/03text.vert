<s>
Austin	Austin	k1gInSc1	Austin
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
státu	stát	k1gInSc2	stát
Texas	Texas	k1gInSc1	Texas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1839	[number]	k4	1839
se	se	k3xPyFc4	se
pět	pět	k4xCc1	pět
jezdců	jezdec	k1gInPc2	jezdec
vydalo	vydat	k5eAaPmAgNnS	vydat
hledat	hledat	k5eAaImF	hledat
území	území	k1gNnSc4	území
pro	pro	k7c4	pro
nové	nový	k2eAgNnSc4d1	nové
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Republiky	republika	k1gFnSc2	republika
Texas	Texas	kA	Texas
(	(	kIx(	(
<g/>
Republic	Republice	k1gFnPc2	Republice
of	of	k?	of
Texas	Texas	k1gInSc1	Texas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zvolili	zvolit	k5eAaPmAgMnP	zvolit
polohu	poloha	k1gFnSc4	poloha
na	na	k7c6	na
severním	severní	k2eAgInSc6d1	severní
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
Colorado	Colorado	k1gNnSc1	Colorado
<g/>
,	,	kIx,	,
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
rozkládala	rozkládat	k5eAaImAgFnS	rozkládat
vesnice	vesnice	k1gFnSc1	vesnice
Waterloo	Waterloo	k1gNnSc2	Waterloo
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vesnice	vesnice	k1gFnSc1	vesnice
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
Stephan	Stephany	k1gInPc2	Stephany
F.	F.	kA	F.
Austina	Austina	k1gFnSc1	Austina
nazvána	nazván	k2eAgFnSc1d1	nazvána
Austin	Austin	k1gInSc1	Austin
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1839	[number]	k4	1839
dopravilo	dopravit	k5eAaPmAgNnS	dopravit
50	[number]	k4	50
vozů	vůz	k1gInPc2	vůz
tažených	tažený	k2eAgInPc2d1	tažený
voly	vůl	k1gMnPc7	vůl
nábytek	nábytek	k1gInSc1	nábytek
a	a	k8xC	a
archivy	archiv	k1gInPc1	archiv
z	z	k7c2	z
bývalého	bývalý	k2eAgNnSc2d1	bývalé
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Houstonu	Houston	k1gInSc2	Houston
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
790	[number]	k4	790
390	[number]	k4	390
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
68,3	[number]	k4	68,3
%	%	kIx~	%
Bílí	bílý	k2eAgMnPc1d1	bílý
Američané	Američan	k1gMnPc1	Američan
8,1	[number]	k4	8,1
%	%	kIx~	%
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
0,9	[number]	k4	0,9
%	%	kIx~	%
Američtí	americký	k2eAgMnPc1d1	americký
indiáni	indián	k1gMnPc1	indián
6,3	[number]	k4	6,3
%	%	kIx~	%
Asijští	asijský	k2eAgMnPc1d1	asijský
Američané	Američan	k1gMnPc1	Američan
0,1	[number]	k4	0,1
%	%	kIx~	%
Pacifičtí	pacifický	k2eAgMnPc1d1	pacifický
ostrované	ostrovan	k1gMnPc1	ostrovan
12,9	[number]	k4	12,9
%	%	kIx~	%
Jiná	jiný	k2eAgFnSc1d1	jiná
rasa	rasa	k1gFnSc1	rasa
3,4	[number]	k4	3,4
%	%	kIx~	%
Dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
ras	ras	k1gMnSc1	ras
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
hispánského	hispánský	k2eAgMnSc2d1	hispánský
nebo	nebo	k8xC	nebo
latinskoamerického	latinskoamerický	k2eAgInSc2d1	latinskoamerický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
35,1	[number]	k4	35,1
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
roku	rok	k1gInSc2	rok
1839	[number]	k4	1839
vyjmul	vyjmout	k5eAaPmAgInS	vyjmout
kongres	kongres	k1gInSc1	kongres
Texaské	texaský	k2eAgFnSc2d1	texaská
republiky	republika	k1gFnSc2	republika
160	[number]	k4	160
000	[number]	k4	000
m2	m2	k4	m2
(	(	kIx(	(
<g/>
40	[number]	k4	40
akrů	akr	k1gInPc2	akr
<g/>
)	)	kIx)	)
půdy	půda	k1gFnSc2	půda
nedaleko	nedaleko	k7c2	nedaleko
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
pro	pro	k7c4	pro
zřízení	zřízení	k1gNnSc4	zřízení
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
území	území	k1gNnSc1	území
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1883	[number]	k4	1883
hlavním	hlavní	k2eAgInSc7d1	hlavní
campusem	campus	k1gInSc7	campus
University	universita	k1gFnSc2	universita
of	of	k?	of
Texas	Texas	k1gInSc1	Texas
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgNnPc4d1	další
vzdělávací	vzdělávací	k2eAgNnPc4d1	vzdělávací
zařízení	zařízení	k1gNnPc4	zařízení
v	v	k7c6	v
Austinu	Austin	k1gInSc6	Austin
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
Austin	Austin	k1gInSc1	Austin
Community	Communita	k1gFnSc2	Communita
College	Colleg	k1gFnSc2	Colleg
<g/>
,	,	kIx,	,
Austin	Austin	k2eAgMnSc1d1	Austin
Presbyterian	Presbyterian	k1gMnSc1	Presbyterian
Theological	Theological	k1gFnSc2	Theological
Seminary	Seminara	k1gFnSc2	Seminara
<g/>
,	,	kIx,	,
concordia	concordium	k1gNnSc2	concordium
University	universita	k1gFnSc2	universita
at	at	k?	at
Austin	Austin	k1gMnSc1	Austin
<g/>
,	,	kIx,	,
Episcopal	Episcopal	k1gMnSc1	Episcopal
Theological	Theological	k1gFnSc2	Theological
Seminary	Seminara	k1gFnSc2	Seminara
of	of	k?	of
southwest	southwest	k1gInSc1	southwest
<g/>
,	,	kIx,	,
Houston-Tilloston	Houston-Tilloston	k1gInSc1	Houston-Tilloston
College	Colleg	k1gFnSc2	Colleg
<g/>
,	,	kIx,	,
St.	st.	kA	st.
Edward	Edward	k1gMnSc1	Edward
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
University	universita	k1gFnSc2	universita
<g/>
.	.	kIx.	.
</s>
<s>
Austin	Austin	k1gInSc1	Austin
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgInSc1d1	známý
také	také	k9	také
jako	jako	k9	jako
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
živé	živý	k2eAgFnSc2d1	živá
hudby	hudba	k1gFnSc2	hudba
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
tvrzení	tvrzení	k1gNnSc1	tvrzení
skutečně	skutečně	k6eAd1	skutečně
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
hudba	hudba	k1gFnSc1	hudba
všech	všecek	k3xTgInPc2	všecek
žánrů	žánr	k1gInPc2	žánr
je	být	k5eAaImIp3nS	být
hrána	hrát	k5eAaImNgFnS	hrát
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
koncentrace	koncentrace	k1gFnSc1	koncentrace
klubů	klub	k1gInPc2	klub
a	a	k8xC	a
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
prezentují	prezentovat	k5eAaBmIp3nP	prezentovat
živou	živý	k2eAgFnSc4d1	živá
hudbu	hudba	k1gFnSc4	hudba
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
podél	podél	k7c2	podél
ulice	ulice	k1gFnSc2	ulice
Sixth	Sixth	k1gMnSc1	Sixth
Street	Street	k1gMnSc1	Street
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
na	na	k7c6	na
dolním	dolní	k2eAgInSc6d1	dolní
toku	tok	k1gInSc6	tok
řeky	řeka	k1gFnSc2	řeka
Colorado	Colorado	k1gNnSc1	Colorado
rozkládají	rozkládat	k5eAaImIp3nP	rozkládat
jezera	jezero	k1gNnSc2	jezero
Travis	Travis	k1gFnPc4	Travis
Lake	Lak	k1gInSc2	Lak
a	a	k8xC	a
Austin	Austin	k2eAgInSc4d1	Austin
Lake	Lake	k1gInSc4	Lake
<g/>
.	.	kIx.	.
</s>
<s>
Austin	Austin	k1gInSc1	Austin
je	být	k5eAaImIp3nS	být
významná	významný	k2eAgFnSc1d1	významná
dopravní	dopravní	k2eAgFnSc1d1	dopravní
křižovatka	křižovatka	k1gFnSc1	křižovatka
pro	pro	k7c4	pro
železniční	železniční	k2eAgFnSc4d1	železniční
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zejména	zejména	k9	zejména
silniční	silniční	k2eAgFnSc4d1	silniční
dopravu	doprava	k1gFnSc4	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
klíčových	klíčový	k2eAgInPc2d1	klíčový
bodů	bod	k1gInPc2	bod
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
budované	budovaný	k2eAgFnSc2d1	budovaná
dálniční	dálniční	k2eAgFnSc2d1	dálniční
sítě	síť	k1gFnSc2	síť
i	i	k9	i
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
velkých	velký	k2eAgInPc2d1	velký
projektů	projekt	k1gInPc2	projekt
typu	typ	k1gInSc2	typ
NAFTA	nafta	k1gFnSc1	nafta
<g/>
,	,	kIx,	,
CAFTA	CAFTA	kA	CAFTA
a	a	k8xC	a
NAU	NAU	kA	NAU
<g/>
.	.	kIx.	.
mrakodrap	mrakodrap	k1gInSc1	mrakodrap
360	[number]	k4	360
Condominiums	Condominiumsa	k1gFnPc2	Condominiumsa
Dakota	Dakota	k1gFnSc1	Dakota
Johnson	Johnson	k1gInSc1	Johnson
(	(	kIx(	(
<g/>
*	*	kIx~	*
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
Dabney	Dabnea	k1gFnSc2	Dabnea
Coleman	Coleman	k1gMnSc1	Coleman
(	(	kIx(	(
<g/>
*	*	kIx~	*
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
<g />
.	.	kIx.	.
</s>
<s>
Tobe	Tobe	k1gFnSc1	Tobe
Hooper	Hoopra	k1gFnPc2	Hoopra
(	(	kIx(	(
<g/>
*	*	kIx~	*
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
producent	producent	k1gMnSc1	producent
a	a	k8xC	a
scenárista	scenárista	k1gMnSc1	scenárista
Calvin	Calvina	k1gFnPc2	Calvina
Russell	Russell	k1gMnSc1	Russell
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
-	-	kIx~	-
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
roots	roots	k6eAd1	roots
rockový	rockový	k2eAgMnSc1d1	rockový
zpěvák-skladatel	zpěvákkladatel	k1gMnSc1	zpěvák-skladatel
a	a	k8xC	a
kytarista	kytarista	k1gMnSc1	kytarista
Kenneth	Kenneth	k1gMnSc1	Kenneth
Dale	Dale	k1gFnPc2	Dale
Cockrell	Cockrell	k1gMnSc1	Cockrell
(	(	kIx(	(
<g/>
*	*	kIx~	*
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
astronaut	astronaut	k1gMnSc1	astronaut
Jay	Jay	k1gMnSc1	Jay
O.	O.	kA	O.
Sanders	Sanders	k1gInSc1	Sanders
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g />
.	.	kIx.	.
</s>
<s>
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
Eric	Eric	k1gFnSc1	Eric
Johnson	Johnson	k1gMnSc1	Johnson
(	(	kIx(	(
<g/>
*	*	kIx~	*
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kytarista	kytarista	k1gMnSc1	kytarista
Matt	Matt	k1gMnSc1	Matt
McCoy	McCoa	k1gFnSc2	McCoa
(	(	kIx(	(
<g/>
*	*	kIx~	*
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
Michelle	Michelle	k1gFnSc2	Michelle
Forbesová	Forbesový	k2eAgFnSc1d1	Forbesová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
Ethan	ethan	k1gInSc1	ethan
Hawke	Hawke	k1gFnSc1	Hawke
(	(	kIx(	(
<g/>
*	*	kIx~	*
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
Ricardo	Ricardo	k1gNnSc1	Ricardo
Chavira	Chavira	k1gMnSc1	Chavira
(	(	kIx(	(
<g/>
*	*	kIx~	*
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
Angela	Angela	k1gFnSc1	Angela
Bettisová	Bettisový	k2eAgFnSc1d1	Bettisový
(	(	kIx(	(
<g/>
*	*	kIx~	*
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
Nelly	Nella	k1gFnSc2	Nella
(	(	kIx(	(
<g/>
*	*	kIx~	*
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rapper	rapper	k1gMnSc1	rapper
Alex	Alex	k1gMnSc1	Alex
Jones	Jones	k1gMnSc1	Jones
(	(	kIx(	(
<g/>
*	*	kIx~	*
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
konferenciér	konferenciér	k1gMnSc1	konferenciér
a	a	k8xC	a
moderátor	moderátor	k1gMnSc1	moderátor
radiových	radiový	k2eAgFnPc2d1	radiová
talkshows	talkshowsa	k1gFnPc2	talkshowsa
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
filmař	filmař	k1gMnSc1	filmař
tvořící	tvořící	k2eAgMnSc1d1	tvořící
faktografické	faktografický	k2eAgInPc4d1	faktografický
filmy	film	k1gInPc4	film
a	a	k8xC	a
dokumenty	dokument	k1gInPc4	dokument
Mehcad	Mehcad	k1gInSc1	Mehcad
Brooks	Brooks	k1gInSc4	Brooks
(	(	kIx(	(
<g/>
*	*	kIx~	*
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
Bryan	Bryan	k1gMnSc1	Bryan
Clay	Claa	k1gFnSc2	Claa
(	(	kIx(	(
<g/>
*	*	kIx~	*
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
desetibojař	desetibojař	k1gMnSc1	desetibojař
Benjamin	Benjamin	k1gMnSc1	Benjamin
McKenzie	McKenzie	k1gFnSc1	McKenzie
(	(	kIx(	(
<g/>
*	*	kIx~	*
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
Marshall	Marshall	k1gMnSc1	Marshall
Allman	Allman	k1gMnSc1	Allman
(	(	kIx(	(
<g/>
*	*	kIx~	*
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
<g />
.	.	kIx.	.
</s>
<s>
Ciara	Ciara	k1gFnSc1	Ciara
(	(	kIx(	(
<g/>
*	*	kIx~	*
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
r	r	kA	r
<g/>
&	&	k?	&
<g/>
b	b	k?	b
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
<g/>
,	,	kIx,	,
textařka	textařka	k1gFnSc1	textařka
<g/>
,	,	kIx,	,
tanečnice	tanečnice	k1gFnSc1	tanečnice
a	a	k8xC	a
herečka	herečka	k1gFnSc1	herečka
Amber	ambra	k1gFnPc2	ambra
Heardová	Heardová	k1gFnSc1	Heardová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
Terrence	Terrence	k1gFnSc2	Terrence
Malick	Malicka	k1gFnPc2	Malicka
(	(	kIx(	(
<g/>
*	*	kIx~	*
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
filmový	filmový	k2eAgMnSc1d1	filmový
režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
producent	producent	k1gMnSc1	producent
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
scenárista	scenárista	k1gMnSc1	scenárista
Adelaide	Adelaid	k1gInSc5	Adelaid
<g/>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
Antalya	Antalyum	k1gNnSc2	Antalyum
<g/>
,	,	kIx,	,
Turecko	Turecko	k1gNnSc1	Turecko
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
Edmonton	Edmonton	k1gInSc1	Edmonton
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
Kwangmjong	Kwangmjong	k1gInSc1	Kwangmjong
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
Koblenz	Koblenz	k1gInSc1	Koblenz
<g/>
,	,	kIx,	,
Porýní-Falc	Porýní-Falc	k1gInSc1	Porýní-Falc
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
Lima	limo	k1gNnSc2	limo
<g/>
,	,	kIx,	,
Peru	Peru	k1gNnSc1	Peru
<g/>
,	,	kIx,	,
1981	[number]	k4	1981
Maseru	maser	k1gInSc2	maser
<g/>
,	,	kIx,	,
Lesotho	Lesot	k1gMnSc2	Lesot
<g/>
,	,	kIx,	,
1978	[number]	k4	1978
Óita	Óitum	k1gNnSc2	Óitum
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
Orlu	Orel	k1gMnSc3	Orel
<g/>
,	,	kIx,	,
Nigérie	Nigérie	k1gFnSc1	Nigérie
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
Saltillo	Saltillo	k1gNnSc1	Saltillo
<g/>
,	,	kIx,	,
Mexiko	Mexiko	k1gNnSc1	Mexiko
<g/>
,	,	kIx,	,
1968	[number]	k4	1968
Tchaj-čung	Tchaj-čunga	k1gFnPc2	Tchaj-čunga
<g/>
,	,	kIx,	,
Čínská	čínský	k2eAgFnSc1d1	čínská
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
1986	[number]	k4	1986
Si-šuang-pan-na	Si-šuangano	k1gNnSc2	Si-šuang-pan-no
<g/>
,	,	kIx,	,
Čínská	čínský	k2eAgFnSc1d1	čínská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Austin	Austina	k1gFnPc2	Austina
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
