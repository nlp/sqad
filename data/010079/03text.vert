<p>
<s>
Artpop	Artpop	k1gInSc1	Artpop
(	(	kIx(	(
<g/>
stylizováno	stylizován	k2eAgNnSc1d1	stylizováno
jako	jako	k8xC	jako
ARTPOP	ARTPOP	kA	ARTPOP
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
třetí	třetí	k4xOgNnSc4	třetí
studiové	studiový	k2eAgNnSc4d1	studiové
album	album	k1gNnSc4	album
americké	americký	k2eAgFnSc2d1	americká
zpěvačky	zpěvačka	k1gFnSc2	zpěvačka
Lady	lady	k1gFnSc1	lady
Gaga	Gaga	k1gFnSc1	Gaga
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2013	[number]	k4	2013
vydal	vydat	k5eAaPmAgMnS	vydat
label	label	k1gMnSc1	label
Interscope	Interscop	k1gInSc5	Interscop
Records	Recordsa	k1gFnPc2	Recordsa
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
nahrávání	nahrávání	k1gNnSc1	nahrávání
bylo	být	k5eAaImAgNnS	být
zveřejněno	zveřejnit	k5eAaPmNgNnS	zveřejnit
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
zatímco	zatímco	k8xS	zatímco
se	se	k3xPyFc4	se
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
připravovala	připravovat	k5eAaImAgFnS	připravovat
na	na	k7c4	na
třetí	třetí	k4xOgNnSc4	třetí
koncertní	koncertní	k2eAgNnSc4d1	koncertní
turné	turné	k1gNnSc4	turné
"	"	kIx"	"
<g/>
Born	Born	k1gMnSc1	Born
This	Thisa	k1gFnPc2	Thisa
Way	Way	k1gMnSc1	Way
Ball	Ball	k1gMnSc1	Ball
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2012	[number]	k4	2012
pak	pak	k6eAd1	pak
zveřejnila	zveřejnit	k5eAaPmAgFnS	zveřejnit
název	název	k1gInSc4	název
nadcházejícího	nadcházející	k2eAgNnSc2d1	nadcházející
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
vyšla	vyjít	k5eAaPmAgFnS	vyjít
i	i	k9	i
speciální	speciální	k2eAgFnPc4d1	speciální
aplikace	aplikace	k1gFnPc4	aplikace
pro	pro	k7c4	pro
chytré	chytrý	k2eAgInPc4d1	chytrý
telefony	telefon	k1gInPc4	telefon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
magazínu	magazín	k1gInSc2	magazín
Billboard	billboard	k1gInSc1	billboard
bude	být	k5eAaImBp3nS	být
ARTPOP	ARTPOP	kA	ARTPOP
druhým	druhý	k4xOgNnSc7	druhý
nejočekávanějším	očekávaný	k2eAgNnSc7d3	nejočekávanější
albem	album	k1gNnSc7	album
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
za	za	k7c4	za
nahrávku	nahrávka	k1gFnSc4	nahrávka
Britney	Britnea	k1gFnSc2	Britnea
Spears	Spearsa	k1gFnPc2	Spearsa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
vyjede	vyjet	k5eAaPmIp3nS	vyjet
umělkyně	umělkyně	k1gFnSc1	umělkyně
na	na	k7c4	na
koncertní	koncertní	k2eAgNnSc4d1	koncertní
turné	turné	k1gNnSc4	turné
"	"	kIx"	"
<g/>
artRAVE	artRAVE	k?	artRAVE
The	The	k1gMnSc1	The
ARTPOP	ARTPOP	kA	ARTPOP
Ball	Ball	k1gMnSc1	Ball
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
propaguje	propagovat	k5eAaImIp3nS	propagovat
toto	tento	k3xDgNnSc4	tento
album	album	k1gNnSc4	album
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
O	o	k7c6	o
albu	album	k1gNnSc6	album
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2011	[number]	k4	2011
Gaga	Gag	k1gInSc2	Gag
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
začala	začít	k5eAaPmAgFnS	začít
promýšlet	promýšlet	k5eAaImF	promýšlet
své	svůj	k3xOyFgFnPc4	svůj
čtvrté	čtvrtá	k1gFnPc4	čtvrtá
studiové	studiový	k2eAgNnSc1d1	studiové
album	album	k1gNnSc1	album
a	a	k8xC	a
začíná	začínat	k5eAaImIp3nS	začínat
se	se	k3xPyFc4	se
připravovat	připravovat	k5eAaImF	připravovat
na	na	k7c4	na
Born	Born	k1gInSc4	Born
This	Thisa	k1gFnPc2	Thisa
Way	Way	k1gMnSc1	Way
Ball	Ball	k1gMnSc1	Ball
Tour	Tour	k1gMnSc1	Tour
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
zveřejnila	zveřejnit	k5eAaPmAgFnS	zveřejnit
<g/>
,	,	kIx,	,
že	že	k8xS	že
začíná	začínat	k5eAaImIp3nS	začínat
psát	psát	k5eAaImF	psát
a	a	k8xC	a
nahrávat	nahrávat	k5eAaImF	nahrávat
nové	nový	k2eAgNnSc4d1	nové
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
už	už	k6eAd1	už
má	mít	k5eAaImIp3nS	mít
pro	pro	k7c4	pro
něj	on	k3xPp3gInSc4	on
děj	děj	k1gInSc4	děj
a	a	k8xC	a
název	název	k1gInSc4	název
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
otázku	otázka	k1gFnSc4	otázka
jak	jak	k8xC	jak
bude	být	k5eAaImBp3nS	být
album	album	k1gNnSc1	album
znít	znít	k5eAaImF	znít
a	a	k8xC	a
z	z	k7c2	z
čeho	co	k3yInSc2	co
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
skládat	skládat	k5eAaImF	skládat
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
její	její	k3xOp3gMnSc1	její
producent	producent	k1gMnSc1	producent
Fernando	Fernanda	k1gFnSc5	Fernanda
Garibay	Garibaum	k1gNnPc7	Garibaum
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Je	být	k5eAaImIp3nS	být
mi	já	k3xPp1nSc3	já
trochu	trochu	k6eAd1	trochu
úzko	úzko	k6eAd1	úzko
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
mluvit	mluvit	k5eAaImF	mluvit
právě	právě	k6eAd1	právě
tady	tady	k6eAd1	tady
a	a	k8xC	a
teď	teď	k6eAd1	teď
<g/>
.	.	kIx.	.
</s>
<s>
Snažím	snažit	k5eAaImIp1nS	snažit
se	se	k3xPyFc4	se
vždycky	vždycky	k6eAd1	vždycky
překonat	překonat	k5eAaPmF	překonat
naše	náš	k3xOp1gFnPc4	náš
písně	píseň	k1gFnPc4	píseň
a	a	k8xC	a
nás	my	k3xPp1nPc4	my
samotné	samotný	k2eAgNnSc1d1	samotné
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
vždycky	vždycky	k6eAd1	vždycky
neuvěřitelné	uvěřitelný	k2eNgFnPc1d1	neuvěřitelná
písně	píseň	k1gFnPc1	píseň
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
mladá	mladý	k2eAgFnSc1d1	mladá
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
velký	velký	k2eAgInSc4d1	velký
prostor	prostor	k1gInSc4	prostor
pro	pro	k7c4	pro
růst	růst	k1gInSc4	růst
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
perfektní	perfektní	k2eAgFnSc1d1	perfektní
platforma	platforma	k1gFnSc1	platforma
k	k	k7c3	k
vyjadřování	vyjadřování	k1gNnSc3	vyjadřování
mě	já	k3xPp1nSc4	já
samotného	samotný	k2eAgMnSc4d1	samotný
<g/>
.	.	kIx.	.
</s>
<s>
Můžete	moct	k5eAaImIp2nP	moct
sní	sníst	k5eAaPmIp3nS	sníst
dělat	dělat	k5eAaImF	dělat
věci	věc	k1gFnPc4	věc
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
ne	ne	k9	ne
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
úžasné	úžasný	k2eAgNnSc1d1	úžasné
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
V	v	k7c6	v
interview	interview	k1gNnSc6	interview
pro	pro	k7c4	pro
MTV	MTV	kA	MTV
se	se	k3xPyFc4	se
nechal	nechat	k5eAaPmAgMnS	nechat
DJ	DJ	kA	DJ
White	Whit	k1gInSc5	Whit
Shadow	Shadow	k1gMnSc1	Shadow
slyšet	slyšet	k5eAaImF	slyšet
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
součásti	součást	k1gFnPc1	součást
produkce	produkce	k1gFnSc2	produkce
alba	album	k1gNnSc2	album
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Myslím	myslet	k5eAaImIp1nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejím	její	k3xOp3gInSc7	její
hlavním	hlavní	k2eAgInSc7d1	hlavní
cílem	cíl	k1gInSc7	cíl
teď	teď	k6eAd1	teď
bude	být	k5eAaImBp3nS	být
soustředit	soustředit	k5eAaPmF	soustředit
se	se	k3xPyFc4	se
na	na	k7c4	na
tourné	tourný	k2eAgNnSc4d1	tourné
a	a	k8xC	a
psaní	psaní	k1gNnSc4	psaní
nových	nový	k2eAgFnPc2d1	nová
písní	píseň	k1gFnPc2	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Poslal	poslat	k5eAaPmAgMnS	poslat
jsem	být	k5eAaImIp1nS	být
ji	on	k3xPp3gFnSc4	on
pár	pár	k4xCyI	pár
šílených	šílený	k2eAgFnPc2d1	šílená
věcí	věc	k1gFnPc2	věc
<g/>
.	.	kIx.	.
</s>
<s>
Něco	něco	k3yInSc1	něco
zní	znět	k5eAaImIp3nS	znět
jako	jako	k9	jako
písně	píseň	k1gFnPc1	píseň
a	a	k8xC	a
něco	něco	k3yInSc1	něco
jako	jako	k9	jako
šílenství	šílenství	k1gNnSc4	šílenství
<g/>
.	.	kIx.	.
</s>
<s>
Vím	vědět	k5eAaImIp1nS	vědět
co	co	k9	co
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
je	být	k5eAaImIp3nS	být
a	a	k8xC	a
co	co	k3yRnSc4	co
ne	ne	k9	ne
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2012	[number]	k4	2012
zveřejnila	zveřejnit	k5eAaPmAgFnS	zveřejnit
na	na	k7c6	na
The	The	k1gFnSc6	The
Insider	insider	k1gMnSc1	insider
její	její	k3xOp3gInPc4	její
plány	plán	k1gInPc4	plán
na	na	k7c4	na
rok	rok	k1gInSc4	rok
2012	[number]	k4	2012
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Chci	chtít	k5eAaImIp1nS	chtít
jít	jít	k5eAaImF	jít
dál	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Nejsem	být	k5eNaImIp1nS	být
posedlá	posedlý	k2eAgFnSc5d1	posedlá
cílem	cíl	k1gInSc7	cíl
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
procesem	proces	k1gInSc7	proces
<g/>
.	.	kIx.	.
</s>
<s>
Chci	chtít	k5eAaImIp1nS	chtít
jenom	jenom	k9	jenom
psát	psát	k5eAaImF	psát
hudbu	hudba	k1gFnSc4	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Těším	těšit	k5eAaImIp1nS	těšit
se	se	k3xPyFc4	se
na	na	k7c4	na
vydání	vydání	k1gNnSc4	vydání
dalšího	další	k2eAgNnSc2d1	další
alba	album	k1gNnSc2	album
a	a	k8xC	a
vydání	vydání	k1gNnSc2	vydání
se	se	k3xPyFc4	se
na	na	k7c4	na
tourné	tourný	k2eAgNnSc4d1	tourné
<g/>
.	.	kIx.	.
</s>
<s>
Cítím	cítit	k5eAaImIp1nS	cítit
se	se	k3xPyFc4	se
požehnaná	požehnaný	k2eAgFnSc1d1	požehnaná
<g/>
,	,	kIx,	,
že	že	k8xS	že
patřím	patřit	k5eAaImIp1nS	patřit
mezi	mezi	k7c4	mezi
umělce	umělec	k1gMnPc4	umělec
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nS	živit
tím	ten	k3xDgNnSc7	ten
co	co	k3yQnSc4	co
milují	milovat	k5eAaImIp3nP	milovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2012	[number]	k4	2012
její	její	k3xOp3gFnSc4	její
manažer	manažer	k1gMnSc1	manažer
Vincent	Vincent	k1gMnSc1	Vincent
Herbert	Herbert	k1gMnSc1	Herbert
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
slyšel	slyšet	k5eAaImAgMnS	slyšet
materiál	materiál	k1gInSc4	materiál
z	z	k7c2	z
alba	album	k1gNnSc2	album
a	a	k8xC	a
poznamenal	poznamenat	k5eAaPmAgMnS	poznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
"	"	kIx"	"
<g/>
šíleně	šíleně	k6eAd1	šíleně
skvělé	skvělý	k2eAgInPc1d1	skvělý
záznamy	záznam	k1gInPc1	záznam
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
prozradila	prozradit	k5eAaPmAgFnS	prozradit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nové	nový	k2eAgNnSc1d1	nové
album	album	k1gNnSc1	album
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
jmenovat	jmenovat	k5eAaImF	jmenovat
ARTPOP	ARTPOP	kA	ARTPOP
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
během	během	k7c2	během
psaní	psaní	k1gNnSc2	psaní
materiálu	materiál	k1gInSc2	materiál
pro	pro	k7c4	pro
album	album	k1gNnSc4	album
ARTPOP	ARTPOP	kA	ARTPOP
si	se	k3xPyFc3	se
byla	být	k5eAaImAgFnS	být
mnohem	mnohem	k6eAd1	mnohem
jistější	jistý	k2eAgFnSc1d2	jistější
<g/>
,	,	kIx,	,
než	než	k8xS	než
tomu	ten	k3xDgNnSc3	ten
bylo	být	k5eAaImAgNnS	být
u	u	k7c2	u
předchozích	předchozí	k2eAgNnPc2d1	předchozí
studiových	studiový	k2eAgNnPc2d1	studiové
alb	album	k1gNnPc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc4	album
popsala	popsat	k5eAaPmAgFnS	popsat
jako	jako	k8xS	jako
své	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
skutečné	skutečný	k2eAgNnSc4d1	skutečné
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
zobrazí	zobrazit	k5eAaPmIp3nS	zobrazit
ji	on	k3xPp3gFnSc4	on
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
fénixe	fénix	k1gMnSc4	fénix
vstávajícího	vstávající	k2eAgMnSc4d1	vstávající
z	z	k7c2	z
popela	popel	k1gInSc2	popel
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
navíc	navíc	k6eAd1	navíc
Gaga	Gaga	k1gFnSc1	Gaga
prohlašovala	prohlašovat	k5eAaImAgFnS	prohlašovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
její	její	k3xOp3gFnSc1	její
ARTPOP	ARTPOP	kA	ARTPOP
může	moct	k5eAaImIp3nS	moct
znamenat	znamenat	k5eAaImF	znamenat
cokoliv	cokoliv	k3yInSc1	cokoliv
<g/>
"	"	kIx"	"
a	a	k8xC	a
že	že	k8xS	že
její	její	k3xOp3gNnSc1	její
album	album	k1gNnSc1	album
je	být	k5eAaImIp3nS	být
o	o	k7c6	o
"	"	kIx"	"
<g/>
možnostech	možnost	k1gFnPc6	možnost
<g/>
"	"	kIx"	"
–	–	k?	–
a	a	k8xC	a
vše	všechen	k3xTgNnSc1	všechen
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
méně	málo	k6eAd2	málo
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
Born	Borna	k1gFnPc2	Borna
This	Thisa	k1gFnPc2	Thisa
Way	Way	k1gFnSc2	Way
a	a	k8xC	a
více	hodně	k6eAd2	hodně
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
The	The	k1gMnSc2	The
Fame	Fam	k1gMnSc2	Fam
a	a	k8xC	a
mělo	mít	k5eAaImAgNnS	mít
vyjít	vyjít	k5eAaPmF	vyjít
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kvůli	kvůli	k7c3	kvůli
jejímu	její	k3xOp3gNnSc3	její
zranění	zranění	k1gNnSc3	zranění
bylo	být	k5eAaImAgNnS	být
vydání	vydání	k1gNnSc3	vydání
alba	album	k1gNnSc2	album
odloženo	odložit	k5eAaPmNgNnS	odložit
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
na	na	k7c4	na
11	[number]	k4	11
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
alba	album	k1gNnSc2	album
prý	prý	k9	prý
už	už	k6eAd1	už
neudělá	udělat	k5eNaPmIp3nS	udělat
chyby	chyba	k1gFnPc4	chyba
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
udělala	udělat	k5eAaPmAgFnS	udělat
u	u	k7c2	u
Born	Borna	k1gFnPc2	Borna
This	Thisa	k1gFnPc2	Thisa
Way	Way	k1gMnPc2	Way
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
fanoušky	fanoušek	k1gMnPc7	fanoušek
mrzelo	mrzet	k5eAaImAgNnS	mrzet
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
skladeb	skladba	k1gFnPc2	skladba
neudělala	udělat	k5eNaPmAgFnS	udělat
singly	singl	k1gInPc4	singl
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
<g/>
,	,	kIx,	,
že	že	k8xS	že
světové	světový	k2eAgNnSc1d1	světové
turné	turné	k1gNnSc1	turné
přišlo	přijít	k5eAaPmAgNnS	přijít
až	až	k9	až
rok	rok	k1gInSc4	rok
po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
ARTPOP	ARTPOP	kA	ARTPOP
éře	éra	k1gFnSc6	éra
se	se	k3xPyFc4	se
dočkáme	dočkat	k5eAaPmIp1nP	dočkat
videoklipu	videoklip	k1gInSc2	videoklip
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
uzavře	uzavřít	k5eAaPmIp3nS	uzavřít
trilogii	trilogie	k1gFnSc4	trilogie
"	"	kIx"	"
<g/>
Paparazzi	Paparazze	k1gFnSc4	Paparazze
<g/>
"	"	kIx"	"
–	–	k?	–
"	"	kIx"	"
<g/>
Telephone	Telephon	k1gInSc5	Telephon
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skladba	skladba	k1gFnSc1	skladba
má	mít	k5eAaImIp3nS	mít
název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
Aura	aura	k1gFnSc1	aura
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
demo	demo	k2eAgFnSc1d1	demo
i	i	k8xC	i
předčasně	předčasně	k6eAd1	předčasně
uniklo	uniknout	k5eAaPmAgNnS	uniknout
na	na	k7c4	na
internet	internet	k1gInSc4	internet
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
singl	singl	k1gInSc1	singl
z	z	k7c2	z
ARTPOP	ARTPOP	kA	ARTPOP
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Applause	Applause	k1gFnSc1	Applause
<g/>
"	"	kIx"	"
vyšel	vyjít	k5eAaPmAgInS	vyjít
už	už	k6eAd1	už
12	[number]	k4	12
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
pilotním	pilotní	k2eAgInSc7d1	pilotní
singlem	singl	k1gInSc7	singl
skladba	skladba	k1gFnSc1	skladba
"	"	kIx"	"
<g/>
Sexxx	Sexxx	k1gInSc1	Sexxx
dreams	dreams	k1gInSc1	dreams
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tým	tým	k1gInSc1	tým
Gaga	Gag	k1gInSc2	Gag
ji	on	k3xPp3gFnSc4	on
přemluvil	přemluvit	k5eAaPmAgMnS	přemluvit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vybrala	vybrat	k5eAaPmAgFnS	vybrat
Applause	Applause	k1gFnSc1	Applause
<g/>
.	.	kIx.	.
</s>
<s>
Gaga	Gaga	k1gFnSc1	Gaga
dala	dát	k5eAaPmAgFnS	dát
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
září	zářit	k5eAaImIp3nP	zářit
otázku	otázka	k1gFnSc4	otázka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
skladba	skladba	k1gFnSc1	skladba
z	z	k7c2	z
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
fanoušci	fanoušek	k1gMnPc1	fanoušek
měli	mít	k5eAaImAgMnP	mít
možnost	možnost	k1gFnSc4	možnost
slyšet	slyšet	k5eAaImF	slyšet
na	na	k7c4	na
iTunes	iTunes	k1gInSc4	iTunes
Festivalu	festival	k1gInSc2	festival
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
dalším	další	k2eAgInSc7d1	další
singlem	singl	k1gInSc7	singl
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
výběr	výběr	k1gInSc4	výběr
dala	dát	k5eAaPmAgFnS	dát
skladby	skladba	k1gFnPc4	skladba
"	"	kIx"	"
<g/>
Aura	aura	k1gFnSc1	aura
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Swine	Swin	k1gMnSc5	Swin
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
MANiCURE	MANiCURE	k1gMnPc5	MANiCURE
<g/>
"	"	kIx"	"
a	a	k8xC	a
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
Sexxx	Sexxx	k1gInSc1	Sexxx
Dreams	Dreams	k1gInSc1	Dreams
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Gaga	Gaga	k1gFnSc1	Gaga
nakonec	nakonec	k6eAd1	nakonec
vybrala	vybrat	k5eAaPmAgFnS	vybrat
jako	jako	k9	jako
druhý	druhý	k4xOgInSc1	druhý
singl	singl	k1gInSc1	singl
skladbu	skladba	k1gFnSc4	skladba
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
ještě	ještě	k9	ještě
diváci	divák	k1gMnPc1	divák
neslyšeli	slyšet	k5eNaImAgMnP	slyšet
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
skladbu	skladba	k1gFnSc4	skladba
"	"	kIx"	"
<g/>
Do	do	k7c2	do
What	Whata	k1gFnPc2	Whata
U	u	k7c2	u
Want	Wanta	k1gFnPc2	Wanta
<g/>
"	"	kIx"	"
<g/>
"	"	kIx"	"
<g/>
"	"	kIx"	"
V	v	k7c6	v
pondělí	pondělí	k1gNnSc6	pondělí
7	[number]	k4	7
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
Gaga	Gagum	k1gNnSc2	Gagum
zveřejnila	zveřejnit	k5eAaPmAgFnS	zveřejnit
obal	obal	k1gInSc4	obal
k	k	k7c3	k
albu	album	k1gNnSc3	album
<g/>
,	,	kIx,	,
odhalování	odhalování	k1gNnSc1	odhalování
po	po	k7c6	po
kouskách	kousek	k1gInPc6	kousek
probíhalo	probíhat	k5eAaImAgNnS	probíhat
na	na	k7c6	na
billboardech	billboard	k1gInPc6	billboard
na	na	k7c4	na
Times	Times	k1gInSc4	Times
Square	square	k1gInSc4	square
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
obalu	obal	k1gInSc6	obal
alba	album	k1gNnSc2	album
je	být	k5eAaImIp3nS	být
vyobrazena	vyobrazen	k2eAgFnSc1d1	vyobrazena
její	její	k3xOp3gFnSc1	její
socha	socha	k1gFnSc1	socha
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
speciálně	speciálně	k6eAd1	speciálně
pro	pro	k7c4	pro
obal	obal	k1gInSc4	obal
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
slavný	slavný	k2eAgMnSc1d1	slavný
umělec	umělec	k1gMnSc1	umělec
Jeff	Jeff	k1gMnSc1	Jeff
Koons	Koons	k1gInSc4	Koons
<g/>
.	.	kIx.	.
</s>
<s>
Koons	Koons	k6eAd1	Koons
je	být	k5eAaImIp3nS	být
také	také	k9	také
autorem	autor	k1gMnSc7	autor
koláže	koláž	k1gFnSc2	koláž
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
album	album	k1gNnSc1	album
cover	covero	k1gNnPc2	covero
přestavuje	přestavovat	k5eAaImIp3nS	přestavovat
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
je	být	k5eAaImIp3nS	být
zobrazením	zobrazení	k1gNnSc7	zobrazení
moderní	moderní	k2eAgFnSc2d1	moderní
Botticelliho	Botticelli	k1gMnSc4	Botticelli
Zrození	zrození	k1gNnSc2	zrození
Venuše	Venuše	k1gFnSc2	Venuše
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
pop	pop	k1gMnSc1	pop
star	star	k1gInSc1	star
<g/>
.	.	kIx.	.
</s>
<s>
Prvních	první	k4xOgFnPc2	první
pul	poulit	k5eAaImRp2nS	poulit
milionů	milion	k4xCgInPc2	milion
kopií	kopie	k1gFnPc2	kopie
alba	album	k1gNnSc2	album
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
ve	v	k7c6	v
speciálním	speciální	k2eAgInSc6d1	speciální
obalu	obal	k1gInSc6	obal
z	z	k7c2	z
růžové	růžový	k2eAgFnSc2d1	růžová
a	a	k8xC	a
stříbrné	stříbrný	k2eAgFnSc2d1	stříbrná
folie	folie	k1gFnSc2	folie
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2013	[number]	k4	2013
kvůli	kvůli	k7c3	kvůli
předčasnému	předčasný	k2eAgInSc3d1	předčasný
prodeji	prodej	k1gInSc3	prodej
alba	album	k1gNnSc2	album
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
a	a	k8xC	a
Austrálii	Austrálie	k1gFnSc6	Austrálie
se	se	k3xPyFc4	se
celé	celý	k2eAgNnSc1d1	celé
album	album	k1gNnSc1	album
dostalo	dostat	k5eAaPmAgNnS	dostat
na	na	k7c4	na
internet	internet	k1gInSc4	internet
<g/>
.	.	kIx.	.
</s>
<s>
Gaga	Gaga	k6eAd1	Gaga
10	[number]	k4	10
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2013	[number]	k4	2013
zahájila	zahájit	k5eAaPmAgFnS	zahájit
funkci	funkce	k1gFnSc4	funkce
aplikace	aplikace	k1gFnSc2	aplikace
ARTPOP	ARTPOP	kA	ARTPOP
pro	pro	k7c4	pro
chytré	chytrý	k2eAgInPc4d1	chytrý
telefony	telefon	k1gInPc4	telefon
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
fanoušci	fanoušek	k1gMnPc1	fanoušek
budou	být	k5eAaImBp3nP	být
moct	moct	k5eAaImF	moct
časem	čas	k1gInSc7	čas
poslechnout	poslechnout	k5eAaPmF	poslechnout
i	i	k9	i
nevydané	vydaný	k2eNgFnPc4d1	nevydaná
písně	píseň	k1gFnPc4	píseň
z	z	k7c2	z
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
na	na	k7c4	na
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
celosvětové	celosvětový	k2eAgFnSc6d1	celosvětová
hitparádě	hitparáda	k1gFnSc6	hitparáda
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
Lady	lady	k1gFnSc1	lady
Gaga	Gaga	k1gFnSc1	Gaga
stala	stát	k5eAaPmAgFnS	stát
nejúspěšnější	úspěšný	k2eAgFnSc7d3	nejúspěšnější
zpěvačkou	zpěvačka	k1gFnSc7	zpěvačka
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Propagace	propagace	k1gFnSc1	propagace
alba	album	k1gNnSc2	album
==	==	k?	==
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
Gaga	Gag	k1gInSc2	Gag
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
Lady	lady	k1gFnSc1	lady
Gaga	Gag	k1gInSc2	Gag
na	na	k7c4	na
iTunes	iTunes	k1gInSc4	iTunes
Festivalu	festival	k1gInSc2	festival
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
poprvé	poprvé	k6eAd1	poprvé
zazpívala	zazpívat	k5eAaPmAgFnS	zazpívat
písně	píseň	k1gFnPc4	píseň
"	"	kIx"	"
<g/>
Aura	aura	k1gFnSc1	aura
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
MANiCURE	MANiCURE	k1gFnSc1	MANiCURE
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
ARTPOP	ARTPOP	kA	ARTPOP
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Jewels	Jewels	k1gInSc1	Jewels
N	N	kA	N
<g/>
'	'	kIx"	'
Drugs	Drugs	k1gInSc1	Drugs
<g/>
"	"	kIx"	"
společně	společně	k6eAd1	společně
s	s	k7c7	s
T.I	T.I	k1gFnSc7	T.I
<g/>
,	,	kIx,	,
Too	Too	k1gMnSc1	Too
$	$	kIx~	$
<g/>
hort	hort	k1gMnSc1	hort
a	a	k8xC	a
Twista	Twista	k1gMnSc1	Twista
<g/>
.	.	kIx.	.
</s>
<s>
Show	show	k1gFnSc1	show
dále	daleko	k6eAd2	daleko
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
s	s	k7c7	s
"	"	kIx"	"
<g/>
Sexxx	Sexxx	k1gInSc1	Sexxx
Dreams	Dreams	k1gInSc1	Dreams
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Swine	Swin	k1gMnSc5	Swin
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
I	i	k9	i
Wanna	Wanno	k1gNnSc2	Wanno
Be	Be	k1gFnSc2	Be
With	With	k1gMnSc1	With
You	You	k1gMnSc1	You
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
albu	album	k1gNnSc6	album
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
upravené	upravený	k2eAgFnSc6d1	upravená
verzi	verze	k1gFnSc6	verze
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Dope	Dope	k1gInSc1	Dope
<g/>
"	"	kIx"	"
a	a	k8xC	a
celou	celý	k2eAgFnSc4d1	celá
show	show	k1gFnSc4	show
zakončila	zakončit	k5eAaPmAgFnS	zakončit
Applause	Applause	k1gFnSc1	Applause
<g/>
.	.	kIx.	.
</s>
<s>
Propagace	propagace	k1gFnSc1	propagace
alba	album	k1gNnSc2	album
byla	být	k5eAaImAgFnS	být
i	i	k9	i
při	při	k7c6	při
zveřejnění	zveřejnění	k1gNnSc6	zveřejnění
obalu	obal	k1gInSc2	obal
alba	album	k1gNnSc2	album
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
dalo	dát	k5eAaPmAgNnS	dát
přes	přes	k7c4	přes
oficiální	oficiální	k2eAgFnPc4d1	oficiální
internetové	internetový	k2eAgFnPc4d1	internetová
stránky	stránka	k1gFnPc4	stránka
Lady	lady	k1gFnSc1	lady
Gaga	Gag	k2eAgFnSc1d1	Gaga
sledovat	sledovat	k5eAaImF	sledovat
postupné	postupný	k2eAgNnSc4d1	postupné
odhalování	odhalování	k1gNnSc4	odhalování
z	z	k7c2	z
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
9	[number]	k4	9
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2013	[number]	k4	2013
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
VEVO	VEVO	kA	VEVO
vydáno	vydat	k5eAaPmNgNnS	vydat
lyric	lyrice	k1gInPc2	lyrice
video	video	k1gNnSc4	video
se	s	k7c7	s
sestříhanou	sestříhaný	k2eAgFnSc7d1	sestříhaná
verzí	verze	k1gFnSc7	verze
písně	píseň	k1gFnSc2	píseň
"	"	kIx"	"
<g/>
Aura	aura	k1gFnSc1	aura
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
sloužila	sloužit	k5eAaImAgFnS	sloužit
jako	jako	k9	jako
promo	promo	k6eAd1	promo
k	k	k7c3	k
jejímu	její	k3xOp3gInSc3	její
filmovému	filmový	k2eAgInSc3d1	filmový
debutu	debut	k1gInSc3	debut
Machete	Mache	k1gNnSc2	Mache
Kills	Killsa	k1gFnPc2	Killsa
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2013	[number]	k4	2013
uspořádala	uspořádat	k5eAaPmAgFnS	uspořádat
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
poslechovou	poslechový	k2eAgFnSc4d1	poslechová
akci	akce	k1gFnSc4	akce
ARTPOP	ARTPOP	kA	ARTPOP
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
příležitosti	příležitost	k1gFnSc6	příležitost
poskytla	poskytnout	k5eAaPmAgFnS	poskytnout
i	i	k9	i
rozhovor	rozhovor	k1gInSc4	rozhovor
a	a	k8xC	a
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
s	s	k7c7	s
akustickou	akustický	k2eAgFnSc7d1	akustická
verzí	verze	k1gFnSc7	verze
písně	píseň	k1gFnSc2	píseň
"	"	kIx"	"
<g/>
Gypsy	gyps	k1gInPc1	gyps
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2013	[number]	k4	2013
vydala	vydat	k5eAaPmAgFnS	vydat
jako	jako	k9	jako
první	první	k4xOgFnSc1	první
oficiální	oficiální	k2eAgFnSc1d1	oficiální
promo	promo	k6eAd1	promo
singl	singl	k1gInSc1	singl
skladbu	skladba	k1gFnSc4	skladba
Venus	Venus	k1gInSc1	Venus
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
původně	původně	k6eAd1	původně
druhým	druhý	k4xOgInSc7	druhý
oficiálním	oficiální	k2eAgInSc7d1	oficiální
singlem	singl	k1gInSc7	singl
z	z	k7c2	z
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
vydala	vydat	k5eAaPmAgFnS	vydat
jako	jako	k9	jako
druhý	druhý	k4xOgMnSc1	druhý
oficiální	oficiální	k2eAgMnSc1d1	oficiální
promo	promo	k6eAd1	promo
singl	singl	k1gInSc1	singl
z	z	k7c2	z
alba	album	k1gNnSc2	album
skladbu	skladba	k1gFnSc4	skladba
"	"	kIx"	"
<g/>
Dope	Dope	k1gFnSc4	Dope
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
i	i	k9	i
živě	živě	k6eAd1	živě
představila	představit	k5eAaPmAgFnS	představit
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
ročníku	ročník	k1gInSc6	ročník
předávání	předávání	k1gNnSc2	předávání
cen	cena	k1gFnPc2	cena
Youtube	Youtub	k1gInSc5	Youtub
Awards	Awards	k1gInSc4	Awards
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
10	[number]	k4	10
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2013	[number]	k4	2013
se	se	k3xPyFc4	se
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
akce	akce	k1gFnSc1	akce
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
Gaga	Gaga	k1gFnSc1	Gaga
nazvala	nazvat	k5eAaPmAgFnS	nazvat
"	"	kIx"	"
<g/>
artRave	artRavat	k5eAaPmIp3nS	artRavat
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
fanoušci	fanoušek	k1gMnPc1	fanoušek
mohli	moct	k5eAaImAgMnP	moct
poslechnou	poslechnout	k5eAaPmIp3nP	poslechnout
album	album	k1gNnSc4	album
a	a	k8xC	a
Gaga	Gaga	k1gFnSc1	Gaga
zde	zde	k6eAd1	zde
i	i	k9	i
živě	živě	k6eAd1	živě
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
akci	akce	k1gFnSc4	akce
byly	být	k5eAaImAgFnP	být
i	i	k9	i
vystaveny	vystaven	k2eAgFnPc1d1	vystavena
sochy	socha	k1gFnPc1	socha
Gagy	gag	k1gInPc4	gag
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgInP	být
vyrobeny	vyrobit	k5eAaPmNgInP	vyrobit
Jeffem	Jeff	k1gInSc7	Jeff
Koonsem	Koonso	k1gNnSc7	Koonso
pro	pro	k7c4	pro
obal	obal	k1gInSc4	obal
alba	album	k1gNnSc2	album
ARTPOP	ARTPOP	kA	ARTPOP
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
zde	zde	k6eAd1	zde
i	i	k8xC	i
předvedla	předvést	k5eAaPmAgFnS	předvést
létající	létající	k2eAgInPc4d1	létající
šaty	šat	k1gInPc4	šat
VOLANTIS	VOLANTIS	kA	VOLANTIS
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yIgInPc6	který
její	její	k3xOp3gFnSc2	její
tým	tým	k1gInSc4	tým
Haus	Hausa	k1gFnPc2	Hausa
of	of	k?	of
Gaga	Gaga	k1gMnSc1	Gaga
pracoval	pracovat	k5eAaImAgMnS	pracovat
přes	přes	k7c4	přes
2	[number]	k4	2
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
podpořila	podpořit	k5eAaPmAgFnS	podpořit
i	i	k9	i
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
na	na	k7c6	na
vánočním	vánoční	k2eAgInSc6d1	vánoční
koncertě	koncert	k1gInSc6	koncert
Jingle	Jingle	k1gFnSc1	Jingle
Bell	bell	k1gInSc1	bell
Ball	Ball	k1gInSc1	Ball
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
Gaga	Gaga	k1gFnSc1	Gaga
měla	mít	k5eAaImAgFnS	mít
problémy	problém	k1gInPc4	problém
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
společností	společnost	k1gFnSc7	společnost
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
dlouho	dlouho	k6eAd1	dlouho
dobu	doba	k1gFnSc4	doba
žádná	žádný	k3yNgFnSc1	žádný
propagace	propagace	k1gFnSc1	propagace
alba	album	k1gNnSc2	album
nekonala	konat	k5eNaImAgFnS	konat
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
s	s	k7c7	s
písní	píseň	k1gFnPc2	píseň
"	"	kIx"	"
<g/>
ARTPOP	ARTPOP	kA	ARTPOP
<g/>
"	"	kIx"	"
v	v	k7c4	v
Tonight	Tonight	k1gInSc4	Tonight
show	show	k1gFnSc2	show
<g/>
.	.	kIx.	.
</s>
<s>
Pár	pár	k4xCyI	pár
písní	píseň	k1gFnPc2	píseň
z	z	k7c2	z
alba	album	k1gNnSc2	album
také	také	k9	také
představila	představit	k5eAaPmAgFnS	představit
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
v	v	k7c6	v
Austinu	Austin	k1gInSc6	Austin
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
Gaga	Gag	k1gInSc2	Gag
sedmkrát	sedmkrát	k6eAd1	sedmkrát
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
v	v	k7c4	v
Roseland	Roseland	k1gInSc4	Roseland
Ballroom	Ballroom	k1gInSc1	Ballroom
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Singly	singl	k1gInPc4	singl
==	==	k?	==
</s>
</p>
<p>
<s>
Pilotním	pilotní	k2eAgInSc7d1	pilotní
singlem	singl	k1gInSc7	singl
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
poslední	poslední	k2eAgFnSc1d1	poslední
skladba	skladba	k1gFnSc1	skladba
z	z	k7c2	z
alba	album	k1gNnSc2	album
"	"	kIx"	"
<g/>
Applause	Applause	k1gFnSc1	Applause
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
měla	mít	k5eAaImAgFnS	mít
vyjít	vyjít	k5eAaPmF	vyjít
19	[number]	k4	19
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kvůli	kvůli	k7c3	kvůli
úniku	únik	k1gInSc3	únik
částí	část	k1gFnPc2	část
písně	píseň	k1gFnSc2	píseň
na	na	k7c4	na
internet	internet	k1gInSc4	internet
ji	on	k3xPp3gFnSc4	on
Gaga	Gaga	k1gFnSc1	Gaga
vydala	vydat	k5eAaPmAgFnS	vydat
už	už	k6eAd1	už
12	[number]	k4	12
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
ho	on	k3xPp3gInSc4	on
živě	živě	k6eAd1	živě
předvedla	předvést	k5eAaPmAgFnS	předvést
na	na	k7c4	na
udílení	udílení	k1gNnSc4	udílení
cen	cena	k1gFnPc2	cena
MTV	MTV	kA	MTV
Video	video	k1gNnSc1	video
Music	Musice	k1gInPc2	Musice
Awards	Awardsa	k1gFnPc2	Awardsa
<g/>
.	.	kIx.	.
</s>
<s>
Videoklip	videoklip	k1gInSc1	videoklip
vyšel	vyjít	k5eAaPmAgInS	vyjít
19	[number]	k4	19
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2013	[number]	k4	2013
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc7	jeho
režiséry	režisér	k1gMnPc7	režisér
jsou	být	k5eAaImIp3nP	být
nizozemští	nizozemský	k2eAgMnPc1d1	nizozemský
fotografové	fotograf	k1gMnPc1	fotograf
Inez	Ineza	k1gFnPc2	Ineza
van	van	k1gInSc1	van
Lamsweerde	Lamsweerd	k1gInSc5	Lamsweerd
and	and	k?	and
Vinoodh	Vinoodh	k1gInSc4	Vinoodh
Matadin	Matadin	k2eAgInSc4d1	Matadin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
se	se	k3xPyFc4	se
singl	singl	k1gInSc1	singl
dostal	dostat	k5eAaPmAgInS	dostat
nejvýše	nejvýše	k6eAd1	nejvýše
na	na	k7c4	na
4	[number]	k4	4
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
ho	on	k3xPp3gNnSc4	on
někteří	některý	k3yIgMnPc1	některý
označovali	označovat	k5eAaImAgMnP	označovat
za	za	k7c4	za
propadák	propadák	k1gInSc4	propadák
<g/>
.	.	kIx.	.
</s>
<s>
Singl	singl	k1gInSc1	singl
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
jejím	její	k3xOp3gInSc7	její
nejúspěšnějším	úspěšný	k2eAgInSc7d3	nejúspěšnější
hitem	hit	k1gInSc7	hit
v	v	k7c4	v
americký	americký	k2eAgInSc4d1	americký
rádiích	rádio	k1gNnPc6	rádio
za	za	k7c4	za
celou	celá	k1gFnSc4	celá
její	její	k3xOp3gFnSc4	její
kariéru	kariéra	k1gFnSc4	kariéra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Druhým	druhý	k4xOgInSc7	druhý
singlem	singl	k1gInSc7	singl
z	z	k7c2	z
ARTPOP	ARTPOP	kA	ARTPOP
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Do	do	k7c2	do
What	Whata	k1gFnPc2	Whata
U	u	k7c2	u
Want	Wanta	k1gFnPc2	Wanta
<g/>
"	"	kIx"	"
a	a	k8xC	a
vyšla	vyjít	k5eAaPmAgFnS	vyjít
21	[number]	k4	21
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
původně	původně	k6eAd1	původně
vydána	vydán	k2eAgFnSc1d1	vydána
jako	jako	k8xS	jako
promo	promo	k6eAd1	promo
singl	singl	k1gInSc4	singl
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
protože	protože	k8xS	protože
měla	mít	k5eAaImAgFnS	mít
obrovský	obrovský	k2eAgInSc4d1	obrovský
úspěch	úspěch	k1gInSc4	úspěch
několik	několik	k4yIc1	několik
hodin	hodina	k1gFnPc2	hodina
po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
ho	on	k3xPp3gMnSc4	on
Gaga	Gaga	k1gFnSc1	Gaga
změnila	změnit	k5eAaPmAgFnS	změnit
na	na	k7c4	na
druhý	druhý	k4xOgInSc4	druhý
singl	singl	k1gInSc4	singl
místo	místo	k1gNnSc4	místo
Venus	Venus	k1gInSc4	Venus
<g/>
.	.	kIx.	.
</s>
<s>
Nejlepším	dobrý	k2eAgNnSc7d3	nejlepší
umístěním	umístění	k1gNnSc7	umístění
v	v	k7c4	v
Billboard	billboard	k1gInSc4	billboard
Hot	hot	k0	hot
100	[number]	k4	100
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
13	[number]	k4	13
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Videoklip	videoklip	k1gInSc1	videoklip
byl	být	k5eAaImAgInS	být
natočen	natočit	k5eAaBmNgInS	natočit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nevyšel	vyjít	k5eNaPmAgInS	vyjít
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
videoklip	videoklip	k1gInSc1	videoklip
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
příliš	příliš	k6eAd1	příliš
šokující	šokující	k2eAgMnSc1d1	šokující
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
minulosti	minulost	k1gFnSc3	minulost
R.	R.	kA	R.
Kellyho	Kelly	k1gMnSc4	Kelly
nebylo	být	k5eNaImAgNnS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
vyšel	vyjít	k5eAaPmAgInS	vyjít
i	i	k9	i
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
remix	remix	k1gInSc1	remix
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
R.	R.	kA	R.
Kellyho	Kellyha	k1gFnSc5	Kellyha
vokály	vokál	k1gInPc4	vokál
vystřídala	vystřídat	k5eAaPmAgFnS	vystřídat
Christina	Christin	k1gMnSc4	Christin
Aguilera	Aguiler	k1gMnSc4	Aguiler
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Třetím	třetí	k4xOgInSc7	třetí
singlem	singl	k1gInSc7	singl
se	se	k3xPyFc4	se
po	po	k7c6	po
krátce	krátce	k6eAd1	krátce
odmlce	odmlka	k1gFnSc6	odmlka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
řešila	řešit	k5eAaImAgFnS	řešit
Gaga	Gaga	k1gFnSc1	Gaga
problémy	problém	k1gInPc4	problém
se	s	k7c7	s
svým	svůj	k3xOyFgNnSc7	svůj
vydavatelstvím	vydavatelství	k1gNnSc7	vydavatelství
<g/>
,	,	kIx,	,
manažerem	manažer	k1gInSc7	manažer
a	a	k8xC	a
s	s	k7c7	s
videoklip	videoklip	k1gInSc4	videoklip
k	k	k7c3	k
Do	do	k7c2	do
What	Whatum	k1gNnPc2	Whatum
U	u	k7c2	u
Want	Wanta	k1gFnPc2	Wanta
<g/>
,	,	kIx,	,
stala	stát	k5eAaPmAgFnS	stát
píseň	píseň	k1gFnSc1	píseň
G.U.Y.	G.U.Y.	k1gFnSc1	G.U.Y.
Ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
vyslána	vyslat	k5eAaPmNgFnS	vyslat
28	[number]	k4	28
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2014	[number]	k4	2014
do	do	k7c2	do
italských	italský	k2eAgNnPc2d1	italské
rádií	rádio	k1gNnPc2	rádio
a	a	k8xC	a
do	do	k7c2	do
amerických	americký	k2eAgFnPc2d1	americká
až	až	k9	až
8	[number]	k4	8
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Videoklip	videoklip	k1gInSc1	videoklip
vyšel	vyjít	k5eAaPmAgInS	vyjít
již	již	k9	již
o	o	k7c4	o
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
dříve	dříve	k6eAd2	dříve
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
22	[number]	k4	22
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
speciální	speciální	k2eAgInSc4d1	speciální
projekt	projekt	k1gInSc4	projekt
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
dostal	dostat	k5eAaPmAgInS	dostat
název	název	k1gInSc4	název
"	"	kIx"	"
<g/>
An	An	k1gMnSc3	An
ARTPOP	ARTPOP	kA	ARTPOP
film	film	k1gInSc1	film
<g/>
"	"	kIx"	"
a	a	k8xC	a
zahrnoval	zahrnovat	k5eAaImAgInS	zahrnovat
i	i	k9	i
kousky	kousek	k1gInPc4	kousek
písní	píseň	k1gFnPc2	píseň
"	"	kIx"	"
<g/>
ARTPOP	ARTPOP	kA	ARTPOP
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Venus	Venus	k1gInSc1	Venus
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Seznam	seznam	k1gInSc1	seznam
skladeb	skladba	k1gFnPc2	skladba
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Hudební	hudební	k2eAgFnPc1d1	hudební
příčky	příčka	k1gFnPc1	příčka
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Lady	lady	k1gFnSc1	lady
Gaga	Gaga	k1gFnSc1	Gaga
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
third	third	k6eAd1	third
album	album	k1gNnSc4	album
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
