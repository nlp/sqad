<s>
Jurský	jurský	k2eAgInSc1d1	jurský
park	park	k1gInSc1	park
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgInSc1d1	americký
vědeckofantastický	vědeckofantastický	k2eAgInSc1d1	vědeckofantastický
film	film	k1gInSc1	film
režiséra	režisér	k1gMnSc2	režisér
Stevena	Steven	k2eAgFnSc1d1	Stevena
Spielberga	Spielberga	k1gFnSc1	Spielberga
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
knižní	knižní	k2eAgFnSc2d1	knižní
předlohy	předloha	k1gFnSc2	předloha
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgMnSc1d1	známý
americký	americký	k2eAgMnSc1d1	americký
spisovatel	spisovatel	k1gMnSc1	spisovatel
Michael	Michael	k1gMnSc1	Michael
Crichton	Crichton	k1gInSc4	Crichton
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
svůj	svůj	k3xOyFgInSc1	svůj
román	román	k1gInSc1	román
vydal	vydat	k5eAaPmAgInS	vydat
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Práva	právo	k1gNnPc1	právo
ke	k	k7c3	k
zfilmování	zfilmování	k1gNnSc3	zfilmování
nakonec	nakonec	k6eAd1	nakonec
získali	získat	k5eAaPmAgMnP	získat
mezi	mezi	k7c7	mezi
mnoha	mnoho	k4c7	mnoho
dalšími	další	k2eAgMnPc7d1	další
zájemci	zájemce	k1gMnPc7	zájemce
Universal	Universal	k1gMnSc7	Universal
Studios	Studios	k?	Studios
a	a	k8xC	a
sám	sám	k3xTgInSc1	sám
Crichton	Crichton	k1gInSc1	Crichton
byl	být	k5eAaImAgInS	být
najat	najat	k2eAgMnSc1d1	najat
jako	jako	k8xC	jako
scenárista	scenárista	k1gMnSc1	scenárista
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ději	děj	k1gInSc6	děj
je	být	k5eAaImIp3nS	být
využíváno	využívat	k5eAaPmNgNnS	využívat
obrovského	obrovský	k2eAgInSc2d1	obrovský
rozvoje	rozvoj	k1gInSc2	rozvoj
genetického	genetický	k2eAgNnSc2d1	genetické
inženýrství	inženýrství	k1gNnSc2	inženýrství
konce	konec	k1gInSc2	konec
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
k	k	k7c3	k
umožnění	umožnění	k1gNnSc3	umožnění
setkání	setkání	k1gNnSc2	setkání
člověka	člověk	k1gMnSc2	člověk
a	a	k8xC	a
dávno	dávno	k6eAd1	dávno
vyhynulých	vyhynulý	k2eAgFnPc2d1	vyhynulá
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
však	však	k9	však
opět	opět	k6eAd1	opět
naklonovaných	naklonovaný	k2eAgFnPc2d1	naklonovaná
<g/>
,	,	kIx,	,
dinosaurů	dinosaurus	k1gMnPc2	dinosaurus
<g/>
.	.	kIx.	.
</s>
<s>
Multimilionář	multimilionář	k1gMnSc1	multimilionář
John	John	k1gMnSc1	John
Hammond	Hammond	k1gMnSc1	Hammond
vybuduje	vybudovat	k5eAaPmIp3nS	vybudovat
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Isla	Isla	k1gMnSc1	Isla
Nublar	Nublar	k1gMnSc1	Nublar
<g/>
,	,	kIx,	,
vzdáleném	vzdálený	k2eAgInSc6d1	vzdálený
126	[number]	k4	126
mil	míle	k1gFnPc2	míle
od	od	k7c2	od
pobřeží	pobřeží	k1gNnSc2	pobřeží
Kostariky	Kostarika	k1gFnSc2	Kostarika
<g/>
,	,	kIx,	,
atrakci	atrakce	k1gFnSc4	atrakce
na	na	k7c4	na
způsob	způsob	k1gInSc4	způsob
zábavního	zábavní	k2eAgInSc2d1	zábavní
parku	park	k1gInSc2	park
<g/>
,	,	kIx,	,
plnou	plný	k2eAgFnSc4d1	plná
naklonovaných	naklonovaný	k2eAgMnPc2d1	naklonovaný
dinosaurů	dinosaurus	k1gMnPc2	dinosaurus
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
biotechnologická	biotechnologický	k2eAgFnSc1d1	biotechnologická
společnost	společnost	k1gFnSc1	společnost
InGen	InGna	k1gFnPc2	InGna
(	(	kIx(	(
<g/>
International	International	k1gFnSc1	International
Genetic	Genetice	k1gFnPc2	Genetice
Technologies	Technologiesa	k1gFnPc2	Technologiesa
<g/>
)	)	kIx)	)
dinosaury	dinosaurus	k1gMnPc7	dinosaurus
po	po	k7c6	po
mnoha	mnoho	k4c6	mnoho
letech	léto	k1gNnPc6	léto
úsilí	úsilí	k1gNnSc2	úsilí
přivede	přivést	k5eAaPmIp3nS	přivést
k	k	k7c3	k
životu	život	k1gInSc3	život
<g/>
.	.	kIx.	.
</s>
<s>
Potřebnou	potřebný	k2eAgFnSc4d1	potřebná
DNA	dno	k1gNnPc1	dno
dávno	dávno	k6eAd1	dávno
vyhynulých	vyhynulý	k2eAgMnPc2d1	vyhynulý
tvorů	tvor	k1gMnPc2	tvor
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
podaří	podařit	k5eAaPmIp3nS	podařit
získat	získat	k5eAaPmF	získat
z	z	k7c2	z
krev	krev	k1gFnSc4	krev
sajících	sající	k2eAgMnPc2d1	sající
moskytů	moskyt	k1gMnPc2	moskyt
<g/>
,	,	kIx,	,
uchovaných	uchovaný	k2eAgMnPc2d1	uchovaný
ve	v	k7c6	v
výborném	výborný	k2eAgInSc6d1	výborný
stavu	stav	k1gInSc6	stav
v	v	k7c6	v
jantaru	jantar	k1gInSc6	jantar
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
pravěká	pravěký	k2eAgFnSc1d1	pravěká
zkamenělá	zkamenělý	k2eAgFnSc1d1	zkamenělá
pryskyřice	pryskyřice	k1gFnSc1	pryskyřice
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yQgFnSc2	který
se	se	k3xPyFc4	se
hmyz	hmyz	k1gInSc1	hmyz
před	před	k7c7	před
mnoha	mnoho	k4c7	mnoho
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
přilepil	přilepit	k5eAaPmAgInS	přilepit
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
skupuje	skupovat	k5eAaImIp3nS	skupovat
zásoby	zásoba	k1gFnPc4	zásoba
jantaru	jantar	k1gInSc2	jantar
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
a	a	k8xC	a
během	během	k7c2	během
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
je	být	k5eAaImIp3nS	být
park	park	k1gInSc1	park
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
je	být	k5eAaImIp3nS	být
atrakce	atrakce	k1gFnSc1	atrakce
již	již	k6eAd1	již
před	před	k7c7	před
otevřením	otevření	k1gNnSc7	otevření
<g/>
.	.	kIx.	.
</s>
<s>
Hammond	Hammond	k1gInSc1	Hammond
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
pro	pro	k7c4	pro
nervózní	nervózní	k2eAgMnPc4d1	nervózní
investory	investor	k1gMnPc4	investor
dobrozdání	dobrozdání	k1gNnSc4	dobrozdání
o	o	k7c6	o
bezpečnosti	bezpečnost	k1gFnSc6	bezpečnost
parku	park	k1gInSc2	park
od	od	k7c2	od
odborníků	odborník	k1gMnPc2	odborník
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
pozve	pozvat	k5eAaPmIp3nS	pozvat
skupinu	skupina	k1gFnSc4	skupina
vědců	vědec	k1gMnPc2	vědec
(	(	kIx(	(
<g/>
paleontolog	paleontolog	k1gMnSc1	paleontolog
Alan	Alan	k1gMnSc1	Alan
Grant	grant	k1gInSc1	grant
<g/>
,	,	kIx,	,
paleobotanička	paleobotanička	k1gFnSc1	paleobotanička
Ellie	Ellie	k1gFnSc1	Ellie
Sattlerová	Sattlerová	k1gFnSc1	Sattlerová
a	a	k8xC	a
matematik	matematik	k1gMnSc1	matematik
Ian	Ian	k1gMnSc1	Ian
Malcolm	Malcolm	k1gMnSc1	Malcolm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zástupce	zástupce	k1gMnSc1	zástupce
investorů	investor	k1gMnPc2	investor
i	i	k8xC	i
svá	svůj	k3xOyFgNnPc4	svůj
vnoučata	vnouče	k1gNnPc4	vnouče
Tima	Tim	k2eAgNnPc4d1	Tim
a	a	k8xC	a
Lex	Lex	k1gFnSc2	Lex
<g/>
.	.	kIx.	.
</s>
<s>
Přesně	přesně	k6eAd1	přesně
podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
eskalace	eskalace	k1gFnSc2	eskalace
děje	děj	k1gInSc2	děj
však	však	k9	však
dojde	dojít	k5eAaPmIp3nS	dojít
ke	k	k7c3	k
katastrofě	katastrofa	k1gFnSc3	katastrofa
<g/>
,	,	kIx,	,
když	když	k8xS	když
hlavní	hlavní	k2eAgMnSc1d1	hlavní
počítačový	počítačový	k2eAgMnSc1d1	počítačový
technik	technik	k1gMnSc1	technik
Dennis	Dennis	k1gFnSc2	Dennis
Nedry	Nedra	k1gFnSc2	Nedra
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
chce	chtít	k5eAaImIp3nS	chtít
zbohatnout	zbohatnout	k5eAaPmF	zbohatnout
prodejem	prodej	k1gInSc7	prodej
embryí	embryo	k1gNnPc2	embryo
dinosaurů	dinosaurus	k1gMnPc2	dinosaurus
konkurenční	konkurenční	k2eAgFnSc2d1	konkurenční
biotechnologické	biotechnologický	k2eAgFnSc2d1	biotechnologická
společnosti	společnost	k1gFnSc2	společnost
Biosyn	Biosyna	k1gFnPc2	Biosyna
<g/>
,	,	kIx,	,
vypne	vypnout	k5eAaPmIp3nS	vypnout
proud	proud	k1gInSc1	proud
v	v	k7c6	v
ohradách	ohrada	k1gFnPc6	ohrada
pro	pro	k7c4	pro
dinosaury	dinosaurus	k1gMnPc4	dinosaurus
<g/>
.	.	kIx.	.
</s>
<s>
Chce	chtít	k5eAaImIp3nS	chtít
se	se	k3xPyFc4	se
tak	tak	k9	tak
dostat	dostat	k5eAaPmF	dostat
z	z	k7c2	z
chráněného	chráněný	k2eAgInSc2d1	chráněný
parku	park	k1gInSc2	park
do	do	k7c2	do
přístaviště	přístaviště	k1gNnSc2	přístaviště
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
způsobí	způsobit	k5eAaPmIp3nS	způsobit
tím	ten	k3xDgNnSc7	ten
kolaps	kolaps	k1gInSc4	kolaps
všech	všecek	k3xTgNnPc2	všecek
bezpečnostních	bezpečnostní	k2eAgNnPc2d1	bezpečnostní
opatření	opatření	k1gNnPc2	opatření
v	v	k7c6	v
parku	park	k1gInSc6	park
a	a	k8xC	a
všichni	všechen	k3xTgMnPc1	všechen
dinosauři	dinosaurus	k1gMnPc1	dinosaurus
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
nesmírně	smírně	k6eNd1	smírně
nebezpečného	bezpečný	k2eNgMnSc2d1	nebezpečný
tyranosaura	tyranosaur	k1gMnSc2	tyranosaur
a	a	k8xC	a
vysoce	vysoce	k6eAd1	vysoce
inteligentních	inteligentní	k2eAgInPc2d1	inteligentní
velociraptorů	velociraptor	k1gInPc2	velociraptor
se	se	k3xPyFc4	se
dostanou	dostat	k5eAaPmIp3nP	dostat
ven	ven	k6eAd1	ven
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
začnou	začít	k5eAaPmIp3nP	začít
ohrožovat	ohrožovat	k5eAaImF	ohrožovat
životy	život	k1gInPc4	život
návštěvníků	návštěvník	k1gMnPc2	návštěvník
i	i	k8xC	i
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
parku	park	k1gInSc2	park
<g/>
.	.	kIx.	.
</s>
<s>
Nedry	Nedra	k1gFnPc1	Nedra
a	a	k8xC	a
mnozí	mnohý	k2eAgMnPc1d1	mnohý
další	další	k2eAgMnPc1d1	další
přijdou	přijít	k5eAaPmIp3nP	přijít
o	o	k7c4	o
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
povede	povést	k5eAaPmIp3nS	povést
přivolat	přivolat	k5eAaPmF	přivolat
pomoc	pomoc	k1gFnSc4	pomoc
zvenčí	zvenčí	k6eAd1	zvenčí
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Telefony	telefon	k1gInPc1	telefon
jsou	být	k5eAaImIp3nP	být
při	při	k7c6	při
vypnutí	vypnutí	k1gNnSc6	vypnutí
bezpečnostních	bezpečnostní	k2eAgNnPc2d1	bezpečnostní
opatření	opatření	k1gNnPc2	opatření
vyřazeny	vyřadit	k5eAaPmNgFnP	vyřadit
z	z	k7c2	z
provozu	provoz	k1gInSc2	provoz
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
nakonec	nakonec	k6eAd1	nakonec
nutné	nutný	k2eAgNnSc1d1	nutné
použít	použít	k5eAaPmF	použít
dosud	dosud	k6eAd1	dosud
fungující	fungující	k2eAgInSc4d1	fungující
počítač	počítač	k1gInSc4	počítač
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
ostrov	ostrov	k1gInSc4	ostrov
pro	pro	k7c4	pro
jistotu	jistota	k1gFnSc4	jistota
zničen	zničit	k5eAaPmNgInS	zničit
bombovým	bombový	k2eAgInSc7d1	bombový
náletem	nálet	k1gInSc7	nálet
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
ve	v	k7c6	v
filmu	film	k1gInSc6	film
nikoliv	nikoliv	k9	nikoliv
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nikdo	nikdo	k3yNnSc1	nikdo
ale	ale	k9	ale
netuší	tušit	k5eNaImIp3nS	tušit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ještě	ještě	k9	ještě
předtím	předtím	k6eAd1	předtím
překonala	překonat	k5eAaPmAgFnS	překonat
skupina	skupina	k1gFnSc1	skupina
vysoce	vysoce	k6eAd1	vysoce
inteligentních	inteligentní	k2eAgInPc2d1	inteligentní
a	a	k8xC	a
nebezpečných	bezpečný	k2eNgInPc2d1	nebezpečný
velociraptorů	velociraptor	k1gInPc2	velociraptor
moře	moře	k1gNnSc2	moře
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
pevnině	pevnina	k1gFnSc3	pevnina
na	na	k7c6	na
plovoucích	plovoucí	k2eAgInPc6d1	plovoucí
kmenech	kmen	k1gInPc6	kmen
stromů	strom	k1gInPc2	strom
a	a	k8xC	a
nákladních	nákladní	k2eAgFnPc6d1	nákladní
lodích	loď	k1gFnPc6	loď
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pralesích	prales	k1gInPc6	prales
Střední	střední	k2eAgFnSc2d1	střední
Ameriky	Amerika	k1gFnSc2	Amerika
pak	pak	k6eAd1	pak
založí	založit	k5eAaPmIp3nP	založit
novou	nový	k2eAgFnSc4d1	nová
<g/>
,	,	kIx,	,
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
šířící	šířící	k2eAgFnSc4d1	šířící
populaci	populace	k1gFnSc4	populace
chytrých	chytrý	k2eAgMnPc2d1	chytrý
zabijáků	zabiják	k1gMnPc2	zabiják
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
nerozpakují	rozpakovat	k5eNaImIp3nP	rozpakovat
zaútočit	zaútočit	k5eAaPmF	zaútočit
ani	ani	k8xC	ani
na	na	k7c4	na
člověka	člověk	k1gMnSc4	člověk
<g/>
...	...	k?	...
Film	film	k1gInSc1	film
ale	ale	k8xC	ale
končí	končit	k5eAaImIp3nS	končit
jinak	jinak	k6eAd1	jinak
-	-	kIx~	-
v	v	k7c6	v
závěrečné	závěrečný	k2eAgFnSc6d1	závěrečná
epické	epický	k2eAgFnSc6d1	epická
scéně	scéna	k1gFnSc6	scéna
útočí	útočit	k5eAaImIp3nS	útočit
na	na	k7c4	na
Granta	Grant	k1gMnSc4	Grant
<g/>
,	,	kIx,	,
Sattlerovou	Sattlerová	k1gFnSc4	Sattlerová
a	a	k8xC	a
obě	dva	k4xCgFnPc4	dva
děti	dítě	k1gFnPc4	dítě
dva	dva	k4xCgMnPc4	dva
velociraptoři	velociraptor	k1gMnPc1	velociraptor
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
pro	pro	k7c4	pro
návštěvníky	návštěvník	k1gMnPc4	návštěvník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
již	již	k9	již
jeden	jeden	k4xCgMnSc1	jeden
dravec	dravec	k1gMnSc1	dravec
vymrští	vymrštit	k5eAaPmIp3nS	vymrštit
do	do	k7c2	do
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
však	však	k9	však
vnikne	vniknout	k5eAaPmIp3nS	vniknout
dovnitř	dovnitř	k6eAd1	dovnitř
tyranosaurus	tyranosaurus	k1gInSc4	tyranosaurus
a	a	k8xC	a
rozdrtí	rozdrtit	k5eAaPmIp3nP	rozdrtit
ho	on	k3xPp3gMnSc4	on
v	v	k7c6	v
čelistech	čelist	k1gFnPc6	čelist
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
raptor	raptor	k1gInSc1	raptor
s	s	k7c7	s
mohutným	mohutný	k2eAgInSc7d1	mohutný
odrazem	odraz	k1gInSc7	odraz
skočí	skočit	k5eAaPmIp3nP	skočit
tyranosaurovi	tyranosaurův	k2eAgMnPc1d1	tyranosaurův
na	na	k7c4	na
záda	záda	k1gNnPc4	záda
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
proti	proti	k7c3	proti
němu	on	k3xPp3gMnSc3	on
příliš	příliš	k6eAd1	příliš
slabý	slabý	k2eAgInSc1d1	slabý
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
je	být	k5eAaImIp3nS	být
také	také	k6eAd1	také
zabit	zabít	k5eAaPmNgMnS	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
všichni	všechen	k3xTgMnPc1	všechen
lidští	lidský	k2eAgMnPc1d1	lidský
hrdinové	hrdina	k1gMnPc1	hrdina
utíkají	utíkat	k5eAaImIp3nP	utíkat
k	k	k7c3	k
džípu	džíp	k1gInSc3	džíp
a	a	k8xC	a
odjedou	odjet	k5eAaPmIp3nP	odjet
k	k	k7c3	k
heliportu	heliport	k1gInSc3	heliport
<g/>
.	.	kIx.	.
</s>
<s>
Odlétají	odlétat	k5eAaImIp3nP	odlétat
z	z	k7c2	z
ostrova	ostrov	k1gInSc2	ostrov
a	a	k8xC	a
cestou	cesta	k1gFnSc7	cesta
pozorují	pozorovat	k5eAaImIp3nP	pozorovat
ptáky	pták	k1gMnPc7	pták
letící	letící	k2eAgFnSc2d1	letící
nad	nad	k7c7	nad
hladinou	hladina	k1gFnSc7	hladina
-	-	kIx~	-
jediné	jediný	k2eAgNnSc1d1	jediné
skutečně	skutečně	k6eAd1	skutečně
žijící	žijící	k2eAgMnPc4d1	žijící
potomky	potomek	k1gMnPc4	potomek
dinosaurů	dinosaurus	k1gMnPc2	dinosaurus
<g/>
...	...	k?	...
Crichtonova	Crichtonův	k2eAgFnSc1d1	Crichtonova
kniha	kniha	k1gFnSc1	kniha
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
vydána	vydán	k2eAgFnSc1d1	vydána
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
a	a	k8xC	a
ihned	ihned	k6eAd1	ihned
bylo	být	k5eAaImAgNnS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
zanedlouho	zanedlouho	k6eAd1	zanedlouho
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
i	i	k9	i
film	film	k1gInSc1	film
podle	podle	k7c2	podle
této	tento	k3xDgFnSc2	tento
podnětné	podnětný	k2eAgFnSc2d1	podnětná
předlohy	předloha	k1gFnSc2	předloha
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
pak	pak	k6eAd1	pak
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
Universal	Universal	k1gFnPc2	Universal
Studios	Studios	k?	Studios
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
natočen	natočit	k5eAaBmNgInS	natočit
pod	pod	k7c7	pod
taktovkou	taktovka	k1gFnSc7	taktovka
úspěšného	úspěšný	k2eAgMnSc2d1	úspěšný
režiséra	režisér	k1gMnSc2	režisér
Stevena	Steven	k2eAgFnSc1d1	Stevena
Spielberga	Spielberga	k1gFnSc1	Spielberga
podle	podle	k7c2	podle
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
románu	román	k1gInSc2	román
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
kin	kino	k1gNnPc2	kino
uveden	uvést	k5eAaPmNgInS	uvést
11	[number]	k4	11
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1993	[number]	k4	1993
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc4	jeho
tržby	tržba	k1gFnPc4	tržba
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
přesáhly	přesáhnout	k5eAaPmAgInP	přesáhnout
celosvětově	celosvětově	k6eAd1	celosvětově
914	[number]	k4	914
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Jurský	jurský	k2eAgInSc1d1	jurský
park	park	k1gInSc1	park
na	na	k7c4	na
dlouho	dlouho	k6eAd1	dlouho
nejúspěšnějším	úspěšný	k2eAgInSc7d3	nejúspěšnější
filmem	film	k1gInSc7	film
vůbec	vůbec	k9	vůbec
a	a	k9	a
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
kolem	kolem	k7c2	kolem
16	[number]	k4	16
<g/>
.	.	kIx.	.
místa	místo	k1gNnSc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Natočila	natočit	k5eAaBmAgFnS	natočit
se	se	k3xPyFc4	se
také	také	k9	také
zatím	zatím	k6eAd1	zatím
tři	tři	k4xCgNnPc4	tři
pokračování	pokračování	k1gNnPc4	pokračování
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
byl	být	k5eAaImAgInS	být
uveden	uvést	k5eAaPmNgInS	uvést
čtvrtý	čtvrtý	k4xOgInSc1	čtvrtý
díl	díl	k1gInSc1	díl
této	tento	k3xDgFnSc2	tento
veleúspěšné	veleúspěšný	k2eAgFnSc2d1	veleúspěšná
série	série	k1gFnSc2	série
<g/>
,	,	kIx,	,
jménem	jméno	k1gNnSc7	jméno
Jurský	jurský	k2eAgInSc1d1	jurský
svět	svět	k1gInSc1	svět
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
prvním	první	k4xOgInSc7	první
filmem	film	k1gInSc7	film
nové	nový	k2eAgFnSc2d1	nová
trilogie	trilogie	k1gFnSc2	trilogie
Jurského	jurský	k2eAgInSc2d1	jurský
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Čtyři	čtyři	k4xCgInPc1	čtyři
dosavadní	dosavadní	k2eAgInPc1d1	dosavadní
filmy	film	k1gInPc1	film
(	(	kIx(	(
<g/>
Jurský	jurský	k2eAgInSc1d1	jurský
park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
Ztracený	ztracený	k2eAgInSc1d1	ztracený
svět	svět	k1gInSc1	svět
<g/>
:	:	kIx,	:
Jurský	jurský	k2eAgInSc1d1	jurský
park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
Jurský	jurský	k2eAgInSc1d1	jurský
park	park	k1gInSc1	park
3	[number]	k4	3
a	a	k8xC	a
Jurský	jurský	k2eAgInSc1d1	jurský
svět	svět	k1gInSc1	svět
<g/>
)	)	kIx)	)
dohromady	dohromady	k6eAd1	dohromady
vydělaly	vydělat	k5eAaPmAgInP	vydělat
svým	svůj	k3xOyFgMnPc3	svůj
tvůrcům	tvůrce	k1gMnPc3	tvůrce
přes	přes	k7c4	přes
2	[number]	k4	2
miliardy	miliarda	k4xCgFnSc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Úspěch	úspěch	k1gInSc4	úspěch
podpořilo	podpořit	k5eAaPmAgNnS	podpořit
i	i	k9	i
první	první	k4xOgNnSc4	první
rozsáhlé	rozsáhlý	k2eAgNnSc4d1	rozsáhlé
využití	využití	k1gNnSc4	využití
filmových	filmový	k2eAgInPc2d1	filmový
počítačových	počítačový	k2eAgInPc2d1	počítačový
triků	trik	k1gInPc2	trik
společnosti	společnost	k1gFnSc2	společnost
Industrial	Industrial	k1gMnSc1	Industrial
Light	Light	k1gMnSc1	Light
and	and	k?	and
Magic	Magic	k1gMnSc1	Magic
<g/>
,	,	kIx,	,
známé	známý	k2eAgFnPc1d1	známá
například	například	k6eAd1	například
z	z	k7c2	z
Hvězdných	hvězdný	k2eAgFnPc2d1	hvězdná
válek	válka	k1gFnPc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Dinosauři	dinosaurus	k1gMnPc1	dinosaurus
byli	být	k5eAaImAgMnP	být
ve	v	k7c6	v
filmu	film	k1gInSc6	film
zobrazeni	zobrazit	k5eAaPmNgMnP	zobrazit
nesmírně	smírně	k6eNd1	smírně
realistickým	realistický	k2eAgInSc7d1	realistický
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
nebyl	být	k5eNaImAgMnS	být
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
starších	starý	k2eAgFnPc2d2	starší
trikových	trikový	k2eAgFnPc2d1	triková
technik	technika	k1gFnPc2	technika
vůbec	vůbec	k9	vůbec
myslitelný	myslitelný	k2eAgInSc4d1	myslitelný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
dílu	díl	k1gInSc6	díl
byla	být	k5eAaImAgFnS	být
počítačová	počítačový	k2eAgFnSc1d1	počítačová
animace	animace	k1gFnSc1	animace
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
celkem	celkem	k6eAd1	celkem
jen	jen	k9	jen
asi	asi	k9	asi
6,5	[number]	k4	6,5
minuty	minuta	k1gFnPc4	minuta
<g/>
,	,	kIx,	,
v	v	k7c6	v
pokračování	pokračování	k1gNnSc6	pokračování
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
však	však	k9	však
už	už	k6eAd1	už
přes	přes	k7c4	přes
20	[number]	k4	20
minut	minuta	k1gFnPc2	minuta
a	a	k8xC	a
ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
Jurském	jurský	k2eAgInSc6d1	jurský
parku	park	k1gInSc6	park
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
o	o	k7c4	o
půl	půl	k1xP	půl
hodiny	hodina	k1gFnSc2	hodina
kratší	krátký	k2eAgFnSc2d2	kratší
než	než	k8xS	než
oba	dva	k4xCgInPc4	dva
předchozí	předchozí	k2eAgInPc4d1	předchozí
díly	díl	k1gInPc4	díl
<g/>
,	,	kIx,	,
představovala	představovat	k5eAaImAgFnS	představovat
počítačová	počítačový	k2eAgFnSc1d1	počítačová
animace	animace	k1gFnSc1	animace
ještě	ještě	k9	ještě
větší	veliký	k2eAgInSc4d2	veliký
podíl	podíl	k1gInSc4	podíl
z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Jurském	jurský	k2eAgInSc6d1	jurský
parku	park	k1gInSc6	park
se	se	k3xPyFc4	se
na	na	k7c6	na
jejich	jejich	k3xOp3gInSc6	jejich
vývoji	vývoj	k1gInSc6	vývoj
podílel	podílet	k5eAaImAgInS	podílet
zejména	zejména	k9	zejména
Dennis	Dennis	k1gInSc1	Dennis
Muren	Murna	k1gFnPc2	Murna
a	a	k8xC	a
Phil	Phil	k1gMnSc1	Phil
Tippett	Tippett	k1gMnSc1	Tippett
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
počítačově	počítačově	k6eAd1	počítačově
vytvořené	vytvořený	k2eAgMnPc4d1	vytvořený
dinosaury	dinosaurus	k1gMnPc4	dinosaurus
v	v	k7c6	v
JP	JP	kA	JP
navázala	navázat	k5eAaPmAgFnS	navázat
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
pseudo-dokumentární	pseudookumentární	k2eAgFnSc1d1	pseudo-dokumentární
série	série	k1gFnSc1	série
BBC	BBC	kA	BBC
Putování	putování	k1gNnSc1	putování
s	s	k7c7	s
dinosaury	dinosaurus	k1gMnPc7	dinosaurus
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
celý	celý	k2eAgInSc4d1	celý
průřez	průřez	k1gInSc4	průřez
druhohorním	druhohorní	k2eAgInSc7d1	druhohorní
světem	svět	k1gInSc7	svět
tvoří	tvořit	k5eAaImIp3nS	tvořit
právě	právě	k9	právě
velmi	velmi	k6eAd1	velmi
zdařilá	zdařilý	k2eAgFnSc1d1	zdařilá
a	a	k8xC	a
vědecky	vědecky	k6eAd1	vědecky
fundovaná	fundovaný	k2eAgFnSc1d1	fundovaná
počítačová	počítačový	k2eAgFnSc1d1	počítačová
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
mnoha	mnoho	k4c3	mnoho
druhů	druh	k1gInPc2	druh
dinosaurů	dinosaurus	k1gMnPc2	dinosaurus
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
obecně	obecně	k6eAd1	obecně
uznává	uznávat	k5eAaImIp3nS	uznávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
premiéra	premiéra	k1gFnSc1	premiéra
filmu	film	k1gInSc2	film
Jurský	jurský	k2eAgInSc1d1	jurský
park	park	k1gInSc1	park
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
spustila	spustit	k5eAaPmAgFnS	spustit
celosvětovou	celosvětový	k2eAgFnSc4d1	celosvětová
vlnu	vlna	k1gFnSc4	vlna
zájmu	zájem	k1gInSc2	zájem
o	o	k7c4	o
dinosaury	dinosaurus	k1gMnPc4	dinosaurus
a	a	k8xC	a
paleontologii	paleontologie	k1gFnSc4	paleontologie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
ve	v	k7c6	v
zvýšené	zvýšený	k2eAgFnSc6d1	zvýšená
míře	míra	k1gFnSc6	míra
udržuje	udržovat	k5eAaImIp3nS	udržovat
zvláště	zvláště	k6eAd1	zvláště
u	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
náctiletých	náctiletý	k2eAgFnPc2d1	náctiletá
téměř	téměř	k6eAd1	téměř
všech	všecek	k3xTgFnPc2	všecek
zemí	zem	k1gFnPc2	zem
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Alan	Alan	k1gMnSc1	Alan
Grant	grant	k1gInSc1	grant
(	(	kIx(	(
<g/>
Sam	Sam	k1gMnSc1	Sam
Neill	Neill	k1gMnSc1	Neill
<g/>
)	)	kIx)	)
-	-	kIx~	-
paleontolog	paleontolog	k1gMnSc1	paleontolog
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
návštěvníků	návštěvník	k1gMnPc2	návštěvník
parku	park	k1gInSc2	park
Ellie	Ellie	k1gFnSc1	Ellie
Sattlerová	Sattlerová	k1gFnSc1	Sattlerová
(	(	kIx(	(
<g/>
Laura	Laura	k1gFnSc1	Laura
Dern	Dern	k1gMnSc1	Dern
<g/>
)	)	kIx)	)
-	-	kIx~	-
paleobotanička	paleobotanička	k1gFnSc1	paleobotanička
<g/>
,	,	kIx,	,
návštěvnice	návštěvnice	k1gFnPc1	návštěvnice
parku	park	k1gInSc2	park
John	John	k1gMnSc1	John
Hammond	Hammond	k1gMnSc1	Hammond
(	(	kIx(	(
<g/>
Richard	Richard	k1gMnSc1	Richard
Attenborough	Attenborough	k1gMnSc1	Attenborough
<g/>
)	)	kIx)	)
-	-	kIx~	-
multimilionář	multimilionář	k1gMnSc1	multimilionář
<g/>
,	,	kIx,	,
majitel	majitel	k1gMnSc1	majitel
a	a	k8xC	a
tvůrce	tvůrce	k1gMnSc1	tvůrce
parku	park	k1gInSc2	park
Ian	Ian	k1gMnSc1	Ian
Malcolm	Malcolm	k1gMnSc1	Malcolm
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Jeff	Jeff	k1gInSc1	Jeff
Goldblum	Goldblum	k1gInSc1	Goldblum
<g/>
)	)	kIx)	)
-	-	kIx~	-
matematik	matematik	k1gMnSc1	matematik
<g/>
,	,	kIx,	,
zastánce	zastánce	k1gMnSc1	zastánce
teorie	teorie	k1gFnSc1	teorie
chaosu	chaos	k1gInSc3	chaos
Robert	Robert	k1gMnSc1	Robert
Muldoon	Muldoon	k1gMnSc1	Muldoon
(	(	kIx(	(
<g/>
Bob	Bob	k1gMnSc1	Bob
Peck	Peck	k1gMnSc1	Peck
<g/>
)	)	kIx)	)
-	-	kIx~	-
kontrolor	kontrolor	k1gMnSc1	kontrolor
zvěře	zvěř	k1gFnSc2	zvěř
pro	pro	k7c4	pro
Jurský	jurský	k2eAgInSc4d1	jurský
park	park	k1gInSc4	park
Dennis	Dennis	k1gFnSc2	Dennis
Nedry	Nedra	k1gFnSc2	Nedra
(	(	kIx(	(
<g/>
Wayne	Wayn	k1gInSc5	Wayn
Knight	Knight	k2eAgMnSc1d1	Knight
<g/>
)	)	kIx)	)
-	-	kIx~	-
počítačový	počítačový	k2eAgMnSc1d1	počítačový
technik	technik	k1gMnSc1	technik
Donald	Donald	k1gMnSc1	Donald
Gennaro	Gennara	k1gFnSc5	Gennara
(	(	kIx(	(
<g/>
Martin	Martin	k1gInSc1	Martin
Ferrero	Ferrero	k1gNnSc1	Ferrero
<g/>
)	)	kIx)	)
-	-	kIx~	-
právník	právník	k1gMnSc1	právník
Alexis	Alexis	k1gFnSc2	Alexis
Murphy	Murpha	k1gFnSc2	Murpha
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Ariana	Ariana	k1gFnSc1	Ariana
Richards	Richardsa	k1gFnPc2	Richardsa
<g/>
)	)	kIx)	)
-	-	kIx~	-
Hammondova	Hammondův	k2eAgFnSc1d1	Hammondova
vnučka	vnučka	k1gFnSc1	vnučka
Tim	Tim	k?	Tim
Murphy	Murpha	k1gFnSc2	Murpha
(	(	kIx(	(
<g/>
Joseph	Joseph	k1gInSc1	Joseph
Mazzello	Mazzello	k1gNnSc1	Mazzello
<g/>
)	)	kIx)	)
-	-	kIx~	-
Hammondův	Hammondův	k2eAgMnSc1d1	Hammondův
vnuk	vnuk	k1gMnSc1	vnuk
John	John	k1gMnSc1	John
Arnold	Arnold	k1gMnSc1	Arnold
(	(	kIx(	(
<g/>
Samuel	Samuel	k1gMnSc1	Samuel
L.	L.	kA	L.
Jackson	Jackson	k1gMnSc1	Jackson
<g/>
)	)	kIx)	)
-	-	kIx~	-
počítačový	počítačový	k2eAgMnSc1d1	počítačový
technik	technik	k1gMnSc1	technik
Henry	Henry	k1gMnSc1	Henry
Wu	Wu	k1gMnSc1	Wu
(	(	kIx(	(
<g/>
B.D.	B.D.	k1gMnSc1	B.D.
Wong	Wong	k1gMnSc1	Wong
<g/>
)	)	kIx)	)
-	-	kIx~	-
genetik	genetik	k1gMnSc1	genetik
Gerry	Gerra	k1gFnSc2	Gerra
Harding	Harding	k1gInSc1	Harding
(	(	kIx(	(
<g/>
Gerald	Gerald	k1gInSc1	Gerald
R.	R.	kA	R.
Molen	Molen	k1gInSc1	Molen
<g/>
)	)	kIx)	)
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
parkový	parkový	k2eAgMnSc1d1	parkový
zvěrolékař	zvěrolékař	k1gMnSc1	zvěrolékař
Lewis	Lewis	k1gFnSc2	Lewis
Dodgson	Dodgson	k1gMnSc1	Dodgson
(	(	kIx(	(
<g/>
Cameron	Cameron	k1gMnSc1	Cameron
Thor	Thor	k1gMnSc1	Thor
<g/>
)	)	kIx)	)
-	-	kIx~	-
vedoucí	vedoucí	k1gMnPc1	vedoucí
konkurenční	konkurenční	k2eAgFnSc2d1	konkurenční
firmy	firma	k1gFnSc2	firma
Biosyn	Biosyn	k1gMnSc1	Biosyn
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
podplatí	podplatit	k5eAaPmIp3nS	podplatit
Nedryho	Nedry	k1gMnSc4	Nedry
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
v	v	k7c6	v
parku	park	k1gInSc6	park
ukradl	ukradnout	k5eAaPmAgInS	ukradnout
dinosauří	dinosauří	k2eAgInSc1d1	dinosauří
embrya	embryo	k1gNnSc2	embryo
Pracovník	pracovník	k1gMnSc1	pracovník
v	v	k7c6	v
docích	dok	k1gInPc6	dok
(	(	kIx(	(
<g/>
Dean	Dean	k1gMnSc1	Dean
Cundey	Cundea	k1gFnSc2	Cundea
<g/>
)	)	kIx)	)
-	-	kIx~	-
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
producentů	producent	k1gMnPc2	producent
filmu	film	k1gInSc2	film
si	se	k3xPyFc3	se
zahrál	zahrát	k5eAaPmAgMnS	zahrát
roli	role	k1gFnSc4	role
kapitána	kapitán	k1gMnSc2	kapitán
lodi	loď	k1gFnSc2	loď
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
<g />
.	.	kIx.	.
</s>
<s>
konverzuje	konverzovat	k5eAaImIp3nS	konverzovat
s	s	k7c7	s
Nedrym	Nedrym	k1gInSc1	Nedrym
Hlas	hlas	k1gInSc1	hlas
pana	pan	k1gMnSc2	pan
DNA	DNA	kA	DNA
(	(	kIx(	(
<g/>
animace	animace	k1gFnSc1	animace
pro	pro	k7c4	pro
návštěvníky	návštěvník	k1gMnPc4	návštěvník
<g/>
)	)	kIx)	)
-	-	kIx~	-
v	v	k7c6	v
podání	podání	k1gNnSc6	podání
Grega	Greg	k1gMnSc2	Greg
Bursona	Burson	k1gMnSc2	Burson
Richard	Richard	k1gMnSc1	Richard
Kiley	Kilea	k1gMnSc2	Kilea
-	-	kIx~	-
mluví	mluvit	k5eAaImIp3nS	mluvit
sám	sám	k3xTgMnSc1	sám
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
hlas	hlas	k1gInSc1	hlas
v	v	k7c6	v
projížďkových	projížďkův	k2eAgInPc6d1	projížďkův
autech	aut	k1gInPc6	aut
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
se	se	k3xPyFc4	se
objevili	objevit	k5eAaPmAgMnP	objevit
dinosauři	dinosaurus	k1gMnPc1	dinosaurus
rodu	rod	k1gInSc2	rod
Parasaurolophus	Parasaurolophus	k1gInSc1	Parasaurolophus
<g/>
,	,	kIx,	,
Triceratops	Triceratops	k1gInSc1	Triceratops
<g/>
,	,	kIx,	,
Brachiosaurus	Brachiosaurus	k1gMnSc1	Brachiosaurus
<g/>
,	,	kIx,	,
Gallimimus	Gallimimus	k1gMnSc1	Gallimimus
<g/>
,	,	kIx,	,
Tyrannosaurus	Tyrannosaurus	k1gMnSc1	Tyrannosaurus
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Velociraptor	Velociraptor	k1gInSc4	Velociraptor
a	a	k8xC	a
Dilophosaurus	Dilophosaurus	k1gInSc4	Dilophosaurus
<g/>
,	,	kIx,	,
v	v	k7c6	v
chladící	chladící	k2eAgFnSc6d1	chladící
místnosti	místnost	k1gFnSc6	místnost
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
objevují	objevovat	k5eAaImIp3nP	objevovat
i	i	k8xC	i
vzorky	vzorek	k1gInPc4	vzorek
DNA	dno	k1gNnSc2	dno
méně	málo	k6eAd2	málo
známých	známý	k2eAgInPc2d1	známý
dinosauřích	dinosauří	k2eAgInPc2d1	dinosauří
rodů	rod	k1gInPc2	rod
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
Metriacanthosaurus	Metriacanthosaurus	k1gInSc1	Metriacanthosaurus
<g/>
,	,	kIx,	,
Proceratosaurus	Proceratosaurus	k1gInSc1	Proceratosaurus
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
populární	populární	k2eAgInSc1d1	populární
Stegosaurus	Stegosaurus	k1gInSc1	Stegosaurus
(	(	kIx(	(
<g/>
ve	v	k7c6	v
filmu	film	k1gInSc6	film
chybně	chybně	k6eAd1	chybně
označen	označit	k5eAaPmNgMnS	označit
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Stegasaurus	Stegasaurus	k1gMnSc1	Stegasaurus
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Paradoxně	paradoxně	k6eAd1	paradoxně
jen	jen	k6eAd1	jen
dva	dva	k4xCgMnPc4	dva
z	z	k7c2	z
těchto	tento	k3xDgMnPc2	tento
sedmi	sedm	k4xCc2	sedm
rodů	rod	k1gInPc2	rod
<g/>
,	,	kIx,	,
Brachiosaurus	Brachiosaurus	k1gMnSc1	Brachiosaurus
a	a	k8xC	a
Dilophosaurus	Dilophosaurus	k1gMnSc1	Dilophosaurus
<g/>
,	,	kIx,	,
pocházely	pocházet	k5eAaImAgFnP	pocházet
opravdu	opravdu	k6eAd1	opravdu
z	z	k7c2	z
období	období	k1gNnSc2	období
jury	jura	k1gFnSc2	jura
(	(	kIx(	(
<g/>
před	před	k7c7	před
201-145	[number]	k4	201-145
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1	ostatní
rody	rod	k1gInPc1	rod
žily	žít	k5eAaImAgInP	žít
až	až	k9	až
v	v	k7c6	v
pozdějším	pozdní	k2eAgNnSc6d2	pozdější
období	období	k1gNnSc6	období
křídy	křída	k1gFnSc2	křída
(	(	kIx(	(
<g/>
před	před	k7c7	před
145-66	[number]	k4	145-66
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Crichtonově	Crichtonův	k2eAgInSc6d1	Crichtonův
románu	román	k1gInSc6	román
Jurský	jurský	k2eAgInSc1d1	jurský
park	park	k1gInSc1	park
(	(	kIx(	(
<g/>
román	román	k1gInSc1	román
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
předlohou	předloha	k1gFnSc7	předloha
filmu	film	k1gInSc2	film
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
však	však	k9	však
objevuje	objevovat	k5eAaImIp3nS	objevovat
podstatně	podstatně	k6eAd1	podstatně
více	hodně	k6eAd2	hodně
dinosauřích	dinosauří	k2eAgInPc2d1	dinosauří
rodů	rod	k1gInPc2	rod
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
zmíněných	zmíněný	k2eAgMnPc2d1	zmíněný
ještě	ještě	k6eAd1	ještě
například	například	k6eAd1	například
Apatosaurus	Apatosaurus	k1gInSc4	Apatosaurus
(	(	kIx(	(
<g/>
nazývaný	nazývaný	k2eAgMnSc1d1	nazývaný
také	také	k9	také
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Brontosaurus	brontosaurus	k1gMnSc1	brontosaurus
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Procompsognathus	Procompsognathus	k1gInSc1	Procompsognathus
<g/>
,	,	kIx,	,
Maiasaura	Maiasaura	k1gFnSc1	Maiasaura
<g/>
,	,	kIx,	,
Hypsilophodon	Hypsilophodon	k1gMnSc1	Hypsilophodon
<g/>
,	,	kIx,	,
Stegosaurus	Stegosaurus	k1gMnSc1	Stegosaurus
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
jsou	být	k5eAaImIp3nP	být
počítačově	počítačově	k6eAd1	počítačově
vytvoření	vytvoření	k1gNnSc4	vytvoření
dinosauři	dinosaurus	k1gMnPc1	dinosaurus
pokládáni	pokládán	k2eAgMnPc1d1	pokládán
za	za	k7c4	za
jakýsi	jakýsi	k3yIgInSc4	jakýsi
vzor	vzor	k1gInSc4	vzor
moderního	moderní	k2eAgNnSc2d1	moderní
pojetí	pojetí	k1gNnSc2	pojetí
<g/>
,	,	kIx,	,
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
se	se	k3xPyFc4	se
tvůrci	tvůrce	k1gMnPc7	tvůrce
neubránili	ubránit	k5eNaPmAgMnP	ubránit
mnoha	mnoho	k4c3	mnoho
chybám	chyba	k1gFnPc3	chyba
v	v	k7c6	v
jejich	jejich	k3xOp3gNnSc6	jejich
zpodobnění	zpodobnění	k1gNnSc6	zpodobnění
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejzávažnější	závažný	k2eAgInPc4d3	nejzávažnější
omyly	omyl	k1gInPc4	omyl
patří	patřit	k5eAaImIp3nS	patřit
příliš	příliš	k6eAd1	příliš
velký	velký	k2eAgInSc1d1	velký
a	a	k8xC	a
neopeřený	opeřený	k2eNgInSc1d1	neopeřený
Velociraptor	Velociraptor	k1gInSc1	Velociraptor
(	(	kIx(	(
<g/>
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
byl	být	k5eAaImAgMnS	být
asi	asi	k9	asi
poloviční	poloviční	k2eAgFnSc3d1	poloviční
velikosti	velikost	k1gFnSc3	velikost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
příliš	příliš	k6eAd1	příliš
malý	malý	k2eAgInSc1d1	malý
Dilophosaurus	Dilophosaurus	k1gInSc1	Dilophosaurus
(	(	kIx(	(
<g/>
naopak	naopak	k6eAd1	naopak
asi	asi	k9	asi
dvojnásobné	dvojnásobný	k2eAgFnSc2d1	dvojnásobná
velikosti	velikost	k1gFnSc2	velikost
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
příliš	příliš	k6eAd1	příliš
rychle	rychle	k6eAd1	rychle
běhající	běhající	k2eAgInSc4d1	běhající
Tyrannosaurus	Tyrannosaurus	k1gInSc4	Tyrannosaurus
<g/>
.	.	kIx.	.
</s>
<s>
Vědeckých	vědecký	k2eAgFnPc2d1	vědecká
chyb	chyba	k1gFnPc2	chyba
v	v	k7c4	v
zobrazení	zobrazení	k1gNnSc4	zobrazení
dinosaurů	dinosaurus	k1gMnPc2	dinosaurus
je	být	k5eAaImIp3nS	být
však	však	k9	však
ještě	ještě	k6eAd1	ještě
více	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
film	film	k1gInSc1	film
přesto	přesto	k8xC	přesto
představuje	představovat	k5eAaImIp3nS	představovat
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
snímek	snímek	k1gInSc4	snímek
svého	svůj	k3xOyFgInSc2	svůj
druhu	druh	k1gInSc2	druh
a	a	k8xC	a
i	i	k9	i
přes	přes	k7c4	přes
zmíněné	zmíněný	k2eAgFnPc4d1	zmíněná
chybky	chybka	k1gFnPc4	chybka
a	a	k8xC	a
nepřesnosti	nepřesnost	k1gFnPc4	nepřesnost
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zobrazení	zobrazení	k1gNnSc6	zobrazení
dinosaurů	dinosaurus	k1gMnPc2	dinosaurus
na	na	k7c4	na
svoji	svůj	k3xOyFgFnSc4	svůj
dobu	doba	k1gFnSc4	doba
vysoce	vysoce	k6eAd1	vysoce
kvalitní	kvalitní	k2eAgMnSc1d1	kvalitní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
možnost	možnost	k1gFnSc4	možnost
klonování	klonování	k1gNnSc2	klonování
druhohorních	druhohorní	k2eAgMnPc2d1	druhohorní
dinosaurů	dinosaurus	k1gMnPc2	dinosaurus
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
hmyzu	hmyz	k1gInSc2	hmyz
uchovaného	uchovaný	k2eAgInSc2d1	uchovaný
v	v	k7c6	v
jantaru	jantar	k1gInSc6	jantar
zřejmě	zřejmě	k6eAd1	zřejmě
nereálná	reálný	k2eNgFnSc1d1	nereálná
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
důvodů	důvod	k1gInPc2	důvod
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
je	být	k5eAaImIp3nS	být
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
že	že	k8xS	že
ani	ani	k8xC	ani
v	v	k7c6	v
případě	případ	k1gInSc6	případ
dokonale	dokonale	k6eAd1	dokonale
zachovaného	zachovaný	k2eAgMnSc2d1	zachovaný
jedince	jedinec	k1gMnSc2	jedinec
krev	krev	k1gFnSc1	krev
sajícího	sající	k2eAgInSc2d1	sající
hmyzu	hmyz	k1gInSc2	hmyz
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
druhohor	druhohory	k1gFnPc2	druhohory
bychom	by	kYmCp1nP	by
nebyli	být	k5eNaImAgMnP	být
schopni	schopen	k2eAgMnPc1d1	schopen
extrahovat	extrahovat	k5eAaBmF	extrahovat
dostatečně	dostatečně	k6eAd1	dostatečně
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
dinosauří	dinosauří	k2eAgFnSc1d1	dinosauří
DNA	dna	k1gFnSc1	dna
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
případě	případ	k1gInSc6	případ
získání	získání	k1gNnSc2	získání
DNA	dno	k1gNnSc2	dno
by	by	kYmCp3nP	by
ale	ale	k8xC	ale
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
kompletního	kompletní	k2eAgInSc2d1	kompletní
dinosauřího	dinosauří	k2eAgInSc2d1	dinosauří
genomu	genom	k1gInSc2	genom
zřejmě	zřejmě	k6eAd1	zřejmě
nebyla	být	k5eNaImAgFnS	být
možná	možný	k2eAgFnSc1d1	možná
<g/>
.	.	kIx.	.
</s>
<s>
Jurský	jurský	k2eAgInSc1d1	jurský
park	park	k1gInSc1	park
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
fenoménem	fenomén	k1gInSc7	fenomén
i	i	k9	i
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
počítačových	počítačový	k2eAgFnPc2d1	počítačová
her	hra	k1gFnPc2	hra
a	a	k8xC	a
herních	herní	k2eAgFnPc2d1	herní
konzolí	konzole	k1gFnPc2	konzole
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
několika	několik	k4yIc2	několik
desítek	desítka	k1gFnPc2	desítka
titulů	titul	k1gInPc2	titul
věnovaných	věnovaný	k2eAgInPc2d1	věnovaný
tomuto	tento	k3xDgNnSc3	tento
tématu	téma	k1gNnSc3	téma
zaslouží	zasloužit	k5eAaPmIp3nP	zasloužit
zmínku	zmínka	k1gFnSc4	zmínka
především	především	k9	především
akční	akční	k2eAgFnSc1d1	akční
střílečka	střílečka	k1gFnSc1	střílečka
Trespasser	Trespasser	k1gMnSc1	Trespasser
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
a	a	k8xC	a
budovatelská	budovatelský	k2eAgFnSc1d1	budovatelská
strategie	strategie	k1gFnSc1	strategie
Jurassic	Jurassic	k1gMnSc1	Jurassic
Park	park	k1gInSc1	park
<g/>
:	:	kIx,	:
Operation	Operation	k1gInSc1	Operation
Genesis	Genesis	k1gFnSc1	Genesis
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
přichází	přicházet	k5eAaImIp3nS	přicházet
společnost	společnost	k1gFnSc1	společnost
Telltale	Telltal	k1gMnSc5	Telltal
Games	Games	k1gInSc4	Games
s	s	k7c7	s
novým	nový	k2eAgNnSc7d1	nové
zpracováním	zpracování	k1gNnSc7	zpracování
původního	původní	k2eAgInSc2d1	původní
filmového	filmový	k2eAgInSc2d1	filmový
příběhu	příběh	k1gInSc2	příběh
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Jurassic	Jurassic	k1gMnSc1	Jurassic
Park	park	k1gInSc1	park
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Game	game	k1gInSc1	game
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Jurský	jurský	k2eAgInSc4d1	jurský
park	park	k1gInSc4	park
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Článek	článek	k1gInSc1	článek
o	o	k7c6	o
dějinách	dějiny	k1gFnPc6	dějiny
Jurského	jurský	k2eAgInSc2d1	jurský
parku	park	k1gInSc2	park
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Článek	článek	k1gInSc1	článek
o	o	k7c6	o
klonování	klonování	k1gNnSc6	klonování
dinosaurů	dinosaurus	k1gMnPc2	dinosaurus
http://www.jurassicpark.com/	[url]	k?	http://www.jurassicpark.com/
http://michaelcrichton.com/jp/index.html	[url]	k1gNnPc2	http://michaelcrichton.com/jp/index.html
http://www.jplegacy.org/	[url]	k?	http://www.jplegacy.org/
http://www.jpdatabase.net/jurassic-park/	[url]	k?	http://www.jpdatabase.net/jurassic-park/
http://www.jptoys.com/	[url]	k?	http://www.jptoys.com/
http://jurassicpark.wikia.com/wiki/Main_Page	[url]	k1gInSc1	http://jurassicpark.wikia.com/wiki/Main_Page
http://www.mansionbooks.com/BookDetail.php?bk=254	[url]	k4	http://www.mansionbooks.com/BookDetail.php?bk=254
http://www.latephilosophers.com/jurassic.html	[url]	k5eAaPmAgInS	http://www.latephilosophers.com/jurassic.html
Článek	článek	k1gInSc1	článek
o	o	k7c6	o
možnostech	možnost	k1gFnPc6	možnost
uskutečnění	uskutečnění	k1gNnSc2	uskutečnění
vize	vize	k1gFnSc2	vize
Jurského	jurský	k2eAgInSc2d1	jurský
parku	park	k1gInSc2	park
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Můžeme	moct	k5eAaImIp1nP	moct
vytvořit	vytvořit	k5eAaPmF	vytvořit
Jurský	jurský	k2eAgInSc4d1	jurský
park	park	k1gInSc4	park
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Můžeme	moct	k5eAaImIp1nP	moct
vytvořit	vytvořit	k5eAaPmF	vytvořit
Jurský	jurský	k2eAgInSc4d1	jurský
park	park	k1gInSc4	park
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Můžeme	moct	k5eAaImIp1nP	moct
vytvořit	vytvořit	k5eAaPmF	vytvořit
Jurský	jurský	k2eAgInSc4d1	jurský
park	park	k1gInSc4	park
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
