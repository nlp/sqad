<s>
Rotterdam	Rotterdam	k1gInSc1	Rotterdam
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
610000	[number]	k4	610000
obyvateli	obyvatel	k1gMnPc7	obyvatel
druhé	druhý	k4xOgNnSc4	druhý
největší	veliký	k2eAgNnSc4d3	veliký
nizozemské	nizozemský	k2eAgNnSc4d1	Nizozemské
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
také	také	k9	také
o	o	k7c4	o
největší	veliký	k2eAgInSc4d3	veliký
evropský	evropský	k2eAgInSc4d1	evropský
přístav	přístav	k1gInSc4	přístav
(	(	kIx(	(
<g/>
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ho	on	k3xPp3gNnSc4	on
předstihla	předstihnout	k5eAaPmAgFnS	předstihnout
Šanghaj	Šanghaj	k1gFnSc1	Šanghaj
<g/>
,	,	kIx,	,
i	i	k8xC	i
světa	svět	k1gInSc2	svět
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
město	město	k1gNnSc1	město
moderní	moderní	k2eAgFnSc2d1	moderní
architektury	architektura	k1gFnSc2	architektura
(	(	kIx(	(
<g/>
např.	např.	kA	např.
továrna	továrna	k1gFnSc1	továrna
Van	van	k1gInSc1	van
Nelle	Nelle	k1gInSc1	Nelle
<g/>
,	,	kIx,	,
světové	světový	k2eAgNnSc4d1	světové
kulturní	kulturní	k2eAgNnSc4d1	kulturní
dědictví	dědictví	k1gNnSc4	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
)	)	kIx)	)
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
město	město	k1gNnSc1	město
velkého	velký	k2eAgInSc2d1	velký
historického	historický	k2eAgInSc2d1	historický
významnu	významn	k1gInSc2	významn
<g/>
.	.	kIx.	.
</s>
<s>
Známý	známý	k1gMnSc1	známý
je	být	k5eAaImIp3nS	být
zdejší	zdejší	k2eAgInSc4d1	zdejší
přístavní	přístavní	k2eAgInSc4d1	přístavní
komplex	komplex	k1gInSc4	komplex
rotterdamského	rotterdamský	k2eAgInSc2d1	rotterdamský
přístavu	přístav	k1gInSc2	přístav
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
druhý	druhý	k4xOgInSc1	druhý
největší	veliký	k2eAgInSc1d3	veliký
na	na	k7c6	na
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
první	první	k4xOgFnPc4	první
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Námořní	námořní	k2eAgNnSc1d1	námořní
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
,	,	kIx,	,
dokumentující	dokumentující	k2eAgFnSc4d1	dokumentující
historii	historie	k1gFnSc4	historie
přístavu	přístav	k1gInSc2	přístav
a	a	k8xC	a
stavby	stavba	k1gFnSc2	stavba
lodí	loď	k1gFnPc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
velkoryse	velkoryse	k6eAd1	velkoryse
založeno	založit	k5eAaPmNgNnS	založit
na	na	k7c6	na
pravoúhlém	pravoúhlý	k2eAgInSc6d1	pravoúhlý
půdorysu	půdorys	k1gInSc6	půdorys
kolonizátory	kolonizátor	k1gMnPc7	kolonizátor
roku	rok	k1gInSc2	rok
1230	[number]	k4	1230
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zničení	zničení	k1gNnSc6	zničení
památek	památka	k1gFnPc2	památka
jeho	jeho	k3xOp3gFnSc2	jeho
bohaté	bohatý	k2eAgFnSc2d1	bohatá
historie	historie	k1gFnSc2	historie
a	a	k8xC	a
architektury	architektura	k1gFnSc2	architektura
se	se	k3xPyFc4	se
podílely	podílet	k5eAaImAgInP	podílet
dva	dva	k4xCgInPc1	dva
faktory	faktor	k1gInPc1	faktor
<g/>
:	:	kIx,	:
bouřlivý	bouřlivý	k2eAgInSc1d1	bouřlivý
rozvoj	rozvoj	k1gInSc1	rozvoj
průmyslu	průmysl	k1gInSc2	průmysl
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
především	především	k9	především
druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgFnSc1d2	veliký
část	část	k1gFnSc1	část
Rotterdamu	Rotterdam	k1gInSc2	Rotterdam
byla	být	k5eAaImAgFnS	být
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
zničena	zničit	k5eAaPmNgFnS	zničit
během	během	k7c2	během
několika	několik	k4yIc2	několik
velkých	velký	k2eAgInPc2d1	velký
kobercových	kobercový	k2eAgInPc2d1	kobercový
náletů	nálet	k1gInPc2	nálet
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
byl	být	k5eAaImAgMnS	být
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
střed	střed	k1gInSc1	střed
města	město	k1gNnSc2	město
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
zastavován	zastavován	k2eAgInSc1d1	zastavován
moderními	moderní	k2eAgFnPc7d1	moderní
výškovými	výškový	k2eAgFnPc7d1	výšková
budovami	budova	k1gFnPc7	budova
<g/>
,	,	kIx,	,
odlišnými	odlišný	k2eAgInPc7d1	odlišný
od	od	k7c2	od
holandských	holandský	k2eAgInPc2d1	holandský
nízkých	nízký	k2eAgInPc2d1	nízký
cihlových	cihlový	k2eAgInPc2d1	cihlový
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
7	[number]	k4	7
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2016	[number]	k4	2016
vstoupili	vstoupit	k5eAaPmAgMnP	vstoupit
zaměstnanci	zaměstnanec	k1gMnPc1	zaměstnanec
rotterdamského	rotterdamský	k2eAgInSc2d1	rotterdamský
přístavu	přístav	k1gInSc2	přístav
(	(	kIx(	(
<g/>
konkrétně	konkrétně	k6eAd1	konkrétně
terminálů	terminál	k1gInPc2	terminál
ECT	ECT	kA	ECT
Delta	delta	k1gFnSc1	delta
<g/>
,	,	kIx,	,
Uniport	Uniport	k1gInSc1	Uniport
a	a	k8xC	a
APM	APM	kA	APM
<g/>
)	)	kIx)	)
do	do	k7c2	do
24	[number]	k4	24
<g/>
hodinové	hodinový	k2eAgFnSc2d1	hodinová
stávky	stávka	k1gFnSc2	stávka
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
je	být	k5eAaImIp3nS	být
předpokládané	předpokládaný	k2eAgNnSc1d1	předpokládané
propouštění	propouštění	k1gNnSc1	propouštění
pro	pro	k7c4	pro
nadbytečnost	nadbytečnost	k1gFnSc4	nadbytečnost
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
plánované	plánovaný	k2eAgFnSc2d1	plánovaná
automatizace	automatizace	k1gFnSc2	automatizace
terminálů	terminál	k1gInPc2	terminál
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
přijít	přijít	k5eAaPmF	přijít
o	o	k7c4	o
práci	práce	k1gFnSc4	práce
700	[number]	k4	700
z	z	k7c2	z
3600	[number]	k4	3600
až	až	k9	až
4000	[number]	k4	4000
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
největšího	veliký	k2eAgInSc2d3	veliký
přístavu	přístav	k1gInSc2	přístav
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Turisticky	turisticky	k6eAd1	turisticky
atraktivní	atraktivní	k2eAgFnSc1d1	atraktivní
je	být	k5eAaImIp3nS	být
vyhlídka	vyhlídka	k1gFnSc1	vyhlídka
na	na	k7c4	na
přístavní	přístavní	k2eAgNnSc4d1	přístavní
město	město	k1gNnSc4	město
z	z	k7c2	z
vyhlídkové	vyhlídkový	k2eAgFnSc2d1	vyhlídková
věže	věž	k1gFnSc2	věž
Euromast	Euromast	k1gMnSc1	Euromast
<g/>
,	,	kIx,	,
vysoké	vysoký	k2eAgFnSc6d1	vysoká
185	[number]	k4	185
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
výtah	výtah	k1gInSc1	výtah
jezdí	jezdit	k5eAaImIp3nS	jezdit
až	až	k9	až
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
věže	věž	k1gFnSc2	věž
nebo	nebo	k8xC	nebo
do	do	k7c2	do
restaurace	restaurace	k1gFnSc2	restaurace
Panorama	panorama	k1gNnSc1	panorama
<g/>
,	,	kIx,	,
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
100	[number]	k4	100
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Populární	populární	k2eAgMnSc1d1	populární
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
Erasmusbrug	Erasmusbrug	k1gInSc1	Erasmusbrug
-	-	kIx~	-
moderní	moderní	k2eAgInSc1d1	moderní
most	most	k1gInSc1	most
spojující	spojující	k2eAgFnSc2d1	spojující
severní	severní	k2eAgFnSc2d1	severní
a	a	k8xC	a
jižní	jižní	k2eAgFnSc2d1	jižní
Rotterdam	Rotterdam	k1gInSc4	Rotterdam
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc4d1	další
atrakci	atrakce	k1gFnSc4	atrakce
představuje	představovat	k5eAaImIp3nS	představovat
Museumpark	Museumpark	k1gInSc1	Museumpark
-	-	kIx~	-
muzejní	muzejní	k2eAgFnSc2d1	muzejní
budovy	budova	k1gFnSc2	budova
volně	volně	k6eAd1	volně
navazující	navazující	k2eAgInPc1d1	navazující
v	v	k7c6	v
pásu	pás	k1gInSc6	pás
městské	městský	k2eAgFnSc2d1	městská
zeleně	zeleň	k1gFnSc2	zeleň
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
a	a	k8xC	a
po	po	k7c6	po
amsterdamském	amsterdamský	k2eAgInSc6d1	amsterdamský
Rijksmuseu	Rijksmuseus	k1gInSc6	Rijksmuseus
druhé	druhý	k4xOgNnSc1	druhý
největší	veliký	k2eAgNnSc1d3	veliký
muzeum	muzeum	k1gNnSc1	muzeum
umění	umění	k1gNnSc2	umění
v	v	k7c6	v
Holandsku	Holandsko	k1gNnSc6	Holandsko
je	být	k5eAaImIp3nS	být
Museum	museum	k1gNnSc1	museum
Boijmans	Boijmansa	k1gFnPc2	Boijmansa
van	vana	k1gFnPc2	vana
Beuningen	Beuningen	k1gInSc1	Beuningen
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc2	jehož
sbírky	sbírka	k1gFnSc2	sbírka
ve	v	k7c6	v
třech	tři	k4xCgNnPc6	tři
patrech	patro	k1gNnPc6	patro
dvou	dva	k4xCgFnPc2	dva
budov	budova	k1gFnPc2	budova
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
12	[number]	k4	12
000	[number]	k4	000
metrů	metr	k1gInPc2	metr
čtverečních	čtvereční	k2eAgInPc2d1	čtvereční
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
deskové	deskový	k2eAgNnSc4d1	deskové
malířství	malířství	k1gNnSc4	malířství
a	a	k8xC	a
sochařství	sochařství	k1gNnSc4	sochařství
od	od	k7c2	od
rané	raný	k2eAgFnSc2d1	raná
italské	italský	k2eAgFnSc2d1	italská
gotiky	gotika	k1gFnSc2	gotika
(	(	kIx(	(
<g/>
Fra	Fra	k1gMnSc1	Fra
Angelico	Angelico	k1gMnSc1	Angelico
<g/>
,	,	kIx,	,
Lorenzo	Lorenza	k1gFnSc5	Lorenza
Monaco	Monaco	k1gNnSc1	Monaco
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
pozdní	pozdní	k2eAgFnSc4d1	pozdní
gotiku	gotika	k1gFnSc4	gotika
(	(	kIx(	(
<g/>
Van	van	k1gInSc4	van
Eyckové	Eycková	k1gFnSc2	Eycková
<g/>
,	,	kIx,	,
Hans	Hans	k1gMnSc1	Hans
Memling	Memling	k1gInSc1	Memling
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
van	vana	k1gFnPc2	vana
Scorel	Scorel	k1gMnSc1	Scorel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
olejomalby	olejomalba	k1gFnPc1	olejomalba
vlámských	vlámský	k2eAgMnPc2d1	vlámský
mistrů	mistr	k1gMnPc2	mistr
renesance	renesance	k1gFnSc2	renesance
(	(	kIx(	(
<g/>
Hieronymus	Hieronymus	k1gMnSc1	Hieronymus
Bosch	Bosch	kA	Bosch
<g/>
,	,	kIx,	,
Pieter	Pieter	k1gMnSc1	Pieter
Breughel	Breughel	k1gMnSc1	Breughel
<g/>
,	,	kIx,	,
německou	německý	k2eAgFnSc4d1	německá
renesanci	renesance	k1gFnSc4	renesance
(	(	kIx(	(
<g/>
Hans	Hans	k1gMnSc1	Hans
Memling	Memling	k1gInSc1	Memling
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nizozemské	nizozemský	k2eAgFnPc4d1	nizozemská
mistry	mistr	k1gMnPc7	mistr
<g />
.	.	kIx.	.
</s>
<s>
raného	raný	k2eAgNnSc2d1	rané
baroka	baroko	k1gNnSc2	baroko
(	(	kIx(	(
<g/>
Rembrandt	Rembrandt	k1gMnSc1	Rembrandt
<g/>
,	,	kIx,	,
Frans	Frans	k1gMnSc1	Frans
Hals	Hals	k1gMnSc1	Hals
<g/>
,	,	kIx,	,
P.	P.	kA	P.
P.	P.	kA	P.
Rubens	Rubens	k1gInSc1	Rubens
<g/>
,	,	kIx,	,
Anton	Anton	k1gMnSc1	Anton
van	vana	k1gFnPc2	vana
Dyck	Dyck	k1gMnSc1	Dyck
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Steen	Steen	k1gInSc1	Steen
<g/>
,	,	kIx,	,
Adriaen	Adriaen	k2eAgInSc1d1	Adriaen
van	van	k1gInSc1	van
Ostade	Ostad	k1gInSc5	Ostad
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mistry	mistr	k1gMnPc7	mistr
italské	italský	k2eAgMnPc4d1	italský
(	(	kIx(	(
<g/>
Tizian	Tizian	k1gInSc1	Tizian
<g/>
,	,	kIx,	,
Tintoretto	Tintoretto	k1gNnSc1	Tintoretto
<g/>
)	)	kIx)	)
a	a	k8xC	a
španělské	španělský	k2eAgNnSc1d1	španělské
(	(	kIx(	(
<g/>
Murillo	Murillo	k1gNnSc1	Murillo
<g/>
,	,	kIx,	,
Goya	Goya	k1gFnSc1	Goya
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
moderní	moderní	k2eAgFnSc4d1	moderní
malbu	malba	k1gFnSc4	malba
závěru	závěr	k1gInSc2	závěr
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
počátku	počátek	k1gInSc2	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
Monet	moneta	k1gFnPc2	moneta
<g/>
,	,	kIx,	,
Gauguin	Gauguina	k1gFnPc2	Gauguina
<g/>
,	,	kIx,	,
Van	vana	k1gFnPc2	vana
Gogh	Gogha	k1gFnPc2	Gogha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
symbolismus	symbolismus	k1gInSc1	symbolismus
(	(	kIx(	(
<g/>
Odilon	Odilon	k1gInSc1	Odilon
Redon	Redon	k1gInSc1	Redon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
expresionismus	expresionismus	k1gInSc1	expresionismus
<g/>
,	,	kIx,	,
kubismus	kubismus	k1gInSc1	kubismus
a	a	k8xC	a
jiné	jiný	k2eAgInPc1d1	jiný
-ismy	sm	k1gInPc1	-ism
(	(	kIx(	(
<g/>
mj.	mj.	kA	mj.
Picasso	Picassa	k1gFnSc5	Picassa
<g/>
,	,	kIx,	,
Kandinsky	Kandinsky	k1gMnSc1	Kandinsky
<g/>
)	)	kIx)	)
až	až	k9	až
po	po	k7c4	po
surrealisty	surrealista	k1gMnPc4	surrealista
(	(	kIx(	(
<g/>
Giorgio	Giorgio	k6eAd1	Giorgio
de	de	k?	de
Chirico	Chirica	k1gMnSc5	Chirica
<g/>
,	,	kIx,	,
René	René	k1gMnSc5	René
Magritte	Magritt	k1gMnSc5	Magritt
<g/>
,	,	kIx,	,
Hans	Hans	k1gMnSc1	Hans
Arp	Arp	k1gMnSc1	Arp
<g/>
,	,	kIx,	,
Salvator	Salvator	k1gMnSc1	Salvator
Dalí	Dalí	k1gMnSc1	Dalí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Umělecká	umělecký	k2eAgNnPc4d1	umělecké
řemesla	řemeslo	k1gNnPc4	řemeslo
a	a	k8xC	a
design	design	k1gInSc4	design
jsou	být	k5eAaImIp3nP	být
zastoupena	zastoupit	k5eAaPmNgNnP	zastoupit
od	od	k7c2	od
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
po	po	k7c4	po
avantgardní	avantgardní	k2eAgInPc4d1	avantgardní
směry	směr	k1gInPc4	směr
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
zejména	zejména	k6eAd1	zejména
nábytkem	nábytek	k1gInSc7	nábytek
<g/>
,	,	kIx,	,
sklem	sklo	k1gNnSc7	sklo
<g/>
,	,	kIx,	,
porcelánem	porcelán	k1gInSc7	porcelán
<g/>
,	,	kIx,	,
delftskou	delftský	k2eAgFnSc7d1	delftská
keramikou	keramika	k1gFnSc7	keramika
<g/>
,	,	kIx,	,
stříbrnictvím	stříbrnictví	k1gNnSc7	stříbrnictví
a	a	k8xC	a
textiliemi	textilie	k1gFnPc7	textilie
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
krátkodobých	krátkodobý	k2eAgFnPc6d1	krátkodobá
výstavách	výstava	k1gFnPc6	výstava
prezentováno	prezentovat	k5eAaBmNgNnS	prezentovat
současné	současný	k2eAgNnSc1d1	současné
umění	umění	k1gNnSc1	umění
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgNnSc1d1	moderní
umění	umění	k1gNnSc1	umění
vystavuje	vystavovat	k5eAaImIp3nS	vystavovat
Kunsthal	Kunsthal	k1gMnSc1	Kunsthal
Rotterdam	Rotterdam	k1gInSc1	Rotterdam
<g/>
,	,	kIx,	,
Witte	Witt	k1gInSc5	Witt
de	de	k?	de
With	With	k1gInSc1	With
<g/>
,	,	kIx,	,
Tent	tent	k1gInSc1	tent
<g/>
,	,	kIx,	,
<g/>
Chabot	Chabot	k1gInSc1	Chabot
Museum	museum	k1gNnSc1	museum
a	a	k8xC	a
řada	řada	k1gFnSc1	řada
soukromých	soukromý	k2eAgFnPc2d1	soukromá
galerií	galerie	k1gFnPc2	galerie
<g/>
.	.	kIx.	.
</s>
<s>
Bohatou	bohatý	k2eAgFnSc4d1	bohatá
sbírku	sbírka	k1gFnSc4	sbírka
fotografií	fotografia	k1gFnPc2	fotografia
má	mít	k5eAaImIp3nS	mít
Nederlands	Nederlands	k1gInSc1	Nederlands
fotomuseum	fotomuseum	k1gInSc1	fotomuseum
<g/>
.	.	kIx.	.
</s>
<s>
Architektuře	architektura	k1gFnSc3	architektura
v	v	k7c6	v
Nizozemí	Nizozemí	k1gNnSc6	Nizozemí
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
od	od	k7c2	od
secese	secese	k1gFnSc2	secese
po	po	k7c4	po
současnost	současnost	k1gFnSc4	současnost
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaPmIp3nS	věnovat
Nederlands	Nederlands	k1gInSc1	Nederlands
Architectuurinstituut	Architectuurinstituut	k1gInSc1	Architectuurinstituut
(	(	kIx(	(
<g/>
NAI	NAI	kA	NAI
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
Rotterdam	Rotterdam	k1gInSc1	Rotterdam
je	být	k5eAaImIp3nS	být
spojeno	spojit	k5eAaPmNgNnS	spojit
s	s	k7c7	s
Prahou	Praha	k1gFnSc7	Praha
díky	díky	k7c3	díky
letecké	letecký	k2eAgFnSc3d1	letecká
společnosti	společnost	k1gFnSc3	společnost
Transavia	Transavium	k1gNnSc2	Transavium
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
nabízí	nabízet	k5eAaImIp3nS	nabízet
letecké	letecký	k2eAgNnSc4d1	letecké
spojení	spojení	k1gNnSc4	spojení
již	již	k6eAd1	již
od	od	k7c2	od
dubna	duben	k1gInSc2	duben
2011	[number]	k4	2011
několikrát	několikrát	k6eAd1	několikrát
týdně	týdně	k6eAd1	týdně
<g/>
.	.	kIx.	.
</s>
<s>
Erasmus	Erasmus	k1gMnSc1	Erasmus
Rotterdamský	rotterdamský	k2eAgMnSc1d1	rotterdamský
(	(	kIx(	(
<g/>
1467	[number]	k4	1467
-	-	kIx~	-
1536	[number]	k4	1536
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
filozof	filozof	k1gMnSc1	filozof
a	a	k8xC	a
humanista	humanista	k1gMnSc1	humanista
Willem	Will	k1gMnSc7	Will
de	de	k?	de
Kooning	Kooning	k1gInSc1	Kooning
(	(	kIx(	(
<g/>
1904	[number]	k4	1904
-	-	kIx~	-
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
abstraktní	abstraktní	k2eAgMnSc1d1	abstraktní
expresionistický	expresionistický	k2eAgMnSc1d1	expresionistický
malíř	malíř	k1gMnSc1	malíř
Edsger	Edsger	k1gMnSc1	Edsger
Dijkstra	Dijkstra	k1gFnSc1	Dijkstra
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
-	-	kIx~	-
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
informatik	informatik	k1gMnSc1	informatik
Rem	Rem	k1gMnSc1	Rem
Koolhaas	Koolhaas	k1gMnSc1	Koolhaas
(	(	kIx(	(
<g/>
*	*	kIx~	*
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
architekt	architekt	k1gMnSc1	architekt
<g/>
,	,	kIx,	,
teoretik	teoretik	k1gMnSc1	teoretik
a	a	k8xC	a
urbanista	urbanista	k1gMnSc1	urbanista
Giovanni	Giovanň	k1gMnSc3	Giovanň
van	van	k1gInSc4	van
Bronckhorst	Bronckhorst	k1gInSc4	Bronckhorst
(	(	kIx(	(
<g/>
*	*	kIx~	*
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fotbalový	fotbalový	k2eAgMnSc1d1	fotbalový
reprezentant	reprezentant	k1gMnSc1	reprezentant
Raemon	Raemon	k1gMnSc1	Raemon
Sluiter	Sluiter	k1gMnSc1	Sluiter
(	(	kIx(	(
<g/>
*	*	kIx~	*
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tenista	tenista	k1gMnSc1	tenista
Robin	robin	k2eAgInSc4d1	robin
van	van	k1gInSc4	van
Persie	Persie	k1gFnSc2	Persie
(	(	kIx(	(
<g/>
*	*	kIx~	*
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fotbalista	fotbalista	k1gMnSc1	fotbalista
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Rotterdam	Rotterdam	k1gInSc1	Rotterdam
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Rotterdam	Rotterdam	k1gInSc1	Rotterdam
<g/>
,	,	kIx,	,
Holandská	holandský	k2eAgFnSc1d1	holandská
turistická	turistický	k2eAgFnSc1d1	turistická
informační	informační	k2eAgFnSc1d1	informační
kancelář	kancelář	k1gFnSc1	kancelář
</s>
