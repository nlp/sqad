<s>
Divadlo	divadlo	k1gNnSc1	divadlo
Husa	Hus	k1gMnSc2	Hus
na	na	k7c6	na
provázku	provázek	k1gInSc6	provázek
je	být	k5eAaImIp3nS	být
divadlo	divadlo	k1gNnSc1	divadlo
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
na	na	k7c6	na
Zelném	zelný	k2eAgInSc6d1	zelný
trhu	trh	k1gInSc6	trh
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
uměleckým	umělecký	k2eAgMnSc7d1	umělecký
ředitelem	ředitel	k1gMnSc7	ředitel
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
Vladimír	Vladimír	k1gMnSc1	Vladimír
Morávek	Morávek	k1gMnSc1	Morávek
<g/>
.	.	kIx.	.
</s>
<s>
Soubor	soubor	k1gInSc1	soubor
funguje	fungovat	k5eAaImIp3nS	fungovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
skupina	skupina	k1gFnSc1	skupina
amatérských	amatérský	k2eAgMnPc2d1	amatérský
divadelníků	divadelník	k1gMnPc2	divadelník
z	z	k7c2	z
JAMU	jam	k1gInSc2	jam
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Bořivoje	Bořivoj	k1gMnSc2	Bořivoj
Srby	Srba	k1gMnSc2	Srba
začala	začít	k5eAaPmAgFnS	začít
experimentovat	experimentovat	k5eAaImF	experimentovat
s	s	k7c7	s
divadelními	divadelní	k2eAgInPc7d1	divadelní
tvary	tvar	k1gInPc7	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
soubor	soubor	k1gInSc1	soubor
působil	působit	k5eAaImAgInS	působit
pod	pod	k7c7	pod
záštitou	záštita	k1gFnSc7	záštita
Domu	dům	k1gInSc2	dům
umění	umění	k1gNnSc2	umění
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
vzniku	vznik	k1gInSc2	vznik
divadla	divadlo	k1gNnSc2	divadlo
byla	být	k5eAaImAgFnS	být
myšlenka	myšlenka	k1gFnSc1	myšlenka
experimentální	experimentální	k2eAgFnSc2d1	experimentální
scény	scéna	k1gFnSc2	scéna
jako	jako	k8xC	jako
širšího	široký	k2eAgNnSc2d2	širší
kulturního	kulturní	k2eAgNnSc2d1	kulturní
centra	centrum	k1gNnSc2	centrum
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
by	by	kYmCp3nS	by
sdružovalo	sdružovat	k5eAaImAgNnS	sdružovat
generačně	generačně	k6eAd1	generačně
a	a	k8xC	a
názorově	názorově	k6eAd1	názorově
spřízněné	spřízněný	k2eAgMnPc4d1	spřízněný
umělce	umělec	k1gMnPc4	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
divadla	divadlo	k1gNnSc2	divadlo
sahá	sahat	k5eAaImIp3nS	sahat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
kolem	kolem	k7c2	kolem
dramaturga	dramaturg	k1gMnSc2	dramaturg
Bořivoje	Bořivoj	k1gMnSc2	Bořivoj
Srby	Srba	k1gMnSc2	Srba
začala	začít	k5eAaPmAgFnS	začít
formovat	formovat	k5eAaImF	formovat
amatérská	amatérský	k2eAgFnSc1d1	amatérská
skupina	skupina	k1gFnSc1	skupina
složená	složený	k2eAgFnSc1d1	složená
z	z	k7c2	z
profesionálních	profesionální	k2eAgMnPc2d1	profesionální
divadelníků	divadelník	k1gMnPc2	divadelník
<g/>
,	,	kIx,	,
studentů	student	k1gMnPc2	student
uměleckých	umělecký	k2eAgFnPc2d1	umělecká
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
mladých	mladý	k2eAgMnPc2d1	mladý
spisovatelů	spisovatel	k1gMnPc2	spisovatel
<g/>
,	,	kIx,	,
hudebníků	hudebník	k1gMnPc2	hudebník
a	a	k8xC	a
výtvarníků	výtvarník	k1gMnPc2	výtvarník
<g/>
.	.	kIx.	.
</s>
<s>
Sdružení	sdružení	k1gNnSc1	sdružení
si	se	k3xPyFc3	se
vypůjčilo	vypůjčit	k5eAaPmAgNnS	vypůjčit
název	název	k1gInSc4	název
knihy	kniha	k1gFnSc2	kniha
brněnského	brněnský	k2eAgMnSc2d1	brněnský
spisovatele	spisovatel	k1gMnSc2	spisovatel
Jiřího	Jiří	k1gMnSc2	Jiří
Mahena	Mahen	k1gMnSc2	Mahen
Husa	Hus	k1gMnSc2	Hus
na	na	k7c6	na
provázku	provázek	k1gInSc6	provázek
(	(	kIx(	(
<g/>
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgFnSc4d1	obsahující
šestici	šestice	k1gFnSc4	šestice
experimentálních	experimentální	k2eAgNnPc2d1	experimentální
divadelních	divadelní	k2eAgNnPc2d1	divadelní
<g/>
,	,	kIx,	,
cirkusových	cirkusový	k2eAgNnPc2d1	cirkusové
a	a	k8xC	a
filmových	filmový	k2eAgNnPc2d1	filmové
libret	libreto	k1gNnPc2	libreto
<g/>
.	.	kIx.	.
</s>
<s>
Předmluva	předmluva	k1gFnSc1	předmluva
knihy	kniha	k1gFnSc2	kniha
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
také	také	k6eAd1	také
uměleckým	umělecký	k2eAgNnSc7d1	umělecké
východiskem	východisko	k1gNnSc7	východisko
souboru	soubor	k1gInSc2	soubor
<g/>
.	.	kIx.	.
</s>
<s>
Srba	Srba	k1gMnSc1	Srba
formuloval	formulovat	k5eAaImAgMnS	formulovat
jako	jako	k9	jako
základní	základní	k2eAgInSc4d1	základní
tvůrčí	tvůrčí	k2eAgInSc4d1	tvůrčí
princip	princip	k1gInSc4	princip
program	program	k1gInSc4	program
nepravidelné	pravidelný	k2eNgFnSc2d1	nepravidelná
dramaturgie	dramaturgie	k1gFnSc2	dramaturgie
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
jde	jít	k5eAaImIp3nS	jít
o	o	k7c6	o
hledání	hledání	k1gNnSc6	hledání
nových	nový	k2eAgNnPc2d1	nové
témat	téma	k1gNnPc2	téma
v	v	k7c6	v
původně	původně	k6eAd1	původně
nedramatických	dramatický	k2eNgInPc6d1	nedramatický
textech	text	k1gInPc6	text
<g/>
.	.	kIx.	.
</s>
<s>
Jádro	jádro	k1gNnSc1	jádro
souboru	soubor	k1gInSc2	soubor
tvořili	tvořit	k5eAaImAgMnP	tvořit
studenti	student	k1gMnPc1	student
činoherní	činoherní	k2eAgFnSc2d1	činoherní
režie	režie	k1gFnSc2	režie
Eva	Eva	k1gFnSc1	Eva
Tálská	Tálská	k1gFnSc1	Tálská
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Pospíšil	Pospíšil	k1gMnSc1	Pospíšil
a	a	k8xC	a
Peter	Peter	k1gMnSc1	Peter
Scherhaufer	Scherhaufer	k1gMnSc1	Scherhaufer
<g/>
,	,	kIx,	,
studenti	student	k1gMnPc1	student
herectví	herectví	k1gNnPc2	herectví
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Jiří	Jiří	k1gMnSc1	Jiří
Pecha	Pecha	k1gMnSc1	Pecha
<g/>
,	,	kIx,	,
Hana	Hana	k1gFnSc1	Hana
Tesařová	Tesařová	k1gFnSc1	Tesařová
nebo	nebo	k8xC	nebo
Jiří	Jiří	k1gMnSc1	Jiří
Čapka	Čapka	k1gMnSc1	Čapka
<g/>
,	,	kIx,	,
skladatel	skladatel	k1gMnSc1	skladatel
Miloš	Miloš	k1gMnSc1	Miloš
Štědroň	Štědroň	k1gMnSc1	Štědroň
<g/>
,	,	kIx,	,
hudebníci	hudebník	k1gMnPc1	hudebník
Bohuš	Bohuš	k1gMnSc1	Bohuš
Zoubek	Zoubek	k1gMnSc1	Zoubek
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Kramář	kramář	k1gMnSc1	kramář
a	a	k8xC	a
Max	Max	k1gMnSc1	Max
Wittmann	Wittmann	k1gMnSc1	Wittmann
<g/>
,	,	kIx,	,
výtvarníci	výtvarník	k1gMnPc1	výtvarník
Boris	Boris	k1gMnSc1	Boris
Mysliveček	Mysliveček	k1gMnSc1	Mysliveček
a	a	k8xC	a
Libor	Libor	k1gMnSc1	Libor
David	David	k1gMnSc1	David
<g/>
,	,	kIx,	,
literáti	literátit	k5eAaImRp2nS	literátit
Miloš	Miloš	k1gMnSc1	Miloš
Pospíšil	Pospíšil	k1gMnSc1	Pospíšil
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Souchop	Souchop	k1gInSc1	Souchop
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Záštitu	záštita	k1gFnSc4	záštita
nad	nad	k7c7	nad
souborem	soubor	k1gInSc7	soubor
převzal	převzít	k5eAaPmAgInS	převzít
brněnský	brněnský	k2eAgInSc1d1	brněnský
Dům	dům	k1gInSc1	dům
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
jeho	jeho	k3xOp3gMnSc1	jeho
ředitel	ředitel	k1gMnSc1	ředitel
Adolf	Adolf	k1gMnSc1	Adolf
Kroupa	Kroupa	k1gMnSc1	Kroupa
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
do	do	k7c2	do
18	[number]	k4	18
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1968	[number]	k4	1968
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
první	první	k4xOgNnPc4	první
veřejná	veřejný	k2eAgNnPc4d1	veřejné
představení	představení	k1gNnPc4	představení
v	v	k7c6	v
Procházkově	Procházkův	k2eAgFnSc6d1	Procházkova
síni	síň	k1gFnSc6	síň
Domu	dům	k1gInSc2	dům
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Z.	Z.	kA	Z.
Pospíšil	Pospíšil	k1gMnSc1	Pospíšil
uvedl	uvést	k5eAaPmAgMnS	uvést
svou	svůj	k3xOyFgFnSc4	svůj
montáž	montáž	k1gFnSc4	montáž
Panta	Pant	k1gInSc2	Pant
Rei	Rea	k1gFnSc3	Rea
aneb	aneb	k?	aneb
Dějiny	dějiny	k1gFnPc4	dějiny
národa	národ	k1gInSc2	národ
českého	český	k2eAgInSc2d1	český
v	v	k7c6	v
kostce	kostka	k1gFnSc6	kostka
podle	podle	k7c2	podle
Milana	Milan	k1gMnSc2	Milan
Uhdeho	Uhde	k1gMnSc2	Uhde
a	a	k8xC	a
E.	E.	kA	E.
Tálská	Tálská	k1gFnSc1	Tálská
představila	představit	k5eAaPmAgFnS	představit
kreaci	kreace	k1gFnSc4	kreace
na	na	k7c4	na
verše	verš	k1gInPc4	verš
Christiana	Christian	k1gMnSc4	Christian
Morgensterna	Morgensterna	k1gFnSc1	Morgensterna
Šibeniční	šibeniční	k2eAgFnPc1d1	šibeniční
písně	píseň	k1gFnPc1	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Dům	dům	k1gInSc1	dům
umění	umění	k1gNnSc2	umění
zůstal	zůstat	k5eAaPmAgInS	zůstat
na	na	k7c4	na
25	[number]	k4	25
let	let	k1gInSc4	let
domovskou	domovský	k2eAgFnSc7d1	domovská
scénou	scéna	k1gFnSc7	scéna
souboru	soubor	k1gInSc2	soubor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1969	[number]	k4	1969
se	se	k3xPyFc4	se
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
KSČ	KSČ	kA	KSČ
dostal	dostat	k5eAaPmAgMnS	dostat
Gustáv	Gustáva	k1gFnPc2	Gustáva
Husák	Husák	k1gMnSc1	Husák
a	a	k8xC	a
soubor	soubor	k1gInSc1	soubor
musel	muset	k5eAaImAgInS	muset
na	na	k7c6	na
základě	základ	k1gInSc6	základ
cenzurního	cenzurní	k2eAgInSc2d1	cenzurní
zásahu	zásah	k1gInSc2	zásah
ze	z	k7c2	z
svého	svůj	k3xOyFgInSc2	svůj
názvu	název	k1gInSc2	název
vypustit	vypustit	k5eAaPmF	vypustit
slovo	slovo	k1gNnSc4	slovo
"	"	kIx"	"
<g/>
husa	husa	k1gFnSc1	husa
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
během	během	k7c2	během
jedné	jeden	k4xCgFnSc2	jeden
noci	noc	k1gFnSc2	noc
někdo	někdo	k3yInSc1	někdo
v	v	k7c6	v
ulicích	ulice	k1gFnPc6	ulice
Brna	Brno	k1gNnSc2	Brno
přepsal	přepsat	k5eAaPmAgInS	přepsat
plakáty	plakát	k1gInPc1	plakát
divadla	divadlo	k1gNnSc2	divadlo
na	na	k7c4	na
"	"	kIx"	"
<g/>
Husák	Husák	k1gMnSc1	Husák
na	na	k7c6	na
provázku	provázek	k1gInSc6	provázek
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Soubor	soubor	k1gInSc1	soubor
přijal	přijmout	k5eAaPmAgInS	přijmout
název	název	k1gInSc4	název
Divadlo	divadlo	k1gNnSc1	divadlo
na	na	k7c6	na
provázku	provázek	k1gInSc6	provázek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvních	první	k4xOgNnPc6	první
letech	léto	k1gNnPc6	léto
fungování	fungování	k1gNnSc2	fungování
se	se	k3xPyFc4	se
soubor	soubor	k1gInSc1	soubor
snažil	snažit	k5eAaImAgInS	snažit
formulovat	formulovat	k5eAaImF	formulovat
své	svůj	k3xOyFgInPc4	svůj
cíle	cíl	k1gInPc4	cíl
a	a	k8xC	a
směřování	směřování	k1gNnSc4	směřování
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
divadelníci	divadelník	k1gMnPc1	divadelník
dokument	dokument	k1gInSc4	dokument
Programová	programový	k2eAgNnPc4d1	programové
východiska	východisko	k1gNnPc4	východisko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jako	jako	k8xS	jako
nejdůležitější	důležitý	k2eAgMnPc1d3	nejdůležitější
označili	označit	k5eAaPmAgMnP	označit
hledání	hledání	k1gNnSc4	hledání
a	a	k8xC	a
experimentování	experimentování	k1gNnSc4	experimentování
<g/>
,	,	kIx,	,
dramaturgickou	dramaturgický	k2eAgFnSc4d1	dramaturgická
otevřenost	otevřenost	k1gFnSc4	otevřenost
<g/>
,	,	kIx,	,
hraní	hranit	k5eAaImIp3nS	hranit
v	v	k7c6	v
nepravidelném	pravidelný	k2eNgInSc6d1	nepravidelný
prostoru	prostor	k1gInSc6	prostor
<g/>
,	,	kIx,	,
fyzickou	fyzický	k2eAgFnSc4d1	fyzická
a	a	k8xC	a
pohybovou	pohybový	k2eAgFnSc4d1	pohybová
připravenost	připravenost	k1gFnSc4	připravenost
herců	herc	k1gInPc2	herc
a	a	k8xC	a
kontakt	kontakt	k1gInSc4	kontakt
s	s	k7c7	s
aktuálními	aktuální	k2eAgFnPc7d1	aktuální
tendencemi	tendence	k1gFnPc7	tendence
světového	světový	k2eAgNnSc2d1	světové
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1972	[number]	k4	1972
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
divadlo	divadlo	k1gNnSc1	divadlo
profesionalizovat	profesionalizovat	k5eAaImF	profesionalizovat
a	a	k8xC	a
od	od	k7c2	od
května	květen	k1gInSc2	květen
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
začal	začít	k5eAaPmAgInS	začít
soubor	soubor	k1gInSc1	soubor
pravidelně	pravidelně	k6eAd1	pravidelně
hrát	hrát	k5eAaImF	hrát
v	v	k7c6	v
Procházkově	Procházkův	k2eAgFnSc6d1	Procházkova
síni	síň	k1gFnSc6	síň
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
postupně	postupně	k6eAd1	postupně
přetvářel	přetvářet	k5eAaImAgInS	přetvářet
do	do	k7c2	do
podoby	podoba	k1gFnSc2	podoba
variabilního	variabilní	k2eAgInSc2d1	variabilní
divadelního	divadelní	k2eAgInSc2d1	divadelní
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
stálého	stálý	k2eAgNnSc2d1	stálé
angažmá	angažmá	k1gNnSc2	angažmá
přišli	přijít	k5eAaPmAgMnP	přijít
Jiří	Jiří	k1gMnSc1	Jiří
Pecha	Pecha	k1gMnSc1	Pecha
<g/>
,	,	kIx,	,
Boleslav	Boleslav	k1gMnSc1	Boleslav
Polívka	Polívka	k1gMnSc1	Polívka
<g/>
,	,	kIx,	,
Gabriela	Gabriela	k1gFnSc1	Gabriela
Wilhelmová	Wilhelmová	k1gFnSc1	Wilhelmová
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Bartoška	Bartoška	k1gMnSc1	Bartoška
a	a	k8xC	a
Karel	Karel	k1gMnSc1	Karel
Heřmánek	Heřmánek	k1gMnSc1	Heřmánek
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
dramaturg	dramaturg	k1gMnSc1	dramaturg
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
Petr	Petr	k1gMnSc1	Petr
Oslzlý	oslzlý	k2eAgMnSc1d1	oslzlý
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
letech	léto	k1gNnPc6	léto
výrazně	výrazně	k6eAd1	výrazně
podílel	podílet	k5eAaImAgInS	podílet
na	na	k7c4	na
rozvíjení	rozvíjení	k1gNnSc4	rozvíjení
paradivadelních	paradivadelní	k2eAgFnPc2d1	paradivadelní
a	a	k8xC	a
společenských	společenský	k2eAgFnPc2d1	společenská
aktivit	aktivita	k1gFnPc2	aktivita
souboru	soubor	k1gInSc2	soubor
<g/>
,	,	kIx,	,
kontaktů	kontakt	k1gInPc2	kontakt
s	s	k7c7	s
hnutím	hnutí	k1gNnSc7	hnutí
otevřeného	otevřený	k2eAgNnSc2d1	otevřené
divadla	divadlo	k1gNnSc2	divadlo
a	a	k8xC	a
přípravě	příprava	k1gFnSc3	příprava
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
projektů	projekt	k1gInPc2	projekt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
festivalová	festivalový	k2eAgFnSc1d1	festivalová
akce	akce	k1gFnSc1	akce
Divadlo	divadlo	k1gNnSc1	divadlo
v	v	k7c6	v
pohybu	pohyb	k1gInSc6	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
se	se	k3xPyFc4	se
divadlo	divadlo	k1gNnSc1	divadlo
stalo	stát	k5eAaPmAgNnS	stát
známým	známý	k1gMnPc3	známý
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
postupně	postupně	k6eAd1	postupně
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
kraji	kraj	k1gInSc6	kraj
a	a	k8xC	a
velkých	velký	k2eAgNnPc6d1	velké
městech	město	k1gNnPc6	město
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1973	[number]	k4	1973
-	-	kIx~	-
1978	[number]	k4	1978
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
řada	řada	k1gFnSc1	řada
inscenací	inscenace	k1gFnPc2	inscenace
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
divadlo	divadlo	k1gNnSc4	divadlo
proslavily	proslavit	k5eAaPmAgFnP	proslavit
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
celé	celý	k2eAgFnSc2d1	celá
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
byl	být	k5eAaImAgInS	být
soubor	soubor	k1gInSc1	soubor
pravidelně	pravidelně	k6eAd1	pravidelně
zván	zvát	k5eAaImNgInS	zvát
na	na	k7c4	na
zahraniční	zahraniční	k2eAgInPc4d1	zahraniční
festivaly	festival	k1gInPc4	festival
a	a	k8xC	a
divadelní	divadelní	k2eAgFnSc2d1	divadelní
přehlídky	přehlídka	k1gFnSc2	přehlídka
(	(	kIx(	(
<g/>
Nancy	Nancy	k1gFnSc1	Nancy
<g/>
,	,	kIx,	,
Lodž	Lodž	k1gFnSc1	Lodž
<g/>
,	,	kIx,	,
Wroclaw	Wroclaw	k1gFnSc1	Wroclaw
<g/>
,	,	kIx,	,
Milán	Milán	k1gInSc1	Milán
<g/>
,	,	kIx,	,
Avignon	Avignon	k1gInSc1	Avignon
<g/>
,	,	kIx,	,
Berlín	Berlín	k1gInSc1	Berlín
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
počaly	počnout	k5eAaPmAgFnP	počnout
také	také	k9	také
mnohá	mnohé	k1gNnPc1	mnohé
přátelství	přátelství	k1gNnSc2	přátelství
a	a	k8xC	a
spolupráce	spolupráce	k1gFnSc2	spolupráce
se	s	k7c7	s
zahraničními	zahraniční	k2eAgFnPc7d1	zahraniční
skupinami	skupina	k1gFnPc7	skupina
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
s	s	k7c7	s
polským	polský	k2eAgNnSc7d1	polské
Teatrem	teatrum	k1gNnSc7	teatrum
77	[number]	k4	77
<g/>
.	.	kIx.	.
</s>
<s>
Divadlo	divadlo	k1gNnSc1	divadlo
na	na	k7c6	na
provázku	provázek	k1gInSc6	provázek
doplnilo	doplnit	k5eAaPmAgNnS	doplnit
svá	svůj	k3xOyFgNnPc4	svůj
programová	programový	k2eAgNnPc4d1	programové
východiska	východisko	k1gNnPc4	východisko
o	o	k7c4	o
koncept	koncept	k1gInSc4	koncept
otevřeného	otevřený	k2eAgNnSc2d1	otevřené
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
sestávající	sestávající	k2eAgMnSc1d1	sestávající
z	z	k7c2	z
paradivadelních	paradivadelní	k2eAgFnPc2d1	paradivadelní
akcí	akce	k1gFnPc2	akce
a	a	k8xC	a
pouličního	pouliční	k2eAgNnSc2d1	pouliční
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
Divadle	divadlo	k1gNnSc6	divadlo
na	na	k7c6	na
provázku	provázek	k1gInSc6	provázek
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
zřízeno	zřídit	k5eAaPmNgNnS	zřídit
amatérské	amatérský	k2eAgNnSc1d1	amatérské
Dětské	dětský	k2eAgNnSc1d1	dětské
studio	studio	k1gNnSc1	studio
<g/>
.	.	kIx.	.
</s>
<s>
Studio	studio	k1gNnSc1	studio
se	se	k3xPyFc4	se
řídilo	řídit	k5eAaImAgNnS	řídit
profesionálním	profesionální	k2eAgInSc7d1	profesionální
uměleckým	umělecký	k2eAgInSc7d1	umělecký
<g/>
,	,	kIx,	,
pedagogickým	pedagogický	k2eAgInSc7d1	pedagogický
a	a	k8xC	a
inscenačním	inscenační	k2eAgInSc7d1	inscenační
programem	program	k1gInSc7	program
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1977	[number]	k4	1977
byl	být	k5eAaImAgInS	být
soubor	soubor	k1gInSc1	soubor
divadla	divadlo	k1gNnSc2	divadlo
vyzván	vyzvat	k5eAaPmNgMnS	vyzvat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
podepsal	podepsat	k5eAaPmAgMnS	podepsat
pod	pod	k7c4	pod
prohlášení	prohlášení	k1gNnSc4	prohlášení
proti	proti	k7c3	proti
Chartě	charta	k1gFnSc3	charta
77	[number]	k4	77
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Antichartu	anticharta	k1gFnSc4	anticharta
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dlouhých	dlouhý	k2eAgFnPc6d1	dlouhá
diskuzích	diskuze	k1gFnPc6	diskuze
se	se	k3xPyFc4	se
kolektiv	kolektiv	k1gInSc1	kolektiv
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
prohlášení	prohlášení	k1gNnSc4	prohlášení
nepodepsat	podepsat	k5eNaPmF	podepsat
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
vědomě	vědomě	k6eAd1	vědomě
postavil	postavit	k5eAaPmAgMnS	postavit
do	do	k7c2	do
opozice	opozice	k1gFnSc2	opozice
vůči	vůči	k7c3	vůči
tehdejšímu	tehdejší	k2eAgInSc3d1	tehdejší
režimu	režim	k1gInSc3	režim
<g/>
.	.	kIx.	.
</s>
<s>
Program	program	k1gInSc1	program
divadla	divadlo	k1gNnSc2	divadlo
byl	být	k5eAaImAgInS	být
formulován	formulovat	k5eAaImNgInS	formulovat
jako	jako	k8xC	jako
nenásilný	násilný	k2eNgInSc1d1	nenásilný
boj	boj	k1gInSc1	boj
za	za	k7c4	za
demokracii	demokracie	k1gFnSc4	demokracie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
přípravou	příprava	k1gFnSc7	příprava
nového	nový	k2eAgInSc2d1	nový
divadelního	divadelní	k2eAgInSc2d1	divadelní
zákona	zákon	k1gInSc2	zákon
rozvíjejí	rozvíjet	k5eAaImIp3nP	rozvíjet
diskuze	diskuze	k1gFnPc1	diskuze
o	o	k7c6	o
budoucí	budoucí	k2eAgFnSc6d1	budoucí
existenci	existence	k1gFnSc6	existence
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
administrativně	administrativně	k6eAd1	administrativně
připojeno	připojit	k5eAaPmNgNnS	připojit
ke	k	k7c3	k
Státnímu	státní	k2eAgNnSc3d1	státní
divadlu	divadlo	k1gNnSc3	divadlo
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jeho	jeho	k3xOp3gMnSc3	jeho
pátý	pátý	k4xOgInSc1	pátý
<g/>
,	,	kIx,	,
experimentální	experimentální	k2eAgInSc1d1	experimentální
soubor	soubor	k1gInSc1	soubor
<g/>
,	,	kIx,	,
ze	z	k7c2	z
svého	svůj	k3xOyFgNnSc2	svůj
sídla	sídlo	k1gNnSc2	sídlo
v	v	k7c6	v
Domě	dům	k1gInSc6	dům
umění	umění	k1gNnSc2	umění
se	se	k3xPyFc4	se
ale	ale	k9	ale
přestěhovat	přestěhovat	k5eAaPmF	přestěhovat
nemuselo	muset	k5eNaImAgNnS	muset
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
se	se	k3xPyFc4	se
Stockholmu	Stockholm	k1gInSc6	Stockholm
Divadlo	divadlo	k1gNnSc4	divadlo
na	na	k7c6	na
provázku	provázek	k1gInSc6	provázek
stalo	stát	k5eAaPmAgNnS	stát
tajným	tajný	k2eAgMnSc7d1	tajný
členem	člen	k1gMnSc7	člen
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
federace	federace	k1gFnSc2	federace
nezávislých	závislý	k2eNgNnPc2d1	nezávislé
alternativních	alternativní	k2eAgNnPc2d1	alternativní
divadel	divadlo	k1gNnPc2	divadlo
(	(	kIx(	(
<g/>
IFIT	IFIT	kA	IFIT
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
česká	český	k2eAgFnSc1d1	Česká
premiéra	premiéra	k1gFnSc1	premiéra
projektu	projekt	k1gInSc2	projekt
Vesna	Vesna	k1gFnSc1	Vesna
národů	národ	k1gInPc2	národ
-	-	kIx~	-
Wiosna	Wiosna	k1gFnSc1	Wiosna
ludów	ludów	k?	ludów
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
Divadlo	divadlo	k1gNnSc1	divadlo
na	na	k7c6	na
provázku	provázek	k1gInSc6	provázek
přichystalo	přichystat	k5eAaPmAgNnS	přichystat
s	s	k7c7	s
polským	polský	k2eAgNnSc7d1	polské
Teatrem	teatrum	k1gNnSc7	teatrum
77	[number]	k4	77
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
událost	událost	k1gFnSc4	událost
rychle	rychle	k6eAd1	rychle
zareagovala	zareagovat	k5eAaPmAgFnS	zareagovat
cenzura	cenzura	k1gFnSc1	cenzura
a	a	k8xC	a
další	další	k2eAgNnPc1d1	další
uvádění	uvádění	k1gNnPc1	uvádění
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
i	i	k8xC	i
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
bylo	být	k5eAaImAgNnS	být
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
tato	tento	k3xDgFnSc1	tento
inscenace	inscenace	k1gFnSc1	inscenace
zapříčinila	zapříčinit	k5eAaPmAgFnS	zapříčinit
také	také	k9	také
nedobrovolnou	dobrovolný	k2eNgFnSc4d1	nedobrovolná
výměnu	výměna	k1gFnSc4	výměna
vedení	vedení	k1gNnSc2	vedení
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
nový	nový	k2eAgMnSc1d1	nový
umělecký	umělecký	k2eAgMnSc1d1	umělecký
vedoucí	vedoucí	k1gMnSc1	vedoucí
byl	být	k5eAaImAgMnS	být
dosazen	dosazen	k2eAgMnSc1d1	dosazen
herec	herec	k1gMnSc1	herec
činohry	činohra	k1gFnSc2	činohra
Státního	státní	k2eAgNnSc2d1	státní
divadla	divadlo	k1gNnSc2	divadlo
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Tuček	Tuček	k1gMnSc1	Tuček
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
KSČ	KSČ	kA	KSČ
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
tyto	tento	k3xDgInPc4	tento
cenzurní	cenzurní	k2eAgInPc4d1	cenzurní
zásahy	zásah	k1gInPc4	zásah
mělo	mít	k5eAaImAgNnS	mít
divadlo	divadlo	k1gNnSc4	divadlo
stále	stále	k6eAd1	stále
možnost	možnost	k1gFnSc4	možnost
vystupovat	vystupovat	k5eAaImF	vystupovat
na	na	k7c6	na
zahraničních	zahraniční	k2eAgInPc6d1	zahraniční
festivalech	festival	k1gInPc6	festival
a	a	k8xC	a
účastnit	účastnit	k5eAaImF	účastnit
se	se	k3xPyFc4	se
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
projektů	projekt	k1gInPc2	projekt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
se	se	k3xPyFc4	se
po	po	k7c6	po
dlouhodobých	dlouhodobý	k2eAgFnPc6d1	dlouhodobá
peripetiích	peripetie	k1gFnPc6	peripetie
podařilo	podařit	k5eAaPmAgNnS	podařit
inscenovat	inscenovat	k5eAaBmF	inscenovat
Příběhy	příběh	k1gInPc4	příběh
dlouhého	dlouhý	k2eAgInSc2d1	dlouhý
nosu	nos	k1gInSc2	nos
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Tálská	Tálská	k1gFnSc1	Tálská
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jevištní	jevištní	k2eAgFnSc4d1	jevištní
kreaci	kreace	k1gFnSc4	kreace
na	na	k7c4	na
verše	verš	k1gInPc4	verš
Edwarda	Edward	k1gMnSc2	Edward
Leara	Lear	k1gMnSc2	Lear
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
září	září	k1gNnSc4	září
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
odehrál	odehrát	k5eAaPmAgInS	odehrát
druhý	druhý	k4xOgInSc1	druhý
ročník	ročník	k1gInSc1	ročník
festivalové	festivalový	k2eAgFnSc2d1	festivalová
akce	akce	k1gFnSc2	akce
Divadlo	divadlo	k1gNnSc1	divadlo
v	v	k7c6	v
pohybu	pohyb	k1gInSc6	pohyb
II	II	kA	II
<g/>
,	,	kIx,	,
koncipované	koncipovaný	k2eAgFnSc2d1	koncipovaná
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
karneval	karneval	k1gInSc1	karneval
umění	umění	k1gNnSc2	umění
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
21	[number]	k4	21
dnech	den	k1gInPc6	den
představilo	představit	k5eAaPmAgNnS	představit
divadlo	divadlo	k1gNnSc4	divadlo
totožný	totožný	k2eAgInSc1d1	totožný
počet	počet	k1gInSc1	počet
vlastních	vlastní	k2eAgFnPc2d1	vlastní
inscenací	inscenace	k1gFnPc2	inscenace
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
vznikají	vznikat	k5eAaImIp3nP	vznikat
projektové	projektový	k2eAgFnPc1d1	projektová
studie	studie	k1gFnPc1	studie
ohledně	ohledně	k7c2	ohledně
nové	nový	k2eAgFnSc2d1	nová
divadelní	divadelní	k2eAgFnSc2d1	divadelní
budovy	budova	k1gFnSc2	budova
pro	pro	k7c4	pro
Divadlo	divadlo	k1gNnSc4	divadlo
na	na	k7c6	na
provázku	provázek	k1gInSc6	provázek
<g/>
.	.	kIx.	.
</s>
<s>
Architekt	architekt	k1gMnSc1	architekt
Václav	Václav	k1gMnSc1	Václav
Králíček	Králíček	k1gMnSc1	Králíček
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
studii	studie	k1gFnSc6	studie
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
návrh	návrh	k1gInSc4	návrh
rekonstrukce	rekonstrukce	k1gFnSc2	rekonstrukce
barokního	barokní	k2eAgInSc2d1	barokní
paláce	palác	k1gInSc2	palác
Domu	dům	k1gInSc2	dům
pánů	pan	k1gMnPc2	pan
z	z	k7c2	z
Fanalu	Fanal	k1gInSc2	Fanal
a	a	k8xC	a
dostavby	dostavba	k1gFnSc2	dostavba
moderní	moderní	k2eAgFnSc2d1	moderní
budovy	budova	k1gFnSc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
prosadit	prosadit	k5eAaPmF	prosadit
a	a	k8xC	a
na	na	k7c4	na
čelní	čelní	k2eAgFnSc4d1	čelní
zeď	zeď	k1gFnSc4	zeď
budoucího	budoucí	k2eAgInSc2d1	budoucí
divadelního	divadelní	k2eAgInSc2d1	divadelní
komplexu	komplex	k1gInSc2	komplex
byl	být	k5eAaImAgInS	být
upevněn	upevněn	k2eAgInSc1d1	upevněn
symbolický	symbolický	k2eAgInSc1d1	symbolický
základní	základní	k2eAgInSc1d1	základní
kámen	kámen	k1gInSc1	kámen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
byla	být	k5eAaImAgFnS	být
formulována	formulovat	k5eAaImNgFnS	formulovat
první	první	k4xOgFnSc1	první
vize	vize	k1gFnSc1	vize
Centra	centrum	k1gNnSc2	centrum
experimentálního	experimentální	k2eAgNnSc2d1	experimentální
divadla	divadlo	k1gNnSc2	divadlo
(	(	kIx(	(
<g/>
CED	CED	kA	CED
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
tvůrčího	tvůrčí	k2eAgInSc2d1	tvůrčí
dramatického	dramatický	k2eAgInSc2d1	dramatický
modelu	model	k1gInSc2	model
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
vizi	vize	k1gFnSc3	vize
připojilo	připojit	k5eAaPmAgNnS	připojit
také	také	k9	také
brněnské	brněnský	k2eAgNnSc1d1	brněnské
HaDivadlo	HaDivadlo	k1gNnSc1	HaDivadlo
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
sedmdesátého	sedmdesátý	k4xOgNnSc2	sedmdesátý
výročí	výročí	k1gNnSc2	výročí
vzniku	vznik	k1gInSc2	vznik
samostatného	samostatný	k2eAgInSc2d1	samostatný
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
Divadlo	divadlo	k1gNnSc1	divadlo
na	na	k7c6	na
provázku	provázek	k1gInSc6	provázek
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
HaDivadlem	HaDivadlo	k1gNnSc7	HaDivadlo
první	první	k4xOgNnSc4	první
číslo	číslo	k1gNnSc4	číslo
scénického	scénický	k2eAgInSc2d1	scénický
časopisu	časopis	k1gInSc2	časopis
Rozrazil	rozrazit	k5eAaPmAgInS	rozrazit
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
88	[number]	k4	88
(	(	kIx(	(
<g/>
o	o	k7c4	o
demokracii	demokracie	k1gFnSc4	demokracie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
publicistického	publicistický	k2eAgNnSc2d1	publicistické
divadla	divadlo	k1gNnSc2	divadlo
Rozrazil	rozrazil	k1gInSc1	rozrazil
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
z	z	k7c2	z
potřeby	potřeba	k1gFnSc2	potřeba
kriticky	kriticky	k6eAd1	kriticky
a	a	k8xC	a
otevřeně	otevřeně	k6eAd1	otevřeně
se	se	k3xPyFc4	se
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
k	k	k7c3	k
naléhavým	naléhavý	k2eAgNnPc3d1	naléhavé
tématům	téma	k1gNnPc3	téma
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
opakovaně	opakovaně	k6eAd1	opakovaně
nebylo	být	k5eNaImAgNnS	být
povoleno	povolit	k5eAaPmNgNnS	povolit
vydání	vydání	k1gNnSc1	vydání
časopisu	časopis	k1gInSc2	časopis
v	v	k7c6	v
tištěné	tištěný	k2eAgFnSc6d1	tištěná
podobě	podoba	k1gFnSc6	podoba
<g/>
,	,	kIx,	,
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
mezi	mezi	k7c7	mezi
umělci	umělec	k1gMnPc7	umělec
kolem	kolem	k7c2	kolem
zmíněných	zmíněný	k2eAgNnPc2d1	zmíněné
brněnských	brněnský	k2eAgNnPc2d1	brněnské
divadel	divadlo	k1gNnPc2	divadlo
idea	idea	k1gFnSc1	idea
vytvořit	vytvořit	k5eAaPmF	vytvořit
jej	on	k3xPp3gInSc4	on
scénicky	scénicky	k6eAd1	scénicky
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
letech	léto	k1gNnPc6	léto
následovaly	následovat	k5eAaImAgFnP	následovat
čísla	číslo	k1gNnSc2	číslo
Rozrazil	rozrazit	k5eAaPmAgInS	rozrazit
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
89	[number]	k4	89
(	(	kIx(	(
<g/>
Občanské	občanský	k2eAgNnSc1d1	občanské
fórum	fórum	k1gNnSc1	fórum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rozrazil	rozrazil	k1gInSc1	rozrazil
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
99	[number]	k4	99
(	(	kIx(	(
<g/>
K	k	k7c3	k
současnosti	současnost	k1gFnSc3	současnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rozrazil	rozrazil	k1gInSc1	rozrazil
4	[number]	k4	4
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
(	(	kIx(	(
<g/>
O	o	k7c6	o
Brně	Brno	k1gNnSc6	Brno
<g/>
)	)	kIx)	)
a	a	k8xC	a
zatím	zatím	k6eAd1	zatím
poslední	poslední	k2eAgInSc4d1	poslední
Rozrazil	rozrazil	k1gInSc4	rozrazil
5	[number]	k4	5
<g/>
/	/	kIx~	/
<g/>
2013	[number]	k4	2013
-	-	kIx~	-
O	o	k7c6	o
naší	náš	k3xOp1gFnSc6	náš
nynější	nynější	k2eAgFnSc6d1	nynější
krizi	krize	k1gFnSc6	krize
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1989	[number]	k4	1989
zahájilo	zahájit	k5eAaPmAgNnS	zahájit
Divadlo	divadlo	k1gNnSc1	divadlo
na	na	k7c6	na
provázku	provázek	k1gInSc6	provázek
společně	společně	k6eAd1	společně
s	s	k7c7	s
HaDivadlem	HaDivadlo	k1gNnSc7	HaDivadlo
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
uměleckým	umělecký	k2eAgInSc7d1	umělecký
a	a	k8xC	a
občanským	občanský	k2eAgInSc7d1	občanský
programem	program	k1gInSc7	program
divadla	divadlo	k1gNnSc2	divadlo
stávku	stávek	k1gInSc2	stávek
divadel	divadlo	k1gNnPc2	divadlo
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
jako	jako	k8xS	jako
protest	protest	k1gInSc1	protest
proti	proti	k7c3	proti
komunistickému	komunistický	k2eAgInSc3d1	komunistický
režimu	režim	k1gInSc3	režim
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
v	v	k7c6	v
prvních	první	k4xOgInPc6	první
dnech	den	k1gInPc6	den
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
vrátilo	vrátit	k5eAaPmAgNnS	vrátit
divadlo	divadlo	k1gNnSc1	divadlo
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
názvu	název	k1gInSc2	název
zakázané	zakázaný	k2eAgNnSc1d1	zakázané
slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
husa	husa	k1gFnSc1	husa
<g/>
"	"	kIx"	"
a	a	k8xC	a
přejmenovalo	přejmenovat	k5eAaPmAgNnS	přejmenovat
se	se	k3xPyFc4	se
na	na	k7c4	na
Divadlo	divadlo	k1gNnSc4	divadlo
Husa	Hus	k1gMnSc2	Hus
na	na	k7c6	na
provázku	provázek	k1gInSc6	provázek
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
vedení	vedení	k1gNnSc2	vedení
odešel	odejít	k5eAaPmAgMnS	odejít
komunistickými	komunistický	k2eAgFnPc7d1	komunistická
orgány	orgány	k1gFnPc4	orgány
přidělený	přidělený	k2eAgMnSc1d1	přidělený
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Tuček	Tuček	k1gMnSc1	Tuček
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
HaDivadlem	HaDivadlo	k1gNnSc7	HaDivadlo
se	se	k3xPyFc4	se
Divadlo	divadlo	k1gNnSc1	divadlo
Husa	husa	k1gFnSc1	husa
na	na	k7c6	na
provázku	provázek	k1gInSc6	provázek
pustilo	pustit	k5eAaPmAgNnS	pustit
do	do	k7c2	do
realizace	realizace	k1gFnSc2	realizace
projektu	projekt	k1gInSc2	projekt
Centra	centrum	k1gNnSc2	centrum
experimentálního	experimentální	k2eAgNnSc2d1	experimentální
divadla	divadlo	k1gNnSc2	divadlo
(	(	kIx(	(
<g/>
CED	CED	kA	CED
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
prvním	první	k4xOgMnSc7	první
ředitelem	ředitel	k1gMnSc7	ředitel
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Milan	Milan	k1gMnSc1	Milan
Sedláček	Sedláček	k1gMnSc1	Sedláček
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgNnPc2	první
československých	československý	k2eAgNnPc2d1	Československé
divadel	divadlo	k1gNnPc2	divadlo
uvedlo	uvést	k5eAaPmAgNnS	uvést
inscenaci	inscenace	k1gFnSc4	inscenace
Havlovy	Havlův	k2eAgFnSc2d1	Havlova
Zahradní	zahradní	k2eAgFnSc2d1	zahradní
slavnosti	slavnost	k1gFnSc2	slavnost
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
se	se	k3xPyFc4	se
také	také	k9	také
rozvíjela	rozvíjet	k5eAaImAgFnS	rozvíjet
realizace	realizace	k1gFnSc1	realizace
nového	nový	k2eAgInSc2d1	nový
areálu	areál	k1gInSc2	areál
Divadla	divadlo	k1gNnSc2	divadlo
Husa	Hus	k1gMnSc2	Hus
na	na	k7c6	na
provázku	provázek	k1gInSc6	provázek
<g/>
.	.	kIx.	.
</s>
<s>
Změna	změna	k1gFnSc1	změna
společenské	společenský	k2eAgFnSc2d1	společenská
situace	situace	k1gFnSc2	situace
umožnila	umožnit	k5eAaPmAgFnS	umožnit
divadlu	divadlo	k1gNnSc3	divadlo
častější	častý	k2eAgFnSc4d2	častější
možnost	možnost	k1gFnSc4	možnost
cesty	cesta	k1gFnSc2	cesta
do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
května	květen	k1gInSc2	květen
divadlo	divadlo	k1gNnSc1	divadlo
pod	pod	k7c7	pod
hlavičkou	hlavička	k1gFnSc7	hlavička
CED	CED	kA	CED
organizovalo	organizovat	k5eAaBmAgNnS	organizovat
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
už	už	k9	už
sjednoceném	sjednocený	k2eAgInSc6d1	sjednocený
Berlíně	Berlín	k1gInSc6	Berlín
přehlídku	přehlídka	k1gFnSc4	přehlídka
českých	český	k2eAgNnPc2d1	české
a	a	k8xC	a
slovenských	slovenský	k2eAgNnPc2d1	slovenské
divadel	divadlo	k1gNnPc2	divadlo
Divadelní	divadelní	k2eAgInSc1d1	divadelní
festival	festival	k1gInSc1	festival
ČSFR	ČSFR	kA	ČSFR
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
doprovázely	doprovázet	k5eAaImAgInP	doprovázet
přednášky	přednáška	k1gFnPc4	přednáška
a	a	k8xC	a
výstavy	výstava	k1gFnPc4	výstava
<g/>
.	.	kIx.	.
</s>
<s>
Následovaly	následovat	k5eAaImAgInP	následovat
festivaly	festival	k1gInPc4	festival
a	a	k8xC	a
představení	představení	k1gNnPc4	představení
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
,	,	kIx,	,
Mnichově	Mnichov	k1gInSc6	Mnichov
nebo	nebo	k8xC	nebo
dánském	dánský	k2eAgInSc6d1	dánský
Aarhusu	Aarhus	k1gInSc6	Aarhus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
se	se	k3xPyFc4	se
Divadlo	divadlo	k1gNnSc1	divadlo
Husa	husa	k1gFnSc1	husa
na	na	k7c6	na
provázku	provázek	k1gInSc6	provázek
a	a	k8xC	a
HaDivadlo	HaDivadlo	k1gNnSc1	HaDivadlo
odpojilo	odpojit	k5eAaPmAgNnS	odpojit
od	od	k7c2	od
Zemského	zemský	k2eAgNnSc2d1	zemské
divadla	divadlo	k1gNnSc2	divadlo
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
začalo	začít	k5eAaPmAgNnS	začít
právně	právně	k6eAd1	právně
existovat	existovat	k5eAaImF	existovat
Centrum	centrum	k1gNnSc1	centrum
experimentálního	experimentální	k2eAgNnSc2d1	experimentální
divadla	divadlo	k1gNnSc2	divadlo
(	(	kIx(	(
<g/>
CED	CED	kA	CED
<g/>
)	)	kIx)	)
a	a	k8xC	a
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
tak	tak	k6eAd1	tak
nová	nový	k2eAgFnSc1d1	nová
kulturní	kulturní	k2eAgFnSc1d1	kulturní
instituce	instituce	k1gFnSc1	instituce
řízená	řízený	k2eAgFnSc1d1	řízená
městem	město	k1gNnSc7	město
Brno	Brno	k1gNnSc1	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
ledna	leden	k1gInSc2	leden
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
byly	být	k5eAaImAgFnP	být
dokončeny	dokončit	k5eAaPmNgFnP	dokončit
práce	práce	k1gFnPc1	práce
na	na	k7c6	na
budově	budova	k1gFnSc6	budova
Domu	dům	k1gInSc2	dům
pánů	pan	k1gMnPc2	pan
z	z	k7c2	z
Fanalu	Fanal	k1gInSc2	Fanal
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
tvořil	tvořit	k5eAaImAgInS	tvořit
první	první	k4xOgFnSc4	první
část	část	k1gFnSc4	část
nového	nový	k2eAgInSc2d1	nový
divadelního	divadelní	k2eAgInSc2d1	divadelní
areálu	areál	k1gInSc2	areál
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
slavnostního	slavnostní	k2eAgNnSc2d1	slavnostní
otevření	otevření	k1gNnSc2	otevření
11	[number]	k4	11
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
probíhala	probíhat	k5eAaImAgFnS	probíhat
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
prostoru	prostor	k1gInSc6	prostor
historické	historický	k2eAgFnSc2d1	historická
budovy	budova	k1gFnSc2	budova
mnohahodinová	mnohahodinový	k2eAgFnSc1d1	mnohahodinová
akce	akce	k1gFnSc1	akce
nazvaná	nazvaný	k2eAgFnSc1d1	nazvaná
Otevření	otevření	k1gNnPc2	otevření
dveří	dveře	k1gFnPc2	dveře
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
května	květen	k1gInSc2	květen
1993	[number]	k4	1993
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
happeningové	happeningový	k2eAgNnSc1d1	happeningové
rozloučení	rozloučení	k1gNnSc1	rozloučení
se	se	k3xPyFc4	se
s	s	k7c7	s
bývalým	bývalý	k2eAgInSc7d1	bývalý
prostorem	prostor	k1gInSc7	prostor
divadla	divadlo	k1gNnSc2	divadlo
v	v	k7c6	v
Domě	dům	k1gInSc6	dům
umění	umění	k1gNnSc2	umění
s	s	k7c7	s
názvem	název	k1gInSc7	název
Posledních	poslední	k2eAgFnPc2d1	poslední
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
-	-	kIx~	-
bál	bál	k1gInSc1	bál
vzpomínek	vzpomínka	k1gFnPc2	vzpomínka
<g/>
.	.	kIx.	.
</s>
<s>
Akce	akce	k1gFnSc1	akce
probíhala	probíhat	k5eAaImAgFnS	probíhat
od	od	k7c2	od
29	[number]	k4	29
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
51	[number]	k4	51
hodin	hodina	k1gFnPc2	hodina
ráno	ráno	k6eAd1	ráno
do	do	k7c2	do
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
52	[number]	k4	52
hodin	hodina	k1gFnPc2	hodina
druhého	druhý	k4xOgInSc2	druhý
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
otevírá	otevírat	k5eAaImIp3nS	otevírat
celý	celý	k2eAgInSc1d1	celý
nový	nový	k2eAgInSc1d1	nový
areál	areál	k1gInSc1	areál
Divadla	divadlo	k1gNnSc2	divadlo
Husa	Hus	k1gMnSc2	Hus
na	na	k7c6	na
provázku	provázek	k1gInSc6	provázek
na	na	k7c6	na
Zelném	zelný	k2eAgInSc6d1	zelný
trhu	trh	k1gInSc6	trh
a	a	k8xC	a
přilehlé	přilehlý	k2eAgFnSc6d1	přilehlá
Petrské	petrský	k2eAgFnSc6d1	Petrská
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
další	další	k2eAgInSc1d1	další
ročník	ročník	k1gInSc1	ročník
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
kulturního	kulturní	k2eAgInSc2d1	kulturní
festivalu	festival	k1gInSc2	festival
Divadlo	divadlo	k1gNnSc1	divadlo
v	v	k7c6	v
pohybu	pohyb	k1gInSc6	pohyb
(	(	kIx(	(
<g/>
IV	IV	kA	IV
<g/>
)	)	kIx)	)
-	-	kIx~	-
Brno	Brno	k1gNnSc1	Brno
93	[number]	k4	93
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1994	[number]	k4	1994
se	se	k3xPyFc4	se
Divadlo	divadlo	k1gNnSc1	divadlo
Husa	husa	k1gFnSc1	husa
na	na	k7c6	na
provázku	provázek	k1gInSc6	provázek
poprvé	poprvé	k6eAd1	poprvé
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
divadelní	divadelní	k2eAgFnSc7d1	divadelní
fakultou	fakulta	k1gFnSc7	fakulta
JAMU	jam	k1gInSc2	jam
a	a	k8xC	a
městem	město	k1gNnSc7	město
Brnem	Brno	k1gNnSc7	Brno
stalo	stát	k5eAaPmAgNnS	stát
spolupořadatelem	spolupořadatel	k1gMnSc7	spolupořadatel
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
festivalu	festival	k1gInSc2	festival
divadelních	divadelní	k2eAgFnPc2d1	divadelní
škol	škola	k1gFnPc2	škola
Setkání	setkání	k1gNnSc2	setkání
<g/>
/	/	kIx~	/
<g/>
Encounter	Encounter	k1gInSc1	Encounter
Brno	Brno	k1gNnSc1	Brno
'	'	kIx"	'
<g/>
94	[number]	k4	94
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yIgNnSc7	který
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
CED	CED	kA	CED
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
podniklo	podniknout	k5eAaPmAgNnS	podniknout
divadlo	divadlo	k1gNnSc1	divadlo
několik	několik	k4yIc4	několik
zahraničních	zahraniční	k2eAgInPc2d1	zahraniční
zájezdů	zájezd	k1gInPc2	zájezd
<g/>
,	,	kIx,	,
vystupovali	vystupovat	k5eAaImAgMnP	vystupovat
na	na	k7c6	na
Knižním	knižní	k2eAgInSc6d1	knižní
veletrhu	veletrh	k1gInSc6	veletrh
v	v	k7c6	v
Lipsku	Lipsko	k1gNnSc6	Lipsko
<g/>
,	,	kIx,	,
s	s	k7c7	s
inscenací	inscenace	k1gFnSc7	inscenace
Labyrint	labyrint	k1gInSc1	labyrint
světa	svět	k1gInSc2	svět
na	na	k7c6	na
slavnostním	slavnostní	k2eAgNnSc6d1	slavnostní
otevření	otevření	k1gNnSc6	otevření
Komenského	Komenský	k1gMnSc2	Komenský
zahrady	zahrada	k1gFnSc2	zahrada
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
v	v	k7c6	v
Bad	Bad	k1gFnSc6	Bad
Kissingenu	Kissingen	k1gInSc2	Kissingen
nebo	nebo	k8xC	nebo
polském	polský	k2eAgInSc6d1	polský
Sanoku	Sanok	k1gInSc6	Sanok
<g/>
.	.	kIx.	.
</s>
<s>
Divadlo	divadlo	k1gNnSc1	divadlo
se	se	k3xPyFc4	se
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
také	také	k6eAd1	také
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
festivalu	festival	k1gInSc2	festival
experimentálních	experimentální	k2eAgNnPc2d1	experimentální
divadel	divadlo	k1gNnPc2	divadlo
<g/>
,	,	kIx,	,
druhé	druhý	k4xOgFnSc2	druhý
nejvýznamnější	významný	k2eAgFnSc2d3	nejvýznamnější
akce	akce	k1gFnSc2	akce
svého	svůj	k3xOyFgInSc2	svůj
druhu	druh	k1gInSc2	druh
v	v	k7c6	v
arabském	arabský	k2eAgInSc6d1	arabský
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
v	v	k7c6	v
egyptské	egyptský	k2eAgFnSc6d1	egyptská
Káhiře	Káhira	k1gFnSc6	Káhira
<g/>
.	.	kIx.	.
</s>
<s>
Divadlo	divadlo	k1gNnSc1	divadlo
Husa	Hus	k1gMnSc2	Hus
na	na	k7c6	na
provázku	provázek	k1gInSc6	provázek
také	také	k9	také
pravidelně	pravidelně	k6eAd1	pravidelně
hostilo	hostit	k5eAaImAgNnS	hostit
soubory	soubor	k1gInPc4	soubor
a	a	k8xC	a
umělce	umělec	k1gMnSc4	umělec
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1998	[number]	k4	1998
uběhlo	uběhnout	k5eAaPmAgNnS	uběhnout
přesně	přesně	k6eAd1	přesně
třicet	třicet	k4xCc1	třicet
let	léto	k1gNnPc2	léto
od	od	k7c2	od
prvního	první	k4xOgNnSc2	první
představení	představení	k1gNnSc2	představení
Husy	husa	k1gFnSc2	husa
na	na	k7c6	na
provázku	provázek	k1gInSc6	provázek
v	v	k7c6	v
Procházkově	Procházkův	k2eAgFnSc6d1	Procházkova
síni	síň	k1gFnSc6	síň
Domu	dům	k1gInSc2	dům
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Divadlo	divadlo	k1gNnSc1	divadlo
toto	tento	k3xDgNnSc1	tento
výročí	výročí	k1gNnSc1	výročí
oslavilo	oslavit	k5eAaPmAgNnS	oslavit
uspořádáním	uspořádání	k1gNnSc7	uspořádání
divadelního	divadelní	k2eAgInSc2d1	divadelní
happening	happening	k1gInSc1	happening
-	-	kIx~	-
expozice	expozice	k1gFnSc1	expozice
Repete	repete	k6eAd1	repete
po	po	k7c6	po
třiceti	třicet	k4xCc6	třicet
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vstupní	vstupní	k2eAgFnSc6d1	vstupní
hale	hala	k1gFnSc6	hala
Domu	dům	k1gInSc2	dům
umění	umění	k1gNnSc2	umění
byly	být	k5eAaImAgInP	být
vystaveny	vystaven	k2eAgInPc1d1	vystaven
plakáty	plakát	k1gInPc1	plakát
<g/>
,	,	kIx,	,
programy	program	k1gInPc1	program
<g/>
,	,	kIx,	,
fotografie	fotografia	k1gFnPc1	fotografia
a	a	k8xC	a
různé	různý	k2eAgFnPc1d1	různá
dekorace	dekorace	k1gFnPc1	dekorace
a	a	k8xC	a
rekvizity	rekvizit	k1gInPc1	rekvizit
z	z	k7c2	z
historie	historie	k1gFnSc2	historie
divadla	divadlo	k1gNnSc2	divadlo
i	i	k8xC	i
jeho	jeho	k3xOp3gFnSc7	jeho
inscenací	inscenace	k1gFnSc7	inscenace
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
zde	zde	k6eAd1	zde
probíhalo	probíhat	k5eAaImAgNnS	probíhat
několikahodinové	několikahodinový	k2eAgNnSc1d1	několikahodinové
pásmo	pásmo	k1gNnSc1	pásmo
složené	složený	k2eAgNnSc1d1	složené
z	z	k7c2	z
písní	píseň	k1gFnPc2	píseň
a	a	k8xC	a
úryvků	úryvek	k1gInPc2	úryvek
z	z	k7c2	z
inscenací	inscenace	k1gFnPc2	inscenace
<g/>
,	,	kIx,	,
předčítání	předčítání	k1gNnSc1	předčítání
dokumentů	dokument	k1gInPc2	dokument
<g/>
,	,	kIx,	,
prohlášení	prohlášení	k1gNnPc2	prohlášení
a	a	k8xC	a
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Vrcholem	vrchol	k1gInSc7	vrchol
byla	být	k5eAaImAgFnS	být
scénická	scénický	k2eAgFnSc1d1	scénická
akce	akce	k1gFnSc1	akce
nazvaná	nazvaný	k2eAgFnSc1d1	nazvaná
Počítání	počítání	k1gNnPc2	počítání
husiček	husička	k1gFnPc2	husička
v	v	k7c6	v
divadelní	divadelní	k2eAgFnSc6d1	divadelní
polévce	polévka	k1gFnSc6	polévka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
rozmanitými	rozmanitý	k2eAgInPc7d1	rozmanitý
způsoby	způsob	k1gInPc4	způsob
odkazovala	odkazovat	k5eAaImAgFnS	odkazovat
na	na	k7c4	na
všechny	všechen	k3xTgFnPc4	všechen
inscenace	inscenace	k1gFnPc4	inscenace
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
Divadlo	divadlo	k1gNnSc1	divadlo
Husa	Hus	k1gMnSc2	Hus
na	na	k7c6	na
provázku	provázek	k1gInSc6	provázek
dosud	dosud	k6eAd1	dosud
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1999	[number]	k4	1999
zemřel	zemřít	k5eAaPmAgMnS	zemřít
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
zakládajících	zakládající	k2eAgMnPc2d1	zakládající
členů	člen	k1gMnPc2	člen
divadla	divadlo	k1gNnSc2	divadlo
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
"	"	kIx"	"
<g/>
duchovní	duchovní	k1gMnSc1	duchovní
vůdce	vůdce	k1gMnSc1	vůdce
<g/>
"	"	kIx"	"
Peter	Peter	k1gMnSc1	Peter
Scherhaufer	Scherhaufer	k1gMnSc1	Scherhaufer
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
den	den	k1gInSc1	den
před	před	k7c7	před
odletem	odlet	k1gInSc7	odlet
Divadla	divadlo	k1gNnSc2	divadlo
Husa	Hus	k1gMnSc2	Hus
na	na	k7c6	na
provázku	provázek	k1gInSc6	provázek
na	na	k7c4	na
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
festival	festival	k1gInSc4	festival
do	do	k7c2	do
Jihoafrické	jihoafrický	k2eAgFnSc2d1	Jihoafrická
republiky	republika	k1gFnSc2	republika
s	s	k7c7	s
inscenací	inscenace	k1gFnSc7	inscenace
Svatba	svatba	k1gFnSc1	svatba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
je	být	k5eAaImIp3nS	být
také	také	k9	také
obnoveno	obnoven	k2eAgNnSc1d1	obnoveno
"	"	kIx"	"
<g/>
vycházení	vycházení	k1gNnSc2	vycházení
<g/>
"	"	kIx"	"
scénického	scénický	k2eAgInSc2d1	scénický
časopisu	časopis	k1gInSc2	časopis
Divadla	divadlo	k1gNnSc2	divadlo
Husa	Hus	k1gMnSc2	Hus
na	na	k7c6	na
provázku	provázek	k1gInSc2	provázek
a	a	k8xC	a
HaDivadla	HaDivadlo	k1gNnSc2	HaDivadlo
Rozrazil	rozrazit	k5eAaPmAgInS	rozrazit
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
99	[number]	k4	99
(	(	kIx(	(
<g/>
k	k	k7c3	k
současnosti	současnost	k1gFnSc3	současnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
více	hodně	k6eAd2	hodně
než	než	k8xS	než
třiceti	třicet	k4xCc6	třicet
letech	let	k1gInPc6	let
je	být	k5eAaImIp3nS	být
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2000	[number]	k4	2000
uvedena	uveden	k2eAgFnSc1d1	uvedena
hra	hra	k1gFnSc1	hra
Komedie	komedie	k1gFnSc2	komedie
o	o	k7c4	o
umučení	umučení	k1gNnSc4	umučení
a	a	k8xC	a
vzkříšení	vzkříšení	k1gNnSc4	vzkříšení
našeho	náš	k3xOp1gMnSc2	náš
Pána	pán	k1gMnSc2	pán
a	a	k8xC	a
Spasitele	spasitel	k1gMnSc2	spasitel
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
v	v	k7c6	v
režii	režie	k1gFnSc6	režie
Evy	Eva	k1gFnSc2	Eva
Tálské	Tálská	k1gFnSc2	Tálská
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
realizaci	realizace	k1gFnSc3	realizace
bylo	být	k5eAaImAgNnS	být
přizváno	přizván	k2eAgNnSc1d1	přizváno
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
účinkujících	účinkující	k1gMnPc2	účinkující
<g/>
,	,	kIx,	,
inscenace	inscenace	k1gFnSc1	inscenace
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stala	stát	k5eAaPmAgFnS	stát
nejlidnatější	lidnatý	k2eAgFnSc7d3	nejlidnatější
inscenací	inscenace	k1gFnSc7	inscenace
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
také	také	k9	také
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
spolupráce	spolupráce	k1gFnSc1	spolupráce
se	s	k7c7	s
Sdružením	sdružení	k1gNnSc7	sdružení
Serpents	Serpentsa	k1gFnPc2	Serpentsa
a	a	k8xC	a
divadlem	divadlo	k1gNnSc7	divadlo
Archa	archa	k1gFnSc1	archa
na	na	k7c6	na
společném	společný	k2eAgInSc6d1	společný
projektu	projekt	k1gInSc6	projekt
Archa	archa	k1gFnSc1	archa
2000	[number]	k4	2000
(	(	kIx(	(
<g/>
divadlo	divadlo	k1gNnSc1	divadlo
v	v	k7c6	v
pohybu	pohyb	k1gInSc6	pohyb
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
Synagoze	synagoga	k1gFnSc6	synagoga
na	na	k7c6	na
Palmovce	Palmovka	k1gFnSc6	Palmovka
<g/>
,	,	kIx,	,
v	v	k7c6	v
opuštěných	opuštěný	k2eAgFnPc6d1	opuštěná
budovách	budova	k1gFnPc6	budova
holešovického	holešovický	k2eAgInSc2d1	holešovický
pivovaru	pivovar	k1gInSc2	pivovar
i	i	k9	i
v	v	k7c6	v
ulicích	ulice	k1gFnPc6	ulice
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
a	a	k8xC	a
tramvajových	tramvajový	k2eAgInPc6d1	tramvajový
vozech	vůz	k1gInPc6	vůz
<g/>
.	.	kIx.	.
</s>
<s>
Posléze	posléze	k6eAd1	posléze
se	se	k3xPyFc4	se
podobná	podobný	k2eAgFnSc1d1	podobná
akce	akce	k1gFnSc1	akce
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
také	také	k9	také
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
sezóny	sezóna	k1gFnSc2	sezóna
2001	[number]	k4	2001
<g/>
/	/	kIx~	/
<g/>
2002	[number]	k4	2002
přišel	přijít	k5eAaPmAgMnS	přijít
do	do	k7c2	do
Divadla	divadlo	k1gNnSc2	divadlo
Husa	Hus	k1gMnSc2	Hus
na	na	k7c6	na
provázku	provázek	k1gInSc6	provázek
coby	coby	k?	coby
režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
dramaturg	dramaturg	k1gMnSc1	dramaturg
a	a	k8xC	a
autor	autor	k1gMnSc1	autor
Luboš	Luboš	k1gMnSc1	Luboš
Balák	Balák	k1gMnSc1	Balák
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
představil	představit	k5eAaPmAgMnS	představit
projektem	projekt	k1gInSc7	projekt
divadelního	divadelní	k2eAgInSc2d1	divadelní
sitcom	sitcom	k1gInSc4	sitcom
seriálu	seriál	k1gInSc2	seriál
Funebráci	funebrák	k1gMnPc1	funebrák
<g/>
,	,	kIx,	,
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2002	[number]	k4	2002
se	se	k3xPyFc4	se
Balák	Balák	k1gMnSc1	Balák
stal	stát	k5eAaPmAgMnS	stát
novým	nový	k2eAgMnSc7d1	nový
uměleckým	umělecký	k2eAgMnSc7d1	umělecký
šéfem	šéf	k1gMnSc7	šéf
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Novou	nový	k2eAgFnSc4d1	nová
sezónu	sezóna	k1gFnSc4	sezóna
2003	[number]	k4	2003
<g/>
/	/	kIx~	/
<g/>
2004	[number]	k4	2004
zahájil	zahájit	k5eAaPmAgInS	zahájit
Losers	Losers	k1gInSc1	Losers
<g/>
́	́	k?	́
<g/>
festival	festival	k1gInSc1	festival
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
náplní	náplň	k1gFnSc7	náplň
byly	být	k5eAaImAgInP	být
hra	hra	k1gFnSc1	hra
na	na	k7c4	na
téma	téma	k1gNnSc4	téma
loserů	loser	k1gMnPc2	loser
a	a	k8xC	a
vyděděnců	vyděděnec	k1gMnPc2	vyděděnec
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Divadla	divadlo	k1gNnSc2	divadlo
Husa	Hus	k1gMnSc2	Hus
na	na	k7c6	na
provázku	provázek	k1gInSc6	provázek
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
této	tento	k3xDgFnSc3	tento
akci	akce	k1gFnSc3	akce
dostalo	dostat	k5eAaPmAgNnS	dostat
několik	několik	k4yIc1	několik
mladých	mladý	k2eAgMnPc2d1	mladý
režisérů	režisér	k1gMnPc2	režisér
z	z	k7c2	z
brněnské	brněnský	k2eAgFnSc2d1	brněnská
JAMU	jam	k1gInSc3	jam
<g/>
,	,	kIx,	,
Hana	Hana	k1gFnSc1	Hana
Mikolášová	Mikolášová	k1gFnSc1	Mikolášová
<g/>
,	,	kIx,	,
Ondřej	Ondřej	k1gMnSc1	Ondřej
Elbl	Elbl	k1gMnSc1	Elbl
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Baďura	Baďura	k1gMnSc1	Baďura
a	a	k8xC	a
Jolana	Jolana	k1gFnSc1	Jolana
Kubíková	Kubíková	k1gFnSc1	Kubíková
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
se	se	k3xPyFc4	se
do	do	k7c2	do
divadla	divadlo	k1gNnSc2	divadlo
vrátil	vrátit	k5eAaPmAgMnS	vrátit
Vladimír	Vladimír	k1gMnSc1	Vladimír
Morávek	Morávek	k1gMnSc1	Morávek
s	s	k7c7	s
prvním	první	k4xOgNnSc7	první
dílem	dílo	k1gNnSc7	dílo
projektu	projekt	k1gInSc2	projekt
Sto	sto	k4xCgNnSc4	sto
roků	rok	k1gInPc2	rok
kobry	kobra	k1gFnSc2	kobra
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
díl	díl	k1gInSc1	díl
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
mnoha	mnoho	k4c3	mnoho
personálním	personální	k2eAgFnPc3d1	personální
a	a	k8xC	a
organizačním	organizační	k2eAgFnPc3d1	organizační
změnám	změna	k1gFnPc3	změna
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
nový	nový	k2eAgMnSc1d1	nový
umělecký	umělecký	k2eAgMnSc1d1	umělecký
šéf	šéf	k1gMnSc1	šéf
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
Vladimír	Vladimír	k1gMnSc1	Vladimír
Morávek	Morávek	k1gMnSc1	Morávek
<g/>
,	,	kIx,	,
do	do	k7c2	do
souboru	soubor	k1gInSc2	soubor
přišlo	přijít	k5eAaPmAgNnS	přijít
několik	několik	k4yIc1	několik
nových	nový	k2eAgMnPc2d1	nový
herců	herec	k1gMnPc2	herec
a	a	k8xC	a
organizačně	organizačně	k6eAd1	organizačně
technických	technický	k2eAgMnPc2d1	technický
pracovníků	pracovník	k1gMnPc2	pracovník
<g/>
.	.	kIx.	.
</s>
<s>
Nástup	nástup	k1gInSc1	nástup
nového	nový	k2eAgNnSc2d1	nové
uměleckého	umělecký	k2eAgNnSc2d1	umělecké
vedení	vedení	k1gNnSc2	vedení
prezentoval	prezentovat	k5eAaBmAgMnS	prezentovat
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
sezóny	sezóna	k1gFnSc2	sezóna
festival	festival	k1gInSc4	festival
Sedm	sedm	k4xCc4	sedm
žlutých	žlutý	k2eAgInPc2d1	žlutý
praporů	prapor	k1gInPc2	prapor
aneb	aneb	k?	aneb
Nová	nový	k2eAgFnSc1d1	nová
smlouva	smlouva	k1gFnSc1	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Skrze	Skrze	k?	Skrze
personifikované	personifikovaný	k2eAgInPc4d1	personifikovaný
hříchy	hřích	k1gInPc4	hřích
naší	náš	k3xOp1gFnSc2	náš
doby	doba	k1gFnSc2	doba
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
sedmi	sedm	k4xCc6	sedm
dnech	den	k1gInPc6	den
tematizovány	tematizovat	k5eAaImNgFnP	tematizovat
největší	veliký	k2eAgFnPc1d3	veliký
lidské	lidský	k2eAgFnPc1d1	lidská
slabosti	slabost	k1gFnPc1	slabost
a	a	k8xC	a
marnosti	marnost	k1gFnPc1	marnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
festivalu	festival	k1gInSc2	festival
probíhaly	probíhat	k5eAaImAgInP	probíhat
koncerty	koncert	k1gInPc1	koncert
kapel	kapela	k1gFnPc2	kapela
<g/>
,	,	kIx,	,
autorská	autorský	k2eAgNnPc1d1	autorské
čtení	čtení	k1gNnPc1	čtení
<g/>
,	,	kIx,	,
představení	představení	k1gNnPc1	představení
krátkých	krátký	k2eAgFnPc2d1	krátká
inscenací	inscenace	k1gFnPc2	inscenace
nebo	nebo	k8xC	nebo
skečů	skeč	k1gInPc2	skeč
<g/>
,	,	kIx,	,
projekce	projekce	k1gFnPc1	projekce
filmů	film	k1gInPc2	film
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
aktivity	aktivita	k1gFnPc4	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Ojedinělou	ojedinělý	k2eAgFnSc7d1	ojedinělá
událostí	událost	k1gFnSc7	událost
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
byla	být	k5eAaImAgFnS	být
premiéra	premiéra	k1gFnSc1	premiéra
jedenáctihodinové	jedenáctihodinový	k2eAgFnSc2d1	jedenáctihodinová
scénické	scénický	k2eAgFnSc2d1	scénická
fresky	freska	k1gFnSc2	freska
Svlékání	svlékání	k1gNnSc1	svlékání
z	z	k7c2	z
kůže	kůže	k1gFnSc2	kůže
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Morávka	Morávek	k1gMnSc2	Morávek
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
festivalu	festival	k1gInSc2	festival
Idioti	idiot	k1gMnPc1	idiot
hoří	hořet	k5eAaImIp3nP	hořet
aneb	aneb	k?	aneb
Evropské	evropský	k2eAgInPc1d1	evropský
dny	den	k1gInPc1	den
Fjodora	Fjodor	k1gMnSc2	Fjodor
Michajloviče	Michajlovič	k1gMnSc2	Michajlovič
Dostojevského	Dostojevský	k2eAgMnSc2d1	Dostojevský
<g/>
.	.	kIx.	.
</s>
<s>
Inscenace	inscenace	k1gFnSc1	inscenace
byla	být	k5eAaImAgFnS	být
všeobecně	všeobecně	k6eAd1	všeobecně
přijata	přijmout	k5eAaPmNgFnS	přijmout
laickou	laický	k2eAgFnSc7d1	laická
i	i	k8xC	i
odbornou	odborný	k2eAgFnSc7d1	odborná
veřejností	veřejnost	k1gFnSc7	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Herec	herec	k1gMnSc1	herec
Jiří	Jiří	k1gMnSc1	Jiří
Vyorálek	Vyorálek	k1gMnSc1	Vyorálek
byl	být	k5eAaImAgMnS	být
za	za	k7c4	za
ztvárnění	ztvárnění	k1gNnSc4	ztvárnění
všech	všecek	k3xTgFnPc2	všecek
mužských	mužský	k2eAgFnPc2d1	mužská
rolí	role	k1gFnPc2	role
nominován	nominován	k2eAgMnSc1d1	nominován
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
Thálie	Thálie	k1gFnSc2	Thálie
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
celého	celý	k2eAgInSc2d1	celý
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
probíhala	probíhat	k5eAaImAgFnS	probíhat
pravidelná	pravidelný	k2eAgFnSc1d1	pravidelná
setkání	setkání	k1gNnSc4	setkání
významných	významný	k2eAgFnPc2d1	významná
osobností	osobnost	k1gFnPc2	osobnost
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
oborů	obor	k1gInPc2	obor
na	na	k7c6	na
Kabinetech	kabinet	k1gInPc6	kabinet
Havel	Havla	k1gFnPc2	Havla
<g/>
.	.	kIx.	.
</s>
<s>
Náplní	náplň	k1gFnSc7	náplň
byly	být	k5eAaImAgFnP	být
diskuze	diskuze	k1gFnPc4	diskuze
o	o	k7c6	o
současné	současný	k2eAgFnSc6d1	současná
české	český	k2eAgFnSc6d1	Česká
společnosti	společnost	k1gFnSc6	společnost
a	a	k8xC	a
budoucnosti	budoucnost	k1gFnSc6	budoucnost
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
nazvané	nazvaný	k2eAgFnSc6d1	nazvaná
České	český	k2eAgFnSc3d1	Česká
moře	mora	k1gFnSc3	mora
byly	být	k5eAaImAgFnP	být
uvedeny	uveden	k2eAgInPc1d1	uveden
ryze	ryze	k6eAd1	ryze
české	český	k2eAgInPc1d1	český
tituly	titul	k1gInPc1	titul
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
zabývají	zabývat	k5eAaImIp3nP	zabývat
tématy	téma	k1gNnPc7	téma
dotýkajících	dotýkající	k2eAgInPc6d1	dotýkající
se	se	k3xPyFc4	se
přímo	přímo	k6eAd1	přímo
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
jejích	její	k3xOp3gMnPc2	její
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
završilo	završit	k5eAaPmAgNnS	završit
divadlo	divadlo	k1gNnSc1	divadlo
svůj	svůj	k3xOyFgInSc4	svůj
návrat	návrat	k1gInSc4	návrat
na	na	k7c4	na
evropskou	evropský	k2eAgFnSc4d1	Evropská
scénu	scéna	k1gFnSc4	scéna
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
účastnilo	účastnit	k5eAaImAgNnS	účastnit
rozsáhlého	rozsáhlý	k2eAgInSc2d1	rozsáhlý
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
projektu	projekt	k1gInSc2	projekt
k	k	k7c3	k
uctění	uctění	k1gNnSc3	uctění
zesnulého	zesnulý	k2eAgMnSc2d1	zesnulý
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
<g/>
.	.	kIx.	.
</s>
<s>
Začátek	začátek	k1gInSc1	začátek
sezóny	sezóna	k1gFnSc2	sezóna
2013	[number]	k4	2013
<g/>
/	/	kIx~	/
<g/>
2014	[number]	k4	2014
patřil	patřit	k5eAaImAgInS	patřit
9	[number]	k4	9
<g/>
.	.	kIx.	.
ročníku	ročník	k1gInSc2	ročník
Divadla	divadlo	k1gNnSc2	divadlo
v	v	k7c6	v
pohybu	pohyb	k1gInSc6	pohyb
s	s	k7c7	s
názvem	název	k1gInSc7	název
Duha	duha	k1gFnSc1	duha
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnPc3	který
Divadlo	divadlo	k1gNnSc1	divadlo
Husa	Hus	k1gMnSc2	Hus
na	na	k7c6	na
provázku	provázek	k1gInSc6	provázek
připomnělo	připomnět	k5eAaPmAgNnS	připomnět
45	[number]	k4	45
let	léto	k1gNnPc2	léto
své	svůj	k3xOyFgFnSc2	svůj
existence	existence	k1gFnSc2	existence
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
devíti	devět	k4xCc2	devět
dní	den	k1gInPc2	den
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
45	[number]	k4	45
divadelních	divadelní	k2eAgFnPc2d1	divadelní
událostí	událost	k1gFnPc2	událost
<g/>
,	,	kIx,	,
festivalu	festival	k1gInSc2	festival
se	se	k3xPyFc4	se
účastnila	účastnit	k5eAaImAgFnS	účastnit
také	také	k9	také
řada	řada	k1gFnSc1	řada
hostů	host	k1gMnPc2	host
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
se	se	k3xPyFc4	se
Divadlo	divadlo	k1gNnSc1	divadlo
Husa	husa	k1gFnSc1	husa
na	na	k7c6	na
provázku	provázek	k1gInSc6	provázek
angažovalo	angažovat	k5eAaBmAgNnS	angažovat
v	v	k7c6	v
iniciativě	iniciativa	k1gFnSc6	iniciativa
pojmenovat	pojmenovat	k5eAaPmF	pojmenovat
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
bezejmennou	bezejmenný	k2eAgFnSc4d1	bezejmenná
uličku	ulička	k1gFnSc4	ulička
pod	pod	k7c7	pod
Petrovem	Petrov	k1gInSc7	Petrov
po	po	k7c6	po
Václavu	Václav	k1gMnSc6	Václav
Havlovi	Havel	k1gMnSc6	Havel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1968	[number]	k4	1968
-	-	kIx~	-
1993	[number]	k4	1993
sídlilo	sídlit	k5eAaImAgNnS	sídlit
divadlo	divadlo	k1gNnSc1	divadlo
v	v	k7c6	v
prázdném	prázdný	k2eAgInSc6d1	prázdný
prostoru	prostor	k1gInSc6	prostor
Procházkovy	Procházkův	k2eAgFnSc2d1	Procházkova
síně	síň	k1gFnSc2	síň
v	v	k7c6	v
brněnském	brněnský	k2eAgInSc6d1	brněnský
Domu	dům	k1gInSc6wR	dům
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
zde	zde	k6eAd1	zde
hrálo	hrát	k5eAaImAgNnS	hrát
sporadicky	sporadicky	k6eAd1	sporadicky
<g/>
,	,	kIx,	,
po	po	k7c6	po
profesionalizaci	profesionalizace	k1gFnSc6	profesionalizace
pravidelně	pravidelně	k6eAd1	pravidelně
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
od	od	k7c2	od
konce	konec	k1gInSc2	konec
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
divadlo	divadlo	k1gNnSc1	divadlo
snažilo	snažit	k5eAaImAgNnS	snažit
najít	najít	k5eAaPmF	najít
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
vhodnější	vhodný	k2eAgFnPc1d2	vhodnější
prostory	prostora	k1gFnPc1	prostora
<g/>
,	,	kIx,	,
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
několik	několik	k4yIc1	několik
návrhů	návrh	k1gInPc2	návrh
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
žádný	žádný	k3yNgInSc1	žádný
nebyl	být	k5eNaImAgInS	být
realizován	realizovat	k5eAaBmNgInS	realizovat
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
prošel	projít	k5eAaPmAgInS	projít
návrh	návrh	k1gInSc1	návrh
rekonstrukce	rekonstrukce	k1gFnSc2	rekonstrukce
a	a	k8xC	a
dostavby	dostavba	k1gFnSc2	dostavba
barokního	barokní	k2eAgInSc2d1	barokní
paláce	palác	k1gInSc2	palác
pánů	pan	k1gMnPc2	pan
z	z	k7c2	z
Fanalu	Fanal	k1gInSc2	Fanal
na	na	k7c6	na
Zelném	zelný	k2eAgInSc6d1	zelný
trhu	trh	k1gInSc6	trh
od	od	k7c2	od
architekta	architekt	k1gMnSc2	architekt
Václava	Václav	k1gMnSc2	Václav
Králíčka	Králíček	k1gMnSc2	Králíček
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
návrh	návrh	k1gInSc1	návrh
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
nesetkal	setkat	k5eNaPmAgMnS	setkat
s	s	k7c7	s
pochopením	pochopení	k1gNnSc7	pochopení
a	a	k8xC	a
místo	místo	k1gNnSc4	místo
Králíčka	Králíček	k1gMnSc2	Králíček
nastoupili	nastoupit	k5eAaPmAgMnP	nastoupit
architekti	architekt	k1gMnPc1	architekt
Karel	Karel	k1gMnSc1	Karel
Hubáček	Hubáček	k1gMnSc1	Hubáček
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Hakulín	Hakulína	k1gFnPc2	Hakulína
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
se	se	k3xPyFc4	se
stavbou	stavba	k1gFnSc7	stavba
nového	nový	k2eAgNnSc2d1	nové
křídla	křídlo	k1gNnSc2	křídlo
probíhala	probíhat	k5eAaImAgFnS	probíhat
také	také	k9	také
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
historické	historický	k2eAgFnSc2d1	historická
budovy	budova	k1gFnSc2	budova
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
sklepních	sklepní	k2eAgFnPc2d1	sklepní
prostor	prostora	k1gFnPc2	prostora
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
nového	nový	k2eAgNnSc2d1	nové
působiště	působiště	k1gNnSc2	působiště
se	se	k3xPyFc4	se
divadlo	divadlo	k1gNnSc1	divadlo
přestěhovalo	přestěhovat	k5eAaPmAgNnS	přestěhovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
a	a	k8xC	a
působí	působit	k5eAaImIp3nS	působit
zde	zde	k6eAd1	zde
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
suterénu	suterén	k1gInSc6	suterén
historické	historický	k2eAgFnSc2d1	historická
budovy	budova	k1gFnSc2	budova
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
unikátní	unikátní	k2eAgFnSc1d1	unikátní
Sklepní	sklepní	k2eAgFnSc1d1	sklepní
scéna	scéna	k1gFnSc1	scéna
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
propojením	propojení	k1gNnSc7	propojení
středověkých	středověký	k2eAgNnPc2d1	středověké
sklepení	sklepení	k1gNnPc2	sklepení
<g/>
.	.	kIx.	.
</s>
<s>
Jeviště	jeviště	k1gNnSc1	jeviště
má	mít	k5eAaImIp3nS	mít
podobu	podoba	k1gFnSc4	podoba
dřevěné	dřevěný	k2eAgFnSc2d1	dřevěná
laťové	laťový	k2eAgFnSc2d1	laťová
podlahy	podlaha	k1gFnSc2	podlaha
a	a	k8xC	a
od	od	k7c2	od
hlediště	hlediště	k1gNnSc2	hlediště
je	být	k5eAaImIp3nS	být
opticky	opticky	k6eAd1	opticky
odděleno	oddělit	k5eAaPmNgNnS	oddělit
mohutným	mohutný	k2eAgInSc7d1	mohutný
klenebním	klenební	k2eAgInSc7d1	klenební
pásem	pás	k1gInSc7	pás
<g/>
.	.	kIx.	.
</s>
<s>
Hlediště	hlediště	k1gNnPc1	hlediště
tvoří	tvořit	k5eAaImIp3nP	tvořit
stupňovité	stupňovitý	k2eAgFnPc4d1	stupňovitá
dřevěné	dřevěný	k2eAgFnPc4d1	dřevěná
podesty	podesta	k1gFnPc4	podesta
a	a	k8xC	a
volně	volně	k6eAd1	volně
postavenými	postavený	k2eAgNnPc7d1	postavené
sedadly	sedadlo	k1gNnPc7	sedadlo
<g/>
,	,	kIx,	,
kapacita	kapacita	k1gFnSc1	kapacita
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
70	[number]	k4	70
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
Sklepní	sklepní	k2eAgFnSc1d1	sklepní
scéna	scéna	k1gFnSc1	scéna
domovským	domovský	k2eAgInSc7d1	domovský
prostorem	prostor	k1gInSc7	prostor
Divadla	divadlo	k1gNnSc2	divadlo
U	u	k7c2	u
stolu	stol	k1gInSc2	stol
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
odehrávají	odehrávat	k5eAaImIp3nP	odehrávat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
i	i	k9	i
mnohé	mnohý	k2eAgFnPc1d1	mnohá
produkce	produkce	k1gFnPc1	produkce
Divadla	divadlo	k1gNnSc2	divadlo
Husa	Hus	k1gMnSc2	Hus
na	na	k7c6	na
provázku	provázek	k1gInSc6	provázek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
částech	část	k1gFnPc6	část
historické	historický	k2eAgFnSc2d1	historická
budovy	budova	k1gFnSc2	budova
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
administrativní	administrativní	k2eAgNnSc1d1	administrativní
a	a	k8xC	a
provozní	provozní	k2eAgNnSc1d1	provozní
zázemí	zázemí	k1gNnSc1	zázemí
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
historickou	historický	k2eAgFnSc4d1	historická
budovu	budova	k1gFnSc4	budova
přímo	přímo	k6eAd1	přímo
navazuje	navazovat	k5eAaImIp3nS	navazovat
venkovní	venkovní	k2eAgFnSc1d1	venkovní
Alžbětinská	alžbětinský	k2eAgFnSc1d1	Alžbětinská
scéna	scéna	k1gFnSc1	scéna
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ji	on	k3xPp3gFnSc4	on
propojuje	propojovat	k5eAaImIp3nS	propojovat
s	s	k7c7	s
novou	nový	k2eAgFnSc7d1	nová
přístavbou	přístavba	k1gFnSc7	přístavba
<g/>
.	.	kIx.	.
</s>
<s>
Otevřený	otevřený	k2eAgInSc4d1	otevřený
prostor	prostor	k1gInSc4	prostor
dvora	dvůr	k1gInSc2	dvůr
obklopují	obklopovat	k5eAaImIp3nP	obklopovat
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
stran	strana	k1gFnPc2	strana
zastřešené	zastřešený	k2eAgInPc1d1	zastřešený
ochozy	ochoz	k1gInPc1	ochoz
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
scéna	scéna	k1gFnSc1	scéna
slouží	sloužit	k5eAaImIp3nS	sloužit
především	především	k9	především
pro	pro	k7c4	pro
představení	představení	k1gNnSc4	představení
v	v	k7c6	v
letních	letní	k2eAgInPc6d1	letní
měsících	měsíc	k1gInPc6	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Alžbětinská	alžbětinský	k2eAgFnSc1d1	Alžbětinská
scéna	scéna	k1gFnSc1	scéna
může	moct	k5eAaImIp3nS	moct
pojmout	pojmout	k5eAaPmF	pojmout
až	až	k9	až
250	[number]	k4	250
diváků	divák	k1gMnPc2	divák
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nově	nově	k6eAd1	nově
postavené	postavený	k2eAgFnSc6d1	postavená
budově	budova	k1gFnSc6	budova
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Velký	velký	k2eAgInSc1d1	velký
sál	sál	k1gInSc1	sál
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
variabilním	variabilní	k2eAgInSc7d1	variabilní
prostorem	prostor	k1gInSc7	prostor
bez	bez	k7c2	bez
zřetelného	zřetelný	k2eAgNnSc2d1	zřetelné
rozdělení	rozdělení	k1gNnSc2	rozdělení
hlediště	hlediště	k1gNnSc2	hlediště
a	a	k8xC	a
jeviště	jeviště	k1gNnSc2	jeviště
<g/>
.	.	kIx.	.
</s>
<s>
Hlediště	hlediště	k1gNnSc1	hlediště
se	se	k3xPyFc4	se
buduje	budovat	k5eAaImIp3nS	budovat
podle	podle	k7c2	podle
potřeb	potřeba	k1gFnPc2	potřeba
konkrétní	konkrétní	k2eAgFnSc2d1	konkrétní
inscenace	inscenace	k1gFnSc2	inscenace
pomocí	pomocí	k7c2	pomocí
stupňovitých	stupňovitý	k2eAgFnPc2d1	stupňovitá
podest	podesta	k1gFnPc2	podesta
a	a	k8xC	a
mobilních	mobilní	k2eAgNnPc2d1	mobilní
sedadel	sedadlo	k1gNnPc2	sedadlo
<g/>
,	,	kIx,	,
variabilní	variabilní	k2eAgInSc1d1	variabilní
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
i	i	k9	i
jeho	jeho	k3xOp3gFnSc1	jeho
kapacita	kapacita	k1gFnSc1	kapacita
<g/>
.	.	kIx.	.
</s>
<s>
Soubor	soubor	k1gInSc1	soubor
Divadla	divadlo	k1gNnSc2	divadlo
Husa	Hus	k1gMnSc2	Hus
na	na	k7c6	na
provázku	provázek	k1gInSc6	provázek
využívá	využívat	k5eAaPmIp3nS	využívat
pro	pro	k7c4	pro
své	svůj	k3xOyFgMnPc4	svůj
inscenace	inscenace	k1gFnSc1	inscenace
také	také	k6eAd1	také
prostoru	prostor	k1gInSc2	prostor
foyer	foyer	k1gNnPc2	foyer
<g/>
.	.	kIx.	.
</s>
<s>
Divadlo	divadlo	k1gNnSc1	divadlo
Husa	Hus	k1gMnSc2	Hus
na	na	k7c6	na
provázku	provázek	k1gInSc6	provázek
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
předním	přední	k2eAgFnPc3d1	přední
českým	český	k2eAgFnPc3d1	Česká
experimentálním	experimentální	k2eAgFnPc3d1	experimentální
scénám	scéna	k1gFnPc3	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Důležitým	důležitý	k2eAgInSc7d1	důležitý
principem	princip	k1gInSc7	princip
tvorby	tvorba	k1gFnSc2	tvorba
Divadla	divadlo	k1gNnSc2	divadlo
Husa	Hus	k1gMnSc2	Hus
na	na	k7c6	na
provázku	provázek	k1gInSc6	provázek
je	být	k5eAaImIp3nS	být
už	už	k6eAd1	už
od	od	k7c2	od
prvních	první	k4xOgInPc2	první
sezón	sezóna	k1gFnPc2	sezóna
nepravidelná	pravidelný	k2eNgFnSc1d1	nepravidelná
dramaturgie	dramaturgie	k1gFnSc1	dramaturgie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
usiluje	usilovat	k5eAaImIp3nS	usilovat
o	o	k7c6	o
hledání	hledání	k1gNnSc6	hledání
nových	nový	k2eAgFnPc2d1	nová
cest	cesta	k1gFnPc2	cesta
metodou	metoda	k1gFnSc7	metoda
zkoumání	zkoumání	k1gNnSc2	zkoumání
a	a	k8xC	a
experimentu	experiment	k1gInSc2	experiment
<g/>
.	.	kIx.	.
</s>
<s>
Klíčovou	klíčový	k2eAgFnSc4d1	klíčová
roli	role	k1gFnSc4	role
při	při	k7c6	při
tvorbě	tvorba	k1gFnSc6	tvorba
inscenace	inscenace	k1gFnSc2	inscenace
hraje	hrát	k5eAaImIp3nS	hrát
osobnost	osobnost	k1gFnSc1	osobnost
režiséra	režisér	k1gMnSc2	režisér
<g/>
.	.	kIx.	.
</s>
<s>
Poetika	poetika	k1gFnSc1	poetika
divadla	divadlo	k1gNnSc2	divadlo
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
silně	silně	k6eAd1	silně
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
vyhraněnými	vyhraněný	k2eAgInPc7d1	vyhraněný
rukopisy	rukopis	k1gInPc7	rukopis
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
režisérských	režisérský	k2eAgFnPc2d1	režisérská
osobností	osobnost	k1gFnPc2	osobnost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
působily	působit	k5eAaImAgInP	působit
a	a	k8xC	a
působí	působit	k5eAaImIp3nP	působit
<g/>
.	.	kIx.	.
</s>
<s>
Důležitou	důležitý	k2eAgFnSc7d1	důležitá
myšlenkou	myšlenka	k1gFnSc7	myšlenka
je	být	k5eAaImIp3nS	být
také	také	k9	také
pohled	pohled	k1gInSc4	pohled
na	na	k7c4	na
divadlo	divadlo	k1gNnSc4	divadlo
jako	jako	k8xS	jako
místo	místo	k6eAd1	místo
sdružující	sdružující	k2eAgMnPc4d1	sdružující
lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yRgFnPc4	který
je	být	k5eAaImIp3nS	být
divadlo	divadlo	k1gNnSc1	divadlo
životním	životní	k2eAgInSc7d1	životní
postojem	postoj	k1gInSc7	postoj
<g/>
,	,	kIx,	,
a	a	k8xC	a
vnímání	vnímání	k1gNnSc1	vnímání
divadla	divadlo	k1gNnSc2	divadlo
jako	jako	k8xC	jako
živého	živý	k2eAgNnSc2d1	živé
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
Rozličné	rozličný	k2eAgInPc1d1	rozličný
přístupy	přístup	k1gInPc1	přístup
se	se	k3xPyFc4	se
uplatňovaly	uplatňovat	k5eAaImAgInP	uplatňovat
už	už	k6eAd1	už
u	u	k7c2	u
režisérů	režisér	k1gMnPc2	režisér
stojících	stojící	k2eAgMnPc2d1	stojící
u	u	k7c2	u
zrodu	zrod	k1gInSc2	zrod
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Režisérka	režisérka	k1gFnSc1	režisérka
Eva	Eva	k1gFnSc1	Eva
Tálská	Tálská	k1gFnSc1	Tálská
inklinovala	inklinovat	k5eAaImAgFnS	inklinovat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
tvorbě	tvorba	k1gFnSc6	tvorba
k	k	k7c3	k
nonsensovým	nonsensový	k2eAgInPc3d1	nonsensový
textům	text	k1gInPc3	text
a	a	k8xC	a
scénickým	scénický	k2eAgFnPc3d1	scénická
variacím	variace	k1gFnPc3	variace
pohádkových	pohádkový	k2eAgInPc2d1	pohádkový
námětů	námět	k1gInPc2	námět
<g/>
.	.	kIx.	.
</s>
<s>
Věnovala	věnovat	k5eAaPmAgFnS	věnovat
se	se	k3xPyFc4	se
také	také	k9	také
adaptacím	adaptace	k1gFnPc3	adaptace
básnických	básnický	k2eAgNnPc2d1	básnické
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
Inspiraci	inspirace	k1gFnSc4	inspirace
čerpala	čerpat	k5eAaImAgFnS	čerpat
především	především	k9	především
z	z	k7c2	z
folklorní	folklorní	k2eAgFnSc2d1	folklorní
a	a	k8xC	a
lidové	lidový	k2eAgFnSc2d1	lidová
tematiky	tematika	k1gFnSc2	tematika
<g/>
,	,	kIx,	,
převažovalo	převažovat	k5eAaImAgNnS	převažovat
u	u	k7c2	u
ní	on	k3xPp3gFnSc2	on
lyrické	lyrický	k2eAgNnSc1d1	lyrické
ladění	ladění	k1gNnSc4	ladění
a	a	k8xC	a
metaforičnost	metaforičnost	k1gFnSc4	metaforičnost
<g/>
,	,	kIx,	,
zabývala	zabývat	k5eAaImAgFnS	zabývat
se	se	k3xPyFc4	se
hudebností	hudebnost	k1gFnSc7	hudebnost
slova	slovo	k1gNnSc2	slovo
a	a	k8xC	a
významotvorným	významotvorný	k2eAgNnSc7d1	významotvorné
využitím	využití	k1gNnSc7	využití
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
soustředila	soustředit	k5eAaPmAgFnS	soustředit
především	především	k9	především
na	na	k7c4	na
práci	práce	k1gFnSc4	práce
s	s	k7c7	s
dětmi	dítě	k1gFnPc7	dítě
a	a	k8xC	a
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Pospíšil	Pospíšil	k1gMnSc1	Pospíšil
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
fungování	fungování	k1gNnSc2	fungování
divadla	divadlo	k1gNnSc2	divadlo
dvě	dva	k4xCgFnPc1	dva
inscenace	inscenace	k1gFnPc1	inscenace
textových	textový	k2eAgFnPc2d1	textová
montáží	montáž	k1gFnPc2	montáž
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
zaměřil	zaměřit	k5eAaPmAgMnS	zaměřit
na	na	k7c4	na
scénické	scénický	k2eAgInPc4d1	scénický
přepisy	přepis	k1gInPc4	přepis
prozaických	prozaický	k2eAgFnPc2d1	prozaická
předloh	předloha	k1gFnPc2	předloha
<g/>
,	,	kIx,	,
spolupracoval	spolupracovat	k5eAaImAgInS	spolupracovat
i	i	k9	i
s	s	k7c7	s
tehdy	tehdy	k6eAd1	tehdy
zakázanými	zakázaný	k2eAgMnPc7d1	zakázaný
autory	autor	k1gMnPc7	autor
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Milan	Milan	k1gMnSc1	Milan
Uhde	Uhd	k1gFnSc2	Uhd
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Typickými	typický	k2eAgInPc7d1	typický
rysy	rys	k1gInPc7	rys
jeho	jeho	k3xOp3gFnSc2	jeho
tvorby	tvorba	k1gFnSc2	tvorba
byla	být	k5eAaImAgFnS	být
hravost	hravost	k1gFnSc1	hravost
<g/>
,	,	kIx,	,
komediální	komediální	k2eAgFnSc1d1	komediální
zkratka	zkratka	k1gFnSc1	zkratka
a	a	k8xC	a
různé	různý	k2eAgFnPc1d1	různá
formy	forma	k1gFnPc1	forma
vyprávění	vyprávění	k1gNnSc2	vyprávění
příběhu	příběh	k1gInSc2	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Peter	Peter	k1gMnSc1	Peter
Scherhaufer	Scherhaufer	k1gMnSc1	Scherhaufer
se	se	k3xPyFc4	se
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
tvorbě	tvorba	k1gFnSc6	tvorba
nechal	nechat	k5eAaPmAgInS	nechat
inspirovat	inspirovat	k5eAaBmF	inspirovat
lidovým	lidový	k2eAgNnSc7d1	lidové
divadlem	divadlo	k1gNnSc7	divadlo
<g/>
,	,	kIx,	,
komedií	komedie	k1gFnSc7	komedie
dell	della	k1gFnPc2	della
<g/>
'	'	kIx"	'
<g/>
Arte	Arte	k1gInSc1	Arte
a	a	k8xC	a
Brechtovským	Brechtovský	k2eAgNnSc7d1	Brechtovský
epickým	epický	k2eAgNnSc7d1	epické
divadlem	divadlo	k1gNnSc7	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Scherhaufer	Scherhaufer	k1gMnSc1	Scherhaufer
inscenoval	inscenovat	k5eAaBmAgMnS	inscenovat
rozličné	rozličný	k2eAgFnPc4d1	rozličná
předlohy	předloha	k1gFnPc4	předloha
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
už	už	k6eAd1	už
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
adaptace	adaptace	k1gFnPc4	adaptace
próz	próza	k1gFnPc2	próza
<g/>
,	,	kIx,	,
poezii	poezie	k1gFnSc4	poezie
<g/>
,	,	kIx,	,
filmové	filmový	k2eAgInPc4d1	filmový
scénáře	scénář	k1gInPc4	scénář
nebo	nebo	k8xC	nebo
dramatické	dramatický	k2eAgInPc1d1	dramatický
texty	text	k1gInPc1	text
<g/>
.	.	kIx.	.
</s>
<s>
Tíhl	tíhnout	k5eAaImAgInS	tíhnout
k	k	k7c3	k
velkým	velký	k2eAgNnPc3d1	velké
a	a	k8xC	a
naléhavým	naléhavý	k2eAgNnPc3d1	naléhavé
tématům	téma	k1gNnPc3	téma
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
sděloval	sdělovat	k5eAaImAgInS	sdělovat
bohatým	bohatý	k2eAgInSc7d1	bohatý
scénickým	scénický	k2eAgInSc7d1	scénický
jazykem	jazyk	k1gInSc7	jazyk
a	a	k8xC	a
dynamickým	dynamický	k2eAgInSc7d1	dynamický
tvarem	tvar	k1gInSc7	tvar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnohých	mnohý	k2eAgFnPc6d1	mnohá
inscenacích	inscenace	k1gFnPc6	inscenace
spojoval	spojovat	k5eAaImAgMnS	spojovat
postupy	postup	k1gInPc4	postup
činohry	činohra	k1gFnSc2	činohra
<g/>
,	,	kIx,	,
opery	opera	k1gFnSc2	opera
<g/>
,	,	kIx,	,
pantomimy	pantomima	k1gFnSc2	pantomima
<g/>
,	,	kIx,	,
klaunerie	klaunerie	k1gFnSc2	klaunerie
i	i	k8xC	i
baletu	balet	k1gInSc2	balet
<g/>
.	.	kIx.	.
</s>
<s>
Koncept	koncept	k1gInSc1	koncept
nepravidelné	pravidelný	k2eNgFnSc2d1	nepravidelná
dramaturgie	dramaturgie	k1gFnSc2	dramaturgie
výrazně	výrazně	k6eAd1	výrazně
rozvíjel	rozvíjet	k5eAaImAgMnS	rozvíjet
Petr	Petr	k1gMnSc1	Petr
Oslzlý	oslzlý	k2eAgMnSc1d1	oslzlý
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
do	do	k7c2	do
divadla	divadlo	k1gNnSc2	divadlo
přišel	přijít	k5eAaPmAgInS	přijít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
<g/>
.	.	kIx.	.
</s>
<s>
Oslzlý	oslzlý	k2eAgMnSc1d1	oslzlý
vědomě	vědomě	k6eAd1	vědomě
vyhledával	vyhledávat	k5eAaImAgMnS	vyhledávat
texty	text	k1gInPc4	text
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
přirozeně	přirozeně	k6eAd1	přirozeně
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
dramatičnost	dramatičnost	k1gFnSc4	dramatičnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gNnSc4	jejich
téma	téma	k1gNnSc4	téma
stavěl	stavět	k5eAaImAgMnS	stavět
do	do	k7c2	do
konfrontace	konfrontace	k1gFnSc2	konfrontace
s	s	k7c7	s
realitou	realita	k1gFnSc7	realita
<g/>
.	.	kIx.	.
</s>
<s>
Inscenace	inscenace	k1gFnSc1	inscenace
tedy	tedy	k8xC	tedy
umožňovaly	umožňovat	k5eAaImAgFnP	umožňovat
nový	nový	k2eAgInSc4d1	nový
pohled	pohled	k1gInSc4	pohled
na	na	k7c4	na
problémy	problém	k1gInPc4	problém
a	a	k8xC	a
potřeby	potřeba	k1gFnPc4	potřeba
tvůrců	tvůrce	k1gMnPc2	tvůrce
a	a	k8xC	a
diváků	divák	k1gMnPc2	divák
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
Scherhaufer	Scherhaufer	k1gMnSc1	Scherhaufer
i	i	k8xC	i
Oslzlý	oslzlý	k2eAgMnSc1d1	oslzlý
pracoval	pracovat	k5eAaImAgMnS	pracovat
s	s	k7c7	s
montáží	montáž	k1gFnSc7	montáž
a	a	k8xC	a
koláží	koláž	k1gFnSc7	koláž
různých	různý	k2eAgInPc2d1	různý
materiálů	materiál	k1gInPc2	materiál
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
vytvářely	vytvářet	k5eAaImAgFnP	vytvářet
prostor	prostor	k1gInSc4	prostor
pro	pro	k7c4	pro
otevřenou	otevřený	k2eAgFnSc4d1	otevřená
komunikaci	komunikace	k1gFnSc4	komunikace
s	s	k7c7	s
publikem	publikum	k1gNnSc7	publikum
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
režijně	režijně	k6eAd1	režijně
realizovat	realizovat	k5eAaBmF	realizovat
také	také	k9	také
herec	herec	k1gMnSc1	herec
Bolek	Bolek	k1gMnSc1	Bolek
Polívka	Polívka	k1gMnSc1	Polívka
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
autorské	autorský	k2eAgInPc1d1	autorský
projekty	projekt	k1gInPc1	projekt
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
další	další	k2eAgFnSc7d1	další
linií	linie	k1gFnSc7	linie
tvorby	tvorba	k1gFnSc2	tvorba
Divadla	divadlo	k1gNnSc2	divadlo
na	na	k7c6	na
provázku	provázek	k1gInSc6	provázek
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgFnPc4	svůj
celovečerní	celovečerní	k2eAgFnPc4d1	celovečerní
klauniády	klauniáda	k1gFnPc4	klauniáda
stavěl	stavět	k5eAaImAgMnS	stavět
na	na	k7c6	na
jednoduchém	jednoduchý	k2eAgInSc6d1	jednoduchý
příběhu	příběh	k1gInSc6	příběh
a	a	k8xC	a
pohybovém	pohybový	k2eAgNnSc6d1	pohybové
umění	umění	k1gNnSc6	umění
<g/>
.	.	kIx.	.
</s>
<s>
Spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
především	především	k9	především
s	s	k7c7	s
herci	herec	k1gMnPc7	herec
Jiřím	Jiří	k1gMnSc7	Jiří
Pechou	Pecha	k1gMnSc7	Pecha
<g/>
,	,	kIx,	,
Chantal	Chantal	k1gMnSc1	Chantal
Poullain	Poullain	k1gMnSc1	Poullain
a	a	k8xC	a
Dagmar	Dagmar	k1gFnSc1	Dagmar
Bláhovou	Bláhová	k1gFnSc7	Bláhová
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
v	v	k7c6	v
Divadle	divadlo	k1gNnSc6	divadlo
Husa	husa	k1gFnSc1	husa
na	na	k7c6	na
provázku	provázek	k1gInSc6	provázek
výrazně	výrazně	k6eAd1	výrazně
prosazuje	prosazovat	k5eAaImIp3nS	prosazovat
režisér	režisér	k1gMnSc1	režisér
Vladimír	Vladimír	k1gMnSc1	Vladimír
Morávek	Morávek	k1gMnSc1	Morávek
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
režie	režie	k1gFnSc1	režie
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
výrazně	výrazně	k6eAd1	výrazně
snovou	snový	k2eAgFnSc7d1	snová
až	až	k8xS	až
bizarní	bizarní	k2eAgFnSc7d1	bizarní
obrazností	obraznost	k1gFnSc7	obraznost
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
až	až	k9	až
s	s	k7c7	s
prvky	prvek	k1gInPc7	prvek
hororu	horor	k1gInSc2	horor
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hereckých	herecký	k2eAgFnPc2d1	herecká
osobností	osobnost	k1gFnPc2	osobnost
působí	působit	k5eAaImIp3nS	působit
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
například	například	k6eAd1	například
Ivana	Ivana	k1gFnSc1	Ivana
Hloužková	Hloužkový	k2eAgFnSc1d1	Hloužková
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Zatloukal	Zatloukal	k1gMnSc1	Zatloukal
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Hauser	Hauser	k1gMnSc1	Hauser
nebo	nebo	k8xC	nebo
Jan	Jan	k1gMnSc1	Jan
Kolařík	Kolařík	k1gMnSc1	Kolařík
<g/>
.	.	kIx.	.
</s>
<s>
Divadlo	divadlo	k1gNnSc1	divadlo
Husa	Hus	k1gMnSc2	Hus
na	na	k7c6	na
provázku	provázek	k1gInSc6	provázek
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
veřejně	veřejně	k6eAd1	veřejně
představilo	představit	k5eAaPmAgNnS	představit
divákům	divák	k1gMnPc3	divák
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
dnech	den	k1gInPc6	den
byly	být	k5eAaImAgFnP	být
uvedeny	uveden	k2eAgFnPc1d1	uvedena
inscenace	inscenace	k1gFnPc1	inscenace
zakládajících	zakládající	k2eAgMnPc2d1	zakládající
režisérů	režisér	k1gMnPc2	režisér
<g/>
.	.	kIx.	.
</s>
<s>
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Pospíšil	Pospíšil	k1gMnSc1	Pospíšil
představil	představit	k5eAaPmAgMnS	představit
montáž	montáž	k1gFnSc4	montáž
Panta	Pant	k1gInSc2	Pant
Rei	Rea	k1gFnSc3	Rea
aneb	aneb	k?	aneb
Dějiny	dějiny	k1gFnPc4	dějiny
národu	národ	k1gInSc2	národ
českého	český	k2eAgInSc2d1	český
v	v	k7c6	v
kostce	kostka	k1gFnSc6	kostka
<g/>
,	,	kIx,	,
Eva	Eva	k1gFnSc1	Eva
Tálská	Tálský	k2eAgFnSc1d1	Tálský
Šibeniční	šibeniční	k2eAgFnPc4d1	šibeniční
písně	píseň	k1gFnPc4	píseň
a	a	k8xC	a
Peter	Peter	k1gMnSc1	Peter
Scherhaufer	Scherhaufer	k1gInSc4	Scherhaufer
Umění	umění	k1gNnSc2	umění
platiti	platit	k5eAaImF	platit
své	svůj	k3xOyFgInPc4	svůj
dluhy	dluh	k1gInPc4	dluh
a	a	k8xC	a
uspokojovati	uspokojovat	k5eAaImF	uspokojovat
věřitele	věřitel	k1gMnPc4	věřitel
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
třeba	třeba	k6eAd1	třeba
vyjmouti	vyjmout	k5eAaPmF	vyjmout
jediný	jediný	k2eAgInSc4d1	jediný
haléř	haléř	k1gInSc4	haléř
z	z	k7c2	z
vlastní	vlastní	k2eAgFnSc2d1	vlastní
kapsy	kapsa	k1gFnSc2	kapsa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
divadlo	divadlo	k1gNnSc1	divadlo
poprvé	poprvé	k6eAd1	poprvé
vystoupilo	vystoupit	k5eAaPmAgNnS	vystoupit
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
s	s	k7c7	s
inscenacemi	inscenace	k1gFnPc7	inscenace
Chceme	chtít	k5eAaImIp1nP	chtít
žít	žít	k5eAaImF	žít
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Tálská	Tálská	k1gFnSc1	Tálská
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Strašidýlka	strašidýlko	k1gNnPc1	strašidýlko
(	(	kIx(	(
<g/>
1972	[number]	k4	1972
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Polívka	polívka	k1gFnSc1	polívka
<g/>
)	)	kIx)	)
a	a	k8xC	a
Theatrum	Theatrum	k1gNnSc1	Theatrum
anatomicum	anatomicum	k1gNnSc1	anatomicum
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Scherhaufer	Scherhaufer	k1gInSc1	Scherhaufer
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Průběžně	průběžně	k6eAd1	průběžně
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
řada	řada	k1gFnSc1	řada
legendárních	legendární	k2eAgFnPc2d1	legendární
inscenací	inscenace	k1gFnPc2	inscenace
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
divadlo	divadlo	k1gNnSc1	divadlo
proslavily	proslavit	k5eAaPmAgInP	proslavit
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
i	i	k8xC	i
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
například	například	k6eAd1	například
o	o	k7c4	o
inscenace	inscenace	k1gFnPc4	inscenace
Commedia	Commedium	k1gNnSc2	Commedium
dell	dell	k1gInSc1	dell
<g/>
'	'	kIx"	'
<g/>
arte	arte	k1gInSc1	arte
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Scherhaufer	Scherhaufer	k1gInSc1	Scherhaufer
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Svatba	svatba	k1gFnSc1	svatba
(	(	kIx(	(
<g/>
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Scherhaufer	Scherhaufer	k1gInSc1	Scherhaufer
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Alenka	Alenka	k1gFnSc1	Alenka
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
divů	div	k1gInPc2	div
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Tálská	Tálská	k1gFnSc1	Tálská
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
klaunérie	klaunérie	k1gFnSc1	klaunérie
Bolka	Bolek	k1gMnSc2	Bolek
Polívky	Polívka	k1gMnSc2	Polívka
Am	Am	k1gMnSc2	Am
a	a	k8xC	a
Ea	Ea	k1gMnSc2	Ea
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Polívka	polívka	k1gFnSc1	polívka
<g/>
)	)	kIx)	)
a	a	k8xC	a
Pépe	Pépe	k1gFnSc1	Pépe
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Polívka	polívka	k1gFnSc1	polívka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
slavná	slavný	k2eAgFnSc1d1	slavná
inscenace	inscenace	k1gFnSc1	inscenace
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
tzv.	tzv.	kA	tzv.
malých	malý	k2eAgInPc2d1	malý
muzikálů	muzikál	k1gInPc2	muzikál
Balada	balada	k1gFnSc1	balada
pro	pro	k7c4	pro
banditu	bandita	k1gMnSc4	bandita
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Pospíšil	Pospíšil	k1gMnSc1	Pospíšil
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgFnPc4	který
se	se	k3xPyFc4	se
tajně	tajně	k6eAd1	tajně
podílel	podílet	k5eAaImAgMnS	podílet
Milan	Milan	k1gMnSc1	Milan
Uhde	Uhd	k1gInSc2	Uhd
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
premiéra	premiéra	k1gFnSc1	premiéra
známé	známý	k2eAgFnSc2d1	známá
Polívkovy	Polívkův	k2eAgFnSc2d1	Polívkova
autorské	autorský	k2eAgFnSc2d1	autorská
inscenace	inscenace	k1gFnSc2	inscenace
Šašek	Šašek	k1gMnSc1	Šašek
a	a	k8xC	a
královna	královna	k1gFnSc1	královna
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Polívka	polívka	k1gFnSc1	polívka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
představila	představit	k5eAaPmAgFnS	představit
Chantal	Chantal	k1gMnSc1	Chantal
Poullain	Poullain	k2eAgInSc1d1	Poullain
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
značných	značný	k2eAgInPc6d1	značný
cenzurních	cenzurní	k2eAgInPc6d1	cenzurní
problémech	problém	k1gInPc6	problém
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
uvést	uvést	k5eAaPmF	uvést
hru	hra	k1gFnSc4	hra
Ludvíka	Ludvík	k1gMnSc2	Ludvík
Kundery	Kundera	k1gFnSc2	Kundera
Labyrint	labyrint	k1gInSc1	labyrint
světa	svět	k1gInSc2	svět
a	a	k8xC	a
Lusthauz	lusthauz	k1gInSc1	lusthauz
srdce	srdce	k1gNnSc2	srdce
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Scherhaufer	Scherhaufer	k1gInSc1	Scherhaufer
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pozornost	pozornost	k1gFnSc4	pozornost
vzbudily	vzbudit	k5eAaPmAgInP	vzbudit
také	také	k9	také
představení	představení	k1gNnSc1	představení
akčního	akční	k2eAgNnSc2d1	akční
dětského	dětský	k2eAgNnSc2d1	dětské
divadla	divadlo	k1gNnSc2	divadlo
Svolávám	svolávat	k5eAaImIp1nS	svolávat
všechny	všechen	k3xTgMnPc4	všechen
skřítky	skřítek	k1gMnPc4	skřítek
<g/>
!	!	kIx.	!
</s>
<s>
Královna	královna	k1gFnSc1	královna
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
r.	r.	kA	r.
Tálská	Tálská	k1gFnSc1	Tálská
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgInPc6	který
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
úplnému	úplný	k2eAgNnSc3d1	úplné
smazání	smazání	k1gNnSc3	smazání
hranice	hranice	k1gFnSc2	hranice
mezi	mezi	k7c7	mezi
diváky	divák	k1gMnPc7	divák
a	a	k8xC	a
herci	herec	k1gMnPc7	herec
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byli	být	k5eAaImAgMnP	být
do	do	k7c2	do
akce	akce	k1gFnSc2	akce
zapojeni	zapojit	k5eAaPmNgMnP	zapojit
všichni	všechen	k3xTgMnPc1	všechen
dětští	dětský	k2eAgMnPc1d1	dětský
diváci	divák	k1gMnPc1	divák
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
významné	významný	k2eAgFnPc4d1	významná
inscenace	inscenace	k1gFnPc4	inscenace
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
Božská	božský	k2eAgFnSc1d1	božská
komedie	komedie	k1gFnSc1	komedie
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Tálská	Tálská	k1gFnSc1	Tálská
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Balet	balet	k1gInSc1	balet
Makábr	Makábr	k1gInSc1	Makábr
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
Scherhaufer	Scherhaufer	k1gInSc1	Scherhaufer
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Prodaný	prodaný	k2eAgInSc1d1	prodaný
a	a	k8xC	a
prodaná	prodaný	k2eAgFnSc1d1	prodaná
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Scherhaufer	Scherhaufer	k1gInSc1	Scherhaufer
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
třináctihodinová	třináctihodinový	k2eAgFnSc1d1	třináctihodinová
kulturní	kulturní	k2eAgFnSc1d1	kulturní
akce	akce	k1gFnSc1	akce
Všichni	všechen	k3xTgMnPc1	všechen
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
provázek	provázek	k1gInSc4	provázek
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
první	první	k4xOgNnSc4	první
číslo	číslo	k1gNnSc4	číslo
scénického	scénický	k2eAgInSc2d1	scénický
časopisu	časopis	k1gInSc2	časopis
Rozrazil	rozrazit	k5eAaPmAgInS	rozrazit
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
88	[number]	k4	88
(	(	kIx(	(
<g/>
O	o	k7c6	o
demokracii	demokracie	k1gFnSc6	demokracie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvních	první	k4xOgInPc6	první
porevolučních	porevoluční	k2eAgInPc6d1	porevoluční
letech	let	k1gInPc6	let
je	být	k5eAaImIp3nS	být
uvedena	uveden	k2eAgFnSc1d1	uvedena
Havlova	Havlův	k2eAgFnSc1d1	Havlova
hra	hra	k1gFnSc1	hra
Zahradní	zahradní	k2eAgFnSc1d1	zahradní
slavnost	slavnost	k1gFnSc1	slavnost
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Scherhaufer	Scherhaufer	k1gInSc1	Scherhaufer
<g/>
)	)	kIx)	)
a	a	k8xC	a
inscenace	inscenace	k1gFnSc1	inscenace
Katynka	Katynka	k1gFnSc1	Katynka
z	z	k7c2	z
Heilbronnu	Heilbronn	k1gInSc2	Heilbronn
neboli	neboli	k8xC	neboli
Zkouška	zkouška	k1gFnSc1	zkouška
ohněm	oheň	k1gInSc7	oheň
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Tálská	Tálská	k1gFnSc1	Tálská
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vyzvání	vyzvání	k1gNnSc6	vyzvání
výstavy	výstava	k1gFnSc2	výstava
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
,	,	kIx,	,
Evropa	Evropa	k1gFnSc1	Evropa
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
nastudována	nastudován	k2eAgFnSc1d1	nastudována
scénická	scénický	k2eAgFnSc1d1	scénická
kompozice	kompozice	k1gFnSc1	kompozice
Omyly	omyl	k1gInPc4	omyl
slečny	slečna	k1gFnSc2	slečna
smrti	smrt	k1gFnSc2	smrt
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
Scherhaufer	Scherhaufer	k1gInSc1	Scherhaufer
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
složena	složit	k5eAaPmNgFnS	složit
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
samostatných	samostatný	k2eAgFnPc2d1	samostatná
částí	část	k1gFnPc2	část
-	-	kIx~	-
Vyvolávači	vyvolávač	k1gMnPc7	vyvolávač
<g/>
,	,	kIx,	,
Justýna	Justýna	k1gFnSc1	Justýna
a	a	k8xC	a
Chyba	chyba	k1gFnSc1	chyba
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
divadlo	divadlo	k1gNnSc1	divadlo
realizovalo	realizovat	k5eAaBmAgNnS	realizovat
projekt	projekt	k1gInSc4	projekt
4	[number]	k4	4
kroky	krok	k1gInPc7	krok
stranou	stranou	k6eAd1	stranou
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Scherhaufer	Scherhaufer	k1gInSc1	Scherhaufer
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
kterého	který	k3yRgMnSc4	který
byly	být	k5eAaImAgInP	být
nastudovány	nastudován	k2eAgInPc1d1	nastudován
čtyři	čtyři	k4xCgInPc1	čtyři
dosud	dosud	k6eAd1	dosud
neinscenované	inscenovaný	k2eNgInPc1d1	inscenovaný
texty	text	k1gInPc1	text
<g/>
:	:	kIx,	:
Třesk	třesk	k1gInSc1	třesk
v	v	k7c6	v
podniku	podnik	k1gInSc6	podnik
B.Ů.H.	B.Ů.H.	k1gFnSc2	B.Ů.H.
<g/>
,	,	kIx,	,
Kašparova	Kašparův	k2eAgFnSc1d1	Kašparova
kráva	kráva	k1gFnSc1	kráva
<g/>
?	?	kIx.	?
</s>
<s>
Dámská	dámský	k2eAgFnSc1d1	dámská
jízda	jízda	k1gFnSc1	jízda
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
aneb	aneb	k?	aneb
Pohřbívání	pohřbívání	k1gNnSc4	pohřbívání
a	a	k8xC	a
Autobus	autobus	k1gInSc4	autobus
na	na	k7c6	na
lince	linka	k1gFnSc6	linka
21	[number]	k4	21
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
byla	být	k5eAaImAgNnP	být
uvedena	uvést	k5eAaPmNgNnP	uvést
dosud	dosud	k6eAd1	dosud
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
divácky	divácky	k6eAd1	divácky
nejúspěšnějších	úspěšný	k2eAgFnPc2d3	nejúspěšnější
inscenací	inscenace	k1gFnPc2	inscenace
Babička	babička	k1gFnSc1	babička
-	-	kIx~	-
fetišistická	fetišistický	k2eAgFnSc1d1	fetišistická
revue	revue	k1gFnSc1	revue
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Krobot	Krobot	k?	Krobot
<g/>
)	)	kIx)	)
s	s	k7c7	s
Jiřím	Jiří	k1gMnSc7	Jiří
Pechou	Pecha	k1gMnSc7	Pecha
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
například	například	k6eAd1	například
inscenace	inscenace	k1gFnSc2	inscenace
Komedie	komedie	k1gFnSc2	komedie
o	o	k7c4	o
umučení	umučení	k1gNnSc4	umučení
a	a	k8xC	a
vzkříšení	vzkříšení	k1gNnSc4	vzkříšení
našeho	náš	k3xOp1gMnSc2	náš
Pána	pán	k1gMnSc2	pán
a	a	k8xC	a
Spasitele	spasitel	k1gMnSc2	spasitel
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Tálská	Tálská	k1gFnSc1	Tálská
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rozum	rozum	k1gInSc1	rozum
do	do	k7c2	do
hrsti	hrst	k1gFnSc2	hrst
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Tálská	Tálská	k1gFnSc1	Tálská
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Baletky	baletek	k1gInPc1	baletek
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Šimák	Šimák	k1gMnSc1	Šimák
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Raskolnikov	Raskolnikov	k1gInSc1	Raskolnikov
-	-	kIx~	-
jeho	jeho	k3xOp3gInSc1	jeho
zločin	zločin	k1gInSc1	zločin
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
trest	trest	k1gInSc1	trest
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Morávek	Morávek	k1gMnSc1	Morávek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Hvězdy	hvězda	k1gFnPc1	hvězda
nad	nad	k7c4	nad
Baltimore	Baltimore	k1gInSc4	Baltimore
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Pitinský	Pitinský	k2eAgInSc1d1	Pitinský
<g/>
)	)	kIx)	)
o	o	k7c4	o
Vlastu	Vlasta	k1gFnSc4	Vlasta
Burianovi	Burian	k1gMnSc3	Burian
nebo	nebo	k8xC	nebo
Běsy	běs	k1gInPc4	běs
-	-	kIx~	-
Stavrogin	Stavrogin	k1gMnSc1	Stavrogin
je	být	k5eAaImIp3nS	být
ďábel	ďábel	k1gMnSc1	ďábel
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Morávek	Morávek	k1gMnSc1	Morávek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
zlomové	zlomový	k2eAgFnSc6d1	zlomová
sezóně	sezóna	k1gFnSc6	sezóna
2005	[number]	k4	2005
<g/>
/	/	kIx~	/
<g/>
2006	[number]	k4	2006
byly	být	k5eAaImAgFnP	být
znovuuvedeny	znovuuveden	k2eAgFnPc1d1	znovuuveden
inscenace	inscenace	k1gFnPc1	inscenace
Zahradní	zahradní	k2eAgFnSc4d1	zahradní
slavnost	slavnost	k1gFnSc4	slavnost
<g/>
,	,	kIx,	,
Rovzpomínání	Rovzpomínání	k1gNnSc4	Rovzpomínání
a	a	k8xC	a
nového	nový	k2eAgNnSc2d1	nové
nastudování	nastudování	k1gNnSc2	nastudování
se	se	k3xPyFc4	se
dočkala	dočkat	k5eAaPmAgFnS	dočkat
i	i	k9	i
Balada	balada	k1gFnSc1	balada
pro	pro	k7c4	pro
banditu	bandita	k1gMnSc4	bandita
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Morávek	Morávek	k1gMnSc1	Morávek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
závěrečná	závěrečný	k2eAgFnSc1d1	závěrečná
inscenace	inscenace	k1gFnSc1	inscenace
probíhajícího	probíhající	k2eAgInSc2d1	probíhající
projektu	projekt	k1gInSc2	projekt
Sto	sto	k4xCgNnSc4	sto
roků	rok	k1gInPc2	rok
kobry	kobra	k1gFnSc2	kobra
byla	být	k5eAaImAgFnS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
inscenace	inscenace	k1gFnSc1	inscenace
Bratři	bratr	k1gMnPc1	bratr
Karamazovi	Karamazův	k2eAgMnPc1d1	Karamazův
<g/>
:	:	kIx,	:
Vkříšení	Vkříšený	k2eAgMnPc1d1	Vkříšený
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Morávek	Morávek	k1gMnSc1	Morávek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2006	[number]	k4	2006
měly	mít	k5eAaImAgFnP	mít
premiéru	premiéra	k1gFnSc4	premiéra
dva	dva	k4xCgInPc4	dva
projekty	projekt	k1gInPc1	projekt
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
navazovaly	navazovat	k5eAaImAgFnP	navazovat
na	na	k7c6	na
tradici	tradice	k1gFnSc6	tradice
inscenování	inscenování	k1gNnSc2	inscenování
v	v	k7c6	v
nepravidelném	pravidelný	k2eNgInSc6d1	nepravidelný
prostoru	prostor	k1gInSc6	prostor
Všechny	všechen	k3xTgFnPc1	všechen
Shakespearovy	Shakespearův	k2eAgFnPc1d1	Shakespearova
ženy	žena	k1gFnPc1	žena
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Plachý	Plachý	k1gMnSc1	Plachý
<g/>
)	)	kIx)	)
a	a	k8xC	a
Kabaret	kabaret	k1gInSc1	kabaret
GAGA	GAGA	kA	GAGA
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Jelínek	Jelínek	k1gMnSc1	Jelínek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
letech	léto	k1gNnPc6	léto
byly	být	k5eAaImAgFnP	být
uvedeny	uvést	k5eAaPmNgFnP	uvést
například	například	k6eAd1	například
inscenace	inscenace	k1gFnSc1	inscenace
Cirkus	cirkus	k1gInSc1	cirkus
Havel	Havel	k1gMnSc1	Havel
aneb	aneb	k?	aneb
My	my	k3xPp1nPc1	my
všichni	všechen	k3xTgMnPc1	všechen
jsme	být	k5eAaImIp1nP	být
Láďa	Láďa	k1gMnSc1	Láďa
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Morávek	Morávek	k1gMnSc1	Morávek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Cesta	cesta	k1gFnSc1	cesta
za	za	k7c7	za
Rusalkou	rusalka	k1gFnSc7	rusalka
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Forman	Forman	k1gMnSc1	Forman
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Hamlet	Hamlet	k1gMnSc1	Hamlet
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Mikulášek	Mikulášek	k1gMnSc1	Mikulášek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
významné	významný	k2eAgFnPc4d1	významná
inscenace	inscenace	k1gFnPc4	inscenace
posledních	poslední	k2eAgNnPc2d1	poslední
let	léto	k1gNnPc2	léto
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
mimořádný	mimořádný	k2eAgInSc4d1	mimořádný
mnohahodinový	mnohahodinový	k2eAgInSc4d1	mnohahodinový
projekt	projekt	k1gInSc4	projekt
Kde	kde	k6eAd1	kde
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
můj	můj	k1gMnSc1	můj
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Leoš	Leoš	k1gMnSc1	Leoš
aneb	aneb	k?	aneb
Tvá	tvůj	k3xOp2gFnSc1	tvůj
nejvěrnější	věrný	k2eAgFnSc1d3	nejvěrnější
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Morávek	Morávek	k1gMnSc1	Morávek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
Tichý	Tichý	k1gMnSc1	Tichý
Tarzan	Tarzan	k1gMnSc1	Tarzan
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Petrželková	Petrželková	k1gFnSc1	Petrželková
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Doktor	doktor	k1gMnSc1	doktor
Faustus	Faustus	k1gMnSc1	Faustus
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Mikulášek	Mikulášek	k1gMnSc1	Mikulášek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Lásky	láska	k1gFnPc1	láska
jedné	jeden	k4xCgFnSc2	jeden
plavovlásky	plavovláska	k1gFnSc2	plavovláska
-	-	kIx~	-
Plavovlásky	plavovláska	k1gFnSc2	plavovláska
jedné	jeden	k4xCgFnSc2	jeden
lásky	láska	k1gFnSc2	láska
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Morávek	Morávek	k1gMnSc1	Morávek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ze	z	k7c2	z
života	život	k1gInSc2	život
hmyzu	hmyz	k1gInSc2	hmyz
-	-	kIx~	-
Oh	oh	k0	oh
<g/>
!	!	kIx.	!
</s>
<s>
Jaká	jaký	k3yQgFnSc1	jaký
podívaná	podívaná	k1gFnSc1	podívaná
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Morávek	Morávek	k1gMnSc1	Morávek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Amadeus	Amadeus	k1gMnSc1	Amadeus
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
milovaný	milovaný	k2eAgInSc1d1	milovaný
Bohem	bůh	k1gMnSc7	bůh
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Morávek	Morávek	k1gMnSc1	Morávek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Myši	myš	k1gFnSc2	myš
Natalie	Natalie	k1gFnSc2	Natalie
Mooshabrové	Mooshabrová	k1gFnSc2	Mooshabrová
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Pitinský	Pitinský	k2eAgInSc1d1	Pitinský
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Mein	Mein	k1gMnSc1	Mein
Švejk	Švejk	k1gMnSc1	Švejk
(	(	kIx(	(
<g/>
Operetta	Operetta	k1gMnSc1	Operetta
<g/>
.	.	kIx.	.
</s>
<s>
Komedie	komedie	k1gFnSc1	komedie
<g/>
.	.	kIx.	.
</s>
<s>
Smrt	smrt	k1gFnSc1	smrt
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Pitinský	Pitinský	k2eAgInSc1d1	Pitinský
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
