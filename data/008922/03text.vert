<p>
<s>
Bruce	Bruko	k6eAd1	Bruko
Lee	Lea	k1gFnSc3	Lea
nebo	nebo	k8xC	nebo
také	také	k9	také
Lee	Lea	k1gFnSc6	Lea
Jun	jun	k1gMnSc1	jun
Fan	Fana	k1gFnPc2	Fana
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1940	[number]	k4	1940
San	San	k1gMnSc1	San
Francisco	Francisco	k1gMnSc1	Francisco
–	–	k?	–
20	[number]	k4	20
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1973	[number]	k4	1973
Hong	Hong	k1gInSc1	Hong
Kong	Kongo	k1gNnPc2	Kongo
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
mistr	mistr	k1gMnSc1	mistr
a	a	k8xC	a
popularizátor	popularizátor	k1gMnSc1	popularizátor
bojových	bojový	k2eAgFnPc2d1	bojová
umění	umění	k1gNnPc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Věnoval	věnovat	k5eAaPmAgInS	věnovat
se	se	k3xPyFc4	se
tréninku	trénink	k1gInSc2	trénink
bojových	bojový	k2eAgNnPc2d1	bojové
umění	umění	k1gNnPc2	umění
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ho	on	k3xPp3gNnSc4	on
později	pozdě	k6eAd2	pozdě
dovedlo	dovést	k5eAaPmAgNnS	dovést
na	na	k7c4	na
filmové	filmový	k2eAgNnSc4d1	filmové
plátno	plátno	k1gNnSc4	plátno
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc7	svůj
hereckou	herecký	k2eAgFnSc7d1	herecká
prací	práce	k1gFnSc7	práce
ovlivnil	ovlivnit	k5eAaPmAgMnS	ovlivnit
natáčení	natáčení	k1gNnSc4	natáčení
a	a	k8xC	a
popularitu	popularita	k1gFnSc4	popularita
soudobých	soudobý	k2eAgInPc2d1	soudobý
akčních	akční	k2eAgInPc2d1	akční
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
své	svůj	k3xOyFgFnSc6	svůj
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
legendou	legenda	k1gFnSc7	legenda
a	a	k8xC	a
dodnes	dodnes	k6eAd1	dodnes
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
jako	jako	k8xS	jako
nejvlivnější	vlivný	k2eAgFnSc1d3	nejvlivnější
osobnost	osobnost	k1gFnSc1	osobnost
bojových	bojový	k2eAgNnPc2d1	bojové
umění	umění	k1gNnPc2	umění
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Mládí	mládí	k1gNnSc1	mládí
===	===	k?	===
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
jako	jako	k8xC	jako
čtvrté	čtvrtý	k4xOgFnPc1	čtvrtý
z	z	k7c2	z
pěti	pět	k4xCc2	pět
dětí	dítě	k1gFnPc2	dítě
do	do	k7c2	do
rodiny	rodina	k1gFnSc2	rodina
Lee	Lea	k1gFnSc3	Lea
Hoi	Hoi	k1gFnSc3	Hoi
Chuen	Chuno	k1gNnPc2	Chuno
a	a	k8xC	a
Grace	Grace	k1gFnSc1	Grace
Ho	on	k3xPp3gMnSc4	on
v	v	k7c6	v
San	San	k1gFnSc6	San
Francisku	Francisek	k1gInSc2	Francisek
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
narození	narození	k1gNnSc6	narození
se	se	k3xPyFc4	se
celá	celý	k2eAgFnSc1d1	celá
rodina	rodina	k1gFnSc1	rodina
vrátila	vrátit	k5eAaPmAgFnS	vrátit
do	do	k7c2	do
Hong	Hong	k1gInSc4	Hong
Kongu	Kongo	k1gNnSc3	Kongo
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
dvě	dva	k4xCgFnPc4	dva
starší	starý	k2eAgFnPc4d2	starší
sestry	sestra	k1gFnPc4	sestra
Phoebe	Phoeb	k1gInSc5	Phoeb
a	a	k8xC	a
Agnes	Agnesa	k1gFnPc2	Agnesa
<g/>
,	,	kIx,	,
staršího	starý	k2eAgMnSc2d2	starší
bratra	bratr	k1gMnSc2	bratr
Petera	Peter	k1gMnSc2	Peter
a	a	k8xC	a
mladšího	mladý	k2eAgMnSc2d2	mladší
Roberta	Robert	k1gMnSc2	Robert
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
vzpomínek	vzpomínka	k1gFnPc2	vzpomínka
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
si	se	k3xPyFc3	se
uchoval	uchovat	k5eAaPmAgMnS	uchovat
z	z	k7c2	z
dětství	dětství	k1gNnSc2	dětství
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
japonská	japonský	k2eAgFnSc1d1	japonská
okupace	okupace	k1gFnSc1	okupace
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
byl	být	k5eAaImAgInS	být
Hong	Hong	k1gInSc1	Hong
Kong	Kongo	k1gNnPc2	Kongo
britskou	britský	k2eAgFnSc7d1	britská
kolonií	kolonie	k1gFnSc7	kolonie
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
s	s	k7c7	s
vlivem	vliv	k1gInSc7	vliv
cizí	cizí	k2eAgFnSc2d1	cizí
mocnosti	mocnost	k1gFnSc2	mocnost
se	se	k3xPyFc4	se
setkával	setkávat	k5eAaImAgMnS	setkávat
od	od	k7c2	od
svého	svůj	k3xOyFgNnSc2	svůj
mládí	mládí	k1gNnSc2	mládí
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
13	[number]	k4	13
letech	léto	k1gNnPc6	léto
začal	začít	k5eAaPmAgMnS	začít
s	s	k7c7	s
tréninkem	trénink	k1gInSc7	trénink
Wing	Wing	k1gMnSc1	Wing
Chun	Chun	k1gMnSc1	Chun
<g/>
.	.	kIx.	.
</s>
<s>
Nevěnoval	věnovat	k5eNaImAgInS	věnovat
se	se	k3xPyFc4	se
však	však	k9	však
jenom	jenom	k9	jenom
boji	boj	k1gInSc3	boj
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
hongkongskou	hongkongský	k2eAgFnSc4d1	hongkongská
taneční	taneční	k2eAgFnSc4d1	taneční
soutěž	soutěž	k1gFnSc4	soutěž
v	v	k7c6	v
ča-ča	ča-ča	k1gNnSc6	ča-ča
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
díky	díky	k7c3	díky
svému	svůj	k3xOyFgMnSc3	svůj
otci	otec	k1gMnSc3	otec
a	a	k8xC	a
vrozenému	vrozený	k2eAgInSc3d1	vrozený
talentu	talent	k1gInSc3	talent
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
několika	několik	k4yIc6	několik
filmech	film	k1gInPc6	film
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
se	se	k3xPyFc4	se
právem	právem	k6eAd1	právem
obával	obávat	k5eAaImAgMnS	obávat
o	o	k7c4	o
budoucnost	budoucnost	k1gFnSc4	budoucnost
svého	svůj	k3xOyFgMnSc2	svůj
syna	syn	k1gMnSc2	syn
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
dával	dávat	k5eAaImAgMnS	dávat
přednost	přednost	k1gFnSc4	přednost
boji	boj	k1gInSc3	boj
<g/>
,	,	kIx,	,
tanci	tanec	k1gInSc3	tanec
a	a	k8xC	a
filmu	film	k1gInSc3	film
před	před	k7c7	před
studiem	studio	k1gNnSc7	studio
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
Bruce	Bruce	k1gMnSc1	Bruce
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
potyčky	potyčka	k1gFnSc2	potyčka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
přilákala	přilákat	k5eAaPmAgFnS	přilákat
pozornost	pozornost	k1gFnSc4	pozornost
policie	policie	k1gFnSc2	policie
<g/>
,	,	kIx,	,
rodiče	rodič	k1gMnPc1	rodič
se	se	k3xPyFc4	se
shodli	shodnout	k5eAaPmAgMnP	shodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
čase	čas	k1gInSc6	čas
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
uplatnil	uplatnit	k5eAaPmAgMnS	uplatnit
svoji	svůj	k3xOyFgFnSc4	svůj
americkou	americký	k2eAgFnSc4d1	americká
národnost	národnost	k1gFnSc4	národnost
a	a	k8xC	a
vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
do	do	k7c2	do
země	zem	k1gFnSc2	zem
svého	svůj	k3xOyFgNnSc2	svůj
narození	narození	k1gNnSc2	narození
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
===	===	k?	===
Učitel	učitel	k1gMnSc1	učitel
bojových	bojový	k2eAgNnPc2d1	bojové
umění	umění	k1gNnPc2	umění
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c4	po
nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
bydlel	bydlet	k5eAaImAgMnS	bydlet
u	u	k7c2	u
přítele	přítel	k1gMnSc2	přítel
otce	otec	k1gMnSc2	otec
v	v	k7c6	v
San	San	k1gFnSc6	San
Francisku	Francisek	k1gInSc2	Francisek
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
živil	živit	k5eAaImAgMnS	živit
jako	jako	k9	jako
taneční	taneční	k2eAgMnSc1d1	taneční
instruktor	instruktor	k1gMnSc1	instruktor
<g/>
.	.	kIx.	.
</s>
<s>
Nevydržel	vydržet	k5eNaPmAgMnS	vydržet
zde	zde	k6eAd1	zde
dlouho	dlouho	k6eAd1	dlouho
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
do	do	k7c2	do
Seattlu	Seattl	k1gInSc2	Seattl
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měla	mít	k5eAaImAgFnS	mít
Ruby	rub	k1gInPc4	rub
Chow	Chow	k1gFnPc2	Chow
svoji	svůj	k3xOyFgFnSc4	svůj
restauraci	restaurace	k1gFnSc4	restaurace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
mu	on	k3xPp3gMnSc3	on
díky	díky	k7c3	díky
jeho	jeho	k3xOp3gMnSc3	jeho
otci	otec	k1gMnSc3	otec
nabídla	nabídnout	k5eAaPmAgFnS	nabídnout
ubytování	ubytování	k1gNnSc3	ubytování
a	a	k8xC	a
práci	práce	k1gFnSc3	práce
<g/>
.	.	kIx.	.
</s>
<s>
Přihlásil	přihlásit	k5eAaPmAgMnS	přihlásit
se	se	k3xPyFc4	se
a	a	k8xC	a
pečlivě	pečlivě	k6eAd1	pečlivě
docházel	docházet	k5eAaImAgInS	docházet
do	do	k7c2	do
Edison	Edisona	k1gFnPc2	Edisona
Technical	Technical	k1gMnSc2	Technical
College	Colleg	k1gMnSc2	Colleg
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
studijní	studijní	k2eAgInPc1d1	studijní
výsledky	výsledek	k1gInPc1	výsledek
byly	být	k5eAaImAgInP	být
dost	dost	k6eAd1	dost
dobré	dobrý	k2eAgFnPc1d1	dobrá
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
na	na	k7c4	na
University	universita	k1gFnPc4	universita
of	of	k?	of
Washington	Washington	k1gInSc1	Washington
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
později	pozdě	k6eAd2	pozdě
získal	získat	k5eAaPmAgMnS	získat
titul	titul	k1gInSc4	titul
bakaláře	bakalář	k1gMnSc2	bakalář
svobodných	svobodný	k2eAgFnPc2d1	svobodná
umění	umění	k1gNnPc2	umění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Frustrován	frustrovat	k5eAaImNgInS	frustrovat
námezdními	námezdní	k2eAgFnPc7d1	námezdní
pracemi	práce	k1gFnPc7	práce
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
ho	on	k3xPp3gNnSc4	on
živily	živit	k5eAaImAgFnP	živit
<g/>
,	,	kIx,	,
založil	založit	k5eAaPmAgMnS	založit
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
Jun	jun	k1gMnSc1	jun
Fan	Fana	k1gFnPc2	Fana
Gung	Gung	k1gMnSc1	Gung
Fu	fu	k0	fu
Institute	institut	k1gInSc5	institut
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
za	za	k7c4	za
nevelký	velký	k2eNgInSc4d1	nevelký
poplatek	poplatek	k1gInSc4	poplatek
kung-fu	kung	k1gInSc2	kung-f
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
nebyly	být	k5eNaImAgFnP	být
školy	škola	k1gFnPc1	škola
bojových	bojový	k2eAgNnPc2d1	bojové
umění	umění	k1gNnPc2	umění
tolik	tolik	k6eAd1	tolik
rozšířené	rozšířený	k2eAgInPc1d1	rozšířený
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
neměl	mít	k5eNaImAgInS	mít
o	o	k7c4	o
studenty	student	k1gMnPc4	student
nouzi	nouze	k1gFnSc4	nouze
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
potkal	potkat	k5eAaPmAgMnS	potkat
Lindu	Linda	k1gFnSc4	Linda
Emery	Emera	k1gFnSc2	Emera
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yRgFnSc7	který
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
<g/>
.	.	kIx.	.
</s>
<s>
Svoji	svůj	k3xOyFgFnSc4	svůj
školu	škola	k1gFnSc4	škola
bojových	bojový	k2eAgNnPc2d1	bojové
umění	umění	k1gNnPc2	umění
nechal	nechat	k5eAaPmAgInS	nechat
na	na	k7c4	na
starost	starost	k1gFnSc4	starost
svému	svůj	k3xOyFgMnSc3	svůj
příteli	přítel	k1gMnSc3	přítel
Takymu	Takym	k1gInSc6	Takym
Kimurovi	Kimurův	k2eAgMnPc1d1	Kimurův
a	a	k8xC	a
se	s	k7c7	s
ženou	žena	k1gFnSc7	žena
se	se	k3xPyFc4	se
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
do	do	k7c2	do
Oaklandu	Oakland	k1gInSc2	Oakland
k	k	k7c3	k
Jamesovi	James	k1gMnSc3	James
Lee	Lea	k1gFnSc6	Lea
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
společně	společně	k6eAd1	společně
otevřeli	otevřít	k5eAaPmAgMnP	otevřít
druhou	druhý	k4xOgFnSc4	druhý
pobočku	pobočka	k1gFnSc4	pobočka
Jun	jun	k1gMnSc1	jun
Fan	Fana	k1gFnPc2	Fana
Gung	Gung	k1gMnSc1	Gung
Fu	fu	k0	fu
Institute	institut	k1gInSc5	institut
<g/>
.	.	kIx.	.
</s>
<s>
Bruce	Bruce	k1gMnSc1	Bruce
Lee	Lea	k1gFnSc6	Lea
poskytoval	poskytovat	k5eAaImAgInS	poskytovat
výuku	výuka	k1gFnSc4	výuka
komukoliv	kdokoliv	k3yInSc3	kdokoliv
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
nelíbilo	líbit	k5eNaImAgNnS	líbit
čínské	čínský	k2eAgFnSc3d1	čínská
menšině	menšina	k1gFnSc3	menšina
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
vyústilo	vyústit	k5eAaPmAgNnS	vyústit
v	v	k7c4	v
duel	duel	k1gInSc4	duel
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
měl	mít	k5eAaImAgInS	mít
ukončit	ukončit	k5eAaPmF	ukončit
vzdělávání	vzdělávání	k1gNnPc4	vzdělávání
nečínských	čínský	k2eNgMnPc2d1	čínský
studentů	student	k1gMnPc2	student
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
Lee	Lea	k1gFnSc3	Lea
prohrál	prohrát	k5eAaPmAgInS	prohrát
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
necelé	celý	k2eNgFnSc6d1	necelá
minutě	minuta	k1gFnSc6	minuta
boj	boj	k1gInSc1	boj
skončil	skončit	k5eAaPmAgInS	skončit
v	v	k7c4	v
Leeův	Leeův	k2eAgInSc4d1	Leeův
prospěch	prospěch	k1gInSc4	prospěch
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
však	však	k9	však
nebyl	být	k5eNaImAgMnS	být
s	s	k7c7	s
výsledkem	výsledek	k1gInSc7	výsledek
spokojen	spokojen	k2eAgInSc1d1	spokojen
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
očekával	očekávat	k5eAaImAgInS	očekávat
rychlejší	rychlý	k2eAgInSc4d2	rychlejší
konec	konec	k1gInSc4	konec
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
boj	boj	k1gInSc1	boj
ho	on	k3xPp3gMnSc4	on
přivedl	přivést	k5eAaPmAgInS	přivést
ke	k	k7c3	k
snaze	snaha	k1gFnSc3	snaha
o	o	k7c4	o
lepší	dobrý	k2eAgNnSc4d2	lepší
zvládnutí	zvládnutí	k1gNnSc4	zvládnutí
bojových	bojový	k2eAgNnPc2d1	bojové
umění	umění	k1gNnPc2	umění
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
po	po	k7c6	po
stránce	stránka	k1gFnSc6	stránka
fyzické	fyzický	k2eAgFnSc6d1	fyzická
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
po	po	k7c6	po
stránce	stránka	k1gFnSc6	stránka
filosofické	filosofický	k2eAgFnSc6d1	filosofická
a	a	k8xC	a
mentální	mentální	k2eAgFnSc6d1	mentální
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
vzniká	vznikat	k5eAaImIp3nS	vznikat
myšlenka	myšlenka	k1gFnSc1	myšlenka
Jeet	Jeeta	k1gFnPc2	Jeeta
Kune	Kun	k1gFnSc2	Kun
Do	do	k7c2	do
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pomalá	pomalý	k2eAgFnSc1d1	pomalá
cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
Hollywoodu	Hollywood	k1gInSc2	Hollywood
===	===	k?	===
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
Hollywoodu	Hollywood	k1gInSc2	Hollywood
začala	začít	k5eAaPmAgFnS	začít
2	[number]	k4	2
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1964	[number]	k4	1964
při	při	k7c6	při
jeho	jeho	k3xOp3gFnSc6	jeho
exhibici	exhibice	k1gFnSc6	exhibice
na	na	k7c6	na
mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
turnaji	turnaj	k1gInSc6	turnaj
v	v	k7c6	v
karate	karate	k1gNnSc6	karate
na	na	k7c4	na
Long	Long	k1gInSc4	Long
Beach	Beacha	k1gFnPc2	Beacha
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
jednopalcové	jednopalcový	k2eAgInPc1d1	jednopalcový
údery	úder	k1gInPc1	úder
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
úderem	úder	k1gInSc7	úder
pěstí	pěst	k1gFnPc2	pěst
ze	z	k7c2	z
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
jednoho	jeden	k4xCgInSc2	jeden
palce	palec	k1gInSc2	palec
povalil	povalit	k5eAaPmAgMnS	povalit
dobrovolníka	dobrovolník	k1gMnSc2	dobrovolník
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
rychlé	rychlý	k2eAgFnPc1d1	rychlá
kliky	klika	k1gFnPc1	klika
na	na	k7c6	na
dvou	dva	k4xCgInPc6	dva
prstech	prst	k1gInPc6	prst
nepřestávaly	přestávat	k5eNaImAgFnP	přestávat
ohromovat	ohromovat	k5eAaImF	ohromovat
publikum	publikum	k1gNnSc4	publikum
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
velkému	velký	k2eAgNnSc3d1	velké
štěstí	štěstí	k1gNnSc3	štěstí
sledoval	sledovat	k5eAaImAgMnS	sledovat
toto	tento	k3xDgNnSc4	tento
představení	představení	k1gNnSc4	představení
i	i	k9	i
kadeřník	kadeřník	k1gMnSc1	kadeřník
filmových	filmový	k2eAgFnPc2d1	filmová
hvězd	hvězda	k1gFnPc2	hvězda
Jay	Jay	k1gFnSc1	Jay
Sebring	Sebring	k1gInSc1	Sebring
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zapříčinil	zapříčinit	k5eAaPmAgInS	zapříčinit
setkání	setkání	k1gNnSc4	setkání
Bruce	Bruce	k1gMnSc2	Bruce
Leeho	Lee	k1gMnSc2	Lee
s	s	k7c7	s
filmovým	filmový	k2eAgMnSc7d1	filmový
producentem	producent	k1gMnSc7	producent
Williamem	William	k1gInSc7	William
Dozierem	Dozier	k1gInSc7	Dozier
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zkušebním	zkušební	k2eAgNnSc6d1	zkušební
natáčení	natáčení	k1gNnSc6	natáčení
se	se	k3xPyFc4	se
Dozier	Dozier	k1gMnSc1	Dozier
nadchl	nadchnout	k5eAaPmAgMnS	nadchnout
a	a	k8xC	a
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
Leemu	Leem	k1gMnSc3	Leem
roli	role	k1gFnSc4	role
Kata	kat	k1gMnSc2	kat
v	v	k7c6	v
chystaném	chystaný	k2eAgInSc6d1	chystaný
televizním	televizní	k2eAgInSc6d1	televizní
seriálu	seriál	k1gInSc6	seriál
"	"	kIx"	"
<g/>
The	The	k1gFnSc6	The
Green	Green	k2eAgMnSc1d1	Green
Hornet	Hornet	k1gMnSc1	Hornet
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Než	než	k8xS	než
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
mohlo	moct	k5eAaImAgNnS	moct
stát	stát	k5eAaPmF	stát
<g/>
,	,	kIx,	,
musel	muset	k5eAaImAgMnS	muset
Dozier	Dozier	k1gMnSc1	Dozier
dokončit	dokončit	k5eAaPmF	dokončit
seriál	seriál	k1gInSc4	seriál
Batman	Batman	k1gMnSc1	Batman
<g/>
,	,	kIx,	,
a	a	k8xC	a
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
Lee	Lea	k1gFnSc3	Lea
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
neupsal	upsat	k5eNaPmAgInS	upsat
někomu	někdo	k3yInSc3	někdo
jinému	jiný	k2eAgMnSc3d1	jiný
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
mu	on	k3xPp3gMnSc3	on
zaplaceno	zaplatit	k5eAaPmNgNnS	zaplatit
$	$	kIx~	$
<g/>
1800	[number]	k4	1800
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
změny	změna	k1gFnPc1	změna
nastaly	nastat	k5eAaPmAgFnP	nastat
i	i	k9	i
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
rodinném	rodinný	k2eAgInSc6d1	rodinný
životě	život	k1gInSc6	život
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1965	[number]	k4	1965
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Brandon	Brandon	k1gMnSc1	Brandon
Lee	Lea	k1gFnSc3	Lea
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgNnSc3	jenž
si	se	k3xPyFc3	se
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
vybudoval	vybudovat	k5eAaPmAgMnS	vybudovat
silné	silný	k2eAgNnSc4d1	silné
pouto	pouto	k1gNnSc4	pouto
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
nato	nato	k6eAd1	nato
dorazila	dorazit	k5eAaPmAgFnS	dorazit
zpráva	zpráva	k1gFnSc1	zpráva
z	z	k7c2	z
Hong	Honga	k1gFnPc2	Honga
Kongu	Kongo	k1gNnSc6	Kongo
<g/>
,	,	kIx,	,
že	že	k8xS	že
Brucův	Brucův	k2eAgMnSc1d1	Brucův
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
Lee	Lea	k1gFnSc3	Lea
Hoi	Hoi	k1gFnSc3	Hoi
Chuen	Chuno	k1gNnPc2	Chuno
<g/>
,	,	kIx,	,
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bruce	Bruce	k1gMnSc1	Bruce
se	se	k3xPyFc4	se
musel	muset	k5eAaImAgMnS	muset
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
bude	být	k5eAaImBp3nS	být
pokračovat	pokračovat	k5eAaImF	pokračovat
ve	v	k7c6	v
vyučování	vyučování	k1gNnSc6	vyučování
bojových	bojový	k2eAgNnPc2d1	bojové
umění	umění	k1gNnPc2	umění
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
naplno	naplno	k6eAd1	naplno
pustí	pustit	k5eAaPmIp3nS	pustit
do	do	k7c2	do
filmování	filmování	k1gNnSc2	filmování
<g/>
.	.	kIx.	.
</s>
<s>
Bál	bát	k5eAaImAgInS	bát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
otevření	otevření	k1gNnSc6	otevření
několika	několik	k4yIc2	několik
dalších	další	k2eAgFnPc2d1	další
škol	škola	k1gFnPc2	škola
by	by	kYmCp3nS	by
ztratil	ztratit	k5eAaPmAgInS	ztratit
přímou	přímý	k2eAgFnSc4d1	přímá
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
výukou	výuka	k1gFnSc7	výuka
a	a	k8xC	a
nechtěl	chtít	k5eNaImAgMnS	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
jeho	jeho	k3xOp3gFnSc1	jeho
vášeň	vášeň	k1gFnSc1	vášeň
stala	stát	k5eAaPmAgFnS	stát
pouze	pouze	k6eAd1	pouze
zdrojem	zdroj	k1gInSc7	zdroj
obživy	obživa	k1gFnSc2	obživa
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
hereckou	herecký	k2eAgFnSc4d1	herecká
kariéru	kariéra	k1gFnSc4	kariéra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
1966	[number]	k4	1966
a	a	k8xC	a
1967	[number]	k4	1967
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgMnS	účastnit
natáčení	natáčení	k1gNnSc4	natáčení
"	"	kIx"	"
<g/>
The	The	k1gFnSc7	The
Green	Green	k2eAgInSc4d1	Green
Hornet	Hornet	k1gInSc4	Hornet
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
nebyla	být	k5eNaImAgNnP	být
asijská	asijský	k2eAgNnPc1d1	asijské
bojová	bojový	k2eAgNnPc1d1	bojové
umění	umění	k1gNnPc1	umění
tolik	tolik	k6eAd1	tolik
známá	známý	k2eAgNnPc1d1	známé
a	a	k8xC	a
rozšířená	rozšířený	k2eAgNnPc1d1	rozšířené
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
bojovým	bojový	k2eAgFnPc3d1	bojová
scénám	scéna	k1gFnPc3	scéna
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
sám	sám	k3xTgInSc1	sám
navrhoval	navrhovat	k5eAaImAgMnS	navrhovat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
pro	pro	k7c4	pro
mnohé	mnohý	k2eAgFnPc4d1	mnohá
diváky	divák	k1gMnPc7	divák
hvězdou	hvězda	k1gFnSc7	hvězda
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
hrál	hrát	k5eAaImAgInS	hrát
vedlejší	vedlejší	k2eAgFnSc4d1	vedlejší
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Green	Green	k1gInSc1	Green
Hornet	Hornet	k1gInSc1	Hornet
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
přesto	přesto	k6eAd1	přesto
nestala	stát	k5eNaPmAgFnS	stát
úspěšnou	úspěšný	k2eAgFnSc7d1	úspěšná
sérií	série	k1gFnSc7	série
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
26	[number]	k4	26
<g/>
.	.	kIx.	.
díle	díl	k1gInSc6	díl
ukončena	ukončit	k5eAaPmNgFnS	ukončit
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
udržel	udržet	k5eAaPmAgInS	udržet
stálý	stálý	k2eAgInSc4d1	stálý
příjem	příjem	k1gInSc4	příjem
<g/>
,	,	kIx,	,
otevřel	otevřít	k5eAaPmAgMnS	otevřít
si	se	k3xPyFc3	se
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
třetí	třetí	k4xOgFnSc3	třetí
Jun	jun	k1gMnSc1	jun
Fan	Fana	k1gFnPc2	Fana
Gung	Gung	k1gMnSc1	Gung
Fu	fu	k0	fu
Institute	institut	k1gInSc5	institut
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
menších	malý	k2eAgFnPc6d2	menší
rolích	role	k1gFnPc6	role
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
seriálech	seriál	k1gInPc6	seriál
začal	začít	k5eAaPmAgMnS	začít
poskytovat	poskytovat	k5eAaImF	poskytovat
soukromé	soukromý	k2eAgInPc4d1	soukromý
tréninky	trénink	k1gInPc4	trénink
různým	různý	k2eAgFnPc3d1	různá
filmovým	filmový	k2eAgFnPc3d1	filmová
osobnostem	osobnost	k1gFnPc3	osobnost
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
požehnání	požehnání	k1gNnSc1	požehnání
přišlo	přijít	k5eAaPmAgNnS	přijít
19	[number]	k4	19
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1969	[number]	k4	1969
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
dcery	dcera	k1gFnSc2	dcera
(	(	kIx(	(
<g/>
Shannon	Shannon	k1gInSc1	Shannon
Emery	Emera	k1gFnSc2	Emera
Lee	Lea	k1gFnSc3	Lea
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
celovečerním	celovečerní	k2eAgInSc6d1	celovečerní
filmu	film	k1gInSc6	film
Marlowe	Marlow	k1gFnSc2	Marlow
v	v	k7c6	v
roli	role	k1gFnSc6	role
Winslowa	Winslowus	k1gMnSc2	Winslowus
Wonga	Wong	k1gMnSc2	Wong
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
ranní	ranní	k2eAgFnSc6d1	ranní
rozcvičce	rozcvička	k1gFnSc6	rozcvička
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
si	se	k3xPyFc3	se
poranil	poranit	k5eAaPmAgMnS	poranit
záda	záda	k1gNnPc4	záda
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
zjistilo	zjistit	k5eAaPmAgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
sakrální	sakrální	k2eAgInSc4d1	sakrální
nerv	nerv	k1gInSc4	nerv
<g/>
)	)	kIx)	)
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
mu	on	k3xPp3gMnSc3	on
nakázán	nakázán	k2eAgInSc4d1	nakázán
šestiměsíční	šestiměsíční	k2eAgInSc4d1	šestiměsíční
klid	klid	k1gInSc4	klid
na	na	k7c6	na
lůžku	lůžko	k1gNnSc6	lůžko
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
strávil	strávit	k5eAaPmAgInS	strávit
převážně	převážně	k6eAd1	převážně
čtením	čtení	k1gNnSc7	čtení
knih	kniha	k1gFnPc2	kniha
a	a	k8xC	a
zapisováním	zapisování	k1gNnSc7	zapisování
svých	svůj	k3xOyFgFnPc2	svůj
úvah	úvaha	k1gFnPc2	úvaha
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
získání	získání	k1gNnSc6	získání
síly	síla	k1gFnSc2	síla
začal	začít	k5eAaPmAgInS	začít
realizovat	realizovat	k5eAaBmF	realizovat
scénář	scénář	k1gInSc1	scénář
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
který	který	k3yIgInSc4	který
sám	sám	k3xTgMnSc1	sám
napsal	napsat	k5eAaPmAgMnS	napsat
příběh	příběh	k1gInSc4	příběh
<g/>
,	,	kIx,	,
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Silent	Silent	k1gMnSc1	Silent
Flute	Flut	k1gInSc5	Flut
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Warner	Warner	k1gMnSc1	Warner
Brothers	Brothersa	k1gFnPc2	Brothersa
projevili	projevit	k5eAaPmAgMnP	projevit
zájem	zájem	k1gInSc4	zájem
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
točit	točit	k5eAaImF	točit
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
krátkém	krátký	k2eAgNnSc6d1	krátké
hledání	hledání	k1gNnSc6	hledání
vhodného	vhodný	k2eAgNnSc2d1	vhodné
místa	místo	k1gNnSc2	místo
studio	studio	k1gNnSc1	studio
projekt	projekt	k1gInSc1	projekt
zrušilo	zrušit	k5eAaPmAgNnS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Hollywood	Hollywood	k1gInSc4	Hollywood
mu	on	k3xPp3gMnSc3	on
uštědřil	uštědřit	k5eAaPmAgMnS	uštědřit
další	další	k2eAgFnSc4d1	další
ránu	rána	k1gFnSc4	rána
<g/>
,	,	kIx,	,
když	když	k8xS	když
přijal	přijmout	k5eAaPmAgInS	přijmout
jeho	jeho	k3xOp3gInSc1	jeho
nápad	nápad	k1gInSc1	nápad
na	na	k7c4	na
seriál	seriál	k1gInSc4	seriál
z	z	k7c2	z
prostředí	prostředí	k1gNnSc2	prostředí
divokého	divoký	k2eAgInSc2d1	divoký
západu	západ	k1gInSc2	západ
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
putuje	putovat	k5eAaImIp3nS	putovat
orientální	orientální	k2eAgInSc1d1	orientální
mnich	mnich	k1gInSc1	mnich
<g/>
,	,	kIx,	,
a	a	k8xC	a
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
přisoudil	přisoudit	k5eAaPmAgMnS	přisoudit
Davidu	David	k1gMnSc3	David
Carradinemu	Carradinema	k1gFnSc4	Carradinema
<g/>
,	,	kIx,	,
s	s	k7c7	s
odůvodněním	odůvodnění	k1gNnSc7	odůvodnění
<g/>
,	,	kIx,	,
že	že	k8xS	že
čínský	čínský	k2eAgMnSc1d1	čínský
herec	herec	k1gMnSc1	herec
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
by	by	kYmCp3nP	by
fanoušci	fanoušek	k1gMnPc1	fanoušek
chtěli	chtít	k5eAaImAgMnP	chtít
vidět	vidět	k5eAaImF	vidět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Filmování	filmování	k1gNnPc4	filmování
v	v	k7c4	v
Hong	Hong	k1gInSc4	Hong
Kongu	Kongo	k1gNnSc6	Kongo
===	===	k?	===
</s>
</p>
<p>
<s>
Zklamán	zklamán	k2eAgMnSc1d1	zklamán
se	se	k3xPyFc4	se
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
vydal	vydat	k5eAaPmAgInS	vydat
do	do	k7c2	do
Hong	Hong	k1gInSc4	Hong
Kongu	Kongo	k1gNnSc3	Kongo
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
překvapen	překvapit	k5eAaPmNgMnS	překvapit
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
odezvou	odezva	k1gFnSc7	odezva
místních	místní	k2eAgMnPc2d1	místní
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
ho	on	k3xPp3gMnSc4	on
oslovovali	oslovovat	k5eAaImAgMnP	oslovovat
Kato	Kato	k1gMnSc1	Kato
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gFnSc2	jeho
role	role	k1gFnSc2	role
v	v	k7c6	v
"	"	kIx"	"
<g/>
The	The	k1gFnSc6	The
Green	Green	k2eAgMnSc1d1	Green
Hornet	Hornet	k1gMnSc1	Hornet
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Neunikl	uniknout	k5eNaPmAgMnS	uniknout
pozornosti	pozornost	k1gFnPc4	pozornost
médií	médium	k1gNnPc2	médium
a	a	k8xC	a
místních	místní	k2eAgMnPc2d1	místní
filmových	filmový	k2eAgMnPc2d1	filmový
producentů	producent	k1gMnPc2	producent
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odmítnutích	odmítnutí	k1gNnPc6	odmítnutí
v	v	k7c6	v
Hollywoodu	Hollywood	k1gInSc6	Hollywood
se	se	k3xPyFc4	se
Lee	Lea	k1gFnSc6	Lea
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
točit	točit	k5eAaImF	točit
v	v	k7c4	v
Hong	Hong	k1gInSc4	Hong
Kongu	Kongo	k1gNnSc6	Kongo
a	a	k8xC	a
podepsal	podepsat	k5eAaPmAgMnS	podepsat
smlouvu	smlouva	k1gFnSc4	smlouva
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
filmy	film	k1gInPc4	film
s	s	k7c7	s
Raymondem	Raymond	k1gMnSc7	Raymond
Chowem	Chow	k1gMnSc7	Chow
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
film	film	k1gInSc1	film
Fist	Fist	k1gInSc1	Fist
of	of	k?	of
Fury	Fura	k1gFnSc2	Fura
(	(	kIx(	(
<g/>
Pěst	pěst	k1gFnSc1	pěst
plná	plný	k2eAgFnSc1d1	plná
hněvu	hněv	k1gInSc2	hněv
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
natáčet	natáčet	k5eAaImF	natáčet
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
a	a	k8xC	a
navzdory	navzdory	k7c3	navzdory
snížené	snížený	k2eAgFnSc3d1	snížená
úrovni	úroveň	k1gFnSc3	úroveň
natáčení	natáčení	k1gNnSc2	natáčení
oproti	oproti	k7c3	oproti
Hollywoodu	Hollywood	k1gInSc3	Hollywood
se	se	k3xPyFc4	se
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
filmu	film	k1gInSc2	film
stal	stát	k5eAaPmAgInS	stát
trhák	trhák	k1gInSc1	trhák
<g/>
.	.	kIx.	.
</s>
<s>
Bruce	Bruce	k1gMnSc1	Bruce
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
i	i	k9	i
se	s	k7c7	s
třetím	třetí	k4xOgInSc7	třetí
filmem	film	k1gInSc7	film
(	(	kIx(	(
<g/>
tím	ten	k3xDgInSc7	ten
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
Game	game	k1gInSc4	game
of	of	k?	of
Death	Death	k1gInSc1	Death
-	-	kIx~	-
Hra	hra	k1gFnSc1	hra
smrti	smrt	k1gFnSc2	smrt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
byl	být	k5eAaImAgInS	být
uznávanou	uznávaný	k2eAgFnSc7d1	uznávaná
hvězdou	hvězda	k1gFnSc7	hvězda
<g/>
,	,	kIx,	,
mohl	moct	k5eAaImAgMnS	moct
ovlivňovat	ovlivňovat	k5eAaImF	ovlivňovat
natáčení	natáčení	k1gNnSc4	natáčení
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
napsal	napsat	k5eAaBmAgMnS	napsat
scénář	scénář	k1gInSc4	scénář
a	a	k8xC	a
režíroval	režírovat	k5eAaImAgMnS	režírovat
The	The	k1gMnSc1	The
Way	Way	k1gFnSc2	Way
of	of	k?	of
the	the	k?	the
Dragon	Dragon	k1gMnSc1	Dragon
(	(	kIx(	(
<g/>
Cesta	cesta	k1gFnSc1	cesta
draka	drak	k1gMnSc2	drak
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
začal	začít	k5eAaPmAgInS	začít
natáčet	natáčet	k5eAaImF	natáčet
Game	game	k1gInSc1	game
of	of	k?	of
Death	Death	k1gInSc1	Death
<g/>
,	,	kIx,	,
když	když	k8xS	když
ho	on	k3xPp3gMnSc4	on
kontaktovali	kontaktovat	k5eAaImAgMnP	kontaktovat
Warner	Warner	k1gMnSc1	Warner
Brothers	Brothers	k1gInSc4	Brothers
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
podepsal	podepsat	k5eAaPmAgMnS	podepsat
smlouvu	smlouva	k1gFnSc4	smlouva
na	na	k7c4	na
první	první	k4xOgInSc4	první
americko-hongkongský	americkoongkongský	k2eAgInSc4d1	americko-hongkongský
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
</s>
<s>
Průběh	průběh	k1gInSc1	průběh
natáčení	natáčení	k1gNnSc3	natáčení
byl	být	k5eAaImAgInS	být
provázen	provázet	k5eAaImNgInS	provázet
menšími	malý	k2eAgInPc7d2	menší
problémy	problém	k1gInPc7	problém
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
kvůli	kvůli	k7c3	kvůli
jazykové	jazykový	k2eAgFnSc3d1	jazyková
bariéře	bariéra	k1gFnSc3	bariéra
<g/>
.	.	kIx.	.
</s>
<s>
Nikoho	nikdo	k3yNnSc4	nikdo
však	však	k9	však
nepřekvapilo	překvapit	k5eNaPmAgNnS	překvapit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Enter	Enter	k1gMnSc1	Enter
the	the	k?	the
Dragon	Dragon	k1gMnSc1	Dragon
stal	stát	k5eAaPmAgMnS	stát
legendárním	legendární	k2eAgInSc7d1	legendární
filmem	film	k1gInSc7	film
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Předčasná	předčasný	k2eAgFnSc1d1	předčasná
smrt	smrt	k1gFnSc1	smrt
===	===	k?	===
</s>
</p>
<p>
<s>
Nedožil	dožít	k5eNaPmAgInS	dožít
se	se	k3xPyFc4	se
premiéry	premiér	k1gMnPc7	premiér
svého	svůj	k3xOyFgInSc2	svůj
nejznámějšího	známý	k2eAgInSc2d3	nejznámější
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1973	[number]	k4	1973
upadl	upadnout	k5eAaPmAgMnS	upadnout
do	do	k7c2	do
kómatu	kóma	k1gNnSc2	kóma
a	a	k8xC	a
krátce	krátce	k6eAd1	krátce
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Bolela	bolet	k5eAaImAgFnS	bolet
jej	on	k3xPp3gInSc4	on
hlava	hlava	k1gFnSc1	hlava
<g/>
,	,	kIx,	,
a	a	k8xC	a
aby	aby	kYmCp3nS	aby
tuto	tento	k3xDgFnSc4	tento
bolest	bolest	k1gFnSc4	bolest
utišil	utišit	k5eAaPmAgMnS	utišit
<g/>
,	,	kIx,	,
vzal	vzít	k5eAaPmAgMnS	vzít
si	se	k3xPyFc3	se
prášek	prášek	k1gMnSc1	prášek
(	(	kIx(	(
<g/>
Equagesic	Equagesic	k1gMnSc1	Equagesic
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Alergická	alergický	k2eAgFnSc1d1	alergická
reakce	reakce	k1gFnSc1	reakce
na	na	k7c4	na
prášek	prášek	k1gInSc4	prášek
způsobila	způsobit	k5eAaPmAgFnS	způsobit
masivní	masivní	k2eAgInSc4d1	masivní
otok	otok	k1gInSc4	otok
mozku	mozek	k1gInSc2	mozek
(	(	kIx(	(
<g/>
při	při	k7c6	při
pitvě	pitva	k1gFnSc6	pitva
bylo	být	k5eAaImAgNnS	být
zjištěno	zjištěn	k2eAgNnSc1d1	zjištěno
zvětšení	zvětšení	k1gNnSc1	zvětšení
mozkové	mozkový	k2eAgFnSc2d1	mozková
hmoty	hmota	k1gFnSc2	hmota
z	z	k7c2	z
1,400	[number]	k4	1,400
na	na	k7c4	na
1,575	[number]	k4	1,575
gramů	gram	k1gInPc2	gram
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
zapříčinilo	zapříčinit	k5eAaPmAgNnS	zapříčinit
smrt	smrt	k1gFnSc4	smrt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Filmografie	filmografie	k1gFnSc2	filmografie
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Jeet	Jeet	k2eAgInSc1d1	Jeet
Kune	Kune	k1gInSc1	Kune
Do	do	k7c2	do
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Bruce	Bruce	k1gFnSc2	Bruce
Lee	Lea	k1gFnSc6	Lea
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Bruce	Bruce	k1gMnSc1	Bruce
Lee	Lea	k1gFnSc3	Lea
Foundation	Foundation	k1gInSc4	Foundation
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bruce	Bruka	k1gFnSc3	Bruka
Lee	Lea	k1gFnSc3	Lea
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
