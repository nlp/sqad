<s>
Hasičský	hasičský	k2eAgInSc1d1
záchranný	záchranný	k2eAgInSc1d1
sbor	sbor	k1gInSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
Hasičský	hasičský	k2eAgInSc1d1
záchranný	záchranný	k2eAgInSc1d1
sbor	sbor	k1gInSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
Znak	znak	k1gInSc4
Hasičského	hasičský	k2eAgInSc2d1
záchranného	záchranný	k2eAgInSc2d1
sboru	sbor	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
Vznik	vznik	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1995	#num#	k4
Sídlo	sídlo	k1gNnSc4
</s>
<s>
Kloknerova	Kloknerův	k2eAgFnSc1d1
2295	#num#	k4
<g/>
/	/	kIx~
<g/>
26	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
148	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
Česko	Česko	k1gNnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
2	#num#	k4
<g/>
′	′	k?
<g/>
10,86	10,86	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
29	#num#	k4
<g/>
′	′	k?
<g/>
18,1	18,1	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Generální	generální	k2eAgMnSc1d1
ředitel	ředitel	k1gMnSc1
</s>
<s>
genpor.	genpor.	k?
Drahoslav	Drahoslava	k1gFnPc2
Ryba	ryba	k1gFnSc1
Zaměstnanců	zaměstnanec	k1gMnPc2
</s>
<s>
10	#num#	k4
850	#num#	k4
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
www.hzscr.cz	www.hzscr.cz	k1gMnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
můžou	můžou	k?
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Hasiči	hasič	k1gMnPc1
ve	v	k7c6
spolupráci	spolupráce	k1gFnSc6
se	s	k7c7
zdravotnickou	zdravotnický	k2eAgFnSc7d1
záchrannou	záchranný	k2eAgFnSc7d1
službou	služba	k1gFnSc7
předvádí	předvádět	k5eAaImIp3nS
ukázku	ukázka	k1gFnSc4
vyprošťování	vyprošťování	k1gNnSc2
zraněného	zraněný	k2eAgNnSc2d1
z	z	k7c2
auta	auto	k1gNnSc2
</s>
<s>
Hasičský	hasičský	k2eAgInSc1d1
záchranný	záchranný	k2eAgInSc1d1
sbor	sbor	k1gInSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
(	(	kIx(
<g/>
HZS	HZS	kA
ČR	ČR	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
bezpečnostní	bezpečnostní	k2eAgInSc1d1
sbor	sbor	k1gInSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc7
základním	základní	k2eAgInSc7d1
úkolem	úkol	k1gInSc7
je	být	k5eAaImIp3nS
chránit	chránit	k5eAaImF
životy	život	k1gInPc4
a	a	k8xC
zdraví	zdraví	k1gNnSc4
obyvatel	obyvatel	k1gMnPc2
<g/>
,	,	kIx,
životní	životní	k2eAgNnSc4d1
prostředí	prostředí	k1gNnSc4
<g/>
,	,	kIx,
zvířata	zvíře	k1gNnPc1
a	a	k8xC
majetek	majetek	k1gInSc1
před	před	k7c7
požáry	požár	k1gInPc7
a	a	k8xC
jinými	jiný	k2eAgFnPc7d1
mimořádnými	mimořádný	k2eAgFnPc7d1
událostmi	událost	k1gFnPc7
a	a	k8xC
krizovými	krizový	k2eAgFnPc7d1
situacemi	situace	k1gFnPc7
(	(	kIx(
<g/>
živelní	živelní	k2eAgFnPc1d1
pohromy	pohroma	k1gFnPc1
apod.	apod.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vznikl	vzniknout	k5eAaPmAgInS
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1995	#num#	k4
ze	z	k7c2
Sboru	sbor	k1gInSc2
požární	požární	k2eAgFnSc2d1
ochrany	ochrana	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
na	na	k7c6
základě	základ	k1gInSc6
zákona	zákon	k1gInSc2
č.	č.	k?
203	#num#	k4
<g/>
/	/	kIx~
<g/>
1994	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
K	K	kA
1	#num#	k4
<g/>
.	.	kIx.
lednu	leden	k1gInSc6
2001	#num#	k4
byla	být	k5eAaImAgFnS
zákonem	zákon	k1gInSc7
č.	č.	k?
238	#num#	k4
<g/>
/	/	kIx~
<g/>
2000	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
Hasičském	hasičský	k2eAgInSc6d1
záchranném	záchranný	k2eAgInSc6d1
sboru	sbor	k1gInSc6
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
a	a	k8xC
o	o	k7c6
změně	změna	k1gFnSc6
některých	některý	k3yIgInPc2
zákonů	zákon	k1gInPc2
<g/>
,	,	kIx,
provedena	proveden	k2eAgFnSc1d1
reorganizace	reorganizace	k1gFnSc1
HZS	HZS	kA
ČR	ČR	kA
do	do	k7c2
současné	současný	k2eAgFnSc2d1
podoby	podoba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Činnost	činnost	k1gFnSc1
HZS	HZS	kA
ČR	ČR	kA
se	se	k3xPyFc4
řídí	řídit	k5eAaImIp3nS
dle	dle	k7c2
zákona	zákon	k1gInSc2
č.	č.	k?
320	#num#	k4
<g/>
/	/	kIx~
<g/>
2015	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
Hasičském	hasičský	k2eAgInSc6d1
záchranném	záchranný	k2eAgInSc6d1
sboru	sbor	k1gInSc6
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
a	a	k8xC
o	o	k7c6
změně	změna	k1gFnSc6
některých	některý	k3yIgInPc2
zákonů	zákon	k1gInPc2
(	(	kIx(
<g/>
zákon	zákon	k1gInSc1
o	o	k7c6
hasičském	hasičský	k2eAgInSc6d1
záchranném	záchranný	k2eAgInSc6d1
sboru	sbor	k1gInSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ve	v	k7c6
znění	znění	k1gNnSc6
pozdějších	pozdní	k2eAgInPc2d2
předpisů	předpis	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Hasiči	hasič	k1gMnPc1
jsou	být	k5eAaImIp3nP
ve	v	k7c6
služebním	služební	k2eAgInSc6d1
poměru	poměr	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
upravuje	upravovat	k5eAaImIp3nS
zákon	zákon	k1gInSc4
č.	č.	k?
361	#num#	k4
<g/>
/	/	kIx~
<g/>
2003	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
služebním	služební	k2eAgInSc6d1
poměru	poměr	k1gInSc6
příslušníků	příslušník	k1gMnPc2
bezpečnostních	bezpečnostní	k2eAgInPc2d1
sborů	sbor	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hasičský	hasičský	k2eAgInSc1d1
záchranný	záchranný	k2eAgInSc1d1
sbor	sbor	k1gInSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
měl	mít	k5eAaImAgInS
v	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
celkem	celkem	k6eAd1
9793	#num#	k4
příslušníků	příslušník	k1gMnPc2
a	a	k8xC
dalších	další	k2eAgNnPc2d1
1057	#num#	k4
civilních	civilní	k2eAgMnPc2d1
zaměstnanců	zaměstnanec	k1gMnPc2
v	v	k7c6
pracovním	pracovní	k2eAgInSc6d1
poměru	poměr	k1gInSc6
(	(	kIx(
<g/>
z	z	k7c2
celkového	celkový	k2eAgInSc2d1
počtu	počet	k1gInSc2
je	být	k5eAaImIp3nS
15	#num#	k4
%	%	kIx~
žen	žena	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
čele	čelo	k1gNnSc6
Hasičského	hasičský	k2eAgInSc2d1
záchranného	záchranný	k2eAgInSc2d1
sboru	sbor	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
stojí	stát	k5eAaImIp3nS
generální	generální	k2eAgMnSc1d1
ředitel	ředitel	k1gMnSc1
<g/>
,	,	kIx,
kterým	který	k3yIgMnSc7,k3yRgMnSc7,k3yQgMnSc7
je	být	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
2011	#num#	k4
Drahoslav	Drahoslava	k1gFnPc2
Ryba	ryba	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručná	stručný	k2eAgFnSc1d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s>
Organizační	organizační	k2eAgFnSc1d1
struktura	struktura	k1gFnSc1
</s>
<s>
Hasičský	hasičský	k2eAgInSc1d1
záchranný	záchranný	k2eAgInSc1d1
sbor	sbor	k1gInSc1
Středočeského	středočeský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
-	-	kIx~
Stanice	stanice	k1gFnSc1
Slaný	Slaný	k1gInSc1
</s>
<s>
Hasičský	hasičský	k2eAgInSc1d1
záchranný	záchranný	k2eAgInSc1d1
sbor	sbor	k1gInSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
dle	dle	k7c2
zákona	zákon	k1gInSc2
tvoří	tvořit	k5eAaImIp3nS
generální	generální	k2eAgMnSc1d1
ředitelství	ředitelství	k1gNnSc4
<g/>
,	,	kIx,
hasičské	hasičský	k2eAgInPc1d1
záchranné	záchranný	k2eAgInPc1d1
sbory	sbor	k1gInPc1
krajů	kraj	k1gInPc2
<g/>
,	,	kIx,
záchranný	záchranný	k2eAgInSc1d1
útvar	útvar	k1gInSc1
a	a	k8xC
škola	škola	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Generální	generální	k2eAgNnSc1d1
ředitelství	ředitelství	k1gNnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Seznam	seznam	k1gInSc1
generálních	generální	k2eAgMnPc2d1
ředitelů	ředitel	k1gMnPc2
Hasičského	hasičský	k2eAgInSc2d1
záchranného	záchranný	k2eAgInSc2d1
sboru	sbor	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Generální	generální	k2eAgNnSc1d1
ředitelství	ředitelství	k1gNnSc1
Hasičského	hasičský	k2eAgInSc2d1
záchranného	záchranný	k2eAgInSc2d1
sboru	sbor	k1gInSc2
je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
Ministerstva	ministerstvo	k1gNnSc2
vnitra	vnitro	k1gNnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
a	a	k8xC
řídí	řídit	k5eAaImIp3nP
hasičské	hasičský	k2eAgInPc1d1
záchranné	záchranný	k2eAgInPc1d1
sbory	sbor	k1gInPc1
krajů	kraj	k1gInPc2
<g/>
,	,	kIx,
záchranný	záchranný	k2eAgInSc4d1
útvar	útvar	k1gInSc4
a	a	k8xC
školu	škola	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
jeho	jeho	k3xOp3gNnSc6
čele	čelo	k1gNnSc6
stojí	stát	k5eAaImIp3nS
generální	generální	k2eAgMnSc1d1
ředitel	ředitel	k1gMnSc1
Hasičského	hasičský	k2eAgInSc2d1
záchranného	záchranný	k2eAgInSc2d1
sboru	sbor	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Generálním	generální	k2eAgMnSc7d1
ředitelem	ředitel	k1gMnSc7
HZS	HZS	kA
ČR	ČR	kA
je	být	k5eAaImIp3nS
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2011	#num#	k4
generálporučík	generálporučík	k1gMnSc1
Drahoslav	Drahoslava	k1gFnPc2
Ryba	Ryba	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Součástí	součást	k1gFnSc7
generálního	generální	k2eAgNnSc2d1
ředitelství	ředitelství	k1gNnSc2
jsou	být	k5eAaImIp3nP
vzdělávací	vzdělávací	k2eAgInPc1d1
<g/>
,	,	kIx,
technická	technický	k2eAgNnPc1d1
a	a	k8xC
jiná	jiný	k2eAgNnPc1d1
účelová	účelový	k2eAgNnPc1d1
zařízení	zařízení	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedním	jeden	k4xCgInSc7
z	z	k7c2
nich	on	k3xPp3gInPc2
je	být	k5eAaImIp3nS
Hasičský	hasičský	k2eAgInSc1d1
útvar	útvar	k1gInSc1
ochrany	ochrana	k1gFnSc2
Pražského	pražský	k2eAgInSc2d1
hradu	hrad	k1gInSc2
(	(	kIx(
<g/>
HÚOPH	HÚOPH	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jehož	jenž	k3xRgMnSc4,k3xOyRp3gMnSc4
velitel	velitel	k1gMnSc1
je	být	k5eAaImIp3nS
přímo	přímo	k6eAd1
podřízen	podřídit	k5eAaPmNgInS
generálnímu	generální	k2eAgMnSc3d1
řediteli	ředitel	k1gMnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Pod	pod	k7c7
náměstky	náměstek	k1gMnPc7
generálního	generální	k2eAgMnSc2d1
ředitele	ředitel	k1gMnSc2
spadá	spadat	k5eAaImIp3nS,k5eAaPmIp3nS
Institut	institut	k1gInSc1
ochrany	ochrana	k1gFnSc2
obyvatelstva	obyvatelstvo	k1gNnSc2
(	(	kIx(
<g/>
IOO	IOO	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
sídlící	sídlící	k2eAgFnSc1d1
v	v	k7c6
Lázních	lázeň	k1gFnPc6
Bohdaneč	Bohdaneč	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
Technický	technický	k2eAgInSc1d1
ústav	ústav	k1gInSc1
požární	požární	k2eAgFnSc2d1
ochrany	ochrana	k1gFnSc2
(	(	kIx(
<g/>
TÚPO	TÚPO	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
sídlící	sídlící	k2eAgFnSc1d1
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
Skladovací	skladovací	k2eAgNnSc4d1
a	a	k8xC
opravárenské	opravárenský	k2eAgNnSc4d1
zařízení	zařízení	k1gNnSc4
HZS	HZS	kA
ČR	ČR	kA
(	(	kIx(
<g/>
SOZ	SOZ	kA
HZS	HZS	kA
ČR	ČR	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
sídlící	sídlící	k2eAgFnSc1d1
v	v	k7c6
Olomouci	Olomouc	k1gFnSc6
<g/>
,	,	kIx,
a	a	k8xC
Školní	školní	k2eAgNnSc1d1
a	a	k8xC
výcvikové	výcvikový	k2eAgNnSc1d1
zařízení	zařízení	k1gNnSc1
HZS	HZS	kA
ČR	ČR	kA
(	(	kIx(
<g/>
ŠVZ	ŠVZ	kA
HZS	HZS	kA
ČR	ČR	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
sídlící	sídlící	k2eAgFnSc1d1
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hasičské	hasičský	k2eAgInPc1d1
záchranné	záchranný	k2eAgInPc1d1
sbory	sbor	k1gInPc1
krajů	kraj	k1gInPc2
</s>
<s>
Hasičské	hasičský	k2eAgInPc1d1
záchranné	záchranný	k2eAgInPc1d1
sbory	sbor	k1gInPc1
krajů	kraj	k1gInPc2
jsou	být	k5eAaImIp3nP
organizační	organizační	k2eAgFnPc1d1
složkami	složka	k1gFnPc7
státu	stát	k1gInSc2
a	a	k8xC
účetními	účetní	k2eAgFnPc7d1
jednotkami	jednotka	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Územní	územní	k2eAgInPc1d1
obvody	obvod	k1gInPc1
HZS	HZS	kA
jednotlivých	jednotlivý	k2eAgInPc2d1
krajů	kraj	k1gInPc2
se	se	k3xPyFc4
shodují	shodovat	k5eAaImIp3nP
s	s	k7c7
územím	území	k1gNnSc7
samosprávných	samosprávný	k2eAgInPc2d1
krajů	kraj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
HZS	HZS	kA
krajů	kraj	k1gInPc2
mají	mít	k5eAaImIp3nP
obdobnou	obdobný	k2eAgFnSc4d1
vnitřní	vnitřní	k2eAgFnSc4d1
strukturu	struktura	k1gFnSc4
jako	jako	k8xS,k8xC
generální	generální	k2eAgNnSc4d1
ředitelství	ředitelství	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krajská	krajský	k2eAgFnSc1d1
ředitelství	ředitelství	k1gNnSc2
<g/>
,	,	kIx,
v	v	k7c6
jejichž	jejichž	k3xOyRp3gNnPc6
čelech	čelo	k1gNnPc6
stojí	stát	k5eAaImIp3nP
ředitelé	ředitel	k1gMnPc1
<g/>
,	,	kIx,
řídí	řídit	k5eAaImIp3nP
jednotlivé	jednotlivý	k2eAgInPc1d1
územní	územní	k2eAgInPc1d1
odbory	odbor	k1gInPc1
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gFnSc1
působnost	působnost	k1gFnSc1
je	být	k5eAaImIp3nS
shodná	shodný	k2eAgFnSc1d1
s	s	k7c7
územím	území	k1gNnSc7
okresů	okres	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
HZS	HZS	kA
kraje	kraj	k1gInPc1
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sídlo	sídlo	k1gNnSc1
</s>
<s>
Počet	počet	k1gInSc1
stanic	stanice	k1gFnPc2
</s>
<s>
Územní	územní	k2eAgInPc1d1
odbory	odbor	k1gInPc1
</s>
<s>
Hasičský	hasičský	k2eAgInSc1d1
záchranný	záchranný	k2eAgInSc1d1
sbor	sbor	k1gInSc1
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
</s>
<s>
Praha	Praha	k1gFnSc1
</s>
<s>
11	#num#	k4
</s>
<s>
nečlení	členit	k5eNaImIp3nS
se	se	k3xPyFc4
</s>
<s>
Hasičský	hasičský	k2eAgInSc1d1
záchranný	záchranný	k2eAgInSc1d1
sbor	sbor	k1gInSc1
Středočeského	středočeský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
</s>
<s>
Kladno	Kladno	k1gNnSc1
</s>
<s>
33	#num#	k4
</s>
<s>
9	#num#	k4
–	–	k?
Benešov	Benešov	k1gInSc1
<g/>
,	,	kIx,
Beroun	Beroun	k1gInSc1
<g/>
,	,	kIx,
Kladno	Kladno	k1gNnSc1
<g/>
,	,	kIx,
Kolín	Kolín	k1gInSc1
<g/>
,	,	kIx,
Kutná	kutný	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
<g/>
,	,	kIx,
Mělník	Mělník	k1gInSc1
<g/>
,	,	kIx,
Mladá	mladá	k1gFnSc1
Boleslav	Boleslav	k1gMnSc1
<g/>
,	,	kIx,
Nymburk	Nymburk	k1gInSc1
<g/>
,	,	kIx,
Příbram	Příbram	k1gFnSc1
</s>
<s>
Hasičský	hasičský	k2eAgInSc1d1
záchranný	záchranný	k2eAgInSc1d1
sbor	sbor	k1gInSc1
Jihočeského	jihočeský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
</s>
<s>
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
</s>
<s>
20	#num#	k4
</s>
<s>
7	#num#	k4
–	–	k?
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
<g/>
,	,	kIx,
Český	český	k2eAgInSc1d1
Krumlov	Krumlov	k1gInSc1
<g/>
,	,	kIx,
Jindřichův	Jindřichův	k2eAgInSc1d1
Hradec	Hradec	k1gInSc1
<g/>
,	,	kIx,
Písek	Písek	k1gInSc1
<g/>
,	,	kIx,
Prachatice	Prachatice	k1gFnPc1
<g/>
,	,	kIx,
Strakonice	Strakonice	k1gFnPc1
<g/>
,	,	kIx,
Tábor	Tábor	k1gInSc1
</s>
<s>
Hasičský	hasičský	k2eAgInSc1d1
záchranný	záchranný	k2eAgInSc1d1
sbor	sbor	k1gInSc1
Plzeňského	plzeňský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
</s>
<s>
Plzeň	Plzeň	k1gFnSc1
</s>
<s>
17	#num#	k4
</s>
<s>
5	#num#	k4
–	–	k?
Domažlice	Domažlice	k1gFnPc1
<g/>
,	,	kIx,
Klatovy	Klatovy	k1gInPc1
<g/>
,	,	kIx,
Plzeň	Plzeň	k1gFnSc1
<g/>
,	,	kIx,
Rokycany	Rokycany	k1gInPc1
<g/>
,	,	kIx,
Tachov	Tachov	k1gInSc1
</s>
<s>
Hasičský	hasičský	k2eAgInSc1d1
záchranný	záchranný	k2eAgInSc1d1
sbor	sbor	k1gInSc1
Karlovarského	karlovarský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
</s>
<s>
Karlovy	Karlův	k2eAgInPc1d1
Vary	Vary	k1gInPc1
</s>
<s>
8	#num#	k4
</s>
<s>
3	#num#	k4
–	–	k?
Cheb	Cheb	k1gInSc1
<g/>
,	,	kIx,
Karlovy	Karlův	k2eAgInPc1d1
Vary	Vary	k1gInPc1
<g/>
,	,	kIx,
Sokolov	Sokolov	k1gInSc1
</s>
<s>
Hasičský	hasičský	k2eAgInSc1d1
záchranný	záchranný	k2eAgInSc1d1
sbor	sbor	k1gInSc1
Ústeckého	ústecký	k2eAgInSc2d1
kraje	kraj	k1gInSc2
</s>
<s>
Ústí	ústí	k1gNnSc1
nad	nad	k7c7
Labem	Labe	k1gNnSc7
</s>
<s>
21	#num#	k4
</s>
<s>
7	#num#	k4
–	–	k?
Děčín	Děčín	k1gInSc1
<g/>
,	,	kIx,
Chomutov	Chomutov	k1gInSc1
<g/>
,	,	kIx,
Litoměřice	Litoměřice	k1gInPc1
<g/>
,	,	kIx,
Most	most	k1gInSc1
<g/>
,	,	kIx,
Teplice	Teplice	k1gFnPc1
<g/>
,	,	kIx,
Ústí	ústí	k1gNnSc1
nad	nad	k7c7
Labem	Labe	k1gNnSc7
<g/>
,	,	kIx,
Žatec	Žatec	k1gInSc1
</s>
<s>
Hasičský	hasičský	k2eAgInSc1d1
záchranný	záchranný	k2eAgInSc1d1
sbor	sbor	k1gInSc1
Libereckého	liberecký	k2eAgInSc2d1
kraje	kraj	k1gInSc2
</s>
<s>
Liberec	Liberec	k1gInSc1
</s>
<s>
9	#num#	k4
</s>
<s>
4	#num#	k4
–	–	k?
Česká	český	k2eAgFnSc1d1
Lípa	lípa	k1gFnSc1
<g/>
,	,	kIx,
Liberec	Liberec	k1gInSc1
<g/>
,	,	kIx,
Jablonec	Jablonec	k1gInSc1
nad	nad	k7c7
Nisou	Nisa	k1gFnSc7
<g/>
,	,	kIx,
Semily	Semily	k1gInPc7
</s>
<s>
Hasičský	hasičský	k2eAgInSc1d1
záchranný	záchranný	k2eAgInSc1d1
sbor	sbor	k1gInSc1
Královéhradeckého	královéhradecký	k2eAgInSc2d1
kraje	kraj	k1gInSc2
</s>
<s>
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
</s>
<s>
14	#num#	k4
</s>
<s>
5	#num#	k4
–	–	k?
Jičín	Jičín	k1gInSc1
<g/>
,	,	kIx,
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
<g/>
,	,	kIx,
Náchod	Náchod	k1gInSc1
<g/>
,	,	kIx,
Rychnov	Rychnov	k1gInSc1
nad	nad	k7c7
Kněžnou	kněžna	k1gFnSc7
<g/>
,	,	kIx,
Trutnov	Trutnov	k1gInSc1
</s>
<s>
Hasičský	hasičský	k2eAgInSc1d1
záchranný	záchranný	k2eAgInSc1d1
sbor	sbor	k1gInSc1
Pardubického	pardubický	k2eAgInSc2d1
kraje	kraj	k1gInSc2
</s>
<s>
Pardubice	Pardubice	k1gInPc1
</s>
<s>
15	#num#	k4
</s>
<s>
4	#num#	k4
–	–	k?
Chrudim	Chrudim	k1gFnSc1
<g/>
,	,	kIx,
Pardubice	Pardubice	k1gInPc1
<g/>
,	,	kIx,
Svitavy	Svitava	k1gFnPc1
<g/>
,	,	kIx,
Ústí	ústí	k1gNnSc1
nad	nad	k7c7
Orlicí	Orlice	k1gFnSc7
</s>
<s>
Hasičský	hasičský	k2eAgInSc4d1
záchranný	záchranný	k2eAgInSc4d1
sbor	sbor	k1gInSc4
Kraje	kraj	k1gInSc2
Vysočina	vysočina	k1gFnSc1
</s>
<s>
Jihlava	Jihlava	k1gFnSc1
</s>
<s>
21	#num#	k4
</s>
<s>
5	#num#	k4
–	–	k?
Havlíčkův	Havlíčkův	k2eAgInSc1d1
Brod	Brod	k1gInSc1
<g/>
,	,	kIx,
Jihlava	Jihlava	k1gFnSc1
<g/>
,	,	kIx,
Pelhřimov	Pelhřimov	k1gInSc1
<g/>
,	,	kIx,
Třebíč	Třebíč	k1gFnSc1
<g/>
,	,	kIx,
Žďár	Žďár	k1gInSc1
nad	nad	k7c7
Sázavou	Sázava	k1gFnSc7
</s>
<s>
Hasičský	hasičský	k2eAgInSc1d1
záchranný	záchranný	k2eAgInSc1d1
sbor	sbor	k1gInSc1
Jihomoravského	jihomoravský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
</s>
<s>
Brno	Brno	k1gNnSc1
</s>
<s>
26	#num#	k4
</s>
<s>
7	#num#	k4
–	–	k?
Brno-město	Brno-města	k1gMnSc5
<g/>
,	,	kIx,
Brno-venkov	Brno-venkov	k1gInSc1
<g/>
,	,	kIx,
Blansko	Blansko	k1gNnSc1
<g/>
,	,	kIx,
Břeclav	Břeclav	k1gFnSc1
<g/>
,	,	kIx,
Hodonín	Hodonín	k1gInSc1
<g/>
,	,	kIx,
Vyškov	Vyškov	k1gInSc1
<g/>
,	,	kIx,
Znojmo	Znojmo	k1gNnSc1
</s>
<s>
Hasičský	hasičský	k2eAgInSc1d1
záchranný	záchranný	k2eAgInSc1d1
sbor	sbor	k1gInSc1
Olomouckého	olomoucký	k2eAgInSc2d1
kraje	kraj	k1gInSc2
</s>
<s>
Olomouc	Olomouc	k1gFnSc1
</s>
<s>
13	#num#	k4
</s>
<s>
5	#num#	k4
–	–	k?
Olomouc	Olomouc	k1gFnSc1
<g/>
,	,	kIx,
Prostějov	Prostějov	k1gInSc1
<g/>
,	,	kIx,
Přerov	Přerov	k1gInSc1
<g/>
,	,	kIx,
Šumperk	Šumperk	k1gInSc1
<g/>
,	,	kIx,
Jeseník	Jeseník	k1gInSc1
</s>
<s>
Hasičský	hasičský	k2eAgInSc1d1
záchranný	záchranný	k2eAgInSc1d1
sbor	sbor	k1gInSc1
Moravskoslezského	moravskoslezský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
</s>
<s>
Ostrava	Ostrava	k1gFnSc1
</s>
<s>
22	#num#	k4
</s>
<s>
6	#num#	k4
–	–	k?
Bruntál	Bruntál	k1gInSc1
<g/>
,	,	kIx,
Frýdek-Místek	Frýdek-Místek	k1gInSc1
<g/>
,	,	kIx,
Karviná	Karviná	k1gFnSc1
<g/>
,	,	kIx,
Nový	nový	k2eAgInSc1d1
Jičín	Jičín	k1gInSc1
<g/>
,	,	kIx,
Ostrava	Ostrava	k1gFnSc1
<g/>
,	,	kIx,
Opava	Opava	k1gFnSc1
</s>
<s>
Hasičský	hasičský	k2eAgInSc1d1
záchranný	záchranný	k2eAgInSc1d1
sbor	sbor	k1gInSc1
Zlínského	zlínský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
</s>
<s>
Zlín	Zlín	k1gInSc1
</s>
<s>
13	#num#	k4
</s>
<s>
4	#num#	k4
–	–	k?
Kroměříž	Kroměříž	k1gFnSc1
<g/>
,	,	kIx,
Uherské	uherský	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
<g/>
,	,	kIx,
Vsetín	Vsetín	k1gInSc1
<g/>
,	,	kIx,
Zlín	Zlín	k1gInSc1
</s>
<s>
Záchranný	záchranný	k2eAgInSc1d1
útvar	útvar	k1gInSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Záchranný	záchranný	k2eAgInSc4d1
útvar	útvar	k1gInSc4
Hasičského	hasičský	k2eAgInSc2d1
záchranného	záchranný	k2eAgInSc2d1
sboru	sbor	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Záchranný	záchranný	k2eAgInSc1d1
útvar	útvar	k1gInSc1
Hasičského	hasičský	k2eAgInSc2d1
záchranného	záchranný	k2eAgInSc2d1
sboru	sbor	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
(	(	kIx(
<g/>
ZÚ	zú	k0
HZS	HZS	kA
ČR	ČR	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
organizační	organizační	k2eAgFnSc7d1
složkou	složka	k1gFnSc7
státu	stát	k1gInSc2
a	a	k8xC
účetní	účetní	k2eAgFnSc7d1
jednotkou	jednotka	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
jeho	jeho	k3xOp3gInSc6
čele	čelo	k1gNnSc6
stojí	stát	k5eAaImIp3nS
velitel	velitel	k1gMnSc1
útvaru	útvar	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
celorepublikový	celorepublikový	k2eAgInSc4d1
útvar	útvar	k1gInSc4
se	s	k7c7
sídlem	sídlo	k1gNnSc7
v	v	k7c6
Hlučíně	Hlučín	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
určen	určit	k5eAaPmNgInS
pro	pro	k7c4
řešení	řešení	k1gNnSc4
mimořádných	mimořádný	k2eAgFnPc2d1
událostí	událost	k1gFnPc2
velkého	velký	k2eAgInSc2d1
rozsahu	rozsah	k1gInSc2
<g/>
,	,	kIx,
k	k	k7c3
čemuž	což	k3yRnSc3,k3yQnSc3
disponuje	disponovat	k5eAaBmIp3nS
odpovídající	odpovídající	k2eAgNnSc1d1
těžkou	těžký	k2eAgFnSc7d1
technikou	technika	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Škola	škola	k1gFnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Střední	střední	k2eAgFnSc1d1
odborná	odborný	k2eAgFnSc1d1
škola	škola	k1gFnSc1
požární	požární	k2eAgFnSc2d1
ochrany	ochrana	k1gFnSc2
a	a	k8xC
Vyšší	vysoký	k2eAgFnSc1d2
odborná	odborný	k2eAgFnSc1d1
škola	škola	k1gFnSc1
požární	požární	k2eAgFnSc2d1
ochrany	ochrana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Střední	střední	k2eAgFnSc1d1
odborná	odborný	k2eAgFnSc1d1
škola	škola	k1gFnSc1
požární	požární	k2eAgFnSc2d1
ochrany	ochrana	k1gFnSc2
a	a	k8xC
Vyšší	vysoký	k2eAgFnSc1d2
odborná	odborný	k2eAgFnSc1d1
škola	škola	k1gFnSc1
požární	požární	k2eAgFnSc2d1
ochrany	ochrana	k1gFnSc2
je	být	k5eAaImIp3nS
organizační	organizační	k2eAgFnSc7d1
složkou	složka	k1gFnSc7
státu	stát	k1gInSc2
a	a	k8xC
účetní	účetní	k2eAgFnSc7d1
jednotkou	jednotka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
jejím	její	k3xOp3gNnSc6
čele	čelo	k1gNnSc6
je	být	k5eAaImIp3nS
ředitel	ředitel	k1gMnSc1
<g/>
,	,	kIx,
sídlí	sídlet	k5eAaImIp3nS
ve	v	k7c6
Frýdku-Místku	Frýdku-Místek	k1gInSc6
a	a	k8xC
poskytuje	poskytovat	k5eAaImIp3nS
vzdělání	vzdělání	k1gNnSc1
v	v	k7c6
oblasti	oblast	k1gFnSc6
požární	požární	k2eAgFnSc2d1
ochrany	ochrana	k1gFnSc2
<g/>
,	,	kIx,
ochrany	ochrana	k1gFnSc2
obyvatelstva	obyvatelstvo	k1gNnSc2
<g/>
,	,	kIx,
integrovaného	integrovaný	k2eAgInSc2d1
záchranného	záchranný	k2eAgInSc2d1
systému	systém	k1gInSc2
a	a	k8xC
krizového	krizový	k2eAgNnSc2d1
řízení	řízení	k1gNnSc2
a	a	k8xC
provádí	provádět	k5eAaImIp3nS
odbornou	odborný	k2eAgFnSc4d1
přípravu	příprava	k1gFnSc4
podle	podle	k7c2
zákona	zákon	k1gInSc2
o	o	k7c6
požární	požární	k2eAgFnSc6d1
ochraně	ochrana	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hasičské	hasičský	k2eAgFnPc1d1
stanice	stanice	k1gFnPc1
</s>
<s>
Sídlo	sídlo	k1gNnSc1
Hasičského	hasičský	k2eAgInSc2d1
záchranného	záchranný	k2eAgInSc2d1
sboru	sbor	k1gInSc2
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
v	v	k7c6
Sokolské	sokolský	k2eAgFnSc6d1
ulici	ulice	k1gFnSc6
</s>
<s>
Po	po	k7c6
celé	celá	k1gFnSc6
ČR	ČR	kA
je	být	k5eAaImIp3nS
rozmístěných	rozmístěný	k2eAgInPc2d1
celkem	celkem	k6eAd1
238	#num#	k4
stanic	stanice	k1gFnPc2
Hasičského	hasičský	k2eAgInSc2d1
záchranného	záchranný	k2eAgInSc2d1
sboru	sbor	k1gInSc2
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s>
C1	C1	k4
-	-	kIx~
stanice	stanice	k1gFnSc1
umístěná	umístěný	k2eAgFnSc1d1
v	v	k7c6
obci	obec	k1gFnSc6
s	s	k7c7
počtem	počet	k1gInSc7
obyvatel	obyvatel	k1gMnPc2
do	do	k7c2
40	#num#	k4
tisíc	tisíc	k4xCgInPc2
<g/>
,	,	kIx,
</s>
<s>
C2	C2	k4
-	-	kIx~
stanice	stanice	k1gFnSc1
umístěná	umístěný	k2eAgFnSc1d1
v	v	k7c6
obci	obec	k1gFnSc6
s	s	k7c7
počtem	počet	k1gInSc7
obyvatel	obyvatel	k1gMnPc2
od	od	k7c2
40	#num#	k4
tisíc	tisíc	k4xCgInPc2
do	do	k7c2
75	#num#	k4
tisíc	tisíc	k4xCgInPc2
<g/>
,	,	kIx,
</s>
<s>
C3	C3	k4
-	-	kIx~
stanice	stanice	k1gFnSc1
umístěná	umístěný	k2eAgFnSc1d1
v	v	k7c6
obci	obec	k1gFnSc6
s	s	k7c7
počtem	počet	k1gInSc7
obyvatel	obyvatel	k1gMnPc2
nad	nad	k7c4
75	#num#	k4
tisíc	tisíc	k4xCgInPc2
<g/>
,	,	kIx,
</s>
<s>
P0	P0	k4
-	-	kIx~
stanice	stanice	k1gFnSc1
umístěná	umístěný	k2eAgFnSc1d1
v	v	k7c6
obci	obec	k1gFnSc6
s	s	k7c7
počtem	počet	k1gInSc7
obyvatel	obyvatel	k1gMnPc2
do	do	k7c2
15	#num#	k4
tisíc	tisíc	k4xCgInPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
jednotka	jednotka	k1gFnSc1
hasičského	hasičský	k2eAgInSc2d1
záchranného	záchranný	k2eAgInSc2d1
sboru	sbor	k1gInSc2
kraje	kraj	k1gInSc2
vznikla	vzniknout	k5eAaPmAgFnS
sdružením	sdružení	k1gNnSc7
prostředků	prostředek	k1gInPc2
obce	obec	k1gFnSc2
a	a	k8xC
hasičského	hasičský	k2eAgInSc2d1
záchranného	záchranný	k2eAgInSc2d1
sboru	sbor	k1gInSc2
kraje	kraj	k1gInSc2
podle	podle	k7c2
§	§	k?
69	#num#	k4
<g/>
a	a	k8xC
zákona	zákon	k1gInSc2
<g/>
,	,	kIx,
</s>
<s>
P1	P1	k4
-	-	kIx~
stanice	stanice	k1gFnSc1
umístěná	umístěný	k2eAgFnSc1d1
v	v	k7c6
obci	obec	k1gFnSc6
s	s	k7c7
počtem	počet	k1gInSc7
obyvatel	obyvatel	k1gMnPc2
do	do	k7c2
15	#num#	k4
tisíc	tisíc	k4xCgInPc2
nebo	nebo	k8xC
v	v	k7c6
části	část	k1gFnSc6
obce	obec	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
jednotka	jednotka	k1gFnSc1
hasičského	hasičský	k2eAgInSc2d1
záchranného	záchranný	k2eAgInSc2d1
sboru	sbor	k1gInSc2
kraje	kraj	k1gInSc2
zabezpečuje	zabezpečovat	k5eAaImIp3nS
výjezd	výjezd	k1gInSc1
družstva	družstvo	k1gNnSc2
o	o	k7c6
zmenšeném	zmenšený	k2eAgInSc6d1
početním	početní	k2eAgInSc6d1
stavu	stav	k1gInSc6
</s>
<s>
P2	P2	k4
-	-	kIx~
stanice	stanice	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
zabezpečuje	zabezpečovat	k5eAaImIp3nS
výjezd	výjezd	k1gInSc4
družstva	družstvo	k1gNnSc2
a	a	k8xC
je	být	k5eAaImIp3nS
vybavena	vybavit	k5eAaPmNgFnS
stanovenou	stanovený	k2eAgFnSc7d1
požární	požární	k2eAgFnSc7d1
technikou	technika	k1gFnSc7
a	a	k8xC
automobilovým	automobilový	k2eAgInSc7d1
žebříkem	žebřík	k1gInSc7
<g/>
,	,	kIx,
</s>
<s>
P3	P3	k4
-	-	kIx~
stanice	stanice	k1gFnSc1
umístěná	umístěný	k2eAgFnSc1d1
v	v	k7c6
obci	obec	k1gFnSc6
s	s	k7c7
počtem	počet	k1gInSc7
obyvatel	obyvatel	k1gMnPc2
nad	nad	k7c4
30	#num#	k4
tisíc	tisíc	k4xCgInPc2
nebo	nebo	k8xC
v	v	k7c6
části	část	k1gFnSc6
obce	obec	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
jednotka	jednotka	k1gFnSc1
hasičského	hasičský	k2eAgInSc2d1
záchranného	záchranný	k2eAgInSc2d1
sboru	sbor	k1gInSc2
kraje	kraj	k1gInSc2
zabezpečuje	zabezpečovat	k5eAaImIp3nS
výjezd	výjezd	k1gInSc1
družstva	družstvo	k1gNnSc2
a	a	k8xC
družstva	družstvo	k1gNnSc2
o	o	k7c6
zmenšeném	zmenšený	k2eAgInSc6d1
početním	početní	k2eAgInSc6d1
stavu	stav	k1gInSc6
a	a	k8xC
je	být	k5eAaImIp3nS
vybavena	vybavit	k5eAaPmNgFnS
stanovenou	stanovený	k2eAgFnSc7d1
požární	požární	k2eAgFnSc7d1
technikou	technika	k1gFnSc7
<g/>
,	,	kIx,
automobilovým	automobilový	k2eAgInSc7d1
žebříkem	žebřík	k1gInSc7
a	a	k8xC
další	další	k2eAgFnSc7d1
požární	požární	k2eAgFnSc7d1
technikou	technika	k1gFnSc7
<g/>
,	,	kIx,
</s>
<s>
P4	P4	k4
-	-	kIx~
stanice	stanice	k1gFnSc1
umístěná	umístěný	k2eAgFnSc1d1
v	v	k7c6
obci	obec	k1gFnSc6
s	s	k7c7
počtem	počet	k1gInSc7
obyvatel	obyvatel	k1gMnPc2
nad	nad	k7c4
15	#num#	k4
tisíc	tisíc	k4xCgInPc2
nebo	nebo	k8xC
v	v	k7c6
části	část	k1gFnSc6
obce	obec	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
jednotka	jednotka	k1gFnSc1
hasičského	hasičský	k2eAgInSc2d1
záchranného	záchranný	k2eAgInSc2d1
sboru	sbor	k1gInSc2
kraje	kraj	k1gInSc2
zabezpečuje	zabezpečovat	k5eAaImIp3nS
výjezdy	výjezd	k1gInPc4
2	#num#	k4
družstev	družstvo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Úkoly	úkol	k1gInPc1
</s>
<s>
Základním	základní	k2eAgNnSc7d1
posláním	poslání	k1gNnSc7
HZS	HZS	kA
ČR	ČR	kA
je	být	k5eAaImIp3nS
chránit	chránit	k5eAaImF
životy	život	k1gInPc4
a	a	k8xC
zdraví	zdraví	k1gNnSc4
obyvatel	obyvatel	k1gMnPc2
a	a	k8xC
majetek	majetek	k1gInSc4
před	před	k7c4
požáry	požár	k1gInPc4
a	a	k8xC
poskytovat	poskytovat	k5eAaImF
účinnou	účinný	k2eAgFnSc4d1
pomoc	pomoc	k1gFnSc4
při	při	k7c6
mimořádných	mimořádný	k2eAgFnPc6d1
událostech	událost	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
HZS	HZS	kA
ČR	ČR	kA
plní	plnit	k5eAaImIp3nS
úkoly	úkol	k1gInPc4
v	v	k7c6
oblasti	oblast	k1gFnSc6
integrovaného	integrovaný	k2eAgInSc2d1
záchranného	záchranný	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
,	,	kIx,
krizového	krizový	k2eAgNnSc2d1
řízení	řízení	k1gNnSc2
a	a	k8xC
požární	požární	k2eAgFnSc2d1
ochrany	ochrana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Pokud	pokud	k8xS
zvláštní	zvláštní	k2eAgInSc1d1
právní	právní	k2eAgInSc1d1
předpis	předpis	k1gInSc1
stanoví	stanovit	k5eAaPmIp3nS
v	v	k7c6
mezích	mez	k1gFnPc6
úkolů	úkol	k1gInPc2
hasičského	hasičský	k2eAgInSc2d1
záchranného	záchranný	k2eAgInSc2d1
sboru	sbor	k1gInSc2
působnost	působnost	k1gFnSc4
ministerstva	ministerstvo	k1gNnSc2
<g/>
,	,	kIx,
vykonává	vykonávat	k5eAaImIp3nS
ji	on	k3xPp3gFnSc4
generální	generální	k2eAgNnSc1d1
ředitelství	ředitelství	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úkoly	úkol	k1gInPc4
hasičského	hasičský	k2eAgInSc2d1
záchranného	záchranný	k2eAgInSc2d1
sboru	sbor	k1gInSc2
plní	plnit	k5eAaImIp3nP
příslušníci	příslušník	k1gMnPc1
HZS	HZS	kA
ve	v	k7c6
služebním	služební	k2eAgInSc6d1
poměru	poměr	k1gInSc6
a	a	k8xC
občanští	občanský	k2eAgMnPc1d1
zaměstnanci	zaměstnanec	k1gMnPc1
HZS	HZS	kA
v	v	k7c6
pracovním	pracovní	k2eAgInSc6d1
poměru	poměr	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Příslušníci	příslušník	k1gMnPc1
</s>
<s>
Příslušníkům	příslušník	k1gMnPc3
přísluší	příslušet	k5eAaImIp3nS
služební	služební	k2eAgInSc4d1
stejnokroj	stejnokroj	k1gInSc4
a	a	k8xC
služební	služební	k2eAgFnSc4d1
hodnost	hodnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každý	každý	k3xTgMnSc1
příslušník	příslušník	k1gMnSc1
je	být	k5eAaImIp3nS
i	i	k9
v	v	k7c6
době	doba	k1gFnSc6
mimo	mimo	k7c4
službu	služba	k1gFnSc4
<g/>
,	,	kIx,
pokud	pokud	k8xS
není	být	k5eNaImIp3nS
pod	pod	k7c7
vlivem	vliv	k1gInSc7
léků	lék	k1gInPc2
nebo	nebo	k8xC
jiných	jiný	k2eAgFnPc2d1
látek	látka	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
závažným	závažný	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
snižují	snižovat	k5eAaImIp3nP
jeho	jeho	k3xOp3gFnSc4
schopnost	schopnost	k1gFnSc4
jednání	jednání	k1gNnSc2
<g/>
,	,	kIx,
povinen	povinen	k2eAgMnSc1d1
provést	provést	k5eAaPmF
zásah	zásah	k1gInSc4
<g/>
,	,	kIx,
popřípadě	popřípadě	k6eAd1
učinit	učinit	k5eAaPmF,k5eAaImF
jiná	jiný	k2eAgNnPc4d1
opatření	opatření	k1gNnPc4
k	k	k7c3
provedení	provedení	k1gNnSc3
zásahu	zásah	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Služební	služební	k2eAgInSc1d1
slib	slib	k1gInSc1
příslušníka	příslušník	k1gMnSc2
zní	znět	k5eAaImIp3nS
<g/>
:	:	kIx,
„	„	k?
<g/>
Slibuji	slibovat	k5eAaImIp1nS
na	na	k7c4
svou	svůj	k3xOyFgFnSc4
čest	čest	k1gFnSc4
a	a	k8xC
svědomí	svědomí	k1gNnSc4
<g/>
,	,	kIx,
že	že	k8xS
při	při	k7c6
výkonu	výkon	k1gInSc6
služby	služba	k1gFnSc2
budu	být	k5eAaImBp1nS
nestranný	nestranný	k2eAgMnSc1d1
a	a	k8xC
budu	být	k5eAaImBp1nS
důsledně	důsledně	k6eAd1
dodržovat	dodržovat	k5eAaImF
právní	právní	k2eAgInPc4d1
a	a	k8xC
služební	služební	k2eAgInPc4d1
předpisy	předpis	k1gInPc4
<g/>
,	,	kIx,
plnit	plnit	k5eAaImF
rozkazy	rozkaz	k1gInPc4
svých	svůj	k3xOyFgMnPc2
nadřízených	nadřízený	k1gMnPc2
a	a	k8xC
nikdy	nikdy	k6eAd1
nezneužiji	zneužít	k5eNaPmIp1nS
svého	svůj	k3xOyFgNnSc2
služebního	služební	k2eAgNnSc2d1
postavení	postavení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Budu	být	k5eAaImBp1nS
se	se	k3xPyFc4
vždy	vždy	k6eAd1
a	a	k8xC
všude	všude	k6eAd1
chovat	chovat	k5eAaImF
tak	tak	k6eAd1
<g/>
,	,	kIx,
abych	aby	kYmCp1nS
svým	svůj	k3xOyFgMnPc3
jednáním	jednání	k1gNnPc3
neohrozil	ohrozit	k5eNaPmAgInS
dobrou	dobrý	k2eAgFnSc4d1
pověst	pověst	k1gFnSc4
bezpečnostního	bezpečnostní	k2eAgInSc2d1
sboru	sbor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Služební	služební	k2eAgFnPc4d1
povinnosti	povinnost	k1gFnPc4
budu	být	k5eAaImBp1nS
plnit	plnit	k5eAaImF
řádně	řádně	k6eAd1
a	a	k8xC
svědomitě	svědomitě	k6eAd1
a	a	k8xC
nebudu	být	k5eNaImBp1nS
váhat	váhat	k5eAaImF
při	při	k7c6
ochraně	ochrana	k1gFnSc6
zájmů	zájem	k1gInPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
nasadit	nasadit	k5eAaPmF
i	i	k9
vlastní	vlastní	k2eAgInSc4d1
život	život	k1gInSc4
<g/>
.	.	kIx.
<g/>
“	“	k?
Složení	složení	k1gNnSc1
služebního	služební	k2eAgInSc2d1
slibu	slib	k1gInSc2
je	být	k5eAaImIp3nS
podmínkou	podmínka	k1gFnSc7
vzniku	vznik	k1gInSc2
služebního	služební	k2eAgInSc2d1
poměru	poměr	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Statistika	statistika	k1gFnSc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
JPO	JPO	kA
(	(	kIx(
<g/>
jednotky	jednotka	k1gFnSc2
požární	požární	k2eAgFnSc2d1
ochrany	ochrana	k1gFnSc2
<g/>
)	)	kIx)
zasahovaly	zasahovat	k5eAaImAgFnP
celkem	celkem	k6eAd1
u	u	k7c2
125	#num#	k4
974	#num#	k4
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
událostí	událost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
celkového	celkový	k2eAgInSc2d1
počtu	počet	k1gInSc2
je	být	k5eAaImIp3nS
zahrnuto	zahrnout	k5eAaPmNgNnS
24	#num#	k4
událostí	událost	k1gFnPc2
(	(	kIx(
<g/>
z	z	k7c2
toho	ten	k3xDgNnSc2
14	#num#	k4
požárů	požár	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
k	k	k7c3
nimž	jenž	k3xRgFnPc3
došlo	dojít	k5eAaPmAgNnS
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
a	a	k8xC
byly	být	k5eAaImAgFnP
k	k	k7c3
nim	on	k3xPp3gInPc3
JPO	JPO	kA
z	z	k7c2
ČR	ČR	kA
povolány	povolán	k2eAgFnPc1d1
nebo	nebo	k8xC
se	se	k3xPyFc4
</s>
<s>
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
zásah	zásah	k1gInSc4
na	na	k7c6
obou	dva	k4xCgFnPc6
stranách	strana	k1gFnPc6
hranice	hranice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
jsou	být	k5eAaImIp3nP
do	do	k7c2
celkového	celkový	k2eAgInSc2d1
počtu	počet	k1gInSc2
zahrnuty	zahrnout	k5eAaPmNgInP
i	i	k8xC
humanitární	humanitární	k2eAgFnSc2d1
pomoci	pomoc	k1gFnSc2
ČR	ČR	kA
do	do	k7c2
zahraničí	zahraničí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hasiči	hasič	k1gMnPc7
při	při	k7c6
zásazích	zásah	k1gInPc6
bezprostředně	bezprostředně	k6eAd1
zachránily	zachránit	k5eAaPmAgFnP
26	#num#	k4
424	#num#	k4
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
osob	osoba	k1gFnPc2
a	a	k8xC
dalších	další	k2eAgNnPc2d1
39	#num#	k4
209	#num#	k4
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
před	před	k7c7
hrozícím	hrozící	k2eAgNnSc7d1
nebezpečím	nebezpečí	k1gNnSc7
evakuovaly	evakuovat	k5eAaBmAgFnP
<g/>
.	.	kIx.
</s>
<s>
Vozy	vůz	k1gInPc1
</s>
<s>
Tatra	Tatra	k1gFnSc1
715-7	715-7	k4
CAS	CAS	kA
30	#num#	k4
9000	#num#	k4
<g/>
/	/	kIx~
<g/>
900	#num#	k4
-	-	kIx~
S3VH	S3VH	k1gFnSc2
Hasičského	hasičský	k2eAgInSc2d1
záchranného	záchranný	k2eAgInSc2d1
sboru	sbor	k1gInSc2
Moravskoslezského	moravskoslezský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
</s>
<s>
HZS	HZS	kA
ČR	ČR	kA
v	v	k7c6
současnosti	současnost	k1gFnSc6
<g/>
[	[	kIx(
<g/>
kdy	kdy	k6eAd1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
používá	používat	k5eAaImIp3nS
tyto	tento	k3xDgInPc4
typy	typ	k1gInPc4
vozů	vůz	k1gInPc2
<g/>
:	:	kIx,
</s>
<s>
CAS	CAS	kA
20	#num#	k4
Scania	Scanium	k1gNnSc2
</s>
<s>
CAS	CAS	kA
32	#num#	k4
T815	T815	k1gFnSc2
</s>
<s>
CAS	CAS	kA
32	#num#	k4
T148	T148	k1gFnSc2
</s>
<s>
CAS	CAS	kA
30	#num#	k4
T815-7	T815-7	k1gFnSc2
</s>
<s>
CAS	CAS	kA
27	#num#	k4
Dennis	Dennis	k1gFnSc2
</s>
<s>
CAS	CAS	kA
25	#num#	k4
T815-2	T815-2	k1gFnSc2
</s>
<s>
CAS	CAS	kA
24	#num#	k4
Liaz	liaz	k1gInSc1
101	#num#	k4
</s>
<s>
CAS	CAS	kA
20	#num#	k4
Man	mana	k1gFnPc2
</s>
<s>
CAS	CAS	kA
20	#num#	k4
Iveco	Iveco	k6eAd1
</s>
<s>
CAS	CAS	kA
20	#num#	k4
Tatra	Tatra	k1gFnSc1
TerrNo	TerrNo	k1gNnSc1
<g/>
.1	.1	k4
</s>
<s>
CAS	CAS	kA
16	#num#	k4
MB	MB	kA
Unimog	Unimog	k1gInSc1
<g/>
,	,	kIx,
<g/>
Renault	renault	k1gInSc1
Camiva	Camivo	k1gNnSc2
</s>
<s>
CAS	CAS	kA
15	#num#	k4
MB	MB	kA
Atego	Atego	k1gMnSc1
nebo	nebo	k8xC
Man	Man	k1gMnSc1
</s>
<s>
CAS	CAS	kA
20	#num#	k4
MB	MB	kA
Atego	Atego	k6eAd1
</s>
<s>
CAS	CAS	kA
16	#num#	k4
Daewoo	Daewoo	k6eAd1
Avia	Avia	k1gFnSc1
nebo	nebo	k8xC
Praga	Praga	k1gFnSc1
V3S	V3S	k1gFnSc2
</s>
<s>
TA	ten	k3xDgFnSc1
-	-	kIx~
L1	L1	k1gFnSc1
</s>
<s>
RZA	RZA	kA
-	-	kIx~
např.	např.	kA
VW	VW	kA
T	T	kA
<g/>
5	#num#	k4
<g/>
,	,	kIx,
MB	MB	kA
Sprinter	sprinter	k1gMnSc1
</s>
<s>
VEA	VEA	kA
</s>
<s>
KHA	KHA	kA
nebo	nebo	k8xC
PLHA	PLHA	kA
</s>
<s>
CAS	CAS	kA
24	#num#	k4
Renault	renault	k1gInSc1
Kerax	Kerax	k1gInSc4
</s>
<s>
AZ	AZ	kA
Iveco	Iveco	k1gMnSc1
Magirus	Magirus	k1gMnSc1
</s>
<s>
AZ	AZ	kA
Renault	renault	k1gInSc1
EPA	EPA	kA
30	#num#	k4
</s>
<s>
AZ	AZ	kA
30	#num#	k4
MB	MB	kA
Atego	Atego	k6eAd1
</s>
<s>
CAS	CAS	kA
25	#num#	k4
Škoda	škoda	k1gFnSc1
706	#num#	k4
RTHP	RTHP	kA
</s>
<s>
AV	AV	kA
14,16	14,16	k4
<g/>
,20	,20	k4
<g/>
,28	,28	k4
nebo	nebo	k8xC
30	#num#	k4
Tatra	Tatra	k1gFnSc1
815	#num#	k4
6	#num#	k4
<g/>
x	x	k?
<g/>
6	#num#	k4
nebo	nebo	k8xC
8	#num#	k4
<g/>
x	x	k?
<g/>
8	#num#	k4
</s>
<s>
Tank	tank	k1gInSc1
SPOT	spot	k1gInSc1
55	#num#	k4
</s>
<s>
AM-50	AM-50	k4
T	T	kA
813	#num#	k4
8	#num#	k4
<g/>
x	x	k?
<g/>
8	#num#	k4
</s>
<s>
VT-	VT-	k?
<g/>
55	#num#	k4
<g/>
,	,	kIx,
<g/>
VT-	VT-	k1gFnSc1
<g/>
72	#num#	k4
</s>
<s>
Nosič	nosič	k1gInSc1
kontejnerů	kontejner	k1gInPc2
Tatra	Tatra	k1gFnSc1
815	#num#	k4
8	#num#	k4
<g/>
x	x	k?
<g/>
8	#num#	k4
</s>
<s>
UDS	UDS	kA
214	#num#	k4
T	T	kA
815	#num#	k4
</s>
<s>
Těžký	těžký	k2eAgInSc1d1
terénní	terénní	k2eAgInSc1d1
tahač	tahač	k1gInSc1
přívěsů	přívěs	k1gInPc2
Tatra	Tatra	k1gFnSc1
815	#num#	k4
8	#num#	k4
<g/>
x	x	k?
<g/>
8	#num#	k4
</s>
<s>
podval	podval	k1gInSc1
P50	P50	k1gFnSc2
</s>
<s>
Jeřáby	jeřáb	k1gInPc1
různých	různý	k2eAgFnPc2d1
nosností	nosnost	k1gFnPc2
</s>
<s>
Klasifikace	klasifikace	k1gFnSc1
techniky	technika	k1gFnSc2
</s>
<s>
AJ	aj	kA
–	–	k?
Automobilový	automobilový	k2eAgInSc4d1
jeřáb	jeřáb	k1gInSc4
</s>
<s>
AP	ap	kA
–	–	k?
Automobilová	automobilový	k2eAgFnSc1d1
plošina	plošina	k1gFnSc1
</s>
<s>
AZ	AZ	kA
–	–	k?
Automobilový	automobilový	k2eAgInSc4d1
žebřík	žebřík	k1gInSc4
</s>
<s>
CAS	CAS	kA
–	–	k?
Cisternová	cisternový	k2eAgFnSc1d1
automobilová	automobilový	k2eAgFnSc1d1
stříkačka	stříkačka	k1gFnSc1
</s>
<s>
DA	DA	kA
–	–	k?
dopravní	dopravní	k2eAgInSc4d1
automobil	automobil	k1gInSc4
</s>
<s>
K	k	k7c3
–	–	k?
kontejner	kontejner	k1gInSc4
</s>
<s>
KHA	KHA	kA
–	–	k?
Kombinovaný	kombinovaný	k2eAgInSc4d1
hasicí	hasicí	k2eAgInSc4d1
automobil	automobil	k1gInSc4
</s>
<s>
LCAS	LCAS	kA
<g/>
/	/	kIx~
<g/>
LKHA	LKHA	kA
–	–	k?
Letištní	letištní	k2eAgInSc4d1
speciál	speciál	k1gInSc4
</s>
<s>
LP	LP	kA
–	–	k?
Lesní	lesní	k2eAgInSc4d1
speciál	speciál	k1gInSc4
</s>
<s>
PHA	pha	k0wR
–	–	k?
Pěnový	pěnový	k2eAgInSc4d1
automobil	automobil	k1gInSc4
</s>
<s>
PLHA	PLHA	kA
–	–	k?
Plynový	plynový	k2eAgInSc4d1
automobil	automobil	k1gInSc4
</s>
<s>
PRHA	prha	k1gFnSc1
–	–	k?
Práškový	práškový	k2eAgInSc4d1
automobil	automobil	k1gInSc4
</s>
<s>
PPLA	PPLA	kA
–	–	k?
Protiplynový	protiplynový	k2eAgInSc4d1
automobil	automobil	k1gInSc4
</s>
<s>
RZA	RZA	kA
–	–	k?
Rychlý	rychlý	k2eAgInSc4d1
zásahový	zásahový	k2eAgInSc4d1
automobil	automobil	k1gInSc4
</s>
<s>
TA	ten	k3xDgFnSc1
–	–	k?
Technický	technický	k2eAgInSc1d1
automobil	automobil	k1gInSc1
</s>
<s>
TACH	TACH	kA
–	–	k?
Chemický	chemický	k2eAgInSc4d1
automobil	automobil	k1gInSc4
</s>
<s>
V	v	k7c6
–	–	k?
Požární	požární	k2eAgInSc4d1
vlak	vlak	k1gInSc4
</s>
<s>
VA	va	k0wR
–	–	k?
Vyšetřovací	vyšetřovací	k2eAgInSc4d1
automobil	automobil	k1gInSc4
</s>
<s>
VEA	VEA	kA
–	–	k?
Velitelský	velitelský	k2eAgInSc4d1
automobil	automobil	k1gInSc4
</s>
<s>
VYA	VYA	kA
–	–	k?
Vyprošťovací	vyprošťovací	k2eAgInSc4d1
automobil	automobil	k1gInSc4
</s>
<s>
O	o	k7c6
–	–	k?
Ostatní	ostatní	k2eAgMnPc1d1
</s>
<s>
Služební	služební	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
</s>
<s>
Podle	podle	k7c2
vyhlášky	vyhláška	k1gFnSc2
Ministerstva	ministerstvo	k1gNnSc2
vnitra	vnitro	k1gNnSc2
č.	č.	k?
433	#num#	k4
<g/>
/	/	kIx~
<g/>
2004	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
uděluje	udělovat	k5eAaImIp3nS
Hasičský	hasičský	k2eAgInSc1d1
záchranný	záchranný	k2eAgInSc1d1
sbor	sbor	k1gInSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
svým	svůj	k3xOyFgMnPc3
příslušníkům	příslušník	k1gMnPc3
následující	následující	k2eAgFnPc4d1
tři	tři	k4xCgFnPc4
služební	služební	k2eAgFnPc4d1
medaile	medaile	k1gFnPc4
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Za	za	k7c4
statečnostZa	statečnostZus	k1gMnSc4
zásluhyo	zásluhyo	k6eAd1
bezpečnostZa	bezpečnostZ	k2eAgFnSc1d1
věrnost	věrnost	k1gFnSc1
<g/>
(	(	kIx(
<g/>
I.	I.	kA
<g/>
,	,	kIx,
II	II	kA
<g/>
.	.	kIx.
a	a	k8xC
III	III	kA
<g/>
.	.	kIx.
stupeň	stupeň	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Statistická	statistický	k2eAgFnSc1d1
ročenka	ročenka	k1gFnSc1
2018	#num#	k4
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
42	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Zákon	zákon	k1gInSc1
č.	č.	k?
320	#num#	k4
<g/>
/	/	kIx~
<g/>
2015	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
Hasičském	hasičský	k2eAgInSc6d1
záchranném	záchranný	k2eAgInSc6d1
sboru	sbor	k1gInSc6
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
a	a	k8xC
o	o	k7c6
změně	změna	k1gFnSc6
některých	některý	k3yIgInPc2
zákonů	zákon	k1gInPc2
(	(	kIx(
<g/>
zákon	zákon	k1gInSc1
o	o	k7c6
hasičském	hasičský	k2eAgInSc6d1
záchranném	záchranný	k2eAgInSc6d1
sboru	sbor	k1gInSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
§	§	k?
5	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Zákon	zákon	k1gInSc1
č.	č.	k?
320	#num#	k4
<g/>
/	/	kIx~
<g/>
2015	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
Hasičském	hasičský	k2eAgInSc6d1
záchranném	záchranný	k2eAgInSc6d1
sboru	sbor	k1gInSc6
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
a	a	k8xC
o	o	k7c6
změně	změna	k1gFnSc6
některých	některý	k3yIgInPc2
zákonů	zákon	k1gInPc2
(	(	kIx(
<g/>
zákon	zákon	k1gInSc1
o	o	k7c6
hasičském	hasičský	k2eAgInSc6d1
záchranném	záchranný	k2eAgInSc6d1
sboru	sbor	k1gInSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
§	§	k?
6	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Základní	základní	k2eAgFnPc4d1
informace	informace	k1gFnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hzscr	Hzscr	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
MV-generální	MV-generální	k2eAgNnSc1d1
ředitelství	ředitelství	k1gNnSc1
HZS	HZS	kA
ČR	ČR	kA
(	(	kIx(
<g/>
platnost	platnost	k1gFnSc4
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2019	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hzscr	Hzscr	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Zákon	zákon	k1gInSc1
č.	č.	k?
320	#num#	k4
<g/>
/	/	kIx~
<g/>
2015	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
Hasičském	hasičský	k2eAgInSc6d1
záchranném	záchranný	k2eAgInSc6d1
sboru	sbor	k1gInSc6
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
a	a	k8xC
o	o	k7c6
změně	změna	k1gFnSc6
některých	některý	k3yIgInPc2
zákonů	zákon	k1gInPc2
(	(	kIx(
<g/>
zákon	zákon	k1gInSc1
o	o	k7c6
hasičském	hasičský	k2eAgInSc6d1
záchranném	záchranný	k2eAgInSc6d1
sboru	sbor	k1gInSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
§	§	k?
7	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Zákon	zákon	k1gInSc1
č.	č.	k?
320	#num#	k4
<g/>
/	/	kIx~
<g/>
2015	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
Hasičském	hasičský	k2eAgInSc6d1
záchranném	záchranný	k2eAgInSc6d1
sboru	sbor	k1gInSc6
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
a	a	k8xC
o	o	k7c6
změně	změna	k1gFnSc6
některých	některý	k3yIgInPc2
zákonů	zákon	k1gInPc2
(	(	kIx(
<g/>
zákon	zákon	k1gInSc1
o	o	k7c6
hasičském	hasičský	k2eAgInSc6d1
záchranném	záchranný	k2eAgInSc6d1
sboru	sbor	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Zákon	zákon	k1gInSc1
č.	č.	k?
320	#num#	k4
<g/>
/	/	kIx~
<g/>
2015	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
Hasičském	hasičský	k2eAgInSc6d1
záchranném	záchranný	k2eAgInSc6d1
sboru	sbor	k1gInSc6
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
a	a	k8xC
o	o	k7c6
změně	změna	k1gFnSc6
některých	některý	k3yIgInPc2
zákonů	zákon	k1gInPc2
(	(	kIx(
<g/>
zákon	zákon	k1gInSc1
o	o	k7c6
hasičském	hasičský	k2eAgInSc6d1
záchranném	záchranný	k2eAgInSc6d1
sboru	sbor	k1gInSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
§	§	k?
8	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Působnost	působnost	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hzscr	Hzscr	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Zákon	zákon	k1gInSc1
č.	č.	k?
320	#num#	k4
<g/>
/	/	kIx~
<g/>
2015	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
Hasičském	hasičský	k2eAgInSc6d1
záchranném	záchranný	k2eAgInSc6d1
sboru	sbor	k1gInSc6
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
a	a	k8xC
o	o	k7c6
změně	změna	k1gFnSc6
některých	některý	k3yIgInPc2
zákonů	zákon	k1gInPc2
(	(	kIx(
<g/>
zákon	zákon	k1gInSc1
o	o	k7c6
hasičském	hasičský	k2eAgInSc6d1
záchranném	záchranný	k2eAgInSc6d1
sboru	sbor	k1gInSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
§	§	k?
9	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
Statistické	statistický	k2eAgFnSc2d1
ročenky	ročenka	k1gFnSc2
Hasičského	hasičský	k2eAgInSc2d1
záchranného	záchranný	k2eAgInSc2d1
sboru	sbor	k1gInSc2
ČR	ČR	kA
<g/>
,	,	kIx,
rok	rok	k1gInSc1
2017	#num#	k4
<g/>
↑	↑	k?
Vyhláška	vyhláška	k1gFnSc1
č.	č.	k?
433	#num#	k4
<g/>
/	/	kIx~
<g/>
2004	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
se	se	k3xPyFc4
stanoví	stanovit	k5eAaPmIp3nS
druh	druh	k1gInSc1
a	a	k8xC
vzor	vzor	k1gInSc1
služebních	služební	k2eAgFnPc2d1
medailí	medaile	k1gFnPc2
bezpečnostních	bezpečnostní	k2eAgInPc2d1
sborů	sbor	k1gInPc2
a	a	k8xC
důvody	důvod	k1gInPc1
pro	pro	k7c4
jejich	jejich	k3xOp3gNnSc4
udělení	udělení	k1gNnSc4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Dokumentace	dokumentace	k1gFnSc1
požářiště	požářiště	k1gNnSc2
</s>
<s>
Hodnosti	hodnost	k1gFnPc1
příslušníků	příslušník	k1gMnPc2
bezpečnostních	bezpečnostní	k2eAgInPc2d1
sborů	sbor	k1gInPc2
(	(	kIx(
<g/>
Česko	Česko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Sbor	sbor	k1gInSc1
dobrovolných	dobrovolný	k2eAgMnPc2d1
hasičů	hasič	k1gMnPc2
</s>
<s>
Jednotka	jednotka	k1gFnSc1
sboru	sbor	k1gInSc2
dobrovolných	dobrovolný	k2eAgMnPc2d1
hasičů	hasič	k1gMnPc2
</s>
<s>
Hasičský	hasičský	k2eAgInSc1d1
záchranný	záchranný	k2eAgInSc1d1
sbor	sbor	k1gInSc1
Dopravního	dopravní	k2eAgInSc2d1
podniku	podnik	k1gInSc2
hl.	hl.	k?
m.	m.	k?
Prahy	Praha	k1gFnSc2
</s>
<s>
Báňský	báňský	k2eAgMnSc1d1
záchranář	záchranář	k1gMnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Hasičský	hasičský	k2eAgInSc4d1
záchranný	záchranný	k2eAgInSc4d1
sbor	sbor	k1gInSc4
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Hasičský	hasičský	k2eAgInSc1d1
záchranný	záchranný	k2eAgInSc1d1
sbor	sbor	k1gInSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
,	,	kIx,
hzscr	hzscr	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Hasičský	hasičský	k2eAgInSc1d1
záchranný	záchranný	k2eAgInSc1d1
sbor	sbor	k1gInSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
na	na	k7c6
Facebooku	Facebook	k1gInSc6
</s>
<s>
Hasičský	hasičský	k2eAgInSc1d1
záchranný	záchranný	k2eAgInSc1d1
sbor	sbor	k1gInSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
na	na	k7c6
YouTube	YouTub	k1gInSc5
</s>
<s>
Zákon	zákon	k1gInSc1
č.	č.	k?
320	#num#	k4
<g/>
/	/	kIx~
<g/>
2015	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
Hasičském	hasičský	k2eAgInSc6d1
záchranném	záchranný	k2eAgInSc6d1
sboru	sbor	k1gInSc6
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
a	a	k8xC
o	o	k7c6
změně	změna	k1gFnSc6
některých	některý	k3yIgInPc2
zákonů	zákon	k1gInPc2
<g/>
,	,	kIx,
ve	v	k7c6
znění	znění	k1gNnSc6
pozdějších	pozdní	k2eAgInPc2d2
předpisů	předpis	k1gInPc2
<g/>
,	,	kIx,
zakonyprolidi	zakonyprolid	k1gMnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
|	|	kIx~
Sport	sport	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
kn	kn	k?
<g/>
20020808025	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
141196523	#num#	k4
</s>
