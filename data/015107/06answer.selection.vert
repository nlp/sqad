<s>
Hasičský	hasičský	k2eAgInSc1d1
záchranný	záchranný	k2eAgInSc1d1
sbor	sbor	k1gInSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
(	(	kIx(
<g/>
HZS	HZS	kA
ČR	ČR	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
bezpečnostní	bezpečnostní	k2eAgInSc1d1
sbor	sbor	k1gInSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc7
základním	základní	k2eAgInSc7d1
úkolem	úkol	k1gInSc7
je	být	k5eAaImIp3nS
chránit	chránit	k5eAaImF
životy	život	k1gInPc4
a	a	k8xC
zdraví	zdraví	k1gNnSc4
obyvatel	obyvatel	k1gMnPc2
<g/>
,	,	kIx,
životní	životní	k2eAgNnSc4d1
prostředí	prostředí	k1gNnSc4
<g/>
,	,	kIx,
zvířata	zvíře	k1gNnPc1
a	a	k8xC
majetek	majetek	k1gInSc1
před	před	k7c7
požáry	požár	k1gInPc7
a	a	k8xC
jinými	jiný	k2eAgFnPc7d1
mimořádnými	mimořádný	k2eAgFnPc7d1
událostmi	událost	k1gFnPc7
a	a	k8xC
krizovými	krizový	k2eAgFnPc7d1
situacemi	situace	k1gFnPc7
(	(	kIx(
<g/>
živelní	živelní	k2eAgFnPc1d1
pohromy	pohroma	k1gFnPc1
apod.	apod.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>