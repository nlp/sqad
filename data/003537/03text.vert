<s>
Chlorid	chlorid	k1gInSc1	chlorid
sodný	sodný	k2eAgInSc1d1	sodný
(	(	kIx(	(
<g/>
NaCl	NaCl	k1gInSc1	NaCl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známý	známý	k2eAgInSc1d1	známý
v	v	k7c6	v
běžném	běžný	k2eAgInSc6d1	běžný
životě	život	k1gInSc6	život
pod	pod	k7c7	pod
označením	označení	k1gNnSc7	označení
kuchyňská	kuchyňská	k1gFnSc1	kuchyňská
sůl	sůl	k1gFnSc1	sůl
a	a	k8xC	a
nejčastěji	často	k6eAd3	často
prostě	prostě	k9	prostě
sůl	sůl	k1gFnSc1	sůl
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
chemická	chemický	k2eAgFnSc1d1	chemická
sloučenina	sloučenina	k1gFnSc1	sloučenina
<g/>
,	,	kIx,	,
vyskytující	vyskytující	k2eAgFnSc7d1	vyskytující
se	se	k3xPyFc4	se
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
nerostu	nerost	k1gInSc2	nerost
halitu	halit	k1gInSc2	halit
<g/>
,	,	kIx,	,
známého	známý	k2eAgMnSc2d1	známý
též	též	k9	též
pod	pod	k7c7	pod
označením	označení	k1gNnSc7	označení
sůl	sůl	k1gFnSc1	sůl
kamenná	kamenný	k2eAgFnSc1d1	kamenná
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
velmi	velmi	k6eAd1	velmi
důležitá	důležitý	k2eAgFnSc1d1	důležitá
sloučenina	sloučenina	k1gFnSc1	sloučenina
potřebná	potřebný	k2eAgFnSc1d1	potřebná
pro	pro	k7c4	pro
životní	životní	k2eAgNnSc4d1	životní
funkce	funkce	k1gFnPc4	funkce
většiny	většina	k1gFnSc2	většina
organismů	organismus	k1gInPc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Krystalický	krystalický	k2eAgInSc1d1	krystalický
chlorid	chlorid	k1gInSc1	chlorid
sodný	sodný	k2eAgInSc1d1	sodný
je	být	k5eAaImIp3nS	být
bezbarvý	bezbarvý	k2eAgInSc1d1	bezbarvý
nebo	nebo	k8xC	nebo
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
,	,	kIx,	,
průhledný	průhledný	k2eAgInSc1d1	průhledný
<g/>
,	,	kIx,	,
skelně	skelně	k6eAd1	skelně
lesklý	lesklý	k2eAgMnSc1d1	lesklý
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
krychlovou	krychlový	k2eAgFnSc4d1	krychlová
odlučnost	odlučnost	k1gFnSc4	odlučnost
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
velké	velký	k2eAgFnSc3d1	velká
rozpustnosti	rozpustnost	k1gFnSc3	rozpustnost
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
je	být	k5eAaImIp3nS	být
většina	většina	k1gFnSc1	většina
chloridu	chlorid	k1gInSc2	chlorid
sodného	sodný	k2eAgMnSc2d1	sodný
přítomného	přítomný	k1gMnSc2	přítomný
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
obsažena	obsáhnout	k5eAaPmNgFnS	obsáhnout
v	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
salinita	salinita	k1gFnSc1	salinita
mořské	mořský	k2eAgFnSc2d1	mořská
vody	voda	k1gFnSc2	voda
činí	činit	k5eAaImIp3nS	činit
přibližně	přibližně	k6eAd1	přibližně
3,5	[number]	k4	3,5
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Koncentrace	koncentrace	k1gFnSc1	koncentrace
NaCl	NaCla	k1gFnPc2	NaCla
v	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
2,7	[number]	k4	2,7
%	%	kIx~	%
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
téměř	téměř	k6eAd1	téměř
80	[number]	k4	80
%	%	kIx~	%
soli	sůl	k1gFnPc1	sůl
obsažené	obsažený	k2eAgFnPc1d1	obsažená
v	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
tvoří	tvořit	k5eAaImIp3nS	tvořit
NaCl	NaCl	k1gInSc4	NaCl
<g/>
.	.	kIx.	.
</s>
<s>
Extrémním	extrémní	k2eAgInSc7d1	extrémní
příkladem	příklad	k1gInSc7	příklad
vysoce	vysoce	k6eAd1	vysoce
salinitních	salinitní	k2eAgFnPc2d1	salinitní
vod	voda	k1gFnPc2	voda
je	být	k5eAaImIp3nS	být
Mrtvé	mrtvý	k2eAgNnSc4d1	mrtvé
moře	moře	k1gNnSc4	moře
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
celkový	celkový	k2eAgInSc1d1	celkový
obsah	obsah	k1gInSc1	obsah
solí	sůl	k1gFnPc2	sůl
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
hodnot	hodnota	k1gFnPc2	hodnota
až	až	k6eAd1	až
35	[number]	k4	35
%	%	kIx~	%
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obsah	obsah	k1gInSc1	obsah
NaCl	NaClum	k1gNnPc2	NaClum
k	k	k7c3	k
jiným	jiný	k2eAgFnPc3d1	jiná
solím	sůl	k1gFnPc3	sůl
jen	jen	k9	jen
30	[number]	k4	30
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Pevný	pevný	k2eAgInSc4d1	pevný
<g/>
,	,	kIx,	,
krystalický	krystalický	k2eAgInSc4d1	krystalický
<g/>
,	,	kIx,	,
chlorid	chlorid	k1gInSc4	chlorid
sodný	sodný	k2eAgInSc4d1	sodný
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
geologické	geologický	k2eAgFnSc6d1	geologická
minulosti	minulost	k1gFnSc6	minulost
Země	zem	k1gFnSc2	zem
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
vysušení	vysušení	k1gNnSc3	vysušení
oddělené	oddělený	k2eAgFnSc2d1	oddělená
části	část	k1gFnSc2	část
moře	moře	k1gNnSc2	moře
nebo	nebo	k8xC	nebo
oceánu	oceán	k1gInSc2	oceán
a	a	k8xC	a
následnému	následný	k2eAgNnSc3d1	následné
překrytí	překrytí	k1gNnSc3	překrytí
vrstvy	vrstva	k1gFnSc2	vrstva
solí	solit	k5eAaImIp3nS	solit
jinými	jiný	k2eAgFnPc7d1	jiná
geologickými	geologický	k2eAgFnPc7d1	geologická
vrstvami	vrstva	k1gFnPc7	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
ložiska	ložisko	k1gNnPc1	ložisko
soli	sůl	k1gFnSc2	sůl
jsou	být	k5eAaImIp3nP	být
intenzivně	intenzivně	k6eAd1	intenzivně
těžena	těžit	k5eAaImNgFnS	těžit
např.	např.	kA	např.
v	v	k7c4	v
Polsku	Polska	k1gFnSc4	Polska
<g/>
,	,	kIx,	,
USA	USA	kA	USA
nebo	nebo	k8xC	nebo
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Chlorid	chlorid	k1gInSc1	chlorid
sodný	sodný	k2eAgInSc1d1	sodný
je	být	k5eAaImIp3nS	být
důležitá	důležitý	k2eAgFnSc1d1	důležitá
surovina	surovina	k1gFnSc1	surovina
pro	pro	k7c4	pro
potravinářský	potravinářský	k2eAgInSc4d1	potravinářský
a	a	k8xC	a
chemický	chemický	k2eAgInSc4d1	chemický
průmysl	průmysl	k1gInSc4	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
potravinářství	potravinářství	k1gNnSc6	potravinářství
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
běžné	běžný	k2eAgFnSc2d1	běžná
úpravy	úprava	k1gFnSc2	úprava
potravin	potravina	k1gFnPc2	potravina
(	(	kIx(	(
<g/>
a	a	k8xC	a
ochucení	ochucení	k1gNnSc4	ochucení
<g/>
)	)	kIx)	)
používá	používat	k5eAaImIp3nS	používat
při	při	k7c6	při
konzervaci	konzervace	k1gFnSc6	konzervace
masa	masa	k1gFnSc1	masa
nasolením	nasolení	k1gNnSc7	nasolení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
chemickém	chemický	k2eAgInSc6d1	chemický
průmyslu	průmysl	k1gInSc6	průmysl
je	být	k5eAaImIp3nS	být
surovinou	surovina	k1gFnSc7	surovina
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
sodíku	sodík	k1gInSc2	sodík
<g/>
,	,	kIx,	,
jedlé	jedlý	k2eAgFnSc2d1	jedlá
sody	soda	k1gFnSc2	soda
<g/>
,	,	kIx,	,
chlóru	chlór	k1gInSc2	chlór
<g/>
,	,	kIx,	,
kyseliny	kyselina	k1gFnSc2	kyselina
chlorovodíkové	chlorovodíkový	k2eAgFnSc2d1	chlorovodíková
a	a	k8xC	a
mnoha	mnoho	k4c2	mnoho
dalších	další	k2eAgFnPc2d1	další
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
další	další	k2eAgNnSc1d1	další
využití	využití	k1gNnSc1	využití
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
v	v	k7c6	v
mýdlovarnictví	mýdlovarnictví	k1gNnSc6	mýdlovarnictví
<g/>
,	,	kIx,	,
sklářství	sklářství	k1gNnSc6	sklářství
<g/>
,	,	kIx,	,
metalurgii	metalurgie	k1gFnSc6	metalurgie
a	a	k8xC	a
v	v	k7c6	v
papírenském	papírenský	k2eAgInSc6d1	papírenský
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
,	,	kIx,	,
či	či	k8xC	či
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
barev	barva	k1gFnPc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgNnSc1d1	významné
je	být	k5eAaImIp3nS	být
též	též	k9	též
jeho	jeho	k3xOp3gNnSc4	jeho
využití	využití	k1gNnSc4	využití
ve	v	k7c6	v
zdravotnictví	zdravotnictví	k1gNnSc6	zdravotnictví
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
0,9	[number]	k4	0,9
<g/>
%	%	kIx~	%
roztoku	roztok	k1gInSc2	roztok
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
fyziologický	fyziologický	k2eAgInSc1d1	fyziologický
roztok	roztok	k1gInSc1	roztok
<g/>
.	.	kIx.	.
</s>
<s>
Zásadní	zásadní	k2eAgInSc4d1	zásadní
význam	význam	k1gInSc4	význam
má	mít	k5eAaImIp3nS	mít
chlorid	chlorid	k1gInSc1	chlorid
sodný	sodný	k2eAgInSc1d1	sodný
v	v	k7c6	v
zimní	zimní	k2eAgFnSc6d1	zimní
údržbě	údržba	k1gFnSc6	údržba
komunikací	komunikace	k1gFnPc2	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Prakticky	prakticky	k6eAd1	prakticky
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
zemích	zem	k1gFnPc6	zem
EU	EU	kA	EU
je	být	k5eAaImIp3nS	být
naprosto	naprosto	k6eAd1	naprosto
převažujícím	převažující	k2eAgInSc7d1	převažující
posypovým	posypový	k2eAgInSc7d1	posypový
materiálem	materiál	k1gInSc7	materiál
<g/>
,	,	kIx,	,
a	a	k8xC	a
posypová	posypový	k2eAgFnSc1d1	posypová
sůl	sůl	k1gFnSc1	sůl
NaCl	NaCla	k1gFnPc2	NaCla
byla	být	k5eAaImAgFnS	být
například	například	k6eAd1	například
v	v	k7c6	v
ČR	ČR	kA	ČR
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
s	s	k7c7	s
cenou	cena	k1gFnSc7	cena
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
1	[number]	k4	1
700	[number]	k4	700
až	až	k9	až
2	[number]	k4	2
200	[number]	k4	200
Kč	Kč	kA	Kč
za	za	k7c4	za
tunu	tuna	k1gFnSc4	tuna
asi	asi	k9	asi
6	[number]	k4	6
<g/>
krát	krát	k6eAd1	krát
levnější	levný	k2eAgFnSc1d2	levnější
než	než	k8xS	než
druhá	druhý	k4xOgFnSc1	druhý
nejužívanější	užívaný	k2eAgFnSc1d3	nejužívanější
rozmrazovací	rozmrazovací	k2eAgFnSc1d1	rozmrazovací
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
chlorid	chlorid	k1gInSc1	chlorid
vápenatý	vápenatý	k2eAgInSc1d1	vápenatý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
zimní	zimní	k2eAgFnSc6d1	zimní
sezóně	sezóna	k1gFnSc6	sezóna
2000	[number]	k4	2000
<g/>
/	/	kIx~	/
<g/>
2001	[number]	k4	2001
na	na	k7c4	na
posyp	posyp	k1gInSc4	posyp
vozovek	vozovka	k1gFnPc2	vozovka
silniční	silniční	k2eAgFnSc2d1	silniční
sítě	síť	k1gFnSc2	síť
v	v	k7c6	v
ČR	ČR	kA	ČR
použito	použít	k5eAaPmNgNnS	použít
168	[number]	k4	168
000	[number]	k4	000
tun	tuna	k1gFnPc2	tuna
soli	sůl	k1gFnSc2	sůl
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
v	v	k7c4	v
98	[number]	k4	98
%	%	kIx~	%
se	se	k3xPyFc4	se
používal	používat	k5eAaImAgInS	používat
chlorid	chlorid	k1gInSc1	chlorid
sodný	sodný	k2eAgInSc1d1	sodný
<g/>
,	,	kIx,	,
ve	v	k7c6	v
zbylém	zbylý	k2eAgInSc6d1	zbylý
podílu	podíl	k1gInSc6	podíl
chlorid	chlorid	k1gInSc4	chlorid
vápenatý	vápenatý	k2eAgInSc1d1	vápenatý
a	a	k8xC	a
jen	jen	k9	jen
v	v	k7c6	v
nepatrném	nepatrný	k2eAgInSc6d1	nepatrný
rozsahu	rozsah	k1gInSc6	rozsah
chlorid	chlorid	k1gInSc1	chlorid
hořečnatý	hořečnatý	k2eAgInSc1d1	hořečnatý
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Eutektický	eutektický	k2eAgInSc4d1	eutektický
bod	bod	k1gInSc4	bod
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
teplota	teplota	k1gFnSc1	teplota
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yQgFnSc2	který
má	mít	k5eAaImIp3nS	mít
sůl	sůl	k1gFnSc4	sůl
rozmrazovací	rozmrazovací	k2eAgInSc4d1	rozmrazovací
účinek	účinek	k1gInSc4	účinek
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
chlorid	chlorid	k1gInSc4	chlorid
sodný	sodný	k2eAgInSc4d1	sodný
ve	v	k7c6	v
vodném	vodný	k2eAgInSc6d1	vodný
roztoku	roztok	k1gInSc6	roztok
s	s	k7c7	s
ideální	ideální	k2eAgFnSc7d1	ideální
koncentrací	koncentrace	k1gFnSc7	koncentrace
cca	cca	kA	cca
22	[number]	k4	22
%	%	kIx~	%
asi	asi	k9	asi
−	−	k?	−
°	°	k?	°
<g/>
C.	C.	kA	C.
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
však	však	k9	však
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
ideální	ideální	k2eAgFnPc4d1	ideální
koncentrace	koncentrace	k1gFnPc4	koncentrace
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
ředí	ředit	k5eAaImIp3nP	ředit
vodou	voda	k1gFnSc7	voda
z	z	k7c2	z
rozmrazeného	rozmrazený	k2eAgInSc2d1	rozmrazený
povrchu	povrch	k1gInSc2	povrch
<g/>
,	,	kIx,	,
padajícím	padající	k2eAgInSc7d1	padající
sněhem	sníh	k1gInSc7	sníh
apod.	apod.	kA	apod.
<g/>
;	;	kIx,	;
proto	proto	k8xC	proto
hranice	hranice	k1gFnSc1	hranice
skutečné	skutečný	k2eAgFnSc2d1	skutečná
účinnosti	účinnost	k1gFnSc2	účinnost
leží	ležet	k5eAaImIp3nS	ležet
výše	výše	k1gFnSc1	výše
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
běžné	běžný	k2eAgFnPc4d1	běžná
potřeby	potřeba	k1gFnPc4	potřeba
zimního	zimní	k2eAgNnSc2d1	zimní
ošetřování	ošetřování	k1gNnSc2	ošetřování
komunikací	komunikace	k1gFnPc2	komunikace
účinkuje	účinkovat	k5eAaImIp3nS	účinkovat
kuchyňská	kuchyňský	k2eAgFnSc1d1	kuchyňská
sůl	sůl	k1gFnSc1	sůl
do	do	k7c2	do
teploty	teplota	k1gFnSc2	teplota
−	−	k?	−
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
maximálně	maximálně	k6eAd1	maximálně
−	−	k?	−
°	°	k?	°
<g/>
C.	C.	kA	C.
Při	při	k7c6	při
teplotách	teplota	k1gFnPc6	teplota
pod	pod	k7c4	pod
−	−	k?	−
°	°	k?	°
<g/>
C	C	kA	C
již	již	k6eAd1	již
bývá	bývat	k5eAaImIp3nS	bývat
zcela	zcela	k6eAd1	zcela
neúčinná	účinný	k2eNgFnSc1d1	neúčinná
(	(	kIx(	(
<g/>
chlorid	chlorid	k1gInSc1	chlorid
vápenatý	vápenatý	k2eAgInSc1d1	vápenatý
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
účinný	účinný	k2eAgInSc1d1	účinný
až	až	k9	až
do	do	k7c2	do
−	−	k?	−
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
při	při	k7c6	při
velkých	velký	k2eAgInPc6d1	velký
mrazech	mráz	k1gInPc6	mráz
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
zpravidla	zpravidla	k6eAd1	zpravidla
jen	jen	k9	jen
jako	jako	k9	jako
příměs	příměs	k1gFnSc1	příměs
chloridu	chlorid	k1gInSc2	chlorid
sodného	sodný	k2eAgInSc2d1	sodný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kuchyňská	kuchyňský	k2eAgFnSc1d1	kuchyňská
sůl	sůl	k1gFnSc1	sůl
normálně	normálně	k6eAd1	normálně
prodávaná	prodávaný	k2eAgFnSc1d1	prodávaná
v	v	k7c6	v
běžných	běžný	k2eAgInPc6d1	běžný
obchodech	obchod	k1gInPc6	obchod
s	s	k7c7	s
potravinami	potravina	k1gFnPc7	potravina
bývá	bývat	k5eAaImIp3nS	bývat
ze	z	k7c2	z
zdravotních	zdravotní	k2eAgInPc2d1	zdravotní
důvodů	důvod	k1gInPc2	důvod
jodizovaná	jodizovaný	k2eAgFnSc1d1	jodizovaná
–	–	k?	–
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
přidáno	přidán	k2eAgNnSc1d1	Přidáno
malé	malý	k2eAgNnSc1d1	malé
množství	množství	k1gNnSc1	množství
jódu	jód	k1gInSc2	jód
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
jodidu	jodid	k1gInSc2	jodid
draselného	draselný	k2eAgInSc2d1	draselný
nebo	nebo	k8xC	nebo
jodičnanu	jodičnan	k1gInSc2	jodičnan
draselného	draselný	k2eAgInSc2d1	draselný
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tím	ten	k3xDgNnSc7	ten
zabezpečeno	zabezpečit	k5eAaPmNgNnS	zabezpečit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
populaci	populace	k1gFnSc6	populace
nevzniká	vznikat	k5eNaImIp3nS	vznikat
deficit	deficit	k1gInSc4	deficit
jódu	jód	k1gInSc2	jód
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
příčinou	příčina	k1gFnSc7	příčina
vleklých	vleklý	k2eAgFnPc2d1	vleklá
zdravotních	zdravotní	k2eAgFnPc2d1	zdravotní
poruch	porucha	k1gFnPc2	porucha
či	či	k8xC	či
nemocí	nemoc	k1gFnPc2	nemoc
–	–	k?	–
především	především	k9	především
štítné	štítný	k2eAgFnSc2d1	štítná
žlázy	žláza	k1gFnSc2	žláza
<g/>
.	.	kIx.	.
</s>
<s>
Nadměrná	nadměrný	k2eAgFnSc1d1	nadměrná
konzumace	konzumace	k1gFnSc1	konzumace
soli	sůl	k1gFnSc2	sůl
ale	ale	k8xC	ale
škodí	škodit	k5eAaImIp3nS	škodit
zdraví	zdraví	k1gNnSc4	zdraví
<g/>
.	.	kIx.	.
</s>
<s>
Doporučená	doporučený	k2eAgFnSc1d1	Doporučená
denní	denní	k2eAgFnSc1d1	denní
dávka	dávka	k1gFnSc1	dávka
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
5	[number]	k4	5
<g/>
g	g	kA	g
soli	sůl	k1gFnSc2	sůl
(	(	kIx(	(
<g/>
cca	cca	kA	cca
2	[number]	k4	2
<g/>
g	g	kA	g
sodíku	sodík	k1gInSc2	sodík
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
VOHLÍDAL	VOHLÍDAL	kA	VOHLÍDAL
<g/>
,	,	kIx,	,
JIŘÍ	Jiří	k1gMnSc1	Jiří
<g/>
;	;	kIx,	;
ŠTULÍK	štulík	k1gInSc1	štulík
<g/>
,	,	kIx,	,
KAREL	Karel	k1gMnSc1	Karel
<g/>
;	;	kIx,	;
JULÁK	JULÁK	kA	JULÁK
<g/>
,	,	kIx,	,
ALOIS	Alois	k1gMnSc1	Alois
<g/>
.	.	kIx.	.
</s>
<s>
Chemické	chemický	k2eAgFnPc1d1	chemická
a	a	k8xC	a
analytické	analytický	k2eAgFnPc1d1	analytická
tabulky	tabulka	k1gFnPc1	tabulka
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Grada	Grada	k1gFnSc1	Grada
Publishing	Publishing	k1gInSc1	Publishing
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7169	[number]	k4	7169
<g/>
-	-	kIx~	-
<g/>
855	[number]	k4	855
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Slánka	slánka	k1gFnSc1	slánka
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
chlorid	chlorid	k1gInSc1	chlorid
sodný	sodný	k2eAgMnSc1d1	sodný
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
