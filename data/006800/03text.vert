<s>
Poděbrady	Poděbrady	k1gInPc1	Poděbrady
jsou	být	k5eAaImIp3nP	být
lázeňské	lázeňský	k2eAgNnSc4d1	lázeňské
město	město	k1gNnSc4	město
ve	v	k7c6	v
Středočeském	středočeský	k2eAgInSc6d1	středočeský
kraji	kraj	k1gInSc6	kraj
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Nymburk	Nymburk	k1gInSc1	Nymburk
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
úrodné	úrodný	k2eAgFnSc6d1	úrodná
polabské	polabský	k2eAgFnSc6d1	Polabská
nížině	nížina	k1gFnSc6	nížina
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Labi	Labe	k1gNnSc6	Labe
<g/>
,	,	kIx,	,
necelých	celý	k2eNgFnPc2d1	necelá
50	[number]	k4	50
km	km	kA	km
východně	východně	k6eAd1	východně
od	od	k7c2	od
Prahy	Praha	k1gFnSc2	Praha
u	u	k7c2	u
dálnice	dálnice	k1gFnSc2	dálnice
D	D	kA	D
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
184	[number]	k4	184
<g/>
–	–	k?	–
<g/>
190	[number]	k4	190
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
přes	přes	k7c4	přes
14	[number]	k4	14
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Výměra	výměra	k1gFnSc1	výměra
katastru	katastr	k1gInSc2	katastr
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
33,70	[number]	k4	33,70
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
místě	místo	k1gNnSc6	místo
dnešního	dnešní	k2eAgNnSc2d1	dnešní
města	město	k1gNnSc2	město
bylo	být	k5eAaImAgNnS	být
nalezeno	naleznout	k5eAaPmNgNnS	naleznout
osídlení	osídlení	k1gNnSc1	osídlení
už	už	k6eAd1	už
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
mladého	mladý	k2eAgInSc2d1	mladý
paleolitu	paleolit	k1gInSc2	paleolit
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
zde	zde	k6eAd1	zde
v	v	k7c6	v
letech	let	k1gInPc6	let
1262	[number]	k4	1262
<g/>
–	–	k?	–
<g/>
1268	[number]	k4	1268
zbudoval	zbudovat	k5eAaPmAgInS	zbudovat
kamenný	kamenný	k2eAgInSc1d1	kamenný
hrad	hrad	k1gInSc1	hrad
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
přebudovaný	přebudovaný	k2eAgInSc1d1	přebudovaný
v	v	k7c4	v
dnešní	dnešní	k2eAgInSc4d1	dnešní
zámek	zámek	k1gInSc4	zámek
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
velkému	velký	k2eAgInSc3d1	velký
rozkvětu	rozkvět	k1gInSc3	rozkvět
panství	panství	k1gNnSc2	panství
došlo	dojít	k5eAaPmAgNnS	dojít
za	za	k7c2	za
pánů	pan	k1gMnPc2	pan
z	z	k7c2	z
Kunštátu	Kunštát	k1gInSc2	Kunštát
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgMnSc1d3	nejznámější
představitel	představitel	k1gMnSc1	představitel
tohoto	tento	k3xDgInSc2	tento
rodu	rod	k1gInSc2	rod
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
Jiří	Jiří	k1gMnSc1	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
synové	syn	k1gMnPc1	syn
Poděbrady	Poděbrady	k1gInPc1	Poděbrady
roku	rok	k1gInSc2	rok
1472	[number]	k4	1472
povýšili	povýšit	k5eAaPmAgMnP	povýšit
na	na	k7c4	na
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1495	[number]	k4	1495
<g/>
–	–	k?	–
<g/>
1839	[number]	k4	1839
bylo	být	k5eAaImAgNnS	být
poděbradské	poděbradský	k2eAgNnSc1d1	Poděbradské
panství	panství	k1gNnSc1	panství
spravováno	spravovat	k5eAaImNgNnS	spravovat
královskou	královský	k2eAgFnSc7d1	královská
komorou	komora	k1gFnSc7	komora
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
ho	on	k3xPp3gNnSc4	on
koupil	koupit	k5eAaPmAgMnS	koupit
vídeňský	vídeňský	k2eAgMnSc1d1	vídeňský
bankéř	bankéř	k1gMnSc1	bankéř
Jiří	Jiří	k1gMnSc1	Jiří
Sina	sino	k1gNnSc2	sino
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
nádvoří	nádvoří	k1gNnSc6	nádvoří
zámku	zámek	k1gInSc2	zámek
navrtán	navrtán	k2eAgInSc4d1	navrtán
minerální	minerální	k2eAgInSc4d1	minerální
pramen	pramen	k1gInSc4	pramen
a	a	k8xC	a
v	v	k7c6	v
následujících	následující	k2eAgFnPc6d1	následující
dekádách	dekáda	k1gFnPc6	dekáda
se	se	k3xPyFc4	se
z	z	k7c2	z
města	město	k1gNnSc2	město
staly	stát	k5eAaPmAgFnP	stát
známé	známý	k2eAgFnPc1d1	známá
lázně	lázeň	k1gFnPc1	lázeň
<g/>
,	,	kIx,	,
specializované	specializovaný	k2eAgInPc1d1	specializovaný
zejména	zejména	k9	zejména
na	na	k7c4	na
léčbu	léčba	k1gFnSc4	léčba
nemocí	nemoc	k1gFnSc7	nemoc
srdce	srdce	k1gNnSc2	srdce
a	a	k8xC	a
oběhového	oběhový	k2eAgNnSc2d1	oběhové
ústrojí	ústrojí	k1gNnSc2	ústrojí
<g/>
.	.	kIx.	.
</s>
<s>
Zdejší	zdejší	k2eAgFnSc1d1	zdejší
městská	městský	k2eAgFnSc1d1	městská
památková	památkový	k2eAgFnSc1d1	památková
zóna	zóna	k1gFnSc1	zóna
<g/>
,	,	kIx,	,
vyhlášená	vyhlášený	k2eAgFnSc1d1	vyhlášená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
chrání	chránit	k5eAaImIp3nS	chránit
jak	jak	k6eAd1	jak
historické	historický	k2eAgNnSc1d1	historické
jádro	jádro	k1gNnSc1	jádro
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
přilehlou	přilehlý	k2eAgFnSc4d1	přilehlá
lázeňskou	lázeňský	k2eAgFnSc4d1	lázeňská
čtvrť	čtvrť	k1gFnSc4	čtvrť
<g/>
.	.	kIx.	.
</s>
<s>
Poděbrady	Poděbrady	k1gInPc1	Poděbrady
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
návštěvníkům	návštěvník	k1gMnPc3	návštěvník
především	především	k9	především
jako	jako	k8xC	jako
klidné	klidný	k2eAgNnSc1d1	klidné
lázeňské	lázeňský	k2eAgNnSc1d1	lázeňské
město	město	k1gNnSc1	město
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
památkami	památka	k1gFnPc7	památka
<g/>
.	.	kIx.	.
</s>
<s>
Příhodná	příhodný	k2eAgFnSc1d1	příhodná
poloha	poloha	k1gFnSc1	poloha
dnešního	dnešní	k2eAgNnSc2d1	dnešní
města	město	k1gNnSc2	město
na	na	k7c6	na
návrší	návrší	k1gNnSc6	návrší
nad	nad	k7c7	nad
řekou	řeka	k1gFnSc7	řeka
byla	být	k5eAaImAgFnS	být
využívána	využívat	k5eAaPmNgFnS	využívat
už	už	k6eAd1	už
zemědělci	zemědělec	k1gMnPc7	zemědělec
v	v	k7c6	v
mladém	mladý	k2eAgInSc6d1	mladý
paleolitu	paleolit	k1gInSc6	paleolit
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgNnSc1d1	významné
bylo	být	k5eAaImAgNnS	být
i	i	k9	i
osídlení	osídlení	k1gNnSc4	osídlení
doby	doba	k1gFnSc2	doba
bronzové	bronzový	k2eAgInPc1d1	bronzový
<g/>
,	,	kIx,	,
sídelní	sídelní	k2eAgInPc1d1	sídelní
a	a	k8xC	a
hrobové	hrobový	k2eAgInPc1d1	hrobový
nálezy	nález	k1gInPc1	nález
patří	patřit	k5eAaImIp3nP	patřit
kulturám	kultura	k1gFnPc3	kultura
mohylové	mohylový	k2eAgFnSc2d1	Mohylová
a	a	k8xC	a
lužické	lužický	k2eAgFnSc2d1	Lužická
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Husovy	Husův	k2eAgFnSc2d1	Husova
ulice	ulice	k1gFnSc2	ulice
byly	být	k5eAaImAgInP	být
nalezeny	nalezen	k2eAgInPc1d1	nalezen
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
osídlení	osídlení	k1gNnPc2	osídlení
z	z	k7c2	z
konce	konec	k1gInSc2	konec
starší	starý	k2eAgFnSc2d2	starší
doby	doba	k1gFnSc2	doba
železné	železný	k2eAgFnSc2d1	železná
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgFnPc1d1	významná
jsou	být	k5eAaImIp3nP	být
i	i	k8xC	i
části	část	k1gFnPc1	část
sídel	sídlo	k1gNnPc2	sídlo
a	a	k8xC	a
vrstvy	vrstva	k1gFnSc2	vrstva
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
laténské	laténský	k2eAgFnSc2d1	laténská
a	a	k8xC	a
především	především	k9	především
pozůstatky	pozůstatek	k1gInPc7	pozůstatek
rozsáhlého	rozsáhlý	k2eAgNnSc2d1	rozsáhlé
sídliště	sídliště	k1gNnSc2	sídliště
ze	z	k7c2	z
starší	starý	k2eAgFnSc2d2	starší
i	i	k8xC	i
mladší	mladý	k2eAgFnSc2d2	mladší
doby	doba	k1gFnSc2	doba
římské	římský	k2eAgFnSc2d1	římská
<g/>
,	,	kIx,	,
nalezené	nalezený	k2eAgFnSc2d1	nalezená
na	na	k7c6	na
dnešním	dnešní	k2eAgInSc6d1	dnešní
Jiřího	Jiří	k1gMnSc2	Jiří
náměstí	náměstí	k1gNnSc4	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
také	také	k9	také
k	k	k7c3	k
četným	četný	k2eAgInPc3d1	četný
nálezům	nález	k1gInPc3	nález
římských	římský	k2eAgFnPc2d1	římská
mincí	mince	k1gFnPc2	mince
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
místě	místo	k1gNnSc6	místo
dnešních	dnešní	k2eAgInPc2d1	dnešní
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
vedla	vést	k5eAaImAgFnS	vést
v	v	k7c6	v
raném	raný	k2eAgInSc6d1	raný
středověku	středověk	k1gInSc6	středověk
důležitá	důležitý	k2eAgFnSc1d1	důležitá
obchodní	obchodní	k2eAgFnSc1d1	obchodní
trasa	trasa	k1gFnSc1	trasa
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Kladská	kladský	k2eAgFnSc1d1	Kladská
cesta	cesta	k1gFnSc1	cesta
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
zdejší	zdejší	k2eAgNnSc4d1	zdejší
osídlení	osídlení	k1gNnSc4	osídlení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
dnešního	dnešní	k2eAgNnSc2d1	dnešní
města	město	k1gNnSc2	město
pak	pak	k6eAd1	pak
bylo	být	k5eAaImAgNnS	být
několik	několik	k4yIc1	několik
dalších	další	k2eAgNnPc2d1	další
významných	významný	k2eAgNnPc2d1	významné
sídel	sídlo	k1gNnPc2	sídlo
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
hrad	hrad	k1gInSc1	hrad
Oldříš	Oldříš	k1gFnSc2	Oldříš
<g/>
,	,	kIx,	,
Sadská	sadský	k2eAgFnSc1d1	Sadská
či	či	k8xC	či
slavníkovská	slavníkovský	k2eAgFnSc1d1	slavníkovská
Libice	Libice	k1gFnPc1	Libice
<g/>
.	.	kIx.	.
</s>
<s>
Kladská	kladský	k2eAgFnSc1d1	Kladská
cesta	cesta	k1gFnSc1	cesta
<g/>
,	,	kIx,	,
vedoucí	vedoucí	k1gMnSc1	vedoucí
od	od	k7c2	od
Prahy	Praha	k1gFnSc2	Praha
a	a	k8xC	a
Sadské	sadský	k2eAgInPc1d1	sadský
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
Hradci	Hradec	k1gInSc3	Hradec
Králové	Králová	k1gFnSc2	Králová
a	a	k8xC	a
pokračující	pokračující	k2eAgFnPc4d1	pokračující
až	až	k9	až
do	do	k7c2	do
Slezska	Slezsko	k1gNnSc2	Slezsko
a	a	k8xC	a
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
zdejším	zdejší	k2eAgInSc7d1	zdejší
brodem	brod	k1gInSc7	brod
(	(	kIx(	(
<g/>
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
mezi	mezi	k7c7	mezi
dnešní	dnešní	k2eAgFnSc7d1	dnešní
ulicí	ulice	k1gFnSc7	ulice
Na	na	k7c6	na
Vinici	vinice	k1gFnSc6	vinice
a	a	k8xC	a
mostem	most	k1gInSc7	most
<g/>
)	)	kIx)	)
překračovala	překračovat	k5eAaImAgFnS	překračovat
řeku	řeka	k1gFnSc4	řeka
Labe	Labe	k1gNnSc2	Labe
<g/>
.	.	kIx.	.
</s>
<s>
Poblíž	poblíž	k7c2	poblíž
tohoto	tento	k3xDgInSc2	tento
brodu	brod	k1gInSc2	brod
stávalo	stávat	k5eAaImAgNnS	stávat
hradiště	hradiště	k1gNnSc1	hradiště
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
raně	raně	k6eAd1	raně
středověké	středověký	k2eAgNnSc1d1	středověké
slovanské	slovanský	k2eAgNnSc1d1	slovanské
osídlení	osídlení	k1gNnSc1	osídlení
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
nacházelo	nacházet	k5eAaImAgNnS	nacházet
přibližně	přibližně	k6eAd1	přibližně
2	[number]	k4	2
km	km	kA	km
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
od	od	k7c2	od
dnešního	dnešní	k2eAgNnSc2d1	dnešní
centra	centrum	k1gNnSc2	centrum
<g/>
,	,	kIx,	,
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
dnešní	dnešní	k2eAgFnSc2d1	dnešní
radiostanice	radiostanice	k1gFnSc2	radiostanice
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
spojován	spojovat	k5eAaImNgMnS	spojovat
s	s	k7c7	s
polohou	poloha	k1gFnSc7	poloha
dřívějšího	dřívější	k2eAgNnSc2d1	dřívější
osídlení	osídlení	k1gNnSc2	osídlení
"	"	kIx"	"
<g/>
pode	pod	k7c4	pod
brody	brod	k1gInPc4	brod
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
ale	ale	k9	ale
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yIgNnSc3	který
konkrétnímu	konkrétní	k2eAgNnSc3d1	konkrétní
sídlu	sídlo	k1gNnSc3	sídlo
se	se	k3xPyFc4	se
vztahovalo	vztahovat	k5eAaImAgNnS	vztahovat
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
12	[number]	k4	12
<g/>
.	.	kIx.	.
a	a	k8xC	a
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
nové	nový	k2eAgNnSc4d1	nové
šlechtické	šlechtický	k2eAgNnSc4d1	šlechtické
sídlo	sídlo	k1gNnSc4	sídlo
Poděbrady	Poděbrady	k1gInPc1	Poděbrady
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterém	který	k3yIgInSc6	který
je	být	k5eAaImIp3nS	být
první	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1223	[number]	k4	1223
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
zmiňován	zmiňován	k2eAgInSc1d1	zmiňován
Hroznata	Hrozne	k1gNnPc4	Hrozne
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Hroznatova	Hroznatův	k2eAgMnSc2d1	Hroznatův
syna	syn	k1gMnSc2	syn
Viléma	Vilém	k1gMnSc2	Vilém
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
roku	rok	k1gInSc2	rok
1262	[number]	k4	1262
sídlo	sídlo	k1gNnSc4	sídlo
získal	získat	k5eAaPmAgMnS	získat
jako	jako	k8xS	jako
odúmrť	odúmrť	k1gFnSc4	odúmrť
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
v	v	k7c6	v
letech	let	k1gInPc6	let
1262	[number]	k4	1262
<g/>
–	–	k?	–
<g/>
1268	[number]	k4	1268
nechal	nechat	k5eAaPmAgMnS	nechat
na	na	k7c6	na
opukovém	opukový	k2eAgNnSc6d1	opukové
návrší	návrší	k1gNnSc6	návrší
nad	nad	k7c7	nad
řekou	řeka	k1gFnSc7	řeka
postavit	postavit	k5eAaPmF	postavit
kamenný	kamenný	k2eAgInSc1d1	kamenný
vodní	vodní	k2eAgInSc1d1	vodní
hrad	hrad	k1gInSc1	hrad
–	–	k?	–
dnešní	dnešní	k2eAgInSc1d1	dnešní
Zámek	zámek	k1gInSc1	zámek
Poděbrady	Poděbrady	k1gInPc1	Poděbrady
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc1d1	původní
osada	osada	k1gFnSc1	osada
poté	poté	k6eAd1	poté
postupně	postupně	k6eAd1	postupně
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
lidé	člověk	k1gMnPc1	člověk
přesídlili	přesídlit	k5eAaPmAgMnP	přesídlit
do	do	k7c2	do
blízkosti	blízkost	k1gFnSc2	blízkost
hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Otakarově	Otakarův	k2eAgFnSc6d1	Otakarova
době	doba	k1gFnSc6	doba
byl	být	k5eAaImAgInS	být
při	při	k7c6	při
původním	původní	k2eAgInSc6d1	původní
vchodu	vchod	k1gInSc6	vchod
do	do	k7c2	do
hradu	hrad	k1gInSc2	hrad
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
východní	východní	k2eAgFnSc6d1	východní
straně	strana	k1gFnSc6	strana
vystavěn	vystavěn	k2eAgInSc4d1	vystavěn
gotický	gotický	k2eAgInSc4d1	gotický
kostel	kostel	k1gInSc4	kostel
Povýšení	povýšení	k1gNnSc2	povýšení
svatého	svatý	k2eAgMnSc2d1	svatý
Kříže	Kříž	k1gMnSc2	Kříž
a	a	k8xC	a
právě	právě	k9	právě
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
okolí	okolí	k1gNnSc6	okolí
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
nová	nový	k2eAgFnSc1d1	nová
ves	ves	k1gFnSc1	ves
<g/>
,	,	kIx,	,
písemně	písemně	k6eAd1	písemně
poprvé	poprvé	k6eAd1	poprvé
jmenovaná	jmenovaný	k2eAgFnSc1d1	jmenovaná
roku	rok	k1gInSc2	rok
1345	[number]	k4	1345
<g/>
.	.	kIx.	.
</s>
<s>
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
i	i	k9	i
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Václav	Václav	k1gMnSc1	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
si	se	k3xPyFc3	se
Poděbrady	Poděbrady	k1gInPc4	Poděbrady
oblíbili	oblíbit	k5eAaPmAgMnP	oblíbit
a	a	k8xC	a
často	často	k6eAd1	často
zde	zde	k6eAd1	zde
pobývali	pobývat	k5eAaImAgMnP	pobývat
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
krále	král	k1gMnSc2	král
Jana	Jan	k1gMnSc2	Jan
Lucemburského	lucemburský	k2eAgNnSc2d1	lucemburské
panství	panství	k1gNnSc2	panství
ztratilo	ztratit	k5eAaPmAgNnS	ztratit
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
významu	význam	k1gInSc6	význam
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
několikrát	několikrát	k6eAd1	několikrát
zastaveno	zastavit	k5eAaPmNgNnS	zastavit
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
naposledy	naposledy	k6eAd1	naposledy
Hynku	Hynek	k1gMnSc3	Hynek
z	z	k7c2	z
Lichtenburka	Lichtenburek	k1gMnSc2	Lichtenburek
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ho	on	k3xPp3gMnSc4	on
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c6	v
manství	manství	k1gNnSc6	manství
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
ho	on	k3xPp3gInSc4	on
získala	získat	k5eAaPmAgFnS	získat
Hynkova	Hynkův	k2eAgFnSc1d1	Hynkova
dcera	dcera	k1gFnSc1	dcera
Eliška	Eliška	k1gFnSc1	Eliška
<g/>
,	,	kIx,	,
manželka	manželka	k1gFnSc1	manželka
Bočka	Boček	k1gMnSc2	Boček
z	z	k7c2	z
Kunštátu	Kunštát	k1gInSc2	Kunštát
<g/>
.	.	kIx.	.
</s>
<s>
Největšího	veliký	k2eAgInSc2d3	veliký
rozkvětu	rozkvět	k1gInSc2	rozkvět
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
panství	panství	k1gNnSc1	panství
právě	právě	k9	právě
za	za	k7c2	za
pánů	pan	k1gMnPc2	pan
z	z	k7c2	z
Kunštátu	Kunštát	k1gInSc2	Kunštát
a	a	k8xC	a
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
tu	tu	k6eAd1	tu
vládli	vládnout	k5eAaImAgMnP	vládnout
v	v	k7c6	v
letech	let	k1gInPc6	let
1347	[number]	k4	1347
<g/>
–	–	k?	–
<g/>
1495	[number]	k4	1495
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
přebudovali	přebudovat	k5eAaPmAgMnP	přebudovat
hrad	hrad	k1gInSc4	hrad
(	(	kIx(	(
<g/>
ten	ten	k3xDgMnSc1	ten
pak	pak	k6eAd1	pak
odolal	odolat	k5eAaPmAgInS	odolat
třem	tři	k4xCgFnPc3	tři
obléháním	obléhání	k1gNnSc7	obléhání
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1402	[number]	k4	1402
<g/>
,	,	kIx,	,
1420	[number]	k4	1420
a	a	k8xC	a
1426	[number]	k4	1426
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
západní	západní	k2eAgFnSc6d1	západní
straně	strana	k1gFnSc6	strana
založili	založit	k5eAaPmAgMnP	založit
nové	nový	k2eAgNnSc4d1	nové
městečko	městečko	k1gNnSc4	městečko
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
náměstím	náměstí	k1gNnSc7	náměstí
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
centrem	centr	k1gMnSc7	centr
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Městečko	městečko	k1gNnSc1	městečko
s	s	k7c7	s
hradem	hrad	k1gInSc7	hrad
chránilo	chránit	k5eAaImAgNnS	chránit
nově	nově	k6eAd1	nově
zbudované	zbudovaný	k2eAgNnSc1d1	zbudované
opevnění	opevnění	k1gNnSc1	opevnění
se	s	k7c7	s
třemi	tři	k4xCgFnPc7	tři
branami	brána	k1gFnPc7	brána
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
ale	ale	k9	ale
nezahrnovalo	zahrnovat	k5eNaImAgNnS	zahrnovat
Nymburské	nymburský	k2eAgNnSc1d1	nymburské
a	a	k8xC	a
Kostelní	kostelní	k2eAgNnSc1d1	kostelní
předměstí	předměstí	k1gNnSc1	předměstí
–	–	k?	–
kostel	kostel	k1gInSc1	kostel
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
ocitl	ocitnout	k5eAaPmAgInS	ocitnout
mimo	mimo	k7c4	mimo
opevnění	opevnění	k1gNnSc4	opevnění
<g/>
.	.	kIx.	.
</s>
<s>
Nechali	nechat	k5eAaPmAgMnP	nechat
také	také	k9	také
vybudovat	vybudovat	k5eAaPmF	vybudovat
Sánský	Sánský	k2eAgInSc4d1	Sánský
kanál	kanál	k1gInSc4	kanál
a	a	k8xC	a
základ	základ	k1gInSc4	základ
poděbradské	poděbradský	k2eAgFnSc2d1	Poděbradská
rybniční	rybniční	k2eAgFnSc2d1	rybniční
soustavy	soustava	k1gFnSc2	soustava
(	(	kIx(	(
<g/>
až	až	k6eAd1	až
do	do	k7c2	do
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
rybníkářských	rybníkářský	k2eAgFnPc2d1	rybníkářská
oblastí	oblast	k1gFnPc2	oblast
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Založili	založit	k5eAaPmAgMnP	založit
také	také	k9	také
řadu	řada	k1gFnSc4	řada
vesnic	vesnice	k1gFnPc2	vesnice
a	a	k8xC	a
dvorů	dvůr	k1gInPc2	dvůr
–	–	k?	–
dnešní	dnešní	k2eAgFnSc2d1	dnešní
okolní	okolní	k2eAgFnSc2d1	okolní
Lhoty	Lhota	k1gFnSc2	Lhota
<g/>
,	,	kIx,	,
Zboží	zboží	k1gNnSc1	zboží
a	a	k8xC	a
Nové	Nové	k2eAgFnPc1d1	Nové
Vsi	ves	k1gFnPc1	ves
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
rodu	rod	k1gInSc2	rod
pocházel	pocházet	k5eAaImAgInS	pocházet
i	i	k9	i
slavný	slavný	k2eAgMnSc1d1	slavný
"	"	kIx"	"
<g/>
husitský	husitský	k2eAgMnSc1d1	husitský
král	král	k1gMnSc1	král
<g/>
"	"	kIx"	"
Jiří	Jiří	k1gMnSc1	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
byl	být	k5eAaImAgInS	být
korunován	korunovat	k5eAaBmNgInS	korunovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1458	[number]	k4	1458
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
Jiří	Jiří	k1gMnSc1	Jiří
z	z	k7c2	z
poděbradského	poděbradský	k2eAgNnSc2d1	Poděbradské
panství	panství	k1gNnSc2	panství
postupně	postupně	k6eAd1	postupně
vybudoval	vybudovat	k5eAaPmAgMnS	vybudovat
druhé	druhý	k4xOgNnSc4	druhý
největší	veliký	k2eAgNnSc4d3	veliký
dominium	dominium	k1gNnSc4	dominium
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
hned	hned	k6eAd1	hned
za	za	k7c4	za
Rožmberky	Rožmberk	k1gMnPc4	Rožmberk
<g/>
.	.	kIx.	.
</s>
<s>
Jiřího	Jiří	k1gMnSc4	Jiří
synové	syn	k1gMnPc1	syn
Poděbrady	Poděbrady	k1gInPc1	Poděbrady
roku	rok	k1gInSc2	rok
1472	[number]	k4	1472
povýšili	povýšit	k5eAaPmAgMnP	povýšit
na	na	k7c4	na
město	město	k1gNnSc4	město
a	a	k8xC	a
udělili	udělit	k5eAaPmAgMnP	udělit
jim	on	k3xPp3gMnPc3	on
četná	četný	k2eAgNnPc4d1	četné
práva	právo	k1gNnPc4	právo
a	a	k8xC	a
městský	městský	k2eAgInSc4d1	městský
znak	znak	k1gInSc4	znak
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
krále	král	k1gMnSc2	král
Jiřího	Jiří	k1gMnSc2	Jiří
spravoval	spravovat	k5eAaImAgInS	spravovat
panství	panství	k1gNnSc6	panství
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Hynek	Hynek	k1gMnSc1	Hynek
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
o	o	k7c4	o
něj	on	k3xPp3gMnSc4	on
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1495	[number]	k4	1495
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
podvodně	podvodně	k6eAd1	podvodně
připraven	připravit	k5eAaPmNgInS	připravit
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
Vladislava	Vladislav	k1gMnSc2	Vladislav
Jagellonského	jagellonský	k2eAgMnSc2d1	jagellonský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1495	[number]	k4	1495
<g/>
–	–	k?	–
<g/>
1839	[number]	k4	1839
bylo	být	k5eAaImAgNnS	být
poděbradské	poděbradský	k2eAgNnSc1d1	Poděbradské
panství	panství	k1gNnSc1	panství
spravováno	spravovat	k5eAaImNgNnS	spravovat
královskou	královský	k2eAgFnSc7d1	královská
komorou	komora	k1gFnSc7	komora
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
období	období	k1gNnSc2	období
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
panství	panství	k1gNnSc1	panství
zastaveno	zastaven	k2eAgNnSc1d1	zastaveno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Ferdinanda	Ferdinand	k1gMnSc4	Ferdinand
I.	I.	kA	I.
Habsburského	habsburský	k2eAgMnSc4d1	habsburský
se	se	k3xPyFc4	se
poděbradské	poděbradský	k2eAgNnSc1d1	Poděbradské
panství	panství	k1gNnSc1	panství
stalo	stát	k5eAaPmAgNnS	stát
oblíbeným	oblíbený	k2eAgInSc7d1	oblíbený
loveckým	lovecký	k2eAgInSc7d1	lovecký
revírem	revír	k1gInSc7	revír
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
přízeň	přízeň	k1gFnSc1	přízeň
mu	on	k3xPp3gMnSc3	on
přinesla	přinést	k5eAaPmAgFnS	přinést
novou	nový	k2eAgFnSc4d1	nová
prosperitu	prosperita	k1gFnSc4	prosperita
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
zde	zde	k6eAd1	zde
také	také	k9	také
roku	rok	k1gInSc2	rok
1538	[number]	k4	1538
založil	založit	k5eAaPmAgMnS	založit
oboru	obora	k1gFnSc4	obora
pro	pro	k7c4	pro
chov	chov	k1gInSc4	chov
daňků	daněk	k1gMnPc2	daněk
<g/>
,	,	kIx,	,
bažantnici	bažantnice	k1gFnSc4	bažantnice
a	a	k8xC	a
zdejší	zdejší	k2eAgInSc4d1	zdejší
hrad	hrad	k1gInSc4	hrad
nechal	nechat	k5eAaPmAgInS	nechat
přestavět	přestavět	k5eAaPmF	přestavět
za	za	k7c4	za
vedení	vedení	k1gNnSc4	vedení
italských	italský	k2eAgMnPc2d1	italský
stavitelů	stavitel	k1gMnPc2	stavitel
na	na	k7c4	na
renesanční	renesanční	k2eAgInSc4d1	renesanční
zámek	zámek	k1gInSc4	zámek
(	(	kIx(	(
<g/>
přestavba	přestavba	k1gFnSc1	přestavba
probíhala	probíhat	k5eAaImAgFnS	probíhat
v	v	k7c6	v
letech	let	k1gInPc6	let
1548	[number]	k4	1548
<g/>
–	–	k?	–
<g/>
1582	[number]	k4	1582
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
až	až	k9	až
za	za	k7c2	za
Rudolfa	Rudolf	k1gMnSc2	Rudolf
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
pro	pro	k7c4	pro
mnoho	mnoho	k4c4	mnoho
dalších	další	k2eAgNnPc2d1	další
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
utrpěly	utrpět	k5eAaPmAgInP	utrpět
Poděbrady	Poděbrady	k1gInPc1	Poděbrady
značné	značný	k2eAgFnSc2d1	značná
škody	škoda	k1gFnSc2	škoda
za	za	k7c2	za
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
vypleněno	vyplenit	k5eAaPmNgNnS	vyplenit
už	už	k9	už
několik	několik	k4yIc1	několik
dní	den	k1gInPc2	den
po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
na	na	k7c6	na
Bílé	bílý	k2eAgFnSc6d1	bílá
hoře	hora	k1gFnSc6	hora
<g/>
,	,	kIx,	,
několikrát	několikrát	k6eAd1	několikrát
ho	on	k3xPp3gNnSc4	on
obsadila	obsadit	k5eAaPmAgFnS	obsadit
švédská	švédský	k2eAgFnSc1d1	švédská
a	a	k8xC	a
saská	saský	k2eAgNnPc1d1	Saské
vojska	vojsko	k1gNnPc1	vojsko
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
velkou	velký	k2eAgFnSc7d1	velká
zátěží	zátěž	k1gFnSc7	zátěž
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gMnPc4	jeho
obyvatele	obyvatel	k1gMnPc4	obyvatel
byl	být	k5eAaImAgMnS	být
i	i	k8xC	i
pobyt	pobyt	k1gInSc4	pobyt
vojsk	vojsko	k1gNnPc2	vojsko
císařských	císařský	k2eAgNnPc2d1	císařské
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
byla	být	k5eAaImAgFnS	být
zničena	zničit	k5eAaPmNgFnS	zničit
většina	většina	k1gFnSc1	většina
dosavadních	dosavadní	k2eAgFnPc2d1	dosavadní
písemností	písemnost	k1gFnPc2	písemnost
o	o	k7c6	o
historii	historie	k1gFnSc6	historie
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
velký	velký	k2eAgInSc1d1	velký
požár	požár	k1gInSc1	požár
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1681	[number]	k4	1681
město	město	k1gNnSc1	město
kompletně	kompletně	k6eAd1	kompletně
zničil	zničit	k5eAaPmAgMnS	zničit
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
obou	dva	k4xCgNnPc2	dva
předměstí	předměstí	k1gNnPc2	předměstí
(	(	kIx(	(
<g/>
zámek	zámek	k1gInSc1	zámek
nevyhořel	vyhořet	k5eNaPmAgInS	vyhořet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
nové	nový	k2eAgInPc1d1	nový
domy	dům	k1gInPc1	dům
už	už	k6eAd1	už
byly	být	k5eAaImAgInP	být
stavěny	stavit	k5eAaImNgInP	stavit
z	z	k7c2	z
kamene	kámen	k1gInSc2	kámen
a	a	k8xC	a
cihel	cihla	k1gFnPc2	cihla
(	(	kIx(	(
<g/>
ničivých	ničivý	k2eAgInPc2d1	ničivý
požárů	požár	k1gInPc2	požár
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
města	město	k1gNnSc2	město
více	hodně	k6eAd2	hodně
–	–	k?	–
například	například	k6eAd1	například
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1453	[number]	k4	1453
a	a	k8xC	a
1832	[number]	k4	1832
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
pak	pak	k6eAd1	pak
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
vinou	vinou	k7c2	vinou
ohromného	ohromný	k2eAgNnSc2d1	ohromné
zatížení	zatížení	k1gNnSc2	zatížení
daněmi	daň	k1gFnPc7	daň
a	a	k8xC	a
úpadku	úpadek	k1gInSc3	úpadek
obchodu	obchod	k1gInSc2	obchod
a	a	k8xC	a
řemesel	řemeslo	k1gNnPc2	řemeslo
<g/>
,	,	kIx,	,
stagnovalo	stagnovat	k5eAaImAgNnS	stagnovat
a	a	k8xC	a
dostávalo	dostávat	k5eAaImAgNnS	dostávat
postupně	postupně	k6eAd1	postupně
zemědělský	zemědělský	k2eAgInSc4d1	zemědělský
charakter	charakter	k1gInSc4	charakter
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
si	se	k3xPyFc3	se
udrželo	udržet	k5eAaPmAgNnS	udržet
až	až	k6eAd1	až
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
zámek	zámek	k1gInSc4	zámek
projevila	projevit	k5eAaPmAgFnS	projevit
zájem	zájem	k1gInSc4	zájem
císařovna	císařovna	k1gFnSc1	císařovna
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc1	Terezie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ho	on	k3xPp3gMnSc4	on
nechala	nechat	k5eAaPmAgFnS	nechat
v	v	k7c6	v
letech	let	k1gInPc6	let
1754	[number]	k4	1754
<g/>
–	–	k?	–
<g/>
1756	[number]	k4	1756
barokně	barokně	k6eAd1	barokně
přestavět	přestavět	k5eAaPmF	přestavět
a	a	k8xC	a
několikrát	několikrát	k6eAd1	několikrát
zde	zde	k6eAd1	zde
i	i	k9	i
pobývala	pobývat	k5eAaImAgFnS	pobývat
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
stalo	stát	k5eAaPmAgNnS	stát
důležitou	důležitý	k2eAgFnSc7d1	důležitá
křižovatkou	křižovatka	k1gFnSc7	křižovatka
císařských	císařský	k2eAgFnPc2d1	císařská
silnic	silnice	k1gFnPc2	silnice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1812	[number]	k4	1812
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
v	v	k7c6	v
Poděbradech	Poděbrady	k1gInPc6	Poděbrady
s	s	k7c7	s
pravidelným	pravidelný	k2eAgNnSc7d1	pravidelné
sbíráním	sbírání	k1gNnSc7	sbírání
dopisů	dopis	k1gInPc2	dopis
pro	pro	k7c4	pro
dopravu	doprava	k1gFnSc4	doprava
poštou	pošta	k1gFnSc7	pošta
<g/>
.	.	kIx.	.
</s>
<s>
Poštovní	poštovní	k2eAgInSc1d1	poštovní
úřad	úřad	k1gInSc1	úřad
v	v	k7c6	v
Poděbradech	Poděbrady	k1gInPc6	Poděbrady
vznikl	vzniknout	k5eAaPmAgMnS	vzniknout
však	však	k9	však
až	až	k9	až
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1832	[number]	k4	1832
<g/>
-	-	kIx~	-
<g/>
33	[number]	k4	33
<g/>
.	.	kIx.	.
</s>
<s>
Telegraf	telegraf	k1gInSc1	telegraf
byl	být	k5eAaImAgInS	být
zavedena	zavést	k5eAaPmNgNnP	zavést
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1850	[number]	k4	1850
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
místní	místní	k2eAgInPc1d1	místní
telefony	telefon	k1gInPc1	telefon
byly	být	k5eAaImAgInP	být
instalovány	instalovat	k5eAaBmNgInP	instalovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1900	[number]	k4	1900
a	a	k8xC	a
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
i	i	k9	i
linky	linka	k1gFnPc4	linka
meziměstské	meziměstský	k2eAgFnPc4d1	meziměstská
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1839	[number]	k4	1839
koupil	koupit	k5eAaPmAgMnS	koupit
poděbradské	poděbradský	k2eAgNnSc4d1	Poděbradské
panství	panství	k1gNnSc4	panství
vídeňský	vídeňský	k2eAgMnSc1d1	vídeňský
bankéř	bankéř	k1gMnSc1	bankéř
baron	baron	k1gMnSc1	baron
Jiří	Jiří	k1gMnSc1	Jiří
Sina	sino	k1gNnSc2	sino
<g/>
.	.	kIx.	.
</s>
<s>
Natrvalo	natrvalo	k6eAd1	natrvalo
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
usídlila	usídlit	k5eAaPmAgFnS	usídlit
až	až	k9	až
jeho	jeho	k3xOp3gFnSc1	jeho
vnučka	vnučka	k1gFnSc1	vnučka
Chariclea	Chariclea	k1gMnSc1	Chariclea
Ypsilanti	Ypsilant	k1gMnPc1	Ypsilant
s	s	k7c7	s
chotěm	choť	k1gMnSc7	choť
bavorským	bavorský	k2eAgMnSc7d1	bavorský
knížetem	kníže	k1gMnSc7	kníže
Filipem	Filip	k1gMnSc7	Filip
Arnoštem	Arnošt	k1gMnSc7	Arnošt
Hohenlohe	Hohenloh	k1gFnSc2	Hohenloh
z	z	k7c2	z
Schillingfürstu	Schillingfürst	k1gInSc2	Schillingfürst
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
byli	být	k5eAaImAgMnP	být
posledními	poslední	k2eAgMnPc7d1	poslední
vlastníky	vlastník	k1gMnPc7	vlastník
panství	panství	k1gNnSc1	panství
(	(	kIx(	(
<g/>
vlastnili	vlastnit	k5eAaImAgMnP	vlastnit
ho	on	k3xPp3gMnSc4	on
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1885	[number]	k4	1885
<g/>
–	–	k?	–
<g/>
1912	[number]	k4	1912
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
přes	přes	k7c4	přes
město	město	k1gNnSc4	město
postavena	postaven	k2eAgFnSc1d1	postavena
železnice	železnice	k1gFnSc1	železnice
a	a	k8xC	a
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
tu	ten	k3xDgFnSc4	ten
i	i	k9	i
několik	několik	k4yIc4	několik
průmyslových	průmyslový	k2eAgInPc2d1	průmyslový
podniků	podnik	k1gInPc2	podnik
(	(	kIx(	(
<g/>
městu	město	k1gNnSc3	město
však	však	k9	však
i	i	k9	i
nadále	nadále	k6eAd1	nadále
dominovalo	dominovat	k5eAaImAgNnS	dominovat
zemědělství	zemědělství	k1gNnSc1	zemědělství
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
rostl	růst	k5eAaImAgInS	růst
a	a	k8xC	a
přibývaly	přibývat	k5eAaImAgFnP	přibývat
i	i	k9	i
nové	nový	k2eAgFnPc1d1	nová
čtvrti	čtvrt	k1gFnPc1	čtvrt
<g/>
.	.	kIx.	.
</s>
<s>
Důležitým	důležitý	k2eAgInSc7d1	důležitý
mezníkem	mezník	k1gInSc7	mezník
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
města	město	k1gNnSc2	město
byl	být	k5eAaImAgInS	být
zejména	zejména	k9	zejména
objev	objev	k1gInSc1	objev
uhličité	uhličitý	k2eAgFnSc2d1	uhličitá
minerální	minerální	k2eAgFnSc2d1	minerální
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
zvané	zvaný	k2eAgFnPc1d1	zvaná
Poděbradka	poděbradka	k1gFnSc1	poděbradka
<g/>
,	,	kIx,	,
při	při	k7c6	při
vrtání	vrtání	k1gNnSc6	vrtání
studny	studna	k1gFnSc2	studna
na	na	k7c6	na
nádvoří	nádvoří	k1gNnSc6	nádvoří
zámku	zámek	k1gInSc2	zámek
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
vrtání	vrtání	k1gNnSc2	vrtání
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
proutku	proutek	k1gInSc2	proutek
nalezl	nalézt	k5eAaBmAgMnS	nalézt
německý	německý	k2eAgMnSc1d1	německý
hrabě	hrabě	k1gMnSc1	hrabě
Karel	Karel	k1gMnSc1	Karel
Bülow	Bülow	k1gMnSc1	Bülow
z	z	k7c2	z
Bothkampu	Bothkamp	k1gInSc2	Bothkamp
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hloubky	hloubka	k1gFnSc2	hloubka
96,7	[number]	k4	96,7
m	m	kA	m
pak	pak	k6eAd1	pak
vytryskl	vytrysknout	k5eAaPmAgMnS	vytrysknout
silný	silný	k2eAgInSc4d1	silný
pramen	pramen	k1gInSc4	pramen
kalné	kalný	k2eAgFnSc2d1	kalná
železité	železitý	k2eAgFnSc2d1	železitá
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
označovaný	označovaný	k2eAgInSc1d1	označovaný
dnes	dnes	k6eAd1	dnes
jako	jako	k8xS	jako
Bülowův	Bülowův	k2eAgInSc4d1	Bülowův
pramen	pramen	k1gInSc4	pramen
<g/>
.	.	kIx.	.
</s>
<s>
Objevení	objevení	k1gNnSc1	objevení
minerálních	minerální	k2eAgFnPc2d1	minerální
vod	voda	k1gFnPc2	voda
město	město	k1gNnSc1	město
změnilo	změnit	k5eAaPmAgNnS	změnit
k	k	k7c3	k
nepoznání	nepoznání	k1gNnSc3	nepoznání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1907	[number]	k4	1907
už	už	k6eAd1	už
byly	být	k5eAaImAgInP	být
vyvrtány	vyvrtán	k2eAgInPc1d1	vyvrtán
tři	tři	k4xCgInPc1	tři
veřejné	veřejný	k2eAgInPc1d1	veřejný
prameny	pramen	k1gInPc1	pramen
(	(	kIx(	(
<g/>
Bülowův	Bülowův	k2eAgInSc1d1	Bülowův
<g/>
,	,	kIx,	,
Hohenlohe	Hohenloh	k1gInSc2	Hohenloh
a	a	k8xC	a
Charicléa	Chariclé	k1gInSc2	Chariclé
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
byly	být	k5eAaImAgFnP	být
založeny	založit	k5eAaPmNgFnP	založit
poděbradské	poděbradský	k2eAgFnPc1d1	Poděbradská
lázně	lázeň	k1gFnPc1	lázeň
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
další	další	k2eAgInSc4d1	další
rozvoj	rozvoj	k1gInSc4	rozvoj
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
5	[number]	k4	5
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1908	[number]	k4	1908
vysvětil	vysvětit	k5eAaPmAgMnS	vysvětit
královéhradecký	královéhradecký	k2eAgMnSc1d1	královéhradecký
biskup	biskup	k1gMnSc1	biskup
Doubrava	Doubrava	k1gMnSc1	Doubrava
budovu	budova	k1gFnSc4	budova
Knížecích	knížecí	k2eAgFnPc2d1	knížecí
lázní	lázeň	k1gFnPc2	lázeň
<g/>
,	,	kIx,	,
založených	založený	k2eAgFnPc2d1	založená
knížetem	kníže	k1gNnSc7wR	kníže
Hohenlohe	Hohenloh	k1gFnSc2	Hohenloh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1910	[number]	k4	1910
Knížecí	knížecí	k2eAgFnSc2d1	knížecí
lázně	lázeň	k1gFnSc2	lázeň
odkoupilo	odkoupit	k5eAaPmAgNnS	odkoupit
město	město	k1gNnSc1	město
a	a	k8xC	a
dalo	dát	k5eAaPmAgNnS	dát
navrtat	navrtat	k5eAaPmF	navrtat
další	další	k2eAgInPc4d1	další
prameny	pramen	k1gInPc4	pramen
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
počátkem	počátkem	k7c2	počátkem
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
tu	tu	k6eAd1	tu
bylo	být	k5eAaImAgNnS	být
12	[number]	k4	12
zřídel	zřídlo	k1gNnPc2	zřídlo
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
již	již	k9	již
16	[number]	k4	16
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc4d1	nový
ráz	ráz	k1gInSc4	ráz
města	město	k1gNnSc2	město
určil	určit	k5eAaPmAgMnS	určit
zejména	zejména	k9	zejména
architekt	architekt	k1gMnSc1	architekt
a	a	k8xC	a
urbanista	urbanista	k1gMnSc1	urbanista
František	František	k1gMnSc1	František
Janda	Janda	k1gMnSc1	Janda
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
před	před	k7c7	před
válkou	válka	k1gFnSc7	válka
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
koncepci	koncepce	k1gFnSc3	koncepce
rozvoje	rozvoj	k1gInSc2	rozvoj
lázeňského	lázeňský	k2eAgInSc2d1	lázeňský
středu	střed	k1gInSc2	střed
v	v	k7c6	v
tehdejším	tehdejší	k2eAgInSc6d1	tehdejší
secesním	secesní	k2eAgInSc6d1	secesní
slohu	sloh	k1gInSc6	sloh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
lázně	lázeň	k1gFnSc2	lázeň
od	od	k7c2	od
města	město	k1gNnSc2	město
převzala	převzít	k5eAaPmAgFnS	převzít
nově	nově	k6eAd1	nově
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
akciová	akciový	k2eAgFnSc1d1	akciová
společnost	společnost	k1gFnSc1	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
první	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
se	se	k3xPyFc4	se
Poděbrady	Poděbrady	k1gInPc1	Poděbrady
změnily	změnit	k5eAaPmAgInP	změnit
ve	v	k7c4	v
významné	významný	k2eAgFnPc4d1	významná
lázně	lázeň	k1gFnPc4	lázeň
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
zásluhou	zásluhou	k7c2	zásluhou
pražského	pražský	k2eAgMnSc2d1	pražský
kardiologa	kardiolog	k1gMnSc2	kardiolog
profesora	profesor	k1gMnSc2	profesor
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Václava	Václav	k1gMnSc2	Václav
Libenského	Libenský	k2eAgMnSc2d1	Libenský
<g/>
,	,	kIx,	,
specializovaly	specializovat	k5eAaBmAgInP	specializovat
na	na	k7c4	na
léčbu	léčba	k1gFnSc4	léčba
nemocí	nemoc	k1gFnSc7	nemoc
srdce	srdce	k1gNnSc2	srdce
a	a	k8xC	a
oběhového	oběhový	k2eAgNnSc2d1	oběhové
ústrojí	ústrojí	k1gNnSc2	ústrojí
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
Janda	Janda	k1gMnSc1	Janda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
vypracoval	vypracovat	k5eAaPmAgInS	vypracovat
urbanistický	urbanistický	k2eAgInSc1d1	urbanistický
plán	plán	k1gInSc1	plán
dalšího	další	k2eAgInSc2d1	další
rozvoje	rozvoj	k1gInSc2	rozvoj
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
lázeňský	lázeňský	k2eAgInSc1d1	lázeňský
střed	střed	k1gInSc1	střed
se	se	k3xPyFc4	se
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
až	až	k9	až
v	v	k7c6	v
železnici	železnice	k1gFnSc6	železnice
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
cca	cca	kA	cca
1	[number]	k4	1
km	km	kA	km
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
<g/>
)	)	kIx)	)
a	a	k8xC	a
založil	založit	k5eAaPmAgMnS	založit
lázeňský	lázeňský	k2eAgInSc4d1	lázeňský
park	park	k1gInSc4	park
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
řada	řada	k1gFnSc1	řada
veřejných	veřejný	k2eAgFnPc2d1	veřejná
budov	budova	k1gFnPc2	budova
a	a	k8xC	a
počet	počet	k1gInSc1	počet
jeho	jeho	k3xOp3gMnPc2	jeho
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
století	století	k1gNnSc2	století
zdvojnásobil	zdvojnásobit	k5eAaPmAgInS	zdvojnásobit
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
územněsprávního	územněsprávní	k2eAgNnSc2d1	územněsprávní
začleňování	začleňování	k1gNnSc2	začleňování
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
období	období	k1gNnSc4	období
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1850	[number]	k4	1850
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
chronologickém	chronologický	k2eAgInSc6d1	chronologický
přehledu	přehled	k1gInSc6	přehled
je	být	k5eAaImIp3nS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
územně	územně	k6eAd1	územně
administrativní	administrativní	k2eAgFnSc4d1	administrativní
příslušnost	příslušnost	k1gFnSc4	příslušnost
obce	obec	k1gFnSc2	obec
v	v	k7c6	v
roce	rok	k1gInSc6	rok
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
došlo	dojít	k5eAaPmAgNnS	dojít
<g/>
:	:	kIx,	:
1850	[number]	k4	1850
země	zem	k1gFnSc2	zem
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
kraj	kraj	k1gInSc1	kraj
Jičín	Jičín	k1gInSc1	Jičín
<g/>
,	,	kIx,	,
politický	politický	k2eAgInSc1d1	politický
i	i	k8xC	i
soudní	soudní	k2eAgInSc1d1	soudní
okres	okres	k1gInSc1	okres
Poděbrady	Poděbrady	k1gInPc1	Poděbrady
1855	[number]	k4	1855
země	zem	k1gFnSc2	zem
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
kraj	kraj	k1gInSc1	kraj
Čáslav	Čáslav	k1gFnSc1	Čáslav
<g/>
,	,	kIx,	,
soudní	soudní	k2eAgInSc1d1	soudní
okres	okres	k1gInSc1	okres
Poděbrady	Poděbrady	k1gInPc1	Poděbrady
1868	[number]	k4	1868
země	zem	k1gFnSc2	zem
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
politický	politický	k2eAgInSc1d1	politický
i	i	k8xC	i
soudní	soudní	k2eAgInSc1d1	soudní
okres	okres	k1gInSc1	okres
Poděbrady	Poděbrady	k1gInPc1	Poděbrady
1939	[number]	k4	1939
země	zem	k1gFnSc2	zem
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
Oberlandrat	Oberlandrat	k1gInSc1	Oberlandrat
Kolín	Kolín	k1gInSc1	Kolín
<g/>
,	,	kIx,	,
politický	politický	k2eAgInSc1d1	politický
i	i	k8xC	i
soudní	soudní	k2eAgInSc1d1	soudní
okres	okres	k1gInSc1	okres
Poděbrady	Poděbrady	k1gInPc1	Poděbrady
1942	[number]	k4	1942
země	zem	k1gFnSc2	zem
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
Oberlandrat	Oberlandrat	k1gInSc1	Oberlandrat
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
,	,	kIx,	,
politický	politický	k2eAgInSc1d1	politický
okres	okres	k1gInSc1	okres
Nymburk	Nymburk	k1gInSc1	Nymburk
<g/>
,	,	kIx,	,
soudní	soudní	k2eAgInSc1d1	soudní
okres	okres	k1gInSc1	okres
Poděbrady	Poděbrady	k1gInPc1	Poděbrady
1945	[number]	k4	1945
země	zem	k1gFnSc2	zem
česká	český	k2eAgNnPc1d1	české
<g/>
,	,	kIx,	,
správní	správní	k2eAgNnSc1d1	správní
i	i	k8xC	i
soudní	soudní	k2eAgInSc1d1	soudní
okres	okres	k1gInSc1	okres
Poděbrady	Poděbrady	k1gInPc1	Poděbrady
1949	[number]	k4	1949
Pražský	pražský	k2eAgInSc1d1	pražský
kraj	kraj	k1gInSc1	kraj
<g/>
,	,	kIx,	,
okres	okres	k1gInSc1	okres
Poděbrady	Poděbrady	k1gInPc1	Poděbrady
1960	[number]	k4	1960
Středočeský	středočeský	k2eAgInSc1d1	středočeský
kraj	kraj	k1gInSc1	kraj
<g/>
,	,	kIx,	,
okres	okres	k1gInSc1	okres
Nymburk	Nymburk	k1gInSc1	Nymburk
2003	[number]	k4	2003
Středočeský	středočeský	k2eAgInSc1d1	středočeský
kraj	kraj	k1gInSc1	kraj
<g/>
,	,	kIx,	,
okres	okres	k1gInSc1	okres
Nymburk	Nymburk	k1gInSc1	Nymburk
<g/>
,	,	kIx,	,
obec	obec	k1gFnSc1	obec
s	s	k7c7	s
rozšířenou	rozšířený	k2eAgFnSc7d1	rozšířená
působností	působnost	k1gFnSc7	působnost
Poděbrady	Poděbrady	k1gInPc7	Poděbrady
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
Poděbrady	Poděbrady	k1gInPc4	Poděbrady
(	(	kIx(	(
<g/>
přísl	přísnout	k5eAaPmAgMnS	přísnout
<g/>
.	.	kIx.	.
</s>
<s>
Malé	Malé	k2eAgNnSc1d1	Malé
Zboží	zboží	k1gNnSc1	zboží
<g/>
,	,	kIx,	,
7236	[number]	k4	7236
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
evidovány	evidovat	k5eAaImNgFnP	evidovat
tyto	tento	k3xDgFnPc1	tento
živnosti	živnost	k1gFnPc1	živnost
a	a	k8xC	a
obchody	obchod	k1gInPc1	obchod
<g/>
:	:	kIx,	:
Instituce	instituce	k1gFnSc1	instituce
<g/>
:	:	kIx,	:
okresní	okresní	k2eAgInSc1d1	okresní
úřad	úřad	k1gInSc1	úřad
<g/>
,	,	kIx,	,
okresní	okresní	k2eAgInSc1d1	okresní
soud	soud	k1gInSc1	soud
<g/>
,	,	kIx,	,
berní	berní	k2eAgFnSc1d1	berní
správa	správa	k1gFnSc1	správa
<g/>
,	,	kIx,	,
berní	berní	k2eAgInSc1d1	berní
úřad	úřad	k1gInSc1	úřad
<g/>
,	,	kIx,	,
důchodkový	důchodkový	k2eAgInSc1d1	důchodkový
kontrolní	kontrolní	k2eAgInSc4d1	kontrolní
úřad	úřad	k1gInSc4	úřad
<g/>
,	,	kIx,	,
cejchovní	cejchovní	k2eAgInSc4d1	cejchovní
úřad	úřad	k1gInSc4	úřad
<g/>
,	,	kIx,	,
katastrální	katastrální	k2eAgInSc4d1	katastrální
zeměměřičský	zeměměřičský	k2eAgInSc4d1	zeměměřičský
úřad	úřad	k1gInSc4	úřad
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
okresní	okresní	k2eAgNnSc4d1	okresní
četnické	četnický	k2eAgNnSc4d1	četnické
velitelství	velitelství	k1gNnSc4	velitelství
<g/>
,	,	kIx,	,
poštovní	poštovní	k2eAgInSc4d1	poštovní
úřad	úřad	k1gInSc4	úřad
<g/>
,	,	kIx,	,
telefonní	telefonní	k2eAgInSc4d1	telefonní
úřad	úřad	k1gInSc4	úřad
<g/>
,	,	kIx,	,
telegrafní	telegrafní	k2eAgInSc1d1	telegrafní
úřad	úřad	k1gInSc1	úřad
<g/>
,	,	kIx,	,
státní	státní	k2eAgFnSc1d1	státní
radiotelegrafická	radiotelegrafický	k2eAgFnSc1d1	radiotelegrafická
stanice	stanice	k1gFnSc1	stanice
<g/>
,	,	kIx,	,
lázně	lázeň	k1gFnPc1	lázeň
se	s	k7c7	s
14	[number]	k4	14
léčivými	léčivý	k2eAgInPc7d1	léčivý
prameny	pramen	k1gInPc7	pramen
<g/>
,	,	kIx,	,
2	[number]	k4	2
katol	katola	k1gFnPc2	katola
<g/>
.	.	kIx.	.
kostely	kostel	k1gInPc1	kostel
<g/>
,	,	kIx,	,
evang	evang	k1gInSc1	evang
<g/>
.	.	kIx.	.
kostel	kostel	k1gInSc1	kostel
<g/>
,	,	kIx,	,
synagoga	synagoga	k1gFnSc1	synagoga
<g/>
,	,	kIx,	,
vyšší	vysoký	k2eAgFnSc1d2	vyšší
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
škola	škola	k1gFnSc1	škola
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
akademie	akademie	k1gFnSc1	akademie
<g/>
,	,	kIx,	,
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
,	,	kIx,	,
Paroubkův	Paroubkův	k2eAgInSc1d1	Paroubkův
ústav	ústav	k1gInSc1	ústav
chudých	chudý	k1gMnPc2	chudý
<g/>
,	,	kIx,	,
okresní	okresní	k2eAgFnSc1d1	okresní
nemocnice	nemocnice	k1gFnSc1	nemocnice
<g/>
,	,	kIx,	,
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
liga	liga	k1gFnSc1	liga
proti	proti	k7c3	proti
tuberkulose	tuberkulosa	k1gFnSc3	tuberkulosa
<g/>
,	,	kIx,	,
sbor	sbor	k1gInSc1	sbor
dobrovolných	dobrovolný	k2eAgMnPc2d1	dobrovolný
hasičů	hasič	k1gMnPc2	hasič
Živnosti	živnost	k1gFnSc2	živnost
a	a	k8xC	a
průmysl	průmysl	k1gInSc1	průmysl
<g/>
:	:	kIx,	:
obchodní	obchodní	k2eAgNnSc1d1	obchodní
grémium	grémium	k1gNnSc1	grémium
<g/>
,	,	kIx,	,
okresní	okresní	k2eAgFnSc1d1	okresní
jednota	jednota	k1gFnSc1	jednota
společenstev	společenstvo	k1gNnPc2	společenstvo
<g/>
,	,	kIx,	,
společenstva	společenstvo	k1gNnSc2	společenstvo
kovy	kov	k1gInPc7	kov
zpracujících	zpracující	k2eAgInPc2d1	zpracující
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
obuvníků	obuvník	k1gMnPc2	obuvník
a	a	k8xC	a
kůže	kůže	k1gFnSc2	kůže
zpracujících	zpracující	k2eAgInPc2d1	zpracující
<g/>
,	,	kIx,	,
oděvních	oděvní	k2eAgInPc2d1	oděvní
<g/>
,	,	kIx,	,
potravních	potravní	k2eAgInPc2d1	potravní
a	a	k8xC	a
smíšených	smíšený	k2eAgFnPc2d1	smíšená
živností	živnost	k1gFnPc2	živnost
<g/>
,	,	kIx,	,
společenstvo	společenstvo	k1gNnSc1	společenstvo
holičů	holič	k1gMnPc2	holič
<g/>
,	,	kIx,	,
hostinských	hostinský	k1gMnPc2	hostinský
a	a	k8xC	a
výčepníků	výčepník	k1gMnPc2	výčepník
<g/>
,	,	kIx,	,
kovářů	kovář	k1gMnPc2	kovář
<g/>
,	,	kIx,	,
mlynářů	mlynář	k1gMnPc2	mlynář
<g/>
,	,	kIx,	,
pekařů	pekař	k1gMnPc2	pekař
<g/>
,	,	kIx,	,
řezníků	řezník	k1gMnPc2	řezník
<g/>
,	,	kIx,	,
různých	různý	k2eAgFnPc2d1	různá
živností	živnost	k1gFnPc2	živnost
a	a	k8xC	a
kolářů	kolář	k1gMnPc2	kolář
<g/>
,	,	kIx,	,
továrna	továrna	k1gFnSc1	továrna
na	na	k7c4	na
vápenopískové	vápenopískový	k2eAgFnPc4d1	vápenopísková
cihly	cihla	k1gFnPc4	cihla
<g/>
,	,	kIx,	,
továrna	továrna	k1gFnSc1	továrna
dlažební	dlažební	k2eAgFnSc2d1	dlažební
<g />
.	.	kIx.	.
</s>
<s>
špalíky	špalík	k1gInPc1	špalík
<g/>
,	,	kIx,	,
továrna	továrna	k1gFnSc1	továrna
na	na	k7c4	na
rostlinné	rostlinný	k2eAgInPc4d1	rostlinný
oleje	olej	k1gInPc4	olej
<g/>
,	,	kIx,	,
pivovar	pivovar	k1gInSc1	pivovar
<g/>
,	,	kIx,	,
sklárna	sklárna	k1gFnSc1	sklárna
<g/>
,	,	kIx,	,
cukrovar	cukrovar	k1gInSc1	cukrovar
<g/>
,	,	kIx,	,
jatky	jatka	k1gFnPc1	jatka
<g/>
,	,	kIx,	,
elektrárna	elektrárna	k1gFnSc1	elektrárna
Služby	služba	k1gFnSc2	služba
(	(	kIx(	(
<g/>
výběr	výběr	k1gInSc1	výběr
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
18	[number]	k4	18
lékařů	lékař	k1gMnPc2	lékař
<g/>
,	,	kIx,	,
2	[number]	k4	2
zubní	zubní	k2eAgMnPc1d1	zubní
lékaři	lékař	k1gMnPc1	lékař
<g/>
,	,	kIx,	,
3	[number]	k4	3
zvěrolékaři	zvěrolékař	k1gMnPc7	zvěrolékař
<g/>
,	,	kIx,	,
5	[number]	k4	5
advokátů	advokát	k1gMnPc2	advokát
<g/>
,	,	kIx,	,
notář	notář	k1gMnSc1	notář
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
banka	banka	k1gFnSc1	banka
expozitura	expozitura	k1gFnSc1	expozitura
Poděbrady	Poděbrady	k1gInPc4	Poděbrady
<g/>
,	,	kIx,	,
biograf	biograf	k1gMnSc1	biograf
Sokol	Sokol	k1gMnSc1	Sokol
<g/>
,	,	kIx,	,
časopis	časopis	k1gInSc1	časopis
Obchodní	obchodní	k2eAgInSc1d1	obchodní
Rozhled	rozhled	k1gInSc1	rozhled
<g/>
,	,	kIx,	,
fotoateliér	fotoateliér	k1gInSc1	fotoateliér
<g/>
,	,	kIx,	,
3	[number]	k4	3
geometři	geometr	k1gMnPc1	geometr
<g/>
,	,	kIx,	,
hotely	hotel	k1gInPc1	hotel
(	(	kIx(	(
<g/>
Grandhotel	grandhotel	k1gInSc1	grandhotel
Urazil	urazit	k5eAaPmAgInS	urazit
<g/>
,	,	kIx,	,
Bylina	bylina	k1gFnSc1	bylina
<g/>
,	,	kIx,	,
Lázeňský	lázeňský	k1gMnSc1	lázeňský
<g/>
,	,	kIx,	,
Na	na	k7c6	na
Račanech	Račan	k1gInPc6	Račan
<g/>
,	,	kIx,	,
Občanská	občanský	k2eAgFnSc1d1	občanská
záložna	záložna	k1gFnSc1	záložna
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
2	[number]	k4	2
kavárny	kavárna	k1gFnPc4	kavárna
<g/>
,	,	kIx,	,
lázně	lázeň	k1gFnPc4	lázeň
léčivé	léčivý	k2eAgFnPc4d1	léčivá
<g/>
,	,	kIx,	,
léčebný	léčebný	k2eAgInSc1d1	léčebný
ústav	ústav	k1gInSc1	ústav
<g/>
,	,	kIx,	,
2	[number]	k4	2
lékárny	lékárna	k1gFnSc2	lékárna
<g/>
,	,	kIx,	,
12	[number]	k4	12
pensionů	pension	k1gInPc2	pension
<g/>
,	,	kIx,	,
7	[number]	k4	7
restaurací	restaurace	k1gFnPc2	restaurace
(	(	kIx(	(
<g/>
Buřil	Buřil	k1gInSc1	Buřil
<g/>
,	,	kIx,	,
Lázeňska	Lázeňsko	k1gNnPc1	Lázeňsko
<g/>
,	,	kIx,	,
Nádražní	nádražní	k2eAgNnPc1d1	nádražní
<g/>
,	,	kIx,	,
U	u	k7c2	u
Bílé	bílý	k2eAgFnSc2d1	bílá
růže	růž	k1gFnSc2	růž
<g/>
,	,	kIx,	,
U	u	k7c2	u
Orla	Orel	k1gMnSc2	Orel
<g/>
,	,	kIx,	,
Sala	Salus	k1gMnSc2	Salus
Terrena	Terren	k1gMnSc2	Terren
<g/>
,	,	kIx,	,
Na	na	k7c6	na
staré	starý	k2eAgFnSc6d1	stará
poště	pošta	k1gFnSc6	pošta
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Občanská	občanský	k2eAgFnSc1d1	občanská
záložna	záložna	k1gFnSc1	záložna
v	v	k7c6	v
Poděbradech	Poděbrady	k1gInPc6	Poděbrady
<g/>
,	,	kIx,	,
Okresní	okresní	k2eAgFnSc1d1	okresní
záložna	záložna	k1gFnSc1	záložna
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
<g/>
,	,	kIx,	,
Spořitelna	spořitelna	k1gFnSc1	spořitelna
městská	městský	k2eAgFnSc1d1	městská
v	v	k7c6	v
Poděbradech	Poděbrady	k1gInPc6	Poděbrady
<g/>
,	,	kIx,	,
Živnostensko-obchodnická	živnostenskobchodnický	k2eAgFnSc1d1	živnostensko-obchodnická
záložna	záložna	k1gFnSc1	záložna
<g/>
,	,	kIx,	,
5	[number]	k4	5
vináren	vinárna	k1gFnPc2	vinárna
<g/>
,	,	kIx,	,
2	[number]	k4	2
zubní	zubní	k2eAgInPc1d1	zubní
ateliéry	ateliér	k1gInPc1	ateliér
V	v	k7c6	v
obci	obec	k1gFnSc6	obec
Kluk	kluk	k1gMnSc1	kluk
(	(	kIx(	(
<g/>
410	[number]	k4	410
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
samostatná	samostatný	k2eAgFnSc1d1	samostatná
obec	obec	k1gFnSc1	obec
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
evidovány	evidovat	k5eAaImNgFnP	evidovat
tyto	tento	k3xDgFnPc1	tento
živnosti	živnost	k1gFnPc1	živnost
a	a	k8xC	a
obchody	obchod	k1gInPc1	obchod
<g/>
:	:	kIx,	:
autodílna	autodílna	k1gFnSc1	autodílna
bratří	bratřit	k5eAaImIp3nS	bratřit
Staňkové	Staňkové	k2eAgFnSc1d1	Staňkové
<g/>
,	,	kIx,	,
výroba	výroba	k1gFnSc1	výroba
cementového	cementový	k2eAgNnSc2d1	cementové
zboží	zboží	k1gNnSc2	zboží
<g/>
,	,	kIx,	,
2	[number]	k4	2
hostince	hostinec	k1gInSc2	hostinec
<g/>
,	,	kIx,	,
kovář	kovář	k1gMnSc1	kovář
<g/>
,	,	kIx,	,
10	[number]	k4	10
rolníků	rolník	k1gMnPc2	rolník
<g/>
,	,	kIx,	,
2	[number]	k4	2
obchody	obchod	k1gInPc4	obchod
se	s	k7c7	s
smíšeným	smíšený	k2eAgNnSc7d1	smíšené
zbožím	zboží	k1gNnSc7	zboží
<g/>
,	,	kIx,	,
trafika	trafika	k1gFnSc1	trafika
<g/>
,	,	kIx,	,
truhlář	truhlář	k1gMnSc1	truhlář
<g/>
,	,	kIx,	,
5	[number]	k4	5
zahradníků	zahradník	k1gMnPc2	zahradník
<g/>
,	,	kIx,	,
zámečník	zámečník	k1gMnSc1	zámečník
<g />
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vsi	ves	k1gFnSc6	ves
Přední	přední	k2eAgFnSc1d1	přední
Lhota	Lhota	k1gFnSc1	Lhota
(	(	kIx(	(
<g/>
512	[number]	k4	512
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
samostatná	samostatný	k2eAgFnSc1d1	samostatná
ves	ves	k1gFnSc1	ves
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
evidovány	evidovat	k5eAaImNgFnP	evidovat
tyto	tento	k3xDgFnPc1	tento
živnosti	živnost	k1gFnPc1	živnost
a	a	k8xC	a
obchody	obchod	k1gInPc1	obchod
<g/>
:	:	kIx,	:
autodílna	autodílna	k1gFnSc1	autodílna
<g/>
,	,	kIx,	,
obchod	obchod	k1gInSc1	obchod
s	s	k7c7	s
dobytkem	dobytek	k1gMnSc7	dobytek
<g/>
,	,	kIx,	,
hospodářské	hospodářský	k2eAgNnSc1d1	hospodářské
strojní	strojní	k2eAgNnSc1d1	strojní
družstvo	družstvo	k1gNnSc1	družstvo
<g/>
,	,	kIx,	,
2	[number]	k4	2
hostince	hostinec	k1gInSc2	hostinec
<g/>
,	,	kIx,	,
kolář	kolář	k1gMnSc1	kolář
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
kovář	kovář	k1gMnSc1	kovář
<g/>
,	,	kIx,	,
obuvník	obuvník	k1gMnSc1	obuvník
<g/>
,	,	kIx,	,
rolník	rolník	k1gMnSc1	rolník
<g/>
,	,	kIx,	,
2	[number]	k4	2
řezníci	řezník	k1gMnPc1	řezník
<g/>
,	,	kIx,	,
3	[number]	k4	3
obchody	obchod	k1gInPc4	obchod
se	s	k7c7	s
smíšeným	smíšený	k2eAgNnSc7d1	smíšené
zbožím	zboží	k1gNnSc7	zboží
<g/>
,	,	kIx,	,
soustružník	soustružník	k1gMnSc1	soustružník
<g/>
,	,	kIx,	,
trafika	trafika	k1gFnSc1	trafika
<g/>
,	,	kIx,	,
truhlář	truhlář	k1gMnSc1	truhlář
<g/>
,	,	kIx,	,
zahradník	zahradník	k1gMnSc1	zahradník
<g/>
,	,	kIx,	,
obchod	obchod	k1gInSc1	obchod
se	s	k7c7	s
zvěřinou	zvěřina	k1gFnSc7	zvěřina
a	a	k8xC	a
drůbeží	drůbež	k1gFnSc7	drůbež
V	v	k7c6	v
obci	obec	k1gFnSc6	obec
Velké	velký	k2eAgNnSc1d1	velké
Zboží	zboží	k1gNnSc1	zboží
(	(	kIx(	(
<g/>
520	[number]	k4	520
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
samostatná	samostatný	k2eAgFnSc1d1	samostatná
obec	obec	k1gFnSc1	obec
se	s	k7c7	s
<g />
.	.	kIx.	.
</s>
<s>
později	pozdě	k6eAd2	pozdě
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
evidovány	evidovat	k5eAaImNgFnP	evidovat
tyto	tento	k3xDgFnPc1	tento
živnosti	živnost	k1gFnPc1	živnost
a	a	k8xC	a
obchody	obchod	k1gInPc1	obchod
<g/>
:	:	kIx,	:
3	[number]	k4	3
hostince	hostinec	k1gInSc2	hostinec
<g/>
,	,	kIx,	,
kolář	kolář	k1gMnSc1	kolář
<g/>
,	,	kIx,	,
konsum	konsum	k1gInSc1	konsum
<g/>
,	,	kIx,	,
krejčí	krejčí	k1gMnSc1	krejčí
<g/>
,	,	kIx,	,
obuvník	obuvník	k1gMnSc1	obuvník
<g/>
,	,	kIx,	,
sedlář	sedlář	k1gMnSc1	sedlář
<g/>
,	,	kIx,	,
obchod	obchod	k1gInSc1	obchod
se	s	k7c7	s
smíšeným	smíšený	k2eAgNnSc7d1	smíšené
zbožím	zboží	k1gNnSc7	zboží
<g/>
,	,	kIx,	,
švadlena	švadlena	k1gFnSc1	švadlena
<g/>
,	,	kIx,	,
2	[number]	k4	2
trafiky	trafika	k1gFnSc2	trafika
<g/>
,	,	kIx,	,
zahradník	zahradník	k1gMnSc1	zahradník
Město	město	k1gNnSc4	město
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
rovinaté	rovinatý	k2eAgFnSc6d1	rovinatá
krajině	krajina	k1gFnSc6	krajina
polabské	polabský	k2eAgFnSc2d1	Polabská
nížiny	nížina	k1gFnSc2	nížina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
málo	málo	k6eAd1	málo
zalesněná	zalesněný	k2eAgFnSc1d1	zalesněná
a	a	k8xC	a
intenzivně	intenzivně	k6eAd1	intenzivně
zemědělsky	zemědělsky	k6eAd1	zemědělsky
užívaná	užívaný	k2eAgNnPc4d1	užívané
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
na	na	k7c6	na
obou	dva	k4xCgInPc6	dva
březích	břeh	k1gInPc6	břeh
řeky	řeka	k1gFnSc2	řeka
Labe	Labe	k1gNnSc2	Labe
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
184	[number]	k4	184
<g/>
–	–	k?	–
<g/>
190	[number]	k4	190
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
V	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
pouze	pouze	k6eAd1	pouze
tři	tři	k4xCgInPc4	tři
kopce	kopec	k1gInPc4	kopec
<g/>
,	,	kIx,	,
kterými	který	k3yQgMnPc7	který
jsou	být	k5eAaImIp3nP	být
Oškobrh	Oškobrh	k1gMnSc1	Oškobrh
(	(	kIx(	(
<g/>
285	[number]	k4	285
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Chotuc	Chotuc	k1gFnSc1	Chotuc
(	(	kIx(	(
<g/>
254	[number]	k4	254
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
a	a	k8xC	a
kopec	kopec	k1gInSc1	kopec
Sadský	sadský	k2eAgInSc1d1	sadský
(	(	kIx(	(
<g/>
213	[number]	k4	213
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
zdymadlem	zdymadlo	k1gNnSc7	zdymadlo
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
slepé	slepý	k2eAgNnSc1d1	slepé
rameno	rameno	k1gNnSc1	rameno
Labe	Labe	k1gNnSc2	Labe
zvané	zvaný	k2eAgFnSc2d1	zvaná
Skupice	Skupice	k1gFnSc2	Skupice
(	(	kIx(	(
<g/>
pozůstatek	pozůstatek	k1gInSc1	pozůstatek
toku	tok	k1gInSc2	tok
Labe	Labe	k1gNnSc2	Labe
před	před	k7c7	před
regulací	regulace	k1gFnSc7	regulace
<g/>
)	)	kIx)	)
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
směrem	směr	k1gInSc7	směr
na	na	k7c6	na
Libici	Libice	k1gFnSc6	Libice
nad	nad	k7c7	nad
Cidlinou	Cidlina	k1gFnSc7	Cidlina
je	on	k3xPp3gFnPc4	on
národní	národní	k2eAgFnPc4d1	národní
přírodní	přírodní	k2eAgFnPc4d1	přírodní
rezervace	rezervace	k1gFnPc4	rezervace
Libický	Libický	k2eAgInSc1d1	Libický
luh	luh	k1gInSc1	luh
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
pozůstatkem	pozůstatek	k1gInSc7	pozůstatek
lužných	lužná	k1gFnPc2	lužná
lesů	les	k1gInPc2	les
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgInPc1d2	veliký
lesní	lesní	k2eAgInPc1d1	lesní
celky	celek	k1gInPc1	celek
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
právě	právě	k9	právě
především	především	k9	především
mezi	mezi	k7c7	mezi
Libicí	Libice	k1gFnSc7	Libice
nad	nad	k7c7	nad
Cidlinou	Cidlina	k1gFnSc7	Cidlina
a	a	k8xC	a
Velkým	velký	k2eAgInSc7d1	velký
Osekem	Osek	k1gInSc7	Osek
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgInPc1d2	menší
lesy	les	k1gInPc1	les
pak	pak	k6eAd1	pak
leží	ležet	k5eAaImIp3nP	ležet
především	především	k9	především
podél	podél	k7c2	podél
řeky	řeka	k1gFnSc2	řeka
Labe	Labe	k1gNnSc2	Labe
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
les	les	k1gInSc1	les
Bor	bor	k1gInSc1	bor
<g/>
,	,	kIx,	,
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Polabí	Polabí	k1gNnSc1	Polabí
má	mít	k5eAaImIp3nS	mít
poměrně	poměrně	k6eAd1	poměrně
teplé	teplý	k2eAgNnSc4d1	teplé
podnebí	podnebí	k1gNnSc4	podnebí
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
roční	roční	k2eAgFnSc1d1	roční
teplota	teplota	k1gFnSc1	teplota
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
9	[number]	k4	9
°	°	k?	°
<g/>
C.	C.	kA	C.
Jaro	jaro	k1gNnSc1	jaro
začíná	začínat	k5eAaImIp3nS	začínat
poměrně	poměrně	k6eAd1	poměrně
brzy	brzy	k6eAd1	brzy
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
dubna	duben	k1gInSc2	duben
<g/>
)	)	kIx)	)
a	a	k8xC	a
zima	zima	k1gFnSc1	zima
nastupuje	nastupovat	k5eAaImIp3nS	nastupovat
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Nejstudenější	studený	k2eAgInSc1d3	nejstudenější
bývá	bývat	k5eAaImIp3nS	bývat
měsíc	měsíc	k1gInSc1	měsíc
leden	leden	k1gInSc4	leden
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jsou	být	k5eAaImIp3nP	být
časté	častý	k2eAgInPc1d1	častý
mrazy	mráz	k1gInPc1	mráz
a	a	k8xC	a
inverzní	inverzní	k2eAgFnSc1d1	inverzní
mlha	mlha	k1gFnSc1	mlha
<g/>
.	.	kIx.	.
</s>
<s>
Trvalejší	trvalý	k2eAgFnSc1d2	trvalejší
sněhová	sněhový	k2eAgFnSc1d1	sněhová
pokrývka	pokrývka	k1gFnSc1	pokrývka
není	být	k5eNaImIp3nS	být
obvyklá	obvyklý	k2eAgFnSc1d1	obvyklá
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
roční	roční	k2eAgInSc1d1	roční
úhrn	úhrn	k1gInSc1	úhrn
srážek	srážka	k1gFnPc2	srážka
je	být	k5eAaImIp3nS	být
nízký	nízký	k2eAgInSc1d1	nízký
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
tyto	tento	k3xDgFnPc4	tento
národní	národní	k2eAgFnPc4d1	národní
přírodní	přírodní	k2eAgFnPc4d1	přírodní
rezervace	rezervace	k1gFnPc4	rezervace
a	a	k8xC	a
přírodní	přírodní	k2eAgFnPc4d1	přírodní
památky	památka	k1gFnPc4	památka
<g/>
:	:	kIx,	:
NPR	NPR	kA	NPR
Libický	Libický	k2eAgInSc1d1	Libický
luh	luh	k1gInSc1	luh
<g/>
,	,	kIx,	,
NPR	NPR	kA	NPR
Žehuňská	Žehuňský	k2eAgFnSc1d1	Žehuňská
obora	obora	k1gFnSc1	obora
a	a	k8xC	a
Žehuňský	Žehuňský	k2eAgInSc1d1	Žehuňský
rybník	rybník	k1gInSc1	rybník
<g/>
,	,	kIx,	,
PP	PP	kA	PP
Písečný	písečný	k2eAgInSc1d1	písečný
přesyp	přesyp	k1gInSc1	přesyp
u	u	k7c2	u
Osečka	Osečko	k1gNnSc2	Osečko
<g/>
,	,	kIx,	,
PP	PP	kA	PP
Báň	báň	k1gFnSc4	báň
<g/>
,	,	kIx,	,
PP	PP	kA	PP
Vinný	vinný	k2eAgInSc4d1	vinný
vrch	vrch	k1gInSc4	vrch
Rozloha	rozloha	k1gFnSc1	rozloha
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
celkem	celkem	k6eAd1	celkem
3369,7	[number]	k4	3369,7
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
<s>
Poděbrady	Poděbrady	k1gInPc1	Poděbrady
tvoří	tvořit	k5eAaImIp3nP	tvořit
celkem	celkem	k6eAd1	celkem
pět	pět	k4xCc4	pět
katastrálních	katastrální	k2eAgNnPc2d1	katastrální
území	území	k1gNnPc2	území
<g/>
.	.	kIx.	.
</s>
<s>
K.	K.	kA	K.
ú.	ú.	k?	ú.
Poděbrady	Poděbrady	k1gInPc4	Poděbrady
(	(	kIx(	(
<g/>
1353,39	[number]	k4	1353,39
ha	ha	kA	ha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
k.	k.	k?	k.
ú.	ú.	k?	ú.
Polabec	Polabec	k1gInSc1	Polabec
(	(	kIx(	(
<g/>
256,34	[number]	k4	256,34
ha	ha	kA	ha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
k.	k.	k?	k.
ú.	ú.	k?	ú.
Velké	velký	k2eAgNnSc1d1	velké
Zboží	zboží	k1gNnSc1	zboží
(	(	kIx(	(
<g/>
493,31	[number]	k4	493,31
ha	ha	kA	ha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
k.	k.	k?	k.
ú.	ú.	k?	ú.
Kluk	kluk	k1gMnSc1	kluk
(	(	kIx(	(
<g/>
868,25	[number]	k4	868,25
ha	ha	kA	ha
<g/>
)	)	kIx)	)
a	a	k8xC	a
k.	k.	k?	k.
ú.	ú.	k?	ú.
Přední	přední	k2eAgFnSc1d1	přední
Lhota	Lhota	k1gFnSc1	Lhota
(	(	kIx(	(
<g/>
398,35	[number]	k4	398,35
ha	ha	kA	ha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
šlechtické	šlechtický	k2eAgNnSc1d1	šlechtické
sídlo	sídlo	k1gNnSc1	sídlo
přímo	přímo	k6eAd1	přímo
nazvané	nazvaný	k2eAgInPc4d1	nazvaný
Poděbrady	Poděbrady	k1gInPc4	Poděbrady
stálo	stát	k5eAaImAgNnS	stát
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
dnešního	dnešní	k2eAgInSc2d1	dnešní
hradu	hrad	k1gInSc2	hrad
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
původní	původní	k2eAgInSc1d1	původní
vchod	vchod	k1gInSc1	vchod
byl	být	k5eAaImAgInS	být
z	z	k7c2	z
východní	východní	k2eAgFnSc2d1	východní
strany	strana	k1gFnSc2	strana
směrem	směr	k1gInSc7	směr
ke	k	k7c3	k
kostelu	kostel	k1gInSc3	kostel
Povýšení	povýšení	k1gNnSc2	povýšení
sv.	sv.	kA	sv.
Kříže	kříž	k1gInSc2	kříž
a	a	k8xC	a
do	do	k7c2	do
okolí	okolí	k1gNnSc2	okolí
kostela	kostel	k1gInSc2	kostel
se	se	k3xPyFc4	se
přeneslo	přenést	k5eAaPmAgNnS	přenést
zdejší	zdejší	k2eAgNnSc1d1	zdejší
osídlení	osídlení	k1gNnSc1	osídlení
<g/>
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
založil	založit	k5eAaPmAgMnS	založit
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
straně	strana	k1gFnSc6	strana
při	při	k7c6	při
křižovatce	křižovatka	k1gFnSc6	křižovatka
cest	cesta	k1gFnPc2	cesta
zcela	zcela	k6eAd1	zcela
nové	nový	k2eAgNnSc1d1	nové
městečko	městečko	k1gNnSc1	městečko
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
natrvalo	natrvalo	k6eAd1	natrvalo
stalo	stát	k5eAaPmAgNnS	stát
novým	nový	k2eAgInSc7d1	nový
městským	městský	k2eAgInSc7d1	městský
centrem	centr	k1gInSc7	centr
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
potvrdilo	potvrdit	k5eAaPmAgNnS	potvrdit
i	i	k9	i
budování	budování	k1gNnSc1	budování
městského	městský	k2eAgNnSc2d1	Městské
opevnění	opevnění	k1gNnSc2	opevnění
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
chránilo	chránit	k5eAaImAgNnS	chránit
hrad	hrad	k1gInSc4	hrad
a	a	k8xC	a
toto	tento	k3xDgNnSc1	tento
městečko	městečko	k1gNnSc1	městečko
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
s	s	k7c7	s
okolím	okolí	k1gNnSc7	okolí
(	(	kIx(	(
<g/>
Kostelní	kostelní	k2eAgNnSc1d1	kostelní
předměstí	předměstí	k1gNnSc1	předměstí
<g/>
)	)	kIx)	)
a	a	k8xC	a
Nymburské	nymburský	k2eAgNnSc4d1	nymburské
předměstí	předměstí	k1gNnSc4	předměstí
ovšem	ovšem	k9	ovšem
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
vně	vně	k7c2	vně
hradeb	hradba	k1gFnPc2	hradba
<g/>
.	.	kIx.	.
</s>
<s>
Opevnění	opevnění	k1gNnSc1	opevnění
zaniklo	zaniknout	k5eAaPmAgNnS	zaniknout
koncem	konec	k1gInSc7	konec
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
a	a	k8xC	a
nový	nový	k2eAgInSc1d1	nový
rozvoj	rozvoj	k1gInSc1	rozvoj
města	město	k1gNnSc2	město
nastal	nastat	k5eAaPmAgInS	nastat
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Východně	východně	k6eAd1	východně
od	od	k7c2	od
kostela	kostel	k1gInSc2	kostel
Povýšení	povýšení	k1gNnSc2	povýšení
sv.	sv.	kA	sv.
Kříže	kříž	k1gInSc2	kříž
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
nová	nový	k2eAgFnSc1d1	nová
čtvrť	čtvrť	k1gFnSc1	čtvrť
Bělidla	bělidlo	k1gNnSc2	bělidlo
a	a	k8xC	a
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
straně	strana	k1gFnSc6	strana
vysušeného	vysušený	k2eAgInSc2d1	vysušený
Předměstského	předměstský	k2eAgInSc2d1	předměstský
rybníka	rybník	k1gInSc2	rybník
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
čtvrť	čtvrť	k1gFnSc1	čtvrť
Hráz	hráz	k1gFnSc1	hráz
<g/>
.	.	kIx.	.
</s>
<s>
Rozrůstalo	rozrůstat	k5eAaImAgNnS	rozrůstat
se	se	k3xPyFc4	se
také	také	k9	také
Nymburské	nymburský	k2eAgNnSc1d1	nymburské
předměstí	předměstí	k1gNnSc1	předměstí
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
lázní	lázeň	k1gFnPc2	lázeň
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
rozrůstalo	rozrůstat	k5eAaImAgNnS	rozrůstat
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
železniční	železniční	k2eAgFnSc3d1	železniční
trati	trať	k1gFnSc3	trať
a	a	k8xC	a
za	za	k7c2	za
ní	on	k3xPp3gFnSc2	on
vyrostla	vyrůst	k5eAaPmAgFnS	vyrůst
nová	nový	k2eAgFnSc1d1	nová
místní	místní	k2eAgFnSc1d1	místní
část	část	k1gFnSc1	část
Žižkov	Žižkov	k1gInSc1	Žižkov
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
čtvrti	čtvrt	k1gFnPc1	čtvrt
moderního	moderní	k2eAgNnSc2d1	moderní
zahradního	zahradní	k2eAgNnSc2d1	zahradní
města	město	k1gNnSc2	město
byly	být	k5eAaImAgFnP	být
budovány	budovat	k5eAaImNgFnP	budovat
podle	podle	k7c2	podle
regulačního	regulační	k2eAgInSc2d1	regulační
plánu	plán	k1gInSc2	plán
architekta	architekt	k1gMnSc2	architekt
Františka	František	k1gMnSc2	František
Jandy	Janda	k1gMnSc2	Janda
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
v	v	k7c6	v
Poděbradech	Poděbrady	k1gInPc6	Poděbrady
<g/>
.	.	kIx.	.
</s>
<s>
Dominanty	dominanta	k1gFnPc1	dominanta
Jiřího	Jiří	k1gMnSc2	Jiří
náměstí	náměstí	k1gNnSc2	náměstí
tvoří	tvořit	k5eAaImIp3nS	tvořit
poděbradský	poděbradský	k2eAgInSc4d1	poděbradský
zámek	zámek	k1gInSc4	zámek
a	a	k8xC	a
pomník	pomník	k1gInSc4	pomník
Jiřího	Jiří	k1gMnSc2	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
od	od	k7c2	od
sochaře	sochař	k1gMnSc2	sochař
Bohuslava	Bohuslava	k1gFnSc1	Bohuslava
Schnircha	Schnircha	k1gFnSc1	Schnircha
<g/>
,	,	kIx,	,
patřící	patřící	k2eAgFnSc1d1	patřící
k	k	k7c3	k
vrcholům	vrchol	k1gInPc3	vrchol
české	český	k2eAgFnSc2d1	Česká
monumentální	monumentální	k2eAgFnSc2d1	monumentální
plastiky	plastika	k1gFnSc2	plastika
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
před	před	k7c7	před
jeho	jeho	k3xOp3gFnSc7	jeho
branou	brána	k1gFnSc7	brána
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
stojí	stát	k5eAaImIp3nS	stát
také	také	k9	také
Mariánský	mariánský	k2eAgInSc1d1	mariánský
sloup	sloup	k1gInSc1	sloup
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1765	[number]	k4	1765
<g/>
,	,	kIx,	,
postavený	postavený	k2eAgInSc4d1	postavený
na	na	k7c4	na
památku	památka	k1gFnSc4	památka
morové	morový	k2eAgFnSc2d1	morová
epidemie	epidemie	k1gFnSc2	epidemie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1714	[number]	k4	1714
<g/>
.	.	kIx.	.
</s>
<s>
Budova	budova	k1gFnSc1	budova
občanské	občanský	k2eAgFnSc2d1	občanská
záložny	záložna	k1gFnSc2	záložna
byla	být	k5eAaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1899	[number]	k4	1899
<g/>
.	.	kIx.	.
</s>
<s>
Renesanční	renesanční	k2eAgFnSc1d1	renesanční
stará	starý	k2eAgFnSc1d1	stará
radnice	radnice	k1gFnSc1	radnice
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
sídlem	sídlo	k1gNnSc7	sídlo
městské	městský	k2eAgFnSc2d1	městská
knihovny	knihovna	k1gFnSc2	knihovna
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
současná	současný	k2eAgFnSc1d1	současná
radnice	radnice	k1gFnSc1	radnice
byla	být	k5eAaImAgFnS	být
vystavěna	vystavět	k5eAaPmNgFnS	vystavět
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
v	v	k7c6	v
novorenesančním	novorenesanční	k2eAgInSc6d1	novorenesanční
slohu	sloh	k1gInSc6	sloh
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Jiřího	Jiří	k1gMnSc4	Jiří
náměstí	náměstí	k1gNnSc2	náměstí
navazuje	navazovat	k5eAaImIp3nS	navazovat
rozsáhlý	rozsáhlý	k2eAgInSc1d1	rozsáhlý
a	a	k8xC	a
v	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
letech	let	k1gInPc6	let
rekonstruovaný	rekonstruovaný	k2eAgInSc1d1	rekonstruovaný
lázeňský	lázeňský	k2eAgInSc1d1	lázeňský
park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
sahá	sahat	k5eAaImIp3nS	sahat
až	až	k9	až
k	k	k7c3	k
nádražní	nádražní	k2eAgFnSc3d1	nádražní
budově	budova	k1gFnSc3	budova
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1932	[number]	k4	1932
<g/>
.	.	kIx.	.
</s>
<s>
Poděbradské	poděbradský	k2eAgNnSc1d1	Poděbradské
nádraží	nádraží	k1gNnSc1	nádraží
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
architektonických	architektonický	k2eAgInPc2d1	architektonický
skvostů	skvost	k1gInPc2	skvost
a	a	k8xC	a
vůbec	vůbec	k9	vůbec
první	první	k4xOgMnSc1	první
funkcionalistickým	funkcionalistický	k2eAgNnSc7d1	funkcionalistické
nádražím	nádraží	k1gNnSc7	nádraží
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
samotném	samotný	k2eAgNnSc6d1	samotné
centru	centrum	k1gNnSc6	centrum
parku	park	k1gInSc2	park
je	být	k5eAaImIp3nS	být
Libenského	Libenský	k2eAgMnSc4d1	Libenský
kolonáda	kolonáda	k1gFnSc1	kolonáda
postavená	postavený	k2eAgFnSc1d1	postavená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
podle	podle	k7c2	podle
projektu	projekt	k1gInSc2	projekt
arch	archa	k1gFnPc2	archa
<g/>
.	.	kIx.	.
</s>
<s>
Vojtěcha	Vojtěch	k1gMnSc4	Vojtěch
Kerharta	Kerhart	k1gMnSc4	Kerhart
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
parku	park	k1gInSc6	park
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
známé	známý	k2eAgFnPc4d1	známá
květinové	květinový	k2eAgFnPc4d1	květinová
hodiny	hodina	k1gFnPc4	hodina
<g/>
,	,	kIx,	,
poblíž	poblíž	k7c2	poblíž
kterých	který	k3yRgMnPc2	který
stojí	stát	k5eAaImIp3nS	stát
kontroverzní	kontroverzní	k2eAgMnSc1d1	kontroverzní
trpaslík	trpaslík	k1gMnSc1	trpaslík
odbíjející	odbíjející	k2eAgInSc4d1	odbíjející
čas	čas	k1gInSc4	čas
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
obvodu	obvod	k1gInSc6	obvod
lázeňského	lázeňský	k2eAgInSc2d1	lázeňský
parku	park	k1gInSc2	park
stojí	stát	k5eAaImIp3nS	stát
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
významných	významný	k2eAgFnPc2d1	významná
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
vila	vít	k5eAaImAgFnS	vít
Kouřimka	Kouřimka	k1gFnSc1	Kouřimka
a	a	k8xC	a
Obereignerova	Obereignerův	k2eAgFnSc1d1	Obereignerův
vila	vila	k1gFnSc1	vila
navržené	navržený	k2eAgFnSc2d1	navržená
architektem	architekt	k1gMnSc7	architekt
Josefem	Josef	k1gMnSc7	Josef
Fantou	Fanta	k1gMnSc7	Fanta
<g/>
,	,	kIx,	,
hotel	hotel	k1gInSc1	hotel
Tlapák	Tlapák	k1gInSc1	Tlapák
<g/>
,	,	kIx,	,
lázeňský	lázeňský	k2eAgInSc1d1	lázeňský
dům	dům	k1gInSc1	dům
Libenský	Libenský	k2eAgInSc1d1	Libenský
či	či	k8xC	či
budova	budova	k1gFnSc1	budova
Knížecích	knížecí	k2eAgFnPc2d1	knížecí
lázní	lázeň	k1gFnPc2	lázeň
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
budov	budova	k1gFnPc2	budova
na	na	k7c6	na
Riegrově	Riegrův	k2eAgNnSc6d1	Riegrovo
náměstí	náměstí	k1gNnSc6	náměstí
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
první	první	k4xOgFnSc2	první
třetiny	třetina	k1gFnSc2	třetina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
budova	budova	k1gFnSc1	budova
městské	městský	k2eAgFnSc2d1	městská
spořitelny	spořitelna	k1gFnSc2	spořitelna
od	od	k7c2	od
Josefa	Josef	k1gMnSc2	Josef
Kerharta	Kerhart	k1gMnSc2	Kerhart
<g/>
.	.	kIx.	.
</s>
<s>
Funkcionalismus	funkcionalismus	k1gInSc4	funkcionalismus
reprezentují	reprezentovat	k5eAaImIp3nP	reprezentovat
též	též	k9	též
budova	budova	k1gFnSc1	budova
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
školy	škola	k1gFnSc2	škola
a	a	k8xC	a
budova	budova	k1gFnSc1	budova
pošty	pošta	k1gFnSc2	pošta
architekta	architekt	k1gMnSc2	architekt
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
Kerharta	Kerharta	k1gFnSc1	Kerharta
<g/>
,	,	kIx,	,
Újezdní	újezdní	k2eAgFnPc1d1	újezdní
Masarykovy	Masarykův	k2eAgFnPc1d1	Masarykova
škola	škola	k1gFnSc1	škola
a	a	k8xC	a
vodárna	vodárna	k1gFnSc1	vodárna
od	od	k7c2	od
Františka	František	k1gMnSc2	František
Jandy	Janda	k1gMnSc2	Janda
<g/>
,	,	kIx,	,
budova	budova	k1gFnSc1	budova
sokolovny	sokolovna	k1gFnSc2	sokolovna
od	od	k7c2	od
Františka	František	k1gMnSc2	František
Profta	Proft	k1gMnSc2	Proft
či	či	k8xC	či
kostel	kostel	k1gInSc4	kostel
Církve	církev	k1gFnSc2	církev
československé	československý	k2eAgFnSc2d1	Československá
husitské	husitský	k2eAgFnSc2d1	husitská
od	od	k7c2	od
Josefa	Josef	k1gMnSc4	Josef
Semeráda	Semerád	k1gMnSc2	Semerád
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
lázeňského	lázeňský	k2eAgInSc2d1	lázeňský
středu	střed	k1gInSc2	střed
též	též	k9	též
byla	být	k5eAaImAgFnS	být
vybudována	vybudován	k2eAgFnSc1d1	vybudována
řada	řada	k1gFnSc1	řada
moderních	moderní	k2eAgFnPc2d1	moderní
vil	vila	k1gFnPc2	vila
a	a	k8xC	a
penzionů	penzion	k1gInPc2	penzion
<g/>
.	.	kIx.	.
</s>
<s>
Nedaleko	nedaleko	k7c2	nedaleko
zámku	zámek	k1gInSc2	zámek
stojí	stát	k5eAaImIp3nS	stát
kostel	kostel	k1gInSc4	kostel
Povýšení	povýšení	k1gNnSc2	povýšení
svatého	svatý	k2eAgMnSc2d1	svatý
Kříže	Kříž	k1gMnSc2	Kříž
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Labi	Labe	k1gNnSc6	Labe
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
hydroelektrárna	hydroelektrárna	k1gFnSc1	hydroelektrárna
se	s	k7c7	s
zdymadlem	zdymadlo	k1gNnSc7	zdymadlo
<g/>
,	,	kIx,	,
navržená	navržený	k2eAgFnSc1d1	navržená
poděbradským	poděbradský	k2eAgMnSc7d1	poděbradský
rodákem	rodák	k1gMnSc7	rodák
Antonínem	Antonín	k1gMnSc7	Antonín
Engelem	Engel	k1gMnSc7	Engel
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
stavba	stavba	k1gFnSc1	stavba
národní	národní	k2eAgFnSc1d1	národní
kulturní	kulturní	k2eAgFnSc7d1	kulturní
památkou	památka	k1gFnSc7	památka
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
golfového	golfový	k2eAgInSc2d1	golfový
areálu	areál	k1gInSc2	areál
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
budova	budova	k1gFnSc1	budova
bývalé	bývalý	k2eAgFnSc2d1	bývalá
Radiotelegrafické	radiotelegrafický	k2eAgFnSc2d1	radiotelegrafická
stanice	stanice	k1gFnSc2	stanice
s	s	k7c7	s
dvojicí	dvojice	k1gFnSc7	dvojice
stopadesátimetrových	stopadesátimetrový	k2eAgInPc2d1	stopadesátimetrový
stožárů	stožár	k1gInPc2	stožár
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
technickou	technický	k2eAgFnSc7d1	technická
památkou	památka	k1gFnSc7	památka
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
sčítání	sčítání	k1gNnSc6	sčítání
obyvatel	obyvatel	k1gMnPc2	obyvatel
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1869	[number]	k4	1869
měly	mít	k5eAaImAgInP	mít
Poděbrady	Poděbrady	k1gInPc1	Poděbrady
4976	[number]	k4	4976
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
století	století	k1gNnSc6	století
tento	tento	k3xDgInSc1	tento
počet	počet	k1gInSc1	počet
narostl	narůst	k5eAaPmAgInS	narůst
na	na	k7c4	na
6738	[number]	k4	6738
<g/>
,	,	kIx,	,
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
na	na	k7c4	na
12	[number]	k4	12
414	[number]	k4	414
obyvatel	obyvatel	k1gMnPc2	obyvatel
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
a	a	k8xC	a
prozatím	prozatím	k6eAd1	prozatím
nejvíce	hodně	k6eAd3	hodně
<g/>
,	,	kIx,	,
celkem	celkem	k6eAd1	celkem
13	[number]	k4	13
782	[number]	k4	782
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
mělo	mít	k5eAaImAgNnS	mít
město	město	k1gNnSc1	město
při	při	k7c6	při
sčítání	sčítání	k1gNnSc6	sčítání
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
zkratce	zkratka	k1gFnSc6	zkratka
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1961	[number]	k4	1961
počet	počet	k1gInSc4	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
města	město	k1gNnSc2	město
mírně	mírně	k6eAd1	mírně
stagnuje	stagnovat	k5eAaImIp3nS	stagnovat
na	na	k7c4	na
přibližně	přibližně	k6eAd1	přibližně
13	[number]	k4	13
tisících	tisící	k4xOgInPc6	tisící
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
zájmových	zájmový	k2eAgFnPc2d1	zájmová
a	a	k8xC	a
volnočasových	volnočasův	k2eAgFnPc2d1	volnočasův
organizací	organizace	k1gFnPc2	organizace
a	a	k8xC	a
skupin	skupina	k1gFnPc2	skupina
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
areálu	areál	k1gInSc6	areál
zámku	zámek	k1gInSc2	zámek
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Divadlo	divadlo	k1gNnSc1	divadlo
Na	na	k7c6	na
Kovárně	kovárna	k1gFnSc6	kovárna
a	a	k8xC	a
také	také	k9	také
Zámecký	zámecký	k2eAgInSc4d1	zámecký
biograf	biograf	k1gInSc4	biograf
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yIgInSc2	který
se	se	k3xPyFc4	se
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
z	z	k7c2	z
prvního	první	k4xOgNnSc2	první
nádvoří	nádvoří	k1gNnSc2	nádvoří
<g/>
.	.	kIx.	.
</s>
<s>
Divadlo	divadlo	k1gNnSc1	divadlo
má	mít	k5eAaImIp3nS	mít
kapacitu	kapacita	k1gFnSc4	kapacita
celkem	celkem	k6eAd1	celkem
233	[number]	k4	233
sedadel	sedadlo	k1gNnPc2	sedadlo
<g/>
.	.	kIx.	.
</s>
<s>
Dlouholetou	dlouholetý	k2eAgFnSc4d1	dlouholetá
tradici	tradice	k1gFnSc4	tradice
má	mít	k5eAaImIp3nS	mít
Divadelní	divadelní	k2eAgInSc1d1	divadelní
spolek	spolek	k1gInSc1	spolek
Jiří	Jiří	k1gMnSc2	Jiří
<g/>
,	,	kIx,	,
založený	založený	k2eAgInSc1d1	založený
roku	rok	k1gInSc2	rok
1861	[number]	k4	1861
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
naplno	naplno	k6eAd1	naplno
opět	opět	k6eAd1	opět
obnovil	obnovit	k5eAaPmAgMnS	obnovit
svou	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
po	po	k7c6	po
rekonstrukci	rekonstrukce	k1gFnSc6	rekonstrukce
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Zámecký	zámecký	k2eAgInSc1d1	zámecký
biograf	biograf	k1gInSc1	biograf
nahradil	nahradit	k5eAaPmAgInS	nahradit
původní	původní	k2eAgNnSc4d1	původní
Městské	městský	k2eAgNnSc4d1	Městské
kino	kino	k1gNnSc4	kino
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
Sokola	Sokol	k1gMnSc2	Sokol
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
budově	budova	k1gFnSc6	budova
bývalé	bývalý	k2eAgFnSc2d1	bývalá
školy	škola	k1gFnSc2	škola
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Na	na	k7c6	na
Dláždění	dláždění	k1gNnSc6	dláždění
dnes	dnes	k6eAd1	dnes
sídlí	sídlet	k5eAaImIp3nS	sídlet
Polabské	polabský	k2eAgNnSc1d1	Polabské
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Poděbradech	Poděbrady	k1gInPc6	Poděbrady
i	i	k8xC	i
budova	budova	k1gFnSc1	budova
středověkého	středověký	k2eAgInSc2d1	středověký
špitálu	špitál	k1gInSc2	špitál
Kunhuta	Kunhuta	k1gFnSc1	Kunhuta
s	s	k7c7	s
vysokým	vysoký	k2eAgNnSc7d1	vysoké
barokním	barokní	k2eAgNnSc7d1	barokní
průčelím	průčelí	k1gNnSc7	průčelí
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
založila	založit	k5eAaPmAgFnS	založit
Kunhuta	Kunhuta	k1gFnSc1	Kunhuta
ze	z	k7c2	z
Šternberka	Šternberk	k1gInSc2	Šternberk
a	a	k8xC	a
památník	památník	k1gInSc1	památník
krále	král	k1gMnSc2	král
Jiřího	Jiří	k1gMnSc2	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
bývalé	bývalý	k2eAgFnSc6d1	bývalá
hradní	hradní	k2eAgFnSc6d1	hradní
kapli	kaple	k1gFnSc6	kaple
a	a	k8xC	a
rodné	rodný	k2eAgFnSc3d1	rodná
síni	síň	k1gFnSc3	síň
krále	král	k1gMnSc2	král
Jiřího	Jiří	k1gMnSc2	Jiří
<g/>
,	,	kIx,	,
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
nádvoří	nádvoří	k1gNnSc6	nádvoří
zámku	zámek	k1gInSc2	zámek
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
památníku	památník	k1gInSc2	památník
je	být	k5eAaImIp3nS	být
expozice	expozice	k1gFnSc1	expozice
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Schnirch	Schnirch	k1gInSc1	Schnirch
a	a	k8xC	a
jeho	on	k3xPp3gInSc4	on
pomník	pomník	k1gInSc4	pomník
krále	král	k1gMnSc2	král
Jiřího	Jiří	k1gMnSc2	Jiří
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
dřevěné	dřevěný	k2eAgInPc1d1	dřevěný
monoxyly	monoxyl	k1gInPc1	monoxyl
z	z	k7c2	z
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
město	město	k1gNnSc4	město
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
muzea	muzeum	k1gNnSc2	muzeum
Muzeum	muzeum	k1gNnSc1	muzeum
Bedřicha	Bedřich	k1gMnSc2	Bedřich
Hrozného	hrozný	k2eAgMnSc2d1	hrozný
Lysá	Lysá	k1gFnSc1	Lysá
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
,	,	kIx,	,
Galerie	galerie	k1gFnSc1	galerie
Melantrich	Melantricha	k1gFnPc2	Melantricha
Rožďalovice	Rožďalovice	k1gFnSc2	Rožďalovice
<g/>
,	,	kIx,	,
Městské	městský	k2eAgNnSc4d1	Městské
muzeum	muzeum	k1gNnSc4	muzeum
Sadská	sadský	k2eAgFnSc1d1	Sadská
a	a	k8xC	a
Muzeum	muzeum	k1gNnSc1	muzeum
Královéměstecka	Královéměstecko	k1gNnSc2	Královéměstecko
Městec	Městec	k1gInSc4	Městec
Králové	Králová	k1gFnSc2	Králová
<g/>
.	.	kIx.	.
</s>
<s>
Městská	městský	k2eAgFnSc1d1	městská
knihovna	knihovna	k1gFnSc1	knihovna
Poděbrady	Poděbrady	k1gInPc1	Poděbrady
sídlí	sídlet	k5eAaImIp3nS	sídlet
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1930	[number]	k4	1930
dodnes	dodnes	k6eAd1	dodnes
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
renesanční	renesanční	k2eAgFnSc2d1	renesanční
staré	starý	k2eAgFnSc2d1	stará
radnice	radnice	k1gFnSc2	radnice
na	na	k7c4	na
Jiřího	Jiří	k1gMnSc4	Jiří
náměstí	náměstí	k1gNnSc2	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
lázeňského	lázeňský	k2eAgInSc2d1	lázeňský
parku	park	k1gInSc2	park
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
komerční	komerční	k2eAgFnSc1d1	komerční
Galerie	galerie	k1gFnSc1	galerie
Ludvíka	Ludvík	k1gMnSc2	Ludvík
Kuby	Kuba	k1gMnSc2	Kuba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Poděbradech	Poděbrady	k1gInPc6	Poděbrady
se	se	k3xPyFc4	se
pravidelně	pravidelně	k6eAd1	pravidelně
konají	konat	k5eAaImIp3nP	konat
mnohé	mnohý	k2eAgInPc4d1	mnohý
festivaly	festival	k1gInPc4	festival
a	a	k8xC	a
soutěžní	soutěžní	k2eAgFnPc4d1	soutěžní
přehlídky	přehlídka	k1gFnPc4	přehlídka
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgFnPc4d3	nejvýznamnější
patří	patřit	k5eAaImIp3nP	patřit
každoroční	každoroční	k2eAgFnPc1d1	každoroční
Historické	historický	k2eAgFnPc1d1	historická
slavnosti	slavnost	k1gFnPc1	slavnost
krále	král	k1gMnSc2	král
Jiřího	Jiří	k1gMnSc2	Jiří
<g/>
,	,	kIx,	,
Poděbradské	poděbradský	k2eAgInPc1d1	poděbradský
dny	den	k1gInPc1	den
poezie	poezie	k1gFnSc2	poezie
<g/>
,	,	kIx,	,
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
soutěžní	soutěžní	k2eAgFnSc1d1	soutěžní
přehlídka	přehlídka	k1gFnSc1	přehlídka
dechové	dechový	k2eAgFnSc2d1	dechová
hudby	hudba	k1gFnSc2	hudba
Evropa	Evropa	k1gFnSc1	Evropa
hraje	hrát	k5eAaImIp3nS	hrát
Kmocha	Kmocha	k?	Kmocha
<g/>
,	,	kIx,	,
divadelní	divadelní	k2eAgMnSc1d1	divadelní
festival	festival	k1gInSc4	festival
mladého	mladý	k2eAgNnSc2d1	mladé
amatérského	amatérský	k2eAgNnSc2d1	amatérské
divadla	divadlo	k1gNnSc2	divadlo
FEMAD	FEMAD	kA	FEMAD
<g/>
,	,	kIx,	,
každoroční	každoroční	k2eAgNnSc1d1	každoroční
finále	finále	k1gNnSc1	finále
Národního	národní	k2eAgInSc2d1	národní
šampionátu	šampionát	k1gInSc2	šampionát
mažoretek	mažoretka	k1gFnPc2	mažoretka
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Jezeře	jezero	k1gNnSc6	jezero
se	se	k3xPyFc4	se
pravidelně	pravidelně	k6eAd1	pravidelně
koná	konat	k5eAaImIp3nS	konat
hudební	hudební	k2eAgInSc1d1	hudební
festival	festival	k1gInSc1	festival
Barvy	barva	k1gFnSc2	barva
léta	léto	k1gNnSc2	léto
<g/>
,	,	kIx,	,
říjen	říjen	k1gInSc1	říjen
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
znamení	znamení	k1gNnSc6	znamení
festivalu	festival	k1gInSc2	festival
rozhlasové	rozhlasový	k2eAgFnSc2d1	rozhlasová
tvorby	tvorba	k1gFnSc2	tvorba
Prix	Prix	k1gInSc1	Prix
Bohemia	bohemia	k1gFnSc1	bohemia
Radio	radio	k1gNnSc4	radio
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
se	se	k3xPyFc4	se
ve	v	k7c6	v
městě	město	k1gNnSc6	město
pořádná	pořádný	k2eAgFnSc1d1	pořádná
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
festival	festival	k1gInSc1	festival
filmové	filmový	k2eAgFnSc2d1	filmová
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
multimédií	multimédium	k1gNnPc2	multimédium
Soundtrack	soundtrack	k1gInSc4	soundtrack
Poděbrady	Poděbrady	k1gInPc1	Poděbrady
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
sčítaní	sčítaný	k2eAgMnPc1d1	sčítaný
lidu	lid	k1gInSc2	lid
<g/>
,	,	kIx,	,
domů	dům	k1gInPc2	dům
a	a	k8xC	a
bytů	byt	k1gInPc2	byt
2001	[number]	k4	2001
uvedlo	uvést	k5eAaPmAgNnS	uvést
3841	[number]	k4	3841
lidí	člověk	k1gMnPc2	člověk
(	(	kIx(	(
<g/>
28,78	[number]	k4	28,78
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
věřící	věřící	k2eAgInSc4d1	věřící
a	a	k8xC	a
8553	[number]	k4	8553
(	(	kIx(	(
<g/>
64	[number]	k4	64
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
bez	bez	k7c2	bez
vyznání	vyznání	k1gNnSc2	vyznání
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
2586	[number]	k4	2586
(	(	kIx(	(
<g/>
19,35	[number]	k4	19,35
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
přihlásilo	přihlásit	k5eAaPmAgNnS	přihlásit
k	k	k7c3	k
církvi	církev	k1gFnSc3	církev
římskokatolické	římskokatolický	k2eAgFnSc3d1	Římskokatolická
<g/>
,	,	kIx,	,
634	[number]	k4	634
(	(	kIx(	(
<g/>
4,74	[number]	k4	4,74
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
k	k	k7c3	k
Českobratrské	českobratrský	k2eAgFnSc3d1	Českobratrská
církvi	církev	k1gFnSc3	církev
evangelické	evangelický	k2eAgFnSc2d1	evangelická
<g/>
,	,	kIx,	,
257	[number]	k4	257
(	(	kIx(	(
<g/>
1,92	[number]	k4	1,92
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
k	k	k7c3	k
Církvi	církev	k1gFnSc3	církev
československé	československý	k2eAgFnSc2d1	Československá
husitské	husitský	k2eAgFnSc2d1	husitská
<g/>
,	,	kIx,	,
55	[number]	k4	55
(	(	kIx(	(
<g/>
0,41	[number]	k4	0,41
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
k	k	k7c3	k
církvi	církev	k1gFnSc3	církev
pravoslavné	pravoslavný	k2eAgFnSc3d1	pravoslavná
a	a	k8xC	a
18	[number]	k4	18
(	(	kIx(	(
<g/>
0,13	[number]	k4	0,13
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
uvedlo	uvést	k5eAaPmAgNnS	uvést
svou	svůj	k3xOyFgFnSc4	svůj
příslušnost	příslušnost	k1gFnSc4	příslušnost
k	k	k7c3	k
náboženské	náboženský	k2eAgFnSc3d1	náboženská
společnosti	společnost	k1gFnSc3	společnost
Svědkové	svědek	k1gMnPc1	svědek
Jehovovi	Jehovův	k2eAgMnPc1d1	Jehovův
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
mělo	mít	k5eAaImAgNnS	mít
město	město	k1gNnSc1	město
i	i	k8xC	i
svou	svůj	k3xOyFgFnSc4	svůj
malou	malý	k2eAgFnSc4d1	malá
židovskou	židovský	k2eAgFnSc4d1	židovská
komunitu	komunita	k1gFnSc4	komunita
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
však	však	k9	však
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
během	během	k7c2	během
holocaustu	holocaust	k1gInSc2	holocaust
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
farnost	farnost	k1gFnSc1	farnost
Poděbrady	Poděbrady	k1gInPc4	Poděbrady
<g/>
.	.	kIx.	.
</s>
<s>
Poděbradské	poděbradský	k2eAgNnSc1d1	Poděbradské
děkanství	děkanství	k1gNnSc1	děkanství
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1886	[number]	k4	1886
povýšeno	povýšit	k5eAaPmNgNnS	povýšit
na	na	k7c4	na
proboštství	proboštství	k1gNnSc4	proboštství
(	(	kIx(	(
<g/>
nad	nad	k7c4	nad
portály	portál	k1gInPc4	portál
vstupních	vstupní	k2eAgFnPc2d1	vstupní
dveří	dveře	k1gFnPc2	dveře
kostela	kostel	k1gInSc2	kostel
Povýšení	povýšení	k1gNnSc2	povýšení
sv.	sv.	kA	sv.
Kříže	kříž	k1gInSc2	kříž
a	a	k8xC	a
fary	fara	k1gFnSc2	fara
jsou	být	k5eAaImIp3nP	být
kamenné	kamenný	k2eAgInPc1d1	kamenný
reliéfy	reliéf	k1gInPc1	reliéf
se	s	k7c7	s
znakem	znak	k1gInSc7	znak
proboštství	proboštství	k1gNnSc1	proboštství
<g/>
,	,	kIx,	,
zhotovené	zhotovený	k2eAgNnSc1d1	zhotovené
sochařem	sochař	k1gMnSc7	sochař
Bohuslavem	Bohuslav	k1gMnSc7	Bohuslav
Schnirchem	Schnirch	k1gMnSc7	Schnirch
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Církev	církev	k1gFnSc1	církev
římskokatolickou	římskokatolický	k2eAgFnSc7d1	Římskokatolická
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
původně	původně	k6eAd1	původně
gotický	gotický	k2eAgInSc1d1	gotický
kostel	kostel	k1gInSc1	kostel
Povýšení	povýšení	k1gNnSc2	povýšení
sv.	sv.	kA	sv.
Kříže	kříž	k1gInPc1	kříž
ze	z	k7c2	z
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
renesančně	renesančně	k6eAd1	renesančně
přestavěný	přestavěný	k2eAgMnSc1d1	přestavěný
a	a	k8xC	a
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
opět	opět	k6eAd1	opět
regotizovaný	regotizovaný	k2eAgMnSc1d1	regotizovaný
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
doplňuje	doplňovat	k5eAaImIp3nS	doplňovat
raně	raně	k6eAd1	raně
barokní	barokní	k2eAgInSc1d1	barokní
kostel	kostel	k1gInSc1	kostel
Nanebevzetí	nanebevzetí	k1gNnSc2	nanebevzetí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průčelí	průčelí	k1gNnSc6	průčelí
kostelíčka	kostelíček	k1gInSc2	kostelíček
jsou	být	k5eAaImIp3nP	být
plastiky	plastika	k1gFnPc1	plastika
od	od	k7c2	od
Bohuslava	Bohuslav	k1gMnSc2	Bohuslav
Schnircha	Schnirch	k1gMnSc2	Schnirch
<g/>
.	.	kIx.	.
</s>
<s>
Třetím	třetí	k4xOgInSc7	třetí
kostelem	kostel	k1gInSc7	kostel
je	být	k5eAaImIp3nS	být
funkcionalistický	funkcionalistický	k2eAgInSc1d1	funkcionalistický
kostel	kostel	k1gInSc1	kostel
Husova	Husův	k2eAgInSc2d1	Husův
sboru	sbor	k1gInSc2	sbor
Církve	církev	k1gFnSc2	církev
československé	československý	k2eAgFnSc2d1	Československá
husitské	husitský	k2eAgFnSc2d1	husitská
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1937	[number]	k4	1937
<g/>
.	.	kIx.	.
</s>
<s>
Českobratrská	českobratrský	k2eAgFnSc1d1	Českobratrská
církev	církev	k1gFnSc1	církev
evangelická	evangelický	k2eAgFnSc1d1	evangelická
má	mít	k5eAaImIp3nS	mít
kostel	kostel	k1gInSc4	kostel
v	v	k7c6	v
Husově	Husův	k2eAgFnSc6d1	Husova
ulici	ulice	k1gFnSc6	ulice
č.	č.	k?	č.
16	[number]	k4	16
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
namísto	namísto	k7c2	namísto
staršího	starý	k2eAgInSc2d2	starší
kostela	kostel	k1gInSc2	kostel
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
v	v	k7c6	v
Husově	Husův	k2eAgFnSc6d1	Husova
ulici	ulice	k1gFnSc6	ulice
stál	stát	k5eAaImAgMnS	stát
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1878	[number]	k4	1878
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
zbourán	zbourat	k5eAaPmNgInS	zbourat
kvůli	kvůli	k7c3	kvůli
stavbě	stavba	k1gFnSc3	stavba
silnice	silnice	k1gFnSc2	silnice
<g/>
.	.	kIx.	.
</s>
<s>
Církev	církev	k1gFnSc1	církev
bratrská	bratrský	k2eAgFnSc1d1	bratrská
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
dokončila	dokončit	k5eAaPmAgFnS	dokončit
svou	svůj	k3xOyFgFnSc4	svůj
modlitebnu	modlitebna	k1gFnSc4	modlitebna
Bétel	Bétela	k1gFnPc2	Bétela
<g/>
.	.	kIx.	.
</s>
<s>
Synagoga	synagoga	k1gFnSc1	synagoga
v	v	k7c6	v
Husově	Husův	k2eAgFnSc6d1	Husova
ulici	ulice	k1gFnSc6	ulice
<g/>
,	,	kIx,	,
postavená	postavený	k2eAgFnSc1d1	postavená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1852	[number]	k4	1852
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
zbourána	zbourat	k5eAaPmNgFnS	zbourat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
dva	dva	k4xCgInPc4	dva
hřbitovy	hřbitov	k1gInPc4	hřbitov
<g/>
.	.	kIx.	.
</s>
<s>
Hřbitov	hřbitov	k1gInSc1	hřbitov
na	na	k7c6	na
předměstí	předměstí	k1gNnSc6	předměstí
Kluk	kluk	k1gMnSc1	kluk
dnes	dnes	k6eAd1	dnes
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
i	i	k9	i
židovský	židovský	k2eAgInSc4d1	židovský
hřbitov	hřbitov	k1gInSc4	hřbitov
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc4	jenž
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1898	[number]	k4	1898
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
protektorátu	protektorát	k1gInSc2	protektorát
byly	být	k5eAaImAgInP	být
zdejší	zdejší	k2eAgInPc1d1	zdejší
náhrobky	náhrobek	k1gInPc1	náhrobek
odvezeny	odvézt	k5eAaPmNgInP	odvézt
<g/>
,	,	kIx,	,
obřadní	obřadní	k2eAgFnSc1d1	obřadní
síň	síň	k1gFnSc1	síň
postavená	postavený	k2eAgFnSc1d1	postavená
pro	pro	k7c4	pro
židovský	židovský	k2eAgInSc4d1	židovský
hřbitov	hřbitov	k1gInSc4	hřbitov
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1900	[number]	k4	1900
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Velkém	velký	k2eAgNnSc6d1	velké
Zboží	zboží	k1gNnSc6	zboží
je	být	k5eAaImIp3nS	být
evangelický	evangelický	k2eAgInSc1d1	evangelický
hřbitov	hřbitov	k1gInSc1	hřbitov
<g/>
.	.	kIx.	.
</s>
<s>
Poděbrady	Poděbrady	k1gInPc1	Poděbrady
jsou	být	k5eAaImIp3nP	být
obcí	obec	k1gFnSc7	obec
s	s	k7c7	s
rozšířenou	rozšířený	k2eAgFnSc7d1	rozšířená
působností	působnost	k1gFnSc7	působnost
<g/>
.	.	kIx.	.
</s>
<s>
Správní	správní	k2eAgInSc1d1	správní
obvod	obvod	k1gInSc1	obvod
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
35	[number]	k4	35
obcí	obec	k1gFnPc2	obec
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
spravuje	spravovat	k5eAaImIp3nS	spravovat
společně	společně	k6eAd1	společně
s	s	k7c7	s
Městcem	Městec	k1gInSc7	Městec
Králové	Králová	k1gFnSc2	Králová
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c4	pod
Poděbrady	Poděbrady	k1gInPc4	Poděbrady
spadá	spadat	k5eAaPmIp3nS	spadat
18	[number]	k4	18
obcí	obec	k1gFnPc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
<g/>
:	:	kIx,	:
Choťánky	Choťánek	k1gMnPc7	Choťánek
<g/>
,	,	kIx,	,
Kolaje	Kolaje	k?	Kolaje
<g/>
,	,	kIx,	,
Kouty	kout	k1gInPc1	kout
<g/>
,	,	kIx,	,
Křečkov	Křečkov	k1gInSc1	Křečkov
<g/>
,	,	kIx,	,
Libice	Libice	k1gFnPc1	Libice
nad	nad	k7c7	nad
Cidlinou	Cidlina	k1gFnSc7	Cidlina
<g/>
,	,	kIx,	,
Odřepsy	Odřeps	k1gInPc7	Odřeps
<g/>
,	,	kIx,	,
Okřínek	okřínek	k1gInSc1	okřínek
<g/>
,	,	kIx,	,
Opolany	Opolan	k1gInPc1	Opolan
<g/>
,	,	kIx,	,
Oseček	Oseček	k1gInSc1	Oseček
<g/>
,	,	kIx,	,
Pátek	pátek	k1gInSc1	pátek
<g/>
,	,	kIx,	,
Písková	pískový	k2eAgFnSc1d1	písková
Lhota	Lhota	k1gFnSc1	Lhota
<g/>
,	,	kIx,	,
Poděbrady	Poděbrady	k1gInPc1	Poděbrady
<g/>
,	,	kIx,	,
Sány	sán	k2eAgInPc1d1	sán
<g/>
,	,	kIx,	,
Senice	Senice	k1gFnPc1	Senice
<g/>
,	,	kIx,	,
Sokoleč	Sokoleč	k1gInSc1	Sokoleč
<g/>
,	,	kIx,	,
Úmyslovice	Úmyslovice	k1gFnSc1	Úmyslovice
<g/>
,	,	kIx,	,
Vlkov	Vlkov	k1gInSc1	Vlkov
pod	pod	k7c7	pod
Oškobrhem	Oškobrh	k1gInSc7	Oškobrh
<g/>
,	,	kIx,	,
Vrbová	vrbový	k2eAgFnSc1d1	vrbová
Lhota	Lhota	k1gFnSc1	Lhota
<g/>
.	.	kIx.	.
</s>
<s>
Vedení	vedení	k1gNnSc1	vedení
města	město	k1gNnSc2	město
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
tvoří	tvořit	k5eAaImIp3nS	tvořit
starosta	starosta	k1gMnSc1	starosta
a	a	k8xC	a
místostarosta	místostarosta	k1gMnSc1	místostarosta
<g/>
.	.	kIx.	.
</s>
<s>
Zastupitelstvo	zastupitelstvo	k1gNnSc1	zastupitelstvo
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
21	[number]	k4	21
členů	člen	k1gMnPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
Radu	rada	k1gFnSc4	rada
města	město	k1gNnSc2	město
tvoří	tvořit	k5eAaImIp3nS	tvořit
7	[number]	k4	7
zastupitelů	zastupitel	k1gMnPc2	zastupitel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgFnPc6d1	poslední
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
zastupitelstev	zastupitelstvo	k1gNnPc2	zastupitelstvo
obcí	obec	k1gFnPc2	obec
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2014	[number]	k4	2014
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
Volba	volba	k1gFnSc1	volba
pro	pro	k7c4	pro
Poděbrady	Poděbrady	k1gInPc4	Poděbrady
s	s	k7c7	s
9	[number]	k4	9
mandáty	mandát	k1gInPc7	mandát
<g/>
,	,	kIx,	,
druhé	druhý	k4xOgFnSc6	druhý
bylo	být	k5eAaImAgNnS	být
uskupení	uskupení	k1gNnSc6	uskupení
Naše	náš	k3xOp1gInPc4	náš
Poděbrady	Poděbrady	k1gInPc4	Poděbrady
se	s	k7c7	s
4	[number]	k4	4
mandáty	mandát	k1gInPc4	mandát
a	a	k8xC	a
třetí	třetí	k4xOgFnPc4	třetí
ČSSD	ČSSD	kA	ČSSD
se	s	k7c7	s
2	[number]	k4	2
mandáty	mandát	k1gInPc7	mandát
<g/>
.	.	kIx.	.
</s>
<s>
Následovaly	následovat	k5eAaImAgFnP	následovat
ODS	ODS	kA	ODS
a	a	k8xC	a
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
(	(	kIx(	(
<g/>
po	po	k7c6	po
2	[number]	k4	2
mandátech	mandát	k1gInPc6	mandát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Volba	volba	k1gFnSc1	volba
pro	pro	k7c4	pro
město	město	k1gNnSc4	město
Poděbrady	Poděbrady	k1gInPc4	Poděbrady
a	a	k8xC	a
ANO	ano	k9	ano
2011	[number]	k4	2011
(	(	kIx(	(
<g/>
po	po	k7c4	po
1	[number]	k4	1
mandátu	mandát	k1gInSc2	mandát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Starostou	Starosta	k1gMnSc7	Starosta
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
PhDr.	PhDr.	kA	PhDr.
Ladislav	Ladislav	k1gMnSc1	Ladislav
Langr	Langr	k1gMnSc1	Langr
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
za	za	k7c4	za
Volbu	volba	k1gFnSc4	volba
pro	pro	k7c4	pro
Poděbrady	Poděbrady	k1gInPc4	Poděbrady
<g/>
,	,	kIx,	,
místostarostou	místostarosta	k1gMnSc7	místostarosta
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Mgr.	Mgr.	kA	Mgr.
Ivan	Ivan	k1gMnSc1	Ivan
Uhlíř	Uhlíř	k1gMnSc1	Uhlíř
z	z	k7c2	z
TOP	topit	k5eAaImRp2nS	topit
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
znak	znak	k1gInSc4	znak
město	město	k1gNnSc1	město
získalo	získat	k5eAaPmAgNnS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1472	[number]	k4	1472
od	od	k7c2	od
synů	syn	k1gMnPc2	syn
krále	král	k1gMnSc2	král
Jiřího	Jiří	k1gMnSc2	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
popis	popis	k1gInSc1	popis
zní	znět	k5eAaImIp3nS	znět
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
Ve	v	k7c6	v
zlatém	zlatý	k2eAgInSc6d1	zlatý
štítě	štít	k1gInSc6	štít
stříbrná	stříbrnat	k5eAaImIp3nS	stříbrnat
kvádrovaná	kvádrovaný	k2eAgFnSc1d1	kvádrovaná
hradební	hradební	k2eAgFnSc1d1	hradební
zeď	zeď	k1gFnSc1	zeď
<g/>
,	,	kIx,	,
vrcholící	vrcholící	k2eAgFnPc1d1	vrcholící
cimbuřím	cimbuří	k1gNnSc7	cimbuří
o	o	k7c6	o
devíti	devět	k4xCc6	devět
červeně	červeně	k6eAd1	červeně
krytých	krytý	k2eAgFnPc6d1	krytá
stínkách	stínka	k1gFnPc6	stínka
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k7c2	uprostřed
zdi	zeď	k1gFnSc2	zeď
otevřená	otevřený	k2eAgFnSc1d1	otevřená
brána	brána	k1gFnSc1	brána
s	s	k7c7	s
černým	černý	k2eAgInSc7d1	černý
otvorem	otvor	k1gInSc7	otvor
<g/>
,	,	kIx,	,
zlatými	zlatý	k2eAgInPc7d1	zlatý
vraty	vrat	k1gInPc7	vrat
a	a	k8xC	a
vytaženou	vytažený	k2eAgFnSc7d1	vytažená
zlatou	zlatý	k2eAgFnSc7d1	zlatá
mříží	mříž	k1gFnSc7	mříž
<g/>
,	,	kIx,	,
zakončenou	zakončený	k2eAgFnSc7d1	zakončená
stříbrnými	stříbrný	k2eAgInPc7d1	stříbrný
hroty	hrot	k1gInPc7	hrot
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
zdí	zeď	k1gFnSc7	zeď
uprostřed	uprostřed	k6eAd1	uprostřed
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
stříbrná	stříbrný	k2eAgFnSc1d1	stříbrná
okrouhlá	okrouhlý	k2eAgFnSc1d1	okrouhlá
věž	věž	k1gFnSc1	věž
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
podlouhlými	podlouhlý	k2eAgFnPc7d1	podlouhlá
okny	okno	k1gNnPc7	okno
nad	nad	k7c7	nad
sebou	se	k3xPyFc7	se
<g/>
,	,	kIx,	,
římsou	římsa	k1gFnSc7	římsa
uprostřed	uprostřed	k6eAd1	uprostřed
<g/>
,	,	kIx,	,
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
pak	pak	k6eAd1	pak
s	s	k7c7	s
cimbuřím	cimbuří	k1gNnSc7	cimbuří
o	o	k7c6	o
čtyřech	čtyři	k4xCgFnPc6	čtyři
stínkách	stínka	k1gFnPc6	stínka
a	a	k8xC	a
špičatou	špičatý	k2eAgFnSc7d1	špičatá
červenou	červený	k2eAgFnSc7d1	červená
střechou	střecha	k1gFnSc7	střecha
s	s	k7c7	s
makovicí	makovice	k1gFnSc7	makovice
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
věží	věž	k1gFnPc2	věž
<g/>
,	,	kIx,	,
jakoby	jakoby	k8xS	jakoby
na	na	k7c6	na
hradbě	hradba	k1gFnSc6	hradba
stojící	stojící	k2eAgFnSc6d1	stojící
<g/>
,	,	kIx,	,
černo-červeně	černo-červeně	k6eAd1	černo-červeně
polcená	polcený	k2eAgFnSc1d1	polcená
orlice	orlice	k1gFnSc1	orlice
se	s	k7c7	s
zlatou	zlatý	k2eAgFnSc7d1	zlatá
zbrojí	zbroj	k1gFnSc7	zbroj
<g/>
,	,	kIx,	,
červeným	červený	k2eAgInSc7d1	červený
jazykem	jazyk	k1gInSc7	jazyk
a	a	k8xC	a
stříbrným	stříbrný	k2eAgInSc7d1	stříbrný
půlměsícem	půlměsíc	k1gInSc7	půlměsíc
přes	přes	k7c4	přes
prsa	prsa	k1gNnPc4	prsa
a	a	k8xC	a
křídla	křídlo	k1gNnPc4	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zdi	zeď	k1gFnSc6	zeď
nad	nad	k7c7	nad
branou	braný	k2eAgFnSc7d1	braná
visí	vise	k1gFnSc7	vise
stříbrný	stříbrný	k2eAgInSc1d1	stříbrný
štítek	štítek	k1gInSc1	štítek
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
černými	černý	k2eAgInPc7d1	černý
pruhy	pruh	k1gInPc7	pruh
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
polovici	polovice	k1gFnSc6	polovice
<g/>
.	.	kIx.	.
</s>
<s>
Štítek	štítek	k1gInSc1	štítek
je	být	k5eAaImIp3nS	být
znakem	znak	k1gInSc7	znak
pánů	pan	k1gMnPc2	pan
z	z	k7c2	z
Kunštátu	Kunštát	k1gInSc2	Kunštát
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
Jiřího	Jiří	k1gMnSc4	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
<g/>
,	,	kIx,	,
orlice	orlice	k1gFnSc1	orlice
pak	pak	k6eAd1	pak
jeho	jeho	k3xOp3gMnPc2	jeho
potomků	potomek	k1gMnPc2	potomek
<g/>
,	,	kIx,	,
knížat	kníže	k1gMnPc2wR	kníže
z	z	k7c2	z
Münsterberka	Münsterberka	k1gFnSc1	Münsterberka
<g/>
.	.	kIx.	.
</s>
<s>
Městská	městský	k2eAgFnSc1d1	městská
vlajka	vlajka	k1gFnSc1	vlajka
byla	být	k5eAaImAgFnS	být
schválena	schválit	k5eAaPmNgFnS	schválit
výnosem	výnos	k1gInSc7	výnos
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
č.	č.	k?	č.
B-	B-	k1gMnSc1	B-
<g/>
8117	[number]	k4	8117
<g/>
-	-	kIx~	-
<g/>
25	[number]	k4	25
<g/>
/	/	kIx~	/
<g/>
11	[number]	k4	11
<g/>
-	-	kIx~	-
<g/>
47	[number]	k4	47
<g/>
-II	-II	k?	-II
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
ze	z	k7c2	z
dne	den	k1gInSc2	den
20	[number]	k4	20
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
Poměr	poměr	k1gInSc1	poměr
stran	strana	k1gFnPc2	strana
je	být	k5eAaImIp3nS	být
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
barevné	barevný	k2eAgInPc4d1	barevný
pruhy	pruh	k1gInPc4	pruh
(	(	kIx(	(
<g/>
bílý	bílý	k1gMnSc1	bílý
<g/>
,	,	kIx,	,
červený	červený	k2eAgMnSc1d1	červený
a	a	k8xC	a
žlutý	žlutý	k2eAgMnSc1d1	žlutý
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
stejně	stejně	k6eAd1	stejně
široké	široký	k2eAgFnPc1d1	široká
<g/>
.	.	kIx.	.
</s>
<s>
Univerzita	univerzita	k1gFnSc1	univerzita
Karlova	Karlův	k2eAgFnSc1d1	Karlova
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
zde	zde	k6eAd1	zde
provozuje	provozovat	k5eAaImIp3nS	provozovat
Středisko	středisko	k1gNnSc1	středisko
jazykové	jazykový	k2eAgFnSc2d1	jazyková
a	a	k8xC	a
odborné	odborný	k2eAgFnSc2d1	odborná
přípravy	příprava	k1gFnSc2	příprava
UK	UK	kA	UK
ÚJOP	ÚJOP	kA	ÚJOP
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
šest	šest	k4xCc1	šest
středních	střední	k2eAgFnPc2d1	střední
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
dvě	dva	k4xCgNnPc1	dva
gymnázia	gymnázium	k1gNnPc1	gymnázium
(	(	kIx(	(
<g/>
Gymnázium	gymnázium	k1gNnSc4	gymnázium
Jiřího	Jiří	k1gMnSc2	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
a	a	k8xC	a
Ekogymnázium	Ekogymnázium	k1gNnSc4	Ekogymnázium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
hotelová	hotelový	k2eAgFnSc1d1	hotelová
škola	škola	k1gFnSc1	škola
(	(	kIx(	(
<g/>
Hotelová	hotelový	k2eAgFnSc1d1	hotelová
škola	škola	k1gFnSc1	škola
a	a	k8xC	a
Vyšší	vysoký	k2eAgFnSc1d2	vyšší
odborná	odborný	k2eAgFnSc1d1	odborná
škola	škola	k1gFnSc1	škola
hotelnictví	hotelnictví	k1gNnSc2	hotelnictví
a	a	k8xC	a
turismu	turismus	k1gInSc2	turismus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
škola	škola	k1gFnSc1	škola
(	(	kIx(	(
<g/>
SZeŠ	SZeŠ	k1gFnSc1	SZeŠ
a	a	k8xC	a
SOŠ	SOŠ	kA	SOŠ
Poděbrady	Poděbrady	k1gInPc1	Poděbrady
<g/>
)	)	kIx)	)
a	a	k8xC	a
dvě	dva	k4xCgNnPc4	dva
střední	střední	k2eAgNnPc4d1	střední
odborná	odborný	k2eAgNnPc4d1	odborné
učiliště	učiliště	k1gNnPc4	učiliště
(	(	kIx(	(
<g/>
Střední	střední	k2eAgNnSc4d1	střední
odborné	odborný	k2eAgNnSc4d1	odborné
učiliště	učiliště	k1gNnSc4	učiliště
společného	společný	k2eAgNnSc2d1	společné
stravování	stravování	k1gNnSc2	stravování
a	a	k8xC	a
Střední	střední	k2eAgNnSc1d1	střední
odborné	odborný	k2eAgNnSc1d1	odborné
učiliště	učiliště	k1gNnSc1	učiliště
Skláren	sklárna	k1gFnPc2	sklárna
Bohemia	bohemia	k1gFnSc1	bohemia
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnPc1d1	základní
školy	škola	k1gFnPc1	škola
jsou	být	k5eAaImIp3nP	být
dvě	dva	k4xCgFnPc1	dva
(	(	kIx(	(
<g/>
ZŠ	ZŠ	kA	ZŠ
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
s	s	k7c7	s
kapacitou	kapacita	k1gFnSc7	kapacita
930	[number]	k4	930
žáků	žák	k1gMnPc2	žák
a	a	k8xC	a
ZŠ	ZŠ	kA	ZŠ
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
s	s	k7c7	s
kapacitou	kapacita	k1gFnSc7	kapacita
1000	[number]	k4	1000
žáků	žák	k1gMnPc2	žák
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
speciální	speciální	k2eAgFnSc1d1	speciální
škola	škola	k1gFnSc1	škola
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
a	a	k8xC	a
1	[number]	k4	1
mateřská	mateřský	k2eAgFnSc1d1	mateřská
škola	škola	k1gFnSc1	škola
s	s	k7c7	s
celkem	celek	k1gInSc7	celek
8	[number]	k4	8
odloučenými	odloučený	k2eAgNnPc7d1	odloučené
pracovišti	pracoviště	k1gNnPc7	pracoviště
<g/>
.	.	kIx.	.
</s>
<s>
Vzdělanost	vzdělanost	k1gFnSc1	vzdělanost
obyvatel	obyvatel	k1gMnPc2	obyvatel
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
nadprůměrná	nadprůměrný	k2eAgFnSc1d1	nadprůměrná
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
sčítání	sčítání	k1gNnSc6	sčítání
lidu	lid	k1gInSc2	lid
a	a	k8xC	a
domů	dům	k1gInPc2	dům
z	z	k7c2	z
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2001	[number]	k4	2001
udalo	udat	k5eAaPmAgNnS	udat
34,6	[number]	k4	34,6
<g/>
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
vyučení	vyučení	k1gNnSc4	vyučení
a	a	k8xC	a
střední	střední	k2eAgNnSc4d1	střední
odborné	odborný	k2eAgNnSc4d1	odborné
vzdělání	vzdělání	k1gNnSc4	vzdělání
bez	bez	k7c2	bez
maturity	maturita	k1gFnSc2	maturita
<g/>
,	,	kIx,	,
29,4	[number]	k4	29,4
<g/>
%	%	kIx~	%
úplné	úplný	k2eAgNnSc1d1	úplné
střední	střední	k2eAgNnSc1d1	střední
vzdělání	vzdělání	k1gNnSc1	vzdělání
s	s	k7c7	s
maturitou	maturita	k1gFnSc7	maturita
<g/>
,	,	kIx,	,
5,6	[number]	k4	5,6
<g/>
%	%	kIx~	%
vyšší	vysoký	k2eAgNnSc4d2	vyšší
odborné	odborný	k2eAgNnSc4d1	odborné
a	a	k8xC	a
nástavbové	nástavbový	k2eAgNnSc4d1	nástavbové
vzdělání	vzdělání	k1gNnSc4	vzdělání
a	a	k8xC	a
12,2	[number]	k4	12,2
<g/>
%	%	kIx~	%
vysokoškolské	vysokoškolský	k2eAgNnSc1d1	vysokoškolské
vzdělání	vzdělání	k1gNnSc1	vzdělání
(	(	kIx(	(
<g/>
celorepublikový	celorepublikový	k2eAgInSc1d1	celorepublikový
průměr	průměr	k1gInSc1	průměr
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
kategoriích	kategorie	k1gFnPc6	kategorie
je	být	k5eAaImIp3nS	být
38	[number]	k4	38
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
24,9	[number]	k4	24,9
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
3,4	[number]	k4	3,4
<g/>
%	%	kIx~	%
a	a	k8xC	a
8,9	[number]	k4	8,9
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dopravní	dopravní	k2eAgFnSc1d1	dopravní
síť	síť	k1gFnSc1	síť
Pozemní	pozemní	k2eAgFnSc2d1	pozemní
komunikace	komunikace	k1gFnSc2	komunikace
Už	už	k9	už
první	první	k4xOgNnSc1	první
osídlení	osídlení	k1gNnSc1	osídlení
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
bylo	být	k5eAaImAgNnS	být
ovlivněno	ovlivněn	k2eAgNnSc1d1	ovlivněno
existencí	existence	k1gFnSc7	existence
strategického	strategický	k2eAgInSc2d1	strategický
přechodu	přechod	k1gInSc2	přechod
před	před	k7c4	před
řeku	řeka	k1gFnSc4	řeka
Labe	Labe	k1gNnSc2	Labe
na	na	k7c6	na
trase	trasa	k1gFnSc6	trasa
Kladské	kladský	k2eAgFnSc2d1	Kladská
cesty	cesta	k1gFnSc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
Kladská	kladský	k2eAgFnSc1d1	Kladská
cesta	cesta	k1gFnSc1	cesta
přeložena	přeložit	k5eAaPmNgFnS	přeložit
přes	přes	k7c4	přes
Nymburk	Nymburk	k1gInSc4	Nymburk
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
stalo	stát	k5eAaPmAgNnS	stát
důležitým	důležitý	k2eAgInSc7d1	důležitý
dopravním	dopravní	k2eAgInSc7d1	dopravní
uzlem	uzel	k1gInSc7	uzel
císařských	císařský	k2eAgFnPc2d1	císařská
cest	cesta	k1gFnPc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
hrázi	hráz	k1gFnSc6	hráz
Předměstského	předměstský	k2eAgInSc2d1	předměstský
rybníka	rybník	k1gInSc2	rybník
byla	být	k5eAaImAgFnS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1817	[number]	k4	1817
zbudována	zbudován	k2eAgFnSc1d1	zbudována
erární	erární	k2eAgFnSc1d1	erární
silnice	silnice	k1gFnSc1	silnice
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
do	do	k7c2	do
Hradce	Hradec	k1gInSc2	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
.	.	kIx.	.
</s>
<s>
Spojení	spojení	k1gNnSc1	spojení
na	na	k7c4	na
Hradec	Hradec	k1gInSc4	Hradec
Králové	Králová	k1gFnSc2	Králová
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
možné	možný	k2eAgNnSc1d1	možné
po	po	k7c6	po
dálnici	dálnice	k1gFnSc6	dálnice
D11	D11	k1gFnSc2	D11
s	s	k7c7	s
exity	exit	k1gInPc7	exit
35	[number]	k4	35
(	(	kIx(	(
<g/>
Poděbrady-západ	Poděbradyápad	k1gInSc1	Poděbrady-západ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
39	[number]	k4	39
(	(	kIx(	(
<g/>
Poděbrady-jih	Poděbradyih	k1gInSc1	Poděbrady-jih
<g/>
)	)	kIx)	)
a	a	k8xC	a
42	[number]	k4	42
(	(	kIx(	(
<g/>
Poděbrady-východ	Poděbradyýchod	k1gInSc1	Poděbrady-východ
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
po	po	k7c6	po
silnici	silnice	k1gFnSc6	silnice
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
611	[number]	k4	611
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Sadská	sadský	k2eAgFnSc1d1	Sadská
–	–	k?	–
Poděbrady	Poděbrady	k1gInPc4	Poděbrady
–	–	k?	–
Chlumec	Chlumec	k1gInSc1	Chlumec
nad	nad	k7c7	nad
Cidlinou	Cidlina	k1gFnSc7	Cidlina
–	–	k?	–
Hradec	Hradec	k1gInSc4	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
západním	západní	k2eAgInSc6d1	západní
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
silnice	silnice	k1gFnSc1	silnice
křížila	křížit	k5eAaImAgFnS	křížit
se	s	k7c7	s
silnicí	silnice	k1gFnSc7	silnice
<g/>
,	,	kIx,	,
vedoucí	vedoucí	k2eAgFnSc7d1	vedoucí
z	z	k7c2	z
Kolína	Kolín	k1gInSc2	Kolín
do	do	k7c2	do
Nymburku	Nymburk	k1gInSc2	Nymburk
<g/>
,	,	kIx,	,
dokončená	dokončený	k2eAgFnSc1d1	dokončená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1836	[number]	k4	1836
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
silnice	silnice	k1gFnPc1	silnice
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
38	[number]	k4	38
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
východ	východ	k1gInSc4	východ
od	od	k7c2	od
města	město	k1gNnSc2	město
vedla	vést	k5eAaImAgFnS	vést
další	další	k2eAgFnSc1d1	další
silnice	silnice	k1gFnSc1	silnice
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Jičín	Jičín	k1gInSc4	Jičín
<g/>
,	,	kIx,	,
Kopidlno	Kopidlno	k1gNnSc4	Kopidlno
a	a	k8xC	a
Trutnov	Trutnov	k1gInSc1	Trutnov
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgFnSc1d1	dnešní
silnice	silnice	k1gFnSc1	silnice
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
32	[number]	k4	32
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Městem	město	k1gNnSc7	město
dále	daleko	k6eAd2	daleko
procházejí	procházet	k5eAaImIp3nP	procházet
silnice	silnice	k1gFnPc1	silnice
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
329	[number]	k4	329
Pečky	pečka	k1gFnSc2	pečka
–	–	k?	–
Poděbrady	Poděbrady	k1gInPc4	Poděbrady
–	–	k?	–
Netřebice	Netřebice	k1gFnSc2	Netřebice
–	–	k?	–
Křinec	Křinec	k1gInSc1	Křinec
a	a	k8xC	a
silnice	silnice	k1gFnSc1	silnice
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
331	[number]	k4	331
Poděbrady	Poděbrady	k1gInPc7	Poděbrady
–	–	k?	–
Nymburk	Nymburk	k1gInSc1	Nymburk
–	–	k?	–
Lysá	Lysá	k1gFnSc1	Lysá
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
–	–	k?	–
Stará	starat	k5eAaImIp3nS	starat
Boleslav	Boleslav	k1gMnSc1	Boleslav
<g/>
.	.	kIx.	.
</s>
<s>
Přímo	přímo	k6eAd1	přímo
ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
železobetonový	železobetonový	k2eAgInSc1d1	železobetonový
silniční	silniční	k2eAgInSc1d1	silniční
most	most	k1gInSc1	most
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
kamenný	kamenný	k2eAgInSc4d1	kamenný
inundační	inundační	k2eAgInSc4d1	inundační
most	most	k1gInSc4	most
postavený	postavený	k2eAgInSc4d1	postavený
v	v	k7c6	v
letech	let	k1gInPc6	let
1828	[number]	k4	1828
<g/>
–	–	k?	–
<g/>
1831	[number]	k4	1831
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
východním	východní	k2eAgInSc6d1	východní
okraji	okraj	k1gInSc6	okraj
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
poblíž	poblíž	k7c2	poblíž
plnírny	plnírna	k1gFnSc2	plnírna
Poděbradky	poděbradka	k1gFnSc2	poděbradka
<g/>
,	,	kIx,	,
stojí	stát	k5eAaImIp3nS	stát
nový	nový	k2eAgInSc4d1	nový
most	most	k1gInSc4	most
přes	přes	k7c4	přes
Labe	Labe	k1gNnSc4	Labe
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
obchvatu	obchvat	k1gInSc2	obchvat
Nymburka	Nymburk	k1gInSc2	Nymburk
<g/>
.	.	kIx.	.
</s>
<s>
Železnice	železnice	k1gFnSc1	železnice
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
poblíž	poblíž	k7c2	poblíž
města	město	k1gNnSc2	město
postavena	postaven	k2eAgFnSc1d1	postavena
významná	významný	k2eAgFnSc1d1	významná
železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
z	z	k7c2	z
Kolína	Kolín	k1gInSc2	Kolín
do	do	k7c2	do
Nymburka	Nymburk	k1gInSc2	Nymburk
<g/>
.	.	kIx.	.
</s>
<s>
Zprovozněna	zprovozněn	k2eAgFnSc1d1	zprovozněna
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1870	[number]	k4	1870
jako	jako	k8xS	jako
součást	součást	k1gFnSc4	součást
Rakouské	rakouský	k2eAgFnSc2d1	rakouská
severozápadní	severozápadní	k2eAgFnPc4d1	severozápadní
dráhy	dráha	k1gFnSc2	dráha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
byla	být	k5eAaImAgFnS	být
trať	trať	k1gFnSc1	trať
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
na	na	k7c4	na
dvoukolejnou	dvoukolejný	k2eAgFnSc4d1	dvoukolejná
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
i	i	k9	i
elektrifikována	elektrifikován	k2eAgFnSc1d1	elektrifikována
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
nese	nést	k5eAaImIp3nS	nést
označení	označení	k1gNnSc1	označení
trať	trať	k1gFnSc1	trať
231	[number]	k4	231
a	a	k8xC	a
vede	vést	k5eAaImIp3nS	vést
na	na	k7c6	na
trase	trasa	k1gFnSc6	trasa
Praha	Praha	k1gFnSc1	Praha
<g/>
–	–	k?	–
<g/>
Lysá	Lysá	k1gFnSc1	Lysá
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
–	–	k?	–
<g/>
Kolín	Kolín	k1gInSc1	Kolín
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
železniční	železniční	k2eAgFnSc2d1	železniční
stanice	stanice	k1gFnSc2	stanice
Poděbrady	Poděbrady	k1gInPc1	Poděbrady
a	a	k8xC	a
místní	místní	k2eAgFnSc1d1	místní
zastávka	zastávka	k1gFnSc1	zastávka
Velké	velká	k1gFnSc2	velká
Zboží	zboží	k1gNnSc1	zboží
<g/>
.	.	kIx.	.
</s>
<s>
Trať	trať	k1gFnSc1	trať
má	mít	k5eAaImIp3nS	mít
velký	velký	k2eAgInSc4d1	velký
význam	význam	k1gInSc4	význam
pro	pro	k7c4	pro
příměstskou	příměstský	k2eAgFnSc4d1	příměstská
dopravu	doprava	k1gFnSc4	doprava
a	a	k8xC	a
současně	současně	k6eAd1	současně
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
součást	součást	k1gFnSc1	součást
sítě	síť	k1gFnSc2	síť
tratí	trať	k1gFnPc2	trať
zařazených	zařazený	k2eAgInPc2d1	zařazený
do	do	k7c2	do
evropského	evropský	k2eAgInSc2d1	evropský
železničního	železniční	k2eAgInSc2d1	železniční
systému	systém	k1gInSc2	systém
hlavním	hlavní	k2eAgMnSc7d1	hlavní
železničním	železniční	k2eAgInSc7d1	železniční
nákladním	nákladní	k2eAgInSc7d1	nákladní
tahem	tah	k1gInSc7	tah
<g/>
.	.	kIx.	.
</s>
<s>
Vodní	vodní	k2eAgFnSc1d1	vodní
doprava	doprava	k1gFnSc1	doprava
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1903	[number]	k4	1903
začala	začít	k5eAaPmAgFnS	začít
realizace	realizace	k1gFnSc1	realizace
regulace	regulace	k1gFnSc2	regulace
Labe	Labe	k1gNnSc2	Labe
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1913	[number]	k4	1913
<g/>
–	–	k?	–
<g/>
1918	[number]	k4	1918
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
regulace	regulace	k1gFnSc2	regulace
postaveno	postaven	k2eAgNnSc1d1	postaveno
zdymadlo	zdymadlo	k1gNnSc1	zdymadlo
a	a	k8xC	a
vodní	vodní	k2eAgFnSc1d1	vodní
elektrárna	elektrárna	k1gFnSc1	elektrárna
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gMnSc7	jejíž
autorem	autor	k1gMnSc7	autor
byl	být	k5eAaImAgMnS	být
poděbradský	poděbradský	k2eAgMnSc1d1	poděbradský
rodák	rodák	k1gMnSc1	rodák
Antonín	Antonín	k1gMnSc1	Antonín
Engel	Engel	k1gMnSc1	Engel
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
sloužila	sloužit	k5eAaImAgFnS	sloužit
řeka	řeka	k1gFnSc1	řeka
k	k	k7c3	k
dopravě	doprava	k1gFnSc3	doprava
uhlí	uhlí	k1gNnSc2	uhlí
pro	pro	k7c4	pro
chvaletickou	chvaletický	k2eAgFnSc4d1	Chvaletická
elektrárnu	elektrárna	k1gFnSc4	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
Vodní	vodní	k2eAgFnSc1d1	vodní
doprava	doprava	k1gFnSc1	doprava
byla	být	k5eAaImAgFnS	být
zastavena	zastavit	k5eAaPmNgFnS	zastavit
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
po	po	k7c6	po
řece	řeka	k1gFnSc6	řeka
plaví	plavit	k5eAaImIp3nS	plavit
především	především	k9	především
výletní	výletní	k2eAgFnSc1d1	výletní
loď	loď	k1gFnSc1	loď
Král	Král	k1gMnSc1	Král
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
,	,	kIx,	,
výletní	výletní	k2eAgMnSc1d1	výletní
<g/>
,	,	kIx,	,
sportovní	sportovní	k2eAgFnPc1d1	sportovní
lodě	loď	k1gFnPc1	loď
a	a	k8xC	a
nově	nově	k6eAd1	nově
postavené	postavený	k2eAgFnPc1d1	postavená
lodě	loď	k1gFnPc1	loď
<g/>
,	,	kIx,	,
plující	plující	k2eAgNnSc1d1	plující
z	z	k7c2	z
loděnice	loděnice	k1gFnSc2	loděnice
ve	v	k7c6	v
Chvaleticích	Chvaletice	k1gFnPc6	Chvaletice
ke	k	k7c3	k
svým	svůj	k3xOyFgMnPc3	svůj
majitelům	majitel	k1gMnPc3	majitel
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jih	jih	k1gInSc4	jih
od	od	k7c2	od
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
využít	využít	k5eAaPmF	využít
pro	pro	k7c4	pro
překonání	překonání	k1gNnSc4	překonání
řeky	řeka	k1gFnSc2	řeka
Přívoz	přívoz	k1gInSc1	přívoz
Oseček	Oseček	k1gInSc1	Oseček
<g/>
.	.	kIx.	.
</s>
<s>
MHD	MHD	kA	MHD
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
není	být	k5eNaImIp3nS	být
městská	městský	k2eAgFnSc1d1	městská
doprava	doprava	k1gFnSc1	doprava
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
ji	on	k3xPp3gFnSc4	on
ale	ale	k9	ale
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
služba	služba	k1gFnSc1	služba
Senior	senior	k1gMnSc1	senior
Taxi	taxe	k1gFnSc4	taxe
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
funguje	fungovat	k5eAaImIp3nS	fungovat
ve	v	k7c4	v
všední	všední	k2eAgInPc4d1	všední
dny	den	k1gInPc4	den
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nabízena	nabízet	k5eAaImNgFnS	nabízet
seniorům	senior	k1gMnPc3	senior
starším	starý	k2eAgFnPc3d2	starší
70	[number]	k4	70
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Veřejná	veřejný	k2eAgFnSc1d1	veřejná
doprava	doprava	k1gFnSc1	doprava
2011	[number]	k4	2011
Autobusová	autobusový	k2eAgFnSc1d1	autobusová
doprava	doprava	k1gFnSc1	doprava
Z	z	k7c2	z
města	město	k1gNnSc2	město
vedly	vést	k5eAaImAgFnP	vést
autobusové	autobusový	k2eAgFnPc1d1	autobusová
spoje	spoj	k1gFnPc1	spoj
např.	např.	kA	např.
do	do	k7c2	do
těchto	tento	k3xDgInPc2	tento
cílů	cíl	k1gInPc2	cíl
<g/>
:	:	kIx,	:
Broumov	Broumov	k1gInSc1	Broumov
<g/>
,	,	kIx,	,
Dobruška	Dobruška	k1gFnSc1	Dobruška
<g/>
,	,	kIx,	,
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
,	,	kIx,	,
Jeseník	Jeseník	k1gInSc1	Jeseník
<g/>
,	,	kIx,	,
Jičín	Jičín	k1gInSc1	Jičín
<g/>
,	,	kIx,	,
Kladsko	Kladsko	k1gNnSc1	Kladsko
<g/>
,	,	kIx,	,
Kolín	Kolín	k1gInSc1	Kolín
<g/>
,	,	kIx,	,
Košice	Košice	k1gInPc1	Košice
<g/>
,	,	kIx,	,
Liberec	Liberec	k1gInSc1	Liberec
<g/>
,	,	kIx,	,
Městec	Městec	k1gInSc1	Městec
Králové	Králová	k1gFnSc2	Králová
<g/>
,	,	kIx,	,
Michalovce	Michalovce	k1gInPc1	Michalovce
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Mladá	mladá	k1gFnSc1	mladá
Boleslav	Boleslav	k1gMnSc1	Boleslav
<g/>
,	,	kIx,	,
Náchod	Náchod	k1gInSc1	Náchod
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
nad	nad	k7c7	nad
Metují	Metuje	k1gFnSc7	Metuje
<g/>
,	,	kIx,	,
Nymburk	Nymburk	k1gInSc1	Nymburk
<g/>
,	,	kIx,	,
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
Pec	Pec	k1gFnSc1	Pec
pod	pod	k7c7	pod
Sněžkou	Sněžka	k1gFnSc7	Sněžka
<g/>
,	,	kIx,	,
Pečky	Pečky	k1gFnPc1	Pečky
<g/>
,	,	kIx,	,
Poprad	Poprad	k1gInSc1	Poprad
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Prešov	Prešov	k1gInSc1	Prešov
<g/>
,	,	kIx,	,
Svitavy	Svitava	k1gFnPc1	Svitava
<g/>
,	,	kIx,	,
Špindlerův	Špindlerův	k2eAgInSc1d1	Špindlerův
Mlýn	mlýn	k1gInSc1	mlýn
<g/>
,	,	kIx,	,
Trutnov	Trutnov	k1gInSc1	Trutnov
<g/>
,	,	kIx,	,
Vrchlabí	Vrchlabí	k1gNnSc1	Vrchlabí
<g/>
,	,	kIx,	,
Zlín	Zlín	k1gInSc1	Zlín
<g/>
,	,	kIx,	,
Žamberk	Žamberk	k1gInSc1	Žamberk
<g/>
,	,	kIx,	,
Žilina	Žilina	k1gFnSc1	Žilina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
období	období	k1gNnSc6	období
jezdila	jezdit	k5eAaImAgFnS	jezdit
v	v	k7c6	v
Poděbradech	Poděbrady	k1gInPc6	Poděbrady
linka	linka	k1gFnSc1	linka
městské	městský	k2eAgFnSc2d1	městská
autobusové	autobusový	k2eAgFnSc2d1	autobusová
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc1d1	poslední
pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
její	její	k3xOp3gNnSc4	její
zavedení	zavedení	k1gNnSc4	zavedení
proběhl	proběhnout	k5eAaPmAgMnS	proběhnout
v	v	k7c6	v
letech	let	k1gInPc6	let
2002	[number]	k4	2002
<g/>
–	–	k?	–
<g/>
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Železniční	železniční	k2eAgFnSc1d1	železniční
doprava	doprava	k1gFnSc1	doprava
Po	po	k7c6	po
trati	trať	k1gFnSc6	trať
231	[number]	k4	231
vedla	vést	k5eAaImAgFnS	vést
linka	linka	k1gFnSc1	linka
S2	S2	k1gFnSc1	S2
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Lysá	Lysá	k1gFnSc1	Lysá
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
–	–	k?	–
Kolín	Kolín	k1gInSc1	Kolín
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
pražského	pražský	k2eAgInSc2d1	pražský
systému	systém	k1gInSc2	systém
Esko	eska	k1gFnSc5	eska
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
železniční	železniční	k2eAgFnSc6d1	železniční
stanici	stanice	k1gFnSc6	stanice
Poděbrady	Poděbrady	k1gInPc4	Poděbrady
zastavovalo	zastavovat	k5eAaImAgNnS	zastavovat
v	v	k7c4	v
pracovní	pracovní	k2eAgInPc4d1	pracovní
dny	den	k1gInPc4	den
27	[number]	k4	27
rychlíků	rychlík	k1gMnPc2	rychlík
a	a	k8xC	a
39	[number]	k4	39
osobních	osobní	k2eAgInPc2d1	osobní
vlaků	vlak	k1gInPc2	vlak
<g/>
,	,	kIx,	,
o	o	k7c6	o
víkendu	víkend	k1gInSc6	víkend
23	[number]	k4	23
rychlíků	rychlík	k1gMnPc2	rychlík
a	a	k8xC	a
20	[number]	k4	20
osobních	osobní	k2eAgInPc2d1	osobní
vlaků	vlak	k1gInPc2	vlak
<g/>
.	.	kIx.	.
</s>
<s>
Rychlíky	rychlík	k1gInPc1	rychlík
zajišťovaly	zajišťovat	k5eAaImAgInP	zajišťovat
spojení	spojení	k1gNnSc4	spojení
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
Hradce	Hradec	k1gInSc2	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
,	,	kIx,	,
Trutnova	Trutnov	k1gInSc2	Trutnov
<g/>
,	,	kIx,	,
Ústí	ústí	k1gNnSc1	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
,	,	kIx,	,
Kolína	Kolín	k1gInSc2	Kolín
a	a	k8xC	a
Rumburka	Rumburk	k1gInSc2	Rumburk
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
město	město	k1gNnSc1	město
prosperovalo	prosperovat	k5eAaImAgNnS	prosperovat
především	především	k9	především
díky	díky	k7c3	díky
obilnářství	obilnářství	k1gNnSc3	obilnářství
a	a	k8xC	a
rybníkářství	rybníkářství	k1gNnSc3	rybníkářství
<g/>
.	.	kIx.	.
</s>
<s>
Poděbradská	poděbradský	k2eAgFnSc1d1	Poděbradská
rybniční	rybniční	k2eAgFnSc1d1	rybniční
soustava	soustava	k1gFnSc1	soustava
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
začala	začít	k5eAaPmAgFnS	začít
vznikat	vznikat	k5eAaImF	vznikat
za	za	k7c2	za
pánů	pan	k1gMnPc2	pan
z	z	k7c2	z
Kunštátu	Kunštát	k1gInSc2	Kunštát
<g/>
,	,	kIx,	,
zahrnovala	zahrnovat	k5eAaImAgFnS	zahrnovat
až	až	k9	až
48	[number]	k4	48
rybníků	rybník	k1gInPc2	rybník
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
rybník	rybník	k1gInSc4	rybník
Blato	Blato	k1gNnSc1	Blato
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
973	[number]	k4	973
ha	ha	kA	ha
byl	být	k5eAaImAgMnS	být
ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
největším	veliký	k2eAgInSc7d3	veliký
rybníkem	rybník	k1gInSc7	rybník
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
<g/>
.	.	kIx.	.
</s>
<s>
Poděbrady	Poděbrady	k1gInPc1	Poděbrady
tehdy	tehdy	k6eAd1	tehdy
patřily	patřit	k5eAaImAgInP	patřit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Třeboňskem	Třeboňsko	k1gNnSc7	Třeboňsko
a	a	k8xC	a
Pardubickem	Pardubicko	k1gNnSc7	Pardubicko
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
největší	veliký	k2eAgFnPc4d3	veliký
rybníkářské	rybníkářský	k2eAgFnPc4d1	rybníkářská
oblasti	oblast	k1gFnPc4	oblast
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
zde	zde	k6eAd1	zde
fungoval	fungovat	k5eAaImAgInS	fungovat
také	také	k9	také
pivovar	pivovar	k1gInSc1	pivovar
<g/>
,	,	kIx,	,
mlýn	mlýn	k1gInSc1	mlýn
<g/>
,	,	kIx,	,
cukrovar	cukrovar	k1gInSc1	cukrovar
či	či	k8xC	či
pila	pila	k1gFnSc1	pila
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
zde	zde	k6eAd1	zde
dominuje	dominovat	k5eAaImIp3nS	dominovat
především	především	k9	především
sklářství	sklářství	k1gNnSc1	sklářství
<g/>
,	,	kIx,	,
lázeňství	lázeňství	k1gNnSc1	lázeňství
a	a	k8xC	a
turismus	turismus	k1gInSc1	turismus
<g/>
.	.	kIx.	.
</s>
<s>
Poblíž	poblíž	k7c2	poblíž
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
plnírna	plnírna	k1gFnSc1	plnírna
minerální	minerální	k2eAgFnSc2d1	minerální
vody	voda	k1gFnSc2	voda
Poděbradka	poděbradka	k1gFnSc1	poděbradka
<g/>
.	.	kIx.	.
</s>
<s>
Zámecký	zámecký	k2eAgInSc1d1	zámecký
vodní	vodní	k2eAgInSc1d1	vodní
mlýn	mlýn	k1gInSc1	mlýn
a	a	k8xC	a
pivovar	pivovar	k1gInSc1	pivovar
byly	být	k5eAaImAgInP	být
postaveny	postavit	k5eAaPmNgInP	postavit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1574	[number]	k4	1574
a	a	k8xC	a
zanikly	zaniknout	k5eAaPmAgFnP	zaniknout
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1863	[number]	k4	1863
přibyl	přibýt	k5eAaPmAgInS	přibýt
parní	parní	k2eAgInSc1d1	parní
mlýn	mlýn	k1gInSc1	mlýn
a	a	k8xC	a
v	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
byl	být	k5eAaImAgInS	být
otevřen	otevřít	k5eAaPmNgInS	otevřít
cukrovar	cukrovar	k1gInSc1	cukrovar
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc4	jenž
byl	být	k5eAaImAgInS	být
zrušen	zrušit	k5eAaPmNgInS	zrušit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
<g/>
.	.	kIx.	.
</s>
<s>
Roubíčkův	Roubíčkův	k2eAgInSc1d1	Roubíčkův
pivovar	pivovar	k1gInSc1	pivovar
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1872	[number]	k4	1872
<g/>
.	.	kIx.	.
</s>
<s>
Kerhartova	Kerhartův	k2eAgFnSc1d1	Kerhartův
pila	pila	k1gFnSc1	pila
a	a	k8xC	a
cementárna	cementárna	k1gFnSc1	cementárna
byla	být	k5eAaImAgFnS	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1873	[number]	k4	1873
<g/>
.	.	kIx.	.
</s>
<s>
Jediným	jediný	k2eAgInSc7d1	jediný
velkým	velký	k2eAgInSc7d1	velký
průmyslovým	průmyslový	k2eAgInSc7d1	průmyslový
podnikem	podnik	k1gInSc7	podnik
ve	v	k7c6	v
městě	město	k1gNnSc6	město
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
Sklárny	sklárna	k1gFnSc2	sklárna
Bohemia	bohemia	k1gFnSc1	bohemia
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
součást	součást	k1gFnSc1	součást
společnosti	společnost	k1gFnSc2	společnost
Crystal	Crystal	k1gFnSc1	Crystal
Bohemia	bohemia	k1gFnSc1	bohemia
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
doplňuje	doplňovat	k5eAaImIp3nS	doplňovat
sklářská	sklářský	k2eAgFnSc1d1	sklářská
firma	firma	k1gFnSc1	firma
J.	J.	kA	J.
Blažek	Blažka	k1gFnPc2	Blažka
Sklo	sklo	k1gNnSc1	sklo
Poděbrady	Poděbrady	k1gInPc7	Poděbrady
<g/>
.	.	kIx.	.
</s>
<s>
Sklárna	sklárna	k1gFnSc1	sklárna
byla	být	k5eAaImAgFnS	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1876	[number]	k4	1876
<g/>
,	,	kIx,	,
po	po	k7c6	po
zhruba	zhruba	k6eAd1	zhruba
10	[number]	k4	10
letech	léto	k1gNnPc6	léto
byla	být	k5eAaImAgFnS	být
uzavřena	uzavřít	k5eAaPmNgFnS	uzavřít
a	a	k8xC	a
provoz	provoz	k1gInSc1	provoz
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
byl	být	k5eAaImAgMnS	být
<g/>
,	,	kIx,	,
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
německých	německý	k2eAgMnPc2d1	německý
sklářů	sklář	k1gMnPc2	sklář
<g/>
,	,	kIx,	,
definitivně	definitivně	k6eAd1	definitivně
obnoven	obnovit	k5eAaPmNgInS	obnovit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1893	[number]	k4	1893
<g/>
.	.	kIx.	.
</s>
<s>
Sklárna	sklárna	k1gFnSc1	sklárna
je	být	k5eAaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
především	především	k9	především
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
produkci	produkce	k1gFnSc3	produkce
olovnatého	olovnatý	k2eAgInSc2d1	olovnatý
křišťálu	křišťál	k1gInSc2	křišťál
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
produkuje	produkovat	k5eAaImIp3nS	produkovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1927	[number]	k4	1927
<g/>
.	.	kIx.	.
</s>
<s>
Výrazně	výrazně	k6eAd1	výrazně
byla	být	k5eAaImAgFnS	být
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
zájmových	zájmový	k2eAgFnPc2d1	zájmová
a	a	k8xC	a
volnočasových	volnočasův	k2eAgFnPc2d1	volnočasův
organizací	organizace	k1gFnPc2	organizace
a	a	k8xC	a
skupin	skupina	k1gFnPc2	skupina
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
poloze	poloha	k1gFnSc3	poloha
v	v	k7c6	v
nížinném	nížinný	k2eAgInSc6d1	nížinný
terénu	terén	k1gInSc6	terén
jsou	být	k5eAaImIp3nP	být
Poděbrady	Poděbrady	k1gInPc1	Poděbrady
vhodné	vhodný	k2eAgInPc1d1	vhodný
pro	pro	k7c4	pro
cykloturistiku	cykloturistika	k1gFnSc4	cykloturistika
a	a	k8xC	a
protínají	protínat	k5eAaImIp3nP	protínat
je	on	k3xPp3gFnPc4	on
cyklotrasy	cyklotrasa	k1gFnPc4	cyklotrasa
č.	č.	k?	č.
0024	[number]	k4	0024
(	(	kIx(	(
<g/>
úsek	úsek	k1gInSc1	úsek
Libice	Libice	k1gFnPc4	Libice
nad	nad	k7c7	nad
Cidlinou-Byšičky	Cidlinou-Byšička	k1gMnPc7	Cidlinou-Byšička
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
40	[number]	k4	40
km	km	kA	km
<g/>
)	)	kIx)	)
a	a	k8xC	a
č.	č.	k?	č.
0019	[number]	k4	0019
(	(	kIx(	(
<g/>
úsek	úsek	k1gInSc1	úsek
Poděbrady-Nymburk	Poděbrady-Nymburk	k1gInSc1	Poděbrady-Nymburk
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
8	[number]	k4	8
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
stezky	stezka	k1gFnPc1	stezka
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
městě	město	k1gNnSc6	město
propojeny	propojit	k5eAaPmNgInP	propojit
lávkou	lávka	k1gFnSc7	lávka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
budoucnu	budoucno	k1gNnSc6	budoucno
je	být	k5eAaImIp3nS	být
plánován	plánovat	k5eAaImNgInS	plánovat
vznik	vznik	k1gInSc1	vznik
cyklostezek	cyklostezka	k1gFnPc2	cyklostezka
přímo	přímo	k6eAd1	přímo
uvnitř	uvnitř	k7c2	uvnitř
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Cyklostezky	cyklostezka	k1gFnPc1	cyklostezka
podél	podél	k7c2	podél
řeky	řeka	k1gFnSc2	řeka
Labe	Labe	k1gNnSc2	Labe
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
vhodné	vhodný	k2eAgInPc1d1	vhodný
i	i	k9	i
pro	pro	k7c4	pro
inline	inlinout	k5eAaPmIp3nS	inlinout
bruslení	bruslený	k2eAgMnPc1d1	bruslený
a	a	k8xC	a
jak	jak	k6eAd1	jak
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
do	do	k7c2	do
Nymburka	Nymburk	k1gInSc2	Nymburk
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
soutoku	soutok	k1gInSc3	soutok
Labe	Labe	k1gNnSc2	Labe
a	a	k8xC	a
Cidliny	Cidlina	k1gFnSc2	Cidlina
(	(	kIx(	(
<g/>
zde	zde	k6eAd1	zde
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
až	až	k9	až
do	do	k7c2	do
Libice	Libice	k1gFnPc1	Libice
nad	nad	k7c7	nad
Cidlinou	Cidlina	k1gFnSc7	Cidlina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Skupici	Skupice	k1gFnSc6	Skupice
byla	být	k5eAaImAgFnS	být
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
naučná	naučný	k2eAgFnSc1d1	naučná
stezka	stezka	k1gFnSc1	stezka
<g/>
,	,	kIx,	,
vedoucí	vedoucí	k2eAgInSc1d1	vedoucí
lužním	lužní	k2eAgInSc7d1	lužní
lesem	les	k1gInSc7	les
až	až	k9	až
k	k	k7c3	k
soutoku	soutok	k1gInSc3	soutok
Labe	Labe	k1gNnSc2	Labe
a	a	k8xC	a
Cidliny	Cidlina	k1gFnSc2	Cidlina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
lužního	lužní	k2eAgInSc2d1	lužní
lesa	les	k1gInSc2	les
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
golfové	golfový	k2eAgNnSc1d1	golfové
hřiště	hřiště	k1gNnSc1	hřiště
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
zahájilo	zahájit	k5eAaPmAgNnS	zahájit
provoz	provoz	k1gInSc4	provoz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
bylo	být	k5eAaImAgNnS	být
hřiště	hřiště	k1gNnSc1	hřiště
devítijamkové	devítijamkový	k2eAgNnSc1d1	devítijamkové
a	a	k8xC	a
až	až	k9	až
poté	poté	k6eAd1	poté
bylo	být	k5eAaImAgNnS	být
rozšířeno	rozšířit	k5eAaPmNgNnS	rozšířit
na	na	k7c4	na
18	[number]	k4	18
jamek	jamka	k1gFnPc2	jamka
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
koupání	koupání	k1gNnSc4	koupání
slouží	sloužit	k5eAaImIp3nS	sloužit
přírodní	přírodní	k2eAgNnSc4d1	přírodní
koupaliště	koupaliště	k1gNnSc4	koupaliště
na	na	k7c6	na
jezeře	jezero	k1gNnSc6	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nP	nacházet
dvě	dva	k4xCgNnPc1	dva
fotbalová	fotbalový	k2eAgNnPc1d1	fotbalové
hřiště	hřiště	k1gNnPc1	hřiště
<g/>
,	,	kIx,	,
zimní	zimní	k2eAgInSc1d1	zimní
stadion	stadion	k1gInSc1	stadion
<g/>
,	,	kIx,	,
jízdárna	jízdárna	k1gFnSc1	jízdárna
s	s	k7c7	s
jezdeckým	jezdecký	k2eAgInSc7d1	jezdecký
klubem	klub	k1gInSc7	klub
<g/>
,	,	kIx,	,
skatepark	skatepark	k1gInSc4	skatepark
<g/>
,	,	kIx,	,
squash	squash	k1gInSc4	squash
a	a	k8xC	a
tenisové	tenisový	k2eAgInPc4d1	tenisový
kurty	kurt	k1gInPc4	kurt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
řece	řeka	k1gFnSc6	řeka
a	a	k8xC	a
jezerech	jezero	k1gNnPc6	jezero
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
rybařit	rybařit	k5eAaImF	rybařit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
2009	[number]	k4	2009
<g/>
–	–	k?	–
<g/>
2011	[number]	k4	2011
se	se	k3xPyFc4	se
v	v	k7c6	v
Poděbradech	Poděbrady	k1gInPc6	Poděbrady
hrála	hrát	k5eAaImAgFnS	hrát
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
soutěž	soutěž	k1gFnSc4	soutěž
v	v	k7c6	v
basketbale	basketbal	k1gInSc6	basketbal
mužů	muž	k1gMnPc2	muž
–	–	k?	–
Mattoni	Mattoň	k1gFnSc6	Mattoň
NBL	NBL	kA	NBL
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
70	[number]	k4	70
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
hrála	hrát	k5eAaImAgFnS	hrát
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
soutěž	soutěž	k1gFnSc4	soutěž
v	v	k7c6	v
basketbalu	basketbal	k1gInSc6	basketbal
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
I.	I.	kA	I.
<g/>
liga	liga	k1gFnSc1	liga
v	v	k7c6	v
podání	podání	k1gNnSc6	podání
Slavie	slavie	k1gFnSc2	slavie
VS	VS	kA	VS
Poděbrady	Poděbrady	k1gInPc1	Poděbrady
<g/>
,	,	kIx,	,
družstvo	družstvo	k1gNnSc1	družstvo
bylo	být	k5eAaImAgNnS	být
složené	složený	k2eAgNnSc1d1	složené
výhradně	výhradně	k6eAd1	výhradně
z	z	k7c2	z
poděbradských	poděbradský	k2eAgFnPc2d1	Poděbradská
hráček	hráčka	k1gFnPc2	hráčka
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
I.	I.	kA	I.
celostátní	celostátní	k2eAgFnSc1d1	celostátní
liga	liga	k1gFnSc1	liga
ve	v	k7c6	v
stolním	stolní	k2eAgInSc6d1	stolní
tenisu	tenis	k1gInSc6	tenis
<g/>
,	,	kIx,	,
oddíl	oddíl	k1gInSc1	oddíl
Sklo	sklo	k1gNnSc1	sklo
Bohemia	bohemia	k1gFnSc1	bohemia
Poděbrady	Poděbrady	k1gInPc4	Poděbrady
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Poděbradech	Poděbrady	k1gInPc6	Poděbrady
jsou	být	k5eAaImIp3nP	být
dvě	dva	k4xCgNnPc4	dva
fotbalová	fotbalový	k2eAgNnPc4d1	fotbalové
hřiště	hřiště	k1gNnPc4	hřiště
<g/>
,	,	kIx,	,
jedno	jeden	k4xCgNnSc1	jeden
U	u	k7c2	u
radiostanice	radiostanice	k1gFnSc2	radiostanice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hrála	hrát	k5eAaImAgFnS	hrát
svá	svůj	k3xOyFgNnPc4	svůj
utkání	utkání	k1gNnPc4	utkání
Sparta	Sparta	k1gFnSc1	Sparta
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
Jiskra	jiskra	k1gFnSc1	jiskra
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgNnSc1	druhý
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Oboře	obora	k1gFnSc6	obora
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hrál	hrát	k5eAaImAgInS	hrát
SK	Sk	kA	Sk
Lázně	lázeň	k1gFnSc2	lázeň
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
Slovan	Slovan	k1gMnSc1	Slovan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
sloučení	sloučení	k1gNnSc3	sloučení
pod	pod	k7c4	pod
název	název	k1gInSc4	název
Sklo	sklo	k1gNnSc1	sklo
Bohemia	bohemia	k1gFnSc1	bohemia
Poděbrady	Poděbrady	k1gInPc4	Poděbrady
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
byla	být	k5eAaImAgFnS	být
činnost	činnost	k1gFnSc1	činnost
Slovanu	Slovan	k1gInSc2	Slovan
obnovena	obnoven	k2eAgFnSc1d1	obnovena
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
osobností	osobnost	k1gFnPc2	osobnost
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
<g/>
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
(	(	kIx(	(
<g/>
1420	[number]	k4	1420
<g/>
–	–	k?	–
<g/>
1471	[number]	k4	1471
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
Viktorín	Viktorín	k1gMnSc1	Viktorín
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
(	(	kIx(	(
<g/>
1443	[number]	k4	1443
<g/>
–	–	k?	–
<g/>
1500	[number]	k4	1500
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
minsterberský	minsterberský	k2eAgMnSc1d1	minsterberský
a	a	k8xC	a
opavský	opavský	k2eAgMnSc1d1	opavský
kníže	kníže	k1gMnSc1	kníže
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
krále	král	k1gMnSc2	král
Jiřího	Jiří	k1gMnSc2	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
<g/>
.	.	kIx.	.
</s>
<s>
Kateřina	Kateřina	k1gFnSc1	Kateřina
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
(	(	kIx(	(
<g/>
1449	[number]	k4	1449
<g/>
–	–	k?	–
<g/>
1464	[number]	k4	1464
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
uherská	uherský	k2eAgFnSc1d1	uherská
královna	královna	k1gFnSc1	královna
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
Jiřího	Jiří	k1gMnSc2	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
Hynek	Hynek	k1gMnSc1	Hynek
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
(	(	kIx(	(
<g/>
1452	[number]	k4	1452
<g/>
–	–	k?	–
<g/>
1492	[number]	k4	1492
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
renesanční	renesanční	k2eAgMnSc1d1	renesanční
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
Jiřího	Jiří	k1gMnSc2	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
Bartoloměj	Bartoloměj	k1gMnSc1	Bartoloměj
z	z	k7c2	z
Minsterberka	Minsterberka	k1gFnSc1	Minsterberka
(	(	kIx(	(
<g/>
1477	[number]	k4	1477
<g />
.	.	kIx.	.
</s>
<s>
<g/>
–	–	k?	–
<g/>
1515	[number]	k4	1515
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
významný	významný	k2eAgMnSc1d1	významný
diplomat	diplomat	k1gMnSc1	diplomat
<g/>
,	,	kIx,	,
vnuk	vnuk	k1gMnSc1	vnuk
Jiřího	Jiří	k1gMnSc2	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
Matěj	Matěj	k1gMnSc1	Matěj
Rössler	Rössler	k1gMnSc1	Rössler
(	(	kIx(	(
<g/>
1754	[number]	k4	1754
<g/>
–	–	k?	–
<g/>
1829	[number]	k4	1829
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poděbradský	poděbradský	k2eAgMnSc1d1	poděbradský
děkan	děkan	k1gMnSc1	děkan
a	a	k8xC	a
zakladatel	zakladatel	k1gMnSc1	zakladatel
českého	český	k2eAgNnSc2d1	české
ovocnářství	ovocnářství	k1gNnSc2	ovocnářství
František	František	k1gMnSc1	František
Turinský	Turinský	k2eAgMnSc1d1	Turinský
(	(	kIx(	(
<g/>
1797	[number]	k4	1797
<g/>
–	–	k?	–
<g/>
1852	[number]	k4	1852
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obrozenecký	obrozenecký	k2eAgMnSc1d1	obrozenecký
básník	básník	k1gMnSc1	básník
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
úředník	úředník	k1gMnSc1	úředník
a	a	k8xC	a
soudce	soudce	k1gMnSc1	soudce
Ludvík	Ludvík	k1gMnSc1	Ludvík
Kuba	Kuba	k1gMnSc1	Kuba
(	(	kIx(	(
<g/>
1863	[number]	k4	1863
<g/>
–	–	k?	–
<g/>
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
folklorista	folklorista	k1gMnSc1	folklorista
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
Gustav	Gustav	k1gMnSc1	Gustav
Friedrich	Friedrich	k1gMnSc1	Friedrich
(	(	kIx(	(
<g/>
1871	[number]	k4	1871
<g/>
–	–	k?	–
<g/>
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
archivář	archivář	k1gMnSc1	archivář
<g/>
,	,	kIx,	,
editor	editor	k1gMnSc1	editor
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
Antonín	Antonín	k1gMnSc1	Antonín
Engel	Engel	k1gMnSc1	Engel
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
1879	[number]	k4	1879
<g/>
–	–	k?	–
<g/>
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
architekt	architekt	k1gMnSc1	architekt
<g/>
,	,	kIx,	,
urbanista	urbanista	k1gMnSc1	urbanista
a	a	k8xC	a
teoretik	teoretik	k1gMnSc1	teoretik
architektury	architektura	k1gFnSc2	architektura
Leopolda	Leopolda	k1gFnSc1	Leopolda
Dostálová	Dostálová	k1gFnSc1	Dostálová
(	(	kIx(	(
<g/>
1879	[number]	k4	1879
<g/>
-	-	kIx~	-
<g/>
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
slavná	slavný	k2eAgFnSc1d1	slavná
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
<g/>
,	,	kIx,	,
členka	členka	k1gFnSc1	členka
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
Jan	Jan	k1gMnSc1	Jan
Trampota	trampota	k1gFnSc1	trampota
(	(	kIx(	(
<g/>
1889	[number]	k4	1889
Žižkov	Žižkov	k1gInSc1	Žižkov
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
1942	[number]	k4	1942
Poděbrady	Poděbrady	k1gInPc4	Poděbrady
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Poděbradech	Poděbrady	k1gInPc6	Poděbrady
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Franz	Franz	k1gMnSc1	Franz
Janowitz	Janowitz	k1gMnSc1	Janowitz
(	(	kIx(	(
<g/>
1892	[number]	k4	1892
<g/>
–	–	k?	–
<g/>
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
píšící	píšící	k2eAgMnSc1d1	píšící
básník	básník	k1gMnSc1	básník
Albert	Albert	k1gMnSc1	Albert
Pek	Pek	k1gMnSc1	Pek
(	(	kIx(	(
<g/>
1893	[number]	k4	1893
<g/>
–	–	k?	–
<g/>
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
folklorista	folklorista	k1gMnSc1	folklorista
<g/>
,	,	kIx,	,
dirigent	dirigent	k1gMnSc1	dirigent
a	a	k8xC	a
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
Alén	Alén	k1gMnSc1	Alén
Diviš	Diviš	k1gMnSc1	Diviš
(	(	kIx(	(
<g/>
1900	[number]	k4	1900
<g/>
–	–	k?	–
<g/>
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
malíř	malíř	k1gMnSc1	malíř
Ctirad	Ctirad	k1gMnSc1	Ctirad
Mašín	Mašín	k1gMnSc1	Mašín
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
–	–	k?	–
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
příslušník	příslušník	k1gMnSc1	příslušník
československého	československý	k2eAgInSc2d1	československý
protikomunistického	protikomunistický	k2eAgInSc2d1	protikomunistický
odboje	odboj	k1gInSc2	odboj
Milan	Milan	k1gMnSc1	Milan
Paumer	Paumer	k1gMnSc1	Paumer
(	(	kIx(	(
<g/>
*	*	kIx~	*
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
příslušník	příslušník	k1gMnSc1	příslušník
československého	československý	k2eAgInSc2d1	československý
protikomunistického	protikomunistický	k2eAgInSc2d1	protikomunistický
odboje	odboj	k1gInSc2	odboj
Josef	Josef	k1gMnSc1	Josef
Mašín	Mašín	k1gMnSc1	Mašín
(	(	kIx(	(
<g/>
*	*	kIx~	*
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
příslušník	příslušník	k1gMnSc1	příslušník
československého	československý	k2eAgInSc2d1	československý
protikomunistického	protikomunistický	k2eAgInSc2d1	protikomunistický
odboje	odboj	k1gInSc2	odboj
Miloš	Miloš	k1gMnSc1	Miloš
Forman	Forman	k1gMnSc1	Forman
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
*	*	kIx~	*
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
–	–	k?	–
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
prezident	prezident	k1gMnSc1	prezident
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
dramatik	dramatik	k1gMnSc1	dramatik
Jaroslava	Jaroslava	k1gFnSc1	Jaroslava
Staňková	Staňková	k1gFnSc1	Staňková
(	(	kIx(	(
<g/>
1937	[number]	k4	1937
<g/>
–	–	k?	–
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
architektka	architektka	k1gFnSc1	architektka
Marta	Marta	k1gFnSc1	Marta
Kubišová	Kubišová	k1gFnSc1	Kubišová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
<g />
.	.	kIx.	.
</s>
<s>
Alice	Alice	k1gFnSc1	Alice
Nellis	Nellis	k1gFnSc1	Nellis
(	(	kIx(	(
<g/>
*	*	kIx~	*
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
režisérka	režisérka	k1gFnSc1	režisérka
a	a	k8xC	a
scenáristka	scenáristka	k1gFnSc1	scenáristka
prof.	prof.	kA	prof.
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Hájek	Hájek	k1gMnSc1	Hájek
(	(	kIx(	(
<g/>
1926	[number]	k4	1926
<g/>
–	–	k?	–
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
matematik	matematik	k1gMnSc1	matematik
<g/>
,	,	kIx,	,
matematický	matematický	k2eAgMnSc1d1	matematický
statistik	statistik	k1gMnSc1	statistik
světového	světový	k2eAgInSc2d1	světový
významu	význam	k1gInSc2	význam
Nědaleko	Nědaleko	k1gNnSc1	Nědaleko
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
obec	obec	k1gFnSc1	obec
Libice	Libice	k1gFnPc1	Libice
nad	nad	k7c7	nad
Cidlinou	Cidlina	k1gFnSc7	Cidlina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
vidět	vidět	k5eAaImF	vidět
pozůstatky	pozůstatek	k1gInPc4	pozůstatek
slavníkovského	slavníkovský	k2eAgNnSc2d1	slavníkovské
hradiště	hradiště	k1gNnSc2	hradiště
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
soutoku	soutok	k1gInSc2	soutok
Labe	Labe	k1gNnSc2	Labe
a	a	k8xC	a
Cidliny	Cidlina	k1gFnSc2	Cidlina
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
si	se	k3xPyFc3	se
prohlédnout	prohlédnout	k5eAaPmF	prohlédnout
zavěšený	zavěšený	k2eAgInSc4d1	zavěšený
dálniční	dálniční	k2eAgInSc4d1	dálniční
most	most	k1gInSc4	most
přes	přes	k7c4	přes
Labe	Labe	k1gNnSc4	Labe
<g/>
,	,	kIx,	,
postavený	postavený	k2eAgInSc4d1	postavený
v	v	k7c6	v
letech	let	k1gInPc6	let
1985	[number]	k4	1985
<g/>
–	–	k?	–
<g/>
1989	[number]	k4	1989
podle	podle	k7c2	podle
projektu	projekt	k1gInSc2	projekt
architektů	architekt	k1gMnPc2	architekt
Jiřího	Jiří	k1gMnSc2	Jiří
Kopřivy	Kopřiva	k1gMnSc2	Kopřiva
a	a	k8xC	a
Jindřicha	Jindřich	k1gMnSc2	Jindřich
Kaňka	kaňka	k1gFnSc1	kaňka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
také	také	k9	také
nachází	nacházet	k5eAaImIp3nS	nacházet
potok	potok	k1gInSc4	potok
Jamborovka	Jamborovka	k1gFnSc1	Jamborovka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
pramení	pramenit	k5eAaImIp3nS	pramenit
v	v	k7c6	v
Bažantnici	bažantnice	k1gFnSc6	bažantnice
a	a	k8xC	a
protéká	protékat	k5eAaImIp3nS	protékat
zahrádkářskou	zahrádkářský	k2eAgFnSc7d1	zahrádkářská
kolonií	kolonie	k1gFnSc7	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Jamborovce	Jamborovka	k1gFnSc6	Jamborovka
žijí	žít	k5eAaImIp3nP	žít
některé	některý	k3yIgInPc4	některý
pozoruhodné	pozoruhodný	k2eAgInPc4d1	pozoruhodný
živočišné	živočišný	k2eAgInPc4d1	živočišný
druhy	druh	k1gInPc4	druh
<g/>
,	,	kIx,	,
jmenovitě	jmenovitě	k6eAd1	jmenovitě
listonoh	listonoh	k1gMnSc1	listonoh
a	a	k8xC	a
jiní	jiný	k2eAgMnPc1d1	jiný
korýši	korýš	k1gMnPc1	korýš
<g/>
,	,	kIx,	,
plovatka	plovatka	k1gFnSc1	plovatka
bahenní	bahenní	k2eAgMnPc1d1	bahenní
a	a	k8xC	a
obojživelníci	obojživelník	k1gMnPc1	obojživelník
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
hladině	hladina	k1gFnSc6	hladina
se	se	k3xPyFc4	se
prohánějí	prohánět	k5eAaImIp3nP	prohánět
slípky	slípka	k1gFnPc1	slípka
zelenonohé	zelenonohý	k2eAgFnPc1d1	zelenonohá
či	či	k8xC	či
kachny	kachna	k1gFnPc1	kachna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
březích	břeh	k1gInPc6	břeh
roste	růst	k5eAaImIp3nS	růst
rákos	rákos	k1gInSc1	rákos
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
vtokem	vtok	k1gInSc7	vtok
do	do	k7c2	do
zahrádkářské	zahrádkářský	k2eAgFnSc2d1	zahrádkářská
kolonie	kolonie	k1gFnSc2	kolonie
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
malé	malý	k2eAgNnSc4d1	malé
jezírko	jezírko	k1gNnSc4	jezírko
s	s	k7c7	s
bahnitým	bahnitý	k2eAgMnSc7d1	bahnitý
<g/>
,	,	kIx,	,
podmáčeným	podmáčený	k2eAgNnSc7d1	podmáčené
okolím	okolí	k1gNnSc7	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
významná	významný	k2eAgFnSc1d1	významná
lokalita	lokalita	k1gFnSc1	lokalita
výskytu	výskyt	k1gInSc2	výskyt
obojživelníků	obojživelník	k1gMnPc2	obojživelník
<g/>
.	.	kIx.	.
</s>
<s>
Piešťany	Piešťany	k1gInPc1	Piešťany
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc1	Slovensko
Mariánské	mariánský	k2eAgFnPc1d1	Mariánská
Lázně	lázeň	k1gFnPc1	lázeň
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
Netanja	Netanja	k1gFnSc1	Netanja
<g/>
,	,	kIx,	,
Izrael	Izrael	k1gInSc1	Izrael
Vertou	Vertá	k1gFnSc4	Vertá
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
</s>
