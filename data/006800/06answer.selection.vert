<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1907	[number]	k4	1907
už	už	k6eAd1	už
byly	být	k5eAaImAgInP	být
vyvrtány	vyvrtán	k2eAgInPc1d1	vyvrtán
tři	tři	k4xCgInPc1	tři
veřejné	veřejný	k2eAgInPc1d1	veřejný
prameny	pramen	k1gInPc1	pramen
(	(	kIx(	(
<g/>
Bülowův	Bülowův	k2eAgInSc1d1	Bülowův
<g/>
,	,	kIx,	,
Hohenlohe	Hohenloh	k1gInSc2	Hohenloh
a	a	k8xC	a
Charicléa	Chariclé	k1gInSc2	Chariclé
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
byly	být	k5eAaImAgFnP	být
založeny	založit	k5eAaPmNgFnP	založit
poděbradské	poděbradský	k2eAgFnPc1d1	Poděbradská
lázně	lázeň	k1gFnPc1	lázeň
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
další	další	k2eAgInSc4d1	další
rozvoj	rozvoj	k1gInSc4	rozvoj
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
