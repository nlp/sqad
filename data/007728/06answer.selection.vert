<s>
Většina	většina	k1gFnSc1	většina
komet	kometa	k1gFnPc2	kometa
se	se	k3xPyFc4	se
po	po	k7c4	po
většinu	většina	k1gFnSc4	většina
času	čas	k1gInSc2	čas
zdržuje	zdržovat	k5eAaImIp3nS	zdržovat
za	za	k7c7	za
oběžnou	oběžný	k2eAgFnSc7d1	oběžná
dráhou	dráha	k1gFnSc7	dráha
Pluta	Pluto	k1gNnSc2	Pluto
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
občas	občas	k6eAd1	občas
nějaká	nějaký	k3yIgFnSc1	nějaký
přilétne	přilétnout	k5eAaPmIp3nS	přilétnout
do	do	k7c2	do
vnitřních	vnitřní	k2eAgFnPc2d1	vnitřní
částí	část	k1gFnPc2	část
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
