<s>
Zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
pohybem	pohyb	k1gInSc7	pohyb
těles	těleso	k1gNnPc2	těleso
v	v	k7c6	v
gravitačním	gravitační	k2eAgNnSc6d1	gravitační
poli	pole	k1gNnSc6	pole
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
planet	planeta	k1gFnPc2	planeta
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
</s>
