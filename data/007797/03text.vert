<s>
Pojmem	pojem	k1gInSc7	pojem
klasický	klasický	k2eAgInSc1d1	klasický
Hollywood	Hollywood	k1gInSc1	Hollywood
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
období	období	k1gNnSc1	období
americké	americký	k2eAgFnSc2d1	americká
kinematografie	kinematografie	k1gFnSc2	kinematografie
zhruba	zhruba	k6eAd1	zhruba
od	od	k7c2	od
konce	konec	k1gInSc2	konec
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
do	do	k7c2	do
pozdních	pozdní	k2eAgNnPc2d1	pozdní
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Přičemž	přičemž	k6eAd1	přičemž
tento	tento	k3xDgInSc1	tento
termín	termín	k1gInSc1	termín
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
jednak	jednak	k8xC	jednak
specifický	specifický	k2eAgInSc1d1	specifický
styl	styl	k1gInSc1	styl
natáčení	natáčení	k1gNnSc2	natáčení
filmů	film	k1gInPc2	film
a	a	k8xC	a
jednak	jednak	k8xC	jednak
specifický	specifický	k2eAgInSc1d1	specifický
mód	mód	k1gInSc1	mód
produkce	produkce	k1gFnSc2	produkce
<g/>
,	,	kIx,	,
distribuce	distribuce	k1gFnSc2	distribuce
a	a	k8xC	a
uvádění	uvádění	k1gNnSc6	uvádění
filmů	film	k1gInPc2	film
v	v	k7c6	v
americké	americký	k2eAgFnSc6d1	americká
kinematografii	kinematografie	k1gFnSc6	kinematografie
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
<g/>
.	.	kIx.	.
</s>
<s>
Klasický	klasický	k2eAgInSc1d1	klasický
Hollywood	Hollywood	k1gInSc1	Hollywood
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
Zlatý	zlatý	k2eAgInSc1d1	zlatý
věk	věk	k1gInSc1	věk
Hollywoodu	Hollywood	k1gInSc2	Hollywood
<g/>
"	"	kIx"	"
a	a	k8xC	a
specifické	specifický	k2eAgInPc4d1	specifický
stylové	stylový	k2eAgInPc4d1	stylový
prostředky	prostředek	k1gInPc4	prostředek
<g/>
,	,	kIx,	,
rozvinuté	rozvinutý	k2eAgInPc4d1	rozvinutý
během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
periody	perioda	k1gFnSc2	perioda
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
klasický	klasický	k2eAgInSc4d1	klasický
hollywoodský	hollywoodský	k2eAgInSc4d1	hollywoodský
styl	styl	k1gInSc4	styl
<g/>
.	.	kIx.	.
</s>
<s>
Zlatý	zlatý	k2eAgInSc1d1	zlatý
věk	věk	k1gInSc1	věk
Hollywoodu	Hollywood	k1gInSc2	Hollywood
trval	trvat	k5eAaImAgInS	trvat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
30	[number]	k4	30
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
od	od	k7c2	od
konce	konec	k1gInSc2	konec
němé	němý	k2eAgFnSc2d1	němá
éry	éra	k1gFnSc2	éra
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
filmu	film	k1gInSc6	film
na	na	k7c6	na
konci	konec	k1gInSc6	konec
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
do	do	k7c2	do
pozdních	pozdní	k2eAgNnPc2d1	pozdní
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Klasická	klasický	k2eAgFnSc1d1	klasická
hollywoodská	hollywoodský	k2eAgFnSc1d1	hollywoodská
kinematografie	kinematografie	k1gFnSc1	kinematografie
byla	být	k5eAaImAgFnS	být
velmi	velmi	k6eAd1	velmi
"	"	kIx"	"
<g/>
žánrová	žánrový	k2eAgFnSc1d1	žánrová
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
hollywoodských	hollywoodský	k2eAgInPc2d1	hollywoodský
filmů	film	k1gInPc2	film
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
se	se	k3xPyFc4	se
striktně	striktně	k6eAd1	striktně
držela	držet	k5eAaImAgFnS	držet
určitého	určitý	k2eAgInSc2d1	určitý
žánru	žánr	k1gInSc2	žánr
<g/>
,	,	kIx,	,
populární	populární	k2eAgFnPc1d1	populární
byly	být	k5eAaImAgFnP	být
například	například	k6eAd1	například
westerny	western	k1gInPc4	western
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
muzikály	muzikál	k1gInPc1	muzikál
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
nástupem	nástup	k1gInSc7	nástup
klasického	klasický	k2eAgInSc2d1	klasický
Hollywoodu	Hollywood	k1gInSc2	Hollywood
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
během	během	k7c2	během
pozdních	pozdní	k2eAgNnPc2d1	pozdní
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
a	a	k8xC	a
raných	raný	k2eAgNnPc2d1	rané
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
americké	americký	k2eAgFnSc6d1	americká
kinematografii	kinematografie	k1gFnSc6	kinematografie
rozšířené	rozšířený	k2eAgInPc4d1	rozšířený
filmy	film	k1gInPc4	film
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgInP	být
značně	značně	k6eAd1	značně
provokativní	provokativní	k2eAgInPc1d1	provokativní
a	a	k8xC	a
z	z	k7c2	z
morálního	morální	k2eAgNnSc2d1	morální
hlediska	hledisko	k1gNnSc2	hledisko
velmi	velmi	k6eAd1	velmi
kontroverzní	kontroverzní	k2eAgMnSc1d1	kontroverzní
v	v	k7c6	v
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
společnosti	společnost	k1gFnSc6	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
například	například	k6eAd1	například
o	o	k7c4	o
celý	celý	k2eAgInSc4d1	celý
žánr	žánr	k1gInSc4	žánr
gangsterského	gangsterský	k2eAgInSc2d1	gangsterský
filmu	film	k1gInSc2	film
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
o	o	k7c4	o
snímky	snímek	k1gInPc4	snímek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
dosti	dosti	k6eAd1	dosti
sexuálně	sexuálně	k6eAd1	sexuálně
odvážné	odvážný	k2eAgNnSc1d1	odvážné
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
krátké	krátký	k2eAgNnSc1d1	krátké
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
slavné	slavný	k2eAgNnSc1d1	slavné
období	období	k1gNnSc1	období
americké	americký	k2eAgFnSc2d1	americká
kinematografie	kinematografie	k1gFnSc2	kinematografie
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
jako	jako	k8xS	jako
Pre-kodexové	Preodexový	k2eAgNnSc1d1	Pre-kodexový
období	období	k1gNnSc1	období
Hollywoodu	Hollywood	k1gInSc2	Hollywood
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
prezident	prezident	k1gMnSc1	prezident
The	The	k1gMnSc1	The
Motion	Motion	k1gInSc4	Motion
Pictures	Pictures	k1gInSc1	Pictures
Producers	Producers	k1gInSc1	Producers
and	and	k?	and
Distributors	Distributors	k1gInSc1	Distributors
Association	Association	k1gInSc1	Association
(	(	kIx(	(
<g/>
MPPDA	MPPDA	kA	MPPDA
<g/>
)	)	kIx)	)
Will	Will	k1gInSc1	Will
Hays	Haysa	k1gFnPc2	Haysa
proto	proto	k8xC	proto
zavedl	zavést	k5eAaPmAgInS	zavést
Produkční	produkční	k2eAgInSc1d1	produkční
kodex	kodex	k1gInSc1	kodex
<g/>
,	,	kIx,	,
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
také	také	k9	také
"	"	kIx"	"
<g/>
Haysův	Haysův	k2eAgInSc1d1	Haysův
kodex	kodex	k1gInSc1	kodex
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vyžadoval	vyžadovat	k5eAaImAgMnS	vyžadovat
plnění	plnění	k1gNnSc4	plnění
určitých	určitý	k2eAgNnPc2d1	určité
předem	předem	k6eAd1	předem
daných	daný	k2eAgNnPc2d1	dané
cenzurních	cenzurní	k2eAgNnPc2d1	cenzurní
opatření	opatření	k1gNnPc2	opatření
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
tento	tento	k3xDgInSc1	tento
kodex	kodex	k1gInSc1	kodex
byl	být	k5eAaImAgInS	být
chápán	chápat	k5eAaImNgInS	chápat
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
menší	malý	k2eAgNnSc1d2	menší
zlo	zlo	k1gNnSc1	zlo
<g/>
"	"	kIx"	"
a	a	k8xC	a
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
jako	jako	k9	jako
reakce	reakce	k1gFnPc1	reakce
na	na	k7c4	na
možné	možný	k2eAgNnSc4d1	možné
zavedení	zavedení	k1gNnSc4	zavedení
nechtěné	chtěný	k2eNgFnSc2d1	nechtěná
státní	státní	k2eAgFnSc2d1	státní
cenzury	cenzura	k1gFnSc2	cenzura
v	v	k7c6	v
americké	americký	k2eAgFnSc6d1	americká
kinematografii	kinematografie	k1gFnSc6	kinematografie
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
produkční	produkční	k2eAgInSc1d1	produkční
kodex	kodex	k1gInSc1	kodex
nebyl	být	k5eNaImAgInS	být
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
uplatňován	uplatňován	k2eAgMnSc1d1	uplatňován
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1934	[number]	k4	1934
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
vlivná	vlivný	k2eAgFnSc1d1	vlivná
katolická	katolický	k2eAgFnSc1d1	katolická
organizace	organizace	k1gFnSc1	organizace
Legie	legie	k1gFnSc2	legie
slušnosti	slušnost	k1gFnSc2	slušnost
extrémně	extrémně	k6eAd1	extrémně
pobouřena	pobouřen	k2eAgFnSc1d1	pobouřena
kvůli	kvůli	k7c3	kvůli
dvěma	dva	k4xCgFnPc7	dva
sexuálně	sexuálně	k6eAd1	sexuálně
provokativním	provokativní	k2eAgInPc3d1	provokativní
filmům	film	k1gInPc3	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1933	[number]	k4	1933
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgMnPc6	který
hrála	hrát	k5eAaImAgFnS	hrát
Mae	Mae	k1gMnPc3	Mae
West	West	k1gInSc4	West
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
"	"	kIx"	"
<g/>
incidentu	incident	k1gInSc6	incident
<g/>
"	"	kIx"	"
bylo	být	k5eAaImAgNnS	být
již	již	k9	již
dodržování	dodržování	k1gNnSc1	dodržování
produkčního	produkční	k2eAgInSc2d1	produkční
kodexu	kodex	k1gInSc2	kodex
vyžadováno	vyžadován	k2eAgNnSc1d1	vyžadováno
a	a	k8xC	a
tato	tento	k3xDgFnSc1	tento
vynucená	vynucený	k2eAgFnSc1d1	vynucená
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
cenzura	cenzura	k1gFnSc1	cenzura
následně	následně	k6eAd1	následně
výrazně	výrazně	k6eAd1	výrazně
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
podobu	podoba	k1gFnSc4	podoba
klasické	klasický	k2eAgFnSc2d1	klasická
hollywoodské	hollywoodský	k2eAgFnSc2d1	hollywoodská
kinematografie	kinematografie	k1gFnSc2	kinematografie
<g/>
.	.	kIx.	.
</s>
<s>
Hollywoodský	hollywoodský	k2eAgInSc1d1	hollywoodský
studiový	studiový	k2eAgInSc1d1	studiový
systém	systém	k1gInSc1	systém
byl	být	k5eAaImAgInS	být
kontrolován	kontrolován	k2eAgInSc4d1	kontrolován
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
velkou	velký	k2eAgFnSc7d1	velká
osmičkou	osmička	k1gFnSc7	osmička
<g/>
"	"	kIx"	"
studií	studie	k1gFnSc7	studie
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
"	"	kIx"	"
<g/>
velká	velký	k2eAgFnSc1d1	velká
pětka	pětka	k1gFnSc1	pětka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byla	být	k5eAaImAgFnS	být
plně	plně	k6eAd1	plně
vertikálně	vertikálně	k6eAd1	vertikálně
integrovaná	integrovaný	k2eAgNnPc1d1	integrované
studia	studio	k1gNnPc1	studio
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
nejsilnější	silný	k2eAgFnSc1d3	nejsilnější
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
tuto	tento	k3xDgFnSc4	tento
pětici	pětice	k1gFnSc4	pětice
nejmocnějších	mocný	k2eAgFnPc2d3	nejmocnější
a	a	k8xC	a
nejdůležitějších	důležitý	k2eAgFnPc2d3	nejdůležitější
studií	studie	k1gFnPc2	studie
patřila	patřit	k5eAaImAgFnS	patřit
studia	studio	k1gNnPc4	studio
MGM	MGM	kA	MGM
<g/>
,	,	kIx,	,
Warner	Warner	k1gMnSc1	Warner
Brothers	Brothersa	k1gFnPc2	Brothersa
<g/>
,	,	kIx,	,
20	[number]	k4	20
<g/>
th	th	k?	th
Century	Centura	k1gFnSc2	Centura
Fox	fox	k1gInSc1	fox
<g/>
,	,	kIx,	,
Paramount	Paramount	k1gInSc1	Paramount
a	a	k8xC	a
RKO	RKO	kA	RKO
<g/>
.	.	kIx.	.
</s>
<s>
Všechna	všechen	k3xTgNnPc1	všechen
tato	tento	k3xDgNnPc1	tento
studia	studio	k1gNnPc1	studio
jednak	jednak	k8xC	jednak
produkovala	produkovat	k5eAaImAgFnS	produkovat
filmy	film	k1gInPc4	film
<g/>
,	,	kIx,	,
a	a	k8xC	a
jednak	jednak	k8xC	jednak
vlastnila	vlastnit	k5eAaImAgFnS	vlastnit
své	svůj	k3xOyFgInPc4	svůj
vlastní	vlastní	k2eAgInPc4d1	vlastní
řetězce	řetězec	k1gInPc4	řetězec
kin	kino	k1gNnPc2	kino
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
tomu	ten	k3xDgNnSc3	ten
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
malá	malý	k2eAgFnSc1d1	malá
trojka	trojka	k1gFnSc1	trojka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byla	být	k5eAaImAgFnS	být
studia	studio	k1gNnSc2	studio
Universal	Universal	k1gMnSc2	Universal
Studios	Studios	k?	Studios
<g/>
,	,	kIx,	,
Columbia	Columbia	k1gFnSc1	Columbia
Pictures	Pictures	k1gMnSc1	Pictures
a	a	k8xC	a
United	United	k1gMnSc1	United
Artists	Artistsa	k1gFnPc2	Artistsa
<g/>
,	,	kIx,	,
sice	sice	k8xC	sice
produkovala	produkovat	k5eAaImAgFnS	produkovat
filmy	film	k1gInPc4	film
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neměla	mít	k5eNaImAgFnS	mít
takový	takový	k3xDgInSc4	takový
finanční	finanční	k2eAgInSc4d1	finanční
kapitál	kapitál	k1gInSc4	kapitál
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
velká	velký	k2eAgFnSc1d1	velká
pětka	pětka	k1gFnSc1	pětka
<g/>
"	"	kIx"	"
a	a	k8xC	a
tak	tak	k6eAd1	tak
produkovala	produkovat	k5eAaImAgFnS	produkovat
méně	málo	k6eAd2	málo
"	"	kIx"	"
<g/>
áčkových	áčkův	k2eAgInPc2d1	áčkův
<g/>
"	"	kIx"	"
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgFnP	být
základem	základ	k1gInSc7	základ
studiového	studiový	k2eAgInSc2d1	studiový
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
studia	studio	k1gNnSc2	studio
navíc	navíc	k6eAd1	navíc
nevlastnila	vlastnit	k5eNaImAgFnS	vlastnit
žádná	žádný	k3yNgNnPc4	žádný
kina	kino	k1gNnPc4	kino
<g/>
.	.	kIx.	.
</s>
<s>
Studiový	studiový	k2eAgInSc1d1	studiový
systém	systém	k1gInSc1	systém
byl	být	k5eAaImAgInS	být
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
vysokou	vysoký	k2eAgFnSc7d1	vysoká
mírou	míra	k1gFnSc7	míra
standardizace	standardizace	k1gFnSc2	standardizace
natáčení	natáčení	k1gNnSc2	natáčení
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
filmoví	filmový	k2eAgMnPc1d1	filmový
pracovníci	pracovník	k1gMnPc1	pracovník
byli	být	k5eAaImAgMnP	být
zaměstnanci	zaměstnanec	k1gMnPc1	zaměstnanec
určitého	určitý	k2eAgNnSc2d1	určité
studia	studio	k1gNnSc2	studio
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
jistou	jistý	k2eAgFnSc4d1	jistá
uniformitu	uniformita	k1gFnSc4	uniformita
filmového	filmový	k2eAgInSc2d1	filmový
stylu	styl	k1gInSc2	styl
–	–	k?	–
režiséři	režisér	k1gMnPc1	režisér
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
klíčoví	klíčový	k2eAgMnPc1d1	klíčový
tvůrčí	tvůrčí	k2eAgMnPc1d1	tvůrčí
pracovníci	pracovník	k1gMnPc1	pracovník
o	o	k7c6	o
sobě	se	k3xPyFc3	se
mysleli	myslet	k5eAaImAgMnP	myslet
spíše	spíše	k9	spíše
jako	jako	k9	jako
o	o	k7c6	o
zaměstnancích	zaměstnanec	k1gMnPc6	zaměstnanec
než	než	k8xS	než
jako	jako	k9	jako
o	o	k7c6	o
umělcích	umělec	k1gMnPc6	umělec
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
nebylo	být	k5eNaImAgNnS	být
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
rozvedeno	rozveden	k2eAgNnSc1d1	rozvedeno
auteurství	auteurství	k1gNnSc1	auteurství
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
specifický	specifický	k2eAgInSc1d1	specifický
přístup	přístup	k1gInSc1	přístup
k	k	k7c3	k
filmu	film	k1gInSc3	film
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
považuje	považovat	k5eAaImIp3nS	považovat
režiséra	režisér	k1gMnSc4	režisér
za	za	k7c4	za
autora	autor	k1gMnSc4	autor
<g/>
,	,	kIx,	,
hlavního	hlavní	k2eAgMnSc2d1	hlavní
tvůrce	tvůrce	k1gMnSc2	tvůrce
filmového	filmový	k2eAgNnSc2d1	filmové
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
paradigma	paradigma	k1gNnSc1	paradigma
vtrhlo	vtrhnout	k5eAaPmAgNnS	vtrhnout
do	do	k7c2	do
americké	americký	k2eAgFnSc2d1	americká
kinematografie	kinematografie	k1gFnSc2	kinematografie
až	až	k9	až
v	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
post-klasickém	postlasický	k2eAgNnSc6d1	post-klasický
období	období	k1gNnSc6	období
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
někteří	některý	k3yIgMnPc1	některý
etablovaní	etablovaný	k2eAgMnPc1d1	etablovaný
režiséři	režisér	k1gMnPc1	režisér
<g/>
,	,	kIx,	,
pracující	pracující	k2eAgMnPc1d1	pracující
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
studiového	studiový	k2eAgInSc2d1	studiový
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
Alfred	Alfred	k1gMnSc1	Alfred
Hitchcock	Hitchcock	k1gMnSc1	Hitchcock
<g/>
,	,	kIx,	,
Orson	Orson	k1gMnSc1	Orson
Welles	Welles	k1gMnSc1	Welles
<g/>
,	,	kIx,	,
Howard	Howard	k1gMnSc1	Howard
Hawks	Hawksa	k1gFnPc2	Hawksa
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
John	John	k1gMnSc1	John
Ford	ford	k1gInSc1	ford
realizovali	realizovat	k5eAaBmAgMnP	realizovat
své	svůj	k3xOyFgFnPc4	svůj
individuální	individuální	k2eAgFnPc4d1	individuální
umělecké	umělecký	k2eAgFnPc4d1	umělecká
vize	vize	k1gFnPc4	vize
a	a	k8xC	a
byli	být	k5eAaImAgMnP	být
později	pozdě	k6eAd2	pozdě
prohlášeni	prohlásit	k5eAaPmNgMnP	prohlásit
za	za	k7c4	za
plnohodnotné	plnohodnotný	k2eAgInPc4d1	plnohodnotný
americké	americký	k2eAgInPc4d1	americký
auteury	auteur	k1gInPc4	auteur
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
USA	USA	kA	USA
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
dosavadní	dosavadní	k2eAgFnSc1d1	dosavadní
praxe	praxe	k1gFnSc1	praxe
distribuce	distribuce	k1gFnSc2	distribuce
filmů	film	k1gInPc2	film
pomocí	pomocí	k7c2	pomocí
balíčkové	balíčkový	k2eAgFnSc2d1	Balíčková
metody	metoda	k1gFnSc2	metoda
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
prodávalo	prodávat	k5eAaImAgNnS	prodávat
několik	několik	k4yIc1	několik
filmů	film	k1gInPc2	film
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nezákonná	zákonný	k2eNgFnSc1d1	nezákonná
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
a	a	k8xC	a
provozování	provozování	k1gNnSc2	provozování
řetězce	řetězec	k1gInSc2	řetězec
kin	kino	k1gNnPc2	kino
hlavními	hlavní	k2eAgMnPc7d1	hlavní
filmovými	filmový	k2eAgNnPc7d1	filmové
studii	studio	k1gNnPc7	studio
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc4	tento
vertikálně	vertikálně	k6eAd1	vertikálně
integrovaný	integrovaný	k2eAgInSc4d1	integrovaný
systém	systém	k1gInSc4	systém
konstituoval	konstituovat	k5eAaBmAgInS	konstituovat
nesoutěžní	soutěžní	k2eNgFnPc4d1	nesoutěžní
a	a	k8xC	a
monopolní	monopolní	k2eAgFnPc4d1	monopolní
tržní	tržní	k2eAgFnPc4d1	tržní
praktiky	praktika	k1gFnPc4	praktika
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
byla	být	k5eAaImAgFnS	být
klíčová	klíčový	k2eAgFnSc1d1	klíčová
změna	změna	k1gFnSc1	změna
v	v	k7c6	v
hollywoodském	hollywoodský	k2eAgInSc6d1	hollywoodský
studiovém	studiový	k2eAgInSc6d1	studiový
systému	systém	k1gInSc6	systém
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
uvolnila	uvolnit	k5eAaPmAgFnS	uvolnit
cestu	cesta	k1gFnSc4	cesta
rostoucímu	rostoucí	k2eAgInSc3d1	rostoucí
počtu	počet	k1gInSc3	počet
nezávislých	závislý	k2eNgMnPc2d1	nezávislý
producentů	producent	k1gMnPc2	producent
a	a	k8xC	a
řídícím	řídící	k2eAgMnPc3d1	řídící
pracovníkům	pracovník	k1gMnPc3	pracovník
menších	malý	k2eAgNnPc2d2	menší
studií	studio	k1gNnPc2	studio
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
od	od	k7c2	od
teď	teď	k6eAd1	teď
mohli	moct	k5eAaImAgMnP	moct
produkovat	produkovat	k5eAaImF	produkovat
filmy	film	k1gInPc4	film
bez	bez	k7c2	bez
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
do	do	k7c2	do
toho	ten	k3xDgNnSc2	ten
zasahovalo	zasahovat	k5eAaImAgNnS	zasahovat
velké	velký	k2eAgNnSc4d1	velké
studio	studio	k1gNnSc4	studio
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
to	ten	k3xDgNnSc1	ten
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
další	další	k2eAgInSc4d1	další
důležitou	důležitý	k2eAgFnSc4d1	důležitá
změnu	změna	k1gFnSc4	změna
ve	v	k7c6	v
výrobě	výroba	k1gFnSc6	výroba
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
a	a	k8xC	a
totiž	totiž	k9	totiž
<g/>
,	,	kIx,	,
že	že	k8xS	že
nadále	nadále	k6eAd1	nadále
pracovníci	pracovník	k1gMnPc1	pracovník
uzavírali	uzavírat	k5eAaPmAgMnP	uzavírat
se	s	k7c7	s
studii	studie	k1gFnSc3	studie
kontrakty	kontrakt	k1gInPc4	kontrakt
na	na	k7c4	na
konkrétní	konkrétní	k2eAgInPc4d1	konkrétní
filmy	film	k1gInPc4	film
a	a	k8xC	a
různí	různit	k5eAaImIp3nP	různit
pracovníci	pracovník	k1gMnPc1	pracovník
tak	tak	k6eAd1	tak
mohli	moct	k5eAaImAgMnP	moct
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
filmech	film	k1gInPc6	film
pro	pro	k7c4	pro
různá	různý	k2eAgNnPc4d1	různé
filmová	filmový	k2eAgNnPc4d1	filmové
studia	studio	k1gNnPc4	studio
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
řečeno	říct	k5eAaPmNgNnS	říct
výše	vysoce	k6eAd2	vysoce
<g/>
,	,	kIx,	,
americký	americký	k2eAgInSc4d1	americký
film	film	k1gInSc4	film
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
velkou	velký	k2eAgFnSc7d1	velká
měrou	míra	k1gFnSc7wR	míra
ovlivňoval	ovlivňovat	k5eAaImAgInS	ovlivňovat
tzv.	tzv.	kA	tzv.
Haysův	Haysův	k2eAgInSc1d1	Haysův
kodex	kodex	k1gInSc1	kodex
<g/>
.	.	kIx.	.
</s>
<s>
Filmům	film	k1gInPc3	film
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
nedodržovaly	dodržovat	k5eNaImAgFnP	dodržovat
tento	tento	k3xDgInSc4	tento
kodex	kodex	k1gInSc4	kodex
<g/>
,	,	kIx,	,
hrozil	hrozit	k5eAaImAgInS	hrozit
bojkot	bojkot	k1gInSc4	bojkot
a	a	k8xC	a
ty	ten	k3xDgInPc4	ten
filmy	film	k1gInPc4	film
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
neschválila	schválit	k5eNaPmAgFnS	schválit
MPPDA	MPPDA	kA	MPPDA
<g/>
,	,	kIx,	,
musely	muset	k5eAaImAgFnP	muset
zaplatit	zaplatit	k5eAaPmF	zaplatit
pokutu	pokuta	k1gFnSc4	pokuta
25	[number]	k4	25
tisíc	tisíc	k4xCgInPc2	tisíc
dolarů	dolar	k1gInPc2	dolar
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
neprofitovaly	profitovat	k5eNaBmAgFnP	profitovat
v	v	k7c6	v
kinech	kino	k1gNnPc6	kino
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
všechna	všechen	k3xTgNnPc1	všechen
kina	kino	k1gNnPc1	kino
"	"	kIx"	"
<g/>
velké	velká	k1gFnSc2	velká
pětky	pětka	k1gFnSc2	pětka
<g/>
"	"	kIx"	"
vlastnila	vlastnit	k5eAaImAgFnS	vlastnit
právě	právě	k9	právě
MPPDA	MPPDA	kA	MPPDA
<g/>
.	.	kIx.	.
</s>
<s>
Produkční	produkční	k2eAgInSc1d1	produkční
kodex	kodex	k1gInSc1	kodex
byl	být	k5eAaImAgInS	být
uplatňován	uplatňovat	k5eAaImNgInS	uplatňovat
až	až	k6eAd1	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
éry	éra	k1gFnSc2	éra
Klasického	klasický	k2eAgInSc2d1	klasický
Hollywoodu	Hollywood	k1gInSc2	Hollywood
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
nahrazen	nahradit	k5eAaPmNgInS	nahradit
ratingovým	ratingový	k2eAgInSc7d1	ratingový
systémem	systém	k1gInSc7	systém
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
hodnocením	hodnocení	k1gNnSc7	hodnocení
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
s	s	k7c7	s
obměnami	obměna	k1gFnPc7	obměna
udržel	udržet	k5eAaPmAgMnS	udržet
až	až	k9	až
dosud	dosud	k6eAd1	dosud
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdních	pozdní	k2eAgNnPc6d1	pozdní
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
potom	potom	k6eAd1	potom
celkově	celkově	k6eAd1	celkově
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
kolapsu	kolaps	k1gInSc3	kolaps
hollywoodského	hollywoodský	k2eAgInSc2d1	hollywoodský
studiového	studiový	k2eAgInSc2d1	studiový
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
zároveň	zároveň	k6eAd1	zároveň
s	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
televize	televize	k1gFnSc2	televize
<g/>
,	,	kIx,	,
zavedením	zavedení	k1gNnSc7	zavedení
auteurismu	auteurismus	k1gInSc2	auteurismus
mezi	mezi	k7c7	mezi
hollywoodskými	hollywoodský	k2eAgMnPc7d1	hollywoodský
režiséry	režisér	k1gMnPc7	režisér
a	a	k8xC	a
kritiky	kritik	k1gMnPc7	kritik
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
se	s	k7c7	s
zvýšením	zvýšení	k1gNnSc7	zvýšení
vlivu	vliv	k1gInSc2	vliv
zahraničních	zahraniční	k2eAgInPc2d1	zahraniční
filmů	film	k1gInPc2	film
a	a	k8xC	a
nezávislých	závislý	k2eNgMnPc2d1	nezávislý
filmařů	filmař	k1gMnPc2	filmař
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Classical	Classical	k1gFnSc2	Classical
Hollywood	Hollywood	k1gInSc1	Hollywood
cinema	cinem	k1gMnSc2	cinem
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
