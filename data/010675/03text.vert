<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Mengele	Mengel	k1gInSc2	Mengel
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1911	[number]	k4	1911
Günzburg	Günzburg	k1gInSc1	Günzburg
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
–	–	k?	–
7	[number]	k4	7
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1979	[number]	k4	1979
Bertioga	Bertioga	k1gFnSc1	Bertioga
<g/>
,	,	kIx,	,
Brazílie	Brazílie	k1gFnSc1	Brazílie
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
německý	německý	k2eAgMnSc1d1	německý
lékař	lékař	k1gMnSc1	lékař
SS	SS	kA	SS
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
na	na	k7c6	na
vězních	vězeň	k1gMnPc6	vězeň
v	v	k7c6	v
koncentračním	koncentrační	k2eAgInSc6d1	koncentrační
táboře	tábor	k1gInSc6	tábor
Osvětim	Osvětim	k1gFnSc1	Osvětim
prováděl	provádět	k5eAaImAgInS	provádět
morbidní	morbidní	k2eAgMnSc1d1	morbidní
a	a	k8xC	a
sadistické	sadistický	k2eAgInPc1d1	sadistický
experimenty	experiment	k1gInPc1	experiment
<g/>
.	.	kIx.	.
</s>
<s>
Osobně	osobně	k6eAd1	osobně
poslal	poslat	k5eAaPmAgMnS	poslat
přes	přes	k7c4	přes
400	[number]	k4	400
000	[number]	k4	000
vězňů	vězeň	k1gMnPc2	vězeň
do	do	k7c2	do
plynové	plynový	k2eAgFnSc2d1	plynová
komory	komora	k1gFnSc2	komora
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
utekl	utéct	k5eAaPmAgMnS	utéct
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
žil	žít	k5eAaImAgMnS	žít
skrytě	skrytě	k6eAd1	skrytě
za	za	k7c7	za
hranicemi	hranice	k1gFnPc7	hranice
až	až	k8xS	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
infarkt	infarkt	k1gInSc4	infarkt
či	či	k8xC	či
křeč	křeč	k1gFnSc4	křeč
při	při	k7c6	při
plavání	plavání	k1gNnSc6	plavání
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
identifikace	identifikace	k1gFnSc1	identifikace
byla	být	k5eAaImAgFnS	být
provedena	provést	k5eAaPmNgFnS	provést
na	na	k7c6	na
základě	základ	k1gInSc6	základ
testů	test	k1gInPc2	test
DNA	DNA	kA	DNA
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
přezdívka	přezdívka	k1gFnSc1	přezdívka
byla	být	k5eAaImAgFnS	být
Beppo	Beppa	k1gFnSc5	Beppa
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
byl	být	k5eAaImAgInS	být
také	také	k9	také
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k9	jako
Todesengel	Todesengel	k1gInSc1	Todesengel
(	(	kIx(	(
<g/>
Anděl	Anděl	k1gMnSc1	Anděl
smrti	smrt	k1gFnSc2	smrt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Před	před	k7c7	před
válkou	válka	k1gFnSc7	válka
a	a	k8xC	a
během	během	k7c2	během
války	válka	k1gFnSc2	válka
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Günzburgu	Günzburg	k1gInSc6	Günzburg
v	v	k7c6	v
Bavorsku	Bavorsko	k1gNnSc6	Bavorsko
jako	jako	k8xS	jako
nejstarší	starý	k2eAgMnSc1d3	nejstarší
ze	z	k7c2	z
tří	tři	k4xCgMnPc2	tři
synů	syn	k1gMnPc2	syn
Karla	Karel	k1gMnSc2	Karel
Mengeleho	Mengele	k1gMnSc2	Mengele
(	(	kIx(	(
<g/>
1881	[number]	k4	1881
<g/>
–	–	k?	–
<g/>
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
úspěšného	úspěšný	k2eAgMnSc4d1	úspěšný
továrníka	továrník	k1gMnSc4	továrník
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
ženy	žena	k1gFnPc1	žena
Wilmy	Wilma	k1gFnSc2	Wilma
(	(	kIx(	(
<g/>
zemřela	zemřít	k5eAaPmAgFnS	zemřít
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgInS	mít
dva	dva	k4xCgMnPc4	dva
mladší	mladý	k2eAgMnPc4d2	mladší
bratry	bratr	k1gMnPc4	bratr
Karla	Karel	k1gMnSc4	Karel
(	(	kIx(	(
<g/>
1912	[number]	k4	1912
<g/>
–	–	k?	–
<g/>
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
a	a	k8xC	a
Aloise	Alois	k1gMnSc4	Alois
(	(	kIx(	(
<g/>
1914	[number]	k4	1914
<g/>
-	-	kIx~	-
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1926	[number]	k4	1926
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
diagnostikována	diagnostikován	k2eAgFnSc1d1	diagnostikována
osteomyelitida	osteomyelitida	k1gFnSc1	osteomyelitida
<g/>
,	,	kIx,	,
bakteriální	bakteriální	k2eAgFnSc1d1	bakteriální
infekce	infekce	k1gFnSc1	infekce
kosti	kost	k1gFnSc2	kost
a	a	k8xC	a
kostní	kostní	k2eAgFnSc2d1	kostní
dřeně	dřeň	k1gFnSc2	dřeň
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
zánět	zánět	k1gInSc4	zánět
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
ke	k	k7c3	k
snížení	snížení	k1gNnSc3	snížení
prokrvování	prokrvování	k1gNnSc2	prokrvování
kosti	kost	k1gFnSc2	kost
<g/>
.	.	kIx.	.
</s>
<s>
Studoval	studovat	k5eAaImAgInS	studovat
medicínu	medicína	k1gFnSc4	medicína
a	a	k8xC	a
antropologii	antropologie	k1gFnSc4	antropologie
na	na	k7c6	na
Mnichovské	mnichovský	k2eAgFnSc6d1	Mnichovská
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
,	,	kIx,	,
Vídeňské	vídeňský	k2eAgFnSc3d1	Vídeňská
universitě	universita	k1gFnSc3	universita
a	a	k8xC	a
Bonnské	bonnský	k2eAgFnSc3d1	Bonnská
universitě	universita	k1gFnSc3	universita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
získal	získat	k5eAaPmAgMnS	získat
titul	titul	k1gInSc4	titul
doktor	doktor	k1gMnSc1	doktor
filozofie	filozofie	k1gFnSc2	filozofie
za	za	k7c4	za
diplomovou	diplomový	k2eAgFnSc4d1	Diplomová
práci	práce	k1gFnSc4	práce
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1935	[number]	k4	1935
o	o	k7c6	o
rasových	rasový	k2eAgInPc6d1	rasový
rozdílech	rozdíl	k1gInPc6	rozdíl
ve	v	k7c6	v
struktuře	struktura	k1gFnSc6	struktura
spodní	spodní	k2eAgFnSc2d1	spodní
dásně	dáseň	k1gFnSc2	dáseň
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
dohledem	dohled	k1gInSc7	dohled
profesora	profesor	k1gMnSc2	profesor
Theodora	Theodor	k1gMnSc2	Theodor
Mollisona	Mollison	k1gMnSc2	Mollison
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
svých	svůj	k3xOyFgFnPc6	svůj
zkouškách	zkouška	k1gFnPc6	zkouška
odešel	odejít	k5eAaPmAgInS	odejít
do	do	k7c2	do
Frankfurtu	Frankfurt	k1gInSc2	Frankfurt
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
asistent	asistent	k1gMnSc1	asistent
Otmara	Otmar	k1gMnSc2	Otmar
von	von	k1gInSc4	von
Verschuera	Verschuero	k1gNnSc2	Verschuero
na	na	k7c6	na
Frankfurtstkém	Frankfurtstký	k2eAgInSc6d1	Frankfurtstký
univerzitním	univerzitní	k2eAgInSc6d1	univerzitní
institutu	institut	k1gInSc6	institut
dědičné	dědičný	k2eAgFnSc2d1	dědičná
biologie	biologie	k1gFnSc2	biologie
a	a	k8xC	a
rasové	rasový	k2eAgFnSc2d1	rasová
hygieny	hygiena	k1gFnSc2	hygiena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
získal	získat	k5eAaPmAgMnS	získat
titul	titul	k1gInSc4	titul
doktor	doktor	k1gMnSc1	doktor
medicíny	medicína	k1gFnSc2	medicína
za	za	k7c4	za
diplomovou	diplomový	k2eAgFnSc4d1	Diplomová
práci	práce	k1gFnSc4	práce
nazvanou	nazvaný	k2eAgFnSc4d1	nazvaná
"	"	kIx"	"
<g/>
Rodinný	rodinný	k2eAgInSc1d1	rodinný
výzkum	výzkum	k1gInSc1	výzkum
rozštěpu	rozštěp	k1gInSc2	rozštěp
horního	horní	k2eAgInSc2d1	horní
rtu	ret	k1gInSc2	ret
<g/>
,	,	kIx,	,
horního	horní	k2eAgNnSc2d1	horní
patra	patro	k1gNnSc2	patro
a	a	k8xC	a
dásně	dáseň	k1gFnSc2	dáseň
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1932	[number]	k4	1932
<g/>
,	,	kIx,	,
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
21	[number]	k4	21
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
do	do	k7c2	do
Stahlhelm	Stahlhelma	k1gFnPc2	Stahlhelma
<g/>
,	,	kIx,	,
Bund	bunda	k1gFnPc2	bunda
der	drát	k5eAaImRp2nS	drát
Frontsoldaten	Frontsoldatno	k1gNnPc2	Frontsoldatno
(	(	kIx(	(
<g/>
Ocelová	ocelový	k2eAgFnSc1d1	ocelová
helma	helma	k1gFnSc1	helma
<g/>
,	,	kIx,	,
Svaz	svaz	k1gInSc1	svaz
frontových	frontový	k2eAgMnPc2d1	frontový
vojáků	voják	k1gMnPc2	voják
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
organizace	organizace	k1gFnSc1	organizace
byla	být	k5eAaImAgFnS	být
včleněna	včlenit	k5eAaPmNgFnS	včlenit
do	do	k7c2	do
SA	SA	kA	SA
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
zdravotním	zdravotní	k2eAgFnPc3d1	zdravotní
potížím	potíž	k1gFnPc3	potíž
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
členství	členství	k1gNnSc6	členství
v	v	k7c6	v
Nacistickém	nacistický	k2eAgInSc6d1	nacistický
spolku	spolek	k1gInSc6	spolek
zažádal	zažádat	k5eAaPmAgInS	zažádat
roku	rok	k1gInSc2	rok
1937	[number]	k4	1937
a	a	k8xC	a
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
SS	SS	kA	SS
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Irenou	Irena	k1gFnSc7	Irena
Schoenbeinovou	Schoenbeinová	k1gFnSc7	Schoenbeinová
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
sloužil	sloužit	k5eAaImAgMnS	sloužit
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
u	u	k7c2	u
speciálně	speciálně	k6eAd1	speciálně
cvičeného	cvičený	k2eAgInSc2d1	cvičený
pluku	pluk	k1gInSc2	pluk
Gebirgsjäger	Gebirgsjägra	k1gFnPc2	Gebirgsjägra
(	(	kIx(	(
<g/>
Horských	Horských	k2eAgFnPc2d1	Horských
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
byl	být	k5eAaImAgInS	být
přeložen	přeložit	k5eAaPmNgInS	přeložit
do	do	k7c2	do
záložního	záložní	k2eAgInSc2d1	záložní
doktorského	doktorský	k2eAgInSc2d1	doktorský
sboru	sbor	k1gInSc2	sbor
a	a	k8xC	a
poté	poté	k6eAd1	poté
sloužil	sloužit	k5eAaImAgMnS	sloužit
v	v	k7c6	v
jednotce	jednotka	k1gFnSc6	jednotka
Waffen-SS	Waffen-SS	k1gFnSc2	Waffen-SS
<g/>
,	,	kIx,	,
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
divize	divize	k1gFnSc2	divize
SS	SS	kA	SS
s	s	k7c7	s
krycím	krycí	k2eAgInSc7d1	krycí
názvem	název	k1gInSc7	název
Wiking	Wiking	k1gInSc1	Wiking
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
byl	být	k5eAaImAgMnS	být
zraněn	zranit	k5eAaPmNgMnS	zranit
na	na	k7c6	na
ruské	ruský	k2eAgFnSc6d1	ruská
frontě	fronta	k1gFnSc6	fronta
<g/>
,	,	kIx,	,
prohlášen	prohlásit	k5eAaPmNgMnS	prohlásit
za	za	k7c2	za
neschopného	schopný	k2eNgInSc2d1	neschopný
boje	boj	k1gInSc2	boj
a	a	k8xC	a
povýšen	povýšit	k5eAaPmNgMnS	povýšit
na	na	k7c4	na
hauptsturmführera	hauptsturmführer	k1gMnSc4	hauptsturmführer
(	(	kIx(	(
<g/>
kapitána	kapitán	k1gMnSc4	kapitán
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Získal	získat	k5eAaPmAgInS	získat
také	také	k9	také
Železný	železný	k2eAgInSc1d1	železný
kříž	kříž	k1gInSc1	kříž
prvního	první	k4xOgInSc2	první
řádu	řád	k1gInSc2	řád
a	a	k8xC	a
Železný	železný	k2eAgInSc1d1	železný
kříž	kříž	k1gInSc1	kříž
druhého	druhý	k4xOgInSc2	druhý
řádu	řád	k1gInSc2	řád
za	za	k7c4	za
statečnost	statečnost	k1gFnSc4	statečnost
v	v	k7c6	v
boji	boj	k1gInSc6	boj
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
Železný	železný	k2eAgInSc1d1	železný
kříž	kříž	k1gInSc1	kříž
první	první	k4xOgFnSc2	první
třídy	třída	k1gFnSc2	třída
byl	být	k5eAaImAgInS	být
udělen	udělit	k5eAaPmNgInS	udělit
za	za	k7c4	za
SS-OStuf	SS-OStuf	k1gInSc4	SS-OStuf
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
nepřátelskou	přátelský	k2eNgFnSc7d1	nepřátelská
palbou	palba	k1gFnSc7	palba
vytáhl	vytáhnout	k5eAaPmAgMnS	vytáhnout
dva	dva	k4xCgInPc4	dva
členy	člen	k1gInPc4	člen
posádky	posádka	k1gFnSc2	posádka
z	z	k7c2	z
hořícího	hořící	k2eAgInSc2d1	hořící
tanku	tank	k1gInSc2	tank
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
zachránil	zachránit	k5eAaPmAgMnS	zachránit
jejich	jejich	k3xOp3gInPc4	jejich
životy	život	k1gInPc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gNnPc4	jeho
další	další	k2eAgNnPc4d1	další
vojenská	vojenský	k2eAgNnPc4d1	vojenské
ocenění	ocenění	k1gNnPc4	ocenění
patří	patřit	k5eAaImIp3nS	patřit
Medaile	medaile	k1gFnSc1	medaile
za	za	k7c4	za
zranění	zranění	k1gNnPc4	zranění
a	a	k8xC	a
Medaile	medaile	k1gFnPc4	medaile
za	za	k7c4	za
péči	péče	k1gFnSc4	péče
o	o	k7c4	o
německý	německý	k2eAgInSc4d1	německý
národ	národ	k1gInSc4	národ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osvětim	Osvětim	k1gFnSc4	Osvětim
==	==	k?	==
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gNnSc7	jeho
dalším	další	k2eAgNnSc7d1	další
působištěm	působiště	k1gNnSc7	působiště
byla	být	k5eAaImAgFnS	být
Osvětim	Osvětim	k1gFnSc4	Osvětim
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vystřídal	vystřídat	k5eAaPmAgMnS	vystřídat
jiného	jiný	k2eAgMnSc4d1	jiný
doktora	doktor	k1gMnSc4	doktor
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
onemocněl	onemocnět	k5eAaPmAgMnS	onemocnět
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
24	[number]	k4	24
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1943	[number]	k4	1943
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
vedoucím	vedoucí	k2eAgMnSc7d1	vedoucí
doktorem	doktor	k1gMnSc7	doktor
v	v	k7c6	v
úseku	úsek	k1gInSc6	úsek
BIIe	BII	k1gInSc2	BII
(	(	kIx(	(
<g/>
cikánský	cikánský	k2eAgInSc1d1	cikánský
tábor	tábor	k1gInSc1	tábor
<g/>
)	)	kIx)	)
koncentračního	koncentrační	k2eAgInSc2d1	koncentrační
tábora	tábor	k1gInSc2	tábor
Auschwitz-Birkenau	Auschwitz-Birkenaus	k1gInSc2	Auschwitz-Birkenaus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1944	[number]	k4	1944
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
táborový	táborový	k2eAgInSc1d1	táborový
úsek	úsek	k1gInSc1	úsek
zlikvidován	zlikvidován	k2eAgInSc1d1	zlikvidován
a	a	k8xC	a
všichni	všechen	k3xTgMnPc1	všechen
zajatci	zajatec	k1gMnPc1	zajatec
zplynováni	zplynovat	k5eAaPmNgMnP	zplynovat
<g/>
.	.	kIx.	.
</s>
<s>
Následovně	následovně	k6eAd1	následovně
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
nejvyšším	vysoký	k2eAgMnSc7d3	nejvyšší
lékařem	lékař	k1gMnSc7	lékař
hlavního	hlavní	k2eAgInSc2d1	hlavní
tábora	tábor	k1gInSc2	tábor
v	v	k7c6	v
Birkenau	Birkenaus	k1gInSc6	Birkenaus
<g/>
.	.	kIx.	.
</s>
<s>
Nebyl	být	k5eNaImAgInS	být
nejvyšším	vysoký	k2eAgMnSc7d3	nejvyšší
lékařem	lékař	k1gMnSc7	lékař
Auschwitzu	Auschwitz	k1gInSc2	Auschwitz
<g/>
,	,	kIx,	,
nadřízeným	nadřízený	k1gMnPc3	nadřízený
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgInS	být
SS-Standortarzt	SS-Standortarzt	k1gInSc1	SS-Standortarzt
(	(	kIx(	(
<g/>
pevnostní	pevnostní	k2eAgMnSc1d1	pevnostní
lékař	lékař	k1gMnSc1	lékař
<g/>
)	)	kIx)	)
Eduard	Eduard	k1gMnSc1	Eduard
Wirths	Wirthsa	k1gFnPc2	Wirthsa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Díky	díky	k7c3	díky
jednadvaceti	jednadvacet	k4xCc3	jednadvacet
měsícům	měsíc	k1gInPc3	měsíc
pobytu	pobyt	k1gInSc3	pobyt
v	v	k7c6	v
Auschwitzu	Auschwitz	k1gInSc6	Auschwitz
byl	být	k5eAaImAgInS	být
později	pozdě	k6eAd2	pozdě
znám	znám	k2eAgInSc1d1	znám
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Anděl	Anděl	k1gMnSc1	Anděl
smrti	smrt	k1gFnSc2	smrt
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Obyčejně	obyčejně	k6eAd1	obyčejně
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
lékařské	lékařský	k2eAgFnSc6d1	lékařská
delegaci	delegace	k1gFnSc6	delegace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
kontrolovala	kontrolovat	k5eAaImAgFnS	kontrolovat
přicházející	přicházející	k2eAgFnSc1d1	přicházející
vězně	vězně	k6eAd1	vězně
a	a	k8xC	a
vyšetřovala	vyšetřovat	k5eAaImAgFnS	vyšetřovat
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jsou	být	k5eAaImIp3nP	být
vhodní	vhodný	k2eAgMnPc1d1	vhodný
pro	pro	k7c4	pro
práci	práce	k1gFnSc4	práce
a	a	k8xC	a
experimenty	experiment	k1gInPc1	experiment
a	a	k8xC	a
kteří	který	k3yQgMnPc1	který
budou	být	k5eAaImBp3nP	být
okamžitě	okamžitě	k6eAd1	okamžitě
posláni	poslat	k5eAaPmNgMnP	poslat
do	do	k7c2	do
plynové	plynový	k2eAgFnSc2d1	plynová
komory	komora	k1gFnSc2	komora
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dostal	dostat	k5eAaPmAgMnS	dostat
příkaz	příkaz	k1gInSc4	příkaz
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgInS	zabývat
možností	možnost	k1gFnSc7	možnost
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
uměle	uměle	k6eAd1	uměle
zvýšit	zvýšit	k5eAaPmF	zvýšit
pravděpodobnost	pravděpodobnost	k1gFnSc4	pravděpodobnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
žena	žena	k1gFnSc1	žena
porodí	porodit	k5eAaPmIp3nP	porodit
dvojčata	dvojče	k1gNnPc1	dvojče
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
vícerčata	vícerče	k1gNnPc4	vícerče
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
na	na	k7c6	na
začátku	začátek	k1gInSc2	začátek
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
bylo	být	k5eAaImAgNnS	být
totiž	totiž	k9	totiž
Hitlerovi	Hitler	k1gMnSc3	Hitler
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc3	jeho
poradcům	poradce	k1gMnPc3	poradce
jasné	jasný	k2eAgNnSc4d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
německá	německý	k2eAgFnSc1d1	německá
žena	žena	k1gFnSc1	žena
je	být	k5eAaImIp3nS	být
těhotná	těhotný	k2eAgFnSc1d1	těhotná
devět	devět	k4xCc1	devět
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
brzy	brzy	k6eAd1	brzy
nebude	být	k5eNaImBp3nS	být
dostatek	dostatek	k1gInSc1	dostatek
Němců	Němec	k1gMnPc2	Němec
pro	pro	k7c4	pro
ovládnutí	ovládnutí	k1gNnSc4	ovládnutí
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1943	[number]	k4	1943
byla	být	k5eAaImAgNnP	být
dvojčata	dvojče	k1gNnPc1	dvojče
vybírána	vybírán	k2eAgNnPc1d1	vybíráno
a	a	k8xC	a
umisťována	umisťován	k2eAgFnSc1d1	umisťován
do	do	k7c2	do
speciálních	speciální	k2eAgFnPc2d1	speciální
budov	budova	k1gFnPc2	budova
s	s	k7c7	s
mírnějším	mírný	k2eAgInSc7d2	mírnější
režimem	režim	k1gInSc7	režim
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
dětí	dítě	k1gFnPc2	dítě
vybraných	vybraný	k2eAgFnPc2d1	vybraná
na	na	k7c4	na
experimenty	experiment	k1gInPc4	experiment
byli	být	k5eAaImAgMnP	být
Židé	Žid	k1gMnPc1	Žid
a	a	k8xC	a
Romové	Rom	k1gMnPc1	Rom
držení	držení	k1gNnSc2	držení
v	v	k7c6	v
Auschwitzu	Auschwitz	k1gInSc6	Auschwitz
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
1500	[number]	k4	1500
případů	případ	k1gInPc2	případ
přežilo	přežít	k5eAaPmAgNnS	přežít
kolem	kolem	k7c2	kolem
100	[number]	k4	100
.	.	kIx.	.
</s>
<s>
Nejméně	málo	k6eAd3	málo
jednou	jednou	k6eAd1	jednou
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
uměle	uměle	k6eAd1	uměle
spojit	spojit	k5eAaPmF	spojit
dvojčata	dvojče	k1gNnPc4	dvojče
sešitím	sešití	k1gNnSc7	sešití
jejich	jejich	k3xOp3gFnPc2	jejich
tepen	tepna	k1gFnPc2	tepna
dohromady	dohromady	k6eAd1	dohromady
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
pokus	pokus	k1gInSc1	pokus
se	se	k3xPyFc4	se
nezdařil	zdařit	k5eNaPmAgInS	zdařit
a	a	k8xC	a
způsobil	způsobit	k5eAaPmAgInS	způsobit
vážné	vážný	k2eAgNnSc4d1	vážné
zanícení	zanícení	k1gNnSc4	zanícení
rukou	ruka	k1gFnPc2	ruka
obou	dva	k4xCgFnPc2	dva
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gInPc4	jeho
další	další	k2eAgInPc4d1	další
údajné	údajný	k2eAgInPc4d1	údajný
experimenty	experiment	k1gInPc4	experiment
patřilo	patřit	k5eAaImAgNnS	patřit
ponořování	ponořování	k1gNnSc1	ponořování
lidí	člověk	k1gMnPc2	člověk
do	do	k7c2	do
kotlů	kotel	k1gInPc2	kotel
s	s	k7c7	s
vařící	vařící	k2eAgFnSc7d1	vařící
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
viděl	vidět	k5eAaImAgMnS	vidět
jak	jak	k6eAd1	jak
velkou	velký	k2eAgFnSc4d1	velká
teplotu	teplota	k1gFnSc4	teplota
může	moct	k5eAaImIp3nS	moct
lidské	lidský	k2eAgNnSc1d1	lidské
tělo	tělo	k1gNnSc4	tělo
vydržet	vydržet	k5eAaPmF	vydržet
před	před	k7c7	před
smrtí	smrt	k1gFnSc7	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Údajně	údajně	k6eAd1	údajně
také	také	k9	také
zkoušel	zkoušet	k5eAaImAgMnS	zkoušet
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
dlouho	dlouho	k6eAd1	dlouho
přežije	přežít	k5eAaPmIp3nS	přežít
novorozeně	novorozeně	k6eAd1	novorozeně
hned	hned	k6eAd1	hned
po	po	k7c6	po
porodu	porod	k1gInSc6	porod
bez	bez	k7c2	bez
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Vězni	vězeň	k1gMnPc1	vězeň
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
experimenty	experiment	k1gInPc1	experiment
přežili	přežít	k5eAaPmAgMnP	přežít
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
krátce	krátce	k6eAd1	krátce
potom	potom	k8xC	potom
zavražděni	zavraždit	k5eAaPmNgMnP	zavraždit
kvůli	kvůli	k7c3	kvůli
pitvě	pitva	k1gFnSc3	pitva
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
byli	být	k5eAaImAgMnP	být
pitváni	pitvat	k5eAaImNgMnP	pitvat
zaživa	zaživa	k6eAd1	zaživa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zajímal	zajímat	k5eAaImAgInS	zajímat
se	se	k3xPyFc4	se
také	také	k9	také
o	o	k7c4	o
trpaslíky	trpaslík	k1gMnPc4	trpaslík
<g/>
.	.	kIx.	.
</s>
<s>
Nalezl	nalézt	k5eAaBmAgInS	nalézt
liliputánskou	liliputánský	k2eAgFnSc4d1	liliputánský
hereckou	herecký	k2eAgFnSc4d1	herecká
společnost	společnost	k1gFnSc4	společnost
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
7	[number]	k4	7
z	z	k7c2	z
10	[number]	k4	10
členů	člen	k1gMnPc2	člen
byli	být	k5eAaImAgMnP	být
trpaslíci	trpaslík	k1gMnPc1	trpaslík
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
nazýval	nazývat	k5eAaImAgMnS	nazývat
"	"	kIx"	"
<g/>
Svojí	svojit	k5eAaImIp3nS	svojit
trpasličí	trpasličí	k2eAgFnSc7d1	trpasličí
rodinou	rodina	k1gFnSc7	rodina
<g/>
"	"	kIx"	"
a	a	k8xC	a
často	často	k6eAd1	často
na	na	k7c6	na
nich	on	k3xPp3gFnPc6	on
prováděl	provádět	k5eAaImAgInS	provádět
své	svůj	k3xOyFgInPc4	svůj
experimenty	experiment	k1gInPc4	experiment
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
fascinován	fascinován	k2eAgMnSc1d1	fascinován
jejich	jejich	k3xOp3gFnSc7	jejich
strukturou	struktura	k1gFnSc7	struktura
<g/>
,	,	kIx,	,
menšími	malý	k2eAgFnPc7d2	menší
končetinami	končetina	k1gFnPc7	končetina
a	a	k8xC	a
normálním	normální	k2eAgInSc7d1	normální
trupem	trup	k1gInSc7	trup
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Osvětimský	osvětimský	k2eAgMnSc1d1	osvětimský
vězeň	vězeň	k1gMnSc1	vězeň
Alex	Alex	k1gMnSc1	Alex
Dekel	Dekel	k1gMnSc1	Dekel
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nikdy	nikdy	k6eAd1	nikdy
jsem	být	k5eAaImIp1nS	být
nepřijal	přijmout	k5eNaPmAgMnS	přijmout
fakt	fakt	k1gInSc4	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
sám	sám	k3xTgMnSc1	sám
Mengele	Mengela	k1gFnSc3	Mengela
věřil	věřit	k5eAaImAgMnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
dělal	dělat	k5eAaImAgMnS	dělat
skutečný	skutečný	k2eAgInSc4d1	skutečný
výzkum	výzkum	k1gInSc4	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
Ne	ne	k9	ne
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
nedbale	dbale	k6eNd1	dbale
svůj	svůj	k3xOyFgInSc4	svůj
výzkum	výzkum	k1gInSc4	výzkum
prováděl	provádět	k5eAaImAgMnS	provádět
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
si	se	k3xPyFc3	se
jen	jen	k9	jen
vychutnával	vychutnávat	k5eAaImAgMnS	vychutnávat
moc	moc	k6eAd1	moc
<g/>
.	.	kIx.	.
</s>
<s>
Mengele	Mengele	k6eAd1	Mengele
provozoval	provozovat	k5eAaImAgMnS	provozovat
jatka	jatka	k1gFnSc1	jatka
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgFnPc1d2	veliký
operace	operace	k1gFnPc1	operace
byly	být	k5eAaImAgFnP	být
prováděny	provádět	k5eAaImNgFnP	provádět
bez	bez	k7c2	bez
umrtvení	umrtvení	k1gNnSc2	umrtvení
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
jsem	být	k5eAaImIp1nS	být
byl	být	k5eAaImAgMnS	být
svědkem	svědek	k1gMnSc7	svědek
operace	operace	k1gFnSc2	operace
žaludku	žaludek	k1gInSc2	žaludek
<g/>
,	,	kIx,	,
Mengele	Mengel	k1gInSc2	Mengel
odstraňoval	odstraňovat	k5eAaImAgInS	odstraňovat
kusy	kus	k1gInPc4	kus
žaludku	žaludek	k1gInSc2	žaludek
bez	bez	k7c2	bez
umrtvení	umrtvení	k1gNnSc2	umrtvení
<g/>
.	.	kIx.	.
</s>
<s>
Jindy	jindy	k6eAd1	jindy
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
srdce	srdce	k1gNnSc4	srdce
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
odstraněno	odstranit	k5eAaPmNgNnS	odstranit
<g/>
,	,	kIx,	,
opět	opět	k6eAd1	opět
bez	bez	k7c2	bez
umrtvení	umrtvení	k1gNnSc2	umrtvení
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
strašné	strašný	k2eAgNnSc1d1	strašné
<g/>
.	.	kIx.	.
</s>
<s>
Mengele	Mengele	k6eAd1	Mengele
byl	být	k5eAaImAgMnS	být
doktor	doktor	k1gMnSc1	doktor
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
zbláznil	zbláznit	k5eAaPmAgMnS	zbláznit
kvůli	kvůli	k7c3	kvůli
moci	moc	k1gFnSc3	moc
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
dostal	dostat	k5eAaPmAgMnS	dostat
<g/>
.	.	kIx.	.
</s>
<s>
Nikdo	nikdo	k3yNnSc1	nikdo
se	se	k3xPyFc4	se
ho	on	k3xPp3gNnSc2	on
nikdy	nikdy	k6eAd1	nikdy
nezeptal	zeptat	k5eNaPmAgMnS	zeptat
proč	proč	k6eAd1	proč
tento	tento	k3xDgMnSc1	tento
pacient	pacient	k1gMnSc1	pacient
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
?	?	kIx.	?
</s>
<s>
A	a	k9	a
proč	proč	k6eAd1	proč
tento	tento	k3xDgInSc4	tento
<g/>
?	?	kIx.	?
</s>
<s>
On	on	k3xPp3gMnSc1	on
nemocné	nemocný	k2eAgNnSc4d1	nemocný
nepočítal	počítat	k5eNaImAgMnS	počítat
<g/>
.	.	kIx.	.
</s>
<s>
Tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
dělá	dělat	k5eAaImIp3nS	dělat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
dělá	dělat	k5eAaImIp3nS	dělat
<g/>
,	,	kIx,	,
ve	v	k7c6	v
jménu	jméno	k1gNnSc6	jméno
vědy	věda	k1gFnSc2	věda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
šílenost	šílenost	k1gFnSc1	šílenost
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
==	==	k?	==
</s>
</p>
<p>
<s>
Auschwitz	Auschwitz	k1gMnSc1	Auschwitz
opustil	opustit	k5eAaPmAgMnS	opustit
a	a	k8xC	a
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
koncentračního	koncentrační	k2eAgInSc2d1	koncentrační
tábora	tábor	k1gInSc2	tábor
Gross-Rosen	Gross-Rosen	k2eAgInSc1d1	Gross-Rosen
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
prováděl	provádět	k5eAaImAgMnS	provádět
pokusy	pokus	k1gInPc4	pokus
na	na	k7c6	na
zajatých	zajatý	k2eAgMnPc6d1	zajatý
sovětských	sovětský	k2eAgMnPc6d1	sovětský
vojácích	voják	k1gMnPc6	voják
<g/>
.	.	kIx.	.
</s>
<s>
Snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
vyvinout	vyvinout	k5eAaPmF	vyvinout
vakcínu	vakcína	k1gFnSc4	vakcína
proti	proti	k7c3	proti
nemocem	nemoc	k1gFnPc3	nemoc
jako	jako	k8xC	jako
byl	být	k5eAaImAgMnS	být
například	například	k6eAd1	například
skvrnitý	skvrnitý	k2eAgInSc4d1	skvrnitý
tyfus	tyfus	k1gInSc4	tyfus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1945	[number]	k4	1945
uprchl	uprchnout	k5eAaPmAgInS	uprchnout
na	na	k7c4	na
západ	západ	k1gInSc4	západ
Evropy	Evropa	k1gFnSc2	Evropa
jako	jako	k8xC	jako
řadový	řadový	k2eAgMnSc1d1	řadový
německý	německý	k2eAgMnSc1d1	německý
voják	voják	k1gMnSc1	voják
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
zadržen	zadržet	k5eAaPmNgMnS	zadržet
jako	jako	k8xC	jako
válečný	válečný	k2eAgMnSc1d1	válečný
zajatec	zajatec	k1gMnSc1	zajatec
a	a	k8xC	a
držen	držet	k5eAaImNgMnS	držet
nedaleko	nedaleko	k7c2	nedaleko
Norimberku	Norimberk	k1gInSc2	Norimberk
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
zajateckém	zajatecký	k2eAgInSc6d1	zajatecký
táboře	tábor	k1gInSc6	tábor
zapsán	zapsán	k2eAgMnSc1d1	zapsán
pod	pod	k7c7	pod
svým	svůj	k3xOyFgNnSc7	svůj
jménem	jméno	k1gNnSc7	jméno
<g/>
,	,	kIx,	,
spojenci	spojenec	k1gMnPc1	spojenec
vůbec	vůbec	k9	vůbec
netušili	tušit	k5eNaImAgMnP	tušit
o	o	k7c4	o
koho	kdo	k3yQnSc4	kdo
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
a	a	k8xC	a
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgInS	být
propuštěn	propuštěn	k2eAgInSc1d1	propuštěn
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
mu	on	k3xPp3gNnSc3	on
velkou	velký	k2eAgFnSc7d1	velká
měrou	míra	k1gFnSc7wR	míra
pomohl	pomoct	k5eAaPmAgInS	pomoct
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
nenechal	nechat	k5eNaPmAgMnS	nechat
pod	pod	k7c7	pod
paží	paže	k1gFnSc7	paže
vytetovat	vytetovat	k5eAaPmF	vytetovat
svoji	svůj	k3xOyFgFnSc4	svůj
krevní	krevní	k2eAgFnSc4d1	krevní
skupinu	skupina	k1gFnSc4	skupina
(	(	kIx(	(
<g/>
což	což	k3yRnSc1	což
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
příslušníky	příslušník	k1gMnPc4	příslušník
SS	SS	kA	SS
povinnost	povinnost	k1gFnSc1	povinnost
<g/>
;	;	kIx,	;
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
pak	pak	k6eAd1	pak
bylo	být	k5eAaImAgNnS	být
díky	díky	k7c3	díky
tomuto	tento	k3xDgInSc3	tento
možno	možno	k6eAd1	možno
příslušníky	příslušník	k1gMnPc7	příslušník
SS	SS	kA	SS
snadno	snadno	k6eAd1	snadno
odhalit	odhalit	k5eAaPmF	odhalit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
táboře	tábor	k1gInSc6	tábor
pomohl	pomoct	k5eAaPmAgMnS	pomoct
Mengelemu	Mengelema	k1gFnSc4	Mengelema
získat	získat	k5eAaPmF	získat
novou	nový	k2eAgFnSc4d1	nová
totožnost	totožnost	k1gFnSc4	totožnost
doktor	doktor	k1gMnSc1	doktor
Fritz	Fritz	k1gMnSc1	Fritz
Ulmann	Ulmann	k1gMnSc1	Ulmann
<g/>
,	,	kIx,	,
dal	dát	k5eAaPmAgMnS	dát
mu	on	k3xPp3gMnSc3	on
osobní	osobní	k2eAgInPc1d1	osobní
doklady	doklad	k1gInPc1	doklad
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
jméno	jméno	k1gNnSc4	jméno
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
posléze	posléze	k6eAd1	posléze
padělané	padělaný	k2eAgFnPc1d1	padělaná
na	na	k7c4	na
jméno	jméno	k1gNnSc4	jméno
Fritz	Fritz	k1gMnSc1	Fritz
Hollmann	Hollmann	k1gMnSc1	Hollmann
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
opuštění	opuštění	k1gNnSc6	opuštění
tábora	tábor	k1gInSc2	tábor
se	se	k3xPyFc4	se
zkontaktoval	zkontaktovat	k5eAaPmAgMnS	zkontaktovat
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
známým	známý	k2eAgMnSc7d1	známý
lékárníkem	lékárník	k1gMnSc7	lékárník
Millerem	Miller	k1gMnSc7	Miller
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
spojkou	spojka	k1gFnSc7	spojka
mezi	mezi	k7c7	mezi
ním	on	k3xPp3gMnSc7	on
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc7	jeho
rodinou	rodina	k1gFnSc7	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
skrýval	skrývat	k5eAaImAgMnS	skrývat
jako	jako	k9	jako
nádeník	nádeník	k1gMnSc1	nádeník
v	v	k7c6	v
Rosenheimu	Rosenheim	k1gInSc6	Rosenheim
v	v	k7c6	v
Horním	horní	k2eAgNnSc6d1	horní
Bavorsku	Bavorsko	k1gNnSc6	Bavorsko
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
rodina	rodina	k1gFnSc1	rodina
byla	být	k5eAaImAgFnS	být
přesvědčena	přesvědčit	k5eAaPmNgFnS	přesvědčit
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
nevině	nevina	k1gFnSc6	nevina
a	a	k8xC	a
často	často	k6eAd1	často
jej	on	k3xPp3gNnSc4	on
navštěvovala	navštěvovat	k5eAaImAgFnS	navštěvovat
<g/>
.	.	kIx.	.
</s>
<s>
Rodinní	rodinní	k2eAgMnPc1d1	rodinní
příslušníci	příslušník	k1gMnPc1	příslušník
mu	on	k3xPp3gInSc3	on
také	také	k9	také
pomohli	pomoct	k5eAaPmAgMnP	pomoct
sehnat	sehnat	k5eAaPmF	sehnat
falešné	falešný	k2eAgInPc4d1	falešný
cestovní	cestovní	k2eAgInPc4d1	cestovní
doklady	doklad	k1gInPc4	doklad
na	na	k7c4	na
jméno	jméno	k1gNnSc4	jméno
Ludwig	Ludwig	k1gMnSc1	Ludwig
Gregor	Gregor	k1gMnSc1	Gregor
a	a	k8xC	a
lístek	lístek	k1gInSc1	lístek
do	do	k7c2	do
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
20	[number]	k4	20
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1949	[number]	k4	1949
vyplul	vyplout	k5eAaPmAgMnS	vyplout
na	na	k7c6	na
lodi	loď	k1gFnSc6	loď
North	North	k1gMnSc1	North
King	King	k1gMnSc1	King
z	z	k7c2	z
Janova	Janov	k1gInSc2	Janov
do	do	k7c2	do
Argentiny	Argentina	k1gFnSc2	Argentina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nalezlo	nalézt	k5eAaBmAgNnS	nalézt
útočiště	útočiště	k1gNnSc1	útočiště
mnoho	mnoho	k6eAd1	mnoho
dalších	další	k2eAgMnPc2d1	další
nacistických	nacistický	k2eAgMnPc2d1	nacistický
důstojníků	důstojník	k1gMnPc2	důstojník
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
ženou	žena	k1gFnSc7	žena
Irenou	Irena	k1gFnSc7	Irena
se	se	k3xPyFc4	se
rozvedl	rozvést	k5eAaPmAgMnS	rozvést
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
si	se	k3xPyFc3	se
vzal	vzít	k5eAaPmAgMnS	vzít
vdovu	vdova	k1gFnSc4	vdova
po	po	k7c6	po
svém	svůj	k3xOyFgMnSc6	svůj
bratru	bratr	k1gMnSc6	bratr
<g/>
,	,	kIx,	,
Martu	Marta	k1gFnSc4	Marta
<g/>
.	.	kIx.	.
</s>
<s>
Ona	onen	k3xDgFnSc1	onen
i	i	k9	i
její	její	k3xOp3gMnSc1	její
syn	syn	k1gMnSc1	syn
se	se	k3xPyFc4	se
přestěhovali	přestěhovat	k5eAaPmAgMnP	přestěhovat
také	také	k9	také
do	do	k7c2	do
Argentiny	Argentina	k1gFnSc2	Argentina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
na	na	k7c4	na
čas	čas	k1gInSc4	čas
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
připojili	připojit	k5eAaPmAgMnP	připojit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
několika	několik	k4yIc6	několik
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
vrátili	vrátit	k5eAaPmAgMnP	vrátit
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
rodina	rodina	k1gFnSc1	rodina
se	se	k3xPyFc4	se
o	o	k7c4	o
něj	on	k3xPp3gMnSc4	on
doma	doma	k6eAd1	doma
finančně	finančně	k6eAd1	finančně
postarala	postarat	k5eAaPmAgFnS	postarat
a	a	k8xC	a
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	let	k1gInPc6	let
pracoval	pracovat	k5eAaImAgInS	pracovat
v	v	k7c6	v
prosperující	prosperující	k2eAgFnSc6d1	prosperující
farmaceutické	farmaceutický	k2eAgFnSc6d1	farmaceutická
společnosti	společnost	k1gFnSc6	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tomto	tento	k3xDgNnSc6	tento
krátkém	krátký	k2eAgNnSc6d1	krátké
období	období	k1gNnSc6	období
žil	žít	k5eAaImAgMnS	žít
spíše	spíše	k9	spíše
chudě	chudě	k6eAd1	chudě
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1959	[number]	k4	1959
ho	on	k3xPp3gInSc4	on
objevili	objevit	k5eAaPmAgMnP	objevit
lovci	lovec	k1gMnPc1	lovec
nacistů	nacista	k1gMnPc2	nacista
a	a	k8xC	a
tak	tak	k9	tak
po	po	k7c4	po
varování	varování	k1gNnPc4	varování
uprchl	uprchnout	k5eAaPmAgMnS	uprchnout
do	do	k7c2	do
Altosu	Altos	k1gInSc2	Altos
v	v	k7c6	v
Paraguayi	Paraguay	k1gFnSc6	Paraguay
<g/>
.	.	kIx.	.
</s>
<s>
Marta	Marta	k1gFnSc1	Marta
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nesmířila	smířit	k5eNaPmAgFnS	smířit
s	s	k7c7	s
novým	nový	k2eAgInSc7d1	nový
životem	život	k1gInSc7	život
a	a	k8xC	a
opustila	opustit	k5eAaPmAgFnS	opustit
ho	on	k3xPp3gMnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
přesunul	přesunout	k5eAaPmAgMnS	přesunout
o	o	k7c4	o
něco	něco	k3yInSc4	něco
jižněji	jižně	k6eAd2	jižně
do	do	k7c2	do
paraguayského	paraguayský	k2eAgInSc2d1	paraguayský
Hohenau	Hohenaus	k1gInSc2	Hohenaus
a	a	k8xC	a
od	od	k7c2	od
konce	konec	k1gInSc2	konec
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
žil	žíla	k1gFnPc2	žíla
nedaleko	nedaleko	k7c2	nedaleko
Sã	Sã	k1gMnSc2	Sã
Paula	Paul	k1gMnSc2	Paul
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
roku	rok	k1gInSc2	rok
1979	[number]	k4	1979
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dostal	dostat	k5eAaPmAgMnS	dostat
infarkt	infarkt	k1gInSc4	infarkt
či	či	k8xC	či
se	se	k3xPyFc4	se
utopil	utopit	k5eAaPmAgMnS	utopit
<g/>
,	,	kIx,	,
když	když	k8xS	když
ho	on	k3xPp3gInSc4	on
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
křeč	křeč	k1gFnSc1	křeč
při	při	k7c6	při
plavání	plavání	k1gNnSc6	plavání
v	v	k7c6	v
plážovém	plážový	k2eAgNnSc6d1	plážové
městě	město	k1gNnSc6	město
Bertioga	Bertiog	k1gMnSc2	Bertiog
<g/>
,	,	kIx,	,
nedaleko	nedaleko	k7c2	nedaleko
brazilského	brazilský	k2eAgInSc2d1	brazilský
Embu	Embus	k1gInSc2	Embus
<g/>
,	,	kIx,	,
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Sao	Sao	k1gFnSc2	Sao
Paolo	Paolo	k1gNnSc1	Paolo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přes	přes	k7c4	přes
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
snahu	snaha	k1gFnSc4	snaha
ho	on	k3xPp3gMnSc4	on
chytit	chytit	k5eAaPmF	chytit
<g/>
,	,	kIx,	,
nebyl	být	k5eNaImAgInS	být
nikdy	nikdy	k6eAd1	nikdy
objeven	objevit	k5eAaPmNgInS	objevit
a	a	k8xC	a
žil	žít	k5eAaImAgInS	žít
35	[number]	k4	35
let	let	k1gInSc1	let
skryt	skryt	k2eAgInSc1d1	skryt
pod	pod	k7c7	pod
falešnými	falešný	k2eAgNnPc7d1	falešné
jmény	jméno	k1gNnPc7	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Chycení	chycení	k1gNnSc1	chycení
Adolfa	Adolf	k1gMnSc2	Adolf
Eichmanna	Eichmann	k1gMnSc2	Eichmann
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc1	jeho
odsouzení	odsouzení	k1gNnSc1	odsouzení
a	a	k8xC	a
poprava	poprava	k1gFnSc1	poprava
v	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
ho	on	k3xPp3gMnSc4	on
vystrašily	vystrašit	k5eAaPmAgFnP	vystrašit
a	a	k8xC	a
donutily	donutit	k5eAaPmAgFnP	donutit
k	k	k7c3	k
častému	častý	k2eAgNnSc3d1	časté
stěhování	stěhování	k1gNnSc3	stěhování
<g/>
.	.	kIx.	.
</s>
<s>
Mosad	Mosad	k6eAd1	Mosad
ho	on	k3xPp3gMnSc4	on
na	na	k7c4	na
nějaký	nějaký	k3yIgInSc4	nějaký
čas	čas	k1gInSc4	čas
vystopoval	vystopovat	k5eAaPmAgMnS	vystopovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Izrael	Izrael	k1gInSc1	Izrael
nařídil	nařídit	k5eAaPmAgInS	nařídit
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
navazování	navazování	k1gNnSc3	navazování
přátelských	přátelský	k2eAgInPc2d1	přátelský
vztahů	vztah	k1gInPc2	vztah
s	s	k7c7	s
Paraguayí	Paraguay	k1gFnSc7	Paraguay
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
hledat	hledat	k5eAaImF	hledat
nepřátele	nepřítel	k1gMnPc4	nepřítel
blíže	blízce	k6eAd2	blízce
k	k	k7c3	k
domovu	domov	k1gInSc3	domov
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Nebyl	být	k5eNaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
lovci	lovec	k1gMnPc1	lovec
nacistů	nacista	k1gMnPc2	nacista
až	až	k9	až
do	do	k7c2	do
6	[number]	k4	6
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1985	[number]	k4	1985
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
nalezeno	nalézt	k5eAaBmNgNnS	nalézt
jeho	jeho	k3xOp3gNnSc1	jeho
tělo	tělo	k1gNnSc1	tělo
a	a	k8xC	a
pomocí	pomocí	k7c2	pomocí
DNA	dno	k1gNnSc2	dno
identifikováno	identifikovat	k5eAaBmNgNnS	identifikovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Astor	Astor	k1gMnSc1	Astor
<g/>
,	,	kIx,	,
G.	G.	kA	G.
<g/>
:	:	kIx,	:
Last	Last	k1gMnSc1	Last
Nazi	Naz	k1gFnSc2	Naz
<g/>
:	:	kIx,	:
Life	Life	k1gInSc1	Life
and	and	k?	and
Times	Times	k1gInSc1	Times
of	of	k?	of
Doctor	Doctor	k1gInSc1	Doctor
Joseph	Josepha	k1gFnPc2	Josepha
Mengele	Mengel	k1gInSc2	Mengel
<g/>
,	,	kIx,	,
1986	[number]	k4	1986
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0-297-78853-1	[number]	k4	0-297-78853-1
</s>
</p>
<p>
<s>
Kubica	Kubica	k1gMnSc1	Kubica
H.	H.	kA	H.
<g/>
:	:	kIx,	:
Dr	dr	kA	dr
Mengele	Mengel	k1gInPc1	Mengel
i	i	k9	i
jego	jego	k1gNnSc1	jego
zbrodnie	zbrodnie	k1gFnSc2	zbrodnie
w	w	k?	w
obozie	obozie	k1gFnSc1	obozie
koncentracyjnym	koncentracyjnym	k1gInSc1	koncentracyjnym
Oświęcim-Brzezinka	Oświęcim-Brzezinka	k1gFnSc1	Oświęcim-Brzezinka
<g/>
,	,	kIx,	,
<g/>
in	in	k?	in
<g/>
:	:	kIx,	:
Zeszyty	Zeszyt	k1gInPc1	Zeszyt
Oświęcimskie	Oświęcimskie	k1gFnSc2	Oświęcimskie
nr	nr	k?	nr
20	[number]	k4	20
<g/>
,	,	kIx,	,
Oświęcim	Oświęcim	k1gInSc1	Oświęcim
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
325	[number]	k4	325
<g/>
-	-	kIx~	-
<g/>
389	[number]	k4	389
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
0474-8581	[number]	k4	0474-8581
</s>
</p>
<p>
<s>
Levin	Levin	k1gMnSc1	Levin
<g/>
,	,	kIx,	,
I.	I.	kA	I.
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Boys	boy	k1gMnPc2	boy
from	from	k1gMnSc1	from
Brazil	Brazil	k1gMnSc1	Brazil
<g/>
,	,	kIx,	,
London	London	k1gMnSc1	London
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0-553-29004-5	[number]	k4	0-553-29004-5
</s>
</p>
<p>
<s>
Nyiszli	Nyisznout	k5eAaPmAgMnP	Nyisznout
<g/>
,	,	kIx,	,
M.	M.	kA	M.
<g/>
:	:	kIx,	:
Byl	být	k5eAaImAgMnS	být
jsem	být	k5eAaImIp1nS	být
Mengeleho	Mengeleha	k1gFnSc5	Mengeleha
asistentem	asistent	k1gMnSc7	asistent
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978-80-200-1757-4	[number]	k4	978-80-200-1757-4
</s>
</p>
<p>
<s>
Posner	Posner	k1gMnSc1	Posner
<g/>
,	,	kIx,	,
G.L.	G.L.	k1gMnSc1	G.L.
<g/>
,	,	kIx,	,
Ware	Ware	k1gFnSc1	Ware
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
:	:	kIx,	:
Mengele	Mengel	k1gInSc2	Mengel
-	-	kIx~	-
anděl	anděl	k1gMnSc1	anděl
smrti	smrt	k1gFnSc2	smrt
-	-	kIx~	-
úplný	úplný	k2eAgInSc1d1	úplný
životní	životní	k2eAgInSc1d1	životní
příběh	příběh	k1gInSc1	příběh
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978-80-87021-76-7	[number]	k4	978-80-87021-76-7
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Josef	Josefa	k1gFnPc2	Josefa
Mengele	Mengel	k1gInSc2	Mengel
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Fabiano	Fabiana	k1gFnSc5	Fabiana
Golgo	Golga	k1gFnSc5	Golga
<g/>
:	:	kIx,	:
Kde	kde	k6eAd1	kde
strávil	strávit	k5eAaPmAgMnS	strávit
Josef	Josef	k1gMnSc1	Josef
Mengele	Mengel	k1gInSc2	Mengel
své	svůj	k3xOyFgInPc4	svůj
poslední	poslední	k2eAgInPc4d1	poslední
dny	den	k1gInPc4	den
<g/>
,	,	kIx,	,
Britské	britský	k2eAgInPc4d1	britský
listy	list	k1gInPc4	list
<g/>
,	,	kIx,	,
24	[number]	k4	24
<g/>
.	.	kIx.	.
6.200	[number]	k4	6.200
<g/>
7	[number]	k4	7
</s>
</p>
<p>
<s>
Ondřej	Ondřej	k1gMnSc1	Ondřej
Šťastný	Šťastný	k1gMnSc1	Šťastný
<g/>
:	:	kIx,	:
Mengele	Mengel	k1gInPc1	Mengel
mi	já	k3xPp1nSc3	já
vykal	vykat	k5eAaImAgMnS	vykat
a	a	k8xC	a
říkal	říkat	k5eAaImAgMnS	říkat
<g/>
:	:	kIx,	:
Prosím	prosit	k5eAaImIp1nS	prosit
<g/>
,	,	kIx,	,
iDnes	iDnes	k1gInSc1	iDnes
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
28	[number]	k4	28
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
2007	[number]	k4	2007
</s>
</p>
