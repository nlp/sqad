<s>
Španělská	španělský	k2eAgFnSc1d1
synagoga	synagoga	k1gFnSc1
(	(	kIx(
<g/>
Benátky	Benátky	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
Španělská	španělský	k2eAgFnSc1d1
synagoga	synagoga	k1gFnSc1
(	(	kIx(
<g/>
Benátky	Benátky	k1gFnPc1
<g/>
)	)	kIx)
Místo	místo	k1gNnSc1
Místo	místo	k7c2
</s>
<s>
Benátky	Benátky	k1gFnPc1
<g/>
,	,	kIx,
Itálie	Itálie	k1gFnSc1
Itálie	Itálie	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
45	#num#	k4
<g/>
°	°	k?
<g/>
26	#num#	k4
<g/>
′	′	k?
<g/>
41	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
12	#num#	k4
<g/>
°	°	k?
<g/>
19	#num#	k4
<g/>
′	′	k?
<g/>
32	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Základní	základní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Ritus	ritus	k1gInSc1
</s>
<s>
sefardský	sefardský	k2eAgInSc1d1
Datum	datum	k1gInSc1
posvěcení	posvěcení	k1gNnSc2
</s>
<s>
1550	#num#	k4
Užívání	užívání	k1gNnSc1
</s>
<s>
aktivní	aktivní	k2eAgInSc4d1
Architektonický	architektonický	k2eAgInSc4d1
popis	popis	k1gInSc4
Architekt	architekt	k1gMnSc1
</s>
<s>
Baldassare	Baldassar	k1gMnSc5
Longhena	Longheno	k1gNnPc4
Sloh	sloh	k1gInSc1
</s>
<s>
baroko	baroko	k1gNnSc1
Výstavba	výstavba	k1gFnSc1
</s>
<s>
1550	#num#	k4
<g/>
–	–	k?
<g/>
?	?	kIx.
</s>
<s desamb="1">
Odkazy	odkaz	k1gInPc5
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Španělská	španělský	k2eAgFnSc1d1
synagoga	synagoga	k1gFnSc1
<g/>
,	,	kIx,
neboli	neboli	k8xC
Scuola	Scuola	k1gFnSc1
spagnola	spagnola	k1gFnSc1
je	být	k5eAaImIp3nS
jedna	jeden	k4xCgFnSc1
z	z	k7c2
celkem	celkem	k6eAd1
pěti	pět	k4xCc2
synagog	synagoga	k1gFnPc2
v	v	k7c6
bývalé	bývalý	k2eAgFnSc6d1
benátské	benátský	k2eAgFnSc6d1
židovské	židovský	k2eAgFnSc6d1
čtvrti	čtvrt	k1gFnSc6
Ghetto	ghetto	k1gNnSc1
Nuovo	Nuovo	k1gNnSc1
(	(	kIx(
<g/>
odtud	odtud	k6eAd1
odvozeno	odvozen	k2eAgNnSc1d1
slovo	slovo	k1gNnSc1
ghetto	ghetto	k1gNnSc1
<g/>
)	)	kIx)
v	v	k7c6
městské	městský	k2eAgFnSc6d1
části	část	k1gFnSc6
Cannaregio	Cannaregio	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
navržena	navržen	k2eAgFnSc1d1
italským	italský	k2eAgMnSc7d1
architektem	architekt	k1gMnSc7
Baldassarem	Baldassar	k1gMnSc7
Longhenou	Longhený	k2eAgFnSc7d1
v	v	k7c6
barokním	barokní	k2eAgInSc6d1
slohu	sloh	k1gInSc6
a	a	k8xC
postavena	postaven	k2eAgFnSc1d1
roku	rok	k1gInSc2
1580	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
jedna	jeden	k4xCgFnSc1
ze	z	k7c2
dvou	dva	k4xCgFnPc2
fungujících	fungující	k2eAgFnPc2d1
synagog	synagoga	k1gFnPc2
místní	místní	k2eAgFnSc2d1
židovské	židovský	k2eAgFnSc2d1
obce	obec	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bohoslužby	bohoslužba	k1gFnPc1
se	se	k3xPyFc4
zde	zde	k6eAd1
konají	konat	k5eAaImIp3nP
od	od	k7c2
Pesachu	pesach	k1gInSc2
do	do	k7c2
Vysokých	vysoký	k2eAgInPc2d1
svátků	svátek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ritus	ritus	k1gInSc1
je	být	k5eAaImIp3nS
sefardský	sefardský	k2eAgInSc1d1
s	s	k7c7
lokálními	lokální	k2eAgNnPc7d1
specifiky	specifikon	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Španělská	španělský	k2eAgFnSc1d1
synagoga	synagoga	k1gFnSc1
byla	být	k5eAaImAgFnS
vystavěna	vystavěn	k2eAgFnSc1d1
roku	rok	k1gInSc2
1550	#num#	k4
španělskými	španělský	k2eAgNnPc7d1
<g/>
,	,	kIx,
tedy	tedy	k9
sefardskými	sefardský	k2eAgMnPc7d1
židy	žid	k1gMnPc7
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
do	do	k7c2
Benátek	Benátky	k1gFnPc2
přišli	přijít	k5eAaPmAgMnP
přes	přes	k7c4
Amsterdam	Amsterdam	k1gInSc4
<g/>
,	,	kIx,
Livorno	Livorno	k1gNnSc4
a	a	k8xC
Ferraru	Ferrara	k1gFnSc4
po	po	k7c6
vypovězení	vypovězení	k1gNnSc6
z	z	k7c2
Pyrenejského	pyrenejský	k2eAgInSc2d1
poloostrova	poloostrov	k1gInSc2
nařízením	nařízení	k1gNnSc7
španělské	španělský	k2eAgFnSc2d1
královny	královna	k1gFnSc2
Isabely	Isabela	k1gFnSc2
Kastilské	kastilský	k2eAgFnSc2d1
v	v	k7c6
roce	rok	k1gInSc6
1490	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
benátském	benátský	k2eAgNnSc6d1
ghettu	ghetto	k1gNnSc6
byly	být	k5eAaImAgFnP
synagogy	synagoga	k1gFnPc1
rozděleny	rozdělit	k5eAaPmNgFnP
podle	podle	k7c2
„	„	k?
<g/>
národností	národnost	k1gFnPc2
<g/>
“	“	k?
jeho	jeho	k3xOp3gMnPc2
obyvatel	obyvatel	k1gMnPc2
<g/>
,	,	kIx,
tedy	tedy	k8xC
dle	dle	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
odkud	odkud	k6eAd1
místní	místní	k2eAgMnPc1d1
židé	žid	k1gMnPc1
přišli	přijít	k5eAaPmAgMnP
a	a	k8xC
jaký	jaký	k3yRgInSc4,k3yIgInSc4,k3yQgInSc4
ritus	ritus	k1gInSc4
praktikovali	praktikovat	k5eAaImAgMnP
<g/>
;	;	kIx,
v	v	k7c6
Ghettu	ghetto	k1gNnSc6
tak	tak	k6eAd1
najdeme	najít	k5eAaPmIp1nP
synagogu	synagoga	k1gFnSc4
německou	německý	k2eAgFnSc4d1
(	(	kIx(
<g/>
Scola	Scola	k1gMnSc1
tedesca	tedesca	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
kantonskou	kantonský	k2eAgFnSc4d1
(	(	kIx(
<g/>
Scola	Scola	k1gMnSc1
cantonese	cantonese	k1gFnSc2
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
praktikoval	praktikovat	k5eAaImAgInS
aškenázský	aškenázský	k2eAgInSc1d1
ritus	ritus	k1gInSc1
<g/>
,	,	kIx,
synagogu	synagoga	k1gFnSc4
levantinskou	levantinský	k2eAgFnSc4d1
(	(	kIx(
<g/>
Scola	Scola	k1gMnSc1
levantini	levantin	k2eAgMnPc1d1
<g/>
)	)	kIx)
a	a	k8xC
španělskou	španělský	k2eAgFnSc4d1
(	(	kIx(
<g/>
Scola	Scola	k1gMnSc1
spagnola	spagnola	k1gFnSc1
<g/>
)	)	kIx)
se	s	k7c7
sefardským	sefardský	k2eAgInSc7d1
ritem	rit	k1gInSc7
a	a	k8xC
synagogu	synagoga	k1gFnSc4
italskou	italský	k2eAgFnSc4d1
(	(	kIx(
<g/>
Scola	Scola	k1gMnSc1
italiana	italian	k1gMnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
fungoval	fungovat	k5eAaImAgInS
jakýsi	jakýsi	k3yIgInSc4
mix	mix	k1gInSc4
obou	dva	k4xCgInPc2
ritů	rit	k1gInPc2
<g/>
,	,	kIx,
zvaný	zvaný	k2eAgInSc1d1
italský	italský	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1
vchod	vchod	k1gInSc1
do	do	k7c2
synagogy	synagoga	k1gFnSc2
</s>
<s>
Památník	památník	k1gInSc1
obětem	oběť	k1gFnPc3
holocaustu	holocaust	k1gInSc2
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Umberto	Umberta	k1gFnSc5
Fortis	Fortis	k1gFnSc5
-	-	kIx~
The	The	k1gMnSc6
Ghetto	ghetto	k1gNnSc1
on	on	k3xPp3gMnSc1
the	the	k?
Lagoon	Lagoon	k1gNnSc1
<g/>
,	,	kIx,
Stori	Stor	k1gFnPc1
edizioni	edizioň	k1gFnSc3
<g/>
,	,	kIx,
Benátky	Benátky	k1gFnPc1
2001	#num#	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Španělská	španělský	k2eAgFnSc1d1
synagoga	synagoga	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
The	The	k?
Spanish	Spanish	k1gInSc1
Synagogue	Synagogue	k1gInSc1
</s>
