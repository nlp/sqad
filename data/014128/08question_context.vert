<s>
Španělská	španělský	k2eAgFnSc1d1
synagoga	synagoga	k1gFnSc1
<g/>
,	,	kIx,
neboli	neboli	k8xC
Scuola	Scuola	k1gFnSc1
spagnola	spagnola	k1gFnSc1
je	být	k5eAaImIp3nS
jedna	jeden	k4xCgFnSc1
z	z	k7c2
celkem	celkem	k6eAd1
pěti	pět	k4xCc2
synagog	synagoga	k1gFnPc2
v	v	k7c6
bývalé	bývalý	k2eAgFnSc6d1
benátské	benátský	k2eAgFnSc6d1
židovské	židovský	k2eAgFnSc6d1
čtvrti	čtvrt	k1gFnSc6
Ghetto	ghetto	k1gNnSc1
Nuovo	Nuovo	k1gNnSc1
(	(	kIx(
<g/>
odtud	odtud	k6eAd1
odvozeno	odvozen	k2eAgNnSc1d1
slovo	slovo	k1gNnSc1
ghetto	ghetto	k1gNnSc1
<g/>
)	)	kIx)
v	v	k7c6
městské	městský	k2eAgFnSc6d1
části	část	k1gFnSc6
Cannaregio	Cannaregio	k6eAd1
<g/>
.	.	kIx.
</s>