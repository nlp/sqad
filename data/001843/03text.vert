<s>
Bambiraptor	Bambiraptor	k1gMnSc1	Bambiraptor
je	být	k5eAaImIp3nS	být
malý	malý	k2eAgMnSc1d1	malý
dravý	dravý	k2eAgMnSc1d1	dravý
dinosaurus	dinosaurus	k1gMnSc1	dinosaurus
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
žil	žít	k5eAaImAgMnS	žít
před	před	k7c7	před
75	[number]	k4	75
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Montany	Montana	k1gFnSc2	Montana
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
jen	jen	k9	jen
asi	asi	k9	asi
30	[number]	k4	30
cm	cm	kA	cm
vysoký	vysoký	k2eAgMnSc1d1	vysoký
<g/>
,	,	kIx,	,
70	[number]	k4	70
cm	cm	kA	cm
dlouhý	dlouhý	k2eAgMnSc1d1	dlouhý
a	a	k8xC	a
vážil	vážit	k5eAaImAgInS	vážit
kolem	kolem	k7c2	kolem
2	[number]	k4	2
kg	kg	kA	kg
(	(	kIx(	(
<g/>
šlo	jít	k5eAaImAgNnS	jít
však	však	k9	však
ještě	ještě	k9	ještě
o	o	k7c4	o
nedospělého	dospělý	k2eNgMnSc4d1	nedospělý
jedince	jedinec	k1gMnSc4	jedinec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bambiraptor	Bambiraptor	k1gInSc1	Bambiraptor
byl	být	k5eAaImAgInS	být
objeven	objeven	k2eAgInSc1d1	objeven
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
teprve	teprve	k6eAd1	teprve
čtrnáctiletým	čtrnáctiletý	k2eAgMnSc7d1	čtrnáctiletý
chlapcem	chlapec	k1gMnSc7	chlapec
v	v	k7c4	v
Glacier	Glacier	k1gInSc4	Glacier
National	National	k1gMnPc2	National
Park	park	k1gInSc1	park
v	v	k7c6	v
Montaně	Montana	k1gFnSc6	Montana
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
svých	svůj	k3xOyFgInPc2	svůj
malých	malý	k2eAgInPc2d1	malý
rozměrů	rozměr	k1gInPc2	rozměr
dostal	dostat	k5eAaPmAgInS	dostat
i	i	k9	i
jméno	jméno	k1gNnSc4	jméno
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
známé	známá	k1gFnSc2	známá
Disneyho	Disney	k1gMnSc2	Disney
postavičky	postavička	k1gFnSc2	postavička
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bambiraptor	Bambiraptor	k1gInSc1	Bambiraptor
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejlépe	dobře	k6eAd3	dobře
zachovaných	zachovaný	k2eAgMnPc2d1	zachovaný
dinosaurů	dinosaurus	k1gMnPc2	dinosaurus
vůbec	vůbec	k9	vůbec
(	(	kIx(	(
<g/>
kostra	kostra	k1gFnSc1	kostra
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
z	z	k7c2	z
95	[number]	k4	95
<g/>
%	%	kIx~	%
kompletní	kompletní	k2eAgFnSc1d1	kompletní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
také	také	k9	také
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
měl	mít	k5eAaImAgInS	mít
vůbec	vůbec	k9	vůbec
největší	veliký	k2eAgInSc1d3	veliký
mozek	mozek	k1gInSc1	mozek
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
k	k	k7c3	k
velikosti	velikost	k1gFnSc3	velikost
těla	tělo	k1gNnSc2	tělo
ze	z	k7c2	z
všech	všecek	k3xTgMnPc2	všecek
známých	známý	k2eAgMnPc2d1	známý
dinosaurů	dinosaurus	k1gMnPc2	dinosaurus
(	(	kIx(	(
<g/>
hodnota	hodnota	k1gFnSc1	hodnota
EQ	EQ	kA	EQ
-	-	kIx~	-
encefalizačního	encefalizační	k2eAgInSc2d1	encefalizační
kvocientu	kvocient	k1gInSc2	kvocient
je	být	k5eAaImIp3nS	být
12,5	[number]	k4	12,5
-	-	kIx~	-
13,8	[number]	k4	13,8
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
tedy	tedy	k9	tedy
o	o	k7c4	o
velmi	velmi	k6eAd1	velmi
bystrého	bystrý	k2eAgMnSc4d1	bystrý
a	a	k8xC	a
agilního	agilní	k2eAgMnSc4d1	agilní
dravce	dravec	k1gMnSc4	dravec
<g/>
,	,	kIx,	,
možná	možná	k9	možná
žil	žít	k5eAaImAgMnS	žít
dokonce	dokonce	k9	dokonce
částečně	částečně	k6eAd1	částečně
na	na	k7c6	na
stromech	strom	k1gInPc6	strom
<g/>
.	.	kIx.	.
</s>
<s>
Bambiraptor	Bambiraptor	k1gMnSc1	Bambiraptor
byl	být	k5eAaImAgMnS	být
ještě	ještě	k6eAd1	ještě
inteligentnější	inteligentní	k2eAgMnSc1d2	inteligentnější
než	než	k8xS	než
příbuzný	příbuzný	k2eAgMnSc1d1	příbuzný
druh	druh	k1gMnSc1	druh
Troodon	Troodon	k1gMnSc1	Troodon
formosus	formosus	k1gMnSc1	formosus
<g/>
.	.	kIx.	.
</s>
<s>
Nedávno	nedávno	k6eAd1	nedávno
navíc	navíc	k6eAd1	navíc
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
dokázal	dokázat	k5eAaPmAgMnS	dokázat
obrátit	obrátit	k5eAaPmF	obrátit
prst	prst	k1gInSc4	prst
proti	proti	k7c3	proti
ostatním	ostatní	k2eAgFnPc3d1	ostatní
dvěma	dva	k4xCgFnPc3	dva
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
tak	tak	k9	tak
schopen	schopen	k2eAgInSc1d1	schopen
manipulovat	manipulovat	k5eAaImF	manipulovat
s	s	k7c7	s
předměty	předmět	k1gInPc7	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
schopnost	schopnost	k1gFnSc4	schopnost
přitom	přitom	k6eAd1	přitom
zřejmě	zřejmě	k6eAd1	zřejmě
žádný	žádný	k3yNgMnSc1	žádný
jiný	jiný	k2eAgMnSc1d1	jiný
dinosaurus	dinosaurus	k1gMnSc1	dinosaurus
až	až	k9	až
na	na	k7c4	na
Iguanodona	Iguanodon	k1gMnSc4	Iguanodon
neměl	mít	k5eNaImAgInS	mít
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Bambiraptor	Bambiraptor	k1gInSc4	Bambiraptor
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc1	Wikimedium
Commons	Commons	k1gInSc4	Commons
Článek	článek	k1gInSc4	článek
na	na	k7c4	na
Wild	Wild	k1gInSc1	Wild
Prehistory	Prehistor	k1gInPc1	Prehistor
<g/>
:	:	kIx,	:
Bambiraptorův	Bambiraptorův	k2eAgInSc1d1	Bambiraptorův
vratiprst	vratiprst	k1gInSc1	vratiprst
Článek	článek	k1gInSc1	článek
na	na	k7c6	na
webu	web	k1gInSc6	web
Dinosauria	Dinosaurium	k1gNnSc2	Dinosaurium
<g/>
:	:	kIx,	:
Bambiraptor	Bambiraptor	k1gInSc1	Bambiraptor
-	-	kIx~	-
nelétavý	létavý	k2eNgMnSc1d1	nelétavý
orel	orel	k1gMnSc1	orel
doby	doba	k1gFnSc2	doba
křídové	křídový	k2eAgFnSc2d1	křídová
</s>
