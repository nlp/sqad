<s>
Křesťanská	křesťanský	k2eAgFnSc1d1	křesťanská
a	a	k8xC	a
demokratická	demokratický	k2eAgFnSc1d1	demokratická
unie	unie	k1gFnSc1	unie
–	–	k?	–
Československá	československý	k2eAgFnSc1d1	Československá
strana	strana	k1gFnSc1	strana
lidová	lidový	k2eAgFnSc1d1	lidová
(	(	kIx(	(
<g/>
KDU-ČSL	KDU-ČSL	k1gFnSc1	KDU-ČSL
<g/>
,	,	kIx,	,
též	též	k9	též
lidovci	lidovec	k1gMnPc1	lidovec
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
křesťansko-demokratická	křesťanskoemokratický	k2eAgFnSc1d1	křesťansko-demokratická
politická	politický	k2eAgFnSc1d1	politická
strana	strana	k1gFnSc1	strana
<g/>
.	.	kIx.	.
</s>
