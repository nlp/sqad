<s>
Velryba	velryba	k1gFnSc1	velryba
je	být	k5eAaImIp3nS	být
mořský	mořský	k2eAgMnSc1d1	mořský
savec	savec	k1gMnSc1	savec
z	z	k7c2	z
řádu	řád	k1gInSc2	řád
kytovci	kytovec	k1gMnPc1	kytovec
(	(	kIx(	(
<g/>
Cetacea	cetaceum	k1gNnSc2	cetaceum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
největší	veliký	k2eAgMnPc4d3	veliký
živočichy	živočich	k1gMnPc4	živočich
žijící	žijící	k2eAgMnPc4d1	žijící
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Živí	živit	k5eAaImIp3nP	živit
se	s	k7c7	s
planktonem	plankton	k1gInSc7	plankton
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgMnS	být
suchozemský	suchozemský	k2eAgMnSc1d1	suchozemský
savec	savec	k1gMnSc1	savec
<g/>
.	.	kIx.	.
</s>
<s>
Zpravidla	zpravidla	k6eAd1	zpravidla
se	se	k3xPyFc4	se
jako	jako	k9	jako
velryby	velryba	k1gFnSc2	velryba
označují	označovat	k5eAaImIp3nP	označovat
příslušníci	příslušník	k1gMnPc1	příslušník
podřádu	podřád	k1gInSc2	podřád
kosticovců	kosticovec	k1gMnPc2	kosticovec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
mnoho	mnoho	k4c1	mnoho
ozubených	ozubený	k2eAgMnPc2d1	ozubený
kytovců	kytovec	k1gMnPc2	kytovec
je	být	k5eAaImIp3nS	být
označováno	označovat	k5eAaImNgNnS	označovat
jako	jako	k8xS	jako
velryby	velryba	k1gFnPc4	velryba
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
pak	pak	k6eAd1	pak
velké	velký	k2eAgInPc1d1	velký
druhy	druh	k1gInPc1	druh
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
kosatka	kosatka	k1gFnSc1	kosatka
dravá	dravý	k2eAgFnSc1d1	dravá
<g/>
,	,	kIx,	,
kulohlavec	kulohlavec	k1gMnSc1	kulohlavec
černý	černý	k1gMnSc1	černý
<g/>
,	,	kIx,	,
běluha	běluha	k1gFnSc1	běluha
<g/>
,	,	kIx,	,
narval	narval	k1gMnSc1	narval
<g/>
,	,	kIx,	,
vorvaň	vorvaň	k1gMnSc1	vorvaň
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Kytovci	kytovec	k1gMnPc1	kytovec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
rodové	rodový	k2eAgNnSc1d1	rodové
jméno	jméno	k1gNnSc1	jméno
velryba	velryba	k1gFnSc1	velryba
<g/>
:	:	kIx,	:
Velryba	velryba	k1gFnSc1	velryba
grónská	grónský	k2eAgFnSc1d1	grónská
(	(	kIx(	(
<g/>
Balaena	Balaena	k1gFnSc1	Balaena
mysticetus	mysticetus	k1gMnSc1	mysticetus
<g/>
)	)	kIx)	)
Velryba	velryba	k1gFnSc1	velryba
černá	černý	k2eAgFnSc1d1	černá
(	(	kIx(	(
<g/>
Eubalaena	Eubalaen	k2eAgFnSc1d1	Eubalaena
glacialis	glacialis	k1gFnSc1	glacialis
<g/>
)	)	kIx)	)
Velryba	velryba	k1gFnSc1	velryba
jižní	jižní	k2eAgFnSc1d1	jižní
(	(	kIx(	(
<g/>
Eubalaena	Eubalaen	k2eAgFnSc1d1	Eubalaena
australis	australis	k1gFnSc1	australis
<g/>
)	)	kIx)	)
Všechny	všechen	k3xTgInPc1	všechen
druhy	druh	k1gInPc1	druh
patří	patřit	k5eAaImIp3nP	patřit
do	do	k7c2	do
čeledi	čeleď	k1gFnSc3	čeleď
velrybovití	velrybovitý	k2eAgMnPc1d1	velrybovitý
<g/>
.	.	kIx.	.
</s>
<s>
Úmluva	úmluva	k1gFnSc1	úmluva
upravující	upravující	k2eAgFnSc1d1	upravující
lov	lov	k1gInSc4	lov
velryb	velryba	k1gFnPc2	velryba
(	(	kIx(	(
<g/>
Ženeva	Ženeva	k1gFnSc1	Ženeva
<g/>
,	,	kIx,	,
24	[number]	k4	24
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vyhlášena	vyhlášen	k2eAgFnSc1d1	vyhlášena
v	v	k7c6	v
ČSR	ČSR	kA	ČSR
pod	pod	k7c7	pod
č.	č.	k?	č.
34	[number]	k4	34
<g/>
/	/	kIx~	/
<g/>
1935	[number]	k4	1935
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
z.	z.	k?	z.
a	a	k8xC	a
n.	n.	k?	n.
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
úmluva	úmluva	k1gFnSc1	úmluva
pro	pro	k7c4	pro
regulaci	regulace	k1gFnSc4	regulace
lovu	lov	k1gInSc2	lov
velryb	velryba	k1gFnPc2	velryba
(	(	kIx(	(
<g/>
Washington	Washington	k1gInSc1	Washington
<g/>
,	,	kIx,	,
D.C.	D.C.	k1gFnSc1	D.C.
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
<g />
.	.	kIx.	.
</s>
<s>
prosince	prosinec	k1gInSc2	prosinec
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
Nařízení	nařízení	k1gNnSc4	nařízení
Rady	rada	k1gFnSc2	rada
(	(	kIx(	(
<g/>
EHS	EHS	kA	EHS
<g/>
)	)	kIx)	)
č.	č.	k?	č.
348	[number]	k4	348
<g/>
/	/	kIx~	/
<g/>
81	[number]	k4	81
z	z	k7c2	z
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1981	[number]	k4	1981
o	o	k7c6	o
společných	společný	k2eAgInPc6d1	společný
předpisech	předpis	k1gInPc6	předpis
pro	pro	k7c4	pro
dovoz	dovoz	k1gInSc4	dovoz
výrobků	výrobek	k1gInPc2	výrobek
z	z	k7c2	z
velryb	velryba	k1gFnPc2	velryba
a	a	k8xC	a
ostatních	ostatní	k2eAgMnPc2d1	ostatní
kytovců	kytovec	k1gMnPc2	kytovec
Nařízení	nařízení	k1gNnSc2	nařízení
Rady	rada	k1gFnSc2	rada
(	(	kIx(	(
<g/>
ES	ES	kA	ES
<g/>
)	)	kIx)	)
č.	č.	k?	č.
338	[number]	k4	338
<g/>
/	/	kIx~	/
<g/>
97	[number]	k4	97
ze	z	k7c2	z
dne	den	k1gInSc2	den
9	[number]	k4	9
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1996	[number]	k4	1996
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
druhů	druh	k1gInPc2	druh
volně	volně	k6eAd1	volně
žijících	žijící	k2eAgMnPc2d1	žijící
živočichů	živočich	k1gMnPc2	živočich
a	a	k8xC	a
planě	planě	k6eAd1	planě
rostoucích	rostoucí	k2eAgFnPc2d1	rostoucí
rostlin	rostlina	k1gFnPc2	rostlina
regulováním	regulování	k1gNnSc7	regulování
obchodu	obchod	k1gInSc2	obchod
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
Kytovci	kytovec	k1gMnPc7	kytovec
Lov	lov	k1gInSc1	lov
velryb	velryba	k1gFnPc2	velryba
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
velryba	velryba	k1gFnSc1	velryba
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
velryba	velryba	k1gFnSc1	velryba
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
