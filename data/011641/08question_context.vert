<s>
Havraspár	Havraspár	k1gInSc1	Havraspár
(	(	kIx(	(
<g/>
v	v	k7c6	v
originálu	originál	k1gInSc6	originál
Ravenclaw	Ravenclaw	k1gFnSc2	Ravenclaw
<g/>
,	,	kIx,	,
spojení	spojení	k1gNnSc1	spojení
slov	slovo	k1gNnPc2	slovo
raven	ravno	k1gNnPc2	ravno
–	–	k?	–
krkavec	krkavec	k1gMnSc1	krkavec
či	či	k8xC	či
havran	havran	k1gMnSc1	havran
a	a	k8xC	a
claw	claw	k?	claw
–	–	k?	–
pařát	pařát	k1gInSc1	pařát
<g/>
,	,	kIx,	,
spár	spár	k1gInSc1	spár
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
Rowenou	Rowena	k1gFnSc7	Rowena
z	z	k7c2	z
Havraspáru	Havraspár	k1gInSc2	Havraspár
<g/>
.	.	kIx.	.
</s>
<s>
Kolejními	kolejní	k2eAgFnPc7d1	kolejní
barvami	barva	k1gFnPc7	barva
jsou	být	k5eAaImIp3nP	být
modrá	modrý	k2eAgFnSc1d1	modrá
a	a	k8xC	a
bronzová	bronzový	k2eAgFnSc1d1	bronzová
(	(	kIx(	(
<g/>
ve	v	k7c6	v
filmech	film	k1gInPc6	film
modrá	modrý	k2eAgFnSc1d1	modrá
a	a	k8xC	a
stříbrná	stříbrný	k2eAgFnSc1d1	stříbrná
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
erbovním	erbovní	k2eAgNnSc7d1	erbovní
zvířetem	zvíře	k1gNnSc7	zvíře
je	být	k5eAaImIp3nS	být
orel	orel	k1gMnSc1	orel
<g/>
.	.	kIx.	.
</s>
