<p>
<s>
Diablovina	Diablovina	k1gFnSc1	Diablovina
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
výrazným	výrazný	k2eAgFnPc3d1	výrazná
věžím	věž	k1gFnPc3	věž
v	v	k7c6	v
hřebeni	hřeben	k1gInSc6	hřeben
Bášt	Bášta	k1gFnPc2	Bášta
ve	v	k7c6	v
Vysokých	vysoký	k2eAgFnPc6d1	vysoká
Tatrách	Tatra	k1gFnPc6	Tatra
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
sousedními	sousední	k2eAgFnPc7d1	sousední
věžemi	věž	k1gFnPc7	věž
přispívá	přispívat	k5eAaImIp3nS	přispívat
k	k	k7c3	k
divoké	divoký	k2eAgFnSc3d1	divoká
rozeklaností	rozeklanost	k1gFnSc7	rozeklanost
nejvyšší	vysoký	k2eAgFnSc3d3	nejvyšší
části	část	k1gFnSc3	část
hřebene	hřeben	k1gInSc2	hřeben
<g/>
.	.	kIx.	.
</s>
<s>
Převážně	převážně	k6eAd1	převážně
pekelné	pekelný	k2eAgNnSc1d1	pekelné
názvosloví	názvosloví	k1gNnSc1	názvosloví
tohoto	tento	k3xDgInSc2	tento
úseku	úsek	k1gInSc2	úsek
se	se	k3xPyFc4	se
váže	vázat	k5eAaImIp3nS	vázat
k	k	k7c3	k
pověsti	pověst	k1gFnSc3	pověst
<g/>
.	.	kIx.	.
</s>
<s>
Maďarský	maďarský	k2eAgInSc4d1	maďarský
název	název	k1gInSc4	název
Ördög-torony	Ördögoron	k1gInPc4	Ördög-toron
i	i	k8xC	i
německý	německý	k2eAgInSc1d1	německý
Teufelsturm	Teufelsturm	k1gInSc1	Teufelsturm
znamená	znamenat	k5eAaImIp3nS	znamenat
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
Čertova	čertův	k2eAgFnSc1d1	Čertova
věž	věž	k1gFnSc1	věž
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
však	však	k9	však
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
masivu	masiv	k1gInSc6	masiv
Kotlového	kotlový	k2eAgInSc2d1	kotlový
štítu	štít	k1gInSc2	štít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Topografie	topografie	k1gFnSc2	topografie
==	==	k?	==
</s>
</p>
<p>
<s>
Popisovanou	popisovaný	k2eAgFnSc4d1	popisovaná
skupinu	skupina	k1gFnSc4	skupina
věží	věž	k1gFnPc2	věž
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
Diablovo	Diablův	k2eAgNnSc4d1	Diablův
sedlo	sedlo	k1gNnSc4	sedlo
od	od	k7c2	od
Zadní	zadní	k2eAgFnSc2d1	zadní
Bašty	bašta	k1gFnSc2	bašta
<g/>
,	,	kIx,	,
Satanovo	Satanův	k2eAgNnSc1d1	Satanův
sedlo	sedlo	k1gNnSc1	sedlo
od	od	k7c2	od
Satana	Satan	k1gMnSc2	Satan
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
severovýchodu	severovýchod	k1gInSc2	severovýchod
je	být	k5eAaImIp3nS	být
první	první	k4xOgFnSc1	první
Zlatinská	Zlatinský	k2eAgFnSc1d1	Zlatinský
veža	veža	k1gFnSc1	veža
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
Zlatinská	Zlatinský	k2eAgFnSc1d1	Zlatinský
štrbina	štrbina	k1gFnSc1	štrbina
<g/>
,	,	kIx,	,
Diablovina	Diablovina	k1gFnSc1	Diablovina
<g/>
,	,	kIx,	,
Pekelníkova	pekelníkův	k2eAgFnSc1d1	pekelníkův
štrbina	štrbina	k1gFnSc1	štrbina
<g/>
,	,	kIx,	,
Pekelník	pekelník	k1gMnSc1	pekelník
<g/>
,	,	kIx,	,
Čertova	čertův	k2eAgFnSc1d1	Čertova
štrbina	štrbina	k1gFnSc1	štrbina
a	a	k8xC	a
Čertov	Čertov	k1gInSc1	Čertov
hrb	hrb	k1gInSc1	hrb
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Několik	několik	k4yIc4	několik
horolezeckých	horolezecký	k2eAgInPc2d1	horolezecký
výstupů	výstup	k1gInPc2	výstup
==	==	k?	==
</s>
</p>
<p>
<s>
1907	[number]	k4	1907
–	–	k?	–
první	první	k4xOgInSc4	první
výstup	výstup	k1gInSc4	výstup
z	z	k7c2	z
Diablova	Diablův	k2eAgNnSc2d1	Diablův
sedla	sedlo	k1gNnSc2	sedlo
přes	přes	k7c4	přes
Zlatinskou	Zlatinský	k2eAgFnSc4d1	Zlatinský
věž	věž	k1gFnSc4	věž
G.	G.	kA	G.
O.	O.	kA	O.
Dyhrenfurth	Dyhrenfurth	k1gInSc4	Dyhrenfurth
a	a	k8xC	a
H.	H.	kA	H.
Rumpelův	Rumpelův	k2eAgMnSc1d1	Rumpelův
<g/>
,	,	kIx,	,
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
na	na	k7c6	na
Pekelník	pekelník	k1gMnSc1	pekelník
<g/>
,	,	kIx,	,
Čertův	čertův	k2eAgInSc1d1	čertův
hrb	hrb	k1gInSc1	hrb
a	a	k8xC	a
dolů	dolů	k6eAd1	dolů
do	do	k7c2	do
Satanova	Satanův	k2eAgNnSc2d1	Satanův
sedla	sedlo	k1gNnSc2	sedlo
<g/>
,	,	kIx,	,
I.	I.	kA	I.
</s>
</p>
<p>
<s>
1914	[number]	k4	1914
–	–	k?	–
první	první	k4xOgMnSc1	první
zimní	zimní	k2eAgInSc1d1	zimní
výstup	výstup	k1gInSc1	výstup
A.	A.	kA	A.
Grósz	Grósz	k1gInSc1	Grósz
a	a	k8xC	a
Z.	Z.	kA	Z.
Neupauer	Neupauer	k1gInSc1	Neupauer
<g/>
,	,	kIx,	,
cestou	cestou	k7c2	cestou
prvního	první	k4xOgInSc2	první
výstupu	výstup	k1gInSc2	výstup
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1935	[number]	k4	1935
–	–	k?	–
prvovýstup	prvovýstup	k1gInSc1	prvovýstup
na	na	k7c4	na
Zlatinskou	Zlatinský	k2eAgFnSc4d1	Zlatinský
věž	věž	k1gFnSc4	věž
"	"	kIx"	"
<g/>
Motykova	Motykův	k2eAgFnSc1d1	Motykův
cesta	cesta	k1gFnSc1	cesta
<g/>
"	"	kIx"	"
A.	A.	kA	A.
a	a	k8xC	a
M.	M.	kA	M.
Bacsányiové	Bacsányius	k1gMnPc1	Bacsányius
<g/>
,	,	kIx,	,
Z.	Z.	kA	Z.
Brull	Brull	k1gInSc1	Brull
<g/>
,	,	kIx,	,
W.	W.	kA	W.
Dobrucký	Dobrucký	k1gMnSc1	Dobrucký
<g/>
,	,	kIx,	,
S.	S.	kA	S.
Motyka	motyka	k1gFnSc1	motyka
a	a	k8xC	a
K.	K.	kA	K.
Mrózek	Mrózek	k1gInSc1	Mrózek
<g/>
,	,	kIx,	,
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1977	[number]	k4	1977
–	–	k?	–
prvovýstup	prvovýstup	k1gInSc1	prvovýstup
středem	střed	k1gInSc7	střed
východní	východní	k2eAgFnSc2d1	východní
stěny	stěna	k1gFnSc2	stěna
Diabloviny	Diablovina	k1gFnSc2	Diablovina
A.	A.	kA	A.
Bieluń	Bieluń	k1gMnSc7	Bieluń
a	a	k8xC	a
J.	J.	kA	J.
Tillak	Tillak	k1gInSc1	Tillak
<g/>
,	,	kIx,	,
pevná	pevný	k2eAgFnSc1d1	pevná
deskovitá	deskovitý	k2eAgFnSc1d1	deskovitá
skála	skála	k1gFnSc1	skála
<g/>
,	,	kIx,	,
V	V	kA	V
+	+	kIx~	+
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pověst	pověst	k1gFnSc1	pověst
==	==	k?	==
</s>
</p>
<p>
<s>
Mladý	mladý	k2eAgMnSc1d1	mladý
horník	horník	k1gMnSc1	horník
Stacho	Stacha	k1gFnSc5	Stacha
z	z	k7c2	z
Mengusovců	Mengusovec	k1gMnPc2	Mengusovec
našel	najít	k5eAaPmAgMnS	najít
zlato	zlato	k1gNnSc4	zlato
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Hincova	Hincův	k2eAgNnSc2d1	Hincovo
plesa	pleso	k1gNnSc2	pleso
a	a	k8xC	a
přestože	přestože	k8xS	přestože
ho	on	k3xPp3gInSc4	on
satan	satan	k1gInSc4	satan
sídlící	sídlící	k2eAgInSc4d1	sídlící
v	v	k7c6	v
nejvyšším	vysoký	k2eAgInSc6d3	Nejvyšší
štítě	štít	k1gInSc6	štít
třikrát	třikrát	k6eAd1	třikrát
varoval	varovat	k5eAaImAgMnS	varovat
<g/>
,	,	kIx,	,
nepřestával	přestávat	k5eNaImAgMnS	přestávat
kutat	kutat	k5eAaImF	kutat
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
se	se	k3xPyFc4	se
zatřásla	zatřást	k5eAaPmAgFnS	zatřást
<g/>
,	,	kIx,	,
z	z	k7c2	z
blízkého	blízký	k2eAgInSc2d1	blízký
štítu	štít	k1gInSc2	štít
spadly	spadnout	k5eAaPmAgFnP	spadnout
dvě	dva	k4xCgFnPc1	dva
skalní	skalní	k2eAgFnPc1d1	skalní
laviny	lavina	k1gFnPc1	lavina
<g/>
,	,	kIx,	,
zahubily	zahubit	k5eAaPmAgInP	zahubit
ho	on	k3xPp3gInSc4	on
a	a	k8xC	a
vyvalily	vyvalit	k5eAaPmAgFnP	vyvalit
se	se	k3xPyFc4	se
až	až	k9	až
do	do	k7c2	do
protisvahu	protisvah	k1gInSc2	protisvah
<g/>
.	.	kIx.	.
</s>
<s>
Zlato	zlato	k1gNnSc1	zlato
se	se	k3xPyFc4	se
ukrylo	ukrýt	k5eAaPmAgNnS	ukrýt
v	v	k7c6	v
Zlatinské	Zlatinský	k2eAgFnSc6d1	Zlatinský
veži	vež	k1gFnSc6	vež
a	a	k8xC	a
podle	podle	k7c2	podle
satanových	satanův	k2eAgMnPc2d1	satanův
služebníků	služebník	k1gMnPc2	služebník
byly	být	k5eAaImAgFnP	být
pojmenovány	pojmenován	k2eAgFnPc1d1	pojmenována
sousední	sousední	k2eAgFnPc1d1	sousední
věže	věž	k1gFnPc1	věž
-	-	kIx~	-
Diablovina	Diablovina	k1gFnSc1	Diablovina
<g/>
,	,	kIx,	,
Pekelník	pekelník	k1gMnSc1	pekelník
<g/>
,	,	kIx,	,
Čertov	Čertov	k1gInSc1	Čertov
hrb	hrb	k1gInSc1	hrb
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Diablovina	Diablovina	k1gFnSc1	Diablovina
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
A.	A.	kA	A.
Puškáš	Puškáš	k1gFnSc1	Puškáš
<g/>
,	,	kIx,	,
Vysoké	vysoký	k2eAgFnSc2d1	vysoká
Tatry	Tatra	k1gFnSc2	Tatra
-	-	kIx~	-
horolezecký	horolezecký	k2eAgMnSc1d1	horolezecký
sprievodca	sprievodca	k1gMnSc1	sprievodca
<g/>
,	,	kIx,	,
Monografie	monografie	k1gFnSc1	monografie
<g/>
,	,	kIx,	,
VIII	VIII	kA	VIII
diel	diel	k1gInSc1	diel
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
A.	A.	kA	A.
Marec	Marec	k1gInSc1	Marec
<g/>
,	,	kIx,	,
Zlato	zlato	k1gNnSc1	zlato
pod	pod	k7c4	pod
Kriváňom	Kriváňom	k1gInSc4	Kriváňom
<g/>
,	,	kIx,	,
Mladé	mladý	k2eAgFnPc4d1	mladá
Letá	letý	k2eAgNnPc4d1	leté
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
mapě	mapa	k1gFnSc6	mapa
Mlynické	Mlynický	k2eAgFnSc2d1	Mlynická
doliny	dolina	k1gFnSc2	dolina
s	s	k7c7	s
nesprávným	správný	k2eNgInSc7d1	nesprávný
názvem	název	k1gInSc7	název
Zlatavá	zlatavý	k2eAgFnSc1d1	zlatavá
namísto	namísto	k7c2	namísto
Zlatinská	Zlatinský	k2eAgFnSc1d1	Zlatinský
(	(	kIx(	(
<g/>
věž	věž	k1gFnSc1	věž
<g/>
,	,	kIx,	,
štěrbina	štěrbina	k1gFnSc1	štěrbina
<g/>
)	)	kIx)	)
</s>
</p>
