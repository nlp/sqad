<s>
Mistr	mistr	k1gMnSc1	mistr
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
r.	r.	kA	r.
1370	[number]	k4	1370
Husinec	Husinec	k1gInSc1	Husinec
-	-	kIx~	-
6	[number]	k4	6
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1415	[number]	k4	1415
Kostnice	Kostnice	k1gFnSc2	Kostnice
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
římskokatolický	římskokatolický	k2eAgMnSc1d1	římskokatolický
kněz	kněz	k1gMnSc1	kněz
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
středověký	středověký	k2eAgMnSc1d1	středověký
náboženský	náboženský	k2eAgMnSc1d1	náboženský
myslitel	myslitel	k1gMnSc1	myslitel
<g/>
,	,	kIx,	,
vysokoškolský	vysokoškolský	k2eAgMnSc1d1	vysokoškolský
pedagog	pedagog	k1gMnSc1	pedagog
<g/>
,	,	kIx,	,
reformátor	reformátor	k1gMnSc1	reformátor
a	a	k8xC	a
kazatel	kazatel	k1gMnSc1	kazatel
<g/>
.	.	kIx.	.
</s>
<s>
Hus	Hus	k1gMnSc1	Hus
byl	být	k5eAaImAgMnS	být
<g/>
,	,	kIx,	,
po	po	k7c6	po
Johnu	John	k1gMnSc6	John
Wycliffovi	Wycliff	k1gMnSc6	Wycliff
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnPc7	jehož
myšlenkami	myšlenka	k1gFnPc7	myšlenka
a	a	k8xC	a
argumentací	argumentace	k1gFnSc7	argumentace
byl	být	k5eAaImAgMnS	být
inspirován	inspirovat	k5eAaBmNgMnS	inspirovat
<g/>
,	,	kIx,	,
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
reformátorů	reformátor	k1gMnPc2	reformátor
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
téměř	téměř	k6eAd1	téměř
o	o	k7c4	o
jedno	jeden	k4xCgNnSc4	jeden
století	století	k1gNnSc4	století
předběhl	předběhnout	k5eAaPmAgMnS	předběhnout
své	svůj	k3xOyFgMnPc4	svůj
následníky	následník	k1gMnPc4	následník
-	-	kIx~	-
reformátory	reformátor	k1gMnPc4	reformátor
Luthera	Luther	k1gMnSc2	Luther
<g/>
,	,	kIx,	,
Kalvína	Kalvín	k1gMnSc2	Kalvín
a	a	k8xC	a
Zwingliho	Zwingli	k1gMnSc2	Zwingli
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1398	[number]	k4	1398
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
na	na	k7c6	na
pražské	pražský	k2eAgFnSc6d1	Pražská
univerzitě	univerzita	k1gFnSc6	univerzita
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1409	[number]	k4	1409
<g/>
-	-	kIx~	-
<g/>
1410	[number]	k4	1410
byl	být	k5eAaImAgMnS	být
jejím	její	k3xOp3gMnSc7	její
rektorem	rektor	k1gMnSc7	rektor
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
náboženských	náboženský	k2eAgFnPc6d1	náboženská
pracích	práce	k1gFnPc6	práce
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
mravní	mravní	k2eAgInSc4d1	mravní
úpadek	úpadek	k1gInSc4	úpadek
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
ocitla	ocitnout	k5eAaPmAgFnS	ocitnout
katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
<g/>
.	.	kIx.	.
</s>
<s>
Katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
ho	on	k3xPp3gMnSc4	on
označila	označit	k5eAaPmAgFnS	označit
za	za	k7c2	za
kacíře	kacíř	k1gMnSc2	kacíř
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
učení	učení	k1gNnSc1	učení
za	za	k7c4	za
herezi	hereze	k1gFnSc4	hereze
a	a	k8xC	a
exkomunikovala	exkomunikovat	k5eAaBmAgFnS	exkomunikovat
jej	on	k3xPp3gMnSc4	on
(	(	kIx(	(
<g/>
1411	[number]	k4	1411
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Římský	římský	k2eAgMnSc1d1	římský
král	král	k1gMnSc1	král
Zikmund	Zikmund	k1gMnSc1	Zikmund
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
mu	on	k3xPp3gMnSc3	on
zaručil	zaručit	k5eAaPmAgMnS	zaručit
bezpečný	bezpečný	k2eAgInSc4d1	bezpečný
příchod	příchod	k1gInSc4	příchod
na	na	k7c4	na
kostnický	kostnický	k2eAgInSc4d1	kostnický
koncil	koncil	k1gInSc4	koncil
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
odsouzen	odsouzet	k5eAaImNgMnS	odsouzet
jako	jako	k8xC	jako
kacíř	kacíř	k1gMnSc1	kacíř
<g/>
,	,	kIx,	,
a	a	k8xC	a
následně	následně	k6eAd1	následně
byl	být	k5eAaImAgInS	být
vydán	vydán	k2eAgInSc1d1	vydán
světské	světský	k2eAgFnSc3d1	světská
moci	moc	k1gFnSc3	moc
k	k	k7c3	k
upálení	upálení	k1gNnSc3	upálení
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
<g/>
,	,	kIx,	,
když	když	k8xS	když
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
odvolat	odvolat	k5eAaPmF	odvolat
své	svůj	k3xOyFgNnSc4	svůj
učení	učení	k1gNnSc4	učení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
papež	papež	k1gMnSc1	papež
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
že	že	k8xS	že
lituje	litovat	k5eAaImIp3nS	litovat
kruté	krutý	k2eAgFnSc3d1	krutá
smrti	smrt	k1gFnSc3	smrt
Jana	Jan	k1gMnSc2	Jan
Husa	Hus	k1gMnSc2	Hus
i	i	k8xC	i
následných	následný	k2eAgFnPc2d1	následná
obětí	oběť	k1gFnPc2	oběť
a	a	k8xC	a
rozdělení	rozdělení	k1gNnSc2	rozdělení
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
národě	národ	k1gInSc6	národ
a	a	k8xC	a
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
k	k	k7c3	k
usmíření	usmíření	k1gNnSc3	usmíření
a	a	k8xC	a
historickému	historický	k2eAgNnSc3d1	historické
hodnocení	hodnocení	k1gNnSc3	hodnocení
jeho	jeho	k3xOp3gFnSc2	jeho
osobnosti	osobnost	k1gFnSc2	osobnost
bez	bez	k7c2	bez
předsudků	předsudek	k1gInPc2	předsudek
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
obecně	obecně	k6eAd1	obecně
rozšířené	rozšířený	k2eAgNnSc4d1	rozšířené
mínění	mínění	k1gNnSc4	mínění
však	však	k9	však
Jana	Jan	k1gMnSc4	Jan
Husa	Hus	k1gMnSc4	Hus
explicitně	explicitně	k6eAd1	explicitně
za	za	k7c2	za
reformátora	reformátor	k1gMnSc2	reformátor
církve	církev	k1gFnSc2	církev
neprohlásil	prohlásit	k5eNaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
konstatoval	konstatovat	k5eAaBmAgMnS	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Husovská	husovský	k2eAgFnSc1d1	Husovská
komise	komise	k1gFnSc1	komise
<g/>
"	"	kIx"	"
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
přesněji	přesně	k6eAd2	přesně
stanovit	stanovit	k5eAaPmF	stanovit
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
mezi	mezi	k7c7	mezi
těmi	ten	k3xDgMnPc7	ten
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
usilovali	usilovat	k5eAaImAgMnP	usilovat
o	o	k7c4	o
reformu	reforma	k1gFnSc4	reforma
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
údajné	údajný	k2eAgInPc1d1	údajný
Husovy	Husův	k2eAgInPc1d1	Husův
názory	názor	k1gInPc1	názor
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
ty	ten	k3xDgMnPc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
kněz	kněz	k1gMnSc1	kněz
ve	v	k7c6	v
smrtelném	smrtelný	k2eAgInSc6d1	smrtelný
hříchu	hřích	k1gInSc6	hřích
nehodně	nehodně	k?	nehodně
udílí	udílet	k5eAaImIp3nS	udílet
svátosti	svátost	k1gFnSc3	svátost
nebo	nebo	k8xC	nebo
že	že	k8xS	že
církev	církev	k1gFnSc1	církev
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
výhradně	výhradně	k6eAd1	výhradně
předurčenými	předurčený	k2eAgInPc7d1	předurčený
ke	k	k7c3	k
spáse	spása	k1gFnSc3	spása
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
katolickou	katolický	k2eAgFnSc7d1	katolická
církví	církev	k1gFnSc7	církev
hodnoceny	hodnocen	k2eAgFnPc1d1	hodnocena
jako	jako	k8xC	jako
teologicky	teologicky	k6eAd1	teologicky
vadné	vadný	k2eAgFnPc1d1	vadná
nebo	nebo	k8xC	nebo
přinejmenším	přinejmenším	k6eAd1	přinejmenším
kontroverzní	kontroverzní	k2eAgMnSc1d1	kontroverzní
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jeho	jeho	k3xOp3gInSc3	jeho
odkazu	odkaz	k1gInSc3	odkaz
se	se	k3xPyFc4	se
hlásili	hlásit	k5eAaImAgMnP	hlásit
husité	husita	k1gMnPc1	husita
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
další	další	k2eAgFnPc1d1	další
církve	církev	k1gFnPc1	církev
a	a	k8xC	a
společnosti	společnost	k1gFnPc1	společnost
vzešlé	vzešlý	k2eAgFnPc1d1	vzešlá
z	z	k7c2	z
české	český	k2eAgFnSc2d1	Česká
i	i	k8xC	i
protestantské	protestantský	k2eAgFnSc2d1	protestantská
reformace	reformace	k1gFnSc2	reformace
<g/>
.	.	kIx.	.
</s>
<s>
Husité	husita	k1gMnPc1	husita
zahrnovali	zahrnovat	k5eAaImAgMnP	zahrnovat
většinu	většina	k1gFnSc4	většina
české	český	k2eAgFnSc2d1	Česká
populace	populace	k1gFnSc2	populace
v	v	k7c6	v
Českém	český	k2eAgNnSc6d1	české
království	království	k1gNnSc6	království
<g/>
,	,	kIx,	,
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
tak	tak	k6eAd1	tak
velkou	velký	k2eAgFnSc4d1	velká
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
sílu	síla	k1gFnSc4	síla
a	a	k8xC	a
během	během	k7c2	během
tzv.	tzv.	kA	tzv.
Husitských	husitský	k2eAgFnPc2d1	husitská
válek	válka	k1gFnPc2	válka
porazili	porazit	k5eAaPmAgMnP	porazit
několik	několik	k4yIc1	několik
křížových	křížový	k2eAgFnPc2d1	křížová
výprav	výprava	k1gFnPc2	výprava
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
století	století	k1gNnSc6	století
později	pozdě	k6eAd2	pozdě
bylo	být	k5eAaImAgNnS	být
více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
90	[number]	k4	90
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
nekatolických	katolický	k2eNgFnPc2d1	nekatolická
a	a	k8xC	a
někteří	některý	k3yIgMnPc1	některý
stále	stále	k6eAd1	stále
následovali	následovat	k5eAaImAgMnP	následovat
učení	učení	k1gNnSc4	učení
Jana	Jan	k1gMnSc2	Jan
Husa	Hus	k1gMnSc2	Hus
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc2	jeho
nástupců	nástupce	k1gMnPc2	nástupce
<g/>
.	.	kIx.	.
</s>
<s>
Datum	datum	k1gNnSc1	datum
jeho	on	k3xPp3gNnSc2	on
upálení	upálení	k1gNnSc2	upálení
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
českým	český	k2eAgInSc7d1	český
státním	státní	k2eAgInSc7d1	státní
svátkem	svátek	k1gInSc7	svátek
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
svého	svůj	k3xOyFgMnSc2	svůj
svatého	svatý	k2eAgMnSc2d1	svatý
mučedníka	mučedník	k1gMnSc2	mučedník
jej	on	k3xPp3gNnSc4	on
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
se	se	k3xPyFc4	se
sv.	sv.	kA	sv.
mučedníkem	mučedník	k1gMnSc7	mučedník
Jeronýmem	Jeroným	k1gMnSc7	Jeroným
Pražským	pražský	k2eAgInPc3d1	pražský
<g/>
)	)	kIx)	)
uctívá	uctívat	k5eAaImIp3nS	uctívat
česká	český	k2eAgFnSc1d1	Česká
starokatolická	starokatolický	k2eAgFnSc1d1	Starokatolická
církev	církev	k1gFnSc1	církev
a	a	k8xC	a
pravoslavná	pravoslavný	k2eAgFnSc1d1	pravoslavná
církev	církev	k1gFnSc1	církev
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
vyhlášení	vyhlášení	k1gNnSc2	vyhlášení
metropolity	metropolita	k1gMnSc2	metropolita
Kryštofa	Kryštof	k1gMnSc2	Kryštof
<g/>
,	,	kIx,	,
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
pražského	pražský	k2eAgMnSc2d1	pražský
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Církev	církev	k1gFnSc4	církev
československou	československý	k2eAgFnSc7d1	Československá
husitskou	husitský	k2eAgFnSc7d1	husitská
je	být	k5eAaImIp3nS	být
výročí	výročí	k1gNnSc3	výročí
jeho	jeho	k3xOp3gFnSc2	jeho
smrti	smrt	k1gFnSc2	smrt
fakticky	fakticky	k6eAd1	fakticky
největším	veliký	k2eAgInSc7d3	veliký
svátkem	svátek	k1gInSc7	svátek
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1370	[number]	k4	1370
v	v	k7c6	v
Husinci	Husinec	k1gInSc6	Husinec
u	u	k7c2	u
Prachatic	Prachatice	k1gFnPc2	Prachatice
v	v	k7c6	v
chudé	chudý	k2eAgFnSc6d1	chudá
poddanské	poddanský	k2eAgFnSc6d1	poddanská
rodině	rodina	k1gFnSc6	rodina
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
jeho	jeho	k3xOp3gMnPc6	jeho
rodičích	rodič	k1gMnPc6	rodič
není	být	k5eNaImIp3nS	být
nic	nic	k3yNnSc1	nic
známo	znám	k2eAgNnSc1d1	známo
<g/>
.	.	kIx.	.
</s>
<s>
Hus	Hus	k1gMnSc1	Hus
měl	mít	k5eAaImAgMnS	mít
zřejmě	zřejmě	k6eAd1	zřejmě
jen	jen	k9	jen
jednoho	jeden	k4xCgMnSc4	jeden
sourozence	sourozenec	k1gMnSc4	sourozenec
<g/>
,	,	kIx,	,
staršího	starý	k2eAgMnSc4d2	starší
bratra	bratr	k1gMnSc4	bratr
<g/>
.	.	kIx.	.
</s>
<s>
Učit	učit	k5eAaImF	učit
se	se	k3xPyFc4	se
číst	číst	k5eAaImF	číst
<g/>
,	,	kIx,	,
psát	psát	k5eAaImF	psát
<g/>
,	,	kIx,	,
počítat	počítat	k5eAaImF	počítat
a	a	k8xC	a
osvojit	osvojit	k5eAaPmF	osvojit
si	se	k3xPyFc3	se
základy	základ	k1gInPc4	základ
latiny	latina	k1gFnSc2	latina
bylo	být	k5eAaImAgNnS	být
tehdy	tehdy	k6eAd1	tehdy
možné	možný	k2eAgNnSc1d1	možné
jen	jen	k9	jen
na	na	k7c6	na
farních	farní	k2eAgFnPc6d1	farní
školách	škola	k1gFnPc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
Taková	takový	k3xDgFnSc1	takový
byla	být	k5eAaImAgFnS	být
i	i	k9	i
v	v	k7c6	v
nedalekých	daleký	k2eNgFnPc6d1	nedaleká
Prachaticích	Prachatice	k1gFnPc6	Prachatice
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
však	však	k9	však
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
Hus	Hus	k1gMnSc1	Hus
získal	získat	k5eAaPmAgMnS	získat
vzdělání	vzdělání	k1gNnSc4	vzdělání
právě	právě	k6eAd1	právě
zde	zde	k6eAd1	zde
<g/>
.	.	kIx.	.
</s>
<s>
Hus	Hus	k1gMnSc1	Hus
se	se	k3xPyFc4	se
musel	muset	k5eAaImAgMnS	muset
učit	učit	k5eAaImF	učit
nazpaměť	nazpaměť	k6eAd1	nazpaměť
morální	morální	k2eAgNnPc1d1	morální
pravidla	pravidlo	k1gNnPc1	pravidlo
<g/>
,	,	kIx,	,
náboženská	náboženský	k2eAgNnPc1d1	náboženské
pravidla	pravidlo	k1gNnPc1	pravidlo
<g/>
,	,	kIx,	,
evangelia	evangelium	k1gNnPc1	evangelium
o	o	k7c6	o
Ježíšově	Ježíšův	k2eAgNnSc6d1	Ježíšovo
dětství	dětství	k1gNnSc6	dětství
atd.	atd.	kA	atd.
Když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
asi	asi	k9	asi
16	[number]	k4	16
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
odešel	odejít	k5eAaPmAgMnS	odejít
studovat	studovat	k5eAaImF	studovat
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
na	na	k7c4	na
Karlovu	Karlův	k2eAgFnSc4d1	Karlova
univerzitu	univerzita	k1gFnSc4	univerzita
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
jej	on	k3xPp3gInSc4	on
pozval	pozvat	k5eAaPmAgMnS	pozvat
jeho	jeho	k3xOp3gMnSc1	jeho
přítel	přítel	k1gMnSc1	přítel
Křišťan	Křišťan	k1gMnSc1	Křišťan
z	z	k7c2	z
Prachatic	Prachatice	k1gFnPc2	Prachatice
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
student	student	k1gMnSc1	student
si	se	k3xPyFc3	se
také	také	k9	také
musel	muset	k5eAaImAgMnS	muset
vydělávat	vydělávat	k5eAaImF	vydělávat
na	na	k7c4	na
živobytí	živobytí	k1gNnSc4	živobytí
(	(	kIx(	(
<g/>
např.	např.	kA	např.
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zpíval	zpívat	k5eAaImAgMnS	zpívat
v	v	k7c6	v
chóru	chór	k1gInSc6	chór
při	při	k7c6	při
různých	různý	k2eAgFnPc6d1	různá
pobožnostech	pobožnost	k1gFnPc6	pobožnost
nebo	nebo	k8xC	nebo
sloužil	sloužit	k5eAaImAgInS	sloužit
u	u	k7c2	u
některého	některý	k3yIgMnSc2	některý
z	z	k7c2	z
profesorů	profesor	k1gMnPc2	profesor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hus	Hus	k1gMnSc1	Hus
sice	sice	k8xC	sice
zpočátku	zpočátku	k6eAd1	zpočátku
toužil	toužit	k5eAaImAgInS	toužit
především	především	k9	především
po	po	k7c6	po
materiálním	materiální	k2eAgNnSc6d1	materiální
zabezpečení	zabezpečení	k1gNnSc6	zabezpečení
(	(	kIx(	(
<g/>
tehdejší	tehdejší	k2eAgFnSc7d1	tehdejší
stravou	strava	k1gFnSc7	strava
studentů	student	k1gMnPc2	student
byl	být	k5eAaImAgInS	být
chleba	chléb	k1gInSc2	chléb
s	s	k7c7	s
hrachem	hrách	k1gInSc7	hrách
nebo	nebo	k8xC	nebo
zelím	zelí	k1gNnSc7	zelí
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
voda	voda	k1gFnSc1	voda
nebo	nebo	k8xC	nebo
špatné	špatný	k2eAgNnSc1d1	špatné
pivo	pivo	k1gNnSc1	pivo
<g/>
)	)	kIx)	)
a	a	k8xC	a
váženosti	váženost	k1gFnPc1	váženost
mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
také	také	k9	také
pilně	pilně	k6eAd1	pilně
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c4	na
kněze	kněz	k1gMnPc4	kněz
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
však	však	k9	však
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
Kristem	Kristus	k1gMnSc7	Kristus
(	(	kIx(	(
<g/>
zakusil	zakusit	k5eAaPmAgInS	zakusit
sílu	síla	k1gFnSc4	síla
Boží	boží	k2eAgFnSc2d1	boží
lásky	láska	k1gFnSc2	láska
a	a	k8xC	a
"	"	kIx"	"
<g/>
obrátil	obrátit	k5eAaPmAgMnS	obrátit
se	se	k3xPyFc4	se
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
hledat	hledat	k5eAaImF	hledat
skutečnou	skutečný	k2eAgFnSc4d1	skutečná
lásku	láska	k1gFnSc4	láska
a	a	k8xC	a
pravdu	pravda	k1gFnSc4	pravda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vedla	vést	k5eAaImAgFnS	vést
i	i	k9	i
k	k	k7c3	k
proměně	proměna	k1gFnSc3	proměna
a	a	k8xC	a
naplnění	naplnění	k1gNnSc4	naplnění
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
jeho	jeho	k3xOp3gInSc2	jeho
života	život	k1gInSc2	život
a	a	k8xC	a
charakteru	charakter	k1gInSc2	charakter
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
ostatních	ostatní	k2eAgMnPc2d1	ostatní
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Hus	Hus	k1gMnSc1	Hus
byl	být	k5eAaImAgMnS	být
taky	taky	k6eAd1	taky
hudebně	hudebně	k6eAd1	hudebně
nadaný	nadaný	k2eAgMnSc1d1	nadaný
a	a	k8xC	a
skládal	skládat	k5eAaImAgMnS	skládat
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
spíše	spíše	k9	spíše
upravoval	upravovat	k5eAaImAgMnS	upravovat
<g/>
,	,	kIx,	,
písně	píseň	k1gFnPc1	píseň
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
lidem	lid	k1gInSc7	lid
zpívaly	zpívat	k5eAaImAgInP	zpívat
co	co	k9	co
nejlépe	dobře	k6eAd3	dobře
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
artistické	artistický	k2eAgFnSc6d1	artistická
fakultě	fakulta	k1gFnSc6	fakulta
získal	získat	k5eAaPmAgMnS	získat
roku	rok	k1gInSc2	rok
1393	[number]	k4	1393
hodnost	hodnost	k1gFnSc1	hodnost
bakaláře	bakalář	k1gMnSc2	bakalář
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1396	[number]	k4	1396
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgInS	být
propůjčen	propůjčen	k2eAgInSc4d1	propůjčen
titul	titul	k1gInSc4	titul
mistr	mistr	k1gMnSc1	mistr
svobodných	svobodný	k2eAgInPc2d1	svobodný
uměnía	uměnía	k6eAd1	uměnía
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
získal	získat	k5eAaPmAgInS	získat
bakalářský	bakalářský	k2eAgInSc1d1	bakalářský
grad	grad	k1gInSc1	grad
i	i	k9	i
na	na	k7c6	na
bohoslovecké	bohoslovecký	k2eAgFnSc6d1	bohoslovecká
fakultě	fakulta	k1gFnSc6	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
1398	[number]	k4	1398
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
jako	jako	k9	jako
magister	magister	k1gMnSc1	magister
actu	actus	k1gInSc2	actus
regens	regens	k1gMnSc1	regens
stal	stát	k5eAaPmAgMnS	stát
plnoprávným	plnoprávný	k2eAgMnSc7d1	plnoprávný
členem	člen	k1gMnSc7	člen
akademické	akademický	k2eAgFnSc2d1	akademická
obce	obec	k1gFnSc2	obec
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ho	on	k3xPp3gMnSc4	on
opravňovalo	opravňovat	k5eAaImAgNnS	opravňovat
k	k	k7c3	k
přednášení	přednášení	k1gNnSc3	přednášení
<g/>
,	,	kIx,	,
vedení	vedení	k1gNnSc3	vedení
disputací	disputace	k1gFnPc2	disputace
a	a	k8xC	a
přípravě	příprava	k1gFnSc3	příprava
dalších	další	k2eAgMnPc2d1	další
studentů	student	k1gMnPc2	student
na	na	k7c4	na
promoce	promoce	k1gFnPc4	promoce
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
15	[number]	k4	15
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1401	[number]	k4	1401
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
děkanem	děkan	k1gMnSc7	děkan
artistické	artistický	k2eAgFnSc2d1	artistická
fakulty	fakulta	k1gFnSc2	fakulta
na	na	k7c4	na
zimní	zimní	k2eAgInSc4d1	zimní
semestr	semestr	k1gInSc4	semestr
1401	[number]	k4	1401
<g/>
-	-	kIx~	-
<g/>
1402	[number]	k4	1402
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1409	[number]	k4	1409
se	se	k3xPyFc4	se
po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
většiny	většina	k1gFnSc2	většina
učitelů	učitel	k1gMnPc2	učitel
a	a	k8xC	a
žáku	žák	k1gMnSc3	žák
do	do	k7c2	do
Lipska	Lipsko	k1gNnSc2	Lipsko
stal	stát	k5eAaPmAgMnS	stát
dokonce	dokonce	k9	dokonce
rektorem	rektor	k1gMnSc7	rektor
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Souběžně	souběžně	k6eAd1	souběžně
s	s	k7c7	s
působením	působení	k1gNnSc7	působení
na	na	k7c6	na
pražské	pražský	k2eAgFnSc6d1	Pražská
univerzitě	univerzita	k1gFnSc6	univerzita
se	se	k3xPyFc4	se
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
začal	začít	k5eAaPmAgMnS	začít
věnovat	věnovat	k5eAaImF	věnovat
i	i	k9	i
dráze	dráha	k1gFnSc3	dráha
duchovního	duchovní	k1gMnSc2	duchovní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1400	[number]	k4	1400
byl	být	k5eAaImAgInS	být
vysvěcen	vysvětit	k5eAaPmNgInS	vysvětit
na	na	k7c4	na
kněze	kněz	k1gMnPc4	kněz
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
kázat	kázat	k5eAaImF	kázat
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
sv.	sv.	kA	sv.
Michala	Michal	k1gMnSc2	Michal
na	na	k7c6	na
Starém	starý	k2eAgNnSc6d1	staré
Městě	město	k1gNnSc6	město
pražském	pražský	k2eAgNnSc6d1	Pražské
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
si	se	k3xPyFc3	se
získal	získat	k5eAaPmAgMnS	získat
značnou	značný	k2eAgFnSc4d1	značná
oblibu	obliba	k1gFnSc4	obliba
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
14	[number]	k4	14
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1402	[number]	k4	1402
povolán	povolat	k5eAaPmNgInS	povolat
do	do	k7c2	do
Betlémské	betlémský	k2eAgFnSc2d1	Betlémská
kaple	kaple	k1gFnSc2	kaple
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
poté	poté	k6eAd1	poté
působil	působit	k5eAaImAgMnS	působit
i	i	k8xC	i
jako	jako	k8xS	jako
její	její	k3xOp3gMnSc1	její
správce	správce	k1gMnSc1	správce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kázáních	kázání	k1gNnPc6	kázání
se	se	k3xPyFc4	se
jako	jako	k9	jako
mnozí	mnohý	k2eAgMnPc1d1	mnohý
jiní	jiný	k2eAgMnPc1d1	jiný
duchovní	duchovní	k1gMnPc1	duchovní
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
věnoval	věnovat	k5eAaPmAgMnS	věnovat
i	i	k9	i
kritice	kritika	k1gFnSc3	kritika
církve	církev	k1gFnSc2	církev
a	a	k8xC	a
nabádal	nabádat	k5eAaBmAgInS	nabádat
k	k	k7c3	k
její	její	k3xOp3gFnSc3	její
reformě	reforma	k1gFnSc3	reforma
<g/>
.	.	kIx.	.
</s>
<s>
Inspiroval	inspirovat	k5eAaBmAgInS	inspirovat
se	se	k3xPyFc4	se
učením	učení	k1gNnSc7	učení
Johna	John	k1gMnSc2	John
Wycliffa	Wycliff	k1gMnSc2	Wycliff
<g/>
.	.	kIx.	.
</s>
<s>
Národnostní	národnostní	k2eAgFnPc1d1	národnostní
neshody	neshoda	k1gFnPc1	neshoda
na	na	k7c6	na
Karlově	Karlův	k2eAgFnSc6d1	Karlova
univerzitě	univerzita	k1gFnSc6	univerzita
se	se	k3xPyFc4	se
stupňovaly	stupňovat	k5eAaImAgFnP	stupňovat
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
kardinálové	kardinál	k1gMnPc1	kardinál
obou	dva	k4xCgFnPc2	dva
obediencí	obedience	k1gFnPc2	obedience
(	(	kIx(	(
<g/>
západní	západní	k2eAgFnSc1d1	západní
církev	církev	k1gFnSc1	církev
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
době	doba	k1gFnSc6	doba
dvojpapežství	dvojpapežství	k1gNnPc2	dvojpapežství
<g/>
)	)	kIx)	)
shodli	shodnout	k5eAaBmAgMnP	shodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
rozkol	rozkol	k1gInSc1	rozkol
odstraní	odstranit	k5eAaPmIp3nS	odstranit
volbou	volba	k1gFnSc7	volba
nového	nový	k2eAgMnSc4d1	nový
papeže	papež	k1gMnSc4	papež
na	na	k7c6	na
koncilu	koncil	k1gInSc6	koncil
v	v	k7c6	v
Pise	Pisa	k1gFnSc6	Pisa
<g/>
,	,	kIx,	,
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
král	král	k1gMnSc1	král
Václav	Václav	k1gMnSc1	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
univerzitu	univerzita	k1gFnSc4	univerzita
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
k	k	k7c3	k
účasti	účast	k1gFnSc3	účast
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
koncilu	koncil	k1gInSc6	koncil
vyjádřila	vyjádřit	k5eAaPmAgFnS	vyjádřit
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
koncil	koncil	k1gInSc1	koncil
měl	mít	k5eAaImAgInS	mít
kromě	kromě	k7c2	kromě
zmíněného	zmíněný	k2eAgMnSc2d1	zmíněný
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
bylo	být	k5eAaImAgNnS	být
neformálně	formálně	k6eNd1	formálně
přislíbeno	přislíbit	k5eAaPmNgNnS	přislíbit
<g/>
,	,	kIx,	,
potvrdit	potvrdit	k5eAaPmF	potvrdit
králi	král	k1gMnSc3	král
Václavu	Václav	k1gMnSc3	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
jeho	jeho	k3xOp3gInPc4	jeho
nároky	nárok	k1gInPc4	nárok
na	na	k7c4	na
titul	titul	k1gInSc4	titul
římského	římský	k2eAgMnSc2d1	římský
krále	král	k1gMnSc2	král
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc1	jehož
byl	být	k5eAaImAgInS	být
zbaven	zbavit	k5eAaPmNgMnS	zbavit
kurfiřty	kurfiřt	k1gMnPc7	kurfiřt
roku	rok	k1gInSc2	rok
1400	[number]	k4	1400
<g/>
,	,	kIx,	,
kteréžto	kteréžto	k?	kteréžto
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
potvrdil	potvrdit	k5eAaPmAgInS	potvrdit
papež	papež	k1gMnSc1	papež
Bonifác	Bonifác	k1gMnSc1	Bonifác
IX	IX	kA	IX
<g/>
.	.	kIx.	.
</s>
<s>
Arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
Zbyněk	Zbyněk	k1gMnSc1	Zbyněk
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
římský	římský	k2eAgMnSc1d1	římský
král	král	k1gMnSc1	král
Ruprecht	Ruprecht	k1gMnSc1	Ruprecht
Falcký	falcký	k2eAgMnSc1d1	falcký
zůstávali	zůstávat	k5eAaImAgMnP	zůstávat
v	v	k7c6	v
obedienci	obedience	k1gFnSc6	obedience
Řehoře	Řehoř	k1gMnSc2	Řehoř
XII	XII	kA	XII
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
situaci	situace	k1gFnSc6	situace
se	se	k3xPyFc4	se
král	král	k1gMnSc1	král
obrátil	obrátit	k5eAaPmAgMnS	obrátit
na	na	k7c4	na
univerzitu	univerzita	k1gFnSc4	univerzita
<g/>
,	,	kIx,	,
a	a	k8xC	a
aby	aby	kYmCp3nS	aby
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
jejího	její	k3xOp3gInSc2	její
souhlasu	souhlas	k1gInSc2	souhlas
<g/>
,	,	kIx,	,
změnil	změnit	k5eAaPmAgInS	změnit
poměr	poměr	k1gInSc1	poměr
hlasů	hlas	k1gInPc2	hlas
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
pomocí	pomocí	k7c2	pomocí
tzv.	tzv.	kA	tzv.
Dekretu	dekret	k1gInSc2	dekret
kutnohorského	kutnohorský	k2eAgInSc2d1	kutnohorský
z	z	k7c2	z
18	[number]	k4	18
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1409	[number]	k4	1409
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
nyní	nyní	k6eAd1	nyní
český	český	k2eAgInSc1d1	český
národ	národ	k1gInSc1	národ
měl	mít	k5eAaImAgInS	mít
hlasy	hlas	k1gInPc4	hlas
tři	tři	k4xCgFnPc4	tři
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
ostatní	ostatní	k2eAgInPc1d1	ostatní
národy	národ	k1gInPc1	národ
hlas	hlas	k1gInSc4	hlas
jediný	jediný	k2eAgMnSc1d1	jediný
<g/>
.	.	kIx.	.
</s>
<s>
Cizí	cizí	k2eAgMnPc1d1	cizí
studenti	student	k1gMnPc1	student
<g/>
,	,	kIx,	,
podporující	podporující	k2eAgMnPc4d1	podporující
Řehoře	Řehoř	k1gMnPc4	Řehoř
XII	XII	kA	XII
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
přesunuli	přesunout	k5eAaPmAgMnP	přesunout
do	do	k7c2	do
Lipska	Lipsko	k1gNnSc2	Lipsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
založili	založit	k5eAaPmAgMnP	založit
novou	nový	k2eAgFnSc4d1	nová
univerzitu	univerzita	k1gFnSc4	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnPc1d1	Česká
země	zem	k1gFnPc1	zem
přešly	přejít	k5eAaPmAgFnP	přejít
po	po	k7c6	po
pisánské	pisánský	k2eAgFnSc6d1	pisánská
volbě	volba	k1gFnSc6	volba
pod	pod	k7c4	pod
obedienci	obedience	k1gFnSc4	obedience
Alexandra	Alexandr	k1gMnSc2	Alexandr
V.	V.	kA	V.
(	(	kIx(	(
<g/>
se	s	k7c7	s
souhlasem	souhlas	k1gInSc7	souhlas
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pražský	pražský	k2eAgMnSc1d1	pražský
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
Zbyněk	Zbyněk	k1gMnSc1	Zbyněk
Zajíc	Zajíc	k1gMnSc1	Zajíc
z	z	k7c2	z
Hazmburka	Hazmburek	k1gMnSc2	Hazmburek
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
situaci	situace	k1gFnSc6	situace
potlačit	potlačit	k5eAaPmF	potlačit
pronikání	pronikání	k1gNnSc4	pronikání
"	"	kIx"	"
<g/>
viklefských	viklefský	k2eAgInPc2d1	viklefský
bludů	blud	k1gInPc2	blud
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
marně	marně	k6eAd1	marně
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
čeští	český	k2eAgMnPc1d1	český
mistři	mistr	k1gMnPc1	mistr
a	a	k8xC	a
opravné	opravný	k2eAgNnSc1d1	opravné
hnutí	hnutí	k1gNnSc1	hnutí
se	se	k3xPyFc4	se
opírali	opírat	k5eAaImAgMnP	opírat
o	o	k7c4	o
královu	králův	k2eAgFnSc4d1	králova
podporu	podpora	k1gFnSc4	podpora
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
situaci	situace	k1gFnSc6	situace
se	se	k3xPyFc4	se
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
obrátil	obrátit	k5eAaPmAgMnS	obrátit
na	na	k7c4	na
Alexandra	Alexandr	k1gMnSc4	Alexandr
V.	V.	kA	V.
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zakázal	zakázat	k5eAaPmAgMnS	zakázat
Viklefovy	Viklefův	k2eAgInPc4d1	Viklefův
spisy	spis	k1gInPc4	spis
a	a	k8xC	a
kázání	kázání	k1gNnSc4	kázání
na	na	k7c6	na
soukromých	soukromý	k2eAgNnPc6d1	soukromé
místech	místo	k1gNnPc6	místo
(	(	kIx(	(
<g/>
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
namířeno	namířit	k5eAaPmNgNnS	namířit
i	i	k9	i
proti	proti	k7c3	proti
Husovu	Husův	k2eAgNnSc3d1	Husovo
kázání	kázání	k1gNnSc3	kázání
v	v	k7c6	v
Betlémské	betlémský	k2eAgFnSc6d1	Betlémská
kapli	kaple	k1gFnSc6	kaple
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
pak	pak	k6eAd1	pak
sám	sám	k3xTgMnSc1	sám
16	[number]	k4	16
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1410	[number]	k4	1410
nařídil	nařídit	k5eAaPmAgMnS	nařídit
spálit	spálit	k5eAaPmF	spálit
Viklefovy	Viklefův	k2eAgFnPc4d1	Viklefova
knihy	kniha	k1gFnPc4	kniha
a	a	k8xC	a
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
později	pozdě	k6eAd2	pozdě
Husa	Husa	k1gMnSc1	Husa
uvrhl	uvrhnout	k5eAaPmAgMnS	uvrhnout
do	do	k7c2	do
klatby	klatba	k1gFnSc2	klatba
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
si	se	k3xPyFc3	se
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
poštval	poštvat	k5eAaPmAgMnS	poštvat
Husovy	Husův	k2eAgMnPc4d1	Husův
stoupence	stoupenec	k1gMnPc4	stoupenec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
samotného	samotný	k2eAgMnSc4d1	samotný
krále	král	k1gMnSc4	král
Václava	Václav	k1gMnSc2	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
o	o	k7c6	o
spálení	spálení	k1gNnSc6	spálení
Viklefových	Viklefův	k2eAgInPc2d1	Viklefův
spisů	spis	k1gInPc2	spis
odvolal	odvolat	k5eAaPmAgInS	odvolat
k	k	k7c3	k
soudu	soud	k1gInSc3	soud
Apoštolského	apoštolský	k2eAgInSc2d1	apoštolský
stolce	stolec	k1gInSc2	stolec
<g/>
,	,	kIx,	,
na	na	k7c6	na
vyzvání	vyzvání	k1gNnSc6	vyzvání
kardinála	kardinál	k1gMnSc2	kardinál
soudce	soudce	k1gMnSc2	soudce
se	se	k3xPyFc4	se
však	však	k9	však
k	k	k7c3	k
papežskému	papežský	k2eAgInSc3d1	papežský
soudu	soud	k1gInSc3	soud
v	v	k7c6	v
Boloni	Boloňa	k1gFnSc6	Boloňa
osobně	osobně	k6eAd1	osobně
nedostavil	dostavit	k5eNaPmAgMnS	dostavit
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
vyhýbání	vyhýbání	k1gNnSc4	vyhýbání
se	se	k3xPyFc4	se
soudu	soud	k1gInSc3	soud
vydána	vydán	k2eAgFnSc1d1	vydána
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1411	[number]	k4	1411
klatba	klatba	k1gFnSc1	klatba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
1412	[number]	k4	1412
ztížena	ztížit	k5eAaPmNgNnP	ztížit
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
agravace	agravace	k1gFnSc2	agravace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
první	první	k4xOgMnSc1	první
jej	on	k3xPp3gMnSc4	on
už	už	k9	už
dříve	dříve	k6eAd2	dříve
exkomunikoval	exkomunikovat	k5eAaBmAgMnS	exkomunikovat
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
Zbyněk	Zbyněk	k1gMnSc1	Zbyněk
Zajíc	Zajíc	k1gMnSc1	Zajíc
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1411	[number]	k4	1411
pak	pak	k6eAd1	pak
i	i	k9	i
pisánský	pisánský	k2eAgMnSc1d1	pisánský
vzdoropapež	vzdoropapež	k1gMnSc1	vzdoropapež
Jan	Jan	k1gMnSc1	Jan
XXIII	XXIII	kA	XXIII
<g/>
.	.	kIx.	.
kvůli	kvůli	k7c3	kvůli
jeho	jeho	k3xOp3gFnSc3	jeho
nepřímé	přímý	k2eNgFnSc3d1	nepřímá
podpoře	podpora	k1gFnSc3	podpora
papeže	papež	k1gMnSc2	papež
v	v	k7c6	v
Avignonu	Avignon	k1gInSc6	Avignon
<g/>
.	.	kIx.	.
</s>
<s>
Hus	Hus	k1gMnSc1	Hus
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
v	v	k7c6	v
kázání	kázání	k1gNnSc6	kázání
i	i	k8xC	i
ve	v	k7c6	v
Viklefově	Viklefův	k2eAgFnSc6d1	Viklefova
obhajobě	obhajoba	k1gFnSc6	obhajoba
(	(	kIx(	(
<g/>
spis	spis	k1gInSc1	spis
Defensio	Defensio	k1gNnSc1	Defensio
articulorum	articulorum	k1gInSc1	articulorum
Wycleff	Wycleff	k1gMnSc1	Wycleff
(	(	kIx(	(
<g/>
Obrana	obrana	k1gFnSc1	obrana
Viklefových	Viklefův	k2eAgInPc2d1	Viklefův
článků	článek	k1gInPc2	článek
<g/>
))	))	k?	))
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
přesto	přesto	k8xC	přesto
došlo	dojít	k5eAaPmAgNnS	dojít
na	na	k7c4	na
nátlak	nátlak	k1gInSc4	nátlak
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
ke	k	k7c3	k
smíru	smír	k1gInSc3	smír
mezi	mezi	k7c7	mezi
arcibiskupovou	arcibiskupův	k2eAgFnSc7d1	arcibiskupova
stranou	strana	k1gFnSc7	strana
a	a	k8xC	a
stranou	strana	k1gFnSc7	strana
Husovou	Husův	k2eAgFnSc7d1	Husova
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1411	[number]	k4	1411
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
5	[number]	k4	5
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
Zbyněk	Zbyněk	k1gMnSc1	Zbyněk
Zajíc	Zajíc	k1gMnSc1	Zajíc
utekl	utéct	k5eAaPmAgMnS	utéct
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
do	do	k7c2	do
Uher	Uhry	k1gFnPc2	Uhry
<g/>
,	,	kIx,	,
vynucený	vynucený	k2eAgInSc1d1	vynucený
smír	smír	k1gInSc1	smír
odvolal	odvolat	k5eAaPmAgInS	odvolat
a	a	k8xC	a
28	[number]	k4	28
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
nástupce	nástupce	k1gMnSc1	nástupce
Albík	Albík	k1gMnSc1	Albík
z	z	k7c2	z
Uničova	Uničův	k2eAgNnSc2d1	Uničův
pak	pak	k6eAd1	pak
již	již	k6eAd1	již
byl	být	k5eAaImAgInS	být
zcela	zcela	k6eAd1	zcela
ve	v	k7c6	v
vleku	vlek	k1gInSc6	vlek
královského	královský	k2eAgInSc2d1	královský
dvora	dvůr	k1gInSc2	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
Spor	spor	k1gInSc1	spor
se	se	k3xPyFc4	se
přiostřoval	přiostřovat	k5eAaImAgInS	přiostřovat
<g/>
,	,	kIx,	,
když	když	k8xS	když
Hus	Hus	k1gMnSc1	Hus
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
proti	proti	k7c3	proti
vyhlášení	vyhlášení	k1gNnSc3	vyhlášení
křížové	křížový	k2eAgFnSc2d1	křížová
výpravy	výprava	k1gFnSc2	výprava
vyhlášené	vyhlášený	k2eAgFnPc1d1	vyhlášená
roku	rok	k1gInSc2	rok
1412	[number]	k4	1412
Janem	Jan	k1gMnSc7	Jan
XXIII	XXIII	kA	XXIII
<g/>
.	.	kIx.	.
proti	proti	k7c3	proti
králi	král	k1gMnSc3	král
Ladislavu	Ladislav	k1gMnSc3	Ladislav
Neapolskému	neapolský	k2eAgMnSc3d1	neapolský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
bule	bula	k1gFnSc6	bula
papež	papež	k1gMnSc1	papež
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
odpustky	odpustek	k1gInPc4	odpustek
každému	každý	k3xTgMnSc3	každý
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
se	se	k3xPyFc4	se
bojů	boj	k1gInPc2	boj
zúčastní	zúčastnit	k5eAaPmIp3nS	zúčastnit
<g/>
;	;	kIx,	;
ve	v	k7c6	v
viklefovských	viklefovský	k2eAgInPc6d1	viklefovský
kruzích	kruh	k1gInPc6	kruh
byl	být	k5eAaImAgInS	být
odpor	odpor	k1gInSc1	odpor
proti	proti	k7c3	proti
odpustkům	odpustek	k1gInPc3	odpustek
již	již	k6eAd1	již
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
zakořeněn	zakořeněn	k2eAgMnSc1d1	zakořeněn
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
17	[number]	k4	17
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
napadl	napadnout	k5eAaPmAgMnS	napadnout
odpustkovou	odpustkový	k2eAgFnSc4d1	odpustková
bulu	bula	k1gFnSc4	bula
a	a	k8xC	a
nazval	nazvat	k5eAaPmAgMnS	nazvat
papeže	papež	k1gMnSc4	papež
antikristem	antikrist	k1gMnSc7	antikrist
<g/>
.	.	kIx.	.
</s>
<s>
Následovaly	následovat	k5eAaImAgFnP	následovat
lidové	lidový	k2eAgFnPc1d1	lidová
bouře	bouř	k1gFnPc1	bouř
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
znepokojily	znepokojit	k5eAaPmAgFnP	znepokojit
i	i	k9	i
krále	král	k1gMnSc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
již	již	k6eAd1	již
Jana	Jan	k1gMnSc2	Jan
Husa	Hus	k1gMnSc2	Hus
nebude	být	k5eNaImBp3nS	být
dále	daleko	k6eAd2	daleko
podporovat	podporovat	k5eAaImF	podporovat
<g/>
.	.	kIx.	.
</s>
<s>
Děkan	děkan	k1gMnSc1	děkan
teologické	teologický	k2eAgFnSc2d1	teologická
fakulty	fakulta	k1gFnSc2	fakulta
Štěpán	Štěpán	k1gMnSc1	Štěpán
z	z	k7c2	z
Pálče	Páleč	k1gFnSc2	Páleč
pak	pak	k6eAd1	pak
odsoudil	odsoudit	k5eAaPmAgInS	odsoudit
45	[number]	k4	45
Viklefových	Viklefův	k2eAgInPc2d1	Viklefův
článků	článek	k1gInPc2	článek
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
vydán	vydán	k2eAgInSc1d1	vydán
zákaz	zákaz	k1gInSc1	zákaz
je	být	k5eAaImIp3nS	být
šířit	šířit	k5eAaImF	šířit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Jana	Jan	k1gMnSc4	Jan
Husa	Hus	k1gMnSc4	Hus
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1412	[number]	k4	1412
Jan	Jan	k1gMnSc1	Jan
XXIII	XXIII	kA	XXIII
<g/>
.	.	kIx.	.
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
klatbu	klatba	k1gFnSc4	klatba
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
na	na	k7c4	na
všechny	všechen	k3xTgMnPc4	všechen
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
stýkali	stýkat	k5eAaImAgMnP	stýkat
(	(	kIx(	(
<g/>
byly	být	k5eAaImAgFnP	být
zakázány	zakázán	k2eAgInPc4d1	zakázán
církevní	církevní	k2eAgInPc4d1	církevní
úkony	úkon	k1gInPc4	úkon
ve	v	k7c6	v
všech	všecek	k3xTgNnPc6	všecek
místech	místo	k1gNnPc6	místo
Husova	Husův	k2eAgInSc2d1	Husův
pobytu	pobyt	k1gInSc2	pobyt
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
zároveň	zároveň	k6eAd1	zároveň
nařídil	nařídit	k5eAaPmAgMnS	nařídit
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
Hus	Hus	k1gMnSc1	Hus
zajat	zajat	k2eAgMnSc1d1	zajat
a	a	k8xC	a
souzen	souzen	k2eAgMnSc1d1	souzen
podle	podle	k7c2	podle
církevních	církevní	k2eAgInPc2d1	církevní
zákonů	zákon	k1gInPc2	zákon
a	a	k8xC	a
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
zbořena	zbořen	k2eAgFnSc1d1	zbořena
Betlémská	betlémský	k2eAgFnSc1d1	Betlémská
kaple	kaple	k1gFnSc1	kaple
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
okamžiku	okamžik	k1gInSc6	okamžik
se	se	k3xPyFc4	se
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
od	od	k7c2	od
papeže	papež	k1gMnSc2	papež
odvolal	odvolat	k5eAaPmAgMnS	odvolat
ke	k	k7c3	k
Kristu	Krista	k1gFnSc4	Krista
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
nejvyššímu	vysoký	k2eAgMnSc3d3	nejvyšší
soudci	soudce	k1gMnPc7	soudce
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
interdikt	interdikt	k1gInSc1	interdikt
na	na	k7c6	na
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Hus	Hus	k1gMnSc1	Hus
pobýval	pobývat	k5eAaImAgMnS	pobývat
<g/>
,	,	kIx,	,
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
uskutečnění	uskutečnění	k1gNnSc3	uskutečnění
zákazu	zákaz	k1gInSc2	zákaz
bohoslužeb	bohoslužba	k1gFnPc2	bohoslužba
<g/>
.	.	kIx.	.
</s>
<s>
Tažením	tažení	k1gNnSc7	tažení
proti	proti	k7c3	proti
odpustkům	odpustek	k1gInPc3	odpustek
navíc	navíc	k6eAd1	navíc
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
ztratil	ztratit	k5eAaPmAgMnS	ztratit
i	i	k9	i
podporu	podpora	k1gFnSc4	podpora
královského	královský	k2eAgInSc2d1	královský
dvora	dvůr	k1gInSc2	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
Opustil	opustit	k5eAaPmAgMnS	opustit
proto	proto	k8xC	proto
Prahu	Praha	k1gFnSc4	Praha
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
uchýlil	uchýlit	k5eAaPmAgMnS	uchýlit
na	na	k7c4	na
venkov	venkov	k1gInSc4	venkov
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
se	se	k3xPyFc4	se
přesto	přesto	k8xC	přesto
do	do	k7c2	do
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
vracel	vracet	k5eAaImAgMnS	vracet
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
všude	všude	k6eAd1	všude
nacházel	nacházet	k5eAaImAgMnS	nacházet
přístřeší	přístřeší	k1gNnSc4	přístřeší
<g/>
.	.	kIx.	.
</s>
<s>
Určitější	určitý	k2eAgInPc1d2	určitější
jsou	být	k5eAaImIp3nP	být
pouze	pouze	k6eAd1	pouze
zmínky	zmínka	k1gFnSc2	zmínka
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
přítomnosti	přítomnost	k1gFnSc6	přítomnost
na	na	k7c6	na
Kozím	kozí	k2eAgInSc6d1	kozí
Hrádku	Hrádok	k1gInSc6	Hrádok
(	(	kIx(	(
<g/>
od	od	k7c2	od
dubna	duben	k1gInSc2	duben
roku	rok	k1gInSc2	rok
1413	[number]	k4	1413
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
pak	pak	k6eAd1	pak
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
roku	rok	k1gInSc2	rok
1414	[number]	k4	1414
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
Sezimova	Sezimův	k2eAgNnSc2d1	Sezimovo
Ústí	ústí	k1gNnSc2	ústí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
přebýval	přebývat	k5eAaImAgInS	přebývat
pod	pod	k7c7	pod
ochranou	ochrana	k1gFnSc7	ochrana
Anny	Anna	k1gFnSc2	Anna
z	z	k7c2	z
Mochova	Mochova	k1gFnSc1	Mochova
<g/>
,	,	kIx,	,
vdovy	vdova	k1gFnPc1	vdova
po	po	k7c6	po
pánu	pán	k1gMnSc6	pán
Janovi	Jan	k1gMnSc6	Jan
z	z	k7c2	z
Ústí	ústí	k1gNnSc2	ústí
<g/>
.	.	kIx.	.
</s>
<s>
Souběžně	souběžně	k6eAd1	souběžně
s	s	k7c7	s
tím	ten	k3xDgMnSc7	ten
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgMnS	začít
vyostřovat	vyostřovat	k5eAaImF	vyostřovat
spor	spor	k1gInSc4	spor
o	o	k7c4	o
svobodné	svobodný	k2eAgNnSc4d1	svobodné
kázání	kázání	k1gNnSc4	kázání
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
Hus	Hus	k1gMnSc1	Hus
nejpozději	pozdě	k6eAd3	pozdě
od	od	k7c2	od
zatčení	zatčení	k1gNnSc2	zatčení
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
Abrahama	Abraham	k1gMnSc2	Abraham
z	z	k7c2	z
Velenovic	Velenovice	k1gFnPc2	Velenovice
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1408	[number]	k4	1408
otevřeně	otevřeně	k6eAd1	otevřeně
stavěl	stavět	k5eAaImAgMnS	stavět
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
Bible	bible	k1gFnSc2	bible
může	moct	k5eAaImIp3nS	moct
veřejně	veřejně	k6eAd1	veřejně
kázat	kázat	k5eAaImF	kázat
i	i	k9	i
laik	laik	k1gMnSc1	laik
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
bez	bez	k7c2	bez
jakéhokoli	jakýkoli	k3yIgNnSc2	jakýkoli
církevního	církevní	k2eAgNnSc2d1	církevní
schválení	schválení	k1gNnSc2	schválení
<g/>
.	.	kIx.	.
</s>
<s>
Odvolával	odvolávat	k5eAaImAgInS	odvolávat
se	se	k3xPyFc4	se
při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
na	na	k7c4	na
zprávy	zpráva	k1gFnPc4	zpráva
z	z	k7c2	z
evangelií	evangelium	k1gNnPc2	evangelium
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Ježíš	ježit	k5eAaImIp2nS	ježit
evangelizaci	evangelizace	k1gFnSc4	evangelizace
přímo	přímo	k6eAd1	přímo
nařídil	nařídit	k5eAaPmAgMnS	nařídit
všem	všecek	k3xTgMnPc3	všecek
svým	svůj	k3xOyFgMnPc3	svůj
následovníkům	následovník	k1gMnPc3	následovník
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
ovšem	ovšem	k9	ovšem
Hus	Hus	k1gMnSc1	Hus
zásadním	zásadní	k2eAgInSc7d1	zásadní
způsobem	způsob	k1gInSc7	způsob
zaútočil	zaútočit	k5eAaPmAgMnS	zaútočit
na	na	k7c4	na
církevní	církevní	k2eAgFnSc4d1	církevní
disciplínu	disciplína	k1gFnSc4	disciplína
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
začal	začít	k5eAaPmAgMnS	začít
Hus	Hus	k1gMnSc1	Hus
také	také	k9	také
výrazněji	výrazně	k6eAd2	výrazně
podporovat	podporovat	k5eAaImF	podporovat
překládání	překládání	k1gNnSc4	překládání
Bible	bible	k1gFnSc2	bible
do	do	k7c2	do
jazyka	jazyk	k1gInSc2	jazyk
prostého	prostý	k2eAgInSc2d1	prostý
lidu	lid	k1gInSc2	lid
<g/>
.	.	kIx.	.
</s>
<s>
Jednak	jednak	k8xC	jednak
trval	trvat	k5eAaImAgInS	trvat
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
číst	číst	k5eAaImF	číst
Bibli	bible	k1gFnSc4	bible
ve	v	k7c6	v
srozumitelném	srozumitelný	k2eAgInSc6d1	srozumitelný
jazyce	jazyk	k1gInSc6	jazyk
mají	mít	k5eAaImIp3nP	mít
právo	právo	k1gNnSc4	právo
všichni	všechen	k3xTgMnPc1	všechen
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
nejen	nejen	k6eAd1	nejen
učenci	učenec	k1gMnPc1	učenec
a	a	k8xC	a
kněží	kněz	k1gMnPc1	kněz
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
dobře	dobře	k6eAd1	dobře
věděl	vědět	k5eAaImAgMnS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
autorita	autorita	k1gFnSc1	autorita
zkažených	zkažený	k2eAgMnPc2d1	zkažený
představitelů	představitel	k1gMnPc2	představitel
dobové	dobový	k2eAgFnSc2d1	dobová
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
žijících	žijící	k2eAgFnPc2d1	žijící
v	v	k7c6	v
hlubokém	hluboký	k2eAgInSc6d1	hluboký
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
Písmem	písmo	k1gNnSc7	písmo
<g/>
,	,	kIx,	,
nemůže	moct	k5eNaImIp3nS	moct
obstát	obstát	k5eAaPmF	obstát
<g/>
,	,	kIx,	,
budou	být	k5eAaImBp3nP	být
<g/>
-li	i	k?	-li
mít	mít	k5eAaImF	mít
lidé	člověk	k1gMnPc1	člověk
k	k	k7c3	k
Bibli	bible	k1gFnSc3	bible
v	v	k7c6	v
mateřském	mateřský	k2eAgInSc6d1	mateřský
jazyce	jazyk	k1gInSc6	jazyk
snazší	snadný	k2eAgInSc4d2	snadnější
přístup	přístup	k1gInSc4	přístup
<g/>
.	.	kIx.	.
</s>
<s>
Husova	Husův	k2eAgFnSc1d1	Husova
podpora	podpora	k1gFnSc1	podpora
neautorizované	autorizovaný	k2eNgFnSc2d1	neautorizovaná
veřejné	veřejný	k2eAgFnSc2d1	veřejná
evangelizace	evangelizace	k1gFnSc2	evangelizace
a	a	k8xC	a
podpora	podpora	k1gFnSc1	podpora
překladů	překlad	k1gInPc2	překlad
Bible	bible	k1gFnSc2	bible
do	do	k7c2	do
srozumitelné	srozumitelný	k2eAgFnSc2d1	srozumitelná
češtiny	čeština	k1gFnSc2	čeština
a	a	k8xC	a
němčiny	němčina	k1gFnSc2	němčina
byla	být	k5eAaImAgFnS	být
nakonec	nakonec	k6eAd1	nakonec
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
klíčových	klíčový	k2eAgInPc2d1	klíčový
bodů	bod	k1gInPc2	bod
jeho	jeho	k3xOp3gInSc2	jeho
pozdějšího	pozdní	k2eAgInSc2d2	pozdější
otevřeného	otevřený	k2eAgInSc2d1	otevřený
rozkolu	rozkol	k1gInSc2	rozkol
s	s	k7c7	s
církví	církev	k1gFnSc7	církev
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
zde	zde	k6eAd1	zde
pak	pak	k6eAd1	pak
prožil	prožít	k5eAaPmAgMnS	prožít
své	svůj	k3xOyFgNnSc4	svůj
klíčové	klíčový	k2eAgNnSc4d1	klíčové
vnitřní	vnitřní	k2eAgNnSc4d1	vnitřní
obrácení	obrácení	k1gNnSc4	obrácení
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c6	o
období	období	k1gNnSc6	období
hlubokého	hluboký	k2eAgNnSc2d1	hluboké
rozjímání	rozjímání	k1gNnSc2	rozjímání
nad	nad	k7c7	nad
Písmem	písmo	k1gNnSc7	písmo
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
vzdálen	vzdálit	k5eAaPmNgInS	vzdálit
od	od	k7c2	od
ruchu	ruch	k1gInSc2	ruch
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
o	o	k7c6	o
období	období	k1gNnSc6	období
veřejného	veřejný	k2eAgNnSc2d1	veřejné
kázání	kázání	k1gNnSc2	kázání
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
prvních	první	k4xOgInPc2	první
apoštolů	apoštol	k1gMnPc2	apoštol
<g/>
.	.	kIx.	.
</s>
<s>
Nikoli	nikoli	k9	nikoli
z	z	k7c2	z
kazatelny	kazatelna	k1gFnSc2	kazatelna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c6	o
evangelizaci	evangelizace	k1gFnSc6	evangelizace
dům	dům	k1gInSc4	dům
od	od	k7c2	od
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
políčky	políček	k1gInPc4	políček
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
ploty	plot	k1gInPc4	plot
<g/>
,	,	kIx,	,
v	v	k7c6	v
lesích	les	k1gInPc6	les
a	a	k8xC	a
na	na	k7c6	na
cestách	cesta	k1gFnPc6	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
také	také	k6eAd1	také
napsal	napsat	k5eAaBmAgMnS	napsat
své	svůj	k3xOyFgNnSc4	svůj
stěžejní	stěžejní	k2eAgNnSc4d1	stěžejní
dílo	dílo	k1gNnSc4	dílo
De	De	k?	De
ecclesia	ecclesia	k1gFnSc1	ecclesia
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
zcela	zcela	k6eAd1	zcela
otevřeně	otevřeně	k6eAd1	otevřeně
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
Viklefa	Viklef	k1gMnSc2	Viklef
kritizuje	kritizovat	k5eAaImIp3nS	kritizovat
soudobou	soudobý	k2eAgFnSc4d1	soudobá
církev	církev	k1gFnSc4	církev
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
ho	on	k3xPp3gInSc4	on
odtud	odtud	k6eAd1	odtud
vyhnala	vyhnat	k5eAaPmAgFnS	vyhnat
morová	morový	k2eAgFnSc1d1	morová
epidemie	epidemie	k1gFnSc1	epidemie
<g/>
.	.	kIx.	.
</s>
<s>
Pražská	pražský	k2eAgFnSc1d1	Pražská
teologická	teologický	k2eAgFnSc1d1	teologická
fakulta	fakulta	k1gFnSc1	fakulta
odsoudila	odsoudit	k5eAaPmAgFnS	odsoudit
Husa	Hus	k1gMnSc4	Hus
jako	jako	k8xC	jako
bludaře	bludař	k1gMnPc4	bludař
roku	rok	k1gInSc2	rok
1413	[number]	k4	1413
<g/>
,	,	kIx,	,
vytýkala	vytýkat	k5eAaImAgFnS	vytýkat
mu	on	k3xPp3gMnSc3	on
body	bod	k1gInPc4	bod
v	v	k7c6	v
nauce	nauka	k1gFnSc6	nauka
o	o	k7c6	o
svátostech	svátost	k1gFnPc6	svátost
<g/>
,	,	kIx,	,
svatých	svatá	k1gFnPc6	svatá
a	a	k8xC	a
odpustcích	odpustek	k1gInPc6	odpustek
<g/>
,	,	kIx,	,
vzpírání	vzpírání	k1gNnSc1	vzpírání
se	se	k3xPyFc4	se
autoritě	autorita	k1gFnSc3	autorita
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
níž	jenž	k3xRgFnSc3	jenž
staví	stavit	k5eAaImIp3nP	stavit
osobní	osobní	k2eAgInSc4d1	osobní
výklad	výklad	k1gInSc4	výklad
Písma	písmo	k1gNnSc2	písmo
svatého	svatý	k2eAgNnSc2d1	svaté
a	a	k8xC	a
vzpírání	vzpírání	k1gNnSc4	vzpírání
se	se	k3xPyFc4	se
poddanosti	poddanost	k1gFnSc2	poddanost
vůči	vůči	k7c3	vůči
papeži	papež	k1gMnSc3	papež
<g/>
,	,	kIx,	,
biskupům	biskup	k1gInPc3	biskup
a	a	k8xC	a
kněžím	kněz	k1gMnPc3	kněz
<g/>
<g />
.	.	kIx.	.
</s>
<s>
20	[number]	k4	20
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1414	[number]	k4	1414
se	se	k3xPyFc4	se
Hus	Hus	k1gMnSc1	Hus
inkognito	inkognito	k6eAd1	inkognito
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
sepsal	sepsat	k5eAaPmAgMnS	sepsat
odsud	odsud	k6eAd1	odsud
několik	několik	k4yIc4	několik
listů	list	k1gInPc2	list
<g/>
,	,	kIx,	,
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
zaslal	zaslat	k5eAaPmAgMnS	zaslat
Jan	Jan	k1gMnSc1	Jan
XXIII	XXIII	kA	XXIII
<g/>
.	.	kIx.	.
litomyšlskému	litomyšlský	k2eAgMnSc3d1	litomyšlský
biskupu	biskup	k1gMnSc3	biskup
Janovi	Jan	k1gMnSc3	Jan
výtku	výtka	k1gFnSc4	výtka
ohledně	ohledně	k7c2	ohledně
rozšíření	rozšíření	k1gNnSc2	rozšíření
Viklefova	Viklefův	k2eAgNnSc2d1	Viklefovo
a	a	k8xC	a
Husova	Husův	k2eAgNnSc2d1	Husovo
učení	učení	k1gNnSc2	učení
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
s	s	k7c7	s
žádostí	žádost	k1gFnSc7	žádost
o	o	k7c4	o
napomenutí	napomenutí	k1gNnSc4	napomenutí
pražského	pražský	k2eAgMnSc2d1	pražský
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
Konráda	Konrád	k1gMnSc2	Konrád
z	z	k7c2	z
Vechty	Vechta	k1gFnSc2	Vechta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
téže	týž	k3xTgFnSc6	týž
době	doba	k1gFnSc6	doba
arcibiskupovi	arcibiskup	k1gMnSc3	arcibiskup
Konrádovi	Konrád	k1gMnSc3	Konrád
zaslal	zaslat	k5eAaPmAgMnS	zaslat
seznam	seznam	k1gInSc4	seznam
Husových	Husových	k2eAgInPc2d1	Husových
bludů	blud	k1gInPc2	blud
v	v	k7c6	v
traktátu	traktát	k1gInSc6	traktát
De	De	k?	De
ecclesia	ecclesius	k1gMnSc4	ecclesius
kancléř	kancléř	k1gMnSc1	kancléř
pařížské	pařížský	k2eAgFnSc2d1	Pařížská
univerzity	univerzita	k1gFnSc2	univerzita
Jean	Jean	k1gMnSc1	Jean
Gerson	Gerson	k1gMnSc1	Gerson
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
odjezdu	odjezd	k1gInSc2	odjezd
na	na	k7c4	na
svolaný	svolaný	k2eAgInSc4d1	svolaný
Kostnický	kostnický	k2eAgInSc4d1	kostnický
koncil	koncil	k1gInSc4	koncil
žil	žít	k5eAaImAgMnS	žít
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
pod	pod	k7c7	pod
ochranou	ochrana	k1gFnSc7	ochrana
Jindřicha	Jindřich	k1gMnSc2	Jindřich
Lefla	Lefl	k1gMnSc2	Lefl
z	z	k7c2	z
Lažan	Lažana	k1gFnPc2	Lažana
na	na	k7c6	na
hradu	hrad	k1gInSc6	hrad
Krakovci	krakovec	k1gInSc6	krakovec
<g/>
.	.	kIx.	.
</s>
<s>
Kostnický	kostnický	k2eAgInSc1d1	kostnický
koncil	koncil	k1gInSc1	koncil
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
svolal	svolat	k5eAaPmAgInS	svolat
30	[number]	k4	30
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1413	[number]	k4	1413
Jan	Jan	k1gMnSc1	Jan
XXIII	XXIII	kA	XXIII
<g/>
.	.	kIx.	.
po	po	k7c6	po
naléhání	naléhání	k1gNnSc6	naléhání
římského	římský	k2eAgMnSc2d1	římský
krále	král	k1gMnSc2	král
Zikmunda	Zikmund	k1gMnSc2	Zikmund
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopad	listopad	k1gInSc1	listopad
1414	[number]	k4	1414
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
ukončit	ukončit	k5eAaPmF	ukončit
neúnosný	únosný	k2eNgInSc1d1	neúnosný
stav	stav	k1gInSc1	stav
rozkolu	rozkol	k1gInSc2	rozkol
západní	západní	k2eAgFnSc2d1	západní
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
reformovat	reformovat	k5eAaBmF	reformovat
církev	církev	k1gFnSc4	církev
"	"	kIx"	"
<g/>
v	v	k7c6	v
hlavě	hlava	k1gFnSc6	hlava
i	i	k8xC	i
v	v	k7c6	v
údech	úd	k1gInPc6	úd
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
in	in	k?	in
capite	capit	k1gInSc5	capit
et	et	k?	et
membris	membris	k1gFnPc6	membris
<g/>
)	)	kIx)	)
a	a	k8xC	a
odstranit	odstranit	k5eAaPmF	odstranit
současné	současný	k2eAgFnPc4d1	současná
hereze	hereze	k1gFnPc4	hereze
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
určení	určení	k1gNnSc2	určení
vysvítalo	vysvítat	k5eAaImAgNnS	vysvítat
<g/>
,	,	kIx,	,
že	že	k8xS	že
proces	proces	k1gInSc1	proces
s	s	k7c7	s
Janem	Jan	k1gMnSc7	Jan
Husem	Hus	k1gMnSc7	Hus
<g/>
,	,	kIx,	,
zahájený	zahájený	k2eAgInSc4d1	zahájený
u	u	k7c2	u
kurie	kurie	k1gFnSc2	kurie
Alexandra	Alexandr	k1gMnSc2	Alexandr
V.	V.	kA	V.
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
tvořit	tvořit	k5eAaImF	tvořit
pouze	pouze	k6eAd1	pouze
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
položek	položka	k1gFnPc2	položka
programu	program	k1gInSc2	program
koncilu	koncil	k1gInSc2	koncil
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svým	svůj	k3xOyFgNnSc7	svůj
(	(	kIx(	(
<g/>
zcela	zcela	k6eAd1	zcela
nekanonickým	kanonický	k2eNgMnSc7d1	nekanonický
<g/>
)	)	kIx)	)
odvoláním	odvolání	k1gNnSc7	odvolání
ke	k	k7c3	k
Kristu	Krista	k1gFnSc4	Krista
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
ke	k	k7c3	k
svolanému	svolaný	k2eAgInSc3d1	svolaný
koncilu	koncil	k1gInSc3	koncil
<g/>
,	,	kIx,	,
nebyl	být	k5eNaImAgInS	být
dokončen	dokončit	k5eAaPmNgInS	dokončit
proces	proces	k1gInSc1	proces
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
papežská	papežský	k2eAgFnSc1d1	Papežská
kurie	kurie	k1gFnSc1	kurie
s	s	k7c7	s
Husem	Hus	k1gMnSc7	Hus
vedla	vést	k5eAaImAgFnS	vést
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
kostnický	kostnický	k2eAgInSc1d1	kostnický
koncil	koncil	k1gInSc1	koncil
měl	mít	k5eAaImAgInS	mít
tento	tento	k3xDgInSc4	tento
proces	proces	k1gInSc4	proces
převzít	převzít	k5eAaPmF	převzít
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
se	se	k3xPyFc4	se
na	na	k7c4	na
pozvání	pozvání	k1gNnSc4	pozvání
krále	král	k1gMnSc2	král
Zikmunda	Zikmund	k1gMnSc2	Zikmund
11	[number]	k4	11
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
(	(	kIx(	(
<g/>
dosud	dosud	k6eAd1	dosud
bez	bez	k7c2	bez
královského	královský	k2eAgInSc2d1	královský
glejtu	glejt	k1gInSc2	glejt
<g/>
)	)	kIx)	)
vydal	vydat	k5eAaPmAgInS	vydat
na	na	k7c4	na
koncil	koncil	k1gInSc4	koncil
v	v	k7c6	v
domněnce	domněnka	k1gFnSc6	domněnka
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
bude	být	k5eAaImBp3nS	být
umožněno	umožnit	k5eAaPmNgNnS	umožnit
svobodně	svobodně	k6eAd1	svobodně
disputovat	disputovat	k5eAaImF	disputovat
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
nauce	nauka	k1gFnSc6	nauka
a	a	k8xC	a
víře	víra	k1gFnSc6	víra
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
neuvědomil	uvědomit	k5eNaPmAgInS	uvědomit
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
již	již	k6eAd1	již
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Kostnici	Kostnice	k1gFnSc6	Kostnice
veden	vést	k5eAaImNgInS	vést
právní	právní	k2eAgInSc1d1	právní
proces	proces	k1gInSc1	proces
<g/>
,	,	kIx,	,
navazující	navazující	k2eAgInSc1d1	navazující
na	na	k7c4	na
nedokončený	dokončený	k2eNgInSc4d1	nedokončený
proces	proces	k1gInSc4	proces
předešlý	předešlý	k2eAgInSc4d1	předešlý
<g/>
.	.	kIx.	.
</s>
<s>
Glejt	glejt	k1gInSc1	glejt
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
Husovi	Hus	k1gMnSc6	Hus
vystavil	vystavit	k5eAaPmAgMnS	vystavit
král	král	k1gMnSc1	král
Zikmund	Zikmund	k1gMnSc1	Zikmund
<g/>
,	,	kIx,	,
nemohl	moct	k5eNaImAgMnS	moct
-	-	kIx~	-
jak	jak	k8xS	jak
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
i	i	k9	i
z	z	k7c2	z
jeho	jeho	k3xOp3gNnSc2	jeho
znění	znění	k1gNnSc2	znění
-	-	kIx~	-
zaručit	zaručit	k5eAaPmF	zaručit
nic	nic	k3yNnSc1	nic
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
bezpečnou	bezpečný	k2eAgFnSc4d1	bezpečná
cestu	cesta	k1gFnSc4	cesta
Zikmundovým	Zikmundův	k2eAgNnSc7d1	Zikmundovo
územím	území	k1gNnSc7	území
do	do	k7c2	do
Kostnice	Kostnice	k1gFnSc2	Kostnice
(	(	kIx(	(
<g/>
přes	přes	k7c4	přes
Zlatou	zlatý	k2eAgFnSc4d1	zlatá
cestu	cesta	k1gFnSc4	cesta
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
církevní	církevní	k2eAgInSc1d1	církevní
soud	soud	k1gInSc1	soud
koncilu	koncil	k1gInSc2	koncil
se	se	k3xPyFc4	se
vymykal	vymykat	k5eAaImAgMnS	vymykat
králově	králův	k2eAgFnSc3d1	králova
pravomoci	pravomoc	k1gFnSc3	pravomoc
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Husem	Hus	k1gMnSc7	Hus
cestovali	cestovat	k5eAaImAgMnP	cestovat
Václav	Václav	k1gMnSc1	Václav
z	z	k7c2	z
Dubé	Dubá	k1gFnSc2	Dubá
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
z	z	k7c2	z
Chlumu	chlum	k1gInSc2	chlum
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
se	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
vozy	vůz	k1gInPc7	vůz
a	a	k8xC	a
více	hodně	k6eAd2	hodně
než	než	k8xS	než
třiceti	třicet	k4xCc7	třicet
koňmi	kůň	k1gMnPc7	kůň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Kostnici	Kostnice	k1gFnSc6	Kostnice
se	se	k3xPyFc4	se
k	k	k7c3	k
Husovi	Hus	k1gMnSc3	Hus
přidružil	přidružit	k5eAaPmAgMnS	přidružit
zástupce	zástupce	k1gMnSc1	zástupce
univerzity	univerzita	k1gFnSc2	univerzita
Jan	Jan	k1gMnSc1	Jan
Kardinál	kardinál	k1gMnSc1	kardinál
z	z	k7c2	z
Rejštejna	Rejštejn	k1gInSc2	Rejštejn
<g/>
,	,	kIx,	,
a	a	k8xC	a
písař	písař	k1gMnSc1	písař
poselstva	poselstvo	k1gNnSc2	poselstvo
Petr	Petr	k1gMnSc1	Petr
z	z	k7c2	z
Mladoňovic	Mladoňovice	k1gFnPc2	Mladoňovice
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zůstal	zůstat	k5eAaPmAgMnS	zůstat
s	s	k7c7	s
Husem	Hus	k1gMnSc7	Hus
do	do	k7c2	do
posledních	poslední	k2eAgInPc2d1	poslední
dnů	den	k1gInPc2	den
a	a	k8xC	a
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
se	se	k3xPyFc4	se
o	o	k7c4	o
vydání	vydání	k1gNnSc4	vydání
Husova	Husův	k2eAgInSc2d1	Husův
životopisu	životopis	k1gInSc2	životopis
<g/>
.	.	kIx.	.
</s>
<s>
Nejvlivnější	vlivný	k2eAgFnSc1d3	nejvlivnější
osobnost	osobnost	k1gFnSc1	osobnost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
Husa	husa	k1gFnSc1	husa
podporovala	podporovat	k5eAaImAgFnS	podporovat
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
Žofie	Žofie	k1gFnSc1	Žofie
<g/>
,	,	kIx,	,
manželka	manželka	k1gFnSc1	manželka
krále	král	k1gMnSc2	král
Václava	Václav	k1gMnSc2	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
bránila	bránit	k5eAaImAgFnS	bránit
husitství	husitství	k1gNnSc4	husitství
jen	jen	k6eAd1	jen
do	do	k7c2	do
nepokojů	nepokoj	k1gInPc2	nepokoj
30	[number]	k4	30
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1419	[number]	k4	1419
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Kostnice	Kostnice	k1gFnSc2	Kostnice
Hus	Hus	k1gMnSc1	Hus
dorazil	dorazit	k5eAaPmAgMnS	dorazit
3	[number]	k4	3
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
a	a	k8xC	a
okamžitě	okamžitě	k6eAd1	okamžitě
se	se	k3xPyFc4	se
po	po	k7c6	po
městě	město	k1gNnSc6	město
rozkřiklo	rozkřiknout	k5eAaPmAgNnS	rozkřiknout
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
přítomen	přítomen	k2eAgMnSc1d1	přítomen
"	"	kIx"	"
<g/>
zatvrzelý	zatvrzelý	k2eAgMnSc1d1	zatvrzelý
kacíř	kacíř	k1gMnSc1	kacíř
český	český	k2eAgMnSc1d1	český
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
vyhlášku	vyhláška	k1gFnSc4	vyhláška
vypustil	vypustit	k5eAaPmAgMnS	vypustit
Husův	Husův	k2eAgMnSc1d1	Husův
odpůrce	odpůrce	k1gMnSc1	odpůrce
Michal	Michal	k1gMnSc1	Michal
de	de	k?	de
Causis	Causis	k1gInSc1	Causis
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
v	v	k7c6	v
Kostnici	Kostnice	k1gFnSc6	Kostnice
zrušil	zrušit	k5eAaPmAgMnS	zrušit
nad	nad	k7c7	nad
Husem	Hus	k1gMnSc7	Hus
klatbu	klatba	k1gFnSc4	klatba
<g/>
,	,	kIx,	,
zřejmě	zřejmě	k6eAd1	zřejmě
i	i	k9	i
pro	pro	k7c4	pro
právní	právní	k2eAgFnPc4d1	právní
souvislosti	souvislost	k1gFnPc4	souvislost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
jeho	jeho	k3xOp3gInSc1	jeho
pobyt	pobyt	k1gInSc1	pobyt
přinášel	přinášet	k5eAaImAgInS	přinášet
pro	pro	k7c4	pro
ostatní	ostatní	k2eAgMnPc4d1	ostatní
koncilní	koncilní	k2eAgMnPc4d1	koncilní
účastníky	účastník	k1gMnPc4	účastník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
Husova	Husův	k2eAgInSc2d1	Husův
pobytu	pobyt	k1gInSc2	pobyt
v	v	k7c6	v
Kostnici	Kostnice	k1gFnSc6	Kostnice
Jakoubek	Jakoubek	k1gMnSc1	Jakoubek
ze	z	k7c2	z
Stříbra	stříbro	k1gNnSc2	stříbro
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
začal	začít	k5eAaPmAgMnS	začít
podávat	podávat	k5eAaImF	podávat
přijímání	přijímání	k1gNnSc4	přijímání
pod	pod	k7c7	pod
obojí	oboj	k1gFnSc7	oboj
způsobou	způsoba	k1gFnSc7	způsoba
<g/>
,	,	kIx,	,
kterýžto	kterýžto	k?	kterýžto
zvyk	zvyk	k1gInSc1	zvyk
Hus	Hus	k1gMnSc1	Hus
schvaloval	schvalovat	k5eAaImAgMnS	schvalovat
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
sám	sám	k3xTgMnSc1	sám
pod	pod	k7c7	pod
obojí	oboj	k1gFnSc7	oboj
nikdy	nikdy	k6eAd1	nikdy
nepodával	podávat	k5eNaImAgInS	podávat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Kostnici	Kostnice	k1gFnSc6	Kostnice
se	s	k7c7	s
27	[number]	k4	27
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
roznesla	roznést	k5eAaPmAgFnS	roznést
zpráva	zpráva	k1gFnSc1	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
chce	chtít	k5eAaImIp3nS	chtít
uprchnout	uprchnout	k5eAaPmF	uprchnout
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
byl	být	k5eAaImAgMnS	být
následujícího	následující	k2eAgInSc2d1	následující
dne	den	k1gInSc2	den
pod	pod	k7c7	pod
záminkou	záminka	k1gFnSc7	záminka
rozhovoru	rozhovor	k1gInSc2	rozhovor
s	s	k7c7	s
kardinály	kardinál	k1gMnPc7	kardinál
přiveden	přiveden	k2eAgInSc1d1	přiveden
do	do	k7c2	do
paláce	palác	k1gInSc2	palác
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dlel	dlít	k5eAaImAgMnS	dlít
i	i	k9	i
Jan	Jan	k1gMnSc1	Jan
XXIII	XXIII	kA	XXIII
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
a	a	k8xC	a
kde	kde	k6eAd1	kde
zůstal	zůstat	k5eAaPmAgMnS	zůstat
(	(	kIx(	(
<g/>
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
souhlasem	souhlas	k1gInSc7	souhlas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Husův	Husův	k2eAgMnSc1d1	Husův
žalobce	žalobce	k1gMnSc1	žalobce
Michal	Michal	k1gMnSc1	Michal
de	de	k?	de
Causis	Causis	k1gFnSc2	Causis
podal	podat	k5eAaPmAgMnS	podat
proti	proti	k7c3	proti
Husovi	Hus	k1gMnSc3	Hus
první	první	k4xOgInPc4	první
články	článek	k1gInPc1	článek
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgMnSc6	jenž
jej	on	k3xPp3gMnSc4	on
obvinil	obvinit	k5eAaPmAgMnS	obvinit
z	z	k7c2	z
osmi	osm	k4xCc2	osm
bludů	blud	k1gInPc2	blud
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
byla	být	k5eAaImAgFnS	být
jmenována	jmenovat	k5eAaImNgFnS	jmenovat
vyšetřovací	vyšetřovací	k2eAgFnSc1d1	vyšetřovací
komise	komise	k1gFnSc1	komise
<g/>
,	,	kIx,	,
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgMnS	být
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
uvězněn	uvěznit	k5eAaPmNgMnS	uvěznit
v	v	k7c6	v
kobce	kobka	k1gFnSc6	kobka
dominikánského	dominikánský	k2eAgInSc2d1	dominikánský
kláštera	klášter	k1gInSc2	klášter
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
na	na	k7c6	na
Bodamském	bodamský	k2eAgNnSc6d1	Bodamské
jezeře	jezero	k1gNnSc6	jezero
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
držen	držet	k5eAaImNgInS	držet
až	až	k6eAd1	až
do	do	k7c2	do
března	březen	k1gInSc2	březen
1415	[number]	k4	1415
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
bylo	být	k5eAaImAgNnS	být
Husovo	Husův	k2eAgNnSc1d1	Husovo
zatčení	zatčení	k1gNnSc1	zatčení
přijato	přijmout	k5eAaPmNgNnS	přijmout
jako	jako	k8xC	jako
porušení	porušení	k1gNnSc1	porušení
královského	královský	k2eAgInSc2d1	královský
glejtu	glejt	k1gInSc2	glejt
<g/>
,	,	kIx,	,
moravští	moravský	k2eAgMnPc1d1	moravský
přední	přední	k2eAgMnPc1d1	přední
pánové	pán	k1gMnPc1	pán
proto	proto	k8xC	proto
u	u	k7c2	u
Zikmunda	Zikmund	k1gMnSc2	Zikmund
intervenovali	intervenovat	k5eAaImAgMnP	intervenovat
<g/>
.	.	kIx.	.
</s>
<s>
Komise	komise	k1gFnSc1	komise
několikrát	několikrát	k6eAd1	několikrát
žádala	žádat	k5eAaImAgFnS	žádat
Husa	Hus	k1gMnSc4	Hus
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
podrobil	podrobit	k5eAaPmAgMnS	podrobit
rozsudku	rozsudek	k1gInSc2	rozsudek
12	[number]	k4	12
či	či	k8xC	či
13	[number]	k4	13
mistrů	mistr	k1gMnPc2	mistr
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
on	on	k3xPp3gMnSc1	on
se	se	k3xPyFc4	se
chtěl	chtít	k5eAaImAgMnS	chtít
zodpovídat	zodpovídat	k5eAaPmF	zodpovídat
jedině	jedině	k6eAd1	jedině
sněmu	sněm	k1gInSc3	sněm
<g/>
;	;	kIx,	;
při	při	k7c6	při
vyšetřování	vyšetřování	k1gNnSc6	vyšetřování
se	se	k3xPyFc4	se
nepřiznal	přiznat	k5eNaPmAgInS	přiznat
k	k	k7c3	k
žádnému	žádný	k3yNgInSc3	žádný
z	z	k7c2	z
Viklefových	Viklefův	k2eAgInPc2d1	Viklefův
názorů	názor	k1gInPc2	názor
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
proto	proto	k8xC	proto
tázán	tázán	k2eAgMnSc1d1	tázán
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
sepsal	sepsat	k5eAaPmAgMnS	sepsat
komentář	komentář	k1gInSc4	komentář
k	k	k7c3	k
bule	bula	k1gFnSc3	bula
Jana	Jan	k1gMnSc2	Jan
XXIII	XXIII	kA	XXIII
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
Viklefovy	Viklefův	k2eAgInPc4d1	Viklefův
názory	názor	k1gInPc4	názor
odsoudila	odsoudit	k5eAaPmAgFnS	odsoudit
<g/>
,	,	kIx,	,
Hus	Hus	k1gMnSc1	Hus
pak	pak	k6eAd1	pak
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c2	za
autora	autor	k1gMnSc2	autor
komentáře	komentář	k1gInSc2	komentář
Jana	Jan	k1gMnSc2	Jan
z	z	k7c2	z
Jesenice	Jesenice	k1gFnSc2	Jesenice
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
pobouřilo	pobouřit	k5eAaPmAgNnS	pobouřit
Jana	Jan	k1gMnSc2	Jan
z	z	k7c2	z
Chlumu	chlum	k1gInSc2	chlum
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
Husa	Husa	k1gMnSc1	Husa
obvinil	obvinit	k5eAaPmAgMnS	obvinit
<g/>
,	,	kIx,	,
že	že	k8xS	že
odstupuje	odstupovat	k5eAaImIp3nS	odstupovat
od	od	k7c2	od
pravdy	pravda	k1gFnSc2	pravda
a	a	k8xC	a
že	že	k8xS	že
se	se	k3xPyFc4	se
bojí	bát	k5eAaImIp3nS	bát
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
poté	poté	k6eAd1	poté
onemocněl	onemocnět	k5eAaPmAgMnS	onemocnět
<g/>
,	,	kIx,	,
jednání	jednání	k1gNnSc1	jednání
bylo	být	k5eAaImAgNnS	být
přerušeno	přerušit	k5eAaPmNgNnS	přerušit
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
u	u	k7c2	u
Husa	Hus	k1gMnSc2	Hus
vypovídali	vypovídat	k5eAaImAgMnP	vypovídat
pod	pod	k7c7	pod
přísahou	přísaha	k1gFnSc7	přísaha
pražští	pražský	k2eAgMnPc1d1	pražský
i	i	k8xC	i
jiní	jiný	k2eAgMnPc1d1	jiný
vyučující	vyučující	k2eAgMnPc1d1	vyučující
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
Zikmund	Zikmund	k1gMnSc1	Zikmund
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
příjezdu	příjezd	k1gInSc6	příjezd
25	[number]	k4	25
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
do	do	k7c2	do
Kostnice	Kostnice	k1gFnSc2	Kostnice
žádal	žádat	k5eAaImAgInS	žádat
kardinály	kardinál	k1gMnPc4	kardinál
o	o	k7c4	o
Husovo	Husův	k2eAgNnSc4d1	Husovo
propuštění	propuštění	k1gNnSc4	propuštění
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
ti	ten	k3xDgMnPc1	ten
pohrozili	pohrozit	k5eAaPmAgMnP	pohrozit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
sněm	sněm	k1gInSc1	sněm
rozejde	rozejít	k5eAaPmIp3nS	rozejít
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
kvůli	kvůli	k7c3	kvůli
zachování	zachování	k1gNnSc3	zachování
důležité	důležitý	k2eAgFnSc2d1	důležitá
věci	věc	k1gFnSc2	věc
odstranění	odstranění	k1gNnSc1	odstranění
rozkolu	rozkol	k1gInSc2	rozkol
v	v	k7c6	v
církvi	církev	k1gFnSc6	církev
nakonec	nakonec	k6eAd1	nakonec
na	na	k7c4	na
nátlak	nátlak	k1gInSc4	nátlak
4	[number]	k4	4
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1415	[number]	k4	1415
ustoupil	ustoupit	k5eAaPmAgInS	ustoupit
<g/>
,	,	kIx,	,
dostalo	dostat	k5eAaPmAgNnS	dostat
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
slibu	slib	k1gInSc3	slib
<g/>
,	,	kIx,	,
že	že	k8xS	že
Janu	Jan	k1gMnSc3	Jan
Husovi	Hus	k1gMnSc3	Hus
bude	být	k5eAaImBp3nS	být
zajištěno	zajistit	k5eAaPmNgNnS	zajistit
veřejné	veřejný	k2eAgNnSc1d1	veřejné
slyšení	slyšení	k1gNnSc1	slyšení
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
změněna	změněn	k2eAgFnSc1d1	změněna
vazba	vazba	k1gFnSc1	vazba
a	a	k8xC	a
8	[number]	k4	8
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1415	[number]	k4	1415
byl	být	k5eAaImAgInS	být
přemístěn	přemístit	k5eAaPmNgInS	přemístit
do	do	k7c2	do
mírnějšího	mírný	k2eAgNnSc2d2	mírnější
vězení	vězení	k1gNnSc2	vězení
v	v	k7c6	v
Dominikánském	dominikánský	k2eAgInSc6d1	dominikánský
klášteře	klášter	k1gInSc6	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
Hus	Hus	k1gMnSc1	Hus
složil	složit	k5eAaPmAgMnS	složit
několik	několik	k4yIc4	několik
spisů	spis	k1gInPc2	spis
<g/>
,	,	kIx,	,
O	o	k7c6	o
pokání	pokání	k1gNnSc6	pokání
<g/>
,	,	kIx,	,
O	o	k7c4	o
milování	milování	k1gNnSc4	milování
Boha	bůh	k1gMnSc2	bůh
<g/>
,	,	kIx,	,
O	o	k7c6	o
třech	tři	k4xCgMnPc6	tři
nepřátelích	nepřítel	k1gMnPc6	nepřítel
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
O	o	k7c6	o
smrtelném	smrtelný	k2eAgInSc6d1	smrtelný
hříchu	hřích	k1gInSc6	hřích
<g/>
,	,	kIx,	,
O	o	k7c4	o
přikázání	přikázání	k1gNnSc4	přikázání
Páně	páně	k2eAgFnSc1d1	páně
a	a	k8xC	a
modlitbě	modlitba	k1gFnSc3	modlitba
<g/>
,	,	kIx,	,
O	o	k7c6	o
manželství	manželství	k1gNnSc6	manželství
a	a	k8xC	a
O	o	k7c6	o
svátosti	svátost	k1gFnSc6	svátost
oltářní	oltářní	k2eAgFnSc6d1	oltářní
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
psal	psát	k5eAaImAgMnS	psát
zašifrované	zašifrovaný	k2eAgInPc4d1	zašifrovaný
dopisy	dopis	k1gInPc4	dopis
těm	ten	k3xDgMnPc3	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
ho	on	k3xPp3gMnSc4	on
provázeli	provázet	k5eAaImAgMnP	provázet
na	na	k7c4	na
koncil	koncil	k1gInSc4	koncil
<g/>
.	.	kIx.	.
</s>
<s>
Šifra	šifra	k1gFnSc1	šifra
spočívala	spočívat	k5eAaImAgFnS	spočívat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Hus	Hus	k1gMnSc1	Hus
přeměňoval	přeměňovat	k5eAaImAgMnS	přeměňovat
samohlásky	samohláska	k1gFnPc4	samohláska
na	na	k7c4	na
písmena	písmeno	k1gNnPc4	písmeno
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
v	v	k7c6	v
abecedě	abeceda	k1gFnSc6	abeceda
následují	následovat	k5eAaImIp3nP	následovat
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
<g/>
.	.	kIx.	.
</s>
<s>
Dostával	dostávat	k5eAaImAgMnS	dostávat
zde	zde	k6eAd1	zde
i	i	k9	i
dopisy	dopis	k1gInPc1	dopis
z	z	k7c2	z
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
mu	on	k3xPp3gInSc3	on
umožněny	umožněn	k2eAgFnPc4d1	umožněna
návštěvy	návštěva	k1gFnPc4	návštěva
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
dorazil	dorazit	k5eAaPmAgMnS	dorazit
na	na	k7c4	na
koncil	koncil	k1gInSc4	koncil
Jean	Jean	k1gMnSc1	Jean
Gerson	Gerson	k1gMnSc1	Gerson
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
soupisem	soupis	k1gInSc7	soupis
20	[number]	k4	20
Husových	Husových	k2eAgInPc2d1	Husových
bludných	bludný	k2eAgInPc2d1	bludný
článků	článek	k1gInPc2	článek
<g/>
;	;	kIx,	;
ty	ty	k3xPp2nSc1	ty
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
lživé	lživý	k2eAgFnPc4d1	lživá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1415	[number]	k4	1415
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
otázka	otázka	k1gFnSc1	otázka
vyvíjet	vyvíjet	k5eAaImF	vyvíjet
velmi	velmi	k6eAd1	velmi
dramaticky	dramaticky	k6eAd1	dramaticky
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
Jan	Jan	k1gMnSc1	Jan
XXIII	XXIII	kA	XXIII
<g/>
.	.	kIx.	.
pod	pod	k7c7	pod
nátlakem	nátlak	k1gInSc7	nátlak
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
ochoten	ochoten	k2eAgMnSc1d1	ochoten
abdikovat	abdikovat	k5eAaBmF	abdikovat
<g/>
,	,	kIx,	,
učiní	učinit	k5eAaPmIp3nS	učinit
<g/>
-li	i	k?	-li
to	ten	k3xDgNnSc1	ten
i	i	k9	i
Řehoř	Řehoř	k1gMnSc1	Řehoř
XII	XII	kA	XII
<g/>
.	.	kIx.	.
a	a	k8xC	a
Benedikt	Benedikt	k1gMnSc1	Benedikt
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
z	z	k7c2	z
20	[number]	k4	20
<g/>
.	.	kIx.	.
na	na	k7c4	na
21	[number]	k4	21
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
však	však	k9	však
prchl	prchnout	k5eAaPmAgInS	prchnout
v	v	k7c6	v
přestrojení	přestrojení	k1gNnSc6	přestrojení
za	za	k7c4	za
pacholka	pacholek	k1gMnSc4	pacholek
z	z	k7c2	z
Kostnice	Kostnice	k1gFnSc2	Kostnice
do	do	k7c2	do
Laufenburgu	Laufenburg	k1gInSc2	Laufenburg
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
30	[number]	k4	30
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
odvolal	odvolat	k5eAaPmAgMnS	odvolat
svůj	svůj	k3xOyFgInSc4	svůj
slib	slib	k1gInSc4	slib
abdikace	abdikace	k1gFnSc2	abdikace
<g/>
.	.	kIx.	.
</s>
<s>
Hrozilo	hrozit	k5eAaImAgNnS	hrozit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
koncil	koncil	k1gInSc1	koncil
rozejde	rozejít	k5eAaPmIp3nS	rozejít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nastalém	nastalý	k2eAgInSc6d1	nastalý
zmatku	zmatek	k1gInSc6	zmatek
se	se	k3xPyFc4	se
všichni	všechen	k3xTgMnPc1	všechen
přestali	přestat	k5eAaPmAgMnP	přestat
o	o	k7c4	o
Husovu	Husův	k2eAgFnSc4d1	Husova
kauzu	kauza	k1gFnSc4	kauza
zajímat	zajímat	k5eAaImF	zajímat
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
dal	dát	k5eAaPmAgMnS	dát
kostnický	kostnický	k2eAgMnSc1d1	kostnický
biskup	biskup	k1gMnSc1	biskup
Otto	Otto	k1gMnSc1	Otto
Jana	Jan	k1gMnSc4	Jan
Husa	Hus	k1gMnSc4	Hus
převézt	převézt	k5eAaPmF	převézt
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
hrad	hrad	k1gInSc4	hrad
v	v	k7c4	v
Gottliebenu	Gottlieben	k2eAgFnSc4d1	Gottlieben
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
překonal	překonat	k5eAaPmAgInS	překonat
koncil	koncil	k1gInSc1	koncil
vážnou	vážný	k2eAgFnSc4d1	vážná
krizi	krize	k1gFnSc4	krize
vyhlášením	vyhlášení	k1gNnSc7	vyhlášení
svrchovanosti	svrchovanost	k1gFnSc2	svrchovanost
koncilu	koncil	k1gInSc2	koncil
nad	nad	k7c7	nad
papežem	papež	k1gMnSc7	papež
dekretem	dekret	k1gInSc7	dekret
Haec	Haec	k1gInSc4	Haec
Sancta	Sanct	k1gInSc2	Sanct
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
XXIII	XXIII	kA	XXIII
<g/>
.	.	kIx.	.
byl	být	k5eAaImAgInS	být
opětně	opětně	k6eAd1	opětně
13	[number]	k4	13
<g/>
.	.	kIx.	.
a	a	k8xC	a
14	[number]	k4	14
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
předvolán	předvolat	k5eAaPmNgInS	předvolat
před	před	k7c4	před
koncil	koncil	k1gInSc4	koncil
a	a	k8xC	a
vyzván	vyzvat	k5eAaPmNgInS	vyzvat
k	k	k7c3	k
abdikaci	abdikace	k1gFnSc3	abdikace
<g/>
.	.	kIx.	.
</s>
<s>
Odpověď	odpověď	k1gFnSc1	odpověď
nepřicházela	přicházet	k5eNaImAgFnS	přicházet
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
byl	být	k5eAaImAgMnS	být
zatčen	zatknout	k5eAaPmNgMnS	zatknout
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
proti	proti	k7c3	proti
němu	on	k3xPp3gInSc3	on
shromážděna	shromážděn	k2eAgNnPc1d1	shromážděno
obvinění	obvinění	k1gNnSc4	obvinění
a	a	k8xC	a
ve	v	k7c6	v
značně	značně	k6eAd1	značně
nechutném	chutný	k2eNgInSc6d1	nechutný
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
procesu	proces	k1gInSc2	proces
29	[number]	k4	29
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
koncil	koncil	k1gInSc1	koncil
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
<g/>
-li	i	k?	-li
papež	papež	k1gMnSc1	papež
sesazen	sesazen	k2eAgMnSc1d1	sesazen
<g/>
,	,	kIx,	,
nikdo	nikdo	k3yNnSc1	nikdo
bez	bez	k7c2	bez
jeho	on	k3xPp3gNnSc2	on
svolení	svolení	k1gNnSc2	svolení
nesmí	smět	k5eNaImIp3nS	smět
přistoupit	přistoupit	k5eAaPmF	přistoupit
k	k	k7c3	k
volbě	volba	k1gFnSc3	volba
nového	nový	k2eAgMnSc2d1	nový
papeže	papež	k1gMnSc2	papež
<g/>
,	,	kIx,	,
a	a	k8xC	a
nato	nato	k6eAd1	nato
Jana	Jana	k1gFnSc1	Jana
XXIII	XXIII	kA	XXIII
<g/>
.	.	kIx.	.
sesadil	sesadit	k5eAaPmAgMnS	sesadit
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
mohl	moct	k5eAaImAgInS	moct
proces	proces	k1gInSc1	proces
s	s	k7c7	s
Husem	Hus	k1gMnSc7	Hus
pokračovat	pokračovat	k5eAaImF	pokračovat
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
si	se	k3xPyFc3	se
připravil	připravit	k5eAaPmAgMnS	připravit
řeč	řeč	k1gFnSc4	řeč
ke	k	k7c3	k
koncilu	koncil	k1gInSc3	koncil
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Řeč	řeč	k1gFnSc1	řeč
o	o	k7c4	o
míru	míra	k1gFnSc4	míra
(	(	kIx(	(
<g/>
Sermo	Serma	k1gFnSc5	Serma
de	de	k?	de
pace	pac	k1gInSc5	pac
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Koncil	koncil	k1gInSc1	koncil
namísto	namísto	k7c2	namísto
disputace	disputace	k1gFnSc2	disputace
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
rozhodoval	rozhodovat	k5eAaImAgMnS	rozhodovat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
Husovy	Husův	k2eAgFnPc1d1	Husova
nauky	nauka	k1gFnPc1	nauka
souhlasí	souhlasit	k5eAaImIp3nP	souhlasit
s	s	k7c7	s
naukou	nauka	k1gFnSc7	nauka
římskokatolické	římskokatolický	k2eAgFnSc2d1	Římskokatolická
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
či	či	k8xC	či
nikoli	nikoli	k9	nikoli
<g/>
.	.	kIx.	.
</s>
<s>
Husova	Husův	k2eAgFnSc1d1	Husova
pozice	pozice	k1gFnSc1	pozice
byla	být	k5eAaImAgFnS	být
ztížena	ztížit	k5eAaPmNgFnS	ztížit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
se	se	k3xPyFc4	se
nedostavil	dostavit	k5eNaPmAgMnS	dostavit
k	k	k7c3	k
soudu	soud	k1gInSc3	soud
v	v	k7c6	v
předchozím	předchozí	k2eAgInSc6d1	předchozí
procesu	proces	k1gInSc6	proces
u	u	k7c2	u
papežské	papežský	k2eAgFnSc2d1	Papežská
kurie	kurie	k1gFnSc2	kurie
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
označen	označit	k5eAaPmNgMnS	označit
za	za	k7c4	za
tvrdošíjného	tvrdošíjný	k2eAgMnSc4d1	tvrdošíjný
a	a	k8xC	a
heretika	heretik	k1gMnSc4	heretik
<g/>
,	,	kIx,	,
a	a	k8xC	a
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gNnSc2	jeho
obvinění	obvinění	k1gNnSc2	obvinění
týkala	týkat	k5eAaImAgFnS	týkat
víry	víra	k1gFnSc2	víra
<g/>
,	,	kIx,	,
neměl	mít	k5eNaImAgMnS	mít
právo	práv	k2eAgNnSc4d1	právo
odvolání	odvolání	k1gNnSc4	odvolání
ani	ani	k8xC	ani
právo	právo	k1gNnSc4	právo
být	být	k5eAaImF	být
hájen	hájen	k2eAgInSc4d1	hájen
advokátem	advokát	k1gMnSc7	advokát
<g/>
.	.	kIx.	.
</s>
<s>
Prvního	první	k4xOgNnSc2	první
slyšení	slyšení	k1gNnSc2	slyšení
se	se	k3xPyFc4	se
Husovi	Hus	k1gMnSc3	Hus
dostalo	dostat	k5eAaPmAgNnS	dostat
5	[number]	k4	5
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1415	[number]	k4	1415
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
rozlehlost	rozlehlost	k1gFnSc4	rozlehlost
látky	látka	k1gFnSc2	látka
bylo	být	k5eAaImAgNnS	být
nakonec	nakonec	k6eAd1	nakonec
zapotřebí	zapotřebí	k6eAd1	zapotřebí
slyšení	slyšení	k1gNnSc2	slyšení
tří	tři	k4xCgNnPc2	tři
<g/>
.	.	kIx.	.
</s>
<s>
Akta	akta	k1gNnPc1	akta
koncilu	koncil	k1gInSc3	koncil
se	se	k3xPyFc4	se
nezachovala	zachovat	k5eNaPmAgFnS	zachovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
jsou	být	k5eAaImIp3nP	být
dějepisci	dějepisec	k1gMnPc1	dějepisec
odkázáni	odkázat	k5eAaPmNgMnP	odkázat
na	na	k7c4	na
osobní	osobní	k2eAgFnPc4d1	osobní
vzpomínky	vzpomínka	k1gFnPc4	vzpomínka
účastníků	účastník	k1gMnPc2	účastník
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
Petra	Petra	k1gFnSc1	Petra
z	z	k7c2	z
Mladoňovic	Mladoňovice	k1gFnPc2	Mladoňovice
<g/>
.	.	kIx.	.
</s>
<s>
Husovi	Husův	k2eAgMnPc1d1	Husův
průvodci	průvodce	k1gMnPc1	průvodce
<g/>
,	,	kIx,	,
přesvědčeni	přesvědčit	k5eAaPmNgMnP	přesvědčit
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
nevině	nevina	k1gFnSc6	nevina
<g/>
,	,	kIx,	,
poskytli	poskytnout	k5eAaPmAgMnP	poskytnout
koncilu	koncil	k1gInSc2	koncil
Husovy	Husův	k2eAgInPc4d1	Husův
traktáty	traktát	k1gInPc4	traktát
De	De	k?	De
ecclesia	ecclesia	k1gFnSc1	ecclesia
<g/>
,	,	kIx,	,
Contra	Contra	k1gFnSc1	Contra
Stanislaum	Stanislaum	k1gInSc1	Stanislaum
<g/>
,	,	kIx,	,
Contra	Contra	k1gFnSc1	Contra
Palecz	Palecz	k1gMnSc1	Palecz
a	a	k8xC	a
Contra	Contra	k1gFnSc1	Contra
occultum	occultum	k1gNnSc1	occultum
adversarium	adversarium	k1gNnSc4	adversarium
<g/>
.	.	kIx.	.
</s>
<s>
Předložení	předložení	k1gNnSc1	předložení
traktátu	traktát	k1gInSc2	traktát
Contra	Contrum	k1gNnSc2	Contrum
occultum	occultum	k1gNnSc1	occultum
adversarium	adversarium	k1gNnSc4	adversarium
Hus	husa	k1gFnPc2	husa
označil	označit	k5eAaPmAgInS	označit
za	za	k7c4	za
chybu	chyba	k1gFnSc4	chyba
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
neměl	mít	k5eNaImAgMnS	mít
během	během	k7c2	během
slyšení	slyšení	k1gNnSc2	slyšení
možnost	možnost	k1gFnSc4	možnost
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
se	se	k3xPyFc4	se
k	k	k7c3	k
ničemu	nic	k3yNnSc3	nic
jinému	jiný	k2eAgNnSc3d1	jiné
než	než	k8xS	než
k	k	k7c3	k
předloženým	předložený	k2eAgInPc3d1	předložený
článkům	článek	k1gInPc3	článek
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Husovu	Husův	k2eAgFnSc4d1	Husova
zdlouhavost	zdlouhavost	k1gFnSc4	zdlouhavost
ve	v	k7c6	v
výkladu	výklad	k1gInSc6	výklad
ztráceli	ztrácet	k5eAaImAgMnP	ztrácet
přítomní	přítomný	k2eAgMnPc1d1	přítomný
při	při	k7c6	při
slyšení	slyšení	k1gNnSc6	slyšení
trpělivost	trpělivost	k1gFnSc1	trpělivost
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Mladoňovicových	Mladoňovicový	k2eAgFnPc2d1	Mladoňovicový
zpráv	zpráva	k1gFnPc2	zpráva
jednal	jednat	k5eAaImAgMnS	jednat
předsedající	předsedající	k1gMnSc1	předsedající
koncilu	koncil	k1gInSc2	koncil
<g/>
,	,	kIx,	,
kardinál	kardinál	k1gMnSc1	kardinál
Petr	Petr	k1gMnSc1	Petr
z	z	k7c2	z
Ailly	Ailla	k1gFnSc2	Ailla
s	s	k7c7	s
Husem	Hus	k1gMnSc7	Hus
poměrně	poměrně	k6eAd1	poměrně
mírně	mírně	k6eAd1	mírně
<g/>
.	.	kIx.	.
</s>
<s>
Přítomní	přítomný	k1gMnPc1	přítomný
se	se	k3xPyFc4	se
shodovali	shodovat	k5eAaImAgMnP	shodovat
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Husovy	Husův	k2eAgInPc1d1	Husův
předkládané	předkládaný	k2eAgInPc1d1	předkládaný
články	článek	k1gInPc1	článek
vyznívaly	vyznívat	k5eAaImAgInP	vyznívat
lépe	dobře	k6eAd2	dobře
vytrženy	vytrhnout	k5eAaPmNgInP	vytrhnout
z	z	k7c2	z
kontextu	kontext	k1gInSc2	kontext
než	než	k8xS	než
v	v	k7c6	v
souvislostech	souvislost	k1gFnPc6	souvislost
<g/>
.	.	kIx.	.
</s>
<s>
Hus	Hus	k1gMnSc1	Hus
však	však	k9	však
po	po	k7c6	po
Petrovi	Petr	k1gMnSc6	Petr
z	z	k7c2	z
Ailly	Ailla	k1gFnSc2	Ailla
žádal	žádat	k5eAaImAgMnS	žádat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gMnSc3	on
dokázal	dokázat	k5eAaPmAgMnS	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInPc1	jeho
články	článek	k1gInPc1	článek
jsou	být	k5eAaImIp3nP	být
bludné	bludný	k2eAgInPc1d1	bludný
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
vyloženo	vyložit	k5eAaPmNgNnS	vyložit
jako	jako	k9	jako
vzpírání	vzpírání	k1gNnSc1	vzpírání
se	se	k3xPyFc4	se
autoritě	autorita	k1gFnSc3	autorita
koncilu	koncil	k1gInSc2	koncil
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
byla	být	k5eAaImAgFnS	být
žádost	žádost	k1gFnSc1	žádost
zamítnuta	zamítnout	k5eAaPmNgFnS	zamítnout
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
bludných	bludný	k2eAgInPc2d1	bludný
článků	článek	k1gInPc2	článek
zříci	zříct	k5eAaPmF	zříct
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
však	však	k9	však
žádal	žádat	k5eAaImAgMnS	žádat
<g/>
,	,	kIx,	,
že	že	k8xS	že
chce	chtít	k5eAaImIp3nS	chtít
být	být	k5eAaImF	být
poučen	poučit	k5eAaPmNgMnS	poučit
o	o	k7c6	o
opaku	opak	k1gInSc6	opak
<g/>
,	,	kIx,	,
a	a	k8xC	a
pak	pak	k6eAd1	pak
že	že	k8xS	že
odvolá	odvolat	k5eAaPmIp3nS	odvolat
<g/>
;	;	kIx,	;
nakonec	nakonec	k6eAd1	nakonec
odvolat	odvolat	k5eAaPmF	odvolat
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
Zikmund	Zikmund	k1gMnSc1	Zikmund
údajně	údajně	k6eAd1	údajně
po	po	k7c6	po
slyšení	slyšení	k1gNnSc6	slyšení
kardinály	kardinál	k1gMnPc4	kardinál
vybízel	vybízet	k5eAaImAgMnS	vybízet
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Husa	Hus	k1gMnSc4	Hus
upálili	upálit	k5eAaPmAgMnP	upálit
nebo	nebo	k8xC	nebo
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
nějak	nějak	k6eAd1	nějak
naložili	naložit	k5eAaPmAgMnP	naložit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
aby	aby	kYmCp3nP	aby
nedopustili	dopustit	k5eNaPmAgMnP	dopustit
jeho	jeho	k3xOp3gInSc4	jeho
návrat	návrat	k1gInSc4	návrat
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
sněmu	sněm	k1gInSc2	sněm
se	se	k3xPyFc4	se
nevzdávali	vzdávat	k5eNaImAgMnP	vzdávat
naděje	naděje	k1gFnSc1	naděje
<g/>
,	,	kIx,	,
že	že	k8xS	že
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
odvolá	odvolat	k5eAaPmIp3nS	odvolat
<g/>
,	,	kIx,	,
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
mu	on	k3xPp3gMnSc3	on
předložili	předložit	k5eAaPmAgMnP	předložit
zúžený	zúžený	k2eAgInSc4d1	zúžený
seznam	seznam	k1gInSc4	seznam
článků	článek	k1gInPc2	článek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
měl	mít	k5eAaImAgMnS	mít
odvolat	odvolat	k5eAaPmF	odvolat
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
však	však	k9	však
odmítl	odmítnout	k5eAaPmAgInS	odmítnout
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
byly	být	k5eAaImAgInP	být
Husovy	Husův	k2eAgInPc1d1	Husův
spisy	spis	k1gInPc1	spis
odsouzeny	odsouzen	k2eAgInPc1d1	odsouzen
ke	k	k7c3	k
spálení	spálení	k1gNnSc3	spálení
a	a	k8xC	a
Husovo	Husův	k2eAgNnSc4d1	Husovo
učení	učení	k1gNnSc4	učení
bylo	být	k5eAaImAgNnS	být
zavrženo	zavrhnout	k5eAaPmNgNnS	zavrhnout
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
se	se	k3xPyFc4	se
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
podrobit	podrobit	k5eAaPmF	podrobit
autoritě	autorita	k1gFnSc3	autorita
koncilu	koncil	k1gInSc2	koncil
<g/>
,	,	kIx,	,
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
odvolat	odvolat	k5eAaPmF	odvolat
ani	ani	k8xC	ani
pomocí	pomocí	k7c2	pomocí
zjednodušené	zjednodušený	k2eAgFnSc2d1	zjednodušená
a	a	k8xC	a
poměrně	poměrně	k6eAd1	poměrně
volné	volný	k2eAgFnPc4d1	volná
formulace	formulace	k1gFnPc4	formulace
"	"	kIx"	"
<g/>
Tyto	tento	k3xDgInPc4	tento
články	článek	k1gInPc4	článek
jsem	být	k5eAaImIp1nS	být
nikdy	nikdy	k6eAd1	nikdy
nedržel	držet	k5eNaImAgMnS	držet
ani	ani	k8xC	ani
nekázal	kázat	k5eNaImAgMnS	kázat
<g/>
,	,	kIx,	,
a	a	k8xC	a
kdybych	kdyby	kYmCp1nS	kdyby
to	ten	k3xDgNnSc4	ten
byl	být	k5eAaImAgMnS	být
učinil	učinit	k5eAaImAgMnS	učinit
<g/>
,	,	kIx,	,
zle	zle	k6eAd1	zle
bych	by	kYmCp1nS	by
to	ten	k3xDgNnSc4	ten
byl	být	k5eAaImAgMnS	být
učinil	učinit	k5eAaImAgMnS	učinit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
je	on	k3xPp3gMnPc4	on
prohlašuji	prohlašovat	k5eAaImIp1nS	prohlašovat
za	za	k7c4	za
mylné	mylný	k2eAgNnSc4d1	mylné
a	a	k8xC	a
přísahám	přísahat	k5eAaImIp1nS	přísahat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	on	k3xPp3gMnPc4	on
nechci	chtít	k5eNaImIp1nS	chtít
držet	držet	k5eAaImF	držet
ani	ani	k8xC	ani
kázat	kázat	k5eAaImF	kázat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Hus	Hus	k1gMnSc1	Hus
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
formulaci	formulace	k1gFnSc4	formulace
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Dosvědčené	dosvědčený	k2eAgInPc4d1	dosvědčený
bludy	blud	k1gInPc4	blud
jsem	být	k5eAaImIp1nS	být
nikdy	nikdy	k6eAd1	nikdy
nekázal	kázat	k5eNaImAgMnS	kázat
<g/>
,	,	kIx,	,
nedržel	držet	k5eNaImAgMnS	držet
a	a	k8xC	a
netvrdil	tvrdit	k5eNaImAgMnS	tvrdit
<g/>
.	.	kIx.	.
</s>
<s>
Nebudu	být	k5eNaImBp1nS	být
je	on	k3xPp3gNnPc4	on
kázat	kázat	k5eAaImF	kázat
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
držet	držet	k5eAaImF	držet
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
tvrdit	tvrdit	k5eAaImF	tvrdit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Ta	ten	k3xDgFnSc1	ten
ale	ale	k9	ale
nebyla	být	k5eNaImAgFnS	být
přijata	přijmout	k5eAaPmNgFnS	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
přátelům	přítel	k1gMnPc3	přítel
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
jej	on	k3xPp3gMnSc4	on
ještě	ještě	k9	ještě
snažili	snažit	k5eAaImAgMnP	snažit
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
<g/>
,	,	kIx,	,
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
ochoten	ochoten	k2eAgInSc1d1	ochoten
pokorně	pokorně	k6eAd1	pokorně
odvolat	odvolat	k5eAaPmF	odvolat
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nS	kdyby
se	se	k3xPyFc4	se
cítil	cítit	k5eAaImAgMnS	cítit
vinný	vinný	k2eAgMnSc1d1	vinný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
že	že	k8xS	že
žádá	žádat	k5eAaImIp3nS	žádat
důkaz	důkaz	k1gInSc1	důkaz
z	z	k7c2	z
Písma	písmo	k1gNnSc2	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Sněm	sněm	k1gInSc1	sněm
tak	tak	k9	tak
dle	dle	k7c2	dle
svého	svůj	k3xOyFgNnSc2	svůj
mínění	mínění	k1gNnSc2	mínění
vyčerpal	vyčerpat	k5eAaPmAgInS	vyčerpat
možnosti	možnost	k1gFnPc4	možnost
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
odvrátit	odvrátit	k5eAaPmF	odvrátit
Husa	Hus	k1gMnSc4	Hus
od	od	k7c2	od
trestů	trest	k1gInPc2	trest
předepsaných	předepsaný	k2eAgInPc2d1	předepsaný
právem	právem	k6eAd1	právem
<g/>
,	,	kIx,	,
a	a	k8xC	a
jednal	jednat	k5eAaImAgMnS	jednat
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
podle	podle	k7c2	podle
církevních	církevní	k2eAgInPc2d1	církevní
zákonů	zákon	k1gInPc2	zákon
jako	jako	k8xC	jako
se	s	k7c7	s
zatvrzelým	zatvrzelý	k2eAgMnSc7d1	zatvrzelý
kacířem	kacíř	k1gMnSc7	kacíř
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
knížce	knížka	k1gFnSc6	knížka
dr	dr	kA	dr
<g/>
.	.	kIx.	.
J.	J.	kA	J.
V.	V.	kA	V.
Šimáka	Šimák	k1gMnSc2	Šimák
Hus	Hus	k1gMnSc1	Hus
a	a	k8xC	a
doba	doba	k1gFnSc1	doba
před	před	k7c7	před
ním	on	k3xPp3gInSc7	on
(	(	kIx(	(
<g/>
nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
F.	F.	kA	F.
Topič	topič	k1gMnSc1	topič
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
píše	psát	k5eAaImIp3nS	psát
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Hus	Hus	k1gMnSc1	Hus
byl	být	k5eAaImAgMnS	být
vržen	vrhnout	k5eAaPmNgMnS	vrhnout
do	do	k7c2	do
temného	temný	k2eAgInSc2d1	temný
<g/>
,	,	kIx,	,
vlhkého	vlhký	k2eAgMnSc2d1	vlhký
a	a	k8xC	a
smrdutého	smrdutý	k2eAgNnSc2d1	smrduté
vězení	vězení	k1gNnSc2	vězení
v	v	k7c6	v
dominikánském	dominikánský	k2eAgInSc6d1	dominikánský
klášteře	klášter	k1gInSc6	klášter
u	u	k7c2	u
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
brzo	brzo	k6eAd1	brzo
těžce	těžce	k6eAd1	těžce
rozstonal	rozstonat	k5eAaPmAgMnS	rozstonat
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
to	ten	k3xDgNnSc4	ten
vše	všechen	k3xTgNnSc1	všechen
komise	komise	k1gFnSc1	komise
<g/>
,	,	kIx,	,
zřízená	zřízený	k2eAgFnSc1d1	zřízená
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
vyšetřování	vyšetřování	k1gNnSc3	vyšetřování
<g/>
,	,	kIx,	,
posílala	posílat	k5eAaImAgFnS	posílat
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
svědky	svědek	k1gMnPc4	svědek
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
před	před	k7c7	před
ním	on	k3xPp3gNnSc7	on
vypovídali	vypovídat	k5eAaPmAgMnP	vypovídat
a	a	k8xC	a
přísahali	přísahat	k5eAaImAgMnP	přísahat
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yQnSc2	což
rozčilování	rozčilování	k1gNnSc6	rozčilování
chorobu	choroba	k1gFnSc4	choroba
Husovu	Husův	k2eAgFnSc4d1	Husova
ještě	ještě	k9	ještě
zvýšilo	zvýšit	k5eAaPmAgNnS	zvýšit
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
kolísal	kolísat	k5eAaImAgInS	kolísat
již	již	k6eAd1	již
mezi	mezi	k7c7	mezi
životem	život	k1gInSc7	život
a	a	k8xC	a
smrtí	smrt	k1gFnSc7	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Připuštěni	připuštěn	k2eAgMnPc1d1	připuštěn
však	však	k9	však
byli	být	k5eAaImAgMnP	být
toliko	toliko	k6eAd1	toliko
svědkové	svědek	k1gMnPc1	svědek
proti	proti	k7c3	proti
Husovi	Hus	k1gMnSc3	Hus
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
obhájci	obhájce	k1gMnPc1	obhájce
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
snažně	snažně	k6eAd1	snažně
Hus	Hus	k1gMnSc1	Hus
prosil	prosít	k5eAaPmAgMnS	prosít
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
osobní	osobní	k2eAgFnPc1d1	osobní
přímluvy	přímluva	k1gFnPc1	přímluva
Zikmundovy	Zikmundův	k2eAgFnPc1d1	Zikmundova
vymohly	vymoct	k5eAaPmAgFnP	vymoct
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
vězeň	vězeň	k1gMnSc1	vězeň
přenesen	přenést	k5eAaPmNgMnS	přenést
do	do	k7c2	do
slušnější	slušný	k2eAgFnSc2d2	slušnější
cely	cela	k1gFnSc2	cela
klášterní	klášterní	k2eAgFnSc2d1	klášterní
a	a	k8xC	a
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
poskytnuta	poskytnut	k2eAgFnSc1d1	poskytnuta
lékařská	lékařský	k2eAgFnSc1d1	lékařská
pomoc	pomoc	k1gFnSc1	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
průvodci	průvodce	k1gMnPc1	průvodce
podplatili	podplatit	k5eAaPmAgMnP	podplatit
stráže	stráž	k1gFnPc4	stráž
a	a	k8xC	a
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
toho	ten	k3xDgMnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Hus	Hus	k1gMnSc1	Hus
mohl	moct	k5eAaImAgMnS	moct
ze	z	k7c2	z
žaláře	žalář	k1gInSc2	žalář
posílati	posílat	k5eAaImF	posílat
listiny	listina	k1gFnSc2	listina
a	a	k8xC	a
též	též	k9	též
přijímati	přijímat	k5eAaImF	přijímat
<g/>
.	.	kIx.	.
</s>
<s>
"	"	kIx"	"
6	[number]	k4	6
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1415	[number]	k4	1415
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
XV	XV	kA	XV
<g/>
.	.	kIx.	.
zasedání	zasedání	k1gNnPc1	zasedání
sněmu	sněm	k1gInSc2	sněm
v	v	k7c6	v
případu	případ	k1gInSc6	případ
mistra	mistr	k1gMnSc2	mistr
Jana	Jan	k1gMnSc2	Jan
Husa	Hus	k1gMnSc2	Hus
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
přečten	přečíst	k5eAaPmNgInS	přečíst
průběh	průběh	k1gInSc1	průběh
procesu	proces	k1gInSc2	proces
a	a	k8xC	a
Husovy	Husův	k2eAgFnSc2d1	Husova
nauky	nauka	k1gFnSc2	nauka
označeny	označen	k2eAgFnPc1d1	označena
za	za	k7c4	za
heretické	heretický	k2eAgNnSc4d1	heretické
<g/>
.	.	kIx.	.
</s>
<s>
Hus	Hus	k1gMnSc1	Hus
se	se	k3xPyFc4	se
chtěl	chtít	k5eAaImAgMnS	chtít
znovu	znovu	k6eAd1	znovu
hájit	hájit	k5eAaImF	hájit
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
byl	být	k5eAaImAgMnS	být
umlčen	umlčet	k5eAaPmNgMnS	umlčet
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
obsahem	obsah	k1gInSc7	obsah
jednání	jednání	k1gNnSc2	jednání
již	již	k6eAd1	již
nebylo	být	k5eNaImAgNnS	být
slyšení	slyšení	k1gNnSc1	slyšení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vynesení	vynesení	k1gNnSc1	vynesení
rozsudku	rozsudek	k1gInSc2	rozsudek
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
byl	být	k5eAaImAgMnS	být
obviněn	obvinit	k5eAaPmNgMnS	obvinit
z	z	k7c2	z
šíření	šíření	k1gNnSc2	šíření
Viklefových	Viklefův	k2eAgFnPc2d1	Viklefova
nauk	nauka	k1gFnPc2	nauka
<g/>
,	,	kIx,	,
ze	z	k7c2	z
snahy	snaha	k1gFnSc2	snaha
bránit	bránit	k5eAaImF	bránit
odsouzení	odsouzení	k1gNnSc4	odsouzení
jeho	jeho	k3xOp3gInPc2	jeho
článků	článek	k1gInPc2	článek
<g/>
,	,	kIx,	,
z	z	k7c2	z
učení	učení	k1gNnSc2	učení
jeho	jeho	k3xOp3gFnSc2	jeho
nauky	nauka	k1gFnSc2	nauka
<g/>
.	.	kIx.	.
</s>
<s>
Husovy	Husův	k2eAgFnSc2d1	Husova
knihy	kniha	k1gFnSc2	kniha
bylo	být	k5eAaImAgNnS	být
nařízeno	nařídit	k5eAaPmNgNnS	nařídit
spálit	spálit	k5eAaPmF	spálit
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
byl	být	k5eAaImAgMnS	být
prohlášen	prohlásit	k5eAaPmNgMnS	prohlásit
za	za	k7c4	za
zatvrzelého	zatvrzelý	k2eAgMnSc4d1	zatvrzelý
kacíře	kacíř	k1gMnSc4	kacíř
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
proveden	provést	k5eAaPmNgInS	provést
obřad	obřad	k1gInSc1	obřad
zbavení	zbavení	k1gNnSc2	zbavení
kněžského	kněžský	k2eAgInSc2d1	kněžský
úřadu	úřad	k1gInSc2	úřad
<g/>
,	,	kIx,	,
zbavení	zbavení	k1gNnSc2	zbavení
všech	všecek	k3xTgNnPc2	všecek
duchovenských	duchovenský	k2eAgNnPc2d1	duchovenské
práv	právo	k1gNnPc2	právo
<g/>
,	,	kIx,	,
narušení	narušení	k1gNnSc1	narušení
tonzury	tonzura	k1gFnSc2	tonzura
<g/>
,	,	kIx,	,
nasazení	nasazení	k1gNnSc2	nasazení
kacířské	kacířský	k2eAgFnSc2d1	kacířská
čepice	čepice	k1gFnSc2	čepice
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
ustrojený	ustrojený	k2eAgMnSc1d1	ustrojený
mistr	mistr	k1gMnSc1	mistr
byl	být	k5eAaImAgMnS	být
vydán	vydat	k5eAaPmNgMnS	vydat
rameni	rameno	k1gNnSc3	rameno
světskému	světský	k2eAgNnSc3d1	světské
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
však	však	k9	však
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgMnS	být
zabit	zabít	k5eAaPmNgMnS	zabít
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
žalářován	žalářovat	k5eAaImNgInS	žalářovat
až	až	k6eAd1	až
do	do	k7c2	do
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
jak	jak	k8xC	jak
Reichenthal	Reichenthal	k1gMnSc1	Reichenthal
dává	dávat	k5eAaImIp3nS	dávat
zprávu	zpráva	k1gFnSc4	zpráva
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
dass	dass	k6eAd1	dass
man	man	k1gMnSc1	man
ihn	ihn	k?	ihn
nicht	nicht	k2eAgMnSc1d1	nicht
toedten	toedten	k2eAgMnSc1d1	toedten
soelt	soelt	k1gMnSc1	soelt
und	und	k?	und
ihn	ihn	k?	ihn
sonst	sonst	k1gMnSc1	sonst
behielt	behielt	k1gMnSc1	behielt
und	und	k?	und
ihm	ihm	k?	ihm
einen	einen	k2eAgInSc4d1	einen
ewigen	ewigen	k1gInSc4	ewigen
Kerker	Kerker	k1gMnSc1	Kerker
gab	gab	k?	gab
<g />
.	.	kIx.	.
</s>
<s>
<g/>
<g/>
"	"	kIx"	"
Avšak	avšak	k8xC	avšak
podotknouti	podotknout	k5eAaPmF	podotknout
sluší	slušet	k5eAaImIp3nS	slušet
<g/>
,	,	kIx,	,
že	že	k8xS	že
prosba	prosba	k1gFnSc1	prosba
tato	tento	k3xDgFnSc1	tento
byla	být	k5eAaImAgFnS	být
pouhou	pouhý	k2eAgFnSc7d1	pouhá
formulí	formule	k1gFnSc7	formule
soudnou	soudný	k2eAgFnSc7d1	soudná
<g/>
,	,	kIx,	,
jsouc	být	k5eAaImSgNnS	být
jako	jako	k9	jako
trvalým	trvalý	k2eAgNnSc7d1	trvalé
svědectvím	svědectví	k1gNnSc7	svědectví
<g/>
,	,	kIx,	,
že	že	k8xS	že
církev	církev	k1gFnSc1	církev
vlastně	vlastně	k9	vlastně
nikoho	nikdo	k3yNnSc2	nikdo
ku	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
neodsuzuje	odsuzovat	k5eNaImIp3nS	odsuzovat
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
sobě	se	k3xPyFc3	se
žádá	žádat	k5eAaImIp3nS	žádat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
kdo	kdo	k3yQnSc1	kdo
života	život	k1gInSc2	život
zbaven	zbavit	k5eAaPmNgMnS	zbavit
byl	být	k5eAaImAgMnS	být
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
i	i	k9	i
pobloudil	pobloudit	k5eAaPmAgMnS	pobloudit
u	u	k7c2	u
víře	víra	k1gFnSc6	víra
a	a	k8xC	a
urputně	urputně	k6eAd1	urputně
ve	v	k7c6	v
bludu	blud	k1gInSc6	blud
trval	trvat	k5eAaImAgInS	trvat
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
Hus	Hus	k1gMnSc1	Hus
světskému	světský	k2eAgInSc3d1	světský
rameni	rameno	k1gNnSc6	rameno
vydán	vydán	k2eAgMnSc1d1	vydán
<g/>
,	,	kIx,	,
odevzdal	odevzdat	k5eAaPmAgMnS	odevzdat
ho	on	k3xPp3gInSc4	on
Zigmund	Zigmund	k1gInSc4	Zigmund
falckraběti	falckrabě	k1gMnSc3	falckrabě
Ludvíkovi	Ludvík	k1gMnSc3	Ludvík
a	a	k8xC	a
tento	tento	k3xDgInSc4	tento
purkrabímu	purkrabí	k1gMnSc3	purkrabí
města	město	k1gNnSc2	město
Kostnice	Kostnice	k1gFnSc2	Kostnice
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
naložilo	naložit	k5eAaPmAgNnS	naložit
vedle	vedle	k7c2	vedle
stávajícího	stávající	k2eAgMnSc2d1	stávající
tehdáž	tehdáž	k?	tehdáž
zákona	zákon	k1gInSc2	zákon
trestného	trestný	k2eAgInSc2d1	trestný
<g/>
,	,	kIx,	,
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
doložením	doložení	k1gNnSc7	doložení
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgMnS	být
se	s	k7c7	s
vším	všecek	k3xTgNnSc7	všecek
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
má	mít	k5eAaImIp3nS	mít
<g/>
,	,	kIx,	,
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
uvržen	uvržen	k2eAgMnSc1d1	uvržen
<g/>
.	.	kIx.	.
</s>
<s>
Popraviště	popraviště	k1gNnSc1	popraviště
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
za	za	k7c7	za
městskou	městský	k2eAgFnSc7d1	městská
branou	brána	k1gFnSc7	brána
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
k	k	k7c3	k
hradu	hrad	k1gInSc3	hrad
Gottlieben	Gottliebna	k1gFnPc2	Gottliebna
<g/>
.	.	kIx.	.
</s>
<s>
Hus	Hus	k1gMnSc1	Hus
cestou	cesta	k1gFnSc7	cesta
zpíval	zpívat	k5eAaImAgMnS	zpívat
mariánskou	mariánský	k2eAgFnSc4d1	Mariánská
píseň	píseň	k1gFnSc4	píseň
Christi	Christ	k1gMnPc1	Christ
virgo	virgo	k1gNnSc4	virgo
dilectissima	dilectissimum	k1gNnSc2	dilectissimum
a	a	k8xC	a
přesvědčoval	přesvědčovat	k5eAaImAgMnS	přesvědčovat
lid	lid	k1gInSc4	lid
<g/>
,	,	kIx,	,
že	že	k8xS	že
neučil	učit	k5eNaImAgInS	učit
bludům	blud	k1gInPc3	blud
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
byl	být	k5eAaImAgMnS	být
přivázán	přivázán	k2eAgMnSc1d1	přivázán
k	k	k7c3	k
hranici	hranice	k1gFnSc3	hranice
<g/>
,	,	kIx,	,
přijel	přijet	k5eAaPmAgMnS	přijet
Zikmundův	Zikmundův	k2eAgMnSc1d1	Zikmundův
maršálek	maršálek	k1gMnSc1	maršálek
a	a	k8xC	a
nabídli	nabídnout	k5eAaPmAgMnP	nabídnout
Husovi	Husův	k2eAgMnPc1d1	Husův
z	z	k7c2	z
králova	králův	k2eAgInSc2d1	králův
rozkazu	rozkaz	k1gInSc2	rozkaz
odvolání	odvolání	k1gNnSc2	odvolání
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
zachoval	zachovat	k5eAaPmAgMnS	zachovat
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Hus	Hus	k1gMnSc1	Hus
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
Petra	Petr	k1gMnSc2	Petr
z	z	k7c2	z
Mladoňovic	Mladoňovice	k1gFnPc2	Mladoňovice
<g/>
,	,	kIx,	,
těmito	tento	k3xDgNnPc7	tento
slovy	slovo	k1gNnPc7	slovo
<g/>
:	:	kIx,	:
Bůh	bůh	k1gMnSc1	bůh
jest	být	k5eAaImIp3nS	být
mi	já	k3xPp1nSc3	já
svědek	svědek	k1gMnSc1	svědek
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
mi	já	k3xPp1nSc3	já
falešně	falešně	k6eAd1	falešně
připisuje	připisovat	k5eAaImIp3nS	připisovat
a	a	k8xC	a
skrze	skrze	k?	skrze
falešné	falešný	k2eAgMnPc4d1	falešný
svědky	svědek	k1gMnPc4	svědek
přičítá	přičítat	k5eAaImIp3nS	přičítat
<g/>
,	,	kIx,	,
jsem	být	k5eAaImIp1nS	být
ani	ani	k9	ani
neučil	učít	k5eNaPmAgMnS	učít
ani	ani	k8xC	ani
nekázal	kázat	k5eNaImAgMnS	kázat
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
předním	přední	k2eAgInSc7d1	přední
úmyslem	úmysl	k1gInSc7	úmysl
mého	můj	k3xOp1gNnSc2	můj
kázání	kázání	k1gNnSc2	kázání
anem	anem	k6eAd1	anem
jiných	jiná	k1gFnPc2	jiná
mých	můj	k3xOp1gInPc2	můj
skutků	skutek	k1gInPc2	skutek
neb	neb	k8xC	neb
písem	písmo	k1gNnPc2	písmo
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
abych	aby	kYmCp1nS	aby
jen	jen	k9	jen
mohl	moct	k5eAaImAgInS	moct
odvrátit	odvrátit	k5eAaPmF	odvrátit
lidi	člověk	k1gMnPc4	člověk
od	od	k7c2	od
hříchu	hřích	k1gInSc2	hřích
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
pak	pak	k9	pak
pravdě	pravda	k1gFnSc3	pravda
evangelia	evangelium	k1gNnSc2	evangelium
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
jsem	být	k5eAaImIp1nS	být
učil	učit	k5eAaImAgMnS	učit
psal	psát	k5eAaImAgInS	psát
a	a	k8xC	a
kázal	kázat	k5eAaImAgInS	kázat
<g/>
,	,	kIx,	,
ze	z	k7c2	z
slov	slovo	k1gNnPc2	slovo
a	a	k8xC	a
výkladů	výklad	k1gInPc2	výklad
svatých	svatý	k2eAgMnPc2d1	svatý
doktorů	doktor	k1gMnPc2	doktor
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
vesele	vesele	k6eAd1	vesele
chci	chtít	k5eAaImIp1nS	chtít
zemříti	zemřít	k5eAaPmF	zemřít
<g/>
.	.	kIx.	.
a	a	k8xC	a
hranice	hranice	k1gFnSc1	hranice
byla	být	k5eAaImAgFnS	být
zapálena	zapálit	k5eAaPmNgFnS	zapálit
<g/>
.	.	kIx.	.
</s>
<s>
Hranice	hranice	k1gFnSc1	hranice
i	i	k9	i
s	s	k7c7	s
Husovými	Husův	k2eAgInPc7d1	Husův
ostatky	ostatek	k1gInPc7	ostatek
byla	být	k5eAaImAgNnP	být
po	po	k7c4	po
upálení	upálení	k1gNnSc4	upálení
vhozena	vhozen	k2eAgFnSc1d1	vhozena
do	do	k7c2	do
řeky	řeka	k1gFnSc2	řeka
Rýna	Rýn	k1gInSc2	Rýn
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
jeho	jeho	k3xOp3gFnSc4	jeho
přívrženci	přívrženec	k1gMnPc1	přívrženec
neudělali	udělat	k5eNaPmAgMnP	udělat
z	z	k7c2	z
hrobu	hrob	k1gInSc2	hrob
poutní	poutní	k2eAgNnSc4d1	poutní
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Husových	Husových	k2eAgMnPc2d1	Husových
přívrženců	přívrženec	k1gMnPc2	přívrženec
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
způsobila	způsobit	k5eAaPmAgFnS	způsobit
zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
upálení	upálení	k1gNnSc6	upálení
hněv	hněv	k1gInSc4	hněv
a	a	k8xC	a
odpor	odpor	k1gInSc4	odpor
vůči	vůči	k7c3	vůči
těm	ten	k3xDgMnPc3	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
jej	on	k3xPp3gMnSc4	on
vydali	vydat	k5eAaPmAgMnP	vydat
na	na	k7c4	na
smrt	smrt	k1gFnSc4	smrt
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
ke	k	k7c3	k
koncilu	koncil	k1gInSc3	koncil
<g/>
.	.	kIx.	.
</s>
<s>
Převládlo	převládnout	k5eAaPmAgNnS	převládnout
mínění	mínění	k1gNnSc1	mínění
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
odsouzen	odsouzet	k5eAaImNgMnS	odsouzet
především	především	k9	především
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
kazatelskou	kazatelský	k2eAgFnSc4d1	kazatelská
činnost	činnost	k1gFnSc4	činnost
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
hájil	hájit	k5eAaImAgMnS	hájit
pravdu	pravda	k1gFnSc4	pravda
a	a	k8xC	a
neodchýlil	odchýlit	k5eNaPmAgMnS	odchýlit
se	se	k3xPyFc4	se
od	od	k7c2	od
učení	učení	k1gNnSc2	učení
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
odsouzení	odsouzení	k1gNnSc1	odsouzení
bylo	být	k5eAaImAgNnS	být
chápáno	chápat	k5eAaImNgNnS	chápat
i	i	k9	i
jako	jako	k9	jako
odsouzení	odsouzení	k1gNnSc4	odsouzení
Českého	český	k2eAgNnSc2d1	české
království	království	k1gNnSc2	království
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc2	jeho
pořádků	pořádek	k1gInPc2	pořádek
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
2	[number]	k4	2
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1415	[number]	k4	1415
byl	být	k5eAaImAgMnS	být
koncilu	koncil	k1gInSc2	koncil
v	v	k7c6	v
Kostnici	Kostnice	k1gFnSc6	Kostnice
odeslán	odeslat	k5eAaPmNgInS	odeslat
protesní	protesní	k2eAgInSc1d1	protesní
list	list	k1gInSc1	list
<g/>
,	,	kIx,	,
opatřený	opatřený	k2eAgInSc1d1	opatřený
452	[number]	k4	452
pečetěmi	pečeť	k1gFnPc7	pečeť
českých	český	k2eAgMnPc2d1	český
a	a	k8xC	a
moravských	moravský	k2eAgMnPc2d1	moravský
šlechticů	šlechtic	k1gMnPc2	šlechtic
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
týden	týden	k1gInSc4	týden
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1415	[number]	k4	1415
vydala	vydat	k5eAaPmAgFnS	vydat
i	i	k9	i
pražská	pražský	k2eAgFnSc1d1	Pražská
univerzita	univerzita	k1gFnSc1	univerzita
osvědčení	osvědčení	k1gNnSc4	osvědčení
o	o	k7c6	o
bezúhonnosti	bezúhonnost	k1gFnSc6	bezúhonnost
a	a	k8xC	a
pravověrnosti	pravověrnost	k1gFnSc6	pravověrnost
Jana	Jan	k1gMnSc2	Jan
Husa	Hus	k1gMnSc2	Hus
<g/>
.	.	kIx.	.
</s>
<s>
Doslov	doslov	k1gInSc1	doslov
k	k	k7c3	k
Husově	Husův	k2eAgFnSc3d1	Husova
procesu	proces	k1gInSc2	proces
tvořilo	tvořit	k5eAaImAgNnS	tvořit
odsouzení	odsouzení	k1gNnSc1	odsouzení
Jeronýma	Jeroným	k1gMnSc2	Jeroným
Pražského	pražský	k2eAgMnSc2d1	pražský
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1415	[number]	k4	1415
dostavil	dostavit	k5eAaPmAgMnS	dostavit
do	do	k7c2	do
Kostnice	Kostnice	k1gFnSc2	Kostnice
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
jej	on	k3xPp3gInSc4	on
měl	mít	k5eAaImAgMnS	mít
k	k	k7c3	k
rychlému	rychlý	k2eAgInSc3d1	rychlý
odchodu	odchod	k1gInSc3	odchod
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
Jeroným	Jeroným	k1gMnSc1	Jeroným
kázal	kázat	k5eAaImAgMnS	kázat
v	v	k7c6	v
Bavorsku	Bavorsko	k1gNnSc6	Bavorsko
proti	proti	k7c3	proti
koncilu	koncil	k1gInSc3	koncil
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
zatčen	zatknout	k5eAaPmNgMnS	zatknout
a	a	k8xC	a
dopraven	dopravit	k5eAaPmNgInS	dopravit
do	do	k7c2	do
Kostnice	Kostnice	k1gFnSc2	Kostnice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
uznal	uznat	k5eAaPmAgInS	uznat
odsouzení	odsouzení	k1gNnSc4	odsouzení
Viklefovo	Viklefův	k2eAgNnSc4d1	Viklefovo
i	i	k8xC	i
Husovo	Husův	k2eAgNnSc4d1	Husovo
<g/>
.	.	kIx.	.
</s>
<s>
Nebyl	být	k5eNaImAgInS	být
však	však	k9	však
propuštěn	propuštěn	k2eAgMnSc1d1	propuštěn
s	s	k7c7	s
podezřením	podezření	k1gNnSc7	podezření
<g/>
,	,	kIx,	,
že	že	k8xS	že
soudce	soudce	k1gMnSc1	soudce
obelstil	obelstít	k5eAaPmAgMnS	obelstít
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
novém	nový	k2eAgNnSc6d1	nové
vyšetřování	vyšetřování	k1gNnSc6	vyšetřování
se	se	k3xPyFc4	se
přiznal	přiznat	k5eAaPmAgMnS	přiznat
k	k	k7c3	k
Viklefovým	Viklefův	k2eAgFnPc3d1	Viklefova
i	i	k8xC	i
Husovým	Husův	k2eAgFnPc3d1	Husova
naukám	nauka	k1gFnPc3	nauka
a	a	k8xC	a
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
největší	veliký	k2eAgInSc4d3	veliký
hřích	hřích	k1gInSc4	hřích
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Jana	Jan	k1gMnSc2	Jan
Husa	Hus	k1gMnSc2	Hus
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
dobrého	dobrý	k2eAgMnSc2d1	dobrý
a	a	k8xC	a
svatého	svatý	k2eAgMnSc2d1	svatý
muže	muž	k1gMnSc2	muž
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
zřekl	zřeknout	k5eAaPmAgMnS	zřeknout
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1416	[number]	k4	1416
byl	být	k5eAaImAgMnS	být
odsouzen	odsoudit	k5eAaPmNgMnS	odsoudit
a	a	k8xC	a
upálen	upálit	k5eAaPmNgMnS	upálit
i	i	k9	i
mistr	mistr	k1gMnSc1	mistr
Jeroným	Jeroným	k1gMnSc1	Jeroným
Pražský	pražský	k2eAgMnSc1d1	pražský
<g/>
.	.	kIx.	.
</s>
<s>
Smrt	smrt	k1gFnSc1	smrt
dvou	dva	k4xCgInPc2	dva
českých	český	k2eAgMnPc2d1	český
kazatelů	kazatel	k1gMnPc2	kazatel
způsobila	způsobit	k5eAaPmAgFnS	způsobit
eskalaci	eskalace	k1gFnSc4	eskalace
napětí	napětí	k1gNnSc2	napětí
uvnitř	uvnitř	k7c2	uvnitř
Českého	český	k2eAgNnSc2d1	české
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
posléze	posléze	k6eAd1	posléze
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
husitské	husitský	k2eAgFnSc3d1	husitská
revoluci	revoluce	k1gFnSc3	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Záhy	záhy	k6eAd1	záhy
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
objevovat	objevovat	k5eAaImF	objevovat
nejrůznější	různý	k2eAgInPc4d3	nejrůznější
texty	text	k1gInPc4	text
<g/>
,	,	kIx,	,
popisující	popisující	k2eAgInPc4d1	popisující
činy	čin	k1gInPc4	čin
<g/>
,	,	kIx,	,
myšlenky	myšlenka	k1gFnPc4	myšlenka
i	i	k8xC	i
smrt	smrt	k1gFnSc1	smrt
obou	dva	k4xCgFnPc2	dva
odsouzených	odsouzená	k1gFnPc2	odsouzená
<g/>
.	.	kIx.	.
</s>
<s>
Sloužily	sloužit	k5eAaImAgInP	sloužit
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k9	i
liturgickým	liturgický	k2eAgInPc3d1	liturgický
účelům	účel	k1gInPc3	účel
<g/>
.	.	kIx.	.
</s>
<s>
Památka	památka	k1gFnSc1	památka
Jana	Jan	k1gMnSc2	Jan
Husa	Hus	k1gMnSc2	Hus
byla	být	k5eAaImAgNnP	být
slavena	slavit	k5eAaImNgNnP	slavit
již	již	k6eAd1	již
při	při	k7c6	při
prvním	první	k4xOgInSc6	první
výročí	výročí	k1gNnSc6	výročí
jeho	jeho	k3xOp3gNnSc2	jeho
upálení	upálení	k1gNnSc2	upálení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1416	[number]	k4	1416
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
však	však	k9	však
došlo	dojít	k5eAaPmAgNnS	dojít
i	i	k9	i
k	k	k7c3	k
šíření	šíření	k1gNnSc3	šíření
negativních	negativní	k2eAgFnPc2d1	negativní
zpráv	zpráva	k1gFnPc2	zpráva
<g/>
,	,	kIx,	,
popisujících	popisující	k2eAgFnPc2d1	popisující
Jana	Jan	k1gMnSc2	Jan
Husa	Hus	k1gMnSc2	Hus
jako	jako	k8xC	jako
zatvrzelého	zatvrzelý	k2eAgMnSc2d1	zatvrzelý
kacíře	kacíř	k1gMnSc2	kacíř
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
vzdát	vzdát	k5eAaPmF	vzdát
svých	svůj	k3xOyFgInPc2	svůj
bludů	blud	k1gInPc2	blud
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
spravedlivě	spravedlivě	k6eAd1	spravedlivě
odsouzen	odsoudit	k5eAaPmNgMnS	odsoudit
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
bylo	být	k5eAaImAgNnS	být
i	i	k9	i
varování	varování	k1gNnSc1	varování
před	před	k7c7	před
šířením	šíření	k1gNnSc7	šíření
jeho	jeho	k3xOp3gInPc2	jeho
spisů	spis	k1gInPc2	spis
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgNnPc1	dva
hlediska	hledisko	k1gNnPc1	hledisko
jsou	být	k5eAaImIp3nP	být
dobře	dobře	k6eAd1	dobře
zachycena	zachytit	k5eAaPmNgFnS	zachytit
dobovými	dobový	k2eAgInPc7d1	dobový
písemnými	písemný	k2eAgInPc7d1	písemný
prameny	pramen	k1gInPc7	pramen
i	i	k8xC	i
obrazy	obraz	k1gInPc7	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
bývá	bývat	k5eAaImIp3nS	bývat
hodnocen	hodnotit	k5eAaImNgMnS	hodnotit
jako	jako	k9	jako
předchůdce	předchůdce	k1gMnSc1	předchůdce
protestantské	protestantský	k2eAgFnSc2d1	protestantská
reformace	reformace	k1gFnSc2	reformace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
začal	začít	k5eAaPmAgMnS	začít
být	být	k5eAaImF	být
díky	díky	k7c3	díky
národnímu	národní	k2eAgInSc3d1	národní
obrození	obrození	k1gNnSc4	obrození
vnímán	vnímat	k5eAaImNgMnS	vnímat
jako	jako	k9	jako
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
velkých	velký	k2eAgFnPc2d1	velká
postav	postava	k1gFnPc2	postava
českých	český	k2eAgFnPc2d1	Česká
dějin	dějiny	k1gFnPc2	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Organizovány	organizován	k2eAgFnPc1d1	organizována
byly	být	k5eAaImAgFnP	být
poutě	pouť	k1gFnPc1	pouť
na	na	k7c4	na
místa	místo	k1gNnPc4	místo
spojená	spojený	k2eAgFnSc1d1	spojená
s	s	k7c7	s
Husem	Hus	k1gMnSc7	Hus
i	i	k8xC	i
husitstvím	husitství	k1gNnSc7	husitství
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
dvou	dva	k4xCgNnPc2	dva
velkých	velký	k2eAgNnPc2d1	velké
poutí	poutí	k1gNnPc2	poutí
do	do	k7c2	do
Kostnice	Kostnice	k1gFnSc2	Kostnice
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1868	[number]	k4	1868
a	a	k8xC	a
1883	[number]	k4	1883
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hojně	hojně	k6eAd1	hojně
byly	být	k5eAaImAgFnP	být
též	též	k9	též
budovány	budován	k2eAgInPc4d1	budován
pomníky	pomník	k1gInPc4	pomník
Mistra	mistr	k1gMnSc2	mistr
Jana	Jan	k1gMnSc2	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Tradice	tradice	k1gFnSc1	tradice
Jana	Jan	k1gMnSc2	Jan
Husa	Hus	k1gMnSc2	Hus
byla	být	k5eAaImAgFnS	být
podporována	podporovat	k5eAaImNgFnS	podporovat
i	i	k9	i
po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
Československa	Československo	k1gNnSc2	Československo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
s	s	k7c7	s
husitstvím	husitství	k1gNnSc7	husitství
byla	být	k5eAaImAgFnS	být
posléze	posléze	k6eAd1	posléze
vyzdvihována	vyzdvihovat	k5eAaImNgFnS	vyzdvihovat
také	také	k9	také
komunistickým	komunistický	k2eAgInSc7d1	komunistický
režimem	režim	k1gInSc7	režim
<g/>
.	.	kIx.	.
</s>
<s>
Psal	psát	k5eAaImAgMnS	psát
především	především	k6eAd1	především
traktáty	traktát	k1gInPc4	traktát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
rozsáhlejší	rozsáhlý	k2eAgNnPc4d2	rozsáhlejší
díla	dílo	k1gNnPc4	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
církev	církev	k1gFnSc4	církev
psal	psát	k5eAaImAgInS	psát
latinsky	latinsky	k6eAd1	latinsky
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
prostý	prostý	k2eAgInSc4d1	prostý
lid	lid	k1gInSc4	lid
česky	česky	k6eAd1	česky
<g/>
.	.	kIx.	.
</s>
<s>
Husova	Husův	k2eAgNnPc1d1	Husovo
díla	dílo	k1gNnPc1	dílo
jsou	být	k5eAaImIp3nP	být
charakteristická	charakteristický	k2eAgNnPc4d1	charakteristické
přístupným	přístupný	k2eAgInSc7d1	přístupný
jazykem	jazyk	k1gInSc7	jazyk
<g/>
,	,	kIx,	,
jasnou	jasný	k2eAgFnSc7d1	jasná
kompozicí	kompozice	k1gFnSc7	kompozice
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
církvi	církev	k1gFnSc6	církev
(	(	kIx(	(
<g/>
De	De	k?	De
ecclesia	ecclesia	k1gFnSc1	ecclesia
<g/>
)	)	kIx)	)
Latinsky	latinsky	k6eAd1	latinsky
<g/>
.	.	kIx.	.
</s>
<s>
Hus	Hus	k1gMnSc1	Hus
pojímá	pojímat	k5eAaImIp3nS	pojímat
církev	církev	k1gFnSc4	církev
jako	jako	k8xC	jako
společenství	společenství	k1gNnSc4	společenství
předurčených	předurčený	k2eAgFnPc2d1	předurčená
ke	k	k7c3	k
spáse	spása	k1gFnSc3	spása
<g/>
,	,	kIx,	,
předzvědění	předzvěděný	k2eAgMnPc1d1	předzvěděný
k	k	k7c3	k
zavržení	zavržení	k1gNnSc3	zavržení
k	k	k7c3	k
církvi	církev	k1gFnSc3	církev
patří	patřit	k5eAaImIp3nS	patřit
jen	jen	k9	jen
vnějškově	vnějškově	k6eAd1	vnějškově
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
příslušníka	příslušník	k1gMnSc2	příslušník
církve	církev	k1gFnSc2	církev
považuje	považovat	k5eAaImIp3nS	považovat
toho	ten	k3xDgMnSc4	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
žije	žít	k5eAaImIp3nS	žít
podle	podle	k7c2	podle
Kristových	Kristův	k2eAgNnPc2d1	Kristovo
přikázání	přikázání	k1gNnPc2	přikázání
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
dokazuje	dokazovat	k5eAaImIp3nS	dokazovat
opravdovou	opravdový	k2eAgFnSc4d1	opravdová
lásku	láska	k1gFnSc4	láska
k	k	k7c3	k
Bohu	bůh	k1gMnSc3	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Kdo	kdo	k3yRnSc1	kdo
svými	svůj	k3xOyFgInPc7	svůj
skutky	skutek	k1gInPc7	skutek
jedná	jednat	k5eAaImIp3nS	jednat
proti	proti	k7c3	proti
Písmu	písmo	k1gNnSc3	písmo
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
pravým	pravý	k2eAgMnSc7d1	pravý
křesťanem	křesťan	k1gMnSc7	křesťan
<g/>
,	,	kIx,	,
a	a	k8xC	a
Boha	bůh	k1gMnSc4	bůh
nemiluje	milovat	k5eNaImIp3nS	milovat
<g/>
.	.	kIx.	.
</s>
<s>
Hlavou	hlava	k1gFnSc7	hlava
církve	církev	k1gFnSc2	církev
není	být	k5eNaImIp3nS	být
papež	papež	k1gMnSc1	papež
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Kristus	Kristus	k1gMnSc1	Kristus
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ji	on	k3xPp3gFnSc4	on
svrchovaně	svrchovaně	k6eAd1	svrchovaně
řídí	řídit	k5eAaImIp3nS	řídit
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
papež	papež	k1gMnSc1	papež
jedná	jednat	k5eAaImIp3nS	jednat
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
Božím	boží	k2eAgNnSc7d1	boží
slovem	slovo	k1gNnSc7	slovo
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
nemá	mít	k5eNaImIp3nS	mít
oprávnění	oprávnění	k1gNnSc4	oprávnění
nazývat	nazývat	k5eAaImF	nazývat
se	s	k7c7	s
zástupcem	zástupce	k1gMnSc7	zástupce
Kristovým	Kristův	k2eAgMnSc7d1	Kristův
<g/>
.	.	kIx.	.
</s>
<s>
Věřící	věřící	k1gMnPc1	věřící
pak	pak	k6eAd1	pak
nejsou	být	k5eNaImIp3nP	být
povinni	povinen	k2eAgMnPc1d1	povinen
jej	on	k3xPp3gNnSc4	on
uznávat	uznávat	k5eAaImF	uznávat
<g/>
.	.	kIx.	.
</s>
<s>
Křesťan	Křesťan	k1gMnSc1	Křesťan
nemá	mít	k5eNaImIp3nS	mít
poslouchat	poslouchat	k5eAaImF	poslouchat
příkazy	příkaz	k1gInPc4	příkaz
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
biblí	bible	k1gFnSc7	bible
<g/>
,	,	kIx,	,
každý	každý	k3xTgMnSc1	každý
křesťan	křesťan	k1gMnSc1	křesťan
má	mít	k5eAaImIp3nS	mít
právo	právo	k1gNnSc4	právo
kontroly	kontrola	k1gFnSc2	kontrola
náboženského	náboženský	k2eAgNnSc2d1	náboženské
učení	učení	k1gNnSc2	učení
<g/>
.	.	kIx.	.
</s>
<s>
Měřítkem	měřítko	k1gNnSc7	měřítko
pravosti	pravost	k1gFnSc2	pravost
učení	učení	k1gNnSc2	učení
je	být	k5eAaImIp3nS	být
Bible	bible	k1gFnSc1	bible
<g/>
.	.	kIx.	.
</s>
<s>
Opírá	opírat	k5eAaImIp3nS	opírat
se	se	k3xPyFc4	se
o	o	k7c4	o
Viklefovo	Viklefův	k2eAgNnSc4d1	Viklefovo
učení	učení	k1gNnSc4	učení
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
jej	on	k3xPp3gMnSc4	on
opakuje	opakovat	k5eAaImIp3nS	opakovat
i	i	k9	i
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
česky	česky	k6eAd1	česky
psaných	psaný	k2eAgInPc6d1	psaný
spisech	spis	k1gInPc6	spis
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
bule	bula	k1gFnSc3	bula
papežské	papežský	k2eAgFnSc2d1	Papežská
Latinsky	latinsky	k6eAd1	latinsky
<g/>
.	.	kIx.	.
</s>
<s>
Vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
proti	proti	k7c3	proti
prodávání	prodávání	k1gNnSc3	prodávání
odpustků	odpustek	k1gInPc2	odpustek
<g/>
,	,	kIx,	,
přímo	přímo	k6eAd1	přímo
napadá	napadat	k5eAaBmIp3nS	napadat
papeže	papež	k1gMnSc4	papež
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
spis	spis	k1gInSc1	spis
vedl	vést	k5eAaImAgInS	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
Hus	Hus	k1gMnSc1	Hus
vyhoštěn	vyhostit	k5eAaPmNgMnS	vyhostit
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
působit	působit	k5eAaImF	působit
na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
pak	pak	k6eAd1	pak
přizpůsobil	přizpůsobit	k5eAaPmAgInS	přizpůsobit
jazyk	jazyk	k1gInSc1	jazyk
prostému	prostý	k2eAgInSc3d1	prostý
lidu	lid	k1gInSc3	lid
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
šesti	šest	k4xCc6	šest
bludech	blud	k1gInPc6	blud
(	(	kIx(	(
<g/>
De	De	k?	De
sex	sex	k1gInSc1	sex
erroribus	erroribus	k1gInSc1	erroribus
<g/>
)	)	kIx)	)
Zamýšlí	zamýšlet	k5eAaImIp3nS	zamýšlet
se	se	k3xPyFc4	se
nad	nad	k7c7	nad
vztahem	vztah	k1gInSc7	vztah
mezi	mezi	k7c7	mezi
církví	církev	k1gFnSc7	církev
a	a	k8xC	a
světským	světský	k2eAgInSc7d1	světský
světem	svět	k1gInSc7	svět
<g/>
.	.	kIx.	.
</s>
<s>
Dcerka	dcerka	k1gFnSc1	dcerka
Navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
Štítného	štítný	k2eAgMnSc4d1	štítný
<g/>
,	,	kIx,	,
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
dívkám	dívka	k1gFnPc3	dívka
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
žít	žít	k5eAaImF	žít
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
Božím	boží	k2eAgNnSc7d1	boží
slovem	slovo	k1gNnSc7	slovo
(	(	kIx(	(
<g/>
jde	jít	k5eAaImIp3nS	jít
tedy	tedy	k9	tedy
o	o	k7c4	o
náboženskou	náboženský	k2eAgFnSc4d1	náboženská
výchovu	výchova	k1gFnSc4	výchova
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výklad	výklad	k1gInSc1	výklad
Viery	Viera	k1gFnSc2	Viera
<g/>
,	,	kIx,	,
Desatera	desatero	k1gNnSc2	desatero
a	a	k8xC	a
Páteře	páteř	k1gFnSc2	páteř
Česky	česky	k6eAd1	česky
<g/>
.	.	kIx.	.
</s>
<s>
Výklad	výklad	k1gInSc1	výklad
víry	víra	k1gFnSc2	víra
<g/>
,	,	kIx,	,
desatera	desatero	k1gNnSc2	desatero
Božích	boží	k2eAgNnPc2d1	boží
přikázání	přikázání	k1gNnPc2	přikázání
a	a	k8xC	a
modlitby	modlitba	k1gFnSc2	modlitba
Otčenáše	otčenáš	k1gInSc2	otčenáš
(	(	kIx(	(
<g/>
Páteř	páteř	k1gFnSc1	páteř
je	být	k5eAaImIp3nS	být
odvozené	odvozený	k2eAgMnPc4d1	odvozený
od	od	k7c2	od
Pater	patro	k1gNnPc2	patro
noster	nostra	k1gFnPc2	nostra
<g/>
,	,	kIx,	,
latinského	latinský	k2eAgNnSc2d1	latinské
označení	označení	k1gNnSc2	označení
modlitby	modlitba	k1gFnSc2	modlitba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hus	Hus	k1gMnSc1	Hus
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
"	"	kIx"	"
<g/>
věřit	věřit	k5eAaImF	věřit
v	v	k7c4	v
Boha	bůh	k1gMnSc4	bůh
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
věřit	věřit	k5eAaImF	věřit
Bohu	bůh	k1gMnSc3	bůh
<g/>
"	"	kIx"	"
-	-	kIx~	-
věřit	věřit	k5eAaImF	věřit
v	v	k7c6	v
existenci	existence	k1gFnSc6	existence
Boha	bůh	k1gMnSc2	bůh
ještě	ještě	k6eAd1	ještě
neznamená	znamenat	k5eNaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
člověk	člověk	k1gMnSc1	člověk
je	být	k5eAaImIp3nS	být
křesťanem	křesťan	k1gMnSc7	křesťan
<g/>
.	.	kIx.	.
</s>
<s>
Být	být	k5eAaImF	být
pravým	pravý	k2eAgMnSc7d1	pravý
křesťanem	křesťan	k1gMnSc7	křesťan
znamená	znamenat	k5eAaImIp3nS	znamenat
věřit	věřit	k5eAaImF	věřit
Bohu	bůh	k1gMnSc3	bůh
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
Božímu	boží	k2eAgNnSc3d1	boží
slovu	slovo	k1gNnSc3	slovo
<g/>
,	,	kIx,	,
a	a	k8xC	a
jednat	jednat	k5eAaImF	jednat
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Věřit	věřit	k5eAaImF	věřit
Bohu	bůh	k1gMnSc3	bůh
znamená	znamenat	k5eAaImIp3nS	znamenat
také	také	k6eAd1	také
důvěřovat	důvěřovat	k5eAaImF	důvěřovat
mu	on	k3xPp3gMnSc3	on
a	a	k8xC	a
milovat	milovat	k5eAaImF	milovat
jej	on	k3xPp3gMnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Knížky	knížka	k1gFnPc1	knížka
o	o	k7c6	o
svatokupectví	svatokupectví	k1gNnSc6	svatokupectví
Kritika	kritika	k1gFnSc1	kritika
soudobé	soudobý	k2eAgFnSc2d1	soudobá
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
napsáno	napsat	k5eAaPmNgNnS	napsat
po	po	k7c6	po
zákazu	zákaz	k1gInSc6	zákaz
působení	působení	k1gNnSc2	působení
v	v	k7c6	v
Betlémské	betlémský	k2eAgFnSc6d1	Betlémská
kapli	kaple	k1gFnSc6	kaple
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
svatokupectví	svatokupectví	k1gNnSc4	svatokupectví
považoval	považovat	k5eAaImAgMnS	považovat
Hus	Hus	k1gMnSc1	Hus
vymáhání	vymáhání	k1gNnSc4	vymáhání
poplatků	poplatek	k1gInPc2	poplatek
za	za	k7c4	za
církevní	církevní	k2eAgInPc4d1	církevní
obřady	obřad	k1gInPc4	obřad
(	(	kIx(	(
<g/>
křty	křest	k1gInPc4	křest
<g/>
,	,	kIx,	,
pohřby	pohřeb	k1gInPc4	pohřeb
<g/>
,	,	kIx,	,
mše	mše	k1gFnPc4	mše
<g/>
,	,	kIx,	,
modlitby	modlitba	k1gFnPc4	modlitba
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
chudobu	chudoba	k1gFnSc4	chudoba
věřících	věřící	k1gMnPc2	věřící
<g/>
.	.	kIx.	.
</s>
<s>
Zasazuje	zasazovat	k5eAaImIp3nS	zasazovat
se	se	k3xPyFc4	se
o	o	k7c4	o
odebrání	odebrání	k1gNnSc4	odebrání
majetku	majetek	k1gInSc2	majetek
církevním	církevní	k2eAgMnPc3d1	církevní
hodnostářům	hodnostář	k1gMnPc3	hodnostář
<g/>
,	,	kIx,	,
neříká	říkat	k5eNaImIp3nS	říkat
však	však	k9	však
<g/>
,	,	kIx,	,
komu	kdo	k3yQnSc3	kdo
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
patřit	patřit	k5eAaImF	patřit
<g/>
.	.	kIx.	.
</s>
<s>
Postila	postila	k1gFnSc1	postila
(	(	kIx(	(
<g/>
z	z	k7c2	z
latinského	latinský	k2eAgInSc2d1	latinský
post	post	k1gInSc4	post
illa	illa	k1gFnSc1	illa
verba	verbum	k1gNnSc2	verbum
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
po	po	k7c6	po
oněch	onen	k3xDgFnPc6	onen
slovech	slovo	k1gNnPc6	slovo
-	-	kIx~	-
po	po	k7c6	po
slovech	slovo	k1gNnPc6	slovo
evangelia	evangelium	k1gNnSc2	evangelium
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Soubor	soubor	k1gInSc1	soubor
kázání	kázání	k1gNnSc2	kázání
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
nuceném	nucený	k2eAgInSc6d1	nucený
odchodu	odchod	k1gInSc6	odchod
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Dopisy	dopis	k1gInPc4	dopis
Husova	Husův	k2eAgFnSc1d1	Husova
latinská	latinský	k2eAgFnSc1d1	Latinská
i	i	k8xC	i
česká	český	k2eAgFnSc1d1	Česká
korespondence	korespondence	k1gFnSc1	korespondence
(	(	kIx(	(
<g/>
Sto	sto	k4xCgNnSc4	sto
listů	list	k1gInPc2	list
M.	M.	kA	M.
Jana	Jan	k1gMnSc2	Jan
Husi	Hus	k1gFnSc2	Hus
<g/>
;	;	kIx,	;
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Laichter	Laichter	k1gMnSc1	Laichter
<g/>
,	,	kIx,	,
1949	[number]	k4	1949
<g/>
;	;	kIx,	;
viz	vidět	k5eAaImRp2nS	vidět
též	též	k9	též
Listy	lista	k1gFnPc1	lista
Husovy	Husův	k2eAgFnPc1d1	Husova
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
A.	A.	kA	A.
Hajn	Hajn	k1gMnSc1	Hajn
<g/>
,	,	kIx,	,
1911	[number]	k4	1911
Dostupné	dostupný	k2eAgNnSc4d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
Listy	list	k1gInPc1	list
Jana	Jan	k1gMnSc2	Jan
Husa	Hus	k1gMnSc2	Hus
<g/>
,	,	kIx,	,
přeloženo	přeložen	k2eAgNnSc1d1	přeloženo
a	a	k8xC	a
vydáno	vydán	k2eAgNnSc1d1	vydáno
Václavem	Václav	k1gMnSc7	Václav
Flajšhansem	Flajšhans	k1gMnSc7	Flajšhans
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
českém	český	k2eAgInSc6d1	český
pravopise	pravopis	k1gInSc6	pravopis
(	(	kIx(	(
<g/>
De	De	k?	De
orthographia	orthographia	k1gFnSc1	orthographia
Bohemica	Bohemica	k1gFnSc1	Bohemica
<g/>
)	)	kIx)	)
Anonymní	anonymní	k2eAgNnSc1d1	anonymní
a	a	k8xC	a
nedatované	datovaný	k2eNgNnSc1d1	nedatované
dílo	dílo	k1gNnSc1	dílo
psané	psaný	k2eAgNnSc1d1	psané
latinsky	latinsky	k6eAd1	latinsky
<g/>
,	,	kIx,	,
Františkem	František	k1gMnSc7	František
Palackým	Palacký	k1gMnSc7	Palacký
připsané	připsaný	k2eAgFnPc1d1	připsaná
Husovi	Hus	k1gMnSc3	Hus
<g/>
.	.	kIx.	.
</s>
<s>
Odstraňuje	odstraňovat	k5eAaImIp3nS	odstraňovat
spřežky	spřežka	k1gFnPc4	spřežka
a	a	k8xC	a
zavádí	zavádět	k5eAaImIp3nS	zavádět
diakritická	diakritický	k2eAgNnPc4d1	diakritické
znaménka	znaménko	k1gNnPc4	znaménko
jako	jako	k8xC	jako
první	první	k4xOgMnPc1	první
pro	pro	k7c4	pro
slovanský	slovanský	k2eAgInSc4d1	slovanský
jazyk	jazyk	k1gInSc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
záměrem	záměr	k1gInSc7	záměr
je	být	k5eAaImIp3nS	být
zjednodušit	zjednodušit	k5eAaPmF	zjednodušit
a	a	k8xC	a
sjednotit	sjednotit	k5eAaPmF	sjednotit
český	český	k2eAgInSc4d1	český
pravopis	pravopis	k1gInSc4	pravopis
<g/>
.	.	kIx.	.
</s>
<s>
Měkkost	měkkost	k1gFnSc1	měkkost
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
naznačovat	naznačovat	k5eAaImF	naznačovat
tečkou	tečka	k1gFnSc7	tečka
(	(	kIx(	(
<g/>
punctus	punctus	k1gMnSc1	punctus
rotundus	rotundus	k1gMnSc1	rotundus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
délku	délka	k1gFnSc4	délka
měla	mít	k5eAaImAgFnS	mít
určovat	určovat	k5eAaImF	určovat
čárka	čárka	k1gFnSc1	čárka
(	(	kIx(	(
<g/>
gracilis	gracilis	k1gFnSc1	gracilis
virgula	virgula	k1gFnSc1	virgula
<g/>
)	)	kIx)	)
namísto	namísto	k7c2	namísto
zdvojených	zdvojený	k2eAgFnPc2d1	zdvojená
hlásek	hláska	k1gFnPc2	hláska
<g/>
.	.	kIx.	.
</s>
<s>
Nepřesně	přesně	k6eNd1	přesně
bývají	bývat	k5eAaImIp3nP	bývat
tato	tento	k3xDgNnPc1	tento
diakritická	diakritický	k2eAgNnPc1d1	diakritické
znaménka	znaménko	k1gNnPc1	znaménko
označována	označován	k2eAgNnPc1d1	označováno
za	za	k7c4	za
nabodeníčko	nabodeníčko	k1gNnSc4	nabodeníčko
krátké	krátký	k2eAgNnSc4d1	krátké
a	a	k8xC	a
nabodeníčko	nabodeníčko	k1gNnSc4	nabodeníčko
dlouhé	dlouhý	k2eAgNnSc4d1	dlouhé
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
samotném	samotný	k2eAgInSc6d1	samotný
spisu	spis	k1gInSc6	spis
se	se	k3xPyFc4	se
tyto	tento	k3xDgInPc1	tento
pojmy	pojem	k1gInPc1	pojem
neobjevují	objevovat	k5eNaImIp3nP	objevovat
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
Husova	Husův	k2eAgNnPc1d1	Husovo
diakritická	diakritický	k2eAgNnPc1d1	diakritické
znaménka	znaménko	k1gNnPc1	znaménko
jsou	být	k5eAaImIp3nP	být
zcela	zcela	k6eAd1	zcela
nepochybně	pochybně	k6eNd1	pochybně
označována	označovat	k5eAaImNgFnS	označovat
omylem	omyl	k1gInSc7	omyl
<g/>
.	.	kIx.	.
</s>
<s>
Skutečná	skutečný	k2eAgFnSc1d1	skutečná
podoba	podoba	k1gFnSc1	podoba
Jana	Jan	k1gMnSc2	Jan
Husa	Hus	k1gMnSc2	Hus
není	být	k5eNaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
<g/>
,	,	kIx,	,
k	k	k7c3	k
jeho	jeho	k3xOp3gNnPc3	jeho
nejslavnějším	slavný	k2eAgNnPc3d3	nejslavnější
vyobrazením	vyobrazení	k1gNnPc3	vyobrazení
patří	patřit	k5eAaImIp3nS	patřit
středověké	středověký	k2eAgFnSc2d1	středověká
iluminace	iluminace	k1gFnSc2	iluminace
v	v	k7c6	v
Martinické	Martinický	k2eAgFnSc6d1	Martinická
bibli	bible	k1gFnSc6	bible
a	a	k8xC	a
Jenském	jenský	k2eAgInSc6d1	jenský
kodexu	kodex	k1gInSc6	kodex
<g/>
,	,	kIx,	,
z	z	k7c2	z
moderních	moderní	k2eAgInPc2d1	moderní
Brožíkův	Brožíkův	k2eAgInSc4d1	Brožíkův
velkoformátový	velkoformátový	k2eAgInSc4d1	velkoformátový
obraz	obraz	k1gInSc4	obraz
a	a	k8xC	a
Šalounův	Šalounův	k2eAgInSc4d1	Šalounův
pomník	pomník	k1gInSc4	pomník
na	na	k7c6	na
Staroměstském	staroměstský	k2eAgNnSc6d1	Staroměstské
náměstí	náměstí	k1gNnSc6	náměstí
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Martinická	Martinický	k2eAgFnSc1d1	Martinická
bible	bible	k1gFnSc1	bible
-	-	kIx~	-
nejstarší	starý	k2eAgNnSc1d3	nejstarší
vyobrazení	vyobrazení	k1gNnSc1	vyobrazení
v	v	k7c6	v
iniciále	iniciála	k1gFnSc6	iniciála
měkké	měkký	k2eAgFnPc4d1	měkká
I	i	k9	i
z	z	k7c2	z
1	[number]	k4	1
<g/>
.	.	kIx.	.
poloviny	polovina	k1gFnSc2	polovina
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Jenský	jenský	k2eAgInSc1d1	jenský
kodex	kodex	k1gInSc1	kodex
-	-	kIx~	-
dvojí	dvojit	k5eAaImIp3nS	dvojit
ikonograficky	ikonograficky	k6eAd1	ikonograficky
významné	významný	k2eAgNnSc1d1	významné
vyobrazení	vyobrazení	k1gNnSc1	vyobrazení
Jana	Jan	k1gMnSc2	Jan
Husa	Hus	k1gMnSc2	Hus
na	na	k7c6	na
kazatelně	kazatelna	k1gFnSc6	kazatelna
a	a	k8xC	a
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
<g/>
,	,	kIx,	,
20	[number]	k4	20
<g/>
.	.	kIx.	.
léta	léto	k1gNnSc2	léto
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
Knihovna	knihovna	k1gFnSc1	knihovna
Národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
Oltářní	oltářní	k2eAgNnSc1d1	oltářní
křídlo	křídlo	k1gNnSc1	křídlo
z	z	k7c2	z
Roudník	Roudník	k1gMnSc1	Roudník
s	s	k7c7	s
Janem	Jan	k1gMnSc7	Jan
Husem	Hus	k1gMnSc7	Hus
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
a	a	k8xC	a
se	se	k3xPyFc4	se
sv.	sv.	kA	sv.
Šebestiánem	Šebestián	k1gMnSc7	Šebestián
<g/>
,	,	kIx,	,
severní	severní	k2eAgFnSc1d1	severní
<g />
.	.	kIx.	.
</s>
<s>
Čechy	Čechy	k1gFnPc1	Čechy
<g/>
,	,	kIx,	,
tempera	tempera	k1gFnSc1	tempera
na	na	k7c6	na
dřevěné	dřevěný	k2eAgFnSc6d1	dřevěná
desce	deska	k1gFnSc6	deska
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
1486	[number]	k4	1486
<g/>
;	;	kIx,	;
biskupství	biskupství	k1gNnSc4	biskupství
litoměřické	litoměřický	k2eAgNnSc4d1	Litoměřické
Predella	predella	k1gFnSc1	predella
oltáře	oltář	k1gInSc2	oltář
Zmrtvýchvstání	zmrtvýchvstání	k1gNnSc2	zmrtvýchvstání
Krista	Kristus	k1gMnSc2	Kristus
z	z	k7c2	z
Chrudimi	Chrudim	k1gFnSc2	Chrudim
-	-	kIx~	-
tempera	tempera	k1gFnSc1	tempera
na	na	k7c6	na
dřevěné	dřevěný	k2eAgFnSc6d1	dřevěná
desce	deska	k1gFnSc6	deska
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
1510	[number]	k4	1510
Deska	deska	k1gFnSc1	deska
oltáře	oltář	k1gInSc2	oltář
z	z	k7c2	z
kostela	kostel	k1gInSc2	kostel
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Křtitele	křtitel	k1gMnSc2	křtitel
ve	v	k7c6	v
Vliněvsi	Vliněvse	k1gFnSc6	Vliněvse
-	-	kIx~	-
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
jako	jako	k8xS	jako
ministrant	ministrant	k1gMnSc1	ministrant
sv.	sv.	kA	sv.
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
<g/>
;	;	kIx,	;
tempera	tempera	k1gFnSc1	tempera
na	na	k7c4	na
<g />
.	.	kIx.	.
</s>
<s>
dřevěné	dřevěný	k2eAgFnSc3d1	dřevěná
desce	deska	k1gFnSc3	deska
<g/>
,	,	kIx,	,
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1510	[number]	k4	1510
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc2	Václav
v	v	k7c6	v
Písku	Písek	k1gInSc6	Písek
-	-	kIx~	-
velká	velký	k2eAgFnSc1d1	velká
nástěnná	nástěnný	k2eAgFnSc1d1	nástěnná
malba	malba	k1gFnSc1	malba
s	s	k7c7	s
výjevem	výjev	k1gInSc7	výjev
Husova	Husův	k2eAgNnSc2d1	Husovo
upálení	upálení	k1gNnSc2	upálení
z	z	k7c2	z
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Radnice	radnice	k1gFnSc2	radnice
Tábor	Tábor	k1gInSc1	Tábor
-	-	kIx~	-
soška	soška	k1gFnSc1	soška
Jana	Jan	k1gMnSc2	Jan
Husa	Hus	k1gMnSc2	Hus
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
na	na	k7c6	na
okenním	okenní	k2eAgInSc6d1	okenní
rámu	rám	k1gInSc6	rám
s	s	k7c7	s
městským	městský	k2eAgInSc7d1	městský
znakem	znak	k1gInSc7	znak
<g/>
,	,	kIx,	,
opuka	opuka	k1gFnSc1	opuka
se	s	k7c7	s
zbytky	zbytek	k1gInPc7	zbytek
polychromie	polychromie	k1gFnSc2	polychromie
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Wendel	Wendlo	k1gNnPc2	Wendlo
Roskopf	Roskopf	k1gInSc1	Roskopf
ze	z	k7c2	z
Zhořelce	Zhořelec	k1gInSc2	Zhořelec
<g/>
,	,	kIx,	,
1517	[number]	k4	1517
<g/>
;	;	kIx,	;
protějšek	protějšek	k1gInSc1	protějšek
sošky	soška	k1gFnSc2	soška
Jeronýma	Jeroným	k1gMnSc2	Jeroným
Pražského	pražský	k2eAgMnSc2d1	pražský
a	a	k8xC	a
Jana	Jan	k1gMnSc2	Jan
Žižky	Žižka	k1gMnSc2	Žižka
"	"	kIx"	"
<g/>
Husovy	Husův	k2eAgInPc4d1	Husův
tolary	tolar	k1gInPc4	tolar
<g/>
"	"	kIx"	"
-	-	kIx~	-
Medaile	medaile	k1gFnSc1	medaile
s	s	k7c7	s
portrétem	portrét	k1gInSc7	portrét
Jana	Jan	k1gMnSc2	Jan
Husa	Hus	k1gMnSc2	Hus
<g/>
,	,	kIx,	,
zlacené	zlacený	k2eAgNnSc1d1	zlacené
stříbro	stříbro	k1gNnSc1	stříbro
<g/>
,	,	kIx,	,
Hieronymus	Hieronymus	k1gMnSc1	Hieronymus
Magdeburger	Magdeburger	k1gMnSc1	Magdeburger
<g/>
,	,	kIx,	,
mincovna	mincovna	k1gFnSc1	mincovna
Jáchymov	Jáchymov	k1gInSc1	Jáchymov
<g/>
,	,	kIx,	,
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1530	[number]	k4	1530
Okenní	okenní	k2eAgFnSc1d1	okenní
vitráž	vitráž	k1gFnSc1	vitráž
s	s	k7c7	s
Janem	Jan	k1gMnSc7	Jan
<g />
.	.	kIx.	.
</s>
<s>
Husem	Hus	k1gMnSc7	Hus
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
,	,	kIx,	,
sklomalba	sklomalba	k1gFnSc1	sklomalba
s	s	k7c7	s
olověnými	olověný	k2eAgFnPc7d1	olověná
přepážkami	přepážka	k1gFnPc7	přepážka
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
pol.	pol.	k?	pol.
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
;	;	kIx,	;
Národní	národní	k2eAgNnSc1d1	národní
muzeum	muzeum	k1gNnSc1	muzeum
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
Zvon	zvon	k1gInSc4	zvon
Matěje	Matěj	k1gMnSc2	Matěj
Špice	špic	k1gMnSc2	špic
s	s	k7c7	s
vyobrazením	vyobrazení	k1gNnSc7	vyobrazení
Jana	Jan	k1gMnSc2	Jan
Husa	Hus	k1gMnSc2	Hus
<g/>
,	,	kIx,	,
z	z	k7c2	z
děkanského	děkanský	k2eAgInSc2d1	děkanský
kostela	kostel	k1gInSc2	kostel
sv.	sv.	kA	sv.
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
<g/>
,	,	kIx,	,
1544	[number]	k4	1544
<g/>
,	,	kIx,	,
Ústí	ústí	k1gNnSc1	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
Kachle	kachel	k1gInSc2	kachel
s	s	k7c7	s
<g />
.	.	kIx.	.
</s>
<s>
portrétem	portrét	k1gInSc7	portrét
Jana	Jan	k1gMnSc2	Jan
Husa	Hus	k1gMnSc2	Hus
Praha-náměstí	Prahaáměstí	k1gNnSc2	Praha-náměstí
Republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
režná	režný	k2eAgFnSc1d1	režná
pálená	pálený	k2eAgFnSc1d1	pálená
hlína	hlína	k1gFnSc1	hlína
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
třetina	třetina	k1gFnSc1	třetina
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
Pražský	pražský	k2eAgInSc1d1	pražský
hrad	hrad	k1gInSc1	hrad
(	(	kIx(	(
<g/>
pálená	pálený	k2eAgFnSc1d1	pálená
hlína	hlína	k1gFnSc1	hlína
s	s	k7c7	s
polychromní	polychromní	k2eAgFnSc7d1	polychromní
glazurou	glazura	k1gFnSc7	glazura
<g/>
,	,	kIx,	,
polovina	polovina	k1gFnSc1	polovina
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
sbírka	sbírka	k1gFnSc1	sbírka
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
Lanny	Lanna	k1gMnSc2	Lanna
(	(	kIx(	(
<g/>
pálená	pálený	k2eAgFnSc1d1	pálená
hlína	hlína	k1gFnSc1	hlína
s	s	k7c7	s
polychromní	polychromní	k2eAgFnSc7d1	polychromní
glazurou	glazura	k1gFnSc7	glazura
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovina	polovina	k1gFnSc1	polovina
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
snad	snad	k9	snad
z	z	k7c2	z
Norimberka	Norimberk	k1gInSc2	Norimberk
<g/>
)	)	kIx)	)
Počátky	počátek	k1gInPc4	počátek
(	(	kIx(	(
<g/>
režná	režný	k2eAgFnSc1d1	režná
pálená	pálený	k2eAgFnSc1d1	pálená
hlína	hlína	k1gFnSc1	hlína
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
třetina	třetina	k1gFnSc1	třetina
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
Malířství	malířství	k1gNnSc2	malířství
Václav	Václav	k1gMnSc1	Václav
Brožík	Brožík	k1gMnSc1	Brožík
<g/>
:	:	kIx,	:
Mistr	mistr	k1gMnSc1	mistr
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
před	před	k7c7	před
koncilem	koncil	k1gInSc7	koncil
kostnickým	kostnický	k2eAgInSc7d1	kostnický
(	(	kIx(	(
<g/>
1883	[number]	k4	1883
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
-	-	kIx~	-
velkoformátový	velkoformátový	k2eAgInSc1d1	velkoformátový
obraz	obraz	k1gInSc1	obraz
<g/>
,	,	kIx,	,
sál	sál	k1gInSc1	sál
<g/>
,	,	kIx,	,
Staroměstská	staroměstský	k2eAgFnSc1d1	Staroměstská
radnice	radnice	k1gFnSc1	radnice
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
Kamil	Kamil	k1gMnSc1	Kamil
Vladislav	Vladislav	k1gMnSc1	Vladislav
Muttich	Muttich	k1gMnSc1	Muttich
<g/>
:	:	kIx,	:
Mistr	mistr	k1gMnSc1	mistr
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
v	v	k7c6	v
Kostnici	Kostnice	k1gFnSc6	Kostnice
naposledy	naposledy	k6eAd1	naposledy
žádán	žádán	k2eAgMnSc1d1	žádán
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
podpisem	podpis	k1gInSc7	podpis
zřekl	zřeknout	k5eAaPmAgInS	zřeknout
kacířského	kacířský	k2eAgNnSc2d1	kacířské
učení	učení	k1gNnSc2	učení
Kamil	Kamil	k1gMnSc1	Kamil
Vladislav	Vladislav	k1gMnSc1	Vladislav
Muttich	Muttich	k1gMnSc1	Muttich
<g/>
:	:	kIx,	:
Mistr	mistr	k1gMnSc1	mistr
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
Josef	Josef	k1gMnSc1	Josef
Mathauser	Mathauser	k1gMnSc1	Mathauser
<g/>
:	:	kIx,	:
Hus	Hus	k1gMnSc1	Hus
ve	v	k7c6	v
vlhkém	vlhký	k2eAgNnSc6d1	vlhké
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
tmavém	tmavý	k2eAgNnSc6d1	tmavé
vězení	vězení	k1gNnSc6	vězení
Sochařství	sochařství	k1gNnSc4	sochařství
čtyři	čtyři	k4xCgInPc1	čtyři
anonymní	anonymní	k2eAgInPc1d1	anonymní
soutěžní	soutěžní	k2eAgInPc1d1	soutěžní
návrhy	návrh	k1gInPc1	návrh
na	na	k7c4	na
pomník	pomník	k1gInSc4	pomník
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
(	(	kIx(	(
<g/>
mj.	mj.	kA	mj.
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Schnirch	Schnirch	k1gMnSc1	Schnirch
<g/>
)	)	kIx)	)
Ladislav	Ladislav	k1gMnSc1	Ladislav
Šaloun	Šaloun	k1gMnSc1	Šaloun
<g/>
:	:	kIx,	:
Model	model	k1gInSc1	model
pomníku	pomník	k1gInSc2	pomník
s	s	k7c7	s
Mistrem	mistr	k1gMnSc7	mistr
Janem	Jan	k1gMnSc7	Jan
Husem	Hus	k1gMnSc7	Hus
u	u	k7c2	u
kůlu	kůl	k1gInSc2	kůl
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
<g/>
,	,	kIx,	,
určen	určit	k5eAaPmNgMnS	určit
do	do	k7c2	do
Kostnice	Kostnice	k1gFnSc2	Kostnice
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
před	před	k7c7	před
1900	[number]	k4	1900
František	František	k1gMnSc1	František
Bílek	Bílek	k1gMnSc1	Bílek
<g/>
:	:	kIx,	:
Strom	strom	k1gInSc1	strom
jenž	jenž	k3xRgInSc1	jenž
bleskem	blesk	k1gInSc7	blesk
zasažen	zasažen	k2eAgInSc1d1	zasažen
<g/>
,	,	kIx,	,
po	po	k7c4	po
věky	věk	k1gInPc4	věk
hořel	hořet	k5eAaImAgInS	hořet
<g/>
,	,	kIx,	,
dřevo	dřevo	k1gNnSc1	dřevo
<g/>
,	,	kIx,	,
1901	[number]	k4	1901
František	František	k1gMnSc1	František
Bílek	Bílek	k1gMnSc1	Bílek
<g/>
:	:	kIx,	:
Husovo	Husův	k2eAgNnSc1d1	Husovo
Běda	běda	k6eAd1	běda
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
dřevo	dřevo	k1gNnSc1	dřevo
<g/>
,	,	kIx,	,
Národní	národní	k2eAgFnSc1d1	národní
galerie	galerie	k1gFnSc1	galerie
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
1934	[number]	k4	1934
Stanislav	Stanislav	k1gMnSc1	Stanislav
Sucharda	Sucharda	k1gMnSc1	Sucharda
<g/>
:	:	kIx,	:
Nanebevzetí	nanebevzetí	k1gNnSc1	nanebevzetí
Jana	Jan	k1gMnSc2	Jan
Husa	Hus	k1gMnSc2	Hus
<g/>
,	,	kIx,	,
model	model	k1gInSc1	model
<g/>
,	,	kIx,	,
maj	maj	k?	maj
<g/>
.	.	kIx.	.
</s>
<s>
Suchardova	Suchardův	k2eAgFnSc1d1	Suchardova
nadace	nadace	k1gFnSc1	nadace
Ladislav	Ladislav	k1gMnSc1	Ladislav
Kofránek	Kofránek	k1gMnSc1	Kofránek
<g/>
:	:	kIx,	:
Socha	socha	k1gFnSc1	socha
mistra	mistr	k1gMnSc2	mistr
Jana	Jan	k1gMnSc2	Jan
Husa	Hus	k1gMnSc2	Hus
s	s	k7c7	s
kalichem	kalich	k1gInSc7	kalich
Věra	Věra	k1gFnSc1	Věra
Beránková-Ducháčková	Beránková-Ducháčková	k1gFnSc1	Beránková-Ducháčková
<g/>
:	:	kIx,	:
Socha	socha	k1gFnSc1	socha
mistra	mistr	k1gMnSc2	mistr
Jana	Jan	k1gMnSc2	Jan
Husa	Hus	k1gMnSc2	Hus
<g/>
,	,	kIx,	,
Panteon	panteon	k1gInSc1	panteon
Národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
<g/>
,	,	kIx,	,
1937	[number]	k4	1937
V	v	k7c4	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
zbudováno	zbudovat	k5eAaPmNgNnS	zbudovat
množství	množství	k1gNnSc1	množství
pomníků	pomník	k1gInPc2	pomník
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
představují	představovat	k5eAaImIp3nP	představovat
náročně	náročně	k6eAd1	náročně
provedené	provedený	k2eAgFnPc1d1	provedená
sochy	socha	k1gFnPc1	socha
<g/>
,	,	kIx,	,
busty	busta	k1gFnPc1	busta
<g/>
,	,	kIx,	,
reliéfy	reliéf	k1gInPc1	reliéf
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
prosté	prostý	k2eAgFnSc2d1	prostá
desky	deska	k1gFnSc2	deska
s	s	k7c7	s
motivem	motiv	k1gInSc7	motiv
kalicha	kalich	k1gInSc2	kalich
a	a	k8xC	a
jednoduchým	jednoduchý	k2eAgInSc7d1	jednoduchý
nápisem	nápis	k1gInSc7	nápis
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
bylo	být	k5eAaImAgNnS	být
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
vytvořeno	vytvořit	k5eAaPmNgNnS	vytvořit
celkem	celek	k1gInSc7	celek
83	[number]	k4	83
pomníků	pomník	k1gInPc2	pomník
se	s	k7c7	s
sochou	socha	k1gFnSc7	socha
Jana	Jan	k1gMnSc2	Jan
Husa	Hus	k1gMnSc2	Hus
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInSc1d3	nejstarší
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
je	být	k5eAaImIp3nS	být
umístěn	umístit	k5eAaPmNgMnS	umístit
v	v	k7c6	v
Jičíně	Jičín	k1gInSc6	Jičín
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
odhalen	odhalit	k5eAaPmNgInS	odhalit
7	[number]	k4	7
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1872	[number]	k4	1872
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
figurálních	figurální	k2eAgInPc2d1	figurální
pomníků	pomník	k1gInPc2	pomník
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
v	v	k7c6	v
období	období	k1gNnSc6	období
první	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgFnPc1	tři
byly	být	k5eAaImAgFnP	být
zničeny	zničit	k5eAaPmNgFnP	zničit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1938-1939	[number]	k4	1938-1939
(	(	kIx(	(
<g/>
Dobrá	dobrý	k2eAgFnSc1d1	dobrá
Voda	voda	k1gFnSc1	voda
u	u	k7c2	u
Českých	český	k2eAgInPc2d1	český
Budějovic	Budějovice	k1gInPc2	Budějovice
<g/>
,	,	kIx,	,
Dobřany	Dobřany	k1gInPc1	Dobřany
<g/>
,	,	kIx,	,
Most	most	k1gInSc1	most
<g/>
)	)	kIx)	)
a	a	k8xC	a
7	[number]	k4	7
dalších	další	k2eAgInPc2d1	další
(	(	kIx(	(
<g/>
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
,	,	kIx,	,
Kolín	Kolín	k1gInSc1	Kolín
<g/>
,	,	kIx,	,
Louny	Louny	k1gInPc1	Louny
<g/>
,	,	kIx,	,
Pečky	Pečky	k1gFnPc1	Pečky
<g/>
,	,	kIx,	,
Praha-Zbraslav	Praha-Zbraslav	k1gMnSc1	Praha-Zbraslav
<g/>
,	,	kIx,	,
Terezín	Terezín	k1gInSc1	Terezín
<g/>
,	,	kIx,	,
Třemošná	Třemošný	k2eAgFnSc1d1	Třemošná
<g/>
)	)	kIx)	)
muselo	muset	k5eAaImAgNnS	muset
být	být	k5eAaImF	být
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
dočasně	dočasně	k6eAd1	dočasně
odstraněno	odstranit	k5eAaPmNgNnS	odstranit
<g/>
.	.	kIx.	.
</s>
<s>
Pomník	pomník	k1gInSc1	pomník
z	z	k7c2	z
Jihlavy	Jihlava	k1gFnSc2	Jihlava
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
instalován	instalovat	k5eAaBmNgMnS	instalovat
v	v	k7c6	v
Hlinsku	Hlinsko	k1gNnSc6	Hlinsko
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
bylo	být	k5eAaImAgNnS	být
vystavěno	vystavět	k5eAaPmNgNnS	vystavět
na	na	k7c4	na
200	[number]	k4	200
nefigurálních	figurální	k2eNgInPc2d1	figurální
pomníků	pomník	k1gInPc2	pomník
a	a	k8xC	a
pamětních	pamětní	k2eAgInPc2d1	pamětní
kamenů	kámen	k1gInPc2	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Nejpočetnější	početní	k2eAgMnSc1d3	nejpočetnější
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
Jičínsku	Jičínsko	k1gNnSc6	Jičínsko
<g/>
,	,	kIx,	,
Královéhradecku	Královéhradecko	k1gNnSc6	Královéhradecko
<g/>
,	,	kIx,	,
Plzeňsku	Plzeňsko	k1gNnSc6	Plzeňsko
a	a	k8xC	a
Chodsku	Chodsko	k1gNnSc6	Chodsko
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgInPc4d1	mnohý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
jsou	být	k5eAaImIp3nP	být
současně	současně	k6eAd1	současně
věnované	věnovaný	k2eAgFnSc3d1	věnovaná
památce	památka	k1gFnSc3	památka
padlých	padlý	k1gMnPc2	padlý
v	v	k7c4	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
1872	[number]	k4	1872
-	-	kIx~	-
Jičín	Jičín	k1gInSc1	Jičín
-	-	kIx~	-
pomník	pomník	k1gInSc1	pomník
mistra	mistr	k1gMnSc2	mistr
Jana	Jan	k1gMnSc2	Jan
Husa	Hus	k1gMnSc2	Hus
(	(	kIx(	(
<g/>
Antonín	Antonín	k1gMnSc1	Antonín
Sucharda	Sucharda	k1gMnSc1	Sucharda
mladší	mladý	k2eAgMnSc1d2	mladší
<g/>
,	,	kIx,	,
kámen	kámen	k1gInSc1	kámen
<g/>
)	)	kIx)	)
1897	[number]	k4	1897
-	-	kIx~	-
Vojice	Vojice	k1gFnSc2	Vojice
(	(	kIx(	(
<g/>
Josef	Josef	k1gMnSc1	Josef
Kalvoda	Kalvoda	k1gMnSc1	Kalvoda
<g/>
,	,	kIx,	,
pískovec	pískovec	k1gInSc1	pískovec
<g/>
)	)	kIx)	)
1898	[number]	k4	1898
-	-	kIx~	-
Nová	nový	k2eAgFnSc1d1	nová
Paka	Paka	k1gFnSc1	Paka
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Antonín	Antonín	k1gMnSc1	Antonín
Lukeš	Lukeš	k1gMnSc1	Lukeš
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Chotětov	Chotětov	k1gInSc1	Chotětov
(	(	kIx(	(
<g/>
Josef	Josef	k1gMnSc1	Josef
Beran	Beran	k1gMnSc1	Beran
<g/>
)	)	kIx)	)
1900	[number]	k4	1900
-	-	kIx~	-
Lomnice	Lomnice	k1gFnSc1	Lomnice
nad	nad	k7c7	nad
Popelkou	Popelka	k1gFnSc7	Popelka
(	(	kIx(	(
<g/>
Jindřich	Jindřich	k1gMnSc1	Jindřich
Říha	Říha	k1gMnSc1	Říha
<g/>
,	,	kIx,	,
kámen	kámen	k1gInSc1	kámen
<g/>
)	)	kIx)	)
1901	[number]	k4	1901
-	-	kIx~	-
Benešov	Benešov	k1gInSc1	Benešov
u	u	k7c2	u
Semil	Semily	k1gInPc2	Semily
(	(	kIx(	(
<g/>
Alois	Alois	k1gMnSc1	Alois
Kuhn	Kuhn	k1gMnSc1	Kuhn
<g/>
)	)	kIx)	)
1903	[number]	k4	1903
-	-	kIx~	-
Kozojedy	Kozojeda	k1gMnSc2	Kozojeda
(	(	kIx(	(
<g/>
Alois	Alois	k1gMnSc1	Alois
Kuhn	Kuhn	k1gMnSc1	Kuhn
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
;	;	kIx,	;
Roztoky	Roztoky	k1gInPc7	Roztoky
u	u	k7c2	u
Jilemnice	Jilemnice	k1gFnSc2	Jilemnice
(	(	kIx(	(
<g/>
Alois	Alois	k1gMnSc1	Alois
Kuhn	Kuhn	k1gMnSc1	Kuhn
<g/>
)	)	kIx)	)
1904	[number]	k4	1904
-	-	kIx~	-
Nechanice	Nechanice	k1gFnPc4	Nechanice
(	(	kIx(	(
<g/>
František	František	k1gMnSc1	František
Čermák	Čermák	k1gMnSc1	Čermák
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Vamberk	Vamberk	k1gInSc1	Vamberk
(	(	kIx(	(
<g/>
Josef	Josef	k1gMnSc1	Josef
Kalvoda	Kalvoda	k1gMnSc1	Kalvoda
<g/>
)	)	kIx)	)
1905	[number]	k4	1905
-	-	kIx~	-
Braník	Braník	k1gInSc1	Braník
(	(	kIx(	(
<g/>
Jindřich	Jindřich	k1gMnSc1	Jindřich
Říha	Říha	k1gMnSc1	Říha
<g/>
,	,	kIx,	,
kámen	kámen	k1gInSc1	kámen
<g/>
)	)	kIx)	)
1906	[number]	k4	1906
-	-	kIx~	-
Slatina	slatina	k1gFnSc1	slatina
nad	nad	k7c7	nad
Zdobnicí	Zdobnice	k1gFnSc7	Zdobnice
1908	[number]	k4	1908
-	-	kIx~	-
Beroun	Beroun	k1gInSc1	Beroun
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
Velík	Velík	k1gMnSc1	Velík
<g/>
)	)	kIx)	)
1911	[number]	k4	1911
-	-	kIx~	-
Kostelec	Kostelec	k1gInSc1	Kostelec
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
(	(	kIx(	(
<g/>
Karel	Karel	k1gMnSc1	Karel
Fink	Fink	k1gMnSc1	Fink
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Bohušovice	Bohušovice	k1gFnSc1	Bohušovice
nad	nad	k7c7	nad
Ohří	Ohře	k1gFnSc7	Ohře
(	(	kIx(	(
<g/>
Ladislav	Ladislav	k1gMnSc1	Ladislav
Beneš	Beneš	k1gMnSc1	Beneš
<g/>
)	)	kIx)	)
1912	[number]	k4	1912
-	-	kIx~	-
Dobříkov	Dobříkov	k1gInSc1	Dobříkov
(	(	kIx(	(
<g/>
Cyril	Cyril	k1gMnSc1	Cyril
Jurečka	Jurečka	k1gMnSc1	Jurečka
<g/>
)	)	kIx)	)
1914	[number]	k4	1914
-	-	kIx~	-
Hořice	Hořice	k1gFnPc1	Hořice
-	-	kIx~	-
Pomník	pomník	k1gInSc1	pomník
mistra	mistr	k1gMnSc2	mistr
Jana	Jan	k1gMnSc2	Jan
Husa	Hus	k1gMnSc2	Hus
(	(	kIx(	(
<g/>
Ladislav	Ladislav	k1gMnSc1	Ladislav
Šaloun	Šaloun	k1gMnSc1	Šaloun
<g/>
,	,	kIx,	,
kámen	kámen	k1gInSc1	kámen
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Kolín	Kolín	k1gInSc1	Kolín
(	(	kIx(	(
<g/>
František	František	k1gMnSc1	František
Bílek	Bílek	k1gMnSc1	Bílek
<g/>
,	,	kIx,	,
kámen	kámen	k1gInSc1	kámen
<g/>
)	)	kIx)	)
1915	[number]	k4	1915
-	-	kIx~	-
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
Pomník	pomník	k1gInSc1	pomník
mistra	mistr	k1gMnSc2	mistr
Jana	Jan	k1gMnSc2	Jan
Husa	Hus	k1gMnSc2	Hus
na	na	k7c6	na
Staroměstském	staroměstský	k2eAgNnSc6d1	Staroměstské
náměstí	náměstí	k1gNnSc6	náměstí
(	(	kIx(	(
<g/>
Ladislav	Ladislav	k1gMnSc1	Ladislav
Šaloun	Šaloun	k1gMnSc1	Šaloun
<g/>
,	,	kIx,	,
bronz	bronz	k1gInSc4	bronz
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Kyšice	Kyšice	k1gFnSc1	Kyšice
(	(	kIx(	(
<g/>
František	František	k1gMnSc1	František
Fink	Fink	k1gMnSc1	Fink
<g/>
)	)	kIx)	)
1919	[number]	k4	1919
-	-	kIx~	-
Pečky	pečka	k1gFnSc2	pečka
(	(	kIx(	(
<g/>
Stanislav	Stanislav	k1gMnSc1	Stanislav
Sucharda	Sucharda	k1gMnSc1	Sucharda
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Sucharda	Sucharda	k1gMnSc1	Sucharda
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Dráchov	Dráchov	k1gInSc1	Dráchov
(	(	kIx(	(
<g/>
Bohumil	Bohumil	k1gMnSc1	Bohumil
Přeučil	přeučit	k5eAaPmAgMnS	přeučit
<g/>
)	)	kIx)	)
1920	[number]	k4	1920
-	-	kIx~	-
Radnice	radnice	k1gFnSc1	radnice
(	(	kIx(	(
<g/>
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Šíp	Šíp	k1gMnSc1	Šíp
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Železnice	železnice	k1gFnSc1	železnice
(	(	kIx(	(
<g/>
Stanislav	Stanislav	k1gMnSc1	Stanislav
Sucharda	Sucharda	k1gMnSc1	Sucharda
a	a	k8xC	a
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Sucharda	Sucharda	k1gMnSc1	Sucharda
<g/>
)	)	kIx)	)
1921	[number]	k4	1921
-	-	kIx~	-
Horní	horní	k2eAgInPc1d1	horní
Kněžeklady	Kněžeklad	k1gInPc1	Kněžeklad
(	(	kIx(	(
<g/>
Václav	Václav	k1gMnSc1	Václav
Švec	Švec	k1gMnSc1	Švec
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Katovice	Katovice	k1gFnPc1	Katovice
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Matějů	Matějů	k1gMnSc1	Matějů
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Třemošná	Třemošný	k2eAgFnSc1d1	Třemošná
<g/>
;	;	kIx,	;
Bohuňovice	Bohuňovice	k1gFnSc1	Bohuňovice
(	(	kIx(	(
<g/>
Julius	Julius	k1gMnSc1	Julius
Pelikán	Pelikán	k1gMnSc1	Pelikán
<g/>
)	)	kIx)	)
1922	[number]	k4	1922
-	-	kIx~	-
Jinín	Jinín	k1gMnSc1	Jinín
(	(	kIx(	(
<g/>
Josef	Josef	k1gMnSc1	Josef
Matějů	Matějů	k1gMnSc1	Matějů
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Popovice	Popovice	k1gFnPc1	Popovice
(	(	kIx(	(
<g/>
Alois	Alois	k1gMnSc1	Alois
Kuhn	Kuhn	k1gMnSc1	Kuhn
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Soběraz	Soběraz	k1gInSc1	Soběraz
1923	[number]	k4	1923
-	-	kIx~	-
Spálené	spálený	k2eAgNnSc1d1	spálené
Poříčí	Poříčí	k1gNnSc1	Poříčí
(	(	kIx(	(
<g/>
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Šíp	Šíp	k1gMnSc1	Šíp
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Rožďalovice	Rožďalovice	k1gFnSc1	Rožďalovice
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
František	František	k1gMnSc1	František
Bílek	Bílek	k1gMnSc1	Bílek
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Dobrá	dobrý	k2eAgFnSc1d1	dobrá
Voda	voda	k1gFnSc1	voda
(	(	kIx(	(
<g/>
Edwin	Edwin	k2eAgInSc1d1	Edwin
Schopenhauer	Schopenhauer	k1gInSc1	Schopenhauer
<g/>
;	;	kIx,	;
nedochováno	dochován	k2eNgNnSc1d1	nedochováno
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Jindřichův	Jindřichův	k2eAgInSc1d1	Jindřichův
Hradec	Hradec	k1gInSc1	Hradec
(	(	kIx(	(
<g/>
Jan	Jan	k1gMnSc1	Jan
Vítězslav	Vítězslav	k1gMnSc1	Vítězslav
Dušek	Dušek	k1gMnSc1	Dušek
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Hranice	hranice	k1gFnSc1	hranice
(	(	kIx(	(
<g/>
Julius	Julius	k1gMnSc1	Julius
Pelikán	Pelikán	k1gMnSc1	Pelikán
<g/>
)	)	kIx)	)
1924	[number]	k4	1924
-	-	kIx~	-
Kly	kel	k1gInPc1	kel
(	(	kIx(	(
<g/>
Václav	Václav	k1gMnSc1	Václav
Švec	Švec	k1gMnSc1	Švec
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
<g />
.	.	kIx.	.
</s>
<s>
Most	most	k1gInSc1	most
<g/>
;	;	kIx,	;
Hradec	Hradec	k1gInSc1	Hradec
Králové-Malšovice	Králové-Malšovice	k1gFnSc2	Králové-Malšovice
1925	[number]	k4	1925
-	-	kIx~	-
Strakonice	Strakonice	k1gFnPc1	Strakonice
(	(	kIx(	(
<g/>
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Šíp	Šíp	k1gMnSc1	Šíp
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Louny	Louny	k1gInPc1	Louny
(	(	kIx(	(
<g/>
Josef	Josef	k1gMnSc1	Josef
Kvasnička	Kvasnička	k1gMnSc1	Kvasnička
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Sušice	Sušice	k1gFnSc1	Sušice
(	(	kIx(	(
<g/>
Emanuel	Emanuel	k1gMnSc1	Emanuel
Kodet	Kodet	k1gMnSc1	Kodet
<g/>
,	,	kIx,	,
kámen	kámen	k1gInSc1	kámen
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Libáň	Libáň	k1gFnSc1	Libáň
(	(	kIx(	(
<g/>
Ladislav	Ladislav	k1gMnSc1	Ladislav
Šaloun	Šaloun	k1gMnSc1	Šaloun
<g/>
)	)	kIx)	)
1926	[number]	k4	1926
-	-	kIx~	-
Praha-Troja	Praha-Troja	k1gMnSc1	Praha-Troja
(	(	kIx(	(
<g/>
Vilém	Vilém	k1gMnSc1	Vilém
<g />
.	.	kIx.	.
</s>
<s>
Amort	Amort	k1gInSc1	Amort
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Lhotka	Lhotka	k1gFnSc1	Lhotka
<g/>
;	;	kIx,	;
Soběslav	Soběslav	k1gMnSc1	Soběslav
(	(	kIx(	(
<g/>
Rudolf	Rudolf	k1gMnSc1	Rudolf
Kabeš	Kabeš	k1gMnSc1	Kabeš
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Ledeč	Ledeč	k1gFnSc1	Ledeč
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
(	(	kIx(	(
<g/>
Rudolf	Rudolf	k1gMnSc1	Rudolf
Kabeš	Kabeš	k1gMnSc1	Kabeš
<g/>
)	)	kIx)	)
1927	[number]	k4	1927
-	-	kIx~	-
Plzeň-Doubravka	Plzeň-Doubravka	k1gFnSc1	Plzeň-Doubravka
(	(	kIx(	(
<g/>
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Šíp	Šíp	k1gMnSc1	Šíp
<g/>
,	,	kIx,	,
kámen	kámen	k1gInSc1	kámen
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Vejprnice	Vejprnice	k1gFnSc1	Vejprnice
(	(	kIx(	(
<g/>
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Šíp	Šíp	k1gMnSc1	Šíp
<g/>
,	,	kIx,	,
kámen	kámen	k1gInSc1	kámen
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
;	;	kIx,	;
Dobřany	Dobřany	k1gInPc4	Dobřany
(	(	kIx(	(
<g/>
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Šíp	Šíp	k1gMnSc1	Šíp
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Horní	horní	k2eAgInPc1d1	horní
Mokropsy	Mokropsy	k1gInPc1	Mokropsy
1928	[number]	k4	1928
-	-	kIx~	-
Běchovice	Běchovice	k1gFnPc4	Běchovice
(	(	kIx(	(
<g/>
Josef	Josef	k1gMnSc1	Josef
Vít	Vít	k1gMnSc1	Vít
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Mochov	Mochov	k1gInSc1	Mochov
<g/>
;	;	kIx,	;
Nymburk	Nymburk	k1gInSc1	Nymburk
(	(	kIx(	(
<g/>
František	František	k1gMnSc1	František
Přítel	přítel	k1gMnSc1	přítel
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Roudnice	Roudnice	k1gFnSc1	Roudnice
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
;	;	kIx,	;
Tábor	tábor	k1gMnSc1	tábor
(	(	kIx(	(
<g/>
František	František	k1gMnSc1	František
Bílek	Bílek	k1gMnSc1	Bílek
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
<g />
.	.	kIx.	.
</s>
<s>
Libošovice	Libošovice	k1gFnSc1	Libošovice
(	(	kIx(	(
<g/>
Josef	Josef	k1gMnSc1	Josef
Bílek	Bílek	k1gMnSc1	Bílek
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Jihlava	Jihlava	k1gFnSc1	Jihlava
(	(	kIx(	(
<g/>
František	František	k1gMnSc1	František
Vladimír	Vladimír	k1gMnSc1	Vladimír
Foit	Foit	k1gMnSc1	Foit
<g/>
)	)	kIx)	)
1930	[number]	k4	1930
-	-	kIx~	-
Libochovice	Libochovice	k1gFnPc4	Libochovice
(	(	kIx(	(
<g/>
Karel	Karel	k1gMnSc1	Karel
Zentner	Zentner	k1gMnSc1	Zentner
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Chrudim	Chrudim	k1gFnSc1	Chrudim
(	(	kIx(	(
<g/>
Josef	Josef	k1gMnSc1	Josef
Mařatka	Mařatka	k1gFnSc1	Mařatka
<g/>
)	)	kIx)	)
1931	[number]	k4	1931
-	-	kIx~	-
Čakovice	Čakovice	k1gFnSc2	Čakovice
(	(	kIx(	(
<g/>
Jan	Jan	k1gMnSc1	Jan
Straka	Straka	k1gMnSc1	Straka
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Lužec	Lužec	k1gInSc1	Lužec
nad	nad	k7c7	nad
Cidlinou	Cidlina	k1gFnSc7	Cidlina
1932	[number]	k4	1932
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
Dobříš	Dobříš	k1gFnSc1	Dobříš
(	(	kIx(	(
<g/>
Jan	Jan	k1gMnSc1	Jan
Škvára	škvára	k1gFnSc1	škvára
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Žebrák	žebrák	k1gMnSc1	žebrák
(	(	kIx(	(
<g/>
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Sucharda	Sucharda	k1gMnSc1	Sucharda
<g/>
)	)	kIx)	)
1933	[number]	k4	1933
-	-	kIx~	-
Benešov	Benešov	k1gInSc1	Benešov
(	(	kIx(	(
<g/>
Miloš	Miloš	k1gMnSc1	Miloš
Suchánek	Suchánek	k1gMnSc1	Suchánek
<g/>
)	)	kIx)	)
1935	[number]	k4	1935
-	-	kIx~	-
Vinoř	Vinoř	k1gFnSc1	Vinoř
-	-	kIx~	-
Prachovická	Prachovický	k2eAgFnSc1d1	Prachovická
ulice	ulice	k1gFnSc1	ulice
(	(	kIx(	(
<g/>
Josef	Josef	k1gMnSc1	Josef
Bílek	Bílek	k1gMnSc1	Bílek
a	a	k8xC	a
V.	V.	kA	V.
Kvasnička	kvasnička	k1gFnSc1	kvasnička
<g/>
,	,	kIx,	,
pískovec	pískovec	k1gInSc1	pískovec
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Zbraslav	Zbraslav	k1gFnSc1	Zbraslav
(	(	kIx(	(
<g/>
J.	J.	kA	J.
<g />
.	.	kIx.	.
</s>
<s>
F.	F.	kA	F.
Žák	Žák	k1gMnSc1	Žák
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Velký	velký	k2eAgInSc1d1	velký
Osek	Osek	k1gInSc1	Osek
(	(	kIx(	(
<g/>
Josef	Josef	k1gMnSc1	Josef
Bílek	Bílek	k1gMnSc1	Bílek
<g/>
)	)	kIx)	)
1936	[number]	k4	1936
-	-	kIx~	-
Plotiště	Plotiště	k1gNnSc4	Plotiště
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
(	(	kIx(	(
<g/>
Rudolf	Rudolf	k1gMnSc1	Rudolf
Semrád	Semrád	k1gMnSc1	Semrád
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
nad	nad	k7c7	nad
Metují	Metuje	k1gFnSc7	Metuje
(	(	kIx(	(
<g/>
Jan	Jan	k1gMnSc1	Jan
Adolf	Adolf	k1gMnSc1	Adolf
Vítek	Vítek	k1gMnSc1	Vítek
<g/>
)	)	kIx)	)
1937	[number]	k4	1937
-	-	kIx~	-
Chrást	Chrást	k1gMnSc1	Chrást
(	(	kIx(	(
<g/>
Josef	Josef	k1gMnSc1	Josef
Fryček	Fryček	k1gMnSc1	Fryček
<g/>
)	)	kIx)	)
1938	[number]	k4	1938
-	-	kIx~	-
Terezín	Terezín	k1gInSc1	Terezín
<g />
.	.	kIx.	.
</s>
<s>
1939	[number]	k4	1939
-	-	kIx~	-
Slavkov	Slavkov	k1gInSc1	Slavkov
u	u	k7c2	u
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
Horní	horní	k2eAgFnSc1d1	horní
Police	police	k1gFnSc1	police
(	(	kIx(	(
<g/>
Josef	Josef	k1gMnSc1	Josef
Bílek	Bílek	k1gMnSc1	Bílek
<g/>
)	)	kIx)	)
1952	[number]	k4	1952
-	-	kIx~	-
Klenčí	Klenčí	k1gNnSc4	Klenčí
pod	pod	k7c7	pod
Čerchovem	Čerchovo	k1gNnSc7	Čerchovo
(	(	kIx(	(
<g/>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Zahoř	zahořet	k5eAaPmRp2nS	zahořet
<g/>
)	)	kIx)	)
1958	[number]	k4	1958
-	-	kIx~	-
Husinec	Husinec	k1gInSc1	Husinec
(	(	kIx(	(
<g/>
Karel	Karel	k1gMnSc1	Karel
Lidický	lidický	k2eAgMnSc1d1	lidický
<g/>
,	,	kIx,	,
bronz	bronz	k1gInSc1	bronz
<g/>
)	)	kIx)	)
1959	[number]	k4	1959
-	-	kIx~	-
Praha-Staré	Praha-Starý	k2eAgNnSc1d1	Praha-Staré
Město	město	k1gNnSc1	město
-	-	kIx~	-
Karolinum	Karolinum	k1gNnSc1	Karolinum
(	(	kIx(	(
<g/>
Karel	Karel	k1gMnSc1	Karel
Lidický	lidický	k2eAgMnSc1d1	lidický
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
bronz	bronz	k1gInSc1	bronz
<g/>
)	)	kIx)	)
2011	[number]	k4	2011
-	-	kIx~	-
Krakovec	krakovec	k1gInSc1	krakovec
(	(	kIx(	(
<g/>
Milan	Milan	k1gMnSc1	Milan
Vácha	Vácha	k1gMnSc1	Vácha
<g/>
)	)	kIx)	)
2012	[number]	k4	2012
-	-	kIx~	-
Dobrá	dobrý	k2eAgFnSc1d1	dobrá
Voda	voda	k1gFnSc1	voda
-	-	kIx~	-
Socha	socha	k1gFnSc1	socha
Jana	Jan	k1gMnSc2	Jan
Husa	Hus	k1gMnSc2	Hus
(	(	kIx(	(
<g/>
Miroslav	Miroslav	k1gMnSc1	Miroslav
Švihla	švihnout	k5eAaPmAgFnS	švihnout
<g/>
)	)	kIx)	)
2015	[number]	k4	2015
-	-	kIx~	-
Nový	nový	k2eAgInSc1d1	nový
Bor	bor	k1gInSc1	bor
(	(	kIx(	(
<g/>
Michal	Michal	k1gMnSc1	Michal
Drobek	drobek	k1gInSc1	drobek
<g/>
,	,	kIx,	,
pískovec	pískovec	k1gInSc1	pískovec
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
Jana	Jan	k1gMnSc2	Jan
Štursy	Štursa	k1gFnSc2	Štursa
<g />
.	.	kIx.	.
</s>
<s>
<g/>
;	;	kIx,	;
bronz	bronz	k1gInSc4	bronz
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Vrchlabí	Vrchlabí	k1gNnSc1	Vrchlabí
(	(	kIx(	(
<g/>
Matěj	Matěj	k1gMnSc1	Matěj
Skavroň	Skavroň	k1gMnSc1	Skavroň
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Kozí	kozí	k2eAgInSc1d1	kozí
Hrádek	hrádek	k1gInSc1	hrádek
(	(	kIx(	(
<g/>
Jan	Jan	k1gMnSc1	Jan
Ambrož	Ambrož	k1gMnSc1	Ambrož
<g/>
)	)	kIx)	)
1901	[number]	k4	1901
-	-	kIx~	-
Čekanice	Čekanice	k1gFnSc2	Čekanice
(	(	kIx(	(
<g/>
Jan	Jan	k1gMnSc1	Jan
Vejrych	Vejrych	k1gMnSc1	Vejrych
<g/>
;	;	kIx,	;
balvan	balvan	k1gMnSc1	balvan
s	s	k7c7	s
bronzovou	bronzový	k2eAgFnSc7d1	bronzová
plaketou	plaketa	k1gFnSc7	plaketa
<g/>
)	)	kIx)	)
1915	[number]	k4	1915
-	-	kIx~	-
Kostelec	Kostelec	k1gInSc1	Kostelec
nad	nad	k7c7	nad
Černými	černý	k2eAgInPc7d1	černý
Lesy	les	k1gInPc7	les
(	(	kIx(	(
<g/>
Antonín	Antonín	k1gMnSc1	Antonín
Odehnal	Odehnal	k1gMnSc1	Odehnal
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Rakovník	Rakovník	k1gInSc1	Rakovník
(	(	kIx(	(
<g/>
Ladislav	Ladislav	k1gMnSc1	Ladislav
Šaloun	Šaloun	k1gMnSc1	Šaloun
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Horažďovice	Horažďovice	k1gFnPc4	Horažďovice
(	(	kIx(	(
<g/>
Karel	Karel	k1gMnSc1	Karel
Němec	Němec	k1gMnSc1	Němec
<g/>
;	;	kIx,	;
balvan	balvan	k1gMnSc1	balvan
s	s	k7c7	s
bronzovou	bronzový	k2eAgFnSc7d1	bronzová
plaketou	plaketa	k1gFnSc7	plaketa
<g/>
)	)	kIx)	)
1919	[number]	k4	1919
-	-	kIx~	-
Lipnice	Lipnice	k1gFnSc1	Lipnice
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
<g/>
;	;	kIx,	;
Volichov	Volichov	k1gInSc1	Volichov
<g/>
;	;	kIx,	;
Neuměřice	Neuměřice	k1gFnSc1	Neuměřice
(	(	kIx(	(
<g/>
Stanislav	Stanislav	k1gMnSc1	Stanislav
Sucharda	Sucharda	k1gMnSc1	Sucharda
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Třebnouševes	Třebnouševes	k1gMnSc1	Třebnouševes
(	(	kIx(	(
<g/>
Václav	Václav	k1gMnSc1	Václav
Suchomel	Suchomel	k1gMnSc1	Suchomel
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
1920	[number]	k4	1920
-	-	kIx~	-
Mutějovice	Mutějovice	k1gFnSc2	Mutějovice
(	(	kIx(	(
<g/>
zkamenělé	zkamenělý	k2eAgNnSc1d1	zkamenělé
dřevo	dřevo	k1gNnSc1	dřevo
s	s	k7c7	s
bronzovým	bronzový	k2eAgInSc7d1	bronzový
medailonem	medailon	k1gInSc7	medailon
<g/>
)	)	kIx)	)
1921	[number]	k4	1921
-	-	kIx~	-
Český	český	k2eAgInSc1d1	český
Brod	Brod	k1gInSc1	Brod
(	(	kIx(	(
<g/>
centrální	centrální	k2eAgInSc1d1	centrální
balvan	balvan	k1gInSc1	balvan
obklopený	obklopený	k2eAgInSc1d1	obklopený
sedmi	sedm	k4xCc7	sedm
menšími	malý	k2eAgInPc7d2	menší
kameny	kámen	k1gInPc7	kámen
se	s	k7c7	s
jmény	jméno	k1gNnPc7	jméno
dalších	další	k2eAgFnPc2d1	další
osobností	osobnost	k1gFnSc7	osobnost
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Chodovice	Chodovice	k1gFnSc1	Chodovice
(	(	kIx(	(
<g/>
Václav	Václav	k1gMnSc1	Václav
Suchomel	Suchomel	k1gMnSc1	Suchomel
<g/>
)	)	kIx)	)
1922	[number]	k4	1922
-	-	kIx~	-
Plzeň-Křimice	Plzeň-Křimika	k1gFnSc6	Plzeň-Křimika
(	(	kIx(	(
<g/>
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Šíp	Šíp	k1gMnSc1	Šíp
<g/>
;	;	kIx,	;
kamenná	kamenný	k2eAgFnSc1d1	kamenná
<g />
.	.	kIx.	.
</s>
<s>
mohyla	mohyla	k1gFnSc1	mohyla
s	s	k7c7	s
medailonem	medailon	k1gInSc7	medailon
a	a	k8xC	a
kalichem	kalich	k1gInSc7	kalich
<g/>
)	)	kIx)	)
1923	[number]	k4	1923
-	-	kIx~	-
Horní	horní	k2eAgFnSc1d1	horní
Blatná	blatný	k2eAgFnSc1d1	Blatná
(	(	kIx(	(
<g/>
Ladislav	Ladislav	k1gMnSc1	Ladislav
Šaloun	Šaloun	k1gMnSc1	Šaloun
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Praha-Slivenec	Praha-Slivenec	k1gMnSc1	Praha-Slivenec
(	(	kIx(	(
<g/>
Karel	Karel	k1gMnSc1	Karel
Zuklín	Zuklína	k1gFnPc2	Zuklína
<g/>
)	)	kIx)	)
1924	[number]	k4	1924
-	-	kIx~	-
Kozí	kozí	k2eAgInSc1d1	kozí
hrádek	hrádek	k1gInSc1	hrádek
(	(	kIx(	(
<g/>
Jan	Jan	k1gMnSc1	Jan
Vítězslav	Vítězslav	k1gMnSc1	Vítězslav
Dušek	Dušek	k1gMnSc1	Dušek
<g/>
;	;	kIx,	;
Sokolská	sokolský	k2eAgFnSc1d1	Sokolská
mohyla	mohyla	k1gFnSc1	mohyla
s	s	k7c7	s
plaketou	plaketa	k1gFnSc7	plaketa
Jana	Jan	k1gMnSc2	Jan
Husa	Hus	k1gMnSc2	Hus
a	a	k8xC	a
Jana	Jan	k1gMnSc2	Jan
Žižky	Žižka	k1gMnSc2	Žižka
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
<g />
.	.	kIx.	.
</s>
<s>
Černilov	Černilov	k1gInSc1	Černilov
(	(	kIx(	(
<g/>
kamenný	kamenný	k2eAgInSc1d1	kamenný
kvádr	kvádr	k1gInSc1	kvádr
s	s	k7c7	s
reliéfy	reliéf	k1gInPc7	reliéf
<g/>
)	)	kIx)	)
1927	[number]	k4	1927
-	-	kIx~	-
Praha-Košíře	Praha-Košíř	k1gInSc2	Praha-Košíř
(	(	kIx(	(
<g/>
Karel	Karel	k1gMnSc1	Karel
Pokorný	Pokorný	k1gMnSc1	Pokorný
<g/>
)	)	kIx)	)
1931	[number]	k4	1931
-	-	kIx~	-
Křivoklát	Křivoklát	k1gInSc1	Křivoklát
(	(	kIx(	(
<g/>
Antonín	Antonín	k1gMnSc1	Antonín
Odehnal	Odehnal	k1gMnSc1	Odehnal
<g/>
)	)	kIx)	)
1936	[number]	k4	1936
-	-	kIx~	-
Hředle	Hředle	k1gMnSc1	Hředle
(	(	kIx(	(
<g/>
Antonín	Antonín	k1gMnSc1	Antonín
Odehnal	Odehnal	k1gMnSc1	Odehnal
<g/>
)	)	kIx)	)
Poprsí	poprsí	k1gNnSc1	poprsí
Jana	Jan	k1gMnSc2	Jan
Husa	Hus	k1gMnSc2	Hus
<g/>
,	,	kIx,	,
bronz	bronz	k1gInSc4	bronz
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1899	[number]	k4	1899
<g/>
-	-	kIx~	-
<g/>
1900	[number]	k4	1900
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
z	z	k7c2	z
Panteonu	panteon	k1gInSc2	panteon
Národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
1937	[number]	k4	1937
nahrazeno	nahradit	k5eAaPmNgNnS	nahradit
sochou	socha	k1gFnSc7	socha
Busta	busta	k1gFnSc1	busta
Jana	Jan	k1gMnSc2	Jan
Husa	Hus	k1gMnSc2	Hus
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
-	-	kIx~	-
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
-	-	kIx~	-
Sukovy	Sukův	k2eAgFnSc2d1	Sukova
sady	sada	k1gFnSc2	sada
(	(	kIx(	(
<g/>
František	František	k1gMnSc1	František
Fabiánek	Fabiánka	k1gFnPc2	Fabiánka
<g/>
;	;	kIx,	;
1914	[number]	k4	1914
<g/>
;	;	kIx,	;
kámen	kámen	k1gInSc1	kámen
<g/>
)	)	kIx)	)
Pomník	pomník	k1gInSc1	pomník
s	s	k7c7	s
bustou	busta	k1gFnSc7	busta
Jana	Jan	k1gMnSc2	Jan
Husa	Hus	k1gMnSc2	Hus
-	-	kIx~	-
Nouzov	Nouzov	k1gInSc1	Nouzov
(	(	kIx(	(
<g/>
Josef	Josef	k1gMnSc1	Josef
Fojtík	Fojtík	k1gMnSc1	Fojtík
<g/>
;	;	kIx,	;
1915	[number]	k4	1915
<g/>
;	;	kIx,	;
kámen	kámen	k1gInSc4	kámen
<g/>
)	)	kIx)	)
Pamětní	pamětní	k2eAgInSc4d1	pamětní
kámen	kámen	k1gInSc4	kámen
-	-	kIx~	-
Husův	Husův	k2eAgInSc4d1	Husův
pomník	pomník	k1gInSc4	pomník
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
hranice	hranice	k1gFnSc1	hranice
<g/>
,	,	kIx,	,
žula	žula	k1gFnSc1	žula
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kostnice	Kostnice	k1gFnSc1	Kostnice
<g/>
,	,	kIx,	,
1862	[number]	k4	1862
Ernst	Ernst	k1gMnSc1	Ernst
Rietschel	Rietschel	k1gMnSc1	Rietschel
<g/>
/	/	kIx~	/
<g/>
Gustav	Gustav	k1gMnSc1	Gustav
Adolph	Adolph	k1gMnSc1	Adolph
Kietz	Kietz	k1gMnSc1	Kietz
<g/>
:	:	kIx,	:
součást	součást	k1gFnSc1	součást
Lutherova	Lutherův	k2eAgInSc2d1	Lutherův
památníku	památník	k1gInSc2	památník
(	(	kIx(	(
<g/>
Lutherdenkmal	Lutherdenkmal	k1gInSc1	Lutherdenkmal
<g/>
)	)	kIx)	)
ve	v	k7c6	v
Wormsu	Worms	k1gInSc6	Worms
<g/>
,	,	kIx,	,
1868	[number]	k4	1868
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
Memorial	Memorial	k1gMnSc1	Memorial
-	-	kIx~	-
Husův	Husův	k2eAgInSc4d1	Husův
pomník	pomník	k1gInSc4	pomník
v	v	k7c4	v
Union	union	k1gInSc4	union
Cemetery	Cemeter	k1gInPc7	Cemeter
<g/>
,	,	kIx,	,
Bohemia	bohemia	k1gFnSc1	bohemia
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
City	City	k1gFnSc1	City
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
1893	[number]	k4	1893
Socha	socha	k1gFnSc1	socha
Jana	Jan	k1gMnSc2	Jan
Husa	Hus	k1gMnSc2	Hus
u	u	k7c2	u
Frederiks	Frederiksa	k1gFnPc2	Frederiksa
Kirke	Kirk	k1gInSc2	Kirk
v	v	k7c6	v
Kodani	Kodaň	k1gFnSc6	Kodaň
Husův	Husův	k2eAgInSc4d1	Husův
kámen	kámen	k1gInSc4	kámen
v	v	k7c6	v
Zelově	zelově	k6eAd1	zelově
<g/>
,	,	kIx,	,
1915	[number]	k4	1915
Husův	Husův	k2eAgInSc1d1	Husův
pomník	pomník	k1gInSc1	pomník
(	(	kIx(	(
<g/>
pylon	pylon	k1gInSc1	pylon
s	s	k7c7	s
bustou	busta	k1gFnSc7	busta
<g/>
)	)	kIx)	)
v	v	k7c6	v
Brezové	Brezová	k1gFnSc6	Brezová
pod	pod	k7c4	pod
Bradlom	Bradlom	k1gInSc4	Bradlom
<g/>
,	,	kIx,	,
1922	[number]	k4	1922
Taras	taras	k1gInSc1	taras
Ševčenko	Ševčenka	k1gFnSc5	Ševčenka
<g/>
:	:	kIx,	:
Kacíř	kacíř	k1gMnSc1	kacíř
(	(	kIx(	(
<g/>
1845	[number]	k4	1845
<g/>
)	)	kIx)	)
-	-	kIx~	-
poezie	poezie	k1gFnSc1	poezie
Josef	Josef	k1gMnSc1	Josef
Kajetán	Kajetán	k1gMnSc1	Kajetán
Tyl	Tyl	k1gMnSc1	Tyl
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
<g />
.	.	kIx.	.
</s>
<s>
Hus	Hus	k1gMnSc1	Hus
(	(	kIx(	(
<g/>
1848	[number]	k4	1848
<g/>
)	)	kIx)	)
-	-	kIx~	-
divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
Victor	Victor	k1gMnSc1	Victor
Hugo	Hugo	k1gMnSc1	Hugo
<g/>
:	:	kIx,	:
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soucit	soucit	k1gInSc1	soucit
(	(	kIx(	(
<g/>
La	la	k1gNnSc1	la
pitié	pitiá	k1gFnSc2	pitiá
suprê	suprê	k?	suprê
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1879	[number]	k4	1879
<g/>
)	)	kIx)	)
-	-	kIx~	-
báseň	báseň	k1gFnSc1	báseň
Alois	Alois	k1gMnSc1	Alois
Jirásek	Jirásek	k1gMnSc1	Jirásek
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
(	(	kIx(	(
<g/>
1911	[number]	k4	1911
<g/>
)	)	kIx)	)
-	-	kIx~	-
divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
Čečetka	čečetka	k1gFnSc1	čečetka
<g/>
:	:	kIx,	:
Mistr	mistr	k1gMnSc1	mistr
Jan	Jan	k1gMnSc1	Jan
<g />
.	.	kIx.	.
</s>
<s>
Hus	Hus	k1gMnSc1	Hus
(	(	kIx(	(
<g/>
1927	[number]	k4	1927
<g/>
)	)	kIx)	)
-	-	kIx~	-
román	román	k1gInSc1	román
Miloš	Miloš	k1gMnSc1	Miloš
Václav	Václav	k1gMnSc1	Václav
Kratochvíl	Kratochvíl	k1gMnSc1	Kratochvíl
<g/>
:	:	kIx,	:
Mistr	mistr	k1gMnSc1	mistr
Jan	Jan	k1gMnSc1	Jan
(	(	kIx(	(
<g/>
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
-	-	kIx~	-
román	román	k1gInSc1	román
Otakar	Otakar	k1gMnSc1	Otakar
Vávra	Vávra	k1gMnSc1	Vávra
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
(	(	kIx(	(
<g/>
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
-	-	kIx~	-
film	film	k1gInSc1	film
Pavel	Pavel	k1gMnSc1	Pavel
Kohout	Kohout	k1gMnSc1	Kohout
<g/>
:	:	kIx,	:
Ecce	Ecce	k1gFnSc1	Ecce
Constantia	Constantia	k1gFnSc1	Constantia
-	-	kIx~	-
divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
/	/	kIx~	/
scénář	scénář	k1gInSc4	scénář
zfilmovaný	zfilmovaný	k2eAgInSc4d1	zfilmovaný
Čs	čs	kA	čs
<g/>
.	.	kIx.	.
televizí	televize	k1gFnPc2	televize
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
Eva	Eva	k1gFnSc1	Eva
Kantůrková	Kantůrková	k1gFnSc1	Kantůrková
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
-	-	kIx~	-
kniha	kniha	k1gFnSc1	kniha
(	(	kIx(	(
<g/>
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
samizdatu	samizdat	k1gInSc6	samizdat
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nesmiřitelná	smiřitelný	k2eNgFnSc1d1	nesmiřitelná
řeč	řeč	k1gFnSc1	řeč
<g/>
.	.	kIx.	.
</s>
<s>
Rozprava	rozprava	k1gFnSc1	rozprava
s	s	k7c7	s
přidaným	přidaný	k2eAgInSc7d1	přidaný
mýtem	mýtus	k1gInSc7	mýtus
o	o	k7c6	o
Janu	Jan	k1gMnSc3	Jan
Husovi	Hus	k1gMnSc3	Hus
-	-	kIx~	-
kniha	kniha	k1gFnSc1	kniha
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
-	-	kIx~	-
filmová	filmový	k2eAgFnSc1d1	filmová
trilogie	trilogie	k1gFnSc1	trilogie
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
-	-	kIx~	-
Cesta	cesta	k1gFnSc1	cesta
bez	bez	k7c2	bez
návratu	návrat	k1gInSc2	návrat
-	-	kIx~	-
film	film	k1gInSc1	film
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
se	se	k3xPyFc4	se
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
umístil	umístit	k5eAaPmAgMnS	umístit
na	na	k7c6	na
sedmém	sedmý	k4xOgNnSc6	sedmý
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
Největší	veliký	k2eAgMnSc1d3	veliký
Čech	Čech	k1gMnSc1	Čech
<g/>
.	.	kIx.	.
</s>
<s>
Dostal	dostat	k5eAaPmAgMnS	dostat
10	[number]	k4	10
487	[number]	k4	487
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnPc1d1	Česká
dráhy	dráha	k1gFnPc1	dráha
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
provozují	provozovat	k5eAaImIp3nP	provozovat
pár	pár	k4xCyI	pár
vlaků	vlak	k1gInPc2	vlak
(	(	kIx(	(
<g/>
Ex	ex	k6eAd1	ex
351	[number]	k4	351
<g/>
/	/	kIx~	/
<g/>
352	[number]	k4	352
<g/>
)	)	kIx)	)
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
v	v	k7c6	v
trase	trasa	k1gFnSc6	trasa
<g/>
:	:	kIx,	:
Praha	Praha	k1gFnSc1	Praha
<g/>
-	-	kIx~	-
<g/>
Domažlice	Domažlice	k1gFnPc1	Domažlice
<g/>
-	-	kIx~	-
<g/>
Schwandorf	Schwandorf	k1gInSc1	Schwandorf
<g/>
-	-	kIx~	-
<g/>
Regensburg	Regensburg	k1gInSc1	Regensburg
<g/>
-	-	kIx~	-
<g/>
Mnichov	Mnichov	k1gInSc1	Mnichov
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Jana	Jan	k1gMnSc2	Jan
Husa	Hus	k1gMnSc2	Hus
bylo	být	k5eAaImAgNnS	být
umístěno	umístit	k5eAaPmNgNnS	umístit
na	na	k7c6	na
tabulce	tabulka	k1gFnSc6	tabulka
pod	pod	k7c7	pod
okny	okno	k1gNnPc7	okno
Národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
dalšími	další	k1gNnPc7	další
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
Dvaasedmdesát	dvaasedmdesát	k4xCc4	dvaasedmdesát
jmen	jméno	k1gNnPc2	jméno
české	český	k2eAgFnSc2d1	Česká
historie	historie	k1gFnSc2	historie
<g/>
.	.	kIx.	.
</s>
<s>
Benito	Benit	k2eAgNnSc1d1	Benito
Mussolini	Mussolin	k2eAgMnPc1d1	Mussolin
napsal	napsat	k5eAaBmAgMnS	napsat
knížku	knížka	k1gFnSc4	knížka
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
<g/>
,	,	kIx,	,
hlasatel	hlasatel	k1gMnSc1	hlasatel
pravdy	pravda	k1gFnSc2	pravda
(	(	kIx(	(
<g/>
Giovanni	Giovann	k1gMnPc1	Giovann
Hus	Hus	k1gMnSc1	Hus
<g/>
,	,	kIx,	,
il	il	k?	il
veridico	veridico	k1gMnSc1	veridico
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1913	[number]	k4	1913
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
Monkey	Monkea	k1gFnSc2	Monkea
Business	business	k1gInSc1	business
nahrála	nahrát	k5eAaPmAgFnS	nahrát
skladbu	skladba	k1gFnSc4	skladba
We	We	k1gFnSc1	We
Feel	Feel	k1gMnSc1	Feel
Better	Better	k1gMnSc1	Better
Than	Than	k1gMnSc1	Than
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vyšla	vyjít	k5eAaPmAgFnS	vyjít
na	na	k7c6	na
albu	album	k1gNnSc6	album
Resistance	Resistanec	k1gMnSc2	Resistanec
Is	Is	k1gMnSc2	Is
Futile	Futila	k1gFnSc6	Futila
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Richard	Richard	k1gMnSc1	Richard
Pachman	Pachman	k1gMnSc1	Pachman
<g/>
,	,	kIx,	,
skladatel	skladatel	k1gMnSc1	skladatel
duchovní	duchovní	k2eAgFnSc2d1	duchovní
a	a	k8xC	a
muzikálové	muzikálový	k2eAgFnSc2d1	muzikálová
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
napsal	napsat	k5eAaBmAgMnS	napsat
scénické	scénický	k2eAgNnSc4d1	scénické
oratorium	oratorium	k1gNnSc4	oratorium
Mistr	mistr	k1gMnSc1	mistr
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
Husových	Husových	k2eAgNnSc2d1	Husových
kázání	kázání	k1gNnSc2	kázání
a	a	k8xC	a
dopisů	dopis	k1gInPc2	dopis
<g/>
.	.	kIx.	.
</s>
<s>
Představení	představení	k1gNnSc1	představení
mělo	mít	k5eAaImAgNnS	mít
premiéru	premiéra	k1gFnSc4	premiéra
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
postav	postava	k1gFnPc2	postava
internetového	internetový	k2eAgInSc2d1	internetový
komiksu	komiks	k1gInSc2	komiks
Opráski	Oprásk	k1gFnSc2	Oprásk
sčeskí	sčeskí	k2eAgFnSc2d1	sčeskí
historje	historj	k1gFnSc2	historj
<g/>
.	.	kIx.	.
</s>
<s>
Rapper	Rapper	k1gMnSc1	Rapper
Kato	Kato	k1gMnSc1	Kato
nahrál	nahrát	k5eAaBmAgMnS	nahrát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
skladbu	skladba	k1gFnSc4	skladba
Díky	díky	k7c3	díky
<g/>
,	,	kIx,	,
Mistře	mistr	k1gMnSc5	mistr
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
oslavuje	oslavovat	k5eAaImIp3nS	oslavovat
Husův	Husův	k2eAgInSc4d1	Husův
přínos	přínos	k1gInSc4	přínos
k	k	k7c3	k
vývoji	vývoj	k1gInSc3	vývoj
českého	český	k2eAgInSc2d1	český
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2015	[number]	k4	2015
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
slavnostnímu	slavnostní	k2eAgNnSc3d1	slavnostní
otevření	otevření	k1gNnSc3	otevření
Centra	centr	k1gMnSc2	centr
Mistra	mistr	k1gMnSc2	mistr
Jana	Jan	k1gMnSc2	Jan
Husa	Hus	k1gMnSc2	Hus
v	v	k7c6	v
Husinci	Husinec	k1gInSc6	Husinec
po	po	k7c6	po
celkové	celkový	k2eAgFnSc6d1	celková
rekonstrukci	rekonstrukce	k1gFnSc6	rekonstrukce
Husova	Husův	k2eAgInSc2d1	Husův
rodného	rodný	k2eAgInSc2d1	rodný
domu	dům	k1gInSc2	dům
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2015	[number]	k4	2015
vydala	vydat	k5eAaPmAgFnS	vydat
Česká	český	k2eAgFnSc1d1	Česká
pošta	pošta	k1gFnSc1	pošta
známku	známka	k1gFnSc4	známka
s	s	k7c7	s
portrétem	portrét	k1gInSc7	portrét
Jana	Jan	k1gMnSc2	Jan
Husa	Hus	k1gMnSc2	Hus
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Suchánka	Suchánek	k1gMnSc2	Suchánek
<g/>
,	,	kIx,	,
založeného	založený	k2eAgInSc2d1	založený
na	na	k7c6	na
středověkém	středověký	k2eAgInSc6d1	středověký
dřevořezu	dřevořez	k1gInSc6	dřevořez
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
střelecký	střelecký	k2eAgInSc1d1	střelecký
pluk	pluk	k1gInSc1	pluk
Československých	československý	k2eAgFnPc2d1	Československá
legií	legie	k1gFnPc2	legie
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
pěší	pěší	k2eAgInSc4d1	pěší
pluk	pluk	k1gInSc4	pluk
1	[number]	k4	1
Československé	československý	k2eAgFnSc2d1	Československá
armády	armáda	k1gFnSc2	armáda
nesly	nést	k5eAaImAgFnP	nést
čestné	čestný	k2eAgNnSc4d1	čestné
pojmenování	pojmenování	k1gNnSc4	pojmenování
"	"	kIx"	"
<g/>
Mistra	mistr	k1gMnSc2	mistr
Jana	Jan	k1gMnSc2	Jan
Husi	Hus	k1gFnSc2	Hus
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
