<p>
<s>
Boromir	Boromir	k1gInSc1	Boromir
(	(	kIx(	(
<g/>
2978	[number]	k4	2978
Třetího	třetí	k4xOgInSc2	třetí
věku	věk	k1gInSc2	věk
–	–	k?	–
26	[number]	k4	26
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
3019	[number]	k4	3019
Třetího	třetí	k4xOgInSc2	třetí
věku	věk	k1gInSc2	věk
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
postava	postava	k1gFnSc1	postava
ve	v	k7c6	v
fiktivním	fiktivní	k2eAgInSc6d1	fiktivní
světě	svět	k1gInSc6	svět
J.	J.	kA	J.
R.	R.	kA	R.
R.	R.	kA	R.
Tolkiena	Tolkieno	k1gNnPc4	Tolkieno
<g/>
,	,	kIx,	,
Středozemi	Středozem	k1gFnSc4	Středozem
<g/>
.	.	kIx.	.
</s>
<s>
Objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
v	v	k7c6	v
trilogii	trilogie	k1gFnSc6	trilogie
Pán	pán	k1gMnSc1	pán
Prstenů	prsten	k1gInPc2	prsten
jako	jako	k8xS	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
devíti	devět	k4xCc2	devět
členů	člen	k1gInPc2	člen
Společenstva	společenstvo	k1gNnSc2	společenstvo
Prstenu	prsten	k1gInSc2	prsten
<g/>
.	.	kIx.	.
</s>
<s>
Boromir	Boromir	k1gMnSc1	Boromir
je	být	k5eAaImIp3nS	být
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgMnSc1d3	nejstarší
syn	syn	k1gMnSc1	syn
Denethora	Denethor	k1gMnSc2	Denethor
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
posledního	poslední	k2eAgMnSc2d1	poslední
vládnoucího	vládnoucí	k2eAgMnSc2d1	vládnoucí
správce	správce	k1gMnSc2	správce
Gondoru	Gondor	k1gInSc2	Gondor
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
také	také	k9	také
pochází	pocházet	k5eAaImIp3nS	pocházet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
filmovém	filmový	k2eAgNnSc6d1	filmové
zpracování	zpracování	k1gNnSc6	zpracování
trilogie	trilogie	k1gFnSc2	trilogie
Pán	pán	k1gMnSc1	pán
prstenů	prsten	k1gInPc2	prsten
jej	on	k3xPp3gMnSc4	on
hraje	hrát	k5eAaImIp3nS	hrát
herec	herec	k1gMnSc1	herec
Sean	Sean	k1gMnSc1	Sean
Bean	Bean	k1gMnSc1	Bean
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Boromirův	Boromirův	k2eAgInSc4d1	Boromirův
život	život	k1gInSc4	život
==	==	k?	==
</s>
</p>
<p>
<s>
Boromir	Boromir	k1gMnSc1	Boromir
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
roku	rok	k1gInSc2	rok
2978	[number]	k4	2978
Třetího	třetí	k4xOgInSc2	třetí
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
otcem	otec	k1gMnSc7	otec
byl	být	k5eAaImAgMnS	být
Denethor	Denethor	k1gMnSc1	Denethor
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
matkou	matka	k1gFnSc7	matka
Finduilas	Finduilasa	k1gFnPc2	Finduilasa
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
Adrahila	Adrahila	k1gFnSc1	Adrahila
z	z	k7c2	z
Dol	dol	k1gInSc4	dol
Amrothu	Amrotha	k1gFnSc4	Amrotha
<g/>
.	.	kIx.	.
</s>
<s>
Boromirův	Boromirův	k2eAgMnSc1d1	Boromirův
mladší	mladý	k2eAgMnSc1d2	mladší
bratr	bratr	k1gMnSc1	bratr
Faramir	Faramir	k1gMnSc1	Faramir
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
roku	rok	k1gInSc2	rok
2983	[number]	k4	2983
<g/>
.	.	kIx.	.
</s>
<s>
Následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
Denethor	Denethor	k1gMnSc1	Denethor
stal	stát	k5eAaPmAgMnS	stát
správcem	správce	k1gMnSc7	správce
trůnu	trůn	k1gInSc2	trůn
Gondoru	Gondor	k1gInSc2	Gondor
<g/>
,	,	kIx,	,
tuto	tento	k3xDgFnSc4	tento
hodnost	hodnost	k1gFnSc4	hodnost
převzal	převzít	k5eAaPmAgMnS	převzít
po	po	k7c6	po
svém	svůj	k3xOyFgMnSc6	svůj
otci	otec	k1gMnSc6	otec
Ecthelionovi	Ecthelion	k1gMnSc6	Ecthelion
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
2988	[number]	k4	2988
zemřela	zemřít	k5eAaPmAgFnS	zemřít
Finduilas	Finduilas	k1gInSc4	Finduilas
a	a	k8xC	a
Denethor	Denethor	k1gInSc4	Denethor
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
pochmurným	pochmurný	k2eAgMnSc7d1	pochmurný
<g/>
,	,	kIx,	,
chladným	chladný	k2eAgMnSc7d1	chladný
a	a	k8xC	a
izoloval	izolovat	k5eAaBmAgInS	izolovat
se	se	k3xPyFc4	se
od	od	k7c2	od
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
sblížili	sblížit	k5eAaPmAgMnP	sblížit
Boromir	Boromir	k1gMnSc1	Boromir
s	s	k7c7	s
Faramirem	Faramir	k1gInSc7	Faramir
a	a	k8xC	a
stali	stát	k5eAaPmAgMnP	stát
se	se	k3xPyFc4	se
opravdu	opravdu	k6eAd1	opravdu
velkými	velký	k2eAgMnPc7d1	velký
přáteli	přítel	k1gMnPc7	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Denethor	Denethor	k1gMnSc1	Denethor
vždycky	vždycky	k6eAd1	vždycky
upřednostňoval	upřednostňovat	k5eAaImAgMnS	upřednostňovat
Boromira	Boromira	k1gMnSc1	Boromira
<g/>
,	,	kIx,	,
pročež	pročež	k6eAd1	pročež
byl	být	k5eAaImAgInS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c2	za
odvážnějšího	odvážný	k2eAgInSc2d2	odvážnější
a	a	k8xC	a
nebojácnějšího	bojácný	k2eNgInSc2d2	bojácný
než	než	k8xS	než
jeho	jeho	k3xOp3gMnSc1	jeho
mladší	mladý	k2eAgMnSc1d2	mladší
bratr	bratr	k1gMnSc1	bratr
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
bratry	bratr	k1gMnPc7	bratr
však	však	k9	však
nepanovala	panovat	k5eNaImAgFnS	panovat
žádná	žádný	k3yNgFnSc1	žádný
rivalita	rivalita	k1gFnSc1	rivalita
<g/>
.	.	kIx.	.
</s>
<s>
Boromir	Boromir	k1gMnSc1	Boromir
vždycky	vždycky	k6eAd1	vždycky
Faramirovi	Faramir	k1gMnSc3	Faramir
pomáhal	pomáhat	k5eAaImAgMnS	pomáhat
a	a	k8xC	a
podporoval	podporovat	k5eAaImAgMnS	podporovat
ho	on	k3xPp3gMnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Boromir	Boromir	k1gMnSc1	Boromir
byl	být	k5eAaImAgMnS	být
velkým	velký	k2eAgMnSc7d1	velký
válečníkem	válečník	k1gMnSc7	válečník
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
v	v	k7c6	v
Gondoru	Gondor	k1gInSc6	Gondor
slavným	slavný	k2eAgMnSc7d1	slavný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
prorocký	prorocký	k2eAgInSc4d1	prorocký
sen	sen	k1gInSc4	sen
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
oba	dva	k4xCgMnPc1	dva
bratři	bratr	k1gMnPc1	bratr
měli	mít	k5eAaImAgMnP	mít
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Boromir	Boromir	k1gMnSc1	Boromir
zvolen	zvolit	k5eAaPmNgMnS	zvolit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
3018	[number]	k4	3018
vydal	vydat	k5eAaPmAgMnS	vydat
do	do	k7c2	do
Roklinky	roklinka	k1gFnSc2	roklinka
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
z	z	k7c2	z
Minas	Minasa	k1gFnPc2	Minasa
Tirith	Tirith	k1gInSc4	Tirith
mu	on	k3xPp3gMnSc3	on
zabrala	zabrat	k5eAaPmAgFnS	zabrat
110	[number]	k4	110
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
jel	jet	k5eAaImAgInS	jet
po	po	k7c6	po
jemu	on	k3xPp3gMnSc3	on
neznámých	neznámá	k1gFnPc6	neznámá
cestách	cesta	k1gFnPc6	cesta
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
konečně	konečně	k6eAd1	konečně
dorazil	dorazit	k5eAaPmAgMnS	dorazit
do	do	k7c2	do
Roklinky	roklinka	k1gFnSc2	roklinka
<g/>
,	,	kIx,	,
o	o	k7c4	o
které	který	k3yIgInPc4	který
málo	málo	k6eAd1	málo
Gondorských	Gondorský	k2eAgNnPc2d1	Gondorské
vůbec	vůbec	k9	vůbec
vědělo	vědět	k5eAaImAgNnS	vědět
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
brodu	brod	k1gInSc6	brod
v	v	k7c6	v
Tharbadu	Tharbad	k1gInSc6	Tharbad
Boromir	Boromir	k1gInSc1	Boromir
ztratil	ztratit	k5eAaPmAgMnS	ztratit
koně	kůň	k1gMnSc4	kůň
<g/>
,	,	kIx,	,
dál	daleko	k6eAd2	daleko
cestoval	cestovat	k5eAaImAgMnS	cestovat
po	po	k7c6	po
Zelené	Zelené	k2eAgFnSc6d1	Zelené
cestě	cesta	k1gFnSc6	cesta
pěšky	pěšky	k6eAd1	pěšky
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Roklinky	roklinka	k1gFnSc2	roklinka
přijel	přijet	k5eAaPmAgMnS	přijet
před	před	k7c7	před
začátkem	začátek	k1gInSc7	začátek
Elrondovy	Elrondův	k2eAgFnSc2d1	Elrondova
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
radě	rada	k1gFnSc6	rada
sdělil	sdělit	k5eAaPmAgMnS	sdělit
ostatním	ostatní	k2eAgMnPc3d1	ostatní
novinky	novinka	k1gFnPc4	novinka
z	z	k7c2	z
Gondoru	Gondor	k1gInSc2	Gondor
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
o	o	k7c6	o
neustálém	neustálý	k2eAgNnSc6d1	neustálé
válčení	válčení	k1gNnSc6	válčení
s	s	k7c7	s
Mordorem	Mordor	k1gInSc7	Mordor
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
ostatní	ostatní	k2eAgMnSc1d1	ostatní
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
směl	smět	k5eAaImAgMnS	smět
vzít	vzít	k5eAaPmF	vzít
Jeden	jeden	k4xCgInSc1	jeden
prsten	prsten	k1gInSc1	prsten
do	do	k7c2	do
Gondoru	Gondor	k1gInSc2	Gondor
a	a	k8xC	a
použít	použít	k5eAaPmF	použít
jej	on	k3xPp3gMnSc4	on
proti	proti	k7c3	proti
nepříteli	nepřítel	k1gMnSc3	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
ale	ale	k8xC	ale
nikdo	nikdo	k3yNnSc1	nikdo
nechtěl	chtít	k5eNaImAgMnS	chtít
dovolit	dovolit	k5eAaPmF	dovolit
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
Prsten	prsten	k1gInSc1	prsten
nelze	lze	k6eNd1	lze
použít	použít	k5eAaPmF	použít
proti	proti	k7c3	proti
Sauronovi	Sauron	k1gMnSc3	Sauron
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jej	on	k3xPp3gMnSc4	on
ovládá	ovládat	k5eAaImIp3nS	ovládat
svou	svůj	k3xOyFgFnSc7	svůj
vůlí	vůle	k1gFnSc7	vůle
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
alespoň	alespoň	k9	alespoň
přidal	přidat	k5eAaPmAgMnS	přidat
ke	k	k7c3	k
Společenstvu	společenstvo	k1gNnSc3	společenstvo
prstenu	prsten	k1gInSc2	prsten
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
zničit	zničit	k5eAaPmF	zničit
Prsten	prsten	k1gInSc4	prsten
v	v	k7c6	v
Hoře	hora	k1gFnSc6	hora
osudu	osud	k1gInSc2	osud
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
odchodem	odchod	k1gInSc7	odchod
Boromir	Boromir	k1gMnSc1	Boromir
zatroubil	zatroubit	k5eAaPmAgMnS	zatroubit
hlasitě	hlasitě	k6eAd1	hlasitě
na	na	k7c4	na
gondorský	gondorský	k2eAgInSc4d1	gondorský
roh	roh	k1gInSc4	roh
se	s	k7c7	s
slovy	slovo	k1gNnPc7	slovo
<g/>
,	,	kIx,	,
že	že	k8xS	že
neodejde	odejít	k5eNaPmIp3nS	odejít
jako	jako	k9	jako
zloděj	zloděj	k1gMnSc1	zloděj
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
plíží	plížit	k5eAaImIp3nS	plížit
nocí	noc	k1gFnSc7	noc
<g/>
.	.	kIx.	.
</s>
<s>
Elrond	Elrond	k1gMnSc1	Elrond
ho	on	k3xPp3gMnSc4	on
poté	poté	k6eAd1	poté
varoval	varovat	k5eAaImAgMnS	varovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
už	už	k6eAd1	už
takhle	takhle	k6eAd1	takhle
nikdy	nikdy	k6eAd1	nikdy
netroubil	troubit	k5eNaImAgMnS	troubit
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
nedorazí	dorazit	k5eNaPmIp3nS	dorazit
k	k	k7c3	k
hranicím	hranice	k1gFnPc3	hranice
Gondoru	Gondor	k1gInSc2	Gondor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Společenstvo	společenstvo	k1gNnSc1	společenstvo
se	se	k3xPyFc4	se
vydalo	vydat	k5eAaPmAgNnS	vydat
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
neúspěšném	úspěšný	k2eNgInSc6d1	neúspěšný
pokusu	pokus	k1gInSc6	pokus
o	o	k7c4	o
přechod	přechod	k1gInSc4	přechod
Mlžných	mlžný	k2eAgFnPc2d1	mlžná
hor	hora	k1gFnPc2	hora
průsmykem	průsmyk	k1gInSc7	průsmyk
Caradhrasu	Caradhras	k1gInSc2	Caradhras
Boromir	Boromir	k1gInSc1	Boromir
prokázal	prokázat	k5eAaPmAgInS	prokázat
svou	svůj	k3xOyFgFnSc4	svůj
sílu	síla	k1gFnSc4	síla
a	a	k8xC	a
statečnost	statečnost	k1gFnSc4	statečnost
při	při	k7c6	při
brodění	brodění	k1gNnSc6	brodění
hlubokým	hluboký	k2eAgInSc7d1	hluboký
sněhem	sníh	k1gInSc7	sníh
a	a	k8xC	a
bojem	boj	k1gInSc7	boj
s	s	k7c7	s
vlky	vlk	k1gMnPc7	vlk
v	v	k7c6	v
pustině	pustina	k1gFnSc6	pustina
<g/>
.	.	kIx.	.
</s>
<s>
Společenstvo	společenstvo	k1gNnSc1	společenstvo
se	se	k3xPyFc4	se
pokusilo	pokusit	k5eAaPmAgNnS	pokusit
překonat	překonat	k5eAaPmF	překonat
hory	hora	k1gFnPc4	hora
skrze	skrze	k?	skrze
doly	dol	k1gInPc4	dol
Morie	Morie	k1gFnSc2	Morie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
padl	padnout	k5eAaPmAgInS	padnout
Gandalf	Gandalf	k1gInSc1	Gandalf
v	v	k7c6	v
boji	boj	k1gInSc6	boj
s	s	k7c7	s
balrogem	balrog	k1gInSc7	balrog
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
Gandalfa	Gandalf	k1gMnSc2	Gandalf
převzal	převzít	k5eAaPmAgMnS	převzít
velení	velení	k1gNnSc4	velení
Aragorn	Aragorna	k1gFnPc2	Aragorna
<g/>
.	.	kIx.	.
</s>
<s>
Dalšího	další	k2eAgInSc2d1	další
dne	den	k1gInSc2	den
dorazili	dorazit	k5eAaPmAgMnP	dorazit
do	do	k7c2	do
Lothlórienu	Lothlórien	k1gInSc2	Lothlórien
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Boromir	Boromir	k1gMnSc1	Boromir
podstoupil	podstoupit	k5eAaPmAgMnS	podstoupit
zkoušku	zkouška	k1gFnSc4	zkouška
mysli	mysl	k1gFnSc2	mysl
od	od	k7c2	od
Galadriel	Galadriela	k1gFnPc2	Galadriela
<g/>
.	.	kIx.	.
</s>
<s>
Než	než	k8xS	než
Lórien	Lórien	k2eAgMnSc1d1	Lórien
opustili	opustit	k5eAaPmAgMnP	opustit
<g/>
,	,	kIx,	,
Boromir	Boromir	k1gInSc4	Boromir
dostal	dostat	k5eAaPmAgMnS	dostat
od	od	k7c2	od
elfů	elf	k1gMnPc2	elf
dary	dar	k1gInPc4	dar
<g/>
:	:	kIx,	:
elfí	elfí	k2eAgInSc4d1	elfí
plášť	plášť	k1gInSc4	plášť
a	a	k8xC	a
zlatý	zlatý	k2eAgInSc4d1	zlatý
opasek	opasek	k1gInSc4	opasek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Boromir	Boromir	k1gMnSc1	Boromir
se	se	k3xPyFc4	se
jako	jako	k8xS	jako
gondorský	gondorský	k2eAgMnSc1d1	gondorský
válečník	válečník	k1gMnSc1	válečník
obával	obávat	k5eAaImAgMnS	obávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Gondor	Gondor	k1gInSc1	Gondor
brzy	brzy	k6eAd1	brzy
padne	padnout	k5eAaImIp3nS	padnout
před	před	k7c7	před
Sauronovým	Sauronův	k2eAgInSc7d1	Sauronův
náporem	nápor	k1gInSc7	nápor
<g/>
,	,	kIx,	,
a	a	k8xC	a
protože	protože	k8xS	protože
chtěl	chtít	k5eAaImAgMnS	chtít
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
zvítězit	zvítězit	k5eAaPmF	zvítězit
(	(	kIx(	(
<g/>
a	a	k8xC	a
získat	získat	k5eAaPmF	získat
slávu	sláva	k1gFnSc4	sláva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Prsten	prsten	k1gInSc1	prsten
nad	nad	k7c7	nad
ním	on	k3xPp3gMnSc7	on
pomalu	pomalu	k6eAd1	pomalu
začal	začít	k5eAaPmAgMnS	začít
získávat	získávat	k5eAaImF	získávat
moc	moc	k6eAd1	moc
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
však	však	k9	však
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
Prsten	prsten	k1gInSc1	prsten
bude	být	k5eAaImBp3nS	být
zničen	zničit	k5eAaPmNgInS	zničit
v	v	k7c6	v
ohních	oheň	k1gInPc6	oheň
Hory	hora	k1gFnSc2	hora
osudu	osud	k1gInSc2	osud
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
Boromir	Boromir	k1gInSc1	Boromir
nesouhlasil	souhlasit	k5eNaImAgInS	souhlasit
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
přesvědčoval	přesvědčovat	k5eAaImAgMnS	přesvědčovat
ostatní	ostatní	k2eAgMnPc4d1	ostatní
<g/>
,	,	kIx,	,
že	že	k8xS	že
Prsten	prsten	k1gInSc1	prsten
bude	být	k5eAaImBp3nS	být
v	v	k7c6	v
Gondoru	Gondor	k1gInSc6	Gondor
v	v	k7c6	v
bezpečí	bezpečí	k1gNnSc6	bezpečí
a	a	k8xC	a
pomůže	pomoct	k5eAaPmIp3nS	pomoct
ubránit	ubránit	k5eAaPmF	ubránit
jeho	jeho	k3xOp3gFnSc4	jeho
zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Neuspěl	uspět	k5eNaPmAgMnS	uspět
však	však	k9	však
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k9	tak
se	se	k3xPyFc4	se
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
mysli	mysl	k1gFnSc6	mysl
<g/>
,	,	kIx,	,
pomalu	pomalu	k6eAd1	pomalu
ovládané	ovládaný	k2eAgNnSc4d1	ovládané
Prstenem	prsten	k1gInSc7	prsten
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgInS	začít
rodit	rodit	k5eAaImF	rodit
nápad	nápad	k1gInSc1	nápad
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
Prsten	prsten	k1gInSc1	prsten
mohl	moct	k5eAaImAgInS	moct
vzít	vzít	k5eAaPmF	vzít
sám	sám	k3xTgMnSc1	sám
<g/>
,	,	kIx,	,
a	a	k8xC	a
ochránit	ochránit	k5eAaPmF	ochránit
svůj	svůj	k3xOyFgInSc4	svůj
lid	lid	k1gInSc4	lid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
Frodo	Frodo	k1gNnSc1	Frodo
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
nesl	nést	k5eAaImAgInS	nést
Prsten	prsten	k1gInSc4	prsten
<g/>
,	,	kIx,	,
vzdálil	vzdálit	k5eAaPmAgInS	vzdálit
od	od	k7c2	od
ostatních	ostatní	k1gNnPc2	ostatní
<g/>
,	,	kIx,	,
vydal	vydat	k5eAaPmAgInS	vydat
se	se	k3xPyFc4	se
Boromir	Boromir	k1gInSc1	Boromir
za	za	k7c7	za
ním	on	k3xPp3gMnSc7	on
a	a	k8xC	a
požadoval	požadovat	k5eAaImAgMnS	požadovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gMnSc3	on
Frodo	Froda	k1gFnSc5	Froda
Prsten	prsten	k1gInSc4	prsten
vydal	vydat	k5eAaPmAgMnS	vydat
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
samozřejmě	samozřejmě	k6eAd1	samozřejmě
nechtěl	chtít	k5eNaImAgMnS	chtít
a	a	k8xC	a
když	když	k8xS	když
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
ho	on	k3xPp3gMnSc4	on
pokusil	pokusit	k5eAaPmAgMnS	pokusit
Boromir	Boromir	k1gMnSc1	Boromir
násilím	násilí	k1gNnSc7	násilí
vzít	vzít	k5eAaPmF	vzít
<g/>
,	,	kIx,	,
nasadil	nasadit	k5eAaPmAgMnS	nasadit
Prsten	prsten	k1gInSc4	prsten
a	a	k8xC	a
utekl	utéct	k5eAaPmAgMnS	utéct
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
teď	teď	k6eAd1	teď
si	se	k3xPyFc3	se
Boromir	Boromir	k1gMnSc1	Boromir
uvědomil	uvědomit	k5eAaPmAgMnS	uvědomit
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
provedl	provést	k5eAaPmAgMnS	provést
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bylo	být	k5eAaImAgNnS	být
příliš	příliš	k6eAd1	příliš
pozdě	pozdě	k6eAd1	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Věděl	vědět	k5eAaImAgMnS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Společenstvo	společenstvo	k1gNnSc1	společenstvo
by	by	kYmCp3nP	by
ho	on	k3xPp3gMnSc4	on
už	už	k6eAd1	už
nepřijalo	přijmout	k5eNaPmAgNnS	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
chvíli	chvíle	k1gFnSc6	chvíle
zaútočili	zaútočit	k5eAaPmAgMnP	zaútočit
skuruti	skurut	k1gMnPc1	skurut
a	a	k8xC	a
Boromir	Boromir	k1gInSc1	Boromir
se	se	k3xPyFc4	se
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
pustil	pustit	k5eAaPmAgMnS	pustit
do	do	k7c2	do
bitvy	bitva	k1gFnSc2	bitva
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
smrtelně	smrtelně	k6eAd1	smrtelně
zraněn	zranit	k5eAaPmNgMnS	zranit
<g/>
,	,	kIx,	,
když	když	k8xS	když
bránil	bránit	k5eAaImAgMnS	bránit
hobity	hobit	k1gMnPc4	hobit
Smíška	Smíšek	k1gMnSc4	Smíšek
a	a	k8xC	a
Pipina	pipina	k1gFnSc1	pipina
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
Boromir	Boromir	k1gMnSc1	Boromir
odčinil	odčinit	k5eAaPmAgMnS	odčinit
zlo	zlo	k1gNnSc4	zlo
<g/>
,	,	kIx,	,
kterého	který	k3yIgInSc2	který
se	se	k3xPyFc4	se
dopustil	dopustit	k5eAaPmAgMnS	dopustit
na	na	k7c6	na
Frodovi	Froda	k1gMnSc6	Froda
<g/>
,	,	kIx,	,
a	a	k8xC	a
odešel	odejít	k5eAaPmAgMnS	odejít
jako	jako	k9	jako
čestný	čestný	k2eAgMnSc1d1	čestný
muž	muž	k1gMnSc1	muž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
smrtí	smrt	k1gFnSc7	smrt
zatroubil	zatroubit	k5eAaPmAgMnS	zatroubit
na	na	k7c4	na
gondorský	gondorský	k2eAgInSc4d1	gondorský
roh	roh	k1gInSc4	roh
a	a	k8xC	a
přivolal	přivolat	k5eAaPmAgInS	přivolat
tak	tak	k6eAd1	tak
Aragorna	Aragorno	k1gNnSc2	Aragorno
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
však	však	k9	však
přiběhl	přiběhnout	k5eAaPmAgMnS	přiběhnout
příliš	příliš	k6eAd1	příliš
pozdě	pozdě	k6eAd1	pozdě
a	a	k8xC	a
hobiti	hobit	k1gMnPc1	hobit
byli	být	k5eAaImAgMnP	být
odvlečeni	odvléct	k5eAaPmNgMnP	odvléct
<g/>
.	.	kIx.	.
</s>
<s>
Než	než	k8xS	než
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
požádal	požádat	k5eAaPmAgMnS	požádat
Aragorna	Aragorno	k1gNnPc4	Aragorno
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
ubránit	ubránit	k5eAaPmF	ubránit
Minas	Minas	k1gMnSc1	Minas
Tirith	Tirith	k1gMnSc1	Tirith
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Aragorn	Aragorn	k1gMnSc1	Aragorn
<g/>
,	,	kIx,	,
Gimli	Giml	k1gMnPc1	Giml
a	a	k8xC	a
Legolas	Legolas	k1gInSc1	Legolas
ho	on	k3xPp3gNnSc4	on
tam	tam	k6eAd1	tam
nechtěli	chtít	k5eNaImAgMnP	chtít
jen	jen	k9	jen
tak	tak	k6eAd1	tak
nechat	nechat	k5eAaPmF	nechat
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
použili	použít	k5eAaPmAgMnP	použít
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
loděk	loďka	k1gFnPc2	loďka
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
Boromirovo	Boromirův	k2eAgNnSc4d1	Boromirovo
tělo	tělo	k1gNnSc4	tělo
uložili	uložit	k5eAaPmAgMnP	uložit
a	a	k8xC	a
poslali	poslat	k5eAaPmAgMnP	poslat
ho	on	k3xPp3gNnSc4	on
společně	společně	k6eAd1	společně
s	s	k7c7	s
jeho	jeho	k3xOp3gInSc7	jeho
mečem	meč	k1gInSc7	meč
<g/>
,	,	kIx,	,
štítem	štít	k1gInSc7	štít
<g/>
,	,	kIx,	,
opaskem	opasek	k1gInSc7	opasek
<g/>
,	,	kIx,	,
pláštěm	plášť	k1gInSc7	plášť
<g/>
,	,	kIx,	,
zlomeným	zlomený	k2eAgInSc7d1	zlomený
rohem	roh	k1gInSc7	roh
a	a	k8xC	a
se	s	k7c7	s
zbraněmi	zbraň	k1gFnPc7	zbraň
nepřátel	nepřítel	k1gMnPc2	nepřítel
<g/>
,	,	kIx,	,
jež	jenž	k3xRgMnPc4	jenž
zabil	zabít	k5eAaPmAgMnS	zabít
<g/>
,	,	kIx,	,
do	do	k7c2	do
Rauroských	Rauroský	k2eAgInPc2d1	Rauroský
vodopádů	vodopád	k1gInPc2	vodopád
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
pak	pak	k6eAd1	pak
zazpívali	zazpívat	k5eAaPmAgMnP	zazpívat
na	na	k7c4	na
Boromirovu	Boromirův	k2eAgFnSc4d1	Boromirova
počest	počest	k1gFnSc4	počest
píseň	píseň	k1gFnSc4	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
26	[number]	k4	26
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
3019	[number]	k4	3019
Třetího	třetí	k4xOgInSc2	třetí
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
třech	tři	k4xCgInPc6	tři
dnech	den	k1gInPc6	den
nalezl	naleznout	k5eAaPmAgInS	naleznout
Faramir	Faramir	k1gInSc1	Faramir
loď	loď	k1gFnSc1	loď
s	s	k7c7	s
Boromirovým	Boromirův	k2eAgNnSc7d1	Boromirovo
tělem	tělo	k1gNnSc7	tělo
<g/>
,	,	kIx,	,
plujícím	plující	k2eAgMnSc7d1	plující
po	po	k7c6	po
řece	řeka	k1gFnSc6	řeka
Anduině	Anduin	k2eAgFnSc6d1	Anduin
<g/>
.	.	kIx.	.
</s>
</p>
