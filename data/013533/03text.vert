<s>
Ahmed	Ahmed	k1gInSc1
Ben	Ben	k1gInSc1
Bella	Bell	k1gMnSc2
</s>
<s>
Ahmed	Ahmed	k1gInSc1
Ben	Ben	k1gInSc1
Bella	Bell	k1gMnSc2
Narození	narození	k1gNnSc2
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1916	#num#	k4
<g/>
Maghnia	Maghnium	k1gNnSc2
Úmrtí	úmrtí	k1gNnSc2
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2012	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
95	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Alžír	Alžír	k1gInSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
politik	politik	k1gMnSc1
a	a	k8xC
fotbalista	fotbalista	k1gMnSc1
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Válečný	válečný	k2eAgInSc1d1
kříž	kříž	k1gInSc1
1939	#num#	k4
<g/>
–	–	k?
<g/>
1945	#num#	k4
<g/>
Leninova	Leninův	k2eAgFnSc1d1
mírová	mírový	k2eAgFnSc1d1
cenaŘád	cenaŘáda	k1gFnPc2
společníků	společník	k1gMnPc2
O.	O.	kA
R.	R.	kA
TambaVálečný	TambaVálečný	k2eAgInSc1d1
křížNárodní	křížNárodní	k2eAgInSc1d1
řád	řád	k1gInSc1
za	za	k7c2
zásluhy	zásluha	k1gFnSc2
<g/>
…	…	k?
více	hodně	k6eAd2
na	na	k7c6
Wikidatech	Wikidat	k1gInPc6
Politické	politický	k2eAgFnPc1d1
strany	strana	k1gFnPc1
</s>
<s>
Fronta	fronta	k1gFnSc1
národního	národní	k2eAgNnSc2d1
osvobození	osvobození	k1gNnSc2
(	(	kIx(
<g/>
1954	#num#	k4
<g/>
–	–	k?
<g/>
1965	#num#	k4
<g/>
)	)	kIx)
<g/>
Movement	Movement	k1gInSc1
for	forum	k1gNnPc2
Democracy	Democraca	k1gFnSc2
in	in	k?
Algeria	Algerium	k1gNnPc1
(	(	kIx(
<g/>
1982	#num#	k4
<g/>
–	–	k?
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
<g/>
Algerian	Algerian	k1gMnSc1
People	People	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Party	party	k1gFnSc7
Nábož	Nábož	k1gFnSc1
<g/>
.	.	kIx.
vyznání	vyznání	k1gNnSc1
</s>
<s>
sunnitský	sunnitský	k2eAgInSc1d1
islám	islám	k1gInSc1
Funkce	funkce	k1gFnSc1
</s>
<s>
Minister	Minister	k1gInSc1
of	of	k?
Foreign	Foreign	k1gInSc1
Affairs	Affairs	k1gInSc1
of	of	k?
Algeria	Algerium	k1gNnSc2
(	(	kIx(
<g/>
duben	duben	k1gInSc1
1963	#num#	k4
–	–	k?
září	zářit	k5eAaImIp3nS
1963	#num#	k4
<g/>
)	)	kIx)
<g/>
prezident	prezident	k1gMnSc1
Alžírska	Alžírsko	k1gNnSc2
(	(	kIx(
<g/>
1963	#num#	k4
<g/>
–	–	k?
<g/>
1965	#num#	k4
<g/>
)	)	kIx)
Podpis	podpis	k1gInSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Ahmed	Ahmed	k1gMnSc1
Ben	Ben	k1gMnSc1
Bella	Bella	k1gMnSc1
(	(	kIx(
<g/>
25	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1916	#num#	k4
v	v	k7c6
Marnii	Marnie	k1gFnSc6
–	–	k?
11	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2012	#num#	k4
v	v	k7c6
Alžíru	Alžír	k1gInSc6
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
alžírský	alžírský	k2eAgMnSc1d1
politik	politik	k1gMnSc1
<g/>
,	,	kIx,
voják	voják	k1gMnSc1
a	a	k8xC
revolucionář	revolucionář	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
prvním	první	k4xOgMnSc7
prezidentem	prezident	k1gMnSc7
nezávislého	závislý	k2eNgNnSc2d1
Alžírska	Alžírsko	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc4
v	v	k7c6
letech	let	k1gInPc6
1963	#num#	k4
<g/>
–	–	k?
<g/>
1965	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1936	#num#	k4
vstoupil	vstoupit	k5eAaPmAgMnS
do	do	k7c2
francouzské	francouzský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1939	#num#	k4
<g/>
–	–	k?
<g/>
1940	#num#	k4
byl	být	k5eAaImAgMnS
fotbalistou	fotbalista	k1gMnSc7
klubu	klub	k1gInSc2
Olympique	Olympique	k1gFnSc1
de	de	k?
Marseille	Marseille	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
získal	získat	k5eAaPmAgMnS
ve	v	k7c6
francouzské	francouzský	k2eAgFnSc6d1
armádě	armáda	k1gFnSc6
válečný	válečný	k2eAgInSc4d1
kříž	kříž	k1gInSc4
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
demobilizován	demobilizovat	k5eAaBmNgInS
po	po	k7c6
pádu	pád	k1gInSc6
Francie	Francie	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
vstoupil	vstoupit	k5eAaPmAgMnS
do	do	k7c2
marocké	marocký	k2eAgFnSc2d1
pěchotní	pěchotní	k2eAgFnSc2d1
divize	divize	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
zapojila	zapojit	k5eAaPmAgFnS
do	do	k7c2
bojů	boj	k1gInPc2
v	v	k7c6
Itálii	Itálie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bojoval	bojovat	k5eAaImAgMnS
v	v	k7c6
bitvě	bitva	k1gFnSc6
o	o	k7c6
Monte	Mont	k1gMnSc5
Casino	Casina	k1gMnSc5
<g/>
,	,	kIx,
za	za	k7c4
niž	jenž	k3xRgFnSc4
získal	získat	k5eAaPmAgMnS
od	od	k7c2
Charlese	Charles	k1gMnSc2
de	de	k?
Gaulla	Gaull	k1gMnSc2
Médaille	Médaill	k1gMnSc2
militaire	militair	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
roce	rok	k1gInSc6
1945	#num#	k4
se	se	k3xPyFc4
v	v	k7c6
Alžírsku	Alžírsko	k1gNnSc6
zapojil	zapojit	k5eAaPmAgMnS
do	do	k7c2
polovojenské	polovojenský	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
Organisation	Organisation	k1gInSc1
spéciale	spéciala	k1gFnSc3
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
zahájila	zahájit	k5eAaPmAgFnS
boj	boj	k1gInSc4
proti	proti	k7c3
francouzskému	francouzský	k2eAgNnSc3d1
koloniálnímu	koloniální	k2eAgNnSc3d1
panství	panství	k1gNnSc3
a	a	k8xC
za	za	k7c2
nezávislost	nezávislost	k1gFnSc4
Alžíru	Alžír	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
ní	on	k3xPp3gFnSc2
záhy	záhy	k6eAd1
vznikla	vzniknout	k5eAaPmAgFnS
Fronta	fronta	k1gFnSc1
národního	národní	k2eAgNnSc2d1
osvobození	osvobození	k1gNnSc2
(	(	kIx(
<g/>
Front	front	k1gInSc1
de	de	k?
Liberation	Liberation	k1gInSc1
Nationale	Nationale	k1gFnSc1
–	–	k?
FNL	FNL	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
posléze	posléze	k6eAd1
zahájila	zahájit	k5eAaPmAgFnS
ozbrojené	ozbrojený	k2eAgNnSc4d1
povstání	povstání	k1gNnSc4
(	(	kIx(
<g/>
roku	rok	k1gInSc2
1954	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1951	#num#	k4
byl	být	k5eAaImAgInS
Ben	Ben	k1gInSc1
Bella	Bello	k1gNnSc2
za	za	k7c4
svou	svůj	k3xOyFgFnSc4
činnost	činnost	k1gFnSc4
uvězněn	uvěznit	k5eAaPmNgMnS
a	a	k8xC
odsouzen	odsouzet	k5eAaImNgInS,k5eAaPmNgInS
k	k	k7c3
osmi	osm	k4xCc2
letům	let	k1gInPc3
žaláře	žalář	k1gInSc2
<g/>
,	,	kIx,
brzy	brzy	k6eAd1
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
však	však	k9
podařilo	podařit	k5eAaPmAgNnS
uprchnout	uprchnout	k5eAaPmF
a	a	k8xC
odjet	odjet	k5eAaPmF
do	do	k7c2
exilu	exil	k1gInSc2
v	v	k7c6
Egyptě	Egypt	k1gInSc6
a	a	k8xC
Tunisku	Tunisko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jakmile	jakmile	k8xS
vypuklo	vypuknout	k5eAaPmAgNnS
povstání	povstání	k1gNnSc1
<g/>
,	,	kIx,
Ben	Ben	k1gInSc1
Bella	Bello	k1gNnSc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
jedním	jeden	k4xCgMnSc7
z	z	k7c2
členů	člen	k1gMnPc2
Revolučního	revoluční	k2eAgInSc2d1
výboru	výbor	k1gInSc2
jednoty	jednota	k1gFnSc2
a	a	k8xC
akce	akce	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
řídil	řídit	k5eAaImAgMnS
FNL	FNL	kA
i	i	k9
průběh	průběh	k1gInSc4
bojů	boj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
Francouzi	Francouz	k1gMnSc3
zatčen	zatčen	k2eAgInSc1d1
roku	rok	k1gInSc2
1956	#num#	k4
<g/>
,	,	kIx,
když	když	k8xS
bylo	být	k5eAaImAgNnS
uneseno	unesen	k2eAgNnSc1d1
jeho	jeho	k3xOp3gNnSc4
letadlo	letadlo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
této	tento	k3xDgFnSc6
kontroverzní	kontroverzní	k2eAgFnSc6d1
akci	akce	k1gFnSc6
vznikly	vzniknout	k5eAaPmAgInP
rozpory	rozpor	k1gInPc1
uvnitř	uvnitř	k7c2
francouzské	francouzský	k2eAgFnSc2d1
Socialistické	socialistický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
u	u	k7c2
moci	moc	k1gFnSc2
(	(	kIx(
<g/>
kabinet	kabinet	k1gInSc1
Guy	Guy	k1gMnSc1
Molleta	Molleta	k1gFnSc1
<g/>
,	,	kIx,
po	po	k7c4
zatčení	zatčení	k1gNnSc4
rezignoval	rezignovat	k5eAaBmAgMnS
sekretář	sekretář	k1gMnSc1
pro	pro	k7c4
zahraniční	zahraniční	k2eAgFnPc4d1
věci	věc	k1gFnPc4
Alain	Alain	k2eAgInSc1d1
Savarin	Savarin	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ještě	ještě	k6eAd1
ve	v	k7c6
vězení	vězení	k1gNnSc6
byl	být	k5eAaImAgMnS
zvolen	zvolit	k5eAaPmNgMnS
místopředsedou	místopředseda	k1gMnSc7
Alžírské	alžírský	k2eAgFnSc2d1
prozatímní	prozatímní	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dosáhl	dosáhnout	k5eAaPmAgMnS
též	též	k9
velké	velký	k2eAgFnSc2d1
podpory	podpora	k1gFnSc2
egyptského	egyptský	k2eAgMnSc2d1
prezidenta	prezident	k1gMnSc2
Gamála	Gamál	k1gMnSc2
Násira	Násir	k1gMnSc2
<g/>
,	,	kIx,
sám	sám	k3xTgMnSc1
se	se	k3xPyFc4
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
označoval	označovat	k5eAaImAgInS
za	za	k7c4
násiristu	násirista	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Silně	silně	k6eAd1
ho	on	k3xPp3gMnSc4
podporoval	podporovat	k5eAaImAgInS
rovněž	rovněž	k9
Pákistán	Pákistán	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
mu	on	k3xPp3gMnSc3
udělil	udělit	k5eAaPmAgInS
pas	pas	k1gInSc1
<g/>
,	,	kIx,
na	na	k7c4
nějž	jenž	k3xRgMnSc4
po	po	k7c6
propuštění	propuštění	k1gNnSc6
cestoval	cestovat	k5eAaImAgMnS
po	po	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1962	#num#	k4
revoluce	revoluce	k1gFnSc1
zvítězila	zvítězit	k5eAaPmAgFnS
a	a	k8xC
Alžírsko	Alžírsko	k1gNnSc1
získalo	získat	k5eAaPmAgNnS
nezávislost	nezávislost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ben	Ben	k1gInSc1
Bella	Bello	k1gNnSc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
rychle	rychle	k6eAd1
nejpopulárnější	populární	k2eAgFnSc7d3
osobností	osobnost	k1gFnSc7
FNL	FNL	kA
a	a	k8xC
stal	stát	k5eAaPmAgInS
se	s	k7c7
premiérem	premiér	k1gMnSc7
země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1963	#num#	k4
získal	získat	k5eAaPmAgInS
i	i	k9
funkci	funkce	k1gFnSc4
prezidenta	prezident	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úspěšně	úspěšně	k6eAd1
odrazil	odrazit	k5eAaPmAgMnS
marockou	marocký	k2eAgFnSc4d1
invazi	invaze	k1gFnSc4
a	a	k8xC
zahájil	zahájit	k5eAaPmAgInS
rozsáhlou	rozsáhlý	k2eAgFnSc4d1
pozemkovou	pozemkový	k2eAgFnSc4d1
reformu	reforma	k1gFnSc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
velké	velký	k2eAgNnSc1d1
množství	množství	k1gNnSc1
půdy	půda	k1gFnSc2
bylo	být	k5eAaImAgNnS
rozdáno	rozdat	k5eAaPmNgNnS
bezzemkům	bezzemek	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc1
socialistická	socialistický	k2eAgFnSc1d1
rétorika	rétorika	k1gFnSc1
mu	on	k3xPp3gMnSc3
získala	získat	k5eAaPmAgFnS
přízeň	přízeň	k1gFnSc4
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
svým	svůj	k3xOyFgFnPc3
autokratickým	autokratický	k2eAgFnPc3d1
tendencím	tendence	k1gFnPc3
si	se	k3xPyFc3
však	však	k9
rychle	rychle	k6eAd1
znepřátelil	znepřátelit	k5eAaPmAgInS
řadu	řada	k1gFnSc4
lidí	člověk	k1gMnPc2
uvnitř	uvnitř	k7c2
FNL	FNL	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1965	#num#	k4
byl	být	k5eAaImAgInS
proto	proto	k8xC
Ben	Ben	k1gInSc1
Bella	Bella	k1gMnSc1
svržen	svrhnout	k5eAaPmNgMnS
svým	svůj	k3xOyFgMnSc7
blízkým	blízký	k2eAgMnSc7d1
spolupracovníkem	spolupracovník	k1gMnSc7
Houari	Houar	k1gFnSc2
Boumedienem	Boumedien	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
ho	on	k3xPp3gMnSc4
umístil	umístit	k5eAaPmAgMnS
do	do	k7c2
domácího	domácí	k2eAgNnSc2d1
vězení	vězení	k1gNnSc2
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgNnSc6
byl	být	k5eAaImAgMnS
až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1980	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Ahmed	Ahmed	k1gMnSc1
Ben	Ben	k1gInSc4
Bella	Bello	k1gNnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Heslo	heslo	k1gNnSc1
v	v	k7c6
Encyklopedii	encyklopedie	k1gFnSc6
Britannica	Britannic	k1gInSc2
</s>
<s>
Portrét	portrét	k1gInSc1
v	v	k7c6
New	New	k1gFnSc6
York	York	k1gInSc1
Times	Timesa	k1gFnPc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
118658123	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2145	#num#	k4
3402	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
83026116	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
100298032	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
83026116	#num#	k4
</s>
