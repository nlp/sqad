<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
rok	rok	k1gInSc1	rok
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
predstavuje	predstavovat	k5eAaImIp3nS	predstavovat
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
světlo	světlo	k1gNnSc1	světlo
ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
urazí	urazit	k5eAaPmIp3nS	urazit
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
juliánský	juliánský	k2eAgInSc4d1	juliánský
rok	rok	k1gInSc4	rok
<g/>
?	?	kIx.	?
</s>
