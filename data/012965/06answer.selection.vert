<s>
Jeden	jeden	k4xCgInSc1	jeden
světelný	světelný	k2eAgInSc1d1	světelný
rok	rok	k1gInSc1	rok
je	být	k5eAaImIp3nS	být
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
světlo	světlo	k1gNnSc1	světlo
ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
urazí	urazit	k5eAaPmIp3nS	urazit
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
juliánský	juliánský	k2eAgInSc4d1	juliánský
rok	rok	k1gInSc4	rok
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
