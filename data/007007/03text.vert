<s>
Michael	Michael	k1gMnSc1	Michael
Emerson	Emerson	k1gMnSc1	Emerson
(	(	kIx(	(
<g/>
*	*	kIx~	*
7	[number]	k4	7
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1954	[number]	k4	1954
Iowa	Iow	k1gInSc2	Iow
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
divadelní	divadelní	k2eAgMnSc1d1	divadelní
a	a	k8xC	a
filmový	filmový	k2eAgMnSc1d1	filmový
herec	herec	k1gMnSc1	herec
<g/>
.	.	kIx.	.
</s>
<s>
Získal	získat	k5eAaPmAgInS	získat
cenu	cena	k1gFnSc4	cena
Emmy	Emma	k1gFnSc2	Emma
"	"	kIx"	"
<g/>
Outstanding	Outstanding	k1gInSc1	Outstanding
Guest	Guest	k1gMnSc1	Guest
Actor	Actor	k1gMnSc1	Actor
<g/>
"	"	kIx"	"
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
za	za	k7c4	za
roli	role	k1gFnSc4	role
ve	v	k7c6	v
filmu	film	k1gInSc6	film
The	The	k1gFnSc2	The
Practice	Practice	k1gFnSc2	Practice
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
známý	známý	k1gMnSc1	známý
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
roli	role	k1gFnSc3	role
Zepa	Zep	k2eAgFnSc1d1	Zep
Hindla	Hindla	k1gFnSc1	Hindla
v	v	k7c6	v
filmu	film	k1gInSc6	film
Saw	Saw	k1gFnSc1	Saw
<g/>
:	:	kIx,	:
Hra	hra	k1gFnSc1	hra
o	o	k7c4	o
přežití	přežití	k1gNnSc4	přežití
a	a	k8xC	a
také	také	k9	také
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
roli	role	k1gFnSc3	role
Benjamina	Benjamin	k1gMnSc2	Benjamin
Linuse	Linuse	k1gFnSc2	Linuse
v	v	k7c6	v
televizním	televizní	k2eAgInSc6d1	televizní
seriálu	seriál	k1gInSc6	seriál
Lost	Lost	k1gInSc1	Lost
a	a	k8xC	a
také	také	k9	také
seriálem	seriál	k1gInSc7	seriál
Lovci	lovec	k1gMnPc1	lovec
zločinců	zločinec	k1gMnPc2	zločinec
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hraje	hrát	k5eAaImIp3nS	hrát
geniálního	geniální	k2eAgMnSc4d1	geniální
programátora	programátor	k1gMnSc4	programátor
a	a	k8xC	a
miliardáře	miliardář	k1gMnSc4	miliardář
Harolda	Harold	k1gMnSc4	Harold
Finche	Finch	k1gMnSc4	Finch
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
snů	sen	k1gInPc2	sen
–	–	k?	–
1999	[number]	k4	1999
The	The	k1gFnSc1	The
Practice	Practice	k1gFnSc1	Practice
–	–	k?	–
2001	[number]	k4	2001
Nevěrná	věrný	k2eNgFnSc1d1	nevěrná
–	–	k?	–
2002	[number]	k4	2002
Saw	Saw	k1gFnSc1	Saw
<g/>
:	:	kIx,	:
Hra	hra	k1gFnSc1	hra
o	o	k7c4	o
přežití	přežití	k1gNnSc4	přežití
–	–	k?	–
2004	[number]	k4	2004
Legenda	legenda	k1gFnSc1	legenda
o	o	k7c6	o
Zorrovi	Zorr	k1gMnSc6	Zorr
–	–	k?	–
2005	[number]	k4	2005
Lost	Lost	k1gInSc1	Lost
–	–	k?	–
2006	[number]	k4	2006
Hunger	Hunger	k1gInSc1	Hunger
Games	Games	k1gInSc4	Games
<g/>
:	:	kIx,	:
Catching	Catching	k1gInSc1	Catching
Fire	Fir	k1gInSc2	Fir
–	–	k?	–
2013	[number]	k4	2013
American	Americany	k1gInPc2	Americany
Experience	Experienec	k1gInSc2	Experienec
–	–	k?	–
1988	[number]	k4	1988
Akta	akta	k1gNnPc4	akta
X	X	kA	X
–	–	k?	–
1993	[number]	k4	1993
Advokáti	advokát	k1gMnPc1	advokát
–	–	k?	–
1997	[number]	k4	1997
Zákon	zákon	k1gInSc4	zákon
a	a	k8xC	a
pořádek	pořádek	k1gInSc4	pořádek
<g/>
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
Útvar	útvar	k1gInSc1	útvar
pro	pro	k7c4	pro
zvláštní	zvláštní	k2eAgFnPc4d1	zvláštní
oběti	oběť	k1gFnPc4	oběť
–	–	k?	–
1999	[number]	k4	1999
Policejní	policejní	k2eAgInSc1d1	policejní
okrsek	okrsek	k1gInSc1	okrsek
–	–	k?	–
2000	[number]	k4	2000
Zákon	zákon	k1gInSc4	zákon
a	a	k8xC	a
pořádek	pořádek	k1gInSc4	pořádek
<g/>
:	:	kIx,	:
Zločinné	zločinný	k2eAgInPc1d1	zločinný
úmysly	úmysl	k1gInPc1	úmysl
–	–	k?	–
2001	[number]	k4	2001
Beze	beze	k7c2	beze
stopy	stopa	k1gFnSc2	stopa
–	–	k?	–
2002	[number]	k4	2002
Ztraceni	ztratit	k5eAaPmNgMnP	ztratit
–	–	k?	–
2004	[number]	k4	2004
<g/>
/	/	kIx~	/
<g/>
2010	[number]	k4	2010
Famílie	famílie	k1gFnSc2	famílie
–	–	k?	–
2010	[number]	k4	2010
Lovci	lovec	k1gMnPc1	lovec
zločinců	zločinec	k1gMnPc2	zločinec
–	–	k?	–
2011	[number]	k4	2011
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Michael	Michael	k1gMnSc1	Michael
Emerson	Emerson	k1gNnSc4	Emerson
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
Michael	Michael	k1gMnSc1	Michael
Emerson	Emerson	k1gNnSc4	Emerson
neoficiální	neoficiální	k2eAgFnSc1d1	neoficiální
stránka	stránka	k1gFnSc1	stránka
Michael	Michael	k1gMnSc1	Michael
Emerson	Emerson	k1gMnSc1	Emerson
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Michael	Michael	k1gMnSc1	Michael
Emerson	Emerson	k1gMnSc1	Emerson
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
Michael	Michael	k1gMnSc1	Michael
Emerson	Emerson	k1gMnSc1	Emerson
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
