<s>
Umění	umění	k1gNnSc1	umění
starověkého	starověký	k2eAgNnSc2d1	starověké
Řecka	Řecko	k1gNnSc2	Řecko
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
vysoké	vysoký	k2eAgFnPc4d1	vysoká
úrovně	úroveň	k1gFnPc4	úroveň
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
architektury	architektura	k1gFnSc2	architektura
a	a	k8xC	a
sochařství	sochařství	k1gNnSc2	sochařství
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
díla	dílo	k1gNnPc1	dílo
klasického	klasický	k2eAgNnSc2d1	klasické
období	období	k1gNnSc2	období
opakovaně	opakovaně	k6eAd1	opakovaně
tvoří	tvořit	k5eAaImIp3nP	tvořit
inspiraci	inspirace	k1gFnSc4	inspirace
mladšímu	mladý	k2eAgNnSc3d2	mladší
umění	umění	k1gNnSc3	umění
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgInSc7d1	významný
oborem	obor	k1gInSc7	obor
je	být	k5eAaImIp3nS	být
také	také	k9	také
keramika	keramika	k1gFnSc1	keramika
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
malířská	malířský	k2eAgFnSc1d1	malířská
výzdoba	výzdoba	k1gFnSc1	výzdoba
<g/>
.	.	kIx.	.
</s>
<s>
Řecko	Řecko	k1gNnSc1	Řecko
navázalo	navázat	k5eAaPmAgNnS	navázat
na	na	k7c6	na
umění	umění	k1gNnSc6	umění
egejské	egejský	k2eAgFnSc2d1	Egejská
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
na	na	k7c4	na
umění	umění	k1gNnSc4	umění
mínojské	mínojský	k2eAgFnSc2d1	mínojská
a	a	k8xC	a
mykénské	mykénský	k2eAgFnSc2d1	mykénská
civilizace	civilizace	k1gFnSc2	civilizace
<g/>
.	.	kIx.	.
</s>
<s>
Osobitě	osobitě	k6eAd1	osobitě
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
v	v	k7c6	v
archaické	archaický	k2eAgFnSc6d1	archaická
době	doba	k1gFnSc6	doba
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vrcholného	vrcholný	k2eAgMnSc2d1	vrcholný
<g/>
,	,	kIx,	,
klasického	klasický	k2eAgNnSc2d1	klasické
období	období	k1gNnSc2	období
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
ve	v	k7c6	v
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
před	před	k7c7	před
n.	n.	k?	n.
l.	l.	k?	l.
V	v	k7c6	v
době	doba	k1gFnSc6	doba
helénské	helénský	k2eAgFnSc2d1	helénská
zahájené	zahájený	k2eAgFnSc2d1	zahájená
říší	říš	k1gFnSc7	říš
Alexandra	Alexandr	k1gMnSc2	Alexandr
Velikého	veliký	k2eAgMnSc2d1	veliký
se	se	k3xPyFc4	se
řecké	řecký	k2eAgNnSc1d1	řecké
umění	umění	k1gNnSc1	umění
otevřelo	otevřít	k5eAaPmAgNnS	otevřít
širším	široký	k2eAgFnPc3d2	širší
oblastem	oblast	k1gFnPc3	oblast
a	a	k8xC	a
vlivům	vliv	k1gInPc3	vliv
a	a	k8xC	a
vyznačovalo	vyznačovat	k5eAaImAgNnS	vyznačovat
se	se	k3xPyFc4	se
expresívností	expresívnost	k1gFnSc7	expresívnost
a	a	k8xC	a
zobrazením	zobrazení	k1gNnSc7	zobrazení
pohybu	pohyb	k1gInSc2	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Římská	římský	k2eAgFnSc1d1	římská
říše	říše	k1gFnSc1	říše
si	se	k3xPyFc3	se
řecké	řecký	k2eAgInPc4d1	řecký
státy	stát	k1gInPc4	stát
podmanila	podmanit	k5eAaPmAgFnS	podmanit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
význam	význam	k1gInSc1	význam
tradice	tradice	k1gFnSc2	tradice
a	a	k8xC	a
zručnosti	zručnost	k1gFnSc2	zručnost
řeckých	řecký	k2eAgMnPc2d1	řecký
umělců	umělec	k1gMnPc2	umělec
si	se	k3xPyFc3	se
udržely	udržet	k5eAaPmAgFnP	udržet
i	i	k8xC	i
nadále	nadále	k6eAd1	nadále
zásadní	zásadní	k2eAgInSc4d1	zásadní
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Architektura	architektura	k1gFnSc1	architektura
starověkého	starověký	k2eAgNnSc2d1	starověké
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Řecká	řecký	k2eAgFnSc1d1	řecká
architektura	architektura	k1gFnSc1	architektura
vycházela	vycházet	k5eAaImAgFnS	vycházet
ze	z	k7c2	z
starších	starý	k2eAgFnPc2d2	starší
dřevěných	dřevěný	k2eAgFnPc2d1	dřevěná
staveb	stavba	k1gFnPc2	stavba
a	a	k8xC	a
megaronu	megaron	k1gInSc2	megaron
<g/>
.	.	kIx.	.
</s>
<s>
Nejcharakterističtější	charakteristický	k2eAgFnSc1d3	nejcharakterističtější
stavba	stavba	k1gFnSc1	stavba
byl	být	k5eAaImAgInS	být
chrám	chrám	k1gInSc1	chrám
se	s	k7c7	s
sloupovým	sloupový	k2eAgInSc7d1	sloupový
ochozem	ochoz	k1gInSc7	ochoz
a	a	k8xC	a
štítem	štít	k1gInSc7	štít
tvaru	tvar	k1gInSc2	tvar
nízkého	nízký	k2eAgInSc2d1	nízký
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
<g/>
.	.	kIx.	.
</s>
<s>
Tvarosloví	tvarosloví	k1gNnSc1	tvarosloví
odpovídalo	odpovídat	k5eAaImAgNnS	odpovídat
jasně	jasně	k6eAd1	jasně
vymezeným	vymezený	k2eAgInPc3d1	vymezený
slohům	sloh	k1gInPc3	sloh
<g/>
,	,	kIx,	,
zvaným	zvaný	k2eAgNnSc7d1	zvané
řády	řád	k1gInPc7	řád
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
byly	být	k5eAaImAgFnP	být
dórský	dórský	k2eAgInSc4d1	dórský
<g/>
,	,	kIx,	,
iónský	iónský	k2eAgInSc4d1	iónský
a	a	k8xC	a
korintský	korintský	k2eAgInSc4d1	korintský
<g/>
.	.	kIx.	.
</s>
<s>
Chrámy	chrám	k1gInPc1	chrám
někdy	někdy	k6eAd1	někdy
stály	stát	k5eAaImAgInP	stát
na	na	k7c6	na
posvátných	posvátný	k2eAgNnPc6d1	posvátné
návrších-akropolích	návršíchkropolí	k1gNnPc6	návrších-akropolí
<g/>
.	.	kIx.	.
</s>
<s>
Centrum	centrum	k1gNnSc1	centrum
města	město	k1gNnSc2	město
tvořila	tvořit	k5eAaImAgFnS	tvořit
sloupořadím	sloupořadí	k1gNnPc3	sloupořadí
obklopená	obklopený	k2eAgFnSc1d1	obklopená
agora	agora	k1gFnSc1	agora
<g/>
,	,	kIx,	,
i	i	k8xC	i
jinde	jinde	k6eAd1	jinde
se	se	k3xPyFc4	se
používala	používat	k5eAaImAgFnS	používat
krytá	krytý	k2eAgFnSc1d1	krytá
promenáda	promenáda	k1gFnSc1	promenáda
otevřená	otevřený	k2eAgFnSc1d1	otevřená
sloupořadím	sloupořadí	k1gNnSc7	sloupořadí
zvaná	zvaný	k2eAgFnSc1d1	zvaná
stoa	stoa	k1gFnSc1	stoa
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
typickým	typický	k2eAgFnPc3d1	typická
veřejným	veřejný	k2eAgFnPc3d1	veřejná
stavbám	stavba	k1gFnPc3	stavba
patřilo	patřit	k5eAaImAgNnS	patřit
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Sochařství	sochařství	k1gNnSc2	sochařství
starověkého	starověký	k2eAgNnSc2d1	starověké
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýraznější	výrazný	k2eAgFnPc1d3	nejvýraznější
sochy	socha	k1gFnPc1	socha
archaického	archaický	k2eAgNnSc2d1	archaické
období	období	k1gNnSc2	období
jsou	být	k5eAaImIp3nP	být
poněkud	poněkud	k6eAd1	poněkud
strnule	strnule	k6eAd1	strnule
stojící	stojící	k2eAgFnPc4d1	stojící
postavy	postava	k1gFnPc4	postava
se	s	k7c7	s
zasněným	zasněný	k2eAgInSc7d1	zasněný
úsměvem	úsměv	k1gInSc7	úsměv
nazývané	nazývaný	k2eAgInPc4d1	nazývaný
kúros	kúros	k1gInSc4	kúros
(	(	kIx(	(
<g/>
chlapec	chlapec	k1gMnSc1	chlapec
<g/>
)	)	kIx)	)
a	a	k8xC	a
koré	koré	k6eAd1	koré
(	(	kIx(	(
<g/>
dívka	dívka	k1gFnSc1	dívka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
nejvýznamnější	významný	k2eAgMnPc1d3	nejvýznamnější
sochaři	sochař	k1gMnPc1	sochař
(	(	kIx(	(
<g/>
Feidiás	Feidiás	k1gInSc1	Feidiás
<g/>
,	,	kIx,	,
Polykleitos	Polykleitos	k1gInSc1	Polykleitos
<g/>
)	)	kIx)	)
a	a	k8xC	a
řecké	řecký	k2eAgNnSc1d1	řecké
sochařství	sochařství	k1gNnSc1	sochařství
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
vrcholu	vrchol	k1gInSc3	vrchol
klasického	klasický	k2eAgNnSc2d1	klasické
období	období	k1gNnSc2	období
<g/>
.	.	kIx.	.
</s>
<s>
Zobrazeno	zobrazen	k2eAgNnSc1d1	zobrazeno
je	být	k5eAaImIp3nS	být
anatomicky	anatomicky	k6eAd1	anatomicky
zvládnuté	zvládnutý	k2eAgNnSc1d1	zvládnuté
a	a	k8xC	a
ideálně	ideálně	k6eAd1	ideálně
krásné	krásný	k2eAgNnSc4d1	krásné
lidské	lidský	k2eAgNnSc4d1	lidské
tělo	tělo	k1gNnSc4	tělo
<g/>
,	,	kIx,	,
vyjadřující	vyjadřující	k2eAgFnSc4d1	vyjadřující
v	v	k7c6	v
kontrapostu	kontrapost	k1gInSc6	kontrapost
(	(	kIx(	(
<g/>
postoj	postoj	k1gInSc4	postoj
s	s	k7c7	s
jednou	jeden	k4xCgFnSc7	jeden
nohou	noha	k1gFnSc7	noha
mírně	mírně	k6eAd1	mírně
pokrčenou	pokrčený	k2eAgFnSc4d1	pokrčená
<g/>
)	)	kIx)	)
harmonickou	harmonický	k2eAgFnSc4d1	harmonická
vyrovnanost	vyrovnanost	k1gFnSc4	vyrovnanost
<g/>
.	.	kIx.	.
</s>
<s>
Sochařství	sochařství	k1gNnSc1	sochařství
se	se	k3xPyFc4	se
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
i	i	k9	i
v	v	k7c6	v
architektuře	architektura	k1gFnSc6	architektura
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vysokém	vysoký	k2eAgInSc6d1	vysoký
reliéf	reliéf	k1gInSc1	reliéf
např.	např.	kA	např.
v	v	k7c6	v
tympanonech	tympanon	k1gInPc6	tympanon
chrámových	chrámový	k2eAgInPc2d1	chrámový
štítů	štít	k1gInPc2	štít
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
figurální	figurální	k2eAgInPc1d1	figurální
sloupy	sloup	k1gInPc1	sloup
<g/>
,	,	kIx,	,
karyatidy	karyatida	k1gFnPc1	karyatida
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
nadále	nadále	k6eAd1	nadále
používá	používat	k5eAaImIp3nS	používat
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
barvený	barvený	k2eAgInSc4d1	barvený
mramor	mramor	k1gInSc4	mramor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
pro	pro	k7c4	pro
volné	volný	k2eAgNnSc4d1	volné
řecké	řecký	k2eAgNnSc4d1	řecké
sochařství	sochařství	k1gNnSc4	sochařství
je	být	k5eAaImIp3nS	být
hojněji	hojně	k6eAd2	hojně
používán	používat	k5eAaImNgInS	používat
bronz	bronz	k1gInSc1	bronz
a	a	k8xC	a
chryselefantina	chryselefantina	k1gFnSc1	chryselefantina
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
kombinace	kombinace	k1gFnSc1	kombinace
zlata	zlato	k1gNnSc2	zlato
a	a	k8xC	a
slonoviny	slonovina	k1gFnSc2	slonovina
<g/>
.	.	kIx.	.
</s>
<s>
Plastika	plastika	k1gFnSc1	plastika
helénského	helénský	k2eAgNnSc2d1	helénské
období	období	k1gNnSc2	období
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
pro	pro	k7c4	pro
zobrazení	zobrazení	k1gNnSc4	zobrazení
pohybu	pohyb	k1gInSc2	pohyb
a	a	k8xC	a
výrazovost	výrazovost	k1gFnSc1	výrazovost
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
"	"	kIx"	"
<g/>
barokní	barokní	k2eAgFnSc1d1	barokní
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
dílem	díl	k1gInSc7	díl
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
nejznámější	známý	k2eAgNnSc1d3	nejznámější
helénské	helénský	k2eAgNnSc1d1	helénské
dílo	dílo	k1gNnSc1	dílo
<g/>
,	,	kIx,	,
sousoší	sousoší	k1gNnSc1	sousoší
Láokoóna	Láokoón	k1gMnSc2	Láokoón
se	s	k7c7	s
syny	syn	k1gMnPc7	syn
rdoušení	rdoušení	k1gNnSc1	rdoušení
jedovatými	jedovatý	k2eAgMnPc7d1	jedovatý
hady	had	k1gMnPc7	had
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Starověká	starověký	k2eAgFnSc1d1	starověká
řecká	řecký	k2eAgFnSc1d1	řecká
keramika	keramika	k1gFnSc1	keramika
<g/>
.	.	kIx.	.
</s>
<s>
Řecká	řecký	k2eAgFnSc1d1	řecká
keramika	keramika	k1gFnSc1	keramika
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
tvarovým	tvarový	k2eAgNnSc7d1	tvarové
bohatstvím	bohatství	k1gNnSc7	bohatství
nádob	nádoba	k1gFnPc2	nádoba
různých	různý	k2eAgInPc2d1	různý
účelů	účel	k1gInPc2	účel
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
výzdoba	výzdoba	k1gFnSc1	výzdoba
se	se	k3xPyFc4	se
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
z	z	k7c2	z
geometrické	geometrický	k2eAgFnSc2d1	geometrická
a	a	k8xC	a
dekorativní	dekorativní	k2eAgFnSc2d1	dekorativní
výzdoby	výzdoba	k1gFnSc2	výzdoba
k	k	k7c3	k
figurálním	figurální	k2eAgFnPc3d1	figurální
scénám	scéna	k1gFnPc3	scéna
představující	představující	k2eAgInPc1d1	představující
motivy	motiv	k1gInPc4	motiv
z	z	k7c2	z
mytologie	mytologie	k1gFnSc2	mytologie
<g/>
,	,	kIx,	,
válečné	válečný	k2eAgFnPc1d1	válečná
<g/>
,	,	kIx,	,
lovecké	lovecký	k2eAgFnPc1d1	lovecká
<g/>
,	,	kIx,	,
z	z	k7c2	z
každodenního	každodenní	k2eAgInSc2d1	každodenní
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
erotické	erotický	k2eAgInPc4d1	erotický
a	a	k8xC	a
pijácké	pijácký	k2eAgInPc4d1	pijácký
motivy	motiv	k1gInPc4	motiv
<g/>
.	.	kIx.	.
</s>
<s>
Keramika	keramika	k1gFnSc1	keramika
tak	tak	k9	tak
nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
nedochované	dochovaný	k2eNgInPc4d1	nedochovaný
doklady	doklad	k1gInPc4	doklad
jiného	jiné	k1gNnSc2	jiné
řeckého	řecký	k2eAgNnSc2d1	řecké
malířství	malířství	k1gNnSc2	malířství
a	a	k8xC	a
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
si	se	k3xPyFc3	se
vytvořit	vytvořit	k5eAaPmF	vytvořit
lepší	dobrý	k2eAgFnSc4d2	lepší
představu	představa	k1gFnSc4	představa
o	o	k7c4	o
tehdejší	tehdejší	k2eAgFnPc4d1	tehdejší
společnosti	společnost	k1gFnPc4	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
řecké	řecký	k2eAgNnSc4d1	řecké
umění	umění	k1gNnSc4	umění
úzce	úzko	k6eAd1	úzko
navazovali	navazovat	k5eAaImAgMnP	navazovat
již	již	k9	již
Římané	Říman	k1gMnPc1	Říman
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
si	se	k3xPyFc3	se
Řeky	řeka	k1gFnPc4	řeka
podmanili	podmanit	k5eAaPmAgMnP	podmanit
vojensky	vojensky	k6eAd1	vojensky
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
období	období	k1gNnSc6	období
středověku	středověk	k1gInSc2	středověk
se	se	k3xPyFc4	se
k	k	k7c3	k
řeckému	řecký	k2eAgNnSc3d1	řecké
umění	umění	k1gNnSc3	umění
programově	programově	k6eAd1	programově
jako	jako	k9	jako
k	k	k7c3	k
ideálu	ideál	k1gInSc3	ideál
vraceli	vracet	k5eAaImAgMnP	vracet
renesanční	renesanční	k2eAgMnPc1d1	renesanční
umělci	umělec	k1gMnPc1	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
bylo	být	k5eAaImAgNnS	být
vyhledáváno	vyhledávat	k5eAaImNgNnS	vyhledávat
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
období	období	k1gNnSc6	období
klasicismu	klasicismus	k1gInSc2	klasicismus
<g/>
.	.	kIx.	.
</s>
