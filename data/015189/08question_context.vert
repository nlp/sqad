<s>
Ballantine	Ballantin	k1gMnSc5
Books	Booksa	k1gFnPc2
Datum	datum	k1gNnSc4
vydání	vydání	k1gNnSc2
</s>
<s>
1953	#num#	k4
Česky	česky	k6eAd1
vydáno	vydat	k5eAaPmNgNnS
</s>
<s>
451	#num#	k4
stupňů	stupeň	k1gInPc2
Fahrenheita	Fahrenheit	k1gMnSc2
</s>
<s>
451	#num#	k4
stupňů	stupeň	k1gInPc2
Fahrenheita	Fahrenheit	k1gMnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
:	:	kIx,
Fahrenheit	Fahrenheit	k1gMnSc1
451	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
jeden	jeden	k4xCgInSc1
z	z	k7c2
nejznámějších	známý	k2eAgInPc2d3
románů	román	k1gInPc2
spisovatele	spisovatel	k1gMnSc2
Raye	Ray	k1gMnSc2
Bradburyho	Bradbury	k1gMnSc2
<g/>
,	,	kIx,
popisuje	popisovat	k5eAaImIp3nS
antiutopickou	antiutopický	k2eAgFnSc4d1
vizi	vize	k1gFnSc4
budoucnosti	budoucnost	k1gFnSc2
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yQgFnSc6,k3yIgFnSc6,k3yRgFnSc6
vítězí	vítězit	k5eAaImIp3nS
technokratická	technokratický	k2eAgFnSc1d1
a	a	k8xC
povrchní	povrchní	k2eAgFnSc1d1
kultura	kultura	k1gFnSc1
masových	masový	k2eAgNnPc2d1
médií	médium	k1gNnPc2
nad	nad	k7c7
přemýšlivou	přemýšlivý	k2eAgFnSc7d1
společností	společnost	k1gFnSc7
uznávající	uznávající	k2eAgFnSc4d1
hodnotu	hodnota	k1gFnSc4
knih	kniha	k1gFnPc2
<g/>
.	.	kIx.
</s>