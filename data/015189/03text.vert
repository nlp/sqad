<s>
451	#num#	k4
stupňů	stupeň	k1gInPc2
Fahrenheita	Fahrenheit	k1gMnSc2
</s>
<s>
451	#num#	k4
stupňů	stupeň	k1gInPc2
FahrenheitaAutor	FahrenheitaAutor	k1gInSc4
</s>
<s>
Ray	Ray	k?
Bradbury	Bradbur	k1gInPc1
Původní	původní	k2eAgInPc1d1
název	název	k1gInSc4
</s>
<s>
Fahrenheit	Fahrenheit	k1gMnSc1
451	#num#	k4
Překladatel	překladatel	k1gMnSc1
</s>
<s>
Jarmila	Jarmila	k1gFnSc1
Emmerová	Emmerová	k1gFnSc1
<g/>
,	,	kIx,
<g/>
Josef	Josef	k1gMnSc1
Škvorecký	Škvorecký	k2eAgMnSc1d1
Země	zem	k1gFnPc4
</s>
<s>
USA	USA	kA
Jazyk	jazyk	k1gInSc1
</s>
<s>
anglický	anglický	k2eAgInSc1d1
Žánr	žánr	k1gInSc1
</s>
<s>
román	román	k1gInSc1
<g/>
,	,	kIx,
science	science	k1gFnSc1
fiction	fiction	k1gInSc1
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Cena	cena	k1gFnSc1
Hugo	Hugo	k1gMnSc1
za	za	k7c4
nejlepší	dobrý	k2eAgInSc4d3
román	román	k1gInSc4
(	(	kIx(
<g/>
1954	#num#	k4
<g/>
)	)	kIx)
<g/>
Prometheus	Prometheus	k1gMnSc1
Award	Award	k1gMnSc1
(	(	kIx(
<g/>
1984	#num#	k4
<g/>
)	)	kIx)
<g/>
NPR	NPR	kA
Top	topit	k5eAaImRp2nS
100	#num#	k4
Science	Scienec	k1gInSc2
Fiction	Fiction	k1gInSc1
and	and	k?
Fantasy	fantas	k1gInPc1
Books	Books	k1gInSc1
Vydavatel	vydavatel	k1gMnSc1
</s>
<s>
Ballantine	Ballantin	k1gMnSc5
Books	Booksa	k1gFnPc2
Datum	datum	k1gNnSc4
vydání	vydání	k1gNnSc2
</s>
<s>
1953	#num#	k4
Česky	česky	k6eAd1
vydáno	vydat	k5eAaPmNgNnS
</s>
<s>
1957	#num#	k4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
451	#num#	k4
stupňů	stupeň	k1gInPc2
Fahrenheita	Fahrenheit	k1gMnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
:	:	kIx,
Fahrenheit	Fahrenheit	k1gMnSc1
451	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
jeden	jeden	k4xCgInSc1
z	z	k7c2
nejznámějších	známý	k2eAgInPc2d3
románů	román	k1gInPc2
spisovatele	spisovatel	k1gMnSc2
Raye	Ray	k1gMnSc2
Bradburyho	Bradbury	k1gMnSc2
<g/>
,	,	kIx,
popisuje	popisovat	k5eAaImIp3nS
antiutopickou	antiutopický	k2eAgFnSc4d1
vizi	vize	k1gFnSc4
budoucnosti	budoucnost	k1gFnSc2
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yQgFnSc6,k3yIgFnSc6,k3yRgFnSc6
vítězí	vítězit	k5eAaImIp3nS
technokratická	technokratický	k2eAgFnSc1d1
a	a	k8xC
povrchní	povrchní	k2eAgFnSc1d1
kultura	kultura	k1gFnSc1
masových	masový	k2eAgNnPc2d1
médií	médium	k1gNnPc2
nad	nad	k7c7
přemýšlivou	přemýšlivý	k2eAgFnSc7d1
společností	společnost	k1gFnSc7
uznávající	uznávající	k2eAgFnSc4d1
hodnotu	hodnota	k1gFnSc4
knih	kniha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidé	člověk	k1gMnPc1
v	v	k7c6
této	tento	k3xDgFnSc6
společnosti	společnost	k1gFnSc6
se	se	k3xPyFc4
již	již	k6eAd1
o	o	k7c4
sebe	sebe	k3xPyFc4
vzájemně	vzájemně	k6eAd1
nezajímají	zajímat	k5eNaImIp3nP
a	a	k8xC
dokonce	dokonce	k9
ztratili	ztratit	k5eAaPmAgMnP
schopnost	schopnost	k1gFnSc4
myslet	myslet	k5eAaImF
<g/>
,	,	kIx,
namísto	namísto	k7c2
toho	ten	k3xDgMnSc2
utíkají	utíkat	k5eAaImIp3nP
do	do	k7c2
svého	svůj	k3xOyFgInSc2
virtuálního	virtuální	k2eAgInSc2d1
světa	svět	k1gInSc2
hedonického	hedonický	k2eAgNnSc2d1
vzrušení	vzrušení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příběh	příběh	k1gInSc1
byl	být	k5eAaImAgInS
jako	jako	k8xS,k8xC
krátká	krátký	k2eAgFnSc1d1
povídka	povídka	k1gFnSc1
otištěn	otištěn	k2eAgInSc1d1
nejprve	nejprve	k6eAd1
v	v	k7c6
časopise	časopis	k1gInSc6
Galaxy	Galax	k1gInPc1
Science	Science	k1gFnSc1
Fiction	Fiction	k1gInSc1
v	v	k7c6
únoru	únor	k1gInSc6
1951	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
češtiny	čeština	k1gFnSc2
román	román	k1gInSc1
přeložili	přeložit	k5eAaPmAgMnP
Jarmila	Jarmila	k1gFnSc1
Emmerová	Emmerová	k1gFnSc1
a	a	k8xC
Josef	Josef	k1gMnSc1
Škvorecký	Škvorecký	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Příběh	příběh	k1gInSc1
</s>
<s>
PřeskočitVarování	PřeskočitVarování	k1gNnSc1
<g/>
:	:	kIx,
Následující	následující	k2eAgFnSc1d1
část	část	k1gFnSc1
článku	článek	k1gInSc2
vyzrazuje	vyzrazovat	k5eAaImIp3nS
zápletku	zápletka	k1gFnSc4
nebo	nebo	k8xC
rozuzlení	rozuzlení	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Děj	děj	k1gInSc1
knihy	kniha	k1gFnSc2
se	se	k3xPyFc4
odehrává	odehrávat	k5eAaImIp3nS
ve	v	k7c6
velice	velice	k6eAd1
hektické	hektický	k2eAgFnSc6d1
budoucnosti	budoucnost	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Doba	doba	k1gFnSc1
je	být	k5eAaImIp3nS
charakterizována	charakterizovat	k5eAaBmNgFnS
neustálým	neustálý	k2eAgInSc7d1
spěchem	spěch	k1gInSc7
<g/>
,	,	kIx,
většinou	většina	k1gFnSc7
za	za	k7c7
zábavou	zábava	k1gFnSc7
<g/>
;	;	kIx,
lidé	člověk	k1gMnPc1
nemají	mít	k5eNaImIp3nP
čas	čas	k1gInSc4
ani	ani	k8xC
chuť	chuť	k1gFnSc4
číst	číst	k5eAaImF
knihy	kniha	k1gFnPc4
a	a	k8xC
starat	starat	k5eAaImF
se	se	k3xPyFc4
o	o	k7c4
něco	něco	k3yInSc4
jiného	jiný	k2eAgNnSc2d1
než	než	k8xS
o	o	k7c4
vlastní	vlastní	k2eAgNnSc4d1
potěšení	potěšení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zájem	zájem	k1gInSc1
o	o	k7c4
literaturu	literatura	k1gFnSc4
postupně	postupně	k6eAd1
upadá	upadat	k5eAaPmIp3nS,k5eAaImIp3nS
a	a	k8xC
knihy	kniha	k1gFnPc1
jsou	být	k5eAaImIp3nP
posléze	posléze	k6eAd1
oficiálně	oficiálně	k6eAd1
zakázány	zakázat	k5eAaPmNgInP
a	a	k8xC
ničeny	ničit	k5eAaImNgInP
<g/>
.	.	kIx.
</s>
<s>
Hlavním	hlavní	k2eAgMnSc7d1
hrdinou	hrdina	k1gMnSc7
je	být	k5eAaImIp3nS
požárník	požárník	k1gMnSc1
Guy	Guy	k1gMnSc1
Montag	Montag	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Požárníci	požárník	k1gMnPc1
však	však	k9
v	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
nejsou	být	k5eNaImIp3nP
těmi	ten	k3xDgMnPc7
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc4
požáry	požár	k1gInPc4
hasí	hasit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Domy	dům	k1gInPc7
se	se	k3xPyFc4
vyrábějí	vyrábět	k5eAaImIp3nP
z	z	k7c2
ohnivzdorných	ohnivzdorný	k2eAgInPc2d1
materiálů	materiál	k1gInPc2
a	a	k8xC
požárníci	požárník	k1gMnPc1
už	už	k6eAd1
nemají	mít	k5eNaImIp3nP
co	co	k9
na	na	k7c4
práci	práce	k1gFnSc4
<g/>
,	,	kIx,
začínají	začínat	k5eAaImIp3nP
dělat	dělat	k5eAaImF
pravý	pravý	k2eAgInSc4d1
opak	opak	k1gInSc4
své	svůj	k3xOyFgFnSc2
původní	původní	k2eAgFnSc2d1
práce	práce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyhledávají	vyhledávat	k5eAaImIp3nP
nelegální	legální	k2eNgMnPc4d1
přechovavatele	přechovavatel	k1gMnPc4
knih	kniha	k1gFnPc2
a	a	k8xC
vypalují	vypalovat	k5eAaImIp3nP
jejich	jejich	k3xOp3gFnPc4
knihovny	knihovna	k1gFnPc4
<g/>
,	,	kIx,
často	často	k6eAd1
i	i	k9
celé	celý	k2eAgInPc1d1
domy	dům	k1gInPc1
<g/>
.	.	kIx.
„	„	k?
<g/>
451	#num#	k4
stupňů	stupeň	k1gInPc2
Fahrenheita	Fahrenheit	k1gMnSc2
(	(	kIx(
<g/>
232,8	232,8	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
teplota	teplota	k1gFnSc1
při	při	k7c6
které	který	k3yIgFnSc6,k3yQgFnSc6,k3yRgFnSc6
se	se	k3xPyFc4
papír	papír	k1gInSc4
vznítí	vznítit	k5eAaPmIp3nS
a	a	k8xC
hoří	hořet	k5eAaImIp3nS
<g/>
“	“	k?
<g/>
,	,	kIx,
píše	psát	k5eAaImIp3nS
Bradbury	Bradbur	k1gInPc4
v	v	k7c6
úvodu	úvod	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
mají	mít	k5eAaImIp3nP
požárníci	požárník	k1gMnPc1
ve	v	k7c6
znaku	znak	k1gInSc6
právě	právě	k9
číslo	číslo	k1gNnSc1
451	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Montag	Montag	k1gInSc1
svou	svůj	k3xOyFgFnSc4
práci	práce	k1gFnSc4
miluje	milovat	k5eAaImIp3nS
<g/>
,	,	kIx,
ale	ale	k8xC
jeho	jeho	k3xOp3gFnSc1
nová	nový	k2eAgFnSc1d1
sousedka	sousedka	k1gFnSc1
Clarissa	Clarissa	k1gFnSc1
McClellanová	McClellanová	k1gFnSc1
<g/>
,	,	kIx,
dívka	dívka	k1gFnSc1
pocházející	pocházející	k2eAgFnSc1d1
z	z	k7c2
rodiny	rodina	k1gFnSc2
podivínů	podivín	k1gMnPc2
(	(	kIx(
<g/>
vzpomínajících	vzpomínající	k2eAgMnPc2d1
na	na	k7c4
minulost	minulost	k1gFnSc4
<g/>
)	)	kIx)
ho	on	k3xPp3gInSc4
svými	svůj	k3xOyFgFnPc7
řečmi	řeč	k1gFnPc7
nahlodá	nahlodat	k5eAaPmIp3nS
a	a	k8xC
Guy	Guy	k1gMnSc1
začne	začít	k5eAaPmIp3nS
přemýšlet	přemýšlet	k5eAaImF
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yRnSc1,k3yInSc1
se	se	k3xPyFc4
v	v	k7c6
knihách	kniha	k1gFnPc6
asi	asi	k9
tak	tak	k6eAd1
píše	psát	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
jedné	jeden	k4xCgFnSc6
akci	akce	k1gFnSc3
Guy	Guy	k1gMnSc1
ukradne	ukradnout	k5eAaPmIp3nS
z	z	k7c2
domu	dům	k1gInSc2
nelegálních	legální	k2eNgMnPc2d1
přechovavatelů	přechovavatel	k1gMnPc2
několik	několik	k4yIc4
knih	kniha	k1gFnPc2
<g/>
,	,	kIx,
vezme	vzít	k5eAaPmIp3nS
je	být	k5eAaImIp3nS
domů	domů	k6eAd1
a	a	k8xC
v	v	k7c6
práci	práce	k1gFnSc6
nahlásí	nahlásit	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
nemocný	nemocný	k2eAgMnSc1d1,k2eNgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velitel	velitel	k1gMnSc1
Beatty	Beatta	k1gFnPc1
si	se	k3xPyFc3
vše	všechen	k3xTgNnSc4
domyslí	domyslet	k5eAaPmIp3nS
a	a	k8xC
přijde	přijít	k5eAaPmIp3nS
k	k	k7c3
Montagovi	Montag	k1gMnSc3
domů	domů	k6eAd1
a	a	k8xC
vypoví	vypovědět	k5eAaPmIp3nS
mu	on	k3xPp3gMnSc3
minulost	minulost	k1gFnSc1
hasičů	hasič	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
řekne	říct	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
když	když	k8xS
hasič	hasič	k1gMnSc1
neodolá	odolat	k5eNaPmIp3nS
pokušení	pokušení	k1gNnSc4
a	a	k8xC
vezme	vzít	k5eAaPmIp3nS
si	se	k3xPyFc3
knihu	kniha	k1gFnSc4
<g/>
,	,	kIx,
nic	nic	k3yNnSc1
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
nestane	stanout	k5eNaPmIp3nS
<g/>
,	,	kIx,
pokud	pokud	k8xS
ji	on	k3xPp3gFnSc4
do	do	k7c2
jednoho	jeden	k4xCgInSc2
dne	den	k1gInSc2
spálí	spálit	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Guy	Guy	k1gMnSc1
ho	on	k3xPp3gNnSc4
sice	sice	k8xC
poslechne	poslechnout	k5eAaPmIp3nS
<g/>
,	,	kIx,
ale	ale	k8xC
nechá	nechat	k5eAaPmIp3nS
spálit	spálit	k5eAaPmF
pouze	pouze	k6eAd1
jedinou	jediný	k2eAgFnSc4d1
knihu	kniha	k1gFnSc4
na	na	k7c4
radu	rada	k1gMnSc4
profesora	profesor	k1gMnSc4
Fabera	Faber	k1gMnSc4
<g/>
,	,	kIx,
kterého	který	k3yIgMnSc4,k3yRgMnSc4,k3yQgMnSc4
kdysi	kdysi	k6eAd1
potkal	potkat	k5eAaPmAgMnS
v	v	k7c6
parku	park	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Beatty	Beatto	k1gNnPc7
se	se	k3xPyFc4
však	však	k9
doví	dovědět	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
Guyovi	Guya	k1gMnSc3
doma	doma	k6eAd1
ještě	ještě	k6eAd1
zbývají	zbývat	k5eAaImIp3nP
knihy	kniha	k1gFnPc1
<g/>
,	,	kIx,
a	a	k8xC
na	na	k7c6
dalším	další	k2eAgInSc6d1
výjezdu	výjezd	k1gInSc6
požárníků	požárník	k1gMnPc2
zastaví	zastavit	k5eAaPmIp3nS
před	před	k7c7
Montagovým	Montagův	k2eAgInSc7d1
domem	dům	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Hrdinu	Hrdina	k1gMnSc4
udala	udat	k5eAaPmAgFnS
jeho	jeho	k3xOp3gNnSc4
žena	žena	k1gFnSc1
Mildred	Mildred	k1gInSc1
a	a	k8xC
její	její	k3xOp3gFnSc1
přítelkyně	přítelkyně	k1gFnSc1
<g/>
,	,	kIx,
jimž	jenž	k3xRgInPc3
předtím	předtím	k6eAd1
četl	číst	k5eAaImAgMnS
básničky	básnička	k1gFnSc2
právě	právě	k6eAd1
z	z	k7c2
ukradené	ukradený	k2eAgFnSc2d1
knihy	kniha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Guy	Guy	k1gMnSc1
je	být	k5eAaImIp3nS
přinucen	přinutit	k5eAaPmNgMnS
zapálit	zapálit	k5eAaPmF
svůj	svůj	k3xOyFgInSc4
dům	dům	k1gInSc4
a	a	k8xC
všechno	všechen	k3xTgNnSc1
v	v	k7c6
něm.	něm.	k?
Avšak	avšak	k8xC
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yRnSc1,k3yInSc1
Beatty	Beatt	k1gInPc4
přijde	přijít	k5eAaPmIp3nS
na	na	k7c4
jeho	jeho	k3xOp3gNnSc4
tajné	tajný	k2eAgNnSc4d1
spojení	spojení	k1gNnSc4
s	s	k7c7
Faberem	Faber	k1gInSc7
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
Guy	Guy	k1gMnSc1
nucen	nutit	k5eAaImNgMnS
zabít	zabít	k5eAaPmF
svého	svůj	k3xOyFgMnSc4
šéfa	šéf	k1gMnSc4
a	a	k8xC
dát	dát	k5eAaPmF
se	se	k3xPyFc4
na	na	k7c4
útěk	útěk	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Policie	policie	k1gFnSc1
okamžitě	okamžitě	k6eAd1
vysílá	vysílat	k5eAaImIp3nS
mechanického	mechanický	k2eAgMnSc4d1
Ohaře	ohař	k1gMnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
Montaga	Montaga	k1gFnSc1
vystopoval	vystopovat	k5eAaPmAgMnS
<g/>
,	,	kIx,
ale	ale	k8xC
Guy	Guy	k1gMnSc1
se	se	k3xPyFc4
převléká	převlékat	k5eAaImIp3nS
do	do	k7c2
Faberových	Faberův	k2eAgInPc2d1
šatů	šat	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
navíc	navíc	k6eAd1
polije	polít	k5eAaPmIp3nS
alkoholem	alkohol	k1gInSc7
<g/>
,	,	kIx,
a	a	k8xC
daří	dařit	k5eAaImIp3nS
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
Ohařovi	ohař	k1gMnSc3
utéct	utéct	k5eAaPmF
korytem	koryto	k1gNnSc7
řeky	řeka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celá	celý	k2eAgFnSc1d1
honička	honička	k1gFnSc1
ovšem	ovšem	k9
běží	běžet	k5eAaImIp3nS
naživo	naživo	k1gNnSc4
v	v	k7c6
televizi	televize	k1gFnSc6
a	a	k8xC
policie	policie	k1gFnSc1
si	se	k3xPyFc3
nemůže	moct	k5eNaImIp3nS
dovolit	dovolit	k5eAaPmF
neuspět	uspět	k5eNaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pošle	pošle	k6eAd1
tedy	tedy	k8xC
Ohaře	ohař	k1gMnPc4
k	k	k7c3
místu	místo	k1gNnSc3
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
každý	každý	k3xTgInSc1
den	den	k1gInSc1
prochází	procházet	k5eAaImIp3nS
nevinný	vinný	k2eNgMnSc1d1
muž	muž	k1gMnSc1
<g/>
,	,	kIx,
kterého	který	k3yQgMnSc4,k3yIgMnSc4,k3yRgMnSc4
nechá	nechat	k5eAaPmIp3nS
zabít	zabít	k5eAaPmF
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
diváci	divák	k1gMnPc1
nepřišli	přijít	k5eNaPmAgMnP
o	o	k7c4
svou	svůj	k3xOyFgFnSc4
podívanou	podívaná	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Guy	Guy	k?
se	se	k3xPyFc4
mezitím	mezitím	k6eAd1
dostává	dostávat	k5eAaImIp3nS
do	do	k7c2
lesa	les	k1gInSc2
ke	k	k7c3
skupině	skupina	k1gFnSc3
bývalých	bývalý	k2eAgMnPc2d1
vysokoškolských	vysokoškolský	k2eAgMnPc2d1
profesorů	profesor	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
putují	putovat	k5eAaImIp3nP
z	z	k7c2
místa	místo	k1gNnSc2
na	na	k7c4
místo	místo	k1gNnSc4
a	a	k8xC
pamatují	pamatovat	k5eAaImIp3nP
si	se	k3xPyFc3
vědomosti	vědomost	k1gFnPc4
z	z	k7c2
knih	kniha	k1gFnPc2
(	(	kIx(
<g/>
každý	každý	k3xTgMnSc1
si	se	k3xPyFc3
pamatuje	pamatovat	k5eAaImIp3nS
nějakou	nějaký	k3yIgFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Montag	Montag	k1gInSc1
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
sám	sám	k3xTgMnSc1
nějaké	nějaký	k3yIgFnSc2
knihy	kniha	k1gFnSc2
četl	číst	k5eAaImAgMnS
<g/>
,	,	kIx,
se	se	k3xPyFc4
k	k	k7c3
nim	on	k3xPp3gFnPc3
přidává	přidávat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
nějaké	nějaký	k3yIgFnSc6
době	doba	k1gFnSc6
propukne	propuknout	k5eAaPmIp3nS
válečný	válečný	k2eAgInSc4d1
konflikt	konflikt	k1gInSc4
a	a	k8xC
město	město	k1gNnSc4
plné	plný	k2eAgFnSc2d1
„	„	k?
<g/>
benzínových	benzínový	k2eAgMnPc2d1
štvanců	štvanec	k1gMnPc2
<g/>
“	“	k?
a	a	k8xC
věčně	věčně	k6eAd1
se	se	k3xPyFc4
bavících	bavící	k2eAgInPc2d1
lidí	člověk	k1gMnPc2
je	být	k5eAaImIp3nS
během	během	k7c2
několika	několik	k4yIc2
minut	minuta	k1gFnPc2
zničeno	zničen	k2eAgNnSc1d1
bombardováním	bombardování	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzdělanci	vzdělanec	k1gMnPc1
se	se	k3xPyFc4
vydávají	vydávat	k5eAaPmIp3nP,k5eAaImIp3nP
městu	město	k1gNnSc3
na	na	k7c4
pomoc	pomoc	k1gFnSc4
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
co	co	k3yQnSc4,k3yInSc4,k3yRnSc4
mají	mít	k5eAaImIp3nP
–	–	k?
vědomostmi	vědomost	k1gFnPc7
získanými	získaný	k2eAgFnPc7d1
z	z	k7c2
knih	kniha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Konec	konec	k1gInSc1
části	část	k1gFnSc2
článku	článek	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
vyzrazuje	vyzrazovat	k5eAaImIp3nS
zápletku	zápletka	k1gFnSc4
nebo	nebo	k8xC
rozuzlení	rozuzlení	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Filmová	filmový	k2eAgFnSc1d1
adaptace	adaptace	k1gFnSc1
</s>
<s>
1966	#num#	k4
<g/>
:	:	kIx,
451	#num#	k4
<g/>
°	°	k?
Fahrenheita	Fahrenheit	k1gMnSc2
<g/>
,	,	kIx,
britský	britský	k2eAgInSc1d1
film	film	k1gInSc1
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
François	François	k1gFnSc1
Truffaut	Truffaut	k1gInSc4
<g/>
,	,	kIx,
v	v	k7c6
hlavních	hlavní	k2eAgFnPc6d1
rolích	role	k1gFnPc6
Oskar	Oskar	k1gMnSc1
Werner	Werner	k1gMnSc1
<g/>
,	,	kIx,
Julie	Julie	k1gFnSc1
Christie	Christie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
2018	#num#	k4
<g/>
:	:	kIx,
451	#num#	k4
stupňů	stupeň	k1gInPc2
Fahrenheita	Fahrenheit	k1gMnSc2
<g/>
,	,	kIx,
americký	americký	k2eAgInSc1d1
film	film	k1gInSc1
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Ramin	Ramin	k1gInSc1
Bahrani	Bahraň	k1gFnSc3
<g/>
,	,	kIx,
v	v	k7c6
hlavních	hlavní	k2eAgFnPc6d1
rolích	role	k1gFnPc6
Michael	Michael	k1gMnSc1
B.	B.	kA
Jordan	Jordan	k1gMnSc1
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
Shannon	Shannon	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Česká	český	k2eAgNnPc1d1
vydání	vydání	k1gNnPc1
</s>
<s>
451	#num#	k4
stupňů	stupeň	k1gInPc2
Fahrenheita	Fahrenheit	k1gMnSc2
<g/>
,	,	kIx,
Melantrich	Melantrich	k1gInSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1957	#num#	k4
<g/>
,	,	kIx,
přeložili	přeložit	k5eAaPmAgMnP
Jarmila	Jarmila	k1gFnSc1
Emmerová	Emmerová	k1gFnSc1
a	a	k8xC
Josef	Josef	k1gMnSc1
Škvorecký	Škvorecký	k2eAgMnSc1d1
<g/>
,	,	kIx,
znovu	znovu	k6eAd1
Svoboda	svoboda	k1gFnSc1
(	(	kIx(
<g/>
edice	edice	k1gFnSc1
Omnia	omnium	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1970	#num#	k4
<g/>
,	,	kIx,
Baronet	baronet	k1gMnSc1
a	a	k8xC
Knižní	knižní	k2eAgInSc1d1
klub	klub	k1gInSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2001	#num#	k4
a	a	k8xC
2009	#num#	k4
a	a	k8xC
Plus	plus	k1gInSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Marťanská	marťanský	k2eAgFnSc1d1
kronika	kronika	k1gFnSc1
<g/>
,	,	kIx,
451	#num#	k4
stupňů	stupeň	k1gInPc2
Fahrenheita	Fahrenheit	k1gMnSc2
<g/>
,	,	kIx,
Odeon	odeon	k1gInSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1978	#num#	k4
<g/>
,	,	kIx,
přeložili	přeložit	k5eAaPmAgMnP
Jarmila	Jarmila	k1gFnSc1
Emmerová	Emmerová	k1gFnSc1
a	a	k8xC
Josef	Josef	k1gMnSc1
Škvorecký	Škvorecký	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Rozhlasové	rozhlasový	k2eAgFnPc1d1
adaptace	adaptace	k1gFnPc1
</s>
<s>
2016	#num#	k4
Ray	Ray	k1gFnSc1
Bradbury	Bradbura	k1gFnSc2
<g/>
:	:	kIx,
451	#num#	k4
stupňů	stupeň	k1gInPc2
Fahrenheita	Fahrenheit	k1gMnSc2
<g/>
,	,	kIx,
překlad	překlad	k1gInSc1
<g/>
:	:	kIx,
Jarmila	Jarmila	k1gFnSc1
Emmerová	Emmerová	k1gFnSc1
a	a	k8xC
Josef	Josef	k1gMnSc1
Škvorecký	Škvorecký	k2eAgMnSc1d1
<g/>
,	,	kIx,
dramatizace	dramatizace	k1gFnSc1
Renata	Renata	k1gFnSc1
Venclová	Venclová	k1gFnSc1
a	a	k8xC
Hana	Hana	k1gFnSc1
Roguljič	Roguljič	k1gInSc1
<g/>
,	,	kIx,
hudba	hudba	k1gFnSc1
Filip	Filip	k1gMnSc1
Skuhrovec	Skuhrovec	k1gMnSc1
<g/>
,	,	kIx,
dramaturgie	dramaturgie	k1gFnSc1
Renata	Renata	k1gFnSc1
Venclová	Venclová	k1gFnSc1
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Lukáš	Lukáš	k1gMnSc1
Hlavica	Hlavica	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Účinkují	účinkovat	k5eAaImIp3nP
<g/>
:	:	kIx,
Petr	Petr	k1gMnSc1
Lněnička	Lněnička	k1gFnSc1
<g/>
,	,	kIx,
Dana	Dana	k1gFnSc1
Černá	Černá	k1gFnSc1
<g/>
,	,	kIx,
Tereza	Tereza	k1gFnSc1
Terberová	Terberová	k1gFnSc1
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
Stránský	Stránský	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
Němec	Němec	k1gMnSc1
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
Zahálka	Zahálka	k1gMnSc1
<g/>
,	,	kIx,
Marek	Marek	k1gMnSc1
Holý	Holý	k1gMnSc1
<g/>
,	,	kIx,
Libuše	Libuše	k1gFnSc1
Švormová	Švormová	k1gFnSc1
<g/>
,	,	kIx,
Lada	Lada	k1gFnSc1
Jelínková	Jelínková	k1gFnSc1
<g/>
,	,	kIx,
Klára	Klára	k1gFnSc1
Sedláčková-Oltová	Sedláčková-Oltová	k1gFnSc1
<g/>
,	,	kIx,
Miroslav	Miroslav	k1gMnSc1
Hanuš	Hanuš	k1gMnSc1
<g/>
,	,	kIx,
Jaromír	Jaromír	k1gMnSc1
Meduna	Meduna	k1gFnSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
Velen	velen	k2eAgMnSc1d1
<g/>
,	,	kIx,
Stanislava	Stanislava	k1gFnSc1
Jachnická	Jachnický	k2eAgFnSc1d1
<g/>
,	,	kIx,
Kamila	Kamila	k1gFnSc1
Špráchalová	Špráchalová	k1gFnSc1
<g/>
,	,	kIx,
Svatopluk	Svatopluk	k1gMnSc1
Schuller	schuller	k1gInSc1
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
Hruška	Hruška	k1gMnSc1
<g/>
,	,	kIx,
Libor	Libor	k1gMnSc1
Vacek	Vacek	k1gMnSc1
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
Vacek	Vacek	k1gMnSc1
<g/>
,	,	kIx,
Vasil	Vasil	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
<g/>
,	,	kIx,
Naděžda	Naděžda	k1gFnSc1
Fořtová	Fořtová	k1gFnSc1
<g/>
,	,	kIx,
Radovan	Radovan	k1gMnSc1
Klučka	Klučka	k1gMnSc1
<g/>
,	,	kIx,
Lukáš	Lukáš	k1gMnSc1
Hlavica	Hlavica	k1gMnSc1
<g/>
,	,	kIx,
Naděžda	Naděžda	k1gFnSc1
Mečířová	Mečířová	k1gFnSc1
<g/>
,	,	kIx,
Mikuláš	Mikuláš	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Převrátil	převrátit	k5eAaPmAgMnS
<g/>
,	,	kIx,
Aleš	Aleš	k1gMnSc1
Vrzák	Vrzák	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
Litoš	Litoš	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
Šplíchal	Šplíchal	k1gMnSc1
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
Pergl	Pergl	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
Plechatý	plechatý	k2eAgMnSc1d1
<g/>
,	,	kIx,
Radka	Radka	k1gFnSc1
Tučková	Tučková	k1gFnSc1
<g/>
,	,	kIx,
Jana	Jana	k1gFnSc1
Knappová	Knappová	k1gFnSc1
<g/>
,	,	kIx,
Dana	Dana	k1gFnSc1
Reichová	Reichová	k1gFnSc1
<g/>
,	,	kIx,
Jaroslava	Jaroslava	k1gFnSc1
Červená	Červená	k1gFnSc1
<g/>
,	,	kIx,
Anna	Anna	k1gFnSc1
Balcarová	Balcarová	k1gFnSc1
a	a	k8xC
Jana	Jana	k1gFnSc1
Odvárková	Odvárková	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Inscenace	inscenace	k1gFnSc1
získala	získat	k5eAaPmAgFnS
2	#num#	k4
<g/>
.	.	kIx.
cenu	cena	k1gFnSc4
na	na	k7c6
festivalu	festival	k1gInSc6
Prix	Prix	k1gInSc1
Bohemia	bohemia	k1gFnSc1
Radio	radio	k1gNnSc1
2017	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
VENCLOVÁ	Venclová	k1gFnSc1
<g/>
,	,	kIx,
Renata	Renata	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ray	Ray	k1gFnSc1
Bradbury	Bradbura	k1gFnSc2
<g/>
:	:	kIx,
451	#num#	k4
stupňů	stupeň	k1gInPc2
Fahrenheita	Fahrenheit	k1gMnSc2
<g/>
,	,	kIx,
teplota	teplota	k1gFnSc1
<g/>
,	,	kIx,
při	při	k7c6
které	který	k3yRgFnSc6,k3yIgFnSc6,k3yQgFnSc6
hoří	hořet	k5eAaImIp3nS
papír	papír	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
KACHLÍK	Kachlík	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
výsledcích	výsledek	k1gInPc6
mezinárodního	mezinárodní	k2eAgInSc2d1
rozhlasového	rozhlasový	k2eAgInSc2d1
festivalu	festival	k1gInSc2
Prix	Prix	k1gInSc1
Bohemia	bohemia	k1gFnSc1
Radio	radio	k1gNnSc4
je	být	k5eAaImIp3nS
rozhodnuto	rozhodnut	k2eAgNnSc1d1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
<g/>
,	,	kIx,
2017-03-23	2017-03-23	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
HTTP	HTTP	kA
451	#num#	k4
(	(	kIx(
<g/>
chybový	chybový	k2eAgInSc4d1
kód	kód	k1gInSc4
pojmenovaný	pojmenovaný	k2eAgInSc4d1
dle	dle	k7c2
románu	román	k1gInSc2
"	"	kIx"
<g/>
451	#num#	k4
stupňů	stupeň	k1gInPc2
Fahrenheita	Fahrenheit	k1gMnSc2
<g/>
"	"	kIx"
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
451	#num#	k4
stupňů	stupeň	k1gInPc2
Fahrenheita	Fahrenheit	k1gMnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Fahrenheit	Fahrenheit	k1gMnSc1
451	#num#	k4
na	na	k7c6
webu	web	k1gInSc6
Fantastic	Fantastice	k1gFnPc2
Fiction	Fiction	k1gInSc4
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Fahrenheit	Fahrenheit	k1gMnSc1
451	#num#	k4
na	na	k7c6
webu	web	k1gInSc6
IMDb	IMDba	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
451	#num#	k4
<g/>
°	°	k?
Fahrenheita	Fahrenheit	k1gMnSc4
na	na	k7c6
webu	web	k1gInSc6
CSFD	CSFD	kA
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Ray	Ray	k1gFnSc1
Bradbury	Bradbura	k1gFnSc2
-	-	kIx~
451	#num#	k4
stupňů	stupeň	k1gInPc2
Fahrenheita	Fahrenheit	k1gMnSc2
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
451	#num#	k4
stupňů	stupeň	k1gInPc2
Fahrenheita	Fahrenheit	k1gMnSc2
na	na	k7c6
webu	web	k1gInSc6
Databazeknih	Databazekniha	k1gFnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
451	#num#	k4
stupňů	stupeň	k1gInPc2
Fahrenheita	Fahrenheit	k1gMnSc2
na	na	k7c6
webu	web	k1gInSc6
LEGIE	legie	k1gFnSc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4322998-0	4322998-0	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
75711	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
185473935	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
75711	#num#	k4
</s>
