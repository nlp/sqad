<s>
Ted	Ted	k1gMnSc1
Drury	Drura	k1gFnSc2
</s>
<s>
Ted	Ted	k1gMnSc1
DruryOsobní	DruryOsobní	k2eAgFnSc2d1
informace	informace	k1gFnSc2
Datum	datum	k1gNnSc1
narození	narození	k1gNnSc2
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1971	#num#	k4
(	(	kIx(
<g/>
49	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Místo	místo	k7c2
narození	narození	k1gNnSc2
</s>
<s>
Boston	Boston	k1gInSc1
<g/>
,	,	kIx,
Massachusetts	Massachusetts	k1gNnSc1
<g/>
,	,	kIx,
USA	USA	kA
Výška	výška	k1gFnSc1
</s>
<s>
183	#num#	k4
cm	cm	kA
Váha	váha	k1gFnSc1
</s>
<s>
86	#num#	k4
kg	kg	kA
Rodiče	rodič	k1gMnPc1
</s>
<s>
Herbert	Herbert	k1gMnSc1
Drury	Drura	k1gFnSc2
Příbuzní	příbuzný	k1gMnPc1
</s>
<s>
Chris	Chris	k1gFnSc1
Drury	Drura	k1gFnSc2
(	(	kIx(
<g/>
sourozenec	sourozenec	k1gMnSc1
<g/>
)	)	kIx)
Klubové	klubový	k2eAgFnSc2d1
informace	informace	k1gFnSc2
Současný	současný	k2eAgInSc1d1
klub	klub	k1gInSc1
</s>
<s>
ukončil	ukončit	k5eAaPmAgInS
aktivní	aktivní	k2eAgFnSc4d1
kariéru	kariéra	k1gFnSc4
Pozice	pozice	k1gFnSc2
</s>
<s>
střední	střední	k2eAgMnSc1d1
útočník	útočník	k1gMnSc1
Předchozí	předchozí	k2eAgInPc4d1
kluby	klub	k1gInPc4
</s>
<s>
High-CTFairfield	High-CTFairfield	k1gMnSc1
Prep	Prep	k1gMnSc1
Jesuits	Jesuitsa	k1gFnPc2
ECACHarvard	ECACHarvard	k1gMnSc1
Crimson	Crimson	k1gMnSc1
NHLCalgary	NHLCalgara	k1gFnSc2
Flames	Flames	k1gMnSc1
Hartford	Hartford	k1gMnSc1
Whalers	Whalers	k1gInSc4
Ottawa	Ottawa	k1gFnSc1
Senators	Senatorsa	k1gFnPc2
Mighty	Mighta	k1gFnSc2
Ducks	Ducks	k1gInSc1
of	of	k?
Anaheim	Anaheim	k1gInSc4
New	New	k1gMnPc2
York	York	k1gInSc1
Islanders	Islandersa	k1gFnPc2
Columbus	Columbus	k1gInSc1
Blue	Blu	k1gInSc2
Jackets	Jacketsa	k1gFnPc2
AHL	AHL	kA
Springfield	Springfield	k1gInSc1
Indians	Indians	k1gInSc1
Albany	Albana	k1gFnSc2
River	River	k1gMnSc1
Rats	Ratsa	k1gFnPc2
Lowell	Lowell	k1gMnSc1
Lock	Lock	k1gMnSc1
Monsters	Monstersa	k1gFnPc2
IHL	IHL	kA
Chicago	Chicago	k1gNnSc4
Wolves	Wolves	k1gInSc1
DEL	DEL	kA
Hamburg	Hamburg	k1gInSc1
Freezers	Freezers	k1gInSc1
Kassel	Kassel	k1gInSc1
Huskies	Huskiesa	k1gFnPc2
Krefeld	Krefelda	k1gFnPc2
Pinguine	Pinguin	k1gInSc5
Draft	draft	k1gInSc1
NHL	NHL	kA
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
jako	jako	k8xC,k8xS
42	#num#	k4
<g/>
.	.	kIx.
celkovětýmem	celkovětým	k1gInSc7
Calgary	Calgary	k1gNnSc2
Flames	Flamesa	k1gFnPc2
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybět	k5eAaImIp3nS,k5eAaPmIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Ted	Ted	k1gMnSc1
Evans	Evansa	k1gFnPc2
Drury	Drura	k1gFnSc2
(	(	kIx(
<g/>
*	*	kIx~
13	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1971	#num#	k4
<g/>
,	,	kIx,
Boston	Boston	k1gInSc1
<g/>
,	,	kIx,
Massachusetts	Massachusetts	k1gNnSc1
<g/>
,	,	kIx,
USA	USA	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
bývalý	bývalý	k2eAgMnSc1d1
americký	americký	k2eAgMnSc1d1
hokejový	hokejový	k2eAgMnSc1d1
útočník	útočník	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
ukončil	ukončit	k5eAaPmAgMnS
svoji	svůj	k3xOyFgFnSc4
hráčskou	hráčský	k2eAgFnSc4d1
kariéru	kariéra	k1gFnSc4
v	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Profil	profil	k1gInSc1
</s>
<s>
Ted	Ted	k1gMnSc1
Drury	Drura	k1gFnSc2
je	být	k5eAaImIp3nS
starší	starý	k2eAgMnSc1d2
bratr	bratr	k1gMnSc1
slavnějšího	slavný	k2eAgInSc2d2
Chrise	Chrise	k1gFnPc1
<g/>
,	,	kIx,
se	s	k7c7
kterým	který	k3yRgMnSc7,k3yQgMnSc7,k3yIgMnSc7
v	v	k7c6
mládí	mládí	k1gNnSc6
hrával	hrávat	k5eAaImAgMnS
za	za	k7c4
tým	tým	k1gInSc4
Fairfield	Fairfield	k1gMnSc1
Prep	Prep	k1gMnSc1
Jesuits	Jesuits	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
odešel	odejít	k5eAaPmAgMnS
na	na	k7c4
Harvardovu	Harvardův	k2eAgFnSc4d1
univerzitu	univerzita	k1gFnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
hrál	hrát	k5eAaImAgMnS
za	za	k7c4
Harvard	Harvard	k1gInSc4
Crimson	Crimsona	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
dařilo	dařit	k5eAaImAgNnS
obzvláště	obzvláště	k6eAd1
v	v	k7c6
sezoně	sezona	k1gFnSc6
1992-93	1992-93	k4
což	což	k3yQnSc4,k3yRnSc4
dokazuje	dokazovat	k5eAaImIp3nS
jeho	jeho	k3xOp3gFnSc2
nominace	nominace	k1gFnSc2
do	do	k7c2
ECAC	ECAC	kA
1	#num#	k4
<g/>
.	.	kIx.
<g/>
All-star	All-star	k1gInSc1
týmu	tým	k1gInSc2
a	a	k8xC
NCAA	NCAA	kA
1	#num#	k4
<g/>
.	.	kIx.
<g/>
All-star	All-star	k1gInSc1
týmu	tým	k1gInSc2
a	a	k8xC
obdržení	obdržení	k1gNnSc6
ceny	cena	k1gFnPc4
pro	pro	k7c4
Hráče	hráč	k1gMnPc4
roku	rok	k1gInSc2
ECAC	ECAC	kA
<g/>
.	.	kIx.
</s>
<s>
Do	do	k7c2
NHL	NHL	kA
ho	on	k3xPp3gNnSc4
draftoval	draftovat	k5eAaImAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
tým	tým	k1gInSc1
Calgary	Calgary	k1gNnSc2
Flames	Flamesa	k1gFnPc2
ve	v	k7c6
2	#num#	k4
<g/>
.	.	kIx.
kole	kolo	k1gNnSc6
(	(	kIx(
<g/>
42	#num#	k4
<g/>
.	.	kIx.
<g/>
celkově	celkově	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ted	Ted	k1gMnSc1
Drury	Drura	k1gMnSc2
se	se	k3xPyFc4
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c6
NHL	NHL	kA
výrazněji	výrazně	k6eAd2
neprosadil	prosadit	k5eNaPmAgMnS
a	a	k8xC
také	také	k9
proto	proto	k8xC
často	často	k6eAd1
měnil	měnit	k5eAaImAgMnS
svá	svůj	k3xOyFgNnPc4
působiště	působiště	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Často	často	k6eAd1
<g/>
,	,	kIx,
také	také	k9
cestoval	cestovat	k5eAaImAgMnS
mezi	mezi	k7c7
NHL	NHL	kA
a	a	k8xC
nižšími	nízký	k2eAgFnPc7d2
ligami	liga	k1gFnPc7
AHL	AHL	kA
a	a	k8xC
IHL	IHL	kA
<g/>
.	.	kIx.
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1994	#num#	k4
byl	být	k5eAaImAgInS
vyměněn	vyměnit	k5eAaPmNgInS
z	z	k7c2
Calgary	Calgary	k1gNnSc2
Flames	Flamesa	k1gFnPc2
do	do	k7c2
Hartfordu	Hartford	k1gInSc2
Whalers	Whalersa	k1gFnPc2
<g/>
,	,	kIx,
společně	společně	k6eAd1
s	s	k7c7
Garym	Garym	k1gInSc1
Suterem	Suter	k1gInSc7
a	a	k8xC
Paulem	Paul	k1gMnSc7
Ranheimem	Ranheim	k1gMnSc7
<g/>
,	,	kIx,
za	za	k7c4
Jamese	Jamese	k1gFnPc4
Patricka	Patricko	k1gNnSc2
<g/>
,	,	kIx,
Zarleye	Zarley	k1gMnSc2
Zalapskiho	Zalapski	k1gMnSc2
a	a	k8xC
Michaela	Michael	k1gMnSc2
Nylandera	Nylander	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1995	#num#	k4
byl	být	k5eAaImAgInS
ve	v	k7c4
Waiver	Waiver	k1gInSc4
draftu	draft	k1gInSc2
<g/>
,	,	kIx,
draftován	draftován	k2eAgInSc1d1
Ottawou	Ottawa	k1gFnSc7
Senators	Senatorsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1996	#num#	k4
byl	být	k5eAaImAgInS
vyměněn	vyměnit	k5eAaPmNgInS
společně	společně	k6eAd1
s	s	k7c7
Marcem	Marce	k1gMnSc7
Moroem	Moro	k1gMnSc7
do	do	k7c2
Mighty	Mighta	k1gFnSc2
Ducks	Ducks	k1gInSc1
of	of	k?
Anaheim	Anaheim	k1gInSc1
<g/>
,	,	kIx,
za	za	k7c2
Jasona	Jason	k1gMnSc2
Yorka	Yorka	k1gFnSc1
a	a	k8xC
Shauna	Shauna	k1gFnSc1
Van	vana	k1gFnPc2
Allena	Allen	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
29	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1999	#num#	k4
byl	být	k5eAaImAgInS
vyměněn	vyměnit	k5eAaPmNgInS
do	do	k7c2
New	New	k1gMnPc2
Yorku	York	k1gInSc2
Islanders	Islandersa	k1gFnPc2
<g/>
,	,	kIx,
za	za	k7c4
Tonyho	Tony	k1gMnSc4
Hrkace	Hrkace	k1gFnSc2
a	a	k8xC
Deana	Dean	k1gMnSc4
Malkoce	Malkoce	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
23	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2000	#num#	k4
byl	být	k5eAaImAgInS
vybrán	vybrat	k5eAaPmNgMnS
v	v	k7c6
Rozšiřovacím	rozšiřovací	k2eAgInSc6d1
draftu	draft	k1gInSc6
Columbusem	Columbus	k1gInSc7
Blue	Blu	k1gInSc2
Jackets	Jacketsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2001	#num#	k4
jako	jako	k8xC,k8xS
Volný	volný	k2eAgMnSc1d1
agent	agent	k1gMnSc1
podepsal	podepsat	k5eAaPmAgMnS
smlouvu	smlouva	k1gFnSc4
s	s	k7c7
New	New	k1gFnSc7
Jersey	Jersea	k1gFnSc2
Devils	Devilsa	k1gFnPc2
<g/>
,	,	kIx,
za	za	k7c4
které	který	k3yRgNnSc4,k3yQgNnSc4,k3yIgNnSc4
ovšem	ovšem	k9
nenastoupil	nastoupit	k5eNaPmAgMnS
v	v	k7c6
jediném	jediný	k2eAgInSc6d1
zápase	zápas	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2002	#num#	k4
byl	být	k5eAaImAgInS
vyměněn	vyměnit	k5eAaPmNgInS
do	do	k7c2
Caroliny	Carolina	k1gFnSc2
Hurricanes	Hurricanes	k1gInSc1
za	za	k7c4
Mikea	Mike	k2eAgMnSc4d1
Rucinskiho	Rucinski	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
posledních	poslední	k2eAgInPc6d1
dvou	dva	k4xCgInPc6
zmíněných	zmíněný	k2eAgInPc6d1
týmech	tým	k1gInPc6
neodehrál	odehrát	k5eNaPmAgInS
ani	ani	k8xC
zápas	zápas	k1gInSc1
a	a	k8xC
v	v	k7c6
posledních	poslední	k2eAgFnPc6d1
dvou	dva	k4xCgFnPc6
sezónách	sezóna	k1gFnPc6
hrál	hrát	k5eAaImAgInS
pouze	pouze	k6eAd1
na	na	k7c6
farmách	farma	k1gFnPc6
v	v	k7c6
AHL	AHL	kA
a	a	k8xC
IHL	IHL	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
se	se	k3xPyFc4
rozhodl	rozhodnout	k5eAaPmAgInS
odejít	odejít	k5eAaPmF
do	do	k7c2
Německa	Německo	k1gNnSc2
kde	kde	k6eAd1
hrál	hrát	k5eAaImAgInS
DEL	DEL	kA
v	v	k7c6
klubech	klub	k1gInPc6
Hamburg	Hamburg	k1gInSc4
Freezers	Freezersa	k1gFnPc2
<g/>
,	,	kIx,
Kassel	Kassel	k1gInSc4
Huskies	Huskiesa	k1gFnPc2
a	a	k8xC
jeho	jeho	k3xOp3gInSc7
posledním	poslední	k2eAgInSc7d1
týmem	tým	k1gInSc7
byl	být	k5eAaImAgInS
Krefeld	Krefeld	k1gInSc1
Pinguine	Pinguin	k1gInSc5
a	a	k8xC
po	po	k7c6
sezóně	sezóna	k1gFnSc6
2006-07	2006-07	k4
ukončil	ukončit	k5eAaPmAgMnS
kariéru	kariéra	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Byl	být	k5eAaImAgMnS
velký	velký	k2eAgMnSc1d1
vlastenec	vlastenec	k1gMnSc1
a	a	k8xC
za	za	k7c4
USA	USA	kA
hrál	hrát	k5eAaImAgMnS
na	na	k7c6
dvou	dva	k4xCgFnPc6
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
<g/>
,	,	kIx,
na	na	k7c6
třech	tři	k4xCgInPc6
světových	světový	k2eAgInPc6d1
šampionátech	šampionát	k1gInPc6
a	a	k8xC
v	v	k7c6
sezonách	sezona	k1gFnPc6
1991-92	1991-92	k4
a	a	k8xC
1993-94	1993-94	k4
hrál	hrát	k5eAaImAgInS
po	po	k7c4
celou	celý	k2eAgFnSc4d1
sezónu	sezóna	k1gFnSc4
za	za	k7c4
olympijský	olympijský	k2eAgInSc4d1
národní	národní	k2eAgInSc4d1
tým	tým	k1gInSc4
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s>
Úspěchy	úspěch	k1gInPc1
a	a	k8xC
trofeje	trofej	k1gFnPc1
</s>
<s>
Individuální	individuální	k2eAgFnPc1d1
trofeje	trofej	k1gFnPc1
</s>
<s>
1993	#num#	k4
jmenován	jmenovat	k5eAaImNgInS,k5eAaBmNgInS
do	do	k7c2
ECAC	ECAC	kA
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
All-star	All-star	k1gInSc1
týmu	tým	k1gInSc2
</s>
<s>
1993	#num#	k4
Hráč	hráč	k1gMnSc1
roku	rok	k1gInSc2
ECAC	ECAC	kA
</s>
<s>
1993	#num#	k4
NCAA	NCAA	kA
East	East	k1gInSc4
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
All-american	All-american	k1gInSc1
tým	tým	k1gInSc4
</s>
<s>
Klubové	klubový	k2eAgFnPc1d1
statistiky	statistika	k1gFnPc1
</s>
<s>
Sezona	sezona	k1gFnSc1
</s>
<s>
Klub	klub	k1gInSc1
</s>
<s>
Soutěž	soutěž	k1gFnSc1
</s>
<s>
Základní	základní	k2eAgFnSc1d1
část	část	k1gFnSc1
</s>
<s>
Play	play	k0
off	off	k?
</s>
<s>
ZGABTM	ZGABTM	kA
</s>
<s>
ZGABTM	ZGABTM	kA
</s>
<s>
1987	#num#	k4
<g/>
-	-	kIx~
<g/>
88	#num#	k4
<g/>
Fairfield	Fairfield	k1gMnSc1
Prep	Prep	k1gMnSc1
JesuitsHigh-CT	JesuitsHigh-CT	k1gMnSc1
<g/>
24212849	#num#	k4
<g/>
------	------	k?
</s>
<s>
1988	#num#	k4
<g/>
-	-	kIx~
<g/>
89	#num#	k4
<g/>
Fairfield	Fairfield	k1gMnSc1
Prep	Prep	k1gMnSc1
JesuitsHigh-CT	JesuitsHigh-CT	k1gMnSc1
<g/>
25353166	#num#	k4
<g/>
------	------	k?
</s>
<s>
1989	#num#	k4
<g/>
–	–	k?
<g/>
90	#num#	k4
<g/>
Harvard	Harvard	k1gInSc1
CrimsonECAC	CrimsonECAC	k1gFnSc2
<g/>
179132210	#num#	k4
<g/>
-----	-----	k?
</s>
<s>
1990	#num#	k4
<g/>
-	-	kIx~
<g/>
91	#num#	k4
<g/>
Harvard	Harvard	k1gInSc1
CrimsonECAC	CrimsonECAC	k1gFnSc2
<g/>
2518183622	#num#	k4
<g/>
-----	-----	k?
</s>
<s>
1992	#num#	k4
<g/>
-	-	kIx~
<g/>
93	#num#	k4
<g/>
Harvard	Harvard	k1gInSc1
CrimsonECAC	CrimsonECAC	k1gFnSc2
<g/>
3122416328	#num#	k4
<g/>
-----	-----	k?
</s>
<s>
1993	#num#	k4
<g/>
-	-	kIx~
<g/>
94	#num#	k4
<g/>
Calgary	Calgary	k1gNnPc2
FlamesNHL	FlamesNHL	k1gFnSc1
<g/>
34571226	#num#	k4
<g/>
-----	-----	k?
</s>
<s>
1993	#num#	k4
<g/>
-	-	kIx~
<g/>
94	#num#	k4
<g/>
Hartford	Hartfordo	k1gNnPc2
WhalersNHL	WhalersNHL	k1gFnSc1
<g/>
1615610	#num#	k4
<g/>
-----	-----	k?
</s>
<s>
1994	#num#	k4
<g/>
-	-	kIx~
<g/>
95	#num#	k4
<g/>
Hartford	Hartfordo	k1gNnPc2
WhalersNHL	WhalersNHL	k1gFnSc1
<g/>
3436921	#num#	k4
<g/>
-----	-----	k?
</s>
<s>
1994	#num#	k4
<g/>
-	-	kIx~
<g/>
95	#num#	k4
<g/>
Springfield	Springfieldo	k1gNnPc2
IndiansAHL	IndiansAHL	k1gFnSc1
<g/>
20110	#num#	k4
<g/>
-----	-----	k?
</s>
<s>
1995	#num#	k4
<g/>
-	-	kIx~
<g/>
96	#num#	k4
<g/>
Ottawa	Ottawa	k1gFnSc1
SenatorsNHL	SenatorsNHL	k1gFnSc1
<g/>
42971654	#num#	k4
<g/>
-----	-----	k?
</s>
<s>
1996	#num#	k4
<g/>
-	-	kIx~
<g/>
97	#num#	k4
<g/>
Mighty	Mighta	k1gFnSc2
Ducks	Ducksa	k1gFnPc2
of	of	k?
AnaheimNHL	AnaheimNHL	k1gMnSc1
<g/>
73991854101014	#num#	k4
</s>
<s>
1997	#num#	k4
<g/>
-	-	kIx~
<g/>
98	#num#	k4
<g/>
Mighty	Mighta	k1gFnSc2
Ducks	Ducksa	k1gFnPc2
of	of	k?
AnaheimNHL	AnaheimNHL	k1gMnSc1
<g/>
736101682	#num#	k4
<g/>
-----	-----	k?
</s>
<s>
1998	#num#	k4
<g/>
-	-	kIx~
<g/>
99	#num#	k4
<g/>
Mighty	Mighta	k1gFnSc2
Ducks	Ducksa	k1gFnPc2
of	of	k?
AnaheimNHL	AnaheimNHL	k1gMnSc1
<g/>
7556118340000	#num#	k4
</s>
<s>
1999	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
Mighty	Mighta	k1gFnSc2
Ducks	Ducksa	k1gFnPc2
of	of	k?
AnaheimNHL	AnaheimNHL	k1gMnSc1
<g/>
111126	#num#	k4
<g/>
-----	-----	k?
</s>
<s>
1999	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
New	New	k1gMnPc2
York	York	k1gInSc4
IslandersNHL	IslandersNHL	k1gFnPc2
<g/>
5521331	#num#	k4
<g/>
-----	-----	k?
</s>
<s>
2000	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
Columbus	Columbus	k1gMnSc1
Blue	Blu	k1gInSc2
JacketsNHL	JacketsNHL	k1gFnSc2
<g/>
10000	#num#	k4
<g/>
-----	-----	k?
</s>
<s>
2000	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
Chicago	Chicago	k1gNnSc4
WolvesIHL	WolvesIHL	k1gFnSc2
<g/>
6821214253145494	#num#	k4
</s>
<s>
2001	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
Albany	Alban	k1gInPc7
River	River	k1gMnSc1
RatsAHL	RatsAHL	k1gMnSc1
<g/>
518101823	#num#	k4
<g/>
-----	-----	k?
</s>
<s>
2001	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
Lowell	Lowella	k1gFnPc2
Lock	Lock	k1gInSc4
MonstersAHL	MonstersAHL	k1gFnSc4
<g/>
1665111050556	#num#	k4
</s>
<s>
2002	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
Hamburg	Hamburg	k1gInSc1
FreezersDEL	FreezersDEL	k1gFnSc2
<g/>
521622385250226	#num#	k4
</s>
<s>
2003	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
Kassel	Kassel	k1gInSc1
HuskiesDEL	HuskiesDEL	k1gFnSc2
<g/>
52141630102	#num#	k4
<g/>
-----	-----	k?
</s>
<s>
2004	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
Kassel	Kassel	k1gInSc1
HuskiesDEL	HuskiesDEL	k1gFnSc2
<g/>
5112152767712318	#num#	k4
</s>
<s>
2005	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
Krefeld	Krefelda	k1gFnPc2
PinguineDEL	PinguineDEL	k1gFnSc4
<g/>
482126476652136	#num#	k4
</s>
<s>
2006	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
Krefeld	Krefelda	k1gFnPc2
PinguineDEL	PinguineDEL	k1gFnSc4
<g/>
49920299721120	#num#	k4
</s>
<s>
High-CT	High-CT	k?
Celkově	celkově	k6eAd1
</s>
<s>
49	#num#	k4
</s>
<s>
56	#num#	k4
</s>
<s>
59	#num#	k4
</s>
<s>
115	#num#	k4
</s>
<s>
--	--	k?
</s>
<s>
--	--	k?
</s>
<s>
--	--	k?
</s>
<s>
--	--	k?
</s>
<s>
--	--	k?
</s>
<s>
--	--	k?
</s>
<s>
ECAC	ECAC	kA
Celkově	celkově	k6eAd1
</s>
<s>
73	#num#	k4
</s>
<s>
49	#num#	k4
</s>
<s>
72	#num#	k4
</s>
<s>
121	#num#	k4
</s>
<s>
60	#num#	k4
</s>
<s>
--	--	k?
</s>
<s>
--	--	k?
</s>
<s>
--	--	k?
</s>
<s>
--	--	k?
</s>
<s>
--	--	k?
</s>
<s>
NHL	NHL	kA
Celkově	celkově	k6eAd1
</s>
<s>
414	#num#	k4
</s>
<s>
41	#num#	k4
</s>
<s>
52	#num#	k4
</s>
<s>
93	#num#	k4
</s>
<s>
367	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
AHL	AHL	kA
Celkově	celkově	k6eAd1
</s>
<s>
69	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
33	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
IHL	IHL	kA
Celkově	celkově	k6eAd1
</s>
<s>
68	#num#	k4
</s>
<s>
21	#num#	k4
</s>
<s>
21	#num#	k4
</s>
<s>
42	#num#	k4
</s>
<s>
53	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
DEL	DEL	kA
Celkově	celkově	k6eAd1
</s>
<s>
252	#num#	k4
</s>
<s>
72	#num#	k4
</s>
<s>
99	#num#	k4
</s>
<s>
171	#num#	k4
</s>
<s>
384	#num#	k4
</s>
<s>
19	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
Reprezentační	reprezentační	k2eAgFnPc1d1
statistiky	statistika	k1gFnPc1
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Turnaj	turnaj	k1gInSc1
</s>
<s>
ZGABTM	ZGABTM	kA
</s>
<s>
1990	#num#	k4
</s>
<s>
MSJ	MSJ	kA
</s>
<s>
7	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
1991	#num#	k4
</s>
<s>
MSJ	MSJ	kA
</s>
<s>
8	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
1992	#num#	k4
</s>
<s>
ZOH	ZOH	kA
</s>
<s>
7	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
1993	#num#	k4
</s>
<s>
MS	MS	kA
</s>
<s>
6	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
1994	#num#	k4
</s>
<s>
ZOH	ZOH	kA
</s>
<s>
7	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
1998	#num#	k4
</s>
<s>
MS	MS	kA
</s>
<s>
6	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
2003	#num#	k4
</s>
<s>
MS	MS	kA
</s>
<s>
6	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
Celkově	celkově	k6eAd1
juniorská	juniorský	k2eAgFnSc1d1
reprezentace	reprezentace	k1gFnSc1
</s>
<s>
15	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
Celkově	celkově	k6eAd1
seniorská	seniorský	k2eAgFnSc1d1
reprezentace	reprezentace	k1gFnSc1
</s>
<s>
96	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
33	#num#	k4
</s>
<s>
49	#num#	k4
</s>
<s>
49	#num#	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Ted	Ted	k1gMnSc1
Drury	Drura	k1gFnSc2
–	–	k?
statistiky	statistika	k1gFnSc2
na	na	k7c4
Eliteprospects	Eliteprospects	k1gInSc4
<g/>
.	.	kIx.
<g/>
com	com	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Ted	Ted	k1gMnSc1
Drury	Drura	k1gFnSc2
–	–	k?
statistiky	statistika	k1gFnSc2
na	na	k7c4
Hockeydb	Hockeydb	k1gInSc4
<g/>
.	.	kIx.
<g/>
com	com	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lední	lední	k2eAgInSc1d1
hokej	hokej	k1gInSc1
|	|	kIx~
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
</s>
