<s>
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
řečený	řečený	k2eAgMnSc1d1	řečený
král	král	k1gMnSc1	král
železný	železný	k2eAgMnSc1d1	železný
a	a	k8xC	a
zlatý	zlatý	k2eAgMnSc1d1	zlatý
(	(	kIx(	(
<g/>
1233	[number]	k4	1233
-	-	kIx~	-
26	[number]	k4	26
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1278	[number]	k4	1278
<g/>
,	,	kIx,	,
Suché	Suché	k2eAgFnSc2d1	Suché
Kruty	kruta	k1gFnSc2	kruta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
pátý	pátý	k4xOgMnSc1	pátý
král	král	k1gMnSc1	král
český	český	k2eAgMnSc1d1	český
z	z	k7c2	z
rodu	rod	k1gInSc6	rod
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
<g/>
.	.	kIx.	.
</s>
