<s>
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
řečený	řečený	k2eAgMnSc1d1	řečený
král	král	k1gMnSc1	král
železný	železný	k2eAgMnSc1d1	železný
a	a	k8xC	a
zlatý	zlatý	k2eAgMnSc1d1	zlatý
(	(	kIx(	(
<g/>
1233	[number]	k4	1233
-	-	kIx~	-
26	[number]	k4	26
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1278	[number]	k4	1278
<g/>
,	,	kIx,	,
Suché	Suché	k2eAgFnSc2d1	Suché
Kruty	kruta	k1gFnSc2	kruta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
pátý	pátý	k4xOgMnSc1	pátý
král	král	k1gMnSc1	král
český	český	k2eAgMnSc1d1	český
z	z	k7c2	z
rodu	rod	k1gInSc6	rod
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
pověsti	pověst	k1gFnSc2	pověst
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
ji	on	k3xPp3gFnSc4	on
uvádí	uvádět	k5eAaImIp3nS	uvádět
Beckovský	Beckovský	k2eAgMnSc1d1	Beckovský
v	v	k7c6	v
Poselkyni	poselkyně	k1gFnSc6	poselkyně
starých	starý	k2eAgFnPc2d1	stará
příběhův	příběhův	k2eAgInSc1d1	příběhův
českých	český	k2eAgInPc2d1	český
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Přemysl	Přemysl	k1gMnSc1	Přemysl
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
Městci	Městec	k1gInSc6	Městec
Králové	Králová	k1gFnSc2	Králová
jako	jako	k8xC	jako
druhorozený	druhorozený	k2eAgMnSc1d1	druhorozený
syn	syn	k1gMnSc1	syn
krále	král	k1gMnSc2	král
Václava	Václav	k1gMnSc2	Václav
I.	I.	kA	I.
a	a	k8xC	a
Kunhuty	Kunhuta	k1gFnPc1	Kunhuta
Štaufské	Štaufský	k2eAgFnPc1d1	Štaufská
<g/>
..	..	k?	..
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
jaký	jaký	k3yRgInSc4	jaký
skutečně	skutečně	k6eAd1	skutečně
byl	být	k5eAaImAgMnS	být
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
si	se	k3xPyFc3	se
získal	získat	k5eAaPmAgMnS	získat
pověst	pověst	k1gFnSc4	pověst
vládce	vládce	k1gMnSc1	vládce
tvrdé	tvrdý	k2eAgFnSc2d1	tvrdá
pěsti	pěst	k1gFnSc2	pěst
<g/>
,	,	kIx,	,
mohutné	mohutný	k2eAgFnSc2d1	mohutná
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
osobní	osobní	k2eAgFnSc2d1	osobní
ctižádosti	ctižádost	k1gFnSc2	ctižádost
<g/>
,	,	kIx,	,
těžko	těžko	k6eAd1	těžko
vyčteme	vyčíst	k5eAaPmIp1nP	vyčíst
ve	v	k7c6	v
středověkých	středověký	k2eAgFnPc6d1	středověká
kronikách	kronika	k1gFnPc6	kronika
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gInPc1	jeho
ostatky	ostatek	k1gInPc1	ostatek
dochovaly	dochovat	k5eAaPmAgInP	dochovat
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
špatném	špatný	k2eAgInSc6d1	špatný
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
antropolog	antropolog	k1gMnSc1	antropolog
Emanuel	Emanuel	k1gMnSc1	Emanuel
Vlček	Vlček	k1gMnSc1	Vlček
víceméně	víceméně	k9	víceméně
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
fyzický	fyzický	k2eAgInSc4d1	fyzický
popis	popis	k1gInSc4	popis
<g/>
.	.	kIx.	.
</s>
<s>
Přemysl	Přemysl	k1gMnSc1	Přemysl
byl	být	k5eAaImAgMnS	být
průměrně	průměrně	k6eAd1	průměrně
vysoký	vysoký	k2eAgMnSc1d1	vysoký
<g/>
,	,	kIx,	,
snědý	snědý	k2eAgInSc1d1	snědý
a	a	k8xC	a
prý	prý	k9	prý
pohledný	pohledný	k2eAgMnSc1d1	pohledný
muž	muž	k1gMnSc1	muž
s	s	k7c7	s
dvorským	dvorský	k2eAgNnSc7d1	dvorské
vychováním	vychování	k1gNnSc7	vychování
a	a	k8xC	a
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
dobu	doba	k1gFnSc4	doba
dobrým	dobrý	k2eAgNnSc7d1	dobré
vzděláním	vzdělání	k1gNnSc7	vzdělání
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
jeho	jeho	k3xOp3gMnPc2	jeho
činů	čin	k1gInPc2	čin
lze	lze	k6eAd1	lze
usuzovat	usuzovat	k5eAaImF	usuzovat
na	na	k7c4	na
velkomyslnost	velkomyslnost	k1gFnSc4	velkomyslnost
a	a	k8xC	a
jistou	jistý	k2eAgFnSc4d1	jistá
svobodomyslnost	svobodomyslnost	k1gFnSc4	svobodomyslnost
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
příliš	příliš	k6eAd1	příliš
unáhlené	unáhlený	k2eAgNnSc1d1	unáhlené
hodnocení	hodnocení	k1gNnSc1	hodnocení
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1254	[number]	k4	1254
například	například	k6eAd1	například
jako	jako	k8xS	jako
král	král	k1gMnSc1	král
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
nebývalá	nebývalý	k2eAgNnPc4d1	nebývalý
práva	právo	k1gNnPc4	právo
Židům	Žid	k1gMnPc3	Žid
<g/>
:	:	kIx,	:
obecní	obecnět	k5eAaImIp3nS	obecnět
samosprávu	samospráva	k1gFnSc4	samospráva
a	a	k8xC	a
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
majetku	majetek	k1gInSc2	majetek
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
to	ten	k3xDgNnSc4	ten
ale	ale	k9	ale
byl	být	k5eAaImAgMnS	být
Přemysl	Přemysl	k1gMnSc1	Přemysl
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
do	do	k7c2	do
země	zem	k1gFnSc2	zem
pozval	pozvat	k5eAaPmAgMnS	pozvat
inkvizici	inkvizice	k1gFnSc4	inkvizice
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
jeho	jeho	k3xOp3gNnSc2	jeho
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
vždy	vždy	k6eAd1	vždy
odpovídala	odpovídat	k5eAaImAgFnS	odpovídat
Otakarovým	Otakarův	k2eAgInPc3d1	Otakarův
aktuálním	aktuální	k2eAgInPc3d1	aktuální
cílům	cíl	k1gInPc3	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Jistě	jistě	k6eAd1	jistě
netrpěl	trpět	k5eNaImAgInS	trpět
přecitlivělostí	přecitlivělost	k1gFnSc7	přecitlivělost
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
a	a	k8xC	a
syna	syn	k1gMnSc2	syn
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
někdy	někdy	k6eAd1	někdy
prý	prý	k9	prý
propadal	propadat	k5eAaImAgMnS	propadat
bezdůvodným	bezdůvodný	k2eAgMnPc3d1	bezdůvodný
záchvatům	záchvat	k1gInPc3	záchvat
vzteku	vztek	k1gInSc2	vztek
a	a	k8xC	a
jako	jako	k9	jako
většina	většina	k1gFnSc1	většina
středověkých	středověký	k2eAgMnPc2d1	středověký
panovníků	panovník	k1gMnPc2	panovník
nesnášel	snášet	k5eNaImAgMnS	snášet
odpor	odpor	k1gInSc4	odpor
<g/>
.	.	kIx.	.
</s>
<s>
Rakouské	rakouský	k2eAgFnPc1d1	rakouská
kroniky	kronika	k1gFnPc1	kronika
se	se	k3xPyFc4	se
například	například	k6eAd1	například
věnují	věnovat	k5eAaImIp3nP	věnovat
především	především	k6eAd1	především
těm	ten	k3xDgMnPc3	ten
činům	čin	k1gInPc3	čin
Otakara	Otakar	k1gMnSc2	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
i	i	k8xC	i
Václava	Václav	k1gMnSc4	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
příliš	příliš	k6eAd1	příliš
sympatií	sympatie	k1gFnSc7	sympatie
nevzbuzují	vzbuzovat	k5eNaImIp3nP	vzbuzovat
<g/>
.	.	kIx.	.
</s>
<s>
Přízvisko	přízvisko	k1gNnSc1	přízvisko
"	"	kIx"	"
<g/>
železný	železný	k2eAgInSc1d1	železný
<g/>
"	"	kIx"	"
mu	on	k3xPp3gMnSc3	on
dali	dát	k5eAaPmAgMnP	dát
divocí	divoký	k2eAgMnPc1d1	divoký
Kumáni	Kumán	k1gMnPc1	Kumán
z	z	k7c2	z
Uherska	Uhersko	k1gNnSc2	Uhersko
po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Kressenbrunnu	Kressenbrunn	k1gInSc2	Kressenbrunn
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
proslavili	proslavit	k5eAaPmAgMnP	proslavit
Přemyslovi	Přemyslův	k2eAgMnPc1d1	Přemyslův
těžce	těžce	k6eAd1	těžce
odění	oděný	k2eAgMnPc1d1	oděný
rytíři	rytíř	k1gMnPc1	rytíř
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k1gNnSc1	místo
"	"	kIx"	"
<g/>
zlatý	zlatý	k1gInSc1	zlatý
<g/>
"	"	kIx"	"
by	by	kYmCp3nP	by
spíše	spíše	k9	spíše
odpovídalo	odpovídat	k5eAaImAgNnS	odpovídat
jiné	jiný	k2eAgNnSc1d1	jiné
označení	označení	k1gNnSc1	označení
-	-	kIx~	-
Přemyslovo	Přemyslův	k2eAgNnSc1d1	Přemyslovo
bohatství	bohatství	k1gNnSc1	bohatství
pocházelo	pocházet	k5eAaImAgNnS	pocházet
ze	z	k7c2	z
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
.	.	kIx.	.
</s>
<s>
Skutečně	skutečně	k6eAd1	skutečně
neexistují	existovat	k5eNaImIp3nP	existovat
záznamy	záznam	k1gInPc1	záznam
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
za	za	k7c2	za
doby	doba	k1gFnSc2	doba
své	svůj	k3xOyFgFnSc2	svůj
vlády	vláda	k1gFnSc2	vláda
dostal	dostat	k5eAaPmAgInS	dostat
do	do	k7c2	do
finančních	finanční	k2eAgFnPc2d1	finanční
potíží	potíž	k1gFnPc2	potíž
<g/>
.	.	kIx.	.
</s>
<s>
Přemysl	Přemysl	k1gMnSc1	Přemysl
byl	být	k5eAaImAgMnS	být
mocným	mocný	k2eAgMnSc7d1	mocný
středoevropským	středoevropský	k2eAgMnSc7d1	středoevropský
panovníkem	panovník	k1gMnSc7	panovník
(	(	kIx(	(
<g/>
v	v	k7c6	v
době	doba	k1gFnSc6	doba
interregna	interregnum	k1gNnSc2	interregnum
v	v	k7c6	v
římskoněmecké	římskoněmecký	k2eAgFnSc6d1	římskoněmecká
říši	říš	k1gFnSc6	říš
vybudoval	vybudovat	k5eAaPmAgMnS	vybudovat
rozsáhlé	rozsáhlý	k2eAgNnSc4d1	rozsáhlé
panství	panství	k1gNnSc4	panství
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
rozkládalo	rozkládat	k5eAaImAgNnS	rozkládat
za	za	k7c2	za
Alpy	alpa	k1gFnSc2	alpa
až	až	k9	až
k	k	k7c3	k
Jaderskému	jaderský	k2eAgNnSc3d1	Jaderské
moři	moře	k1gNnSc3	moře
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
za	za	k7c2	za
jehož	jehož	k3xOyRp3gFnSc2	jehož
vlády	vláda	k1gFnSc2	vláda
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
české	český	k2eAgNnSc1d1	české
království	království	k1gNnSc1	království
největšího	veliký	k2eAgInSc2d3	veliký
rozmachu	rozmach	k1gInSc2	rozmach
až	až	k9	až
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
svým	svůj	k3xOyFgFnPc3	svůj
diplomatickým	diplomatický	k2eAgFnPc3d1	diplomatická
schopnostem	schopnost	k1gFnPc3	schopnost
či	či	k8xC	či
sňatkové	sňatkový	k2eAgFnSc6d1	sňatková
politice	politika	k1gFnSc6	politika
Přemysl	Přemysl	k1gMnSc1	Přemysl
získal	získat	k5eAaPmAgMnS	získat
kromě	kromě	k7c2	kromě
české	český	k2eAgFnSc2d1	Česká
koruny	koruna	k1gFnSc2	koruna
titul	titul	k1gInSc1	titul
vévoda	vévoda	k1gMnSc1	vévoda
rakouský	rakouský	k2eAgMnSc1d1	rakouský
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1251	[number]	k4	1251
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vévoda	vévoda	k1gMnSc1	vévoda
štýrský	štýrský	k2eAgMnSc1d1	štýrský
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1261	[number]	k4	1261
<g/>
)	)	kIx)	)
a	a	k8xC	a
vévoda	vévoda	k1gMnSc1	vévoda
korutanský	korutanský	k2eAgMnSc1d1	korutanský
a	a	k8xC	a
kraňský	kraňský	k2eAgMnSc1d1	kraňský
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1269	[number]	k4	1269
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
významu	význam	k1gInSc2	význam
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
projevil	projevit	k5eAaPmAgInS	projevit
i	i	k9	i
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Otakar	Otakar	k1gMnSc1	Otakar
několikrát	několikrát	k6eAd1	několikrát
ucházel	ucházet	k5eAaImAgMnS	ucházet
o	o	k7c4	o
římskoněmeckou	římskoněmecký	k2eAgFnSc4d1	římskoněmecká
korunu	koruna	k1gFnSc4	koruna
-	-	kIx~	-
ovšem	ovšem	k9	ovšem
právě	právě	k9	právě
jeho	jeho	k3xOp3gFnSc1	jeho
moc	moc	k1gFnSc1	moc
mu	on	k3xPp3gMnSc3	on
tuto	tento	k3xDgFnSc4	tento
výsadu	výsada	k1gFnSc4	výsada
nakonec	nakonec	k6eAd1	nakonec
neumožnila	umožnit	k5eNaPmAgFnS	umožnit
získat	získat	k5eAaPmF	získat
<g/>
.	.	kIx.	.
</s>
<s>
Přemyslův	Přemyslův	k2eAgInSc1d1	Přemyslův
závratný	závratný	k2eAgInSc1d1	závratný
vzestup	vzestup	k1gInSc1	vzestup
následoval	následovat	k5eAaImAgInS	následovat
strmý	strmý	k2eAgInSc1d1	strmý
pád	pád	k1gInSc1	pád
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1247	[number]	k4	1247
zemřel	zemřít	k5eAaPmAgMnS	zemřít
Přemyslův	Přemyslův	k2eAgMnSc1d1	Přemyslův
starší	starý	k2eAgMnSc1d2	starší
bratr	bratr	k1gMnSc1	bratr
Vladislav	Vladislav	k1gMnSc1	Vladislav
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
zanechal	zanechat	k5eAaPmAgMnS	zanechat
dědice	dědic	k1gMnSc4	dědic
<g/>
,	,	kIx,	,
a	a	k8xC	a
Přemysl	Přemysl	k1gMnSc1	Přemysl
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
vychovávaný	vychovávaný	k2eAgInSc1d1	vychovávaný
pro	pro	k7c4	pro
církevní	církevní	k2eAgFnSc4d1	církevní
kariéru	kariéra	k1gFnSc4	kariéra
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
tak	tak	k9	tak
stal	stát	k5eAaPmAgInS	stát
jediným	jediný	k2eAgMnSc7d1	jediný
dědicem	dědic	k1gMnSc7	dědic
českého	český	k2eAgInSc2d1	český
trůnu	trůn	k1gInSc2	trůn
a	a	k8xC	a
moravským	moravský	k2eAgMnSc7d1	moravský
markrabětem	markrabě	k1gMnSc7	markrabě
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
předáků	předák	k1gMnPc2	předák
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
chtěli	chtít	k5eAaImAgMnP	chtít
v	v	k7c6	v
úřadech	úřad	k1gInPc6	úřad
nahradit	nahradit	k5eAaPmF	nahradit
Václavovy	Václavův	k2eAgMnPc4d1	Václavův
oblíbence	oblíbenec	k1gMnPc4	oblíbenec
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
králova	králův	k2eAgNnSc2d1	královo
vědomí	vědomí	k1gNnSc2	vědomí
zvolila	zvolit	k5eAaPmAgFnS	zvolit
31	[number]	k4	31
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1247	[number]	k4	1247
ctižádostivého	ctižádostivý	k2eAgMnSc2d1	ctižádostivý
prince	princ	k1gMnSc2	princ
za	za	k7c4	za
spoluvládce	spoluvládce	k1gMnSc4	spoluvládce
s	s	k7c7	s
titulem	titul	k1gInSc7	titul
mladší	mladý	k2eAgMnSc1d2	mladší
král	král	k1gMnSc1	král
a	a	k8xC	a
postavila	postavit	k5eAaPmAgFnS	postavit
ho	on	k3xPp3gInSc4	on
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
odboje	odboj	k1gInSc2	odboj
proti	proti	k7c3	proti
jeho	jeho	k3xOp3gMnSc3	jeho
otci	otec	k1gMnSc3	otec
<g/>
.	.	kIx.	.
</s>
<s>
Volba	volba	k1gFnSc1	volba
Přemysla	Přemysl	k1gMnSc2	Přemysl
představovala	představovat	k5eAaImAgFnS	představovat
zjevnou	zjevný	k2eAgFnSc4d1	zjevná
vzpouru	vzpoura	k1gFnSc4	vzpoura
a	a	k8xC	a
jak	jak	k8xS	jak
Přemysl	Přemysl	k1gMnSc1	Přemysl
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
předáci	předák	k1gMnPc1	předák
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
ho	on	k3xPp3gMnSc4	on
zvolili	zvolit	k5eAaPmAgMnP	zvolit
<g/>
,	,	kIx,	,
předpokládali	předpokládat	k5eAaImAgMnP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
králi	král	k1gMnPc7	král
nezbude	zbýt	k5eNaPmIp3nS	zbýt
nic	nic	k3yNnSc1	nic
jiného	jiný	k2eAgNnSc2d1	jiné
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
s	s	k7c7	s
daným	daný	k2eAgInSc7d1	daný
stavem	stav	k1gInSc7	stav
smířit	smířit	k5eAaPmF	smířit
<g/>
.	.	kIx.	.
</s>
<s>
Přemyslova	Přemyslův	k2eAgFnSc1d1	Přemyslova
spoluvláda	spoluvláda	k1gFnSc1	spoluvláda
pro	pro	k7c4	pro
něj	on	k3xPp3gNnSc4	on
začala	začít	k5eAaPmAgFnS	začít
velmi	velmi	k6eAd1	velmi
slibně	slibně	k6eAd1	slibně
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
Václav	Václav	k1gMnSc1	Václav
<g/>
,	,	kIx,	,
opuštěný	opuštěný	k2eAgMnSc1d1	opuštěný
většinou	většina	k1gFnSc7	většina
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
,	,	kIx,	,
musel	muset	k5eAaImAgMnS	muset
nejprve	nejprve	k6eAd1	nejprve
s	s	k7c7	s
volbou	volba	k1gFnSc7	volba
souhlasit	souhlasit	k5eAaImF	souhlasit
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
však	však	k9	však
přesvědčil	přesvědčit	k5eAaPmAgMnS	přesvědčit
o	o	k7c6	o
vážnosti	vážnost	k1gFnSc6	vážnost
situace	situace	k1gFnSc2	situace
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
synem	syn	k1gMnSc7	syn
stála	stát	k5eAaImAgFnS	stát
šlechta	šlechta	k1gFnSc1	šlechta
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
otce	otec	k1gMnSc4	otec
neměla	mít	k5eNaImAgFnS	mít
v	v	k7c6	v
oblibě	obliba	k1gFnSc6	obliba
<g/>
,	,	kIx,	,
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
straně	strana	k1gFnSc6	strana
naopak	naopak	k6eAd1	naopak
byla	být	k5eAaImAgFnS	být
jen	jen	k9	jen
malá	malý	k2eAgFnSc1d1	malá
skupina	skupina	k1gFnSc1	skupina
stoupenců	stoupenec	k1gMnPc2	stoupenec
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
si	se	k3xPyFc3	se
přála	přát	k5eAaImAgFnS	přát
tvrdý	tvrdý	k2eAgInSc4d1	tvrdý
zákrok	zákrok	k1gInSc4	zákrok
proti	proti	k7c3	proti
jeho	jeho	k3xOp3gMnSc3	jeho
synovi	syn	k1gMnSc3	syn
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
situaci	situace	k1gFnSc4	situace
vyřeší	vyřešit	k5eAaPmIp3nS	vyřešit
jen	jen	k9	jen
bojové	bojový	k2eAgNnSc1d1	bojové
střetnutí	střetnutí	k1gNnSc1	střetnutí
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
však	však	k9	však
viděl	vidět	k5eAaImAgMnS	vidět
synovu	synův	k2eAgFnSc4d1	synova
převahu	převaha	k1gFnSc4	převaha
a	a	k8xC	a
uchýlil	uchýlit	k5eAaPmAgMnS	uchýlit
se	se	k3xPyFc4	se
do	do	k7c2	do
Míšně	Míšeň	k1gFnSc2	Míšeň
<g/>
.	.	kIx.	.
</s>
<s>
Přemysl	Přemysl	k1gMnSc1	Přemysl
si	se	k3xPyFc3	se
uvědomoval	uvědomovat	k5eAaImAgMnS	uvědomovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
otcovi	otcův	k2eAgMnPc1d1	otcův
stoupenci	stoupenec	k1gMnPc1	stoupenec
představují	představovat	k5eAaImIp3nP	představovat
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gNnSc4	jeho
postavení	postavení	k1gNnSc4	postavení
hrozbu	hrozba	k1gFnSc4	hrozba
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1248	[number]	k4	1248
vypravil	vypravit	k5eAaPmAgMnS	vypravit
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
malého	malý	k2eAgNnSc2d1	malé
vojska	vojsko	k1gNnSc2	vojsko
dobýt	dobýt	k5eAaPmF	dobýt
jejich	jejich	k3xOp3gFnSc4	jejich
baštu	bašta	k1gFnSc4	bašta
<g/>
,	,	kIx,	,
město	město	k1gNnSc1	město
Most	most	k1gInSc1	most
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
ovšem	ovšem	k9	ovšem
k	k	k7c3	k
rychlému	rychlý	k2eAgInSc3d1	rychlý
obratu	obrat	k1gInSc3	obrat
situace	situace	k1gFnSc2	situace
<g/>
.	.	kIx.	.
</s>
<s>
Václavovi	Václavův	k2eAgMnPc1d1	Václavův
stoupenci	stoupenec	k1gMnPc1	stoupenec
přepadli	přepadnout	k5eAaPmAgMnP	přepadnout
pod	pod	k7c7	pod
Mostem	most	k1gInSc7	most
Přemyslův	Přemyslův	k2eAgInSc4d1	Přemyslův
vojenský	vojenský	k2eAgInSc4d1	vojenský
tábor	tábor	k1gInSc4	tábor
a	a	k8xC	a
uštědřili	uštědřit	k5eAaPmAgMnP	uštědřit
mu	on	k3xPp3gMnSc3	on
porážku	porážka	k1gFnSc4	porážka
<g/>
.	.	kIx.	.
</s>
<s>
Koalice	koalice	k1gFnSc1	koalice
proti	proti	k7c3	proti
králi	král	k1gMnSc3	král
Václavovi	Václav	k1gMnSc3	Václav
I.	I.	kA	I.
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
rozpadat	rozpadat	k5eAaImF	rozpadat
a	a	k8xC	a
Přemysl	Přemysl	k1gMnSc1	Přemysl
začal	začít	k5eAaPmAgMnS	začít
s	s	k7c7	s
otcem	otec	k1gMnSc7	otec
vyjednávat	vyjednávat	k5eAaImF	vyjednávat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1249	[number]	k4	1249
Václavovo	Václavův	k2eAgNnSc1d1	Václavovo
vojsko	vojsko	k1gNnSc1	vojsko
oblehlo	oblehnout	k5eAaPmAgNnS	oblehnout
Pražský	pražský	k2eAgInSc4d1	pražský
hrad	hrad	k1gInSc4	hrad
a	a	k8xC	a
dobylo	dobýt	k5eAaPmAgNnS	dobýt
ho	on	k3xPp3gMnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Přemyslovi	Přemyslův	k2eAgMnPc1d1	Přemyslův
nezbylo	zbýt	k5eNaPmAgNnS	zbýt
nic	nic	k3yNnSc1	nic
jiného	jiný	k2eAgNnSc2d1	jiné
než	než	k8xS	než
kapitulovat	kapitulovat	k5eAaBmF	kapitulovat
a	a	k8xC	a
zřejmě	zřejmě	k6eAd1	zřejmě
i	i	k9	i
za	za	k7c4	za
přispění	přispění	k1gNnSc4	přispění
své	svůj	k3xOyFgFnSc2	svůj
tety	teta	k1gFnSc2	teta
Anežky	Anežka	k1gFnSc2	Anežka
se	se	k3xPyFc4	se
s	s	k7c7	s
otcem	otec	k1gMnSc7	otec
smířit	smířit	k5eAaPmF	smířit
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
svého	svůj	k3xOyFgMnSc4	svůj
syna	syn	k1gMnSc4	syn
krátce	krátce	k6eAd1	krátce
věznil	věznit	k5eAaImAgMnS	věznit
na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
Přimda	Přimdo	k1gNnSc2	Přimdo
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
ovšem	ovšem	k9	ovšem
jeho	jeho	k3xOp3gMnSc1	jeho
jediný	jediný	k2eAgMnSc1d1	jediný
dědic	dědic	k1gMnSc1	dědic
a	a	k8xC	a
šance	šance	k1gFnSc1	šance
na	na	k7c4	na
pokračování	pokračování	k1gNnSc4	pokračování
dynastie	dynastie	k1gFnSc2	dynastie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
týdnech	týden	k1gInPc6	týden
ho	on	k3xPp3gMnSc4	on
z	z	k7c2	z
hradního	hradní	k2eAgNnSc2d1	hradní
vězení	vězení	k1gNnSc2	vězení
propustil	propustit	k5eAaPmAgInS	propustit
<g/>
,	,	kIx,	,
odbojní	odbojný	k2eAgMnPc1d1	odbojný
šlechtici	šlechtic	k1gMnPc1	šlechtic
ovšem	ovšem	k9	ovšem
už	už	k6eAd1	už
tak	tak	k6eAd1	tak
snadno	snadno	k6eAd1	snadno
nevyvázli	vyváznout	k5eNaPmAgMnP	vyváznout
<g/>
.	.	kIx.	.
</s>
<s>
Přemysla	Přemysl	k1gMnSc2	Přemysl
poté	poté	k6eAd1	poté
otec	otec	k1gMnSc1	otec
znovu	znovu	k6eAd1	znovu
stanovil	stanovit	k5eAaPmAgMnS	stanovit
markrabětem	markrabě	k1gMnSc7	markrabě
moravským	moravský	k2eAgNnSc7d1	Moravské
-	-	kIx~	-
pod	pod	k7c7	pod
dozorem	dozor	k1gInSc7	dozor
králových	králův	k2eAgMnPc2d1	králův
spolehlivých	spolehlivý	k2eAgMnPc2d1	spolehlivý
šlechticů	šlechtic	k1gMnPc2	šlechtic
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
byly	být	k5eAaImAgInP	být
vztahy	vztah	k1gInPc1	vztah
otce	otec	k1gMnSc2	otec
a	a	k8xC	a
syna	syn	k1gMnSc2	syn
tak	tak	k9	tak
dlouho	dlouho	k6eAd1	dlouho
napjaté	napjatý	k2eAgInPc4d1	napjatý
<g/>
,	,	kIx,	,
politické	politický	k2eAgInPc4d1	politický
zájmy	zájem	k1gInPc4	zájem
je	být	k5eAaImIp3nS	být
opět	opět	k6eAd1	opět
spojily	spojit	k5eAaPmAgFnP	spojit
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
společný	společný	k2eAgInSc1d1	společný
krok	krok	k1gInSc1	krok
obou	dva	k4xCgMnPc2	dva
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
na	na	k7c6	na
sebe	sebe	k3xPyFc4	sebe
nenechal	nechat	k5eNaPmAgMnS	nechat
dlouho	dlouho	k6eAd1	dlouho
čekat	čekat	k5eAaImF	čekat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
posledního	poslední	k2eAgMnSc2d1	poslední
vévody	vévoda	k1gMnSc2	vévoda
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Babenberků	Babenberk	k1gInPc2	Babenberk
Fridricha	Fridrich	k1gMnSc2	Fridrich
II	II	kA	II
<g/>
.	.	kIx.	.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1246	[number]	k4	1246
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
ohledně	ohledně	k7c2	ohledně
nástupnictví	nástupnictví	k1gNnSc2	nástupnictví
složitá	složitý	k2eAgFnSc1d1	složitá
situace	situace	k1gFnSc1	situace
<g/>
.	.	kIx.	.
</s>
<s>
Tzv.	tzv.	kA	tzv.
privilegium	privilegium	k1gNnSc1	privilegium
minus	minus	k6eAd1	minus
potvrzovalo	potvrzovat	k5eAaImAgNnS	potvrzovat
Babenberkovnám	Babenberkovna	k1gFnPc3	Babenberkovna
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
Rakousko	Rakousko	k1gNnSc4	Rakousko
a	a	k8xC	a
Štýrsko	Štýrsko	k1gNnSc4	Štýrsko
<g/>
,	,	kIx,	,
a	a	k8xC	a
s	s	k7c7	s
jejich	jejich	k3xOp3gFnSc7	jejich
rukou	ruka	k1gFnSc7	ruka
je	být	k5eAaImIp3nS	být
mohl	moct	k5eAaImAgInS	moct
získat	získat	k5eAaPmF	získat
i	i	k9	i
jejich	jejich	k3xOp3gMnSc1	jejich
nápadník	nápadník	k1gMnSc1	nápadník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1251	[number]	k4	1251
si	se	k3xPyFc3	se
rakouská	rakouský	k2eAgFnSc1d1	rakouská
šlechta	šlechta	k1gFnSc1	šlechta
zvolila	zvolit	k5eAaPmAgFnS	zvolit
za	za	k7c4	za
vládce	vládce	k1gMnSc4	vládce
českého	český	k2eAgMnSc2d1	český
prince	princ	k1gMnSc2	princ
Přemysla	Přemysl	k1gMnSc2	Přemysl
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
tedy	tedy	k9	tedy
pravda	pravda	k1gFnSc1	pravda
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
i	i	k9	i
dnes	dnes	k6eAd1	dnes
mylně	mylně	k6eAd1	mylně
uvádějí	uvádět	k5eAaImIp3nP	uvádět
někteří	některý	k3yIgMnPc1	některý
anglicky	anglicky	k6eAd1	anglicky
a	a	k8xC	a
německy	německy	k6eAd1	německy
píšící	píšící	k2eAgMnPc1d1	píšící
historici	historik	k1gMnPc1	historik
<g/>
,	,	kIx,	,
že	že	k8xS	že
Rakousko	Rakousko	k1gNnSc4	Rakousko
dobyl	dobýt	k5eAaPmAgMnS	dobýt
silou	síla	k1gFnSc7	síla
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
nárok	nárok	k1gInSc4	nárok
na	na	k7c4	na
Rakousy	Rakousy	k1gInPc4	Rakousy
a	a	k8xC	a
Štýrsko	Štýrsko	k1gNnSc4	Štýrsko
podpořil	podpořit	k5eAaPmAgInS	podpořit
sňatkem	sňatek	k1gInSc7	sňatek
se	s	k7c7	s
sestrou	sestra	k1gFnSc7	sestra
Fridricha	Fridrich	k1gMnSc2	Fridrich
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Markétou	Markéta	k1gFnSc7	Markéta
(	(	kIx(	(
<g/>
1252	[number]	k4	1252
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
byla	být	k5eAaImAgFnS	být
o	o	k7c4	o
asi	asi	k9	asi
třicet	třicet	k4xCc4	třicet
let	léto	k1gNnPc2	léto
starší	starý	k2eAgFnSc1d2	starší
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
výsostně	výsostně	k6eAd1	výsostně
politický	politický	k2eAgInSc1d1	politický
svazek	svazek	k1gInSc1	svazek
mu	on	k3xPp3gMnSc3	on
sice	sice	k8xC	sice
příliš	příliš	k6eAd1	příliš
osobního	osobní	k2eAgNnSc2d1	osobní
štěstí	štěstí	k1gNnSc2	štěstí
nepřinesl	přinést	k5eNaPmAgInS	přinést
<g/>
,	,	kIx,	,
splnil	splnit	k5eAaPmAgInS	splnit
však	však	k9	však
svůj	svůj	k3xOyFgInSc4	svůj
účel	účel	k1gInSc4	účel
<g/>
:	:	kIx,	:
garantoval	garantovat	k5eAaBmAgInS	garantovat
územní	územní	k2eAgInSc4d1	územní
zisk	zisk	k1gInSc4	zisk
pro	pro	k7c4	pro
rod	rod	k1gInSc4	rod
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
a	a	k8xC	a
výrazně	výrazně	k6eAd1	výrazně
posilnil	posilnit	k5eAaPmAgMnS	posilnit
jejich	jejich	k3xOp3gFnSc4	jejich
pozici	pozice	k1gFnSc4	pozice
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc4	ten
nerovné	rovný	k2eNgNnSc4d1	nerovné
spojení	spojení	k1gNnSc4	spojení
a	a	k8xC	a
už	už	k6eAd1	už
mnozí	mnohý	k2eAgMnPc1d1	mnohý
současníci	současník	k1gMnPc1	současník
ho	on	k3xPp3gNnSc4	on
komentovali	komentovat	k5eAaBmAgMnP	komentovat
různými	různý	k2eAgFnPc7d1	různá
ironickými	ironický	k2eAgFnPc7d1	ironická
poznámkami	poznámka	k1gFnPc7	poznámka
<g/>
.	.	kIx.	.
</s>
<s>
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stal	stát	k5eAaPmAgMnS	stát
pánem	pán	k1gMnSc7	pán
Rakouska	Rakousko	k1gNnSc2	Rakousko
a	a	k8xC	a
Štýrska	Štýrsko	k1gNnSc2	Štýrsko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
těžko	těžko	k6eAd1	těžko
mohl	moct	k5eAaImAgMnS	moct
doufat	doufat	k5eAaImF	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gInSc4	on
padesátiletá	padesátiletý	k2eAgFnSc1d1	padesátiletá
Markéta	Markéta	k1gFnSc1	Markéta
obdaří	obdařit	k5eAaPmIp3nS	obdařit
dětmi	dítě	k1gFnPc7	dítě
a	a	k8xC	a
zajistí	zajistit	k5eAaPmIp3nS	zajistit
tak	tak	k9	tak
pokračování	pokračování	k1gNnSc1	pokračování
dynastie	dynastie	k1gFnSc2	dynastie
<g/>
.	.	kIx.	.
</s>
<s>
Přemysl	Přemysl	k1gMnSc1	Přemysl
zřejmě	zřejmě	k6eAd1	zřejmě
věděl	vědět	k5eAaImAgMnS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
otázkou	otázka	k1gFnSc7	otázka
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
rozluce	rozluka	k1gFnSc3	rozluka
manželství	manželství	k1gNnSc2	manželství
<g/>
.	.	kIx.	.
</s>
<s>
Těmto	tento	k3xDgMnPc3	tento
myšlenkám	myšlenka	k1gFnPc3	myšlenka
podřídil	podřídit	k5eAaPmAgInS	podřídit
i	i	k9	i
své	svůj	k3xOyFgInPc4	svůj
kroky	krok	k1gInPc4	krok
ohledně	ohledně	k7c2	ohledně
korunovace	korunovace	k1gFnSc2	korunovace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1253	[number]	k4	1253
zemřel	zemřít	k5eAaPmAgMnS	zemřít
Václav	Václav	k1gMnSc1	Václav
I.	I.	kA	I.
a	a	k8xC	a
Přemysl	Přemysl	k1gMnSc1	Přemysl
se	se	k3xPyFc4	se
tak	tak	k9	tak
jako	jako	k9	jako
jediný	jediný	k2eAgMnSc1d1	jediný
žijící	žijící	k2eAgMnSc1d1	žijící
Přemyslovec	Přemyslovec	k1gMnSc1	Přemyslovec
stal	stát	k5eAaPmAgMnS	stát
jeho	jeho	k3xOp3gMnSc7	jeho
nástupcem	nástupce	k1gMnSc7	nástupce
-	-	kIx~	-
králem	král	k1gMnSc7	král
se	se	k3xPyFc4	se
korunovat	korunovat	k5eAaBmF	korunovat
dlouho	dlouho	k6eAd1	dlouho
nedal	dát	k5eNaPmAgMnS	dát
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
zahraniční	zahraniční	k2eAgFnPc1d1	zahraniční
kroniky	kronika	k1gFnPc1	kronika
o	o	k7c6	o
něm	on	k3xPp3gMnSc6	on
tak	tak	k6eAd1	tak
mluví	mluvit	k5eAaImIp3nS	mluvit
<g/>
.	.	kIx.	.
</s>
<s>
Přemysl	Přemysl	k1gMnSc1	Přemysl
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nechtěl	chtít	k5eNaImAgMnS	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
společně	společně	k6eAd1	společně
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
byla	být	k5eAaImAgFnS	být
korunovaná	korunovaný	k2eAgFnSc1d1	korunovaná
i	i	k9	i
Markéta	Markéta	k1gFnSc1	Markéta
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jejich	jejich	k3xOp3gFnSc1	jejich
rozluka	rozluka	k1gFnSc1	rozluka
by	by	kYmCp3nS	by
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
o	o	k7c4	o
dost	dost	k6eAd1	dost
komplikovanější	komplikovaný	k2eAgFnSc1d2	komplikovanější
<g/>
.	.	kIx.	.
</s>
<s>
Získat	získat	k5eAaPmF	získat
babenberské	babenberský	k2eAgNnSc4d1	babenberské
dědictví	dědictví	k1gNnSc4	dědictví
si	se	k3xPyFc3	se
však	však	k9	však
přál	přát	k5eAaImAgMnS	přát
také	také	k9	také
uherský	uherský	k2eAgMnSc1d1	uherský
král	král	k1gMnSc1	král
Béla	Béla	k1gMnSc1	Béla
IV	IV	kA	IV
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
i	i	k9	i
jiní	jiný	k2eAgMnPc1d1	jiný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
dlouholetým	dlouholetý	k2eAgInPc3d1	dlouholetý
sporům	spor	k1gInPc3	spor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1252	[number]	k4	1252
oženil	oženit	k5eAaPmAgMnS	oženit
Béla	Béla	k1gMnSc1	Béla
svého	své	k1gNnSc2	své
synovce	synovec	k1gMnSc2	synovec
Romana	Roman	k1gMnSc2	Roman
Haličského	haličský	k2eAgMnSc2d1	haličský
s	s	k7c7	s
Markétinou	Markétin	k2eAgFnSc7d1	Markétina
neteří	neteř	k1gFnSc7	neteř
a	a	k8xC	a
vdovou	vdova	k1gFnSc7	vdova
po	po	k7c6	po
českém	český	k2eAgMnSc6d1	český
princi	princ	k1gMnSc6	princ
Vladislavovi	Vladislav	k1gMnSc6	Vladislav
Gertrudou	Gertrudý	k2eAgFnSc4d1	Gertrudý
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
držitelkou	držitelka	k1gFnSc7	držitelka
výše	vysoce	k6eAd2	vysoce
zmíněného	zmíněný	k2eAgNnSc2d1	zmíněné
privilegia	privilegium	k1gNnSc2	privilegium
<g/>
.	.	kIx.	.
</s>
<s>
Uherský	uherský	k2eAgMnSc1d1	uherský
král	král	k1gMnSc1	král
byl	být	k5eAaImAgMnS	být
odhodlaný	odhodlaný	k2eAgMnSc1d1	odhodlaný
prosadit	prosadit	k5eAaPmF	prosadit
své	svůj	k3xOyFgInPc4	svůj
nároky	nárok	k1gInPc4	nárok
i	i	k8xC	i
silou	síla	k1gFnSc7	síla
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
dvou	dva	k4xCgInPc6	dva
letech	let	k1gInPc6	let
vtrhla	vtrhnout	k5eAaPmAgNnP	vtrhnout
uherská	uherský	k2eAgNnPc1d1	Uherské
vojska	vojsko	k1gNnPc1	vojsko
do	do	k7c2	do
Rakouska	Rakousko	k1gNnSc2	Rakousko
a	a	k8xC	a
pronikla	proniknout	k5eAaPmAgFnS	proniknout
i	i	k9	i
na	na	k7c4	na
Moravu	Morava	k1gFnSc4	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Přemysl	Přemysl	k1gMnSc1	Přemysl
byl	být	k5eAaImAgMnS	být
rozhodnutý	rozhodnutý	k2eAgMnSc1d1	rozhodnutý
toto	tento	k3xDgNnSc4	tento
počínání	počínání	k1gNnSc4	počínání
zastavit	zastavit	k5eAaPmF	zastavit
i	i	k9	i
za	za	k7c4	za
cenu	cena	k1gFnSc4	cena
ztráty	ztráta	k1gFnSc2	ztráta
části	část	k1gFnSc2	část
Štýrska	Štýrsko	k1gNnSc2	Štýrsko
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
uherského	uherský	k2eAgMnSc2d1	uherský
krále	král	k1gMnSc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Mírová	mírový	k2eAgFnSc1d1	mírová
smlouva	smlouva	k1gFnSc1	smlouva
byla	být	k5eAaImAgFnS	být
uzavřená	uzavřený	k2eAgFnSc1d1	uzavřená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1254	[number]	k4	1254
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
expanze	expanze	k1gFnSc2	expanze
na	na	k7c4	na
jih	jih	k1gInSc4	jih
se	se	k3xPyFc4	se
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
rozšířit	rozšířit	k5eAaPmF	rozšířit
své	svůj	k3xOyFgNnSc4	svůj
panství	panství	k1gNnSc4	panství
i	i	k8xC	i
směrem	směr	k1gInSc7	směr
severovýchodním	severovýchodní	k2eAgInSc7d1	severovýchodní
-	-	kIx~	-
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1254-55	[number]	k4	1254-55
(	(	kIx(	(
<g/>
a	a	k8xC	a
1267	[number]	k4	1267
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgMnS	účastnit
křížové	křížový	k2eAgFnSc2d1	křížová
výpravy	výprava	k1gFnSc2	výprava
do	do	k7c2	do
(	(	kIx(	(
<g/>
pobaltského	pobaltský	k2eAgNnSc2d1	pobaltské
<g/>
)	)	kIx)	)
Pruska	Prusko	k1gNnSc2	Prusko
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc2	jeho
snahy	snaha	k1gFnSc2	snaha
o	o	k7c6	o
nastolení	nastolení	k1gNnSc6	nastolení
přemyslovské	přemyslovský	k2eAgFnSc2d1	Přemyslovská
moci	moc	k1gFnSc2	moc
v	v	k7c6	v
Litvě	Litva	k1gFnSc6	Litva
a	a	k8xC	a
Pobaltí	Pobaltí	k1gNnSc6	Pobaltí
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
z	z	k7c2	z
olomouckého	olomoucký	k2eAgNnSc2d1	olomoucké
biskupství	biskupství	k1gNnSc2	biskupství
se	se	k3xPyFc4	se
mělo	mít	k5eAaImAgNnS	mít
stát	stát	k5eAaPmF	stát
arcibiskupství	arcibiskupství	k1gNnPc4	arcibiskupství
spravující	spravující	k2eAgNnPc4d1	spravující
dobytá	dobytý	k2eAgNnPc4d1	dobyté
území	území	k1gNnPc4	území
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejsou	být	k5eNaImIp3nP	být
úspěšné	úspěšný	k2eAgMnPc4d1	úspěšný
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
díky	díky	k7c3	díky
malé	malý	k2eAgFnSc3d1	malá
podpoře	podpora	k1gFnSc3	podpora
papežské	papežský	k2eAgFnSc2d1	Papežská
kurie	kurie	k1gFnSc2	kurie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodující	rozhodující	k2eAgFnSc1d1	rozhodující
bitva	bitva	k1gFnSc1	bitva
mezi	mezi	k7c7	mezi
pohanskými	pohanský	k2eAgMnPc7d1	pohanský
Prusy	Prus	k1gMnPc7	Prus
a	a	k8xC	a
vojsky	vojsky	k6eAd1	vojsky
Přemysla	Přemysl	k1gMnSc2	Přemysl
se	se	k3xPyFc4	se
odehrála	odehrát	k5eAaPmAgNnP	odehrát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1255	[number]	k4	1255
u	u	k7c2	u
Rudavy	Rudavy	k?	Rudavy
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
nechal	nechat	k5eAaPmAgMnS	nechat
založit	založit	k5eAaPmF	založit
město	město	k1gNnSc4	město
Královec	Královec	k1gInSc4	Královec
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Königsberg	Königsberg	k1gInSc1	Königsberg
<g/>
,	,	kIx,	,
dnešní	dnešní	k2eAgInSc1d1	dnešní
Kaliningrad	Kaliningrad	k1gInSc1	Kaliningrad
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1255	[number]	k4	1255
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
Otakar	Otakar	k1gMnSc1	Otakar
vrátil	vrátit	k5eAaPmAgMnS	vrátit
z	z	k7c2	z
pobaltské	pobaltský	k2eAgFnSc2d1	pobaltská
výpravy	výprava	k1gFnSc2	výprava
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rozhodovalo	rozhodovat	k5eAaImAgNnS	rozhodovat
o	o	k7c6	o
římskoněmecké	římskoněmecký	k2eAgFnSc6d1	římskoněmecká
královské	královský	k2eAgFnSc3d1	královská
koruně	koruna	k1gFnSc3	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Přemysl	Přemysl	k1gMnSc1	Přemysl
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
po	po	k7c6	po
matce	matka	k1gFnSc6	matka
pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
císařského	císařský	k2eAgInSc2d1	císařský
rodu	rod	k1gInSc2	rod
Štaufů	Štauf	k1gMnPc2	Štauf
<g/>
,	,	kIx,	,
z	z	k7c2	z
nejasných	jasný	k2eNgInPc2d1	nejasný
důvodů	důvod	k1gInPc2	důvod
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Snad	snad	k9	snad
viděl	vidět	k5eAaImAgMnS	vidět
do	do	k7c2	do
politické	politický	k2eAgFnSc2d1	politická
hry	hra	k1gFnSc2	hra
říšských	říšský	k2eAgMnPc2d1	říšský
knížat	kníže	k1gMnPc2wR	kníže
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
si	se	k3xPyFc3	se
zvykli	zvyknout	k5eAaPmAgMnP	zvyknout
na	na	k7c4	na
takřka	takřka	k6eAd1	takřka
nulovou	nulový	k2eAgFnSc4d1	nulová
moc	moc	k1gFnSc4	moc
římských	římský	k2eAgMnPc2d1	římský
králů	král	k1gMnPc2	král
a	a	k8xC	a
císařů	císař	k1gMnPc2	císař
<g/>
.	.	kIx.	.
</s>
<s>
Přemysl	Přemysl	k1gMnSc1	Přemysl
tedy	tedy	k9	tedy
pragmaticky	pragmaticky	k6eAd1	pragmaticky
podporoval	podporovat	k5eAaImAgInS	podporovat
oba	dva	k4xCgMnPc4	dva
nominální	nominální	k2eAgMnPc4d1	nominální
vládce	vládce	k1gMnPc4	vládce
<g/>
,	,	kIx,	,
Alfonse	Alfons	k1gMnSc2	Alfons
Kastilského	kastilský	k2eAgMnSc2d1	kastilský
i	i	k8xC	i
Richarda	Richard	k1gMnSc2	Richard
Cornwallského	Cornwallský	k2eAgMnSc2d1	Cornwallský
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
bez	bez	k7c2	bez
císařské	císařský	k2eAgFnSc2d1	císařská
koruny	koruna	k1gFnSc2	koruna
byl	být	k5eAaImAgMnS	být
nejmocnějším	mocný	k2eAgMnSc7d3	nejmocnější
mužem	muž	k1gMnSc7	muž
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
pevný	pevný	k2eAgInSc1d1	pevný
a	a	k8xC	a
rozlehlý	rozlehlý	k2eAgInSc1d1	rozlehlý
stát	stát	k1gInSc1	stát
působil	působit	k5eAaImAgInS	působit
jako	jako	k9	jako
protiklad	protiklad	k1gInSc1	protiklad
k	k	k7c3	k
okolním	okolní	k2eAgFnPc3d1	okolní
zemím	zem	k1gFnPc3	zem
<g/>
,	,	kIx,	,
Římské	římský	k2eAgFnSc3d1	římská
říši	říš	k1gFnSc3	říš
a	a	k8xC	a
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
rozdrobené	rozdrobený	k2eAgInPc1d1	rozdrobený
na	na	k7c4	na
řadu	řada	k1gFnSc4	řada
samostatných	samostatný	k2eAgNnPc2d1	samostatné
knížectví	knížectví	k1gNnPc2	knížectví
<g/>
,	,	kIx,	,
měst	město	k1gNnPc2	město
a	a	k8xC	a
údělů	úděl	k1gInPc2	úděl
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Kressenbrunnu	Kressenbrunn	k1gInSc2	Kressenbrunn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1258	[number]	k4	1258
<g/>
,	,	kIx,	,
po	po	k7c6	po
neúspěšném	úspěšný	k2eNgInSc6d1	neúspěšný
výpadu	výpad	k1gInSc6	výpad
do	do	k7c2	do
Bavorska	Bavorsko	k1gNnSc2	Bavorsko
<g/>
,	,	kIx,	,
Přemysl	Přemysl	k1gMnSc1	Přemysl
kapituloval	kapitulovat	k5eAaBmAgMnS	kapitulovat
před	před	k7c7	před
Jindřichem	Jindřich	k1gMnSc7	Jindřich
Bavorským	bavorský	k2eAgMnSc7d1	bavorský
<g/>
,	,	kIx,	,
zetěm	zeť	k1gMnSc7	zeť
a	a	k8xC	a
spojencem	spojenec	k1gMnSc7	spojenec
Bély	Béla	k1gMnSc2	Béla
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
a	a	k8xC	a
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
mír	mír	k1gInSc1	mír
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1260	[number]	k4	1260
byla	být	k5eAaImAgFnS	být
svedena	svést	k5eAaPmNgFnS	svést
i	i	k9	i
bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Kressenbrunnu	Kressenbrunn	k1gInSc2	Kressenbrunn
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
o	o	k7c4	o
poznání	poznání	k1gNnSc4	poznání
úspěšnější	úspěšný	k2eAgNnSc4d2	úspěšnější
než	než	k8xS	než
v	v	k7c6	v
boji	boj	k1gInSc6	boj
s	s	k7c7	s
bavorským	bavorský	k2eAgMnSc7d1	bavorský
vévodou	vévoda	k1gMnSc7	vévoda
a	a	k8xC	a
ukončil	ukončit	k5eAaPmAgInS	ukončit
tak	tak	k6eAd1	tak
letité	letitý	k2eAgInPc4d1	letitý
spory	spor	k1gInPc4	spor
o	o	k7c4	o
Štýrsko	Štýrsko	k1gNnSc4	Štýrsko
s	s	k7c7	s
Bélou	Béla	k1gMnSc7	Béla
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Otakar	Otakar	k1gMnSc1	Otakar
chtěl	chtít	k5eAaImAgMnS	chtít
ovšem	ovšem	k9	ovšem
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
jen	jen	k6eAd1	jen
získat	získat	k5eAaPmF	získat
území	území	k1gNnSc4	území
<g/>
,	,	kIx,	,
o	o	k7c4	o
které	který	k3yIgMnPc4	který
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
sňatku	sňatek	k1gInSc6	sňatek
s	s	k7c7	s
Markétou	Markéta	k1gFnSc7	Markéta
ochuzen	ochudit	k5eAaPmNgMnS	ochudit
-	-	kIx~	-
chtěl	chtít	k5eAaImAgMnS	chtít
si	se	k3xPyFc3	se
udělat	udělat	k5eAaPmF	udělat
z	z	k7c2	z
uherského	uherský	k2eAgMnSc2d1	uherský
krále	král	k1gMnSc2	král
spojence	spojenec	k1gMnSc2	spojenec
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
1260	[number]	k4	1260
se	se	k3xPyFc4	se
Přemysl	Přemysl	k1gMnSc1	Přemysl
pokusil	pokusit	k5eAaPmAgMnS	pokusit
vymoci	vymoct	k5eAaPmF	vymoct
na	na	k7c4	na
papeži	papež	k1gMnSc3	papež
Alexandrovi	Alexandrův	k2eAgMnPc1d1	Alexandrův
IV	IV	kA	IV
<g/>
.	.	kIx.	.
potvrzení	potvrzení	k1gNnSc4	potvrzení
o	o	k7c6	o
legitimním	legitimní	k2eAgInSc6d1	legitimní
původu	původ	k1gInSc6	původ
svých	svůj	k3xOyFgMnPc2	svůj
nemanželských	manželský	k2eNgMnPc2d1	nemanželský
potomků	potomek	k1gMnPc2	potomek
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
dětí	dítě	k1gFnPc2	dítě
s	s	k7c7	s
Anežkou	Anežka	k1gFnSc7	Anežka
z	z	k7c2	z
Kuenringu	Kuenring	k1gInSc2	Kuenring
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
stěží	stěží	k6eAd1	stěží
předpokládat	předpokládat	k5eAaImF	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
věřil	věřit	k5eAaImAgMnS	věřit
v	v	k7c4	v
úspěch	úspěch	k1gInSc4	úspěch
<g/>
,	,	kIx,	,
a	a	k8xC	a
papež	papež	k1gMnSc1	papež
zareagoval	zareagovat	k5eAaPmAgMnS	zareagovat
přesně	přesně	k6eAd1	přesně
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
dalo	dát	k5eAaPmAgNnS	dát
čekat	čekat	k5eAaImF	čekat
<g/>
.	.	kIx.	.
</s>
<s>
Nepotvrdil	potvrdit	k5eNaPmAgMnS	potvrdit
následnické	následnický	k2eAgNnSc4d1	následnické
právo	právo	k1gNnSc4	právo
jeho	on	k3xPp3gMnSc2	on
syna	syn	k1gMnSc2	syn
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
na	na	k7c4	na
český	český	k2eAgInSc4d1	český
trůn	trůn	k1gInSc4	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Přemysl	Přemysl	k1gMnSc1	Přemysl
tak	tak	k9	tak
ze	z	k7c2	z
svého	svůj	k3xOyFgInSc2	svůj
rozvodu	rozvod	k1gInSc2	rozvod
s	s	k7c7	s
Markétou	Markéta	k1gFnSc7	Markéta
Babenberskou	babenberský	k2eAgFnSc7d1	Babenberská
udělal	udělat	k5eAaPmAgMnS	udělat
věc	věc	k1gFnSc4	věc
politickou	politický	k2eAgFnSc4d1	politická
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
pohyboval	pohybovat	k5eAaImAgInS	pohybovat
ve	v	k7c6	v
vysoké	vysoký	k2eAgFnSc6d1	vysoká
politice	politika	k1gFnSc6	politika
už	už	k6eAd1	už
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
a	a	k8xC	a
mnohé	mnohý	k2eAgInPc4d1	mnohý
jeho	jeho	k3xOp3gInPc4	jeho
skutky	skutek	k1gInPc4	skutek
ze	z	k7c2	z
zralého	zralý	k2eAgNnSc2d1	zralé
období	období	k1gNnSc2	období
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
téměř	téměř	k6eAd1	téměř
ďábelskou	ďábelský	k2eAgFnSc7d1	ďábelská
promyšleností	promyšlenost	k1gFnSc7	promyšlenost
<g/>
.	.	kIx.	.
</s>
<s>
Dlužno	dlužno	k6eAd1	dlužno
ovšem	ovšem	k9	ovšem
dodat	dodat	k5eAaPmF	dodat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Markéta	Markéta	k1gFnSc1	Markéta
se	se	k3xPyFc4	se
rozvodu	rozvod	k1gInSc3	rozvod
nijak	nijak	k6eAd1	nijak
nebránila	bránit	k5eNaImAgFnS	bránit
-	-	kIx~	-
dožila	dožít	k5eAaPmAgFnS	dožít
ve	v	k7c6	v
velkorysém	velkorysý	k2eAgNnSc6d1	velkorysé
ústraní	ústraní	k1gNnSc6	ústraní
-	-	kIx~	-
a	a	k8xC	a
Přemysl	Přemysl	k1gMnSc1	Přemysl
se	se	k3xPyFc4	se
o	o	k7c4	o
své	svůj	k3xOyFgFnPc4	svůj
nemanželské	manželský	k2eNgFnPc4d1	nemanželská
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
snad	snad	k9	snad
i	i	k9	i
o	o	k7c4	o
Anežku	Anežka	k1gFnSc4	Anežka
postaral	postarat	k5eAaPmAgMnS	postarat
<g/>
.	.	kIx.	.
</s>
<s>
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
později	pozdě	k6eAd2	pozdě
získal	získat	k5eAaPmAgMnS	získat
Opavsko	Opavsko	k1gNnSc4	Opavsko
a	a	k8xC	a
dcery	dcera	k1gFnSc2	dcera
král	král	k1gMnSc1	král
provdal	provdat	k5eAaPmAgMnS	provdat
za	za	k7c4	za
přední	přední	k2eAgMnPc4d1	přední
české	český	k2eAgMnPc4d1	český
šlechtice	šlechtic	k1gMnPc4	šlechtic
<g/>
.	.	kIx.	.
</s>
<s>
Přemysl	Přemysl	k1gMnSc1	Přemysl
si	se	k3xPyFc3	se
tak	tak	k6eAd1	tak
uvolnil	uvolnit	k5eAaPmAgMnS	uvolnit
cestu	cesta	k1gFnSc4	cesta
k	k	k7c3	k
dalšímu	další	k2eAgInSc3d1	další
sňatku	sňatek	k1gInSc3	sňatek
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yIgInSc2	který
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgMnS	moct
narodit	narodit	k5eAaPmF	narodit
legitimní	legitimní	k2eAgMnSc1d1	legitimní
dědic	dědic	k1gMnSc1	dědic
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
požádal	požádat	k5eAaPmAgMnS	požádat
"	"	kIx"	"
<g/>
uchvácen	uchvácen	k2eAgMnSc1d1	uchvácen
její	její	k3xOp3gFnSc7	její
krásou	krása	k1gFnSc7	krása
<g/>
"	"	kIx"	"
o	o	k7c4	o
ruku	ruka	k1gFnSc4	ruka
Bélovu	Bélův	k2eAgFnSc4d1	Bélova
dceru	dcera	k1gFnSc4	dcera
Markétu	Markéta	k1gFnSc4	Markéta
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
dominikánská	dominikánský	k2eAgFnSc1d1	Dominikánská
jeptiška	jeptiška	k1gFnSc1	jeptiška
ovšem	ovšem	k9	ovšem
o	o	k7c4	o
sňatek	sňatek	k1gInSc4	sňatek
nejevila	jevit	k5eNaImAgFnS	jevit
zájem	zájem	k1gInSc4	zájem
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
požádala	požádat	k5eAaPmAgFnS	požádat
o	o	k7c6	o
"	"	kIx"	"
<g/>
zahalení	zahalení	k1gNnSc6	zahalení
svatým	svatý	k2eAgInSc7d1	svatý
závojem	závoj	k1gInSc7	závoj
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
plánovaného	plánovaný	k2eAgInSc2d1	plánovaný
sňatku	sňatek	k1gInSc2	sňatek
tudíž	tudíž	k8xC	tudíž
sešlo	sejít	k5eAaPmAgNnS	sejít
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dalších	další	k2eAgNnPc6d1	další
jednáních	jednání	k1gNnPc6	jednání
se	se	k3xPyFc4	se
Přemysl	Přemysl	k1gMnSc1	Přemysl
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Bélovou	Bélův	k2eAgFnSc7d1	Bélova
vnučkou	vnučka	k1gFnSc7	vnučka
Kunhutou	Kunhuta	k1gFnSc7	Kunhuta
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
tak	tak	k6eAd1	tak
byla	být	k5eAaImAgFnS	být
zpečetěna	zpečetěn	k2eAgFnSc1d1	zpečetěna
mírová	mírový	k2eAgFnSc1d1	mírová
smlouva	smlouva	k1gFnSc1	smlouva
mezi	mezi	k7c7	mezi
Otakarem	Otakar	k1gMnSc7	Otakar
a	a	k8xC	a
Bélou	Béla	k1gMnSc7	Béla
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
získal	získat	k5eAaPmAgMnS	získat
Dolní	dolní	k2eAgMnSc1d1	dolní
i	i	k8xC	i
Horní	horní	k2eAgInPc1d1	horní
Rakousy	Rakousy	k1gInPc1	Rakousy
a	a	k8xC	a
Štýrsko	Štýrsko	k1gNnSc1	Štýrsko
(	(	kIx(	(
<g/>
1251	[number]	k4	1251
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1266	[number]	k4	1266
připojil	připojit	k5eAaPmAgMnS	připojit
Chebsko	Chebsko	k1gNnSc4	Chebsko
-	-	kIx~	-
jako	jako	k8xC	jako
věno	věno	k1gNnSc1	věno
po	po	k7c6	po
matce	matka	k1gFnSc6	matka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1269	[number]	k4	1269
zdědil	zdědit	k5eAaPmAgInS	zdědit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
Poděbradské	poděbradský	k2eAgFnSc2d1	Poděbradská
smlouvy	smlouva	k1gFnSc2	smlouva
po	po	k7c6	po
bratranci	bratranec	k1gMnSc6	bratranec
Oldřichovi	Oldřich	k1gMnSc6	Oldřich
Korutansko	Korutansko	k1gNnSc4	Korutansko
a	a	k8xC	a
Kraňsko	Kraňsko	k1gNnSc4	Kraňsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1272	[number]	k4	1272
Aquileia	Aquileia	k1gFnSc1	Aquileia
jmenovala	jmenovat	k5eAaImAgFnS	jmenovat
Přemysla	Přemysl	k1gMnSc4	Přemysl
generálním	generální	k2eAgMnSc7d1	generální
vládcem	vládce	k1gMnSc7	vládce
své	svůj	k3xOyFgFnSc2	svůj
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc1	jeho
zeť	zeť	k1gMnSc1	zeť
poté	poté	k6eAd1	poté
dobyl	dobýt	k5eAaPmAgMnS	dobýt
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Cividale	Cividala	k1gFnSc3	Cividala
a	a	k8xC	a
král	král	k1gMnSc1	král
tak	tak	k6eAd1	tak
získal	získat	k5eAaPmAgMnS	získat
Furlánsko	Furlánsko	k1gNnSc4	Furlánsko
<g/>
.	.	kIx.	.
</s>
<s>
Otakar	Otakar	k1gMnSc1	Otakar
se	se	k3xPyFc4	se
ocitl	ocitnout	k5eAaPmAgMnS	ocitnout
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
potíže	potíž	k1gFnPc4	potíž
s	s	k7c7	s
novým	nový	k2eAgMnSc7d1	nový
uherským	uherský	k2eAgMnSc7d1	uherský
králem	král	k1gMnSc7	král
Štěpánem	Štěpán	k1gMnSc7	Štěpán
V.	V.	kA	V.
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
ale	ale	k8xC	ale
nenechaly	nechat	k5eNaPmAgFnP	nechat
dlouho	dlouho	k6eAd1	dlouho
čekat	čekat	k5eAaImF	čekat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
října	říjen	k1gInSc2	říjen
1270	[number]	k4	1270
se	se	k3xPyFc4	se
oba	dva	k4xCgMnPc1	dva
panovníci	panovník	k1gMnPc1	panovník
setkali	setkat	k5eAaPmAgMnP	setkat
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
a	a	k8xC	a
dohodli	dohodnout	k5eAaPmAgMnP	dohodnout
se	se	k3xPyFc4	se
na	na	k7c6	na
dvouletém	dvouletý	k2eAgInSc6d1	dvouletý
míru	mír	k1gInSc6	mír
<g/>
.	.	kIx.	.
</s>
<s>
Uherský	uherský	k2eAgMnSc1d1	uherský
král	král	k1gMnSc1	král
ale	ale	k8xC	ale
dohodu	dohoda	k1gFnSc4	dohoda
nedodržel	dodržet	k5eNaPmAgMnS	dodržet
a	a	k8xC	a
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
zaneprázdněný	zaneprázdněný	k2eAgInSc4d1	zaneprázdněný
problémy	problém	k1gInPc4	problém
v	v	k7c6	v
Korutanech	Korutany	k1gInPc6	Korutany
<g/>
,	,	kIx,	,
uherská	uherský	k2eAgNnPc4d1	Uherské
vojska	vojsko	k1gNnPc4	vojsko
podnikla	podniknout	k5eAaPmAgFnS	podniknout
pustošivé	pustošivý	k2eAgInPc4d1	pustošivý
vpády	vpád	k1gInPc4	vpád
do	do	k7c2	do
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
odvetné	odvetný	k2eAgFnSc6d1	odvetná
výpravě	výprava	k1gFnSc6	výprava
vojska	vojsko	k1gNnSc2	vojsko
Přemysla	Přemysl	k1gMnSc2	Přemysl
Otakara	Otakar	k1gMnSc2	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1271	[number]	k4	1271
vtrhla	vtrhnout	k5eAaPmAgFnS	vtrhnout
do	do	k7c2	do
Uherska	Uhersko	k1gNnSc2	Uhersko
<g/>
,	,	kIx,	,
přešla	přejít	k5eAaPmAgFnS	přejít
Dunaj	Dunaj	k1gInSc4	Dunaj
a	a	k8xC	a
obsadila	obsadit	k5eAaPmAgFnS	obsadit
Devín	Devín	k1gInSc4	Devín
<g/>
,	,	kIx,	,
Stupavu	Stupava	k1gFnSc4	Stupava
<g/>
,	,	kIx,	,
Svätý	Svätý	k2eAgInSc4d1	Svätý
Jur	jura	k1gFnPc2	jura
<g/>
,	,	kIx,	,
Nitru	Nitra	k1gFnSc4	Nitra
a	a	k8xC	a
Pezinok	Pezinok	k1gInSc4	Pezinok
<g/>
.	.	kIx.	.
</s>
<s>
Válka	válka	k1gFnSc1	válka
znovu	znovu	k6eAd1	znovu
skončila	skončit	k5eAaPmAgFnS	skončit
mírem	mír	k1gInSc7	mír
<g/>
,	,	kIx,	,
podepsaným	podepsaný	k1gMnSc7	podepsaný
2	[number]	k4	2
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1271	[number]	k4	1271
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
se	se	k3xPyFc4	se
dohodly	dohodnout	k5eAaPmAgFnP	dohodnout
na	na	k7c4	na
zachování	zachování	k1gNnSc4	zachování
současného	současný	k2eAgInSc2d1	současný
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Uherský	uherský	k2eAgMnSc1d1	uherský
král	král	k1gMnSc1	král
však	však	k9	však
ani	ani	k8xC	ani
tuto	tento	k3xDgFnSc4	tento
smlouvu	smlouva	k1gFnSc4	smlouva
nedodržel	dodržet	k5eNaPmAgMnS	dodržet
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1273	[number]	k4	1273
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
nová	nový	k2eAgFnSc1d1	nová
česko-uherská	českoherský	k2eAgFnSc1d1	česko-uherská
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
bylo	být	k5eAaImAgNnS	být
zpustošené	zpustošený	k2eAgNnSc4d1	zpustošené
jihozápadní	jihozápadní	k2eAgNnSc4d1	jihozápadní
Slovensko	Slovensko	k1gNnSc4	Slovensko
až	až	k9	až
po	po	k7c4	po
Nitru	Nitra	k1gFnSc4	Nitra
<g/>
.	.	kIx.	.
</s>
<s>
Uhersko	Uhersko	k1gNnSc1	Uhersko
<g/>
,	,	kIx,	,
oslabené	oslabený	k2eAgInPc1d1	oslabený
vnitřními	vnitřní	k2eAgInPc7d1	vnitřní
boji	boj	k1gInPc7	boj
oligarchů	oligarch	k1gMnPc2	oligarch
<g/>
,	,	kIx,	,
nebylo	být	k5eNaImAgNnS	být
schopné	schopný	k2eAgNnSc1d1	schopné
čelit	čelit	k5eAaImF	čelit
síle	síla	k1gFnSc3	síla
Přemyslových	Přemyslův	k2eAgNnPc2d1	Přemyslovo
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1273-6	[number]	k4	1273-6
byla	být	k5eAaImAgFnS	být
Bratislava	Bratislava	k1gFnSc1	Bratislava
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Otakarova	Otakarův	k2eAgFnSc1d1	Otakarova
vláda	vláda	k1gFnSc1	vláda
znamenala	znamenat	k5eAaImAgFnS	znamenat
zvýšení	zvýšení	k1gNnSc4	zvýšení
prestiže	prestiž	k1gFnSc2	prestiž
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
i	i	k8xC	i
jeho	on	k3xPp3gMnSc2	on
panovníka	panovník	k1gMnSc2	panovník
-	-	kIx~	-
Přemysl	Přemysl	k1gMnSc1	Přemysl
až	až	k9	až
neobyčejně	obyčejně	k6eNd1	obyčejně
posílil	posílit	k5eAaPmAgInS	posílit
své	svůj	k3xOyFgNnSc4	svůj
postavení	postavení	k1gNnSc4	postavení
uvnitř	uvnitř	k7c2	uvnitř
království	království	k1gNnSc2	království
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
Otakar	Otakar	k1gMnSc1	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
i	i	k8xC	i
Václav	Václav	k1gMnSc1	Václav
I.	I.	kA	I.
zcela	zcela	k6eAd1	zcela
ignorovali	ignorovat	k5eAaImAgMnP	ignorovat
tendenci	tendence	k1gFnSc4	tendence
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
šlechta	šlechta	k1gFnSc1	šlechta
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
žádala	žádat	k5eAaImAgFnS	žádat
podíl	podíl	k1gInSc4	podíl
na	na	k7c6	na
moci	moc	k1gFnSc6	moc
<g/>
.	.	kIx.	.
</s>
<s>
Přemysl	Přemysl	k1gMnSc1	Přemysl
své	svůj	k3xOyFgNnSc4	svůj
postavení	postavení	k1gNnSc4	postavení
opíral	opírat	k5eAaImAgMnS	opírat
především	především	k9	především
o	o	k7c4	o
nově	nově	k6eAd1	nově
zakládaná	zakládaný	k2eAgNnPc4d1	zakládané
města	město	k1gNnPc4	město
(	(	kIx(	(
<g/>
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
,	,	kIx,	,
Menší	malý	k2eAgNnSc1d2	menší
Město	město	k1gNnSc1	město
pražské	pražský	k2eAgNnSc1d1	Pražské
-	-	kIx~	-
Malá	malý	k2eAgFnSc1d1	malá
Strana	strana	k1gFnSc1	strana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
do	do	k7c2	do
nichž	jenž	k3xRgInPc2	jenž
zval	zvát	k5eAaImAgInS	zvát
německé	německý	k2eAgMnPc4d1	německý
kolonisty	kolonista	k1gMnPc4	kolonista
(	(	kIx(	(
<g/>
za	za	k7c2	za
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
přes	přes	k7c4	přes
padesát	padesát	k4xCc4	padesát
českých	český	k2eAgNnPc2d1	české
měst	město	k1gNnPc2	město
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
listině	listina	k1gFnSc6	listina
píše	psát	k5eAaImIp3nS	psát
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
království	království	k1gNnSc1	království
našeho	náš	k3xOp1gInSc2	náš
sláva	sláva	k1gFnSc1	sláva
<g/>
,	,	kIx,	,
kteréž	kteréž	k?	kteréž
žádosti	žádost	k1gFnPc1	žádost
jsme	být	k5eAaImIp1nP	být
<g/>
,	,	kIx,	,
nejvíce	nejvíce	k6eAd1	nejvíce
z	z	k7c2	z
okrasy	okrasa	k1gFnSc2	okrasa
a	a	k8xC	a
ozdoby	ozdoba	k1gFnSc2	ozdoba
měst	město	k1gNnPc2	město
roste	růst	k5eAaImIp3nS	růst
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
své	svůj	k3xOyFgFnSc2	svůj
vlády	vláda	k1gFnSc2	vláda
také	také	k9	také
zřídil	zřídit	k5eAaPmAgInS	zřídit
zemský	zemský	k2eAgInSc1d1	zemský
soud	soud	k1gInSc1	soud
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgInSc6	jenž
jsou	být	k5eAaImIp3nP	být
vedeny	vést	k5eAaImNgFnP	vést
zemské	zemský	k2eAgFnPc1d1	zemská
desky	deska	k1gFnPc1	deska
<g/>
.	.	kIx.	.
</s>
<s>
Bohatla	bohatnout	k5eAaImAgFnS	bohatnout
a	a	k8xC	a
sílila	sílit	k5eAaImAgFnS	sílit
také	také	k9	také
šlechta	šlechta	k1gFnSc1	šlechta
<g/>
.	.	kIx.	.
</s>
<s>
Mocné	mocný	k2eAgInPc1d1	mocný
rody	rod	k1gInPc1	rod
přebíraly	přebírat	k5eAaImAgInP	přebírat
do	do	k7c2	do
dědičné	dědičný	k2eAgFnSc2d1	dědičná
držby	držba	k1gFnSc2	držba
staré	starý	k2eAgInPc1d1	starý
knížecí	knížecí	k2eAgInPc1d1	knížecí
hrady	hrad	k1gInPc1	hrad
i	i	k8xC	i
statky	statek	k1gInPc1	statek
a	a	k8xC	a
rády	rád	k2eAgFnPc1d1	ráda
zapomínaly	zapomínat	k5eAaImAgFnP	zapomínat
<g/>
,	,	kIx,	,
že	že	k8xS	že
byly	být	k5eAaImAgFnP	být
jejich	jejich	k3xOp3gFnPc1	jejich
předkům	předek	k1gMnPc3	předek
svěřeny	svěřit	k5eAaPmNgFnP	svěřit
pouze	pouze	k6eAd1	pouze
do	do	k7c2	do
"	"	kIx"	"
<g/>
knížecí	knížecí	k2eAgFnSc2d1	knížecí
vůle	vůle	k1gFnSc2	vůle
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Přemysl	Přemysl	k1gMnSc1	Přemysl
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
jeho	jeho	k3xOp3gNnSc4	jeho
předchůdci	předchůdce	k1gMnSc3	předchůdce
považoval	považovat	k5eAaImAgMnS	považovat
stát	stát	k5eAaImF	stát
za	za	k7c4	za
své	svůj	k3xOyFgNnSc4	svůj
vlastnictví	vlastnictví	k1gNnSc4	vlastnictví
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
různá	různý	k2eAgNnPc4d1	různé
panství	panství	k1gNnPc4	panství
patřila	patřit	k5eAaImAgFnS	patřit
šlechtě	šlechta	k1gFnSc3	šlechta
a	a	k8xC	a
panovníkovi	panovník	k1gMnSc3	panovník
patřila	patřit	k5eAaImAgFnS	patřit
jen	jen	k6eAd1	jen
královská	královský	k2eAgNnPc4d1	královské
města	město	k1gNnPc4	město
a	a	k8xC	a
malá	malý	k2eAgNnPc4d1	malé
území	území	k1gNnPc4	území
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zakládání	zakládání	k1gNnSc6	zakládání
měst	město	k1gNnPc2	město
musel	muset	k5eAaImAgMnS	muset
Přemysl	Přemysl	k1gMnSc1	Přemysl
složitě	složitě	k6eAd1	složitě
získat	získat	k5eAaPmF	získat
statky	statek	k1gInPc4	statek
zpět	zpět	k6eAd1	zpět
od	od	k7c2	od
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
.	.	kIx.	.
</s>
<s>
Omezování	omezování	k1gNnSc1	omezování
svých	svůj	k3xOyFgFnPc2	svůj
dosavadních	dosavadní	k2eAgFnPc2d1	dosavadní
výsad	výsada	k1gFnPc2	výsada
<g/>
,	,	kIx,	,
prosazování	prosazování	k1gNnSc4	prosazování
západního	západní	k2eAgNnSc2d1	západní
práva	právo	k1gNnSc2	právo
a	a	k8xC	a
pevnou	pevný	k2eAgFnSc4d1	pevná
královskou	královský	k2eAgFnSc4d1	královská
vládu	vláda	k1gFnSc4	vláda
nesla	nést	k5eAaImAgFnS	nést
šlechta	šlechta	k1gFnSc1	šlechta
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
i	i	k8xC	i
rakouských	rakouský	k2eAgFnPc6d1	rakouská
zemích	zem	k1gFnPc6	zem
velmi	velmi	k6eAd1	velmi
těžce	těžce	k6eAd1	těžce
<g/>
.	.	kIx.	.
</s>
<s>
Svojí	svůj	k3xOyFgFnSc7	svůj
politikou	politika	k1gFnSc7	politika
si	se	k3xPyFc3	se
král	král	k1gMnSc1	král
znepřátelil	znepřátelit	k5eAaPmAgMnS	znepřátelit
především	především	k6eAd1	především
rozvětvený	rozvětvený	k2eAgInSc4d1	rozvětvený
jihočeský	jihočeský	k2eAgInSc4d1	jihočeský
rod	rod	k1gInSc4	rod
Vítkovců	Vítkovec	k1gMnPc2	Vítkovec
<g/>
.	.	kIx.	.
</s>
<s>
Zlom	zlom	k1gInSc1	zlom
v	v	k7c6	v
životě	život	k1gInSc6	život
panovníka	panovník	k1gMnSc2	panovník
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgNnSc4	jenž
dílo	dílo	k1gNnSc4	dílo
přes	přes	k7c4	přes
všechny	všechen	k3xTgInPc4	všechen
dílčí	dílčí	k2eAgInPc4d1	dílčí
neúspěchy	neúspěch	k1gInPc4	neúspěch
stále	stále	k6eAd1	stále
rostlo	růst	k5eAaImAgNnS	růst
<g/>
,	,	kIx,	,
přišel	přijít	k5eAaPmAgMnS	přijít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1273	[number]	k4	1273
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
Richard	Richard	k1gMnSc1	Richard
Cornwallský	Cornwallský	k2eAgMnSc1d1	Cornwallský
znehybněl	znehybnět	k5eAaPmAgInS	znehybnět
po	po	k7c6	po
záchvatu	záchvat	k1gInSc6	záchvat
mrtvice	mrtvice	k1gFnSc2	mrtvice
a	a	k8xC	a
rok	rok	k1gInSc4	rok
nato	nato	k6eAd1	nato
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
třeba	třeba	k6eAd1	třeba
najít	najít	k5eAaPmF	najít
nového	nový	k2eAgMnSc4d1	nový
římského	římský	k2eAgMnSc4d1	římský
krále	král	k1gMnSc4	král
<g/>
.	.	kIx.	.
</s>
<s>
Jednohlasně	jednohlasně	k6eAd1	jednohlasně
jím	jíst	k5eAaImIp1nS	jíst
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
takřka	takřka	k6eAd1	takřka
neznámý	známý	k2eNgMnSc1d1	neznámý
Rudolf	Rudolf	k1gMnSc1	Rudolf
Habsburský	habsburský	k2eAgMnSc1d1	habsburský
<g/>
.	.	kIx.	.
</s>
<s>
Římští	římský	k2eAgMnPc1d1	římský
kurfiřti	kurfiřt	k1gMnPc1	kurfiřt
nechtěli	chtít	k5eNaImAgMnP	chtít
za	za	k7c4	za
krále	král	k1gMnSc4	král
mocného	mocný	k2eAgMnSc2d1	mocný
Přemysla	Přemysl	k1gMnSc2	Přemysl
a	a	k8xC	a
považovali	považovat	k5eAaImAgMnP	považovat
Rudolfa	Rudolf	k1gMnSc4	Rudolf
za	za	k7c4	za
slabého	slabý	k2eAgMnSc4d1	slabý
<g/>
.	.	kIx.	.
</s>
<s>
Netušili	tušit	k5eNaImAgMnP	tušit
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
hluboce	hluboko	k6eAd1	hluboko
se	se	k3xPyFc4	se
mýlí	mýlit	k5eAaImIp3nP	mýlit
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Bitva	bitva	k1gFnSc1	bitva
na	na	k7c6	na
Moravském	moravský	k2eAgNnSc6d1	Moravské
poli	pole	k1gNnSc6	pole
<g/>
.	.	kIx.	.
</s>
<s>
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Rudolfovu	Rudolfův	k2eAgFnSc4d1	Rudolfova
volbu	volba	k1gFnSc4	volba
neuznal	uznat	k5eNaPmAgMnS	uznat
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
nebyl	být	k5eNaImAgInS	být
k	k	k7c3	k
volbě	volba	k1gFnSc3	volba
přizván	přizván	k2eAgInSc1d1	přizván
a	a	k8xC	a
český	český	k2eAgInSc1d1	český
hlas	hlas	k1gInSc1	hlas
byl	být	k5eAaImAgInS	být
nahrazen	nahradit	k5eAaPmNgInS	nahradit
hlasem	hlas	k1gInSc7	hlas
bavorským	bavorský	k2eAgInSc7d1	bavorský
<g/>
.	.	kIx.	.
</s>
<s>
Špatně	špatně	k6eAd1	špatně
odhadl	odhadnout	k5eAaPmAgInS	odhadnout
situaci	situace	k1gFnSc4	situace
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
směřovala	směřovat	k5eAaImAgFnS	směřovat
k	k	k7c3	k
rozbití	rozbití	k1gNnSc3	rozbití
jeho	jeho	k3xOp3gNnSc2	jeho
panství	panství	k1gNnSc2	panství
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
začátku	začátek	k1gInSc2	začátek
především	především	k6eAd1	především
podcenil	podcenit	k5eAaPmAgMnS	podcenit
ctižádost	ctižádost	k1gFnSc4	ctižádost
a	a	k8xC	a
schopnosti	schopnost	k1gFnPc4	schopnost
prvního	první	k4xOgInSc2	první
známého	známý	k2eAgInSc2d1	známý
Habsburka	Habsburk	k1gInSc2	Habsburk
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
19	[number]	k4	19
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1273	[number]	k4	1273
Rudolf	Rudolf	k1gMnSc1	Rudolf
vydal	vydat	k5eAaPmAgMnS	vydat
prohlášení	prohlášení	k1gNnSc4	prohlášení
<g/>
,	,	kIx,	,
že	že	k8xS	že
všechna	všechen	k3xTgNnPc1	všechen
léna	léno	k1gNnPc1	léno
a	a	k8xC	a
majetkové	majetkový	k2eAgFnPc1d1	majetková
změny	změna	k1gFnPc1	změna
uskutečněné	uskutečněný	k2eAgFnPc1d1	uskutečněná
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1245	[number]	k4	1245
podléhají	podléhat	k5eAaImIp3nP	podléhat
novému	nový	k2eAgNnSc3d1	nové
potvrzení	potvrzení	k1gNnSc3	potvrzení
<g/>
.	.	kIx.	.
</s>
<s>
Přemysl	Přemysl	k1gMnSc1	Přemysl
se	se	k3xPyFc4	se
musel	muset	k5eAaImAgMnS	muset
oprávněně	oprávněně	k6eAd1	oprávněně
obávat	obávat	k5eAaImF	obávat
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
bude	být	k5eAaImBp3nS	být
své	své	k1gNnSc1	své
země	zem	k1gFnSc2	zem
od	od	k7c2	od
římského	římský	k2eAgMnSc2d1	římský
krále	král	k1gMnSc2	král
přijímat	přijímat	k5eAaImF	přijímat
v	v	k7c4	v
léno	léno	k1gNnSc4	léno
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
už	už	k9	už
by	by	kYmCp3nS	by
mu	on	k3xPp3gMnSc3	on
nemusel	muset	k5eNaImAgMnS	muset
rakouské	rakouský	k2eAgFnSc2d1	rakouská
země	zem	k1gFnSc2	zem
vrátit	vrátit	k5eAaPmF	vrátit
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
těch	ten	k3xDgNnPc2	ten
českých	český	k2eAgNnPc2d1	české
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
výsledku	výsledek	k1gInSc3	výsledek
říšské	říšský	k2eAgFnSc2d1	říšská
volby	volba	k1gFnSc2	volba
protestoval	protestovat	k5eAaBmAgMnS	protestovat
i	i	k9	i
u	u	k7c2	u
papeže	papež	k1gMnSc2	papež
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
ho	on	k3xPp3gNnSc4	on
ovšem	ovšem	k9	ovšem
uvalil	uvalit	k5eAaPmAgInS	uvalit
do	do	k7c2	do
klatby	klatba	k1gFnSc2	klatba
-	-	kIx~	-
Rudolf	Rudolf	k1gMnSc1	Rudolf
Habsburský	habsburský	k2eAgMnSc1d1	habsburský
mu	on	k3xPp3gMnSc3	on
totiž	totiž	k9	totiž
přislíbil	přislíbit	k5eAaPmAgInS	přislíbit
účast	účast	k1gFnSc4	účast
na	na	k7c6	na
křížové	křížový	k2eAgFnSc6d1	křížová
výpravě	výprava	k1gFnSc6	výprava
<g/>
.	.	kIx.	.
</s>
<s>
Habsburk	Habsburk	k1gInSc1	Habsburk
začal	začít	k5eAaPmAgInS	začít
bezprávně	bezprávně	k6eAd1	bezprávně
zasahovat	zasahovat	k5eAaImF	zasahovat
do	do	k7c2	do
záležitostí	záležitost	k1gFnPc2	záležitost
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
a	a	k8xC	a
podporovat	podporovat	k5eAaImF	podporovat
proti	proti	k7c3	proti
králi	král	k1gMnSc3	král
odbojnou	odbojný	k2eAgFnSc4d1	odbojná
šlechtu	šlechta	k1gFnSc4	šlechta
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Přemysla	Přemysl	k1gMnSc2	Přemysl
nakonec	nakonec	k6eAd1	nakonec
odpadl	odpadnout	k5eAaPmAgInS	odpadnout
další	další	k2eAgInSc1d1	další
ze	z	k7c2	z
spojenců	spojenec	k1gMnPc2	spojenec
<g/>
,	,	kIx,	,
bavorský	bavorský	k2eAgMnSc1d1	bavorský
vévoda	vévoda	k1gMnSc1	vévoda
Jindřich	Jindřich	k1gMnSc1	Jindřich
<g/>
,	,	kIx,	,
kterému	který	k3yQgNnSc3	který
Habsburk	Habsburk	k1gMnSc1	Habsburk
promptně	promptně	k6eAd1	promptně
přislíbil	přislíbit	k5eAaPmAgMnS	přislíbit
ruku	ruka	k1gFnSc4	ruka
své	svůj	k3xOyFgFnSc2	svůj
dcery	dcera	k1gFnSc2	dcera
<g/>
,	,	kIx,	,
a	a	k8xC	a
umožnil	umožnit	k5eAaPmAgMnS	umožnit
Rudolfovu	Rudolfův	k2eAgNnSc3d1	Rudolfovo
říšskému	říšský	k2eAgNnSc3d1	říšské
vojsku	vojsko	k1gNnSc3	vojsko
průchod	průchod	k1gInSc4	průchod
svým	svůj	k3xOyFgNnSc7	svůj
územím	území	k1gNnSc7	území
a	a	k8xC	a
následný	následný	k2eAgInSc1d1	následný
vpád	vpád	k1gInSc1	vpád
do	do	k7c2	do
Rakous	Rakousy	k1gInPc2	Rakousy
<g/>
.	.	kIx.	.
</s>
<s>
Vítkovci	Vítkovec	k1gMnPc1	Vítkovec
(	(	kIx(	(
<g/>
vedoucí	vedoucí	k2eAgFnSc1d1	vedoucí
úloha	úloha	k1gFnSc1	úloha
Záviše	Záviš	k1gMnSc2	Záviš
z	z	k7c2	z
Falkenštejna	Falkenštejn	k1gInSc2	Falkenštejn
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
pozdější	pozdní	k2eAgInSc4d2	pozdější
literární	literární	k2eAgInSc4d1	literární
výmysl	výmysl	k1gInSc4	výmysl
<g/>
)	)	kIx)	)
a	a	k8xC	a
Rýzmburkové	Rýzmburkové	k2eAgFnSc7d1	Rýzmburkové
(	(	kIx(	(
<g/>
hlavou	hlava	k1gFnSc7	hlava
Boreš	Boreš	k1gFnSc2	Boreš
z	z	k7c2	z
Rýzmburka	Rýzmburka	k1gFnSc1	Rýzmburka
<g/>
)	)	kIx)	)
zahájili	zahájit	k5eAaPmAgMnP	zahájit
pečlivě	pečlivě	k6eAd1	pečlivě
načasovanou	načasovaný	k2eAgFnSc4d1	načasovaná
vzpouru	vzpoura	k1gFnSc4	vzpoura
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
Přemysla	Přemysl	k1gMnSc4	Přemysl
roku	rok	k1gInSc2	rok
1276	[number]	k4	1276
donutila	donutit	k5eAaPmAgFnS	donutit
uzavřít	uzavřít	k5eAaPmF	uzavřít
s	s	k7c7	s
Rudolfem	Rudolf	k1gMnSc7	Rudolf
Vídeňský	vídeňský	k2eAgInSc1d1	vídeňský
mír	mír	k1gInSc4	mír
a	a	k8xC	a
vzdát	vzdát	k5eAaPmF	vzdát
se	se	k3xPyFc4	se
alpských	alpský	k2eAgFnPc2d1	alpská
zemí	zem	k1gFnPc2	zem
a	a	k8xC	a
Chebska	Chebsko	k1gNnSc2	Chebsko
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Přemysla	Přemysl	k1gMnSc4	Přemysl
a	a	k8xC	a
český	český	k2eAgInSc4d1	český
stát	stát	k1gInSc4	stát
to	ten	k3xDgNnSc1	ten
sice	sice	k8xC	sice
znamenalo	znamenat	k5eAaImAgNnS	znamenat
ztrátu	ztráta	k1gFnSc4	ztráta
dosavadních	dosavadní	k2eAgInPc2d1	dosavadní
územních	územní	k2eAgInPc2d1	územní
zisků	zisk	k1gInPc2	zisk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
českému	český	k2eAgMnSc3d1	český
králi	král	k1gMnSc3	král
byla	být	k5eAaImAgFnS	být
formou	forma	k1gFnSc7	forma
svatební	svatební	k2eAgFnSc2d1	svatební
zástavy	zástava	k1gFnSc2	zástava
(	(	kIx(	(
<g/>
jelikož	jelikož	k8xS	jelikož
Přemyslův	Přemyslův	k2eAgMnSc1d1	Přemyslův
syn	syn	k1gMnSc1	syn
Václav	Václav	k1gMnSc1	Václav
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgMnS	mít
oženit	oženit	k5eAaPmF	oženit
s	s	k7c7	s
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
Rudolfových	Rudolfových	k2eAgFnPc2d1	Rudolfových
dcer	dcera	k1gFnPc2	dcera
<g/>
)	)	kIx)	)
zajištěna	zajištěn	k2eAgFnSc1d1	zajištěna
jiná	jiný	k2eAgNnPc4d1	jiné
rozsáhlá	rozsáhlý	k2eAgNnPc4d1	rozsáhlé
území	území	k1gNnPc4	území
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
se	se	k3xPyFc4	se
ocitl	ocitnout	k5eAaPmAgMnS	ocitnout
v	v	k7c6	v
nemilé	milý	k2eNgFnSc6d1	nemilá
situaci	situace	k1gFnSc6	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ho	on	k3xPp3gMnSc4	on
Rudolf	Rudolf	k1gMnSc1	Rudolf
tlačil	tlačit	k5eAaImAgMnS	tlačit
od	od	k7c2	od
ústupku	ústupek	k1gInSc2	ústupek
k	k	k7c3	k
ústupku	ústupek	k1gInSc3	ústupek
<g/>
.	.	kIx.	.
</s>
<s>
Římský	římský	k2eAgMnSc1d1	římský
král	král	k1gMnSc1	král
ztrácel	ztrácet	k5eAaImAgMnS	ztrácet
sympatie	sympatie	k1gFnPc4	sympatie
říšských	říšský	k2eAgMnPc2d1	říšský
pánů	pan	k1gMnPc2	pan
i	i	k8xC	i
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
,	,	kIx,	,
osvobozeném	osvobozený	k2eAgInSc6d1	osvobozený
od	od	k7c2	od
Přemyslovy	Přemyslův	k2eAgFnSc2d1	Přemyslova
"	"	kIx"	"
<g/>
tyranie	tyranie	k1gFnSc2	tyranie
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
sevřel	sevřít	k5eAaPmAgMnS	sevřít
v	v	k7c6	v
kleštích	kleště	k1gFnPc6	kleště
daní	daň	k1gFnPc2	daň
<g/>
.	.	kIx.	.
</s>
<s>
Otakar	Otakar	k1gMnSc1	Otakar
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
stav	stav	k1gInSc1	stav
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
řešit	řešit	k5eAaImF	řešit
bitvou	bitva	k1gFnSc7	bitva
na	na	k7c6	na
Moravském	moravský	k2eAgNnSc6d1	Moravské
poli	pole	k1gNnSc6	pole
(	(	kIx(	(
<g/>
1278	[number]	k4	1278
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
změnila	změnit	k5eAaPmAgFnS	změnit
v	v	k7c4	v
politickou	politický	k2eAgFnSc4d1	politická
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
osobní	osobní	k2eAgFnSc4d1	osobní
katastrofu	katastrofa	k1gFnSc4	katastrofa
<g/>
.	.	kIx.	.
</s>
<s>
Soupeřova	soupeřův	k2eAgFnSc1d1	soupeřova
smrt	smrt	k1gFnSc1	smrt
pro	pro	k7c4	pro
Habsburka	Habsburk	k1gMnSc4	Habsburk
cenu	cena	k1gFnSc4	cena
vítězství	vítězství	k1gNnSc3	vítězství
ještě	ještě	k6eAd1	ještě
znásobila	znásobit	k5eAaPmAgFnS	znásobit
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stal	stát	k5eAaPmAgInS	stát
jediným	jediný	k2eAgMnSc7d1	jediný
hegemonem	hegemon	k1gMnSc7	hegemon
střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Přemyslův	Přemyslův	k2eAgInSc1d1	Přemyslův
konec	konec	k1gInSc1	konec
<g/>
,	,	kIx,	,
příběh	příběh	k1gInSc1	příběh
tolik	tolik	k6eAd1	tolik
oblíbený	oblíbený	k2eAgInSc1d1	oblíbený
mezi	mezi	k7c7	mezi
minnesangry	minnesangr	k1gInPc7	minnesangr
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
především	především	k9	především
svědectvím	svědectví	k1gNnSc7	svědectví
o	o	k7c6	o
nihilismu	nihilismus	k1gInSc6	nihilismus
dějin	dějiny	k1gFnPc2	dějiny
a	a	k8xC	a
realitě	realita	k1gFnSc3	realita
jeho	jeho	k3xOp3gFnSc2	jeho
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Šlechta	šlechta	k1gFnSc1	šlechta
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
bojovala	bojovat	k5eAaImAgFnS	bojovat
proti	proti	k7c3	proti
panovníkovi	panovník	k1gMnSc3	panovník
kvůli	kvůli	k7c3	kvůli
podílu	podíl	k1gInSc3	podíl
na	na	k7c6	na
moci	moc	k1gFnSc6	moc
a	a	k8xC	a
majetku	majetek	k1gInSc6	majetek
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
Vítkovci	Vítkovec	k1gMnPc1	Vítkovec
nedlouho	dlouho	k6eNd1	dlouho
po	po	k7c6	po
králově	králův	k2eAgFnSc6d1	králova
smrti	smrt	k1gFnSc6	smrt
znovu	znovu	k6eAd1	znovu
útočili	útočit	k5eAaImAgMnP	útočit
na	na	k7c4	na
České	český	k2eAgInPc4d1	český
Budějovice	Budějovice	k1gInPc4	Budějovice
(	(	kIx(	(
<g/>
tentokrát	tentokrát	k6eAd1	tentokrát
se	se	k3xPyFc4	se
už	už	k6eAd1	už
účastnil	účastnit	k5eAaImAgInS	účastnit
i	i	k9	i
Falkenštejn	Falkenštejn	k1gInSc1	Falkenštejn
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1281	[number]	k4	1281
svolala	svolat	k5eAaPmAgFnS	svolat
šlechta	šlechta	k1gFnSc1	šlechta
zemský	zemský	k2eAgInSc4d1	zemský
sněm	sněm	k1gInSc4	sněm
jako	jako	k8xS	jako
protiváhu	protiváha	k1gFnSc4	protiváha
královské	královský	k2eAgFnSc2d1	královská
moci	moc	k1gFnSc2	moc
v	v	k7c6	v
otřesené	otřesený	k2eAgFnSc6d1	otřesená
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Čechy	Čechy	k1gFnPc4	Čechy
plenila	plenit	k5eAaImAgFnS	plenit
vojska	vojsko	k1gNnPc4	vojsko
Oty	Ota	k1gMnSc2	Ota
Braniborského	braniborský	k2eAgMnSc2d1	braniborský
<g/>
,	,	kIx,	,
poručníka	poručník	k1gMnSc2	poručník
mladého	mladý	k2eAgMnSc2d1	mladý
kralevice	kralevic	k1gMnSc2	kralevic
Václava	Václav	k1gMnSc2	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
a	a	k8xC	a
Moravu	Morava	k1gFnSc4	Morava
obsadil	obsadit	k5eAaPmAgMnS	obsadit
Rudolf	Rudolf	k1gMnSc1	Rudolf
Habsburský	habsburský	k2eAgMnSc1d1	habsburský
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1281	[number]	k4	1281
vypukl	vypuknout	k5eAaPmAgInS	vypuknout
hladomor	hladomor	k1gInSc1	hladomor
a	a	k8xC	a
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
byla	být	k5eAaImAgFnS	být
taková	takový	k3xDgFnSc1	takový
bída	bída	k1gFnSc1	bída
<g/>
,	,	kIx,	,
že	že	k8xS	že
podle	podle	k7c2	podle
letopisce	letopisec	k1gMnSc2	letopisec
Petra	Petr	k1gMnSc2	Petr
Žitavského	žitavský	k2eAgMnSc2d1	žitavský
i	i	k8xC	i
dříve	dříve	k6eAd2	dříve
majetní	majetný	k2eAgMnPc1d1	majetný
řemeslníci	řemeslník	k1gMnPc1	řemeslník
museli	muset	k5eAaImAgMnP	muset
nyní	nyní	k6eAd1	nyní
žebrat	žebrat	k5eAaImF	žebrat
<g/>
.	.	kIx.	.
</s>
<s>
Ostatky	ostatek	k1gInPc1	ostatek
pátého	pátý	k4xOgMnSc2	pátý
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
byly	být	k5eAaImAgFnP	být
nejprve	nejprve	k6eAd1	nejprve
veřejně	veřejně	k6eAd1	veřejně
vystaveny	vystavit	k5eAaPmNgInP	vystavit
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
byly	být	k5eAaImAgFnP	být
po	po	k7c6	po
uplynutí	uplynutí	k1gNnSc6	uplynutí
papežské	papežský	k2eAgFnSc2d1	Papežská
klatby	klatba	k1gFnSc2	klatba
uloženy	uložen	k2eAgInPc4d1	uložen
v	v	k7c6	v
minoritském	minoritský	k2eAgInSc6d1	minoritský
klášteře	klášter	k1gInSc6	klášter
ve	v	k7c6	v
Znojmě	Znojmo	k1gNnSc6	Znojmo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1296	[number]	k4	1296
je	být	k5eAaImIp3nS	být
nechal	nechat	k5eAaPmAgInS	nechat
Václav	Václav	k1gMnSc1	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
přenést	přenést	k5eAaPmF	přenést
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
a	a	k8xC	a
pohřbít	pohřbít	k5eAaPmF	pohřbít
v	v	k7c6	v
Anežském	anežský	k2eAgInSc6d1	anežský
klášteře	klášter	k1gInSc6	klášter
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měli	mít	k5eAaImAgMnP	mít
hrob	hrob	k1gInSc4	hrob
i	i	k9	i
Přemyslovi	Přemyslův	k2eAgMnPc1d1	Přemyslův
rodiče	rodič	k1gMnPc1	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
měly	mít	k5eAaImAgFnP	mít
odpočívat	odpočívat	k5eAaImF	odpočívat
jen	jen	k9	jen
dočasně	dočasně	k6eAd1	dočasně
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
chystal	chystat	k5eAaImAgMnS	chystat
vybudovat	vybudovat	k5eAaPmF	vybudovat
novou	nový	k2eAgFnSc4d1	nová
nekropoli	nekropole	k1gFnSc4	nekropole
svého	svůj	k3xOyFgInSc2	svůj
rodu	rod	k1gInSc2	rod
v	v	k7c6	v
cisterciáckém	cisterciácký	k2eAgInSc6d1	cisterciácký
klášteře	klášter	k1gInSc6	klášter
na	na	k7c6	na
Zbraslavi	Zbraslav	k1gFnSc6	Zbraslav
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
sám	sám	k3xTgMnSc1	sám
založil	založit	k5eAaPmAgMnS	založit
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
ovšem	ovšem	k9	ovšem
ještě	ještě	k6eAd1	ještě
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
tento	tento	k3xDgInSc1	tento
úmysl	úmysl	k1gInSc1	úmysl
dokončil	dokončit	k5eAaPmAgInS	dokončit
<g/>
.	.	kIx.	.
</s>
<s>
Přemyslovy	Přemyslův	k2eAgInPc1d1	Přemyslův
ostatky	ostatek	k1gInPc1	ostatek
byly	být	k5eAaImAgInP	být
přemístěny	přemístit	k5eAaPmNgInP	přemístit
ještě	ještě	k6eAd1	ještě
jednou	jeden	k4xCgFnSc7	jeden
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
příkaz	příkaz	k1gInSc4	příkaz
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
ctitele	ctitel	k1gMnSc4	ctitel
odkazu	odkaz	k1gInSc2	odkaz
svých	svůj	k3xOyFgMnPc2	svůj
přemyslovských	přemyslovský	k2eAgMnPc2d1	přemyslovský
předků	předek	k1gMnPc2	předek
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
byly	být	k5eAaImAgInP	být
uloženy	uložit	k5eAaPmNgInP	uložit
v	v	k7c6	v
chrámu	chrám	k1gInSc6	chrám
sv.	sv.	kA	sv.
Víta	Vít	k1gMnSc2	Vít
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
(	(	kIx(	(
<g/>
1373	[number]	k4	1373
<g/>
)	)	kIx)	)
pod	pod	k7c7	pod
skvostným	skvostný	k2eAgInSc7d1	skvostný
náhrobkem	náhrobek	k1gInSc7	náhrobek
od	od	k7c2	od
Petra	Petr	k1gMnSc2	Petr
Parléře	Parléř	k1gMnSc2	Parléř
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ostatky	ostatek	k1gInPc7	ostatek
krále	král	k1gMnSc2	král
byly	být	k5eAaImAgFnP	být
do	do	k7c2	do
hrobky	hrobka	k1gFnSc2	hrobka
uloženy	uložen	k2eAgInPc1d1	uložen
i	i	k8xC	i
pohřební	pohřební	k2eAgInPc1d1	pohřební
klenoty	klenot	k1gInPc1	klenot
(	(	kIx(	(
<g/>
koruna	koruna	k1gFnSc1	koruna
<g/>
,	,	kIx,	,
jablko	jablko	k1gNnSc1	jablko
<g/>
,	,	kIx,	,
žezlo	žezlo	k1gNnSc1	žezlo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pohřební	pohřební	k2eAgFnSc6d1	pohřební
koruně	koruna	k1gFnSc6	koruna
Otakara	Otakar	k1gMnSc2	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
dochoval	dochovat	k5eAaPmAgInS	dochovat
nápis	nápis	k1gInSc1	nápis
"	"	kIx"	"
<g/>
Hic	Hic	k?	Hic
sunt	sunt	k5eAaPmF	sunt
ossa	ossa	k6eAd1	ossa
Otakari	Otakare	k1gFnSc4	Otakare
incliti	inclit	k5eAaPmF	inclit
<g/>
,	,	kIx,	,
regis	regis	k1gInSc1	regis
Bohemiae	Bohemia	k1gFnSc2	Bohemia
quinti	quinť	k1gFnSc2	quinť
<g/>
"	"	kIx"	"
neboli	neboli	k8xC	neboli
"	"	kIx"	"
<g/>
Toto	tento	k3xDgNnSc1	tento
jsou	být	k5eAaImIp3nP	být
kosti	kost	k1gFnPc1	kost
Otakara	Otakar	k1gMnSc2	Otakar
vznešeného	vznešený	k2eAgMnSc2d1	vznešený
<g/>
,	,	kIx,	,
pátého	pátý	k4xOgNnSc2	pátý
krále	král	k1gMnSc2	král
českého	český	k2eAgMnSc2d1	český
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Taková	takový	k3xDgFnSc1	takový
identifikace	identifikace	k1gFnSc1	identifikace
nebyla	být	k5eNaImAgFnS	být
příliš	příliš	k6eAd1	příliš
obvyklá	obvyklý	k2eAgFnSc1d1	obvyklá
a	a	k8xC	a
zřejmě	zřejmě	k6eAd1	zřejmě
byla	být	k5eAaImAgFnS	být
dílem	dílem	k6eAd1	dílem
ještě	ještě	k9	ještě
Václava	Václava	k1gFnSc1	Václava
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Pravost	pravost	k1gFnSc1	pravost
ostatků	ostatek	k1gInPc2	ostatek
je	být	k5eAaImIp3nS	být
ostatně	ostatně	k6eAd1	ostatně
doložena	doložit	k5eAaPmNgFnS	doložit
ještě	ještě	k9	ještě
jednou	jeden	k4xCgFnSc7	jeden
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
sdělení	sdělení	k1gNnSc4	sdělení
na	na	k7c6	na
olověné	olověný	k2eAgFnSc6d1	olověná
destičce	destička	k1gFnSc6	destička
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
vložena	vložit	k5eAaPmNgFnS	vložit
do	do	k7c2	do
truhlice	truhlice	k1gFnSc2	truhlice
s	s	k7c7	s
pozůstatky	pozůstatek	k1gInPc7	pozůstatek
krále	král	k1gMnSc2	král
při	při	k7c6	při
jejich	jejich	k3xOp3gInSc6	jejich
přenosu	přenos	k1gInSc6	přenos
do	do	k7c2	do
nového	nový	k2eAgInSc2d1	nový
hrobu	hrob	k1gInSc2	hrob
v	v	k7c6	v
gotické	gotický	k2eAgFnSc6d1	gotická
katedrále	katedrála	k1gFnSc6	katedrála
<g/>
:	:	kIx,	:
Pod	pod	k7c7	pod
náhrobkem	náhrobek	k1gInSc7	náhrobek
tvořeným	tvořený	k2eAgInSc7d1	tvořený
ležící	ležící	k2eAgFnSc7d1	ležící
sochou	socha	k1gFnSc7	socha
ve	v	k7c6	v
zbroji	zbroj	k1gFnSc6	zbroj
těžkooděnce	těžkooděnec	k1gMnSc2	těžkooděnec
<g/>
,	,	kIx,	,
královském	královský	k2eAgNnSc6d1	královské
rouchu	roucho	k1gNnSc6	roucho
a	a	k8xC	a
s	s	k7c7	s
korunou	koruna	k1gFnSc7	koruna
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Přemyslovy	Přemyslův	k2eAgFnPc1d1	Přemyslova
kosti	kost	k1gFnPc1	kost
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
špatně	špatně	k6eAd1	špatně
zachovalé	zachovalý	k2eAgFnPc1d1	zachovalá
<g/>
,	,	kIx,	,
nacházejí	nacházet	k5eAaImIp3nP	nacházet
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Smrt	smrt	k1gFnSc1	smrt
Přemysla	Přemysl	k1gMnSc2	Přemysl
Otakara	Otakar	k1gMnSc2	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
silnou	silný	k2eAgFnSc4d1	silná
literární	literární	k2eAgFnSc4d1	literární
odezvu	odezva	k1gFnSc4	odezva
<g/>
,	,	kIx,	,
dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInSc1	jeho
vzlet	vzlet	k1gInSc1	vzlet
a	a	k8xC	a
pád	pád	k1gInSc1	pád
fascinuje	fascinovat	k5eAaBmIp3nS	fascinovat
umělce	umělec	k1gMnSc4	umělec
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
úspěchy	úspěch	k1gInPc1	úspěch
i	i	k8xC	i
neúspěchy	neúspěch	k1gInPc1	neúspěch
inspirovaly	inspirovat	k5eAaBmAgFnP	inspirovat
Franze	Franze	k1gFnPc1	Franze
Grillparzera	Grillparzero	k1gNnSc2	Grillparzero
k	k	k7c3	k
rakouskému	rakouský	k2eAgInSc3d1	rakouský
"	"	kIx"	"
<g/>
národnímu	národní	k2eAgNnSc3d1	národní
dramatu	drama	k1gNnSc3	drama
<g/>
"	"	kIx"	"
König	König	k1gInSc4	König
Ottokars	Ottokarsa	k1gFnPc2	Ottokarsa
Glück	Glücka	k1gFnPc2	Glücka
und	und	k?	und
Ende	Ende	k1gNnSc4	Ende
a	a	k8xC	a
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
i	i	k9	i
v	v	k7c4	v
Danteho	Dante	k1gMnSc4	Dante
Božské	božská	k1gFnSc2	božská
komedii	komedie	k1gFnSc4	komedie
-	-	kIx~	-
před	před	k7c7	před
branami	brána	k1gFnPc7	brána
očistce	očistec	k1gInSc2	očistec
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
Rudolfa	Rudolf	k1gMnSc2	Rudolf
Habsburského	habsburský	k2eAgMnSc2d1	habsburský
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
mimochodem	mimochodem	k9	mimochodem
velká	velký	k2eAgFnSc1d1	velká
čest	čest	k1gFnSc1	čest
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Dante	Dante	k1gMnSc1	Dante
se	se	k3xPyFc4	se
o	o	k7c6	o
světských	světský	k2eAgMnPc6d1	světský
panovnících	panovník	k1gMnPc6	panovník
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
jen	jen	k6eAd1	jen
zřídka	zřídka	k6eAd1	zřídka
<g/>
.	.	kIx.	.
</s>
<s>
Španělský	španělský	k2eAgMnSc1d1	španělský
dramatik	dramatik	k1gMnSc1	dramatik
a	a	k8xC	a
básník	básník	k1gMnSc1	básník
Lope	Lop	k1gFnSc2	Lop
de	de	k?	de
Vega	Vega	k1gMnSc1	Vega
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
zabýval	zabývat	k5eAaImAgInS	zabývat
domácími	domácí	k2eAgFnPc7d1	domácí
dějinami	dějiny	k1gFnPc7	dějiny
<g/>
,	,	kIx,	,
českému	český	k2eAgMnSc3d1	český
králi	král	k1gMnSc3	král
věnoval	věnovat	k5eAaPmAgMnS	věnovat
hru	hra	k1gFnSc4	hra
Císařská	císařský	k2eAgFnSc1d1	císařská
koruna	koruna	k1gFnSc1	koruna
Otakarova	Otakarův	k2eAgFnSc1d1	Otakarova
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
českých	český	k2eAgMnPc2d1	český
klasiků	klasik	k1gMnPc2	klasik
se	se	k3xPyFc4	se
situaci	situace	k1gFnSc3	situace
po	po	k7c6	po
Přemyslově	Přemyslův	k2eAgFnSc6d1	Přemyslova
smrti	smrt	k1gFnSc6	smrt
věnoval	věnovat	k5eAaPmAgMnS	věnovat
Bedřich	Bedřich	k1gMnSc1	Bedřich
Smetana	Smetana	k1gMnSc1	Smetana
v	v	k7c6	v
opeře	opera	k1gFnSc6	opera
Braniboři	Branibor	k1gMnPc1	Branibor
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgFnSc7d1	hlavní
postavou	postava	k1gFnSc7	postava
knihy	kniha	k1gFnSc2	kniha
Ludmily	Ludmila	k1gFnSc2	Ludmila
Vaňkové	Vaňková	k1gFnSc2	Vaňková
Král	Král	k1gMnSc1	Král
železný	železný	k2eAgMnSc1d1	železný
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
zlatý	zlatý	k1gInSc1	zlatý
<g/>
.	.	kIx.	.
</s>
<s>
Obdobím	období	k1gNnSc7	období
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
zabývá	zabývat	k5eAaImIp3nS	zabývat
její	její	k3xOp3gFnSc1	její
další	další	k2eAgFnSc1d1	další
kniha	kniha	k1gFnSc1	kniha
Zlá	zlá	k1gFnSc1	zlá
léta	léto	k1gNnSc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgNnPc2	některý
měst	město	k1gNnPc2	město
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gNnPc4	jejich
spojení	spojení	k1gNnPc4	spojení
se	s	k7c7	s
zakladatelskou	zakladatelský	k2eAgFnSc7d1	zakladatelská
činností	činnost	k1gFnSc7	činnost
Přemysla	Přemysl	k1gMnSc2	Přemysl
Otakara	Otakar	k1gMnSc2	Otakar
s	s	k7c7	s
otazníkem	otazník	k1gInSc7	otazník
a	a	k8xC	a
odvozuje	odvozovat	k5eAaImIp3nS	odvozovat
se	se	k3xPyFc4	se
přes	přes	k7c4	přes
druhotné	druhotný	k2eAgInPc4d1	druhotný
zdroje	zdroj	k1gInPc4	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnosti	podrobnost	k1gFnPc1	podrobnost
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Čechy	Čechy	k1gFnPc4	Čechy
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Beroun	Beroun	k1gInSc1	Beroun
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Čáslav	Čáslav	k1gFnSc1	Čáslav
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Děčín	Děčín	k1gInSc1	Děčín
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Domažlice	Domažlice	k1gFnPc1	Domažlice
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Dvůr	Dvůr	k1gInSc1	Dvůr
Králové	Králová	k1gFnSc2	Králová
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Hluboká	Hluboká	k1gFnSc1	Hluboká
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Hostinné	hostinný	k2eAgNnSc4d1	Hostinné
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Hostivice	Hostivice	k1gFnSc1	Hostivice
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
Chotěboř	Chotěboř	k1gFnSc1	Chotěboř
11	[number]	k4	11
<g/>
.	.	kIx.	.
</s>
<s>
Chrudim	Chrudim	k1gFnSc1	Chrudim
12	[number]	k4	12
<g/>
.	.	kIx.	.
</s>
<s>
Jaroměř	Jaroměř	k1gFnSc1	Jaroměř
13	[number]	k4	13
<g/>
.	.	kIx.	.
</s>
<s>
Jevíčko	Jevíčko	k1gNnSc1	Jevíčko
14	[number]	k4	14
<g/>
.	.	kIx.	.
</s>
<s>
Kadaň	Kadaň	k1gFnSc1	Kadaň
15	[number]	k4	15
<g/>
.	.	kIx.	.
</s>
<s>
Klatovy	Klatovy	k1gInPc4	Klatovy
16	[number]	k4	16
<g/>
.	.	kIx.	.
</s>
<s>
Kolín	Kolín	k1gInSc1	Kolín
17	[number]	k4	17
<g/>
.	.	kIx.	.
</s>
<s>
Kostelec	Kostelec	k1gInSc1	Kostelec
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
18	[number]	k4	18
<g/>
.	.	kIx.	.
</s>
<s>
Kouřim	Kouřim	k1gFnSc1	Kouřim
19	[number]	k4	19
<g/>
.	.	kIx.	.
</s>
<s>
Kuřivody-Ralsko	Kuřivody-Ralsko	k6eAd1	Kuřivody-Ralsko
20	[number]	k4	20
<g/>
.	.	kIx.	.
</s>
<s>
Kutná	kutný	k2eAgFnSc1d1	Kutná
Hora	hora	k1gFnSc1	hora
21	[number]	k4	21
<g/>
.	.	kIx.	.
</s>
<s>
Loket	loket	k1gInSc1	loket
nad	nad	k7c7	nad
Ohří	Ohře	k1gFnSc7	Ohře
22	[number]	k4	22
<g/>
.	.	kIx.	.
</s>
<s>
Louny	Louny	k1gInPc4	Louny
23	[number]	k4	23
<g/>
.	.	kIx.	.
</s>
<s>
Mělník	Mělník	k1gInSc1	Mělník
24	[number]	k4	24
<g/>
.	.	kIx.	.
</s>
<s>
Městec	Městec	k1gInSc1	Městec
Králové	Králová	k1gFnSc2	Králová
25	[number]	k4	25
<g/>
.	.	kIx.	.
</s>
<s>
Most	most	k1gInSc1	most
26	[number]	k4	26
<g/>
.	.	kIx.	.
</s>
<s>
Nymburk	Nymburk	k1gInSc1	Nymburk
27	[number]	k4	27
<g/>
.	.	kIx.	.
</s>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
28	[number]	k4	28
<g/>
.	.	kIx.	.
</s>
<s>
Písek	Písek	k1gInSc1	Písek
29	[number]	k4	29
<g/>
.	.	kIx.	.
</s>
<s>
Poděbrady	Poděbrady	k1gInPc4	Poděbrady
30	[number]	k4	30
<g/>
.	.	kIx.	.
</s>
<s>
Polička	Polička	k1gFnSc1	Polička
31	[number]	k4	31
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
Malá	malý	k2eAgFnSc1d1	malá
Strana	strana	k1gFnSc1	strana
32	[number]	k4	32
<g/>
.	.	kIx.	.
</s>
<s>
Starý	starý	k2eAgMnSc1d1	starý
Plzenec	Plzenec	k1gMnSc1	Plzenec
33	[number]	k4	33
<g/>
.	.	kIx.	.
</s>
<s>
Stříbro	stříbro	k1gNnSc1	stříbro
34	[number]	k4	34
<g/>
.	.	kIx.	.
</s>
<s>
Tachov	Tachov	k1gInSc1	Tachov
35	[number]	k4	35
<g/>
.	.	kIx.	.
</s>
<s>
Trutnov	Trutnov	k1gInSc1	Trutnov
36	[number]	k4	36
<g/>
.	.	kIx.	.
</s>
<s>
Ústí	ústí	k1gNnSc1	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
37	[number]	k4	37
<g/>
.	.	kIx.	.
</s>
<s>
Vodňany	Vodňan	k1gMnPc4	Vodňan
38	[number]	k4	38
<g/>
.	.	kIx.	.
</s>
<s>
Vysoké	vysoký	k2eAgNnSc1d1	vysoké
Mýto	mýto	k1gNnSc1	mýto
39	[number]	k4	39
<g/>
.	.	kIx.	.
</s>
<s>
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
Koruna	koruna	k1gFnSc1	koruna
40	[number]	k4	40
<g/>
.	.	kIx.	.
</s>
<s>
Žatec	Žatec	k1gInSc1	Žatec
Morava	Morava	k1gFnSc1	Morava
41	[number]	k4	41
<g/>
.	.	kIx.	.
</s>
<s>
Litovel	Litovel	k1gFnSc1	Litovel
42	[number]	k4	42
<g/>
.	.	kIx.	.
</s>
<s>
Moravský	moravský	k2eAgInSc1d1	moravský
Krumlov	Krumlov	k1gInSc1	Krumlov
43	[number]	k4	43
<g/>
.	.	kIx.	.
</s>
<s>
Přerov	Přerov	k1gInSc1	Přerov
44	[number]	k4	44
<g/>
.	.	kIx.	.
</s>
<s>
Uherské	uherský	k2eAgNnSc1d1	Uherské
Hradiště	Hradiště	k1gNnSc1	Hradiště
45	[number]	k4	45
<g/>
.	.	kIx.	.
</s>
<s>
Uherský	uherský	k2eAgInSc1d1	uherský
Brod	Brod	k1gInSc1	Brod
Slezsko	Slezsko	k1gNnSc1	Slezsko
46	[number]	k4	46
<g/>
.	.	kIx.	.
</s>
<s>
Hlučín	Hlučín	k1gInSc1	Hlučín
Štýrsko	Štýrsko	k1gNnSc1	Štýrsko
(	(	kIx(	(
<g/>
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
)	)	kIx)	)
47	[number]	k4	47
<g/>
.	.	kIx.	.
</s>
<s>
Bruck	Bruck	k1gInSc1	Bruck
an	an	k?	an
der	drát	k5eAaImRp2nS	drát
Mur	mur	k1gInSc4	mur
48	[number]	k4	48
<g/>
.	.	kIx.	.
</s>
<s>
Leoben	Leoben	k2eAgInSc1d1	Leoben
49	[number]	k4	49
<g/>
.	.	kIx.	.
</s>
<s>
Radkersburg	Radkersburg	k1gInSc1	Radkersburg
Dolní	dolní	k2eAgInPc1d1	dolní
Rakousy	Rakousy	k1gInPc1	Rakousy
(	(	kIx(	(
<g/>
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
)	)	kIx)	)
50	[number]	k4	50
<g/>
.	.	kIx.	.
</s>
<s>
Marchegg	Marchegg	k1gInSc1	Marchegg
Rusko	Rusko	k1gNnSc1	Rusko
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Prusko	Prusko	k1gNnSc1	Prusko
<g/>
)	)	kIx)	)
51	[number]	k4	51
<g/>
.	.	kIx.	.
</s>
<s>
Kaliningrad	Kaliningrad	k1gInSc1	Kaliningrad
-	-	kIx~	-
Královec	Královec	k1gInSc1	Královec
Polsko	Polsko	k1gNnSc1	Polsko
52	[number]	k4	52
<g/>
.	.	kIx.	.
</s>
<s>
Kladsko	Kladsko	k1gNnSc1	Kladsko
Sasko	Sasko	k1gNnSc1	Sasko
(	(	kIx(	(
<g/>
Německo	Německo	k1gNnSc1	Německo
<g/>
)	)	kIx)	)
53	[number]	k4	53
<g/>
.	.	kIx.	.
</s>
<s>
Zittau	Zittau	k5eAaPmIp1nS	Zittau
-	-	kIx~	-
Žitava	Žitava	k1gFnSc1	Žitava
Manželství	manželství	k1gNnSc2	manželství
s	s	k7c7	s
Markétou	Markéta	k1gFnSc7	Markéta
Babenberskou	babenberský	k2eAgFnSc7d1	Babenberská
(	(	kIx(	(
<g/>
1204	[number]	k4	1204
<g/>
/	/	kIx~	/
<g/>
1205	[number]	k4	1205
<g/>
-	-	kIx~	-
<g/>
1266	[number]	k4	1266
<g/>
)	)	kIx)	)
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
dle	dle	k7c2	dle
očekávání	očekávání	k1gNnPc2	očekávání
bezdětné	bezdětný	k2eAgFnPc1d1	bezdětná
<g/>
;	;	kIx,	;
Přemysl	Přemysl	k1gMnSc1	Přemysl
měl	mít	k5eAaImAgMnS	mít
několik	několik	k4yIc4	několik
(	(	kIx(	(
<g/>
zřejmě	zřejmě	k6eAd1	zřejmě
šest	šest	k4xCc1	šest
<g/>
)	)	kIx)	)
dětí	dítě	k1gFnPc2	dítě
s	s	k7c7	s
Kunhutou	Kunhuta	k1gFnSc7	Kunhuta
Uherskou	uherský	k2eAgFnSc7d1	uherská
(	(	kIx(	(
<g/>
1246	[number]	k4	1246
<g/>
-	-	kIx~	-
<g/>
1285	[number]	k4	1285
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dospělosti	dospělost	k1gFnPc1	dospělost
se	se	k3xPyFc4	se
dožily	dožít	k5eAaPmAgFnP	dožít
tři	tři	k4xCgFnPc1	tři
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
<g/>
:	:	kIx,	:
Kunhuta	Kunhuta	k1gFnSc1	Kunhuta
Přemyslovna	Přemyslovna	k1gFnSc1	Přemyslovna
(	(	kIx(	(
<g/>
1265	[number]	k4	1265
<g/>
-	-	kIx~	-
<g/>
1321	[number]	k4	1321
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mazovská	mazovský	k2eAgFnSc1d1	mazovský
kněžna	kněžna	k1gFnSc1	kněžna
a	a	k8xC	a
abatyše	abatyše	k1gFnSc1	abatyše
u	u	k7c2	u
sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc1	Jiří
∞	∞	k?	∞
1291	[number]	k4	1291
Boleslav	Boleslav	k1gMnSc1	Boleslav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Mazovský	Mazovský	k1gMnSc1	Mazovský
Anežka	Anežka	k1gFnSc1	Anežka
Přemyslovna	Přemyslovna	k1gFnSc1	Přemyslovna
(	(	kIx(	(
<g/>
1269	[number]	k4	1269
<g/>
-	-	kIx~	-
<g/>
1296	[number]	k4	1296
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rakouská	rakouský	k2eAgFnSc1d1	rakouská
vévodkyně	vévodkyně	k1gFnSc1	vévodkyně
∞	∞	k?	∞
1285	[number]	k4	1285
Rudolf	Rudolfa	k1gFnPc2	Rudolfa
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Švábský	švábský	k2eAgMnSc1d1	švábský
<g/>
)	)	kIx)	)
Václav	Václav	k1gMnSc1	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1271	[number]	k4	1271
<g/>
-	-	kIx~	-
<g/>
1305	[number]	k4	1305
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
a	a	k8xC	a
polský	polský	k2eAgMnSc1d1	polský
král	král	k1gMnSc1	král
∞	∞	k?	∞
1285	[number]	k4	1285
Guta	Gut	k2eAgFnSc1d1	Guta
Habsburská	habsburský	k2eAgFnSc1d1	habsburská
∞	∞	k?	∞
1300	[number]	k4	1300
Eliška	Eliška	k1gFnSc1	Eliška
Rejčka	Rejčka	k1gMnSc1	Rejčka
Přemysl	Přemysl	k1gMnSc1	Přemysl
měl	mít	k5eAaImAgMnS	mít
dva	dva	k4xCgInPc4	dva
nemanželské	manželský	k2eNgInPc4d1	nemanželský
syny	syn	k1gMnPc7	syn
a	a	k8xC	a
několik	několik	k4yIc4	několik
dcer	dcera	k1gFnPc2	dcera
<g/>
:	:	kIx,	:
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Opavský	opavský	k2eAgMnSc1d1	opavský
(	(	kIx(	(
<g/>
1255	[number]	k4	1255
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
-	-	kIx~	-
<g/>
1318	[number]	k4	1318
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vévoda	vévoda	k1gMnSc1	vévoda
opavský	opavský	k2eAgMnSc1d1	opavský
Anežka	Anežka	k1gFnSc1	Anežka
(	(	kIx(	(
<g/>
před	před	k7c7	před
1260	[number]	k4	1260
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
manžel	manžel	k1gMnSc1	manžel
Bavor	Bavor	k1gMnSc1	Bavor
II	II	kA	II
<g/>
.	.	kIx.	.
ze	z	k7c2	z
Strakonic	Strakonice	k1gFnPc2	Strakonice
Eliška	Eliška	k1gFnSc1	Eliška
(	(	kIx(	(
<g/>
před	před	k7c7	před
1260	[number]	k4	1260
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
manželé	manžel	k1gMnPc1	manžel
Oldřich	Oldřich	k1gMnSc1	Oldřich
z	z	k7c2	z
Drnholce	Drnholec	k1gInSc2	Drnholec
(	(	kIx(	(
<g/>
†	†	k?	†
1274	[number]	k4	1274
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jindřich	Jindřich	k1gMnSc1	Jindřich
z	z	k7c2	z
Kuenringu	Kuenring	k1gInSc2	Kuenring
(	(	kIx(	(
<g/>
†	†	k?	†
1281	[number]	k4	1281
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vikart	Vikart	k1gInSc1	Vikart
z	z	k7c2	z
Polné	Polná	k1gFnSc2	Polná
dcera	dcera	k1gFnSc1	dcera
<g/>
,	,	kIx,	,
manžel	manžel	k1gMnSc1	manžel
Vok	Vok	k1gMnSc1	Vok
z	z	k7c2	z
Kravař	kravař	k1gMnSc1	kravař
Jan	Jan	k1gMnSc1	Jan
<g/>
,	,	kIx,	,
probošt	probošt	k1gMnSc1	probošt
vyšehradský	vyšehradský	k2eAgMnSc1d1	vyšehradský
(	(	kIx(	(
<g/>
†	†	k?	†
1296	[number]	k4	1296
<g/>
)	)	kIx)	)
</s>
