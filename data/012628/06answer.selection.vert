<s>
Atacama	Atacamum	k1gNnPc1	Atacamum
<g/>
,	,	kIx,	,
zvaná	zvaný	k2eAgNnPc1d1	zvané
také	také	k9	také
Puna	Pun	k1gMnSc2	Pun
de	de	k?	de
Atacama	Atacam	k1gMnSc2	Atacam
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nejsušší	suchý	k2eAgFnSc1d3	nejsušší
poušť	poušť	k1gFnSc1	poušť
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
