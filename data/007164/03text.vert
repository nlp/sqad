<s>
Krtek	krtek	k1gMnSc1	krtek
je	být	k5eAaImIp3nS	být
české	český	k2eAgNnSc4d1	české
jméno	jméno	k1gNnSc4	jméno
pro	pro	k7c4	pro
13	[number]	k4	13
rodů	rod	k1gInPc2	rod
malých	malý	k2eAgMnPc2d1	malý
savců	savec	k1gMnPc2	savec
<g/>
,	,	kIx,	,
hmyzožravců	hmyzožravec	k1gMnPc2	hmyzožravec
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
krtkovitých	krtkovitý	k2eAgFnPc2d1	krtkovitá
(	(	kIx(	(
<g/>
Talpidae	Talpidae	k1gFnPc2	Talpidae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gMnSc7	jejich
nejznámějším	známý	k2eAgMnSc7d3	nejznámější
zástupcem	zástupce	k1gMnSc7	zástupce
je	být	k5eAaImIp3nS	být
krtek	krtek	k1gMnSc1	krtek
obecný	obecný	k2eAgMnSc1d1	obecný
<g/>
,	,	kIx,	,
žijící	žijící	k2eAgMnSc1d1	žijící
i	i	k9	i
na	na	k7c6	na
území	území	k1gNnSc6	území
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
zvířátko	zvířátko	k1gNnSc1	zvířátko
inspirovalo	inspirovat	k5eAaBmAgNnS	inspirovat
výtvarníka	výtvarník	k1gMnSc4	výtvarník
Zdeňka	Zdeněk	k1gMnSc4	Zdeněk
Milera	Miler	k1gMnSc4	Miler
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
animované	animovaný	k2eAgFnSc2d1	animovaná
postavičky	postavička	k1gFnSc2	postavička
Krtečka	krteček	k1gMnSc2	krteček
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
Krtka	krtek	k1gMnSc2	krtek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Krtci	krtek	k1gMnPc1	krtek
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
podzemních	podzemní	k2eAgFnPc6d1	podzemní
chodbách	chodba	k1gFnPc6	chodba
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
si	se	k3xPyFc3	se
vyhrabávají	vyhrabávat	k5eAaImIp3nP	vyhrabávat
velkými	velký	k2eAgFnPc7d1	velká
tlapami	tlapa	k1gFnPc7	tlapa
<g/>
.	.	kIx.	.
</s>
<s>
Krtek	krtek	k1gMnSc1	krtek
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nS	živit
bezobratlými	bezobratlý	k2eAgInPc7d1	bezobratlý
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
vyhledává	vyhledávat	k5eAaImIp3nS	vyhledávat
pomocí	pomocí	k7c2	pomocí
sluchu	sluch	k1gInSc2	sluch
a	a	k8xC	a
čichu	čich	k1gInSc2	čich
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k6eAd1	především
žere	žrát	k5eAaImIp3nS	žrát
žížaly	žížala	k1gFnPc4	žížala
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
loví	lovit	k5eAaImIp3nP	lovit
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
chodbách	chodba	k1gFnPc6	chodba
<g/>
.	.	kIx.	.
rod	rod	k1gInSc4	rod
<g/>
:	:	kIx,	:
Condylura	Condylur	k1gMnSc2	Condylur
krtek	krtek	k1gMnSc1	krtek
hvězdonosý	hvězdonosý	k2eAgMnSc1d1	hvězdonosý
(	(	kIx(	(
<g/>
Condylura	Condylura	k1gFnSc1	Condylura
cristata	cristata	k1gFnSc1	cristata
<g/>
)	)	kIx)	)
rod	rod	k1gInSc1	rod
<g/>
:	:	kIx,	:
Euroscaptor	Euroscaptor	k1gInSc1	Euroscaptor
Euroscaptor	Euroscaptor	k1gInSc1	Euroscaptor
mizura	mizura	k1gFnSc1	mizura
Euroscaptor	Euroscaptor	k1gMnSc1	Euroscaptor
parvidens	parvidens	k6eAd1	parvidens
krtek	krtek	k1gMnSc1	krtek
dlouhonosý	dlouhonosý	k2eAgMnSc1d1	dlouhonosý
(	(	kIx(	(
<g/>
Euroscaptor	Euroscaptor	k1gInSc1	Euroscaptor
longirostris	longirostris	k1gFnSc2	longirostris
<g/>
)	)	kIx)	)
krtek	krtek	k1gMnSc1	krtek
jihočínský	jihočínský	k2eAgMnSc1d1	jihočínský
(	(	kIx(	(
<g/>
Euroscaptor	Euroscaptor	k1gInSc1	Euroscaptor
grandis	grandis	k1gFnSc2	grandis
<g/>
)	)	kIx)	)
krtek	krtek	k1gMnSc1	krtek
Klossův	Klossův	k2eAgMnSc1d1	Klossův
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Euroscaptor	Euroscaptor	k1gInSc4	Euroscaptor
klossi	klosse	k1gFnSc4	klosse
<g/>
)	)	kIx)	)
krtek	krtek	k1gMnSc1	krtek
východní	východní	k2eAgMnSc1d1	východní
(	(	kIx(	(
<g/>
Euroscaptor	Euroscaptor	k1gMnSc1	Euroscaptor
micrura	micrur	k1gMnSc2	micrur
<g/>
)	)	kIx)	)
rod	rod	k1gInSc1	rod
<g/>
:	:	kIx,	:
Nesoscaptor	Nesoscaptor	k1gMnSc1	Nesoscaptor
Nesoscaptor	Nesoscaptor	k1gMnSc1	Nesoscaptor
uchidai	uchida	k1gFnSc2	uchida
rod	rod	k1gInSc1	rod
<g/>
:	:	kIx,	:
Neurotrichus	Neurotrichus	k1gMnSc1	Neurotrichus
krtek	krtek	k1gMnSc1	krtek
rejskovitý	rejskovitý	k2eAgMnSc1d1	rejskovitý
(	(	kIx(	(
<g/>
Neurotrichus	Neurotrichus	k1gMnSc1	Neurotrichus
gibbsii	gibbsie	k1gFnSc4	gibbsie
<g/>
)	)	kIx)	)
rod	rod	k1gInSc1	rod
<g/>
:	:	kIx,	:
Parascalops	Parascalops	k1gInSc1	Parascalops
krtek	krtek	k1gMnSc1	krtek
bělohlavý	bělohlavý	k2eAgMnSc1d1	bělohlavý
(	(	kIx(	(
<g/>
Parascalops	Parascalops	k1gInSc1	Parascalops
breweri	breweri	k1gNnSc1	breweri
<g/>
)	)	kIx)	)
rod	rod	k1gInSc1	rod
<g/>
:	:	kIx,	:
Parascaptor	Parascaptor	k1gMnSc1	Parascaptor
krtek	krtek	k1gMnSc1	krtek
asámský	asámský	k2eAgMnSc1d1	asámský
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Parascaptor	Parascaptor	k1gInSc1	Parascaptor
leucura	leucura	k1gFnSc1	leucura
<g/>
)	)	kIx)	)
rod	rod	k1gInSc1	rod
<g/>
:	:	kIx,	:
Scalopus	Scalopus	k1gMnSc1	Scalopus
krtek	krtek	k1gMnSc1	krtek
východoamerický	východoamerický	k2eAgMnSc1d1	východoamerický
(	(	kIx(	(
<g/>
Scalopus	Scalopus	k1gMnSc1	Scalopus
aquaticus	aquaticus	k1gMnSc1	aquaticus
<g/>
)	)	kIx)	)
rod	rod	k1gInSc1	rod
<g/>
:	:	kIx,	:
Scapanulus	Scapanulus	k1gMnSc1	Scapanulus
krtek	krtek	k1gMnSc1	krtek
čínský	čínský	k2eAgMnSc1d1	čínský
(	(	kIx(	(
<g/>
Scapanulus	Scapanulus	k1gInSc1	Scapanulus
oweni	oweeň	k1gFnSc3	oweeň
<g/>
)	)	kIx)	)
rod	rod	k1gInSc1	rod
<g/>
:	:	kIx,	:
Scapanus	Scapanus	k1gMnSc1	Scapanus
krtek	krtek	k1gMnSc1	krtek
pobřežní	pobřežní	k1gMnSc1	pobřežní
(	(	kIx(	(
<g/>
Scapanus	Scapanus	k1gMnSc1	Scapanus
orarius	orarius	k1gMnSc1	orarius
<g/>
)	)	kIx)	)
krtek	krtek	k1gMnSc1	krtek
širokostopý	širokostopý	k2eAgMnSc1d1	širokostopý
(	(	kIx(	(
<g/>
Scapanus	Scapanus	k1gMnSc1	Scapanus
latimanus	latimanus	k1gMnSc1	latimanus
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
krtek	krtek	k1gMnSc1	krtek
západoamerický	západoamerický	k2eAgMnSc1d1	západoamerický
(	(	kIx(	(
<g/>
Scapanus	Scapanus	k1gMnSc1	Scapanus
townsendii	townsendie	k1gFnSc4	townsendie
<g/>
)	)	kIx)	)
rod	rod	k1gInSc1	rod
<g/>
:	:	kIx,	:
Scaptochirus	Scaptochirus	k1gMnSc1	Scaptochirus
krtek	krtek	k1gMnSc1	krtek
lesklý	lesklý	k2eAgMnSc1d1	lesklý
(	(	kIx(	(
<g/>
Scaptochirus	Scaptochirus	k1gMnSc1	Scaptochirus
moschatus	moschatus	k1gMnSc1	moschatus
<g/>
)	)	kIx)	)
rod	rod	k1gInSc1	rod
<g/>
:	:	kIx,	:
Scaptonyx	Scaptonyx	k1gInSc1	Scaptonyx
krtek	krtek	k1gMnSc1	krtek
dlouhoocasý	dlouhoocasý	k2eAgMnSc1d1	dlouhoocasý
(	(	kIx(	(
<g/>
Scaptonyx	Scaptonyx	k1gInSc1	Scaptonyx
fusicaudus	fusicaudus	k1gInSc1	fusicaudus
<g/>
)	)	kIx)	)
rod	rod	k1gInSc1	rod
<g/>
:	:	kIx,	:
Talpa	Talpa	k1gFnSc1	Talpa
krtek	krtek	k1gMnSc1	krtek
anatolský	anatolský	k2eAgMnSc1d1	anatolský
(	(	kIx(	(
<g/>
Talpa	Talpa	k1gFnSc1	Talpa
levantis	levantis	k1gFnSc1	levantis
<g/>
)	)	kIx)	)
krtek	krtek	k1gMnSc1	krtek
balkánský	balkánský	k2eAgMnSc1d1	balkánský
(	(	kIx(	(
<g/>
Talpa	Talpa	k1gFnSc1	Talpa
stankovici	stankovice	k1gFnSc4	stankovice
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
krtek	krtek	k1gMnSc1	krtek
iberský	iberský	k2eAgMnSc1d1	iberský
(	(	kIx(	(
<g/>
Talpa	Talpa	k1gFnSc1	Talpa
occidentalis	occidentalis	k1gFnSc1	occidentalis
<g/>
)	)	kIx)	)
krtek	krtek	k1gMnSc1	krtek
kavkazský	kavkazský	k2eAgMnSc1d1	kavkazský
(	(	kIx(	(
<g/>
Talpa	Talp	k1gMnSc4	Talp
caucasica	caucasicus	k1gMnSc4	caucasicus
<g/>
)	)	kIx)	)
krtek	krtek	k1gMnSc1	krtek
obecný	obecný	k2eAgMnSc1d1	obecný
(	(	kIx(	(
<g/>
Talpa	Talp	k1gMnSc4	Talp
europaea	europaeus	k1gMnSc4	europaeus
<g/>
)	)	kIx)	)
krtek	krtek	k1gMnSc1	krtek
perský	perský	k2eAgMnSc1d1	perský
(	(	kIx(	(
<g/>
Talpa	Talp	k1gMnSc4	Talp
streeti	street	k5eAaPmF	street
<g/>
)	)	kIx)	)
krtek	krtek	k1gMnSc1	krtek
římský	římský	k2eAgMnSc1d1	římský
(	(	kIx(	(
<g/>
Talpa	Talp	k1gMnSc4	Talp
romana	roman	k1gMnSc4	roman
<g/>
)	)	kIx)	)
krtek	krtek	k1gMnSc1	krtek
sibiřský	sibiřský	k2eAgMnSc1d1	sibiřský
(	(	kIx(	(
<g/>
Talpa	Talp	k1gMnSc4	Talp
altaica	altaicus	k1gMnSc4	altaicus
<g/>
)	)	kIx)	)
krtek	krtek	k1gMnSc1	krtek
slepý	slepý	k2eAgMnSc1d1	slepý
(	(	kIx(	(
<g/>
Talpa	Talpa	k1gFnSc1	Talpa
caeca	caecum	k1gNnSc2	caecum
<g/>
)	)	kIx)	)
rod	rod	k1gInSc1	rod
<g/>
:	:	kIx,	:
Urotrichus	Urotrichus	k1gMnSc1	Urotrichus
krtek	krtek	k1gMnSc1	krtek
horský	horský	k2eAgMnSc1d1	horský
(	(	kIx(	(
<g/>
Urotrichus	Urotrichus	k1gInSc1	Urotrichus
pilirostris	pilirostris	k1gFnSc2	pilirostris
<g/>
)	)	kIx)	)
krtek	krtek	k1gMnSc1	krtek
japonský	japonský	k2eAgMnSc1d1	japonský
(	(	kIx(	(
<g/>
Urotrichus	Urotrichus	k1gMnSc1	Urotrichus
talpoides	talpoides	k1gMnSc1	talpoides
<g/>
)	)	kIx)	)
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Krtek	krtek	k1gMnSc1	krtek
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
krtek	krtek	k1gMnSc1	krtek
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
