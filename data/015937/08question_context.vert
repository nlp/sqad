<s>
Šípatka	šípatka	k1gFnSc1
střelolistá	střelolistý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Sagittaria	Sagittarium	k1gNnSc2
sagittifolia	sagittifolium	k1gNnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
druh	druh	k1gInSc4
jednoděložné	jednoděložný	k2eAgFnSc2d1
rostliny	rostlina	k1gFnSc2
z	z	k7c2
čeledi	čeleď	k1gFnSc2
žabníkovité	žabníkovitý	k2eAgFnSc2d1
(	(	kIx(
<g/>
Alismataceae	Alismatacea	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>