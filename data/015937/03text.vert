<s>
Šípatka	šípatka	k1gFnSc1
střelolistá	střelolistý	k2eAgFnSc1d1
</s>
<s>
Šípatka	šípatka	k1gFnSc1
střelolistá	střelolistý	k2eAgFnSc1d1
Šípatka	šípatka	k1gFnSc1
střelolistá	střelolistý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Sagittaria	Sagittarium	k1gNnSc2
sagittifolia	sagittifolium	k1gNnSc2
<g/>
)	)	kIx)
Stupeň	stupeň	k1gInSc1
ohrožení	ohrožení	k1gNnSc2
podle	podle	k7c2
IUCN	IUCN	kA
</s>
<s>
málo	málo	k6eAd1
dotčený	dotčený	k2eAgMnSc1d1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
Říše	říš	k1gFnSc2
</s>
<s>
rostliny	rostlina	k1gFnPc1
(	(	kIx(
<g/>
Plantae	Plantae	k1gFnSc1
<g/>
)	)	kIx)
Podříše	podříše	k1gFnSc1
</s>
<s>
cévnaté	cévnatý	k2eAgFnPc1d1
rostliny	rostlina	k1gFnPc1
(	(	kIx(
<g/>
Tracheobionta	Tracheobionta	k1gFnSc1
<g/>
)	)	kIx)
Oddělení	oddělení	k1gNnSc1
</s>
<s>
krytosemenné	krytosemenný	k2eAgNnSc1d1
(	(	kIx(
<g/>
Magnoliophyta	Magnoliophyta	k1gFnSc1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
jednoděložné	jednoděložná	k1gFnPc1
(	(	kIx(
<g/>
Liliopsida	Liliopsida	k1gFnSc1
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
žabníkotvaré	žabníkotvarý	k2eAgFnPc1d1
(	(	kIx(
<g/>
Alismatales	Alismatales	k1gInSc1
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc1
</s>
<s>
žabníkovité	žabníkovitý	k2eAgInPc4d1
(	(	kIx(
<g/>
Alismataceae	Alismatacea	k1gInPc4
<g/>
)	)	kIx)
Rod	rod	k1gInSc4
</s>
<s>
šípatka	šípatka	k1gFnSc1
(	(	kIx(
<g/>
Sagittaria	Sagittarium	k1gNnPc4
<g/>
)	)	kIx)
Binomické	binomický	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
Sagittaria	Sagittarium	k1gNnPc1
sagittifoliaL	sagittifoliaL	k?
<g/>
.	.	kIx.
<g/>
,	,	kIx,
1753	#num#	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Šípatka	šípatka	k1gFnSc1
střelolistá	střelolistý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Sagittaria	Sagittarium	k1gNnSc2
sagittifolia	sagittifolium	k1gNnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
druh	druh	k1gInSc4
jednoděložné	jednoděložný	k2eAgFnSc2d1
rostliny	rostlina	k1gFnSc2
z	z	k7c2
čeledi	čeleď	k1gFnSc2
žabníkovité	žabníkovitý	k2eAgFnSc2d1
(	(	kIx(
<g/>
Alismataceae	Alismatacea	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
vytrvalou	vytrvalý	k2eAgFnSc4d1
bahenní	bahenní	k2eAgFnSc4d1
až	až	k8xS
vodní	vodní	k2eAgFnSc4d1
rostlinu	rostlina	k1gFnSc4
<g/>
,	,	kIx,
dorůstá	dorůstat	k5eAaImIp3nS
výšky	výška	k1gFnPc4
cca	cca	kA
30-100	30-100	k4
cm	cm	kA
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
vyrůstá	vyrůstat	k5eAaImIp3nS
z	z	k7c2
krátkého	krátký	k2eAgInSc2d1
silného	silný	k2eAgInSc2d1
oddenku	oddenek	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
má	mít	k5eAaImIp3nS
na	na	k7c6
konci	konec	k1gInSc6
ztlustlé	ztlustlý	k2eAgInPc1d1
výběžky	výběžek	k1gInPc1
<g/>
,	,	kIx,
hlízy	hlíza	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Listy	lista	k1gFnPc1
jsou	být	k5eAaImIp3nP
v	v	k7c6
přízemní	přízemní	k2eAgFnSc6d1
růžici	růžice	k1gFnSc6
<g/>
,	,	kIx,
vynořené	vynořený	k2eAgInPc4d1
listy	list	k1gInPc4
jsou	být	k5eAaImIp3nP
dlouze	dlouho	k6eAd1
řapíkaté	řapíkatý	k2eAgInPc1d1
s	s	k7c7
výrazně	výrazně	k6eAd1
střelovitou	střelovitý	k2eAgFnSc7d1
čepelí	čepel	k1gFnSc7
<g/>
,	,	kIx,
ponořené	ponořený	k2eAgInPc1d1
jsou	být	k5eAaImIp3nP
dlouze	dlouho	k6eAd1
čárkovité	čárkovitý	k2eAgInPc1d1
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
se	se	k3xPyFc4
vyvíjí	vyvíjet	k5eAaImIp3nS
i	i	k9
listy	list	k1gInPc4
přechodné	přechodný	k2eAgInPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Květy	Květa	k1gFnPc1
jsou	být	k5eAaImIp3nP
uspořádány	uspořádat	k5eAaPmNgFnP
do	do	k7c2
květenství	květenství	k1gNnSc2
<g/>
,	,	kIx,
jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
vrcholovou	vrcholový	k2eAgFnSc4d1
latu	lata	k1gFnSc4
uspořádanou	uspořádaný	k2eAgFnSc4d1
do	do	k7c2
trojčetných	trojčetný	k2eAgInPc2d1
přeslenů	přeslen	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Květy	Květa	k1gFnPc1
jsou	být	k5eAaImIp3nP
jednopohlavné	jednopohlavný	k2eAgFnPc1d1
<g/>
,	,	kIx,
samčí	samčí	k2eAgFnPc1d1
jsou	být	k5eAaImIp3nP
na	na	k7c6
delších	dlouhý	k2eAgFnPc6d2
stopkách	stopka	k1gFnPc6
než	než	k8xS
samičí	samičí	k2eAgFnSc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okvětí	okvětí	k1gNnSc1
je	být	k5eAaImIp3nS
rozlišeno	rozlišit	k5eAaPmNgNnS
na	na	k7c4
kalich	kalich	k1gInSc4
a	a	k8xC
korunu	koruna	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kališní	kališní	k2eAgInPc1d1
lístky	lístek	k1gInPc1
jsou	být	k5eAaImIp3nP
3	#num#	k4
<g/>
,	,	kIx,
korunní	korunní	k2eAgInSc1d1
také	také	k9
3	#num#	k4
<g/>
,	,	kIx,
korunní	korunní	k2eAgInPc1d1
lístky	lístek	k1gInPc1
jsou	být	k5eAaImIp3nP
bílé	bílý	k2eAgInPc1d1
<g/>
,	,	kIx,
na	na	k7c6
bázi	báze	k1gFnSc6
mají	mít	k5eAaImIp3nP
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
šípatky	šípatka	k1gFnSc2
širolisté	širolistý	k2eAgFnSc2d1
sytě	sytě	k6eAd1
fialovou	fialový	k2eAgFnSc4d1
skvrnu	skvrna	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prašníky	prašník	k1gInPc7
jsou	být	k5eAaImIp3nP
černofialové	černofialová	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gyneceum	Gyneceum	k1gInSc1
je	on	k3xPp3gMnPc4
apokarpní	apokarpnit	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plodem	plod	k1gInSc7
jsou	být	k5eAaImIp3nP
nažky	nažka	k1gFnPc4
uspořádané	uspořádaný	k2eAgFnPc4d1
v	v	k7c6
souplodí	souplodí	k1gNnSc6
<g/>
,	,	kIx,
zobánek	zobánek	k1gInSc4
nažky	nažka	k1gFnSc2
je	být	k5eAaImIp3nS
kratší	krátký	k2eAgFnSc1d2
než	než	k8xS
1	#num#	k4
mm	mm	kA
<g/>
.	.	kIx.
</s>
<s>
Detail	detail	k1gInSc1
samčího	samčí	k2eAgInSc2d1
květu	květ	k1gInSc2
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1
ve	v	k7c6
světě	svět	k1gInSc6
</s>
<s>
Šípatka	šípatka	k1gFnSc1
střelolistá	střelolistý	k2eAgFnSc1d1
roste	růst	k5eAaImIp3nS
přirozeně	přirozeně	k6eAd1
na	na	k7c6
většině	většina	k1gFnSc6
území	území	k1gNnSc2
Evropy	Evropa	k1gFnSc2
<g/>
,	,	kIx,
kromě	kromě	k7c2
úplného	úplný	k2eAgInSc2d1
jihu	jih	k1gInSc2
a	a	k8xC
severu	sever	k1gInSc2
<g/>
,	,	kIx,
na	na	k7c4
východ	východ	k1gInSc4
sahá	sahat	k5eAaImIp3nS
až	až	k9
po	po	k7c4
západní	západní	k2eAgFnSc4d1
Sibiř	Sibiř	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
v	v	k7c6
Asii	Asie	k1gFnSc6
roste	růst	k5eAaImIp3nS
příbuzná	příbuzná	k1gFnSc1
Sagittaria	Sagittarium	k1gNnSc2
trifolia	trifolium	k1gNnSc2
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
v	v	k7c6
Americe	Amerika	k1gFnSc6
zase	zase	k9
Sagittaria	Sagittarium	k1gNnPc4
montevidensis	montevidensis	k1gFnPc2
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Adventivně	adventivně	k6eAd1
či	či	k8xC
jako	jako	k9
pěstovaná	pěstovaný	k2eAgFnSc1d1
však	však	k8xC
roste	růst	k5eAaImIp3nS
i	i	k9
jinde	jinde	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1
v	v	k7c6
ČR	ČR	kA
</s>
<s>
V	v	k7c6
ČR	ČR	kA
roste	růst	k5eAaImIp3nS
celkem	celkem	k6eAd1
běžně	běžně	k6eAd1
hlavně	hlavně	k9
v	v	k7c6
teplejších	teplý	k2eAgFnPc6d2
oblastech	oblast	k1gFnPc6
od	od	k7c2
nížin	nížina	k1gFnPc2
po	po	k7c4
pahorkatiny	pahorkatina	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Najdeme	najít	k5eAaPmIp1nP
ji	on	k3xPp3gFnSc4
na	na	k7c6
okrajích	okraj	k1gInPc6
stojatých	stojatý	k2eAgInPc6d1
a	a	k8xC
pomalu	pomalu	k6eAd1
tekoucích	tekoucí	k2eAgFnPc2d1
vod	voda	k1gFnPc2
či	či	k8xC
na	na	k7c6
obnažených	obnažený	k2eAgInPc6d1
dnech	den	k1gInPc6
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Častá	častý	k2eAgFnSc1d1
ve	v	k7c6
společenstvu	společenstvo	k1gNnSc6
sv.	sv.	kA
Oenanthion	Oenanthion	k1gInSc4
aquaticae	aquatica	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Využití	využití	k1gNnSc1
</s>
<s>
Vařené	vařený	k2eAgInPc1d1
či	či	k8xC
opékané	opékaný	k2eAgInPc1d1
kořeny	kořen	k1gInPc1
a	a	k8xC
hlízy	hlíza	k1gFnPc1
jsou	být	k5eAaImIp3nP
jedlé	jedlý	k2eAgFnPc1d1
a	a	k8xC
chutnají	chutnat	k5eAaImIp3nP
podobně	podobně	k6eAd1
jak	jak	k8xS,k8xC
brambory	brambora	k1gFnPc1
<g/>
,	,	kIx,
hlízy	hlíza	k1gFnPc1
se	s	k7c7
škrobovitou	škrobovitý	k2eAgFnSc7d1
příchutí	příchuť	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
listy	list	k1gInPc1
a	a	k8xC
výhonky	výhonek	k1gInPc1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
vařeny	vařen	k2eAgInPc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
jsou	být	k5eAaImIp3nP
trochu	trochu	k6eAd1
štiplavé	štiplavý	k2eAgInPc4d1
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zvláště	zvláště	k6eAd1
v	v	k7c6
Asii	Asie	k1gFnSc6
a	a	k8xC
na	na	k7c6
Havaji	Havaj	k1gFnSc6
je	být	k5eAaImIp3nS
i	i	k9
za	za	k7c7
tímto	tento	k3xDgInSc7
účelem	účel	k1gInSc7
pěstována	pěstován	k2eAgFnSc1d1
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
hlízy	hlíza	k1gFnPc4
příbuzného	příbuzný	k2eAgInSc2d1
asijského	asijský	k2eAgInSc2d1
druhu	druh	k1gInSc2
Sagittaria	Sagittarium	k1gNnSc2
trifolia	trifolium	k1gNnSc2
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
The	The	k1gFnSc1
IUCN	IUCN	kA
Red	Red	k1gFnSc1
List	list	k1gInSc1
of	of	k?
Threatened	Threatened	k1gInSc1
Species	species	k1gFnSc1
2021.1	2021.1	k4
<g/>
.	.	kIx.
25	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
1	#num#	k4
2	#num#	k4
Kirschner	Kirschnra	k1gFnPc2
J.	J.	kA
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Sagittaria	Sagittarium	k1gNnPc1
L.	L.	kA
<g/>
:	:	kIx,
In	In	k1gMnSc1
Kubát	Kubát	k1gMnSc1
K.	K.	kA
et	et	k?
al	ala	k1gFnPc2
(	(	kIx(
<g/>
eds	eds	k?
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Klíč	klíč	k1gInSc1
ke	k	k7c3
květeně	květena	k1gFnSc3
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
p.	p.	k?
<g/>
:	:	kIx,
<g/>
733	#num#	k4
<g/>
,	,	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
↑	↑	k?
http://linnaeus.nrm.se/flora/mono/alismata/sagit/sagisagv.jpg	http://linnaeus.nrm.se/flora/mono/alismata/sagit/sagisagv.jpg	k1gInSc1
<g/>
↑	↑	k?
http://www.efloras.org/florataxon.aspx?flora_id=1&	http://www.efloras.org/florataxon.aspx?flora_id=1&	k?
<g/>
↑	↑	k?
http://www.ibiblio.org/pfaf/cgi-bin/arr_html?Sagittaria+sagittifolia	http://www.ibiblio.org/pfaf/cgi-bin/arr_html?Sagittaria+sagittifolium	k1gNnSc2
<g/>
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.lucidcentral.org	www.lucidcentral.org	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Klíč	klíč	k1gInSc1
ke	k	k7c3
Květeně	květena	k1gFnSc3
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
Kubát	Kubát	k1gMnSc1
K.	K.	kA
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
(	(	kIx(
<g/>
eds	eds	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
</s>
<s>
Nová	nový	k2eAgFnSc1d1
Květena	květena	k1gFnSc1
ČSSR	ČSSR	kA
<g/>
,	,	kIx,
vol	vol	k6eAd1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
,	,	kIx,
Dostál	Dostál	k1gMnSc1
J.	J.	kA
(	(	kIx(
<g/>
1989	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
šípatka	šípatka	k1gFnSc1
střelolistá	střelolistý	k2eAgFnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Galerie	galerie	k1gFnSc1
šípatka	šípatka	k1gFnSc1
střelolistá	střelolistý	k2eAgFnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
{	{	kIx(
<g/>
box-sizing	box-sizing	k1gInSc1
<g/>
:	:	kIx,
<g/>
border-box	border-box	k1gInSc1
<g/>
;	;	kIx,
<g/>
border	border	k1gInSc1
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
px	px	k?
solid	solid	k1gInSc1
#	#	kIx~
<g/>
a	a	k8xC
<g/>
2	#num#	k4
<g/>
a	a	k8xC
<g/>
9	#num#	k4
<g/>
b	b	k?
<g/>
1	#num#	k4
<g/>
;	;	kIx,
<g/>
width	widtha	k1gFnPc2
<g/>
:	:	kIx,
<g/>
100	#num#	k4
<g/>
%	%	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
;	;	kIx,
<g/>
clear	clear	k1gInSc1
<g/>
:	:	kIx,
<g/>
both	both	k1gInSc1
<g/>
;	;	kIx,
<g/>
font-size	font-size	k1gFnSc1
<g/>
:	:	kIx,
<g/>
88	#num#	k4
<g/>
%	%	kIx~
<g/>
;	;	kIx,
<g/>
text-align	text-align	k1gInSc1
<g/>
:	:	kIx,
<g/>
center	centrum	k1gNnPc2
<g/>
;	;	kIx,
<g/>
padding	padding	k1gInSc1
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
px	px	k?
<g/>
;	;	kIx,
<g/>
margin	margin	k1gMnSc1
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
em	em	k?
auto	auto	k1gNnSc1
0	#num#	k4
<g/>
}	}	kIx)
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
{	{	kIx(
<g/>
margin-top	margin-top	k1gInSc1
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
+	+	kIx~
<g/>
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
{	{	kIx(
<g/>
margin-top	margin-top	k1gInSc1
<g/>
:	:	kIx,
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
px	px	k?
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
-inner	-innra	k1gFnPc2
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc4
.	.	kIx.
<g/>
navbox	navbox	k1gInSc4
<g/>
2	#num#	k4
<g/>
-subgroup	-subgroup	k1gMnSc1
<g/>
{	{	kIx(
<g/>
width	width	k1gMnSc1
<g/>
:	:	kIx,
<g/>
100	#num#	k4
<g/>
%	%	kIx~
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
-group	-group	k1gInSc1
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
-title	-titlat	k5eAaPmIp3nS
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
-abovebelow	-abovebelow	k?
<g/>
{	{	kIx(
<g/>
padding	padding	k1gInSc1
<g/>
:	:	kIx,
<g/>
0.25	0.25	k4
<g/>
em	em	k?
1	#num#	k4
<g/>
em	em	k?
<g/>
;	;	kIx,
<g/>
line-height	line-height	k1gMnSc1
<g/>
:	:	kIx,
<g/>
1.5	1.5	k4
<g/>
em	em	k?
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
;	;	kIx,
<g/>
text-align	text-aligno	k1gNnPc2
<g/>
:	:	kIx,
<g/>
center	centrum	k1gNnPc2
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
th	th	k?
<g/>
.	.	kIx.
<g/>
navbox	navbox	k1gInSc4
<g/>
2	#num#	k4
<g/>
-group	-group	k1gInSc1
<g/>
{	{	kIx(
<g/>
white-space	white-space	k1gFnSc1
<g/>
:	:	kIx,
<g/>
nowrap	nowrap	k1gMnSc1
<g/>
;	;	kIx,
<g/>
text-align	text-align	k1gMnSc1
<g/>
:	:	kIx,
<g/>
right	right	k1gMnSc1
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc4
.	.	kIx.
<g/>
navbox	navbox	k1gInSc4
<g/>
2	#num#	k4
<g/>
-subgroup	-subgroup	k1gMnSc1
<g/>
{	{	kIx(
<g/>
background	background	k1gMnSc1
<g/>
:	:	kIx,
<g/>
#	#	kIx~
<g/>
fdfdfd	fdfdfd	k6eAd1
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc4d1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc4
<g/>
2	#num#	k4
<g/>
-list	-list	k1gMnSc1
<g/>
{	{	kIx(
<g/>
line-height	line-height	k1gMnSc1
<g/>
:	:	kIx,
<g/>
1.5	1.5	k4
<g/>
em	em	k?
<g/>
;	;	kIx,
<g/>
border-color	border-color	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
:	:	kIx,
<g/>
#	#	kIx~
<g/>
fdfdfd	fdfdfd	k6eAd1
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
tr	tr	k?
<g/>
+	+	kIx~
<g/>
tr	tr	k?
<g/>
>	>	kIx)
<g/>
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
-abovebelow	-abovebelow	k?
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
tr	tr	k?
<g/>
+	+	kIx~
<g/>
tr	tr	k?
<g/>
>	>	kIx)
<g/>
.	.	kIx.
<g/>
navbox	navbox	k1gInSc4
<g/>
2	#num#	k4
<g/>
-group	-group	k1gMnSc1
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
tr	tr	k?
<g/>
+	+	kIx~
<g/>
tr	tr	k?
<g/>
>	>	kIx)
<g/>
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
-image	-imagat	k5eAaPmIp3nS
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
tr	tr	k?
<g/>
+	+	kIx~
<g/>
tr	tr	k?
<g/>
>	>	kIx)
<g/>
.	.	kIx.
<g/>
navbox	navbox	k1gInSc4
<g/>
2	#num#	k4
<g/>
-list	-list	k1gInSc1
<g/>
{	{	kIx(
<g/>
border-top	border-top	k1gInSc1
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
px	px	k?
<g />
.	.	kIx.
</s>
<s hack="1">
solid	solid	k1gInSc1
#	#	kIx~
<g/>
fdfdfd	fdfdfd	k1gInSc1
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
th	th	k?
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
-title	-title	k1gFnSc2
<g/>
{	{	kIx(
<g/>
background	background	k1gMnSc1
<g/>
:	:	kIx,
<g/>
#	#	kIx~
<g/>
e	e	k0
<g/>
0	#num#	k4
<g/>
e	e	k0
<g/>
0	#num#	k4
<g/>
e	e	k0
<g/>
0	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
-abovebelow	-abovebelow	k?
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
th	th	k?
<g/>
.	.	kIx.
<g/>
navbox	navbox	k1gInSc4
<g/>
2	#num#	k4
<g/>
-group	-group	k1gInSc4
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc4
.	.	kIx.
<g/>
navbox	navbox	k1gInSc4
<g/>
2	#num#	k4
<g/>
-subgroup	-subgroup	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
-title	-title	k1gFnSc2
<g/>
{	{	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
background	background	k1gInSc1
<g/>
:	:	kIx,
<g/>
#	#	kIx~
<g/>
e	e	k0
<g/>
7	#num#	k4
<g/>
e	e	k0
<g/>
7	#num#	k4
<g/>
e	e	k0
<g/>
7	#num#	k4
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc4d1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc4
<g/>
2	#num#	k4
<g/>
-subgroup	-subgroup	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
-title	-title	k1gFnSc2
<g/>
{	{	kIx(
<g/>
font-size	font-size	k1gFnSc1
<g/>
:	:	kIx,
<g/>
88	#num#	k4
<g/>
%	%	kIx~
<g/>
}	}	kIx)
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc4d1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc4
<g/>
2	#num#	k4
<g/>
-subgroup	-subgroup	k1gInSc4
.	.	kIx.
<g/>
navbox	navbox	k1gInSc4
<g/>
2	#num#	k4
<g/>
-group	-group	k1gInSc4
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc4
.	.	kIx.
<g/>
navbox	navbox	k1gInSc4
<g/>
2	#num#	k4
<g/>
-subgroup	-subgroup	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
-abovebelow	-abovebelow	k?
<g/>
{	{	kIx(
<g/>
background	background	k1gMnSc1
<g/>
:	:	kIx,
<g/>
#	#	kIx~
<g/>
f	f	k?
<g/>
0	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
f	f	k?
<g/>
0	#num#	k4
<g/>
f	f	k?
<g/>
0	#num#	k4
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
-even	-evna	k1gFnPc2
<g/>
{	{	kIx(
<g/>
background	background	k1gInSc1
<g/>
:	:	kIx,
<g/>
#	#	kIx~
<g/>
f	f	k?
<g/>
7	#num#	k4
<g/>
f	f	k?
<g/>
7	#num#	k4
<g/>
f	f	k?
<g/>
7	#num#	k4
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
2	#num#	k4
<g/>
-odd	-odda	k1gFnPc2
<g/>
{	{	kIx(
<g/>
background	background	k1gInSc1
<g/>
:	:	kIx,
<g/>
transparent	transparent	k1gInSc1
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
.	.	kIx.
<g/>
hlist	hlist	k1gFnSc1
td	td	k?
dl	dl	k?
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
.	.	kIx.
<g/>
hlist	hlist	k1gFnSc1
td	td	k?
ol	ol	k?
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
.	.	kIx.
<g/>
hlist	hlist	k1gFnSc1
td	td	k?
ul	ul	kA
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
td	td	k?
<g/>
.	.	kIx.
<g/>
hlist	hlist	k1gMnSc1
dl	dl	k?
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
td	td	k?
<g/>
.	.	kIx.
<g/>
hlist	hlist	k1gMnSc1
ol	ol	k?
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
td	td	k?
<g/>
.	.	kIx.
<g/>
hlist	hlist	k1gMnSc1
ul	ul	kA
<g/>
{	{	kIx(
<g/>
padding	padding	k1gInSc1
<g/>
:	:	kIx,
<g/>
0.125	0.125	k4
<g/>
em	em	k?
0	#num#	k4
<g/>
}	}	kIx)
<g/>
Identifikátory	identifikátor	k1gInPc1
taxonu	taxon	k1gInSc2
</s>
<s>
Wikidata	Wikidata	k1gFnSc1
<g/>
:	:	kIx,
Q161565	Q161565	k1gFnSc1
</s>
<s>
Wikidruhy	Wikidruha	k1gFnSc2
<g/>
:	:	kIx,
Sagittaria	Sagittarium	k1gNnSc2
sagittifolia	sagittifolium	k1gNnPc1
</s>
<s>
APNI	APNI	kA
<g/>
:	:	kIx,
59066	#num#	k4
</s>
<s>
BioLib	BioLib	k1gInSc1
<g/>
:	:	kIx,
41881	#num#	k4
</s>
<s>
NDOP	NDOP	kA
<g/>
:	:	kIx,
39462	#num#	k4
</s>
<s>
Ecocrop	Ecocrop	k1gInSc1
<g/>
:	:	kIx,
9441	#num#	k4
</s>
<s>
EoL	EoL	k?
<g/>
:	:	kIx,
1082273	#num#	k4
</s>
<s>
EPPO	EPPO	kA
<g/>
:	:	kIx,
SAGSA	SAGSA	kA
</s>
<s>
EUNIS	EUNIS	kA
<g/>
:	:	kIx,
185605	#num#	k4
</s>
<s>
GBIF	GBIF	kA
<g/>
:	:	kIx,
5328882	#num#	k4
</s>
<s>
GRIN	GRIN	kA
<g/>
:	:	kIx,
32648	#num#	k4
</s>
<s>
iNaturalist	iNaturalist	k1gInSc1
<g/>
:	:	kIx,
69814	#num#	k4
</s>
<s>
IPNI	IPNI	kA
<g/>
:	:	kIx,
30074004-2	30074004-2	k4
</s>
<s>
IRMNG	IRMNG	kA
<g/>
:	:	kIx,
10690562	#num#	k4
</s>
<s>
ISC	ISC	kA
<g/>
:	:	kIx,
48125	#num#	k4
</s>
<s>
ITIS	ITIS	kA
<g/>
:	:	kIx,
38931	#num#	k4
</s>
<s>
IUCN	IUCN	kA
<g/>
:	:	kIx,
167821	#num#	k4
</s>
<s>
NBN	NBN	kA
<g/>
:	:	kIx,
NBNSYS0000002105	NBNSYS0000002105	k1gMnSc1
</s>
<s>
NCBI	NCBI	kA
<g/>
:	:	kIx,
4451	#num#	k4
</s>
<s>
NZOR	NZOR	kA
<g/>
:	:	kIx,
b	b	k?
<g/>
2	#num#	k4
<g/>
fc	fc	k?
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
d	d	k?
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
eec-	eec-	k?
<g/>
48	#num#	k4
<g/>
f	f	k?
<g/>
6	#num#	k4
<g/>
-a	-a	k?
<g/>
314	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
160	#num#	k4
<g/>
cc	cc	k?
<g/>
48	#num#	k4
<g/>
d	d	k?
<g/>
8	#num#	k4
<g/>
f	f	k?
<g/>
9	#num#	k4
</s>
<s>
NZPCN	NZPCN	kA
<g/>
:	:	kIx,
4122	#num#	k4
</s>
<s>
PalDat	PalDat	k1gInSc1
<g/>
:	:	kIx,
Sagittaria_sagittifolia	Sagittaria_sagittifolia	k1gFnSc1
</s>
<s>
PfaF	PfaF	k?
<g/>
:	:	kIx,
Sagittaria	Sagittarium	k1gNnSc2
sagittifolia	sagittifolium	k1gNnPc1
</s>
<s>
Plant	planta	k1gFnPc2
List	lista	k1gFnPc2
<g/>
:	:	kIx,
kew-	kew-	k?
<g/>
287474	#num#	k4
</s>
<s>
PLANTS	PLANTS	kA
<g/>
:	:	kIx,
SASA7	SASA7	k1gMnSc1
</s>
<s>
POWO	POWO	kA
<g/>
:	:	kIx,
urn	urn	k?
<g/>
:	:	kIx,
<g/>
lsid	lsid	k1gInSc1
<g/>
:	:	kIx,
<g/>
ipni	ipni	k1gNnSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
<g/>
:	:	kIx,
<g/>
names	names	k1gMnSc1
<g/>
:	:	kIx,
<g/>
30074004	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
</s>
<s>
Tropicos	Tropicos	k1gInSc1
<g/>
:	:	kIx,
900018	#num#	k4
</s>
<s>
WCSP	WCSP	kA
<g/>
:	:	kIx,
287474	#num#	k4
</s>
<s>
WFO	WFO	kA
<g/>
:	:	kIx,
wfo-	wfo-	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
738109	#num#	k4
</s>
<s>
WoRMS	WoRMS	k?
<g/>
:	:	kIx,
1415739	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Rostliny	rostlina	k1gFnPc1
</s>
