hromadná	hromadný	k2eAgFnSc1d1	hromadná
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
sportovní	sportovní	k2eAgFnSc1d1	sportovní
soutěž	soutěž	k1gFnSc1	soutěž
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
různých	různý	k2eAgFnPc6d1	různá
disciplínách	disciplína	k1gFnPc6	disciplína
a	a	k8xC	a
sportech	sport	k1gInPc6	sport
<g/>
,	,	kIx,	,
za	za	k7c2	za
účasti	účast	k1gFnSc2	účast
sportovců	sportovec	k1gMnPc2	sportovec
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
pořádají	pořádat	k5eAaImIp3nP	pořádat
jednou	jednou	k6eAd1	jednou
za	za	k7c4	za
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
