<s>
Kanada	Kanada	k1gFnSc1
na	na	k7c6
Letních	letní	k2eAgFnPc6d1
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
1956	#num#	k4
</s>
<s>
Kanada	Kanada	k1gFnSc1
na	na	k7c6
Letních	letní	k2eAgFnPc6d1
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
1956	#num#	k4
</s>
<s>
Vlajka	vlajka	k1gFnSc1
výpravyIOC	výpravyIOC	k?
kód	kód	k1gInSc4
</s>
<s>
CAN	CAN	kA
</s>
<s>
ZlatáStříbrnáBronzováCelkem	ZlatáStříbrnáBronzováCelek	k1gInSc7
</s>
<s>
2	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
NOV	nov	k1gInSc1
</s>
<s>
Olympijský	olympijský	k2eAgInSc1d1
výbor	výbor	k1gInSc1
Kanady	Kanada	k1gFnSc2
Vlajkonoš	vlajkonoš	k1gMnSc1
</s>
<s>
Bob	Bob	k1gMnSc1
Steckle	Steckl	k1gInSc5
</s>
<s>
Kanada	Kanada	k1gFnSc1
se	se	k3xPyFc4
účastnila	účastnit	k5eAaImAgFnS
Letní	letní	k2eAgFnSc2d1
olympiády	olympiáda	k1gFnSc2
1956	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zastupovalo	zastupovat	k5eAaImAgNnS
ji	on	k3xPp3gFnSc4
92	#num#	k4
sportovců	sportovec	k1gMnPc2
(	(	kIx(
<g/>
77	#num#	k4
mužů	muž	k1gMnPc2
a	a	k8xC
15	#num#	k4
žen	žena	k1gFnPc2
<g/>
)	)	kIx)
v	v	k7c6
14	#num#	k4
sportech	sport	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Medailisté	medailista	k1gMnPc1
</s>
<s>
Medaile	medaile	k1gFnSc1
</s>
<s>
Jméno	jméno	k1gNnSc1
</s>
<s>
Sport	sport	k1gInSc1
</s>
<s>
Soutěž	soutěž	k1gFnSc1
</s>
<s>
ZlatoArchibald	ZlatoArchibald	k1gMnSc1
McKinnon	McKinnon	k1gMnSc1
Kenneth	Kenneth	k1gMnSc1
Loomer	Loomer	k1gMnSc1
Walter	Walter	k1gMnSc1
D	D	kA
<g/>
'	'	kIx"
<g/>
Hondt	Hondt	k1gMnSc1
Donald	Donald	k1gMnSc1
Arnold	Arnold	k1gMnSc1
VeslováníMuži	VeslováníMuž	k1gFnSc6
čtyřka	čtyřka	k1gFnSc1
bez	bez	k7c2
kormidelníka	kormidelník	k1gMnSc2
</s>
<s>
ZlatoRaymond	ZlatoRaymond	k1gMnSc1
Ouellette	Ouellett	k1gInSc5
Sportovní	sportovní	k2eAgFnSc6d1
střelbaMuži	střelbaMuž	k1gFnSc3
50	#num#	k4
<g/>
m	m	kA
Rifle	rifle	k1gFnPc4
Prone	Pron	k1gMnSc5
</s>
<s>
StříbroPhilip	StříbroPhilip	k1gMnSc1
Kueber	Kueber	k1gMnSc1
Richard	Richard	k1gMnSc1
McClure	McClur	k1gMnSc5
Robert	Robert	k1gMnSc1
Wilson	Wilsona	k1gFnPc2
David	David	k1gMnSc1
Helliwell	Helliwell	k1gMnSc1
Donald	Donald	k1gMnSc1
PrettyWilliam	PrettyWilliam	k1gInSc4
McKerlich	McKerlich	k1gMnSc1
Douglas	Douglas	k1gMnSc1
McDonald	McDonald	k1gMnSc1
Lawrence	Lawrenec	k1gInSc2
West	West	k2eAgInSc4d1
Carlton	Carlton	k1gInSc4
Ogawa	Ogaw	k1gInSc2
VeslováníMuži	VeslováníMuž	k1gFnSc3
osmiveslice	osmiveslice	k1gFnSc1
</s>
<s>
BronzIrene	BronzIren	k1gMnSc5
MacDonald	Macdonald	k1gMnSc1
Skoky	skok	k1gInPc7
do	do	k7c2
vodyŽeny	vodyŽena	k1gFnSc2
3	#num#	k4
m	m	kA
prkno	prkno	k1gNnSc1
</s>
<s>
BronzStuart	BronzStuart	k1gInSc1
Boa	boa	k1gFnSc2
Sportovní	sportovní	k2eAgFnSc2d1
střelbaMuži	střelbaMuzat	k5eAaPmIp1nS
50	#num#	k4
metrů	metr	k1gInPc2
libovolná	libovolný	k2eAgFnSc1d1
malorážka	malorážka	k1gFnSc1
60	#num#	k4
ran	rána	k1gFnPc2
vleže	vleže	k6eAd1
</s>
<s>
BronzJohn	BronzJohn	k1gMnSc1
Rumble	Rumble	k1gMnSc1
James	James	k1gMnSc1
Elder	Eldra	k1gFnPc2
Brian	Briana	k1gFnPc2
HerbinsonJezdectvíJezdecká	HerbinsonJezdectvíJezdecký	k2eAgFnSc1d1
všestrannost	všestrannost	k1gFnSc1
-	-	kIx~
družstva	družstvo	k1gNnPc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Kanada	Kanada	k1gFnSc1
na	na	k7c4
LOH	LOH	kA
1956	#num#	k4
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Kanada	Kanada	k1gFnSc1
na	na	k7c6
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
Kanada	Kanada	k1gFnSc1
na	na	k7c4
LOH	LOH	kA
</s>
<s>
1900	#num#	k4
•	•	k?
1904	#num#	k4
•	•	k?
1908	#num#	k4
•	•	k?
1912	#num#	k4
•	•	k?
1920	#num#	k4
•	•	k?
1924	#num#	k4
•	•	k?
1928	#num#	k4
•	•	k?
1932	#num#	k4
•	•	k?
1936	#num#	k4
•	•	k?
1948	#num#	k4
•	•	k?
1952	#num#	k4
•	•	k?
1956	#num#	k4
•	•	k?
1960	#num#	k4
•	•	k?
1964	#num#	k4
•	•	k?
1968	#num#	k4
•	•	k?
1972	#num#	k4
•	•	k?
1976	#num#	k4
•	•	k?
1980	#num#	k4
•	•	k?
1984	#num#	k4
•	•	k?
1988	#num#	k4
•	•	k?
1992	#num#	k4
•	•	k?
1996	#num#	k4
•	•	k?
2000	#num#	k4
•	•	k?
2004	#num#	k4
•	•	k?
2008	#num#	k4
•	•	k?
2012	#num#	k4
•	•	k?
2016	#num#	k4
•	•	k?
2020	#num#	k4
Kanada	Kanada	k1gFnSc1
na	na	k7c4
ZOH	ZOH	kA
</s>
<s>
1924	#num#	k4
•	•	k?
1928	#num#	k4
•	•	k?
1932	#num#	k4
•	•	k?
1936	#num#	k4
•	•	k?
1948	#num#	k4
•	•	k?
1952	#num#	k4
•	•	k?
1956	#num#	k4
•	•	k?
1960	#num#	k4
•	•	k?
1964	#num#	k4
•	•	k?
1968	#num#	k4
•	•	k?
1972	#num#	k4
•	•	k?
1976	#num#	k4
•	•	k?
1980	#num#	k4
•	•	k?
1984	#num#	k4
•	•	k?
1988	#num#	k4
•	•	k?
1992	#num#	k4
•	•	k?
1994	#num#	k4
•	•	k?
1998	#num#	k4
•	•	k?
2002	#num#	k4
•	•	k?
2006	#num#	k4
•	•	k?
2010	#num#	k4
•	•	k?
2014	#num#	k4
•	•	k?
2018	#num#	k4
•	•	k?
2022	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Sport	sport	k1gInSc1
|	|	kIx~
Kanada	Kanada	k1gFnSc1
</s>
