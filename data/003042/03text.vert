<s>
Lentilky	lentilka	k1gFnPc1	lentilka
jsou	být	k5eAaImIp3nP	být
čokoládové	čokoládový	k2eAgInPc4d1	čokoládový
bonbóny	bonbón	k1gInPc4	bonbón
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
pravidelné	pravidelný	k2eAgFnSc2d1	pravidelná
čočky	čočka	k1gFnSc2	čočka
s	s	k7c7	s
různobarevným	různobarevný	k2eAgInSc7d1	různobarevný
cukrovým	cukrový	k2eAgInSc7d1	cukrový
potahem	potah	k1gInSc7	potah
vyráběné	vyráběný	k2eAgFnPc1d1	vyráběná
firmou	firma	k1gFnSc7	firma
Nestlé	Nestlý	k2eAgFnPc1d1	Nestlý
<g/>
.	.	kIx.	.
</s>
<s>
Lentilky	lentilka	k1gFnPc1	lentilka
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
firma	firma	k1gFnSc1	firma
Sfinx	sfinx	k1gFnSc1	sfinx
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
část	část	k1gFnSc4	část
koncernu	koncern	k1gInSc2	koncern
Nestlé	Nestlý	k2eAgNnSc1d1	Nestlé
<g/>
)	)	kIx)	)
v	v	k7c6	v
Holešově	Holešov	k1gInSc6	Holešov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vyráběly	vyrábět	k5eAaImAgFnP	vyrábět
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1907	[number]	k4	1907
a	a	k8xC	a
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
i	i	k9	i
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
a	a	k8xC	a
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgInPc1	tento
výrobky	výrobek	k1gInPc1	výrobek
známy	znám	k2eAgInPc1d1	znám
pod	pod	k7c7	pod
tradičním	tradiční	k2eAgInSc7d1	tradiční
názvem	název	k1gInSc7	název
Lentilky	lentilka	k1gFnSc2	lentilka
<g/>
,	,	kIx,	,
v	v	k7c6	v
ostatních	ostatní	k2eAgFnPc6d1	ostatní
zemích	zem	k1gFnPc6	zem
se	se	k3xPyFc4	se
prodávají	prodávat	k5eAaImIp3nP	prodávat
podobné	podobný	k2eAgInPc1d1	podobný
výrobky	výrobek	k1gInPc1	výrobek
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Smarties	Smartiesa	k1gFnPc2	Smartiesa
<g/>
.	.	kIx.	.
</s>
<s>
Smarties	Smarties	k1gInSc1	Smarties
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
některé	některý	k3yIgInPc4	některý
druhy	druh	k1gInPc4	druh
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
stejná	stejný	k2eAgFnSc1d1	stejná
firma	firma	k1gFnSc1	firma
jako	jako	k8xS	jako
Lentilky	lentilka	k1gFnPc1	lentilka
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
tvar	tvar	k1gInSc4	tvar
nepravidelné	pravidelný	k2eNgFnSc2d1	nepravidelná
čočky	čočka	k1gFnSc2	čočka
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
pravidelných	pravidelný	k2eAgFnPc2d1	pravidelná
Lentilek	lentilka	k1gFnPc2	lentilka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
větší	veliký	k2eAgInSc4d2	veliký
podíl	podíl	k1gInSc4	podíl
mléčné	mléčný	k2eAgFnSc2d1	mléčná
čokolády	čokoláda	k1gFnSc2	čokoláda
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
mají	mít	k5eAaImIp3nP	mít
tenčí	tenký	k2eAgFnSc4d2	tenčí
cukrovou	cukrový	k2eAgFnSc4d1	cukrová
krustu	krusta	k1gFnSc4	krusta
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
navíc	navíc	k6eAd1	navíc
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
pouze	pouze	k6eAd1	pouze
přírodní	přírodní	k2eAgNnPc4d1	přírodní
barviva	barvivo	k1gNnPc4	barvivo
<g/>
.	.	kIx.	.
</s>
<s>
Smarties	Smarties	k1gInSc1	Smarties
Sfinx	sfinx	k1gInSc1	sfinx
Nestlé	Nestlý	k2eAgFnSc2d1	Nestlý
M	M	kA	M
<g/>
&	&	k?	&
<g/>
M	M	kA	M
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Reese	Reese	k1gFnPc1	Reese
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Pieces	Pieces	k1gMnSc1	Pieces
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Lentilky	lentilka	k1gFnSc2	lentilka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
</s>
