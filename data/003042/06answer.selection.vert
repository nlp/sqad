<s>
Lentilky	lentilka	k1gFnPc1	lentilka
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
firma	firma	k1gFnSc1	firma
Sfinx	sfinx	k1gFnSc1	sfinx
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
část	část	k1gFnSc4	část
koncernu	koncern	k1gInSc2	koncern
Nestlé	Nestlý	k2eAgNnSc1d1	Nestlé
<g/>
)	)	kIx)	)
v	v	k7c6	v
Holešově	Holešov	k1gInSc6	Holešov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vyráběly	vyrábět	k5eAaImAgFnP	vyrábět
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1907	[number]	k4	1907
a	a	k8xC	a
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
i	i	k9	i
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
