<p>
<s>
Deska	deska	k1gFnSc1	deska
Nazca	Nazc	k1gInSc2	Nazc
je	být	k5eAaImIp3nS	být
tektonická	tektonický	k2eAgFnSc1d1	tektonická
deska	deska	k1gFnSc1	deska
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
<g/>
,	,	kIx,	,
západně	západně	k6eAd1	západně
od	od	k7c2	od
pobřeží	pobřeží	k1gNnSc2	pobřeží
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
oceánskou	oceánský	k2eAgFnSc7d1	oceánská
zemskou	zemský	k2eAgFnSc7d1	zemská
kůrou	kůra	k1gFnSc7	kůra
<g/>
.	.	kIx.	.
</s>
<s>
Pojmenována	pojmenovat	k5eAaPmNgFnS	pojmenovat
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
regionu	region	k1gInSc2	region
Nazca	Nazc	k1gInSc2	Nazc
v	v	k7c6	v
Peru	Peru	k1gNnSc6	Peru
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Východní	východní	k2eAgFnSc1d1	východní
část	část	k1gFnSc1	část
desky	deska	k1gFnSc2	deska
je	být	k5eAaImIp3nS	být
aktivní	aktivní	k2eAgInSc4d1	aktivní
okraj	okraj	k1gInSc4	okraj
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
oceánská	oceánský	k2eAgFnSc1d1	oceánská
kůra	kůra	k1gFnSc1	kůra
desky	deska	k1gFnSc2	deska
tlačí	tlačit	k5eAaImIp3nS	tlačit
do	do	k7c2	do
subdukční	subdukční	k2eAgFnSc2d1	subdukční
zóny	zóna	k1gFnSc2	zóna
pod	pod	k7c4	pod
jihoamerickou	jihoamerický	k2eAgFnSc4d1	jihoamerická
desku	deska	k1gFnSc4	deska
a	a	k8xC	a
pohoří	pohoří	k1gNnSc4	pohoří
Andy	Anda	k1gFnSc2	Anda
vytvářejíc	vytvářet	k5eAaImSgFnS	vytvářet
peruánsko-chilský	peruánskohilský	k2eAgInSc4d1	peruánsko-chilský
příkop	příkop	k1gInSc4	příkop
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgInSc1d1	jižní
okraj	okraj	k1gInSc1	okraj
desky	deska	k1gFnSc2	deska
je	být	k5eAaImIp3nS	být
divergentní	divergentní	k2eAgNnSc1d1	divergentní
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
zde	zde	k6eAd1	zde
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
rozpínání	rozpínání	k1gNnSc3	rozpínání
oceánského	oceánský	k2eAgNnSc2d1	oceánské
dna	dno	k1gNnSc2	dno
na	na	k7c6	na
chilském	chilský	k2eAgInSc6d1	chilský
hřbetě	hřbet	k1gInSc6	hřbet
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
desku	deska	k1gFnSc4	deska
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
od	od	k7c2	od
Antarktické	antarktický	k2eAgFnSc2d1	antarktická
desky	deska	k1gFnSc2	deska
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgInSc1d1	západní
okraj	okraj	k1gInSc1	okraj
je	být	k5eAaImIp3nS	být
taktéž	taktéž	k?	taktéž
aktivní	aktivní	k2eAgFnSc1d1	aktivní
a	a	k8xC	a
dochází	docházet	k5eAaImIp3nS	docházet
tu	tu	k6eAd1	tu
k	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
nové	nový	k2eAgFnSc2d1	nová
oceánské	oceánský	k2eAgFnSc2d1	oceánská
kůry	kůra	k1gFnSc2	kůra
podél	podél	k7c2	podél
východopacifického	východopacifický	k2eAgInSc2d1	východopacifický
hřbetu	hřbet	k1gInSc2	hřbet
<g/>
.	.	kIx.	.
</s>
<s>
Hřbet	hřbet	k1gInSc1	hřbet
je	být	k5eAaImIp3nS	být
dělícím	dělící	k2eAgInSc7d1	dělící
prvkem	prvek	k1gInSc7	prvek
mezi	mezi	k7c7	mezi
deskou	deska	k1gFnSc7	deska
Nazca	Nazc	k1gInSc2	Nazc
a	a	k8xC	a
Pacifickou	pacifický	k2eAgFnSc7d1	Pacifická
deskou	deska	k1gFnSc7	deska
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgFnSc1d1	severní
strana	strana	k1gFnSc1	strana
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
divergentním	divergentní	k2eAgInSc7d1	divergentní
okrajem	okraj	k1gInSc7	okraj
oddělujícím	oddělující	k2eAgInSc7d1	oddělující
desku	deska	k1gFnSc4	deska
od	od	k7c2	od
kokosové	kokosový	k2eAgFnSc2d1	kokosová
desky	deska	k1gFnSc2	deska
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
rozcházení	rozcházení	k1gNnSc3	rozcházení
dna	dno	k1gNnSc2	dno
podél	podél	k7c2	podél
galapážského	galapážský	k2eAgInSc2d1	galapážský
hřbetu	hřbet	k1gInSc2	hřbet
<g/>
.	.	kIx.	.
</s>
<s>
Desku	deska	k1gFnSc4	deska
ohraničují	ohraničovat	k5eAaImIp3nP	ohraničovat
dva	dva	k4xCgInPc1	dva
trojné	trojný	k2eAgInPc1d1	trojný
body	bod	k1gInPc1	bod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Trojný	trojný	k2eAgInSc4d1	trojný
bod	bod	k1gInSc4	bod
styku	styk	k1gInSc2	styk
severních	severní	k2eAgFnPc2d1	severní
tří	tři	k4xCgFnPc2	tři
desek	deska	k1gFnPc2	deska
(	(	kIx(	(
<g/>
kokosové	kokosový	k2eAgFnSc2d1	kokosová
<g/>
,	,	kIx,	,
Nazca	Nazca	k1gMnSc1	Nazca
a	a	k8xC	a
pacifické	pacifický	k2eAgNnSc1d1	pacifické
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
západně	západně	k6eAd1	západně
od	od	k7c2	od
pobřeží	pobřeží	k1gNnSc2	pobřeží
Kolumbie	Kolumbie	k1gFnSc2	Kolumbie
<g/>
.	.	kIx.	.
</s>
<s>
Jižním	jižní	k2eAgInSc7d1	jižní
tzv.	tzv.	kA	tzv.
Chilským	chilský	k2eAgInSc7d1	chilský
trojným	trojný	k2eAgInSc7d1	trojný
bodem	bod	k1gInSc7	bod
(	(	kIx(	(
<g/>
styk	styk	k1gInSc1	styk
desek	deska	k1gFnPc2	deska
Nazca	Nazcum	k1gNnSc2	Nazcum
<g/>
,	,	kIx,	,
pacifické	pacifický	k2eAgFnSc2d1	Pacifická
a	a	k8xC	a
antarktické	antarktický	k2eAgFnSc2d1	antarktická
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
pobřeží	pobřeží	k1gNnSc2	pobřeží
Chile	Chile	k1gNnSc2	Chile
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
trojný	trojný	k2eAgInSc1d1	trojný
bod	bod	k1gInSc1	bod
je	být	k5eAaImIp3nS	být
nestandardní	standardní	k2eNgInSc1d1	nestandardní
i	i	k9	i
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
zde	zde	k6eAd1	zde
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
podsouvání	podsouvání	k1gNnSc3	podsouvání
středooceánského	středooceánský	k2eAgInSc2d1	středooceánský
hřbetu	hřbet	k1gInSc2	hřbet
(	(	kIx(	(
<g/>
chilský	chilský	k2eAgInSc1d1	chilský
hřbet	hřbet	k1gInSc1	hřbet
<g/>
)	)	kIx)	)
pod	pod	k7c4	pod
jihoamerickou	jihoamerický	k2eAgFnSc4d1	jihoamerická
desku	deska	k1gFnSc4	deska
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
této	tento	k3xDgFnSc2	tento
subdukční	subdukční	k2eAgFnSc2d1	subdukční
zóny	zóna	k1gFnSc2	zóna
mělo	mít	k5eAaImAgNnS	mít
hypocentrum	hypocentrum	k1gNnSc1	hypocentrum
i	i	k9	i
doposud	doposud	k6eAd1	doposud
nejsilnější	silný	k2eAgNnSc4d3	nejsilnější
zemětřesení	zemětřesení	k1gNnSc4	zemětřesení
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
(	(	kIx(	(
<g/>
místní	místní	k2eAgNnSc1d1	místní
magnitudo	magnitudo	k1gNnSc1	magnitudo
9,5	[number]	k4	9,5
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
22	[number]	k4	22
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1960	[number]	k4	1960
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
známé	známý	k2eAgNnSc4d1	známé
jako	jako	k8xS	jako
velké	velký	k2eAgNnSc4d1	velké
chilské	chilský	k2eAgNnSc4d1	Chilské
zemětřesení	zemětřesení	k1gNnSc4	zemětřesení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
každém	každý	k3xTgInSc6	každý
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
trojných	trojný	k2eAgInPc2d1	trojný
bodů	bod	k1gInPc2	bod
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
neobvyklá	obvyklý	k2eNgFnSc1d1	neobvyklá
menší	malý	k2eAgFnSc1d2	menší
mikrodeska	mikrodeska	k1gFnSc1	mikrodeska
<g/>
,	,	kIx,	,
na	na	k7c6	na
severu	sever	k1gInSc6	sever
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
galapážská	galapážský	k2eAgFnSc1d1	galapážská
deska	deska	k1gFnSc1	deska
a	a	k8xC	a
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
deska	deska	k1gFnSc1	deska
Juan	Juan	k1gMnSc1	Juan
Fernandéz	Fernandéza	k1gFnPc2	Fernandéza
<g/>
.	.	kIx.	.
</s>
<s>
Severně	severně	k6eAd1	severně
od	od	k7c2	od
desky	deska	k1gFnSc2	deska
Juan	Juan	k1gMnSc1	Juan
Fernandéz	Fernandéza	k1gFnPc2	Fernandéza
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
třetí	třetí	k4xOgFnSc1	třetí
mikrodeska	mikrodeska	k1gFnSc1	mikrodeska
Easter	Easter	k1gInSc4	Easter
Island	Island	k1gInSc1	Island
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
desce	deska	k1gFnSc6	deska
Nazca	Nazcus	k1gMnSc2	Nazcus
se	se	k3xPyFc4	se
mimo	mimo	k7c4	mimo
souostroví	souostroví	k1gNnSc4	souostroví
Juan	Juan	k1gMnSc1	Juan
Fernandéz	Fernandéza	k1gFnPc2	Fernandéza
nenachází	nacházet	k5eNaImIp3nS	nacházet
mnoho	mnoho	k4c1	mnoho
obydlených	obydlený	k2eAgInPc2d1	obydlený
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Topografii	topografie	k1gFnSc4	topografie
desky	deska	k1gFnSc2	deska
dominuje	dominovat	k5eAaImIp3nS	dominovat
tzv.	tzv.	kA	tzv.
Carnegiův	Carnegiův	k2eAgInSc4d1	Carnegiův
hřbet	hřbet	k1gInSc4	hřbet
<g/>
,	,	kIx,	,
asi	asi	k9	asi
1350	[number]	k4	1350
km	km	kA	km
dlouhý	dlouhý	k2eAgMnSc1d1	dlouhý
a	a	k8xC	a
do	do	k7c2	do
300	[number]	k4	300
km	km	kA	km
široký	široký	k2eAgInSc4d1	široký
pás	pás	k1gInSc4	pás
oceánského	oceánský	k2eAgNnSc2d1	oceánské
dna	dno	k1gNnSc2	dno
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
západním	západní	k2eAgInSc6d1	západní
konci	konec	k1gInSc6	konec
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
i	i	k9	i
souostroví	souostroví	k1gNnSc2	souostroví
Galapágy	Galapágy	k1gFnPc1	Galapágy
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
kokosový	kokosový	k2eAgInSc4d1	kokosový
hřbet	hřbet	k1gInSc4	hřbet
a	a	k8xC	a
hřbet	hřbet	k1gInSc4	hřbet
Malpelo	Malpela	k1gFnSc5	Malpela
jsou	být	k5eAaImIp3nP	být
zřejmě	zřejmě	k6eAd1	zřejmě
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
po	po	k7c6	po
pohybu	pohyb	k1gInSc6	pohyb
horké	horký	k2eAgFnSc2d1	horká
skvrny	skvrna	k1gFnSc2	skvrna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pohyb	pohyb	k1gInSc1	pohyb
desky	deska	k1gFnSc2	deska
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
jihoamerické	jihoamerický	k2eAgFnSc3d1	jihoamerická
desce	deska	k1gFnSc3	deska
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
kterou	který	k3yIgFnSc7	který
subdukuje	subdukovat	k5eAaImIp3nS	subdukovat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
kolem	kolem	k7c2	kolem
3,7	[number]	k4	3,7
cm	cm	kA	cm
<g/>
/	/	kIx~	/
<g/>
rok	rok	k1gInSc1	rok
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
východ	východ	k1gInSc4	východ
<g/>
.	.	kIx.	.
</s>
<s>
Nazca	Nazca	k1gFnSc1	Nazca
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejrychleji	rychle	k6eAd3	rychle
se	se	k3xPyFc4	se
pohybujících	pohybující	k2eAgFnPc2d1	pohybující
tektonických	tektonický	k2eAgFnPc2d1	tektonická
desek	deska	k1gFnPc2	deska
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
==	==	k?	==
</s>
</p>
<p>
<s>
Předchůdcem	předchůdce	k1gMnSc7	předchůdce
desky	deska	k1gFnSc2	deska
Nazca	Nazc	k1gInSc2	Nazc
byla	být	k5eAaImAgFnS	být
farallonská	farallonský	k2eAgFnSc1d1	farallonský
deska	deska	k1gFnSc1	deska
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
před	před	k7c7	před
asi	asi	k9	asi
23	[number]	k4	23
milióny	milión	k4xCgInPc1	milión
let	léto	k1gNnPc2	léto
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
oligocénu	oligocén	k1gInSc2	oligocén
a	a	k8xC	a
miocénu	miocén	k1gInSc2	miocén
<g/>
.	.	kIx.	.
</s>
<s>
Rozpadem	rozpad	k1gInSc7	rozpad
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
desky	deska	k1gFnPc1	deska
Nazca	Nazca	k1gFnSc1	Nazca
a	a	k8xC	a
kokosová	kokosový	k2eAgFnSc1d1	kokosová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Platňa	Platňus	k1gMnSc2	Platňus
Nazca	Nazcus	k1gMnSc2	Nazcus
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
