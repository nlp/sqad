<s>
Ferdinand	Ferdinand	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kastilský	kastilský	k2eAgInSc1d1
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kastilský	kastilský	k2eAgInSc1d1
</s>
<s>
Král	Král	k1gMnSc1
Kastilie	Kastilie	k1gFnSc2
a	a	k8xC
Toleda	Toledo	k1gNnSc2
</s>
<s>
Období	období	k1gNnSc1
</s>
<s>
31	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1217	#num#	k4
<g/>
–	–	k?
<g/>
30	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1252	#num#	k4
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
</s>
<s>
Berenguela	Berenguela	k1gFnSc1
</s>
<s>
Nástupce	nástupce	k1gMnSc1
</s>
<s>
Alfons	Alfons	k1gMnSc1
X.	X.	kA
</s>
<s>
Král	Král	k1gMnSc1
Leónu	León	k1gInSc2
a	a	k8xC
Galicie	Galicie	k1gFnSc2
</s>
<s>
Období	období	k1gNnSc1
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1230	#num#	k4
<g/>
–	–	k?
<g/>
30	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1252	#num#	k4
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
</s>
<s>
Alfons	Alfons	k1gMnSc1
IX	IX	kA
<g/>
.	.	kIx.
</s>
<s>
Nástupce	nástupce	k1gMnSc1
</s>
<s>
Alfons	Alfons	k1gMnSc1
X.	X.	kA
</s>
<s>
Narození	narození	k1gNnSc1
</s>
<s>
30	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
nebo	nebo	k8xC
5	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1199	#num#	k4
<g/>
?	?	kIx.
</s>
<s>
Zamora	Zamora	k1gFnSc1
</s>
<s>
Úmrtí	úmrtí	k1gNnSc1
</s>
<s>
30	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1252	#num#	k4
(	(	kIx(
<g/>
52	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
Sevilla	Sevilla	k1gFnSc1
</s>
<s>
Pohřben	pohřben	k2eAgInSc1d1
</s>
<s>
Katedrála	katedrála	k1gFnSc1
v	v	k7c6
Seville	Sevilla	k1gFnSc6
</s>
<s>
Manželka	manželka	k1gFnSc1
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
Štaufská	Štaufský	k2eAgFnSc1d1
</s>
<s>
Johana	Johana	k1gFnSc1
z	z	k7c2
Ponthieu	Ponthieus	k1gInSc2
</s>
<s>
Potomci	potomek	k1gMnPc1
</s>
<s>
Alfons	Alfons	k1gMnSc1
X.	X.	kA
<g/>
FridrichFerdinandEleonoraBerengariaJindřichFilipSanchoManuelMarieFerdinandEleonoraLudvíkŠimonJan	FridrichFerdinandEleonoraBerengariaJindřichFilipSanchoManuelMarieFerdinandEleonoraLudvíkŠimonJan	k1gMnSc1
</s>
<s>
Dynastie	dynastie	k1gFnSc1
</s>
<s>
Burgundsko-Ivrejská	Burgundsko-Ivrejský	k2eAgFnSc1d1
dynastie	dynastie	k1gFnSc1
</s>
<s>
Otec	otec	k1gMnSc1
</s>
<s>
Alfons	Alfons	k1gMnSc1
IX	IX	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leónský	Leónský	k1gMnSc1
</s>
<s>
Matka	matka	k1gFnSc1
</s>
<s>
Berenguela	Berenguela	k1gFnSc1
Kastilská	kastilský	k2eAgFnSc1d1
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kastilský	kastilský	k2eAgInSc1d1
zvaný	zvaný	k2eAgInSc1d1
Svatý	svatý	k2eAgInSc1d1
(	(	kIx(
<g/>
30	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
nebo	nebo	k8xC
5	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1199	#num#	k4
<g/>
?	?	kIx.
</s>
<s desamb="1">
–	–	k?
30	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1252	#num#	k4
Sevilla	Sevilla	k1gFnSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
králem	král	k1gMnSc7
Kastilie	Kastilie	k1gFnSc2
od	od	k7c2
roku	rok	k1gInSc2
1217	#num#	k4
a	a	k8xC
Leónu	Leóna	k1gFnSc4
od	od	k7c2
roku	rok	k1gInSc2
1230	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1231	#num#	k4
obě	dva	k4xCgFnPc4
království	království	k1gNnPc2
trvale	trvale	k6eAd1
sjednotil	sjednotit	k5eAaPmAgInS
<g/>
,	,	kIx,
a	a	k8xC
pomohl	pomoct	k5eAaPmAgMnS
tak	tak	k6eAd1
položit	položit	k5eAaPmF
základ	základ	k1gInSc4
Kastilské	kastilský	k2eAgFnSc2d1
koruně	koruna	k1gFnSc3
a	a	k8xC
následně	následně	k6eAd1
i	i	k9
dnešního	dnešní	k2eAgNnSc2d1
Španělska	Španělsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úspěšně	úspěšně	k6eAd1
pokračoval	pokračovat	k5eAaImAgInS
v	v	k7c6
reconquistě	reconquist	k1gInSc6
<g/>
,	,	kIx,
znovudobývání	znovudobývání	k1gNnSc4
území	území	k1gNnSc2
obsazených	obsazený	k2eAgFnPc2d1
Maury	Maur	k1gMnPc4
<g/>
,	,	kIx,
a	a	k8xC
podařilo	podařit	k5eAaPmAgNnS
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
získat	získat	k5eAaPmF
velká	velký	k2eAgNnPc1d1
území	území	k1gNnPc1
na	na	k7c6
jihu	jih	k1gInSc6
Pyrenejského	pyrenejský	k2eAgInSc2d1
poloostrova	poloostrov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
oba	dva	k4xCgMnPc1
rodiče	rodič	k1gMnPc1
pocházel	pocházet	k5eAaImAgMnS
z	z	k7c2
burgundsko-ivrejské	burgundsko-ivrejský	k2eAgFnSc2d1
dynastie	dynastie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Během	během	k7c2
Ferdinandovy	Ferdinandův	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
vzrostla	vzrůst	k5eAaPmAgFnS
politická	politický	k2eAgFnSc1d1
moc	moc	k1gFnSc1
měst	město	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
začala	začít	k5eAaPmAgFnS
zúčastňovat	zúčastňovat	k5eAaImF
dvorských	dvorský	k2eAgInPc2d1
sněmů	sněm	k1gInPc2
<g/>
,	,	kIx,
král	král	k1gMnSc1
podporoval	podporovat	k5eAaImAgMnS
rozvoj	rozvoj	k1gInSc4
univerzity	univerzita	k1gFnSc2
v	v	k7c6
Salamance	Salamanka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zemřel	zemřít	k5eAaPmAgMnS
roku	rok	k1gInSc2
1252	#num#	k4
a	a	k8xC
na	na	k7c6
jeho	jeho	k3xOp3gInSc6
náhrobku	náhrobek	k1gInSc6
v	v	k7c6
sevillské	sevillský	k2eAgFnSc6d1
katedrále	katedrála	k1gFnSc6
byly	být	k5eAaImAgInP
nápisy	nápis	k1gInPc1
v	v	k7c6
kastilštině	kastilština	k1gFnSc6
<g/>
,	,	kIx,
latině	latina	k1gFnSc6
<g/>
,	,	kIx,
hebrejštině	hebrejština	k1gFnSc6
a	a	k8xC
arabštině	arabština	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Roku	rok	k1gInSc2
1671	#num#	k4
byl	být	k5eAaImAgMnS
prohlášen	prohlásit	k5eAaPmNgMnS
za	za	k7c4
svatého	svatý	k1gMnSc4
a	a	k8xC
ve	v	k7c6
Španělsku	Španělsko	k1gNnSc6
jej	on	k3xPp3gInSc4
uctívají	uctívat	k5eAaImIp3nP
pod	pod	k7c7
jménem	jméno	k1gNnSc7
Fernando	Fernanda	k1gFnSc5
el	ela	k1gFnPc2
Santo	Santo	k1gNnSc1
nebo	nebo	k8xC
San	San	k1gFnSc1
Fernando	Fernanda	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s>
Vývod	vývod	k1gInSc1
z	z	k7c2
předků	předek	k1gInPc2
</s>
<s>
Raimund	Raimunda	k1gFnPc2
Burgundský	burgundský	k2eAgInSc1d1
</s>
<s>
Alfons	Alfons	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kastilský	kastilský	k2eAgInSc1d1
</s>
<s>
Urraca	Urrac	k2eAgFnSc1d1
Kastilská	kastilský	k2eAgFnSc1d1
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leónský	Leónský	k1gMnSc1
</s>
<s>
Ramon	Ramona	k1gFnPc2
Berenguer	Berengura	k1gFnPc2
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Barcelonský	barcelonský	k2eAgInSc1d1
</s>
<s>
Berenguela	Berenguela	k1gFnSc1
Barcelonská	barcelonský	k2eAgFnSc1d1
</s>
<s>
Dulce	Dulko	k6eAd1
Provensálská	provensálský	k2eAgFnSc1d1
</s>
<s>
Alfons	Alfons	k1gMnSc1
IX	IX	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leónský	Leónský	k1gMnSc1
</s>
<s>
Jindřich	Jindřich	k1gMnSc1
Burgundský	burgundský	k2eAgMnSc1d1
</s>
<s>
Alfons	Alfons	k1gMnSc1
I.	I.	kA
Portugalský	portugalský	k2eAgMnSc1d1
</s>
<s>
Tereza	Tereza	k1gFnSc1
Kastilská	kastilský	k2eAgFnSc1d1
</s>
<s>
Urraca	Urrac	k2eAgFnSc1d1
Portugalská	portugalský	k2eAgFnSc1d1
</s>
<s>
Amadeus	Amadeus	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Savojský	savojský	k2eAgInSc1d1
</s>
<s>
Matilda	Matilda	k1gFnSc1
Savojská	savojský	k2eAgFnSc1d1
</s>
<s>
Matylda	Matylda	k1gFnSc1
z	z	k7c2
Albonu	Albon	k1gInSc2
</s>
<s>
'	'	kIx"
<g/>
Ferdinand	Ferdinand	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kastilský	kastilský	k2eAgInSc1d1
<g/>
'	'	kIx"
</s>
<s>
Alfons	Alfons	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kastilský	kastilský	k2eAgInSc1d1
</s>
<s>
Sancho	Sancho	k6eAd1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kastilský	kastilský	k2eAgInSc1d1
</s>
<s>
Berenguela	Berenguela	k1gFnSc1
Barcelonská	barcelonský	k2eAgFnSc1d1
</s>
<s>
Alfons	Alfons	k1gMnSc1
VIII	VIII	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kastilský	kastilský	k2eAgInSc1d1
</s>
<s>
García	García	k1gFnSc1
VI	VI	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navarrský	navarrský	k2eAgInSc1d1
</s>
<s>
Blanka	Blanka	k1gFnSc1
Navarrská	navarrský	k2eAgFnSc1d1
</s>
<s>
Markéta	Markéta	k1gFnSc1
z	z	k7c2
Aigle	Aigle	k1gFnSc2
</s>
<s>
Berenguela	Berenguela	k1gFnSc1
Kastilská	kastilský	k2eAgFnSc1d1
</s>
<s>
Geoffroy	Geoffroa	k1gFnPc1
V.	V.	kA
z	z	k7c2
Anjou	Anjý	k2eAgFnSc4d1
</s>
<s>
Jindřich	Jindřich	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plantagenet	Plantagenet	k1gInSc1
</s>
<s>
Matylda	Matylda	k1gFnSc1
Anglická	anglický	k2eAgFnSc1d1
</s>
<s>
Eleonora	Eleonora	k1gFnSc1
Anglická	anglický	k2eAgFnSc1d1
</s>
<s>
Vilém	Vilém	k1gMnSc1
X.	X.	kA
Akvitánský	Akvitánský	k2eAgMnSc5d1
</s>
<s>
Eleonora	Eleonora	k1gFnSc1
Akvitánská	Akvitánský	k2eAgFnSc1d1
</s>
<s>
Eleonora	Eleonora	k1gFnSc1
ze	z	k7c2
Châtelleraultu	Châtellerault	k1gInSc2
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
MUNDY	MUNDY	kA
<g/>
,	,	kIx,
John	John	k1gMnSc1
Hine	hin	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Evropa	Evropa	k1gFnSc1
vrcholného	vrcholný	k2eAgInSc2d1
středověku	středověk	k1gInSc2
1150	#num#	k4
<g/>
-	-	kIx~
<g/>
1300	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Vyšehrad	Vyšehrad	k1gInSc1
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7021	#num#	k4
<g/>
-	-	kIx~
<g/>
927	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
60	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
jen	jen	k9
Evropa	Evropa	k1gFnSc1
vrcholného	vrcholný	k2eAgInSc2d1
středověku	středověk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
SCHAUBER	SCHAUBER	kA
<g/>
,	,	kIx,
Vera	Vera	k1gMnSc1
<g/>
;	;	kIx,
SCHINDLER	Schindler	k1gMnSc1
<g/>
,	,	kIx,
Hanns	Hanns	k1gInSc1
Michael	Michaela	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rok	rok	k1gInSc1
se	s	k7c7
svatými	svatá	k1gFnPc7
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kostelní	kostelní	k2eAgInSc1d1
Vydří	vydří	k2eAgInSc1d1
<g/>
:	:	kIx,
Karmelitánské	karmelitánský	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
.	.	kIx.
702	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7192	#num#	k4
<g/>
-	-	kIx~
<g/>
304	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Ferdinand	Ferdinand	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kastilský	kastilský	k2eAgInSc4d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Berenguela	Berenguela	k1gFnSc1
</s>
<s>
Kastilský	kastilský	k2eAgMnSc1d1
král	král	k1gMnSc1
1217	#num#	k4
<g/>
–	–	k?
<g/>
1252	#num#	k4
</s>
<s>
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Alfons	Alfons	k1gMnSc1
X.	X.	kA
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Alfons	Alfons	k1gMnSc1
IX	IX	kA
<g/>
.	.	kIx.
</s>
<s>
Leónský	Leónský	k2eAgMnSc1d1
král	král	k1gMnSc1
1230	#num#	k4
<g/>
–	–	k?
<g/>
1252	#num#	k4
</s>
<s>
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Alfons	Alfons	k1gMnSc1
X.	X.	kA
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Alfons	Alfons	k1gMnSc1
IX	IX	kA
<g/>
.	.	kIx.
</s>
<s>
Galicijský	Galicijský	k2eAgMnSc1d1
král	král	k1gMnSc1
1230	#num#	k4
<g/>
–	–	k?
<g/>
1252	#num#	k4
</s>
<s>
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Alfons	Alfons	k1gMnSc1
X.	X.	kA
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jx	jx	k?
<g/>
20050919002	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
119146193	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
8959	#num#	k4
7747	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
2002113012	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
14448956	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
2002113012	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k5eAaPmF
<g/>
:	:	kIx,
<g/>
bold	bold	k6eAd1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc7
<g/>
:	:	kIx,
Křížové	Křížové	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
|	|	kIx~
Středověk	středověk	k1gInSc1
|	|	kIx~
Španělsko	Španělsko	k1gNnSc1
</s>
