<s>
Neptunium	neptunium	k1gNnSc1
</s>
<s>
Neptunium	neptunium	k1gNnSc1
</s>
<s>
[	[	kIx(
<g/>
Rn	Rn	k1gMnSc6
<g/>
]	]	kIx)
5	#num#	k4
<g/>
f	f	k?
<g/>
4	#num#	k4
6	#num#	k4
<g/>
d	d	k?
<g/>
1	#num#	k4
7	#num#	k4
<g/>
s	s	k7c7
<g/>
2	#num#	k4
</s>
<s>
237	#num#	k4
</s>
<s>
Np	Np	kA
</s>
<s>
93	#num#	k4
</s>
<s>
↓	↓	k?
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
↓	↓	k?
</s>
<s>
koule	koule	k1gFnSc1
z	z	k7c2
neptunia	neptunium	k1gNnSc2
</s>
<s>
Obecné	obecný	k2eAgNnSc1d1
</s>
<s>
Název	název	k1gInSc1
<g/>
,	,	kIx,
značka	značka	k1gFnSc1
<g/>
,	,	kIx,
číslo	číslo	k1gNnSc1
</s>
<s>
Neptunium	neptunium	k1gNnSc1
<g/>
,	,	kIx,
Np	Np	k1gFnSc1
<g/>
,	,	kIx,
93	#num#	k4
</s>
<s>
Cizojazyčné	cizojazyčný	k2eAgInPc1d1
názvy	název	k1gInPc1
</s>
<s>
lat.	lat.	k?
Neptunium	neptunium	k1gNnSc1
</s>
<s>
Skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
f	f	k?
</s>
<s>
Chemická	chemický	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
</s>
<s>
Aktinoidy	Aktinoida	k1gFnPc1
</s>
<s>
Koncentrace	koncentrace	k1gFnPc1
v	v	k7c6
zemské	zemský	k2eAgFnSc6d1
kůře	kůra	k1gFnSc6
</s>
<s>
4	#num#	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
13	#num#	k4
ppm	ppm	k?
</s>
<s>
Vzhled	vzhled	k1gInSc1
</s>
<s>
stříbrobílý	stříbrobílý	k2eAgInSc1d1
kov	kov	k1gInSc1
</s>
<s>
Identifikace	identifikace	k1gFnSc1
</s>
<s>
Registrační	registrační	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
CAS	CAS	kA
</s>
<s>
7439-99-8	7439-99-8	k4
</s>
<s>
Atomové	atomový	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Relativní	relativní	k2eAgFnSc1d1
atomová	atomový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
</s>
<s>
(	(	kIx(
<g/>
230,048	230,048	k4
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Atomový	atomový	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
</s>
<s>
175	#num#	k4
pm	pm	k?
</s>
<s>
Kovalentní	kovalentní	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
</s>
<s>
150	#num#	k4
pm	pm	k?
</s>
<s>
Iontový	iontový	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
</s>
<s>
(	(	kIx(
<g/>
Np	Np	k1gFnPc2
<g/>
3	#num#	k4
<g/>
+	+	kIx~
<g/>
)	)	kIx)
104	#num#	k4
pm	pm	k?
<g/>
(	(	kIx(
<g/>
Np	Np	k1gFnSc1
<g/>
4	#num#	k4
<g/>
+	+	kIx~
<g/>
)	)	kIx)
91	#num#	k4
pm	pm	k?
<g/>
(	(	kIx(
<g/>
Np	Np	k1gFnSc1
<g/>
5	#num#	k4
<g/>
+	+	kIx~
<g/>
)	)	kIx)
82	#num#	k4
pm	pm	k?
<g/>
(	(	kIx(
<g/>
Np	Np	k1gFnSc1
<g/>
6	#num#	k4
<g/>
+	+	kIx~
<g/>
)	)	kIx)
76	#num#	k4
pm	pm	k?
</s>
<s>
Elektronová	elektronový	k2eAgFnSc1d1
konfigurace	konfigurace	k1gFnSc1
</s>
<s>
[	[	kIx(
<g/>
Rn	Rn	k1gMnSc6
<g/>
]	]	kIx)
5	#num#	k4
<g/>
f	f	k?
<g/>
4	#num#	k4
6	#num#	k4
<g/>
d	d	k?
<g/>
1	#num#	k4
7	#num#	k4
<g/>
s	s	k7c7
<g/>
2	#num#	k4
</s>
<s>
Oxidační	oxidační	k2eAgNnPc1d1
čísla	číslo	k1gNnPc1
</s>
<s>
III	III	kA
<g/>
,	,	kIx,
IV	IV	kA
<g/>
,	,	kIx,
V	V	kA
<g/>
,	,	kIx,
VI	VI	kA
<g/>
,	,	kIx,
VII	VII	kA
</s>
<s>
Elektronegativita	Elektronegativita	k1gFnSc1
(	(	kIx(
<g/>
Paulingova	Paulingův	k2eAgFnSc1d1
stupnice	stupnice	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1,36	1,36	k4
</s>
<s>
Ionizační	ionizační	k2eAgFnSc1d1
energie	energie	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc1
</s>
<s>
5,90	5,90	k4
eV	eV	k?
</s>
<s>
Druhá	druhý	k4xOgFnSc1
</s>
<s>
11,67	11,67	k4
eV	eV	k?
</s>
<s>
Třetí	třetí	k4xOgFnSc1
</s>
<s>
22	#num#	k4
eV	eV	k?
</s>
<s>
Čtvrtá	čtvrtý	k4xOgFnSc1
</s>
<s>
38	#num#	k4
eV	eV	k?
</s>
<s>
Látkové	látkový	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Krystalografická	krystalografický	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
</s>
<s>
α	α	k1gFnSc1
kosočtverečná	kosočtverečný	k2eAgFnSc1d1
a	a	k8xC
<g/>
=	=	kIx~
472,3	472,3	k4
pm	pm	k?
b	b	k?
<g/>
=	=	kIx~
488,7	488,7	k4
pm	pm	k?
c	c	k0
<g/>
=	=	kIx~
666,3	666,3	k4
pm	pm	k?
β	β	k1gFnSc1
čtverečná	čtverečný	k2eAgFnSc1d1
a	a	k8xC
<g/>
=	=	kIx~
488,3	488,3	k4
pm	pm	k?
c	c	k0
<g/>
=	=	kIx~
338,9	338,9	k4
pm	pm	k?
γ	γ	k1gFnSc1
krychlová	krychlový	k2eAgFnSc1d1
tělesně	tělesně	k6eAd1
centrovaná	centrovaný	k2eAgFnSc1d1
a	a	k8xC
<g/>
=	=	kIx~
<g/>
352	#num#	k4
pm	pm	k?
</s>
<s>
Molární	molární	k2eAgInSc1d1
objem	objem	k1gInSc1
</s>
<s>
11,59	11,59	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
6	#num#	k4
m	m	kA
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
mol	mol	k1gInSc1
</s>
<s>
Teplota	teplota	k1gFnSc1
změny	změna	k1gFnSc2
modifikace	modifikace	k1gFnSc2
</s>
<s>
277	#num#	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
α	α	k?
→	→	k?
β	β	k?
<g/>
)	)	kIx)
575	#num#	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
β	β	k?
→	→	k?
γ	γ	k?
<g/>
)	)	kIx)
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
550,15	550,15	k4
K	k	k7c3
<g/>
)	)	kIx)
</s>
<s>
Mechanické	mechanický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Hustota	hustota	k1gFnSc1
</s>
<s>
20,464	20,464	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
cm	cm	kA
<g/>
3	#num#	k4
(	(	kIx(
<g/>
mod.	mod.	k?
α	α	k?
<g/>
,	,	kIx,
20	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
19,369	19,369	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
cm	cm	kA
<g/>
3	#num#	k4
(	(	kIx(
<g/>
mod.	mod.	k?
β	β	k?
<g/>
,	,	kIx,
313	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
18,00	18,00	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
cm	cm	kA
<g/>
3	#num#	k4
(	(	kIx(
<g/>
mod.	mod.	k?
γ	γ	k?
<g/>
,	,	kIx,
580	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
</s>
<s>
Skupenství	skupenství	k1gNnSc1
</s>
<s>
pevné	pevný	k2eAgNnSc1d1
</s>
<s>
Termické	termický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Tepelná	tepelný	k2eAgFnSc1d1
vodivost	vodivost	k1gFnSc1
</s>
<s>
(	(	kIx(
<g/>
27	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
6,3	6,3	k4
W	W	kA
<g/>
⋅	⋅	k?
<g/>
m	m	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
<g/>
⋅	⋅	k?
<g/>
K	K	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
</s>
<s>
Molární	molární	k2eAgFnSc1d1
atomizační	atomizační	k2eAgFnSc1d1
entalpie	entalpie	k1gFnSc1
</s>
<s>
394,8	394,8	k4
</s>
<s>
Standardní	standardní	k2eAgFnSc1d1
molární	molární	k2eAgFnSc1d1
entropie	entropie	k1gFnSc1
S	s	k7c7
<g/>
°	°	k?
</s>
<s>
50,6	50,6	k4
J	J	kA
K	K	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
mol	mol	k1gInSc1
<g/>
−	−	k?
<g/>
1	#num#	k4
(	(	kIx(
<g/>
mod.	mod.	k?
α	α	k?
<g/>
)	)	kIx)
</s>
<s>
Termodynamické	termodynamický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Teplota	teplota	k1gFnSc1
tání	tání	k1gNnSc2
</s>
<s>
640	#num#	k4
±	±	k?
1	#num#	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
913,15	913,15	k4
K	k	k7c3
<g/>
)	)	kIx)
</s>
<s>
Teplota	teplota	k1gFnSc1
varu	var	k1gInSc2
</s>
<s>
2	#num#	k4
250	#num#	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
2	#num#	k4
523,15	523,15	k4
K	k	k7c3
<g/>
)	)	kIx)
</s>
<s>
Specifické	specifický	k2eAgNnSc4d1
teplo	teplo	k1gNnSc4
tání	tání	k1gNnSc2
</s>
<s>
41	#num#	k4
J	J	kA
<g/>
/	/	kIx~
<g/>
g	g	kA
</s>
<s>
Specifické	specifický	k2eAgNnSc4d1
teplo	teplo	k1gNnSc4
varu	var	k1gInSc2
</s>
<s>
970	#num#	k4
J	J	kA
<g/>
/	/	kIx~
<g/>
g	g	kA
</s>
<s>
Entalpie	entalpie	k1gFnSc1
změny	změna	k1gFnSc2
modifikace	modifikace	k1gFnSc2
Δ	Δ	k?
<g/>
→	→	k?
<g/>
β	β	k?
</s>
<s>
35	#num#	k4
kJ	kJ	k?
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
(	(	kIx(
<g/>
α	α	k?
→	→	k?
β	β	k?
<g/>
)	)	kIx)
</s>
<s>
Molární	molární	k2eAgFnSc1d1
tepelná	tepelný	k2eAgFnSc1d1
kapacita	kapacita	k1gFnSc1
</s>
<s>
0,124	0,124	k4
J	J	kA
K	K	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
mol	mol	k1gInSc1
<g/>
−	−	k?
<g/>
1	#num#	k4
(	(	kIx(
<g/>
25	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
<g/>
0,131	0,131	k4
J	J	kA
K	K	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
mol	mol	k1gInSc1
<g/>
−	−	k?
<g/>
1	#num#	k4
(	(	kIx(
<g/>
60	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
<g/>
0,168	0,168	k4
J	J	kA
K	K	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
mol	mol	k1gInSc1
<g/>
−	−	k?
<g/>
1	#num#	k4
(	(	kIx(
<g/>
207	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
</s>
<s>
Elektromagnetické	elektromagnetický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Elektrická	elektrický	k2eAgFnSc1d1
vodivost	vodivost	k1gFnSc1
</s>
<s>
0,82	0,82	k4
S	s	k7c7
<g/>
/	/	kIx~
<g/>
m	m	kA
</s>
<s>
Měrný	měrný	k2eAgInSc4d1
elektrický	elektrický	k2eAgInSc4d1
odpor	odpor	k1gInSc4
</s>
<s>
115	#num#	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
8	#num#	k4
Ω	Ω	k?
m	m	kA
(	(	kIx(
<g/>
mod.	mod.	k?
α	α	k?
<g/>
,	,	kIx,
20	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
105	#num#	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
8	#num#	k4
Ω	Ω	k?
m	m	kA
(	(	kIx(
<g/>
mod.	mod.	k?
β	β	k?
<g/>
,	,	kIx,
280	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
110	#num#	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
8	#num#	k4
Ω	Ω	k?
m	m	kA
(	(	kIx(
<g/>
mod.	mod.	k?
γ	γ	k?
<g/>
,	,	kIx,
580	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
</s>
<s>
Teplotní	teplotní	k2eAgMnSc1d1
součinitel	součinitel	k1gMnSc1
elektrického	elektrický	k2eAgInSc2d1
odporu	odpor	k1gInSc2
</s>
<s>
0,000	0,000	k4
43	#num#	k4
K	K	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
</s>
<s>
Standardní	standardní	k2eAgInSc1d1
elektrodový	elektrodový	k2eAgInSc1d1
potenciál	potenciál	k1gInSc1
</s>
<s>
(	(	kIx(
<g/>
Np	Np	k1gFnPc2
<g/>
3	#num#	k4
<g/>
+	+	kIx~
→	→	k?
Np	Np	k1gFnSc7
<g/>
0	#num#	k4
<g/>
)	)	kIx)
−	−	k?
V	V	kA
(	(	kIx(
<g/>
Np	Np	k1gFnSc1
<g/>
4	#num#	k4
<g/>
+	+	kIx~
→	→	k?
Np	Np	k1gFnSc7
<g/>
3	#num#	k4
<g/>
+	+	kIx~
<g/>
)	)	kIx)
0,147	0,147	k4
V	v	k7c6
</s>
<s>
Magnetické	magnetický	k2eAgNnSc1d1
chování	chování	k1gNnSc1
</s>
<s>
paramagnetický	paramagnetický	k2eAgInSc1d1
</s>
<s>
Bezpečnost	bezpečnost	k1gFnSc1
</s>
<s>
Radioaktivní	radioaktivní	k2eAgFnSc1d1
</s>
<s>
I	i	k9
</s>
<s>
V	v	k7c6
(	(	kIx(
<g/>
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
S	s	k7c7
</s>
<s>
T	T	kA
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
E	E	kA
(	(	kIx(
<g/>
MeV	MeV	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
P	P	kA
</s>
<s>
{{{	{{{	k?
<g/>
izotopy	izotop	k1gInPc1
<g/>
}}}	}}}	k?
</s>
<s>
Není	být	k5eNaImIp3nS
<g/>
-li	-li	k?
uvedeno	uvést	k5eAaPmNgNnS
jinak	jinak	k6eAd1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
použityjednotky	použityjednotka	k1gFnPc1
SI	si	k1gNnSc2
a	a	k8xC
STP	STP	kA
(	(	kIx(
<g/>
25	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
100	#num#	k4
kPa	kPa	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Pm	Pm	k?
<g/>
⋏	⋏	k?
</s>
<s>
Uran	Uran	k1gInSc1
≺	≺	k?
<g/>
Np	Np	k1gFnSc2
<g/>
≻	≻	k?
Plutonium	plutonium	k1gNnSc1
</s>
<s>
Neptunium	neptunium	k1gNnSc1
(	(	kIx(
<g/>
chemická	chemický	k2eAgFnSc1d1
značka	značka	k1gFnSc1
Np	Np	k1gFnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
prvek	prvek	k1gInSc1
s	s	k7c7
protonovým	protonový	k2eAgNnSc7d1
číslem	číslo	k1gNnSc7
93	#num#	k4
a	a	k8xC
je	být	k5eAaImIp3nS
prvním	první	k4xOgMnSc7
z	z	k7c2
řady	řada	k1gFnSc2
transuranů	transuran	k1gInPc2
a	a	k8xC
pátým	pátý	k4xOgInSc7
prvkem	prvek	k1gInSc7
z	z	k7c2
řady	řada	k1gFnSc2
aktinoidů	aktinoid	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgInS
objeven	objeven	k2eAgInSc1d1
roku	rok	k1gInSc2
1940	#num#	k4
McMillanem	McMillan	k1gMnSc7
a	a	k8xC
Abelsonem	Abelson	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
o	o	k7c4
umělý	umělý	k2eAgInSc4d1
radioaktivní	radioaktivní	k2eAgInSc4d1
kov	kov	k1gInSc4
stříbrné	stříbrný	k2eAgFnSc2d1
barvy	barva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgInPc1
pokusy	pokus	k1gInPc1
o	o	k7c4
přípravu	příprava	k1gFnSc4
jeho	jeho	k3xOp3gInPc2
izotopů	izotop	k1gInPc2
byly	být	k5eAaImAgFnP
provedeny	provést	k5eAaPmNgFnP
dříve	dříve	k6eAd2
<g/>
,	,	kIx,
v	v	k7c6
polovině	polovina	k1gFnSc6
třicátých	třicátý	k4xOgNnPc2
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navržená	navržený	k2eAgFnSc1d1
metoda	metoda	k1gFnSc1
využívala	využívat	k5eAaImAgFnS,k5eAaPmAgFnS
faktu	fakt	k1gInSc3
<g/>
,	,	kIx,
že	že	k8xS
záchytem	záchyt	k1gInSc7
neutronu	neutron	k1gInSc2
často	často	k6eAd1
vzniká	vznikat	k5eAaImIp3nS
β	β	k?
<g/>
−	−	k?
aktivní	aktivní	k2eAgInSc4d1
izotop	izotop	k1gInSc4
a	a	k8xC
následně	následně	k6eAd1
těžší	těžký	k2eAgInSc4d2
prvek	prvek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1940	#num#	k4
zjistili	zjistit	k5eAaPmAgMnP
Edwin	Edwin	k2eAgInSc4d1
McMillan	McMillan	k1gInSc4
a	a	k8xC
Philip	Philip	k1gInSc4
H.	H.	kA
Abelson	Abelson	k1gNnSc1
<g/>
,	,	kIx,
že	že	k8xS
při	při	k7c6
bombardování	bombardování	k1gNnSc6
tenké	tenký	k2eAgFnSc2d1
uranové	uranový	k2eAgFnSc2d1
fólie	fólie	k1gFnSc2
neutrony	neutron	k1gInPc1
většina	většina	k1gFnSc1
štěpných	štěpný	k2eAgInPc2d1
produktů	produkt	k1gInPc2
odletí	odletět	k5eAaPmIp3nP
<g/>
,	,	kIx,
ale	ale	k8xC
dvě	dva	k4xCgFnPc1
radioaktivní	radioaktivní	k2eAgFnPc1d1
látky	látka	k1gFnPc1
(	(	kIx(
<g/>
s	s	k7c7
poločasem	poločas	k1gInSc7
rozpadu	rozpad	k1gInSc2
23	#num#	k4
min	mina	k1gFnPc2
a	a	k8xC
2,3	2,3	k4
dne	den	k1gInSc2
<g/>
)	)	kIx)
zůstanou	zůstat	k5eAaPmIp3nP
na	na	k7c6
fólii	fólie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvek	prvek	k1gInSc1
s	s	k7c7
kratším	krátký	k2eAgInSc7d2
poločasem	poločas	k1gInSc7
byl	být	k5eAaImAgInS
identifikován	identifikovat	k5eAaBmNgInS
jako	jako	k9
239	#num#	k4
<g/>
U.	U.	kA
Druhým	druhý	k4xOgInSc7
produktem	produkt	k1gInSc7
byl	být	k5eAaImAgInS
izotop	izotop	k1gInSc1
prvku	prvek	k1gInSc2
93	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
prvek	prvek	k1gInSc1
byl	být	k5eAaImAgInS
nazván	nazvat	k5eAaBmNgInS,k5eAaPmNgInS
Neptunium	neptunium	k1gNnSc1
podle	podle	k7c2
planety	planeta	k1gFnSc2
nacházející	nacházející	k2eAgFnSc2d1
se	se	k3xPyFc4
za	za	k7c7
Uranem	Uran	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Chemie	chemie	k1gFnSc1
tohoto	tento	k3xDgInSc2
prvku	prvek	k1gInSc2
je	být	k5eAaImIp3nS
v	v	k7c6
mnoha	mnoho	k4c6
ohledech	ohled	k1gInPc6
odlišná	odlišný	k2eAgNnPc1d1
od	od	k7c2
nejbližších	blízký	k2eAgMnPc2d3
aktinoidů	aktinoid	k1gInPc2
–	–	k?
uranu	uran	k1gInSc2
a	a	k8xC
plutonia	plutonium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc1
vlastnosti	vlastnost	k1gFnPc1
jsou	být	k5eAaImIp3nP
bližší	blízký	k2eAgFnPc1d2
spíše	spíše	k9
vlastnostem	vlastnost	k1gFnPc3
uranu	uran	k1gInSc2
než	než	k8xS
plutonia	plutonium	k1gNnSc2
<g/>
,	,	kIx,
zvláště	zvláště	k9
pokud	pokud	k8xS
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
chování	chování	k1gNnSc4
ve	v	k7c6
vodném	vodný	k2eAgInSc6d1
roztoku	roztok	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Izotopy	izotop	k1gInPc4
a	a	k8xC
jaderné	jaderný	k2eAgFnPc4d1
vlastnosti	vlastnost	k1gFnPc4
neptunia	neptunium	k1gNnSc2
</s>
<s>
Neptunium	neptunium	k1gNnSc1
nemá	mít	k5eNaImIp3nS
žádný	žádný	k3yNgInSc1
stabilní	stabilní	k2eAgInSc1d1
izotop	izotop	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
20	#num#	k4
známých	známý	k2eAgInPc2d1
radioizotopů	radioizotop	k1gInPc2
má	mít	k5eAaImIp3nS
nejdelší	dlouhý	k2eAgInSc1d3
poločas	poločas	k1gInSc1
rozpadu	rozpad	k1gInSc2
237	#num#	k4
<g/>
Np	Np	k1gFnSc2
–	–	k?
2,144	2,144	k4
milionů	milion	k4xCgInPc2
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
byl	být	k5eAaImAgInS
poprvé	poprvé	k6eAd1
připraven	připravit	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
1942	#num#	k4
ostřelováním	ostřelování	k1gNnSc7
uranu	uran	k1gInSc2
neutrony	neutron	k1gInPc1
<g/>
:	:	kIx,
</s>
<s>
238	#num#	k4
</s>
<s>
U	u	k7c2
</s>
<s>
(	(	kIx(
</s>
<s>
n	n	k0
</s>
<s>
,	,	kIx,
</s>
<s>
2	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
)	)	kIx)
</s>
<s>
237	#num#	k4
</s>
<s>
U	u	k7c2
</s>
<s>
→	→	k?
</s>
<s>
6	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
75	#num#	k4
</s>
<s>
d	d	k?
</s>
<s>
β	β	k?
</s>
<s>
−	−	k?
</s>
<s>
237	#num#	k4
</s>
<s>
N	N	kA
</s>
<s>
p	p	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
^	^	kIx~
<g/>
{	{	kIx(
<g/>
238	#num#	k4
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
U	u	k7c2
<g/>
(	(	kIx(
<g/>
n	n	k0
<g/>
,2	,2	k4
<g/>
n	n	k0
<g/>
)	)	kIx)
<g/>
}	}	kIx)
\	\	kIx~
^	^	kIx~
<g/>
{	{	kIx(
<g/>
237	#num#	k4
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
U	u	k7c2
<g/>
}	}	kIx)
\	\	kIx~
{	{	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
\	\	kIx~
<g/>
xrightarrow	xrightarrow	k?
<g/>
[	[	kIx(
<g/>
{	{	kIx(
<g/>
6,75	6,75	k4
<g/>
\	\	kIx~
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
}	}	kIx)
<g/>
]	]	kIx)
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
beta	beta	k1gNnSc1
^	^	kIx~
<g/>
{	{	kIx(
<g/>
-	-	kIx~
<g/>
}}}	}}}	k?
<g/>
\	\	kIx~
^	^	kIx~
<g/>
{	{	kIx(
<g/>
237	#num#	k4
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
Np	Np	k1gFnSc1
<g/>
}	}	kIx)
}	}	kIx)
</s>
<s>
Další	další	k2eAgInPc1d1
významné	významný	k2eAgInPc1d1
izotopy	izotop	k1gInPc1
jsou	být	k5eAaImIp3nP
236	#num#	k4
<g/>
Np	Np	k1gFnPc1
s	s	k7c7
poločasem	poločas	k1gInSc7
rozpadu	rozpad	k1gInSc2
153	#num#	k4
000	#num#	k4
let	léto	k1gNnPc2
a	a	k8xC
235	#num#	k4
<g/>
Np	Np	k1gFnPc2
s	s	k7c7
poločasem	poločas	k1gInSc7
396,1	396,1	k4
dne	den	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Izotopy	izotop	k1gInPc1
s	s	k7c7
atomovým	atomový	k2eAgNnSc7d1
číslem	číslo	k1gNnSc7
větším	veliký	k2eAgNnSc7d2
než	než	k8xS
237	#num#	k4
jsou	být	k5eAaImIp3nP
β	β	k?
<g/>
−	−	k?
nestabilní	stabilní	k2eNgFnSc1d1
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
u	u	k7c2
izotopů	izotop	k1gInPc2
s	s	k7c7
deficitem	deficit	k1gInSc7
neutronů	neutron	k1gInPc2
je	být	k5eAaImIp3nS
častý	častý	k2eAgInSc1d1
záchyt	záchyt	k1gInSc1
elektronu	elektron	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgInPc4
izotopy	izotop	k1gInPc4
vykazují	vykazovat	k5eAaImIp3nP
α	α	k?
rozpad	rozpad	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobná	podobný	k2eAgFnSc1d1
situace	situace	k1gFnSc1
je	být	k5eAaImIp3nS
u	u	k7c2
lehčích	lehký	k2eAgInPc2d2
izotopů	izotop	k1gInPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
vysoká	vysoký	k2eAgFnSc1d1
pravděpodobnost	pravděpodobnost	k1gFnSc1
elektronového	elektronový	k2eAgInSc2d1
záchytu	záchyt	k1gInSc2
<g/>
:	:	kIx,
</s>
<s>
Izotoppoločas	Izotoppoločas	k1gMnSc1
rozpaduDruh	rozpaduDruh	k1gMnSc1
rozpaduProdukt	rozpaduProdukt	k1gInSc4
rozpadu	rozpad	k1gInSc2
</s>
<s>
225	#num#	k4
<g/>
Np	Np	k1gFnSc1
<g/>
3,6	3,6	k4
msα	msα	k?
<g/>
221	#num#	k4
<g/>
Pa	Pa	kA
</s>
<s>
226	#num#	k4
<g/>
Np	Np	k1gFnSc1
<g/>
35	#num#	k4
msα	msα	k?
<g/>
222	#num#	k4
<g/>
Pa	Pa	kA
</s>
<s>
227	#num#	k4
<g/>
Np	Np	k1gFnSc1
<g/>
510	#num#	k4
msα	msα	k?
<g/>
223	#num#	k4
<g/>
Pa	Pa	kA
</s>
<s>
228	#num#	k4
<g/>
Np	Np	k1gFnSc1
<g/>
61,4	61,4	k4
sε	sε	k?
(	(	kIx(
<g/>
60	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
/	/	kIx~
α	α	k?
(	(	kIx(
<g/>
40	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
228	#num#	k4
<g/>
U	U	kA
<g/>
/	/	kIx~
224	#num#	k4
<g/>
Pa	Pa	kA
</s>
<s>
229	#num#	k4
<g/>
Np	Np	k1gFnSc1
<g/>
4	#num#	k4
minα	minα	k?
(	(	kIx(
<g/>
68	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
/	/	kIx~
ε	ε	k?
(	(	kIx(
<g/>
32	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
225	#num#	k4
<g/>
Pa	Pa	kA
<g/>
/	/	kIx~
229U	229U	k4
</s>
<s>
230	#num#	k4
<g/>
Np	Np	k1gFnSc1
<g/>
4,6	4,6	k4
minε	minε	k?
(	(	kIx(
<g/>
≤	≤	k?
<g/>
97	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
/	/	kIx~
α	α	k?
(	(	kIx(
<g/>
≥	≥	k?
<g/>
3	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
230	#num#	k4
<g/>
U	U	kA
<g/>
/	/	kIx~
226	#num#	k4
<g/>
Pa	Pa	kA
</s>
<s>
231	#num#	k4
<g/>
Np	Np	k1gFnSc1
<g/>
48,8	48,8	k4
minε	minε	k?
(	(	kIx(
<g/>
98	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
/	/	kIx~
α	α	k?
(	(	kIx(
<g/>
2	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
231	#num#	k4
<g/>
U	U	kA
<g/>
/	/	kIx~
227	#num#	k4
<g/>
Pa	Pa	kA
</s>
<s>
232	#num#	k4
<g/>
Np	Np	k1gFnSc1
<g/>
14,7	14,7	k4
minε	minε	k?
(	(	kIx(
<g/>
100,00	100,00	k4
%	%	kIx~
<g/>
)	)	kIx)
/	/	kIx~
α	α	k?
(	(	kIx(
<g/>
2	#num#	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
4	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
232	#num#	k4
<g/>
U	U	kA
<g/>
/	/	kIx~
228	#num#	k4
<g/>
Pa	Pa	kA
</s>
<s>
233	#num#	k4
<g/>
Np	Np	k1gFnSc1
<g/>
36,2	36,2	k4
minε	minε	k?
(	(	kIx(
<g/>
100,00	100,00	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
/	/	kIx~
α	α	k?
(	(	kIx(
<g/>
≤	≤	k?
<g/>
1	#num#	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
3	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
233	#num#	k4
<g/>
U	U	kA
<g/>
/	/	kIx~
229	#num#	k4
<g/>
Pa	Pa	kA
</s>
<s>
234	#num#	k4
<g/>
Np	Np	k1gFnSc1
<g/>
4,4	4,4	k4
dε	dε	k?
<g/>
234	#num#	k4
<g/>
U	u	k7c2
</s>
<s>
235	#num#	k4
<g/>
Np	Np	k1gFnSc1
<g/>
396,1	396,1	k4
dε	dε	k?
(	(	kIx(
<g/>
100,00	100,00	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
/	/	kIx~
α	α	k?
(	(	kIx(
<g/>
2,6	2,6	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
3	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
235	#num#	k4
<g/>
U	U	kA
<g/>
/	/	kIx~
231	#num#	k4
<g/>
Pa	Pa	kA
</s>
<s>
236	#num#	k4
<g/>
Np	Np	k1gFnSc1
<g/>
153	#num#	k4
000	#num#	k4
rε	rε	k?
(	(	kIx(
<g/>
86,3	86,3	k4
%	%	kIx~
<g/>
)	)	kIx)
/	/	kIx~
β	β	k?
<g/>
−	−	k?
(	(	kIx(
<g/>
13,5	13,5	k4
%	%	kIx~
<g/>
)	)	kIx)
/	/	kIx~
α	α	k?
(	(	kIx(
<g/>
0,16	0,16	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
236	#num#	k4
<g/>
U	U	kA
<g/>
/	/	kIx~
236	#num#	k4
<g/>
Pu	Pu	k1gFnPc2
<g/>
/	/	kIx~
232	#num#	k4
<g/>
Pa	Pa	kA
</s>
<s>
237	#num#	k4
<g/>
Np	Np	k1gFnSc1
<g/>
2	#num#	k4
144	#num#	k4
000	#num#	k4
rα	rα	k?
(	(	kIx(
<g/>
100	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
/	/	kIx~
SF	SF	kA
(	(	kIx(
<g/>
≤	≤	k?
<g/>
2	#num#	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
10	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
233	#num#	k4
<g/>
Pa	Pa	kA
/	/	kIx~
různé	různý	k2eAgFnPc1d1
</s>
<s>
238	#num#	k4
<g/>
Np	Np	k1gFnSc1
<g/>
2,117	2,117	k4
dβ	dβ	k?
<g/>
−	−	k?
<g/>
238	#num#	k4
<g/>
Pu	Pu	k1gFnPc2
</s>
<s>
239	#num#	k4
<g/>
Np	Np	k1gFnSc1
<g/>
2,356	2,356	k4
dβ	dβ	k?
<g/>
−	−	k?
<g/>
239	#num#	k4
<g/>
Pu	Pu	k1gFnPc2
</s>
<s>
240	#num#	k4
<g/>
Np	Np	k1gFnSc1
<g/>
61,9	61,9	k4
minβ	minβ	k?
<g/>
−	−	k?
<g/>
240	#num#	k4
<g/>
Pu	Pu	k1gFnSc1
</s>
<s>
241	#num#	k4
<g/>
Np	Np	k1gFnSc1
<g/>
13,9	13,9	k4
minβ	minβ	k?
<g/>
−	−	k?
<g/>
241	#num#	k4
<g/>
Pu	Pu	k1gFnPc2
</s>
<s>
242	#num#	k4
<g/>
Np	Np	k1gFnSc1
<g/>
2,2	2,2	k4
minβ	minβ	k?
<g/>
−	−	k?
<g/>
242	#num#	k4
<g/>
Pu	Pu	k1gFnPc2
</s>
<s>
243	#num#	k4
<g/>
Np	Np	k1gFnSc1
<g/>
1,85	1,85	k4
minβ	minβ	k?
<g/>
−	−	k?
<g/>
243	#num#	k4
<g/>
Pu	Pu	k1gFnSc1
</s>
<s>
244	#num#	k4
<g/>
Np	Np	k1gFnSc1
<g/>
2,29	2,29	k4
minβ	minβ	k?
<g/>
−	−	k?
<g/>
244	#num#	k4
<g/>
Pu	Pu	k1gFnPc2
</s>
<s>
245	#num#	k4
<g/>
Np	Np	k1gFnSc1
??	??	k?
?	?	kIx.
</s>
<s>
První	první	k4xOgNnSc1
vážitelné	vážitelný	k2eAgNnSc1d1
množství	množství	k1gNnSc1
neptunia	neptunium	k1gNnSc2
získali	získat	k5eAaPmAgMnP
v	v	k7c6
roce	rok	k1gInSc6
1944	#num#	k4
Magnusson	Magnussona	k1gFnPc2
a	a	k8xC
LaChapelle	LaChapelle	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Separace	separace	k1gFnSc1
a	a	k8xC
čištění	čištění	k1gNnSc1
nejdůležitějších	důležitý	k2eAgInPc2d3
izotopů	izotop	k1gInPc2
neptunia	neptunium	k1gNnSc2
</s>
<s>
Výroba	výroba	k1gFnSc1
237	#num#	k4
<g/>
Np	Np	k1gFnPc2
</s>
<s>
237	#num#	k4
<g/>
Np	Np	k1gFnSc1
slouží	sloužit	k5eAaImIp3nS
jako	jako	k9
terč	terč	k1gInSc4
při	při	k7c6
výrobě	výroba	k1gFnSc6
238	#num#	k4
<g/>
Pu	Pu	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
237	#num#	k4
</s>
<s>
N	N	kA
</s>
<s>
p	p	k?
</s>
<s>
(	(	kIx(
</s>
<s>
n	n	k0
</s>
<s>
,	,	kIx,
</s>
<s>
γ	γ	k?
</s>
<s>
)	)	kIx)
</s>
<s>
238	#num#	k4
</s>
<s>
N	N	kA
</s>
<s>
p	p	k?
</s>
<s>
→	→	k?
</s>
<s>
2	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
1	#num#	k4
</s>
<s>
d	d	k?
</s>
<s>
β	β	k?
</s>
<s>
−	−	k?
</s>
<s>
238	#num#	k4
</s>
<s>
P	P	kA
</s>
<s>
u	u	k7c2
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
^	^	kIx~
<g/>
{	{	kIx(
<g/>
237	#num#	k4
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
Np	Np	k1gFnSc1
<g/>
(	(	kIx(
<g/>
n	n	k0
<g/>
,	,	kIx,
<g/>
\	\	kIx~
<g/>
gamma	gamma	k1gFnSc1
)	)	kIx)
<g/>
}	}	kIx)
\	\	kIx~
^	^	kIx~
<g/>
{	{	kIx(
<g/>
238	#num#	k4
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
Np	Np	k1gFnSc1
<g/>
}	}	kIx)
\	\	kIx~
{	{	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
\	\	kIx~
<g/>
xrightarrow	xrightarrow	k?
<g/>
[	[	kIx(
<g/>
{	{	kIx(
<g/>
2,1	2,1	k4
<g/>
\	\	kIx~
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
}	}	kIx)
<g/>
]	]	kIx)
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
beta	beta	k1gNnSc1
^	^	kIx~
<g/>
{	{	kIx(
<g/>
-	-	kIx~
<g/>
}}}	}}}	k?
<g/>
\	\	kIx~
^	^	kIx~
<g/>
{	{	kIx(
<g/>
238	#num#	k4
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
Pu	Pu	k1gFnSc1
<g/>
}	}	kIx)
}	}	kIx)
</s>
<s>
237	#num#	k4
<g/>
Np	Np	k1gFnPc2
je	být	k5eAaImIp3nS
produkováno	produkovat	k5eAaImNgNnS
v	v	k7c6
jaderných	jaderný	k2eAgInPc6d1
reaktorech	reaktor	k1gInPc6
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
jako	jako	k8xS,k8xC
palivo	palivo	k1gNnSc4
používají	používat	k5eAaImIp3nP
uran	uran	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Probíhající	probíhající	k2eAgInPc1d1
procesy	proces	k1gInPc1
lze	lze	k6eAd1
popsat	popsat	k5eAaPmF
rovnicemi	rovnice	k1gFnPc7
<g/>
:	:	kIx,
</s>
<s>
235	#num#	k4
</s>
<s>
U	u	k7c2
</s>
<s>
(	(	kIx(
</s>
<s>
n	n	k0
</s>
<s>
,	,	kIx,
</s>
<s>
γ	γ	k?
</s>
<s>
)	)	kIx)
</s>
<s>
236	#num#	k4
</s>
<s>
U	u	k7c2
</s>
<s>
(	(	kIx(
</s>
<s>
n	n	k0
</s>
<s>
,	,	kIx,
</s>
<s>
γ	γ	k?
</s>
<s>
)	)	kIx)
</s>
<s>
237	#num#	k4
</s>
<s>
U	u	k7c2
</s>
<s>
→	→	k?
</s>
<s>
6	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
7	#num#	k4
</s>
<s>
d	d	k?
</s>
<s>
β	β	k?
</s>
<s>
−	−	k?
</s>
<s>
237	#num#	k4
</s>
<s>
N	N	kA
</s>
<s>
p	p	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
^	^	kIx~
<g/>
{	{	kIx(
<g/>
235	#num#	k4
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
U	u	k7c2
<g/>
(	(	kIx(
<g/>
n	n	k0
<g/>
,	,	kIx,
<g/>
\	\	kIx~
<g/>
gamma	gamma	k1gFnSc1
)	)	kIx)
<g/>
}	}	kIx)
\	\	kIx~
^	^	kIx~
<g/>
{	{	kIx(
<g/>
236	#num#	k4
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
U	u	k7c2
<g/>
(	(	kIx(
<g/>
n	n	k0
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
<g/>
\	\	kIx~
<g/>
gamma	gamma	k1gFnSc1
)	)	kIx)
<g/>
}	}	kIx)
\	\	kIx~
^	^	kIx~
<g/>
{	{	kIx(
<g/>
237	#num#	k4
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
U	u	k7c2
<g/>
}	}	kIx)
\	\	kIx~
{	{	kIx(
<g/>
\	\	kIx~
<g/>
xrightarrow	xrightarrow	k?
<g/>
[	[	kIx(
<g/>
{	{	kIx(
<g/>
6,7	6,7	k4
<g/>
\	\	kIx~
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
}	}	kIx)
<g/>
]	]	kIx)
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
beta	beta	k1gNnSc1
^	^	kIx~
<g/>
{	{	kIx(
<g/>
-	-	kIx~
<g/>
}}}	}}}	k?
<g/>
\	\	kIx~
^	^	kIx~
<g/>
{	{	kIx(
<g/>
237	#num#	k4
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
Np	Np	k1gFnSc1
<g/>
}	}	kIx)
}	}	kIx)
</s>
<s>
a	a	k8xC
</s>
<s>
238	#num#	k4
</s>
<s>
U	u	k7c2
</s>
<s>
(	(	kIx(
</s>
<s>
n	n	k0
</s>
<s>
,	,	kIx,
</s>
<s>
2	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
)	)	kIx)
</s>
<s>
237	#num#	k4
</s>
<s>
U	u	k7c2
</s>
<s>
→	→	k?
</s>
<s>
β	β	k?
</s>
<s>
−	−	k?
</s>
<s>
237	#num#	k4
</s>
<s>
N	N	kA
</s>
<s>
p	p	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
^	^	kIx~
<g/>
{	{	kIx(
<g/>
238	#num#	k4
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
U	u	k7c2
<g/>
(	(	kIx(
<g/>
n	n	k0
<g/>
,2	,2	k4
<g/>
n	n	k0
<g/>
)	)	kIx)
<g/>
}	}	kIx)
\	\	kIx~
^	^	kIx~
<g/>
{	{	kIx(
<g/>
237	#num#	k4
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
U	u	k7c2
<g/>
}	}	kIx)
\	\	kIx~
{	{	kIx(
<g/>
\	\	kIx~
<g/>
xrightarrow	xrightarrow	k?
{	{	kIx(
<g/>
\	\	kIx~
<g/>
beta	beta	k1gNnSc1
^	^	kIx~
<g/>
{	{	kIx(
<g/>
-	-	kIx~
<g/>
}}}	}}}	k?
<g/>
\	\	kIx~
^	^	kIx~
<g/>
{	{	kIx(
<g/>
237	#num#	k4
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
Np	Np	k1gFnSc1
<g/>
}	}	kIx)
}	}	kIx)
</s>
<s>
V	v	k7c6
reaktorech	reaktor	k1gInPc6
na	na	k7c4
přírodní	přírodní	k2eAgInSc4d1
uran	uran	k1gInSc4
převažuje	převažovat	k5eAaImIp3nS
(	(	kIx(
<g/>
n	n	k0
<g/>
,2	,2	k4
<g/>
n	n	k0
<g/>
)	)	kIx)
reakce	reakce	k1gFnPc4
s	s	k7c7
neutrony	neutron	k1gInPc7
o	o	k7c4
energii	energie	k1gFnSc4
>	>	kIx)
6.7	6.7	k4
MeV	MeV	k1gFnPc2
<g/>
,	,	kIx,
naproti	naproti	k7c3
tomu	ten	k3xDgNnSc3
v	v	k7c6
reaktorech	reaktor	k1gInPc6
na	na	k7c4
obohacený	obohacený	k2eAgInSc4d1
uran	uran	k1gInSc4
převažuje	převažovat	k5eAaImIp3nS
dvojitý	dvojitý	k2eAgInSc4d1
záchyt	záchyt	k1gInSc4
neutronu	neutron	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Metody	metoda	k1gFnPc1
separace	separace	k1gFnSc1
237	#num#	k4
<g/>
Np	Np	k1gFnSc1
z	z	k7c2
vyhořelého	vyhořelý	k2eAgNnSc2d1
jaderného	jaderný	k2eAgNnSc2d1
paliva	palivo	k1gNnSc2
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
podobná	podobný	k2eAgFnSc1d1
procesu	proces	k1gInSc6
Purex	Purex	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgInSc1
krok	krok	k1gInSc1
spočívá	spočívat	k5eAaImIp3nS
v	v	k7c6
extrakci	extrakce	k1gFnSc6
neptunia	neptunium	k1gNnSc2
společně	společně	k6eAd1
s	s	k7c7
uranem	uran	k1gInSc7
a	a	k8xC
plutoniem	plutonium	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
druhém	druhý	k4xOgInSc6
kroku	krok	k1gInSc6
je	být	k5eAaImIp3nS
neptunium	neptunium	k1gNnSc1
z	z	k7c2
této	tento	k3xDgFnSc2
směsi	směs	k1gFnSc2
odděleno	oddělen	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Výroba	výroba	k1gFnSc1
238	#num#	k4
<g/>
Np	Np	k1gFnPc2
</s>
<s>
Tento	tento	k3xDgInSc1
izotop	izotop	k1gInSc1
se	se	k3xPyFc4
získává	získávat	k5eAaImIp3nS
ozařováním	ozařování	k1gNnSc7
237	#num#	k4
<g/>
Np	Np	k1gFnPc2
neutrony	neutron	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Separace	separace	k1gFnSc1
se	se	k3xPyFc4
provádí	provádět	k5eAaImIp3nS
na	na	k7c6
iontoměničových	iontoměničův	k2eAgFnPc6d1
kolonách	kolona	k1gFnPc6
(	(	kIx(
<g/>
anexech	anex	k1gInPc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Výroba	výroba	k1gFnSc1
239	#num#	k4
<g/>
Np	Np	k1gFnPc2
</s>
<s>
Existují	existovat	k5eAaImIp3nP
dva	dva	k4xCgInPc4
způsoby	způsob	k1gInPc4
přípravy	příprava	k1gFnSc2
tohoto	tento	k3xDgInSc2
izotopu	izotop	k1gInSc2
<g/>
:	:	kIx,
</s>
<s>
ozařování	ozařování	k1gNnSc1
uranu	uran	k1gInSc2
neutrony	neutron	k1gInPc4
<g/>
,	,	kIx,
</s>
<s>
243	#num#	k4
<g/>
Am	Am	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
se	se	k3xPyFc4
rozpadá	rozpadat	k5eAaImIp3nS,k5eAaPmIp3nS
na	na	k7c4
239	#num#	k4
<g/>
Np	Np	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
prvním	první	k4xOgInSc6
případě	případ	k1gInSc6
je	být	k5eAaImIp3nS
nutné	nutný	k2eAgNnSc1d1
ze	z	k7c2
směsi	směs	k1gFnSc2
odstranit	odstranit	k5eAaPmF
uran	uran	k1gInSc1
a	a	k8xC
thorium	thorium	k1gNnSc1
<g/>
,	,	kIx,
v	v	k7c6
druhém	druhý	k4xOgMnSc6
pouze	pouze	k6eAd1
americium	americium	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Kovové	kovový	k2eAgNnSc1d1
neptunium	neptunium	k1gNnSc1
a	a	k8xC
jeho	jeho	k3xOp3gFnPc1
slitiny	slitina	k1gFnPc1
</s>
<s>
Kovové	kovový	k2eAgNnSc4d1
neptunium	neptunium	k1gNnSc4
poprvé	poprvé	k6eAd1
získali	získat	k5eAaPmAgMnP
v	v	k7c6
roce	rok	k1gInSc6
1948	#num#	k4
Fried	Fried	k1gInSc4
a	a	k8xC
Davidson	Davidson	k1gInSc4
redukcí	redukce	k1gFnPc2
50	#num#	k4
µ	µ	k1gFnPc2
NpF	NpF	k1gFnSc4
<g/>
3	#num#	k4
</s>
<s>
parami	para	k1gFnPc7
barya	baryum	k1gNnSc2
při	při	k7c6
1	#num#	k4
200	#num#	k4
°	°	k?
<g/>
C.	C.	kA
Pro	pro	k7c4
výrobu	výroba	k1gFnSc4
většího	veliký	k2eAgNnSc2d2
množství	množství	k1gNnSc2
je	být	k5eAaImIp3nS
výhodnější	výhodný	k2eAgMnSc1d2
použít	použít	k5eAaPmF
redukci	redukce	k1gFnSc4
NpF	NpF	k1gFnSc2
<g/>
4	#num#	k4
vápníkem	vápník	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Kovové	kovový	k2eAgNnSc1d1
neptunium	neptunium	k1gNnSc1
má	mít	k5eAaImIp3nS
tři	tři	k4xCgFnPc4
modifikace	modifikace	k1gFnPc4
α	α	k?
<g/>
,	,	kIx,
β	β	k?
a	a	k8xC
γ	γ	k?
<g/>
,	,	kIx,
s	s	k7c7
bodem	bod	k1gInSc7
přechodu	přechod	k1gInSc2
při	při	k7c6
280	#num#	k4
°	°	k?
<g/>
C	C	kA
a	a	k8xC
577	#num#	k4
°	°	k?
<g/>
C.	C.	kA
Struktura	struktura	k1gFnSc1
α	α	k1gInSc1
je	být	k5eAaImIp3nS
jedinečná	jedinečný	k2eAgFnSc1d1
<g/>
,	,	kIx,
nebyla	být	k5eNaImAgFnS
pozorována	pozorovat	k5eAaImNgFnS
u	u	k7c2
žádného	žádný	k3yNgInSc2
jiného	jiný	k2eAgInSc2d1
kovu	kov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Orthorombická	Orthorombický	k2eAgFnSc1d1
základní	základní	k2eAgFnSc1d1
buňka	buňka	k1gFnSc1
obsahuje	obsahovat	k5eAaImIp3nS
osm	osm	k4xCc4
atomů	atom	k1gInPc2
ve	v	k7c6
dvou	dva	k4xCgNnPc6
rozdílných	rozdílný	k2eAgNnPc6d1
</s>
<s>
pozicích	pozice	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejkratší	krátký	k2eAgFnSc1d3
vzdálenost	vzdálenost	k1gFnSc1
Np-Np	Np-Np	k1gInSc1
je	být	k5eAaImIp3nS
2,60	2,60	k4
Å	Å	k?
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
ukazuje	ukazovat	k5eAaImIp3nS
na	na	k7c4
kovalentní	kovalentní	k2eAgInSc4d1
charakter	charakter	k1gInSc4
vazby	vazba	k1gFnSc2
<g/>
.	.	kIx.
β	β	k1gInSc1
je	být	k5eAaImIp3nS
tetragonální	tetragonální	k2eAgInSc1d1
s	s	k7c7
vrstevnatou	vrstevnatý	k2eAgFnSc7d1
strukturou	struktura	k1gFnSc7
podobnou	podobný	k2eAgFnSc4d1
InBi	InBe	k1gFnSc4
(	(	kIx(
<g/>
čtyři	čtyři	k4xCgInPc1
atomy	atom	k1gInPc1
v	v	k7c6
základní	základní	k2eAgFnSc6d1
buňce	buňka	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
γ	γ	k1gInSc1
má	mít	k5eAaImIp3nS
stejnou	stejný	k2eAgFnSc4d1
mřížku	mřížka	k1gFnSc4
jako	jako	k8xC,k8xS
α	α	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Kovové	kovový	k2eAgNnSc1d1
neptunium	neptunium	k1gNnSc1
je	být	k5eAaImIp3nS
stálé	stálý	k2eAgNnSc1d1
na	na	k7c6
suchém	suchý	k2eAgInSc6d1
vzduchu	vzduch	k1gInSc6
při	při	k7c6
pokojové	pokojový	k2eAgFnSc6d1
teplotě	teplota	k1gFnSc6
a	a	k8xC
jen	jen	k9
velmi	velmi	k6eAd1
pomalu	pomalu	k6eAd1
se	se	k3xPyFc4
pokrývá	pokrývat	k5eAaImIp3nS
tenkou	tenký	k2eAgFnSc7d1
vrstvou	vrstva	k1gFnSc7
oxidu	oxid	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
za	za	k7c2
vyšších	vysoký	k2eAgFnPc2d2
teplot	teplota	k1gFnPc2
probíhá	probíhat	k5eAaImIp3nS
tento	tento	k3xDgInSc1
proces	proces	k1gInSc1
velmi	velmi	k6eAd1
rychle	rychle	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neptunium	neptunium	k1gNnSc1
se	se	k3xPyFc4
rozpouští	rozpouštět	k5eAaImIp3nS
v	v	k7c6
kyselině	kyselina	k1gFnSc6
chlorovodíkové	chlorovodíkový	k2eAgFnSc6d1
a	a	k8xC
sírové	sírový	k2eAgFnSc6d1
<g/>
.	.	kIx.
</s>
<s>
Neptunium	neptunium	k1gNnSc1
je	být	k5eAaImIp3nS
unikátní	unikátní	k2eAgFnSc1d1
svou	svůj	k3xOyFgFnSc7
vysokou	vysoký	k2eAgFnSc7d1
rozpustností	rozpustnost	k1gFnSc7
v	v	k7c6
α	α	k?
i	i	k9
β	β	k5eAaPmIp1nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Intermetalické	Intermetalický	k2eAgFnPc1d1
sloučeniny	sloučenina	k1gFnPc1
tvoří	tvořit	k5eAaImIp3nP
neptunium	neptunium	k1gNnSc4
s	s	k7c7
hliníkem	hliník	k1gInSc7
a	a	k8xC
berylliem	beryllium	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lze	lze	k6eAd1
je	být	k5eAaImIp3nS
připravit	připravit	k5eAaPmF
redukcí	redukce	k1gFnSc7
NpF	NpF	k1gFnSc2
<g/>
3	#num#	k4
nadbytkem	nadbytek	k1gInSc7
kovového	kovový	k2eAgInSc2d1
Al	ala	k1gFnPc2
nebo	nebo	k8xC
Be	Be	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
2	#num#	k4
NpF	NpF	k1gFnSc1
<g/>
3	#num#	k4
+	+	kIx~
29	#num#	k4
Be	Be	k1gFnSc2
→	→	k?
2	#num#	k4
NpBe	NpBe	k1gInSc1
<g/>
13	#num#	k4
+	+	kIx~
3	#num#	k4
BeF	BeF	k1gFnPc2
<g/>
2	#num#	k4
</s>
<s>
(	(	kIx(
<g/>
1200	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
</s>
<s>
Jsou	být	k5eAaImIp3nP
isotypické	isotypický	k2eAgInPc1d1
se	s	k7c7
sloučeninami	sloučenina	k1gFnPc7
thoria	thorium	k1gNnSc2
<g/>
,	,	kIx,
uranu	uran	k1gInSc2
a	a	k8xC
plutonia	plutonium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Boridy	Borid	k1gMnPc7
NpB	NpB	k1gFnSc2
<g/>
2	#num#	k4
<g/>
,	,	kIx,
NpB	NpB	k1gFnSc1
<g/>
4	#num#	k4
<g/>
,	,	kIx,
NpB	NpB	k1gFnSc1
<g/>
6	#num#	k4
a	a	k8xC
NpB	NpB	k1gMnSc1
<g/>
12	#num#	k4
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
jsou	být	k5eAaImIp3nP
isostrukturní	isostrukturní	k2eAgInPc1d1
s	s	k7c7
odpovídajícími	odpovídající	k2eAgFnPc7d1
sloučeninami	sloučenina	k1gFnPc7
uranu	uran	k1gInSc2
a	a	k8xC
plutonia	plutonium	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
intermetalické	intermetalický	k2eAgFnSc2d1
sloučeniny	sloučenina	k1gFnSc2
NpCd	NpCd	k1gInSc4
<g/>
6	#num#	k4
a	a	k8xC
NpCd	NpCd	k1gInSc4
<g/>
12	#num#	k4
získáme	získat	k5eAaPmIp1nP
přímou	přímý	k2eAgFnSc7d1
syntézou	syntéza	k1gFnSc7
z	z	k7c2
prvků	prvek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Sloučeniny	sloučenina	k1gFnPc1
neptunia	neptunium	k1gNnSc2
</s>
<s>
Barvy	barva	k1gFnPc1
roztoků	roztok	k1gInPc2
solí	solit	k5eAaImIp3nP
neptunia	neptunium	k1gNnSc2
podle	podle	k7c2
oxidačního	oxidační	k2eAgNnSc2d1
čísla	číslo	k1gNnSc2
</s>
<s>
Hydridy	hydrid	k1gInPc1
neptunia	neptunium	k1gNnSc2
</s>
<s>
Hydridy	hydrid	k1gInPc1
NpH	NpH	k1gFnSc1
<g/>
2	#num#	k4
a	a	k8xC
NpH	NpH	k1gMnSc1
<g/>
3	#num#	k4
lze	lze	k6eAd1
připravit	připravit	k5eAaPmF
přímým	přímý	k2eAgNnSc7d1
působením	působení	k1gNnSc7
vodíku	vodík	k1gInSc2
na	na	k7c4
kovové	kovový	k2eAgNnSc4d1
neptunium	neptunium	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
NpH	NpH	k1gFnSc1
<g/>
2	#num#	k4
si	se	k3xPyFc3
zachovává	zachovávat	k5eAaImIp3nS
svou	svůj	k3xOyFgFnSc4
strukturu	struktura	k1gFnSc4
v	v	k7c6
širokém	široký	k2eAgNnSc6d1
rozmezí	rozmezí	k1gNnSc6
obsahu	obsah	k1gInSc2
vodíku	vodík	k1gInSc2
(	(	kIx(
<g/>
NpH	NpH	k1gFnSc1
<g/>
2	#num#	k4
<g/>
+	+	kIx~
<g/>
x	x	k?
<g/>
,	,	kIx,
0	#num#	k4
≤	≤	k?
x	x	k?
≤	≤	k?
0,7	0,7	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
mřížková	mřížkový	k2eAgFnSc1d1
konstanta	konstanta	k1gFnSc1
roste	růst	k5eAaImIp3nS
s	s	k7c7
poměrem	poměr	k1gInSc7
H	H	kA
<g/>
:	:	kIx,
<g/>
Np	Np	k1gMnSc1
<g/>
,	,	kIx,
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
hydridu	hydrid	k1gInSc2
PuH	puh	k0wR
<g/>
2	#num#	k4
<g/>
+	+	kIx~
<g/>
x.	x.	k?
Pokud	pokud	k8xS
poměr	poměr	k1gInSc1
H	H	kA
<g/>
:	:	kIx,
<g/>
Np	Np	k1gMnSc1
přesáhne	přesáhnout	k5eAaPmIp3nS
hodnotu	hodnota	k1gFnSc4
2,7	2,7	k4
<g/>
,	,	kIx,
tak	tak	k6eAd1
můžeme	moct	k5eAaImIp1nP
pozorovat	pozorovat	k5eAaImF
hexagonální	hexagonální	k2eAgFnSc1d1
NpH	NpH	k1gFnSc1
<g/>
3	#num#	k4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
isostrukturní	isostrukturní	k2eAgInSc1d1
s	s	k7c7
PuH	puh	k0wR
<g/>
3	#num#	k4
<g/>
,	,	kIx,
GdH	GdH	k1gFnSc1
<g/>
3	#num#	k4
a	a	k8xC
HoD	hod	k1gInSc1
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Halogenidy	Halogenida	k1gFnPc1
neptunia	neptunium	k1gNnSc2
</s>
<s>
Fluoridy	fluorid	k1gInPc1
</s>
<s>
Fialový	fialový	k2eAgInSc4d1
NpF	NpF	k1gFnSc7
<g/>
3	#num#	k4
a	a	k8xC
zelený	zelený	k2eAgInSc4d1
NpF	NpF	k1gFnSc7
<g/>
4	#num#	k4
připravíme	připravit	k5eAaPmIp1nP
hydrofluorací	hydrofluorace	k1gFnSc7
oxidu	oxid	k1gInSc2
neptuničitého	ptuničitý	k2eNgInSc2d1
NpO	NpO	k1gFnSc7
<g/>
2	#num#	k4
v	v	k7c6
přítomnosti	přítomnost	k1gFnSc6
vodíku	vodík	k1gInSc2
nebo	nebo	k8xC
kyslíku	kyslík	k1gInSc2
<g/>
:	:	kIx,
</s>
<s>
NpO	NpO	k?
<g/>
2	#num#	k4
+	+	kIx~
1	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
H2	H2	k1gFnPc2
+	+	kIx~
3	#num#	k4
HF	HF	kA
→	→	k?
NpF	NpF	k1gMnSc1
<g/>
3	#num#	k4
+	+	kIx~
2	#num#	k4
H2O	H2O	k1gFnPc2
</s>
<s>
NpF	NpF	k?
<g/>
3	#num#	k4
+	+	kIx~
1	#num#	k4
<g/>
/	/	kIx~
<g/>
4	#num#	k4
O2	O2	k1gFnPc2
+	+	kIx~
3	#num#	k4
HF	HF	kA
→	→	k?
NpF	NpF	k1gMnSc1
<g/>
4	#num#	k4
+	+	kIx~
1	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
H2O	H2O	k1gFnPc2
</s>
<s>
NpF	NpF	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
v	v	k7c6
pevném	pevný	k2eAgInSc6d1
stavu	stav	k1gInSc6
oranžový	oranžový	k2eAgMnSc1d1
a	a	k8xC
v	v	k7c6
parách	para	k1gFnPc6
bezbarvý	bezbarvý	k2eAgInSc1d1
<g/>
,	,	kIx,
můžeme	moct	k5eAaImIp1nP
připravit	připravit	k5eAaPmF
fluorací	fluorace	k1gFnSc7
NpO	NpO	k1gFnSc2
<g/>
2	#num#	k4
(	(	kIx(
<g/>
nebo	nebo	k8xC
lépe	dobře	k6eAd2
NpF	NpF	k1gFnPc1
<g/>
4	#num#	k4
<g/>
)	)	kIx)
pomocí	pomocí	k7c2
BrF	BrF	k1gFnSc2
<g/>
3	#num#	k4
<g/>
,	,	kIx,
BrF	BrF	k1gFnSc1
<g/>
5	#num#	k4
nebo	nebo	k8xC
elementárního	elementární	k2eAgInSc2d1
fluoru	fluor	k1gInSc2
při	při	k7c6
teplotách	teplota	k1gFnPc6
300	#num#	k4
<g/>
–	–	k?
<g/>
500	#num#	k4
°	°	k?
<g/>
C.	C.	kA
Tento	tento	k3xDgInSc4
fluorid	fluorid	k1gInSc4
se	se	k3xPyFc4
rozkládá	rozkládat	k5eAaImIp3nS
působením	působení	k1gNnSc7
světla	světlo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stopami	stopa	k1gFnPc7
vlhkosti	vlhkost	k1gFnSc2
prudce	prudko	k6eAd1
hydrolyzuje	hydrolyzovat	k5eAaBmIp3nS
na	na	k7c4
fluorid	fluorid	k1gInSc4
neptunylu	neptunyl	k1gInSc2
NpO	NpO	k1gFnSc2
<g/>
2	#num#	k4
<g/>
F	F	kA
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1
halogenidy	halogenid	k1gInPc1
</s>
<s>
Těkavý	těkavý	k2eAgInSc1d1
chlorid	chlorid	k1gInSc1
neptuničitý	ptuničitý	k2eNgInSc1d1
(	(	kIx(
<g/>
NpCl	NpCl	k1gInSc1
<g/>
4	#num#	k4
<g/>
)	)	kIx)
můžeme	moct	k5eAaImIp1nP
připravit	připravit	k5eAaPmF
reakcí	reakce	k1gFnSc7
oxidu	oxid	k1gInSc2
neptuničitého	ptuničitý	k2eNgInSc2d1
nebo	nebo	k8xC
šťavelanu	šťavelan	k1gInSc2
neptuničitého	ptuničitý	k2eNgMnSc2d1
Np	Np	k1gMnSc2
<g/>
[	[	kIx(
<g/>
(	(	kIx(
<g/>
COO	COO	kA
<g/>
)	)	kIx)
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
2	#num#	k4
v	v	k7c6
proudu	proud	k1gInSc6
chloru	chlor	k1gInSc2
nasyceného	nasycený	k2eAgInSc2d1
parami	para	k1gFnPc7
CCl	CCl	k1gFnPc6
<g/>
4	#num#	k4
při	při	k7c6
teplotě	teplota	k1gFnSc6
450	#num#	k4
°	°	k?
<g/>
C.	C.	kA
Tato	tento	k3xDgFnSc1
látka	látka	k1gFnSc1
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
hygroskopická	hygroskopický	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Světle	světle	k6eAd1
hnědý	hnědý	k2eAgInSc4d1
NpOCl	NpOCl	k1gInSc4
<g/>
2	#num#	k4
a	a	k8xC
oranžový	oranžový	k2eAgInSc4d1
NpOBr	NpOBr	k1gInSc4
<g/>
2	#num#	k4
získáme	získat	k5eAaPmIp1nP
v	v	k7c6
čistém	čistý	k2eAgInSc6d1
stavu	stav	k1gInSc6
reakcí	reakce	k1gFnPc2
MX4	MX4	k1gFnSc4
s	s	k7c7
oxidem	oxid	k1gInSc7
antimonitým	antimonitý	k2eAgInSc7d1
Sb	sb	kA
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Červenohnědý	červenohnědý	k2eAgInSc1d1
NpBr	NpBr	k1gInSc1
<g/>
4	#num#	k4
vzniká	vznikat	k5eAaImIp3nS
bromací	bromací	k2eAgFnSc1d1
NpO	NpO	k1gFnSc1
<g/>
2	#num#	k4
nadbytkem	nadbytek	k1gInSc7
bromidu	bromid	k1gInSc2
hlinitého	hlinitý	k2eAgInSc2d1
AlBr	AlBr	k1gInSc1
<g/>
3	#num#	k4
při	při	k7c6
350	#num#	k4
°	°	k?
<g/>
C.	C.	kA
Další	další	k2eAgFnSc7d1
možností	možnost	k1gFnSc7
přípravy	příprava	k1gFnSc2
je	být	k5eAaImIp3nS
přímá	přímý	k2eAgFnSc1d1
syntéza	syntéza	k1gFnSc1
z	z	k7c2
prvků	prvek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Bromace	Bromace	k1gFnSc1
NpO	NpO	k1gFnSc2
<g/>
2	#num#	k4
pomocí	pomoc	k1gFnPc2
AlBr	AlBr	k1gInSc4
<g/>
3	#num#	k4
v	v	k7c6
přítomnosti	přítomnost	k1gFnSc6
kovového	kovový	k2eAgInSc2d1
hliníku	hliník	k1gInSc2
poskytuje	poskytovat	k5eAaImIp3nS
zelený	zelený	k2eAgInSc1d1
bromid	bromid	k1gInSc1
neptunitý	ptunitý	k2eNgInSc1d1
<g/>
:	:	kIx,
</s>
<s>
3	#num#	k4
NpO	NpO	k1gFnSc1
<g/>
2	#num#	k4
+	+	kIx~
3	#num#	k4
AlBr	AlBr	k1gInSc1
<g/>
3	#num#	k4
+	+	kIx~
Al	ala	k1gFnPc2
→	→	k?
3	#num#	k4
NpBr	NpBr	k1gInSc1
<g/>
3	#num#	k4
+	+	kIx~
2	#num#	k4
Al	ala	k1gFnPc2
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
3	#num#	k4
</s>
<s>
NpI	NpI	k?
<g/>
3	#num#	k4
připravíme	připravit	k5eAaPmIp1nP
podobně	podobně	k6eAd1
reakcí	reakce	k1gFnSc7
NpO	NpO	k1gFnSc7
<g/>
2	#num#	k4
s	s	k7c7
AlI	AlI	k1gFnSc7
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
NpI	NpI	k1gFnSc1
<g/>
4	#num#	k4
se	se	k3xPyFc4
připravit	připravit	k5eAaPmF
nepodařilo	podařit	k5eNaPmAgNnS
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
souhlasí	souhlasit	k5eAaImIp3nS
s	s	k7c7
výsledky	výsledek	k1gInPc7
výpočtů	výpočet	k1gInPc2
<g/>
,	,	kIx,
podle	podle	k7c2
nichž	jenž	k3xRgInPc2
je	být	k5eAaImIp3nS
tato	tento	k3xDgFnSc1
látka	látka	k1gFnSc1
termodynamicky	termodynamicky	k6eAd1
nestabilní	stabilní	k2eNgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Oxidy	oxid	k1gInPc1
neptunia	neptunium	k1gNnSc2
</s>
<s>
Binární	binární	k2eAgInPc1d1
oxidy	oxid	k1gInPc1
</s>
<s>
Neptunium	neptunium	k1gNnSc4
tvoří	tvořit	k5eAaImIp3nP
následující	následující	k2eAgInPc1d1
binární	binární	k2eAgInPc1d1
oxidy	oxid	k1gInPc1
<g/>
:	:	kIx,
NpO	NpO	k1gFnSc1
<g/>
3	#num#	k4
<g/>
·	·	k?
<g/>
2	#num#	k4
<g/>
H	H	kA
<g/>
2	#num#	k4
<g/>
O	O	kA
<g/>
,	,	kIx,
NpO	NpO	k1gFnSc1
<g/>
3	#num#	k4
<g/>
·	·	k?
<g/>
H	H	kA
<g/>
2	#num#	k4
<g/>
O	O	kA
<g/>
,	,	kIx,
Np	Np	k1gFnSc1
<g/>
3	#num#	k4
<g/>
O	o	k7c4
<g/>
8	#num#	k4
<g/>
,	,	kIx,
Np	Np	k1gFnSc1
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
5	#num#	k4
a	a	k8xC
NpO	NpO	k1gMnSc1
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Hydráty	hydrát	k1gInPc4
oxidu	oxid	k1gInSc2
neptuniového	ptuniový	k2eNgInSc2d1
(	(	kIx(
<g/>
NpO	NpO	k1gFnPc2
<g/>
3	#num#	k4
<g/>
)	)	kIx)
připravíme	připravit	k5eAaPmIp1nP
oxidací	oxidace	k1gFnSc7
vodných	vodný	k2eAgInPc2d1
roztoků	roztok	k1gInPc2
hydroxidu	hydroxid	k1gInSc2
neptuničného	ptuničný	k2eNgMnSc4d1
Np	Np	k1gMnSc4
<g/>
(	(	kIx(
<g/>
OH	OH	kA
<g/>
)	)	kIx)
<g/>
5	#num#	k4
při	při	k7c6
probublávání	probublávání	k1gNnSc6
ozonem	ozon	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tepelnou	tepelný	k2eAgFnSc7d1
degradací	degradace	k1gFnSc7
NpO	NpO	k1gFnSc2
<g/>
3	#num#	k4
<g/>
·	·	k?
<g/>
H	H	kA
<g/>
2	#num#	k4
<g/>
O	O	kA
získáme	získat	k5eAaPmIp1nP
oxid	oxid	k1gInSc1
neptuničitý	ptuničitý	k2eNgInSc1d1
(	(	kIx(
<g/>
Np	Np	k1gFnSc1
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
5	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc1
struktura	struktura	k1gFnSc1
je	být	k5eAaImIp3nS
podobná	podobný	k2eAgFnSc1d1
struktuře	struktura	k1gFnSc3
Np	Np	k1gFnSc1
<g/>
3	#num#	k4
<g/>
O	o	k7c4
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Np	Np	k?
<g/>
3	#num#	k4
<g/>
O	O	kA
<g/>
8	#num#	k4
získáme	získat	k5eAaPmIp1nP
oxidací	oxidace	k1gFnSc7
hydroxidu	hydroxid	k1gInSc2
neptuničitého	ptuničitý	k2eNgMnSc4d1
Np	Np	k1gMnSc4
<g/>
(	(	kIx(
<g/>
OH	OH	kA
<g/>
)	)	kIx)
<g/>
4	#num#	k4
nebo	nebo	k8xC
neptuničného	ptuničný	k2eNgInSc2d1
vzduchem	vzduch	k1gInSc7
nebo	nebo	k8xC
oxidem	oxid	k1gInSc7
dusičitým	dusičitý	k2eAgInSc7d1
NO2	NO2	k1gFnSc4
při	při	k7c6
300	#num#	k4
<g/>
–	–	k?
<g/>
400	#num#	k4
°	°	k?
<g/>
C.	C.	kA
Tento	tento	k3xDgInSc4
oxid	oxid	k1gInSc4
se	se	k3xPyFc4
snadno	snadno	k6eAd1
rozkládá	rozkládat	k5eAaImIp3nS
při	při	k7c6
zvýšené	zvýšený	k2eAgFnSc6d1
teplotě	teplota	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nad	nad	k7c7
teplotou	teplota	k1gFnSc7
500	#num#	k4
°	°	k?
<g/>
C	C	kA
ztrácí	ztrácet	k5eAaImIp3nS
kyslík	kyslík	k1gInSc4
a	a	k8xC
přechází	přecházet	k5eAaImIp3nS
na	na	k7c4
NpO	NpO	k1gFnSc4
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Oxid	oxid	k1gInSc1
neptuničitý	ptuničitý	k2eNgInSc1d1
(	(	kIx(
<g/>
NpO	NpO	k1gFnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
–	–	k?
nejstabilnější	stabilní	k2eAgInSc4d3
oxid	oxid	k1gInSc4
neptunia	neptunium	k1gNnSc2
lze	lze	k6eAd1
připravit	připravit	k5eAaPmF
termickou	termický	k2eAgFnSc7d1
dekompozicí	dekompozice	k1gFnSc7
mnoha	mnoho	k4c2
sloučenin	sloučenina	k1gFnPc2
neptunia	neptunium	k1gNnSc2
<g/>
,	,	kIx,
např.	např.	kA
hydroxidů	hydroxid	k1gInPc2
<g/>
,	,	kIx,
šťavelanů	šťavelan	k1gInPc2
<g/>
,	,	kIx,
dusičnanů	dusičnan	k1gInPc2
atd.	atd.	kA
při	při	k7c6
teplotách	teplota	k1gFnPc6
600	#num#	k4
<g/>
–	–	k?
<g/>
1000	#num#	k4
°	°	k?
<g/>
C.	C.	kA
Stejně	stejně	k6eAd1
jako	jako	k8xC,k8xS
ostatní	ostatní	k2eAgInPc1d1
dioxidy	dioxid	k1gInPc1
aktinoidů	aktinoid	k1gInPc2
má	mít	k5eAaImIp3nS
strukturu	struktura	k1gFnSc4
fluoritu	fluorit	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Ternární	ternární	k2eAgInPc1d1
a	a	k8xC
vyšší	vysoký	k2eAgInPc1d2
oxidy	oxid	k1gInPc1
</s>
<s>
Reakce	reakce	k1gFnSc1
NpO	NpO	k1gFnSc1
<g/>
2	#num#	k4
v	v	k7c6
pevné	pevný	k2eAgFnSc6d1
fázi	fáze	k1gFnSc6
s	s	k7c7
oxidy	oxid	k1gInPc7
mnoha	mnoho	k4c2
prvků	prvek	k1gInPc2
nebo	nebo	k8xC
srážení	srážení	k1gNnPc2
z	z	k7c2
taveniny	tavenina	k1gFnSc2
LiNO	Lina	k1gFnSc5
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
NaNO	NaNO	k1gFnPc2
<g/>
3	#num#	k4
poskytuje	poskytovat	k5eAaImIp3nS
ternární	ternární	k2eAgInPc1d1
oxidy	oxid	k1gInPc1
nebo	nebo	k8xC
oxidické	oxidický	k2eAgFnPc1d1
fáze	fáze	k1gFnPc1
se	se	k3xPyFc4
čtyř-	čtyř-	k?
<g/>
,	,	kIx,
pěti-	pěti-	k?
<g/>
,	,	kIx,
šesti-	šesti-	k?
a	a	k8xC
sedmivalentním	sedmivalentní	k2eAgNnSc7d1
neptuniem	neptunium	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Povaha	povaha	k1gFnSc1
produktu	produkt	k1gInSc2
závisí	záviset	k5eAaImIp3nS
na	na	k7c6
reakčních	reakční	k2eAgFnPc6d1
podmínkách	podmínka	k1gFnPc6
a	a	k8xC
použitém	použitý	k2eAgInSc6d1
oxidu	oxid	k1gInSc6
kovu	kov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Organokovové	organokovový	k2eAgFnPc1d1
sloučeniny	sloučenina	k1gFnPc1
a	a	k8xC
alkoxidy	alkoxida	k1gFnPc1
neptunia	neptunium	k1gNnSc2
</s>
<s>
Chlorid	chlorid	k1gInSc1
tris	tris	k1gInSc1
<g/>
(	(	kIx(
<g/>
cyklopentadienyl	cyklopentadienyl	k1gInSc1
<g/>
)	)	kIx)
<g/>
neptuničitý	ptuničitý	k2eNgMnSc1d1
(	(	kIx(
<g/>
C	C	kA
<g/>
5	#num#	k4
<g/>
H	H	kA
<g/>
5	#num#	k4
<g/>
)	)	kIx)
<g/>
3	#num#	k4
<g/>
NpCl	NpClum	k1gNnPc2
a	a	k8xC
odpovídající	odpovídající	k2eAgInSc1d1
fluorid	fluorid	k1gInSc1
byl	být	k5eAaImAgInS
připraven	připravit	k5eAaPmNgInS
β	β	k?
<g/>
−	−	k?
přeměnou	přeměna	k1gFnSc7
odpovídající	odpovídající	k2eAgFnSc2d1
sloučeniny	sloučenina	k1gFnSc2
uranu	uran	k1gInSc2
(	(	kIx(
<g/>
239	#num#	k4
<g/>
U	U	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Tetracyklopentadienyl	Tetracyklopentadienyl	k1gInSc1
neptuničitý	ptuničitý	k2eNgInSc1d1
(	(	kIx(
<g/>
C	C	kA
<g/>
5	#num#	k4
<g/>
H	H	kA
<g/>
5	#num#	k4
<g/>
)	)	kIx)
<g/>
4	#num#	k4
<g/>
Np	Np	k1gFnSc1
byl	být	k5eAaImAgInS
poprvé	poprvé	k6eAd1
připraven	připravit	k5eAaPmNgInS
reakcí	reakce	k1gFnSc7
C8H8K2	C8H8K2	k1gFnPc2
s	s	k7c7
NpCl	NpCl	k1gInSc1
<g/>
4	#num#	k4
v	v	k7c6
tetrahydrofuranu	tetrahydrofuran	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Infračervené	infračervený	k2eAgNnSc1d1
spektrum	spektrum	k1gNnSc1
prokázalo	prokázat	k5eAaPmAgNnS
sendvičovou	sendvičový	k2eAgFnSc4d1
strukturu	struktura	k1gFnSc4
komplexu	komplex	k1gInSc2
(	(	kIx(
<g/>
symetrie	symetrie	k1gFnSc2
D	D	kA
<g/>
8	#num#	k4
<g/>
h	h	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Reakce	reakce	k1gFnSc1
NpCl	NpCl	k1gInSc1
<g/>
4	#num#	k4
s	s	k7c7
alkoxidy	alkoxid	k1gMnPc7
lithia	lithium	k1gNnSc2
LiOR	LiOR	k1gFnSc2
(	(	kIx(
<g/>
R	R	kA
=	=	kIx~
CH	Ch	kA
<g/>
3	#num#	k4
<g/>
,	,	kIx,
C	C	kA
<g/>
2	#num#	k4
<g/>
H	H	kA
<g/>
5	#num#	k4
<g/>
)	)	kIx)
poskytuje	poskytovat	k5eAaImIp3nS
alkoxidy	alkoxid	k1gMnPc4
NpIV	NpIV	k1gMnSc1
Np	Np	k1gMnSc1
<g/>
(	(	kIx(
<g/>
OCH	och	k0
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
4	#num#	k4
nebo	nebo	k8xC
Np	Np	k1gMnSc1
<g/>
(	(	kIx(
<g/>
OC	OC	kA
<g/>
2	#num#	k4
<g/>
H	H	kA
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
5	#num#	k4
<g/>
)	)	kIx)
<g/>
4	#num#	k4
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
se	se	k3xPyFc4
transformují	transformovat	k5eAaBmIp3nP
na	na	k7c4
směsné	směsný	k2eAgMnPc4d1
alkoxidy	alkoxid	k1gMnPc4
NpBr	NpBra	k1gFnPc2
<g/>
(	(	kIx(
<g/>
OC	OC	kA
<g/>
2	#num#	k4
<g/>
H	H	kA
<g/>
5	#num#	k4
<g/>
)	)	kIx)
<g/>
3	#num#	k4
nebo	nebo	k8xC
NpBr	NpBr	k1gInSc1
<g/>
2	#num#	k4
<g/>
(	(	kIx(
<g/>
OC	OC	kA
<g/>
2	#num#	k4
<g/>
H	H	kA
<g/>
5	#num#	k4
<g/>
)	)	kIx)
<g/>
2	#num#	k4
protřepáváním	protřepávání	k1gNnSc7
s	s	k7c7
bromem	brom	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
přítomnosti	přítomnost	k1gFnSc6
volného	volný	k2eAgInSc2d1
ethoxidu	ethoxid	k1gInSc2
sodného	sodný	k2eAgInSc2d1
je	být	k5eAaImIp3nS
neptunium	neptunium	k1gNnSc1
oxidováno	oxidován	k2eAgNnSc1d1
na	na	k7c4
směsný	směsný	k2eAgInSc4d1
pětikoordinovaný	pětikoordinovaný	k2eAgInSc4d1
alkoxid	alkoxid	k1gInSc4
NpBr	NpBr	k1gMnSc1
<g/>
(	(	kIx(
<g/>
OC	OC	kA
<g/>
2	#num#	k4
<g/>
H	H	kA
<g/>
5	#num#	k4
<g/>
)	)	kIx)
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Cotton	Cotton	k1gInSc1
F.	F.	kA
A.	A.	kA
<g/>
,	,	kIx,
Wilkinson	Wilkinson	k1gMnSc1
J.	J.	kA
<g/>
:	:	kIx,
Anorganická	anorganický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
<g/>
,	,	kIx,
souborné	souborný	k2eAgNnSc1d1
zpracování	zpracování	k1gNnSc1
pro	pro	k7c4
pokročilé	pokročilý	k1gMnPc4
<g/>
,	,	kIx,
ACADEMIA	academia	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1973	#num#	k4
</s>
<s>
Holzbecher	Holzbechra	k1gFnPc2
Z.	Z.	kA
<g/>
:	:	kIx,
Analytická	analytický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
<g/>
,	,	kIx,
SNTL	SNTL	kA
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1974	#num#	k4
</s>
<s>
Dr	dr	kA
<g/>
.	.	kIx.
Heinrich	Heinrich	k1gMnSc1
Remy	remy	k1gNnSc2
<g/>
,	,	kIx,
Anorganická	anorganický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc4
1961	#num#	k4
</s>
<s>
N.	N.	kA
N.	N.	kA
Greenwood	Greenwood	k1gInSc1
–	–	k?
A.	A.	kA
Earnshaw	Earnshaw	k1gFnSc2
<g/>
,	,	kIx,
Chemie	chemie	k1gFnSc1
prvků	prvek	k1gInPc2
II	II	kA
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc2
1993	#num#	k4
ISBN	ISBN	kA
80-85427-38-9	80-85427-38-9	k4
</s>
<s>
VOHLÍDAL	VOHLÍDAL	kA
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
;	;	kIx,
ŠTULÍK	štulík	k1gInSc1
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
;	;	kIx,
JULÁK	JULÁK	kA
<g/>
,	,	kIx,
Alois	Alois	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chemické	chemický	k2eAgFnPc4d1
a	a	k8xC
analytické	analytický	k2eAgFnPc4d1
tabulky	tabulka	k1gFnPc4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Grada	Grada	k1gFnSc1
Publishing	Publishing	k1gInSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7169	#num#	k4
<g/>
-	-	kIx~
<g/>
855	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Neptuniová	Neptuniový	k2eAgFnSc1d1
rozpadová	rozpadový	k2eAgFnSc1d1
řada	řada	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
neptunium	neptunium	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
neptunium	neptunium	k1gNnSc4
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
prvků	prvek	k1gInPc2
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
17	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
H	H	kA
</s>
<s>
He	he	k0
</s>
<s>
Li	li	k8xS
</s>
<s>
Be	Be	k?
</s>
<s>
B	B	kA
</s>
<s>
C	C	kA
</s>
<s>
N	N	kA
</s>
<s>
O	o	k7c6
</s>
<s>
F	F	kA
</s>
<s>
Ne	ne	k9
</s>
<s>
Na	na	k7c6
</s>
<s>
Mg	mg	kA
</s>
<s>
Al	ala	k1gFnPc2
</s>
<s>
Si	se	k3xPyFc3
</s>
<s>
P	P	kA
</s>
<s>
S	s	k7c7
</s>
<s>
Cl	Cl	k?
</s>
<s>
Ar	ar	k1gInSc1
</s>
<s>
K	k	k7c3
</s>
<s>
Ca	ca	kA
</s>
<s>
Sc	Sc	k?
</s>
<s>
Ti	ten	k3xDgMnPc1
</s>
<s>
V	v	k7c6
</s>
<s>
Cr	cr	k0
</s>
<s>
Mn	Mn	k?
</s>
<s>
Fe	Fe	k?
</s>
<s>
Co	co	k3yInSc1,k3yRnSc1,k3yQnSc1
</s>
<s>
Ni	on	k3xPp3gFnSc4
</s>
<s>
Cu	Cu	k?
</s>
<s>
Zn	zn	kA
</s>
<s>
Ga	Ga	k?
</s>
<s>
Ge	Ge	k?
</s>
<s>
As	as	k9
</s>
<s>
Se	s	k7c7
</s>
<s>
Br	br	k0
</s>
<s>
Kr	Kr	k?
</s>
<s>
Rb	Rb	k?
</s>
<s>
Sr	Sr	k?
</s>
<s>
Y	Y	kA
</s>
<s>
Zr	Zr	k?
</s>
<s>
Nb	Nb	k?
</s>
<s>
Mo	Mo	k?
</s>
<s>
Tc	tc	k0
</s>
<s>
Ru	Ru	k?
</s>
<s>
Rh	Rh	k?
</s>
<s>
Pd	Pd	k?
</s>
<s>
Ag	Ag	k?
</s>
<s>
Cd	cd	kA
</s>
<s>
In	In	k?
</s>
<s>
Sn	Sn	k?
</s>
<s>
Sb	sb	kA
</s>
<s>
Te	Te	k?
</s>
<s>
I	i	k9
</s>
<s>
Xe	Xe	k?
</s>
<s>
Cs	Cs	k?
</s>
<s>
Ba	ba	k9
</s>
<s>
La	la	k1gNnSc1
</s>
<s>
Ce	Ce	k?
</s>
<s>
Pr	pr	k0
</s>
<s>
Nd	Nd	k?
</s>
<s>
Pm	Pm	k?
</s>
<s>
Sm	Sm	k?
</s>
<s>
Eu	Eu	k?
</s>
<s>
Gd	Gd	k?
</s>
<s>
Tb	Tb	k?
</s>
<s>
Dy	Dy	k?
</s>
<s>
Ho	on	k3xPp3gNnSc4
</s>
<s>
Er	Er	k?
</s>
<s>
Tm	Tm	k?
</s>
<s>
Yb	Yb	k?
</s>
<s>
Lu	Lu	k?
</s>
<s>
Hf	Hf	k?
</s>
<s>
Ta	ten	k3xDgFnSc1
</s>
<s>
W	W	kA
</s>
<s>
Re	re	k9
</s>
<s>
Os	osa	k1gFnPc2
</s>
<s>
Ir	Ir	k1gMnSc1
</s>
<s>
Pt	Pt	k?
</s>
<s>
Au	au	k0
</s>
<s>
Hg	Hg	k?
</s>
<s>
Tl	Tl	k?
</s>
<s>
Pb	Pb	k?
</s>
<s>
Bi	Bi	k?
</s>
<s>
Po	po	k7c6
</s>
<s>
At	At	k?
</s>
<s>
Rn	Rn	k?
</s>
<s>
Fr	fr	k0
</s>
<s>
Ra	ra	k0
</s>
<s>
Ac	Ac	k?
</s>
<s>
Th	Th	k?
</s>
<s>
Pa	Pa	kA
</s>
<s>
U	u	k7c2
</s>
<s>
Np	Np	k?
</s>
<s>
Pu	Pu	k?
</s>
<s>
Am	Am	k?
</s>
<s>
Cm	cm	kA
</s>
<s>
Bk	Bk	k?
</s>
<s>
Cf	Cf	k?
</s>
<s>
Es	es	k1gNnSc1
</s>
<s>
Fm	Fm	k?
</s>
<s>
Md	Md	k?
</s>
<s>
No	no	k9
</s>
<s>
Lr	Lr	k?
</s>
<s>
Rf	Rf	k?
</s>
<s>
Db	db	kA
</s>
<s>
Sg	Sg	k?
</s>
<s>
Bh	Bh	k?
</s>
<s>
Hs	Hs	k?
</s>
<s>
Mt	Mt	k?
</s>
<s>
Ds	Ds	k?
</s>
<s>
Rg	Rg	k?
</s>
<s>
Cn	Cn	k?
</s>
<s>
Nh	Nh	k?
</s>
<s>
Fl	Fl	k?
</s>
<s>
Mc	Mc	k?
</s>
<s>
Lv	Lv	k?
</s>
<s>
Ts	ts	k0
</s>
<s>
Og	Og	k?
</s>
<s>
Alkalické	alkalický	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Kovy	kov	k1gInPc1
alkalických	alkalický	k2eAgFnPc2d1
zemin	zemina	k1gFnPc2
</s>
<s>
Lanthanoidy	Lanthanoida	k1gFnPc1
</s>
<s>
Aktinoidy	Aktinoida	k1gFnPc1
</s>
<s>
Přechodné	přechodný	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Nepřechodné	přechodný	k2eNgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Polokovy	Polokův	k2eAgFnPc1d1
</s>
<s>
Nekovy	nekov	k1gInPc1
</s>
<s>
Halogeny	halogen	k1gInPc1
</s>
<s>
Vzácné	vzácný	k2eAgInPc1d1
plyny	plyn	k1gInPc1
</s>
<s>
neznámé	známý	k2eNgNnSc1d1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4171471-4	4171471-4	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85090879	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85090879	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Chemie	chemie	k1gFnSc1
</s>
