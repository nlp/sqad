<p>
<s>
Pojem	pojem	k1gInSc1	pojem
predátor	predátor	k1gMnSc1	predátor
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
definován	definovat	k5eAaBmNgInS	definovat
dvěma	dva	k4xCgFnPc7	dva
mírně	mírně	k6eAd1	mírně
odlišnými	odlišný	k2eAgInPc7d1	odlišný
způsoby	způsob	k1gInPc7	způsob
<g/>
,	,	kIx,	,
obecně	obecně	k6eAd1	obecně
se	se	k3xPyFc4	se
však	však	k9	však
o	o	k7c6	o
něm	on	k3xPp3gMnSc6	on
dá	dát	k5eAaPmIp3nS	dát
hovořit	hovořit	k5eAaImF	hovořit
jako	jako	k9	jako
o	o	k7c6	o
živém	živý	k2eAgInSc6d1	živý
organismu	organismus	k1gInSc6	organismus
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgNnSc4	jenž
přežití	přežití	k1gNnSc4	přežití
je	být	k5eAaImIp3nS	být
závislé	závislý	k2eAgNnSc1d1	závislé
na	na	k7c6	na
usmrcení	usmrcení	k1gNnSc6	usmrcení
jiného	jiný	k2eAgInSc2d1	jiný
živého	živý	k2eAgInSc2d1	živý
organismu	organismus	k1gInSc2	organismus
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
jeho	jeho	k3xOp3gFnSc2	jeho
konzumace	konzumace	k1gFnSc2	konzumace
<g/>
.	.	kIx.	.
</s>
<s>
Cíl	cíl	k1gInSc4	cíl
predátorovy	predátorův	k2eAgFnSc2d1	predátorův
potravy	potrava	k1gFnSc2	potrava
označujeme	označovat	k5eAaImIp1nP	označovat
jako	jako	k8xC	jako
kořist	kořist	k1gFnSc4	kořist
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Definice	definice	k1gFnSc2	definice
==	==	k?	==
</s>
</p>
<p>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
ekologie	ekologie	k1gFnSc2	ekologie
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
predátor	predátor	k1gMnSc1	predátor
brán	brát	k5eAaImNgMnS	brát
</s>
</p>
<p>
<s>
v	v	k7c6	v
užším	úzký	k2eAgInSc6d2	užší
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
jako	jako	k8xS	jako
vrcholný	vrcholný	k2eAgInSc1d1	vrcholný
článek	článek	k1gInSc1	článek
pastevně-kořistnického	pastevněořistnický	k2eAgInSc2d1	pastevně-kořistnický
potravního	potravní	k2eAgInSc2d1	potravní
řetězce	řetězec	k1gInSc2	řetězec
<g/>
.	.	kIx.	.
</s>
<s>
Vždy	vždy	k6eAd1	vždy
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
striktně	striktně	k6eAd1	striktně
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
masožravý	masožravý	k2eAgInSc4d1	masožravý
(	(	kIx(	(
<g/>
popř.	popř.	kA	popř.
všežravý	všežravý	k2eAgInSc1d1	všežravý
<g/>
)	)	kIx)	)
druh	druh	k1gInSc1	druh
žijící	žijící	k2eAgInSc1d1	žijící
dravým	dravý	k2eAgInSc7d1	dravý
způsobem	způsob	k1gInSc7	způsob
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
potravu	potrava	k1gFnSc4	potrava
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
živočišného	živočišný	k2eAgInSc2d1	živočišný
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
získává	získávat	k5eAaImIp3nS	získávat
buď	buď	k8xC	buď
aktivním	aktivní	k2eAgNnSc7d1	aktivní
lovením	lovení	k1gNnSc7	lovení
kořisti	kořist	k1gFnSc2	kořist
<g/>
,	,	kIx,	,
či	či	k8xC	či
pasivním	pasivní	k2eAgNnSc7d1	pasivní
vyčkáváním	vyčkávání	k1gNnSc7	vyčkávání
<g/>
,	,	kIx,	,
až	až	k8xS	až
kořist	kořist	k1gFnSc1	kořist
přijde	přijít	k5eAaPmIp3nS	přijít
sama	sám	k3xTgFnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
jaguár	jaguár	k1gMnSc1	jaguár
<g/>
,	,	kIx,	,
kosatka	kosatka	k1gFnSc1	kosatka
<g/>
,	,	kIx,	,
jestřáb	jestřáb	k1gMnSc1	jestřáb
<g/>
,	,	kIx,	,
slíďák	slíďák	k1gMnSc1	slíďák
<g/>
,	,	kIx,	,
z	z	k7c2	z
všežravců	všežravec	k1gMnPc2	všežravec
např.	např.	kA	např.
medvěd	medvěd	k1gMnSc1	medvěd
<g/>
.	.	kIx.	.
<g/>
v	v	k7c6	v
širším	široký	k2eAgInSc6d2	širší
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
jako	jako	k8xS	jako
kterýkoli	kterýkoli	k3yIgMnSc1	kterýkoli
živočich	živočich	k1gMnSc1	živočich
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
při	při	k7c6	při
konzumaci	konzumace	k1gFnSc6	konzumace
vyřadí	vyřadit	k5eAaPmIp3nP	vyřadit
celého	celý	k2eAgMnSc4d1	celý
jedince	jedinec	k1gMnSc4	jedinec
kořisti	kořist	k1gFnSc2	kořist
z	z	k7c2	z
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Vyřazení	vyřazení	k1gNnSc4	vyřazení
celého	celý	k2eAgMnSc2d1	celý
jedince	jedinec	k1gMnSc2	jedinec
z	z	k7c2	z
populace	populace	k1gFnSc2	populace
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
pravá	pravý	k2eAgFnSc1d1	pravá
predace	predace	k1gFnSc1	predace
<g/>
.	.	kIx.	.
</s>
<s>
Pravá	pravý	k2eAgFnSc1d1	pravá
predace	predace	k1gFnSc1	predace
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
i	i	k9	i
pojídání	pojídání	k1gNnSc1	pojídání
semen	semeno	k1gNnPc2	semeno
a	a	k8xC	a
vajec	vejce	k1gNnPc2	vejce
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
semena	semeno	k1gNnPc1	semeno
a	a	k8xC	a
vejce	vejce	k1gNnPc1	vejce
jsou	být	k5eAaImIp3nP	být
potenciální	potenciální	k2eAgInPc1d1	potenciální
organismy	organismus	k1gInPc1	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Příklad	příklad	k1gInSc1	příklad
pravé	pravý	k2eAgFnSc2d1	pravá
predace	predace	k1gFnSc2	predace
tedy	tedy	k9	tedy
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
navíc	navíc	k6eAd1	navíc
i	i	k8xC	i
takové	takový	k3xDgMnPc4	takový
živočichy	živočich	k1gMnPc4	živočich
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
perloočka	perloočka	k1gFnSc1	perloočka
<g/>
,	,	kIx,	,
mravkolev	mravkolev	k1gMnSc1	mravkolev
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
kur	kur	k1gMnSc1	kur
domácí	domácí	k2eAgMnSc1d1	domácí
<g/>
.	.	kIx.	.
<g/>
Poznámka	poznámka	k1gFnSc1	poznámka
<g/>
:	:	kIx,	:
Rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
pravou	pravý	k2eAgFnSc7d1	pravá
predací	predace	k1gFnSc7	predace
–	–	k?	–
pojídání	pojídání	k1gNnPc2	pojídání
semen	semeno	k1gNnPc2	semeno
–	–	k?	–
a	a	k8xC	a
pastvou	pastva	k1gFnSc7	pastva
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
spásání	spásání	k1gNnSc6	spásání
rostlin	rostlina	k1gFnPc2	rostlina
není	být	k5eNaImIp3nS	být
zkonzumována	zkonzumován	k2eAgFnSc1d1	zkonzumována
rostlina	rostlina	k1gFnSc1	rostlina
celá	celý	k2eAgFnSc1d1	celá
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
pouhá	pouhý	k2eAgFnSc1d1	pouhá
její	její	k3xOp3gFnSc1	její
část	část	k1gFnSc1	část
<g/>
.	.	kIx.	.
</s>
<s>
Rostliny	rostlina	k1gFnPc1	rostlina
jsou	být	k5eAaImIp3nP	být
navíc	navíc	k6eAd1	navíc
uzpůsobeny	uzpůsobit	k5eAaPmNgInP	uzpůsobit
k	k	k7c3	k
regeneraci	regenerace	k1gFnSc3	regenerace
ztracených	ztracený	k2eAgFnPc2d1	ztracená
částí	část	k1gFnPc2	část
svého	své	k1gNnSc2	své
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Matematické	matematický	k2eAgInPc4d1	matematický
modely	model	k1gInPc4	model
predátor-kořist	predátorořist	k1gInSc1	predátor-kořist
==	==	k?	==
</s>
</p>
<p>
<s>
Závislost	závislost	k1gFnSc1	závislost
množství	množství	k1gNnSc2	množství
kořisti	kořist	k1gFnSc2	kořist
na	na	k7c4	na
množství	množství	k1gNnSc4	množství
predátora	predátor	k1gMnSc2	predátor
lze	lze	k6eAd1	lze
ve	v	k7c6	v
zkratce	zkratka	k1gFnSc6	zkratka
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
následujícím	následující	k2eAgInSc7d1	následující
způsobem	způsob	k1gInSc7	způsob
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Čím	co	k3yRnSc7	co
více	hodně	k6eAd2	hodně
přibývá	přibývat	k5eAaImIp3nS	přibývat
kořisti	kořist	k1gFnSc2	kořist
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
více	hodně	k6eAd2	hodně
s	s	k7c7	s
jistým	jistý	k2eAgNnSc7d1	jisté
zpožděním	zpoždění	k1gNnSc7	zpoždění
přibývá	přibývat	k5eAaImIp3nS	přibývat
predátora	predátor	k1gMnSc4	predátor
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgNnSc1d2	veliký
množství	množství	k1gNnSc1	množství
predátora	predátor	k1gMnSc2	predátor
zvýší	zvýšit	k5eAaPmIp3nS	zvýšit
tlak	tlak	k1gInSc1	tlak
na	na	k7c4	na
kořist	kořist	k1gFnSc4	kořist
<g/>
,	,	kIx,	,
a	a	k8xC	a
té	ten	k3xDgFnSc3	ten
tak	tak	k6eAd1	tak
začne	začít	k5eAaPmIp3nS	začít
ubývat	ubývat	k5eAaImF	ubývat
<g/>
.	.	kIx.	.
</s>
<s>
Posléze	posléze	k6eAd1	posléze
s	s	k7c7	s
klesajícím	klesající	k2eAgNnSc7d1	klesající
množstvím	množství	k1gNnSc7	množství
kořisti	kořist	k1gFnSc2	kořist
začne	začít	k5eAaPmIp3nS	začít
klesat	klesat	k5eAaImF	klesat
i	i	k9	i
množství	množství	k1gNnSc1	množství
predátora	predátor	k1gMnSc2	predátor
<g/>
,	,	kIx,	,
kterému	který	k3yIgMnSc3	který
ubývá	ubývat	k5eAaImIp3nS	ubývat
potrava	potrava	k1gFnSc1	potrava
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ubývajícím	ubývající	k2eAgNnSc7d1	ubývající
množstvím	množství	k1gNnSc7	množství
predátora	predátor	k1gMnSc2	predátor
se	se	k3xPyFc4	se
pokles	pokles	k1gInSc1	pokles
kořisti	kořist	k1gFnSc2	kořist
zastaví	zastavit	k5eAaPmIp3nS	zastavit
a	a	k8xC	a
její	její	k3xOp3gNnPc4	její
množství	množství	k1gNnPc4	množství
začne	začít	k5eAaPmIp3nS	začít
opět	opět	k6eAd1	opět
stoupat	stoupat	k5eAaImF	stoupat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Klasické	klasický	k2eAgFnPc1d1	klasická
oscilace	oscilace	k1gFnPc1	oscilace
predátora	predátor	k1gMnSc2	predátor
a	a	k8xC	a
kořisti	kořist	k1gFnPc1	kořist
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
popsány	popsat	k5eAaPmNgFnP	popsat
především	především	k9	především
v	v	k7c6	v
případech	případ	k1gInPc6	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
predátor	predátor	k1gMnSc1	predátor
má	mít	k5eAaImIp3nS	mít
jen	jen	k9	jen
jednu	jeden	k4xCgFnSc4	jeden
převažující	převažující	k2eAgFnSc4d1	převažující
kořist	kořist	k1gFnSc4	kořist
<g/>
:	:	kIx,	:
tedy	tedy	k9	tedy
především	především	k9	především
vlk	vlk	k1gMnSc1	vlk
+	+	kIx~	+
zajíc	zajíc	k1gMnSc1	zajíc
bělák	bělák	k1gMnSc1	bělák
a	a	k8xC	a
liška	liška	k1gFnSc1	liška
polární	polární	k2eAgFnSc1d1	polární
+	+	kIx~	+
lumík	lumík	k1gMnSc1	lumík
v	v	k7c6	v
polárních	polární	k2eAgFnPc6d1	polární
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
oscilace	oscilace	k1gFnSc2	oscilace
kolísá	kolísat	k5eAaImIp3nS	kolísat
mezi	mezi	k7c7	mezi
6	[number]	k4	6
a	a	k8xC	a
10	[number]	k4	10
lety	let	k1gInPc7	let
podle	podle	k7c2	podle
podmínek	podmínka	k1gFnPc2	podmínka
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obecný	obecný	k2eAgInSc1d1	obecný
matematický	matematický	k2eAgInSc1d1	matematický
model	model	k1gInSc1	model
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
dx	dx	k?	dx
<g/>
/	/	kIx~	/
<g/>
dt	dt	k?	dt
=	=	kIx~	=
x	x	k?	x
*	*	kIx~	*
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
-	-	kIx~	-
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
g	g	kA	g
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
y	y	k?	y
<g/>
)	)	kIx)	)
*	*	kIx~	*
y	y	k?	y
</s>
</p>
<p>
<s>
dy	dy	k?	dy
<g/>
/	/	kIx~	/
<g/>
dt	dt	k?	dt
=	=	kIx~	=
h	h	k?	h
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
y	y	k?	y
<g/>
)	)	kIx)	)
*	*	kIx~	*
y	y	k?	y
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
-	-	kIx~	-
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
d	d	k?	d
*	*	kIx~	*
yVysvětlivky	yVysvětlivka	k1gFnSc2	yVysvětlivka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
x	x	k?	x
–	–	k?	–
množství	množství	k1gNnSc4	množství
kořisti	kořist	k1gFnSc2	kořist
</s>
</p>
<p>
<s>
y	y	k?	y
–	–	k?	–
množství	množství	k1gNnSc2	množství
predátora	predátor	k1gMnSc2	predátor
</s>
</p>
<p>
<s>
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
–	–	k?	–
dynamika	dynamika	k1gFnSc1	dynamika
růstu	růst	k1gInSc2	růst
populace	populace	k1gFnSc2	populace
kořisti	kořist	k1gFnSc2	kořist
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
populační	populační	k2eAgFnSc1d1	populační
dynamika	dynamika	k1gFnSc1	dynamika
<g/>
)	)	kIx)	)
bez	bez	k7c2	bez
přítomnosti	přítomnost	k1gFnSc2	přítomnost
dravce	dravec	k1gMnSc4	dravec
</s>
</p>
<p>
<s>
g	g	kA	g
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
y	y	k?	y
<g/>
)	)	kIx)	)
–	–	k?	–
funkcionální	funkcionální	k2eAgFnSc1d1	funkcionální
odezva	odezva	k1gFnSc1	odezva
</s>
</p>
<p>
<s>
h	h	k?	h
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
y	y	k?	y
<g/>
)	)	kIx)	)
–	–	k?	–
numerická	numerický	k2eAgFnSc1d1	numerická
odezva	odezva	k1gFnSc1	odezva
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
k-násobek	kásobek	k1gInSc1	k-násobek
funkcionální	funkcionální	k2eAgFnSc2d1	funkcionální
odezvy	odezva	k1gFnSc2	odezva
(	(	kIx(	(
<g/>
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
účinnost	účinnost	k1gFnSc4	účinnost
přeměny	přeměna	k1gFnSc2	přeměna
biomasy	biomasa	k1gFnSc2	biomasa
kořisti	kořist	k1gFnSc2	kořist
na	na	k7c4	na
biomasu	biomasa	k1gFnSc4	biomasa
predátora	predátor	k1gMnSc2	predátor
<g/>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
-	-	kIx~	-
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
d	d	k?	d
*	*	kIx~	*
y	y	k?	y
–	–	k?	–
exponenciální	exponenciální	k2eAgNnSc4d1	exponenciální
vymírání	vymírání	k1gNnSc4	vymírání
populace	populace	k1gFnSc2	populace
dravce	dravec	k1gMnSc2	dravec
(	(	kIx(	(
<g/>
dělo	dělo	k1gNnSc1	dělo
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nS	kdyby
nebyla	být	k5eNaImAgFnS	být
přítomna	přítomen	k2eAgFnSc1d1	přítomna
kořist	kořist	k1gFnSc1	kořist
–	–	k?	–
předchozí	předchozí	k2eAgMnSc1d1	předchozí
člen	člen	k1gMnSc1	člen
by	by	kYmCp3nS	by
byl	být	k5eAaImAgMnS	být
nulový	nulový	k2eAgMnSc1d1	nulový
<g/>
)	)	kIx)	)
<g/>
Funkcionální	funkcionální	k2eAgFnSc1d1	funkcionální
odezva	odezva	k1gFnSc1	odezva
g	g	kA	g
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
y	y	k?	y
<g/>
)	)	kIx)	)
–	–	k?	–
závislost	závislost	k1gFnSc1	závislost
množství	množství	k1gNnSc2	množství
sežraných	sežraný	k2eAgInPc2d1	sežraný
jedinců	jedinec	k1gMnPc2	jedinec
na	na	k7c6	na
nabídce	nabídka	k1gFnSc6	nabídka
kořisti	kořist	k1gFnSc2	kořist
x	x	k?	x
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Lotka-Volterovský	Lotka-Volterovský	k2eAgInSc1d1	Lotka-Volterovský
vztah	vztah	k1gInSc1	vztah
–	–	k?	–
lineární	lineární	k2eAgFnSc4d1	lineární
závislost	závislost	k1gFnSc4	závislost
g	g	kA	g
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
y	y	k?	y
<g/>
)	)	kIx)	)
=	=	kIx~	=
ax	ax	k?	ax
<g/>
,	,	kIx,	,
čím	co	k3yRnSc7	co
více	hodně	k6eAd2	hodně
kořisti	kořist	k1gFnSc2	kořist
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
více	hodně	k6eAd2	hodně
je	být	k5eAaImIp3nS	být
jí	on	k3xPp3gFnSc3	on
sežráno	sežrán	k2eAgNnSc1d1	sežráno
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
omezení	omezení	k1gNnSc2	omezení
<g/>
)	)	kIx)	)
–	–	k?	–
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jen	jen	k9	jen
idealizovaný	idealizovaný	k2eAgInSc1d1	idealizovaný
vztah	vztah	k1gInSc1	vztah
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Holing	Holing	k1gInSc1	Holing
I	I	kA	I
–	–	k?	–
upravený	upravený	k2eAgInSc4d1	upravený
Lotka-Volterovský	Lotka-Volterovský	k2eAgInSc4d1	Lotka-Volterovský
vztah	vztah	k1gInSc4	vztah
s	s	k7c7	s
maximálním	maximální	k2eAgInSc7d1	maximální
limitem	limit	k1gInSc7	limit
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
lineární	lineární	k2eAgInSc1d1	lineární
vztah	vztah	k1gInSc1	vztah
až	až	k9	až
do	do	k7c2	do
míry	míra	k1gFnSc2	míra
maximálního	maximální	k2eAgNnSc2d1	maximální
nasycení	nasycení	k1gNnSc2	nasycení
g	g	kA	g
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
y	y	k?	y
<g/>
)	)	kIx)	)
=	=	kIx~	=
ax	ax	k?	ax
pro	pro	k7c4	pro
ax	ax	k?	ax
<	<	kIx(	<
S	s	k7c7	s
<g/>
,	,	kIx,	,
g	g	kA	g
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
y	y	k?	y
<g/>
)	)	kIx)	)
=	=	kIx~	=
S	s	k7c7	s
pro	pro	k7c4	pro
ax	ax	k?	ax
>	>	kIx)	>
S	s	k7c7	s
–	–	k?	–
funguje	fungovat	k5eAaImIp3nS	fungovat
např.	např.	kA	např.
u	u	k7c2	u
filtrátorů	filtrátor	k1gInPc2	filtrátor
planktonu	plankton	k1gInSc2	plankton
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Holing	Holing	k1gInSc1	Holing
II	II	kA	II
–	–	k?	–
čím	co	k3yInSc7	co
víc	hodně	k6eAd2	hodně
je	být	k5eAaImIp3nS	být
predátor	predátor	k1gMnSc1	predátor
nasycen	nasytit	k5eAaPmNgMnS	nasytit
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
méně	málo	k6eAd2	málo
je	být	k5eAaImIp3nS	být
ochoten	ochoten	k2eAgMnSc1d1	ochoten
hledat	hledat	k5eAaImF	hledat
svou	svůj	k3xOyFgFnSc4	svůj
kořist	kořist	k1gFnSc4	kořist
<g/>
,	,	kIx,	,
funkce	funkce	k1gFnSc1	funkce
roste	růst	k5eAaImIp3nS	růst
limitně	limitně	k6eAd1	limitně
k	k	k7c3	k
míře	míra	k1gFnSc3	míra
maximálního	maximální	k2eAgNnSc2d1	maximální
nasycení	nasycení	k1gNnSc2	nasycení
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
y	y	k?	y
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
g	g	kA	g
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
y	y	k?	y
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
aSx	aSx	k?	aSx
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
ax	ax	k?	ax
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
nebo	nebo	k8xC	nebo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
y	y	k?	y
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
<s>
∗	∗	k?	∗
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
g	g	kA	g
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
y	y	k?	y
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
S	s	k7c7	s
<g/>
*	*	kIx~	*
<g/>
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
-e	-e	k?	-e
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-ax	x	k?	-ax
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
–	–	k?	–
funguje	fungovat	k5eAaImIp3nS	fungovat
např.	např.	kA	např.
u	u	k7c2	u
bezobratlých	bezobratlý	k2eAgMnPc2d1	bezobratlý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Holing	Holing	k1gInSc1	Holing
III	III	kA	III
–	–	k?	–
typický	typický	k2eAgInSc4d1	typický
pro	pro	k7c4	pro
rozhodování	rozhodování	k1gNnSc4	rozhodování
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
typy	typ	k1gInPc7	typ
kořisti	kořist	k1gFnSc2	kořist
<g/>
,	,	kIx,	,
hojnější	hojný	k2eAgFnSc1d2	hojnější
kořist	kořist	k1gFnSc1	kořist
je	být	k5eAaImIp3nS	být
výrazně	výrazně	k6eAd1	výrazně
preferována	preferován	k2eAgFnSc1d1	preferována
(	(	kIx(	(
<g/>
učení	učení	k1gNnPc1	učení
se	se	k3xPyFc4	se
jejímu	její	k3xOp3gInSc3	její
lovu	lov	k1gInSc3	lov
<g/>
/	/	kIx~	/
<g/>
sběru	sběr	k1gInSc2	sběr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
y	y	k?	y
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
g	g	kA	g
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
y	y	k?	y
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
aSx	aSx	k?	aSx
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
ax	ax	k?	ax
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
nebo	nebo	k8xC	nebo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
y	y	k?	y
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
<s>
∗	∗	k?	∗
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
g	g	kA	g
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
y	y	k?	y
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
S	s	k7c7	s
<g/>
*	*	kIx~	*
<g/>
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
-e	-e	k?	-e
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-ax	x	k?	-ax
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
–	–	k?	–
funguje	fungovat	k5eAaImIp3nS	fungovat
např.	např.	kA	např.
u	u	k7c2	u
některých	některý	k3yIgMnPc2	některý
ptáků	pták	k1gMnPc2	pták
a	a	k8xC	a
savců	savec	k1gMnPc2	savec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
pojmy	pojem	k1gInPc1	pojem
==	==	k?	==
</s>
</p>
<p>
<s>
Herbivor	Herbivor	k1gInSc1	Herbivor
<g/>
:	:	kIx,	:
Živí	živit	k5eAaImIp3nS	živit
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
částmi	část	k1gFnPc7	část
zelených	zelený	k2eAgFnPc2d1	zelená
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
tak	tak	k6eAd1	tak
nevyřazuje	vyřazovat	k5eNaImIp3nS	vyřazovat
z	z	k7c2	z
populace	populace	k1gFnSc2	populace
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
tyto	tento	k3xDgFnPc1	tento
zelené	zelený	k2eAgFnPc1d1	zelená
rostliny	rostlina	k1gFnPc1	rostlina
jsou	být	k5eAaImIp3nP	být
na	na	k7c4	na
požírání	požírání	k1gNnPc4	požírání
často	často	k6eAd1	často
adaptovány	adaptován	k2eAgFnPc1d1	adaptována
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Parazit	parazit	k1gMnSc1	parazit
<g/>
:	:	kIx,	:
Živočich	živočich	k1gMnSc1	živočich
<g/>
,	,	kIx,	,
rostlina	rostlina	k1gFnSc1	rostlina
nebo	nebo	k8xC	nebo
houba	houba	k1gFnSc1	houba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
v	v	k7c6	v
dospělosti	dospělost	k1gFnSc6	dospělost
nebo	nebo	k8xC	nebo
v	v	k7c6	v
některém	některý	k3yIgNnSc6	některý
z	z	k7c2	z
vývojových	vývojový	k2eAgNnPc2d1	vývojové
stadií	stadion	k1gNnPc2	stadion
využívá	využívat	k5eAaImIp3nS	využívat
těla	tělo	k1gNnPc4	tělo
jiného	jiný	k1gMnSc2	jiný
živočicha	živočich	k1gMnSc2	živočich
<g/>
,	,	kIx,	,
rostliny	rostlina	k1gFnSc2	rostlina
či	či	k8xC	či
houby	houba	k1gFnSc2	houba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Parazitoid	parazitoid	k1gMnSc1	parazitoid
<g/>
:	:	kIx,	:
Živočich	živočich	k1gMnSc1	živočich
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
nebo	nebo	k8xC	nebo
buňkách	buňka	k1gFnPc6	buňka
jiného	jiný	k2eAgMnSc2d1	jiný
živočicha	živočich	k1gMnSc2	živočich
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
na	na	k7c6	na
konci	konec	k1gInSc6	konec
svého	svůj	k3xOyFgInSc2	svůj
vývoje	vývoj	k1gInSc2	vývoj
usmrcuje	usmrcovat	k5eAaImIp3nS	usmrcovat
hostitelského	hostitelský	k2eAgMnSc4d1	hostitelský
živočicha	živočich	k1gMnSc4	živočich
(	(	kIx(	(
<g/>
a	a	k8xC	a
často	často	k6eAd1	často
i	i	k9	i
zkonzumuje	zkonzumovat	k5eAaPmIp3nS	zkonzumovat
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
viz	vidět	k5eAaImRp2nS	vidět
též	též	k9	též
larvální	larvální	k2eAgNnSc4d1	larvální
stadium	stadium	k1gNnSc4	stadium
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
pojímán	pojímán	k2eAgInSc1d1	pojímán
jen	jen	k9	jen
jako	jako	k8xS	jako
speciální	speciální	k2eAgInSc4d1	speciální
typ	typ	k1gInSc4	typ
parazita	parazit	k1gMnSc2	parazit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kořist	kořist	k1gFnSc1	kořist
<g/>
:	:	kIx,	:
Individuum	individuum	k1gNnSc1	individuum
(	(	kIx(	(
<g/>
druh	druh	k1gInSc4	druh
<g/>
)	)	kIx)	)
konzumovaný	konzumovaný	k2eAgInSc4d1	konzumovaný
dravcem	dravec	k1gMnSc7	dravec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dravec	dravec	k1gMnSc1	dravec
–	–	k?	–
kořist	kořist	k1gFnSc1	kořist
<g/>
:	:	kIx,	:
Model	model	k1gInSc1	model
vzájemné	vzájemný	k2eAgFnSc2d1	vzájemná
potřebnosti	potřebnost	k1gFnSc2	potřebnost
dravce	dravec	k1gMnPc4	dravec
a	a	k8xC	a
kořisti	kořist	k1gFnPc4	kořist
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Efekt	efekt	k1gInSc1	efekt
červené	červený	k2eAgFnSc2d1	červená
královny	královna	k1gFnSc2	královna
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
predátor	predátor	k1gMnSc1	predátor
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
