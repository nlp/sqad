<s>
Pojem	pojem	k1gInSc1	pojem
predátor	predátor	k1gMnSc1	predátor
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
definován	definovat	k5eAaBmNgInS	definovat
dvěma	dva	k4xCgFnPc7	dva
mírně	mírně	k6eAd1	mírně
odlišnými	odlišný	k2eAgInPc7d1	odlišný
způsoby	způsob	k1gInPc7	způsob
<g/>
,	,	kIx,	,
obecně	obecně	k6eAd1	obecně
se	se	k3xPyFc4	se
však	však	k9	však
o	o	k7c6	o
něm	on	k3xPp3gMnSc6	on
dá	dát	k5eAaPmIp3nS	dát
hovořit	hovořit	k5eAaImF	hovořit
jako	jako	k9	jako
o	o	k7c6	o
živém	živý	k2eAgInSc6d1	živý
organismu	organismus	k1gInSc6	organismus
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgNnSc4	jenž
přežití	přežití	k1gNnSc4	přežití
je	být	k5eAaImIp3nS	být
závislé	závislý	k2eAgNnSc1d1	závislé
na	na	k7c6	na
usmrcení	usmrcení	k1gNnSc6	usmrcení
jiného	jiný	k2eAgInSc2d1	jiný
živého	živý	k2eAgInSc2d1	živý
organismu	organismus	k1gInSc2	organismus
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
jeho	jeho	k3xOp3gFnSc2	jeho
konzumace	konzumace	k1gFnSc2	konzumace
<g/>
.	.	kIx.	.
</s>
