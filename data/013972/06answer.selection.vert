<s>
Světová	světový	k2eAgFnSc1d1
zdravotnická	zdravotnický	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
(	(	kIx(
<g/>
WHO	WHO	kA
<g/>
,	,	kIx,
z	z	k7c2
anglického	anglický	k2eAgInSc2d1
názvu	název	k1gInSc2
World	World	k1gInSc2
Health	Health	k1gNnSc2
Organization	Organization	k1gInSc1
<g/>
,	,	kIx,
česká	český	k2eAgFnSc1d1
zkratka	zkratka	k1gFnSc1
SZO	SZO	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
agentura	agentura	k1gFnSc1
Organizace	organizace	k1gFnSc2
spojených	spojený	k2eAgInPc2d1
národů	národ	k1gInPc2
<g/>
.	.	kIx.
</s>
