<p>
<s>
Kalous	Kalous	k1gMnSc1	Kalous
(	(	kIx(	(
<g/>
Asio	Asio	k1gMnSc1	Asio
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rod	rod	k1gInSc1	rod
čeledi	čeleď	k1gFnPc4	čeleď
puštíkovití	puštíkovitý	k2eAgMnPc1d1	puštíkovitý
<g/>
.	.	kIx.	.
</s>
<s>
Zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
7	[number]	k4	7
druhů	druh	k1gInPc2	druh
ušatých	ušatý	k2eAgFnPc2d1	ušatá
sov	sova	k1gFnPc2	sova
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
a	a	k8xC	a
slovenském	slovenský	k2eAgNnSc6d1	slovenské
území	území	k1gNnSc6	území
běžně	běžně	k6eAd1	běžně
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
dva	dva	k4xCgInPc1	dva
druhy	druh	k1gInPc1	druh
<g/>
:	:	kIx,	:
kalous	kalous	k1gMnSc1	kalous
ušatý	ušatý	k2eAgMnSc1d1	ušatý
a	a	k8xC	a
kalous	kalous	k1gMnSc1	kalous
pustovka	pustovka	k1gFnSc1	pustovka
<g/>
.	.	kIx.	.
</s>
<s>
Pustovka	pustovka	k1gFnSc1	pustovka
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
ovšem	ovšem	k9	ovšem
spíš	spíš	k9	spíš
protahuje	protahovat	k5eAaImIp3nS	protahovat
a	a	k8xC	a
zimuje	zimovat	k5eAaImIp3nS	zimovat
<g/>
,	,	kIx,	,
k	k	k7c3	k
zahnízdění	zahnízdění	k1gNnSc3	zahnízdění
dochází	docházet	k5eAaImIp3nS	docházet
jen	jen	k9	jen
výjimečně	výjimečně	k6eAd1	výjimečně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Druhy	druh	k1gInPc4	druh
==	==	k?	==
</s>
</p>
<p>
<s>
kalous	kalous	k1gMnSc1	kalous
africký	africký	k2eAgMnSc1d1	africký
(	(	kIx(	(
<g/>
Asio	Asio	k1gMnSc1	Asio
capensis	capensis	k1gFnSc2	capensis
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
kalous	kalous	k1gMnSc1	kalous
americký	americký	k2eAgMnSc1d1	americký
(	(	kIx(	(
<g/>
Asio	Asio	k1gMnSc1	Asio
stygius	stygius	k1gMnSc1	stygius
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
kalous	kalous	k1gMnSc1	kalous
etiopský	etiopský	k2eAgMnSc1d1	etiopský
(	(	kIx(	(
<g/>
Asio	Asio	k1gMnSc1	Asio
abyssinicus	abyssinicus	k1gMnSc1	abyssinicus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
kalous	kalous	k1gMnSc1	kalous
madagaskarský	madagaskarský	k2eAgMnSc1d1	madagaskarský
(	(	kIx(	(
<g/>
Asio	Asio	k1gMnSc1	Asio
madagascariensis	madagascariensis	k1gFnSc2	madagascariensis
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
kalous	kalous	k1gMnSc1	kalous
páskovaný	páskovaný	k2eAgMnSc1d1	páskovaný
(	(	kIx(	(
<g/>
Asio	Asio	k1gMnSc1	Asio
clamator	clamator	k1gMnSc1	clamator
<g/>
)	)	kIx)	)
–	–	k?	–
někdy	někdy	k6eAd1	někdy
řazen	řadit	k5eAaImNgInS	řadit
do	do	k7c2	do
rodů	rod	k1gInPc2	rod
Pseudoscops	Pseudoscops	k1gInSc1	Pseudoscops
nebo	nebo	k8xC	nebo
Rhinoptynx	Rhinoptynx	k1gInSc1	Rhinoptynx
</s>
</p>
<p>
<s>
kalous	kalous	k1gMnSc1	kalous
pustovka	pustovka	k1gFnSc1	pustovka
(	(	kIx(	(
<g/>
Asio	Asio	k1gMnSc1	Asio
flammeus	flammeus	k1gMnSc1	flammeus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
kalous	kalous	k1gMnSc1	kalous
ušatý	ušatý	k2eAgMnSc1d1	ušatý
(	(	kIx(	(
<g/>
Asio	Asio	k6eAd1	Asio
otus	otus	k6eAd1	otus
<g/>
)	)	kIx)	)
<g/>
Fosilní	fosilní	k2eAgMnPc1d1	fosilní
zástupci	zástupce	k1gMnPc1	zástupce
rodu	rod	k1gInSc2	rod
Asio	Asio	k1gNnSc1	Asio
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Asio	Asio	k1gMnSc1	Asio
brevipes	brevipes	k1gMnSc1	brevipes
</s>
</p>
<p>
<s>
Asio	Asio	k6eAd1	Asio
priscusJako	priscusJako	k6eAd1	priscusJako
kalousi	kalous	k1gMnPc1	kalous
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
nazýváni	nazýván	k2eAgMnPc1d1	nazýván
další	další	k2eAgInPc4d1	další
dva	dva	k4xCgInPc4	dva
druhy	druh	k1gInPc4	druh
podčeledi	podčeleď	k1gFnSc3	podčeleď
Asioninae	Asionina	k1gFnSc2	Asionina
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
však	však	k9	však
do	do	k7c2	do
jiných	jiný	k2eAgInPc2d1	jiný
rodů	rod	k1gInPc2	rod
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
kalous	kalous	k1gMnSc1	kalous
jamajský	jamajský	k2eAgMnSc1d1	jamajský
(	(	kIx(	(
<g/>
Pseudoscops	Pseudoscops	k1gInSc1	Pseudoscops
grammicus	grammicus	k1gInSc1	grammicus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
kalous	kalous	k1gMnSc1	kalous
šalomounský	šalomounský	k2eAgMnSc1d1	šalomounský
(	(	kIx(	(
<g/>
Nesasio	Nesasio	k1gMnSc1	Nesasio
solomonensis	solomonensis	k1gFnSc2	solomonensis
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
The	The	k?	The
Owl	Owl	k1gMnSc1	Owl
Pages	Pages	k1gMnSc1	Pages
</s>
</p>
