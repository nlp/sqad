<s>
S	s	k7c7	s
Jičínem	Jičín	k1gInSc7	Jičín
je	být	k5eAaImIp3nS	být
spojen	spojit	k5eAaPmNgInS	spojit
především	především	k9	především
spojeno	spojen	k2eAgNnSc1d1	spojeno
působení	působení	k1gNnSc1	působení
Albrecht	Albrecht	k1gMnSc1	Albrecht
z	z	k7c2	z
Valdštejna	Valdštejn	k1gInSc2	Valdštejn
<g/>
,	,	kIx,	,
vrchního	vrchní	k2eAgMnSc2d1	vrchní
velitele	velitel	k1gMnSc2	velitel
císařských	císařský	k2eAgInPc2d1	císařský
vojsk	vojsko	k1gNnPc2	vojsko
za	za	k7c2	za
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
v	v	k7c6	v
Jičíně	Jičín	k1gInSc6	Jičín
jako	jako	k8xS	jako
frýdlantský	frýdlantský	k2eAgMnSc1d1	frýdlantský
vévoda	vévoda	k1gMnSc1	vévoda
provedl	provést	k5eAaPmAgMnS	provést
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
přestavbu	přestavba	k1gFnSc4	přestavba
<g/>
,	,	kIx,	,
a	a	k8xC	a
loupežník	loupežník	k1gMnSc1	loupežník
Rumcajs	Rumcajsa	k1gFnPc2	Rumcajsa
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
v	v	k7c6	v
pohádkách	pohádka	k1gFnPc6	pohádka
Václava	Václav	k1gMnSc2	Václav
Čtvrtka	čtvrtek	k1gInSc2	čtvrtek
a	a	k8xC	a
Radka	Radek	k1gMnSc2	Radek
Pilaře	Pilař	k1gMnSc2	Pilař
sídlil	sídlit	k5eAaImAgInS	sídlit
ve	v	k7c6	v
fiktivním	fiktivní	k2eAgInSc6d1	fiktivní
lese	les	k1gInSc6	les
Řáholci	Řáholec	k1gInSc6	Řáholec
<g/>
.	.	kIx.	.
</s>
