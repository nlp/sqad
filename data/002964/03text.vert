<s>
Jičín	Jičín	k1gInSc1	Jičín
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Jitschin	Jitschin	k2eAgInSc1d1	Jitschin
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
v	v	k7c6	v
Královéhradeckém	královéhradecký	k2eAgInSc6d1	královéhradecký
kraji	kraj	k1gInSc6	kraj
v	v	k7c6	v
severovýchodních	severovýchodní	k2eAgFnPc6d1	severovýchodní
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
Jičínské	jičínský	k2eAgFnSc6d1	Jičínská
pahorkatině	pahorkatina	k1gFnSc6	pahorkatina
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Cidlině	Cidlina	k1gFnSc6	Cidlina
<g/>
.	.	kIx.	.
</s>
<s>
Rozloha	rozloha	k1gFnSc1	rozloha
města	město	k1gNnSc2	město
činí	činit	k5eAaImIp3nS	činit
2	[number]	k4	2
493	[number]	k4	493
ha	ha	kA	ha
a	a	k8xC	a
žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
přes	přes	k7c4	přes
16	[number]	k4	16
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Jičínem	Jičín	k1gInSc7	Jičín
je	být	k5eAaImIp3nS	být
spojen	spojit	k5eAaPmNgInS	spojit
především	především	k9	především
spojeno	spojen	k2eAgNnSc1d1	spojeno
působení	působení	k1gNnSc1	působení
Albrecht	Albrecht	k1gMnSc1	Albrecht
z	z	k7c2	z
Valdštejna	Valdštejn	k1gInSc2	Valdštejn
<g/>
,	,	kIx,	,
vrchního	vrchní	k2eAgMnSc2d1	vrchní
velitele	velitel	k1gMnSc2	velitel
císařských	císařský	k2eAgInPc2d1	císařský
vojsk	vojsko	k1gNnPc2	vojsko
za	za	k7c2	za
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
v	v	k7c6	v
Jičíně	Jičín	k1gInSc6	Jičín
jako	jako	k8xC	jako
frýdlantský	frýdlantský	k2eAgMnSc1d1	frýdlantský
vévoda	vévoda	k1gMnSc1	vévoda
provedl	provést	k5eAaPmAgMnS	provést
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
přestavbu	přestavba	k1gFnSc4	přestavba
<g/>
,	,	kIx,	,
a	a	k8xC	a
loupežník	loupežník	k1gMnSc1	loupežník
Rumcajs	Rumcajsa	k1gFnPc2	Rumcajsa
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
pohádkách	pohádka	k1gFnPc6	pohádka
Václava	Václav	k1gMnSc2	Václav
Čtvrtka	čtvrtek	k1gInSc2	čtvrtek
a	a	k8xC	a
Radka	Radek	k1gMnSc2	Radek
Pilaře	Pilař	k1gMnSc2	Pilař
sídlil	sídlit	k5eAaImAgInS	sídlit
ve	v	k7c6	v
fiktivním	fiktivní	k2eAgInSc6d1	fiktivní
lese	les	k1gInSc6	les
Řáholci	Řáholec	k1gInSc6	Řáholec
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
množství	množství	k1gNnSc4	množství
historických	historický	k2eAgFnPc2d1	historická
památek	památka	k1gFnPc2	památka
<g/>
,	,	kIx,	,
od	od	k7c2	od
gotických	gotický	k2eAgNnPc2d1	gotické
až	až	k9	až
po	po	k7c4	po
prvorepublikové	prvorepublikový	k2eAgFnPc4d1	prvorepubliková
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
pro	pro	k7c4	pro
polohu	poloha	k1gFnSc4	poloha
v	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
ráji	ráj	k1gInSc6	ráj
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Jičín	Jičín	k1gInSc1	Jičín
turisticky	turisticky	k6eAd1	turisticky
vyhledávaným	vyhledávaný	k2eAgNnSc7d1	vyhledávané
místem	místo	k1gNnSc7	místo
a	a	k8xC	a
bývá	bývat	k5eAaImIp3nS	bývat
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
jedno	jeden	k4xCgNnSc4	jeden
z	z	k7c2	z
nejkrásnějších	krásný	k2eAgNnPc2d3	nejkrásnější
měst	město	k1gNnPc2	město
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgInSc2d1	dnešní
Jičína	Jičín	k1gInSc2	Jičín
a	a	k8xC	a
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
okolí	okolí	k1gNnSc6	okolí
je	být	k5eAaImIp3nS	být
zaznamenáno	zaznamenán	k2eAgNnSc1d1	zaznamenáno
osídlení	osídlení	k1gNnSc1	osídlení
již	již	k6eAd1	již
v	v	k7c6	v
mladší	mladý	k2eAgFnSc6d2	mladší
době	doba	k1gFnSc6	doba
kamenné	kamenný	k2eAgInPc1d1	kamenný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
římské	římský	k2eAgNnSc4d1	římské
zde	zde	k6eAd1	zde
žili	žít	k5eAaImAgMnP	žít
Keltové	Kelt	k1gMnPc1	Kelt
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
zanechali	zanechat	k5eAaPmAgMnP	zanechat
šperky	šperk	k1gInPc4	šperk
<g/>
,	,	kIx,	,
nástroje	nástroj	k1gInPc4	nástroj
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
předměty	předmět	k1gInPc4	předmět
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
lze	lze	k6eAd1	lze
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
zhlédnout	zhlédnout	k5eAaPmF	zhlédnout
v	v	k7c6	v
jičínském	jičínský	k2eAgNnSc6d1	Jičínské
muzeu	muzeum	k1gNnSc6	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nich	on	k3xPp3gInPc6	on
osídlili	osídlit	k5eAaPmAgMnP	osídlit
oblast	oblast	k1gFnSc4	oblast
Slované	Slovan	k1gMnPc1	Slovan
<g/>
,	,	kIx,	,
rod	rod	k1gInSc1	rod
Slavníkovců	Slavníkovec	k1gMnPc2	Slavníkovec
-	-	kIx~	-
ti	ten	k3xDgMnPc1	ten
byli	být	k5eAaImAgMnP	být
vyvražděni	vyvraždit	k5eAaPmNgMnP	vyvraždit
roku	rok	k1gInSc3	rok
995	[number]	k4	995
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
jako	jako	k8xC	jako
královské	královský	k2eAgNnSc1d1	královské
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1300	[number]	k4	1300
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Starého	staré	k1gNnSc2	staré
Místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
brzy	brzy	k6eAd1	brzy
přeneseno	přenést	k5eAaPmNgNnS	přenést
asi	asi	k9	asi
o	o	k7c4	o
2	[number]	k4	2
km	km	kA	km
severněji	severně	k6eAd2	severně
na	na	k7c4	na
ploché	plochý	k2eAgNnSc4d1	ploché
návrší	návrší	k1gNnSc4	návrší
<g/>
,	,	kIx,	,
chráněné	chráněný	k2eAgInPc4d1	chráněný
řekou	řeka	k1gFnSc7	řeka
Cidlinou	Cidlina	k1gFnSc7	Cidlina
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
obci	obec	k1gFnSc6	obec
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1293	[number]	k4	1293
z	z	k7c2	z
listu	list	k1gInSc2	list
Guty	Gut	k2eAgFnPc4d1	Guta
Habsburské	habsburský	k2eAgFnPc4d1	habsburská
čili	čili	k8xC	čili
Jitky	Jitka	k1gFnPc1	Jitka
<g/>
,	,	kIx,	,
manželky	manželka	k1gFnPc1	manželka
Václava	Václav	k1gMnSc2	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
po	po	k7c6	po
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
dodnes	dodnes	k6eAd1	dodnes
nazývá	nazývat	k5eAaImIp3nS	nazývat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
ve	v	k7c6	v
městě	město	k1gNnSc6	město
rozvinuly	rozvinout	k5eAaPmAgInP	rozvinout
obchod	obchod	k1gInSc4	obchod
a	a	k8xC	a
řemesla	řemeslo	k1gNnSc2	řemeslo
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
postaveny	postaven	k2eAgInPc1d1	postaven
první	první	k4xOgFnPc1	první
gotické	gotický	k2eAgFnPc1d1	gotická
stavby	stavba	k1gFnPc1	stavba
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Ignáce	Ignác	k1gMnSc2	Ignác
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1337	[number]	k4	1337
prodává	prodávat	k5eAaImIp3nS	prodávat
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
Jan	Jan	k1gMnSc1	Jan
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
město	město	k1gNnSc1	město
šlechtickému	šlechtický	k2eAgInSc3d1	šlechtický
rodu	rod	k1gInSc3	rod
Vartenberků	Vartenberka	k1gMnPc2	Vartenberka
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
zakládali	zakládat	k5eAaImAgMnP	zakládat
v	v	k7c4	v
okolí	okolí	k1gNnSc4	okolí
města	město	k1gNnSc2	město
nové	nový	k2eAgFnSc2d1	nová
vsi	ves	k1gFnSc2	ves
a	a	k8xC	a
zasloužili	zasloužit	k5eAaPmAgMnP	zasloužit
se	se	k3xPyFc4	se
o	o	k7c4	o
jeho	on	k3xPp3gInSc4	on
další	další	k2eAgInSc4d1	další
rozvoj	rozvoj	k1gInSc4	rozvoj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
renesance	renesance	k1gFnSc2	renesance
vládli	vládnout	k5eAaImAgMnP	vládnout
městu	město	k1gNnSc3	město
Trčkové	Trčkové	k2eAgFnSc4d1	Trčkové
z	z	k7c2	z
Lípy	lípa	k1gFnSc2	lípa
-	-	kIx~	-
za	za	k7c2	za
jejich	jejich	k3xOp3gFnSc2	jejich
vlády	vláda	k1gFnSc2	vláda
byla	být	k5eAaImAgFnS	být
vystavěna	vystavěn	k2eAgFnSc1d1	vystavěna
Valdická	valdický	k2eAgFnSc1d1	Valdická
brána	brána	k1gFnSc1	brána
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
město	město	k1gNnSc1	město
vlastnili	vlastnit	k5eAaImAgMnP	vlastnit
Smiřičtí	smiřický	k2eAgMnPc1d1	smiřický
ze	z	k7c2	z
Smiřic	Smiřice	k1gFnPc2	Smiřice
<g/>
,	,	kIx,	,
ti	ten	k3xDgMnPc1	ten
se	se	k3xPyFc4	se
zase	zase	k9	zase
zasloužili	zasloužit	k5eAaPmAgMnP	zasloužit
o	o	k7c4	o
výstavbu	výstavba	k1gFnSc4	výstavba
jičínského	jičínský	k2eAgInSc2d1	jičínský
zámku	zámek	k1gInSc2	zámek
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
na	na	k7c6	na
Bílé	bílý	k2eAgFnSc6d1	bílá
hoře	hora	k1gFnSc6	hora
si	se	k3xPyFc3	se
Jičín	Jičín	k1gInSc4	Jičín
za	za	k7c4	za
svoje	svůj	k3xOyFgNnSc4	svůj
sídlo	sídlo	k1gNnSc4	sídlo
vybral	vybrat	k5eAaPmAgMnS	vybrat
Albrecht	Albrecht	k1gMnSc1	Albrecht
z	z	k7c2	z
Valdštejna	Valdštejno	k1gNnSc2	Valdštejno
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
tak	tak	k9	tak
de	de	k?	de
facto	facto	k1gNnSc1	facto
stalo	stát	k5eAaPmAgNnS	stát
hlavním	hlavní	k2eAgInSc7d1	hlavní
městem	město	k1gNnSc7	město
autonomního	autonomní	k2eAgNnSc2d1	autonomní
Frýdlantského	frýdlantský	k2eAgNnSc2d1	Frýdlantské
vévodství	vévodství	k1gNnSc2	vévodství
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgMnSc1d1	nový
panovník	panovník	k1gMnSc1	panovník
přivedl	přivést	k5eAaPmAgMnS	přivést
do	do	k7c2	do
města	město	k1gNnSc2	město
italské	italský	k2eAgMnPc4d1	italský
architekty	architekt	k1gMnPc4	architekt
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
je	on	k3xPp3gMnPc4	on
dále	daleko	k6eAd2	daleko
zvelebili	zvelebit	k5eAaPmAgMnP	zvelebit
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
barokním	barokní	k2eAgInSc7d1	barokní
kostel	kostel	k1gInSc4	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Jakuba	Jakub	k1gMnSc2	Jakub
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vévodově	vévodův	k2eAgFnSc6d1	vévodova
smrti	smrt	k1gFnSc6	smrt
byl	být	k5eAaImAgInS	být
Jičín	Jičín	k1gInSc1	Jičín
vypleněn	vyplenit	k5eAaPmNgInS	vyplenit
švédskými	švédský	k2eAgNnPc7d1	švédské
i	i	k8xC	i
císařskými	císařský	k2eAgNnPc7d1	císařské
vojsky	vojsko	k1gNnPc7	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
zbytek	zbytek	k1gInSc4	zbytek
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
byl	být	k5eAaImAgInS	být
násilně	násilně	k6eAd1	násilně
rekatolizován	rekatolizovat	k5eAaBmNgInS	rekatolizovat
<g/>
.	.	kIx.	.
</s>
<s>
Tovaryšstvo	tovaryšstvo	k1gNnSc1	tovaryšstvo
Ježíšovo	Ježíšův	k2eAgNnSc1d1	Ježíšovo
zde	zde	k6eAd1	zde
založilo	založit	k5eAaPmAgNnS	založit
gymnázium	gymnázium	k1gNnSc4	gymnázium
a	a	k8xC	a
jezuitskou	jezuitský	k2eAgFnSc4d1	jezuitská
kolej	kolej	k1gFnSc4	kolej
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
působil	působit	k5eAaImAgMnS	působit
například	například	k6eAd1	například
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Balbín	Balbín	k1gMnSc1	Balbín
<g/>
.	.	kIx.	.
</s>
<s>
Jičín	Jičín	k1gInSc1	Jičín
se	se	k3xPyFc4	se
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
stal	stát	k5eAaPmAgMnS	stát
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
center	centrum	k1gNnPc2	centrum
českého	český	k2eAgNnSc2d1	české
národního	národní	k2eAgNnSc2d1	národní
obrození	obrození	k1gNnSc2	obrození
<g/>
,	,	kIx,	,
na	na	k7c6	na
zdejším	zdejší	k2eAgNnSc6d1	zdejší
gymnáziu	gymnázium	k1gNnSc6	gymnázium
učil	učít	k5eAaPmAgMnS	učít
František	František	k1gMnSc1	František
Ladislav	Ladislav	k1gMnSc1	Ladislav
Rieger	Rieger	k1gMnSc1	Rieger
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zde	zde	k6eAd1	zde
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
závod	závod	k1gInSc1	závod
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
zemědělských	zemědělský	k2eAgInPc2d1	zemědělský
strojů	stroj	k1gInPc2	stroj
Agrostroj	agrostroj	k1gInSc1	agrostroj
(	(	kIx(	(
<g/>
Knotek	knotek	k1gInSc1	knotek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
velmi	velmi	k6eAd1	velmi
úspěšný	úspěšný	k2eAgMnSc1d1	úspěšný
za	za	k7c2	za
první	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
i	i	k9	i
za	za	k7c2	za
socialismu	socialismus	k1gInSc2	socialismus
a	a	k8xC	a
v	v	k7c6	v
omezené	omezený	k2eAgFnSc6d1	omezená
míře	míra	k1gFnSc6	míra
funguje	fungovat	k5eAaImIp3nS	fungovat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1866	[number]	k4	1866
se	se	k3xPyFc4	se
městem	město	k1gNnSc7	město
přehnala	přehnat	k5eAaPmAgFnS	přehnat
Prusko-rakouská	pruskoakouský	k2eAgFnSc1d1	prusko-rakouská
válka	válka	k1gFnSc1	válka
a	a	k8xC	a
odehrála	odehrát	k5eAaPmAgFnS	odehrát
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Jičína	Jičín	k1gInSc2	Jičín
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zde	zde	k6eAd1	zde
profesor	profesor	k1gMnSc1	profesor
Tomáš	Tomáš	k1gMnSc1	Tomáš
Garrigue	Garrigue	k1gFnPc2	Garrigue
Masaryk	Masaryk	k1gMnSc1	Masaryk
uskutečnil	uskutečnit	k5eAaPmAgMnS	uskutečnit
několik	několik	k4yIc4	několik
cyklů	cyklus	k1gInPc2	cyklus
přednášek	přednáška	k1gFnPc2	přednáška
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
univerzitních	univerzitní	k2eAgInPc6d1	univerzitní
kursech	kurs	k1gInPc6	kurs
jičínských	jičínský	k2eAgInPc6d1	jičínský
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
některé	některý	k3yIgFnPc1	některý
vyšly	vyjít	k5eAaPmAgFnP	vyjít
i	i	k9	i
knižně	knižně	k6eAd1	knižně
(	(	kIx(	(
<g/>
např.	např.	kA	např.
<g/>
:	:	kIx,	:
Národnostní	národnostní	k2eAgFnSc1d1	národnostní
filosofie	filosofie	k1gFnSc1	filosofie
doby	doba	k1gFnSc2	doba
novější	nový	k2eAgFnSc2d2	novější
<g/>
,	,	kIx,	,
Jičín	Jičín	k1gInSc1	Jičín
1905	[number]	k4	1905
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
první	první	k4xOgFnSc2	první
československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
zažilo	zažít	k5eAaPmAgNnS	zažít
město	město	k1gNnSc4	město
další	další	k2eAgInSc1d1	další
rozkvět	rozkvět	k1gInSc1	rozkvět
<g/>
,	,	kIx,	,
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
například	například	k6eAd1	například
vilová	vilový	k2eAgFnSc1d1	vilová
čtvrť	čtvrť	k1gFnSc1	čtvrť
Čeřovka	Čeřovka	k1gFnSc1	Čeřovka
<g/>
.	.	kIx.	.
</s>
<s>
Působili	působit	k5eAaImAgMnP	působit
zde	zde	k6eAd1	zde
architekt	architekt	k1gMnSc1	architekt
Čeněk	Čeněk	k1gMnSc1	Čeněk
Musil	Musil	k1gMnSc1	Musil
či	či	k8xC	či
spisovatel	spisovatel	k1gMnSc1	spisovatel
Václav	Václav	k1gMnSc1	Václav
Čtvrtek	čtvrtka	k1gFnPc2	čtvrtka
<g/>
.	.	kIx.	.
22	[number]	k4	22
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1921	[number]	k4	1921
Jičín	Jičín	k1gInSc1	Jičín
navštívil	navštívit	k5eAaPmAgMnS	navštívit
prezident	prezident	k1gMnSc1	prezident
Masaryk	Masaryk	k1gMnSc1	Masaryk
s	s	k7c7	s
ministerským	ministerský	k2eAgMnSc7d1	ministerský
předsedou	předseda	k1gMnSc7	předseda
E.	E.	kA	E.
Benešem	Beneš	k1gMnSc7	Beneš
a	a	k8xC	a
Fr.	Fr.	k1gMnSc7	Fr.
Udržalem	Udržal	k1gMnSc7	Udržal
ministrem	ministr	k1gMnSc7	ministr
obrany	obrana	k1gFnSc2	obrana
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgInS	účastnit
slavnosti	slavnost	k1gFnSc2	slavnost
22	[number]	k4	22
<g/>
.	.	kIx.	.
pěšího	pěší	k2eAgInSc2d1	pěší
pluku	pluk	k1gInSc2	pluk
<g/>
.	.	kIx.	.
</s>
<s>
Zajel	zajet	k5eAaPmAgMnS	zajet
hned	hned	k6eAd1	hned
na	na	k7c4	na
cvičiště	cvičiště	k1gNnSc4	cvičiště
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
promluvil	promluvit	k5eAaPmAgMnS	promluvit
k	k	k7c3	k
vojsku	vojsko	k1gNnSc3	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
navštívil	navštívit	k5eAaPmAgMnS	navštívit
jičínský	jičínský	k2eAgMnSc1d1	jičínský
"	"	kIx"	"
<g/>
Sokol	Sokol	k1gMnSc1	Sokol
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
závody	závod	k1gInPc1	závod
Knotkovy	Knotkův	k2eAgInPc1d1	Knotkův
<g/>
,	,	kIx,	,
podniky	podnik	k1gInPc1	podnik
rolnického	rolnický	k2eAgNnSc2d1	rolnické
družstva	družstvo	k1gNnSc2	družstvo
a	a	k8xC	a
radnici	radnice	k1gFnSc4	radnice
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
zbytek	zbytek	k1gInSc4	zbytek
okleštěných	okleštěný	k2eAgFnPc2d1	okleštěná
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
obsazeno	obsadit	k5eAaPmNgNnS	obsadit
německými	německý	k2eAgNnPc7d1	německé
vojsky	vojsko	k1gNnPc7	vojsko
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
okresu	okres	k1gInSc2	okres
"	"	kIx"	"
<g/>
Jitschin	Jitschin	k1gInSc1	Jitschin
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
znárodňování	znárodňování	k1gNnSc3	znárodňování
velkých	velký	k2eAgInPc2d1	velký
závodů	závod	k1gInPc2	závod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
obsadili	obsadit	k5eAaPmAgMnP	obsadit
město	město	k1gNnSc4	město
vojska	vojsko	k1gNnSc2	vojsko
zemí	zem	k1gFnPc2	zem
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
a	a	k8xC	a
polští	polský	k2eAgMnPc1d1	polský
vojáci	voják	k1gMnPc1	voják
zastřelili	zastřelit	k5eAaPmAgMnP	zastřelit
na	na	k7c6	na
křižovatce	křižovatka	k1gFnSc6	křižovatka
Na	na	k7c6	na
Letné	Letná	k1gFnSc6	Letná
muže	muž	k1gMnSc4	muž
a	a	k8xC	a
ženu	žena	k1gFnSc4	žena
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
tuto	tento	k3xDgFnSc4	tento
událost	událost	k1gFnSc4	událost
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
připomíná	připomínat	k5eAaImIp3nS	připomínat
pomník	pomník	k1gInSc1	pomník
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
základnu	základna	k1gFnSc4	základna
měla	mít	k5eAaImAgFnS	mít
sovětská	sovětský	k2eAgFnSc1d1	sovětská
vojska	vojsko	k1gNnPc1	vojsko
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
v	v	k7c6	v
jezuitské	jezuitský	k2eAgFnSc6d1	jezuitská
koleji	kolej	k1gFnSc6	kolej
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
přispěla	přispět	k5eAaPmAgFnS	přispět
k	k	k7c3	k
její	její	k3xOp3gFnSc3	její
devastaci	devastace	k1gFnSc3	devastace
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
privatizaci	privatizace	k1gFnSc3	privatizace
státních	státní	k2eAgFnPc2d1	státní
společností	společnost	k1gFnPc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
také	také	k9	také
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
nová	nový	k2eAgFnSc1d1	nová
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
zóna	zóna	k1gFnSc1	zóna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1994	[number]	k4	1994
<g/>
-	-	kIx~	-
<g/>
2002	[number]	k4	2002
byl	být	k5eAaImAgMnS	být
starostou	starosta	k1gMnSc7	starosta
Jičína	Jičín	k1gInSc2	Jičín
pozdější	pozdní	k2eAgMnSc1d2	pozdější
senátor	senátor	k1gMnSc1	senátor
MVDr.	MVDr.	kA	MVDr.
Jiří	Jiří	k1gMnSc1	Jiří
Liška	Liška	k1gMnSc1	Liška
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
odvolání	odvolání	k1gNnSc6	odvolání
předchozího	předchozí	k2eAgMnSc2d1	předchozí
starosty	starosta	k1gMnSc2	starosta
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
-	-	kIx~	-
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
znovuzvolen	znovuzvolit	k5eAaPmNgInS	znovuzvolit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
jej	on	k3xPp3gMnSc4	on
nahradil	nahradit	k5eAaPmAgMnS	nahradit
Jan	Jan	k1gMnSc1	Jan
Malý	Malý	k1gMnSc1	Malý
(	(	kIx(	(
<g/>
zvolen	zvolen	k2eAgMnSc1d1	zvolen
za	za	k7c4	za
ANO	ano	k9	ano
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnPc2	sčítání
1921	[number]	k4	1921
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
880	[number]	k4	880
domech	dům	k1gInPc6	dům
10	[number]	k4	10
478	[number]	k4	478
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
bylo	být	k5eAaImAgNnS	být
5	[number]	k4	5
065	[number]	k4	065
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
10	[number]	k4	10
166	[number]	k4	166
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
hlásilo	hlásit	k5eAaImAgNnS	hlásit
k	k	k7c3	k
československé	československý	k2eAgFnSc3d1	Československá
národnosti	národnost	k1gFnSc3	národnost
<g/>
,	,	kIx,	,
151	[number]	k4	151
k	k	k7c3	k
německé	německý	k2eAgFnSc3d1	německá
a	a	k8xC	a
9	[number]	k4	9
k	k	k7c3	k
židovské	židovská	k1gFnSc3	židovská
<g/>
.	.	kIx.	.
</s>
<s>
Žilo	žít	k5eAaImAgNnS	žít
zde	zde	k6eAd1	zde
5	[number]	k4	5
920	[number]	k4	920
římských	římský	k2eAgMnPc2d1	římský
katolíků	katolík	k1gMnPc2	katolík
<g/>
,	,	kIx,	,
238	[number]	k4	238
evangelíků	evangelík	k1gMnPc2	evangelík
<g/>
,	,	kIx,	,
1	[number]	k4	1
928	[number]	k4	928
příslušníků	příslušník	k1gMnPc2	příslušník
Církve	církev	k1gFnSc2	církev
československé	československý	k2eAgFnSc2d1	Československá
husitské	husitský	k2eAgFnSc2d1	husitská
a	a	k8xC	a
136	[number]	k4	136
židů	žid	k1gMnPc2	žid
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnPc2	sčítání
1930	[number]	k4	1930
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c4	v
1	[number]	k4	1
152	[number]	k4	152
domech	dům	k1gInPc6	dům
11	[number]	k4	11
034	[number]	k4	034
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
10	[number]	k4	10
636	[number]	k4	636
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
hlásilo	hlásit	k5eAaImAgNnS	hlásit
k	k	k7c3	k
československé	československý	k2eAgFnSc3d1	Československá
národnosti	národnost	k1gFnSc3	národnost
a	a	k8xC	a
172	[number]	k4	172
k	k	k7c3	k
německé	německý	k2eAgFnSc2d1	německá
<g/>
.	.	kIx.	.
</s>
<s>
Žilo	žít	k5eAaImAgNnS	žít
zde	zde	k6eAd1	zde
6	[number]	k4	6
195	[number]	k4	195
římských	římský	k2eAgMnPc2d1	římský
katolíků	katolík	k1gMnPc2	katolík
<g/>
,	,	kIx,	,
339	[number]	k4	339
evangelíků	evangelík	k1gMnPc2	evangelík
<g/>
,	,	kIx,	,
2	[number]	k4	2
802	[number]	k4	802
příslušníků	příslušník	k1gMnPc2	příslušník
Církve	církev	k1gFnSc2	církev
československé	československý	k2eAgFnSc2d1	Československá
husitské	husitský	k2eAgFnSc2d1	husitská
a	a	k8xC	a
119	[number]	k4	119
židů	žid	k1gMnPc2	žid
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
obchvatu	obchvat	k1gInSc6	obchvat
kolem	kolem	k7c2	kolem
města	město	k1gNnSc2	město
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
písmene	písmeno	k1gNnSc2	písmeno
U	U	kA	U
vede	vést	k5eAaImIp3nS	vést
silnice	silnice	k1gFnPc4	silnice
I.	I.	kA	I.
třídy	třída	k1gFnSc2	třída
č.	č.	k?	č.
16	[number]	k4	16
Řevničov-Královec	Řevničov-Královec	k1gMnSc1	Řevničov-Královec
<g/>
,	,	kIx,	,
na	na	k7c6	na
severozápadním	severozápadní	k2eAgInSc6d1	severozápadní
okraji	okraj	k1gInSc6	okraj
města	město	k1gNnSc2	město
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
odbočuje	odbočovat	k5eAaImIp3nS	odbočovat
silnice	silnice	k1gFnPc4	silnice
I.	I.	kA	I.
třídy	třída	k1gFnSc2	třída
č.	č.	k?	č.
35	[number]	k4	35
Turnov-Mohelnice	Turnov-Mohelnice	k1gFnSc1	Turnov-Mohelnice
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
odpojuje	odpojovat	k5eAaImIp3nS	odpojovat
až	až	k9	až
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Úlibice	Úlibice	k1gFnSc2	Úlibice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jižním	jižní	k2eAgInSc6d1	jižní
okraji	okraj	k1gInSc6	okraj
Jičína	Jičín	k1gInSc2	Jičín
odbočuje	odbočovat	k5eAaImIp3nS	odbočovat
silnice	silnice	k1gFnSc1	silnice
I.	I.	kA	I.
třídy	třída	k1gFnSc2	třída
č.	č.	k?	č.
32	[number]	k4	32
do	do	k7c2	do
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
<g/>
.	.	kIx.	.
</s>
<s>
Vede	vést	k5eAaImIp3nS	vést
odsud	odsud	k6eAd1	odsud
také	také	k9	také
silnice	silnice	k1gFnSc1	silnice
II	II	kA	II
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
č.	č.	k?	č.
286	[number]	k4	286
do	do	k7c2	do
Lomnice	Lomnice	k1gFnSc2	Lomnice
nad	nad	k7c7	nad
Popelkou	Popelka	k1gFnSc7	Popelka
a	a	k8xC	a
Jilemnice	Jilemnice	k1gFnSc1	Jilemnice
<g/>
.	.	kIx.	.
</s>
<s>
Okraje	okraj	k1gInPc1	okraj
města	město	k1gNnSc2	město
jsou	být	k5eAaImIp3nP	být
proto	proto	k8xC	proto
významnou	významný	k2eAgFnSc7d1	významná
silniční	silniční	k2eAgFnSc7d1	silniční
křižovatkou	křižovatka	k1gFnSc7	křižovatka
<g/>
.	.	kIx.	.
</s>
<s>
Blízko	blízko	k7c2	blízko
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
také	také	k9	také
nachází	nacházet	k5eAaImIp3nS	nacházet
autobusové	autobusový	k2eAgNnSc4d1	autobusové
nádraží	nádraží	k1gNnSc4	nádraží
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yRgInSc2	který
vyjíždějí	vyjíždět	k5eAaImIp3nP	vyjíždět
autobusy	autobus	k1gInPc1	autobus
například	například	k6eAd1	například
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
Liberce	Liberec	k1gInSc2	Liberec
<g/>
,	,	kIx,	,
Hradce	Hradec	k1gInSc2	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
,	,	kIx,	,
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
Špindlerova	Špindlerův	k2eAgInSc2d1	Špindlerův
Mlýna	mlýn	k1gInSc2	mlýn
<g/>
,	,	kIx,	,
Kněžmostu	Kněžmost	k1gInSc2	Kněžmost
nebo	nebo	k8xC	nebo
do	do	k7c2	do
Hlinska	Hlinsko	k1gNnSc2	Hlinsko
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
i	i	k9	i
vlakové	vlakový	k2eAgNnSc1d1	vlakové
nádraží	nádraží	k1gNnSc1	nádraží
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgFnSc1d1	ležící
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
041	[number]	k4	041
zMěstská_autobusová_doprava_v_Královéhradeckém_kraji	zMěstská_autobusová_doprava_v_Královéhradeckém_krat	k5eAaPmIp1nS	zMěstská_autobusová_doprava_v_Královéhradeckém_krat
<g/>
#	#	kIx~	#
<g/>
Ji	on	k3xPp3gFnSc4	on
<g/>
.	.	kIx.	.
<g/>
C	C	kA	C
<g/>
4.8	[number]	k4	4.8
<g/>
D.C	D.C	k1gFnSc1	D.C
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
ADnTurnova	ADnTurnov	k1gInSc2	ADnTurnov
do	do	k7c2	do
Hradce	Hradec	k1gInSc2	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yRgInSc2	který
odbočuje	odbočovat	k5eAaImIp3nS	odbočovat
regionální	regionální	k2eAgFnSc1d1	regionální
trať	trať	k1gFnSc1	trať
061	[number]	k4	061
do	do	k7c2	do
Kopidlna	Kopidlno	k1gNnSc2	Kopidlno
a	a	k8xC	a
Nymburka	Nymburk	k1gInSc2	Nymburk
<g/>
,	,	kIx,	,
po	po	k7c6	po
které	který	k3yRgFnSc6	který
jezdí	jezdit	k5eAaImIp3nP	jezdit
vlaky	vlak	k1gInPc1	vlak
ve	v	k7c6	v
dvouhodinových	dvouhodinový	k2eAgInPc6d1	dvouhodinový
intervalech	interval	k1gInPc6	interval
<g/>
.	.	kIx.	.
</s>
<s>
Vlakové	vlakový	k2eAgNnSc1d1	vlakové
nádraží	nádraží	k1gNnSc1	nádraží
má	mít	k5eAaImIp3nS	mít
4	[number]	k4	4
osobní	osobní	k2eAgFnSc2d1	osobní
koleje	kolej	k1gFnSc2	kolej
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
Jičín	Jičín	k1gInSc1	Jičín
těží	těžet	k5eAaImIp3nS	těžet
především	především	k6eAd1	především
z	z	k7c2	z
turismu	turismus	k1gInSc2	turismus
a	a	k8xC	a
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Továrny	továrna	k1gFnPc1	továrna
zde	zde	k6eAd1	zde
mají	mít	k5eAaImIp3nP	mít
společnosti	společnost	k1gFnPc1	společnost
jako	jako	k8xS	jako
Ronal	Ronal	k1gMnSc1	Ronal
<g/>
,	,	kIx,	,
Continental	Continental	k1gMnSc1	Continental
<g/>
,	,	kIx,	,
AEG	AEG	kA	AEG
nebo	nebo	k8xC	nebo
SpofaDental	SpofaDental	k1gMnSc1	SpofaDental
<g/>
,	,	kIx,	,
v	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
stále	stále	k6eAd1	stále
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
nástupce	nástupce	k1gMnSc4	nástupce
Agrostroje	agrostroj	k1gInSc2	agrostroj
<g/>
,	,	kIx,	,
AGS	AGS	kA	AGS
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
Seco	Seco	k1gMnSc1	Seco
GROUP	GROUP	kA	GROUP
<g/>
.	.	kIx.	.
</s>
<s>
Sídlo	sídlo	k1gNnSc1	sídlo
zde	zde	k6eAd1	zde
má	mít	k5eAaImIp3nS	mít
též	též	k9	též
dopravní	dopravní	k2eAgFnSc1d1	dopravní
firma	firma	k1gFnSc1	firma
C.	C.	kA	C.
<g/>
S.	S.	kA	S.
<g/>
CARGO	CARGO	kA	CARGO
a.s.	a.s.	k?	a.s.
Probíhá	probíhat	k5eAaImIp3nS	probíhat
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
města	město	k1gNnSc2	město
-	-	kIx~	-
opravy	oprava	k1gFnPc1	oprava
památek	památka	k1gFnPc2	památka
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
Valdštejnova	Valdštejnův	k2eAgNnSc2d1	Valdštejnovo
náměstí	náměstí	k1gNnSc2	náměstí
<g/>
,	,	kIx,	,
chodníků	chodník	k1gInPc2	chodník
<g/>
,	,	kIx,	,
silnic	silnice	k1gFnPc2	silnice
či	či	k8xC	či
panelových	panelový	k2eAgInPc2d1	panelový
domů	dům	k1gInPc2	dům
z	z	k7c2	z
převážně	převážně	k6eAd1	převážně
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
naplánována	naplánován	k2eAgFnSc1d1	naplánována
je	být	k5eAaImIp3nS	být
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
přeměna	přeměna	k1gFnSc1	přeměna
veřejné	veřejný	k2eAgFnSc2d1	veřejná
dopravy	doprava	k1gFnSc2	doprava
(	(	kIx(	(
<g/>
přesun	přesun	k1gInSc1	přesun
autobusového	autobusový	k2eAgNnSc2d1	autobusové
nádraží	nádraží	k1gNnSc2	nádraží
<g/>
,	,	kIx,	,
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
vlakového	vlakový	k2eAgNnSc2d1	vlakové
nádraží	nádraží	k1gNnSc2	nádraží
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
též	též	k9	též
postavilo	postavit	k5eAaPmAgNnS	postavit
bazénový	bazénový	k2eAgInSc4d1	bazénový
komplex	komplex	k1gInSc4	komplex
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
plovárny	plovárna	k1gFnSc2	plovárna
ze	z	k7c2	z
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
u	u	k7c2	u
rybníku	rybník	k1gInSc3	rybník
Kníže	kníže	k1gNnSc1wR	kníže
<g/>
.	.	kIx.	.
</s>
<s>
Nevyřešený	vyřešený	k2eNgMnSc1d1	nevyřešený
nebo	nebo	k8xC	nebo
nedořešený	dořešený	k2eNgMnSc1d1	nedořešený
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
problém	problém	k1gInSc4	problém
několika	několik	k4yIc2	několik
chátrajících	chátrající	k2eAgInPc2d1	chátrající
objektů	objekt	k1gInPc2	objekt
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
jezuitské	jezuitský	k2eAgFnSc2d1	jezuitská
koleje	kolej	k1gFnSc2	kolej
<g/>
,	,	kIx,	,
kasáren	kasárny	k1gFnPc2	kasárny
a	a	k8xC	a
Hotelu	hotel	k1gInSc2	hotel
Start	start	k1gInSc1	start
(	(	kIx(	(
<g/>
ten	ten	k3xDgMnSc1	ten
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
soukromá	soukromý	k2eAgFnSc1d1	soukromá
společnost	společnost	k1gFnSc1	společnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
v	v	k7c6	v
Jičíně	Jičín	k1gInSc6	Jičín
<g/>
.	.	kIx.	.
</s>
<s>
Zbytky	zbytek	k1gInPc1	zbytek
městských	městský	k2eAgFnPc2d1	městská
hradeb	hradba	k1gFnPc2	hradba
na	na	k7c6	na
jihovýchodním	jihovýchodní	k2eAgInSc6d1	jihovýchodní
okraji	okraj	k1gInSc6	okraj
historického	historický	k2eAgNnSc2d1	historické
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
za	za	k7c7	za
kostelem	kostel	k1gInSc7	kostel
sv.	sv.	kA	sv.
Jakuba	Jakub	k1gMnSc2	Jakub
<g/>
)	)	kIx)	)
končí	končit	k5eAaImIp3nS	končit
Valdickou	valdický	k2eAgFnSc7d1	Valdická
branou	brána	k1gFnSc7	brána
<g/>
.	.	kIx.	.
</s>
<s>
Kamenná	kamenný	k2eAgFnSc1d1	kamenná
hranolová	hranolový	k2eAgFnSc1d1	hranolová
věž	věž	k1gFnSc1	věž
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1568-1570	[number]	k4	1568-1570
byla	být	k5eAaImAgFnS	být
různě	různě	k6eAd1	různě
upravována	upravován	k2eAgFnSc1d1	upravována
<g/>
,	,	kIx,	,
dřevěný	dřevěný	k2eAgInSc1d1	dřevěný
ochoz	ochoz	k1gInSc1	ochoz
a	a	k8xC	a
jehlanovitá	jehlanovitý	k2eAgFnSc1d1	jehlanovitá
střecha	střecha	k1gFnSc1	střecha
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
romantické	romantický	k2eAgFnSc2d1	romantická
přestavby	přestavba	k1gFnSc2	přestavba
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1840	[number]	k4	1840
<g/>
.	.	kIx.	.
</s>
<s>
Valdickou	valdický	k2eAgFnSc7d1	Valdická
branou	brána	k1gFnSc7	brána
se	se	k3xPyFc4	se
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
kolem	kolem	k7c2	kolem
kostela	kostel	k1gInSc2	kostel
sv.	sv.	kA	sv.
Jakuba	Jakub	k1gMnSc2	Jakub
do	do	k7c2	do
jihovýchodního	jihovýchodní	k2eAgInSc2d1	jihovýchodní
rohu	roh	k1gInSc2	roh
Valdštejnova	Valdštejnův	k2eAgNnSc2d1	Valdštejnovo
náměstí	náměstí	k1gNnSc2	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Valdštejnovo	Valdštejnův	k2eAgNnSc1d1	Valdštejnovo
náměstí	náměstí	k1gNnSc1	náměstí
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
pravidelné	pravidelný	k2eAgFnPc1d1	pravidelná
a	a	k8xC	a
mírně	mírně	k6eAd1	mírně
svažité	svažitý	k2eAgNnSc4d1	svažité
obdélné	obdélný	k2eAgNnSc4d1	obdélné
náměstí	náměstí	k1gNnSc4	náměstí
<g/>
,	,	kIx,	,
asi	asi	k9	asi
150	[number]	k4	150
m	m	kA	m
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
zachovalo	zachovat	k5eAaPmAgNnS	zachovat
úplné	úplný	k2eAgNnSc1d1	úplné
podloubí	podloubí	k1gNnSc1	podloubí
po	po	k7c6	po
všech	všecek	k3xTgFnPc6	všecek
čtyřech	čtyři	k4xCgFnPc6	čtyři
stranách	strana	k1gFnPc6	strana
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
zámku	zámek	k1gInSc2	zámek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
jeho	jeho	k3xOp3gFnSc4	jeho
jihovýchodní	jihovýchodní	k2eAgFnSc4d1	jihovýchodní
část	část	k1gFnSc4	část
<g/>
.	.	kIx.	.
</s>
<s>
Měšťanské	měšťanský	k2eAgInPc1d1	měšťanský
domy	dům	k1gInPc1	dům
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
raně	raně	k6eAd1	raně
barokní	barokní	k2eAgFnSc7d1	barokní
<g/>
,	,	kIx,	,
většinou	většina	k1gFnSc7	většina
s	s	k7c7	s
fasádami	fasáda	k1gFnPc7	fasáda
a	a	k8xC	a
někde	někde	k6eAd1	někde
i	i	k8xC	i
přistavěným	přistavěný	k2eAgNnSc7d1	přistavěné
druhým	druhý	k4xOgNnSc7	druhý
patrem	patro	k1gNnSc7	patro
z	z	k7c2	z
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Nejcennější	cenný	k2eAgInPc1d3	nejcennější
domy	dům	k1gInPc1	dům
stojí	stát	k5eAaImIp3nP	stát
v	v	k7c6	v
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
a	a	k8xC	a
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
náměstí	náměstí	k1gNnSc2	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Mariánský	mariánský	k2eAgInSc1d1	mariánský
sloup	sloup	k1gInSc1	sloup
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1702	[number]	k4	1702
<g/>
,	,	kIx,	,
sochy	socha	k1gFnPc1	socha
na	na	k7c6	na
podstavci	podstavec	k1gInSc6	podstavec
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1795	[number]	k4	1795
<g/>
.	.	kIx.	.
</s>
<s>
Korunovační	korunovační	k2eAgFnSc1d1	korunovační
kašna	kašna	k1gFnSc1	kašna
<g/>
,	,	kIx,	,
kamenný	kamenný	k2eAgInSc1d1	kamenný
pavilon	pavilon	k1gInSc1	pavilon
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1834	[number]	k4	1834
Kašna	kašna	k1gFnSc1	kašna
Amfitrité	Amfitritý	k2eAgFnSc2d1	Amfitritý
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1836	[number]	k4	1836
se	s	k7c7	s
sochou	socha	k1gFnSc7	socha
od	od	k7c2	od
Jana	Jan	k1gMnSc2	Jan
Suchardy	Sucharda	k1gMnSc2	Sucharda
<g/>
.	.	kIx.	.
</s>
<s>
Zámek	zámek	k1gInSc1	zámek
<g/>
,	,	kIx,	,
rozlehlá	rozlehlý	k2eAgFnSc1d1	rozlehlá
obdélná	obdélný	k2eAgFnSc1d1	obdélná
dvoupatrová	dvoupatrový	k2eAgFnSc1d1	dvoupatrová
stavba	stavba	k1gFnSc1	stavba
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
založil	založit	k5eAaPmAgMnS	založit
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1500	[number]	k4	1500
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Trčka	Trčka	k1gMnSc1	Trčka
z	z	k7c2	z
Lípy	lípa	k1gFnSc2	lípa
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1608	[number]	k4	1608
rozšířil	rozšířit	k5eAaPmAgMnS	rozšířit
Zikmund	Zikmund	k1gMnSc1	Zikmund
Smiřický	smiřický	k2eAgMnSc1d1	smiřický
a	a	k8xC	a
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1624	[number]	k4	1624
přestavěl	přestavět	k5eAaPmAgMnS	přestavět
Albrecht	Albrecht	k1gMnSc1	Albrecht
z	z	k7c2	z
Valdštejna	Valdštejn	k1gInSc2	Valdštejn
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
čtvercová	čtvercový	k2eAgFnSc1d1	čtvercová
budova	budova	k1gFnSc1	budova
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
nádvořími	nádvoří	k1gNnPc7	nádvoří
<g/>
,	,	kIx,	,
zčásti	zčásti	k6eAd1	zčásti
s	s	k7c7	s
arkádami	arkáda	k1gFnPc7	arkáda
<g/>
.	.	kIx.	.
</s>
<s>
Průčelí	průčelí	k1gNnSc1	průčelí
do	do	k7c2	do
náměstí	náměstí	k1gNnSc2	náměstí
s	s	k7c7	s
rohovými	rohový	k2eAgInPc7d1	rohový
arkýři	arkýř	k1gInPc7	arkýř
má	mít	k5eAaImIp3nS	mít
stejné	stejný	k2eAgNnSc1d1	stejné
podloubí	podloubí	k1gNnSc1	podloubí
jako	jako	k8xS	jako
okolní	okolní	k2eAgInPc1d1	okolní
měšťanské	měšťanský	k2eAgInPc1d1	měšťanský
domy	dům	k1gInPc1	dům
<g/>
.	.	kIx.	.
</s>
<s>
Podoba	podoba	k1gFnSc1	podoba
fasády	fasáda	k1gFnSc2	fasáda
se	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
nevýraznými	výrazný	k2eNgInPc7d1	nevýrazný
rizality	rizalit	k1gInPc7	rizalit
a	a	k8xC	a
plochými	plochý	k2eAgInPc7d1	plochý
trojúhelnými	trojúhelný	k2eAgInPc7d1	trojúhelný
štíty	štít	k1gInPc7	štít
je	být	k5eAaImIp3nS	být
patrně	patrně	k6eAd1	patrně
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1775	[number]	k4	1775
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
části	část	k1gFnSc6	část
zámku	zámek	k1gInSc2	zámek
sídlí	sídlet	k5eAaImIp3nS	sídlet
krajské	krajský	k2eAgNnSc1d1	krajské
muzeum	muzeum	k1gNnSc1	muzeum
a	a	k8xC	a
galerie	galerie	k1gFnSc1	galerie
<g/>
.	.	kIx.	.
</s>
<s>
Barokní	barokní	k2eAgInSc1d1	barokní
kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Jakuba	Jakub	k1gMnSc2	Jakub
Většího	veliký	k2eAgMnSc2d2	veliký
u	u	k7c2	u
Valdické	valdický	k2eAgFnSc2d1	Valdická
brány	brána	k1gFnSc2	brána
měl	mít	k5eAaImAgInS	mít
sloužit	sloužit	k5eAaImF	sloužit
jako	jako	k9	jako
katedrála	katedrála	k1gFnSc1	katedrála
biskupství	biskupství	k1gNnSc2	biskupství
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
Valdštejn	Valdštejn	k1gNnSc1	Valdštejn
v	v	k7c6	v
Jičíně	Jičín	k1gInSc6	Jičín
plánoval	plánovat	k5eAaImAgMnS	plánovat
<g/>
.	.	kIx.	.
</s>
<s>
Pozoruhodnou	pozoruhodný	k2eAgFnSc4d1	pozoruhodná
centrální	centrální	k2eAgFnSc4d1	centrální
stavbu	stavba	k1gFnSc4	stavba
postavili	postavit	k5eAaPmAgMnP	postavit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1627-1634	[number]	k4	1627-1634
italští	italský	k2eAgMnPc1d1	italský
umělci	umělec	k1gMnPc1	umělec
<g/>
,	,	kIx,	,
snad	snad	k9	snad
podle	podle	k7c2	podle
plánů	plán	k1gInPc2	plán
a	a	k8xC	a
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
A.	A.	kA	A.
Spezzy	Spezza	k1gFnSc2	Spezza
a	a	k8xC	a
N.	N.	kA	N.
Sebregondiho	Sebregondi	k1gMnSc2	Sebregondi
<g/>
.	.	kIx.	.
</s>
<s>
Pravidelný	pravidelný	k2eAgInSc1d1	pravidelný
půdorys	půdorys	k1gInSc1	půdorys
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
řeckého	řecký	k2eAgInSc2d1	řecký
kříže	kříž	k1gInSc2	kříž
doplňují	doplňovat	k5eAaImIp3nP	doplňovat
v	v	k7c6	v
rozích	roh	k1gInPc6	roh
čtyři	čtyři	k4xCgFnPc4	čtyři
mohutné	mohutný	k2eAgFnPc4d1	mohutná
nízké	nízký	k2eAgFnPc4d1	nízká
věže	věž	k1gFnPc4	věž
<g/>
,	,	kIx,	,
půlkruhový	půlkruhový	k2eAgInSc1d1	půlkruhový
chór	chór	k1gInSc1	chór
je	být	k5eAaImIp3nS	být
obrácen	obrátit	k5eAaPmNgInS	obrátit
k	k	k7c3	k
jihu	jih	k1gInSc3	jih
<g/>
.	.	kIx.	.
</s>
<s>
Střední	střední	k2eAgInSc1d1	střední
prostor	prostor	k1gInSc1	prostor
je	být	k5eAaImIp3nS	být
zaklenut	zaklenout	k5eAaPmNgInS	zaklenout
plochou	plocha	k1gFnSc7	plocha
kupolí	kupole	k1gFnPc2	kupole
s	s	k7c7	s
iluzivní	iluzivní	k2eAgFnSc7d1	iluzivní
freskou	freska	k1gFnSc7	freska
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
1770	[number]	k4	1770
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
presbytáři	presbytář	k1gInSc6	presbytář
jsou	být	k5eAaImIp3nP	být
dvě	dva	k4xCgNnPc4	dva
veliká	veliký	k2eAgNnPc4d1	veliké
obdélná	obdélný	k2eAgNnPc4d1	obdélné
okna	okno	k1gNnPc4	okno
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgNnSc1d1	vnitřní
zařízení	zařízení	k1gNnSc1	zařízení
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
po	po	k7c6	po
požáru	požár	k1gInSc6	požár
roku	rok	k1gInSc2	rok
1768	[number]	k4	1768
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgNnSc1d1	hlavní
průčelí	průčelí	k1gNnSc1	průčelí
dokončeno	dokončit	k5eAaPmNgNnS	dokončit
v	v	k7c6	v
novoklasicistním	novoklasicistní	k2eAgInSc6d1	novoklasicistní
slohu	sloh	k1gInSc6	sloh
až	až	k9	až
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1865	[number]	k4	1865
<g/>
-	-	kIx~	-
<g/>
1867	[number]	k4	1867
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
kostelem	kostel	k1gInSc7	kostel
je	být	k5eAaImIp3nS	být
patrová	patrový	k2eAgFnSc1d1	patrová
barokní	barokní	k2eAgFnSc1d1	barokní
budova	budova	k1gFnSc1	budova
arciděkanství	arciděkanství	k1gNnSc2	arciděkanství
s	s	k7c7	s
mansardovou	mansardový	k2eAgFnSc7d1	mansardová
střechou	střecha	k1gFnSc7	střecha
a	a	k8xC	a
jednoduchou	jednoduchý	k2eAgFnSc7d1	jednoduchá
výzdobou	výzdoba	k1gFnSc7	výzdoba
<g/>
.	.	kIx.	.
</s>
<s>
Farní	farní	k2eAgInSc1d1	farní
kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Ignáce	Ignác	k1gMnSc2	Ignác
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Svobody	svoboda	k1gFnSc2	svoboda
(	(	kIx(	(
<g/>
nedaleko	nedaleko	k7c2	nedaleko
jihozápadního	jihozápadní	k2eAgInSc2d1	jihozápadní
rohu	roh	k1gInSc2	roh
Valdštějnova	Valdštějnův	k2eAgNnSc2d1	Valdštějnův
náměstí	náměstí	k1gNnSc2	náměstí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
založen	založen	k2eAgInSc4d1	založen
počátkem	počátkem	k7c2	počátkem
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
přestavěn	přestavěn	k2eAgInSc1d1	přestavěn
na	na	k7c4	na
síňové	síňový	k2eAgNnSc4d1	síňové
trojlodí	trojlodí	k1gNnSc4	trojlodí
a	a	k8xC	a
po	po	k7c6	po
požáru	požár	k1gInSc6	požár
zaklenut	zaklenut	k2eAgInSc1d1	zaklenut
roku	rok	k1gInSc2	rok
1597	[number]	k4	1597
plochou	plocha	k1gFnSc7	plocha
renesanční	renesanční	k2eAgFnSc7d1	renesanční
klenbou	klenba	k1gFnSc7	klenba
na	na	k7c4	na
šest	šest	k4xCc4	šest
toskánských	toskánský	k2eAgInPc2d1	toskánský
sloupů	sloup	k1gInPc2	sloup
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
loď	loď	k1gFnSc1	loď
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
3	[number]	k4	3
x	x	k?	x
4	[number]	k4	4
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Presbytář	presbytář	k1gInSc1	presbytář
gotický	gotický	k2eAgInSc1d1	gotický
s	s	k7c7	s
vnějšími	vnější	k2eAgInPc7d1	vnější
opěráky	opěrák	k1gInPc7	opěrák
má	mít	k5eAaImIp3nS	mít
gotickou	gotický	k2eAgFnSc4d1	gotická
hvězdicovou	hvězdicový	k2eAgFnSc4d1	hvězdicová
klenbu	klenba	k1gFnSc4	klenba
<g/>
,	,	kIx,	,
okna	okno	k1gNnSc2	okno
hrotitá	hrotitý	k2eAgFnSc1d1	hrotitá
s	s	k7c7	s
kružbami	kružba	k1gFnPc7	kružba
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1622	[number]	k4	1622
byl	být	k5eAaImAgMnS	být
svěřen	svěřit	k5eAaPmNgMnS	svěřit
jezuitům	jezuita	k1gMnPc3	jezuita
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1773	[number]	k4	1773
opět	opět	k6eAd1	opět
farní	farní	k2eAgInSc1d1	farní
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
kostelu	kostel	k1gInSc3	kostel
přiléhá	přiléhat	k5eAaImIp3nS	přiléhat
bývalá	bývalý	k2eAgFnSc1d1	bývalá
jezuitská	jezuitský	k2eAgFnSc1d1	jezuitská
kolej	kolej	k1gFnSc1	kolej
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1628	[number]	k4	1628
<g/>
-	-	kIx~	-
<g/>
1632	[number]	k4	1632
<g/>
,	,	kIx,	,
mnohokrát	mnohokrát	k6eAd1	mnohokrát
přestavovaná	přestavovaný	k2eAgFnSc1d1	přestavovaná
a	a	k8xC	a
rozšiřovaná	rozšiřovaný	k2eAgFnSc1d1	rozšiřovaná
<g/>
.	.	kIx.	.
</s>
<s>
Čtyřkřídlá	čtyřkřídlý	k2eAgFnSc1d1	čtyřkřídlá
dvoupatrová	dvoupatrový	k2eAgFnSc1d1	dvoupatrová
budova	budova	k1gFnSc1	budova
s	s	k7c7	s
kaplí	kaple	k1gFnSc7	kaple
a	a	k8xC	a
původně	původně	k6eAd1	původně
arkádami	arkáda	k1gFnPc7	arkáda
v	v	k7c6	v
přízemí	přízemí	k1gNnSc6	přízemí
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
zrušení	zrušení	k1gNnSc2	zrušení
jezuitského	jezuitský	k2eAgInSc2d1	jezuitský
řádu	řád	k1gInSc2	řád
1773	[number]	k4	1773
sloužila	sloužit	k5eAaImAgFnS	sloužit
jako	jako	k9	jako
kasárna	kasárna	k1gNnPc4	kasárna
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
koleji	kolej	k1gFnSc3	kolej
přiléhají	přiléhat	k5eAaImIp3nP	přiléhat
budovy	budova	k1gFnPc1	budova
bývalého	bývalý	k2eAgInSc2d1	bývalý
semináře	seminář	k1gInSc2	seminář
<g/>
,	,	kIx,	,
gymnázia	gymnázium	k1gNnSc2	gymnázium
a	a	k8xC	a
nemocnice	nemocnice	k1gFnSc2	nemocnice
<g/>
.	.	kIx.	.
</s>
<s>
Bývalá	bývalý	k2eAgFnSc1d1	bývalá
synagoga	synagoga	k1gFnSc1	synagoga
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Na	na	k7c6	na
příkopech	příkop	k1gInPc6	příkop
(	(	kIx(	(
<g/>
severovýchodně	severovýchodně	k6eAd1	severovýchodně
od	od	k7c2	od
Valdštejnova	Valdštejnův	k2eAgNnSc2d1	Valdštejnovo
náměstí	náměstí	k1gNnSc2	náměstí
<g/>
)	)	kIx)	)
a	a	k8xC	a
židovský	židovský	k2eAgInSc4d1	židovský
hřbitov	hřbitov	k1gInSc4	hřbitov
u	u	k7c2	u
Valdické	valdický	k2eAgFnSc2d1	Valdická
obory	obora	k1gFnSc2	obora
<g/>
.	.	kIx.	.
</s>
<s>
Vodárna	vodárna	k1gFnSc1	vodárna
<g/>
,	,	kIx,	,
hranolová	hranolový	k2eAgFnSc1d1	hranolová
vodárenská	vodárenský	k2eAgFnSc1d1	vodárenská
věž	věž	k1gFnSc1	věž
u	u	k7c2	u
rybníka	rybník	k1gInSc2	rybník
Kníže	kníže	k1gMnSc1	kníže
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1631	[number]	k4	1631
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
využita	využít	k5eAaPmNgFnS	využít
pro	pro	k7c4	pro
transformátor	transformátor	k1gInSc4	transformátor
V	v	k7c6	v
Koněvově	Koněvův	k2eAgFnSc6d1	Koněvova
ulici	ulice	k1gFnSc6	ulice
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
města	město	k1gNnSc2	město
stojí	stát	k5eAaImIp3nS	stát
vila	vila	k1gFnSc1	vila
Dóry	Dór	k1gMnPc7	Dór
Němcové	Němcové	k2eAgMnPc7d1	Němcové
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1910	[number]	k4	1910
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
D.	D.	kA	D.
Jurkoviče	Jurkovič	k1gMnSc2	Jurkovič
a	a	k8xC	a
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgFnPc2d1	další
moderních	moderní	k2eAgFnPc2d1	moderní
vil	vila	k1gFnPc2	vila
<g/>
.	.	kIx.	.
</s>
<s>
Lipová	lipový	k2eAgFnSc1d1	Lipová
alej	alej	k1gFnSc1	alej
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
vysázená	vysázený	k2eAgFnSc1d1	vysázená
na	na	k7c4	na
příkaz	příkaz	k1gInSc4	příkaz
Albrechta	Albrecht	k1gMnSc2	Albrecht
z	z	k7c2	z
Valdštejna	Valdštejn	k1gInSc2	Valdštejn
<g/>
,	,	kIx,	,
končí	končit	k5eAaImIp3nS	končit
Valdštejnskou	valdštejnský	k2eAgFnSc7d1	Valdštejnská
lodžií	lodžie	k1gFnSc7	lodžie
od	od	k7c2	od
A.	A.	kA	A.
Spezzy	Spezza	k1gFnSc2	Spezza
a	a	k8xC	a
parkem	park	k1gInSc7	park
Libosad	libosad	k1gInSc1	libosad
<g/>
.	.	kIx.	.
</s>
<s>
Návrší	návrší	k1gNnSc1	návrší
Čeřovka	Čeřovka	k1gFnSc1	Čeřovka
s	s	k7c7	s
novogotickou	novogotický	k2eAgFnSc7d1	novogotická
rozhlednou	rozhledný	k2eAgFnSc7d1	rozhledná
Milohlídkou	Milohlídka	k1gFnSc7	Milohlídka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1844	[number]	k4	1844
Obchodní	obchodní	k2eAgFnSc2d1	obchodní
akademie	akademie	k1gFnSc2	akademie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1928	[number]	k4	1928
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
jičínského	jičínský	k2eAgMnSc2d1	jičínský
stavitele	stavitel	k1gMnSc2	stavitel
Č.	Č.	kA	Č.
Musila	Musil	k1gMnSc2	Musil
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
projektoval	projektovat	k5eAaBmAgMnS	projektovat
také	také	k9	také
nemocnici	nemocnice	k1gFnSc4	nemocnice
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
školy	škola	k1gFnPc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Lepařovo	Lepařův	k2eAgNnSc1d1	Lepařovo
gymnázium	gymnázium	k1gNnSc1	gymnázium
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1883	[number]	k4	1883
Na	na	k7c6	na
Novám	nova	k1gFnPc3	nova
Městě	město	k1gNnSc6	město
za	za	k7c7	za
řekou	řeka	k1gFnSc7	řeka
špitální	špitální	k2eAgFnSc1d1	špitální
a	a	k8xC	a
hřbitovní	hřbitovní	k2eAgInSc1d1	hřbitovní
kostel	kostel	k1gInSc1	kostel
P.	P.	kA	P.
Marie	Maria	k1gFnSc2	Maria
de	de	k?	de
Sale	Sal	k1gFnSc2	Sal
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
Nejsvětější	nejsvětější	k2eAgFnSc1d1	nejsvětější
Trojice	trojice	k1gFnSc1	trojice
<g/>
,	,	kIx,	,
založený	založený	k2eAgInSc1d1	založený
1628	[number]	k4	1628
a	a	k8xC	a
dokončený	dokončený	k2eAgInSc4d1	dokončený
až	až	k6eAd1	až
1663	[number]	k4	1663
<g/>
.	.	kIx.	.
</s>
<s>
Jednolodní	jednolodní	k2eAgFnSc1d1	jednolodní
obdélná	obdélný	k2eAgFnSc1d1	obdélná
stavba	stavba	k1gFnSc1	stavba
s	s	k7c7	s
půlkruhovým	půlkruhový	k2eAgInSc7d1	půlkruhový
presbytářem	presbytář	k1gInSc7	presbytář
a	a	k8xC	a
kaplemi	kaple	k1gFnPc7	kaple
a	a	k8xC	a
hranolovou	hranolový	k2eAgFnSc7d1	hranolová
věží	věž	k1gFnSc7	věž
v	v	k7c6	v
průčelí	průčelí	k1gNnSc6	průčelí
<g/>
.	.	kIx.	.
</s>
<s>
Cenné	cenný	k2eAgNnSc1d1	cenné
barokní	barokní	k2eAgNnSc1d1	barokní
zařízení	zařízení	k1gNnSc1	zařízení
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1730	[number]	k4	1730
<g/>
.	.	kIx.	.
</s>
<s>
Sochy	socha	k1gFnPc1	socha
a	a	k8xC	a
pomníky	pomník	k1gInPc1	pomník
<g/>
:	:	kIx,	:
Socha	socha	k1gFnSc1	socha
svatého	svatý	k2eAgMnSc2d1	svatý
Václava	Václav	k1gMnSc2	Václav
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1743	[number]	k4	1743
při	při	k7c6	při
Moravčické	Moravčický	k2eAgFnSc6d1	Moravčický
cestě	cesta	k1gFnSc6	cesta
Sloup	sloup	k1gInSc1	sloup
P.	P.	kA	P.
Marie	Maria	k1gFnSc2	Maria
Rušánské	Rušánský	k2eAgFnSc2d1	Rušánský
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1749	[number]	k4	1749
na	na	k7c6	na
Komenského	Komenského	k2eAgNnSc6d1	Komenského
náměstí	náměstí	k1gNnSc6	náměstí
Pomník	pomník	k1gInSc4	pomník
padlým	padlý	k2eAgFnPc3d1	padlá
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1867	[number]	k4	1867
na	na	k7c6	na
Letné	Letná	k1gFnSc6	Letná
Pomník	pomník	k1gInSc4	pomník
Mistra	mistr	k1gMnSc2	mistr
Jana	Jan	k1gMnSc2	Jan
Husa	Hus	k1gMnSc2	Hus
od	od	k7c2	od
A.	A.	kA	A.
Suchardy	Sucharda	k1gMnSc2	Sucharda
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1872	[number]	k4	1872
v	v	k7c6	v
Husově	Husův	k2eAgFnSc6d1	Husova
ulici	ulice	k1gFnSc6	ulice
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgFnSc1d3	nejstarší
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
Pomník	pomník	k1gInSc4	pomník
<g />
.	.	kIx.	.
</s>
<s>
Karla	Karel	k1gMnSc4	Karel
Havlíčka	Havlíček	k1gMnSc4	Havlíček
Borovského	Borovský	k1gMnSc4	Borovský
od	od	k7c2	od
J.	J.	kA	J.
Říhy	Říha	k1gMnSc2	Říha
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1906	[number]	k4	1906
Hvězdárna	hvězdárna	k1gFnSc1	hvězdárna
v	v	k7c6	v
Raisově	Raisův	k2eAgFnSc6d1	Raisova
ulici	ulice	k1gFnSc6	ulice
na	na	k7c6	na
západním	západní	k2eAgInSc6d1	západní
okraji	okraj	k1gInSc6	okraj
města	město	k1gNnSc2	město
Zebín	Zebína	k1gFnPc2	Zebína
(	(	kIx(	(
<g/>
399	[number]	k4	399
m.	m.	k?	m.
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
nad	nad	k7c7	nad
Valdicemi	Valdice	k1gFnPc7	Valdice
<g/>
,	,	kIx,	,
severovýchodně	severovýchodně	k6eAd1	severovýchodně
od	od	k7c2	od
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
s	s	k7c7	s
kapličkou	kaplička	k1gFnSc7	kaplička
Maří	mařit	k5eAaImIp3nS	mařit
Magdalény	Magdaléna	k1gFnPc4	Magdaléna
Valdická	valdický	k2eAgFnSc1d1	Valdická
kartouza	kartouza	k1gFnSc1	kartouza
někdejší	někdejší	k2eAgFnSc1d1	někdejší
klášter	klášter	k1gInSc4	klášter
řádu	řád	k1gInSc2	řád
kartuziánů	kartuzián	k1gMnPc2	kartuzián
ve	v	k7c6	v
Valdicích	Valdice	k1gFnPc6	Valdice
založený	založený	k2eAgInSc4d1	založený
Albrechtem	Albrecht	k1gMnSc7	Albrecht
z	z	k7c2	z
<g />
.	.	kIx.	.
</s>
<s>
Valdštejna	Valdštejna	k6eAd1	Valdštejna
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
nápravné	nápravný	k2eAgNnSc4d1	nápravné
zařízení	zařízení	k1gNnSc4	zařízení
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejstřeženějších	střežený	k2eAgFnPc2d3	nejstřeženější
věznic	věznice	k1gFnPc2	věznice
Česka	Česko	k1gNnSc2	Česko
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
tvořeno	tvořit	k5eAaImNgNnS	tvořit
11	[number]	k4	11
místními	místní	k2eAgFnPc7d1	místní
částmi	část	k1gFnPc7	část
na	na	k7c6	na
4	[number]	k4	4
katastrálních	katastrální	k2eAgNnPc6d1	katastrální
územích	území	k1gNnPc6	území
Jičín	Jičín	k1gInSc4	Jičín
-	-	kIx~	-
části	část	k1gFnSc2	část
Holínské	Holínský	k2eAgNnSc1d1	Holínský
Předměstí	předměstí	k1gNnSc1	předměstí
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
Pražské	pražský	k2eAgNnSc1d1	Pražské
Předměstí	předměstí	k1gNnSc1	předměstí
<g/>
,	,	kIx,	,
Sedličky	Sedlička	k1gFnPc1	Sedlička
<g/>
,	,	kIx,	,
Soudná	soudný	k2eAgNnPc1d1	soudné
<g/>
,	,	kIx,	,
Staré	Staré	k2eAgNnSc1d1	Staré
Město	město	k1gNnSc1	město
a	a	k8xC	a
Valdické	valdický	k2eAgNnSc1d1	Valdické
Předměstí	předměstí	k1gNnSc1	předměstí
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
část	část	k1gFnSc1	část
<g/>
)	)	kIx)	)
Moravčice	Moravčice	k1gFnSc1	Moravčice
-	-	kIx~	-
části	část	k1gFnPc1	část
Moravčice	Moravčice	k1gFnSc2	Moravčice
a	a	k8xC	a
Valdické	valdický	k2eAgNnSc1d1	Valdické
Předměstí	předměstí	k1gNnSc1	předměstí
(	(	kIx(	(
<g/>
část	část	k1gFnSc1	část
<g/>
)	)	kIx)	)
Popovice	Popovice	k1gFnPc1	Popovice
u	u	k7c2	u
Jičína	Jičín	k1gInSc2	Jičín
-	-	kIx~	-
část	část	k1gFnSc1	část
Popovice	Popovice	k1gFnPc1	Popovice
Robousy	Robous	k1gInPc1	Robous
-	-	kIx~	-
části	část	k1gFnPc1	část
Dvorce	dvorec	k1gInPc1	dvorec
a	a	k8xC	a
Robousy	Robous	k1gInPc1	Robous
Čeřovka	Čeřovka	k1gFnSc1	Čeřovka
<g/>
,	,	kIx,	,
Dvorce	dvorec	k1gInPc1	dvorec
<g/>
,	,	kIx,	,
Holínské	Holínský	k2eAgNnSc1d1	Holínský
Předměstí	předměstí	k1gNnSc1	předměstí
<g/>
,	,	kIx,	,
Hradecké	Hradecké	k2eAgNnSc1d1	Hradecké
Předměstí	předměstí	k1gNnSc1	předměstí
<g/>
,	,	kIx,	,
Jarošov	Jarošov	k1gInSc1	Jarošov
<g/>
,	,	kIx,	,
Jičín-Staré	Jičín-Starý	k2eAgNnSc1d1	Jičín-Starý
Město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
Moravčice	Moravčice	k1gFnSc1	Moravčice
<g/>
,	,	kIx,	,
Na	na	k7c6	na
vrchách	vrch	k1gInPc6	vrch
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
Pod	pod	k7c7	pod
Šibeňákem	Šibeňákem	k?	Šibeňákem
<g/>
,	,	kIx,	,
Pod	pod	k7c7	pod
zámkem	zámek	k1gInSc7	zámek
<g/>
,	,	kIx,	,
Popovice	Popovice	k1gFnPc1	Popovice
<g/>
,	,	kIx,	,
Porák	Porák	k1gInSc1	Porák
<g/>
,	,	kIx,	,
Pražské	pražský	k2eAgNnSc1d1	Pražské
Předměstí	předměstí	k1gNnSc1	předměstí
<g/>
,	,	kIx,	,
Robousy	Robous	k1gInPc1	Robous
Související	související	k2eAgFnSc2d1	související
informace	informace	k1gFnSc2	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Okres	okres	k1gInSc1	okres
Jičín	Jičín	k1gInSc1	Jičín
a	a	k8xC	a
Obvod	obvod	k1gInSc1	obvod
obce	obec	k1gFnSc2	obec
s	s	k7c7	s
rozšířenou	rozšířený	k2eAgFnSc7d1	rozšířená
působností	působnost	k1gFnSc7	působnost
Jičín	Jičín	k1gInSc1	Jičín
<g/>
.	.	kIx.	.
</s>
<s>
Jičín	Jičín	k1gInSc1	Jičín
byl	být	k5eAaImAgInS	být
dříve	dříve	k6eAd2	dříve
okresním	okresní	k2eAgNnSc7d1	okresní
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
obcí	obec	k1gFnSc7	obec
s	s	k7c7	s
rozšířenou	rozšířený	k2eAgFnSc7d1	rozšířená
působností	působnost	k1gFnSc7	působnost
a	a	k8xC	a
pověřeným	pověřený	k2eAgInSc7d1	pověřený
obecním	obecní	k2eAgInSc7d1	obecní
úřadem	úřad	k1gInSc7	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Okres	okres	k1gInSc1	okres
Jičín	Jičín	k1gInSc1	Jičín
ale	ale	k8xC	ale
stále	stále	k6eAd1	stále
existuje	existovat	k5eAaImIp3nS	existovat
a	a	k8xC	a
skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
ze	z	k7c2	z
111	[number]	k4	111
obcí	obec	k1gFnPc2	obec
<g/>
,	,	kIx,	,
ORP	ORP	kA	ORP
ze	z	k7c2	z
77	[number]	k4	77
obcí	obec	k1gFnPc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
Jakub	Jakub	k1gMnSc1	Jakub
Baševi	Bašeev	k1gFnSc6	Bašeev
z	z	k7c2	z
Treuenburka	Treuenburek	k1gMnSc2	Treuenburek
(	(	kIx(	(
<g/>
1580	[number]	k4	1580
<g/>
-	-	kIx~	-
<g/>
1634	[number]	k4	1634
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
náboženský	náboženský	k2eAgMnSc1d1	náboženský
hodnostář	hodnostář	k1gMnSc1	hodnostář
židovské	židovský	k2eAgFnSc2d1	židovská
obce	obec	k1gFnSc2	obec
Albrecht	Albrecht	k1gMnSc1	Albrecht
z	z	k7c2	z
Valdštejna	Valdštejn	k1gInSc2	Valdštejn
(	(	kIx(	(
<g/>
1583	[number]	k4	1583
<g/>
-	-	kIx~	-
<g/>
1634	[number]	k4	1634
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
velitel	velitel	k1gMnSc1	velitel
císařsko-ligistických	císařskoigistický	k2eAgNnPc2d1	císařsko-ligistický
vojsk	vojsko	k1gNnPc2	vojsko
v	v	k7c6	v
třicetileté	třicetiletý	k2eAgFnSc6d1	třicetiletá
válce	válka	k1gFnSc6	válka
(	(	kIx(	(
<g/>
generalissimus	generalissimus	k1gMnSc1	generalissimus
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Jan	Jan	k1gMnSc1	Jan
Barner	Barner	k1gMnSc1	Barner
(	(	kIx(	(
<g/>
1643	[number]	k4	1643
<g/>
-	-	kIx~	-
<g/>
1708	[number]	k4	1708
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
římskokatolický	římskokatolický	k2eAgMnSc1d1	římskokatolický
kněz-jezuita	knězezuita	k1gMnSc1	kněz-jezuita
<g/>
,	,	kIx,	,
filolog	filolog	k1gMnSc1	filolog
<g/>
,	,	kIx,	,
kazatel	kazatel	k1gMnSc1	kazatel
a	a	k8xC	a
překladatel	překladatel	k1gMnSc1	překladatel
Bible	bible	k1gFnSc2	bible
svatováclavské	svatováclavský	k2eAgFnSc2d1	Svatováclavská
František	František	k1gMnSc1	František
Šír	Šír	k1gMnSc1	Šír
(	(	kIx(	(
<g/>
1796	[number]	k4	1796
<g/>
-	-	kIx~	-
<g/>
1867	[number]	k4	1867
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dlouholetý	dlouholetý	k2eAgMnSc1d1	dlouholetý
gymnazijní	gymnazijní	k2eAgMnSc1d1	gymnazijní
profesor	profesor	k1gMnSc1	profesor
a	a	k8xC	a
národní	národní	k2eAgMnSc1d1	národní
buditel	buditel	k1gMnSc1	buditel
na	na	k7c6	na
Jičínsku	Jičínsko	k1gNnSc6	Jičínsko
František	František	k1gMnSc1	František
Cyril	Cyril	k1gMnSc1	Cyril
Kampelík	Kampelík	k1gMnSc1	Kampelík
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
1805	[number]	k4	1805
<g/>
-	-	kIx~	-
<g/>
1872	[number]	k4	1872
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
družstevních	družstevní	k2eAgFnPc2d1	družstevní
záložen	záložna	k1gFnPc2	záložna
Josef	Josef	k1gMnSc1	Josef
Walter	Walter	k1gMnSc1	Walter
(	(	kIx(	(
<g/>
1815	[number]	k4	1815
<g/>
-	-	kIx~	-
<g/>
1890	[number]	k4	1890
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
pedagog	pedagog	k1gMnSc1	pedagog
<g/>
,	,	kIx,	,
ředitel	ředitel	k1gMnSc1	ředitel
učitelského	učitelský	k2eAgInSc2d1	učitelský
ústavu	ústav	k1gInSc2	ústav
v	v	k7c6	v
Jičíně	Jičín	k1gInSc6	Jičín
(	(	kIx(	(
<g/>
1871	[number]	k4	1871
<g/>
-	-	kIx~	-
<g/>
75	[number]	k4	75
<g/>
)	)	kIx)	)
Vladislav	Vladislav	k1gMnSc1	Vladislav
Šír	Šír	k1gMnSc1	Šír
(	(	kIx(	(
<g/>
1830	[number]	k4	1830
<g />
.	.	kIx.	.
</s>
<s>
<g/>
-	-	kIx~	-
<g/>
1889	[number]	k4	1889
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
přírodovědec	přírodovědec	k1gMnSc1	přírodovědec
<g/>
,	,	kIx,	,
organizátor	organizátor	k1gMnSc1	organizátor
spolků	spolek	k1gInPc2	spolek
Jan	Jan	k1gMnSc1	Jan
Gebauer	Gebauer	k1gMnSc1	Gebauer
(	(	kIx(	(
<g/>
1838	[number]	k4	1838
<g/>
-	-	kIx~	-
<g/>
1907	[number]	k4	1907
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
bohemista	bohemista	k1gMnSc1	bohemista
Marianna	Marianno	k1gNnSc2	Marianno
Pečírková	Pečírková	k1gFnSc1	Pečírková
(	(	kIx(	(
<g/>
1838	[number]	k4	1838
<g/>
-	-	kIx~	-
<g/>
1904	[number]	k4	1904
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vydavatelka	vydavatelka	k1gFnSc1	vydavatelka
Pečírkova	Pečírkův	k2eAgInSc2d1	Pečírkův
Národního	národní	k2eAgInSc2d1	národní
kalendáře	kalendář	k1gInSc2	kalendář
František	František	k1gMnSc1	František
Klouček	Klouček	k1gMnSc1	Klouček
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
1849	[number]	k4	1849
<g/>
-	-	kIx~	-
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
právník	právník	k1gMnSc1	právník
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
poslanec	poslanec	k1gMnSc1	poslanec
zemského	zemský	k2eAgInSc2d1	zemský
sněmu	sněm	k1gInSc2	sněm
Kajetán	Kajetán	k1gMnSc1	Kajetán
Tichý	Tichý	k1gMnSc1	Tichý
(	(	kIx(	(
<g/>
1859	[number]	k4	1859
<g/>
-	-	kIx~	-
<g/>
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
kantor	kantor	k1gMnSc1	kantor
a	a	k8xC	a
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
Josef	Josef	k1gMnSc1	Josef
Štefan	Štefan	k1gMnSc1	Štefan
Kubín	Kubín	k1gMnSc1	Kubín
(	(	kIx(	(
<g/>
1864	[number]	k4	1864
<g/>
-	-	kIx~	-
<g/>
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
folklorista	folklorista	k1gMnSc1	folklorista
František	František	k1gMnSc1	František
Kaván	Kaván	k1gMnSc1	Kaván
(	(	kIx(	(
<g/>
1866	[number]	k4	1866
<g/>
-	-	kIx~	-
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
malíř-krajinář	malířrajinář	k1gMnSc1	malíř-krajinář
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
překladatel	překladatel	k1gMnSc1	překladatel
z	z	k7c2	z
ruštiny	ruština	k1gFnSc2	ruština
Ema	Ema	k1gFnSc1	Ema
Pechová	Pechová	k1gFnSc1	Pechová
(	(	kIx(	(
<g/>
1869	[number]	k4	1869
<g/>
-	-	kIx~	-
<g/>
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
herce	herec	k1gMnSc2	herec
Ladislava	Ladislav	k1gMnSc2	Ladislav
Peška	Peška	k1gMnSc1	Peška
Karl	Karl	k1gMnSc1	Karl
<g />
.	.	kIx.	.
</s>
<s>
Kraus	Kraus	k1gMnSc1	Kraus
(	(	kIx(	(
<g/>
1874	[number]	k4	1874
<g/>
-	-	kIx~	-
<g/>
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rakouský	rakouský	k2eAgMnSc1d1	rakouský
spisovatel	spisovatel	k1gMnSc1	spisovatel
Robert	Robert	k1gMnSc1	Robert
Winterstein	Winterstein	k1gMnSc1	Winterstein
(	(	kIx(	(
<g/>
1874	[number]	k4	1874
<g/>
-	-	kIx~	-
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rakouský	rakouský	k2eAgMnSc1d1	rakouský
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
ministr	ministr	k1gMnSc1	ministr
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
Josef	Josef	k1gMnSc1	Josef
Gočár	Gočár	k1gMnSc1	Gočár
(	(	kIx(	(
<g/>
1880	[number]	k4	1880
<g/>
-	-	kIx~	-
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
architekt	architekt	k1gMnSc1	architekt
Vlastislav	Vlastislav	k1gMnSc1	Vlastislav
Hofman	Hofman	k1gMnSc1	Hofman
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
1884	[number]	k4	1884
<g/>
-	-	kIx~	-
<g/>
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
architekt	architekt	k1gMnSc1	architekt
<g/>
,	,	kIx,	,
urbanista	urbanista	k1gMnSc1	urbanista
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
grafik	grafik	k1gMnSc1	grafik
Josef	Josef	k1gMnSc1	Josef
Váchal	Váchal	k1gMnSc1	Váchal
(	(	kIx(	(
<g/>
1884	[number]	k4	1884
<g/>
-	-	kIx~	-
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
grafik	grafik	k1gMnSc1	grafik
<g/>
,	,	kIx,	,
ilustrátor	ilustrátor	k1gMnSc1	ilustrátor
<g/>
,	,	kIx,	,
sochař	sochař	k1gMnSc1	sochař
<g/>
,	,	kIx,	,
řezbář	řezbář	k1gMnSc1	řezbář
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
básník	básník	k1gMnSc1	básník
Ernst	Ernst	k1gMnSc1	Ernst
Polak	Polak	k1gMnSc1	Polak
(	(	kIx(	(
<g/>
1886	[number]	k4	1886
<g/>
-	-	kIx~	-
<g/>
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rakouský	rakouský	k2eAgMnSc1d1	rakouský
literární	literární	k2eAgMnSc1d1	literární
kritik	kritik	k1gMnSc1	kritik
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Novotný	Novotný	k1gMnSc1	Novotný
(	(	kIx(	(
<g/>
1886	[number]	k4	1886
<g/>
-	-	kIx~	-
<g/>
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
Jan	Jan	k1gMnSc1	Jan
Morávek	Morávek	k1gMnSc1	Morávek
(	(	kIx(	(
<g/>
1887	[number]	k4	1887
<g/>
-	-	kIx~	-
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
historik	historik	k1gMnSc1	historik
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
archivář	archivář	k1gMnSc1	archivář
Karel	Karel	k1gMnSc1	Karel
Kazbunda	Kazbunda	k1gFnSc1	Kazbunda
(	(	kIx(	(
<g/>
1888	[number]	k4	1888
<g/>
-	-	kIx~	-
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
historik	historik	k1gMnSc1	historik
a	a	k8xC	a
archivář	archivář	k1gMnSc1	archivář
Čeněk	Čeněk	k1gMnSc1	Čeněk
Musil	Musil	k1gMnSc1	Musil
(	(	kIx(	(
<g/>
1889	[number]	k4	1889
<g/>
-	-	kIx~	-
<g/>
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
architekt	architekt	k1gMnSc1	architekt
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
významně	významně	k6eAd1	významně
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
vzhled	vzhled	k1gInSc1	vzhled
města	město	k1gNnSc2	město
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Lisý	Lisý	k1gMnSc1	Lisý
(	(	kIx(	(
<g/>
1895	[number]	k4	1895
<g/>
-	-	kIx~	-
<g/>
1943	[number]	k4	1943
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
legionář	legionář	k1gMnSc1	legionář
a	a	k8xC	a
generál	generál	k1gMnSc1	generál
<g/>
,	,	kIx,	,
náčelník	náčelník	k1gMnSc1	náčelník
štábu	štáb	k1gInSc2	štáb
a	a	k8xC	a
velitel	velitel	k1gMnSc1	velitel
odbojové	odbojový	k2eAgFnSc2d1	odbojová
organizace	organizace	k1gFnSc2	organizace
Obrana	obrana	k1gFnSc1	obrana
Národa	národ	k1gInSc2	národ
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Seifert	Seifert	k1gMnSc1	Seifert
(	(	kIx(	(
<g/>
1901	[number]	k4	1901
<g/>
-	-	kIx~	-
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
novinář	novinář	k1gMnSc1	novinář
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Tomáš	Tomáš	k1gMnSc1	Tomáš
(	(	kIx(	(
<g/>
1915	[number]	k4	1915
<g />
.	.	kIx.	.
</s>
<s>
<g/>
-	-	kIx~	-
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
dirigent	dirigent	k1gMnSc1	dirigent
a	a	k8xC	a
sbormistr	sbormistr	k1gMnSc1	sbormistr
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Řehoř	Řehoř	k1gMnSc1	Řehoř
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
-	-	kIx~	-
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
herec	herec	k1gMnSc1	herec
Josef	Josef	k1gMnSc1	Josef
Smolík	Smolík	k1gMnSc1	Smolík
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
-	-	kIx~	-
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
evangelický	evangelický	k2eAgMnSc1d1	evangelický
teolog	teolog	k1gMnSc1	teolog
<g/>
,	,	kIx,	,
duchovní	duchovní	k1gMnSc1	duchovní
ČCE	ČCE	kA	ČCE
<g/>
,	,	kIx,	,
vysokoškolský	vysokoškolský	k2eAgMnSc1d1	vysokoškolský
pedagog	pedagog	k1gMnSc1	pedagog
<g />
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Mošna	mošna	k1gFnSc1	mošna
(	(	kIx(	(
<g/>
1928	[number]	k4	1928
<g/>
-	-	kIx~	-
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
římskokatolický	římskokatolický	k2eAgMnSc1d1	římskokatolický
kněz	kněz	k1gMnSc1	kněz
a	a	k8xC	a
osobní	osobní	k2eAgMnSc1d1	osobní
arciděkan	arciděkan	k1gMnSc1	arciděkan
Rainer	Rainer	k1gMnSc1	Rainer
Schandry	Schandra	k1gFnSc2	Schandra
(	(	kIx(	(
<g/>
*	*	kIx~	*
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
psycholog	psycholog	k1gMnSc1	psycholog
Michal	Michal	k1gMnSc1	Michal
Suchánek	Suchánek	k1gMnSc1	Suchánek
(	(	kIx(	(
<g/>
*	*	kIx~	*
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
moderátor	moderátor	k1gMnSc1	moderátor
a	a	k8xC	a
bavič	bavič	k1gMnSc1	bavič
Michal	Michal	k1gMnSc1	Michal
Malátný	malátný	k2eAgMnSc1d1	malátný
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
*	*	kIx~	*
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
zpěvák	zpěvák	k1gMnSc1	zpěvák
skupiny	skupina	k1gFnSc2	skupina
Chinaski	Chinask	k1gFnSc2	Chinask
Pavel	Pavel	k1gMnSc1	Pavel
Grohman	Grohman	k1gMnSc1	Grohman
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
<g/>
-	-	kIx~	-
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
bubeník	bubeník	k1gMnSc1	bubeník
skupiny	skupina	k1gFnSc2	skupina
Chinaski	Chinask	k1gFnSc2	Chinask
Miluše	Miluše	k1gFnSc1	Miluše
Bittnerová	Bittnerová	k1gFnSc1	Bittnerová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
Jana	Jana	k1gFnSc1	Jana
Plodková	Plodkový	k2eAgFnSc1d1	Plodková
(	(	kIx(	(
<g/>
*	*	kIx~	*
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Soukup	Soukup	k1gMnSc1	Soukup
(	(	kIx(	(
<g/>
*	*	kIx~	*
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
biatlonista	biatlonista	k1gMnSc1	biatlonista
(	(	kIx(	(
<g/>
OH	OH	kA	OH
Soči	Soči	k1gNnSc1	Soči
2014	[number]	k4	2014
-	-	kIx~	-
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
místo	místo	k1gNnSc1	místo
<g/>
)	)	kIx)	)
Nový	nový	k2eAgInSc1d1	nový
Jičín	Jičín	k1gInSc1	Jičín
Starý	starý	k2eAgInSc1d1	starý
Jičín	Jičín	k1gInSc1	Jičín
Jičíněves	Jičíněvesa	k1gFnPc2	Jičíněvesa
Erbach	Erbach	k1gInSc1	Erbach
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
King	Kinga	k1gFnPc2	Kinga
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Lynn	Lynn	k1gInSc1	Lynn
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
Martin	Martin	k1gInSc1	Martin
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc1	Slovensko
Svídnice	Svídnice	k1gFnSc2	Svídnice
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
Wijk	Wijka	k1gFnPc2	Wijka
bij	bít	k5eAaImRp2nS	bít
Duurstede	Duursted	k1gMnSc5	Duursted
<g/>
,	,	kIx,	,
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
</s>
