<s>
Jeřáb	jeřáb	k1gMnSc1	jeřáb
milský	milský	k1gMnSc1	milský
(	(	kIx(	(
<g/>
Sorbus	Sorbus	k1gMnSc1	Sorbus
milensis	milensis	k1gFnSc2	milensis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
m	m	kA	m
vysoký	vysoký	k2eAgInSc1d1	vysoký
opadavý	opadavý	k2eAgInSc1d1	opadavý
strom	strom	k1gInSc1	strom
nebo	nebo	k8xC	nebo
keř	keř	k1gInSc1	keř
<g/>
,	,	kIx,	,
popsaný	popsaný	k2eAgInSc1d1	popsaný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
z	z	k7c2	z
vrchu	vrch	k1gInSc2	vrch
Milá	milá	k1gFnSc1	milá
v	v	k7c6	v
Českém	český	k2eAgNnSc6d1	české
středohoří	středohoří	k1gNnSc6	středohoří
<g/>
.	.	kIx.	.
</s>
<s>
Listy	lista	k1gFnPc1	lista
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
mají	mít	k5eAaImIp3nP	mít
mělce	mělce	k6eAd1	mělce
laločnatou	laločnatý	k2eAgFnSc4d1	laločnatá
na	na	k7c6	na
rubu	rub	k1gInSc6	rub
žlutošedě	žlutošedě	k6eAd1	žlutošedě
plstnatou	plstnatý	k2eAgFnSc4d1	plstnatá
čepel	čepel	k1gFnSc4	čepel
<g/>
.	.	kIx.	.
</s>
<s>
Květy	květ	k1gInPc1	květ
jsou	být	k5eAaImIp3nP	být
pětičetné	pětičetný	k2eAgInPc1d1	pětičetný
<g/>
,	,	kIx,	,
bělavé	bělavý	k2eAgInPc1d1	bělavý
<g/>
,	,	kIx,	,
s	s	k7c7	s
narůžovělými	narůžovělý	k2eAgInPc7d1	narůžovělý
prašníky	prašník	k1gInPc7	prašník
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
uspořádány	uspořádat	k5eAaPmNgInP	uspořádat
do	do	k7c2	do
mnohokvětých	mnohokvětý	k2eAgInPc2d1	mnohokvětý
chocholičnatých	chocholičnatý	k2eAgInPc2d1	chocholičnatý
lat.	lat.	k?	lat.
Zralé	zralý	k2eAgFnSc2d1	zralá
malvice	malvice	k1gFnSc2	malvice
jsou	být	k5eAaImIp3nP	být
lesklé	lesklý	k2eAgFnPc1d1	lesklá
a	a	k8xC	a
oranžově	oranžově	k6eAd1	oranžově
červené	červený	k2eAgFnPc1d1	červená
<g/>
.	.	kIx.	.
</s>
<s>
Druh	druh	k1gInSc1	druh
náleží	náležet	k5eAaImIp3nS	náležet
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
Sorbus	Sorbus	k1gMnSc1	Sorbus
latifolia	latifolium	k1gNnSc2	latifolium
agg	agg	k?	agg
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
sdružuje	sdružovat	k5eAaImIp3nS	sdružovat
taxony	taxon	k1gInPc1	taxon
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
hybridizací	hybridizace	k1gFnSc7	hybridizace
jeřábu	jeřáb	k1gInSc2	jeřáb
břeku	břek	k1gInSc2	břek
(	(	kIx(	(
<g/>
Sorbus	Sorbus	k1gInSc1	Sorbus
torminalis	torminalis	k1gFnSc2	torminalis
<g/>
)	)	kIx)	)
a	a	k8xC	a
některého	některý	k3yIgMnSc2	některý
ze	z	k7c2	z
zástupců	zástupce	k1gMnPc2	zástupce
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
jeřábu	jeřáb	k1gInSc2	jeřáb
muku	muk	k1gInSc2	muk
(	(	kIx(	(
<g/>
Sorbus	Sorbus	k1gInSc1	Sorbus
aria	ari	k1gInSc2	ari
s.	s.	k?	s.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Druh	druh	k1gInSc1	druh
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
roste	růst	k5eAaImIp3nS	růst
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
vrchu	vrch	k1gInSc6	vrch
Milá	milý	k2eAgFnSc1d1	Milá
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
nelze	lze	k6eNd1	lze
v	v	k7c6	v
budoucnosti	budoucnost	k1gFnSc6	budoucnost
vyloučit	vyloučit	k5eAaPmF	vyloučit
nový	nový	k2eAgInSc4d1	nový
nález	nález	k1gInSc4	nález
na	na	k7c6	na
některém	některý	k3yIgInSc6	některý
z	z	k7c2	z
přilehlých	přilehlý	k2eAgInPc2d1	přilehlý
kopců	kopec	k1gInPc2	kopec
Českého	český	k2eAgNnSc2d1	české
středohoří	středohoří	k1gNnSc2	středohoří
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
hlavně	hlavně	k9	hlavně
na	na	k7c6	na
strmých	strmý	k2eAgFnPc6d1	strmá
těžko	těžko	k6eAd1	těžko
přístupných	přístupný	k2eAgFnPc6d1	přístupná
čedičových	čedičový	k2eAgFnPc6d1	čedičová
skálách	skála	k1gFnPc6	skála
<g/>
,	,	kIx,	,
v	v	k7c6	v
lesnatých	lesnatý	k2eAgFnPc6d1	lesnatá
roklích	rokle	k1gFnPc6	rokle
a	a	k8xC	a
sutích	suť	k1gFnPc6	suť
<g/>
.	.	kIx.	.
</s>
<s>
Podrobným	podrobný	k2eAgNnSc7d1	podrobné
sčítáním	sčítání	k1gNnSc7	sčítání
bylo	být	k5eAaImAgNnS	být
nalezeno	nalezen	k2eAgNnSc1d1	Nalezeno
celkem	celkem	k6eAd1	celkem
57	[number]	k4	57
exemplářů	exemplář	k1gInPc2	exemplář
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
38	[number]	k4	38
dospělých	dospělý	k2eAgFnPc2d1	dospělá
a	a	k8xC	a
19	[number]	k4	19
juvenilních	juvenilní	k2eAgFnPc2d1	juvenilní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
známé	známý	k2eAgFnSc6d1	známá
pouze	pouze	k6eAd1	pouze
čtyři	čtyři	k4xCgInPc1	čtyři
příbuzné	příbuzný	k2eAgInPc1d1	příbuzný
druhy	druh	k1gInPc1	druh
<g/>
.	.	kIx.	.
</s>
<s>
Jeřábu	jeřáb	k1gMnSc3	jeřáb
milskému	milský	k1gMnSc3	milský
je	být	k5eAaImIp3nS	být
nejpodobnější	podobný	k2eAgMnSc1d3	nejpodobnější
jeřáb	jeřáb	k1gMnSc1	jeřáb
manětínský	manětínský	k2eAgMnSc1d1	manětínský
(	(	kIx(	(
<g/>
S.	S.	kA	S.
rhodanthera	rhodanthera	k1gFnSc1	rhodanthera
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
S.	S.	kA	S.
milensis	milensis	k1gInSc1	milensis
tmavěji	tmavě	k6eAd2	tmavě
růžové	růžový	k2eAgInPc4d1	růžový
prašníky	prašník	k1gInPc4	prašník
<g/>
,	,	kIx,	,
větší	veliký	k2eAgInPc4d2	veliký
plody	plod	k1gInPc4	plod
a	a	k8xC	a
hlouběji	hluboko	k6eAd2	hluboko
laločnaté	laločnatý	k2eAgInPc4d1	laločnatý
listy	list	k1gInPc4	list
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
známý	známý	k2eAgMnSc1d1	známý
z	z	k7c2	z
Chlumské	chlumský	k2eAgFnSc2d1	Chlumská
hory	hora	k1gFnSc2	hora
u	u	k7c2	u
Manětína	Manětín	k1gInSc2	Manětín
na	na	k7c6	na
Plzeňsku	Plzeňsko	k1gNnSc6	Plzeňsko
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
druhy	druh	k1gInPc1	druh
<g/>
,	,	kIx,	,
jeřáb	jeřáb	k1gMnSc1	jeřáb
krasový	krasový	k2eAgMnSc1d1	krasový
(	(	kIx(	(
<g/>
S.	S.	kA	S.
eximia	eximia	k1gFnSc1	eximia
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
popsaný	popsaný	k2eAgInSc1d1	popsaný
z	z	k7c2	z
Českého	český	k2eAgInSc2d1	český
krasu	kras	k1gInSc2	kras
a	a	k8xC	a
jeřáb	jeřáb	k1gMnSc1	jeřáb
džbánský	džbánský	k2eAgMnSc1d1	džbánský
(	(	kIx(	(
<g/>
S.	S.	kA	S.
gemella	gemella	k1gFnSc1	gemella
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rostoucí	rostoucí	k2eAgInSc1d1	rostoucí
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Džbánu	džbán	k1gInSc2	džbán
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
oranžové	oranžový	k2eAgInPc1d1	oranžový
plody	plod	k1gInPc1	plod
a	a	k8xC	a
žluté	žlutý	k2eAgInPc1d1	žlutý
prašníky	prašník	k1gInPc1	prašník
<g/>
.	.	kIx.	.
</s>
<s>
Jeřáb	jeřáb	k1gMnSc1	jeřáb
český	český	k2eAgMnSc1d1	český
(	(	kIx(	(
<g/>
S.	S.	kA	S.
bohemica	bohemica	k1gFnSc1	bohemica
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vyskytující	vyskytující	k2eAgMnSc1d1	vyskytující
se	se	k3xPyFc4	se
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
Českém	český	k2eAgNnSc6d1	české
středohoří	středohoří	k1gNnSc6	středohoří
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
menšími	malý	k2eAgInPc7d2	menší
eliptickými	eliptický	k2eAgInPc7d1	eliptický
až	až	k8xS	až
vejčitými	vejčitý	k2eAgInPc7d1	vejčitý
listy	list	k1gInPc7	list
a	a	k8xC	a
žlutými	žlutý	k2eAgInPc7d1	žlutý
prašníky	prašník	k1gInPc7	prašník
<g/>
.	.	kIx.	.
</s>
<s>
Druh	druh	k1gInSc1	druh
je	být	k5eAaImIp3nS	být
ohrožen	ohrozit	k5eAaPmNgInS	ohrozit
vysokými	vysoký	k2eAgInPc7d1	vysoký
stavy	stav	k1gInPc7	stav
zvěře	zvěř	k1gFnSc2	zvěř
a	a	k8xC	a
masivním	masivní	k2eAgNnSc7d1	masivní
šířením	šíření	k1gNnSc7	šíření
jasanu	jasan	k1gInSc2	jasan
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
nezbytné	nezbytný	k2eAgFnPc1d1	nezbytná
jeho	jeho	k3xOp3gFnSc3	jeho
ochraně	ochrana	k1gFnSc3	ochrana
a	a	k8xC	a
věnovat	věnovat	k5eAaPmF	věnovat
maximální	maximální	k2eAgFnSc4d1	maximální
pozornost	pozornost	k1gFnSc4	pozornost
<g/>
.	.	kIx.	.
</s>
<s>
Druh	druh	k1gInSc1	druh
je	být	k5eAaImIp3nS	být
žádoucí	žádoucí	k2eAgMnSc1d1	žádoucí
zařadit	zařadit	k5eAaPmF	zařadit
jak	jak	k6eAd1	jak
do	do	k7c2	do
červeného	červený	k2eAgInSc2d1	červený
seznamu	seznam	k1gInSc2	seznam
taxonů	taxon	k1gInPc2	taxon
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
tak	tak	k6eAd1	tak
mezi	mezi	k7c7	mezi
zákonem	zákon	k1gInSc7	zákon
zvláště	zvláště	k6eAd1	zvláště
chráněné	chráněný	k2eAgInPc4d1	chráněný
druhy	druh	k1gInPc4	druh
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
případech	případ	k1gInPc6	případ
do	do	k7c2	do
kategorie	kategorie	k1gFnSc2	kategorie
kriticky	kriticky	k6eAd1	kriticky
ohrožený	ohrožený	k2eAgInSc4d1	ohrožený
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
Jeřáb	jeřáb	k1gMnSc1	jeřáb
milský	milský	k1gMnSc1	milský
byl	být	k5eAaImAgMnS	být
popsán	popsat	k5eAaPmNgMnS	popsat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
z	z	k7c2	z
vrchu	vrch	k1gInSc2	vrch
Milá	milá	k1gFnSc1	milá
(	(	kIx(	(
<g/>
České	český	k2eAgNnSc1d1	české
středohoří	středohoří	k1gNnSc1	středohoří
<g/>
)	)	kIx)	)
v	v	k7c6	v
Českém	český	k2eAgNnSc6d1	české
středohoří	středohoří	k1gNnSc6	středohoří
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Preslia	Preslium	k1gNnSc2	Preslium
<g/>
.	.	kIx.	.
</s>
<s>
Přívlastek	přívlastek	k1gInSc1	přívlastek
"	"	kIx"	"
<g/>
milský	milský	k1gMnSc1	milský
<g/>
"	"	kIx"	"
resp.	resp.	kA	resp.
"	"	kIx"	"
<g/>
milensis	milensis	k1gInSc1	milensis
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
z	z	k7c2	z
názvu	název	k1gInSc2	název
vrchu	vrch	k1gInSc2	vrch
Milá	milý	k2eAgFnSc1d1	Milá
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
druh	druh	k1gInSc1	druh
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
Typová	typový	k2eAgFnSc1d1	typová
herbářová	herbářový	k2eAgFnSc1d1	herbářová
položka	položka	k1gFnSc1	položka
<g/>
,	,	kIx,	,
ke	k	k7c3	k
které	který	k3yIgFnSc3	který
se	se	k3xPyFc4	se
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
vědecké	vědecký	k2eAgNnSc1d1	vědecké
jméno	jméno	k1gNnSc1	jméno
druhu	druh	k1gInSc2	druh
je	být	k5eAaImIp3nS	být
uložena	uložit	k5eAaPmNgFnS	uložit
v	v	k7c6	v
botanických	botanický	k2eAgFnPc6d1	botanická
sbírkách	sbírka	k1gFnPc6	sbírka
Jihočeského	jihočeský	k2eAgNnSc2d1	Jihočeské
muzea	muzeum	k1gNnSc2	muzeum
v	v	k7c6	v
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
<g/>
.	.	kIx.	.
</s>
<s>
Oprávněnost	oprávněnost	k1gFnSc1	oprávněnost
hodnocení	hodnocení	k1gNnSc2	hodnocení
jeřábu	jeřáb	k1gInSc2	jeřáb
milského	milské	k1gNnSc2	milské
na	na	k7c6	na
druhové	druhový	k2eAgFnSc6d1	druhová
úrovni	úroveň	k1gFnSc6	úroveň
potvrdily	potvrdit	k5eAaPmAgFnP	potvrdit
nejmodernější	moderní	k2eAgFnPc4d3	nejmodernější
biosystematické	biosystematický	k2eAgFnPc4d1	biosystematický
a	a	k8xC	a
morfomerické	morfomerický	k2eAgFnPc4d1	morfomerický
metody	metoda	k1gFnPc4	metoda
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
rovněž	rovněž	k6eAd1	rovněž
podpořily	podpořit	k5eAaPmAgFnP	podpořit
apomiktický	apomiktický	k2eAgInSc4d1	apomiktický
způsob	způsob	k1gInSc4	způsob
reprodukce	reprodukce	k1gFnSc2	reprodukce
druhu	druh	k1gInSc2	druh
<g/>
.	.	kIx.	.
</s>
