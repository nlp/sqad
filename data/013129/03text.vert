<p>
<s>
Výklad	výklad	k1gInSc1	výklad
snů	sen	k1gInPc2	sen
(	(	kIx(	(
<g/>
v	v	k7c6	v
německém	německý	k2eAgInSc6d1	německý
originále	originál	k1gInSc6	originál
Die	Die	k1gMnSc1	Die
Traumdeutung	Traumdeutung	k1gMnSc1	Traumdeutung
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
kniha	kniha	k1gFnSc1	kniha
zakladatele	zakladatel	k1gMnSc2	zakladatel
psychoanalýzy	psychoanalýza	k1gFnSc2	psychoanalýza
Sigmunda	Sigmund	k1gMnSc2	Sigmund
Freuda	Freud	k1gMnSc2	Freud
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výklad	výklad	k1gInSc1	výklad
snů	sen	k1gInPc2	sen
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejznámějším	známý	k2eAgFnPc3d3	nejznámější
Freudovým	Freudový	k2eAgFnPc3d1	Freudový
knihám	kniha	k1gFnPc3	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
vyšla	vyjít	k5eAaPmAgFnS	vyjít
roku	rok	k1gInSc2	rok
1899	[number]	k4	1899
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Franz	Franza	k1gFnPc2	Franza
Deuticke	Deuticke	k1gNnPc2	Deuticke
<g/>
.	.	kIx.	.
</s>
<s>
Nakladatel	nakladatel	k1gMnSc1	nakladatel
nicméně	nicméně	k8xC	nicméně
knihu	kniha	k1gFnSc4	kniha
plánoval	plánovat	k5eAaImAgMnS	plánovat
na	na	k7c4	na
rok	rok	k1gInSc4	rok
1900	[number]	k4	1900
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
vydání	vydání	k1gNnSc6	vydání
uvedeno	uvést	k5eAaPmNgNnS	uvést
právě	právě	k6eAd1	právě
toto	tento	k3xDgNnSc4	tento
vročení	vročení	k1gNnSc4	vročení
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
distribucí	distribuce	k1gFnSc7	distribuce
však	však	k9	však
začal	začít	k5eAaPmAgMnS	začít
již	již	k6eAd1	již
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
1899	[number]	k4	1899
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kniha	kniha	k1gFnSc1	kniha
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
klíčový	klíčový	k2eAgInSc4d1	klíčový
Freudův	Freudův	k2eAgInSc4d1	Freudův
spis	spis	k1gInSc4	spis
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
kromě	kromě	k7c2	kromě
popisu	popis	k1gInSc2	popis
techniky	technika	k1gFnSc2	technika
výkladu	výklad	k1gInSc2	výklad
snu	sen	k1gInSc2	sen
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
i	i	k9	i
definici	definice	k1gFnSc4	definice
pojmů	pojem	k1gInPc2	pojem
vědomí	vědomí	k1gNnSc2	vědomí
a	a	k8xC	a
nevědomí	nevědomí	k1gNnSc2	nevědomí
(	(	kIx(	(
<g/>
v	v	k7c6	v
pozdějších	pozdní	k2eAgNnPc6d2	pozdější
vydáních	vydání	k1gNnPc6	vydání
poté	poté	k6eAd1	poté
ještě	ještě	k6eAd1	ještě
předvědomí	předvědomý	k2eAgMnPc1d1	předvědomý
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
tzv.	tzv.	kA	tzv.
Oidipovského	oidipovský	k2eAgInSc2d1	oidipovský
komplexu	komplex	k1gInSc2	komplex
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Freud	Freud	k6eAd1	Freud
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
analyzuje	analyzovat	k5eAaImIp3nS	analyzovat
především	především	k9	především
své	svůj	k3xOyFgInPc4	svůj
vlastní	vlastní	k2eAgInPc4d1	vlastní
sny	sen	k1gInPc4	sen
a	a	k8xC	a
Výklad	výklad	k1gInSc1	výklad
snů	sen	k1gInPc2	sen
je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
klíčových	klíčový	k2eAgInPc2d1	klíčový
informačních	informační	k2eAgInPc2d1	informační
zdrojů	zdroj	k1gInPc2	zdroj
Freudových	Freudový	k2eAgMnPc2d1	Freudový
životopisců	životopisec	k1gMnPc2	životopisec
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgInPc1d1	jiný
možné	možný	k2eAgInPc1d1	možný
zdroje	zdroj	k1gInPc1	zdroj
Freud	Freuda	k1gFnPc2	Freuda
systematicky	systematicky	k6eAd1	systematicky
ničil	ničit	k5eAaImAgMnS	ničit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kniha	kniha	k1gFnSc1	kniha
se	se	k3xPyFc4	se
dlouho	dlouho	k6eAd1	dlouho
nemohla	moct	k5eNaImAgFnS	moct
u	u	k7c2	u
čtenářů	čtenář	k1gMnPc2	čtenář
prosadit	prosadit	k5eAaPmF	prosadit
<g/>
,	,	kIx,	,
za	za	k7c4	za
prvních	první	k4xOgInPc2	první
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
prodalo	prodat	k5eAaPmAgNnS	prodat
jen	jen	k9	jen
něco	něco	k3yInSc4	něco
přes	přes	k7c4	přes
300	[number]	k4	300
výtisků	výtisk	k1gInPc2	výtisk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
MARINELLI	MARINELLI	kA	MARINELLI
<g/>
,	,	kIx,	,
Lydia	Lydium	k1gNnSc2	Lydium
<g/>
,	,	kIx,	,
MAYER	Mayer	k1gMnSc1	Mayer
<g/>
,	,	kIx,	,
Andreas	Andreas	k1gMnSc1	Andreas
A.	A.	kA	A.
<g/>
:	:	kIx,	:
Dreaming	Dreaming	k1gInSc1	Dreaming
by	by	kYmCp3nS	by
the	the	k?	the
Book	Booko	k1gNnPc2	Booko
<g/>
:	:	kIx,	:
A	a	k8xC	a
History	Histor	k1gInPc1	Histor
of	of	k?	of
Freud	Freud	k1gInSc1	Freud
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
'	'	kIx"	'
<g/>
The	The	k1gMnSc2	The
Interpretation	Interpretation	k1gInSc1	Interpretation
of	of	k?	of
Dreams	Dreams	k1gInSc1	Dreams
<g/>
'	'	kIx"	'
and	and	k?	and
the	the	k?	the
Psychoanalytic	Psychoanalytice	k1gFnPc2	Psychoanalytice
Movement	Movement	k1gMnSc1	Movement
<g/>
,	,	kIx,	,
New	New	k1gMnSc1	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
Other	Other	k1gInSc1	Other
Press	Press	k1gInSc1	Press
2003	[number]	k4	2003
</s>
</p>
