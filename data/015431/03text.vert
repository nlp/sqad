<s>
Děkanát	děkanát	k1gInSc1
Prostějov	Prostějov	k1gInSc1
</s>
<s>
Děkanát	děkanát	k1gInSc1
ProstějovDiecéze	ProstějovDiecéza	k1gFnSc3
</s>
<s>
arcidiecéze	arcidiecéze	k1gFnSc1
olomoucká	olomoucký	k2eAgFnSc1d1
Provincie	provincie	k1gFnSc1
</s>
<s>
moravská	moravský	k2eAgFnSc1d1
Děkan	děkan	k1gMnSc1
</s>
<s>
R.	R.	kA
D.	D.	kA
Mgr.	Mgr.	kA
Aleš	Aleš	k1gMnSc1
Vrzala	Vrzal	k1gMnSc2
Další	další	k2eAgInSc1d1
úřad	úřad	k1gInSc1
děkana	děkan	k1gMnSc2
</s>
<s>
farář	farář	k1gMnSc1
u	u	k7c2
Povýšení	povýšení	k1gNnSc2
sv.	sv.	kA
Kříže	kříž	k1gInSc2
v	v	k7c6
Prostějově	Prostějov	k1gInSc6
Údaje	údaj	k1gInSc2
v	v	k7c6
infoboxu	infobox	k1gInSc6
aktuální	aktuální	k2eAgInPc1d1
k	k	k7c3
2019	#num#	k4
</s>
<s>
Děkanát	děkanát	k1gInSc1
Prostějov	Prostějov	k1gInSc1
(	(	kIx(
<g/>
latinsky	latinsky	k6eAd1
<g/>
:	:	kIx,
Decanatus	Decanatus	k1gInSc1
Prostannensis	Prostannensis	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
územní	územní	k2eAgFnSc1d1
část	část	k1gFnSc1
Arcidiecéze	arcidiecéze	k1gFnSc1
Olomouc	Olomouc	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvoří	tvořit	k5eAaImIp3nS
ho	on	k3xPp3gNnSc2
34	#num#	k4
farností	farnost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Děkanem	děkan	k1gMnSc7
je	být	k5eAaImIp3nS
R.	R.	kA
D.	D.	kA
Mgr.	Mgr.	kA
Aleš	Aleš	k1gMnSc1
Vrzala	Vrzal	k1gMnSc2
<g/>
,	,	kIx,
farář	farář	k1gMnSc1
v	v	k7c6
prostějovské	prostějovský	k2eAgFnSc6d1
farnosti	farnost	k1gFnSc6
Povýšení	povýšení	k1gNnSc4
svatého	svatý	k2eAgMnSc2d1
Kříže	Kříž	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kaplan	Kaplan	k1gMnSc1
pro	pro	k7c4
mládež	mládež	k1gFnSc4
je	být	k5eAaImIp3nS
P.	P.	kA
Mgr.	Mgr.	kA
Tomáš	Tomáš	k1gMnSc1
Strogan	Strogan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Místoděkanem	Místoděkan	k1gInSc7
je	být	k5eAaImIp3nS
od	od	k7c2
července	červenec	k1gInSc2
2019	#num#	k4
P.	P.	kA
Mgr.	Mgr.	kA
Andrzej	Andrzej	k1gMnSc1
Kaliciak	Kaliciak	k1gMnSc1
<g/>
,	,	kIx,
SDS	SDS	kA
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
ve	v	k7c6
Vřesovicích	Vřesovice	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
děkanátu	děkanát	k1gInSc6
působí	působit	k5eAaImIp3nS
6	#num#	k4
diecézních	diecézní	k2eAgMnPc2d1
a	a	k8xC
9	#num#	k4
řeholních	řeholní	k2eAgMnPc2d1
kněží	kněz	k1gMnPc2
<g/>
,	,	kIx,
spadá	spadat	k5eAaPmIp3nS,k5eAaImIp3nS
sem	sem	k6eAd1
34	#num#	k4
farností	farnost	k1gFnPc2
s	s	k7c7
93	#num#	k4
kostely	kostel	k1gInPc7
a	a	k8xC
kaplemi	kaple	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
území	území	k1gNnSc6
děkanátu	děkanát	k1gInSc2
žije	žít	k5eAaImIp3nS
31	#num#	k4
000	#num#	k4
lidí	člověk	k1gMnPc2
katolického	katolický	k2eAgNnSc2d1
vyznání	vyznání	k1gNnSc2
<g/>
,	,	kIx,
nedělní	nedělní	k2eAgFnSc4d1
účast	účast	k1gFnSc4
na	na	k7c6
bohoslužbách	bohoslužba	k1gFnPc6
je	být	k5eAaImIp3nS
odhadována	odhadovat	k5eAaImNgFnS
na	na	k7c4
12,90	12,90	k4
<g/>
%	%	kIx~
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
FarnostFarní	FarnostFarnět	k5eAaPmIp3nS
kostelSprávceWeb	kostelSprávceWba	k1gFnPc2
farnosti	farnost	k1gFnSc2
</s>
<s>
Brodek	brodek	k1gInSc1
u	u	k7c2
Prostějova	Prostějov	k1gInSc2
</s>
<s>
Povýšení	povýšení	k1gNnSc1
svatého	svatý	k2eAgMnSc2d1
Kříže	Kříž	k1gMnSc2
</s>
<s>
farář	farář	k1gMnSc1
–	–	k?
P.	P.	kA
Šíra	šíro	k1gNnPc1
Pavel	Pavel	k1gMnSc1
<g/>
,	,	kIx,
Mgr.	Mgr.	kA
</s>
<s>
Čehovice	Čehovice	k1gFnSc1
</s>
<s>
svatého	svatý	k2eAgMnSc2d1
Prokopa	Prokop	k1gMnSc2
</s>
<s>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k1gNnSc1
–	–	k?
P.	P.	kA
Pachołek	Pachołek	k1gMnSc1
Jan	Jan	k1gMnSc1
Józef	Józef	k1gMnSc1
<g/>
,	,	kIx,
ThLic	ThLic	k1gMnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
SDS	SDS	kA
</s>
<s>
Dobromilice	Dobromilice	k1gFnSc1
</s>
<s>
Všech	všecek	k3xTgMnPc2
svatých	svatý	k1gMnPc2
</s>
<s>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k1gNnSc1
–	–	k?
P.	P.	kA
Marek	Marek	k1gMnSc1
František	František	k1gMnSc1
Glac	Glac	k1gFnSc1
<g/>
,	,	kIx,
Mgr.	Mgr.	kA
</s>
<s>
Drahany	Drahana	k1gFnPc1
</s>
<s>
svatého	svatý	k2eAgMnSc2d1
Jana	Jan	k1gMnSc2
Křtitele	křtitel	k1gMnSc2
</s>
<s>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k1gNnSc1
–	–	k?
P.	P.	kA
Šidleja	Šidleja	k1gMnSc1
Radomír	Radomír	k1gMnSc1
<g/>
,	,	kIx,
Ing.	ing.	kA
Mgr.	Mgr.	kA
</s>
<s>
–	–	k?
</s>
<s>
Dubany	Dubana	k1gFnPc1
</s>
<s>
Narození	narození	k1gNnSc1
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
</s>
<s>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k1gNnSc1
–	–	k?
P.	P.	kA
Jiří	Jiří	k1gMnSc1
Bernard	Bernard	k1gMnSc1
Špaček	Špaček	k1gMnSc1
<g/>
,	,	kIx,
Mgr.	Mgr.	kA
<g/>
,	,	kIx,
OP	op	k1gMnSc1
</s>
<s>
–	–	k?
</s>
<s>
Hruška	hruška	k1gFnSc1
</s>
<s>
svatého	svatý	k2eAgMnSc2d1
Jana	Jan	k1gMnSc2
Nepomuckého	Nepomucký	k1gMnSc2
</s>
<s>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k1gNnSc1
–	–	k?
P.	P.	kA
Strogan	Strogan	k1gMnSc1
Tomáš	Tomáš	k1gMnSc1
<g/>
,	,	kIx,
Mgr.	Mgr.	kA
</s>
<s>
–	–	k?
</s>
<s>
Klenovice	Klenovice	k1gFnSc1
na	na	k7c6
Hané	Haná	k1gFnSc6
</s>
<s>
svatého	svatý	k2eAgMnSc4d1
Bartoloměje	Bartoloměj	k1gMnSc4
</s>
<s>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k1gNnSc1
–	–	k?
P.	P.	kA
Pachołek	Pachołek	k1gMnSc1
Jan	Jan	k1gMnSc1
Józef	Józef	k1gMnSc1
<g/>
,	,	kIx,
ThLic	ThLic	k1gMnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
SDS	SDS	kA
</s>
<s>
Kostelec	Kostelec	k1gInSc1
na	na	k7c6
Hané	Haná	k1gFnSc6
</s>
<s>
svatého	svatý	k2eAgMnSc2d1
Jakuba	Jakub	k1gMnSc2
Staršího	starší	k1gMnSc2
</s>
<s>
farář	farář	k1gMnSc1
–	–	k?
P.	P.	kA
Šimara	Šimara	k1gFnSc1
Petr	Petr	k1gMnSc1
<g/>
,	,	kIx,
Ing.	ing.	kA
Mgr.	Mgr.	kA
</s>
<s>
Kralice	Kralice	k1gFnPc1
na	na	k7c6
Hané	Haná	k1gFnSc6
</s>
<s>
Nanebevzetí	nanebevzetí	k1gNnSc1
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
</s>
<s>
administrátor	administrátor	k1gMnSc1
–	–	k?
P.	P.	kA
Pachołek	Pachołek	k1gMnSc1
Jan	Jan	k1gMnSc1
Józef	Józef	k1gMnSc1
<g/>
,	,	kIx,
ThLic	ThLic	k1gMnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
SDS	SDS	kA
</s>
<s>
Krumsín	Krumsín	k1gMnSc1
</s>
<s>
svatého	svatý	k2eAgMnSc4d1
Bartoloměje	Bartoloměj	k1gMnSc4
</s>
<s>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k1gNnSc1
–	–	k?
P.	P.	kA
Salaga	Salaga	k1gFnSc1
Tomasz	Tomasz	k1gInSc1
<g/>
,	,	kIx,
Mgr.	Mgr.	kA
<g/>
,	,	kIx,
SDS	SDS	kA
</s>
<s>
–	–	k?
</s>
<s>
Mostkovice	Mostkovice	k1gFnSc1
</s>
<s>
Nanebevzetí	nanebevzetí	k1gNnSc1
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
</s>
<s>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k1gNnSc1
–	–	k?
P.	P.	kA
Salaga	Salaga	k1gFnSc1
Tomasz	Tomasz	k1gInSc1
Mgr.	Mgr.	kA
<g/>
,	,	kIx,
SDS	SDS	kA
</s>
<s>
Myslejovice	Myslejovice	k1gFnSc1
</s>
<s>
Zvěstování	zvěstování	k1gNnSc1
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
</s>
<s>
administrátor	administrátor	k1gMnSc1
exkurendo	exkurendo	k6eAd1
-	-	kIx~
P.	P.	kA
Barbořák	Barbořák	k1gMnSc1
Pavel	Pavel	k1gMnSc1
<g/>
,	,	kIx,
Mgr.	Mgr.	kA
<g/>
,	,	kIx,
SDS	SDS	kA
</s>
<s>
Němčice	Němčice	k1gFnSc1
nad	nad	k7c7
Hanou	Hana	k1gFnSc7
</s>
<s>
svaté	svatý	k2eAgFnPc4d1
Máří	Máří	k?
Magdalény	Magdaléna	k1gFnPc4
</s>
<s>
farář	farář	k1gMnSc1
–	–	k?
P.	P.	kA
Strogan	Strogan	k1gMnSc1
Tomáš	Tomáš	k1gMnSc1
<g/>
,	,	kIx,
Mgr.	Mgr.	kA
</s>
<s>
–	–	k?
</s>
<s>
Nezamyslice	Nezamyslice	k1gFnPc1
na	na	k7c6
Hané	Haná	k1gFnSc6
</s>
<s>
svatého	svatý	k2eAgMnSc4d1
Václava	Václav	k1gMnSc4
</s>
<s>
farář	farář	k1gMnSc1
–	–	k?
P.	P.	kA
Marek	Marek	k1gMnSc1
František	František	k1gMnSc1
Glac	Glac	k1gFnSc1
<g/>
,	,	kIx,
Mgr.	Mgr.	kA
</s>
<s>
–	–	k?
</s>
<s>
Ohrozim	Ohrozim	k?
</s>
<s>
svatého	svatý	k2eAgMnSc4d1
Václava	Václav	k1gMnSc4
</s>
<s>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
-	-	kIx~
P.	P.	kA
Šimara	Šimara	k1gFnSc1
Petr	Petr	k1gMnSc1
<g/>
,	,	kIx,
Ing.	ing.	kA
Mgr.	Mgr.	kA
</s>
<s>
Olšany	Olšany	k1gInPc1
u	u	k7c2
Prostějova	Prostějov	k1gInSc2
</s>
<s>
sv.	sv.	kA
Jana	Jan	k1gMnSc4
Křtitele	křtitel	k1gMnSc4
</s>
<s>
administrátor	administrátor	k1gMnSc1
-	-	kIx~
P.	P.	kA
Jiří	Jiří	k1gMnSc1
Bernard	Bernard	k1gMnSc1
Špaček	Špaček	k1gMnSc1
<g/>
,	,	kIx,
Mgr.	Mgr.	kA
<g/>
,	,	kIx,
OP	op	k1gMnSc1
</s>
<s>
–	–	k?
</s>
<s>
Otaslavice	Otaslavice	k1gFnSc1
</s>
<s>
svatého	svatý	k2eAgMnSc4d1
Michaela	Michael	k1gMnSc4
archanděla	archanděl	k1gMnSc4
</s>
<s>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k1gNnSc1
–	–	k?
P.	P.	kA
Šíra	šíro	k1gNnPc1
Pavel	Pavel	k1gMnSc1
<g/>
,	,	kIx,
Mgr.	Mgr.	kA
</s>
<s>
Pavlovice	Pavlovice	k1gFnPc1
u	u	k7c2
Kojetína	Kojetín	k1gInSc2
</s>
<s>
svatého	svatý	k2eAgMnSc2d1
Ondřeje	Ondřej	k1gMnSc2
</s>
<s>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k1gNnSc1
–	–	k?
P.	P.	kA
Strogan	Strogan	k1gMnSc1
Tomáš	Tomáš	k1gMnSc1
<g/>
,	,	kIx,
Mgr.	Mgr.	kA
</s>
<s>
–	–	k?
</s>
<s>
Pivín	Pivín	k1gMnSc1
</s>
<s>
svatého	svatý	k2eAgMnSc2d1
Jiří	Jiří	k1gMnSc1
</s>
<s>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
-	-	kIx~
P.	P.	kA
Andrzej	Andrzej	k1gMnSc1
Kaliciak	Kaliciak	k1gMnSc1
<g/>
,	,	kIx,
Mgr.	Mgr.	kA
<g/>
,	,	kIx,
SDS	SDS	kA
<g/>
.	.	kIx.
</s>
<s>
Plumlov	Plumlov	k1gInSc1
</s>
<s>
Nejsvětější	nejsvětější	k2eAgFnSc1d1
Trojice	trojice	k1gFnSc1
</s>
<s>
administrátor	administrátor	k1gMnSc1
P.	P.	kA
Tomasz	Tomasz	k1gMnSc1
Sałaga	Sałag	k1gMnSc2
Mgr.	Mgr.	kA
<g/>
,	,	kIx,
SDS	SDS	kA
</s>
<s>
–	–	k?
</s>
<s>
Prostějov	Prostějov	k1gInSc1
–	–	k?
Povýšení	povýšení	k1gNnSc1
sv.	sv.	kA
Kříže	kříž	k1gInSc2
</s>
<s>
Povýšení	povýšení	k1gNnSc1
sv.	sv.	kA
Kříže	kříž	k1gInSc2
</s>
<s>
farář	farář	k1gMnSc1
-	-	kIx~
P.	P.	kA
Aleš	Aleš	k1gMnSc1
Vrzala	Vrzal	k1gMnSc2
<g/>
,	,	kIx,
ICLic	ICLic	k1gMnSc1
<g/>
,	,	kIx,
Mgr.	Mgr.	kA
</s>
<s>
Prostějov	Prostějov	k1gInSc1
–	–	k?
sv.	sv.	kA
Petr	Petr	k1gMnSc1
a	a	k8xC
Pavel	Pavel	k1gMnSc1
</s>
<s>
sv.	sv.	kA
Petra	Petr	k1gMnSc4
a	a	k8xC
Pavla	Pavel	k1gMnSc4
</s>
<s>
administrátor	administrátor	k1gMnSc1
-	-	kIx~
P.	P.	kA
Pavel	Pavel	k1gMnSc1
Maria	Mario	k1gMnSc2
Čáp	Čáp	k1gMnSc1
<g/>
,	,	kIx,
ThLic	ThLic	k1gMnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
SDB	SDB	kA
</s>
<s>
Prostějov	Prostějov	k1gInSc1
–	–	k?
Vrahovice	Vrahovice	k1gFnSc2
</s>
<s>
sv.	sv.	kA
Bartoloměje	Bartoloměj	k1gMnSc4
</s>
<s>
farář	farář	k1gMnSc1
-	-	kIx~
P.	P.	kA
Matušů	Matuš	k1gInPc2
Emil	Emil	k1gMnSc1
<g/>
,	,	kIx,
Mgr.	Mgr.	kA
<g/>
,	,	kIx,
SDB	SDB	kA
</s>
<s>
–	–	k?
</s>
<s>
Rozstání	Rozstání	k1gNnSc1
</s>
<s>
svatého	svatý	k2eAgMnSc4d1
Michaela	Michael	k1gMnSc4
archanděla	archanděl	k1gMnSc4
</s>
<s>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k1gNnSc1
–	–	k?
P.	P.	kA
Šidleja	Šidleja	k1gMnSc1
Radomír	Radomír	k1gMnSc1
<g/>
,	,	kIx,
Ing.	ing.	kA
Mgr.	Mgr.	kA
</s>
<s>
–	–	k?
</s>
<s>
Smržice	Smržice	k1gFnSc1
</s>
<s>
sv.	sv.	kA
Petra	Petr	k1gMnSc4
a	a	k8xC
Pavla	Pavel	k1gMnSc4
</s>
<s>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
-	-	kIx~
P.	P.	kA
Matušů	Matuš	k1gInPc2
Emil	Emil	k1gMnSc1
<g/>
,	,	kIx,
Mgr.	Mgr.	kA
<g/>
,	,	kIx,
SDB	SDB	kA
</s>
<s>
–	–	k?
</s>
<s>
Tištín	Tištín	k1gMnSc1
</s>
<s>
sv.	sv.	kA
Petra	Petr	k1gMnSc4
a	a	k8xC
Pavla	Pavel	k1gMnSc4
</s>
<s>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k1gNnSc1
–	–	k?
P.	P.	kA
Marek	Marek	k1gMnSc1
František	František	k1gMnSc1
Glac	Glac	k1gFnSc1
<g/>
,	,	kIx,
Mgr.	Mgr.	kA
</s>
<s>
–	–	k?
</s>
<s>
Určice	Určice	k1gFnPc1
</s>
<s>
sv.	sv.	kA
Jana	Jan	k1gMnSc4
Křtitele	křtitel	k1gMnSc4
</s>
<s>
farář	farář	k1gMnSc1
-	-	kIx~
P.	P.	kA
Barbořák	Barbořák	k1gMnSc1
Pavel	Pavel	k1gMnSc1
<g/>
,	,	kIx,
Mgr.	Mgr.	kA
<g/>
,	,	kIx,
SDS	SDS	kA
</s>
<s>
Vícov	Vícov	k1gInSc1
</s>
<s>
sv.	sv.	kA
Floriána	Florián	k1gMnSc4
</s>
<s>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
-	-	kIx~
P.	P.	kA
Šimara	Šimara	k1gFnSc1
Petr	Petr	k1gMnSc1
<g/>
,	,	kIx,
Ing.	ing.	kA
Mgr.	Mgr.	kA
</s>
<s>
Vranovice	Vranovice	k1gFnSc1
u	u	k7c2
Prostějova	Prostějov	k1gInSc2
</s>
<s>
sv.	sv.	kA
Kunhuty	Kunhuta	k1gFnPc1
</s>
<s>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
-	-	kIx~
P.	P.	kA
Adam	Adam	k1gMnSc1
Kazimierz	Kazimierz	k1gMnSc1
Cynarski	Cynarske	k1gFnSc4
<g/>
,	,	kIx,
Mgr.	Mgr.	kA
<g/>
,	,	kIx,
SDS	SDS	kA
</s>
<s>
Vrchoslavice	Vrchoslavice	k1gFnSc1
</s>
<s>
sv.	sv.	kA
Michaela	Michaela	k1gFnSc1
</s>
<s>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k1gNnSc1
–	–	k?
P.	P.	kA
Strogan	Strogan	k1gMnSc1
Tomáš	Tomáš	k1gMnSc1
<g/>
,	,	kIx,
Mgr.	Mgr.	kA
</s>
<s>
–	–	k?
</s>
<s>
Vřesovice	Vřesovice	k1gFnSc1
</s>
<s>
sv.	sv.	kA
Petra	Petr	k1gMnSc4
a	a	k8xC
Pavla	Pavel	k1gMnSc4
</s>
<s>
administrátor	administrátor	k1gMnSc1
-	-	kIx~
P.	P.	kA
Andrzej	Andrzej	k1gMnSc1
Kaliciak	Kaliciak	k1gMnSc1
<g/>
,	,	kIx,
Mgr.	Mgr.	kA
<g/>
,	,	kIx,
SDS	SDS	kA
</s>
<s>
–	–	k?
</s>
<s>
Výšovice	Výšovice	k1gFnSc1
</s>
<s>
sv.	sv.	kA
Vavřince	Vavřinec	k1gMnSc4
</s>
<s>
administrátor	administrátor	k1gMnSc1
excurrendo-	excurrendo-	k?
P.	P.	kA
Andrzej	Andrzej	k1gMnSc1
Kaliciak	Kaliciak	k1gMnSc1
<g/>
,	,	kIx,
Mgr.	Mgr.	kA
<g/>
,	,	kIx,
SDS	SDS	kA
</s>
<s>
–	–	k?
</s>
<s>
Želeč	Želeč	k1gInSc1
u	u	k7c2
Prostějova	Prostějov	k1gInSc2
</s>
<s>
sv.	sv.	kA
Bartoloměje	Bartoloměj	k1gMnSc4
</s>
<s>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k1gNnSc1
–	–	k?
P.	P.	kA
Šíra	šíro	k1gNnPc1
Pavel	Pavel	k1gMnSc1
<g/>
,	,	kIx,
Mgr.	Mgr.	kA
</s>
<s>
Žešov	Žešov	k1gInSc1
</s>
<s>
sv.	sv.	kA
Cyrila	Cyril	k1gMnSc4
a	a	k8xC
Metoděje	Metoděj	k1gMnSc4
</s>
<s>
administrátor	administrátor	k1gMnSc1
-	-	kIx~
P.	P.	kA
Adam	Adam	k1gMnSc1
Kazimierz	Kazimierz	k1gMnSc1
Cynarski	Cynarske	k1gFnSc4
<g/>
,	,	kIx,
Mgr.	Mgr.	kA
<g/>
,	,	kIx,
SDS	SDS	kA
</s>
<s>
Podle	podle	k7c2
stránek	stránka	k1gFnPc2
děkanátu	děkanát	k1gInSc2
a	a	k8xC
arcidiecéze	arcidiecéze	k1gFnSc2
z	z	k7c2
9	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2020	#num#	k4
</s>
<s>
Fotogalerie	Fotogalerie	k1gFnSc1
</s>
<s>
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Jana	Jan	k1gMnSc2
Křtitele	křtitel	k1gMnSc2
<g/>
,	,	kIx,
Drahany	Drahana	k1gFnSc2
</s>
<s>
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Bartoloměje	Bartoloměj	k1gMnSc2
<g/>
,	,	kIx,
Krumsín	Krumsín	k1gInSc1
</s>
<s>
Fara	fara	k1gFnSc1
a	a	k8xC
kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Jana	Jan	k1gMnSc2
Křtitele	křtitel	k1gMnSc2
<g/>
,	,	kIx,
Olšany	Olšany	k1gInPc7
</s>
<s>
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Michala	Michal	k1gMnSc2
<g/>
,	,	kIx,
Otaslavice	Otaslavice	k1gFnSc2
</s>
<s>
Kostel	kostel	k1gInSc1
Nejsvětější	nejsvětější	k2eAgFnSc2d1
Trojice	trojice	k1gFnSc2
<g/>
,	,	kIx,
Plumlov	Plumlov	k1gInSc4
</s>
<s>
Kostel	kostel	k1gInSc1
svatých	svatý	k1gMnPc2
Petra	Petr	k1gMnSc2
a	a	k8xC
Pavla	Pavel	k1gMnSc2
<g/>
,	,	kIx,
Prostějov	Prostějov	k1gInSc1
</s>
<s>
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Floriána	Florián	k1gMnSc2
<g/>
,	,	kIx,
Vícov	Vícov	k1gInSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Děkanát	děkanát	k1gInSc1
Prostějov	Prostějov	k1gInSc1
|	|	kIx~
Arcidiecéze	arcidiecéze	k1gFnSc1
olomoucká	olomoucký	k2eAgFnSc1d1
<g/>
.	.	kIx.
www.ado.cz	www.ado.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Prostějov	Prostějov	k1gInSc1
</s>
<s>
Arcidiecéze	arcidiecéze	k1gFnSc1
olomoucká	olomoucký	k2eAgFnSc1d1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Stránky	stránka	k1gFnPc1
děkanátu	děkanát	k1gInSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Farnosti	farnost	k1gFnSc2
Děkanátu	děkanát	k1gInSc2
Prostějov	Prostějov	k1gInSc1
</s>
<s>
Brodek	brodek	k1gInSc1
u	u	k7c2
Prostějova	Prostějov	k1gInSc2
•	•	k?
Čehovice	Čehovice	k1gFnSc2
•	•	k?
Dobromilice	Dobromilice	k1gFnSc2
•	•	k?
Drahany	Drahana	k1gFnSc2
•	•	k?
Dubany	Dubana	k1gFnSc2
•	•	k?
Hruška	Hruška	k1gMnSc1
•	•	k?
Klenovice	Klenovice	k1gFnSc2
na	na	k7c6
Hané	Haná	k1gFnSc6
•	•	k?
Kostelec	Kostelec	k1gInSc1
na	na	k7c4
Hané	Haná	k1gFnPc4
•	•	k?
Kralice	Kralice	k1gFnPc4
na	na	k7c6
Hané	Haná	k1gFnSc6
•	•	k?
Krumsín	Krumsín	k1gInSc1
•	•	k?
Mostkovice	Mostkovice	k1gFnSc2
•	•	k?
Myslejovice	Myslejovice	k1gFnSc2
•	•	k?
Němčice	Němčice	k1gFnSc1
nad	nad	k7c7
Hanou	Hana	k1gFnSc7
•	•	k?
Nezamyslice	Nezamyslice	k1gFnPc4
•	•	k?
Ohrozim	Ohrozim	k?
•	•	k?
Olšany	Olšany	k1gInPc4
u	u	k7c2
Prostějova	Prostějov	k1gInSc2
•	•	k?
Otaslavice	Otaslavice	k1gFnSc2
•	•	k?
Pavlovice	Pavlovice	k1gFnPc1
u	u	k7c2
Kojetína	Kojetín	k1gInSc2
•	•	k?
Pivín	Pivín	k1gMnSc1
•	•	k?
Plumlov	Plumlov	k1gInSc1
•	•	k?
Prostějov	Prostějov	k1gInSc1
-	-	kIx~
Povýšení	povýšení	k1gNnSc1
sv.	sv.	kA
Kříže	kříž	k1gInSc2
•	•	k?
Prostějov	Prostějov	k1gInSc1
-	-	kIx~
sv.	sv.	kA
Petr	Petr	k1gMnSc1
a	a	k8xC
Pavel	Pavel	k1gMnSc1
•	•	k?
Prostějov	Prostějov	k1gInSc1
–	–	k?
Vrahovice	Vrahovice	k1gFnSc1
•	•	k?
Rozstání	Rozstání	k1gNnPc2
•	•	k?
Smržice	Smržice	k1gFnSc2
•	•	k?
Tištín	Tištín	k1gMnSc1
•	•	k?
Určice	Určice	k1gFnPc4
•	•	k?
Vícov	Vícov	k1gInSc1
•	•	k?
Vranovice	Vranovice	k1gFnSc2
u	u	k7c2
Prostějova	Prostějov	k1gInSc2
•	•	k?
Vrchoslavice	Vrchoslavice	k1gFnSc2
•	•	k?
Vřesovice	Vřesovice	k1gFnSc2
•	•	k?
Výšovice	Výšovice	k1gFnSc2
•	•	k?
Želeč	Želeč	k1gMnSc1
u	u	k7c2
Prostějova	Prostějov	k1gInSc2
•	•	k?
Žešov	Žešov	k1gInSc1
</s>
<s>
Děkanáty	děkanát	k1gInPc1
Arcidiecéze	arcidiecéze	k1gFnSc2
olomoucké	olomoucký	k2eAgFnSc2d1
</s>
<s>
Holešov	Holešov	k1gInSc1
•	•	k?
Hranice	hranice	k1gFnSc2
•	•	k?
Konice	Konice	k1gFnSc2
•	•	k?
Kroměříž	Kroměříž	k1gFnSc1
•	•	k?
Kyjov	Kyjov	k1gInSc1
•	•	k?
Olomouc	Olomouc	k1gFnSc1
•	•	k?
Prostějov	Prostějov	k1gInSc1
•	•	k?
Přerov	Přerov	k1gInSc1
•	•	k?
Svitavy	Svitava	k1gFnSc2
•	•	k?
Šternberk	Šternberk	k1gInSc1
•	•	k?
Šumperk	Šumperk	k1gInSc1
•	•	k?
Uherské	uherský	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
•	•	k?
Uherský	uherský	k2eAgInSc1d1
Brod	Brod	k1gInSc1
•	•	k?
Valašské	valašský	k2eAgInPc1d1
Klobouky	Klobouky	k1gInPc1
•	•	k?
Valašské	valašský	k2eAgNnSc1d1
Meziříčí	Meziříčí	k1gNnSc1
•	•	k?
Veselí	veselí	k1gNnSc1
nad	nad	k7c7
Moravou	Morava	k1gFnSc7
•	•	k?
Vizovice	Vizovice	k1gFnPc4
•	•	k?
Vsetín	Vsetín	k1gInSc1
•	•	k?
Vyškov	Vyškov	k1gInSc1
•	•	k?
Zábřeh	Zábřeh	k1gInSc1
•	•	k?
Zlín	Zlín	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Prostějov	Prostějov	k1gInSc1
|	|	kIx~
Křesťanství	křesťanství	k1gNnSc1
</s>
