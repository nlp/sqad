<s>
Zemská	zemský	k2eAgFnSc1d1	zemská
atmosféra	atmosféra	k1gFnSc1	atmosféra
propouští	propouštět	k5eAaImIp3nS	propouštět
jen	jen	k9	jen
část	část	k1gFnSc1	část
spektra	spektrum	k1gNnSc2	spektrum
slunečního	sluneční	k2eAgNnSc2d1	sluneční
záření	záření	k1gNnSc2	záření
–	–	k?	–
všechny	všechen	k3xTgFnPc4	všechen
složky	složka	k1gFnPc4	složka
viditelného	viditelný	k2eAgNnSc2d1	viditelné
spektra	spektrum	k1gNnSc2	spektrum
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
ultrafialového	ultrafialový	k2eAgMnSc2d1	ultrafialový
<g/>
,	,	kIx,	,
infračerveného	infračervený	k2eAgNnSc2d1	infračervené
a	a	k8xC	a
radiového	radiový	k2eAgNnSc2d1	radiové
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
