<s>
Její	její	k3xOp3gFnSc1	její
královská	královský	k2eAgFnSc1d1	královská
Výsost	výsost	k1gFnSc1	výsost
princezna	princezna	k1gFnSc1	princezna
Margaret	Margareta	k1gFnPc2	Margareta
<g/>
,	,	kIx,	,
hraběnka	hraběnka	k1gFnSc1	hraběnka
ze	z	k7c2	z
Snowdonu	Snowdon	k1gInSc2	Snowdon
(	(	kIx(	(
<g/>
Margaret	Margareta	k1gFnPc2	Margareta
Rose	Rose	k1gMnSc1	Rose
<g/>
;	;	kIx,	;
21	[number]	k4	21
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
Glamis	Glamis	k1gFnSc1	Glamis
<g/>
,	,	kIx,	,
Skotsko	Skotsko	k1gNnSc1	Skotsko
–	–	k?	–
9	[number]	k4	9
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
členka	členka	k1gFnSc1	členka
britské	britský	k2eAgFnSc2d1	britská
královské	královský	k2eAgFnSc2d1	královská
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
krále	král	k1gMnSc2	král
Jiřího	Jiří	k1gMnSc2	Jiří
VI	VI	kA	VI
<g/>
.	.	kIx.	.
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
manželky	manželka	k1gFnPc1	manželka
královny	královna	k1gFnSc2	královna
Alžběty	Alžběta	k1gFnSc2	Alžběta
<g/>
,	,	kIx,	,
mladší	mladý	k2eAgFnSc1d2	mladší
sestra	sestra	k1gFnSc1	sestra
královny	královna	k1gFnSc2	královna
Alžběty	Alžběta	k1gFnSc2	Alžběta
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Narodila	narodit	k5eAaPmAgFnS	narodit
se	s	k7c7	s
21	[number]	k4	21
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1930	[number]	k4	1930
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
Glamis	Glamis	k1gFnSc2	Glamis
ve	v	k7c6	v
Skotsku	Skotsko	k1gNnSc6	Skotsko
jako	jako	k8xS	jako
mladší	mladý	k2eAgMnSc1d2	mladší
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
dcer	dcera	k1gFnPc2	dcera
vévody	vévoda	k1gMnSc2	vévoda
z	z	k7c2	z
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
pozdějšího	pozdní	k2eAgMnSc2d2	pozdější
krále	král	k1gMnSc2	král
Jiřího	Jiří	k1gMnSc2	Jiří
VI	VI	kA	VI
<g/>
.	.	kIx.	.
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
manželky	manželka	k1gFnPc1	manželka
Alžběty	Alžběta	k1gFnSc2	Alžběta
Bowes-Lyon	Bowes-Lyona	k1gFnPc2	Bowes-Lyona
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
známé	známý	k2eAgFnPc1d1	známá
jako	jako	k8xS	jako
královna-matka	královnaatka	k1gFnSc1	královna-matka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokřtěna	pokřtěn	k2eAgFnSc1d1	pokřtěna
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
kapli	kaple	k1gFnSc6	kaple
Buckinghamského	buckinghamský	k2eAgInSc2d1	buckinghamský
paláce	palác	k1gInSc2	palác
<g/>
.	.	kIx.	.
</s>
<s>
Jejími	její	k3xOp3gMnPc7	její
kmotry	kmotr	k1gMnPc7	kmotr
byli	být	k5eAaImAgMnP	být
starší	starý	k2eAgMnSc1d2	starší
bratr	bratr	k1gMnSc1	bratr
jejího	její	k3xOp3gMnSc2	její
otce	otec	k1gMnSc2	otec
<g/>
,	,	kIx,	,
budoucí	budoucí	k2eAgMnSc1d1	budoucí
král	král	k1gMnSc1	král
Eduard	Eduard	k1gMnSc1	Eduard
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
a	a	k8xC	a
švédská	švédský	k2eAgFnSc1d1	švédská
princezna	princezna	k1gFnSc1	princezna
Ingrid	Ingrid	k1gFnSc1	Ingrid
<g/>
,	,	kIx,	,
budoucí	budoucí	k2eAgFnSc1d1	budoucí
dánská	dánský	k2eAgFnSc1d1	dánská
královna	královna	k1gFnSc1	královna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
se	se	k3xPyFc4	se
Margaretin	Margaretin	k2eAgMnSc1d1	Margaretin
strýc	strýc	k1gMnSc1	strýc
Eduard	Eduard	k1gMnSc1	Eduard
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
vzdal	vzdát	k5eAaPmAgMnS	vzdát
trůnu	trůn	k1gInSc2	trůn
a	a	k8xC	a
její	její	k3xOp3gMnSc1	její
otec	otec	k1gMnSc1	otec
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
králem	král	k1gMnSc7	král
jako	jako	k9	jako
Jiří	Jiří	k1gMnSc1	Jiří
VI	VI	kA	VI
<g/>
.	.	kIx.	.
</s>
<s>
Margaret	Margareta	k1gFnPc2	Margareta
byla	být	k5eAaImAgFnS	být
vychovávána	vychovávat	k5eAaImNgFnS	vychovávat
společně	společně	k6eAd1	společně
se	s	k7c7	s
sestrou	sestra	k1gFnSc7	sestra
<g/>
,	,	kIx,	,
budoucí	budoucí	k2eAgFnSc7d1	budoucí
královnou	královna	k1gFnSc7	královna
Alžbětou	Alžběta	k1gFnSc7	Alžběta
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
na	na	k7c6	na
královském	královský	k2eAgInSc6d1	královský
dvoře	dvůr	k1gInSc6	dvůr
jako	jako	k8xS	jako
následnice	následnice	k1gFnSc2	následnice
trůnu	trůn	k1gInSc2	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
všestranně	všestranně	k6eAd1	všestranně
založená	založený	k2eAgFnSc1d1	založená
–	–	k?	–
se	s	k7c7	s
zájmem	zájem	k1gInSc7	zájem
se	se	k3xPyFc4	se
věnovala	věnovat	k5eAaPmAgFnS	věnovat
jízdě	jízda	k1gFnSc6	jízda
na	na	k7c6	na
koni	kůň	k1gMnSc6	kůň
a	a	k8xC	a
plavání	plavání	k1gNnSc6	plavání
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
zahradnictví	zahradnictví	k1gNnSc2	zahradnictví
a	a	k8xC	a
především	především	k6eAd1	především
hudbě	hudba	k1gFnSc3	hudba
(	(	kIx(	(
<g/>
od	od	k7c2	od
dětství	dětství	k1gNnSc2	dětství
hrála	hrát	k5eAaImAgFnS	hrát
na	na	k7c4	na
klavír	klavír	k1gInSc4	klavír
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
pobývala	pobývat	k5eAaImAgFnS	pobývat
Margaret	Margareta	k1gFnPc2	Margareta
na	na	k7c6	na
Windsorském	windsorský	k2eAgInSc6d1	windsorský
hradě	hrad	k1gInSc6	hrad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
se	se	k3xPyFc4	se
princezna	princezna	k1gFnSc1	princezna
Margaret	Margareta	k1gFnPc2	Margareta
seznámila	seznámit	k5eAaPmAgFnS	seznámit
a	a	k8xC	a
zamilovala	zamilovat	k5eAaPmAgFnS	zamilovat
s	s	k7c7	s
kapitánem	kapitán	k1gMnSc7	kapitán
Peterem	Peter	k1gMnSc7	Peter
Townsendem	Townsend	k1gMnSc7	Townsend
<g/>
,	,	kIx,	,
hrdinou	hrdina	k1gMnSc7	hrdina
bitvy	bitva	k1gFnSc2	bitva
o	o	k7c6	o
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
,	,	kIx,	,
příslušníkem	příslušník	k1gMnSc7	příslušník
RAF	raf	k0	raf
<g/>
.	.	kIx.	.
</s>
<s>
Třebaže	třebaže	k8xS	třebaže
nebyl	být	k5eNaImAgMnS	být
šlechtického	šlechtický	k2eAgInSc2d1	šlechtický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
přidělen	přidělit	k5eAaPmNgInS	přidělit
do	do	k7c2	do
Buckinghamského	buckinghamský	k2eAgInSc2d1	buckinghamský
paláce	palác	k1gInSc2	palác
a	a	k8xC	a
do	do	k7c2	do
kruhu	kruh	k1gInSc2	kruh
královské	královský	k2eAgFnSc2d1	královská
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byl	být	k5eAaImAgInS	být
již	již	k6eAd1	již
rozveden	rozvést	k5eAaPmNgInS	rozvést
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
z	z	k7c2	z
manželství	manželství	k1gNnSc2	manželství
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
příčinou	příčina	k1gFnSc7	příčina
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gNnSc1	jeho
manželství	manželství	k1gNnSc1	manželství
s	s	k7c7	s
princeznou	princezna	k1gFnSc7	princezna
Margaret	Margareta	k1gFnPc2	Margareta
nebylo	být	k5eNaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
:	:	kIx,	:
anglikánská	anglikánský	k2eAgFnSc1d1	anglikánská
církev	církev	k1gFnSc1	církev
i	i	k8xC	i
královská	královský	k2eAgFnSc1d1	královská
tradice	tradice	k1gFnSc1	tradice
zakazovaly	zakazovat	k5eAaImAgFnP	zakazovat
sňatek	sňatek	k1gInSc4	sňatek
s	s	k7c7	s
rozvedeným	rozvedený	k1gMnSc7	rozvedený
člověkem	člověk	k1gMnSc7	člověk
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc4	ten
ostatně	ostatně	k6eAd1	ostatně
byla	být	k5eAaImAgFnS	být
i	i	k9	i
příčina	příčina	k1gFnSc1	příčina
abdikace	abdikace	k1gFnSc2	abdikace
Margaretina	Margaretin	k2eAgMnSc4d1	Margaretin
strýce	strýc	k1gMnSc4	strýc
Eduarda	Eduard	k1gMnSc2	Eduard
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
Margaret	Margareta	k1gFnPc2	Margareta
za	za	k7c4	za
kapitána	kapitán	k1gMnSc4	kapitán
Townsenda	Townsend	k1gMnSc4	Townsend
mohla	moct	k5eAaImAgFnS	moct
provdat	provdat	k5eAaPmF	provdat
<g/>
,	,	kIx,	,
musela	muset	k5eAaImAgFnS	muset
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
pravidly	pravidlo	k1gNnPc7	pravidlo
královského	královský	k2eAgInSc2d1	královský
dvora	dvůr	k1gInSc2	dvůr
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
Eduard	Eduard	k1gMnSc1	Eduard
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
vzdát	vzdát	k5eAaPmF	vzdát
svých	svůj	k3xOyFgNnPc2	svůj
práv	právo	k1gNnPc2	právo
k	k	k7c3	k
následnictví	následnictví	k1gNnSc3	následnictví
královského	královský	k2eAgInSc2d1	královský
trůnu	trůn	k1gInSc2	trůn
i	i	k8xC	i
svých	svůj	k3xOyFgInPc2	svůj
důchodů	důchod	k1gInPc2	důchod
jako	jako	k8xS	jako
královské	královský	k2eAgFnSc2d1	královská
princezny	princezna	k1gFnSc2	princezna
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
vše	všechen	k3xTgNnSc1	všechen
ovšem	ovšem	k9	ovšem
až	až	k9	až
po	po	k7c6	po
dosažení	dosažení	k1gNnSc6	dosažení
věku	věk	k1gInSc2	věk
25	[number]	k4	25
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
letech	let	k1gInPc6	let
vztahu	vztah	k1gInSc2	vztah
Margaret	Margareta	k1gFnPc2	Margareta
veřejně	veřejně	k6eAd1	veřejně
oznámila	oznámit	k5eAaPmAgFnS	oznámit
svůj	svůj	k3xOyFgInSc4	svůj
rozchod	rozchod	k1gInSc4	rozchod
s	s	k7c7	s
Peterem	Peter	k1gMnSc7	Peter
Townsendem	Townsend	k1gMnSc7	Townsend
"	"	kIx"	"
<g/>
kvůli	kvůli	k7c3	kvůli
svým	svůj	k3xOyFgFnPc3	svůj
povinnostem	povinnost	k1gFnPc3	povinnost
a	a	k8xC	a
závazkům	závazek	k1gInPc3	závazek
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
zemi	zem	k1gFnSc3	zem
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
М	М	k?	М
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
náležela	náležet	k5eAaImAgFnS	náležet
k	k	k7c3	k
londýnské	londýnský	k2eAgFnSc3d1	londýnská
smetánce	smetánka	k1gFnSc3	smetánka
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
vyhledávaným	vyhledávaný	k2eAgInSc7d1	vyhledávaný
objektem	objekt	k1gInSc7	objekt
paparazziů	paparazzi	k1gMnPc2	paparazzi
pro	pro	k7c4	pro
své	svůj	k3xOyFgNnSc4	svůj
vysoké	vysoký	k2eAgNnSc4d1	vysoké
společenské	společenský	k2eAgNnSc4d1	společenské
postavení	postavení	k1gNnSc4	postavení
a	a	k8xC	a
výstřední	výstřední	k2eAgNnSc4d1	výstřední
chování	chování	k1gNnSc4	chování
neobvyklé	obvyklý	k2eNgNnSc4d1	neobvyklé
pro	pro	k7c4	pro
člena	člen	k1gMnSc4	člen
královské	královský	k2eAgFnSc2d1	královská
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
pozorována	pozorovat	k5eAaImNgFnS	pozorovat
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
Johna	John	k1gMnSc2	John
Turnera	turner	k1gMnSc2	turner
<g/>
,	,	kIx,	,
pozdějšího	pozdní	k2eAgMnSc2d2	pozdější
premiéra	premiér	k1gMnSc2	premiér
К	К	k?	К
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
se	se	k3xPyFc4	se
provdala	provdat	k5eAaPmAgFnS	provdat
za	za	k7c2	za
známého	známý	k2eAgMnSc2d1	známý
fotografa	fotograf	k1gMnSc2	fotograf
Antonyho	Antony	k1gMnSc2	Antony
Armstrong-Jonese	Armstrong-Jonese	k1gFnSc1	Armstrong-Jonese
<g/>
,	,	kIx,	,
potomka	potomek	k1gMnSc4	potomek
nevýznamného	významný	k2eNgInSc2d1	nevýznamný
velšského	velšský	k2eAgInSc2d1	velšský
šlechtického	šlechtický	k2eAgInSc2d1	šlechtický
rodu	rod	k1gInSc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
Královna	královna	k1gFnSc1	královna
Alžběta	Alžběta	k1gFnSc1	Alžběta
ho	on	k3xPp3gInSc4	on
jmenovala	jmenovat	k5eAaBmAgFnS	jmenovat
lordem	lord	k1gMnSc7	lord
Snowdonem	Snowdon	k1gMnSc7	Snowdon
<g/>
,	,	kIx,	,
hrabětem	hrabě	k1gMnSc7	hrabě
z	z	k7c2	z
Linley	Linlea	k1gFnSc2	Linlea
<g/>
;	;	kIx,	;
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
princezna	princezna	k1gFnSc1	princezna
Margaret	Margareta	k1gFnPc2	Margareta
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
oficiálně	oficiálně	k6eAd1	oficiálně
jako	jako	k8xS	jako
hraběnka	hraběnka	k1gFnSc1	hraběnka
Snowdon	Snowdona	k1gFnPc2	Snowdona
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
manželství	manželství	k1gNnSc2	manželství
se	se	k3xPyFc4	se
narodily	narodit	k5eAaPmAgFnP	narodit
dvě	dva	k4xCgFnPc1	dva
děti	dítě	k1gFnPc1	dítě
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
a	a	k8xC	a
dcera	dcera	k1gFnSc1	dcera
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgInPc3	jenž
byla	být	k5eAaImAgFnS	být
oddanou	oddaný	k2eAgFnSc7d1	oddaná
matkou	matka	k1gFnSc7	matka
<g/>
:	:	kIx,	:
David	David	k1gMnSc1	David
Armstrong-Jones	Armstrong-Jones	k1gMnSc1	Armstrong-Jones
(	(	kIx(	(
<g/>
vikomt	vikomt	k1gMnSc1	vikomt
Linley	Linlea	k1gFnSc2	Linlea
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Sarah	Sarah	k1gFnSc1	Sarah
Chatto	Chatto	k1gNnSc4	Chatto
(	(	kIx(	(
<g/>
lady	lad	k1gInPc1	lad
Chatto	Chatto	k1gNnSc1	Chatto
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Manželství	manželství	k1gNnSc1	manželství
po	po	k7c6	po
dvouleté	dvouletý	k2eAgFnSc6d1	dvouletá
zkušební	zkušební	k2eAgFnSc6d1	zkušební
době	doba	k1gFnSc6	doba
roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
skočilo	skočit	k5eAaPmAgNnS	skočit
rozvodem	rozvod	k1gInSc7	rozvod
<g/>
.	.	kIx.	.
</s>
<s>
Princezna	princezna	k1gFnSc1	princezna
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
již	již	k6eAd1	již
znovu	znovu	k6eAd1	znovu
neprovdala	provdat	k5eNaPmAgFnS	provdat
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
bohatého	bohatý	k2eAgInSc2d1	bohatý
a	a	k8xC	a
pestrého	pestrý	k2eAgInSc2d1	pestrý
života	život	k1gInSc2	život
hrála	hrát	k5eAaImAgFnS	hrát
princezna	princezna	k1gFnSc1	princezna
aktivní	aktivní	k2eAgFnSc4d1	aktivní
roli	role	k1gFnSc4	role
ve	v	k7c6	v
veřejné	veřejný	k2eAgFnSc6d1	veřejná
činnosti	činnost	k1gFnSc6	činnost
královské	královský	k2eAgFnSc2d1	královská
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
zastupujíc	zastupovat	k5eAaImSgNnS	zastupovat
často	často	k6eAd1	často
královnu	královna	k1gFnSc4	královna
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
členka	členka	k1gFnSc1	členka
královské	královský	k2eAgFnSc2d1	královská
rodiny	rodina	k1gFnSc2	rodina
byla	být	k5eAaImAgFnS	být
patronkou	patronka	k1gFnSc7	patronka
nebo	nebo	k8xC	nebo
předsedkyní	předsedkyně	k1gFnSc7	předsedkyně
řady	řada	k1gFnSc2	řada
(	(	kIx(	(
<g/>
více	hodně	k6eAd2	hodně
než	než	k8xS	než
80	[number]	k4	80
<g/>
)	)	kIx)	)
institucí	instituce	k1gFnPc2	instituce
a	a	k8xC	a
organizací	organizace	k1gFnPc2	organizace
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
dobročinných	dobročinný	k2eAgInPc6d1	dobročinný
či	či	k8xC	či
uměleckých	umělecký	k2eAgInPc6d1	umělecký
<g/>
,	,	kIx,	,
m.	m.	k?	m.
<g/>
j.	j.	k?	j.
Rady	rada	k1gFnSc2	rada
spolků	spolek	k1gInPc2	spolek
pro	pro	k7c4	pro
výchovu	výchova	k1gFnSc4	výchova
dívek	dívka	k1gFnPc2	dívka
<g/>
,	,	kIx,	,
Národního	národní	k2eAgInSc2d1	národní
a	a	k8xC	a
skotského	skotský	k2eAgInSc2d1	skotský
spolku	spolek	k1gInSc2	spolek
ochrany	ochrana	k1gFnSc2	ochrana
dětí	dítě	k1gFnPc2	dítě
proti	proti	k7c3	proti
násilí	násilí	k1gNnSc3	násilí
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
<g/>
,	,	kIx,	,
Královského	královský	k2eAgInSc2d1	královský
baletu	balet	k1gInSc2	balet
(	(	kIx(	(
<g/>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dostala	dostat	k5eAaPmAgFnS	dostat
doktorát	doktorát	k1gInSc4	doktorát
honoris	honoris	k1gFnSc1	honoris
causa	causa	k1gFnSc1	causa
z	z	k7c2	z
hudby	hudba	k1gFnSc2	hudba
na	na	k7c6	na
Londýnské	londýnský	k2eAgFnSc6d1	londýnská
univerzitě	univerzita	k1gFnSc6	univerzita
a	a	k8xC	a
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Cambridgi	Cambridge	k1gFnSc6	Cambridge
a	a	k8xC	a
v	v	k7c6	v
Keele	Keela	k1gFnSc6	Keela
<g/>
.	.	kIx.	.
</s>
<s>
Měla	mít	k5eAaImAgFnS	mít
i	i	k8xC	i
čestné	čestný	k2eAgFnSc2d1	čestná
funkce	funkce	k1gFnSc2	funkce
vojenské	vojenský	k2eAgFnSc2d1	vojenská
<g/>
,	,	kIx,	,
m.	m.	k?	m.
<g/>
j.	j.	k?	j.
komandéra	komandér	k1gMnSc2	komandér
leteckých	letecký	k2eAgFnPc2d1	letecká
sil	síla	k1gFnPc2	síla
a	a	k8xC	a
velitele	velitel	k1gMnSc2	velitel
Pěšího	pěší	k2eAgInSc2d1	pěší
korpusu	korpus	k1gInSc2	korpus
královské	královský	k2eAgFnSc2d1	královská
armády	armáda	k1gFnSc2	armáda
královny	královna	k1gFnSc2	královna
Alexandry	Alexandra	k1gFnSc2	Alexandra
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
dámou	dáma	k1gFnSc7	dáma
nejvýznamnějších	významný	k2eAgInPc2d3	nejvýznamnější
britských	britský	k2eAgInPc2d1	britský
i	i	k8xC	i
zahraničních	zahraniční	k2eAgInPc2d1	zahraniční
řádů	řád	k1gInPc2	řád
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Velkého	velký	k2eAgInSc2d1	velký
kříže	kříž	k1gInSc2	kříž
královny	královna	k1gFnSc2	královna
Viktorie	Viktoria	k1gFnSc2	Viktoria
(	(	kIx(	(
<g/>
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kříže	kříž	k1gInSc2	kříž
velkého	velký	k2eAgInSc2d1	velký
řádu	řád	k1gInSc2	řád
Belgického	belgický	k2eAgNnSc2d1	Belgické
království	království	k1gNnSc2	království
(	(	kIx(	(
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kříže	kříž	k1gInSc2	kříž
Velkého	velký	k2eAgInSc2d1	velký
řádu	řád	k1gInSc2	řád
holandského	holandský	k2eAgMnSc4d1	holandský
lva	lev	k1gMnSc4	lev
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Řádu	řád	k1gInSc2	řád
Briliantové	briliantový	k2eAgFnPc1d1	briliantová
hvězdy	hvězda	k1gFnPc1	hvězda
Zanzibaru	Zanzibar	k1gInSc2	Zanzibar
I.	I.	kA	I.
třídy	třída	k1gFnSc2	třída
(	(	kIx(	(
<g/>
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Řádu	řád	k1gInSc3	řád
Koruny	koruna	k1gFnSc2	koruna
<g/>
,	,	kIx,	,
lva	lev	k1gMnSc2	lev
a	a	k8xC	a
vlajky	vlajka	k1gFnPc1	vlajka
království	království	k1gNnSc2	království
Toro	Toro	k?	Toro
(	(	kIx(	(
<g/>
Uganda	Uganda	k1gFnSc1	Uganda
<g/>
,	,	kIx,	,
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Řádu	řád	k1gInSc2	řád
Koruny	koruna	k1gFnSc2	koruna
I.	I.	kA	I.
třídy	třída	k1gFnSc2	třída
(	(	kIx(	(
<g/>
Japonsko	Japonsko	k1gNnSc1	Japonsko
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
sídlo	sídlo	k1gNnSc4	sídlo
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
Kensingtonském	Kensingtonský	k2eAgInSc6d1	Kensingtonský
paláci	palác	k1gInSc6	palác
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
М	М	k?	М
nazývali	nazývat	k5eAaImAgMnP	nazývat
"	"	kIx"	"
<g/>
buřičskou	buřičský	k2eAgFnSc7d1	buřičská
princeznou	princezna	k1gFnSc7	princezna
<g/>
"	"	kIx"	"
pro	pro	k7c4	pro
její	její	k3xOp3gNnSc4	její
skandální	skandální	k2eAgNnSc4d1	skandální
chování	chování	k1gNnSc4	chování
<g/>
;	;	kIx,	;
byla	být	k5eAaImAgFnS	být
stálým	stálý	k2eAgMnSc7d1	stálý
hostem	host	k1gMnSc7	host
londýnských	londýnský	k2eAgInPc2d1	londýnský
nočních	noční	k2eAgInPc2d1	noční
klubů	klub	k1gInPc2	klub
a	a	k8xC	a
ráda	rád	k2eAgFnSc1d1	ráda
se	se	k3xPyFc4	se
objevovala	objevovat	k5eAaImAgFnS	objevovat
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
rockerů	rocker	k1gMnPc2	rocker
<g/>
,	,	kIx,	,
se	s	k7c7	s
skleničkou	sklenička	k1gFnSc7	sklenička
tvrdého	tvrdý	k2eAgInSc2d1	tvrdý
alkoholu	alkohol	k1gInSc2	alkohol
a	a	k8xC	a
dlouhou	dlouhý	k2eAgFnSc7d1	dlouhá
cigaretovou	cigaretový	k2eAgFnSc7d1	cigaretová
špičkou	špička	k1gFnSc7	špička
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Média	médium	k1gNnPc1	médium
tvrdila	tvrdit	k5eAaImAgNnP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
kouřila	kouřit	k5eAaImAgFnS	kouřit
60	[number]	k4	60
cigaret	cigareta	k1gFnPc2	cigareta
denně	denně	k6eAd1	denně
a	a	k8xC	a
milovala	milovat	k5eAaImAgFnS	milovat
gin	gin	k1gInSc4	gin
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
u	u	k7c2	u
ní	on	k3xPp3gFnSc2	on
objevily	objevit	k5eAaPmAgFnP	objevit
vážné	vážný	k2eAgInPc4d1	vážný
zdravotní	zdravotní	k2eAgInPc4d1	zdravotní
problémy	problém	k1gInPc4	problém
a	a	k8xC	a
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
života	život	k1gInSc2	život
byla	být	k5eAaImAgFnS	být
těžce	těžce	k6eAd1	těžce
nemocná	mocný	k2eNgFnSc1d1	mocný
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
měla	mít	k5eAaImAgFnS	mít
slabou	slabý	k2eAgFnSc4d1	slabá
mozkovou	mozkový	k2eAgFnSc4d1	mozková
mrtvici	mrtvice	k1gFnSc4	mrtvice
<g/>
,	,	kIx,	,
nedlouho	dlouho	k6eNd1	dlouho
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
zhroutila	zhroutit	k5eAaPmAgFnS	zhroutit
<g/>
;	;	kIx,	;
následovalo	následovat	k5eAaImAgNnS	následovat
trvalé	trvalý	k2eAgNnSc1d1	trvalé
poškození	poškození	k1gNnSc1	poškození
hybnosti	hybnost	k1gFnSc2	hybnost
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
mrtvice	mrtvice	k1gFnPc1	mrtvice
následovaly	následovat	k5eAaImAgFnP	následovat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2000	[number]	k4	2000
a	a	k8xC	a
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Naposledy	naposledy	k6eAd1	naposledy
se	se	k3xPyFc4	se
veřejně	veřejně	k6eAd1	veřejně
ukázala	ukázat	k5eAaPmAgFnS	ukázat
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
na	na	k7c6	na
oslavách	oslava	k1gFnPc6	oslava
stých	stý	k4xOgInPc2	stý
narozenin	narozeniny	k1gFnPc2	narozeniny
své	svůj	k3xOyFgFnSc2	svůj
tety	teta	k1gFnSc2	teta
Alice	Alice	k1gFnSc2	Alice
<g/>
,	,	kIx,	,
vévodkyně	vévodkyně	k1gFnSc2	vévodkyně
z	z	k7c2	z
Gloucesteru	Gloucester	k1gInSc2	Gloucester
<g/>
.	.	kIx.	.
</s>
<s>
Zemřela	zemřít	k5eAaPmAgFnS	zemřít
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
72	[number]	k4	72
let	léto	k1gNnPc2	léto
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
krále	král	k1gMnSc4	král
Eduarda	Eduard	k1gMnSc2	Eduard
VII	VII	kA	VII
<g/>
.	.	kIx.	.
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
na	na	k7c4	na
mozkovou	mozkový	k2eAgFnSc4d1	mozková
mrtvici	mrtvice	k1gFnSc4	mrtvice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
stála	stát	k5eAaImAgFnS	stát
v	v	k7c4	v
pořadí	pořadí	k1gNnSc4	pořadí
následníků	následník	k1gMnPc2	následník
své	svůj	k3xOyFgFnSc2	svůj
sestry	sestra	k1gFnSc2	sestra
Alžběty	Alžběta	k1gFnSc2	Alžběta
na	na	k7c6	na
jedenáctém	jedenáctý	k4xOgNnSc6	jedenáctý
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
byl	být	k5eAaImAgMnS	být
o	o	k7c6	o
jejím	její	k3xOp3gInSc6	její
životě	život	k1gInSc6	život
natočen	natočit	k5eAaBmNgInS	natočit
film	film	k1gInSc1	film
"	"	kIx"	"
<g/>
Princess	Princess	k1gInSc1	Princess
Margaret	Margareta	k1gFnPc2	Margareta
<g/>
,	,	kIx,	,
a	a	k8xC	a
love	lov	k1gInSc5	lov
story	story	k1gFnSc1	story
(	(	kIx(	(
<g/>
Princezna	princezna	k1gFnSc1	princezna
Margaret	Margareta	k1gFnPc2	Margareta
–	–	k?	–
příběh	příběh	k1gInSc1	příběh
lásky	láska	k1gFnSc2	láska
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
založený	založený	k2eAgInSc1d1	založený
na	na	k7c6	na
řadě	řada	k1gFnSc6	řada
kompromitujících	kompromitující	k2eAgInPc2d1	kompromitující
klepů	klep	k1gInPc2	klep
<g/>
.	.	kIx.	.
</s>
<s>
Zápletka	zápletka	k1gFnSc1	zápletka
dalšího	další	k2eAgInSc2d1	další
filmu	film	k1gInSc2	film
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Bank	bank	k1gInSc1	bank
Job	Job	k1gMnSc1	Job
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
postav	postava	k1gFnPc2	postava
filmu	film	k1gInSc2	film
má	mít	k5eAaImIp3nS	mít
kompromitující	kompromitující	k2eAgInSc4d1	kompromitující
materiály	materiál	k1gInPc4	materiál
na	na	k7c4	na
princeznu	princezna	k1gFnSc4	princezna
Margaret	Margareta	k1gFnPc2	Margareta
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc1	jejichž
zveřejnění	zveřejnění	k1gNnSc1	zveřejnění
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
vyvolat	vyvolat	k5eAaPmF	vyvolat
sexuální	sexuální	k2eAgInSc4d1	sexuální
skandál	skandál	k1gInSc4	skandál
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Margaret	Margareta	k1gFnPc2	Margareta
<g/>
,	,	kIx,	,
hraběnka	hraběnka	k1gFnSc1	hraběnka
Snowdon	Snowdona	k1gFnPc2	Snowdona
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Osoba	osoba	k1gFnSc1	osoba
Margareta	Margareta	k1gFnSc1	Margareta
<g/>
,	,	kIx,	,
hraběnka	hraběnka	k1gFnSc1	hraběnka
ze	z	k7c2	z
Snowdonu	Snowdon	k1gInSc2	Snowdon
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
http://thepeerage.com/p10070.htm#i100700	[url]	k4	http://thepeerage.com/p10070.htm#i100700
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
