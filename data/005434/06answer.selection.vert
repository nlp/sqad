<s>
Její	její	k3xOp3gFnSc1	její
královská	královský	k2eAgFnSc1d1	královská
Výsost	výsost	k1gFnSc1	výsost
princezna	princezna	k1gFnSc1	princezna
Margaret	Margareta	k1gFnPc2	Margareta
<g/>
,	,	kIx,	,
hraběnka	hraběnka	k1gFnSc1	hraběnka
ze	z	k7c2	z
Snowdonu	Snowdon	k1gInSc2	Snowdon
(	(	kIx(	(
<g/>
Margaret	Margareta	k1gFnPc2	Margareta
Rose	Rose	k1gMnSc1	Rose
<g/>
;	;	kIx,	;
21	[number]	k4	21
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
Glamis	Glamis	k1gFnSc1	Glamis
<g/>
,	,	kIx,	,
Skotsko	Skotsko	k1gNnSc1	Skotsko
–	–	k?	–
9	[number]	k4	9
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
členka	členka	k1gFnSc1	členka
britské	britský	k2eAgFnSc2d1	britská
královské	královský	k2eAgFnSc2d1	královská
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
krále	král	k1gMnSc2	král
Jiřího	Jiří	k1gMnSc2	Jiří
VI	VI	kA	VI
<g/>
.	.	kIx.	.
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
manželky	manželka	k1gFnPc1	manželka
královny	královna	k1gFnSc2	královna
Alžběty	Alžběta	k1gFnSc2	Alžběta
<g/>
,	,	kIx,	,
mladší	mladý	k2eAgFnSc1d2	mladší
sestra	sestra	k1gFnSc1	sestra
královny	královna	k1gFnSc2	královna
Alžběty	Alžběta	k1gFnSc2	Alžběta
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
