<p>
<s>
Michal	Michal	k1gMnSc1	Michal
Hrůza	Hrůza	k1gMnSc1	Hrůza
(	(	kIx(	(
<g/>
*	*	kIx~	*
31	[number]	k4	31
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
Turnov	Turnov	k1gInSc1	Turnov
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
zpěvák	zpěvák	k1gMnSc1	zpěvák
a	a	k8xC	a
zakládající	zakládající	k2eAgMnSc1d1	zakládající
člen	člen	k1gMnSc1	člen
skupiny	skupina	k1gFnSc2	skupina
Ready	ready	k0	ready
Kirken	Kirken	k1gInSc1	Kirken
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hudbě	hudba	k1gFnSc3	hudba
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgMnS	začít
věnovat	věnovat	k5eAaPmF	věnovat
na	na	k7c6	na
střední	střední	k2eAgFnSc6d1	střední
vojenské	vojenský	k2eAgFnSc6d1	vojenská
škole	škola	k1gFnSc6	škola
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
školní	školní	k2eAgFnSc2d1	školní
hudební	hudební	k2eAgFnSc2d1	hudební
skupiny	skupina	k1gFnSc2	skupina
a	a	k8xC	a
kde	kde	k6eAd1	kde
hrál	hrát	k5eAaImAgMnS	hrát
amatérské	amatérský	k2eAgNnSc4d1	amatérské
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
maturitě	maturita	k1gFnSc6	maturita
založil	založit	k5eAaPmAgMnS	založit
punk	punk	k1gMnSc1	punk
rockovou	rockový	k2eAgFnSc4d1	rocková
kapelu	kapela	k1gFnSc4	kapela
Brambory	brambora	k1gFnSc2	brambora
a	a	k8xC	a
hard	hard	k6eAd1	hard
rockové	rockový	k2eAgNnSc1d1	rockové
trio	trio	k1gNnSc1	trio
51	[number]	k4	51
<g/>
.	.	kIx.	.
chvíle	chvíle	k1gFnSc2	chvíle
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
se	se	k3xPyFc4	se
odstěhoval	odstěhovat	k5eAaPmAgInS	odstěhovat
do	do	k7c2	do
Pardubic	Pardubice	k1gInPc2	Pardubice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hrál	hrát	k5eAaImAgMnS	hrát
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
Anachronic	Anachronice	k1gFnPc2	Anachronice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
založil	založit	k5eAaPmAgMnS	založit
skupinu	skupina	k1gFnSc4	skupina
Ready	ready	k0	ready
Kirken	Kirkno	k1gNnPc2	Kirkno
<g/>
,	,	kIx,	,
s	s	k7c7	s
kterou	který	k3yQgFnSc7	který
natočil	natočit	k5eAaBmAgMnS	natočit
pět	pět	k4xCc4	pět
hudebních	hudební	k2eAgNnPc2d1	hudební
alb	album	k1gNnPc2	album
a	a	k8xC	a
napsal	napsat	k5eAaBmAgInS	napsat
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
více	hodně	k6eAd2	hodně
než	než	k8xS	než
padesát	padesát	k4xCc4	padesát
písní	píseň	k1gFnPc2	píseň
a	a	k8xC	a
textů	text	k1gInPc2	text
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
skladba	skladba	k1gFnSc1	skladba
pro	pro	k7c4	pro
Anetu	Aneta	k1gFnSc4	Aneta
Langerovou	Langerová	k1gFnSc4	Langerová
Voda	voda	k1gFnSc1	voda
živá	živá	k1gFnSc1	živá
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2005	[number]	k4	2005
a	a	k8xC	a
2006	[number]	k4	2006
hitem	hit	k1gInSc7	hit
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
byl	být	k5eAaImAgMnS	být
z	z	k7c2	z
kapely	kapela	k1gFnSc2	kapela
vyloučen	vyloučit	k5eAaPmNgMnS	vyloučit
a	a	k8xC	a
tak	tak	k6eAd1	tak
stál	stát	k5eAaImAgMnS	stát
u	u	k7c2	u
zrodu	zrod	k1gInSc2	zrod
jeho	jeho	k3xOp3gFnSc2	jeho
nové	nový	k2eAgFnSc2d1	nová
skupiny	skupina	k1gFnSc2	skupina
Kapely	kapela	k1gFnSc2	kapela
Hrůzy	hrůza	k1gFnSc2	hrůza
<g/>
.	.	kIx.	.
</s>
<s>
Natočil	natočit	k5eAaBmAgMnS	natočit
album	album	k1gNnSc4	album
Bílá	bílý	k2eAgFnSc1d1	bílá
velryba	velryba	k1gFnSc1	velryba
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Napořád	napořád	k6eAd1	napořád
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
a	a	k8xC	a
Noc	noc	k1gFnSc1	noc
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podílel	podílet	k5eAaImAgInS	podílet
se	se	k3xPyFc4	se
i	i	k9	i
na	na	k7c6	na
hudbě	hudba	k1gFnSc6	hudba
k	k	k7c3	k
filmu	film	k1gInSc6	film
Lidice	Lidice	k1gInPc1	Lidice
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
a	a	k8xC	a
filmu	film	k1gInSc2	film
Martin	Martin	k1gMnSc1	Martin
a	a	k8xC	a
Venuše	Venuše	k1gFnSc1	Venuše
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
i	i	k8xC	i
mnoha	mnoho	k4c2	mnoho
televizních	televizní	k2eAgFnPc2d1	televizní
znělek	znělka	k1gFnPc2	znělka
(	(	kIx(	(
<g/>
seriál	seriál	k1gInSc1	seriál
Vyprávěj	vyprávět	k5eAaImRp2nS	vyprávět
<g/>
,	,	kIx,	,
Mazalové	Mazalové	k2eAgMnSc4d1	Mazalové
<g/>
,	,	kIx,	,
Mistr	mistr	k1gMnSc1	mistr
E	E	kA	E
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zdravotní	zdravotní	k2eAgInSc1d1	zdravotní
stav	stav	k1gInSc1	stav
==	==	k?	==
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2014	[number]	k4	2014
při	při	k7c6	při
potyčce	potyčka	k1gFnSc6	potyčka
v	v	k7c6	v
ostravské	ostravský	k2eAgFnSc6d1	Ostravská
Stodolní	stodolní	k2eAgFnSc6d1	stodolní
ulici	ulice	k1gFnSc6	ulice
utrpěl	utrpět	k5eAaPmAgInS	utrpět
vážný	vážný	k2eAgInSc1d1	vážný
úraz	úraz	k1gInSc1	úraz
hlavy	hlava	k1gFnSc2	hlava
provázený	provázený	k2eAgInSc1d1	provázený
krvácením	krvácení	k1gNnSc7	krvácení
do	do	k7c2	do
mozku	mozek	k1gInSc2	mozek
<g/>
,	,	kIx,	,
po	po	k7c6	po
němž	jenž	k3xRgInSc6	jenž
zůstal	zůstat	k5eAaPmAgInS	zůstat
v	v	k7c6	v
bezvědomí	bezvědomí	k1gNnSc6	bezvědomí
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
byl	být	k5eAaImAgInS	být
operován	operovat	k5eAaImNgInS	operovat
v	v	k7c6	v
Městské	městský	k2eAgFnSc6d1	městská
nemocnici	nemocnice	k1gFnSc6	nemocnice
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
<g/>
,	,	kIx,	,
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
do	do	k7c2	do
30	[number]	k4	30
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
umělém	umělý	k2eAgInSc6d1	umělý
spánku	spánek	k1gInSc6	spánek
<g/>
.	.	kIx.	.
</s>
<s>
Událost	událost	k1gFnSc1	událost
je	být	k5eAaImIp3nS	být
kvalifikována	kvalifikovat	k5eAaBmNgFnS	kvalifikovat
jako	jako	k8xC	jako
těžké	těžký	k2eAgNnSc1d1	těžké
ublížení	ublížení	k1gNnSc1	ublížení
na	na	k7c6	na
zdraví	zdraví	k1gNnSc6	zdraví
<g/>
,	,	kIx,	,
dvojici	dvojice	k1gFnSc6	dvojice
zadržených	zadržený	k2eAgMnPc2d1	zadržený
mladých	mladý	k2eAgMnPc2d1	mladý
studentů	student	k1gMnPc2	student
z	z	k7c2	z
Ostravy	Ostrava	k1gFnSc2	Ostrava
za	za	k7c4	za
ně	on	k3xPp3gMnPc4	on
hrozí	hrozit	k5eAaImIp3nS	hrozit
5	[number]	k4	5
až	až	k9	až
12	[number]	k4	12
let	léto	k1gNnPc2	léto
vězení	vězení	k1gNnSc2	vězení
<g/>
.	.	kIx.	.
</s>
<s>
Mladíci	mladík	k1gMnPc1	mladík
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnPc1	jejichž
rvačce	rvačka	k1gFnSc6	rvačka
chtěl	chtít	k5eAaImAgMnS	chtít
zpěvák	zpěvák	k1gMnSc1	zpěvák
zabránit	zabránit	k5eAaPmF	zabránit
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
obviněni	obvinit	k5eAaPmNgMnP	obvinit
z	z	k7c2	z
vydírání	vydírání	k1gNnSc2	vydírání
a	a	k8xC	a
výtržnictví	výtržnictví	k1gNnSc2	výtržnictví
a	a	k8xC	a
následně	následně	k6eAd1	následně
vzati	vzít	k5eAaPmNgMnP	vzít
do	do	k7c2	do
vazby	vazba	k1gFnSc2	vazba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
1996	[number]	k4	1996
Ready	ready	k0	ready
Kirken	Kirkna	k1gFnPc2	Kirkna
</s>
</p>
<p>
<s>
2001	[number]	k4	2001
Vlny	vlna	k1gFnPc1	vlna
</s>
</p>
<p>
<s>
2002	[number]	k4	2002
Čekal	čekat	k5eAaImAgMnS	čekat
jsem	být	k5eAaImIp1nS	být
víc	hodně	k6eAd2	hodně
</s>
</p>
<p>
<s>
2004	[number]	k4	2004
Krasohled	krasohled	k1gInSc1	krasohled
</s>
</p>
<p>
<s>
2006	[number]	k4	2006
Asi	asi	k9	asi
se	se	k3xPyFc4	se
něco	něco	k3yInSc1	něco
děje	dít	k5eAaImIp3nS	dít
</s>
</p>
<p>
<s>
2006	[number]	k4	2006
12	[number]	k4	12
NEJMichal	NEJMichal	k1gFnPc2	NEJMichal
Hrůza	hrůza	k6eAd1	hrůza
a	a	k8xC	a
KAPELA	kapela	k1gFnSc1	kapela
HRŮZY	hrůza	k1gFnSc2	hrůza
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Bílá	bílý	k2eAgFnSc1d1	bílá
velryba	velryba	k1gFnSc1	velryba
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Napořád	napořád	k6eAd1	napořád
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
film	film	k1gInSc1	film
Lidice	Lidice	k1gInPc1	Lidice
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Noc	noc	k1gFnSc1	noc
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
film	film	k1gInSc1	film
Martin	Martin	k1gMnSc1	Martin
a	a	k8xC	a
Venuše	Venuše	k1gFnSc1	Venuše
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Den	den	k1gInSc1	den
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
píseň	píseň	k1gFnSc1	píseň
k	k	k7c3	k
filmu	film	k1gInSc3	film
Zakázané	zakázaný	k2eAgNnSc1d1	zakázané
uvolnění	uvolnění	k1gNnSc1	uvolnění
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Padesátka	padesátka	k1gFnSc1	padesátka
-	-	kIx~	-
pro	pro	k7c4	pro
film	film	k1gInSc4	film
Padesátka	padesátka	k1gFnSc1	padesátka
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sám	sám	k3xTgMnSc1	sám
se	s	k7c7	s
sebou	se	k3xPyFc7	se
(	(	kIx(	(
<g/>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
V	v	k7c6	v
Rudolfinu	Rudolfinum	k1gNnSc6	Rudolfinum
-	-	kIx~	-
Live	Live	k1gNnSc1	Live
(	(	kIx(	(
<g/>
2018	[number]	k4	2018
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
píseň	píseň	k1gFnSc4	píseň
Za	za	k7c4	za
100	[number]	k4	100
let	léto	k1gNnPc2	léto
</s>
</p>
<p>
<s>
píseň	píseň	k1gFnSc1	píseň
k	k	k7c3	k
filmu	film	k1gInSc2	film
Ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
tě	ty	k3xPp2nSc4	ty
miloval	milovat	k5eAaImAgInS	milovat
(	(	kIx(	(
<g/>
společně	společně	k6eAd1	společně
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Anna	Anna	k1gFnSc1	Anna
K	k	k7c3	k
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
http://revue.idnes.cz/michal-hruza-c9g-/lidicky.aspx?c=A170524_143501_lidicky_zar	[url]	k1gMnSc1	http://revue.idnes.cz/michal-hruza-c9g-/lidicky.aspx?c=A170524_143501_lidicky_zar
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Michal	Michala	k1gFnPc2	Michala
Hrůza	hrůza	k6eAd1	hrůza
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Michal	Michal	k1gMnSc1	Michal
Hrůza	Hrůza	k1gMnSc1	Hrůza
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
