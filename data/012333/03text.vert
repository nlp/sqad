<p>
<s>
Objektiv	objektiv	k1gInSc1	objektiv
je	být	k5eAaImIp3nS	být
čočka	čočka	k1gFnSc1	čočka
nebo	nebo	k8xC	nebo
soustava	soustava	k1gFnSc1	soustava
čoček	čočka	k1gFnPc2	čočka
<g/>
,	,	kIx,	,
vytvářející	vytvářející	k2eAgInSc1d1	vytvářející
opticky	opticky	k6eAd1	opticky
změněný	změněný	k2eAgInSc1d1	změněný
obraz	obraz	k1gInSc1	obraz
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
ještě	ještě	k6eAd1	ještě
dále	daleko	k6eAd2	daleko
zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
(	(	kIx(	(
<g/>
záznamem	záznam	k1gInSc7	záznam
<g/>
,	,	kIx,	,
okulárem	okulár	k1gInSc7	okulár
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
například	například	k6eAd1	například
ve	v	k7c6	v
fotoaparátu	fotoaparát	k1gInSc6	fotoaparát
k	k	k7c3	k
soustředění	soustředění	k1gNnSc3	soustředění
světla	světlo	k1gNnSc2	světlo
na	na	k7c4	na
senzor	senzor	k1gInSc4	senzor
nebo	nebo	k8xC	nebo
na	na	k7c4	na
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
objektivy	objektiv	k1gInPc7	objektiv
fotoaparátu	fotoaparát	k1gInSc2	fotoaparát
<g/>
,	,	kIx,	,
kamery	kamera	k1gFnSc2	kamera
<g/>
,	,	kIx,	,
dalekohledu	dalekohled	k1gInSc2	dalekohled
<g/>
,	,	kIx,	,
mikroskopu	mikroskop	k1gInSc2	mikroskop
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
optických	optický	k2eAgNnPc2d1	optické
zařízení	zařízení	k1gNnPc2	zařízení
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
principu	princip	k1gInSc6	princip
rozdíl	rozdíl	k1gInSc4	rozdíl
<g/>
,	,	kIx,	,
liší	lišit	k5eAaImIp3nS	lišit
se	se	k3xPyFc4	se
ale	ale	k9	ale
svou	svůj	k3xOyFgFnSc7	svůj
konstrukcí	konstrukce	k1gFnSc7	konstrukce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obecně	obecně	k6eAd1	obecně
==	==	k?	==
</s>
</p>
<p>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
jako	jako	k9	jako
primitivní	primitivní	k2eAgInSc1d1	primitivní
objektiv	objektiv	k1gInSc1	objektiv
poslouží	posloužit	k5eAaPmIp3nS	posloužit
jakákoli	jakýkoli	k3yIgFnSc1	jakýkoli
spojná	spojný	k2eAgFnSc1d1	spojná
čočka	čočka	k1gFnSc1	čočka
<g/>
,	,	kIx,	,
či	či	k8xC	či
dokonce	dokonce	k9	dokonce
pouhý	pouhý	k2eAgInSc4d1	pouhý
otvor	otvor	k1gInSc4	otvor
v	v	k7c6	v
neprůsvitném	průsvitný	k2eNgInSc6d1	neprůsvitný
materiálu	materiál	k1gInSc6	materiál
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
camera	camera	k1gFnSc1	camera
obscura	obscura	k1gFnSc1	obscura
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
optické	optický	k2eAgFnPc1d1	optická
soustavy	soustava	k1gFnPc1	soustava
několika	několik	k4yIc2	několik
různých	různý	k2eAgMnPc2d1	různý
druhů	druh	k1gMnPc2	druh
čoček	čočka	k1gFnPc2	čočka
kvůli	kvůli	k7c3	kvůli
potlačení	potlačení	k1gNnSc3	potlačení
různých	různý	k2eAgFnPc2d1	různá
optických	optický	k2eAgFnPc2d1	optická
vad	vada	k1gFnPc2	vada
<g/>
.	.	kIx.	.
</s>
<s>
Taková	takový	k3xDgFnSc1	takový
optická	optický	k2eAgFnSc1d1	optická
soustava	soustava	k1gFnSc1	soustava
pak	pak	k6eAd1	pak
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
schopna	schopen	k2eAgFnSc1d1	schopna
i	i	k8xC	i
měnit	měnit	k5eAaImF	měnit
svoji	svůj	k3xOyFgFnSc4	svůj
ohniskovou	ohniskový	k2eAgFnSc4d1	ohnisková
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
–	–	k?	–
takzvaně	takzvaně	k6eAd1	takzvaně
"	"	kIx"	"
<g/>
zoomovat	zoomovat	k5eAaBmF	zoomovat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
objektivech	objektiv	k1gInPc6	objektiv
fotoaparátů	fotoaparát	k1gInPc2	fotoaparát
také	také	k6eAd1	také
bývá	bývat	k5eAaImIp3nS	bývat
zabudována	zabudován	k2eAgFnSc1d1	zabudována
clona	clona	k1gFnSc1	clona
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
dovoluje	dovolovat	k5eAaImIp3nS	dovolovat
regulovat	regulovat	k5eAaImF	regulovat
množství	množství	k1gNnSc4	množství
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
objektivem	objektiv	k1gInSc7	objektiv
prochází	procházet	k5eAaImIp3nS	procházet
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
objektivu	objektiv	k1gInSc2	objektiv
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
i	i	k9	i
závěrka	závěrka	k1gFnSc1	závěrka
(	(	kIx(	(
<g/>
v	v	k7c6	v
případě	případ	k1gInSc6	případ
velkoformátových	velkoformátový	k2eAgInPc2d1	velkoformátový
nebo	nebo	k8xC	nebo
některých	některý	k3yIgInPc2	některý
kompaktních	kompaktní	k2eAgInPc2d1	kompaktní
fotoaparátů	fotoaparát	k1gInPc2	fotoaparát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
však	však	k9	však
bývá	bývat	k5eAaImIp3nS	bývat
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
fotoaparátu	fotoaparát	k1gInSc2	fotoaparát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
své	svůj	k3xOyFgFnSc2	svůj
ohniskové	ohniskový	k2eAgFnSc2d1	ohnisková
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
spíše	spíše	k9	spíše
úhlu	úhel	k1gInSc2	úhel
záběru	záběr	k1gInSc2	záběr
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
fotografické	fotografický	k2eAgInPc1d1	fotografický
objektivy	objektiv	k1gInPc1	objektiv
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
základní	základní	k2eAgFnPc4d1	základní
skupiny	skupina	k1gFnPc4	skupina
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
normální	normální	k2eAgInSc4d1	normální
objektiv	objektiv	k1gInSc4	objektiv
–	–	k?	–
úhel	úhel	k1gInSc1	úhel
záběru	záběr	k1gInSc2	záběr
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
50	[number]	k4	50
<g/>
°	°	k?	°
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
úhel	úhel	k1gInSc4	úhel
vnímání	vnímání	k1gNnSc2	vnímání
lidského	lidský	k2eAgNnSc2d1	lidské
oka	oko	k1gNnSc2	oko
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc7	jejich
účelem	účel	k1gInSc7	účel
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
obraz	obraz	k1gInSc4	obraz
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
co	co	k9	co
nejvíce	nejvíce	k6eAd1	nejvíce
podobá	podobat	k5eAaImIp3nS	podobat
vnímání	vnímání	k1gNnSc4	vnímání
situace	situace	k1gFnSc2	situace
člověkem	člověk	k1gMnSc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Snímky	snímek	k1gInPc4	snímek
pořízené	pořízený	k2eAgInPc4d1	pořízený
takovým	takový	k3xDgInSc7	takový
objektivem	objektiv	k1gInSc7	objektiv
mají	mít	k5eAaImIp3nP	mít
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
nejpřirozenější	přirozený	k2eAgFnSc4d3	nejpřirozenější
perspektivu	perspektiva	k1gFnSc4	perspektiva
</s>
</p>
<p>
<s>
širokoúhlý	širokoúhlý	k2eAgInSc1d1	širokoúhlý
objektiv	objektiv	k1gInSc1	objektiv
–	–	k?	–
ohnisková	ohniskový	k2eAgFnSc1d1	ohnisková
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
je	být	k5eAaImIp3nS	být
kratší	krátký	k2eAgFnSc1d2	kratší
než	než	k8xS	než
u	u	k7c2	u
normálního	normální	k2eAgInSc2d1	normální
objektivu	objektiv	k1gInSc2	objektiv
<g/>
,	,	kIx,	,
snímek	snímek	k1gInSc1	snímek
má	mít	k5eAaImIp3nS	mít
tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
širší	široký	k2eAgInSc4d2	širší
záběr	záběr	k1gInSc4	záběr
<g/>
;	;	kIx,	;
extrémním	extrémní	k2eAgInSc7d1	extrémní
případem	případ	k1gInSc7	případ
jsou	být	k5eAaImIp3nP	být
objektivy	objektiv	k1gInPc1	objektiv
typu	typ	k1gInSc2	typ
"	"	kIx"	"
<g/>
rybí	rybí	k2eAgNnSc1d1	rybí
oko	oko	k1gNnSc1	oko
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
úhel	úhel	k1gInSc4	úhel
záběru	záběr	k1gInSc2	záběr
až	až	k9	až
180	[number]	k4	180
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
Důsledkem	důsledek	k1gInSc7	důsledek
bývá	bývat	k5eAaImIp3nS	bývat
zmenšení	zmenšení	k1gNnSc1	zmenšení
objektů	objekt	k1gInPc2	objekt
v	v	k7c6	v
obraze	obraz	k1gInSc6	obraz
a	a	k8xC	a
deformace	deformace	k1gFnSc1	deformace
jejich	jejich	k3xOp3gNnSc2	jejich
zobrazení	zobrazení	k1gNnSc2	zobrazení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
teleobjektiv	teleobjektiv	k1gInSc1	teleobjektiv
–	–	k?	–
zorný	zorný	k2eAgInSc1d1	zorný
úhel	úhel	k1gInSc1	úhel
je	být	k5eAaImIp3nS	být
užší	úzký	k2eAgInSc1d2	užší
<g/>
,	,	kIx,	,
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vyplnit	vyplnit	k5eAaPmF	vyplnit
celý	celý	k2eAgInSc4d1	celý
snímek	snímek	k1gInSc4	snímek
i	i	k9	i
poměrně	poměrně	k6eAd1	poměrně
vzdáleným	vzdálený	k2eAgInSc7d1	vzdálený
předmětem	předmět	k1gInSc7	předmět
<g/>
,	,	kIx,	,
obraz	obraz	k1gInSc1	obraz
předmětů	předmět	k1gInPc2	předmět
je	být	k5eAaImIp3nS	být
zvětšený	zvětšený	k2eAgMnSc1d1	zvětšený
<g/>
.	.	kIx.	.
<g/>
Dalším	další	k2eAgInSc7d1	další
faktorem	faktor	k1gInSc7	faktor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
určuje	určovat	k5eAaImIp3nS	určovat
vlastnosti	vlastnost	k1gFnPc4	vlastnost
objektivu	objektiv	k1gInSc2	objektiv
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
světelnost	světelnost	k1gFnSc1	světelnost
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
bezrozměrná	bezrozměrný	k2eAgFnSc1d1	bezrozměrná
hodnota	hodnota	k1gFnSc1	hodnota
<g/>
,	,	kIx,	,
číslo	číslo	k1gNnSc1	číslo
udávající	udávající	k2eAgFnSc4d1	udávající
hodnotu	hodnota	k1gFnSc4	hodnota
jmenovatele	jmenovatel	k1gInSc2	jmenovatel
ve	v	k7c6	v
zlomku	zlomek	k1gInSc6	zlomek
s	s	k7c7	s
čitatelem	čitatel	k1gInSc7	čitatel
1	[number]	k4	1
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
podíl	podíl	k1gInSc1	podíl
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
objektiv	objektiv	k1gInSc4	objektiv
propustí	propustit	k5eAaPmIp3nS	propustit
<g/>
.	.	kIx.	.
</s>
<s>
Teoretický	teoretický	k2eAgInSc1d1	teoretický
ideální	ideální	k2eAgInSc1d1	ideální
objektiv	objektiv	k1gInSc1	objektiv
propouští	propouštět	k5eAaImIp3nS	propouštět
veškeré	veškerý	k3xTgNnSc4	veškerý
světlo	světlo	k1gNnSc4	světlo
v	v	k7c6	v
úhlu	úhel	k1gInSc6	úhel
záběru	záběr	k1gInSc2	záběr
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
by	by	kYmCp3nS	by
tedy	tedy	k9	tedy
ve	v	k7c6	v
jmenovateli	jmenovatel	k1gInSc6	jmenovatel
mít	mít	k5eAaImF	mít
1	[number]	k4	1
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
tedy	tedy	k8xC	tedy
světelnost	světelnost	k1gFnSc1	světelnost
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Reálné	reálný	k2eAgInPc1d1	reálný
objektivy	objektiv	k1gInPc1	objektiv
vždy	vždy	k6eAd1	vždy
část	část	k1gFnSc1	část
světla	světlo	k1gNnSc2	světlo
pohltí	pohltit	k5eAaPmIp3nS	pohltit
-	-	kIx~	-
proto	proto	k8xC	proto
mají	mít	k5eAaImIp3nP	mít
číslo	číslo	k1gNnSc4	číslo
světelnosti	světelnost	k1gFnSc2	světelnost
vyšší	vysoký	k2eAgInPc1d2	vyšší
(	(	kIx(	(
<g/>
např.	např.	kA	např.
2	[number]	k4	2
=	=	kIx~	=
propustí	propust	k1gFnPc2	propust
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
=	=	kIx~	=
polovinu	polovina	k1gFnSc4	polovina
<g/>
,	,	kIx,	,
5	[number]	k4	5
propustí	propust	k1gFnPc2	propust
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
5	[number]	k4	5
-	-	kIx~	-
pětinu	pětina	k1gFnSc4	pětina
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hodnota	hodnota	k1gFnSc1	hodnota
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
1	[number]	k4	1
byla	být	k5eAaImAgFnS	být
prolomena	prolomit	k5eAaPmNgFnS	prolomit
například	například	k6eAd1	například
objektivem	objektiv	k1gInSc7	objektiv
Leica	Leicum	k1gNnSc2	Leicum
Noctilux	Noctilux	k1gInSc4	Noctilux
50	[number]	k4	50
<g/>
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
0,95	[number]	k4	0,95
<g/>
.	.	kIx.	.
</s>
<s>
Těchto	tento	k3xDgFnPc2	tento
hodnot	hodnota	k1gFnPc2	hodnota
lze	lze	k6eAd1	lze
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
především	především	k9	především
zvětšováním	zvětšování	k1gNnSc7	zvětšování
přední	přední	k2eAgFnSc2d1	přední
čočky	čočka	k1gFnSc2	čočka
-	-	kIx~	-
do	do	k7c2	do
těla	tělo	k1gNnSc2	tělo
se	se	k3xPyFc4	se
dostane	dostat	k5eAaPmIp3nS	dostat
více	hodně	k6eAd2	hodně
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
než	než	k8xS	než
by	by	kYmCp3nS	by
zachytil	zachytit	k5eAaPmAgMnS	zachytit
ideální	ideální	k2eAgInSc4d1	ideální
objektiv	objektiv	k1gInSc4	objektiv
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Číslo	číslo	k1gNnSc1	číslo
světelnosti	světelnost	k1gFnSc2	světelnost
bývá	bývat	k5eAaImIp3nS	bývat
většinou	většinou	k6eAd1	většinou
celé	celý	k2eAgNnSc1d1	celé
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
uváděné	uváděný	k2eAgNnSc4d1	uváděné
na	na	k7c4	na
jedno	jeden	k4xCgNnSc4	jeden
desetinné	desetinný	k2eAgNnSc4d1	desetinné
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Světelnost	světelnost	k1gFnSc1	světelnost
objektivu	objektiv	k1gInSc2	objektiv
lze	lze	k6eAd1	lze
regulovat	regulovat	k5eAaImF	regulovat
zacloněním	zaclonění	k1gNnSc7	zaclonění
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
vyšším	vysoký	k2eAgFnPc3d2	vyšší
hodnotám	hodnota	k1gFnPc3	hodnota
-	-	kIx~	-
na	na	k7c6	na
stupnici	stupnice	k1gFnSc6	stupnice
clony	clona	k1gFnSc2	clona
fotografických	fotografický	k2eAgInPc2d1	fotografický
přístrojů	přístroj	k1gInPc2	přístroj
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
některé	některý	k3yIgFnSc2	některý
hodnoty	hodnota	k1gFnSc2	hodnota
uvedeny	uveden	k2eAgInPc1d1	uveden
(	(	kIx(	(
<g/>
např.	např.	kA	např.
5,6	[number]	k4	5,6
<g/>
...	...	k?	...
11	[number]	k4	11
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejde	jít	k5eNaImIp3nS	jít
však	však	k9	však
o	o	k7c4	o
vlastnost	vlastnost	k1gFnSc4	vlastnost
objektivu	objektiv	k1gInSc2	objektiv
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pomocného	pomocný	k2eAgNnSc2d1	pomocné
zařízení	zařízení	k1gNnSc2	zařízení
-	-	kIx~	-
clony	clona	k1gFnSc2	clona
<g/>
.	.	kIx.	.
</s>
<s>
Nejnižší	nízký	k2eAgFnSc1d3	nejnižší
clona	clona	k1gFnSc1	clona
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
shodná	shodný	k2eAgFnSc1d1	shodná
se	s	k7c7	s
světelností	světelnost	k1gFnSc7	světelnost
objektivu	objektiv	k1gInSc2	objektiv
(	(	kIx(	(
<g/>
necloní	clonit	k5eNaImIp3nS	clonit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
plně	plně	k6eAd1	plně
otevřená	otevřený	k2eAgFnSc1d1	otevřená
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
hodnota	hodnota	k1gFnSc1	hodnota
uváděna	uvádět	k5eAaImNgFnS	uvádět
jako	jako	k8xS	jako
zlomek	zlomek	k1gInSc1	zlomek
s	s	k7c7	s
čitatelem	čitatel	k1gInSc7	čitatel
f	f	k?	f
(	(	kIx(	(
<g/>
např.	např.	kA	např.
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
objektivech	objektiv	k1gInPc6	objektiv
pro	pro	k7c4	pro
fotografii	fotografia	k1gFnSc4	fotografia
se	se	k3xPyFc4	se
ze	z	k7c2	z
zlomku	zlomek	k1gInSc2	zlomek
objevuje	objevovat	k5eAaImIp3nS	objevovat
často	často	k6eAd1	často
jen	jen	k9	jen
jmenovatel	jmenovatel	k1gInSc1	jmenovatel
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ohniskovou	ohniskový	k2eAgFnSc7d1	ohnisková
vzdáleností	vzdálenost	k1gFnSc7	vzdálenost
(	(	kIx(	(
<g/>
např.	např.	kA	např.
2,2	[number]	k4	2,2
–	–	k?	–
32	[number]	k4	32
mm	mm	kA	mm
nebo	nebo	k8xC	nebo
třeba	třeba	k6eAd1	třeba
1,8	[number]	k4	1,8
<g/>
/	/	kIx~	/
<g/>
50	[number]	k4	50
<g/>
mm	mm	kA	mm
<g/>
)	)	kIx)	)
popř.	popř.	kA	popř.
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
rozsahy	rozsah	k1gInPc4	rozsah
u	u	k7c2	u
zoomů	zoom	k1gInPc2	zoom
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
hodnota	hodnota	k1gFnSc1	hodnota
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
objektivy	objektiv	k1gInPc4	objektiv
důležitá	důležitý	k2eAgFnSc1d1	důležitá
je	být	k5eAaImIp3nS	být
hloubka	hloubka	k1gFnSc1	hloubka
ostrosti	ostrost	k1gFnSc2	ostrost
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
hodnota	hodnota	k1gFnSc1	hodnota
udávající	udávající	k2eAgFnSc1d1	udávající
rozsah	rozsah	k1gInSc4	rozsah
vzdálenosti	vzdálenost	k1gFnSc3	vzdálenost
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
je	být	k5eAaImIp3nS	být
objektiv	objektiv	k1gInSc1	objektiv
schopen	schopen	k2eAgInSc1d1	schopen
vykreslit	vykreslit	k5eAaPmF	vykreslit
obraz	obraz	k1gInSc4	obraz
ostře	ostro	k6eAd1	ostro
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
zobrazování	zobrazování	k1gNnSc2	zobrazování
vzdálených	vzdálený	k2eAgInPc2d1	vzdálený
předmětů	předmět	k1gInPc2	předmět
existují	existovat	k5eAaImIp3nP	existovat
objektivy	objektiv	k1gInPc1	objektiv
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
pracují	pracovat	k5eAaImIp3nP	pracovat
na	na	k7c6	na
extrémně	extrémně	k6eAd1	extrémně
krátké	krátký	k2eAgFnSc3d1	krátká
vzdálenosti	vzdálenost	k1gFnSc3	vzdálenost
-	-	kIx~	-
objektivy	objektiv	k1gInPc1	objektiv
mikroskopů	mikroskop	k1gInPc2	mikroskop
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
hloubka	hloubka	k1gFnSc1	hloubka
ostrosti	ostrost	k1gFnSc2	ostrost
se	se	k3xPyFc4	se
měří	měřit	k5eAaImIp3nS	měřit
na	na	k7c4	na
desetiny	desetina	k1gFnPc4	desetina
milimetru	milimetr	k1gInSc2	milimetr
u	u	k7c2	u
objektivů	objektiv	k1gInPc2	objektiv
optických	optický	k2eAgInPc2d1	optický
<g/>
,	,	kIx,	,
a	a	k8xC	a
u	u	k7c2	u
objektivů	objektiv	k1gInPc2	objektiv
elektronových	elektronový	k2eAgInPc2d1	elektronový
mikroskopů	mikroskop	k1gInPc2	mikroskop
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
ještě	ještě	k9	ještě
daleko	daleko	k6eAd1	daleko
nižší	nízký	k2eAgFnPc4d2	nižší
hodnoty	hodnota	k1gFnPc4	hodnota
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Objektivy	objektiv	k1gInPc1	objektiv
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
hlavní	hlavní	k2eAgFnPc4d1	hlavní
skupiny	skupina	k1gFnPc4	skupina
<g/>
:	:	kIx,	:
zrcadlové	zrcadlový	k2eAgInPc4d1	zrcadlový
(	(	kIx(	(
<g/>
reflektory	reflektor	k1gInPc4	reflektor
<g/>
)	)	kIx)	)
a	a	k8xC	a
čočkové	čočkový	k2eAgInPc4d1	čočkový
(	(	kIx(	(
<g/>
refraktory	refraktor	k1gInPc4	refraktor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
objektivy	objektiv	k1gInPc1	objektiv
"	"	kIx"	"
<g/>
mezi	mezi	k7c4	mezi
<g/>
"	"	kIx"	"
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
katadioptrické	katadioptrický	k2eAgNnSc1d1	katadioptrický
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Důvodem	důvod	k1gInSc7	důvod
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vedl	vést	k5eAaImAgInS	vést
k	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
rozdělení	rozdělení	k1gNnSc3	rozdělení
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
reálné	reálný	k2eAgFnPc1d1	reálná
optické	optický	k2eAgFnPc1d1	optická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
materiálů	materiál	k1gInPc2	materiál
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
jsou	být	k5eAaImIp3nP	být
objektivy	objektiv	k1gInPc1	objektiv
zhotovovány	zhotovován	k2eAgInPc1d1	zhotovován
"	"	kIx"	"
<g/>
sklo	sklo	k1gNnSc4	sklo
<g/>
"	"	kIx"	"
a	a	k8xC	a
fyzikální	fyzikální	k2eAgInPc1d1	fyzikální
zákony	zákon	k1gInPc1	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
si	se	k3xPyFc3	se
totiž	totiž	k9	totiž
uvědomit	uvědomit	k5eAaPmF	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vnímané	vnímaný	k2eAgInPc1d1	vnímaný
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
zaznamenávané	zaznamenávaný	k2eAgNnSc1d1	zaznamenávané
<g/>
)	)	kIx)	)
světlo	světlo	k1gNnSc1	světlo
je	být	k5eAaImIp3nS	být
vlastně	vlastně	k9	vlastně
"	"	kIx"	"
<g/>
směs	směs	k1gFnSc1	směs
světel	světlo	k1gNnPc2	světlo
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
vlnění	vlnění	k1gNnSc2	vlnění
v	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
vlnovém	vlnový	k2eAgInSc6d1	vlnový
rozsahu	rozsah	k1gInSc6	rozsah
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
frekvence	frekvence	k1gFnSc1	frekvence
(	(	kIx(	(
<g/>
vlnová	vlnový	k2eAgFnSc1d1	vlnová
délka	délka	k1gFnSc1	délka
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
v	v	k7c6	v
materiálu	materiál	k1gInSc6	materiál
lomí	lomit	k5eAaImIp3nS	lomit
pod	pod	k7c7	pod
jiným	jiný	k2eAgInSc7d1	jiný
úhlem	úhel	k1gInSc7	úhel
<g/>
,	,	kIx,	,
závislým	závislý	k2eAgMnSc7d1	závislý
na	na	k7c6	na
indexu	index	k1gInSc6	index
lomu	lom	k1gInSc2	lom
materiálu	materiál	k1gInSc2	materiál
a	a	k8xC	a
vlnové	vlnový	k2eAgFnSc3d1	vlnová
délce	délka	k1gFnSc3	délka
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
jsou	být	k5eAaImIp3nP	být
vlnové	vlnový	k2eAgFnPc4d1	vlnová
délky	délka	k1gFnPc4	délka
v	v	k7c6	v
požadovaném	požadovaný	k2eAgInSc6d1	požadovaný
rozsahu	rozsah	k1gInSc6	rozsah
obvykle	obvykle	k6eAd1	obvykle
blízké	blízký	k2eAgInPc1d1	blízký
<g/>
,	,	kIx,	,
přece	přece	k9	přece
jen	jen	k9	jen
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
při	při	k7c6	při
průchodu	průchod	k1gInSc6	průchod
čočkou	čočka	k1gFnSc7	čočka
určitý	určitý	k2eAgInSc4d1	určitý
rozptyl	rozptyl	k1gInSc4	rozptyl
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
fyzikální	fyzikální	k2eAgInSc1d1	fyzikální
jev	jev	k1gInSc1	jev
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těch	ten	k3xDgInPc6	ten
"	"	kIx"	"
<g/>
nejhorších	zlý	k2eAgInPc6d3	Nejhorší
<g/>
"	"	kIx"	"
objektivech	objektiv	k1gInPc6	objektiv
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
projevuje	projevovat	k5eAaImIp3nS	projevovat
například	například	k6eAd1	například
červeným	červený	k2eAgInSc7d1	červený
závojem	závoj	k1gInSc7	závoj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tuto	tento	k3xDgFnSc4	tento
vlastnost	vlastnost	k1gFnSc4	vlastnost
znal	znát	k5eAaImAgInS	znát
už	už	k9	už
Isaac	Isaac	k1gInSc1	Isaac
Newton	Newton	k1gMnSc1	Newton
a	a	k8xC	a
díky	díky	k7c3	díky
jeho	jeho	k3xOp3gInSc3	jeho
objevu	objev	k1gInSc3	objev
existují	existovat	k5eAaImIp3nP	existovat
objektivy	objektiv	k1gInPc1	objektiv
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
tuto	tento	k3xDgFnSc4	tento
vadu	vada	k1gFnSc4	vada
nemají	mít	k5eNaImIp3nP	mít
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
světlo	světlo	k1gNnSc1	světlo
u	u	k7c2	u
nich	on	k3xPp3gInPc2	on
žádným	žádný	k3yNgInSc7	žádný
materiálem	materiál	k1gInSc7	materiál
neprochází	procházet	k5eNaImIp3nS	procházet
(	(	kIx(	(
<g/>
když	když	k8xS	když
vynecháme	vynechat	k5eAaPmIp1nP	vynechat
vzduch	vzduch	k1gInSc4	vzduch
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
objektivy	objektiv	k1gInPc1	objektiv
zrcadlové	zrcadlový	k2eAgFnSc2d1	zrcadlová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
sestavě	sestava	k1gFnSc6	sestava
mají	mít	k5eAaImIp3nP	mít
ovšem	ovšem	k9	ovšem
tyto	tento	k3xDgInPc1	tento
objektivy	objektiv	k1gInPc1	objektiv
zase	zase	k9	zase
některé	některý	k3yIgInPc1	některý
praktické	praktický	k2eAgInPc4d1	praktický
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
teoretické	teoretický	k2eAgFnPc4d1	teoretická
nevýhody	nevýhoda	k1gFnPc4	nevýhoda
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
jsou	být	k5eAaImIp3nP	být
rozměrné	rozměrný	k2eAgInPc1d1	rozměrný
<g/>
,	,	kIx,	,
obraz	obraz	k1gInSc1	obraz
je	být	k5eAaImIp3nS	být
mimo	mimo	k7c4	mimo
osu	osa	k1gFnSc4	osa
a	a	k8xC	a
zobrazované	zobrazovaný	k2eAgNnSc4d1	zobrazované
pole	pole	k1gNnSc4	pole
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
okrajích	okraj	k1gInPc6	okraj
deformované	deformovaný	k2eAgFnPc1d1	deformovaná
(	(	kIx(	(
<g/>
koma	koma	k1gNnSc1	koma
<g/>
)	)	kIx)	)
tím	ten	k3xDgNnSc7	ten
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
čím	čí	k3xOyQgNnSc7	čí
je	být	k5eAaImIp3nS	být
křivost	křivost	k1gFnSc1	křivost
zrcadla	zrcadlo	k1gNnSc2	zrcadlo
vyšší	vysoký	k2eAgFnSc1d2	vyšší
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čočkové	čočkový	k2eAgInPc1d1	čočkový
objektivy	objektiv	k1gInPc1	objektiv
se	se	k3xPyFc4	se
z	z	k7c2	z
uvedených	uvedený	k2eAgInPc2d1	uvedený
důvodů	důvod	k1gInPc2	důvod
konstruují	konstruovat	k5eAaImIp3nP	konstruovat
nikoliv	nikoliv	k9	nikoliv
jako	jako	k9	jako
jedna	jeden	k4xCgFnSc1	jeden
čočka	čočka	k1gFnSc1	čočka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jako	jako	k8xC	jako
soustava	soustava	k1gFnSc1	soustava
čoček	čočka	k1gFnPc2	čočka
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
co	co	k3yInSc4	co
nejvíce	nejvíce	k6eAd1	nejvíce
potlačit	potlačit	k5eAaPmF	potlačit
chromatickou	chromatický	k2eAgFnSc4d1	chromatická
vadu	vada	k1gFnSc4	vada
při	při	k7c6	při
zachování	zachování	k1gNnSc6	zachování
záměru	záměr	k1gInSc2	záměr
(	(	kIx(	(
<g/>
ohniskové	ohniskový	k2eAgFnSc2d1	ohnisková
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
a	a	k8xC	a
světelnosti	světelnost	k1gFnSc2	světelnost
<g/>
)	)	kIx)	)
a	a	k8xC	a
věrnosti	věrnost	k1gFnSc2	věrnost
obrazu	obraz	k1gInSc2	obraz
(	(	kIx(	(
<g/>
spektrum	spektrum	k1gNnSc1	spektrum
<g/>
,	,	kIx,	,
rovinnost	rovinnost	k1gFnSc1	rovinnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Takový	takový	k3xDgInSc1	takový
objektiv	objektiv	k1gInSc1	objektiv
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
obvykle	obvykle	k6eAd1	obvykle
slovem	slovem	k6eAd1	slovem
panchromatický	panchromatický	k2eAgMnSc1d1	panchromatický
(	(	kIx(	(
<g/>
pan	pan	k1gMnSc1	pan
~	~	kIx~	~
vše	všechen	k3xTgNnSc1	všechen
<g/>
,	,	kIx,	,
chromos	chromos	k1gInSc1	chromos
~	~	kIx~	~
barva	barva	k1gFnSc1	barva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
fotografie	fotografie	k1gFnSc1	fotografie
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
vlastnost	vlastnost	k1gFnSc1	vlastnost
objektivů	objektiv	k1gInPc2	objektiv
vyzdvihována	vyzdvihovat	k5eAaImNgFnS	vyzdvihovat
jako	jako	k8xS	jako
důkaz	důkaz	k1gInSc1	důkaz
kvality	kvalita	k1gFnSc2	kvalita
konstrukce	konstrukce	k1gFnSc2	konstrukce
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
víceméně	víceméně	k9	víceméně
očekávána	očekáván	k2eAgFnSc1d1	očekávána
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
v	v	k7c6	v
označení	označení	k1gNnSc6	označení
přestává	přestávat	k5eAaImIp3nS	přestávat
objevovat	objevovat	k5eAaImF	objevovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zrcadlové	zrcadlový	k2eAgInPc1d1	zrcadlový
objektivy	objektiv	k1gInPc1	objektiv
absolvovaly	absolvovat	k5eAaPmAgFnP	absolvovat
relativně	relativně	k6eAd1	relativně
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
vývoj	vývoj	k1gInSc4	vývoj
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
uplatňují	uplatňovat	k5eAaImIp3nP	uplatňovat
v	v	k7c6	v
astrofotografii	astrofotografie	k1gFnSc6	astrofotografie
a	a	k8xC	a
především	především	k9	především
ve	v	k7c6	v
špičkových	špičkový	k2eAgInPc6d1	špičkový
hvězdářských	hvězdářský	k2eAgInPc6d1	hvězdářský
dalekohledech	dalekohled	k1gInPc6	dalekohled
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
fotografii	fotografia	k1gFnSc6	fotografia
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
soustava	soustava	k1gFnSc1	soustava
Cassegrain	Cassegraina	k1gFnPc2	Cassegraina
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
idea	idea	k1gFnSc1	idea
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
Newtonova	Newtonův	k2eAgInSc2d1	Newtonův
objektivu	objektiv	k1gInSc2	objektiv
sestaveného	sestavený	k2eAgInSc2d1	sestavený
ze	z	k7c2	z
zrcadel	zrcadlo	k1gNnPc2	zrcadlo
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
Newtonův	Newtonův	k2eAgInSc1d1	Newtonův
objektiv	objektiv	k1gInSc1	objektiv
má	mít	k5eAaImIp3nS	mít
nelineární	lineární	k2eNgInSc1d1	nelineární
jen	jen	k9	jen
jedno	jeden	k4xCgNnSc1	jeden
zrcadlo	zrcadlo	k1gNnSc1	zrcadlo
(	(	kIx(	(
<g/>
parabolické	parabolický	k2eAgNnSc1d1	parabolické
<g/>
)	)	kIx)	)
a	a	k8xC	a
druhé	druhý	k4xOgNnSc1	druhý
je	být	k5eAaImIp3nS	být
rovinné	rovinný	k2eAgNnSc1d1	rovinné
<g/>
,	,	kIx,	,
v	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
Cassegrain	Cassegraina	k1gFnPc2	Cassegraina
jsou	být	k5eAaImIp3nP	být
nelineární	lineární	k2eNgNnPc4d1	nelineární
dvě	dva	k4xCgNnPc4	dva
zrcadla	zrcadlo	k1gNnPc4	zrcadlo
(	(	kIx(	(
<g/>
klasicky	klasicky	k6eAd1	klasicky
parabola	parabola	k1gFnSc1	parabola
-	-	kIx~	-
hyperbola	hyperbola	k1gFnSc1	hyperbola
<g/>
)	)	kIx)	)
a	a	k8xC	a
obraz	obraz	k1gInSc1	obraz
je	být	k5eAaImIp3nS	být
vyveden	vyvést	k5eAaPmNgInS	vyvést
v	v	k7c6	v
ose	osa	k1gFnSc6	osa
objektivu	objektiv	k1gInSc2	objektiv
otvorem	otvor	k1gInSc7	otvor
za	za	k7c4	za
hlavní	hlavní	k2eAgNnSc4d1	hlavní
zrcadlo	zrcadlo	k1gNnSc4	zrcadlo
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
mimo	mimo	k6eAd1	mimo
jiné	jiný	k2eAgNnSc1d1	jiné
zkracuje	zkracovat	k5eAaImIp3nS	zkracovat
podstatně	podstatně	k6eAd1	podstatně
stavební	stavební	k2eAgFnSc4d1	stavební
délku	délka	k1gFnSc4	délka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
další	další	k2eAgFnPc1d1	další
modifikace	modifikace	k1gFnPc1	modifikace
tohoto	tento	k3xDgNnSc2	tento
schématu	schéma	k1gNnSc2	schéma
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
nevýznamnější	významný	k2eNgNnPc1d2	nevýznamnější
jsou	být	k5eAaImIp3nP	být
asi	asi	k9	asi
objektivy	objektiv	k1gInPc1	objektiv
Maksutov	Maksutovo	k1gNnPc2	Maksutovo
<g/>
,	,	kIx,	,
Schmidt	Schmidt	k1gMnSc1	Schmidt
(	(	kIx(	(
<g/>
oba	dva	k4xCgMnPc1	dva
s	s	k7c7	s
korekcí	korekce	k1gFnSc7	korekce
-	-	kIx~	-
meniskem	meniskus	k1gInSc7	meniskus
ještě	ještě	k9	ještě
před	před	k7c7	před
hlavním	hlavní	k2eAgNnSc7d1	hlavní
zrcadlem	zrcadlo	k1gNnSc7	zrcadlo
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgNnSc4d1	další
s	s	k7c7	s
jiným	jiný	k2eAgInSc7d1	jiný
tvarem	tvar	k1gInSc7	tvar
zrcadel	zrcadlo	k1gNnPc2	zrcadlo
Ritchey-Chretien	Ritchey-Chretien	k1gInSc1	Ritchey-Chretien
(	(	kIx(	(
<g/>
hyperbola	hyperbola	k1gFnSc1	hyperbola
-	-	kIx~	-
hyperbola	hyperbola	k1gFnSc1	hyperbola
<g/>
)	)	kIx)	)
-	-	kIx~	-
nejznámější	známý	k2eAgInSc1d3	nejznámější
kus	kus	k1gInSc1	kus
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
Hubbleově	Hubbleův	k2eAgInSc6d1	Hubbleův
vesmírném	vesmírný	k2eAgInSc6d1	vesmírný
dalekohledu	dalekohled	k1gInSc6	dalekohled
(	(	kIx(	(
<g/>
HST	HST	kA	HST
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
35	[number]	k4	35
<g/>
mm	mm	kA	mm
filmové	filmový	k2eAgNnSc1d1	filmové
políčko	políčko	k1gNnSc1	políčko
má	mít	k5eAaImIp3nS	mít
normální	normální	k2eAgInSc4d1	normální
objektiv	objektiv	k1gInSc4	objektiv
ohniskovou	ohniskový	k2eAgFnSc4d1	ohnisková
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
asi	asi	k9	asi
50	[number]	k4	50
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
většiny	většina	k1gFnSc2	většina
digitálních	digitální	k2eAgInPc2d1	digitální
fotoaparátů	fotoaparát	k1gInPc2	fotoaparát
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mají	mít	k5eAaImIp3nP	mít
senzory	senzor	k1gInPc1	senzor
daleko	daleko	k6eAd1	daleko
menší	malý	k2eAgInPc1d2	menší
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jen	jen	k9	jen
několik	několik	k4yIc4	několik
milimetrů	milimetr	k1gInPc2	milimetr
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
přehlednost	přehlednost	k1gFnSc4	přehlednost
se	se	k3xPyFc4	se
u	u	k7c2	u
nich	on	k3xPp3gMnPc2	on
ale	ale	k9	ale
zpravidla	zpravidla	k6eAd1	zpravidla
uvádí	uvádět	k5eAaImIp3nS	uvádět
"	"	kIx"	"
<g/>
35	[number]	k4	35
<g/>
mm	mm	kA	mm
ekvivalent	ekvivalent	k1gInSc1	ekvivalent
<g/>
"	"	kIx"	"
ohnisková	ohniskový	k2eAgFnSc1d1	ohnisková
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
na	na	k7c6	na
normálním	normální	k2eAgInSc6d1	normální
filmovém	filmový	k2eAgInSc6d1	filmový
políčku	políček	k1gInSc6	políček
odpovídala	odpovídat	k5eAaImAgFnS	odpovídat
stejnému	stejný	k2eAgInSc3d1	stejný
úhlu	úhel	k1gInSc3	úhel
záběru	záběr	k1gInSc2	záběr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Objektivy	objektiv	k1gInPc1	objektiv
s	s	k7c7	s
měnitelnou	měnitelný	k2eAgFnSc7d1	měnitelná
ohniskovou	ohniskový	k2eAgFnSc7d1	ohnisková
vzdáleností	vzdálenost	k1gFnSc7	vzdálenost
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
zoomovací	zoomovací	k2eAgFnSc1d1	zoomovací
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
použití	použití	k1gNnSc1	použití
ušetří	ušetřit	k5eAaPmIp3nS	ušetřit
fotografovi	fotograf	k1gMnSc3	fotograf
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
<g/>
-li	i	k?	-li
změnit	změnit	k5eAaPmF	změnit
snímaný	snímaný	k2eAgInSc4d1	snímaný
úhel	úhel	k1gInSc4	úhel
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
jsou	být	k5eAaImIp3nP	být
oblíbené	oblíbený	k2eAgFnPc1d1	oblíbená
v	v	k7c6	v
reportážní	reportážní	k2eAgFnSc6d1	reportážní
a	a	k8xC	a
amatérské	amatérský	k2eAgFnSc6d1	amatérská
fotografii	fotografia	k1gFnSc6	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
Zoomové	Zoomový	k2eAgInPc1d1	Zoomový
objektivy	objektiv	k1gInPc1	objektiv
mají	mít	k5eAaImIp3nP	mít
i	i	k9	i
své	svůj	k3xOyFgInPc4	svůj
nedostatky	nedostatek	k1gInPc4	nedostatek
–	–	k?	–
kvůli	kvůli	k7c3	kvůli
většímu	veliký	k2eAgInSc3d2	veliký
počtu	počet	k1gInSc3	počet
optických	optický	k2eAgMnPc2d1	optický
členů	člen	k1gMnPc2	člen
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
objektivy	objektiv	k1gInPc7	objektiv
s	s	k7c7	s
pevnou	pevný	k2eAgFnSc7d1	pevná
ohniskovou	ohniskový	k2eAgFnSc7d1	ohnisková
vzdáleností	vzdálenost	k1gFnSc7	vzdálenost
větší	veliký	k2eAgNnPc1d2	veliký
<g/>
,	,	kIx,	,
těžší	těžký	k2eAgNnPc1d2	těžší
a	a	k8xC	a
dražší	drahý	k2eAgNnPc1d2	dražší
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Například	například	k6eAd1	například
pevný	pevný	k2eAgInSc4d1	pevný
objektiv	objektiv	k1gInSc4	objektiv
Canon	Canon	kA	Canon
EF	EF	kA	EF
200	[number]	k4	200
<g/>
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2.8	[number]	k4	2.8
<g/>
L	L	kA	L
má	mít	k5eAaImIp3nS	mít
9	[number]	k4	9
elementů	element	k1gInPc2	element
a	a	k8xC	a
váží	vážit	k5eAaImIp3nS	vážit
765	[number]	k4	765
gramů	gram	k1gInPc2	gram
<g/>
,	,	kIx,	,
zoom	zoom	k1gInSc1	zoom
Canon	Canon	kA	Canon
EF	EF	kA	EF
70	[number]	k4	70
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2.8	[number]	k4	2.8
<g/>
L	L	kA	L
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
18	[number]	k4	18
elementů	element	k1gInPc2	element
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
hmotnost	hmotnost	k1gFnSc1	hmotnost
je	být	k5eAaImIp3nS	být
1,3	[number]	k4	1,3
kg	kg	kA	kg
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Ve	v	k7c6	v
srovnatelných	srovnatelný	k2eAgFnPc6d1	srovnatelná
cenových	cenový	k2eAgFnPc6d1	cenová
kategoriích	kategorie	k1gFnPc6	kategorie
mají	mít	k5eAaImIp3nP	mít
zoomy	zooma	k1gFnPc1	zooma
výrazně	výrazně	k6eAd1	výrazně
horší	horšit	k5eAaImIp3nP	horšit
světelnost	světelnost	k1gFnSc4	světelnost
i	i	k8xC	i
optické	optický	k2eAgFnPc4d1	optická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Speciální	speciální	k2eAgInPc1d1	speciální
objektivy	objektiv	k1gInPc1	objektiv
==	==	k?	==
</s>
</p>
<p>
<s>
zrcadlový	zrcadlový	k2eAgInSc1d1	zrcadlový
objektiv	objektiv	k1gInSc1	objektiv
–	–	k?	–
teleobjektiv	teleobjektiv	k1gInSc1	teleobjektiv
<g/>
,	,	kIx,	,
používající	používající	k2eAgFnPc1d1	používající
místo	místo	k7c2	místo
čoček	čočka	k1gFnPc2	čočka
zakřivené	zakřivený	k2eAgNnSc1d1	zakřivené
zrcadlo	zrcadlo	k1gNnSc1	zrcadlo
(	(	kIx(	(
<g/>
konstrukce	konstrukce	k1gFnSc1	konstrukce
je	být	k5eAaImIp3nS	být
podobná	podobný	k2eAgFnSc1d1	podobná
moderním	moderní	k2eAgInPc3d1	moderní
hvězdářským	hvězdářský	k2eAgInPc3d1	hvězdářský
dalekohledům	dalekohled	k1gInPc3	dalekohled
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
soustava	soustava	k1gFnSc1	soustava
Cassegrain	Cassegraina	k1gFnPc2	Cassegraina
doplněná	doplněná	k1gFnSc1	doplněná
menisky	meniskus	k1gInPc4	meniskus
-	-	kIx~	-
katadioptrické	katadioptrický	k2eAgFnPc4d1	katadioptrický
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
zrcadlové	zrcadlový	k2eAgInPc1d1	zrcadlový
objektivy	objektiv	k1gInPc1	objektiv
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
zřídka	zřídka	k6eAd1	zřídka
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
nemívají	mívat	k5eNaImIp3nP	mívat
měnitelnou	měnitelný	k2eAgFnSc4d1	měnitelná
clonu	clona	k1gFnSc4	clona
a	a	k8xC	a
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
nezvyklý	zvyklý	k2eNgInSc4d1	nezvyklý
bokeh	bokeh	k1gInSc4	bokeh
</s>
</p>
<p>
<s>
makroobjektiv	makroobjektiv	k1gInSc1	makroobjektiv
–	–	k?	–
objektiv	objektiv	k1gInSc4	objektiv
pro	pro	k7c4	pro
makrofotografii	makrofotografie	k1gFnSc4	makrofotografie
<g/>
,	,	kIx,	,
schopný	schopný	k2eAgMnSc1d1	schopný
zobrazení	zobrazení	k1gNnSc4	zobrazení
ve	v	k7c6	v
skutečné	skutečný	k2eAgFnSc6d1	skutečná
velikosti	velikost	k1gFnSc6	velikost
</s>
</p>
<p>
<s>
tilt-shift	tilthift	k2eAgInSc1d1	tilt-shift
objektiv	objektiv	k1gInSc1	objektiv
–	–	k?	–
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
posun	posun	k1gInSc1	posun
nebo	nebo	k8xC	nebo
náklon	náklon	k1gInSc1	náklon
optické	optický	k2eAgFnSc2d1	optická
osy	osa	k1gFnSc2	osa
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
korekci	korekce	k1gFnSc4	korekce
perspektivního	perspektivní	k2eAgNnSc2d1	perspektivní
zkreslení	zkreslení	k1gNnSc2	zkreslení
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
při	při	k7c6	při
fotografování	fotografování	k1gNnSc6	fotografování
architektury	architektura	k1gFnSc2	architektura
</s>
</p>
<p>
<s>
==	==	k?	==
Doporučené	doporučený	k2eAgNnSc1d1	Doporučené
použití	použití	k1gNnSc1	použití
objektivu	objektiv	k1gInSc2	objektiv
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
principu	princip	k1gInSc6	princip
lze	lze	k6eAd1	lze
téměř	téměř	k6eAd1	téměř
každý	každý	k3xTgInSc4	každý
objekt	objekt	k1gInSc4	objekt
fotografovat	fotografovat	k5eAaImF	fotografovat
libovolným	libovolný	k2eAgInSc7d1	libovolný
objektivem	objektiv	k1gInSc7	objektiv
<g/>
.	.	kIx.	.
</s>
<s>
Nejlepší	dobrý	k2eAgInPc1d3	nejlepší
snímky	snímek	k1gInPc1	snímek
se	se	k3xPyFc4	se
získají	získat	k5eAaPmIp3nP	získat
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
použit	použít	k5eAaPmNgInS	použít
objektiv	objektiv	k1gInSc1	objektiv
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
ohnisková	ohniskový	k2eAgFnSc1d1	ohnisková
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
typu	typ	k1gInSc2	typ
fotografovaného	fotografovaný	k2eAgInSc2d1	fotografovaný
objektu	objekt	k1gInSc2	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
vhodné	vhodný	k2eAgFnPc4d1	vhodná
ohniskové	ohniskový	k2eAgFnPc4d1	ohnisková
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
pro	pro	k7c4	pro
klasické	klasický	k2eAgNnSc4d1	klasické
snímání	snímání	k1gNnSc4	snímání
některých	některý	k3yIgInPc2	některý
objektů	objekt	k1gInPc2	objekt
nebo	nebo	k8xC	nebo
situací	situace	k1gFnPc2	situace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výrobci	výrobce	k1gMnPc1	výrobce
==	==	k?	==
</s>
</p>
<p>
<s>
Někteří	některý	k3yIgMnPc1	některý
významní	významný	k2eAgMnPc1d1	významný
výrobci	výrobce	k1gMnPc1	výrobce
objektivů	objektiv	k1gInPc2	objektiv
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Canon	Canon	kA	Canon
</s>
</p>
<p>
<s>
Carl	Carl	k1gMnSc1	Carl
Zeiss	Zeissa	k1gFnPc2	Zeissa
AG	AG	kA	AG
</s>
</p>
<p>
<s>
Cosina	Cosina	k1gFnSc1	Cosina
</s>
</p>
<p>
<s>
Leica	Leica	k6eAd1	Leica
</s>
</p>
<p>
<s>
Minolta	Minolta	kA	Minolta
</s>
</p>
<p>
<s>
Nikon	Nikon	k1gMnSc1	Nikon
</s>
</p>
<p>
<s>
Olympus	Olympus	k1gMnSc1	Olympus
</s>
</p>
<p>
<s>
Pentax	Pentax	k1gInSc1	Pentax
</s>
</p>
<p>
<s>
Tamron	Tamron	k1gMnSc1	Tamron
</s>
</p>
<p>
<s>
Tokina	Tokina	k1gFnSc1	Tokina
</s>
</p>
<p>
<s>
Sony	Sony	kA	Sony
</s>
</p>
<p>
<s>
Schneider	Schneider	k1gMnSc1	Schneider
Kreuznach	Kreuznach	k1gMnSc1	Kreuznach
</s>
</p>
<p>
<s>
Sigma	sigma	k1gNnSc1	sigma
Corporation	Corporation	k1gInSc1	Corporation
</s>
</p>
<p>
<s>
Samyang	Samyang	k1gMnSc1	Samyang
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Škola	škola	k1gFnSc1	škola
fotografování	fotografování	k1gNnSc2	fotografování
National	National	k1gMnPc2	National
Geographic	Geographic	k1gMnSc1	Geographic
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
K.	K.	kA	K.
Burian	Burian	k1gMnSc1	Burian
a	a	k8xC	a
Robert	Robert	k1gMnSc1	Robert
Caputo	Caput	k2eAgNnSc1d1	Caputo
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7026-253-2	[number]	k4	80-7026-253-2
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Čočka	čočka	k1gFnSc1	čočka
(	(	kIx(	(
<g/>
optika	optika	k1gFnSc1	optika
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Chromatická	chromatický	k2eAgFnSc1d1	chromatická
aberace	aberace	k1gFnSc1	aberace
</s>
</p>
<p>
<s>
Systémový	systémový	k2eAgInSc1d1	systémový
fotoaparát	fotoaparát	k1gInSc1	fotoaparát
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
objektiv	objektiv	k1gInSc4	objektiv
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
objektiv	objektiv	k1gInSc1	objektiv
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Rozdělení	rozdělení	k1gNnSc1	rozdělení
a	a	k8xC	a
vlastnosti	vlastnost	k1gFnPc1	vlastnost
objektivů	objektiv	k1gInPc2	objektiv
</s>
</p>
<p>
<s>
Ukázka	ukázka	k1gFnSc1	ukázka
ohniskové	ohniskový	k2eAgFnSc2d1	ohnisková
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
</s>
</p>
<p>
<s>
OBJEKTIVY	objektiv	k1gInPc1	objektiv
-	-	kIx~	-
vše	všechen	k3xTgNnSc1	všechen
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
o	o	k7c6	o
nich	on	k3xPp3gMnPc6	on
dalo	dát	k5eAaPmAgNnS	dát
říci	říct	k5eAaPmF	říct
</s>
</p>
