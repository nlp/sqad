<s>
Objektiv	objektiv	k1gInSc1	objektiv
je	být	k5eAaImIp3nS	být
čočka	čočka	k1gFnSc1	čočka
nebo	nebo	k8xC	nebo
soustava	soustava	k1gFnSc1	soustava
čoček	čočka	k1gFnPc2	čočka
<g/>
,	,	kIx,	,
vytvářející	vytvářející	k2eAgInSc1d1	vytvářející
opticky	opticky	k6eAd1	opticky
změněný	změněný	k2eAgInSc1d1	změněný
obraz	obraz	k1gInSc1	obraz
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
ještě	ještě	k6eAd1	ještě
dále	daleko	k6eAd2	daleko
zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
(	(	kIx(	(
<g/>
záznamem	záznam	k1gInSc7	záznam
<g/>
,	,	kIx,	,
okulárem	okulár	k1gInSc7	okulár
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
