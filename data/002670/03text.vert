<s>
Město	město	k1gNnSc1	město
Kravaře	kravař	k1gMnSc2	kravař
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Deutsch	Deutsch	k1gMnSc1	Deutsch
Krawarn	Krawarn	k1gMnSc1	Krawarn
<g/>
,	,	kIx,	,
slezština	slezština	k1gFnSc1	slezština
Krawôrz	Krawôrz	k1gMnSc1	Krawôrz
<g/>
)	)	kIx)	)
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Opava	Opava	k1gFnSc1	Opava
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
přes	přes	k7c4	přes
6	[number]	k4	6
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
8	[number]	k4	8
km	km	kA	km
západně	západně	k6eAd1	západně
leží	ležet	k5eAaImIp3nS	ležet
statutární	statutární	k2eAgNnSc1d1	statutární
město	město	k1gNnSc1	město
Opava	Opava	k1gFnSc1	Opava
<g/>
,	,	kIx,	,
13	[number]	k4	13
km	km	kA	km
východně	východně	k6eAd1	východně
město	město	k1gNnSc1	město
Hlučín	Hlučína	k1gFnPc2	Hlučína
<g/>
,	,	kIx,	,
19	[number]	k4	19
km	km	kA	km
jižně	jižně	k6eAd1	jižně
město	město	k1gNnSc1	město
Bílovec	Bílovec	k1gInSc1	Bílovec
a	a	k8xC	a
20	[number]	k4	20
km	km	kA	km
východně	východně	k6eAd1	východně
statutární	statutární	k2eAgNnSc4d1	statutární
město	město	k1gNnSc4	město
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Dvořiska	Dvořisko	k1gNnSc2	Dvořisko
bylo	být	k5eAaImAgNnS	být
území	území	k1gNnSc4	území
Kravař	Kravaře	k1gInPc2	Kravaře
součástí	součást	k1gFnPc2	součást
Hlučínska	Hlučínsko	k1gNnSc2	Hlučínsko
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgNnSc7	jenž
sdílelo	sdílet	k5eAaImAgNnS	sdílet
jeho	jeho	k3xOp3gInPc4	jeho
osudy	osud	k1gInPc4	osud
<g/>
.	.	kIx.	.
</s>
<s>
Dvořisko	Dvořisko	k6eAd1	Dvořisko
Kouty	kout	k1gInPc4	kout
Kravaře	kravař	k1gMnSc2	kravař
Kravaře	kravař	k1gMnSc2	kravař
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
Česky	česky	k6eAd1	česky
Kravaře	kravař	k1gMnSc2	kravař
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Kravaře	Kravaře	k1gInPc1	Kravaře
u	u	k7c2	u
Opavy	Opava	k1gFnSc2	Opava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Deutsch	Deutsch	k1gMnSc1	Deutsch
Krawarn	Krawarn	k1gMnSc1	Krawarn
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Krawarn	Krawarn	k1gNnSc1	Krawarn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
polsky	polsky	k6eAd1	polsky
Krawarze	Krawarze	k1gFnSc1	Krawarze
<g/>
,	,	kIx,	,
Krawarz	Krawarz	k1gInSc1	Krawarz
<g/>
.	.	kIx.	.
</s>
<s>
Česky	česky	k6eAd1	česky
Dvořisko	Dvořisko	k1gNnSc1	Dvořisko
(	(	kIx(	(
<g/>
1880	[number]	k4	1880
Dvoříště	Dvoříště	k1gNnSc1	Dvoříště
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Oppahof	Oppahof	k1gInSc1	Oppahof
<g/>
,	,	kIx,	,
polsky	polsky	k6eAd1	polsky
Dworzysko	Dworzysko	k1gNnSc1	Dworzysko
<g/>
.	.	kIx.	.
</s>
<s>
Česky	česky	k6eAd1	česky
Kouty	kout	k1gInPc1	kout
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Kauthen	Kauthen	k1gInSc1	Kauthen
<g/>
,	,	kIx,	,
polsky	polsky	k6eAd1	polsky
Kąty	Kąt	k2eAgFnPc1d1	Kąt
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
obci	obec	k1gFnSc6	obec
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1224	[number]	k4	1224
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1224	[number]	k4	1224
a	a	k8xC	a
1263	[number]	k4	1263
obdrželi	obdržet	k5eAaPmAgMnP	obdržet
kravařské	kravařský	k2eAgNnSc4d1	kravařské
panství	panství	k1gNnSc4	panství
členové	člen	k1gMnPc1	člen
rodu	rod	k1gInSc2	rod
Benešoviců	Benešovice	k1gMnPc2	Benešovice
<g/>
.	.	kIx.	.
barokní	barokní	k2eAgInSc1d1	barokní
zámek	zámek	k1gInSc1	zámek
Kravaře	kravař	k1gMnSc2	kravař
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Zámecké	zámecký	k2eAgNnSc1d1	zámecké
muzeum	muzeum	k1gNnSc1	muzeum
(	(	kIx(	(
<g/>
Muzeum	muzeum	k1gNnSc1	muzeum
Hlučínska	Hlučínsko	k1gNnSc2	Hlučínsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
nejhodnotnější	hodnotný	k2eAgFnSc7d3	nejhodnotnější
částí	část	k1gFnSc7	část
je	být	k5eAaImIp3nS	být
zámecká	zámecký	k2eAgFnSc1d1	zámecká
kaple	kaple	k1gFnSc1	kaple
s	s	k7c7	s
původním	původní	k2eAgInSc7d1	původní
dochovaným	dochovaný	k2eAgInSc7d1	dochovaný
interiérem	interiér	k1gInSc7	interiér
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
ostatní	ostatní	k2eAgInPc1d1	ostatní
interiéry	interiér	k1gInPc1	interiér
byly	být	k5eAaImAgInP	být
zničeny	zničit	k5eAaPmNgInP	zničit
při	při	k7c6	při
požáru	požár	k1gInSc6	požár
zámku	zámek	k1gInSc2	zámek
r.	r.	kA	r.
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
a	a	k8xC	a
zámecká	zámecký	k2eAgFnSc1d1	zámecká
zahrada	zahrada	k1gFnSc1	zahrada
(	(	kIx(	(
<g/>
cca	cca	kA	cca
20	[number]	k4	20
ha	ha	kA	ha
<g/>
,	,	kIx,	,
na	na	k7c6	na
velké	velký	k2eAgFnSc6d1	velká
části	část	k1gFnSc6	část
bylo	být	k5eAaImAgNnS	být
zbudováno	zbudovat	k5eAaPmNgNnS	zbudovat
golfové	golfový	k2eAgNnSc1d1	golfové
hřiště	hřiště	k1gNnSc1	hřiště
<g/>
,	,	kIx,	,
z	z	k7c2	z
dřevin	dřevina	k1gFnPc2	dřevina
jsou	být	k5eAaImIp3nP	být
cenné	cenný	k2eAgInPc1d1	cenný
především	především	k6eAd1	především
mohutné	mohutný	k2eAgInPc4d1	mohutný
exempláře	exemplář	k1gInPc4	exemplář
dubu	dub	k1gInSc2	dub
letního	letní	k2eAgNnSc2d1	letní
a	a	k8xC	a
lip	lípa	k1gFnPc2	lípa
srdčitých	srdčitý	k2eAgFnPc2d1	srdčitá
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Klášter	klášter	k1gInSc1	klášter
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
MÚ	MÚ	kA	MÚ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
chrám	chrám	k1gInSc1	chrám
svatého	svatý	k2eAgMnSc2d1	svatý
Bartoloměje	Bartoloměj	k1gMnSc2	Bartoloměj
a	a	k8xC	a
fara	fara	k1gFnSc1	fara
(	(	kIx(	(
<g/>
vše	všechen	k3xTgNnSc1	všechen
v	v	k7c6	v
cihlové	cihlový	k2eAgFnSc6d1	cihlová
neogotice	neogotika	k1gFnSc6	neogotika
typické	typický	k2eAgFnPc1d1	typická
pro	pro	k7c4	pro
Slezsko	Slezsko	k1gNnSc4	Slezsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
projektantem	projektant	k1gMnSc7	projektant
byl	být	k5eAaImAgMnS	být
kravařský	kravařský	k2eAgMnSc1d1	kravařský
rodák	rodák	k1gMnSc1	rodák
Josef	Josef	k1gMnSc1	Josef
Seyfried	Seyfried	k1gMnSc1	Seyfried
-	-	kIx~	-
architekt	architekt	k1gMnSc1	architekt
<g/>
,	,	kIx,	,
stavitel	stavitel	k1gMnSc1	stavitel
a	a	k8xC	a
hudebník	hudebník	k1gMnSc1	hudebník
(	(	kIx(	(
<g/>
známý	známý	k2eAgMnSc1d1	známý
z	z	k7c2	z
četných	četný	k2eAgInPc2d1	četný
dalších	další	k2eAgInPc2d1	další
realizovaných	realizovaný	k2eAgInPc2d1	realizovaný
projektů	projekt	k1gInPc2	projekt
sakrálních	sakrální	k2eAgInPc2d1	sakrální
i	i	k8xC	i
<g />
.	.	kIx.	.
</s>
<s>
profánních	profánní	k2eAgFnPc2d1	profánní
staveb	stavba	k1gFnPc2	stavba
na	na	k7c6	na
Hlučínsku	Hlučínsko	k1gNnSc6	Hlučínsko
a	a	k8xC	a
v	v	k7c6	v
dnešním	dnešní	k2eAgNnSc6d1	dnešní
Polsku	Polsko	k1gNnSc6	Polsko
je	být	k5eAaImIp3nS	být
-	-	kIx~	-
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
v	v	k7c6	v
rodných	rodný	k2eAgInPc6d1	rodný
Kravařích	Kravaře	k1gInPc6	Kravaře
-	-	kIx~	-
nejvýznamnější	významný	k2eAgInSc1d3	nejvýznamnější
komplex	komplex	k1gInSc1	komplex
církevních	církevní	k2eAgFnPc2d1	církevní
staveb	stavba	k1gFnPc2	stavba
v	v	k7c6	v
nedalekých	daleký	k2eNgFnPc6d1	nedaleká
Sudicích	sudice	k1gFnPc6	sudice
<g/>
)	)	kIx)	)
Socha	socha	k1gFnSc1	socha
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
na	na	k7c4	na
ul	ul	kA	ul
<g/>
.	.	kIx.	.
Petra	Petr	k1gMnSc2	Petr
z	z	k7c2	z
Kravař	Kravaře	k1gInPc2	Kravaře
(	(	kIx(	(
<g/>
poblíž	poblíž	k6eAd1	poblíž
ul	ul	kA	ul
<g/>
.	.	kIx.	.
Náměstí	náměstí	k1gNnSc1	náměstí
<g/>
)	)	kIx)	)
Lubliniec	Lubliniec	k1gInSc1	Lubliniec
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
Woźniki	Woźnik	k1gFnSc2	Woźnik
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
Lisková	Lisková	k1gFnSc1	Lisková
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc1	Slovensko
Karl	Karl	k1gMnSc1	Karl
Düsterberg	Düsterberg	k1gMnSc1	Düsterberg
(	(	kIx(	(
<g/>
1917	[number]	k4	1917
<g/>
-	-	kIx~	-
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
-	-	kIx~	-
německý	německý	k2eAgMnSc1d1	německý
podnikatel	podnikatel	k1gMnSc1	podnikatel
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
potravinářského	potravinářský	k2eAgInSc2d1	potravinářský
koncernu	koncern	k1gInSc2	koncern
Apetito	Apetit	k2eAgNnSc1d1	Apetito
a	a	k8xC	a
průkopník	průkopník	k1gMnSc1	průkopník
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
hluboce	hluboko	k6eAd1	hluboko
zmražených	zmražený	k2eAgFnPc2d1	zmražená
potravin	potravina	k1gFnPc2	potravina
Alois	Alois	k1gMnSc1	Alois
Hadamczik	Hadamczik	k1gMnSc1	Hadamczik
(	(	kIx(	(
<g/>
*	*	kIx~	*
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
-	-	kIx~	-
trenér	trenér	k1gMnSc1	trenér
české	český	k2eAgFnSc2d1	Česká
hokejové	hokejový	k2eAgFnSc2d1	hokejová
reprezentace	reprezentace	k1gFnSc2	reprezentace
Prof.	prof.	kA	prof.
PhDr.	PhDr.	kA	PhDr.
Josef	Josef	k1gMnSc1	Josef
<g />
.	.	kIx.	.
</s>
<s>
Jařab	Jařab	k1gMnSc1	Jařab
<g/>
,	,	kIx,	,
CSc.	CSc.	kA	CSc.
<g/>
,	,	kIx,	,
dr	dr	kA	dr
<g/>
.	.	kIx.	.
h.	h.	k?	h.
c.	c.	k?	c.
(	(	kIx(	(
<g/>
*	*	kIx~	*
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
-	-	kIx~	-
anglista	anglista	k1gMnSc1	anglista
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1990	[number]	k4	1990
<g/>
-	-	kIx~	-
<g/>
97	[number]	k4	97
rektor	rektor	k1gMnSc1	rektor
Univerzity	univerzita	k1gFnSc2	univerzita
Palackého	Palackého	k2eAgMnSc1d1	Palackého
Augustin	Augustin	k1gMnSc1	Augustin
Kaluža	Kaluža	k1gMnSc1	Kaluža
(	(	kIx(	(
<g/>
1776	[number]	k4	1776
<g/>
-	-	kIx~	-
<g/>
1836	[number]	k4	1836
<g/>
)	)	kIx)	)
-	-	kIx~	-
rodák	rodák	k1gMnSc1	rodák
z	z	k7c2	z
Koutů	kout	k1gInPc2	kout
<g/>
,	,	kIx,	,
přírodovědec	přírodovědec	k1gMnSc1	přírodovědec
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
kněz	kněz	k1gMnSc1	kněz
<g/>
,	,	kIx,	,
učitel	učitel	k1gMnSc1	učitel
na	na	k7c6	na
gymnáziu	gymnázium	k1gNnSc6	gymnázium
ve	v	k7c6	v
Vratislavi	Vratislav	k1gFnSc6	Vratislav
<g/>
,	,	kIx,	,
knižně	knižně	k6eAd1	knižně
vydal	vydat	k5eAaPmAgMnS	vydat
pět	pět	k4xCc4	pět
děl	dělo	k1gNnPc2	dělo
o	o	k7c6	o
přírodě	příroda	k1gFnSc6	příroda
Slezska	Slezsko	k1gNnSc2	Slezsko
šlechtický	šlechtický	k2eAgInSc1d1	šlechtický
rod	rod	k1gInSc1	rod
pánů	pan	k1gMnPc2	pan
z	z	k7c2	z
Kravař	kravař	k1gMnSc1	kravař
Petr	Petr	k1gMnSc1	Petr
z	z	k7c2	z
Kravař	kravař	k1gMnSc1	kravař
(	(	kIx(	(
<g/>
1389	[number]	k4	1389
<g/>
-	-	kIx~	-
<g/>
1434	[number]	k4	1434
<g/>
)	)	kIx)	)
-	-	kIx~	-
moravský	moravský	k2eAgMnSc1d1	moravský
šlechtic	šlechtic	k1gMnSc1	šlechtic
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
příslušníkem	příslušník	k1gMnSc7	příslušník
rodu	rod	k1gInSc2	rod
pánů	pan	k1gMnPc2	pan
z	z	k7c2	z
Kravař	Kravaře	k1gInPc2	Kravaře
Pavel	Pavel	k1gMnSc1	Pavel
Kravař	kravař	k1gMnSc1	kravař
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1391	[number]	k4	1391
<g/>
-	-	kIx~	-
<g/>
1433	[number]	k4	1433
<g/>
)	)	kIx)	)
-	-	kIx~	-
husitský	husitský	k2eAgMnSc1d1	husitský
emisař	emisař	k1gMnSc1	emisař
Michal	Michal	k1gMnSc1	Michal
Sendivoj	Sendivoj	k1gInSc4	Sendivoj
ze	z	k7c2	z
Skorska	Skorsk	k1gInSc2	Skorsk
(	(	kIx(	(
<g/>
1566	[number]	k4	1566
<g/>
-	-	kIx~	-
<g/>
1636	[number]	k4	1636
<g/>
)	)	kIx)	)
-	-	kIx~	-
alchymista	alchymista	k1gMnSc1	alchymista
a	a	k8xC	a
chemik	chemik	k1gMnSc1	chemik
<g/>
,	,	kIx,	,
majitel	majitel	k1gMnSc1	majitel
Kravař	kravař	k1gMnSc1	kravař
Josef	Josef	k1gMnSc1	Josef
Seyfried	Seyfried	k1gMnSc1	Seyfried
(	(	kIx(	(
<g/>
1865	[number]	k4	1865
<g/>
-	-	kIx~	-
<g/>
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
-	-	kIx~	-
architekt	architekt	k1gMnSc1	architekt
a	a	k8xC	a
stavitel	stavitel	k1gMnSc1	stavitel
(	(	kIx(	(
<g/>
autodidaktik	autodidaktik	k1gMnSc1	autodidaktik
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
projektoval	projektovat	k5eAaBmAgInS	projektovat
a	a	k8xC	a
stavebně	stavebně	k6eAd1	stavebně
upravil	upravit	k5eAaPmAgMnS	upravit
četné	četný	k2eAgFnPc4d1	četná
významné	významný	k2eAgFnPc4d1	významná
církevní	církevní	k2eAgFnPc4d1	církevní
i	i	k8xC	i
profánní	profánní	k2eAgFnPc4d1	profánní
stavby	stavba	k1gFnPc4	stavba
a	a	k8xC	a
celky	celek	k1gInPc1	celek
tehdejšího	tehdejší	k2eAgNnSc2d1	tehdejší
Pruska	Prusko	k1gNnSc2	Prusko
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Hlučínsko	Hlučínsko	k1gNnSc1	Hlučínsko
a	a	k8xC	a
příhraniční	příhraniční	k2eAgNnSc1d1	příhraniční
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnPc1	jeho
nejvýznamnější	významný	k2eAgFnPc1d3	nejvýznamnější
charakterizující	charakterizující	k2eAgFnPc1d1	charakterizující
stavby	stavba	k1gFnPc1	stavba
v	v	k7c6	v
severské	severský	k2eAgFnSc6d1	severská
cihlové	cihlový	k2eAgFnSc6d1	cihlová
neogotice	neogotika	k1gFnSc6	neogotika
Leo	Leo	k1gMnSc1	Leo
a	a	k8xC	a
Alice	Alice	k1gFnSc1	Alice
Stoklasovi	Stoklasovi	k1gRnPc1	Stoklasovi
-	-	kIx~	-
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
významní	významný	k2eAgMnPc1d1	významný
podnikatelé	podnikatel	k1gMnPc1	podnikatel
s	s	k7c7	s
textilní	textilní	k2eAgFnSc7d1	textilní
galanterií	galanterie	k1gFnSc7	galanterie
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
bižuterií	bižuterie	k1gFnSc7	bižuterie
PhDr.	PhDr.	kA	PhDr.
Erich	Erich	k1gMnSc1	Erich
Šefčík	Šefčík	k1gMnSc1	Šefčík
(	(	kIx(	(
<g/>
1945	[number]	k4	1945
<g/>
-	-	kIx~	-
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
-	-	kIx~	-
archivář	archivář	k1gMnSc1	archivář
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
<g/>
,	,	kIx,	,
numismatik	numismatik	k1gMnSc1	numismatik
<g/>
,	,	kIx,	,
vlastivědec	vlastivědec	k1gMnSc1	vlastivědec
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
Zámeckého	zámecký	k2eAgNnSc2d1	zámecké
muzea	muzeum	k1gNnSc2	muzeum
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
stovek	stovka	k1gFnPc2	stovka
odborných	odborný	k2eAgInPc2d1	odborný
článků	článek	k1gInPc2	článek
a	a	k8xC	a
několika	několik	k4yIc2	několik
monografií	monografie	k1gFnPc2	monografie
PhDr.	PhDr.	kA	PhDr.
Ondřej	Ondřej	k1gMnSc1	Ondřej
Šefčík	Šefčík	k1gMnSc1	Šefčík
<g/>
,	,	kIx,	,
PhD	PhD	k1gMnSc1	PhD
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
-	-	kIx~	-
český	český	k2eAgMnSc1d1	český
obecný	obecný	k2eAgMnSc1d1	obecný
lingvista	lingvista	k1gMnSc1	lingvista
<g/>
,	,	kIx,	,
indoevropeista	indoevropeista	k1gMnSc1	indoevropeista
<g/>
,	,	kIx,	,
bohemista	bohemista	k1gMnSc1	bohemista
a	a	k8xC	a
slavista	slavista	k1gMnSc1	slavista
<g/>
,	,	kIx,	,
specializující	specializující	k2eAgMnSc1d1	specializující
se	se	k3xPyFc4	se
na	na	k7c4	na
fonologii	fonologie	k1gFnSc4	fonologie
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
Ericha	Erich	k1gMnSc2	Erich
Šefčíka	Šefčík	k1gMnSc2	Šefčík
Marie	Marie	k1gFnSc1	Marie
Šindelářová	Šindelářová	k1gFnSc1	Šindelářová
(	(	kIx(	(
<g/>
rozená	rozený	k2eAgFnSc1d1	rozená
Holubková	Holubkový	k2eAgFnSc1d1	Holubková
<g/>
,	,	kIx,	,
pseudonymem	pseudonym	k1gInSc7	pseudonym
Ludmila	Ludmila	k1gFnSc1	Ludmila
Hořká	hořká	k1gFnSc1	hořká
<g/>
)	)	kIx)	)
-	-	kIx~	-
nar	nar	kA	nar
<g/>
.	.	kIx.	.
1892	[number]	k4	1892
ve	v	k7c6	v
Dvořisku	Dvořisek	k1gInSc6	Dvořisek
<g/>
,	,	kIx,	,
pohřbena	pohřben	k2eAgMnSc4d1	pohřben
na	na	k7c4	na
sousední	sousední	k2eAgNnSc4d1	sousední
<g />
.	.	kIx.	.
</s>
<s>
Štítině	Štítina	k1gFnSc3	Štítina
(	(	kIx(	(
<g/>
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
významná	významný	k2eAgFnSc1d1	významná
regionální	regionální	k2eAgFnSc1d1	regionální
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
a	a	k8xC	a
sběratelka	sběratelka	k1gFnSc1	sběratelka
lidové	lidový	k2eAgFnSc2d1	lidová
slovesnosti	slovesnost	k1gFnSc2	slovesnost
Ivo	Ivo	k1gMnSc1	Ivo
Žídek	Žídek	k1gMnSc1	Žídek
(	(	kIx(	(
<g/>
1926	[number]	k4	1926
<g/>
-	-	kIx~	-
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
-	-	kIx~	-
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
českých	český	k2eAgMnPc2d1	český
tenorů	tenor	k1gMnPc2	tenor
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Jeník	Jeník	k1gMnSc1	Jeník
z	z	k7c2	z
Prodané	prodaný	k2eAgFnSc2d1	prodaná
nevěsty	nevěsta	k1gFnSc2	nevěsta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
člen	člen	k1gInSc1	člen
operního	operní	k2eAgInSc2d1	operní
souboru	soubor	k1gInSc2	soubor
ND	ND	kA	ND
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1989	[number]	k4	1989
<g/>
-	-	kIx~	-
<g/>
1991	[number]	k4	1991
ředitel	ředitel	k1gMnSc1	ředitel
ND	ND	kA	ND
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
Základní	základní	k2eAgFnSc1d1	základní
umělecká	umělecký	k2eAgFnSc1d1	umělecká
škola	škola	k1gFnSc1	škola
Ivo	Ivo	k1gMnSc1	Ivo
Žídka	Žídek	k1gMnSc2	Žídek
v	v	k7c6	v
Kravařích	Kravaře	k1gInPc6	Kravaře
Monika	Monika	k1gFnSc1	Monika
Žídková	Žídková	k1gFnSc1	Žídková
(	(	kIx(	(
<g/>
*	*	kIx~	*
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
-	-	kIx~	-
Miss	miss	k1gFnSc1	miss
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
Miss	miss	k1gFnSc1	miss
Europe	Europ	k1gMnSc5	Europ
1995	[number]	k4	1995
</s>
