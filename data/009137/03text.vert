<p>
<s>
Člun	člun	k1gInSc1	člun
je	být	k5eAaImIp3nS	být
plavidlo	plavidlo	k1gNnSc1	plavidlo
obvykle	obvykle	k6eAd1	obvykle
menší	malý	k2eAgMnSc1d2	menší
než	než	k8xS	než
loď	loď	k1gFnSc1	loď
a	a	k8xC	a
často	často	k6eAd1	často
bez	bez	k7c2	bez
vlastního	vlastní	k2eAgInSc2d1	vlastní
mechanického	mechanický	k2eAgInSc2d1	mechanický
pohonu	pohon	k1gInSc2	pohon
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
čluny	člun	k1gInPc1	člun
jsou	být	k5eAaImIp3nP	být
tak	tak	k6eAd1	tak
malé	malý	k2eAgFnPc1d1	malá
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
přepravují	přepravovat	k5eAaImIp3nP	přepravovat
loděmi	loď	k1gFnPc7	loď
a	a	k8xC	a
na	na	k7c6	na
souši	souš	k1gFnSc6	souš
v	v	k7c6	v
přívěsech	přívěs	k1gInPc6	přívěs
za	za	k7c7	za
motorovými	motorový	k2eAgNnPc7d1	motorové
vozidly	vozidlo	k1gNnPc7	vozidlo
<g/>
.	.	kIx.	.
</s>
<s>
Člunem	člun	k1gInSc7	člun
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
i	i	k9	i
například	například	k6eAd1	například
malá	malý	k2eAgNnPc1d1	malé
nafukovací	nafukovací	k2eAgNnPc1d1	nafukovací
plavidla	plavidlo	k1gNnPc1	plavidlo
k	k	k7c3	k
plážové	plážový	k2eAgFnSc3d1	plážová
rekreaci	rekreace	k1gFnSc3	rekreace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
širším	široký	k2eAgInSc6d2	širší
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
je	být	k5eAaImIp3nS	být
člun	člun	k1gInSc1	člun
malá	malý	k2eAgFnSc1d1	malá
loď	loď	k1gFnSc1	loď
<g/>
,	,	kIx,	,
obdobně	obdobně	k6eAd1	obdobně
jako	jako	k9	jako
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
pojem	pojem	k1gInSc1	pojem
boat	boata	k1gFnPc2	boata
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ship	ship	k1gInSc4	ship
či	či	k8xC	či
v	v	k7c6	v
němčině	němčina	k1gFnSc6	němčina
das	das	k?	das
Boot	Boota	k1gFnPc2	Boota
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Schiff	Schiff	k1gInSc1	Schiff
<g/>
.	.	kIx.	.
</s>
<s>
Čluny	člun	k1gInPc1	člun
pro	pro	k7c4	pro
delší	dlouhý	k2eAgFnPc4d2	delší
plavby	plavba	k1gFnPc4	plavba
i	i	k9	i
po	po	k7c6	po
otevřeném	otevřený	k2eAgNnSc6d1	otevřené
moři	moře	k1gNnSc6	moře
mívají	mívat	k5eAaImIp3nP	mívat
podpalubí	podpalubí	k1gNnSc4	podpalubí
-	-	kIx~	-
v	v	k7c6	v
civilním	civilní	k2eAgInSc6d1	civilní
sektoru	sektor	k1gInSc6	sektor
typicky	typicky	k6eAd1	typicky
rybářský	rybářský	k2eAgInSc1d1	rybářský
člun	člun	k1gInSc1	člun
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
válečná	válečný	k2eAgNnPc4d1	válečné
plavidla	plavidlo	k1gNnPc4	plavidlo
pak	pak	k9	pak
řadíme	řadit	k5eAaImIp1nP	řadit
čluny	člun	k1gInPc1	člun
dělové	dělový	k2eAgInPc1d1	dělový
<g/>
,	,	kIx,	,
torpédové	torpédový	k2eAgInPc1d1	torpédový
<g/>
,	,	kIx,	,
raketové	raketový	k2eAgInPc1d1	raketový
<g/>
,	,	kIx,	,
hlídkové	hlídkový	k2eAgInPc1d1	hlídkový
<g/>
/	/	kIx~	/
<g/>
strážní	strážní	k2eAgInPc1d1	strážní
<g/>
,	,	kIx,	,
vyloďovací	vyloďovací	k2eAgInPc1d1	vyloďovací
(	(	kIx(	(
<g/>
které	který	k3yIgNnSc4	který
zpravidla	zpravidla	k6eAd1	zpravidla
podpalubí	podpalubí	k1gNnSc4	podpalubí
nemají	mít	k5eNaImIp3nP	mít
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
starší	starý	k2eAgFnSc6d2	starší
literatuře	literatura	k1gFnSc6	literatura
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
pojmem	pojem	k1gInSc7	pojem
podmořský	podmořský	k2eAgInSc1d1	podmořský
nebo	nebo	k8xC	nebo
ponorný	ponorný	k2eAgInSc1d1	ponorný
člun	člun	k1gInSc1	člun
jako	jako	k8xC	jako
označením	označení	k1gNnSc7	označení
pro	pro	k7c4	pro
ponorku	ponorka	k1gFnSc4	ponorka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
vnitrozemské	vnitrozemský	k2eAgFnSc6d1	vnitrozemská
plavbě	plavba	k1gFnSc6	plavba
se	se	k3xPyFc4	se
označení	označení	k1gNnSc1	označení
člun	člun	k1gInSc1	člun
oficielně	oficielně	k6eAd1	oficielně
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
lodě	loď	k1gFnPc4	loď
bez	bez	k7c2	bez
vlastního	vlastní	k2eAgInSc2d1	vlastní
strojního	strojní	k2eAgInSc2d1	strojní
pohonu	pohon	k1gInSc2	pohon
(	(	kIx(	(
<g/>
vlečný	vlečný	k2eAgInSc1d1	vlečný
člun	člun	k1gInSc1	člun
<g/>
,	,	kIx,	,
tlačený	tlačený	k2eAgInSc1d1	tlačený
člun	člun	k1gInSc1	člun
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
pro	pro	k7c4	pro
malá	malý	k2eAgNnPc4d1	malé
plavidla	plavidlo	k1gNnPc4	plavidlo
s	s	k7c7	s
vlastním	vlastní	k2eAgInSc7d1	vlastní
strojním	strojní	k2eAgInSc7d1	strojní
pohonem	pohon	k1gInSc7	pohon
(	(	kIx(	(
<g/>
otevřený	otevřený	k2eAgInSc1d1	otevřený
člun	člun	k1gInSc1	člun
<g/>
,	,	kIx,	,
kajutový	kajutový	k2eAgInSc1d1	kajutový
člun	člun	k1gInSc1	člun
<g/>
,	,	kIx,	,
závodní	závodní	k2eAgInSc1d1	závodní
člun	člun	k1gInSc1	člun
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Člun	člun	k1gInSc1	člun
je	být	k5eAaImIp3nS	být
skořápkovitá	skořápkovitý	k2eAgFnSc1d1	skořápkovitý
vztlaková	vztlakový	k2eAgFnSc1d1	vztlaková
struktura	struktura	k1gFnSc1	struktura
podle	podle	k7c2	podle
Archimédova	Archimédův	k2eAgInSc2d1	Archimédův
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
trup	trup	k1gInSc1	trup
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
doplněná	doplněný	k2eAgFnSc1d1	doplněná
kvůli	kvůli	k7c3	kvůli
stabilitě	stabilita	k1gFnSc3	stabilita
pomocnými	pomocný	k2eAgInPc7d1	pomocný
trupy	trup	k1gInPc7	trup
či	či	k8xC	či
plováky	plovák	k1gInPc7	plovák
(	(	kIx(	(
<g/>
katamarán	katamarán	k1gInSc1	katamarán
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pohon	pohon	k1gInSc1	pohon
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
lidskou	lidský	k2eAgFnSc7d1	lidská
silou	síla	k1gFnSc7	síla
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
vesly	veslo	k1gNnPc7	veslo
(	(	kIx(	(
<g/>
pramice	pramice	k1gFnSc1	pramice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pádlem	pádlo	k1gNnSc7	pádlo
(	(	kIx(	(
<g/>
kajak	kajak	k1gInSc1	kajak
<g/>
,	,	kIx,	,
kánoe	kánoe	k1gFnSc1	kánoe
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bidlem	bidlo	k1gNnSc7	bidlo
(	(	kIx(	(
<g/>
přívoz	přívoz	k1gInSc1	přívoz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
delší	dlouhý	k2eAgFnSc4d2	delší
a	a	k8xC	a
sportovní	sportovní	k2eAgFnSc2d1	sportovní
plavby	plavba	k1gFnSc2	plavba
systémem	systém	k1gInSc7	systém
plachet	plachta	k1gFnPc2	plachta
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgInPc1d2	veliký
a	a	k8xC	a
vojenské	vojenský	k2eAgInPc1d1	vojenský
čluny	člun	k1gInPc1	člun
pohání	pohánět	k5eAaImIp3nP	pohánět
nejčastěji	často	k6eAd3	často
lodní	lodní	k2eAgInSc4d1	lodní
šroub	šroub	k1gInSc4	šroub
<g/>
,	,	kIx,	,
na	na	k7c6	na
mělkých	mělký	k2eAgInPc6d1	mělký
tocích	tok	k1gInPc6	tok
parní	parní	k2eAgFnSc1d1	parní
kolesa	kolesa	k1gFnSc1	kolesa
a	a	k8xC	a
pro	pro	k7c4	pro
rychlé	rychlý	k2eAgInPc4d1	rychlý
čluny	člun	k1gInPc4	člun
trysky	tryska	k1gFnSc2	tryska
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Nejstarší	starý	k2eAgInPc1d3	nejstarší
čluny	člun	k1gInPc1	člun
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
monoxyly	monoxyl	k1gInPc1	monoxyl
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
vydlabány	vydlabat	k5eAaPmNgFnP	vydlabat
z	z	k7c2	z
jediného	jediný	k2eAgInSc2d1	jediný
kmene	kmen	k1gInSc2	kmen
stromu	strom	k1gInSc2	strom
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
už	už	k6eAd1	už
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
stavěli	stavět	k5eAaImAgMnP	stavět
lidé	člověk	k1gMnPc1	člověk
dřevěné	dřevěný	k2eAgInPc1d1	dřevěný
čluny	člun	k1gInPc1	člun
s	s	k7c7	s
kostrou	kostra	k1gFnSc7	kostra
(	(	kIx(	(
<g/>
kýlem	kýl	k1gInSc7	kýl
a	a	k8xC	a
žebry	žebr	k1gInPc7	žebr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c4	na
niž	jenž	k3xRgFnSc4	jenž
se	se	k3xPyFc4	se
vázala	vázat	k5eAaImAgFnS	vázat
obšívka	obšívka	k1gFnSc1	obšívka
z	z	k7c2	z
úzkých	úzký	k2eAgNnPc2d1	úzké
prkének	prkénko	k1gNnPc2	prkénko
<g/>
.	.	kIx.	.
</s>
<s>
Mezery	mezera	k1gFnPc1	mezera
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
se	se	k3xPyFc4	se
ucpávaly	ucpávat	k5eAaImAgFnP	ucpávat
koudelí	koudel	k1gFnSc7	koudel
a	a	k8xC	a
tmelily	tmelit	k5eAaImAgInP	tmelit
pryskyřicí	pryskyřice	k1gFnSc7	pryskyřice
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
se	se	k3xPyFc4	se
užívaly	užívat	k5eAaImAgFnP	užívat
k	k	k7c3	k
rybolovu	rybolov	k1gInSc3	rybolov
<g/>
,	,	kIx,	,
k	k	k7c3	k
dopravě	doprava	k1gFnSc3	doprava
po	po	k7c6	po
řekách	řeka	k1gFnPc6	řeka
a	a	k8xC	a
jezerech	jezero	k1gNnPc6	jezero
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
vozily	vozit	k5eAaImAgFnP	vozit
lidi	člověk	k1gMnPc4	člověk
i	i	k8xC	i
zboží	zboží	k1gNnSc4	zboží
podél	podél	k6eAd1	podél
pobřež	pobřež	k6eAd1	pobřež
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgFnPc2	některý
zpráv	zpráva	k1gFnPc2	zpráva
už	už	k6eAd1	už
egyptští	egyptský	k2eAgMnPc1d1	egyptský
mořeplavci	mořeplavec	k1gMnPc1	mořeplavec
obepluli	obeplout	k5eAaPmAgMnP	obeplout
Mys	mys	k1gInSc4	mys
Dobré	dobrý	k2eAgFnSc2d1	dobrá
naděje	naděje	k1gFnSc2	naděje
a	a	k8xC	a
Středozemním	středozemní	k2eAgNnSc7d1	středozemní
mořem	moře	k1gNnSc7	moře
se	se	k3xPyFc4	se
vrátili	vrátit	k5eAaPmAgMnP	vrátit
domů	domů	k6eAd1	domů
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgInPc1d2	veliký
čluny	člun	k1gInPc1	člun
dostaly	dostat	k5eAaPmAgInP	dostat
palubu	paluba	k1gFnSc4	paluba
a	a	k8xC	a
začaly	začít	k5eAaPmAgFnP	začít
se	se	k3xPyFc4	se
vyvíjet	vyvíjet	k5eAaImF	vyvíjet
v	v	k7c4	v
lodě	loď	k1gFnPc4	loď
<g/>
.	.	kIx.	.
</s>
<s>
Důležitou	důležitý	k2eAgFnSc7d1	důležitá
novinkou	novinka	k1gFnSc7	novinka
bylo	být	k5eAaImAgNnS	být
otočné	otočný	k2eAgNnSc1d1	otočné
kormidlo	kormidlo	k1gNnSc1	kormidlo
<g/>
,	,	kIx,	,
vetknuté	vetknutý	k2eAgNnSc1d1	vetknuté
mezi	mezi	k7c4	mezi
kýl	kýl	k1gInSc4	kýl
a	a	k8xC	a
palubu	paluba	k1gFnSc4	paluba
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
nahradilo	nahradit	k5eAaPmAgNnS	nahradit
dřívější	dřívější	k2eAgNnSc1d1	dřívější
kormidelní	kormidelní	k2eAgNnSc1d1	kormidelní
veslo	veslo	k1gNnSc1	veslo
<g/>
,	,	kIx,	,
zdokonalení	zdokonalení	k1gNnSc1	zdokonalení
plachet	plachta	k1gFnPc2	plachta
a	a	k8xC	a
motorový	motorový	k2eAgInSc4d1	motorový
pohon	pohon	k1gInSc4	pohon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Současnost	současnost	k1gFnSc4	současnost
==	==	k?	==
</s>
</p>
<p>
<s>
Nejmenší	malý	k2eAgInPc1d3	nejmenší
současné	současný	k2eAgInPc1d1	současný
čluny	člun	k1gInPc1	člun
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
z	z	k7c2	z
umělých	umělý	k2eAgFnPc2d1	umělá
hmot	hmota	k1gFnPc2	hmota
<g/>
,	,	kIx,	,
z	z	k7c2	z
laminátů	laminát	k1gInPc2	laminát
a	a	k8xC	a
z	z	k7c2	z
gumy	guma	k1gFnSc2	guma
<g/>
.	.	kIx.	.
</s>
<s>
Pozvolna	pozvolna	k6eAd1	pozvolna
nahrazují	nahrazovat	k5eAaImIp3nP	nahrazovat
ploché	plochý	k2eAgFnPc1d1	plochá
říční	říční	k2eAgFnPc1d1	říční
pramice	pramice	k1gFnPc1	pramice
z	z	k7c2	z
prken	prkno	k1gNnPc2	prkno
a	a	k8xC	a
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
rekreaci	rekreace	k1gFnSc3	rekreace
<g/>
,	,	kIx,	,
drobnému	drobný	k2eAgInSc3d1	drobný
rybolovu	rybolov	k1gInSc3	rybolov
a	a	k8xC	a
sportu	sport	k1gInSc3	sport
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgInPc1d2	veliký
rybářské	rybářský	k2eAgInPc1d1	rybářský
čluny	člun	k1gInPc1	člun
se	se	k3xPyFc4	se
modernizují	modernizovat	k5eAaBmIp3nP	modernizovat
<g/>
,	,	kIx,	,
staví	stavit	k5eAaBmIp3nS	stavit
z	z	k7c2	z
kovu	kov	k1gInSc2	kov
a	a	k8xC	a
umělých	umělý	k2eAgFnPc2d1	umělá
hmot	hmota	k1gFnPc2	hmota
a	a	k8xC	a
opatřují	opatřovat	k5eAaImIp3nP	opatřovat
nejen	nejen	k6eAd1	nejen
motory	motor	k1gInPc4	motor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
radary	radar	k1gInPc4	radar
<g/>
.	.	kIx.	.
</s>
<s>
Sportovní	sportovní	k2eAgInPc1d1	sportovní
čluny	člun	k1gInPc1	člun
se	se	k3xPyFc4	se
technicky	technicky	k6eAd1	technicky
velmi	velmi	k6eAd1	velmi
zdokonalily	zdokonalit	k5eAaPmAgFnP	zdokonalit
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
však	však	k9	však
prudce	prudko	k6eAd1	prudko
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
jejich	jejich	k3xOp3gFnSc1	jejich
cena	cena	k1gFnSc1	cena
a	a	k8xC	a
vybavením	vybavení	k1gNnSc7	vybavení
se	se	k3xPyFc4	se
často	často	k6eAd1	často
blíží	blížit	k5eAaImIp3nS	blížit
jachtám	jachta	k1gFnPc3	jachta
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
válečném	válečný	k2eAgNnSc6d1	válečné
námořnictvu	námořnictvo	k1gNnSc6	námořnictvo
není	být	k5eNaImIp3nS	být
hranice	hranice	k1gFnSc1	hranice
mezi	mezi	k7c7	mezi
člunem	člun	k1gInSc7	člun
a	a	k8xC	a
lodí	loď	k1gFnSc7	loď
přesně	přesně	k6eAd1	přesně
stanovena	stanoven	k2eAgFnSc1d1	stanovena
a	a	k8xC	a
spíš	spíš	k9	spíš
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c6	na
tradici	tradice	k1gFnSc6	tradice
se	se	k3xPyFc4	se
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
dělových	dělový	k2eAgInPc6d1	dělový
<g/>
,	,	kIx,	,
hlídkových	hlídkový	k2eAgInPc6d1	hlídkový
či	či	k8xC	či
raketových	raketový	k2eAgInPc6d1	raketový
člunech	člun	k1gInPc6	člun
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Boat	Boata	k1gFnPc2	Boata
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
člun	člun	k1gInSc1	člun
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Štěpán	Štěpán	k1gMnSc1	Štěpán
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
Hubert	Hubert	k1gMnSc1	Hubert
<g/>
,	,	kIx,	,
Motorové	motorový	k2eAgInPc1d1	motorový
čluny	člun	k1gInPc1	člun
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
SNTL	SNTL	kA	SNTL
1973	[number]	k4	1973
</s>
</p>
<p>
<s>
Peter	Peter	k1gMnSc1	Peter
White	Whit	k1gInSc5	Whit
<g/>
,	,	kIx,	,
Motorové	motorový	k2eAgInPc1d1	motorový
čluny	člun	k1gInPc1	člun
<g/>
:	:	kIx,	:
plavba	plavba	k1gFnSc1	plavba
<g/>
,	,	kIx,	,
ovládání	ovládání	k1gNnSc1	ovládání
<g/>
,	,	kIx,	,
údržba	údržba	k1gFnSc1	údržba
a	a	k8xC	a
užitečné	užitečný	k2eAgFnPc1d1	užitečná
rady	rada	k1gFnPc1	rada
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Yacht	Yacht	k1gInSc1	Yacht
2005	[number]	k4	2005
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Loď	loď	k1gFnSc1	loď
</s>
</p>
