<s>
Austrálie	Austrálie	k1gFnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
svazovém	svazový	k2eAgInSc6d1
státu	stát	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Austrálie	Austrálie	k1gFnSc2
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Australské	australský	k2eAgNnSc1d1
společenství	společenství	k1gNnSc1
Commonwealth	Commonwealth	k1gInSc1
of	of	k?
Australia	Australia	k1gFnSc1
</s>
<s>
vlajka	vlajka	k1gFnSc1
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
HymnaAdvance	HymnaAdvanec	k1gInPc1
Australia	Australia	k1gFnSc1
FairGod	FairGod	k1gInSc1
Save	Save	k1gFnSc1
the	the	k?
Queen	Queen	k1gInSc1
(	(	kIx(
<g/>
královská	královský	k2eAgFnSc1d1
<g/>
)	)	kIx)
Geografie	geografie	k1gFnSc1
</s>
<s>
Poloha	poloha	k1gFnSc1
Austrálie	Austrálie	k1gFnSc2
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
</s>
<s>
Canberra	Canberra	k1gFnSc1
Rozloha	rozloha	k1gFnSc1
</s>
<s>
7	#num#	k4
692	#num#	k4
024	#num#	k4
km²	km²	k?
(	(	kIx(
<g/>
6	#num#	k4
<g/>
.	.	kIx.
na	na	k7c6
světě	svět	k1gInSc6
<g/>
)	)	kIx)
z	z	k7c2
toho	ten	k3xDgNnSc2
1	#num#	k4
%	%	kIx~
vodní	vodní	k2eAgFnSc2d1
plochy	plocha	k1gFnSc2
Nejvyšší	vysoký	k2eAgInSc1d3
bod	bod	k1gInSc1
</s>
<s>
Mount	Mount	k1gMnSc1
Kosciuszko	Kosciuszka	k1gFnSc5
(	(	kIx(
<g/>
2	#num#	k4
228	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
Časové	časový	k2eAgNnSc1d1
pásmo	pásmo	k1gNnSc1
</s>
<s>
+8	+8	k4
–	–	k?
+10,5	+10,5	k4
Poloha	poloha	k1gFnSc1
</s>
<s>
28	#num#	k4
<g/>
°	°	k?
j.	j.	k?
š.	š.	k?
<g/>
,	,	kIx,
137	#num#	k4
<g/>
°	°	k?
v.	v.	k?
d.	d.	k?
Geodata	Geodat	k1gMnSc2
(	(	kIx(
<g/>
OSM	osm	k4xCc1
<g/>
)	)	kIx)
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
Obyvatelstvo	obyvatelstvo	k1gNnSc1
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
25	#num#	k4
390	#num#	k4
0	#num#	k4
<g/>
22	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
56	#num#	k4
<g/>
.	.	kIx.
na	na	k7c6
světě	svět	k1gInSc6
<g/>
,	,	kIx,
2020	#num#	k4
<g/>
)	)	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
2,8	2,8	k4
ob.	ob.	k?
/	/	kIx~
km²	km²	k?
(	(	kIx(
<g/>
224	#num#	k4
<g/>
.	.	kIx.
na	na	k7c6
světě	svět	k1gInSc6
<g/>
)	)	kIx)
HDI	HDI	kA
</s>
<s>
▬	▬	k?
0.933	0.933	k4
(	(	kIx(
<g/>
velmi	velmi	k6eAd1
vysoký	vysoký	k2eAgInSc1d1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
na	na	k7c6
světě	svět	k1gInSc6
<g/>
,	,	kIx,
2020	#num#	k4
<g/>
)	)	kIx)
Jazyk	jazyk	k1gInSc1
</s>
<s>
angličtina	angličtina	k1gFnSc1
Náboženství	náboženství	k1gNnSc2
</s>
<s>
katolíci	katolík	k1gMnPc1
26,8	26,8	k4
%	%	kIx~
<g/>
,	,	kIx,
anglikáni	anglikán	k1gMnPc1
21,8	21,8	k4
%	%	kIx~
<g/>
,	,	kIx,
ostatní	ostatní	k2eAgMnPc1d1
křesťané	křesťan	k1gMnPc1
2,6	2,6	k4
%	%	kIx~
<g/>
,	,	kIx,
muslimové	muslim	k1gMnPc1
1,1	1,1	k4
%	%	kIx~
<g/>
,	,	kIx,
buddhisté	buddhista	k1gMnPc1
1,1	1,1	k4
%	%	kIx~
<g/>
,	,	kIx,
hinduisté	hinduista	k1gMnPc1
0,4	0,4	k4
%	%	kIx~
<g/>
,	,	kIx,
ostatní	ostatní	k2eAgMnSc1d1
27,1	27,1	k4
%	%	kIx~
Státní	státní	k2eAgInSc1d1
útvar	útvar	k1gInSc1
Státní	státní	k2eAgInSc1d1
zřízení	zřízení	k1gNnSc4
</s>
<s>
konstituční	konstituční	k2eAgFnSc1d1
monarchie	monarchie	k1gFnSc1
Vznik	vznik	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1901	#num#	k4
(	(	kIx(
<g/>
nezávislost	nezávislost	k1gFnSc4
na	na	k7c6
Spojeném	spojený	k2eAgNnSc6d1
království	království	k1gNnSc6
<g/>
)	)	kIx)
Královna	královna	k1gFnSc1
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
II	II	kA
<g/>
.	.	kIx.
<g/>
reprezentovaná	reprezentovaný	k2eAgFnSc1d1
generálním	generální	k2eAgMnSc7d1
guvernérem	guvernér	k1gMnSc7
Davidem	David	k1gMnSc7
Hurleyem	Hurley	k1gMnSc7
Předseda	předseda	k1gMnSc1
vlády	vláda	k1gFnSc2
</s>
<s>
Scott	Scott	k2eAgInSc1d1
Morrison	Morrison	k1gInSc1
Měna	měna	k1gFnSc1
</s>
<s>
australský	australský	k2eAgInSc1d1
dolar	dolar	k1gInSc1
(	(	kIx(
<g/>
AUD	AUD	kA
<g/>
)	)	kIx)
HDP	HDP	kA
<g/>
/	/	kIx~
<g/>
obyv	obyv	k1gMnSc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
PPP	PPP	kA
<g/>
)	)	kIx)
</s>
<s>
45	#num#	k4
501	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
USD	USD	kA
(	(	kIx(
<g/>
19	#num#	k4
<g/>
.	.	kIx.
na	na	k7c6
světě	svět	k1gInSc6
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
)	)	kIx)
Mezinárodní	mezinárodní	k2eAgFnSc2d1
identifikace	identifikace	k1gFnSc2
ISO	ISO	kA
3166-1	3166-1	k4
</s>
<s>
036	#num#	k4
AUS	AUS	kA
AU	au	k0
MPZ	MPZ	kA
</s>
<s>
AUS	AUS	kA
Telefonní	telefonní	k2eAgFnSc1d1
předvolba	předvolba	k1gFnSc1
</s>
<s>
+61	+61	k4
Národní	národní	k2eAgInSc1d1
TLD	TLD	kA
</s>
<s>
<g/>
au	au	k0
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
</s>
<s>
Austrálie	Austrálie	k1gFnSc2
<g/>
,	,	kIx,
plným	plný	k2eAgInSc7d1
názvem	název	k1gInSc7
Australské	australský	k2eAgNnSc4d1
společenství	společenství	k1gNnSc4
(	(	kIx(
<g/>
též	též	k9
Australský	australský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
<g/>
,	,	kIx,
anglicky	anglicky	k6eAd1
Commonwealth	Commonwealth	k1gInSc1
of	of	k?
Australia	Australia	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
federativní	federativní	k2eAgNnSc1d1
stát	stát	k5eAaPmF,k5eAaImF
na	na	k7c6
jižní	jižní	k2eAgFnSc6d1
polokouli	polokoule	k1gFnSc6
nacházející	nacházející	k2eAgFnSc6d1
se	se	k3xPyFc4
na	na	k7c6
stejnojmenném	stejnojmenný	k2eAgInSc6d1
kontinentu	kontinent	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
pevninské	pevninský	k2eAgFnSc2d1
části	část	k1gFnSc2
ho	on	k3xPp3gMnSc4
tvoří	tvořit	k5eAaImIp3nP
i	i	k9
velký	velký	k2eAgInSc4d1
ostrov	ostrov	k1gInSc4
Tasmánie	Tasmánie	k1gFnSc2
a	a	k8xC
množství	množství	k1gNnSc2
menších	malý	k2eAgInPc2d2
ostrovů	ostrov	k1gInPc2
v	v	k7c6
Jižním	jižní	k2eAgNnSc6d1
<g/>
,	,	kIx,
Indickém	indický	k2eAgInSc6d1
a	a	k8xC
Tichém	tichý	k2eAgInSc6d1
oceánu	oceán	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
severu	sever	k1gInSc6
sousedí	sousedit	k5eAaImIp3nS
s	s	k7c7
Indonésií	Indonésie	k1gFnSc7
<g/>
,	,	kIx,
Východním	východní	k2eAgInSc7d1
Timorem	Timor	k1gInSc7
a	a	k8xC
Papuou	Papuý	k2eAgFnSc7d1
Novou	nový	k2eAgFnSc7d1
Guineou	Guinea	k1gFnSc7
<g/>
,	,	kIx,
na	na	k7c6
severovýchodě	severovýchod	k1gInSc6
se	s	k7c7
Šalomounovými	Šalomounův	k2eAgInPc7d1
ostrovy	ostrov	k1gInPc7
<g/>
,	,	kIx,
Vanuatu	Vanuat	k1gMnSc3
a	a	k8xC
Novou	nový	k2eAgFnSc4d1
Kaledonií	Kaledonie	k1gFnSc7
a	a	k8xC
na	na	k7c6
jihovýchodě	jihovýchod	k1gInSc6
s	s	k7c7
Novým	nový	k2eAgInSc7d1
Zélandem	Zéland	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Austrálie	Austrálie	k1gFnSc1
je	být	k5eAaImIp3nS
největší	veliký	k2eAgFnSc7d3
zemí	zem	k1gFnSc7
Oceánie	Oceánie	k1gFnSc2
a	a	k8xC
šestou	šestý	k4xOgFnSc7
největší	veliký	k2eAgFnSc7d3
zemí	zem	k1gFnSc7
na	na	k7c6
světě	svět	k1gInSc6
podle	podle	k7c2
celkové	celkový	k2eAgFnSc2d1
rozlohy	rozloha	k1gFnSc2
(	(	kIx(
<g/>
7	#num#	k4
617	#num#	k4
930	#num#	k4
kilometrů	kilometr	k1gInPc2
čtverečních	čtvereční	k2eAgInPc2d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc2
populace	populace	k1gFnSc2
<g/>
,	,	kIx,
skládající	skládající	k2eAgInPc4d1
se	se	k3xPyFc4
z	z	k7c2
26	#num#	k4
milionů	milion	k4xCgInPc2
lidí	člověk	k1gMnPc2
je	být	k5eAaImIp3nS
vysoce	vysoce	k6eAd1
urbanizovaná	urbanizovaný	k2eAgFnSc1d1
a	a	k8xC
vysoce	vysoce	k6eAd1
koncentrovaná	koncentrovaný	k2eAgFnSc1d1
na	na	k7c6
východním	východní	k2eAgNnSc6d1
pobřeží	pobřeží	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Hlavním	hlavní	k2eAgNnSc7d1
městem	město	k1gNnSc7
je	být	k5eAaImIp3nS
Canberra	Canberra	k1gFnSc1
a	a	k8xC
největším	veliký	k2eAgNnPc3d3
Sydney	Sydney	k1gNnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k1gNnSc1
důležité	důležitý	k2eAgFnSc2d1
městské	městský	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
v	v	k7c6
zemi	zem	k1gFnSc6
jsou	být	k5eAaImIp3nP
Melbourne	Melbourne	k1gNnSc4
<g/>
,	,	kIx,
Brisbane	Brisban	k1gMnSc5
<g/>
,	,	kIx,
Perth	Pertha	k1gFnPc2
a	a	k8xC
Adelaide	Adelaid	k1gInSc5
<g/>
.	.	kIx.
</s>
<s>
Austrálci	Austrálec	k1gMnPc1
osídlovali	osídlovat	k5eAaImAgMnP
Austrálii	Austrálie	k1gFnSc4
(	(	kIx(
<g/>
kontinent	kontinent	k1gInSc4
<g/>
)	)	kIx)
po	po	k7c4
asi	asi	k9
65	#num#	k4
000	#num#	k4
let	léto	k1gNnPc2
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
před	před	k7c7
prvním	první	k4xOgInSc7
příjezdem	příjezd	k1gInSc7
průzkumníku	průzkumník	k1gMnSc3
Spojených	spojený	k2eAgFnPc2d1
provincií	provincie	k1gFnPc2
nizozemských	nizozemský	k2eAgFnPc2d1
(	(	kIx(
<g/>
dnešního	dnešní	k2eAgNnSc2d1
Nizozemska	Nizozemsko	k1gNnSc2
<g/>
)	)	kIx)
na	na	k7c6
začátku	začátek	k1gInSc6
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
území	území	k1gNnPc4
pojmenovali	pojmenovat	k5eAaPmAgMnP
jako	jako	k8xC,k8xS
Nové	Nové	k2eAgNnSc4d1
Holandsko	Holandsko	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1770	#num#	k4
si	se	k3xPyFc3
východní	východní	k2eAgFnSc4d1
část	část	k1gFnSc4
Austrálie	Austrálie	k1gFnSc2
vyžádalo	vyžádat	k5eAaPmAgNnS
Království	království	k1gNnSc1
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
a	a	k8xC
původně	původně	k6eAd1
se	se	k3xPyFc4
usadila	usadit	k5eAaPmAgFnS
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
transpontovala	transpontovat	k5eAaBmAgFnS,k5eAaPmAgFnS,k5eAaImAgFnS
trestance	trestanec	k1gMnSc4
do	do	k7c2
kolonie	kolonie	k1gFnSc2
Nový	nový	k2eAgInSc1d1
Jižní	jižní	k2eAgInSc1d1
Wales	Wales	k1gInSc1
od	od	k7c2
26	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1788	#num#	k4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
datum	datum	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
australským	australský	k2eAgInSc7d1
národním	národní	k2eAgInSc7d1
dnem	den	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Populace	populace	k1gFnSc1
v	v	k7c6
následujících	následující	k2eAgNnPc6d1
desetiletích	desetiletí	k1gNnPc6
neustále	neustále	k6eAd1
rostla	růst	k5eAaImAgFnS
a	a	k8xC
v	v	k7c6
době	doba	k1gFnSc6
zlaté	zlatý	k2eAgFnSc2d1
horečky	horečka	k1gFnSc2
v	v	k7c6
padesátých	padesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
byla	být	k5eAaImAgFnS
většina	většina	k1gFnSc1
kontinentu	kontinent	k1gInSc2
prozkoumána	prozkoumán	k2eAgFnSc1d1
evropskými	evropský	k2eAgMnPc7d1
osadníky	osadník	k1gMnPc7
a	a	k8xC
bylo	být	k5eAaImAgNnS
založeno	založit	k5eAaPmNgNnS
dalších	další	k2eAgFnPc2d1
pět	pět	k4xCc1
samosprávných	samosprávný	k2eAgFnPc2d1
kolonií	kolonie	k1gFnPc2
koruny	koruna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1901	#num#	k4
se	se	k3xPyFc4
šest	šest	k4xCc1
kolonií	kolonie	k1gFnPc2
federovalo	federovat	k5eAaImAgNnS
a	a	k8xC
vytvořilo	vytvořit	k5eAaPmAgNnS
Australské	australský	k2eAgNnSc1d1
společenství	společenství	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Austrálie	Austrálie	k1gFnSc1
si	se	k3xPyFc3
od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
udržuje	udržovat	k5eAaImIp3nS
stabilní	stabilní	k2eAgInSc4d1
liberálně	liberálně	k6eAd1
demokratický	demokratický	k2eAgInSc4d1
politický	politický	k2eAgInSc4d1
systém	systém	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
funguje	fungovat	k5eAaImIp3nS
jako	jako	k9
federální	federální	k2eAgFnSc2d1
parlamentní	parlamentní	k2eAgFnSc2d1
konstituční	konstituční	k2eAgFnSc2d1
monarchie	monarchie	k1gFnSc2
a	a	k8xC
zahrnuje	zahrnovat	k5eAaImIp3nS
šest	šest	k4xCc1
států	stát	k1gInPc2
a	a	k8xC
deset	deset	k4xCc4
teritotií	teritotie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
je	být	k5eAaImIp3nS
nejstarší	starý	k2eAgFnSc1d3
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
nejplošší	plochý	k2eAgNnPc1d3
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
nejsušší	suchý	k2eAgInSc4d3
obydlený	obydlený	k2eAgInSc4d1
kontinent	kontinent	k1gInSc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
s	s	k7c7
nejméně	málo	k6eAd3
úrodnou	úrodný	k2eAgFnSc7d1
půdou	půda	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Má	mít	k5eAaImIp3nS
rozlohu	rozloha	k1gFnSc4
7	#num#	k4
617	#num#	k4
930	#num#	k4
kilometrů	kilometr	k1gInPc2
čtverečních	čtvereční	k2eAgInPc2d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
velice	velice	k6eAd1
různorodá	různorodý	k2eAgFnSc1d1
země	země	k1gFnSc1
<g/>
,	,	kIx,
její	její	k3xOp3gFnSc1
velikost	velikost	k1gFnSc1
jí	on	k3xPp3gFnSc3
dává	dávat	k5eAaImIp3nS
širokou	široký	k2eAgFnSc4d1
škálu	škála	k1gFnSc4
krajina	krajina	k1gFnSc1
a	a	k8xC
podnebí	podnebí	k1gNnSc1
<g/>
,	,	kIx,
s	s	k7c7
pouští	poušť	k1gFnSc7
ve	v	k7c6
vnitrozemí	vnitrozemí	k1gNnSc6
<g/>
,	,	kIx,
tropickými	tropický	k2eAgInPc7d1
deštnými	deštný	k2eAgInPc7d1
pralesy	prales	k1gInPc7
na	na	k7c6
severovýchodě	severovýchod	k1gInSc6
a	a	k8xC
horskými	horský	k2eAgNnPc7d1
pásmy	pásmo	k1gNnPc7
na	na	k7c6
jihovýchodě	jihovýchod	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Austrálie	Austrálie	k1gFnSc1
generuje	generovat	k5eAaImIp3nS
své	svůj	k3xOyFgInPc4
příjmy	příjem	k1gInPc4
z	z	k7c2
různých	různý	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
exportu	export	k1gInSc2
souvisejícího	související	k2eAgMnSc4d1
s	s	k7c7
těžbou	těžba	k1gFnSc7
<g/>
,	,	kIx,
telekomunikací	telekomunikace	k1gFnSc7
<g/>
,	,	kIx,
bankovnictvím	bankovnictví	k1gNnSc7
<g/>
,	,	kIx,
výrobou	výroba	k1gFnSc7
a	a	k8xC
mezinárodním	mezinárodní	k2eAgNnSc7d1
vzděláváním	vzdělávání	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
je	být	k5eAaImIp3nS
vyspělá	vyspělý	k2eAgFnSc1d1
země	země	k1gFnSc1
s	s	k7c7
třináctou	třináctý	k4xOgFnSc7
největší	veliký	k2eAgFnSc7d3
ekonomikou	ekonomika	k1gFnSc7
na	na	k7c6
světě	svět	k1gInSc6
a	a	k8xC
desátým	desátý	k4xOgInSc7
nejvyšším	vysoký	k2eAgInSc7d3
příjmem	příjem	k1gInSc7
na	na	k7c4
obyvatele	obyvatel	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
považována	považován	k2eAgFnSc1d1
za	za	k7c4
regionální	regionální	k2eAgFnSc4d1
mocnost	mocnost	k1gFnSc4
a	a	k8xC
má	mít	k5eAaImIp3nS
třinácté	třináctý	k4xOgInPc4
nejvyšší	vysoký	k2eAgInPc4d3
vojenské	vojenský	k2eAgInPc4d1
výdaje	výdaj	k1gInPc4
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přistěhovalci	přistěhovalec	k1gMnPc1
tvoří	tvořit	k5eAaImIp3nP
30	#num#	k4
<g/>
%	%	kIx~
populace	populace	k1gFnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
nejvyšší	vysoký	k2eAgInSc1d3
podíl	podíl	k1gInSc1
ve	v	k7c6
všech	všecek	k3xTgFnPc6
zemích	zem	k1gFnPc6
s	s	k7c7
počtem	počet	k1gInSc7
obyvatel	obyvatel	k1gMnPc2
nad	nad	k7c4
10	#num#	k4
milionů	milion	k4xCgInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
Země	zem	k1gFnPc1
se	se	k3xPyFc4
řadí	řadit	k5eAaImIp3nP
na	na	k7c4
první	první	k4xOgNnSc4
místo	místo	k1gNnSc4
v	v	k7c6
oblasti	oblast	k1gFnSc6
zdraví	zdraví	k1gNnSc2
<g/>
,	,	kIx,
vzdělání	vzdělání	k1gNnSc2
<g/>
,	,	kIx,
ekonomické	ekonomický	k2eAgFnSc2d1
svobody	svoboda	k1gFnSc2
a	a	k8xC
občanských	občanský	k2eAgNnPc2d1
práv	právo	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
je	být	k5eAaImIp3nS
členem	člen	k1gInSc7
OSN	OSN	kA
<g/>
,	,	kIx,
G	G	kA
<g/>
20	#num#	k4
<g/>
,	,	kIx,
Commonwealthu	Commonwealtha	k1gFnSc4
<g/>
,	,	kIx,
ANZUS	ANZUS	kA
<g/>
,	,	kIx,
Organizace	organizace	k1gFnSc1
pro	pro	k7c4
hospodářskou	hospodářský	k2eAgFnSc4d1
spolupráci	spolupráce	k1gFnSc4
a	a	k8xC
rozvoj	rozvoj	k1gInSc4
(	(	kIx(
<g/>
OECD	OECD	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Světové	světový	k2eAgFnSc2d1
obchodní	obchodní	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
<g/>
,	,	kIx,
Asijsko-pacifické	asijsko-pacifický	k2eAgFnSc2d1
hospodářské	hospodářský	k2eAgFnSc2d1
spolupráce	spolupráce	k1gFnSc2
<g/>
,	,	kIx,
Fóra	fórum	k1gNnSc2
tichomořských	tichomořský	k2eAgInPc2d1
ostrovů	ostrov	k1gInPc2
a	a	k8xC
ASEAN	ASEAN	kA
Plus	plus	k1gInSc1
Six	Six	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Název	název	k1gInSc1
</s>
<s>
Pohled	pohled	k1gInSc1
na	na	k7c4
Port	port	k1gInSc4
Jackson	Jackson	k1gNnSc4
<g/>
,	,	kIx,
místo	místo	k1gNnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
bylo	být	k5eAaImAgNnS
později	pozdě	k6eAd2
založeno	založit	k5eAaPmNgNnS
Sydney	Sydney	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Jméno	jméno	k1gNnSc1
Austrálie	Austrálie	k1gFnSc2
je	být	k5eAaImIp3nS
odvozeno	odvodit	k5eAaPmNgNnS
z	z	k7c2
latinského	latinský	k2eAgInSc2d1
Terra	Terr	k1gInSc2
Australis	Australis	k1gFnSc2
(	(	kIx(
<g/>
„	„	k?
<g/>
jižní	jižní	k2eAgFnSc2d1
země	zem	k1gFnSc2
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
název	název	k1gInSc1
používaný	používaný	k2eAgInSc1d1
pro	pro	k7c4
hypotetický	hypotetický	k2eAgInSc4d1
kontinent	kontinent	k1gInSc4
na	na	k7c6
jižní	jižní	k2eAgFnSc6d1
polokouli	polokoule	k1gFnSc6
už	už	k6eAd1
od	od	k7c2
starověku	starověk	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
Když	když	k8xS
Evropané	Evropan	k1gMnPc1
začali	začít	k5eAaPmAgMnP
v	v	k7c6
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
poprvé	poprvé	k6eAd1
navštěvovat	navštěvovat	k5eAaImF
a	a	k8xC
mapovat	mapovat	k5eAaImF
Austrálii	Austrálie	k1gFnSc4
<g/>
,	,	kIx,
název	název	k1gInSc4
Terra	Terr	k1gInSc2
Australis	Australis	k1gFnSc2
se	se	k3xPyFc4
přirozeně	přirozeně	k6eAd1
aplikoval	aplikovat	k5eAaBmAgMnS
na	na	k7c4
nová	nový	k2eAgNnPc4d1
území	území	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s>
Až	až	k6eAd1
do	do	k7c2
počátku	počátek	k1gInSc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
byla	být	k5eAaImAgFnS
Austrálie	Austrálie	k1gFnSc1
nejvíce	nejvíce	k6eAd1,k6eAd3
známá	známý	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
„	„	k?
<g/>
Nové	Nové	k2eAgNnSc1d1
Holandsko	Holandsko	k1gNnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
jménem	jméno	k1gNnSc7
<g/>
,	,	kIx,
které	který	k3yQgNnSc4,k3yIgNnSc4,k3yRgNnSc4
poprvé	poprvé	k6eAd1
použil	použít	k5eAaPmAgMnS
nizozemský	nizozemský	k2eAgMnSc1d1
průzkumník	průzkumník	k1gMnSc1
Abel	Abel	k1gMnSc1
Tasman	Tasman	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1644	#num#	k4
(	(	kIx(
<g/>
jako	jako	k8xC,k8xS
Nieuw-Holland	Nieuw-Holland	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
následně	následně	k6eAd1
poangličtěné	poangličtěný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Název	název	k1gInSc4
Austrálie	Austrálie	k1gFnSc2
popularizoval	popularizovat	k5eAaImAgMnS
průzkumník	průzkumník	k1gMnSc1
Matthew	Matthew	k1gMnSc1
Flinders	Flinders	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
řekl	říct	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
„	„	k?
<g/>
příjemnější	příjemný	k2eAgFnSc7d2
pro	pro	k7c4
ucho	ucho	k1gNnSc4
a	a	k8xC
je	být	k5eAaImIp3nS
podobné	podobný	k2eAgNnSc1d1
ke	k	k7c3
jménům	jméno	k1gNnPc3
dalších	další	k2eAgInPc2d1
světadílů	světadíl	k1gInPc2
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Několik	několik	k4yIc1
raných	raný	k2eAgMnPc2d1
kartografů	kartograf	k1gMnPc2
také	také	k6eAd1
použilo	použít	k5eAaPmAgNnS
slovo	slovo	k1gNnSc1
Austrálie	Austrálie	k1gFnSc2
na	na	k7c6
mapách	mapa	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gerardus	Gerardus	k1gMnSc1
Mercator	Mercator	k1gMnSc1
(	(	kIx(
<g/>
1512	#num#	k4
<g/>
–	–	k?
<g/>
1594	#num#	k4
<g/>
)	)	kIx)
použil	použít	k5eAaPmAgMnS
na	na	k7c6
své	svůj	k3xOyFgFnSc6
dvojité	dvojitý	k2eAgFnSc6d1
cordiformní	cordiformní	k2eAgFnSc6d1
mapě	mapa	k1gFnSc6
světa	svět	k1gInSc2
z	z	k7c2
roku	rok	k1gInSc2
1538	#num#	k4
frázi	fráze	k1gFnSc4
climata	clima	k1gNnPc1
australia	australia	k1gFnSc1
(	(	kIx(
<g/>
klima	klima	k1gNnSc1
Austrálie	Austrálie	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
Gemma	Gemma	k1gFnSc1
Frisius	Frisius	k1gMnSc1
(	(	kIx(
<g/>
1508	#num#	k4
<g/>
–	–	k?
<g/>
1555	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yIgInSc4,k3yRgInSc4,k3yQgInSc4
byla	být	k5eAaImAgFnS
Mercatorovým	Mercatorův	k2eAgMnSc7d1
učitelem	učitel	k1gMnSc7
a	a	k8xC
spolupracovníkem	spolupracovník	k1gMnSc7
<g/>
,	,	kIx,
na	na	k7c6
své	svůj	k3xOyFgFnSc6
vlastní	vlastní	k2eAgFnSc6d1
mapě	mapa	k1gFnSc6
cordiform	cordiform	k1gInSc1
wall	walnout	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
1540	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Austrálie	Austrálie	k1gFnSc1
se	se	k3xPyFc4
objevuje	objevovat	k5eAaImIp3nS
v	v	k7c6
knize	kniha	k1gFnSc6
o	o	k7c4
astronomii	astronomie	k1gFnSc4
od	od	k7c2
Cyriaca	Cyriac	k1gInSc2
Jacoba	Jacoba	k1gFnSc1
zum	zum	k?
Bartha	Bartha	k1gFnSc1
vydané	vydaný	k2eAgNnSc4d1
ve	v	k7c6
Frankfurtu	Frankfurt	k1gInSc6
nad	nad	k7c7
Mohanem	Mohan	k1gInSc7
v	v	k7c6
roce	rok	k1gInSc6
1545	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zdá	zdát	k5eAaPmIp3nS,k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
jméno	jméno	k1gNnSc1
Austrálie	Austrálie	k1gFnSc2
bylo	být	k5eAaImAgNnS
oficiálně	oficiálně	k6eAd1
použito	použít	k5eAaPmNgNnS
<g/>
,	,	kIx,
v	v	k7c6
dubnu	duben	k1gInSc6
1817	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
guvernér	guvernér	k1gMnSc1
Lachlan	Lachlan	k1gMnSc1
Macquarie	Macquarie	k1gFnSc2
potvrdil	potvrdit	k5eAaPmAgMnS
přijetí	přijetí	k1gNnSc4
Flindersových	Flindersův	k2eAgFnPc2d1
map	mapa	k1gFnPc2
Austrálie	Austrálie	k1gFnSc2
od	od	k7c2
lorda	lord	k1gMnSc2
Bathursta	Bathurst	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
prosinci	prosinec	k1gInSc6
1817	#num#	k4
Macquarie	Macquarie	k1gFnSc1
doporučil	doporučit	k5eAaPmAgInS
Colonial	Colonial	k1gInSc1
Office	Office	kA
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
bylo	být	k5eAaImAgNnS
formálně	formálně	k6eAd1
přijato	přijmout	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1824	#num#	k4
se	se	k3xPyFc4
admirálové	admirál	k1gMnPc1
dohodli	dohodnout	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
kontinent	kontinent	k1gInSc1
by	by	kYmCp3nS
měl	mít	k5eAaImAgInS
být	být	k5eAaImF
oficiálně	oficiálně	k6eAd1
znám	znám	k2eAgMnSc1d1
pod	pod	k7c7
tímto	tento	k3xDgNnSc7
jménem	jméno	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
První	první	k4xOgFnSc4
oficiální	oficiální	k2eAgFnSc4d1
publikované	publikovaný	k2eAgNnSc4d1
použití	použití	k1gNnSc4
nového	nový	k2eAgNnSc2d1
jména	jméno	k1gNnSc2
přišlo	přijít	k5eAaPmAgNnS
s	s	k7c7
vydáním	vydání	k1gNnSc7
The	The	k1gFnSc2
Australia	Australia	k1gFnSc1
Directory	Director	k1gInPc1
v	v	k7c6
roce	rok	k1gInSc6
1830	#num#	k4
Hydrografickou	hydrografický	k2eAgFnSc7d1
kanceláří	kancelář	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hovorové	hovorový	k2eAgInPc1d1
názvy	název	k1gInPc1
pro	pro	k7c4
Austrálii	Austrálie	k1gFnSc4
jsou	být	k5eAaImIp3nP
například	například	k6eAd1
„	„	k?
<g/>
Oz	Oz	k1gMnSc1
<g/>
“	“	k?
a	a	k8xC
„	„	k?
<g/>
Land	Land	k1gMnSc1
Down	Down	k1gMnSc1
Under	Under	k1gMnSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
obvykle	obvykle	k6eAd1
zkráceno	zkrátit	k5eAaPmNgNnS
na	na	k7c6
„	„	k?
<g/>
Down	Down	k1gMnSc1
Under	Under	k1gMnSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgNnPc1d1
epiteta	epiteton	k1gNnPc1
zahrnují	zahrnovat	k5eAaImIp3nP
„	„	k?
<g/>
Velkou	velký	k2eAgFnSc4d1
jižní	jižní	k2eAgFnSc4d1
zemi	zem	k1gFnSc4
<g/>
“	“	k?
<g/>
,	,	kIx,
„	„	k?
<g/>
Lucky	lucky	k6eAd1
Country	country	k2eAgInPc7d1
<g/>
“	“	k?
<g/>
,	,	kIx,
„	„	k?
<g/>
Sunburnt	Sunburnt	k1gInSc4
Country	country	k2eAgFnSc7d1
<g/>
“	“	k?
a	a	k8xC
„	„	k?
<g/>
Wide	Wide	k1gInSc1
Brown	Brown	k1gMnSc1
Land	Land	k1gMnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgFnSc7d1
dva	dva	k4xCgInPc1
jsou	být	k5eAaImIp3nP
odvozeny	odvodit	k5eAaPmNgInP
z	z	k7c2
básně	báseň	k1gFnSc2
Dorothey	Dorothea	k1gMnSc2
Mackellar	Mackellara	k1gFnPc2
z	z	k7c2
roku	rok	k1gInSc2
1908	#num#	k4
„	„	k?
<g/>
Moje	můj	k3xOp1gFnPc1
země	zem	k1gFnPc1
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dějiny	dějiny	k1gFnPc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Dějiny	dějiny	k1gFnPc4
Austrálie	Austrálie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Skalní	skalní	k2eAgFnPc1d1
malby	malba	k1gFnPc1
Austrálců	Austrálec	k1gMnPc2
(	(	kIx(
<g/>
Aboriginců	Aboriginec	k1gMnPc2
<g/>
)	)	kIx)
v	v	k7c6
národním	národní	k2eAgInSc6d1
parku	park	k1gInSc6
Kakadu	kakadu	k1gMnPc2
</s>
<s>
Pravěk	pravěk	k1gInSc1
</s>
<s>
Bližší	blízký	k2eAgFnPc4d2
informace	informace	k1gFnPc4
<g/>
:	:	kIx,
Austrálci	Austrálec	k1gMnPc1
</s>
<s>
Je	být	k5eAaImIp3nS
známo	znám	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
lidské	lidský	k2eAgNnSc1d1
osidlování	osidlování	k1gNnSc1
Austrálie	Austrálie	k1gFnSc2
jako	jako	k8xC,k8xS
kontinentu	kontinent	k1gInSc2
začalo	začít	k5eAaPmAgNnS
před	před	k7c7
minimálně	minimálně	k6eAd1
65	#num#	k4
000	#num#	k4
lety	let	k1gInPc7
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
s	s	k7c7
migrací	migrace	k1gFnSc7
lidí	člověk	k1gMnPc2
přes	přes	k7c4
pozemní	pozemní	k2eAgInPc4d1
mosty	most	k1gInPc4
a	a	k8xC
krátké	krátký	k2eAgInPc4d1
námořní	námořní	k2eAgInPc4d1
přechody	přechod	k1gInPc4
z	z	k7c2
dnešní	dnešní	k2eAgFnSc2d1
jihovýchodní	jihovýchodní	k2eAgFnSc2d1
Asie	Asie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
Obvyklý	obvyklý	k2eAgInSc1d1
<g />
.	.	kIx.
</s>
<s hack="1">
názor	názor	k1gInSc1
za	za	k7c4
příčinu	příčina	k1gFnSc4
pohybu	pohyb	k1gInSc2
obyvatel	obyvatel	k1gMnPc2
jihovýchodní	jihovýchodní	k2eAgFnSc2d1
Asie	Asie	k1gFnSc2
na	na	k7c4
jih	jih	k1gInSc4
označuje	označovat	k5eAaImIp3nS
klimatickou	klimatický	k2eAgFnSc4d1
změnu	změna	k1gFnSc4
<g/>
,	,	kIx,
přesněji	přesně	k6eAd2
dobu	doba	k1gFnSc4
ledovou	ledový	k2eAgFnSc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
australského	australský	k2eAgMnSc2d1
archeologa	archeolog	k1gMnSc2
Scotta	Scotta	k1gFnSc1
Canea	Canea	k1gFnSc1
byla	být	k5eAaImAgFnS
příčinou	příčina	k1gFnSc7
migrace	migrace	k1gFnSc2
tzv.	tzv.	kA
tobská	tobská	k1gFnSc1
katastrofa	katastrofa	k1gFnSc1
<g/>
,	,	kIx,
k	k	k7c3
níž	jenž	k3xRgFnSc3
došlo	dojít	k5eAaPmAgNnS
72	#num#	k4
000	#num#	k4
let	léto	k1gNnPc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
Šlo	jít	k5eAaImAgNnS
o	o	k7c6
erupci	erupce	k1gFnSc6
supervulkánu	supervulkán	k2eAgFnSc4d1
Toba	Tob	k2eAgMnSc4d1
v	v	k7c6
Indonésii	Indonésie	k1gFnSc6
na	na	k7c6
ostrově	ostrov	k1gInSc6
Sumatra	Sumatra	k1gFnSc1
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
je	být	k5eAaImIp3nS
někdy	někdy	k6eAd1
připisováno	připisován	k2eAgNnSc4d1
poslední	poslední	k2eAgNnSc4d1
velké	velký	k2eAgNnSc4d1
vymírání	vymírání	k1gNnSc4
druhů	druh	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skalní	skalní	k2eAgInSc4d1
úkryt	úkryt	k1gInSc4
Madjedbebe	Madjedbeb	k1gInSc5
<g/>
,	,	kIx,
v	v	k7c6
Arnhemské	Arnhemský	k2eAgFnSc6d1
zemi	zem	k1gFnSc6
je	být	k5eAaImIp3nS
považován	považován	k2eAgInSc1d1
za	za	k7c4
nejstarší	starý	k2eAgNnSc4d3
místo	místo	k1gNnSc4
ukazující	ukazující	k2eAgFnSc1d1
přítomnost	přítomnost	k1gFnSc1
lidí	člověk	k1gMnPc2
v	v	k7c6
Austrálii	Austrálie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
Nejstarší	starý	k2eAgInPc1d3
nalezené	nalezený	k2eAgInPc1d1
lidské	lidský	k2eAgInPc1d1
ostatky	ostatek	k1gInPc1
jsou	být	k5eAaImIp3nP
pozůstatky	pozůstatek	k1gInPc1
v	v	k7c6
jezeru	jezero	k1gNnSc6
Mungo	mungo	k1gMnSc1
<g/>
,	,	kIx,
na	na	k7c6
jihozápadě	jihozápad	k1gInSc6
Nového	Nového	k2eAgInSc2d1
Jižního	jižní	k2eAgInSc2d1
Walesu	Wales	k1gInSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
byly	být	k5eAaImAgInP
datovány	datovat	k5eAaImNgInP
zhruba	zhruba	k6eAd1
na	na	k7c4
dobu	doba	k1gFnSc4
před	před	k7c7
41	#num#	k4
000	#num#	k4
lety	let	k1gInPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
Tito	tento	k3xDgMnPc1
lidé	člověk	k1gMnPc1
byli	být	k5eAaImAgMnP
předky	předek	k1gInPc4
moderních	moderní	k2eAgMnPc2d1
domorodých	domorodý	k2eAgMnPc2d1
Australanů	Australan	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
Austrálci	Austrálec	k1gMnPc1
používali	používat	k5eAaImAgMnP
kamenné	kamenný	k2eAgInPc4d1
nástroje	nástroj	k1gInPc4
a	a	k8xC
po	po	k7c6
svém	svůj	k3xOyFgInSc6
příchodu	příchod	k1gInSc6
brzy	brzy	k6eAd1
vyhubili	vyhubit	k5eAaPmAgMnP
obří	obří	k2eAgMnPc4d1
vačnatce	vačnatec	k1gMnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
Krajinu	Krajina	k1gFnSc4
změnili	změnit	k5eAaPmAgMnP
žďářením	žďáření	k1gNnSc7
<g/>
,	,	kIx,
kterým	který	k3yRgInSc7,k3yIgInSc7,k3yQgInSc7
si	se	k3xPyFc3
ulehčovali	ulehčovat	k5eAaImAgMnP
lov	lov	k1gInSc4
zvířat	zvíře	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
Krajina	Krajina	k1gFnSc1
tak	tak	k6eAd1
získala	získat	k5eAaPmAgFnS
většinově	většinově	k6eAd1
charakter	charakter	k1gInSc4
savany	savana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Domorodá	domorodý	k2eAgFnSc1d1
australská	australský	k2eAgFnSc1d1
kultura	kultura	k1gFnSc1
je	být	k5eAaImIp3nS
jednou	jeden	k4xCgFnSc7
z	z	k7c2
nejstarších	starý	k2eAgFnPc2d3
kontinuálních	kontinuální	k2eAgFnPc2d1
kultur	kultura	k1gFnPc2
na	na	k7c6
Zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
době	doba	k1gFnSc6
prvního	první	k4xOgInSc2
kontaktu	kontakt	k1gInSc2
s	s	k7c7
Evropany	Evropan	k1gMnPc7
byla	být	k5eAaImAgFnS
většina	většina	k1gFnSc1
domorodých	domorodý	k2eAgMnPc2d1
Australanů	Australan	k1gMnPc2
lovci	lovec	k1gMnPc1
a	a	k8xC
sběrači	sběrač	k1gMnPc1
se	s	k7c7
složitými	složitý	k2eAgFnPc7d1
ekonomikami	ekonomika	k1gFnPc7
a	a	k8xC
společnostmi	společnost	k1gFnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
Nedávné	dávný	k2eNgInPc1d1
archeologické	archeologický	k2eAgInPc1d1
nálezy	nález	k1gInPc1
naznačují	naznačovat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
bylo	být	k5eAaImAgNnS
možné	možný	k2eAgNnSc1d1
udržet	udržet	k5eAaPmF
populaci	populace	k1gFnSc4
750	#num#	k4
000	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
Domorodí	domorodý	k2eAgMnPc1d1
Australani	Australan	k1gMnPc1
mají	mít	k5eAaImIp3nP
<g />
.	.	kIx.
</s>
<s hack="1">
ústní	ústní	k2eAgFnSc4d1
tradici	tradice	k1gFnSc4
s	s	k7c7
duchovními	duchovní	k2eAgFnPc7d1
hodnotami	hodnota	k1gFnPc7
založenými	založený	k2eAgFnPc7d1
na	na	k7c6
úctě	úcta	k1gFnSc6
k	k	k7c3
zemi	zem	k1gFnSc3
a	a	k8xC
víře	víra	k1gFnSc6
v	v	k7c4
"	"	kIx"
<g/>
Čas	čas	k1gInSc4
snů	sen	k1gInPc2
<g/>
"	"	kIx"
<g/>
,	,	kIx,
tedy	tedy	k9
ve	v	k7c4
zlatou	zlatý	k2eAgFnSc4d1
éru	éra	k1gFnSc4
na	na	k7c6
počátku	počátek	k1gInSc6
světa	svět	k1gInSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
kdy	kdy	k6eAd1
totemové	totemový	k2eAgFnPc1d1
bytosti	bytost	k1gFnPc1
stvořily	stvořit	k5eAaPmAgFnP
svět	svět	k1gInSc4
<g/>
,	,	kIx,
stanovily	stanovit	k5eAaPmAgInP
zákony	zákon	k1gInPc1
a	a	k8xC
přikázaly	přikázat	k5eAaPmAgInP
obřady	obřad	k1gInPc1
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc4
je	být	k5eAaImIp3nS
nutné	nutný	k2eAgNnSc1d1
provádět	provádět	k5eAaImF
k	k	k7c3
zajištění	zajištění	k1gNnSc3
kontinuity	kontinuita	k1gFnSc2
života	život	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Domorodé	domorodý	k2eAgNnSc1d1
umění	umění	k1gNnSc1
Austrálců	Austrálec	k1gMnPc2
je	být	k5eAaImIp3nS
považováno	považován	k2eAgNnSc1d1
za	za	k7c4
nejstarší	starý	k2eAgFnSc4d3
pokračující	pokračující	k2eAgFnSc4d1
uměleckou	umělecký	k2eAgFnSc4d1
tradici	tradice	k1gFnSc4
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Doklady	doklad	k1gInPc4
o	o	k7c6
něm	on	k3xPp3gInSc6
lze	lze	k6eAd1
vysledovat	vysledovat	k5eAaImF,k5eAaPmF
nejméně	málo	k6eAd3
30	#num#	k4
000	#num#	k4
let	léto	k1gNnPc2
nazpět	nazpět	k6eAd1
a	a	k8xC
nacházejí	nacházet	k5eAaImIp3nP
se	se	k3xPyFc4
v	v	k7c6
celé	celý	k2eAgFnSc6d1
Austrálii	Austrálie	k1gFnSc6
<g/>
,	,	kIx,
zejména	zejména	k9
v	v	k7c6
blízkosti	blízkost	k1gFnSc6
Uluru	Ulur	k1gInSc2
a	a	k8xC
v	v	k7c6
národních	národní	k2eAgInPc6d1
parcích	park	k1gInPc6
Kakadu	kakadu	k1gMnPc2
či	či	k8xC
Ku-ring-gai	Ku-ring-ga	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dělili	dělit	k5eAaImAgMnP
se	se	k3xPyFc4
na	na	k7c4
zhruba	zhruba	k6eAd1
250	#num#	k4
kmenů	kmen	k1gInPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgInPc2
každý	každý	k3xTgInSc1
měl	mít	k5eAaImAgInS
vlastní	vlastní	k2eAgInSc1d1
jazyk	jazyk	k1gInSc1
<g/>
;	;	kIx,
200	#num#	k4
z	z	k7c2
těchto	tento	k3xDgMnPc2
jazyků	jazyk	k1gInPc2
již	již	k9
zcela	zcela	k6eAd1
zaniklo	zaniknout	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největší	veliký	k2eAgFnSc1d3
hustota	hustota	k1gFnSc1
obyvatelstva	obyvatelstvo	k1gNnSc2
se	se	k3xPyFc4
předpokládá	předpokládat	k5eAaImIp3nS
v	v	k7c6
údolí	údolí	k1gNnSc6
řeky	řeka	k1gFnSc2
Murray	Murraa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostrovy	ostrov	k1gInPc1
Torres	Torres	k1gMnSc1
Strait	Strait	k2eAgInSc1d1
Island	Island	k1gInSc1
<g/>
,	,	kIx,
etnicky	etnicky	k6eAd1
melaneské	melaneský	k2eAgFnPc1d1
<g/>
,	,	kIx,
se	se	k3xPyFc4
živily	živit	k5eAaImAgFnP
sezónním	sezónní	k2eAgNnSc7d1
zahradnictvím	zahradnictví	k1gNnSc7
a	a	k8xC
zdroji	zdroj	k1gInPc7
svých	svůj	k3xOyFgInPc2
útesů	útes	k1gInPc2
a	a	k8xC
moří	moře	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
Severní	severní	k2eAgNnSc1d1
pobřeží	pobřeží	k1gNnSc1
a	a	k8xC
vody	voda	k1gFnPc1
Austrálie	Austrálie	k1gFnSc2
byly	být	k5eAaImAgFnP
sporadicky	sporadicky	k6eAd1
navštěvovány	navštěvovat	k5eAaImNgFnP
makassanskými	makassanský	k2eAgMnPc7d1
rybáři	rybář	k1gMnPc7
z	z	k7c2
dnešní	dnešní	k2eAgFnSc2d1
Indonésie	Indonésie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInPc1
kultura	kultura	k1gFnSc1
však	však	k9
byla	být	k5eAaImAgFnS
od	od	k7c2
dnešních	dnešní	k2eAgMnPc2d1
Austrálců	Austrálec	k1gMnPc2
odlišná	odlišný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Objevení	objevení	k1gNnSc1
kontinentu	kontinent	k1gInSc2
Evropany	Evropan	k1gMnPc4
</s>
<s>
První	první	k4xOgNnSc4
zaznamenané	zaznamenaný	k2eAgNnSc4d1
evropské	evropský	k2eAgNnSc4d1
pozorování	pozorování	k1gNnSc4
australské	australský	k2eAgFnSc2d1
pevniny	pevnina	k1gFnSc2
a	a	k8xC
první	první	k4xOgNnSc4
zaznamenané	zaznamenaný	k2eAgNnSc4d1
evropské	evropský	k2eAgNnSc4d1
přistání	přistání	k1gNnSc4
na	na	k7c6
australském	australský	k2eAgInSc6d1
kontinentu	kontinent	k1gInSc6
jsou	být	k5eAaImIp3nP
přičítány	přičítán	k2eAgFnPc1d1
Nizozemcům	Nizozemec	k1gMnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
Existuje	existovat	k5eAaImIp3nS
sice	sice	k8xC
teorie	teorie	k1gFnSc1
portugalského	portugalský	k2eAgNnSc2d1
přistání	přistání	k1gNnSc2
u	u	k7c2
břehů	břeh	k1gInPc2
Austrálie	Austrálie	k1gFnSc2
ve	v	k7c4
20	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
však	však	k9
věrohodně	věrohodně	k6eAd1
doložena	doložit	k5eAaPmNgFnS
a	a	k8xC
stále	stále	k6eAd1
se	se	k3xPyFc4
<g />
.	.	kIx.
</s>
<s hack="1">
o	o	k7c6
ní	on	k3xPp3gFnSc6
diskutuje	diskutovat	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
První	první	k4xOgFnSc1
loď	loď	k1gFnSc1
a	a	k8xC
posádka	posádka	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
zmapovala	zmapovat	k5eAaPmAgFnS
Australské	australský	k2eAgNnSc4d1
pobřeží	pobřeží	k1gNnSc4
a	a	k8xC
potkala	potkat	k5eAaPmAgFnS
se	se	k3xPyFc4
s	s	k7c7
Austrálci	Austrálec	k1gMnPc7
byla	být	k5eAaImAgFnS
Duyfken	Duyfken	k1gInSc4
vedená	vedený	k2eAgFnSc1d1
nizozemským	nizozemský	k2eAgInSc7d1
navigátorem	navigátor	k1gInSc7
<g/>
,	,	kIx,
Willemem	Willem	k1gInSc7
Janszoonem	Janszoon	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
začátku	začátek	k1gInSc6
roku	rok	k1gInSc2
1606	#num#	k4
zahlédl	zahlédnout	k5eAaPmAgMnS
pobřeží	pobřeží	k1gNnSc4
Yorského	yorský	k2eAgInSc2d1
poloostrova	poloostrov	k1gInSc2
a	a	k8xC
26	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1606	#num#	k4
přistál	přistát	k5eAaImAgInS,k5eAaPmAgInS
u	u	k7c2
řeky	řeka	k1gFnSc2
Pennefather	Pennefathra	k1gFnPc2
blízko	blízko	k7c2
dnešního	dnešní	k2eAgNnSc2d1
města	město	k1gNnSc2
Weipa	Weip	k1gMnSc2
na	na	k7c6
mysu	mys	k1gInSc6
York	York	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
proplul	proplout	k5eAaPmAgMnS
španělský	španělský	k2eAgMnSc1d1
průzkumník	průzkumník	k1gMnSc1
Luís	Luísa	k1gFnPc2
Vaz	vaz	k1gInSc4
de	de	k?
Torres	Torresa	k1gFnPc2
ostrovy	ostrov	k1gInPc1
kolem	kolem	k6eAd1
Torresova	Torresův	k2eAgInSc2d1
průlivu	průliv	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nizozemec	Nizozemec	k1gMnSc1
zmapoval	zmapovat	k5eAaPmAgMnS
celé	celý	k2eAgNnSc4d1
západní	západní	k2eAgNnSc4d1
a	a	k8xC
severní	severní	k2eAgNnSc4d1
pobřeží	pobřeží	k1gNnSc4
a	a	k8xC
pojmenoval	pojmenovat	k5eAaPmAgMnS
ostrovní	ostrovní	k2eAgInSc4d1
kontinent	kontinent	k1gInSc4
„	„	k?
<g/>
Nové	Nové	k2eAgNnSc1d1
Holandsko	Holandsko	k1gNnSc1
<g/>
“	“	k?
v	v	k7c6
průběhu	průběh	k1gInSc6
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
přestože	přestože	k8xS
nebyl	být	k5eNaImAgInS
učiněn	učinit	k5eAaImNgInS,k5eAaPmNgInS
žádný	žádný	k3yNgInSc1
pokus	pokus	k1gInSc1
o	o	k7c4
osídlení	osídlení	k1gNnSc4
<g/>
,	,	kIx,
řada	řada	k1gFnSc1
vraků	vrak	k1gInPc2
zanechala	zanechat	k5eAaPmAgFnS
muže	muž	k1gMnSc4
buď	buď	k8xC
uvízlé	uvízlý	k2eAgMnPc4d1
<g/>
,	,	kIx,
nebo	nebo	k8xC
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
v	v	k7c6
případě	případ	k1gInSc6
Batavie	Batavie	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1629	#num#	k4
<g/>
,	,	kIx,
opuštěné	opuštěný	k2eAgNnSc4d1
kvůli	kvůli	k7c3
vzpouře	vzpoura	k1gFnSc3
a	a	k8xC
vraždě	vražda	k1gFnSc3
<g/>
,	,	kIx,
a	a	k8xC
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
tak	tak	k9
prvním	první	k4xOgMnSc7
Evropanem	Evropan	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
trvale	trvale	k6eAd1
obýval	obývat	k5eAaImAgMnS
kontinent	kontinent	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
William	William	k1gInSc1
Dampier	Dampier	k1gInSc4
<g/>
,	,	kIx,
anglický	anglický	k2eAgMnSc1d1
objevitel	objevitel	k1gMnSc1
a	a	k8xC
lupič	lupič	k1gMnSc1
<g/>
,	,	kIx,
se	se	k3xPyFc4
vylodil	vylodit	k5eAaPmAgMnS
na	na	k7c6
severozápadním	severozápadní	k2eAgNnSc6d1
pobřeží	pobřeží	k1gNnSc6
Nového	Nového	k2eAgNnSc2d1
Holandska	Holandsko	k1gNnSc2
v	v	k7c6
roce	rok	k1gInSc6
1668	#num#	k4
(	(	kIx(
<g/>
zatímco	zatímco	k8xS
sloužil	sloužit	k5eAaImAgMnS
jako	jako	k9
člen	člen	k1gMnSc1
posádky	posádka	k1gFnSc2
pod	pod	k7c7
kapitánem	kapitán	k1gMnSc7
Johnem	John	k1gMnSc7
Readem	Read	k1gMnSc7
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
a	a	k8xC
znovu	znovu	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1699	#num#	k4
na	na	k7c6
zpáteční	zpáteční	k2eAgFnSc6d1
cestě	cesta	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1770	#num#	k4
se	se	k3xPyFc4
plavil	plavit	k5eAaImAgInS
kolem	kolem	k6eAd1
a	a	k8xC
zmapoval	zmapovat	k5eAaPmAgInS
východní	východní	k2eAgNnSc4d1
pobřeží	pobřeží	k1gNnSc4
James	James	k1gMnSc1
Cook	Cook	k1gMnSc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc4,k3yQgFnPc4,k3yIgFnPc4
pojmenoval	pojmenovat	k5eAaPmAgInS
Nový	nový	k2eAgInSc1d1
Jižní	jižní	k2eAgInSc1d1
Wales	Wales	k1gInSc1
přiznal	přiznat	k5eAaPmAgInS
ho	on	k3xPp3gInSc4
Velké	velký	k2eAgFnPc1d1
Británii	Británie	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Se	s	k7c7
ztrátou	ztráta	k1gFnSc7
amerických	americký	k2eAgFnPc2d1
kolonií	kolonie	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
1783	#num#	k4
poslala	poslat	k5eAaPmAgFnS
britská	britský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
flotilu	flotila	k1gFnSc4
lodí	loď	k1gFnSc7
<g/>
,	,	kIx,
tzv.	tzv.	kA
"	"	kIx"
<g/>
První	první	k4xOgFnSc4
flotilu	flotila	k1gFnSc4
<g/>
"	"	kIx"
pod	pod	k7c7
velením	velení	k1gNnSc7
kapitána	kapitán	k1gMnSc2
Arthura	Arthur	k1gMnSc2
Phillipa	Phillip	k1gMnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
založil	založit	k5eAaPmAgMnS
novou	nový	k2eAgFnSc4d1
trestaneckou	trestanecký	k2eAgFnSc4d1
kolonii	kolonie	k1gFnSc4
v	v	k7c6
Novém	nový	k2eAgInSc6d1
Jižním	jižní	k2eAgInSc6d1
Walesu	Wales	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tábor	Tábor	k1gInSc1
byl	být	k5eAaImAgInS
založen	založit	k5eAaPmNgInS
a	a	k8xC
britská	britský	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
byla	být	k5eAaImAgFnS
vyzvednuta	vyzvednout	k5eAaPmNgFnS
v	v	k7c4
Sydney	Sydney	k1gNnSc4
Cove	Cov	k1gFnSc2
v	v	k7c4
Port	port	k1gInSc4
Jacksonu	Jackson	k1gInSc2
26	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1788	#num#	k4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
48	#num#	k4
<g/>
]	]	kIx)
v	v	k7c4
den	den	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
se	se	k3xPyFc4
později	pozdě	k6eAd2
stal	stát	k5eAaPmAgInS
australským	australský	k2eAgMnSc7d1
národním	národní	k2eAgInSc7d1
dnem	den	k1gInSc7
<g/>
,	,	kIx,
australským	australský	k2eAgInSc7d1
dnem	den	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
časně	časně	k6eAd1
odsouzených	odsouzená	k1gFnPc2
byla	být	k5eAaImAgFnS
přesunuta	přesunout	k5eAaPmNgFnS
za	za	k7c4
malicherné	malicherný	k2eAgInPc4d1
zločiny	zločin	k1gInPc4
a	a	k8xC
při	při	k7c6
příjezdu	příjezd	k1gInSc6
zapsání	zapsání	k1gNnSc2
jako	jako	k8xS,k8xC
dělníci	dělník	k1gMnPc1
nebo	nebo	k8xC
sloužící	sloužící	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
se	se	k3xPyFc4
většina	většina	k1gFnSc1
po	po	k7c6
emancipaci	emancipace	k1gFnSc6
usadila	usadit	k5eAaPmAgFnS
v	v	k7c6
koloniální	koloniální	k2eAgFnSc6d1
společnosti	společnost	k1gFnSc6
<g/>
,	,	kIx,
pořádaly	pořádat	k5eAaImAgInP
se	se	k3xPyFc4
také	také	k9
vzpoury	vzpoura	k1gFnSc2
a	a	k8xC
povstání	povstání	k1gNnSc2
odsouzených	odsouzená	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
byly	být	k5eAaImAgFnP
vždy	vždy	k6eAd1
stanným	stanný	k2eAgNnSc7d1
právem	právo	k1gNnSc7
potlačeny	potlačit	k5eAaPmNgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rumová	rumový	k2eAgFnSc1d1
vzpoura	vzpoura	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
1808	#num#	k4
bylo	být	k5eAaImAgNnS
jediné	jediný	k2eAgNnSc1d1
úspěšné	úspěšný	k2eAgNnSc1d1
ozbrojené	ozbrojený	k2eAgNnSc1d1
převzetí	převzetí	k1gNnSc1
vlády	vláda	k1gFnSc2
v	v	k7c6
Austrálii	Austrálie	k1gFnSc6
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
podnítilo	podnítit	k5eAaPmAgNnS
dvouleté	dvouletý	k2eAgNnSc1d1
období	období	k1gNnSc1
vojenské	vojenský	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Domorodá	domorodý	k2eAgFnSc1d1
populace	populace	k1gFnSc1
se	se	k3xPyFc4
snížila	snížit	k5eAaPmAgFnS
v	v	k7c6
důsledku	důsledek	k1gInSc6
150	#num#	k4
letému	letý	k2eAgNnSc3d1
usazování	usazování	k1gNnSc3
Evropanů	Evropan	k1gMnPc2
<g/>
,	,	kIx,
hlavně	hlavně	k9
kvůli	kvůli	k7c3
infekčním	infekční	k2eAgFnPc3d1
chorobám	choroba	k1gFnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
49	#num#	k4
<g/>
]	]	kIx)
Tisíce	tisíc	k4xCgInPc1
dalších	další	k2eAgNnPc6d1
zemřelo	zemřít	k5eAaPmAgNnS
v	v	k7c6
důsledku	důsledek	k1gInSc6
hraničních	hraniční	k2eAgInPc2d1
konfliktů	konflikt	k1gInPc2
s	s	k7c7
osadníky	osadník	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vládní	vládní	k2eAgFnSc1d1
politika	politika	k1gFnSc1
„	„	k?
<g/>
asimilace	asimilace	k1gFnSc1
<g/>
“	“	k?
počínaje	počínaje	k7c7
zákonem	zákon	k1gInSc7
o	o	k7c6
ochraně	ochrana	k1gFnSc6
domorodců	domorodec	k1gMnPc2
z	z	k7c2
roku	rok	k1gInSc2
1869	#num#	k4
vyústila	vyústit	k5eAaPmAgFnS
v	v	k7c6
odstranění	odstranění	k1gNnSc6
mnoha	mnoho	k4c2
domorodých	domorodý	k2eAgFnPc2d1
dětí	dítě	k1gFnPc2
z	z	k7c2
jejich	jejich	k3xOp3gFnPc2
rodin	rodina	k1gFnPc2
a	a	k8xC
komunit	komunita	k1gFnPc2
-	-	kIx~
označovaných	označovaný	k2eAgFnPc2d1
jako	jako	k8xC,k8xS
Ukradené	ukradený	k2eAgFnPc1d1
generace	generace	k1gFnPc1
-	-	kIx~
postup	postup	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
rovněž	rovněž	k9
přispěl	přispět	k5eAaPmAgInS
k	k	k7c3
poklesu	pokles	k1gInSc3
domorodého	domorodý	k2eAgNnSc2d1
obyvatelstva	obyvatelstvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
výsledek	výsledek	k1gInSc1
referenda	referendum	k1gNnSc2
z	z	k7c2
roku	rok	k1gInSc2
1967	#num#	k4
<g/>
,	,	kIx,
pravomoc	pravomoc	k1gFnSc4
federální	federální	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
přijímat	přijímat	k5eAaImF
zvláštní	zvláštní	k2eAgInPc4d1
zákony	zákon	k1gInPc4
s	s	k7c7
ohledem	ohled	k1gInSc7
na	na	k7c4
konkrétní	konkrétní	k2eAgFnSc4d1
rasu	rasa	k1gFnSc4
byla	být	k5eAaImAgFnS
rozšířena	rozšířit	k5eAaPmNgFnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
bylo	být	k5eAaImAgNnS
možné	možný	k2eAgNnSc1d1
vytvářet	vytvářet	k5eAaImF
zákony	zákon	k1gInPc4
s	s	k7c7
ohledem	ohled	k1gInSc7
na	na	k7c6
domorodce	domorodka	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
50	#num#	k4
<g/>
]	]	kIx)
Tradiční	tradiční	k2eAgNnSc1d1
vlastnictví	vlastnictví	k1gNnSc1
půdy	půda	k1gFnSc2
(	(	kIx(
<g/>
„	„	k?
<g/>
rodný	rodný	k2eAgInSc4d1
titul	titul	k1gInSc4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
“	“	k?
<g/>
)	)	kIx)
bylo	být	k5eAaImAgNnS
zákonem	zákon	k1gInSc7
uznáno	uznat	k5eAaPmNgNnS
až	až	k9
v	v	k7c6
roce	rok	k1gInSc6
1992	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Vrchní	vrchní	k2eAgInSc1d1
soud	soud	k1gInSc1
Austrálie	Austrálie	k1gFnSc2
rozhodl	rozhodnout	k5eAaPmAgInS
v	v	k7c4
Mabo	Mabo	k1gNnSc4
v	v	k7c6
Queenslandu	Queensland	k1gInSc6
(	(	kIx(
<g/>
č.	č.	k?
2	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
že	že	k8xS
právní	právní	k2eAgFnSc1d1
doktrína	doktrína	k1gFnSc1
<g/>
,	,	kIx,
podle	podle	k7c2
které	který	k3yQgFnSc2,k3yRgFnSc2,k3yIgFnSc2
byla	být	k5eAaImAgFnS
Austrálie	Austrálie	k1gFnSc1
terra	terra	k1gMnSc1
nullius	nullius	k1gMnSc1
(	(	kIx(
<g/>
„	„	k?
<g/>
země	zem	k1gFnSc2
nikoho	nikdo	k3yNnSc2
<g/>
„	„	k?
<g/>
)	)	kIx)
se	se	k3xPyFc4
v	v	k7c6
době	doba	k1gFnSc6
britského	britský	k2eAgNnSc2d1
vypořádání	vypořádání	k1gNnSc2
nevztahovalo	vztahovat	k5eNaImAgNnS
na	na	k7c6
Austrálii	Austrálie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Britská	britský	k2eAgFnSc1d1
kolonizace	kolonizace	k1gFnSc1
</s>
<s>
Replika	replika	k1gFnSc1
lodi	loď	k1gFnSc2
HMS	HMS	kA
Endeavour	Endeavour	k1gInSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
postavena	postaven	k2eAgFnSc1d1
roku	rok	k1gInSc2
1988	#num#	k4
ve	v	k7c6
Fremantlu	Fremantl	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poručík	poručík	k1gMnSc1
James	James	k1gMnSc1
Cook	Cook	k1gMnSc1
na	na	k7c6
původní	původní	k2eAgFnSc6d1
lodi	loď	k1gFnSc6
připlul	připlout	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1770	#num#	k4
do	do	k7c2
Botanického	botanický	k2eAgInSc2d1
zálivu	záliv	k1gInSc2
a	a	k8xC
zabral	zabrat	k5eAaPmAgMnS
východní	východní	k2eAgNnSc4d1
pobřeží	pobřeží	k1gNnSc4
Austrálie	Austrálie	k1gFnSc2
ve	v	k7c4
prospěch	prospěch	k1gInSc4
Království	království	k1gNnSc2
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Přistání	přistání	k1gNnSc1
kapitána	kapitán	k1gMnSc2
Cooka	Cooek	k1gMnSc2
v	v	k7c6
Botanickém	botanický	k2eAgInSc6d1
zálivu	záliv	k1gInSc6
<g/>
,	,	kIx,
malba	malba	k1gFnSc1
z	z	k7c2
roku	rok	k1gInSc2
1902	#num#	k4
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1769	#num#	k4
se	se	k3xPyFc4
britský	britský	k2eAgMnSc1d1
námořní	námořní	k2eAgMnSc1d1
poručík	poručík	k1gMnSc1
James	James	k1gMnSc1
Cook	Cook	k1gMnSc1
na	na	k7c6
lodi	loď	k1gFnSc6
Endeavour	Endeavour	k1gInSc1
připlavil	připlavit	k5eAaPmAgInS
nejprve	nejprve	k6eAd1
na	na	k7c6
Tahiti	Tahiti	k1gNnSc6
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
pozoroval	pozorovat	k5eAaImAgMnS
přechod	přechod	k1gInSc4
Měsíce	měsíc	k1gInSc2
přes	přes	k7c4
Venuši	Venuše	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měl	mít	k5eAaImAgMnS
však	však	k9
tajný	tajný	k2eAgInSc4d1
pokyn	pokyn	k1gInSc4
britské	britský	k2eAgFnSc2d1
admirality	admiralita	k1gFnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
pokusil	pokusit	k5eAaPmAgMnS
nalézt	nalézt	k5eAaBmF,k5eAaPmF
cestu	cesta	k1gFnSc4
k	k	k7c3
jižnímu	jižní	k2eAgInSc3d1
kontinentu	kontinent	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cook	Cook	k1gMnSc1
se	se	k3xPyFc4
rozhodl	rozhodnout	k5eAaPmAgMnS
prozkoumat	prozkoumat	k5eAaPmF
východní	východní	k2eAgNnSc4d1
pobřeží	pobřeží	k1gNnSc4
<g/>
,	,	kIx,
jedinou	jediný	k2eAgFnSc4d1
část	část	k1gFnSc4
Austrálie	Austrálie	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
nizozemští	nizozemský	k2eAgMnPc1d1
mořeplavci	mořeplavec	k1gMnPc1
neprobádali	probádat	k5eNaPmAgMnP
<g/>
.	.	kIx.
19	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1770	#num#	k4
posádka	posádka	k1gFnSc1
Endeavouru	Endeavour	k1gInSc2
zahlédla	zahlédnout	k5eAaPmAgFnS
východní	východní	k2eAgNnSc4d1
pobřeží	pobřeží	k1gNnSc4
a	a	k8xC
o	o	k7c4
deset	deset	k4xCc4
dní	den	k1gInPc2
později	pozdě	k6eAd2
Cook	Cook	k1gMnSc1
přistál	přistát	k5eAaPmAgMnS,k5eAaImAgMnS
v	v	k7c6
Botanickém	botanický	k2eAgInSc6d1
zálivu	záliv	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
51	#num#	k4
<g/>
]	]	kIx)
Cook	Cook	k1gInSc1
zmapoval	zmapovat	k5eAaPmAgInS
pobřeží	pobřeží	k1gNnSc4
spolu	spolu	k6eAd1
s	s	k7c7
přírodovědcem	přírodovědec	k1gMnSc7
lodi	loď	k1gFnSc2
Josephem	Joseph	k1gInSc7
Banksem	Banks	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
následně	následně	k6eAd1
sepsal	sepsat	k5eAaPmAgMnS
zprávu	zpráva	k1gFnSc4
o	o	k7c6
příznivých	příznivý	k2eAgFnPc6d1
podmínkách	podmínka	k1gFnPc6
pro	pro	k7c4
založení	založení	k1gNnSc4
kolonie	kolonie	k1gFnSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
doporučil	doporučit	k5eAaPmAgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
kolonie	kolonie	k1gFnSc1
byla	být	k5eAaImAgFnS
trestanecká	trestanecký	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1788	#num#	k4
dorazila	dorazit	k5eAaPmAgFnS
do	do	k7c2
Botanického	botanický	k2eAgInSc2d1
zálivu	záliv	k1gInSc6
první	první	k4xOgFnSc1
kolonizační	kolonizační	k2eAgFnSc1d1
britská	britský	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
(	(	kIx(
<g/>
tzv.	tzv.	kA
First	First	k1gMnSc1
Fleet	Fleet	k1gMnSc1
<g/>
)	)	kIx)
vedená	vedený	k2eAgFnSc1d1
kapitánem	kapitán	k1gMnSc7
Arthurem	Arthur	k1gMnSc7
Philippem	Philipp	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedenáct	jedenáct	k4xCc1
lodí	loď	k1gFnSc7
sebou	se	k3xPyFc7
přivezlo	přivézt	k5eAaPmAgNnS
i	i	k9
první	první	k4xOgMnPc4
trestance	trestanec	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Několik	několik	k4yIc1
dní	den	k1gInPc2
po	po	k7c6
příjezdu	příjezd	k1gInSc6
do	do	k7c2
Botanického	botanický	k2eAgInSc2d1
zálivu	záliv	k1gInSc2
se	se	k3xPyFc4
flotila	flotila	k1gFnSc1
přesunula	přesunout	k5eAaPmAgFnS
do	do	k7c2
vhodnějšího	vhodný	k2eAgInSc2d2
přístavu	přístav	k1gInSc2
Jackson	Jacksona	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
byla	být	k5eAaImAgFnS
26	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1788	#num#	k4
založena	založen	k2eAgFnSc1d1
osada	osada	k1gFnSc1
Sydney	Sydney	k1gNnSc2
Cove	Cove	k1gFnPc2
<g/>
,	,	kIx,
dnešní	dnešní	k2eAgNnSc4d1
Sydney	Sydney	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
Toto	tento	k3xDgNnSc1
datum	datum	k1gNnSc1
se	se	k3xPyFc4
později	pozdě	k6eAd2
stalo	stát	k5eAaPmAgNnS
australským	australský	k2eAgMnSc7d1
státním	státní	k2eAgInSc7d1
svátkem	svátek	k1gInSc7
<g/>
,	,	kIx,
Dnem	den	k1gInSc7
Austrálie	Austrálie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgMnSc1
guvernér	guvernér	k1gMnSc1
Austrálie	Austrálie	k1gFnSc2
Arthur	Arthur	k1gMnSc1
Phillip	Phillip	k1gMnSc1
</s>
<s>
Oficiálně	oficiálně	k6eAd1
vyhlásilo	vyhlásit	k5eAaPmAgNnS
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
a	a	k8xC
Irska	Irsko	k1gNnSc2
své	svůj	k3xOyFgInPc4
nároky	nárok	k1gInPc4
na	na	k7c4
západní	západní	k2eAgFnSc4d1
část	část	k1gFnSc4
Austrálie	Austrálie	k1gFnSc2
roku	rok	k1gInSc2
1829	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tehdejší	tehdejší	k2eAgFnSc1d1
Země	země	k1gFnSc1
Van	vana	k1gFnPc2
Diemena	Diemen	k2eAgFnSc1d1
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
známá	známý	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
Tasmánie	Tasmánie	k1gFnSc1
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
Brity	Brit	k1gMnPc4
(	(	kIx(
<g/>
z	z	k7c2
velké	velký	k2eAgFnSc2d1
části	část	k1gFnSc2
trestanci	trestanec	k1gMnPc1
<g/>
)	)	kIx)
osídlena	osídlen	k2eAgFnSc1d1
v	v	k7c6
roce	rok	k1gInSc6
1803	#num#	k4
<g/>
,	,	kIx,
roku	rok	k1gInSc2
1825	#num#	k4
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
samostatnou	samostatný	k2eAgFnSc7d1
britskou	britský	k2eAgFnSc7d1
kolonií	kolonie	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
částí	část	k1gFnPc2
Nového	Nového	k2eAgInSc2d1
Jižního	jižní	k2eAgInSc2d1
Walesu	Wales	k1gInSc2
byly	být	k5eAaImAgFnP
vytvořeny	vytvořit	k5eAaPmNgFnP
další	další	k2eAgFnPc1d1
samostatné	samostatný	k2eAgFnPc1d1
kolonie	kolonie	k1gFnPc1
<g/>
:	:	kIx,
Jižní	jižní	k2eAgFnSc1d1
Austrálie	Austrálie	k1gFnSc1
roku	rok	k1gInSc2
1836	#num#	k4
<g/>
,	,	kIx,
Victoria	Victorium	k1gNnSc2
roku	rok	k1gInSc2
1851	#num#	k4
a	a	k8xC
Queensland	Queensland	k1gInSc1
roku	rok	k1gInSc2
1859	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Severní	severní	k2eAgInSc4d1
teritorium	teritorium	k1gNnSc1
bylo	být	k5eAaImAgNnS
založeno	založit	k5eAaPmNgNnS
roku	rok	k1gInSc2
1911	#num#	k4
vynětím	vynětí	k1gNnSc7
z	z	k7c2
Jižní	jižní	k2eAgFnSc2d1
Austrálie	Austrálie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jižní	jižní	k2eAgFnSc1d1
Austrálie	Austrálie	k1gFnSc1
byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
jako	jako	k9
„	„	k?
<g/>
svobodná	svobodný	k2eAgFnSc1d1
provincie	provincie	k1gFnSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
free	free	k1gFnSc1
province	province	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
nebyla	být	k5eNaImAgFnS
tedy	tedy	k9
nikdy	nikdy	k6eAd1
trestaneckou	trestanecký	k2eAgFnSc7d1
kolonií	kolonie	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Provincie	provincie	k1gFnSc1
Victoria	Victorium	k1gNnSc2
a	a	k8xC
Západní	západní	k2eAgFnPc1d1
Austrálie	Austrálie	k1gFnPc1
byly	být	k5eAaImAgFnP
také	také	k9
založeny	založit	k5eAaPmNgFnP
jako	jako	k8xC,k8xS
„	„	k?
<g/>
svobodné	svobodný	k2eAgFnSc2d1
<g/>
“	“	k?
<g/>
,	,	kIx,
ale	ale	k8xC
později	pozdě	k6eAd2
přijaly	přijmout	k5eAaPmAgInP
transporty	transport	k1gInPc1
trestanců	trestanec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Transporty	transport	k1gInPc7
do	do	k7c2
Nového	Nového	k2eAgInSc2d1
Jižního	jižní	k2eAgInSc2d1
Walesu	Wales	k1gInSc2
byly	být	k5eAaImAgFnP
roku	rok	k1gInSc2
1848	#num#	k4
po	po	k7c6
protestech	protest	k1gInPc6
obyvatel	obyvatel	k1gMnPc2
zastaveny	zastaven	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s>
Port	port	k1gInSc1
Arthur	Arthura	k1gFnPc2
na	na	k7c6
Tasmánii	Tasmánie	k1gFnSc6
byl	být	k5eAaImAgMnS
největší	veliký	k2eAgFnSc7d3
australskou	australský	k2eAgFnSc7d1
trestaneckou	trestanecký	k2eAgFnSc7d1
kolonií	kolonie	k1gFnSc7
</s>
<s>
Britové	Brit	k1gMnPc1
již	již	k6eAd1
na	na	k7c6
konci	konec	k1gInSc6
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
přišli	přijít	k5eAaPmAgMnP
s	s	k7c7
opatřením	opatření	k1gNnSc7
<g/>
,	,	kIx,
že	že	k8xS
odsouzený	odsouzený	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
zůstane	zůstat	k5eAaPmIp3nS
po	po	k7c6
vykonání	vykonání	k1gNnSc6
trestu	trest	k1gInSc2
v	v	k7c6
Austrálii	Austrálie	k1gFnSc6
<g/>
,	,	kIx,
získá	získat	k5eAaPmIp3nS
vlastní	vlastní	k2eAgFnSc4d1
půdu	půda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejné	stejné	k1gNnSc1
opatření	opatření	k1gNnPc2
se	se	k3xPyFc4
vztahovalo	vztahovat	k5eAaImAgNnS
i	i	k9
na	na	k7c4
vojáky	voják	k1gMnPc4
<g/>
,	,	kIx,
kterým	který	k3yQgInPc3,k3yIgInPc3,k3yRgInPc3
skončila	skončit	k5eAaPmAgFnS
vojenská	vojenský	k2eAgFnSc1d1
služba	služba	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
nastartovalo	nastartovat	k5eAaPmAgNnS
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
skomírající	skomírající	k2eAgInSc1d1
zemědělský	zemědělský	k2eAgInSc1d1
sektor	sektor	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Guvernér	guvernér	k1gMnSc1
Lachlan	Lachlan	k1gMnSc1
Macquarie	Macquarie	k1gFnSc2
bývá	bývat	k5eAaImIp3nS
označován	označovat	k5eAaImNgInS
za	za	k7c2
otce	otec	k1gMnSc2
australského	australský	k2eAgNnSc2d1
egalitářství	egalitářství	k1gNnSc2
<g/>
,	,	kIx,
neboť	neboť	k8xC
to	ten	k3xDgNnSc1
byl	být	k5eAaImAgMnS
on	on	k3xPp3gMnSc1
<g/>
,	,	kIx,
kdo	kdo	k3yQnSc1,k3yInSc1,k3yRnSc1
zavedl	zavést	k5eAaPmAgMnS
pravidlo	pravidlo	k1gNnSc4
<g/>
,	,	kIx,
že	že	k8xS
odsouzenec	odsouzenec	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
získá	získat	k5eAaPmIp3nS
svobodu	svoboda	k1gFnSc4
<g/>
,	,	kIx,
stává	stávat	k5eAaImIp3nS
se	se	k3xPyFc4
plnohodnotným	plnohodnotný	k2eAgMnSc7d1
kolonistou	kolonista	k1gMnSc7
–	–	k?
a	a	k8xC
začal	začít	k5eAaPmAgMnS
takovým	takový	k3xDgMnPc3
lidem	člověk	k1gMnPc3
přidělovat	přidělovat	k5eAaImF
i	i	k9
úřady	úřad	k1gInPc1
a	a	k8xC
dokonce	dokonce	k9
funkci	funkce	k1gFnSc4
soudce	soudce	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navíc	navíc	k6eAd1
začali	začít	k5eAaPmAgMnP
přicházet	přicházet	k5eAaImF
i	i	k9
regulérní	regulérní	k2eAgMnPc1d1
osadníci	osadník	k1gMnPc1
<g/>
,	,	kIx,
16	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1793	#num#	k4
dorazili	dorazit	k5eAaPmAgMnP
ti	ten	k3xDgMnPc1
první	první	k4xOgMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trestanecký	trestanecký	k2eAgInSc4d1
charakter	charakter	k1gInSc4
si	se	k3xPyFc3
však	však	k9
kolonie	kolonie	k1gFnSc1
držela	držet	k5eAaImAgFnS
dlouho	dlouho	k6eAd1
<g/>
,	,	kIx,
dovoz	dovoz	k1gInSc1
vězňů	vězeň	k1gMnPc2
skončil	skončit	k5eAaPmAgInS
až	až	k9
roku	rok	k1gInSc2
1868	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jádrem	jádro	k1gNnSc7
rané	raný	k2eAgFnSc2d1
australské	australský	k2eAgFnSc2d1
ekonomiky	ekonomika	k1gFnSc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
chov	chov	k1gInSc1
ovcí	ovce	k1gFnPc2
a	a	k8xC
výroba	výroba	k1gFnSc1
vlny	vlna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1834	#num#	k4
byly	být	k5eAaImAgFnP
z	z	k7c2
Austrálie	Austrálie	k1gFnSc2
do	do	k7c2
Anglie	Anglie	k1gFnSc2
vyvezeny	vyvezen	k2eAgFnPc1d1
již	již	k6eAd1
dva	dva	k4xCgInPc4
tisíce	tisíc	k4xCgInPc4
tun	tuna	k1gFnPc2
vlny	vlna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dovoz	dovoz	k1gInSc4
některých	některý	k3yIgNnPc2
evropských	evropský	k2eAgNnPc2d1
zvířat	zvíře	k1gNnPc2
ovšem	ovšem	k9
narušil	narušit	k5eAaPmAgInS
australský	australský	k2eAgInSc1d1
ekosystém	ekosystém	k1gInSc1
(	(	kIx(
<g/>
známé	známý	k2eAgNnSc1d1
přemnožení	přemnožení	k1gNnSc1
králíků	králík	k1gMnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
původní	původní	k2eAgMnPc4d1
obyvatele	obyvatel	k1gMnPc4
znamenala	znamenat	k5eAaImAgFnS
kolonizace	kolonizace	k1gFnSc1
katastrofu	katastrofa	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvních	první	k4xOgInPc2
150	#num#	k4
let	léto	k1gNnPc2
po	po	k7c6
příchodu	příchod	k1gInSc6
Evropanů	Evropan	k1gMnPc2
jejich	jejich	k3xOp3gFnSc2
populace	populace	k1gFnSc2
prudce	prudko	k6eAd1
klesala	klesat	k5eAaImAgFnS
<g/>
,	,	kIx,
vlivem	vliv	k1gInSc7
rozšíření	rozšíření	k1gNnSc2
infekčních	infekční	k2eAgFnPc2d1
nemocí	nemoc	k1gFnPc2
přinesených	přinesený	k2eAgFnPc2d1
Brity	Brit	k1gMnPc7
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
byly	být	k5eAaImAgFnP
neštovice	neštovice	k1gFnPc1
nebo	nebo	k8xC
spalničky	spalničky	k1gFnPc1
<g/>
,	,	kIx,
a	a	k8xC
násilného	násilný	k2eAgNnSc2d1
přesídlování	přesídlování	k1gNnSc2
spojeného	spojený	k2eAgNnSc2d1
s	s	k7c7
kulturním	kulturní	k2eAgInSc7d1
rozkladem	rozklad	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odebírání	odebírání	k1gNnSc1
dětí	dítě	k1gFnPc2
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
by	by	kYmCp3nS
podle	podle	k7c2
některých	některý	k3yIgMnPc2
historiků	historik	k1gMnPc2
i	i	k8xC
samotných	samotný	k2eAgMnPc2d1
Aboriginců	Aboriginec	k1gMnPc2
mohlo	moct	k5eAaImAgNnS
být	být	k5eAaImF
dle	dle	k7c2
jistých	jistý	k2eAgFnPc2d1
definic	definice	k1gFnPc2
považováno	považován	k2eAgNnSc1d1
za	za	k7c4
genocidu	genocida	k1gFnSc4
<g/>
,	,	kIx,
pravděpodobně	pravděpodobně	k6eAd1
také	také	k9
přispělo	přispět	k5eAaPmAgNnS
ke	k	k7c3
snížení	snížení	k1gNnSc3
populace	populace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobná	podobný	k2eAgFnSc1d1
podání	podání	k1gNnSc6
historie	historie	k1gFnSc2
jsou	být	k5eAaImIp3nP
ovšem	ovšem	k9
sporná	sporný	k2eAgFnSc1d1
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
jsou	být	k5eAaImIp3nP
velmi	velmi	k6eAd1
často	často	k6eAd1
přehnaná	přehnaný	k2eAgFnSc1d1
nebo	nebo	k8xC
ovlivněná	ovlivněný	k2eAgFnSc1d1
politickými	politický	k2eAgInPc7d1
či	či	k8xC
ideologickými	ideologický	k2eAgInPc7d1
cíli	cíl	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Austrálii	Austrálie	k1gFnSc6
jsou	být	k5eAaImIp3nP
tyto	tento	k3xDgInPc1
spory	spor	k1gInPc1
známé	známý	k2eAgInPc1d1
jako	jako	k8xS,k8xC
„	„	k?
<g/>
Války	válka	k1gFnSc2
o	o	k7c6
historii	historie	k1gFnSc6
<g/>
“	“	k?
(	(	kIx(
<g/>
History	Histor	k1gInPc4
Wars	Wars	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
referendu	referendum	k1gNnSc6
z	z	k7c2
roku	rok	k1gInSc2
1967	#num#	k4
a	a	k8xC
následných	následný	k2eAgFnPc6d1
změnách	změna	k1gFnPc6
ústavy	ústava	k1gFnSc2
získala	získat	k5eAaPmAgFnS
federální	federální	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
pravomoc	pravomoc	k1gFnSc4
uplatňovat	uplatňovat	k5eAaImF
moc	moc	k6eAd1
i	i	k9
vůči	vůči	k7c3
původním	původní	k2eAgMnPc3d1
obyvatelům	obyvatel	k1gMnPc3
kontinentu	kontinent	k1gInSc2
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
jim	on	k3xPp3gFnPc3
bylo	být	k5eAaImAgNnS
přiznáno	přiznat	k5eAaPmNgNnS
postavení	postavení	k1gNnSc1
rovnoprávných	rovnoprávný	k2eAgMnPc2d1
občanů	občan	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nP
původní	původní	k2eAgMnPc1d1
obyvatelé	obyvatel	k1gMnPc1
mohli	moct	k5eAaImAgMnP
být	být	k5eAaImF
bráni	brát	k5eAaImNgMnP
jako	jako	k9
vlastníci	vlastník	k1gMnPc1
půdy	půda	k1gFnSc2
předtím	předtím	k6eAd1
<g/>
,	,	kIx,
než	než	k8xS
ji	on	k3xPp3gFnSc4
ztratili	ztratit	k5eAaPmAgMnP
během	během	k7c2
evropské	evropský	k2eAgFnSc2d1
kolonizace	kolonizace	k1gFnSc2
<g/>
,	,	kIx,
nebylo	být	k5eNaImAgNnS
uznáváno	uznávat	k5eAaImNgNnS
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1992	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Australský	australský	k2eAgInSc1d1
nejvyšší	vysoký	k2eAgInSc1d3
soud	soud	k1gInSc1
v	v	k7c6
případu	případ	k1gInSc6
Mabo	Mabo	k1gMnSc1
v.	v.	k?
Queensland	Queensland	k1gInSc1
(	(	kIx(
<g/>
No	no	k9
2	#num#	k4
<g/>
)	)	kIx)
odmítl	odmítnout	k5eAaPmAgMnS
představu	představa	k1gFnSc4
Austrálie	Austrálie	k1gFnSc2
jako	jako	k8xS,k8xC
„	„	k?
<g/>
prázdné	prázdný	k2eAgFnSc2d1
země	zem	k1gFnSc2
<g/>
“	“	k?
(	(	kIx(
<g/>
terra	terra	k1gMnSc1
nullius	nullius	k1gMnSc1
<g/>
)	)	kIx)
v	v	k7c6
době	doba	k1gFnSc6
jejího	její	k3xOp3gMnSc4
zabrání	zabránit	k5eAaPmIp3nS
Evropany	Evropan	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s>
Eureka	Eureka	k6eAd1
flag	flag	k1gInSc1
<g/>
,	,	kIx,
vlajka	vlajka	k1gFnSc1
vzbouřenců	vzbouřenec	k1gMnPc2
z	z	k7c2
Eureky	Eurek	k1gInPc7
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
často	často	k6eAd1
považována	považován	k2eAgFnSc1d1
za	za	k7c4
alternativu	alternativa	k1gFnSc4
k	k	k7c3
australské	australský	k2eAgFnSc3d1
vlajce	vlajka	k1gFnSc3
a	a	k8xC
má	mít	k5eAaImIp3nS
většinou	většinou	k6eAd1
význam	význam	k1gInSc4
protivládního	protivládní	k2eAgInSc2d1
odporu	odpor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c4
21	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
jí	on	k3xPp3gFnSc3
užívá	užívat	k5eAaImIp3nS
jak	jak	k6eAd1
australská	australský	k2eAgFnSc1d1
krajní	krajní	k2eAgFnSc1d1
pravice	pravice	k1gFnSc1
<g/>
,	,	kIx,
tak	tak	k6eAd1
australští	australský	k2eAgMnPc1d1
odboráři	odborář	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
vzpouru	vzpoura	k1gFnSc4
v	v	k7c6
Eurece	Eureka	k1gFnSc6
považují	považovat	k5eAaImIp3nP
za	za	k7c4
vznik	vznik	k1gInSc4
odborového	odborový	k2eAgNnSc2d1
hnutí	hnutí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
počátku	počátek	k1gInSc6
50	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
vypukla	vypuknout	k5eAaPmAgFnS
v	v	k7c6
Bathurstu	Bathurst	k1gInSc6
v	v	k7c6
Novém	nový	k2eAgInSc6d1
Jižním	jižní	k2eAgInSc6d1
Walesu	Wales	k1gInSc6
zlatá	zlatý	k2eAgFnSc1d1
horečka	horečka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tzv.	tzv.	kA
„	„	k?
<g/>
Eureka	Eureka	k1gMnSc1
Stockade	Stockad	k1gInSc5
<g/>
“	“	k?
<g/>
,	,	kIx,
vzpoura	vzpoura	k1gFnSc1
proti	proti	k7c3
zavedení	zavedení	k1gNnSc3
poplatků	poplatek	k1gInPc2
za	za	k7c4
těžební	těžební	k2eAgFnPc4d1
licence	licence	k1gFnPc4
<g/>
,	,	kIx,
pak	pak	k6eAd1
byla	být	k5eAaImAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1854	#num#	k4
v	v	k7c6
Austrálii	Austrálie	k1gFnSc6
prvním	první	k4xOgInSc7
projevem	projev	k1gInSc7
občanské	občanský	k2eAgFnSc2d1
neposlušnosti	neposlušnost	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
53	#num#	k4
<g/>
]	]	kIx)
Ačkoli	ačkoli	k8xS
vzpoura	vzpoura	k1gFnSc1
byla	být	k5eAaImAgFnS
rozdrcena	rozdrtit	k5eAaPmNgFnS
<g/>
,	,	kIx,
nutno	nutno	k6eAd1
říci	říct	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
samo	sám	k3xTgNnSc1
odborové	odborový	k2eAgNnSc1d1
hnutí	hnutí	k1gNnSc1
bylo	být	k5eAaImAgNnS
v	v	k7c6
dalších	další	k2eAgNnPc6d1
letech	léto	k1gNnPc6
značně	značně	k6eAd1
úspěšné	úspěšný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nedostatek	nedostatek	k1gInSc1
pracovních	pracovní	k2eAgFnPc2d1
sil	síla	k1gFnPc2
totiž	totiž	k9
vedl	vést	k5eAaImAgInS
k	k	k7c3
vysokým	vysoký	k2eAgFnPc3d1
mzdám	mzda	k1gFnPc3
<g/>
,	,	kIx,
uzákonění	uzákonění	k1gNnSc1
osmihodinového	osmihodinový	k2eAgInSc2d1
pracovního	pracovní	k2eAgInSc2d1
dne	den	k1gInSc2
a	a	k8xC
dalším	další	k2eAgFnPc3d1
výhodám	výhoda	k1gFnPc3
<g/>
,	,	kIx,
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
v	v	k7c6
Evropě	Evropa	k1gFnSc6
neslýchaným	slýchaný	k2eNgNnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Austrálie	Austrálie	k1gFnSc1
získala	získat	k5eAaPmAgFnS
pověst	pověst	k1gFnSc4
„	„	k?
<g/>
ráje	ráj	k1gInSc2
pracujících	pracující	k1gMnPc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
zaměstnavatelé	zaměstnavatel	k1gMnPc1
se	se	k3xPyFc4
pokusili	pokusit	k5eAaPmAgMnP
podkopat	podkopat	k5eAaPmF
rostoucí	rostoucí	k2eAgInSc4d1
vliv	vliv	k1gInSc4
odborů	odbor	k1gInPc2
dovozem	dovoz	k1gInSc7
levných	levný	k2eAgMnPc2d1
čínských	čínský	k2eAgMnPc2d1
pracovníků	pracovník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
vyvolalo	vyvolat	k5eAaPmAgNnS
protitlak	protitlak	k1gInSc4
odborů	odbor	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
si	se	k3xPyFc3
u	u	k7c2
koloniálních	koloniální	k2eAgFnPc2d1
správ	správa	k1gFnPc2
vynutily	vynutit	k5eAaPmAgFnP
omezování	omezování	k1gNnSc4
přistěhovalectví	přistěhovalectví	k1gNnSc2
z	z	k7c2
Asie	Asie	k1gFnSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
vedlo	vést	k5eAaImAgNnS
k	k	k7c3
rozšíření	rozšíření	k1gNnSc3
politiky	politika	k1gFnSc2
„	„	k?
<g/>
Bílé	bílý	k2eAgFnSc2d1
Austrálie	Austrálie	k1gFnSc2
<g/>
“	“	k?
(	(	kIx(
<g/>
White	Whit	k1gInSc5
Australia	Australius	k1gMnSc2
policy	polic	k2eAgFnPc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
dlouho	dlouho	k6eAd1
součástí	součást	k1gFnSc7
australského	australský	k2eAgInSc2d1
politického	politický	k2eAgInSc2d1
konsensu	konsens	k1gInSc2
a	a	k8xC
rozpadla	rozpadnout	k5eAaPmAgFnS
se	se	k3xPyFc4
až	až	k9
po	po	k7c6
<g />
.	.	kIx.
</s>
<s hack="1">
druhé	druhý	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
definitivně	definitivně	k6eAd1
byla	být	k5eAaImAgFnS
z	z	k7c2
legislativy	legislativa	k1gFnSc2
jako	jako	k8xS,k8xC
rasistická	rasistický	k2eAgFnSc1d1
odstraněna	odstraněn	k2eAgFnSc1d1
roku	rok	k1gInSc2
1973	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
54	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
55	#num#	k4
<g/>
]	]	kIx)
Druhá	druhý	k4xOgFnSc1
polovina	polovina	k1gFnSc1
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
byla	být	k5eAaImAgFnS
každopádně	každopádně	k6eAd1
ve	v	k7c6
znamení	znamení	k1gNnSc6
rychlého	rychlý	k2eAgInSc2d1
ekonomického	ekonomický	k2eAgInSc2d1
růstu	růst	k1gInSc2
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
nazývaného	nazývaný	k2eAgInSc2d1
„	„	k?
<g/>
dlouhý	dlouhý	k2eAgInSc4d1
boom	boom	k1gInSc4
<g/>
“	“	k?
<g/>
,	,	kIx,
během	během	k7c2
nějž	jenž	k3xRgInSc2
se	se	k3xPyFc4
Melbourne	Melbourne	k1gNnSc1
údajně	údajně	k6eAd1
stalo	stát	k5eAaPmAgNnS
nejbohatším	bohatý	k2eAgMnSc7d3
městem	město	k1gNnSc7
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Boom	boom	k1gInSc1
skončil	skončit	k5eAaPmAgInS
v	v	k7c6
90	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgInS
doprovázen	doprovázet	k5eAaImNgInS
růstem	růst	k1gInSc7
populace	populace	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1900	#num#	k4
dosáhla	dosáhnout	k5eAaPmAgFnS
3,7	3,7	k4
milionu	milion	k4xCgInSc2
<g/>
,	,	kIx,
přitom	přitom	k6eAd1
téměř	téměř	k6eAd1
milion	milion	k4xCgInSc1
lidí	člověk	k1gMnPc2
žil	žít	k5eAaImAgInS
v	v	k7c6
Melbourne	Melbourne	k1gNnSc6
a	a	k8xC
Sydney	Sydney	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Populace	populace	k1gFnSc1
ovcí	ovce	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
1891	#num#	k4
dosáhla	dosáhnout	k5eAaPmAgFnS
100	#num#	k4
miliónů	milión	k4xCgInPc2
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c4
roky	rok	k1gInPc4
1855	#num#	k4
a	a	k8xC
1890	#num#	k4
postupně	postupně	k6eAd1
všech	všecek	k3xTgFnPc2
pět	pět	k4xCc1
kolonií	kolonie	k1gFnPc2
získalo	získat	k5eAaPmAgNnS
svou	svůj	k3xOyFgFnSc4
vlastní	vlastní	k2eAgFnSc4d1
samosprávnou	samosprávný	k2eAgFnSc4d1
vládu	vláda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samozřejmě	samozřejmě	k6eAd1
stále	stále	k6eAd1
zůstávaly	zůstávat	k5eAaImAgInP
součástí	součást	k1gFnSc7
Britského	britský	k2eAgNnSc2d1
impéria	impérium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pod	pod	k7c7
vlastní	vlastní	k2eAgFnSc7d1
kontrolou	kontrola	k1gFnSc7
londýnské	londýnský	k2eAgNnSc4d1
Ministerstvo	ministerstvo	k1gNnSc4
pro	pro	k7c4
kolonie	kolonie	k1gFnPc4
ponechalo	ponechat	k5eAaPmAgNnS
především	především	k9
zahraniční	zahraniční	k2eAgFnSc4d1
reprezentaci	reprezentace	k1gFnSc4
<g/>
,	,	kIx,
obranu	obrana	k1gFnSc4
a	a	k8xC
mezinárodní	mezinárodní	k2eAgFnSc4d1
přepravu	přeprava	k1gFnSc4
a	a	k8xC
obchod	obchod	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgFnPc1
australské	australský	k2eAgFnPc1d1
parlamentní	parlamentní	k2eAgFnPc1d1
volby	volba	k1gFnPc1
(	(	kIx(
<g/>
do	do	k7c2
zákonodárné	zákonodárný	k2eAgFnSc2d1
rady	rada	k1gFnSc2
Nového	Nového	k2eAgInSc2d1
Jižního	jižní	k2eAgInSc2d1
Walesu	Wales	k1gInSc2
<g/>
)	)	kIx)
se	se	k3xPyFc4
uskutečnily	uskutečnit	k5eAaPmAgInP
v	v	k7c6
roce	rok	k1gInSc6
1843	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Demokratizace	demokratizace	k1gFnSc1
pak	pak	k6eAd1
postupovala	postupovat	k5eAaImAgFnS
celým	celý	k2eAgInSc7d1
kontinentem	kontinent	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Australské	australský	k2eAgFnSc2d1
ženy	žena	k1gFnSc2
získaly	získat	k5eAaPmAgInP
právo	právo	k1gNnSc4
volit	volit	k5eAaImF
do	do	k7c2
parlamentu	parlament	k1gInSc2
Jižní	jižní	k2eAgFnSc2d1
Austrálie	Austrálie	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1895	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Souběžně	souběžně	k6eAd1
bylo	být	k5eAaImAgNnS
ženám	žena	k1gFnPc3
umožněno	umožnit	k5eAaPmNgNnS
kandidovat	kandidovat	k5eAaImF
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
prvním	první	k4xOgInSc7
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Domorodí	domorodý	k2eAgMnPc1d1
australští	australský	k2eAgMnPc1d1
muži	muž	k1gMnPc1
získali	získat	k5eAaPmAgMnP
právo	právo	k1gNnSc4
volit	volit	k5eAaImF
ve	v	k7c6
Victorii	Victorie	k1gFnSc6
<g/>
,	,	kIx,
Novém	nový	k2eAgInSc6d1
Jižním	jižní	k2eAgInSc6d1
Walesu	Wales	k1gInSc6
<g/>
,	,	kIx,
Tasmánii	Tasmánie	k1gFnSc6
a	a	k8xC
Jižní	jižní	k2eAgFnSc6d1
Austrálii	Austrálie	k1gFnSc6
stejně	stejně	k6eAd1
jako	jako	k9
ženy	žena	k1gFnSc2
roku	rok	k1gInSc2
1895	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pouze	pouze	k6eAd1
Queensland	Queenslanda	k1gFnPc2
a	a	k8xC
západní	západní	k2eAgFnSc2d1
Austrálie	Austrálie	k1gFnSc2
zakázaly	zakázat	k5eAaPmAgInP
domorodcům	domorodec	k1gMnPc3
hlasovat	hlasovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c7
jednotlivými	jednotlivý	k2eAgInPc7d1
státy	stát	k1gInPc7
na	na	k7c6
kontinentu	kontinent	k1gInSc6
existovala	existovat	k5eAaImAgFnS
jistá	jistý	k2eAgFnSc1d1
rivalita	rivalita	k1gFnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
pozvolna	pozvolna	k6eAd1
se	se	k3xPyFc4
rodilo	rodit	k5eAaImAgNnS
i	i	k9
vědomí	vědomí	k1gNnSc1
jednoty	jednota	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgMnSc1
<g/>
,	,	kIx,
kdo	kdo	k3yInSc1,k3yRnSc1,k3yQnSc1
začal	začít	k5eAaPmAgMnS
mluvit	mluvit	k5eAaImF
o	o	k7c6
nutnosti	nutnost	k1gFnSc6
federální	federální	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
premiér	premiér	k1gMnSc1
Nového	Nového	k2eAgInSc2d1
Jižního	jižní	k2eAgInSc2d1
Walesu	Wales	k1gInSc2
Henry	Henry	k1gMnSc1
Parkes	Parkes	k1gMnSc1
roku	rok	k1gInSc2
1889	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
je	být	k5eAaImIp3nS
zván	zván	k2eAgInSc4d1
„	„	k?
<g/>
otcem	otec	k1gMnSc7
federace	federace	k1gFnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
56	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sjednocení	sjednocení	k1gNnSc1
a	a	k8xC
federalizace	federalizace	k1gFnSc1
Austrálie	Austrálie	k1gFnSc2
</s>
<s>
První	první	k4xOgNnSc1
jednání	jednání	k1gNnSc1
federálního	federální	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
roku	rok	k1gInSc2
1901	#num#	k4
<g/>
,	,	kIx,
obraz	obraz	k1gInSc4
Toma	Tom	k1gMnSc2
Robertse	Roberts	k1gMnSc2
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1890	#num#	k4
se	se	k3xPyFc4
zástupci	zástupce	k1gMnPc1
šesti	šest	k4xCc2
kolonií	kolonie	k1gFnPc2
a	a	k8xC
Nového	Nového	k2eAgInSc2d1
Zélandu	Zéland	k1gInSc2
setkali	setkat	k5eAaPmAgMnP
v	v	k7c6
Melbourne	Melbourne	k1gNnSc6
a	a	k8xC
vyzvali	vyzvat	k5eAaPmAgMnP
ke	k	k7c3
sjednocení	sjednocení	k1gNnSc3
země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jmenovali	jmenovat	k5eAaImAgMnP,k5eAaBmAgMnP
též	též	k9
zástupce	zástupce	k1gMnSc1
do	do	k7c2
komise	komise	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
měla	mít	k5eAaImAgFnS
připravit	připravit	k5eAaPmF
federální	federální	k2eAgFnSc4d1
ústavu	ústava	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledkem	výsledek	k1gInSc7
byl	být	k5eAaImAgInS
návrh	návrh	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
byl	být	k5eAaImAgInS
roku	rok	k1gInSc2
1898	#num#	k4
předložen	předložit	k5eAaPmNgInS
obyvatelům	obyvatel	k1gMnPc3
čtyř	čtyři	k4xCgFnPc2
kolonií	kolonie	k1gFnPc2
ke	k	k7c3
schválení	schválení	k1gNnSc3
v	v	k7c6
referendu	referendum	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nový	nový	k2eAgInSc1d1
Jižní	jižní	k2eAgInSc1d1
Wales	Wales	k1gInSc1
návrh	návrh	k1gInSc4
však	však	k9
zamítl	zamítnout	k5eAaPmAgInS
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
byl	být	k5eAaImAgInS
upraven	upravit	k5eAaPmNgInS
a	a	k8xC
roku	rok	k1gInSc2
1899	#num#	k4
se	se	k3xPyFc4
konala	konat	k5eAaImAgFnS
druhá	druhý	k4xOgFnSc1
série	série	k1gFnSc1
referend	referendum	k1gNnPc2
<g/>
,	,	kIx,
tentokrát	tentokrát	k6eAd1
již	již	k9
pěti	pět	k4xCc2
kolonií	kolonie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Návrh	návrh	k1gInSc1
byl	být	k5eAaImAgInS
schválen	schválit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1900	#num#	k4
ho	on	k3xPp3gMnSc4
schválil	schválit	k5eAaPmAgInS
i	i	k9
britský	britský	k2eAgInSc1d1
parlament	parlament	k1gInSc1
a	a	k8xC
podepsala	podepsat	k5eAaPmAgFnS
královna	královna	k1gFnSc1
Viktorie	Viktoria	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzápětí	vzápětí	k6eAd1
se	se	k3xPyFc4
k	k	k7c3
pěti	pět	k4xCc2
koloniím	kolonie	k1gFnPc3
připojila	připojit	k5eAaPmAgFnS
i	i	k9
Západní	západní	k2eAgFnSc1d1
Austrálie	Austrálie	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
lednem	leden	k1gInSc7
1901	#num#	k4
tak	tak	k8xC,k8xS
vznikla	vzniknout	k5eAaPmAgFnS
Australská	australský	k2eAgFnSc1d1
federace	federace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Severní	severní	k2eAgFnSc1d1
teritorium	teritorium	k1gNnSc4
bylo	být	k5eAaImAgNnS
pod	pod	k7c4
pravomoc	pravomoc	k1gFnSc4
federální	federální	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
převedeno	převést	k5eAaPmNgNnS
roku	rok	k1gInSc2
1911	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnPc1
federální	federální	k2eAgFnPc1d1
volby	volba	k1gFnPc1
se	se	k3xPyFc4
konaly	konat	k5eAaImAgFnP
v	v	k7c6
březnu	březen	k1gInSc6
1901	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zvítězila	zvítězit	k5eAaPmAgFnS
liberální	liberální	k2eAgFnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
ochranářská	ochranářský	k2eAgFnSc1d1
Protectionist	Protectionist	k1gInSc1
Party	party	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
podpoře	podpora	k1gFnSc3
třetích	třetí	k4xOgNnPc2
labouristů	labourista	k1gMnPc2
sestavila	sestavit	k5eAaPmAgFnS
první	první	k4xOgInSc4
vládní	vládní	k2eAgInSc4d1
kabinet	kabinet	k1gInSc4
<g/>
,	,	kIx,
s	s	k7c7
Edmundem	Edmund	k1gMnSc7
Bartonem	Barton	k1gInSc7
v	v	k7c6
čele	čelo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavním	hlavní	k2eAgNnSc7d1
městem	město	k1gNnSc7
Austrálie	Austrálie	k1gFnSc2
bylo	být	k5eAaImAgNnS
od	od	k7c2
roku	rok	k1gInSc2
1901	#num#	k4
Melbourne	Melbourne	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vláda	vláda	k1gFnSc1
zde	zde	k6eAd1
sídlila	sídlit	k5eAaImAgFnS
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1927	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1911	#num#	k4
nicméně	nicméně	k8xC
bylo	být	k5eAaImAgNnS
rozhodnuto	rozhodnout	k5eAaPmNgNnS
o	o	k7c6
výstavbě	výstavba	k1gFnSc6
zcela	zcela	k6eAd1
nového	nový	k2eAgNnSc2d1
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Canberra	Canberr	k1gInSc2
<g/>
,	,	kIx,
kam	kam	k6eAd1
se	se	k3xPyFc4
federální	federální	k2eAgFnSc1d1
moc	moc	k1gFnSc1
postupně	postupně	k6eAd1
přesouvala	přesouvat	k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s>
Australští	australský	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
za	za	k7c2
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
v	v	k7c6
Egyptě	Egypt	k1gInSc6
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
se	se	k3xPyFc4
poté	poté	k6eAd1
podílela	podílet	k5eAaImAgFnS
na	na	k7c6
první	první	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Více	hodně	k6eAd2
než	než	k8xS
416	#num#	k4
000	#num#	k4
australských	australský	k2eAgMnPc2d1
mužů	muž	k1gMnPc2
se	se	k3xPyFc4
dobrovolně	dobrovolně	k6eAd1
přihlásilo	přihlásit	k5eAaPmAgNnS
do	do	k7c2
armády	armáda	k1gFnSc2
a	a	k8xC
zapojilo	zapojit	k5eAaPmAgNnS
se	se	k3xPyFc4
do	do	k7c2
bojů	boj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historik	historik	k1gMnSc1
Lloyd	Lloyd	k1gMnSc1
Robson	Robson	k1gMnSc1
to	ten	k3xDgNnSc4
odhadl	odhadnout	k5eAaPmAgMnS
na	na	k7c4
třetinu	třetina	k1gFnSc4
až	až	k8xS
polovinu	polovina	k1gFnSc4
způsobilé	způsobilý	k2eAgFnSc2d1
mužské	mužský	k2eAgFnSc2d1
populace	populace	k1gFnSc2
tehdejší	tehdejší	k2eAgFnSc2d1
Austrálie	Austrálie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krvavé	krvavý	k2eAgFnPc4d1
bylo	být	k5eAaImAgNnS
jejich	jejich	k3xOp3gNnSc7
angažmá	angažmá	k1gNnSc7
při	při	k7c6
pokusu	pokus	k1gInSc6
dobýt	dobýt	k5eAaPmF
Gallipoli	Gallipoli	k1gNnSc4
v	v	k7c6
tehdejší	tehdejší	k2eAgFnSc6d1
Osmanské	osmanský	k2eAgFnSc6d1
říši	říš	k1gFnSc6
(	(	kIx(
<g/>
dnešním	dnešní	k2eAgNnSc6d1
Turecku	Turecko	k1gNnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zahynulo	zahynout	k5eAaPmAgNnS
při	při	k7c6
něm	on	k3xPp3gNnSc6
během	během	k7c2
osmi	osm	k4xCc2
měsíců	měsíc	k1gInPc2
bojů	boj	k1gInPc2
8141	#num#	k4
Australanů	Australan	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
57	#num#	k4
<g/>
]	]	kIx)
Ačkoli	ačkoli	k8xS
britsko-australské	britsko-australský	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
u	u	k7c2
Gallipoli	Gallipoli	k1gNnSc2
prohrály	prohrát	k5eAaPmAgFnP
<g/>
,	,	kIx,
pro	pro	k7c4
Australany	Australan	k1gMnPc4
se	se	k3xPyFc4
Gallipoli	Gallipoli	k1gNnSc2
stalo	stát	k5eAaPmAgNnS
významným	významný	k2eAgMnSc7d1
stavebním	stavební	k2eAgInSc7d1
kamenem	kámen	k1gInSc7
národní	národní	k2eAgFnSc2d1
identity	identita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Památku	památka	k1gFnSc4
veteránů	veterán	k1gMnPc2
si	se	k3xPyFc3
Australané	Australan	k1gMnPc1
také	také	k9
každoročně	každoročně	k6eAd1
připomínají	připomínat	k5eAaImIp3nP
25	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c4
den	den	k1gInSc4
vylodění	vylodění	k1gNnSc2
na	na	k7c4
Gallipoli	Gallipoli	k1gNnSc4
v	v	k7c6
roce	rok	k1gInSc6
1915	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
bojištích	bojiště	k1gNnPc6
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
zahynulo	zahynout	k5eAaPmAgNnS
celkem	celkem	k6eAd1
60	#num#	k4
000	#num#	k4
Australanů	Australan	k1gMnPc2
a	a	k8xC
160	#num#	k4
000	#num#	k4
bylo	být	k5eAaImAgNnS
zraněno	zranit	k5eAaPmNgNnS
<g/>
,	,	kIx,
takže	takže	k8xS
Australané	Australan	k1gMnPc1
patřili	patřit	k5eAaImAgMnP
k	k	k7c3
národům	národ	k1gInPc3
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
za	za	k7c4
válku	válka	k1gFnSc4
-	-	kIx~
relativně	relativně	k6eAd1
-	-	kIx~
zaplatily	zaplatit	k5eAaPmAgInP
nejvíce	nejvíce	k6eAd1,k6eAd3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
svou	svůj	k3xOyFgFnSc4
oběť	oběť	k1gFnSc4
ovšem	ovšem	k9
Austrálie	Austrálie	k1gFnSc2
po	po	k7c6
válce	válka	k1gFnSc6
žádala	žádat	k5eAaImAgFnS
větší	veliký	k2eAgFnSc4d2
míru	míra	k1gFnSc4
nezávislosti	nezávislost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Politická	politický	k2eAgFnSc1d1
závislost	závislost	k1gFnSc1
na	na	k7c6
Británii	Británie	k1gFnSc6
byla	být	k5eAaImAgFnS
prakticky	prakticky	k6eAd1
vynulována	vynulován	k2eAgFnSc1d1
<g/>
,	,	kIx,
když	když	k8xS
Britové	Brit	k1gMnPc1
roku	rok	k1gInSc2
1926	#num#	k4
udělili	udělit	k5eAaPmAgMnP
Austrálii	Austrálie	k1gFnSc4
status	status	k1gInSc1
dominia	dominion	k1gNnSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgNnSc1
je	být	k5eAaImIp3nS
jen	jen	k9
symbolicky	symbolicky	k6eAd1
podřízeno	podřízen	k2eAgNnSc4d1
britské	britský	k2eAgFnSc3d1
koruně	koruna	k1gFnSc3
<g/>
,	,	kIx,
jinak	jinak	k6eAd1
v	v	k7c6
žádném	žádný	k3yNgInSc6
jiném	jiný	k2eAgInSc6d1
smyslu	smysl	k1gInSc6
<g/>
,	,	kIx,
což	což	k3yQnSc4,k3yRnSc4
stvrdil	stvrdit	k5eAaPmAgInS
tzv.	tzv.	kA
Westminsterský	Westminsterský	k2eAgInSc4d1
status	status	k1gInSc4
z	z	k7c2
roku	rok	k1gInSc2
1931	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
však	však	k9
paradoxně	paradoxně	k6eAd1
Australané	Australan	k1gMnPc1
přijali	přijmout	k5eAaPmAgMnP
až	až	k6eAd1
po	po	k7c6
jistém	jistý	k2eAgNnSc6d1
zdráhání	zdráhání	k1gNnSc6
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
v	v	k7c6
roce	rok	k1gInSc6
1942	#num#	k4
<g/>
,	,	kIx,
neboť	neboť	k8xC
přináležitost	přináležitost	k1gFnSc1
k	k	k7c3
Británii	Británie	k1gFnSc3
jim	on	k3xPp3gMnPc3
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
nijak	nijak	k6eAd1
nevadila	vadit	k5eNaImAgFnS
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
</s>
<s>
Australští	australský	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
TobrukuJeště	TobrukuJešť	k1gFnSc2
na	na	k7c6
počátku	počátek	k1gInSc6
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
bylo	být	k5eAaImAgNnS
zřejmé	zřejmý	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
emocionální	emocionální	k2eAgFnSc1d1
vazba	vazba	k1gFnSc1
na	na	k7c4
Spojené	spojený	k2eAgNnSc4d1
království	království	k1gNnSc4
je	být	k5eAaImIp3nS
nesmírně	smírně	k6eNd1
silná	silný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Australané	Australan	k1gMnPc1
vyrazili	vyrazit	k5eAaPmAgMnP
Britům	Brit	k1gMnPc3
na	na	k7c4
pomoc	pomoc	k1gFnSc4
podobně	podobně	k6eAd1
jako	jako	k8xS,k8xC
za	za	k7c2
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
-	-	kIx~
bojovali	bojovat	k5eAaImAgMnP
zejména	zejména	k9
u	u	k7c2
Tobruku	Tobruk	k1gInSc2
a	a	k8xC
u	u	k7c2
El	Ela	k1gFnPc2
Alameinu	Alamein	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
58	#num#	k4
<g/>
]	]	kIx)
Stále	stále	k6eAd1
více	hodně	k6eAd2
však	však	k9
byli	být	k5eAaImAgMnP
Australané	Australan	k1gMnPc1
znepokojeni	znepokojit	k5eAaPmNgMnP
britskou	britský	k2eAgFnSc7d1
slabostí	slabost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marně	marně	k6eAd1
žádali	žádat	k5eAaImAgMnP
Brity	Brit	k1gMnPc4
o	o	k7c4
větší	veliký	k2eAgFnSc4d2
aktivitu	aktivita	k1gFnSc4
v	v	k7c6
Tichomoří	Tichomoří	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spojené	spojený	k2eAgInPc1d1
království	království	k1gNnSc1
již	již	k6eAd1
nebylo	být	k5eNaImAgNnS
globální	globální	k2eAgInPc4d1
války	válek	k1gInPc4
schopno	schopen	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
tedy	tedy	k8xC
vydala	vydat	k5eAaPmAgFnS
historické	historický	k2eAgNnSc4d1
prohlášení	prohlášení	k1gNnSc4
<g/>
,	,	kIx,
že	že	k8xS
obrana	obrana	k1gFnSc1
demokracie	demokracie	k1gFnSc2
v	v	k7c6
Tichomoří	Tichomoří	k1gNnSc6
je	být	k5eAaImIp3nS
nově	nově	k6eAd1
na	na	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
a	a	k8xC
Austrálii	Austrálie	k1gFnSc6
a	a	k8xC
jejich	jejich	k3xOp3gFnSc1
spolupráce	spolupráce	k1gFnSc1
musí	muset	k5eAaImIp3nS
probíhat	probíhat	k5eAaImF
bez	bez	k7c2
ohledu	ohled	k1gInSc2
na	na	k7c4
tradiční	tradiční	k2eAgFnPc4d1
vazby	vazba	k1gFnPc4
k	k	k7c3
Británii	Británie	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
byl	být	k5eAaImAgInS
ohlášen	ohlášen	k2eAgInSc4d1
významný	významný	k2eAgInSc4d1
geopolitický	geopolitický	k2eAgInSc4d1
přesun	přesun	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozhodujícím	rozhodující	k2eAgMnSc7d1
spojencem	spojenec	k1gMnSc7
Austrálie	Austrálie	k1gFnSc2
(	(	kIx(
<g/>
až	až	k9
do	do	k7c2
dnešních	dnešní	k2eAgInPc2d1
dnů	den	k1gInPc2
<g/>
)	)	kIx)
se	se	k3xPyFc4
staly	stát	k5eAaPmAgInP
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
<g/>
,	,	kIx,
poslední	poslední	k2eAgFnPc1d1
reálné	reálný	k2eAgFnPc1d1
mezinárodněpolitické	mezinárodněpolitický	k2eAgFnPc1d1
vazby	vazba	k1gFnPc1
na	na	k7c4
Londýn	Londýn	k1gInSc4
byly	být	k5eAaImAgFnP
přeťaty	přeťat	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Emocionálně	emocionálně	k6eAd1
toto	tento	k3xDgNnSc4
přetnutí	přetnutí	k1gNnSc4
podpořil	podpořit	k5eAaPmAgInS
šok	šok	k1gInSc1
australské	australský	k2eAgFnSc2d1
veřejnosti	veřejnost	k1gFnSc2
z	z	k7c2
britských	britský	k2eAgFnPc2d1
porážek	porážka	k1gFnPc2
v	v	k7c6
Malajsii	Malajsie	k1gFnSc6
a	a	k8xC
Singapuru	Singapur	k1gInSc6
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
zejména	zejména	k9
v	v	k7c6
Singapuru	Singapur	k1gInSc6
prožily	prožít	k5eAaPmAgFnP
potupný	potupný	k2eAgInSc4d1
akt	akt	k1gInSc4
kapitulace	kapitulace	k1gFnSc2
i	i	k8xC
australské	australský	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
zajetí	zajetí	k1gNnSc2
zde	zde	k6eAd1
padlo	padnout	k5eAaImAgNnS,k5eAaPmAgNnS
15	#num#	k4
000	#num#	k4
australských	australský	k2eAgMnPc2d1
vojáků	voják	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnozí	mnohý	k2eAgMnPc1d1
ze	z	k7c2
zajatců	zajatec	k1gMnPc2
pak	pak	k6eAd1
prožili	prožít	k5eAaPmAgMnP
peklo	peklo	k1gNnSc4
-	-	kIx~
v	v	k7c6
roce	rok	k1gInSc6
1943	#num#	k4
jich	on	k3xPp3gMnPc2
zemřelo	zemřít	k5eAaPmAgNnS
2815	#num#	k4
při	při	k7c6
stavbě	stavba	k1gFnSc6
japonské	japonský	k2eAgFnSc2d1
barmsko-thajské	barmsko-thajský	k2eAgFnSc2d1
železnice	železnice	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1944	#num#	k4
Japonci	Japonec	k1gMnPc1
vyslali	vyslat	k5eAaPmAgMnP
2000	#num#	k4
australských	australský	k2eAgMnPc2d1
zajatců	zajatec	k1gMnPc2
na	na	k7c4
pochod	pochod	k1gInSc4
smrti	smrt	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc4,k3yIgInSc4,k3yRgInSc4
přežilo	přežít	k5eAaPmAgNnS
jen	jen	k6eAd1
šest	šest	k4xCc1
lidí	člověk	k1gMnPc2
-	-	kIx~
šlo	jít	k5eAaImAgNnS
o	o	k7c4
nejhorší	zlý	k2eAgInSc4d3
válečný	válečný	k2eAgInSc4d1
zločin	zločin	k1gInSc4
spáchaný	spáchaný	k2eAgInSc4d1
na	na	k7c6
Australanech	Australan	k1gMnPc6
v	v	k7c6
celé	celý	k2eAgFnSc6d1
historii	historie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
březnu	březen	k1gInSc6
1942	#num#	k4
byly	být	k5eAaImAgFnP
všechny	všechen	k3xTgFnPc1
australské	australský	k2eAgFnPc1d1
vojenské	vojenský	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
podřízeny	podřízen	k2eAgFnPc1d1
americkému	americký	k2eAgMnSc3d1
velení	velení	k1gNnSc2
<g/>
,	,	kIx,
konkrétně	konkrétně	k6eAd1
generálu	generál	k1gMnSc3
Douglasovi	Douglas	k1gMnSc3
MacArthurovi	MacArthur	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japonci	Japonec	k1gMnPc7
poté	poté	k6eAd1
provedli	provést	k5eAaPmAgMnP
na	na	k7c6
území	území	k1gNnSc6
Austrálie	Austrálie	k1gFnSc2
více	hodně	k6eAd2
než	než	k8xS
sto	sto	k4xCgNnSc4
náletů	nálet	k1gInPc2
<g/>
,	,	kIx,
útočili	útočit	k5eAaImAgMnP
i	i	k9
ponorkami	ponorka	k1gFnPc7
na	na	k7c4
přístavy	přístav	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
zahájili	zahájit	k5eAaPmAgMnP
invazi	invaze	k1gFnSc3
<g/>
,	,	kIx,
na	na	k7c6
Nové	Nové	k2eAgFnSc6d1
Guineji	Guinea	k1gFnSc6
(	(	kIx(
<g/>
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
měla	mít	k5eAaImAgFnS
Austrálie	Austrálie	k1gFnSc1
ve	v	k7c6
správě	správa	k1gFnSc6
od	od	k7c2
Versailleské	versailleský	k2eAgFnSc2d1
konference	konference	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1919	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
červencem	červenec	k1gInSc7
a	a	k8xC
listopadem	listopad	k1gInSc7
1942	#num#	k4
australská	australský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
japonský	japonský	k2eAgInSc4d1
útok	útok	k1gInSc4
zastavila	zastavit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bitva	bitva	k1gFnSc1
o	o	k7c4
Milne	Miln	k1gInSc5
Bay	Bay	k1gMnSc6
v	v	k7c6
srpnu	srpen	k1gInSc6
1942	#num#	k4
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
Australané	Australan	k1gMnPc1
svedli	svést	k5eAaPmAgMnP
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
první	první	k4xOgFnSc7
porážkou	porážka	k1gFnSc7
japonských	japonský	k2eAgFnPc2d1
pozemních	pozemní	k2eAgFnPc2d1
sil	síla	k1gFnPc2
během	během	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
59	#num#	k4
<g/>
]	]	kIx)
Bitva	bitva	k1gFnSc1
o	o	k7c4
Novou	nový	k2eAgFnSc4d1
Guineu	Guinea	k1gFnSc4
nicméně	nicméně	k8xC
trvala	trvat	k5eAaImAgFnS
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1945	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
konci	konec	k1gInSc6
války	válka	k1gFnSc2
stáli	stát	k5eAaImAgMnP
Australané	Australan	k1gMnPc1
znovu	znovu	k6eAd1
mezi	mezi	k7c7
vítězi	vítěz	k1gMnPc7
<g/>
,	,	kIx,
ovšem	ovšem	k9
za	za	k7c4
cenu	cena	k1gFnSc4
40	#num#	k4
000	#num#	k4
obětí	oběť	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Robert	Robert	k1gMnSc1
Menzies	Menzies	k1gMnSc1
<g/>
,	,	kIx,
otec	otec	k1gMnSc1
poválečného	poválečný	k2eAgInSc2d1
blahobytu	blahobyt	k1gInSc2
</s>
<s>
Nejsilnější	silný	k2eAgFnSc7d3
politickou	politický	k2eAgFnSc7d1
stranou	strana	k1gFnSc7
se	se	k3xPyFc4
po	po	k7c6
válce	válka	k1gFnSc6
stali	stát	k5eAaPmAgMnP
<g/>
,	,	kIx,
po	po	k7c6
krátkém	krátký	k2eAgNnSc6d1
vzepětí	vzepětí	k1gNnSc6
levice	levice	k1gFnSc2
<g/>
,	,	kIx,
liberálové	liberál	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ti	ten	k3xDgMnPc1
získali	získat	k5eAaPmAgMnP
až	až	k6eAd1
mocenskou	mocenský	k2eAgFnSc4d1
hegemonii	hegemonie	k1gFnSc4
–	–	k?
vládli	vládnout	k5eAaImAgMnP
zemi	zem	k1gFnSc4
v	v	k7c6
letech	let	k1gInPc6
1949	#num#	k4
<g/>
–	–	k?
<g/>
1972	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ústřední	ústřední	k2eAgFnSc7d1
postavou	postava	k1gFnSc7
poválečné	poválečný	k2eAgFnSc2d1
politiky	politika	k1gFnSc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
Robert	Robert	k1gMnSc1
Menzies	Menzies	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Post	post	k1gInSc1
premiéra	premiér	k1gMnSc4
zastával	zastávat	k5eAaImAgInS
v	v	k7c6
letech	léto	k1gNnPc6
1949	#num#	k4
<g/>
–	–	k?
<g/>
1966	#num#	k4
<g/>
,	,	kIx,
tedy	tedy	k9
více	hodně	k6eAd2
než	než	k8xS
osmnáct	osmnáct	k4xCc4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
australský	australský	k2eAgInSc1d1
rekord	rekord	k1gInSc1
<g/>
,	,	kIx,
jemuž	jenž	k3xRgInSc3
se	se	k3xPyFc4
po	po	k7c6
něm	on	k3xPp3gMnSc6
nikdo	nikdo	k3yNnSc1
ani	ani	k8xC
nepřiblížil	přiblížit	k5eNaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Liberálové	liberál	k1gMnPc1
pod	pod	k7c7
Menziesovým	Menziesový	k2eAgNnSc7d1
vedením	vedení	k1gNnSc7
se	se	k3xPyFc4
vyhoupli	vyhoupnout	k5eAaPmAgMnP
do	do	k7c2
sedla	sedlo	k1gNnSc2
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yRnSc1,k3yInSc1
v	v	k7c6
roce	rok	k1gInSc6
1949	#num#	k4
vedli	vést	k5eAaImAgMnP
v	v	k7c6
předvolební	předvolební	k2eAgFnSc6d1
kampani	kampaň	k1gFnSc6
tažení	tažení	k1gNnSc2
proti	proti	k7c3
záměru	záměr	k1gInSc3
labouristů	labourista	k1gMnPc2
znárodnit	znárodnit	k5eAaPmF
banky	banka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Komunistické	komunistický	k2eAgNnSc1d1
nebezpečí	nebezpečí	k1gNnSc1
vedlo	vést	k5eAaImAgNnS
Menziesovy	Menziesův	k2eAgMnPc4d1
liberály	liberál	k1gMnPc4
i	i	k9
k	k	k7c3
zapojení	zapojení	k1gNnSc3
se	se	k3xPyFc4
do	do	k7c2
korejské	korejský	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
60	#num#	k4
<g/>
]	]	kIx)
17	#num#	k4
000	#num#	k4
Australanů	Australan	k1gMnPc2
se	se	k3xPyFc4
zapojilo	zapojit	k5eAaPmAgNnS
do	do	k7c2
bojů	boj	k1gInPc2
<g/>
,	,	kIx,
počet	počet	k1gInSc1
australských	australský	k2eAgFnPc2d1
obětí	oběť	k1gFnPc2
činil	činit	k5eAaImAgInS
více	hodně	k6eAd2
než	než	k8xS
1	#num#	k4
500	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Australané	Australan	k1gMnPc1
podpořili	podpořit	k5eAaPmAgMnP
Američany	Američan	k1gMnPc4
i	i	k8xC
ve	v	k7c6
vietnamské	vietnamský	k2eAgFnSc6d1
válce	válka	k1gFnSc6
od	od	k7c2
poloviny	polovina	k1gFnSc2
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ztratili	ztratit	k5eAaPmAgMnP
v	v	k7c6
této	tento	k3xDgFnSc6
válce	válka	k1gFnSc6
500	#num#	k4
mužů	muž	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
61	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Menziesova	Menziesův	k2eAgFnSc1d1
dlouhá	dlouhý	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
je	být	k5eAaImIp3nS
spojena	spojit	k5eAaPmNgFnS
s	s	k7c7
mimořádným	mimořádný	k2eAgInSc7d1
ekonomickým	ekonomický	k2eAgInSc7d1
rozmachem	rozmach	k1gInSc7
<g/>
,	,	kIx,
pevným	pevný	k2eAgNnSc7d1
spojenectvím	spojenectví	k1gNnSc7
se	s	k7c7
Spojenými	spojený	k2eAgInPc7d1
státy	stát	k1gInPc7
(	(	kIx(
<g/>
smlouva	smlouva	k1gFnSc1
SEATO	SEATO	kA
z	z	k7c2
roku	rok	k1gInSc2
1955	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
antikomunismem	antikomunismus	k1gInSc7
<g/>
,	,	kIx,
navázáním	navázání	k1gNnSc7
pevných	pevný	k2eAgInPc2d1
obchodních	obchodní	k2eAgInPc2d1
vztahů	vztah	k1gInPc2
se	se	k3xPyFc4
starým	starý	k2eAgMnSc7d1
nepřítelem	nepřítel	k1gMnSc7
Japonskem	Japonsko	k1gNnSc7
(	(	kIx(
<g/>
jež	jenž	k3xRgNnSc1
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
pro	pro	k7c4
Austrálii	Austrálie	k1gFnSc4
největším	veliký	k2eAgNnSc7d3
odbytištěm	odbytiště	k1gNnSc7
<g/>
,	,	kIx,
zejména	zejména	k9
uhlí	uhlí	k1gNnSc4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
<g/>
,	,	kIx,
neschopností	neschopnost	k1gFnSc7
plně	plně	k6eAd1
otevřít	otevřít	k5eAaPmF
trh	trh	k1gInSc4
a	a	k8xC
zrušit	zrušit	k5eAaPmF
tradiční	tradiční	k2eAgNnPc1d1
ochranářská	ochranářský	k2eAgNnPc1d1
opatření	opatření	k1gNnPc1
(	(	kIx(
<g/>
to	ten	k3xDgNnSc1
byl	být	k5eAaImAgInS
kompromis	kompromis	k1gInSc1
s	s	k7c7
odbory	odbor	k1gInPc7
a	a	k8xC
zemědělci	zemědělec	k1gMnPc7
<g/>
)	)	kIx)
a	a	k8xC
politikou	politika	k1gFnSc7
podpory	podpora	k1gFnSc2
hromadné	hromadný	k2eAgFnSc2d1
imigrace	imigrace	k1gFnSc2
z	z	k7c2
Evropy	Evropa	k1gFnSc2
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
1970	#num#	k4
po	po	k7c6
opuštění	opuštění	k1gNnSc6
tzv.	tzv.	kA
„	„	k?
<g/>
politiky	politika	k1gFnSc2
bílé	bílý	k2eAgFnSc2d1
Austrálie	Austrálie	k1gFnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
pak	pak	k6eAd1
i	i	k9
z	z	k7c2
Asie	Asie	k1gFnSc2
a	a	k8xC
z	z	k7c2
dalších	další	k2eAgFnPc2d1
částí	část	k1gFnPc2
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
způsobilo	způsobit	k5eAaPmAgNnS
radikální	radikální	k2eAgFnSc4d1
přeměnu	přeměna	k1gFnSc4
australského	australský	k2eAgInSc2d1
demografického	demografický	k2eAgInSc2d1
obrazu	obraz	k1gInSc2
<g/>
,	,	kIx,
její	její	k3xOp3gFnSc2
kultury	kultura	k1gFnSc2
i	i	k8xC
vnímání	vnímání	k1gNnSc2
v	v	k7c6
očích	oko	k1gNnPc6
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1945	#num#	k4
až	až	k9
1985	#num#	k4
migrovalo	migrovat	k5eAaImAgNnS
do	do	k7c2
Austrálie	Austrálie	k1gFnSc2
4,2	4,2	k4
milionu	milion	k4xCgInSc2
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
asi	asi	k9
40	#num#	k4
procent	procento	k1gNnPc2
pocházelo	pocházet	k5eAaImAgNnS
z	z	k7c2
Británie	Británie	k1gFnSc2
a	a	k8xC
Irska	Irsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Typickou	typický	k2eAgFnSc7d1
byl	být	k5eAaImAgMnS
pro	pro	k7c4
Menziesovu	Menziesův	k2eAgFnSc4d1
éru	éra	k1gFnSc4
jakýsi	jakýsi	k3yIgInSc1
společenský	společenský	k2eAgInSc1d1
kompromis	kompromis	k1gInSc1
–	–	k?
hodně	hodně	k6eAd1
prostoru	prostor	k1gInSc2
pro	pro	k7c4
obchod	obchod	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
za	za	k7c4
cenu	cena	k1gFnSc4
značného	značný	k2eAgNnSc2d1
přerozdělování	přerozdělování	k1gNnSc2
bohatství	bohatství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šlo	jít	k5eAaImAgNnS
o	o	k7c4
jakousi	jakýsi	k3yIgFnSc4
australskou	australský	k2eAgFnSc4d1
variantu	varianta	k1gFnSc4
evropského	evropský	k2eAgInSc2d1
sociálního	sociální	k2eAgInSc2d1
státu	stát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
šedesátých	šedesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
měla	mít	k5eAaImAgFnS
Austrálie	Austrálie	k1gFnSc1
nejrovnoměrnější	rovnoměrný	k2eAgNnSc1d3
rozdělení	rozdělení	k1gNnSc4
příjmů	příjem	k1gInPc2
v	v	k7c6
západním	západní	k2eAgInSc6d1
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1972	#num#	k4
Nová	nový	k2eAgFnSc1d1
Guinea	Guinea	k1gFnSc1
získala	získat	k5eAaPmAgFnS
autonomii	autonomie	k1gFnSc4
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1975	#num#	k4
vznikl	vzniknout	k5eAaPmAgInS
nový	nový	k2eAgInSc1d1
<g/>
,	,	kIx,
samostatný	samostatný	k2eAgInSc1d1
stát	stát	k1gInSc1
Papua	Papu	k2eAgFnSc1d1
Nová	nový	k2eAgFnSc1d1
Guinea	Guinea	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nauru	Naur	k1gInSc2
získalo	získat	k5eAaPmAgNnS
nezávislost	nezávislost	k1gFnSc4
již	již	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1968	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Poslední	poslední	k2eAgFnPc1d1
ústavní	ústavní	k2eAgFnPc1d1
vazby	vazba	k1gFnPc1
mezi	mezi	k7c7
Austrálií	Austrálie	k1gFnSc7
a	a	k8xC
Spojeným	spojený	k2eAgNnSc7d1
královstvím	království	k1gNnSc7
byly	být	k5eAaImAgFnP
zrušeny	zrušit	k5eAaPmNgFnP
v	v	k7c6
roce	rok	k1gInSc6
1986	#num#	k4
přijetím	přijetí	k1gNnSc7
tzv.	tzv.	kA
Zákona	zákon	k1gInSc2
o	o	k7c6
Austrálii	Austrálie	k1gFnSc6
odstraňujícího	odstraňující	k2eAgInSc2d1
všechny	všechen	k3xTgInPc1
britské	britský	k2eAgInPc1d1
vlivy	vliv	k1gInPc1
v	v	k7c6
jednotlivých	jednotlivý	k2eAgInPc6d1
australských	australský	k2eAgInPc6d1
státech	stát	k1gInPc6
a	a	k8xC
rušícího	rušící	k2eAgInSc2d1
soudní	soudní	k2eAgFnSc4d1
odvolatelnost	odvolatelnost	k1gFnSc4
k	k	k7c3
orgánům	orgán	k1gInPc3
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přechod	přechod	k1gInSc1
k	k	k7c3
republikánskému	republikánský	k2eAgNnSc3d1
zřízení	zřízení	k1gNnSc3
však	však	k9
australští	australský	k2eAgMnPc1d1
voliči	volič	k1gMnPc1
v	v	k7c6
roce	rok	k1gInSc6
1999	#num#	k4
odmítli	odmítnout	k5eAaPmAgMnP
<g/>
,	,	kIx,
55	#num#	k4
<g/>
%	%	kIx~
většinou	většina	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Formální	formální	k2eAgFnSc7d1
hlavou	hlava	k1gFnSc7
státu	stát	k1gInSc2
tak	tak	k6eAd1
zůstává	zůstávat	k5eAaImIp3nS
britský	britský	k2eAgMnSc1d1
monarcha	monarcha	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1972	#num#	k4
se	se	k3xPyFc4
do	do	k7c2
čela	čelo	k1gNnSc2
země	zem	k1gFnSc2
po	po	k7c6
mnoha	mnoho	k4c6
letech	léto	k1gNnPc6
postavil	postavit	k5eAaPmAgMnS
labourista	labourista	k1gMnSc1
<g/>
,	,	kIx,
Gough	Gough	k1gMnSc1
Whitlam	Whitlam	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začal	začít	k5eAaPmAgMnS
klást	klást	k5eAaImF
důraz	důraz	k1gInSc4
na	na	k7c4
budoucnost	budoucnost	k1gFnSc4
Austrálie	Austrálie	k1gFnSc2
jako	jako	k8xS,k8xC
součást	součást	k1gFnSc1
regionu	region	k1gInSc2
jižní	jižní	k2eAgFnSc2d1
Asie	Asie	k1gFnSc2
a	a	k8xC
Pacifiku	Pacifik	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Labouristé	labourista	k1gMnPc1
byli	být	k5eAaImAgMnP
znovu	znovu	k6eAd1
u	u	k7c2
moci	moc	k1gFnSc2
v	v	k7c6
letech	let	k1gInPc6
1983	#num#	k4
<g/>
–	–	k?
<g/>
1996	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bob	Bob	k1gMnSc1
Hawke	Hawke	k1gFnSc4
věřil	věřit	k5eAaImAgMnS
v	v	k7c4
tradiční	tradiční	k2eAgNnSc4d1
spojenectví	spojenectví	k1gNnSc4
s	s	k7c7
USA	USA	kA
a	a	k8xC
zapojil	zapojit	k5eAaPmAgInS
se	se	k3xPyFc4
do	do	k7c2
války	válka	k1gFnSc2
v	v	k7c6
Zálivu	záliv	k1gInSc6
roku	rok	k1gInSc2
1991	#num#	k4
<g/>
,	,	kIx,
druhý	druhý	k4xOgInSc1
labouristický	labouristický	k2eAgMnSc1d1
vůdce	vůdce	k1gMnSc1
Paul	Paul	k1gMnSc1
Keating	Keating	k1gInSc1
však	však	k9
obnovil	obnovit	k5eAaPmAgInS
důraz	důraz	k1gInSc1
na	na	k7c6
kooperaci	kooperace	k1gFnSc6
v	v	k7c6
rámci	rámec	k1gInSc6
APEC	APEC	kA
<g/>
,	,	kIx,
tedy	tedy	k8xC
se	se	k3xPyFc4
státy	stát	k1gInPc1
jihovýchodní	jihovýchodní	k2eAgFnSc2d1
Asie	Asie	k1gFnSc2
<g/>
,	,	kIx,
zvláště	zvláště	k6eAd1
zlepšil	zlepšit	k5eAaPmAgMnS
vztahy	vztah	k1gInPc4
s	s	k7c7
Indonésií	Indonésie	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Liberál	liberál	k1gMnSc1
John	John	k1gMnSc1
Howard	Howard	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
nastoupil	nastoupit	k5eAaPmAgMnS
do	do	k7c2
premiérského	premiérský	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1996	#num#	k4
a	a	k8xC
vydržel	vydržet	k5eAaPmAgMnS
v	v	k7c6
něm	on	k3xPp3gMnSc6
deset	deset	k4xCc1
let	léto	k1gNnPc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
vrátil	vrátit	k5eAaPmAgInS
spíše	spíše	k9
k	k	k7c3
proamerické	proamerický	k2eAgFnSc3d1
politice	politika	k1gFnSc3
a	a	k8xC
vyslal	vyslat	k5eAaPmAgMnS
vojáky	voják	k1gMnPc4
do	do	k7c2
války	válka	k1gFnSc2
v	v	k7c6
Afghánistánu	Afghánistán	k1gInSc6
a	a	k8xC
Iráku	Irák	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
62	#num#	k4
<g/>
]	]	kIx)
Jako	jako	k9
první	první	k4xOgMnSc1
poválečný	poválečný	k2eAgMnSc1d1
australský	australský	k2eAgMnSc1d1
vůdce	vůdce	k1gMnSc1
zpřísnil	zpřísnit	k5eAaPmAgMnS
imigrační	imigrační	k2eAgFnSc4d1
politiku	politika	k1gFnSc4
<g/>
,	,	kIx,
když	když	k8xS
zavedl	zavést	k5eAaPmAgMnS
zadržování	zadržování	k1gNnPc4
žadatelů	žadatel	k1gMnPc2
o	o	k7c4
azyl	azyl	k1gInSc4
v	v	k7c6
detenčních	detenční	k2eAgInPc6d1
táborech	tábor	k1gInPc6
mimo	mimo	k7c4
australské	australský	k2eAgNnSc4d1
území	území	k1gNnSc4
v	v	k7c6
Nauru	Naur	k1gInSc6
a	a	k8xC
na	na	k7c6
Papui	Papu	k1gFnSc6
Nové	Nové	k2eAgFnSc3d1
Guineji	Guinea	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
63	#num#	k4
<g/>
]	]	kIx)
Přišel	přijít	k5eAaPmAgMnS
též	též	k9
s	s	k7c7
vracením	vracení	k1gNnSc7
plavidel	plavidlo	k1gNnPc2
s	s	k7c7
nelegálními	legální	k2eNgMnPc7d1
uprchlíky	uprchlík	k1gMnPc7
zachycenými	zachycený	k2eAgMnPc7d1
na	na	k7c6
moři	moře	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Labouristé	labourista	k1gMnPc1
poté	poté	k6eAd1
vládli	vládnout	k5eAaImAgMnP
v	v	k7c6
letech	let	k1gInPc6
2007	#num#	k4
<g/>
–	–	k?
<g/>
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Austrálie	Austrálie	k1gFnSc1
dostala	dostat	k5eAaPmAgFnS
v	v	k7c6
tomto	tento	k3xDgNnSc6
období	období	k1gNnSc6
mj.	mj.	kA
první	první	k4xOgFnSc4
ženu	žena	k1gFnSc4
v	v	k7c6
premiérském	premiérský	k2eAgInSc6d1
úřadě	úřad	k1gInSc6
<g/>
,	,	kIx,
Julii	Julie	k1gFnSc4
Gillardovou	Gillardový	k2eAgFnSc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
64	#num#	k4
<g/>
]	]	kIx)
Vláda	vláda	k1gFnSc1
labouristů	labourista	k1gMnPc2
přinesla	přinést	k5eAaPmAgFnS
zemi	zem	k1gFnSc4
zintenzivnění	zintenzivnění	k1gNnSc2
vztahů	vztah	k1gInPc2
s	s	k7c7
jihovýchodní	jihovýchodní	k2eAgFnSc7d1
Asií	Asie	k1gFnSc7
a	a	k8xC
zrušení	zrušení	k1gNnSc1
Howardových	Howardův	k2eAgNnPc2d1
protimigračních	protimigrační	k2eAgNnPc2d1
opatření	opatření	k1gNnPc2
<g/>
,	,	kIx,
k	k	k7c3
nimž	jenž	k3xRgFnPc3
se	se	k3xPyFc4
však	však	k9
<g/>
,	,	kIx,
po	po	k7c6
silném	silný	k2eAgNnSc6d1
zintenzivnění	zintenzivnění	k1gNnSc6
imigrace	imigrace	k1gFnSc2
<g/>
,	,	kIx,
labouristé	labourista	k1gMnPc1
pozvolna	pozvolna	k6eAd1
vraceli	vracet	k5eAaImAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2013	#num#	k4
vládli	vládnout	k5eAaImAgMnP
znovu	znovu	k6eAd1
liberálové	liberál	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ihned	ihned	k6eAd1
po	po	k7c6
převzetí	převzetí	k1gNnSc6
moci	moc	k1gFnSc2
opětovně	opětovně	k6eAd1
zpřísnili	zpřísnit	k5eAaPmAgMnP
migrační	migrační	k2eAgFnSc4d1
politiku	politika	k1gFnSc4
v	v	k7c6
rámci	rámec	k1gInSc6
operace	operace	k1gFnSc1
Sovereign	sovereign	k1gInSc1
Borders	Borders	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
65	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Geografie	geografie	k1gFnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Geografie	geografie	k1gFnSc2
Austrálie	Austrálie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Uluru	Ulura	k1gFnSc4
ve	v	k7c6
vyprahlém	vyprahlý	k2eAgNnSc6d1
vnitrozemí	vnitrozemí	k1gNnSc6
Austrálie	Austrálie	k1gFnSc2
</s>
<s>
Klimatické	klimatický	k2eAgFnPc1d1
zóny	zóna	k1gFnPc1
Austrálie	Austrálie	k1gFnSc2
podle	podle	k7c2
Köppenovy	Köppenův	k2eAgFnSc2d1
klasifikace	klasifikace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
7,617,930	7,617,930	k4
km²	km²	k?
pevninské	pevninský	k2eAgFnSc2d1
Austrálie	Austrálie	k1gFnSc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
na	na	k7c6
Indoaustralské	Indoaustralský	k2eAgFnSc6d1
desce	deska	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obklopena	obklopen	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
Indickým	indický	k2eAgMnSc7d1
<g/>
,	,	kIx,
Jižním	jižní	k2eAgMnSc7d1
a	a	k8xC
Tichým	tichý	k2eAgInSc7d1
oceánem	oceán	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
Asie	Asie	k1gFnSc2
je	být	k5eAaImIp3nS
oddělena	oddělit	k5eAaPmNgFnS
Arafurským	Arafurský	k2eAgNnSc7d1
a	a	k8xC
Timorským	Timorský	k2eAgNnSc7d1
mořem	moře	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gNnSc1
pobřeží	pobřeží	k1gNnSc1
je	být	k5eAaImIp3nS
dlouhé	dlouhý	k2eAgNnSc4d1
25	#num#	k4
760	#num#	k4
kilometrů	kilometr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Velký	velký	k2eAgInSc1d1
bariérový	bariérový	k2eAgInSc1d1
útes	útes	k1gInSc1
je	být	k5eAaImIp3nS
největším	veliký	k2eAgInSc7d3
korálovým	korálový	k2eAgInSc7d1
útesem	útes	k1gInSc7
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leží	ležet	k5eAaImIp3nS
poměrně	poměrně	k6eAd1
blízko	blízko	k6eAd1
severovýchodnímu	severovýchodní	k2eAgNnSc3d1
pobřeží	pobřeží	k1gNnSc3
<g/>
,	,	kIx,
dlouhý	dlouhý	k2eAgInSc1d1
je	být	k5eAaImIp3nS
více	hodně	k6eAd2
než	než	k8xS
2	#num#	k4
000	#num#	k4
kilometrů	kilometr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největším	veliký	k2eAgInSc7d3
světovým	světový	k2eAgInSc7d1
monolitem	monolit	k1gInSc7
je	být	k5eAaImIp3nS
Mount	Mount	k1gMnSc1
Augustus	Augustus	k1gMnSc1
v	v	k7c6
Západní	západní	k2eAgFnSc6d1
Austrálii	Austrálie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalším	další	k2eAgMnSc7d1
kandidátem	kandidát	k1gMnSc7
na	na	k7c4
tento	tento	k3xDgInSc4
titul	titul	k1gInSc4
je	být	k5eAaImIp3nS
Uluru	Ulura	k1gFnSc4
známý	známý	k1gMnSc1
též	též	k9
jako	jako	k9
Ayerova	Ayerův	k2eAgFnSc1d1
skála	skála	k1gFnSc1
<g/>
,	,	kIx,
ležící	ležící	k2eAgFnSc1d1
v	v	k7c6
centrální	centrální	k2eAgFnSc6d1
části	část	k1gFnSc6
<g/>
.	.	kIx.
2	#num#	k4
228	#num#	k4
metrů	metr	k1gInPc2
vysoký	vysoký	k2eAgInSc4d1
Mount	Mount	k1gInSc4
Kosciuszko	Kosciuszka	k1gFnSc5
ve	v	k7c6
Velkém	velký	k2eAgNnSc6d1
předělovém	předělový	k2eAgNnSc6d1
pohoří	pohoří	k1gNnSc6
je	být	k5eAaImIp3nS
nejvyšší	vysoký	k2eAgFnSc7d3
horou	hora	k1gFnSc7
Austrálie	Austrálie	k1gFnSc2
jako	jako	k8xS,k8xC
kontinentu	kontinent	k1gInSc2
<g/>
,	,	kIx,
ne	ne	k9
však	však	k9
státu	stát	k1gInSc3
<g/>
:	:	kIx,
Mawson	Mawson	k1gMnSc1
Peak	Peak	k1gMnSc1
na	na	k7c6
Heardově	Heardův	k2eAgInSc6d1
ostrově	ostrov	k1gInSc6
jej	on	k3xPp3gMnSc4
převyšuje	převyšovat	k5eAaImIp3nS
o	o	k7c4
517	#num#	k4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Největší	veliký	k2eAgFnSc4d3
plochu	plocha	k1gFnSc4
zabírají	zabírat	k5eAaImIp3nP
v	v	k7c6
Austrálii	Austrálie	k1gFnSc6
pouště	poušť	k1gFnSc2
a	a	k8xC
polopouště	polopoušť	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Austrálie	Austrálie	k1gFnSc1
je	být	k5eAaImIp3nS
nejsušším	suchý	k2eAgInSc7d3
a	a	k8xC
nejplošším	plochý	k2eAgInSc7d3
obydleným	obydlený	k2eAgInSc7d1
kontinentem	kontinent	k1gInSc7
a	a	k8xC
má	mít	k5eAaImIp3nS
nejmenší	malý	k2eAgNnSc4d3
množství	množství	k1gNnSc4
úrodné	úrodný	k2eAgFnSc2d1
půdy	půda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mírné	mírný	k2eAgNnSc4d1
klima	klima	k1gNnSc4
má	mít	k5eAaImIp3nS
pouze	pouze	k6eAd1
jihovýchodní	jihovýchodní	k2eAgFnSc1d1
a	a	k8xC
jihozápadní	jihozápadní	k2eAgFnSc1d1
část	část	k1gFnSc1
kontinentu	kontinent	k1gInSc2
<g/>
,	,	kIx,
především	především	k6eAd1
podél	podél	k7c2
pobřeží	pobřeží	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
severní	severní	k2eAgFnSc6d1
části	část	k1gFnSc6
země	zem	k1gFnSc2
je	být	k5eAaImIp3nS
klima	klima	k1gNnSc4
tropické	tropický	k2eAgFnSc2d1
<g/>
,	,	kIx,
vegetace	vegetace	k1gFnSc2
se	se	k3xPyFc4
zde	zde	k6eAd1
skládá	skládat	k5eAaImIp3nS
z	z	k7c2
deštných	deštný	k2eAgInPc2d1
i	i	k8xC
obyčejných	obyčejný	k2eAgInPc2d1
lesů	les	k1gInPc2
a	a	k8xC
mangrovových	mangrovový	k2eAgFnPc2d1
bažin	bažina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
celek	celek	k1gInSc1
je	být	k5eAaImIp3nS
klima	klima	k1gNnSc1
velmi	velmi	k6eAd1
ovlivněno	ovlivnit	k5eAaPmNgNnS
oceánem	oceán	k1gInSc7
včetně	včetně	k7c2
jevu	jev	k1gInSc2
El	Ela	k1gFnPc2
Niñ	Niñ	k6eAd1
způsobujícího	způsobující	k2eAgInSc2d1
pravidelná	pravidelný	k2eAgFnSc1d1
sucha	sucho	k1gNnSc2
a	a	k8xC
sezónními	sezónní	k2eAgInPc7d1
systémy	systém	k1gInPc7
tlakových	tlakový	k2eAgFnPc2d1
níží	níže	k1gFnPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgInPc2
vznikají	vznikat	k5eAaImIp3nP
cyklóny	cyklóna	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
trpí	trpět	k5eAaImIp3nS
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
východního	východní	k2eAgNnSc2d1
pobřeží	pobřeží	k1gNnSc2
a	a	k8xC
Tasmánie	Tasmánie	k1gFnSc2
nedostatkem	nedostatek	k1gInSc7
srážek	srážka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
tohoto	tento	k3xDgNnSc2
hlediska	hledisko	k1gNnSc2
je	být	k5eAaImIp3nS
na	na	k7c6
tom	ten	k3xDgNnSc6
Austrálie	Austrálie	k1gFnSc1
nejhůře	zle	k6eAd3
ze	z	k7c2
všech	všecek	k3xTgInPc2
kontinentů	kontinent	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc7
převážná	převážný	k2eAgFnSc1d1
část	část	k1gFnSc1
leží	ležet	k5eAaImIp3nS
v	v	k7c6
oblasti	oblast	k1gFnSc6
suchého	suchý	k2eAgNnSc2d1
až	až	k8xS
polosuchého	polosuchý	k2eAgNnSc2d1
tropického	tropický	k2eAgNnSc2d1
a	a	k8xC
subtropického	subtropický	k2eAgNnSc2d1
klimatu	klima	k1gNnSc2
<g/>
,	,	kIx,
proto	proto	k8xC
rozsáhlé	rozsáhlý	k2eAgFnPc4d1
části	část	k1gFnPc4
vnitrozemí	vnitrozemí	k1gNnSc2
a	a	k8xC
dokonce	dokonce	k9
i	i	k9
části	část	k1gFnPc1
západního	západní	k2eAgNnSc2d1
pobřeží	pobřeží	k1gNnSc2
mají	mít	k5eAaImIp3nP
jen	jen	k9
slabě	slabě	k6eAd1
vyvinutou	vyvinutý	k2eAgFnSc4d1
říční	říční	k2eAgFnSc4d1
síť	síť	k1gFnSc4
nebo	nebo	k8xC
vůbec	vůbec	k9
žádnou	žádný	k3yNgFnSc4
<g/>
.	.	kIx.
65	#num#	k4
<g/>
%	%	kIx~
pevniny	pevnina	k1gFnSc2
je	být	k5eAaImIp3nS
bez	bez	k7c2
povrchového	povrchový	k2eAgInSc2d1
odtoku	odtok	k1gInSc2
k	k	k7c3
moři	moře	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pouze	pouze	k6eAd1
v	v	k7c6
horách	hora	k1gFnPc6
v	v	k7c6
okolí	okolí	k1gNnSc6
Cairnsu	Cairns	k1gInSc2
v	v	k7c6
Queenslandu	Queensland	k1gInSc6
<g/>
,	,	kIx,
na	na	k7c6
jihovýchodě	jihovýchod	k1gInSc6
kontinentu	kontinent	k1gInSc2
a	a	k8xC
na	na	k7c6
Tasmánii	Tasmánie	k1gFnSc6
je	být	k5eAaImIp3nS
poměrně	poměrně	k6eAd1
hustá	hustý	k2eAgFnSc1d1
říční	říční	k2eAgFnSc1d1
síť	síť	k1gFnSc1
stálých	stálý	k2eAgInPc2d1
toků	tok	k1gInPc2
<g/>
,	,	kIx,
odpovídající	odpovídající	k2eAgFnSc4d1
poměrům	poměr	k1gInPc3
ve	v	k7c6
středoevropských	středoevropský	k2eAgNnPc6d1
pohořích	pohoří	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s>
Prostředí	prostředí	k1gNnSc1
</s>
<s>
Koala	koala	k1gFnSc1
na	na	k7c6
eukalyptu	eukalypt	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Ačkoliv	ačkoliv	k8xS
má	mít	k5eAaImIp3nS
převážná	převážný	k2eAgFnSc1d1
většina	většina	k1gFnSc1
australského	australský	k2eAgInSc2d1
povrchu	povrch	k1gInSc2
pouštní	pouštní	k2eAgMnSc1d1
nebo	nebo	k8xC
polopouštní	polopouštní	k2eAgInSc1d1
charakter	charakter	k1gInSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
na	na	k7c6
něm	on	k3xPp3gNnSc6
rozvinuto	rozvinout	k5eAaPmNgNnS
mnoho	mnoho	k4c1
různých	různý	k2eAgFnPc2d1
prostředí	prostředí	k1gNnSc4
od	od	k7c2
alpinských	alpinský	k2eAgNnPc2d1
vřesovišť	vřesoviště	k1gNnPc2
po	po	k7c4
tropické	tropický	k2eAgInPc4d1
deštné	deštný	k2eAgInPc4d1
lesy	les	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dlouhé	Dlouhé	k2eAgNnSc1d1
geografické	geografický	k2eAgNnSc1d1
odloučení	odloučení	k1gNnSc1
způsobilo	způsobit	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
mnoho	mnoho	k4c1
australských	australský	k2eAgInPc2d1
organismů	organismus	k1gInPc2
je	být	k5eAaImIp3nS
zcela	zcela	k6eAd1
unikátních	unikátní	k2eAgInPc2d1
<g/>
.	.	kIx.
85	#num#	k4
%	%	kIx~
rostlin	rostlina	k1gFnPc2
<g/>
,	,	kIx,
84	#num#	k4
%	%	kIx~
savců	savec	k1gMnPc2
<g/>
,	,	kIx,
více	hodně	k6eAd2
než	než	k8xS
45	#num#	k4
%	%	kIx~
ptáků	pták	k1gMnPc2
a	a	k8xC
89	#num#	k4
%	%	kIx~
procent	procento	k1gNnPc2
příbřežních	příbřežní	k2eAgFnPc2d1
ryb	ryba	k1gFnPc2
je	být	k5eAaImIp3nS
endemických	endemický	k2eAgFnPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnoho	mnoho	k6eAd1
australských	australský	k2eAgMnPc2d1
ekoregionů	ekoregion	k1gInPc2
a	a	k8xC
jejich	jejich	k3xOp3gInPc2
druhů	druh	k1gInPc2
je	být	k5eAaImIp3nS
zasaženo	zasáhnout	k5eAaPmNgNnS
lidskou	lidský	k2eAgFnSc7d1
aktivitou	aktivita	k1gFnSc7
a	a	k8xC
člověkem	člověk	k1gMnSc7
vysazenými	vysazený	k2eAgNnPc7d1
zvířaty	zvíře	k1gNnPc7
či	či	k8xC
rostlinami	rostlina	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Řada	řada	k1gFnSc1
australských	australský	k2eAgFnPc2d1
dřevin	dřevina	k1gFnPc2
je	být	k5eAaImIp3nS
vždy	vždy	k6eAd1
zelených	zelená	k1gFnPc2
<g/>
,	,	kIx,
mnoho	mnoho	k6eAd1
z	z	k7c2
nich	on	k3xPp3gMnPc2
je	být	k5eAaImIp3nS
přizpůsobeno	přizpůsobit	k5eAaPmNgNnS
vzdorování	vzdorování	k1gNnSc3
ohni	oheň	k1gInSc3
nebo	nebo	k8xC
suchu	sucho	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
tyto	tento	k3xDgFnPc4
rostliny	rostlina	k1gFnPc4
patří	patřit	k5eAaImIp3nP
například	například	k6eAd1
eukalypty	eukalypt	k1gInPc1
nebo	nebo	k8xC
akácie	akácie	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Austrálie	Austrálie	k1gFnSc1
je	být	k5eAaImIp3nS
bohatá	bohatý	k2eAgFnSc1d1
na	na	k7c4
endemické	endemický	k2eAgInPc4d1
druhy	druh	k1gInPc4
luštěnin	luštěnina	k1gFnPc2
<g/>
,	,	kIx,
jimž	jenž	k3xRgMnPc3
se	se	k3xPyFc4
díky	díky	k7c3
jejich	jejich	k3xOp3gFnSc3
symbióze	symbióza	k1gFnSc3
s	s	k7c7
bakteriemi	bakterie	k1gFnPc7
rhizobia	rhizobium	k1gNnSc2
a	a	k8xC
mykorhizujícími	mykorhizující	k2eAgFnPc7d1
houbami	houba	k1gFnPc7
daří	dařit	k5eAaImIp3nS
v	v	k7c6
na	na	k7c4
živiny	živina	k1gFnPc1
chudých	chudý	k2eAgFnPc6d1
zeminách	zemina	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Austrálii	Austrálie	k1gFnSc6
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
odhadem	odhad	k1gInSc7
250	#num#	k4
000	#num#	k4
druhů	druh	k1gInPc2
hub	houba	k1gFnPc2
<g/>
,	,	kIx,
popsáno	popsán	k2eAgNnSc1d1
z	z	k7c2
nich	on	k3xPp3gMnPc2
bylo	být	k5eAaImAgNnS
pouhých	pouhý	k2eAgNnPc2d1
pět	pět	k4xCc1
procent	procento	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nejznámější	známý	k2eAgFnSc1d3
australská	australský	k2eAgFnSc1d1
fauna	fauna	k1gFnSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
ptakořitné	ptakořitný	k2eAgNnSc1d1
(	(	kIx(
<g/>
ptakopyska	ptakopysk	k1gMnSc4
a	a	k8xC
ježury	ježura	k1gFnPc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
velké	velký	k2eAgNnSc1d1
množství	množství	k1gNnSc1
vačnatců	vačnatec	k1gMnPc2
včetně	včetně	k7c2
klokanů	klokan	k1gMnPc2
<g/>
,	,	kIx,
koaly	koala	k1gFnSc2
a	a	k8xC
vombata	vombat	k1gMnSc2
nebo	nebo	k8xC
ptáků	pták	k1gMnPc2
(	(	kIx(
<g/>
například	například	k6eAd1
emu	emu	k1gMnPc1
<g/>
,	,	kIx,
ledňáci	ledňák	k1gMnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dingo	dingo	k1gMnSc1
byl	být	k5eAaImAgMnS
do	do	k7c2
Austrálie	Austrálie	k1gFnSc2
zavlečen	zavlečen	k2eAgInSc4d1
Austronésany	Austronésan	k1gInPc7
obchodujícími	obchodující	k2eAgInPc7d1
s	s	k7c7
původními	původní	k2eAgMnPc7d1
obyvateli	obyvatel	k1gMnPc7
okolo	okolo	k7c2
roku	rok	k1gInSc2
4000	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
Mnoho	mnoho	k4c1
rostlin	rostlina	k1gFnPc2
i	i	k8xC
živočichů	živočich	k1gMnPc2
vyhynulo	vyhynout	k5eAaPmAgNnS
brzy	brzy	k6eAd1
po	po	k7c6
zahájení	zahájení	k1gNnSc6
lidského	lidský	k2eAgNnSc2d1
osídlování	osídlování	k1gNnSc2
(	(	kIx(
<g/>
australská	australský	k2eAgFnSc1d1
megafauna	megafauna	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
mnoho	mnoho	k4c1
dalších	další	k2eAgMnPc2d1
vyhynulo	vyhynout	k5eAaPmAgNnS
po	po	k7c6
příchodu	příchod	k1gInSc6
Evropanů	Evropan	k1gMnPc2
(	(	kIx(
<g/>
například	například	k6eAd1
vakovlk	vakovlk	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navzdory	navzdory	k6eAd1
tomu	ten	k3xDgInSc3
se	se	k3xPyFc4
v	v	k7c6
některých	některý	k3yIgInPc6
segmentech	segment	k1gInPc6
živočišné	živočišný	k2eAgFnSc2d1
říše	říš	k1gFnSc2
zachovala	zachovat	k5eAaPmAgFnS
mimořádná	mimořádný	k2eAgFnSc1d1
rozmanitost	rozmanitost	k1gFnSc1
<g/>
:	:	kIx,
v	v	k7c6
Austrálii	Austrálie	k1gFnSc6
se	se	k3xPyFc4
například	například	k6eAd1
vyskytuje	vyskytovat	k5eAaImIp3nS
nejméně	málo	k6eAd3
755	#num#	k4
druhů	druh	k1gInPc2
plazů	plaz	k1gInPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
více	hodně	k6eAd2
než	než	k8xS
v	v	k7c6
kterékoli	kterýkoli	k3yIgFnSc6
jiné	jiný	k2eAgFnSc6d1
zemi	zem	k1gFnSc6
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
67	#num#	k4
<g/>
]	]	kIx)
Mnozí	mnohý	k2eAgMnPc1d1
z	z	k7c2
australských	australský	k2eAgMnPc2d1
hadů	had	k1gMnPc2
patří	patřit	k5eAaImIp3nS
k	k	k7c3
nejjedovatějším	jedovatý	k2eAgNnPc3d3
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
Antarktidy	Antarktida	k1gFnSc2
je	být	k5eAaImIp3nS
Austrálie	Austrálie	k1gFnSc2
jediným	jediný	k2eAgInSc7d1
kontinentem	kontinent	k1gInSc7
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
nevyvinuly	vyvinout	k5eNaPmAgFnP
kočkovité	kočkovitý	k2eAgFnPc1d1
šelmy	šelma	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Divoké	divoký	k2eAgFnPc1d1
kočky	kočka	k1gFnPc1
sem	sem	k6eAd1
byly	být	k5eAaImAgFnP
zavlečeny	zavlečen	k2eAgInPc1d1
až	až	k9
v	v	k7c4
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
z	z	k7c2
nizozemských	nizozemský	k2eAgInPc2d1
vraků	vrak	k1gInPc2
lodí	loď	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nyní	nyní	k6eAd1
jsou	být	k5eAaImIp3nP
považovány	považován	k2eAgInPc1d1
za	za	k7c4
hlavní	hlavní	k2eAgFnSc4d1
příčinu	příčina	k1gFnSc4
úbytku	úbytek	k1gInSc2
či	či	k8xC
vyhynutí	vyhynutí	k1gNnSc1
mnoha	mnoho	k4c2
zranitelných	zranitelný	k2eAgInPc2d1
a	a	k8xC
ohrožených	ohrožený	k2eAgInPc2d1
původních	původní	k2eAgInPc2d1
druhů	druh	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
68	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
seznamu	seznam	k1gInSc6
ohrožených	ohrožený	k2eAgInPc2d1
druhů	druh	k1gInPc2
je	být	k5eAaImIp3nS
asi	asi	k9
500	#num#	k4
druhů	druh	k1gInPc2
těchto	tento	k3xDgNnPc2
zvířat	zvíře	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Federální	federální	k2eAgInSc1d1
zákon	zákon	k1gInSc1
o	o	k7c6
ochraně	ochrana	k1gFnSc6
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
a	a	k8xC
ochraně	ochrana	k1gFnSc3
biodiverzity	biodiverzita	k1gFnSc2
(	(	kIx(
<g/>
EPBC	EPBC	kA
Act	Act	k1gFnSc2
<g/>
)	)	kIx)
z	z	k7c2
roku	rok	k1gInSc2
1999	#num#	k4
je	být	k5eAaImIp3nS
hlavním	hlavní	k2eAgInSc7d1
právním	právní	k2eAgInSc7d1
rámcem	rámec	k1gInSc7
pro	pro	k7c4
ochranu	ochrana	k1gFnSc4
ohrožených	ohrožený	k2eAgInPc2d1
druhů	druh	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
69	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
rámci	rámec	k1gInSc6
Národní	národní	k2eAgFnSc2d1
strategie	strategie	k1gFnSc2
ochrany	ochrana	k1gFnSc2
biologické	biologický	k2eAgFnSc2d1
rozmanitosti	rozmanitost	k1gFnSc2
Austrálie	Austrálie	k1gFnSc2
byla	být	k5eAaImAgFnS
vytvořena	vytvořit	k5eAaPmNgFnS
řada	řada	k1gFnSc1
chráněných	chráněný	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
za	za	k7c7
účelem	účel	k1gInSc7
ochrany	ochrana	k1gFnSc2
a	a	k8xC
zachování	zachování	k1gNnSc4
jedinečných	jedinečný	k2eAgInPc2d1
ekosystémů	ekosystém	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
Ramsarské	Ramsarský	k2eAgFnSc2d1
úmluvy	úmluva	k1gFnSc2
je	být	k5eAaImIp3nS
registrováno	registrovat	k5eAaBmNgNnS
64	#num#	k4
mokřadů	mokřad	k1gInPc2
<g/>
,	,	kIx,
16	#num#	k4
míst	místo	k1gNnPc2
je	být	k5eAaImIp3nS
registrováno	registrovat	k5eAaBmNgNnS
jako	jako	k8xC,k8xS
Světové	světový	k2eAgNnSc1d1
dědictví	dědictví	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
Environmental	Environmental	k1gMnSc1
Performance	performance	k1gFnSc2
Index	index	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yIgInSc4,k3yQgInSc4
sestavuje	sestavovat	k5eAaImIp3nS
Yaleova	Yaleův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
Austrálie	Austrálie	k1gFnSc1
třináctou	třináctý	k4xOgFnSc4
nejlepší	dobrý	k2eAgFnSc4d3
ochranu	ochrana	k1gFnSc4
přírody	příroda	k1gFnSc2
na	na	k7c6
světě	svět	k1gInSc6
(	(	kIx(
<g/>
k	k	k7c3
roku	rok	k1gInSc3
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
70	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Politický	politický	k2eAgInSc1d1
systém	systém	k1gInSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Politický	politický	k2eAgInSc1d1
systém	systém	k1gInSc1
Austrálie	Austrálie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Budova	budova	k1gFnSc1
parlamentu	parlament	k1gInSc2
v	v	k7c6
Canbeře	Canbera	k1gFnSc6
otevřená	otevřený	k2eAgFnSc1d1
v	v	k7c6
roce	rok	k1gInSc6
1988	#num#	k4
nahradila	nahradit	k5eAaPmAgFnS
provizorní	provizorní	k2eAgFnSc4d1
budovu	budova	k1gFnSc4
z	z	k7c2
roku	rok	k1gInSc2
1927	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Australský	australský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
je	být	k5eAaImIp3nS
federální	federální	k2eAgFnSc7d1
konstituční	konstituční	k2eAgFnSc7d1
monarchií	monarchie	k1gFnSc7
s	s	k7c7
parlamentním	parlamentní	k2eAgInSc7d1
systémem	systém	k1gInSc7
vlády	vláda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Královnou	královna	k1gFnSc7
Austrálie	Austrálie	k1gFnSc2
je	být	k5eAaImIp3nS
Alžběta	Alžběta	k1gFnSc1
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc1
role	role	k1gFnSc1
je	být	k5eAaImIp3nS
podobná	podobný	k2eAgFnSc1d1
jako	jako	k9
v	v	k7c6
Kanadě	Kanada	k1gFnSc6
a	a	k8xC
v	v	k7c6
dalších	další	k1gNnPc6
v	v	k7c6
zemích	zem	k1gFnPc6
Commonwealthu	Commonwealth	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zastupovaná	zastupovaný	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
generálním	generální	k2eAgMnSc7d1
guvernérem	guvernér	k1gMnSc7
na	na	k7c6
federální	federální	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
i	i	k9
guvernéry	guvernér	k1gMnPc4
na	na	k7c6
úrovni	úroveň	k1gFnSc6
jednotlivých	jednotlivý	k2eAgInPc2d1
států	stát	k1gInPc2
federace	federace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoliv	ačkoliv	k8xS
dává	dávat	k5eAaImIp3nS
ústava	ústava	k1gFnSc1
generálnímu	generální	k2eAgMnSc3d1
guvernérovi	guvernér	k1gMnSc3
výkonnou	výkonný	k2eAgFnSc4d1
moc	moc	k1gFnSc4
<g/>
,	,	kIx,
za	za	k7c2
běžných	běžný	k2eAgFnPc2d1
okolností	okolnost	k1gFnPc2
ji	on	k3xPp3gFnSc4
aplikuje	aplikovat	k5eAaBmIp3nS
pouze	pouze	k6eAd1
na	na	k7c4
žádost	žádost	k1gFnSc4
premiéra	premiér	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejím	její	k3xOp3gNnSc7
nejvýznamnějším	významný	k2eAgNnSc7d3
uplatněním	uplatnění	k1gNnSc7
z	z	k7c2
vlastní	vlastní	k2eAgFnSc2d1
iniciativy	iniciativa	k1gFnSc2
bylo	být	k5eAaImAgNnS
rozpuštění	rozpuštění	k1gNnSc1
Whitlamovy	Whitlamův	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
během	během	k7c2
ústavní	ústavní	k2eAgFnSc2d1
krize	krize	k1gFnSc2
roku	rok	k1gInSc2
1975	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Státní	státní	k2eAgFnSc1d1
moc	moc	k1gFnSc1
je	být	k5eAaImIp3nS
rozdělena	rozdělit	k5eAaPmNgFnS
na	na	k7c6
<g/>
:	:	kIx,
</s>
<s>
moc	moc	k6eAd1
zákonodárnou	zákonodárný	k2eAgFnSc4d1
–	–	k?
reprezentuje	reprezentovat	k5eAaImIp3nS
ji	on	k3xPp3gFnSc4
Australský	australský	k2eAgInSc1d1
parlament	parlament	k1gInSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc7
součástí	součást	k1gFnSc7
je	být	k5eAaImIp3nS
Senát	senát	k1gInSc1
<g/>
,	,	kIx,
Sněmovna	sněmovna	k1gFnSc1
reprezentantů	reprezentant	k1gMnPc2
a	a	k8xC
královna	královna	k1gFnSc1
(	(	kIx(
<g/>
zastoupená	zastoupený	k2eAgFnSc1d1
generálním	generální	k2eAgMnSc7d1
guvernérem	guvernér	k1gMnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc1
moc	moc	k1gFnSc1
je	být	k5eAaImIp3nS
omezena	omezit	k5eAaPmNgFnS
na	na	k7c4
souhlas	souhlas	k1gInSc4
či	či	k8xC
nesouhlas	nesouhlas	k1gInSc4
se	s	k7c7
schvalovanými	schvalovaný	k2eAgInPc7d1
zákony	zákon	k1gInPc7
</s>
<s>
moc	moc	k6eAd1
výkonnou	výkonný	k2eAgFnSc4d1
–	–	k?
reprezentuje	reprezentovat	k5eAaImIp3nS
ji	on	k3xPp3gFnSc4
Federální	federální	k2eAgFnSc1d1
výkonná	výkonný	k2eAgFnSc1d1
rada	rada	k1gFnSc1
(	(	kIx(
<g/>
Federal	Federal	k1gFnSc1
Executive	Executiv	k1gInSc5
Council	Councila	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
jejímž	jejíž	k3xOyRp3gNnSc6
čele	čelo	k1gNnSc6
stojí	stát	k5eAaImIp3nS
generální	generální	k2eAgMnSc1d1
guvernér	guvernér	k1gMnSc1
a	a	k8xC
jejímiž	jejíž	k3xOyRp3gMnPc7
členy	člen	k1gMnPc7
jsou	být	k5eAaImIp3nP
v	v	k7c6
podstatě	podstata	k1gFnSc6
především	především	k9
jen	jen	k9
premiér	premiér	k1gMnSc1
a	a	k8xC
ministři	ministr	k1gMnPc1
</s>
<s>
moc	moc	k6eAd1
soudní	soudní	k2eAgInSc1d1
–	–	k?
reprezentuje	reprezentovat	k5eAaImIp3nS
ji	on	k3xPp3gFnSc4
Nejvyšší	vysoký	k2eAgInSc1d3
soud	soud	k1gInSc1
Austrálie	Austrálie	k1gFnSc2
a	a	k8xC
ostatní	ostatní	k2eAgInPc1d1
níže	nízce	k6eAd2
postavené	postavený	k2eAgInPc1d1
soudy	soud	k1gInPc1
<g/>
;	;	kIx,
na	na	k7c6
orgánech	orgán	k1gInPc6
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
jsou	být	k5eAaImIp3nP
formálně	formálně	k6eAd1
nezávislé	závislý	k2eNgFnPc1d1
od	od	k7c2
roku	rok	k1gInSc2
1986	#num#	k4
</s>
<s>
Dvoukomorový	dvoukomorový	k2eAgInSc1d1
svazový	svazový	k2eAgInSc1d1
parlament	parlament	k1gInSc1
je	být	k5eAaImIp3nS
složen	složit	k5eAaPmNgInS
ze	z	k7c2
Senátu	senát	k1gInSc2
(	(	kIx(
<g/>
horní	horní	k2eAgFnPc1d1
komory	komora	k1gFnPc1
<g/>
)	)	kIx)
se	s	k7c7
76	#num#	k4
senátory	senátor	k1gMnPc7
<g/>
,	,	kIx,
Sněmovny	sněmovna	k1gFnPc1
reprezentantů	reprezentant	k1gMnPc2
(	(	kIx(
<g/>
dolní	dolní	k2eAgFnSc2d1
komory	komora	k1gFnSc2
<g/>
)	)	kIx)
se	s	k7c7
150	#num#	k4
členy	člen	k1gMnPc7
a	a	k8xC
jeho	jeho	k3xOp3gFnSc7
součástí	součást	k1gFnSc7
je	být	k5eAaImIp3nS
i	i	k9
královna	královna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počet	počet	k1gInSc1
křesel	křeslo	k1gNnPc2
v	v	k7c6
dolní	dolní	k2eAgFnSc6d1
komoře	komora	k1gFnSc6
je	být	k5eAaImIp3nS
určen	určit	k5eAaPmNgInS
podle	podle	k7c2
počtu	počet	k1gInSc2
obyvatel	obyvatel	k1gMnPc2
jednotlivých	jednotlivý	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
,	,	kIx,
v	v	k7c6
Senátu	senát	k1gInSc6
má	mít	k5eAaImIp3nS
každý	každý	k3xTgInSc4
stát	stát	k1gInSc4
dvanáct	dvanáct	k4xCc1
zástupců	zástupce	k1gMnPc2
<g/>
,	,	kIx,
teritoria	teritorium	k1gNnPc1
po	po	k7c6
dvou	dva	k4xCgInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Volby	volba	k1gFnSc2
do	do	k7c2
obou	dva	k4xCgFnPc2
komor	komora	k1gFnPc2
se	se	k3xPyFc4
konají	konat	k5eAaImIp3nP
každé	každý	k3xTgInPc1
tři	tři	k4xCgInPc1
roky	rok	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mění	měnit	k5eAaImIp3nS
se	se	k3xPyFc4
vždy	vždy	k6eAd1
jen	jen	k9
polovina	polovina	k1gFnSc1
Senátu	senát	k1gInSc2
<g/>
,	,	kIx,
protože	protože	k8xS
funkční	funkční	k2eAgNnSc1d1
období	období	k1gNnSc1
senátora	senátor	k1gMnSc2
je	být	k5eAaImIp3nS
šest	šest	k4xCc4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Strana	strana	k1gFnSc1
s	s	k7c7
nejvyšší	vysoký	k2eAgFnSc7d3
podporou	podpora	k1gFnSc7
ve	v	k7c6
Sněmovně	sněmovna	k1gFnSc6
reprezentantů	reprezentant	k1gMnPc2
vytváří	vytvářet	k5eAaImIp3nS,k5eAaPmIp3nS
vládu	vláda	k1gFnSc4
vedenou	vedený	k2eAgFnSc4d1
premiérem	premiér	k1gMnSc7
(	(	kIx(
<g/>
„	„	k?
<g/>
prvním	první	k4xOgMnSc6
ministrem	ministr	k1gMnSc7
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Volby	volba	k1gFnPc1
jsou	být	k5eAaImIp3nP
povinné	povinný	k2eAgFnPc1d1
pro	pro	k7c4
všechny	všechen	k3xTgMnPc4
občany	občan	k1gMnPc4
starší	starý	k2eAgMnPc1d2
18	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
zemi	zem	k1gFnSc6
působí	působit	k5eAaImIp3nP
tři	tři	k4xCgFnPc1
hlavní	hlavní	k2eAgFnPc1d1
politické	politický	k2eAgFnPc1d1
strany	strana	k1gFnPc1
<g/>
:	:	kIx,
Australská	australský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
práce	práce	k1gFnSc1
<g/>
,	,	kIx,
Liberální	liberální	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Austrálie	Austrálie	k1gFnSc2
a	a	k8xC
Australská	australský	k2eAgFnSc1d1
národní	národní	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Své	svůj	k3xOyFgInPc4
zastoupení	zastoupení	k1gNnSc4
v	v	k7c6
parlamentu	parlament	k1gInSc6
<g/>
,	,	kIx,
především	především	k6eAd1
v	v	k7c6
horní	horní	k2eAgFnSc6d1
komoře	komora	k1gFnSc6
<g/>
,	,	kIx,
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
získat	získat	k5eAaPmF
i	i	k8xC
několika	několik	k4yIc3
nezávislým	závislý	k2eNgFnPc3d1
a	a	k8xC
menším	malý	k2eAgFnPc3d2
stranám	strana	k1gFnPc3
<g/>
,	,	kIx,
jakými	jaký	k3yQgInPc7,k3yRgInPc7,k3yIgInPc7
jsou	být	k5eAaImIp3nP
například	například	k6eAd1
Strana	strana	k1gFnSc1
zelených	zelená	k1gFnPc2
nebo	nebo	k8xC
Australští	australský	k2eAgMnPc1d1
demokraté	demokrat	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
voleb	volba	k1gFnPc2
roce	rok	k1gInSc6
1996	#num#	k4
vládla	vládnout	k5eAaImAgFnS
koalice	koalice	k1gFnSc1
národní	národní	k2eAgFnSc2d1
a	a	k8xC
liberální	liberální	k2eAgFnSc2d1
strany	strana	k1gFnSc2
v	v	k7c6
čele	čelo	k1gNnSc6
s	s	k7c7
Johnem	John	k1gMnSc7
Howardem	Howard	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
volbách	volba	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
koalice	koalice	k1gFnSc1
ovládla	ovládnout	k5eAaPmAgFnS
i	i	k9
Senát	senát	k1gInSc4
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
prvním	první	k4xOgNnSc7
uskupením	uskupení	k1gNnSc7
<g/>
,	,	kIx,
jemuž	jenž	k3xRgMnSc3
se	se	k3xPyFc4
to	ten	k3xDgNnSc1
podařilo	podařit	k5eAaPmAgNnS
během	během	k7c2
mandátu	mandát	k1gInSc2
jeho	jeho	k3xOp3gFnSc2
vlády	vláda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Strana	strana	k1gFnSc1
práce	práce	k1gFnSc1
však	však	k9
ovládala	ovládat	k5eAaImAgFnS
státy	stát	k1gInPc4
a	a	k8xC
teritoria	teritorium	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s>
Zapojení	zapojení	k1gNnSc1
australských	australský	k2eAgMnPc2d1
vojáků	voják	k1gMnPc2
do	do	k7c2
války	válka	k1gFnSc2
v	v	k7c6
Iráku	Irák	k1gInSc6
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
zvítězila	zvítězit	k5eAaPmAgFnS
ve	v	k7c6
federálních	federální	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
Australská	australský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
práce	práce	k1gFnSc2
a	a	k8xC
ministerským	ministerský	k2eAgMnSc7d1
předsedou	předseda	k1gMnSc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
lídr	lídr	k1gMnSc1
strany	strana	k1gFnSc2
Kevin	Kevin	k1gMnSc1
Rudd	Rudd	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
byl	být	k5eAaImAgInS
však	však	k9
na	na	k7c6
vnitrostranické	vnitrostranický	k2eAgFnSc6d1
konferenci	konference	k1gFnSc6
v	v	k7c6
červnu	červen	k1gInSc6
2010	#num#	k4
ve	v	k7c6
vedení	vedení	k1gNnSc6
strany	strana	k1gFnSc2
vystřídán	vystřídán	k2eAgInSc4d1
Julií	Julie	k1gFnSc7
Gillardovou	Gillardův	k2eAgFnSc7d1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
jej	on	k3xPp3gMnSc4
současně	současně	k6eAd1
nahradila	nahradit	k5eAaPmAgFnS
i	i	k9
ve	v	k7c6
vedení	vedení	k1gNnSc6
vlády	vláda	k1gFnSc2
a	a	k8xC
stala	stát	k5eAaPmAgFnS
se	se	k3xPyFc4
tak	tak	k8xC,k8xS
první	první	k4xOgFnSc7
premiérkou	premiérka	k1gFnSc7
v	v	k7c6
historii	historie	k1gFnSc6
země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kevin	Kevin	k1gMnSc1
Rudd	Rudd	k1gMnSc1
se	s	k7c7
24	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
nakrátko	nakrátko	k6eAd1
opět	opět	k6eAd1
stal	stát	k5eAaPmAgMnS
premiérem	premiér	k1gMnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
18	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2013	#num#	k4
jej	on	k3xPp3gMnSc4
ve	v	k7c6
funkci	funkce	k1gFnSc6
vystřídal	vystřídat	k5eAaPmAgMnS
Tony	Tony	k1gMnSc1
Abbott	Abbott	k1gMnSc1
<g/>
,	,	kIx,
když	když	k8xS
Liberální	liberální	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Austrálie	Austrálie	k1gFnSc2
zvítězila	zvítězit	k5eAaPmAgFnS
v	v	k7c6
zářijových	zářijový	k2eAgFnPc6d1
parlamentních	parlamentní	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
71	#num#	k4
<g/>
]	]	kIx)
15	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2015	#num#	k4
jej	on	k3xPp3gNnSc4
nahradil	nahradit	k5eAaPmAgMnS
vnitrostranický	vnitrostranický	k2eAgMnSc1d1
rival	rival	k1gMnSc1
Malcolm	Malcolm	k1gMnSc1
Turnbull	Turnbull	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yRnSc1,k3yInSc1
ztratil	ztratit	k5eAaPmAgMnS
důvěru	důvěra	k1gFnSc4
vlastní	vlastní	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
nahradil	nahradit	k5eAaPmAgMnS
ho	on	k3xPp3gNnSc4
v	v	k7c6
jejím	její	k3xOp3gNnSc6
čele	čelo	k1gNnSc6
i	i	k9
v	v	k7c6
čele	čelo	k1gNnSc6
vlády	vláda	k1gFnSc2
roku	rok	k1gInSc2
2018	#num#	k4
Scott	Scott	k1gMnSc1
Morrison	Morrison	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
72	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Administrativní	administrativní	k2eAgNnSc1d1
uspořádání	uspořádání	k1gNnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Australské	australský	k2eAgInPc4d1
státy	stát	k1gInPc4
a	a	k8xC
teritoria	teritorium	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s>
území	území	k1gNnSc1
</s>
<s>
hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
</s>
<s>
6	#num#	k4
spolkových	spolkový	k2eAgMnPc2d1
států	stát	k1gInPc2
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
AustrálieAdelaide	AustrálieAdelaid	k1gInSc5
</s>
<s>
Nový	Nový	k1gMnSc1
Jižní	jižní	k2eAgFnSc2d1
WalesSydney	WalesSydnea	k1gFnSc2
</s>
<s>
QueenslandBrisbane	QueenslandBrisbanout	k5eAaPmIp3nS
</s>
<s>
TasmánieHobart	TasmánieHobart	k1gInSc1
</s>
<s>
VictoriaMelbourne	VictoriaMelbournout	k5eAaPmIp3nS
</s>
<s>
Západní	západní	k2eAgInSc1d1
AustráliePerth	AustráliePerth	k1gInSc1
</s>
<s>
3	#num#	k4
federální	federální	k2eAgInSc1d1
teritoria	teritorium	k1gNnPc4
</s>
<s>
Severní	severní	k2eAgInSc1d1
teritoriumDarwin	teritoriumDarwin	k1gInSc1
</s>
<s>
Teritorium	teritorium	k1gNnSc1
hlavního	hlavní	k2eAgNnSc2d1
městaCanberra	městaCanberro	k1gNnSc2
</s>
<s>
Teritorium	teritorium	k1gNnSc4
Jervisova	Jervisův	k2eAgFnSc1d1
zátoka	zátoka	k1gFnSc1
</s>
<s>
6	#num#	k4
zámořských	zámořský	k2eAgNnPc2d1
území	území	k1gNnPc2
</s>
<s>
Ashmorův	Ashmorův	k2eAgInSc4d1
a	a	k8xC
Cartierův	Cartierův	k2eAgInSc4d1
ostrov	ostrov	k1gInSc4
<g/>
(	(	kIx(
<g/>
neobydlené	obydlený	k2eNgInPc4d1
<g/>
)	)	kIx)
</s>
<s>
Kokosové	kokosový	k2eAgNnSc1d1
ostrovyBantam	ostrovyBantam	k6eAd1
</s>
<s>
Heardův	Heardův	k2eAgMnSc1d1
a	a	k8xC
MacDonaldovy	Macdonaldův	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
<g/>
(	(	kIx(
<g/>
jen	jen	k9
výzkumná	výzkumný	k2eAgFnSc1d1
stanice	stanice	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Ostrovy	ostrov	k1gInPc1
Korálového	korálový	k2eAgNnSc2d1
moře	moře	k1gNnSc2
<g/>
(	(	kIx(
<g/>
trvale	trvale	k6eAd1
neobydlené	obydlený	k2eNgNnSc1d1
<g/>
)	)	kIx)
</s>
<s>
NorfolkKingston	NorfolkKingston	k1gInSc1
</s>
<s>
Vánoční	vánoční	k2eAgInSc1d1
ostrovFlying	ostrovFlying	k1gInSc1
Fish	Fisha	k1gFnPc2
Cove	Cov	k1gInSc2
</s>
<s>
1	#num#	k4
dependence	dependence	k1gFnSc1
Tasmánie	Tasmánie	k1gFnSc1
</s>
<s>
Macquarieovy	Macquarieův	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
<g/>
(	(	kIx(
<g/>
meteorologická	meteorologický	k2eAgFnSc1d1
stanice	stanice	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
sestává	sestávat	k5eAaImIp3nS
z	z	k7c2
šesti	šest	k4xCc2
států	stát	k1gInPc2
a	a	k8xC
dvou	dva	k4xCgInPc2
pevninských	pevninský	k2eAgInPc2d1
a	a	k8xC
několika	několik	k4yIc2
malých	malý	k2eAgNnPc2d1
ostrovních	ostrovní	k2eAgNnPc2d1
teritorií	teritorium	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Státy	stát	k1gInPc7
jsou	být	k5eAaImIp3nP
Nový	nový	k2eAgInSc1d1
Jižní	jižní	k2eAgInSc1d1
Wales	Wales	k1gInSc1
<g/>
,	,	kIx,
Queensland	Queensland	k1gInSc1
<g/>
,	,	kIx,
Jižní	jižní	k2eAgFnSc2d1
Austrálie	Austrálie	k1gFnSc2
<g/>
,	,	kIx,
Tasmánie	Tasmánie	k1gFnSc2
<g/>
,	,	kIx,
Victoria	Victorium	k1gNnSc2
a	a	k8xC
Západní	západní	k2eAgFnSc2d1
Austrálie	Austrálie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvěma	dva	k4xCgInPc7
velkými	velký	k2eAgInPc7d1
pevninskými	pevninský	k2eAgInPc7d1
teritorii	teritorium	k1gNnPc7
pak	pak	k6eAd1
Severní	severní	k2eAgNnSc4d1
teritorium	teritorium	k1gNnSc4
a	a	k8xC
Teritorium	teritorium	k1gNnSc4
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teritoria	teritorium	k1gNnPc1
v	v	k7c6
mnoha	mnoho	k4c6
ohledech	ohled	k1gInPc6
fungují	fungovat	k5eAaImIp3nP
podobně	podobně	k6eAd1
jako	jako	k9
státy	stát	k1gInPc1
<g/>
,	,	kIx,
svazový	svazový	k2eAgInSc1d1
parlament	parlament	k1gInSc1
však	však	k9
může	moct	k5eAaImIp3nS
veškerou	veškerý	k3xTgFnSc4
jejich	jejich	k3xOp3gFnSc4
legislativu	legislativa	k1gFnSc4
upravit	upravit	k5eAaPmF
<g/>
,	,	kIx,
změnit	změnit	k5eAaPmF
nebo	nebo	k8xC
zrušit	zrušit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
druhou	druhý	k4xOgFnSc4
stranu	strana	k1gFnSc4
je	být	k5eAaImIp3nS
federální	federální	k2eAgFnSc1d1
legislativa	legislativa	k1gFnSc1
nadřazena	nadřazen	k2eAgFnSc1d1
kromě	kromě	k7c2
některých	některý	k3yIgFnPc2
oblastí	oblast	k1gFnPc2
vyjmenovaných	vyjmenovaný	k2eAgFnPc2d1
v	v	k7c6
ústavě	ústava	k1gFnSc6
i	i	k8xC
legislativám	legislativa	k1gFnPc3
jednotlivých	jednotlivý	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbylé	zbylý	k2eAgFnPc4d1
oblasti	oblast	k1gFnPc4
jsou	být	k5eAaImIp3nP
pak	pak	k6eAd1
pod	pod	k7c7
legislativní	legislativní	k2eAgFnSc7d1
pravomocí	pravomoc	k1gFnSc7
státních	státní	k2eAgInPc2d1
parlamentů	parlament	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zahrnují	zahrnovat	k5eAaImIp3nP
správu	správa	k1gFnSc4
zdravotnictví	zdravotnictví	k1gNnSc2
<g/>
,	,	kIx,
vzdělání	vzdělání	k1gNnSc2
<g/>
,	,	kIx,
policie	policie	k1gFnSc2
<g/>
,	,	kIx,
soudnictví	soudnictví	k1gNnSc2
<g/>
,	,	kIx,
přepravy	přeprava	k1gFnSc2
a	a	k8xC
místní	místní	k2eAgFnSc2d1
správy	správa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Každý	každý	k3xTgInSc1
stát	stát	k1gInSc1
i	i	k8xC
teritorium	teritorium	k1gNnSc1
má	mít	k5eAaImIp3nS
svůj	svůj	k3xOyFgInSc4
vlastní	vlastní	k2eAgInSc4d1
legislativní	legislativní	k2eAgInSc4d1
orgán	orgán	k1gInSc4
<g/>
,	,	kIx,
jednokomorový	jednokomorový	k2eAgInSc4d1
v	v	k7c6
případě	případ	k1gInSc6
Severního	severní	k2eAgNnSc2d1
teritoria	teritorium	k1gNnSc2
<g/>
,	,	kIx,
Teritoria	teritorium	k1gNnSc2
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
a	a	k8xC
Queenslandu	Queensland	k1gInSc2
<g/>
,	,	kIx,
dvoukomorový	dvoukomorový	k2eAgInSc1d1
v	v	k7c6
ostatních	ostatní	k2eAgInPc6d1
státech	stát	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dolní	dolní	k2eAgFnPc1d1
komory	komora	k1gFnPc1
jsou	být	k5eAaImIp3nP
známé	známý	k2eAgFnPc1d1
jako	jako	k8xC,k8xS
Zákonodárná	zákonodárný	k2eAgNnPc1d1
shromáždění	shromáždění	k1gNnPc1
<g/>
,	,	kIx,
horní	horní	k2eAgFnPc1d1
komory	komora	k1gFnPc1
pak	pak	k6eAd1
jako	jako	k9
Zákonodárné	zákonodárný	k2eAgFnSc2d1
rady	rada	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavami	hlava	k1gFnPc7
států	stát	k1gInPc2
i	i	k8xC
teritorií	teritorium	k1gNnPc2
jsou	být	k5eAaImIp3nP
premiéři	premiér	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Královna	královna	k1gFnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
každém	každý	k3xTgInSc6
státě	stát	k1gInSc6
zastoupena	zastoupen	k2eAgFnSc1d1
guvernérem	guvernér	k1gMnSc7
<g/>
,	,	kIx,
v	v	k7c6
administrativě	administrativa	k1gFnSc6
Severního	severní	k2eAgNnSc2d1
teritoria	teritorium	k1gNnSc2
administrátorem	administrátor	k1gMnSc7
a	a	k8xC
generálním	generální	k2eAgMnSc7d1
guvernérem	guvernér	k1gMnSc7
v	v	k7c6
Teritoriu	teritorium	k1gNnSc6
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
také	také	k9
několik	několik	k4yIc4
malých	malý	k2eAgNnPc2d1
teritorií	teritorium	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Federální	federální	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
tak	tak	k6eAd1
například	například	k6eAd1
spravuje	spravovat	k5eAaImIp3nS
Teritorium	teritorium	k1gNnSc1
Jervis	Jervis	k1gFnSc2
Bay	Bay	k1gFnSc2
v	v	k7c6
Novém	nový	k2eAgInSc6d1
Jižním	jižní	k2eAgInSc6d1
Walesu	Wales	k1gInSc6
sloužící	sloužící	k2eAgMnSc1d1
jako	jako	k8xC,k8xS
námořní	námořní	k2eAgFnSc1d1
základna	základna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Součástí	součást	k1gFnSc7
země	zem	k1gFnSc2
jsou	být	k5eAaImIp3nP
i	i	k9
tzv.	tzv.	kA
vnější	vnější	k2eAgNnPc1d1
obydlená	obydlený	k2eAgNnPc1d1
teritoria	teritorium	k1gNnPc1
<g/>
:	:	kIx,
Norfolk	Norfolk	k1gInSc1
<g/>
,	,	kIx,
Vánoční	vánoční	k2eAgInSc1d1
ostrov	ostrov	k1gInSc1
<g/>
,	,	kIx,
Kokosové	kokosový	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
a	a	k8xC
několik	několik	k4yIc1
neobydlených	obydlený	k2eNgFnPc2d1
<g/>
:	:	kIx,
Ashmorův	Ashmorův	k2eAgInSc1d1
a	a	k8xC
Cartierův	Cartierův	k2eAgInSc1d1
ostrov	ostrov	k1gInSc1
<g/>
,	,	kIx,
Ostrovy	ostrov	k1gInPc1
Korálového	korálový	k2eAgNnSc2d1
moře	moře	k1gNnSc2
<g/>
,	,	kIx,
Heardův	Heardův	k2eAgMnSc1d1
a	a	k8xC
MacDonaldovy	Macdonaldův	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
a	a	k8xC
Australské	australský	k2eAgNnSc4d1
antarktické	antarktický	k2eAgNnSc4d1
teritorium	teritorium	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Politické	politický	k2eAgNnSc1d1
uspořádání	uspořádání	k1gNnSc1
Austrálie	Austrálie	k1gFnSc2
</s>
<s>
Politická	politický	k2eAgFnSc1d1
mapa	mapa	k1gFnSc1
Austrálie	Austrálie	k1gFnSc2
prezentující	prezentující	k2eAgInSc1d1
vývoj	vývoj	k1gInSc1
administrativního	administrativní	k2eAgNnSc2d1
členění	členění	k1gNnSc2
</s>
<s>
Ekonomika	ekonomika	k1gFnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Ekonomika	ekonomika	k1gFnSc1
Austrálie	Austrálie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Australský	australský	k2eAgInSc1d1
vývoz	vývoz	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
<g/>
[	[	kIx(
<g/>
73	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Boddington	Boddington	k1gInSc1
<g/>
,	,	kIx,
největší	veliký	k2eAgInSc1d3
zlatý	zlatý	k2eAgInSc4d1
důl	důl	k1gInSc4
v	v	k7c6
Austrálii	Austrálie	k1gFnSc6
</s>
<s>
Tradiční	tradiční	k2eAgInSc1d1
chov	chov	k1gInSc1
ovcí	ovce	k1gFnPc2
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
patří	patřit	k5eAaImIp3nS
ke	k	k7c3
světovým	světový	k2eAgFnPc3d1
ekonomickým	ekonomický	k2eAgFnPc3d1
velmocem	velmoc	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gInPc4
ekonomika	ekonomika	k1gFnSc1
byla	být	k5eAaImAgFnS
k	k	k7c3
roku	rok	k1gInSc3
2020	#num#	k4
třináctou	třináctý	k4xOgFnSc4
největší	veliký	k2eAgFnSc4d3
na	na	k7c6
světě	svět	k1gInSc6
(	(	kIx(
<g/>
dle	dle	k7c2
nominálního	nominální	k2eAgInSc2d1
HDP	HDP	kA
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
74	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
sedmnáctou	sedmnáctý	k4xOgFnSc4
největší	veliký	k2eAgFnSc4d3
v	v	k7c6
HDP	HDP	kA
v	v	k7c6
paritě	parita	k1gFnSc6
kupní	kupní	k2eAgFnSc2d1
síly	síla	k1gFnSc2
na	na	k7c4
hlavu	hlava	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
75	#num#	k4
<g/>
]	]	kIx)
Australský	australský	k2eAgInSc1d1
HDP	HDP	kA
tvoří	tvořit	k5eAaImIp3nS
1,2	1,2	k4
%	%	kIx~
světové	světový	k2eAgFnSc2d1
ekonomiky	ekonomika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Australská	australský	k2eAgFnSc1d1
ekonomika	ekonomika	k1gFnSc1
má	mít	k5eAaImIp3nS
již	již	k6eAd1
postindustriální	postindustriální	k2eAgInSc4d1
charakter	charakter	k1gInSc4
<g/>
,	,	kIx,
takže	takže	k8xS
převažuje	převažovat	k5eAaImIp3nS
terciární	terciární	k2eAgInSc4d1
sektor	sektor	k1gInSc4
<g/>
,	,	kIx,
tedy	tedy	k8xC
sektor	sektor	k1gInSc1
služeb	služba	k1gFnPc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
vytváří	vytvářet	k5eAaImIp3nS,k5eAaPmIp3nS
65	#num#	k4
%	%	kIx~
HDP	HDP	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tradiční	tradiční	k2eAgInSc1d1
těžební	těžební	k2eAgInSc1d1
průmysl	průmysl	k1gInSc1
má	mít	k5eAaImIp3nS
také	také	k9
stále	stále	k6eAd1
ještě	ještě	k9
velkou	velký	k2eAgFnSc4d1
váhu	váha	k1gFnSc4
<g/>
,	,	kIx,
tvoří	tvořit	k5eAaImIp3nS
13,5	13,5	k4
%	%	kIx~
HDP	HDP	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zemědělství	zemědělství	k1gNnSc1
již	již	k6eAd1
jen	jen	k9
2	#num#	k4
%	%	kIx~
HDP	HDP	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
76	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
má	mít	k5eAaImIp3nS
bohaté	bohatý	k2eAgFnPc4d1
zásoby	zásoba	k1gFnPc4
a	a	k8xC
významnou	významný	k2eAgFnSc4d1
těžbu	těžba	k1gFnSc4
nerostných	nerostný	k2eAgFnPc2d1
surovin	surovina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těží	těžet	k5eAaImIp3nS
se	se	k3xPyFc4
kvalitní	kvalitní	k2eAgNnSc1d1
černé	černé	k1gNnSc1
uhlí	uhlí	k1gNnSc2
v	v	k7c6
Novém	nový	k2eAgInSc6d1
Jižním	jižní	k2eAgInSc6d1
Walesu	Wales	k1gInSc6
a	a	k8xC
Queenslandu	Queensland	k1gInSc6
<g/>
,	,	kIx,
hnědé	hnědý	k2eAgNnSc4d1
uhlí	uhlí	k1gNnSc4
ve	v	k7c6
Victorii	Victorie	k1gFnSc6
<g/>
,	,	kIx,
železná	železný	k2eAgFnSc1d1
ruda	ruda	k1gFnSc1
v	v	k7c6
Západní	západní	k2eAgFnSc6d1
Austrálii	Austrálie	k1gFnSc6
<g/>
,	,	kIx,
dále	daleko	k6eAd2
bauxit	bauxit	k1gInSc1
<g/>
,	,	kIx,
ropa	ropa	k1gFnSc1
<g/>
,	,	kIx,
zemní	zemní	k2eAgInSc1d1
plyn	plyn	k1gInSc1
<g/>
,	,	kIx,
olovo	olovo	k1gNnSc1
<g/>
,	,	kIx,
zinek	zinek	k1gInSc1
<g/>
,	,	kIx,
cín	cín	k1gInSc1
<g/>
,	,	kIx,
měď	měď	k1gFnSc1
<g/>
,	,	kIx,
mangan	mangan	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
zlato	zlato	k1gNnSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
na	na	k7c6
světě	svět	k1gInSc6
<g/>
,	,	kIx,
10,1	10,1	k4
%	%	kIx~
světové	světový	k2eAgFnSc2d1
produkce	produkce	k1gFnSc2
<g/>
[	[	kIx(
<g/>
77	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
,	,	kIx,
stříbro	stříbro	k1gNnSc1
<g/>
,	,	kIx,
diamanty	diamant	k1gInPc1
<g/>
,	,	kIx,
nikl	nikl	k1gInSc1
<g/>
,	,	kIx,
platina	platina	k1gFnSc1
<g/>
,	,	kIx,
palladium	palladium	k1gNnSc1
<g/>
,	,	kIx,
zirkon	zirkon	k1gInSc1
<g/>
,	,	kIx,
rutil	rutil	k1gInSc1
<g/>
,	,	kIx,
ilmenit	ilmenit	k1gInSc1
<g/>
,	,	kIx,
sůl	sůl	k1gFnSc1
<g/>
,	,	kIx,
uran	uran	k1gInSc1
a	a	k8xC
opály	opál	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
nejdůležitější	důležitý	k2eAgInPc4d3
průmyslová	průmyslový	k2eAgNnPc1d1
odvětví	odvětví	k1gNnPc1
patří	patřit	k5eAaImIp3nP
hutnictví	hutnictví	k1gNnPc4
(	(	kIx(
<g/>
surové	surový	k2eAgNnSc4d1
železo	železo	k1gNnSc4
<g/>
,	,	kIx,
ocel	ocel	k1gFnSc4
<g/>
,	,	kIx,
barevné	barevný	k2eAgInPc4d1
kovy	kov	k1gInPc4
a	a	k8xC
hliník	hliník	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
petrochemie	petrochemie	k1gFnSc1
(	(	kIx(
<g/>
rafinérie	rafinérie	k1gFnSc1
ropy	ropa	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
chemie	chemie	k1gFnSc1
(	(	kIx(
<g/>
plastické	plastický	k2eAgFnSc2d1
hmoty	hmota	k1gFnSc2
<g/>
,	,	kIx,
kyseliny	kyselina	k1gFnPc1
<g/>
,	,	kIx,
superfosfáty	superfosfát	k1gInPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
strojírenství	strojírenství	k1gNnSc6
<g/>
,	,	kIx,
textilní	textilní	k2eAgInSc4d1
a	a	k8xC
potravinářský	potravinářský	k2eAgInSc4d1
průmysl	průmysl	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejdůležitější	důležitý	k2eAgFnPc4d3
průmyslová	průmyslový	k2eAgNnPc4d1
centra	centrum	k1gNnPc4
jsou	být	k5eAaImIp3nP
v	v	k7c6
aglomeracích	aglomerace	k1gFnPc6
Sydney	Sydney	k1gNnSc2
a	a	k8xC
Melbourne	Melbourne	k1gNnSc2
a	a	k8xC
také	také	k9
ve	v	k7c6
velkých	velký	k2eAgNnPc6d1
městech	město	k1gNnPc6
Brisbane	Brisban	k1gMnSc5
<g/>
,	,	kIx,
Perth	Pertha	k1gFnPc2
<g/>
,	,	kIx,
Adelaide	Adelaid	k1gMnSc5
<g/>
,	,	kIx,
Newcastle	Newcastle	k1gFnPc2
a	a	k8xC
Geelong	Geelonga	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Významná	významný	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
výroba	výroba	k1gFnSc1
elektrické	elektrický	k2eAgFnSc2d1
energie	energie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Orná	orný	k2eAgFnSc1d1
půda	půda	k1gFnSc1
zaujímá	zaujímat	k5eAaImIp3nS
jen	jen	k9
6,5	6,5	k4
%	%	kIx~
<g/>
,	,	kIx,
louky	louka	k1gFnPc1
a	a	k8xC
pastviny	pastvina	k1gFnPc1
54	#num#	k4
%	%	kIx~
a	a	k8xC
lesy	les	k1gInPc1
19	#num#	k4
%	%	kIx~
plochy	plocha	k1gFnSc2
země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sklízí	sklíze	k1gFnPc2
se	se	k3xPyFc4
pšenice	pšenice	k1gFnSc2
<g/>
,	,	kIx,
ječmen	ječmen	k1gInSc1
<g/>
,	,	kIx,
oves	oves	k1gInSc1
<g/>
,	,	kIx,
rýže	rýže	k1gFnSc1
<g/>
,	,	kIx,
cukrová	cukrový	k2eAgFnSc1d1
třtina	třtina	k1gFnSc1
<g/>
,	,	kIx,
bavlna	bavlna	k1gFnSc1
<g/>
,	,	kIx,
luštěniny	luštěnina	k1gFnPc1
<g/>
,	,	kIx,
brambory	brambora	k1gFnPc1
<g/>
,	,	kIx,
sorgo	sorgo	k1gNnSc1
<g/>
,	,	kIx,
zelenina	zelenina	k1gFnSc1
<g/>
,	,	kIx,
ovoce	ovoce	k1gNnSc1
<g/>
,	,	kIx,
slunečnice	slunečnice	k1gFnSc1
<g/>
,	,	kIx,
řepka	řepka	k1gFnSc1
<g/>
,	,	kIx,
sója	sója	k1gFnSc1
<g/>
,	,	kIx,
vinné	vinný	k2eAgInPc4d1
hrozny	hrozen	k1gInPc4
<g/>
,	,	kIx,
chmel	chmel	k1gInSc4
a	a	k8xC
tabák	tabák	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Austrálie	Austrálie	k1gFnSc1
je	být	k5eAaImIp3nS
čtvrtým	čtvrtý	k4xOgMnSc7
největším	veliký	k2eAgMnSc7d3
vývozcem	vývozce	k1gMnSc7
vína	víno	k1gNnSc2
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
78	#num#	k4
<g/>
]	]	kIx)
Nejdůležitější	důležitý	k2eAgMnSc1d3
pro	pro	k7c4
živočišnou	živočišný	k2eAgFnSc4d1
výrobu	výroba	k1gFnSc4
je	být	k5eAaImIp3nS
chov	chov	k1gInSc1
ovcí	ovce	k1gFnPc2
<g/>
,	,	kIx,
kterých	který	k3yQgFnPc2,k3yRgFnPc2,k3yIgFnPc2
je	být	k5eAaImIp3nS
100	#num#	k4
000	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
5	#num#	k4
<g/>
x	x	k?
více	hodně	k6eAd2
než	než	k8xS
lidí	člověk	k1gMnPc2
v	v	k7c6
Austrálii	Austrálie	k1gFnSc6
(	(	kIx(
<g/>
produkce	produkce	k1gFnSc2
surové	surový	k2eAgFnSc2d1
a	a	k8xC
prané	praný	k2eAgFnSc2d1
vlny	vlna	k1gFnSc2
<g/>
,	,	kIx,
ovčích	ovčí	k2eAgFnPc2d1
kůží	kůže	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dále	daleko	k6eAd2
skotu	skot	k1gInSc2
<g/>
,	,	kIx,
prasat	prase	k1gNnPc2
<g/>
,	,	kIx,
koní	kůň	k1gMnPc2
a	a	k8xC
drůbeže	drůbež	k1gFnSc2
a	a	k8xC
rybolov	rybolov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těží	těžet	k5eAaImIp3nS
se	se	k3xPyFc4
dřevo	dřevo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
důvodu	důvod	k1gInSc2
rozlehlosti	rozlehlost	k1gFnSc2
země	zem	k1gFnSc2
má	mít	k5eAaImIp3nS
velký	velký	k2eAgInSc1d1
význam	význam	k1gInSc1
zejména	zejména	k9
letecká	letecký	k2eAgFnSc1d1
a	a	k8xC
silniční	silniční	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
a	a	k8xC
také	také	k9
doprava	doprava	k1gFnSc1
železniční	železniční	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důležité	důležitý	k2eAgNnSc1d1
je	být	k5eAaImIp3nS
obchodní	obchodní	k2eAgNnSc1d1
loďstvo	loďstvo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zemi	zem	k1gFnSc4
ročně	ročně	k6eAd1
navštíví	navštívit	k5eAaPmIp3nS
více	hodně	k6eAd2
než	než	k8xS
5	#num#	k4
milionů	milion	k4xCgInPc2
návštěvníků	návštěvník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
je	být	k5eAaImIp3nS
24	#num#	k4
<g/>
.	.	kIx.
největším	veliký	k2eAgMnSc7d3
vývozcem	vývozce	k1gMnSc7
na	na	k7c6
světě	svět	k1gInSc6
a	a	k8xC
22	#num#	k4
<g/>
.	.	kIx.
největším	veliký	k2eAgMnSc7d3
dovozcem	dovozce	k1gMnSc7
(	(	kIx(
<g/>
k	k	k7c3
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejími	její	k3xOp3gMnPc7
největšími	veliký	k2eAgMnPc7d3
obchodními	obchodní	k2eAgMnPc7d1
partnery	partner	k1gMnPc7
jsou	být	k5eAaImIp3nP
Čína	Čína	k1gFnSc1
<g/>
,	,	kIx,
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
a	a	k8xC
Japonsko	Japonsko	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
79	#num#	k4
<g/>
]	]	kIx)
Vyváží	vyvážet	k5eAaImIp3nS,k5eAaPmIp3nS
se	se	k3xPyFc4
především	především	k9
suroviny	surovina	k1gFnPc1
<g/>
,	,	kIx,
tedy	tedy	k8xC
železná	železný	k2eAgFnSc1d1
ruda	ruda	k1gFnSc1
a	a	k8xC
uhlí	uhlí	k1gNnSc1
<g/>
,	,	kIx,
krom	krom	k7c2
toho	ten	k3xDgNnSc2
je	být	k5eAaImIp3nS
Austrálie	Austrálie	k1gFnSc1
největším	veliký	k2eAgMnSc7d3
vývozcem	vývozce	k1gMnSc7
zkapalnělého	zkapalnělý	k2eAgInSc2d1
zemního	zemní	k2eAgInSc2d1
plynu	plyn	k1gInSc2
na	na	k7c6
světě	svět	k1gInSc6
<g/>
,	,	kIx,
když	když	k8xS
v	v	k7c6
tomto	tento	k3xDgInSc6
ohledu	ohled	k1gInSc6
překonala	překonat	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
2019	#num#	k4
Katar	katar	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
nejvíce	hodně	k6eAd3,k6eAd1
musí	muset	k5eAaImIp3nP
Australané	Australan	k1gMnPc1
dovážet	dovážet	k5eAaImF
ropu	ropa	k1gFnSc4
a	a	k8xC
automobily	automobil	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Austrálie	Austrálie	k1gFnSc1
se	se	k3xPyFc4
snaží	snažit	k5eAaImIp3nS
vytvořit	vytvořit	k5eAaPmF
mohutnou	mohutný	k2eAgFnSc4d1
zónu	zóna	k1gFnSc4
volného	volný	k2eAgInSc2d1
obchodu	obchod	k1gInSc2
v	v	k7c6
jihovýchodní	jihovýchodní	k2eAgFnSc6d1
Asii	Asie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dohodu	dohoda	k1gFnSc4
o	o	k7c6
volném	volný	k2eAgInSc6d1
obchodu	obchod	k1gInSc6
již	již	k6eAd1
podepsala	podepsat	k5eAaPmAgFnS
se	s	k7c7
sdružením	sdružení	k1gNnSc7
ASEAN	ASEAN	kA
<g/>
,	,	kIx,
krom	krom	k7c2
toho	ten	k3xDgNnSc2
jednotlivě	jednotlivě	k6eAd1
se	s	k7c7
Singapurem	Singapur	k1gInSc7
<g/>
,	,	kIx,
Thajskem	Thajsko	k1gNnSc7
<g/>
,	,	kIx,
Malajsií	Malajsie	k1gFnSc7
<g/>
,	,	kIx,
Jižní	jižní	k2eAgFnSc7d1
Koreou	Korea	k1gFnSc7
<g/>
,	,	kIx,
Japonskem	Japonsko	k1gNnSc7
<g/>
,	,	kIx,
Novým	nový	k2eAgInSc7d1
Zélandem	Zéland	k1gInSc7
<g/>
,	,	kIx,
Čínou	Čína	k1gFnSc7
<g/>
,	,	kIx,
Hongkongem	Hongkong	k1gInSc7
a	a	k8xC
Indonésií	Indonésie	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Austrálie	Austrálie	k1gFnSc1
tvoří	tvořit	k5eAaImIp3nS
zónu	zóna	k1gFnSc4
volného	volný	k2eAgInSc2d1
obchodu	obchod	k1gInSc2
také	také	k9
s	s	k7c7
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pozoruhodné	pozoruhodný	k2eAgNnSc1d1
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc7
podařilo	podařit	k5eAaPmAgNnS
takovou	takový	k3xDgFnSc4
dohodu	dohoda	k1gFnSc4
podepsat	podepsat	k5eAaPmF
také	také	k9
se	s	k7c7
dvěma	dva	k4xCgFnPc7
jihoamerickými	jihoamerický	k2eAgFnPc7d1
zeměmi	zem	k1gFnPc7
<g/>
,	,	kIx,
s	s	k7c7
Chile	Chile	k1gNnSc7
a	a	k8xC
Peru	Peru	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
2007	#num#	k4
pak	pak	k6eAd1
probíhají	probíhat	k5eAaImIp3nP
jednání	jednání	k1gNnPc1
o	o	k7c4
vytvoření	vytvoření	k1gNnSc4
zóny	zóna	k1gFnSc2
volného	volný	k2eAgInSc2d1
obchodu	obchod	k1gInSc2
s	s	k7c7
Bahrajnem	Bahrajn	k1gInSc7
<g/>
,	,	kIx,
Kuvajtem	Kuvajt	k1gInSc7
<g/>
,	,	kIx,
Ománem	Omán	k1gInSc7
<g/>
,	,	kIx,
Katarem	katar	k1gInSc7
<g/>
,	,	kIx,
Saudskou	saudský	k2eAgFnSc7d1
Arábií	Arábie	k1gFnSc7
a	a	k8xC
Spojenými	spojený	k2eAgInPc7d1
arabskými	arabský	k2eAgInPc7d1
emiráty	emirát	k1gInPc7
<g/>
,	,	kIx,
intenzivní	intenzivní	k2eAgNnSc1d1
jednání	jednání	k1gNnSc1
probíhají	probíhat	k5eAaImIp3nP
též	též	k9
s	s	k7c7
Indií	Indie	k1gFnSc7
a	a	k8xC
Evropskou	evropský	k2eAgFnSc7d1
unií	unie	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největším	veliký	k2eAgMnSc7d3
přímým	přímý	k2eAgMnSc7d1
investorem	investor	k1gMnSc7
v	v	k7c6
zemi	zem	k1gFnSc6
jsou	být	k5eAaImIp3nP
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s>
Měnou	měna	k1gFnSc7
je	být	k5eAaImIp3nS
australský	australský	k2eAgInSc1d1
dolar	dolar	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tuto	tento	k3xDgFnSc4
měnu	měna	k1gFnSc4
užívají	užívat	k5eAaImIp3nP
i	i	k9
tři	tři	k4xCgInPc4
další	další	k2eAgInPc4d1
nezávislé	závislý	k2eNgInPc4d1
státy	stát	k1gInPc4
<g/>
:	:	kIx,
Kiribati	Kiribati	k1gFnPc4
<g/>
,	,	kIx,
Nauru	Naura	k1gFnSc4
a	a	k8xC
Tuvalu	Tuvala	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Australian	Australian	k1gMnSc1
Securities	Securities	k1gMnSc1
Exchange	Exchange	k1gFnPc2
je	být	k5eAaImIp3nS
devátá	devátý	k4xOgFnSc1
největší	veliký	k2eAgFnSc1d3
burza	burza	k1gFnSc1
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
80	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
je	být	k5eAaImIp3nS
též	též	k9
známa	znám	k2eAgFnSc1d1
svobodným	svobodný	k2eAgNnSc7d1
prostředím	prostředí	k1gNnSc7
pro	pro	k7c4
podnikání	podnikání	k1gNnSc4
<g/>
,	,	kIx,
podle	podle	k7c2
Indexu	index	k1gInSc2
ekonomické	ekonomický	k2eAgFnSc2d1
svobody	svoboda	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
každoročně	každoročně	k6eAd1
sestavuje	sestavovat	k5eAaImIp3nS
The	The	k1gFnSc4
Heritage	Heritag	k1gFnSc2
Foundation	Foundation	k1gInSc1
a	a	k8xC
The	The	k1gMnSc1
Wall	Wall	k1gMnSc1
Street	Street	k1gMnSc1
Journal	Journal	k1gMnSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
Austrálie	Austrálie	k1gFnSc1
čtvrtou	čtvrtá	k1gFnSc4
nejsvobodnější	svobodný	k2eAgFnSc4d3
zemí	zem	k1gFnSc7
pro	pro	k7c4
obchod	obchod	k1gInSc4
na	na	k7c6
světě	svět	k1gInSc6
(	(	kIx(
<g/>
k	k	k7c3
roku	rok	k1gInSc3
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
navíc	navíc	k6eAd1
je	být	k5eAaImIp3nS
nejvýše	nejvýše	k6eAd1,k6eAd3
umístěnou	umístěný	k2eAgFnSc4d1
<g />
.	.	kIx.
</s>
<s hack="1">
opravdu	opravdu	k6eAd1
velkou	velká	k1gFnSc4
zemí	zem	k1gFnPc2
(	(	kIx(
<g/>
na	na	k7c6
čele	čelo	k1gNnSc6
žebříčku	žebříček	k1gInSc2
jsou	být	k5eAaImIp3nP
městské	městský	k2eAgInPc1d1
státy	stát	k1gInPc1
Singapur	Singapur	k1gInSc1
a	a	k8xC
Hongkong	Hongkong	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
navíc	navíc	k6eAd1
není	být	k5eNaImIp3nS
zemí	zem	k1gFnPc2
v	v	k7c6
pravém	pravý	k2eAgInSc6d1
slova	slovo	k1gNnSc2
smyslu	smysl	k1gInSc6
<g/>
;	;	kIx,
třetí	třetí	k4xOgInSc1
je	být	k5eAaImIp3nS
sousední	sousední	k2eAgInSc1d1
Nový	nový	k2eAgInSc1d1
Zéland	Zéland	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
81	#num#	k4
<g/>
]	]	kIx)
Souběžně	souběžně	k6eAd1
s	s	k7c7
tím	ten	k3xDgNnSc7
má	mít	k5eAaImIp3nS
přitom	přitom	k6eAd1
velmi	velmi	k6eAd1
vysokou	vysoký	k2eAgFnSc4d1
kvalitu	kvalita	k1gFnSc4
života	život	k1gInSc2
<g/>
,	,	kIx,
podle	podle	k7c2
Indexu	index	k1gInSc2
lidského	lidský	k2eAgInSc2d1
rozvoje	rozvoj	k1gInSc2
šestou	šestý	k4xOgFnSc4
nejvyšší	vysoký	k2eAgFnSc4d3
na	na	k7c6
světě	svět	k1gInSc6
(	(	kIx(
<g/>
k	k	k7c3
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
82	#num#	k4
<g/>
]	]	kIx)
Australská	australský	k2eAgNnPc1d1
města	město	k1gNnPc1
se	se	k3xPyFc4
rovněž	rovněž	k9
tradičně	tradičně	k6eAd1
umisťují	umisťovat	k5eAaImIp3nP
vysoko	vysoko	k6eAd1
v	v	k7c6
žebříčku	žebříček	k1gInSc6
měst	město	k1gNnPc2
nejlepších	dobrý	k2eAgFnPc2d3
k	k	k7c3
životu	život	k1gInSc3
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
sestavuje	sestavovat	k5eAaImIp3nS
týdeník	týdeník	k1gInSc1
The	The	k1gMnSc1
Economist	Economist	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2019	#num#	k4
se	se	k3xPyFc4
Melbourne	Melbourne	k1gNnPc1
v	v	k7c6
tomto	tento	k3xDgInSc6
žebříčku	žebříček	k1gInSc6
umístilo	umístit	k5eAaPmAgNnS
na	na	k7c6
druhém	druhý	k4xOgInSc6
místě	místo	k1gNnSc6
<g/>
,	,	kIx,
Sydney	Sydney	k1gNnSc4
na	na	k7c6
třetím	třetí	k4xOgInSc6
a	a	k8xC
Adelaide	Adelaid	k1gInSc5
na	na	k7c6
desátém	desátý	k4xOgInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
83	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Obyvatelstvo	obyvatelstvo	k1gNnSc4
Austrálie	Austrálie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Téměř	téměř	k6eAd1
tři	tři	k4xCgFnPc4
čtvrtiny	čtvrtina	k1gFnPc1
Australanů	Australan	k1gMnPc2
žijí	žít	k5eAaImIp3nP
ve	v	k7c6
městech	město	k1gNnPc6
a	a	k8xC
pobřežních	pobřežní	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pláž	pláž	k1gFnSc1
je	být	k5eAaImIp3nS
nedílnou	dílný	k2eNgFnSc7d1
součástí	součást	k1gFnSc7
australské	australský	k2eAgFnSc2d1
identity	identita	k1gFnSc2
<g/>
[	[	kIx(
<g/>
84	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Melbourne	Melbourne	k1gNnSc1
je	být	k5eAaImIp3nS
druhé	druhý	k4xOgNnSc1
největší	veliký	k2eAgNnSc1d3
město	město	k1gNnSc1
Austrálie	Austrálie	k1gFnSc2
se	se	k3xPyFc4
4	#num#	k4
miliony	milion	k4xCgInPc1
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
řídce	řídce	k6eAd1
osídlenou	osídlený	k2eAgFnSc7d1
zemí	zem	k1gFnSc7
<g/>
,	,	kIx,
zemi	zem	k1gFnSc4
o	o	k7c6
dvojnásobné	dvojnásobný	k2eAgFnSc6d1
rozloze	rozloha	k1gFnSc6
Indie	Indie	k1gFnSc2
obývá	obývat	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
23	#num#	k4
milionů	milion	k4xCgInPc2
obyvatel	obyvatel	k1gMnPc2
(	(	kIx(
<g/>
podle	podle	k7c2
sčítání	sčítání	k1gNnSc2
z	z	k7c2
roku	rok	k1gInSc2
2016	#num#	k4
<g/>
[	[	kIx(
<g/>
85	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
odhad	odhad	k1gInSc1
pro	pro	k7c4
rok	rok	k1gInSc4
2021	#num#	k4
je	být	k5eAaImIp3nS
25	#num#	k4
milionů	milion	k4xCgInPc2
<g/>
[	[	kIx(
<g/>
86	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obyvatelstvo	obyvatelstvo	k1gNnSc1
by	by	kYmCp3nP
mohlo	moct	k5eAaImAgNnS
stoupnout	stoupnout	k5eAaPmF
až	až	k9
na	na	k7c6
hranici	hranice	k1gFnSc6
42	#num#	k4
milionů	milion	k4xCgInPc2
v	v	k7c6
roce	rok	k1gInSc6
2050	#num#	k4
<g/>
,	,	kIx,
pokud	pokud	k8xS
by	by	kYmCp3nS
současný	současný	k2eAgInSc1d1
migrační	migrační	k2eAgInSc1d1
trend	trend	k1gInSc1
pokračoval	pokračovat	k5eAaImAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
asi	asi	k9
o	o	k7c4
6	#num#	k4
milionů	milion	k4xCgInPc2
víc	hodně	k6eAd2
<g/>
,	,	kIx,
než	než	k8xS
plánuje	plánovat	k5eAaImIp3nS
vláda	vláda	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
87	#num#	k4
<g/>
]	]	kIx)
Většina	většina	k1gFnSc1
obyvatel	obyvatel	k1gMnPc2
je	být	k5eAaImIp3nS
soustředěna	soustředit	k5eAaPmNgFnS
při	při	k7c6
pobřeží	pobřeží	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
je	být	k5eAaImIp3nS
2,8	2,8	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
/	/	kIx~
<g/>
km	km	kA
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
76,1	76,1	k4
%	%	kIx~
obyvatel	obyvatel	k1gMnPc2
o	o	k7c6
sobě	sebe	k3xPyFc6
uvádí	uvádět	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
jsou	být	k5eAaImIp3nP
potomci	potomek	k1gMnPc1
britských	britský	k2eAgInPc2d1
a	a	k8xC
irských	irský	k2eAgMnPc2d1
přistěhovalců	přistěhovalec	k1gMnPc2
<g/>
,	,	kIx,
15,6	15,6	k4
%	%	kIx~
jsou	být	k5eAaImIp3nP
potomci	potomek	k1gMnPc1
přistěhovalců	přistěhovalec	k1gMnPc2
z	z	k7c2
ostatních	ostatní	k2eAgFnPc2d1
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
,	,	kIx,
5,3	5,3	k4
%	%	kIx~
má	mít	k5eAaImIp3nS
původ	původ	k1gInSc4
asijský	asijský	k2eAgInSc4d1
a	a	k8xC
3	#num#	k4
procenta	procento	k1gNnSc2
jsou	být	k5eAaImIp3nP
původní	původní	k2eAgMnPc1d1
Austrálci	Austrálec	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původních	původní	k2eAgMnPc2d1
obyvatel	obyvatel	k1gMnPc2
zde	zde	k6eAd1
žije	žít	k5eAaImIp3nS
asi	asi	k9
517	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
obývají	obývat	k5eAaImIp3nP
především	především	k9
Severní	severní	k2eAgNnSc4d1
teritorium	teritorium	k1gNnSc4
a	a	k8xC
ostrovy	ostrov	k1gInPc4
v	v	k7c6
Torresově	Torresův	k2eAgInSc6d1
průlivu	průliv	k1gInSc6
<g/>
.	.	kIx.
28,5	28,5	k4
%	%	kIx~
obyvatel	obyvatel	k1gMnSc1
Austrálie	Austrálie	k1gFnSc2
(	(	kIx(
<g/>
asi	asi	k9
7	#num#	k4
milionů	milion	k4xCgInPc2
<g/>
)	)	kIx)
se	se	k3xPyFc4
narodila	narodit	k5eAaPmAgFnS
mimo	mimo	k7c4
její	její	k3xOp3gNnSc4
území	území	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
76	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
má	mít	k5eAaImIp3nS
dlouhou	dlouhý	k2eAgFnSc4d1
historii	historie	k1gFnSc4
přistěhovalectví	přistěhovalectví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
a	a	k8xC
na	na	k7c6
počátku	počátek	k1gInSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
převládali	převládat	k5eAaImAgMnP
imigranti	imigrant	k1gMnPc1
z	z	k7c2
Britských	britský	k2eAgInPc2d1
ostrovů	ostrov	k1gInPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
dnes	dnes	k6eAd1
tvoří	tvořit	k5eAaImIp3nP
základ	základ	k1gInSc4
australského	australský	k2eAgInSc2d1
národa	národ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c4
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
přistěhovalo	přistěhovat	k5eAaPmAgNnS
mnoho	mnoho	k4c1
Řeků	Řek	k1gMnPc2
<g/>
,	,	kIx,
Italů	Ital	k1gMnPc2
<g/>
,	,	kIx,
Němců	Němec	k1gMnPc2
a	a	k8xC
také	také	k9
Češi	Čech	k1gMnPc1
a	a	k8xC
Slováci	Slovák	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
70	#num#	k4
<g/>
.	.	kIx.
letech	let	k1gInPc6
vláda	vláda	k1gFnSc1
zrušila	zrušit	k5eAaPmAgFnS
politiku	politika	k1gFnSc4
„	„	k?
<g/>
bílé	bílý	k2eAgFnSc2d1
Austrálie	Austrálie	k1gFnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
preferovala	preferovat	k5eAaImAgFnS
imigranty	imigrant	k1gMnPc4
z	z	k7c2
Evropy	Evropa	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
roste	růst	k5eAaImIp3nS
počet	počet	k1gInSc1
přistěhovalců	přistěhovalec	k1gMnPc2
z	z	k7c2
Asie	Asie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současnosti	současnost	k1gFnSc6
australská	australský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
přijímá	přijímat	k5eAaImIp3nS
ročně	ročně	k6eAd1
okolo	okolo	k7c2
190	#num#	k4
000	#num#	k4
imigrantů	imigrant	k1gMnPc2
a	a	k8xC
většina	většina	k1gFnSc1
z	z	k7c2
nich	on	k3xPp3gFnPc2
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
Asie	Asie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
88	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
89	#num#	k4
%	%	kIx~
obyvatel	obyvatel	k1gMnPc2
žije	žít	k5eAaImIp3nS
ve	v	k7c6
velkých	velký	k2eAgNnPc6d1
městech	město	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Austrálie	Austrálie	k1gFnSc1
tak	tak	k9
patří	patřit	k5eAaImIp3nS
k	k	k7c3
nejurbanizovanějším	urbanizovaný	k2eAgFnPc3d3
zemím	zem	k1gFnPc3
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pět	pět	k4xCc1
měst	město	k1gNnPc2
má	mít	k5eAaImIp3nS
více	hodně	k6eAd2
než	než	k8xS
milion	milion	k4xCgInSc4
obyvatel	obyvatel	k1gMnPc2
<g/>
:	:	kIx,
Sydney	Sydney	k1gNnSc1
<g/>
,	,	kIx,
Melbourne	Melbourne	k1gNnSc1
<g/>
,	,	kIx,
Brisbane	Brisban	k1gMnSc5
<g/>
,	,	kIx,
Perth	Pertha	k1gFnPc2
a	a	k8xC
Adelaide	Adelaid	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dohromady	dohromady	k6eAd1
v	v	k7c6
těchto	tento	k3xDgFnPc6
pěti	pět	k4xCc6
městech	město	k1gNnPc6
<g/>
,	,	kIx,
nebo	nebo	k8xC
jejich	jejich	k3xOp3gNnSc6
bezprostředním	bezprostřední	k2eAgNnSc6d1
okolí	okolí	k1gNnSc6
<g/>
,	,	kIx,
žije	žít	k5eAaImIp3nS
16,3	16,3	k4
milionu	milion	k4xCgInSc2
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
tedy	tedy	k9
přes	přes	k7c4
70	#num#	k4
procent	procento	k1gNnPc2
obyvatel	obyvatel	k1gMnPc2
země	zem	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
89	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jazyk	jazyk	k1gInSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Jazyky	jazyk	k1gInPc1
Austrálie	Austrálie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
I	i	k9
když	když	k8xS
Austrálie	Austrálie	k1gFnSc1
žádný	žádný	k3yNgInSc4
úřední	úřední	k2eAgInSc4d1
jazyk	jazyk	k1gInSc4
nemá	mít	k5eNaImIp3nS
<g/>
,	,	kIx,
tak	tak	k8xS,k8xC
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
pevně	pevně	k6eAd1
zakotvena	zakotven	k2eAgFnSc1d1
angličtina	angličtina	k1gFnSc1
de	de	k?
facto	facto	k1gNnSc1
jako	jako	k8xC,k8xS
národní	národní	k2eAgInSc1d1
jazyk	jazyk	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc7
nejpoužívanější	používaný	k2eAgFnSc7d3
formou	forma	k1gFnSc7
je	být	k5eAaImIp3nS
australská	australský	k2eAgFnSc1d1
angličtina	angličtina	k1gFnSc1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
dialekt	dialekt	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
se	se	k3xPyFc4
od	od	k7c2
angličtiny	angličtina	k1gFnSc2
ve	v	k7c6
zbytku	zbytek	k1gInSc6
světa	svět	k1gInSc2
liší	lišit	k5eAaImIp3nS
jak	jak	k6eAd1
výslovností	výslovnost	k1gFnSc7
<g/>
,	,	kIx,
tak	tak	k6eAd1
slovní	slovní	k2eAgFnSc7d1
zásobou	zásoba	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
sčítání	sčítání	k1gNnSc2
lidu	lid	k1gInSc2
z	z	k7c2
roku	rok	k1gInSc2
2016	#num#	k4
se	se	k3xPyFc4
v	v	k7c6
Austrálii	Austrálie	k1gFnSc6
mluví	mluvit	k5eAaImIp3nS
kromě	kromě	k7c2
angličtiny	angličtina	k1gFnSc2
více	hodně	k6eAd2
než	než	k8xS
300	#num#	k4
jazyky	jazyk	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zjistilo	zjistit	k5eAaPmAgNnS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
72,7	72,7	k4
%	%	kIx~
australských	australský	k2eAgFnPc2d1
domácností	domácnost	k1gFnPc2
používá	používat	k5eAaImIp3nS
pouze	pouze	k6eAd1
angličtinu	angličtina	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhým	druhý	k4xOgInSc7
nejrozšířenějším	rozšířený	k2eAgInSc7d3
jazykem	jazyk	k1gInSc7
je	být	k5eAaImIp3nS
standardní	standardní	k2eAgFnSc1d1
čínština	čínština	k1gFnSc1
(	(	kIx(
<g/>
2,5	2,5	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
následovaná	následovaný	k2eAgFnSc1d1
arabštinou	arabština	k1gFnSc7
(	(	kIx(
<g/>
1,4	1,4	k4
%	%	kIx~
<g/>
)	)	kIx)
a	a	k8xC
kantonštinou	kantonština	k1gFnSc7
(	(	kIx(
<g/>
1,2	1,2	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
90	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Když	když	k8xS
do	do	k7c2
Austrálie	Austrálie	k1gFnSc2
poprvé	poprvé	k6eAd1
přijel	přijet	k5eAaPmAgMnS
James	James	k1gMnSc1
Cook	Cook	k1gMnSc1
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
počet	počet	k1gInSc1
jazyků	jazyk	k1gInPc2
původních	původní	k2eAgMnPc2d1
obyvatel	obyvatel	k1gMnPc2
více	hodně	k6eAd2
než	než	k8xS
250	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
dnešní	dnešní	k2eAgFnSc2d1
doby	doba	k1gFnSc2
se	se	k3xPyFc4
jich	on	k3xPp3gMnPc2
uchovalo	uchovat	k5eAaPmAgNnS
něco	něco	k6eAd1
přes	přes	k7c4
100	#num#	k4
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
méně	málo	k6eAd2
než	než	k8xS
20	#num#	k4
je	být	k5eAaImIp3nS
stále	stále	k6eAd1
aktivně	aktivně	k6eAd1
používáno	používán	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Austrálské	Austrálský	k2eAgInPc4d1
domorodé	domorodý	k2eAgInPc4d1
jazyky	jazyk	k1gInPc4
mají	mít	k5eAaImIp3nP
okolo	okolo	k7c2
50	#num#	k4
000	#num#	k4
mluvčích	mluvčí	k1gMnPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
méně	málo	k6eAd2
než	než	k8xS
pětina	pětina	k1gFnSc1
austrálské	austrálský	k2eAgFnSc2d1
domorodé	domorodý	k2eAgFnSc2d1
populace	populace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
nejpoužívanější	používaný	k2eAgInPc4d3
austrálské	austrálský	k2eAgInPc4d1
domorodé	domorodý	k2eAgInPc4d1
jazyky	jazyk	k1gInPc4
patří	patřit	k5eAaImIp3nS
jazyk	jazyk	k1gInSc1
Západní	západní	k2eAgFnSc2d1
pouště	poušť	k1gFnSc2
a	a	k8xC
warlpiri	warlpir	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naprostá	naprostý	k2eAgFnSc1d1
většina	většina	k1gFnSc1
austrálských	austrálský	k2eAgInPc2d1
domorodých	domorodý	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
patří	patřit	k5eAaImIp3nS
do	do	k7c2
velké	velký	k2eAgFnSc2d1
jazykové	jazykový	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
pama-nyunganských	pama-nyunganský	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
<g/>
,	,	kIx,
na	na	k7c6
severu	sever	k1gInSc6
Austrálie	Austrálie	k1gFnSc2
ale	ale	k8xC
existují	existovat	k5eAaImIp3nP
další	další	k2eAgFnPc4d1
menší	malý	k2eAgFnPc4d2
jazykové	jazykový	k2eAgFnPc4d1
rodiny	rodina	k1gFnPc4
(	(	kIx(
<g/>
například	například	k6eAd1
garawanské	garawanský	k2eAgInPc1d1
jazyky	jazyk	k1gInPc1
nebo	nebo	k8xC
nyulnyulanské	nyulnyulanský	k2eAgInPc1d1
jazyky	jazyk	k1gInPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
91	#num#	k4
<g/>
]	]	kIx)
Domorodci	domorodec	k1gMnPc1
z	z	k7c2
Torresových	Torresový	k2eAgInPc2d1
ostrovů	ostrov	k1gInPc2
používají	používat	k5eAaImIp3nP
dva	dva	k4xCgInPc1
domorodé	domorodý	k2eAgInPc1d1
jazyky	jazyk	k1gInPc1
<g/>
:	:	kIx,
kalaw	kalaw	k?
lagaw	lagaw	k?
ya	ya	k?
(	(	kIx(
<g/>
patřící	patřící	k2eAgInPc1d1
do	do	k7c2
pama-nyunganských	pama-nyunganský	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
<g/>
)	)	kIx)
a	a	k8xC
meriam	meriam	k6eAd1
(	(	kIx(
<g/>
patřící	patřící	k2eAgFnSc1d1
do	do	k7c2
rodiny	rodina	k1gFnSc2
východních	východní	k2eAgInPc2d1
trans-Fly	trans-Flo	k1gNnPc7
jazyků	jazyk	k1gInPc2
<g/>
,	,	kIx,
jedné	jeden	k4xCgFnSc2
z	z	k7c2
rodin	rodina	k1gFnPc2
papuánských	papuánský	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původní	původní	k2eAgInPc1d1
jazyky	jazyk	k1gInPc1
Tasmánců	Tasmánec	k1gMnPc2
vymřely	vymřít	k5eAaPmAgInP
<g/>
,	,	kIx,
potomci	potomek	k1gMnPc1
Tasmánců	Tasmánec	k1gMnPc2
vyvíjejí	vyvíjet	k5eAaImIp3nP
nový	nový	k2eAgInSc4d1
<g/>
,	,	kIx,
umělý	umělý	k2eAgInSc4d1
jazyk	jazyk	k1gInSc4
vycházející	vycházející	k2eAgInPc4d1
z	z	k7c2
původních	původní	k2eAgInPc2d1
tasmánských	tasmánský	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
palawa	palawa	k6eAd1
kani	kani	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Austrálii	Austrálie	k1gFnSc6
se	se	k3xPyFc4
používají	používat	k5eAaImIp3nP
dva	dva	k4xCgInPc1
kreolské	kreolský	k2eAgInPc1d1
jazyky	jazyk	k1gInPc1
na	na	k7c6
bázi	báze	k1gFnSc6
angličtiny	angličtina	k1gFnSc2
<g/>
:	:	kIx,
australská	australský	k2eAgFnSc1d1
kreolština	kreolština	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
mluví	mluvit	k5eAaImIp3nS
Aboridžinci	Aboridžinec	k1gInSc3
<g/>
,	,	kIx,
a	a	k8xC
kreolština	kreolština	k1gFnSc1
Torresovy	Torresův	k2eAgFnSc2d1
úžiny	úžina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Západní	západní	k2eAgFnSc6d1
Austrálii	Austrálie	k1gFnSc6
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
také	také	k9
smíšený	smíšený	k2eAgInSc1d1
jazyk	jazyk	k1gInSc1
Broome	Broom	k1gInSc5
Pearling	Pearling	k1gInSc1
Lugger	Lugger	k1gMnSc1
Pidgin	Pidgin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Austrálii	Austrálie	k1gFnSc6
také	také	k9
vznikly	vzniknout	k5eAaPmAgInP
zcela	zcela	k6eAd1
nové	nový	k2eAgInPc1d1
dialekty	dialekt	k1gInPc1
některých	některý	k3yIgInPc2
evropských	evropský	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
<g/>
,	,	kIx,
jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
dialekt	dialekt	k1gInSc4
němčiny	němčina	k1gFnSc2
z	z	k7c2
údolí	údolí	k1gNnSc2
Barossa	Baross	k1gMnSc2
nebo	nebo	k8xC
Il-Maltraljan	Il-Maltraljan	k1gMnSc1
(	(	kIx(
<g/>
maltralian	maltralian	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
varianta	varianta	k1gFnSc1
maltštiny	maltština	k1gFnSc2
rozšířená	rozšířený	k2eAgFnSc1d1
mezi	mezi	k7c4
Malťany	Malťan	k1gMnPc4
v	v	k7c6
Austrálii	Austrálie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
92	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
93	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Náboženství	náboženství	k1gNnSc1
</s>
<s>
Anglikánská	anglikánský	k2eAgFnSc1d1
katedrála	katedrála	k1gFnSc1
sv.	sv.	kA
Pavla	Pavel	k1gMnSc2
v	v	k7c6
Melbourne	Melbourne	k1gNnSc6
</s>
<s>
Při	při	k7c6
posledním	poslední	k2eAgNnSc6d1
sčítání	sčítání	k1gNnSc6
lidu	lid	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
se	s	k7c7
64	#num#	k4
%	%	kIx~
obyvatel	obyvatel	k1gMnPc2
prohlásilo	prohlásit	k5eAaPmAgNnS
ke	k	k7c3
křesťanství	křesťanství	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvíce	hodně	k6eAd3,k6eAd1
obyvatel	obyvatel	k1gMnPc2
se	se	k3xPyFc4
přihlásilo	přihlásit	k5eAaPmAgNnS
ke	k	k7c3
katolické	katolický	k2eAgFnSc3d1
církvi	církev	k1gFnSc3
–	–	k?
25	#num#	k4
%	%	kIx~
<g/>
,	,	kIx,
včetně	včetně	k7c2
východních	východní	k2eAgMnPc2d1
katolíků	katolík	k1gMnPc2
<g/>
,	,	kIx,
jimiž	jenž	k3xRgMnPc7
jsou	být	k5eAaImIp3nP
<g/>
:	:	kIx,
</s>
<s>
Chaldejští	chaldejský	k2eAgMnPc1d1
katolíci	katolík	k1gMnPc1
<g/>
:	:	kIx,
Eparchie	eparchie	k1gFnSc1
sv.	sv.	kA
<g/>
Tomáše	Tomáš	k1gMnSc2
Apoštola	apoštol	k1gMnSc2
v	v	k7c6
Sydney	Sydney	k1gNnSc6
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
vznikla	vzniknout	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
měla	mít	k5eAaImAgFnS
32	#num#	k4
000	#num#	k4
členů	člen	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počet	počet	k1gInSc4
věřících	věřící	k2eAgMnPc2d1
narůstá	narůstat	k5eAaImIp3nS
<g/>
:	:	kIx,
2014	#num#	k4
–	–	k?
39	#num#	k4
000	#num#	k4
členů	člen	k1gInPc2
<g/>
,	,	kIx,
2015	#num#	k4
–	–	k?
35	#num#	k4
000	#num#	k4
členů	člen	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Maronité	Maronitý	k2eAgNnSc1d1
<g/>
:	:	kIx,
Eparchie	eparchie	k1gFnSc1
sv.	sv.	kA
Marona	Marona	k1gFnSc1
v	v	k7c6
Sydney	Sydney	k1gNnSc6
<g/>
,	,	kIx,
1990	#num#	k4
–	–	k?
125	#num#	k4
000	#num#	k4
členů	člen	k1gMnPc2
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gInSc1
počet	počet	k1gInSc1
narůstá	narůstat	k5eAaImIp3nS
<g/>
:	:	kIx,
2000	#num#	k4
–	–	k?
160	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
2010	#num#	k4
–	–	k?
150	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
2014	#num#	k4
–	–	k?
154	#num#	k4
500	#num#	k4
<g/>
,	,	kIx,
2015	#num#	k4
–	–	k?
150	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Syromalabarští	Syromalabarský	k2eAgMnPc1d1
katolíci	katolík	k1gMnPc1
v	v	k7c6
Indii	Indie	k1gFnSc6
<g/>
:	:	kIx,
Eparchie	eparchie	k1gFnSc2
sv.	sv.	kA
Tomáše	Tomáš	k1gMnSc2
v	v	k7c6
Melbourne	Melbourne	k1gNnSc6
<g/>
,	,	kIx,
vznikla	vzniknout	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
2015	#num#	k4
měla	mít	k5eAaImAgFnS
50	#num#	k4
000	#num#	k4
členů	člen	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Řeckokatolíci-melchité	Řeckokatolíci-melchitý	k2eAgNnSc1d1
<g/>
:	:	kIx,
Eparchie	eparchie	k1gFnSc1
sv.	sv.	kA
Michaela	Michael	k1gMnSc2
v	v	k7c6
Sydney	Sydney	k1gNnSc6
<g/>
,	,	kIx,
1990	#num#	k4
–	–	k?
40	#num#	k4
000	#num#	k4
věřících	věřící	k1gMnPc2
<g/>
,	,	kIx,
počet	počet	k1gInSc1
narůstá	narůstat	k5eAaImIp3nS
<g/>
:	:	kIx,
2000	#num#	k4
–	–	k?
45	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
2010	#num#	k4
–	–	k?
50	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
2014	#num#	k4
–	–	k?
52	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
2015	#num#	k4
–	–	k?
52	#num#	k4
900	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Ukrajinští	ukrajinský	k2eAgMnPc1d1
řeckokatolíci	řeckokatolík	k1gMnPc1
<g/>
:	:	kIx,
Eparchie	eparchie	k1gFnSc1
sv.	sv.	kA
Petra	Petra	k1gFnSc1
a	a	k8xC
Pavla	Pavla	k1gFnSc1
v	v	k7c6
Melbourne	Melbourne	k1gNnSc6
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1990	#num#	k4
měla	mít	k5eAaImAgFnS
25	#num#	k4
000	#num#	k4
členů	člen	k1gMnPc2
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
2015	#num#	k4
už	už	k9
33	#num#	k4
100	#num#	k4
členů	člen	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Působí	působit	k5eAaImIp3nS
zde	zde	k6eAd1
i	i	k9
Personální	personální	k2eAgInSc1d1
ordinariát	ordinariát	k1gInSc1
Naší	náš	k3xOp1gFnSc2
Paní	paní	k1gFnSc2
(	(	kIx(
<g/>
tj.	tj.	kA
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
<g/>
)	)	kIx)
Jižního	jižní	k2eAgInSc2d1
kříže	kříž	k1gInSc2
pro	pro	k7c4
římské	římský	k2eAgMnPc4d1
katolíky	katolík	k1gMnPc4
anglikánského	anglikánský	k2eAgInSc2d1
ritu	rit	k1gInSc2
(	(	kIx(
<g/>
bývalé	bývalý	k2eAgMnPc4d1
anglikány	anglikán	k1gMnPc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
vznikl	vzniknout	k5eAaPmAgInS
15	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
měl	mít	k5eAaImAgInS
300	#num#	k4
věřících	věřící	k1gMnPc2
a	a	k8xC
7	#num#	k4
kněží	kněz	k1gMnPc2
ve	v	k7c6
4	#num#	k4
farnostech	farnost	k1gFnPc6
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gFnPc1
počet	počet	k1gInSc1
však	však	k9
narůstá	narůstat	k5eAaImIp3nS
<g/>
:	:	kIx,
V	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
měl	mít	k5eAaImAgInS
1	#num#	k4
000	#num#	k4
věřících	věřící	k1gMnPc2
a	a	k8xC
8	#num#	k4
kněží	kněz	k1gMnPc2
ve	v	k7c6
4	#num#	k4
farnostech	farnost	k1gFnPc6
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
již	již	k9
2	#num#	k4
000	#num#	k4
věřících	věřící	k1gMnPc2
a	a	k8xC
14	#num#	k4
kněží	kněz	k1gMnPc2
v	v	k7c6
11	#num#	k4
farnostech	farnost	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ordinářem	ordinář	k1gMnSc7
je	být	k5eAaImIp3nS
od	od	k7c2
15	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2012	#num#	k4
Harry	Harra	k1gMnSc2
Entwistle	Entwistle	k1gMnSc2
(	(	kIx(
<g/>
narozen	narozen	k2eAgInSc1d1
31	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
1940	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Římskokatolická	římskokatolický	k2eAgFnSc1d1
církev	církev	k1gFnSc1
v	v	k7c6
Austrálii	Austrálie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
anglikánské	anglikánský	k2eAgFnSc3d1
církvi	církev	k1gFnSc3
se	se	k3xPyFc4
hlásí	hlásit	k5eAaImIp3nS
17	#num#	k4
%	%	kIx~
obyvatelstva	obyvatelstvo	k1gNnSc2
(	(	kIx(
<g/>
asi	asi	k9
19	#num#	k4
%	%	kIx~
protestanti	protestant	k1gMnPc1
<g/>
,	,	kIx,
celkem	celkem	k6eAd1
včetně	včetně	k7c2
mormonů	mormon	k1gMnPc2
a	a	k8xC
jehovistů	jehovista	k1gMnPc2
<g/>
)	)	kIx)
a	a	k8xC
asi	asi	k9
3	#num#	k4
%	%	kIx~
jsou	být	k5eAaImIp3nP
pravoslavní	pravoslavný	k2eAgMnPc1d1
<g/>
,	,	kIx,
včetně	včetně	k7c2
např.	např.	kA
syrských	syrský	k2eAgMnPc2d1
jakobitů	jakobita	k1gMnPc2
(	(	kIx(
<g/>
miafyzit	miafyzit	k1gInSc1
<g/>
.	.	kIx.
vyznání	vyznání	k1gNnSc1
<g/>
)	)	kIx)
–	–	k?
patriarchální	patriarchální	k2eAgInSc4d1
vikariát	vikariát	k1gInSc4
–	–	k?
arcidiecéze	arcidiecéze	k1gFnSc2
Austrálie	Austrálie	k1gFnSc2
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
7	#num#	k4
farností	farnost	k1gFnPc2
a	a	k8xC
jednu	jeden	k4xCgFnSc4
farnost	farnost	k1gFnSc4
na	na	k7c6
Novém	nový	k2eAgInSc6d1
Zélandu	Zéland	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
nekřesťanským	křesťanský	k2eNgMnPc3d1
náboženstvím	náboženství	k1gNnSc7
se	se	k3xPyFc4
přihlásilo	přihlásit	k5eAaPmAgNnS
7	#num#	k4
%	%	kIx~
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
nejvíce	hodně	k6eAd3,k6eAd1
k	k	k7c3
buddhismu	buddhismus	k1gInSc2
a	a	k8xC
islámu	islám	k1gInSc2
<g/>
,	,	kIx,
22	#num#	k4
%	%	kIx~
obyvatel	obyvatel	k1gMnPc2
se	se	k3xPyFc4
označilo	označit	k5eAaPmAgNnS
jako	jako	k9
"	"	kIx"
<g/>
bez	bez	k7c2
vyznání	vyznání	k1gNnSc2
<g/>
"	"	kIx"
a	a	k8xC
10	#num#	k4
%	%	kIx~
odmítlo	odmítnout	k5eAaPmAgNnS
na	na	k7c4
otázku	otázka	k1gFnSc4
o	o	k7c6
svém	svůj	k3xOyFgNnSc6
vyznání	vyznání	k1gNnSc6
odpovědět	odpovědět	k5eAaPmF
(	(	kIx(
<g/>
vyznání	vyznání	k1gNnSc4
nezjištěno	zjištěn	k2eNgNnSc4d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
94	#num#	k4
<g/>
]	]	kIx)
Nedělní	nedělní	k2eAgFnPc1d1
bohoslužby	bohoslužba	k1gFnPc1
vykazují	vykazovat	k5eAaImIp3nP
(	(	kIx(
<g/>
pravidelnou	pravidelný	k2eAgFnSc4d1
<g/>
)	)	kIx)
návštěvu	návštěva	k1gFnSc4
okolo	okolo	k7c2
7,5	7,5	k4
%	%	kIx~
obyvatel	obyvatel	k1gMnPc2
(	(	kIx(
<g/>
polovina	polovina	k1gFnSc1
z	z	k7c2
toho	ten	k3xDgNnSc2
připadá	připadat	k5eAaPmIp3nS,k5eAaImIp3nS
na	na	k7c4
katolické	katolický	k2eAgFnPc4d1
bohoslužby	bohoslužba	k1gFnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Zdraví	zdraví	k1gNnSc1
</s>
<s>
Očekávaná	očekávaný	k2eAgFnSc1d1
(	(	kIx(
<g/>
či	či	k8xC
též	též	k9
střední	střední	k2eAgFnSc1d1
<g/>
)	)	kIx)
délka	délka	k1gFnSc1
života	život	k1gInSc2
(	(	kIx(
<g/>
dle	dle	k7c2
statistik	statistika	k1gFnPc2
z	z	k7c2
let	léto	k1gNnPc2
2014	#num#	k4
<g/>
–	–	k?
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
v	v	k7c6
Austrálii	Austrálie	k1gFnSc6
čtvrtá	čtvrtý	k4xOgFnSc1
nejvyšší	vysoký	k2eAgFnSc1d3
na	na	k7c6
světě	svět	k1gInSc6
u	u	k7c2
mužů	muž	k1gMnPc2
a	a	k8xC
třetí	třetí	k4xOgFnSc4
nejvyšší	vysoký	k2eAgFnSc4d3
u	u	k7c2
žen	žena	k1gFnPc2
(	(	kIx(
<g/>
přičemž	přičemž	k6eAd1
do	do	k7c2
statistiky	statistika	k1gFnSc2
OSN	OSN	kA
byl	být	k5eAaImAgInS
zařazen	zařadit	k5eAaPmNgInS
i	i	k9
Hongkong	Hongkong	k1gInSc1
<g/>
,	,	kIx,
z	z	k7c2
reálně	reálně	k6eAd1
suverénních	suverénní	k2eAgInPc2d1
států	stát	k1gInPc2
byly	být	k5eAaImAgFnP
před	před	k7c7
Austrálií	Austrálie	k1gFnSc7
u	u	k7c2
mužů	muž	k1gMnPc2
Japonsko	Japonsko	k1gNnSc4
a	a	k8xC
Island	Island	k1gInSc4
<g/>
,	,	kIx,
u	u	k7c2
žen	žena	k1gFnPc2
jen	jen	k9
Japonsko	Japonsko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
95	#num#	k4
<g/>
]	]	kIx)
Tato	tento	k3xDgFnSc1
očekávaná	očekávaný	k2eAgFnSc1d1
délka	délka	k1gFnSc1
byla	být	k5eAaImAgFnS
u	u	k7c2
mužů	muž	k1gMnPc2
80,4	80,4	k4
roku	rok	k1gInSc2
a	a	k8xC
u	u	k7c2
žen	žena	k1gFnPc2
84,6	84,6	k4
roku	rok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
má	mít	k5eAaImIp3nS
nejvyšší	vysoký	k2eAgInSc4d3
výskyt	výskyt	k1gInSc4
rakoviny	rakovina	k1gFnSc2
kůže	kůže	k1gFnSc2
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
96	#num#	k4
<g/>
]	]	kIx)
Pravděpodobnost	pravděpodobnost	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
Australan	Australan	k1gMnSc1
onemocní	onemocnět	k5eAaPmIp3nS
rakovinou	rakovina	k1gFnSc7
kůže	kůže	k1gFnSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
čtyřikrát	čtyřikrát	k6eAd1
vyšší	vysoký	k2eAgFnSc1d2
<g/>
,	,	kIx,
než	než	k8xS
že	že	k8xS
onemocní	onemocnět	k5eAaPmIp3nP
jiným	jiný	k2eAgInSc7d1
druhem	druh	k1gInSc7
rakoviny	rakovina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
příčinách	příčina	k1gFnPc6
se	se	k3xPyFc4
vede	vést	k5eAaImIp3nS
debata	debata	k1gFnSc1
<g/>
,	,	kIx,
hlavní	hlavní	k2eAgFnSc4d1
roli	role	k1gFnSc4
hraje	hrát	k5eAaImIp3nS
vysoké	vysoký	k2eAgNnSc1d1
vystavení	vystavení	k1gNnSc1
slunečnímu	sluneční	k2eAgNnSc3d1
záření	záření	k1gNnSc3
a	a	k8xC
možná	možná	k9
i	i	k9
tenčí	tenký	k2eAgFnSc1d2
ozónová	ozónový	k2eAgFnSc1d1
vrstva	vrstva	k1gFnSc1
nad	nad	k7c7
Austrálií	Austrálie	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejčastější	častý	k2eAgFnSc7d3
lidmi	člověk	k1gMnPc7
způsobenou	způsobený	k2eAgFnSc7d1
příčinou	příčina	k1gFnSc7
onemocnění	onemocnění	k1gNnSc2
a	a	k8xC
smrti	smrt	k1gFnSc2
je	být	k5eAaImIp3nS
kouření	kouření	k1gNnSc1
cigaret	cigareta	k1gFnPc2
<g/>
,	,	kIx,
odpovídá	odpovídat	k5eAaImIp3nS
za	za	k7c4
7,8	7,8	k4
<g/>
%	%	kIx~
celkové	celkový	k2eAgFnSc2d1
úmrtnosti	úmrtnost	k1gFnSc2
a	a	k8xC
nemocí	nemoc	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
97	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
druhém	druhý	k4xOgInSc6
místě	místo	k1gNnSc6
je	být	k5eAaImIp3nS
hypertenze	hypertenze	k1gFnSc1
způsobená	způsobený	k2eAgFnSc1d1
životním	životní	k2eAgInSc7d1
stylem	styl	k1gInSc7
(	(	kIx(
<g/>
7,6	7,6	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
a	a	k8xC
na	na	k7c6
třetím	třetí	k4xOgInSc6
obezita	obezita	k1gFnSc1
(	(	kIx(
<g/>
7,5	7,5	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Austrálie	Austrálie	k1gFnSc1
zaujímá	zaujímat	k5eAaImIp3nS
35	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
na	na	k7c6
světě	svět	k1gInSc6
a	a	k8xC
je	být	k5eAaImIp3nS
na	na	k7c6
vrcholu	vrchol	k1gInSc6
vyspělých	vyspělý	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
v	v	k7c6
podílu	podíl	k1gInSc6
obézních	obézní	k2eAgMnPc2d1
dospělých	dospělí	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
98	#num#	k4
<g/>
]	]	kIx)
Téměř	téměř	k6eAd1
dvě	dva	k4xCgFnPc4
třetiny	třetina	k1gFnPc1
(	(	kIx(
<g/>
63	#num#	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
její	její	k3xOp3gFnSc2
dospělé	dospělý	k2eAgFnSc2d1
populace	populace	k1gFnSc2
trpí	trpět	k5eAaImIp3nS
nadváhou	nadváha	k1gFnSc7
nebo	nebo	k8xC
obezitou	obezita	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Celkové	celkový	k2eAgInPc1d1
výdaje	výdaj	k1gInPc1
na	na	k7c6
zdravotnictví	zdravotnictví	k1gNnSc6
(	(	kIx(
<g/>
včetně	včetně	k7c2
výdajů	výdaj	k1gInPc2
soukromého	soukromý	k2eAgInSc2d1
sektoru	sektor	k1gInSc2
<g/>
)	)	kIx)
se	se	k3xPyFc4
pohybují	pohybovat	k5eAaImIp3nP
kolem	kolem	k7c2
9,8	9,8	k4
<g/>
%	%	kIx~
HDP	HDP	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
99	#num#	k4
<g/>
]	]	kIx)
Austrálie	Austrálie	k1gFnSc2
zavedla	zavést	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1975	#num#	k4
systém	systém	k1gInSc1
univerzálního	univerzální	k2eAgNnSc2d1
zdravotního	zdravotní	k2eAgNnSc2d1
pojištění	pojištění	k1gNnSc2
evropského	evropský	k2eAgInSc2d1
typu	typ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
znám	znám	k2eAgMnSc1d1
jako	jako	k8xC,k8xS
Medicare	Medicar	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Federální	federální	k2eAgInPc1d1
státy	stát	k1gInPc1
spravují	spravovat	k5eAaImIp3nP
nemocnice	nemocnice	k1gFnPc4
a	a	k8xC
související	související	k2eAgFnPc4d1
ambulantní	ambulantní	k2eAgFnPc4d1
služby	služba	k1gFnPc4
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
federální	federální	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
financuje	financovat	k5eAaBmIp3nS
program	program	k1gInSc4
subvencování	subvencování	k1gNnSc2
nákladů	náklad	k1gInPc2
na	na	k7c4
léky	lék	k1gInPc4
a	a	k8xC
obecnou	obecný	k2eAgFnSc4d1
lékařskou	lékařský	k2eAgFnSc4d1
praxi	praxe	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Kultura	kultura	k1gFnSc1
</s>
<s>
Opera	opera	k1gFnSc1
v	v	k7c6
Sydney	Sydney	k1gNnSc6
</s>
<s>
AC	AC	kA
<g/>
/	/	kIx~
<g/>
DC	DC	kA
</s>
<s>
Nejoceňovanějším	oceňovaný	k2eAgMnSc7d3
australským	australský	k2eAgMnSc7d1
architektem	architekt	k1gMnSc7
je	být	k5eAaImIp3nS
nositel	nositel	k1gMnSc1
prestižní	prestižní	k2eAgFnSc2d1
Pritzkerovy	Pritzkerův	k2eAgFnSc2d1
ceny	cena	k1gFnSc2
Glenn	Glenn	k1gMnSc1
Murcutt	Murcutt	k1gMnSc1
(	(	kIx(
<g/>
narozen	narozen	k2eAgMnSc1d1
v	v	k7c6
Anglii	Anglie	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
100	#num#	k4
<g/>
]	]	kIx)
Dvě	dva	k4xCgFnPc1
významné	významný	k2eAgFnPc1d1
australské	australský	k2eAgFnPc1d1
stavby	stavba	k1gFnPc1
byly	být	k5eAaImAgFnP
zapsány	zapsat	k5eAaPmNgFnP
na	na	k7c4
seznam	seznam	k1gInSc4
světového	světový	k2eAgNnSc2d1
kulturního	kulturní	k2eAgNnSc2d1
dědictví	dědictví	k1gNnSc2
UNESCO	UNESCO	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
je	být	k5eAaImIp3nS
budova	budova	k1gFnSc1
Opery	opera	k1gFnSc2
v	v	k7c6
Sydney	Sydney	k1gNnSc6
<g/>
,	,	kIx,
patrně	patrně	k6eAd1
nejikoničtější	ikonický	k2eAgFnSc1d3
stavba	stavba	k1gFnSc1
v	v	k7c6
zemi	zem	k1gFnSc6
<g/>
,	,	kIx,
již	již	k6eAd1
dostavili	dostavit	k5eAaPmAgMnP
roku	rok	k1gInSc2
1973	#num#	k4
podle	podle	k7c2
návrhu	návrh	k1gInSc2
dánského	dánský	k2eAgMnSc4d1
architekta	architekt	k1gMnSc4
Jorna	Jorn	k1gMnSc4
Utzona	Utzon	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhou	druhý	k4xOgFnSc4
je	být	k5eAaImIp3nS
Královská	královský	k2eAgFnSc1d1
výstavní	výstavní	k2eAgFnSc1d1
budova	budova	k1gFnSc1
v	v	k7c6
Melbourne	Melbourne	k1gNnSc6
navržená	navržený	k2eAgFnSc1d1
pro	pro	k7c4
mezinárodní	mezinárodní	k2eAgFnPc4d1
výstavy	výstava	k1gFnPc4
v	v	k7c6
letech	léto	k1gNnPc6
1880	#num#	k4
a	a	k8xC
1888	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejím	její	k3xOp3gMnSc7
autorem	autor	k1gMnSc7
byl	být	k5eAaImAgMnS
Joseph	Joseph	k1gMnSc1
Reed	Reed	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krom	krom	k7c2
toho	ten	k3xDgNnSc2
jsou	být	k5eAaImIp3nP
na	na	k7c6
seznamu	seznam	k1gInSc6
australské	australský	k2eAgFnSc2d1
historické	historický	k2eAgFnSc2d1
káznice	káznice	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
101	#num#	k4
<g/>
]	]	kIx)
Unikátním	unikátní	k2eAgInSc7d1
urbanistickým	urbanistický	k2eAgInSc7d1
celkem	celek	k1gInSc7
je	být	k5eAaImIp3nS
hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
Canberra	Canberr	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
na	na	k7c6
začátku	začátek	k1gInSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
vyprojektoval	vyprojektovat	k5eAaPmAgMnS
americký	americký	k2eAgMnSc1d1
architekt	architekt	k1gMnSc1
Walter	Walter	k1gMnSc1
Burley	Burlea	k1gFnSc2
Griffin	Griffin	k2eAgMnSc1d1
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
vzorové	vzorový	k2eAgNnSc4d1
město	město	k1gNnSc4
prérijní	prérijní	k2eAgFnSc2d1
architektury	architektura	k1gFnSc2
<g/>
,	,	kIx,
plné	plný	k2eAgFnSc2d1
zeleně	zeleň	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
102	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nejvýznamnějším	významný	k2eAgMnSc7d3
australským	australský	k2eAgMnSc7d1
malířem	malíř	k1gMnSc7
byl	být	k5eAaImAgInS
Sidney	Sidnea	k1gFnSc2
Nolan	Nolan	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
se	se	k3xPyFc4
rád	rád	k6eAd1
věnoval	věnovat	k5eAaImAgMnS,k5eAaPmAgMnS
australské	australský	k2eAgFnSc3d1
historii	historie	k1gFnSc3
a	a	k8xC
přispěl	přispět	k5eAaPmAgMnS
rovněž	rovněž	k9
k	k	k7c3
legendarizaci	legendarizace	k1gFnSc3
zločince	zločinec	k1gMnSc2
Neda	Nedus	k1gMnSc2
Kellyho	Kelly	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
103	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
2006	#num#	k4
byl	být	k5eAaImAgInS
australským	australský	k2eAgMnSc7d1
občanem	občan	k1gMnSc7
i	i	k8xC
spisovatel	spisovatel	k1gMnSc1
a	a	k8xC
nositel	nositel	k1gMnSc1
Nobelovy	Nobelův	k2eAgFnSc2d1
ceny	cena	k1gFnSc2
za	za	k7c4
literaturu	literatura	k1gFnSc4
John	John	k1gMnSc1
Maxwell	maxwell	k1gInSc4
Coetzee	Coetze	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalším	další	k2eAgMnSc7d1
literárním	literární	k2eAgMnSc7d1
nobelistou	nobelista	k1gMnSc7
byl	být	k5eAaImAgMnS
Patrick	Patrick	k1gMnSc1
White	Whit	k1gInSc5
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
104	#num#	k4
<g/>
]	]	kIx)
Významnou	významný	k2eAgFnSc7d1
představitelkou	představitelka	k1gFnSc7
feminismu	feminismus	k1gInSc2
byla	být	k5eAaImAgFnS
spisovatelka	spisovatelka	k1gFnSc1
Germaine	Germain	k1gInSc5
Greerová	Greerová	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
klasikům	klasik	k1gMnPc3
australské	australský	k2eAgFnSc2d1
literatury	literatura	k1gFnSc2
patří	patřit	k5eAaImIp3nS
Pamela	Pamela	k1gFnSc1
Lyndon	Lyndon	k1gInSc1
Traversová	Traversová	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
stvořila	stvořit	k5eAaPmAgFnS
postavu	postava	k1gFnSc4
chůvy	chůva	k1gFnSc2
Mary	Mary	k1gFnSc2
Poppinsové	Poppinsová	k1gFnSc2
<g/>
,	,	kIx,
populární	populární	k2eAgMnSc1d1
zvláště	zvláště	k6eAd1
v	v	k7c6
anglosaském	anglosaský	k2eAgInSc6d1
světě	svět	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
105	#num#	k4
<g/>
]	]	kIx)
Ke	k	k7c3
známým	známý	k2eAgMnPc3d1
autorům	autor	k1gMnPc3
patří	patřit	k5eAaImIp3nS
i	i	k9
Colleen	Colleen	k1gInSc4
McCulloughová	McCulloughová	k1gFnSc1
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
Carey	Carea	k1gFnSc2
<g/>
,	,	kIx,
Richard	Richard	k1gMnSc1
Flanagan	Flanagan	k1gMnSc1
<g/>
,	,	kIx,
nebo	nebo	k8xC
autor	autor	k1gMnSc1
Schindlerova	Schindlerův	k2eAgInSc2d1
seznamu	seznam	k1gInSc2
Thomas	Thomas	k1gMnSc1
Keneally	Kenealla	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
žánru	žánr	k1gInSc6
sci-fi	sci-fi	k1gNnPc2
se	se	k3xPyFc4
prosadil	prosadit	k5eAaPmAgMnS
Greg	Greg	k1gMnSc1
Egan	Egan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Filmový	filmový	k2eAgMnSc1d1
scenárista	scenárista	k1gMnSc1
John	John	k1gMnSc1
Farrow	Farrow	k1gFnSc4
získal	získat	k5eAaPmAgMnS
roku	rok	k1gInSc2
1957	#num#	k4
Oscara	Oscara	k1gFnSc1
za	za	k7c4
scénář	scénář	k1gInSc4
k	k	k7c3
filmu	film	k1gInSc3
Cesta	cesta	k1gFnSc1
kolem	kolem	k7c2
světa	svět	k1gInSc2
za	za	k7c4
80	#num#	k4
dní	den	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k9
mnozí	mnohý	k2eAgMnPc1d1
australští	australský	k2eAgMnPc1d1
herci	herec	k1gMnPc1
získali	získat	k5eAaPmAgMnP
světový	světový	k2eAgInSc4d1
věhlas	věhlas	k1gInSc4
v	v	k7c6
Hollywodu	Hollywod	k1gInSc6
<g/>
,	,	kIx,
jako	jako	k9
Errol	Errola	k1gFnPc2
Flynn	Flynn	k1gMnSc1
<g/>
,	,	kIx,
Russell	Russell	k1gMnSc1
Crowe	Crowe	k1gFnSc1
<g/>
,	,	kIx,
Nicole	Nicole	k1gFnSc1
Kidmanová	Kidmanová	k1gFnSc1
<g/>
,	,	kIx,
Cate	Cate	k1gFnSc1
Blanchettová	Blanchettová	k1gFnSc1
<g/>
,	,	kIx,
Naomi	Nao	k1gFnPc7
Wattsová	Wattsová	k1gFnSc1
či	či	k8xC
Heath	Heath	k1gMnSc1
Ledger	Ledger	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
sérii	série	k1gFnSc6
X-men	X-mna	k1gFnPc2
se	se	k3xPyFc4
objevil	objevit	k5eAaPmAgMnS
Hugh	Hugh	k1gMnSc1
Jackman	Jackman	k1gMnSc1
<g/>
,	,	kIx,
ve	v	k7c6
filmech	film	k1gInPc6
Thor	Thora	k1gFnPc2
či	či	k8xC
Avengers	Avengersa	k1gFnPc2
Chris	Chris	k1gFnSc2
Hemsworth	Hemsworth	k1gInSc1
<g/>
,	,	kIx,
ve	v	k7c6
Spielbergově	Spielbergův	k2eAgInSc6d1
snímku	snímek	k1gInSc6
Mnichov	Mnichov	k1gInSc1
Eric	Eric	k1gInSc1
Bana	Banus	k1gMnSc2
a	a	k8xC
ve	v	k7c6
Stroji	stroj	k1gInSc6
času	čas	k1gInSc2
hrál	hrát	k5eAaImAgMnS
Guy	Guy	k1gMnSc1
Pearce	Pearce	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Australanem	Australan	k1gMnSc7
byl	být	k5eAaImAgMnS
i	i	k9
jeden	jeden	k4xCgMnSc1
z	z	k7c2
představitelů	představitel	k1gMnPc2
Jamese	Jamese	k1gFnSc2
Bonda	Bond	k1gMnSc2
George	Georg	k1gMnSc2
Lazenby	Lazenba	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
televizním	televizní	k2eAgInSc6d1
seriálu	seriál	k1gInSc6
Mentalista	Mentalista	k1gMnSc1
účinkoval	účinkovat	k5eAaImAgMnS
Simon	Simon	k1gMnSc1
Baker	Baker	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Televize	televize	k1gFnSc1
zajistila	zajistit	k5eAaPmAgFnS
celosvětovou	celosvětový	k2eAgFnSc4d1
proslulost	proslulost	k1gFnSc4
i	i	k8xC
"	"	kIx"
<g/>
lovci	lovec	k1gMnSc3
krokodýlů	krokodýl	k1gMnPc2
<g/>
"	"	kIx"
Stevu	Steve	k1gMnSc3
Irwinovi	Irwin	k1gMnSc3
či	či	k8xC
šiřiteli	šiřitel	k1gMnSc3
křesťanství	křesťanství	k1gNnSc2
Nicku	nicka	k1gFnSc4
Vujicicovi	Vujicic	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
režisérů	režisér	k1gMnPc2
se	se	k3xPyFc4
v	v	k7c6
Hollywoodu	Hollywood	k1gInSc6
prosadili	prosadit	k5eAaPmAgMnP
Baz	Baz	k1gMnSc1
Luhrmann	Luhrmann	k1gMnSc1
<g/>
,	,	kIx,
George	George	k1gFnSc1
Miller	Miller	k1gMnSc1
či	či	k8xC
Bruce	Bruce	k1gMnSc1
Beresford	Beresford	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgFnSc7d1
postavou	postava	k1gFnSc7
tzv.	tzv.	kA
australské	australský	k2eAgFnSc2d1
nové	nový	k2eAgFnSc2d1
vlny	vlna	k1gFnSc2
byl	být	k5eAaImAgMnS
režisér	režisér	k1gMnSc1
Peter	Peter	k1gMnSc1
Weir	Weir	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
tvůrce	tvůrce	k1gMnSc1
hudebních	hudební	k2eAgInPc2d1
videoklipů	videoklip	k1gInPc2
se	se	k3xPyFc4
prosadil	prosadit	k5eAaPmAgMnS
Russell	Russell	k1gMnSc1
Mulcahy	Mulcaha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
australské	australský	k2eAgInPc4d1
modelky	modelek	k1gInPc4
se	se	k3xPyFc4
řadí	řadit	k5eAaImIp3nS
Miranda	Miranda	k1gFnSc1
Kerrová	Kerrová	k1gFnSc1
nebo	nebo	k8xC
Elle	Elle	k1gFnSc1
Macphersonová	Macphersonová	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c4
známé	známý	k2eAgMnPc4d1
australské	australský	k2eAgMnPc4d1
hudební	hudební	k2eAgMnPc4d1
skladatele	skladatel	k1gMnPc4
se	se	k3xPyFc4
zařadil	zařadit	k5eAaPmAgMnS
Percy	Percy	k1gInPc4
Grainger	Graingra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
oblasti	oblast	k1gFnSc6
vážné	vážný	k2eAgFnSc2d1
hudby	hudba	k1gFnSc2
si	se	k3xPyFc3
získaly	získat	k5eAaPmAgInP
kredit	kredit	k1gInSc4
operní	operní	k2eAgFnSc2d1
pěvkyně	pěvkyně	k1gFnSc2
Joan	Joan	k1gInSc4
Sutherlandová	Sutherlandový	k2eAgFnSc1d1
a	a	k8xC
Nellie	Nellie	k1gFnSc1
Melba	Melba	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Australané	Australan	k1gMnPc1
se	se	k3xPyFc4
podobně	podobně	k6eAd1
jako	jako	k9
jiní	jiný	k2eAgMnPc1d1
umělci	umělec	k1gMnPc1
z	z	k7c2
anglofonního	anglofonní	k2eAgInSc2d1
prostoru	prostor	k1gInSc2
poměrně	poměrně	k6eAd1
hodně	hodně	k6eAd1
prosazují	prosazovat	k5eAaImIp3nP
v	v	k7c6
globální	globální	k2eAgFnSc6d1
popové	popový	k2eAgFnSc3d1
a	a	k8xC
rockové	rockový	k2eAgFnSc3d1
hudbě	hudba	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
australské	australský	k2eAgFnPc4d1
kapely	kapela	k1gFnPc4
patří	patřit	k5eAaImIp3nS
AC	AC	kA
<g/>
/	/	kIx~
<g/>
DC	DC	kA
(	(	kIx(
<g/>
frontman	frontman	k1gMnSc1
Bon	bona	k1gFnPc2
Scott	Scott	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Bee	Bee	k1gMnSc1
Gees	Geesa	k1gFnPc2
<g/>
,	,	kIx,
5	#num#	k4
Seconds	Seconds	k1gInSc1
of	of	k?
Summer	Summer	k1gInSc1
<g/>
,	,	kIx,
INXS	INXS	kA
(	(	kIx(
<g/>
Michael	Michael	k1gMnSc1
Hutchence	Hutchence	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Dead	Dead	k1gInSc1
Can	Can	k1gFnSc2
Dance	Danka	k1gFnSc3
(	(	kIx(
<g/>
Lisa	Lisa	k1gFnSc1
Gerrard	Gerrard	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Savage	Savage	k1gNnSc1
Garden	Gardna	k1gFnPc2
(	(	kIx(
<g/>
Darren	Darrna	k1gFnPc2
Hayes	Hayes	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Men	Men	k1gMnSc1
at	at	k?
Work	Work	k1gMnSc1
či	či	k8xC
Midnight	Midnight	k1gMnSc1
Oil	Oil	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
sólových	sólový	k2eAgMnPc2d1
umělců	umělec	k1gMnPc2
Kylie	Kylie	k1gFnSc2
Minogue	Minogue	k1gFnSc1
<g/>
,	,	kIx,
Nick	Nick	k1gInSc1
Cave	Cave	k1gFnSc1
<g/>
,	,	kIx,
Natalie	Natalie	k1gFnSc1
Imbruglia	Imbruglia	k1gFnSc1
<g/>
,	,	kIx,
Sia	Sia	k1gMnSc1
či	či	k8xC
Jason	Jason	k1gMnSc1
Donovan	Donovan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olivia	Olivia	k1gFnSc1
Newton-John	Newton-Johna	k1gFnPc2
(	(	kIx(
<g/>
narozená	narozený	k2eAgFnSc1d1
v	v	k7c6
Anglii	Anglie	k1gFnSc6
<g/>
)	)	kIx)
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
známou	známá	k1gFnSc4
ve	v	k7c6
filmovém	filmový	k2eAgInSc6d1
muzikálu	muzikál	k1gInSc6
Pomáda	pomáda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Michael	Michael	k1gMnSc1
Balzary	Balzara	k1gFnSc2
je	být	k5eAaImIp3nS
kytaristou	kytarista	k1gMnSc7
americké	americký	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
Red	Red	k1gFnSc2
Hot	hot	k0
Chili	Chil	k1gMnSc3
Peppers	Peppersa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
akustický	akustický	k2eAgMnSc1d1
kytarista	kytarista	k1gMnSc1
proslul	proslout	k5eAaPmAgMnS
Tommy	Tomma	k1gFnPc4
Emmanuel	Emmanuel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Kuchyně	kuchyně	k1gFnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Australská	australský	k2eAgFnSc1d1
kuchyně	kuchyně	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Australská	australský	k2eAgFnSc1d1
vánoční	vánoční	k2eAgFnSc1d1
tabule	tabule	k1gFnSc1
<g/>
,	,	kIx,
jíž	jenž	k3xRgFnSc3
dominuje	dominovat	k5eAaImIp3nS
dort	dort	k1gInSc1
pavlova	pavlův	k2eAgInSc2d1
</s>
<s>
Australská	australský	k2eAgFnSc1d1
kuchyně	kuchyně	k1gFnSc1
vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
kuchyně	kuchyně	k1gFnSc2
anglické	anglický	k2eAgFnSc2d1
a	a	k8xC
je	být	k5eAaImIp3nS
podobná	podobný	k2eAgFnSc1d1
kuchyni	kuchyně	k1gFnSc3
novozélandské	novozélandský	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
ovlivněna	ovlivnit	k5eAaPmNgFnS
místními	místní	k2eAgFnPc7d1
surovinami	surovina	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Austrálii	Austrálie	k1gFnSc6
je	být	k5eAaImIp3nS
populární	populární	k2eAgNnSc4d1
grilování	grilování	k1gNnSc4
<g/>
,	,	kIx,
protože	protože	k8xS
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
na	na	k7c4
to	ten	k3xDgNnSc4
vhodné	vhodný	k2eAgNnSc4d1
podnebí	podnebí	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oblíbená	oblíbený	k2eAgNnPc1d1
jsou	být	k5eAaImIp3nP
také	také	k9
jídla	jídlo	k1gNnPc4
domorodých	domorodý	k2eAgInPc2d1
Aboridžínců	Aboridžínec	k1gInPc2
<g/>
,	,	kIx,
tzv.	tzv.	kA
bushfood	bushfood	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
O	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
jídlo	jídlo	k1gNnSc1
je	být	k5eAaImIp3nS
australským	australský	k2eAgNnSc7d1
jídlem	jídlo	k1gNnSc7
národním	národní	k2eAgNnSc7d1
<g/>
,	,	kIx,
se	se	k3xPyFc4
vedou	vést	k5eAaImIp3nP
spory	spor	k1gInPc1
<g/>
,	,	kIx,
ale	ale	k8xC
mnozí	mnohý	k2eAgMnPc1d1
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
například	například	k6eAd1
bývalý	bývalý	k2eAgMnSc1d1
premiér	premiér	k1gMnSc1
Nového	Nového	k2eAgInSc2d1
Jižního	jižní	k2eAgInSc2d1
Walesu	Wales	k1gInSc2
Bob	Bob	k1gMnSc1
Carre	Carr	k1gMnSc5
<g/>
,	,	kIx,
za	za	k7c4
něj	on	k3xPp3gMnSc4
považují	považovat	k5eAaImIp3nP
masový	masový	k2eAgInSc4d1
koláč	koláč	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
106	#num#	k4
<g/>
]	]	kIx)
Za	za	k7c4
své	svůj	k3xOyFgNnSc4
národní	národní	k2eAgNnSc4d1
jídlo	jídlo	k1gNnSc4
ho	on	k3xPp3gMnSc4
ale	ale	k8xC
považují	považovat	k5eAaImIp3nP
i	i	k9
Novozélanďané	Novozélanďan	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zvláštností	zvláštnost	k1gFnPc2
australské	australský	k2eAgFnSc2d1
kuchyně	kuchyně	k1gFnSc2
je	být	k5eAaImIp3nS
také	také	k9
Vegemite	Vegemit	k1gInSc5
<g/>
,	,	kIx,
tmavě	tmavě	k6eAd1
hnědá	hnědý	k2eAgFnSc1d1
pomazánka	pomazánka	k1gFnSc1
s	s	k7c7
kvasnicovým	kvasnicový	k2eAgInSc7d1
extraktem	extrakt	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
obdobou	obdoba	k1gFnSc7
pomazánky	pomazánka	k1gFnSc2
Marmite	Marmit	k1gInSc5
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
oblíbená	oblíbený	k2eAgFnSc1d1
v	v	k7c6
Británii	Británie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
původně	původně	k6eAd1
oblíbenou	oblíbený	k2eAgFnSc7d1
i	i	k8xC
v	v	k7c6
Austrálii	Austrálie	k1gFnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1919	#num#	k4
došlo	dojít	k5eAaPmAgNnS
v	v	k7c6
důsledku	důsledek	k1gInSc6
poválečných	poválečný	k2eAgFnPc2d1
potíží	potíž	k1gFnPc2
k	k	k7c3
narušení	narušení	k1gNnSc3
jejího	její	k3xOp3gInSc2
dovozu	dovoz	k1gInSc2
z	z	k7c2
Británie	Británie	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
australská	australský	k2eAgFnSc1d1
firma	firma	k1gFnSc1
Fred	Fred	k1gMnSc1
Walker	Walker	k1gMnSc1
&	&	k?
Co	co	k9
vyvinula	vyvinout	k5eAaPmAgFnS
vlastní	vlastní	k2eAgFnSc4d1
kvasnicovou	kvasnicový	k2eAgFnSc4d1
pomazánku	pomazánka	k1gFnSc4
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
s	s	k7c7
výtažkem	výtažek	k1gInSc7
z	z	k7c2
cibule	cibule	k1gFnSc2
a	a	k8xC
celeru	celer	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
nový	nový	k2eAgInSc1d1
produkt	produkt	k1gInSc1
spatřil	spatřit	k5eAaPmAgInS
světlo	světlo	k1gNnSc4
světa	svět	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1922	#num#	k4
a	a	k8xC
stal	stát	k5eAaPmAgInS
se	se	k3xPyFc4
ikonou	ikona	k1gFnSc7
australské	australský	k2eAgFnSc2d1
gastronomie	gastronomie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
nejoblíbenějším	oblíbený	k2eAgInPc3d3
dezertům	dezert	k1gInPc3
patří	patřit	k5eAaImIp3nS
lamington	lamington	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
máslový	máslový	k2eAgInSc1d1
nebo	nebo	k8xC
piškotový	piškotový	k2eAgInSc1d1
střed	střed	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
obalený	obalený	k2eAgMnSc1d1
vrstvou	vrstva	k1gFnSc7
čokolády	čokoláda	k1gFnSc2
a	a	k8xC
kokosu	kokos	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Častá	častý	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
i	i	k9
vrstva	vrstva	k1gFnSc1
krému	krém	k1gInSc2
nebo	nebo	k8xC
jahodového	jahodový	k2eAgInSc2d1
džemu	džem	k1gInSc2
uprostřed	uprostřed	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zvláštní	zvláštní	k2eAgFnSc4d1
roli	role	k1gFnSc4
v	v	k7c6
australské	australský	k2eAgFnSc6d1
kultuře	kultura	k1gFnSc6
má	mít	k5eAaImIp3nS
také	také	k9
dort	dort	k1gInSc1
zvaný	zvaný	k2eAgInSc1d1
pavlova	pavlův	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
z	z	k7c2
pečeného	pečený	k2eAgInSc2d1
krému	krém	k1gInSc2
ze	z	k7c2
šlehaných	šlehaný	k2eAgInPc2d1
bílků	bílek	k1gInPc2
<g/>
,	,	kIx,
zdobený	zdobený	k2eAgInSc1d1
šlehačkou	šlehačka	k1gFnSc7
a	a	k8xC
ovocem	ovoce	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dort	dort	k1gInSc1
je	být	k5eAaImIp3nS
pojmenovaný	pojmenovaný	k2eAgMnSc1d1
na	na	k7c4
počest	počest	k1gFnSc4
ruské	ruský	k2eAgFnSc2d1
primabaleríny	primabalerína	k1gFnSc2
Anny	Anna	k1gFnSc2
Pavlovové	Pavlovová	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
v	v	k7c6
letech	léto	k1gNnPc6
1926	#num#	k4
a	a	k8xC
1929	#num#	k4
navštívila	navštívit	k5eAaPmAgFnS
Austrálii	Austrálie	k1gFnSc4
a	a	k8xC
Nový	nový	k2eAgInSc4d1
Zéland	Zéland	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
původ	původ	k1gInSc4
tohoto	tento	k3xDgInSc2
dortu	dort	k1gInSc2
se	se	k3xPyFc4
oba	dva	k4xCgInPc1
státy	stát	k1gInPc1
přou	přít	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každopádně	každopádně	k6eAd1
v	v	k7c6
Austrálii	Austrálie	k1gFnSc6
získal	získat	k5eAaPmAgInS
charakter	charakter	k1gInSc4
jídla	jídlo	k1gNnSc2
vánočního	vánoční	k2eAgNnSc2d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
107	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Králem	Král	k1gMnSc7
rychlého	rychlý	k2eAgNnSc2d1
a	a	k8xC
pouličního	pouliční	k2eAgNnSc2d1
občerstvení	občerstvení	k1gNnSc2
je	být	k5eAaImIp3nS
v	v	k7c6
Austrálii	Austrálie	k1gFnSc6
Chiko	Chiko	k1gNnSc4
Roll	Rolla	k1gFnPc2
<g/>
,	,	kIx,
drůbeží	drůbeží	k2eAgFnSc1d1
rolka	rolka	k1gFnSc1
inspirovaná	inspirovaný	k2eAgFnSc1d1
čínskou	čínský	k2eAgFnSc7d1
kuchyní	kuchyně	k1gFnSc7
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
roku	rok	k1gInSc2
1951	#num#	k4
vymyslel	vymyslet	k5eAaPmAgMnS
Frank	Frank	k1gMnSc1
McEncroe	McEncro	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
108	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
má	mít	k5eAaImIp3nS
také	také	k9
silnou	silný	k2eAgFnSc4d1
kávovou	kávový	k2eAgFnSc4d1
kulturu	kultura	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeden	jeden	k4xCgInSc1
způsob	způsob	k1gInSc1
přípravy	příprava	k1gFnSc2
kávy	káva	k1gFnSc2
se	se	k3xPyFc4
zde	zde	k6eAd1
také	také	k9
zrodil	zrodit	k5eAaPmAgInS
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc4
tzv.	tzv.	kA
flat	flat	k1gInSc4
white	white	k5eAaPmIp2nP
<g/>
,	,	kIx,
tedy	tedy	k8xC
espresso	espressa	k1gFnSc5
s	s	k7c7
velmi	velmi	k6eAd1
tenkou	tenký	k2eAgFnSc7d1
vrstvou	vrstva	k1gFnSc7
pěny	pěna	k1gFnSc2
z	z	k7c2
mléka	mléko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzniklo	vzniknout	k5eAaPmAgNnS
patrně	patrně	k6eAd1
v	v	k7c6
Sydney	Sydney	k1gNnSc6
v	v	k7c6
80	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
<g/>
,	,	kIx,
byť	byť	k8xS
jako	jako	k9
v	v	k7c6
řadě	řada	k1gFnSc6
jiných	jiný	k2eAgInPc2d1
podobných	podobný	k2eAgInPc2d1
případů	případ	k1gInPc2
se	se	k3xPyFc4
o	o	k7c4
vynález	vynález	k1gInSc4
přou	přít	k5eAaImIp3nP
Australané	Australan	k1gMnPc1
s	s	k7c7
Novozélanďany	Novozélanďan	k1gMnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
109	#num#	k4
<g/>
]	]	kIx)
Popularita	popularita	k1gFnSc1
flat	flata	k1gFnPc2
white	white	k5eAaPmIp2nP
vzrostla	vzrůst	k5eAaPmAgFnS
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k9
ho	on	k3xPp3gMnSc4
do	do	k7c2
své	svůj	k3xOyFgFnSc2
nabídky	nabídka	k1gFnSc2
zařadil	zařadit	k5eAaPmAgInS
globální	globální	k2eAgInSc1d1
fast	fast	k2eAgInSc1d1
foodový	foodový	k2eAgInSc1d1
řetězec	řetězec	k1gInSc1
McDonald	McDonalda	k1gFnPc2
<g/>
'	'	kIx"
<g/>
s.	s.	k?
</s>
<s>
Věda	věda	k1gFnSc1
</s>
<s>
Nobelista	Nobelista	k1gMnSc1
John	John	k1gMnSc1
Carew	Carew	k1gMnSc1
Eccles	Eccles	k1gMnSc1
</s>
<s>
Nobelovu	Nobelův	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
za	za	k7c4
fyziku	fyzika	k1gFnSc4
získali	získat	k5eAaPmAgMnP
dva	dva	k4xCgMnPc1
rodilí	rodilý	k2eAgMnPc1d1
Australané	Australan	k1gMnPc1
William	William	k1gInSc4
Lawrence	Lawrenka	k1gFnSc3
Bragg	Bragg	k1gInSc1
(	(	kIx(
<g/>
též	též	k9
občan	občan	k1gMnSc1
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
Alexandr	Alexandr	k1gMnSc1
Michajlovič	Michajlovič	k1gMnSc1
Prochorov	Prochorov	k1gInSc1
(	(	kIx(
<g/>
též	též	k9
občan	občan	k1gMnSc1
SSSR	SSSR	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
ji	on	k3xPp3gFnSc4
získal	získat	k5eAaPmAgMnS
naturalizovaný	naturalizovaný	k2eAgMnSc1d1
Australan	Australan	k1gMnSc1
Brian	Brian	k1gMnSc1
Schmidt	Schmidt	k1gMnSc1
(	(	kIx(
<g/>
narozen	narozen	k2eAgInSc1d1
v	v	k7c6
USA	USA	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nobelovu	Nobelův	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
za	za	k7c4
chemii	chemie	k1gFnSc4
získal	získat	k5eAaPmAgMnS
John	John	k1gMnSc1
Cornforth	Cornforth	k1gMnSc1
(	(	kIx(
<g/>
též	též	k9
občan	občan	k1gMnSc1
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvíce	hodně	k6eAd3,k6eAd1
Australané	Australan	k1gMnPc1
uspěli	uspět	k5eAaPmAgMnP
při	při	k7c6
udílení	udílení	k1gNnSc6
Nobelových	Nobelových	k2eAgFnPc2d1
cen	cena	k1gFnPc2
za	za	k7c4
fyziologii	fyziologie	k1gFnSc4
nebo	nebo	k8xC
lékařství	lékařství	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
australským	australský	k2eAgMnPc3d1
laureátům	laureát	k1gMnPc3
patří	patřit	k5eAaImIp3nS
Elizabeth	Elizabeth	k1gFnSc1
Blackburnová	Blackburnová	k1gFnSc1
(	(	kIx(
<g/>
též	též	k9
občanka	občanka	k1gFnSc1
USA	USA	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Barry	Barra	k1gFnPc4
Marshall	Marshalla	k1gFnPc2
<g/>
,	,	kIx,
Robin	Robina	k1gFnPc2
Warren	Warrna	k1gFnPc2
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
C.	C.	kA
Doherty	Dohert	k1gInPc1
<g/>
,	,	kIx,
John	John	k1gMnSc1
Carew	Carew	k1gMnSc1
Eccles	Eccles	k1gMnSc1
<g/>
,	,	kIx,
Frank	Frank	k1gMnSc1
Macfarlane	Macfarlan	k1gMnSc5
Burnet	Burnet	k1gMnSc1
a	a	k8xC
Howard	Howard	k1gMnSc1
Florey	Florea	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naturalizovaným	naturalizovaný	k2eAgMnSc7d1
Australanem	Australan	k1gMnSc7
byl	být	k5eAaImAgMnS
Bernard	Bernard	k1gMnSc1
Katz	Katz	k1gMnSc1
(	(	kIx(
<g/>
narozen	narozen	k2eAgMnSc1d1
v	v	k7c6
Německu	Německo	k1gNnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
významným	významný	k2eAgMnPc3d1
představitelům	představitel	k1gMnPc3
australské	australský	k2eAgFnSc2d1
vědy	věda	k1gFnSc2
patřil	patřit	k5eAaImAgMnS
i	i	k9
anatom	anatom	k1gMnSc1
a	a	k8xC
antropolog	antropolog	k1gMnSc1
Raymond	Raymond	k1gMnSc1
Dart	Dart	k1gMnSc1
<g/>
,	,	kIx,
vynálezce	vynálezce	k1gMnSc1
černé	černý	k2eAgFnSc2d1
skřínky	skřínka	k1gFnSc2
David	David	k1gMnSc1
Warren	Warrna	k1gFnPc2
<g/>
[	[	kIx(
<g/>
110	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
astronom	astronom	k1gMnSc1
James	James	k1gMnSc1
Dunlop	Dunlop	k1gInSc1
(	(	kIx(
<g/>
narozen	narozen	k2eAgMnSc1d1
ve	v	k7c6
Skotsku	Skotsko	k1gNnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
průkopník	průkopník	k1gMnSc1
radiové	radiový	k2eAgFnSc2d1
astronomie	astronomie	k1gFnSc2
Grote	grot	k1gInSc5
Reber	Reber	k1gInSc1
(	(	kIx(
<g/>
narozen	narozen	k2eAgInSc1d1
v	v	k7c6
USA	USA	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
průkopník	průkopník	k1gMnSc1
jaderné	jaderný	k2eAgFnSc2d1
fúze	fúze	k1gFnSc2
a	a	k8xC
fyzik	fyzik	k1gMnSc1
Mark	Mark	k1gMnSc1
Oliphant	Oliphant	k1gMnSc1
či	či	k8xC
matematici	matematik	k1gMnPc1
a	a	k8xC
nositelé	nositel	k1gMnPc1
Fieldsovy	Fieldsův	k2eAgFnSc2d1
medaile	medaile	k1gFnSc2
Terence	Terence	k1gFnSc2
Tao	Tao	k1gFnSc2
a	a	k8xC
Akshay	Akshaa	k1gFnSc2
Venkatesh	Venkatesha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
oblasti	oblast	k1gFnSc6
sociálních	sociální	k2eAgFnPc2d1
a	a	k8xC
humanitních	humanitní	k2eAgFnPc2d1
věd	věda	k1gFnPc2
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc4
nositel	nositel	k1gMnSc1
Nobelovy	Nobelův	k2eAgFnSc2d1
ceny	cena	k1gFnSc2
za	za	k7c4
ekonomii	ekonomie	k1gFnSc4
John	John	k1gMnSc1
Harsanyi	Harsany	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klasikem	klasik	k1gMnSc7
australské	australský	k2eAgFnSc2d1
filozofie	filozofie	k1gFnSc2
je	být	k5eAaImIp3nS
Samuel	Samuel	k1gMnSc1
Alexander	Alexandra	k1gFnPc2
(	(	kIx(
<g/>
jakkoli	jakkoli	k8xS
se	se	k3xPyFc4
prosadil	prosadit	k5eAaPmAgMnS
hlavně	hlavně	k9
v	v	k7c6
Británii	Británie	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současnosti	současnost	k1gFnSc6
australskou	australský	k2eAgFnSc4d1
filozofii	filozofie	k1gFnSc4
reprezentují	reprezentovat	k5eAaImIp3nP
zejména	zejména	k9
Peter	Peter	k1gMnSc1
Singer	Singer	k1gMnSc1
a	a	k8xC
David	David	k1gMnSc1
Chalmers	Chalmersa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Významným	významný	k2eAgMnSc7d1
archeologem	archeolog	k1gMnSc7
byl	být	k5eAaImAgMnS
Vere	Vere	k1gFnSc4
Gordon	Gordon	k1gMnSc1
Childe	Child	k1gInSc5
<g/>
,	,	kIx,
psychologem	psycholog	k1gMnSc7
Elton	Elton	k1gMnSc1
Mayo	Mayo	k1gMnSc1
<g/>
,	,	kIx,
lingvistiku	lingvistika	k1gFnSc4
rozvinuli	rozvinout	k5eAaPmAgMnP
zejména	zejména	k9
Michael	Michael	k1gMnSc1
Halliday	Hallidaa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Pět	pět	k4xCc1
australských	australský	k2eAgFnPc2d1
univerzit	univerzita	k1gFnPc2
se	se	k3xPyFc4
umístilo	umístit	k5eAaPmAgNnS
v	v	k7c6
první	první	k4xOgFnSc6
padesátce	padesátka	k1gFnSc6
žebříčku	žebříček	k1gInSc2
QS	QS	kA
World	World	k1gMnSc1
University	universita	k1gFnSc2
Rankings	Rankings	k1gInSc1
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvýše	nejvýše	k6eAd1,k6eAd3
Australská	australský	k2eAgFnSc1d1
národní	národní	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
<g/>
,	,	kIx,
státní	státní	k2eAgFnSc1d1
instituce	instituce	k1gFnSc1
zaměřená	zaměřený	k2eAgFnSc1d1
na	na	k7c4
vědecký	vědecký	k2eAgInSc4d1
výzkum	výzkum	k1gInSc4
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
sídlí	sídlet	k5eAaImIp3nS
v	v	k7c6
australském	australský	k2eAgNnSc6d1
hlavním	hlavní	k2eAgNnSc6d1
městě	město	k1gNnSc6
Canbeře	Canbera	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
jsou	být	k5eAaImIp3nP
to	ten	k3xDgNnSc1
Sydneyská	sydneyský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
<g/>
,	,	kIx,
University	universita	k1gFnPc1
of	of	k?
Melbourne	Melbourne	k1gNnSc1
<g/>
,	,	kIx,
University	universita	k1gFnPc1
of	of	k?
New	New	k1gFnSc1
South	South	k1gMnSc1
Wales	Wales	k1gInSc1
a	a	k8xC
University	universita	k1gFnPc1
of	of	k?
Queensland	Queensland	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
111	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sport	sport	k1gInSc1
</s>
<s>
Stadion	stadion	k1gInSc1
Australia	Australium	k1gNnSc2
v	v	k7c4
Sydney	Sydney	k1gNnSc4
během	během	k7c2
rugbyového	rugbyový	k2eAgInSc2d1
zápasu	zápas	k1gInSc2
</s>
<s>
Kriketista	Kriketista	k1gMnSc1
Don	Don	k1gMnSc1
Bradman	Bradman	k1gMnSc1
</s>
<s>
Plavec	plavec	k1gMnSc1
Ian	Ian	k1gMnSc1
Thorpe	Thorp	k1gInSc5
</s>
<s>
Na	na	k7c6
mezinárodním	mezinárodní	k2eAgNnSc6d1
poli	pole	k1gNnSc6
vynikají	vynikat	k5eAaImIp3nP
australské	australský	k2eAgFnPc1d1
reprezentace	reprezentace	k1gFnPc1
v	v	k7c6
kriketu	kriket	k1gInSc6
<g/>
,	,	kIx,
pozemním	pozemní	k2eAgInSc6d1
hokeji	hokej	k1gInSc6
<g/>
,	,	kIx,
netballu	netball	k1gInSc6
<g/>
,	,	kIx,
rugby	rugby	k1gNnSc1
league	league	k1gNnSc1
a	a	k8xC
rugby	rugby	k1gNnSc1
union	union	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Populární	populární	k2eAgInPc1d1
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
australský	australský	k2eAgInSc1d1
fotbal	fotbal	k1gInSc1
a	a	k8xC
také	také	k9
vodní	vodní	k2eAgInPc4d1
sporty	sport	k1gInPc4
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
např.	např.	kA
surfing	surfing	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Australané	Australan	k1gMnPc1
mají	mít	k5eAaImIp3nP
též	též	k9
mimořádnou	mimořádný	k2eAgFnSc4d1
tenisovou	tenisový	k2eAgFnSc4d1
tradici	tradice	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	stoletý	k2eAgMnPc1d1
Australané	Australan	k1gMnPc1
v	v	k7c6
tenise	tenis	k1gInSc6
dominovali	dominovat	k5eAaImAgMnP
<g/>
,	,	kIx,
z	z	k7c2
této	tento	k3xDgFnSc2
éry	éra	k1gFnSc2
pocházejí	pocházet	k5eAaImIp3nP
jména	jméno	k1gNnPc4
legend	legenda	k1gFnPc2
jako	jako	k8xS,k8xC
Rod	rod	k1gInSc1
Laver	lavra	k1gFnPc2
<g/>
,	,	kIx,
John	John	k1gMnSc1
Newcombe	Newcomb	k1gMnSc5
<g/>
,	,	kIx,
Evonne	Evonn	k1gMnSc5
Goolagongová	Goolagongový	k2eAgFnSc1d1
<g/>
,	,	kIx,
Margaret	Margareta	k1gFnPc2
Courtová	Courtový	k2eAgFnSc1d1
<g/>
,	,	kIx,
Arthur	Arthur	k1gMnSc1
Ashe	Ashe	k1gFnPc2
<g/>
,	,	kIx,
Margaret	Margareta	k1gFnPc2
Smithová	Smithový	k2eAgFnSc1d1
<g/>
,	,	kIx,
Ken	Ken	k1gMnSc1
Rosewall	Rosewall	k1gMnSc1
či	či	k8xC
Roy	Roy	k1gMnSc1
Emerson	Emerson	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tzv.	tzv.	kA
open	open	k1gInSc4
éře	éra	k1gFnSc3
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
začala	začít	k5eAaPmAgFnS
na	na	k7c6
konci	konec	k1gInSc6
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
<g/>
,	,	kIx,
výsadní	výsadní	k2eAgNnSc4d1
postavení	postavení	k1gNnSc4
ztratili	ztratit	k5eAaPmAgMnP
<g/>
,	,	kIx,
přesto	přesto	k8xC
se	se	k3xPyFc4
řada	řada	k1gFnSc1
australských	australský	k2eAgMnPc2d1
tenistů	tenista	k1gMnPc2
prosadila	prosadit	k5eAaPmAgFnS
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
například	například	k6eAd1
světová	světový	k2eAgFnSc1d1
jednička	jednička	k1gFnSc1
a	a	k8xC
vítěz	vítěz	k1gMnSc1
Wimbledonu	Wimbledon	k1gInSc2
Lleyton	Lleyton	k1gInSc1
Hewitt	Hewitt	k2eAgInSc1d1
<g/>
,	,	kIx,
světová	světový	k2eAgFnSc1d1
jednička	jednička	k1gFnSc1
Patrick	Patrick	k1gMnSc1
Rafter	Rafter	k1gMnSc1
<g/>
,	,	kIx,
vítěz	vítěz	k1gMnSc1
Wimbledonu	Wimbledon	k1gInSc2
Pat	pat	k1gInSc1
Cash	cash	k1gFnSc1
nebo	nebo	k8xC
Samantha	Samantha	k1gFnSc1
Stosurová	stosurový	k2eAgFnSc1d1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
vyhrála	vyhrát	k5eAaPmAgFnS
US	US	kA
Open	Open	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mimořádně	mimořádně	k6eAd1
úspěšný	úspěšný	k2eAgInSc1d1
pár	pár	k1gInSc1
pro	pro	k7c4
čtyřhru	čtyřhra	k1gFnSc4
vytvořili	vytvořit	k5eAaPmAgMnP
Mark	Mark	k1gMnSc1
Woodforde	Woodford	k1gInSc5
a	a	k8xC
Todd	Todd	k1gMnSc1
Woodbridge	Woodbridg	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Austrálii	Austrálie	k1gFnSc6
má	mít	k5eAaImIp3nS
velkou	velký	k2eAgFnSc4d1
tradici	tradice	k1gFnSc4
také	také	k9
automobilový	automobilový	k2eAgInSc4d1
sport	sport	k1gInSc4
<g/>
,	,	kIx,
mj.	mj.	kA
i	i	k9
proto	proto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
na	na	k7c6
sousedním	sousední	k2eAgInSc6d1
Novém	nový	k2eAgInSc6d1
Zélandu	Zéland	k1gInSc6
vznikla	vzniknout	k5eAaPmAgFnS
stáj	stáj	k1gFnSc1
Formule	formule	k1gFnSc1
1	#num#	k4
McLaren	McLarna	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
dávala	dávat	k5eAaImAgFnS
Australanům	Australan	k1gMnPc3
často	často	k6eAd1
příležitost	příležitost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jack	Jack	k1gInSc1
Brabham	Brabham	k1gInSc1
se	se	k3xPyFc4
v	v	k7c6
závodech	závod	k1gInPc6
F1	F1	k1gMnPc2
stal	stát	k5eAaPmAgInS
trojnásobným	trojnásobný	k2eAgMnSc7d1
mistrem	mistr	k1gMnSc7
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeden	jeden	k4xCgInSc4
titul	titul	k1gInSc4
si	se	k3xPyFc3
připsal	připsat	k5eAaPmAgMnS
Alan	Alan	k1gMnSc1
Jones	Jones	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Donedávna	donedávna	k6eAd1
po	po	k7c6
okruzích	okruh	k1gInPc6
kroužil	kroužit	k5eAaImAgMnS
i	i	k9
Mark	Mark	k1gMnSc1
Webber	Webber	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
v	v	k7c6
šampionátu	šampionát	k1gInSc6
F1	F1	k1gFnSc2
dosáhl	dosáhnout	k5eAaPmAgInS
na	na	k7c4
třetí	třetí	k4xOgNnSc4
místo	místo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
v	v	k7c6
závodech	závod	k1gInPc6
silničních	silniční	k2eAgInPc2d1
motocyklů	motocykl	k1gInPc2
měli	mít	k5eAaImAgMnP
Australané	Australan	k1gMnPc1
své	svůj	k3xOyFgInPc4
trumfy	trumf	k1gInPc4
<g/>
:	:	kIx,
Mick	Mick	k1gMnSc1
Doohan	Doohan	k1gMnSc1
je	být	k5eAaImIp3nS
pětinásobným	pětinásobný	k2eAgMnSc7d1
mistrem	mistr	k1gMnSc7
světa	svět	k1gInSc2
<g/>
,	,	kIx,
Casey	Casea	k1gFnSc2
Stoner	Stonra	k1gFnPc2
dvojnásobným	dvojnásobný	k2eAgMnSc7d1
<g/>
.	.	kIx.
</s>
<s>
Australané	Australan	k1gMnPc1
dlouho	dlouho	k6eAd1
odmítali	odmítat	k5eAaImAgMnP
<g/>
,	,	kIx,
podobně	podobně	k6eAd1
jako	jako	k9
Američané	Američan	k1gMnPc1
<g/>
,	,	kIx,
propadnout	propadnout	k5eAaPmF
evropskému	evropský	k2eAgInSc3d1
fotbalu	fotbal	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ovšem	ovšem	k9
na	na	k7c6
konci	konec	k1gInSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
to	ten	k3xDgNnSc1
změnilo	změnit	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Evropě	Evropa	k1gFnSc6
i	i	k8xC
na	na	k7c6
světových	světový	k2eAgInPc6d1
šampionátech	šampionát	k1gInPc6
uspěli	uspět	k5eAaPmAgMnP
Tim	Tim	k?
Cahill	Cahill	k1gMnSc1
<g/>
,	,	kIx,
Mark	Mark	k1gMnSc1
Schwarzer	Schwarzer	k1gMnSc1
<g/>
,	,	kIx,
Lucas	Lucas	k1gMnSc1
Neill	Neill	k1gMnSc1
<g/>
,	,	kIx,
Harry	Harr	k1gInPc1
Kewell	Kewell	k1gInSc1
nebo	nebo	k8xC
Mark	Mark	k1gMnSc1
Viduka	Viduk	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Andrew	Andrew	k?
Gaze	Gaze	k1gFnSc1
je	být	k5eAaImIp3nS
členem	člen	k1gInSc7
síně	síň	k1gFnSc2
slávy	sláva	k1gFnSc2
Mezinárodní	mezinárodní	k2eAgFnSc2d1
basketbalové	basketbalový	k2eAgFnSc2d1
federace	federace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Basketbalistka	basketbalistka	k1gFnSc1
Lauren	Laurna	k1gFnPc2
Jacksonová	Jacksonová	k1gFnSc1
byla	být	k5eAaImAgFnS
vybrána	vybrat	k5eAaPmNgFnS
mezi	mezi	k7c4
15	#num#	k4
nejlepších	dobrý	k2eAgFnPc2d3
hráček	hráčka	k1gFnPc2
americké	americký	k2eAgFnSc2d1
WNBA	WNBA	kA
všech	všecek	k3xTgFnPc2
dob	doba	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
112	#num#	k4
<g/>
]	]	kIx)
Cyklista	cyklista	k1gMnSc1
Cadel	Cadel	k1gMnSc1
Evans	Evans	k1gInSc4
je	být	k5eAaImIp3nS
vítěz	vítěz	k1gMnSc1
Tour	Tour	k1gMnSc1
de	de	k?
France	Franc	k1gMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
113	#num#	k4
<g/>
]	]	kIx)
Karrie	Karrie	k1gFnSc1
Webbová	Webbová	k1gFnSc1
je	být	k5eAaImIp3nS
jedinou	jediný	k2eAgFnSc7d1
golfistkou	golfistka	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
dosáhla	dosáhnout	k5eAaPmAgFnS
tzv.	tzv.	kA
super	super	k1gInSc4
grandslamu	grandslam	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Tradičními	tradiční	k2eAgInPc7d1
národními	národní	k2eAgInPc7d1
sporty	sport	k1gInPc7
jsou	být	k5eAaImIp3nP
australský	australský	k2eAgInSc1d1
fotbal	fotbal	k1gInSc1
<g/>
,	,	kIx,
kriket	kriket	k1gInSc1
a	a	k8xC
pozemní	pozemní	k2eAgInSc1d1
hokej	hokej	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
nejlepším	dobrý	k2eAgMnPc3d3
hráčům	hráč	k1gMnPc3
kriketu	kriket	k1gInSc2
v	v	k7c6
historii	historie	k1gFnSc6
patří	patřit	k5eAaImIp3nS
Australan	Australan	k1gMnSc1
Don	Don	k1gMnSc1
Bradman	Bradman	k1gMnSc1
<g/>
[	[	kIx(
<g/>
114	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
k	k	k7c3
legendám	legenda	k1gFnPc3
tohoto	tento	k3xDgInSc2
sportu	sport	k1gInSc2
patří	patřit	k5eAaImIp3nS
i	i	k9
Allan	Allan	k1gMnSc1
Border	Border	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rechelle	Rechelle	k1gFnSc1
Hawkesová	Hawkesový	k2eAgFnSc1d1
vybojovala	vybojovat	k5eAaPmAgFnS
s	s	k7c7
národním	národní	k2eAgInSc7d1
týmem	tým	k1gInSc7
pozemního	pozemní	k2eAgInSc2d1
hokeje	hokej	k1gInSc2
tři	tři	k4xCgNnPc4
olympijská	olympijský	k2eAgNnPc4d1
zlata	zlato	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jamie	Jamie	k1gFnSc1
Dwyer	Dwyra	k1gFnPc2
byl	být	k5eAaImAgMnS
pětkrát	pětkrát	k6eAd1
vyhlášen	vyhlásit	k5eAaPmNgMnS
nejlepším	dobrý	k2eAgMnSc7d3
pozemním	pozemní	k2eAgMnSc7d1
hokejistou	hokejista	k1gMnSc7
světa	svět	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
115	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Významná	významný	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
i	i	k9
australská	australský	k2eAgFnSc1d1
plavecká	plavecký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plavec	plavec	k1gMnSc1
Ian	Ian	k1gMnSc1
Thorpe	Thorp	k1gInSc5
má	mít	k5eAaImIp3nS
na	na	k7c6
svém	svůj	k3xOyFgNnSc6
kontě	konto	k1gNnSc6
pět	pět	k4xCc1
zlatých	zlatý	k2eAgFnPc2d1
olympijských	olympijský	k2eAgFnPc2d1
medailí	medaile	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
116	#num#	k4
<g/>
]	]	kIx)
Ale	ale	k9
na	na	k7c6
olympiádách	olympiáda	k1gFnPc6
uspěli	uspět	k5eAaPmAgMnP
i	i	k8xC
další	další	k2eAgMnPc1d1
plavci	plavec	k1gMnPc1
<g/>
,	,	kIx,
čtyři	čtyři	k4xCgFnPc1
zlaté	zlatá	k1gFnPc1
mají	mít	k5eAaImIp3nP
Dawn	Dawn	k1gInSc4
Fraserová	Fraserová	k1gFnSc1
<g/>
,	,	kIx,
Libby	Libba	k1gMnSc2
Trickettová	Trickettová	k1gFnSc1
a	a	k8xC
Murray	Murra	k2eAgMnPc4d1
Rose	Ros	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tři	tři	k4xCgInPc1
zlata	zlato	k1gNnSc2
mají	mít	k5eAaImIp3nP
ve	v	k7c6
sbírce	sbírka	k1gFnSc6
Leisel	Leisela	k1gFnPc2
Jonesová	Jonesový	k2eAgFnSc1d1
<g/>
,	,	kIx,
Petria	Petrium	k1gNnPc1
Thomasová	Thomasová	k1gFnSc1
<g/>
,	,	kIx,
Grant	grant	k1gInSc1
Hackett	Hackett	k2eAgInSc1d1
<g/>
,	,	kIx,
Shane	Shan	k1gInSc5
Gouldová	Gouldová	k1gFnSc1
<g/>
,	,	kIx,
Jodie	Jodie	k1gFnSc1
Henryová	Henryová	k1gFnSc1
a	a	k8xC
Stephanie	Stephanie	k1gFnSc1
Riceová	Riceová	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Tradičně	tradičně	k6eAd1
silní	silný	k2eAgMnPc1d1
jsou	být	k5eAaImIp3nP
Australané	Australan	k1gMnPc1
též	též	k9
v	v	k7c6
jízdě	jízda	k1gFnSc6
na	na	k7c6
koni	kůň	k1gMnSc6
<g/>
,	,	kIx,
Andrew	Andrew	k1gFnPc6
Hoy	Hoy	k1gMnPc2
a	a	k8xC
Matthew	Matthew	k1gMnPc1
Ryan	Ryana	k1gFnPc2
získali	získat	k5eAaPmAgMnP
v	v	k7c6
parkuru	parkur	k1gInSc6
na	na	k7c6
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
každý	každý	k3xTgMnSc1
po	po	k7c6
třech	tři	k4xCgFnPc6
zlatých	zlatý	k2eAgFnPc6d1
medailích	medaile	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejnou	stejný	k2eAgFnSc4d1
bilanci	bilance	k1gFnSc4
mají	mít	k5eAaImIp3nP
i	i	k9
veslaři	veslař	k1gMnPc1
Drew	Drew	k1gMnSc1
Ginn	Ginn	k1gMnSc1
a	a	k8xC
James	James	k1gMnSc1
Tomkins	Tomkinsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
atletice	atletika	k1gFnSc6
se	se	k3xPyFc4
Australanům	Australan	k1gMnPc3
dařilo	dařit	k5eAaImAgNnS
v	v	k7c6
minulosti	minulost	k1gFnSc6
zvláště	zvláště	k6eAd1
v	v	k7c6
bězích	běh	k1gInPc6
<g/>
,	,	kIx,
k	k	k7c3
legendárním	legendární	k2eAgFnPc3d1
běžkyním	běžkyně	k1gFnPc3
patřily	patřit	k5eAaImAgFnP
Betty	Betty	k1gFnSc1
Cuthbertová	Cuthbertová	k1gFnSc1
(	(	kIx(
<g/>
4	#num#	k4
olympijská	olympijský	k2eAgFnSc1d1
zlata	zlato	k1gNnSc2
<g/>
)	)	kIx)
a	a	k8xC
Shirley	Shirlea	k1gFnPc1
Stricklandová	Stricklandový	k2eAgFnSc1d1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
má	mít	k5eAaImIp3nS
nejcennější	cenný	k2eAgInPc4d3
olympijské	olympijský	k2eAgInPc4d1
kovy	kov	k1gInPc4
tři	tři	k4xCgInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
roku	rok	k1gInSc3
2018	#num#	k4
získala	získat	k5eAaPmAgFnS
Austrálie	Austrálie	k1gFnSc1
na	na	k7c6
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
152	#num#	k4
zlatých	zlatý	k2eAgFnPc2d1
medailí	medaile	k1gFnPc2
<g/>
.	.	kIx.
60	#num#	k4
z	z	k7c2
nich	on	k3xPp3gMnPc2
bylo	být	k5eAaImAgNnS
v	v	k7c6
plavání	plavání	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pět	pět	k4xCc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
na	na	k7c6
hrách	hra	k1gFnPc6
zimních	zimní	k2eAgFnPc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
kolektivních	kolektivní	k2eAgInPc2d1
sportů	sport	k1gInPc2
jsou	být	k5eAaImIp3nP
Australané	Australan	k1gMnPc1
nejúspěšnější	úspěšný	k2eAgMnPc1d3
v	v	k7c6
pozemním	pozemní	k2eAgInSc6d1
hokeji	hokej	k1gInSc6
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgMnSc6
brali	brát	k5eAaImAgMnP
již	již	k6eAd1
čtyři	čtyři	k4xCgFnPc4
zlaté	zlatý	k2eAgFnPc4d1
a	a	k8xC
celkem	celkem	k6eAd1
vybojovali	vybojovat	k5eAaPmAgMnP
12	#num#	k4
olympijských	olympijský	k2eAgFnPc2d1
medailí	medaile	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Dvakrát	dvakrát	k6eAd1
Austrálie	Austrálie	k1gFnSc1
olympijské	olympijský	k2eAgFnSc2d1
hry	hra	k1gFnSc2
pořádala	pořádat	k5eAaImAgFnS
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1956	#num#	k4
je	on	k3xPp3gFnPc4
hostilo	hostit	k5eAaImAgNnS
Melbourne	Melbourne	k1gNnSc1
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
Sydney	Sydney	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
http://www.abs.gov.au/ausstats/abs@.nsf/94713ad445ff1425ca25682000192af2/1647509ef7e25faaca2568a900154b63?OpenPocument	http://www.abs.gov.au/ausstats/abs@.nsf/94713ad445ff1425ca25682000192af2/1647509ef7e25faaca2568a900154b63?OpenPocument	k1gInSc1
<g/>
↑	↑	k?
Světová	světový	k2eAgFnSc1d1
banka	banka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
GDP	GDP	kA
per	pero	k1gNnPc2
capita	capitum	k1gNnSc2
<g/>
,	,	kIx,
PPP	PPP	kA
(	(	kIx(
<g/>
current	current	k1gMnSc1
international	internationat	k5eAaPmAgMnS,k5eAaImAgMnS
$	$	kIx~
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
STATISTICS	STATISTICS	kA
<g/>
,	,	kIx,
c	c	k0
<g/>
=	=	kIx~
<g/>
AU	au	k0
<g/>
;	;	kIx,
o	o	k7c6
<g/>
=	=	kIx~
<g/>
Commonwealth	Commonwealth	k1gInSc1
of	of	k?
Australia	Australia	k1gFnSc1
<g/>
;	;	kIx,
ou	ou	k0
<g/>
=	=	kIx~
<g/>
Australian	Australian	k1gInSc1
Bureau	Bureaus	k1gInSc2
of	of	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Main	Main	k1gNnSc1
Features	Featuresa	k1gFnPc2
-	-	kIx~
Geographic	Geographice	k1gFnPc2
distribution	distribution	k1gInSc1
of	of	k?
the	the	k?
population	population	k1gInSc1
<g/>
.	.	kIx.
www.abs.gov.au	www.abs.gov.au	k6eAd1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2012-05-24	2012-05-24	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
CLARKSON	CLARKSON	kA
<g/>
,	,	kIx,
Chris	Chris	k1gFnSc1
<g/>
;	;	kIx,
JACOBS	JACOBS	kA
<g/>
,	,	kIx,
Zenobia	Zenobia	k1gFnSc1
<g/>
;	;	kIx,
MARWICK	MARWICK	kA
<g/>
,	,	kIx,
Ben	Ben	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Human	Human	k1gInSc1
occupation	occupation	k1gInSc4
of	of	k?
northern	northern	k1gInSc1
Australia	Australia	k1gFnSc1
by	by	k9
65,000	65,000	k4
years	yearsa	k1gFnPc2
ago	aga	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nature	Natur	k1gMnSc5
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
547	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
7663	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
306	#num#	k4
<g/>
–	–	k?
<g/>
310	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
28	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
836	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.103	10.103	k4
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
nature	natur	k1gMnSc5
<g/>
22968	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
KORSCH	KORSCH	kA
<g/>
,	,	kIx,
R.	R.	kA
J.	J.	kA
<g/>
;	;	kIx,
KOSITCIN	KOSITCIN	kA
<g/>
,	,	kIx,
N.	N.	kA
<g/>
;	;	kIx,
CHAMPION	CHAMPION	kA
<g/>
,	,	kIx,
D.	D.	kA
C.	C.	kA
Australian	Australiana	k1gFnPc2
island	islanda	k1gFnPc2
arcs	arcs	k1gInSc1
through	through	k1gMnSc1
time	time	k1gInSc1
<g/>
:	:	kIx,
Geodynamic	Geodynamic	k1gMnSc1
implications	implications	k6eAd1
for	forum	k1gNnPc2
the	the	k?
Archean	Archean	k1gMnSc1
and	and	k?
Proterozoic	Proterozoic	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gondwana	Gondwana	k1gFnSc1
Research	Researcha	k1gFnPc2
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
19	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
716	#num#	k4
<g/>
–	–	k?
<g/>
734	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.101	10.101	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
j.	j.	k?
<g/>
gr	gr	k?
<g/>
.2010	.2010	k4
<g/>
.11	.11	k4
<g/>
.018	.018	k4
<g/>
.	.	kIx.
↑	↑	k?
Map	mapa	k1gFnPc2
from	from	k6eAd1
above	abovat	k5eAaPmIp3nS
shows	shows	k6eAd1
Australia	Australia	k1gFnSc1
is	is	k?
a	a	k8xC
very	vera	k1gMnSc2
flat	flat	k1gInSc1
place	plac	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Sydney	Sydney	k1gNnSc2
Morning	Morning	k1gInSc1
Herald	Heraldo	k1gNnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2005-01-22	2005-01-22	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
AGENCY	AGENCY	kA
<g/>
,	,	kIx,
Digital	Digital	kA
Transformation	Transformation	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Australian	Australian	k1gMnSc1
continent	continent	k1gMnSc1
<g/>
.	.	kIx.
the-australian-continent	the-australian-continent	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
English	English	k1gInSc1
<g/>
)	)	kIx)
↑	↑	k?
AUSTRALIA	AUSTRALIA	kA
<g/>
,	,	kIx,
c	c	k0
<g/>
\	\	kIx~
<g/>
=	=	kIx~
<g/>
AU	au	k0
<g/>
\	\	kIx~
<g/>
;	;	kIx,
<g/>
o	o	k7c4
<g/>
\	\	kIx~
<g/>
=	=	kIx~
<g/>
Australia	Australia	k1gFnSc1
Government	Government	k1gMnSc1
<g/>
\	\	kIx~
<g/>
;	;	kIx,
<g/>
ou	ou	k0
<g/>
\	\	kIx~
<g/>
=	=	kIx~
<g/>
Geoscience	Geoscience	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deserts	Deserts	k1gInSc1
<g/>
.	.	kIx.
www.ga.gov.au	www.ga.gov.au	k6eAd1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-05-15	2014-05-15	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
web.archive.org	web.archive.org	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Wayback	Wayback	k1gInSc1
Machine	Machin	k1gInSc5
<g/>
.	.	kIx.
web.archive.org	web.archive.org	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-07-06	2011-07-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Geoscience	Geoscience	k1gFnSc1
Australia	Australia	k1gFnSc1
-	-	kIx~
Fab	Fab	k1gFnSc1
Facts	Facts	k1gInSc1
<g/>
,	,	kIx,
Dimensions	Dimensions	k1gInSc1
<g/>
,	,	kIx,
Australia	Australia	k1gFnSc1
Compared	Compared	k1gMnSc1
<g/>
.	.	kIx.
web.archive.org	web.archive.org	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2007-03-24	2007-03-24	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Australia	Australia	k1gFnSc1
wealthiest	wealthiest	k1gMnSc1
nation	nation	k1gInSc1
in	in	k?
world	world	k1gInSc1
<g/>
:	:	kIx,
report	report	k1gInSc1
<g/>
.	.	kIx.
web.archive.org	web.archive.org	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2012-07-21	2012-07-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Australia	Australia	k1gFnSc1
wealthiest	wealthiest	k1gMnSc1
nation	nation	k1gInSc1
in	in	k?
world	world	k1gInSc1
<g/>
:	:	kIx,
report	report	k1gInSc1
<g/>
.	.	kIx.
web.archive.org	web.archive.org	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2012-07-21	2012-07-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Australians	Australians	k1gInSc1
the	the	k?
world	world	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
wealthiest	wealthiest	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Sydney	Sydney	k1gNnSc2
Morning	Morning	k1gInSc1
Herald	Heraldo	k1gNnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-10-30	2011-10-30	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Migration	Migration	k1gInSc1
<g/>
,	,	kIx,
Australia	Australia	k1gFnSc1
<g/>
,	,	kIx,
2018-19	2018-19	k4
financial	financial	k1gInSc1
year	year	k1gInSc4
|	|	kIx~
Australian	Australian	k1gInSc1
Bureau	Bureaus	k1gInSc2
of	of	k?
Statistics	Statistics	k1gInSc1
<g/>
.	.	kIx.
www.abs.gov.au	www.abs.gov.au	k6eAd1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-04-28	2020-04-28	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
United	United	k1gInSc1
Nations	Nations	k1gInSc1
Population	Population	k1gInSc1
Division	Division	k1gInSc1
|	|	kIx~
Department	department	k1gInSc1
of	of	k?
Economic	Economice	k1gInPc2
and	and	k?
Social	Social	k1gInSc1
Affairs	Affairs	k1gInSc1
<g/>
.	.	kIx.
www.un.org	www.un.org	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Australia	Australia	k1gFnSc1
<g/>
:	:	kIx,
World	World	k1gInSc1
Audit	audit	k1gInSc1
Democracy	Democraca	k1gMnSc2
Profile	profil	k1gInSc5
<g/>
.	.	kIx.
web.archive.org	web.archive.org	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2007-12-13	2007-12-13	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
australia	australia	k1gFnSc1
|	|	kIx~
Origin	Origin	k1gInSc1
and	and	k?
meaning	meaning	k1gInSc1
of	of	k?
the	the	k?
name	nam	k1gMnSc2
australia	australius	k1gMnSc2
by	by	kYmCp3nS
Online	Onlin	k1gInSc5
Etymology	etymolog	k1gMnPc7
Dictionary	Dictionara	k1gFnSc2
<g/>
.	.	kIx.
www.etymonline.com	www.etymonline.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Online	Onlin	k1gInSc5
Etymology	etymolog	k1gMnPc7
Dictionary	Dictionara	k1gFnSc2
<g/>
.	.	kIx.
web.archive.org	web.archive.org	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016-12-23	2016-12-23	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
CLARKE	CLARKE	kA
<g/>
,	,	kIx,
Jacqueline	Jacquelin	k1gInSc5
<g/>
;	;	kIx,
CLARKE	CLARKE	kA
<g/>
,	,	kIx,
Philip	Philip	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Putting	Putting	k1gInSc1
'	'	kIx"
<g/>
Australia	Australia	k1gFnSc1
<g/>
'	'	kIx"
on	on	k3xPp3gMnSc1
the	the	k?
map	mapa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Conversation	Conversation	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
WHO	WHO	kA
NAMED	NAMED	kA
AUSTRALIA	AUSTRALIA	kA
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mail	mail	k1gInSc1
(	(	kIx(
<g/>
Adelaide	Adelaid	k1gMnSc5
<g/>
,	,	kIx,
SA	SA	kA
:	:	kIx,
1912	#num#	k4
-	-	kIx~
1954	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
11	#num#	k4
II	II	kA
1928	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
16	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
Wayback	Wayback	k1gInSc1
Machine	Machin	k1gInSc5
<g/>
.	.	kIx.
web.archive.org	web.archive.org	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2009-10-17	2009-10-17	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
COMAN	COMAN	kA
<g/>
,	,	kIx,
Brian	Brian	k1gMnSc1
J.	J.	kA
A	a	k9
Loose	Loose	k1gFnSc1
Canon	Canon	kA
<g/>
:	:	kIx,
Essays	Essays	k1gInSc1
on	on	k3xPp3gMnSc1
History	Histor	k1gInPc1
<g/>
,	,	kIx,
Modernity	modernita	k1gFnPc1
and	and	k?
Tradition	Tradition	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Connor	Connor	k1gInSc1
Court	Courta	k1gFnPc2
Publishing	Publishing	k1gInSc1
Pty	Pty	k1gMnSc1
Ltd	ltd	kA
188	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
9802936	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Google-Books-ID	Google-Books-ID	k1gFnSc1
<g/>
:	:	kIx,
P	P	kA
<g/>
5	#num#	k4
<g/>
m	m	kA
<g/>
4	#num#	k4
<g/>
zNxaaSUC	zNxaaSUC	k?
<g/>
.	.	kIx.
↑	↑	k?
SCHOOL	SCHOOL	kA
<g/>
,	,	kIx,
Head	Head	k1gInSc1
of	of	k?
<g/>
;	;	kIx,
ADMIN.SLLL@ANU.EDU.AU	ADMIN.SLLL@ANU.EDU.AU	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Australian	Australian	k1gInSc1
National	National	k1gFnSc2
Dictionary	Dictionara	k1gFnSc2
Centre	centr	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
ANU	ANU	kA
School	Schoola	k1gFnPc2
of	of	k?
Literature	Literatur	k1gMnSc5
<g/>
,	,	kIx,
Languages	Languages	k1gInSc1
and	and	k?
Linguistics	Linguistics	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
NUNN	NUNN	kA
<g/>
,	,	kIx,
Patrick	Patrick	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Edge	Edg	k1gFnSc2
of	of	k?
Memory	Memora	k1gFnSc2
<g/>
:	:	kIx,
Ancient	Ancient	k1gMnSc1
Stories	Stories	k1gMnSc1
<g/>
,	,	kIx,
Oral	orat	k5eAaImAgInS
Tradition	Tradition	k1gInSc1
and	and	k?
the	the	k?
Post-Glacial	Post-Glacial	k1gMnSc1
World	World	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Bloomsbury	Bloomsbur	k1gInPc4
Publishing	Publishing	k1gInSc1
244	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
4729	#num#	k4
<g/>
-	-	kIx~
<g/>
4327	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Google-Books-ID	Google-Books-ID	k1gFnSc1
<g/>
:	:	kIx,
Z	z	k7c2
<g/>
4	#num#	k4
<g/>
xaDwAAQBAJ	xaDwAAQBAJ	k?
<g/>
.	.	kIx.
↑	↑	k?
FAGAN	FAGAN	k?
<g/>
,	,	kIx,
Brian	Brian	k1gMnSc1
M.	M.	kA
<g/>
;	;	kIx,
DURRANI	DURRANI	kA
<g/>
,	,	kIx,
Nadia	Nadium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
People	People	k1gFnSc1
of	of	k?
the	the	k?
Earth	Earth	k1gInSc1
<g/>
:	:	kIx,
An	An	k1gFnSc1
Introduction	Introduction	k1gInSc1
to	ten	k3xDgNnSc1
World	World	k1gInSc1
Prehistory	Prehistor	k1gInPc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Routledge	Routledge	k1gInSc1
1205	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
351	#num#	k4
<g/>
-	-	kIx~
<g/>
75764	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Google-Books-ID	Google-Books-ID	k1gFnSc1
<g/>
:	:	kIx,
W	W	kA
<g/>
0	#num#	k4
<g/>
NvDwAAQBAJ	NvDwAAQBAJ	k1gFnPc1
<g/>
.	.	kIx.
↑	↑	k?
OPPENHEIMER	OPPENHEIMER	kA
<g/>
,	,	kIx,
Stephen	Stephen	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Out	Out	k1gFnSc1
of	of	k?
Eden	Eden	k1gInSc1
<g/>
:	:	kIx,
The	The	k1gFnSc1
Peopling	Peopling	k1gInSc1
of	of	k?
the	the	k?
World	World	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Little	Little	k1gFnSc1
<g/>
,	,	kIx,
Brown	Brown	k1gMnSc1
Book	Book	k1gMnSc1
Group	Group	k1gMnSc1
266	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
78033	#num#	k4
<g/>
-	-	kIx~
<g/>
753	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Google-Books-ID	Google-Books-ID	k1gFnSc1
<g/>
:	:	kIx,
VQQvDwAAQBAJ	VQQvDwAAQBAJ	k1gFnSc1
<g/>
.	.	kIx.
↑	↑	k?
Původní	původní	k2eAgNnSc1d1
obyvatelstvo	obyvatelstvo	k1gNnSc1
|	|	kIx~
Austrálie	Austrálie	k1gFnSc1
|	|	kIx~
Pedagogická	pedagogický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
Masarykovy	Masarykův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
<g/>
.	.	kIx.
is	is	k?
<g/>
.	.	kIx.
<g/>
muni	muni	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Scott	Scott	k1gInSc1
Cane	Cane	k1gFnSc1
<g/>
;	;	kIx,
First	First	k1gInSc1
Footprints	Footprints	k1gInSc1
–	–	k?
the	the	k?
epic	epic	k1gInSc1
story	story	k1gFnSc1
of	of	k?
the	the	k?
first	first	k1gInSc1
Australians	Australians	k1gInSc1
<g/>
;	;	kIx,
Allen	Allen	k1gMnSc1
&	&	k?
Unwin	Unwin	k1gMnSc1
<g/>
;	;	kIx,
2013	#num#	k4
<g/>
;	;	kIx,
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
74331	#num#	k4
<g/>
-	-	kIx~
<g/>
493	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
;	;	kIx,
str	str	kA
<g/>
.	.	kIx.
25	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
↑	↑	k?
GILLIGAN	GILLIGAN	kA
<g/>
,	,	kIx,
Ian	Ian	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Climate	Climat	k1gInSc5
<g/>
,	,	kIx,
Clothing	Clothing	k1gInSc1
<g/>
,	,	kIx,
and	and	k?
Agriculture	Agricultur	k1gMnSc5
in	in	k?
Prehistory	Prehistor	k1gInPc4
<g/>
:	:	kIx,
Linking	Linking	k1gInSc4
Evidence	evidence	k1gFnSc2
<g/>
,	,	kIx,
Causes	Causes	k1gMnSc1
<g/>
,	,	kIx,
and	and	k?
Effects	Effects	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Cambridge	Cambridge	k1gFnSc1
University	universita	k1gFnSc2
Press	Press	k1gInSc1
347	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
108	#num#	k4
<g/>
-	-	kIx~
<g/>
47008	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Google-Books-ID	Google-Books-ID	k1gFnSc1
<g/>
:	:	kIx,
Ux	Ux	k1gFnSc1
<g/>
50	#num#	k4
<g/>
DwAAQBAJ	DwAAQBAJ	k1gFnPc2
<g/>
.	.	kIx.
↑	↑	k?
BOWLER	BOWLER	kA
<g/>
,	,	kIx,
James	James	k1gMnSc1
M.	M.	kA
<g/>
;	;	kIx,
JOHNSTON	JOHNSTON	kA
<g/>
,	,	kIx,
Harvey	Harvea	k1gFnSc2
<g/>
;	;	kIx,
OLLEY	OLLEY	kA
<g/>
,	,	kIx,
Jon	Jon	k1gFnSc1
M.	M.	kA
New	New	k1gFnSc4
ages	agesa	k1gFnPc2
for	forum	k1gNnPc2
human	humany	k1gInPc2
occupation	occupation	k1gInSc4
and	and	k?
climatic	climatice	k1gFnPc2
change	change	k1gFnSc1
at	at	k?
Lake	Lake	k1gNnSc1
Mungo	mungo	k1gMnSc1
<g/>
,	,	kIx,
Australia	Australia	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nature	Natur	k1gMnSc5
<g/>
.	.	kIx.
2003	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
421	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
837	#num#	k4
<g/>
–	–	k?
<g/>
840	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
28	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
836	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.103	10.103	k4
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
nature	natur	k1gMnSc5
<g/>
0	#num#	k4
<g/>
1383	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
TUNIZ	TUNIZ	kA
<g/>
,	,	kIx,
Claudio	Claudio	k1gMnSc1
<g/>
;	;	kIx,
GILLESPIE	GILLESPIE	kA
<g/>
,	,	kIx,
Richard	Richard	k1gMnSc1
<g/>
;	;	kIx,
JONES	JONES	kA
<g/>
,	,	kIx,
Cheryl	Cheryl	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Bone	bon	k1gInSc5
Readers	Readers	k1gInSc1
<g/>
:	:	kIx,
Science	Science	k1gFnSc1
and	and	k?
Politics	Politics	k1gInSc1
in	in	k?
Human	Human	k1gInSc1
Origins	Origins	k1gInSc1
Research	Research	k1gInSc4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Routledge	Routledge	k1gInSc1
273	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
315	#num#	k4
<g/>
-	-	kIx~
<g/>
41888	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Google-Books-ID	Google-Books-ID	k1gFnSc1
<g/>
:	:	kIx,
WrJmDAAAQBAJ	WrJmDAAAQBAJ	k1gFnSc1
<g/>
.	.	kIx.
↑	↑	k?
CASTILLO	CASTILLO	kA
<g/>
,	,	kIx,
Alicia	Alicium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Archaeological	Archaeological	k1gFnPc2
Dimension	Dimension	k1gInSc4
of	of	k?
World	World	k1gInSc1
Heritage	Heritage	k1gFnSc1
<g/>
:	:	kIx,
From	From	k1gInSc1
Prevention	Prevention	k1gInSc1
to	ten	k3xDgNnSc1
Social	Social	k1gInSc4
Implications	Implications	k1gInSc4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Springer	Springer	k1gInSc1
Science	Science	k1gFnSc2
&	&	k?
Business	business	k1gInSc1
Media	medium	k1gNnSc2
121	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
4939	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
283	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Google-Books-ID	Google-Books-ID	k1gFnSc1
<g/>
:	:	kIx,
jV	jV	k?
<g/>
64	#num#	k4
<g/>
BAAAQBAJ	BAAAQBAJ	kA
<g/>
.	.	kIx.
↑	↑	k?
NEWS	NEWS	kA
<g/>
,	,	kIx,
Opening	Opening	k1gInSc4
Hours	Hoursa	k1gFnPc2
10	#num#	k4
<g/>
am-	am-	k?
<g/>
5	#num#	k4
<g/>
pmFree	pmFre	k1gMnSc2
General	General	k1gFnSc4
Entry	Entra	k1gFnSc2
Address	Address	k1gInSc4
1	#num#	k4
William	William	k1gInSc1
StreetSydney	StreetSydnea	k1gFnSc2
NSW	NSW	kA
2010	#num#	k4
Australia	Australium	k1gNnSc2
Phone	Phon	k1gInSc5
+61	+61	k4
2	#num#	k4
9320	#num#	k4
6000	#num#	k4
www	www	k?
australian	australian	k1gInSc1
museum	museum	k1gNnSc1
Copyright	copyright	k1gInSc1
©	©	k?
2021	#num#	k4
The	The	k1gMnPc2
Australian	Australian	k1gInSc1
Museum	museum	k1gNnSc4
ABN	ABN	kA
85	#num#	k4
407	#num#	k4
224	#num#	k4
698	#num#	k4
View	View	k1gFnPc2
Museum	museum	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnPc2
spread	spread	k6eAd1
of	of	k?
people	people	k6eAd1
to	ten	k3xDgNnSc1
Australia	Australia	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Australian	Australiany	k1gInPc2
Museum	museum	k1gNnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Třímetrové	třímetrový	k2eAgMnPc4d1
klokany	klokan	k1gMnPc4
a	a	k8xC
obří	obří	k2eAgMnPc4d1
wombaty	wombat	k1gMnPc4
vyhubil	vyhubit	k5eAaPmAgMnS
člověk	člověk	k1gMnSc1
<g/>
,	,	kIx,
ne	ne	k9
změna	změna	k1gFnSc1
klimatu	klima	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
National	National	k1gMnSc1
Geographic	Geographic	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
.	.	kIx.
<g/>
↑	↑	k?
Aboriginal	Aboriginal	k1gFnSc1
Australians	Australians	k1gInSc1
the	the	k?
oldest	oldest	k1gInSc1
culture	cultur	k1gMnSc5
on	on	k3xPp3gMnSc1
Earth	Earth	k1gMnSc1
-	-	kIx~
Australian	Australian	k1gMnSc1
Geographic	Geographic	k1gMnSc1
<g/>
.	.	kIx.
web.archive.org	web.archive.org	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013-05-18	2013-05-18	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
SÁENZ	SÁENZ	kA
<g/>
,	,	kIx,
Rogelio	Rogelio	k1gMnSc1
<g/>
;	;	kIx,
EMBRICK	EMBRICK	kA
<g/>
,	,	kIx,
David	David	k1gMnSc1
G.	G.	kA
<g/>
;	;	kIx,
RODRÍGUEZ	RODRÍGUEZ	kA
<g/>
,	,	kIx,
Néstor	Néstor	k1gMnSc1
P.	P.	kA
The	The	k1gMnSc1
International	International	k1gMnSc1
Handbook	handbook	k1gInSc1
of	of	k?
the	the	k?
Demography	Demographa	k1gFnSc2
of	of	k?
Race	Rac	k1gFnSc2
and	and	k?
Ethnicity	Ethnicita	k1gFnSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Springer	Springer	k1gInSc1
637	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
90	#num#	k4
<g/>
-	-	kIx~
<g/>
481	#num#	k4
<g/>
-	-	kIx~
<g/>
8891	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Google-Books-ID	Google-Books-ID	k1gFnSc1
<g/>
:	:	kIx,
v_bLCQAAQBAJ	v_bLCQAAQBAJ	k?
<g/>
.	.	kIx.
↑	↑	k?
STATISTICS	STATISTICS	kA
<g/>
,	,	kIx,
c	c	k0
<g/>
=	=	kIx~
<g/>
AU	au	k0
<g/>
;	;	kIx,
o	o	k7c6
<g/>
=	=	kIx~
<g/>
Commonwealth	Commonwealth	k1gInSc1
of	of	k?
Australia	Australia	k1gFnSc1
<g/>
;	;	kIx,
ou	ou	k0
<g/>
=	=	kIx~
<g/>
Australian	Australian	k1gInSc1
Bureau	Bureaus	k1gInSc2
of	of	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chapter	Chapter	k1gMnSc1
-	-	kIx~
Aboriginal	Aboriginal	k1gMnSc1
and	and	k?
Torres	Torres	k1gMnSc1
Strait	Strait	k1gMnSc1
Islander	Islander	k1gMnSc1
population	population	k1gInSc1
<g/>
.	.	kIx.
www.abs.gov.au	www.abs.gov.au	k6eAd1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2002-01-25	2002-01-25	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
What	What	k1gInSc1
is	is	k?
Aboriginal	Aboriginal	k1gMnSc5
Dreamtime	Dreamtim	k1gMnSc5
<g/>
?	?	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aboriginal-art-australia	Aboriginal-art-australia	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Jak	jak	k6eAd1
vznikl	vzniknout	k5eAaPmAgInS
svět	svět	k1gInSc1
podle	podle	k7c2
Aboriginců	Aboriginec	k1gInPc2
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2002-08-29	2002-08-29	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
GALVÁN	GALVÁN	kA
<g/>
,	,	kIx,
Javier	Javier	k1gMnSc1
A.	A.	kA
They	Thea	k1gFnPc1
Do	do	k7c2
What	Whata	k1gFnPc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
A	a	k8xC
Cultural	Cultural	k1gMnSc1
Encyclopedia	Encyclopedium	k1gNnSc2
of	of	k?
Extraordinary	Extraordinar	k1gInPc1
and	and	k?
Exotic	Exotice	k1gFnPc2
Customs	Customs	k1gInSc1
from	from	k1gMnSc1
around	around	k1gMnSc1
the	the	k?
World	World	k1gMnSc1
<g/>
:	:	kIx,
A	a	k8xC
Cultural	Cultural	k1gMnSc1
Encyclopedia	Encyclopedium	k1gNnSc2
of	of	k?
Extraordinary	Extraordinar	k1gInPc1
and	and	k?
Exotic	Exotice	k1gFnPc2
Customs	Customs	k1gInSc1
from	from	k1gMnSc1
around	around	k1gMnSc1
the	the	k?
World	World	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
ABC-CLIO	ABC-CLIO	k1gFnSc1
400	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
61069	#num#	k4
<g/>
-	-	kIx~
<g/>
342	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Google-Books-ID	Google-Books-ID	k1gFnSc1
<g/>
:	:	kIx,
e	e	k0
<g/>
2	#num#	k4
<g/>
RyBAAAQBAJ	RyBAAAQBAJ	k1gFnPc1
<g/>
.	.	kIx.
↑	↑	k?
Ancient	Ancient	k1gInSc1
Rock	rock	k1gInSc1
Art	Art	k1gFnSc1
Matches	Matches	k1gMnSc1
Tattoos	Tattoos	k1gMnSc1
:	:	kIx,
Discovery	Discover	k1gInPc1
News	Newsa	k1gFnPc2
:	:	kIx,
Discovery	Discover	k1gInPc1
Channel	Channela	k1gFnPc2
<g/>
.	.	kIx.
web.archive.org	web.archive.org	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2008-07-10	2008-07-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
BARBER	BARBER	kA
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
<g/>
;	;	kIx,
BARNES	BARNES	kA
<g/>
,	,	kIx,
Katherine	Katherin	k1gInSc5
<g/>
;	;	kIx,
ERSKINE	ERSKINE	kA
<g/>
,	,	kIx,
Dr	dr	kA
Nigel	Nigel	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mapping	Mapping	k1gInSc1
Our	Our	k1gFnSc2
World	Worlda	k1gFnPc2
<g/>
:	:	kIx,
Terra	Terra	k1gFnSc1
Incognita	Incognit	k2eAgFnSc1d1
To	to	k9
Australia	Australia	k1gFnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
National	National	k1gFnSc1
Library	Librara	k1gFnSc2
of	of	k?
Australia	Australia	k1gFnSc1
291	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
642	#num#	k4
<g/>
-	-	kIx~
<g/>
27809	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Google-Books-ID	Google-Books-ID	k1gFnSc1
<g/>
:	:	kIx,
uZ_sAQAAQBAJ	uZ_sAQAAQBAJ	k?
<g/>
.	.	kIx.
↑	↑	k?
Kdo	kdo	k3yQnSc1,k3yRnSc1,k3yInSc1
objevil	objevit	k5eAaPmAgMnS
Austrálii	Austrálie	k1gFnSc4
<g/>
?	?	kIx.
</s>
<s desamb="1">
|	|	kIx~
Zajímavosti	zajímavost	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2007-03-22	2007-03-22	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
SMITH	SMITH	kA
<g/>
,	,	kIx,
Claire	Clair	k1gMnSc5
<g/>
;	;	kIx,
BURKE	BURKE	kA
<g/>
,	,	kIx,
Heather	Heathra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Digging	Digging	k1gInSc1
It	It	k1gFnSc2
Up	Up	k1gMnSc1
Down	Down	k1gMnSc1
Under	Under	k1gMnSc1
<g/>
:	:	kIx,
A	A	kA
Practical	Practical	k1gMnSc1
Guide	Guid	k1gInSc5
to	ten	k3xDgNnSc1
Doing	Doing	k1gInSc1
Archaeology	Archaeolog	k1gMnPc4
in	in	k?
Australia	Australia	k1gFnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Springer	Springer	k1gInSc1
Science	Science	k1gFnSc2
&	&	k?
Business	business	k1gInSc1
Media	medium	k1gNnSc2
352	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
387	#num#	k4
<g/>
-	-	kIx~
<g/>
35263	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Google-Books-ID	Google-Books-ID	k1gFnSc1
<g/>
:	:	kIx,
0	#num#	k4
<g/>
HsRb_AY	HsRb_AY	k1gFnSc1
<g/>
9	#num#	k4
<g/>
jQC	jQC	k?
<g/>
.	.	kIx.
↑	↑	k?
BAER	BAER	kA
<g/>
,	,	kIx,
Joel	Joel	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pirates	Pirates	k1gMnSc1
of	of	k?
the	the	k?
British	British	k1gMnSc1
Isles	Isles	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Stroud	Stroud	k1gInSc1
<g/>
,	,	kIx,
Gloucestershire	Gloucestershir	k1gMnSc5
:	:	kIx,
Tempus	Tempus	k1gInSc1
266	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
7524	#num#	k4
<g/>
-	-	kIx~
<g/>
2304	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
GOUCHER	GOUCHER	kA
<g/>
,	,	kIx,
Candice	Candice	k1gFnSc1
<g/>
;	;	kIx,
WALTON	WALTON	kA
<g/>
,	,	kIx,
Linda	Linda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
World	World	k1gInSc1
History	Histor	k1gInPc1
<g/>
:	:	kIx,
Journeys	Journeys	k1gInSc1
from	from	k1gInSc1
Past	past	k1gFnSc1
to	ten	k3xDgNnSc4
Present	Present	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Routledge	Routledge	k1gInSc1
769	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
135	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8829	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Google-Books-ID	Google-Books-ID	k1gFnSc1
<g/>
:	:	kIx,
O_	O_	k1gFnSc1
<g/>
3	#num#	k4
<g/>
fCgAAQBAJ	fCgAAQBAJ	k?
<g/>
.	.	kIx.
↑	↑	k?
EGAN	EGAN	kA
<g/>
,	,	kIx,
Ted	Ted	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Land	Land	k1gMnSc1
Downunder	Downunder	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Grice	Grice	k1gMnSc1
Chapman	Chapman	k1gMnSc1
Publishing	Publishing	k1gInSc4
238	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
9545726	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Google-Books-ID	Google-Books-ID	k1gFnSc1
<g/>
:	:	kIx,
ND	ND	kA
<g/>
3	#num#	k4
<g/>
OqVdOwqoC	OqVdOwqoC	k1gFnPc1
<g/>
.	.	kIx.
↑	↑	k?
MSN	MSN	kA
Encarta	Encart	k1gMnSc2
-	-	kIx~
Multimedia	multimedium	k1gNnSc2
-	-	kIx~
Smallpox	Smallpox	k1gInSc1
Through	Through	k1gInSc1
History	Histor	k1gInPc1
<g/>
.	.	kIx.
web.archive.org	web.archive.org	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2004-06-18	2004-06-18	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
EDWARDS	EDWARDS	kA
<g/>
,	,	kIx,
William	William	k1gInSc4
Howell	Howella	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
An	An	k1gFnPc2
Introduction	Introduction	k1gInSc4
to	ten	k3xDgNnSc4
Aboriginal	Aboriginal	k1gFnSc7
Societies	Societies	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Social	Social	k1gMnSc1
Science	Science	k1gFnSc2
Press	Press	k1gInSc1
172	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
876633	#num#	k4
<g/>
-	-	kIx~
<g/>
89	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
James	James	k1gMnSc1
Cook	Cook	k1gMnSc1
-	-	kIx~
slavný	slavný	k2eAgMnSc1d1
mořeplavec	mořeplavec	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
HedvabnaStezka	HedvabnaStezka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
První	první	k4xOgFnSc7
osadou	osada	k1gFnSc7
v	v	k7c6
Austrálii	Austrálie	k1gFnSc6
bylo	být	k5eAaImAgNnS
Sydney	Sydney	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČT24	ČT24	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
National	National	k1gMnSc1
Museum	museum	k1gNnSc1
of	of	k?
Australia	Australius	k1gMnSc2
-	-	kIx~
Eureka	Eureek	k1gMnSc2
Stockade	Stockad	k1gInSc5
<g/>
.	.	kIx.
www.nma.gov.au	www.nma.gov.aus	k1gInSc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Austrálie	Austrálie	k1gFnSc1
<g/>
:	:	kIx,
Ukradená	ukradený	k2eAgFnSc1d1
země	země	k1gFnSc1
<g/>
.	.	kIx.
100	#num#	k4
<g/>
+	+	kIx~
<g/>
1	#num#	k4
zahraniční	zahraniční	k2eAgFnSc4d1
zajímavost	zajímavost	k1gFnSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013-04-27	2013-04-27	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
White	Whit	k1gInSc5
Australia	Australius	k1gMnSc2
policy	polic	k2eAgFnPc1d1
|	|	kIx~
Summary	Summara	k1gFnPc1
&	&	k?
Facts	Facts	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Encyclopedia	Encyclopedium	k1gNnSc2
Britannica	Britannicum	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Henry	henry	k1gInSc1
Parkes	Parkesa	k1gFnPc2
<g/>
,	,	kIx,
Father	Fathra	k1gFnPc2
of	of	k?
Australian	Australian	k1gInSc1
Federation	Federation	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Inside	Insid	k1gInSc5
the	the	k?
Collection	Collection	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Gallipoli	Gallipoli	k1gNnSc1
bylo	být	k5eAaImAgNnS
koncem	koncem	k7c2
australské	australský	k2eAgFnSc2d1
puberty	puberta	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
bitvě	bitva	k1gFnSc6
ztratila	ztratit	k5eAaPmAgFnS
tisíce	tisíc	k4xCgInSc2
mužů	muž	k1gMnPc2
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2015-04-26	2015-04-26	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Austrálie	Austrálie	k1gFnSc2
za	za	k7c4
2	#num#	k4
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
www.druhasvetova.com	www.druhasvetova.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Battle	Battle	k1gFnSc1
of	of	k?
Milne	Miln	k1gMnSc5
Bay	Bay	k1gMnSc5
<g/>
.	.	kIx.
www.awm.gov.au	www.awm.gov.au	k5eAaPmIp1nS
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Out	Out	k1gFnSc1
in	in	k?
the	the	k?
Cold	Cold	k1gInSc1
<g/>
:	:	kIx,
Australia	Australia	k1gFnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
involvement	involvement	k1gInSc1
in	in	k?
the	the	k?
Korean	Korean	k1gMnSc1
War	War	k1gMnSc1
|	|	kIx~
The	The	k1gMnSc1
Australian	Australian	k1gMnSc1
War	War	k1gMnSc1
Memorial	Memorial	k1gMnSc1
<g/>
.	.	kIx.
www.awm.gov.au	www.awm.gov.au	k6eAd1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Australian	Australian	k1gMnSc1
Involvement	Involvement	k1gMnSc1
In	In	k1gMnSc1
The	The	k1gMnSc1
Vietnam	Vietnam	k1gInSc4
War	War	k1gFnSc2
<g/>
.	.	kIx.
rslnsw	rslnsw	k?
<g/>
.	.	kIx.
<g/>
org	org	k?
<g/>
.	.	kIx.
<g/>
au	au	k0
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
John	John	k1gMnSc1
Howard	Howard	k1gMnSc1
defends	defends	k1gInSc4
Australia	Australium	k1gNnSc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
involvement	involvement	k1gInSc1
in	in	k?
Iraq	Iraq	k1gFnSc2
war	war	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
SBS	SBS	kA
News	Newsa	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
(	(	kIx(
<g/>
PDF	PDF	kA
<g/>
)	)	kIx)
Immigration	Immigration	k1gInSc1
policy	polica	k1gFnSc2
under	under	k1gMnSc1
the	the	k?
Howard	Howard	k1gMnSc1
Government	Government	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ResearchGate	ResearchGat	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Julia	Julius	k1gMnSc2
Gillardová	Gillardová	k1gFnSc1
povede	vést	k5eAaImIp3nS,k5eAaPmIp3nS
jako	jako	k9
první	první	k4xOgFnSc1
žena	žena	k1gFnSc1
australský	australský	k2eAgInSc1d1
kabinet	kabinet	k1gInSc1
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
LESLIE	LESLIE	kA
<g/>
,	,	kIx,
Reporters	Reporters	k1gInSc1
<g/>
:	:	kIx,
Tim	Tim	k?
<g/>
;	;	kIx,
LIDDY	LIDDY	kA
<g/>
,	,	kIx,
Mark	Mark	k1gMnSc1
Corcoran	Corcoran	k1gInSc1
<g/>
;	;	kIx,
Designer	Designer	k1gInSc1
<g/>
:	:	kIx,
Ben	Ben	k1gInSc1
Spraggon	Spraggon	k1gMnSc1
<g/>
;	;	kIx,
Developer	developer	k1gMnSc1
<g/>
:	:	kIx,
Colin	Colin	k1gMnSc1
Gourlay	Gourlaa	k1gFnSc2
<g/>
;	;	kIx,
Editor	editor	k1gInSc1
<g/>
:	:	kIx,
Matthew	Matthew	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Operation	Operation	k1gInSc1
Sovereign	sovereign	k1gInSc4
Borders	Borders	k1gInSc4
-	-	kIx~
the	the	k?
first	first	k1gInSc1
six	six	k?
months	months	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ABC	ABC	kA
News	Newsa	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-03-26	2014-03-26	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
SHORT	SHORT	kA
<g/>
,	,	kIx,
Philip	Philip	k1gMnSc1
S.	S.	kA
<g/>
;	;	kIx,
SOCIETY	societa	k1gFnPc1
<g/>
,	,	kIx,
Australian	Australian	k1gInSc1
Systematic	Systematice	k1gFnPc2
Botany	Botana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
History	Histor	k1gInPc7
of	of	k?
Systematic	Systematice	k1gFnPc2
Botany	Botana	k1gFnSc2
in	in	k?
Australasia	Australasia	k1gFnSc1
<g/>
:	:	kIx,
Proceedings	Proceedings	k1gInSc1
of	of	k?
a	a	k8xC
Symposium	symposium	k1gNnSc1
Held	Helda	k1gFnPc2
at	at	k?
the	the	k?
University	universita	k1gFnSc2
of	of	k?
Melbourne	Melbourne	k1gNnSc1
<g/>
,	,	kIx,
25-27	25-27	k4
May	May	k1gMnSc1
1988	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Australian	Australian	k1gInSc1
Systematic	Systematice	k1gFnPc2
Botany	Botana	k1gFnSc2
Society	societa	k1gFnSc2
344	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
7316	#num#	k4
<g/>
-	-	kIx~
<g/>
8463	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Google-Books-ID	Google-Books-ID	k1gFnSc1
<g/>
:	:	kIx,
s	s	k7c7
<g/>
4	#num#	k4
<g/>
s_AAAAYAAJ	s_AAAAYAAJ	k?
<g/>
.	.	kIx.
↑	↑	k?
LAMBERTINI	LAMBERTINI	kA
<g/>
,	,	kIx,
Marco	Marco	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
Naturalist	Naturalist	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Guide	Guid	k1gInSc5
to	ten	k3xDgNnSc1
the	the	k?
Tropics	Tropics	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
University	universita	k1gFnPc1
of	of	k?
Chicago	Chicago	k1gNnSc4
Press	Press	k1gInSc1
338	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
226	#num#	k4
<g/>
-	-	kIx~
<g/>
46828	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Google-Books-ID	Google-Books-ID	k1gFnSc1
<g/>
:	:	kIx,
5	#num#	k4
<g/>
MSqbJIVDWkC	MSqbJIVDWkC	k1gFnPc2
<g/>
.	.	kIx.
↑	↑	k?
Fact	Fact	k1gMnSc1
check	check	k1gMnSc1
<g/>
:	:	kIx,
Are	ar	k1gInSc5
feral	feral	k1gMnSc1
cats	catsa	k1gFnPc2
killing	killing	k1gInSc1
over	over	k1gMnSc1
20	#num#	k4
billion	billion	k1gInSc1
native	natiev	k1gFnSc2
animals	animalsa	k1gFnPc2
a	a	k8xC
year	yeara	k1gFnPc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
ABC	ABC	kA
News	Newsa	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-11-13	2014-11-13	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
About	About	k1gInSc1
the	the	k?
EPBC	EPBC	kA
Act	Act	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Department	department	k1gInSc1
of	of	k?
Agriculture	Agricultur	k1gMnSc5
<g/>
,	,	kIx,
Water	Watero	k1gNnPc2
and	and	k?
the	the	k?
Environment	Environment	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
2020	#num#	k4
EPI	EPI	kA
Results	Resultsa	k1gFnPc2
|	|	kIx~
Environmental	Environmental	k1gMnSc1
Performance	performance	k1gFnSc2
Index	index	k1gInSc1
<g/>
.	.	kIx.
epi	epi	k?
<g/>
.	.	kIx.
<g/>
yale	yale	k1gFnSc1
<g/>
.	.	kIx.
<g/>
edu	edu	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Volby	volba	k1gFnPc1
v	v	k7c6
Austrálii	Austrálie	k1gFnSc6
s	s	k7c7
převahou	převaha	k1gFnSc7
vyhrála	vyhrát	k5eAaPmAgFnS
pravice	pravice	k1gFnSc1
<g/>
,	,	kIx,
labouristé	labourista	k1gMnPc1
doplatili	doplatit	k5eAaPmAgMnP
na	na	k7c4
rozkol	rozkol	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2013-09-07	2013-09-07	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Sedmý	sedmý	k4xOgInSc4
premiér	premiéra	k1gFnPc2
za	za	k7c4
11	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Austrálie	Austrálie	k1gFnSc1
bude	být	k5eAaImBp3nS
mít	mít	k5eAaImF
opět	opět	k6eAd1
nového	nový	k2eAgMnSc4d1
předsedu	předseda	k1gMnSc4
vlády	vláda	k1gFnSc2
|	|	kIx~
Svět	svět	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-08-24	2018-08-24	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
5368.0	5368.0	k4
–	–	k?
International	International	k1gMnSc5
Trade	Trad	k1gMnSc5
in	in	k?
Goods	Goods	k1gInSc1
and	and	k?
Services	Services	k1gInSc1
<g/>
,	,	kIx,
Australia	Australia	k1gFnSc1
<g/>
,	,	kIx,
April	April	k1gInSc1
2007	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Australian	Australian	k1gMnSc1
Bureau	Bureaus	k1gInSc2
of	of	k?
Statistics	Statisticsa	k1gFnPc2
<g/>
,	,	kIx,
31	#num#	k4
May	May	k1gMnSc1
2007	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Report	report	k1gInSc1
for	forum	k1gNnPc2
Selected	Selected	k1gMnSc1
Countries	Countries	k1gMnSc1
and	and	k?
Subjects	Subjects	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
IMF	IMF	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Report	report	k1gInSc1
for	forum	k1gNnPc2
Selected	Selected	k1gMnSc1
Countries	Countries	k1gMnSc1
and	and	k?
Subjects	Subjects	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
IMF	IMF	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Austrálie	Austrálie	k1gFnSc1
<g/>
:	:	kIx,
Základní	základní	k2eAgFnSc1d1
charakteristika	charakteristika	k1gFnSc1
teritoria	teritorium	k1gNnSc2
<g/>
,	,	kIx,
ekonomický	ekonomický	k2eAgInSc1d1
přehled	přehled	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
BusinessInfo	BusinessInfo	k1gNnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
-	-	kIx~
Ministerstvo	ministerstvo	k1gNnSc1
zahraničních	zahraniční	k2eAgFnPc2d1
věcí	věc	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
DEYL	DEYL	kA
<g/>
,	,	kIx,
Daniel	Daniel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dědicové	dědic	k1gMnPc1
krále	král	k1gMnSc2
Midase	Midas	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Týden	týden	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leden	leden	k1gInSc1
2009	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
2	#num#	k4
<g/>
/	/	kIx~
<g/>
2009	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
34	#num#	k4
<g/>
-	-	kIx~
<g/>
39	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Welcome	Welcom	k1gInSc5
to	ten	k3xDgNnSc4
wineaustralia	wineaustralia	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
web.archive.org	web.archive.org	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2010-10-23	2010-10-23	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Austrálie	Austrálie	k1gFnSc2
<g/>
:	:	kIx,
Zahraniční	zahraniční	k2eAgInSc1d1
obchod	obchod	k1gInSc1
a	a	k8xC
investice	investice	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
BusinessInfo	BusinessInfo	k1gNnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
-	-	kIx~
Ministerstvo	ministerstvo	k1gNnSc1
zahraničních	zahraniční	k2eAgFnPc2d1
věcí	věc	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
On	on	k3xPp3gMnSc1
the	the	k?
International	International	k1gMnSc1
Realignment	Realignment	k1gMnSc1
of	of	k?
Exchanges	Exchanges	k1gMnSc1
and	and	k?
Related	Related	k1gInSc1
Trends	Trends	k1gInSc1
in	in	k?
Self-Regulation	Self-Regulation	k1gInSc1
<g/>
.	.	kIx.
web.archive.org	web.archive.org	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2010-12-13	2010-12-13	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Country	country	k2eAgInSc4d1
Rankings	Rankings	k1gInSc4
<g/>
:	:	kIx,
World	World	k1gMnSc1
&	&	k?
Global	globat	k5eAaImAgMnS
Economy	Econom	k1gInPc4
Rankings	Rankings	k1gInSc4
on	on	k3xPp3gMnSc1
Economic	Economic	k1gMnSc1
Freedom	Freedom	k1gInSc1
<g/>
.	.	kIx.
www.heritage.org	www.heritage.org	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Human	Human	k1gMnSc1
Development	Development	k1gMnSc1
Report	report	k1gInSc4
2019	#num#	k4
<g/>
.	.	kIx.
www.hdr.undp.org	www.hdr.undp.org	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
OSN	OSN	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
LOCKE	LOCKE	kA
<g/>
,	,	kIx,
Taylor	Taylor	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
These	these	k1gFnSc1
are	ar	k1gInSc5
the	the	k?
world	worldo	k1gNnPc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
most	most	k1gInSc1
liveable	liveable	k6eAd1
cities	cities	k1gInSc1
in	in	k?
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
CNBC	CNBC	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-09-04	2019-09-04	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
The	The	k1gMnSc1
Beach	Beach	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Department	department	k1gInSc1
of	of	k?
the	the	k?
Environment	Environment	k1gInSc1
<g/>
,	,	kIx,
Water	Water	k1gInSc1
<g/>
,	,	kIx,
Heritage	Heritage	k1gInSc1
and	and	k?
the	the	k?
Arts	Arts	k1gInSc1
<g/>
,	,	kIx,
Commonwealth	Commonwealth	k1gInSc1
of	of	k?
Australia	Australia	k1gFnSc1
<g/>
,	,	kIx,
17	#num#	k4
March	March	k1gInSc1
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
26	#num#	k4
February	Februara	k1gFnSc2
2010	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
2016	#num#	k4
Census	census	k1gInSc1
QuickStats	QuickStats	k1gInSc4
<g/>
:	:	kIx,
Australia	Australia	k1gFnSc1
<g/>
.	.	kIx.
quickstats	quickstats	k1gInSc1
<g/>
.	.	kIx.
<g/>
censusdata	censusdata	k1gFnSc1
<g/>
.	.	kIx.
<g/>
abs	abs	k?
<g/>
.	.	kIx.
<g/>
gov	gov	k?
<g/>
.	.	kIx.
<g/>
au	au	k0
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Population	Population	k1gInSc1
clock	clock	k1gInSc1
<g/>
.	.	kIx.
www.abs.gov.au	www.abs.gov.au	k6eAd1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-12-16	2020-12-16	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Australia	Australia	k1gFnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
population	population	k1gInSc1
to	ten	k3xDgNnSc1
grow	grow	k?
to	ten	k3xDgNnSc1
42	#num#	k4
million	million	k1gInSc4
by	by	k9
2050	#num#	k4
<g/>
,	,	kIx,
modelling	modelling	k1gInSc1
shows	shows	k6eAd1
<g/>
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.immi.gov.au	www.immi.gov.au	k6eAd1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Regional	Regional	k1gFnSc1
population	population	k1gInSc1
<g/>
,	,	kIx,
2018-19	2018-19	k4
financial	financial	k1gInSc1
year	year	k1gInSc4
|	|	kIx~
Australian	Australian	k1gInSc1
Bureau	Bureaus	k1gInSc2
of	of	k?
Statistics	Statistics	k1gInSc1
<g/>
.	.	kIx.
www.abs.gov.au	www.abs.gov.au	k6eAd1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-03-25	2020-03-25	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
CULTURAL	CULTURAL	kA
DIVERSITY	DIVERSITY	kA
IN	IN	kA
AUSTRALIA	AUSTRALIA	kA
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
.	.	kIx.
2071.0	2071.0	k4
-	-	kIx~
Census	census	k1gInSc1
of	of	k?
Population	Population	k1gInSc1
and	and	k?
Housing	Housing	k1gInSc1
<g/>
:	:	kIx,
Reflecting	Reflecting	k1gInSc1
Australia	Australius	k1gMnSc2
-	-	kIx~
Stories	Stories	k1gMnSc1
from	from	k1gMnSc1
the	the	k?
Census	census	k1gInSc1
<g/>
,	,	kIx,
2016	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-06-28	2017-06-28	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Pama-Nyungan	Pama-Nyungan	k1gMnSc1
languages	languages	k1gMnSc1
|	|	kIx~
linguistics	linguistics	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Encyclopedia	Encyclopedium	k1gNnSc2
Britannica	Britannicum	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Maltralian	Maltralian	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Maltese	Maltese	k1gFnSc2
ethnolect	ethnolect	k1gMnSc1
of	of	k?
Australia	Australia	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Times	Times	k1gMnSc1
of	of	k?
Malta	Malta	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Keeping	Keeping	k1gInSc1
SA	SA	kA
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Barossa	Barossa	k1gFnSc1
Deutsch	Deutsch	k1gInSc4
alive	alivat	k5eAaPmIp3nS
over	over	k1gMnSc1
kaffee	kaffe	k1gFnSc2
und	und	k?
kuchen	kuchen	k1gInSc1
<g/>
.	.	kIx.
www.abc.net.au	www.abc.net.au	k6eAd1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-03-25	2017-03-25	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
http://www.abs.gov.au/ausstats/abs@.nsf/Lookup/2071.0main+features902012-2013	http://www.abs.gov.au/ausstats/abs@.nsf/Lookup/2071.0main+features902012-2013	k4
<g/>
↑	↑	k?
How	How	k1gMnSc4
Australia	Australius	k1gMnSc4
compares	compares	k1gMnSc1
<g/>
.	.	kIx.
web.archive.org	web.archive.org	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-03-12	2011-03-12	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Skin	skin	k1gMnSc1
Cancer	Cancer	k1gMnSc1
-	-	kIx~
Key	Key	k1gFnSc1
statistics	statistics	k1gInSc1
<g/>
.	.	kIx.
web.archive.org	web.archive.org	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-02-08	2014-02-08	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnPc4d1
online	onlin	k1gInSc5
</s>
<s>
<g/>
↑	↑	k?
Smoking	smoking	k1gInSc1
-	-	kIx~
A	a	k8xC
Leading	Leading	k1gInSc1
Cause	causa	k1gFnSc3
of	of	k?
Death	Death	k1gInSc1
<g/>
.	.	kIx.
web.archive.org	web.archive.org	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-02-19	2011-02-19	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Department	department	k1gInSc1
of	of	k?
Health	Health	k1gInSc1
and	and	k?
Ageing	Ageing	k1gInSc1
-	-	kIx~
About	About	k2eAgInSc1d1
Overweight	Overweight	k1gInSc1
and	and	k?
Obesity	obesita	k1gFnSc2
<g/>
.	.	kIx.
web.archive.org	web.archive.org	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2010-05-07	2010-05-07	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
About	About	k1gInSc1
Australia	Australia	k1gFnSc1
<g/>
:	:	kIx,
Health	Health	k1gInSc1
Care	car	k1gMnSc5
in	in	k?
Australia	Australium	k1gNnPc1
<g/>
.	.	kIx.
web.archive.org	web.archive.org	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2010-04-04	2010-04-04	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Solo	Solo	k1gMnSc1
Aussie	Aussie	k1gFnSc2
designer	designer	k1gMnSc1
Glenn	Glenn	k1gMnSc1
Murcutt	Murcutt	k1gMnSc1
wins	wins	k6eAd1
Pritzker	Pritzker	k1gInSc4
Prize	Prize	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Architects	Architects	k1gInSc1
Journal	Journal	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Austrálie	Austrálie	k1gFnSc1
-	-	kIx~
dědictví	dědictví	k1gNnSc1
UNESCO	Unesco	k1gNnSc1
<g/>
:	:	kIx,
Austrálie	Austrálie	k1gFnSc1
<g/>
:	:	kIx,
Můj	můj	k3xOp1gMnSc1
průvodce	průvodce	k1gMnSc1
<g/>
.	.	kIx.
www.mujpruvodce.cz	www.mujpruvodce.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
Canberra	Canberro	k1gNnSc2
<g/>
:	:	kIx,
Australskou	australský	k2eAgFnSc7d1
metropolí	metropol	k1gFnSc7
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
vesnice	vesnice	k1gFnSc1
o	o	k7c6
pár	pár	k4xCyI
číslech	číslo	k1gNnPc6
<g/>
!	!	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Epochaplus	Epochaplus	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Sidney	Sidney	k1gInPc1
Nolan	Nolan	k1gInSc1
|	|	kIx~
artnet	artnet	k1gInSc1
<g/>
.	.	kIx.
www.artnet.com	www.artnet.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
The	The	k1gMnSc4
Nobel	Nobel	k1gMnSc1
Prize	Prize	k1gFnSc2
in	in	k?
Literature	Literatur	k1gMnSc5
1973	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
NobelPrize	NobelPrize	k1gFnSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
HOFMANOVÁ	Hofmanová	k1gFnSc1
<g/>
,	,	kIx,
Adéla	Adéla	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bolestné	bolestné	k1gNnSc1
tajemství	tajemství	k1gNnSc1
Mary	Mary	k1gFnSc2
Poppins	Poppinsa	k1gFnPc2
<g/>
.	.	kIx.
www.krajskelisty.cz	www.krajskelisty.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
HARLAND	HARLAND	kA
<g/>
,	,	kIx,
Robert	Robert	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aussie	Aussie	k1gFnSc1
meat	meata	k1gFnPc2
pies	pies	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sunstar	Sunstar	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016-10-04	2016-10-04	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
<g/>
.	.	kIx.
(	(	kIx(
<g/>
English	English	k1gInSc1
<g/>
)	)	kIx)
↑	↑	k?
SANTICH	SANTICH	kA
<g/>
,	,	kIx,
Barbara	Barbara	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bold	Bold	k1gMnSc1
Palates	Palates	k1gMnSc1
<g/>
:	:	kIx,
Australia	Australia	k1gFnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Gastronomic	Gastronomice	k1gFnPc2
Heritage	Heritage	k1gFnSc7
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Wakefield	Wakefield	k1gInSc1
Press	Press	k1gInSc1
338	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
74305	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
94	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Google-Books-ID	Google-Books-ID	k1gFnSc1
<g/>
:	:	kIx,
RkHKFAzc_	RkHKFAzc_	k1gFnSc1
<g/>
5	#num#	k4
<g/>
AC	AC	kA
<g/>
.	.	kIx.
↑	↑	k?
DUNSTAN	DUNSTAN	kA
<g/>
,	,	kIx,
David	David	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
McEncroe	McEncroe	k1gFnSc1
<g/>
,	,	kIx,
Francis	Francis	k1gFnSc1
Gerald	Gerald	k1gMnSc1
(	(	kIx(
<g/>
Frank	Frank	k1gMnSc1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
1908	#num#	k4
<g/>
–	–	k?
<g/>
1979	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Canberra	Canberra	k1gFnSc1
<g/>
:	:	kIx,
National	National	k1gFnSc1
Centre	centr	k1gInSc5
of	of	k?
Biography	Biographa	k1gFnPc1
<g/>
,	,	kIx,
Australian	Australian	k1gInSc1
National	National	k1gFnSc2
University	universita	k1gFnSc2
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
How	How	k1gFnSc1
the	the	k?
flat	flat	k1gInSc4
white	white	k5eAaPmIp2nP
conquered	conquered	k1gInSc4
the	the	k?
UK	UK	kA
coffee	coffee	k6eAd1
scene	scenout	k5eAaImIp3nS,k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Independent	independent	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-04-10	2018-04-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Zemřel	zemřít	k5eAaPmAgMnS
vynálezce	vynálezce	k1gMnSc1
černé	černý	k2eAgFnSc2d1
skříňky	skříňka	k1gFnSc2
|	|	kIx~
Noviny	novina	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2010-07-22	2010-07-22	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
QS	QS	kA
World	World	k1gInSc4
University	universita	k1gFnSc2
Rankings	Rankings	k1gInSc4
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Top	topit	k5eAaImRp2nS
Universities	Universities	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-05-28	2020-05-28	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
https://www.si.com/nba/photos/2011/07/23wnbas-top-15-players-of-all-time#6	https://www.si.com/nba/photos/2011/07/23wnbas-top-15-players-of-all-time#6	k4
<g/>
↑	↑	k?
Australian	Australian	k1gMnSc1
Cadel	Cadel	k1gMnSc1
Evans	Evansa	k1gFnPc2
wins	wins	k1gInSc1
Tour	Tour	k1gMnSc1
de	de	k?
France	Franc	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Independent	independent	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
SHAW	SHAW	kA
<g/>
,	,	kIx,
John	John	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sir	sir	k1gMnSc1
Donald	Donald	k1gMnSc1
Bradman	Bradman	k1gMnSc1
<g/>
,	,	kIx,
92	#num#	k4
<g/>
,	,	kIx,
Cricket	Cricket	k1gInSc1
Legend	legenda	k1gFnPc2
<g/>
,	,	kIx,
Dies	Dies	k1gInSc1
(	(	kIx(
<g/>
Published	Published	k1gInSc1
2001	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
New	New	k1gFnSc2
York	York	k1gInSc1
Times	Times	k1gMnSc1
<g/>
.	.	kIx.
2001	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
362	#num#	k4
<g/>
-	-	kIx~
<g/>
4331	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
http://www.fih.ch/global-stars/hockey-stars/player-of-the-year-past-winners/	http://www.fih.ch/global-stars/hockey-stars/player-of-the-year-past-winners/	k?
<g/>
↑	↑	k?
Plavecký	plavecký	k2eAgInSc1d1
fenomén	fenomén	k1gInSc1
Thorpe	Thorp	k1gInSc5
ukončil	ukončit	k5eAaPmAgMnS
kariéru	kariéra	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2006-11-21	2006-11-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
BLAINEY	BLAINEY	kA
<g/>
,	,	kIx,
Geoffrey	Geoffrey	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc4
Austrálie	Austrálie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Lidové	lidový	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7106	#num#	k4
<g/>
-	-	kIx~
<g/>
334	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Austrálie	Austrálie	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Austrálie	Austrálie	k1gFnSc2
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Kategorie	kategorie	k1gFnPc1
Austrálie	Austrálie	k1gFnSc2
ve	v	k7c6
Wikizprávách	Wikizpráva	k1gFnPc6
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
na	na	k7c4
OpenStreetMap	OpenStreetMap	k1gInSc4
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gNnSc7
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Austrálie	Austrálie	k1gFnSc2
</s>
<s>
Australia	Australia	k1gFnSc1
-	-	kIx~
Amnesty	Amnest	k1gInPc1
International	International	k1gFnSc1
Report	report	k1gInSc1
2011	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Amnesty	Amnest	k1gInPc4
International	International	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Australia	Australia	k1gFnSc1
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Freedom	Freedom	k1gInSc1
House	house	k1gNnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
-	-	kIx~
o	o	k7c6
českých	český	k2eAgMnPc6d1
krajanech	krajan	k1gMnPc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ministerstvo	ministerstvo	k1gNnSc1
zahraničních	zahraniční	k2eAgFnPc2d1
věcí	věc	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
2002-05-16	2002-05-16	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Bureau	Bureau	k6eAd1
of	of	k?
East	East	k1gMnSc1
Asian	Asian	k1gMnSc1
and	and	k?
Pacific	Pacific	k1gMnSc1
Affairs	Affairs	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Background	Background	k1gInSc1
Note	Not	k1gInSc2
<g/>
:	:	kIx,
Australia	Australia	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
U.	U.	kA
<g/>
S.	S.	kA
Department	department	k1gInSc1
of	of	k?
State	status	k1gInSc5
<g/>
,	,	kIx,
2011-08-10	2011-08-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
CIA	CIA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
World	World	k1gMnSc1
Factbook	Factbook	k1gInSc1
-	-	kIx~
Australia	Australia	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rev	Rev	k1gFnSc1
<g/>
.	.	kIx.
2011-07-05	2011-07-05	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Library	Librar	k1gInPc1
of	of	k?
Congress	Congress	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Country	country	k2eAgInSc1d1
Profile	profil	k1gInSc5
<g/>
:	:	kIx,
Australia	Australium	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2006-02-01	2006-02-01	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Souhrnná	souhrnný	k2eAgFnSc1d1
teritoriální	teritoriální	k2eAgFnSc1d1
informace	informace	k1gFnSc1
<g/>
:	:	kIx,
Austrálie	Austrálie	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Businessinfo	Businessinfo	k1gNnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2011-04-11	2011-04-11	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
LANGE	LANGE	kA
<g/>
,	,	kIx,
Robert	Robert	k1gMnSc1
Terence	Terence	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Australia	Australia	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Encyclopaedia	Encyclopaedium	k1gNnSc2
Britannica	Britannicum	k1gNnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Austrálie	Austrálie	k1gFnSc1
–	–	k?
Commonwealth	Commonwealth	k1gInSc1
of	of	k?
Australia	Australia	k1gFnSc1
–	–	k?
(	(	kIx(
<g/>
AUS	AUS	kA
<g/>
)	)	kIx)
Státy	stát	k1gInPc1
</s>
<s>
Jižní	jižní	k2eAgFnSc2d1
Austrálie	Austrálie	k1gFnSc2
•	•	k?
Nový	nový	k2eAgInSc1d1
Jižní	jižní	k2eAgInSc1d1
Wales	Wales	k1gInSc1
•	•	k?
Queensland	Queensland	k1gInSc1
•	•	k?
Tasmánie	Tasmánie	k1gFnSc1
•	•	k?
Victoria	Victorium	k1gNnSc2
•	•	k?
Západní	západní	k2eAgFnSc2d1
Austrálie	Austrálie	k1gFnSc2
Teritoria	teritorium	k1gNnSc2
</s>
<s>
Severní	severní	k2eAgNnSc1d1
teritorium	teritorium	k1gNnSc1
•	•	k?
Teritorium	teritorium	k1gNnSc1
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
•	•	k?
Teritorium	teritorium	k1gNnSc4
Jervisova	Jervisův	k2eAgFnSc1d1
zátoka	zátoka	k1gFnSc1
Ostrovní	ostrovní	k2eAgNnSc4d1
území	území	k1gNnSc4
Austrálie	Austrálie	k1gFnSc2
(	(	kIx(
<g/>
seznam	seznam	k1gInSc1
ostrovů	ostrov	k1gInPc2
<g/>
)	)	kIx)
</s>
<s>
Ashmorův	Ashmorův	k2eAgInSc1d1
a	a	k8xC
Cartierův	Cartierův	k2eAgInSc1d1
ostrov	ostrov	k1gInSc1
•	•	k?
Australské	australský	k2eAgNnSc1d1
antarktické	antarktický	k2eAgNnSc1d1
území	území	k1gNnSc1
•	•	k?
Kokosové	kokosový	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
•	•	k?
Heardův	Heardův	k2eAgMnSc1d1
a	a	k8xC
MacDonaldovy	Macdonaldův	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
•	•	k?
Ostrovy	ostrov	k1gInPc4
Korálového	korálový	k2eAgNnSc2d1
moře	moře	k1gNnSc2
•	•	k?
Norfolk	Norfolk	k1gInSc1
•	•	k?
Vánoční	vánoční	k2eAgInSc1d1
ostrov	ostrov	k1gInSc1
</s>
<s>
Commonwealth	Commonwealth	k1gMnSc1
realms	realms	k6eAd1
</s>
<s>
Antigua	Antigua	k1gMnSc1
a	a	k8xC
Barbuda	Barbuda	k1gMnSc1
•	•	k?
Austrálie	Austrálie	k1gFnSc2
•	•	k?
Bahamy	Bahamy	k1gFnPc4
•	•	k?
Barbados	Barbados	k1gMnSc1
•	•	k?
Belize	Belize	k1gFnSc2
•	•	k?
Grenada	Grenada	k1gFnSc1
•	•	k?
Jamajka	Jamajka	k1gFnSc1
•	•	k?
Kanada	Kanada	k1gFnSc1
•	•	k?
Nový	nový	k2eAgInSc1d1
Zéland	Zéland	k1gInSc1
•	•	k?
Papua	Papuum	k1gNnSc2
Nová	nový	k2eAgFnSc1d1
Guinea	Guinea	k1gFnSc1
•	•	k?
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
•	•	k?
Svatá	svatat	k5eAaImIp3nS
Lucie	Lucie	k1gFnSc1
•	•	k?
Svatý	svatý	k2eAgMnSc1d1
Kryštof	Kryštof	k1gMnSc1
a	a	k8xC
Nevis	viset	k5eNaImRp2nS
•	•	k?
Svatý	svatý	k2eAgMnSc1d1
Vincenc	Vincenc	k1gMnSc1
a	a	k8xC
Grenadiny	grenadina	k1gFnPc1
•	•	k?
Šalomounovy	Šalomounův	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
•	•	k?
Tuvalu	Tuval	k1gInSc2
</s>
<s>
Státy	stát	k1gInPc1
a	a	k8xC
území	území	k1gNnPc1
v	v	k7c6
Austrálii	Austrálie	k1gFnSc6
a	a	k8xC
Oceánii	Oceánie	k1gFnSc6
Nezávislé	závislý	k2eNgInPc1d1
státy	stát	k1gInPc1
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
•	•	k?
Cookovy	Cookov	k1gInPc1
ostrovy¹	ostrovy¹	k?
•	•	k?
Federativní	federativní	k2eAgInPc4d1
státy	stát	k1gInPc4
Mikronésie²	Mikronésie²	k1gFnSc2
•	•	k?
Fidži	Fidž	k1gFnSc6
•	•	k?
Kiribati	Kiribati	k1gFnPc2
•	•	k?
Marshallovy	Marshallův	k2eAgFnSc2d1
ostrovy²	ostrovy²	k?
•	•	k?
Nauru	Naur	k1gInSc2
•	•	k?
Nový	nový	k2eAgInSc1d1
Zéland	Zéland	k1gInSc1
•	•	k?
Niue¹	Niue¹	k1gFnSc2
•	•	k?
Palau²	Palau²	k1gFnSc2
•	•	k?
Papua	Papu	k2eAgFnSc1d1
Nová	nový	k2eAgFnSc1d1
Guinea	Guinea	k1gFnSc1
•	•	k?
Samoa	Samoa	k1gFnSc1
•	•	k?
Šalomounovy	Šalomounův	k2eAgInPc4d1
ostrovy	ostrov	k1gInPc4
•	•	k?
Tonga	Tonga	k1gFnSc1
•	•	k?
Tuvalu	Tuval	k1gInSc2
•	•	k?
Vanuatu	Vanuat	k1gInSc2
Součásti	součást	k1gFnSc2
suverénních	suverénní	k2eAgInPc2d1
států	stát	k1gInPc2
</s>
<s>
Chile	Chile	k1gNnSc1
<g/>
:	:	kIx,
(	(	kIx(
<g/>
Velikonoční	velikonoční	k2eAgInSc1d1
ostrov	ostrov	k1gInSc1
•	•	k?
Ostrovy	ostrov	k1gInPc1
Juana	Juan	k1gMnSc2
Fernándeze	Fernándeze	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
Indonésie	Indonésie	k1gFnSc1
<g/>
:	:	kIx,
(	(	kIx(
<g/>
Papua	Papu	k2eAgFnSc1d1
•	•	k?
Západní	západní	k2eAgFnSc1d1
Papua	Papua	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Japonsko	Japonsko	k1gNnSc1
<g/>
:	:	kIx,
(	(	kIx(
<g/>
Boninské	Boninský	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
•	•	k?
Minami	mina	k1gFnPc7
Torišima	Torišimum	k1gNnPc4
<g/>
)	)	kIx)
•	•	k?
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
<g/>
:	:	kIx,
(	(	kIx(
<g/>
Havaj	Havaj	k1gFnSc1
•	•	k?
Palmyra	Palmyra	k1gFnSc1
<g/>
)	)	kIx)
Teritoria	teritorium	k1gNnSc2
<g/>
,	,	kIx,
kolonie	kolonie	k1gFnSc2
azávislá	azávislý	k2eAgNnPc1d1
území	území	k1gNnPc1
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
<g/>
:	:	kIx,
(	(	kIx(
<g/>
Ostrovy	ostrov	k1gInPc1
Korálového	korálový	k2eAgNnSc2d1
moře	moře	k1gNnSc2
•	•	k?
Norfolk	Norfolk	k1gMnSc1
<g/>
)	)	kIx)
<g/>
Francie	Francie	k1gFnSc1
<g/>
:	:	kIx,
(	(	kIx(
<g/>
Francouzská	francouzský	k2eAgFnSc1d1
Polynésie	Polynésie	k1gFnSc1
•	•	k?
Nová	nový	k2eAgFnSc1d1
Kaledonie	Kaledonie	k1gFnSc1
•	•	k?
Wallis	Wallis	k1gFnSc1
a	a	k8xC
Futuna	Futuna	k1gFnSc1
<g/>
)	)	kIx)
<g/>
Nový	nový	k2eAgInSc1d1
Zéland	Zéland	k1gInSc1
<g/>
:	:	kIx,
(	(	kIx(
<g/>
Tokelau	Tokelaus	k1gInSc2
<g/>
)	)	kIx)
<g/>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
:	:	kIx,
(	(	kIx(
<g/>
Pitcairnovy	Pitcairnův	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
<g/>
)	)	kIx)
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
<g/>
:	:	kIx,
(	(	kIx(
<g/>
Americká	americký	k2eAgFnSc1d1
Samoa	Samoa	k1gFnSc1
•	•	k?
Bakerův	Bakerův	k2eAgInSc4d1
ostrov	ostrov	k1gInSc4
•	•	k?
Guam	Guam	k1gInSc1
•	•	k?
Howlandův	Howlandův	k2eAgInSc1d1
ostrov	ostrov	k1gInSc1
•	•	k?
Jarvisův	Jarvisův	k2eAgInSc1d1
ostrov	ostrov	k1gInSc1
•	•	k?
Johnstonův	Johnstonův	k2eAgInSc1d1
atol	atol	k1gInSc1
•	•	k?
Kingmanův	Kingmanův	k2eAgInSc1d1
útes	útes	k1gInSc1
•	•	k?
Midway	Midwaa	k1gFnSc2
•	•	k?
Severní	severní	k2eAgFnSc2d1
Mariany	Mariana	k1gFnSc2
•	•	k?
Wake	Wake	k1gInSc1
<g/>
)	)	kIx)
¹	¹	k1gInPc1
přidružené	přidružený	k2eAgInPc1d1
k	k	k7c3
Novému	nový	k2eAgInSc3d1
Zélandu	Zéland	k1gInSc3
<g/>
.	.	kIx.
²	²	k1gInPc1
přidružené	přidružený	k2eAgFnSc2d1
ke	k	k7c3
Spojeným	spojený	k2eAgInPc3d1
státům	stát	k1gInPc3
<g/>
.	.	kIx.
³	³	k2eAgNnSc1d1
také	také	k9
do	do	k7c2
jihovýchodní	jihovýchodní	k2eAgFnSc2d1
Asie	Asie	k1gFnSc2
</s>
<s>
Členství	členství	k1gNnSc1
v	v	k7c6
mezinárodních	mezinárodní	k2eAgFnPc6d1
organizacích	organizace	k1gFnPc6
</s>
<s>
Státy	stát	k1gInPc1
a	a	k8xC
území	území	k1gNnSc1
s	s	k7c7
členstvím	členství	k1gNnSc7
v	v	k7c6
APEC	APEC	kA
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
•	•	k?
Brunej	Brunej	k1gFnSc1
•	•	k?
Čína	Čína	k1gFnSc1
•	•	k?
Filipíny	Filipíny	k1gFnPc4
•	•	k?
Hongkong	Hongkong	k1gInSc1
•	•	k?
Chile	Chile	k1gNnSc2
•	•	k?
Indonésie	Indonésie	k1gFnSc1
•	•	k?
Japonsko	Japonsko	k1gNnSc1
•	•	k?
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
•	•	k?
Kanada	Kanada	k1gFnSc1
•	•	k?
Malajsie	Malajsie	k1gFnSc1
•	•	k?
Mexiko	Mexiko	k1gNnSc1
•	•	k?
Nový	nový	k2eAgInSc1d1
Zéland	Zéland	k1gInSc1
•	•	k?
Papua	Papuum	k1gNnSc2
Nová	nový	k2eAgFnSc1d1
Guinea	Guinea	k1gFnSc1
•	•	k?
Peru	prát	k5eAaImIp1nS
•	•	k?
Rusko	Rusko	k1gNnSc4
•	•	k?
Singapur	Singapur	k1gInSc1
•	•	k?
Spojené	spojený	k2eAgInPc4d1
státy	stát	k1gInPc4
americké	americký	k2eAgInPc4d1
•	•	k?
Thajsko	Thajsko	k1gNnSc1
•	•	k?
Tchaj-wan	Tchaj-wan	k1gInSc1
•	•	k?
Vietnam	Vietnam	k1gInSc1
</s>
<s>
Commonwealth	Commonwealth	k1gInSc1
(	(	kIx(
<g/>
Společenství	společenství	k1gNnSc1
národů	národ	k1gInPc2
<g/>
)	)	kIx)
Řádné	řádný	k2eAgFnPc1d1
členské	členský	k2eAgFnPc1d1
země	zem	k1gFnPc1
</s>
<s>
Antigua	Antigua	k1gMnSc1
a	a	k8xC
Barbuda	Barbuda	k1gMnSc1
•	•	k?
Austrálie	Austrálie	k1gFnSc2
•	•	k?
Bahamy	Bahamy	k1gFnPc4
•	•	k?
Bangladéš	Bangladéš	k1gInSc1
•	•	k?
Barbados	Barbados	k1gInSc1
•	•	k?
Belize	Belize	k1gFnSc2
•	•	k?
Botswana	Botswana	k1gFnSc1
•	•	k?
Brunej	Brunej	k1gMnSc2
•	•	k?
Dominika	Dominik	k1gMnSc2
•	•	k?
Fidži	Fidž	k1gFnSc6
•	•	k?
Gambie	Gambie	k1gFnSc2
•	•	k?
Ghana	Ghana	k1gFnSc1
•	•	k?
Grenada	Grenada	k1gFnSc1
•	•	k?
Guyana	Guyana	k1gFnSc1
•	•	k?
Indie	Indie	k1gFnSc2
•	•	k?
Jamajka	Jamajka	k1gFnSc1
•	•	k?
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
•	•	k?
Kamerun	Kamerun	k1gInSc1
•	•	k?
Kanada	Kanada	k1gFnSc1
•	•	k?
Keňa	Keňa	k1gFnSc1
•	•	k?
Kiribati	Kiribati	k1gFnSc2
•	•	k?
Kypr	Kypr	k1gInSc1
•	•	k?
Lesotho	Lesot	k1gMnSc2
•	•	k?
Malajsie	Malajsie	k1gFnSc2
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Malawi	Malawi	k1gNnSc4
•	•	k?
Malta	Malta	k1gFnSc1
•	•	k?
Mauricius	Mauricius	k1gInSc1
•	•	k?
Mosambik	Mosambik	k1gInSc1
•	•	k?
Namibie	Namibie	k1gFnSc2
•	•	k?
Nauru	Naur	k1gInSc2
•	•	k?
Nigérie	Nigérie	k1gFnSc2
•	•	k?
Nový	nový	k2eAgInSc1d1
Zéland	Zéland	k1gInSc1
•	•	k?
Pákistán	Pákistán	k1gInSc1
•	•	k?
Papua	Papua	k1gFnSc1
Nová	nový	k2eAgFnSc1d1
Guinea	guinea	k1gFnSc7
•	•	k?
Rwanda	Rwanda	k1gFnSc1
•	•	k?
Samoa	Samoa	k1gFnSc1
•	•	k?
Seychely	Seychely	k1gFnPc4
•	•	k?
Sierra	Sierra	k1gFnSc1
Leone	Leo	k1gMnSc5
•	•	k?
Singapur	Singapur	k1gInSc4
•	•	k?
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
•	•	k?
Sv.	sv.	kA
Kryštof	Kryštof	k1gMnSc1
a	a	k8xC
Nevis	viset	k5eNaImRp2nS
•	•	k?
Svatá	svatý	k2eAgFnSc1d1
Lucie	Lucie	k1gFnSc1
•	•	k?
Sv.	sv.	kA
Vincenc	Vincenc	k1gMnSc1
a	a	k8xC
Grenadiny	grenadina	k1gFnPc1
•	•	k?
Svazijsko	Svazijsko	k1gNnSc1
•	•	k?
Šalamounovy	Šalamounův	k2eAgInPc4d1
ostrovy	ostrov	k1gInPc4
•	•	k?
Srí	Srí	k1gFnSc2
Lanka	lanko	k1gNnSc2
•	•	k?
Tanzanie	Tanzanie	k1gFnSc1
•	•	k?
Tonga	Tonga	k1gFnSc1
•	•	k?
Trinidad	Trinidad	k1gInSc1
a	a	k8xC
Tobago	Tobago	k1gNnSc1
•	•	k?
Tuvalu	Tuval	k1gInSc2
•	•	k?
Uganda	Uganda	k1gFnSc1
•	•	k?
Vanuatu	Vanuat	k1gInSc2
•	•	k?
Zambie	Zambie	k1gFnSc1
Bývalí	bývalý	k2eAgMnPc1d1
členové	člen	k1gMnPc1
</s>
<s>
Irsko	Irsko	k1gNnSc1
•	•	k?
Maledivy	Maledivy	k1gFnPc4
•	•	k?
Zimbabwe	Zimbabwe	k1gFnSc1
Závislá	závislý	k2eAgFnSc1d1
území	území	k1gNnSc4
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
Akrotiri	Akrotiri	k6eAd1
a	a	k8xC
Dekelia	Dekelia	k1gFnSc1
•	•	k?
Anguilla	Anguilla	k1gFnSc1
•	•	k?
Bermudy	Bermudy	k1gFnPc4
•	•	k?
Britské	britský	k2eAgNnSc1d1
antarktické	antarktický	k2eAgNnSc1d1
území	území	k1gNnSc1
•	•	k?
Britské	britský	k2eAgNnSc1d1
indickooceánské	indickooceánský	k2eAgNnSc1d1
území	území	k1gNnSc1
•	•	k?
Britské	britský	k2eAgInPc4d1
Panenské	panenský	k2eAgInPc4d1
ostrovy	ostrov	k1gInPc4
•	•	k?
Falklandy	Falklanda	k1gFnSc2
•	•	k?
Gibraltar	Gibraltar	k1gInSc1
•	•	k?
Jižní	jižní	k2eAgFnSc2d1
Georgie	Georgie	k1gFnSc2
a	a	k8xC
Jižní	jižní	k2eAgInPc1d1
Sandwichovy	Sandwichův	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
•	•	k?
Kajmanské	Kajmanský	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
•	•	k?
Montserrat	Montserrat	k1gInSc4
•	•	k?
Pitcairnovy	Pitcairnův	k2eAgInPc4d1
ostrovy	ostrov	k1gInPc4
•	•	k?
Svatá	svatá	k1gFnSc1
Helena	Helena	k1gFnSc1
<g/>
,	,	kIx,
Ascension	Ascension	k1gInSc1
a	a	k8xC
Tristan	Tristan	k1gInSc1
da	da	k?
Cunha	Cunha	k1gFnSc1
•	•	k?
Turks	Turks	k1gInSc1
a	a	k8xC
Caicos	Caicos	k1gInSc1
Austrálie	Austrálie	k1gFnSc2
</s>
<s>
Ashmorův	Ashmorův	k2eAgInSc1d1
a	a	k8xC
Cartierův	Cartierův	k2eAgInSc1d1
ostrov	ostrov	k1gInSc1
•	•	k?
Australské	australský	k2eAgNnSc1d1
antarktické	antarktický	k2eAgNnSc1d1
území	území	k1gNnSc1
•	•	k?
Kokosové	kokosový	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
•	•	k?
Heardův	Heardův	k2eAgMnSc1d1
a	a	k8xC
MacDonaldovy	Macdonaldův	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
•	•	k?
Ostrovy	ostrov	k1gInPc4
Korálového	korálový	k2eAgNnSc2d1
moře	moře	k1gNnSc2
•	•	k?
Norfolk	Norfolk	k1gInSc1
•	•	k?
Vánoční	vánoční	k2eAgInSc1d1
ostrov	ostrov	k1gInSc1
Nový	nový	k2eAgInSc1d1
Zéland	Zéland	k1gInSc1
</s>
<s>
Cookovy	Cookův	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
•	•	k?
Niue	Niu	k1gInSc2
•	•	k?
Rossova	Rossův	k2eAgFnSc1d1
dependence	dependence	k1gFnSc1
•	•	k?
Tokelau	Tokelaus	k1gInSc2
</s>
<s>
G20	G20	k4
</s>
<s>
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
•	•	k?
Argentina	Argentina	k1gFnSc1
•	•	k?
Brazílie	Brazílie	k1gFnSc2
•	•	k?
Kanada	Kanada	k1gFnSc1
•	•	k?
Mexiko	Mexiko	k1gNnSc1
•	•	k?
Spojené	spojený	k2eAgInPc4d1
státy	stát	k1gInPc4
americké	americký	k2eAgFnSc2d1
•	•	k?
Indonésie	Indonésie	k1gFnSc2
•	•	k?
Saúdská	saúdský	k2eAgFnSc1d1
Arábie	Arábie	k1gFnSc1
•	•	k?
Indie	Indie	k1gFnSc2
•	•	k?
Čína	Čína	k1gFnSc1
•	•	k?
Japonsko	Japonsko	k1gNnSc1
•	•	k?
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
•	•	k?
Rusko	Rusko	k1gNnSc1
•	•	k?
Turecko	Turecko	k1gNnSc1
•	•	k?
Evropská	evropský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
•	•	k?
Francie	Francie	k1gFnSc2
•	•	k?
Německo	Německo	k1gNnSc1
•	•	k?
Itálie	Itálie	k1gFnSc2
•	•	k?
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
•	•	k?
Austrálie	Austrálie	k1gFnSc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
128695	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4003900-6	4003900-6	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2096	#num#	k4
9976	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
79021326	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
122687544	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
79021326	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Austrálie	Austrálie	k1gFnSc1
|	|	kIx~
Geografie	geografie	k1gFnSc1
</s>
