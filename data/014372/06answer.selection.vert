<s>
Austrálie	Austrálie	k1gFnSc1
má	mít	k5eAaImIp3nS
nejvyšší	vysoký	k2eAgInSc4d3
výskyt	výskyt	k1gInSc4
rakoviny	rakovina	k1gFnSc2
kůže	kůže	k1gFnSc2
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
96	#num#	k4
<g/>
]	]	kIx)
Pravděpodobnost	pravděpodobnost	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
Australan	Australan	k1gMnSc1
onemocní	onemocnět	k5eAaPmIp3nS
rakovinou	rakovina	k1gFnSc7
kůže	kůže	k1gFnSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
čtyřikrát	čtyřikrát	k6eAd1
vyšší	vysoký	k2eAgFnSc1d2
<g/>
,	,	kIx,
než	než	k8xS
že	že	k8xS
onemocní	onemocnět	k5eAaPmIp3nP
jiným	jiný	k2eAgInSc7d1
druhem	druh	k1gInSc7
rakoviny	rakovina	k1gFnSc2
<g/>
.	.	kIx.
</s>