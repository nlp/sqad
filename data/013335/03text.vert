<p>
<s>
Filip	Filip	k1gMnSc1	Filip
III	III	kA	III
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1578	[number]	k4	1578
<g/>
,	,	kIx,	,
Madrid	Madrid	k1gInSc1	Madrid
–	–	k?	–
31	[number]	k4	31
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1621	[number]	k4	1621
<g/>
,	,	kIx,	,
Madrid	Madrid	k1gInSc1	Madrid
<g/>
)	)	kIx)	)
z	z	k7c2	z
dynastie	dynastie	k1gFnSc2	dynastie
rodu	rod	k1gInSc2	rod
Habsburků	Habsburk	k1gMnPc2	Habsburk
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
Španělska	Španělsko	k1gNnSc2	Španělsko
1598	[number]	k4	1598
<g/>
–	–	k?	–
<g/>
1621	[number]	k4	1621
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
1598	[number]	k4	1598
<g/>
–	–	k?	–
<g/>
1621	[number]	k4	1621
<g/>
.	.	kIx.	.
</s>
<s>
Syn	syn	k1gMnSc1	syn
Filipa	Filip	k1gMnSc2	Filip
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
Anny	Anna	k1gFnSc2	Anna
Habsburské	habsburský	k2eAgFnSc2d1	habsburská
<g/>
.	.	kIx.	.
</s>
<s>
Vzal	vzít	k5eAaPmAgMnS	vzít
si	se	k3xPyFc3	se
za	za	k7c4	za
manželku	manželka	k1gFnSc4	manželka
Markétu	Markéta	k1gFnSc4	Markéta
Rakouskou	rakouský	k2eAgFnSc4d1	rakouská
(	(	kIx(	(
<g/>
1584	[number]	k4	1584
<g/>
–	–	k?	–
<g/>
1611	[number]	k4	1611
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
dcerou	dcera	k1gFnSc7	dcera
Karla	Karel	k1gMnSc2	Karel
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Štýrského	štýrský	k2eAgMnSc2d1	štýrský
a	a	k8xC	a
Marie	Maria	k1gFnPc4	Maria
Anny	Anna	k1gFnSc2	Anna
Bavorské	bavorský	k2eAgFnSc2d1	bavorská
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
vláda	vláda	k1gFnSc1	vláda
se	se	k3xPyFc4	se
vyznačovala	vyznačovat	k5eAaImAgFnS	vyznačovat
narůstáním	narůstání	k1gNnSc7	narůstání
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
krize	krize	k1gFnSc2	krize
říše	říš	k1gFnSc2	říš
(	(	kIx(	(
<g/>
hosp	hosp	k1gInSc1	hosp
<g/>
.	.	kIx.	.
potíže	potíž	k1gFnPc1	potíž
<g/>
,	,	kIx,	,
korupce	korupce	k1gFnSc1	korupce
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
<g/>
,	,	kIx,	,
vzrůstání	vzrůstání	k1gNnSc2	vzrůstání
výdajů	výdaj	k1gInPc2	výdaj
dvora	dvůr	k1gInSc2	dvůr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Způsobené	způsobený	k2eAgNnSc1d1	způsobené
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgMnPc4d1	jiný
neomezeným	omezený	k2eNgInSc7d1	neomezený
vlivem	vliv	k1gInSc7	vliv
králova	králův	k2eAgMnSc4d1	králův
favorita	favorit	k1gMnSc4	favorit
Francisca	Franciscus	k1gMnSc4	Franciscus
Goméze	Goméza	k1gFnSc6	Goméza
<g/>
,	,	kIx,	,
vévody	vévoda	k1gMnSc2	vévoda
z	z	k7c2	z
Lermy	Lermy	k?	Lermy
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zahraniční	zahraniční	k2eAgFnSc6d1	zahraniční
politice	politika	k1gFnSc6	politika
prosazoval	prosazovat	k5eAaImAgInS	prosazovat
mírová	mírový	k2eAgNnPc4d1	Mírové
řešení	řešení	k1gNnPc4	řešení
(	(	kIx(	(
<g/>
smlouvy	smlouva	k1gFnPc4	smlouva
s	s	k7c7	s
Holandskem	Holandsko	k1gNnSc7	Holandsko
a	a	k8xC	a
Anglií	Anglie	k1gFnSc7	Anglie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvních	první	k4xOgNnPc6	první
letech	léto	k1gNnPc6	léto
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
obsadila	obsadit	k5eAaPmAgFnS	obsadit
jeho	jeho	k3xOp3gFnSc1	jeho
armáda	armáda	k1gFnSc1	armáda
Rýnskou	rýnský	k2eAgFnSc4d1	Rýnská
Falc	Falc	k1gFnSc4	Falc
(	(	kIx(	(
<g/>
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
falckrabětem	falckrabě	k1gMnSc7	falckrabě
a	a	k8xC	a
kurfiřtem	kurfiřt	k1gMnSc7	kurfiřt
Fridrich	Fridrich	k1gMnSc1	Fridrich
Falcký	falcký	k2eAgMnSc1d1	falcký
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
umění	umění	k1gNnSc2	umění
prožívala	prožívat	k5eAaImAgFnS	prožívat
za	za	k7c2	za
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
velký	velký	k2eAgInSc1d1	velký
rozmach	rozmach	k1gInSc1	rozmach
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Vytoužený	vytoužený	k2eAgMnSc1d1	vytoužený
potomek	potomek	k1gMnSc1	potomek
===	===	k?	===
</s>
</p>
<p>
<s>
Filip	Filip	k1gMnSc1	Filip
III	III	kA	III
<g/>
.	.	kIx.	.
byl	být	k5eAaImAgMnS	být
syn	syn	k1gMnSc1	syn
čtvrté	čtvrtá	k1gFnSc2	čtvrtá
ženy	žena	k1gFnSc2	žena
Filipa	Filip	k1gMnSc2	Filip
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Žádná	žádný	k3yNgFnSc1	žádný
z	z	k7c2	z
předchozích	předchozí	k2eAgFnPc2d1	předchozí
manželek	manželka	k1gFnPc2	manželka
krále	král	k1gMnSc2	král
mu	on	k3xPp3gNnSc3	on
neporodila	porodit	k5eNaPmAgFnS	porodit
dítě	dítě	k1gNnSc4	dítě
mužského	mužský	k2eAgNnSc2d1	mužské
pohlaví	pohlaví	k1gNnSc2	pohlaví
–	–	k?	–
následníka	následník	k1gMnSc4	následník
trůnu	trůn	k1gInSc2	trůn
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
manželka	manželka	k1gFnSc1	manželka
–	–	k?	–
portugalská	portugalský	k2eAgFnSc1d1	portugalská
infantka	infantka	k1gFnSc1	infantka
–	–	k?	–
Marie	Maria	k1gFnSc2	Maria
mu	on	k3xPp3gMnSc3	on
sice	sice	k8xC	sice
již	již	k6eAd1	již
jednoho	jeden	k4xCgMnSc4	jeden
syna	syn	k1gMnSc4	syn
porodila	porodit	k5eAaPmAgFnS	porodit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
sama	sám	k3xTgFnSc1	sám
o	o	k7c4	o
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
zemřela	zemřít	k5eAaPmAgFnS	zemřít
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
její	její	k3xOp3gMnSc1	její
syn	syn	k1gMnSc1	syn
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
byl	být	k5eAaImAgMnS	být
nemocný	nemocný	k2eAgMnSc1d1	nemocný
<g/>
,	,	kIx,	,
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
u	u	k7c2	u
něho	on	k3xPp3gInSc2	on
začaly	začít	k5eAaPmAgInP	začít
objevovat	objevovat	k5eAaImF	objevovat
příznaky	příznak	k1gInPc1	příznak
mentálního	mentální	k2eAgNnSc2d1	mentální
postižení	postižení	k1gNnSc2	postižení
a	a	k8xC	a
tělesných	tělesný	k2eAgFnPc2d1	tělesná
vad	vada	k1gFnPc2	vada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bezdětným	bezdětný	k2eAgMnPc3d1	bezdětný
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
být	být	k5eAaImF	být
i	i	k9	i
další	další	k2eAgNnSc4d1	další
manželství	manželství	k1gNnSc4	manželství
Filipa	Filip	k1gMnSc2	Filip
II	II	kA	II
<g/>
.	.	kIx.	.
s	s	k7c7	s
anglickou	anglický	k2eAgFnSc7d1	anglická
královnou	královna	k1gFnSc7	královna
Marií	Maria	k1gFnSc7	Maria
Tudorovnou	Tudorovna	k1gFnSc7	Tudorovna
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
pouze	pouze	k6eAd1	pouze
politickou	politický	k2eAgFnSc7d1	politická
záležitostí	záležitost	k1gFnSc7	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc1	třetí
manželka	manželka	k1gFnSc1	manželka
<g/>
,	,	kIx,	,
francouzská	francouzský	k2eAgFnSc1d1	francouzská
Alžběta	Alžběta	k1gFnSc1	Alžběta
z	z	k7c2	z
Valois	Valois	k1gFnSc2	Valois
porodila	porodit	k5eAaPmAgFnS	porodit
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
<g/>
,	,	kIx,	,
první	první	k4xOgNnSc1	první
těhotenství	těhotenství	k1gNnSc1	těhotenství
však	však	k9	však
skončilo	skončit	k5eAaPmAgNnS	skončit
potratem	potrat	k1gInSc7	potrat
dvojčat	dvojče	k1gNnPc2	dvojče
a	a	k8xC	a
potrat	potrat	k1gInSc4	potrat
čtvrtého	čtvrtý	k4xOgNnSc2	čtvrtý
dítěte	dítě	k1gNnSc2	dítě
–	–	k?	–
syna	syn	k1gMnSc2	syn
–	–	k?	–
zaplatila	zaplatit	k5eAaPmAgFnS	zaplatit
Alžběta	Alžběta	k1gFnSc1	Alžběta
životem	život	k1gInSc7	život
<g/>
;	;	kIx,	;
zůstaly	zůstat	k5eAaPmAgInP	zůstat
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
dvě	dva	k4xCgFnPc4	dva
dcery	dcera	k1gFnPc4	dcera
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc4	dva
princezny	princezna	k1gFnPc4	princezna
král	král	k1gMnSc1	král
laskavě	laskavě	k6eAd1	laskavě
vychovával	vychovávat	k5eAaImAgMnS	vychovávat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
žádná	žádný	k3yNgFnSc1	žádný
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
nemohla	moct	k5eNaImAgFnS	moct
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
platného	platný	k2eAgNnSc2d1	platné
nástupnického	nástupnický	k2eAgNnSc2d1	nástupnické
Salického	Salický	k2eAgNnSc2d1	Salický
práva	právo	k1gNnSc2	právo
<g/>
)	)	kIx)	)
převzít	převzít	k5eAaPmF	převzít
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
otci	otec	k1gMnSc3	otec
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Filip	Filip	k1gMnSc1	Filip
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
oženil	oženit	k5eAaPmAgMnS	oženit
po	po	k7c6	po
čtvrté	čtvrtý	k4xOgFnSc6	čtvrtý
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
s	s	k7c7	s
Annou	Anna	k1gFnSc7	Anna
<g/>
,	,	kIx,	,
dcerou	dcera	k1gFnSc7	dcera
císaře	císař	k1gMnSc2	císař
Maxmiliána	Maxmilián	k1gMnSc2	Maxmilián
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gFnSc1	jeho
neteř	neteř	k1gFnSc1	neteř
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
manželství	manželství	k1gNnSc2	manželství
vzešlo	vzejít	k5eAaPmAgNnS	vzejít
pět	pět	k4xCc1	pět
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
čtyři	čtyři	k4xCgInPc1	čtyři
však	však	k9	však
zemřely	zemřít	k5eAaPmAgFnP	zemřít
<g/>
,	,	kIx,	,
dospělosti	dospělost	k1gFnSc2	dospělost
se	se	k3xPyFc4	se
dočkal	dočkat	k5eAaPmAgMnS	dočkat
druhý	druhý	k4xOgMnSc1	druhý
syn	syn	k1gMnSc1	syn
Filip	Filip	k1gMnSc1	Filip
<g/>
,	,	kIx,	,
pozdější	pozdní	k2eAgMnSc1d2	pozdější
král	král	k1gMnSc1	král
Filip	Filip	k1gMnSc1	Filip
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
přišel	přijít	k5eAaPmAgMnS	přijít
na	na	k7c4	na
svět	svět	k1gInSc4	svět
14	[number]	k4	14
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1578	[number]	k4	1578
v	v	k7c6	v
královském	královský	k2eAgInSc6d1	královský
paláci	palác	k1gInSc6	palác
v	v	k7c6	v
Madridu	Madrid	k1gInSc6	Madrid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
věku	věk	k1gInSc6	věk
čtyř	čtyři	k4xCgNnPc2	čtyři
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1582	[number]	k4	1582
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Filip	Filip	k1gMnSc1	Filip
oficiálně	oficiálně	k6eAd1	oficiálně
určen	určit	k5eAaPmNgMnS	určit
za	za	k7c4	za
následníka	následník	k1gMnSc4	následník
španělského	španělský	k2eAgInSc2d1	španělský
trůnu	trůn	k1gInSc2	trůn
a	a	k8xC	a
získal	získat	k5eAaPmAgInS	získat
titul	titul	k1gInSc1	titul
prince	princ	k1gMnSc2	princ
asturského	asturský	k2eAgMnSc2d1	asturský
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
však	však	k9	však
rychle	rychle	k6eAd1	rychle
přišel	přijít	k5eAaPmAgMnS	přijít
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgMnSc1	tento
syn	syn	k1gMnSc1	syn
nebyl	být	k5eNaImAgMnS	být
předurčen	předurčit	k5eAaPmNgMnS	předurčit
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vedl	vést	k5eAaImAgInS	vést
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
říší	říš	k1gFnPc2	říš
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starší	k1gMnSc1	starší
Filip	Filip	k1gMnSc1	Filip
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
upnul	upnout	k5eAaPmAgMnS	upnout
na	na	k7c4	na
své	svůj	k3xOyFgFnPc4	svůj
dcery	dcera	k1gFnPc4	dcera
<g/>
.	.	kIx.	.
</s>
<s>
Jedině	jedině	k6eAd1	jedině
ony	onen	k3xDgFnPc1	onen
mu	on	k3xPp3gMnSc3	on
mohly	moct	k5eAaImAgInP	moct
pomáhat	pomáhat	k5eAaImF	pomáhat
při	při	k7c6	při
práci	práce	k1gFnSc6	práce
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Filip	Filip	k1gMnSc1	Filip
II	II	kA	II
<g/>
.	.	kIx.	.
byl	být	k5eAaImAgInS	být
uznáván	uznávat	k5eAaImNgInS	uznávat
za	za	k7c4	za
tvrdého	tvrdý	k2eAgMnSc4d1	tvrdý
a	a	k8xC	a
chladného	chladný	k2eAgMnSc4d1	chladný
muže	muž	k1gMnSc4	muž
a	a	k8xC	a
také	také	k9	také
žádné	žádný	k3yNgInPc4	žádný
city	cit	k1gInPc4	cit
ke	k	k7c3	k
svým	svůj	k3xOyFgFnPc3	svůj
dcerám	dcera	k1gFnPc3	dcera
nedával	dávat	k5eNaImAgMnS	dávat
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
znát	znát	k5eAaImF	znát
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
jeho	jeho	k3xOp3gInPc6	jeho
dopisech	dopis	k1gInPc6	dopis
se	se	k3xPyFc4	se
projevovaly	projevovat	k5eAaImAgInP	projevovat
otcovské	otcovský	k2eAgInPc1d1	otcovský
city	cit	k1gInPc1	cit
<g/>
.	.	kIx.	.
</s>
<s>
Syn	syn	k1gMnSc1	syn
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
komunikoval	komunikovat	k5eAaImAgMnS	komunikovat
přes	přes	k7c4	přes
prostředníka	prostředník	k1gMnSc4	prostředník
–	–	k?	–
král	král	k1gMnSc1	král
připustil	připustit	k5eAaPmAgMnS	připustit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Bůh	bůh	k1gMnSc1	bůh
mu	on	k3xPp3gMnSc3	on
nedopřál	dopřát	k5eNaPmAgMnS	dopřát
mužských	mužský	k2eAgInPc2d1	mužský
potomků	potomek	k1gMnPc2	potomek
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
by	by	kYmCp3nP	by
dokázali	dokázat	k5eAaPmAgMnP	dokázat
mít	mít	k5eAaImF	mít
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
chod	chod	k1gInSc4	chod
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
a	a	k8xC	a
prorokoval	prorokovat	k5eAaImAgMnS	prorokovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
až	až	k9	až
Filip	Filip	k1gMnSc1	Filip
III	III	kA	III
<g/>
.	.	kIx.	.
převezme	převzít	k5eAaPmIp3nS	převzít
korunu	koruna	k1gFnSc4	koruna
<g/>
,	,	kIx,	,
státní	státní	k2eAgMnPc1d1	státní
ministři	ministr	k1gMnPc1	ministr
převezmou	převzít	k5eAaPmIp3nP	převzít
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
prokázalo	prokázat	k5eAaPmAgNnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
toto	tento	k3xDgNnSc1	tento
proroctví	proroctví	k1gNnSc1	proroctví
bude	být	k5eAaImBp3nS	být
pravdivé	pravdivý	k2eAgNnSc1d1	pravdivé
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Dědictví	dědictví	k1gNnSc2	dědictví
===	===	k?	===
</s>
</p>
<p>
<s>
Filip	Filip	k1gMnSc1	Filip
II	II	kA	II
<g/>
.	.	kIx.	.
zemřel	zemřít	k5eAaPmAgMnS	zemřít
13	[number]	k4	13
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1598	[number]	k4	1598
a	a	k8xC	a
svému	svůj	k3xOyFgMnSc3	svůj
synovi	syn	k1gMnSc3	syn
odkázal	odkázat	k5eAaPmAgMnS	odkázat
plošně	plošně	k6eAd1	plošně
největší	veliký	k2eAgFnSc4d3	veliký
zemi	zem	k1gFnSc4	zem
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
i	i	k9	i
bez	bez	k7c2	bez
přihlédnutí	přihlédnutí	k1gNnSc2	přihlédnutí
k	k	k7c3	k
majetku	majetek	k1gInSc3	majetek
v	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
světě	svět	k1gInSc6	svět
a	a	k8xC	a
na	na	k7c6	na
Filipínách	Filipíny	k1gFnPc6	Filipíny
<g/>
,	,	kIx,	,
oblast	oblast	k1gFnSc1	oblast
království	království	k1gNnSc2	království
a	a	k8xC	a
území	území	k1gNnSc2	území
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
přímo	přímo	k6eAd1	přímo
závislých	závislý	k2eAgFnPc2d1	závislá
byla	být	k5eAaImAgFnS	být
obrovská	obrovský	k2eAgFnSc1d1	obrovská
–	–	k?	–
zahrnovala	zahrnovat	k5eAaImAgFnS	zahrnovat
celý	celý	k2eAgInSc4d1	celý
Pyrenejský	pyrenejský	k2eAgInSc4d1	pyrenejský
poloostrov	poloostrov	k1gInSc4	poloostrov
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
<g/>
,	,	kIx,	,
Franche-Comté	Franche-Comtý	k2eAgNnSc1d1	Franche-Comté
<g/>
,	,	kIx,	,
Milánské	milánský	k2eAgNnSc1d1	Milánské
vévodství	vévodství	k1gNnSc1	vévodství
<g/>
,	,	kIx,	,
Sardinie	Sardinie	k1gFnSc1	Sardinie
<g/>
,	,	kIx,	,
Neapol	Neapol	k1gFnSc1	Neapol
a	a	k8xC	a
Sicílie	Sicílie	k1gFnSc1	Sicílie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Celá	celý	k2eAgFnSc1d1	celá
říše	říše	k1gFnSc1	říše
těžila	těžit	k5eAaImAgFnS	těžit
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
úspěchů	úspěch	k1gInPc2	úspěch
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
vlády	vláda	k1gFnSc2	vláda
Filipa	Filip	k1gMnSc2	Filip
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
jeho	jeho	k3xOp3gFnSc4	jeho
vládu	vláda	k1gFnSc4	vláda
potkalo	potkat	k5eAaPmAgNnS	potkat
několik	několik	k4yIc1	několik
proher	prohra	k1gFnPc2	prohra
v	v	k7c6	v
zahraniční	zahraniční	k2eAgFnSc6d1	zahraniční
politice	politika	k1gFnSc6	politika
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
zkáza	zkáza	k1gFnSc1	zkáza
Neporazitelné	porazitelný	k2eNgFnSc2d1	neporazitelná
Armady	Armada	k1gFnSc2	Armada
<g/>
.	.	kIx.	.
</s>
<s>
Neúspěchy	neúspěch	k1gInPc1	neúspěch
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
politiky	politika	k1gFnSc2	politika
způsobily	způsobit	k5eAaPmAgInP	způsobit
vážné	vážný	k2eAgInPc1d1	vážný
vnitřní	vnitřní	k2eAgInPc1d1	vnitřní
problémy	problém	k1gInPc1	problém
<g/>
.	.	kIx.	.
</s>
<s>
Zkoumání	zkoumání	k1gNnSc1	zkoumání
nových	nový	k2eAgFnPc2d1	nová
zemí	zem	k1gFnPc2	zem
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Indii	Indie	k1gFnSc6	Indie
dala	dát	k5eAaPmAgFnS	dát
Španělsku	Španělsko	k1gNnSc6	Španělsko
ohromné	ohromný	k2eAgNnSc4d1	ohromné
bohatství	bohatství	k1gNnSc4	bohatství
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
užitečné	užitečný	k2eAgNnSc4d1	užitečné
–	–	k?	–
ale	ale	k8xC	ale
ne	ne	k9	ne
dostatečně	dostatečně	k6eAd1	dostatečně
–	–	k?	–
spjaté	spjatý	k2eAgFnPc4d1	spjatá
s	s	k7c7	s
expanzivní	expanzivní	k2eAgFnSc7d1	expanzivní
politikou	politika	k1gFnSc7	politika
Filipa	Filip	k1gMnSc2	Filip
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Nálezy	nález	k1gInPc1	nález
zlata	zlato	k1gNnSc2	zlato
vedly	vést	k5eAaImAgInP	vést
zároveň	zároveň	k6eAd1	zároveň
k	k	k7c3	k
ohromné	ohromný	k2eAgFnSc3d1	ohromná
inflaci	inflace	k1gFnSc3	inflace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
století	století	k1gNnSc2	století
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
500	[number]	k4	500
procent	procento	k1gNnPc2	procento
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
prosperita	prosperita	k1gFnSc1	prosperita
dosažená	dosažený	k2eAgFnSc1d1	dosažená
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
prostředků	prostředek	k1gInPc2	prostředek
ze	z	k7c2	z
zlata	zlato	k1gNnSc2	zlato
a	a	k8xC	a
stříbra	stříbro	k1gNnSc2	stříbro
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
nepodporovala	podporovat	k5eNaImAgFnS	podporovat
rozvoj	rozvoj	k1gInSc4	rozvoj
ekonomiky	ekonomika	k1gFnSc2	ekonomika
–	–	k?	–
společnost	společnost	k1gFnSc1	společnost
žila	žít	k5eAaImAgFnS	žít
v	v	k7c6	v
radovánkách	radovánka	k1gFnPc6	radovánka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
města	město	k1gNnPc1	město
pomalu	pomalu	k6eAd1	pomalu
upadala	upadat	k5eAaImAgNnP	upadat
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
dovoz	dovoz	k1gInSc1	dovoz
hotových	hotový	k2eAgInPc2d1	hotový
výrobků	výrobek	k1gInPc2	výrobek
převažoval	převažovat	k5eAaImAgInS	převažovat
nad	nad	k7c7	nad
vývozem	vývoz	k1gInSc7	vývoz
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
deficitu	deficit	k1gInSc3	deficit
obchodní	obchodní	k2eAgFnSc2d1	obchodní
bilance	bilance	k1gFnSc2	bilance
oproti	oproti	k7c3	oproti
zbytku	zbytek	k1gInSc3	zbytek
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1598	[number]	k4	1598
tedy	tedy	k8xC	tedy
Filip	Filip	k1gMnSc1	Filip
III	III	kA	III
<g/>
.	.	kIx.	.
získal	získat	k5eAaPmAgInS	získat
zemi	zem	k1gFnSc4	zem
v	v	k7c6	v
ekonomické	ekonomický	k2eAgFnSc6d1	ekonomická
krizi	krize	k1gFnSc6	krize
a	a	k8xC	a
v	v	k7c6	v
konfliktu	konflikt	k1gInSc6	konflikt
s	s	k7c7	s
téměř	téměř	k6eAd1	téměř
celou	celý	k2eAgFnSc4d1	celá
západní	západní	k2eAgFnSc4d1	západní
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Politicky	politicky	k6eAd1	politicky
nevychovaný	vychovaný	k2eNgMnSc1d1	nevychovaný
vládce	vládce	k1gMnSc1	vládce
<g/>
,	,	kIx,	,
neseznámený	seznámený	k2eNgMnSc1d1	neseznámený
se	s	k7c7	s
složitostí	složitost	k1gFnSc7	složitost
správy	správa	k1gFnSc2	správa
<g/>
,	,	kIx,	,
fascinován	fascinovat	k5eAaBmNgMnS	fascinovat
divadlem	divadlo	k1gNnSc7	divadlo
<g/>
,	,	kIx,	,
malířstvím	malířství	k1gNnSc7	malířství
a	a	k8xC	a
lovem	lov	k1gInSc7	lov
hledal	hledat	k5eAaImAgMnS	hledat
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
podporu	podpora	k1gFnSc4	podpora
mezi	mezi	k7c7	mezi
svými	svůj	k3xOyFgMnPc7	svůj
věrnými	věrný	k2eAgMnPc7d1	věrný
šlechtici	šlechtic	k1gMnPc7	šlechtic
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k6eAd1	tak
začala	začít	k5eAaPmAgFnS	začít
éra	éra	k1gFnSc1	éra
validos	validosa	k1gFnPc2	validosa
(	(	kIx(	(
<g/>
milců	milec	k1gMnPc2	milec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ministrů	ministr	k1gMnPc2	ministr
a	a	k8xC	a
královských	královský	k2eAgMnPc2d1	královský
oblíbenců	oblíbenec	k1gMnPc2	oblíbenec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
převzali	převzít	k5eAaPmAgMnP	převzít
de	de	k?	de
facto	facto	k1gNnSc4	facto
moc	moc	k6eAd1	moc
nad	nad	k7c7	nad
zemí	zem	k1gFnSc7	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rady	rada	k1gFnPc1	rada
přátel	přítel	k1gMnPc2	přítel
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
španělských	španělský	k2eAgMnPc2d1	španělský
šlechticů	šlechtic	k1gMnPc2	šlechtic
stál	stát	k5eAaImAgMnS	stát
Francisco	Francisco	k1gMnSc1	Francisco
Gómez	Gómez	k1gMnSc1	Gómez
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
královského	královský	k2eAgInSc2d1	královský
dvoru	dvůr	k1gInSc3	dvůr
se	se	k3xPyFc4	se
pohyboval	pohybovat	k5eAaImAgInS	pohybovat
již	již	k6eAd1	již
od	od	k7c2	od
třetího	třetí	k4xOgInSc2	třetí
roku	rok	k1gInSc2	rok
života	život	k1gInSc2	život
Filipa	Filip	k1gMnSc2	Filip
III	III	kA	III
<g/>
.	.	kIx.	.
a	a	k8xC	a
proto	proto	k8xC	proto
měl	mít	k5eAaImAgMnS	mít
dost	dost	k6eAd1	dost
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vypozoroval	vypozorovat	k5eAaPmAgMnS	vypozorovat
jistou	jistý	k2eAgFnSc4d1	jistá
nesmělost	nesmělost	k1gFnSc4	nesmělost
u	u	k7c2	u
mladého	mladý	k2eAgMnSc2d1	mladý
infanta	infant	k1gMnSc2	infant
(	(	kIx(	(
<g/>
prince	princ	k1gMnSc2	princ
<g/>
)	)	kIx)	)
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
si	se	k3xPyFc3	se
získal	získat	k5eAaPmAgInS	získat
jeho	jeho	k3xOp3gFnSc4	jeho
důvěru	důvěra	k1gFnSc4	důvěra
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
se	se	k3xPyFc4	se
Filip	Filip	k1gMnSc1	Filip
II	II	kA	II
<g/>
.	.	kIx.	.
snažil	snažit	k5eAaImAgMnS	snažit
jeho	on	k3xPp3gInSc4	on
vliv	vliv	k1gInSc4	vliv
u	u	k7c2	u
dvora	dvůr	k1gInSc2	dvůr
zmenšit	zmenšit	k5eAaPmF	zmenšit
(	(	kIx(	(
<g/>
v	v	k7c6	v
1595	[number]	k4	1595
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
vévodu	vévoda	k1gMnSc4	vévoda
z	z	k7c2	z
Lermy	Lermy	k?	Lermy
místokrálem	místokrál	k1gMnSc7	místokrál
Valencie	Valencie	k1gFnSc2	Valencie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
když	když	k8xS	když
stáří	stáří	k1gNnSc4	stáří
krále	král	k1gMnSc2	král
připoutalo	připoutat	k5eAaPmAgNnS	připoutat
k	k	k7c3	k
lůžku	lůžko	k1gNnSc3	lůžko
a	a	k8xC	a
zbavilo	zbavit	k5eAaPmAgNnS	zbavit
ho	on	k3xPp3gInSc2	on
vlivu	vliv	k1gInSc2	vliv
nad	nad	k7c7	nad
zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
vévoda	vévoda	k1gMnSc1	vévoda
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Madridu	Madrid	k1gInSc2	Madrid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ihned	ihned	k6eAd1	ihned
po	po	k7c4	po
převzetí	převzetí	k1gNnSc4	převzetí
koruny	koruna	k1gFnSc2	koruna
Filipem	Filip	k1gMnSc7	Filip
III	III	kA	III
<g/>
.	.	kIx.	.
získal	získat	k5eAaPmAgInS	získat
Gómez	Gómez	k1gInSc1	Gómez
neomezený	omezený	k2eNgInSc4d1	neomezený
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
vládu	vláda	k1gFnSc4	vláda
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Lermy	Lermy	k?	Lermy
(	(	kIx(	(
<g/>
král	král	k1gMnSc1	král
udělil	udělit	k5eAaPmAgMnS	udělit
Gómezovi	Gómezův	k2eAgMnPc1d1	Gómezův
tento	tento	k3xDgInSc4	tento
titul	titul	k1gInSc4	titul
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1599	[number]	k4	1599
<g/>
)	)	kIx)	)
vlastně	vlastně	k9	vlastně
mohl	moct	k5eAaImAgInS	moct
nahradit	nahradit	k5eAaPmF	nahradit
krále	král	k1gMnSc4	král
úplně	úplně	k6eAd1	úplně
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
si	se	k3xPyFc3	se
později	pozdě	k6eAd2	pozdě
vymohl	vymoct	k5eAaPmAgMnS	vymoct
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
podepisování	podepisování	k1gNnSc4	podepisování
královských	královský	k2eAgFnPc2d1	královská
listin	listina	k1gFnPc2	listina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dalším	další	k2eAgInSc7d1	další
krokem	krok	k1gInSc7	krok
vlivného	vlivný	k2eAgMnSc2d1	vlivný
vévody	vévoda	k1gMnSc2	vévoda
z	z	k7c2	z
Lermy	Lermy	k?	Lermy
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
přesunutí	přesunutí	k1gNnSc4	přesunutí
královského	královský	k2eAgInSc2d1	královský
dvora	dvůr	k1gInSc2	dvůr
z	z	k7c2	z
Madridu	Madrid	k1gInSc2	Madrid
do	do	k7c2	do
Valladolidu	Valladolid	k1gInSc2	Valladolid
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1601	[number]	k4	1601
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
bylo	být	k5eAaImAgNnS	být
posílení	posílení	k1gNnSc1	posílení
vlastního	vlastní	k2eAgInSc2d1	vlastní
vlivu	vliv	k1gInSc2	vliv
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
ostatních	ostatní	k2eAgMnPc2d1	ostatní
královských	královský	k2eAgMnPc2d1	královský
dvořanů	dvořan	k1gMnPc2	dvořan
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
císařovny	císařovna	k1gFnSc2	císařovna
Marie	Maria	k1gFnSc2	Maria
<g/>
,	,	kIx,	,
manželky	manželka	k1gFnPc1	manželka
Maxmiliána	Maxmilián	k1gMnSc2	Maxmilián
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
vrátila	vrátit	k5eAaPmAgFnS	vrátit
do	do	k7c2	do
Španělska	Španělsko	k1gNnSc2	Španělsko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1581	[number]	k4	1581
a	a	k8xC	a
žila	žít	k5eAaImAgFnS	žít
zde	zde	k6eAd1	zde
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dopadem	dopad	k1gInSc7	dopad
tohoto	tento	k3xDgNnSc2	tento
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
<g/>
,	,	kIx,	,
však	však	k9	však
byly	být	k5eAaImAgInP	být
obrovské	obrovský	k2eAgInPc4d1	obrovský
náklady	náklad	k1gInPc4	náklad
na	na	k7c4	na
práci	práce	k1gFnSc4	práce
a	a	k8xC	a
fungování	fungování	k1gNnSc4	fungování
dvora	dvůr	k1gInSc2	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
Králi	Král	k1gMnSc3	Král
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
přemístit	přemístit	k5eAaPmF	přemístit
dvůr	dvůr	k1gInSc4	dvůr
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Madridu	Madrid	k1gInSc2	Madrid
za	za	k7c4	za
5	[number]	k4	5
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stálo	stát	k5eAaImAgNnS	stát
ho	on	k3xPp3gMnSc4	on
to	ten	k3xDgNnSc1	ten
250	[number]	k4	250
000	[number]	k4	000
dukátů	dukát	k1gInPc2	dukát
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
dohromady	dohromady	k6eAd1	dohromady
zaplatil	zaplatit	k5eAaPmAgMnS	zaplatit
Madridu	Madrid	k1gInSc3	Madrid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Válka	válka	k1gFnSc1	válka
a	a	k8xC	a
mír	mír	k1gInSc1	mír
===	===	k?	===
</s>
</p>
<p>
<s>
Vláda	vláda	k1gFnSc1	vláda
Filipa	Filip	k1gMnSc2	Filip
III	III	kA	III
<g/>
.	.	kIx.	.
plynula	plynout	k5eAaImAgFnS	plynout
ve	v	k7c6	v
znamení	znamení	k1gNnSc6	znamení
míru	míra	k1gFnSc4	míra
a	a	k8xC	a
uzavírání	uzavírání	k1gNnSc4	uzavírání
smluv	smlouva	k1gFnPc2	smlouva
s	s	k7c7	s
nepřáteli	nepřítel	k1gMnPc7	nepřítel
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byl	být	k5eAaImAgMnS	být
zásadní	zásadní	k2eAgInSc4d1	zásadní
rozdíl	rozdíl	k1gInSc4	rozdíl
oproti	oproti	k7c3	oproti
vládě	vláda	k1gFnSc3	vláda
děda	děd	k1gMnSc2	děd
Karla	Karel	k1gMnSc2	Karel
V.	V.	kA	V.
a	a	k8xC	a
otce	otec	k1gMnSc2	otec
Filipa	Filip	k1gMnSc2	Filip
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tuto	tento	k3xDgFnSc4	tento
vládu	vláda	k1gFnSc4	vláda
vděčí	vděčit	k5eAaImIp3nS	vděčit
Španělsko	Španělsko	k1gNnSc1	Španělsko
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
politice	politika	k1gFnSc6	politika
vévody	vévoda	k1gMnSc2	vévoda
z	z	k7c2	z
Lermy	Lermy	k?	Lermy
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
neustále	neustále	k6eAd1	neustále
hledal	hledat	k5eAaImAgInS	hledat
konec	konec	k1gInSc1	konec
války	válka	k1gFnSc2	válka
točících	točící	k2eAgInPc2d1	točící
se	se	k3xPyFc4	se
okolo	okolo	k7c2	okolo
koruny	koruna	k1gFnSc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
vstupem	vstup	k1gInSc7	vstup
Filipa	Filip	k1gMnSc2	Filip
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
francouzských	francouzský	k2eAgInPc6d1	francouzský
Vervins	Vervinsa	k1gFnPc2	Vervinsa
podepsána	podepsat	k5eAaPmNgFnS	podepsat
mírová	mírový	k2eAgFnSc1d1	mírová
smlouva	smlouva	k1gFnSc1	smlouva
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ukončila	ukončit	k5eAaPmAgFnS	ukončit
válku	válka	k1gFnSc4	válka
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc4d1	další
události	událost	k1gFnPc4	událost
byla	být	k5eAaImAgFnS	být
smrt	smrt	k1gFnSc1	smrt
anglické	anglický	k2eAgFnSc2d1	anglická
královny	královna	k1gFnSc2	královna
Alžběty	Alžběta	k1gFnSc2	Alžběta
I.	I.	kA	I.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1603	[number]	k4	1603
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
Filipovi	Filip	k1gMnSc3	Filip
podepsat	podepsat	k5eAaPmF	podepsat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1605	[number]	k4	1605
mírovou	mírový	k2eAgFnSc4d1	mírová
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
katolickým	katolický	k2eAgMnSc7d1	katolický
Jakubem	Jakub	k1gMnSc7	Jakub
I.	I.	kA	I.
Smlouva	smlouva	k1gFnSc1	smlouva
zaváděla	zavádět	k5eAaImAgFnS	zavádět
volný	volný	k2eAgInSc4d1	volný
obchod	obchod	k1gInSc4	obchod
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
zeměmi	zem	k1gFnPc7	zem
a	a	k8xC	a
zajišťovala	zajišťovat	k5eAaImAgFnS	zajišťovat
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
pro	pro	k7c4	pro
anglické	anglický	k2eAgMnPc4d1	anglický
obchodníky	obchodník	k1gMnPc4	obchodník
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
nemohli	moct	k5eNaImAgMnP	moct
být	být	k5eAaImF	být
napříště	napříště	k6eAd1	napříště
stíhání	stíhání	k1gNnSc4	stíhání
inkvizicí	inkvizice	k1gFnPc2	inkvizice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Úspěch	úspěch	k1gInSc1	úspěch
si	se	k3xPyFc3	se
také	také	k6eAd1	také
zajistilo	zajistit	k5eAaPmAgNnS	zajistit
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
byly	být	k5eAaImAgFnP	být
snahy	snaha	k1gFnPc1	snaha
k	k	k7c3	k
udržení	udržení	k1gNnSc3	udržení
statutu	statut	k1gInSc2	statut
quo	quo	k?	quo
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
mnoho	mnoho	k4c4	mnoho
Španělů	Španěl	k1gMnPc2	Španěl
by	by	kYmCp3nS	by
byl	být	k5eAaImAgMnS	být
mír	mír	k1gInSc4	mír
s	s	k7c7	s
Nizozemskem	Nizozemsko	k1gNnSc7	Nizozemsko
jistý	jistý	k2eAgInSc1d1	jistý
ústupek	ústupek	k1gInSc1	ústupek
kacířství	kacířství	k1gNnSc2	kacířství
a	a	k8xC	a
tiché	tichý	k2eAgNnSc4d1	tiché
schválení	schválení	k1gNnSc4	schválení
povstání	povstání	k1gNnSc2	povstání
proti	proti	k7c3	proti
monarchii	monarchie	k1gFnSc3	monarchie
<g/>
,	,	kIx,	,
Nizozemcům	Nizozemec	k1gMnPc3	Nizozemec
zase	zase	k9	zase
vyhovoval	vyhovovat	k5eAaImAgInS	vyhovovat
válečný	válečný	k2eAgInSc1d1	válečný
stav	stav	k1gInSc1	stav
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
jim	on	k3xPp3gMnPc3	on
umožnil	umožnit	k5eAaPmAgInS	umožnit
beztrestně	beztrestně	k6eAd1	beztrestně
bojovat	bojovat	k5eAaImF	bojovat
v	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
mír	mír	k1gInSc4	mír
nenastala	nastat	k5eNaPmAgFnS	nastat
ještě	ještě	k9	ještě
vhodná	vhodný	k2eAgFnSc1d1	vhodná
doba	doba	k1gFnSc1	doba
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
bylo	být	k5eAaImAgNnS	být
podepsáno	podepsat	k5eAaPmNgNnS	podepsat
9	[number]	k4	9
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1609	[number]	k4	1609
pouze	pouze	k6eAd1	pouze
příměří	příměří	k1gNnSc4	příměří
<g/>
,	,	kIx,	,
vypočítané	vypočítaný	k2eAgInPc4d1	vypočítaný
na	na	k7c4	na
12	[number]	k4	12
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ale	ale	k9	ale
de	de	k?	de
facto	facto	k1gNnSc1	facto
Nizozemcům	Nizozemec	k1gMnPc3	Nizozemec
uznalo	uznat	k5eAaPmAgNnS	uznat
samostatnost	samostatnost	k1gFnSc4	samostatnost
<g/>
.	.	kIx.	.
</s>
<s>
Příměří	příměří	k1gNnSc1	příměří
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
jako	jako	k9	jako
nestabilní	stabilní	k2eNgMnPc1d1	nestabilní
–	–	k?	–
Nizozemci	Nizozemec	k1gMnPc1	Nizozemec
dále	daleko	k6eAd2	daleko
napadali	napadat	k5eAaBmAgMnP	napadat
portugalské	portugalský	k2eAgFnPc4d1	portugalská
državy	država	k1gFnPc4	država
a	a	k8xC	a
zakládali	zakládat	k5eAaImAgMnP	zakládat
své	svůj	k3xOyFgFnPc4	svůj
vlastní	vlastní	k2eAgFnPc4d1	vlastní
kolonie	kolonie	k1gFnPc4	kolonie
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
a	a	k8xC	a
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
příměří	příměří	k1gNnSc4	příměří
v	v	k7c6	v
Nizozemsku	Nizozemsko	k1gNnSc6	Nizozemsko
zřejmě	zřejmě	k6eAd1	zřejmě
uznalo	uznat	k5eAaPmAgNnS	uznat
pouze	pouze	k6eAd1	pouze
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
,	,	kIx,	,
vytvoření	vytvoření	k1gNnSc1	vytvoření
federace	federace	k1gFnSc2	federace
Spojených	spojený	k2eAgFnPc2d1	spojená
provincií	provincie	k1gFnPc2	provincie
na	na	k7c6	na
severu	sever	k1gInSc6	sever
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
znamenalo	znamenat	k5eAaImAgNnS	znamenat
uklidnění	uklidnění	k1gNnSc4	uklidnění
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
totiž	totiž	k9	totiž
pro	pro	k7c4	pro
Evropu	Evropa	k1gFnSc4	Evropa
silně	silně	k6eAd1	silně
polarizující	polarizující	k2eAgInSc1d1	polarizující
prvek	prvek	k1gInSc1	prvek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
znesvářel	znesvářet	k5eAaPmAgInS	znesvářet
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
,	,	kIx,	,
podporované	podporovaný	k2eAgNnSc1d1	podporované
Svatou	svatý	k2eAgFnSc7d1	svatá
říší	říš	k1gFnSc7	říš
a	a	k8xC	a
většinou	většina	k1gFnSc7	většina
katolických	katolický	k2eAgFnPc2d1	katolická
zemí	zem	k1gFnPc2	zem
a	a	k8xC	a
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
protestantské	protestantský	k2eAgFnPc1d1	protestantská
země	zem	k1gFnPc1	zem
a	a	k8xC	a
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
i	i	k8xC	i
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Dvanáctileté	dvanáctiletý	k2eAgNnSc4d1	dvanáctileté
příměří	příměří	k1gNnSc4	příměří
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
pax	pax	k?	pax
hispanica	hispanica	k1gFnSc1	hispanica
<g/>
)	)	kIx)	)
uvedlo	uvést	k5eAaPmAgNnS	uvést
Evropu	Evropa	k1gFnSc4	Evropa
do	do	k7c2	do
klidu	klid	k1gInSc2	klid
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
několika	několik	k4yIc2	několik
let	léto	k1gNnPc2	léto
přerušených	přerušený	k2eAgMnPc2d1	přerušený
pouze	pouze	k6eAd1	pouze
lokální	lokální	k2eAgInPc4d1	lokální
konflikty	konflikt	k1gInPc4	konflikt
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
stav	stav	k1gInSc1	stav
zůstal	zůstat	k5eAaPmAgInS	zůstat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1618	[number]	k4	1618
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
začala	začít	k5eAaPmAgFnS	začít
třicetiletá	třicetiletý	k2eAgFnSc1d1	třicetiletá
válka	válka	k1gFnSc1	válka
pražskou	pražský	k2eAgFnSc7d1	Pražská
defenestrací	defenestrace	k1gFnSc7	defenestrace
<g/>
.	.	kIx.	.
</s>
<s>
Vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Lermy	Lermy	k?	Lermy
byl	být	k5eAaImAgInS	být
proti	proti	k7c3	proti
zapojení	zapojení	k1gNnSc3	zapojení
Španělska	Španělsko	k1gNnSc2	Španělsko
do	do	k7c2	do
konfliktu	konflikt	k1gInSc2	konflikt
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
však	však	k9	však
upadl	upadnout	k5eAaPmAgMnS	upadnout
v	v	k7c4	v
nemilost	nemilost	k1gFnSc4	nemilost
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
ostatní	ostatní	k2eAgMnPc1d1	ostatní
dvořané	dvořan	k1gMnPc1	dvořan
byli	být	k5eAaImAgMnP	být
připraveni	připravit	k5eAaPmNgMnP	připravit
k	k	k7c3	k
boji	boj	k1gInSc3	boj
o	o	k7c4	o
obnovení	obnovení	k1gNnSc4	obnovení
prestiže	prestiž	k1gFnSc2	prestiž
monarchie	monarchie	k1gFnSc2	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
Španělsko	Španělsko	k1gNnSc1	Španělsko
rychle	rychle	k6eAd1	rychle
poslalo	poslat	k5eAaPmAgNnS	poslat
armádu	armáda	k1gFnSc4	armáda
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitějším	důležitý	k2eAgInSc7d3	nejdůležitější
argumentem	argument	k1gInSc7	argument
pro	pro	k7c4	pro
zapojení	zapojení	k1gNnSc4	zapojení
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
byla	být	k5eAaImAgFnS	být
otázka	otázka	k1gFnSc1	otázka
práv	právo	k1gNnPc2	právo
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
koruně	koruna	k1gFnSc6	koruna
–	–	k?	–
česky	česky	k6eAd1	česky
král	král	k1gMnSc1	král
<g/>
,	,	kIx,	,
Fridrich	Fridrich	k1gMnSc1	Fridrich
Falcký	falcký	k2eAgMnSc1d1	falcký
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
sedmi	sedm	k4xCc2	sedm
kurfiřtů	kurfiřt	k1gMnPc2	kurfiřt
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
by	by	kYmCp3nS	by
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
důsledku	důsledek	k1gInSc6	důsledek
mohlo	moct	k5eAaImAgNnS	moct
znamenat	znamenat	k5eAaImF	znamenat
převzetí	převzetí	k1gNnSc4	převzetí
moci	moc	k1gFnSc2	moc
protestantů	protestant	k1gMnPc2	protestant
ve	v	k7c6	v
Svaté	svatý	k2eAgFnSc6d1	svatá
říši	říš	k1gFnSc6	říš
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
by	by	kYmCp3nP	by
Španělsko	Španělsko	k1gNnSc1	Španělsko
ztratilo	ztratit	k5eAaPmAgNnS	ztratit
hlavního	hlavní	k2eAgMnSc4d1	hlavní
spojence	spojenec	k1gMnSc4	spojenec
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Přítel	přítel	k1gMnSc1	přítel
umění	umění	k1gNnSc4	umění
a	a	k8xC	a
nepřítel	nepřítel	k1gMnSc1	nepřítel
morisků	morisk	k1gInPc2	morisk
===	===	k?	===
</s>
</p>
<p>
<s>
Filip	Filip	k1gMnSc1	Filip
III	III	kA	III
<g/>
.	.	kIx.	.
sice	sice	k8xC	sice
neměl	mít	k5eNaImAgMnS	mít
velký	velký	k2eAgInSc4d1	velký
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
vládu	vláda	k1gFnSc4	vláda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
měl	mít	k5eAaImAgInS	mít
velkou	velký	k2eAgFnSc4d1	velká
zálibu	záliba	k1gFnSc4	záliba
v	v	k7c6	v
kultuře	kultura	k1gFnSc6	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
zájem	zájem	k1gInSc1	zájem
soustřeďoval	soustřeďovat	k5eAaImAgInS	soustřeďovat
na	na	k7c4	na
literaturu	literatura	k1gFnSc4	literatura
<g/>
,	,	kIx,	,
především	především	k9	především
na	na	k7c4	na
dramatickou	dramatický	k2eAgFnSc4d1	dramatická
tvorbu	tvorba	k1gFnSc4	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
literatura	literatura	k1gFnSc1	literatura
asi	asi	k9	asi
největší	veliký	k2eAgInSc4d3	veliký
rozkvět	rozkvět	k1gInSc4	rozkvět
v	v	k7c6	v
španělské	španělský	k2eAgFnSc6d1	španělská
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1605	[number]	k4	1605
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
první	první	k4xOgFnSc1	první
část	část	k1gFnSc1	část
románu	román	k1gInSc2	román
Don	dona	k1gFnPc2	dona
Quijote	Quijot	k1gInSc5	Quijot
od	od	k7c2	od
Miguela	Miguel	k1gMnSc2	Miguel
Cervantese	Cervantese	k1gFnSc2	Cervantese
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
o	o	k7c4	o
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
publikoval	publikovat	k5eAaBmAgInS	publikovat
i	i	k9	i
druhou	druhý	k4xOgFnSc4	druhý
část	část	k1gFnSc4	část
<g/>
.	.	kIx.	.
</s>
<s>
Osobní	osobní	k2eAgMnSc1d1	osobní
kaplan	kaplan	k1gMnSc1	kaplan
na	na	k7c6	na
dvoře	dvůr	k1gInSc6	dvůr
krále	král	k1gMnSc2	král
byl	být	k5eAaImAgMnS	být
vynikající	vynikající	k2eAgMnSc1d1	vynikající
básník	básník	k1gMnSc1	básník
Luis	Luisa	k1gFnPc2	Luisa
de	de	k?	de
Góngora	Góngor	k1gMnSc2	Góngor
y	y	k?	y
Argote	argot	k1gInSc5	argot
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
také	také	k6eAd1	také
započal	započnout	k5eAaPmAgInS	započnout
svoji	svůj	k3xOyFgFnSc4	svůj
kariéru	kariéra	k1gFnSc4	kariéra
Francisco	Francisco	k6eAd1	Francisco
de	de	k?	de
Quevedo	Quevedo	k1gNnSc1	Quevedo
Baltasar	Baltasar	k1gMnSc1	Baltasar
Gracián	Gracián	k1gMnSc1	Gracián
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vpravdě	vpravdě	k9	vpravdě
však	však	k9	však
prožívala	prožívat	k5eAaImAgFnS	prožívat
svůj	svůj	k3xOyFgInSc4	svůj
zlatý	zlatý	k2eAgInSc4d1	zlatý
věk	věk	k1gInSc4	věk
španělská	španělský	k2eAgFnSc1d1	španělská
divadelní	divadelní	k2eAgFnSc1d1	divadelní
tvorba	tvorba	k1gFnSc1	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
píše	psát	k5eAaImIp3nS	psát
dramata	drama	k1gNnPc1	drama
William	William	k1gInSc4	William
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
<g/>
,	,	kIx,	,
na	na	k7c6	na
Pyrenejském	pyrenejský	k2eAgInSc6d1	pyrenejský
poloostrově	poloostrov	k1gInSc6	poloostrov
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nP	tvořit
Lope	Lop	k1gFnPc4	Lop
de	de	k?	de
Vega	Vega	k1gMnSc1	Vega
(	(	kIx(	(
<g/>
známý	známý	k1gMnSc1	známý
jako	jako	k8xC	jako
zakladatel	zakladatel	k1gMnSc1	zakladatel
národního	národní	k2eAgNnSc2d1	národní
dramatu	drama	k1gNnSc2	drama
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tirso	Tirsa	k1gFnSc5	Tirsa
de	de	k?	de
Molina	molino	k1gNnSc2	molino
<g/>
,	,	kIx,	,
Juan	Juan	k1gMnSc1	Juan
Ruiz	Ruiz	k1gMnSc1	Ruiz
de	de	k?	de
Alarcón	Alarcón	k1gMnSc1	Alarcón
<g/>
,	,	kIx,	,
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
poté	poté	k6eAd1	poté
Pedro	Pedro	k1gNnSc1	Pedro
Calderón	Calderón	k1gInSc1	Calderón
de	de	k?	de
la	la	k1gNnSc1	la
Barca	Barc	k1gInSc2	Barc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
architektury	architektura	k1gFnSc2	architektura
se	se	k3xPyFc4	se
nejvíce	hodně	k6eAd3	hodně
objevuje	objevovat	k5eAaImIp3nS	objevovat
problém	problém	k1gInSc1	problém
související	související	k2eAgInSc1d1	související
s	s	k7c7	s
úpadkem	úpadek	k1gInSc7	úpadek
měšťanstva	měšťanstvo	k1gNnSc2	měšťanstvo
a	a	k8xC	a
ekonomickou	ekonomický	k2eAgFnSc7d1	ekonomická
recesí	recese	k1gFnSc7	recese
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
však	však	k9	však
dobré	dobrý	k2eAgNnSc1d1	dobré
upozornit	upozornit	k5eAaPmF	upozornit
na	na	k7c4	na
některé	některý	k3yIgInPc4	některý
pozůstatky	pozůstatek	k1gInPc4	pozůstatek
z	z	k7c2	z
Filipovy	Filipův	k2eAgFnSc2d1	Filipova
éry	éra	k1gFnSc2	éra
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
především	především	k9	především
na	na	k7c4	na
náměstí	náměstí	k1gNnSc4	náměstí
Plaza	plaz	k1gMnSc2	plaz
Mayor	Mayor	k1gMnSc1	Mayor
v	v	k7c6	v
Madridu	Madrid	k1gInSc6	Madrid
(	(	kIx(	(
<g/>
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
náměstí	náměstí	k1gNnSc2	náměstí
stojí	stát	k5eAaImIp3nS	stát
dodnes	dodnes	k6eAd1	dodnes
jezdecká	jezdecký	k2eAgFnSc1d1	jezdecká
socha	socha	k1gFnSc1	socha
krále	král	k1gMnSc2	král
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
kulturní	kulturní	k2eAgInSc1d1	kulturní
rozkvět	rozkvět	k1gInSc1	rozkvět
měl	mít	k5eAaImAgInS	mít
ale	ale	k9	ale
i	i	k9	i
svoji	svůj	k3xOyFgFnSc4	svůj
stinnou	stinný	k2eAgFnSc4d1	stinná
stránku	stránka	k1gFnSc4	stránka
spojenou	spojený	k2eAgFnSc4d1	spojená
s	s	k7c7	s
postupem	postup	k1gInSc7	postup
kontrareformace	kontrareformace	k1gFnSc2	kontrareformace
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
týkalo	týkat	k5eAaImAgNnS	týkat
muslimské	muslimský	k2eAgFnPc4d1	muslimská
populace	populace	k1gFnPc4	populace
na	na	k7c6	na
poloostrově	poloostrov	k1gInSc6	poloostrov
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
reconquistě	reconquist	k1gInSc6	reconquist
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
konvertovat	konvertovat	k5eAaBmF	konvertovat
na	na	k7c4	na
křesťanskou	křesťanský	k2eAgFnSc4d1	křesťanská
víru	víra	k1gFnSc4	víra
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
morisků	morisk	k1gInPc2	morisk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
byli	být	k5eAaImAgMnP	být
mnohdy	mnohdy	k6eAd1	mnohdy
majetní	majetný	k2eAgMnPc1d1	majetný
a	a	k8xC	a
pracovití	pracovitý	k2eAgMnPc1d1	pracovitý
<g/>
.	.	kIx.	.
</s>
<s>
Moriskové	Moriskový	k2eAgNnSc1d1	Moriskový
byli	být	k5eAaImAgMnP	být
diskriminováni	diskriminovat	k5eAaBmNgMnP	diskriminovat
již	již	k9	již
za	za	k7c2	za
Filipa	Filip	k1gMnSc2	Filip
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
až	až	k9	až
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
tlakem	tlak	k1gInSc7	tlak
z	z	k7c2	z
duchovních	duchovní	k2eAgInPc2d1	duchovní
a	a	k8xC	a
vojenských	vojenský	k2eAgInPc2d1	vojenský
kruhů	kruh	k1gInPc2	kruh
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
pro	pro	k7c4	pro
konečné	konečný	k2eAgNnSc4d1	konečné
řešení	řešení	k1gNnSc4	řešení
tohoto	tento	k3xDgInSc2	tento
problému	problém	k1gInSc2	problém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
1609	[number]	k4	1609
<g/>
,	,	kIx,	,
vydal	vydat	k5eAaPmAgInS	vydat
edikt	edikt	k1gInSc1	edikt
přikazující	přikazující	k2eAgNnSc1d1	přikazující
moriskům	morisk	k1gInPc3	morisk
opustit	opustit	k5eAaPmF	opustit
oblast	oblast	k1gFnSc4	oblast
Valencie	Valencie	k1gFnSc2	Valencie
a	a	k8xC	a
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
je	být	k5eAaImIp3nS	být
vyhnal	vyhnat	k5eAaPmAgMnS	vyhnat
z	z	k7c2	z
Aragonie	Aragonie	k1gFnSc2	Aragonie
a	a	k8xC	a
Kastilie	Kastilie	k1gFnSc2	Kastilie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
krátké	krátký	k2eAgFnSc6d1	krátká
době	doba	k1gFnSc6	doba
Španělsko	Španělsko	k1gNnSc1	Španělsko
přišlo	přijít	k5eAaPmAgNnS	přijít
o	o	k7c4	o
300	[number]	k4	300
až	až	k9	až
600	[number]	k4	600
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
ze	z	k7c2	z
svých	svůj	k3xOyFgInPc2	svůj
osmi	osm	k4xCc2	osm
milionů	milion	k4xCgInPc2	milion
občanů	občan	k1gMnPc2	občan
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
krok	krok	k1gInSc1	krok
nezůstal	zůstat	k5eNaPmAgInS	zůstat
bez	bez	k7c2	bez
vlivu	vliv	k1gInSc2	vliv
na	na	k7c4	na
hospodářství	hospodářství	k1gNnSc4	hospodářství
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
ve	v	k7c6	v
Valencii	Valencie	k1gFnSc6	Valencie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žila	žít	k5eAaImAgFnS	žít
téměř	téměř	k6eAd1	téměř
polovina	polovina	k1gFnSc1	polovina
ze	z	k7c2	z
španělských	španělský	k2eAgInPc2d1	španělský
morisků	morisk	k1gInPc2	morisk
<g/>
.	.	kIx.	.
</s>
<s>
Prudký	prudký	k2eAgInSc1d1	prudký
pokles	pokles	k1gInSc1	pokles
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
státu	stát	k1gInSc2	stát
(	(	kIx(	(
<g/>
k	k	k7c3	k
čemuž	což	k3yQnSc3	což
také	také	k9	také
přispěly	přispět	k5eAaPmAgFnP	přispět
epidemie	epidemie	k1gFnPc1	epidemie
moru	mor	k1gInSc2	mor
a	a	k8xC	a
emigrace	emigrace	k1gFnSc2	emigrace
do	do	k7c2	do
amerických	americký	k2eAgFnPc2d1	americká
držav	država	k1gFnPc2	država
<g/>
)	)	kIx)	)
znamenal	znamenat	k5eAaImAgInS	znamenat
pokles	pokles	k1gInSc4	pokles
počtu	počet	k1gInSc2	počet
pracovních	pracovní	k2eAgFnPc2d1	pracovní
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
nižší	nízký	k2eAgFnSc7d2	nižší
poptávkou	poptávka	k1gFnSc7	poptávka
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
a	a	k8xC	a
stratifikace	stratifikace	k1gFnSc2	stratifikace
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgMnSc4	ten
však	však	k9	však
fanatikové	fanatik	k1gMnPc1	fanatik
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
valencijský	valencijský	k2eAgMnSc1d1	valencijský
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
Don	Don	k1gMnSc1	Don
Juan	Juan	k1gMnSc1	Juan
de	de	k?	de
Ribero	Ribero	k1gNnSc4	Ribero
nebo	nebo	k8xC	nebo
lidé	člověk	k1gMnPc1	člověk
jako	jako	k9	jako
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Lermy	Lermy	k?	Lermy
nedbali	dbát	k5eNaImAgMnP	dbát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vyhnání	vyhnání	k1gNnPc1	vyhnání
morisků	morisk	k1gInPc2	morisk
doprovázela	doprovázet	k5eAaImAgNnP	doprovázet
další	další	k2eAgNnPc1d1	další
opatření	opatření	k1gNnPc1	opatření
namířená	namířený	k2eAgNnPc1d1	namířené
proti	proti	k7c3	proti
muslimům	muslim	k1gMnPc3	muslim
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgMnPc2	některý
vlivných	vlivný	k2eAgMnPc2d1	vlivný
dvořanů	dvořan	k1gMnPc2	dvořan
měl	mít	k5eAaImAgMnS	mít
Filip	Filip	k1gMnSc1	Filip
pozastavit	pozastavit	k5eAaPmF	pozastavit
boje	boj	k1gInPc4	boj
s	s	k7c7	s
křesťany	křesťan	k1gMnPc7	křesťan
(	(	kIx(	(
<g/>
i	i	k8xC	i
protestanty	protestant	k1gMnPc7	protestant
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
v	v	k7c6	v
Nizozemsku	Nizozemsko	k1gNnSc6	Nizozemsko
nebo	nebo	k8xC	nebo
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
)	)	kIx)	)
a	a	k8xC	a
vážně	vážně	k6eAd1	vážně
se	se	k3xPyFc4	se
pustit	pustit	k5eAaPmF	pustit
do	do	k7c2	do
boje	boj	k1gInSc2	boj
proti	proti	k7c3	proti
následovníkům	následovník	k1gMnPc3	následovník
Mohameda	Mohamed	k1gMnSc2	Mohamed
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
uzavření	uzavření	k1gNnSc6	uzavření
bojů	boj	k1gInPc2	boj
na	na	k7c6	na
severu	sever	k1gInSc6	sever
se	se	k3xPyFc4	se
španělská	španělský	k2eAgFnSc1d1	španělská
flotila	flotila	k1gFnSc1	flotila
přesunula	přesunout	k5eAaPmAgFnS	přesunout
k	k	k7c3	k
akci	akce	k1gFnSc3	akce
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
vítězná	vítězný	k2eAgFnSc1d1	vítězná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1610	[number]	k4	1610
<g/>
,	,	kIx,	,
když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
dobyto	dobyt	k2eAgNnSc1d1	dobyto
město	město	k1gNnSc1	město
Larache	Larach	k1gFnSc2	Larach
(	(	kIx(	(
<g/>
v	v	k7c6	v
dnešním	dnešní	k2eAgNnSc6d1	dnešní
Maroku	Maroko	k1gNnSc6	Maroko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1614	[number]	k4	1614
při	při	k7c6	při
obléhání	obléhání	k1gNnSc6	obléhání
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Mahdie	Mahdie	k1gFnSc2	Mahdie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1618	[number]	k4	1618
se	se	k3xPyFc4	se
Španělsko	Španělsko	k1gNnSc1	Španělsko
připravovalo	připravovat	k5eAaImAgNnS	připravovat
k	k	k7c3	k
útoku	útok	k1gInSc3	útok
na	na	k7c4	na
Alžír	Alžír	k1gInSc4	Alžír
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgFnPc4d1	hlavní
základny	základna	k1gFnPc4	základna
berberských	berberský	k2eAgMnPc2d1	berberský
pirátů	pirát	k1gMnPc2	pirát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vypuknutí	vypuknutí	k1gNnSc1	vypuknutí
povstání	povstání	k1gNnSc2	povstání
v	v	k7c6	v
Českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
a	a	k8xC	a
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
přerušilo	přerušit	k5eAaPmAgNnS	přerušit
tyto	tento	k3xDgFnPc4	tento
přípravy	příprava	k1gFnPc4	příprava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Konec	konec	k1gInSc1	konec
vlády	vláda	k1gFnSc2	vláda
silného	silný	k2eAgMnSc2d1	silný
rádce	rádce	k1gMnSc2	rádce
i	i	k8xC	i
slabého	slabý	k2eAgMnSc2d1	slabý
panovníka	panovník	k1gMnSc2	panovník
===	===	k?	===
</s>
</p>
<p>
<s>
Politika	politika	k1gFnSc1	politika
samozvaného	samozvaný	k2eAgMnSc2d1	samozvaný
španělského	španělský	k2eAgMnSc2d1	španělský
vládce	vládce	k1gMnSc2	vládce
<g/>
,	,	kIx,	,
Lermy	Lermy	k?	Lermy
<g/>
,	,	kIx,	,
dráždila	dráždit	k5eAaImAgFnS	dráždit
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
dalších	další	k2eAgMnPc2d1	další
dvořanů	dvořan	k1gMnPc2	dvořan
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgNnSc7	první
znamením	znamení	k1gNnSc7	znamení
<g/>
,	,	kIx,	,
že	že	k8xS	že
vévoda	vévoda	k1gMnSc1	vévoda
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
své	svůj	k3xOyFgFnPc4	svůj
pozice	pozice	k1gFnPc4	pozice
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
pád	pád	k1gInSc1	pád
jeho	jeho	k3xOp3gMnSc4	jeho
oblíbence	oblíbenec	k1gMnSc4	oblíbenec
Rodriga	Rodrig	k1gMnSc4	Rodrig
de	de	k?	de
Calderón	Calderón	k1gMnSc1	Calderón
<g/>
,	,	kIx,	,
markýze	markýza	k1gFnSc3	markýza
de	de	k?	de
Siete	Siet	k1gInSc5	Siet
Iglesias	Iglesias	k1gInSc4	Iglesias
<g/>
.	.	kIx.	.
</s>
<s>
Calderón	Calderón	k1gMnSc1	Calderón
byl	být	k5eAaImAgMnS	být
obviněn	obvinit	k5eAaPmNgMnS	obvinit
z	z	k7c2	z
vraždy	vražda	k1gFnSc2	vražda
a	a	k8xC	a
zpronevěry	zpronevěra	k1gFnSc2	zpronevěra
<g/>
,	,	kIx,	,
odsouzen	odsouzet	k5eAaImNgMnS	odsouzet
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
popraven	popraven	k2eAgInSc1d1	popraven
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Skutečnou	skutečný	k2eAgFnSc4d1	skutečná
ránu	rána	k1gFnSc4	rána
do	do	k7c2	do
zad	záda	k1gNnPc2	záda
pro	pro	k7c4	pro
Lermu	Lermu	k?	Lermu
bylo	být	k5eAaImAgNnS	být
spiknutí	spiknutí	k1gNnSc1	spiknutí
jeho	jeho	k3xOp3gMnSc2	jeho
syna	syn	k1gMnSc2	syn
<g/>
,	,	kIx,	,
vévody	vévoda	k1gMnSc2	vévoda
de	de	k?	de
Uceda	Uced	k1gMnSc2	Uced
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
této	tento	k3xDgFnSc3	tento
akci	akce	k1gFnSc3	akce
pozdější	pozdní	k2eAgNnSc4d2	pozdější
valido	valida	k1gFnSc5	valida
Filipa	Filip	k1gMnSc2	Filip
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
hrabě	hrabě	k1gMnSc1	hrabě
vévoda	vévoda	k1gMnSc1	vévoda
Olivarez	Olivarez	k1gMnSc1	Olivarez
<g/>
,	,	kIx,	,
a	a	k8xC	a
zpovědník	zpovědník	k1gMnSc1	zpovědník
Aliaga	Aliag	k1gMnSc2	Aliag
přiměli	přimět	k5eAaPmAgMnP	přimět
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
roku	rok	k1gInSc2	rok
1618	[number]	k4	1618
vévodu	vévoda	k1gMnSc4	vévoda
z	z	k7c2	z
Lermy	Lermy	k?	Lermy
k	k	k7c3	k
opuštění	opuštění	k1gNnSc3	opuštění
královského	královský	k2eAgInSc2d1	královský
dvora	dvůr	k1gInSc2	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
vévodovi	vévoda	k1gMnSc3	vévoda
podařilo	podařit	k5eAaPmAgNnS	podařit
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
papeže	papež	k1gMnSc4	papež
Pavla	Pavel	k1gMnSc4	Pavel
V.	V.	kA	V.
ke	k	k7c3	k
kardinálské	kardinálský	k2eAgFnSc3d1	kardinálská
kandidatuře	kandidatura	k1gFnSc3	kandidatura
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
mu	on	k3xPp3gMnSc3	on
měla	mít	k5eAaImAgFnS	mít
zajistit	zajistit	k5eAaPmF	zajistit
bezpečí	bezpečí	k1gNnSc4	bezpečí
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Valladolidu	Valladolid	k1gInSc2	Valladolid
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1624	[number]	k4	1624
odsouzen	odsoudit	k5eAaPmNgMnS	odsoudit
k	k	k7c3	k
zaplacení	zaplacení	k1gNnSc3	zaplacení
jednoho	jeden	k4xCgInSc2	jeden
milionu	milion	k4xCgInSc2	milion
dukátů	dukát	k1gInPc2	dukát
státní	státní	k2eAgFnSc3d1	státní
pokladně	pokladna	k1gFnSc3	pokladna
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
vévody	vévoda	k1gMnSc2	vévoda
z	z	k7c2	z
Lermy	Lermy	k?	Lermy
se	se	k3xPyFc4	se
o	o	k7c4	o
jeho	jeho	k3xOp3gNnSc4	jeho
místo	místo	k1gNnSc4	místo
ucházeli	ucházet	k5eAaImAgMnP	ucházet
de	de	k?	de
Uceda	Uceda	k1gMnSc1	Uceda
a	a	k8xC	a
Aliaga	Aliaga	k1gFnSc1	Aliaga
<g/>
.	.	kIx.	.
</s>
<s>
Žádný	žádný	k3yNgMnSc1	žádný
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
však	však	k9	však
již	již	k6eAd1	již
nezískal	získat	k5eNaPmAgInS	získat
úřad	úřad	k1gInSc1	úřad
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
zastával	zastávat	k5eAaImAgMnS	zastávat
Lerma	Lerma	k?	Lerma
<g/>
.	.	kIx.	.
</s>
<s>
Filip	Filip	k1gMnSc1	Filip
zemřel	zemřít	k5eAaPmAgMnS	zemřít
dne	den	k1gInSc2	den
31	[number]	k4	31
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1621	[number]	k4	1621
v	v	k7c6	v
královském	královský	k2eAgInSc6d1	královský
paláci	palác	k1gInSc6	palác
v	v	k7c6	v
Madridu	Madrid	k1gInSc6	Madrid
a	a	k8xC	a
korunu	koruna	k1gFnSc4	koruna
zdědil	zdědit	k5eAaPmAgMnS	zdědit
jeho	jeho	k3xOp3gFnSc4	jeho
syn	syn	k1gMnSc1	syn
Filip	Filip	k1gMnSc1	Filip
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Manželství	manželství	k1gNnSc1	manželství
a	a	k8xC	a
potomci	potomek	k1gMnPc1	potomek
==	==	k?	==
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1599	[number]	k4	1599
se	se	k3xPyFc4	se
Filip	Filip	k1gMnSc1	Filip
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Markétou	Markéta	k1gFnSc7	Markéta
Habsburskou	habsburský	k2eAgFnSc7d1	habsburská
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgFnSc3	třetí
dcerou	dcera	k1gFnSc7	dcera
arcivévody	arcivévoda	k1gMnSc2	arcivévoda
Karla	Karel	k1gMnSc2	Karel
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Štýrského	štýrský	k2eAgMnSc2d1	štýrský
a	a	k8xC	a
Marie	Maria	k1gFnPc4	Maria
Anny	Anna	k1gFnSc2	Anna
Bavorské	bavorský	k2eAgFnSc2d1	bavorská
<g/>
.	.	kIx.	.
</s>
<s>
Manželství	manželství	k1gNnSc1	manželství
bylo	být	k5eAaImAgNnS	být
údajně	údajně	k6eAd1	údajně
spokojené	spokojený	k2eAgNnSc1d1	spokojené
a	a	k8xC	a
narodilo	narodit	k5eAaPmAgNnS	narodit
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
osm	osm	k4xCc1	osm
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Markéta	Markéta	k1gFnSc1	Markéta
zemřela	zemřít	k5eAaPmAgFnS	zemřít
necelé	celý	k2eNgInPc4d1	necelý
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
po	po	k7c6	po
porodu	porod	k1gInSc6	porod
svého	své	k1gNnSc2	své
osmého	osmý	k4xOgNnSc2	osmý
dítěte	dítě	k1gNnSc2	dítě
a	a	k8xC	a
král	král	k1gMnSc1	král
Filip	Filip	k1gMnSc1	Filip
se	se	k3xPyFc4	se
už	už	k6eAd1	už
znovu	znovu	k6eAd1	znovu
neoženil	oženit	k5eNaPmAgMnS	oženit
<g/>
;	;	kIx,	;
zemřel	zemřít	k5eAaPmAgMnS	zemřít
o	o	k7c4	o
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potomci	potomek	k1gMnPc5	potomek
==	==	k?	==
</s>
</p>
<p>
<s>
Z	z	k7c2	z
manželství	manželství	k1gNnSc2	manželství
Markéty	Markéta	k1gFnSc2	Markéta
a	a	k8xC	a
Filipa	Filip	k1gMnSc4	Filip
vzešlo	vzejít	k5eAaPmAgNnS	vzejít
osm	osm	k4xCc1	osm
potomků	potomek	k1gMnPc2	potomek
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Anna	Anna	k1gFnSc1	Anna
Marie	Maria	k1gFnSc2	Maria
(	(	kIx(	(
<g/>
1601	[number]	k4	1601
<g/>
–	–	k?	–
<g/>
1666	[number]	k4	1666
<g/>
)	)	kIx)	)
∞	∞	k?	∞
1615	[number]	k4	1615
francouzský	francouzský	k2eAgMnSc1d1	francouzský
král	král	k1gMnSc1	král
Ludvík	Ludvík	k1gMnSc1	Ludvík
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1601	[number]	k4	1601
<g/>
–	–	k?	–
<g/>
1643	[number]	k4	1643
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Marie	Marie	k1gFnSc1	Marie
(	(	kIx(	(
<g/>
1603	[number]	k4	1603
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Filip	Filip	k1gMnSc1	Filip
IV	IV	kA	IV
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1605	[number]	k4	1605
<g/>
–	–	k?	–
<g/>
1665	[number]	k4	1665
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
španělský	španělský	k2eAgMnSc1d1	španělský
král	král	k1gMnSc1	král
<g/>
∞	∞	k?	∞
1	[number]	k4	1
<g/>
/	/	kIx~	/
1615	[number]	k4	1615
princezna	princezna	k1gFnSc1	princezna
Izabela	Izabela	k1gFnSc1	Izabela
Bourbonská	bourbonský	k2eAgFnSc1d1	Bourbonská
(	(	kIx(	(
<g/>
1602	[number]	k4	1602
<g/>
–	–	k?	–
<g/>
1644	[number]	k4	1644
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
francouzského	francouzský	k2eAgNnSc2d1	francouzské
a	a	k8xC	a
navarrského	navarrský	k2eAgMnSc2d1	navarrský
krále	král	k1gMnSc2	král
Jindřicha	Jindřich	k1gMnSc2	Jindřich
IV	IV	kA	IV
<g/>
.	.	kIx.	.
a	a	k8xC	a
Marie	Marie	k1gFnSc1	Marie
Medicejské	Medicejský	k2eAgFnSc2d1	Medicejská
</s>
</p>
<p>
<s>
∞	∞	k?	∞
2	[number]	k4	2
<g/>
/	/	kIx~	/
1649	[number]	k4	1649
arcivévodkyně	arcivévodkyně	k1gFnSc1	arcivévodkyně
Marie	Marie	k1gFnSc1	Marie
Anna	Anna	k1gFnSc1	Anna
Habsburská	habsburský	k2eAgFnSc1d1	habsburská
(	(	kIx(	(
<g/>
1634	[number]	k4	1634
<g/>
–	–	k?	–
<g/>
1696	[number]	k4	1696
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
císaře	císař	k1gMnSc2	císař
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
III	III	kA	III
<g/>
.	.	kIx.	.
a	a	k8xC	a
Marie	Marie	k1gFnSc1	Marie
Anny	Anna	k1gFnSc2	Anna
ŠpanělskéMarie	ŠpanělskéMarie	k1gFnSc1	ŠpanělskéMarie
Anna	Anna	k1gFnSc1	Anna
(	(	kIx(	(
<g/>
1606	[number]	k4	1606
<g/>
–	–	k?	–
<g/>
1646	[number]	k4	1646
<g/>
)	)	kIx)	)
∞	∞	k?	∞
1631	[number]	k4	1631
císař	císař	k1gMnSc1	císař
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
III	III	kA	III
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1608	[number]	k4	1608
<g/>
–	–	k?	–
<g/>
1657	[number]	k4	1657
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
(	(	kIx(	(
<g/>
1607	[number]	k4	1607
<g/>
–	–	k?	–
<g/>
1632	[number]	k4	1632
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
(	(	kIx(	(
<g/>
1609	[number]	k4	1609
<g/>
–	–	k?	–
<g/>
1641	[number]	k4	1641
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nozozemský	nozozemský	k2eAgMnSc1d1	nozozemský
místodržitel	místodržitel	k1gMnSc1	místodržitel
<g/>
,	,	kIx,	,
kardinál	kardinál	k1gMnSc1	kardinál
</s>
</p>
<p>
<s>
Markéta	Markéta	k1gFnSc1	Markéta
Františka	František	k1gMnSc2	František
(	(	kIx(	(
<g/>
1610	[number]	k4	1610
<g/>
–	–	k?	–
<g/>
1617	[number]	k4	1617
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Alfons	Alfons	k1gMnSc1	Alfons
Mořic	Mořic	k1gMnSc1	Mořic
(	(	kIx(	(
<g/>
1611	[number]	k4	1611
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Vývod	vývod	k1gInSc4	vývod
předků	předek	k1gInPc2	předek
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Filip	Filip	k1gMnSc1	Filip
III	III	kA	III
Habsburg	Habsburg	k1gMnSc1	Habsburg
na	na	k7c6	na
polské	polský	k2eAgFnSc6d1	polská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Filip	Filip	k1gMnSc1	Filip
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Španělský	španělský	k2eAgMnSc1d1	španělský
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
ČORNEJ	ČORNEJ	kA	ČORNEJ
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
;	;	kIx,	;
VANÍČEK	Vaníček	k1gMnSc1	Vaníček
<g/>
,	,	kIx,	,
Vratislav	Vratislav	k1gMnSc1	Vratislav
<g/>
,	,	kIx,	,
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
</s>
<s>
Evropa	Evropa	k1gFnSc1	Evropa
králů	král	k1gMnPc2	král
a	a	k8xC	a
císařů	císař	k1gMnPc2	císař
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ivo	Ivo	k1gMnSc1	Ivo
Železný	Železný	k1gMnSc1	Železný
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
237	[number]	k4	237
<g/>
-	-	kIx~	-
<g/>
3941	[number]	k4	3941
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HAMANNOVÁ	HAMANNOVÁ	kA	HAMANNOVÁ
<g/>
,	,	kIx,	,
Brigitte	Brigitte	k1gFnSc1	Brigitte
<g/>
.	.	kIx.	.
</s>
<s>
Habsburkové	Habsburk	k1gMnPc1	Habsburk
<g/>
.	.	kIx.	.
</s>
<s>
Životopisná	životopisný	k2eAgFnSc1d1	životopisná
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Brána	brána	k1gFnSc1	brána
;	;	kIx,	;
Knižní	knižní	k2eAgInSc1d1	knižní
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
408	[number]	k4	408
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85946	[number]	k4	85946
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
KIENIEWICZ	KIENIEWICZ	kA	KIENIEWICZ
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Historia	Historium	k1gNnSc2	Historium
Półwyspu	Półwysp	k1gInSc2	Półwysp
Iberyjskiego	Iberyjskiego	k6eAd1	Iberyjskiego
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
czasów	czasów	k?	czasów
prehistorycznych	prehistorycznych	k1gInSc1	prehistorycznych
do	do	k7c2	do
nowożytności	nowożytnośce	k1gMnPc1	nowożytnośce
<g/>
.	.	kIx.	.
</s>
<s>
Varšava	Varšava	k1gFnSc1	Varšava
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
n.	n.	k?	n.
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
MERCHÁN	MERCHÁN	kA	MERCHÁN
<g/>
,	,	kIx,	,
C.	C.	kA	C.
El	Ela	k1gFnPc2	Ela
duque	duque	k1gFnSc1	duque
de	de	k?	de
Lerma	Lerma	k?	Lerma
decidió	decidió	k?	decidió
el	ela	k1gFnPc2	ela
traslado	traslada	k1gFnSc5	traslada
de	de	k?	de
la	la	k0	la
Corte	Cort	k1gInSc5	Cort
a	a	k8xC	a
Valladolid	Valladolid	k1gInSc1	Valladolid
w	w	k?	w
"	"	kIx"	"
<g/>
El	Ela	k1gFnPc2	Ela
Norte	Nort	k1gInSc5	Nort
de	de	k?	de
Castilla	Castillo	k1gNnPc4	Castillo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Valladolid	Valladolid	k1gInSc1	Valladolid
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
n.	n.	k?	n.
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
SANZ	SANZ	kA	SANZ
<g/>
,	,	kIx,	,
F.	F.	kA	F.
Martín	Martín	k1gInSc1	Martín
<g/>
.	.	kIx.	.
</s>
<s>
La	la	k1gNnSc4	la
política	política	k6eAd1	política
internacional	internacionat	k5eAaImAgInS	internacionat
de	de	k?	de
Felipe	Felip	k1gInSc5	Felip
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Segovia	Segovia	k1gFnSc1	Segovia
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
n.	n.	k?	n.
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
DEL	DEL	kA	DEL
RÍO	RÍO	kA	RÍO
<g/>
,	,	kIx,	,
Á.	Á.	kA	Á.
Historia	Historium	k1gNnSc2	Historium
literatury	literatura	k1gFnSc2	literatura
hiszpańskiej	hiszpańskiej	k1gInSc1	hiszpańskiej
<g/>
.	.	kIx.	.
</s>
<s>
Varšava	Varšava	k1gFnSc1	Varšava
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
n.	n.	k?	n.
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
1970	[number]	k4	1970
<g/>
–	–	k?	–
<g/>
72	[number]	k4	72
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
TUÑ	TUÑ	k?	TUÑ
DE	DE	k?	DE
LARA	LARA	kA	LARA
<g/>
,	,	kIx,	,
M.	M.	kA	M.
<g/>
;	;	kIx,	;
BARUQUE	BARUQUE	kA	BARUQUE
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Valdeón	Valdeón	k1gInSc1	Valdeón
<g/>
;	;	kIx,	;
ORTIZ	ORTIZ	kA	ORTIZ
<g/>
,	,	kIx,	,
A.	A.	kA	A.
Domínguez	Domínguez	k1gInSc1	Domínguez
<g/>
.	.	kIx.	.
</s>
<s>
Historia	Historium	k1gNnSc2	Historium
Hiszpanii	Hiszpanie	k1gFnSc3	Hiszpanie
<g/>
.	.	kIx.	.
</s>
<s>
Krakov	Krakov	k1gInSc1	Krakov
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
n.	n.	k?	n.
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
VILAR	VILAR	kA	VILAR
<g/>
,	,	kIx,	,
P.	P.	kA	P.
Historia	Historium	k1gNnSc2	Historium
Hiszpanii	Hiszpanie	k1gFnSc3	Hiszpanie
<g/>
.	.	kIx.	.
</s>
<s>
Varšava	Varšava	k1gFnSc1	Varšava
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
n.	n.	k?	n.
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
<g/>
)	)	kIx)	)
</s>
</p>
