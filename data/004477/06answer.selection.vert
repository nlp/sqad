<s>
Film	film	k1gInSc1	film
obdržel	obdržet	k5eAaPmAgInS	obdržet
sedm	sedm	k4xCc4	sedm
nominací	nominace	k1gFnPc2	nominace
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
<g/>
,	,	kIx,	,
proměnil	proměnit	k5eAaPmAgInS	proměnit
ale	ale	k9	ale
jen	jen	k9	jen
jedinou	jediný	k2eAgFnSc4d1	jediná
<g/>
,	,	kIx,	,
Tarantino	Tarantin	k2eAgNnSc4d1	Tarantino
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Rogerem	Roger	k1gInSc7	Roger
Avarym	Avarym	k1gInSc1	Avarym
získali	získat	k5eAaPmAgMnP	získat
Oscara	Oscara	k1gFnSc1	Oscara
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
původní	původní	k2eAgInSc4d1	původní
scénář	scénář	k1gInSc4	scénář
<g/>
.	.	kIx.	.
</s>
