<s>
Rovník	rovník	k1gInSc1	rovník
je	být	k5eAaImIp3nS	být
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
rovnoběžka	rovnoběžka	k1gFnSc1	rovnoběžka
<g/>
,	,	kIx,	,
pomyslná	pomyslný	k2eAgFnSc1d1	pomyslná
čára	čára	k1gFnSc1	čára
spojující	spojující	k2eAgFnSc1d1	spojující
body	bod	k1gInPc7	bod
s	s	k7c7	s
nulovou	nulový	k2eAgFnSc7d1	nulová
zeměpisnou	zeměpisný	k2eAgFnSc7d1	zeměpisná
šířkou	šířka	k1gFnSc7	šířka
<g/>
.	.	kIx.	.
</s>
<s>
Rovník	rovník	k1gInSc1	rovník
je	být	k5eAaImIp3nS	být
průsečnice	průsečnice	k1gFnSc1	průsečnice
zemského	zemský	k2eAgInSc2d1	zemský
povrchu	povrch	k1gInSc2	povrch
s	s	k7c7	s
rovinou	rovina	k1gFnSc7	rovina
<g/>
,	,	kIx,	,
procházející	procházející	k2eAgFnSc1d1	procházející
středem	střed	k1gInSc7	střed
Země	zem	k1gFnSc2	zem
a	a	k8xC	a
kolmou	kolmý	k2eAgFnSc4d1	kolmá
k	k	k7c3	k
zemské	zemský	k2eAgFnSc3d1	zemská
ose	osa	k1gFnSc3	osa
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
pomyslnou	pomyslný	k2eAgFnSc4d1	pomyslná
čáru	čára	k1gFnSc4	čára
vedoucí	vedoucí	k2eAgFnSc4d1	vedoucí
po	po	k7c6	po
povrchu	povrch	k1gInSc6	povrch
kulového	kulový	k2eAgNnSc2d1	kulové
rotujícího	rotující	k2eAgNnSc2d1	rotující
tělesa	těleso	k1gNnSc2	těleso
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
planety	planeta	k1gFnPc1	planeta
<g/>
,	,	kIx,	,
měsíce	měsíc	k1gInPc1	měsíc
nebo	nebo	k8xC	nebo
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
,	,	kIx,	,
přesně	přesně	k6eAd1	přesně
na	na	k7c6	na
půli	půle	k1gFnSc6	půle
cesty	cesta	k1gFnSc2	cesta
mezi	mezi	k7c7	mezi
jeho	jeho	k3xOp3gInPc7	jeho
geografickými	geografický	k2eAgInPc7d1	geografický
póly	pól	k1gInPc7	pól
<g/>
.	.	kIx.	.
</s>
<s>
Rovník	rovník	k1gInSc1	rovník
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
povrch	povrch	k1gInSc4	povrch
planety	planeta	k1gFnSc2	planeta
nebo	nebo	k8xC	nebo
jiného	jiný	k2eAgNnSc2d1	jiné
nebeského	nebeský	k2eAgNnSc2d1	nebeské
tělesa	těleso	k1gNnSc2	těleso
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
polokoule	polokoule	k1gFnPc4	polokoule
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
zeměkoule	zeměkoule	k1gFnSc2	zeměkoule
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c6	o
severní	severní	k2eAgFnSc6d1	severní
a	a	k8xC	a
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
zemského	zemský	k2eAgInSc2d1	zemský
rovníku	rovník	k1gInSc2	rovník
činí	činit	k5eAaImIp3nS	činit
asi	asi	k9	asi
40	[number]	k4	40
075	[number]	k4	075
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Rovník	rovník	k1gInSc1	rovník
protíná	protínat	k5eAaImIp3nS	protínat
povrch	povrch	k1gInSc4	povrch
nebo	nebo	k8xC	nebo
teritoriální	teritoriální	k2eAgFnSc2d1	teritoriální
vody	voda	k1gFnSc2	voda
14	[number]	k4	14
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
začneme	začít	k5eAaPmIp1nP	začít
na	na	k7c6	na
západě	západ	k1gInSc6	západ
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
budeme	být	k5eAaImBp1nP	být
pokračovat	pokračovat	k5eAaImF	pokračovat
východně	východně	k6eAd1	východně
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
<g/>
:	:	kIx,	:
Svatý	svatý	k2eAgMnSc1d1	svatý
Tomáš	Tomáš	k1gMnSc1	Tomáš
a	a	k8xC	a
Princův	princův	k2eAgInSc1d1	princův
ostrov	ostrov	k1gInSc1	ostrov
Svatý	svatý	k2eAgMnSc1d1	svatý
Tomáš	Tomáš	k1gMnSc1	Tomáš
a	a	k8xC	a
Princův	princův	k2eAgInSc1d1	princův
ostrov	ostrov	k1gInSc1	ostrov
(	(	kIx(	(
<g/>
ostrov	ostrov	k1gInSc1	ostrov
Sã	Sã	k1gFnSc2	Sã
Tomé	Tomá	k1gFnSc2	Tomá
<g/>
)	)	kIx)	)
Gabon	Gabon	k1gMnSc1	Gabon
Gabon	Gabon	k1gMnSc1	Gabon
Kongo	Kongo	k1gNnSc4	Kongo
Kongo	Kongo	k1gNnSc4	Kongo
DR	dr	kA	dr
Kongo	Kongo	k1gNnSc4	Kongo
DR	dr	kA	dr
Kongo	Kongo	k1gNnSc1	Kongo
Uganda	Uganda	k1gFnSc1	Uganda
Uganda	Uganda	k1gFnSc1	Uganda
-	-	kIx~	-
prochází	procházet	k5eAaImIp3nS	procházet
též	též	k9	též
některými	některý	k3yIgInPc7	některý
ostrovy	ostrov	k1gInPc7	ostrov
ve	v	k7c6	v
Viktoriině	Viktoriin	k2eAgNnSc6d1	Viktoriino
jezeře	jezero	k1gNnSc6	jezero
<g />
.	.	kIx.	.
</s>
<s>
Keňa	Keňa	k1gFnSc1	Keňa
Keňa	Keňa	k1gFnSc1	Keňa
Somálsko	Somálsko	k1gNnSc1	Somálsko
Somálsko	Somálsko	k1gNnSc1	Somálsko
Maledivy	Maledivy	k1gFnPc1	Maledivy
Maledivy	Maledivy	k1gFnPc1	Maledivy
-	-	kIx~	-
všechny	všechen	k3xTgInPc4	všechen
ostrovy	ostrov	k1gInPc4	ostrov
míjí	míjet	k5eAaImIp3nS	míjet
<g/>
,	,	kIx,	,
prochází	procházet	k5eAaImIp3nS	procházet
mezi	mezi	k7c4	mezi
atoly	atol	k1gInPc4	atol
Huvadu	Huvad	k1gInSc2	Huvad
a	a	k8xC	a
Addu	Addus	k1gInSc2	Addus
Indonésie	Indonésie	k1gFnSc1	Indonésie
Indonésie	Indonésie	k1gFnSc1	Indonésie
-	-	kIx~	-
protíná	protínat	k5eAaImIp3nS	protínat
více	hodně	k6eAd2	hodně
ostrovů	ostrov	k1gInPc2	ostrov
včetně	včetně	k7c2	včetně
těch	ten	k3xDgMnPc2	ten
největších	veliký	k2eAgMnPc2d3	veliký
(	(	kIx(	(
<g/>
Sumatra	Sumatra	k1gFnSc1	Sumatra
<g/>
,	,	kIx,	,
Borneo	Borneo	k1gNnSc1	Borneo
<g/>
,	,	kIx,	,
Celebes	Celebes	k1gInSc1	Celebes
<g/>
)	)	kIx)	)
Kiribati	Kiribati	k1gFnSc1	Kiribati
Kiribati	Kiribati	k1gFnPc2	Kiribati
-	-	kIx~	-
všechny	všechen	k3xTgInPc4	všechen
ostrovy	ostrov	k1gInPc4	ostrov
míjí	míjet	k5eAaImIp3nS	míjet
<g/>
,	,	kIx,	,
prochází	procházet	k5eAaImIp3nS	procházet
mezi	mezi	k7c4	mezi
atoly	atol	k1gInPc4	atol
Aranuka	Aranuk	k1gMnSc2	Aranuk
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
Nonouti	Nonout	k1gMnPc1	Nonout
v	v	k7c6	v
Gilbertových	Gilbertův	k2eAgInPc6d1	Gilbertův
ostrovech	ostrov	k1gInPc6	ostrov
USA	USA	kA	USA
USA	USA	kA	USA
(	(	kIx(	(
<g/>
Bakerův	Bakerův	k2eAgInSc4d1	Bakerův
ostrov	ostrov	k1gInSc4	ostrov
a	a	k8xC	a
Jarvisův	Jarvisův	k2eAgInSc4d1	Jarvisův
ostrov	ostrov	k1gInSc4	ostrov
<g/>
)	)	kIx)	)
-	-	kIx~	-
ostrovy	ostrov	k1gInPc1	ostrov
samotné	samotný	k2eAgInPc1d1	samotný
ale	ale	k8xC	ale
neprotíná	protínat	k5eNaImIp3nS	protínat
Ekvádor	Ekvádor	k1gInSc1	Ekvádor
Ekvádor	Ekvádor	k1gInSc1	Ekvádor
-	-	kIx~	-
prochází	procházet	k5eAaImIp3nS	procházet
již	již	k9	již
souostrovím	souostroví	k1gNnSc7	souostroví
Galapágy	Galapágy	k1gFnPc4	Galapágy
<g/>
,	,	kIx,	,
ležícím	ležící	k2eAgMnSc7d1	ležící
v	v	k7c6	v
Tichém	tichý	k2eAgInSc6d1	tichý
oceánu	oceán	k1gInSc6	oceán
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
teprve	teprve	k6eAd1	teprve
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
na	na	k7c4	na
pevninu	pevnina	k1gFnSc4	pevnina
(	(	kIx(	(
<g/>
překlad	překlad	k1gInSc1	překlad
názvu	název	k1gInSc2	název
státu	stát	k1gInSc2	stát
Ekvádor	Ekvádor	k1gInSc1	Ekvádor
znamená	znamenat	k5eAaImIp3nS	znamenat
právě	právě	k9	právě
"	"	kIx"	"
<g/>
rovník	rovník	k1gInSc1	rovník
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
Brazílie	Brazílie	k1gFnSc1	Brazílie
Brazílie	Brazílie	k1gFnSc1	Brazílie
-	-	kIx~	-
včetně	včetně	k7c2	včetně
ostrovů	ostrov	k1gInPc2	ostrov
v	v	k7c6	v
širokém	široký	k2eAgNnSc6d1	široké
ústí	ústí	k1gNnSc6	ústí
řeky	řeka	k1gFnSc2	řeka
Amazonky	Amazonka	k1gFnSc2	Amazonka
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
žádnou	žádný	k3yNgFnSc4	žádný
část	část	k1gFnSc4	část
Rovníkové	rovníkový	k2eAgFnSc2d1	Rovníková
Guiney	Guinea	k1gFnSc2	Guinea
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
rovník	rovník	k1gInSc4	rovník
neprotíná	protínat	k5eNaImIp3nS	protínat
<g/>
.	.	kIx.	.
</s>
<s>
Rovník	rovník	k1gInSc1	rovník
protíná	protínat	k5eAaImIp3nS	protínat
také	také	k9	také
tři	tři	k4xCgInPc4	tři
velké	velký	k2eAgInPc4d1	velký
světové	světový	k2eAgInPc4d1	světový
oceány	oceán	k1gInPc4	oceán
<g/>
:	:	kIx,	:
Indický	indický	k2eAgInSc1d1	indický
oceán	oceán	k1gInSc1	oceán
Tichý	tichý	k2eAgInSc1d1	tichý
oceán	oceán	k1gInSc1	oceán
Atlantický	atlantický	k2eAgInSc1d1	atlantický
oceán	oceán	k1gInSc1	oceán
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
vexilologie	vexilologie	k1gFnSc2	vexilologie
(	(	kIx(	(
<g/>
pomocné	pomocný	k2eAgFnSc6d1	pomocná
historické	historický	k2eAgFnSc6d1	historická
vědě	věda	k1gFnSc6	věda
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
byl	být	k5eAaImAgInS	být
<g/>
)	)	kIx)	)
symbol	symbol	k1gInSc1	symbol
rovníku	rovník	k1gInSc2	rovník
umístěn	umístit	k5eAaPmNgInS	umístit
např.	např.	kA	např.
na	na	k7c6	na
různých	různý	k2eAgFnPc6d1	různá
vlajkách	vlajka	k1gFnPc6	vlajka
<g/>
:	:	kIx,	:
Brazilská	brazilský	k2eAgFnSc1d1	brazilská
vlajka	vlajka	k1gFnSc1	vlajka
Gabonská	Gabonský	k2eAgFnSc1d1	Gabonská
vlajka	vlajka	k1gFnSc1	vlajka
Nauruská	Nauruský	k2eAgFnSc1d1	Nauruská
vlajka	vlajka	k1gFnSc1	vlajka
Rovník	rovník	k1gInSc1	rovník
je	být	k5eAaImIp3nS	být
také	také	k9	také
obsažen	obsáhnout	k5eAaPmNgInS	obsáhnout
i	i	k9	i
v	v	k7c6	v
názvu	název	k1gInSc6	název
jihoamerického	jihoamerický	k2eAgInSc2d1	jihoamerický
státu	stát	k1gInSc2	stát
Ekvádor	Ekvádor	k1gInSc1	Ekvádor
(	(	kIx(	(
<g/>
španělské	španělský	k2eAgNnSc1d1	španělské
slovo	slovo	k1gNnSc1	slovo
Ecuador	Ecuador	k1gInSc1	Ecuador
znamená	znamenat	k5eAaImIp3nS	znamenat
rovník	rovník	k1gInSc1	rovník
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
v	v	k7c6	v
názvu	název	k1gInSc6	název
afrického	africký	k2eAgInSc2d1	africký
státu	stát	k1gInSc2	stát
Rovníková	rovníkový	k2eAgFnSc1d1	Rovníková
Guinea	Guinea	k1gFnSc1	Guinea
(	(	kIx(	(
<g/>
i	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
rovník	rovník	k1gInSc1	rovník
neprotíná	protínat	k5eNaImIp3nS	protínat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Geografický	geografický	k2eAgInSc1d1	geografický
místopisný	místopisný	k2eAgInSc1d1	místopisný
slovník	slovník	k1gInSc1	slovník
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
Praha	Praha	k1gFnSc1	Praha
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
Ilustrovaný	ilustrovaný	k2eAgInSc4d1	ilustrovaný
atlas	atlas	k1gInSc4	atlas
světa	svět	k1gInSc2	svět
poledník	poledník	k1gInSc1	poledník
rovnoběžka	rovnoběžka	k1gFnSc1	rovnoběžka
sever	sever	k1gInSc4	sever
jih	jih	k1gInSc4	jih
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
zemský	zemský	k2eAgInSc4d1	zemský
rovník	rovník	k1gInSc4	rovník
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
rovník	rovník	k1gInSc1	rovník
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
www.ian.cz	www.ian.cza	k1gFnPc2	www.ian.cza
<g/>
,	,	kIx,	,
inAstroPoradna	inAstroPoradna	k1gFnSc1	inAstroPoradna
<g/>
:	:	kIx,	:
pravý	pravý	k2eAgInSc1d1	pravý
a	a	k8xC	a
střední	střední	k2eAgInSc1d1	střední
rovník	rovník	k1gInSc1	rovník
</s>
