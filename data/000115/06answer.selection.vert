<s>
Rovník	rovník	k1gInSc1	rovník
je	být	k5eAaImIp3nS	být
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
rovnoběžka	rovnoběžka	k1gFnSc1	rovnoběžka
<g/>
,	,	kIx,	,
pomyslná	pomyslný	k2eAgFnSc1d1	pomyslná
čára	čára	k1gFnSc1	čára
spojující	spojující	k2eAgFnSc1d1	spojující
body	bod	k1gInPc7	bod
s	s	k7c7	s
nulovou	nulový	k2eAgFnSc7d1	nulová
zeměpisnou	zeměpisný	k2eAgFnSc7d1	zeměpisná
šířkou	šířka	k1gFnSc7	šířka
<g/>
.	.	kIx.	.
</s>
