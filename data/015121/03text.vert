<s>
Památník	památník	k1gInSc1
homosexuálním	homosexuální	k2eAgFnPc3d1
obětem	oběť	k1gFnPc3
nacismu	nacismus	k1gInSc2
</s>
<s>
Památník	památník	k1gInSc1
homosexuálním	homosexuální	k2eAgFnPc3d1
obětem	oběť	k1gFnPc3
nacismu	nacismus	k1gInSc2
Výstavba	výstavba	k1gFnSc1
</s>
<s>
2008	#num#	k4
Poloha	poloha	k1gFnSc1
Adresa	adresa	k1gFnSc1
</s>
<s>
Berlín	Berlín	k1gInSc1
<g/>
,	,	kIx,
Mitte	Mitt	k1gMnSc5
<g/>
,	,	kIx,
Německo	Německo	k1gNnSc1
Německo	Německo	k1gNnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
52	#num#	k4
<g/>
°	°	k?
<g/>
30	#num#	k4
<g/>
′	′	k?
<g/>
47,88	47,88	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
13	#num#	k4
<g/>
°	°	k?
<g/>
22	#num#	k4
<g/>
′	′	k?
<g/>
33,89	33,89	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Památník	památník	k1gInSc1
homosexuálním	homosexuální	k2eAgFnPc3d1
obětem	oběť	k1gFnPc3
nacismu	nacismus	k1gInSc2
byl	být	k5eAaImAgInS
v	v	k7c6
Berlíně	Berlín	k1gInSc6
slavnostně	slavnostně	k6eAd1
odhalen	odhalit	k5eAaPmNgInS
27	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Upomíná	upomínat	k5eAaImIp3nS
na	na	k7c6
perzekuci	perzekuce	k1gFnSc6
asi	asi	k9
100	#num#	k4
000	#num#	k4
obviněných	obviněný	k1gMnPc2
(	(	kIx(
<g/>
z	z	k7c2
toho	ten	k3xDgNnSc2
50	#num#	k4
000	#num#	k4
odsouzených	odsouzený	k2eAgMnPc2d1
a	a	k8xC
z	z	k7c2
nich	on	k3xPp3gInPc2
asi	asi	k9
5	#num#	k4
<g/>
–	–	k?
<g/>
15	#num#	k4
tisíc	tisíc	k4xCgInPc2
v	v	k7c6
koncentračních	koncentrační	k2eAgInPc6d1
táborech	tábor	k1gInPc6
uvězněných	uvězněný	k2eAgInPc2d1
<g/>
)	)	kIx)
a	a	k8xC
nevyčíslený	vyčíslený	k2eNgInSc1d1
počet	počet	k1gInSc1
dalších	další	k2eAgFnPc2d1
diskriminovaných	diskriminovaný	k2eAgFnPc2d1
homosexuálních	homosexuální	k2eAgFnPc2d1
osob	osoba	k1gFnPc2
v	v	k7c6
nacistickém	nacistický	k2eAgNnSc6d1
Německu	Německo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Památník	památník	k1gInSc1
má	mít	k5eAaImIp3nS
podobu	podoba	k1gFnSc4
betonového	betonový	k2eAgInSc2d1
kvádru	kvádr	k1gInSc2
<g/>
,	,	kIx,
na	na	k7c6
jehož	jehož	k3xOyRp3gFnSc6
přední	přední	k2eAgFnSc6d1
straně	strana	k1gFnSc6
je	být	k5eAaImIp3nS
okénko	okénko	k1gNnSc1
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
mohou	moct	k5eAaImIp3nP
návštěvníci	návštěvník	k1gMnPc1
sledovat	sledovat	k5eAaImF
krátký	krátký	k2eAgInSc4d1
film	film	k1gInSc4
se	s	k7c7
dvěma	dva	k4xCgInPc7
líbajícími	líbající	k2eAgInPc7d1
se	se	k3xPyFc4
muži	muž	k1gMnSc6
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
památníku	památník	k1gInSc2
</s>
<s>
Homosexuální	homosexuální	k2eAgFnPc1d1
oběti	oběť	k1gFnPc1
holokaustu	holokaust	k1gInSc2
nebyly	být	k5eNaImAgFnP
v	v	k7c6
Západním	západní	k2eAgNnSc6d1
Německu	Německo	k1gNnSc6
po	po	k7c6
druhé	druhý	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
uznávány	uznávat	k5eAaImNgFnP
<g/>
,	,	kIx,
protože	protože	k8xS
homosexualita	homosexualita	k1gFnSc1
byla	být	k5eAaImAgFnS
v	v	k7c6
Německé	německý	k2eAgFnSc6d1
spolkové	spolkový	k2eAgFnSc6d1
republice	republika	k1gFnSc6
zcela	zcela	k6eAd1
legalizována	legalizovat	k5eAaBmNgFnS
až	až	k9
v	v	k7c6
roce	rok	k1gInSc6
1973	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2003	#num#	k4
Spolkový	spolkový	k2eAgInSc1d1
sněm	sněm	k1gInSc1
schválil	schválit	k5eAaPmAgInS
vybudování	vybudování	k1gNnSc4
památníku	památník	k1gInSc2
v	v	k7c6
blízkosti	blízkost	k1gFnSc6
tehdy	tehdy	k6eAd1
chystaného	chystaný	k2eAgInSc2d1
památníku	památník	k1gInSc2
Židů	Žid	k1gMnPc2
zavražděných	zavražděný	k2eAgMnPc2d1
v	v	k7c6
Evropě	Evropa	k1gFnSc6
a	a	k8xC
byla	být	k5eAaImAgFnS
vyhlášena	vyhlásit	k5eAaPmNgFnS
umělecká	umělecký	k2eAgFnSc1d1
soutěž	soutěž	k1gFnSc1
na	na	k7c4
jeho	jeho	k3xOp3gNnSc4
zpracování	zpracování	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Slavnostního	slavnostní	k2eAgNnSc2d1
odhalení	odhalení	k1gNnSc2
památníku	památník	k1gInSc2
se	se	k3xPyFc4
zúčastnila	zúčastnit	k5eAaPmAgFnS
řada	řada	k1gFnSc1
německých	německý	k2eAgFnPc2d1
osobností	osobnost	k1gFnPc2
a	a	k8xC
politiků	politik	k1gMnPc2
<g/>
,	,	kIx,
krom	krom	k7c2
jiných	jiná	k1gFnPc2
berlínský	berlínský	k2eAgMnSc1d1
starosta	starosta	k1gMnSc1
Klaus	Klaus	k1gMnSc1
Wowereit	Wowereit	k1gMnSc1
<g/>
,	,	kIx,
předseda	předseda	k1gMnSc1
spolkového	spolkový	k2eAgInSc2d1
sněmu	sněm	k1gInSc2
Wolfgang	Wolfgang	k1gMnSc1
Thierse	Thierse	k1gFnSc2
a	a	k8xC
německý	německý	k2eAgMnSc1d1
ministr	ministr	k1gMnSc1
kultury	kultura	k1gFnSc2
Bernd	Bernd	k1gMnSc1
Neumann	Neumann	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
samotném	samotný	k2eAgInSc6d1
roce	rok	k1gInSc6
2008	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
památník	památník	k1gInSc1
terčem	terč	k1gInSc7
řady	řada	k1gFnSc2
vandalských	vandalský	k2eAgInPc2d1
útoků	útok	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Holokaust	holokaust	k1gInSc1
</s>
<s>
Růžový	růžový	k2eAgInSc1d1
trojúhelník	trojúhelník	k1gInSc1
</s>
<s>
Homomonument	Homomonument	k1gMnSc1
</s>
<s>
Památník	památník	k1gInSc1
Židů	Žid	k1gMnPc2
zavražděných	zavražděný	k2eAgMnPc2d1
v	v	k7c6
Evropě	Evropa	k1gFnSc6
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Památník	památník	k1gInSc1
homosexuálním	homosexuální	k2eAgFnPc3d1
obětem	oběť	k1gFnPc3
nacismu	nacismus	k1gInSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Architektura	architektura	k1gFnSc1
a	a	k8xC
stavebnictví	stavebnictví	k1gNnSc1
|	|	kIx~
Kultura	kultura	k1gFnSc1
|	|	kIx~
Německo	Německo	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
VIAF	VIAF	kA
<g/>
:	:	kIx,
246354042	#num#	k4
</s>
