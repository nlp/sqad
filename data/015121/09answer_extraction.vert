Památník	památník	k1gInSc1
má	mít	k5eAaImIp3nS
podobu	podoba	k1gFnSc4
betonového	betonový	k2eAgInSc2d1
kvádru	kvádr	k1gInSc2
<g/>
,	,	kIx,
na	na	k7c6
jehož	jehož	k3xOyRp3gFnSc6
přední	přední	k2eAgFnSc6d1
straně	strana	k1gFnSc6
je	být	k5eAaImIp3nS
okénko	okénko	k1gNnSc1
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
mohou	moct	k5eAaImIp3nP
návštěvníci	návštěvník	k1gMnPc1
sledovat	sledovat	k5eAaImF
krátký	krátký	k2eAgInSc4d1
film	film	k1gInSc4
se	s	k7c7
dvěma	dva	k4xCgInPc7
líbajícími	líbající	k2eAgInPc7d1
se	se	k3xPyFc4
muži	muž	k1gMnSc6
<g/>
.	.	kIx.
