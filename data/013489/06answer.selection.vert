<s>
Světová	světový	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
pro	pro	k7c4
zdraví	zdraví	k1gNnSc4
zvířat	zvíře	k1gNnPc2
(	(	kIx(
<g/>
OIE	OIE	kA
<g/>
,	,	kIx,
anglicky	anglicky	k6eAd1
The	The	kA
World	World	k1gMnSc1
Organisation	Organisation	k1gInSc4
for	forum	k7
Animal	animal	k1gMnSc1
Health	Health	k1gMnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
mezinárodní	mezinárodní	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
zabývající	zabývající	k2eAgFnSc2d1
se	se	k3xPyFc4
zdravím	zdraví	k1gNnSc7
zvířat	zvíře	k1gNnPc2
<g/>
.	.	kIx.
</s>