<s>
Světová	světový	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
pro	pro	k7c4
zdraví	zdraví	k1gNnSc4
zvířat	zvíře	k1gNnPc2
</s>
<s>
Světová	světový	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
pro	pro	k7c4
zdraví	zdraví	k1gNnSc4
zvířat	zvíře	k1gNnPc2
Vznik	vznik	k1gInSc1
</s>
<s>
1924	#num#	k4
Sídlo	sídlo	k1gNnSc1
</s>
<s>
rue	rue	k?
de	de	k?
Prony	Prona	k1gFnSc2
12	#num#	k4
<g/>
,	,	kIx,
17	#num#	k4
<g/>
.	.	kIx.
obvod	obvod	k1gInSc1
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
48	#num#	k4
<g/>
°	°	k?
<g/>
52	#num#	k4
<g/>
′	′	k?
<g/>
52,68	52,68	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
2	#num#	k4
<g/>
°	°	k?
<g/>
18	#num#	k4
<g/>
′	′	k?
<g/>
24,16	24,16	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Úřední	úřední	k2eAgInSc4d1
jazyk	jazyk	k1gInSc4
</s>
<s>
angličtina	angličtina	k1gFnSc1
<g/>
,	,	kIx,
španělština	španělština	k1gFnSc1
a	a	k8xC
francouzština	francouzština	k1gFnSc1
Oficiální	oficiální	k2eAgFnSc1d1
web	web	k1gInSc4
</s>
<s>
www.oie.int	www.oie.int	k1gMnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
můžou	můžou	k?
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Světová	světový	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
pro	pro	k7c4
zdraví	zdraví	k1gNnSc4
zvířat	zvíře	k1gNnPc2
(	(	kIx(
<g/>
OIE	OIE	kA
<g/>
,	,	kIx,
anglicky	anglicky	k6eAd1
The	The	kA
World	World	k1gMnSc1
Organisation	Organisation	k1gInSc4
for	forum	k7
Animal	animal	k1gMnSc1
Health	Health	k1gMnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
mezinárodní	mezinárodní	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
zabývající	zabývající	k2eAgFnSc2d1
se	se	k3xPyFc4
zdravím	zdraví	k1gNnSc7
zvířat	zvíře	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1924	#num#	k4
původně	původně	k6eAd1
pod	pod	k7c7
názvem	název	k1gInSc7
Office	Office	kA
International	International	k1gFnSc7
des	des	k1gNnSc2
Epizooties	Epizooties	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přestože	přestože	k8xS
byla	být	k5eAaImAgFnS
v	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
přejmenována	přejmenovat	k5eAaPmNgFnS
na	na	k7c4
The	The	k1gFnPc4
World	World	k1gInSc4
Organisation	Organisation	k1gInSc4
for	forum	k1gNnPc2
Animal	animal	k1gMnSc1
Health	Health	k1gMnSc1
<g/>
,	,	kIx,
organizace	organizace	k1gFnSc1
si	se	k3xPyFc3
ponechala	ponechat	k5eAaPmAgFnS
zkratku	zkratka	k1gFnSc4
(	(	kIx(
<g/>
OIE	OIE	kA
<g/>
)	)	kIx)
z	z	k7c2
původního	původní	k2eAgInSc2d1
názvu	název	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Členem	člen	k1gMnSc7
OIE	OIE	kA
v	v	k7c6
roce	rok	k1gInSc6
2019	#num#	k4
bylo	být	k5eAaImAgNnS
182	#num#	k4
členských	členský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
OIE	OIE	kA
představuje	představovat	k5eAaImIp3nS
jakousi	jakýsi	k3yIgFnSc4
obdobu	obdoba	k1gFnSc4
Světové	světový	k2eAgFnSc2d1
zdravotnické	zdravotnický	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
(	(	kIx(
<g/>
WHO	WHO	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cílem	cíl	k1gInSc7
organizace	organizace	k1gFnSc2
je	být	k5eAaImIp3nS
ochrana	ochrana	k1gFnSc1
a	a	k8xC
tvorba	tvorba	k1gFnSc1
zdraví	zdraví	k1gNnSc2
zvířat	zvíře	k1gNnPc2
a	a	k8xC
to	ten	k3xDgNnSc1
především	především	k9
v	v	k7c6
oblasti	oblast	k1gFnSc6
infekčních	infekční	k2eAgFnPc2d1
nemocí	nemoc	k1gFnPc2
zvířat	zvíře	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
The	The	k1gFnSc1
182	#num#	k4
OIE	OIE	kA
Members	Membersa	k1gFnPc2
<g/>
,	,	kIx,
oie	oie	k?
<g/>
.	.	kIx.
<g/>
int	int	k?
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Světová	světový	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
pro	pro	k7c4
zdraví	zdraví	k1gNnSc4
zvířat	zvíře	k1gNnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
uzp	uzp	k?
<g/>
2007391468	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
2035386-8	2035386-8	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2185	#num#	k4
0408	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
79104279	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
126127087	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
79104279	#num#	k4
</s>
