<p>
<s>
Červený	červený	k2eAgInSc1d1	červený
pokoj	pokoj	k1gInSc1	pokoj
(	(	kIx(	(
<g/>
Röda	Röda	k1gMnSc1	Röda
rummet	rummet	k1gMnSc1	rummet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přelomové	přelomový	k2eAgNnSc1d1	přelomové
dílo	dílo	k1gNnSc1	dílo
švédské	švédský	k2eAgFnSc2d1	švédská
literatury	literatura	k1gFnSc2	literatura
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
román	román	k1gInSc1	román
Augusta	August	k1gMnSc2	August
Strindberga	Strindberg	k1gMnSc2	Strindberg
vydaný	vydaný	k2eAgInSc4d1	vydaný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1879	[number]	k4	1879
nakladatelstvím	nakladatelství	k1gNnSc7	nakladatelství
Josepha	Joseph	k1gMnSc2	Joseph
Selingmanna	Selingmann	k1gMnSc2	Selingmann
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
autorovu	autorův	k2eAgFnSc4d1	autorova
prvotinu	prvotina	k1gFnSc4	prvotina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
kritického	kritický	k2eAgInSc2d1	kritický
realismu	realismus	k1gInSc2	realismus
a	a	k8xC	a
naturalismu	naturalismus	k1gInSc2	naturalismus
nastavuje	nastavovat	k5eAaImIp3nS	nastavovat
zrcadlo	zrcadlo	k1gNnSc1	zrcadlo
soudobé	soudobý	k2eAgFnSc2d1	soudobá
stockholmské	stockholmský	k2eAgFnSc2d1	Stockholmská
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
deníku	deník	k1gInSc6	deník
Aftonbladet	Aftonbladeta	k1gFnPc2	Aftonbladeta
bylo	být	k5eAaImAgNnS	být
dílo	dílo	k1gNnSc1	dílo
nazváno	nazvat	k5eAaBmNgNnS	nazvat
románem	román	k1gInSc7	román
"	"	kIx"	"
<g/>
špíny	špína	k1gFnSc2	špína
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Čtenářsky	čtenářsky	k6eAd1	čtenářsky
byl	být	k5eAaImAgInS	být
román	román	k1gInSc1	román
nicméně	nicméně	k8xC	nicméně
velice	velice	k6eAd1	velice
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
–	–	k?	–
v	v	k7c6	v
letech	let	k1gInPc6	let
1879	[number]	k4	1879
<g/>
–	–	k?	–
<g/>
1880	[number]	k4	1880
vyšel	vyjít	k5eAaPmAgInS	vyjít
v	v	k7c6	v
pěti	pět	k4xCc6	pět
vydáních	vydání	k1gNnPc6	vydání
v	v	k7c6	v
celkovém	celkový	k2eAgInSc6d1	celkový
nákladu	náklad	k1gInSc6	náklad
6000	[number]	k4	6000
výtisků	výtisk	k1gInPc2	výtisk
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
přeložen	přeložit	k5eAaPmNgInS	přeložit
do	do	k7c2	do
několika	několik	k4yIc2	několik
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Česky	česky	k6eAd1	česky
jej	on	k3xPp3gMnSc4	on
vydalo	vydat	k5eAaPmAgNnS	vydat
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Odeon	odeon	k1gInSc1	odeon
(	(	kIx(	(
<g/>
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
Františka	František	k1gMnSc2	František
Fröhlicha	Fröhlich	k1gMnSc2	Fröhlich
<g/>
)	)	kIx)	)
celkově	celkově	k6eAd1	celkově
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1901	[number]	k4	1901
<g/>
,	,	kIx,	,
1927	[number]	k4	1927
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1932	[number]	k4	1932
a	a	k8xC	a
1952	[number]	k4	1952
a	a	k8xC	a
naposledy	naposledy	k6eAd1	naposledy
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Červený	červený	k2eAgInSc1d1	červený
pokoj	pokoj	k1gInSc1	pokoj
je	být	k5eAaImIp3nS	být
typicky	typicky	k6eAd1	typicky
naturalisticky	naturalisticky	k6eAd1	naturalisticky
obrácený	obrácený	k2eAgMnSc1d1	obrácený
bildungsroman	bildungsroman	k1gMnSc1	bildungsroman
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
široký	široký	k2eAgInSc4d1	široký
záběr	záběr	k1gInSc4	záběr
<g/>
,	,	kIx,	,
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
ukázat	ukázat	k5eAaPmF	ukázat
společnost	společnost	k1gFnSc4	společnost
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
úhlů	úhel	k1gInPc2	úhel
a	a	k8xC	a
za	za	k7c7	za
tímto	tento	k3xDgInSc7	tento
účelem	účel	k1gInSc7	účel
je	být	k5eAaImIp3nS	být
vlastně	vlastně	k9	vlastně
také	také	k9	také
jakousi	jakýsi	k3yIgFnSc7	jakýsi
moderní	moderní	k2eAgFnSc7d1	moderní
variací	variace	k1gFnSc7	variace
pikareskního	pikareskní	k2eAgInSc2d1	pikareskní
románu	román	k1gInSc2	román
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
kompozice	kompozice	k1gFnSc1	kompozice
rozvolněná	rozvolněný	k2eAgFnSc1d1	rozvolněná
a	a	k8xC	a
útržkovitá	útržkovitý	k2eAgFnSc1d1	útržkovitá
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
v	v	k7c6	v
autorově	autorův	k2eAgFnSc6d1	autorova
rezignaci	rezignace	k1gFnSc6	rezignace
na	na	k7c4	na
organizovanější	organizovaný	k2eAgFnSc4d2	organizovanější
formu	forma	k1gFnSc4	forma
odráželo	odrážet	k5eAaImAgNnS	odrážet
jeho	jeho	k3xOp3gNnSc4	jeho
zolovsky	zolovsky	k6eAd1	zolovsky
naturalistické	naturalistický	k2eAgNnSc4d1	naturalistické
přesvědčení	přesvědčení	k1gNnSc4	přesvědčení
<g/>
.	.	kIx.	.
</s>
<s>
Darwinistické	darwinistický	k2eAgNnSc1d1	darwinistické
líčení	líčení	k1gNnSc1	líčení
lidských	lidský	k2eAgFnPc2d1	lidská
bytostí	bytost	k1gFnPc2	bytost
jako	jako	k8xS	jako
společenských	společenský	k2eAgMnPc2d1	společenský
živočichů	živočich	k1gMnPc2	živočich
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
textu	text	k1gInSc6	text
umocněno	umocněn	k2eAgNnSc1d1	umocněno
pohledem	pohled	k1gInSc7	pohled
na	na	k7c4	na
lidi	člověk	k1gMnPc4	člověk
jako	jako	k8xS	jako
na	na	k7c4	na
odpadky	odpadek	k1gInPc4	odpadek
pomocí	pomocí	k7c2	pomocí
podobných	podobný	k2eAgNnPc2d1	podobné
degradujících	degradující	k2eAgNnPc2d1	degradující
přirovnání	přirovnání	k1gNnPc2	přirovnání
<g/>
.	.	kIx.	.
</s>
<s>
Ironie	ironie	k1gFnSc1	ironie
a	a	k8xC	a
humor	humor	k1gInSc1	humor
<g/>
,	,	kIx,	,
blížící	blížící	k2eAgFnPc1d1	blížící
se	se	k3xPyFc4	se
až	až	k9	až
karikatuře	karikatura	k1gFnSc3	karikatura
<g/>
,	,	kIx,	,
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
břitkost	břitkost	k1gFnSc1	břitkost
textu	text	k1gInSc2	text
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dnešního	dnešní	k2eAgInSc2d1	dnešní
pohledu	pohled	k1gInSc2	pohled
se	se	k3xPyFc4	se
román	román	k1gInSc1	román
nejeví	jevit	k5eNaImIp3nS	jevit
jako	jako	k9	jako
velké	velký	k2eAgNnSc1d1	velké
nadčasové	nadčasový	k2eAgNnSc1d1	nadčasové
dílo	dílo	k1gNnSc1	dílo
<g/>
,	,	kIx,	,
proslulost	proslulost	k1gFnSc1	proslulost
textu	text	k1gInSc2	text
je	být	k5eAaImIp3nS	být
přece	přece	k9	přece
jen	jen	k9	jen
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
dána	dán	k2eAgFnSc1d1	dána
jeho	jeho	k3xOp3gFnSc7	jeho
ohromující	ohromující	k2eAgFnSc7d1	ohromující
výjimečností	výjimečnost	k1gFnSc7	výjimečnost
v	v	k7c6	v
místním	místní	k2eAgInSc6d1	místní
a	a	k8xC	a
dobovém	dobový	k2eAgInSc6d1	dobový
kontextu	kontext	k1gInSc6	kontext
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
román	román	k1gInSc1	román
dodnes	dodnes	k6eAd1	dodnes
čtivý	čtivý	k2eAgInSc1d1	čtivý
díky	díky	k7c3	díky
svěžímu	svěží	k2eAgNnSc3d1	svěží
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
impresionistickému	impresionistický	k2eAgInSc3d1	impresionistický
stylu	styl	k1gInSc3	styl
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgInSc3	jenž
dává	dávat	k5eAaImIp3nS	dávat
jiskru	jiskra	k1gFnSc4	jiskra
zejména	zejména	k9	zejména
napětí	napětí	k1gNnSc4	napětí
mezi	mezi	k7c7	mezi
hovorovým	hovorový	k2eAgInSc7d1	hovorový
jazykem	jazyk	k1gInSc7	jazyk
dialogů	dialog	k1gInPc2	dialog
a	a	k8xC	a
jistou	jistý	k2eAgFnSc7d1	jistá
mírou	míra	k1gFnSc7	míra
poetizace	poetizace	k1gFnSc2	poetizace
a	a	k8xC	a
lyrizace	lyrizace	k1gFnSc2	lyrizace
jinak	jinak	k6eAd1	jinak
syntakticky	syntakticky	k6eAd1	syntakticky
sevřeného	sevřený	k2eAgNnSc2d1	sevřené
vyprávění	vyprávění	k1gNnSc2	vyprávění
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
Červeného	Červeného	k2eAgInSc2d1	Červeného
pokoje	pokoj	k1gInSc2	pokoj
se	se	k3xPyFc4	se
Strindberg	Strindberg	k1gMnSc1	Strindberg
stal	stát	k5eAaPmAgMnS	stát
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejdiskutovanějších	diskutovaný	k2eAgMnPc2d3	nejdiskutovanější
a	a	k8xC	a
nejkontroverznějších	kontroverzní	k2eAgMnPc2d3	nejkontroverznější
spisovatelů	spisovatel	k1gMnPc2	spisovatel
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
zesměšňuje	zesměšňovat	k5eAaImIp3nS	zesměšňovat
švédskou	švédský	k2eAgFnSc4d1	švédská
maloměšťáckost	maloměšťáckost	k1gFnSc4	maloměšťáckost
<g/>
,	,	kIx,	,
odhaluje	odhalovat	k5eAaImIp3nS	odhalovat
pokrytectví	pokrytectví	k1gNnSc1	pokrytectví
politických	politický	k2eAgFnPc2d1	politická
elit	elita	k1gFnPc2	elita
a	a	k8xC	a
kritizuje	kritizovat	k5eAaImIp3nS	kritizovat
státní	státní	k2eAgInSc4d1	státní
aparát	aparát	k1gInSc4	aparát
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
však	však	k9	však
oslavuje	oslavovat	k5eAaImIp3nS	oslavovat
bohémský	bohémský	k2eAgInSc4d1	bohémský
život	život	k1gInSc4	život
umělců	umělec	k1gMnPc2	umělec
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
zahálčivých	zahálčivý	k2eAgFnPc2d1	zahálčivá
existencí	existence	k1gFnPc2	existence
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
je	být	k5eAaImIp3nS	být
plastickým	plastický	k2eAgInSc7d1	plastický
obrazem	obraz	k1gInSc7	obraz
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
švédské	švédský	k2eAgFnSc2d1	švédská
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Arvida	Arvid	k1gMnSc4	Arvid
Falka	Falek	k1gMnSc4	Falek
<g/>
,	,	kIx,	,
hlavního	hlavní	k2eAgMnSc4d1	hlavní
hrdinu	hrdina	k1gMnSc4	hrdina
Červeného	Červeného	k2eAgInSc2d1	Červeného
pokoje	pokoj	k1gInSc2	pokoj
<g/>
,	,	kIx,	,
můžeme	moct	k5eAaImIp1nP	moct
označit	označit	k5eAaPmF	označit
za	za	k7c4	za
postavu	postava	k1gFnSc4	postava
značně	značně	k6eAd1	značně
autobiografickou	autobiografický	k2eAgFnSc4d1	autobiografická
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
drtivé	drtivý	k2eAgFnSc2d1	drtivá
většiny	většina	k1gFnSc2	většina
Strindbergova	Strindbergův	k2eAgNnSc2d1	Strindbergovo
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Postavy	postava	k1gFnPc1	postava
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
stručná	stručný	k2eAgFnSc1d1	stručná
charakteristika	charakteristika	k1gFnSc1	charakteristika
==	==	k?	==
</s>
</p>
<p>
<s>
Arvid	Arvid	k1gInSc1	Arvid
Falk	Falk	k1gInSc1	Falk
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgMnSc1d1	hlavní
hrdina	hrdina	k1gMnSc1	hrdina
románu	román	k1gInSc2	román
<g/>
,	,	kIx,	,
usiluje	usilovat	k5eAaImIp3nS	usilovat
o	o	k7c4	o
pravdu	pravda	k1gFnSc4	pravda
a	a	k8xC	a
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Peníze	peníz	k1gInPc1	peníz
a	a	k8xC	a
sláva	sláva	k1gFnSc1	sláva
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
až	až	k6eAd1	až
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
postavami	postava	k1gFnPc7	postava
knihy	kniha	k1gFnPc4	kniha
jsou	být	k5eAaImIp3nP	být
mladí	mladý	k2eAgMnPc1d1	mladý
intelektuálové	intelektuál	k1gMnPc1	intelektuál
<g/>
,	,	kIx,	,
bohémové	bohém	k1gMnPc1	bohém
a	a	k8xC	a
filozofové	filozof	k1gMnPc1	filozof
s	s	k7c7	s
různými	různý	k2eAgInPc7d1	různý
životními	životní	k2eAgInPc7d1	životní
ideály	ideál	k1gInPc7	ideál
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
chudobě	chudoba	k1gFnSc6	chudoba
<g/>
.	.	kIx.	.
</s>
<s>
Setkáváme	setkávat	k5eAaImIp1nP	setkávat
se	se	k3xPyFc4	se
s	s	k7c7	s
Ygbergem	Ygberg	k1gMnSc7	Ygberg
<g/>
,	,	kIx,	,
filozofem	filozof	k1gMnSc7	filozof
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
v	v	k7c6	v
zoufalství	zoufalství	k1gNnSc6	zoufalství
z	z	k7c2	z
hladovění	hladovění	k1gNnSc2	hladovění
později	pozdě	k6eAd2	pozdě
přijímá	přijímat	k5eAaImIp3nS	přijímat
práci	práce	k1gFnSc4	práce
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
Arvid	Arvid	k1gInSc1	Arvid
odmítl	odmítnout	k5eAaPmAgInS	odmítnout
<g/>
;	;	kIx,	;
Sellenem	Sellen	k1gInSc7	Sellen
<g/>
,	,	kIx,	,
malířem	malíř	k1gMnSc7	malíř
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgMnSc4	jenž
dílo	dílo	k1gNnSc1	dílo
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
době	doba	k1gFnSc6	doba
konečně	konečně	k6eAd1	konečně
oceněno	ocenit	k5eAaPmNgNnS	ocenit
<g/>
;	;	kIx,	;
Ollem	Ollem	k1gInSc1	Ollem
Montanem	Montan	k1gInSc7	Montan
<g/>
,	,	kIx,	,
sochařem	sochař	k1gMnSc7	sochař
a	a	k8xC	a
filozofem	filozof	k1gMnSc7	filozof
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
přežívá	přežívat	k5eAaImIp3nS	přežívat
jen	jen	k9	jen
díky	díky	k7c3	díky
přátelům	přítel	k1gMnPc3	přítel
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
konečně	konečně	k6eAd1	konečně
s	s	k7c7	s
žurnalistou	žurnalista	k1gMnSc7	žurnalista
Struvem	Struv	k1gMnSc7	Struv
<g/>
.	.	kIx.	.
</s>
<s>
Sledujeme	sledovat	k5eAaImIp1nP	sledovat
příběh	příběh	k1gInSc4	příběh
Arvida	Arvida	k1gFnSc1	Arvida
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
prosadit	prosadit	k5eAaPmF	prosadit
jako	jako	k9	jako
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
pracuje	pracovat	k5eAaImIp3nS	pracovat
v	v	k7c6	v
několika	několik	k4yIc6	několik
novinách	novina	k1gFnPc6	novina
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
skončí	skončit	k5eAaPmIp3nS	skončit
jako	jako	k9	jako
učitel	učitel	k1gMnSc1	učitel
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
domů	dům	k1gInPc2	dům
jako	jako	k8xS	jako
slavný	slavný	k2eAgMnSc1d1	slavný
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
je	být	k5eAaImIp3nS	být
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
podle	podle	k7c2	podle
salonku	salonek	k1gInSc2	salonek
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
se	se	k3xPyFc4	se
stockholmská	stockholmský	k2eAgFnSc1d1	Stockholmská
bohéma	bohéma	k1gFnSc1	bohéma
schází	scházet	k5eAaImIp3nS	scházet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Citace	citace	k1gFnPc1	citace
z	z	k7c2	z
knihy	kniha	k1gFnSc2	kniha
==	==	k?	==
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Jak	jak	k8xC	jak
už	už	k6eAd1	už
jsem	být	k5eAaImIp1nS	být
řekl	říct	k5eAaPmAgMnS	říct
příteli	přítel	k1gMnSc3	přítel
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
zahájil	zahájit	k5eAaPmAgMnS	zahájit
Falk	Falk	k1gMnSc1	Falk
rozhovor	rozhovor	k1gInSc4	rozhovor
nanovo	nanovo	k6eAd1	nanovo
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
rozešel	rozejít	k5eAaPmAgMnS	rozejít
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
a	a	k8xC	a
opustil	opustit	k5eAaPmAgMnS	opustit
úřednickou	úřednický	k2eAgFnSc4d1	úřednická
dráhu	dráha	k1gFnSc4	dráha
<g/>
;	;	kIx,	;
chci	chtít	k5eAaImIp1nS	chtít
jen	jen	k9	jen
dodat	dodat	k5eAaPmF	dodat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mám	mít	k5eAaImIp1nS	mít
v	v	k7c6	v
úmyslu	úmysl	k1gInSc6	úmysl
stát	stát	k5eAaPmF	stát
se	se	k3xPyFc4	se
literátem	literát	k1gMnSc7	literát
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Literátem	literát	k1gMnSc7	literát
<g/>
!	!	kIx.	!
</s>
<s>
Ale	ale	k9	ale
<g/>
,	,	kIx,	,
probůh	probůh	k?	probůh
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
<g/>
?	?	kIx.	?
</s>
<s>
Vždyť	vždyť	k9	vždyť
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
škoda	škoda	k1gFnSc1	škoda
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
škoda	škoda	k1gFnSc1	škoda
<g/>
;	;	kIx,	;
ale	ale	k8xC	ale
chtěl	chtít	k5eAaImAgMnS	chtít
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
zeptat	zeptat	k5eAaPmF	zeptat
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
nevíte	vědět	k5eNaImIp2nP	vědět
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bych	by	kYmCp1nS	by
mohl	moct	k5eAaImAgMnS	moct
dostat	dostat	k5eAaPmF	dostat
nějakou	nějaký	k3yIgFnSc4	nějaký
práci	práce	k1gFnSc4	práce
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Hm	Hm	k?	Hm
<g/>
,	,	kIx,	,
opravdu	opravdu	k6eAd1	opravdu
těžko	těžko	k6eAd1	těžko
říci	říct	k5eAaPmF	říct
<g/>
.	.	kIx.	.
</s>
<s>
Přichází	přicházet	k5eAaImIp3nS	přicházet
tolik	tolik	k4xDc4	tolik
zájemců	zájemce	k1gMnPc2	zájemce
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
ale	ale	k8xC	ale
nemyslete	myslet	k5eNaImRp2nP	myslet
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
opravdu	opravdu	k6eAd1	opravdu
škoda	škoda	k1gFnSc1	škoda
<g/>
,	,	kIx,	,
že	že	k8xS	že
jste	být	k5eAaImIp2nP	být
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
přesedlat	přesedlat	k5eAaPmF	přesedlat
<g/>
.	.	kIx.	.
</s>
<s>
Dráha	dráha	k1gFnSc1	dráha
literáta	literát	k1gMnSc2	literát
je	být	k5eAaImIp3nS	být
těžká	těžký	k2eAgFnSc1d1	těžká
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Struve	Struev	k1gFnPc4	Struev
dával	dávat	k5eAaImAgInS	dávat
najevo	najevo	k6eAd1	najevo
lítost	lítost	k1gFnSc4	lítost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nemohl	moct	k5eNaImAgMnS	moct
skrýt	skrýt	k5eAaPmF	skrýt
určité	určitý	k2eAgNnSc4d1	určité
potěšení	potěšení	k1gNnSc4	potěšení
nad	nad	k7c7	nad
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
druha	druh	k1gMnSc4	druh
v	v	k7c6	v
neštěstí	neštěstí	k1gNnSc6	neštěstí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
"	"	kIx"	"
Vysvětlete	vysvětlit	k5eAaPmRp2nP	vysvětlit
mi	já	k3xPp1nSc3	já
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
<g/>
,	,	kIx,	,
"	"	kIx"	"
proč	proč	k6eAd1	proč
opouštíte	opouštět	k5eAaImIp2nP	opouštět
dráhu	dráha	k1gFnSc4	dráha
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
slibovala	slibovat	k5eAaImAgFnS	slibovat
slávu	sláva	k1gFnSc4	sláva
i	i	k8xC	i
moc	moc	k1gFnSc4	moc
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Slávu	Sláva	k1gFnSc4	Sláva
těm	ten	k3xDgMnPc3	ten
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
uchvátili	uchvátit	k5eAaPmAgMnP	uchvátit
moc	moc	k6eAd1	moc
<g/>
,	,	kIx,	,
a	a	k8xC	a
moc	moc	k6eAd1	moc
těm	ten	k3xDgMnPc3	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
neberou	brát	k5eNaImIp3nP	brát
žádné	žádný	k3yNgInPc4	žádný
ohledy	ohled	k1gInPc4	ohled
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Ale	ale	k9	ale
jakpak	jakpak	k6eAd1	jakpak
<g/>
!	!	kIx.	!
</s>
<s>
Tak	tak	k9	tak
hrozné	hrozný	k2eAgNnSc1d1	hrozné
to	ten	k3xDgNnSc1	ten
snad	snad	k9	snad
není	být	k5eNaImIp3nS	být
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Že	že	k9	že
ne	ne	k9	ne
<g/>
?	?	kIx.	?
</s>
<s>
Můžeme	moct	k5eAaImIp1nP	moct
si	se	k3xPyFc3	se
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
promluvit	promluvit	k5eAaPmF	promluvit
právě	právě	k6eAd1	právě
tak	tak	k6eAd1	tak
jako	jako	k8xS	jako
o	o	k7c6	o
čemkoli	cokoli	k3yInSc6	cokoli
jiném	jiný	k2eAgNnSc6d1	jiné
<g/>
.	.	kIx.	.
</s>
<s>
Vylíčím	vylíčit	k5eAaPmIp1nS	vylíčit
vám	vy	k3xPp2nPc3	vy
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
to	ten	k3xDgNnSc1	ten
vypadalo	vypadat	k5eAaPmAgNnS	vypadat
jenom	jenom	k9	jenom
z	z	k7c2	z
těch	ten	k3xDgMnPc2	ten
šesti	šest	k4xCc2	šest
úřadů	úřad	k1gInPc2	úřad
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgInPc6	který
jsem	být	k5eAaImIp1nS	být
byl	být	k5eAaImAgMnS	být
zapsán	zapsat	k5eAaPmNgMnS	zapsat
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těch	ten	k3xDgInPc2	ten
prvních	první	k4xOgInPc2	první
pěti	pět	k4xCc3	pět
jsem	být	k5eAaImIp1nS	být
odešel	odejít	k5eAaPmAgMnS	odejít
hned	hned	k6eAd1	hned
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
prostého	prostý	k2eAgInSc2d1	prostý
důvodu	důvod	k1gInSc2	důvod
<g/>
,	,	kIx,	,
že	že	k8xS	že
tam	tam	k6eAd1	tam
nebylo	být	k5eNaImAgNnS	být
co	co	k3yQnSc4	co
dělat	dělat	k5eAaImF	dělat
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
přišel	přijít	k5eAaPmAgMnS	přijít
zeptat	zeptat	k5eAaPmF	zeptat
po	po	k7c6	po
nějaké	nějaký	k3yIgFnSc6	nějaký
práci	práce	k1gFnSc6	práce
<g/>
,	,	kIx,	,
zněla	znět	k5eAaImAgFnS	znět
odpověď	odpověď	k1gFnSc1	odpověď
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Nemáme	mít	k5eNaImIp1nP	mít
<g/>
!	!	kIx.	!
</s>
<s>
A	a	k9	a
taky	taky	k6eAd1	taky
jsem	být	k5eAaImIp1nS	být
tam	tam	k6eAd1	tam
nikdy	nikdy	k6eAd1	nikdy
nikoho	nikdo	k3yNnSc4	nikdo
neviděl	vidět	k5eNaImAgMnS	vidět
něco	něco	k3yInSc4	něco
dělat	dělat	k5eAaImF	dělat
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
to	ten	k3xDgNnSc1	ten
protože	protože	k8xS	protože
jsem	být	k5eAaImIp1nS	být
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
úřadech	úřad	k1gInPc6	úřad
natolik	natolik	k6eAd1	natolik
zahrnovaných	zahrnovaný	k2eAgMnPc2d1	zahrnovaný
úkoly	úkol	k1gInPc4	úkol
jako	jako	k8xC	jako
Kolegium	kolegium	k1gNnSc4	kolegium
pro	pro	k7c4	pro
papírenství	papírenství	k1gNnSc4	papírenství
<g/>
,	,	kIx,	,
Kancelář	kancelář	k1gFnSc1	kancelář
pro	pro	k7c4	pro
uvalování	uvalování	k1gNnSc4	uvalování
daní	daň	k1gFnPc2	daň
a	a	k8xC	a
Generální	generální	k2eAgNnSc1d1	generální
ředitelství	ředitelství	k1gNnSc1	ředitelství
pro	pro	k7c4	pro
penze	penze	k1gFnPc4	penze
vyšších	vysoký	k2eAgMnPc2d2	vyšší
úředníků	úředník	k1gMnPc2	úředník
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgMnPc7	jenž
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
tam	tam	k6eAd1	tam
jen	jen	k9	jen
hemží	hemžit	k5eAaImIp3nS	hemžit
<g/>
,	,	kIx,	,
myslel	myslet	k5eAaImAgMnS	myslet
jsem	být	k5eAaImIp1nS	být
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
aspoň	aspoň	k9	aspoň
úřad	úřad	k1gInSc1	úřad
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
všechny	všechen	k3xTgMnPc4	všechen
platí	platit	k5eAaImIp3nP	platit
<g/>
,	,	kIx,	,
přece	přece	k9	přece
musí	muset	k5eAaImIp3nS	muset
mít	mít	k5eAaImF	mít
nějakou	nějaký	k3yIgFnSc4	nějaký
práci	práce	k1gFnSc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
Zapsal	zapsat	k5eAaPmAgMnS	zapsat
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
v	v	k7c6	v
Kolegiu	kolegium	k1gNnSc6	kolegium
pro	pro	k7c4	pro
vyplácení	vyplácení	k1gNnSc4	vyplácení
platů	plat	k1gInPc2	plat
vyšších	vysoký	k2eAgMnPc2d2	vyšší
úředníků	úředník	k1gMnPc2	úředník
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
