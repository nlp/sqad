<s>
Červený	červený	k2eAgInSc1d1	červený
pokoj	pokoj	k1gInSc1	pokoj
(	(	kIx(	(
<g/>
Röda	Röda	k1gMnSc1	Röda
rummet	rummet	k1gMnSc1	rummet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přelomové	přelomový	k2eAgNnSc1d1	přelomové
dílo	dílo	k1gNnSc1	dílo
švédské	švédský	k2eAgFnSc2d1	švédská
literatury	literatura	k1gFnSc2	literatura
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
román	román	k1gInSc1	román
Augusta	August	k1gMnSc2	August
Strindberga	Strindberg	k1gMnSc2	Strindberg
vydaný	vydaný	k2eAgInSc4d1	vydaný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1879	[number]	k4	1879
nakladatelstvím	nakladatelství	k1gNnSc7	nakladatelství
Josepha	Joseph	k1gMnSc2	Joseph
Selingmanna	Selingmann	k1gMnSc2	Selingmann
<g/>
.	.	kIx.	.
</s>
