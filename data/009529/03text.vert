<p>
<s>
Dhaulágirí	Dhaulágirí	k1gFnSc1	Dhaulágirí
(	(	kIx(	(
<g/>
v	v	k7c6	v
sanskrtu	sanskrt	k1gInSc6	sanskrt
:	:	kIx,	:
"	"	kIx"	"
<g/>
Bílá	bílý	k2eAgFnSc1d1	bílá
hora	hora	k1gFnSc1	hora
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
sedmá	sedmý	k4xOgFnSc1	sedmý
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
Himálaji	Himálaj	k1gFnSc6	Himálaj
<g/>
,	,	kIx,	,
přesněji	přesně	k6eAd2	přesně
v	v	k7c6	v
Nepálu	Nepál	k1gInSc6	Nepál
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
Pokhary	Pokhara	k1gFnSc2	Pokhara
<g/>
,	,	kIx,	,
důležitého	důležitý	k2eAgNnSc2d1	důležité
regionálního	regionální	k2eAgNnSc2d1	regionální
a	a	k8xC	a
turistického	turistický	k2eAgNnSc2d1	turistické
centra	centrum	k1gNnSc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
hluboké	hluboký	k2eAgFnSc2d1	hluboká
rokle	rokle	k1gFnSc2	rokle
řeky	řeka	k1gFnSc2	řeka
Kali	Kal	k1gFnSc2	Kal
Gandaki	Gandak	k1gFnSc2	Gandak
leží	ležet	k5eAaImIp3nS	ležet
Annapurna	Annapurna	k1gFnSc1	Annapurna
<g/>
,	,	kIx,	,
další	další	k2eAgFnPc1d1	další
z	z	k7c2	z
osmitisícovek	osmitisícovka	k1gFnPc2	osmitisícovka
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
výstupů	výstup	k1gInPc2	výstup
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
kopíruje	kopírovat	k5eAaImIp3nS	kopírovat
klasickou	klasický	k2eAgFnSc4d1	klasická
cestu	cesta	k1gFnSc4	cesta
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Normal	Normal	k1gInSc4	Normal
route	route	k5eAaPmIp2nP	route
<g/>
)	)	kIx)	)
i	i	k8xC	i
když	když	k8xS	když
hora	hora	k1gFnSc1	hora
již	již	k6eAd1	již
byla	být	k5eAaImAgFnS	být
vylezena	vylezen	k2eAgFnSc1d1	vylezena
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
světových	světový	k2eAgFnPc2d1	světová
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
objevu	objev	k1gInSc6	objev
hory	hora	k1gFnSc2	hora
v	v	k7c6	v
r.	r.	kA	r.
1808	[number]	k4	1808
západním	západní	k2eAgInSc7d1	západní
světem	svět	k1gInSc7	svět
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
Dhaulágirí	Dhaulágirí	k1gFnSc1	Dhaulágirí
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
horu	hora	k1gFnSc4	hora
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
trvalo	trvat	k5eAaImAgNnS	trvat
dlouhých	dlouhý	k2eAgInPc2d1	dlouhý
30	[number]	k4	30
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgNnP	být
před	před	k7c7	před
Dhaulágirí	Dhaulágir	k1gFnSc7	Dhaulágir
posunuta	posunut	k2eAgFnSc1d1	posunuta
Kančendženga	Kančendženga	k1gFnSc1	Kančendženga
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
převýšení	převýšení	k1gNnSc2	převýšení
svého	svůj	k3xOyFgNnSc2	svůj
okolí	okolí	k1gNnSc2	okolí
je	být	k5eAaImIp3nS	být
Dhaulágirí	Dhaulágirí	k1gFnSc1	Dhaulágirí
<g/>
,	,	kIx,	,
skutečně	skutečně	k6eAd1	skutečně
bezkonkurenčně	bezkonkurenčně	k6eAd1	bezkonkurenčně
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Údolí	údolí	k1gNnSc1	údolí
řeky	řeka	k1gFnSc2	řeka
Kali	Kal	k1gFnSc2	Kal
Gandaki	Gandak	k1gFnSc2	Gandak
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
převyšuje	převyšovat	k5eAaImIp3nS	převyšovat
o	o	k7c4	o
neuvěřitelných	uvěřitelný	k2eNgInPc2d1	neuvěřitelný
7	[number]	k4	7
000	[number]	k4	000
výškových	výškový	k2eAgInPc2d1	výškový
metrů	metr	k1gInPc2	metr
jen	jen	k9	jen
na	na	k7c4	na
30	[number]	k4	30
km	km	kA	km
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	řeka	k1gFnSc1	řeka
Kali	Kal	k1gFnSc2	Kal
Gandaki	Gandak	k1gFnSc2	Gandak
teče	téct	k5eAaImIp3nS	téct
ve	v	k7c6	v
skutečně	skutečně	k6eAd1	skutečně
dramatickém	dramatický	k2eAgNnSc6d1	dramatické
prostředí	prostředí	k1gNnSc6	prostředí
masivů	masiv	k1gInPc2	masiv
Dhaulágirí	Dhaulágirí	k1gFnSc2	Dhaulágirí
a	a	k8xC	a
Annapurny	Annapurna	k1gFnSc2	Annapurna
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
nad	nad	k7c7	nad
ní	on	k3xPp3gFnSc7	on
svými	svůj	k3xOyFgMnPc7	svůj
stěnami	stěna	k1gFnPc7	stěna
naproti	naproti	k7c3	naproti
sobě	se	k3xPyFc3	se
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
a	a	k8xC	a
západní	západní	k2eAgFnSc1d1	západní
stěna	stěna	k1gFnSc1	stěna
Dhaulágirí	Dhaulágirí	k1gNnSc1	Dhaulágirí
jsou	být	k5eAaImIp3nP	být
vysoké	vysoký	k2eAgInPc1d1	vysoký
přes	přes	k7c4	přes
4	[number]	k4	4
000	[number]	k4	000
m	m	kA	m
<g/>
,	,	kIx,	,
východní	východní	k2eAgFnSc1d1	východní
a	a	k8xC	a
severní	severní	k2eAgFnSc1d1	severní
stěna	stěna	k1gFnSc1	stěna
zhruba	zhruba	k6eAd1	zhruba
3000	[number]	k4	3000
<g/>
m.	m.	k?	m.
Dhaulágirí	Dhaulágirí	k1gMnSc1	Dhaulágirí
je	být	k5eAaImIp3nS	být
mezi	mezi	k7c4	mezi
turisty	turist	k1gMnPc4	turist
velmi	velmi	k6eAd1	velmi
atraktivní	atraktivní	k2eAgFnSc7d1	atraktivní
horou	hora	k1gFnSc7	hora
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
Kančendženga	Kančendženga	k1gFnSc1	Kančendženga
a	a	k8xC	a
Nanga	Nanga	k1gFnSc1	Nanga
Parbat	Parbat	k1gFnPc2	Parbat
není	být	k5eNaImIp3nS	být
cloněna	clonit	k5eAaImNgFnS	clonit
jinou	jiný	k2eAgFnSc7d1	jiná
vysokou	vysoký	k2eAgFnSc7d1	vysoká
horou	hora	k1gFnSc7	hora
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
za	za	k7c2	za
dobrého	dobrý	k2eAgNnSc2d1	dobré
počasí	počasí	k1gNnSc2	počasí
viditelná	viditelný	k2eAgFnSc1d1	viditelná
až	až	k9	až
ze	z	k7c2	z
severní	severní	k2eAgFnSc2d1	severní
Indie	Indie	k1gFnSc2	Indie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vrcholy	vrchol	k1gInPc4	vrchol
==	==	k?	==
</s>
</p>
<p>
<s>
Následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
seznam	seznam	k1gInSc1	seznam
vrcholů	vrchol	k1gInPc2	vrchol
masívu	masív	k1gInSc2	masív
Dhaulágirí	Dhaulágir	k1gFnPc2	Dhaulágir
vyšších	vysoký	k2eAgFnPc2d2	vyšší
než	než	k8xS	než
7	[number]	k4	7
000	[number]	k4	000
m.	m.	k?	m.
</s>
</p>
<p>
<s>
==	==	k?	==
Prvovýstup	prvovýstup	k1gInSc4	prvovýstup
==	==	k?	==
</s>
</p>
<p>
<s>
Dhaulágirí	Dhaulágirí	k1gFnSc1	Dhaulágirí
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
zdolána	zdolán	k2eAgFnSc1d1	zdolána
13	[number]	k4	13
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1960	[number]	k4	1960
členy	člen	k1gMnPc7	člen
švýcarsko-rakouské	švýcarskoakouský	k2eAgFnSc2d1	švýcarsko-rakouský
expedice	expedice	k1gFnSc2	expedice
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
vedl	vést	k5eAaImAgMnS	vést
Max	Max	k1gMnSc1	Max
Eiselin	Eiselina	k1gFnPc2	Eiselina
<g/>
,	,	kIx,	,
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
hory	hora	k1gFnSc2	hora
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
<g/>
:	:	kIx,	:
Kurt	Kurt	k1gMnSc1	Kurt
Diemberger	Diemberger	k1gMnSc1	Diemberger
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
Diener	Diener	k1gMnSc1	Diener
<g/>
,	,	kIx,	,
Ernst	Ernst	k1gMnSc1	Ernst
Forrer	Forrer	k1gMnSc1	Forrer
<g/>
,	,	kIx,	,
Albin	Albin	k1gMnSc1	Albin
Schelbert	Schelbert	k1gMnSc1	Schelbert
<g/>
,	,	kIx,	,
Nyima	Nyima	k1gFnSc1	Nyima
Dorji	Dorje	k1gFnSc4	Dorje
a	a	k8xC	a
Nawang	Nawang	k1gInSc4	Nawang
Dorji	Dorje	k1gFnSc4	Dorje
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
deset	deset	k4xCc4	deset
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
je	on	k3xPp3gMnPc4	on
následovali	následovat	k5eAaImAgMnP	následovat
ještě	ještě	k6eAd1	ještě
Michel	Michel	k1gInSc4	Michel
Vaucher	Vauchra	k1gFnPc2	Vauchra
a	a	k8xC	a
Weber	weber	k1gInSc1	weber
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Historie	historie	k1gFnSc1	historie
výstupů	výstup	k1gInPc2	výstup
na	na	k7c4	na
Dhaulágirí	Dhaulágirí	k1gFnSc4	Dhaulágirí
===	===	k?	===
</s>
</p>
<p>
<s>
Dhaulágirí	Dhaulágirí	k1gFnSc1	Dhaulágirí
stojí	stát	k5eAaImIp3nS	stát
v	v	k7c6	v
centrálním	centrální	k2eAgInSc6d1	centrální
Nepálu	Nepál	k1gInSc6	Nepál
<g/>
,	,	kIx,	,
nebyla	být	k5eNaImAgFnS	být
proto	proto	k8xC	proto
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
přístupná	přístupný	k2eAgFnSc1d1	přístupná
cizincům	cizinec	k1gMnPc3	cizinec
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
pilot	pilot	k1gMnSc1	pilot
Geiger	Geiger	k1gMnSc1	Geiger
přeletěl	přeletět	k5eAaPmAgMnS	přeletět
nad	nad	k7c7	nad
Dhaulágirí	Dhaulágir	k1gFnSc7	Dhaulágir
a	a	k8xC	a
Annapurnou	Annapurna	k1gFnSc7	Annapurna
a	a	k8xC	a
pořídil	pořídit	k5eAaPmAgMnS	pořídit
první	první	k4xOgFnPc4	první
fotografie	fotografia	k1gFnPc4	fotografia
těchto	tento	k3xDgInPc2	tento
vrcholů	vrchol	k1gInPc2	vrchol
zblízka	zblízka	k6eAd1	zblízka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc7	první
výpravou	výprava	k1gFnSc7	výprava
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
byli	být	k5eAaImAgMnP	být
Francouzi	Francouz	k1gMnPc1	Francouz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
<g/>
.	.	kIx.	.
</s>
<s>
Nalezli	nalézt	k5eAaBmAgMnP	nalézt
přístupové	přístupový	k2eAgFnPc4d1	přístupová
cesty	cesta	k1gFnPc4	cesta
k	k	k7c3	k
Dhaulágirí	Dhaulágirí	k1gFnSc3	Dhaulágirí
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nenašli	najít	k5eNaPmAgMnP	najít
žádnou	žádný	k3yNgFnSc4	žádný
vhodnou	vhodný	k2eAgFnSc4d1	vhodná
trasu	trasa	k1gFnSc4	trasa
k	k	k7c3	k
vrcholu	vrchol	k1gInSc3	vrchol
hory	hora	k1gFnSc2	hora
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
proto	proto	k8xC	proto
obrátili	obrátit	k5eAaPmAgMnP	obrátit
svou	svůj	k3xOyFgFnSc4	svůj
pozornost	pozornost	k1gFnSc4	pozornost
k	k	k7c3	k
Annapurně	Annapurna	k1gFnSc3	Annapurna
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yRgFnSc4	který
3	[number]	k4	3
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
jako	jako	k8xC	jako
první	první	k4xOgMnPc1	první
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgInSc1d1	další
průzkum	průzkum	k1gInSc1	průzkum
svahů	svah	k1gInPc2	svah
Dhaulágirí	Dhaulágirí	k1gNnSc2	Dhaulágirí
provedli	provést	k5eAaPmAgMnP	provést
Švýcaři	Švýcar	k1gMnPc1	Švýcar
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
<g/>
.	.	kIx.	.
</s>
<s>
Vybrali	vybrat	k5eAaPmAgMnP	vybrat
si	se	k3xPyFc3	se
pravou	pravý	k2eAgFnSc4d1	pravá
část	část	k1gFnSc4	část
S	s	k7c7	s
stěny	stěn	k1gInPc7	stěn
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
stoupali	stoupat	k5eAaImAgMnP	stoupat
přes	přes	k7c4	přes
skalní	skalní	k2eAgNnSc4d1	skalní
žebro	žebro	k1gNnSc4	žebro
nazývané	nazývaný	k2eAgNnSc4d1	nazývané
Hruška	hruška	k1gFnSc1	hruška
(	(	kIx(	(
<g/>
La	la	k0	la
Poire	Poir	k1gMnSc5	Poir
<g/>
)	)	kIx)	)
do	do	k7c2	do
7600	[number]	k4	7600
<g/>
m.	m.	k?	m.
V	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
stěny	stěna	k1gFnSc2	stěna
zbrzdila	zbrzdit	k5eAaPmAgFnS	zbrzdit
jejich	jejich	k3xOp3gInSc4	jejich
postup	postup	k1gInSc4	postup
ledový	ledový	k2eAgInSc1d1	ledový
svah	svah	k1gInSc1	svah
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
nebylo	být	k5eNaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
postavit	postavit	k5eAaPmF	postavit
stany	stan	k1gInPc4	stan
dalšího	další	k2eAgInSc2d1	další
tábora	tábor	k1gInSc2	tábor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
do	do	k7c2	do
cesty	cesta	k1gFnSc2	cesta
přes	přes	k7c4	přes
Hrušku	hruška	k1gFnSc4	hruška
pustili	pustit	k5eAaPmAgMnP	pustit
Argentinci	Argentinec	k1gMnPc1	Argentinec
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
prozkoumání	prozkoumání	k1gNnSc6	prozkoumání
zpráv	zpráva	k1gFnPc2	zpráva
švýcarské	švýcarský	k2eAgFnSc2d1	švýcarská
expedice	expedice	k1gFnSc2	expedice
si	se	k3xPyFc3	se
s	s	k7c7	s
sebou	se	k3xPyFc7	se
přivezli	přivézt	k5eAaPmAgMnP	přivézt
experta	expert	k1gMnSc4	expert
na	na	k7c4	na
trhaviny	trhavina	k1gFnPc4	trhavina
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zcela	zcela	k6eAd1	zcela
nehorolezeckou	horolezecký	k2eNgFnSc7d1	horolezecký
metodou	metoda	k1gFnSc7	metoda
odstřelil	odstřelit	k5eAaPmAgInS	odstřelit
část	část	k1gFnSc4	část
ledové	ledový	k2eAgFnSc2d1	ledová
stěny	stěna	k1gFnSc2	stěna
a	a	k8xC	a
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
plošinu	plošina	k1gFnSc4	plošina
pro	pro	k7c4	pro
postupový	postupový	k2eAgInSc4d1	postupový
tábor	tábor	k1gInSc4	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Počasí	počasí	k1gNnSc1	počasí
poté	poté	k6eAd1	poté
Argentince	Argentinka	k1gFnSc3	Argentinka
zastavilo	zastavit	k5eAaPmAgNnS	zastavit
v	v	k7c6	v
7900	[number]	k4	7900
<g/>
m	m	kA	m
na	na	k7c6	na
SZ	SZ	kA	SZ
hřebeni	hřeben	k1gInSc6	hřeben
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stejnou	stejný	k2eAgFnSc7d1	stejná
trasou	trasa	k1gFnSc7	trasa
poté	poté	k6eAd1	poté
zkoušeli	zkoušet	k5eAaImAgMnP	zkoušet
vystoupit	vystoupit	k5eAaPmF	vystoupit
Němci	Němec	k1gMnPc1	Němec
(	(	kIx(	(
<g/>
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
Argentinci	Argentinec	k1gMnPc1	Argentinec
(	(	kIx(	(
<g/>
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
Švýcaři	Švýcar	k1gMnPc1	Švýcar
(	(	kIx(	(
<g/>
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
získala	získat	k5eAaPmAgFnS	získat
Dhaulágirí	Dhaulágirí	k2eAgFnSc1d1	Dhaulágirí
pověst	pověst	k1gFnSc1	pověst
nedobytné	dobytný	k2eNgFnSc2d1	nedobytná
hory	hora	k1gFnSc2	hora
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
zůstala	zůstat	k5eAaPmAgFnS	zůstat
poslední	poslední	k2eAgFnSc7d1	poslední
nezlezenou	zlezený	k2eNgFnSc7d1	zlezený
osmitisícovkou	osmitisícovka	k1gFnSc7	osmitisícovka
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
z	z	k7c2	z
politických	politický	k2eAgInPc2d1	politický
důvodů	důvod	k1gInPc2	důvod
nepřístupné	přístupný	k2eNgFnSc2d1	nepřístupná
Šišapangmy	Šišapangma	k1gFnSc2	Šišapangma
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Max	Max	k1gMnSc1	Max
Eiselin	Eiselina	k1gFnPc2	Eiselina
proto	proto	k8xC	proto
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
začal	začít	k5eAaPmAgInS	začít
uvažovat	uvažovat	k5eAaImF	uvažovat
o	o	k7c6	o
možnostech	možnost	k1gFnPc6	možnost
výstupu	výstup	k1gInSc2	výstup
SV	sv	kA	sv
hřebenem	hřeben	k1gInSc7	hřeben
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezitím	mezitím	k6eAd1	mezitím
získala	získat	k5eAaPmAgFnS	získat
povolení	povolení	k1gNnSc4	povolení
k	k	k7c3	k
výstupu	výstup	k1gInSc3	výstup
rakouská	rakouský	k2eAgFnSc1d1	rakouská
výprava	výprava	k1gFnSc1	výprava
<g/>
.	.	kIx.	.
</s>
<s>
Rakušané	Rakušan	k1gMnPc1	Rakušan
byli	být	k5eAaImAgMnP	být
dosud	dosud	k6eAd1	dosud
na	na	k7c6	na
osmitisícovkách	osmitisícovka	k1gFnPc6	osmitisícovka
nejúspěšnější	úspěšný	k2eAgFnSc1d3	nejúspěšnější
<g/>
,	,	kIx,	,
zlezli	zlézt	k5eAaPmAgMnP	zlézt
čtyři	čtyři	k4xCgMnPc1	čtyři
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
vyzkoušeli	vyzkoušet	k5eAaPmAgMnP	vyzkoušet
novou	nový	k2eAgFnSc4d1	nová
trasu	trasa	k1gFnSc4	trasa
SV	sv	kA	sv
hřebenem	hřeben	k1gInSc7	hřeben
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgInPc4	který
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
7900	[number]	k4	7900
<g/>
m.	m.	k?	m.
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
H.	H.	kA	H.
<g/>
Roiss	Roiss	k1gInSc1	Roiss
zemřel	zemřít	k5eAaPmAgInS	zemřít
pádem	pád	k1gInSc7	pád
do	do	k7c2	do
trhliny	trhlina	k1gFnSc2	trhlina
a	a	k8xC	a
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
táborů	tábor	k1gInPc2	tábor
smetla	smetnout	k5eAaPmAgFnS	smetnout
lavina	lavina	k1gFnSc1	lavina
<g/>
,	,	kIx,	,
svůj	svůj	k3xOyFgInSc4	svůj
postup	postup	k1gInSc4	postup
vzdali	vzdát	k5eAaPmAgMnP	vzdát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1960	[number]	k4	1960
-	-	kIx~	-
SV	sv	kA	sv
hřeben	hřeben	k1gInSc1	hřeben
<g/>
.	.	kIx.	.
</s>
<s>
Max	Max	k1gMnSc1	Max
Eiselin	Eiselina	k1gFnPc2	Eiselina
připravil	připravit	k5eAaPmAgMnS	připravit
vlastní	vlastní	k2eAgFnSc4d1	vlastní
expedici	expedice	k1gFnSc4	expedice
k	k	k7c3	k
Dhaulágirí	Dhaulágirí	k1gFnSc3	Dhaulágirí
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
součástí	součást	k1gFnSc7	součást
bylo	být	k5eAaImAgNnS	být
i	i	k9	i
zásobovací	zásobovací	k2eAgNnSc1d1	zásobovací
letadlo	letadlo	k1gNnSc1	letadlo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
přistávalo	přistávat	k5eAaImAgNnS	přistávat
v	v	k7c6	v
sedlech	sedlo	k1gNnPc6	sedlo
Dambuš	Dambuš	k1gInSc4	Dambuš
(	(	kIx(	(
<g/>
5200	[number]	k4	5200
<g/>
m	m	kA	m
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
Severovýchodním	severovýchodní	k2eAgNnSc6d1	severovýchodní
sedle	sedlo	k1gNnSc6	sedlo
(	(	kIx(	(
<g/>
5700	[number]	k4	5700
<g/>
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
přeletech	přelet	k1gInPc6	přelet
museli	muset	k5eAaImAgMnP	muset
piloti	pilot	k1gMnPc1	pilot
nouzově	nouzově	k6eAd1	nouzově
doplachtit	doplachtit	k5eAaPmF	doplachtit
na	na	k7c4	na
letiště	letiště	k1gNnSc4	letiště
v	v	k7c4	v
Pokhaře	Pokhař	k1gMnPc4	Pokhař
s	s	k7c7	s
nefunkčním	funkční	k2eNgInSc7d1	nefunkční
motorem	motor	k1gInSc7	motor
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zaslání	zaslání	k1gNnSc6	zaslání
nového	nový	k2eAgInSc2d1	nový
motoru	motor	k1gInSc2	motor
lety	léto	k1gNnPc7	léto
pokračovaly	pokračovat	k5eAaImAgFnP	pokračovat
až	až	k6eAd1	až
do	do	k7c2	do
havárie	havárie	k1gFnSc2	havárie
při	při	k7c6	při
startu	start	k1gInSc6	start
ze	z	k7c2	z
sedla	sedlo	k1gNnSc2	sedlo
Dambuš	Dambuš	k1gInSc1	Dambuš
<g/>
.	.	kIx.	.
</s>
<s>
Posádka	posádka	k1gFnSc1	posádka
vyvázla	vyváznout	k5eAaPmAgFnS	vyváznout
ve	v	k7c4	v
zdraví	zdraví	k1gNnSc4	zdraví
<g/>
,	,	kIx,	,
vrak	vrak	k1gInSc1	vrak
letadla	letadlo	k1gNnSc2	letadlo
zůstal	zůstat	k5eAaPmAgInS	zůstat
v	v	k7c6	v
sedle	sedlo	k1gNnSc6	sedlo
několik	několik	k4yIc4	několik
dalších	další	k2eAgNnPc2d1	další
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Expedice	expedice	k1gFnSc1	expedice
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
ve	v	k7c6	v
výstupu	výstup	k1gInSc6	výstup
hřebenem	hřeben	k1gInSc7	hřeben
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
celkem	celkem	k6eAd1	celkem
osm	osm	k4xCc1	osm
jejích	její	k3xOp3gMnPc2	její
členů	člen	k1gMnPc2	člen
nevystoupilo	vystoupit	k5eNaPmAgNnS	vystoupit
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
hory	hora	k1gFnSc2	hora
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
výše	výše	k1gFnSc2	výše
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Trasa	trasa	k1gFnSc1	trasa
SV	sv	kA	sv
hřebenem	hřeben	k1gInSc7	hřeben
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
klasickou	klasický	k2eAgFnSc7d1	klasická
výstupovou	výstupový	k2eAgFnSc7d1	výstupová
cestou	cesta	k1gFnSc7	cesta
na	na	k7c6	na
Dhaulágirí	Dhaulágirí	k1gFnSc6	Dhaulágirí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgInSc4	první
sólovýstup	sólovýstup	k1gInSc4	sólovýstup
zvládl	zvládnout	k5eAaPmAgMnS	zvládnout
Japonec	Japonec	k1gMnSc1	Japonec
Hinorobu	Hinoroba	k1gFnSc4	Hinoroba
Kamuro	Kamura	k1gFnSc5	Kamura
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
použil	použít	k5eAaPmAgInS	použít
však	však	k9	však
k	k	k7c3	k
výstupu	výstup	k1gInSc3	výstup
lana	lano	k1gNnSc2	lano
i	i	k8xC	i
tábory	tábor	k1gInPc4	tábor
ukončené	ukončený	k2eAgFnSc2d1	ukončená
kanadské	kanadský	k2eAgFnSc2d1	kanadská
expedice	expedice	k1gFnSc2	expedice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc7	první
ženou	žena	k1gFnSc7	žena
na	na	k7c6	na
Dhaulágirí	Dhaulágirí	k1gFnSc6	Dhaulágirí
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Belgičanka	Belgičanka	k1gFnSc1	Belgičanka
Lutgaarde	Lutgaard	k1gMnSc5	Lutgaard
Vivijs	Vivijs	k1gInSc4	Vivijs
po	po	k7c6	po
výstupu	výstup	k1gInSc6	výstup
klasickou	klasický	k2eAgFnSc7d1	klasická
cestou	cesta	k1gFnSc7	cesta
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgInSc1	první
zimní	zimní	k2eAgInSc1d1	zimní
výstup	výstup	k1gInSc1	výstup
na	na	k7c4	na
Dhaulágirí	Dhaulágirí	k1gFnSc4	Dhaulágirí
se	se	k3xPyFc4	se
podařil	podařit	k5eAaPmAgInS	podařit
klasickou	klasický	k2eAgFnSc7d1	klasická
cestou	cesta	k1gFnSc7	cesta
Japoncům	Japonec	k1gMnPc3	Japonec
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1982	[number]	k4	1982
<g/>
,	,	kIx,	,
použili	použít	k5eAaPmAgMnP	použít
však	však	k9	však
přitom	přitom	k6eAd1	přitom
připravenou	připravený	k2eAgFnSc4d1	připravená
trasu	trasa	k1gFnSc4	trasa
po	po	k7c6	po
podzimní	podzimní	k2eAgFnSc6d1	podzimní
japonské	japonský	k2eAgFnSc6d1	japonská
výpravě	výprava	k1gFnSc6	výprava
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nich	on	k3xPp3gInPc6	on
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1985	[number]	k4	1985
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
Poláci	Polák	k1gMnPc1	Polák
Andrzej	Andrzej	k1gMnSc1	Andrzej
Czok	Czok	k1gMnSc1	Czok
a	a	k8xC	a
Jerzy	Jerza	k1gFnSc2	Jerza
Kukuczka	Kukuczka	k1gFnSc1	Kukuczka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1978	[number]	k4	1978
-	-	kIx~	-
JZ	JZ	kA	JZ
hřeben	hřeben	k1gInSc1	hřeben
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
prvním	první	k4xOgInSc6	první
pokusu	pokus	k1gInSc6	pokus
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
druhá	druhý	k4xOgFnSc1	druhý
japonská	japonský	k2eAgFnSc1d1	japonská
expedice	expedice	k1gFnSc1	expedice
k	k	k7c3	k
obtížnému	obtížný	k2eAgNnSc3d1	obtížné
JZ	JZ	kA	JZ
hřebeni	hřeben	k1gInSc6	hřeben
<g/>
,	,	kIx,	,
po	po	k7c6	po
kterém	který	k3yIgNnSc6	který
stoupá	stoupat	k5eAaImIp3nS	stoupat
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
<g/>
.	.	kIx.	.
<g/>
Jejich	jejich	k3xOp3gInSc4	jejich
výstup	výstup	k1gInSc4	výstup
zopakovali	zopakovat	k5eAaPmAgMnP	zopakovat
Francouzi	Francouz	k1gMnPc1	Francouz
André	André	k1gMnSc2	André
Roche	Roche	k1gNnSc1	Roche
a	a	k8xC	a
Pierre	Pierr	k1gInSc5	Pierr
Beghin	Beghin	k1gInSc4	Beghin
alpským	alpský	k2eAgInSc7d1	alpský
stylem	styl	k1gInSc7	styl
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
(	(	kIx(	(
<g/>
zároveň	zároveň	k6eAd1	zároveň
první	první	k4xOgInSc4	první
výstup	výstup	k1gInSc4	výstup
alpským	alpský	k2eAgInSc7d1	alpský
stylem	styl	k1gInSc7	styl
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
Dhaulágirí	Dhaulágirí	k1gFnSc2	Dhaulágirí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1978	[number]	k4	1978
-	-	kIx~	-
JV	JV	kA	JV
hřeben	hřeben	k1gInSc1	hřeben
<g/>
.	.	kIx.	.
</s>
<s>
Touto	tento	k3xDgFnSc7	tento
trasou	trasa	k1gFnSc7	trasa
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
pokusili	pokusit	k5eAaPmAgMnP	pokusit
o	o	k7c4	o
výstup	výstup	k1gInSc4	výstup
Američané	Američan	k1gMnPc1	Američan
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
téměř	téměř	k6eAd1	téměř
všichni	všechen	k3xTgMnPc1	všechen
členové	člen	k1gMnPc1	člen
výpravy	výprava	k1gFnSc2	výprava
zahynuli	zahynout	k5eAaPmAgMnP	zahynout
pod	pod	k7c7	pod
lavinou	lavina	k1gFnSc7	lavina
<g/>
.	.	kIx.	.
</s>
<s>
Japonci	Japonec	k1gMnPc1	Japonec
úspěšně	úspěšně	k6eAd1	úspěšně
trasu	trasa	k1gFnSc4	trasa
dokončili	dokončit	k5eAaPmAgMnP	dokončit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
oni	onen	k3xDgMnPc1	onen
přišli	přijít	k5eAaPmAgMnP	přijít
o	o	k7c4	o
čtyři	čtyři	k4xCgMnPc4	čtyři
horolezce	horolezec	k1gMnPc4	horolezec
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
tento	tento	k3xDgInSc1	tento
výstup	výstup	k1gInSc1	výstup
nebyl	být	k5eNaImAgInS	být
opakován	opakovat	k5eAaImNgInS	opakovat
<g/>
.1980	.1980	k4	.1980
-	-	kIx~	-
V	V	kA	V
stěna	stěna	k1gFnSc1	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
čtveřice	čtveřice	k1gFnSc1	čtveřice
Ghillini	Ghillin	k2eAgMnPc1d1	Ghillin
<g/>
,	,	kIx,	,
Kurtyka	Kurtyka	k1gFnSc1	Kurtyka
<g/>
,	,	kIx,	,
McIntyre	McIntyr	k1gInSc5	McIntyr
a	a	k8xC	a
Wilczynski	Wilczynski	k1gNnSc1	Wilczynski
zdolává	zdolávat	k5eAaImIp3nS	zdolávat
alpským	alpský	k2eAgInSc7d1	alpský
stylem	styl	k1gInSc7	styl
V	v	k7c4	v
stěnu	stěna	k1gFnSc4	stěna
<g/>
,	,	kIx,	,
výstup	výstup	k1gInSc4	výstup
ukončují	ukončovat	k5eAaImIp3nP	ukončovat
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
spojení	spojení	k1gNnPc2	spojení
JV	JV	kA	JV
a	a	k8xC	a
SV	sv	kA	sv
hřebene	hřeben	k1gInSc2	hřeben
v	v	k7c6	v
8000	[number]	k4	8000
<g/>
m.	m.	k?	m.
Po	po	k7c6	po
sestupu	sestup	k1gInSc6	sestup
a	a	k8xC	a
odpočinku	odpočinek	k1gInSc3	odpočinek
zlézají	zlézat	k5eAaImIp3nP	zlézat
Dhaulágirí	Dhaulágirí	k2eAgInPc1d1	Dhaulágirí
během	během	k7c2	během
dvou	dva	k4xCgInPc2	dva
dnů	den	k1gInPc2	den
klasickou	klasický	k2eAgFnSc7d1	klasická
cestou	cesta	k1gFnSc7	cesta
alpským	alpský	k2eAgInSc7d1	alpský
stylem	styl	k1gInSc7	styl
<g/>
.	.	kIx.	.
</s>
<s>
V	V	kA	V
stěna	stěna	k1gFnSc1	stěna
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
snímku	snímek	k1gInSc6	snímek
v	v	k7c6	v
levé	levý	k2eAgFnSc6d1	levá
částiV	částiV	k?	částiV
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
přelezla	přelézt	k5eAaPmAgFnS	přelézt
V	v	k7c6	v
stěnu	stěn	k1gInSc6	stěn
trojice	trojice	k1gFnSc2	trojice
Erhard	Erharda	k1gFnPc2	Erharda
Loretan	Loretan	k1gInSc1	Loretan
<g/>
,	,	kIx,	,
Jean	Jean	k1gMnSc1	Jean
Troillet	Troillet	k1gInSc1	Troillet
a	a	k8xC	a
Pierre	Pierr	k1gInSc5	Pierr
Alain	Alain	k1gMnSc1	Alain
Steiner	Steiner	k1gMnSc1	Steiner
ze	z	k7c2	z
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Postupovali	postupovat	k5eAaImAgMnP	postupovat
alpským	alpský	k2eAgInSc7d1	alpský
stylem	styl	k1gInSc7	styl
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
(	(	kIx(	(
<g/>
celkově	celkově	k6eAd1	celkově
třetí	třetí	k4xOgInSc4	třetí
zimní	zimní	k2eAgInSc4d1	zimní
výstup	výstup	k1gInSc4	výstup
na	na	k7c4	na
Dhaulágirí	Dhaulágirí	k2eAgNnSc4d1	Dhaulágirí
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc4	druhý
a	a	k8xC	a
poslední	poslední	k2eAgInSc4d1	poslední
výstup	výstup	k1gInSc4	výstup
touto	tento	k3xDgFnSc7	tento
cestou	cesta	k1gFnSc7	cesta
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc4	druhý
zimní	zimní	k2eAgInSc4d1	zimní
výstup	výstup	k1gInSc4	výstup
alpským	alpský	k2eAgInSc7d1	alpský
stylem	styl	k1gInSc7	styl
na	na	k7c4	na
osmitisícovku	osmitisícovka	k1gFnSc4	osmitisícovka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1980	[number]	k4	1980
-	-	kIx~	-
Francouzi	Francouz	k1gMnSc5	Francouz
Pierre	Pierr	k1gMnSc5	Pierr
Beghin	Beghin	k1gMnSc1	Beghin
a	a	k8xC	a
Bernard	Bernard	k1gMnSc1	Bernard
Muller	Muller	k1gMnSc1	Muller
zlézají	zlézat	k5eAaImIp3nP	zlézat
mimořádně	mimořádně	k6eAd1	mimořádně
obtížný	obtížný	k2eAgInSc4d1	obtížný
JZ	JZ	kA	JZ
pilíř	pilíř	k1gInSc4	pilíř
vlevo	vlevo	k6eAd1	vlevo
od	od	k7c2	od
japonské	japonský	k2eAgFnSc2d1	japonská
cesty	cesta	k1gFnSc2	cesta
JZ	JZ	kA	JZ
hřebenem	hřeben	k1gInSc7	hřeben
<g/>
.	.	kIx.	.
</s>
<s>
Výstup	výstup	k1gInSc4	výstup
přerušili	přerušit	k5eAaPmAgMnP	přerušit
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
7250	[number]	k4	7250
<g/>
m	m	kA	m
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pilíř	pilíř	k1gInSc1	pilíř
kulminuje	kulminovat	k5eAaImIp3nS	kulminovat
a	a	k8xC	a
přechází	přecházet	k5eAaImIp3nS	přecházet
do	do	k7c2	do
podvrcholové	podvrcholový	k2eAgFnSc2d1	podvrcholová
plošiny	plošina	k1gFnSc2	plošina
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
se	se	k3xPyFc4	se
pokusili	pokusit	k5eAaPmAgMnP	pokusit
o	o	k7c4	o
výstup	výstup	k1gInSc4	výstup
členové	člen	k1gMnPc1	člen
československé	československý	k2eAgFnSc2d1	Československá
výpravy	výprava	k1gFnSc2	výprava
Zoltán	Zoltán	k1gMnSc1	Zoltán
Demján	Demján	k1gMnSc1	Demján
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
Božík	Božík	k1gMnSc1	Božík
a	a	k8xC	a
Josef	Josef	k1gMnSc1	Josef
Rakoncaj	Rakoncaj	k1gMnSc1	Rakoncaj
alpským	alpský	k2eAgInSc7d1	alpský
stylem	styl	k1gInSc7	styl
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pěti	pět	k4xCc6	pět
dnech	den	k1gInPc6	den
výstup	výstup	k1gInSc4	výstup
přerušilo	přerušit	k5eAaPmAgNnS	přerušit
špatné	špatný	k2eAgNnSc1d1	špatné
počasí	počasí	k1gNnSc1	počasí
v	v	k7c6	v
6800	[number]	k4	6800
<g/>
m.	m.	k?	m.
</s>
</p>
<p>
<s>
Cestu	cesta	k1gFnSc4	cesta
JZ	JZ	kA	JZ
pilířem	pilíř	k1gInSc7	pilíř
zopakovali	zopakovat	k5eAaPmAgMnP	zopakovat
a	a	k8xC	a
prodloužili	prodloužit	k5eAaPmAgMnP	prodloužit
až	až	k6eAd1	až
k	k	k7c3	k
vrcholu	vrchol	k1gInSc3	vrchol
hory	hora	k1gFnSc2	hora
horolezci	horolezec	k1gMnPc1	horolezec
z	z	k7c2	z
Kazachstánu	Kazachstán	k1gInSc2	Kazachstán
a	a	k8xC	a
Slovenska	Slovensko	k1gNnSc2	Slovensko
Jurij	Jurij	k1gMnSc1	Jurij
Mojsejev	Mojsejev	k1gMnSc1	Mojsejev
<g/>
,	,	kIx,	,
Kazbek	Kazbek	k1gMnSc1	Kazbek
Valijev	Valijev	k1gMnSc1	Valijev
a	a	k8xC	a
Zoltán	Zoltán	k1gMnSc1	Zoltán
Demján	Demján	k1gMnSc1	Demján
alpským	alpský	k2eAgInSc7d1	alpský
stylem	styl	k1gInSc7	styl
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
výstupu	výstup	k1gInSc2	výstup
lezli	lézt	k5eAaImAgMnP	lézt
obtíže	obtíž	k1gFnPc4	obtíž
VI	VI	kA	VI
<g/>
+	+	kIx~	+
A2	A2	k1gMnSc1	A2
klasifikace	klasifikace	k1gFnSc2	klasifikace
UIAA	UIAA	kA	UIAA
<g/>
,	,	kIx,	,
výstup	výstup	k1gInSc1	výstup
trval	trvat	k5eAaImAgInS	trvat
16	[number]	k4	16
dní	den	k1gInPc2	den
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejnáročnější	náročný	k2eAgInPc4d3	nejnáročnější
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1981	[number]	k4	1981
-	-	kIx~	-
Čtveřice	čtveřice	k1gFnSc1	čtveřice
horolezců	horolezec	k1gMnPc2	horolezec
z	z	k7c2	z
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
zlézá	zlézat	k5eAaImIp3nS	zlézat
poprvé	poprvé	k6eAd1	poprvé
J	J	kA	J
stěnu	stěn	k1gInSc6	stěn
<g/>
.	.	kIx.	.
</s>
<s>
Postupují	postupovat	k5eAaImIp3nP	postupovat
v	v	k7c6	v
pravé	pravý	k2eAgFnSc6d1	pravá
části	část	k1gFnSc6	část
stěny	stěna	k1gFnSc2	stěna
osm	osm	k4xCc1	osm
dní	den	k1gInPc2	den
alpským	alpský	k2eAgInSc7d1	alpský
stylem	styl	k1gInSc7	styl
<g/>
.	.	kIx.	.
</s>
<s>
Výstup	výstup	k1gInSc4	výstup
ukončují	ukončovat	k5eAaImIp3nP	ukončovat
na	na	k7c6	na
vrcholovém	vrcholový	k2eAgNnSc6d1	vrcholové
hřebeni	hřeben	k1gInSc6	hřeben
v	v	k7c4	v
8000	[number]	k4	8000
<g/>
m	m	kA	m
a	a	k8xC	a
sestupují	sestupovat	k5eAaImIp3nP	sestupovat
klasickou	klasický	k2eAgFnSc7d1	klasická
cestou	cesta	k1gFnSc7	cesta
<g/>
.1982	.1982	k4	.1982
-	-	kIx~	-
Cesta	cesta	k1gFnSc1	cesta
přes	přes	k7c4	přes
Hrušku	hruška	k1gFnSc4	hruška
<g/>
.	.	kIx.	.
</s>
<s>
Japonská	japonský	k2eAgFnSc1d1	japonská
expedice	expedice	k1gFnSc1	expedice
stoupá	stoupat	k5eAaImIp3nS	stoupat
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
trasou	trasa	k1gFnSc7	trasa
zkoušenou	zkoušený	k2eAgFnSc7d1	zkoušená
v	v	k7c4	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
v	v	k7c6	v
pravé	pravý	k2eAgFnSc6d1	pravá
části	část	k1gFnSc6	část
S	s	k7c7	s
stěny	stěn	k1gInPc7	stěn
<g/>
.1984	.1984	k4	.1984
-	-	kIx~	-
Z	Z	kA	Z
stěna	stěna	k1gFnSc1	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Československá	československý	k2eAgFnSc1d1	Československá
expedice	expedice	k1gFnSc1	expedice
vedená	vedený	k2eAgFnSc1d1	vedená
Jiřím	Jiří	k1gMnSc7	Jiří
Novákem	Novák	k1gMnSc7	Novák
otevírá	otevírat	k5eAaImIp3nS	otevírat
novou	nový	k2eAgFnSc4d1	nová
cestu	cesta	k1gFnSc4	cesta
4500	[number]	k4	4500
<g/>
m	m	kA	m
vysokou	vysoký	k2eAgFnSc7d1	vysoká
Z	Z	kA	Z
stěnou	stěna	k1gFnSc7	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Výstup	výstup	k1gInSc1	výstup
vede	vést	k5eAaImIp3nS	vést
levým	levý	k2eAgInSc7d1	levý
pilířem	pilíř	k1gInSc7	pilíř
stěny	stěna	k1gFnSc2	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pokusu	pokus	k1gInSc6	pokus
Miroslava	Miroslav	k1gMnSc2	Miroslav
Šmída	Šmíd	k1gMnSc2	Šmíd
(	(	kIx(	(
<g/>
7900	[number]	k4	7900
<g/>
m	m	kA	m
<g/>
)	)	kIx)	)
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
Jarýk	Jarýk	k1gMnSc1	Jarýk
Stejskal	Stejskal	k1gMnSc1	Stejskal
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Jakeš	Jakeš	k1gMnSc1	Jakeš
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
Šimon	Šimon	k1gMnSc1	Šimon
(	(	kIx(	(
<g/>
ztratil	ztratit	k5eAaPmAgMnS	ztratit
se	se	k3xPyFc4	se
během	během	k7c2	během
sestupu	sestup	k1gInSc2	sestup
<g/>
)	)	kIx)	)
<g/>
.1985	.1985	k4	.1985
-	-	kIx~	-
Z	Z	kA	Z
stěna	stěna	k1gFnSc1	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
československá	československý	k2eAgFnSc1d1	Československá
expedice	expedice	k1gFnSc1	expedice
zlézá	zlézat	k5eAaImIp3nS	zlézat
ještě	ještě	k6eAd1	ještě
obtížnější	obtížný	k2eAgInSc4d2	obtížnější
pravý	pravý	k2eAgInSc4d1	pravý
pilíř	pilíř	k1gInSc4	pilíř
Z	z	k7c2	z
stěny	stěna	k1gFnSc2	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Výstup	výstup	k1gInSc1	výstup
končí	končit	k5eAaImIp3nS	končit
na	na	k7c6	na
vrcholovém	vrcholový	k2eAgInSc6d1	vrcholový
hřebeni	hřeben	k1gInSc6	hřeben
v	v	k7c6	v
7300	[number]	k4	7300
<g/>
m.	m.	k?	m.
Velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
sněhu	sníh	k1gInSc2	sníh
na	na	k7c6	na
jinak	jinak	k6eAd1	jinak
snadné	snadný	k2eAgFnSc6d1	snadná
podvrcholové	podvrcholový	k2eAgFnSc6d1	podvrcholová
plošině	plošina	k1gFnSc6	plošina
neumožnilo	umožnit	k5eNaPmAgNnS	umožnit
pokračovat	pokračovat	k5eAaImF	pokračovat
dál	daleko	k6eAd2	daleko
<g/>
.1986	.1986	k4	.1986
-	-	kIx~	-
J	J	kA	J
pilíř	pilíř	k1gInSc1	pilíř
<g/>
.	.	kIx.	.
</s>
<s>
Polská	polský	k2eAgFnSc1d1	polská
expedice	expedice	k1gFnSc1	expedice
zlézá	zlézat	k5eAaImIp3nS	zlézat
další	další	k2eAgFnSc4d1	další
náročnou	náročný	k2eAgFnSc4d1	náročná
cestu	cesta	k1gFnSc4	cesta
v	v	k7c6	v
levé	levý	k2eAgFnSc6d1	levá
části	část	k1gFnSc6	část
jižní	jižní	k2eAgFnSc2d1	jižní
stěny	stěna	k1gFnSc2	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Čechy	Čech	k1gMnPc4	Čech
a	a	k8xC	a
Slováky	Slovák	k1gMnPc4	Slovák
předchozího	předchozí	k2eAgInSc2d1	předchozí
roku	rok	k1gInSc2	rok
i	i	k9	i
je	být	k5eAaImIp3nS	být
zastavil	zastavit	k5eAaPmAgMnS	zastavit
hluboký	hluboký	k2eAgInSc4d1	hluboký
sníh	sníh	k1gInSc4	sníh
na	na	k7c6	na
podvrcholové	podvrcholový	k2eAgFnSc6d1	podvrcholová
plošině	plošina	k1gFnSc6	plošina
JZ	JZ	kA	JZ
hřebene	hřeben	k1gInSc2	hřeben
<g/>
.	.	kIx.	.
</s>
<s>
Polský	polský	k2eAgInSc1d1	polský
výstup	výstup	k1gInSc1	výstup
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
završen	završit	k5eAaPmNgInS	završit
v	v	k7c6	v
7500	[number]	k4	7500
<g/>
m	m	kA	m
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
J	J	kA	J
pilíř	pilíř	k1gInSc4	pilíř
kulminuje	kulminovat	k5eAaImIp3nS	kulminovat
<g/>
.1986	.1986	k4	.1986
-	-	kIx~	-
Slovinec	Slovinec	k1gMnSc1	Slovinec
Stane	stanout	k5eAaPmIp3nS	stanout
Belak	Belak	k1gInSc1	Belak
zlézá	zlézat	k5eAaImIp3nS	zlézat
novou	nový	k2eAgFnSc4d1	nová
cestu	cesta	k1gFnSc4	cesta
v	v	k7c6	v
pravé	pravý	k2eAgFnSc6d1	pravá
části	část	k1gFnSc6	část
V	v	k7c4	v
stěny	stěna	k1gFnPc4	stěna
sólo	sólo	k1gNnSc1	sólo
<g/>
.1990	.1990	k4	.1990
-	-	kIx~	-
Krzysztof	Krzysztof	k1gInSc1	Krzysztof
Wielicki	Wielick	k1gFnSc2	Wielick
z	z	k7c2	z
Polska	Polsko	k1gNnSc2	Polsko
zlézá	zlézat	k5eAaImIp3nS	zlézat
Dhaulágirí	Dhaulágirí	k2eAgFnSc7d1	Dhaulágirí
novou	nový	k2eAgFnSc7d1	nová
cestou	cesta	k1gFnSc7	cesta
V	V	kA	V
stěnou	stěna	k1gFnSc7	stěna
sólo	sólo	k1gNnSc1	sólo
alpským	alpský	k2eAgInSc7d1	alpský
stylem	styl	k1gInSc7	styl
za	za	k7c4	za
16	[number]	k4	16
hodin	hodina	k1gFnPc2	hodina
<g/>
.1991	.1991	k4	.1991
-	-	kIx~	-
Kazašská	kazašský	k2eAgFnSc1d1	kazašská
expedice	expedice	k1gFnSc1	expedice
otevírá	otevírat	k5eAaImIp3nS	otevírat
cestu	cesta	k1gFnSc4	cesta
prostředním	prostřední	k2eAgInSc7d1	prostřední
pilířem	pilíř	k1gInSc7	pilíř
v	v	k7c6	v
Z	Z	kA	Z
stěně	stěna	k1gFnSc6	stěna
<g/>
.1993	.1993	k4	.1993
-	-	kIx~	-
S	s	k7c7	s
stěna	stěna	k1gFnSc1	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Rusko-britská	ruskoritský	k2eAgFnSc1d1	rusko-britský
expedice	expedice	k1gFnSc1	expedice
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
na	na	k7c4	na
Dhaulágirí	Dhaulágirí	k1gFnSc4	Dhaulágirí
středem	střed	k1gInSc7	střed
S	s	k7c7	s
stěny	stěn	k1gInPc7	stěn
<g/>
.1999	.1999	k4	.1999
-	-	kIx~	-
Slovinec	Slovinec	k1gMnSc1	Slovinec
Tomaz	Tomaz	k1gInSc4	Tomaz
Humar	Humar	k1gInSc1	Humar
stoupá	stoupat	k5eAaImIp3nS	stoupat
na	na	k7c4	na
Dhaulágirí	Dhaulágirí	k1gNnSc4	Dhaulágirí
středem	středem	k7c2	středem
J	J	kA	J
stěny	stěna	k1gFnSc2	stěna
sólo	sólo	k2eAgInSc2d1	sólo
alpským	alpský	k2eAgInSc7d1	alpský
stylem	styl	k1gInSc7	styl
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c4	na
Dhaulágirí	Dhaulágirí	k1gFnSc4	Dhaulágirí
dnes	dnes	k6eAd1	dnes
vede	vést	k5eAaImIp3nS	vést
15	[number]	k4	15
cest	cesta	k1gFnPc2	cesta
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
tři	tři	k4xCgInPc1	tři
(	(	kIx(	(
<g/>
zatím	zatím	k6eAd1	zatím
<g/>
)	)	kIx)	)
nekončí	končit	k5eNaImIp3nS	končit
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
hory	hora	k1gFnSc2	hora
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
vrcholových	vrcholový	k2eAgInPc6d1	vrcholový
hřebenech	hřeben	k1gInPc6	hřeben
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
zvykem	zvyk	k1gInSc7	zvyk
například	například	k6eAd1	například
v	v	k7c6	v
Alpách	Alpy	k1gFnPc6	Alpy
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
končí	končit	k5eAaImIp3nS	končit
lezecké	lezecký	k2eAgFnPc4d1	lezecká
obtíže	obtíž	k1gFnPc4	obtíž
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Dhaulágirí	Dhaulágirí	k1gFnSc6	Dhaulágirí
bylo	být	k5eAaImAgNnS	být
završeno	završit	k5eAaPmNgNnS	završit
nejvíce	nejvíce	k6eAd1	nejvíce
výstupů	výstup	k1gInPc2	výstup
a	a	k8xC	a
prvovýstupů	prvovýstup	k1gInPc2	prvovýstup
alpským	alpský	k2eAgInSc7d1	alpský
stylem	styl	k1gInSc7	styl
(	(	kIx(	(
<g/>
i	i	k8xC	i
zimních	zimní	k2eAgFnPc2d1	zimní
<g/>
)	)	kIx)	)
jinou	jiný	k2eAgFnSc7d1	jiná
než	než	k8xS	než
klasickou	klasický	k2eAgFnSc7d1	klasická
trasou	trasa	k1gFnSc7	trasa
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
všechny	všechen	k3xTgFnPc4	všechen
cesty	cesta	k1gFnPc4	cesta
na	na	k7c6	na
Dhaulágirí	Dhaulágirí	k1gFnSc6	Dhaulágirí
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
klasické	klasický	k2eAgFnSc2d1	klasická
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
až	až	k9	až
mimořádně	mimořádně	k6eAd1	mimořádně
technicky	technicky	k6eAd1	technicky
obtížné	obtížný	k2eAgNnSc1d1	obtížné
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
nebyla	být	k5eNaImAgFnS	být
nikdy	nikdy	k6eAd1	nikdy
opakována	opakován	k2eAgFnSc1d1	opakována
<g/>
.	.	kIx.	.
</s>
<s>
Nezlezenou	zlezený	k2eNgFnSc7d1	zlezený
logickou	logický	k2eAgFnSc7d1	logická
linií	linie	k1gFnSc7	linie
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
SZ	SZ	kA	SZ
hřeben	hřeben	k1gInSc4	hřeben
hory	hora	k1gFnSc2	hora
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výstupy	výstup	k1gInPc4	výstup
českých	český	k2eAgMnPc2d1	český
horolezců	horolezec	k1gMnPc2	horolezec
==	==	k?	==
</s>
</p>
<p>
<s>
1984	[number]	k4	1984
-	-	kIx~	-
Jan	Jan	k1gMnSc1	Jan
Šimon	Šimon	k1gMnSc1	Šimon
(	(	kIx(	(
<g/>
zahynul	zahynout	k5eAaPmAgMnS	zahynout
při	při	k7c6	při
sestupu	sestup	k1gInSc6	sestup
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Jakeš	Jakeš	k1gMnSc1	Jakeš
<g/>
,	,	kIx,	,
Jarýk	Jarýk	k1gMnSc1	Jarýk
Stejskal	Stejskal	k1gMnSc1	Stejskal
-	-	kIx~	-
prvovýstup	prvovýstup	k1gInSc1	prvovýstup
západní	západní	k2eAgFnSc2d1	západní
stěnou	stěna	k1gFnSc7	stěna
</s>
</p>
<p>
<s>
1990	[number]	k4	1990
-	-	kIx~	-
Leopold	Leopold	k1gMnSc1	Leopold
Sulovský	Sulovský	k1gMnSc1	Sulovský
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
Šilhán	šilhán	k2eAgMnSc1d1	šilhán
-	-	kIx~	-
klasickou	klasický	k2eAgFnSc7d1	klasická
cestou	cesta	k1gFnSc7	cesta
SV	sv	kA	sv
hřebenem	hřeben	k1gInSc7	hřeben
</s>
</p>
<p>
<s>
2008	[number]	k4	2008
-	-	kIx~	-
Martin	Martin	k1gMnSc1	Martin
Minařík	Minařík	k1gMnSc1	Minařík
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
<g/>
;	;	kIx,	;
Radek	Radek	k1gMnSc1	Radek
Jaroš	Jaroš	k1gMnSc1	Jaroš
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Hrubý	Hrubý	k1gMnSc1	Hrubý
[	[	kIx(	[
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
2009	[number]	k4	2009
-	-	kIx~	-
David	David	k1gMnSc1	David
Fojtík	Fojtík	k1gMnSc1	Fojtík
[	[	kIx(	[
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Miroslav	Miroslav	k1gMnSc1	Miroslav
Šmíd	Šmíd	k1gMnSc1	Šmíd
<g/>
:	:	kIx,	:
Dva	dva	k4xCgInPc4	dva
kroky	krok	k1gInPc4	krok
od	od	k7c2	od
vrcholu	vrchol	k1gInSc2	vrchol
<g/>
:	:	kIx,	:
horolezecká	horolezecký	k2eAgFnSc1d1	horolezecká
expedice	expedice	k1gFnSc1	expedice
Dhaulágiri	Dhaulágiri	k1gNnSc1	Dhaulágiri
1984	[number]	k4	1984
<g/>
;	;	kIx,	;
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
204	[number]	k4	204
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
88	[number]	k4	88
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
159	[number]	k4	159
s.	s.	k?	s.
</s>
</p>
<p>
<s>
Max	Max	k1gMnSc1	Max
Eiselin	Eiselin	k2eAgMnSc1d1	Eiselin
<g/>
:	:	kIx,	:
Erfolg	Erfolg	k1gInSc1	Erfolg
am	am	k?	am
Dhaulágiri	Dhaulágir	k1gFnSc2	Dhaulágir
<g/>
;	;	kIx,	;
Orell	Orell	k1gMnSc1	Orell
Füssli	Füssli	k1gMnSc1	Füssli
Verlag	Verlag	k1gMnSc1	Verlag
<g/>
,	,	kIx,	,
Zürich	Zürich	k1gMnSc1	Zürich
<g/>
,	,	kIx,	,
1960	[number]	k4	1960
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Max	Max	k1gMnSc1	Max
Eiselin	Eiselin	k2eAgMnSc1d1	Eiselin
<g/>
:	:	kIx,	:
Úspech	Úspech	k1gInSc1	Úspech
na	na	k7c4	na
Dhaulágiri	Dhaulágiri	k1gNnSc4	Dhaulágiri
<g/>
;	;	kIx,	;
Šport	Šport	k1gInSc1	Šport
<g/>
,	,	kIx,	,
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
,	,	kIx,	,
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Štefan	Štefan	k1gMnSc1	Štefan
Kýška	Kýška	k1gMnSc1	Kýška
<g/>
,	,	kIx,	,
77	[number]	k4	77
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
62	[number]	k4	62
<g/>
,	,	kIx,	,
183	[number]	k4	183
s.	s.	k?	s.
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Max	Max	k1gMnSc1	Max
Eiselin	Eiselin	k2eAgMnSc1d1	Eiselin
<g/>
:	:	kIx,	:
Úspěch	úspěch	k1gInSc1	úspěch
na	na	k7c4	na
Dhaulágiri	Dhaulágiri	k1gNnSc4	Dhaulágiri
<g/>
:	:	kIx,	:
prvovýstup	prvovýstup	k1gInSc1	prvovýstup
švýcarské	švýcarský	k2eAgFnSc2d1	švýcarská
himálajské	himálajský	k2eAgFnSc2d1	himálajská
expedice	expedice	k1gFnSc2	expedice
1960	[number]	k4	1960
na	na	k7c4	na
osmitisícový	osmitisícový	k2eAgInSc4d1	osmitisícový
vrchol	vrchol	k1gInSc4	vrchol
<g/>
;	;	kIx,	;
STN	STN	kA	STN
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Arnošt	Arnošt	k1gMnSc1	Arnošt
Černík	Černík	k1gMnSc1	Černík
<g/>
,	,	kIx,	,
27	[number]	k4	27
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
45	[number]	k4	45
<g/>
-	-	kIx~	-
<g/>
62	[number]	k4	62
<g/>
,	,	kIx,	,
208	[number]	k4	208
<g/>
+	+	kIx~	+
<g/>
24	[number]	k4	24
s.	s.	k?	s.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
hor	hora	k1gFnPc2	hora
světa	svět	k1gInSc2	svět
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Dhaulágirí	Dhaulágirí	k1gFnSc2	Dhaulágirí
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Dhaulágirí	Dhaulágirí	k1gFnSc2	Dhaulágirí
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
