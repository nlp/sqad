<p>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
kosmická	kosmický	k2eAgFnSc1d1	kosmická
agentura	agentura	k1gFnSc1	agentura
(	(	kIx(	(
<g/>
European	European	k1gInSc1	European
Space	Space	k1gFnSc2	Space
Agency	Agenca	k1gFnSc2	Agenca
<g/>
,	,	kIx,	,
ESA	eso	k1gNnSc2	eso
<g/>
;	;	kIx,	;
Agence	agence	k1gFnSc1	agence
spatiale	spatiale	k6eAd1	spatiale
européenne	européennout	k5eAaImIp3nS	européennout
<g/>
,	,	kIx,	,
ASE	as	k1gInSc5	as
<g/>
)	)	kIx)	)
je	on	k3xPp3gFnPc4	on
mezivládní	mezivládní	k2eAgFnPc4d1	mezivládní
organizace	organizace	k1gFnPc4	organizace
pro	pro	k7c4	pro
využití	využití	k1gNnSc4	využití
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
22	[number]	k4	22
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
včetně	včetně	k7c2	včetně
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Sídlo	sídlo	k1gNnSc4	sídlo
jejího	její	k3xOp3gNnSc2	její
ředitelství	ředitelství	k1gNnSc2	ředitelství
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
a	a	k8xC	a
její	její	k3xOp3gNnPc1	její
jednotlivá	jednotlivý	k2eAgNnPc1d1	jednotlivé
centra	centrum	k1gNnPc1	centrum
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
zbytku	zbytek	k1gInSc6	zbytek
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Zaměstnává	zaměstnávat	k5eAaImIp3nS	zaměstnávat
zhruba	zhruba	k6eAd1	zhruba
2	[number]	k4	2
000	[number]	k4	000
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
subdodavatelů	subdodavatel	k1gMnPc2	subdodavatel
a	a	k8xC	a
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
státních	státní	k2eAgFnPc2d1	státní
kosmických	kosmický	k2eAgFnPc2d1	kosmická
agentur	agentura	k1gFnPc2	agentura
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
má	mít	k5eAaImIp3nS	mít
roční	roční	k2eAgInSc1d1	roční
rozpočet	rozpočet	k1gInSc1	rozpočet
přibližně	přibližně	k6eAd1	přibližně
3	[number]	k4	3
miliardy	miliarda	k4xCgFnSc2	miliarda
€	€	k?	€
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kosmodromem	kosmodrom	k1gInSc7	kosmodrom
ESA	eso	k1gNnSc2	eso
je	být	k5eAaImIp3nS	být
Guyanské	Guyanský	k2eAgNnSc1d1	Guyanský
kosmické	kosmický	k2eAgNnSc1d1	kosmické
centrum	centrum	k1gNnSc1	centrum
v	v	k7c6	v
Kourou	Kourou	k?	Kourou
v	v	k7c6	v
jihoamerické	jihoamerický	k2eAgFnSc6d1	jihoamerická
Francouzské	francouzský	k2eAgFnSc6d1	francouzská
Guyaně	Guyana	k1gFnSc6	Guyana
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
jejíž	jejíž	k3xOyRp3gFnSc3	jejíž
blízkosti	blízkost	k1gFnSc3	blízkost
k	k	k7c3	k
rovníku	rovník	k1gInSc3	rovník
může	moct	k5eAaImIp3nS	moct
snadno	snadno	k6eAd1	snadno
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
komerčně	komerčně	k6eAd1	komerčně
důležité	důležitý	k2eAgFnPc4d1	důležitá
oběžné	oběžný	k2eAgFnPc4d1	oběžná
dráhy	dráha	k1gFnPc4	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
spolehlivému	spolehlivý	k2eAgMnSc3d1	spolehlivý
a	a	k8xC	a
výkonnému	výkonný	k2eAgInSc3d1	výkonný
nosiči	nosič	k1gInSc3	nosič
Ariane	Arian	k1gMnSc5	Arian
4	[number]	k4	4
ESA	eso	k1gNnPc1	eso
získala	získat	k5eAaPmAgFnS	získat
důležité	důležitý	k2eAgNnSc4d1	důležité
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
kosmických	kosmický	k2eAgMnPc2d1	kosmický
dopravců	dopravce	k1gMnPc2	dopravce
posílené	posílená	k1gFnSc2	posílená
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
novou	nový	k2eAgFnSc7d1	nová
výkonnou	výkonný	k2eAgFnSc7d1	výkonná
raketou	raketa	k1gFnSc7	raketa
Ariane	Arian	k1gMnSc5	Arian
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
ESA	eso	k1gNnSc2	eso
stala	stát	k5eAaPmAgFnS	stát
druhým	druhý	k4xOgNnSc7	druhý
nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
hráčem	hráč	k1gMnSc7	hráč
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
kosmického	kosmický	k2eAgInSc2d1	kosmický
výzkumu	výzkum	k1gInSc2	výzkum
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
NASA	NASA	kA	NASA
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
také	také	k9	také
čtvrtá	čtvrtý	k4xOgFnSc1	čtvrtý
největší	veliký	k2eAgFnSc1d3	veliký
kosmická	kosmický	k2eAgFnSc1d1	kosmická
agentura	agentura	k1gFnSc1	agentura
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
ESA	eso	k1gNnSc2	eso
byla	být	k5eAaImAgFnS	být
zřízena	zřídit	k5eAaPmNgFnS	zřídit
Úmluvou	úmluva	k1gFnSc7	úmluva
o	o	k7c4	o
založení	založení	k1gNnSc4	založení
ESA	eso	k1gNnSc2	eso
ze	z	k7c2	z
dne	den	k1gInSc2	den
30	[number]	k4	30
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1975	[number]	k4	1975
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nové	nový	k2eAgFnSc6d1	nová
agentuře	agentura	k1gFnSc6	agentura
byly	být	k5eAaImAgFnP	být
sjednoceny	sjednotit	k5eAaPmNgFnP	sjednotit
Evropská	evropský	k2eAgFnSc1d1	Evropská
organizace	organizace	k1gFnSc1	organizace
pro	pro	k7c4	pro
výzkum	výzkum	k1gInSc4	výzkum
vesmíru	vesmír	k1gInSc2	vesmír
(	(	kIx(	(
<g/>
ESRO	ESRO	kA	ESRO
–	–	k?	–
European	European	k1gMnSc1	European
Space	Space	k1gFnSc2	Space
Research	Research	k1gMnSc1	Research
Organisation	Organisation	k1gInSc1	Organisation
<g/>
)	)	kIx)	)
a	a	k8xC	a
Evropská	evropský	k2eAgFnSc1d1	Evropská
organizace	organizace	k1gFnSc1	organizace
pro	pro	k7c4	pro
vývoj	vývoj	k1gInSc4	vývoj
startovacích	startovací	k2eAgNnPc2d1	startovací
zařízení	zařízení	k1gNnPc2	zařízení
(	(	kIx(	(
<g/>
ELDO	ELDO	kA	ELDO
–	–	k?	–
European	European	k1gMnSc1	European
Launch	Launch	k1gMnSc1	Launch
Development	Development	k1gMnSc1	Development
Organisation	Organisation	k1gInSc1	Organisation
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
její	její	k3xOp3gFnSc1	její
součástí	součást	k1gFnSc7	součást
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Evropské	evropský	k2eAgNnSc1d1	Evropské
centrum	centrum	k1gNnSc1	centrum
výzkumu	výzkum	k1gInSc2	výzkum
vesmíru	vesmír	k1gInSc2	vesmír
a	a	k8xC	a
technologií	technologie	k1gFnPc2	technologie
(	(	kIx(	(
<g/>
ESTEC	ESTEC	kA	ESTEC
-	-	kIx~	-
European	European	k1gMnSc1	European
Space	Space	k1gMnSc1	Space
Research	Research	k1gMnSc1	Research
and	and	k?	and
Technology	technolog	k1gMnPc4	technolog
Centre	centr	k1gInSc5	centr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Členské	členský	k2eAgInPc1d1	členský
státy	stát	k1gInPc1	stát
<g/>
,	,	kIx,	,
rozpočet	rozpočet	k1gInSc1	rozpočet
a	a	k8xC	a
organizační	organizační	k2eAgFnSc1d1	organizační
struktura	struktura	k1gFnSc1	struktura
==	==	k?	==
</s>
</p>
<p>
<s>
Členy	člen	k1gInPc4	člen
ESA	eso	k1gNnSc2	eso
je	být	k5eAaImIp3nS	být
dvacet	dvacet	k4xCc4	dvacet
dva	dva	k4xCgInPc4	dva
států	stát	k1gInPc2	stát
<g/>
:	:	kIx,	:
Belgie	Belgie	k1gFnSc1	Belgie
<g/>
,	,	kIx,	,
Česko	Česko	k1gNnSc1	Česko
<g/>
,	,	kIx,	,
Dánsko	Dánsko	k1gNnSc1	Dánsko
<g/>
,	,	kIx,	,
Estonsko	Estonsko	k1gNnSc1	Estonsko
<g/>
,	,	kIx,	,
Finsko	Finsko	k1gNnSc1	Finsko
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
Irsko	Irsko	k1gNnSc1	Irsko
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
,	,	kIx,	,
Lucembursko	Lucembursko	k1gNnSc1	Lucembursko
<g/>
,	,	kIx,	,
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
<g/>
,	,	kIx,	,
Norsko	Norsko	k1gNnSc1	Norsko
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
,	,	kIx,	,
Řecko	Řecko	k1gNnSc1	Řecko
<g/>
,	,	kIx,	,
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
,	,	kIx,	,
Švédsko	Švédsko	k1gNnSc1	Švédsko
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
a	a	k8xC	a
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
a	a	k8xC	a
Lotyšsko	Lotyšsko	k1gNnSc1	Lotyšsko
mají	mít	k5eAaImIp3nP	mít
podepsanou	podepsaný	k2eAgFnSc4d1	podepsaná
s	s	k7c7	s
ESA	eso	k1gNnPc1	eso
smlouvu	smlouva	k1gFnSc4	smlouva
o	o	k7c4	o
spolupráci	spolupráce	k1gFnSc4	spolupráce
PECS	PECS	kA	PECS
(	(	kIx(	(
<g/>
trvání	trvání	k1gNnPc2	trvání
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
jim	on	k3xPp3gMnPc3	on
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
zapojení	zapojení	k1gNnSc4	zapojení
se	se	k3xPyFc4	se
do	do	k7c2	do
většiny	většina	k1gFnSc2	většina
programů	program	k1gInPc2	program
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
předstupněm	předstupeň	k1gInSc7	předstupeň
k	k	k7c3	k
plnému	plný	k2eAgNnSc3d1	plné
členství	členství	k1gNnSc3	členství
<g/>
.	.	kIx.	.
</s>
<s>
Česko	Česko	k1gNnSc1	Česko
začalo	začít	k5eAaPmAgNnS	začít
o	o	k7c6	o
plném	plný	k2eAgNnSc6d1	plné
členství	členství	k1gNnSc6	členství
v	v	k7c6	v
ESA	eso	k1gNnSc2	eso
vyjednávat	vyjednávat	k5eAaImF	vyjednávat
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2007	[number]	k4	2007
a	a	k8xC	a
stalo	stát	k5eAaPmAgNnS	stát
se	s	k7c7	s
plnohodnotným	plnohodnotný	k2eAgMnSc7d1	plnohodnotný
členem	člen	k1gMnSc7	člen
dne	den	k1gInSc2	den
12	[number]	k4	12
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
díky	dík	k1gInPc7	dík
své	svůj	k3xOyFgFnSc2	svůj
formální	formální	k2eAgFnSc2d1	formální
příslušnosti	příslušnost	k1gFnSc2	příslušnost
k	k	k7c3	k
britskému	britský	k2eAgNnSc3d1	Britské
Společenství	společenství	k1gNnSc3	společenství
národů	národ	k1gInPc2	národ
má	mít	k5eAaImIp3nS	mít
úzké	úzký	k2eAgInPc4d1	úzký
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
Evropou	Evropa	k1gFnSc7	Evropa
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1979	[number]	k4	1979
spolupracující	spolupracující	k2eAgFnSc7d1	spolupracující
zemí	zem	k1gFnSc7	zem
a	a	k8xC	a
zapojuje	zapojovat	k5eAaImIp3nS	zapojovat
se	se	k3xPyFc4	se
do	do	k7c2	do
vybraných	vybraný	k2eAgInPc2d1	vybraný
projektů	projekt	k1gInPc2	projekt
ESA	eso	k1gNnSc2	eso
na	na	k7c6	na
základě	základ	k1gInSc6	základ
vzájemných	vzájemný	k2eAgFnPc2d1	vzájemná
smluv	smlouva	k1gFnPc2	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
ESA	eso	k1gNnPc4	eso
také	také	k6eAd1	také
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
s	s	k7c7	s
Evropskou	evropský	k2eAgFnSc7d1	Evropská
unií	unie	k1gFnSc7	unie
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yIgFnPc4	který
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
formálně	formálně	k6eAd1	formálně
nezávislá	závislý	k2eNgFnSc1d1	nezávislá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozpočet	rozpočet	k1gInSc1	rozpočet
ESA	eso	k1gNnSc2	eso
na	na	k7c4	na
rok	rok	k1gInSc4	rok
2006	[number]	k4	2006
byl	být	k5eAaImAgInS	být
schválen	schválit	k5eAaPmNgInS	schválit
členskými	členský	k2eAgInPc7d1	členský
státy	stát	k1gInPc7	stát
na	na	k7c6	na
ministerské	ministerský	k2eAgFnSc6d1	ministerská
konferenci	konference	k1gFnSc6	konference
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2005	[number]	k4	2005
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
necelé	celý	k2eNgFnSc2d1	necelá
€	€	k?	€
<g/>
3	[number]	k4	3
miliardy	miliarda	k4xCgFnSc2	miliarda
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
o	o	k7c4	o
druhý	druhý	k4xOgInSc4	druhý
největší	veliký	k2eAgInSc4d3	veliký
rozpočet	rozpočet	k1gInSc4	rozpočet
kosmické	kosmický	k2eAgFnSc2d1	kosmická
agentury	agentura	k1gFnSc2	agentura
po	po	k7c6	po
NASA	NASA	kA	NASA
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
americkým	americký	k2eAgInSc7d1	americký
kosmickým	kosmický	k2eAgInSc7d1	kosmický
programem	program	k1gInSc7	program
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
jen	jen	k6eAd1	jen
čtvrtinový	čtvrtinový	k2eAgInSc1d1	čtvrtinový
(	(	kIx(	(
<g/>
rozpočet	rozpočet	k1gInSc1	rozpočet
NASA	NASA	kA	NASA
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
€	€	k?	€
<g/>
13	[number]	k4	13
miliard	miliarda	k4xCgFnPc2	miliarda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
má	mít	k5eAaImIp3nS	mít
ale	ale	k9	ale
většina	většina	k1gFnSc1	většina
členských	členský	k2eAgFnPc2d1	členská
zemí	zem	k1gFnPc2	zem
svůj	svůj	k3xOyFgInSc4	svůj
vlastní	vlastní	k2eAgInSc4d1	vlastní
kosmický	kosmický	k2eAgInSc4d1	kosmický
program	program	k1gInSc4	program
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
celkové	celkový	k2eAgInPc1d1	celkový
výdaje	výdaj	k1gInPc1	výdaj
na	na	k7c4	na
kosmický	kosmický	k2eAgInSc4d1	kosmický
výzkum	výzkum	k1gInSc4	výzkum
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
téměř	téměř	k6eAd1	téměř
dvojnásobné	dvojnásobný	k2eAgInPc1d1	dvojnásobný
a	a	k8xC	a
některé	některý	k3yIgInPc1	některý
projekty	projekt	k1gInPc1	projekt
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
např.	např.	kA	např.
navigační	navigační	k2eAgInSc4d1	navigační
systém	systém	k1gInSc4	systém
Galileo	Galilea	k1gFnSc5	Galilea
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
finančně	finančně	k6eAd1	finančně
podpořeny	podpořit	k5eAaPmNgInP	podpořit
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
se	se	k3xPyFc4	se
rozpočet	rozpočet	k1gInSc1	rozpočet
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
na	na	k7c4	na
3,9	[number]	k4	3,9
miliardy	miliarda	k4xCgFnSc2	miliarda
€	€	k?	€
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
podíl	podíl	k1gInSc1	podíl
byl	být	k5eAaImAgInS	být
10,4	[number]	k4	10,4
milionu	milion	k4xCgInSc2	milion
€	€	k?	€
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
rok	rok	k1gInSc4	rok
2015	[number]	k4	2015
byl	být	k5eAaImAgInS	být
sestaven	sestavit	k5eAaPmNgInS	sestavit
rozpočet	rozpočet	k1gInSc1	rozpočet
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
4,4	[number]	k4	4,4
miliardy	miliarda	k4xCgFnSc2	miliarda
€	€	k?	€
a	a	k8xC	a
příspěvek	příspěvek	k1gInSc1	příspěvek
ČR	ČR	kA	ČR
byl	být	k5eAaImAgInS	být
14,2	[number]	k4	14,2
milionu	milion	k4xCgInSc2	milion
€	€	k?	€
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Specializovaná	specializovaný	k2eAgNnPc1d1	specializované
střediska	středisko	k1gNnPc1	středisko
==	==	k?	==
</s>
</p>
<p>
<s>
Centrála	centrála	k1gFnSc1	centrála
agentury	agentura	k1gFnSc2	agentura
je	být	k5eAaImIp3nS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
ESA	eso	k1gNnPc1	eso
vybudovala	vybudovat	k5eAaPmAgNnP	vybudovat
postupem	postupem	k7c2	postupem
času	čas	k1gInSc2	čas
několik	několik	k4yIc4	několik
specializovaných	specializovaný	k2eAgNnPc2d1	specializované
středisek	středisko	k1gNnPc2	středisko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
ESTEC	ESTEC	kA	ESTEC
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
nizozemském	nizozemský	k2eAgInSc6d1	nizozemský
Noordwijku	Noordwijka	k1gFnSc4	Noordwijka
je	být	k5eAaImIp3nS	být
umístěno	umístit	k5eAaPmNgNnS	umístit
vývojové	vývojový	k2eAgNnSc1d1	vývojové
středisko	středisko	k1gNnSc1	středisko
pro	pro	k7c4	pro
satelitní	satelitní	k2eAgFnPc4d1	satelitní
technologie	technologie	k1gFnPc4	technologie
ESTEC	ESTEC	kA	ESTEC
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
European	European	k1gMnSc1	European
Space	Space	k1gMnSc1	Space
Research	Research	k1gMnSc1	Research
and	and	k?	and
Technology	technolog	k1gMnPc4	technolog
Centre	centr	k1gInSc5	centr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vzdáleno	vzdálit	k5eAaPmNgNnS	vzdálit
od	od	k7c2	od
amsterodamského	amsterodamský	k2eAgNnSc2d1	amsterodamské
letiště	letiště	k1gNnSc2	letiště
45	[number]	k4	45
minut	minuta	k1gFnPc2	minuta
jízdy	jízda	k1gFnSc2	jízda
autem	auto	k1gNnSc7	auto
<g/>
.	.	kIx.	.
</s>
<s>
Pracuje	pracovat	k5eAaImIp3nS	pracovat
zde	zde	k6eAd1	zde
1	[number]	k4	1
600	[number]	k4	600
odborníků	odborník	k1gMnPc2	odborník
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
je	být	k5eAaImIp3nS	být
pět	pět	k4xCc4	pět
Čechů	Čech	k1gMnPc2	Čech
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
byly	být	k5eAaImAgFnP	být
zkoušeny	zkoušen	k2eAgInPc1d1	zkoušen
dvě	dva	k4xCgFnPc1	dva
automatické	automatický	k2eAgFnPc1d1	automatická
lodě	loď	k1gFnPc1	loď
ATV	ATV	kA	ATV
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
dopravily	dopravit	k5eAaPmAgFnP	dopravit
materiál	materiál	k1gInSc4	materiál
na	na	k7c4	na
vesmírnou	vesmírný	k2eAgFnSc4d1	vesmírná
stanici	stanice	k1gFnSc4	stanice
ISS	ISS	kA	ISS
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
ESOC	ESOC	kA	ESOC
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
německém	německý	k2eAgInSc6d1	německý
Darmstadtu	Darmstadt	k1gInSc6	Darmstadt
je	být	k5eAaImIp3nS	být
umístěno	umístit	k5eAaPmNgNnS	umístit
Evropské	evropský	k2eAgNnSc1d1	Evropské
středisko	středisko	k1gNnSc1	středisko
vesmírných	vesmírný	k2eAgFnPc2d1	vesmírná
operací	operace	k1gFnPc2	operace
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
European	European	k1gInSc1	European
Space	Space	k1gMnSc1	Space
Operations	Operations	k1gInSc1	Operations
Centre	centr	k1gInSc5	centr
–	–	k?	–
ESOC	ESOC	kA	ESOC
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
jsou	být	k5eAaImIp3nP	být
řízeny	řízen	k2eAgFnPc4d1	řízena
družice	družice	k1gFnPc4	družice
a	a	k8xC	a
kosmické	kosmický	k2eAgFnPc4d1	kosmická
sondy	sonda	k1gFnPc4	sonda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
EAC	EAC	kA	EAC
===	===	k?	===
</s>
</p>
<p>
<s>
Středisko	středisko	k1gNnSc1	středisko
pro	pro	k7c4	pro
výcvik	výcvik	k1gInSc4	výcvik
evropských	evropský	k2eAgMnPc2d1	evropský
astronautů	astronaut	k1gMnPc2	astronaut
EAC	EAC	kA	EAC
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
European	European	k1gMnSc1	European
Astronaut	astronaut	k1gMnSc1	astronaut
Centre	centr	k1gInSc5	centr
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
rovněž	rovněž	k9	rovněž
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Kolín	Kolín	k1gInSc4	Kolín
nad	nad	k7c7	nad
Rýnem	Rýn	k1gInSc7	Rýn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
ESRIN	ESRIN	kA	ESRIN
===	===	k?	===
</s>
</p>
<p>
<s>
Nedaleko	nedaleko	k7c2	nedaleko
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Frascati	Frascati	k1gFnSc2	Frascati
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
výzkumné	výzkumný	k2eAgNnSc1d1	výzkumné
středisko	středisko	k1gNnSc1	středisko
ESRIN	ESRIN	kA	ESRIN
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
European	European	k1gMnSc1	European
Space	Space	k1gMnSc1	Space
Research	Research	k1gMnSc1	Research
Institute	institut	k1gInSc5	institut
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
úkolem	úkol	k1gInSc7	úkol
je	být	k5eAaImIp3nS	být
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
shromažďovat	shromažďovat	k5eAaImF	shromažďovat
<g/>
,	,	kIx,	,
ukládat	ukládat	k5eAaImF	ukládat
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
distribuovat	distribuovat	k5eAaBmF	distribuovat
data	datum	k1gNnPc4	datum
z	z	k7c2	z
družic	družice	k1gFnPc2	družice
a	a	k8xC	a
sond	sonda	k1gFnPc2	sonda
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
a	a	k8xC	a
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
uvedeno	uveden	k2eAgNnSc1d1	uvedeno
o	o	k7c4	o
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
převzalo	převzít	k5eAaPmAgNnS	převzít
funkci	funkce	k1gFnSc4	funkce
ústředního	ústřední	k2eAgNnSc2d1	ústřední
střediska	středisko	k1gNnSc2	středisko
pro	pro	k7c4	pro
dálkový	dálkový	k2eAgInSc4d1	dálkový
průzkum	průzkum	k1gInSc4	průzkum
naší	náš	k3xOp1gFnSc2	náš
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k6eAd1	mimo
to	ten	k3xDgNnSc1	ten
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
hospodářskou	hospodářský	k2eAgFnSc4d1	hospodářská
agendu	agenda	k1gFnSc4	agenda
ESA	eso	k1gNnSc2	eso
<g/>
,	,	kIx,	,
odtud	odtud	k6eAd1	odtud
jsou	být	k5eAaImIp3nP	být
zadávány	zadáván	k2eAgFnPc4d1	zadávána
soutěže	soutěž	k1gFnPc4	soutěž
<g/>
,	,	kIx,	,
zakázky	zakázka	k1gFnPc4	zakázka
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
spravována	spravován	k2eAgFnSc1d1	spravována
webová	webový	k2eAgFnSc1d1	webová
prezentace	prezentace	k1gFnSc1	prezentace
ESA	eso	k1gNnSc2	eso
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
starost	starost	k1gFnSc4	starost
vše	všechen	k3xTgNnSc1	všechen
kolem	kolem	k6eAd1	kolem
nové	nový	k2eAgFnPc4d1	nová
kosmické	kosmický	k2eAgFnPc4d1	kosmická
rakety	raketa	k1gFnPc4	raketa
VEGA	VEGA	kA	VEGA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
členem	člen	k1gInSc7	člen
==	==	k?	==
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
18	[number]	k4	18
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2008	[number]	k4	2008
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
hlasování	hlasování	k1gNnSc1	hlasování
o	o	k7c6	o
vstupu	vstup	k1gInSc6	vstup
Česka	Česko	k1gNnSc2	Česko
do	do	k7c2	do
ESA	eso	k1gNnSc2	eso
<g/>
,	,	kIx,	,
během	během	k7c2	během
kterého	který	k3yQgInSc2	který
byla	být	k5eAaImAgFnS	být
jednomyslně	jednomyslně	k6eAd1	jednomyslně
vyjádřena	vyjádřen	k2eAgFnSc1d1	vyjádřena
podpora	podpora	k1gFnSc1	podpora
jejímu	její	k3xOp3gInSc3	její
vstupu	vstup	k1gInSc3	vstup
do	do	k7c2	do
organizace	organizace	k1gFnSc2	organizace
<g/>
,	,	kIx,	,
do	do	k7c2	do
prosince	prosinec	k1gInSc2	prosinec
probíhala	probíhat	k5eAaImAgFnS	probíhat
ratifikace	ratifikace	k1gFnSc1	ratifikace
smluv	smlouva	k1gFnPc2	smlouva
a	a	k8xC	a
začleňování	začleňování	k1gNnSc2	začleňování
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
8	[number]	k4	8
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2008	[number]	k4	2008
byla	být	k5eAaImAgFnS	být
smlouva	smlouva	k1gFnSc1	smlouva
o	o	k7c4	o
přistoupení	přistoupení	k1gNnSc4	přistoupení
Česka	Česko	k1gNnSc2	Česko
k	k	k7c3	k
ESA	eso	k1gNnPc1	eso
podepsána	podepsán	k2eAgFnSc1d1	podepsána
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
ČR	ČR	kA	ČR
byl	být	k5eAaImAgInS	být
signatářem	signatář	k1gMnSc7	signatář
český	český	k2eAgMnSc1d1	český
premiér	premiér	k1gMnSc1	premiér
Mirek	Mirek	k1gMnSc1	Mirek
Topolánek	Topolánek	k1gMnSc1	Topolánek
<g/>
,	,	kIx,	,
za	za	k7c2	za
ESA	eso	k1gNnSc2	eso
její	její	k3xOp3gInSc4	její
generální	generální	k2eAgMnSc1d1	generální
ředitel	ředitel	k1gMnSc1	ředitel
Jean-Jacques	Jean-Jacques	k1gMnSc1	Jean-Jacques
Dordain	Dordain	k1gMnSc1	Dordain
<g/>
.	.	kIx.	.
</s>
<s>
Dohodu	dohoda	k1gFnSc4	dohoda
18	[number]	k4	18
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
2008	[number]	k4	2008
schválil	schválit	k5eAaPmAgInS	schválit
Senát	senát	k1gInSc1	senát
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
25	[number]	k4	25
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
také	také	k9	také
poslanecká	poslanecký	k2eAgFnSc1d1	Poslanecká
sněmovna	sněmovna	k1gFnSc1	sněmovna
a	a	k8xC	a
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2008	[number]	k4	2008
ji	on	k3xPp3gFnSc4	on
podepsal	podepsat	k5eAaPmAgInS	podepsat
prezident	prezident	k1gMnSc1	prezident
ČR	ČR	kA	ČR
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
12	[number]	k4	12
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2008	[number]	k4	2008
se	se	k3xPyFc4	se
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
stala	stát	k5eAaPmAgFnS	stát
plnohodnotným	plnohodnotný	k2eAgInSc7d1	plnohodnotný
členem	člen	k1gInSc7	člen
organizace	organizace	k1gFnSc2	organizace
uložením	uložení	k1gNnSc7	uložení
smlouvy	smlouva	k1gFnSc2	smlouva
na	na	k7c6	na
francouzském	francouzský	k2eAgNnSc6d1	francouzské
ministerstvu	ministerstvo	k1gNnSc6	ministerstvo
zahraničí	zahraničí	k1gNnSc2	zahraničí
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2008	[number]	k4	2008
byla	být	k5eAaImAgFnS	být
česká	český	k2eAgFnSc1d1	Česká
státní	státní	k2eAgFnSc1d1	státní
vlajka	vlajka	k1gFnSc1	vlajka
vztyčena	vztyčen	k2eAgFnSc1d1	vztyčena
po	po	k7c6	po
boku	bok	k1gInSc6	bok
vlajek	vlajka	k1gFnPc2	vlajka
ostatních	ostatní	k2eAgInPc2d1	ostatní
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
před	před	k7c7	před
budovami	budova	k1gFnPc7	budova
Evropské	evropský	k2eAgFnSc2d1	Evropská
kosmické	kosmický	k2eAgFnSc2d1	kosmická
agentury	agentura	k1gFnSc2	agentura
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
se	se	k3xPyFc4	se
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
podílela	podílet	k5eAaImAgFnS	podílet
na	na	k7c6	na
projektech	projekt	k1gInPc6	projekt
družic	družice	k1gFnPc2	družice
Swarm	Swarm	k1gInSc1	Swarm
<g/>
,	,	kIx,	,
Proba	Proba	k1gFnSc1	Proba
2	[number]	k4	2
a	a	k8xC	a
na	na	k7c6	na
přípravě	příprava	k1gFnSc6	příprava
atomových	atomový	k2eAgFnPc2d1	atomová
hodin	hodina	k1gFnPc2	hodina
pro	pro	k7c4	pro
ISS	ISS	kA	ISS
<g/>
.	.	kIx.	.
</s>
<s>
Trojice	trojice	k1gFnSc2	trojice
satelitů	satelit	k1gInPc2	satelit
Swarm	Swarm	k1gInSc1	Swarm
i	i	k8xC	i
s	s	k7c7	s
českými	český	k2eAgInPc7d1	český
přístroji	přístroj	k1gInPc7	přístroj
odstartovala	odstartovat	k5eAaPmAgFnS	odstartovat
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2013	[number]	k4	2013
na	na	k7c4	na
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Program	program	k1gInSc1	program
ESA	eso	k1gNnSc2	eso
==	==	k?	==
</s>
</p>
<p>
<s>
ESA	eso	k1gNnSc2	eso
vysílá	vysílat	k5eAaImIp3nS	vysílat
své	svůj	k3xOyFgMnPc4	svůj
kosmonauty	kosmonaut	k1gMnPc4	kosmonaut
na	na	k7c6	na
základě	základ	k1gInSc6	základ
smluv	smlouva	k1gFnPc2	smlouva
s	s	k7c7	s
Roskosmosem	Roskosmos	k1gInSc7	Roskosmos
loděmi	loď	k1gFnPc7	loď
Sojuz	Sojuz	k1gInSc1	Sojuz
k	k	k7c3	k
vesmírné	vesmírný	k2eAgFnSc3d1	vesmírná
stanici	stanice	k1gFnSc3	stanice
ISS	ISS	kA	ISS
<g/>
.	.	kIx.	.
</s>
<s>
Provozuje	provozovat	k5eAaImIp3nS	provozovat
rakety	raketa	k1gFnPc4	raketa
Ariane	Arian	k1gMnSc5	Arian
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vynášejí	vynášet	k5eAaImIp3nP	vynášet
kosmické	kosmický	k2eAgFnPc4d1	kosmická
družice	družice	k1gFnPc4	družice
na	na	k7c4	na
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Vypouštěla	vypouštět	k5eAaImAgFnS	vypouštět
ATV	ATV	kA	ATV
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
sloužila	sloužit	k5eAaImAgFnS	sloužit
jako	jako	k9	jako
zásobovací	zásobovací	k2eAgFnSc1d1	zásobovací
vesmírná	vesmírný	k2eAgFnSc1d1	vesmírná
loď	loď	k1gFnSc1	loď
pro	pro	k7c4	pro
ISS	ISS	kA	ISS
<g/>
.	.	kIx.	.
</s>
<s>
ESA	eso	k1gNnPc1	eso
společně	společně	k6eAd1	společně
s	s	k7c7	s
NASA	NASA	kA	NASA
provozovala	provozovat	k5eAaImAgFnS	provozovat
laboratoř	laboratoř	k1gFnSc4	laboratoř
Spacelab	Spacelaba	k1gFnPc2	Spacelaba
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
vynášel	vynášet	k5eAaImAgInS	vynášet
raketoplán	raketoplán	k1gInSc1	raketoplán
Space	Space	k1gFnSc2	Space
Shuttle	Shuttle	k1gFnSc2	Shuttle
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Moduly	modul	k1gInPc4	modul
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
kosmické	kosmický	k2eAgFnSc2d1	kosmická
stanice	stanice	k1gFnSc2	stanice
ISS	ISS	kA	ISS
===	===	k?	===
</s>
</p>
<p>
<s>
Columbus	Columbus	k1gInSc1	Columbus
–	–	k?	–
vědecký	vědecký	k2eAgInSc1d1	vědecký
modul	modul	k1gInSc1	modul
stanice	stanice	k1gFnSc2	stanice
ISS	ISS	kA	ISS
</s>
</p>
<p>
<s>
Automated	Automated	k1gInSc1	Automated
Transfer	transfer	k1gInSc1	transfer
Vehicle	Vehicle	k1gFnSc1	Vehicle
–	–	k?	–
automatická	automatický	k2eAgFnSc1d1	automatická
zásobovací	zásobovací	k2eAgFnSc1d1	zásobovací
kosmická	kosmický	k2eAgFnSc1d1	kosmická
loď	loď	k1gFnSc1	loď
</s>
</p>
<p>
<s>
ERA	ERA	kA	ERA
–	–	k?	–
robotická	robotický	k2eAgFnSc1d1	robotická
manipulační	manipulační	k2eAgFnSc1d1	manipulační
paže	paže	k1gFnSc1	paže
</s>
</p>
<p>
<s>
Cupola	Cupola	k1gFnSc1	Cupola
–	–	k?	–
prosklený	prosklený	k2eAgInSc4d1	prosklený
výklenek	výklenek	k1gInSc4	výklenek
umožňující	umožňující	k2eAgInSc4d1	umožňující
výhled	výhled	k1gInSc4	výhled
na	na	k7c6	na
ISS	ISS	kA	ISS
zvenku	zvenku	k6eAd1	zvenku
</s>
</p>
<p>
<s>
===	===	k?	===
Projekty	projekt	k1gInPc1	projekt
průzkumu	průzkum	k1gInSc2	průzkum
planety	planeta	k1gFnSc2	planeta
Země	zem	k1gFnSc2	zem
===	===	k?	===
</s>
</p>
<p>
<s>
Earth	Earth	k1gInSc1	Earth
explorers	explorers	k1gInSc1	explorers
-	-	kIx~	-
soubor	soubor	k1gInSc1	soubor
projektů	projekt	k1gInPc2	projekt
pro	pro	k7c4	pro
průzkum	průzkum	k1gInSc4	průzkum
planety	planeta	k1gFnSc2	planeta
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
CryoSat	CryoSat	k1gInSc1	CryoSat
-	-	kIx~	-
průzkum	průzkum	k1gInSc1	průzkum
a	a	k8xC	a
výzkum	výzkum	k1gInSc1	výzkum
kryosféry	kryosféra	k1gFnSc2	kryosféra
<g/>
,	,	kIx,	,
zmrzlé	zmrzlý	k2eAgFnSc2d1	zmrzlá
vody	voda	k1gFnSc2	voda
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
sněhu	sníh	k1gInSc2	sníh
<g/>
,	,	kIx,	,
ledovců	ledovec	k1gInPc2	ledovec
<g/>
,	,	kIx,	,
plovoucích	plovoucí	k2eAgFnPc2d1	plovoucí
ker	kra	k1gFnPc2	kra
či	či	k8xC	či
trvale	trvale	k6eAd1	trvale
zmrzlé	zmrzlý	k2eAgFnSc2d1	zmrzlá
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
GOCE	GOCE	kA	GOCE
-	-	kIx~	-
vytvoření	vytvoření	k1gNnSc1	vytvoření
modelu	model	k1gInSc2	model
gravitačního	gravitační	k2eAgNnSc2d1	gravitační
pole	pole	k1gNnSc2	pole
ve	v	k7c6	v
vysokém	vysoký	k2eAgNnSc6d1	vysoké
rozlišení	rozlišení	k1gNnSc6	rozlišení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ADM-Aeolus	ADM-Aeolus	k1gInSc1	ADM-Aeolus
-	-	kIx~	-
zmapování	zmapování	k1gNnSc1	zmapování
globálního	globální	k2eAgNnSc2d1	globální
rozložení	rozložení	k1gNnSc2	rozložení
a	a	k8xC	a
pohybu	pohyb	k1gInSc2	pohyb
vzdušných	vzdušný	k2eAgFnPc2d1	vzdušná
mas	masa	k1gFnPc2	masa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
EarthCARE	EarthCARE	k?	EarthCARE
-	-	kIx~	-
pro	pro	k7c4	pro
pochopení	pochopení	k1gNnSc4	pochopení
vztahů	vztah	k1gInPc2	vztah
mezi	mezi	k7c7	mezi
oblačností	oblačnost	k1gFnSc7	oblačnost
<g/>
,	,	kIx,	,
radiací	radiace	k1gFnSc7	radiace
a	a	k8xC	a
aerosolem	aerosol	k1gInSc7	aerosol
a	a	k8xC	a
k	k	k7c3	k
odhalení	odhalení	k1gNnSc3	odhalení
procesů	proces	k1gInPc2	proces
<g/>
,	,	kIx,	,
jakými	jaký	k3yIgInPc7	jaký
se	se	k3xPyFc4	se
podílejí	podílet	k5eAaImIp3nP	podílet
na	na	k7c6	na
globálním	globální	k2eAgNnSc6d1	globální
klimatu	klima	k1gNnSc6	klima
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Swarm	Swarm	k1gInSc1	Swarm
-	-	kIx~	-
průzkum	průzkum	k1gInSc1	průzkum
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Copernicus	Copernicus	k1gMnSc1	Copernicus
</s>
</p>
<p>
<s>
Sentinel-	Sentinel-	k?	Sentinel-
<g/>
1	[number]	k4	1
-	-	kIx~	-
průzkum	průzkum	k1gInSc1	průzkum
povrchu	povrch	k1gInSc2	povrch
Země	zem	k1gFnSc2	zem
za	za	k7c7	za
pomocí	pomoc	k1gFnSc7	pomoc
radaru	radar	k1gInSc2	radar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sentinel-	Sentinel-	k?	Sentinel-
<g/>
2	[number]	k4	2
-	-	kIx~	-
sledování	sledování	k1gNnSc2	sledování
stavu	stav	k1gInSc2	stav
lesů	les	k1gInPc2	les
a	a	k8xC	a
zemědělství	zemědělství	k1gNnSc2	zemědělství
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
multispektrální	multispektrální	k2eAgFnSc2d1	multispektrální
kamery	kamera	k1gFnSc2	kamera
s	s	k7c7	s
vysokým	vysoký	k2eAgNnSc7d1	vysoké
rozlišením	rozlišení	k1gNnSc7	rozlišení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sentinel-	Sentinel-	k?	Sentinel-
<g/>
3	[number]	k4	3
-	-	kIx~	-
průzkum	průzkum	k1gInSc4	průzkum
oceánů	oceán	k1gInPc2	oceán
<g/>
,	,	kIx,	,
ledové	ledový	k2eAgFnSc2d1	ledová
pokrývky	pokrývka	k1gFnSc2	pokrývka
a	a	k8xC	a
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sentinel-	Sentinel-	k?	Sentinel-
<g/>
4	[number]	k4	4
-	-	kIx~	-
sledování	sledování	k1gNnSc6	sledování
stavu	stav	k1gInSc2	stav
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sentinel-	Sentinel-	k?	Sentinel-
<g/>
5	[number]	k4	5
<g/>
P	P	kA	P
-	-	kIx~	-
dočasná	dočasný	k2eAgFnSc1d1	dočasná
náhrada	náhrada	k1gFnSc1	náhrada
za	za	k7c4	za
družici	družice	k1gFnSc4	družice
Envisat	Envisat	k1gMnPc2	Envisat
pokrývající	pokrývající	k2eAgNnSc1d1	pokrývající
období	období	k1gNnSc1	období
do	do	k7c2	do
vypuštění	vypuštění	k1gNnSc2	vypuštění
Sentinelu-	Sentinelu-	k1gFnSc2	Sentinelu-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sentinel-	Sentinel-	k?	Sentinel-
<g/>
5	[number]	k4	5
-	-	kIx~	-
sledování	sledování	k1gNnSc6	sledování
stavu	stav	k1gInSc2	stav
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sentinel-	Sentinel-	k?	Sentinel-
<g/>
6	[number]	k4	6
-	-	kIx~	-
průzkum	průzkum	k1gInSc1	průzkum
topografie	topografie	k1gFnSc2	topografie
oceánů	oceán	k1gInPc2	oceán
za	za	k7c7	za
pomocí	pomoc	k1gFnSc7	pomoc
radarového	radarový	k2eAgInSc2d1	radarový
výškoměru	výškoměr	k1gInSc2	výškoměr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Výzkum	výzkum	k1gInSc1	výzkum
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
===	===	k?	===
</s>
</p>
<p>
<s>
Giotto	Giotto	k1gNnSc1	Giotto
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
–	–	k?	–
sonda	sonda	k1gFnSc1	sonda
určená	určený	k2eAgFnSc1d1	určená
k	k	k7c3	k
pozorování	pozorování	k1gNnSc3	pozorování
Halleyovy	Halleyův	k2eAgFnSc2d1	Halleyova
komety	kometa	k1gFnSc2	kometa
</s>
</p>
<p>
<s>
Huygens	Huygens	k1gInSc1	Huygens
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
–	–	k?	–
přistávací	přistávací	k2eAgInSc4d1	přistávací
modul	modul	k1gInSc4	modul
určený	určený	k2eAgInSc4d1	určený
k	k	k7c3	k
průzkumu	průzkum	k1gInSc3	průzkum
Saturnova	Saturnův	k2eAgInSc2d1	Saturnův
měsíce	měsíc	k1gInSc2	měsíc
Titanu	titan	k1gInSc2	titan
</s>
</p>
<p>
<s>
Mars	Mars	k1gInSc1	Mars
Express	express	k1gInSc1	express
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
–	–	k?	–
sonda	sonda	k1gFnSc1	sonda
zkoumající	zkoumající	k2eAgFnSc4d1	zkoumající
planetu	planeta	k1gFnSc4	planeta
Mars	Mars	k1gInSc1	Mars
s	s	k7c7	s
neúspěšným	úspěšný	k2eNgInSc7d1	neúspěšný
přistávacím	přistávací	k2eAgInSc7d1	přistávací
modulem	modul	k1gInSc7	modul
</s>
</p>
<p>
<s>
SMART-1	SMART-1	k4	SMART-1
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
–	–	k?	–
experimentální	experimentální	k2eAgFnSc1d1	experimentální
technologická	technologický	k2eAgFnSc1d1	technologická
sonda	sonda	k1gFnSc1	sonda
zkoumající	zkoumající	k2eAgFnSc1d1	zkoumající
Měsíc	měsíc	k1gInSc4	měsíc
</s>
</p>
<p>
<s>
Rosetta	Rosetta	k1gFnSc1	Rosetta
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
–	–	k?	–
sonda	sonda	k1gFnSc1	sonda
určená	určený	k2eAgFnSc1d1	určená
k	k	k7c3	k
výzkumu	výzkum	k1gInSc3	výzkum
komety	kometa	k1gFnSc2	kometa
s	s	k7c7	s
přistávacím	přistávací	k2eAgInSc7d1	přistávací
modulem	modul	k1gInSc7	modul
</s>
</p>
<p>
<s>
Venus	Venus	k1gInSc1	Venus
Express	express	k1gInSc1	express
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
–	–	k?	–
sonda	sonda	k1gFnSc1	sonda
určená	určený	k2eAgFnSc1d1	určená
k	k	k7c3	k
průzkumu	průzkum	k1gInSc3	průzkum
planety	planeta	k1gFnSc2	planeta
Venuše	Venuše	k1gFnSc2	Venuše
</s>
</p>
<p>
<s>
Don	Don	k1gMnSc1	Don
Quijote	Quijot	k1gInSc5	Quijot
(	(	kIx(	(
<g/>
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
<g/>
,	,	kIx,	,
plánováno	plánovat	k5eAaImNgNnS	plánovat
na	na	k7c4	na
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
–	–	k?	–
soubor	soubor	k1gInSc1	soubor
dvou	dva	k4xCgFnPc2	dva
sond	sonda	k1gFnPc2	sonda
určených	určený	k2eAgFnPc2d1	určená
k	k	k7c3	k
pokusnému	pokusný	k2eAgInSc3d1	pokusný
odklonu	odklon	k1gInSc3	odklon
dráhy	dráha	k1gFnSc2	dráha
blízkozemní	blízkozemní	k2eAgFnSc2d1	blízkozemní
planetky	planetka	k1gFnSc2	planetka
</s>
</p>
<p>
<s>
BepiColombo	BepiColomba	k1gMnSc5	BepiColomba
(	(	kIx(	(
<g/>
2018	[number]	k4	2018
<g/>
)	)	kIx)	)
–	–	k?	–
sonda	sonda	k1gFnSc1	sonda
určená	určený	k2eAgFnSc1d1	určená
k	k	k7c3	k
výzkumu	výzkum	k1gInSc3	výzkum
planety	planeta	k1gFnSc2	planeta
Merkur	Merkur	k1gInSc1	Merkur
</s>
</p>
<p>
<s>
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
programu	program	k1gInSc2	program
Aurora	Aurora	k1gFnSc1	Aurora
</s>
</p>
<p>
<s>
ExoMars	ExoMars	k1gInSc1	ExoMars
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
-	-	kIx~	-
<g/>
2020	[number]	k4	2020
<g/>
)	)	kIx)	)
–	–	k?	–
mise	mise	k1gFnSc2	mise
roveru	rover	k1gMnSc3	rover
určeného	určený	k2eAgInSc2d1	určený
k	k	k7c3	k
hledání	hledání	k1gNnSc3	hledání
stop	stop	k2eAgInSc2d1	stop
života	život	k1gInSc2	život
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
<g/>
,	,	kIx,	,
součástí	součást	k1gFnSc7	součást
geofyzikální	geofyzikální	k2eAgInSc1d1	geofyzikální
modul	modul	k1gInSc1	modul
</s>
</p>
<p>
<s>
Mars	Mars	k1gMnSc1	Mars
Sample	Sample	k1gMnSc1	Sample
Return	Return	k1gMnSc1	Return
–	–	k?	–
soubor	soubor	k1gInSc1	soubor
několika	několik	k4yIc2	několik
sond	sonda	k1gFnPc2	sonda
a	a	k8xC	a
přistávacího	přistávací	k2eAgInSc2d1	přistávací
modulu	modul	k1gInSc2	modul
určených	určený	k2eAgFnPc2d1	určená
k	k	k7c3	k
dopravě	doprava	k1gFnSc3	doprava
vzorků	vzorek	k1gInPc2	vzorek
půdy	půda	k1gFnSc2	půda
z	z	k7c2	z
Marsu	Mars	k1gInSc2	Mars
zpět	zpět	k6eAd1	zpět
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
(	(	kIx(	(
<g/>
plánováno	plánovat	k5eAaImNgNnS	plánovat
na	na	k7c4	na
2020	[number]	k4	2020
<g/>
–	–	k?	–
<g/>
2022	[number]	k4	2022
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Vědecké	vědecký	k2eAgInPc1d1	vědecký
projekty	projekt	k1gInPc1	projekt
==	==	k?	==
</s>
</p>
<p>
<s>
Planck	Planck	k1gInSc1	Planck
–	–	k?	–
kosmický	kosmický	k2eAgInSc1d1	kosmický
dalekohled	dalekohled	k1gInSc1	dalekohled
k	k	k7c3	k
průzkumu	průzkum	k1gInSc3	průzkum
reliktního	reliktní	k2eAgNnSc2d1	reliktní
záření	záření	k1gNnSc2	záření
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
,	,	kIx,	,
výrazně	výrazně	k6eAd1	výrazně
přispěl	přispět	k5eAaPmAgMnS	přispět
ke	k	k7c3	k
zpřesnění	zpřesnění	k1gNnSc3	zpřesnění
stáří	stáří	k1gNnSc2	stáří
vesmíru	vesmír	k1gInSc2	vesmír
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
-	-	kIx~	-
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
LISA	Lisa	k1gFnSc1	Lisa
Pathfinder	Pathfinder	k1gInSc1	Pathfinder
–	–	k?	–
družice	družice	k1gFnPc1	družice
k	k	k7c3	k
ověření	ověření	k1gNnSc3	ověření
principů	princip	k1gInPc2	princip
funkce	funkce	k1gFnSc2	funkce
hlavních	hlavní	k2eAgNnPc2d1	hlavní
zařízení	zařízení	k1gNnPc2	zařízení
projektu	projekt	k1gInSc2	projekt
eLISA	eLISA	k?	eLISA
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
-	-	kIx~	-
<g/>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Evolved	Evolved	k1gInSc1	Evolved
Laser	laser	k1gInSc1	laser
Interferometer	Interferometer	k1gInSc1	Interferometer
Space	Space	k1gMnSc2	Space
Antenna	Antenn	k1gMnSc2	Antenn
(	(	kIx(	(
<g/>
eLISA	eLISA	k?	eLISA
<g/>
)	)	kIx)	)
–	–	k?	–
družice	družice	k1gFnPc4	družice
k	k	k7c3	k
měření	měření	k1gNnSc3	měření
gravitačních	gravitační	k2eAgFnPc2d1	gravitační
vln	vlna	k1gFnPc2	vlna
(	(	kIx(	(
<g/>
plánováno	plánovat	k5eAaImNgNnS	plánovat
2034	[number]	k4	2034
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Navigační	navigační	k2eAgInPc4d1	navigační
a	a	k8xC	a
bezpečnostní	bezpečnostní	k2eAgInPc4d1	bezpečnostní
systémy	systém	k1gInPc4	systém
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
EU	EU	kA	EU
===	===	k?	===
</s>
</p>
<p>
<s>
EGNOS	EGNOS	kA	EGNOS
–	–	k?	–
systém	systém	k1gInSc1	systém
vyhodnocující	vyhodnocující	k2eAgFnSc4d1	vyhodnocující
přesnost	přesnost	k1gFnSc4	přesnost
navigačních	navigační	k2eAgInPc2d1	navigační
systémů	systém	k1gInPc2	systém
GPS	GPS	kA	GPS
a	a	k8xC	a
Glonass	Glonass	k1gInSc1	Glonass
</s>
</p>
<p>
<s>
Galileo	Galilea	k1gFnSc5	Galilea
–	–	k?	–
globální	globální	k2eAgInSc1d1	globální
satelitní	satelitní	k2eAgInSc1d1	satelitní
navigační	navigační	k2eAgInSc1d1	navigační
systém	systém	k1gInSc1	systém
</s>
</p>
<p>
<s>
GMES	GMES	kA	GMES
–	–	k?	–
satelitní	satelitní	k2eAgInSc4d1	satelitní
systém	systém	k1gInSc4	systém
monitorující	monitorující	k2eAgNnSc4d1	monitorující
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
a	a	k8xC	a
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
LÁLA	Lála	k1gMnSc1	Lála
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
;	;	kIx,	;
VÍTEK	Vítek	k1gMnSc1	Vítek
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
<g/>
.	.	kIx.	.
</s>
<s>
Malá	malý	k2eAgFnSc1d1	malá
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
kosmonautiky	kosmonautika	k1gFnSc2	kosmonautika
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Kosmická	kosmický	k2eAgFnSc1d1	kosmická
vize	vize	k1gFnSc1	vize
2015	[number]	k4	2015
<g/>
–	–	k?	–
<g/>
2025	[number]	k4	2025
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
kosmických	kosmický	k2eAgFnPc2d1	kosmická
agentur	agentura	k1gFnPc2	agentura
</s>
</p>
<p>
<s>
NASA	NASA	kA	NASA
</s>
</p>
<p>
<s>
Ruská	ruský	k2eAgFnSc1d1	ruská
kosmická	kosmický	k2eAgFnSc1d1	kosmická
agentura	agentura	k1gFnSc1	agentura
</s>
</p>
<p>
<s>
CNSA	CNSA	kA	CNSA
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Evropská	evropský	k2eAgFnSc1d1	Evropská
kosmická	kosmický	k2eAgFnSc1d1	kosmická
agentura	agentura	k1gFnSc1	agentura
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Portál	portál	k1gInSc1	portál
ESA	eso	k1gNnSc2	eso
</s>
</p>
<p>
<s>
Více	hodně	k6eAd2	hodně
o	o	k7c6	o
startu	start	k1gInSc6	start
SMOS	SMOS	kA	SMOS
1.11	[number]	k4	1.11
<g/>
.2009	.2009	k4	.2009
</s>
</p>
<p>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
flotila	flotila	k1gFnSc1	flotila
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
(	(	kIx(	(
<g/>
Budoucí	budoucí	k2eAgFnSc2d1	budoucí
sondy	sonda	k1gFnSc2	sonda
<g/>
)	)	kIx)	)
</s>
</p>
