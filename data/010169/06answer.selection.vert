<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
kosmická	kosmický	k2eAgFnSc1d1	kosmická
agentura	agentura	k1gFnSc1	agentura
(	(	kIx(	(
<g/>
European	European	k1gInSc1	European
Space	Space	k1gFnSc2	Space
Agency	Agenca	k1gFnSc2	Agenca
<g/>
,	,	kIx,	,
ESA	eso	k1gNnSc2	eso
<g/>
;	;	kIx,	;
Agence	agence	k1gFnSc1	agence
spatiale	spatiale	k6eAd1	spatiale
européenne	européennout	k5eAaPmIp3nS	européennout
<g/>
,	,	kIx,	,
ASE	as	k1gInSc5	as
<g/>
)	)	kIx)	)
je	on	k3xPp3gFnPc4	on
mezivládní	mezivládní	k2eAgFnPc4d1	mezivládní
organizace	organizace	k1gFnPc4	organizace
pro	pro	k7c4	pro
využití	využití	k1gNnSc4	využití
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
22	[number]	k4	22
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
včetně	včetně	k7c2	včetně
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
