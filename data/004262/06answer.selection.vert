<s>
Celou	celý	k2eAgFnSc7d1	celá
ji	on	k3xPp3gFnSc4	on
zajišťovala	zajišťovat	k5eAaImAgFnS	zajišťovat
italská	italský	k2eAgFnSc1d1	italská
společnost	společnost	k1gFnSc1	společnost
Grandi	grand	k1gMnPc1	grand
Stazioni	Stazion	k1gMnPc1	Stazion
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
si	se	k3xPyFc3	se
pro	pro	k7c4	pro
projektovou	projektový	k2eAgFnSc4d1	projektová
činnost	činnost	k1gFnSc4	činnost
vybrala	vybrat	k5eAaPmAgFnS	vybrat
sdružení	sdružení	k1gNnSc4	sdružení
projektantů	projektant	k1gMnPc2	projektant
Metroprojekt	Metroprojekt	k1gInSc1	Metroprojekt
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
architektem	architekt	k1gMnSc7	architekt
Patrikem	Patrik	k1gMnSc7	Patrik
Kotasem	Kotas	k1gMnSc7	Kotas
a	a	k8xC	a
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
původními	původní	k2eAgMnPc7d1	původní
architekty	architekt	k1gMnPc7	architekt
(	(	kIx(	(
<g/>
Janem	Jan	k1gMnSc7	Jan
Bočanem	bočan	k1gMnSc7	bočan
a	a	k8xC	a
Alenou	Alena	k1gFnSc7	Alena
Šrámkovou	Šrámková	k1gFnSc7	Šrámková
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
