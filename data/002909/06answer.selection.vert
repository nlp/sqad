<s>
Proslulá	proslulý	k2eAgFnSc1d1	proslulá
je	být	k5eAaImIp3nS	být
zoo	zoo	k1gFnSc1	zoo
Dvůr	Dvůr	k1gInSc4	Dvůr
Králové	Králová	k1gFnSc2	Králová
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
proslavil	proslavit	k5eAaPmAgMnS	proslavit
zejména	zejména	k9	zejména
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
její	její	k3xOp3gMnSc1	její
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
ředitel	ředitel	k1gMnSc1	ředitel
<g/>
,	,	kIx,	,
vynikající	vynikající	k2eAgMnSc1d1	vynikající
zoolog	zoolog	k1gMnSc1	zoolog
a	a	k8xC	a
cestovatel	cestovatel	k1gMnSc1	cestovatel
Josef	Josef	k1gMnSc1	Josef
Vágner	Vágner	k1gMnSc1	Vágner
<g/>
.	.	kIx.	.
</s>
