<s>
Dusík	dusík	k1gInSc1	dusík
je	být	k5eAaImIp3nS	být
inertní	inertní	k2eAgInSc1d1	inertní
plyn	plyn	k1gInSc1	plyn
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
reaguje	reagovat	k5eAaBmIp3nS	reagovat
s	s	k7c7	s
jinými	jiný	k2eAgFnPc7d1	jiná
chemickými	chemický	k2eAgFnPc7d1	chemická
sloučeninami	sloučenina	k1gFnPc7	sloučenina
pouze	pouze	k6eAd1	pouze
za	za	k7c2	za
vysokých	vysoký	k2eAgFnPc2d1	vysoká
teplot	teplota	k1gFnPc2	teplota
a	a	k8xC	a
tlaků	tlak	k1gInPc2	tlak
<g/>
.	.	kIx.	.
</s>
