<s>
Windows	Windows	kA	Windows
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
informatice	informatika	k1gFnSc6	informatika
označení	označení	k1gNnSc2	označení
pro	pro	k7c4	pro
několik	několik	k4yIc4	několik
různých	různý	k2eAgInPc2d1	různý
operačních	operační	k2eAgInPc2d1	operační
systémů	systém	k1gInPc2	systém
od	od	k7c2	od
firmy	firma	k1gFnSc2	firma
Microsoft	Microsoft	kA	Microsoft
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
mají	mít	k5eAaImIp3nP	mít
grafické	grafický	k2eAgNnSc4d1	grafické
uživatelské	uživatelský	k2eAgNnSc4d1	Uživatelské
rozhraní	rozhraní	k1gNnSc4	rozhraní
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
liší	lišit	k5eAaImIp3nP	lišit
se	se	k3xPyFc4	se
použitým	použitý	k2eAgNnSc7d1	Použité
jádrem	jádro	k1gNnSc7	jádro
<g/>
,	,	kIx,	,
úrovní	úroveň	k1gFnSc7	úroveň
podpory	podpora	k1gFnSc2	podpora
multitaskingu	multitasking	k1gInSc2	multitasking
(	(	kIx(	(
<g/>
současného	současný	k2eAgInSc2d1	současný
běhu	běh	k1gInSc2	běh
více	hodně	k6eAd2	hodně
úloh	úloha	k1gFnPc2	úloha
najednou	najednou	k6eAd1	najednou
<g/>
)	)	kIx)	)
i	i	k9	i
používanými	používaný	k2eAgFnPc7d1	používaná
knihovnami	knihovna	k1gFnPc7	knihovna
a	a	k8xC	a
účelem	účel	k1gInSc7	účel
použití	použití	k1gNnSc2	použití
<g/>
.	.	kIx.	.
</s>
<s>
Grafické	grafický	k2eAgNnSc4d1	grafické
uživatelské	uživatelský	k2eAgNnSc4d1	Uživatelské
rozhraní	rozhraní	k1gNnSc4	rozhraní
(	(	kIx(	(
<g/>
GUI	GUI	kA	GUI
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
poprvé	poprvé	k6eAd1	poprvé
použito	použít	k5eAaPmNgNnS	použít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
v	v	k7c6	v
počítači	počítač	k1gInSc6	počítač
Xerox	Xerox	kA	Xerox
Alto	Alto	k6eAd1	Alto
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
WIMP	WIMP	kA	WIMP
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc4	jeho
popularizaci	popularizace	k1gFnSc4	popularizace
mezi	mezi	k7c7	mezi
běžnými	běžný	k2eAgMnPc7d1	běžný
uživateli	uživatel	k1gMnPc7	uživatel
zajistil	zajistit	k5eAaPmAgMnS	zajistit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
počítač	počítač	k1gInSc1	počítač
Apple	Apple	kA	Apple
Lisa	Lisa	k1gFnSc1	Lisa
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
pak	pak	k6eAd1	pak
masivně	masivně	k6eAd1	masivně
Apple	Apple	kA	Apple
Macintosh	Macintosh	kA	Macintosh
<g/>
.	.	kIx.	.
</s>
<s>
Počítače	počítač	k1gInPc1	počítač
IBM	IBM	kA	IBM
PC	PC	kA	PC
kompatibilní	kompatibilní	k2eAgFnSc1d1	kompatibilní
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
uvedeny	uvést	k5eAaPmNgInP	uvést
na	na	k7c4	na
trh	trh	k1gInSc4	trh
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
používaly	používat	k5eAaImAgFnP	používat
systém	systém	k1gInSc4	systém
MS-DOS	MS-DOS	k1gFnSc2	MS-DOS
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
používal	používat	k5eAaImAgMnS	používat
textové	textový	k2eAgNnSc4d1	textové
<g/>
,	,	kIx,	,
grafické	grafický	k2eAgNnSc4d1	grafické
rozhraní	rozhraní	k1gNnSc4	rozhraní
musely	muset	k5eAaImAgInP	muset
řešit	řešit	k5eAaImF	řešit
programy	program	k1gInPc4	program
samostatně	samostatně	k6eAd1	samostatně
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
textový	textový	k2eAgInSc1d1	textový
editor	editor	k1gInSc1	editor
Text	text	k1gInSc1	text
<g/>
602	[number]	k4	602
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
programátory	programátor	k1gMnPc4	programátor
pracné	pracný	k2eAgNnSc1d1	pracné
a	a	k8xC	a
nevýhodné	výhodný	k2eNgNnSc1d1	nevýhodné
<g/>
.	.	kIx.	.
</s>
<s>
Úplně	úplně	k6eAd1	úplně
první	první	k4xOgNnPc1	první
předběžná	předběžný	k2eAgNnPc1d1	předběžné
vydání	vydání	k1gNnPc1	vydání
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
byly	být	k5eAaImAgFnP	být
vydány	vydat	k5eAaPmNgFnP	vydat
v	v	k7c6	v
rocích	rok	k1gInPc6	rok
1983	[number]	k4	1983
až	až	k9	až
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
1.0	[number]	k4	1.0
byly	být	k5eAaImAgFnP	být
uvedeny	uvést	k5eAaPmNgFnP	uvést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
jako	jako	k8xC	jako
nadstavba	nadstavba	k1gFnSc1	nadstavba
systému	systém	k1gInSc2	systém
DOS	DOS	kA	DOS
<g/>
.	.	kIx.	.
</s>
<s>
Komerčně	komerčně	k6eAd1	komerčně
úspěšné	úspěšný	k2eAgFnPc1d1	úspěšná
byly	být	k5eAaImAgFnP	být
Windows	Windows	kA	Windows
3.0	[number]	k4	3.0
vydané	vydaný	k2eAgFnSc2d1	vydaná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Označení	označení	k1gNnSc1	označení
Windows	Windows	kA	Windows
je	být	k5eAaImIp3nS	být
firmou	firma	k1gFnSc7	firma
Microsoft	Microsoft	kA	Microsoft
z	z	k7c2	z
obchodních	obchodní	k2eAgInPc2d1	obchodní
důvodů	důvod	k1gInPc2	důvod
používáno	používat	k5eAaImNgNnS	používat
pro	pro	k7c4	pro
několik	několik	k4yIc4	několik
různých	různý	k2eAgInPc2d1	různý
systémů	systém	k1gInPc2	systém
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
používají	používat	k5eAaImIp3nP	používat
různá	různý	k2eAgNnPc4d1	různé
jádra	jádro	k1gNnPc4	jádro
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Stejné	stejný	k2eAgNnSc1d1	stejné
označení	označení	k1gNnSc1	označení
je	být	k5eAaImIp3nS	být
využíváno	využívat	k5eAaImNgNnS	využívat
jako	jako	k8xC	jako
propagační	propagační	k2eAgInSc1d1	propagační
prvek	prvek	k1gInSc1	prvek
díky	díky	k7c3	díky
oligopolnímu	oligopolní	k2eAgNnSc3d1	oligopolní
postavení	postavení	k1gNnSc3	postavení
této	tento	k3xDgFnSc2	tento
firmy	firma	k1gFnSc2	firma
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
osobních	osobní	k2eAgInPc2d1	osobní
počítačů	počítač	k1gInPc2	počítač
IBM	IBM	kA	IBM
PC	PC	kA	PC
kompatibilních	kompatibilní	k2eAgMnPc2d1	kompatibilní
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
Windows	Windows	kA	Windows
7	[number]	k4	7
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
systémy	systém	k1gInPc1	systém
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
aplikacím	aplikace	k1gFnPc3	aplikace
podobné	podobný	k2eAgNnSc1d1	podobné
aplikační	aplikační	k2eAgNnSc1d1	aplikační
rozhraní	rozhraní	k1gNnSc1	rozhraní
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
API	API	kA	API
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
Windows	Windows	kA	Windows
API	API	kA	API
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
Win	Win	k1gFnPc1	Win
<g/>
32	[number]	k4	32
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Provozovány	provozován	k2eAgInPc1d1	provozován
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
na	na	k7c6	na
různých	různý	k2eAgFnPc6d1	různá
hardwarových	hardwarový	k2eAgFnPc6d1	hardwarová
platformách	platforma	k1gFnPc6	platforma
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
liší	lišit	k5eAaImIp3nS	lišit
konfigurací	konfigurace	k1gFnSc7	konfigurace
počítače	počítač	k1gInSc2	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Windows	Windows	kA	Windows
pro	pro	k7c4	pro
DOS	DOS	kA	DOS
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnPc4	označení
pro	pro	k7c4	pro
první	první	k4xOgInSc4	první
grafický	grafický	k2eAgInSc4d1	grafický
systém	systém	k1gInSc4	systém
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
firma	firma	k1gFnSc1	firma
Microsoft	Microsoft	kA	Microsoft
začala	začít	k5eAaPmAgFnS	začít
prodávat	prodávat	k5eAaImF	prodávat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
termín	termín	k1gInSc1	termín
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
sama	sám	k3xTgFnSc1	sám
firma	firma	k1gFnSc1	firma
Microsoft	Microsoft	kA	Microsoft
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
svoji	svůj	k3xOyFgFnSc4	svůj
řadu	řada	k1gFnSc4	řada
systémů	systém	k1gInPc2	systém
používala	používat	k5eAaImAgFnS	používat
(	(	kIx(	(
<g/>
používáno	používán	k2eAgNnSc1d1	používáno
bylo	být	k5eAaImAgNnS	být
označení	označení	k1gNnSc1	označení
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
s	s	k7c7	s
číselným	číselný	k2eAgNnSc7d1	číselné
označením	označení	k1gNnSc7	označení
verze	verze	k1gFnSc2	verze
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejednalo	jednat	k5eNaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
operační	operační	k2eAgInSc4d1	operační
systém	systém	k1gInSc4	systém
v	v	k7c6	v
pravém	pravý	k2eAgInSc6d1	pravý
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c4	o
nadstavbu	nadstavba	k1gFnSc4	nadstavba
nad	nad	k7c7	nad
16	[number]	k4	16
<g/>
bitový	bitový	k2eAgInSc1d1	bitový
systém	systém	k1gInSc1	systém
DOS	DOS	kA	DOS
(	(	kIx(	(
<g/>
DOS	DOS	kA	DOS
měl	mít	k5eAaImAgInS	mít
pouze	pouze	k6eAd1	pouze
textové	textový	k2eAgNnSc4d1	textové
uživatelské	uživatelský	k2eAgNnSc4d1	Uživatelské
rozhraní	rozhraní	k1gNnSc4	rozhraní
s	s	k7c7	s
příkazovým	příkazový	k2eAgInSc7d1	příkazový
řádkem	řádek	k1gInSc7	řádek
<g/>
,	,	kIx,	,
systém	systém	k1gInSc1	systém
nezajišťoval	zajišťovat	k5eNaImAgInS	zajišťovat
plnou	plný	k2eAgFnSc4d1	plná
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
počítačem	počítač	k1gInSc7	počítač
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
Windows	Windows	kA	Windows
pro	pro	k7c4	pro
DOS	DOS	kA	DOS
se	se	k3xPyFc4	se
spouštěly	spouštět	k5eAaImAgFnP	spouštět
jako	jako	k9	jako
běžný	běžný	k2eAgInSc4d1	běžný
program	program	k1gInSc4	program
až	až	k6eAd1	až
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
nastartoval	nastartovat	k5eAaPmAgInS	nastartovat
systém	systém	k1gInSc1	systém
DOS	DOS	kA	DOS
<g/>
.	.	kIx.	.
</s>
<s>
Windows	Windows	kA	Windows
pro	pro	k7c4	pro
DOS	DOS	kA	DOS
používaly	používat	k5eAaImAgInP	používat
DOS	DOS	kA	DOS
pro	pro	k7c4	pro
své	své	k1gNnSc4	své
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
O	o	k7c4	o
operace	operace	k1gFnPc4	operace
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
souborovému	souborový	k2eAgInSc3d1	souborový
systému	systém	k1gInSc3	systém
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tvorba	tvorba	k1gFnSc1	tvorba
grafického	grafický	k2eAgNnSc2d1	grafické
uživatelského	uživatelský	k2eAgNnSc2d1	Uživatelské
rozhraní	rozhraní	k1gNnSc2	rozhraní
byla	být	k5eAaImAgFnS	být
naopak	naopak	k6eAd1	naopak
hlavním	hlavní	k2eAgInSc7d1	hlavní
úkolem	úkol	k1gInSc7	úkol
Windows	Windows	kA	Windows
pro	pro	k7c4	pro
DOS	DOS	kA	DOS
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
bylo	být	k5eAaImAgNnS	být
programátorům	programátor	k1gMnPc3	programátor
nabídnuto	nabídnut	k2eAgNnSc4d1	nabídnuto
jednotné	jednotný	k2eAgNnSc4d1	jednotné
aplikační	aplikační	k2eAgNnSc4d1	aplikační
rozhraní	rozhraní	k1gNnSc4	rozhraní
<g/>
,	,	kIx,	,
jednotný	jednotný	k2eAgInSc4d1	jednotný
subsystém	subsystém	k1gInSc4	subsystém
pro	pro	k7c4	pro
tisk	tisk	k1gInSc4	tisk
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgInSc3	ten
byly	být	k5eAaImAgFnP	být
využívány	využívat	k5eAaPmNgInP	využívat
ovladače	ovladač	k1gInPc1	ovladač
zařízení	zařízení	k1gNnSc1	zařízení
dodávané	dodávaný	k2eAgNnSc1d1	dodávané
výrobci	výrobce	k1gMnPc7	výrobce
hardwaru	hardware	k1gInSc2	hardware
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
verze	verze	k1gFnSc1	verze
Windows	Windows	kA	Windows
1.0	[number]	k4	1.0
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
trh	trh	k1gInSc4	trh
uvedena	uvést	k5eAaPmNgNnP	uvést
20.11	[number]	k4	20.11
<g/>
.1985	.1985	k4	.1985
<g/>
.	.	kIx.	.
</s>
<s>
Windows	Windows	kA	Windows
1.0	[number]	k4	1.0
poskytovaly	poskytovat	k5eAaImAgFnP	poskytovat
jednoduché	jednoduchý	k2eAgNnSc4d1	jednoduché
grafické	grafický	k2eAgNnSc4d1	grafické
rozhraní	rozhraní	k1gNnSc4	rozhraní
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
spouštět	spouštět	k5eAaImF	spouštět
většinu	většina	k1gFnSc4	většina
jednoduchých	jednoduchý	k2eAgInPc2d1	jednoduchý
programů	program	k1gInPc2	program
určených	určený	k2eAgInPc2d1	určený
původně	původně	k6eAd1	původně
pro	pro	k7c4	pro
DOS	DOS	kA	DOS
<g/>
.	.	kIx.	.
</s>
<s>
Okna	okno	k1gNnPc1	okno
se	se	k3xPyFc4	se
nemohla	moct	k5eNaImAgNnP	moct
překrývat	překrývat	k5eAaImF	překrývat
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
složitější	složitý	k2eAgInPc1d2	složitější
programy	program	k1gInPc1	program
se	se	k3xPyFc4	se
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
prostředí	prostředí	k1gNnSc6	prostředí
používat	používat	k5eAaImF	používat
nedaly	dát	k5eNaPmAgInP	dát
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
bylo	být	k5eAaImAgNnS	být
výhodnější	výhodný	k2eAgMnSc1d2	výhodnější
používat	používat	k5eAaImF	používat
grafické	grafický	k2eAgFnPc4d1	grafická
aplikace	aplikace	k1gFnPc4	aplikace
určené	určený	k2eAgFnPc4d1	určená
přímo	přímo	k6eAd1	přímo
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
systém	systém	k1gInSc4	systém
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
postupně	postupně	k6eAd1	postupně
vytlačily	vytlačit	k5eAaPmAgInP	vytlačit
programy	program	k1gInPc1	program
určené	určený	k2eAgInPc1d1	určený
pro	pro	k7c4	pro
DOS	DOS	kA	DOS
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
textový	textový	k2eAgInSc1d1	textový
editor	editor	k1gInSc1	editor
Text	text	k1gInSc1	text
<g/>
602	[number]	k4	602
byl	být	k5eAaImAgInS	být
nahrazen	nahradit	k5eAaPmNgInS	nahradit
programy	program	k1gInPc1	program
Microsoft	Microsoft	kA	Microsoft
Word	Word	kA	Word
<g/>
,	,	kIx,	,
Ami	Ami	k1gFnSc1	Ami
Pro	pro	k7c4	pro
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Verze	verze	k1gFnSc1	verze
Windows	Windows	kA	Windows
2.0	[number]	k4	2.0
byla	být	k5eAaImAgFnS	být
vypuštěna	vypustit	k5eAaPmNgFnS	vypustit
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1987	[number]	k4	1987
a	a	k8xC	a
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
několik	několik	k4yIc4	několik
vylepšení	vylepšení	k1gNnPc2	vylepšení
uživatelského	uživatelský	k2eAgNnSc2d1	Uživatelské
rozhraní	rozhraní	k1gNnSc2	rozhraní
a	a	k8xC	a
správy	správa	k1gFnSc2	správa
paměti	paměť	k1gFnSc2	paměť
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Windows	Windows	kA	Windows
2.0	[number]	k4	2.0
se	se	k3xPyFc4	se
mohla	moct	k5eAaImAgFnS	moct
okna	okno	k1gNnSc2	okno
programů	program	k1gInPc2	program
překrývat	překrývat	k5eAaImF	překrývat
přes	přes	k7c4	přes
druhé	druhý	k4xOgNnSc4	druhý
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
změněny	změněn	k2eAgFnPc1d1	změněna
a	a	k8xC	a
rozšířeny	rozšířen	k2eAgFnPc1d1	rozšířena
klávesové	klávesový	k2eAgFnPc1d1	klávesová
zkratky	zkratka	k1gFnPc1	zkratka
<g/>
.	.	kIx.	.
</s>
<s>
Verze	verze	k1gFnSc1	verze
2.1	[number]	k4	2.1
byla	být	k5eAaImAgFnS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1988	[number]	k4	1988
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
dostupná	dostupný	k2eAgFnSc1d1	dostupná
ve	v	k7c6	v
dvou	dva	k4xCgNnPc6	dva
provedeních	provedení	k1gNnPc6	provedení
<g/>
:	:	kIx,	:
Windows	Windows	kA	Windows
<g/>
/	/	kIx~	/
<g/>
286	[number]	k4	286
2.10	[number]	k4	2.10
a	a	k8xC	a
Windows	Windows	kA	Windows
<g/>
/	/	kIx~	/
<g/>
386	[number]	k4	386
2.10	[number]	k4	2.10
<g/>
.	.	kIx.	.
</s>
<s>
Označení	označení	k1gNnSc1	označení
verzí	verze	k1gFnPc2	verze
korespondovalo	korespondovat	k5eAaImAgNnS	korespondovat
se	se	k3xPyFc4	se
schopností	schopnost	k1gFnSc7	schopnost
využít	využít	k5eAaPmF	využít
rozšířený	rozšířený	k2eAgInSc1d1	rozšířený
režim	režim	k1gInSc1	režim
procesoru	procesor	k1gInSc2	procesor
Intel	Intel	kA	Intel
80286	[number]	k4	80286
a	a	k8xC	a
chráněný	chráněný	k2eAgInSc1d1	chráněný
režim	režim	k1gInSc1	režim
procesoru	procesor	k1gInSc2	procesor
Intel	Intel	kA	Intel
80386	[number]	k4	80386
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
poskytovaly	poskytovat	k5eAaImAgFnP	poskytovat
snadnější	snadný	k2eAgInSc4d2	snadnější
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
většímu	veliký	k2eAgNnSc3d2	veliký
množství	množství	k1gNnSc3	množství
paměti	paměť	k1gFnSc2	paměť
nad	nad	k7c4	nad
hranici	hranice	k1gFnSc4	hranice
1	[number]	k4	1
MiB	MiB	k1gFnPc2	MiB
operační	operační	k2eAgFnSc2d1	operační
paměti	paměť	k1gFnSc2	paměť
RAM	RAM	kA	RAM
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
komerční	komerční	k2eAgInSc4d1	komerční
úspěch	úspěch	k1gInSc4	úspěch
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
Windows	Windows	kA	Windows
3.0	[number]	k4	3.0
(	(	kIx(	(
<g/>
22.5	[number]	k4	22.5
<g/>
.1990	.1990	k4	.1990
<g/>
)	)	kIx)	)
a	a	k8xC	a
Windows	Windows	kA	Windows
3.1	[number]	k4	3.1
(	(	kIx(	(
<g/>
18.3	[number]	k4	18.3
<g/>
.1992	.1992	k4	.1992
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jádro	jádro	k1gNnSc1	jádro
těchto	tento	k3xDgInPc2	tento
systémů	systém	k1gInPc2	systém
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
částečnou	částečný	k2eAgFnSc4d1	částečná
podporu	podpora	k1gFnSc4	podpora
32	[number]	k4	32
<g/>
bitových	bitový	k2eAgInPc2d1	bitový
ovladačů	ovladač	k1gInPc2	ovladač
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
přistupovaly	přistupovat	k5eAaImAgFnP	přistupovat
k	k	k7c3	k
hardwaru	hardware	k1gInSc3	hardware
přímo	přímo	k6eAd1	přímo
bez	bez	k7c2	bez
využití	využití	k1gNnSc2	využití
služeb	služba	k1gFnPc2	služba
DOSu	DOSus	k1gInSc2	DOSus
a	a	k8xC	a
BIOSu	BIOSus	k1gInSc2	BIOSus
<g/>
.	.	kIx.	.
</s>
<s>
Windows	Windows	kA	Windows
3.0	[number]	k4	3.0
a	a	k8xC	a
Windows	Windows	kA	Windows
3.1	[number]	k4	3.1
měly	mít	k5eAaImAgFnP	mít
i	i	k9	i
lepší	dobrý	k2eAgInSc4d2	lepší
design	design	k1gInSc4	design
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
kvůli	kvůli	k7c3	kvůli
dispozicím	dispozice	k1gFnPc3	dispozice
nových	nový	k2eAgFnPc2d1	nová
virtuálních	virtuální	k2eAgFnPc2d1	virtuální
paměti	paměť	k1gFnSc2	paměť
a	a	k8xC	a
načítatelných	načítatelný	k2eAgInPc2d1	načítatelný
virtuálních	virtuální	k2eAgInPc2d1	virtuální
ovladačů	ovladač	k1gInPc2	ovladač
zařízení	zařízení	k1gNnSc2	zařízení
(	(	kIx(	(
<g/>
VxDs	VxDs	k1gInSc1	VxDs
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
jim	on	k3xPp3gMnPc3	on
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
sdílení	sdílení	k1gNnSc4	sdílení
libovolných	libovolný	k2eAgNnPc2d1	libovolné
zařízení	zařízení	k1gNnPc2	zařízení
mezi	mezi	k7c7	mezi
okny	okno	k1gNnPc7	okno
DOS	DOS	kA	DOS
<g/>
.	.	kIx.	.
</s>
<s>
Aplikace	aplikace	k1gFnSc1	aplikace
pro	pro	k7c4	pro
Windows	Windows	kA	Windows
tehdy	tehdy	k6eAd1	tehdy
už	už	k6eAd1	už
mohly	moct	k5eAaImAgFnP	moct
běžet	běžet	k5eAaImF	běžet
ve	v	k7c6	v
chráněném	chráněný	k2eAgInSc6d1	chráněný
režimu	režim	k1gInSc6	režim
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
jim	on	k3xPp3gMnPc3	on
dal	dát	k5eAaPmAgInS	dát
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
několika	několik	k4yIc3	několik
megabajtům	megabajt	k1gInPc3	megabajt
paměti	paměť	k1gFnSc2	paměť
a	a	k8xC	a
odstranil	odstranit	k5eAaPmAgInS	odstranit
povinnost	povinnost	k1gFnSc4	povinnost
podílet	podílet	k5eAaImF	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
schématu	schéma	k1gNnSc6	schéma
softwarové	softwarový	k2eAgFnSc2d1	softwarová
virtuální	virtuální	k2eAgFnSc2d1	virtuální
paměti	paměť	k1gFnSc2	paměť
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
běžely	běžet	k5eAaImAgInP	běžet
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
stejného	stejný	k2eAgInSc2d1	stejný
adresního	adresní	k2eAgInSc2d1	adresní
prostoru	prostor	k1gInSc2	prostor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
segmentovaná	segmentovaný	k2eAgFnSc1d1	segmentovaná
paměť	paměť	k1gFnSc1	paměť
poskytovala	poskytovat	k5eAaImAgFnS	poskytovat
určitý	určitý	k2eAgInSc4d1	určitý
stupeň	stupeň	k1gInSc4	stupeň
ochrany	ochrana	k1gFnSc2	ochrana
a	a	k8xC	a
také	také	k9	také
běžela	běžet	k5eAaImAgFnS	běžet
v	v	k7c6	v
multi-taskingu	multiasking	k1gInSc6	multi-tasking
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
doinstalovat	doinstalovat	k5eAaPmF	doinstalovat
rozšíření	rozšíření	k1gNnSc4	rozšíření
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc4	některý
části	část	k1gFnPc4	část
systému	systém	k1gInSc2	systém
však	však	k9	však
zůstávaly	zůstávat	k5eAaImAgInP	zůstávat
16	[number]	k4	16
<g/>
bitové	bitový	k2eAgInPc1d1	bitový
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
systém	systém	k1gInSc1	systém
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
ohrožen	ohrozit	k5eAaPmNgInS	ohrozit
nesprávně	správně	k6eNd1	správně
fungujícími	fungující	k2eAgInPc7d1	fungující
programy	program	k1gInPc7	program
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
systému	systém	k1gInSc2	systém
OS	OS	kA	OS
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
plně	plně	k6eAd1	plně
využíval	využívat	k5eAaImAgInS	využívat
schopnosti	schopnost	k1gFnSc2	schopnost
tehdejších	tehdejší	k2eAgInPc2d1	tehdejší
32	[number]	k4	32
<g/>
bitových	bitový	k2eAgInPc2d1	bitový
procesorů	procesor	k1gInPc2	procesor
Intel	Intel	kA	Intel
80386	[number]	k4	80386
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
nedostatky	nedostatek	k1gInPc1	nedostatek
nutily	nutit	k5eAaImAgInP	nutit
uživatele	uživatel	k1gMnSc4	uživatel
přecházet	přecházet	k5eAaImF	přecházet
na	na	k7c4	na
novější	nový	k2eAgInSc4d2	novější
systém	systém	k1gInSc4	systém
Windows	Windows	kA	Windows
NT	NT	kA	NT
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Změna	změna	k1gFnSc1	změna
vzhledu	vzhled	k1gInSc2	vzhled
grafického	grafický	k2eAgNnSc2d1	grafické
rozhraní	rozhraní	k1gNnSc2	rozhraní
(	(	kIx(	(
<g/>
výměna	výměna	k1gFnSc1	výměna
správce	správce	k1gMnSc2	správce
oken	okno	k1gNnPc2	okno
<g/>
)	)	kIx)	)
ve	v	k7c6	v
Windows	Windows	kA	Windows
95	[number]	k4	95
(	(	kIx(	(
<g/>
24.8	[number]	k4	24.8
<g/>
.1995	.1995	k4	.1995
<g/>
)	)	kIx)	)
přinesla	přinést	k5eAaPmAgFnS	přinést
intuitivnější	intuitivní	k2eAgNnSc4d2	intuitivnější
ovládání	ovládání	k1gNnSc4	ovládání
a	a	k8xC	a
vyšší	vysoký	k2eAgInSc4d2	vyšší
zájem	zájem	k1gInSc4	zájem
uživatelů	uživatel	k1gMnPc2	uživatel
<g/>
.	.	kIx.	.
</s>
<s>
Ihned	ihned	k6eAd1	ihned
po	po	k7c4	po
vypuštění	vypuštění	k1gNnSc4	vypuštění
na	na	k7c4	na
trh	trh	k1gInSc4	trh
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
první	první	k4xOgInSc4	první
den	den	k1gInSc4	den
prodalo	prodat	k5eAaPmAgNnS	prodat
300,000	[number]	k4	300,000
kopií	kopie	k1gFnPc2	kopie
<g/>
.	.	kIx.	.
</s>
<s>
Windows	Windows	kA	Windows
95	[number]	k4	95
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
i	i	k9	i
podporu	podpora	k1gFnSc4	podpora
protokolu	protokol	k1gInSc2	protokol
TCP	TCP	kA	TCP
<g/>
/	/	kIx~	/
<g/>
IP	IP	kA	IP
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamenalo	znamenat	k5eAaImAgNnS	znamenat
umožnění	umožnění	k1gNnSc1	umožnění
přímého	přímý	k2eAgInSc2d1	přímý
přístupu	přístup	k1gInSc2	přístup
k	k	k7c3	k
Internetu	Internet	k1gInSc3	Internet
bez	bez	k7c2	bez
instalace	instalace	k1gFnSc2	instalace
doplňků	doplněk	k1gInPc2	doplněk
od	od	k7c2	od
jiných	jiný	k2eAgMnPc2d1	jiný
dodavatelů	dodavatel	k1gMnPc2	dodavatel
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Trumpet	trumpeta	k1gFnPc2	trumpeta
Winsock	Winsocka	k1gFnPc2	Winsocka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tlačítko	tlačítko	k1gNnSc1	tlačítko
Start	start	k1gInSc1	start
<g/>
,	,	kIx,	,
podporu	podpora	k1gFnSc4	podpora
dlouhých	dlouhý	k2eAgInPc2d1	dlouhý
názvů	název	k1gInPc2	název
pro	pro	k7c4	pro
soubory	soubor	k1gInPc4	soubor
–	–	k?	–
až	až	k6eAd1	až
255	[number]	k4	255
znaků	znak	k1gInPc2	znak
a	a	k8xC	a
schopnost	schopnost	k1gFnSc4	schopnost
automaticky	automaticky	k6eAd1	automaticky
detekovat	detekovat	k5eAaImF	detekovat
a	a	k8xC	a
nakonfigurovat	nakonfigurovat	k5eAaPmF	nakonfigurovat
nově	nově	k6eAd1	nově
připojený	připojený	k2eAgInSc1d1	připojený
hardware	hardware	k1gInSc1	hardware
do	do	k7c2	do
počítače	počítač	k1gInSc2	počítač
(	(	kIx(	(
<g/>
Plug	Plug	k1gInSc1	Plug
and	and	k?	and
Play	play	k0	play
–	–	k?	–
užívaný	užívaný	k2eAgInSc1d1	užívaný
dodnes	dodnes	k6eAd1	dodnes
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mohl	moct	k5eAaImAgMnS	moct
nativně	nativně	k6eAd1	nativně
spouštět	spouštět	k5eAaImF	spouštět
32	[number]	k4	32
<g/>
bitové	bitový	k2eAgFnPc4d1	bitová
aplikace	aplikace	k1gFnPc4	aplikace
<g/>
,	,	kIx,	,
a	a	k8xC	a
obsahoval	obsahovat	k5eAaImAgMnS	obsahovat
několik	několik	k4yIc4	několik
technologických	technologický	k2eAgNnPc2d1	Technologické
vylepšení	vylepšení	k1gNnPc2	vylepšení
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
zvýšily	zvýšit	k5eAaPmAgFnP	zvýšit
stabilitu	stabilita	k1gFnSc4	stabilita
systému	systém	k1gInSc2	systém
oproti	oproti	k7c3	oproti
Windows	Windows	kA	Windows
3.1	[number]	k4	3.1
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
představeny	představit	k5eAaPmNgFnP	představit
několik	několik	k4yIc4	několik
OEM	OEM	kA	OEM
Service	Service	k1gFnSc1	Service
Releases	Releases	k1gMnSc1	Releases
(	(	kIx(	(
<g/>
OSR	OSR	kA	OSR
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
velké	velký	k2eAgInPc1d1	velký
balíky	balík	k1gInPc1	balík
aktualizací	aktualizace	k1gFnPc2	aktualizace
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
Windows	Windows	kA	Windows
95	[number]	k4	95
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
každý	každý	k3xTgMnSc1	každý
byl	být	k5eAaImAgInS	být
hrubě	hrubě	k6eAd1	hrubě
ekvivalentní	ekvivalentní	k2eAgInPc4d1	ekvivalentní
k	k	k7c3	k
aktualizaci	aktualizace	k1gFnSc3	aktualizace
Service	Service	k1gFnSc1	Service
Pack	Pack	k1gMnSc1	Pack
(	(	kIx(	(
<g/>
SP	SP	kA	SP
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgNnSc7d1	další
vydáním	vydání	k1gNnSc7	vydání
byl	být	k5eAaImAgInS	být
Windows	Windows	kA	Windows
98	[number]	k4	98
<g/>
,	,	kIx,	,
uveden	uveden	k2eAgInSc4d1	uveden
25.6	[number]	k4	25.6
<g/>
.1998	.1998	k4	.1998
<g/>
.	.	kIx.	.
</s>
<s>
Windows	Windows	kA	Windows
98	[number]	k4	98
přidal	přidat	k5eAaPmAgInS	přidat
podporu	podpora	k1gFnSc4	podpora
USB	USB	kA	USB
<g/>
,	,	kIx,	,
automatickou	automatický	k2eAgFnSc4d1	automatická
aktualizaci	aktualizace	k1gFnSc4	aktualizace
počítače	počítač	k1gInSc2	počítač
<g/>
,	,	kIx,	,
Internet	Internet	k1gInSc1	Internet
Explorer	Explorer	k1gInSc1	Explorer
4.01	[number]	k4	4.01
<g/>
,	,	kIx,	,
Panel	panel	k1gInSc4	panel
Snadné	snadný	k2eAgNnSc1d1	snadné
spuštění	spuštění	k1gNnSc1	spuštění
<g/>
...	...	k?	...
</s>
<s>
Microsoft	Microsoft	kA	Microsoft
představil	představit	k5eAaPmAgInS	představit
i	i	k9	i
druhou	druhý	k4xOgFnSc4	druhý
verzi	verze	k1gFnSc4	verze
systému	systém	k1gInSc2	systém
Windows	Windows	kA	Windows
98	[number]	k4	98
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
pojmenovanou	pojmenovaný	k2eAgFnSc7d1	pojmenovaná
Windows	Windows	kA	Windows
98	[number]	k4	98
Second	Second	k1gInSc1	Second
Edition	Edition	k1gInSc4	Edition
(	(	kIx(	(
<g/>
Druhé	druhý	k4xOgNnSc1	druhý
Vydání	vydání	k1gNnSc1	vydání
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
zkracován	zkracován	k2eAgInSc1d1	zkracován
<g />
.	.	kIx.	.
</s>
<s>
jako	jako	k9	jako
Windows	Windows	kA	Windows
98	[number]	k4	98
SE	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
opravila	opravit	k5eAaPmAgFnS	opravit
řadu	řada	k1gFnSc4	řada
menších	malý	k2eAgFnPc2d2	menší
chyb	chyba	k1gFnPc2	chyba
a	a	k8xC	a
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
novější	nový	k2eAgFnSc1d2	novější
verze	verze	k1gFnSc1	verze
programů	program	k1gInPc2	program
např.	např.	kA	např.
<g/>
:	:	kIx,	:
Internet	Internet	k1gInSc1	Internet
Explorer	Explorer	k1gInSc1	Explorer
5.0	[number]	k4	5.0
<g/>
,	,	kIx,	,
Windows	Windows	kA	Windows
Media	medium	k1gNnSc2	medium
Player	Player	k1gInSc4	Player
6.2	[number]	k4	6.2
14.9	[number]	k4	14.9
<g/>
.	.	kIx.	.
2000	[number]	k4	2000
byl	být	k5eAaImAgInS	být
uveden	uvést	k5eAaPmNgInS	uvést
systém	systém	k1gInSc1	systém
Windows	Windows	kA	Windows
ME	ME	kA	ME
(	(	kIx(	(
<g/>
Millennium	millennium	k1gNnSc1	millennium
Edition	Edition	k1gInSc1	Edition
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
posledním	poslední	k2eAgInSc7d1	poslední
systém	systém	k1gInSc1	systém
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
Windows	Windows	kA	Windows
pro	pro	k7c4	pro
DOS	DOS	kA	DOS
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Windows	Windows	kA	Windows
NT	NT	kA	NT
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
byla	být	k5eAaImAgFnS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
do	do	k7c2	do
prodeje	prodej	k1gInSc2	prodej
nová	nový	k2eAgFnSc1d1	nová
řada	řada	k1gFnSc1	řada
Windows	Windows	kA	Windows
NT	NT	kA	NT
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
New	New	k1gFnSc3	New
Technology	technolog	k1gMnPc7	technolog
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
plně	plně	k6eAd1	plně
využívá	využívat	k5eAaImIp3nS	využívat
schopnosti	schopnost	k1gFnPc1	schopnost
procesoru	procesor	k1gInSc2	procesor
Intel	Intel	kA	Intel
80386	[number]	k4	80386
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
chráněný	chráněný	k2eAgInSc1d1	chráněný
režim	režim	k1gInSc1	režim
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
kterého	který	k3yRgInSc2	který
neztrácí	ztrácet	k5eNaImIp3nS	ztrácet
jádro	jádro	k1gNnSc1	jádro
systému	systém	k1gInSc2	systém
nikdy	nikdy	k6eAd1	nikdy
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
počítačem	počítač	k1gInSc7	počítač
a	a	k8xC	a
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
provozovaných	provozovaný	k2eAgInPc6d1	provozovaný
programech	program	k1gInPc6	program
<g/>
.	.	kIx.	.
</s>
<s>
Zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
preemptivní	preemptivní	k2eAgInSc4d1	preemptivní
multitasking	multitasking	k1gInSc4	multitasking
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
špatně	špatně	k6eAd1	špatně
naprogramovaná	naprogramovaný	k2eAgFnSc1d1	naprogramovaná
aplikace	aplikace	k1gFnSc1	aplikace
nemohla	moct	k5eNaImAgFnS	moct
ohrozit	ohrozit	k5eAaPmF	ohrozit
běh	běh	k1gInSc4	běh
celého	celý	k2eAgInSc2d1	celý
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
Windows	Windows	kA	Windows
NT	NT	kA	NT
měl	mít	k5eAaImAgInS	mít
vyšší	vysoký	k2eAgInPc4d2	vyšší
hardwarové	hardwarový	k2eAgInPc4d1	hardwarový
požadavky	požadavek	k1gInPc4	požadavek
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
zamýšlen	zamýšlet	k5eAaImNgInS	zamýšlet
pro	pro	k7c4	pro
firemní	firemní	k2eAgNnSc4d1	firemní
prostředí	prostředí	k1gNnSc4	prostředí
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
workstation	workstation	k1gInSc1	workstation
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
pracovní	pracovní	k2eAgFnSc2d1	pracovní
stanice	stanice	k1gFnSc2	stanice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Označení	označení	k1gNnSc1	označení
NT	NT	kA	NT
bylo	být	k5eAaImAgNnS	být
později	pozdě	k6eAd2	pozdě
z	z	k7c2	z
názvů	název	k1gInPc2	název
produktů	produkt	k1gInPc2	produkt
odstraněno	odstraněn	k2eAgNnSc1d1	odstraněno
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stále	stále	k6eAd1	stále
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
stejnou	stejný	k2eAgFnSc4d1	stejná
řadu	řada	k1gFnSc4	řada
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
řadě	řada	k1gFnSc6	řada
systémů	systém	k1gInPc2	systém
Windows	Windows	kA	Windows
NT	NT	kA	NT
byla	být	k5eAaImAgFnS	být
jako	jako	k9	jako
první	první	k4xOgFnSc1	první
vydána	vydán	k2eAgFnSc1d1	vydána
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
verze	verze	k1gFnSc2	verze
Windows	Windows	kA	Windows
NT	NT	kA	NT
3.1	[number]	k4	3.1
a	a	k8xC	a
mezi	mezi	k7c4	mezi
další	další	k2eAgMnPc4d1	další
populární	populární	k2eAgMnPc4d1	populární
patří	patřit	k5eAaImIp3nP	patřit
Windows	Windows	kA	Windows
NT	NT	kA	NT
4.0	[number]	k4	4.0
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Windows	Windows	kA	Windows
2000	[number]	k4	2000
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Windows	Windows	kA	Windows
XP	XP	kA	XP
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Windows	Windows	kA	Windows
7	[number]	k4	7
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Windows	Windows	kA	Windows
10	[number]	k4	10
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
stejné	stejný	k2eAgFnSc2d1	stejná
řady	řada	k1gFnSc2	řada
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
serverová	serverový	k2eAgNnPc4d1	serverové
vydání	vydání	k1gNnPc4	vydání
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
Windows	Windows	kA	Windows
2000	[number]	k4	2000
Server	server	k1gInSc1	server
Windows	Windows	kA	Windows
Server	server	k1gInSc4	server
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
Windows	Windows	kA	Windows
Server	server	k1gInSc4	server
2012	[number]	k4	2012
nebo	nebo	k8xC	nebo
Windows	Windows	kA	Windows
Server	server	k1gInSc4	server
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Windows	Windows	kA	Windows
CE	CE	kA	CE
<g/>
.	.	kIx.	.
</s>
<s>
Windows	Windows	kA	Windows
CE	CE	kA	CE
je	být	k5eAaImIp3nS	být
samostatný	samostatný	k2eAgInSc4d1	samostatný
systém	systém	k1gInSc4	systém
určený	určený	k2eAgInSc4d1	určený
pro	pro	k7c4	pro
malé	malý	k2eAgInPc4d1	malý
vestavěné	vestavěný	k2eAgInPc4d1	vestavěný
systémy	systém	k1gInPc4	systém
(	(	kIx(	(
<g/>
PDA	PDA	kA	PDA
<g/>
,	,	kIx,	,
chytré	chytrý	k2eAgInPc1d1	chytrý
telefony	telefon	k1gInPc1	telefon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
tedy	tedy	k9	tedy
minimalizovanou	minimalizovaný	k2eAgFnSc7d1	minimalizovaná
variantou	varianta	k1gFnSc7	varianta
odvozenou	odvozený	k2eAgFnSc7d1	odvozená
od	od	k7c2	od
ostatních	ostatní	k2eAgNnPc2d1	ostatní
Windows	Windows	kA	Windows
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
postaveny	postavit	k5eAaPmNgFnP	postavit
na	na	k7c6	na
hybridním	hybridní	k2eAgNnSc6d1	hybridní
jádře	jádro	k1gNnSc6	jádro
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
pro	pro	k7c4	pro
platformy	platforma	k1gFnPc4	platforma
x	x	k?	x
<g/>
86	[number]	k4	86
<g/>
,	,	kIx,	,
ARM	ARM	kA	ARM
a	a	k8xC	a
MIPS	MIPS	kA	MIPS
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
verze	verze	k1gFnSc1	verze
byla	být	k5eAaImAgFnS	být
vydána	vydat	k5eAaPmNgFnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
druhého	druhý	k4xOgNnSc2	druhý
vydání	vydání	k1gNnSc2	vydání
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
deterministické	deterministický	k2eAgNnSc4d1	deterministické
plánování	plánování	k1gNnSc4	plánování
procesů	proces	k1gInPc2	proces
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
operačním	operační	k2eAgInSc7d1	operační
systémem	systém	k1gInSc7	systém
reálného	reálný	k2eAgInSc2d1	reálný
času	čas	k1gInSc2	čas
(	(	kIx(	(
<g/>
RTOS	RTOS	kA	RTOS
–	–	k?	–
Real	Real	k1gInSc1	Real
Time	Time	k1gNnSc1	Time
Operating	Operating	k1gInSc1	Operating
System	Systo	k1gNnSc7	Systo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
aplikace	aplikace	k1gFnPc4	aplikace
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
redukované	redukovaný	k2eAgInPc4d1	redukovaný
Windows	Windows	kA	Windows
API	API	kA	API
(	(	kIx(	(
<g/>
Win	Win	k1gFnSc1	Win
<g/>
32	[number]	k4	32
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Windows	Windows	kA	Windows
CE	CE	kA	CE
jsou	být	k5eAaImIp3nP	být
odvozeny	odvozen	k2eAgInPc1d1	odvozen
různé	různý	k2eAgInPc1d1	různý
systémy	systém	k1gInPc1	systém
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgNnSc4d3	nejznámější
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
Pocket	Pocket	k1gInSc1	Pocket
PC	PC	kA	PC
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
–	–	k?	–
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Windows	Windows	kA	Windows
Mobile	mobile	k1gNnSc4	mobile
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
–	–	k?	–
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Windows	Windows	kA	Windows
Phone	Phon	k1gInSc5	Phon
7	[number]	k4	7
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Windows	Windows	kA	Windows
Phone	Phon	k1gInSc5	Phon
8	[number]	k4	8
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Windows	Windows	kA	Windows
RT	RT	kA	RT
<g/>
.	.	kIx.	.
</s>
<s>
Windows	Windows	kA	Windows
RT	RT	kA	RT
je	být	k5eAaImIp3nS	být
samostatný	samostatný	k2eAgInSc1d1	samostatný
systém	systém	k1gInSc1	systém
vycházející	vycházející	k2eAgInSc1d1	vycházející
z	z	k7c2	z
Windows	Windows	kA	Windows
NT	NT	kA	NT
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
určen	určit	k5eAaPmNgInS	určit
pro	pro	k7c4	pro
procesory	procesor	k1gInPc4	procesor
ARM	ARM	kA	ARM
<g/>
,	,	kIx,	,
a	a	k8xC	a
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
uveden	uvést	k5eAaPmNgInS	uvést
na	na	k7c4	na
trh	trh	k1gInSc4	trh
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
prodáván	prodávat	k5eAaImNgInS	prodávat
samostatně	samostatně	k6eAd1	samostatně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
je	být	k5eAaImIp3nS	být
předinstalován	předinstalovat	k5eAaPmNgInS	předinstalovat
na	na	k7c6	na
nově	nově	k6eAd1	nově
prodávaných	prodávaný	k2eAgNnPc6d1	prodávané
zařízeních	zařízení	k1gNnPc6	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
Windows	Windows	kA	Windows
10	[number]	k4	10
již	již	k6eAd1	již
není	být	k5eNaImIp3nS	být
podporován	podporovat	k5eAaImNgMnS	podporovat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
verzi	verze	k1gFnSc6	verze
Windows	Windows	kA	Windows
8	[number]	k4	8
je	být	k5eAaImIp3nS	být
zachována	zachován	k2eAgFnSc1d1	zachována
plocha	plocha	k1gFnSc1	plocha
(	(	kIx(	(
<g/>
Desktop	desktop	k1gInSc1	desktop
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tlačítko	tlačítko	k1gNnSc1	tlačítko
Start	start	k1gInSc4	start
zmizelo	zmizet	k5eAaPmAgNnS	zmizet
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
nahrazeno	nahradit	k5eAaPmNgNnS	nahradit
dlaždicovým	dlaždicový	k2eAgNnSc7d1	dlaždicové
prostředím	prostředí	k1gNnSc7	prostředí
Modern	Moderna	k1gFnPc2	Moderna
UI	UI	kA	UI
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
označovaným	označovaný	k2eAgInSc7d1	označovaný
pojmem	pojem	k1gInSc7	pojem
Metro	metro	k1gNnSc1	metro
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
změna	změna	k1gFnSc1	změna
nebyla	být	k5eNaImAgFnS	být
uživateli	uživatel	k1gMnSc3	uživatel
příliš	příliš	k6eAd1	příliš
přívětivě	přívětivě	k6eAd1	přívětivě
přijata	přijmout	k5eAaPmNgFnS	přijmout
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
další	další	k2eAgFnSc6d1	další
verzi	verze	k1gFnSc6	verze
tlačítko	tlačítko	k1gNnSc1	tlačítko
Start	start	k1gInSc1	start
opět	opět	k6eAd1	opět
vráceno	vrátit	k5eAaPmNgNnS	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Windows	Windows	kA	Windows
8.1	[number]	k4	8.1
je	být	k5eAaImIp3nS	být
další	další	k2eAgFnSc7d1	další
verzí	verze	k1gFnSc7	verze
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
Windows	Windows	kA	Windows
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
uvolněn	uvolnit	k5eAaPmNgInS	uvolnit
17	[number]	k4	17
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2013	[number]	k4	2013
na	na	k7c6	na
Windows	Windows	kA	Windows
Store	Stor	k1gMnSc5	Stor
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
bezplatný	bezplatný	k2eAgInSc4d1	bezplatný
upgrade	upgrade	k1gInSc4	upgrade
předchozí	předchozí	k2eAgFnSc2d1	předchozí
verze	verze	k1gFnSc2	verze
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
dosavadní	dosavadní	k2eAgMnPc4d1	dosavadní
uživatele	uživatel	k1gMnPc4	uživatel
Windows	Windows	kA	Windows
8	[number]	k4	8
<g/>
)	)	kIx)	)
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
všeobecně	všeobecně	k6eAd1	všeobecně
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
výchozímu	výchozí	k2eAgMnSc3d1	výchozí
Windows	Windows	kA	Windows
8	[number]	k4	8
se	se	k3xPyFc4	se
vrátilo	vrátit	k5eAaPmAgNnS	vrátit
tlačítko	tlačítko	k1gNnSc1	tlačítko
Start	start	k1gInSc1	start
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
patrné	patrný	k2eAgNnSc1d1	patrné
také	také	k9	také
vylepšení	vylepšení	k1gNnSc1	vylepšení
grafiky	grafika	k1gFnSc2	grafika
a	a	k8xC	a
více	hodně	k6eAd2	hodně
možností	možnost	k1gFnSc7	možnost
využití	využití	k1gNnSc2	využití
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
Microsoft	Microsoft	kA	Microsoft
vypsal	vypsat	k5eAaPmAgMnS	vypsat
odměnu	odměna	k1gFnSc4	odměna
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
někdo	někdo	k3yInSc1	někdo
nalezne	nalézt	k5eAaBmIp3nS	nalézt
a	a	k8xC	a
opraví	opravit	k5eAaPmIp3nP	opravit
chyby	chyba	k1gFnPc4	chyba
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
verzi	verze	k1gFnSc6	verze
OS	osa	k1gFnPc2	osa
<g/>
.	.	kIx.	.
</s>
<s>
Windows	Windows	kA	Windows
10	[number]	k4	10
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
trh	trh	k1gInSc4	trh
uveden	uvést	k5eAaPmNgInS	uvést
29	[number]	k4	29
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
od	od	k7c2	od
30	[number]	k4	30
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2014	[number]	k4	2014
byl	být	k5eAaImAgMnS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
v	v	k7c6	v
předběžné	předběžný	k2eAgFnSc6d1	předběžná
testovací	testovací	k2eAgFnSc6d1	testovací
verzi	verze	k1gFnSc6	verze
Technical	Technical	k1gMnSc1	Technical
Preview	Preview	k1gMnSc1	Preview
<g/>
.	.	kIx.	.
</s>
<s>
Spojuje	spojovat	k5eAaImIp3nS	spojovat
v	v	k7c6	v
sobě	sebe	k3xPyFc6	sebe
vlastnosti	vlastnost	k1gFnPc4	vlastnost
Windows	Windows	kA	Windows
7	[number]	k4	7
a	a	k8xC	a
8	[number]	k4	8
<g/>
;	;	kIx,	;
nabízí	nabízet	k5eAaImIp3nS	nabízet
klasickou	klasický	k2eAgFnSc4d1	klasická
nabídku	nabídka	k1gFnSc4	nabídka
Start	start	k1gInSc1	start
(	(	kIx(	(
<g/>
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
při	při	k7c6	při
ovládání	ovládání	k1gNnSc6	ovládání
prstem	prst	k1gInSc7	prst
zvětší	zvětšit	k5eAaPmIp3nS	zvětšit
na	na	k7c4	na
celou	celý	k2eAgFnSc4d1	celá
obrazovku	obrazovka	k1gFnSc4	obrazovka
<g/>
)	)	kIx)	)
i	i	k9	i
živé	živý	k2eAgFnPc1d1	živá
dlaždice	dlaždice	k1gFnPc1	dlaždice
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
aplikace	aplikace	k1gFnPc1	aplikace
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
spouštějí	spouštět	k5eAaImIp3nP	spouštět
v	v	k7c6	v
okně	okno	k1gNnSc6	okno
(	(	kIx(	(
<g/>
a	a	k8xC	a
ne	ne	k9	ne
přes	přes	k7c4	přes
celou	celý	k2eAgFnSc4d1	celá
obrazovku	obrazovka	k1gFnSc4	obrazovka
jako	jako	k8xS	jako
ve	v	k7c6	v
Windows	Windows	kA	Windows
8	[number]	k4	8
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
dotykové	dotykový	k2eAgInPc4d1	dotykový
displeje	displej	k1gInPc4	displej
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
primárně	primárně	k6eAd1	primárně
maximalizované	maximalizovaný	k2eAgNnSc1d1	maximalizované
<g/>
.	.	kIx.	.
</s>
<s>
Úplnou	úplný	k2eAgFnSc7d1	úplná
novinkou	novinka	k1gFnSc7	novinka
je	být	k5eAaImIp3nS	být
tlačítko	tlačítko	k1gNnSc1	tlačítko
Task	Task	k1gInSc1	Task
Bar	bar	k1gInSc1	bar
na	na	k7c6	na
hlavním	hlavní	k2eAgInSc6d1	hlavní
panelu	panel	k1gInSc6	panel
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
správce	správce	k1gMnSc1	správce
virtuálních	virtuální	k2eAgFnPc2d1	virtuální
ploch	plocha	k1gFnPc2	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Microsoft	Microsoft	kA	Microsoft
tento	tento	k3xDgInSc4	tento
systém	systém	k1gInSc4	systém
zatím	zatím	k6eAd1	zatím
prezentoval	prezentovat	k5eAaBmAgMnS	prezentovat
primárně	primárně	k6eAd1	primárně
firmám	firma	k1gFnPc3	firma
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
něž	jenž	k3xRgFnPc4	jenž
přináší	přinášet	k5eAaImIp3nS	přinášet
právě	právě	k9	právě
návrat	návrat	k1gInSc4	návrat
klasického	klasický	k2eAgNnSc2d1	klasické
prostředí	prostředí	k1gNnSc2	prostředí
nebo	nebo	k8xC	nebo
oddělení	oddělení	k1gNnSc2	oddělení
firemních	firemní	k2eAgNnPc2d1	firemní
dat	datum	k1gNnPc2	datum
od	od	k7c2	od
osobních	osobní	k2eAgInPc2d1	osobní
<g/>
.	.	kIx.	.
</s>
<s>
Update	update	k1gInSc1	update
byl	být	k5eAaImAgInS	být
vyvíjen	vyvíjet	k5eAaImNgInS	vyvíjet
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Aniversary	Aniversara	k1gFnSc2	Aniversara
update	update	k1gInSc1	update
(	(	kIx(	(
<g/>
výroční	výroční	k2eAgFnSc1d1	výroční
aktualizace	aktualizace	k1gFnSc1	aktualizace
<g/>
)	)	kIx)	)
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
uveden	uvést	k5eAaPmNgInS	uvést
29	[number]	k4	29
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
ji	on	k3xPp3gFnSc4	on
provázelo	provázet	k5eAaImAgNnS	provázet
několik	několik	k4yIc4	několik
problémů	problém	k1gInPc2	problém
<g/>
.	.	kIx.	.
</s>
<s>
Přinesla	přinést	k5eAaPmAgFnS	přinést
změnu	změna	k1gFnSc4	změna
uživatelského	uživatelský	k2eAgNnSc2d1	Uživatelské
notifikačního	notifikační	k2eAgNnSc2d1	notifikační
centra	centrum	k1gNnSc2	centrum
a	a	k8xC	a
nabídky	nabídka	k1gFnSc2	nabídka
Start	start	k1gInSc1	start
<g/>
,	,	kIx,	,
nové	nový	k2eAgFnSc2d1	nová
animace	animace	k1gFnSc2	animace
<g/>
,	,	kIx,	,
sjednocení	sjednocení	k1gNnSc1	sjednocení
jader	jádro	k1gNnPc2	jádro
s	s	k7c7	s
mobilní	mobilní	k2eAgFnSc7d1	mobilní
verzí	verze	k1gFnSc7	verze
Windows	Windows	kA	Windows
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgFnPc1d1	Nové
univerzální	univerzální	k2eAgFnPc1d1	univerzální
aplikace	aplikace	k1gFnPc1	aplikace
a	a	k8xC	a
například	například	k6eAd1	například
psaní	psaní	k1gNnSc4	psaní
perem	pero	k1gNnSc7	pero
na	na	k7c6	na
dotykovém	dotykový	k2eAgInSc6d1	dotykový
displeji	displej	k1gInSc6	displej
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
Cortany	Cortan	k1gInPc7	Cortan
(	(	kIx(	(
<g/>
Není	být	k5eNaImIp3nS	být
dostupná	dostupný	k2eAgFnSc1d1	dostupná
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
)	)	kIx)	)
lze	lze	k6eAd1	lze
nyní	nyní	k6eAd1	nyní
provázat	provázat	k5eAaPmF	provázat
mobilní	mobilní	k2eAgInSc4d1	mobilní
telefon	telefon	k1gInSc4	telefon
s	s	k7c7	s
PC	PC	kA	PC
<g/>
,	,	kIx,	,
tak	tak	k9	tak
jako	jako	k9	jako
u	u	k7c2	u
konkurenční	konkurenční	k2eAgFnSc2d1	konkurenční
značky	značka	k1gFnSc2	značka
Apple	Apple	kA	Apple
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
měl	mít	k5eAaImAgInS	mít
byt	byt	k1gInSc1	byt
Redstone	Redston	k1gInSc5	Redston
1	[number]	k4	1
uveden	uvést	k5eAaPmNgMnS	uvést
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byl	být	k5eAaImAgInS	být
odložen	odložit	k5eAaPmNgInS	odložit
na	na	k7c4	na
počátek	počátek	k1gInSc4	počátek
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
uveden	uvést	k5eAaPmNgInS	uvést
společně	společně	k6eAd1	společně
s	s	k7c7	s
exkluzivními	exkluzivní	k2eAgFnPc7d1	exkluzivní
novinkami	novinka	k1gFnPc7	novinka
<g/>
.	.	kIx.	.
</s>
<s>
Verze	verze	k1gFnSc1	verze
systémů	systém	k1gInPc2	systém
Windows	Windows	kA	Windows
postupně	postupně	k6eAd1	postupně
přinášely	přinášet	k5eAaImAgFnP	přinášet
různá	různý	k2eAgNnPc4d1	různé
vylepšení	vylepšení	k1gNnSc4	vylepšení
<g/>
.	.	kIx.	.
</s>
<s>
Nejpodstatnější	podstatný	k2eAgMnPc1d3	nejpodstatnější
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
jsou	být	k5eAaImIp3nP	být
zmíněny	zmínit	k5eAaPmNgInP	zmínit
v	v	k7c6	v
následujících	následující	k2eAgFnPc6d1	následující
kapitolách	kapitola	k1gFnPc6	kapitola
<g/>
.	.	kIx.	.
</s>
<s>
Žádný	žádný	k3yNgInSc1	žádný
systém	systém	k1gInSc1	systém
Windows	Windows	kA	Windows
pro	pro	k7c4	pro
DOS	DOS	kA	DOS
neposkytoval	poskytovat	k5eNaImAgMnS	poskytovat
možnost	možnost	k1gFnSc4	možnost
nastavení	nastavení	k1gNnSc2	nastavení
oprávnění	oprávnění	k1gNnSc2	oprávnění
v	v	k7c6	v
souborovém	souborový	k2eAgInSc6d1	souborový
systému	systém	k1gInSc6	systém
FAT	fatum	k1gNnPc2	fatum
na	na	k7c4	na
soubory	soubor	k1gInPc4	soubor
a	a	k8xC	a
adresáře	adresář	k1gInPc4	adresář
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
tyto	tento	k3xDgInPc1	tento
systémy	systém	k1gInPc1	systém
nabízely	nabízet	k5eAaImAgInP	nabízet
možnost	možnost	k1gFnSc4	možnost
definování	definování	k1gNnSc2	definování
uživatelských	uživatelský	k2eAgInPc2d1	uživatelský
profilů	profil	k1gInPc2	profil
pro	pro	k7c4	pro
různé	různý	k2eAgMnPc4d1	různý
uživatele	uživatel	k1gMnPc4	uživatel
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k9	teprve
Windows	Windows	kA	Windows
NT	NT	kA	NT
přinesly	přinést	k5eAaPmAgFnP	přinést
možnost	možnost	k1gFnSc4	možnost
definování	definování	k1gNnSc2	definování
oprávnění	oprávnění	k1gNnSc2	oprávnění
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
souborovém	souborový	k2eAgInSc6d1	souborový
systému	systém	k1gInSc6	systém
NTFS	NTFS	kA	NTFS
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
systému	systém	k1gInSc2	systém
podpora	podpora	k1gFnSc1	podpora
pro	pro	k7c4	pro
rozlišení	rozlišení	k1gNnSc4	rozlišení
jednotlivých	jednotlivý	k2eAgMnPc2d1	jednotlivý
uživatelů	uživatel	k1gMnPc2	uživatel
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc2	jejich
procesů	proces	k1gInPc2	proces
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
omezením	omezení	k1gNnSc7	omezení
jejich	jejich	k3xOp3gFnPc2	jejich
možností	možnost	k1gFnPc2	možnost
zasahovat	zasahovat	k5eAaImF	zasahovat
si	se	k3xPyFc3	se
navzájem	navzájem	k6eAd1	navzájem
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Jádro	jádro	k1gNnSc1	jádro
systémů	systém	k1gInPc2	systém
Windows	Windows	kA	Windows
pro	pro	k7c4	pro
DOS	DOS	kA	DOS
používalo	používat	k5eAaImAgNnS	používat
nepreemptivní	preemptivní	k2eNgInSc4d1	preemptivní
multitasking	multitasking	k1gInSc4	multitasking
(	(	kIx(	(
<g/>
z	z	k7c2	z
marketingových	marketingový	k2eAgInPc2d1	marketingový
důvodů	důvod	k1gInPc2	důvod
byl	být	k5eAaImAgInS	být
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k8xC	jako
kooperativní	kooperativní	k2eAgInSc1d1	kooperativní
multitasking	multitasking	k1gInSc1	multitasking
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Aplikace	aplikace	k1gFnPc1	aplikace
se	se	k3xPyFc4	se
musely	muset	k5eAaImAgFnP	muset
samy	sám	k3xTgFnPc1	sám
vzdát	vzdát	k5eAaPmF	vzdát
procesoru	procesor	k1gInSc2	procesor
pomocí	pomocí	k7c2	pomocí
speciálního	speciální	k2eAgNnSc2d1	speciální
volání	volání	k1gNnSc2	volání
služby	služba	k1gFnSc2	služba
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
byla	být	k5eAaImAgFnS	být
taková	takový	k3xDgFnSc1	takový
aplikace	aplikace	k1gFnSc1	aplikace
špatně	špatně	k6eAd1	špatně
naprogramována	naprogramovat	k5eAaPmNgFnS	naprogramovat
<g/>
,	,	kIx,	,
ponechala	ponechat	k5eAaPmAgFnS	ponechat
si	se	k3xPyFc3	se
procesor	procesor	k1gInSc1	procesor
jen	jen	k9	jen
sama	sám	k3xTgFnSc1	sám
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
a	a	k8xC	a
ostatní	ostatní	k2eAgFnPc1d1	ostatní
aplikace	aplikace	k1gFnPc1	aplikace
i	i	k8xC	i
části	část	k1gFnPc1	část
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
se	se	k3xPyFc4	se
k	k	k7c3	k
procesoru	procesor	k1gInSc3	procesor
nedostaly	dostat	k5eNaPmAgInP	dostat
(	(	kIx(	(
<g/>
neběžely	běžet	k5eNaImAgFnP	běžet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
uživatele	uživatel	k1gMnSc2	uživatel
tak	tak	k6eAd1	tak
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
zaseknutí	zaseknutí	k1gNnSc3	zaseknutí
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
zamrznutí	zamrznutí	k1gNnSc4	zamrznutí
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
počítače	počítač	k1gInPc1	počítač
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
pak	pak	k6eAd1	pak
nefungovala	fungovat	k5eNaImAgFnS	fungovat
ani	ani	k8xC	ani
obsluha	obsluha	k1gFnSc1	obsluha
myši	myš	k1gFnSc2	myš
ani	ani	k8xC	ani
klávesnice	klávesnice	k1gFnSc2	klávesnice
<g/>
.	.	kIx.	.
</s>
<s>
Jádro	jádro	k1gNnSc1	jádro
systémů	systém	k1gInPc2	systém
Windows	Windows	kA	Windows
NT	NT	kA	NT
využívalo	využívat	k5eAaPmAgNnS	využívat
naplno	naplno	k6eAd1	naplno
schopnosti	schopnost	k1gFnSc6	schopnost
procesorů	procesor	k1gInPc2	procesor
386	[number]	k4	386
a	a	k8xC	a
novějších	nový	k2eAgInPc2d2	novější
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
podporovalo	podporovat	k5eAaImAgNnS	podporovat
preemptivní	preemptivní	k2eAgInSc4d1	preemptivní
multitasking	multitasking	k1gInSc4	multitasking
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yQgInSc6	který
jádro	jádro	k1gNnSc1	jádro
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
nikdy	nikdy	k6eAd1	nikdy
neztrácí	ztrácet	k5eNaImIp3nS	ztrácet
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
počítačem	počítač	k1gInSc7	počítač
a	a	k8xC	a
jakoukoliv	jakýkoliv	k3yIgFnSc4	jakýkoliv
úlohu	úloha	k1gFnSc4	úloha
může	moct	k5eAaImIp3nS	moct
násilně	násilně	k6eAd1	násilně
přerušit	přerušit	k5eAaPmF	přerušit
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
ji	on	k3xPp3gFnSc4	on
znovu	znovu	k6eAd1	znovu
spustit	spustit	k5eAaPmF	spustit
(	(	kIx(	(
<g/>
jádro	jádro	k1gNnSc1	jádro
pro	pro	k7c4	pro
plánování	plánování	k1gNnSc4	plánování
procesů	proces	k1gInPc2	proces
využívá	využívat	k5eAaImIp3nS	využívat
privilegovaný	privilegovaný	k2eAgInSc1d1	privilegovaný
režim	režim	k1gInSc1	režim
procesoru	procesor	k1gInSc2	procesor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Systémy	systém	k1gInPc1	systém
Windows	Windows	kA	Windows
pro	pro	k7c4	pro
DOS	DOS	kA	DOS
i	i	k8xC	i
Windows	Windows	kA	Windows
NT	NT	kA	NT
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
definovat	definovat	k5eAaBmF	definovat
v	v	k7c6	v
systému	systém	k1gInSc6	systém
uživatele	uživatel	k1gMnSc2	uživatel
a	a	k8xC	a
používat	používat	k5eAaImF	používat
pro	pro	k7c4	pro
každého	každý	k3xTgMnSc4	každý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
jiný	jiný	k2eAgInSc1d1	jiný
profil	profil	k1gInSc4	profil
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
nastavení	nastavení	k1gNnSc1	nastavení
prostředí	prostředí	k1gNnSc1	prostředí
<g/>
,	,	kIx,	,
nastavení	nastavení	k1gNnSc1	nastavení
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
programů	program	k1gInPc2	program
<g/>
,	,	kIx,	,
vlastní	vlastní	k2eAgInSc4d1	vlastní
domácí	domácí	k2eAgInSc4d1	domácí
adresář	adresář	k1gInSc4	adresář
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Takový	takový	k3xDgInSc1	takový
systém	systém	k1gInSc1	systém
však	však	k9	však
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
víceuživatelský	víceuživatelský	k2eAgInSc4d1	víceuživatelský
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jednotliví	jednotlivý	k2eAgMnPc1d1	jednotlivý
uživatelé	uživatel	k1gMnPc1	uživatel
nepracují	pracovat	k5eNaImIp3nP	pracovat
v	v	k7c6	v
systému	systém	k1gInSc6	systém
zároveň	zároveň	k6eAd1	zároveň
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
postupně	postupně	k6eAd1	postupně
(	(	kIx(	(
<g/>
po	po	k7c6	po
odhlášení	odhlášení	k1gNnSc6	odhlášení
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
přihlásit	přihlásit	k5eAaPmF	přihlásit
jiný	jiný	k2eAgMnSc1d1	jiný
uživatel	uživatel	k1gMnSc1	uživatel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
Windows	Windows	kA	Windows
pro	pro	k7c4	pro
DOS	DOS	kA	DOS
nemohl	moct	k5eNaImAgInS	moct
nabídnout	nabídnout	k5eAaPmF	nabídnout
víceuživatelské	víceuživatelský	k2eAgNnSc4d1	víceuživatelské
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jeho	jeho	k3xOp3gNnSc1	jeho
jádro	jádro	k1gNnSc1	jádro
neumělo	umět	k5eNaImAgNnS	umět
jednotlivé	jednotlivý	k2eAgMnPc4d1	jednotlivý
uživatele	uživatel	k1gMnPc4	uživatel
rozlišit	rozlišit	k5eAaPmF	rozlišit
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
Windows	Windows	kA	Windows
NT	NT	kA	NT
4.0	[number]	k4	4.0
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
již	již	k6eAd1	již
podporoval	podporovat	k5eAaImAgInS	podporovat
systém	systém	k1gInSc1	systém
oprávnění	oprávnění	k1gNnPc2	oprávnění
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
vydán	vydat	k5eAaPmNgMnS	vydat
ve	v	k7c6	v
variantě	varianta	k1gFnSc6	varianta
Terminal	Terminal	k1gFnPc2	Terminal
Server	server	k1gInSc1	server
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
umožňovala	umožňovat	k5eAaImAgFnS	umožňovat
současnou	současný	k2eAgFnSc4d1	současná
práci	práce	k1gFnSc4	práce
se	s	k7c7	s
systémem	systém	k1gInSc7	systém
více	hodně	k6eAd2	hodně
uživatelům	uživatel	k1gMnPc3	uživatel
zároveň	zároveň	k6eAd1	zároveň
pomocí	pomoc	k1gFnSc7	pomoc
Remote	Remot	k1gInSc5	Remot
Desktop	desktop	k1gInSc1	desktop
Services	Services	k1gInSc4	Services
(	(	kIx(	(
<g/>
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
nazývané	nazývaný	k2eAgFnSc2d1	nazývaná
Terminal	Terminal	k1gFnSc2	Terminal
Services	Servicesa	k1gFnPc2	Servicesa
<g/>
)	)	kIx)	)
tak	tak	k9	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
uživatelé	uživatel	k1gMnPc1	uživatel
připojovali	připojovat	k5eAaImAgMnP	připojovat
k	k	k7c3	k
Terminal	Terminal	k1gFnSc3	Terminal
serveru	server	k1gInSc2	server
pomocí	pomocí	k7c2	pomocí
klientského	klientský	k2eAgInSc2d1	klientský
programu	program	k1gInSc2	program
z	z	k7c2	z
jiných	jiný	k2eAgMnPc2d1	jiný
počítačů	počítač	k1gMnPc2	počítač
(	(	kIx(	(
<g/>
komunikace	komunikace	k1gFnSc1	komunikace
probíhá	probíhat	k5eAaImIp3nS	probíhat
pomocí	pomocí	k7c2	pomocí
protokolu	protokol	k1gInSc2	protokol
RDP	RDP	kA	RDP
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
možnost	možnost	k1gFnSc1	možnost
je	být	k5eAaImIp3nS	být
dostupná	dostupný	k2eAgFnSc1d1	dostupná
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
serverové	serverový	k2eAgNnSc4d1	serverové
vydání	vydání	k1gNnSc4	vydání
Windows	Windows	kA	Windows
NT	NT	kA	NT
(	(	kIx(	(
<g/>
poslední	poslední	k2eAgMnSc1d1	poslední
je	být	k5eAaImIp3nS	být
Windows	Windows	kA	Windows
Server	server	k1gInSc4	server
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Desktopové	desktopový	k2eAgInPc1d1	desktopový
systémy	systém	k1gInPc1	systém
(	(	kIx(	(
<g/>
Windows	Windows	kA	Windows
XP	XP	kA	XP
<g/>
,	,	kIx,	,
Windows	Windows	kA	Windows
Vista	vista	k2eAgFnPc2d1	vista
a	a	k8xC	a
Windows	Windows	kA	Windows
7	[number]	k4	7
<g/>
)	)	kIx)	)
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
pouze	pouze	k6eAd1	pouze
částečnou	částečný	k2eAgFnSc4d1	částečná
podporu	podpora	k1gFnSc4	podpora
Remote	Remot	k1gInSc5	Remot
Desktop	desktop	k1gInSc1	desktop
Services	Servicesa	k1gFnPc2	Servicesa
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
okamžiku	okamžik	k1gInSc6	okamžik
přihlášení	přihlášení	k1gNnSc2	přihlášení
pouze	pouze	k6eAd1	pouze
jediného	jediný	k2eAgMnSc2d1	jediný
uživatele	uživatel	k1gMnSc2	uživatel
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Vzdálená	vzdálený	k2eAgFnSc1d1	vzdálená
plocha	plocha	k1gFnSc1	plocha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
toto	tento	k3xDgNnSc4	tento
omezení	omezení	k1gNnSc4	omezení
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
softwarová	softwarový	k2eAgFnSc1d1	softwarová
a	a	k8xC	a
marketingová	marketingový	k2eAgFnSc1d1	marketingová
záležitost	záležitost	k1gFnSc1	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
Ne	ne	k9	ne
vždy	vždy	k6eAd1	vždy
je	být	k5eAaImIp3nS	být
nová	nový	k2eAgFnSc1d1	nová
verze	verze	k1gFnSc1	verze
skutečně	skutečně	k6eAd1	skutečně
lepší	dobrý	k2eAgFnSc1d2	lepší
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
testy	test	k1gInPc1	test
na	na	k7c6	na
hardwaru	hardware	k1gInSc6	hardware
korespondujícím	korespondující	k2eAgFnPc3d1	korespondující
dobám	doba	k1gFnPc3	doba
vydání	vydání	k1gNnSc2	vydání
testovaného	testovaný	k2eAgInSc2d1	testovaný
softwaru	software	k1gInSc2	software
ukázaly	ukázat	k5eAaPmAgInP	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
simulaci	simulace	k1gFnSc6	simulace
práce	práce	k1gFnSc2	práce
pomocí	pomocí	k7c2	pomocí
nástroje	nástroj	k1gInSc2	nástroj
OfficeBench	OfficeBencha	k1gFnPc2	OfficeBencha
byla	být	k5eAaImAgFnS	být
kombinace	kombinace	k1gFnSc1	kombinace
Windows	Windows	kA	Windows
Vista	vista	k2eAgInPc2d1	vista
a	a	k8xC	a
Microsoft	Microsoft	kA	Microsoft
Office	Office	kA	Office
2007	[number]	k4	2007
o	o	k7c4	o
22	[number]	k4	22
%	%	kIx~	%
pomalejší	pomalý	k2eAgMnSc1d2	pomalejší
než	než	k8xS	než
kombinace	kombinace	k1gFnPc1	kombinace
Windows	Windows	kA	Windows
XP	XP	kA	XP
a	a	k8xC	a
Office	Office	kA	Office
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Windows	Windows	kA	Windows
User	usrat	k5eAaPmRp2nS	usrat
Group	Group	k1gMnSc1	Group
Czech	Czech	k1gMnSc1	Czech
Republic	Republice	k1gFnPc2	Republice
(	(	kIx(	(
<g/>
WUG	WUG	kA	WUG
<g/>
)	)	kIx)	)
–	–	k?	–
Sdružení	sdružení	k1gNnSc2	sdružení
administrátorů	administrátor	k1gMnPc2	administrátor
<g/>
,	,	kIx,	,
vývojářů	vývojář	k1gMnPc2	vývojář
<g/>
,	,	kIx,	,
učitelů	učitel	k1gMnPc2	učitel
<g/>
,	,	kIx,	,
studentů	student	k1gMnPc2	student
a	a	k8xC	a
uživatelů	uživatel	k1gMnPc2	uživatel
zabývajících	zabývající	k2eAgMnPc2d1	zabývající
se	s	k7c7	s
platformou	platforma	k1gFnSc7	platforma
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
pořádá	pořádat	k5eAaImIp3nS	pořádat
pravidelné	pravidelný	k2eAgInPc4d1	pravidelný
meetingy	meeting	k1gInPc4	meeting
Microsoft	Microsoft	kA	Microsoft
TechNet	TechNet	k1gInSc4	TechNet
Blog	Bloga	k1gFnPc2	Bloga
CZ	CZ	kA	CZ
<g/>
/	/	kIx~	/
<g/>
SK	Sk	kA	Sk
–	–	k?	–
Technický	technický	k2eAgInSc4d1	technický
blog	blog	k1gInSc4	blog
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yQgInSc4	který
přispívají	přispívat	k5eAaImIp3nP	přispívat
lidé	člověk	k1gMnPc1	člověk
z	z	k7c2	z
českého	český	k2eAgInSc2d1	český
Microsoftu	Microsoft	k1gInSc2	Microsoft
a	a	k8xC	a
MVP	MVP	kA	MVP
Stránky	stránka	k1gFnSc2	stránka
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
Embedded	Embedded	k1gMnSc1	Embedded
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
