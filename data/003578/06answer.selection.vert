<s>
Všechny	všechen	k3xTgInPc1	všechen
mají	mít	k5eAaImIp3nP	mít
grafické	grafický	k2eAgNnSc4d1	grafické
uživatelské	uživatelský	k2eAgNnSc4d1	Uživatelské
rozhraní	rozhraní	k1gNnSc4	rozhraní
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
liší	lišit	k5eAaImIp3nP	lišit
se	se	k3xPyFc4	se
použitým	použitý	k2eAgNnSc7d1	Použité
jádrem	jádro	k1gNnSc7	jádro
<g/>
,	,	kIx,	,
úrovní	úroveň	k1gFnSc7	úroveň
podpory	podpora	k1gFnSc2	podpora
multitaskingu	multitasking	k1gInSc2	multitasking
(	(	kIx(	(
<g/>
současného	současný	k2eAgInSc2d1	současný
běhu	běh	k1gInSc2	běh
více	hodně	k6eAd2	hodně
úloh	úloha	k1gFnPc2	úloha
najednou	najednou	k6eAd1	najednou
<g/>
)	)	kIx)	)
i	i	k9	i
používanými	používaný	k2eAgFnPc7d1	používaná
knihovnami	knihovna	k1gFnPc7	knihovna
a	a	k8xC	a
účelem	účel	k1gInSc7	účel
použití	použití	k1gNnSc2	použití
<g/>
.	.	kIx.	.
</s>
