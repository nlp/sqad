<s>
Vůdcův	vůdcův	k2eAgInSc1d1
bunkr	bunkr	k1gInSc1
(	(	kIx(
Führerbunker	Führerbunker	k1gInSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
podzemní	podzemní	k2eAgInSc1d1
komplex	komplex	k1gInSc1
v	v	k7c6
Berlíně	Berlín	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
na	na	k7c6
konci	konec	k1gInSc6
války	válka	k1gFnSc2
ukrývali	ukrývat	k5eAaImAgMnP
Adolf	Adolf	k1gMnSc1
Hitler	Hitler	k1gMnSc1
a	a	k8xC
Eva	Eva	k1gFnSc1
Braunová	Braunová	k1gFnSc1
a	a	k8xC
kde	kde	k6eAd1
nakonec	nakonec	k6eAd1
spáchali	spáchat	k5eAaPmAgMnP
sebevraždu	sebevražda	k1gFnSc4
<g/>
.	.	kIx.
</s>