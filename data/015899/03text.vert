<s>
Vůdcův	vůdcův	k2eAgInSc1d1
bunkr	bunkr	k1gInSc1
</s>
<s>
Vůdcův	vůdcův	k2eAgInSc1d1
bunkr	bunkr	k1gInSc1
Základní	základní	k2eAgFnSc2d1
informace	informace	k1gFnSc2
Architekti	architekt	k1gMnPc1
</s>
<s>
Albert	Albert	k1gMnSc1
Speer	Speer	k1gMnSc1
a	a	k8xC
Hochtief	Hochtief	k1gMnSc1
(	(	kIx(
<g/>
architektonická	architektonický	k2eAgFnSc1d1
kancelář	kancelář	k1gFnSc1
<g/>
)	)	kIx)
Výstavba	výstavba	k1gFnSc1
</s>
<s>
1944	#num#	k4
Poloha	poloha	k1gFnSc1
Adresa	adresa	k1gFnSc1
</s>
<s>
Berlín	Berlín	k1gInSc1
<g/>
,	,	kIx,
Německo	Německo	k1gNnSc1
Německo	Německo	k1gNnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
52	#num#	k4
<g/>
°	°	k?
<g/>
30	#num#	k4
<g/>
′	′	k?
<g/>
45	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
13	#num#	k4
<g/>
°	°	k?
<g/>
22	#num#	k4
<g/>
′	′	k?
<g/>
53,4	53,4	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Model	model	k1gInSc1
bunkru	bunkr	k1gInSc2
</s>
<s>
Informační	informační	k2eAgFnPc1d1
tabule	tabule	k1gFnPc1
na	na	k7c6
místě	místo	k1gNnSc6
vůdcova	vůdcův	k2eAgInSc2d1
bunkru	bunkr	k1gInSc2
</s>
<s>
Vůdcův	vůdcův	k2eAgInSc1d1
bunkr	bunkr	k1gInSc1
(	(	kIx(
Führerbunker	Führerbunker	k1gInSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
podzemní	podzemní	k2eAgInSc1d1
komplex	komplex	k1gInSc1
v	v	k7c6
Berlíně	Berlín	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
na	na	k7c6
konci	konec	k1gInSc6
války	válka	k1gFnSc2
ukrývali	ukrývat	k5eAaImAgMnP
Adolf	Adolf	k1gMnSc1
Hitler	Hitler	k1gMnSc1
a	a	k8xC
Eva	Eva	k1gFnSc1
Braunová	Braunová	k1gFnSc1
a	a	k8xC
kde	kde	k6eAd1
nakonec	nakonec	k6eAd1
spáchali	spáchat	k5eAaPmAgMnP
sebevraždu	sebevražda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
bunkr	bunkr	k1gInSc4
představuje	představovat	k5eAaImIp3nS
jedno	jeden	k4xCgNnSc1
z	z	k7c2
nejznámějších	známý	k2eAgNnPc2d3
Hitlerových	Hitlerových	k2eAgNnPc2d1
sídel	sídlo	k1gNnPc2
<g/>
,	,	kIx,
mezi	mezi	k7c4
další	další	k2eAgFnSc4d1
patří	patřit	k5eAaImIp3nS
například	například	k6eAd1
Vlčí	vlčí	k2eAgNnSc4d1
doupě	doupě	k1gNnSc4
v	v	k7c6
dnešním	dnešní	k2eAgNnSc6d1
Polsku	Polsko	k1gNnSc6
nebo	nebo	k8xC
Orlí	orlí	k2eAgNnSc1d1
hnízdo	hnízdo	k1gNnSc1
v	v	k7c6
bavorských	bavorský	k2eAgFnPc6d1
Alpách	Alpy	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
O	o	k7c6
životě	život	k1gInSc6
v	v	k7c6
bunkru	bunkr	k1gInSc6
vypovídá	vypovídat	k5eAaImIp3nS,k5eAaPmIp3nS
film	film	k1gInSc1
Pád	Pád	k1gInSc1
Třetí	třetí	k4xOgFnSc2
říše	říš	k1gFnSc2
z	z	k7c2
roku	rok	k1gInSc2
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Starý	starý	k2eAgInSc1d1
bunkr	bunkr	k1gInSc1
</s>
<s>
Komplex	komplex	k1gInSc1
byl	být	k5eAaImAgInS
vybudován	vybudovat	k5eAaPmNgInS
v	v	k7c6
několika	několik	k4yIc6
etapách	etapa	k1gFnPc6
na	na	k7c6
zahradě	zahrada	k1gFnSc6
starého	starý	k2eAgNnSc2d1
říšského	říšský	k2eAgNnSc2d1
kancléřství	kancléřství	k1gNnSc2
na	na	k7c4
Wilhelmstraße	Wilhelmstraße	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yRgNnSc4,k3yIgNnSc4,k3yQgNnSc4
Adolf	Adolf	k1gMnSc1
Hitler	Hitler	k1gMnSc1
používal	používat	k5eAaImAgMnS
jako	jako	k9
svoji	svůj	k3xOyFgFnSc4
hlavní	hlavní	k2eAgFnSc4d1
soukromou	soukromý	k2eAgFnSc4d1
rezidenci	rezidence	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
nutných	nutný	k2eAgFnPc6d1
přestavbách	přestavba	k1gFnPc6
nevyhovujících	vyhovující	k2eNgFnPc2d1
vnitřních	vnitřní	k2eAgFnPc2d1
prostor	prostora	k1gFnPc2
bylo	být	k5eAaImAgNnS
v	v	k7c6
roce	rok	k1gInSc6
1935	#num#	k4
započato	započnout	k5eAaPmNgNnS
s	s	k7c7
rekonstrukcí	rekonstrukce	k1gFnSc7
velkého	velký	k2eAgInSc2d1
Přijímacího	přijímací	k2eAgInSc2d1
sálu	sál	k1gInSc2
(	(	kIx(
<g/>
v	v	k7c6
některých	některý	k3yIgInPc6
dokumentech	dokument	k1gInPc6
je	být	k5eAaImIp3nS
uváděn	uvádět	k5eAaImNgInS
jako	jako	k8xC,k8xS
Diplomatický	diplomatický	k2eAgInSc1d1
sál	sál	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
přiléhající	přiléhající	k2eAgFnPc4d1
zimní	zimní	k2eAgFnPc4d1
zahrady	zahrada	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současně	současně	k6eAd1
byla	být	k5eAaImAgFnS
zahájena	zahájit	k5eAaPmNgFnS
stavba	stavba	k1gFnSc1
betonového	betonový	k2eAgInSc2d1
úkrytu	úkryt	k1gInSc2
v	v	k7c6
části	část	k1gFnSc6
stávajících	stávající	k2eAgInPc2d1
sklepů	sklep	k1gInPc2
a	a	k8xC
rozšířením	rozšíření	k1gNnSc7
do	do	k7c2
zahrady	zahrada	k1gFnSc2
kancléřství	kancléřství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Není	být	k5eNaImIp3nS
zcela	zcela	k6eAd1
přesně	přesně	k6eAd1
zdokumentováno	zdokumentovat	k5eAaPmNgNnS
<g/>
,	,	kIx,
z	z	k7c2
jakého	jaký	k3yRgInSc2,k3yIgInSc2,k3yQgInSc2
popudu	popud	k1gInSc2
či	či	k8xC
pod	pod	k7c7
jakou	jaký	k3yQgFnSc7,k3yRgFnSc7,k3yIgFnSc7
záminkou	záminka	k1gFnSc7
bylo	být	k5eAaImAgNnS
o	o	k7c6
této	tento	k3xDgFnSc6
stavbě	stavba	k1gFnSc6
rozhodnuto	rozhodnout	k5eAaPmNgNnS
<g/>
,	,	kIx,
ale	ale	k8xC
dá	dát	k5eAaPmIp3nS
se	se	k3xPyFc4
dedukovat	dedukovat	k5eAaImF
o	o	k7c6
určitém	určitý	k2eAgInSc6d1
vlivu	vliv	k1gInSc6
architekta	architekt	k1gMnSc2
Speera	Speer	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
etapa	etapa	k1gFnSc1
stavby	stavba	k1gFnSc2
byla	být	k5eAaImAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1936	#num#	k4
dokončena	dokončit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původní	původní	k2eAgFnSc1d1
zastavěná	zastavěný	k2eAgFnSc1d1
plocha	plocha	k1gFnSc1
krytu	kryt	k1gInSc2
(	(	kIx(
<g/>
bunkru	bunkr	k1gInSc2
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
21,5	21,5	k4
x	x	k?
21,5	21,5	k4
m	m	kA
s	s	k7c7
mocností	mocnost	k1gFnSc7
železobetonových	železobetonový	k2eAgFnPc2d1
obvodových	obvodový	k2eAgFnPc2d1
stěn	stěna	k1gFnPc2
a	a	k8xC
stropu	strop	k1gInSc2
1,2	1,2	k4
až	až	k9
1,6	1,6	k4
m.	m.	k?
Obyvatelný	obyvatelný	k2eAgInSc1d1
prostor	prostor	k1gInSc1
byl	být	k5eAaImAgInS
cca	cca	kA
18	#num#	k4
x	x	k?
19	#num#	k4
m.	m.	k?
Dodatečně	dodatečně	k6eAd1
byl	být	k5eAaImAgInS
strop	strop	k1gInSc1
zesílen	zesílit	k5eAaPmNgInS
vrstvou	vrstva	k1gFnSc7
železobetonu	železobeton	k1gInSc2
o	o	k7c6
mocnosti	mocnost	k1gFnSc6
cca	cca	kA
1,0	1,0	k4
m.	m.	k?
S	s	k7c7
přihlédnutím	přihlédnutí	k1gNnSc7
na	na	k7c4
stavební	stavební	k2eAgFnPc4d1
úpravy	úprava	k1gFnPc4
v	v	k7c6
zastavěné	zastavěný	k2eAgFnSc6d1
ploše	plocha	k1gFnSc6
starého	starý	k2eAgNnSc2d1
kancléřství	kancléřství	k1gNnSc2
přesahovala	přesahovat	k5eAaImAgFnS
mocnost	mocnost	k1gFnSc1
stropu	strop	k1gInSc2
3,0	3,0	k4
m	m	kA
a	a	k8xC
odolala	odolat	k5eAaPmAgFnS
by	by	kYmCp3nS
jakékoliv	jakýkoliv	k3yIgNnSc1
tehdejší	tehdejší	k2eAgInSc1d1
letecké	letecký	k2eAgFnSc3d1
pumě	puma	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
tohoto	tento	k3xDgInSc2
krytu	kryt	k1gInSc2
byl	být	k5eAaImAgInS
přístup	přístup	k1gInSc1
ze	z	k7c2
sklepa	sklep	k1gInSc2
pod	pod	k7c7
Slavnostním	slavnostní	k2eAgInSc7d1
sálem	sál	k1gInSc7
<g/>
,	,	kIx,
kam	kam	k6eAd1
se	se	k3xPyFc4
vcházelo	vcházet	k5eAaImAgNnS
bočním	boční	k2eAgNnSc7d1
schodištěm	schodiště	k1gNnSc7
na	na	k7c6
spojovací	spojovací	k2eAgFnSc6d1
chodbě	chodba	k1gFnSc6
mezi	mezi	k7c7
jídelnou	jídelna	k1gFnSc7
a	a	k8xC
zimní	zimní	k2eAgFnSc7d1
zahradou	zahrada	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
bunkru	bunkr	k1gInSc2
byl	být	k5eAaImAgInS
vyveden	vyveden	k2eAgInSc1d1
ještě	ještě	k6eAd1
nouzový	nouzový	k2eAgInSc1d1
únikový	únikový	k2eAgInSc1d1
východ	východ	k1gInSc1
<g/>
,	,	kIx,
osazený	osazený	k2eAgInSc1d1
těžkými	těžký	k2eAgInPc7d1
pancéřovými	pancéřový	k2eAgInPc7d1
vraty	vrat	k1gInPc7
se	s	k7c7
vzduchotěsnými	vzduchotěsný	k2eAgFnPc7d1
přepážkami	přepážka	k1gFnPc7
a	a	k8xC
ventilačním	ventilační	k2eAgInSc7d1
systémem	systém	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celý	celý	k2eAgInSc1d1
bunkr	bunkr	k1gInSc1
byl	být	k5eAaImAgInS
energeticky	energeticky	k6eAd1
soběstačný	soběstačný	k2eAgInSc1d1
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
byl	být	k5eAaImAgInS
napájen	napájet	k5eAaImNgInS
dieselelektrickým	dieselelektrický	k2eAgInSc7d1
agregátem	agregát	k1gInSc7
napojeným	napojený	k2eAgInSc7d1
na	na	k7c4
ventilační	ventilační	k2eAgInSc4d1
systém	systém	k1gInSc4
i	i	k9
na	na	k7c4
systém	systém	k1gInSc4
vodovodu	vodovod	k1gInSc2
a	a	k8xC
kanalizace	kanalizace	k1gFnSc2
<g/>
,	,	kIx,
protože	protože	k8xS
kanalizační	kanalizační	k2eAgFnSc1d1
jímka	jímka	k1gFnSc1
bunkru	bunkr	k1gInSc2
byla	být	k5eAaImAgFnS
umístěna	umístit	k5eAaPmNgFnS
pod	pod	k7c7
úrovní	úroveň	k1gFnSc7
uliční	uliční	k2eAgFnSc2d1
kanalizační	kanalizační	k2eAgFnSc2d1
sítě	síť	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
bunkru	bunkr	k1gInSc6
bylo	být	k5eAaImAgNnS
celkem	celkem	k6eAd1
12	#num#	k4
místností	místnost	k1gFnPc2
s	s	k7c7
centrální	centrální	k2eAgFnSc7d1
chodbou	chodba	k1gFnSc7
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
po	po	k7c6
přestavbě	přestavba	k1gFnSc6
sloužila	sloužit	k5eAaImAgFnS
jako	jako	k9
jídelna	jídelna	k1gFnSc1
a	a	k8xC
zároveň	zároveň	k6eAd1
i	i	k9
společenská	společenský	k2eAgFnSc1d1
místnost	místnost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Část	část	k1gFnSc1
stávajících	stávající	k2eAgFnPc2d1
zrekonstruovaných	zrekonstruovaný	k2eAgFnPc2d1
sklepních	sklepní	k2eAgFnPc2d1
prostor	prostora	k1gFnPc2
pod	pod	k7c7
starým	starý	k2eAgNnSc7d1
kancléřstvím	kancléřství	k1gNnSc7
sloužila	sloužit	k5eAaImAgFnS
až	až	k6eAd1
do	do	k7c2
konce	konec	k1gInSc2
jako	jako	k8xS,k8xC
proviantní	proviantní	k2eAgFnSc1d1
a	a	k8xC
technická	technický	k2eAgFnSc1d1
zásobárna	zásobárna	k1gFnSc1
bunkru	bunkr	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Nový	nový	k2eAgInSc1d1
bunkr	bunkr	k1gInSc1
</s>
<s>
V	v	k7c6
průběhu	průběh	k1gInSc6
let	léto	k1gNnPc2
1942	#num#	k4
až	až	k8xS
1943	#num#	k4
bylo	být	k5eAaImAgNnS
započato	započat	k2eAgNnSc1d1
s	s	k7c7
výstavbou	výstavba	k1gFnSc7
druhé	druhý	k4xOgFnSc2
části	část	k1gFnSc2
vůdcova	vůdcův	k2eAgInSc2d1
bunkru	bunkr	k1gInSc2
<g/>
,	,	kIx,
jenž	jenž	k3xRgInSc1
byl	být	k5eAaImAgInS
budován	budovat	k5eAaImNgInS
dále	daleko	k6eAd2
do	do	k7c2
zahrady	zahrada	k1gFnSc2
říšského	říšský	k2eAgNnSc2d1
kancléřství	kancléřství	k1gNnSc2
a	a	k8xC
téměř	téměř	k6eAd1
kopíroval	kopírovat	k5eAaImAgInS
ulici	ulice	k1gFnSc4
Voßstraße	Voßstraß	k1gInSc2
a	a	k8xC
na	na	k7c6
druhé	druhý	k4xOgFnSc6
straně	strana	k1gFnSc6
byl	být	k5eAaImAgInS
ohraničen	ohraničit	k5eAaPmNgInS
budovou	budova	k1gFnSc7
nového	nový	k2eAgNnSc2d1
kancléřství	kancléřství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dokumenty	dokument	k1gInPc7
uvádějí	uvádět	k5eAaImIp3nP
stavební	stavební	k2eAgFnSc4d1
jámu	jáma	k1gFnSc4
o	o	k7c6
hloubce	hloubka	k1gFnSc6
přesahující	přesahující	k2eAgFnSc1d1
10	#num#	k4
m.	m.	k?
Vstup	vstup	k1gInSc1
do	do	k7c2
spodního	spodní	k2eAgInSc2d1
vůdcova	vůdcův	k2eAgInSc2d1
bunkru	bunkr	k1gInSc2
byl	být	k5eAaImAgMnS
jednak	jednak	k8xC
točitým	točitý	k2eAgNnSc7d1
schodištěm	schodiště	k1gNnSc7
ze	z	k7c2
„	„	k?
<g/>
starého	starý	k2eAgInSc2d1
bunkru	bunkr	k1gInSc2
<g/>
“	“	k?
původní	původní	k2eAgFnSc2d1
stavby	stavba	k1gFnSc2
z	z	k7c2
roku	rok	k1gInSc2
1936	#num#	k4
a	a	k8xC
dále	daleko	k6eAd2
pak	pak	k6eAd1
schodištěm	schodiště	k1gNnSc7
s	s	k7c7
kasematovými	kasematův	k2eAgFnPc7d1
střílnami	střílna	k1gFnPc7
a	a	k8xC
plynovými	plynový	k2eAgInPc7d1
uzávěry	uzávěr	k1gInPc7
ze	z	k7c2
zahrady	zahrada	k1gFnSc2
kancléřství	kancléřství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyústění	vyústění	k1gNnPc1
do	do	k7c2
zahrady	zahrada	k1gFnSc2
bylo	být	k5eAaImAgNnS
kryto	krýt	k5eAaImNgNnS
pancéřovými	pancéřový	k2eAgFnPc7d1
dveřmi	dveře	k1gFnPc7
<g/>
,	,	kIx,
bočními	boční	k2eAgFnPc7d1
kasematovými	kasematův	k2eAgFnPc7d1
střílnami	střílna	k1gFnPc7
a	a	k8xC
strážní	strážní	k2eAgFnSc7d1
věží	věž	k1gFnSc7
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
byla	být	k5eAaImAgFnS
zaústěna	zaústěn	k2eAgFnSc1d1
ventilace	ventilace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c4
přepracování	přepracování	k1gNnSc4
projektu	projekt	k1gInSc2
týmem	tým	k1gInSc7
projektantů	projektant	k1gMnPc2
vedeným	vedený	k2eAgNnSc7d1
Albertem	Albert	k1gMnSc7
Speerem	Speero	k1gNnSc7
byly	být	k5eAaImAgFnP
zdi	zeď	k1gFnPc1
a	a	k8xC
stropy	strop	k1gInPc1
zbudované	zbudovaný	k2eAgInPc1d1
ze	z	k7c2
železobetonu	železobeton	k1gInSc2
zesíleny	zesílen	k2eAgFnPc4d1
na	na	k7c4
mocnost	mocnost	k1gFnSc4
3,5	3,5	k4
m.	m.	k?
Ani	ani	k8xC
takto	takto	k6eAd1
zbudované	zbudovaný	k2eAgFnPc1d1
železobetonové	železobetonový	k2eAgFnPc1d1
zdi	zeď	k1gFnPc1
nebyly	být	k5eNaImAgFnP
schopny	schopen	k2eAgFnPc1d1
vzdorovat	vzdorovat	k5eAaImF
průsakům	průsak	k1gInPc3
spodní	spodní	k2eAgFnSc2d1
vody	voda	k1gFnSc2
do	do	k7c2
obytného	obytný	k2eAgInSc2d1
prostoru	prostor	k1gInSc2
bunkru	bunkr	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
řešeno	řešen	k2eAgNnSc1d1
instalací	instalace	k1gFnSc7
výkonných	výkonný	k2eAgNnPc2d1
čerpadel	čerpadlo	k1gNnPc2
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc1
prosakující	prosakující	k2eAgInPc1d1
vodu	voda	k1gFnSc4
jímanou	jímaný	k2eAgFnSc4d1
do	do	k7c2
průsakových	průsakový	k2eAgFnPc2d1
jímek	jímka	k1gFnPc2
permanentně	permanentně	k6eAd1
odčerpávaly	odčerpávat	k5eAaImAgFnP
do	do	k7c2
kanalizačního	kanalizační	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
přes	přes	k7c4
toto	tento	k3xDgNnSc4
opatření	opatření	k1gNnSc4
byla	být	k5eAaImAgFnS
v	v	k7c6
bunkru	bunkr	k1gInSc6
velmi	velmi	k6eAd1
vysoká	vysoký	k2eAgFnSc1d1
vlhkost	vlhkost	k1gFnSc1
vzduchu	vzduch	k1gInSc2
a	a	k8xC
poměrně	poměrně	k6eAd1
rozsáhlé	rozsáhlý	k2eAgNnSc1d1
zaplísnění	zaplísnění	k1gNnSc1
stěn	stěna	k1gFnPc2
(	(	kIx(
<g/>
svědectví	svědectví	k1gNnSc1
přeživších	přeživší	k2eAgMnPc2d1
obyvatel	obyvatel	k1gMnPc2
bunkru	bunkr	k1gInSc2
nebo	nebo	k8xC
návštěvníků	návštěvník	k1gMnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
přes	přes	k7c4
dobře	dobře	k6eAd1
fungující	fungující	k2eAgInSc4d1
ventilační	ventilační	k2eAgInSc4d1
systém	systém	k1gInSc4
byl	být	k5eAaImAgInS
v	v	k7c6
bunkru	bunkr	k1gInSc6
stálý	stálý	k2eAgInSc4d1
zápach	zápach	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Technologické	technologický	k2eAgNnSc4d1
zařízení	zařízení	k1gNnPc4
měl	mít	k5eAaImAgInS
horní	horní	k2eAgInSc4d1
i	i	k8xC
spodní	spodní	k2eAgInSc4d1
bunkr	bunkr	k1gInSc4
samostatné	samostatný	k2eAgNnSc1d1
<g/>
,	,	kIx,
od	od	k7c2
sebe	sebe	k3xPyFc4
oddělené	oddělený	k2eAgInPc1d1
a	a	k8xC
na	na	k7c6
sobě	sebe	k3xPyFc6
nezávisle	závisle	k6eNd1
pracující	pracující	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spodní	spodní	k2eAgInSc1d1
bunkr	bunkr	k1gInSc1
byl	být	k5eAaImAgInS
stavebně	stavebně	k6eAd1
dokončen	dokončit	k5eAaPmNgInS
teprve	teprve	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1944	#num#	k4
a	a	k8xC
okamžitě	okamžitě	k6eAd1
obsazen	obsadit	k5eAaPmNgInS
stálou	stálý	k2eAgFnSc7d1
osádkou	osádka	k1gFnSc7
obsluhy	obsluha	k1gFnSc2
pod	pod	k7c7
velením	velení	k1gNnSc7
vrchního	vrchní	k2eAgMnSc2d1
strojníka	strojník	k1gMnSc2
Hentschela	Hentschel	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rovněž	rovněž	k6eAd1
pokoje	pokoj	k1gInPc4
nebo	nebo	k8xC
prostory	prostor	k1gInPc4
určené	určený	k2eAgInPc4d1
pro	pro	k7c4
Begleitkommando	Begleitkommanda	k1gFnSc5
(	(	kIx(
<g/>
tělesná	tělesný	k2eAgFnSc1d1
stáž	stáž	k1gFnSc1
vůdce	vůdce	k1gMnSc1
<g/>
)	)	kIx)
byly	být	k5eAaImAgFnP
trvale	trvale	k6eAd1
obsazeny	obsadit	k5eAaPmNgInP
a	a	k8xC
udržovány	udržovat	k5eAaImNgInP
v	v	k7c6
pohotovostním	pohotovostní	k2eAgInSc6d1
režimu	režim	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spojovací	spojovací	k2eAgInSc1d1
uzel	uzel	k1gInSc1
s	s	k7c7
nejmodernější	moderní	k2eAgFnSc7d3
tehdejší	tehdejší	k2eAgFnSc7d1
dostupnou	dostupný	k2eAgFnSc7d1
spojovací	spojovací	k2eAgFnSc7d1
technikou	technika	k1gFnSc7
byl	být	k5eAaImAgInS
nainstalován	nainstalován	k2eAgInSc1d1
a	a	k8xC
plně	plně	k6eAd1
provozuschopný	provozuschopný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
provoz	provoz	k1gInSc4
se	se	k3xPyFc4
od	od	k7c2
samého	samý	k3xTgInSc2
počátku	počátek	k1gInSc2
staral	starat	k5eAaImAgMnS
jeden	jeden	k4xCgMnSc1
z	z	k7c2
přeživších	přeživší	k2eAgMnPc2d1
stálých	stálý	k2eAgMnPc2d1
obyvatel	obyvatel	k1gMnPc2
bunkru	bunkr	k1gInSc2
SS-Obergefreiter	SS-Obergefreiter	k1gMnSc1
Rochus	Rochus	k1gMnSc1
Misch	Misch	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
bunkru	bunkr	k1gInSc6
byly	být	k5eAaImAgFnP
pokoje	pokoj	k1gInPc4
pro	pro	k7c4
jednak	jednak	k8xC
stálé	stálý	k2eAgMnPc4d1
obyvatele	obyvatel	k1gMnPc4
<g/>
,	,	kIx,
příznivce	příznivec	k1gMnPc4
a	a	k8xC
blízké	blízký	k2eAgMnPc4d1
spolupracovníky	spolupracovník	k1gMnPc4
vůdce	vůdce	k1gMnSc4
<g/>
,	,	kIx,
včetně	včetně	k7c2
administrativy	administrativa	k1gFnSc2
<g/>
,	,	kIx,
dále	daleko	k6eAd2
pak	pak	k6eAd1
pokoje	pokoj	k1gInPc1
pro	pro	k7c4
návštěvy	návštěva	k1gFnPc4
<g/>
,	,	kIx,
konferenční	konferenční	k2eAgFnSc1d1
místnost	místnost	k1gFnSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
Hitler	Hitler	k1gMnSc1
pořádal	pořádat	k5eAaImAgMnS
porady	porada	k1gFnPc4
(	(	kIx(
<g/>
poslední	poslední	k2eAgFnPc4d1
22	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1945	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
lékařský	lékařský	k2eAgInSc1d1
pokoj	pokoj	k1gInSc1
a	a	k8xC
sanitární	sanitární	k2eAgFnSc1d1
místnost	místnost	k1gFnSc1
a	a	k8xC
ubikace	ubikace	k1gFnSc1
strážního	strážní	k2eAgInSc2d1
oddílu	oddíl	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zvláštní	zvláštní	k2eAgInPc1d1
pokoje	pokoj	k1gInPc1
s	s	k7c7
příslušenstvím	příslušenství	k1gNnSc7
zde	zde	k6eAd1
měla	mít	k5eAaImAgFnS
Eva	Eva	k1gFnSc1
Braunová	Braunová	k1gFnSc1
<g/>
,	,	kIx,
manželé	manžel	k1gMnPc1
Goebbelsovi	Goebbelsův	k2eAgMnPc1d1
a	a	k8xC
„	„	k?
<g/>
dvorní	dvorní	k2eAgFnSc2d1
<g/>
“	“	k?
lékař	lékař	k1gMnSc1
dr	dr	kA
<g/>
.	.	kIx.
Morell	Morell	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dubnu	duben	k1gInSc6
1945	#num#	k4
v	v	k7c6
bunkru	bunkr	k1gInSc6
krátce	krátce	k6eAd1
či	či	k8xC
déle	dlouho	k6eAd2
pobývalo	pobývat	k5eAaImAgNnS
až	až	k9
50	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
vznášelo	vznášet	k5eAaImAgNnS
velké	velký	k2eAgInPc4d1
nároky	nárok	k1gInPc4
především	především	k9
na	na	k7c4
inženýrské	inženýrský	k2eAgFnPc4d1
sítě	síť	k1gFnPc4
a	a	k8xC
vzduchotechniku	vzduchotechnika	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Konec	konec	k1gInSc1
války	válka	k1gFnSc2
</s>
<s>
Při	při	k7c6
bojích	boj	k1gInPc6
o	o	k7c4
vládní	vládní	k2eAgFnSc4d1
čtvrť	čtvrť	k1gFnSc4
Berlína	Berlín	k1gInSc2
v	v	k7c6
dubnu	duben	k1gInSc6
a	a	k8xC
květnu	květen	k1gInSc6
1945	#num#	k4
bylo	být	k5eAaImAgNnS
staré	starý	k2eAgNnSc1d1
i	i	k8xC
nové	nový	k2eAgNnSc1d1
kancléřství	kancléřství	k1gNnSc1
bojovými	bojový	k2eAgFnPc7d1
operacemi	operace	k1gFnPc7
a	a	k8xC
především	především	k6eAd1
přímou	přímý	k2eAgFnSc7d1
nebo	nebo	k8xC
nepřímou	přímý	k2eNgFnSc7d1
palbou	palba	k1gFnSc7
dělostřelectva	dělostřelectvo	k1gNnSc2
velmi	velmi	k6eAd1
poškozeno	poškozen	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bunkr	bunkr	k1gInSc4
však	však	k9
žádné	žádný	k3yNgNnSc4
poškození	poškození	k1gNnSc4
neutrpěl	utrpět	k5eNaPmAgInS
vyjma	vyjma	k7c2
několika	několik	k4yIc2
zásahů	zásah	k1gInPc2
nejspíše	nejspíše	k9
minometnými	minometný	k2eAgFnPc7d1
střelami	střela	k1gFnPc7
do	do	k7c2
kopulí	kopule	k1gFnPc2
strážních	strážní	k2eAgFnPc2d1
(	(	kIx(
<g/>
ventilačních	ventilační	k2eAgFnPc2d1
<g/>
)	)	kIx)
věží	věž	k1gFnPc2
v	v	k7c6
zahradě	zahrada	k1gFnSc6
kancléřství	kancléřství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koncem	koncem	k7c2
dubna	duben	k1gInSc2
1945	#num#	k4
byl	být	k5eAaImAgInS
horní	horní	k2eAgInSc1d1
bunkr	bunkr	k1gInSc1
téměř	téměř	k6eAd1
celý	celý	k2eAgInSc4d1
využíván	využíván	k2eAgInSc4d1
jako	jako	k8xC,k8xS
polní	polní	k2eAgInSc4d1
lazaret	lazaret	k1gInSc4
pod	pod	k7c7
velením	velení	k1gNnSc7
profesora	profesor	k1gMnSc2
Hasseho	Hasse	k1gMnSc2
a	a	k8xC
skupinou	skupina	k1gFnSc7
zdravotních	zdravotní	k2eAgFnPc2d1
sester	sestra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
brzkých	brzký	k2eAgFnPc6d1
ranních	ranní	k2eAgFnPc6d1
hodinách	hodina	k1gFnPc6
2	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1945	#num#	k4
pronikli	proniknout	k5eAaPmAgMnP
první	první	k4xOgMnPc1
vojáci	voják	k1gMnPc1
Rudé	rudý	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
do	do	k7c2
prostor	prostora	k1gFnPc2
zdevastovaného	zdevastovaný	k2eAgNnSc2d1
starého	starý	k2eAgNnSc2d1
říšského	říšský	k2eAgNnSc2d1
kancléřství	kancléřství	k1gNnSc2
a	a	k8xC
zároveň	zároveň	k6eAd1
z	z	k7c2
ulice	ulice	k1gFnSc2
Voßstraße	Voßstraß	k1gFnSc2
do	do	k7c2
zahrady	zahrada	k1gFnSc2
říšského	říšský	k2eAgNnSc2d1
kancléřství	kancléřství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
přes	přes	k7c4
přítomnost	přítomnost	k1gFnSc4
několika	několik	k4yIc2
příslušníků	příslušník	k1gMnPc2
obávané	obávaný	k2eAgFnSc2d1
divize	divize	k1gFnSc2
SS	SS	kA
Leibstandarte	Leibstandart	k1gInSc5
Adolf	Adolf	k1gMnSc1
Hitler	Hitler	k1gMnSc1
bunkr	bunkr	k1gInSc4
obsadili	obsadit	k5eAaPmAgMnP
bez	bez	k7c2
boje	boj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
skončení	skončení	k1gNnSc6
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
bylo	být	k5eAaImAgNnS
uskutečněno	uskutečnit	k5eAaPmNgNnS
několik	několik	k4yIc1
pokusů	pokus	k1gInPc2
bunkr	bunkr	k1gInSc4
zničit	zničit	k5eAaPmF
<g/>
,	,	kIx,
avšak	avšak	k8xC
nikdy	nikdy	k6eAd1
nebyl	být	k5eNaImAgInS
zničen	zničit	k5eAaPmNgInS
zcela	zcela	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
pozdějších	pozdní	k2eAgInPc6d2
letech	let	k1gInPc6
občas	občas	k6eAd1
stavební	stavební	k2eAgMnPc1d1
dělníci	dělník	k1gMnPc1
narazili	narazit	k5eAaPmAgMnP
na	na	k7c4
část	část	k1gFnSc4
prostor	prostora	k1gFnPc2
<g/>
,	,	kIx,
ovšem	ovšem	k9
tyto	tento	k3xDgFnPc1
byly	být	k5eAaImAgFnP
vždy	vždy	k6eAd1
opět	opět	k6eAd1
zasypány	zasypán	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2006	#num#	k4
je	být	k5eAaImIp3nS
na	na	k7c6
místě	místo	k1gNnSc6
bývalého	bývalý	k2eAgInSc2d1
bunkru	bunkr	k1gInSc2
umístěna	umístit	k5eAaPmNgFnS
pamětní	pamětní	k2eAgFnSc1d1
deska	deska	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Poslední	poslední	k2eAgMnPc1d1
obyvatelé	obyvatel	k1gMnPc1
</s>
<s>
Poslední	poslední	k2eAgMnPc1d1
obyvatelé	obyvatel	k1gMnPc1
Vůdcova	vůdcův	k2eAgInSc2d1
bunkru	bunkr	k1gInSc2
dle	dle	k7c2
data	datum	k1gNnSc2
odchodu	odchod	k1gInSc2
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
</s>
<s>
Karl	Karl	k1gMnSc1
Gebhardt	Gebhardt	k1gMnSc1
•	•	k?
Julius	Julius	k1gMnSc1
Schaub	Schaub	k1gMnSc1
•	•	k?
Christa	Christa	k1gMnSc1
Schroeder	Schroeder	k1gMnSc1
•	•	k?
Johanna	Johann	k1gMnSc4
Wolf	Wolf	k1gMnSc1
</s>
<s>
23	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
</s>
<s>
Theodor	Theodor	k1gMnSc1
Morell	Morell	k1gMnSc1
•	•	k?
Albert	Albert	k1gMnSc1
Speer	Speer	k1gMnSc1
•	•	k?
Joachim	Joachim	k1gInSc1
von	von	k1gInSc1
Ribbentrop	Ribbentrop	k1gInSc1
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
</s>
<s>
Walter	Walter	k1gMnSc1
Frentz	Frentz	k1gMnSc1
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
</s>
<s>
Robert	Robert	k1gMnSc1
von	von	k1gInSc1
Greim	Greim	k1gInSc4
•	•	k?
Hanna	Hann	k1gInSc2
Reitsch	Reitscha	k1gFnPc2
</s>
<s>
29	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
</s>
<s>
Heinrich	Heinrich	k1gMnSc1
Müller	Müller	k1gMnSc1
•	•	k?
Bernd	Bernd	k1gMnSc1
Freytag	Freytag	k1gMnSc1
von	von	k1gInSc4
Loringhoven	Loringhoven	k2eAgInSc4d1
•	•	k?
Gerhardt	Gerhardt	k1gInSc1
Boldt	Boldt	k1gMnSc1
•	•	k?
Rudolf	Rudolf	k1gMnSc1
Weiss	Weiss	k1gMnSc1
</s>
<s>
30	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
</s>
<s>
Nicolaus	Nicolaus	k1gInSc1
von	von	k1gInSc1
Below	Below	k1gFnSc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
</s>
<s>
Erich	Erich	k1gMnSc1
Kempka	Kempek	k1gMnSc2
•	•	k?
Traudl	Traudl	k1gMnSc2
Junge	Jung	k1gMnSc2
•	•	k?
Gerda	Gerda	k1gMnSc1
Christian	Christian	k1gMnSc1
•	•	k?
Constanze	Constanze	k1gFnSc1
Manziarly	Manziarla	k1gFnSc2
•	•	k?
Else	Elsa	k1gFnSc3
Krüger	Krüger	k1gMnSc1
•	•	k?
Otto	Otto	k1gMnSc1
Günsche	Günsch	k1gFnSc2
•	•	k?
Johann	Johann	k1gMnSc1
Rattenhuber	Rattenhuber	k1gMnSc1
•	•	k?
Werner	Werner	k1gMnSc1
Naumann	Naumann	k1gMnSc1
•	•	k?
Wilhelm	Wilhelm	k1gMnSc1
Mohnke	Mohnk	k1gFnSc2
•	•	k?
Hans-Erich	Hans-Erich	k1gInSc1
Voss	Voss	k1gInSc1
•	•	k?
Ludwig	Ludwig	k1gMnSc1
Stumpfegger	Stumpfegger	k1gMnSc1
•	•	k?
Martin	Martin	k1gMnSc1
Bormann	Bormann	k1gMnSc1
•	•	k?
Artur	Artur	k1gMnSc1
Axmann	Axmann	k1gNnSc1
•	•	k?
Walther	Walthra	k1gFnPc2
Hewel	Hewela	k1gFnPc2
•	•	k?
Günther	Günthra	k1gFnPc2
Schwägermann	Schwägermann	k1gMnSc1
•	•	k?
Armin	Armin	k1gMnSc1
D.	D.	kA
Lehmann	Lehmann	k1gInSc1
•	•	k?
Ernst-Günther	Ernst-Günthra	k1gFnPc2
Schenck	Schenck	k1gMnSc1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
</s>
<s>
Rochus	Rochus	k1gMnSc1
Misch	Misch	k1gMnSc1
•	•	k?
Helmuth	Helmuth	k1gInSc1
Weidling	Weidling	k1gInSc1
•	•	k?
Hans	Hans	k1gMnSc1
Refior	Refior	k1gMnSc1
•	•	k?
Theodor	Theodor	k1gMnSc1
von	von	k1gInSc1
Dufving	Dufving	k1gInSc1
•	•	k?
Siegfried	Siegfried	k1gInSc1
Knappe	Knapp	k1gMnSc5
</s>
<s>
Nejisté	jistý	k2eNgNnSc1d1
datum	datum	k1gNnSc1
</s>
<s>
Wilhelm	Wilhelm	k1gMnSc1
Zander	Zander	k1gMnSc1
•	•	k?
Heinz	Heinz	k1gMnSc1
Lorenz	Lorenz	k1gMnSc1
•	•	k?
Heinz	Heinz	k1gMnSc1
Linge	Ling	k1gFnSc2
•	•	k?
Hans	Hans	k1gMnSc1
Baur	Baur	k1gMnSc1
</s>
<s>
Přítomni	přítomen	k2eAgMnPc1d1
i	i	k9
2	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
</s>
<s>
Erna	Erna	k1gMnSc1
Flegel	Flegel	k1gMnSc1
•	•	k?
Werner	Werner	k1gMnSc1
Haase	Haase	k1gFnSc2
•	•	k?
Johannes	Johannes	k1gMnSc1
Hentschel	Hentschel	k1gMnSc1
</s>
<s>
Sebevražda	sebevražda	k1gFnSc1
</s>
<s>
Ernst-Robert	Ernst-Robert	k1gMnSc1
Grawitz	Grawitz	k1gMnSc1
•	•	k?
Adolf	Adolf	k1gMnSc1
a	a	k8xC
Eva	Eva	k1gFnSc1
Hitlerovi	Hitler	k1gMnSc3
•	•	k?
Joseph	Joseph	k1gMnSc1
a	a	k8xC
Magda	Magda	k1gFnSc1
Goebbelsovi	Goebbels	k1gMnSc3
•	•	k?
Wilhelm	Wilhelm	k1gMnSc1
Burgdorf	Burgdorf	k1gMnSc1
•	•	k?
Hans	Hans	k1gMnSc1
Krebs	Krebsa	k1gFnPc2
•	•	k?
Peter	Peter	k1gMnSc1
Högl	Högl	k1gMnSc1
</s>
<s>
Zabití	zabití	k1gNnSc1
</s>
<s>
Hermann	Hermann	k1gMnSc1
Fegelein	Fegelein	k1gMnSc1
•	•	k?
Děti	dítě	k1gFnPc1
Josepha	Joseph	k1gMnSc2
Goebbelse	Goebbels	k1gMnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Architektura	architektura	k1gFnSc1
a	a	k8xC
stavebnictví	stavebnictví	k1gNnSc1
|	|	kIx~
Druhá	druhý	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
|	|	kIx~
Německo	Německo	k1gNnSc1
</s>
