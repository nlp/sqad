<s>
Panda	panda	k1gFnSc1
</s>
<s>
Panda	panda	k1gFnSc1
velká	velký	k2eAgFnSc1d1
</s>
<s>
Panda	panda	k1gFnSc1
je	být	k5eAaImIp3nS
souhrnný	souhrnný	k2eAgInSc1d1
český	český	k2eAgInSc1d1
název	název	k1gInSc1
pro	pro	k7c4
nepříbuzné	příbuzný	k2eNgInPc4d1
rody	rod	k1gInPc4
šelem	šelma	k1gFnPc2
Ailurus	Ailurus	k1gInSc1
a	a	k8xC
Ailuropoda	Ailuropoda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
každého	každý	k3xTgInSc2
z	z	k7c2
rodů	rod	k1gInPc2
patří	patřit	k5eAaImIp3nS
jediný	jediný	k2eAgInSc1d1
žijící	žijící	k2eAgInSc1d1
druh	druh	k1gInSc1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
ale	ale	k8xC
známy	znám	k2eAgFnPc1d1
i	i	k9
tři	tři	k4xCgInPc4
fosilní	fosilní	k2eAgInPc4d1
druhy	druh	k1gInPc4
rodu	rod	k1gInSc2
Ailuropoda	Ailuropoda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Panda	panda	k1gFnSc1
velká	velká	k1gFnSc1
žije	žít	k5eAaImIp3nS
v	v	k7c6
hornatých	hornatý	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
například	například	k6eAd1
Sečuán	Sečuán	k1gInSc1
a	a	k8xC
Tibet	Tibet	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
druhé	druhý	k4xOgFnSc2
poloviny	polovina	k1gFnSc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
národním	národní	k2eAgInSc7d1
symbolem	symbol	k1gInSc7
Číny	Čína	k1gFnSc2
a	a	k8xC
je	být	k5eAaImIp3nS
zobrazována	zobrazovat	k5eAaImNgFnS
na	na	k7c6
čínských	čínský	k2eAgFnPc6d1
zlatých	zlatý	k2eAgFnPc6d1
mincích	mince	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
její	její	k3xOp3gNnSc4
zabití	zabití	k1gNnSc4
je	být	k5eAaImIp3nS
v	v	k7c6
Číně	Čína	k1gFnSc6
trest	trest	k1gInSc4
smrti	smrt	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Druhy	druh	k1gInPc1
</s>
<s>
Ailurus	Ailurus	k1gMnSc1
</s>
<s>
Panda	panda	k1gFnSc1
červená	červenat	k5eAaImIp3nS
Ailurus	Ailurus	k1gInSc4
fulgens	fulgensa	k1gFnPc2
F.	F.	kA
G.	G.	kA
Cuvier	Cuvier	k1gMnSc1
<g/>
,	,	kIx,
1825	#num#	k4
</s>
<s>
Ailuropoda	Ailuropoda	k1gFnSc1
</s>
<s>
Panda	panda	k1gFnSc1
velká	velký	k2eAgFnSc1d1
Ailuropoda	Ailuropoda	k1gFnSc1
melanoleuca	melanoleuca	k1gMnSc1
David	David	k1gMnSc1
<g/>
,	,	kIx,
1869	#num#	k4
</s>
<s>
Ailuropoda	Ailuropoda	k1gFnSc1
microta	microt	k1gMnSc2
Pei	Pei	k1gMnSc2
<g/>
,	,	kIx,
1962	#num#	k4
(	(	kIx(
<g/>
pozdní	pozdní	k2eAgInSc1d1
pliocén	pliocén	k1gInSc1
<g/>
,	,	kIx,
†	†	k?
<g/>
)	)	kIx)
</s>
<s>
Ailuropoda	Ailuropoda	k1gMnSc1
wulingshanensis	wulingshanensis	k1gFnPc2
Wang	Wang	k1gMnSc1
<g/>
,	,	kIx,
1982	#num#	k4
(	(	kIx(
<g/>
pozdní	pozdní	k2eAgInSc1d1
pliocén	pliocén	k1gInSc1
až	až	k6eAd1
raný	raný	k2eAgInSc1d1
pleistocén	pleistocén	k1gInSc1
<g/>
,	,	kIx,
†	†	k?
<g/>
)	)	kIx)
</s>
<s>
Ailuropoda	Ailuropoda	k1gMnSc1
baconi	bacon	k1gMnPc1
Woodward	Woodward	k1gMnSc1
<g/>
,	,	kIx,
1915	#num#	k4
(	(	kIx(
<g/>
pleistocén	pleistocén	k1gInSc1
<g/>
,	,	kIx,
†	†	k?
<g/>
)	)	kIx)
</s>
<s>
Ailuropoda	Ailuropoda	k1gFnSc1
minor	minor	k2eAgFnPc2d1
Pei	Pei	k1gFnPc2
<g/>
,	,	kIx,
1962	#num#	k4
(	(	kIx(
<g/>
pleistocén	pleistocén	k1gInSc1
<g/>
,	,	kIx,
†	†	k?
<g/>
)	)	kIx)
</s>
<s>
Systematika	systematika	k1gFnSc1
</s>
<s>
Panda	panda	k1gFnSc1
velká	velká	k1gFnSc1
patří	patřit	k5eAaImIp3nS
do	do	k7c2
čeledi	čeleď	k1gFnSc2
medvědovitých	medvědovitý	k2eAgMnPc2d1
<g/>
,	,	kIx,
panda	panda	k1gFnSc1
červená	červená	k1gFnSc1
je	být	k5eAaImIp3nS
jediným	jediný	k2eAgMnSc7d1
žijícím	žijící	k2eAgMnSc7d1
zástupcem	zástupce	k1gMnSc7
větve	větev	k1gFnSc2
příbuzné	příbuzný	k2eAgFnSc2d1
skunkovitým	skunkovitý	k2eAgNnSc7d1
<g/>
,	,	kIx,
medvídkovitým	medvídkovitý	k2eAgNnSc7d1
a	a	k8xC
kunovitým	kunovitý	k2eAgNnSc7d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Zrzavý	zrzavý	k2eAgMnSc1d1
<g/>
,	,	kIx,
J.	J.	kA
<g/>
,	,	kIx,
Robovský	Robovský	k2eAgMnSc1d1
J.	J.	kA
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
:	:	kIx,
Kočkovité	kočkovitý	k2eAgFnSc2d1
šelmy	šelma	k1gFnSc2
<g/>
,	,	kIx,
Vesmír	vesmír	k1gInSc1
86	#num#	k4
<g/>
:	:	kIx,
566	#num#	k4
<g/>
-	-	kIx~
<g/>
567	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
panda	panda	k1gFnSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Živočichové	živočich	k1gMnPc1
</s>
