<s>
Národní	národní	k2eAgInSc1d1
park	park	k1gInSc1
Tucholské	Tucholský	k2eAgInPc1d1
bory	bor	k1gInPc1
</s>
<s>
Zdroje	zdroj	k1gInPc1
k	k	k7c3
infoboxuNárodní	infoboxuNárodní	k2eAgFnSc3d1
park	park	k1gInSc1
Tucholské	Tucholský	k2eAgInPc1d1
bory	bor	k1gInPc1
Základní	základní	k2eAgInPc1d1
informace	informace	k1gFnPc4
Vyhlášení	vyhlášení	k1gNnPc2
</s>
<s>
1996	#num#	k4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
46,13	46,13	k4
km²	km²	k?
Poloha	poloha	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Polsko	Polsko	k1gNnSc1
Polsko	Polsko	k1gNnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
53	#num#	k4
<g/>
°	°	k?
<g/>
26	#num#	k4
<g/>
′	′	k?
<g/>
8,88	8,88	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
17	#num#	k4
<g/>
°	°	k?
<g/>
18	#num#	k4
<g/>
′	′	k?
<g/>
10,8	10,8	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Geodata	Geodat	k1gMnSc2
(	(	kIx(
<g/>
OSM	osm	k4xCc1
<g/>
)	)	kIx)
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
</s>
<s>
Národní	národní	k2eAgInSc1d1
park	park	k1gInSc1
Tucholské	Tucholský	k2eAgInPc1d1
bory	bor	k1gInPc1
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Web	web	k1gInSc4
</s>
<s>
www.pnbt.com.pl	www.pnbt.com.pnout	k5eAaPmAgMnS
</s>
<s>
Obrázky	obrázek	k1gInPc4
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc4
či	či	k8xC
videa	video	k1gNnPc4
v	v	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
borovice	borovice	k1gFnSc1
v	v	k7c6
Národním	národní	k2eAgInSc6d1
parku	park	k1gInSc6
Tucholské	Tucholský	k2eAgInPc4d1
bory	bor	k1gInPc4
</s>
<s>
Národní	národní	k2eAgInSc1d1
park	park	k1gInSc1
Tucholské	Tucholský	k2eAgInPc1d1
bory	bor	k1gInPc7
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
polsky	polsky	k6eAd1
Park	park	k1gInSc1
Narodowy	Narodow	k2eAgInPc1d1
Bory	bor	k1gInPc1
Tucholskie	Tucholskie	k1gFnSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
národní	národní	k2eAgInSc1d1
park	park	k1gInSc1
v	v	k7c6
Polsku	Polsko	k1gNnSc6
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
založen	založit	k5eAaPmNgInS
v	v	k7c6
červenci	červenec	k1gInSc6
1996	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozkládá	rozkládat	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c6
ploše	plocha	k1gFnSc6
46,13	46,13	k4
km	km	kA
<g/>
2	#num#	k4
a	a	k8xC
tvoří	tvořit	k5eAaImIp3nP
jej	on	k3xPp3gInSc4
lesy	les	k1gInPc4
<g/>
,	,	kIx,
louky	louka	k1gFnPc4
<g/>
,	,	kIx,
jezera	jezero	k1gNnSc2
a	a	k8xC
rašelinišť	rašeliniště	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Park	park	k1gInSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
severní	severní	k2eAgFnSc6d1
části	část	k1gFnSc6
Polska	Polsko	k1gNnSc2
<g/>
,	,	kIx,
ve	v	k7c6
vojvodství	vojvodství	k1gNnSc6
Pomorskie	Pomorskie	k1gFnSc2
<g/>
,	,	kIx,
poblíž	poblíž	k7c2
obcí	obec	k1gFnPc2
Chojnice	Chojnice	k1gFnSc2
<g/>
,	,	kIx,
Brusy	brus	k1gInPc1
<g/>
,	,	kIx,
Charzykowy	Charzykow	k2eAgInPc1d1
v	v	k7c6
srdci	srdce	k1gNnSc6
Tucholských	Tucholský	k2eAgInPc2d1
borů	bor	k1gInPc2
<g/>
,	,	kIx,
největším	veliký	k2eAgNnSc6d3
zalesněném	zalesněný	k2eAgNnSc6d1
území	území	k1gNnSc6
v	v	k7c6
Polsku	Polsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Správa	správa	k1gFnSc1
parku	park	k1gInSc2
je	být	k5eAaImIp3nS
v	v	k7c6
obci	obec	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
3	#num#	k4
km	km	kA
od	od	k7c2
Chojnic	Chojnice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
sousedství	sousedství	k1gNnSc6
větší	veliký	k2eAgFnSc2d2
chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
zvané	zvaný	k2eAgFnSc2d1
Zaborski	Zaborsk	k1gFnSc2
Park	park	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
parku	park	k1gInSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
na	na	k7c4
21	#num#	k4
jezer	jezero	k1gNnPc2
<g/>
,	,	kIx,
největším	veliký	k2eAgNnSc7d3
a	a	k8xC
nejhlubším	hluboký	k2eAgNnSc7d3
jezerem	jezero	k1gNnSc7
je	být	k5eAaImIp3nS
jezero	jezero	k1gNnSc1
Ostrowite	Ostrowit	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osm	osm	k4xCc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
je	být	k5eAaImIp3nS
propojeno	propojen	k2eAgNnSc1d1
a	a	k8xC
vytváří	vytvářit	k5eAaPmIp3nS,k5eAaImIp3nS
řetězec	řetězec	k1gInSc1
nazývá	nazývat	k5eAaImIp3nS
„	„	k?
<g/>
Struga	Struga	k1gFnSc1
Siedmiu	Siedmium	k1gNnSc6
Jezior	Jezior	k1gMnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgInSc1
návrh	návrh	k1gInSc1
národního	národní	k2eAgInSc2d1
parku	park	k1gInSc2
předpokládal	předpokládat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
národní	národní	k2eAgInSc1d1
park	park	k1gInSc1
bude	být	k5eAaImBp3nS
zahrnovat	zahrnovat	k5eAaImF
130	#num#	k4
čtverečních	čtvereční	k2eAgInPc2d1
kilometrů	kilometr	k1gInPc2
<g/>
,	,	kIx,
ale	ale	k8xC
po	po	k7c6
četných	četný	k2eAgNnPc6d1
jednáních	jednání	k1gNnPc6
s	s	k7c7
místními	místní	k2eAgInPc7d1
úřady	úřad	k1gInPc7
bylo	být	k5eAaImAgNnS
rozhodnuto	rozhodnut	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
ohraničoval	ohraničovat	k5eAaImAgMnS
pouze	pouze	k6eAd1
oblast	oblast	k1gFnSc4
zvanou	zvaný	k2eAgFnSc4d1
Struga	Strug	k1gMnSc2
Siedmiu	Siedmium	k1gNnSc3
Jezior	Jezior	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pozemky	pozemek	k1gInPc1
začleněny	začleněn	k2eAgInPc1d1
do	do	k7c2
národního	národní	k2eAgInSc2d1
parku	park	k1gInSc2
byly	být	k5eAaImAgInP
státním	státní	k2eAgInSc7d1
majetkem	majetek	k1gInSc7
,	,	kIx,
nikoliv	nikoliv	k9
soukromým	soukromý	k2eAgNnSc7d1
vlastnictvím	vlastnictví	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lesy	les	k1gInPc7
<g/>
,	,	kIx,
louky	louka	k1gFnPc4
a	a	k8xC
rašeliniště	rašeliniště	k1gNnSc2
byly	být	k5eAaImAgInP
dříve	dříve	k6eAd2
součástí	součást	k1gFnSc7
lesní	lesní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
Rytel	Rytela	k1gFnPc2
a	a	k8xC
jezery	jezero	k1gNnPc7
byly	být	k5eAaImAgInP
spravovány	spravován	k2eAgInPc1d1
státní	státní	k2eAgFnSc7d1
správou	správa	k1gFnSc7
zemědělského	zemědělský	k2eAgInSc2d1
půdního	půdní	k2eAgInSc2d1
fondu	fond	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
logo	logo	k1gNnSc4
</s>
<s>
Geologie	geologie	k1gFnSc1
</s>
<s>
Oblast	oblast	k1gFnSc1
v	v	k7c6
okolí	okolí	k1gNnSc6
Tucholského	Tucholský	k2eAgInSc2d1
boru	bor	k1gInSc2
byla	být	k5eAaImAgFnS
formována	formovat	k5eAaImNgFnS
skandinávským	skandinávský	k2eAgInSc7d1
ledovcem	ledovec	k1gInSc7
<g/>
,	,	kIx,
takže	takže	k8xS
se	se	k3xPyFc4
rozkládá	rozkládat	k5eAaImIp3nS
v	v	k7c6
písčitých	písčitý	k2eAgFnPc6d1
rovinách	rovina	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
roviny	rovina	k1gFnPc1
jsou	být	k5eAaImIp3nP
rozděleny	rozdělit	k5eAaPmNgFnP
na	na	k7c4
četné	četný	k2eAgFnPc4d1
duny	duna	k1gFnPc4
a	a	k8xC
jezera	jezero	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jezera	jezero	k1gNnPc1
jsou	být	k5eAaImIp3nP
dlouhé	dlouhý	k2eAgFnPc1d1
a	a	k8xC
úzké	úzký	k2eAgFnPc1d1
<g/>
,	,	kIx,
tvoří	tvořit	k5eAaImIp3nP
dlouhé	dlouhý	k2eAgInPc1d1
kanály	kanál	k1gInPc1
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
nejdelší	dlouhý	k2eAgFnSc1d3
je	být	k5eAaImIp3nS
17	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Půda	půda	k1gFnSc1
v	v	k7c6
parku	park	k1gInSc6
je	být	k5eAaImIp3nS
nekvalitní	kvalitní	k2eNgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
rámci	rámec	k1gInSc6
parku	park	k1gInSc2
je	být	k5eAaImIp3nS
největší	veliký	k2eAgNnSc1d3
uskupení	uskupení	k1gNnSc1
vnitrozemských	vnitrozemský	k2eAgFnPc2d1
dun	duna	k1gFnPc2
v	v	k7c6
Tucholských	Tucholský	k2eAgInPc6d1
borech	bor	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Charakteristickým	charakteristický	k2eAgInSc7d1
rysem	rys	k1gInSc7
povrchu	povrch	k1gInSc2
po	po	k7c6
činnosti	činnost	k1gFnSc6
ledovce	ledovec	k1gInSc2
jsou	být	k5eAaImIp3nP
podlouhlá	podlouhlý	k2eAgNnPc1d1
údolí	údolí	k1gNnPc1
soustředěná	soustředěný	k2eAgNnPc1d1
jedním	jeden	k4xCgInSc7
směrem	směr	k1gInSc7
<g/>
,	,	kIx,
rýhy	rýha	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příkladem	příklad	k1gInSc7
je	být	k5eAaImIp3nS
Rynna	Rynen	k2eAgFnSc1d1
Jeziora	Jeziora	k1gFnSc1
Charzykowskiego	Charzykowskiego	k6eAd1
<g/>
,	,	kIx,
kousek	kousek	k1gInSc4
od	od	k7c2
západní	západní	k2eAgFnSc2d1
hranice	hranice	k1gFnSc2
parku	park	k1gInSc2
<g/>
.	.	kIx.
"	"	kIx"
<g/>
Wytopiska	Wytopiska	k1gFnSc1
<g/>
"	"	kIx"
,	,	kIx,
ledovcová	ledovcový	k2eAgNnPc1d1
jezera	jezero	k1gNnPc1
byla	být	k5eAaImAgNnP
vytvořena	vytvořit	k5eAaPmNgNnP
po	po	k7c6
roztání	roztání	k1gNnSc6
ledovce	ledovec	k1gInSc2
<g/>
.	.	kIx.
<g/>
Mají	mít	k5eAaImIp3nP
tvar	tvar	k1gInSc4
kotle	kotel	k1gInSc2
<g/>
,	,	kIx,
cesty	cesta	k1gFnSc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
nepravidelné	pravidelný	k2eNgFnPc1d1
kapky	kapka	k1gFnPc1
v	v	k7c6
oblasti	oblast	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
parku	park	k1gInSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
více	hodně	k6eAd2
než	než	k8xS
20	#num#	k4
jezer	jezero	k1gNnPc2
v	v	k7c6
parku	park	k1gInSc6
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
některé	některý	k3yIgFnPc1
mají	mít	k5eAaImIp3nP
křišťálově	křišťálově	k6eAd1
čistou	čistá	k1gFnSc4
vody	voda	k1gFnSc2
(	(	kIx(
<g/>
např.	např.	kA
Gacno	Gacno	k1gNnSc4
Wielkie	Wielkie	k1gFnSc2
a	a	k8xC
Gacno	Gacno	k6eAd1
Male	male	k6eAd1
<g/>
,	,	kIx,
Nierybno	Nierybna	k1gFnSc5
<g/>
,	,	kIx,
Gluche	Gluch	k1gInPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Můžeme	moct	k5eAaImIp1nP
tady	tady	k6eAd1
najít	najít	k5eAaPmF
mnoho	mnoho	k4c4
druhů	druh	k1gInPc2
(	(	kIx(
<g/>
asi	asi	k9
25	#num#	k4
<g/>
)	)	kIx)
ryb	ryba	k1gFnPc2
a	a	k8xC
také	také	k9
populaci	populace	k1gFnSc4
evropského	evropský	k2eAgMnSc4d1
bobra	bobr	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
Flora	Flora	k1gFnSc1
</s>
<s>
Parku	park	k1gInSc2
dominují	dominovat	k5eAaImIp3nP
bory	bor	k1gInPc1
<g/>
,	,	kIx,
včetně	včetně	k7c2
suchých	suchý	k2eAgInPc2d1
borů	bor	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
se	se	k3xPyFc4
zde	zde	k6eAd1
vyskytují	vyskytovat	k5eAaImIp3nP
vedle	vedle	k7c2
nich	on	k3xPp3gNnPc2
mokřadních	mokřadní	k2eAgNnPc2d1
stanovišť	stanoviště	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Najdeme	najít	k5eAaPmIp1nP
zde	zde	k6eAd1
640	#num#	k4
druhů	druh	k1gInPc2
rostlin	rostlina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roste	růst	k5eAaImIp3nS
zde	zde	k6eAd1
mnoho	mnoho	k4c1
vzácných	vzácný	k2eAgInPc2d1
druhů	druh	k1gInPc2
specifických	specifický	k2eAgInPc2d1
pro	pro	k7c4
chudé	chudý	k1gMnPc4
(	(	kIx(
<g/>
na	na	k7c4
výživu	výživa	k1gFnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
oligotrofní	oligotrofní	k2eAgInPc1d1
biotopy	biotop	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
jsou	být	k5eAaImIp3nP
ohroženy	ohrožen	k2eAgFnPc4d1
především	především	k6eAd1
eutrofizací	eutrofizace	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rostliny	rostlina	k1gFnSc2
zde	zde	k6eAd1
jsou	být	k5eAaImIp3nP
pozůstatky	pozůstatek	k1gInPc1
ledovcové	ledovcový	k2eAgFnSc2d1
(	(	kIx(
<g/>
glaciální	glaciální	k2eAgFnSc2d1
<g/>
)	)	kIx)
flóry	flóra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roste	růst	k5eAaImIp3nS
zde	zde	k6eAd1
arkticko-alpská	arkticko-alpský	k2eAgFnSc1d1
<g/>
,	,	kIx,
boreálně-alpská	boreálně-alpský	k2eAgFnSc1d1
a	a	k8xC
borealní	borealný	k2eAgMnPc1d1
květena	květena	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Patří	patřit	k5eAaImIp3nS
k	k	k7c3
nim	on	k3xPp3gMnPc3
mimo	mimo	k6eAd1
jiné	jiné	k1gNnSc4
Violka	violka	k1gFnSc1
olysalá	olysalý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Viola	Viola	k1gFnSc1
epipsila	epipsit	k5eAaPmAgFnS,k5eAaImAgFnS
<g/>
)	)	kIx)
<g/>
,	,	kIx,
stulík	stulík	k1gInSc1
malý	malý	k2eAgInSc1d1
(	(	kIx(
<g/>
Nuphar	Nuphar	k1gInSc1
pumila	pumil	k1gMnSc2
(	(	kIx(
<g/>
Timm	Timm	k1gMnSc1
<g/>
)	)	kIx)
DC	DC	kA
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Stellaria	Stellarium	k1gNnPc1
crassifolia	crassifolia	k1gFnSc1
Ehrh	Ehrh	k1gMnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
lomikámen	lomikámen	k1gInSc4
bažinný	bažinný	k2eAgInSc4d1
(	(	kIx(
<g/>
Saxifraga	Saxifraga	k1gFnSc1
hirculus	hirculus	k1gMnSc1
(	(	kIx(
<g/>
L.	L.	kA
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
Scop	Scop	k1gInSc1
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
,	,	kIx,
třtina	třtina	k1gFnSc1
přehlížená	přehlížený	k2eAgFnSc1d1
(	(	kIx(
<g/>
Calamagrostis	Calamagrostis	k1gFnSc1
stricta	stricta	k1gMnSc1
(	(	kIx(
<g/>
L.	L.	kA
<g/>
)	)	kIx)
Roth	Roth	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ostřice	ostřice	k1gFnSc1
šlahounovitá	šlahounovitý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Carex	Carex	k1gInSc1
chordorrhiza	chordorrhiz	k1gMnSc2
L.	L.	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zimozel	zimozet	k5eAaImAgInS,k5eAaPmAgInS
severní	severní	k2eAgMnSc1d1
(	(	kIx(
<g/>
Linnaea	Linnae	k2eAgFnSc1d1
borealis	borealis	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
měkčilka	měkčilka	k1gFnSc1
jednolistá	jednolistý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Malaxis	Malaxis	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
monophyllos	monophyllos	k1gInSc1
(	(	kIx(
<g/>
L.	L.	kA
<g/>
)	)	kIx)
Sw	Sw	k1gMnSc1
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
,	,	kIx,
leknín	leknín	k1gInSc1
bělostný	bělostný	k2eAgInSc1d1
(	(	kIx(
<g/>
Nymphaea	Nymphae	k2eAgFnSc1d1
candida	candida	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
rojovník	rojovník	k1gInSc1
bahenní	bahenní	k2eAgInSc1d1
(	(	kIx(
<g/>
Ledum	Ledum	k1gInSc1
palustre	palustr	k1gInSc5
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vlochyně	vlochyně	k1gFnSc1
(	(	kIx(
<g/>
Vaccinium	Vaccinium	k1gNnSc1
uliginosum	uliginosum	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
šícha	šícha	k1gFnSc1
černá	černá	k1gFnSc1
(	(	kIx(
<g/>
Empetrum	Empetrum	k1gNnSc1
nigrum	nigrum	k?
<g/>
)	)	kIx)
a	a	k8xC
hadilka	hadilka	k1gFnSc1
obecná	obecná	k1gFnSc1
(	(	kIx(
<g/>
Ophioglossum	Ophioglossum	k1gInSc1
vulgatum	vulgatum	k1gNnSc4
L.	L.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
čisté	čistý	k2eAgFnSc6d1
vodě	voda	k1gFnSc6
jezer	jezero	k1gNnPc2
se	se	k3xPyFc4
vyskytují	vyskytovat	k5eAaImIp3nP
lobelka	lobelka	k1gFnSc1
vodní	vodní	k2eAgFnSc1d1
(	(	kIx(
<g/>
Lobelia	Lobelia	k1gFnSc1
dortmanna	dortmanna	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
žabníček	žabníček	k1gMnSc1
vzplývavý	vzplývavý	k2eAgMnSc1d1
(	(	kIx(
<g/>
Luronium	Luronium	k1gNnSc1
natans	natansa	k1gFnPc2
L.	L.	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
šídlatka	šídlatka	k1gFnSc1
jelení	jelení	k2eAgFnSc1d1
(	(	kIx(
<g/>
Isoëtes	Isoëtes	k1gMnSc1
lacustris	lacustris	k1gFnSc2
L.	L.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vzácně	vzácně	k6eAd1
zde	zde	k6eAd1
roste	růst	k5eAaImIp3nS
javor	javor	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Můžete	moct	k5eAaImIp2nP
často	často	k6eAd1
najít	najít	k5eAaPmF
duby	dub	k1gInPc4
<g/>
,	,	kIx,
buky	buk	k1gInPc4
<g/>
,	,	kIx,
habry	habr	k1gInPc4
<g/>
,	,	kIx,
ale	ale	k8xC
borovic	borovice	k1gFnPc2
převládají	převládat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s>
Fauna	fauna	k1gFnSc1
</s>
<s>
Žije	žít	k5eAaImIp3nS
zde	zde	k6eAd1
</s>
<s>
43	#num#	k4
druhů	druh	k1gInPc2
savců	savec	k1gMnPc2
<g/>
,	,	kIx,
7	#num#	k4
druhů	druh	k1gInPc2
netopýrů	netopýr	k1gMnPc2
<g/>
,	,	kIx,
</s>
<s>
144	#num#	k4
druhů	druh	k1gInPc2
ptáků	pták	k1gMnPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
108	#num#	k4
chovaných	chovaný	k2eAgInPc2d1
druhů	druh	k1gInPc2
a	a	k8xC
35	#num#	k4
tažných	tažný	k2eAgMnPc2d1
ptáků	pták	k1gMnPc2
<g/>
,	,	kIx,
</s>
<s>
6	#num#	k4
druhů	druh	k1gInPc2
plazů	plaz	k1gInPc2
(	(	kIx(
<g/>
všechny	všechen	k3xTgInPc1
typické	typický	k2eAgInPc1d1
pro	pro	k7c4
Polské	polský	k2eAgFnPc4d1
nížiny	nížina	k1gFnPc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
</s>
<s>
13	#num#	k4
druhů	druh	k1gInPc2
obojživelníků	obojživelník	k1gMnPc2
<g/>
,	,	kIx,
</s>
<s>
25	#num#	k4
druhů	druh	k1gInPc2
ryb	ryba	k1gFnPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
pstruha	pstruh	k1gMnSc2
duhového	duhový	k2eAgMnSc2d1
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Národní	národní	k2eAgInSc1d1
park	park	k1gInSc1
je	být	k5eAaImIp3nS
rájem	ráj	k1gInSc7
pro	pro	k7c4
ptáky	pták	k1gMnPc4
-	-	kIx~
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
144	#num#	k4
druhů	druh	k1gInPc2
ptáků	pták	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeřáb	jeřáb	k1gMnSc1
<g/>
,	,	kIx,
čáp	čáp	k1gMnSc1
černý	černý	k1gMnSc1
(	(	kIx(
<g/>
Ciconia	Ciconium	k1gNnSc2
nigra	nigr	k1gMnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
volavka	volavka	k1gFnSc1
popelavá	popelavý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Ardea	Arde	k2eAgFnSc1d1
cinerea	cinerea	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kormoráni	kormorán	k1gMnPc1
<g/>
,	,	kIx,
labuť	labuť	k1gFnSc4
(	(	kIx(
<g/>
Cygnus	Cygnus	k1gInSc4
<g/>
)	)	kIx)
a	a	k8xC
sokol	sokol	k1gMnSc1
stěhovavý	stěhovavý	k2eAgMnSc1d1
(	(	kIx(
<g/>
Falco	Falco	k1gMnSc1
peregrinus	peregrinus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Symbolem	symbol	k1gInSc7
parku	park	k1gInSc2
je	být	k5eAaImIp3nS
tetřev	tetřev	k1gMnSc1
hlušec-	hlušec-	k?
donedávna	donedávna	k6eAd1
byl	být	k5eAaImAgMnS
běžný	běžný	k2eAgMnSc1d1
v	v	k7c6
této	tento	k3xDgFnSc6
oblasti	oblast	k1gFnSc6
<g/>
,	,	kIx,
především	především	k6eAd1
v	v	k7c6
lesích	les	k1gInPc6
kolem	kolem	k6eAd1
Klosnowo	Klosnowo	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nyní	nyní	k6eAd1
orgány	orgán	k1gInPc7
národním	národní	k2eAgInPc3d1
parku	park	k1gInSc2
<g/>
,	,	kIx,
vytváří	vytvářet	k5eAaImIp3nS,k5eAaPmIp3nS
plán	plán	k1gInSc4
na	na	k7c4
obnovení	obnovení	k1gNnSc4
původních	původní	k2eAgInPc2d1
stavů	stav	k1gInPc2
tohoto	tento	k3xDgMnSc2
ptáka	pták	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
savců	savec	k1gMnPc2
zde	zde	k6eAd1
najdeme	najít	k5eAaPmIp1nP
například	například	k6eAd1
vlka	vlk	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obzvláště	obzvláště	k6eAd1
důležitou	důležitý	k2eAgFnSc7d1
součástí	součást	k1gFnSc7
fauny	fauna	k1gFnSc2
jsou	být	k5eAaImIp3nP
netopýři	netopýr	k1gMnPc1
-	-	kIx~
několika	několik	k4yIc3
druhům	druh	k1gMnPc3
z	z	k7c2
nich	on	k3xPp3gFnPc2
se	se	k3xPyFc4
v	v	k7c6
parku	park	k1gInSc6
daří	dařit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
zimozel	zimozet	k5eAaImAgMnS,k5eAaPmAgMnS
severní	severní	k2eAgMnSc1d1
(	(	kIx(
<g/>
Linnaea	Linnae	k2eAgFnSc1d1
borealis	borealis	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Turistika	turistika	k1gFnSc1
</s>
<s>
Nejdůležitější	důležitý	k2eAgNnPc1d3
turistická	turistický	k2eAgNnPc1d1
střediska	středisko	k1gNnPc1
Tucholského	Tucholský	k2eAgInSc2d1
boru	bor	k1gInSc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
u	u	k7c2
jezer	jezero	k1gNnPc2
Charzykowskie	Charzykowskie	k1gFnSc2
a	a	k8xC
Karsinskie	Karsinskie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
posledních	poslední	k2eAgNnPc6d1
letech	léto	k1gNnPc6
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
populární	populární	k2eAgFnSc1d1
i	i	k8xC
agro-turistika	agro-turistika	k1gFnSc1
-	-	kIx~
např.	např.	kA
v	v	k7c6
obci	obec	k1gFnSc6
Swornegacie	Swornegacie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
řece	řeka	k1gFnSc6
Brda	brdo	k1gNnSc2
je	být	k5eAaImIp3nS
kajakářská	kajakářský	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Jezero	jezero	k1gNnSc1
Charzykowy	Charzykowa	k1gFnSc2
je	být	k5eAaImIp3nS
známé	známý	k2eAgNnSc1d1
jako	jako	k9
místo	místo	k7c2
zrodu	zrod	k1gInSc2
polského	polský	k2eAgInSc2d1
jachtingu	jachting	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jezero	jezero	k1gNnSc1
Charzykowy	Charzykowa	k1gFnSc2
nabízí	nabízet	k5eAaImIp3nS
dobrý	dobrý	k2eAgMnSc1d1
jachtařské	jachtařský	k2eAgNnSc4d1
podmínky	podmínka	k1gFnPc4
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
jak	jak	k6eAd1
v	v	k7c6
létě	léto	k1gNnSc6
a	a	k8xC
v	v	k7c6
zimě	zima	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existuje	existovat	k5eAaImIp3nS
několik	několik	k4yIc4
cyklostezek	cyklostezka	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
umožňují	umožňovat	k5eAaImIp3nP
seznámit	seznámit	k5eAaPmF
se	se	k3xPyFc4
na	na	k7c6
kole	kolo	k1gNnSc6
s	s	k7c7
turistickými	turistický	k2eAgFnPc7d1
atrakcemi	atrakce	k1gFnPc7
Tucholského	Tucholský	k2eAgInSc2d1
boru	bor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
toho	ten	k3xDgNnSc2
je	být	k5eAaImIp3nS
Tucholský	Tucholský	k2eAgInSc1d1
bor	bor	k1gInSc1
je	být	k5eAaImIp3nS
propleten	propleten	k2eAgInSc1d1
mnoha	mnoho	k4c2
turistickými	turistický	k2eAgFnPc7d1
trasami	trasa	k1gFnPc7
-	-	kIx~
mezi	mezi	k7c7
nimi	on	k3xPp3gMnPc7
je	být	k5eAaImIp3nS
i	i	k9
Kaszubski	Kaszubske	k1gFnSc4
Trasa	trasa	k1gFnSc1
z	z	k7c2
Chojnice	Chojnice	k1gFnSc2
do	do	k7c2
Wiela	Wielo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byly	být	k5eAaImAgInP
použity	použit	k2eAgInPc1d1
překlady	překlad	k1gInPc1
textů	text	k1gInPc2
z	z	k7c2
článků	článek	k1gInPc2
Bory	bor	k1gInPc4
Tucholskie	Tucholskie	k1gFnSc2
National	National	k1gMnPc2
Park	park	k1gInSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
a	a	k8xC
Park	park	k1gInSc1
Narodowy	Narodow	k2eAgInPc1d1
Bory	bor	k1gInPc1
Tucholskie	Tucholskie	k1gFnSc2
na	na	k7c6
polské	polský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Národní	národní	k2eAgInSc1d1
park	park	k1gInSc1
Tucholské	Tucholský	k2eAgInPc1d1
bory	bor	k1gInPc1
<g/>
.	.	kIx.
www.polsko.travel	www.polsko.travel	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Bory	bor	k1gInPc1
Tucholskie	Tucholskie	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Borow	Borow	k1gMnSc1
Tucholskich	Tucholskich	k1gMnSc1
National	National	k1gMnSc1
Park	park	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
[	[	kIx(
<g/>
web	web	k1gInSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Last	Last	k2eAgInSc1d1
updated	updated	k1gInSc1
<g/>
:	:	kIx,
April	April	k1gInSc1
20	#num#	k4
<g/>
,	,	kIx,
2008	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglosasky	anglosasky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Begni	Begn	k1gMnPc1
<g/>
,	,	kIx,
Gerard	Gerard	k1gMnSc1
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Observing	Observing	k1gInSc1
Our	Our	k1gFnSc2
Environment	Environment	k1gMnSc1
From	From	k1gMnSc1
Space	Space	k1gMnSc1
<g/>
.	.	kIx.
<g/>
"	"	kIx"
Lisse	Lisse	k1gFnSc1
<g/>
:	:	kIx,
Swets	Swets	k1gInSc1
and	and	k?
Zeitlinger	Zeitlinger	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
Environmental	Environmental	k1gMnSc1
Protection	Protection	k1gInSc4
Improves	Improves	k1gInSc1
<g/>
,	,	kIx,
Says	Says	k1gInSc1
Zelichowski	Zelichowsk	k1gFnSc2
<g/>
.	.	kIx.
<g/>
"	"	kIx"
Poland	Poland	k1gInSc1
News	News	k1gInSc1
Bulletin	bulletin	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
January	Januara	k1gFnSc2
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Národní	národní	k2eAgInSc4d1
park	park	k1gInSc4
Tucholské	Tucholský	k2eAgInPc4d1
bory	bor	k1gInPc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
www	www	k?
stránky	stránka	k1gFnPc4
Parku	park	k1gInSc2
Narodowego	Narodowego	k6eAd1
Bory	bor	k1gInPc1
Tucholskie	Tucholskie	k1gFnSc2
(	(	kIx(
<g/>
polsky	polsky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
stránky	stránka	k1gFnPc4
tucholského	tucholský	k2eAgInSc2d1
parku	park	k1gInSc2
//	//	k?
<g/>
www.tuchpark.tuchola.pl/	www.tuchpark.tuchola.pl/	k?
(	(	kIx(
<g/>
polsky	polsky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
http://nature.poland.pl/regions/pojezierza_pd/check/article,Bory_Tucholskie_National_Park,id,140584.htm%5B%5D	http://nature.poland.pl/regions/pojezierza_pd/check/article,Bory_Tucholskie_National_Park,id,140584.htm%5B%5D	k4
</s>
<s>
citace	citace	k1gFnPc1
z	z	k7c2
"	"	kIx"
<g/>
Gazeta	gazeta	k1gFnSc1
Krakowska	Krakowska	k1gFnSc1
<g/>
"	"	kIx"
28.10	28.10	k4
<g/>
.92	.92	k4
<g/>
na	na	k7c4
http://www.zb.eco.pl	http://www.zb.eco.pnout	k5eAaPmAgInS
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Polsko	Polsko	k1gNnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Národní	národní	k2eAgInPc1d1
parky	park	k1gInPc1
v	v	k7c6
Polsku	Polsko	k1gNnSc6
</s>
<s>
Babia	Babia	k1gFnSc1
Góra	Gór	k1gInSc2
•	•	k?
Bělověžský	Bělověžský	k2eAgInSc1d1
•	•	k?
Biebrzanský	Biebrzanský	k2eAgInSc1d1
•	•	k?
Bieszczadský	Bieszczadský	k2eAgInSc1d1
•	•	k?
Drawenský	Drawenský	k2eAgInSc1d1
•	•	k?
Gorczaňský	Gorczaňský	k2eAgInSc1d1
•	•	k?
Kampinoský	Kampinoský	k2eAgInSc1d1
•	•	k?
Krkonošský	krkonošský	k2eAgInSc1d1
•	•	k?
Magurský	Magurský	k2eAgInSc1d1
•	•	k?
Narwiaňský	Narwiaňský	k2eAgInSc1d1
•	•	k?
Ojcowský	Ojcowský	k2eAgInSc1d1
•	•	k?
Pieninský	Pieninský	k2eAgInSc1d1
•	•	k?
Poleský	Poleský	k2eAgInSc1d1
•	•	k?
Roztoczanský	Roztoczanský	k2eAgInSc1d1
•	•	k?
Sloviňský	Sloviňský	k2eAgInSc1d1
•	•	k?
Stolové	stolový	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Svatokřížský	Svatokřížský	k2eAgInSc1d1
•	•	k?
Tatranský	tatranský	k2eAgInSc1d1
•	•	k?
Tucholské	Tucholský	k2eAgInPc1d1
bory	bor	k1gInPc1
•	•	k?
Ústí	ústí	k1gNnSc1
Warty	Warta	k1gFnSc2
•	•	k?
Velkopolský	velkopolský	k2eAgInSc1d1
•	•	k?
Wigerský	Wigerský	k2eAgInSc1d1
•	•	k?
Wolinský	Wolinský	k2eAgInSc1d1
</s>
