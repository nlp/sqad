<s>
Národní	národní	k2eAgInSc1d1
park	park	k1gInSc1
Tucholské	Tucholský	k2eAgInPc1d1
bory	bor	k1gInPc7
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
polsky	polsky	k6eAd1
Park	park	k1gInSc1
Narodowy	Narodow	k2eAgInPc1d1
Bory	bor	k1gInPc1
Tucholskie	Tucholskie	k1gFnSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
národní	národní	k2eAgInSc1d1
park	park	k1gInSc1
v	v	k7c6
Polsku	Polsko	k1gNnSc6
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
založen	založit	k5eAaPmNgInS
v	v	k7c6
červenci	červenec	k1gInSc6
1996	#num#	k4
<g/>
.	.	kIx.
</s>