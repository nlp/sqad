<p>
<s>
Sultán	sultán	k1gMnSc1	sultán
je	být	k5eAaImIp3nS	být
panovnický	panovnický	k2eAgInSc4d1	panovnický
titul	titul	k1gInSc4	titul
používaný	používaný	k2eAgInSc4d1	používaný
v	v	k7c6	v
muslimských	muslimský	k2eAgFnPc6d1	muslimská
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
arabštině	arabština	k1gFnSc6	arabština
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
původně	původně	k6eAd1	původně
abstraktní	abstraktní	k2eAgNnSc4d1	abstraktní
podstatné	podstatný	k2eAgNnSc4d1	podstatné
jméno	jméno	k1gNnSc4	jméno
s	s	k7c7	s
významem	význam	k1gInSc7	význam
vládnutí	vládnutí	k1gNnSc2	vládnutí
<g/>
,	,	kIx,	,
panovnická	panovnický	k2eAgFnSc1d1	panovnická
moc	moc	k1gFnSc1	moc
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
obecným	obecný	k2eAgNnSc7d1	obecné
označením	označení	k1gNnSc7	označení
pro	pro	k7c4	pro
vládní	vládní	k2eAgMnPc4d1	vládní
hodnostáře	hodnostář	k1gMnPc4	hodnostář
<g/>
,	,	kIx,	,
ministry	ministr	k1gMnPc4	ministr
či	či	k8xC	či
panovníky	panovník	k1gMnPc4	panovník
<g/>
,	,	kIx,	,
až	až	k9	až
od	od	k7c2	od
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
panovnickým	panovnický	k2eAgInSc7d1	panovnický
titulem	titul	k1gInSc7	titul
<g/>
,	,	kIx,	,
ekvivalentem	ekvivalent	k1gInSc7	ekvivalent
evropského	evropský	k2eAgMnSc4d1	evropský
krále	král	k1gMnSc4	král
<g/>
.	.	kIx.	.
</s>
<s>
Seldžukové	Seldžuk	k1gMnPc1	Seldžuk
v	v	k7c6	v
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
význam	význam	k1gInSc1	význam
tohoto	tento	k3xDgInSc2	tento
titulu	titul	k1gInSc2	titul
opět	opět	k6eAd1	opět
posunuli	posunout	k5eAaPmAgMnP	posunout
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
měl	mít	k5eAaImAgMnS	mít
označovat	označovat	k5eAaImF	označovat
svrchovaného	svrchovaný	k2eAgMnSc4d1	svrchovaný
panovníka	panovník	k1gMnSc4	panovník
celého	celý	k2eAgInSc2d1	celý
islámského	islámský	k2eAgInSc2d1	islámský
světa	svět	k1gInSc2	svět
–	–	k?	–
tedy	tedy	k9	tedy
pozici	pozice	k1gFnSc4	pozice
<g/>
,	,	kIx,	,
o	o	k7c4	o
kterou	který	k3yIgFnSc4	který
Seldžukové	Seldžukový	k2eAgNnSc1d1	Seldžukový
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
osmanští	osmanský	k2eAgMnPc1d1	osmanský
Turci	Turek	k1gMnPc1	Turek
trvale	trvale	k6eAd1	trvale
usilovali	usilovat	k5eAaImAgMnP	usilovat
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
ekvivalentem	ekvivalent	k1gInSc7	ekvivalent
perského	perský	k2eAgInSc2d1	perský
titulu	titul	k1gInSc2	titul
pádišáh	pádišáh	k1gMnSc1	pádišáh
(	(	kIx(	(
<g/>
kníže	kníže	k1gMnSc1	kníže
věřících	věřící	k1gMnPc2	věřící
<g/>
)	)	kIx)	)
a	a	k8xC	a
evropského	evropský	k2eAgMnSc4d1	evropský
císaře	císař	k1gMnSc4	císař
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sultán	sultán	k1gMnSc1	sultán
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
zaměňuje	zaměňovat	k5eAaImIp3nS	zaměňovat
s	s	k7c7	s
titulem	titul	k1gInSc7	titul
chalífa	chalífa	k1gMnSc1	chalífa
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
však	však	k9	však
není	být	k5eNaImIp3nS	být
přesné	přesný	k2eAgNnSc1d1	přesné
<g/>
.	.	kIx.	.
</s>
<s>
Chalífa	chalífa	k1gMnSc1	chalífa
byl	být	k5eAaImAgMnS	být
později	pozdě	k6eAd2	pozdě
pouze	pouze	k6eAd1	pouze
titul	titul	k1gInSc4	titul
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
náboženské	náboženský	k2eAgFnSc2d1	náboženská
autority	autorita	k1gFnSc2	autorita
v	v	k7c6	v
islámu	islám	k1gInSc6	islám
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
ne	ne	k9	ne
světský	světský	k2eAgMnSc1d1	světský
panovník	panovník	k1gMnSc1	panovník
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
proroka	prorok	k1gMnSc2	prorok
Muhammada	Muhammada	k1gFnSc1	Muhammada
se	s	k7c7	s
vládce	vládce	k1gMnPc4	vládce
všech	všecek	k3xTgFnPc2	všecek
věřících	věřící	k1gFnPc2	věřící
označoval	označovat	k5eAaImAgMnS	označovat
chalífa	chalífa	k1gMnSc1	chalífa
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
následovník	následovník	k1gMnSc1	následovník
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
právě	právě	k6eAd1	právě
proroka	prorok	k1gMnSc4	prorok
Muhammada	Muhammada	k1gFnSc1	Muhammada
<g/>
.	.	kIx.	.
</s>
<s>
Chalífa	chalífa	k1gMnSc1	chalífa
byl	být	k5eAaImAgMnS	být
náboženský	náboženský	k2eAgMnSc1d1	náboženský
i	i	k8xC	i
politický	politický	k2eAgMnSc1d1	politický
hodnostář	hodnostář	k1gMnSc1	hodnostář
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Se	s	k7c7	s
zánikem	zánik	k1gInSc7	zánik
chalífátu	chalífát	k1gInSc2	chalífát
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rozdělení	rozdělení	k1gNnSc3	rozdělení
funkcí	funkce	k1gFnPc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Osmanské	osmanský	k2eAgFnSc6d1	Osmanská
říši	říš	k1gFnSc6	říš
si	se	k3xPyFc3	se
však	však	k9	však
od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
sultán	sultán	k1gMnSc1	sultán
nárokoval	nárokovat	k5eAaImAgMnS	nárokovat
i	i	k9	i
titul	titul	k1gInSc4	titul
chalífa	chalífa	k1gMnSc1	chalífa
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
oba	dva	k4xCgInPc1	dva
pojmy	pojem	k1gInPc1	pojem
fakticky	fakticky	k6eAd1	fakticky
splynuly	splynout	k5eAaPmAgInP	splynout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
sultán	sultána	k1gFnPc2	sultána
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
