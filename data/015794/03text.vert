<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Moháče	Moháč	k1gInSc2
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Moháče	Moháč	k1gInSc2
</s>
<s>
konflikt	konflikt	k1gInSc1
<g/>
:	:	kIx,
Osmansko-uherské	osmansko-uherský	k2eAgFnSc2d1
války	válka	k1gFnSc2
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Moháče	Moháč	k1gInSc2
<g/>
,	,	kIx,
osmanská	osmanský	k2eAgFnSc1d1
miniatura	miniatura	k1gFnSc1
</s>
<s>
trvání	trvání	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
29	#num#	k4
<g/>
.	.	kIx.
srpen	srpen	k1gInSc1
1526	#num#	k4
</s>
<s>
místo	místo	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Moháč	Moháč	k1gInSc1
<g/>
,	,	kIx,
jižně	jižně	k6eAd1
od	od	k7c2
Budapešti	Budapešť	k1gFnSc2
</s>
<s>
výsledek	výsledek	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
drtivé	drtivý	k2eAgNnSc1d1
vítězství	vítězství	k1gNnSc1
Osmanů	Osman	k1gMnPc2
</s>
<s>
strany	strana	k1gFnPc1
</s>
<s>
Osmanská	osmanský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
</s>
<s>
Krymský	krymský	k2eAgInSc1d1
chanát	chanát	k1gInSc1
Krymský	krymský	k2eAgInSc4d1
chanát	chanát	k1gInSc4
</s>
<s>
Uherské	uherský	k2eAgNnSc1d1
království	království	k1gNnSc1
Chorvatské	chorvatský	k2eAgNnSc1d1
království	království	k1gNnSc1
Svatá	svatat	k5eAaImIp3nS
říše	říše	k1gFnSc1
římská	římský	k2eAgFnSc1d1
</s>
<s>
Země	země	k1gFnSc1
Koruny	koruna	k1gFnSc2
české	český	k2eAgFnSc2d1
Země	zem	k1gFnSc2
Koruny	koruna	k1gFnSc2
české	český	k2eAgFnSc2d1
</s>
<s>
Bavorské	bavorský	k2eAgNnSc1d1
vévodství	vévodství	k1gNnSc1
</s>
<s>
Papežský	papežský	k2eAgInSc1d1
stát	stát	k1gInSc1
</s>
<s>
Polské	polský	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
velitelé	velitel	k1gMnPc1
</s>
<s>
Süleyman	Süleyman	k1gMnSc1
I.	I.	kA
Pargali	Pargali	k1gMnSc1
Ibrahim	Ibrahim	k1gMnSc1
Paša	paša	k1gMnSc1
Behram	Behram	k1gInSc4
Paša	paša	k1gMnSc1
Malkoçoğ	Malkoçoğ	k1gInSc2
Balı	Balı	k1gMnSc1
Bey	Bey	k1gMnSc1
Gazi	Gaz	k1gFnSc2
Husrev-beg	Husrev-beg	k1gInSc1
Devlet	Devlet	k1gInSc1
I.	I.	kA
Girej	Girej	k1gInSc1
</s>
<s>
Ludvík	Ludvík	k1gMnSc1
Jagellonský	jagellonský	k2eAgMnSc1d1
†	†	k?
Pál	Pál	k1gMnSc1
Tomori	Tomor	k1gFnSc2
†	†	k?
Jiří	Jiří	k1gMnSc1
Zápolský	Zápolský	k2eAgMnSc1d1
†	†	k?
Štěpán	Štěpán	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Báthory	Báthora	k1gFnPc5
</s>
<s>
síla	síla	k1gFnSc1
</s>
<s>
~	~	kIx~
cca	cca	kA
55	#num#	k4
000	#num#	k4
mužů	muž	k1gMnPc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
~	~	kIx~
minimálně	minimálně	k6eAd1
160	#num#	k4
děl	dělo	k1gNnPc2
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
~	~	kIx~
cca	cca	kA
25	#num#	k4
000	#num#	k4
mužů	muž	k1gMnPc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
~	~	kIx~
80	#num#	k4
děl	dělo	k1gNnPc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
ztráty	ztráta	k1gFnPc1
</s>
<s>
~	~	kIx~
15	#num#	k4
000	#num#	k4
mužů	muž	k1gMnPc2
</s>
<s>
odhad	odhad	k1gInSc1
historiků	historik	k1gMnPc2
<g/>
:	:	kIx,
<g/>
~	~	kIx~
cca	cca	kA
14	#num#	k4
<g/>
–	–	k?
<g/>
15	#num#	k4
000	#num#	k4
mužů	muž	k1gMnPc2
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
–	–	k?
28	#num#	k4
uherských	uherský	k2eAgMnPc2d1
magnátů	magnát	k1gMnPc2
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
–	–	k?
500	#num#	k4
urozených	urozený	k2eAgMnPc2d1
mužů	muž	k1gMnPc2
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
–	–	k?
2500	#num#	k4
zajatců	zajatec	k1gMnPc2
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
odhad	odhad	k1gInSc1
Osmanů	Osman	k1gMnPc2
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
~	~	kIx~
20	#num#	k4
000	#num#	k4
pěchota	pěchota	k1gFnSc1
<g/>
~	~	kIx~
4000	#num#	k4
těžkooděnců	těžkooděnec	k1gMnPc2
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Moháče	Moháč	k1gInSc2
(	(	kIx(
<g/>
maďarsky	maďarsky	k6eAd1
Mohácsi-csata	Mohácsi-csat	k2eAgFnSc1d1
<g/>
,	,	kIx,
německy	německy	k6eAd1
Schlacht	Schlacht	k1gInSc1
bei	bei	k?
Mohács	Mohács	k1gInSc1
<g/>
,	,	kIx,
turecky	turecky	k6eAd1
Mohaç	Mohaç	k1gFnPc4
Muharebesi	Muharebese	k1gFnSc4
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
jednou	jeden	k4xCgFnSc7
z	z	k7c2
nejvýznamnějších	významný	k2eAgFnPc2d3
bitev	bitva	k1gFnPc2
v	v	k7c6
dějinách	dějiny	k1gFnPc6
střední	střední	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
některých	některý	k3yIgNnPc6
pojetích	pojetí	k1gNnPc6
považovaná	považovaný	k2eAgFnSc1d1
za	za	k7c4
mezník	mezník	k1gInSc4
symbolizující	symbolizující	k2eAgInSc4d1
zde	zde	k6eAd1
konec	konec	k1gInSc4
středověku	středověk	k1gInSc2
a	a	k8xC
počátek	počátek	k1gInSc4
novověku	novověk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odehrála	odehrát	k5eAaPmAgFnS
29	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1526	#num#	k4
na	na	k7c6
planině	planina	k1gFnSc6
vzdálené	vzdálený	k2eAgFnSc6d1
přibližně	přibližně	k6eAd1
6	#num#	k4
km	km	kA
jižně	jižně	k6eAd1
od	od	k7c2
dolnouherského	dolnouherský	k2eAgNnSc2d1
města	město	k1gNnSc2
Moháč	Moháč	k1gInSc1
<g/>
,	,	kIx,
nedaleko	nedaleko	k7c2
dnešní	dnešní	k2eAgFnSc2d1
srbsko-chorvatsko-maďarské	srbsko-chorvatsko-maďarský	k2eAgFnSc2d1
hranice	hranice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Početně	početně	k6eAd1
silnější	silný	k2eAgNnSc4d2
vojsko	vojsko	k1gNnSc4
osmanského	osmanský	k2eAgMnSc2d1
sultána	sultán	k1gMnSc2
Süleymana	Süleyman	k1gMnSc2
I.	I.	kA
zde	zde	k6eAd1
během	během	k7c2
necelých	celý	k2eNgFnPc2d1
dvou	dva	k4xCgFnPc2
hodin	hodina	k1gFnPc2
drtivě	drtivě	k6eAd1
porazilo	porazit	k5eAaPmAgNnS
oddíly	oddíl	k1gInPc4
shromážděné	shromážděný	k2eAgInPc4d1
pod	pod	k7c7
korouhví	korouhev	k1gFnSc7
dvacetiletého	dvacetiletý	k2eAgInSc2d1
českého	český	k2eAgInSc2d1
<g/>
,	,	kIx,
uherského	uherský	k2eAgMnSc2d1
a	a	k8xC
chorvatského	chorvatský	k2eAgMnSc2d1
krále	král	k1gMnSc2
Ludvíka	Ludvík	k1gMnSc2
Jagellonského	jagellonský	k2eAgMnSc2d1
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
při	při	k7c6
útěku	útěk	k1gInSc6
z	z	k7c2
bojiště	bojiště	k1gNnSc2
utonul	utonout	k5eAaPmAgMnS
v	v	k7c6
říčce	říčka	k1gFnSc6
Csele	Csele	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
velmi	velmi	k6eAd1
riskantnímu	riskantní	k2eAgInSc3d1
plánu	plán	k1gInSc3
vrchního	vrchní	k2eAgMnSc2d1
velitele	velitel	k1gMnSc2
uherských	uherský	k2eAgFnPc2d1
sil	síla	k1gFnPc2
Pála	Pála	k1gMnSc1
Tomoryho	Tomory	k1gMnSc2
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
se	se	k3xPyFc4
ve	v	k7c6
velké	velký	k2eAgFnSc6d1
míře	míra	k1gFnSc6
spoléhal	spoléhat	k5eAaImAgInS
na	na	k7c4
válečné	válečný	k2eAgNnSc4d1
štěstí	štěstí	k1gNnSc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
zahynulo	zahynout	k5eAaPmAgNnS
na	na	k7c6
straně	strana	k1gFnSc6
křesťanů	křesťan	k1gMnPc2
zhruba	zhruba	k6eAd1
dalších	další	k2eAgInPc6d1
15	#num#	k4
000	#num#	k4
vojáků	voják	k1gMnPc2
a	a	k8xC
výsledek	výsledek	k1gInSc1
střetnutí	střetnutí	k1gNnSc2
předznamenal	předznamenat	k5eAaPmAgInS
tureckou	turecký	k2eAgFnSc4d1
expanzi	expanze	k1gFnSc4
do	do	k7c2
Uher	Uhry	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Smrtí	smrt	k1gFnPc2
Jagellonského	jagellonský	k2eAgMnSc2d1
panovníka	panovník	k1gMnSc2
se	se	k3xPyFc4
uvolnily	uvolnit	k5eAaPmAgInP
trůny	trůn	k1gInPc1
českého	český	k2eAgNnSc2d1
a	a	k8xC
uherského	uherský	k2eAgNnSc2d1
království	království	k1gNnSc2
<g/>
,	,	kIx,
čehož	což	k3yRnSc2,k3yQnSc2
ke	k	k7c3
svému	svůj	k3xOyFgInSc3
prospěchu	prospěch	k1gInSc3
využili	využít	k5eAaPmAgMnP
rakouští	rakouský	k2eAgMnPc1d1
Habsburkové	Habsburk	k1gMnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Předehra	předehra	k1gFnSc1
</s>
<s>
Hans	Hans	k1gMnSc1
Krell	Krell	k1gMnSc1
<g/>
:	:	kIx,
král	král	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
Jagellonský	jagellonský	k2eAgMnSc1d1
</s>
<s>
Tizian	Tizian	k1gMnSc1
<g/>
:	:	kIx,
sultán	sultán	k1gMnSc1
Süleyman	Süleyman	k1gMnSc1
I.	I.	kA
</s>
<s>
Počátky	počátek	k1gInPc4
expanze	expanze	k1gFnSc2
Osmanů	Osman	k1gMnPc2
směrem	směr	k1gInSc7
přes	přes	k7c4
Černé	Černé	k2eAgNnSc4d1
moře	moře	k1gNnSc4
na	na	k7c4
Balkán	Balkán	k1gInSc4
se	se	k3xPyFc4
datují	datovat	k5eAaImIp3nP
k	k	k7c3
polovině	polovina	k1gFnSc3
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
první	první	k4xOgFnSc7
pocítili	pocítit	k5eAaPmAgMnP
důsledky	důsledek	k1gInPc4
této	tento	k3xDgFnSc2
rozpínavosti	rozpínavost	k1gFnSc2
Bulhaři	Bulhar	k1gMnPc1
a	a	k8xC
po	po	k7c6
bitvě	bitva	k1gFnSc6
na	na	k7c6
Kosově	Kosův	k2eAgNnSc6d1
poli	pole	k1gNnSc6
také	také	k9
Srbové	Srb	k1gMnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
dalších	další	k2eAgNnPc6d1
desetiletích	desetiletí	k1gNnPc6
se	se	k3xPyFc4
terčem	terč	k1gInSc7
osmanských	osmanský	k2eAgInPc2d1
nájezdů	nájezd	k1gInPc2
stalo	stát	k5eAaPmAgNnS
pohraničí	pohraničí	k1gNnSc1
jižních	jižní	k2eAgFnPc2d1
Uher	Uhry	k1gFnPc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnPc1
panovníci	panovník	k1gMnPc1
zde	zde	k6eAd1
proti	proti	k7c3
stávajícímu	stávající	k2eAgNnSc3d1
ohrožení	ohrožení	k1gNnSc3
postupem	postupem	k7c2
času	čas	k1gInSc2
vybudovali	vybudovat	k5eAaPmAgMnP
nárazníkové	nárazníkový	k2eAgNnSc4d1
pásmo	pásmo	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
se	se	k3xPyFc4
skládalo	skládat	k5eAaImAgNnS
ze	z	k7c2
dvou	dva	k4xCgFnPc2
souběžných	souběžný	k2eAgFnPc2d1
pevnostních	pevnostní	k2eAgFnPc2d1
linií	linie	k1gFnPc2
stojících	stojící	k2eAgFnPc2d1
v	v	k7c6
prostoru	prostor	k1gInSc6
od	od	k7c2
Sedmihradska	Sedmihradsko	k1gNnSc2
po	po	k7c4
Jaderské	jaderský	k2eAgNnSc4d1
moře	moře	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInSc1
pás	pás	k1gInSc1
fortifikací	fortifikace	k1gFnPc2
reprezentovaly	reprezentovat	k5eAaImAgInP
Srijem	Srij	k1gInSc7
<g/>
,	,	kIx,
Bělehrad	Bělehrad	k1gInSc1
<g/>
,	,	kIx,
Srebrenický	srebrenický	k2eAgInSc1d1
banát	banát	k1gInSc1
<g/>
,	,	kIx,
Šabac	Šabac	k1gInSc1
<g/>
,	,	kIx,
Banja	banjo	k1gNnPc1
Luka	luka	k1gNnPc1
<g/>
,	,	kIx,
Jajce	Jajec	k1gInPc1
a	a	k8xC
Knin	Knin	k1gInSc1
<g/>
,	,	kIx,
druhá	druhý	k4xOgFnSc1
linie	linie	k1gFnSc1
se	se	k3xPyFc4
opírala	opírat	k5eAaImAgFnS
o	o	k7c6
pevnosti	pevnost	k1gFnSc6
Temešvár	Temešvár	k1gInSc1
<g/>
,	,	kIx,
Dubica	Dubica	k1gFnSc1
<g/>
,	,	kIx,
Krupa	krupa	k1gFnSc1
<g/>
,	,	kIx,
Bihač	Bihač	k1gInSc1
a	a	k8xC
Senj	Senj	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
]	]	kIx)
Systém	systém	k1gInSc1
se	se	k3xPyFc4
osvědčil	osvědčit	k5eAaPmAgInS
jako	jako	k9
velmi	velmi	k6eAd1
odolná	odolný	k2eAgFnSc1d1
ochrana	ochrana	k1gFnSc1
před	před	k7c7
vpády	vpád	k1gInPc7
tureckých	turecký	k2eAgInPc2d1
oddílů	oddíl	k1gInPc2
z	z	k7c2
příhraničních	příhraniční	k2eAgInPc2d1
sandžaků	sandžak	k1gInPc2
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
výdaje	výdaj	k1gInPc1
na	na	k7c4
udržování	udržování	k1gNnSc4
bojeschopnosti	bojeschopnost	k1gFnSc2
této	tento	k3xDgFnSc2
protiturecké	protiturecký	k2eAgFnSc2d1
bariéry	bariéra	k1gFnSc2
v	v	k7c6
průměru	průměr	k1gInSc6
dosahovaly	dosahovat	k5eAaImAgFnP
částky	částka	k1gFnPc1
až	až	k6eAd1
140	#num#	k4
000	#num#	k4
zlatých	zlatá	k1gFnPc2
ročně	ročně	k6eAd1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
byla	být	k5eAaImAgFnS
zhruba	zhruba	k6eAd1
polovina	polovina	k1gFnSc1
ročního	roční	k2eAgInSc2d1
příjmu	příjem	k1gInSc2
Uherské	uherský	k2eAgFnSc2d1
koruny	koruna	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Vysoké	vysoký	k2eAgInPc1d1
náklady	náklad	k1gInPc1
v	v	k7c6
kombinaci	kombinace	k1gFnSc6
s	s	k7c7
agresivní	agresivní	k2eAgFnSc7d1
zahraniční	zahraniční	k2eAgFnSc7d1
politikou	politika	k1gFnSc7
Matyáše	Matyáš	k1gMnSc2
Korvína	Korvín	k1gMnSc2
nakonec	nakonec	k6eAd1
přivedly	přivést	k5eAaPmAgFnP
zemi	zem	k1gFnSc4
k	k	k7c3
vyčerpání	vyčerpání	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
roce	rok	k1gInSc6
1490	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
na	na	k7c4
uherský	uherský	k2eAgInSc4d1
trůn	trůn	k1gInSc4
dosedl	dosednout	k5eAaPmAgMnS
český	český	k2eAgMnSc1d1
panovník	panovník	k1gMnSc1
Vladislav	Vladislav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jagellonský	jagellonský	k2eAgInSc1d1
<g/>
,	,	kIx,
se	se	k3xPyFc4
proto	proto	k8xC
panovnický	panovnický	k2eAgInSc1d1
dvůr	dvůr	k1gInSc1
v	v	k7c6
Budíně	Budín	k1gInSc6
rozhodl	rozhodnout	k5eAaPmAgInS
finanční	finanční	k2eAgFnSc4d1
tíseň	tíseň	k1gFnSc4
vyřešit	vyřešit	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
náklady	náklad	k1gInPc1
na	na	k7c4
udržování	udržování	k1gNnSc4
balkánských	balkánský	k2eAgFnPc2d1
pevností	pevnost	k1gFnPc2
razantně	razantně	k6eAd1
sníží	snížit	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
první	první	k4xOgFnSc4
bylo	být	k5eAaImAgNnS
rozpuštěno	rozpuštěn	k2eAgNnSc4d1
stálé	stálý	k2eAgNnSc4d1
polní	polní	k2eAgNnSc4d1
vojsko	vojsko	k1gNnSc4
<g/>
,	,	kIx,
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
zeštíhlení	zeštíhlení	k1gNnSc3
početního	početní	k2eAgInSc2d1
stavu	stav	k1gInSc2
posádek	posádka	k1gFnPc2
a	a	k8xC
stále	stále	k6eAd1
méně	málo	k6eAd2
peněz	peníze	k1gInPc2
bylo	být	k5eAaImAgNnS
vynakládáno	vynakládat	k5eAaImNgNnS
na	na	k7c4
opravu	oprava	k1gFnSc4
hradeb	hradba	k1gFnPc2
a	a	k8xC
udržování	udržování	k1gNnSc1
dělostřeleckého	dělostřelecký	k2eAgNnSc2d1
vybavení	vybavení	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
I	i	k9
přes	přes	k7c4
tuto	tento	k3xDgFnSc4
nepříznivou	příznivý	k2eNgFnSc4d1
situaci	situace	k1gFnSc4
si	se	k3xPyFc3
král	král	k1gMnSc1
a	a	k8xC
jeho	jeho	k3xOp3gMnSc1
rada	rada	k1gMnSc1
uvědomovali	uvědomovat	k5eAaImAgMnP
hrozbu	hrozba	k1gFnSc4
z	z	k7c2
turecké	turecký	k2eAgFnSc2d1
strany	strana	k1gFnSc2
a	a	k8xC
pokoušeli	pokoušet	k5eAaImAgMnP
se	se	k3xPyFc4
zajistit	zajistit	k5eAaPmF
alespoň	alespoň	k9
nejnutnější	nutný	k2eAgInPc4d3
výdaje	výdaj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
účelově	účelově	k6eAd1
prodlužovali	prodlužovat	k5eAaImAgMnP
mírovou	mírový	k2eAgFnSc4d1
smlouvu	smlouva	k1gFnSc4
s	s	k7c7
osmanskými	osmanský	k2eAgMnPc7d1
sultány	sultán	k1gMnPc7
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInPc1
zájmy	zájem	k1gInPc1
byly	být	k5eAaImAgInP
počátkem	počátkem	k7c2
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
obráceny	obrácen	k2eAgFnPc1d1
k	k	k7c3
výbojům	výboj	k1gInPc3
v	v	k7c6
Asii	Asie	k1gFnSc6
a	a	k8xC
severní	severní	k2eAgFnSc6d1
Africe	Afrika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dohoda	dohoda	k1gFnSc1
byla	být	k5eAaImAgFnS
o	o	k7c4
tři	tři	k4xCgInPc4
roky	rok	k1gInPc4
prodloužena	prodloužen	k2eAgFnSc1d1
i	i	k8xC
v	v	k7c6
březnu	březen	k1gInSc6
1519	#num#	k4
na	na	k7c6
počátku	počátek	k1gInSc6
panování	panování	k1gNnSc2
Ludvíka	Ludvík	k1gMnSc2
Jagellonského	jagellonský	k2eAgMnSc2d1
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
zůstávalo	zůstávat	k5eAaImAgNnS
jen	jen	k9
otázkou	otázka	k1gFnSc7
času	čas	k1gInSc2
<g/>
,	,	kIx,
než	než	k8xS
muslimský	muslimský	k2eAgMnSc1d1
soused	soused	k1gMnSc1
změní	změnit	k5eAaPmIp3nS
priority	priorita	k1gFnPc4
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
zahraniční	zahraniční	k2eAgFnSc6d1
politice	politika	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
K	k	k7c3
tomuto	tento	k3xDgInSc3
obratu	obrat	k1gInSc3
došlo	dojít	k5eAaPmAgNnS
za	za	k7c2
panování	panování	k1gNnSc2
sultána	sultán	k1gMnSc2
Süleymana	Süleyman	k1gMnSc2
I.	I.	kA
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
se	se	k3xPyFc4
po	po	k7c6
vzoru	vzor	k1gInSc6
svých	svůj	k3xOyFgMnPc2
předků	předek	k1gMnPc2
považoval	považovat	k5eAaImAgMnS
za	za	k7c2
dědice	dědic	k1gMnSc2
odkazu	odkaz	k1gInSc2
Alexandra	Alexandr	k1gMnSc2
Velikého	veliký	k2eAgMnSc2d1
a	a	k8xC
cítil	cítit	k5eAaImAgMnS
se	se	k3xPyFc4
zavázán	zavázat	k5eAaPmNgMnS
k	k	k7c3
dobytí	dobytí	k1gNnSc3
Uher	Uhry	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc4,k3yIgInPc4,k3yQgInPc4
pokládal	pokládat	k5eAaImAgMnS
za	za	k7c4
součást	součást	k1gFnSc4
starověké	starověký	k2eAgFnSc2d1
Makedonie	Makedonie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
Svou	svůj	k3xOyFgFnSc4
útočnou	útočný	k2eAgFnSc4d1
mezinárodní	mezinárodní	k2eAgFnSc4d1
strategii	strategie	k1gFnSc4
začal	začít	k5eAaPmAgInS
naplňovat	naplňovat	k5eAaImF
již	již	k6eAd1
v	v	k7c6
prvním	první	k4xOgInSc6
roce	rok	k1gInSc6
panování	panování	k1gNnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
nařídil	nařídit	k5eAaPmAgMnS
oblehnout	oblehnout	k5eAaPmF
chorvatskou	chorvatský	k2eAgFnSc4d1
pevnost	pevnost	k1gFnSc4
Jajcu	Jajc	k2eAgFnSc4d1
a	a	k8xC
zároveň	zároveň	k6eAd1
vypravil	vypravit	k5eAaPmAgMnS
do	do	k7c2
Budína	Budín	k1gMnSc2
posla	posel	k1gMnSc2
Behrama	Behram	k1gMnSc2
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
přivážel	přivážet	k5eAaImAgMnS
nový	nový	k2eAgInSc4d1
návrh	návrh	k1gInSc4
prodloužení	prodloužení	k1gNnSc2
mírové	mírový	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoli	ačkoli	k8xS
se	se	k3xPyFc4
uherským	uherský	k2eAgMnSc7d1
ozbrojencům	ozbrojenec	k1gMnPc3
podařilo	podařit	k5eAaPmAgNnS
fortifikaci	fortifikace	k1gFnSc3
ubránit	ubránit	k5eAaPmF
<g/>
,	,	kIx,
mocnáři	mocnář	k1gMnPc1
v	v	k7c6
Budíně	Budín	k1gInSc6
vyhodnotili	vyhodnotit	k5eAaPmAgMnP
sultánův	sultánův	k2eAgInSc4d1
postup	postup	k1gInSc4
jako	jako	k8xC,k8xS
nepřijatelný	přijatelný	k2eNgInSc4d1
<g/>
,	,	kIx,
uvrhli	uvrhnout	k5eAaPmAgMnP
Behrama	Behramum	k1gNnPc4
do	do	k7c2
vězení	vězení	k1gNnSc2
a	a	k8xC
začali	začít	k5eAaPmAgMnP
hledat	hledat	k5eAaImF
pomoc	pomoc	k1gFnSc4
v	v	k7c6
cizině	cizina	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
Toto	tento	k3xDgNnSc1
jednání	jednání	k1gNnSc1
ovšem	ovšem	k9
osmanskému	osmanský	k2eAgMnSc3d1
panovníkovi	panovník	k1gMnSc3
umožnilo	umožnit	k5eAaPmAgNnS
casus	casus	k1gMnSc1
belli	belle	k1gFnSc4
prezentovat	prezentovat	k5eAaBmF
jako	jako	k8xS,k8xC
pomstu	pomsta	k1gFnSc4
za	za	k7c2
urážky	urážka	k1gFnSc2
a	a	k8xC
křivdy	křivda	k1gFnSc2
<g/>
,	,	kIx,
kterých	který	k3yQgFnPc2,k3yIgFnPc2,k3yRgFnPc2
se	se	k3xPyFc4
proti	proti	k7c3
němu	on	k3xPp3gMnSc3
dopustila	dopustit	k5eAaPmAgFnS
uherská	uherský	k2eAgFnSc1d1
královská	královský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
pyšně	pyšně	k6eAd1
odmítla	odmítnout	k5eAaPmAgFnS
návrh	návrh	k1gInSc4
nové	nový	k2eAgFnSc2d1
mírové	mírový	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
a	a	k8xC
vsadila	vsadit	k5eAaPmAgFnS
do	do	k7c2
vězení	vězení	k1gNnSc2
jeho	jeho	k3xOp3gMnSc2,k3xPp3gMnSc2
posla	posel	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
cílenou	cílený	k2eAgFnSc4d1
propagandu	propaganda	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
určena	určit	k5eAaPmNgFnS
nejen	nejen	k6eAd1
pro	pro	k7c4
muslimskou	muslimský	k2eAgFnSc4d1
veřejnost	veřejnost	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k8xC
evropským	evropský	k2eAgMnPc3d1
protivníkům	protivník	k1gMnPc3
a	a	k8xC
pouze	pouze	k6eAd1
maskovala	maskovat	k5eAaBmAgFnS
Süleymanovy	Süleymanův	k2eAgFnPc4d1
dobyvatelské	dobyvatelský	k2eAgFnPc4d1
tendence	tendence	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
K	k	k7c3
dalším	další	k2eAgFnPc3d1
útočným	útočný	k2eAgFnPc3d1
akcím	akce	k1gFnPc3
za	za	k7c2
severní	severní	k2eAgFnSc2d1
hranice	hranice	k1gFnSc2
dal	dát	k5eAaPmAgMnS
osmanský	osmanský	k2eAgMnSc1d1
sultán	sultán	k1gMnSc1
povel	povel	k1gInSc1
na	na	k7c6
jaře	jaro	k1gNnSc6
1521	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Využil	využít	k5eAaPmAgMnS
tristního	tristní	k2eAgInSc2d1
stavu	stav	k1gInSc2
balkánského	balkánský	k2eAgInSc2d1
obranného	obranný	k2eAgInSc2d1
systému	systém	k1gInSc2
i	i	k8xC
chaotických	chaotický	k2eAgInPc2d1
poměrů	poměr	k1gInPc2
v	v	k7c6
Jagellonské	jagellonský	k2eAgFnSc6d1
monarchii	monarchie	k1gFnSc6
a	a	k8xC
v	v	k7c6
rámci	rámec	k1gInSc6
bojových	bojový	k2eAgFnPc2d1
operací	operace	k1gFnPc2
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
podařilo	podařit	k5eAaPmAgNnS
dobýt	dobýt	k5eAaPmF
pevnosti	pevnost	k1gFnSc3
Šabac	Šabac	k1gFnSc4
a	a	k8xC
Bělehrad	Bělehrad	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Posléze	posléze	k6eAd1
nechal	nechat	k5eAaPmAgMnS
většinu	většina	k1gFnSc4
svých	svůj	k3xOyFgMnPc2
vojáků	voják	k1gMnPc2
odtáhnout	odtáhnout	k5eAaPmF
zpět	zpět	k6eAd1
na	na	k7c4
domovské	domovský	k2eAgNnSc4d1
území	území	k1gNnSc4
a	a	k8xC
všeobecně	všeobecně	k6eAd1
očekávalo	očekávat	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
příštího	příští	k2eAgInSc2d1
roku	rok	k1gInSc2
bude	být	k5eAaImBp3nS
ofenzíva	ofenzíva	k1gFnSc1
opět	opět	k6eAd1
obnovena	obnoven	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
druhému	druhý	k4xOgInSc3
tažení	tažení	k1gNnSc1
do	do	k7c2
Uher	Uhry	k1gFnPc2
však	však	k9
došlo	dojít	k5eAaPmAgNnS
až	až	k9
o	o	k7c4
pět	pět	k4xCc4
let	léto	k1gNnPc2
později	pozdě	k6eAd2
<g/>
,	,	kIx,
neboť	neboť	k8xC
Süleymanovu	Süleymanův	k2eAgFnSc4d1
pozornost	pozornost	k1gFnSc4
si	se	k3xPyFc3
nejprve	nejprve	k6eAd1
vyžádalo	vyžádat	k5eAaPmAgNnS
povstání	povstání	k1gNnSc1
v	v	k7c6
Sýrii	Sýrie	k1gFnSc6
(	(	kIx(
<g/>
1521	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
posléze	posléze	k6eAd1
půlroční	půlroční	k2eAgNnSc1d1
obléhání	obléhání	k1gNnSc1
Rhodu	Rhodos	k1gInSc2
(	(	kIx(
<g/>
1522	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
nakonec	nakonec	k6eAd1
rebelie	rebelie	k1gFnSc1
v	v	k7c6
Egyptě	Egypt	k1gInSc6
(	(	kIx(
<g/>
1524	#num#	k4
<g/>
–	–	k?
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
1525	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
Přípravy	příprava	k1gFnPc1
ke	k	k7c3
vpádu	vpád	k1gInSc3
do	do	k7c2
Evropy	Evropa	k1gFnSc2
byly	být	k5eAaImAgFnP
zahájeny	zahájit	k5eAaPmNgFnP
v	v	k7c6
prosinci	prosinec	k1gInSc6
1525	#num#	k4
<g/>
,	,	kIx,
z	z	k7c2
tábora	tábor	k1gInSc2
v	v	k7c6
Edirne	Edirn	k1gInSc5
vytáhl	vytáhnout	k5eAaPmAgMnS
osmanský	osmanský	k2eAgMnSc1d1
sultán	sultán	k1gMnSc1
na	na	k7c6
začátku	začátek	k1gInSc6
května	květen	k1gInSc2
následujícího	následující	k2eAgInSc2d1
roku	rok	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
Vojsko	vojsko	k1gNnSc1
postupovalo	postupovat	k5eAaImAgNnS
směrem	směr	k1gInSc7
k	k	k7c3
Plovdivu	Plovdiv	k1gInSc3
na	na	k7c6
Sofii	Sofia	k1gFnSc6
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
odkud	odkud	k6eAd1
se	se	k3xPyFc4
vojáci	voják	k1gMnPc1
po	po	k7c6
několikadenním	několikadenní	k2eAgInSc6d1
odpočinku	odpočinek	k1gInSc6
vydali	vydat	k5eAaPmAgMnP
k	k	k7c3
Niši	Niši	k1gNnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
dorazily	dorazit	k5eAaPmAgInP
prvosledové	prvosledový	k2eAgInPc1d1
oddíly	oddíl	k1gInPc1
pod	pod	k7c7
velením	velení	k1gNnSc7
sultánova	sultánův	k2eAgMnSc2d1
přítele	přítel	k1gMnSc2
velkovezíra	velkovezír	k1gMnSc2
Ibrahima	Ibrahim	k1gMnSc2
Paši	paša	k1gMnSc2
k	k	k7c3
Bělehradu	Bělehrad	k1gInSc3
a	a	k8xC
12	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
se	se	k3xPyFc4
již	již	k6eAd1
turecká	turecký	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
nacházela	nacházet	k5eAaImAgFnS
před	před	k7c7
Petrovaradínem	Petrovaradín	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
byl	být	k5eAaImAgInS
bez	bez	k7c2
boje	boj	k1gInSc2
obsazen	obsazen	k2eAgInSc4d1
28	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Signály	signál	k1gInPc1
o	o	k7c6
reálném	reálný	k2eAgNnSc6d1
nebezpečí	nebezpečí	k1gNnSc6
z	z	k7c2
muslimské	muslimský	k2eAgFnSc2d1
strany	strana	k1gFnSc2
přicházely	přicházet	k5eAaImAgFnP
do	do	k7c2
Uher	Uhry	k1gFnPc2
již	již	k6eAd1
od	od	k7c2
podzimu	podzim	k1gInSc2
1525	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ludvík	Ludvík	k1gMnSc1
Jagellonský	jagellonský	k2eAgMnSc1d1
i	i	k8xC
jeho	jeho	k3xOp3gMnSc1
rada	rada	k1gMnSc1
si	se	k3xPyFc3
dobře	dobře	k6eAd1
uvědomovali	uvědomovat	k5eAaImAgMnP
<g/>
,	,	kIx,
že	že	k8xS
nejsou	být	k5eNaImIp3nP
schopni	schopen	k2eAgMnPc1d1
lépe	dobře	k6eAd2
vyzbrojené	vyzbrojený	k2eAgFnSc3d1
a	a	k8xC
početnější	početní	k2eAgFnSc3d2
turecké	turecký	k2eAgFnSc3d1
armádě	armáda	k1gFnSc3
vzdorovat	vzdorovat	k5eAaImF
<g/>
,	,	kIx,
začali	začít	k5eAaPmAgMnP
proto	proto	k8xC
usilovat	usilovat	k5eAaImF
o	o	k7c4
uzavření	uzavření	k1gNnSc4
míru	mír	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současně	současně	k6eAd1
také	také	k9
zahájili	zahájit	k5eAaPmAgMnP
přípravy	příprava	k1gFnPc4
na	na	k7c4
válku	válka	k1gFnSc4
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
doufali	doufat	k5eAaImAgMnP
ve	v	k7c4
výraznou	výrazný	k2eAgFnSc4d1
podporu	podpora	k1gFnSc4
ze	z	k7c2
zahraničí	zahraničí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
se	se	k3xPyFc4
však	však	k9
s	s	k7c7
velkým	velký	k2eAgInSc7d1
ohlasem	ohlas	k1gInSc7
nesetkali	setkat	k5eNaPmAgMnP
<g/>
,	,	kIx,
neboť	neboť	k8xC
Poláci	Polák	k1gMnPc1
uzavřeli	uzavřít	k5eAaPmAgMnP
s	s	k7c7
Osmany	Osman	k1gMnPc7
roční	roční	k2eAgInSc4d1
mír	mír	k1gInSc4
<g/>
,	,	kIx,
rakouský	rakouský	k2eAgMnSc1d1
arcivévoda	arcivévoda	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
dal	dát	k5eAaPmAgMnS
přednost	přednost	k1gFnSc4
svým	svůj	k3xOyFgInPc3
zájmům	zájem	k1gInPc3
v	v	k7c6
Itálii	Itálie	k1gFnSc6
a	a	k8xC
středomořská	středomořský	k2eAgFnSc1d1
velmoc	velmoc	k1gFnSc1
Benátky	Benátky	k1gFnPc4
pokládala	pokládat	k5eAaImAgFnS
Turky	turek	k1gInPc4
za	za	k7c4
obchodního	obchodní	k2eAgMnSc4d1
a	a	k8xC
politického	politický	k2eAgMnSc4d1
partnera	partner	k1gMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
Výzvy	výzva	k1gFnSc2
k	k	k7c3
pomoci	pomoc	k1gFnSc3
dosáhly	dosáhnout	k5eAaPmAgInP
výrazného	výrazný	k2eAgMnSc4d1
podpory	podpora	k1gFnSc2
pouze	pouze	k6eAd1
ve	v	k7c6
Vatikánu	Vatikán	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
bylo	být	k5eAaImAgNnS
pro	pro	k7c4
boj	boj	k1gInSc4
s	s	k7c7
pohany	pohana	k1gFnSc2
vyčleněno	vyčlenit	k5eAaPmNgNnS
25	#num#	k4
000	#num#	k4
dukátů	dukát	k1gInPc2
<g/>
,	,	kIx,
za	za	k7c4
něž	jenž	k3xRgFnPc4
se	se	k3xPyFc4
však	však	k9
podařilo	podařit	k5eAaPmAgNnS
najmout	najmout	k5eAaPmF
pouze	pouze	k6eAd1
1300	#num#	k4
moravských	moravský	k2eAgMnPc2d1
žoldnéřů	žoldnéř	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
domácí	domácí	k2eAgFnSc6d1
frontě	fronta	k1gFnSc6
byla	být	k5eAaImAgFnS
situace	situace	k1gFnSc1
také	také	k9
kritická	kritický	k2eAgFnSc1d1
<g/>
,	,	kIx,
neboť	neboť	k8xC
na	na	k7c6
shromaždišti	shromaždiště	k1gNnSc6
vojska	vojsko	k1gNnSc2
u	u	k7c2
Tolny	Tolna	k1gFnSc2
ještě	ještě	k9
2	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
netábořil	tábořit	k5eNaImAgMnS
ani	ani	k8xC
jediný	jediný	k2eAgMnSc1d1
ozbrojenec	ozbrojenec	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgMnPc4
váhající	váhající	k2eAgMnPc1d1
šlechtici	šlechtic	k1gMnPc1
se	se	k3xPyFc4
ke	k	k7c3
králi	král	k1gMnSc3
začali	začít	k5eAaPmAgMnP
připojovat	připojovat	k5eAaImF
teprve	teprve	k6eAd1
až	až	k9
po	po	k7c6
pádu	pád	k1gInSc6
Petrovaradína	Petrovaradín	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
Z	z	k7c2
Tolny	Tolna	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
byli	být	k5eAaImAgMnP
při	při	k7c6
velitelských	velitelský	k2eAgFnPc6d1
poradách	porada	k1gFnPc6
vrchními	vrchní	k2eAgMnPc7d1
hejtmany	hejtman	k1gMnPc7
vojska	vojsko	k1gNnSc2
jmenovaní	jmenovaný	k1gMnPc1
Pál	Pál	k1gMnPc1
Tomory	Tomora	k1gFnSc2
a	a	k8xC
György	Györg	k1gInPc4
Zápolský	Zápolský	k2eAgInSc1d1
<g/>
,	,	kIx,
se	se	k3xPyFc4
uherské	uherský	k2eAgInPc1d1
oddíly	oddíl	k1gInPc1
ve	v	k7c6
dnech	den	k1gInPc6
14	#num#	k4
<g/>
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
–	–	k?
<g/>
19	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
pomalým	pomalý	k2eAgInSc7d1
pochodem	pochod	k1gInSc7
přesunuly	přesunout	k5eAaPmAgFnP
k	k	k7c3
městečku	městečko	k1gNnSc3
Moháč	Moháč	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
řadách	řada	k1gFnPc6
zúčastněných	zúčastněný	k2eAgMnPc2d1
kontingentů	kontingent	k1gInPc2
však	však	k9
citelně	citelně	k6eAd1
chybělo	chybět	k5eAaImAgNnS
15	#num#	k4
000	#num#	k4
vojáků	voják	k1gMnPc2
sedmihradského	sedmihradský	k2eAgMnSc2d1
vévody	vévoda	k1gMnSc2
Jana	Jan	k1gMnSc2
Zápolského	Zápolský	k2eAgMnSc2d1
<g/>
,	,	kIx,
setrvávajících	setrvávající	k2eAgInPc2d1
v	v	k7c6
ležení	ležení	k1gNnSc6
u	u	k7c2
řeky	řeka	k1gFnSc2
Tisy	Tisa	k1gFnSc2
<g/>
,	,	kIx,
vojáci	voják	k1gMnPc1
shromáždění	shromáždění	k1gNnPc2
z	z	k7c2
prostředků	prostředek	k1gInPc2
zemí	zem	k1gFnPc2
Koruny	koruna	k1gFnSc2
české	český	k2eAgFnSc2d1
a	a	k8xC
početný	početný	k2eAgInSc1d1
odřad	odřad	k1gInSc1
velmože	velmož	k1gMnSc2
Kryštofa	Kryštof	k1gMnSc2
Frangepána	Frangepán	k2eAgFnSc1d1
stojící	stojící	k2eAgFnSc1d1
u	u	k7c2
Záhřebu	Záhřeb	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vojska	vojsko	k1gNnPc1
</s>
<s>
Uherské	uherský	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
Daniel	Daniel	k1gMnSc1
Hopfer	Hopfer	k1gMnSc1
<g/>
:	:	kIx,
němečtí	německý	k2eAgMnPc1d1
lancknechti	lancknecht	k1gMnPc1
na	na	k7c6
rytině	rytina	k1gFnSc6
z	z	k7c2
roku	rok	k1gInSc2
1530	#num#	k4
</s>
<s>
O	o	k7c6
početních	početní	k2eAgInPc6d1
stavech	stav	k1gInPc6
uherského	uherský	k2eAgNnSc2d1
vojska	vojsko	k1gNnSc2
zanechali	zanechat	k5eAaPmAgMnP
soudobí	soudobý	k2eAgMnPc1d1
kronikáři	kronikář	k1gMnPc1
poměrně	poměrně	k6eAd1
shodné	shodný	k2eAgInPc4d1
údaje	údaj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kancléř	kancléř	k1gMnSc1
Brodarić	Brodarić	k1gMnSc1
hovoří	hovořit	k5eAaImIp3nS
o	o	k7c4
24	#num#	k4
000	#num#	k4
mužích	muž	k1gMnPc6
<g/>
,	,	kIx,
papežský	papežský	k2eAgInSc1d1
legát	legát	k1gInSc1
Burgi	Burgi	k1gNnSc2
o	o	k7c4
25	#num#	k4
<g/>
–	–	k?
<g/>
30	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
bude	být	k5eAaImBp3nS
opomenuta	opomenut	k2eAgFnSc1d1
srpnová	srpnový	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
ze	z	k7c2
Záhřebu	Záhřeb	k1gInSc2
do	do	k7c2
Benátek	Benátky	k1gFnPc2
hovořící	hovořící	k2eAgInSc1d1
až	až	k6eAd1
o	o	k7c4
109	#num#	k4
000	#num#	k4
vojácích	voják	k1gMnPc6
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
přidržují	přidržovat	k5eAaImIp3nP
se	se	k3xPyFc4
těchto	tento	k3xDgInPc2
odhadů	odhad	k1gInPc2
i	i	k9
současní	současný	k2eAgMnPc1d1
odborníci	odborník	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
předpokládají	předpokládat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
Ludvík	Ludvík	k1gMnSc1
Jagellonský	jagellonský	k2eAgMnSc1d1
u	u	k7c2
Moháče	Moháč	k1gInSc2
velel	velet	k5eAaImAgInS
maximálně	maximálně	k6eAd1
25	#num#	k4
000	#num#	k4
ozbrojencům	ozbrojenec	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
tohoto	tento	k3xDgInSc2
počtu	počet	k1gInSc2
asi	asi	k9
8000	#num#	k4
bojovníků	bojovník	k1gMnPc2
pocházelo	pocházet	k5eAaImAgNnS
ze	z	k7c2
zahraničí	zahraničí	k1gNnSc2
<g/>
,	,	kIx,
zejména	zejména	k9
pak	pak	k6eAd1
z	z	k7c2
Čech	Čechy	k1gFnPc2
<g/>
,	,	kIx,
Moravy	Morava	k1gFnSc2
a	a	k8xC
Slezska	Slezsko	k1gNnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
bitvy	bitva	k1gFnPc1
se	se	k3xPyFc4
však	však	k9
účastnilo	účastnit	k5eAaImAgNnS
i	i	k9
1500	#num#	k4
polských	polský	k2eAgMnPc2d1
jezdců	jezdec	k1gMnPc2
v	v	k7c6
čele	čelo	k1gNnSc6
s	s	k7c7
Leonardem	Leonardo	k1gMnSc7
Gnojenským	Gnojenský	k2eAgMnSc7d1
<g/>
,	,	kIx,
papežská	papežský	k2eAgFnSc1d1
pěchota	pěchota	k1gFnSc1
vedená	vedený	k2eAgFnSc1d1
Hannibalem	Hannibal	k1gInSc7
z	z	k7c2
Cypru	Cypr	k1gInSc2
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
žoldnéři	žoldnér	k1gMnPc1
ze	z	k7c2
Španělska	Španělsko	k1gNnSc2
<g/>
,	,	kIx,
Itálie	Itálie	k1gFnSc2
<g/>
,	,	kIx,
Německa	Německo	k1gNnSc2
a	a	k8xC
Chorvatska	Chorvatsko	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Zbytek	zbytek	k1gInSc1
vojska	vojsko	k1gNnSc2
tvořily	tvořit	k5eAaImAgFnP
družiny	družina	k1gFnPc1
jednotlivých	jednotlivý	k2eAgMnPc2d1
uherských	uherský	k2eAgMnPc2d1
magnátů	magnát	k1gMnPc2
a	a	k8xC
další	další	k2eAgMnPc1d1
příslušníci	příslušník	k1gMnPc1
šlechtické	šlechtický	k2eAgFnSc2d1
hotovosti	hotovost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
nejkvalitnějším	kvalitní	k2eAgMnPc3d3
vojákům	voják	k1gMnPc3
patřili	patřit	k5eAaImAgMnP
především	především	k9
příslušníci	příslušník	k1gMnPc1
těžké	těžký	k2eAgFnSc2d1
jízdy	jízda	k1gFnSc2
a	a	k8xC
žoldnéři	žoldnér	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
měli	mít	k5eAaImAgMnP
jak	jak	k6eAd1
vojenský	vojenský	k2eAgInSc4d1
výcvik	výcvik	k1gInSc4
<g/>
,	,	kIx,
tak	tak	k6eAd1
zkušenosti	zkušenost	k1gFnPc1
z	z	k7c2
předešlých	předešlý	k2eAgInPc2d1
bojů	boj	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
Sám	sám	k3xTgMnSc1
Ludvík	Ludvík	k1gMnSc1
Jagellonský	jagellonský	k2eAgInSc1d1
opustil	opustit	k5eAaPmAgInS
Budínský	budínský	k2eAgInSc1d1
hrad	hrad	k1gInSc1
s	s	k7c7
družinou	družina	k1gFnSc7
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gInSc4
předvoj	předvoj	k1gInSc4
tvořilo	tvořit	k5eAaImAgNnS
asi	asi	k9
600	#num#	k4
českých	český	k2eAgMnPc2d1
<g/>
,	,	kIx,
moravských	moravský	k2eAgMnPc2d1
a	a	k8xC
slezských	slezský	k2eAgMnPc2d1
žoldnéřů	žoldnéř	k1gMnPc2
na	na	k7c6
válečných	válečný	k2eAgInPc6d1
vozech	vůz	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
nimi	on	k3xPp3gMnPc7
postupovalo	postupovat	k5eAaImAgNnS
2500	#num#	k4
příslušníků	příslušník	k1gMnPc2
uherské	uherský	k2eAgFnSc2d1
a	a	k8xC
polské	polský	k2eAgFnSc2d1
lehké	lehký	k2eAgFnSc2d1
jízdy	jízda	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
následovalo	následovat	k5eAaImAgNnS
600	#num#	k4
těžkooděných	těžkooděný	k2eAgMnPc2d1
jezdců	jezdec	k1gMnPc2
pod	pod	k7c7
velením	velení	k1gNnSc7
Ambrože	Ambrož	k1gMnSc2
Sarkányho	Sarkány	k1gMnSc2
a	a	k8xC
kancléře	kancléř	k1gMnSc2
Brodariće	Brodarić	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c7
tímto	tento	k3xDgInSc7
oddílem	oddíl	k1gInSc7
se	se	k3xPyFc4
přesouvala	přesouvat	k5eAaImAgFnS
panovníkova	panovníkův	k2eAgFnSc1d1
pěší	pěší	k2eAgFnSc1d1
garda	garda	k1gFnSc1
a	a	k8xC
samotného	samotný	k2eAgMnSc2d1
krále	král	k1gMnSc2
obklopovalo	obklopovat	k5eAaImAgNnS
padesát	padesát	k4xCc1
vybraných	vybraný	k2eAgMnPc2d1
německých	německý	k2eAgMnPc2d1
lancknechtů	lancknecht	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
Celkově	celkově	k6eAd1
lze	lze	k6eAd1
předpokládat	předpokládat	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
Uherské	uherský	k2eAgNnSc1d1
vojsko	vojsko	k1gNnSc1
skládalo	skládat	k5eAaImAgNnS
z	z	k7c2
10	#num#	k4
000	#num#	k4
pěšáků	pěšák	k1gMnPc2
a	a	k8xC
15	#num#	k4
000	#num#	k4
jezdců	jezdec	k1gMnPc2
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
podporovalo	podporovat	k5eAaImAgNnS
jej	on	k3xPp3gNnSc4
85	#num#	k4
<g/>
–	–	k?
<g/>
90	#num#	k4
děl	dělo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kázeň	kázeň	k1gFnSc1
tohoto	tento	k3xDgNnSc2
uskupení	uskupení	k1gNnSc2
nebyla	být	k5eNaImAgFnS
valná	valný	k2eAgFnSc1d1
a	a	k8xC
oddílům	oddíl	k1gInPc3
chyběla	chybět	k5eAaImAgFnS
vzájemná	vzájemný	k2eAgFnSc1d1
souhra	souhra	k1gFnSc1
i	i	k8xC
secvičenost	secvičenost	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Osmanská	osmanský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
</s>
<s>
Johann	Johann	k1gMnSc1
Christoph	Christoph	k1gMnSc1
Weigel	Weigel	k1gMnSc1
<g/>
:	:	kIx,
janičár	janičár	k1gMnSc1
</s>
<s>
Podle	podle	k7c2
reálných	reálný	k2eAgInPc2d1
odhadů	odhad	k1gInPc2
se	se	k3xPyFc4
uherského	uherský	k2eAgMnSc2d1
tažení	tažení	k1gNnSc1
účastnilo	účastnit	k5eAaImAgNnS
kolem	kolem	k7c2
100	#num#	k4
000	#num#	k4
osmanských	osmanský	k2eAgMnPc2d1
vojáků	voják	k1gMnPc2
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
29	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
jich	on	k3xPp3gFnPc2
měl	mít	k5eAaImAgMnS
sultán	sultán	k1gMnSc1
Süleyman	Süleyman	k1gMnSc1
k	k	k7c3
dispozici	dispozice	k1gFnSc3
zhruba	zhruba	k6eAd1
55	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Jádro	jádro	k1gNnSc4
těchto	tento	k3xDgFnPc2
ozbrojených	ozbrojený	k2eAgFnPc2d1
sil	síla	k1gFnPc2
tvořili	tvořit	k5eAaImAgMnP
janičáři	janičár	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příslušníci	příslušník	k1gMnPc1
tohoto	tento	k3xDgInSc2
uzavřeného	uzavřený	k2eAgInSc2d1
elitního	elitní	k2eAgInSc2d1
sboru	sbor	k1gInSc2
pocházeli	pocházet	k5eAaImAgMnP
z	z	k7c2
Balkánských	balkánský	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
<g/>
,	,	kIx,
odkud	odkud	k6eAd1
byli	být	k5eAaImAgMnP
odváděni	odváděn	k2eAgMnPc1d1
od	od	k7c2
svých	svůj	k3xOyFgMnPc2
křesťanských	křesťanský	k2eAgMnPc2d1
rodičů	rodič	k1gMnPc2
v	v	k7c6
rámci	rámec	k1gInSc6
zvláštní	zvláštní	k2eAgFnSc2d1
daně	daň	k1gFnSc2
(	(	kIx(
<g/>
devširme	devširm	k1gInSc5
<g/>
)	)	kIx)
ještě	ještě	k9
dětském	dětský	k2eAgInSc6d1
věku	věk	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výcvik	výcvik	k1gInSc4
mladíci	mladík	k1gMnPc1
prodělali	prodělat	k5eAaPmAgMnP
ve	v	k7c6
speciálních	speciální	k2eAgNnPc6d1
islámských	islámský	k2eAgNnPc6d1
učilištích	učiliště	k1gNnPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
jim	on	k3xPp3gMnPc3
byla	být	k5eAaImAgFnS
tuhou	tuhý	k2eAgFnSc7d1
kázní	kázeň	k1gFnSc7
vštěpena	vštěpen	k2eAgFnSc1d1
fanatická	fanatický	k2eAgFnSc1d1
oddanost	oddanost	k1gFnSc1
sultánovi	sultán	k1gMnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
době	doba	k1gFnSc6
moháčské	moháčský	k2eAgFnSc2d1
kampaně	kampaň	k1gFnSc2
v	v	k7c6
řadách	řada	k1gFnPc6
janičárů	janičár	k1gMnPc2
sloužilo	sloužit	k5eAaImAgNnS
zhruba	zhruba	k6eAd1
10	#num#	k4
<g/>
–	–	k?
<g/>
12	#num#	k4
000	#num#	k4
mužů	muž	k1gMnPc2
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
výhradně	výhradně	k6eAd1
pěšáků	pěšák	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
boje	boj	k1gInSc2
se	se	k3xPyFc4
vyzbrojovali	vyzbrojovat	k5eAaImAgMnP
především	především	k6eAd1
mušketami	mušketa	k1gFnPc7
s	s	k7c7
doutnákovým	doutnákový	k2eAgInSc7d1
zámkem	zámek	k1gInSc7
<g/>
,	,	kIx,
šavlemi	šavle	k1gFnPc7
a	a	k8xC
jatagany	jatagan	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nepoužívali	používat	k5eNaImAgMnP
žádnou	žádný	k3yNgFnSc4
zbroj	zbroj	k1gFnSc4
<g/>
,	,	kIx,
na	na	k7c6
hlavě	hlava	k1gFnSc6
nosili	nosit	k5eAaImAgMnP
vysokou	vysoký	k2eAgFnSc4d1
čepici	čepice	k1gFnSc4
z	z	k7c2
velbloudí	velbloudí	k2eAgFnSc2d1
srsti	srst	k1gFnSc2
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc1
špice	špice	k1gFnSc1
nebyla	být	k5eNaImAgFnS
sešita	sešít	k5eAaPmNgFnS
<g/>
,	,	kIx,
ale	ale	k8xC
materiál	materiál	k1gInSc1
jim	on	k3xPp3gMnPc3
ve	v	k7c6
volném	volný	k2eAgInSc6d1
pruhu	pruh	k1gInSc6
přepadával	přepadávat	k5eAaImAgInS
až	až	k9
na	na	k7c4
záda	záda	k1gNnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
tureckých	turecký	k2eAgInPc2d1
pramenů	pramen	k1gInPc2
v	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
Süleyman	Süleyman	k1gMnSc1
I.	I.	kA
disponoval	disponovat	k5eAaBmAgMnS
také	také	k9
asi	asi	k9
<g />
.	.	kIx.
</s>
<s hack="1">
50	#num#	k4
000	#num#	k4
příslušníky	příslušník	k1gMnPc7
svého	svůj	k3xOyFgNnSc2
osobního	osobní	k2eAgNnSc2d1
vojska	vojsko	k1gNnSc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgMnPc2
většinu	většina	k1gFnSc4
tvořili	tvořit	k5eAaImAgMnP
dvorští	dvorský	k2eAgMnPc1d1
sipáhiové	sipáhius	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
svou	svůj	k3xOyFgFnSc4
službu	služba	k1gFnSc4
vykonávali	vykonávat	k5eAaImAgMnP
v	v	k7c6
posádkách	posádka	k1gFnPc6
osmanské	osmanský	k2eAgFnSc2d1
metropole	metropol	k1gFnSc2
a	a	k8xC
byli	být	k5eAaImAgMnP
placeni	platit	k5eAaImNgMnP
ze	z	k7c2
sultánových	sultánův	k2eAgInPc2d1
finančních	finanční	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
Hlavu	hlava	k1gFnSc4
si	se	k3xPyFc3
tito	tento	k3xDgMnPc1
vojáci	voják	k1gMnPc1
chránili	chránit	k5eAaImAgMnP
špičatými	špičatý	k2eAgFnPc7d1
přilbami	přilba	k1gFnPc7
kónického	kónický	k2eAgInSc2d1
tvaru	tvar	k1gInSc2
<g/>
,	,	kIx,
vyzbrojovali	vyzbrojovat	k5eAaImAgMnP
se	se	k3xPyFc4
reflexními	reflexní	k2eAgInPc7d1
luky	luk	k1gInPc7
<g/>
,	,	kIx,
dlouhými	dlouhý	k2eAgInPc7d1
kopími	kopí	k1gNnPc7
<g/>
,	,	kIx,
šavlemi	šavle	k1gFnPc7
<g/>
,	,	kIx,
palcáty	palcát	k1gInPc7
a	a	k8xC
malými	malý	k2eAgInPc7d1
kruhovými	kruhový	k2eAgInPc7d1
štíty	štít	k1gInPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
Číslo	číslo	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
by	by	kYmCp3nS
stanovilo	stanovit	k5eAaPmAgNnS
<g/>
,	,	kIx,
kolik	kolik	k9
z	z	k7c2
těchto	tento	k3xDgMnPc2
vojáků	voják	k1gMnPc2
se	se	k3xPyFc4
účastnilo	účastnit	k5eAaImAgNnS
samotné	samotný	k2eAgFnPc4d1
bitvy	bitva	k1gFnPc4
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
známo	znám	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neznámý	známý	k2eNgInSc1d1
podíl	podíl	k1gInSc1
muslimské	muslimský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
tvořily	tvořit	k5eAaImAgFnP
také	také	k9
nepravidelné	pravidelný	k2eNgFnPc1d1
jednotky	jednotka	k1gFnPc1
najaté	najatý	k2eAgFnPc1d1
v	v	k7c6
průběhu	průběh	k1gInSc6
tažení	tažení	k1gNnSc2
na	na	k7c6
Balkáně	Balkán	k1gInSc6
<g/>
,	,	kIx,
spojenečtí	spojenecký	k2eAgMnPc1d1
Tataři	Tatar	k1gMnPc1
a	a	k8xC
oddíly	oddíl	k1gInPc1
vyslané	vyslaný	k2eAgInPc1d1
z	z	k7c2
dalších	další	k2eAgFnPc2d1
evropských	evropský	k2eAgFnPc2d1
a	a	k8xC
asijských	asijský	k2eAgFnPc2d1
provincií	provincie	k1gFnPc2
Osmanské	osmanský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
(	(	kIx(
<g/>
azábové	azábová	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Svou	svůj	k3xOyFgFnSc4
úlohu	úloha	k1gFnSc4
v	v	k7c6
událostech	událost	k1gFnPc6
u	u	k7c2
Moháče	Moháč	k1gInSc2
sehrálo	sehrát	k5eAaPmAgNnS
také	také	k9
Süleymanovo	Süleymanův	k2eAgNnSc1d1
dělostřelectvo	dělostřelectvo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základem	základ	k1gInSc7
jeho	jeho	k3xOp3gFnSc2
artilerie	artilerie	k1gFnSc2
byly	být	k5eAaImAgInP
lehčí	lehký	k2eAgInPc1d2
modely	model	k1gInPc1
tzv.	tzv.	kA
zarbuzanů	zarbuzan	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
svou	svůj	k3xOyFgFnSc4
konstrukcí	konstrukce	k1gFnPc2
vycházely	vycházet	k5eAaImAgFnP
ze	z	k7c2
středoevropských	středoevropský	k2eAgInPc2d1
falkonetů	falkonet	k1gInPc2
a	a	k8xC
kulverinů	kulverin	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
Nejmenší	malý	k2eAgFnSc1d3
z	z	k7c2
těchto	tento	k3xDgNnPc2
děl	dělo	k1gNnPc2
se	se	k3xPyFc4
daly	dát	k5eAaPmAgInP
rozložit	rozložit	k5eAaPmF
na	na	k7c4
dva	dva	k4xCgInPc4
kusy	kus	k1gInPc4
a	a	k8xC
bylo	být	k5eAaImAgNnS
možno	možno	k6eAd1
je	on	k3xPp3gInPc4
převážet	převážet	k5eAaImF
na	na	k7c6
jednom	jeden	k4xCgMnSc6
oslovi	osel	k1gMnSc6
nebo	nebo	k8xC
velbloudovi	velbloud	k1gMnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
přesunům	přesun	k1gInPc3
větších	veliký	k2eAgFnPc2d2
kusů	kus	k1gInPc2
bylo	být	k5eAaImAgNnS
zapotřebí	zapotřebí	k6eAd1
koňských	koňský	k2eAgNnPc2d1
spřežení	spřežení	k1gNnPc2
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gFnSc4
obsluhu	obsluha	k1gFnSc4
zajišťovali	zajišťovat	k5eAaImAgMnP
až	až	k9
tři	tři	k4xCgMnPc1
muži	muž	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
munice	munice	k1gFnPc1
byly	být	k5eAaImAgFnP
do	do	k7c2
hlavní	hlavní	k2eAgFnSc2d1
nabíjeny	nabíjet	k5eAaImNgInP
olověné	olověný	k2eAgInPc1d1
nebo	nebo	k8xC
kamenné	kamenný	k2eAgInPc1d1
projektily	projektil	k1gInPc1
o	o	k7c6
váze	váha	k1gFnSc6
mezi	mezi	k7c7
jedním	jeden	k4xCgInSc7
až	až	k8xS
třemi	tři	k4xCgInPc7
kilogramy	kilogram	k1gInPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
Odhaduje	odhadovat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
na	na	k7c6
straně	strana	k1gFnSc6
osmanské	osmanský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
bylo	být	k5eAaImAgNnS
do	do	k7c2
střetnutí	střetnutí	k1gNnPc2
zapojeno	zapojit	k5eAaPmNgNnS
minimálně	minimálně	k6eAd1
160	#num#	k4
děl	dělo	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bojiště	bojiště	k1gNnSc1
</s>
<s>
Památník	památník	k1gInSc1
bitvy	bitva	k1gFnSc2
u	u	k7c2
Moháče	Moháč	k1gInSc2
"	"	kIx"
<g/>
Mohácsi	Mohácse	k1gFnSc4
Nemzeti	Nemzeti	k1gFnSc2
Emlékhely	Emlékhela	k1gFnSc2
<g/>
"	"	kIx"
(	(	kIx(
<g/>
odkaz	odkaz	k1gInSc4
na	na	k7c4
umístění	umístění	k1gNnSc4
na	na	k7c6
mapě	mapa	k1gFnSc6
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
současnosti	současnost	k1gFnSc6
se	se	k3xPyFc4
pláň	pláň	k1gFnSc1
<g/>
,	,	kIx,
na	na	k7c6
níž	jenž	k3xRgFnSc6
se	se	k3xPyFc4
odehrály	odehrát	k5eAaPmAgFnP
události	událost	k1gFnPc1
z	z	k7c2
29	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1526	#num#	k4
<g/>
,	,	kIx,
na	na	k7c6
západní	západní	k2eAgFnSc6d1
hranici	hranice	k1gFnSc6
táhne	táhnout	k5eAaImIp3nS
k	k	k7c3
osadě	osada	k1gFnSc3
Nagynyárád	Nagynyáráda	k1gFnPc2
u	u	k7c2
železniční	železniční	k2eAgFnSc2d1
tratě	trať	k1gFnSc2
z	z	k7c2
Moháče	Moháč	k1gInSc2
do	do	k7c2
Pětikostelí	pětikostelí	k1gNnSc2
<g/>
,	,	kIx,
na	na	k7c6
východě	východ	k1gInSc6
se	se	k3xPyFc4
pak	pak	k6eAd1
opírá	opírat	k5eAaImIp3nS
o	o	k7c6
silnici	silnice	k1gFnSc6
E	E	kA
73	#num#	k4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
vede	vést	k5eAaImIp3nS
z	z	k7c2
Moháče	Moháč	k1gInSc2
do	do	k7c2
chorvatského	chorvatský	k2eAgInSc2d1
Osijeku	Osijek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
době	doba	k1gFnSc6
střetnutí	střetnutí	k1gNnSc2
tuto	tento	k3xDgFnSc4
oblast	oblast	k1gFnSc4
vymezovaly	vymezovat	k5eAaImAgInP
břehy	břeh	k1gInPc1
Dunaje	Dunaj	k1gInSc2
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
se	se	k3xPyFc4
rozmáchlým	rozmáchlý	k2eAgInSc7d1
meandrem	meandr	k1gInSc7
zařezával	zařezávat	k5eAaImAgInS
hluboko	hluboko	k6eAd1
k	k	k7c3
západu	západ	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Břehy	břeh	k1gInPc4
řeky	řeka	k1gFnSc2
lemoval	lemovat	k5eAaImAgInS
souvislý	souvislý	k2eAgInSc1d1
pás	pás	k1gInSc1
bažin	bažina	k1gFnPc2
a	a	k8xC
močálů	močál	k1gInPc2
<g/>
,	,	kIx,
porostlý	porostlý	k2eAgInSc1d1
hustou	hustý	k2eAgFnSc7d1
vegetací	vegetace	k1gFnSc7
rákosu	rákos	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
Z	z	k7c2
jihu	jih	k1gInSc2
se	se	k3xPyFc4
nad	nad	k7c7
bojištěm	bojiště	k1gNnSc7
vypínal	vypínat	k5eAaImAgInS
hřeben	hřeben	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
převyšoval	převyšovat	k5eAaImAgInS
okolní	okolní	k2eAgInSc4d1
terén	terén	k1gInSc4
asi	asi	k9
o	o	k7c4
30	#num#	k4
<g/>
–	–	k?
<g/>
40	#num#	k4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ukončoval	ukončovat	k5eAaImAgMnS
6,5	6,5	k4
km	km	kA
širokou	široký	k2eAgFnSc7d1
plošinou	plošina	k1gFnSc7
a	a	k8xC
prudce	prudko	k6eAd1
se	se	k3xPyFc4
svažoval	svažovat	k5eAaImAgMnS
k	k	k7c3
severu	sever	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Půda	půda	k1gFnSc1
nasáklá	nasáklý	k2eAgFnSc1d1
deštěm	dešť	k1gInSc7
z	z	k7c2
předchozích	předchozí	k2eAgInPc2d1
dní	den	k1gInPc2
byla	být	k5eAaImAgFnS
v	v	k7c6
těchto	tento	k3xDgNnPc6
místech	místo	k1gNnPc6
značně	značně	k6eAd1
kluzká	kluzký	k2eAgFnSc1d1
a	a	k8xC
velmi	velmi	k6eAd1
znesnadňovala	znesnadňovat	k5eAaImAgFnS
příchozím	příchozí	k1gMnPc3
osmanským	osmanský	k2eAgFnPc3d1
jednotkám	jednotka	k1gFnPc3
sestup	sestup	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zhruba	zhruba	k6eAd1
uprostřed	uprostřed	k7c2
hřebene	hřeben	k1gInSc2
byl	být	k5eAaImAgInS
položen	položit	k5eAaPmNgInS
záhyb	záhyb	k1gInSc1
<g/>
,	,	kIx,
v	v	k7c6
jehož	jehož	k3xOyRp3gInSc6
středu	střed	k1gInSc6
ležela	ležet	k5eAaImAgFnS
již	již	k6eAd1
zaniklá	zaniklý	k2eAgFnSc1d1
osada	osada	k1gFnSc1
Földvár	Földvár	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
Povrch	povrch	k7c2wR
bojiště	bojiště	k1gNnSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
pokrývaly	pokrývat	k5eAaImAgFnP
nepravidelně	pravidelně	k6eNd1
rozmístěné	rozmístěný	k2eAgFnPc1d1
prohlubně	prohlubeň	k1gFnPc1
a	a	k8xC
nízké	nízký	k2eAgInPc1d1
hřebeny	hřeben	k1gInPc1
<g/>
,	,	kIx,
tvořila	tvořit	k5eAaImAgFnS
sypká	sypký	k2eAgFnSc1d1
půda	půda	k1gFnSc1
z	z	k7c2
nánosů	nános	k1gInPc2
hlíny	hlína	k1gFnSc2
<g/>
,	,	kIx,
písku	písek	k1gInSc2
a	a	k8xC
kamení	kamení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
vytrvalých	vytrvalý	k2eAgInPc6d1
srpnových	srpnový	k2eAgInPc6d1
deštích	dešť	k1gInPc6
se	se	k3xPyFc4
většina	většina	k1gFnSc1
přítoků	přítok	k1gInPc2
Dunaje	Dunaj	k1gInSc2
vylila	vylít	k5eAaPmAgFnS
z	z	k7c2
břehů	břeh	k1gInPc2
a	a	k8xC
mnoho	mnoho	k4c1
cest	cesta	k1gFnPc2
v	v	k7c6
širokém	široký	k2eAgNnSc6d1
okolí	okolí	k1gNnSc6
se	se	k3xPyFc4
proměnilo	proměnit	k5eAaPmAgNnS
v	v	k7c4
těžko	těžko	k6eAd1
překročitelné	překročitelný	k2eAgFnPc4d1
mokřiny	mokřina	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Příchod	příchod	k1gInSc1
Osmanů	Osman	k1gMnPc2
</s>
<s>
Sultán	sultán	k1gMnSc1
Süleyman	Süleyman	k1gMnSc1
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Moháče	Moháč	k1gInSc2
(	(	kIx(
<g/>
turecká	turecký	k2eAgFnSc1d1
miniatura	miniatura	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1
tábor	tábor	k1gInSc1
turecké	turecký	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
se	se	k3xPyFc4
ráno	ráno	k6eAd1
29	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1526	#num#	k4
nacházel	nacházet	k5eAaImAgMnS
na	na	k7c6
severním	severní	k2eAgInSc6d1
břehu	břeh	k1gInSc6
říčky	říčka	k1gFnSc2
Karašica	Karašic	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
místech	místo	k1gNnPc6
dnešních	dnešní	k2eAgFnPc2d1
osad	osada	k1gFnPc2
Branjina	Branjino	k1gNnSc2
a	a	k8xC
Branjin	Branjin	k2eAgInSc4d1
Vrch	vrch	k1gInSc4
<g/>
,	,	kIx,
ležících	ležící	k2eAgFnPc2d1
zhruba	zhruba	k6eAd1
deset	deset	k4xCc4
kilometrů	kilometr	k1gInPc2
od	od	k7c2
budoucího	budoucí	k2eAgNnSc2d1
bojiště	bojiště	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
předchozím	předchozí	k2eAgInSc6d1
celodenním	celodenní	k2eAgInSc6d1
odpočinku	odpočinek	k1gInSc6
dostali	dostat	k5eAaPmAgMnP
vojáci	voják	k1gMnPc1
rozkaz	rozkaz	k1gInSc1
být	být	k5eAaImF
připraveni	připravit	k5eAaPmNgMnP
vyrazit	vyrazit	k5eAaPmF
proti	proti	k7c3
nepříteli	nepřítel	k1gMnSc3
již	již	k6eAd1
v	v	k7c4
pět	pět	k4xCc4
hodin	hodina	k1gFnPc2
ráno	ráno	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
sever	sever	k1gInSc4
směrem	směr	k1gInSc7
k	k	k7c3
Moháči	Moháč	k1gInSc3
postupovali	postupovat	k5eAaImAgMnP
v	v	k7c6
bojové	bojový	k2eAgFnSc6d1
sestavě	sestava	k1gFnSc6
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gInSc1
pochod	pochod	k1gInSc1
byl	být	k5eAaImAgInS
ovšem	ovšem	k9
značně	značně	k6eAd1
namáhavý	namáhavý	k2eAgMnSc1d1
<g/>
,	,	kIx,
neboť	neboť	k8xC
deště	dešť	k1gInPc1
z	z	k7c2
předchozích	předchozí	k2eAgInPc2d1
dní	den	k1gInPc2
rozmáčely	rozmáčet	k5eAaBmAgFnP
všechny	všechen	k3xTgFnPc1
cesty	cesta	k1gFnPc1
a	a	k8xC
voda	voda	k1gFnSc1
z	z	k7c2
blízkých	blízký	k2eAgFnPc2d1
řek	řeka	k1gFnPc2
a	a	k8xC
potoků	potok	k1gInPc2
se	se	k3xPyFc4
vylila	vylít	k5eAaPmAgFnS
z	z	k7c2
břehů	břeh	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
Do	do	k7c2
bláta	bláto	k1gNnSc2
se	se	k3xPyFc4
tak	tak	k6eAd1
bořily	bořit	k5eAaImAgFnP
nejen	nejen	k6eAd1
nohy	noha	k1gFnPc4
vojáků	voják	k1gMnPc2
a	a	k8xC
koní	kůň	k1gMnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
kola	kolo	k1gNnSc2
na	na	k7c6
lafetách	lafeta	k1gFnPc6
zhruba	zhruba	k6eAd1
tří	tři	k4xCgNnPc2
set	sto	k4xCgNnPc2
děl	dělo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
nejvýraznější	výrazný	k2eAgFnPc4d3
potíže	potíž	k1gFnPc4
však	však	k9
narazily	narazit	k5eAaPmAgFnP
jednotky	jednotka	k1gFnPc1
na	na	k7c6
pravém	pravý	k2eAgNnSc6d1
křídle	křídlo	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
byl	být	k5eAaImAgInS
dislokován	dislokován	k2eAgInSc1d1
třicetitisícový	třicetitisícový	k2eAgInSc1d1
anatolský	anatolský	k2eAgInSc1d1
sbor	sbor	k1gInSc1
pod	pod	k7c7
velením	velení	k1gNnSc7
anatolského	anatolský	k2eAgMnSc2d1
beglerbega	beglerbeg	k1gMnSc2
Behrama	Behram	k1gMnSc2
Paši	paša	k1gMnSc2
<g/>
,	,	kIx,
kterému	který	k3yQgMnSc3,k3yRgMnSc3,k3yIgMnSc3
cestu	cesta	k1gFnSc4
zablokovaly	zablokovat	k5eAaPmAgInP
rozsáhlé	rozsáhlý	k2eAgInPc1d1
úseky	úsek	k1gInPc1
zaplavené	zaplavený	k2eAgMnPc4d1
rozvodněným	rozvodněný	k2eAgInSc7d1
Dunajem	Dunaj	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celé	celá	k1gFnSc6
uskupení	uskupení	k1gNnSc1
nakonec	nakonec	k6eAd1
muselo	muset	k5eAaImAgNnS
počkat	počkat	k5eAaPmF
<g/>
,	,	kIx,
než	než	k8xS
kolem	kolem	k7c2
něj	on	k3xPp3gInSc2
projdou	projít	k5eAaPmIp3nP
středové	středový	k2eAgFnPc1d1
formace	formace	k1gFnPc1
<g/>
,	,	kIx,
pak	pak	k6eAd1
odbočilo	odbočit	k5eAaPmAgNnS
západním	západní	k2eAgInSc7d1
směrem	směr	k1gInSc7
a	a	k8xC
opět	opět	k6eAd1
se	se	k3xPyFc4
snažilo	snažit	k5eAaImAgNnS
dosáhnou	dosáhnout	k5eAaPmIp3nP
určených	určený	k2eAgFnPc2d1
pozic	pozice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Komplikace	komplikace	k1gFnSc1
při	při	k7c6
přesunu	přesun	k1gInSc6
se	se	k3xPyFc4
nevyhnuly	vyhnout	k5eNaPmAgFnP
ani	ani	k8xC
předvoji	předvoj	k1gInPc7
tvořenému	tvořený	k2eAgInSc3d1
rumelijskými	rumelijský	k2eAgFnPc7d1
jednotkami	jednotka	k1gFnPc7
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
1	#num#	k4
<g/>
]	]	kIx)
pod	pod	k7c7
velením	velení	k1gNnSc7
sultánova	sultánův	k2eAgMnSc2d1
přítele	přítel	k1gMnSc2
velkovezíra	velkovezír	k1gMnSc2
Ibrahima	Ibrahim	k1gMnSc2
Paši	paša	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
na	na	k7c6
výšinu	výšin	k1gInSc6
nad	nad	k7c7
Földvárem	Földvár	k1gMnSc7
dorazil	dorazit	k5eAaPmAgMnS
krátce	krátce	k6eAd1
před	před	k7c7
polednem	poledne	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgFnPc4d1
síly	síla	k1gFnPc4
Süleymanova	Süleymanův	k2eAgNnSc2d1
vojska	vojsko	k1gNnSc2
<g/>
,	,	kIx,
janičáři	janičár	k1gMnPc1
a	a	k8xC
sipahiové	sipahius	k1gMnPc1
<g/>
,	,	kIx,
dosáhly	dosáhnout	k5eAaPmAgFnP
stejné	stejný	k2eAgFnPc4d1
pozice	pozice	k1gFnPc4
asi	asi	k9
s	s	k7c7
dvouhodinovým	dvouhodinový	k2eAgNnSc7d1
zpožděním	zpoždění	k1gNnSc7
a	a	k8xC
Anatolci	Anatolec	k1gMnPc1
až	až	k9
kolem	kolem	k7c2
třetí	třetí	k4xOgFnSc2
hodiny	hodina	k1gFnSc2
odpolední	odpolední	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vzhledem	vzhledem	k7c3
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
si	se	k3xPyFc3
sultán	sultán	k1gMnSc1
Süleyman	Süleyman	k1gMnSc1
nebyl	být	k5eNaImAgMnS
jist	jist	k2eAgMnSc1d1
úmysly	úmysl	k1gInPc4
protivníka	protivník	k1gMnSc2
<g/>
,	,	kIx,
krátce	krátce	k6eAd1
po	po	k7c6
příjezdu	příjezd	k1gInSc6
na	na	k7c6
výšinu	výšin	k1gInSc6
svolal	svolat	k5eAaPmAgMnS
své	svůj	k3xOyFgMnPc4
vezíry	vezír	k1gMnPc4
k	k	k7c3
válečné	válečný	k2eAgFnSc3d1
poradě	porada	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
obavy	obava	k1gFnSc2
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
celá	celý	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
mohla	moct	k5eAaImAgFnS
být	být	k5eAaImF
obrácena	obrátit	k5eAaPmNgFnS
na	na	k7c4
ústup	ústup	k1gInSc4
přímým	přímý	k2eAgInSc7d1
nájezdem	nájezd	k1gInSc7
těžkého	těžký	k2eAgNnSc2d1
jezdectva	jezdectvo	k1gNnSc2
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
nakonec	nakonec	k6eAd1
rozhodnuto	rozhodnout	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
rumelijské	rumelijský	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
podpořené	podpořený	k2eAgFnPc1d1
čtyřmi	čtyři	k4xCgFnPc7
tisícovkami	tisícovka	k1gFnPc7
janičářů	janičář	k1gMnPc2
pomalu	pomalu	k6eAd1
sejdou	sejít	k5eAaPmIp3nP
ze	z	k7c2
svahu	svah	k1gInSc2
a	a	k8xC
začnou	začít	k5eAaPmIp3nP
s	s	k7c7
výstavbou	výstavba	k1gFnSc7
ležení	ležení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úkolem	úkol	k1gInSc7
Ibrahima	Ibrahim	k1gMnSc2
Paši	paša	k1gMnSc2
bylo	být	k5eAaImAgNnS
osobně	osobně	k6eAd1
prozkoumat	prozkoumat	k5eAaPmF
planinu	planina	k1gFnSc4
před	před	k7c7
vojskem	vojsko	k1gNnSc7
křesťanů	křesťan	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
asi	asi	k9
deset	deset	k4xCc1
tisíc	tisíc	k4xCgInSc1
vojáků	voják	k1gMnPc2
nepravidelného	pravidelný	k2eNgNnSc2d1
jezdectva	jezdectvo	k1gNnSc2
rozdělených	rozdělená	k1gFnPc2
do	do	k7c2
dvou	dva	k4xCgFnPc2
částí	část	k1gFnPc2
<g/>
,	,	kIx,
kterým	který	k3yQgNnSc7,k3yIgNnSc7,k3yRgNnSc7
veleli	velet	k5eAaImAgMnP
smedeverský	smedeverský	k2eAgInSc4d1
beglerbeg	beglerbeg	k1gInSc4
Ghazi	Ghaze	k1gFnSc4
Bali	Bal	k1gFnSc2
(	(	kIx(
<g/>
Bali	Bal	k1gFnSc2
Beg	beg	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
jeho	jeho	k3xOp3gMnSc1
spolurodák	spolurodák	k1gMnSc1
Ghazi	Ghaze	k1gFnSc4
Chosrev	Chosrva	k1gFnPc2
<g/>
,	,	kIx,
obdrželo	obdržet	k5eAaPmAgNnS
rozkaz	rozkaz	k1gInSc4
v	v	k7c6
širokém	široký	k2eAgInSc6d1
oblouku	oblouk	k1gInSc6
objet	objet	k5eAaPmF
pravé	pravý	k2eAgNnSc4d1
křídlo	křídlo	k1gNnSc4
protivníkova	protivníkův	k2eAgNnSc2d1
postavení	postavení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInSc7
úkolem	úkol	k1gInSc7
bylo	být	k5eAaImAgNnS
udeřit	udeřit	k5eAaPmF
do	do	k7c2
pravého	pravý	k2eAgInSc2d1
boku	bok	k1gInSc2
křesťanů	křesťan	k1gMnPc2
ve	v	k7c6
chvíli	chvíle	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
budou	být	k5eAaImBp3nP
ustupovat	ustupovat	k5eAaImF
<g/>
,	,	kIx,
atakovat	atakovat	k5eAaBmF
jejich	jejich	k3xOp3gInSc4
tábor	tábor	k1gInSc4
a	a	k8xC
zabránit	zabránit	k5eAaPmF
příchodu	příchod	k1gInSc3
případných	případný	k2eAgFnPc2d1
uherských	uherský	k2eAgFnPc2d1
posil	posila	k1gFnPc2
ze	z	k7c2
západu	západ	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Manévry	manévr	k1gInPc1
uherského	uherský	k2eAgNnSc2d1
vojska	vojsko	k1gNnSc2
</s>
<s>
Mapa	mapa	k1gFnSc1
bojiště	bojiště	k1gNnSc2
a	a	k8xC
bojových	bojový	k2eAgFnPc2d1
operací	operace	k1gFnPc2
</s>
<s>
Duel	duel	k1gInSc1
před	před	k7c7
bitvou	bitva	k1gFnSc7
(	(	kIx(
<g/>
turecká	turecký	k2eAgFnSc1d1
miniatura	miniatura	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Vojsko	vojsko	k1gNnSc1
Ludvíka	Ludvík	k1gMnSc2
Jagellonského	jagellonský	k2eAgMnSc2d1
se	se	k3xPyFc4
začalo	začít	k5eAaPmAgNnS
řadit	řadit	k5eAaImF
k	k	k7c3
bitvě	bitva	k1gFnSc3
již	již	k6eAd1
brzy	brzy	k6eAd1
po	po	k7c6
rozbřesku	rozbřesk	k1gInSc6
a	a	k8xC
ranní	ranní	k2eAgFnSc3d1
modlitbě	modlitba	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krátce	krátce	k6eAd1
na	na	k7c4
to	ten	k3xDgNnSc4
mladý	mladý	k2eAgMnSc1d1
král	král	k1gMnSc1
spolu	spolu	k6eAd1
s	s	k7c7
palatinem	palatin	k1gMnSc7
(	(	kIx(
<g/>
uherským	uherský	k2eAgMnSc7d1
místokrálem	místokrál	k1gMnSc7
<g/>
)	)	kIx)
Štěpánem	Štěpán	k1gMnSc7
Báthorym	Báthorym	k1gInSc1
provedl	provést	k5eAaPmAgInS
přehlídku	přehlídka	k1gFnSc4
vojáků	voják	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
mladému	mladý	k2eAgMnSc3d1
panovníkovi	panovník	k1gMnSc3
nadšeně	nadšeně	k6eAd1
složili	složit	k5eAaPmAgMnP
slavnostní	slavnostní	k2eAgInSc4d1
slib	slib	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
budou	být	k5eAaImBp3nP
odhodláni	odhodlán	k2eAgMnPc1d1
podstoupit	podstoupit	k5eAaPmF
krajní	krajní	k2eAgNnSc4d1
nebezpečí	nebezpečí	k1gNnSc4
i	i	k8xC
smrt	smrt	k1gFnSc4
pro	pro	k7c4
vlast	vlast	k1gFnSc4
<g/>
,	,	kIx,
víru	víra	k1gFnSc4
a	a	k8xC
své	svůj	k3xOyFgFnPc4
rodiny	rodina	k1gFnPc4
a	a	k8xC
svobodu	svoboda	k1gFnSc4
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
Tento	tento	k3xDgInSc1
spontánní	spontánní	k2eAgInSc1d1
projev	projev	k1gInSc1
odhodlání	odhodlání	k1gNnSc2
vyjadřující	vyjadřující	k2eAgFnSc4d1
mimořádnou	mimořádný	k2eAgFnSc4d1
obětavost	obětavost	k1gFnSc4
a	a	k8xC
ochotu	ochota	k1gFnSc4
bojovat	bojovat	k5eAaImF
až	až	k9
do	do	k7c2
krajnosti	krajnost	k1gFnSc2
zcela	zcela	k6eAd1
korespondoval	korespondovat	k5eAaImAgMnS
s	s	k7c7
plánem	plán	k1gInSc7
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
vypracoval	vypracovat	k5eAaPmAgMnS
vrchní	vrchní	k2eAgMnSc1d1
hejtman	hejtman	k1gMnSc1
vojska	vojsko	k1gNnSc2
Pál	Pál	k1gMnSc1
Tomory	Tomora	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
si	se	k3xPyFc3
dobře	dobře	k6eAd1
uvědomoval	uvědomovat	k5eAaImAgMnS
slabiny	slabina	k1gFnPc4
shromážděných	shromážděný	k2eAgMnPc2d1
sborů	sbor	k1gInPc2
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
zůstával	zůstávat	k5eAaImAgMnS
věrný	věrný	k2eAgMnSc1d1
názoru	názor	k1gInSc3
<g/>
,	,	kIx,
že	že	k8xS
<g />
.	.	kIx.
</s>
<s hack="1">
povinností	povinnost	k1gFnPc2
křesťana	křesťan	k1gMnSc4
je	být	k5eAaImIp3nS
bojovat	bojovat	k5eAaImF
proti	proti	k7c3
pohanům	pohan	k1gMnPc3
bez	bez	k7c2
ohledu	ohled	k1gInSc2
na	na	k7c4
okolnosti	okolnost	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Vzhledem	vzhledem	k7c3
k	k	k7c3
početnímu	početní	k2eAgNnSc3d1
přečíslení	přečíslení	k1gNnSc3
protivníka	protivník	k1gMnSc2
mu	on	k3xPp3gMnSc3
ani	ani	k9
nezbylo	zbýt	k5eNaPmAgNnS
než	než	k8xS
počítat	počítat	k5eAaImF
s	s	k7c7
naprostým	naprostý	k2eAgNnSc7d1
odhodláním	odhodlání	k1gNnSc7
svých	svůj	k3xOyFgMnPc2
mužů	muž	k1gMnPc2
a	a	k8xC
vše	všechen	k3xTgNnSc1
nasvědčuje	nasvědčovat	k5eAaImIp3nS
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
připravený	připravený	k2eAgInSc1d1
záměr	záměr	k1gInSc1
ve	v	k7c6
značné	značný	k2eAgFnSc6d1
míře	míra	k1gFnSc6
spoléhal	spoléhat	k5eAaImAgInS
na	na	k7c4
šťastnou	šťastný	k2eAgFnSc4d1
náhodu	náhoda	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Před	před	k7c7
zahájením	zahájení	k1gNnSc7
střetnutí	střetnutí	k1gNnSc2
bylo	být	k5eAaImAgNnS
vojsko	vojsko	k1gNnSc1
českého	český	k2eAgMnSc2d1
a	a	k8xC
uherského	uherský	k2eAgMnSc2d1
panovníka	panovník	k1gMnSc2
rozděleno	rozdělit	k5eAaPmNgNnS
do	do	k7c2
dvou	dva	k4xCgFnPc2
sledů	sled	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pravé	pravá	k1gFnSc6
křídlo	křídlo	k1gNnSc4
první	první	k4xOgFnSc2
linie	linie	k1gFnSc2
tvořila	tvořit	k5eAaImAgFnS
jízda	jízda	k1gFnSc1
pod	pod	k7c7
velením	velení	k1gNnSc7
chorvatského	chorvatský	k2eAgMnSc2d1
bána	bán	k1gMnSc2
Františka	František	k1gMnSc2
Batthyányho	Batthyány	k1gMnSc2
a	a	k8xC
Jana	Jan	k1gMnSc2
Tahy	tah	k1gInPc7
hraběte	hrabě	k1gMnSc2
z	z	k7c2
Tahváru	Tahvár	k1gInSc2
a	a	k8xC
Tarkö	Tarkö	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
středu	střed	k1gInSc6
formace	formace	k1gFnSc2
bylo	být	k5eAaImAgNnS
umístěno	umístit	k5eAaPmNgNnS
asi	asi	k9
10	#num#	k4
000	#num#	k4
příslušníků	příslušník	k1gMnPc2
pěchoty	pěchota	k1gFnSc2
<g/>
;	;	kIx,
velením	velení	k1gNnSc7
jezdectva	jezdectvo	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
tvořilo	tvořit	k5eAaImAgNnS
křídlo	křídlo	k1gNnSc4
levé	levá	k1gFnSc3
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
pověřen	pověřen	k2eAgMnSc1d1
správce	správce	k1gMnSc1
temešské	temešský	k2eAgFnSc2d1
župy	župa	k1gFnSc2
Petr	Petr	k1gMnSc1
Peréni	Peréň	k1gMnSc6
ze	z	k7c2
Siklósu	Siklós	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uskupení	uskupení	k1gNnPc4
vojáků	voják	k1gMnPc2
v	v	k7c6
druhém	druhý	k4xOgInSc6
sledu	sled	k1gInSc6
se	se	k3xPyFc4
skládalo	skládat	k5eAaImAgNnS
z	z	k7c2
velmi	velmi	k6eAd1
dobře	dobře	k6eAd1
vyzbrojené	vyzbrojený	k2eAgFnSc2d1
těžké	těžký	k2eAgFnSc2d1
jízdy	jízda	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
jejíchž	jejíž	k3xOyRp3gFnPc6
řadách	řada	k1gFnPc6
se	se	k3xPyFc4
nacházel	nacházet	k5eAaImAgMnS
i	i	k9
Ludvík	Ludvík	k1gMnSc1
Jagellonský	jagellonský	k2eAgMnSc1d1
se	se	k3xPyFc4
svým	svůj	k3xOyFgInSc7
ozbrojeným	ozbrojený	k2eAgInSc7d1
doprovodem	doprovod	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Asi	asi	k9
80	#num#	k4
nebo	nebo	k8xC
85	#num#	k4
děl	dělo	k1gNnPc2
křesťanského	křesťanský	k2eAgNnSc2d1
vojska	vojsko	k1gNnSc2
pod	pod	k7c7
velením	velení	k1gNnSc7
Jana	Jan	k1gMnSc2
hraběte	hrabě	k1gMnSc2
z	z	k7c2
Hardeggu	Hardegg	k1gInSc2
bylo	být	k5eAaImAgNnS
dislokováno	dislokován	k2eAgNnSc1d1
před	před	k7c7
čelní	čelní	k2eAgFnSc7d1
řadou	řada	k1gFnSc7
pěchoty	pěchota	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
Celé	celý	k2eAgInPc4d1
toto	tento	k3xDgNnSc1
uskupení	uskupení	k1gNnSc1
se	se	k3xPyFc4
během	během	k7c2
dopoledne	dopoledne	k1gNnSc2
přesunulo	přesunout	k5eAaPmAgNnS
zhruba	zhruba	k6eAd1
šest	šest	k4xCc1
kilometrů	kilometr	k1gInPc2
směrem	směr	k1gInSc7
na	na	k7c4
jih	jih	k1gInSc4
a	a	k8xC
zastavilo	zastavit	k5eAaPmAgNnS
se	se	k3xPyFc4
teprve	teprve	k6eAd1
po	po	k7c6
překonání	překonání	k1gNnSc6
strmých	strmý	k2eAgInPc2d1
břehů	břeh	k1gInPc2
říčky	říčka	k1gFnSc2
Borzy	Borza	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
po	po	k7c6
uspořádání	uspořádání	k1gNnSc6
narušených	narušený	k2eAgFnPc2d1
řad	řada	k1gFnPc2
vyčkalo	vyčkat	k5eAaPmAgNnS
příchodu	příchod	k1gInSc3
nepřítele	nepřítel	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jelikož	jelikož	k8xS
se	se	k3xPyFc4
vrchní	vrchní	k2eAgMnPc1d1
velitelé	velitel	k1gMnPc1
obávali	obávat	k5eAaImAgMnP
obchvatu	obchvat	k1gInSc2
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
celá	celý	k2eAgFnSc1d1
přední	přední	k2eAgFnSc1d1
linie	linie	k1gFnSc1
roztažena	roztažen	k2eAgFnSc1d1
do	do	k7c2
délky	délka	k1gFnSc2
čtyř	čtyři	k4xCgInPc2
kilometrů	kilometr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oba	dva	k4xCgInPc1
sledy	sled	k1gInPc1
<g/>
,	,	kIx,
podle	podle	k7c2
svědectví	svědectví	k1gNnSc2
jednoho	jeden	k4xCgMnSc2
z	z	k7c2
účastníků	účastník	k1gMnPc2
bitvy	bitva	k1gFnSc2
<g/>
,	,	kIx,
dělila	dělit	k5eAaImAgFnS
vzdálenost	vzdálenost	k1gFnSc4
<g/>
,	,	kIx,
sotva	sotva	k8xS
co	co	k9
by	by	k9
kamenem	kámen	k1gInSc7
dohodil	dohodit	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hlavní	hlavní	k2eAgFnPc1d1
vojenské	vojenský	k2eAgFnPc1d1
síly	síla	k1gFnPc1
osmanského	osmanský	k2eAgMnSc2d1
sultána	sultán	k1gMnSc2
se	se	k3xPyFc4
začaly	začít	k5eAaPmAgInP
na	na	k7c6
hřebenu	hřeben	k1gInSc6
nad	nad	k7c7
Földvárem	Földvár	k1gMnSc7
objevovat	objevovat	k5eAaImF
v	v	k7c6
prvních	první	k4xOgFnPc6
odpoledních	odpolední	k2eAgFnPc6d1
hodinách	hodina	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoliv	ačkoliv	k8xS
se	se	k3xPyFc4
Pál	Pál	k1gMnSc1
Tomory	Tomora	k1gFnSc2
pokoušel	pokoušet	k5eAaImAgMnS
volbou	volba	k1gFnSc7
postavení	postavení	k1gNnSc2
získat	získat	k5eAaPmF
nad	nad	k7c7
protivníkem	protivník	k1gMnSc7
východu	východ	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
této	tento	k3xDgFnSc6
chvíli	chvíle	k1gFnSc6
se	se	k3xPyFc4
projevila	projevit	k5eAaPmAgFnS
slabina	slabina	k1gFnSc1
jeho	jeho	k3xOp3gFnSc2
pozice	pozice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reálné	reálný	k2eAgInPc1d1
předpoklady	předpoklad	k1gInPc1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
řady	řada	k1gFnPc1
útočících	útočící	k2eAgMnPc2d1
nepřátel	nepřítel	k1gMnPc2
pochodem	pochod	k1gInSc7
přes	přes	k7c4
těžký	těžký	k2eAgInSc4d1
terén	terén	k1gInSc4
značně	značně	k6eAd1
naruší	narušit	k5eAaPmIp3nS
<g/>
,	,	kIx,
znevažovala	znevažovat	k5eAaImAgFnS
skutečnost	skutečnost	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
osmanské	osmanský	k2eAgNnSc1d1
vojsko	vojsko	k1gNnSc1
na	na	k7c6
výšině	výšina	k1gFnSc6
mělo	mít	k5eAaImAgNnS
o	o	k7c6
rozmístění	rozmístění	k1gNnSc6
křesťanských	křesťanský	k2eAgInPc2d1
oddílů	oddíl	k1gInPc2
dokonalý	dokonalý	k2eAgInSc4d1
přehled	přehled	k1gInSc4
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
uherskému	uherský	k2eAgNnSc3d1
velení	velení	k1gNnSc3
se	se	k3xPyFc4
o	o	k7c4
dění	dění	k1gNnSc4
za	za	k7c7
jižním	jižní	k2eAgInSc7d1
zenitem	zenit	k1gInSc7
nedostávalo	dostávat	k5eNaImAgNnS
žádných	žádný	k3yNgFnPc2
informací	informace	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Pokud	pokud	k8xS
jde	jít	k5eAaImIp3nS
o	o	k7c4
konkrétní	konkrétní	k2eAgFnSc4d1
podobu	podoba	k1gFnSc4
dispozic	dispozice	k1gFnPc2
pro	pro	k7c4
bitvu	bitva	k1gFnSc4
<g/>
,	,	kIx,
záměrem	záměr	k1gInSc7
kaločského	kaločský	k2eAgMnSc4d1
arcibiskupa	arcibiskup	k1gMnSc4
bylo	být	k5eAaImAgNnS
vylákat	vylákat	k5eAaPmF
osmanské	osmanský	k2eAgFnPc4d1
předsunuté	předsunutý	k2eAgFnPc4d1
jednotky	jednotka	k1gFnPc4
k	k	k7c3
neuváženému	uvážený	k2eNgInSc3d1
výpadu	výpad	k1gInSc3
a	a	k8xC
posléze	posléze	k6eAd1
je	být	k5eAaImIp3nS
rozdrtit	rozdrtit	k5eAaPmF
jedním	jeden	k4xCgInSc7
rozhodným	rozhodný	k2eAgInSc7d1
úderem	úder	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spoléhal	spoléhat	k5eAaImAgMnS
se	se	k3xPyFc4
zejména	zejména	k9
na	na	k7c4
okolnost	okolnost	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
prchající	prchající	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
způsobí	způsobit	k5eAaPmIp3nP
paniku	panika	k1gFnSc4
v	v	k7c6
celém	celý	k2eAgNnSc6d1
protivníkově	protivníkův	k2eAgNnSc6d1
vojsku	vojsko	k1gNnSc6
<g/>
,	,	kIx,
avšak	avšak	k8xC
jednotky	jednotka	k1gFnPc1
z	z	k7c2
Rumelie	Rumelie	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
se	se	k3xPyFc4
na	na	k7c6
bojišti	bojiště	k1gNnSc6
objevily	objevit	k5eAaPmAgFnP
jako	jako	k9
první	první	k4xOgInSc4
<g/>
,	,	kIx,
se	se	k3xPyFc4
k	k	k7c3
útoku	útok	k1gInSc3
strhnout	strhnout	k5eAaPmF
nedaly	dát	k5eNaPmAgInP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pálu	Pála	k1gMnSc4
Tomorymu	Tomorym	k1gInSc2
proto	proto	k8xC
nezbylo	zbýt	k5eNaPmAgNnS
než	než	k8xS
vyčkat	vyčkat	k5eAaPmF
<g/>
,	,	kIx,
dokud	dokud	k8xS
na	na	k7c4
planinu	planina	k1gFnSc4
před	před	k7c7
ním	on	k3xPp3gInSc7
nesestoupí	sestoupit	k5eNaPmIp3nS
uskupení	uskupení	k1gNnSc1
právě	právě	k9
tolika	tolik	k4yIc2,k4xDc2
nepřátel	nepřítel	k1gMnPc2
<g/>
,	,	kIx,
jež	jenž	k3xRgFnPc1
by	by	kYmCp3nP
jeho	jeho	k3xOp3gInPc1
oddíly	oddíl	k1gInPc1
dokázaly	dokázat	k5eAaPmAgInP
spolehlivě	spolehlivě	k6eAd1
rozprášit	rozprášit	k5eAaPmF
<g/>
,	,	kIx,
aniž	aniž	k8xC,k8xS
by	by	kYmCp3nS
Süleyman	Süleyman	k1gMnSc1
I.	I.	kA
dokázal	dokázat	k5eAaPmAgMnS
uplatnit	uplatnit	k5eAaPmF
početní	početní	k2eAgFnSc4d1
převahu	převaha	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vzhledem	vzhledem	k7c3
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
ani	ani	k8xC
po	po	k7c6
poledni	poledne	k1gNnSc6
nezačali	začít	k5eNaPmAgMnP
Osmané	Osman	k1gMnPc1
vyvíjet	vyvíjet	k5eAaImF
výraznější	výrazný	k2eAgFnSc4d2
aktivitu	aktivita	k1gFnSc4
<g/>
,	,	kIx,
rozhodl	rozhodnout	k5eAaPmAgMnS
se	se	k3xPyFc4
znuděný	znuděný	k2eAgMnSc1d1
Ludvík	Ludvík	k1gMnSc1
Jagellonský	jagellonský	k2eAgMnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
i	i	k9
s	s	k7c7
doprovodem	doprovod	k1gInSc7
vrátí	vrátit	k5eAaPmIp3nS
zpět	zpět	k6eAd1
do	do	k7c2
tábora	tábor	k1gInSc2
u	u	k7c2
Moháče	Moháč	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tu	ten	k3xDgFnSc4
chvíli	chvíle	k1gFnSc4
se	se	k3xPyFc4
daleko	daleko	k6eAd1
na	na	k7c6
pravém	pravý	k2eAgNnSc6d1
křídle	křídlo	k1gNnSc6
uherského	uherský	k2eAgNnSc2d1
vojska	vojsko	k1gNnSc2
objevilo	objevit	k5eAaPmAgNnS
poměrně	poměrně	k6eAd1
velké	velký	k2eAgNnSc1d1
uskupení	uskupení	k1gNnSc1
protivníkovy	protivníkův	k2eAgFnSc2d1
jízdy	jízda	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
zanedlouho	zanedlouho	k6eAd1
ztratilo	ztratit	k5eAaPmAgNnS
směrem	směr	k1gInSc7
na	na	k7c4
severozápad	severozápad	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Arcibiskup	arcibiskup	k1gMnSc1
Pál	Pál	k1gMnSc1
Tomory	Tomor	k1gInPc4
došel	dojít	k5eAaPmAgMnS
k	k	k7c3
závěru	závěr	k1gInSc3
<g/>
,	,	kIx,
že	že	k8xS
jde	jít	k5eAaImIp3nS
o	o	k7c4
další	další	k2eAgFnPc4d1
z	z	k7c2
klamných	klamný	k2eAgInPc2d1
výpadů	výpad	k1gInPc2
<g/>
,	,	kIx,
jakých	jaký	k3yQgInPc2,k3yIgInPc2,k3yRgInPc2
vojsko	vojsko	k1gNnSc1
zažilo	zažít	k5eAaPmAgNnS
v	v	k7c6
předchozích	předchozí	k2eAgInPc6d1
dnech	den	k1gInPc6
několik	několik	k4yIc4
<g/>
,	,	kIx,
vyslal	vyslat	k5eAaPmAgMnS
proto	proto	k6eAd1
600	#num#	k4
husarů	husar	k1gMnPc2
a	a	k8xC
několik	několik	k4yIc1
desítek	desítka	k1gFnPc2
lehkých	lehký	k2eAgMnPc2d1
jezdců	jezdec	k1gMnPc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
záměr	záměr	k1gInSc1
protivníkova	protivníkův	k2eAgInSc2d1
taktického	taktický	k2eAgInSc2d1
manévru	manévr	k1gInSc2
pokusili	pokusit	k5eAaPmAgMnP
odhalit	odhalit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velení	velení	k1gNnSc1
nad	nad	k7c7
oddílem	oddíl	k1gInSc7
bylo	být	k5eAaImAgNnS
svěřeno	svěřit	k5eAaPmNgNnS
Kašparu	Kašpar	k1gMnSc3
Ráskayovi	Ráskaya	k1gMnSc3
<g/>
,	,	kIx,
Valentinu	Valentin	k1gMnSc3
Törokovi	Törok	k1gMnSc3
a	a	k8xC
Janu	Jan	k1gMnSc3
Kállayovi	Kállaya	k1gMnSc3
<g/>
,	,	kIx,
šlechticům	šlechtic	k1gMnPc3
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
byli	být	k5eAaImAgMnP
již	již	k6eAd1
dříve	dříve	k6eAd2
pověření	pověření	k1gNnSc4
přímou	přímý	k2eAgFnSc7d1
ochranou	ochrana	k1gFnSc7
krále	král	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
2	#num#	k4
<g/>
]	]	kIx)
Ačkoliv	ačkoliv	k8xS
nebylo	být	k5eNaImAgNnS
vyloučeno	vyloučit	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
cílem	cíl	k1gInSc7
turecké	turecký	k2eAgFnSc2d1
aktivity	aktivita	k1gFnSc2
je	být	k5eAaImIp3nS
uherské	uherský	k2eAgNnSc1d1
ležení	ležení	k1gNnSc1
<g/>
,	,	kIx,
král	král	k1gMnSc1
se	se	k3xPyFc4
nevzdal	vzdát	k5eNaPmAgMnS
svého	svůj	k3xOyFgInSc2
původního	původní	k2eAgInSc2d1
záměru	záměr	k1gInSc2
a	a	k8xC
zamířil	zamířit	k5eAaPmAgMnS
k	k	k7c3
Moháči	Moháč	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
o	o	k7c6
odjezdu	odjezd	k1gInSc6
Ludvíka	Ludvík	k1gMnSc2
Jagellonského	jagellonský	k2eAgMnSc2d1
dozvěděl	dozvědět	k5eAaPmAgMnS
Pál	Pál	k1gFnPc2
Tomory	Tomora	k1gFnSc2
<g/>
,	,	kIx,
okamžitě	okamžitě	k6eAd1
spolu	spolu	k6eAd1
s	s	k7c7
Györgem	Györg	k1gInSc7
Zápolským	Zápolský	k2eAgInSc7d1
vyrazil	vyrazit	k5eAaPmAgMnS
v	v	k7c6
jeho	jeho	k3xOp3gFnPc6
stopách	stopa	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tu	ten	k3xDgFnSc4
chvíli	chvíle	k1gFnSc4
začaly	začít	k5eAaPmAgFnP
z	z	k7c2
výšiny	výšina	k1gFnSc2
nad	nad	k7c7
Földvárem	Földvár	k1gInSc7
sestupovat	sestupovat	k5eAaImF
formace	formace	k1gFnSc1
osmanské	osmanský	k2eAgFnSc2d1
jízdy	jízda	k1gFnSc2
a	a	k8xC
pěchoty	pěchota	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
Velitelé	velitel	k1gMnPc1
křesťanského	křesťanský	k2eAgNnSc2d1
vojska	vojsko	k1gNnSc2
se	se	k3xPyFc4
v	v	k7c6
první	první	k4xOgFnSc6
chvíli	chvíle	k1gFnSc6
domnívali	domnívat	k5eAaImAgMnP
<g/>
,	,	kIx,
že	že	k8xS
zanedlouho	zanedlouho	k6eAd1
budou	být	k5eAaImBp3nP
nuceni	nutit	k5eAaImNgMnP
čelit	čelit	k5eAaImF
hlavnímu	hlavní	k2eAgInSc3d1
úderu	úder	k1gInSc3
<g/>
,	,	kIx,
protivník	protivník	k1gMnSc1
však	však	k9
namísto	namísto	k7c2
šikování	šikování	k1gNnSc2
k	k	k7c3
bitvě	bitva	k1gFnSc3
začal	začít	k5eAaPmAgInS
ihned	ihned	k6eAd1
po	po	k7c6
příchodu	příchod	k1gInSc6
na	na	k7c4
planinu	planina	k1gFnSc4
budovat	budovat	k5eAaImF
opevněný	opevněný	k2eAgInSc4d1
tábor	tábor	k1gInSc4
a	a	k8xC
rozmisťovat	rozmisťovat	k5eAaImF
svá	svůj	k3xOyFgNnPc4
děla	dělo	k1gNnPc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
jejich	jejich	k3xOp3gFnPc4
obsluhy	obsluha	k1gFnPc4
vzájemně	vzájemně	k6eAd1
pospojovaly	pospojovat	k5eAaPmAgInP
řetězy	řetěz	k1gInPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mezitím	mezitím	k6eAd1
Pál	Pál	k1gFnSc1
Tomory	Tomora	k1gFnSc2
a	a	k8xC
Györg	Györg	k1gInSc1
Zápolský	Zápolský	k2eAgInSc1d1
pokračovali	pokračovat	k5eAaImAgMnP
v	v	k7c6
cestě	cesta	k1gFnSc6
s	s	k7c7
královskou	královský	k2eAgFnSc7d1
družinou	družina	k1gFnSc7
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
zastihli	zastihnout	k5eAaPmAgMnP
odpočívat	odpočívat	k5eAaImF
na	na	k7c6
stanovišti	stanoviště	k1gNnSc6
zhruba	zhruba	k6eAd1
dva	dva	k4xCgInPc4
kilometry	kilometr	k1gInPc4
od	od	k7c2
výšiny	výšina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kaločský	Kaločský	k2eAgInSc1d1
arcibiskup	arcibiskup	k1gMnSc1
se	se	k3xPyFc4
neprodleně	prodleně	k6eNd1
panovníka	panovník	k1gMnSc2
pokusili	pokusit	k5eAaPmAgMnP
přemluvit	přemluvit	k5eAaPmF
o	o	k7c6
nutnosti	nutnost	k1gFnSc6
přejít	přejít	k5eAaPmF
do	do	k7c2
ofenzivy	ofenziva	k1gFnSc2
<g/>
,	,	kIx,
dokud	dokud	k8xS
protivník	protivník	k1gMnSc1
neshromáždil	shromáždit	k5eNaPmAgMnS
všechny	všechen	k3xTgMnPc4
své	svůj	k3xOyFgMnPc4
vojáky	voják	k1gMnPc4
u	u	k7c2
Földváru	Földvár	k1gInSc2
<g/>
,	,	kIx,
avšak	avšak	k8xC
Ludvík	Ludvík	k1gMnSc1
Jagellonský	jagellonský	k2eAgMnSc1d1
zprvu	zprvu	k6eAd1
váhal	váhat	k5eAaImAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zanedlouho	zanedlouho	k6eAd1
ovšem	ovšem	k9
ke	k	k7c3
všem	všecek	k3xTgFnPc3
třem	tři	k4xCgFnPc3
velitelům	velitel	k1gMnPc3
dorazila	dorazit	k5eAaPmAgFnS
zpráva	zpráva	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
Osmané	Osman	k1gMnPc1
k	k	k7c3
útoku	útok	k1gInSc3
nepřipravují	připravovat	k5eNaImIp3nP
a	a	k8xC
budují	budovat	k5eAaImIp3nP
polní	polní	k2eAgNnSc4d1
opevnění	opevnění	k1gNnSc4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
krále	král	k1gMnSc4
přesvědčilo	přesvědčit	k5eAaPmAgNnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
uznal	uznat	k5eAaPmAgMnS
Tomoryho	Tomory	k1gMnSc4
argumenty	argument	k1gInPc4
a	a	k8xC
dal	dát	k5eAaPmAgInS
povel	povel	k1gInSc1
vyrazit	vyrazit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Arcibiskup	arcibiskup	k1gMnSc1
proto	proto	k8xC
neprodleně	prodleně	k6eNd1
zamířil	zamířit	k5eAaPmAgMnS
na	na	k7c4
své	svůj	k3xOyFgNnSc4
místo	místo	k1gNnSc4
v	v	k7c6
čele	čelo	k1gNnSc6
vojska	vojsko	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
zjistil	zjistit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
nejlepší	dobrý	k2eAgFnSc4d3
chvíli	chvíle	k1gFnSc4
k	k	k7c3
útoku	útok	k1gInSc3
patrně	patrně	k6eAd1
promarnil	promarnit	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
nížině	nížina	k1gFnSc6
se	se	k3xPyFc4
již	již	k6eAd1
shromáždily	shromáždit	k5eAaPmAgFnP
značné	značný	k2eAgFnPc1d1
protivníkovy	protivníkův	k2eAgFnPc1d1
síly	síla	k1gFnPc1
a	a	k8xC
další	další	k2eAgMnPc1d1
Süleymanovi	Süleymanův	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
přicházeli	přicházet	k5eAaImAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
krátce	krátce	k6eAd1
před	před	k7c7
čtvrtou	čtvrtý	k4xOgFnSc7
hodinou	hodina	k1gFnSc7
odpolední	odpolední	k1gNnSc2
poprvé	poprvé	k6eAd1
vystřelila	vystřelit	k5eAaPmAgNnP
uherská	uherský	k2eAgNnPc1d1
děla	dělo	k1gNnPc1
a	a	k8xC
vojáci	voják	k1gMnPc1
začali	začít	k5eAaPmAgMnP
po	po	k7c4
třikrát	třikrát	k6eAd1
vzývat	vzývat	k5eAaImF
Kristovo	Kristův	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
poručil	poručit	k5eAaPmAgMnS
vykonat	vykonat	k5eAaPmF
před	před	k7c7
zahájením	zahájení	k1gNnSc7
útoku	útok	k1gInSc2
král	král	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bitva	bitva	k1gFnSc1
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Moháče	Moháč	k1gInSc2
na	na	k7c6
turecké	turecký	k2eAgFnSc6d1
miniatuře	miniatura	k1gFnSc6
</s>
<s>
Jako	jako	k9
první	první	k4xOgInSc1
sled	sled	k1gInSc1
Turků	turek	k1gInPc2
zaútočily	zaútočit	k5eAaPmAgInP
asi	asi	k9
hodinu	hodina	k1gFnSc4
po	po	k7c6
poledni	poledne	k1gNnSc6
nepravidelné	pravidelný	k2eNgFnSc2d1
rumélijské	rumélijský	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uherské	uherský	k2eAgFnSc2d1
pravé	pravá	k1gFnSc2
křídlo	křídlo	k1gNnSc1
útok	útok	k1gInSc4
odvrátilo	odvrátit	k5eAaPmAgNnS
a	a	k8xC
vydalo	vydat	k5eAaPmAgNnS
se	se	k3xPyFc4
do	do	k7c2
protiútoku	protiútok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rumélijci	Rumélijce	k1gMnPc1
se	se	k3xPyFc4
dali	dát	k5eAaPmAgMnP
na	na	k7c4
útěk	útěk	k1gInSc4
a	a	k8xC
zbytek	zbytek	k1gInSc4
osmanské	osmanský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
ustoupil	ustoupit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uhři	Uhři	k1gNnPc1
a	a	k8xC
další	další	k2eAgMnPc1d1
křesťané	křesťan	k1gMnPc1
vyrazili	vyrazit	k5eAaPmAgMnP
za	za	k7c7
nimi	on	k3xPp3gMnPc7
v	v	k7c6
porušené	porušený	k2eAgFnSc6d1
sestavě	sestava	k1gFnSc6
-	-	kIx~
jízda	jízda	k1gFnSc1
se	se	k3xPyFc4
vzdálila	vzdálit	k5eAaPmAgFnS
pěchotě	pěchota	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
jejich	jejich	k3xOp3gNnSc4
pravé	pravý	k2eAgNnSc4d1
křídlo	křídlo	k1gNnSc4
v	v	k7c6
těch	ten	k3xDgFnPc6
chvílích	chvíle	k1gFnPc6
zaútočila	zaútočit	k5eAaPmAgFnS
lehká	lehký	k2eAgFnSc1d1
islámská	islámský	k2eAgFnSc1d1
jízda	jízda	k1gFnSc1
a	a	k8xC
uherský	uherský	k2eAgInSc1d1
protiútok	protiútok	k1gInSc1
byl	být	k5eAaImAgInS
zastaven	zastavit	k5eAaPmNgInS
jednotkami	jednotka	k1gFnPc7
druhého	druhý	k4xOgInSc2
sledu	sled	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bojovaly	bojovat	k5eAaImAgInP
už	už	k6eAd1
i	i	k9
uherské	uherský	k2eAgFnPc1d1
jízdní	jízdní	k2eAgFnPc1d1
zálohy	záloha	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tuto	tento	k3xDgFnSc4
chvíli	chvíle	k1gFnSc4
probíhal	probíhat	k5eAaImAgInS
tvrdý	tvrdý	k2eAgInSc1d1
nerozhodný	rozhodný	k2eNgInSc1d1
boj	boj	k1gInSc1
a	a	k8xC
také	také	k9
vyrazil	vyrazit	k5eAaPmAgMnS
třetí	třetí	k4xOgInSc4
sled	sled	k1gInSc4
složený	složený	k2eAgInSc4d1
ze	z	k7c2
Sulejmanovy	Sulejmanův	k2eAgFnSc2d1
osobní	osobní	k2eAgFnSc2d1
gardy	garda	k1gFnSc2
-	-	kIx~
zkušených	zkušený	k2eAgMnPc2d1
a	a	k8xC
kvalitních	kvalitní	k2eAgMnPc2d1
janičářů	janičář	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bitva	bitva	k1gFnSc1
probíhala	probíhat	k5eAaImAgFnS
už	už	k6eAd1
asi	asi	k9
hodinu	hodina	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příchod	příchod	k1gInSc1
janičárů	janičár	k1gMnPc2
znamenal	znamenat	k5eAaImAgInS
obrat	obrat	k1gInSc1
bitvy	bitva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uherské	uherský	k2eAgInPc1d1
oddíly	oddíl	k1gInPc1
se	se	k3xPyFc4
začaly	začít	k5eAaPmAgInP
rozpadat	rozpadat	k5eAaPmF,k5eAaImF
a	a	k8xC
z	z	k7c2
nejvytíženějšího	vytížený	k2eAgNnSc2d3
pravého	pravý	k2eAgNnSc2d1
křídla	křídlo	k1gNnSc2
křesťanské	křesťanský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
začali	začít	k5eAaPmAgMnP
utíkat	utíkat	k5eAaImF
první	první	k4xOgMnPc1
vojáci	voják	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
bojiště	bojiště	k1gNnSc4
navíc	navíc	k6eAd1
ještě	ještě	k6eAd1
dorazila	dorazit	k5eAaPmAgFnS
skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
uherskou	uherský	k2eAgFnSc4d1
armádu	armáda	k1gFnSc4
obešla	obejít	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uherské	uherský	k2eAgNnSc1d1
vojsko	vojsko	k1gNnSc1
bylo	být	k5eAaImAgNnS
poraženo	porazit	k5eAaPmNgNnS
<g/>
,	,	kIx,
rozehnáno	rozehnat	k5eAaPmNgNnS
<g/>
,	,	kIx,
bojovaly	bojovat	k5eAaImAgFnP
už	už	k6eAd1
jen	jen	k9
jeho	jeho	k3xOp3gInPc1
zbytky	zbytek	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Král	Král	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
při	při	k7c6
útěku	útěk	k1gInSc6
utonul	utonout	k5eAaPmAgMnS
v	v	k7c6
bažinách	bažina	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
obou	dva	k4xCgFnPc6
stranách	strana	k1gFnPc6
byly	být	k5eAaImAgFnP
ztráty	ztráta	k1gFnPc1
skoro	skoro	k6eAd1
stejné	stejný	k2eAgInPc4d1
-	-	kIx~
16	#num#	k4
000	#num#	k4
mužů	muž	k1gMnPc2
<g/>
,	,	kIx,
pro	pro	k7c4
uherskou	uherský	k2eAgFnSc4d1
armádu	armáda	k1gFnSc4
to	ten	k3xDgNnSc1
znamenalo	znamenat	k5eAaImAgNnS
drtivou	drtivý	k2eAgFnSc4d1
porážku	porážka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
velmi	velmi	k6eAd1
důležitá	důležitý	k2eAgFnSc1d1
bitva	bitva	k1gFnSc1
trvala	trvat	k5eAaImAgFnS
sotva	sotva	k6eAd1
tři	tři	k4xCgFnPc4
hodiny	hodina	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Důsledky	důsledek	k1gInPc1
</s>
<s>
Objev	objev	k1gInSc1
mrtvého	mrtvý	k2eAgMnSc2d1
krále	král	k1gMnSc2
Ludvíka	Ludvík	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Ludvíkovi	Ludvíkův	k2eAgMnPc1d1
hodně	hodně	k6eAd1
uškodilo	uškodit	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
třetina	třetina	k1gFnSc1
jeho	jeho	k3xOp3gFnSc2
armády	armáda	k1gFnSc2
(	(	kIx(
<g/>
zemská	zemský	k2eAgFnSc1d1
hotovost	hotovost	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
čele	čelo	k1gNnSc6
s	s	k7c7
Janem	Jan	k1gMnSc7
Zápolským	Zápolský	k2eAgInSc7d1
(	(	kIx(
<g/>
8000	#num#	k4
Uhrů	Uher	k1gMnPc2
a	a	k8xC
5000	#num#	k4
Chorvatů	Chorvat	k1gMnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zůstala	zůstat	k5eAaPmAgFnS
u	u	k7c2
Segedína	Segedín	k1gInSc2
a	a	k8xC
nestihla	stihnout	k5eNaPmAgFnS
se	se	k3xPyFc4
dostavit	dostavit	k5eAaPmF
na	na	k7c4
bojiště	bojiště	k1gNnSc4
včas	včas	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledkem	výsledek	k1gInSc7
bitvy	bitva	k1gFnSc2
nebyl	být	k5eNaImAgInS
jen	jen	k9
počátek	počátek	k1gInSc4
hegemonie	hegemonie	k1gFnSc2
Turků	Turek	k1gMnPc2
v	v	k7c6
Uhersku	Uhersko	k1gNnSc6
(	(	kIx(
<g/>
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
rychle	rychle	k6eAd1
postupovali	postupovat	k5eAaImAgMnP
směrem	směr	k1gInSc7
na	na	k7c4
sever	sever	k1gInSc4
až	až	k9
k	k	k7c3
dnešnímu	dnešní	k2eAgNnSc3d1
Slovensku	Slovensko	k1gNnSc3
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
vymření	vymření	k1gNnSc4
česko-uherské	česko-uherský	k2eAgFnSc2d1
větve	větev	k1gFnSc2
Jagellonců	Jagellonec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
nástupnických	nástupnický	k2eAgFnPc2d1
dohod	dohoda	k1gFnPc2
se	se	k3xPyFc4
uherským	uherský	k2eAgMnSc7d1
i	i	k9
českým	český	k2eAgMnSc7d1
králem	král	k1gMnSc7
po	po	k7c6
bitvě	bitva	k1gFnSc6
stal	stát	k5eAaPmAgMnS
Ferdinand	Ferdinand	k1gMnSc1
I.	I.	kA
Habsburský	habsburský	k2eAgMnSc1d1
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
se	se	k3xPyFc4
Habsburkové	Habsburk	k1gMnPc1
stali	stát	k5eAaPmAgMnP
na	na	k7c4
několik	několik	k4yIc4
století	století	k1gNnPc2
klíčovými	klíčový	k2eAgMnPc7d1
hráči	hráč	k1gMnPc7
středoevropské	středoevropský	k2eAgFnSc2d1
politiky	politika	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
na	na	k7c4
čas	čas	k1gInSc4
(	(	kIx(
<g/>
do	do	k7c2
vymření	vymření	k1gNnSc2
španělských	španělský	k2eAgInPc2d1
Habsburků	Habsburk	k1gInPc2
<g/>
)	)	kIx)
vůbec	vůbec	k9
nejmocnějším	mocný	k2eAgInSc7d3
evropským	evropský	k2eAgInSc7d1
rodem	rod	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bitva	bitva	k1gFnSc1
u	u	k7c2
Moháče	Moháč	k1gInSc2
tak	tak	k6eAd1
zahájila	zahájit	k5eAaPmAgFnS
jejich	jejich	k3xOp3gInSc4
vzestup	vzestup	k1gInSc4
v	v	k7c6
evropské	evropský	k2eAgFnSc6d1
politice	politika	k1gFnSc6
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Turci	Turek	k1gMnPc1
zanedlouho	zanedlouho	k6eAd1
poté	poté	k6eAd1
obsadili	obsadit	k5eAaPmAgMnP
Budín	Budín	k1gInSc4
<g/>
,	,	kIx,
uherské	uherský	k2eAgNnSc4d1
hlavní	hlavní	k2eAgNnSc4d1
město	město	k1gNnSc4
<g/>
;	;	kIx,
od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
je	být	k5eAaImIp3nS
bitva	bitva	k1gFnSc1
u	u	k7c2
Moháče	Moháč	k1gInSc2
pro	pro	k7c4
Maďary	Maďar	k1gMnPc4
symbolem	symbol	k1gInSc7
totální	totální	k2eAgFnSc2d1
porážky	porážka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
jiných	jiný	k2eAgFnPc2d1
velkých	velký	k2eAgFnPc2d1
porážek	porážka	k1gFnPc2
maďarských	maďarský	k2eAgFnPc2d1
dějin	dějiny	k1gFnPc2
(	(	kIx(
<g/>
bitva	bitva	k1gFnSc1
na	na	k7c4
Lechu	lecha	k1gFnSc4
<g/>
,	,	kIx,
bitva	bitva	k1gFnSc1
na	na	k7c4
Slané	Slaný	k1gInPc4
<g/>
,	,	kIx,
bitva	bitva	k1gFnSc1
u	u	k7c2
Világose	Világosa	k1gFnSc3
<g/>
)	)	kIx)
měla	mít	k5eAaImAgFnS
bitva	bitva	k1gFnSc1
u	u	k7c2
Moháče	Moháč	k1gInSc2
zničující	zničující	k2eAgInPc4d1
a	a	k8xC
hluboké	hluboký	k2eAgInPc4d1
důsledky	důsledek	k1gInPc4
na	na	k7c4
několik	několik	k4yIc4
staletí	staletí	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Turecká	turecký	k2eAgFnSc1d1
okupace	okupace	k1gFnSc1
střední	střední	k2eAgFnSc2d1
části	část	k1gFnSc2
Uherska	Uhersko	k1gNnSc2
trvala	trvat	k5eAaImAgFnS
150	#num#	k4
let	léto	k1gNnPc2
a	a	k8xC
ještě	ještě	k6eAd1
o	o	k7c4
dalších	další	k2eAgNnPc2d1
200	#num#	k4
let	léto	k1gNnPc2
déle	dlouho	k6eAd2
trvalo	trvat	k5eAaImAgNnS
<g/>
,	,	kIx,
než	než	k8xS
Maďaři	Maďar	k1gMnPc1
získali	získat	k5eAaPmAgMnP
v	v	k7c6
rámci	rámec	k1gInSc6
rakousko-uherského	rakousko-uherský	k2eAgNnSc2d1
vyrovnání	vyrovnání	k1gNnSc2
opět	opět	k6eAd1
(	(	kIx(
<g/>
faktickou	faktický	k2eAgFnSc4d1
<g/>
)	)	kIx)
suverenitu	suverenita	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
určitého	určitý	k2eAgInSc2d1
pohledu	pohled	k1gInSc2
se	se	k3xPyFc4
Uhersko	Uhersko	k1gNnSc1
z	z	k7c2
následků	následek	k1gInPc2
bitvy	bitva	k1gFnSc2
zcela	zcela	k6eAd1
nevzpamatovalo	vzpamatovat	k5eNaPmAgNnS
nikdy	nikdy	k6eAd1
–	–	k?
již	již	k6eAd1
se	se	k3xPyFc4
nikdy	nikdy	k6eAd1
nestalo	stát	k5eNaPmAgNnS
mocností	mocnost	k1gFnPc2
na	na	k7c6
úrovni	úroveň	k1gFnSc6
anjouovských	anjouovský	k2eAgFnPc2d1
Uher	Uhry	k1gFnPc2
ve	v	k7c6
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
Maďaři	Maďar	k1gMnPc1
<g/>
,	,	kIx,
pod	pod	k7c7
vládou	vláda	k1gFnSc7
Habsburků	Habsburk	k1gMnPc2
připravení	připravení	k1gNnSc2
o	o	k7c4
možnost	možnost	k1gFnSc4
suverénního	suverénní	k2eAgInSc2d1
vývoje	vývoj	k1gInSc2
v	v	k7c6
době	doba	k1gFnSc6
osvícenství	osvícenství	k1gNnSc2
a	a	k8xC
počátků	počátek	k1gInPc2
nacionalismu	nacionalismus	k1gInSc2
<g/>
,	,	kIx,
zůstali	zůstat	k5eAaPmAgMnP
jen	jen	k9
relativně	relativně	k6eAd1
malým	malý	k2eAgInSc7d1
evropským	evropský	k2eAgInSc7d1
národem	národ	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
už	už	k6eAd1
nedokázal	dokázat	k5eNaPmAgMnS
ovládnout	ovládnout	k5eAaPmF
celé	celý	k2eAgNnSc4d1
své	své	k1gNnSc4
historické	historický	k2eAgNnSc4d1
teritorium	teritorium	k1gNnSc4
(	(	kIx(
<g/>
Karpatskou	karpatský	k2eAgFnSc4d1
kotlinu	kotlina	k1gFnSc4
<g/>
)	)	kIx)
proti	proti	k7c3
souběžně	souběžně	k6eAd1
se	se	k3xPyFc4
obrozujícím	obrozující	k2eAgMnPc3d1
Slovanům	Slovan	k1gMnPc3
a	a	k8xC
Rumunům	Rumun	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Symbolickým	symbolický	k2eAgNnSc7d1
i	i	k8xC
praktickým	praktický	k2eAgNnSc7d1
stvrzením	stvrzení	k1gNnSc7
důsledků	důsledek	k1gInPc2
moháčské	moháčský	k2eAgFnSc2d1
katastrofy	katastrofa	k1gFnSc2
tak	tak	k6eAd1
byla	být	k5eAaImAgFnS
Trianonská	trianonský	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
určující	určující	k2eAgFnPc4d1
okleštěné	okleštěný	k2eAgFnPc4d1
hranice	hranice	k1gFnPc4
maďarského	maďarský	k2eAgInSc2d1
národního	národní	k2eAgInSc2d1
státu	stát	k1gInSc2
–	–	k?
dnešního	dnešní	k2eAgNnSc2d1
Maďarska	Maďarsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Druhá	druhý	k4xOgFnSc1
bitva	bitva	k1gFnSc1
u	u	k7c2
Moháče	Moháč	k1gInSc2
</s>
<s>
Jako	jako	k9
druhá	druhý	k4xOgFnSc1
bitva	bitva	k1gFnSc1
u	u	k7c2
Moháče	Moháč	k1gInSc2
se	se	k3xPyFc4
někdy	někdy	k6eAd1
označuje	označovat	k5eAaImIp3nS
bitva	bitva	k1gFnSc1
u	u	k7c2
Nagyharsánye	Nagyharsány	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
proběhla	proběhnout	k5eAaPmAgFnS
12	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1687	#num#	k4
s	s	k7c7
opačným	opačný	k2eAgInSc7d1
výsledkem	výsledek	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
pojmenování	pojmenování	k1gNnSc1
je	být	k5eAaImIp3nS
ovšem	ovšem	k9
silně	silně	k6eAd1
poplatné	poplatný	k2eAgFnSc3d1
propagandistické	propagandistický	k2eAgFnSc3d1
snaze	snaha	k1gFnSc3
přebít	přebít	k5eAaPmF
paměť	paměť	k1gFnSc4
první	první	k4xOgFnSc2
bitvy	bitva	k1gFnSc2
<g/>
,	,	kIx,
neboť	neboť	k8xC
od	od	k7c2
Moháče	Moháč	k1gInSc2
bylo	být	k5eAaImAgNnS
v	v	k7c6
tomto	tento	k3xDgInSc6
případě	případ	k1gInSc6
bitevní	bitevní	k2eAgNnSc1d1
pole	pole	k1gNnSc1
vzdáleno	vzdálit	k5eAaPmNgNnS
přes	přes	k7c4
40	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
Vojáci	voják	k1gMnPc1
z	z	k7c2
území	území	k1gNnSc2
evropské	evropský	k2eAgFnSc2d1
části	část	k1gFnSc2
osmanské	osmanský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
rozkládající	rozkládající	k2eAgFnSc2d1
se	se	k3xPyFc4
zhruba	zhruba	k6eAd1
na	na	k7c6
území	území	k1gNnSc6
dnešního	dnešní	k2eAgNnSc2d1
Řecka	Řecko	k1gNnSc2
a	a	k8xC
Makedonie	Makedonie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Kašpar	Kašpar	k1gMnSc1
Ráskay	Ráskaa	k1gFnSc2
se	se	k3xPyFc4
z	z	k7c2
prestižních	prestižní	k2eAgInPc2d1
důvodů	důvod	k1gInPc2
odmítal	odmítat	k5eAaImAgMnS
této	tento	k3xDgFnSc2
akce	akce	k1gFnSc2
zúčastnit	zúčastnit	k5eAaPmF
<g/>
,	,	kIx,
učinil	učinit	k5eAaPmAgMnS,k5eAaImAgMnS
tak	tak	k6eAd1
až	až	k9
na	na	k7c4
přímý	přímý	k2eAgInSc4d1
pokyn	pokyn	k1gInSc4
krále	král	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
ČORNEJ	ČORNEJ	kA
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
;	;	kIx,
BĚLINA	Bělina	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slavné	slavný	k2eAgFnPc4d1
bitvy	bitva	k1gFnPc4
naší	náš	k3xOp1gFnSc2
historie	historie	k1gFnSc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Marsyas	Marsyas	k1gMnSc1
<g/>
,	,	kIx,
1993	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
901606	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
116	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Dále	daleko	k6eAd2
jen	jen	k9
<g/>
:	:	kIx,
Čornej	Čornej	k1gMnSc1
<g/>
,	,	kIx,
Bělina	Bělina	k1gMnSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
MACEK	Macek	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jagellonský	jagellonský	k2eAgInSc4d1
věk	věk	k1gInSc4
v	v	k7c6
českých	český	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
I.	I.	kA
<g/>
–	–	k?
<g/>
II	II	kA
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
895	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
317	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
PERNES	PERNES	kA
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
;	;	kIx,
FUČÍK	Fučík	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
;	;	kIx,
HAVEL	Havel	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pod	pod	k7c7
císařským	císařský	k2eAgInSc7d1
praporem	prapor	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historie	historie	k1gFnSc1
habsburské	habsburský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
1526	#num#	k4
<g/>
-	-	kIx~
<g/>
1918	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Elka	Elka	k1gFnSc1
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
902745	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
11	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Dále	daleko	k6eAd2
jen	jen	k9
<g/>
:	:	kIx,
Pernes	Pernes	k1gMnSc1
<g/>
,	,	kIx,
Fučík	Fučík	k1gMnSc1
<g/>
,	,	kIx,
Havel	Havel	k1gMnSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Čornej	Čornej	k1gFnPc2
<g/>
,	,	kIx,
Bělina	bělina	k1gFnSc1
<g/>
,	,	kIx,
s.	s.	k?
117	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
VYBÍRAL	Vybíral	k1gMnSc1
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bitva	bitva	k1gFnSc1
u	u	k7c2
Moháče	Moháč	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krvavá	krvavý	k2eAgFnSc1d1
porážka	porážka	k1gFnSc1
uherského	uherský	k2eAgNnSc2d1
a	a	k8xC
českého	český	k2eAgMnSc2d1
krále	král	k1gMnSc2
Ludvíka	Ludvík	k1gMnSc2
Jagellonského	jagellonský	k2eAgMnSc2d1
v	v	k7c6
boji	boj	k1gInSc6
s	s	k7c7
Osmany	Osman	k1gMnPc7
29	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1526	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Havran	Havran	k1gMnSc1
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86515	#num#	k4
<g/>
-	-	kIx~
<g/>
87	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
68	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Dále	daleko	k6eAd2
jen	jen	k9
<g/>
:	:	kIx,
Vybíral	Vybíral	k1gMnSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
Vybíral	Vybíral	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
70	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
KOLEKTIV	kolektivum	k1gNnPc2
AUTORŮ	autor	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vojenské	vojenský	k2eAgFnPc1d1
dějiny	dějiny	k1gFnPc1
Československa	Československo	k1gNnSc2
I.	I.	kA
díl	díl	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Naše	náš	k3xOp1gNnSc1
vojsko	vojsko	k1gNnSc1
<g/>
,	,	kIx,
1986	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
342	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Dále	daleko	k6eAd2
jen	jen	k9
<g/>
:	:	kIx,
Vojenské	vojenský	k2eAgFnPc4d1
dějiny	dějiny	k1gFnPc4
Československa	Československo	k1gNnSc2
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
Čornej	Čornej	k1gMnSc1
<g/>
,	,	kIx,
Bělina	Bělina	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
111	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Vybíral	Vybíral	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
119	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Vybíral	Vybíral	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
121	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Vybíral	Vybíral	k1gMnSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
s.	s.	k?
121	#num#	k4
<g/>
–	–	k?
<g/>
123	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Vybíral	Vybíral	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
122	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Vybíral	Vybíral	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
122	#num#	k4
<g/>
–	–	k?
<g/>
124	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Vybíral	Vybíral	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
127	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Vybíral	Vybíral	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
128	#num#	k4
<g/>
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
↑	↑	k?
Vybíral	Vybíral	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
125	#num#	k4
a	a	k8xC
128	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Vybíral	Vybíral	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
163	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Vybíral	Vybíral	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
166	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Vybíral	Vybíral	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
164.1	164.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Čornej	Čornej	k1gFnPc2
<g/>
,	,	kIx,
Bělina	bělina	k1gFnSc1
<g/>
,	,	kIx,
s.	s.	k?
113	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Čornej	Čornej	k1gMnSc1
<g/>
,	,	kIx,
Bělina	Bělina	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
113	#num#	k4
<g/>
–	–	k?
<g/>
116	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Vybíral	Vybíral	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
169	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Vojenské	vojenský	k2eAgFnPc4d1
dějiny	dějiny	k1gFnPc4
Československa	Československo	k1gNnSc2
<g/>
,	,	kIx,
s.	s.	k?
331	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Vybíral	Vybíral	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
168	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Vojenské	vojenský	k2eAgFnPc4d1
dějiny	dějiny	k1gFnPc4
Československa	Československo	k1gNnSc2
<g/>
,	,	kIx,
s.	s.	k?
341	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Pernes	Pernes	k1gMnSc1
<g/>
,	,	kIx,
Fučík	Fučík	k1gMnSc1
<g/>
,	,	kIx,
Havel	Havel	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
10	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
VOGELTANZ	VOGELTANZ	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Malý	malý	k2eAgInSc1d1
slovník	slovník	k1gInSc1
starého	starý	k2eAgNnSc2d1
vojenského	vojenský	k2eAgNnSc2d1
názvosloví	názvosloví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
a	a	k8xC
Litomyšl	Litomyšl	k1gFnSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7185	#num#	k4
<g/>
-	-	kIx~
<g/>
928	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
55	#num#	k4
<g/>
–	–	k?
<g/>
56	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Vybíral	Vybíral	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
189	#num#	k4
<g/>
–	–	k?
<g/>
190	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Vybíral	Vybíral	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
18.1	18.1	k4
2	#num#	k4
Vybíral	Vybíral	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
190	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Vybíral	Vybíral	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
18	#num#	k4
<g/>
–	–	k?
<g/>
19	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Vybíral	Vybíral	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
40	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Vybíral	Vybíral	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
41.1	41.1	k4
2	#num#	k4
3	#num#	k4
Vybíral	Vybíral	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
16	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Vybíral	Vybíral	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
17	#num#	k4
<g/>
–	–	k?
<g/>
18	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Vybíral	Vybíral	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
17	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Vybíral	Vybíral	k1gMnSc1
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
s.	s.	k?
20	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Vybíral	Vybíral	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
22	#num#	k4
<g/>
–	–	k?
<g/>
23	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Vybíral	Vybíral	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
203	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Vybíral	Vybíral	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
26	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Vybíral	Vybíral	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
29	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Vybíral	Vybíral	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
30	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Vybíral	Vybíral	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
31	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Vybíral	Vybíral	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
32	#num#	k4
a	a	k8xC
43	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Vybíral	Vybíral	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
33	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
BARTLOVÁ	Bartlová	k1gFnSc1
<g/>
,	,	kIx,
Milena	Milena	k1gFnSc1
<g/>
;	;	kIx,
ČORNEJ	ČORNEJ	kA
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velké	velký	k2eAgFnPc1d1
dějiny	dějiny	k1gFnPc1
zemí	zem	k1gFnPc2
Koruny	koruna	k1gFnSc2
české	český	k2eAgFnSc2d1
VI	VI	kA
<g/>
..	..	k?
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Paseka	Paseka	k1gMnSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
800	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7185	#num#	k4
<g/>
-	-	kIx~
<g/>
873	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
ČORNEJ	ČORNEJ	kA
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
;	;	kIx,
BĚLINA	Bělina	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slavné	slavný	k2eAgFnPc4d1
bitvy	bitva	k1gFnPc4
naší	náš	k3xOp1gFnSc2
historie	historie	k1gFnSc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Marsyas	Marsyas	k1gMnSc1
<g/>
,	,	kIx,
1993	#num#	k4
<g/>
.	.	kIx.
272	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
901606	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
MACEK	Macek	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jagellonský	jagellonský	k2eAgInSc4d1
věk	věk	k1gInSc4
v	v	k7c6
českých	český	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
I.	I.	kA
<g/>
–	–	k?
<g/>
II	II	kA
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
561	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
895	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
PERNES	PERNES	kA
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
;	;	kIx,
FUČÍK	Fučík	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
;	;	kIx,
HAVEL	Havel	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pod	pod	k7c7
císařským	císařský	k2eAgInSc7d1
praporem	prapor	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historie	historie	k1gFnSc1
habsburské	habsburský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
1526	#num#	k4
<g/>
-	-	kIx~
<g/>
1918	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Elka	Elka	k1gFnSc1
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
555	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
902745	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
VYBÍRAL	Vybíral	k1gMnSc1
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bitva	bitva	k1gFnSc1
u	u	k7c2
Moháče	Moháč	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krvavá	krvavý	k2eAgFnSc1d1
porážka	porážka	k1gFnSc1
uherského	uherský	k2eAgNnSc2d1
a	a	k8xC
českého	český	k2eAgMnSc2d1
krále	král	k1gMnSc2
Ludvíka	Ludvík	k1gMnSc2
Jagellonského	jagellonský	k2eAgMnSc2d1
v	v	k7c6
boji	boj	k1gInSc6
s	s	k7c7
Osmany	Osman	k1gMnPc7
29	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1526	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Havran	Havran	k1gMnSc1
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
225	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86515	#num#	k4
<g/>
-	-	kIx~
<g/>
87	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Bitva	bitva	k1gFnSc1
u	u	k7c2
Moháče	Moháč	k1gInSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Bellum	Bellum	k1gNnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
:	:	kIx,
bitva	bitva	k1gFnSc1
u	u	k7c2
Moháče	Moháč	k1gInSc2
29	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1526	#num#	k4
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
památníku	památník	k1gInSc2
bitvy	bitva	k1gFnSc2
u	u	k7c2
Moháče	Moháč	k1gInSc2
(	(	kIx(
<g/>
maďarsky	maďarsky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ph	ph	kA
<g/>
506467	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4211930-3	4211930-3	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85086492	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85086492	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Maďarsko	Maďarsko	k1gNnSc1
|	|	kIx~
Válka	válka	k1gFnSc1
</s>
