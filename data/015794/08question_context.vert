<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Moháče	Moháč	k1gInSc2
(	(	kIx(
<g/>
maďarsky	maďarsky	k6eAd1
Mohácsi-csata	Mohácsi-csat	k2eAgFnSc1d1
<g/>
,	,	kIx,
německy	německy	k6eAd1
Schlacht	Schlacht	k1gInSc1
bei	bei	k?
Mohács	Mohács	k1gInSc1
<g/>
,	,	kIx,
turecky	turecky	k6eAd1
Mohaç	Mohaç	k1gFnPc4
Muharebesi	Muharebese	k1gFnSc4
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
jednou	jeden	k4xCgFnSc7
z	z	k7c2
nejvýznamnějších	významný	k2eAgFnPc2d3
bitev	bitva	k1gFnPc2
v	v	k7c6
dějinách	dějiny	k1gFnPc6
střední	střední	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
některých	některý	k3yIgNnPc6
pojetích	pojetí	k1gNnPc6
považovaná	považovaný	k2eAgFnSc1d1
za	za	k7c4
mezník	mezník	k1gInSc4
symbolizující	symbolizující	k2eAgInSc4d1
zde	zde	k6eAd1
konec	konec	k1gInSc4
středověku	středověk	k1gInSc2
a	a	k8xC
počátek	počátek	k1gInSc4
novověku	novověk	k1gInSc2
<g/>
.	.	kIx.
</s>