<s desamb="1">
Početně	početně	k6eAd1
silnější	silný	k2eAgNnSc4d2
vojsko	vojsko	k1gNnSc4
osmanského	osmanský	k2eAgMnSc2d1
sultána	sultán	k1gMnSc2
Süleymana	Süleyman	k1gMnSc2
I.	I.	kA
zde	zde	k6eAd1
během	během	k7c2
necelých	celý	k2eNgFnPc2d1
dvou	dva	k4xCgFnPc2
hodin	hodina	k1gFnPc2
drtivě	drtivě	k6eAd1
porazilo	porazit	k5eAaPmAgNnS
oddíly	oddíl	k1gInPc4
shromážděné	shromážděný	k2eAgInPc4d1
pod	pod	k7c7
korouhví	korouhev	k1gFnSc7
dvacetiletého	dvacetiletý	k2eAgInSc2d1
českého	český	k2eAgInSc2d1
<g/>
,	,	kIx,
uherského	uherský	k2eAgMnSc2d1
a	a	k8xC
chorvatského	chorvatský	k2eAgMnSc2d1
krále	král	k1gMnSc2
Ludvíka	Ludvík	k1gMnSc2
Jagellonského	jagellonský	k2eAgMnSc2d1
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
při	při	k7c6
útěku	útěk	k1gInSc6
z	z	k7c2
bojiště	bojiště	k1gNnSc2
utonul	utonout	k5eAaPmAgMnS
v	v	k7c6
říčce	říčka	k1gFnSc6
Csele	Csele	k1gFnSc2
<g/>
.	.	kIx.
</s>