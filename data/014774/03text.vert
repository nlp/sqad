<s>
Skládací	skládací	k2eAgNnSc1d1
kolo	kolo	k1gNnSc1
</s>
<s>
Kolo	kolo	k1gNnSc1
Brompton	Brompton	k1gInSc1
v	v	k7c6
rozloženém	rozložený	k2eAgInSc6d1
stavu	stav	k1gInSc6
</s>
<s>
Kolo	kolo	k1gNnSc1
Brompton	Brompton	k1gInSc1
ve	v	k7c6
složeném	složený	k2eAgInSc6d1
stavu	stav	k1gInSc6
</s>
<s>
Skládací	skládací	k2eAgNnSc1d1
kolo	kolo	k1gNnSc1
je	být	k5eAaImIp3nS
takové	takový	k3xDgNnSc1
jízdní	jízdní	k2eAgNnSc1d1
kolo	kolo	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
snadno	snadno	k6eAd1
složit	složit	k5eAaPmF
do	do	k7c2
výrazně	výrazně	k6eAd1
menší	malý	k2eAgFnSc2d2
podoby	podoba	k1gFnSc2
<g/>
,	,	kIx,
nejčastěji	často	k6eAd3
pro	pro	k7c4
potřeby	potřeba	k1gFnPc4
jeho	jeho	k3xOp3gNnPc2
převozu	převoz	k1gInSc3
jiným	jiný	k2eAgInSc7d1
dopravním	dopravní	k2eAgInSc7d1
prostředkem	prostředek	k1gInSc7
<g/>
,	,	kIx,
nebo	nebo	k8xC
za	za	k7c7
účelem	účel	k1gInSc7
uskladnění	uskladnění	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Skládací	skládací	k2eAgInSc1d1
systém	systém	k1gInSc1
je	být	k5eAaImIp3nS
nejčastěji	často	k6eAd3
založen	založit	k5eAaPmNgInS
na	na	k7c4
použití	použití	k1gNnSc4
pantu	pant	k1gInSc2
(	(	kIx(
<g/>
nebo	nebo	k8xC
i	i	k9
více	hodně	k6eAd2
pantů	pant	k1gInPc2
<g/>
)	)	kIx)
na	na	k7c6
rámu	rám	k1gInSc6
kola	kolo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavním	hlavní	k2eAgInSc7d1
cílem	cíl	k1gInSc7
při	při	k7c6
návrhu	návrh	k1gInSc6
skládacího	skládací	k2eAgNnSc2d1
kola	kolo	k1gNnSc2
bývá	bývat	k5eAaImIp3nS
právě	právě	k9
co	co	k9
největší	veliký	k2eAgFnSc4d3
skladnost	skladnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Té	ten	k3xDgFnSc2
se	se	k3xPyFc4
dociluje	docilovat	k5eAaImIp3nS
obvykle	obvykle	k6eAd1
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
zejména	zejména	k9
použitím	použití	k1gNnSc7
menších	malý	k2eAgNnPc2d2
kol	kolo	k1gNnPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
dělá	dělat	k5eAaImIp3nS
jízdu	jízda	k1gFnSc4
po	po	k7c6
horším	zlý	k2eAgInSc6d2
terénu	terén	k1gInSc6
méně	málo	k6eAd2
komfortní	komfortní	k2eAgNnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc7d1
nevýhodou	nevýhoda	k1gFnSc7
bývá	bývat	k5eAaImIp3nS
o	o	k7c4
něco	něco	k3yInSc4
větší	veliký	k2eAgFnSc1d2
hmotnost	hmotnost	k1gFnSc1
<g/>
,	,	kIx,
daná	daný	k2eAgFnSc1d1
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jednotlivé	jednotlivý	k2eAgFnPc1d1
části	část	k1gFnPc1
(	(	kIx(
<g/>
zejména	zejména	k9
rám	rám	k1gInSc1
a	a	k8xC
sedlovka	sedlovka	k1gFnSc1
<g/>
)	)	kIx)
nemohou	moct	k5eNaImIp3nP
mít	mít	k5eAaImF
ideální	ideální	k2eAgInSc4d1
tvar	tvar	k1gInSc4
z	z	k7c2
hlediska	hledisko	k1gNnSc2
pevnosti	pevnost	k1gFnSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
kompenzováno	kompenzovat	k5eAaBmNgNnS
použitím	použití	k1gNnSc7
více	hodně	k6eAd2
materiálu	materiál	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Významní	významný	k2eAgMnPc1d1
světoví	světový	k2eAgMnPc1d1
výrobci	výrobce	k1gMnPc1
skládacích	skládací	k2eAgFnPc2d1
kol	kolo	k1gFnPc2
jsou	být	k5eAaImIp3nP
firmy	firma	k1gFnPc1
Dahon	Dahona	k1gInSc1
a	a	k8xC
Brompton	Brompton	k1gInSc1
Bicycle	Bicycle	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Československu	Československo	k1gNnSc6
měla	mít	k5eAaImAgFnS
dlouhou	dlouhý	k2eAgFnSc4d1
tradici	tradice	k1gFnSc4
skládací	skládací	k2eAgFnSc1d1
kola	kola	k1gFnSc1
značky	značka	k1gFnSc2
Eska	eska	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Využití	využití	k1gNnSc1
</s>
<s>
Skládací	skládací	k2eAgNnSc1d1
kolo	kolo	k1gNnSc1
lze	lze	k6eAd1
dobře	dobře	k6eAd1
využívat	využívat	k5eAaImF,k5eAaPmF
v	v	k7c6
rámci	rámec	k1gInSc6
osobní	osobní	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
pro	pro	k7c4
kombinování	kombinování	k1gNnSc4
více	hodně	k6eAd2
dopravních	dopravní	k2eAgInPc2d1
prostředků	prostředek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Typickým	typický	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
použití	použití	k1gNnSc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
například	například	k6eAd1
dojíždění	dojíždění	k1gNnSc4
do	do	k7c2
práce	práce	k1gFnSc2
ze	z	k7c2
vsi	ves	k1gFnSc2
do	do	k7c2
města	město	k1gNnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
je	být	k5eAaImIp3nS
nejprve	nejprve	k6eAd1
použito	použit	k2eAgNnSc1d1
skládací	skládací	k2eAgNnSc1d1
kolo	kolo	k1gNnSc1
pro	pro	k7c4
dopravu	doprava	k1gFnSc4
k	k	k7c3
nádraží	nádraží	k1gNnSc3
<g/>
,	,	kIx,
pak	pak	k6eAd1
se	se	k3xPyFc4
pracovník	pracovník	k1gMnSc1
vlakem	vlak	k1gInSc7
převeze	převézt	k5eAaPmIp3nS
do	do	k7c2
města	město	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
okolo	okolo	k7c2
opět	opět	k6eAd1
rozloží	rozložit	k5eAaPmIp3nS
<g/>
,	,	kIx,
dopraví	dopravit	k5eAaPmIp3nS
se	se	k3xPyFc4
na	na	k7c6
něm	on	k3xPp3gMnSc6
do	do	k7c2
práce	práce	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
ho	on	k3xPp3gNnSc4
složené	složený	k2eAgNnSc4d1
snadno	snadno	k6eAd1
uskladní	uskladnit	k5eAaPmIp3nS
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
mu	on	k3xPp3gMnSc3
nemohlo	moct	k5eNaImAgNnS
být	být	k5eAaImF
ukradeno	ukrást	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prostředky	prostředek	k1gInPc7
hromadné	hromadný	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
nepovolují	povolovat	k5eNaImIp3nP
dopravu	doprava	k1gFnSc4
obyčejných	obyčejný	k2eAgNnPc2d1
jízdních	jízdní	k2eAgNnPc2d1
kol	kolo	k1gNnPc2
<g/>
,	,	kIx,
obvykle	obvykle	k6eAd1
umožňují	umožňovat	k5eAaImIp3nP
přepravovat	přepravovat	k5eAaImF
složená	složený	k2eAgFnSc1d1
skládací	skládací	k2eAgFnSc1d1
kola	kola	k1gFnSc1
a	a	k8xC
to	ten	k3xDgNnSc1
buď	buď	k8xC
explicitně	explicitně	k6eAd1
(	(	kIx(
<g/>
tak	tak	k6eAd1
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
povoleno	povolit	k5eAaPmNgNnS
například	například	k6eAd1
v	v	k7c6
hromadné	hromadný	k2eAgFnSc6d1
dopravě	doprava	k1gFnSc6
v	v	k7c6
Londýně	Londýn	k1gInSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nebo	nebo	k8xC
je	být	k5eAaImIp3nS
lze	lze	k6eAd1
zabalit	zabalit	k5eAaPmF
a	a	k8xC
přepravovat	přepravovat	k5eAaImF
je	on	k3xPp3gInPc4
jako	jako	k8xC,k8xS
běžnou	běžný	k2eAgFnSc4d1
velkou	velký	k2eAgFnSc4d1
tašku	taška	k1gFnSc4
či	či	k8xC
kufr	kufr	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
rámci	rámec	k1gInSc6
rekreačního	rekreační	k2eAgNnSc2d1
cestování	cestování	k1gNnSc2
je	být	k5eAaImIp3nS
běžné	běžný	k2eAgNnSc1d1
používání	používání	k1gNnSc1
skládacích	skládací	k2eAgNnPc2d1
kol	kolo	k1gNnPc2
tam	tam	k6eAd1
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
majitel	majitel	k1gMnSc1
přepravuje	přepravovat	k5eAaImIp3nS
primárně	primárně	k6eAd1
větším	veliký	k2eAgInSc7d2
dopravním	dopravní	k2eAgInSc7d1
prostředkem	prostředek	k1gInSc7
<g/>
,	,	kIx,
například	například	k6eAd1
cestuje	cestovat	k5eAaImIp3nS
lodí	loď	k1gFnSc7
nebo	nebo	k8xC
s	s	k7c7
karavanem	karavan	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Skládací	skládací	k2eAgFnSc1d1
kola	kola	k1gFnSc1
byla	být	k5eAaImAgFnS
též	též	k9
používána	používán	k2eAgFnSc1d1
ve	v	k7c6
válečných	válečný	k2eAgInPc6d1
konfliktech	konflikt	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Britská	britský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
používala	používat	k5eAaImAgFnS
Pedersenovo	Pedersenův	k2eAgNnSc4d1
kolo	kolo	k1gNnSc4
v	v	k7c6
druhé	druhý	k4xOgFnSc6
búrské	búrský	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skládací	skládací	k2eAgInSc1d1
kola	kolo	k1gNnSc2
použili	použít	k5eAaPmAgMnP
také	také	k9
britští	britský	k2eAgMnPc1d1
armádní	armádní	k2eAgMnPc1d1
parašutisté	parašutista	k1gMnPc1
v	v	k7c6
druhé	druhý	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
,	,	kIx,
například	například	k6eAd1
při	při	k7c6
vylodění	vylodění	k1gNnSc6
v	v	k7c6
Normandii	Normandie	k1gFnSc6
a	a	k8xC
bitvě	bitva	k1gFnSc6
o	o	k7c6
Arnhem	Arnh	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
skládací	skládací	k2eAgFnSc2d1
kolo	kolo	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Do	do	k7c2
českých	český	k2eAgFnPc2d1
ulic	ulice	k1gFnPc2
se	se	k3xPyFc4
vracejí	vracet	k5eAaImIp3nP
skládací	skládací	k2eAgNnPc1d1
kola	kolo	k1gNnPc1
<g/>
,	,	kIx,
cyklotoulky	cyklotoulka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Test	test	k1gInSc1
skládacích	skládací	k2eAgNnPc2d1
kol	kolo	k1gNnPc2
–	–	k?
NaKole	NaKole	k1gFnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Cyklistika	cyklistika	k1gFnSc1
</s>
