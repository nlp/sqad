<p>
<s>
Obrnice	Obrnice	k1gFnSc1	Obrnice
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Obernitz	Obernitz	k1gInSc1	Obernitz
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
obec	obec	k1gFnSc4	obec
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Most	most	k1gInSc1	most
v	v	k7c6	v
Ústeckém	ústecký	k2eAgInSc6d1	ústecký
kraji	kraj	k1gInSc6	kraj
na	na	k7c6	na
říčce	říčka	k1gFnSc6	říčka
Srpině	Srpina	k1gFnSc6	Srpina
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
213	[number]	k4	213
m.	m.	k?	m.
Obec	obec	k1gFnSc1	obec
leží	ležet	k5eAaImIp3nS	ležet
jen	jen	k9	jen
asi	asi	k9	asi
3,5	[number]	k4	3,5
km	km	kA	km
východně	východně	k6eAd1	východně
od	od	k7c2	od
města	město	k1gNnSc2	město
Mostu	most	k1gInSc2	most
po	po	k7c6	po
silnici	silnice	k1gFnSc6	silnice
I	i	k9	i
<g/>
/	/	kIx~	/
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
Mikroregionu	mikroregion	k1gInSc2	mikroregion
Srpina	Srpina	k1gFnSc1	Srpina
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
přibližně	přibližně	k6eAd1	přibližně
2	[number]	k4	2
100	[number]	k4	100
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Obrnicích	Obrnice	k1gFnPc6	Obrnice
je	být	k5eAaImIp3nS	být
železniční	železniční	k2eAgFnSc1d1	železniční
stanice	stanice	k1gFnSc1	stanice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
k	k	k7c3	k
procházející	procházející	k2eAgFnSc3d1	procházející
trati	trať	k1gFnSc3	trať
Louny	Louny	k1gInPc1	Louny
-	-	kIx~	-
Most	most	k1gInSc1	most
připojuje	připojovat	k5eAaImIp3nS	připojovat
železniční	železniční	k2eAgFnSc4d1	železniční
trať	trať	k1gFnSc4	trať
z	z	k7c2	z
Čížkovic	Čížkovice	k1gFnPc2	Čížkovice
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
pravidelné	pravidelný	k2eAgFnSc2d1	pravidelná
osobní	osobní	k2eAgFnSc2d1	osobní
dopravy	doprava	k1gFnSc2	doprava
<g/>
)	)	kIx)	)
a	a	k8xC	a
trať	trať	k1gFnSc1	trať
z	z	k7c2	z
Žatce	Žatec	k1gInSc2	Žatec
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
spojkou	spojka	k1gFnSc7	spojka
bez	bez	k7c2	bez
pravidelné	pravidelný	k2eAgFnSc2d1	pravidelná
osobní	osobní	k2eAgFnSc2d1	osobní
dopravy	doprava	k1gFnSc2	doprava
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
do	do	k7c2	do
Odbočky	odbočka	k1gFnSc2	odbočka
České	český	k2eAgInPc1d1	český
Zlatníky	zlatník	k1gInPc1	zlatník
<g/>
.	.	kIx.	.
</s>
<s>
Spojení	spojení	k1gNnSc1	spojení
s	s	k7c7	s
Mostem	most	k1gInSc7	most
kromě	kromě	k7c2	kromě
vlaků	vlak	k1gInPc2	vlak
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
též	též	k9	též
autobusové	autobusový	k2eAgFnPc1d1	autobusová
linky	linka	k1gFnPc1	linka
MHD	MHD	kA	MHD
č.	č.	k?	č.
10	[number]	k4	10
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obrnice	Obrnice	k1gFnPc1	Obrnice
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
obcí	obec	k1gFnPc2	obec
s	s	k7c7	s
největším	veliký	k2eAgInSc7d3	veliký
podílem	podíl	k1gInSc7	podíl
osob	osoba	k1gFnPc2	osoba
v	v	k7c6	v
exekuci	exekuce	k1gFnSc6	exekuce
v	v	k7c6	v
ČR	ČR	kA	ČR
(	(	kIx(	(
<g/>
51,2	[number]	k4	51,2
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
obci	obec	k1gFnSc6	obec
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1282	[number]	k4	1282
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgFnP	být
Obrnice	Obrnice	k1gFnPc1	Obrnice
darovány	darován	k2eAgFnPc1d1	darována
cisterciáckému	cisterciácký	k2eAgInSc3d1	cisterciácký
klášteru	klášter	k1gInSc3	klášter
v	v	k7c6	v
Oseku	Osek	k1gInSc6	Osek
<g/>
.	.	kIx.	.
</s>
<s>
Historie	historie	k1gFnSc1	historie
Obrnice	Obrnice	k1gFnSc1	Obrnice
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
klášterem	klášter	k1gInSc7	klášter
úzce	úzko	k6eAd1	úzko
spjata	spjat	k2eAgFnSc1d1	spjata
<g/>
.	.	kIx.	.
</s>
<s>
Klášter	klášter	k1gInSc1	klášter
měl	mít	k5eAaImAgInS	mít
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
města	město	k1gNnSc2	město
vinice	vinice	k1gFnSc2	vinice
<g/>
,	,	kIx,	,
na	na	k7c4	na
což	což	k3yRnSc4	což
dodnes	dodnes	k6eAd1	dodnes
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
pomístní	pomístní	k2eAgNnSc4d1	pomístní
jméno	jméno	k1gNnSc4	jméno
Osecká	osecký	k2eAgFnSc1d1	Osecká
Vinice	vinice	k1gFnSc1	vinice
na	na	k7c6	na
jihovýchodním	jihovýchodní	k2eAgInSc6d1	jihovýchodní
svahu	svah	k1gInSc6	svah
Keřového	keřový	k2eAgInSc2d1	keřový
vrchu	vrch	k1gInSc2	vrch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obrnice	Obrnice	k1gFnPc1	Obrnice
jsou	být	k5eAaImIp3nP	být
jako	jako	k9	jako
klášterní	klášterní	k2eAgInSc4d1	klášterní
majetek	majetek	k1gInSc4	majetek
jmenovány	jmenovat	k5eAaBmNgInP	jmenovat
i	i	k9	i
v	v	k7c6	v
potvrzení	potvrzení	k1gNnSc6	potvrzení
privilegií	privilegium	k1gNnPc2	privilegium
oseckého	osecký	k2eAgInSc2d1	osecký
kláštera	klášter	k1gInSc2	klášter
Janem	Jan	k1gMnSc7	Jan
Lucemburským	lucemburský	k2eAgFnPc3d1	Lucemburská
roku	rok	k1gInSc2	rok
1341	[number]	k4	1341
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1420	[number]	k4	1420
císař	císař	k1gMnSc1	císař
Zikmund	Zikmund	k1gMnSc1	Zikmund
věnoval	věnovat	k5eAaPmAgMnS	věnovat
Obrnice	Obrnice	k1gFnPc4	Obrnice
Mikuláši	Mikuláš	k1gMnSc3	Mikuláš
Schönbritzovi	Schönbritz	k1gMnSc3	Schönbritz
za	za	k7c2	za
prokázané	prokázaný	k2eAgFnSc2d1	prokázaná
válečné	válečný	k2eAgFnSc2d1	válečná
služby	služba	k1gFnSc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
něj	on	k3xPp3gNnSc2	on
přešla	přejít	k5eAaPmAgFnS	přejít
ves	ves	k1gFnSc1	ves
na	na	k7c4	na
Jana	Jan	k1gMnSc4	Jan
z	z	k7c2	z
Vřesovic	Vřesovice	k1gFnPc2	Vřesovice
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
majitelem	majitel	k1gMnSc7	majitel
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1454	[number]	k4	1454
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1460	[number]	k4	1460
se	se	k3xPyFc4	se
Obrnice	Obrnice	k1gFnSc1	Obrnice
opět	opět	k6eAd1	opět
staly	stát	k5eAaPmAgFnP	stát
majetkem	majetek	k1gInSc7	majetek
oseckého	osecký	k2eAgInSc2d1	osecký
kláštera	klášter	k1gInSc2	klášter
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1531	[number]	k4	1531
klášter	klášter	k1gInSc1	klášter
zastavil	zastavit	k5eAaPmAgInS	zastavit
Obrnice	Obrnice	k1gFnPc4	Obrnice
na	na	k7c4	na
dluhy	dluh	k1gInPc4	dluh
Hanušovi	Hanuš	k1gMnSc3	Hanuš
Manvicovi	Manvic	k1gMnSc3	Manvic
z	z	k7c2	z
Patokryj	Patokryj	k1gFnSc1	Patokryj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1620-1629	[number]	k4	1620-1629
patřila	patřit	k5eAaImAgFnS	patřit
ves	ves	k1gFnSc1	ves
rytíři	rytíř	k1gMnSc3	rytíř
Felixi	Felix	k1gMnSc3	Felix
Kaplířovi	Kaplíř	k1gMnSc3	Kaplíř
ze	z	k7c2	z
Sulevic	Sulevice	k1gFnPc2	Sulevice
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
byla	být	k5eAaImAgFnS	být
opět	opět	k6eAd1	opět
majetkem	majetek	k1gInSc7	majetek
kláštera	klášter	k1gInSc2	klášter
a	a	k8xC	a
do	do	k7c2	do
panství	panství	k1gNnSc2	panství
patřily	patřit	k5eAaImAgFnP	patřit
Obrnice	Obrnice	k1gFnPc1	Obrnice
až	až	k6eAd1	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
feudalismu	feudalismus	k1gInSc2	feudalismus
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1848	[number]	k4	1848
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1850	[number]	k4	1850
se	se	k3xPyFc4	se
Obrnice	Obrnice	k1gFnPc1	Obrnice
staly	stát	k5eAaPmAgFnP	stát
osadou	osada	k1gFnSc7	osada
obce	obec	k1gFnPc1	obec
Střimice	Střimika	k1gFnSc6	Střimika
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1909	[number]	k4	1909
se	se	k3xPyFc4	se
osamostatnily	osamostatnit	k5eAaPmAgFnP	osamostatnit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
živilo	živit	k5eAaImAgNnS	živit
v	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
probíhal	probíhat	k5eAaImAgInS	probíhat
rozvoj	rozvoj	k1gInSc4	rozvoj
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
související	související	k2eAgFnSc1d1	související
nárůst	nárůst	k1gInSc4	nárůst
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Obrnic	Obrnice	k1gFnPc2	Obrnice
vznikaly	vznikat	k5eAaImAgFnP	vznikat
cihelny	cihelna	k1gFnPc1	cihelna
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
zaměstnávaly	zaměstnávat	k5eAaImAgFnP	zaměstnávat
velký	velký	k2eAgInSc4d1	velký
počet	počet	k1gInSc4	počet
přistěhovalých	přistěhovalý	k2eAgMnPc2d1	přistěhovalý
dělníků	dělník	k1gMnPc2	dělník
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
Čechů	Čech	k1gMnPc2	Čech
z	z	k7c2	z
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
zde	zde	k6eAd1	zde
proto	proto	k8xC	proto
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
česká	český	k2eAgFnSc1d1	Česká
obecná	obecný	k2eAgFnSc1d1	obecná
škola	škola	k1gFnSc1	škola
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
zde	zde	k6eAd1	zde
také	také	k9	také
velké	velký	k2eAgNnSc1d1	velké
železniční	železniční	k2eAgNnSc1d1	železniční
nádraží	nádraží	k1gNnSc1	nádraží
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
zde	zde	k6eAd1	zde
byly	být	k5eAaImAgFnP	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
zajatecké	zajatecký	k2eAgInPc1d1	zajatecký
a	a	k8xC	a
pracovní	pracovní	k2eAgInPc4d1	pracovní
tábory	tábor	k1gInPc4	tábor
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgInPc6	který
bydlelo	bydlet	k5eAaImAgNnS	bydlet
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
cizinců	cizinec	k1gMnPc2	cizinec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
pracovali	pracovat	k5eAaImAgMnP	pracovat
ve	v	k7c6	v
zdejší	zdejší	k2eAgFnSc6d1	zdejší
cihelně	cihelna	k1gFnSc6	cihelna
<g/>
,	,	kIx,	,
štěrkovně	štěrkovna	k1gFnSc6	štěrkovna
<g/>
,	,	kIx,	,
na	na	k7c6	na
úpravě	úprava	k1gFnSc6	úprava
trati	trať	k1gFnSc6	trať
i	i	k8xC	i
v	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
bylo	být	k5eAaImAgNnS	být
původní	původní	k2eAgNnSc1d1	původní
německy	německy	k6eAd1	německy
mluvící	mluvící	k2eAgNnSc1d1	mluvící
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
vysídleno	vysídlen	k2eAgNnSc1d1	vysídleno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
Českých	český	k2eAgInPc6d1	český
Zlatníkách	Zlatníkách	k?	Zlatníkách
a	a	k8xC	a
Chanově	Chanov	k1gInSc6	Chanov
zrušeny	zrušit	k5eAaPmNgInP	zrušit
místní	místní	k2eAgInPc1d1	místní
národní	národní	k2eAgInPc1d1	národní
výbory	výbor	k1gInPc1	výbor
a	a	k8xC	a
obě	dva	k4xCgFnPc1	dva
vsi	ves	k1gFnPc1	ves
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
osadami	osada	k1gFnPc7	osada
Obrnic	Obrnice	k1gFnPc2	Obrnice
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
začala	začít	k5eAaPmAgFnS	začít
přestavba	přestavba	k1gFnSc1	přestavba
železniční	železniční	k2eAgFnSc2d1	železniční
spojky	spojka	k1gFnSc2	spojka
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
Most-Bílina	Most-Bílin	k2eAgFnSc1d1	Most-Bílin
a	a	k8xC	a
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
související	související	k2eAgFnSc1d1	související
regulace	regulace	k1gFnSc1	regulace
řeky	řeka	k1gFnSc2	řeka
Bíliny	Bílina	k1gFnSc2	Bílina
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nedocházelo	docházet	k5eNaImAgNnS	docházet
k	k	k7c3	k
záplavám	záplava	k1gFnPc3	záplava
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
každoročně	každoročně	k6eAd1	každoročně
ohrožovaly	ohrožovat	k5eAaImAgFnP	ohrožovat
obec	obec	k1gFnSc4	obec
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
regulaci	regulace	k1gFnSc3	regulace
Srpiny	Srpina	k1gFnSc2	Srpina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
zahájily	zahájit	k5eAaPmAgInP	zahájit
v	v	k7c6	v
Obrnicích	Obrnik	k1gInPc6	Obrnik
provoz	provoz	k1gInSc4	provoz
Severočeské	severočeský	k2eAgInPc4d1	severočeský
keramické	keramický	k2eAgInPc4d1	keramický
závody	závod	k1gInPc4	závod
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
závod	závod	k1gInSc1	závod
podniku	podnik	k1gInSc2	podnik
KERAMOST	KERAMOST	kA	KERAMOST
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
zde	zde	k6eAd1	zde
těží	těžet	k5eAaImIp3nP	těžet
a	a	k8xC	a
zpracovávají	zpracovávat	k5eAaImIp3nP	zpracovávat
bentonit	bentonit	k5eAaPmF	bentonit
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
monopolní	monopolní	k2eAgFnSc4d1	monopolní
produkci	produkce	k1gFnSc4	produkce
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
tehdejšího	tehdejší	k2eAgNnSc2d1	tehdejší
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Obrnice	Obrnice	k1gFnPc1	Obrnice
byly	být	k5eAaImAgFnP	být
plánovány	plánovat	k5eAaImNgFnP	plánovat
jako	jako	k8xS	jako
středisková	střediskový	k2eAgFnSc1d1	středisková
obec	obec	k1gFnSc1	obec
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
projevilo	projevit	k5eAaPmAgNnS	projevit
v	v	k7c6	v
rozsáhlé	rozsáhlý	k2eAgFnSc6d1	rozsáhlá
výstavbě	výstavba	k1gFnSc6	výstavba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
byla	být	k5eAaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
nová	nový	k2eAgFnSc1d1	nová
budova	budova	k1gFnSc1	budova
základní	základní	k2eAgFnSc2d1	základní
školy	škola	k1gFnSc2	škola
a	a	k8xC	a
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
začala	začít	k5eAaPmAgFnS	začít
výstavba	výstavba	k1gFnSc1	výstavba
panelového	panelový	k2eAgNnSc2d1	panelové
sídliště	sídliště	k1gNnSc2	sídliště
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
pracovníky	pracovník	k1gMnPc4	pracovník
okolních	okolní	k2eAgInPc2d1	okolní
závodů	závod	k1gInPc2	závod
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
budovaly	budovat	k5eAaImAgInP	budovat
mateřská	mateřský	k2eAgFnSc1d1	mateřská
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
jesle	jesle	k1gFnPc1	jesle
<g/>
,	,	kIx,	,
obchodní	obchodní	k2eAgNnSc1d1	obchodní
středisko	středisko	k1gNnSc1	středisko
Zlatník	zlatník	k1gInSc1	zlatník
<g/>
,	,	kIx,	,
zdravotní	zdravotní	k2eAgNnSc4d1	zdravotní
středisko	středisko	k1gNnSc4	středisko
<g/>
,	,	kIx,	,
kino	kino	k1gNnSc4	kino
a	a	k8xC	a
restaurace	restaurace	k1gFnPc4	restaurace
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
výstavba	výstavba	k1gFnSc1	výstavba
zásadně	zásadně	k6eAd1	zásadně
změnila	změnit	k5eAaPmAgFnS	změnit
ráz	ráz	k1gInSc4	ráz
obce	obec	k1gFnSc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc1d1	původní
vesnická	vesnický	k2eAgFnSc1d1	vesnická
zástavba	zástavba	k1gFnSc1	zástavba
byla	být	k5eAaImAgFnS	být
zčásti	zčásti	k6eAd1	zčásti
vybourána	vybourat	k5eAaPmNgFnS	vybourat
a	a	k8xC	a
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
novými	nový	k2eAgInPc7d1	nový
panelovými	panelový	k2eAgInPc7d1	panelový
objekty	objekt	k1gInPc7	objekt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
Obrnicích	Obrnice	k1gFnPc6	Obrnice
také	také	k9	také
obalovna	obalovna	k1gFnSc1	obalovna
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
asfaltobetonové	asfaltobetonový	k2eAgFnSc2d1	asfaltobetonová
směsi	směs	k1gFnSc2	směs
pro	pro	k7c4	pro
povrch	povrch	k1gInSc4	povrch
vozovek	vozovka	k1gFnPc2	vozovka
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
sídlo	sídlo	k1gNnSc1	sídlo
společnost	společnost	k1gFnSc1	společnost
Eduard	Eduard	k1gMnSc1	Eduard
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
plastikových	plastikový	k2eAgInPc2d1	plastikový
modelů	model	k1gInPc2	model
a	a	k8xC	a
doplňků	doplněk	k1gInPc2	doplněk
zaměstnává	zaměstnávat	k5eAaImIp3nS	zaměstnávat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
sto	sto	k4xCgNnSc1	sto
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obecní	obecní	k2eAgInPc4d1	obecní
symboly	symbol	k1gInPc4	symbol
==	==	k?	==
</s>
</p>
<p>
<s>
Obrnice	Obrnika	k1gFnSc3	Obrnika
získaly	získat	k5eAaPmAgInP	získat
právo	právo	k1gNnSc4	právo
užívat	užívat	k5eAaImF	užívat
obecní	obecní	k2eAgInSc4d1	obecní
znak	znak	k1gInSc4	znak
a	a	k8xC	a
vlajku	vlajka	k1gFnSc4	vlajka
na	na	k7c6	na
základě	základ	k1gInSc6	základ
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
předsedy	předseda	k1gMnSc2	předseda
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
č.	č.	k?	č.
41	[number]	k4	41
ze	z	k7c2	z
dne	den	k1gInSc2	den
26	[number]	k4	26
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Znak	znak	k1gInSc4	znak
===	===	k?	===
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
stříbrném	stříbrný	k2eAgInSc6d1	stříbrný
štítě	štít	k1gInSc6	štít
nad	nad	k7c7	nad
sníženým	snížený	k2eAgInSc7d1	snížený
modrým	modrý	k2eAgInSc7d1	modrý
ondřejským	ondřejský	k2eAgInSc7d1	ondřejský
křížem	kříž	k1gInSc7	kříž
modrý	modrý	k2eAgInSc1d1	modrý
vinný	vinný	k2eAgInSc1d1	vinný
hrozen	hrozen	k1gInSc1	hrozen
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
zelenými	zelený	k2eAgInPc7d1	zelený
listy	list	k1gInPc7	list
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vlajka	vlajka	k1gFnSc1	vlajka
===	===	k?	===
</s>
</p>
<p>
<s>
Bílý	bílý	k2eAgInSc1d1	bílý
list	list	k1gInSc1	list
s	s	k7c7	s
modrým	modrý	k2eAgInSc7d1	modrý
ondřejským	ondřejský	k2eAgInSc7d1	ondřejský
křížem	kříž	k1gInSc7	kříž
s	s	k7c7	s
rameny	rameno	k1gNnPc7	rameno
širokými	široký	k2eAgNnPc7d1	široké
jednu	jeden	k4xCgFnSc4	jeden
osminu	osmina	k1gFnSc4	osmina
šířky	šířka	k1gFnSc2	šířka
listu	list	k1gInSc2	list
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
osy	osa	k1gFnPc1	osa
vycházejí	vycházet	k5eAaImIp3nP	vycházet
z	z	k7c2	z
poloviny	polovina	k1gFnSc2	polovina
žerďového	žerďový	k2eAgInSc2d1	žerďový
a	a	k8xC	a
vlajícího	vlající	k2eAgInSc2d1	vlající
okraje	okraj	k1gInSc2	okraj
a	a	k8xC	a
z	z	k7c2	z
dolního	dolní	k2eAgInSc2d1	dolní
rohu	roh	k1gInSc2	roh
a	a	k8xC	a
cípu	cíp	k1gInSc2	cíp
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
křížem	kříž	k1gInSc7	kříž
modrý	modrý	k2eAgInSc1d1	modrý
vinný	vinný	k2eAgInSc1d1	vinný
hrozen	hrozen	k1gInSc1	hrozen
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
zelenými	zelený	k2eAgInPc7d1	zelený
listy	list	k1gInPc7	list
<g/>
.	.	kIx.	.
</s>
<s>
Poměr	poměr	k1gInSc1	poměr
šířky	šířka	k1gFnSc2	šířka
k	k	k7c3	k
délce	délka	k1gFnSc3	délka
listu	list	k1gInSc2	list
je	být	k5eAaImIp3nS	být
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ondřejský	ondřejský	k2eAgInSc1d1	ondřejský
kříž	kříž	k1gInSc1	kříž
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
křižovatku	křižovatka	k1gFnSc4	křižovatka
železničních	železniční	k2eAgFnPc2d1	železniční
tratí	trať	k1gFnPc2	trať
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
výstavba	výstavba	k1gFnSc1	výstavba
podnítila	podnítit	k5eAaPmAgFnS	podnítit
rozvoj	rozvoj	k1gInSc4	rozvoj
obce	obec	k1gFnSc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
Vinný	vinný	k2eAgInSc1d1	vinný
hrozen	hrozen	k1gInSc1	hrozen
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
nejstaršího	starý	k2eAgMnSc4d3	nejstarší
známého	známý	k2eAgMnSc4d1	známý
majitele	majitel	k1gMnSc4	majitel
obce	obec	k1gFnSc2	obec
cisterciácký	cisterciácký	k2eAgInSc1d1	cisterciácký
klášter	klášter	k1gInSc1	klášter
v	v	k7c6	v
Oseku	Osek	k1gInSc6	Osek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
měl	mít	k5eAaImAgInS	mít
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
vinice	vinice	k1gFnSc2	vinice
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgInPc1	tři
listy	list	k1gInPc1	list
vinné	vinný	k2eAgFnSc2d1	vinná
révy	réva	k1gFnSc2	réva
symbolizují	symbolizovat	k5eAaImIp3nP	symbolizovat
Obrnice	Obrnice	k1gFnPc1	Obrnice
a	a	k8xC	a
její	její	k3xOp3gFnPc1	její
dvě	dva	k4xCgFnPc1	dva
osady	osada	k1gFnPc1	osada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Seznam	seznam	k1gInSc1	seznam
ulic	ulice	k1gFnPc2	ulice
v	v	k7c6	v
Obrnicích	Obrnice	k1gFnPc6	Obrnice
==	==	k?	==
</s>
</p>
<p>
<s>
náměstí	náměstí	k1gNnSc1	náměstí
5	[number]	k4	5
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
</s>
</p>
<p>
<s>
Dukelská	dukelský	k2eAgFnSc1d1	Dukelská
</s>
</p>
<p>
<s>
K	k	k7c3	k
Vinici	vinice	k1gFnSc3	vinice
</s>
</p>
<p>
<s>
Mírová	mírový	k2eAgFnSc1d1	mírová
</s>
</p>
<p>
<s>
Nádražní	nádražní	k2eAgFnSc7d1	nádražní
</s>
</p>
<p>
<s>
Nová	nový	k2eAgFnSc1d1	nová
výstavba	výstavba	k1gFnSc1	výstavba
</s>
</p>
<p>
<s>
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Pamětihodnosti	pamětihodnost	k1gFnSc6	pamětihodnost
==	==	k?	==
</s>
</p>
<p>
<s>
Kaple	kaple	k1gFnSc1	kaple
Nejsvětější	nejsvětější	k2eAgFnSc2d1	nejsvětější
Trojice	trojice	k1gFnSc2	trojice
na	na	k7c6	na
návsi	náves	k1gFnSc6	náves
<g/>
,	,	kIx,	,
restaurovaná	restaurovaný	k2eAgFnSc1d1	restaurovaná
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2007-2008	[number]	k4	2007-2008
</s>
</p>
<p>
<s>
Sousoší	sousoší	k1gNnSc1	sousoší
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
a	a	k8xC	a
sv.	sv.	kA	sv.
Antonína	Antonín	k1gMnSc2	Antonín
Paduánského	paduánský	k2eAgMnSc2d1	paduánský
u	u	k7c2	u
kaple	kaple	k1gFnSc2	kaple
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
sousoší	sousoší	k1gNnSc6	sousoší
vedou	vést	k5eAaImIp3nP	vést
Obrnice	Obrnice	k1gFnPc4	Obrnice
soudní	soudní	k2eAgInSc4d1	soudní
spor	spor	k1gInSc4	spor
s	s	k7c7	s
městem	město	k1gNnSc7	město
Horní	horní	k2eAgInSc1d1	horní
Jiřetín	Jiřetín	k1gInSc1	Jiřetín
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
socha	socha	k1gFnSc1	socha
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gInSc7	jeho
majetkem	majetek	k1gInSc7	majetek
a	a	k8xC	a
žádá	žádat	k5eAaImIp3nS	žádat
její	její	k3xOp3gNnSc4	její
vrácení	vrácení	k1gNnSc4	vrácení
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
Obrnice	Obrnice	k1gFnSc1	Obrnice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zaplatila	zaplatit	k5eAaPmAgFnS	zaplatit
její	její	k3xOp3gFnSc4	její
opravu	oprava	k1gFnSc4	oprava
a	a	k8xC	a
na	na	k7c6	na
jejímž	jejíž	k3xOyRp3gInSc6	jejíž
pozemku	pozemek	k1gInSc6	pozemek
stojí	stát	k5eAaImIp3nS	stát
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
však	však	k9	však
brání	bránit	k5eAaImIp3nS	bránit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Obrnice	Obrnice	k1gFnSc2	Obrnice
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Obrnice	Obrnice	k1gFnSc2	Obrnice
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Územně	územně	k6eAd1	územně
identifikační	identifikační	k2eAgInSc1d1	identifikační
registr	registr	k1gInSc1	registr
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
Obrnice	Obrnice	k1gFnSc2	Obrnice
v	v	k7c6	v
Územně	územně	k6eAd1	územně
identifikačním	identifikační	k2eAgInSc6d1	identifikační
registru	registr	k1gInSc6	registr
ČR	ČR	kA	ČR
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Historie	historie	k1gFnSc1	historie
obce	obec	k1gFnSc2	obec
</s>
</p>
<p>
<s>
Kaple	kaple	k1gFnSc1	kaple
v	v	k7c6	v
Obrnicích	Obrnice	k1gFnPc6	Obrnice
</s>
</p>
