<p>
<s>
Mamutové	mamutový	k2eAgNnSc4d1	mamutový
čerpadlo	čerpadlo	k1gNnSc4	čerpadlo
či	či	k8xC	či
mamutkové	mamutkový	k2eAgNnSc4d1	mamutkový
čerpadlo	čerpadlo	k1gNnSc4	čerpadlo
<g/>
,	,	kIx,	,
slangově	slangově	k6eAd1	slangově
zvané	zvaný	k2eAgFnPc1d1	zvaná
mamutka	mamutka	k1gFnSc1	mamutka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc1	druh
hydraulicko-pneumatického	hydraulickoneumatický	k2eAgNnSc2d1	hydraulicko-pneumatický
čerpadla	čerpadlo	k1gNnSc2	čerpadlo
<g/>
.	.	kIx.	.
</s>
<s>
Pracuje	pracovat	k5eAaImIp3nS	pracovat
na	na	k7c6	na
principu	princip	k1gInSc6	princip
spojených	spojený	k2eAgFnPc2d1	spojená
nádob	nádoba	k1gFnPc2	nádoba
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
spojených	spojený	k2eAgFnPc2d1	spojená
nádob	nádoba	k1gFnPc2	nádoba
je	být	k5eAaImIp3nS	být
nádrž	nádrž	k1gFnSc1	nádrž
s	s	k7c7	s
tekutinou	tekutina	k1gFnSc7	tekutina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
čerpá	čerpat	k5eAaImIp3nS	čerpat
<g/>
,	,	kIx,	,
a	a	k8xC	a
druhou	druhý	k4xOgFnSc4	druhý
nádobu	nádoba	k1gFnSc4	nádoba
tvoří	tvořit	k5eAaImIp3nS	tvořit
odtoková	odtokový	k2eAgFnSc1d1	odtoková
trubka	trubka	k1gFnSc1	trubka
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
kapalina	kapalina	k1gFnSc1	kapalina
vytéká	vytékat	k5eAaImIp3nS	vytékat
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
dolní	dolní	k2eAgFnSc2d1	dolní
části	část	k1gFnSc2	část
odtokové	odtokový	k2eAgFnSc2d1	odtoková
trubky	trubka	k1gFnSc2	trubka
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
tlakem	tlak	k1gInSc7	tlak
vhání	vhánět	k5eAaImIp3nS	vhánět
stlačený	stlačený	k2eAgInSc1d1	stlačený
plyn	plyn	k1gInSc1	plyn
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnPc1	jehož
bubliny	bublina	k1gFnPc1	bublina
snižují	snižovat	k5eAaImIp3nP	snižovat
hustotu	hustota	k1gFnSc4	hustota
kapaliny	kapalina	k1gFnSc2	kapalina
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
trubce	trubka	k1gFnSc6	trubka
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
lehčí	lehký	k2eAgFnSc1d2	lehčí
kapalina	kapalina	k1gFnSc1	kapalina
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
tlakem	tlak	k1gInSc7	tlak
druhé	druhý	k4xOgFnSc2	druhý
spojené	spojený	k2eAgFnSc2d1	spojená
nádoby	nádoba	k1gFnSc2	nádoba
vytlačována	vytlačovat	k5eAaImNgFnS	vytlačovat
nahoru	nahoru	k6eAd1	nahoru
a	a	k8xC	a
odtéká	odtékat	k5eAaImIp3nS	odtékat
z	z	k7c2	z
nádrže	nádrž	k1gFnSc2	nádrž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výhodou	výhoda	k1gFnSc7	výhoda
mamutových	mamutový	k2eAgNnPc2d1	mamutový
čerpadel	čerpadlo	k1gNnPc2	čerpadlo
je	být	k5eAaImIp3nS	být
jednoduchost	jednoduchost	k1gFnSc1	jednoduchost
a	a	k8xC	a
minimální	minimální	k2eAgInPc1d1	minimální
nároky	nárok	k1gInPc1	nárok
na	na	k7c4	na
údržbu	údržba	k1gFnSc4	údržba
částí	část	k1gFnPc2	část
ponořených	ponořený	k2eAgFnPc2d1	ponořená
pod	pod	k7c7	pod
hladinou	hladina	k1gFnSc7	hladina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
neobsahují	obsahovat	k5eNaImIp3nP	obsahovat
žádné	žádný	k3yNgFnPc1	žádný
pohyblivé	pohyblivý	k2eAgFnPc1d1	pohyblivá
části	část	k1gFnPc1	část
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
často	často	k6eAd1	často
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
přečerpávání	přečerpávání	k1gNnSc3	přečerpávání
kalů	kal	k1gInPc2	kal
a	a	k8xC	a
odpadních	odpadní	k2eAgFnPc2d1	odpadní
kapalin	kapalina	k1gFnPc2	kapalina
<g/>
.	.	kIx.	.
</s>
<s>
Čerpadlo	čerpadlo	k1gNnSc1	čerpadlo
také	také	k9	také
neohrozí	ohrozit	k5eNaPmIp3nS	ohrozit
předměty	předmět	k1gInPc4	předmět
plovoucí	plovoucí	k2eAgInPc4d1	plovoucí
v	v	k7c6	v
kapalině	kapalina	k1gFnSc6	kapalina
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
ho	on	k3xPp3gInSc4	on
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
k	k	k7c3	k
čerpání	čerpání	k1gNnSc3	čerpání
vody	voda	k1gFnSc2	voda
obsahující	obsahující	k2eAgFnSc2d1	obsahující
živé	živá	k1gFnSc2	živá
organismy	organismus	k1gInPc1	organismus
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
nemají	mít	k5eNaImIp3nP	mít
být	být	k5eAaImF	být
poškozeny	poškozen	k2eAgInPc1d1	poškozen
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
při	při	k7c6	při
odběru	odběr	k1gInSc6	odběr
biologických	biologický	k2eAgInPc2d1	biologický
vzorků	vzorek	k1gInPc2	vzorek
<g/>
.	.	kIx.	.
</s>
<s>
Nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
je	být	k5eAaImIp3nS	být
malá	malý	k2eAgFnSc1d1	malá
účinnost	účinnost	k1gFnSc1	účinnost
20	[number]	k4	20
až	až	k9	až
40	[number]	k4	40
procent	procento	k1gNnPc2	procento
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
slabé	slabý	k2eAgNnSc1d1	slabé
sání	sání	k1gNnSc1	sání
čerpadla	čerpadlo	k1gNnSc2	čerpadlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgNnSc1	první
čerpadlo	čerpadlo	k1gNnSc1	čerpadlo
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
principu	princip	k1gInSc6	princip
zkonstruoval	zkonstruovat	k5eAaPmAgMnS	zkonstruovat
německý	německý	k2eAgMnSc1d1	německý
konstruktér	konstruktér	k1gMnSc1	konstruktér
a	a	k8xC	a
vynálezce	vynálezce	k1gMnSc1	vynálezce
Carl	Carl	k1gMnSc1	Carl
Immanuel	Immanuel	k1gMnSc1	Immanuel
Löscher	Löschra	k1gFnPc2	Löschra
(	(	kIx(	(
<g/>
1750	[number]	k4	1750
<g/>
–	–	k?	–
<g/>
1813	[number]	k4	1813
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1797	[number]	k4	1797
<g/>
,	,	kIx,	,
v	v	k7c6	v
německojazyčné	německojazyčný	k2eAgFnSc6d1	německojazyčná
oblasti	oblast	k1gFnSc6	oblast
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
čerpadlo	čerpadlo	k1gNnSc1	čerpadlo
dříve	dříve	k6eAd2	dříve
nazývalo	nazývat	k5eAaImAgNnS	nazývat
Löscherpumpe	Löscherpump	k1gInSc5	Löscherpump
(	(	kIx(	(
<g/>
Löscherovo	Löscherův	k2eAgNnSc4d1	Löscherův
čerpadlo	čerpadlo	k1gNnSc4	čerpadlo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
