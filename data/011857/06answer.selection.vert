<s>
Výhodou	výhoda	k1gFnSc7
mamutových	mamutový	k2eAgNnPc2d1
čerpadel	čerpadlo	k1gNnPc2
je	být	k5eAaImIp3nS
jednoduchost	jednoduchost	k1gFnSc1
a	a	k8xC
minimální	minimální	k2eAgInPc1d1
nároky	nárok	k1gInPc1
na	na	k7c4
údržbu	údržba	k1gFnSc4
částí	část	k1gFnPc2
ponořených	ponořený	k2eAgFnPc2d1
pod	pod	k7c7
hladinou	hladina	k1gFnSc7
<g/>
,	,	kIx,
které	který	k3yRgFnPc1
neobsahují	obsahovat	k5eNaImIp3nP
žádné	žádný	k3yNgFnPc1
pohyblivé	pohyblivý	k2eAgFnPc1d1
části	část	k1gFnPc1
<g/>
.	.	kIx.
</s>