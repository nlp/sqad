<s>
Přibližně	přibližně	k6eAd1	přibližně
polovina	polovina	k1gFnSc1	polovina
obyvatel	obyvatel	k1gMnPc2	obyvatel
Egypta	Egypt	k1gInSc2	Egypt
žije	žít	k5eAaImIp3nS	žít
ve	v	k7c6	v
městech	město	k1gNnPc6	město
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
v	v	k7c6	v
hustě	hustě	k6eAd1	hustě
obydlených	obydlený	k2eAgNnPc6d1	obydlené
centrech	centrum	k1gNnPc6	centrum
Káhiry	Káhira	k1gFnSc2	Káhira
<g/>
,	,	kIx,	,
Alexandrie	Alexandrie	k1gFnSc2	Alexandrie
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
velkých	velký	k2eAgNnPc2d1	velké
měst	město	k1gNnPc2	město
Nilské	nilský	k2eAgFnSc2d1	nilská
delty	delta	k1gFnSc2	delta
<g/>
.	.	kIx.	.
</s>
