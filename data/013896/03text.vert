<s>
Spirit	Spirit	k1gInSc1
(	(	kIx(
<g/>
film	film	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Spirit	Spirit	k1gInSc1
Původní	původní	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
The	The	k?
Spirit	Spirit	k1gInSc1
Země	zem	k1gFnSc2
</s>
<s>
USA	USA	kA
Jazyk	jazyk	k1gInSc1
</s>
<s>
angličtina	angličtina	k1gFnSc1
Délka	délka	k1gFnSc1
</s>
<s>
103	#num#	k4
minut	minuta	k1gFnPc2
Žánry	žánr	k1gInPc1
</s>
<s>
akční	akční	k2eAgMnSc1d1
<g/>
,	,	kIx,
komedie	komedie	k1gFnSc1
<g/>
,	,	kIx,
krimi	krimi	k1gNnSc1
<g/>
,	,	kIx,
fantasy	fantas	k1gInPc1
Předloha	předloha	k1gFnSc1
</s>
<s>
Spirit	Spirit	k1gInSc1
Námět	námět	k1gInSc1
</s>
<s>
Will	Will	k1gInSc1
Eisner	Eisnra	k1gFnPc2
Scénář	scénář	k1gInSc1
a	a	k8xC
režie	režie	k1gFnSc1
</s>
<s>
Frank	Frank	k1gMnSc1
Miller	Miller	k1gMnSc1
Obsazení	obsazení	k1gNnPc2
a	a	k8xC
filmový	filmový	k2eAgInSc1d1
štáb	štáb	k1gInSc1
Hlavní	hlavní	k2eAgFnSc1d1
role	role	k1gFnSc1
</s>
<s>
Gabriel	Gabriel	k1gMnSc1
MachtSamuel	MachtSamuel	k1gMnSc1
L.	L.	kA
JacksonScarlett	JacksonScarlett	k1gMnSc1
JohanssonEva	JohanssonEv	k1gMnSc2
MendesSarah	MendesSarah	k1gMnSc1
Paulson	Paulson	k1gMnSc1
Produkce	produkce	k1gFnSc1
</s>
<s>
Deborah	Deborah	k1gMnSc1
Del	Del	k1gMnSc1
PreteGigi	PreteGig	k1gFnSc2
PritzkerMichael	PritzkerMichael	k1gMnSc1
E.	E.	kA
Uslan	Uslan	k1gInSc1
Hudba	hudba	k1gFnSc1
</s>
<s>
David	David	k1gMnSc1
Newman	Newman	k1gMnSc1
Kamera	kamera	k1gFnSc1
</s>
<s>
Bill	Bill	k1gMnSc1
Pope	pop	k1gMnSc5
Kostýmy	kostým	k1gInPc1
</s>
<s>
Michael	Michael	k1gMnSc1
Dennison	Dennison	k1gMnSc1
Střih	střih	k1gInSc4
</s>
<s>
Gregory	Gregor	k1gMnPc4
Nussbaum	Nussbaum	k1gInSc1
Zvuk	zvuk	k1gInSc4
</s>
<s>
David	David	k1gMnSc1
J.	J.	kA
Brownlow	Brownlow	k1gMnSc1
Architekt	architekt	k1gMnSc1
</s>
<s>
Rosario	Rosario	k6eAd1
Provenza	Provenza	k1gFnSc1
Výroba	výroba	k1gFnSc1
a	a	k8xC
distribuce	distribuce	k1gFnSc1
Premiéra	premiéra	k1gFnSc1
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
prosinec	prosinec	k1gInSc1
2008	#num#	k4
Produkční	produkční	k2eAgFnSc4d1
společnost	společnost	k1gFnSc4
</s>
<s>
OddLot	OddLot	k1gMnSc1
Entertainment	Entertainment	k1gMnSc1
Distribuce	distribuce	k1gFnSc1
</s>
<s>
Lionsgate	Lionsgat	k1gMnSc5
Rozpočet	rozpočet	k1gInSc1
</s>
<s>
60	#num#	k4
miliónů	milión	k4xCgInPc2
dolarů	dolar	k1gInPc2
Tržby	tržba	k1gFnSc2
</s>
<s>
39	#num#	k4
031	#num#	k4
337	#num#	k4
dolarů	dolar	k1gInPc2
Spirit	Spirita	k1gFnPc2
na	na	k7c6
ČSFD	ČSFD	kA
<g/>
,	,	kIx,
FDb	FDb	k1gFnPc1
<g/>
,	,	kIx,
IMDbNěkterá	IMDbNěkterý	k2eAgNnPc1d1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Spirit	Spirit	k1gInSc1
(	(	kIx(
<g/>
v	v	k7c6
americkém	americký	k2eAgInSc6d1
originále	originál	k1gInSc6
<g/>
:	:	kIx,
The	The	k1gFnSc1
Spirit	Spirit	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
americký	americký	k2eAgInSc1d1
akční	akční	k2eAgInSc1d1
film	film	k1gInSc1
z	z	k7c2
roku	rok	k1gInSc2
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Režisérem	režisér	k1gMnSc7
filmu	film	k1gInSc2
je	být	k5eAaImIp3nS
Frank	Frank	k1gMnSc1
Miller	Miller	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgFnSc1d1
role	role	k1gFnSc1
ve	v	k7c6
filmu	film	k1gInSc6
ztvárnili	ztvárnit	k5eAaPmAgMnP
Gabriel	Gabriel	k1gMnSc1
Macht	Macht	k1gMnSc1
<g/>
,	,	kIx,
Samuel	Samuel	k1gMnSc1
L.	L.	kA
Jackson	Jackson	k1gMnSc1
<g/>
,	,	kIx,
Scarlett	Scarlett	k1gMnSc1
Johansson	Johansson	k1gMnSc1
<g/>
,	,	kIx,
Eva	Eva	k1gFnSc1
Mendes	Mendes	k1gMnSc1
a	a	k8xC
Sarah	Sarah	k1gFnSc1
Paulson	Paulsona	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Obsazení	obsazení	k1gNnSc1
</s>
<s>
Gabriel	Gabriel	k1gMnSc1
Macht	Macht	k1gMnSc1
</s>
<s>
Denny	Denn	k1gInPc1
Colt	Colta	k1gFnPc2
<g/>
/	/	kIx~
<g/>
Spirit	Spirit	k1gInSc1
</s>
<s>
Samuel	Samuel	k1gMnSc1
L.	L.	kA
Jackson	Jackson	k1gMnSc1
</s>
<s>
Octopus	Octopus	k1gMnSc1
</s>
<s>
Scarlett	Scarlett	k1gMnSc1
Johansson	Johansson	k1gMnSc1
</s>
<s>
Silken	Silken	k2eAgInSc1d1
Floss	Floss	k1gInSc1
</s>
<s>
Eva	Eva	k1gFnSc1
Mendes	Mendesa	k1gFnPc2
</s>
<s>
Sand	Sand	k1gMnSc1
Saref	Saref	k1gMnSc1
</s>
<s>
Sarah	Sarah	k1gFnSc1
Paulson	Paulsona	k1gFnPc2
</s>
<s>
Ellen	Ellen	k2eAgMnSc1d1
Dolan	dolan	k1gMnSc1
</s>
<s>
Seychelle	Seychelle	k6eAd1
Gabriel	Gabriel	k1gMnSc1
</s>
<s>
mladá	mladá	k1gFnSc1
Sand	Sanda	k1gFnPc2
</s>
<s>
Dan	Dan	k1gMnSc1
Lauria	Laurium	k1gNnSc2
</s>
<s>
Eustace	Eustace	k1gFnSc1
Dolan	Dolany	k1gInPc2
</s>
<s>
Stana	Stana	k1gFnSc1
Katic	Katice	k1gFnPc2
</s>
<s>
Morgenstern	Morgenstern	k1gMnSc1
</s>
<s>
Jaime	Jaimat	k5eAaPmIp3nS
King	King	k1gMnSc1
</s>
<s>
Lorelei	Lorelei	k6eAd1
</s>
<s>
Paz	Paz	k?
Vega	Vega	k1gFnSc1
</s>
<s>
Plaster	Plaster	k1gInSc1
z	z	k7c2
Paříže	Paříž	k1gFnSc2
</s>
<s>
Reakce	reakce	k1gFnSc1
</s>
<s>
aktuální	aktuální	k2eAgInPc1d1
k	k	k7c3
18	#num#	k4
<g/>
.	.	kIx.
prosinci	prosinec	k1gInSc3
2014	#num#	k4
</s>
<s>
csfd	csfd	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
:	:	kIx,
</s>
<s>
imdb	imdb	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
:	:	kIx,
</s>
<s>
fdb	fdb	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
:	:	kIx,
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Spirit	Spirit	k1gInSc1
v	v	k7c6
Česko-Slovenské	česko-slovenský	k2eAgFnSc6d1
filmové	filmový	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
</s>
<s>
Spirit	Spirit	k1gInSc1
v	v	k7c4
Internet	Internet	k1gInSc4
Movie	Movie	k1gFnSc2
Database	Databasa	k1gFnSc3
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Spirit	Spirit	k1gInSc1
(	(	kIx(
<g/>
film	film	k1gInSc1
<g/>
)	)	kIx)
ve	v	k7c6
Filmové	filmový	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Film	film	k1gInSc1
</s>
