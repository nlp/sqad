<s>
Harry	Harr	k1gInPc1	Harr
Potter	Pottra	k1gFnPc2	Pottra
a	a	k8xC	a
Ohnivý	ohnivý	k2eAgInSc4d1	ohnivý
pohár	pohár	k1gInSc4	pohár
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Harry	Harr	k1gInPc4	Harr
Potter	Pottrum	k1gNnPc2	Pottrum
and	and	k?	and
the	the	k?	the
Goblet	Goblet	k1gInSc1	Goblet
of	of	k?	of
Fire	Fire	k1gInSc1	Fire
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
čtvrtá	čtvrtý	k4xOgFnSc1	čtvrtý
kniha	kniha	k1gFnSc1	kniha
ze	z	k7c2	z
série	série	k1gFnSc2	série
Harry	Harra	k1gMnSc2	Harra
Potter	Pottra	k1gFnPc2	Pottra
od	od	k7c2	od
spisovatelky	spisovatelka	k1gFnSc2	spisovatelka
J.	J.	kA	J.
K.	K.	kA	K.
Rowlingové	Rowlingový	k2eAgFnPc4d1	Rowlingová
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc1	Harra
Potter	Pottra	k1gFnPc2	Pottra
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
dostane	dostat	k5eAaPmIp3nS	dostat
od	od	k7c2	od
svých	svůj	k3xOyFgFnPc2	svůj
neoblíbených	oblíbený	k2eNgFnPc2d1	neoblíbená
příbuzných	příbuzný	k2eAgFnPc2d1	příbuzná
Dursleyových	Dursleyová	k1gFnPc2	Dursleyová
<g/>
,	,	kIx,	,
tráví	trávit	k5eAaImIp3nP	trávit
prázdniny	prázdniny	k1gFnPc4	prázdniny
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
Weasleyových	Weasleyových	k2eAgFnSc7d1	Weasleyových
a	a	k8xC	a
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
nejlepší	dobrý	k2eAgFnSc7d3	nejlepší
kamarádkou	kamarádka	k1gFnSc7	kamarádka
Hermionou	Hermiona	k1gFnSc7	Hermiona
Grangerovou	Grangerová	k1gFnSc7	Grangerová
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yRgInPc7	který
se	se	k3xPyFc4	se
vydává	vydávat	k5eAaPmIp3nS	vydávat
na	na	k7c6	na
finále	finále	k1gNnSc6	finále
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
famfrpálu	famfrpál	k1gInSc6	famfrpál
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
napínavém	napínavý	k2eAgInSc6d1	napínavý
zápase	zápas	k1gInSc6	zápas
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
nakonec	nakonec	k6eAd1	nakonec
vyhraje	vyhrát	k5eAaPmIp3nS	vyhrát
Irsko	Irsko	k1gNnSc1	Irsko
<g/>
,	,	kIx,	,
na	na	k7c4	na
mudlovský	mudlovský	k2eAgInSc4d1	mudlovský
kemp	kemp	k1gInSc4	kemp
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
Harry	Harr	k1gInPc4	Harr
s	s	k7c7	s
Hermionou	Hermiona	k1gFnSc7	Hermiona
a	a	k8xC	a
Weasleyovými	Weasleyová	k1gFnPc7	Weasleyová
ubytovaný	ubytovaný	k2eAgInSc1d1	ubytovaný
<g/>
,	,	kIx,	,
zaútočí	zaútočit	k5eAaPmIp3nP	zaútočit
Smrtijedi	Smrtijed	k1gMnPc1	Smrtijed
<g/>
,	,	kIx,	,
přívrženci	přívrženec	k1gMnPc1	přívrženec
lorda	lord	k1gMnSc2	lord
Voldemorta	Voldemort	k1gMnSc2	Voldemort
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
někdo	někdo	k3yInSc1	někdo
poblíž	poblíž	k7c2	poblíž
Harryho	Harry	k1gMnSc2	Harry
s	s	k7c7	s
Ronem	Ron	k1gMnSc7	Ron
a	a	k8xC	a
Hermionou	Hermiona	k1gFnSc7	Hermiona
vyčaruje	vyčarovat	k5eAaPmIp3nS	vyčarovat
na	na	k7c4	na
oblohu	obloha	k1gFnSc4	obloha
Harryho	Harry	k1gMnSc2	Harry
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
hůlkou	hůlka	k1gFnSc7	hůlka
znamení	znamení	k1gNnSc2	znamení
zla	zlo	k1gNnSc2	zlo
<g/>
,	,	kIx,	,
Voldemortův	Voldemortův	k2eAgInSc1d1	Voldemortův
znak	znak	k1gInSc1	znak
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
Bradavic	bradavice	k1gFnPc2	bradavice
Brumbál	brumbál	k1gMnSc1	brumbál
představí	představit	k5eAaPmIp3nS	představit
žákům	žák	k1gMnPc3	žák
nového	nový	k2eAgMnSc2d1	nový
učitele	učitel	k1gMnSc2	učitel
obrany	obrana	k1gFnSc2	obrana
proti	proti	k7c3	proti
černé	černý	k2eAgFnSc3d1	černá
magii	magie	k1gFnSc3	magie
<g/>
,	,	kIx,	,
Alastora	Alastor	k1gMnSc2	Alastor
"	"	kIx"	"
<g/>
Pošuka	pošuk	k1gMnSc2	pošuk
<g/>
"	"	kIx"	"
Moodyho	Moody	k1gMnSc2	Moody
<g/>
,	,	kIx,	,
bývalého	bývalý	k2eAgMnSc2d1	bývalý
bystrozora	bystrozor	k1gMnSc2	bystrozor
<g/>
,	,	kIx,	,
a	a	k8xC	a
oznámí	oznámit	k5eAaPmIp3nS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
konat	konat	k5eAaImF	konat
Turnaj	turnaj	k1gInSc4	turnaj
tří	tři	k4xCgFnPc2	tři
kouzelnických	kouzelnický	k2eAgFnPc2d1	kouzelnická
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yIgInSc4	který
přijedou	přijet	k5eAaPmIp3nP	přijet
do	do	k7c2	do
Bradavic	bradavice	k1gFnPc2	bradavice
studenti	student	k1gMnPc1	student
z	z	k7c2	z
dalších	další	k2eAgFnPc2d1	další
kouzelnických	kouzelnický	k2eAgFnPc2d1	kouzelnická
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
Kruvalu	Kruval	k1gInSc2	Kruval
a	a	k8xC	a
Krásnohůlek	Krásnohůlka	k1gFnPc2	Krásnohůlka
<g/>
.	.	kIx.	.
</s>
<s>
Ohnivý	ohnivý	k2eAgInSc4d1	ohnivý
pohár	pohár	k1gInSc4	pohár
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vybírá	vybírat	k5eAaImIp3nS	vybírat
účastníky	účastník	k1gMnPc4	účastník
<g/>
,	,	kIx,	,
pošle	poslat	k5eAaPmIp3nS	poslat
do	do	k7c2	do
turnaje	turnaj	k1gInSc2	turnaj
Viktora	Viktor	k1gMnSc2	Viktor
Kruma	Krum	k1gMnSc2	Krum
za	za	k7c4	za
Kruval	Kruval	k1gFnSc4	Kruval
<g/>
,	,	kIx,	,
Fleur	Fleur	k1gMnSc1	Fleur
Delacourovou	Delacourův	k2eAgFnSc7d1	Delacourův
za	za	k7c4	za
Krásnohůlky	Krásnohůlek	k1gMnPc4	Krásnohůlek
a	a	k8xC	a
Cedrika	Cedrik	k1gMnSc4	Cedrik
Diggoryho	Diggory	k1gMnSc4	Diggory
za	za	k7c2	za
Bradavice	bradavice	k1gFnSc2	bradavice
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
však	však	k9	však
pohár	pohár	k1gInSc1	pohár
vybere	vybrat	k5eAaPmIp3nS	vybrat
Harryho	Harry	k1gMnSc4	Harry
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
sám	sám	k3xTgMnSc1	sám
nepřihlásil	přihlásit	k5eNaPmAgMnS	přihlásit
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
nemohl	moct	k5eNaImAgMnS	moct
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ještě	ještě	k6eAd1	ještě
nedosáhl	dosáhnout	k5eNaPmAgInS	dosáhnout
17	[number]	k4	17
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc4	Harra
absolvuje	absolvovat	k5eAaPmIp3nS	absolvovat
během	během	k7c2	během
turnaje	turnaj	k1gInSc2	turnaj
tří	tři	k4xCgFnPc2	tři
kouzelnických	kouzelnický	k2eAgFnPc2d1	kouzelnická
škol	škola	k1gFnPc2	škola
tři	tři	k4xCgInPc4	tři
úkoly	úkol	k1gInPc4	úkol
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
úkolu	úkol	k1gInSc6	úkol
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mají	mít	k5eAaImIp3nP	mít
drakovi	drakův	k2eAgMnPc1d1	drakův
sebrat	sebrat	k5eAaPmF	sebrat
zlaté	zlatý	k2eAgNnSc4d1	Zlaté
vejce	vejce	k1gNnSc4	vejce
<g/>
,	,	kIx,	,
vyhraje	vyhrát	k5eAaPmIp3nS	vyhrát
společně	společně	k6eAd1	společně
s	s	k7c7	s
Viktorem	Viktor	k1gMnSc7	Viktor
Krumem	Krum	k1gMnSc7	Krum
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhém	druhý	k4xOgInSc6	druhý
úkolu	úkol	k1gInSc6	úkol
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
má	mít	k5eAaImIp3nS	mít
osvobodit	osvobodit	k5eAaPmF	osvobodit
svého	svůj	k3xOyFgMnSc4	svůj
kamaráda	kamarád	k1gMnSc4	kamarád
Rona	Ron	k1gMnSc4	Ron
od	od	k7c2	od
jezerních	jezerní	k2eAgMnPc2d1	jezerní
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
žijících	žijící	k2eAgMnPc2d1	žijící
na	na	k7c6	na
dně	dno	k1gNnSc6	dno
jezera	jezero	k1gNnSc2	jezero
poblíž	poblíž	k7c2	poblíž
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
s	s	k7c7	s
Cedrikem	Cedrik	k1gMnSc7	Cedrik
o	o	k7c4	o
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Třetím	třetí	k4xOgInSc7	třetí
úkolem	úkol	k1gInSc7	úkol
je	být	k5eAaImIp3nS	být
projít	projít	k5eAaPmF	projít
bludištěm	bludiště	k1gNnSc7	bludiště
s	s	k7c7	s
nástrahami	nástraha	k1gFnPc7	nástraha
a	a	k8xC	a
jako	jako	k8xC	jako
první	první	k4xOgFnSc1	první
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
Ohnivého	ohnivý	k2eAgInSc2d1	ohnivý
poháru	pohár	k1gInSc2	pohár
<g/>
.	.	kIx.	.
</s>
<s>
Cedrik	Cedrik	k1gMnSc1	Cedrik
s	s	k7c7	s
Harrym	Harrym	k1gInSc1	Harrym
vyrazí	vyrazit	k5eAaPmIp3nP	vyrazit
do	do	k7c2	do
bludiště	bludiště	k1gNnSc2	bludiště
jako	jako	k9	jako
první	první	k4xOgFnSc7	první
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
stejně	stejně	k6eAd1	stejně
rychle	rychle	k6eAd1	rychle
projdou	projít	k5eAaPmIp3nP	projít
bludištěm	bludiště	k1gNnSc7	bludiště
k	k	k7c3	k
poháru	pohár	k1gInSc3	pohár
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vzájemné	vzájemný	k2eAgFnSc6d1	vzájemná
výpomoci	výpomoc	k1gFnSc6	výpomoc
se	se	k3xPyFc4	se
dohodnou	dohodnout	k5eAaPmIp3nP	dohodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
ho	on	k3xPp3gNnSc2	on
dotknou	dotknout	k5eAaPmIp3nP	dotknout
najednou	najednou	k6eAd1	najednou
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
oba	dva	k4xCgMnPc1	dva
<g/>
.	.	kIx.	.
</s>
<s>
Pohár	pohár	k1gInSc1	pohár
je	být	k5eAaImIp3nS	být
však	však	k9	však
přenášedlo	přenášedlo	k1gNnSc1	přenášedlo
a	a	k8xC	a
přenese	přenést	k5eAaPmIp3nS	přenést
je	on	k3xPp3gMnPc4	on
na	na	k7c4	na
hřbitov	hřbitov	k1gInSc4	hřbitov
kouzelnické	kouzelnický	k2eAgFnSc2d1	kouzelnická
vesničky	vesnička	k1gFnSc2	vesnička
Malý	Malý	k1gMnSc1	Malý
Visánek	Visánek	k1gMnSc1	Visánek
(	(	kIx(	(
<g/>
bydliště	bydliště	k1gNnSc1	bydliště
Voldemortova	Voldemortův	k2eAgMnSc2d1	Voldemortův
otce	otec	k1gMnSc2	otec
<g/>
)	)	kIx)	)
k	k	k7c3	k
Voldemortovi	Voldemort	k1gMnSc3	Voldemort
a	a	k8xC	a
Červíčkovi	červíček	k1gMnSc3	červíček
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zavraždí	zavraždit	k5eAaPmIp3nS	zavraždit
Cedrika	Cedrik	k1gMnSc4	Cedrik
a	a	k8xC	a
pomůže	pomoct	k5eAaPmIp3nS	pomoct
Voldemortovi	Voldemort	k1gMnSc3	Voldemort
nabýt	nabýt	k5eAaPmF	nabýt
lidské	lidský	k2eAgNnSc4d1	lidské
tělo	tělo	k1gNnSc4	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
chce	chtít	k5eAaImIp3nS	chtít
zabít	zabít	k5eAaPmF	zabít
také	také	k9	také
Harryho	Harry	k1gMnSc4	Harry
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
díky	díky	k7c3	díky
neobvyklému	obvyklý	k2eNgNnSc3d1	neobvyklé
spojení	spojení	k1gNnSc3	spojení
jejich	jejich	k3xOp3gFnPc2	jejich
hůlek	hůlka	k1gFnPc2	hůlka
nepovede	vést	k5eNaImIp3nS	vést
a	a	k8xC	a
Harry	Harr	k1gInPc1	Harr
se	se	k3xPyFc4	se
pomocí	pomocí	k7c2	pomocí
přenášedla	přenášedlo	k1gNnSc2	přenášedlo
dostává	dostávat	k5eAaImIp3nS	dostávat
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Bradavic	bradavice	k1gFnPc2	bradavice
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Brumbálem	brumbál	k1gMnSc7	brumbál
objeví	objevit	k5eAaPmIp3nS	objevit
<g/>
,	,	kIx,	,
že	že	k8xS	že
profesor	profesor	k1gMnSc1	profesor
Moody	Mooda	k1gFnSc2	Mooda
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
mnoholičným	mnoholičný	k2eAgInSc7d1	mnoholičný
lektvarem	lektvar	k1gInSc7	lektvar
přeměněný	přeměněný	k2eAgInSc1d1	přeměněný
Barty	Bart	k1gMnPc7	Bart
Skrk	Skrko	k1gNnPc2	Skrko
ml.	ml.	kA	ml.
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
přemohl	přemoct	k5eAaPmAgInS	přemoct
opravdového	opravdový	k2eAgMnSc4d1	opravdový
Moodyho	Moody	k1gMnSc4	Moody
<g/>
,	,	kIx,	,
dostal	dostat	k5eAaPmAgMnS	dostat
Harryho	Harry	k1gMnSc2	Harry
do	do	k7c2	do
turnaje	turnaj	k1gInSc2	turnaj
a	a	k8xC	a
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
snažil	snažit	k5eAaImAgMnS	snažit
zajistit	zajistit	k5eAaPmF	zajistit
výhru	výhra	k1gFnSc4	výhra
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
konci	konec	k1gInSc6	konec
Voldemortem	Voldemort	k1gInSc7	Voldemort
využit	využít	k5eAaPmNgMnS	využít
a	a	k8xC	a
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
<g/>
.	.	kIx.	.
</s>
