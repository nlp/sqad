<s>
Jan	Jan	k1gMnSc1	Jan
Werich	Werich	k1gMnSc1	Werich
studoval	studovat	k5eAaImAgMnS	studovat
Masarykovo	Masarykův	k2eAgNnSc4d1	Masarykovo
gymnázium	gymnázium	k1gNnSc4	gymnázium
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
v	v	k7c6	v
Křemencově	křemencově	k6eAd1	křemencově
ulici	ulice	k1gFnSc6	ulice
(	(	kIx(	(
<g/>
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
stejném	stejný	k2eAgNnSc6d1	stejné
místě	místo	k1gNnSc6	místo
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
střední	střední	k2eAgFnSc1d1	střední
škola	škola	k1gFnSc1	škola
chemická	chemický	k2eAgFnSc1d1	chemická
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
také	také	k9	také
seznámil	seznámit	k5eAaPmAgMnS	seznámit
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
pozdějším	pozdní	k2eAgMnSc7d2	pozdější
divadelním	divadelní	k2eAgMnSc7d1	divadelní
partnerem	partner	k1gMnSc7	partner
Jiřím	Jiří	k1gMnSc7	Jiří
Voskovcem	Voskovec	k1gMnSc7	Voskovec
<g/>
.	.	kIx.	.
</s>
