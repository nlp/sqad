<p>
<s>
Rákosník	rákosník	k1gInSc1	rákosník
velký	velký	k2eAgInSc1d1	velký
(	(	kIx(	(
<g/>
Acrocephalus	Acrocephalus	k1gInSc1	Acrocephalus
arundinaceus	arundinaceus	k1gInSc1	arundinaceus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
středně	středně	k6eAd1	středně
velkým	velký	k2eAgInSc7d1	velký
druhem	druh	k1gInSc7	druh
pěvce	pěvec	k1gMnSc2	pěvec
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
rákosníkovitých	rákosníkovitý	k2eAgFnPc2d1	rákosníkovitý
(	(	kIx(	(
<g/>
Acrocephalidae	Acrocephalidae	k1gFnPc2	Acrocephalidae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Taxonomie	taxonomie	k1gFnSc2	taxonomie
==	==	k?	==
</s>
</p>
<p>
<s>
Polytypický	Polytypický	k2eAgInSc1d1	Polytypický
druh	druh	k1gInSc1	druh
se	s	k7c7	s
2	[number]	k4	2
poddruhy	poddruh	k1gInPc7	poddruh
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
A.	A.	kA	A.
a.	a.	k?	a.
arundinaceus	arundinaceus	k1gMnSc1	arundinaceus
(	(	kIx(	(
<g/>
Linnaeus	Linnaeus	k1gMnSc1	Linnaeus
<g/>
,	,	kIx,	,
1758	[number]	k4	1758
<g/>
)	)	kIx)	)
–	–	k?	–
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
severozápadní	severozápadní	k2eAgFnSc3d1	severozápadní
Africe	Afrika	k1gFnSc3	Afrika
a	a	k8xC	a
Turecku	Turecko	k1gNnSc3	Turecko
východně	východně	k6eAd1	východně
po	po	k7c4	po
řeku	řeka	k1gFnSc4	řeka
Volhu	Volha	k1gFnSc4	Volha
<g/>
,	,	kIx,	,
Kavkaz	Kavkaz	k1gInSc4	Kavkaz
a	a	k8xC	a
Kaspické	kaspický	k2eAgNnSc4d1	Kaspické
moře	moře	k1gNnSc4	moře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
A.	A.	kA	A.
a.	a.	k?	a.
zarudnyi	zarudnyi	k1gNnSc1	zarudnyi
E.	E.	kA	E.
J.	J.	kA	J.
O.	O.	kA	O.
Hartert	Hartert	k1gMnSc1	Hartert
<g/>
,	,	kIx,	,
1907	[number]	k4	1907
–	–	k?	–
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
v	v	k7c6	v
severním	severní	k2eAgInSc6d1	severní
Iráku	Irák	k1gInSc6	Irák
a	a	k8xC	a
Íránu	Írán	k1gInSc6	Írán
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
od	od	k7c2	od
Volhy	Volha	k1gFnSc2	Volha
a	a	k8xC	a
Kaspického	kaspický	k2eAgNnSc2d1	Kaspické
moře	moře	k1gNnSc2	moře
východně	východně	k6eAd1	východně
po	po	k7c4	po
severozápadní	severozápadní	k2eAgNnSc4d1	severozápadní
Mongolsko	Mongolsko	k1gNnSc4	Mongolsko
a	a	k8xC	a
jižně	jižně	k6eAd1	jižně
po	po	k7c4	po
Tádžikistán	Tádžikistán	k1gInSc4	Tádžikistán
a	a	k8xC	a
severozápadní	severozápadní	k2eAgFnSc4d1	severozápadní
Čínu	Čína	k1gFnSc4	Čína
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
druh	druh	k1gInSc1	druh
rákosníka	rákosník	k1gMnSc2	rákosník
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
velikosti	velikost	k1gFnSc2	velikost
drozda	drozd	k1gMnSc2	drozd
<g/>
;	;	kIx,	;
dorůstá	dorůstat	k5eAaImIp3nS	dorůstat
délky	délka	k1gFnSc2	délka
17	[number]	k4	17
<g/>
–	–	k?	–
<g/>
20	[number]	k4	20
cm	cm	kA	cm
<g/>
,	,	kIx,	,
v	v	k7c6	v
rozpětí	rozpětí	k1gNnSc6	rozpětí
křídel	křídlo	k1gNnPc2	křídlo
měří	měřit	k5eAaImIp3nS	měřit
24	[number]	k4	24
<g/>
–	–	k?	–
<g/>
26	[number]	k4	26
cm	cm	kA	cm
a	a	k8xC	a
váží	vážit	k5eAaImIp3nP	vážit
28	[number]	k4	28
<g/>
–	–	k?	–
<g/>
38	[number]	k4	38
g	g	kA	g
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nenápadně	nápadně	k6eNd1	nápadně
zbarvený	zbarvený	k2eAgInSc4d1	zbarvený
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
tmavě	tmavě	k6eAd1	tmavě
hnědý	hnědý	k2eAgInSc1d1	hnědý
se	s	k7c7	s
světlejší	světlý	k2eAgFnSc7d2	světlejší
<g/>
,	,	kIx,	,
žlutavě	žlutavě	k6eAd1	žlutavě
bílou	bílý	k2eAgFnSc7d1	bílá
spodinou	spodina	k1gFnSc7	spodina
<g/>
,	,	kIx,	,
bílým	bílý	k2eAgNnSc7d1	bílé
hrdlem	hrdlo	k1gNnSc7	hrdlo
a	a	k8xC	a
světlým	světlý	k2eAgInSc7d1	světlý
nadočním	nadoční	k2eAgInSc7d1	nadoční
proužkem	proužek	k1gInSc7	proužek
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgNnPc1	dva
pohlaví	pohlaví	k1gNnPc1	pohlaví
jsou	být	k5eAaImIp3nP	být
zbarvena	zbarvit	k5eAaPmNgNnP	zbarvit
stejně	stejně	k6eAd1	stejně
<g/>
.	.	kIx.	.
</s>
<s>
Podobá	podobat	k5eAaImIp3nS	podobat
se	se	k3xPyFc4	se
rákosníku	rákosník	k1gInSc2	rákosník
obecnému	obecný	k2eAgInSc3d1	obecný
(	(	kIx(	(
<g/>
A.	A.	kA	A.
scirpaceus	scirpaceus	k1gInSc1	scirpaceus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
však	však	k9	však
viditelně	viditelně	k6eAd1	viditelně
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
výraznější	výrazný	k2eAgInSc1d2	výraznější
nadoční	nadoční	k2eAgInSc1d1	nadoční
proužek	proužek	k1gInSc1	proužek
a	a	k8xC	a
delší	dlouhý	k2eAgInSc1d2	delší
silnější	silný	k2eAgInSc1d2	silnější
zobák	zobák	k1gInSc1	zobák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hlas	hlas	k1gInSc1	hlas
==	==	k?	==
</s>
</p>
<p>
<s>
Varuje	varovat	k5eAaImIp3nS	varovat
drsným	drsný	k2eAgMnSc7d1	drsný
"	"	kIx"	"
<g/>
krrr	krrr	k1gInSc4	krrr
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
se	se	k3xPyFc4	se
samci	samec	k1gMnPc1	samec
ozývají	ozývat	k5eAaImIp3nP	ozývat
při	při	k7c6	při
prolézání	prolézání	k1gNnSc6	prolézání
rákosin	rákosina	k1gFnPc2	rákosina
nebo	nebo	k8xC	nebo
z	z	k7c2	z
větví	větev	k1gFnPc2	větev
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nS	tvořit
výrazné	výrazný	k2eAgNnSc1d1	výrazné
skřehotavé	skřehotavý	k2eAgNnSc1d1	skřehotavé
"	"	kIx"	"
<g/>
karra	karra	k1gFnSc1	karra
karra	karra	k1gFnSc1	karra
kit	kit	k1gInSc4	kit
kit	kit	k1gInSc1	kit
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
zvuková	zvukový	k2eAgFnSc1d1	zvuková
nahrávka	nahrávka	k1gFnSc1	nahrávka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Areál	areál	k1gInSc1	areál
rozšíření	rozšíření	k1gNnSc1	rozšíření
rákosníka	rákosník	k1gMnSc2	rákosník
velkého	velký	k2eAgMnSc4d1	velký
sahá	sahat	k5eAaImIp3nS	sahat
od	od	k7c2	od
severozápadní	severozápadní	k2eAgFnSc2d1	severozápadní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
a	a	k8xC	a
Španělska	Španělsko	k1gNnSc2	Španělsko
po	po	k7c4	po
severovýchodní	severovýchodní	k2eAgFnSc4d1	severovýchodní
Čínu	Čína	k1gFnSc4	Čína
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgInSc1d1	jižní
Sachalin	Sachalin	k1gInSc1	Sachalin
a	a	k8xC	a
severní	severní	k2eAgNnSc1d1	severní
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tažný	tažný	k2eAgInSc1d1	tažný
se	s	k7c7	s
zimovišti	zimoviště	k1gNnPc7	zimoviště
v	v	k7c6	v
subsaharské	subsaharský	k2eAgFnSc6d1	subsaharská
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Česka	Česko	k1gNnSc2	Česko
odlétá	odlétat	k5eAaImIp3nS	odlétat
během	během	k7c2	během
srpna	srpen	k1gInSc2	srpen
a	a	k8xC	a
září	září	k1gNnSc2	září
a	a	k8xC	a
přilétá	přilétat	k5eAaImIp3nS	přilétat
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
až	až	k8xS	až
květnu	květen	k1gInSc6	květen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Početnost	početnost	k1gFnSc4	početnost
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
došlo	dojít	k5eAaPmAgNnS	dojít
u	u	k7c2	u
mnohých	mnohý	k2eAgFnPc2d1	mnohá
evropských	evropský	k2eAgFnPc2d1	Evropská
populací	populace	k1gFnPc2	populace
k	k	k7c3	k
výraznému	výrazný	k2eAgInSc3d1	výrazný
poklesu	pokles	k1gInSc3	pokles
stavů	stav	k1gInPc2	stav
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
ještě	ještě	k6eAd1	ještě
masivnějším	masivní	k2eAgNnSc7d2	masivnější
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
velmi	velmi	k6eAd1	velmi
citelným	citelný	k2eAgFnPc3d1	citelná
populačním	populační	k2eAgFnPc3d1	populační
ztrátám	ztráta	k1gFnPc3	ztráta
došlo	dojít	k5eAaPmAgNnS	dojít
např.	např.	kA	např.
v	v	k7c6	v
Nizozemsku	Nizozemsko	k1gNnSc6	Nizozemsko
<g/>
,	,	kIx,	,
Belgii	Belgie	k1gFnSc6	Belgie
<g/>
,	,	kIx,	,
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
např.	např.	kA	např.
jen	jen	k9	jen
v	v	k7c6	v
Nizozemsku	Nizozemsko	k1gNnSc6	Nizozemsko
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
hnízdilo	hnízdit	k5eAaImAgNnS	hnízdit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
10	[number]	k4	10
000	[number]	k4	000
párů	pár	k1gInPc2	pár
<g/>
,	,	kIx,	,
o	o	k7c4	o
40	[number]	k4	40
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
méně	málo	k6eAd2	málo
než	než	k8xS	než
300	[number]	k4	300
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
hlavní	hlavní	k2eAgFnPc1d1	hlavní
příčiny	příčina	k1gFnPc1	příčina
bývají	bývat	k5eAaImIp3nP	bývat
uváděny	uvádět	k5eAaImNgFnP	uvádět
odvodňování	odvodňování	k1gNnSc1	odvodňování
mokřadů	mokřad	k1gInPc2	mokřad
a	a	k8xC	a
eutrofizace	eutrofizace	k1gFnSc2	eutrofizace
vod	voda	k1gFnPc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
citlivý	citlivý	k2eAgInSc4d1	citlivý
na	na	k7c4	na
rušení	rušení	k1gNnSc4	rušení
na	na	k7c6	na
hnízdištích	hnízdiště	k1gNnPc6	hnízdiště
a	a	k8xC	a
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
navíc	navíc	k6eAd1	navíc
ohrožován	ohrožovat	k5eAaImNgInS	ohrožovat
lovem	lov	k1gInSc7	lov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
byla	být	k5eAaImAgFnS	být
evropská	evropský	k2eAgFnSc1d1	Evropská
populace	populace	k1gFnSc1	populace
druhu	druh	k1gInSc2	druh
odhadována	odhadovat	k5eAaImNgFnS	odhadovat
na	na	k7c4	na
1,5	[number]	k4	1,5
<g/>
–	–	k?	–
<g/>
2,9	[number]	k4	2,9
milionů	milion	k4xCgInPc2	milion
párů	pár	k1gInPc2	pár
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
po	po	k7c4	po
600	[number]	k4	600
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
1,5	[number]	k4	1,5
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
tisíc	tisíc	k4xCgInPc2	tisíc
párů	pár	k1gInPc2	pár
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
zvláště	zvláště	k6eAd1	zvláště
chráněn	chránit	k5eAaImNgInS	chránit
jako	jako	k8xC	jako
silně	silně	k6eAd1	silně
ohrožený	ohrožený	k2eAgInSc4d1	ohrožený
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biotop	biotop	k1gInSc4	biotop
==	==	k?	==
</s>
</p>
<p>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
v	v	k7c4	v
hustých	hustý	k2eAgInPc2d1	hustý
<g/>
,	,	kIx,	,
vysokých	vysoký	k2eAgInPc2d1	vysoký
<g/>
,	,	kIx,	,
z	z	k7c2	z
vody	voda	k1gFnSc2	voda
rostoucích	rostoucí	k2eAgFnPc6d1	rostoucí
rákosinách	rákosina	k1gFnPc6	rákosina
větší	veliký	k2eAgFnSc2d2	veliký
rozlohy	rozloha	k1gFnSc2	rozloha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potrava	potrava	k1gFnSc1	potrava
==	==	k?	==
</s>
</p>
<p>
<s>
Obratně	obratně	k6eAd1	obratně
šplhá	šplhat	k5eAaImIp3nS	šplhat
po	po	k7c6	po
rákosí	rákosí	k1gNnSc6	rákosí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
také	také	k9	také
pátrá	pátrat	k5eAaImIp3nS	pátrat
po	po	k7c6	po
potravě	potrava	k1gFnSc6	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Živí	živit	k5eAaImIp3nP	živit
se	se	k3xPyFc4	se
hlavně	hlavně	k6eAd1	hlavně
hmyzem	hmyz	k1gInSc7	hmyz
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k8xC	i
jinými	jiný	k2eAgMnPc7d1	jiný
malými	malý	k2eAgMnPc7d1	malý
živočichy	živočich	k1gMnPc7	živočich
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
pavouků	pavouk	k1gMnPc2	pavouk
<g/>
,	,	kIx,	,
měkkýšů	měkkýš	k1gMnPc2	měkkýš
nebo	nebo	k8xC	nebo
obojživelníků	obojživelník	k1gMnPc2	obojživelník
<g/>
;	;	kIx,	;
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
požírá	požírat	k5eAaImIp3nS	požírat
také	také	k9	také
bobule	bobule	k1gFnSc1	bobule
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hnízdění	hnízdění	k1gNnSc2	hnízdění
==	==	k?	==
</s>
</p>
<p>
<s>
Pohlavně	pohlavně	k6eAd1	pohlavně
dospívá	dospívat	k5eAaImIp3nS	dospívat
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
kalendářním	kalendářní	k2eAgInSc6d1	kalendářní
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
1	[number]	k4	1
<g/>
×	×	k?	×
až	až	k9	až
2	[number]	k4	2
<g/>
×	×	k?	×
ročně	ročně	k6eAd1	ročně
od	od	k7c2	od
května	květen	k1gInSc2	květen
do	do	k7c2	do
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
<s>
Páry	pár	k1gInPc1	pár
bývají	bývat	k5eAaImIp3nP	bývat
monogamní	monogamní	k2eAgInPc1d1	monogamní
<g/>
,	,	kIx,	,
u	u	k7c2	u
samců	samec	k1gMnPc2	samec
se	se	k3xPyFc4	se
však	však	k9	však
objevuje	objevovat	k5eAaImIp3nS	objevovat
také	také	k9	také
bigamie	bigamie	k1gFnSc1	bigamie
či	či	k8xC	či
trigamie	trigamie	k1gFnSc1	trigamie
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mohou	moct	k5eAaImIp3nP	moct
zahnízdit	zahnízdit	k5eAaPmF	zahnízdit
hned	hned	k6eAd1	hned
se	s	k7c7	s
2	[number]	k4	2
nebo	nebo	k8xC	nebo
3	[number]	k4	3
samicemi	samice	k1gFnPc7	samice
<g/>
.	.	kIx.	.
</s>
<s>
Hluboké	hluboký	k2eAgNnSc1d1	hluboké
miskovité	miskovitý	k2eAgNnSc1d1	miskovité
hnízdo	hnízdo	k1gNnSc1	hnízdo
spletené	spletený	k2eAgNnSc1d1	spletené
ze	z	k7c2	z
stébel	stéblo	k1gNnPc2	stéblo
trav	tráva	k1gFnPc2	tráva
vplétá	vplétat	k5eAaImIp3nS	vplétat
samotná	samotný	k2eAgFnSc1d1	samotná
samice	samice	k1gFnSc1	samice
do	do	k7c2	do
hustého	hustý	k2eAgNnSc2d1	husté
rákosí	rákosí	k1gNnSc2	rákosí
nad	nad	k7c7	nad
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
snůšce	snůška	k1gFnSc6	snůška
je	být	k5eAaImIp3nS	být
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
zelenavě	zelenavě	k6eAd1	zelenavě
bílých	bílý	k2eAgMnPc2d1	bílý
<g/>
,	,	kIx,	,
tmavě	tmavě	k6eAd1	tmavě
skvrnitých	skvrnitý	k2eAgNnPc2d1	skvrnité
vajec	vejce	k1gNnPc2	vejce
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
22,5	[number]	k4	22,5
x	x	k?	x
16,1	[number]	k4	16,1
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Inkubační	inkubační	k2eAgFnSc1d1	inkubační
doba	doba	k1gFnSc1	doba
trvá	trvat	k5eAaImIp3nS	trvat
13	[number]	k4	13
<g/>
–	–	k?	–
<g/>
15	[number]	k4	15
dnů	den	k1gInPc2	den
<g/>
,	,	kIx,	,
na	na	k7c6	na
vejcích	vejce	k1gNnPc6	vejce
sedí	sedit	k5eAaImIp3nP	sedit
pouze	pouze	k6eAd1	pouze
samice	samice	k1gFnPc1	samice
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
hnízdo	hnízdo	k1gNnSc1	hnízdo
opouští	opouštět	k5eAaImIp3nS	opouštět
po	po	k7c6	po
12	[number]	k4	12
<g/>
–	–	k?	–
<g/>
14	[number]	k4	14
dnech	den	k1gInPc6	den
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
monogamních	monogamní	k2eAgInPc2d1	monogamní
párů	pár	k1gInPc2	pár
je	být	k5eAaImIp3nS	být
krmí	krmit	k5eAaImIp3nP	krmit
oba	dva	k4xCgMnPc1	dva
rodiče	rodič	k1gMnPc1	rodič
<g/>
,	,	kIx,	,
polygynní	polygynní	k2eAgMnPc1d1	polygynní
samci	samec	k1gMnPc1	samec
však	však	k9	však
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
s	s	k7c7	s
krmením	krmení	k1gNnSc7	krmení
mláďat	mládě	k1gNnPc2	mládě
pouze	pouze	k6eAd1	pouze
první	první	k4xOgFnSc4	první
samici	samice	k1gFnSc4	samice
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
ostatní	ostatní	k2eAgFnPc1d1	ostatní
samice	samice	k1gFnPc1	samice
krmí	krmit	k5eAaImIp3nP	krmit
svá	svůj	k3xOyFgNnPc4	svůj
mláďata	mládě	k1gNnPc4	mládě
samy	sám	k3xTgFnPc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
častým	častý	k2eAgMnSc7d1	častý
hostitelem	hostitel	k1gMnSc7	hostitel
kukačky	kukačka	k1gFnSc2	kukačka
obecné	obecná	k1gFnSc2	obecná
(	(	kIx(	(
<g/>
Cuculus	Cuculus	k1gMnSc1	Cuculus
canorus	canorus	k1gMnSc1	canorus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
dožívá	dožívat	k5eAaImIp3nS	dožívat
průměrně	průměrně	k6eAd1	průměrně
2,4	[number]	k4	2,4
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
ptáků	pták	k1gMnPc2	pták
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
rákosník	rákosník	k1gMnSc1	rákosník
velký	velký	k2eAgMnSc1d1	velký
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
rákosník	rákosník	k1gMnSc1	rákosník
velký	velký	k2eAgMnSc1d1	velký
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Acrocephalus	Acrocephalus	k1gInSc1	Acrocephalus
arundinaceus	arundinaceus	k1gInSc1	arundinaceus
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
