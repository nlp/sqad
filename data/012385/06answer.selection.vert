<s>
Největší	veliký	k2eAgInSc1d3	veliký
druh	druh	k1gInSc1	druh
rákosníka	rákosník	k1gMnSc2	rákosník
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
velikosti	velikost	k1gFnSc2	velikost
drozda	drozd	k1gMnSc2	drozd
<g/>
;	;	kIx,	;
dorůstá	dorůstat	k5eAaImIp3nS	dorůstat
délky	délka	k1gFnSc2	délka
17	[number]	k4	17
<g/>
–	–	k?	–
<g/>
20	[number]	k4	20
cm	cm	kA	cm
<g/>
,	,	kIx,	,
v	v	k7c6	v
rozpětí	rozpětí	k1gNnSc6	rozpětí
křídel	křídlo	k1gNnPc2	křídlo
měří	měřit	k5eAaImIp3nS	měřit
24	[number]	k4	24
<g/>
–	–	k?	–
<g/>
26	[number]	k4	26
cm	cm	kA	cm
a	a	k8xC	a
váží	vážit	k5eAaImIp3nP	vážit
28	[number]	k4	28
<g/>
–	–	k?	–
<g/>
38	[number]	k4	38
g	g	kA	g
<g/>
.	.	kIx.	.
</s>
