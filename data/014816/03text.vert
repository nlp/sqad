<s>
Velouté	Veloutý	k2eAgNnSc1d1
</s>
<s>
Velouté	Veloutý	k2eAgFnPc1d1
Omáčka	omáčka	k1gFnSc1
velouté	veloutý	k2eAgFnPc1d1
s	s	k7c7
májovkamiZákladní	májovkamiZákladný	k2eAgMnPc1d1
informace	informace	k1gFnPc4
Místo	místo	k7c2
původu	původ	k1gInSc2
</s>
<s>
Francie	Francie	k1gFnSc1
Složení	složení	k1gNnSc2
</s>
<s>
jíška	jíška	k1gFnSc1
a	a	k8xC
vývar	vývar	k1gInSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Velouté	Veloutý	k2eAgInPc4d1
(	(	kIx(
<g/>
z	z	k7c2
francouzského	francouzský	k2eAgInSc2d1
výrazu	výraz	k1gInSc2
velours	velours	k6eAd1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
znamená	znamenat	k5eAaImIp3nS
samet	samet	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
omáčka	omáčka	k1gFnSc1
z	z	k7c2
máslové	máslový	k2eAgFnSc2d1
jíšky	jíška	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
rozředěná	rozředěný	k2eAgFnSc1d1
vývarem	vývar	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
jedna	jeden	k4xCgFnSc1
ze	z	k7c2
základních	základní	k2eAgFnPc2d1
omáček	omáčka	k1gFnPc2
<g/>
,	,	kIx,
převzatá	převzatý	k2eAgFnSc1d1
z	z	k7c2
francouzské	francouzský	k2eAgFnSc2d1
kuchyně	kuchyně	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Základní	základní	k2eAgInSc1d1
předpis	předpis	k1gInSc1
uvádí	uvádět	k5eAaImIp3nS
zhruba	zhruba	k6eAd1
toto	tento	k3xDgNnSc4
složení	složení	k1gNnSc4
a	a	k8xC
postup	postup	k1gInSc4
<g/>
:	:	kIx,
</s>
<s>
2	#num#	k4
díly	dílo	k1gNnPc7
másla	máslo	k1gNnSc2
</s>
<s>
1	#num#	k4
díl	díl	k1gInSc1
mouky	mouka	k1gFnSc2
</s>
<s>
vývar	vývar	k1gInSc1
</s>
<s>
Máslo	máslo	k1gNnSc4
zpěnit	zpěnit	k5eAaPmF
<g/>
,	,	kIx,
nepřepálit	přepálit	k5eNaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přidat	přidat	k5eAaPmF
hladkou	hladký	k2eAgFnSc4d1
mouku	mouka	k1gFnSc4
a	a	k8xC
krátce	krátce	k6eAd1
orestovat	orestovat	k5eAaPmF
<g/>
,	,	kIx,
dodat	dodat	k5eAaPmF
vývar	vývar	k1gInSc4
na	na	k7c4
patřičnou	patřičný	k2eAgFnSc4d1
hustotu	hustota	k1gFnSc4
a	a	k8xC
provařit	provařit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Vývar	vývar	k1gInSc4
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
dodat	dodat	k5eAaPmF
z	z	k7c2
ryb	ryba	k1gFnPc2
<g/>
,	,	kIx,
tzv.	tzv.	kA
„	„	k?
<g/>
rybí	rybí	k2eAgInSc4d1
fond	fond	k1gInSc4
<g/>
“	“	k?
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
masa	maso	k1gNnSc2
nebo	nebo	k8xC
zeleniny	zelenina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejznámější	známý	k2eAgInSc4d3
klasický	klasický	k2eAgInSc4d1
recept	recept	k1gInSc4
z	z	k7c2
francouzské	francouzský	k2eAgFnSc2d1
kuchyně	kuchyně	k1gFnSc2
používá	používat	k5eAaImIp3nS
k	k	k7c3
zalití	zalití	k1gNnSc3
vývar	vývar	k1gInSc4
z	z	k7c2
telecích	telecí	k2eAgFnPc2d1
nožiček	nožička	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Recept	recept	k1gInSc1
se	se	k3xPyFc4
objevuje	objevovat	k5eAaImIp3nS
už	už	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1533	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Auguste	August	k1gMnSc5
Escoffier	Escoffier	k1gInSc4
řadil	řadit	k5eAaImAgInS
velouté	veloutý	k2eAgFnPc4d1
k	k	k7c3
pěti	pět	k4xCc3
„	„	k?
<g/>
mateřským	mateřský	k2eAgFnPc3d1
omáčkám	omáčka	k1gFnPc3
<g/>
“	“	k?
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
se	se	k3xPyFc4
dále	daleko	k6eAd2
dochucují	dochucovat	k5eAaImIp3nP,k5eAaPmIp3nP,k5eAaBmIp3nP
např.	např.	kA
vínem	víno	k1gNnSc7
<g/>
,	,	kIx,
houbami	houba	k1gFnPc7
<g/>
,	,	kIx,
bylinkami	bylinka	k1gFnPc7
nebo	nebo	k8xC
chřestem	chřest	k1gInSc7
<g/>
,	,	kIx,
přidáním	přidání	k1gNnSc7
smetany	smetana	k1gFnSc2
a	a	k8xC
másla	máslo	k1gNnSc2
vznikne	vzniknout	k5eAaPmIp3nS
známá	známý	k2eAgFnSc1d1
sauce	sauce	k1gFnSc1
suprê	suprê	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
http://dadala.hyperlinx.cz/om/omr0003.html	http://dadala.hyperlinx.cz/om/omr0003.html	k1gInSc1
<g/>
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.foodguru.com	www.foodguru.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Velouté	Veloutý	k2eAgFnPc4d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
http://slovnik.vareni.cz/veloute/	http://slovnik.vareni.cz/veloute/	k?
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Francie	Francie	k1gFnSc1
|	|	kIx~
Gastronomie	gastronomie	k1gFnSc1
</s>
