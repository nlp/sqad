<s>
Velouté	Veloutý	k2eAgInPc4d1
(	(	kIx(
<g/>
z	z	k7c2
francouzského	francouzský	k2eAgInSc2d1
výrazu	výraz	k1gInSc2
velours	velours	k6eAd1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
znamená	znamenat	k5eAaImIp3nS
samet	samet	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
omáčka	omáčka	k1gFnSc1
z	z	k7c2
máslové	máslový	k2eAgFnSc2d1
jíšky	jíška	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
rozředěná	rozředěný	k2eAgFnSc1d1
vývarem	vývar	k1gInSc7
<g/>
.	.	kIx.
</s>