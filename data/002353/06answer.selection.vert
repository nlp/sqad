<s>
Granuloma	Granuloma	k1gFnSc1	Granuloma
inguinale	inguinale	k6eAd1	inguinale
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
též	též	k9	též
jako	jako	k8xC	jako
donovanóza	donovanóza	k1gFnSc1	donovanóza
<g/>
,	,	kIx,	,
granuloma	granuloma	k1gFnSc1	granuloma
venereum	venereum	k1gNnSc1	venereum
či	či	k8xC	či
inguinální	inguinální	k2eAgInSc1d1	inguinální
granulom	granulom	k1gInSc1	granulom
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
sexuálně	sexuálně	k6eAd1	sexuálně
přenosná	přenosný	k2eAgFnSc1d1	přenosná
nemoc	nemoc	k1gFnSc1	nemoc
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gMnSc7	jejíž
původcem	původce	k1gMnSc7	původce
je	být	k5eAaImIp3nS	být
bakterie	bakterie	k1gFnSc1	bakterie
Calymmatobacterium	Calymmatobacterium	k1gNnSc1	Calymmatobacterium
granulomatis	granulomatis	k1gFnSc2	granulomatis
<g/>
.	.	kIx.	.
</s>
