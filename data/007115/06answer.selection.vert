<s>
Řeč	řeč	k1gFnSc1	řeč
je	být	k5eAaImIp3nS	být
artikulovaný	artikulovaný	k2eAgInSc4d1	artikulovaný
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
zvukový	zvukový	k2eAgInSc4d1	zvukový
projev	projev	k1gInSc4	projev
člověka	člověk	k1gMnSc2	člověk
sloužící	sloužící	k1gFnSc2	sloužící
především	především	k9	především
ke	k	k7c3	k
vzájemnému	vzájemný	k2eAgNnSc3d1	vzájemné
dorozumívání	dorozumívání	k1gNnSc3	dorozumívání
<g/>
.	.	kIx.	.
</s>
