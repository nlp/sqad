<s>
Řeč	řeč	k1gFnSc1	řeč
je	být	k5eAaImIp3nS	být
artikulovaný	artikulovaný	k2eAgInSc4d1	artikulovaný
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
zvukový	zvukový	k2eAgInSc4d1	zvukový
projev	projev	k1gInSc4	projev
člověka	člověk	k1gMnSc2	člověk
sloužící	sloužící	k1gFnSc2	sloužící
především	především	k9	především
ke	k	k7c3	k
vzájemnému	vzájemný	k2eAgNnSc3d1	vzájemné
dorozumívání	dorozumívání	k1gNnSc3	dorozumívání
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
řeč	řeč	k1gFnSc1	řeč
je	být	k5eAaImIp3nS	být
článkovaná	článkovaný	k2eAgFnSc1d1	článkovaná
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
ze	z	k7c2	z
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
slovní	slovní	k2eAgFnSc4d1	slovní
zásobu	zásoba	k1gFnSc4	zásoba
(	(	kIx(	(
<g/>
lexikální	lexikální	k2eAgInSc1d1	lexikální
systém	systém	k1gInSc1	systém
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
jejich	jejich	k3xOp3gNnSc1	jejich
skládání	skládání	k1gNnSc1	skládání
se	se	k3xPyFc4	se
řídí	řídit	k5eAaImIp3nS	řídit
pravidly	pravidlo	k1gNnPc7	pravidlo
jazyka	jazyk	k1gInSc2	jazyk
(	(	kIx(	(
<g/>
gramatika	gramatika	k1gFnSc1	gramatika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Slova	slovo	k1gNnPc1	slovo
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
z	z	k7c2	z
hlásek	hláska	k1gFnPc2	hláska
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
tvoří	tvořit	k5eAaImIp3nP	tvořit
jeho	jeho	k3xOp3gInSc4	jeho
fonémický	fonémický	k2eAgInSc4d1	fonémický
systém	systém	k1gInSc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
Slova	slovo	k1gNnPc1	slovo
nejsou	být	k5eNaImIp3nP	být
pouhé	pouhý	k2eAgInPc1d1	pouhý
signály	signál	k1gInPc1	signál
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
by	by	kYmCp3nP	by
svůj	svůj	k3xOyFgInSc4	svůj
význam	význam	k1gInSc4	význam
odvozovaly	odvozovat	k5eAaImAgInP	odvozovat
ze	z	k7c2	z
situace	situace	k1gFnSc2	situace
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
symboly	symbol	k1gInPc1	symbol
s	s	k7c7	s
kulturně	kulturně	k6eAd1	kulturně
daným	daný	k2eAgInSc7d1	daný
významem	význam	k1gInSc7	význam
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
slovo	slovo	k1gNnSc1	slovo
nese	nést	k5eAaImIp3nS	nést
v	v	k7c6	v
nejrůznějších	různý	k2eAgFnPc6d3	nejrůznější
situacích	situace	k1gFnPc6	situace
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
spojování	spojování	k1gNnSc1	spojování
slov	slovo	k1gNnPc2	slovo
není	být	k5eNaImIp3nS	být
libovolné	libovolný	k2eAgNnSc1d1	libovolné
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
vždy	vždy	k6eAd1	vždy
se	se	k3xPyFc4	se
řídí	řídit	k5eAaImIp3nS	řídit
syntaxí	syntax	k1gFnSc7	syntax
daného	daný	k2eAgInSc2d1	daný
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgFnSc1d1	moderní
lingvistika	lingvistika	k1gFnSc1	lingvistika
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
jazyk	jazyk	k1gInSc4	jazyk
jako	jako	k8xS	jako
systém	systém	k1gInSc4	systém
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
Noama	Noamum	k1gNnSc2	Noamum
Chomského	Chomský	k2eAgInSc2d1	Chomský
jazykovou	jazykový	k2eAgFnSc7d1	jazyková
kompetenci	kompetence	k1gFnSc4	kompetence
<g/>
)	)	kIx)	)
a	a	k8xC	a
řeč	řeč	k1gFnSc4	řeč
nebo	nebo	k8xC	nebo
řečové	řečový	k2eAgInPc4d1	řečový
akty	akt	k1gInPc4	akt
jako	jako	k8xS	jako
aktuální	aktuální	k2eAgNnSc4d1	aktuální
použití	použití	k1gNnSc4	použití
této	tento	k3xDgFnSc2	tento
kompetence	kompetence	k1gFnSc2	kompetence
<g/>
.	.	kIx.	.
</s>
<s>
Jazyků	jazyk	k1gInPc2	jazyk
je	být	k5eAaImIp3nS	být
mnoho	mnoho	k4c1	mnoho
<g/>
,	,	kIx,	,
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
však	však	k9	však
lze	lze	k6eAd1	lze
rozeznat	rozeznat	k5eAaPmF	rozeznat
podobné	podobný	k2eAgFnPc4d1	podobná
složky	složka	k1gFnPc4	složka
či	či	k8xC	či
vrstvy	vrstva	k1gFnPc4	vrstva
(	(	kIx(	(
<g/>
např.	např.	kA	např.
vrstvu	vrstva	k1gFnSc4	vrstva
fonetickou	fonetický	k2eAgFnSc7d1	fonetická
<g/>
,	,	kIx,	,
lexikální	lexikální	k2eAgFnSc7d1	lexikální
<g/>
,	,	kIx,	,
gramatickou	gramatický	k2eAgFnSc7d1	gramatická
a	a	k8xC	a
stylovou	stylový	k2eAgFnSc7d1	stylová
<g/>
)	)	kIx)	)
a	a	k8xC	a
i	i	k9	i
vztah	vztah	k1gInSc4	vztah
mezi	mezi	k7c7	mezi
jazykem	jazyk	k1gInSc7	jazyk
a	a	k8xC	a
řečí	řeč	k1gFnSc7	řeč
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
všech	všecek	k3xTgInPc2	všecek
podobný	podobný	k2eAgInSc1d1	podobný
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
řečový	řečový	k2eAgInSc1d1	řečový
akt	akt	k1gInSc1	akt
se	se	k3xPyFc4	se
děje	dít	k5eAaImIp3nS	dít
v	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
jazyce	jazyk	k1gInSc6	jazyk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnSc1	jeho
uživatel	uživatel	k1gMnSc1	uživatel
musel	muset	k5eAaImAgMnS	muset
naučit	naučit	k5eAaPmF	naučit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
doplněn	doplnit	k5eAaPmNgInS	doplnit
slovy	slovo	k1gNnPc7	slovo
z	z	k7c2	z
jazyků	jazyk	k1gInPc2	jazyk
jiných	jiný	k2eAgInPc2d1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
Řečový	řečový	k2eAgInSc1d1	řečový
akt	akt	k1gInSc1	akt
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
mluvený	mluvený	k2eAgInSc4d1	mluvený
(	(	kIx(	(
<g/>
akustický	akustický	k2eAgInSc4d1	akustický
-	-	kIx~	-
promluva	promluva	k1gFnSc1	promluva
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
audiovizuální	audiovizuální	k2eAgInSc1d1	audiovizuální
<g/>
,	,	kIx,	,
provázený	provázený	k2eAgInSc1d1	provázený
gestikulací	gestikulace	k1gFnSc7	gestikulace
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vyjádřen	vyjádřit	k5eAaPmNgMnS	vyjádřit
písmem	písmo	k1gNnSc7	písmo
jako	jako	k8xC	jako
text	text	k1gInSc1	text
a	a	k8xC	a
neslyšící	slyšící	k2eNgNnPc1d1	neslyšící
užívají	užívat	k5eAaImIp3nP	užívat
vizuální	vizuální	k2eAgFnSc4d1	vizuální
znakovou	znakový	k2eAgFnSc4d1	znaková
řeč	řeč	k1gFnSc4	řeč
<g/>
.	.	kIx.	.
</s>
<s>
Studiem	studio	k1gNnSc7	studio
jazyka	jazyk	k1gInSc2	jazyk
vůbec	vůbec	k9	vůbec
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
obecná	obecný	k2eAgFnSc1d1	obecná
lingvistika	lingvistika	k1gFnSc1	lingvistika
<g/>
,	,	kIx,	,
studiem	studio	k1gNnSc7	studio
řeči	řeč	k1gFnSc2	řeč
mnohem	mnohem	k6eAd1	mnohem
mladší	mladý	k2eAgFnSc1d2	mladší
pragmatika	pragmatika	k1gFnSc1	pragmatika
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
obory	obor	k1gInPc1	obor
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Řeč	řeč	k1gFnSc1	řeč
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
člověku	člověk	k1gMnSc6	člověk
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
účelové	účelový	k2eAgNnSc1d1	účelové
jednání	jednání	k1gNnSc1	jednání
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgNnSc7	jenž
se	se	k3xPyFc4	se
sdělují	sdělovat	k5eAaImIp3nP	sdělovat
a	a	k8xC	a
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
myšlenky	myšlenka	k1gFnPc4	myšlenka
<g/>
,	,	kIx,	,
pocity	pocit	k1gInPc4	pocit
a	a	k8xC	a
přání	přání	k1gNnPc4	přání
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
kulturně	kulturně	k6eAd1	kulturně
daných	daný	k2eAgInPc2d1	daný
symbolů	symbol	k1gInPc2	symbol
a	a	k8xC	a
pravidel	pravidlo	k1gNnPc2	pravidlo
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gNnSc4	jejich
řazení	řazení	k1gNnSc4	řazení
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Všechny	všechen	k3xTgFnPc1	všechen
činnosti	činnost	k1gFnPc1	činnost
související	související	k2eAgFnPc1d1	související
s	s	k7c7	s
řečí	řeč	k1gFnSc7	řeč
jsou	být	k5eAaImIp3nP	být
řízeny	řídit	k5eAaImNgFnP	řídit
mozkem	mozek	k1gInSc7	mozek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mozkové	mozkový	k2eAgFnSc6d1	mozková
kůře	kůra	k1gFnSc6	kůra
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
centrum	centrum	k1gNnSc1	centrum
řeči	řeč	k1gFnSc2	řeč
a	a	k8xC	a
odtud	odtud	k6eAd1	odtud
vycházejí	vycházet	k5eAaImIp3nP	vycházet
složitě	složitě	k6eAd1	složitě
koordinované	koordinovaný	k2eAgFnPc4d1	koordinovaná
instrukce	instrukce	k1gFnPc4	instrukce
(	(	kIx(	(
<g/>
signály	signál	k1gInPc4	signál
<g/>
)	)	kIx)	)
do	do	k7c2	do
svalů	sval	k1gInPc2	sval
plic	plíce	k1gFnPc2	plíce
<g/>
,	,	kIx,	,
hrtanu	hrtan	k1gInSc2	hrtan
<g/>
,	,	kIx,	,
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
obličeje	obličej	k1gInSc2	obličej
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
pro	pro	k7c4	pro
tvorbu	tvorba	k1gFnSc4	tvorba
řeči	řeč	k1gFnSc2	řeč
je	být	k5eAaImIp3nS	být
patrně	patrně	k6eAd1	patrně
podstatnější	podstatný	k2eAgFnSc4d2	podstatnější
činnost	činnost	k1gFnSc4	činnost
obličejových	obličejový	k2eAgInPc2d1	obličejový
svalů	sval	k1gInPc2	sval
než	než	k8xS	než
hrtanu	hrtan	k1gInSc2	hrtan
<g/>
.	.	kIx.	.
</s>
<s>
Fyziologie	fyziologie	k1gFnSc1	fyziologie
nemusí	muset	k5eNaImIp3nS	muset
hrát	hrát	k5eAaImF	hrát
roli	role	k1gFnSc4	role
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rozhodující	rozhodující	k2eAgFnSc1d1	rozhodující
je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
mozek	mozek	k1gInSc1	mozek
<g/>
.	.	kIx.	.
</s>
<s>
Řeč	řeč	k1gFnSc1	řeč
vzniká	vznikat	k5eAaImIp3nS	vznikat
proudem	proud	k1gInSc7	proud
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
plic	plíce	k1gFnPc2	plíce
<g/>
,	,	kIx,	,
prochází	procházet	k5eAaImIp3nS	procházet
mezi	mezi	k7c7	mezi
hlasivkami	hlasivka	k1gFnPc7	hlasivka
(	(	kIx(	(
<g/>
hlasovou	hlasový	k2eAgFnSc7d1	hlasová
štěrbinou	štěrbina	k1gFnSc7	štěrbina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
zpravidla	zpravidla	k6eAd1	zpravidla
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
základní	základní	k2eAgInSc4d1	základní
zvuk	zvuk	k1gInSc4	zvuk
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
modulován	modulovat	k5eAaImNgInS	modulovat
(	(	kIx(	(
<g/>
ovlivněn	ovlivněn	k2eAgInSc1d1	ovlivněn
a	a	k8xC	a
doplněn	doplněn	k2eAgInSc1d1	doplněn
<g/>
)	)	kIx)	)
jazykem	jazyk	k1gInSc7	jazyk
a	a	k8xC	a
patrem	patro	k1gNnSc7	patro
<g/>
,	,	kIx,	,
dásněmi	dáseň	k1gFnPc7	dáseň
a	a	k8xC	a
rty	ret	k1gInPc7	ret
v	v	k7c6	v
ústní	ústní	k2eAgFnSc6d1	ústní
dutině	dutina	k1gFnSc6	dutina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
působí	působit	k5eAaImIp3nS	působit
také	také	k9	také
jako	jako	k9	jako
rezonátor	rezonátor	k1gInSc4	rezonátor
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tvorbě	tvorba	k1gFnSc6	tvorba
samohlásek	samohláska	k1gFnPc2	samohláska
se	se	k3xPyFc4	se
podílejí	podílet	k5eAaImIp3nP	podílet
hlasivky	hlasivka	k1gFnPc1	hlasivka
a	a	k8xC	a
ústní	ústní	k2eAgFnSc1d1	ústní
dutina	dutina	k1gFnSc1	dutina
<g/>
,	,	kIx,	,
na	na	k7c6	na
tvorbě	tvorba	k1gFnSc6	tvorba
neznělých	znělý	k2eNgFnPc2d1	neznělá
souhlásek	souhláska	k1gFnPc2	souhláska
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
patro	patro	k1gNnSc1	patro
<g/>
,	,	kIx,	,
dásně	dáseň	k1gFnPc1	dáseň
a	a	k8xC	a
rty	ret	k1gInPc1	ret
<g/>
,	,	kIx,	,
na	na	k7c6	na
tvorbě	tvorba	k1gFnSc6	tvorba
znělých	znělý	k2eAgFnPc2d1	znělá
souhlásek	souhláska	k1gFnPc2	souhláska
ještě	ještě	k6eAd1	ještě
také	také	k9	také
hlasivky	hlasivka	k1gFnPc4	hlasivka
<g/>
.	.	kIx.	.
</s>
<s>
Tvorbu	tvorba	k1gFnSc4	tvorba
hlásek	hláska	k1gFnPc2	hláska
studuje	studovat	k5eAaImIp3nS	studovat
fonetika	fonetika	k1gFnSc1	fonetika
<g/>
.	.	kIx.	.
</s>
<s>
Řeč	řeč	k1gFnSc1	řeč
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
běžném	běžný	k2eAgNnSc6d1	běžné
pojetí	pojetí	k1gNnSc6	pojetí
výsadou	výsada	k1gFnSc7	výsada
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vývoji	vývoj	k1gInSc6	vývoj
jazykových	jazykový	k2eAgFnPc2d1	jazyková
schopností	schopnost	k1gFnPc2	schopnost
hrál	hrát	k5eAaImAgInS	hrát
podle	podle	k7c2	podle
některých	některý	k3yIgFnPc2	některý
studií	studie	k1gFnPc2	studie
velkou	velký	k2eAgFnSc4d1	velká
roli	role	k1gFnSc4	role
gen	gen	k1gInSc1	gen
FOXP	FOXP	kA	FOXP
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
nejbližšího	blízký	k2eAgMnSc2d3	nejbližší
příbuzného	příbuzný	k1gMnSc2	příbuzný
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
šimpanze	šimpanz	k1gMnPc4	šimpanz
<g/>
,	,	kIx,	,
mnohem	mnohem	k6eAd1	mnohem
méně	málo	k6eAd2	málo
vyvinutý	vyvinutý	k2eAgInSc1d1	vyvinutý
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgMnPc1d1	ostatní
živočichové	živočich	k1gMnPc1	živočich
sice	sice	k8xC	sice
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
schopni	schopen	k2eAgMnPc1d1	schopen
naučit	naučit	k5eAaPmF	naučit
se	se	k3xPyFc4	se
porozumět	porozumět	k5eAaPmF	porozumět
některým	některý	k3yIgNnPc3	některý
lidským	lidský	k2eAgNnPc3d1	lidské
slovům	slovo	k1gNnPc3	slovo
<g/>
,	,	kIx,	,
dělají	dělat	k5eAaImIp3nP	dělat
to	ten	k3xDgNnSc4	ten
však	však	k9	však
způsobem	způsob	k1gInSc7	způsob
odlišným	odlišný	k2eAgInSc7d1	odlišný
od	od	k7c2	od
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Pes	peso	k1gNnPc2	peso
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
naučit	naučit	k5eAaPmF	naučit
chápat	chápat	k5eAaImF	chápat
asi	asi	k9	asi
200	[number]	k4	200
slov	slovo	k1gNnPc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Zvířecí	zvířecí	k2eAgFnSc1d1	zvířecí
řeč	řeč	k1gFnSc1	řeč
je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
obdobou	obdoba	k1gFnSc7	obdoba
lidské	lidský	k2eAgFnSc2d1	lidská
řeči	řeč	k1gFnSc2	řeč
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zatím	zatím	k6eAd1	zatím
není	být	k5eNaImIp3nS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
jaké	jaký	k3yQgInPc1	jaký
jsou	být	k5eAaImIp3nP	být
kvalitativní	kvalitativní	k2eAgInPc1d1	kvalitativní
rozdíly	rozdíl	k1gInPc1	rozdíl
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Šimpanz	šimpanz	k1gMnSc1	šimpanz
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
měnit	měnit	k5eAaImF	měnit
zvuky	zvuk	k1gInPc4	zvuk
své	svůj	k3xOyFgFnSc2	svůj
řeči	řeč	k1gFnSc2	řeč
podle	podle	k7c2	podle
jiných	jiný	k2eAgMnPc2d1	jiný
šimpanzů	šimpanz	k1gMnPc2	šimpanz
a	a	k8xC	a
zvuky	zvuk	k1gInPc1	zvuk
patrně	patrně	k6eAd1	patrně
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
a	a	k8xC	a
sdělují	sdělovat	k5eAaImIp3nP	sdělovat
například	například	k6eAd1	například
i	i	k9	i
typ	typ	k1gInSc1	typ
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Poruchy	poruch	k1gInPc1	poruch
řeči	řeč	k1gFnSc2	řeč
lze	lze	k6eAd1	lze
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
poruchy	poruch	k1gInPc4	poruch
vývoje	vývoj	k1gInSc2	vývoj
hlasu	hlas	k1gInSc2	hlas
(	(	kIx(	(
<g/>
problémy	problém	k1gInPc4	problém
plynoucí	plynoucí	k2eAgInPc4d1	plynoucí
z	z	k7c2	z
poškození	poškození	k1gNnSc2	poškození
řečových	řečový	k2eAgNnPc2d1	řečové
center	centrum	k1gNnPc2	centrum
v	v	k7c6	v
mozku	mozek	k1gInSc6	mozek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poruchu	porucha	k1gFnSc4	porucha
tvorby	tvorba	k1gFnSc2	tvorba
hlasu	hlas	k1gInSc2	hlas
(	(	kIx(	(
<g/>
onemocnění	onemocnění	k1gNnSc2	onemocnění
hrtanu	hrtan	k1gInSc2	hrtan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
onemocnění	onemocnění	k1gNnSc1	onemocnění
úst	ústa	k1gNnPc2	ústa
apod.	apod.	kA	apod.
Vývojovými	vývojový	k2eAgFnPc7d1	vývojová
řečovými	řečový	k2eAgFnPc7d1	řečová
poruchami	porucha	k1gFnPc7	porucha
rozumíme	rozumět	k5eAaImIp1nP	rozumět
řeč	řeč	k1gFnSc4	řeč
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
svým	svůj	k3xOyFgInSc7	svůj
rozsahem	rozsah	k1gInSc7	rozsah
a	a	k8xC	a
formou	forma	k1gFnSc7	forma
neodpovídá	odpovídat	k5eNaImIp3nS	odpovídat
věku	věk	k1gInSc2	věk
dítěte	dítě	k1gNnSc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Poruchami	porucha	k1gFnPc7	porucha
řeči	řeč	k1gFnSc3	řeč
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
foniatrie	foniatrie	k1gFnSc1	foniatrie
a	a	k8xC	a
logopedie	logopedie	k1gFnSc1	logopedie
<g/>
.	.	kIx.	.
</s>
