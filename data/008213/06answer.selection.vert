<s>
Asijsko-pacifické	asijskoacifický	k2eAgNnSc1d1	asijsko-pacifický
hospodářské	hospodářský	k2eAgNnSc1d1	hospodářské
společenství	společenství	k1gNnSc1	společenství
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Asia-Pacific	Asia-Pacific	k1gMnSc1	Asia-Pacific
Economic	Economice	k1gFnPc2	Economice
Cooperation	Cooperation	k1gInSc1	Cooperation
<g/>
,	,	kIx,	,
zkratka	zkratka	k1gFnSc1	zkratka
APEC	APEC	kA	APEC
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
organizace	organizace	k1gFnSc1	organizace
sdružující	sdružující	k2eAgFnSc1d1	sdružující
21	[number]	k4	21
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
