<p>
<s>
Asijsko-pacifické	asijskoacifický	k2eAgNnSc1d1	asijsko-pacifický
hospodářské	hospodářský	k2eAgNnSc1d1	hospodářské
společenství	společenství	k1gNnSc1	společenství
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Asia-Pacific	Asia-Pacific	k1gMnSc1	Asia-Pacific
Economic	Economice	k1gFnPc2	Economice
Cooperation	Cooperation	k1gInSc1	Cooperation
<g/>
,	,	kIx,	,
zkratka	zkratka	k1gFnSc1	zkratka
APEC	APEC	kA	APEC
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
organizace	organizace	k1gFnSc1	organizace
sdružující	sdružující	k2eAgFnSc1d1	sdružující
21	[number]	k4	21
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
země	zem	k1gFnPc1	zem
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
polovinu	polovina	k1gFnSc4	polovina
HDP	HDP	kA	HDP
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
tohoto	tento	k3xDgNnSc2	tento
seskupení	seskupení	k1gNnSc2	seskupení
je	být	k5eAaImIp3nS	být
zlepšit	zlepšit	k5eAaPmF	zlepšit
ekonomické	ekonomický	k2eAgInPc4d1	ekonomický
a	a	k8xC	a
politické	politický	k2eAgInPc4d1	politický
vztahy	vztah	k1gInPc4	vztah
mezi	mezi	k7c7	mezi
členskými	členský	k2eAgInPc7d1	členský
státy	stát	k1gInPc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
integrace	integrace	k1gFnSc1	integrace
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgNnSc4	svůj
sídlo	sídlo	k1gNnSc4	sídlo
v	v	k7c6	v
Singapuru	Singapur	k1gInSc6	Singapur
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Členové	člen	k1gMnPc1	člen
==	==	k?	==
</s>
</p>
<p>
<s>
Zakládajících	zakládající	k2eAgMnPc2d1	zakládající
členů	člen	k1gMnPc2	člen
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
dvanáct	dvanáct	k4xCc4	dvanáct
<g/>
:	:	kIx,	:
Austrálie	Austrálie	k1gFnSc1	Austrálie
<g/>
,	,	kIx,	,
Brunej	Brunej	k1gFnSc1	Brunej
<g/>
,	,	kIx,	,
Filipíny	Filipíny	k1gFnPc1	Filipíny
<g/>
,	,	kIx,	,
Indonésie	Indonésie	k1gFnSc1	Indonésie
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
,	,	kIx,	,
Malajsie	Malajsie	k1gFnSc1	Malajsie
<g/>
,	,	kIx,	,
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
<g/>
,	,	kIx,	,
Thajsko	Thajsko	k1gNnSc1	Thajsko
<g/>
,	,	kIx,	,
Singapur	Singapur	k1gInSc1	Singapur
a	a	k8xC	a
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
přistoupily	přistoupit	k5eAaPmAgFnP	přistoupit
Čína	Čína	k1gFnSc1	Čína
<g/>
,	,	kIx,	,
Hong	Hong	k1gMnSc1	Hong
Kong	Kongo	k1gNnPc2	Kongo
a	a	k8xC	a
Tchaj-wan	Tchajan	k1gInSc1	Tchaj-wan
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
Mexiko	Mexiko	k1gNnSc1	Mexiko
a	a	k8xC	a
Papua	Papu	k2eAgFnSc1d1	Papua
Nová	nový	k2eAgFnSc1d1	nová
Guinea	Guinea	k1gFnSc1	Guinea
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
Chile	Chile	k1gNnSc2	Chile
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
Peru	Peru	k1gNnPc2	Peru
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
a	a	k8xC	a
Vietnam	Vietnam	k1gInSc1	Vietnam
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Cíle	cíl	k1gInPc1	cíl
organizace	organizace	k1gFnSc2	organizace
==	==	k?	==
</s>
</p>
<p>
<s>
APEC	APEC	kA	APEC
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
původně	původně	k6eAd1	původně
jako	jako	k8xS	jako
diskusní	diskusní	k2eAgNnSc1d1	diskusní
fórum	fórum	k1gNnSc1	fórum
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
chtěly	chtít	k5eAaImAgFnP	chtít
řešit	řešit	k5eAaImF	řešit
ekonomické	ekonomický	k2eAgInPc4d1	ekonomický
problémy	problém	k1gInPc4	problém
regionu	region	k1gInSc2	region
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
celé	celá	k1gFnSc6	celá
období	období	k1gNnSc2	období
své	svůj	k3xOyFgFnSc2	svůj
existence	existence	k1gFnSc2	existence
sleduje	sledovat	k5eAaImIp3nS	sledovat
následující	následující	k2eAgInPc4d1	následující
cíle	cíl	k1gInPc4	cíl
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
růst	růst	k1gInSc4	růst
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc4	rozvoj
regionu	region	k1gInSc2	region
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
zvýšení	zvýšení	k1gNnSc4	zvýšení
efektů	efekt	k1gInPc2	efekt
regionu	region	k1gInSc2	region
plynoucích	plynoucí	k2eAgInPc2d1	plynoucí
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
spolupráce	spolupráce	k1gFnSc2	spolupráce
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
rozvoj	rozvoj	k1gInSc1	rozvoj
a	a	k8xC	a
posilování	posilování	k1gNnSc1	posilování
otevřeného	otevřený	k2eAgInSc2d1	otevřený
multilaterálního	multilaterální	k2eAgInSc2d1	multilaterální
systému	systém	k1gInSc2	systém
obchodu	obchod	k1gInSc2	obchod
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
odstranění	odstranění	k1gNnSc1	odstranění
bariér	bariéra	k1gFnPc2	bariéra
obchodu	obchod	k1gInSc2	obchod
se	s	k7c7	s
zbožím	zboží	k1gNnSc7	zboží
<g/>
,	,	kIx,	,
službami	služba	k1gFnPc7	služba
a	a	k8xC	a
investicemi	investice	k1gFnPc7	investice
<g/>
.	.	kIx.	.
<g/>
Naplňováním	naplňování	k1gNnSc7	naplňování
těchto	tento	k3xDgInPc2	tento
cílů	cíl	k1gInPc2	cíl
vedlo	vést	k5eAaImAgNnS	vést
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
regionálního	regionální	k2eAgNnSc2d1	regionální
seskupení	seskupení	k1gNnSc2	seskupení
s	s	k7c7	s
obrovskou	obrovský	k2eAgFnSc7d1	obrovská
ekonomickou	ekonomický	k2eAgFnSc7d1	ekonomická
silou	síla	k1gFnSc7	síla
<g/>
.	.	kIx.	.
</s>
<s>
Státy	stát	k1gInPc1	stát
APEC	APEC	kA	APEC
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
na	na	k7c6	na
třetině	třetina	k1gFnSc6	třetina
rozlohy	rozloha	k1gFnSc2	rozloha
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
přibližně	přibližně	k6eAd1	přibližně
40	[number]	k4	40
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
podílí	podílet	k5eAaImIp3nS	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
více	hodně	k6eAd2	hodně
než	než	k8xS	než
polovině	polovina	k1gFnSc6	polovina
světového	světový	k2eAgNnSc2d1	světové
HDP	HDP	kA	HDP
a	a	k8xC	a
přibližně	přibližně	k6eAd1	přibližně
45	[number]	k4	45
%	%	kIx~	%
světového	světový	k2eAgInSc2d1	světový
obchodu	obchod	k1gInSc2	obchod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
byl	být	k5eAaImAgInS	být
stanoven	stanovit	k5eAaPmNgInS	stanovit
ambiciózní	ambiciózní	k2eAgInSc1d1	ambiciózní
cíl	cíl	k1gInSc1	cíl
−	−	k?	−
vytvoření	vytvoření	k1gNnSc1	vytvoření
zóny	zóna	k1gFnSc2	zóna
volného	volný	k2eAgInSc2d1	volný
obchodu	obchod	k1gInSc2	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
tímto	tento	k3xDgInSc7	tento
účelem	účel	k1gInSc7	účel
byl	být	k5eAaImAgInS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
akční	akční	k2eAgInSc1d1	akční
plán	plán	k1gInSc1	plán
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
vést	vést	k5eAaImF	vést
k	k	k7c3	k
naplňování	naplňování	k1gNnSc3	naplňování
volného	volný	k2eAgInSc2d1	volný
obchodu	obchod	k1gInSc2	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Plán	plán	k1gInSc1	plán
je	být	k5eAaImIp3nS	být
postavený	postavený	k2eAgInSc1d1	postavený
na	na	k7c6	na
dobrovolnosti	dobrovolnost	k1gFnSc6	dobrovolnost
a	a	k8xC	a
korespondují	korespondovat	k5eAaImIp3nP	korespondovat
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
tři	tři	k4xCgInPc4	tři
hlavní	hlavní	k2eAgInPc4d1	hlavní
pilíře	pilíř	k1gInPc4	pilíř
APEC	APEC	kA	APEC
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Obchodní	obchodní	k2eAgFnSc1d1	obchodní
a	a	k8xC	a
investiční	investiční	k2eAgFnSc1d1	investiční
liberalizace	liberalizace	k1gFnSc1	liberalizace
</s>
</p>
<p>
<s>
Ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
a	a	k8xC	a
obchodní	obchodní	k2eAgFnSc1d1	obchodní
spolupráce	spolupráce	k1gFnSc1	spolupráce
</s>
</p>
<p>
<s>
Podpora	podpora	k1gFnSc1	podpora
obchodu	obchod	k1gInSc2	obchod
a	a	k8xC	a
investicOd	investicOd	k1gInSc1	investicOd
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
se	se	k3xPyFc4	se
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
obchod	obchod	k1gInSc1	obchod
a	a	k8xC	a
rostou	růst	k5eAaImIp3nP	růst
investice	investice	k1gFnPc4	investice
v	v	k7c6	v
regionu	region	k1gInSc6	region
<g/>
.	.	kIx.	.
</s>
<s>
Roste	růst	k5eAaImIp3nS	růst
rovněž	rovněž	k9	rovněž
podíl	podíl	k1gInSc4	podíl
vzájemného	vzájemný	k2eAgInSc2d1	vzájemný
obchodu	obchod	k1gInSc2	obchod
mezi	mezi	k7c7	mezi
státy	stát	k1gInPc7	stát
APEC	APEC	kA	APEC
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Asijsko-pacifické	asijskoacifický	k2eAgFnSc2d1	asijsko-pacifická
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
společenství	společenství	k1gNnSc4	společenství
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Asijsko-pacifické	asijskoacifický	k2eAgFnSc2d1	asijsko-pacifická
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
společenství	společenství	k1gNnSc4	společenství
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
společenství	společenství	k1gNnSc2	společenství
</s>
</p>
