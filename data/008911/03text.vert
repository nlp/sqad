<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Etiopie	Etiopie	k1gFnSc2	Etiopie
byla	být	k5eAaImAgFnS	být
přijata	přijat	k2eAgFnSc1d1	přijata
31	[number]	k4	31
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Základ	základ	k1gInSc1	základ
etiopské	etiopský	k2eAgFnSc2d1	etiopská
vlajky	vlajka	k1gFnSc2	vlajka
tvoří	tvořit	k5eAaImIp3nS	tvořit
červená	červený	k2eAgFnSc1d1	červená
<g/>
,	,	kIx,	,
žlutá	žlutý	k2eAgFnSc1d1	žlutá
a	a	k8xC	a
zelená	zelený	k2eAgFnSc1d1	zelená
barva	barva	k1gFnSc1	barva
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
barvy	barva	k1gFnPc1	barva
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
symbolem	symbol	k1gInSc7	symbol
africké	africký	k2eAgFnSc2d1	africká
jednoty	jednota	k1gFnSc2	jednota
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Etiopie	Etiopie	k1gFnSc1	Etiopie
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgInSc1d3	nejstarší
stát	stát	k1gInSc1	stát
subsaharské	subsaharský	k2eAgFnSc2d1	subsaharská
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
nejstarší	starý	k2eAgNnSc4d3	nejstarší
nezávislé	závislý	k2eNgNnSc4d1	nezávislé
africké	africký	k2eAgNnSc4d1	africké
území	území	k1gNnSc4	území
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejchudších	chudý	k2eAgFnPc2d3	nejchudší
zemí	zem	k1gFnPc2	zem
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
kultura	kultura	k1gFnSc1	kultura
je	být	k5eAaImIp3nS	být
však	však	k9	však
velice	velice	k6eAd1	velice
bohatá	bohatý	k2eAgFnSc1d1	bohatá
a	a	k8xC	a
mezi	mezi	k7c4	mezi
etiopské	etiopský	k2eAgNnSc4d1	etiopské
kulturní	kulturní	k2eAgNnSc4d1	kulturní
dědictví	dědictví	k1gNnSc4	dědictví
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
národní	národní	k2eAgFnSc1d1	národní
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Tajemný	tajemný	k2eAgInSc4d1	tajemný
původ	původ	k1gInSc4	původ
etiopské	etiopský	k2eAgFnSc2d1	etiopská
vlajky	vlajka	k1gFnSc2	vlajka
===	===	k?	===
</s>
</p>
<p>
<s>
Kořeny	kořen	k2eAgFnPc1d1	Kořena
etiopské	etiopský	k2eAgFnPc1d1	etiopská
státní	státní	k2eAgFnPc1d1	státní
vlajky	vlajka	k1gFnPc1	vlajka
sahají	sahat	k5eAaImIp3nP	sahat
hluboko	hluboko	k6eAd1	hluboko
do	do	k7c2	do
minulosti	minulost	k1gFnSc2	minulost
<g/>
.	.	kIx.	.
</s>
<s>
Názory	názor	k1gInPc1	názor
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
kdysi	kdysi	k6eAd1	kdysi
dávno	dávno	k6eAd1	dávno
inspirovalo	inspirovat	k5eAaBmAgNnS	inspirovat
Etiopy	Etiop	k1gMnPc7	Etiop
při	při	k7c6	při
výběru	výběr	k1gInSc6	výběr
barev	barva	k1gFnPc2	barva
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
národní	národní	k2eAgInSc4d1	národní
symbol	symbol	k1gInSc4	symbol
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
je	být	k5eAaImIp3nS	být
uváděna	uváděn	k2eAgFnSc1d1	uváděna
duha	duha	k1gFnSc1	duha
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gNnSc6	jejíž
barevném	barevný	k2eAgNnSc6d1	barevné
spektru	spektrum	k1gNnSc6	spektrum
nejvíce	nejvíce	k6eAd1	nejvíce
vynikají	vynikat	k5eAaImIp3nP	vynikat
právě	právě	k9	právě
červená	červený	k2eAgFnSc1d1	červená
<g/>
,	,	kIx,	,
žlutá	žlutý	k2eAgFnSc1d1	žlutá
a	a	k8xC	a
zelená	zelený	k2eAgFnSc1d1	zelená
barva	barva	k1gFnSc1	barva
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
přírodní	přírodní	k2eAgInSc1d1	přírodní
úkaz	úkaz	k1gInSc1	úkaz
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
etiopských	etiopský	k2eAgFnPc6d1	etiopská
klimatických	klimatický	k2eAgFnPc6d1	klimatická
podmínkách	podmínka	k1gFnPc6	podmínka
nikterak	nikterak	k6eAd1	nikterak
ojedinělý	ojedinělý	k2eAgInSc4d1	ojedinělý
<g/>
,	,	kIx,	,
nad	nad	k7c4	nad
vodopády	vodopád	k1gInPc4	vodopád
na	na	k7c6	na
Modrém	modrý	k2eAgInSc6d1	modrý
Nilu	Nil	k1gInSc6	Nil
se	se	k3xPyFc4	se
duha	duha	k1gFnSc1	duha
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
dokonce	dokonce	k9	dokonce
nepřetržitě	přetržitě	k6eNd1	přetržitě
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Možná	možná	k9	možná
<g/>
,	,	kIx,	,
že	že	k8xS	že
podobně	podobně	k6eAd1	podobně
smýšleli	smýšlet	k5eAaImAgMnP	smýšlet
i	i	k9	i
obyvatelé	obyvatel	k1gMnPc1	obyvatel
Bolívie	Bolívie	k1gFnSc2	Bolívie
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
vlajka	vlajka	k1gFnSc1	vlajka
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
té	ten	k3xDgFnSc6	ten
etiopské	etiopský	k2eAgFnSc6d1	etiopská
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
stejný	stejný	k2eAgInSc4d1	stejný
barevný	barevný	k2eAgInSc4d1	barevný
základ	základ	k1gInSc4	základ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nesporným	sporný	k2eNgInSc7d1	nesporný
důkazem	důkaz	k1gInSc7	důkaz
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
červená	červený	k2eAgFnSc1d1	červená
<g/>
,	,	kIx,	,
žlutá	žlutý	k2eAgFnSc1d1	žlutá
a	a	k8xC	a
zelená	zelený	k2eAgFnSc1d1	zelená
barva	barva	k1gFnSc1	barva
jsou	být	k5eAaImIp3nP	být
užívány	užíván	k2eAgInPc1d1	užíván
etiopským	etiopský	k2eAgMnSc7d1	etiopský
lidem	člověk	k1gMnPc3	člověk
jako	jako	k9	jako
barvy	barva	k1gFnPc4	barva
národní	národní	k2eAgFnPc4d1	národní
už	už	k6eAd1	už
několik	několik	k4yIc4	několik
staletí	staletí	k1gNnPc2	staletí
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Orteliova	Orteliův	k2eAgFnSc1d1	Orteliův
mapa	mapa	k1gFnSc1	mapa
Vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
a	a	k8xC	a
Vnější	vnější	k2eAgFnSc1d1	vnější
Etiopie	Etiopie	k1gFnSc1	Etiopie
(	(	kIx(	(
<g/>
souhrnně	souhrnně	k6eAd1	souhrnně
Abyssinie	Abyssinie	k1gFnSc1	Abyssinie
<g/>
)	)	kIx)	)
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1573	[number]	k4	1573
<g/>
.	.	kIx.	.
</s>
<s>
Belgický	belgický	k2eAgMnSc1d1	belgický
kartograf	kartograf	k1gMnSc1	kartograf
ozdobil	ozdobit	k5eAaPmAgMnS	ozdobit
nadpis	nadpis	k1gInSc4	nadpis
této	tento	k3xDgFnSc2	tento
mapy	mapa	k1gFnSc2	mapa
obrázky	obrázek	k1gInPc1	obrázek
muže	muž	k1gMnSc2	muž
<g/>
,	,	kIx,	,
ženy	žena	k1gFnSc2	žena
a	a	k8xC	a
dvou	dva	k4xCgFnPc2	dva
dětí	dítě	k1gFnPc2	dítě
nesoucích	nesoucí	k2eAgFnPc2d1	nesoucí
slunečníky	slunečník	k1gInPc4	slunečník
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc4	tento
postavy	postava	k1gFnPc4	postava
symbolizující	symbolizující	k2eAgFnSc4d1	symbolizující
rodinu	rodina	k1gFnSc4	rodina
etiopských	etiopský	k2eAgMnPc2d1	etiopský
domorodců	domorodec	k1gMnPc2	domorodec
jsou	být	k5eAaImIp3nP	být
oděny	odět	k5eAaPmNgInP	odět
v	v	k7c6	v
šatech	šat	k1gInPc6	šat
ušitých	ušitý	k2eAgInPc6d1	ušitý
z	z	k7c2	z
červené	červená	k1gFnSc2	červená
<g/>
,	,	kIx,	,
žluté	žlutý	k2eAgFnPc4d1	žlutá
a	a	k8xC	a
zelené	zelený	k2eAgFnPc4d1	zelená
látky	látka	k1gFnPc4	látka
<g/>
.	.	kIx.	.
</s>
<s>
Plátno	plátno	k1gNnSc1	plátno
pod	pod	k7c7	pod
nadpisem	nadpis	k1gInSc7	nadpis
a	a	k8xC	a
slunečníky	slunečník	k1gInPc1	slunečník
jsou	být	k5eAaImIp3nP	být
rovněž	rovněž	k9	rovněž
vyobrazeny	vyobrazen	k2eAgInPc1d1	vyobrazen
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
barvách	barva	k1gFnPc6	barva
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
také	také	k9	také
svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
odedávné	odedávný	k2eAgFnSc6d1	odedávná
národní	národní	k2eAgFnSc6d1	národní
hrdosti	hrdost	k1gFnSc6	hrdost
Etiopů	Etiop	k1gMnPc2	Etiop
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc3	jejich
lásce	láska	k1gFnSc3	láska
ke	k	k7c3	k
státní	státní	k2eAgFnSc3d1	státní
vlajce	vlajka	k1gFnSc3	vlajka
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc7	který
nazývají	nazývat	k5eAaImIp3nP	nazývat
Sendek	Sendek	k1gInSc4	Sendek
Alama	Alama	k?	Alama
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
v	v	k7c6	v
amharštině	amharština	k1gFnSc6	amharština
<g/>
.	.	kIx.	.
doslova	doslova	k6eAd1	doslova
znamená	znamenat	k5eAaImIp3nS	znamenat
národní	národní	k2eAgInSc4d1	národní
symbol	symbol	k1gInSc4	symbol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Význam	význam	k1gInSc1	význam
základních	základní	k2eAgFnPc2d1	základní
barev	barva	k1gFnPc2	barva
etiopské	etiopský	k2eAgFnSc2d1	etiopská
vlajky	vlajka	k1gFnSc2	vlajka
===	===	k?	===
</s>
</p>
<p>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
její	její	k3xOp3gInSc4	její
původ	původ	k1gInSc4	původ
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
význam	význam	k1gInSc4	význam
základních	základní	k2eAgFnPc2d1	základní
barev	barva	k1gFnPc2	barva
etiopské	etiopský	k2eAgFnSc2d1	etiopská
vlajky	vlajka	k1gFnSc2	vlajka
není	být	k5eNaImIp3nS	být
úplně	úplně	k6eAd1	úplně
jasný	jasný	k2eAgInSc1d1	jasný
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
interpretace	interpretace	k1gFnPc1	interpretace
bývají	bývat	k5eAaImIp3nP	bývat
odlišné	odlišný	k2eAgFnPc1d1	odlišná
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
variant	varianta	k1gFnPc2	varianta
je	být	k5eAaImIp3nS	být
vzájemné	vzájemný	k2eAgNnSc1d1	vzájemné
propojení	propojení	k1gNnSc1	propojení
a	a	k8xC	a
síla	síla	k1gFnSc1	síla
jednoty	jednota	k1gFnSc2	jednota
Svaté	svatý	k2eAgFnSc2d1	svatá
trojice	trojice	k1gFnSc2	trojice
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
pouto	pouto	k1gNnSc1	pouto
etiopského	etiopský	k2eAgInSc2d1	etiopský
lidu	lid	k1gInSc2	lid
k	k	k7c3	k
Bohu	bůh	k1gMnSc3	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Vyznavači	vyznavač	k1gMnPc1	vyznavač
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
zemi	zem	k1gFnSc6	zem
velmi	velmi	k6eAd1	velmi
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
tradici	tradice	k1gFnSc4	tradice
<g/>
,	,	kIx,	,
sice	sice	k8xC	sice
tvoří	tvořit	k5eAaImIp3nP	tvořit
většinu	většina	k1gFnSc4	většina
etiopské	etiopský	k2eAgFnSc2d1	etiopská
populace	populace	k1gFnSc2	populace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vůči	vůči	k7c3	vůči
zdejším	zdejší	k2eAgFnPc3d1	zdejší
náboženským	náboženský	k2eAgFnPc3d1	náboženská
minoritám	minorita	k1gFnPc3	minorita
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
výklad	výklad	k1gInSc1	výklad
významu	význam	k1gInSc2	význam
barev	barva	k1gFnPc2	barva
národní	národní	k2eAgFnSc2d1	národní
vlajky	vlajka	k1gFnSc2	vlajka
poněkud	poněkud	k6eAd1	poněkud
diskriminující	diskriminující	k2eAgFnSc1d1	diskriminující
<g/>
.	.	kIx.	.
</s>
<s>
Mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
neutrální	neutrální	k2eAgFnSc1d1	neutrální
interpretace	interpretace	k1gFnSc1	interpretace
je	být	k5eAaImIp3nS	být
ta	ten	k3xDgFnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
barvy	barva	k1gFnPc1	barva
značí	značit	k5eAaImIp3nP	značit
víru	víra	k1gFnSc4	víra
<g/>
,	,	kIx,	,
naději	naděje	k1gFnSc4	naděje
a	a	k8xC	a
laskavost	laskavost	k1gFnSc4	laskavost
boží	boží	k2eAgFnSc4d1	boží
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
však	však	k9	však
setkáváme	setkávat	k5eAaImIp1nP	setkávat
s	s	k7c7	s
myšlenkou	myšlenka	k1gFnSc7	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
zelená	zelený	k2eAgFnSc1d1	zelená
barva	barva	k1gFnSc1	barva
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
zemi	zem	k1gFnSc4	zem
a	a	k8xC	a
dar	dar	k1gInSc4	dar
její	její	k3xOp3gFnSc2	její
úrodnosti	úrodnost	k1gFnSc2	úrodnost
<g/>
,	,	kIx,	,
žlutá	žlutat	k5eAaImIp3nS	žlutat
mír	mír	k1gInSc4	mír
a	a	k8xC	a
harmonii	harmonie	k1gFnSc4	harmonie
mezi	mezi	k7c7	mezi
různými	různý	k2eAgNnPc7d1	různé
etniky	etnikum	k1gNnPc7	etnikum
a	a	k8xC	a
náboženskými	náboženský	k2eAgFnPc7d1	náboženská
skupinami	skupina	k1gFnPc7	skupina
zde	zde	k6eAd1	zde
žijícími	žijící	k2eAgFnPc7d1	žijící
a	a	k8xC	a
červená	červenat	k5eAaImIp3nS	červenat
sílu	síla	k1gFnSc4	síla
a	a	k8xC	a
krev	krev	k1gFnSc4	krev
Etiopů	Etiop	k1gMnPc2	Etiop
prolitou	prolitý	k2eAgFnSc4d1	prolitá
při	při	k7c6	při
obraně	obrana	k1gFnSc6	obrana
své	svůj	k3xOyFgFnSc2	svůj
vlasti	vlast	k1gFnSc2	vlast
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vývoj	vývoj	k1gInSc1	vývoj
etiopské	etiopský	k2eAgFnSc2d1	etiopská
vlajky	vlajka	k1gFnSc2	vlajka
v	v	k7c6	v
období	období	k1gNnSc6	období
před	před	k7c7	před
italskou	italský	k2eAgFnSc7d1	italská
okupací	okupace	k1gFnSc7	okupace
===	===	k?	===
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
oficiálně	oficiálně	k6eAd1	oficiálně
přijata	přijat	k2eAgFnSc1d1	přijata
státní	státní	k2eAgFnSc1d1	státní
vlajka	vlajka	k1gFnSc1	vlajka
Etiopie	Etiopie	k1gFnSc1	Etiopie
byla	být	k5eAaImAgFnS	být
složena	složit	k5eAaPmNgFnS	složit
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
samostatných	samostatný	k2eAgFnPc2d1	samostatná
<g/>
,	,	kIx,	,
vertikálně	vertikálně	k6eAd1	vertikálně
řazených	řazený	k2eAgInPc2d1	řazený
barevných	barevný	k2eAgInPc2d1	barevný
praporů	prapor	k1gInPc2	prapor
trojúhelníkových	trojúhelníkový	k2eAgInPc2d1	trojúhelníkový
tvarů	tvar	k1gInPc2	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýše	vysoce	k6eAd3	vysoce
byl	být	k5eAaImAgInS	být
umístěn	umístit	k5eAaPmNgInS	umístit
červený	červený	k2eAgInSc1d1	červený
<g/>
,	,	kIx,	,
uprostřed	uprostřed	k6eAd1	uprostřed
žlutý	žlutý	k2eAgMnSc1d1	žlutý
a	a	k8xC	a
nejníže	nízce	k6eAd3	nízce
zelený	zelený	k2eAgInSc1d1	zelený
prapor	prapor	k1gInSc1	prapor
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
kombinaci	kombinace	k1gFnSc4	kombinace
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1881	[number]	k4	1881
císař	císař	k1gMnSc1	císař
Yohannes	Yohannes	k1gMnSc1	Yohannes
IV	IV	kA	IV
<g/>
.	.	kIx.	.
za	za	k7c4	za
symbol	symbol	k1gInSc4	symbol
Etiopského	etiopský	k2eAgNnSc2d1	etiopské
císařství	císařství	k1gNnSc2	císařství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
6	[number]	k4	6
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1897	[number]	k4	1897
<g/>
,	,	kIx,	,
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
císaře	císař	k1gMnSc2	císař
Menelika	Menelik	k1gMnSc2	Menelik
II	II	kA	II
<g/>
.	.	kIx.	.
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
spojení	spojení	k1gNnSc3	spojení
trojúhelníkových	trojúhelníkový	k2eAgInPc2d1	trojúhelníkový
praporů	prapor	k1gInPc2	prapor
v	v	k7c4	v
souvislou	souvislý	k2eAgFnSc4d1	souvislá
vlajku	vlajka	k1gFnSc4	vlajka
obdélníkového	obdélníkový	k2eAgInSc2d1	obdélníkový
tvaru	tvar	k1gInSc2	tvar
s	s	k7c7	s
poměrem	poměr	k1gInSc7	poměr
stran	stran	k7c2	stran
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
státní	státní	k2eAgInSc1d1	státní
symbol	symbol	k1gInSc1	symbol
Etiopie	Etiopie	k1gFnSc2	Etiopie
byl	být	k5eAaImAgInS	být
také	také	k9	také
používán	používat	k5eAaImNgInS	používat
jako	jako	k9	jako
znak	znak	k1gInSc4	znak
jejího	její	k3xOp3gMnSc2	její
císaře	císař	k1gMnSc2	císař
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
středu	střed	k1gInSc6	střed
nacházely	nacházet	k5eAaImAgFnP	nacházet
Menelikovy	Menelikův	k2eAgFnPc1d1	Menelikův
iniciály	iniciála	k1gFnPc1	iniciála
červené	červený	k2eAgFnSc2d1	červená
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
prohození	prohození	k1gNnSc3	prohození
umístění	umístění	k1gNnSc2	umístění
červeného	červený	k2eAgInSc2d1	červený
a	a	k8xC	a
zeleného	zelený	k2eAgInSc2d1	zelený
pruhu	pruh	k1gInSc2	pruh
na	na	k7c6	na
etiopské	etiopský	k2eAgFnSc6d1	etiopská
vlajce	vlajka	k1gFnSc6	vlajka
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
za	za	k7c4	za
vlády	vláda	k1gFnPc4	vláda
císařovny	císařovna	k1gFnSc2	císařovna
Zewditu	Zewdita	k1gFnSc4	Zewdita
I.	I.	kA	I.
</s>
</p>
<p>
<s>
===	===	k?	===
Etiopská	etiopský	k2eAgFnSc1d1	etiopská
vlajka	vlajka	k1gFnSc1	vlajka
během	během	k7c2	během
italské	italský	k2eAgFnSc2d1	italská
okupace	okupace	k1gFnSc2	okupace
===	===	k?	===
</s>
</p>
<p>
<s>
Italská	italský	k2eAgFnSc1d1	italská
fašistická	fašistický	k2eAgFnSc1d1	fašistická
vojska	vojsko	k1gNnPc4	vojsko
vtrhla	vtrhnout	k5eAaPmAgFnS	vtrhnout
do	do	k7c2	do
Etiopie	Etiopie	k1gFnSc2	Etiopie
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1935	[number]	k4	1935
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
obsadila	obsadit	k5eAaPmAgFnS	obsadit
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Addis	Addis	k1gFnSc2	Addis
Abebu	Abeb	k1gInSc2	Abeb
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
níž	jenž	k3xRgFnSc7	jenž
zavlála	zavlát	k5eAaPmAgFnS	zavlát
vlajka	vlajka	k1gFnSc1	vlajka
Italského	italský	k2eAgNnSc2d1	italské
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Etiopská	etiopský	k2eAgFnSc1d1	etiopská
národní	národní	k2eAgFnSc1d1	národní
trikolóra	trikolóra	k1gFnSc1	trikolóra
byla	být	k5eAaImAgFnS	být
pečlivě	pečlivě	k6eAd1	pečlivě
uschována	uschovat	k5eAaPmNgFnS	uschovat
až	až	k9	až
do	do	k7c2	do
května	květen	k1gInSc2	květen
1941	[number]	k4	1941
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
spojenecká	spojenecký	k2eAgFnSc1d1	spojenecká
armáda	armáda	k1gFnSc1	armáda
nadobro	nadobro	k6eAd1	nadobro
porazila	porazit	k5eAaPmAgFnS	porazit
fašisty	fašista	k1gMnPc4	fašista
okupující	okupující	k2eAgFnSc4d1	okupující
tuto	tento	k3xDgFnSc4	tento
africkou	africký	k2eAgFnSc4d1	africká
zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Etiopie	Etiopie	k1gFnSc1	Etiopie
znovu	znovu	k6eAd1	znovu
získala	získat	k5eAaPmAgFnS	získat
ztracenou	ztracený	k2eAgFnSc4d1	ztracená
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Lev	Lev	k1gMnSc1	Lev
z	z	k7c2	z
Judeje	Judea	k1gFnSc2	Judea
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
etiopské	etiopský	k2eAgFnSc2d1	etiopská
vlajky	vlajka	k1gFnSc2	vlajka
===	===	k?	===
</s>
</p>
<p>
<s>
Nejznámějším	známý	k2eAgInSc7d3	nejznámější
obrazcem	obrazec	k1gInSc7	obrazec
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
kdy	kdy	k6eAd1	kdy
objevil	objevit	k5eAaPmAgMnS	objevit
na	na	k7c6	na
etiopské	etiopský	k2eAgFnSc6d1	etiopská
vlajce	vlajka	k1gFnSc6	vlajka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
symbol	symbol	k1gInSc1	symbol
císaře	císař	k1gMnSc2	císař
Haile	Hail	k1gMnSc2	Hail
Selassieho	Selassie	k1gMnSc2	Selassie
I.	I.	kA	I.
<g/>
,	,	kIx,	,
Lev	Lev	k1gMnSc1	Lev
z	z	k7c2	z
Judeje	Judea	k1gFnSc2	Judea
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1941	[number]	k4	1941
se	se	k3xPyFc4	se
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
etiopského	etiopský	k2eAgMnSc2d1	etiopský
císaře	císař	k1gMnSc2	císař
vrátil	vrátit	k5eAaPmAgInS	vrátit
Haile	Haile	k1gInSc1	Haile
Selassie	Selassie	k1gFnSc2	Selassie
I.	I.	kA	I.
Téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
středu	střed	k1gInSc2	střed
národní	národní	k2eAgFnSc2d1	národní
vlajky	vlajka	k1gFnSc2	vlajka
přidán	přidán	k2eAgInSc1d1	přidán
panovníkův	panovníkův	k2eAgInSc1d1	panovníkův
symbol	symbol	k1gInSc1	symbol
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
nejen	nejen	k6eAd1	nejen
znamením	znamení	k1gNnSc7	znamení
samotného	samotný	k2eAgMnSc2d1	samotný
vladaře	vladař	k1gMnSc2	vladař
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
celé	celý	k2eAgFnSc2d1	celá
Etiopské	etiopský	k2eAgFnSc2d1	etiopská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Nejednalo	jednat	k5eNaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
císařovy	císařův	k2eAgFnPc4d1	císařova
iniciály	iniciála	k1gFnPc4	iniciála
jako	jako	k8xC	jako
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Menelika	Menelik	k1gMnSc2	Menelik
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c4	o
obraz	obraz	k1gInSc4	obraz
Lva	lev	k1gInSc2	lev
z	z	k7c2	z
Judeje	Judea	k1gFnSc2	Judea
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
Lev	lev	k1gInSc1	lev
<g/>
,	,	kIx,	,
kráčející	kráčející	k2eAgInSc1d1	kráčející
na	na	k7c4	na
východ	východ	k1gInSc4	východ
<g/>
,	,	kIx,	,
nese	nést	k5eAaImIp3nS	nést
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
zlatou	zlatý	k2eAgFnSc4d1	zlatá
korunu	koruna	k1gFnSc4	koruna
etiopského	etiopský	k2eAgMnSc4d1	etiopský
císaře	císař	k1gMnSc4	císař
a	a	k8xC	a
pravou	pravý	k2eAgFnSc4d1	pravá
přední	přední	k2eAgFnSc4d1	přední
tlapu	tlapa	k1gFnSc4	tlapa
má	mít	k5eAaImIp3nS	mít
vztyčenou	vztyčený	k2eAgFnSc7d1	vztyčená
<g/>
.	.	kIx.	.
</s>
<s>
Drží	držet	k5eAaImIp3nS	držet
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
zlatou	zlatý	k2eAgFnSc4d1	zlatá
tyč	tyč	k1gFnSc4	tyč
zakončenou	zakončený	k2eAgFnSc4d1	zakončená
křížem	kříž	k1gInSc7	kříž
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
Koptskou	koptský	k2eAgFnSc4d1	koptská
pravoslavnou	pravoslavný	k2eAgFnSc4d1	pravoslavná
církev	církev	k1gFnSc4	církev
<g/>
.	.	kIx.	.
</s>
<s>
Těsně	těsně	k6eAd1	těsně
pod	pod	k7c7	pod
křížem	kříž	k1gInSc7	kříž
je	být	k5eAaImIp3nS	být
uvázána	uvázán	k2eAgFnSc1d1	uvázána
vlající	vlající	k2eAgFnSc1d1	vlající
etiopská	etiopský	k2eAgFnSc1d1	etiopská
trikolóra	trikolóra	k1gFnSc1	trikolóra
<g/>
.	.	kIx.	.
</s>
<s>
Příznivci	příznivec	k1gMnPc1	příznivec
rastafariánství	rastafariánství	k1gNnSc2	rastafariánství
<g/>
,	,	kIx,	,
náboženského	náboženský	k2eAgNnSc2d1	náboženské
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
zrodilo	zrodit	k5eAaPmAgNnS	zrodit
na	na	k7c6	na
Jamajce	Jamajka	k1gFnSc6	Jamajka
<g/>
,	,	kIx,	,
přijali	přijmout	k5eAaPmAgMnP	přijmout
tuto	tento	k3xDgFnSc4	tento
vlajku	vlajka	k1gFnSc4	vlajka
za	za	k7c4	za
symbol	symbol	k1gInSc4	symbol
své	svůj	k3xOyFgFnSc2	svůj
víry	víra	k1gFnSc2	víra
<g/>
.	.	kIx.	.
</s>
<s>
Rastafariáni	Rastafarián	k1gMnPc1	Rastafarián
také	také	k9	také
považují	považovat	k5eAaImIp3nP	považovat
císaře	císař	k1gMnSc2	císař
Haile	Hail	k1gMnSc2	Hail
Selassieho	Selassie	k1gMnSc2	Selassie
I.	I.	kA	I.
za	za	k7c4	za
ztělesnění	ztělesnění	k1gNnSc4	ztělesnění
Boha	bůh	k1gMnSc2	bůh
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
počest	počest	k1gFnSc4	počest
císaře	císař	k1gMnSc2	císař
Haile	Haile	k1gFnSc2	Haile
Selassieho	Selassie	k1gMnSc2	Selassie
I.	I.	kA	I.
byla	být	k5eAaImAgFnS	být
rovněž	rovněž	k9	rovněž
vytvořena	vytvořen	k2eAgFnSc1d1	vytvořena
oboustranná	oboustranný	k2eAgFnSc1d1	oboustranná
standarta	standarta	k1gFnSc1	standarta
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
vyvěšovala	vyvěšovat	k5eAaImAgFnS	vyvěšovat
při	při	k7c6	při
slavnostních	slavnostní	k2eAgFnPc6d1	slavnostní
příležitostech	příležitost	k1gFnPc6	příležitost
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc4	její
základ	základ	k1gInSc4	základ
tvořila	tvořit	k5eAaImAgFnS	tvořit
etiopská	etiopský	k2eAgFnSc1d1	etiopská
trikolóra	trikolóra	k1gFnSc1	trikolóra
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přední	přední	k2eAgFnSc6d1	přední
straně	strana	k1gFnSc6	strana
byl	být	k5eAaImAgInS	být
vyobrazen	vyobrazen	k2eAgInSc1d1	vyobrazen
Lev	lev	k1gInSc1	lev
z	z	k7c2	z
Judeje	Judea	k1gFnSc2	Judea
uprostřed	uprostřed	k7c2	uprostřed
Šalamounova	Šalamounův	k2eAgInSc2d1	Šalamounův
řádu	řád	k1gInSc2	řád
<g/>
,	,	kIx,	,
nejvyššího	vysoký	k2eAgNnSc2d3	nejvyšší
etiopského	etiopský	k2eAgNnSc2d1	etiopské
státního	státní	k2eAgNnSc2d1	státní
vyznamenání	vyznamenání	k1gNnSc2	vyznamenání
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zadní	zadní	k2eAgFnSc6d1	zadní
straně	strana	k1gFnSc6	strana
byl	být	k5eAaImAgMnS	být
uprostřed	uprostřed	k7c2	uprostřed
téhož	týž	k3xTgInSc2	týž
řádu	řád	k1gInSc2	řád
znázorněn	znázorněn	k2eAgInSc1d1	znázorněn
sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
,	,	kIx,	,
patron	patron	k1gMnSc1	patron
Etiopské	etiopský	k2eAgFnSc2d1	etiopská
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
sedící	sedící	k2eAgFnSc1d1	sedící
na	na	k7c6	na
bílém	bílé	k1gNnSc6	bílé
vzpínajícím	vzpínající	k2eAgNnSc6d1	vzpínající
se	se	k3xPyFc4	se
koni	kůň	k1gMnSc3	kůň
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
kopím	kopí	k1gNnSc7	kopí
probodává	probodávat	k5eAaImIp3nS	probodávat
draka	drak	k1gMnSc4	drak
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
standarty	standarta	k1gFnSc2	standarta
byla	být	k5eAaImAgNnP	být
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
rohu	roh	k1gInSc6	roh
umístěna	umístit	k5eAaPmNgFnS	umístit
zlatá	zlatý	k2eAgFnSc1d1	zlatá
Šalamounova	Šalamounův	k2eAgFnSc1d1	Šalamounova
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
šesticípá	šesticípý	k2eAgFnSc1d1	šesticípá
Davidova	Davidův	k2eAgFnSc1d1	Davidova
hvězda	hvězda	k1gFnSc1	hvězda
s	s	k7c7	s
křížem	kříž	k1gInSc7	kříž
uprostřed	uprostřed	k6eAd1	uprostřed
<g/>
.	.	kIx.	.
</s>
<s>
Touto	tento	k3xDgFnSc7	tento
standartou	standarta	k1gFnSc7	standarta
byla	být	k5eAaImAgFnS	být
přikryta	přikryt	k2eAgFnSc1d1	přikryta
rakev	rakev	k1gFnSc1	rakev
císaře	císař	k1gMnSc2	císař
Haile	Hail	k1gMnSc2	Hail
Selassieho	Selassie	k1gMnSc2	Selassie
I.	I.	kA	I.
při	při	k7c6	při
jeho	jeho	k3xOp3gInSc6	jeho
velkolepém	velkolepý	k2eAgInSc6d1	velkolepý
pohřbu	pohřeb	k1gInSc6	pohřeb
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Dopad	dopad	k1gInSc1	dopad
vojenského	vojenský	k2eAgInSc2d1	vojenský
převratu	převrat	k1gInSc2	převrat
a	a	k8xC	a
socialistických	socialistický	k2eAgFnPc2d1	socialistická
reforem	reforma	k1gFnPc2	reforma
na	na	k7c4	na
podobu	podoba	k1gFnSc4	podoba
etiopské	etiopský	k2eAgFnSc2d1	etiopská
vlajky	vlajka	k1gFnSc2	vlajka
===	===	k?	===
</s>
</p>
<p>
<s>
Když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
císař	císař	k1gMnSc1	císař
Haile	Hail	k1gMnSc2	Hail
Selassie	Selassie	k1gFnSc2	Selassie
I.	I.	kA	I.
12	[number]	k4	12
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1974	[number]	k4	1974
zbaven	zbaven	k2eAgInSc1d1	zbaven
moci	moct	k5eAaImF	moct
a	a	k8xC	a
kontroly	kontrola	k1gFnSc2	kontrola
nad	nad	k7c7	nad
státem	stát	k1gInSc7	stát
se	se	k3xPyFc4	se
chopila	chopit	k5eAaPmAgFnS	chopit
vojenská	vojenský	k2eAgFnSc1d1	vojenská
junta	junta	k1gFnSc1	junta
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
k	k	k7c3	k
úpravě	úprava	k1gFnSc3	úprava
národní	národní	k2eAgFnSc2d1	národní
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
byla	být	k5eAaImAgFnS	být
odstraněna	odstranit	k5eAaPmNgFnS	odstranit
císařská	císařský	k2eAgFnSc1d1	císařská
koruna	koruna	k1gFnSc1	koruna
z	z	k7c2	z
hlavy	hlava	k1gFnSc2	hlava
Judejského	Judejský	k2eAgInSc2d1	Judejský
Lva	lev	k1gInSc2	lev
a	a	k8xC	a
zlatá	zlatý	k2eAgFnSc1d1	zlatá
tyč	tyč	k1gFnSc1	tyč
zakončená	zakončený	k2eAgFnSc1d1	zakončená
křížem	křížem	k6eAd1	křížem
nahrazena	nahrazen	k2eAgFnSc1d1	nahrazena
kopím	kopit	k5eAaImIp1nS	kopit
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgMnSc1d1	samotný
lev	lev	k1gMnSc1	lev
otočen	otočit	k5eAaPmNgMnS	otočit
nebyl	být	k5eNaImAgMnS	být
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
divokého	divoký	k2eAgMnSc2d1	divoký
bílého	bílý	k2eAgMnSc2d1	bílý
koně	kůň	k1gMnSc2	kůň
vyobrazeného	vyobrazený	k2eAgMnSc2d1	vyobrazený
na	na	k7c6	na
venezuelském	venezuelský	k2eAgInSc6d1	venezuelský
státním	státní	k2eAgInSc6d1	státní
znaku	znak	k1gInSc6	znak
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
socialistických	socialistický	k2eAgFnPc2d1	socialistická
reforem	reforma	k1gFnPc2	reforma
změnil	změnit	k5eAaPmAgMnS	změnit
směr	směr	k1gInSc4	směr
svého	svůj	k3xOyFgNnSc2	svůj
cválání	cválání	k1gNnSc2	cválání
z	z	k7c2	z
východu	východ	k1gInSc2	východ
na	na	k7c4	na
západ	západ	k1gInSc4	západ
(	(	kIx(	(
<g/>
zprava	zprava	k6eAd1	zprava
doleva	doleva	k6eAd1	doleva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
byla	být	k5eAaImAgFnS	být
sice	sice	k8xC	sice
modifikována	modifikován	k2eAgFnSc1d1	modifikována
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obraz	obraz	k1gInSc1	obraz
Lva	Lev	k1gMnSc2	Lev
nacházejícího	nacházející	k2eAgMnSc2d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
středu	střed	k1gInSc6	střed
stále	stále	k6eAd1	stále
připomínal	připomínat	k5eAaImAgMnS	připomínat
bývalého	bývalý	k2eAgMnSc4d1	bývalý
monarchu	monarcha	k1gMnSc4	monarcha
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgInS	být
přesně	přesně	k6eAd1	přesně
po	po	k7c6	po
roce	rok	k1gInSc6	rok
ode	ode	k7c2	ode
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
Dergové	Derg	k1gMnPc1	Derg
chopili	chopit	k5eAaPmAgMnP	chopit
moci	moct	k5eAaImF	moct
v	v	k7c6	v
Etiopii	Etiopie	k1gFnSc6	Etiopie
<g/>
,	,	kIx,	,
z	z	k7c2	z
národní	národní	k2eAgFnSc2d1	národní
vlajky	vlajka	k1gFnSc2	vlajka
úplně	úplně	k6eAd1	úplně
odstraněn	odstranit	k5eAaPmNgInS	odstranit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
místo	místo	k6eAd1	místo
zaujal	zaujmout	k5eAaPmAgInS	zaujmout
na	na	k7c4	na
dalších	další	k2eAgNnPc2d1	další
12	[number]	k4	12
let	léto	k1gNnPc2	léto
znak	znak	k1gInSc1	znak
vládnoucí	vládnoucí	k2eAgFnSc2d1	vládnoucí
vojenské	vojenský	k2eAgFnSc2d1	vojenská
junty	junta	k1gFnSc2	junta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
10	[number]	k4	10
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1987	[number]	k4	1987
<g/>
,	,	kIx,	,
při	při	k7c6	při
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
Etiopské	etiopský	k2eAgFnSc2d1	etiopská
lidově-demokratické	lidověemokratický	k2eAgFnSc2d1	lidově-demokratická
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
státní	státní	k2eAgFnSc2d1	státní
vlajky	vlajka	k1gFnSc2	vlajka
objevil	objevit	k5eAaPmAgMnS	objevit
nový	nový	k2eAgInSc4d1	nový
emblém	emblém	k1gInSc4	emblém
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
dominantou	dominanta	k1gFnSc7	dominanta
je	být	k5eAaImIp3nS	být
veliké	veliký	k2eAgNnSc1d1	veliké
ozubené	ozubený	k2eAgNnSc1d1	ozubené
kolo	kolo	k1gNnSc1	kolo
<g/>
,	,	kIx,	,
symbol	symbol	k1gInSc1	symbol
dělnické	dělnický	k2eAgFnSc2d1	Dělnická
třídy	třída	k1gFnSc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
z	z	k7c2	z
části	část	k1gFnSc2	část
zakrývá	zakrývat	k5eAaImIp3nS	zakrývat
Aksumský	Aksumský	k2eAgInSc1d1	Aksumský
obelisk	obelisk	k1gInSc1	obelisk
<g/>
,	,	kIx,	,
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
historických	historický	k2eAgFnPc2d1	historická
památek	památka	k1gFnPc2	památka
afrického	africký	k2eAgInSc2d1	africký
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
,	,	kIx,	,
tyčící	tyčící	k2eAgFnSc7d1	tyčící
se	se	k3xPyFc4	se
v	v	k7c6	v
pozadí	pozadí	k1gNnSc6	pozadí
<g/>
.	.	kIx.	.
</s>
<s>
Spodní	spodní	k2eAgFnSc1d1	spodní
část	část	k1gFnSc1	část
ozubeného	ozubený	k2eAgNnSc2d1	ozubené
kola	kolo	k1gNnSc2	kolo
překrývá	překrývat	k5eAaImIp3nS	překrývat
červená	červený	k2eAgFnSc1d1	červená
stuha	stuha	k1gFnSc1	stuha
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
zlatá	zlatý	k2eAgFnSc1d1	zlatá
lví	lví	k2eAgFnSc1d1	lví
hlava	hlava	k1gFnSc1	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
obou	dva	k4xCgInPc2	dva
konců	konec	k1gInPc2	konec
této	tento	k3xDgFnSc2	tento
stuhy	stuha	k1gFnSc2	stuha
se	se	k3xPyFc4	se
vypínají	vypínat	k5eAaImIp3nP	vypínat
olivové	olivový	k2eAgFnPc1d1	olivová
větvě	větvit	k5eAaImSgInS	větvit
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
utváří	utvářet	k5eAaImIp3nS	utvářet
kruhový	kruhový	k2eAgInSc4d1	kruhový
tvar	tvar	k1gInSc4	tvar
emblému	emblém	k1gInSc2	emblém
<g/>
.	.	kIx.	.
</s>
<s>
Zpoza	zpoza	k7c2	zpoza
kola	kolo	k1gNnSc2	kolo
vyčnívá	vyčnívat	k5eAaImIp3nS	vyčnívat
žezlo	žezlo	k1gNnSc4	žezlo
a	a	k8xC	a
kopí	kopí	k1gNnSc4	kopí
<g/>
.	.	kIx.	.
</s>
<s>
Žlutá	žlutý	k2eAgFnSc1d1	žlutá
pěticípá	pěticípý	k2eAgFnSc1d1	pěticípá
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
,	,	kIx,	,
znázorněna	znázornit	k5eAaPmNgFnS	znázornit
na	na	k7c6	na
červeném	červený	k2eAgInSc6d1	červený
kotouči	kotouč	k1gInSc6	kotouč
<g/>
,	,	kIx,	,
jasně	jasně	k6eAd1	jasně
září	zářit	k5eAaImIp3nP	zářit
na	na	k7c6	na
modré	modrý	k2eAgFnSc6d1	modrá
obloze	obloha	k1gFnSc6	obloha
nad	nad	k7c7	nad
objekty	objekt	k1gInPc7	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vrcholku	vrcholek	k1gInSc6	vrcholek
emblému	emblém	k1gInSc2	emblém
je	být	k5eAaImIp3nS	být
napsán	napsat	k5eAaPmNgInS	napsat
název	název	k1gInSc1	název
státu	stát	k1gInSc2	stát
v	v	k7c6	v
amhárštině	amhárština	k1gFnSc6	amhárština
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
symbol	symbol	k1gInSc1	symbol
zmizel	zmizet	k5eAaPmAgInS	zmizet
z	z	k7c2	z
etiopské	etiopský	k2eAgFnSc2d1	etiopská
vlajky	vlajka	k1gFnSc2	vlajka
28	[number]	k4	28
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
socialistický	socialistický	k2eAgInSc1d1	socialistický
politický	politický	k2eAgInSc1d1	politický
systém	systém	k1gInSc1	systém
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
zhroutil	zhroutit	k5eAaPmAgInS	zhroutit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Současná	současný	k2eAgFnSc1d1	současná
podoba	podoba	k1gFnSc1	podoba
etiopské	etiopský	k2eAgFnSc2d1	etiopská
vlajky	vlajka	k1gFnSc2	vlajka
===	===	k?	===
</s>
</p>
<p>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
6	[number]	k4	6
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
po	po	k7c6	po
přijetí	přijetí	k1gNnSc6	přijetí
nové	nový	k2eAgFnSc2d1	nová
ústavy	ústava	k1gFnSc2	ústava
a	a	k8xC	a
federalizaci	federalizace	k1gFnSc4	federalizace
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
zůstala	zůstat	k5eAaPmAgFnS	zůstat
etiopskou	etiopský	k2eAgFnSc7d1	etiopská
vlajkou	vlajka	k1gFnSc7	vlajka
jen	jen	k9	jen
prostá	prostý	k2eAgFnSc1d1	prostá
trikolóra	trikolóra	k1gFnSc1	trikolóra
<g/>
.	.	kIx.	.
</s>
<s>
Novým	nový	k2eAgInSc7d1	nový
národním	národní	k2eAgInSc7d1	národní
symbolem	symbol	k1gInSc7	symbol
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
žlutá	žlutý	k2eAgFnSc1d1	žlutá
pěticípá	pěticípý	k2eAgFnSc1d1	pěticípá
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
,	,	kIx,	,
utvořena	utvořen	k2eAgFnSc1d1	utvořena
z	z	k7c2	z
tenkých	tenký	k2eAgFnPc2d1	tenká
linek	linka	k1gFnPc2	linka
<g/>
,	,	kIx,	,
na	na	k7c6	na
modrém	modrý	k2eAgInSc6d1	modrý
kruhovém	kruhový	k2eAgInSc6d1	kruhový
podkladě	podklad	k1gInSc6	podklad
symbolizujícím	symbolizující	k2eAgInSc6d1	symbolizující
nebe	nebe	k1gNnSc4	nebe
<g/>
.	.	kIx.	.
</s>
<s>
Hvězda	Hvězda	k1gMnSc1	Hvězda
představuje	představovat	k5eAaImIp3nS	představovat
znamení	znamení	k1gNnSc4	znamení
světlé	světlý	k2eAgFnSc2d1	světlá
budoucnosti	budoucnost	k1gFnSc2	budoucnost
pro	pro	k7c4	pro
celý	celý	k2eAgInSc4d1	celý
etiopský	etiopský	k2eAgInSc4d1	etiopský
národ	národ	k1gInSc4	národ
<g/>
.	.	kIx.	.
</s>
<s>
Paprsky	paprsek	k1gInPc1	paprsek
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
vycházející	vycházející	k2eAgFnSc1d1	vycházející
značí	značit	k5eAaImIp3nS	značit
rovnost	rovnost	k1gFnSc4	rovnost
mezi	mezi	k7c7	mezi
všemi	všecek	k3xTgMnPc7	všecek
obyvateli	obyvatel	k1gMnPc7	obyvatel
této	tento	k3xDgFnSc2	tento
země	zem	k1gFnSc2	zem
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
etnickou	etnický	k2eAgFnSc4d1	etnická
příslušnost	příslušnost	k1gFnSc4	příslušnost
<g/>
,	,	kIx,	,
pohlaví	pohlaví	k1gNnSc4	pohlaví
či	či	k8xC	či
náboženské	náboženský	k2eAgNnSc4d1	náboženské
vyznání	vyznání	k1gNnSc4	vyznání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Středový	středový	k2eAgInSc1d1	středový
kruh	kruh	k1gInSc1	kruh
by	by	kYmCp3nP	by
oproti	oproti	k7c3	oproti
dnešní	dnešní	k2eAgFnSc3d1	dnešní
vlajce	vlajka	k1gFnSc3	vlajka
menší	malý	k2eAgFnSc3d2	menší
a	a	k8xC	a
světlejší	světlý	k2eAgInSc1d2	světlejší
<g/>
.	.	kIx.	.
</s>
<s>
Design	design	k1gInSc1	design
byl	být	k5eAaImAgInS	být
změněn	změnit	k5eAaPmNgInS	změnit
31	[number]	k4	31
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Etiopská	etiopský	k2eAgFnSc1d1	etiopská
vlajka	vlajka	k1gFnSc1	vlajka
jako	jako	k8xC	jako
symbol	symbol	k1gInSc1	symbol
africké	africký	k2eAgFnSc2d1	africká
jednoty	jednota	k1gFnSc2	jednota
==	==	k?	==
</s>
</p>
<p>
<s>
Barvy	barva	k1gFnPc1	barva
tvořící	tvořící	k2eAgFnPc1d1	tvořící
základ	základ	k1gInSc4	základ
etiopské	etiopský	k2eAgFnSc2d1	etiopská
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
,	,	kIx,	,
doplněny	doplněn	k2eAgInPc4d1	doplněn
o	o	k7c4	o
černou	černá	k1gFnSc4	černá
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
africký	africký	k2eAgInSc1d1	africký
lid	lid	k1gInSc1	lid
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
přijaty	přijmout	k5eAaPmNgInP	přijmout
za	za	k7c4	za
symbol	symbol	k1gInSc4	symbol
Africké	africký	k2eAgFnSc2d1	africká
jednoty	jednota	k1gFnSc2	jednota
(	(	kIx(	(
<g/>
předchůdce	předchůdce	k1gMnSc2	předchůdce
Africké	africký	k2eAgFnSc2d1	africká
unie	unie	k1gFnSc2	unie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Můžeme	moct	k5eAaImIp1nP	moct
je	on	k3xPp3gFnPc4	on
nalézt	nalézt	k5eAaBmF	nalézt
na	na	k7c6	na
státních	státní	k2eAgFnPc6d1	státní
vlajkách	vlajka	k1gFnPc6	vlajka
mnoha	mnoho	k4c2	mnoho
dalších	další	k2eAgFnPc2d1	další
afrických	africký	k2eAgFnPc2d1	africká
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
zemí	zem	k1gFnPc2	zem
si	se	k3xPyFc3	se
takzvané	takzvaný	k2eAgFnPc4d1	takzvaná
panafrické	panafrický	k2eAgFnPc4d1	panafrická
barvy	barva	k1gFnPc4	barva
uspořádala	uspořádat	k5eAaPmAgFnS	uspořádat
nezávisle	závisle	k6eNd1	závisle
na	na	k7c4	na
Etiopii	Etiopie	k1gFnSc4	Etiopie
a	a	k8xC	a
doplnila	doplnit	k5eAaPmAgFnS	doplnit
je	on	k3xPp3gFnPc4	on
o	o	k7c4	o
vlastní	vlastní	k2eAgInPc4d1	vlastní
státní	státní	k2eAgInPc4d1	státní
symboly	symbol	k1gInPc4	symbol
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
vlajky	vlajka	k1gFnPc4	vlajka
Beninu	Benin	k1gInSc2	Benin
<g/>
,	,	kIx,	,
Burkiny	Burkina	k1gFnPc1	Burkina
Faso	Faso	k1gNnSc1	Faso
<g/>
,	,	kIx,	,
Ghany	Ghana	k1gFnPc1	Ghana
<g/>
,	,	kIx,	,
Guiney	Guinea	k1gFnPc1	Guinea
<g/>
,	,	kIx,	,
Guiney	Guinea	k1gFnPc1	Guinea
<g/>
–	–	k?	–
<g/>
Bissau	Bissaus	k1gInSc2	Bissaus
<g/>
,	,	kIx,	,
Kamerunu	Kamerun	k1gInSc2	Kamerun
<g/>
,	,	kIx,	,
Konžské	konžský	k2eAgFnSc2d1	Konžská
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
Mali	Mali	k1gNnSc2	Mali
<g/>
,	,	kIx,	,
Mosambiku	Mosambik	k1gInSc2	Mosambik
<g/>
,	,	kIx,	,
Senegalu	Senegal	k1gInSc2	Senegal
<g/>
,	,	kIx,	,
Sv.	sv.	kA	sv.
Tomáše	Tomáš	k1gMnSc4	Tomáš
a	a	k8xC	a
Princova	princův	k2eAgFnSc1d1	princova
ostrovu	ostrov	k1gInSc3	ostrov
<g/>
,	,	kIx,	,
Toga	Togo	k1gNnSc2	Togo
a	a	k8xC	a
Zimbabwe	Zimbabwe	k1gNnSc2	Zimbabwe
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlajky	vlajka	k1gFnPc4	vlajka
etiopských	etiopský	k2eAgInPc2d1	etiopský
regionů	region	k1gInPc2	region
==	==	k?	==
</s>
</p>
<p>
<s>
Území	území	k1gNnSc1	území
Etiopie	Etiopie	k1gFnSc2	Etiopie
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
dvě	dva	k4xCgFnPc1	dva
samosprávné	samosprávný	k2eAgFnPc1d1	samosprávná
městské	městský	k2eAgFnPc1d1	městská
oblasti	oblast	k1gFnPc1	oblast
–	–	k?	–
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Addis	Addis	k1gFnSc2	Addis
Abeba	Abeba	k1gMnSc1	Abeba
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
sídlem	sídlo	k1gNnSc7	sídlo
státu	stát	k1gInSc2	stát
Oromie	Oromie	k1gFnSc2	Oromie
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
město	město	k1gNnSc1	město
Dire	Dir	k1gMnSc2	Dir
Dawa	Dawus	k1gMnSc2	Dawus
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
–	–	k?	–
a	a	k8xC	a
na	na	k7c4	na
9	[number]	k4	9
etnicky	etnicky	k6eAd1	etnicky
založených	založený	k2eAgInPc2d1	založený
federálních	federální	k2eAgInPc2d1	federální
států	stát	k1gInPc2	stát
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
Etiopie	Etiopie	k1gFnSc2	Etiopie
</s>
</p>
<p>
<s>
Etiopská	etiopský	k2eAgFnSc1d1	etiopská
hymna	hymna	k1gFnSc1	hymna
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Etiopie	Etiopie	k1gFnSc2	Etiopie
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Etiopská	etiopský	k2eAgFnSc1d1	etiopská
vlajka	vlajka	k1gFnSc1	vlajka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
http://ethiopedia.blogspot.com/2007/01/mysterious-origin-of-flag-of-ethiopia.html	[url]	k1gMnSc1	http://ethiopedia.blogspot.com/2007/01/mysterious-origin-of-flag-of-ethiopia.html
</s>
</p>
<p>
<s>
Etiopská	etiopský	k2eAgFnSc1d1	etiopská
císařská	císařský	k2eAgFnSc1d1	císařská
vlajka	vlajka	k1gFnSc1	vlajka
(	(	kIx(	(
<g/>
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
na	na	k7c6	na
FOTW	FOTW	kA	FOTW
</s>
</p>
<p>
<s>
http://www.angelfire.com/ny/ethiocrown/flags.html	[url]	k1gMnSc1	http://www.angelfire.com/ny/ethiocrown/flags.html
</s>
</p>
<p>
<s>
http://www.angelfire.com/ny/ethiocrown/nov2nd.html	[url]	k1gMnSc1	http://www.angelfire.com/ny/ethiocrown/nov2nd.html
</s>
</p>
<p>
<s>
http://www.worldstatesmen.org/Ethiopia.html	[url]	k1gMnSc1	http://www.worldstatesmen.org/Ethiopia.html
</s>
</p>
<p>
<s>
http://vexilolognet.hyperlink.cz/Etiopie.htm	[url]	k6eAd1	http://vexilolognet.hyperlink.cz/Etiopie.htm
</s>
</p>
