<s>
William	William	k1gInSc1	William
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
(	(	kIx(	(
<g/>
pokřtěn	pokřtěn	k2eAgMnSc1d1	pokřtěn
26	[number]	k4	26
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1564	[number]	k4	1564
–	–	k?	–
zemřel	zemřít	k5eAaPmAgInS	zemřít
23	[number]	k4	23
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1616	[number]	k4	1616
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
anglický	anglický	k2eAgMnSc1d1	anglický
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
dramatik	dramatik	k1gMnSc1	dramatik
a	a	k8xC	a
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
široce	široko	k6eAd1	široko
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
největšího	veliký	k2eAgMnSc4d3	veliký
anglicky	anglicky	k6eAd1	anglicky
píšícího	píšící	k2eAgMnSc2d1	píšící
spisovatele	spisovatel	k1gMnSc2	spisovatel
a	a	k8xC	a
celosvětově	celosvětově	k6eAd1	celosvětově
nejpřednějšího	přední	k2eAgMnSc2d3	nejpřednější
dramatika	dramatik	k1gMnSc2	dramatik
<g/>
.	.	kIx.	.
</s>
