<s>
Roku	rok	k1gInSc2	rok
1513	[number]	k4	1513
zabrala	zabrat	k5eAaPmAgFnS	zabrat
Kastilie	Kastilie	k1gFnSc1	Kastilie
jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
Navarrského	navarrský	k2eAgNnSc2d1	Navarrské
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
však	však	k9	však
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
nezávislé	závislý	k2eNgNnSc1d1	nezávislé
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1589	[number]	k4	1589
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
zřízena	zřízen	k2eAgFnSc1d1	zřízena
personální	personální	k2eAgFnSc1d1	personální
unie	unie	k1gFnSc1	unie
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
<g/>
.	.	kIx.	.
</s>
