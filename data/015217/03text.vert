<s>
Alžběta	Alžběta	k1gFnSc1
Vilemína	Vilemína	k1gFnSc1
Württemberská	Württemberský	k2eAgFnSc1d1
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
Vilemína	Vilemína	k1gFnSc1
Württemberskárakouská	Württemberskárakouský	k2eAgFnSc1d1
arcivévodkyně	arcivévodkyně	k1gFnSc1
Alžběta	Alžběta	k1gFnSc1
Vilemína	Vilemína	k1gFnSc1
Württemberská	Württemberský	k2eAgFnSc1d1
<g/>
,	,	kIx,
Johann	Johann	k1gMnSc1
Baptist	Baptist	k1gMnSc1
von	von	k1gInSc4
Lampi	Lampi	k1gNnSc2
starší	starší	k1gMnPc4
<g/>
,	,	kIx,
1785	#num#	k4
Sňatek	sňatek	k1gInSc1
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1788	#num#	k4
Manžel	manžel	k1gMnSc1
</s>
<s>
František	František	k1gMnSc1
Rakouský	rakouský	k2eAgMnSc1d1
Úplné	úplný	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
Vilemína	Vilemína	k1gFnSc1
Luisa	Luisa	k1gFnSc1
Narození	narození	k1gNnSc5
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1767	#num#	k4
Trzebiatów	Trzebiatów	k1gFnPc2
<g/>
,	,	kIx,
Pomořansko	Pomořansko	k1gNnSc1
Úmrtí	úmrť	k1gFnPc2
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1790	#num#	k4
(	(	kIx(
<g/>
22	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Vídeň	Vídeň	k1gFnSc1
<g/>
,	,	kIx,
Rakousko	Rakousko	k1gNnSc1
Potomci	potomek	k1gMnPc5
</s>
<s>
Ludovika	Ludovika	k1gFnSc1
Alžběta	Alžběta	k1gFnSc1
Rakouská	rakouský	k2eAgFnSc1d1
Dynastie	dynastie	k1gFnSc1
</s>
<s>
Württemberkové	Württemberkové	k2eAgMnSc1d1
Otec	otec	k1gMnSc1
</s>
<s>
Fridrich	Fridrich	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Evžen	Evžen	k1gMnSc1
Württemberský	Württemberský	k2eAgMnSc1d1
Matka	matka	k1gFnSc1
</s>
<s>
Bedřiška	Bedřiška	k1gFnSc1
Braniborsko-Schwedtská	Braniborsko-Schwedtský	k2eAgFnSc1d1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
Vilemína	Vilemína	k1gFnSc1
Luisa	Luisa	k1gFnSc1
Württemberská	Württemberský	k2eAgFnSc1d1
(	(	kIx(
<g/>
21	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1767	#num#	k4
Trzebiatów	Trzebiatów	k1gFnSc2
–	–	k?
18	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1790	#num#	k4
Vídeň	Vídeň	k1gFnSc1
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
württemberská	württemberský	k2eAgFnSc1d1
princezna	princezna	k1gFnSc1
a	a	k8xC
první	první	k4xOgFnSc1
manželka	manželka	k1gFnSc1
pozdějšího	pozdní	k2eAgMnSc2d2
císaře	císař	k1gMnSc2
Františka	František	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Původ	původ	k1gInSc1
</s>
<s>
Narodila	narodit	k5eAaPmAgFnS
se	se	k3xPyFc4
v	v	k7c6
Trzebiatówě	Trzebiatówa	k1gFnSc6
(	(	kIx(
<g/>
německy	německy	k6eAd1
Treptow	Treptow	k1gMnSc2
an	an	k?
der	drát	k5eAaImRp2nS
Rega	Rega	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tehdy	tehdy	k6eAd1
součást	součást	k1gFnSc1
Braniborského	braniborský	k2eAgNnSc2d1
markrabství	markrabství	k1gNnSc2
z	z	k7c2
manželství	manželství	k1gNnSc2
vévody	vévoda	k1gMnSc2
Fridricha	Fridrich	k1gMnSc2
Evžena	Evžen	k1gMnSc2
(	(	kIx(
<g/>
1732	#num#	k4
<g/>
–	–	k?
<g/>
1797	#num#	k4
<g/>
)	)	kIx)
s	s	k7c7
Bedřiškou	Bedřiška	k1gFnSc7
Braniborsko-Schwedtskou	Braniborsko-Schwedtský	k2eAgFnSc7d1
(	(	kIx(
<g/>
1736	#num#	k4
<g/>
–	–	k?
<g/>
1798	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
byla	být	k5eAaImAgFnS
už	už	k6eAd1
osmým	osmý	k4xOgMnSc7
narozeným	narozený	k2eAgMnSc7d1
dítětem	dítě	k1gNnSc7
z	z	k7c2
dvanácti	dvanáct	k4xCc2
<g/>
.	.	kIx.
</s>
<s>
Sourozenci	sourozenec	k1gMnPc1
<g/>
:	:	kIx,
württemberský	württemberský	k2eAgMnSc1d1
král	král	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
Vilém	Vilém	k1gMnSc1
(	(	kIx(
<g/>
1754	#num#	k4
<g/>
–	–	k?
<g/>
1816	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Ludvík	Ludvík	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
(	(	kIx(
<g/>
1756	#num#	k4
<g/>
–	–	k?
<g/>
1817	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Evžen	Evžen	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
(	(	kIx(
<g/>
1758	#num#	k4
<g/>
–	–	k?
<g/>
1822	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ruská	ruský	k2eAgFnSc1d1
carevna	carevna	k1gFnSc1
Marie	Marie	k1gFnSc1
Fjodorovna	Fjodorovna	k1gFnSc1
(	(	kIx(
<g/>
původním	původní	k2eAgNnSc7d1
<g />
.	.	kIx.
</s>
<s hack="1">
jménem	jméno	k1gNnSc7
Žofie	Žofie	k1gFnSc2
Dorota	Dorota	k1gFnSc1
<g/>
,	,	kIx,
1759	#num#	k4
<g/>
–	–	k?
<g/>
1828	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Vilém	Vilém	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
(	(	kIx(
<g/>
1761	#num#	k4
<g/>
–	–	k?
<g/>
1830	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Bedřiška	Bedřiška	k1gFnSc1
Alžběta	Alžběta	k1gFnSc1
(	(	kIx(
<g/>
1765	#num#	k4
<g/>
–	–	k?
<g/>
1785	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Alexandr	Alexandr	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
(	(	kIx(
<g/>
1771	#num#	k4
<g/>
–	–	k?
<g/>
1833	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Jindřich	Jindřich	k1gMnSc1
Karel	Karel	k1gMnSc1
(	(	kIx(
<g/>
1772	#num#	k4
<g/>
–	–	k?
<g/>
1838	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
V	v	k7c6
patnácti	patnáct	k4xCc6
letech	léto	k1gNnPc6
přišla	přijít	k5eAaPmAgFnS
z	z	k7c2
podnětu	podnět	k1gInSc2
císaře	císař	k1gMnSc2
Josefa	Josef	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
do	do	k7c2
Vídně	Vídeň	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
byla	být	k5eAaImAgFnS
v	v	k7c6
salesiánském	salesiánský	k2eAgInSc6d1
klášteře	klášter	k1gInSc6
v	v	k7c6
katolické	katolický	k2eAgFnSc6d1
víře	víra	k1gFnSc6
vychována	vychovat	k5eAaPmNgFnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
mohla	moct	k5eAaImAgFnS
stát	stát	k5eAaImF,k5eAaPmF
manželkou	manželka	k1gFnSc7
Josefova	Josefov	k1gInSc2
synovce	synovec	k1gMnSc2
Františka	František	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svatba	svatba	k1gFnSc1
se	se	k3xPyFc4
konala	konat	k5eAaImAgFnS
6	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1788	#num#	k4
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
Alžběta	Alžběta	k1gFnSc1
otěhotněla	otěhotnět	k5eAaPmAgFnS
<g/>
,	,	kIx,
ale	ale	k8xC
její	její	k3xOp3gNnSc1
zdraví	zdraví	k1gNnSc1
se	se	k3xPyFc4
povážlivě	povážlivě	k6eAd1
zhoršilo	zhoršit	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k8xS
spatřila	spatřit	k5eAaPmAgFnS
15	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1790	#num#	k4
císaře	císař	k1gMnSc2
Josefa	Josef	k1gMnSc2
po	po	k7c6
posledním	poslední	k2eAgNnSc6d1
pomazání	pomazání	k1gNnSc6
na	na	k7c6
smrtelné	smrtelný	k2eAgFnSc6d1
posteli	postel	k1gFnSc6
<g/>
,	,	kIx,
omdlela	omdlet	k5eAaPmAgFnS
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
o	o	k7c4
dva	dva	k4xCgInPc4
dny	den	k1gInPc4
později	pozdě	k6eAd2
<g/>
,	,	kIx,
17	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
roku	rok	k1gInSc2
1790	#num#	k4
pak	pak	k6eAd1
na	na	k7c4
svět	svět	k1gInSc4
přivedla	přivést	k5eAaPmAgFnS
dceru	dcera	k1gFnSc4
Luisu	Luisa	k1gFnSc4
Alžbětu	Alžběta	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Porod	porod	k1gInSc1
byl	být	k5eAaImAgInS
dlouhý	dlouhý	k2eAgInSc1d1
a	a	k8xC
těžký	těžký	k2eAgInSc1d1
a	a	k8xC
k	k	k7c3
vybavení	vybavení	k1gNnSc3
dítěte	dítě	k1gNnSc2
nakonec	nakonec	k6eAd1
lékaři	lékař	k1gMnPc1
museli	muset	k5eAaImAgMnP
použít	použít	k5eAaPmF
kleští	kleštit	k5eAaImIp3nS
<g/>
;	;	kIx,
holčička	holčička	k1gFnSc1
se	se	k3xPyFc4
narodila	narodit	k5eAaPmAgFnS
silně	silně	k6eAd1
poškozená	poškozený	k2eAgFnSc1d1
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
a	a	k8xC
půl	půl	k1xP
věku	věk	k1gInSc2
zemřela	zemřít	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alžběta	Alžběta	k1gFnSc1
sama	sám	k3xTgFnSc1
zemřela	zemřít	k5eAaPmAgFnS
den	den	k1gInSc4
po	po	k7c6
porodu	porod	k1gInSc6
na	na	k7c4
nezvládnutelné	zvládnutelný	k2eNgNnSc4d1
poporodní	poporodní	k2eAgNnSc4d1
krvácení	krvácení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pohřbena	pohřben	k2eAgFnSc1d1
byla	být	k5eAaImAgFnS
v	v	k7c6
kapucínské	kapucínský	k2eAgFnSc6d1
kryptě	krypta	k1gFnSc6
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
měla	mít	k5eAaImAgFnS
se	s	k7c7
stárnoucím	stárnoucí	k2eAgMnSc7d1
císařem	císař	k1gMnSc7
Josefem	Josef	k1gMnSc7
velmi	velmi	k6eAd1
vřelý	vřelý	k2eAgInSc4d1
vztah	vztah	k1gInSc4
a	a	k8xC
stala	stát	k5eAaPmAgFnS
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
potěchou	potěcha	k1gFnSc7
v	v	k7c6
jeho	jeho	k3xOp3gInPc6
posledních	poslední	k2eAgInPc6d1
dnech	den	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
však	však	k9
zemřela	zemřít	k5eAaPmAgFnS
<g/>
,	,	kIx,
císař	císař	k1gMnSc1
ztratil	ztratit	k5eAaPmAgMnS
i	i	k9
tu	ten	k3xDgFnSc4
poslední	poslední	k2eAgFnSc4d1
chuť	chuť	k1gFnSc4
žít	žít	k5eAaImF
a	a	k8xC
skonal	skonat	k5eAaPmAgInS
pouhé	pouhý	k2eAgInPc4d1
dva	dva	k4xCgInPc4
dny	den	k1gInPc4
po	po	k7c6
ní	on	k3xPp3gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gMnSc1
manžel	manžel	k1gMnSc1
<g/>
,	,	kIx,
pozdější	pozdní	k2eAgMnSc1d2
císař	císař	k1gMnSc1
František	František	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
se	se	k3xPyFc4
půl	půl	k6eAd1
roku	rok	k1gInSc2
po	po	k7c6
její	její	k3xOp3gFnSc6
smrti	smrt	k1gFnSc6
znovu	znovu	k6eAd1
oženil	oženit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
Potomci	potomek	k1gMnPc1
</s>
<s>
Luisa	Luisa	k1gFnSc1
Alžběta	Alžběta	k1gFnSc1
(	(	kIx(
<g/>
18	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1790	#num#	k4
Vídeň	Vídeň	k1gFnSc1
–	–	k?
24	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1791	#num#	k4
Vídeň	Vídeň	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Vývod	vývod	k1gInSc1
z	z	k7c2
předků	předek	k1gInPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Předkové	předek	k1gMnPc1
</s>
<s>
Fridrich	Fridrich	k1gMnSc1
Karel	Karel	k1gMnSc1
Württembersko-Winnentalský	Württembersko-Winnentalský	k2eAgMnSc1d1
</s>
<s>
Karel	Karel	k1gMnSc1
Alexandr	Alexandr	k1gMnSc1
Württemberský	Württemberský	k2eAgMnSc1d1
</s>
<s>
Eleonora	Eleonora	k1gFnSc1
Juliana	Julian	k1gMnSc2
Braniborsko-Ansbašská	Braniborsko-Ansbašský	k2eAgNnPc4d1
</s>
<s>
Fridrich	Fridrich	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Evžen	Evžen	k1gMnSc1
Württemberský	Württemberský	k2eAgInSc1d1
</s>
<s>
Anselm	Anselm	k6eAd1
František	František	k1gMnSc1
Thurn-Taxis	Thurn-Taxis	k1gFnSc2
</s>
<s>
Marie	Marie	k1gFnSc1
Augusta	August	k1gMnSc2
Thurn-Taxis	Thurn-Taxis	k1gFnSc2
</s>
<s>
Marie	Marie	k1gFnSc1
Ludovika	Ludovika	k1gFnSc1
Anna	Anna	k1gFnSc1
z	z	k7c2
Lobkovic	Lobkovice	k1gInPc2
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
Vilemína	Vilemína	k1gFnSc1
</s>
<s>
Filip	Filip	k1gMnSc1
Vilém	Vilém	k1gMnSc1
Braniborsko-Schwedtský	Braniborsko-Schwedtský	k2eAgMnSc1d1
</s>
<s>
Fridrich	Fridrich	k1gMnSc1
Vilém	Vilém	k1gMnSc1
Braniborsko-Schwedtský	Braniborsko-Schwedtský	k2eAgMnSc1d1
</s>
<s>
Jana	Jana	k1gFnSc1
Šarlota	Šarlota	k1gFnSc1
Anhaltsko-Desavská	Anhaltsko-Desavský	k2eAgFnSc1d1
</s>
<s>
Bedřiška	Bedřiška	k1gFnSc1
Braniborsko-Schwedtská	Braniborsko-Schwedtský	k2eAgFnSc1d1
</s>
<s>
Fridrich	Fridrich	k1gMnSc1
Vilém	Vilém	k1gMnSc1
I.	I.	kA
</s>
<s>
Žofie	Žofie	k1gFnSc1
Dorota	Dorota	k1gFnSc1
Pruská	pruský	k2eAgFnSc1d1
</s>
<s>
Žofie	Žofie	k1gFnSc1
Dorotea	Dorote	k1gInSc2
Hannoverská	hannoverský	k2eAgFnSc1d1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Elisabeth	Elisabeth	k1gInSc4
von	von	k1gInSc1
Württemberg	Württemberg	k1gInSc1
na	na	k7c6
německé	německý	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
HAMANNOVÁ	HAMANNOVÁ	kA
<g/>
,	,	kIx,
Brigitte	Brigitte	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Habsburkové	Habsburk	k1gMnPc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Životopisná	životopisný	k2eAgFnSc1d1
encyklopedie	encyklopedie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
BRÁNA	brána	k1gFnSc1
<g/>
,	,	kIx,
Knižní	knižní	k2eAgInSc1d1
klub	klub	k1gInSc1
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
.	.	kIx.
408	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85946	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
S.	S.	kA
54	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Alžběta	Alžběta	k1gFnSc1
Vilemína	Vilemína	k1gFnSc1
Württemberská	Württemberský	k2eAgFnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Stručný	stručný	k2eAgInSc1d1
životopis	životopis	k1gInSc1
na	na	k7c6
stránkách	stránka	k1gFnPc6
The	The	k1gFnSc2
Peerage	Peerag	k1gFnSc2
</s>
<s>
Rakouské	rakouský	k2eAgFnSc2d1
arcivévodkyně	arcivévodkyně	k1gFnSc2
sňatkem	sňatek	k1gInSc7
1	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Eleonora	Eleonora	k1gFnSc1
Portugalská	portugalský	k2eAgFnSc1d1
•	•	k?
Matylda	Matylda	k1gFnSc1
Falcká	falcký	k2eAgFnSc1d1
•	•	k?
Eleonora	Eleonora	k1gFnSc1
Skotská	skotský	k2eAgFnSc1d1
•	•	k?
Kateřina	Kateřina	k1gFnSc1
Saská	saský	k2eAgFnSc1d1
Znak	znak	k1gInSc4
rakouské	rakouský	k2eAgFnSc2d1
císařské	císařský	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
2	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Marie	Marie	k1gFnSc1
Burgundská	burgundský	k2eAgFnSc1d1
•	•	k?
Anna	Anna	k1gFnSc1
Bretaňská	bretaňský	k2eAgFnSc1d1
•	•	k?
Bianca	Bianca	k1gFnSc1
Marie	Marie	k1gFnSc1
Sforza	Sforza	k1gFnSc1
3	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Jana	Jana	k1gFnSc1
I.	I.	kA
Kastilská	kastilský	k2eAgFnSc1d1
4	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Isabela	Isabela	k1gFnSc1
Portugalská	portugalský	k2eAgFnSc1d1
•	•	k?
Anna	Anna	k1gFnSc1
Jagellonská	jagellonský	k2eAgFnSc1d1
5	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Marie	Marie	k1gFnSc1
Portugalská	portugalský	k2eAgFnSc1d1
<g/>
*	*	kIx~
•	•	k?
Marie	Marie	k1gFnSc1
I.	I.	kA
Tudorovna	Tudorovna	k1gFnSc1
<g/>
*	*	kIx~
•	•	k?
Marie	Marie	k1gFnSc1
Španělská	španělský	k2eAgFnSc1d1
•	•	k?
Anna	Anna	k1gFnSc1
Kateřina	Kateřina	k1gFnSc1
Gonzagová	Gonzagový	k2eAgFnSc1d1
•	•	k?
Marie	Marie	k1gFnSc1
Anna	Anna	k1gFnSc1
Bavorská	bavorský	k2eAgFnSc1d1
6	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Anna	Anna	k1gFnSc1
Tyrolská	tyrolský	k2eAgFnSc1d1
•	•	k?
Isabela	Isabela	k1gFnSc1
Klára	Klára	k1gFnSc1
Evženie	Evženie	k1gFnSc1
•	•	k?
Marie	Marie	k1gFnSc1
Anna	Anna	k1gFnSc1
Bavorská	bavorský	k2eAgFnSc1d1
•	•	k?
Eleonora	Eleonora	k1gFnSc1
Gonzagová	Gonzagový	k2eAgFnSc1d1
•	•	k?
Klaudie	Klaudie	k1gFnSc1
Medicejská	Medicejský	k2eAgFnSc1d1
7	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Izabela	Izabela	k1gFnSc1
Bourbonská	bourbonský	k2eAgFnSc1d1
<g/>
*	*	kIx~
•	•	k?
Marie	Marie	k1gFnSc1
Anna	Anna	k1gFnSc1
Španělská	španělský	k2eAgFnSc1d1
•	•	k?
Marie	Marie	k1gFnSc1
Leopoldina	Leopoldina	k1gFnSc1
Tyrolská	tyrolský	k2eAgFnSc1d1
•	•	k?
Eleonora	Eleonora	k1gFnSc1
Magdalena	Magdalena	k1gFnSc1
Gonzagová	Gonzagový	k2eAgFnSc1d1
•	•	k?
Anna	Anna	k1gFnSc1
Medicejská	Medicejský	k2eAgFnSc1d1
•	•	k?
Hedvika	Hedvika	k1gFnSc1
Falcko-Sulzbašská	Falcko-Sulzbašský	k2eAgFnSc1d1
8	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Markéta	Markéta	k1gFnSc1
Habsburská	habsburský	k2eAgFnSc1d1
•	•	k?
Klaudie	Klaudie	k1gFnSc1
Felicitas	Felicitas	k1gFnSc1
Tyrolská	tyrolský	k2eAgFnSc1d1
•	•	k?
Eleonora	Eleonora	k1gFnSc1
Magdalena	Magdalena	k1gFnSc1
Falcko-Neuburská	Falcko-Neuburský	k2eAgFnSc1d1
9	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Amálie	Amálie	k1gFnSc1
Vilemína	Vilemína	k1gFnSc1
Brunšvicko-Lüneburská	Brunšvicko-Lüneburský	k2eAgFnSc1d1
•	•	k?
Alžběta	Alžběta	k1gFnSc1
Kristýna	Kristýna	k1gFnSc1
Brunšvicko-Wolfenbüttelská	Brunšvicko-Wolfenbüttelský	k2eAgFnSc1d1
10	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
nikdo	nikdo	k3yNnSc1
11	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Isabela	Isabela	k1gFnSc1
Parmská	parmský	k2eAgFnSc1d1
<g/>
**	**	k?
•	•	k?
Marie	Marie	k1gFnSc1
Josefa	Josefa	k1gFnSc1
Bavorská	bavorský	k2eAgFnSc1d1
<g/>
**	**	k?
•	•	k?
Marie	Marie	k1gFnSc1
Ludovika	Ludovika	k1gFnSc1
Španělská	španělský	k2eAgFnSc1d1
<g/>
**	**	k?
•	•	k?
Marie	Marie	k1gFnSc1
Beatrice	Beatrice	k1gFnSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
Este	Est	k1gMnSc5
<g/>
**	**	k?
12	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
Vilemína	Vilemína	k1gFnSc1
Württemberská	Württemberský	k2eAgFnSc1d1
<g/>
**	**	k?
•	•	k?
Marie	Marie	k1gFnSc1
Tereza	Tereza	k1gFnSc1
Neapolsko-Sicilská	neapolsko-sicilský	k2eAgFnSc1d1
<g/>
**	**	k?
•	•	k?
Luisa	Luisa	k1gFnSc1
Marie	Marie	k1gFnSc1
Amélie	Amélie	k1gFnSc1
Tereza	Tereza	k1gFnSc1
Neapolsko-Sicilská	neapolsko-sicilský	k2eAgFnSc1d1
<g/>
**	**	k?
•	•	k?
Jindřiška	Jindřiška	k1gFnSc1
Nasavsko-Weilburská	Nasavsko-Weilburský	k2eAgFnSc1d1
<g/>
**	**	k?
•	•	k?
Alexandra	Alexandra	k1gFnSc1
Pavlovna	Pavlovna	k1gFnSc1
Ruská	ruský	k2eAgFnSc1d1
<g/>
**	**	k?
•	•	k?
Hermína	Hermína	k1gFnSc1
z	z	k7c2
Anhalt-Bernburg-Schaumburg-Hoymu	Anhalt-Bernburg-Schaumburg-Hoym	k1gInSc2
<g/>
**	**	k?
•	•	k?
Marie	Marie	k1gFnSc1
Dorotea	Dorotea	k1gFnSc1
Württemberská	Württemberský	k2eAgFnSc1d1
<g/>
**	**	k?
•	•	k?
Alžběta	Alžběta	k1gFnSc1
Savojská	savojský	k2eAgFnSc1d1
<g/>
**	**	k?
•	•	k?
Marie	Marie	k1gFnSc1
Beatrice	Beatrice	k1gFnSc1
Savojská	savojský	k2eAgFnSc1d1
<g/>
***	***	k?
13	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Žofie	Žofie	k1gFnSc1
Frederika	Frederika	k1gFnSc1
Bavorská	bavorský	k2eAgFnSc1d1
•	•	k?
Marie	Marie	k1gFnSc1
Anna	Anna	k1gFnSc1
Saská	saský	k2eAgFnSc1d1
<g/>
**	**	k?
•	•	k?
Hildegarda	Hildegarda	k1gFnSc1
Luisa	Luisa	k1gFnSc1
Bavorská	bavorský	k2eAgFnSc1d1
•	•	k?
Alžběta	Alžběta	k1gFnSc1
Františka	Františka	k1gFnSc1
Marie	Marie	k1gFnSc1
Habsbursko-Lotrinská	habsbursko-lotrinský	k2eAgFnSc1d1
•	•	k?
Klotylda	Klotylda	k1gFnSc1
Sasko-Kobursko-Gothajská	Sasko-Kobursko-Gothajský	k2eAgFnSc1d1
•	•	k?
Adéla	Adéla	k1gFnSc1
Augusta	Augusta	k1gMnSc1
Bavorská	bavorský	k2eAgFnSc1d1
<g/>
***	***	k?
14	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Charlotta	Charlotta	k1gFnSc1
Belgická	belgický	k2eAgFnSc1d1
•	•	k?
Markéta	Markéta	k1gFnSc1
Saská	saský	k2eAgFnSc1d1
•	•	k?
Marie	Marie	k1gFnSc1
Annunziata	Annunziata	k1gFnSc1
Neapolsko-Sicilská	neapolsko-sicilský	k2eAgFnSc1d1
•	•	k?
Marie	Marie	k1gFnSc1
Tereza	Tereza	k1gFnSc1
Portugalská	portugalský	k2eAgFnSc1d1
•	•	k?
Anna	Anna	k1gFnSc1
Saská	saský	k2eAgFnSc1d1
<g/>
**	**	k?
•	•	k?
Alice	Alice	k1gFnSc1
Bourbonsko-Parmská	bourbonsko-parmský	k2eAgFnSc1d1
<g/>
**	**	k?
•	•	k?
Marie	Marie	k1gFnSc1
Imakuláta	Imakuláta	k1gFnSc1
Neapolsko-Sicilská	neapolsko-sicilský	k2eAgFnSc1d1
<g/>
**	**	k?
•	•	k?
Isabela	Isabela	k1gFnSc1
z	z	k7c2
Croy	Croa	k1gFnSc2
•	•	k?
Marie	Marie	k1gFnSc1
Tereza	Tereza	k1gFnSc1
Habsbursko-Lotrinská	habsbursko-lotrinský	k2eAgFnSc1d1
•	•	k?
Augusta	August	k1gMnSc4
Marie	Maria	k1gFnSc2
Luisa	Luisa	k1gFnSc1
Bavorská	bavorský	k2eAgFnSc1d1
15	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Štěpánka	Štěpánka	k1gFnSc1
Belgická	belgický	k2eAgFnSc1d1
•	•	k?
Marie	Marie	k1gFnSc1
Josefa	Josefa	k1gFnSc1
Saská	saský	k2eAgFnSc1d1
•	•	k?
Marie	Marie	k1gFnSc1
Kristýna	Kristýna	k1gFnSc1
Bourbonsko-Sicilská	bourbonsko-sicilský	k2eAgFnSc1d1
<g/>
**	**	k?
•	•	k?
Blanka	Blanka	k1gFnSc1
Bourbonsko-Kastilská	bourbonsko-kastilský	k2eAgFnSc1d1
<g/>
**	**	k?
•	•	k?
Marie	Marie	k1gFnSc1
Valerie	Valerie	k1gFnSc1
Habsbursko-Lotrinská	habsbursko-lotrinský	k2eAgFnSc1d1
<g/>
**	**	k?
•	•	k?
Anna	Anna	k1gFnSc1
Saská	saský	k2eAgFnSc1d1
16	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Zita	Zita	k1gFnSc1
Bourbonsko-Parmská	bourbonsko-parmský	k2eAgFnSc1d1
•	•	k?
Františka	Františka	k1gFnSc1
z	z	k7c2
Hohenlohe-Waldenburg-Schillingsfürstu	Hohenlohe-Waldenburg-Schillingsfürst	k1gInSc2
•	•	k?
Dorotea	Dorotea	k1gFnSc1
Bavorská	bavorský	k2eAgFnSc1d1
<g/>
**	**	k?
•	•	k?
Marie	Marie	k1gFnSc1
Valerie	Valerie	k1gFnSc1
z	z	k7c2
Waldburg-Zeil-Hohenems	Waldburg-Zeil-Hohenemsa	k1gFnPc2
<g/>
**	**	k?
•	•	k?
Rosemary	Rosemar	k1gInPc1
Salm-Salm	Salm-Salm	k1gInSc1
<g/>
**	**	k?
•	•	k?
Marie	Marie	k1gFnSc1
Tereza	Tereza	k1gFnSc1
z	z	k7c2
Waldburg-Zeil	Waldburg-Zeila	k1gFnPc2
<g/>
**	**	k?
•	•	k?
Marie	Maria	k1gFnSc2
Löwenstein-Wertheim-Rosenberg	Löwenstein-Wertheim-Rosenberg	k1gMnSc1
•	•	k?
Kristýna	Kristýna	k1gFnSc1
z	z	k7c2
Löwenstein-Wertheim-Rosenberg	Löwenstein-Wertheim-Rosenberg	k1gInSc4
17	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Regina	Regina	k1gFnSc1
Sasko-Meiningenská	Sasko-Meiningenský	k2eAgFnSc1d1
•	•	k?
Markéta	Markéta	k1gFnSc1
Savojská-Aosta	Savojská-Aosta	k1gFnSc1
<g/>
***	***	k?
•	•	k?
Anna	Anna	k1gFnSc1
Eugénie	Eugénie	k1gFnSc1
Arenbergová	Arenbergový	k2eAgFnSc1d1
•	•	k?
Yolanda	Yolanda	k1gFnSc1
z	z	k7c2
Ligne	Lign	k1gInSc5
•	•	k?
Xenia	Xenia	k1gFnSc1
Czernicheva-Besobrasova	Czernicheva-Besobrasův	k2eAgFnSc1d1
•	•	k?
Anna	Anna	k1gFnSc1
Gabriela	Gabriela	k1gFnSc1
z	z	k7c2
Wrede	Wred	k1gInSc5
•	•	k?
Helena	Helena	k1gFnSc1
z	z	k7c2
Toerring-Jettenbach	Toerring-Jettenbach	k1gMnSc1
•	•	k?
Ludmila	Ludmila	k1gFnSc1
z	z	k7c2
Gallen	Gallna	k1gFnPc2
•	•	k?
Laetitia	Laetitius	k1gMnSc2
z	z	k7c2
Arenbergu	Arenberg	k1gInSc2
<g/>
**	**	k?
•	•	k?
Margaret	Margareta	k1gFnPc2
Kálnoky	Kálnok	k1gInPc1
von	von	k1gInSc4
Köröspatak	Köröspatak	k1gInSc4
<g/>
**	**	k?
•	•	k?
Maria	Maria	k1gFnSc1
Espinosa	Espinosa	k1gFnSc1
de	de	k?
los	los	k1gInSc1
Monteros	Monterosa	k1gFnPc2
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
**	**	k?
•	•	k?
Valerie	Valerie	k1gFnPc4
z	z	k7c2
Podstatzky-Lichtenstein	Podstatzky-Lichtensteina	k1gFnPc2
<g/>
**	**	k?
•	•	k?
Freiin	Freiin	k2eAgInSc4d1
Eva	Eva	k1gFnSc1
Antonia	Antonio	k1gMnSc2
von	von	k1gInSc4
Hofmann	Hofmann	k1gInSc4
<g/>
**	**	k?
•	•	k?
Anna	Anna	k1gFnSc1
Amelie	Amelie	k1gFnSc1
ze	z	k7c2
Schönburg-Waldenburg	Schönburg-Waldenburg	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Hedvika	Hedvika	k1gFnSc1
z	z	k7c2
Lichem-Löwenburg	Lichem-Löwenburg	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Freiin	Freiin	k2eAgInSc4d1
Edith	Edith	k1gInSc4
von	von	k1gInSc1
Sternbach	Sternbach	k1gInSc1
<g/>
**	**	k?
•	•	k?
Margarite	Margarit	k1gInSc5
z	z	k7c2
Hohenbergu	Hohenberg	k1gInSc2
•	•	k?
Marie	Maria	k1gFnSc2
Christine	Christin	k1gInSc5
von	von	k1gInSc1
Hatzfeldt-Dönhoff	Hatzfeldt-Dönhoff	k1gInSc4
•	•	k?
Eugenia	Eugenium	k1gNnSc2
de	de	k?
Calonge	Calonge	k1gInSc1
•	•	k?
Freiin	Freiin	k2eAgInSc1d1
Maria	Mario	k1gMnSc2
Theresia	Theresia	k1gFnSc1
von	von	k1gInSc1
Gudenus	Gudenus	k1gInSc1
*	*	kIx~
také	také	k9
španělská	španělský	k2eAgFnSc1d1
infanta	infant	k1gMnSc2
sňatkem	sňatek	k1gInSc7
<g/>
,	,	kIx,
**	**	k?
také	také	k9
toskánská	toskánský	k2eAgFnSc1d1
princezna	princezna	k1gFnSc1
asňatkem	asňatek	k1gInSc7
<g/>
,	,	kIx,
***	***	k?
také	také	k9
modenská	modenský	k2eAgFnSc1d1
princezna	princezna	k1gFnSc1
sňatkem	sňatek	k1gInSc7
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
12303566X	12303566X	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
7864	#num#	k4
5598	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
2010072948	#num#	k4
|	|	kIx~
ULAN	ULAN	kA
<g/>
:	:	kIx,
500354535	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
13206447	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
2010072948	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
|	|	kIx~
Rakousko	Rakousko	k1gNnSc1
</s>
