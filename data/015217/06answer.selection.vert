<s>
Narodila	narodit	k5eAaPmAgFnS
se	se	k3xPyFc4
v	v	k7c6
Trzebiatówě	Trzebiatówa	k1gFnSc6
(	(	kIx(
<g/>
německy	německy	k6eAd1
Treptow	Treptow	k1gMnSc2
an	an	k?
der	drát	k5eAaImRp2nS
Rega	Rega	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tehdy	tehdy	k6eAd1
součást	součást	k1gFnSc1
Braniborského	braniborský	k2eAgNnSc2d1
markrabství	markrabství	k1gNnSc2
z	z	k7c2
manželství	manželství	k1gNnSc2
vévody	vévoda	k1gMnSc2
Fridricha	Fridrich	k1gMnSc2
Evžena	Evžen	k1gMnSc2
(	(	kIx(
<g/>
1732	#num#	k4
<g/>
–	–	k?
<g/>
1797	#num#	k4
<g/>
)	)	kIx)
s	s	k7c7
Bedřiškou	Bedřiška	k1gFnSc7
Braniborsko-Schwedtskou	Braniborsko-Schwedtský	k2eAgFnSc7d1
(	(	kIx(
<g/>
1736	#num#	k4
<g/>
–	–	k?
<g/>
1798	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
byla	být	k5eAaImAgFnS
už	už	k6eAd1
osmým	osmý	k4xOgMnSc7
narozeným	narozený	k2eAgMnSc7d1
dítětem	dítě	k1gNnSc7
z	z	k7c2
dvanácti	dvanáct	k4xCc2
<g/>
.	.	kIx.
</s>