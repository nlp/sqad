<s>
Epidemiologické	epidemiologický	k2eAgFnPc1d1	epidemiologická
studie	studie	k1gFnPc1	studie
prokázaly	prokázat	k5eAaPmAgFnP	prokázat
souvislost	souvislost	k1gFnSc4	souvislost
mezi	mezi	k7c7	mezi
vitamínem	vitamín	k1gInSc7	vitamín
K	K	kA	K
a	a	k8xC	a
řídnutím	řídnutí	k1gNnSc7	řídnutí
kostí	kost	k1gFnPc2	kost
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
věkem	věk	k1gInSc7	věk
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
osteoporózou	osteoporóza	k1gFnSc7	osteoporóza
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
