<s>
Marťanská	marťanský	k2eAgFnSc1d1	Marťanská
kronika	kronika	k1gFnSc1	kronika
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Martian	Martian	k1gMnSc1	Martian
Chronicles	Chronicles	k1gMnSc1	Chronicles
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
cyklus	cyklus	k1gInSc1	cyklus
povídek	povídka	k1gFnPc2	povídka
Raye	Ray	k1gMnSc2	Ray
Bradburyho	Bradbury	k1gMnSc2	Bradbury
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
popisujících	popisující	k2eAgFnPc2d1	popisující
fiktivní	fiktivní	k2eAgFnSc4d1	fiktivní
kolonizaci	kolonizace	k1gFnSc4	kolonizace
Marsu	Mars	k1gInSc2	Mars
<g/>
.	.	kIx.	.
</s>
