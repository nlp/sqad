<p>
<s>
Marťanská	marťanský	k2eAgFnSc1d1	Marťanská
kronika	kronika	k1gFnSc1	kronika
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Martian	Martian	k1gMnSc1	Martian
Chronicles	Chronicles	k1gMnSc1	Chronicles
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
cyklus	cyklus	k1gInSc1	cyklus
povídek	povídka	k1gFnPc2	povídka
Raye	Ray	k1gMnSc2	Ray
Bradburyho	Bradbury	k1gMnSc2	Bradbury
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
popisujících	popisující	k2eAgFnPc2d1	popisující
fiktivní	fiktivní	k2eAgFnSc4d1	fiktivní
kolonizaci	kolonizace	k1gFnSc4	kolonizace
Marsu	Mars	k1gInSc2	Mars
<g/>
.	.	kIx.	.
</s>
<s>
Povídky	povídka	k1gFnPc1	povídka
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
let	léto	k1gNnPc2	léto
1945-1950	[number]	k4	1945-1950
a	a	k8xC	a
velká	velký	k2eAgFnSc1d1	velká
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
vyšla	vyjít	k5eAaPmAgFnS	vyjít
nejprve	nejprve	k6eAd1	nejprve
časopisecky	časopisecky	k6eAd1	časopisecky
<g/>
.	.	kIx.	.
<g/>
Děj	děj	k1gInSc1	děj
povídek	povídka	k1gFnPc2	povídka
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
na	na	k7c6	na
planetě	planeta	k1gFnSc6	planeta
Mars	Mars	k1gInSc1	Mars
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
dvě	dva	k4xCgFnPc1	dva
povídky	povídka	k1gFnPc1	povídka
jsou	být	k5eAaImIp3nP	být
situovány	situovat	k5eAaBmNgFnP	situovat
na	na	k7c4	na
Zem	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
povídka	povídka	k1gFnSc1	povídka
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
názvu	název	k1gInSc6	název
měsíc	měsíc	k1gInSc4	měsíc
a	a	k8xC	a
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
děj	děj	k1gInSc1	děj
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
(	(	kIx(	(
<g/>
první	první	k4xOgFnSc1	první
povídka	povídka	k1gFnSc1	povídka
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1999	[number]	k4	1999
a	a	k8xC	a
poslední	poslední	k2eAgMnSc1d1	poslední
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2026	[number]	k4	2026
<g/>
,	,	kIx,	,
v	v	k7c6	v
upravené	upravený	k2eAgFnSc6d1	upravená
verzi	verze	k1gFnSc6	verze
se	se	k3xPyFc4	se
děj	děj	k1gInSc1	děj
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2030	[number]	k4	2030
<g/>
-	-	kIx~	-
<g/>
2057	[number]	k4	2057
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
povídky	povídka	k1gFnPc4	povídka
přibližují	přibližovat	k5eAaImIp3nP	přibližovat
události	událost	k1gFnPc1	událost
od	od	k7c2	od
neúspěchů	neúspěch	k1gInPc2	neúspěch
prvních	první	k4xOgInPc2	první
expedic	expedice	k1gFnPc2	expedice
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
vyhubení	vyhubení	k1gNnSc4	vyhubení
Marťanů	Marťan	k1gMnPc2	Marťan
lidskou	lidský	k2eAgFnSc7d1	lidská
infekcí	infekce	k1gFnSc7	infekce
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
poté	poté	k6eAd1	poté
vypukne	vypuknout	k5eAaPmIp3nS	vypuknout
jaderná	jaderný	k2eAgFnSc1d1	jaderná
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
níž	jenž	k3xRgFnSc7	jenž
pozemšťané	pozemšťan	k1gMnPc1	pozemšťan
přerušují	přerušovat	k5eAaImIp3nP	přerušovat
kolonizaci	kolonizace	k1gFnSc4	kolonizace
Marsu	Mars	k1gInSc2	Mars
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
závěr	závěr	k1gInSc4	závěr
nově	nově	k6eAd1	nově
příchozí	příchozí	k1gMnPc1	příchozí
<g/>
,	,	kIx,	,
vyhnaní	vyhnaný	k2eAgMnPc1d1	vyhnaný
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
na	na	k7c4	na
Mars	Mars	k1gInSc4	Mars
<g/>
,	,	kIx,	,
získávají	získávat	k5eAaImIp3nP	získávat
etičtější	etický	k2eAgInSc4d2	etičtější
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
novému	nový	k2eAgInSc3d1	nový
domovu	domov	k1gInSc3	domov
<g/>
,	,	kIx,	,
místu	místo	k1gNnSc3	místo
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
jim	on	k3xPp3gMnPc3	on
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
žít	žít	k5eAaImF	žít
lidštěji	lidsky	k6eAd2	lidsky
než	než	k8xS	než
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Povídky	povídka	k1gFnPc1	povídka
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
rozdělit	rozdělit	k5eAaPmF	rozdělit
do	do	k7c2	do
tří	tři	k4xCgInPc2	tři
tematických	tematický	k2eAgInPc2d1	tematický
okruhů	okruh	k1gInPc2	okruh
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
příběhy	příběh	k1gInPc1	příběh
o	o	k7c6	o
objevitelských	objevitelský	k2eAgFnPc6d1	objevitelská
výpravách	výprava	k1gFnPc6	výprava
na	na	k7c4	na
Mars	Mars	k1gInSc4	Mars
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
příběhy	příběh	k1gInPc1	příběh
o	o	k7c4	o
osidlování	osidlování	k1gNnSc4	osidlování
Marsu	Mars	k1gInSc2	Mars
–	–	k?	–
o	o	k7c6	o
budování	budování	k1gNnSc6	budování
lidské	lidský	k2eAgFnSc2d1	lidská
civilizace	civilizace	k1gFnSc2	civilizace
<g/>
,	,	kIx,	,
o	o	k7c6	o
setkáních	setkání	k1gNnPc6	setkání
lidí	člověk	k1gMnPc2	člověk
s	s	k7c7	s
původními	původní	k2eAgMnPc7d1	původní
obyvateli	obyvatel	k1gMnPc7	obyvatel
Marsu	Mars	k1gInSc2	Mars
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
okruh	okruh	k1gInSc1	okruh
povídek	povídka	k1gFnPc2	povídka
odehrávající	odehrávající	k2eAgFnSc2d1	odehrávající
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
na	na	k7c6	na
planetě	planeta	k1gFnSc6	planeta
Zemi	zem	k1gFnSc6	zem
vypukne	vypuknout	k5eAaPmIp3nS	vypuknout
jaderná	jaderný	k2eAgFnSc1d1	jaderná
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
po	po	k7c6	po
níž	jenž	k3xRgFnSc6	jenž
pozemšťané	pozemšťan	k1gMnPc1	pozemšťan
přerušují	přerušovat	k5eAaImIp3nP	přerušovat
kolonizaci	kolonizace	k1gFnSc4	kolonizace
Marsu	Mars	k1gInSc2	Mars
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
určitá	určitý	k2eAgFnSc1d1	určitá
snivá	snivý	k2eAgFnSc1d1	snivá
atmosféra	atmosféra	k1gFnSc1	atmosféra
<g/>
,	,	kIx,	,
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
v	v	k7c6	v
ději	děj	k1gInSc6	děj
objeví	objevit	k5eAaPmIp3nP	objevit
Marťané	Marťan	k1gMnPc1	Marťan
(	(	kIx(	(
<g/>
jsou	být	k5eAaImIp3nP	být
nadáni	nadat	k5eAaPmNgMnP	nadat
schopností	schopnost	k1gFnSc7	schopnost
telepatie	telepatie	k1gFnSc2	telepatie
a	a	k8xC	a
hypnózy	hypnóza	k1gFnSc2	hypnóza
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Důležitým	důležitý	k2eAgNnSc7d1	důležité
tématem	téma	k1gNnSc7	téma
knihy	kniha	k1gFnSc2	kniha
je	být	k5eAaImIp3nS	být
otázka	otázka	k1gFnSc1	otázka
<g/>
,	,	kIx,	,
o	o	k7c4	o
kolik	kolik	k9	kolik
technický	technický	k2eAgInSc4d1	technický
pokrok	pokrok	k1gInSc4	pokrok
předběhl	předběhnout	k5eAaPmAgMnS	předběhnout
pokrok	pokrok	k1gInSc4	pokrok
lidský	lidský	k2eAgInSc4d1	lidský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Povídky	povídka	k1gFnPc1	povídka
==	==	k?	==
</s>
</p>
<p>
<s>
Za	za	k7c7	za
českým	český	k2eAgInSc7d1	český
názvem	název	k1gInSc7	název
povídky	povídka	k1gFnSc2	povídka
je	být	k5eAaImIp3nS	být
uveden	uveden	k2eAgInSc1d1	uveden
rok	rok	k1gInSc1	rok
jejího	její	k3xOp3gInSc2	její
vzniku	vznik	k1gInSc2	vznik
<g/>
,	,	kIx,	,
anglický	anglický	k2eAgInSc1d1	anglický
název	název	k1gInSc1	název
a	a	k8xC	a
období	období	k1gNnSc1	období
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
podle	podle	k7c2	podle
první	první	k4xOgFnSc2	první
a	a	k8xC	a
podle	podle	k7c2	podle
upravené	upravený	k2eAgFnSc2d1	upravená
verze	verze	k1gFnSc2	verze
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Raketové	raketový	k2eAgNnSc1d1	raketové
léto	léto	k1gNnSc1	léto
(	(	kIx(	(
<g/>
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
Rocket	Rocket	k1gMnSc1	Rocket
Summer	Summer	k1gMnSc1	Summer
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
leden	leden	k1gInSc1	leden
1999	[number]	k4	1999
<g/>
/	/	kIx~	/
<g/>
2030	[number]	k4	2030
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Ohiu	Ohio	k1gNnSc6	Ohio
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
extrémnímu	extrémní	k2eAgNnSc3d1	extrémní
horku	horko	k1gNnSc3	horko
při	při	k7c6	při
startu	start	k1gInSc6	start
rakety	raketa	k1gFnSc2	raketa
zima	zima	k1gFnSc1	zima
krátce	krátce	k6eAd1	krátce
změní	změnit	k5eAaPmIp3nS	změnit
v	v	k7c4	v
léto	léto	k1gNnSc4	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ylla	Ylla	k1gFnSc1	Ylla
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
únor	únor	k1gInSc1	únor
1999	[number]	k4	1999
<g/>
/	/	kIx~	/
<g/>
2030	[number]	k4	2030
<g/>
.	.	kIx.	.
</s>
<s>
Marťanka	Marťanka	k1gFnSc1	Marťanka
Ylla	Yll	k1gInSc2	Yll
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
neromantickém	romantický	k2eNgNnSc6d1	neromantické
manželství	manželství	k1gNnSc6	manželství
a	a	k8xC	a
díky	díky	k7c3	díky
telepatii	telepatie	k1gFnSc3	telepatie
ve	v	k7c6	v
snu	sen	k1gInSc6	sen
vidí	vidět	k5eAaImIp3nS	vidět
příchod	příchod	k1gInSc1	příchod
astronautů	astronaut	k1gMnPc2	astronaut
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnSc1	její
manžel	manžel	k1gMnSc1	manžel
popírá	popírat	k5eAaImIp3nS	popírat
pravdivost	pravdivost	k1gFnSc4	pravdivost
oněch	onen	k3xDgInPc2	onen
snů	sen	k1gInPc2	sen
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
nitru	nitro	k1gNnSc6	nitro
začne	začít	k5eAaPmIp3nS	začít
hořce	hořko	k6eAd1	hořko
žárlit	žárlit	k5eAaImF	žárlit
<g/>
.	.	kIx.	.
</s>
<s>
Cítí	cítit	k5eAaImIp3nS	cítit
jakési	jakýsi	k3yIgNnSc1	jakýsi
počínající	počínající	k2eAgInPc4d1	počínající
romantické	romantický	k2eAgInPc4d1	romantický
city	cit	k1gInPc4	cit
Ylly	Yll	k2eAgInPc4d1	Yll
k	k	k7c3	k
jednomu	jeden	k4xCgMnSc3	jeden
z	z	k7c2	z
astronautů	astronaut	k1gMnPc2	astronaut
<g/>
.	.	kIx.	.
</s>
<s>
Astronauty	astronaut	k1gMnPc4	astronaut
zabije	zabít	k5eAaPmIp3nS	zabít
<g/>
,	,	kIx,	,
jakmile	jakmile	k8xS	jakmile
přiletí	přiletět	k5eAaPmIp3nS	přiletět
na	na	k7c4	na
Mars	Mars	k1gInSc4	Mars
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Letní	letní	k2eAgFnSc1d1	letní
noc	noc	k1gFnSc1	noc
(	(	kIx(	(
<g/>
1949	[number]	k4	1949
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Summer	Summer	k1gMnSc1	Summer
Night	Night	k1gMnSc1	Night
<g/>
,	,	kIx,	,
srpen	srpen	k1gInSc1	srpen
1999	[number]	k4	1999
<g/>
/	/	kIx~	/
<g/>
2030	[number]	k4	2030
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
Marsu	Mars	k1gInSc2	Mars
se	se	k3xPyFc4	se
v	v	k7c4	v
letní	letní	k2eAgFnSc4d1	letní
noc	noc	k1gFnSc4	noc
scházejí	scházet	k5eAaImIp3nP	scházet
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
si	se	k3xPyFc3	se
poslechli	poslechnout	k5eAaPmAgMnP	poslechnout
koncert	koncert	k1gInSc4	koncert
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hudba	hudba	k1gFnSc1	hudba
a	a	k8xC	a
zpěv	zpěv	k1gInSc1	zpěv
jsou	být	k5eAaImIp3nP	být
divné	divný	k2eAgFnPc1d1	divná
<g/>
,	,	kIx,	,
podivné	podivný	k2eAgFnPc1d1	podivná
<g/>
,	,	kIx,	,
nemarťanské	marťanský	k2eNgFnPc1d1	marťanský
<g/>
.	.	kIx.	.
</s>
<s>
Marťané	Marťan	k1gMnPc1	Marťan
vycítili	vycítit	k5eAaPmAgMnP	vycítit
lidské	lidský	k2eAgFnPc4d1	lidská
myšlenky	myšlenka	k1gFnPc4	myšlenka
astronautů	astronaut	k1gMnPc2	astronaut
z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
expedice	expedice	k1gFnSc2	expedice
<g/>
.	.	kIx.	.
</s>
<s>
Blíží	blížit	k5eAaImIp3nS	blížit
se	se	k3xPyFc4	se
změny	změna	k1gFnPc4	změna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pozemšťani	Pozemšťan	k1gMnPc1	Pozemšťan
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Earth	Earth	k1gMnSc1	Earth
Men	Men	k1gMnSc1	Men
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
srpen	srpen	k1gInSc1	srpen
1999	[number]	k4	1999
<g/>
/	/	kIx~	/
<g/>
2030	[number]	k4	2030
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
příběh	příběh	k1gInSc4	příběh
druhé	druhý	k4xOgFnSc2	druhý
expedice	expedice	k1gFnSc2	expedice
na	na	k7c4	na
Mars	Mars	k1gInSc4	Mars
<g/>
.	.	kIx.	.
</s>
<s>
Čtyři	čtyři	k4xCgMnPc1	čtyři
astronauti	astronaut	k1gMnPc1	astronaut
shledávají	shledávat	k5eAaImIp3nP	shledávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Marťané	Marťan	k1gMnPc1	Marťan
jsou	být	k5eAaImIp3nP	být
vůči	vůči	k7c3	vůči
jejich	jejich	k3xOp3gFnSc3	jejich
přítomnosti	přítomnost	k1gFnSc3	přítomnost
podivně	podivně	k6eAd1	podivně
apatičtí	apatický	k2eAgMnPc1d1	apatický
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
ale	ale	k9	ale
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
nebo	nebo	k8xC	nebo
jiných	jiný	k2eAgFnPc2d1	jiná
planet	planeta	k1gFnPc2	planeta
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Kapitánovi	kapitán	k1gMnSc3	kapitán
expedice	expedice	k1gFnSc2	expedice
pomalu	pomalu	k6eAd1	pomalu
dochází	docházet	k5eAaImIp3nS	docházet
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
útulku	útulek	k1gInSc6	útulek
pro	pro	k7c4	pro
mentálně	mentálně	k6eAd1	mentálně
choré	chorý	k2eAgFnPc4d1	chorá
<g/>
.	.	kIx.	.
</s>
<s>
Marťané	Marťan	k1gMnPc1	Marťan
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
potkali	potkat	k5eAaPmAgMnP	potkat
<g/>
,	,	kIx,	,
považovali	považovat	k5eAaImAgMnP	považovat
jejich	jejich	k3xOp3gInSc4	jejich
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
vzhled	vzhled	k1gInSc4	vzhled
za	za	k7c4	za
kolektivní	kolektivní	k2eAgFnSc4d1	kolektivní
halucinaci	halucinace	k1gFnSc4	halucinace
<g/>
.	.	kIx.	.
</s>
<s>
Doktor	doktor	k1gMnSc1	doktor
pro	pro	k7c4	pro
tvrdohlavé	tvrdohlavý	k2eAgNnSc4d1	tvrdohlavé
odmítání	odmítání	k1gNnSc4	odmítání
astronautů	astronaut	k1gMnPc2	astronaut
uznat	uznat	k5eAaPmF	uznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejsou	být	k5eNaImIp3nP	být
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
posádku	posádka	k1gFnSc4	posádka
zabije	zabít	k5eAaPmIp3nS	zabít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Řádný	řádný	k2eAgMnSc1d1	řádný
občan	občan	k1gMnSc1	občan
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Taxpayer	Taxpayer	k1gMnSc1	Taxpayer
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
březen	březen	k1gInSc1	březen
2000	[number]	k4	2000
<g/>
/	/	kIx~	/
<g/>
2031	[number]	k4	2031
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
muž	muž	k1gMnSc1	muž
trvá	trvat	k5eAaImIp3nS	trvat
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jako	jako	k8xC	jako
řádný	řádný	k2eAgMnSc1d1	řádný
občan	občan	k1gMnSc1	občan
má	mít	k5eAaImIp3nS	mít
právo	právo	k1gNnSc4	právo
být	být	k5eAaImF	být
v	v	k7c6	v
další	další	k2eAgFnSc6d1	další
raketě	raketa	k1gFnSc6	raketa
na	na	k7c4	na
Mars	Mars	k1gInSc4	Mars
<g/>
,	,	kIx,	,
a	a	k8xC	a
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
vypukne	vypuknout	k5eAaPmIp3nS	vypuknout
nukleární	nukleární	k2eAgFnSc1d1	nukleární
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Třetí	třetí	k4xOgFnSc1	třetí
expedice	expedice	k1gFnSc1	expedice
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Third	Third	k1gMnSc1	Third
Expedition	Expedition	k1gInSc1	Expedition
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
duben	duben	k1gInSc1	duben
2000	[number]	k4	2000
<g/>
/	/	kIx~	/
<g/>
2031	[number]	k4	2031
<g/>
,	,	kIx,	,
také	také	k9	také
jako	jako	k9	jako
Mars	Mars	k1gInSc1	Mars
Is	Is	k1gFnSc2	Is
Heaven	Heavna	k1gFnPc2	Heavna
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
Mars	Mars	k1gInSc1	Mars
je	být	k5eAaImIp3nS	být
nebe	nebe	k1gNnSc4	nebe
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
příběh	příběh	k1gInSc4	příběh
třetí	třetí	k4xOgFnSc2	třetí
expedice	expedice	k1gFnSc2	expedice
na	na	k7c4	na
Mars	Mars	k1gInSc4	Mars
<g/>
.	.	kIx.	.
</s>
<s>
Tentokrát	tentokrát	k6eAd1	tentokrát
jsou	být	k5eAaImIp3nP	být
Marťané	Marťan	k1gMnPc1	Marťan
na	na	k7c4	na
pozemšťany	pozemšťan	k1gMnPc4	pozemšťan
připraveni	připraven	k2eAgMnPc1d1	připraven
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
posádka	posádka	k1gFnSc1	posádka
dorazí	dorazit	k5eAaPmIp3nS	dorazit
<g/>
,	,	kIx,	,
vidí	vidět	k5eAaImIp3nS	vidět
idylické	idylický	k2eAgNnSc1d1	idylické
městečko	městečko	k1gNnSc1	městečko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
obývají	obývat	k5eAaImIp3nP	obývat
jejich	jejich	k3xOp3gMnPc1	jejich
mrtví	mrtvý	k1gMnPc1	mrtvý
blízcí	blízký	k2eAgMnPc1d1	blízký
<g/>
.	.	kIx.	.
</s>
<s>
Posádka	posádka	k1gFnSc1	posádka
ignoruje	ignorovat	k5eAaImIp3nS	ignorovat
kapitánovy	kapitánův	k2eAgInPc4d1	kapitánův
rozkazy	rozkaz	k1gInPc4	rozkaz
a	a	k8xC	a
připojí	připojit	k5eAaPmIp3nP	připojit
se	se	k3xPyFc4	se
k	k	k7c3	k
domnělým	domnělý	k2eAgMnPc3d1	domnělý
členům	člen	k1gMnPc3	člen
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Marťané	Marťan	k1gMnPc1	Marťan
ale	ale	k9	ale
jen	jen	k9	jen
použili	použít	k5eAaPmAgMnP	použít
vzpomínky	vzpomínka	k1gFnPc4	vzpomínka
astronautů	astronaut	k1gMnPc2	astronaut
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
je	on	k3xPp3gNnPc4	on
vylákali	vylákat	k5eAaPmAgMnP	vylákat
do	do	k7c2	do
jejich	jejich	k3xOp3gInPc2	jejich
"	"	kIx"	"
<g/>
starých	starý	k2eAgInPc2d1	starý
<g/>
"	"	kIx"	"
domovů	domov	k1gInPc2	domov
a	a	k8xC	a
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
je	on	k3xPp3gInPc4	on
zabijí	zabít	k5eAaPmIp3nP	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Ráno	ráno	k6eAd1	ráno
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
šestnácti	šestnáct	k4xCc2	šestnáct
domů	dům	k1gInPc2	dům
vyneseno	vynést	k5eAaPmNgNnS	vynést
šestnáct	šestnáct	k4xCc4	šestnáct
rakví	rakev	k1gFnPc2	rakev
<g/>
.	.	kIx.	.
</s>
<s>
Pohřbí	pohřbít	k5eAaPmIp3nS	pohřbít
je	on	k3xPp3gMnPc4	on
truchlící	truchlící	k2eAgMnPc4d1	truchlící
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
někdy	někdy	k6eAd1	někdy
vypadají	vypadat	k5eAaPmIp3nP	vypadat
jako	jako	k9	jako
lidé	člověk	k1gMnPc1	člověk
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
jako	jako	k9	jako
"	"	kIx"	"
<g/>
cosi	cosi	k3yInSc1	cosi
jiného	jiný	k2eAgNnSc2d1	jiné
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
...	...	k?	...
<g/>
ač	ač	k8xS	ač
bude	být	k5eAaImBp3nS	být
stejně	stejně	k6eAd1	stejně
luna	luna	k1gFnSc1	luna
plát	plát	k1gInSc1	plát
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
And	Anda	k1gFnPc2	Anda
the	the	k?	the
Moon	Moon	k1gMnSc1	Moon
Be	Be	k1gMnSc1	Be
Still	Still	k1gMnSc1	Still
as	as	k9	as
Bright	Bright	k2eAgMnSc1d1	Bright
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
červen	červen	k1gInSc1	červen
2001	[number]	k4	2001
<g/>
/	/	kIx~	/
<g/>
2032	[number]	k4	2032
<g/>
.	.	kIx.	.
</s>
<s>
Muži	muž	k1gMnPc1	muž
ze	z	k7c2	z
čtvrté	čtvrtá	k1gFnSc2	čtvrtá
expedice	expedice	k1gFnSc2	expedice
zjišťují	zjišťovat	k5eAaImIp3nP	zjišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
všichni	všechen	k3xTgMnPc1	všechen
Marťané	Marťan	k1gMnPc1	Marťan
zemřeli	zemřít	k5eAaPmAgMnP	zemřít
na	na	k7c4	na
nákázu	nákáza	k1gFnSc4	nákáza
<g/>
,	,	kIx,	,
přinesenou	přinesený	k2eAgFnSc4d1	přinesená
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
tří	tři	k4xCgInPc2	tři
expedic	expedice	k1gFnPc2	expedice
<g/>
.	.	kIx.	.
</s>
<s>
Archeolog	archeolog	k1gMnSc1	archeolog
Spender	Spender	k1gMnSc1	Spender
je	být	k5eAaImIp3nS	být
rozrušený	rozrušený	k2eAgMnSc1d1	rozrušený
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
lidská	lidský	k2eAgFnSc1d1	lidská
rasa	rasa	k1gFnSc1	rasa
(	(	kIx(	(
<g/>
opět	opět	k6eAd1	opět
jednou	jednou	k6eAd1	jednou
<g/>
)	)	kIx)	)
způsobila	způsobit	k5eAaPmAgFnS	způsobit
a	a	k8xC	a
vydá	vydat	k5eAaPmIp3nS	vydat
se	se	k3xPyFc4	se
zkoumat	zkoumat	k5eAaImF	zkoumat
marťanské	marťanský	k2eAgFnSc2d1	Marťanská
ruiny	ruina	k1gFnSc2	ruina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Osídlenci	osídlenec	k1gMnPc1	osídlenec
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Settlers	Settlersa	k1gFnPc2	Settlersa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
srpen	srpen	k1gInSc1	srpen
2001	[number]	k4	2001
<g/>
/	/	kIx~	/
<g/>
2032	[number]	k4	2032
<g/>
.	.	kIx.	.
</s>
<s>
Spender	Spender	k1gInSc1	Spender
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
k	k	k7c3	k
expedici	expedice	k1gFnSc3	expedice
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
s	s	k7c7	s
sebou	se	k3xPyFc7	se
zbraň	zbraň	k1gFnSc4	zbraň
a	a	k8xC	a
zastřelí	zastřelit	k5eAaPmIp3nS	zastřelit
šest	šest	k4xCc4	šest
mužů	muž	k1gMnPc2	muž
z	z	k7c2	z
posádky	posádka	k1gFnSc2	posádka
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
on	on	k3xPp3gMnSc1	on
je	být	k5eAaImIp3nS	být
posledním	poslední	k2eAgMnSc7d1	poslední
Marťanem	Marťan	k1gMnSc7	Marťan
<g/>
.	.	kIx.	.
</s>
<s>
Kapitánu	kapitán	k1gMnSc3	kapitán
Wilderovi	Wilder	k1gMnSc3	Wilder
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podaří	podařit	k5eAaPmIp3nS	podařit
posádku	posádka	k1gFnSc4	posádka
zabít	zabít	k5eAaPmF	zabít
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařit	podařit	k5eAaPmF	podařit
oddálit	oddálit	k5eAaPmF	oddálit
lidskou	lidský	k2eAgFnSc4d1	lidská
kolonizaci	kolonizace	k1gFnSc4	kolonizace
Marsu	Mars	k1gInSc2	Mars
o	o	k7c4	o
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
možná	možná	k9	možná
na	na	k7c4	na
dost	dost	k6eAd1	dost
dlouho	dlouho	k6eAd1	dlouho
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
očekávaná	očekávaný	k2eAgFnSc1d1	očekávaná
nukleární	nukleární	k2eAgFnSc1d1	nukleární
válka	válka	k1gFnSc1	válka
uchránila	uchránit	k5eAaPmAgFnS	uchránit
Mars	Mars	k1gInSc4	Mars
od	od	k7c2	od
kolonizace	kolonizace	k1gFnSc2	kolonizace
úplně	úplně	k6eAd1	úplně
<g/>
.	.	kIx.	.
</s>
<s>
Kapitán	kapitán	k1gMnSc1	kapitán
sice	sice	k8xC	sice
nesouhlasí	souhlasit	k5eNaImIp3nS	souhlasit
se	s	k7c7	s
Spenderovými	Spenderův	k2eAgFnPc7d1	Spenderův
metodami	metoda	k1gFnPc7	metoda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
souhlasí	souhlasit	k5eAaImIp3nP	souhlasit
s	s	k7c7	s
jeho	jeho	k3xOp3gInSc7	jeho
přístupem	přístup	k1gInSc7	přístup
ke	k	k7c3	k
kolonizaci	kolonizace	k1gFnSc3	kolonizace
a	a	k8xC	a
přeje	přát	k5eAaImIp3nS	přát
mu	on	k3xPp3gMnSc3	on
lehkou	lehký	k2eAgFnSc4d1	lehká
smrt	smrt	k1gFnSc4	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
ho	on	k3xPp3gInSc4	on
při	při	k7c6	při
pronásledování	pronásledování	k1gNnSc6	pronásledování
zastřelí	zastřelit	k5eAaPmIp3nS	zastřelit
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
uhodí	uhodit	k5eAaPmIp3nS	uhodit
dalšího	další	k2eAgMnSc4d1	další
člena	člen	k1gMnSc4	člen
posádky	posádka	k1gFnSc2	posádka
Parkhilla	Parkhilla	k1gFnSc1	Parkhilla
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
k	k	k7c3	k
marťanským	marťanský	k2eAgFnPc3d1	Marťanská
ruinám	ruina	k1gFnPc3	ruina
nechová	chovat	k5eNaImIp3nS	chovat
s	s	k7c7	s
úctou	úcta	k1gFnSc7	úcta
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
postavy	postava	k1gFnPc1	postava
ze	z	k7c2	z
čtvrté	čtvrtá	k1gFnSc2	čtvrtá
expedice	expedice	k1gFnSc2	expedice
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
objevují	objevovat	k5eAaImIp3nP	objevovat
znovu	znovu	k6eAd1	znovu
<g/>
.	.	kIx.	.
</s>
<s>
Bradbury	Bradbur	k1gInPc1	Bradbur
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
Spender	Spender	k1gInSc4	Spender
<g/>
,	,	kIx,	,
vidí	vidět	k5eAaImIp3nP	vidět
některé	některý	k3yIgInPc4	některý
způsoby	způsob	k1gInPc4	způsob
kolonizace	kolonizace	k1gFnSc1	kolonizace
jako	jako	k8xS	jako
špatné	špatný	k2eAgNnSc1d1	špatné
a	a	k8xC	a
jiné	jiný	k2eAgNnSc1d1	jiné
jako	jako	k8xC	jako
dobré	dobrý	k2eAgNnSc1d1	dobré
<g/>
.	.	kIx.	.
</s>
<s>
Pokoušet	pokoušet	k5eAaImF	pokoušet
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
vytvořit	vytvořit	k5eAaPmF	vytvořit
Zemi	zem	k1gFnSc4	zem
je	být	k5eAaImIp3nS	být
viděno	vidět	k5eAaImNgNnS	vidět
jako	jako	k8xC	jako
špatné	špatný	k2eAgNnSc1d1	špatné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přístup	přístup	k1gInSc1	přístup
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
ctí	ctít	k5eAaImIp3nS	ctít
padlou	padlý	k2eAgFnSc4d1	padlá
civilizaci	civilizace	k1gFnSc4	civilizace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
nahrazovaná	nahrazovaný	k2eAgFnSc1d1	nahrazovaná
novou	nova	k1gFnSc7	nova
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dobrý	dobrý	k2eAgMnSc1d1	dobrý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zelené	Zelené	k2eAgNnSc1d1	Zelené
ráno	ráno	k1gNnSc1	ráno
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Green	Green	k1gInSc1	Green
Morning	Morning	k1gInSc1	Morning
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
prosinec	prosinec	k1gInSc1	prosinec
2001	[number]	k4	2001
<g/>
/	/	kIx~	/
<g/>
2032	[number]	k4	2032
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
stromy	strom	k1gInPc1	strom
vytvářející	vytvářející	k2eAgInSc1d1	vytvářející
kyslík	kyslík	k1gInSc4	kyslík
a	a	k8xC	a
vznikají	vznikat	k5eAaImIp3nP	vznikat
malá	malý	k2eAgNnPc4d1	malé
lidská	lidský	k2eAgNnPc4d1	lidské
městečka	městečko	k1gNnPc4	městečko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kobylky	kobylka	k1gFnPc1	kobylka
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Locusts	Locustsa	k1gFnPc2	Locustsa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
únor	únor	k1gInSc1	únor
2002	[number]	k4	2002
<g/>
/	/	kIx~	/
<g/>
2033	[number]	k4	2033
<g/>
.	.	kIx.	.
</s>
<s>
Mars	Mars	k1gInSc1	Mars
je	být	k5eAaImIp3nS	být
překotně	překotně	k6eAd1	překotně
kolonizován	kolonizován	k2eAgInSc1d1	kolonizován
<g/>
,	,	kIx,	,
přilétají	přilétat	k5eAaImIp3nP	přilétat
další	další	k2eAgFnPc1d1	další
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
rakety	raketa	k1gFnPc1	raketa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Noční	noční	k2eAgNnSc1d1	noční
setkání	setkání	k1gNnSc1	setkání
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
Night	Night	k2eAgInSc1d1	Night
Meeting	meeting	k1gInSc1	meeting
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
srpen	srpen	k1gInSc1	srpen
2002	[number]	k4	2002
<g/>
/	/	kIx~	/
<g/>
2033	[number]	k4	2033
<g/>
.	.	kIx.	.
</s>
<s>
Starý	starý	k2eAgMnSc1d1	starý
muž	muž	k1gMnSc1	muž
říká	říkat	k5eAaImIp3nS	říkat
cestovateli	cestovatel	k1gMnSc3	cestovatel
Gomezovi	Gomez	k1gMnSc3	Gomez
<g/>
,	,	kIx,	,
že	že	k8xS	že
přišel	přijít	k5eAaPmAgMnS	přijít
na	na	k7c4	na
Mars	Mars	k1gInSc4	Mars
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
oceňuje	oceňovat	k5eAaImIp3nS	oceňovat
novoty	novota	k1gFnPc4	novota
<g/>
.	.	kIx.	.
</s>
<s>
Každodenní	každodenní	k2eAgFnPc1d1	každodenní
věci	věc	k1gFnPc1	věc
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
znovu	znovu	k6eAd1	znovu
úžasné	úžasný	k2eAgFnPc1d1	úžasná
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
Gomez	Gomez	k1gMnSc1	Gomez
potká	potkat	k5eAaPmIp3nS	potkat
Marťana	Marťan	k1gMnSc4	Marťan
a	a	k8xC	a
oba	dva	k4xCgMnPc4	dva
vidí	vidět	k5eAaImIp3nS	vidět
Mars	Mars	k1gInSc1	Mars
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yIgInSc4	který
jsou	být	k5eAaImIp3nP	být
zvyklí	zvyklý	k2eAgMnPc1d1	zvyklý
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
čase	čas	k1gInSc6	čas
a	a	k8xC	a
mlhavou	mlhavý	k2eAgFnSc4d1	mlhavá
vizi	vize	k1gFnSc4	vize
druhé	druhý	k4xOgFnSc2	druhý
verze	verze	k1gFnSc2	verze
<g/>
.	.	kIx.	.
</s>
<s>
Mladý	mladý	k2eAgMnSc1d1	mladý
muž	muž	k1gMnSc1	muž
vidí	vidět	k5eAaImIp3nS	vidět
ruiny	ruina	k1gFnPc4	ruina
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Marťan	Marťan	k1gMnSc1	Marťan
vidí	vidět	k5eAaImIp3nS	vidět
živé	živý	k2eAgNnSc4d1	živé
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Marťan	Marťan	k1gMnSc1	Marťan
vidí	vidět	k5eAaImIp3nS	vidět
oceán	oceán	k1gInSc4	oceán
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Gomez	Gomez	k1gMnSc1	Gomez
vidí	vidět	k5eAaImIp3nS	vidět
nové	nový	k2eAgNnSc4d1	nové
pozemské	pozemský	k2eAgNnSc4d1	pozemské
osídlení	osídlení	k1gNnSc4	osídlení
<g/>
.	.	kIx.	.
</s>
<s>
Bradbury	Bradbur	k1gMnPc4	Bradbur
tak	tak	k9	tak
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
každá	každý	k3xTgFnSc1	každý
civilizace	civilizace	k1gFnSc1	civilizace
jednou	jednou	k6eAd1	jednou
zmizí	zmizet	k5eAaPmIp3nS	zmizet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Břeh	břeh	k1gInSc1	břeh
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
The	The	k1gMnSc5	The
Shore	Shor	k1gMnSc5	Shor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
říjen	říjen	k1gInSc1	říjen
2002	[number]	k4	2002
<g/>
/	/	kIx~	/
<g/>
2033	[number]	k4	2033
<g/>
.	.	kIx.	.
</s>
<s>
Povídka	povídka	k1gFnSc1	povídka
popisuje	popisovat	k5eAaImIp3nS	popisovat
první	první	k4xOgFnSc4	první
vlnu	vlna	k1gFnSc4	vlna
osídlenců	osídlenec	k1gMnPc2	osídlenec
jako	jako	k8xC	jako
osamělé	osamělý	k2eAgInPc1d1	osamělý
pionýry	pionýr	k1gInPc1	pionýr
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
přicházejí	přicházet	k5eAaImIp3nP	přicházet
chudí	chudý	k2eAgMnPc1d1	chudý
lidé	člověk	k1gMnPc1	člověk
z	z	k7c2	z
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezidobí	mezidobí	k1gNnSc1	mezidobí
(	(	kIx(	(
<g/>
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
Imterim	Imterim	k1gInSc1	Imterim
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
únor	únor	k1gInSc1	únor
2003	[number]	k4	2003
<g/>
/	/	kIx~	/
<g/>
2034	[number]	k4	2034
<g/>
.	.	kIx.	.
</s>
<s>
Povídka	povídka	k1gFnSc1	povídka
popisuje	popisovat	k5eAaImIp3nS	popisovat
stavbu	stavba	k1gFnSc4	stavba
nového	nový	k2eAgNnSc2d1	nové
města	město	k1gNnSc2	město
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
připomíná	připomínat	k5eAaImIp3nS	připomínat
průměrné	průměrný	k2eAgNnSc1d1	průměrné
středozápadní	středozápadní	k2eAgNnSc1d1	středozápadní
americké	americký	k2eAgNnSc1d1	americké
město	město	k1gNnSc1	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Muzikanti	muzikant	k1gMnPc1	muzikant
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Musicians	Musiciansa	k1gFnPc2	Musiciansa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
duben	duben	k1gInSc1	duben
2003	[number]	k4	2003
<g/>
/	/	kIx~	/
<g/>
2034	[number]	k4	2034
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
chlapců	chlapec	k1gMnPc2	chlapec
si	se	k3xPyFc3	se
hraje	hrát	k5eAaImIp3nS	hrát
v	v	k7c6	v
okouzlujících	okouzlující	k2eAgFnPc6d1	okouzlující
ruinách	ruina	k1gFnPc6	ruina
marťanských	marťanský	k2eAgFnPc2d1	Marťanská
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
mají	mít	k5eAaImIp3nP	mít
přijít	přijít	k5eAaPmF	přijít
požárnící	požárnící	k2eAgMnPc1d1	požárnící
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
ruiny	ruina	k1gFnPc4	ruina
vyčistit	vyčistit	k5eAaPmF	vyčistit
a	a	k8xC	a
připraví	připravit	k5eAaPmIp3nS	připravit
je	on	k3xPp3gFnPc4	on
tak	tak	k9	tak
o	o	k7c4	o
zábavu	zábava	k1gFnSc4	zábava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Má	můj	k3xOp1gFnSc1	můj
duše	duše	k1gFnSc1	duše
se	se	k3xPyFc4	se
vznáší	vznášet	k5eAaImIp3nS	vznášet
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
Way	Way	k1gFnSc1	Way
in	in	k?	in
the	the	k?	the
Middle	Middle	k1gFnSc2	Middle
of	of	k?	of
the	the	k?	the
Air	Air	k1gFnSc2	Air
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
červen	červen	k1gInSc1	červen
2003	[number]	k4	2003
<g/>
/	/	kIx~	/
<g/>
2034	[number]	k4	2034
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
neznámém	známý	k2eNgNnSc6d1	neznámé
jižanském	jižanský	k2eAgNnSc6d1	jižanské
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
bělochů	běloch	k1gMnPc2	běloch
dozvídá	dozvídat	k5eAaImIp3nS	dozvídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
černoši	černoch	k1gMnPc1	černoch
plánují	plánovat	k5eAaImIp3nP	plánovat
emigrovat	emigrovat	k5eAaBmF	emigrovat
na	na	k7c4	na
Mars	Mars	k1gInSc4	Mars
<g/>
.	.	kIx.	.
</s>
<s>
Rasista	rasista	k1gMnSc1	rasista
Samuel	Samuel	k1gMnSc1	Samuel
Teece	Teece	k1gMnSc1	Teece
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
zastavit	zastavit	k5eAaPmF	zastavit
ty	ten	k3xDgInPc4	ten
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yRgInPc7	který
má	mít	k5eAaImIp3nS	mít
nevyřízené	vyřízený	k2eNgInPc4d1	nevyřízený
účty	účet	k1gInPc4	účet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
oni	onen	k3xDgMnPc1	onen
díky	díky	k7c3	díky
solidaritě	solidarita	k1gFnSc6	solidarita
ostatních	ostatní	k1gNnPc2	ostatní
odcházejí	odcházet	k5eAaImIp3nP	odcházet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nová	nový	k2eAgNnPc1d1	nové
jména	jméno	k1gNnPc1	jméno
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Naming	Naming	k1gInSc1	Naming
of	of	k?	of
Names	Names	k1gInSc1	Names
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
/	/	kIx~	/
<g/>
2035	[number]	k4	2035
<g/>
-	-	kIx~	-
<g/>
36	[number]	k4	36
<g/>
.	.	kIx.	.
</s>
<s>
Přicházejí	přicházet	k5eAaImIp3nP	přicházet
další	další	k2eAgFnPc1d1	další
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
vlny	vlna	k1gFnPc1	vlna
kolonistů	kolonista	k1gMnPc2	kolonista
<g/>
.	.	kIx.	.
</s>
<s>
Prvky	prvek	k1gInPc1	prvek
krajiny	krajina	k1gFnSc2	krajina
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
dostávají	dostávat	k5eAaImIp3nP	dostávat
jména	jméno	k1gNnPc1	jméno
po	po	k7c6	po
účastnících	účastnící	k2eAgInPc2d1	účastnící
prvních	první	k4xOgInPc2	první
čtyř	čtyři	k4xCgInPc2	čtyři
expedic	expedice	k1gFnPc2	expedice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Usher	Usher	k1gMnSc1	Usher
II	II	kA	II
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
duben	duben	k1gInSc1	duben
2005	[number]	k4	2005
<g/>
/	/	kIx~	/
<g/>
2036	[number]	k4	2036
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
o	o	k7c6	o
cenzuře	cenzura	k1gFnSc6	cenzura
<g/>
.	.	kIx.	.
</s>
<s>
William	William	k1gInSc1	William
Stendahl	Stendahl	k1gFnSc2	Stendahl
je	být	k5eAaImIp3nS	být
milovník	milovník	k1gMnSc1	milovník
knih	kniha	k1gFnPc2	kniha
a	a	k8xC	a
odešel	odejít	k5eAaPmAgMnS	odejít
na	na	k7c4	na
Mars	Mars	k1gInSc4	Mars
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
vláda	vláda	k1gFnSc1	vláda
zkonfiskovala	zkonfiskovat	k5eAaPmAgFnS	zkonfiskovat
a	a	k8xC	a
zničila	zničit	k5eAaPmAgFnS	zničit
jeho	jeho	k3xOp3gFnSc4	jeho
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
sbírku	sbírka	k1gFnSc4	sbírka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
vytvoří	vytvořit	k5eAaPmIp3nP	vytvořit
svoji	svůj	k3xOyFgFnSc4	svůj
vizi	vize	k1gFnSc4	vize
dokonalého	dokonalý	k2eAgInSc2d1	dokonalý
strašidelného	strašidelný	k2eAgInSc2d1	strašidelný
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
mechanických	mechanický	k2eAgMnPc2d1	mechanický
tvorů	tvor	k1gMnPc2	tvor
<g/>
,	,	kIx,	,
děsivé	děsivý	k2eAgMnPc4d1	děsivý
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
jedů	jed	k1gInPc2	jed
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
do	do	k7c2	do
domu	dům	k1gInSc2	dům
přijde	přijít	k5eAaPmIp3nS	přijít
inspekce	inspekce	k1gFnSc1	inspekce
<g/>
,	,	kIx,	,
každého	každý	k3xTgMnSc2	každý
z	z	k7c2	z
inspektorů	inspektor	k1gMnPc2	inspektor
zabije	zabít	k5eAaPmIp3nS	zabít
v	v	k7c6	v
hororovém	hororový	k2eAgInSc6d1	hororový
stylu	styl	k1gInSc6	styl
na	na	k7c6	na
základě	základ	k1gInSc6	základ
příběhů	příběh	k1gInPc2	příběh
Edgara	Edgar	k1gMnSc4	Edgar
Allana	Allan	k1gMnSc4	Allan
Poea	Poeus	k1gMnSc4	Poeus
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
helikoptéry	helikoptéra	k1gFnSc2	helikoptéra
pak	pak	k6eAd1	pak
sleduje	sledovat	k5eAaImIp3nS	sledovat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
dům	dům	k1gInSc1	dům
zřítí	zřítit	k5eAaPmIp3nS	zřítit
do	do	k7c2	do
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stáří	stáří	k1gNnSc1	stáří
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Old	Olda	k1gFnPc2	Olda
Ones	Onesa	k1gFnPc2	Onesa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
srpen	srpen	k1gInSc1	srpen
2005	[number]	k4	2005
<g/>
/	/	kIx~	/
<g/>
2036	[number]	k4	2036
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
pokročile	pokročile	k6eAd1	pokročile
kolonizovaný	kolonizovaný	k2eAgInSc4d1	kolonizovaný
Mars	Mars	k1gInSc4	Mars
konečně	konečně	k6eAd1	konečně
dorážejí	dorážet	k5eAaImIp3nP	dorážet
i	i	k9	i
staří	starý	k2eAgMnPc1d1	starý
lidé	člověk	k1gMnPc1	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Marťan	Marťan	k1gMnSc1	Marťan
(	(	kIx(	(
<g/>
1949	[number]	k4	1949
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Martian	Martian	k1gMnSc1	Martian
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
září	září	k1gNnSc2	září
2005	[number]	k4	2005
<g/>
/	/	kIx~	/
<g/>
2036	[number]	k4	2036
<g/>
.	.	kIx.	.
</s>
<s>
LaFarge	LaFarge	k1gInSc1	LaFarge
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
žena	žena	k1gFnSc1	žena
Anna	Anna	k1gFnSc1	Anna
si	se	k3xPyFc3	se
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
nový	nový	k2eAgInSc4d1	nový
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stále	stále	k6eAd1	stále
jim	on	k3xPp3gMnPc3	on
chybí	chybět	k5eAaImIp3nS	chybět
jejich	jejich	k3xOp3gMnSc1	jejich
mrtvý	mrtvý	k2eAgMnSc1d1	mrtvý
syn	syn	k1gMnSc1	syn
Tom	Tom	k1gMnSc1	Tom
<g/>
.	.	kIx.	.
</s>
<s>
Jedné	jeden	k4xCgFnSc6	jeden
noci	noc	k1gFnSc6	noc
před	před	k7c7	před
domem	dům	k1gInSc7	dům
vidí	vidět	k5eAaImIp3nP	vidět
postavu	postava	k1gFnSc4	postava
stojící	stojící	k2eAgFnSc4d1	stojící
v	v	k7c6	v
dešti	dešť	k1gInSc6	dešť
<g/>
.	.	kIx.	.
</s>
<s>
Anna	Anna	k1gFnSc1	Anna
se	se	k3xPyFc4	se
bojí	bát	k5eAaImIp3nS	bát
a	a	k8xC	a
jde	jít	k5eAaImIp3nS	jít
do	do	k7c2	do
postele	postel	k1gFnSc2	postel
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
LaFarge	LaFarge	k1gFnSc1	LaFarge
věří	věřit	k5eAaImIp3nS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Tom	Tom	k1gMnSc1	Tom
tu	tu	k6eAd1	tu
teď	teď	k6eAd1	teď
nějakým	nějaký	k3yIgInSc7	nějaký
způsobem	způsob	k1gInSc7	způsob
znovu	znovu	k6eAd1	znovu
stojí	stát	k5eAaImIp3nS	stát
před	před	k7c7	před
ním	on	k3xPp3gMnSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Dům	dům	k1gInSc1	dům
nechá	nechat	k5eAaPmIp3nS	nechat
odemčený	odemčený	k2eAgInSc1d1	odemčený
<g/>
.	.	kIx.	.
</s>
<s>
Ráno	ráno	k1gNnSc1	ráno
"	"	kIx"	"
<g/>
Tom	Tom	k1gMnSc1	Tom
<g/>
"	"	kIx"	"
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
Anně	Anna	k1gFnSc3	Anna
<g/>
.	.	kIx.	.
</s>
<s>
LaFarge	LaFarge	k6eAd1	LaFarge
vidí	vidět	k5eAaImIp3nS	vidět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Anna	Anna	k1gFnSc1	Anna
jaksi	jaksi	k6eAd1	jaksi
zapomněla	zapomnět	k5eAaImAgFnS	zapomnět
na	na	k7c4	na
Tomovu	Tomův	k2eAgFnSc4d1	Tomova
smrt	smrt	k1gFnSc4	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Promluví	promluvit	k5eAaPmIp3nP	promluvit
si	se	k3xPyFc3	se
s	s	k7c7	s
"	"	kIx"	"
<g/>
Tomem	Tom	k1gMnSc7	Tom
<g/>
"	"	kIx"	"
a	a	k8xC	a
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
Marťan	Marťan	k1gMnSc1	Marťan
s	s	k7c7	s
empatickou	empatický	k2eAgFnSc7d1	empatická
přeměňovací	přeměňovací	k2eAgFnSc7d1	přeměňovací
schopností	schopnost	k1gFnSc7	schopnost
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
ho	on	k3xPp3gMnSc4	on
vidí	vidět	k5eAaImIp3nS	vidět
jako	jako	k9	jako
svého	svůj	k3xOyFgMnSc4	svůj
mrtvého	mrtvý	k2eAgMnSc4d1	mrtvý
syna	syn	k1gMnSc4	syn
<g/>
.	.	kIx.	.
</s>
<s>
Anna	Anna	k1gFnSc1	Anna
trvá	trvat	k5eAaImIp3nS	trvat
na	na	k7c6	na
návštěvě	návštěva	k1gFnSc6	návštěva
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Tom	Tom	k1gMnSc1	Tom
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
zděšený	zděšený	k2eAgMnSc1d1	zděšený
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
tolika	tolik	k4xDc2	tolik
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
LaFarge	LaFarge	k1gInSc1	LaFarge
mu	on	k3xPp3gMnSc3	on
ale	ale	k9	ale
slibuje	slibovat	k5eAaImIp3nS	slibovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
blízkosti	blízkost	k1gFnSc6	blízkost
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
ve	v	k7c6	v
městě	město	k1gNnSc6	město
jeden	jeden	k4xCgMnSc1	jeden
druhému	druhý	k4xOgInSc3	druhý
ztratí	ztratit	k5eAaPmIp3nS	ztratit
<g/>
.	.	kIx.	.
</s>
<s>
LaFarge	LaFarge	k6eAd1	LaFarge
se	se	k3xPyFc4	se
doslechne	doslechnout	k5eAaPmIp3nS	doslechnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
rodina	rodina	k1gFnSc1	rodina
Spauldingových	Spauldingový	k2eAgMnPc2d1	Spauldingový
má	mít	k5eAaImIp3nS	mít
zázrakem	zázrak	k1gInSc7	zázrak
zpět	zpět	k6eAd1	zpět
svou	svůj	k3xOyFgFnSc4	svůj
dceru	dcera	k1gFnSc4	dcera
<g/>
.	.	kIx.	.
</s>
<s>
Zoufalý	zoufalý	k2eAgInSc1d1	zoufalý
LaFarge	LaFarge	k1gInSc1	LaFarge
nechce	chtít	k5eNaImIp3nS	chtít
své	svůj	k3xOyFgFnSc3	svůj
ženě	žena	k1gFnSc3	žena
přivodit	přivodit	k5eAaPmF	přivodit
další	další	k2eAgFnSc1d1	další
bolest	bolest	k1gFnSc1	bolest
a	a	k8xC	a
přemlouvá	přemlouvat	k5eAaImIp3nS	přemlouvat
"	"	kIx"	"
<g/>
Toma	Tom	k1gMnSc4	Tom
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
dívku	dívka	k1gFnSc4	dívka
Lavinii	Lavinie	k1gFnSc4	Lavinie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
k	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
<g/>
.	.	kIx.	.
</s>
<s>
Utíkají	utíkat	k5eAaImIp3nP	utíkat
z	z	k7c2	z
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
<g/>
,	,	kIx,	,
koho	kdo	k3yQnSc2	kdo
potkají	potkat	k5eAaPmIp3nP	potkat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
"	"	kIx"	"
<g/>
Tomovi	Tomův	k2eAgMnPc1d1	Tomův
<g/>
"	"	kIx"	"
vidí	vidět	k5eAaImIp3nP	vidět
někoho	někdo	k3yInSc4	někdo
jiného	jiný	k2eAgMnSc4d1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
Marťan	Marťan	k1gMnSc1	Marťan
unavený	unavený	k2eAgMnSc1d1	unavený
konstantním	konstantní	k2eAgNnPc3d1	konstantní
přeměňováním	přeměňování	k1gNnPc3	přeměňování
umírá	umírat	k5eAaImIp3nS	umírat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Brašnářství	brašnářství	k1gNnSc1	brašnářství
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
The	The	k1gMnSc5	The
Luggage	Luggagus	k1gMnSc5	Luggagus
Store	Stor	k1gMnSc5	Stor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
listopad	listopad	k1gInSc1	listopad
2005	[number]	k4	2005
<g/>
/	/	kIx~	/
<g/>
2036	[number]	k4	2036
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
bezprostředně	bezprostředně	k6eAd1	bezprostředně
hrozí	hrozit	k5eAaImIp3nS	hrozit
nukleární	nukleární	k2eAgFnSc1d1	nukleární
válka	válka	k1gFnSc1	válka
a	a	k8xC	a
kněz	kněz	k1gMnSc1	kněz
předvídá	předvídat	k5eAaImIp3nS	předvídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
většina	většina	k1gFnSc1	většina
kolonistů	kolonista	k1gMnPc2	kolonista
se	se	k3xPyFc4	se
vrátí	vrátit	k5eAaPmIp3nS	vrátit
na	na	k7c4	na
Zem	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mrtvá	mrtvý	k2eAgFnSc1d1	mrtvá
sezóna	sezóna	k1gFnSc1	sezóna
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Off	Off	k1gMnSc1	Off
Season	Season	k1gMnSc1	Season
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
listopad	listopad	k1gInSc1	listopad
2005	[number]	k4	2005
<g/>
/	/	kIx~	/
<g/>
2036	[number]	k4	2036
<g/>
.	.	kIx.	.
</s>
<s>
Bývalý	bývalý	k2eAgMnSc1d1	bývalý
člen	člen	k1gMnSc1	člen
čtvrté	čtvrtá	k1gFnSc2	čtvrtá
expedice	expedice	k1gFnSc1	expedice
Sam	Sam	k1gMnSc1	Sam
Parkhill	Parkhill	k1gMnSc1	Parkhill
si	se	k3xPyFc3	se
otevřel	otevřít	k5eAaPmAgMnS	otevřít
stánek	stánek	k1gInSc4	stánek
s	s	k7c7	s
hot-dogy	hotog	k1gInPc7	hot-dog
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
očekává	očekávat	k5eAaImIp3nS	očekávat
růst	růst	k1gInSc4	růst
obchodu	obchod	k1gInSc2	obchod
díky	díky	k7c3	díky
nově	nova	k1gFnSc3	nova
příchozím	příchozí	k1gFnPc3	příchozí
osadníkům	osadník	k1gMnPc3	osadník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
panice	panika	k1gFnSc6	panika
zabíjí	zabíjet	k5eAaImIp3nP	zabíjet
osamělého	osamělý	k2eAgMnSc4d1	osamělý
Marťana	Marťan	k1gMnSc4	Marťan
<g/>
.	.	kIx.	.
</s>
<s>
Přicházejí	přicházet	k5eAaImIp3nP	přicházet
ostatní	ostatní	k2eAgMnPc1d1	ostatní
Marťané	Marťan	k1gMnPc1	Marťan
v	v	k7c6	v
písečných	písečný	k2eAgFnPc6d1	písečná
lodích	loď	k1gFnPc6	loď
a	a	k8xC	a
žádají	žádat	k5eAaImIp3nP	žádat
Parkhilla	Parkhill	k1gMnSc4	Parkhill
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
ženu	žena	k1gFnSc4	žena
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
přešli	přejít	k5eAaPmAgMnP	přejít
poušť	poušť	k1gFnSc4	poušť
<g/>
.	.	kIx.	.
</s>
<s>
Šokují	šokovat	k5eAaBmIp3nP	šokovat
Parkhilla	Parkhilla	k1gFnSc1	Parkhilla
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gNnSc3	on
věnují	věnovat	k5eAaPmIp3nP	věnovat
polovinu	polovina	k1gFnSc4	polovina
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Vrací	vracet	k5eAaImIp3nS	vracet
se	se	k3xPyFc4	se
ke	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
stánku	stánek	k1gInSc3	stánek
právě	právě	k6eAd1	právě
včas	včas	k6eAd1	včas
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
viděl	vidět	k5eAaImAgInS	vidět
začátek	začátek	k1gInSc4	začátek
nukleární	nukleární	k2eAgFnSc2d1	nukleární
války	válka	k1gFnSc2	válka
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
končí	končit	k5eAaImIp3nS	končit
příchod	příchod	k1gInSc1	příchod
nových	nový	k2eAgMnPc2d1	nový
kolonistů	kolonista	k1gMnPc2	kolonista
a	a	k8xC	a
jeho	jeho	k3xOp3gNnPc2	jeho
obchodování	obchodování	k1gNnPc2	obchodování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pozorovatelé	pozorovatel	k1gMnPc1	pozorovatel
(	(	kIx(	(
<g/>
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Watchers	Watchersa	k1gFnPc2	Watchersa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
listopad	listopad	k1gInSc1	listopad
2005	[number]	k4	2005
<g/>
/	/	kIx~	/
<g/>
2036	[number]	k4	2036
<g/>
.	.	kIx.	.
</s>
<s>
Kolonisté	kolonista	k1gMnPc1	kolonista
sledují	sledovat	k5eAaImIp3nP	sledovat
nukleární	nukleární	k2eAgFnSc4d1	nukleární
válku	válka	k1gFnSc4	válka
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Urychleně	urychleně	k6eAd1	urychleně
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
na	na	k7c4	na
Zem	zem	k1gFnSc4	zem
kvůli	kvůli	k7c3	kvůli
obavám	obava	k1gFnPc3	obava
o	o	k7c4	o
své	svůj	k3xOyFgMnPc4	svůj
přátele	přítel	k1gMnPc4	přítel
a	a	k8xC	a
rodiny	rodina	k1gFnPc4	rodina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mlčící	mlčící	k2eAgNnPc1d1	mlčící
města	město	k1gNnPc1	město
(	(	kIx(	(
<g/>
1949	[number]	k4	1949
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Silent	Silent	k1gMnSc1	Silent
Towns	Towns	k1gInSc1	Towns
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
prosinec	prosinec	k1gInSc1	prosinec
2005	[number]	k4	2005
<g/>
/	/	kIx~	/
<g/>
2036	[number]	k4	2036
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
už	už	k6eAd1	už
opustili	opustit	k5eAaPmAgMnP	opustit
Mars	Mars	k1gInSc4	Mars
až	až	k9	až
na	na	k7c4	na
muže	muž	k1gMnSc4	muž
jménem	jméno	k1gNnSc7	jméno
Walter	Walter	k1gMnSc1	Walter
Gripp	Gripp	k1gMnSc1	Gripp
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
žije	žít	k5eAaImIp3nS	žít
osaměle	osaměle	k6eAd1	osaměle
v	v	k7c6	v
horách	hora	k1gFnPc6	hora
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
neslyšel	slyšet	k5eNaImAgMnS	slyšet
o	o	k7c6	o
masovém	masový	k2eAgInSc6d1	masový
odchodu	odchod	k1gInSc6	odchod
<g/>
.	.	kIx.	.
</s>
<s>
Zprvu	zprvu	k6eAd1	zprvu
je	být	k5eAaImIp3nS	být
nadšený	nadšený	k2eAgInSc4d1	nadšený
objevem	objev	k1gInSc7	objev
prázdného	prázdný	k2eAgNnSc2d1	prázdné
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
užívá	užívat	k5eAaImIp3nS	užívat
si	se	k3xPyFc3	se
peněz	peníze	k1gInPc2	peníze
<g/>
,	,	kIx,	,
jídla	jídlo	k1gNnSc2	jídlo
<g/>
,	,	kIx,	,
oblečení	oblečení	k1gNnSc2	oblečení
a	a	k8xC	a
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
však	však	k9	však
začne	začít	k5eAaPmIp3nS	začít
postrádat	postrádat	k5eAaImF	postrádat
lidskou	lidský	k2eAgFnSc4d1	lidská
společnost	společnost	k1gFnSc4	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Jedné	jeden	k4xCgFnSc3	jeden
noci	noc	k1gFnSc3	noc
slyší	slyšet	k5eAaImIp3nS	slyšet
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
domě	dům	k1gInSc6	dům
zvonit	zvonit	k5eAaImF	zvonit
telefon	telefon	k1gInSc4	telefon
a	a	k8xC	a
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
ještě	ještě	k9	ještě
žije	žít	k5eAaImIp3nS	žít
někdo	někdo	k3yInSc1	někdo
další	další	k2eAgNnSc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
volání	volání	k1gNnSc6	volání
(	(	kIx(	(
<g/>
a	a	k8xC	a
několik	několik	k4yIc4	několik
následujících	následující	k2eAgFnPc2d1	následující
<g/>
)	)	kIx)	)
ale	ale	k8xC	ale
nestihne	stihnout	k5eNaPmIp3nS	stihnout
odpovědět	odpovědět	k5eAaPmF	odpovědět
<g/>
.	.	kIx.	.
</s>
<s>
Začne	začít	k5eAaPmIp3nS	začít
tedy	tedy	k8xC	tedy
obvolávat	obvolávat	k5eAaImF	obvolávat
všechna	všechen	k3xTgNnPc4	všechen
telefonní	telefonní	k2eAgNnPc4d1	telefonní
čísla	číslo	k1gNnPc4	číslo
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
od	od	k7c2	od
A.	A.	kA	A.
Později	pozdě	k6eAd2	pozdě
začne	začít	k5eAaPmIp3nS	začít
obvolávat	obvolávat	k5eAaImF	obvolávat
hotely	hotel	k1gInPc4	hotel
<g/>
.	.	kIx.	.
</s>
<s>
Odhadne	odhadnout	k5eAaPmIp3nS	odhadnout
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nS	by
žena	žena	k1gFnSc1	žena
mohla	moct	k5eAaImAgFnS	moct
zřejmě	zřejmě	k6eAd1	zřejmě
být	být	k5eAaImF	být
<g/>
,	,	kIx,	,
a	a	k8xC	a
zavolá	zavolat	k5eAaPmIp3nS	zavolat
do	do	k7c2	do
největšího	veliký	k2eAgInSc2d3	veliký
salonu	salon	k1gInSc2	salon
krásy	krása	k1gFnSc2	krása
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
<g/>
.	.	kIx.	.
</s>
<s>
Žena	žena	k1gFnSc1	žena
skutečně	skutečně	k6eAd1	skutečně
telefon	telefon	k1gInSc4	telefon
zvedne	zvednout	k5eAaPmIp3nS	zvednout
<g/>
.	.	kIx.	.
</s>
<s>
Povídají	povídat	k5eAaImIp3nP	povídat
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hovor	hovor	k1gInSc1	hovor
je	být	k5eAaImIp3nS	být
přerušen	přerušit	k5eAaPmNgInS	přerušit
<g/>
.	.	kIx.	.
</s>
<s>
Walter	Walter	k1gMnSc1	Walter
ujede	ujet	k5eAaPmIp3nS	ujet
několik	několik	k4yIc4	několik
set	set	k1gInSc4	set
mil	míle	k1gFnPc2	míle
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
ona	onen	k3xDgFnSc1	onen
žena	žena	k1gFnSc1	žena
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
vydala	vydat	k5eAaPmAgFnS	vydat
hledat	hledat	k5eAaImF	hledat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
městě	město	k1gNnSc6	město
pak	pak	k6eAd1	pak
konečně	konečně	k6eAd1	konečně
potkává	potkávat	k5eAaImIp3nS	potkávat
ženu	žena	k1gFnSc4	žena
jménem	jméno	k1gNnSc7	jméno
Genevieve	Genevieev	k1gFnSc2	Genevieev
Selsor	Selsora	k1gFnPc2	Selsora
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
setkání	setkání	k1gNnSc1	setkání
ale	ale	k9	ale
není	být	k5eNaImIp3nS	být
takové	takový	k3xDgNnSc1	takový
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
si	se	k3xPyFc3	se
romanticky	romanticky	k6eAd1	romanticky
vysnil	vysnít	k5eAaPmAgMnS	vysnít
<g/>
.	.	kIx.	.
</s>
<s>
Genevieve	Genevieev	k1gFnPc4	Genevieev
je	být	k5eAaImIp3nS	být
mdlá	mdlý	k2eAgFnSc1d1	mdlá
<g/>
,	,	kIx,	,
neatraktivní	atraktivní	k2eNgFnSc1d1	neatraktivní
a	a	k8xC	a
obtloustlá	obtloustlý	k2eAgFnSc1d1	obtloustlá
žena	žena	k1gFnSc1	žena
bez	bez	k7c2	bez
břitkého	břitký	k2eAgInSc2d1	břitký
rozumu	rozum	k1gInSc2	rozum
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
od	od	k7c2	od
ní	on	k3xPp3gFnSc2	on
dostane	dostat	k5eAaPmIp3nS	dostat
nabídku	nabídka	k1gFnSc4	nabídka
k	k	k7c3	k
sňatku	sňatek	k1gInSc3	sňatek
<g/>
,	,	kIx,	,
raději	rád	k6eAd2	rád
utíká	utíkat	k5eAaImIp3nS	utíkat
pryč	pryč	k6eAd1	pryč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
léta	léto	k1gNnPc4	léto
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Long	Long	k1gMnSc1	Long
Years	Years	k1gInSc1	Years
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
duben	duben	k1gInSc1	duben
2026	[number]	k4	2026
<g/>
/	/	kIx~	/
<g/>
2057	[number]	k4	2057
<g/>
.	.	kIx.	.
</s>
<s>
Vědec	vědec	k1gMnSc1	vědec
Hathaway	Hathawaa	k1gFnSc2	Hathawaa
ze	z	k7c2	z
čtvrté	čtvrtá	k1gFnSc2	čtvrtá
expedice	expedice	k1gFnSc2	expedice
stále	stále	k6eAd1	stále
žije	žít	k5eAaImIp3nS	žít
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
manželkou	manželka	k1gFnSc7	manželka
a	a	k8xC	a
dětmi	dítě	k1gFnPc7	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Hathaway	Hathawaa	k1gFnPc4	Hathawaa
je	být	k5eAaImIp3nS	být
schopný	schopný	k2eAgMnSc1d1	schopný
vynálezce	vynálezce	k1gMnSc1	vynálezce
a	a	k8xC	a
pusté	pustý	k2eAgNnSc1d1	pusté
město	město	k1gNnSc1	město
pod	pod	k7c7	pod
jejich	jejich	k3xOp3gInSc7	jejich
domem	dům	k1gInSc7	dům
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
dokázal	dokázat	k5eAaPmAgMnS	dokázat
osvítit	osvítit	k5eAaPmF	osvítit
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
stále	stále	k6eAd1	stále
živé	živý	k2eAgNnSc1d1	živé
<g/>
.	.	kIx.	.
</s>
<s>
Jedné	jeden	k4xCgFnSc6	jeden
noci	noc	k1gFnSc6	noc
vidí	vidět	k5eAaImIp3nS	vidět
raketu	raketa	k1gFnSc4	raketa
blížící	blížící	k2eAgFnSc4d1	blížící
se	se	k3xPyFc4	se
k	k	k7c3	k
Marsu	Mars	k1gInSc2	Mars
a	a	k8xC	a
ohněm	oheň	k1gInSc7	oheň
astronauty	astronaut	k1gMnPc7	astronaut
přiláká	přilákat	k5eAaPmIp3nS	přilákat
ke	k	k7c3	k
svojí	svůj	k3xOyFgFnSc3	svůj
pozici	pozice	k1gFnSc3	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
rakety	raketa	k1gFnSc2	raketa
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gMnSc1	jeho
starý	starý	k2eAgMnSc1d1	starý
velitel	velitel	k1gMnSc1	velitel
<g/>
,	,	kIx,	,
kapitál	kapitál	k1gInSc1	kapitál
Wilder	Wilder	k1gInSc1	Wilder
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
na	na	k7c4	na
Mars	Mars	k1gInSc4	Mars
vrátil	vrátit	k5eAaPmAgMnS	vrátit
po	po	k7c6	po
dvaceti	dvacet	k4xCc6	dvacet
letech	let	k1gInPc6	let
zkoumání	zkoumání	k1gNnSc2	zkoumání
vnější	vnější	k2eAgFnSc2d1	vnější
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Posádka	posádka	k1gFnSc1	posádka
se	se	k3xPyFc4	se
setkává	setkávat	k5eAaImIp3nS	setkávat
se	s	k7c7	s
stařičkým	stařičký	k2eAgMnSc7d1	stařičký
Hathawayem	Hathaway	k1gMnSc7	Hathaway
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
trpí	trpět	k5eAaImIp3nS	trpět
srdeční	srdeční	k2eAgFnSc7d1	srdeční
slabostí	slabost	k1gFnSc7	slabost
<g/>
.	.	kIx.	.
</s>
<s>
Představuje	představovat	k5eAaImIp3nS	představovat
jim	on	k3xPp3gMnPc3	on
svoji	svůj	k3xOyFgFnSc4	svůj
rodinu	rodina	k1gFnSc4	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Wilder	Wilder	k1gMnSc1	Wilder
si	se	k3xPyFc3	se
pamatuje	pamatovat	k5eAaImIp3nS	pamatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
kdysi	kdysi	k6eAd1	kdysi
s	s	k7c7	s
Hathawayovou	Hathawayový	k2eAgFnSc7d1	Hathawayová
ženou	žena	k1gFnSc7	žena
setkal	setkat	k5eAaPmAgInS	setkat
<g/>
,	,	kIx,	,
poznamenává	poznamenávat	k5eAaImIp3nS	poznamenávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vypadá	vypadat	k5eAaPmIp3nS	vypadat
pozoruhodně	pozoruhodně	k6eAd1	pozoruhodně
mladě	mladě	k6eAd1	mladě
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Hathaway	Hathaway	k1gInPc4	Hathaway
značne	značnout	k5eAaImIp3nS	značnout
zestárl	zestárnout	k5eAaPmAgMnS	zestárnout
<g/>
.	.	kIx.	.
</s>
<s>
Wilder	Wilder	k1gMnSc1	Wilder
zbledne	zblednout	k5eAaPmIp3nS	zblednout
<g/>
,	,	kIx,	,
když	když	k8xS	když
Hathawayův	Hathawayův	k2eAgMnSc1d1	Hathawayův
syn	syn	k1gMnSc1	syn
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
mu	on	k3xPp3gMnSc3	on
23	[number]	k4	23
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
už	už	k9	už
čtyřicátník	čtyřicátník	k1gMnSc1	čtyřicátník
<g/>
.	.	kIx.	.
</s>
<s>
Wilder	Wilder	k1gMnSc1	Wilder
pošle	pošle	k6eAd1	pošle
jednoho	jeden	k4xCgMnSc4	jeden
člena	člen	k1gMnSc4	člen
posádky	posádka	k1gFnSc2	posádka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zkontroloval	zkontrolovat	k5eAaPmAgMnS	zkontrolovat
místní	místní	k2eAgInSc4d1	místní
hřbitov	hřbitov	k1gInSc4	hřbitov
<g/>
.	.	kIx.	.
</s>
<s>
Dozvídá	dozvídat	k5eAaImIp3nS	dozvídat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
hřbitově	hřbitov	k1gInSc6	hřbitov
jsou	být	k5eAaImIp3nP	být
hroby	hrob	k1gInPc4	hrob
všech	všecek	k3xTgMnPc2	všecek
členů	člen	k1gMnPc2	člen
rodiny	rodina	k1gFnSc2	rodina
kromě	kromě	k7c2	kromě
Hathawaye	Hathaway	k1gFnSc2	Hathaway
<g/>
.	.	kIx.	.
</s>
<s>
Hathaway	Hathawaa	k1gFnPc4	Hathawaa
dostává	dostávat	k5eAaImIp3nS	dostávat
infarkt	infarkt	k1gInSc4	infarkt
a	a	k8xC	a
prosí	prosit	k5eAaImIp3nP	prosit
Wildera	Wilder	k1gMnSc4	Wilder
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nevolal	volat	k5eNaImAgMnS	volat
jeho	jeho	k3xOp3gFnSc4	jeho
rodinu	rodina	k1gFnSc4	rodina
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
by	by	kYmCp3nP	by
tomu	ten	k3xDgMnSc3	ten
nerozuměli	rozumět	k5eNaImAgMnP	rozumět
<g/>
.	.	kIx.	.
</s>
<s>
Hathawayova	Hathawayův	k2eAgFnSc1d1	Hathawayův
manželka	manželka	k1gFnSc1	manželka
a	a	k8xC	a
děti	dítě	k1gFnPc1	dítě
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
androidi	android	k1gMnPc1	android
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
si	se	k3xPyFc3	se
před	před	k7c7	před
lety	léto	k1gNnPc7	léto
Hathaway	Hathawaa	k1gFnSc2	Hathawaa
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
členů	člen	k1gInPc2	člen
posádky	posádka	k1gFnSc2	posádka
se	se	k3xPyFc4	se
před	před	k7c7	před
odletech	odlet	k1gInPc6	odlet
vrací	vracet	k5eAaImIp3nS	vracet
do	do	k7c2	do
domu	dům	k1gInSc2	dům
s	s	k7c7	s
pistolí	pistol	k1gFnSc7	pistol
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
androidy	android	k1gInPc4	android
zničit	zničit	k5eAaPmF	zničit
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc4	ten
nejsou	být	k5eNaImIp3nP	být
lidé	člověk	k1gMnPc1	člověk
a	a	k8xC	a
nyní	nyní	k6eAd1	nyní
už	už	k9	už
jejich	jejich	k3xOp3gFnSc1	jejich
existence	existence	k1gFnSc1	existence
nemá	mít	k5eNaImIp3nS	mít
smysl	smysl	k1gInSc4	smysl
<g/>
.	.	kIx.	.
</s>
<s>
Raketa	raketa	k1gFnSc1	raketa
odlétá	odlétat	k5eAaImIp3nS	odlétat
a	a	k8xC	a
rodina	rodina	k1gFnSc1	rodina
androidů	android	k1gInPc2	android
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
rutině	rutina	k1gFnSc6	rutina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přijdou	přijít	k5eAaPmIp3nP	přijít
vlahé	vlahý	k2eAgInPc1d1	vlahý
deště	dešť	k1gInPc1	dešť
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
There	Ther	k1gInSc5	Ther
Will	Will	k1gInSc1	Will
Come	Com	k1gInPc1	Com
Soft	Soft	k?	Soft
Rains	Rains	k1gInSc1	Rains
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
srpen	srpen	k1gInSc1	srpen
2026	[number]	k4	2026
<g/>
/	/	kIx~	/
<g/>
2057	[number]	k4	2057
<g/>
.	.	kIx.	.
</s>
<s>
Povídka	povídka	k1gFnSc1	povídka
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
kalifornské	kalifornský	k2eAgFnSc6d1	kalifornská
domácnosti	domácnost	k1gFnSc6	domácnost
po	po	k7c6	po
nukleární	nukleární	k2eAgFnSc6d1	nukleární
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vyhladila	vyhladit	k5eAaPmAgFnS	vyhladit
místní	místní	k2eAgFnSc4d1	místní
populaci	populace	k1gFnSc4	populace
<g/>
.	.	kIx.	.
</s>
<s>
Rodina	rodina	k1gFnSc1	rodina
je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
mrtvá	mrtvý	k2eAgFnSc1d1	mrtvá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zautomatizovaný	zautomatizovaný	k2eAgInSc1d1	zautomatizovaný
dům	dům	k1gInSc1	dům
stále	stále	k6eAd1	stále
provádí	provádět	k5eAaImIp3nS	provádět
naprogramované	naprogramovaný	k2eAgInPc4d1	naprogramovaný
úkoly	úkol	k1gInPc4	úkol
<g/>
.	.	kIx.	.
</s>
<s>
Automaticky	automaticky	k6eAd1	automaticky
je	být	k5eAaImIp3nS	být
připravovaná	připravovaný	k2eAgFnSc1d1	připravovaná
snídaně	snídaně	k1gFnSc1	snídaně
i	i	k9	i
oběd	oběd	k1gInSc4	oběd
<g/>
,	,	kIx,	,
upozornění	upozornění	k1gNnSc4	upozornění
na	na	k7c4	na
denní	denní	k2eAgFnPc4d1	denní
aktivity	aktivita	k1gFnPc4	aktivita
stále	stále	k6eAd1	stále
funguje	fungovat	k5eAaImIp3nS	fungovat
<g/>
,	,	kIx,	,
robotické	robotický	k2eAgFnPc1d1	robotická
myši	myš	k1gFnPc1	myš
udržují	udržovat	k5eAaImIp3nP	udržovat
dům	dům	k1gInSc4	dům
v	v	k7c6	v
čistotě	čistota	k1gFnSc6	čistota
<g/>
.	.	kIx.	.
</s>
<s>
Venku	venku	k6eAd1	venku
jsou	být	k5eAaImIp3nP	být
do	do	k7c2	do
jedné	jeden	k4xCgFnSc2	jeden
ze	z	k7c2	z
stěn	stěna	k1gFnPc2	stěna
domu	dům	k1gInSc2	dům
vpálené	vpálený	k2eAgFnSc2d1	vpálená
siluety	silueta	k1gFnSc2	silueta
členů	člen	k1gMnPc2	člen
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Té	ten	k3xDgFnSc3	ten
noci	noc	k1gFnSc3	noc
přichází	přicházet	k5eAaImIp3nS	přicházet
bouřka	bouřka	k1gFnSc1	bouřka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nalomí	nalomit	k5eAaBmIp3nS	nalomit
strom	strom	k1gInSc4	strom
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
větví	větev	k1gFnPc2	větev
dopadne	dopadnout	k5eAaPmIp3nS	dopadnout
do	do	k7c2	do
domu	dům	k1gInSc2	dům
a	a	k8xC	a
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
tak	tak	k9	tak
požár	požár	k1gInSc1	požár
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
už	už	k6eAd1	už
si	se	k3xPyFc3	se
dům	dům	k1gInSc4	dům
poradit	poradit	k5eAaPmF	poradit
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
ráno	ráno	k1gNnSc1	ráno
z	z	k7c2	z
domu	dům	k1gInSc2	dům
zůstane	zůstat	k5eAaPmIp3nS	zůstat
jen	jen	k9	jen
jedna	jeden	k4xCgFnSc1	jeden
stěna	stěna	k1gFnSc1	stěna
znovu	znovu	k6eAd1	znovu
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
ohlašující	ohlašující	k2eAgNnSc4d1	ohlašující
datum	datum	k1gNnSc4	datum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výlet	výlet	k1gInSc1	výlet
na	na	k7c4	na
milión	milión	k4xCgInSc4	milión
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Million	Million	k1gInSc1	Million
Year	Year	k1gMnSc1	Year
Picnic	Picnice	k1gFnPc2	Picnice
<g/>
,	,	kIx,	,
říjen	říjen	k1gInSc1	říjen
2026	[number]	k4	2026
<g/>
/	/	kIx~	/
<g/>
2057	[number]	k4	2057
<g/>
.	.	kIx.	.
</s>
<s>
Rodina	rodina	k1gFnSc1	rodina
podniká	podnikat	k5eAaImIp3nS	podnikat
v	v	k7c6	v
raketě	raketa	k1gFnSc6	raketa
výlet	výlet	k1gInSc1	výlet
"	"	kIx"	"
<g/>
na	na	k7c4	na
ryby	ryba	k1gFnPc4	ryba
<g/>
"	"	kIx"	"
na	na	k7c4	na
Mars	Mars	k1gInSc4	Mars
<g/>
.	.	kIx.	.
</s>
<s>
Vybírají	vybírat	k5eAaImIp3nP	vybírat
si	se	k3xPyFc3	se
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
usadili	usadit	k5eAaPmAgMnP	usadit
a	a	k8xC	a
nazývali	nazývat	k5eAaImAgMnP	nazývat
ho	on	k3xPp3gMnSc4	on
domovem	domov	k1gInSc7	domov
<g/>
,	,	kIx,	,
a	a	k8xC	a
zničí	zničit	k5eAaPmIp3nS	zničit
raketu	raketa	k1gFnSc4	raketa
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
už	už	k9	už
na	na	k7c4	na
Zem	zem	k1gFnSc4	zem
nemohli	moct	k5eNaImAgMnP	moct
vrátit	vrátit	k5eAaPmF	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
v	v	k7c6	v
ohni	oheň	k1gInSc6	oheň
ničí	ničit	k5eAaImIp3nP	ničit
daňové	daňový	k2eAgInPc1d1	daňový
a	a	k8xC	a
vládní	vládní	k2eAgInPc1d1	vládní
dokumenty	dokument	k1gInPc1	dokument
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgFnSc7d1	poslední
věcí	věc	k1gFnSc7	věc
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
shoří	shořet	k5eAaPmIp3nS	shořet
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
mapa	mapa	k1gFnSc1	mapa
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
nabízí	nabízet	k5eAaImIp3nS	nabízet
svým	svůj	k3xOyFgMnPc3	svůj
synům	syn	k1gMnPc3	syn
dar	dar	k1gInSc4	dar
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
nového	nový	k2eAgInSc2d1	nový
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Představuje	představovat	k5eAaImIp3nS	představovat
jim	on	k3xPp3gMnPc3	on
Marťany	Marťan	k1gMnPc4	Marťan
-	-	kIx~	-
jejich	jejich	k3xOp3gInSc1	jejich
vlastní	vlastní	k2eAgInSc1d1	vlastní
odraz	odraz	k1gInSc1	odraz
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Filmové	filmový	k2eAgFnSc2d1	filmová
adaptace	adaptace	k1gFnSc2	adaptace
==	==	k?	==
</s>
</p>
<p>
<s>
The	The	k?	The
Martian	Martian	k1gMnSc1	Martian
Chronicles	Chronicles	k1gMnSc1	Chronicles
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
,	,	kIx,	,
Marťanská	marťanský	k2eAgFnSc1d1	Marťanská
kronika	kronika	k1gFnSc1	kronika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
třídílný	třídílný	k2eAgInSc1d1	třídílný
americký	americký	k2eAgInSc1d1	americký
televizní	televizní	k2eAgInSc1d1	televizní
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Michael	Michael	k1gMnSc1	Michael
Anderson	Anderson	k1gMnSc1	Anderson
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Česká	český	k2eAgNnPc1d1	české
vydání	vydání	k1gNnPc1	vydání
==	==	k?	==
</s>
</p>
<p>
<s>
Marťanská	marťanský	k2eAgFnSc1d1	Marťanská
kronika	kronika	k1gFnSc1	kronika
<g/>
,	,	kIx,	,
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Jarmila	Jarmila	k1gFnSc1	Jarmila
Emmerová	Emmerová	k1gFnSc1	Emmerová
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
</s>
<s>
Magnet-Press	Magnet-Press	k1gInSc1	Magnet-Press
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1991	[number]	k4	1991
a	a	k8xC	a
Baronet	baronet	k1gMnSc1	baronet
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Marťanská	marťanský	k2eAgFnSc1d1	Marťanská
kronika	kronika	k1gFnSc1	kronika
<g/>
,	,	kIx,	,
451	[number]	k4	451
stupňů	stupeň	k1gInPc2	stupeň
Fahrenheita	Fahrenheit	k1gMnSc2	Fahrenheit
<g/>
,	,	kIx,	,
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
přeložili	přeložit	k5eAaPmAgMnP	přeložit
Jarmila	Jarmila	k1gFnSc1	Jarmila
Emmerová	Emmerová	k1gFnSc1	Emmerová
a	a	k8xC	a
Josef	Josef	k1gMnSc1	Josef
Škvorecký	Škvorecký	k2eAgMnSc1d1	Škvorecký
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Marťanská	marťanský	k2eAgFnSc1d1	Marťanská
kronika	kronika	k1gFnSc1	kronika
<g/>
,	,	kIx,	,
Plus	plus	k1gInSc1	plus
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
přeložili	přeložit	k5eAaPmAgMnP	přeložit
Jarmila	Jarmila	k1gFnSc1	Jarmila
Emmerová	Emmerová	k1gFnSc1	Emmerová
a	a	k8xC	a
Josef	Josef	k1gMnSc1	Josef
Škvorecký	Škvorecký	k2eAgMnSc1d1	Škvorecký
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
100	[number]	k4	100
nejdůležitějších	důležitý	k2eAgFnPc2d3	nejdůležitější
knih	kniha	k1gFnPc2	kniha
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
podle	podle	k7c2	podle
Le	Le	k1gFnSc2	Le
Monde	mond	k1gInSc5	mond
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Martian	Martiana	k1gFnPc2	Martiana
Chronicles	Chroniclesa	k1gFnPc2	Chroniclesa
at	at	k?	at
Fantastic	Fantastice	k1gFnPc2	Fantastice
Fiction	Fiction	k1gInSc4	Fiction
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Table	tablo	k1gNnSc6	tablo
of	of	k?	of
contents	contents	k1gInSc1	contents
with	with	k1gInSc1	with
publication	publication	k1gInSc1	publication
details	detailsa	k1gFnPc2	detailsa
for	forum	k1gNnPc2	forum
the	the	k?	the
various	various	k1gMnSc1	various
stories	stories	k1gMnSc1	stories
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Marťanská	marťanský	k2eAgFnSc1d1	Marťanská
kronika	kronika	k1gFnSc1	kronika
na	na	k7c6	na
webu	web	k1gInSc6	web
LEGIE	legie	k1gFnSc2	legie
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Marťanská	marťanský	k2eAgFnSc1d1	Marťanská
kronika	kronika	k1gFnSc1	kronika
na	na	k7c6	na
webu	web	k1gInSc6	web
Databazeknih	Databazekniha	k1gFnPc2	Databazekniha
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
