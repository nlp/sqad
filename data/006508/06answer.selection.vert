<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
duryňském	duryňský	k2eAgInSc6d1	duryňský
Eisenachu	Eisenach	k1gInSc6	Eisenach
v	v	k7c6	v
Sasko-eisenašském	Saskoisenašský	k2eAgNnSc6d1	Sasko-eisenašský
vévodství	vévodství	k1gNnSc6	vévodství
ve	v	k7c6	v
středovýchodním	středovýchodní	k2eAgNnSc6d1	středovýchodní
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
čtvrtý	čtvrtý	k4xOgMnSc1	čtvrtý
syn	syn	k1gMnSc1	syn
dvorního	dvorní	k2eAgMnSc2d1	dvorní
městského	městský	k2eAgMnSc2d1	městský
hudebníka	hudebník	k1gMnSc2	hudebník
Johanna	Johann	k1gMnSc4	Johann
Ambrozia	Ambrozius	k1gMnSc4	Ambrozius
Bacha	Bacha	k?	Bacha
<g/>
.	.	kIx.	.
</s>
