<s>
Hermitovský	Hermitovský	k2eAgInSc4d1
operátor	operátor	k1gInSc4
</s>
<s>
Hermitovský	Hermitovský	k2eAgInSc1d1
operátor	operátor	k1gInSc1
<g/>
,	,	kIx,
též	též	k9
samoadjungovaný	samoadjungovaný	k2eAgInSc1d1
operátor	operátor	k1gInSc1
nebo	nebo	k8xC
samosdružený	samosdružený	k2eAgInSc1d1
operátor	operátor	k1gInSc1
je	být	k5eAaImIp3nS
v	v	k7c6
matematice	matematika	k1gFnSc6
označení	označení	k1gNnSc1
pro	pro	k7c4
takový	takový	k3xDgInSc4
omezený	omezený	k2eAgInSc4d1
operátor	operátor	k1gInSc4
na	na	k7c6
Hilbertově	Hilbertův	k2eAgInSc6d1
prostoru	prostor	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yRgInSc1
je	být	k5eAaImIp3nS
roven	roven	k2eAgInSc1d1
své	svůj	k3xOyFgFnSc3
adjunkci	adjunkce	k1gFnSc3
<g/>
,	,	kIx,
tzn.	tzn.	kA
takový	takový	k3xDgInSc1
operátor	operátor	k1gInSc1
</s>
<s>
T	T	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
T	T	kA
<g/>
}	}	kIx)
</s>
<s>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
splňuje	splňovat	k5eAaImIp3nS
</s>
<s>
⟨	⟨	k?
</s>
<s>
T	T	kA
</s>
<s>
x	x	k?
</s>
<s>
,	,	kIx,
</s>
<s>
y	y	k?
</s>
<s>
⟩	⟩	k?
</s>
<s>
=	=	kIx~
</s>
<s>
⟨	⟨	k?
</s>
<s>
x	x	k?
</s>
<s>
,	,	kIx,
</s>
<s>
T	T	kA
</s>
<s>
y	y	k?
</s>
<s>
⟩	⟩	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
langle	langl	k1gMnSc5
Tx	Tx	k1gMnSc5
<g/>
,	,	kIx,
<g/>
y	y	k?
<g/>
\	\	kIx~
<g/>
rangle	rangle	k1gFnSc1
=	=	kIx~
<g/>
\	\	kIx~
<g/>
langle	langle	k1gFnSc1
x	x	k?
<g/>
,	,	kIx,
<g/>
Ty	ty	k3xPp2nSc1
<g/>
\	\	kIx~
<g/>
rangle	rangle	k6eAd1
}	}	kIx)
</s>
<s>
pro	pro	k7c4
všechna	všechen	k3xTgNnPc4
</s>
<s>
x	x	k?
</s>
<s>
,	,	kIx,
</s>
<s>
y	y	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
x	x	k?
<g/>
,	,	kIx,
<g/>
y	y	k?
<g/>
}	}	kIx)
</s>
<s>
pro	pro	k7c4
která	který	k3yIgNnPc4,k3yQgNnPc4,k3yRgNnPc4
je	být	k5eAaImIp3nS
definován	definován	k2eAgInSc1d1
<g/>
,	,	kIx,
kde	kde	k6eAd1
</s>
<s>
⟨	⟨	k?
</s>
<s>
⋅	⋅	k?
</s>
<s>
,	,	kIx,
</s>
<s>
⋅	⋅	k?
</s>
<s>
⟩	⟩	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
langle	langle	k1gNnPc3
\	\	kIx~
<g/>
cdot	cdot	k5eAaPmF
,	,	kIx,
<g/>
\	\	kIx~
<g/>
cdot	cdot	k1gInSc1
\	\	kIx~
<g/>
rangle	rangle	k1gInSc1
}	}	kIx)
</s>
<s>
značí	značit	k5eAaImIp3nS
skalární	skalární	k2eAgInSc1d1
součin	součin	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Hermitovský	Hermitovský	k2eAgInSc1d1
operátor	operátor	k1gInSc1
bývá	bývat	k5eAaImIp3nS
na	na	k7c4
prostoru	prostora	k1gFnSc4
operátorů	operátor	k1gMnPc2
považován	považován	k2eAgMnSc1d1
za	za	k7c4
jakési	jakýsi	k3yIgNnSc4
zobecnění	zobecnění	k1gNnSc4
reálného	reálný	k2eAgNnSc2d1
čísla	číslo	k1gNnSc2
<g/>
,	,	kIx,
platí	platit	k5eAaImIp3nS
následující	následující	k2eAgFnPc4d1
vlastnosti	vlastnost	k1gFnPc4
<g/>
:	:	kIx,
</s>
<s>
T	T	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
T	T	kA
<g/>
}	}	kIx)
</s>
<s>
je	být	k5eAaImIp3nS
hermitovský	hermitovský	k2eAgInSc1d1
právě	právě	k9
když	když	k8xS
<g/>
:	:	kIx,
</s>
<s>
⟨	⟨	k?
</s>
<s>
T	T	kA
</s>
<s>
x	x	k?
</s>
<s>
,	,	kIx,
</s>
<s>
x	x	k?
</s>
<s>
⟩	⟩	k?
</s>
<s>
∈	∈	k?
</s>
<s>
R	R	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
langle	langl	k1gMnSc5
Tx	Tx	k1gMnSc5
<g/>
,	,	kIx,
<g/>
x	x	k?
<g/>
\	\	kIx~
<g/>
rangle	rangle	k1gFnSc1
\	\	kIx~
<g/>
in	in	k?
\	\	kIx~
<g/>
mathbb	mathbb	k1gMnSc1
{	{	kIx(
<g/>
R	R	kA
<g/>
}	}	kIx)
}	}	kIx)
</s>
<s>
Vlastní	vlastní	k2eAgNnPc1d1
čísla	číslo	k1gNnPc1
hermitovského	hermitovský	k2eAgInSc2d1
operátoru	operátor	k1gInSc2
jsou	být	k5eAaImIp3nP
reálná	reálný	k2eAgNnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
prostoru	prostor	k1gInSc6
konečné	konečný	k2eAgFnSc2d1
dimenze	dimenze	k1gFnSc2
je	být	k5eAaImIp3nS
reprezentován	reprezentovat	k5eAaImNgMnS
hermitovskou	hermitovský	k2eAgFnSc7d1
maticí	matice	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Hermitovský	Hermitovský	k2eAgInSc1d1
operátor	operátor	k1gInSc1
komutuje	komutovat	k5eAaBmIp3nS
se	s	k7c7
svou	svůj	k3xOyFgFnSc7
adjunkcí	adjunkce	k1gFnSc7
(	(	kIx(
<g/>
tzn.	tzn.	kA
dle	dle	k7c2
definice	definice	k1gFnSc2
sám	sám	k3xTgMnSc1
se	s	k7c7
sebou	se	k3xPyFc7
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
zřejmé	zřejmý	k2eAgNnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
tedy	tedy	k9
takzvaně	takzvaně	k6eAd1
normální	normální	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
toho	ten	k3xDgInSc2
podle	podle	k7c2
věty	věta	k1gFnSc2
o	o	k7c6
spektrálním	spektrální	k2eAgInSc6d1
rozkladu	rozklad	k1gInSc6
plyne	plynout	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
jeho	jeho	k3xOp3gInPc1
vlastní	vlastní	k2eAgInPc1d1
vektory	vektor	k1gInPc1
jsou	být	k5eAaImIp3nP
ortogonální	ortogonální	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s>
Využití	využití	k1gNnSc1
</s>
<s>
Hermitovské	Hermitovský	k2eAgInPc1d1
operátory	operátor	k1gInPc1
mají	mít	k5eAaImIp3nP
velké	velký	k2eAgNnSc4d1
uplatnění	uplatnění	k1gNnSc4
v	v	k7c6
kvantové	kvantový	k2eAgFnSc6d1
fyzice	fyzika	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
jimi	on	k3xPp3gFnPc7
reprezentují	reprezentovat	k5eAaImIp3nP
pozorovatelné	pozorovatelný	k2eAgFnPc4d1
veličiny	veličina	k1gFnPc4
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gNnPc1
vlastní	vlastní	k2eAgNnPc1d1
čísla	číslo	k1gNnPc1
odpovídají	odpovídat	k5eAaImIp3nP
možným	možný	k2eAgFnPc3d1
hodnotám	hodnota	k1gFnPc3
měření	měření	k1gNnSc2
a	a	k8xC
proto	proto	k8xC
je	být	k5eAaImIp3nS
přirozený	přirozený	k2eAgInSc4d1
požadavek	požadavek	k1gInSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
byla	být	k5eAaImAgFnS
reálná	reálný	k2eAgFnSc1d1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
splňují	splňovat	k5eAaImIp3nP
právě	právě	k6eAd1
hermitovské	hermitovský	k2eAgInPc1d1
operátory	operátor	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Operátor	operátor	k1gInSc1
<g/>
#	#	kIx~
<g/>
Symetrický	symetrický	k2eAgInSc4d1
<g/>
,	,	kIx,
hermiteovský	hermiteovský	k2eAgInSc4d1
a	a	k8xC
sdružený	sdružený	k2eAgInSc4d1
operátor	operátor	k1gInSc4
</s>
<s>
Pozitivní	pozitivní	k2eAgInSc1d1
operátor	operátor	k1gInSc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Matematika	matematika	k1gFnSc1
</s>
