<s>
Hermitovský	Hermitovský	k2eAgInSc1d1
operátor	operátor	k1gInSc1
<g/>
,	,	kIx,
též	též	k9
samoadjungovaný	samoadjungovaný	k2eAgInSc1d1
operátor	operátor	k1gInSc1
nebo	nebo	k8xC
samosdružený	samosdružený	k2eAgInSc1d1
operátor	operátor	k1gInSc1
je	být	k5eAaImIp3nS
v	v	k7c6
matematice	matematika	k1gFnSc6
označení	označení	k1gNnSc1
pro	pro	k7c4
takový	takový	k3xDgInSc4
omezený	omezený	k2eAgInSc4d1
operátor	operátor	k1gInSc4
na	na	k7c6
Hilbertově	Hilbertův	k2eAgInSc6d1
prostoru	prostor	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yRgInSc1
je	být	k5eAaImIp3nS
roven	roven	k2eAgInSc1d1
své	svůj	k3xOyFgFnSc3
adjunkci	adjunkce	k1gFnSc3
<g/>
,	,	kIx,
tzn.	tzn.	kA
takový	takový	k3xDgInSc1
operátor	operátor	k1gInSc1
</s>