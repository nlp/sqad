<s>
Zlato	zlato	k1gNnSc1	zlato
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
Aurum	Aurum	k1gNnSc1	Aurum
<g/>
,	,	kIx,	,
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
prvku	prvek	k1gInSc2	prvek
Au	au	k0	au
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
chemicky	chemicky	k6eAd1	chemicky
odolný	odolný	k2eAgInSc1d1	odolný
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
tepelně	tepelně	k6eAd1	tepelně
i	i	k8xC	i
elektricky	elektricky	k6eAd1	elektricky
vodivý	vodivý	k2eAgMnSc1d1	vodivý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
poměrně	poměrně	k6eAd1	poměrně
měkký	měkký	k2eAgInSc1d1	měkký
drahý	drahý	k2eAgInSc1d1	drahý
kov	kov	k1gInSc1	kov
žluté	žlutý	k2eAgFnSc2d1	žlutá
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
