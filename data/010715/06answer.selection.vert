<s>
Největším	veliký	k2eAgInSc7d3	veliký
měsícem	měsíc	k1gInSc7	měsíc
Saturnu	Saturn	k1gInSc2	Saturn
je	být	k5eAaImIp3nS	být
Titan	titan	k1gInSc1	titan
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
5	[number]	k4	5
150	[number]	k4	150
km	km	kA	km
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
<g/>
.	.	kIx.	.
</s>
