<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
asi	asi	k9	asi
50	[number]	k4	50
<g/>
–	–	k?	–
<g/>
100	[number]	k4	100
miliard	miliarda	k4xCgFnPc2	miliarda
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
asi	asi	k9	asi
1011	[number]	k4	1011
<g/>
)	)	kIx)	)
neuronů	neuron	k1gInPc2	neuron
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
asi	asi	k9	asi
10	[number]	k4	10
%	%	kIx~	%
(	(	kIx(	(
<g/>
1010	[number]	k4	1010
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
pyramidální	pyramidální	k2eAgFnPc1d1	pyramidální
buňky	buňka	k1gFnPc1	buňka
v	v	k7c6	v
mozkové	mozkový	k2eAgFnSc6d1	mozková
kůře	kůra	k1gFnSc6	kůra
<g/>
.	.	kIx.	.
</s>
