<s>
Lidský	lidský	k2eAgInSc1d1	lidský
mozek	mozek	k1gInSc1	mozek
(	(	kIx(	(
<g/>
encephalon	encephalon	k1gInSc1	encephalon
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
řídící	řídící	k2eAgMnSc1d1	řídící
a	a	k8xC	a
integrační	integrační	k2eAgInSc1d1	integrační
orgán	orgán	k1gInSc1	orgán
nervové	nervový	k2eAgFnSc2d1	nervová
soustavy	soustava	k1gFnSc2	soustava
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Řídí	řídit	k5eAaImIp3nS	řídit
a	a	k8xC	a
kontroluje	kontrolovat	k5eAaImIp3nS	kontrolovat
veškeré	veškerý	k3xTgFnPc4	veškerý
tělesné	tělesný	k2eAgFnPc4d1	tělesná
funkce	funkce	k1gFnPc4	funkce
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
činnost	činnost	k1gFnSc4	činnost
srdce	srdce	k1gNnSc2	srdce
<g/>
,	,	kIx,	,
trávení	trávení	k1gNnSc2	trávení
<g/>
,	,	kIx,	,
pohyb	pohyb	k1gInSc1	pohyb
<g/>
,	,	kIx,	,
řeč	řeč	k1gFnSc1	řeč
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
samotné	samotný	k2eAgNnSc1d1	samotné
myšlení	myšlení	k1gNnSc1	myšlení
<g/>
,	,	kIx,	,
paměť	paměť	k1gFnSc1	paměť
či	či	k8xC	či
vnímání	vnímání	k1gNnSc1	vnímání
emocí	emoce	k1gFnPc2	emoce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lidský	lidský	k2eAgInSc1d1	lidský
mozek	mozek	k1gInSc1	mozek
má	mít	k5eAaImIp3nS	mít
objem	objem	k1gInSc4	objem
asi	asi	k9	asi
1450	[number]	k4	1450
cm	cm	kA	cm
<g/>
3	[number]	k4	3
a	a	k8xC	a
váží	vážit	k5eAaImIp3nS	vážit
přibližně	přibližně	k6eAd1	přibližně
1300	[number]	k4	1300
<g/>
–	–	k?	–
<g/>
1400	[number]	k4	1400
g	g	kA	g
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
2	[number]	k4	2
%	%	kIx~	%
lidské	lidský	k2eAgFnSc2d1	lidská
váhy	váha	k1gFnSc2	váha
<g/>
.	.	kIx.	.
</s>
<s>
Spotřebuje	spotřebovat	k5eAaPmIp3nS	spotřebovat
asi	asi	k9	asi
pětinu	pětina	k1gFnSc4	pětina
veškeré	veškerý	k3xTgFnSc2	veškerý
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
tělo	tělo	k1gNnSc1	tělo
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
asi	asi	k9	asi
50	[number]	k4	50
<g/>
–	–	k?	–
<g/>
100	[number]	k4	100
miliard	miliarda	k4xCgFnPc2	miliarda
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
asi	asi	k9	asi
1011	[number]	k4	1011
<g/>
)	)	kIx)	)
neuronů	neuron	k1gInPc2	neuron
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
asi	asi	k9	asi
10	[number]	k4	10
%	%	kIx~	%
(	(	kIx(	(
<g/>
1010	[number]	k4	1010
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
pyramidální	pyramidální	k2eAgFnPc1d1	pyramidální
buňky	buňka	k1gFnPc1	buňka
v	v	k7c6	v
mozkové	mozkový	k2eAgFnSc6d1	mozková
kůře	kůra	k1gFnSc6	kůra
<g/>
.	.	kIx.	.
</s>

