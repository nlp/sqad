<p>
<s>
Lidský	lidský	k2eAgInSc1d1	lidský
mozek	mozek	k1gInSc1	mozek
(	(	kIx(	(
<g/>
encephalon	encephalon	k1gInSc1	encephalon
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
řídící	řídící	k2eAgMnSc1d1	řídící
a	a	k8xC	a
integrační	integrační	k2eAgInSc1d1	integrační
orgán	orgán	k1gInSc1	orgán
nervové	nervový	k2eAgFnSc2d1	nervová
soustavy	soustava	k1gFnSc2	soustava
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Řídí	řídit	k5eAaImIp3nS	řídit
a	a	k8xC	a
kontroluje	kontrolovat	k5eAaImIp3nS	kontrolovat
veškeré	veškerý	k3xTgFnPc4	veškerý
tělesné	tělesný	k2eAgFnPc4d1	tělesná
funkce	funkce	k1gFnPc4	funkce
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
činnost	činnost	k1gFnSc4	činnost
srdce	srdce	k1gNnSc2	srdce
<g/>
,	,	kIx,	,
trávení	trávení	k1gNnSc2	trávení
<g/>
,	,	kIx,	,
pohyb	pohyb	k1gInSc1	pohyb
<g/>
,	,	kIx,	,
řeč	řeč	k1gFnSc1	řeč
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
samotné	samotný	k2eAgNnSc1d1	samotné
myšlení	myšlení	k1gNnSc1	myšlení
<g/>
,	,	kIx,	,
paměť	paměť	k1gFnSc1	paměť
či	či	k8xC	či
vnímání	vnímání	k1gNnSc1	vnímání
emocí	emoce	k1gFnPc2	emoce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lidský	lidský	k2eAgInSc1d1	lidský
mozek	mozek	k1gInSc1	mozek
má	mít	k5eAaImIp3nS	mít
objem	objem	k1gInSc4	objem
asi	asi	k9	asi
1450	[number]	k4	1450
cm	cm	kA	cm
<g/>
3	[number]	k4	3
a	a	k8xC	a
váží	vážit	k5eAaImIp3nS	vážit
přibližně	přibližně	k6eAd1	přibližně
1300	[number]	k4	1300
<g/>
–	–	k?	–
<g/>
1400	[number]	k4	1400
g	g	kA	g
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
2	[number]	k4	2
%	%	kIx~	%
lidské	lidský	k2eAgFnSc2d1	lidská
váhy	váha	k1gFnSc2	váha
<g/>
.	.	kIx.	.
</s>
<s>
Spotřebuje	spotřebovat	k5eAaPmIp3nS	spotřebovat
asi	asi	k9	asi
pětinu	pětina	k1gFnSc4	pětina
veškeré	veškerý	k3xTgFnSc2	veškerý
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
tělo	tělo	k1gNnSc1	tělo
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
asi	asi	k9	asi
50	[number]	k4	50
<g/>
–	–	k?	–
<g/>
100	[number]	k4	100
miliard	miliarda	k4xCgFnPc2	miliarda
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
asi	asi	k9	asi
1011	[number]	k4	1011
<g/>
)	)	kIx)	)
neuronů	neuron	k1gInPc2	neuron
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
asi	asi	k9	asi
10	[number]	k4	10
%	%	kIx~	%
(	(	kIx(	(
<g/>
1010	[number]	k4	1010
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
pyramidální	pyramidální	k2eAgFnPc1d1	pyramidální
buňky	buňka	k1gFnPc1	buňka
v	v	k7c6	v
mozkové	mozkový	k2eAgFnSc6d1	mozková
kůře	kůra	k1gFnSc6	kůra
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
nervovými	nervový	k2eAgFnPc7d1	nervová
buňkami	buňka	k1gFnPc7	buňka
existuje	existovat	k5eAaImIp3nS	existovat
až	až	k6eAd1	až
biliarda	biliarda	k4xCgFnSc1	biliarda
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
1015	[number]	k4	1015
<g/>
)	)	kIx)	)
synaptických	synaptický	k2eAgNnPc2d1	synaptické
spojení	spojení	k1gNnPc2	spojení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Makroskopická	makroskopický	k2eAgFnSc1d1	makroskopická
anatomie	anatomie	k1gFnSc1	anatomie
==	==	k?	==
</s>
</p>
<p>
<s>
Mozek	mozek	k1gInSc1	mozek
lze	lze	k6eAd1	lze
zjednodušeně	zjednodušeně	k6eAd1	zjednodušeně
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
mozkový	mozkový	k2eAgInSc4d1	mozkový
kmen	kmen	k1gInSc4	kmen
<g/>
,	,	kIx,	,
mezimozek	mezimozek	k1gInSc1	mezimozek
(	(	kIx(	(
<g/>
diencephalon	diencephalon	k1gInSc1	diencephalon
<g/>
)	)	kIx)	)
a	a	k8xC	a
koncový	koncový	k2eAgInSc4d1	koncový
mozek	mozek	k1gInSc4	mozek
(	(	kIx(	(
<g/>
telencephalon	telencephalon	k1gInSc4	telencephalon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Anatomicky	anatomicky	k6eAd1	anatomicky
se	se	k3xPyFc4	se
však	však	k9	však
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
tyto	tento	k3xDgFnPc4	tento
části	část	k1gFnPc4	část
<g/>
:	:	kIx,	:
prodloužená	prodloužená	k1gFnSc1	prodloužená
mícha	mícha	k1gFnSc1	mícha
(	(	kIx(	(
<g/>
medulla	medulla	k1gFnSc1	medulla
oblongata	oblongata	k1gFnSc1	oblongata
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Varolův	Varolův	k2eAgInSc1d1	Varolův
most	most	k1gInSc1	most
(	(	kIx(	(
<g/>
Pons	Pons	k1gInSc1	Pons
Varoli	Varole	k1gFnSc4	Varole
<g/>
,	,	kIx,	,
běžně	běžně	k6eAd1	běžně
Pons	Pons	k1gInSc1	Pons
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Střední	střední	k2eAgInSc1d1	střední
mozek	mozek	k1gInSc1	mozek
(	(	kIx(	(
<g/>
mesencephalon	mesencephalon	k1gInSc1	mesencephalon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mozeček	mozeček	k1gInSc1	mozeček
(	(	kIx(	(
<g/>
Cerebellum	Cerebellum	k1gInSc1	Cerebellum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mezimozek	mezimozek	k1gInSc1	mezimozek
(	(	kIx(	(
<g/>
diencephalon	diencephalon	k1gInSc1	diencephalon
<g/>
)	)	kIx)	)
a	a	k8xC	a
koncový	koncový	k2eAgInSc4d1	koncový
mozek	mozek	k1gInSc4	mozek
(	(	kIx(	(
<g/>
telencephalon	telencephalon	k1gInSc4	telencephalon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Makroskopicky	makroskopicky	k6eAd1	makroskopicky
se	se	k3xPyFc4	se
na	na	k7c6	na
mozku	mozek	k1gInSc6	mozek
popisují	popisovat	k5eAaImIp3nP	popisovat
mnohé	mnohý	k2eAgInPc1d1	mnohý
útvary	útvar	k1gInPc1	útvar
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
často	často	k6eAd1	často
překrývají	překrývat	k5eAaImIp3nP	překrývat
s	s	k7c7	s
funkčními	funkční	k2eAgFnPc7d1	funkční
strukturami	struktura	k1gFnPc7	struktura
téhož	týž	k3xTgNnSc2	týž
jména	jméno	k1gNnSc2	jméno
nebo	nebo	k8xC	nebo
v	v	k7c6	v
sobě	se	k3xPyFc3	se
takové	takový	k3xDgFnSc2	takový
struktury	struktura	k1gFnSc2	struktura
ukrývají	ukrývat	k5eAaImIp3nP	ukrývat
<g/>
,	,	kIx,	,
struktura	struktura	k1gFnSc1	struktura
funkční	funkční	k2eAgFnSc1d1	funkční
pak	pak	k6eAd1	pak
obvykle	obvykle	k6eAd1	obvykle
nese	nést	k5eAaImIp3nS	nést
jméno	jméno	k1gNnSc4	jméno
podle	podle	k7c2	podle
struktury	struktura	k1gFnSc2	struktura
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
kterou	který	k3yQgFnSc7	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
(	(	kIx(	(
<g/>
např.	např.	kA	např.
nuclei	nuclee	k1gFnSc4	nuclee
mamilarii	mamilarie	k1gFnSc4	mamilarie
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
hrbolcích	hrbolec	k1gInPc6	hrbolec
na	na	k7c6	na
<g />
.	.	kIx.	.
</s>
<s>
spodině	spodina	k1gFnSc3	spodina
mozku	mozek	k1gInSc2	mozek
s	s	k7c7	s
názvem	název	k1gInSc7	název
corpora	corpor	k1gMnSc2	corpor
mamillaria	mamillarium	k1gNnSc2	mamillarium
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
případně	případně	k6eAd1	případně
jsou	být	k5eAaImIp3nP	být
struktury	struktura	k1gFnPc1	struktura
pojmenovány	pojmenován	k2eAgFnPc1d1	pojmenována
na	na	k7c6	na
základě	základ	k1gInSc6	základ
svého	svůj	k3xOyFgInSc2	svůj
vzhledu	vzhled	k1gInSc2	vzhled
(	(	kIx(	(
<g/>
tvaru	tvar	k1gInSc2	tvar
či	či	k8xC	či
barvy	barva	k1gFnSc2	barva
<g/>
,	,	kIx,	,
kupř	kupř	kA	kupř
<g/>
.	.	kIx.	.
corpora	corpor	k1gMnSc2	corpor
geniculata	genicule	k1gNnPc1	genicule
–	–	k?	–
"	"	kIx"	"
<g/>
kolínkovitá	kolínkovitý	k2eAgNnPc1d1	kolínkovitý
tělíska	tělísko	k1gNnPc1	tělísko
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
locus	locus	k1gMnSc1	locus
caeruleus	caeruleus	k1gMnSc1	caeruleus
–	–	k?	–
"	"	kIx"	"
<g/>
modrošedé	modrošedý	k2eAgNnSc1d1	modrošedé
místo	místo	k1gNnSc1	místo
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Prodloužená	prodloužený	k2eAgFnSc1d1	prodloužená
mícha	mícha	k1gFnSc1	mícha
===	===	k?	===
</s>
</p>
<p>
<s>
Prodloužená	prodloužený	k2eAgFnSc1d1	prodloužená
mícha	mícha	k1gFnSc1	mícha
neboli	neboli	k8xC	neboli
Medulla	Medulla	k1gFnSc1	Medulla
oblongata	oblongata	k1gFnSc1	oblongata
<g/>
,	,	kIx,	,
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
často	často	k6eAd1	často
pouze	pouze	k6eAd1	pouze
oblongata	oblongat	k2eAgFnSc1d1	oblongat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
přechodem	přechod	k1gInSc7	přechod
mezi	mezi	k7c7	mezi
míchou	mícha	k1gFnSc7	mícha
a	a	k8xC	a
mozkovým	mozkový	k2eAgInSc7d1	mozkový
kmenem	kmen	k1gInSc7	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
hranici	hranice	k1gFnSc4	hranice
mezi	mezi	k7c7	mezi
míchou	mícha	k1gFnSc7	mícha
a	a	k8xC	a
oblongatou	oblongata	k1gFnSc7	oblongata
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
decussatio	decussatio	k6eAd1	decussatio
pyramidorum	pyramidorum	k1gInSc4	pyramidorum
<g/>
,	,	kIx,	,
viditelné	viditelný	k2eAgNnSc4d1	viditelné
zkřížení	zkřížení	k1gNnSc4	zkřížení
vláken	vlákna	k1gFnPc2	vlákna
pyramidové	pyramidový	k2eAgFnSc2d1	pyramidová
dráhy	dráha	k1gFnSc2	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Oblongata	Oblongata	k1gFnSc1	Oblongata
je	být	k5eAaImIp3nS	být
místem	místo	k1gNnSc7	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
dráhy	dráha	k1gFnPc1	dráha
zadních	zadní	k2eAgInPc2d1	zadní
provazců	provazec	k1gInPc2	provazec
míchy	mícha	k1gFnSc2	mícha
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
lemniskální	lemniskální	k2eAgInSc1d1	lemniskální
systém	systém	k1gInSc1	systém
<g/>
)	)	kIx)	)
přepojuje	přepojovat	k5eAaImIp3nS	přepojovat
v	v	k7c4	v
nucleus	nucleus	k1gInSc4	nucleus
cuneatus	cuneatus	k1gMnSc1	cuneatus
a	a	k8xC	a
nucleus	nucleus	k1gMnSc1	nucleus
gracilis	gracilis	k1gFnSc2	gracilis
(	(	kIx(	(
<g/>
obě	dva	k4xCgFnPc1	dva
tato	tento	k3xDgNnPc1	tento
jádra	jádro	k1gNnPc4	jádro
vyzdvihují	vyzdvihovat	k5eAaImIp3nP	vyzdvihovat
stejnojmenné	stejnojmenný	k2eAgInPc1d1	stejnojmenný
hrbolky	hrbolek	k1gInPc1	hrbolek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
její	její	k3xOp3gFnPc1	její
šedé	šedý	k2eAgFnPc1d1	šedá
hmoty	hmota	k1gFnPc1	hmota
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
část	část	k1gFnSc4	část
jader	jádro	k1gNnPc2	jádro
hlavových	hlavový	k2eAgInPc2d1	hlavový
nervů	nerv	k1gInPc2	nerv
a	a	k8xC	a
úsek	úsek	k1gInSc1	úsek
retikulární	retikulární	k2eAgFnSc2d1	retikulární
formace	formace	k1gFnSc2	formace
<g/>
.	.	kIx.	.
</s>
<s>
Nejnápadnějšími	nápadní	k2eAgInPc7d3	nápadní
makroskopickými	makroskopický	k2eAgInPc7d1	makroskopický
útvary	útvar	k1gInPc7	útvar
na	na	k7c6	na
oblongatě	oblongat	k1gInSc6	oblongat
jsou	být	k5eAaImIp3nP	být
tzv.	tzv.	kA	tzv.
pyramidy	pyramid	k1gInPc4	pyramid
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
kterých	který	k3yRgFnPc2	který
dostala	dostat	k5eAaPmAgFnS	dostat
jméno	jméno	k1gNnSc4	jméno
tudy	tudy	k6eAd1	tudy
probíhající	probíhající	k2eAgFnSc1d1	probíhající
pyramidová	pyramidový	k2eAgFnSc1d1	pyramidová
dráha	dráha	k1gFnSc1	dráha
<g/>
)	)	kIx)	)
a	a	k8xC	a
oliva	oliva	k1gFnSc1	oliva
<g/>
,	,	kIx,	,
oválné	oválný	k2eAgNnSc1d1	oválné
vyvýšení	vyvýšení	k1gNnSc1	vyvýšení
po	po	k7c6	po
straně	strana	k1gFnSc6	strana
oblongaty	oblongata	k1gFnSc2	oblongata
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
sobě	sebe	k3xPyFc6	sebe
ukrývá	ukrývat	k5eAaImIp3nS	ukrývat
stejnojmenná	stejnojmenný	k2eAgNnPc4d1	stejnojmenné
jádra	jádro	k1gNnPc4	jádro
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Varolův	Varolův	k2eAgInSc1d1	Varolův
most	most	k1gInSc1	most
===	===	k?	===
</s>
</p>
<p>
<s>
Varolův	Varolův	k2eAgInSc1d1	Varolův
most	most	k1gInSc1	most
neboli	neboli	k8xC	neboli
Pons	Pons	k1gInSc1	Pons
Varoli	Varole	k1gFnSc3	Varole
<g/>
,	,	kIx,	,
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
často	často	k6eAd1	často
pouze	pouze	k6eAd1	pouze
pons	pons	k6eAd1	pons
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
úsek	úsek	k1gInSc1	úsek
mozkového	mozkový	k2eAgInSc2d1	mozkový
kmene	kmen	k1gInSc2	kmen
navazující	navazující	k2eAgFnPc4d1	navazující
na	na	k7c4	na
oblongatu	oblongata	k1gFnSc4	oblongata
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
struktury	struktura	k1gFnPc1	struktura
z	z	k7c2	z
oblongaty	oblongata	k1gFnSc2	oblongata
(	(	kIx(	(
<g/>
zejm.	zejm.	k?	zejm.
retikulární	retikulární	k2eAgFnSc2d1	retikulární
formace	formace	k1gFnSc2	formace
a	a	k8xC	a
některé	některý	k3yIgFnSc2	některý
ascendentní	ascendentní	k2eAgFnSc2d1	ascendentní
dráhy	dráha	k1gFnSc2	dráha
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
i	i	k9	i
v	v	k7c6	v
pontu	pont	k1gInSc6	pont
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
nachází	nacházet	k5eAaImIp3nS	nacházet
další	další	k2eAgNnPc4d1	další
jádra	jádro	k1gNnPc4	jádro
hlavových	hlavový	k2eAgInPc2d1	hlavový
nervů	nerv	k1gInPc2	nerv
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
relé	relé	k1gNnSc1	relé
jádra	jádro	k1gNnSc2	jádro
<g/>
"	"	kIx"	"
sluchové	sluchový	k2eAgFnPc1d1	sluchová
dráhy	dráha	k1gFnPc1	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitější	důležitý	k2eAgFnSc1d3	nejdůležitější
struktura	struktura	k1gFnSc1	struktura
v	v	k7c6	v
pontu	pont	k1gInSc6	pont
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
pontinní	pontinný	k2eAgMnPc1d1	pontinný
jádra	jádro	k1gNnSc2	jádro
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
přivádí	přivádět	k5eAaImIp3nS	přivádět
informace	informace	k1gFnPc4	informace
do	do	k7c2	do
mozečku	mozeček	k1gInSc2	mozeček
<g/>
.	.	kIx.	.
</s>
<s>
Makroskopicky	makroskopicky	k6eAd1	makroskopicky
významným	významný	k2eAgInSc7d1	významný
útvarem	útvar	k1gInSc7	útvar
v	v	k7c6	v
pontu	pont	k1gInSc6	pont
(	(	kIx(	(
<g/>
částečně	částečně	k6eAd1	částečně
i	i	k9	i
v	v	k7c6	v
oblongatě	oblongata	k1gFnSc6	oblongata
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
čtvrtá	čtvrtý	k4xOgFnSc1	čtvrtý
mozková	mozkový	k2eAgFnSc1d1	mozková
komora	komora	k1gFnSc1	komora
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Mozeček	mozeček	k1gInSc4	mozeček
===	===	k?	===
</s>
</p>
<p>
<s>
Mozeček	mozeček	k1gInSc1	mozeček
vývojově	vývojově	k6eAd1	vývojově
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
z	z	k7c2	z
dorsální	dorsální	k2eAgFnSc2d1	dorsální
ploténky	ploténka	k1gFnSc2	ploténka
methencephala	methencephal	k1gMnSc2	methencephal
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
a	a	k8xC	a
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
koordinační	koordinační	k2eAgNnSc4d1	koordinační
centrum	centrum	k1gNnSc4	centrum
motoriky	motorik	k1gMnPc4	motorik
<g/>
.	.	kIx.	.
</s>
<s>
Šedá	šedý	k2eAgFnSc1d1	šedá
hmota	hmota	k1gFnSc1	hmota
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
kůru	kůra	k1gFnSc4	kůra
mozečku	mozeček	k1gInSc2	mozeček
a	a	k8xC	a
na	na	k7c4	na
mozečková	mozečkový	k2eAgNnPc4d1	mozečkový
jádra	jádro	k1gNnPc4	jádro
(	(	kIx(	(
<g/>
ncl	ncl	k?	ncl
<g/>
.	.	kIx.	.
</s>
<s>
Dentatus	Dentatus	k1gMnSc1	Dentatus
<g/>
,	,	kIx,	,
ncl	ncl	k?	ncl
<g/>
.	.	kIx.	.
emboliformis	emboliformis	k1gInSc4	emboliformis
<g/>
,	,	kIx,	,
ncll	ncll	k1gInSc4	ncll
<g/>
.	.	kIx.	.
globosi	globos	k1gMnSc3	globos
a	a	k8xC	a
ncl	ncl	k?	ncl
<g/>
.	.	kIx.	.
fastigii	fastigie	k1gFnSc4	fastigie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bílá	bílý	k2eAgFnSc1d1	bílá
hmota	hmota	k1gFnSc1	hmota
přivádí	přivádět	k5eAaImIp3nS	přivádět
do	do	k7c2	do
mozečkové	mozečkový	k2eAgFnSc2d1	mozečková
kůry	kůra	k1gFnSc2	kůra
informace	informace	k1gFnSc2	informace
jednak	jednak	k8xC	jednak
z	z	k7c2	z
mozkové	mozkový	k2eAgFnSc2d1	mozková
kůry	kůra	k1gFnSc2	kůra
(	(	kIx(	(
<g/>
informace	informace	k1gFnPc1	informace
o	o	k7c6	o
zamýšleném	zamýšlený	k2eAgInSc6d1	zamýšlený
pohybu	pohyb	k1gInSc6	pohyb
a	a	k8xC	a
zraková	zrakový	k2eAgFnSc1d1	zraková
či	či	k8xC	či
sluchová	sluchový	k2eAgFnSc1d1	sluchová
korekce	korekce	k1gFnSc1	korekce
z	z	k7c2	z
příslušných	příslušný	k2eAgFnPc2d1	příslušná
korových	korový	k2eAgFnPc2d1	korová
oblastí	oblast	k1gFnPc2	oblast
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
z	z	k7c2	z
vestibulárních	vestibulární	k2eAgNnPc2d1	vestibulární
jader	jádro	k1gNnPc2	jádro
a	a	k8xC	a
rovnovážného	rovnovážný	k2eAgNnSc2d1	rovnovážné
ústrojí	ústrojí	k1gNnSc2	ústrojí
(	(	kIx(	(
<g/>
informace	informace	k1gFnPc1	informace
o	o	k7c6	o
poloze	poloha	k1gFnSc6	poloha
či	či	k8xC	či
pohybu	pohyb	k1gInSc6	pohyb
těla	tělo	k1gNnSc2	tělo
<g/>
)	)	kIx)	)
a	a	k8xC	a
jednak	jednak	k8xC	jednak
z	z	k7c2	z
míchy	mícha	k1gFnSc2	mícha
(	(	kIx(	(
<g/>
proprioceptivní	proprioceptivní	k2eAgFnPc1d1	proprioceptivní
informace	informace	k1gFnPc1	informace
z	z	k7c2	z
těla	tělo	k1gNnSc2	tělo
–	–	k?	–
pozice	pozice	k1gFnSc2	pozice
končetin	končetina	k1gFnPc2	končetina
<g/>
,	,	kIx,	,
napětí	napětí	k1gNnSc2	napětí
svalů	sval	k1gInPc2	sval
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
vlákna	vlákno	k1gNnPc1	vlákno
tvoří	tvořit	k5eAaImIp3nP	tvořit
v	v	k7c6	v
mozečku	mozeček	k1gInSc6	mozeček
tzv.	tzv.	kA	tzv.
arbor	arbor	k1gInSc1	arbor
vitae	vitae	k1gInSc1	vitae
neboli	neboli	k8xC	neboli
strom	strom	k1gInSc1	strom
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
jejich	jejich	k3xOp3gNnPc4	jejich
větvení	větvení	k1gNnPc4	větvení
opravdu	opravdu	k6eAd1	opravdu
silně	silně	k6eAd1	silně
připomíná	připomínat	k5eAaImIp3nS	připomínat
strom	strom	k1gInSc1	strom
<g/>
.	.	kIx.	.
</s>
<s>
Mozečková	mozečkový	k2eAgFnSc1d1	mozečková
kůra	kůra	k1gFnSc1	kůra
tyto	tento	k3xDgFnPc4	tento
informace	informace	k1gFnPc4	informace
zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
a	a	k8xC	a
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
jader	jádro	k1gNnPc2	jádro
je	být	k5eAaImIp3nS	být
vysílá	vysílat	k5eAaImIp3nS	vysílat
skrze	skrze	k?	skrze
Thalamus	thalamus	k1gInSc1	thalamus
do	do	k7c2	do
kůry	kůra	k1gFnSc2	kůra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Střední	střední	k2eAgInSc1d1	střední
mozek	mozek	k1gInSc1	mozek
===	===	k?	===
</s>
</p>
<p>
<s>
Střední	střední	k2eAgInSc1d1	střední
mozek	mozek	k1gInSc1	mozek
neboli	neboli	k8xC	neboli
mesencephalon	mesencephalon	k1gInSc1	mesencephalon
je	být	k5eAaImIp3nS	být
poslední	poslední	k2eAgInSc4d1	poslední
úsek	úsek	k1gInSc4	úsek
mozkového	mozkový	k2eAgInSc2d1	mozkový
kmene	kmen	k1gInSc2	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Anatomicky	anatomicky	k6eAd1	anatomicky
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
části	část	k1gFnPc4	část
–	–	k?	–
tectum	tectum	k1gNnSc1	tectum
<g/>
,	,	kIx,	,
tegmentum	tegmentum	k1gNnSc1	tegmentum
a	a	k8xC	a
crura	crura	k1gFnSc1	crura
cerebri	cerebr	k1gFnSc2	cerebr
<g/>
.	.	kIx.	.
</s>
<s>
Tectum	Tectum	k1gNnSc1	Tectum
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
čtverhrbolí	čtverhrbolit	k5eAaImIp3nS	čtverhrbolit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
skupina	skupina	k1gFnSc1	skupina
čtyřech	čtyři	k4xCgInPc2	čtyři
drobných	drobný	k2eAgMnPc2d1	drobný
hrbolků	hrbolek	k1gInPc2	hrbolek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
evolučně	evolučně	k6eAd1	evolučně
stará	starý	k2eAgNnPc1d1	staré
vývojová	vývojový	k2eAgNnPc1d1	vývojové
centra	centrum	k1gNnPc1	centrum
pro	pro	k7c4	pro
zrak	zrak	k1gInSc4	zrak
a	a	k8xC	a
sluch	sluch	k1gInSc4	sluch
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc4	jejichž
funkci	funkce	k1gFnSc4	funkce
později	pozdě	k6eAd2	pozdě
převzal	převzít	k5eAaPmAgInS	převzít
Thalamus	thalamus	k1gInSc1	thalamus
a	a	k8xC	a
korová	korový	k2eAgNnPc1d1	korové
centra	centrum	k1gNnPc1	centrum
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
stále	stále	k6eAd1	stále
do	do	k7c2	do
nich	on	k3xPp3gMnPc2	on
vedou	vést	k5eAaImIp3nP	vést
odbočky	odbočka	k1gFnPc4	odbočka
ze	z	k7c2	z
zrakové	zrakový	k2eAgFnSc2d1	zraková
a	a	k8xC	a
sluchové	sluchový	k2eAgFnSc2d1	sluchová
dráhy	dráha	k1gFnSc2	dráha
a	a	k8xC	a
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
mimovolné	mimovolný	k2eAgFnPc4d1	mimovolná
odpovědi	odpověď	k1gFnPc4	odpověď
na	na	k7c4	na
zrakové	zrakový	k2eAgInPc4d1	zrakový
a	a	k8xC	a
sluchové	sluchový	k2eAgInPc4d1	sluchový
podněty	podnět	k1gInPc4	podnět
<g/>
.	.	kIx.	.
</s>
<s>
Tegmentum	Tegmentum	k1gNnSc1	Tegmentum
je	být	k5eAaImIp3nS	být
střední	střední	k2eAgFnSc4d1	střední
etáž	etáž	k1gFnSc4	etáž
mesencephala	mesencephal	k1gMnSc2	mesencephal
a	a	k8xC	a
kromě	kromě	k7c2	kromě
mnoha	mnoho	k4c2	mnoho
šedých	šedý	k2eAgFnPc2d1	šedá
hmot	hmota	k1gFnPc2	hmota
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
významem	význam	k1gInSc7	význam
jak	jak	k8xC	jak
pro	pro	k7c4	pro
motoriku	motorik	k1gMnSc3	motorik
(	(	kIx(	(
<g/>
nucleus	nucleus	k1gMnSc1	nucleus
ruber	rubrum	k1gNnPc2	rubrum
a	a	k8xC	a
další	další	k2eAgNnPc4d1	další
jádra	jádro	k1gNnPc4	jádro
hlavových	hlavový	k2eAgInPc2d1	hlavový
nervů	nerv	k1gInPc2	nerv
<g/>
)	)	kIx)	)
tak	tak	k9	tak
pro	pro	k7c4	pro
mimovolnou	mimovolný	k2eAgFnSc4d1	mimovolná
činnost	činnost	k1gFnSc4	činnost
(	(	kIx(	(
<g/>
substancia	substancia	k1gFnSc1	substancia
grisea	grisea	k1gFnSc1	grisea
centralis	centralis	k1gFnSc1	centralis
okolo	okolo	k7c2	okolo
aqueductus	aqueductus	k1gInSc4	aqueductus
mesencephali	mesencephat	k5eAaImAgMnP	mesencephat
<g/>
,	,	kIx,	,
retikulární	retikulární	k2eAgInPc1d1	retikulární
formace	formace	k1gFnPc4	formace
a	a	k8xC	a
četná	četný	k2eAgNnPc4d1	četné
intersticiální	intersticiální	k2eAgNnPc4d1	intersticiální
jádra	jádro	k1gNnPc4	jádro
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Skrze	Skrze	k?	Skrze
tegmentum	tegmentum	k1gNnSc1	tegmentum
prochází	procházet	k5eAaImIp3nS	procházet
tzv.	tzv.	kA	tzv.
aqueductus	aqueductus	k1gMnSc1	aqueductus
mesencephali	mesencephat	k5eAaBmAgMnP	mesencephat
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
spojuje	spojovat	k5eAaImIp3nS	spojovat
třetí	třetí	k4xOgFnSc4	třetí
a	a	k8xC	a
čtvrtou	čtvrtý	k4xOgFnSc4	čtvrtý
mozkovou	mozkový	k2eAgFnSc4d1	mozková
komoru	komora	k1gFnSc4	komora
<g/>
.	.	kIx.	.
</s>
<s>
Hranici	hranice	k1gFnSc4	hranice
mezi	mezi	k7c7	mezi
tegmentem	tegment	k1gInSc7	tegment
a	a	k8xC	a
crura	crura	k6eAd1	crura
cerebri	cerebri	k6eAd1	cerebri
tvoří	tvořit	k5eAaImIp3nP	tvořit
funkčně	funkčně	k6eAd1	funkčně
nesmírně	smírně	k6eNd1	smírně
významná	významný	k2eAgFnSc1d1	významná
struktura	struktura	k1gFnSc1	struktura
–	–	k?	–
substantia	substantius	k1gMnSc2	substantius
nigra	nigr	k1gMnSc2	nigr
(	(	kIx(	(
<g/>
černá	černý	k2eAgFnSc1d1	černá
hmota	hmota	k1gFnSc1	hmota
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
kromě	kromě	k7c2	kromě
svého	svůj	k3xOyFgNnSc2	svůj
zapojení	zapojení	k1gNnSc2	zapojení
do	do	k7c2	do
motorických	motorický	k2eAgInPc2d1	motorický
okruhů	okruh	k1gInPc2	okruh
také	také	k9	také
místem	místem	k6eAd1	místem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vzniká	vznikat	k5eAaImIp3nS	vznikat
dopamin	dopamin	k1gInSc1	dopamin
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
nedostatek	nedostatek	k1gInSc1	nedostatek
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
parkinsonismus	parkinsonismus	k1gInSc4	parkinsonismus
<g/>
.	.	kIx.	.
</s>
<s>
Crura	Crura	k6eAd1	Crura
cerebri	cerebri	k6eAd1	cerebri
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
výhradně	výhradně	k6eAd1	výhradně
z	z	k7c2	z
bílé	bílý	k2eAgFnSc2d1	bílá
hmoty	hmota	k1gFnSc2	hmota
a	a	k8xC	a
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
descendentní	descendentní	k2eAgFnPc1d1	descendentní
dráhy	dráha	k1gFnPc1	dráha
z	z	k7c2	z
mozkové	mozkový	k2eAgFnSc2d1	mozková
kůry	kůra	k1gFnSc2	kůra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Mezimozek	mezimozek	k1gInSc4	mezimozek
===	===	k?	===
</s>
</p>
<p>
<s>
Mezimozek	mezimozek	k1gInSc1	mezimozek
neboli	neboli	k8xC	neboli
diencephalon	diencephalon	k1gInSc1	diencephalon
je	být	k5eAaImIp3nS	být
soubor	soubor	k1gInSc4	soubor
šedých	šedý	k2eAgFnPc2d1	šedá
hmot	hmota	k1gFnPc2	hmota
sdružených	sdružený	k2eAgInPc2d1	sdružený
okolo	okolo	k7c2	okolo
třetí	třetí	k4xOgFnSc2	třetí
mozkové	mozkový	k2eAgFnSc2d1	mozková
komory	komora	k1gFnSc2	komora
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
velký	velký	k2eAgInSc4d1	velký
funkční	funkční	k2eAgInSc4d1	funkční
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
struktur	struktura	k1gFnPc2	struktura
<g/>
:	:	kIx,	:
Thalamus	thalamus	k1gInSc1	thalamus
<g/>
,	,	kIx,	,
hypothalamus	hypothalamus	k1gMnSc1	hypothalamus
<g/>
,	,	kIx,	,
subthalamus	subthalamus	k1gMnSc1	subthalamus
<g/>
,	,	kIx,	,
metathalamus	metathalamus	k1gMnSc1	metathalamus
a	a	k8xC	a
epithalamus	epithalamus	k1gMnSc1	epithalamus
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
i	i	k8xC	i
sítnice	sítnice	k1gFnSc2	sítnice
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
výchlipkou	výchlipka	k1gFnSc7	výchlipka
diencephala	diencephal	k1gMnSc2	diencephal
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
====	====	k?	====
Thalamus	thalamus	k1gInSc4	thalamus
====	====	k?	====
</s>
</p>
<p>
<s>
Thalamus	thalamus	k1gInSc1	thalamus
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgInSc1d3	veliký
útvar	útvar	k1gInSc1	útvar
v	v	k7c6	v
diencephalu	diencephal	k1gInSc6	diencephal
<g/>
,	,	kIx,	,
soubor	soubor	k1gInSc1	soubor
šedých	šedý	k2eAgFnPc2d1	šedá
hmot	hmota	k1gFnPc2	hmota
který	který	k3yQgInSc4	který
přijímá	přijímat	k5eAaImIp3nS	přijímat
sensitivní	sensitivní	k2eAgInPc4d1	sensitivní
podněty	podnět	k1gInPc4	podnět
z	z	k7c2	z
celého	celý	k2eAgNnSc2d1	celé
těla	tělo	k1gNnSc2	tělo
a	a	k8xC	a
přepojuje	přepojovat	k5eAaImIp3nS	přepojovat
je	být	k5eAaImIp3nS	být
dále	daleko	k6eAd2	daleko
do	do	k7c2	do
kůry	kůra	k1gFnSc2	kůra
<g/>
.	.	kIx.	.
</s>
<s>
Thalamus	thalamus	k1gInSc1	thalamus
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
"	"	kIx"	"
<g/>
bránu	brána	k1gFnSc4	brána
do	do	k7c2	do
vědomí	vědomí	k1gNnSc2	vědomí
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
že	že	k8xS	že
podněty	podnět	k1gInPc1	podnět
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
nedosáhnou	dosáhnout	k5eNaPmIp3nP	dosáhnout
Thalamu	thalamus	k1gInSc3	thalamus
si	se	k3xPyFc3	se
člověk	člověk	k1gMnSc1	člověk
neuvědomí	uvědomit	k5eNaPmIp3nS	uvědomit
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
je	být	k5eAaImIp3nS	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
retikulární	retikulární	k2eAgFnSc7d1	retikulární
formací	formace	k1gFnSc7	formace
součást	součást	k1gFnSc4	součást
ascendentního	ascendentní	k2eAgInSc2d1	ascendentní
budivého	budivý	k2eAgInSc2d1	budivý
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Subthalamus	Subthalamus	k1gInSc4	Subthalamus
====	====	k?	====
</s>
</p>
<p>
<s>
Subthalamus	Subthalamus	k1gInSc1	Subthalamus
je	být	k5eAaImIp3nS	být
soubor	soubor	k1gInSc4	soubor
šedých	šedý	k2eAgFnPc2d1	šedá
hmot	hmota	k1gFnPc2	hmota
uložený	uložený	k2eAgInSc1d1	uložený
těsně	těsně	k6eAd1	těsně
pod	pod	k7c7	pod
thalamem	thalamus	k1gInSc7	thalamus
<g/>
.	.	kIx.	.
</s>
<s>
Účastní	účastnit	k5eAaImIp3nS	účastnit
se	se	k3xPyFc4	se
na	na	k7c4	na
koordinaci	koordinace	k1gFnSc4	koordinace
pohybů	pohyb	k1gInPc2	pohyb
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc1	jeho
poškození	poškození	k1gNnSc1	poškození
může	moct	k5eAaImIp3nS	moct
vyústit	vyústit	k5eAaPmF	vyústit
v	v	k7c4	v
různé	různý	k2eAgFnPc4d1	různá
poruchy	porucha	k1gFnPc4	porucha
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
hemibalismus	hemibalismus	k1gInSc4	hemibalismus
–	–	k?	–
prudké	prudký	k2eAgInPc4d1	prudký
<g/>
,	,	kIx,	,
házivé	házivý	k2eAgInPc4d1	házivý
pohyby	pohyb	k1gInPc4	pohyb
končetin	končetina	k1gFnPc2	končetina
<g/>
,	,	kIx,	,
objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
kontralaterálně	kontralaterálně	k6eAd1	kontralaterálně
k	k	k7c3	k
poškození	poškození	k1gNnSc3	poškození
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
na	na	k7c6	na
opačné	opačný	k2eAgFnSc6d1	opačná
straně	strana	k1gFnSc6	strana
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
poškození	poškození	k1gNnSc1	poškození
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Hypothalamus	Hypothalamus	k1gInSc4	Hypothalamus
====	====	k?	====
</s>
</p>
<p>
<s>
Hypothalamus	Hypothalamus	k1gInSc1	Hypothalamus
je	být	k5eAaImIp3nS	být
podkorové	podkorový	k2eAgNnSc4d1	podkorový
visceromotorické	visceromotorický	k2eAgNnSc4d1	visceromotorický
centrum	centrum	k1gNnSc4	centrum
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
mnoho	mnoho	k4c1	mnoho
významných	významný	k2eAgFnPc2d1	významná
funkcí	funkce	k1gFnPc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Jednak	jednak	k8xC	jednak
je	být	k5eAaImIp3nS	být
zapojený	zapojený	k2eAgMnSc1d1	zapojený
do	do	k7c2	do
endokrinního	endokrinní	k2eAgInSc2d1	endokrinní
systému	systém	k1gInSc2	systém
(	(	kIx(	(
<g/>
hypothalamo-hypofysární	hypothalamoypofysární	k2eAgInSc1d1	hypothalamo-hypofysární
systém	systém	k1gInSc1	systém
<g/>
)	)	kIx)	)
a	a	k8xC	a
jednak	jednak	k8xC	jednak
je	být	k5eAaImIp3nS	být
nadřazený	nadřazený	k2eAgMnSc1d1	nadřazený
autonomnímu	autonomní	k2eAgNnSc3d1	autonomní
nervstvu	nervstvo	k1gNnSc3	nervstvo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
zapojený	zapojený	k2eAgInSc4d1	zapojený
do	do	k7c2	do
limbického	limbický	k2eAgInSc2d1	limbický
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
v	v	k7c6	v
nás	my	k3xPp1nPc6	my
emoce	emoce	k1gFnSc1	emoce
vzbuzují	vzbuzovat	k5eAaImIp3nP	vzbuzovat
somatické	somatický	k2eAgFnPc4d1	somatická
reakce	reakce	k1gFnPc4	reakce
(	(	kIx(	(
<g/>
červenání	červenání	k1gNnSc6	červenání
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
zrychlení	zrychlení	k1gNnSc4	zrychlení
srdeční	srdeční	k2eAgFnSc2d1	srdeční
akce	akce	k1gFnSc2	akce
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
je	být	k5eAaImIp3nS	být
pomocí	pomocí	k7c2	pomocí
svého	svůj	k3xOyFgNnSc2	svůj
napojení	napojení	k1gNnSc2	napojení
na	na	k7c4	na
zrakovou	zrakový	k2eAgFnSc4d1	zraková
dráhu	dráha	k1gFnSc4	dráha
a	a	k8xC	a
epifýsu	epifýsa	k1gFnSc4	epifýsa
účasten	účasten	k2eAgMnSc1d1	účasten
na	na	k7c6	na
regulaci	regulace	k1gFnSc6	regulace
cirkadiánních	cirkadiánní	k2eAgInPc2d1	cirkadiánní
rytmů	rytmus	k1gInPc2	rytmus
(	(	kIx(	(
<g/>
něco	něco	k6eAd1	něco
jako	jako	k8xC	jako
denní	denní	k2eAgInSc1d1	denní
režim	režim	k1gInSc1	režim
těla	tělo	k1gNnSc2	tělo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Metathalamus	Metathalamus	k1gInSc4	Metathalamus
====	====	k?	====
</s>
</p>
<p>
<s>
Metathalamus	Metathalamus	k1gInSc1	Metathalamus
sestává	sestávat	k5eAaImIp3nS	sestávat
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
hrbolků	hrbolek	k1gInPc2	hrbolek
připojených	připojený	k2eAgInPc2d1	připojený
na	na	k7c4	na
zatní	zatní	k2eAgFnSc4d1	zatní
stranu	strana	k1gFnSc4	strana
thalamu	thalamus	k1gInSc2	thalamus
–	–	k?	–
corpus	corpus	k1gInSc1	corpus
geniculatum	geniculatum	k1gNnSc4	geniculatum
laterale	laterale	k6eAd1	laterale
et	et	k?	et
mediale	mediale	k6eAd1	mediale
a	a	k8xC	a
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
podkorové	podkorový	k2eAgInPc4d1	podkorový
zrakové	zrakový	k2eAgInPc4d1	zrakový
(	(	kIx(	(
<g/>
c.g.	c.g.	k?	c.g.
laterale	laterale	k6eAd1	laterale
<g/>
)	)	kIx)	)
a	a	k8xC	a
sluchové	sluchový	k2eAgNnSc1d1	sluchové
(	(	kIx(	(
<g/>
c.g.	c.g.	k?	c.g.
mediale	mediala	k1gFnSc6	mediala
<g/>
)	)	kIx)	)
centrum	centrum	k1gNnSc1	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
přepojuje	přepojovat	k5eAaImIp3nS	přepojovat
zraková	zrakový	k2eAgFnSc1d1	zraková
<g/>
/	/	kIx~	/
<g/>
sluchová	sluchový	k2eAgFnSc1d1	sluchová
dráha	dráha	k1gFnSc1	dráha
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
odtud	odtud	k6eAd1	odtud
vychází	vycházet	k5eAaImIp3nS	vycházet
odbočky	odbočka	k1gFnPc4	odbočka
do	do	k7c2	do
evolučně	evolučně	k6eAd1	evolučně
starších	starý	k2eAgNnPc2d2	starší
center	centrum	k1gNnPc2	centrum
v	v	k7c6	v
tectu	tect	k1gInSc6	tect
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Epithalamus	Epithalamus	k1gInSc4	Epithalamus
====	====	k?	====
</s>
</p>
<p>
<s>
Epithalamus	Epithalamus	k1gInSc1	Epithalamus
je	být	k5eAaImIp3nS	být
oddíl	oddíl	k1gInSc4	oddíl
diencephala	diencephal	k1gMnSc2	diencephal
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
tyto	tento	k3xDgFnPc4	tento
důležité	důležitý	k2eAgFnPc4d1	důležitá
struktury	struktura	k1gFnPc4	struktura
<g/>
:	:	kIx,	:
epifýsu	epifýsa	k1gFnSc4	epifýsa
<g/>
,	,	kIx,	,
habenulum	habenulum	k1gInSc1	habenulum
a	a	k8xC	a
comissura	comissura	k1gFnSc1	comissura
habenulare	habenular	k1gMnSc5	habenular
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
pretektální	pretektální	k2eAgFnSc4d1	pretektální
oblast	oblast	k1gFnSc4	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
přepojuje	přepojovat	k5eAaImIp3nS	přepojovat
většina	většina	k1gFnSc1	většina
očních	oční	k2eAgInPc2d1	oční
reflexů	reflex	k1gInPc2	reflex
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Koncový	koncový	k2eAgInSc1d1	koncový
mozek	mozek	k1gInSc1	mozek
===	===	k?	===
</s>
</p>
<p>
<s>
Koncový	koncový	k2eAgInSc1d1	koncový
mozek	mozek	k1gInSc1	mozek
neboli	neboli	k8xC	neboli
telencepalon	telencepalon	k1gInSc1	telencepalon
je	být	k5eAaImIp3nS	být
evolučně	evolučně	k6eAd1	evolučně
nejmladší	mladý	k2eAgFnSc4d3	nejmladší
část	část	k1gFnSc4	část
mozku	mozek	k1gInSc2	mozek
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
savců	savec	k1gMnPc2	savec
a	a	k8xC	a
zejména	zejména	k9	zejména
u	u	k7c2	u
primátů	primát	k1gMnPc2	primát
silně	silně	k6eAd1	silně
vyvinuta	vyvinout	k5eAaPmNgFnS	vyvinout
<g/>
.	.	kIx.	.
</s>
<s>
Telencephalon	Telencephalon	k1gInSc1	Telencephalon
je	být	k5eAaImIp3nS	být
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
hemisféry	hemisféra	k1gFnPc4	hemisféra
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
každá	každý	k3xTgFnSc1	každý
má	mít	k5eAaImIp3nS	mít
pět	pět	k4xCc4	pět
laloků	lalok	k1gInPc2	lalok
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
šest	šest	k4xCc1	šest
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
některých	některý	k3yIgNnPc2	některý
dělení	dělení	k1gNnPc2	dělení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Frontální	frontální	k2eAgInSc4d1	frontální
(	(	kIx(	(
<g/>
čelní	čelní	k2eAgInSc4d1	čelní
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Parietální	parietální	k2eAgFnSc1d1	parietální
(	(	kIx(	(
<g/>
temenní	temenní	k2eAgFnSc1d1	temenní
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Okcipitální	okcipitální	k2eAgInSc4d1	okcipitální
(	(	kIx(	(
<g/>
týlní	týlní	k2eAgInSc4d1	týlní
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Temporální	temporální	k2eAgInSc4d1	temporální
(	(	kIx(	(
<g/>
spánkový	spánkový	k2eAgInSc4d1	spánkový
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jediný	jediný	k2eAgInSc1d1	jediný
je	být	k5eAaImIp3nS	být
fyzicky	fyzicky	k6eAd1	fyzicky
oddělen	oddělit	k5eAaPmNgInS	oddělit
od	od	k7c2	od
ostatních	ostatní	k2eAgMnPc2d1	ostatní
ve	v	k7c4	v
fissura	fissur	k1gMnSc4	fissur
lateralis	lateralis	k1gFnSc2	lateralis
cerebri	cerebr	k1gFnSc2	cerebr
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Insulární	insulární	k2eAgFnSc1d1	insulární
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
insula	insula	k1gFnSc1	insula
=	=	kIx~	=
ostrov	ostrov	k1gInSc1	ostrov
<g/>
;	;	kIx,	;
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
úsek	úsek	k1gInSc4	úsek
kůry	kůra	k1gFnSc2	kůra
skrytý	skrytý	k2eAgInSc4d1	skrytý
pod	pod	k7c7	pod
spánkovým	spánkový	k2eAgInSc7d1	spánkový
lalokem	lalok	k1gInSc7	lalok
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
vyčleňuje	vyčleňovat	k5eAaImIp3nS	vyčleňovat
limbický	limbický	k2eAgInSc4d1	limbický
lalok	lalok	k1gInSc4	lalok
(	(	kIx(	(
<g/>
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
spíše	spíše	k9	spíše
o	o	k7c4	o
funkční	funkční	k2eAgFnSc4d1	funkční
jednotku	jednotka	k1gFnSc4	jednotka
<g/>
)	)	kIx)	)
<g/>
Hemisféry	hemisféra	k1gFnSc2	hemisféra
jsou	být	k5eAaImIp3nP	být
pokryty	pokrýt	k5eAaPmNgInP	pokrýt
kůrou	kůra	k1gFnSc7	kůra
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
typy	typ	k1gInPc4	typ
<g/>
:	:	kIx,	:
paleocortex	paleocortex	k1gInSc1	paleocortex
–	–	k?	–
evolučně	evolučně	k6eAd1	evolučně
nejstarší	starý	k2eAgFnSc1d3	nejstarší
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgFnPc4	tři
vrstvy	vrstva	k1gFnPc4	vrstva
<g/>
,	,	kIx,	,
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
čichovém	čichový	k2eAgInSc6d1	čichový
mozku	mozek	k1gInSc6	mozek
<g/>
;	;	kIx,	;
archicortex	archicortex	k1gInSc1	archicortex
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
taktéž	taktéž	k?	taktéž
evolučně	evolučně	k6eAd1	evolučně
stará	starý	k2eAgFnSc1d1	stará
třívrstevná	třívrstevný	k2eAgFnSc1d1	třívrstevná
kůra	kůra	k1gFnSc1	kůra
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
některých	některý	k3yIgFnPc2	některý
struktur	struktura	k1gFnPc2	struktura
limbického	limbický	k2eAgInSc2d1	limbický
systému	systém	k1gInSc2	systém
<g/>
;	;	kIx,	;
neocortex	neocortex	k1gInSc1	neocortex
–	–	k?	–
tvoří	tvořit	k5eAaImIp3nS	tvořit
96	[number]	k4	96
%	%	kIx~	%
povrchu	povrch	k1gInSc2	povrch
hemisfér	hemisféra	k1gFnPc2	hemisféra
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
evolučně	evolučně	k6eAd1	evolučně
mladší	mladý	k2eAgMnSc1d2	mladší
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
šestivrstevná	šestivrstevný	k2eAgFnSc1d1	šestivrstevný
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
koncovém	koncový	k2eAgInSc6d1	koncový
mozku	mozek	k1gInSc6	mozek
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
tyto	tento	k3xDgFnPc1	tento
významné	významný	k2eAgFnPc1d1	významná
struktury	struktura	k1gFnPc1	struktura
<g/>
:	:	kIx,	:
bazální	bazální	k2eAgNnPc1d1	bazální
ganglia	ganglion	k1gNnPc1	ganglion
–	–	k?	–
podkorové	podkorový	k2eAgNnSc1d1	podkorový
motorické	motorický	k2eAgNnSc1d1	motorické
centrum	centrum	k1gNnSc1	centrum
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
velký	velký	k2eAgInSc4d1	velký
význam	význam	k1gInSc4	význam
pro	pro	k7c4	pro
přípravu	příprava	k1gFnSc4	příprava
složitých	složitý	k2eAgInPc2d1	složitý
pohybů	pohyb	k1gInPc2	pohyb
<g/>
,	,	kIx,	,
u	u	k7c2	u
některých	některý	k3yIgMnPc2	některý
nižších	nízký	k2eAgMnPc2d2	nižší
obratlovců	obratlovec	k1gMnPc2	obratlovec
(	(	kIx(	(
<g/>
např.	např.	kA	např.
laboratorní	laboratorní	k2eAgFnSc4d1	laboratorní
myš	myš	k1gFnSc4	myš
s	s	k7c7	s
experimentálně	experimentálně	k6eAd1	experimentálně
poškozenou	poškozený	k2eAgFnSc7d1	poškozená
kůrou	kůra	k1gFnSc7	kůra
<g/>
)	)	kIx)	)
mohou	moct	k5eAaImIp3nP	moct
suplovat	suplovat	k5eAaImF	suplovat
funkci	funkce	k1gFnSc4	funkce
korových	korový	k2eAgNnPc2d1	korové
motorických	motorický	k2eAgNnPc2d1	motorické
center	centrum	k1gNnPc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
sem	sem	k6eAd1	sem
patří	patřit	k5eAaImIp3nP	patřit
struktury	struktura	k1gFnPc1	struktura
limbického	limbický	k2eAgInSc2d1	limbický
systému	systém	k1gInSc2	systém
–	–	k?	–
podkorové	podkorový	k2eAgNnSc1d1	podkorový
centrum	centrum	k1gNnSc1	centrum
amygdala	amygdal	k1gMnSc2	amygdal
<g/>
,	,	kIx,	,
hipokampus	hipokampus	k1gInSc1	hipokampus
(	(	kIx(	(
<g/>
korové	korový	k2eAgNnSc1d1	korové
centrum	centrum	k1gNnSc1	centrum
tvořené	tvořený	k2eAgNnSc1d1	tvořené
archicortexem	archicortex	k1gInSc7	archicortex
<g/>
)	)	kIx)	)
a	a	k8xC	a
gyrus	gyrus	k1gInSc1	gyrus
cinguli	cingule	k1gFnSc3	cingule
(	(	kIx(	(
<g/>
korové	korový	k2eAgNnSc1d1	korové
centrum	centrum	k1gNnSc1	centrum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Povrch	povrch	k1gInSc1	povrch
hemisfér	hemisféra	k1gFnPc2	hemisféra
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
rozbrázděn	rozbrázděn	k2eAgInSc4d1	rozbrázděn
do	do	k7c2	do
mnoha	mnoho	k4c2	mnoho
závitů	závit	k1gInPc2	závit
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
lissencephalních	lissencephalní	k2eAgMnPc2d1	lissencephalní
živočichů	živočich	k1gMnPc2	živočich
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
mozek	mozek	k1gInSc4	mozek
hladký	hladký	k2eAgInSc4d1	hladký
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
efektivně	efektivně	k6eAd1	efektivně
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
plocha	plocha	k1gFnSc1	plocha
kůry	kůra	k1gFnSc2	kůra
<g/>
.	.	kIx.	.
</s>
<s>
Závity	závit	k1gInPc1	závit
(	(	kIx(	(
<g/>
gyri	gyr	k1gFnPc1	gyr
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
sebe	se	k3xPyFc2	se
odděleny	oddělit	k5eAaPmNgInP	oddělit
rýhami	rýha	k1gFnPc7	rýha
(	(	kIx(	(
<g/>
sulci	sulec	k1gInPc7	sulec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
kterých	který	k3yQgInPc2	který
je	být	k5eAaImIp3nS	být
nejdůležitějším	důležitý	k2eAgInSc7d3	nejdůležitější
sulcus	sulcus	k1gInSc1	sulcus
centralis	centralis	k1gInSc4	centralis
který	který	k3yQgMnSc1	který
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
motorické	motorický	k2eAgFnSc3d1	motorická
a	a	k8xC	a
sensitivní	sensitivní	k2eAgFnSc3d1	sensitivní
korové	korový	k2eAgFnSc3d1	korová
oblasti	oblast	k1gFnSc3	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Gyrifikace	Gyrifikace	k1gFnSc1	Gyrifikace
probíhá	probíhat	k5eAaImIp3nS	probíhat
již	již	k6eAd1	již
v	v	k7c6	v
embryonálním	embryonální	k2eAgNnSc6d1	embryonální
období	období	k1gNnSc6	období
a	a	k8xC	a
při	při	k7c6	při
narození	narození	k1gNnSc6	narození
je	být	k5eAaImIp3nS	být
dokončená	dokončený	k2eAgFnSc1d1	dokončená
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Bílá	bílý	k2eAgFnSc1d1	bílá
hmota	hmota	k1gFnSc1	hmota
===	===	k?	===
</s>
</p>
<p>
<s>
Bílá	bílý	k2eAgFnSc1d1	bílá
hmota	hmota	k1gFnSc1	hmota
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
myelinisovaných	myelinisovaný	k2eAgInPc2d1	myelinisovaný
axonů	axon	k1gInPc2	axon
nervových	nervový	k2eAgFnPc2d1	nervová
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mozku	mozek	k1gInSc6	mozek
tvoří	tvořit	k5eAaImIp3nS	tvořit
tři	tři	k4xCgFnPc4	tři
základní	základní	k2eAgFnPc4d1	základní
skupiny	skupina	k1gFnPc4	skupina
<g/>
:	:	kIx,	:
vlákna	vlákna	k1gFnSc1	vlákna
projekční	projekční	k2eAgFnSc1d1	projekční
<g/>
,	,	kIx,	,
asociační	asociační	k2eAgFnSc1d1	asociační
a	a	k8xC	a
komisurální	komisurální	k2eAgFnSc1d1	komisurální
<g/>
.	.	kIx.	.
</s>
<s>
Projekční	projekční	k2eAgNnPc1d1	projekční
vlákna	vlákno	k1gNnPc1	vlákno
jsou	být	k5eAaImIp3nP	být
vlákna	vlákno	k1gNnPc1	vlákno
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
předávají	předávat	k5eAaImIp3nP	předávat
informace	informace	k1gFnPc4	informace
z	z	k7c2	z
nižších	nízký	k2eAgFnPc2d2	nižší
etáží	etáž	k1gFnPc2	etáž
na	na	k7c4	na
vyšší	vysoký	k2eAgFnPc4d2	vyšší
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
capsula	capsula	k1gFnSc1	capsula
interna	interna	k1gFnSc1	interna
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc1d3	veliký
skupina	skupina	k1gFnSc1	skupina
projekčních	projekční	k2eAgNnPc2d1	projekční
vláken	vlákno	k1gNnPc2	vlákno
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
vedou	vést	k5eAaImIp3nP	vést
motorické	motorický	k2eAgInPc1d1	motorický
povely	povel	k1gInPc1	povel
z	z	k7c2	z
kůry	kůra	k1gFnSc2	kůra
<g/>
.	.	kIx.	.
<g/>
Asociační	asociační	k2eAgNnPc1d1	asociační
vlákna	vlákno	k1gNnPc1	vlákno
jsou	být	k5eAaImIp3nP	být
taková	takový	k3xDgNnPc1	takový
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
spojují	spojovat	k5eAaImIp3nP	spojovat
různé	různý	k2eAgNnSc1d1	různé
místa	místo	k1gNnPc1	místo
na	na	k7c6	na
téže	týž	k3xTgFnSc6	týž
etáži	etáž	k1gFnSc6	etáž
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc4d3	veliký
význam	význam	k1gInSc4	význam
mají	mít	k5eAaImIp3nP	mít
asociační	asociační	k2eAgNnPc1d1	asociační
vlákna	vlákno	k1gNnPc1	vlákno
korová	korový	k2eAgNnPc1d1	korové
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
integrují	integrovat	k5eAaBmIp3nP	integrovat
v	v	k7c6	v
asociačních	asociační	k2eAgFnPc6d1	asociační
oblastech	oblast	k1gFnPc6	oblast
informace	informace	k1gFnSc2	informace
a	a	k8xC	a
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
tak	tak	k6eAd1	tak
složité	složitý	k2eAgInPc1d1	složitý
procesy	proces	k1gInPc1	proces
jako	jako	k8xS	jako
poznávání	poznávání	k1gNnSc1	poznávání
<g/>
,	,	kIx,	,
plánování	plánování	k1gNnSc1	plánování
či	či	k8xC	či
uvažování	uvažování	k1gNnSc1	uvažování
<g/>
.	.	kIx.	.
<g/>
Komisurální	komisurální	k2eAgNnPc1d1	komisurální
vlákna	vlákno	k1gNnPc1	vlákno
jsou	být	k5eAaImIp3nP	být
taková	takový	k3xDgNnPc1	takový
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
spojují	spojovat	k5eAaImIp3nP	spojovat
navzájem	navzájem	k6eAd1	navzájem
hemisféry	hemisféra	k1gFnPc1	hemisféra
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgMnSc1d3	veliký
takovou	takový	k3xDgFnSc7	takový
komisurou	komisura	k1gFnSc7	komisura
je	být	k5eAaImIp3nS	být
corpus	corpus	k1gInSc1	corpus
callosum	callosum	k1gInSc1	callosum
–	–	k?	–
nejvýraznější	výrazný	k2eAgInSc1d3	nejvýraznější
útvar	útvar	k1gInSc1	útvar
na	na	k7c6	na
sagitálním	sagitální	k2eAgInSc6d1	sagitální
řezu	řez	k1gInSc6	řez
mozkem	mozek	k1gInSc7	mozek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Komorový	komorový	k2eAgInSc1d1	komorový
systém	systém	k1gInSc1	systém
===	===	k?	===
</s>
</p>
<p>
<s>
Uvnitř	uvnitř	k7c2	uvnitř
centrální	centrální	k2eAgFnSc2d1	centrální
nervové	nervový	k2eAgFnSc2d1	nervová
soustavy	soustava	k1gFnSc2	soustava
je	být	k5eAaImIp3nS	být
systém	systém	k1gInSc1	systém
dutin	dutina	k1gFnPc2	dutina
vyplněných	vyplněný	k2eAgFnPc2d1	vyplněná
mozkomíšním	mozkomíšní	k2eAgInSc7d1	mozkomíšní
mokem	mok	k1gInSc7	mok
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
likvorem	likvor	k1gInSc7	likvor
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
pozůstatek	pozůstatek	k1gInSc1	pozůstatek
embryonálního	embryonální	k2eAgInSc2d1	embryonální
vývoje	vývoj	k1gInSc2	vývoj
(	(	kIx(	(
<g/>
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
počátečních	počáteční	k2eAgNnPc2d1	počáteční
stadií	stadion	k1gNnPc2	stadion
vývoje	vývoj	k1gInSc2	vývoj
je	být	k5eAaImIp3nS	být
dutá	dutý	k2eAgFnSc1d1	dutá
<g/>
,	,	kIx,	,
uzavřená	uzavřený	k2eAgFnSc1d1	uzavřená
trubice	trubice	k1gFnSc1	trubice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mozku	mozek	k1gInSc6	mozek
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
čtyři	čtyři	k4xCgFnPc1	čtyři
komory	komora	k1gFnPc1	komora
<g/>
:	:	kIx,	:
dvě	dva	k4xCgFnPc1	dva
postranní	postranní	k2eAgFnPc1d1	postranní
komory	komora	k1gFnPc1	komora
v	v	k7c6	v
koncovém	koncový	k2eAgInSc6d1	koncový
mozku	mozek	k1gInSc6	mozek
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgFnSc1	třetí
mozková	mozkový	k2eAgFnSc1d1	mozková
komora	komora	k1gFnSc1	komora
v	v	k7c6	v
diencephalu	diencephal	k1gInSc6	diencephal
a	a	k8xC	a
čtvrtá	čtvrtý	k4xOgFnSc1	čtvrtý
mozková	mozkový	k2eAgFnSc1d1	mozková
komora	komora	k1gFnSc1	komora
v	v	k7c6	v
pontu	pont	k1gInSc6	pont
a	a	k8xC	a
oblongatě	oblongata	k1gFnSc6	oblongata
<g/>
.	.	kIx.	.
<g/>
Uvnitř	uvnitř	k7c2	uvnitř
komor	komora	k1gFnPc2	komora
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
nazvaných	nazvaný	k2eAgMnPc2d1	nazvaný
plexus	plexus	k1gInSc4	plexus
chorioideus	chorioideus	k1gInSc1	chorioideus
likvor	likvor	k1gInSc1	likvor
vzniká	vznikat	k5eAaImIp3nS	vznikat
(	(	kIx(	(
<g/>
jedno	jeden	k4xCgNnSc1	jeden
takové	takový	k3xDgMnPc4	takový
místo	místo	k1gNnSc1	místo
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
komoře	komora	k1gFnSc6	komora
<g/>
)	)	kIx)	)
a	a	k8xC	a
odtéká	odtékat	k5eAaImIp3nS	odtékat
směrem	směr	k1gInSc7	směr
do	do	k7c2	do
čtvrté	čtvrtá	k1gFnSc2	čtvrtá
komory	komora	k1gFnSc2	komora
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
skrze	skrze	k?	skrze
tři	tři	k4xCgInPc4	tři
otvory	otvor	k1gInPc4	otvor
(	(	kIx(	(
<g/>
nepárové	párový	k2eNgFnPc4d1	nepárová
foramen	foramen	k2eAgMnSc1d1	foramen
Magenti	Magent	k1gMnPc1	Magent
a	a	k8xC	a
párová	párový	k2eAgFnSc1d1	párová
foramina	foramin	k2eAgFnSc1d1	foramina
Luschkae	Luschkae	k1gFnSc1	Luschkae
<g/>
)	)	kIx)	)
dostává	dostávat	k5eAaImIp3nS	dostávat
do	do	k7c2	do
subarachnoidálního	subarachnoidální	k2eAgInSc2d1	subarachnoidální
prostoru	prostor	k1gInSc2	prostor
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
se	se	k3xPyFc4	se
vstřebává	vstřebávat	k5eAaImIp3nS	vstřebávat
do	do	k7c2	do
žilních	žilní	k2eAgInPc2d1	žilní
splavů	splav	k1gInPc2	splav
<g/>
.	.	kIx.	.
</s>
<s>
Neprůchodnost	Neprůchodnost	k1gFnSc1	Neprůchodnost
těchto	tento	k3xDgInPc2	tento
otvorů	otvor	k1gInPc2	otvor
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
patologickému	patologický	k2eAgInSc3d1	patologický
stavu	stav	k1gInSc3	stav
známému	známý	k2eAgInSc3d1	známý
pod	pod	k7c4	pod
názvem	název	k1gInSc7	název
vnitřní	vnitřní	k2eAgInSc4d1	vnitřní
hydrocephalus	hydrocephalus	k1gInSc4	hydrocephalus
<g/>
.	.	kIx.	.
</s>
<s>
Likvor	likvor	k1gInSc1	likvor
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
na	na	k7c4	na
udržování	udržování	k1gNnSc4	udržování
stálého	stálý	k2eAgNnSc2d1	stálé
prostředí	prostředí	k1gNnSc2	prostředí
v	v	k7c6	v
mozku	mozek	k1gInSc6	mozek
a	a	k8xC	a
na	na	k7c6	na
odstraňování	odstraňování	k1gNnSc6	odstraňování
metabolitů	metabolit	k1gInPc2	metabolit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Mozkové	mozkový	k2eAgInPc1d1	mozkový
pleny	plen	k1gInPc1	plen
===	===	k?	===
</s>
</p>
<p>
<s>
Mozek	mozek	k1gInSc1	mozek
a	a	k8xC	a
mícha	mícha	k1gFnSc1	mícha
jsou	být	k5eAaImIp3nP	být
uloženy	uložit	k5eAaPmNgInP	uložit
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
meningovém	meningový	k2eAgInSc6d1	meningový
vaku	vak	k1gInSc6	vak
<g/>
.	.	kIx.	.
</s>
<s>
Meningy	meninga	k1gFnPc1	meninga
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
mozkové	mozkový	k2eAgFnSc2d1	mozková
pleny	plena	k1gFnSc2	plena
jsou	být	k5eAaImIp3nP	být
tři	tři	k4xCgInPc1	tři
<g/>
:	:	kIx,	:
dura	dur	k2eAgFnSc1d1	dura
mater	mater	k1gFnSc1	mater
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
tvrdá	tvrdý	k2eAgFnSc1d1	tvrdá
plena	plena	k1gFnSc1	plena
mozková	mozkový	k2eAgFnSc1d1	mozková
je	být	k5eAaImIp3nS	být
svrchní	svrchní	k2eAgInSc4d1	svrchní
tuhý	tuhý	k2eAgInSc4d1	tuhý
obal	obal	k1gInSc4	obal
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lebce	lebka	k1gFnSc6	lebka
těsně	těsně	k6eAd1	těsně
naléhá	naléhat	k5eAaBmIp3nS	naléhat
na	na	k7c4	na
periost	periost	k1gInSc4	periost
lebky	lebka	k1gFnSc2	lebka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
páteřním	páteřní	k2eAgInSc6d1	páteřní
kanálu	kanál	k1gInSc6	kanál
je	být	k5eAaImIp3nS	být
nad	nad	k7c7	nad
ní	on	k3xPp3gFnSc7	on
epidurální	epidurální	k2eAgInSc1d1	epidurální
prostor	prostor	k1gInSc1	prostor
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
podávání	podávání	k1gNnSc4	podávání
epidurální	epidurální	k2eAgFnSc2d1	epidurální
anestesie	anestesie	k1gFnSc2	anestesie
<g/>
.	.	kIx.	.
<g/>
Pod	pod	k7c7	pod
durou	durý	k2eAgFnSc7d1	durý
mater	mater	k1gFnSc7	mater
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
arachnoidea	arachnoidea	k6eAd1	arachnoidea
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
pavučnice	pavučnice	k1gFnSc1	pavučnice
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
velice	velice	k6eAd1	velice
jemnou	jemný	k2eAgFnSc4d1	jemná
blánu	blána	k1gFnSc4	blána
ve	v	k7c4	v
které	který	k3yRgFnPc4	který
probíhají	probíhat	k5eAaImIp3nP	probíhat
větší	veliký	k2eAgFnPc4d2	veliký
cévy	céva	k1gFnPc4	céva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
místě	místo	k1gNnSc6	místo
kontaktu	kontakt	k1gInSc2	kontakt
arachnoidey	arachnoidea	k1gFnSc2	arachnoidea
a	a	k8xC	a
žilních	žilní	k2eAgInPc2d1	žilní
splavů	splav	k1gInPc2	splav
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
tzv.	tzv.	kA	tzv.
granulationes	granulationes	k1gInSc4	granulationes
arachnoidae	arachnoidae	k1gNnSc2	arachnoidae
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
kterých	který	k3yIgInPc2	který
se	se	k3xPyFc4	se
vstřebává	vstřebávat	k5eAaImIp3nS	vstřebávat
likvor	likvor	k1gInSc1	likvor
ze	z	k7c2	z
subarachnoidálního	subarachnoidální	k2eAgInSc2d1	subarachnoidální
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
tyto	tento	k3xDgFnPc1	tento
chybí	chybit	k5eAaPmIp3nP	chybit
(	(	kIx(	(
<g/>
následkem	následkem	k7c2	následkem
úrazu	úraz	k1gInSc2	úraz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vzniká	vznikat	k5eAaImIp3nS	vznikat
zevní	zevní	k2eAgInSc1d1	zevní
hydrocephalus	hydrocephalus	k1gInSc1	hydrocephalus
<g/>
.	.	kIx.	.
<g/>
Nejvnitřnější	vnitřní	k2eAgFnSc1d3	nejvnitřnější
je	být	k5eAaImIp3nS	být
pia	pia	k?	pia
mater	mater	k1gFnSc1	mater
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
omozečnice	omozečnice	k1gFnSc1	omozečnice
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
blánu	blána	k1gFnSc4	blána
pevnější	pevný	k2eAgFnSc1d2	pevnější
než	než	k8xS	než
arachnoidea	arachnoide	k2eAgFnSc1d1	arachnoide
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
tak	tak	k6eAd1	tak
pevnou	pevný	k2eAgFnSc4d1	pevná
jako	jako	k8xS	jako
dura	dura	k6eAd1	dura
mater	mater	k1gFnSc1	mater
<g/>
.	.	kIx.	.
</s>
<s>
Mozek	mozek	k1gInSc1	mozek
těsně	těsně	k6eAd1	těsně
obklopuje	obklopovat	k5eAaImIp3nS	obklopovat
a	a	k8xC	a
nedá	dát	k5eNaPmIp3nS	dát
se	se	k3xPyFc4	se
odpreparovat	odpreparovat	k5eAaPmF	odpreparovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mikroskopická	mikroskopický	k2eAgFnSc1d1	mikroskopická
anatomie	anatomie	k1gFnSc1	anatomie
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
mozku	mozek	k1gInSc6	mozek
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
dvě	dva	k4xCgFnPc1	dva
skupiny	skupina	k1gFnPc1	skupina
buněk	buňka	k1gFnPc2	buňka
–	–	k?	–
neurony	neuron	k1gInPc1	neuron
a	a	k8xC	a
glie	gli	k1gInPc1	gli
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
tkáně	tkáň	k1gFnSc2	tkáň
tvoří	tvořit	k5eAaImIp3nS	tvořit
právě	právě	k9	právě
podpůrná	podpůrný	k2eAgFnSc1d1	podpůrná
glie	glie	k1gFnSc1	glie
<g/>
.	.	kIx.	.
</s>
<s>
Jistou	jistý	k2eAgFnSc4d1	jistá
pozornost	pozornost	k1gFnSc4	pozornost
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
věnovat	věnovat	k5eAaImF	věnovat
mikroskopické	mikroskopický	k2eAgFnSc3d1	mikroskopická
stavbě	stavba	k1gFnSc3	stavba
mozkové	mozkový	k2eAgFnSc2d1	mozková
kůry	kůra	k1gFnSc2	kůra
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
neocortexu	neocortex	k1gInSc2	neocortex
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
má	mít	k5eAaImIp3nS	mít
šest	šest	k4xCc4	šest
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
lamina	lamin	k2eAgFnSc1d1	lamina
molecularis	molecularis	k1gFnSc1	molecularis
–	–	k?	–
nejsvrchnější	svrchní	k2eAgFnSc1d3	nejsvrchnější
vrstva	vrstva	k1gFnSc1	vrstva
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
málo	málo	k4c4	málo
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
horizontální	horizontální	k2eAgFnPc1d1	horizontální
buňky	buňka	k1gFnPc1	buňka
Cajalovy	Cajalův	k2eAgFnPc1d1	Cajalův
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
vrstvě	vrstva	k1gFnSc6	vrstva
spolu	spolu	k6eAd1	spolu
komunikují	komunikovat	k5eAaImIp3nP	komunikovat
výběžky	výběžek	k1gInPc4	výběžek
buněk	buňka	k1gFnPc2	buňka
z	z	k7c2	z
ostatních	ostatní	k2eAgFnPc2d1	ostatní
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
lamina	lamin	k2eAgFnSc1d1	lamina
granularis	granularis	k1gFnSc1	granularis
externa	externa	k1gFnSc1	externa
–	–	k?	–
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
asociační	asociační	k2eAgFnSc4d1	asociační
vrstvu	vrstva	k1gFnSc4	vrstva
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
převažují	převažovat	k5eAaImIp3nP	převažovat
granulární	granulární	k2eAgInPc4d1	granulární
a	a	k8xC	a
gliaformní	gliaformní	k2eAgInPc4d1	gliaformní
neurony	neuron	k1gInPc4	neuron
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
jméno	jméno	k1gNnSc4	jméno
dostala	dostat	k5eAaPmAgFnS	dostat
podle	podle	k7c2	podle
svého	svůj	k3xOyFgInSc2	svůj
vzhledu	vzhled	k1gInSc2	vzhled
v	v	k7c6	v
Nisslově	Nisslově	k1gFnSc6	Nisslově
barvení	barvení	k1gNnSc2	barvení
–	–	k?	–
malé	malý	k2eAgInPc1d1	malý
neurony	neuron	k1gInPc1	neuron
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
vrstvě	vrstva	k1gFnSc6	vrstva
oproti	oproti	k7c3	oproti
velkým	velký	k2eAgNnPc3d1	velké
pyramidovým	pyramidový	k2eAgNnPc3d1	pyramidové
vypadají	vypadat	k5eAaImIp3nP	vypadat
jako	jako	k8xC	jako
zrníčka	zrníčko	k1gNnPc4	zrníčko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
lamina	lamin	k2eAgFnSc1d1	lamina
pyramidalis	pyramidalis	k1gFnSc1	pyramidalis
externa	externa	k1gFnSc1	externa
–	–	k?	–
tato	tento	k3xDgFnSc1	tento
vrstva	vrstva	k1gFnSc1	vrstva
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
zejména	zejména	k9	zejména
středními	střední	k2eAgInPc7d1	střední
pyramidovými	pyramidový	k2eAgInPc7d1	pyramidový
neurony	neuron	k1gInPc7	neuron
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
neurony	neuron	k1gInPc1	neuron
hvězdicovité	hvězdicovitý	k2eAgInPc1d1	hvězdicovitý
<g/>
(	(	kIx(	(
<g/>
Martinottiho	Martinotti	k1gMnSc2	Martinotti
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
košíčkové	košíčkový	k2eAgFnPc1d1	Košíčková
a	a	k8xC	a
fusiformní	fusiformní	k2eAgFnPc1d1	fusiformní
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vrstva	vrstva	k1gFnSc1	vrstva
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
zejména	zejména	k9	zejména
komisurální	komisurální	k2eAgNnSc1d1	komisurální
spojení	spojení	k1gNnSc1	spojení
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
výše	výše	k1gFnSc2	výše
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
lamina	lamin	k2eAgFnSc1d1	lamina
granularis	granularis	k1gFnSc1	granularis
interna	interna	k1gFnSc1	interna
–	–	k?	–
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
vrstvu	vrstva	k1gFnSc4	vrstva
s	s	k7c7	s
naprostou	naprostý	k2eAgFnSc7d1	naprostá
převahou	převaha	k1gFnSc7	převaha
granulárních	granulární	k2eAgInPc2d1	granulární
neuronů	neuron	k1gInPc2	neuron
a	a	k8xC	a
s	s	k7c7	s
malou	malý	k2eAgFnSc7d1	malá
účastí	účast	k1gFnSc7	účast
jiných	jiný	k2eAgInPc2d1	jiný
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
třeba	třeba	k6eAd1	třeba
košíčkových	košíčkový	k2eAgInPc2d1	košíčkový
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
vrstvě	vrstva	k1gFnSc6	vrstva
končí	končit	k5eAaImIp3nS	končit
většina	většina	k1gFnSc1	většina
vláken	vlákna	k1gFnPc2	vlákna
z	z	k7c2	z
thalamu	thalamus	k1gInSc2	thalamus
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
variabilní	variabilní	k2eAgInSc1d1	variabilní
–	–	k?	–
v	v	k7c6	v
sensitivních	sensitivní	k2eAgFnPc6d1	sensitivní
oblastech	oblast	k1gFnPc6	oblast
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
silná	silný	k2eAgFnSc1d1	silná
<g/>
,	,	kIx,	,
v	v	k7c6	v
motorických	motorický	k2eAgFnPc6d1	motorická
téměř	téměř	k6eAd1	téměř
není	být	k5eNaImIp3nS	být
poznat	poznat	k5eAaPmF	poznat
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInPc1	její
neurony	neuron	k1gInPc1	neuron
mají	mít	k5eAaImIp3nP	mít
zejména	zejména	k9	zejména
funkci	funkce	k1gFnSc4	funkce
interneuronů	interneuron	k1gInPc2	interneuron
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
lamina	lamin	k2eAgFnSc1d1	lamina
pyramidalis	pyramidalis	k1gFnSc1	pyramidalis
interna	interna	k1gFnSc1	interna
–	–	k?	–
tuto	tento	k3xDgFnSc4	tento
vrstvu	vrstva	k1gFnSc4	vrstva
tvoří	tvořit	k5eAaImIp3nP	tvořit
zejména	zejména	k9	zejména
velké	velký	k2eAgFnPc4d1	velká
pyramidové	pyramidový	k2eAgFnPc4d1	pyramidová
buňky	buňka	k1gFnPc4	buňka
(	(	kIx(	(
<g/>
ty	ten	k3xDgInPc1	ten
největší	veliký	k2eAgInPc1d3	veliký
<g/>
,	,	kIx,	,
Betzovy	Betzův	k2eAgInPc1d1	Betzův
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
měřit	měřit	k5eAaImF	měřit
až	až	k9	až
100	[number]	k4	100
μ	μ	k?	μ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
hlavní	hlavní	k2eAgFnSc4d1	hlavní
projekční	projekční	k2eAgFnSc4d1	projekční
vrstvu	vrstva	k1gFnSc4	vrstva
(	(	kIx(	(
<g/>
projikuje	projikovat	k5eAaImIp3nS	projikovat
do	do	k7c2	do
podkorových	podkorový	k2eAgNnPc2d1	podkorový
center	centrum	k1gNnPc2	centrum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
lamina	lamin	k2eAgFnSc1d1	lamina
multiformis	multiformis	k1gFnSc1	multiformis
–	–	k?	–
jak	jak	k8xC	jak
název	název	k1gInSc1	název
napovídá	napovídat	k5eAaBmIp3nS	napovídat
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
vrstvu	vrstva	k1gFnSc4	vrstva
tvořenou	tvořený	k2eAgFnSc7d1	tvořená
různými	různý	k2eAgInPc7d1	různý
neurony	neuron	k1gInPc7	neuron
(	(	kIx(	(
<g/>
hvězdicovité-Martinottiho	hvězdicovité-Martinotti	k1gMnSc2	hvězdicovité-Martinotti
<g/>
,	,	kIx,	,
malé	malý	k2eAgNnSc4d1	malé
polyformní	polyformní	k2eAgNnSc4d1	polyformní
a	a	k8xC	a
vřetenité	vřetenitý	k2eAgNnSc4d1	vřetenitý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Touto	tento	k3xDgFnSc7	tento
vrstvou	vrstva	k1gFnSc7	vrstva
prochází	procházet	k5eAaImIp3nS	procházet
všechna	všechen	k3xTgFnSc1	všechen
vlákna	vlákna	k1gFnSc1	vlákna
z	z	k7c2	z
kůry	kůra	k1gFnSc2	kůra
a	a	k8xC	a
do	do	k7c2	do
kůry	kůra	k1gFnSc2	kůra
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
rozdílů	rozdíl	k1gInPc2	rozdíl
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
vyvinutosti	vyvinutost	k1gFnSc2	vyvinutost
těchto	tento	k3xDgFnPc2	tento
vrstev	vrstva	k1gFnPc2	vrstva
Korbinian	Korbinian	k1gMnSc1	Korbinian
Brodmann	Brodmann	k1gMnSc1	Brodmann
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1907	[number]	k4	1907
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
cytoarchitektonickou	cytoarchitektonický	k2eAgFnSc4d1	cytoarchitektonický
mapu	mapa	k1gFnSc4	mapa
kůry	kůra	k1gFnSc2	kůra
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
povrch	povrch	k1gInSc1	povrch
rozčlenil	rozčlenit	k5eAaPmAgInS	rozčlenit
na	na	k7c4	na
11	[number]	k4	11
regionů	region	k1gInPc2	region
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c4	na
52	[number]	k4	52
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
mapa	mapa	k1gFnSc1	mapa
byla	být	k5eAaImAgFnS	být
četnými	četný	k2eAgMnPc7d1	četný
výzkumy	výzkum	k1gInPc4	výzkum
prokázaná	prokázaný	k2eAgFnSc1d1	prokázaná
jako	jako	k8xS	jako
správná	správný	k2eAgFnSc1d1	správná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
vzniknul	vzniknout	k5eAaPmAgInS	vzniknout
projekt	projekt	k1gInSc1	projekt
Brain	Brain	k1gInSc4	Brain
Activity	Activita	k1gFnSc2	Activita
Map	mapa	k1gFnPc2	mapa
Project	Projecta	k1gFnPc2	Projecta
usilující	usilující	k2eAgFnSc1d1	usilující
o	o	k7c4	o
zmapování	zmapování	k1gNnSc4	zmapování
aktivity	aktivita	k1gFnSc2	aktivita
každého	každý	k3xTgInSc2	každý
neuronu	neuron	k1gInSc2	neuron
lidského	lidský	k2eAgInSc2d1	lidský
mozku	mozek	k1gInSc2	mozek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Neexistuje	existovat	k5eNaImIp3nS	existovat
rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
mužským	mužský	k2eAgInSc7d1	mužský
a	a	k8xC	a
ženským	ženský	k2eAgInSc7d1	ženský
mozkem	mozek	k1gInSc7	mozek
<g/>
,	,	kIx,	,
rozdíly	rozdíl	k1gInPc4	rozdíl
mezi	mezi	k7c4	mezi
mozky	mozek	k1gInPc4	mozek
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
žen	žena	k1gFnPc2	žena
jsou	být	k5eAaImIp3nP	být
dány	dát	k5eAaPmNgInP	dát
vnějšími	vnější	k2eAgFnPc7d1	vnější
okolnostmi	okolnost	k1gFnPc7	okolnost
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
způsobem	způsob	k1gInSc7	způsob
výchovy	výchova	k1gFnSc2	výchova
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Cévní	cévní	k2eAgNnSc1d1	cévní
zásobování	zásobování	k1gNnSc1	zásobování
mozku	mozek	k1gInSc2	mozek
==	==	k?	==
</s>
</p>
<p>
<s>
Mozek	mozek	k1gInSc1	mozek
je	být	k5eAaImIp3nS	být
artériemi	artérie	k1gFnPc7	artérie
zásoben	zásobit	k5eAaPmNgMnS	zásobit
okysličenou	okysličený	k2eAgFnSc7d1	okysličená
krví	krev	k1gFnSc7	krev
<g/>
.	.	kIx.	.
</s>
<s>
Mozková	mozkový	k2eAgFnSc1d1	mozková
tkáň	tkáň	k1gFnSc1	tkáň
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
krve	krev	k1gFnSc2	krev
<g/>
,	,	kIx,	,
asi	asi	k9	asi
15	[number]	k4	15
<g/>
–	–	k?	–
<g/>
20	[number]	k4	20
%	%	kIx~	%
právě	právě	k6eAd1	právě
cirkulující	cirkulující	k2eAgFnSc1d1	cirkulující
<g/>
.	.	kIx.	.
</s>
<s>
Mozek	mozek	k1gInSc4	mozek
zásobují	zásobovat	k5eAaImIp3nP	zásobovat
dva	dva	k4xCgInPc1	dva
páry	pár	k1gInPc1	pár
tepen	tepna	k1gFnPc2	tepna
<g/>
.	.	kIx.	.
</s>
<s>
Arteriae	Arteria	k1gInPc1	Arteria
vertebralis	vertebralis	k1gFnSc2	vertebralis
zásobují	zásobovat	k5eAaImIp3nP	zásobovat
mozkový	mozkový	k2eAgInSc4d1	mozkový
kmen	kmen	k1gInSc4	kmen
a	a	k8xC	a
mozeček	mozeček	k1gInSc4	mozeček
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
mozku	mozek	k1gInSc2	mozek
je	být	k5eAaImIp3nS	být
zásobovaná	zásobovaný	k2eAgFnSc1d1	zásobovaná
z	z	k7c2	z
arteriae	arteria	k1gInSc2	arteria
carotidaes	carotidaes	k1gInSc4	carotidaes
internae	internae	k1gNnSc2	internae
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
tepenné	tepenný	k2eAgInPc1d1	tepenný
systémy	systém	k1gInPc1	systém
spolu	spolu	k6eAd1	spolu
tvoří	tvořit	k5eAaImIp3nP	tvořit
anastomotický	anastomotický	k2eAgInSc4d1	anastomotický
okruh	okruh	k1gInSc4	okruh
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
circulus	circulus	k1gMnSc1	circulus
arteriosus	arteriosus	k1gMnSc1	arteriosus
Wilisi	Wilise	k1gFnSc6	Wilise
pojmenovaný	pojmenovaný	k2eAgMnSc1d1	pojmenovaný
po	po	k7c6	po
svém	svůj	k3xOyFgMnSc6	svůj
objeviteli	objevitel	k1gMnSc6	objevitel
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
i	i	k9	i
při	při	k7c6	při
ucpání	ucpání	k1gNnSc6	ucpání
některé	některý	k3yIgFnSc2	některý
z	z	k7c2	z
tepen	tepna	k1gFnPc2	tepna
nepřerušila	přerušit	k5eNaPmAgFnS	přerušit
dodávka	dodávka	k1gFnSc1	dodávka
krve	krev	k1gFnSc2	krev
do	do	k7c2	do
mozku	mozek	k1gInSc2	mozek
(	(	kIx(	(
<g/>
když	když	k8xS	když
se	se	k3xPyFc4	se
ucpe	ucpat	k5eAaPmIp3nS	ucpat
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
aa	aa	k?	aa
<g/>
.	.	kIx.	.
carotidae	carotidaat	k5eAaPmIp3nS	carotidaat
internae	internae	k1gFnSc1	internae
<g/>
,	,	kIx,	,
zbývající	zbývající	k2eAgFnPc1d1	zbývající
tepny	tepna	k1gFnPc1	tepna
se	se	k3xPyFc4	se
dokáží	dokázat	k5eAaPmIp3nP	dokázat
adaptovat	adaptovat	k5eAaBmF	adaptovat
a	a	k8xC	a
mozek	mozek	k1gInSc4	mozek
uživit	uživit	k5eAaPmF	uživit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mozkové	mozkový	k2eAgFnPc1d1	mozková
žíly	žíla	k1gFnPc1	žíla
odvádějí	odvádět	k5eAaImIp3nP	odvádět
krev	krev	k1gFnSc4	krev
do	do	k7c2	do
systému	systém	k1gInSc2	systém
splavů	splav	k1gInPc2	splav
(	(	kIx(	(
<g/>
sinusů	sinus	k1gInPc2	sinus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
směřuje	směřovat	k5eAaImIp3nS	směřovat
do	do	k7c2	do
srdce	srdce	k1gNnSc2	srdce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Významné	významný	k2eAgFnPc1d1	významná
funkční	funkční	k2eAgFnPc1d1	funkční
oblasti	oblast	k1gFnPc1	oblast
mozkové	mozkový	k2eAgFnSc2d1	mozková
kůry	kůra	k1gFnSc2	kůra
==	==	k?	==
</s>
</p>
<p>
<s>
Kůra	kůra	k1gFnSc1	kůra
hemisfér	hemisféra	k1gFnPc2	hemisféra
koncového	koncový	k2eAgInSc2d1	koncový
mozku	mozek	k1gInSc2	mozek
hostí	hostit	k5eAaImIp3nS	hostit
mnoho	mnoho	k4c1	mnoho
významných	významný	k2eAgNnPc2d1	významné
center	centrum	k1gNnPc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Jednak	jednak	k8xC	jednak
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
primární	primární	k2eAgNnPc4d1	primární
a	a	k8xC	a
sekundární	sekundární	k2eAgNnPc4d1	sekundární
sensorická	sensorický	k2eAgNnPc4d1	sensorické
centra	centrum	k1gNnPc4	centrum
(	(	kIx(	(
<g/>
zrakové	zrakový	k2eAgNnSc4d1	zrakové
<g/>
,	,	kIx,	,
sluchové	sluchový	k2eAgFnPc1d1	sluchová
<g/>
,	,	kIx,	,
čichové	čichový	k2eAgFnPc1d1	čichová
<g/>
,	,	kIx,	,
chuťové	chuťový	k2eAgFnPc1d1	chuťová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
o	o	k7c4	o
motorické	motorický	k2eAgNnSc4d1	motorické
a	a	k8xC	a
sensitivní	sensitivní	k2eAgNnSc4d1	sensitivní
centrum	centrum	k1gNnSc4	centrum
(	(	kIx(	(
<g/>
tato	tento	k3xDgNnPc4	tento
dvě	dva	k4xCgNnPc4	dva
centra	centrum	k1gNnPc4	centrum
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
závitech	závit	k1gInPc6	závit
těsně	těsně	k6eAd1	těsně
obklopujících	obklopující	k2eAgInPc2d1	obklopující
sulcus	sulcus	k1gInSc4	sulcus
centralis	centralis	k1gFnPc4	centralis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
premotorická	premotorický	k2eAgFnSc1d1	premotorická
oblast	oblast	k1gFnSc1	oblast
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
uložené	uložený	k2eAgInPc1d1	uložený
naučené	naučený	k2eAgInPc1d1	naučený
pohyby	pohyb	k1gInPc1	pohyb
<g/>
,	,	kIx,	,
frontální	frontální	k2eAgNnSc1d1	frontální
okohybné	okohybný	k2eAgNnSc1d1	okohybný
pole	pole	k1gNnSc1	pole
(	(	kIx(	(
<g/>
významné	významný	k2eAgNnSc1d1	významné
korové	korový	k2eAgNnSc1d1	korové
centrum	centrum	k1gNnSc1	centrum
řídící	řídící	k2eAgInPc1d1	řídící
nevědomé	vědomý	k2eNgInPc1d1	nevědomý
oční	oční	k2eAgInPc1d1	oční
pohyby	pohyb	k1gInPc1	pohyb
<g/>
)	)	kIx)	)
a	a	k8xC	a
řečová	řečový	k2eAgNnPc1d1	řečové
centra	centrum	k1gNnPc1	centrum
<g/>
.	.	kIx.	.
<g/>
Řečová	řečový	k2eAgNnPc1d1	řečové
centra	centrum	k1gNnPc1	centrum
jsou	být	k5eAaImIp3nP	být
dvě	dva	k4xCgFnPc4	dva
–	–	k?	–
Brocovo	Brocův	k2eAgNnSc1d1	Brocovo
motorické	motorický	k2eAgNnSc1d1	motorické
centrum	centrum	k1gNnSc1	centrum
(	(	kIx(	(
<g/>
při	při	k7c6	při
jeho	jeho	k3xOp3gNnSc6	jeho
poškození	poškození	k1gNnSc6	poškození
člověk	člověk	k1gMnSc1	člověk
trpí	trpět	k5eAaImIp3nS	trpět
motorickou	motorický	k2eAgFnSc7d1	motorická
afázií	afázie	k1gFnSc7	afázie
řeči	řeč	k1gFnSc2	řeč
–	–	k?	–
sice	sice	k8xC	sice
rozumí	rozumět	k5eAaImIp3nS	rozumět
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
vydávat	vydávat	k5eAaImF	vydávat
srozumitelná	srozumitelný	k2eAgNnPc1d1	srozumitelné
slova	slovo	k1gNnPc1	slovo
<g/>
)	)	kIx)	)
a	a	k8xC	a
Wernikeho	Wernike	k1gMnSc2	Wernike
gnostické	gnostický	k2eAgNnSc1d1	gnostické
centrum	centrum	k1gNnSc1	centrum
(	(	kIx(	(
<g/>
při	při	k7c6	při
jeho	jeho	k3xOp3gInSc6	jeho
poškození	poškození	k1gNnSc6	poškození
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
sensorické	sensorický	k2eAgFnSc3d1	sensorická
afázii	afázie	k1gFnSc3	afázie
–	–	k?	–
pacient	pacient	k1gMnSc1	pacient
nerozumí	rozumět	k5eNaImIp3nS	rozumět
mluvenému	mluvený	k2eAgNnSc3d1	mluvené
slovu	slovo	k1gNnSc3	slovo
a	a	k8xC	a
když	když	k8xS	když
mluví	mluvit	k5eAaImIp3nS	mluvit
spojuje	spojovat	k5eAaImIp3nS	spojovat
slova	slovo	k1gNnPc4	slovo
do	do	k7c2	do
nesrozumitelných	srozumitelný	k2eNgInPc2d1	nesrozumitelný
celků	celek	k1gInPc2	celek
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
si	se	k3xPyFc3	se
ale	ale	k9	ale
neuvědomuje	uvědomovat	k5eNaImIp3nS	uvědomovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
si	se	k3xPyFc3	se
sám	sám	k3xTgMnSc1	sám
nerozumí	rozumět	k5eNaImIp3nS	rozumět
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c6	o
evolučně	evolučně	k6eAd1	evolučně
velmi	velmi	k6eAd1	velmi
mladá	mladý	k2eAgNnPc1d1	mladé
centra	centrum	k1gNnPc1	centrum
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
nejsou	být	k5eNaImIp3nP	být
přítomná	přítomný	k2eAgFnSc1d1	přítomná
bilaterálně	bilaterálně	k6eAd1	bilaterálně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
dominantní	dominantní	k2eAgFnSc6d1	dominantní
hemisféře	hemisféra	k1gFnSc6	hemisféra
<g/>
.	.	kIx.	.
<g/>
Hemisféry	hemisféra	k1gFnPc1	hemisféra
nejsou	být	k5eNaImIp3nP	být
obě	dva	k4xCgFnPc1	dva
symetrické	symetrický	k2eAgFnPc1d1	symetrická
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
známé	známý	k2eAgNnSc1d1	známé
že	že	k8xS	že
levá	levý	k2eAgFnSc1d1	levá
hemisféra	hemisféra	k1gFnSc1	hemisféra
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
zaměřená	zaměřený	k2eAgFnSc1d1	zaměřená
na	na	k7c4	na
analytické	analytický	k2eAgNnSc4d1	analytické
myšlení	myšlení	k1gNnSc4	myšlení
a	a	k8xC	a
početní	početní	k2eAgFnPc4d1	početní
operace	operace	k1gFnPc4	operace
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
pravá	pravý	k2eAgFnSc1d1	pravá
je	být	k5eAaImIp3nS	být
zaměřená	zaměřený	k2eAgFnSc1d1	zaměřená
spíše	spíše	k9	spíše
na	na	k7c6	na
vnímání	vnímání	k1gNnSc6	vnímání
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
abstraktní	abstraktní	k2eAgNnSc4d1	abstraktní
myšlení	myšlení	k1gNnSc4	myšlení
<g/>
.	.	kIx.	.
</s>
<s>
To	to	k9	to
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
hemisféra	hemisféra	k1gFnSc1	hemisféra
je	být	k5eAaImIp3nS	být
dominantní	dominantní	k2eAgNnSc4d1	dominantní
se	se	k3xPyFc4	se
určuje	určovat	k5eAaImIp3nS	určovat
podle	podle	k7c2	podle
přítomnosti	přítomnost	k1gFnSc2	přítomnost
unilaterálních	unilaterální	k2eAgMnPc2d1	unilaterální
(	(	kIx(	(
<g/>
=	=	kIx~	=
jen	jen	k9	jen
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
přítomných	přítomný	k2eAgMnPc2d1	přítomný
<g/>
)	)	kIx)	)
center	centrum	k1gNnPc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
všichni	všechen	k3xTgMnPc1	všechen
praváci	pravák	k1gMnPc1	pravák
a	a	k8xC	a
asi	asi	k9	asi
50	[number]	k4	50
%	%	kIx~	%
leváků	levák	k1gMnPc2	levák
má	mít	k5eAaImIp3nS	mít
dominantní	dominantní	k2eAgFnSc4d1	dominantní
levou	levý	k2eAgFnSc4d1	levá
hemisféru	hemisféra	k1gFnSc4	hemisféra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vývoj	vývoj	k1gInSc1	vývoj
mozku	mozek	k1gInSc2	mozek
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
kraniálním	kraniální	k2eAgInSc6d1	kraniální
konci	konec	k1gInSc6	konec
nervové	nervový	k2eAgFnPc1d1	nervová
trubice	trubice	k1gFnPc1	trubice
se	se	k3xPyFc4	se
zformují	zformovat	k5eAaPmIp3nP	zformovat
tři	tři	k4xCgInPc1	tři
mozkové	mozkový	k2eAgInPc1d1	mozkový
váčky	váček	k1gInPc1	váček
–	–	k?	–
prosencephalon	prosencephalon	k1gInSc1	prosencephalon
<g/>
,	,	kIx,	,
mesencephalon	mesencephalon	k1gInSc1	mesencephalon
a	a	k8xC	a
rhombencephalon	rhombencephalon	k1gInSc1	rhombencephalon
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
stavba	stavba	k1gFnSc1	stavba
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
stavbě	stavba	k1gFnSc3	stavba
nervové	nervový	k2eAgFnSc2d1	nervová
trubice	trubice	k1gFnSc2	trubice
–	–	k?	–
mají	mít	k5eAaImIp3nP	mít
ventrální	ventrální	k2eAgFnPc1d1	ventrální
<g/>
,	,	kIx,	,
dorsální	dorsální	k2eAgFnPc1d1	dorsální
<g/>
,	,	kIx,	,
basální	basální	k2eAgFnPc1d1	basální
a	a	k8xC	a
alární	alární	k2eAgFnPc1d1	alární
ploténky	ploténka	k1gFnPc1	ploténka
<g/>
.	.	kIx.	.
</s>
<s>
Ventrální	ventrální	k2eAgFnPc1d1	ventrální
a	a	k8xC	a
dorsální	dorsální	k2eAgFnPc1d1	dorsální
ploténky	ploténka	k1gFnPc1	ploténka
se	se	k3xPyFc4	se
v	v	k7c6	v
mozku	mozek	k1gInSc6	mozek
moc	moc	k6eAd1	moc
nerozvíjejí	rozvíjet	k5eNaImIp3nP	rozvíjet
<g/>
,	,	kIx,	,
výjimkou	výjimka	k1gFnSc7	výjimka
jsou	být	k5eAaImIp3nP	být
methencephalon	methencephalon	k1gInSc4	methencephalon
a	a	k8xC	a
mesencephalon	mesencephalon	k1gInSc4	mesencephalon
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
basálních	basální	k2eAgFnPc2d1	basální
plotének	ploténka	k1gFnPc2	ploténka
vzniknou	vzniknout	k5eAaPmIp3nP	vzniknout
motorická	motorický	k2eAgNnPc1d1	motorické
a	a	k8xC	a
visceromotorická	visceromotorický	k2eAgNnPc1d1	visceromotorický
centra	centrum	k1gNnPc1	centrum
a	a	k8xC	a
z	z	k7c2	z
alárních	alární	k2eAgFnPc2d1	alární
plotének	ploténka	k1gFnPc2	ploténka
centra	centrum	k1gNnSc2	centrum
sensitivní	sensitivní	k2eAgNnSc1d1	sensitivní
a	a	k8xC	a
sensorická	sensorický	k2eAgFnSc1d1	sensorická
<g/>
.	.	kIx.	.
<g/>
Prosencephalon	Prosencephalon	k1gInSc1	Prosencephalon
se	se	k3xPyFc4	se
rozdělí	rozdělit	k5eAaPmIp3nS	rozdělit
na	na	k7c4	na
telencephalon	telencephalon	k1gInSc4	telencephalon
(	(	kIx(	(
<g/>
ze	z	k7c2	z
kterého	který	k3yIgNnSc2	který
začnou	začít	k5eAaPmIp3nP	začít
růst	růst	k5eAaImF	růst
hemisféry	hemisféra	k1gFnPc4	hemisféra
<g/>
)	)	kIx)	)
a	a	k8xC	a
diencephalon	diencephalon	k1gInSc4	diencephalon
(	(	kIx(	(
<g/>
ze	z	k7c2	z
kterého	který	k3yQgInSc2	který
se	se	k3xPyFc4	se
vyhlípí	vyhlípit	k5eAaPmIp3nS	vyhlípit
základ	základ	k1gInSc1	základ
pro	pro	k7c4	pro
oko	oko	k1gNnSc4	oko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
telencephalu	telencephal	k1gInSc6	telencephal
se	se	k3xPyFc4	se
z	z	k7c2	z
palia	palium	k1gNnSc2	palium
(	(	kIx(	(
<g/>
budoucí	budoucí	k2eAgFnSc1d1	budoucí
kůra	kůra	k1gFnSc1	kůra
<g/>
)	)	kIx)	)
oddělují	oddělovat	k5eAaImIp3nP	oddělovat
podkorové	podkorový	k2eAgFnPc1d1	podkorová
struktury	struktura	k1gFnPc1	struktura
jako	jako	k8xS	jako
basální	basální	k2eAgNnPc1d1	basální
ganglia	ganglion	k1gNnPc1	ganglion
a	a	k8xC	a
amygdala	amygdal	k1gMnSc4	amygdal
a	a	k8xC	a
třívrstevné	třívrstevný	k2eAgNnSc1d1	třívrstevný
paleopalium	paleopalium	k1gNnSc1	paleopalium
je	být	k5eAaImIp3nS	být
nahrazováno	nahrazovat	k5eAaImNgNnS	nahrazovat
neopaliem	neopalium	k1gNnSc7	neopalium
<g/>
.	.	kIx.	.
<g/>
Mesencephalon	Mesencephalon	k1gInSc1	Mesencephalon
se	se	k3xPyFc4	se
nijak	nijak	k6eAd1	nijak
nedělí	dělit	k5eNaImIp3nS	dělit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gFnSc1	jeho
dorsální	dorsální	k2eAgFnSc1d1	dorsální
ploténka	ploténka	k1gFnSc1	ploténka
se	se	k3xPyFc4	se
začne	začít	k5eAaPmIp3nS	začít
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
a	a	k8xC	a
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
tak	tak	k6eAd1	tak
tectum	tectum	k1gNnSc1	tectum
(	(	kIx(	(
<g/>
čtverhrbolí	čtverhrbolí	k1gNnSc1	čtverhrbolí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Rhombencephalon	Rhombencephalon	k1gInSc1	Rhombencephalon
se	se	k3xPyFc4	se
rozdělí	rozdělit	k5eAaPmIp3nS	rozdělit
na	na	k7c4	na
methencephalon	methencephalon	k1gInSc4	methencephalon
(	(	kIx(	(
<g/>
základ	základ	k1gInSc1	základ
pontu	pont	k1gInSc2	pont
<g/>
)	)	kIx)	)
a	a	k8xC	a
myelencephalon	myelencephalon	k1gInSc1	myelencephalon
(	(	kIx(	(
<g/>
základ	základ	k1gInSc1	základ
oblongaty	oblongata	k1gFnSc2	oblongata
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dorsální	dorsální	k2eAgFnSc1d1	dorsální
ploténka	ploténka	k1gFnSc1	ploténka
methencephala	methencephal	k1gMnSc2	methencephal
začne	začít	k5eAaPmIp3nS	začít
pruce	pruko	k6eAd1	pruko
růst	růst	k1gInSc4	růst
a	a	k8xC	a
vzniká	vznikat	k5eAaImIp3nS	vznikat
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
mozeček	mozeček	k1gInSc1	mozeček
<g/>
.	.	kIx.	.
<g/>
Gyrifikace	Gyrifikace	k1gFnSc1	Gyrifikace
hemisfér	hemisféra	k1gFnPc2	hemisféra
probíhá	probíhat	k5eAaImIp3nS	probíhat
postupně	postupně	k6eAd1	postupně
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
se	se	k3xPyFc4	se
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
fissura	fissura	k1gFnSc1	fissura
longitudinalis	longitudinalis	k1gFnSc2	longitudinalis
cerebri	cerebr	k1gFnSc2	cerebr
(	(	kIx(	(
<g/>
dáno	dát	k5eAaPmNgNnS	dát
už	už	k9	už
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
hemisféry	hemisféra	k1gFnPc1	hemisféra
vyrostou	vyrůst	k5eAaPmIp3nP	vyrůst
odděleně	odděleně	k6eAd1	odděleně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
fissura	fissura	k1gFnSc1	fissura
lateralis	lateralis	k1gFnSc2	lateralis
cerebri	cerebr	k1gFnSc2	cerebr
(	(	kIx(	(
<g/>
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
spánkový	spánkový	k2eAgInSc4d1	spánkový
lalok	lalok	k1gInSc4	lalok
<g/>
)	)	kIx)	)
za	za	k7c4	za
kterou	který	k3yRgFnSc4	který
následuje	následovat	k5eAaImIp3nS	následovat
sulcus	sulcus	k1gInSc1	sulcus
centralis	centralis	k1gFnSc2	centralis
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1	ostatní
následují	následovat	k5eAaImIp3nP	následovat
postupně	postupně	k6eAd1	postupně
za	za	k7c7	za
těmito	tento	k3xDgInPc7	tento
útvary	útvar	k1gInPc7	útvar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozvoj	rozvoj	k1gInSc1	rozvoj
(	(	kIx(	(
<g/>
růst	růst	k1gInSc1	růst
<g/>
)	)	kIx)	)
a	a	k8xC	a
regenerace	regenerace	k1gFnSc2	regenerace
==	==	k?	==
</s>
</p>
<p>
<s>
Při	při	k7c6	při
vývoji	vývoj	k1gInSc6	vývoj
lidského	lidský	k2eAgInSc2d1	lidský
zárodku	zárodek	k1gInSc2	zárodek
(	(	kIx(	(
<g/>
embrya	embryo	k1gNnSc2	embryo
<g/>
)	)	kIx)	)
roste	růst	k5eAaImIp3nS	růst
nejrychleji	rychle	k6eAd3	rychle
mozek	mozek	k1gInSc4	mozek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
osmém	osmý	k4xOgInSc6	osmý
týdnu	týden	k1gInSc6	týden
má	mít	k5eAaImIp3nS	mít
již	již	k6eAd1	již
embryo	embryo	k1gNnSc4	embryo
vytvořený	vytvořený	k2eAgInSc1d1	vytvořený
základ	základ	k1gInSc1	základ
všech	všecek	k3xTgInPc2	všecek
budoucích	budoucí	k2eAgInPc2d1	budoucí
orgánů	orgán	k1gInPc2	orgán
lidského	lidský	k2eAgNnSc2d1	lidské
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Průměrně	průměrně	k6eAd1	průměrně
velký	velký	k2eAgInSc1d1	velký
zárodek	zárodek	k1gInSc1	zárodek
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
8	[number]	k4	8
<g/>
.	.	kIx.	.
a	a	k8xC	a
9	[number]	k4	9
<g/>
.	.	kIx.	.
týdne	týden	k1gInSc2	týden
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
mu	on	k3xPp3gMnSc3	on
začínáme	začínat	k5eAaImIp1nP	začínat
říkat	říkat	k5eAaImF	říkat
plod	plod	k1gInSc4	plod
<g/>
,	,	kIx,	,
váží	vážit	k5eAaImIp3nS	vážit
9	[number]	k4	9
gramů	gram	k1gInPc2	gram
a	a	k8xC	a
měří	měřit	k5eAaImIp3nS	měřit
5	[number]	k4	5
centimetrů	centimetr	k1gInPc2	centimetr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Kompletně	kompletně	k6eAd1	kompletně
rozvinutý	rozvinutý	k2eAgInSc1d1	rozvinutý
lidský	lidský	k2eAgInSc1d1	lidský
mozek	mozek	k1gInSc1	mozek
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
85	[number]	k4	85
až	až	k9	až
100	[number]	k4	100
miliard	miliarda	k4xCgFnPc2	miliarda
navzájem	navzájem	k6eAd1	navzájem
složitě	složitě	k6eAd1	složitě
pospojovaných	pospojovaný	k2eAgFnPc2d1	pospojovaná
mozkových	mozkový	k2eAgFnPc2d1	mozková
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Převážná	převážný	k2eAgFnSc1d1	převážná
část	část	k1gFnSc1	část
růstu	růst	k1gInSc2	růst
mozku	mozek	k1gInSc2	mozek
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
během	během	k7c2	během
prvních	první	k4xOgInPc2	první
tří	tři	k4xCgInPc2	tři
měsíců	měsíc	k1gInPc2	měsíc
nitroděložního	nitroděložní	k2eAgInSc2d1	nitroděložní
života	život	k1gInSc2	život
a	a	k8xC	a
při	při	k7c6	při
narození	narození	k1gNnSc6	narození
je	být	k5eAaImIp3nS	být
počet	počet	k1gInSc1	počet
mozkových	mozkový	k2eAgFnPc2d1	mozková
buněk	buňka	k1gFnPc2	buňka
už	už	k6eAd1	už
konečný	konečný	k2eAgInSc1d1	konečný
<g/>
.	.	kIx.	.
</s>
<s>
Mozek	mozek	k1gInSc1	mozek
se	se	k3xPyFc4	se
však	však	k9	však
dále	daleko	k6eAd2	daleko
poněkud	poněkud	k6eAd1	poněkud
zvětšuje	zvětšovat	k5eAaImIp3nS	zvětšovat
a	a	k8xC	a
zejména	zejména	k9	zejména
zraje	zrát	k5eAaImIp3nS	zrát
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mozkové	mozkový	k2eAgFnPc1d1	mozková
buňky	buňka	k1gFnPc1	buňka
postupně	postupně	k6eAd1	postupně
zdokonalují	zdokonalovat	k5eAaImIp3nP	zdokonalovat
svoje	svůj	k3xOyFgNnSc4	svůj
uspořádání	uspořádání	k1gNnSc4	uspořádání
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
svoji	svůj	k3xOyFgFnSc4	svůj
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Mozkovna	mozkovna	k1gFnSc1	mozkovna
novorozeného	novorozený	k2eAgNnSc2d1	novorozené
dítěte	dítě	k1gNnSc2	dítě
již	již	k6eAd1	již
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
tří	tři	k4xCgFnPc2	tři
čtvrtin	čtvrtina	k1gFnPc2	čtvrtina
své	svůj	k3xOyFgFnSc2	svůj
budoucí	budoucí	k2eAgFnSc2d1	budoucí
dospělé	dospělý	k2eAgFnSc2d1	dospělá
velikosti	velikost	k1gFnSc2	velikost
a	a	k8xC	a
hlavička	hlavička	k1gFnSc1	hlavička
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
odrazem	odraz	k1gInSc7	odraz
velikosti	velikost	k1gFnSc2	velikost
mozku	mozek	k1gInSc2	mozek
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nP	tvořit
celou	celý	k2eAgFnSc4d1	celá
čtvrtinu	čtvrtina	k1gFnSc4	čtvrtina
délky	délka	k1gFnSc2	délka
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
poměr	poměr	k1gInSc1	poměr
se	se	k3xPyFc4	se
během	během	k7c2	během
dalšího	další	k2eAgInSc2d1	další
růstu	růst	k1gInSc2	růst
a	a	k8xC	a
vývoje	vývoj	k1gInSc2	vývoj
podstatně	podstatně	k6eAd1	podstatně
mění	měnit	k5eAaImIp3nS	měnit
a	a	k8xC	a
ve	v	k7c6	v
25	[number]	k4	25
letech	léto	k1gNnPc6	léto
tvoří	tvořit	k5eAaImIp3nS	tvořit
hlava	hlava	k1gFnSc1	hlava
už	už	k6eAd1	už
pouhou	pouhý	k2eAgFnSc4d1	pouhá
osminu	osmina	k1gFnSc4	osmina
tělesné	tělesný	k2eAgFnSc2d1	tělesná
výšky	výška	k1gFnSc2	výška
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
narození	narození	k1gNnSc6	narození
tedy	tedy	k9	tedy
roste	růst	k5eAaImIp3nS	růst
zbytek	zbytek	k1gInSc1	zbytek
těla	tělo	k1gNnSc2	tělo
o	o	k7c4	o
poznání	poznání	k1gNnSc4	poznání
více	hodně	k6eAd2	hodně
než	než	k8xS	než
hlava	hlava	k1gFnSc1	hlava
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
mozek	mozek	k1gInSc1	mozek
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
rodiče	rodič	k1gMnPc4	rodič
je	být	k5eAaImIp3nS	být
jistě	jistě	k9	jistě
známá	známý	k2eAgFnSc1d1	známá
zkušenost	zkušenost	k1gFnSc1	zkušenost
<g/>
,	,	kIx,	,
že	že	k8xS	že
dítě	dítě	k1gNnSc1	dítě
vyroste	vyrůst	k5eAaPmIp3nS	vyrůst
mnohem	mnohem	k6eAd1	mnohem
dříve	dříve	k6eAd2	dříve
z	z	k7c2	z
kalhot	kalhoty	k1gFnPc2	kalhoty
než	než	k8xS	než
z	z	k7c2	z
čepice	čepice	k1gFnSc2	čepice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
druhém	druhý	k4xOgInSc6	druhý
roce	rok	k1gInSc6	rok
věku	věk	k1gInSc2	věk
se	se	k3xPyFc4	se
již	již	k6eAd1	již
obvod	obvod	k1gInSc1	obvod
hlavy	hlava	k1gFnSc2	hlava
zvětšuje	zvětšovat	k5eAaImIp3nS	zvětšovat
jen	jen	k9	jen
málo	málo	k6eAd1	málo
<g/>
,	,	kIx,	,
nejvýše	vysoce	k6eAd3	vysoce
o	o	k7c4	o
2	[number]	k4	2
cm	cm	kA	cm
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
7	[number]	k4	7
letech	léto	k1gNnPc6	léto
je	být	k5eAaImIp3nS	být
růst	růst	k1gInSc1	růst
mozku	mozek	k1gInSc2	mozek
ukončen	ukončit	k5eAaPmNgInS	ukončit
z	z	k7c2	z
90	[number]	k4	90
%	%	kIx~	%
a	a	k8xC	a
v	v	k7c6	v
10	[number]	k4	10
letech	léto	k1gNnPc6	léto
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
mozek	mozek	k1gInSc1	mozek
hmotnosti	hmotnost	k1gFnSc2	hmotnost
mozku	mozek	k1gInSc2	mozek
dospělého	dospělý	k2eAgMnSc2d1	dospělý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Důležitá	důležitý	k2eAgFnSc1d1	důležitá
část	část	k1gFnSc1	část
zrání	zrání	k1gNnSc2	zrání
mozku	mozek	k1gInSc2	mozek
po	po	k7c6	po
narození	narození	k1gNnSc6	narození
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
během	během	k7c2	během
prvních	první	k4xOgInPc2	první
čtyř	čtyři	k4xCgInPc2	čtyři
měsíců	měsíc	k1gInPc2	měsíc
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
němu	on	k3xPp3gInSc3	on
postupně	postupně	k6eAd1	postupně
mizejí	mizet	k5eAaImIp3nP	mizet
reflexy	reflex	k1gInPc1	reflex
typické	typický	k2eAgInPc1d1	typický
pro	pro	k7c4	pro
novorozenecké	novorozenecký	k2eAgNnSc4d1	novorozenecké
období	období	k1gNnSc4	období
<g/>
,	,	kIx,	,
dítě	dítě	k1gNnSc4	dítě
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
čilejší	čilý	k2eAgFnSc1d2	čilejší
a	a	k8xC	a
více	hodně	k6eAd2	hodně
reaguje	reagovat	k5eAaBmIp3nS	reagovat
na	na	k7c6	na
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Kojenec	kojenec	k1gMnSc1	kojenec
se	se	k3xPyFc4	se
již	již	k6eAd1	již
snaží	snažit	k5eAaImIp3nS	snažit
uchopovat	uchopovat	k5eAaImF	uchopovat
předměty	předmět	k1gInPc4	předmět
<g/>
,	,	kIx,	,
usmívá	usmívat	k5eAaImIp3nS	usmívat
se	se	k3xPyFc4	se
a	a	k8xC	a
směje	smát	k5eAaImIp3nS	smát
se	se	k3xPyFc4	se
i	i	k9	i
hlasitě	hlasitě	k6eAd1	hlasitě
<g/>
,	,	kIx,	,
očekává	očekávat	k5eAaImIp3nS	očekávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
dostane	dostat	k5eAaPmIp3nS	dostat
jídlo	jídlo	k1gNnSc1	jídlo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
dohled	dohled	k1gInSc4	dohled
<g/>
,	,	kIx,	,
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
začíná	začínat	k5eAaImIp3nS	začínat
sedět	sedět	k5eAaImF	sedět
s	s	k7c7	s
oporou	opora	k1gFnSc7	opora
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
zrání	zrání	k1gNnSc4	zrání
a	a	k8xC	a
vývoj	vývoj	k1gInSc4	vývoj
mozku	mozek	k1gInSc2	mozek
velmi	velmi	k6eAd1	velmi
těsně	těsně	k6eAd1	těsně
navazuje	navazovat	k5eAaImIp3nS	navazovat
jak	jak	k6eAd1	jak
rozvoj	rozvoj	k1gInSc4	rozvoj
psychiky	psychika	k1gFnSc2	psychika
a	a	k8xC	a
duševních	duševní	k2eAgFnPc2d1	duševní
schopností	schopnost	k1gFnPc2	schopnost
dítěte	dítě	k1gNnSc2	dítě
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
jeho	jeho	k3xOp3gFnSc4	jeho
obratnost	obratnost	k1gFnSc4	obratnost
<g/>
,	,	kIx,	,
schopnost	schopnost	k1gFnSc4	schopnost
pohybové	pohybový	k2eAgFnSc2d1	pohybová
koordinace	koordinace	k1gFnSc2	koordinace
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
jemná	jemný	k2eAgFnSc1d1	jemná
motorika	motorik	k1gMnSc2	motorik
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Postupný	postupný	k2eAgInSc4d1	postupný
rozvoj	rozvoj	k1gInSc4	rozvoj
obratnosti	obratnost	k1gFnSc2	obratnost
můžeme	moct	k5eAaImIp1nP	moct
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
roce	rok	k1gInSc6	rok
života	život	k1gInSc2	život
dobře	dobře	k6eAd1	dobře
pozorovat	pozorovat	k5eAaImF	pozorovat
například	například	k6eAd1	například
na	na	k7c6	na
způsobu	způsob	k1gInSc6	způsob
<g/>
,	,	kIx,	,
jakým	jaký	k3yRgInSc7	jaký
dítě	dítě	k1gNnSc1	dítě
uchopí	uchopit	k5eAaPmIp3nP	uchopit
rukou	ruka	k1gFnSc7	ruka
předmět	předmět	k1gInSc4	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Schopnost	schopnost	k1gFnSc1	schopnost
úchopu	úchop	k1gInSc2	úchop
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
obvykle	obvykle	k6eAd1	obvykle
ve	v	k7c6	v
3-4	[number]	k4	3-4
měsících	měsíc	k1gInPc6	měsíc
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
nová	nový	k2eAgFnSc1d1	nová
dovednost	dovednost	k1gFnSc1	dovednost
přináší	přinášet	k5eAaImIp3nS	přinášet
dítěti	dítě	k1gNnSc3	dítě
mnoho	mnoho	k6eAd1	mnoho
radosti	radost	k1gFnSc3	radost
a	a	k8xC	a
potěšení	potěšení	k1gNnSc4	potěšení
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
začíná	začínat	k5eAaImIp3nS	začínat
uchopovat	uchopovat	k5eAaImF	uchopovat
předměty	předmět	k1gInPc4	předmět
celou	celý	k2eAgFnSc7d1	celá
dlaní	dlaň	k1gFnSc7	dlaň
<g/>
.	.	kIx.	.
</s>
<s>
Palec	palec	k1gInSc1	palec
jako	jako	k8xC	jako
součást	součást	k1gFnSc1	součást
dlaně	dlaň	k1gFnSc2	dlaň
se	se	k3xPyFc4	se
do	do	k7c2	do
úchopu	úchop	k1gInSc2	úchop
zapojí	zapojit	k5eAaPmIp3nS	zapojit
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
pátém	pátý	k4xOgInSc6	pátý
měsíci	měsíc	k1gInSc6	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sedmém	sedmý	k4xOgInSc6	sedmý
měsíci	měsíc	k1gInSc6	měsíc
začne	začít	k5eAaPmIp3nS	začít
pomáhat	pomáhat	k5eAaImF	pomáhat
dítěti	dítě	k1gNnSc3	dítě
v	v	k7c6	v
úchopu	úchop	k1gInSc6	úchop
některých	některý	k3yIgMnPc2	některý
předmětů	předmět	k1gInPc2	předmět
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
kostky	kostka	k1gFnPc1	kostka
<g/>
,	,	kIx,	,
palec	palec	k1gInSc1	palec
otočený	otočený	k2eAgInSc1d1	otočený
proti	proti	k7c3	proti
ostatním	ostatní	k2eAgInPc3d1	ostatní
prstům	prst	k1gInPc3	prst
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
v	v	k7c6	v
9	[number]	k4	9
měsících	měsíc	k1gInPc6	měsíc
umí	umět	k5eAaImIp3nP	umět
dítě	dítě	k1gNnSc4	dítě
využít	využít	k5eAaPmF	využít
pro	pro	k7c4	pro
uchopení	uchopení	k1gNnSc4	uchopení
drobtů	drobet	k1gInPc2	drobet
nebo	nebo	k8xC	nebo
malých	malý	k2eAgFnPc2d1	malá
kuliček	kulička	k1gFnPc2	kulička
také	také	k9	také
jemný	jemný	k2eAgInSc4d1	jemný
klíšťkový	klíšťkový	k2eAgInSc4d1	klíšťkový
úchop	úchop	k1gInSc4	úchop
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
ostatních	ostatní	k2eAgFnPc6d1	ostatní
dovednostech	dovednost	k1gFnPc6	dovednost
<g/>
,	,	kIx,	,
i	i	k8xC	i
zde	zde	k6eAd1	zde
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
každé	každý	k3xTgNnSc1	každý
dítě	dítě	k1gNnSc1	dítě
je	být	k5eAaImIp3nS	být
samostatným	samostatný	k2eAgNnSc7d1	samostatné
individuem	individuum	k1gNnSc7	individuum
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
vývoj	vývoj	k1gInSc1	vývoj
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
od	od	k7c2	od
uvedených	uvedený	k2eAgInPc2d1	uvedený
údajů	údaj	k1gInPc2	údaj
poněkud	poněkud	k6eAd1	poněkud
lišit	lišit	k5eAaImF	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Zmiňujeme	zmiňovat	k5eAaImIp1nP	zmiňovat
data	datum	k1gNnPc4	datum
průměrná	průměrný	k2eAgNnPc4d1	průměrné
<g/>
,	,	kIx,	,
zjištěná	zjištěný	k2eAgNnPc4d1	zjištěné
při	při	k7c6	při
hodnocení	hodnocení	k1gNnSc6	hodnocení
velkých	velký	k2eAgFnPc2d1	velká
skupin	skupina	k1gFnPc2	skupina
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
objevování	objevování	k1gNnSc4	objevování
okolí	okolí	k1gNnSc2	okolí
má	mít	k5eAaImIp3nS	mít
pro	pro	k7c4	pro
dítě	dítě	k1gNnSc4	dítě
obrovský	obrovský	k2eAgInSc1d1	obrovský
význam	význam	k1gInSc1	význam
celkový	celkový	k2eAgInSc1d1	celkový
pohybový	pohybový	k2eAgInSc1d1	pohybový
vývoj	vývoj	k1gInSc1	vývoj
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
hrubá	hrubá	k1gFnSc1	hrubá
motorika	motorik	k1gMnSc2	motorik
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dítě	dítě	k1gNnSc1	dítě
obvykle	obvykle	k6eAd1	obvykle
začne	začít	k5eAaPmIp3nS	začít
samostatně	samostatně	k6eAd1	samostatně
sedět	sedět	k5eAaImF	sedět
mezi	mezi	k7c7	mezi
6	[number]	k4	6
<g/>
.	.	kIx.	.
a	a	k8xC	a
8	[number]	k4	8
<g/>
.	.	kIx.	.
měsícem	měsíc	k1gInSc7	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
chvíle	chvíle	k1gFnSc2	chvíle
může	moct	k5eAaImIp3nS	moct
pozorovat	pozorovat	k5eAaImF	pozorovat
bez	bez	k7c2	bez
omezení	omezení	k1gNnPc2	omezení
dění	dění	k1gNnSc2	dění
okolo	okolo	k6eAd1	okolo
sebe	sebe	k3xPyFc4	sebe
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
obzor	obzor	k1gInSc1	obzor
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
jednoho	jeden	k4xCgInSc2	jeden
roku	rok	k1gInSc2	rok
začíná	začínat	k5eAaImIp3nS	začínat
samostatně	samostatně	k6eAd1	samostatně
chodit	chodit	k5eAaImF	chodit
(	(	kIx(	(
<g/>
s	s	k7c7	s
rozmezím	rozmezí	k1gNnSc7	rozmezí
u	u	k7c2	u
zdravých	zdravý	k2eAgFnPc2d1	zdravá
dětí	dítě	k1gFnPc2	dítě
mezi	mezi	k7c7	mezi
9	[number]	k4	9
a	a	k8xC	a
17	[number]	k4	17
měsíci	měsíc	k1gInPc7	měsíc
věku	věk	k1gInSc6	věk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
samostatné	samostatný	k2eAgFnSc3d1	samostatná
chůzi	chůze	k1gFnSc3	chůze
může	moct	k5eAaImIp3nS	moct
dítě	dítě	k1gNnSc1	dítě
rovnýma	rovný	k2eAgFnPc7d1	rovná
nohama	noha	k1gFnPc7	noha
vstoupit	vstoupit	k5eAaPmF	vstoupit
do	do	k7c2	do
druhého	druhý	k4xOgInSc2	druhý
roku	rok	k1gInSc2	rok
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
hlavní	hlavní	k2eAgFnSc7d1	hlavní
náplní	náplň	k1gFnSc7	náplň
je	být	k5eAaImIp3nS	být
postupné	postupný	k2eAgNnSc1d1	postupné
získávání	získávání	k1gNnSc1	získávání
nezávislosti	nezávislost	k1gFnSc2	nezávislost
a	a	k8xC	a
samostatné	samostatný	k2eAgNnSc1d1	samostatné
objevování	objevování	k1gNnSc1	objevování
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Mezi	mezi	k7c4	mezi
5	[number]	k4	5
<g/>
.	.	kIx.	.
a	a	k8xC	a
7	[number]	k4	7
<g/>
.	.	kIx.	.
rokem	rok	k1gInSc7	rok
života	život	k1gInSc2	život
již	již	k6eAd1	již
dětský	dětský	k2eAgInSc1d1	dětský
mozek	mozek	k1gInSc1	mozek
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
90	[number]	k4	90
%	%	kIx~	%
své	svůj	k3xOyFgFnSc2	svůj
dospělé	dospělý	k2eAgFnSc2d1	dospělá
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
6	[number]	k4	6
letech	léto	k1gNnPc6	léto
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
poslednímu	poslední	k2eAgInSc3d1	poslední
významnému	významný	k2eAgInSc3d1	významný
kroku	krok	k1gInSc3	krok
v	v	k7c6	v
přestavbě	přestavba	k1gFnSc6	přestavba
mozkové	mozkový	k2eAgFnSc2d1	mozková
kůry	kůra	k1gFnSc2	kůra
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
ní	on	k3xPp3gFnSc3	on
vyzrává	vyzrávat	k5eAaImIp3nS	vyzrávat
jemná	jemný	k2eAgFnSc1d1	jemná
senzori-motorická	senzoriotorický	k2eAgFnSc1d1	senzori-motorický
koordinace	koordinace	k1gFnSc1	koordinace
(	(	kIx(	(
<g/>
vztah	vztah	k1gInSc1	vztah
mezi	mezi	k7c7	mezi
smyslovým	smyslový	k2eAgInSc7d1	smyslový
vjemem	vjem	k1gInSc7	vjem
a	a	k8xC	a
jemnou	jemný	k2eAgFnSc7d1	jemná
pohybovou	pohybový	k2eAgFnSc7d1	pohybová
odpovědí	odpověď	k1gFnSc7	odpověď
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
významně	významně	k6eAd1	významně
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
při	při	k7c6	při
zvládnutí	zvládnutí	k1gNnSc6	zvládnutí
školních	školní	k2eAgFnPc2d1	školní
dovedností	dovednost	k1gFnPc2	dovednost
-	-	kIx~	-
jak	jak	k8xS	jak
při	při	k7c6	při
práci	práce	k1gFnSc6	práce
s	s	k7c7	s
tužkou	tužka	k1gFnSc7	tužka
a	a	k8xC	a
papírem	papír	k1gInSc7	papír
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
při	při	k7c6	při
psaní	psaní	k1gNnSc6	psaní
a	a	k8xC	a
kreslení	kreslení	k1gNnSc6	kreslení
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
při	při	k7c6	při
tělocviku	tělocvik	k1gInSc6	tělocvik
a	a	k8xC	a
sportu	sport	k1gInSc6	sport
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mozek	mozek	k1gInSc1	mozek
dospělého	dospělý	k2eAgMnSc2d1	dospělý
člověka	člověk	k1gMnSc2	člověk
ukrývá	ukrývat	k5eAaImIp3nS	ukrývat
85	[number]	k4	85
až	až	k9	až
100	[number]	k4	100
miliard	miliarda	k4xCgFnPc2	miliarda
nervových	nervový	k2eAgFnPc2d1	nervová
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ještě	ještě	k6eAd1	ještě
na	na	k7c6	na
konci	konec	k1gInSc6	konec
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	stoletý	k2eAgMnPc1d1	stoletý
někteří	některý	k3yIgMnPc1	některý
odborníci	odborník	k1gMnPc1	odborník
věřili	věřit	k5eAaImAgMnP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
dosažení	dosažení	k1gNnSc6	dosažení
dospělosti	dospělost	k1gFnSc2	dospělost
už	už	k6eAd1	už
lidský	lidský	k2eAgInSc1d1	lidský
mozek	mozek	k1gInSc1	mozek
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
více	hodně	k6eAd2	hodně
produkovat	produkovat	k5eAaImF	produkovat
nové	nový	k2eAgInPc4d1	nový
neurony	neuron	k1gInPc4	neuron
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Dnes	dnes	k6eAd1	dnes
však	však	k9	však
vědci	vědec	k1gMnPc1	vědec
obecně	obecně	k6eAd1	obecně
zastávají	zastávat	k5eAaImIp3nP	zastávat
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
mozek	mozek	k1gInSc1	mozek
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
procesu	proces	k1gInSc2	proces
neurogeneze	neurogeneze	k1gFnSc2	neurogeneze
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
vznik	vznik	k1gInSc1	vznik
nových	nový	k2eAgFnPc2d1	nová
mozkových	mozkový	k2eAgFnPc2d1	mozková
buněk	buňka	k1gFnPc2	buňka
<g/>
)	)	kIx)	)
i	i	k9	i
nadále	nadále	k6eAd1	nadále
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
nové	nový	k2eAgFnPc4d1	nová
nervové	nervový	k2eAgFnPc4d1	nervová
buňky	buňka	k1gFnPc4	buňka
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
dokonce	dokonce	k9	dokonce
i	i	k9	i
ve	v	k7c6	v
starším	starý	k2eAgInSc6d2	starší
věku	věk	k1gInSc6	věk
<g/>
.	.	kIx.	.
</s>
<s>
Výzkumy	výzkum	k1gInPc1	výzkum
ukázaly	ukázat	k5eAaPmAgInP	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
tvorbě	tvorba	k1gFnSc3	tvorba
nových	nový	k2eAgInPc2d1	nový
neuronů	neuron	k1gInPc2	neuron
dochází	docházet	k5eAaImIp3nS	docházet
nejvíce	nejvíce	k6eAd1	nejvíce
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
částech	část	k1gFnPc6	část
mozku	mozek	k1gInSc2	mozek
–	–	k?	–
v	v	k7c6	v
subventrikulární	subventrikulární	k2eAgFnSc6d1	subventrikulární
oblasti	oblast	k1gFnSc6	oblast
a	a	k8xC	a
v	v	k7c6	v
části	část	k1gFnSc6	část
hipokampus	hipokampus	k1gInSc4	hipokampus
<g/>
.	.	kIx.	.
</s>
<s>
Hipokampus	Hipokampus	k1gInSc1	Hipokampus
je	být	k5eAaImIp3nS	být
zodpovědný	zodpovědný	k2eAgInSc1d1	zodpovědný
za	za	k7c4	za
schopnost	schopnost	k1gFnSc4	schopnost
učení	učení	k1gNnSc2	učení
a	a	k8xC	a
paměti	paměť	k1gFnSc2	paměť
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Když	když	k8xS	když
nefunguje	fungovat	k5eNaImIp3nS	fungovat
správně	správně	k6eAd1	správně
<g/>
,	,	kIx,	,
vznikají	vznikat	k5eAaImIp3nP	vznikat
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
více	hodně	k6eAd2	hodně
projevují	projevovat	k5eAaImIp3nP	projevovat
onemocnění	onemocnění	k1gNnSc4	onemocnění
jako	jako	k8xC	jako
například	například	k6eAd1	například
deprese	deprese	k1gFnSc1	deprese
<g/>
,	,	kIx,	,
úzkost	úzkost	k1gFnSc1	úzkost
či	či	k8xC	či
Parkinsonova	Parkinsonův	k2eAgFnSc1d1	Parkinsonova
choroba	choroba	k1gFnSc1	choroba
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
však	však	k9	však
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
případech	případ	k1gInPc6	případ
neurodegenerativní	urodegenerativní	k2eNgInSc4d1	urodegenerativní
charakter	charakter	k1gInSc4	charakter
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
V	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
si	se	k3xPyFc3	se
vědecký	vědecký	k2eAgInSc1d1	vědecký
tým	tým	k1gInSc1	tým
Josepha	Joseph	k1gMnSc2	Joseph
Altmana	Altman	k1gMnSc2	Altman
všiml	všimnout	k5eAaPmAgMnS	všimnout
buněčné	buněčný	k2eAgFnSc2d1	buněčná
proliferace	proliferace	k1gFnSc2	proliferace
(	(	kIx(	(
<g/>
neuronové	neuronový	k2eAgFnSc2d1	neuronová
novotvorby	novotvorba	k1gFnSc2	novotvorba
<g/>
)	)	kIx)	)
u	u	k7c2	u
potkanů	potkan	k1gMnPc2	potkan
<g/>
.	.	kIx.	.
</s>
<s>
Nikdo	nikdo	k3yNnSc1	nikdo
mu	on	k3xPp3gMnSc3	on
ale	ale	k8xC	ale
nevěřil	věřit	k5eNaImAgMnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
něco	něco	k3yInSc1	něco
podobného	podobný	k2eAgNnSc2d1	podobné
děje	dít	k5eAaImIp3nS	dít
i	i	k8xC	i
v	v	k7c6	v
mozku	mozek	k1gInSc6	mozek
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
přišel	přijít	k5eAaPmAgMnS	přijít
švédský	švédský	k2eAgMnSc1d1	švédský
badatel	badatel	k1gMnSc1	badatel
Peter	Peter	k1gMnSc1	Peter
S.	S.	kA	S.
Eriksson	Eriksson	k1gMnSc1	Eriksson
s	s	k7c7	s
důkazy	důkaz	k1gInPc7	důkaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
neurogeneze	neurogeneze	k1gFnSc1	neurogeneze
je	být	k5eAaImIp3nS	být
skutečností	skutečnost	k1gFnSc7	skutečnost
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
výzkumy	výzkum	k1gInPc1	výzkum
hlásí	hlásit	k5eAaImIp3nP	hlásit
vznik	vznik	k1gInSc4	vznik
přibližně	přibližně	k6eAd1	přibližně
700	[number]	k4	700
nových	nový	k2eAgFnPc2d1	nová
mozkových	mozkový	k2eAgFnPc2d1	mozková
buněk	buňka	k1gFnPc2	buňka
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
...	...	k?	...
</s>
</p>
<p>
<s>
Neurovědkyně	Neurovědkyně	k1gFnSc1	Neurovědkyně
Sandrine	Sandrin	k1gInSc5	Sandrin
Thuret	Thuret	k1gMnSc1	Thuret
z	z	k7c2	z
londýnské	londýnský	k2eAgFnSc2d1	londýnská
King	King	k1gMnSc1	King
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
College	College	k1gFnSc7	College
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
padesátiletý	padesátiletý	k2eAgMnSc1d1	padesátiletý
člověk	člověk	k1gMnSc1	člověk
nemá	mít	k5eNaImIp3nS	mít
v	v	k7c6	v
mozku	mozek	k1gInSc6	mozek
ani	ani	k8xC	ani
jeden	jeden	k4xCgInSc4	jeden
neuron	neuron	k1gInSc4	neuron
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
stejné	stejný	k2eAgNnSc4d1	stejné
datum	datum	k1gNnSc4	datum
narození	narození	k1gNnSc2	narození
jako	jako	k8xS	jako
jeho	jeho	k3xOp3gMnSc1	jeho
majitel	majitel	k1gMnSc1	majitel
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
do	do	k7c2	do
jednoho	jeden	k4xCgInSc2	jeden
se	se	k3xPyFc4	se
už	už	k6eAd1	už
vyměnily	vyměnit	k5eAaPmAgFnP	vyměnit
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgFnPc1d1	Nové
nervové	nervový	k2eAgFnPc1d1	nervová
buňky	buňka	k1gFnPc1	buňka
hrají	hrát	k5eAaImIp3nP	hrát
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
v	v	k7c6	v
učení	učení	k1gNnSc6	učení
dospělých	dospělí	k1gMnPc2	dospělí
<g/>
.	.	kIx.	.
</s>
<s>
Doktorka	doktorka	k1gFnSc1	doktorka
Thuret	Thureta	k1gFnPc2	Thureta
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
týmem	tým	k1gInSc7	tým
v	v	k7c6	v
laboratoři	laboratoř	k1gFnSc6	laboratoř
zjistila	zjistit	k5eAaPmAgFnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zabránění	zabránění	k1gNnSc4	zabránění
neurogenezi	neurogeneze	k1gFnSc4	neurogeneze
v	v	k7c6	v
dospělém	dospělý	k2eAgInSc6d1	dospělý
mozku	mozek	k1gInSc6	mozek
vede	vést	k5eAaImIp3nS	vést
ke	k	k7c3	k
zhoršení	zhoršení	k1gNnSc3	zhoršení
paměti	paměť	k1gFnSc2	paměť
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgInPc1d1	Nové
neurony	neuron	k1gInPc1	neuron
mají	mít	k5eAaImIp3nP	mít
také	také	k9	také
významný	významný	k2eAgInSc4d1	významný
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
naši	náš	k3xOp1gFnSc4	náš
náladu	nálada	k1gFnSc4	nálada
<g/>
.	.	kIx.	.
</s>
<s>
Thuret	Thuret	k1gMnSc1	Thuret
také	také	k9	také
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
týmem	tým	k1gInSc7	tým
zjistila	zjistit	k5eAaPmAgFnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
zastaví	zastavit	k5eAaPmIp3nS	zastavit
tvorba	tvorba	k1gFnSc1	tvorba
nových	nový	k2eAgFnPc2d1	nová
mozkových	mozkový	k2eAgFnPc2d1	mozková
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
dostane	dostat	k5eAaPmIp3nS	dostat
se	se	k3xPyFc4	se
ke	k	k7c3	k
slovu	slovo	k1gNnSc3	slovo
deprese	deprese	k1gFnSc2	deprese
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
přetrvávající	přetrvávající	k2eAgMnSc1d1	přetrvávající
depresivní	depresivní	k2eAgNnSc4d1	depresivní
ladění	ladění	k1gNnSc4	ladění
u	u	k7c2	u
pacientů	pacient	k1gMnPc2	pacient
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
zvítězili	zvítězit	k5eAaPmAgMnP	zvítězit
nad	nad	k7c7	nad
rakovinou	rakovina	k1gFnSc7	rakovina
<g/>
.	.	kIx.	.
</s>
<s>
Vedlejším	vedlejší	k2eAgInSc7d1	vedlejší
efektem	efekt	k1gInSc7	efekt
náročné	náročný	k2eAgFnSc2d1	náročná
onkologické	onkologický	k2eAgFnSc2d1	onkologická
léčby	léčba	k1gFnSc2	léčba
je	být	k5eAaImIp3nS	být
totiž	totiž	k9	totiž
dočasná	dočasný	k2eAgFnSc1d1	dočasná
stopka	stopka	k1gFnSc1	stopka
neurogenezi	neurogeneze	k1gFnSc4	neurogeneze
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
==	==	k?	==
Nemoci	nemoc	k1gFnSc3	nemoc
mozku	mozek	k1gInSc2	mozek
==	==	k?	==
</s>
</p>
<p>
<s>
Infekční	infekční	k2eAgNnSc4d1	infekční
onemocnění	onemocnění	k1gNnSc4	onemocnění
–	–	k?	–
Zánět	zánět	k1gInSc1	zánět
mozkových	mozkový	k2eAgFnPc2d1	mozková
blan	blána	k1gFnPc2	blána
a	a	k8xC	a
mozku	mozek	k1gInSc2	mozek
</s>
</p>
<p>
<s>
Degenerativní	degenerativní	k2eAgNnSc1d1	degenerativní
onemocnění	onemocnění	k1gNnSc1	onemocnění
mozku	mozek	k1gInSc2	mozek
–	–	k?	–
Alzheimerova	Alzheimerův	k2eAgFnSc1d1	Alzheimerova
choroba	choroba	k1gFnSc1	choroba
(	(	kIx(	(
<g/>
degenerativní	degenerativní	k2eAgNnSc1d1	degenerativní
onemocnění	onemocnění	k1gNnSc1	onemocnění
mozku	mozek	k1gInSc2	mozek
–	–	k?	–
zánik	zánik	k1gInSc1	zánik
některých	některý	k3yIgFnPc2	některý
mozkových	mozkový	k2eAgFnPc2d1	mozková
buněk	buňka	k1gFnPc2	buňka
<g/>
)	)	kIx)	)
a	a	k8xC	a
Parkinsonova	Parkinsonův	k2eAgFnSc1d1	Parkinsonova
choroba	choroba	k1gFnSc1	choroba
porucha	porucha	k1gFnSc1	porucha
pohybu	pohyb	k1gInSc2	pohyb
způsobená	způsobený	k2eAgFnSc1d1	způsobená
změnami	změna	k1gFnPc7	změna
v	v	k7c6	v
distribuci	distribuce	k1gFnSc6	distribuce
důležitého	důležitý	k2eAgInSc2d1	důležitý
mediátoru	mediátor	k1gInSc2	mediátor
dopaminu	dopamin	k1gInSc2	dopamin
</s>
</p>
<p>
<s>
Degenerativní	degenerativní	k2eAgNnSc1d1	degenerativní
onemocnění	onemocnění	k1gNnSc1	onemocnění
nervů	nerv	k1gInPc2	nerv
–	–	k?	–
roztroušená	roztroušený	k2eAgFnSc1d1	roztroušená
skleróza	skleróza	k1gFnSc1	skleróza
(	(	kIx(	(
<g/>
autoimunní	autoimunný	k2eAgMnPc1d1	autoimunný
postižení	postižený	k1gMnPc1	postižený
nervových	nervový	k2eAgInPc2d1	nervový
obalů	obal	k1gInPc2	obal
–	–	k?	–
(	(	kIx(	(
<g/>
porucha	porucha	k1gFnSc1	porucha
koordinace	koordinace	k1gFnSc2	koordinace
pohybů	pohyb	k1gInPc2	pohyb
<g/>
,	,	kIx,	,
řeči	řeč	k1gFnSc2	řeč
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Amyotrofická	amyotrofický	k2eAgFnSc1d1	amyotrofická
laterální	laterální	k2eAgFnSc1d1	laterální
skleróza	skleróza	k1gFnSc1	skleróza
degenerace	degenerace	k1gFnSc2	degenerace
nervových	nervový	k2eAgInPc2d1	nervový
kořenů	kořen	k1gInPc2	kořen
s	s	k7c7	s
nejasnou	jasný	k2eNgFnSc7d1	nejasná
příčinou	příčina	k1gFnSc7	příčina
(	(	kIx(	(
<g/>
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
rozvíjející	rozvíjející	k2eAgFnSc1d1	rozvíjející
porucha	porucha	k1gFnSc1	porucha
koordinace	koordinace	k1gFnSc2	koordinace
pohybů	pohyb	k1gInPc2	pohyb
až	až	k9	až
smrt	smrt	k1gFnSc4	smrt
na	na	k7c4	na
obrnu	obrna	k1gFnSc4	obrna
dýchacích	dýchací	k2eAgInPc2d1	dýchací
svalů	sval	k1gInPc2	sval
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mozková	mozkový	k2eAgFnSc1d1	mozková
mrtvice	mrtvice	k1gFnSc1	mrtvice
–	–	k?	–
poškození	poškození	k1gNnSc2	poškození
mozkové	mozkový	k2eAgFnSc2d1	mozková
tkáně	tkáň	k1gFnSc2	tkáň
krvácením	krvácení	k1gNnSc7	krvácení
nebo	nebo	k8xC	nebo
nedokrvením	nedokrvení	k1gNnSc7	nedokrvení
<g/>
;	;	kIx,	;
mezi	mezi	k7c4	mezi
její	její	k3xOp3gInPc4	její
následky	následek	k1gInPc4	následek
patří	patřit	k5eAaImIp3nP	patřit
zejména	zejména	k9	zejména
trvalé	trvalý	k2eAgFnPc1d1	trvalá
poruchy	porucha	k1gFnPc1	porucha
pohyblivosti	pohyblivost	k1gFnSc2	pohyblivost
</s>
</p>
<p>
<s>
Migréna	migréna	k1gFnSc1	migréna
–	–	k?	–
porucha	porucha	k1gFnSc1	porucha
cévního	cévní	k2eAgNnSc2d1	cévní
zásobení	zásobení	k1gNnSc2	zásobení
způsobující	způsobující	k2eAgFnSc2d1	způsobující
intenzivní	intenzivní	k2eAgFnSc2d1	intenzivní
bolesti	bolest	k1gFnSc2	bolest
</s>
</p>
<p>
<s>
Epilepsie	epilepsie	k1gFnSc1	epilepsie
–	–	k?	–
porucha	porucha	k1gFnSc1	porucha
vzniku	vznik	k1gInSc2	vznik
vzruchu	vzruch	k1gInSc2	vzruch
v	v	k7c6	v
mozkové	mozkový	k2eAgFnSc6d1	mozková
kůře	kůra	k1gFnSc6	kůra
vedoucí	vedoucí	k1gFnSc2	vedoucí
k	k	k7c3	k
specifickým	specifický	k2eAgInPc3d1	specifický
záchvatům	záchvat	k1gInPc3	záchvat
provázeným	provázený	k2eAgInPc3d1	provázený
křečemi	křeč	k1gFnPc7	křeč
nebo	nebo	k8xC	nebo
ztrátou	ztráta	k1gFnSc7	ztráta
vědomí	vědomí	k1gNnSc2	vědomí
</s>
</p>
<p>
<s>
Schizofrenie	schizofrenie	k1gFnSc1	schizofrenie
</s>
</p>
<p>
<s>
Deprese	deprese	k1gFnSc1	deprese
</s>
</p>
<p>
<s>
Rakovina	rakovina	k1gFnSc1	rakovina
v	v	k7c6	v
mozku	mozek	k1gInSc6	mozek
–	–	k?	–
nádory	nádor	k1gInPc4	nádor
v	v	k7c6	v
mozku	mozek	k1gInSc6	mozek
jsou	být	k5eAaImIp3nP	být
všeobecně	všeobecně	k6eAd1	všeobecně
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
maligní	maligní	k2eAgNnSc4d1	maligní
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
nemetastázují	metastázovat	k5eNaBmIp3nP	metastázovat
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
vzniknou	vzniknout	k5eAaPmIp3nP	vzniknout
samy	sám	k3xTgFnPc4	sám
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
mozku	mozek	k1gInSc6	mozek
samotném	samotný	k2eAgInSc6d1	samotný
nebo	nebo	k8xC	nebo
do	do	k7c2	do
něj	on	k3xPp3gMnSc2	on
být	být	k5eAaImF	být
zaneseny	zanést	k5eAaPmNgFnP	zanést
z	z	k7c2	z
jiného	jiný	k2eAgInSc2d1	jiný
metastázujícího	metastázující	k2eAgInSc2d1	metastázující
nádoru	nádor	k1gInSc2	nádor
jinde	jinde	k6eAd1	jinde
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Godaux	Godaux	k1gInSc1	Godaux
<g/>
,	,	kIx,	,
Émile	Émile	k1gInSc1	Émile
–	–	k?	–
Mozek	mozek	k1gInSc1	mozek
(	(	kIx(	(
<g/>
edice	edice	k1gFnSc1	edice
Malá	malý	k2eAgFnSc1d1	malá
moderní	moderní	k2eAgFnSc1d1	moderní
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
KMa	KMa	k1gFnSc1	KMa
s.	s.	k?	s.
r.	r.	kA	r.
o.	o.	k?	o.
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
2007	[number]	k4	2007
</s>
</p>
<p>
<s>
Ždychynec	Ždychynec	k1gMnSc1	Ždychynec
<g/>
,	,	kIx,	,
Bohumil	Bohumil	k1gMnSc1	Bohumil
–	–	k?	–
Tajemství	tajemství	k1gNnSc4	tajemství
mozku	mozek	k1gInSc2	mozek
<g/>
,	,	kIx,	,
ISV	ISV	kA	ISV
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2002	[number]	k4	2002
</s>
</p>
<p>
<s>
Broks	Broks	k1gInSc1	Broks
<g/>
,	,	kIx,	,
Paul	Paul	k1gMnSc1	Paul
–	–	k?	–
Do	do	k7c2	do
země	zem	k1gFnSc2	zem
ticha	ticho	k1gNnSc2	ticho
<g/>
:	:	kIx,	:
lidský	lidský	k2eAgInSc1d1	lidský
mozek	mozek	k1gInSc1	mozek
–	–	k?	–
tajemný	tajemný	k2eAgInSc1d1	tajemný
svět	svět	k1gInSc1	svět
"	"	kIx"	"
<g/>
uvnitř	uvnitř	k7c2	uvnitř
hlavy	hlava	k1gFnSc2	hlava
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
NLN	NLN	kA	NLN
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2005	[number]	k4	2005
</s>
</p>
<p>
<s>
Ingram	Ingram	k1gInSc1	Ingram
<g/>
,	,	kIx,	,
Jay	Jay	k1gMnSc1	Jay
–	–	k?	–
Cesta	cesta	k1gFnSc1	cesta
za	za	k7c7	za
tajemstvím	tajemství	k1gNnSc7	tajemství
mozku	mozek	k1gInSc2	mozek
<g/>
,	,	kIx,	,
OLDAG	OLDAG	kA	OLDAG
<g/>
,	,	kIx,	,
Ostrava	Ostrava	k1gFnSc1	Ostrava
1996	[number]	k4	1996
</s>
</p>
<p>
<s>
Koukolík	Koukolík	k1gMnSc1	Koukolík
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
–	–	k?	–
Mozek	mozek	k1gInSc1	mozek
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
duše	duše	k1gFnSc1	duše
<g/>
,	,	kIx,	,
Galén	Galén	k1gInSc1	Galén
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2005	[number]	k4	2005
</s>
</p>
<p>
<s>
Koukolík	Koukolík	k1gMnSc1	Koukolík
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
–	–	k?	–
Vybrané	vybraný	k2eAgFnPc1d1	vybraná
přednášky	přednáška	k1gFnPc1	přednáška
o	o	k7c6	o
vztahu	vztah	k1gInSc6	vztah
mozku	mozek	k1gInSc2	mozek
a	a	k8xC	a
chování	chování	k1gNnSc1	chování
<g/>
,	,	kIx,	,
Karolinum	Karolinum	k1gNnSc1	Karolinum
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1995	[number]	k4	1995
</s>
</p>
<p>
<s>
SINĚLNIKOV	SINĚLNIKOV	kA	SINĚLNIKOV
<g/>
,	,	kIx,	,
R.	R.	kA	R.
D.	D.	kA	D.
a	a	k8xC	a
kolektiv	kolektiv	k1gInSc1	kolektiv
<g/>
.	.	kIx.	.
</s>
<s>
Atlas	Atlas	k1gInSc1	Atlas
anatomie	anatomie	k1gFnSc2	anatomie
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Svazek	svazek	k1gInSc1	svazek
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Státní	státní	k2eAgNnSc1d1	státní
zdravotnické	zdravotnický	k2eAgNnSc1d1	zdravotnické
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
1965	[number]	k4	1965
<g/>
.	.	kIx.	.
400	[number]	k4	400
s.	s.	k?	s.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
lidský	lidský	k2eAgInSc4d1	lidský
mozek	mozek	k1gInSc4	mozek
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
mozek	mozek	k1gInSc1	mozek
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Téma	téma	k1gNnSc1	téma
Mozek	mozek	k1gInSc1	mozek
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Země	země	k1gFnSc1	země
neznámá	známý	k2eNgFnSc1d1	neznámá
i	i	k8xC	i
fantastická	fantastický	k2eAgFnSc1d1	fantastická
<g/>
:	:	kIx,	:
Mapa	mapa	k1gFnSc1	mapa
lidského	lidský	k2eAgInSc2d1	lidský
mozku	mozek	k1gInSc2	mozek
připomíná	připomínat	k5eAaImIp3nS	připomínat
sen	sen	k1gInSc1	sen
</s>
</p>
<p>
<s>
Video	video	k1gNnSc1	video
s	s	k7c7	s
pitvou	pitva	k1gFnSc7	pitva
lidského	lidský	k2eAgInSc2d1	lidský
mozku	mozek	k1gInSc2	mozek
</s>
</p>
