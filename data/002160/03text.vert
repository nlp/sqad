<s>
Indianapolis	Indianapolis	k1gInSc1	Indianapolis
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
a	a	k8xC	a
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
Indiany	Indiana	k1gFnSc2	Indiana
<g/>
,	,	kIx,	,
státu	stát	k1gInSc2	stát
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
843	[number]	k4	843
393	[number]	k4	393
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
z	z	k7c2	z
Indianapolis	Indianapolis	k1gFnSc2	Indianapolis
dělá	dělat	k5eAaImIp3nS	dělat
14	[number]	k4	14
<g/>
.	.	kIx.	.
největší	veliký	k2eAgNnSc1d3	veliký
město	město	k1gNnSc1	město
USA	USA	kA	USA
a	a	k8xC	a
po	po	k7c6	po
Chicagu	Chicago	k1gNnSc6	Chicago
a	a	k8xC	a
Detroitu	Detroit	k1gInSc6	Detroit
třetí	třetí	k4xOgNnSc4	třetí
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
středozápadu	středozápad	k1gInSc2	středozápad
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
820	[number]	k4	820
445	[number]	k4	445
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
61,8	[number]	k4	61,8
<g/>
%	%	kIx~	%
Bílí	bílý	k2eAgMnPc1d1	bílý
Američané	Američan	k1gMnPc1	Američan
27,5	[number]	k4	27,5
<g/>
%	%	kIx~	%
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
0,3	[number]	k4	0,3
<g/>
%	%	kIx~	%
Američtí	americký	k2eAgMnPc1d1	americký
indiáni	indián	k1gMnPc1	indián
2,1	[number]	k4	2,1
<g/>
%	%	kIx~	%
Asijští	asijský	k2eAgMnPc1d1	asijský
Američané	Američan	k1gMnPc1	Američan
0,0	[number]	k4	0,0
<g/>
%	%	kIx~	%
Pacifičtí	pacifický	k2eAgMnPc1d1	pacifický
ostrované	ostrovan	k1gMnPc1	ostrovan
5,5	[number]	k4	5,5
<g/>
%	%	kIx~	%
Jiná	jiný	k2eAgFnSc1d1	jiná
rasa	rasa	k1gFnSc1	rasa
2,8	[number]	k4	2,8
<g/>
%	%	kIx~	%
Dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
ras	ras	k1gMnSc1	ras
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
hispánského	hispánský	k2eAgMnSc2d1	hispánský
nebo	nebo	k8xC	nebo
latinskoamerického	latinskoamerický	k2eAgInSc2d1	latinskoamerický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
9,4	[number]	k4	9,4
<g/>
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Kurt	Kurt	k1gMnSc1	Kurt
Vonnegut	Vonnegut	k1gMnSc1	Vonnegut
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
-	-	kIx~	-
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
Wes	Wes	k1gFnSc2	Wes
Montgomery	Montgomera	k1gFnSc2	Montgomera
(	(	kIx(	(
<g/>
1923	[number]	k4	1923
<g/>
-	-	kIx~	-
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jazzový	jazzový	k2eAgMnSc1d1	jazzový
kytarista	kytarista	k1gMnSc1	kytarista
Philip	Philip	k1gMnSc1	Philip
Warren	Warrna	k1gFnPc2	Warrna
Anderson	Anderson	k1gMnSc1	Anderson
(	(	kIx(	(
<g/>
*	*	kIx~	*
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
,	,	kIx,	,
držitel	držitel	k1gMnSc1	držitel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
Steve	Steve	k1gMnSc1	Steve
McQueen	McQueen	k1gInSc1	McQueen
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1930	[number]	k4	1930
<g/>
-	-	kIx~	-
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
Michael	Michael	k1gMnSc1	Michael
Graves	Graves	k1gMnSc1	Graves
(	(	kIx(	(
<g/>
*	*	kIx~	*
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
architekt	architekt	k1gMnSc1	architekt
Freddie	Freddie	k1gFnSc2	Freddie
Hubbard	Hubbard	k1gMnSc1	Hubbard
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
-	-	kIx~	-
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jazzový	jazzový	k2eAgMnSc1d1	jazzový
trumpetista	trumpetista	k1gMnSc1	trumpetista
Dan	Dan	k1gMnSc1	Dan
Quayle	Quayl	k1gInSc5	Quayl
(	(	kIx(	(
<g/>
*	*	kIx~	*
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
44	[number]	k4	44
<g/>
.	.	kIx.	.
viceprezident	viceprezident	k1gMnSc1	viceprezident
<g />
.	.	kIx.	.
</s>
<s>
USA	USA	kA	USA
David	David	k1gMnSc1	David
Letterman	Letterman	k1gMnSc1	Letterman
<g/>
(	(	kIx(	(
<g/>
*	*	kIx~	*
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
komik	komik	k1gMnSc1	komik
a	a	k8xC	a
moderátor	moderátor	k1gMnSc1	moderátor
talk	talk	k1gMnSc1	talk
show	show	k1gFnSc1	show
Babyface	Babyface	k1gFnSc1	Babyface
(	(	kIx(	(
<g/>
*	*	kIx~	*
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
kytarista	kytarista	k1gMnSc1	kytarista
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
filmový	filmový	k2eAgMnSc1d1	filmový
a	a	k8xC	a
hudební	hudební	k2eAgMnSc1d1	hudební
producent	producent	k1gMnSc1	producent
Vivica	Vivica	k1gMnSc1	Vivica
A.	A.	kA	A.
Foxová	Foxová	k1gFnSc1	Foxová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1964	[number]	k4	1964
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
Brendan	Brendan	k1gMnSc1	Brendan
Fraser	Fraser	k1gMnSc1	Fraser
(	(	kIx(	(
<g/>
*	*	kIx~	*
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
Anthony	Anthona	k1gFnSc2	Anthona
Montgomery	Montgomera	k1gFnSc2	Montgomera
(	(	kIx(	(
<g/>
*	*	kIx~	*
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
hudebník	hudebník	k1gMnSc1	hudebník
Adam	Adam	k1gMnSc1	Adam
Lambert	Lambert	k1gMnSc1	Lambert
(	(	kIx(	(
<g/>
*	*	kIx~	*
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
a	a	k8xC	a
herec	herec	k1gMnSc1	herec
Kolín	Kolín	k1gInSc4	Kolín
nad	nad	k7c7	nad
Rýnem	Rýn	k1gInSc7	Rýn
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc4	Německo
Monza	Monza	k1gFnSc1	Monza
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
Piran	Piran	k1gInSc1	Piran
<g/>
,	,	kIx,	,
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
Scarborough	Scarborougha	k1gFnPc2	Scarborougha
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
Tchaj-pej	Tchajej	k1gFnSc1	Tchaj-pej
<g/>
,	,	kIx,	,
Taiwan	Taiwan	k1gInSc1	Taiwan
</s>
