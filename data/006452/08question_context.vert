<s>
Kyselina	kyselina	k1gFnSc1
askorbová	askorbový	k2eAgFnSc1d1
<g/>
,	,	kIx,
označovaná	označovaný	k2eAgFnSc1d1
také	také	k9
jako	jako	k8xS
vitamin	vitamin	k1gInSc4
C	C	kA
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
ve	v	k7c6
vodě	voda	k1gFnSc6
rozpustná	rozpustný	k2eAgFnSc1d1
organická	organický	k2eAgFnSc1d1
sloučenina	sloučenina	k1gFnSc1
s	s	k7c7
redukčními	redukční	k2eAgInPc7d1
účinky	účinek	k1gInPc7
<g/>
,	,	kIx,
v	v	k7c6
pevném	pevný	k2eAgInSc6d1
stavu	stav	k1gInSc6
bílého	bílý	k2eAgInSc2d1
vzhledu	vzhled	k1gInSc2
<g/>
;	;	kIx,
znečištěná	znečištěný	k2eAgFnSc1d1
s	s	k7c7
možným	možný	k2eAgNnSc7d1
nažloutlým	nažloutlý	k2eAgNnSc7d1
zabarvením	zabarvení	k1gNnSc7
<g/>
.	.	kIx.
</s>