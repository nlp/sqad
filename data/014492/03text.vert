<s>
Auxon	Auxon	k1gNnSc1
(	(	kIx(
<g/>
Aube	Aube	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Auxon	Auxon	k1gInSc1
radnice	radnice	k1gFnSc2
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
Poloha	poloha	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
48	#num#	k4
<g/>
°	°	k?
<g/>
6	#num#	k4
<g/>
′	′	k?
<g/>
17	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
3	#num#	k4
<g/>
°	°	k?
<g/>
55	#num#	k4
<g/>
′	′	k?
<g/>
6	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
122	#num#	k4
<g/>
–	–	k?
<g/>
294	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Stát	stát	k1gInSc1
</s>
<s>
Francie	Francie	k1gFnSc1
Francie	Francie	k1gFnSc2
region	region	k1gInSc1
</s>
<s>
Grand	grand	k1gMnSc1
Est	Est	k1gMnSc1
departement	departement	k1gInSc4
</s>
<s>
Aube	Aube	k6eAd1
arrondissement	arrondissement	k1gInSc1
</s>
<s>
Troyes	Troyes	k1gInSc1
kanton	kanton	k1gInSc1
</s>
<s>
Aix-en-Othe	Aix-en-Othe	k6eAd1
</s>
<s>
Auxon	Auxon	k1gMnSc1
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
29,49	29,49	k4
km²	km²	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
941	#num#	k4
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
37	#num#	k4
obyv	obyv	k1gInSc1
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Správa	správa	k1gFnSc1
Oficiální	oficiální	k2eAgFnSc1d1
web	web	k1gInSc4
</s>
<s>
auxon	auxon	k1gInSc1
<g/>
.	.	kIx.
<g/>
free	freat	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
fr	fr	k0
PSČ	PSČ	kA
</s>
<s>
10130	#num#	k4
INSEE	INSEE	kA
</s>
<s>
10018	#num#	k4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Auxon	Auxon	k1gInSc1
je	být	k5eAaImIp3nS
francouzská	francouzský	k2eAgFnSc1d1
obec	obec	k1gFnSc1
v	v	k7c6
departementu	departement	k1gInSc6
Aube	Aub	k1gFnSc2
v	v	k7c6
regionu	region	k1gInSc6
Grand	grand	k1gMnSc1
Est	Est	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
zde	zde	k6eAd1
žilo	žít	k5eAaImAgNnS
941	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sousední	sousední	k2eAgFnPc1d1
obce	obec	k1gFnPc1
</s>
<s>
Davrey	Davrea	k1gFnPc1
<g/>
,	,	kIx,
Eaux-Puiseaux	Eaux-Puiseaux	k1gInSc1
<g/>
,	,	kIx,
Ervy-le-Châtel	Ervy-le-Châtel	k1gInSc1
<g/>
,	,	kIx,
Chamoy	Chamoy	k1gInPc1
<g/>
,	,	kIx,
Maraye-en-Othe	Maraye-en-Othe	k1gNnPc1
<g/>
,	,	kIx,
Montigny-les-Monts	Montigny-les-Monts	k1gInSc1
<g/>
,	,	kIx,
Saint-Phal	Saint-Phal	k1gMnSc1
</s>
<s>
Vývoj	vývoj	k1gInSc1
počtu	počet	k1gInSc2
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
INSEE	INSEE	kA
<g/>
↑	↑	k?
INSEE	INSEE	kA
(	(	kIx(
<g/>
ZIP	zip	k1gInSc1
2,2	2,2	k4
MB	MB	kA
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
obcí	obec	k1gFnPc2
v	v	k7c6
departementu	departement	k1gInSc6
Aube	Aub	k1gFnSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Auxon	Auxona	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
obce	obec	k1gFnSc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Obce	obec	k1gFnSc2
kantonu	kanton	k1gInSc2
Aix-en-Othe	Aix-en-Oth	k1gFnSc2
</s>
<s>
Aix-en-Othe	Aix-en-Othe	k1gFnSc1
•	•	k?
Auxon	Auxon	k1gInSc1
•	•	k?
Bercenay-en-Othe	Bercenay-en-Oth	k1gMnSc2
•	•	k?
Bérulle	Bérull	k1gMnSc2
•	•	k?
Bucey-en-Othe	Bucey-en-Oth	k1gMnSc2
•	•	k?
Chamoy	Chamoa	k1gMnSc2
•	•	k?
Chennegy	Chennega	k1gFnSc2
•	•	k?
Chessy-les-Prés	Chessy-les-Prés	k1gInSc1
•	•	k?
Coursan-en-Othe	Coursan-en-Othe	k1gInSc1
•	•	k?
Courtaoult	Courtaoult	k1gInSc1
•	•	k?
Les	les	k1gInSc1
Croû	Croû	k1gInSc1
•	•	k?
Davrey	Davrea	k1gFnSc2
•	•	k?
Eaux-Puiseaux	Eaux-Puiseaux	k1gInSc1
•	•	k?
Ervy-le-Châtel	Ervy-le-Châtel	k1gInSc1
•	•	k?
Estissac	Estissac	k1gInSc1
•	•	k?
Fontvannes	Fontvannes	k1gInSc1
•	•	k?
Maraye-en-Othe	Maraye-en-Oth	k1gFnSc2
•	•	k?
Marolles-sous-Ligniè	Marolles-sous-Ligniè	k1gMnSc1
•	•	k?
Messon	Messon	k1gMnSc1
•	•	k?
Montfey	Montfea	k1gFnSc2
•	•	k?
Montigny-les-Monts	Montigny-les-Monts	k1gInSc1
•	•	k?
Neuville-sur-Vanne	Neuville-sur-Vann	k1gInSc5
•	•	k?
Nogent-en-Othe	Nogent-en-Othe	k1gNnSc6
•	•	k?
Paisy-Cosdon	Paisy-Cosdon	k1gInSc1
•	•	k?
Palis	Palis	k1gInSc1
•	•	k?
Planty	planta	k1gFnSc2
•	•	k?
Prugny	Prugna	k1gFnSc2
•	•	k?
Racines	Racines	k1gMnSc1
•	•	k?
Rigny-le-Ferron	Rigny-le-Ferron	k1gMnSc1
•	•	k?
Saint-Benoist-sur-Vanne	Saint-Benoist-sur-Vann	k1gInSc5
•	•	k?
Saint-Mards-en-Othe	Saint-Mards-en-Oth	k1gFnSc2
•	•	k?
Saint-Phal	Saint-Phal	k1gMnSc1
•	•	k?
Vauchassis	Vauchassis	k1gInSc1
•	•	k?
Villemaur-sur-Vanne	Villemaur-sur-Vann	k1gInSc5
•	•	k?
Villemoiron-en-Othe	Villemoiron-en-Othe	k1gNnSc6
•	•	k?
Villeneuve-au-Chemin	Villeneuve-au-Chemin	k1gInSc1
•	•	k?
Vosnon	Vosnon	k1gInSc1
•	•	k?
Vulaines	Vulaines	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Francie	Francie	k1gFnSc1
</s>
