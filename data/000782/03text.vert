<s>
Adalbert	Adalbert	k1gMnSc1	Adalbert
Stifter	Stifter	k1gMnSc1	Stifter
(	(	kIx(	(
<g/>
23	[number]	k4	23
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1805	[number]	k4	1805
Horní	horní	k2eAgFnSc1d1	horní
Planá	Planá	k1gFnSc1	Planá
-	-	kIx~	-
28	[number]	k4	28
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1868	[number]	k4	1868
Linec	Linec	k1gInSc1	Linec
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
česko-rakouský	českoakouský	k2eAgMnSc1d1	česko-rakouský
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
a	a	k8xC	a
pedagog	pedagog	k1gMnSc1	pedagog
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejvýznamnějším	významný	k2eAgMnPc3d3	nejvýznamnější
představitelům	představitel	k1gMnPc3	představitel
biedermeieru	biedermeier	k1gInSc2	biedermeier
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
aktuální	aktuální	k2eAgMnSc1d1	aktuální
svou	svůj	k3xOyFgFnSc7	svůj
kritikou	kritika	k1gFnSc7	kritika
vyhrocených	vyhrocený	k2eAgInPc2d1	vyhrocený
vztahů	vztah	k1gInPc2	vztah
mezi	mezi	k7c7	mezi
národy	národ	k1gInPc7	národ
a	a	k8xC	a
svým	svůj	k3xOyFgInSc7	svůj
vztahem	vztah	k1gInSc7	vztah
k	k	k7c3	k
šumavské	šumavský	k2eAgFnSc3d1	Šumavská
přírodě	příroda	k1gFnSc3	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
jeho	jeho	k3xOp3gInPc3	jeho
příkladným	příkladný	k2eAgInPc3d1	příkladný
postojům	postoj	k1gInPc3	postoj
ve	v	k7c6	v
vztazích	vztah	k1gInPc6	vztah
mezi	mezi	k7c7	mezi
českým	český	k2eAgNnSc7d1	české
a	a	k8xC	a
německým	německý	k2eAgNnSc7d1	německé
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
zemí	zem	k1gFnPc2	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
<g/>
,	,	kIx,	,
po	po	k7c6	po
něm	on	k3xPp3gMnSc6	on
bylo	být	k5eAaImAgNnS	být
pojmenováno	pojmenovat	k5eAaPmNgNnS	pojmenovat
sdružení	sdružení	k1gNnSc1	sdružení
Adalberta	Adalbert	k1gMnSc2	Adalbert
Stiftera	Stifter	k1gMnSc2	Stifter
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
podporou	podpora	k1gFnSc7	podpora
česko-německého	českoěmecký	k2eAgNnSc2d1	česko-německé
porozumění	porozumění	k1gNnSc2	porozumění
<g/>
.	.	kIx.	.
</s>
<s>
Adalbert	Adalbert	k1gMnSc1	Adalbert
Stifter	Stifter	k1gMnSc1	Stifter
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
23	[number]	k4	23
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1805	[number]	k4	1805
jako	jako	k8xC	jako
nejstarší	starý	k2eAgMnSc1d3	nejstarší
syn	syn	k1gMnSc1	syn
tkalce	tkadlec	k1gMnSc2	tkadlec
Johanna	Johanno	k1gNnSc2	Johanno
Stiftera	Stifter	k1gMnSc2	Stifter
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
ženy	žena	k1gFnSc2	žena
Magdaleny	Magdalena	k1gFnSc2	Magdalena
v	v	k7c6	v
Horní	horní	k2eAgFnSc6d1	horní
Plané	Planá	k1gFnSc6	Planá
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
(	(	kIx(	(
<g/>
Šumava	Šumava	k1gFnSc1	Šumava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
se	s	k7c7	s
jménem	jméno	k1gNnSc7	jméno
Albert	Albert	k1gMnSc1	Albert
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
otec	otec	k1gMnSc1	otec
zahynul	zahynout	k5eAaPmAgMnS	zahynout
při	při	k7c6	při
nehodě	nehoda	k1gFnSc6	nehoda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1817	[number]	k4	1817
<g/>
,	,	kIx,	,
vychovávala	vychovávat	k5eAaImAgFnS	vychovávat
jej	on	k3xPp3gMnSc4	on
matka	matka	k1gFnSc1	matka
<g/>
.	.	kIx.	.
</s>
<s>
Prací	práce	k1gFnSc7	práce
na	na	k7c4	na
hospodářství	hospodářství	k1gNnSc4	hospodářství
svého	svůj	k3xOyFgMnSc2	svůj
dědečka	dědeček	k1gMnSc2	dědeček
z	z	k7c2	z
otcovy	otcův	k2eAgFnSc2d1	otcova
strany	strana	k1gFnSc2	strana
Augustina	Augustin	k1gMnSc2	Augustin
Stiftera	Stifter	k1gMnSc2	Stifter
pomáhal	pomáhat	k5eAaImAgInS	pomáhat
zlepšovat	zlepšovat	k5eAaImF	zlepšovat
rodinné	rodinný	k2eAgInPc4d1	rodinný
poměry	poměr	k1gInPc4	poměr
<g/>
.	.	kIx.	.
</s>
<s>
Smrt	smrt	k1gFnSc1	smrt
otce	otec	k1gMnSc2	otec
Adalbertem	Adalbert	k1gMnSc7	Adalbert
natolik	natolik	k6eAd1	natolik
otřásla	otřást	k5eAaPmAgFnS	otřást
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
odmítal	odmítat	k5eAaImAgMnS	odmítat
přijmout	přijmout	k5eAaPmF	přijmout
jako	jako	k9	jako
fakt	fakt	k1gInSc4	fakt
a	a	k8xC	a
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
držel	držet	k5eAaImAgMnS	držet
hladovku	hladovka	k1gFnSc4	hladovka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1818	[number]	k4	1818
ho	on	k3xPp3gMnSc4	on
poslal	poslat	k5eAaPmAgMnS	poslat
dědeček	dědeček	k1gMnSc1	dědeček
Franz	Franz	k1gMnSc1	Franz
Friepes	Friepes	k1gMnSc1	Friepes
(	(	kIx(	(
<g/>
z	z	k7c2	z
matčiny	matčin	k2eAgFnSc2d1	matčina
strany	strana	k1gFnSc2	strana
<g/>
)	)	kIx)	)
proti	proti	k7c3	proti
jeho	jeho	k3xOp3gFnSc3	jeho
vůli	vůle	k1gFnSc3	vůle
na	na	k7c4	na
latinskou	latinský	k2eAgFnSc4d1	Latinská
školu	škola	k1gFnSc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1820	[number]	k4	1820
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
vdala	vdát	k5eAaPmAgFnS	vdát
za	za	k7c2	za
pekaře	pekař	k1gMnSc2	pekař
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
Mayera	Mayer	k1gMnSc2	Mayer
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1825	[number]	k4	1825
Stifter	Stifter	k1gMnSc1	Stifter
prodělal	prodělat	k5eAaPmAgMnS	prodělat
neštovice	neštovice	k1gFnPc4	neštovice
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1826	[number]	k4	1826
navštěvoval	navštěvovat	k5eAaImAgInS	navštěvovat
Stifter	Stifter	k1gInSc1	Stifter
latinskou	latinský	k2eAgFnSc4d1	Latinská
školu	škola	k1gFnSc4	škola
benediktinů	benediktin	k1gMnPc2	benediktin
v	v	k7c6	v
Kremsmünsteru	Kremsmünster	k1gInSc6	Kremsmünster
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
později	pozdě	k6eAd2	pozdě
popsal	popsat	k5eAaPmAgMnS	popsat
jako	jako	k9	jako
nejkrásnější	krásný	k2eAgNnSc4d3	nejkrásnější
období	období	k1gNnSc4	období
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
rodí	rodit	k5eAaImIp3nP	rodit
cit	cit	k1gInSc4	cit
pro	pro	k7c4	pro
přírodu	příroda	k1gFnSc4	příroda
<g/>
,	,	kIx,	,
literaturu	literatura	k1gFnSc4	literatura
i	i	k8xC	i
umění	umění	k1gNnSc4	umění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
latinské	latinský	k2eAgFnSc6d1	Latinská
škole	škola	k1gFnSc6	škola
byl	být	k5eAaImAgInS	být
nazýván	nazývat	k5eAaImNgInS	nazývat
"	"	kIx"	"
<g/>
Stifter	Stifter	k1gMnSc1	Stifter
Adalbertus	Adalbertus	k1gMnSc1	Adalbertus
<g/>
,	,	kIx,	,
Bohemus	Bohemus	k1gMnSc1	Bohemus
Oberplanensis	Oberplanensis	k1gFnSc2	Oberplanensis
<g/>
"	"	kIx"	"
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Adalbert	Adalbert	k1gMnSc1	Adalbert
Stifter	Stifter	k1gMnSc1	Stifter
Čech	Čech	k1gMnSc1	Čech
Hornoplanský	Hornoplanský	k2eAgMnSc1d1	Hornoplanský
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Školu	škola	k1gFnSc4	škola
ukončil	ukončit	k5eAaPmAgMnS	ukončit
s	s	k7c7	s
vynikajícími	vynikající	k2eAgInPc7d1	vynikající
výsledky	výsledek	k1gInPc7	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
Pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
na	na	k7c6	na
Vídeňské	vídeňský	k2eAgFnSc6d1	Vídeňská
univerzitě	univerzita	k1gFnSc6	univerzita
studiem	studio	k1gNnSc7	studio
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Zajímá	zajímat	k5eAaImIp3nS	zajímat
se	se	k3xPyFc4	se
o	o	k7c4	o
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
,	,	kIx,	,
jazyky	jazyk	k1gInPc4	jazyk
a	a	k8xC	a
filosofii	filosofie	k1gFnSc4	filosofie
<g/>
.	.	kIx.	.
</s>
<s>
Studium	studium	k1gNnSc1	studium
si	se	k3xPyFc3	se
financuje	financovat	k5eAaBmIp3nS	financovat
jako	jako	k9	jako
soukromý	soukromý	k2eAgMnSc1d1	soukromý
učitel	učitel	k1gMnSc1	učitel
(	(	kIx(	(
<g/>
má	mít	k5eAaImIp3nS	mít
výrazné	výrazný	k2eAgNnSc4d1	výrazné
pedagogické	pedagogický	k2eAgNnSc4d1	pedagogické
nadání	nadání	k1gNnSc4	nadání
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
nikdy	nikdy	k6eAd1	nikdy
si	se	k3xPyFc3	se
nenalezne	naleznout	k5eNaPmIp3nS	naleznout
stálé	stálý	k2eAgNnSc4d1	stálé
učitelské	učitelský	k2eAgNnSc4d1	učitelské
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
této	tento	k3xDgFnSc3	tento
práci	práce	k1gFnSc3	práce
také	také	k9	také
poznává	poznávat	k5eAaImIp3nS	poznávat
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
lásku	láska	k1gFnSc4	láska
<g/>
,	,	kIx,	,
Fanny	Fanen	k2eAgFnPc4d1	Fanen
Greipelovou	Greipelová	k1gFnSc7	Greipelová
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnPc1	její
rodiče	rodič	k1gMnPc1	rodič
však	však	k9	však
vztah	vztah	k1gInSc4	vztah
ukončují	ukončovat	k5eAaImIp3nP	ukončovat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
vrozené	vrozený	k2eAgFnSc2d1	vrozená
Stifterovy	Stifterův	k2eAgFnSc2d1	Stifterova
letargie	letargie	k1gFnSc2	letargie
a	a	k8xC	a
neschopnosti	neschopnost	k1gFnSc2	neschopnost
nalézt	nalézt	k5eAaBmF	nalézt
si	se	k3xPyFc3	se
pevné	pevný	k2eAgNnSc4d1	pevné
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1832	[number]	k4	1832
potkává	potkávat	k5eAaImIp3nS	potkávat
Amalii	Amalie	k1gFnSc4	Amalie
Mohauptovou	Mohauptová	k1gFnSc4	Mohauptová
<g/>
,	,	kIx,	,
dceru	dcera	k1gFnSc4	dcera
důstojníka	důstojník	k1gMnSc2	důstojník
z	z	k7c2	z
Kyjova	Kyjov	k1gInSc2	Kyjov
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
také	také	k9	také
ukončuje	ukončovat	k5eAaImIp3nS	ukončovat
studia	studio	k1gNnPc4	studio
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1833	[number]	k4	1833
až	až	k8xS	až
1848	[number]	k4	1848
se	se	k3xPyFc4	se
živil	živit	k5eAaImAgInS	živit
částečně	částečně	k6eAd1	částečně
jako	jako	k9	jako
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
jako	jako	k9	jako
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
.	.	kIx.	.
</s>
<s>
Vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
tehdy	tehdy	k6eAd1	tehdy
jeho	jeho	k3xOp3gFnPc4	jeho
první	první	k4xOgFnPc4	první
povídky	povídka	k1gFnPc4	povídka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
svatbě	svatba	k1gFnSc6	svatba
s	s	k7c7	s
Amalií	Amalie	k1gFnSc7	Amalie
(	(	kIx(	(
<g/>
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
modistkou	modistka	k1gFnSc7	modistka
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1837	[number]	k4	1837
zůstal	zůstat	k5eAaPmAgInS	zůstat
pár	pár	k1gInSc1	pár
dlouho	dlouho	k6eAd1	dlouho
bezdětný	bezdětný	k2eAgMnSc1d1	bezdětný
<g/>
,	,	kIx,	,
což	což	k3yRnSc4	což
Stifter	Stifter	k1gMnSc1	Stifter
těžce	těžce	k6eAd1	těžce
nesl	nést	k5eAaImAgMnS	nést
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
se	se	k3xPyFc4	se
ujali	ujmout	k5eAaPmAgMnP	ujmout
péče	péče	k1gFnSc1	péče
o	o	k7c4	o
Amaliinu	Amaliin	k2eAgFnSc4d1	Amaliin
neteř	neteř	k1gFnSc4	neteř
Julianu	Julian	k1gMnSc3	Julian
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
Stifter	Stiftra	k1gFnPc2	Stiftra
cestoval	cestovat	k5eAaImAgMnS	cestovat
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
rodné	rodný	k2eAgFnSc2d1	rodná
vsi	ves	k1gFnSc2	ves
<g/>
,	,	kIx,	,
také	také	k9	také
do	do	k7c2	do
Mnichova	Mnichov	k1gInSc2	Mnichov
či	či	k8xC	či
Lince	Linec	k1gInSc2	Linec
<g/>
.	.	kIx.	.
</s>
<s>
Vyšel	vyjít	k5eAaPmAgMnS	vyjít
mu	on	k3xPp3gMnSc3	on
román	román	k1gInSc4	román
Pozdní	pozdní	k2eAgNnSc4d1	pozdní
léto	léto	k1gNnSc4	léto
(	(	kIx(	(
<g/>
Der	drát	k5eAaImRp2nS	drát
Nachsommer	Nachsommer	k1gInSc1	Nachsommer
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1841	[number]	k4	1841
opět	opět	k6eAd1	opět
působil	působit	k5eAaImAgMnS	působit
jako	jako	k8xS	jako
domácí	domácí	k2eAgMnSc1d1	domácí
učitel	učitel	k1gMnSc1	učitel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1843	[number]	k4	1843
až	až	k9	až
1846	[number]	k4	1846
například	například	k6eAd1	například
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
Kynžvart	Kynžvart	k1gInSc1	Kynžvart
učil	učit	k5eAaImAgMnS	učit
Richarda	Richard	k1gMnSc4	Richard
<g/>
,	,	kIx,	,
syna	syn	k1gMnSc4	syn
rakouského	rakouský	k2eAgMnSc2d1	rakouský
kancléře	kancléř	k1gMnSc2	kancléř
Klemense	Klemens	k1gMnSc2	Klemens
Václava	Václav	k1gMnSc2	Václav
Metternicha	Metternich	k1gMnSc2	Metternich
<g/>
.	.	kIx.	.
</s>
<s>
Revoluční	revoluční	k2eAgInSc4d1	revoluční
rok	rok	k1gInSc4	rok
1848	[number]	k4	1848
silně	silně	k6eAd1	silně
zasáhl	zasáhnout	k5eAaPmAgInS	zasáhnout
Stiftera	Stifter	k1gMnSc4	Stifter
svými	svůj	k3xOyFgFnPc7	svůj
myšlenkami	myšlenka	k1gFnPc7	myšlenka
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
je	být	k5eAaImIp3nS	být
však	však	k9	však
zklamán	zklamat	k5eAaPmNgMnS	zklamat
<g/>
,	,	kIx,	,
stahuje	stahovat	k5eAaImIp3nS	stahovat
se	se	k3xPyFc4	se
do	do	k7c2	do
ústraní	ústraní	k1gNnSc2	ústraní
a	a	k8xC	a
do	do	k7c2	do
konce	konec	k1gInSc2	konec
života	život	k1gInSc2	život
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
konzervativní	konzervativní	k2eAgFnSc1d1	konzervativní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
díle	dílo	k1gNnSc6	dílo
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
přírodní	přírodní	k2eAgInPc1d1	přírodní
motivy	motiv	k1gInPc1	motiv
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1850	[number]	k4	1850
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
hornorakouským	hornorakouský	k2eAgMnSc7d1	hornorakouský
školním	školní	k2eAgMnSc7d1	školní
inspektorem	inspektor	k1gMnSc7	inspektor
s	s	k7c7	s
titulem	titul	k1gInSc7	titul
c.	c.	k?	c.
a	a	k8xC	a
k.	k.	k?	k.
školní	školní	k2eAgFnSc1d1	školní
rada	rada	k1gFnSc1	rada
a	a	k8xC	a
s	s	k7c7	s
ročním	roční	k2eAgInSc7d1	roční
příjmem	příjem	k1gInSc7	příjem
1500	[number]	k4	1500
zlatých	zlatá	k1gFnPc2	zlatá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1858	[number]	k4	1858
umírá	umírat	k5eAaImIp3nS	umírat
Stifterova	Stifterův	k2eAgFnSc1d1	Stifterova
matka	matka	k1gFnSc1	matka
<g/>
.	.	kIx.	.
</s>
<s>
Stifter	Stifter	k1gInSc1	Stifter
pracuje	pracovat	k5eAaImIp3nS	pracovat
na	na	k7c6	na
historickém	historický	k2eAgInSc6d1	historický
románu	román	k1gInSc6	román
Vítek	Vítek	k1gMnSc1	Vítek
(	(	kIx(	(
<g/>
Witiko	Witika	k1gFnSc5	Witika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInPc4	jehož
tři	tři	k4xCgInPc4	tři
díly	díl	k1gInPc4	díl
postupně	postupně	k6eAd1	postupně
vycházejí	vycházet	k5eAaImIp3nP	vycházet
během	během	k7c2	během
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
práce	práce	k1gFnSc2	práce
se	se	k3xPyFc4	se
u	u	k7c2	u
něj	on	k3xPp3gNnSc2	on
objevují	objevovat	k5eAaImIp3nP	objevovat
zdravotní	zdravotní	k2eAgInPc4d1	zdravotní
problémy	problém	k1gInPc4	problém
<g/>
.	.	kIx.	.
26	[number]	k4	26
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1868	[number]	k4	1868
se	se	k3xPyFc4	se
zřejmě	zřejmě	k6eAd1	zřejmě
kvůli	kvůli	k7c3	kvůli
zhoršujícímu	zhoršující	k2eAgInSc3d1	zhoršující
se	se	k3xPyFc4	se
zdravotnímu	zdravotní	k2eAgInSc3d1	zdravotní
stavu	stav	k1gInSc3	stav
pokouší	pokoušet	k5eAaImIp3nS	pokoušet
o	o	k7c4	o
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kómatu	kóma	k1gNnSc6	kóma
leží	ležet	k5eAaImIp3nS	ležet
až	až	k9	až
do	do	k7c2	do
28	[number]	k4	28
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
umírá	umírat	k5eAaImIp3nS	umírat
<g/>
.	.	kIx.	.
</s>
<s>
Poznámka	poznámka	k1gFnSc1	poznámka
<g/>
:	:	kIx,	:
výběr	výběr	k1gInSc1	výběr
Stifterových	Stifterův	k2eAgNnPc2d1	Stifterovo
děl	dělo	k1gNnPc2	dělo
dostupných	dostupný	k2eAgNnPc2d1	dostupné
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
překladu	překlad	k1gInSc6	překlad
(	(	kIx(	(
<g/>
rok	rok	k1gInSc4	rok
v	v	k7c6	v
závorce	závorka	k1gFnSc6	závorka
=	=	kIx~	=
první	první	k4xOgNnPc1	první
vydání	vydání	k1gNnSc1	vydání
originálu	originál	k1gInSc2	originál
<g/>
)	)	kIx)	)
Hrad	hrad	k1gInSc1	hrad
bláznů	blázen	k1gMnPc2	blázen
(	(	kIx(	(
<g/>
1844	[number]	k4	1844
<g/>
)	)	kIx)	)
Studie	studie	k1gFnSc1	studie
(	(	kIx(	(
<g/>
6	[number]	k4	6
svazků	svazek	k1gInPc2	svazek
-	-	kIx~	-
1842	[number]	k4	1842
<g/>
-	-	kIx~	-
<g/>
1845	[number]	k4	1845
<g/>
)	)	kIx)	)
Das	Das	k1gMnSc1	Das
Haidedorf	Haidedorf	k1gMnSc1	Haidedorf
(	(	kIx(	(
<g/>
1840	[number]	k4	1840
<g/>
)	)	kIx)	)
Hvozd	hvozd	k1gInSc1	hvozd
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1841	[number]	k4	1841
<g/>
)	)	kIx)	)
Abdiáš	Abdiáš	k1gFnSc2	Abdiáš
(	(	kIx(	(
<g/>
1842	[number]	k4	1842
<g/>
)	)	kIx)	)
Brigita	Brigita	k1gFnSc1	Brigita
(	(	kIx(	(
<g/>
1844	[number]	k4	1844
<g/>
)	)	kIx)	)
Starý	Starý	k1gMnSc1	Starý
mládenec	mládenec	k1gMnSc1	mládenec
(	(	kIx(	(
<g/>
1845	[number]	k4	1845
<g/>
)	)	kIx)	)
Lesní	lesní	k2eAgFnSc1d1	lesní
pěšina	pěšina	k1gFnSc1	pěšina
(	(	kIx(	(
<g/>
1845	[number]	k4	1845
<g/>
)	)	kIx)	)
Popsaná	popsaný	k2eAgFnSc1d1	popsaná
jedlička	jedlička	k1gFnSc1	jedlička
(	(	kIx(	(
<g/>
1846	[number]	k4	1846
<g/>
)	)	kIx)	)
Lesní	lesní	k2eAgMnSc1d1	lesní
poutník	poutník	k1gMnSc1	poutník
(	(	kIx(	(
<g/>
1847	[number]	k4	1847
<g/>
)	)	kIx)	)
Horský	horský	k2eAgInSc4d1	horský
křišťál	křišťál	k1gInSc4	křišťál
Pozdní	pozdní	k2eAgNnSc1d1	pozdní
léto	léto	k1gNnSc1	léto
(	(	kIx(	(
<g/>
1857	[number]	k4	1857
<g/>
)	)	kIx)	)
Z	z	k7c2	z
kroniky	kronika	k1gFnSc2	kronika
našeho	náš	k3xOp1gInSc2	náš
rodu	rod	k1gInSc2	rod
(	(	kIx(	(
<g/>
1864	[number]	k4	1864
<g/>
)	)	kIx)	)
Potomci	potomek	k1gMnPc1	potomek
(	(	kIx(	(
<g/>
1864	[number]	k4	1864
<g/>
)	)	kIx)	)
Vítek	Vítek	k1gMnSc1	Vítek
(	(	kIx(	(
<g/>
3	[number]	k4	3
svazky	svazek	k1gInPc4	svazek
-	-	kIx~	-
1865	[number]	k4	1865
<g/>
-	-	kIx~	-
<g/>
1867	[number]	k4	1867
<g/>
)	)	kIx)	)
Povídky	povídka	k1gFnPc1	povídka
(	(	kIx(	(
<g/>
1869	[number]	k4	1869
<g/>
)	)	kIx)	)
Polní	polní	k2eAgFnSc2d1	polní
květiny	květina	k1gFnSc2	květina
</s>
