<s>
Adalbert	Adalbert	k1gMnSc1
Stifter	Stifter	k1gMnSc1
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgMnS
23	[number]	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1805	[number]	k4
jako	jako	k8xC
nejstarší	starý	k2eAgMnSc1d3
syn	syn	k1gMnSc1
tkalce	tkadlec	k1gMnSc2
Johanna	Johanno	k1gNnSc2
Stiftera	Stifter	k1gMnSc2
a	a	k8xC
jeho	jeho	k3xOp3gFnSc2
ženy	žena	k1gFnSc2
Magdaleny	Magdalena	k1gFnSc2
v	v	k7c6
Horní	horní	k2eAgFnSc6d1
Plané	Planá	k1gFnSc6
nad	nad	k7c7
Vltavou	Vltava	k1gFnSc7
(	(	kIx(
<g/>
Šumava	Šumava	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
původně	původně	k6eAd1
se	s	k7c7
jménem	jméno	k1gNnSc7
Albert	Albert	k1gMnSc1
<g/>
.	.	kIx.
</s>