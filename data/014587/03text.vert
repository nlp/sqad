<s>
Číslovka	číslovka	k1gFnSc1
</s>
<s>
Číslovka	číslovka	k1gFnSc1
(	(	kIx(
<g/>
lat.	lat.	kA
numerale	numerale	k1gFnSc1
<g/>
,	,	kIx,
plurál	plurál	k1gInSc1
numeralia	numeralia	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
slovní	slovní	k2eAgInSc1d1
druh	druh	k1gInSc1
<g/>
,	,	kIx,
kterým	který	k3yRgInSc7
se	se	k3xPyFc4
vyjadřuje	vyjadřovat	k5eAaImIp3nS
počet	počet	k1gInSc1
<g/>
,	,	kIx,
pořadí	pořadí	k1gNnSc1
<g/>
,	,	kIx,
násobenost	násobenost	k1gFnSc1
<g/>
,	,	kIx,
díl	díl	k1gInSc1
celku	celek	k1gInSc2
apod.	apod.	kA
</s>
<s>
Typy	typ	k1gInPc1
číslovek	číslovka	k1gFnPc2
</s>
<s>
Dělení	dělení	k1gNnSc1
podle	podle	k7c2
určitosti	určitost	k1gFnSc2
počtu	počet	k1gInSc2
</s>
<s>
Číslovky	číslovka	k1gFnPc1
se	se	k3xPyFc4
dělí	dělit	k5eAaImIp3nP
na	na	k7c4
<g/>
:	:	kIx,
</s>
<s>
neurčité	určitý	k2eNgNnSc1d1
–	–	k?
označují	označovat	k5eAaImIp3nP
počet	počet	k1gInSc4
jen	jen	k9
obecně	obecně	k6eAd1
(	(	kIx(
<g/>
několik	několik	k4yIc4
<g/>
,	,	kIx,
několikrát	několikrát	k6eAd1
<g/>
,	,	kIx,
několikanásobný	několikanásobný	k2eAgInSc1d1
<g/>
,	,	kIx,
málo	málo	k1gNnSc1
<g/>
,	,	kIx,
moc	moc	k1gFnSc1
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
se	se	k3xPyFc4
mezi	mezi	k7c4
neurčité	určitý	k2eNgFnPc4d1
číslovky	číslovka	k1gFnPc4
počítají	počítat	k5eAaImIp3nP
i	i	k9
slova	slovo	k1gNnPc1
jako	jako	k9
nikdo	nikdo	k3yNnSc1
<g/>
,	,	kIx,
žádný	žádný	k3yNgMnSc1
<g/>
)	)	kIx)
</s>
<s>
určité	určitý	k2eAgInPc1d1
–	–	k?
označují	označovat	k5eAaImIp3nP
přesný	přesný	k2eAgInSc4d1
počet	počet	k1gInSc4
(	(	kIx(
<g/>
dvakrát	dvakrát	k6eAd1
<g/>
,	,	kIx,
dvojnásobný	dvojnásobný	k2eAgMnSc1d1
<g/>
,	,	kIx,
dva	dva	k4xCgInPc4
<g/>
)	)	kIx)
<g/>
;	;	kIx,
některé	některý	k3yIgFnPc1
číslovky	číslovka	k1gFnPc1
určité	určitý	k2eAgFnPc1d1
mohou	moct	k5eAaImIp3nP
mít	mít	k5eAaImF
význam	význam	k1gInSc4
neurčitý	určitý	k2eNgInSc4d1
(	(	kIx(
<g/>
tisíceré	tisícerý	k4xRgInPc4
díky	dík	k1gInPc1
<g/>
,	,	kIx,
mám	mít	k5eAaImIp1nS
sto	sto	k4xCgNnSc4
chutí	chuť	k1gFnSc7
<g/>
)	)	kIx)
</s>
<s>
Dělení	dělení	k1gNnSc1
podle	podle	k7c2
syntaktické	syntaktický	k2eAgFnSc2d1
formy	forma	k1gFnSc2
</s>
<s>
základní	základní	k2eAgInSc4d1
(	(	kIx(
<g/>
kardinální	kardinální	k2eAgInSc4d1
<g/>
)	)	kIx)
–	–	k?
označují	označovat	k5eAaImIp3nP
počet	počet	k1gInSc4
<g/>
,	,	kIx,
pojmenovávají	pojmenovávat	k5eAaImIp3nP
čísla	číslo	k1gNnPc4
(	(	kIx(
<g/>
tři	tři	k4xCgNnPc1
<g/>
,	,	kIx,
polovina	polovina	k1gFnSc1
<g/>
,	,	kIx,
dvě	dva	k4xCgFnPc4
stě	sto	k4xCgFnPc1
třicet	třicet	k4xCc1
dva	dva	k4xCgInPc4
<g/>
,	,	kIx,
několik	několik	k4yIc4
<g/>
,	,	kIx,
mnoho	mnoho	k4c4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
gramaticky	gramaticky	k6eAd1
jsou	být	k5eAaImIp3nP
přibližně	přibližně	k6eAd1
v	v	k7c6
roli	role	k1gFnSc6
podstatného	podstatný	k2eAgNnSc2d1
jména	jméno	k1gNnSc2
</s>
<s>
řadové	řadový	k2eAgInPc4d1
(	(	kIx(
<g/>
ordinální	ordinální	k2eAgInPc4d1
<g/>
)	)	kIx)
–	–	k?
označují	označovat	k5eAaImIp3nP
pořadí	pořadí	k1gNnSc1
(	(	kIx(
<g/>
osmý	osmý	k4xOgMnSc1
<g/>
,	,	kIx,
poněkolikáté	poněkolikáté	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zpravidla	zpravidla	k6eAd1
ve	v	k7c6
funkci	funkce	k1gFnSc6
přídavného	přídavný	k2eAgNnSc2d1
jména	jméno	k1gNnSc2
nebo	nebo	k8xC
příslovce	příslovce	k1gNnSc2
</s>
<s>
souborové	souborový	k2eAgInPc1d1
–	–	k?
označují	označovat	k5eAaImIp3nP
počet	počet	k1gInSc4
souborů	soubor	k1gInPc2
(	(	kIx(
<g/>
dvoje	dvoje	k4xRgFnPc1
ponožky	ponožka	k1gFnPc1
<g/>
,	,	kIx,
patero	patero	k1gNnSc1
nářadí	nářadí	k1gNnSc2
<g/>
,	,	kIx,
troje	troje	k4xRgNnSc4
povlečení	povlečení	k1gNnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
též	též	k9
vyjadřují	vyjadřovat	k5eAaImIp3nP
počet	počet	k1gInSc4
věcí	věc	k1gFnPc2
vyjádřených	vyjádřený	k2eAgFnPc2d1
pomnožným	pomnožný	k2eAgInSc7d1
tvarem	tvar	k1gInSc7
<g/>
,	,	kIx,
syntakticky	syntakticky	k6eAd1
jsou	být	k5eAaImIp3nP
obdobné	obdobný	k2eAgInPc1d1
základním	základní	k2eAgInSc7d1
číslovkám	číslovka	k1gFnPc3
</s>
<s>
druhové	druh	k1gMnPc1
–	–	k?
označují	označovat	k5eAaImIp3nP
počet	počet	k1gInSc4
druhů	druh	k1gInPc2
(	(	kIx(
<g/>
dvojí	dvojit	k5eAaImIp3nP
<g/>
,	,	kIx,
sterý	sterý	k4xRgInSc1
<g/>
,	,	kIx,
pod	pod	k7c7
obojí	oboj	k1gFnSc7
způsobou	způsoba	k1gFnSc7
<g/>
,	,	kIx,
dvojí	dvojí	k4xRgInSc4
lid	lid	k1gInSc4
<g/>
,	,	kIx,
trojí	trojit	k5eAaImIp3nS
lid	lid	k1gInSc1
<g/>
,	,	kIx,
doktor	doktor	k1gMnSc1
obojího	obojí	k4xRgMnSc2
práva	právo	k1gNnSc2
=	=	kIx~
JUDr.	JUDr.	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
češtině	čeština	k1gFnSc6
převážně	převážně	k6eAd1
nepoužívané	používaný	k2eNgFnPc4d1
</s>
<s>
násobné	násobný	k2eAgInPc4d1
(	(	kIx(
<g/>
multiplikativní	multiplikativní	k2eAgInPc4d1
<g/>
)	)	kIx)
–	–	k?
vyjadřují	vyjadřovat	k5eAaImIp3nP
násobek	násobek	k1gInSc4
čísla	číslo	k1gNnSc2
(	(	kIx(
<g/>
trojnásobný	trojnásobný	k2eAgInSc1d1
<g/>
,	,	kIx,
pětkrát	pětkrát	k6eAd1
<g/>
,	,	kIx,
mnohokrát	mnohokrát	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zpravidla	zpravidla	k6eAd1
ve	v	k7c6
funkci	funkce	k1gFnSc6
přídavného	přídavný	k2eAgNnSc2d1
jména	jméno	k1gNnSc2
nebo	nebo	k8xC
příslovce	příslovce	k1gNnSc2
</s>
<s>
podílné	podílný	k2eAgFnPc4d1
(	(	kIx(
<g/>
distributivní	distributivní	k2eAgFnPc4d1
<g/>
)	)	kIx)
-	-	kIx~
vyjadřují	vyjadřovat	k5eAaImIp3nP
číselný	číselný	k2eAgInSc4d1
podíl	podíl	k1gInSc4
(	(	kIx(
<g/>
po	po	k7c6
dvou	dva	k4xCgInPc6
<g/>
,	,	kIx,
po	po	k7c6
několika	několik	k4yIc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zpravidla	zpravidla	k6eAd1
ve	v	k7c6
funkci	funkce	k1gFnSc6
příslovce	příslovce	k1gNnSc2
</s>
<s>
Číslovky	číslovka	k1gFnPc1
v	v	k7c6
češtině	čeština	k1gFnSc6
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
České	český	k2eAgFnSc2d1
číslovky	číslovka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Číslovky	číslovka	k1gFnPc1
v	v	k7c6
češtině	čeština	k1gFnSc6
podléhají	podléhat	k5eAaImIp3nP
ohýbání	ohýbání	k1gNnSc4
a	a	k8xC
ve	v	k7c6
větě	věta	k1gFnSc6
nejčastěji	často	k6eAd3
plní	plnit	k5eAaImIp3nS
roli	role	k1gFnSc4
přívlastku	přívlastek	k1gInSc2
shodného	shodný	k2eAgInSc2d1
a	a	k8xC
určuje	určovat	k5eAaImIp3nS
se	se	k3xPyFc4
u	u	k7c2
nich	on	k3xPp3gFnPc2
gramatická	gramatický	k2eAgFnSc1d1
kategorie	kategorie	k1gFnSc1
pádu	pád	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgFnSc2
číslovky	číslovka	k1gFnSc2
ale	ale	k8xC
nejsou	být	k5eNaImIp3nP
jmény	jméno	k1gNnPc7
<g/>
,	,	kIx,
ale	ale	k8xC
mají	mít	k5eAaImIp3nP
funkci	funkce	k1gFnSc4
příslovce	příslovce	k1gNnSc1
(	(	kIx(
<g/>
např.	např.	kA
třikrát	třikrát	k6eAd1
<g/>
,	,	kIx,
poněkolikáté	poněkolikáté	k6eAd1
<g/>
,	,	kIx,
milionkrát	milionkrát	k6eAd1
<g/>
,	,	kIx,
...	...	k?
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
flexi	flexe	k1gFnSc4
tedy	tedy	k9
nepodléhají	podléhat	k5eNaImIp3nP
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
číslovka	číslovka	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
číslovka	číslovka	k1gFnSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Kategorie	kategorie	k1gFnPc1
Číslovky	číslovka	k1gFnSc2
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Slovní	slovní	k2eAgInPc1d1
druhy	druh	k1gInPc1
</s>
<s>
podstatné	podstatný	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
•	•	k?
přídavné	přídavný	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
•	•	k?
zájmeno	zájmeno	k1gNnSc4
•	•	k?
číslovka	číslovka	k1gFnSc1
•	•	k?
sloveso	sloveso	k1gNnSc4
•	•	k?
příslovce	příslovce	k1gNnSc2
•	•	k?
předložka	předložka	k1gFnSc1
•	•	k?
spojka	spojka	k1gFnSc1
•	•	k?
částice	částice	k1gFnSc1
•	•	k?
citoslovce	citoslovce	k1gNnSc1
jméno	jméno	k1gNnSc4
</s>
<s>
obecné	obecný	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
vlastní	vlastní	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
bionymum	bionymum	k1gInSc1
</s>
<s>
antroponymum	antroponymum	k1gNnSc1
(	(	kIx(
<g/>
rodné	rodný	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
/	/	kIx~
křestní	křestní	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
•	•	k?
hypokoristikon	hypokoristikon	k1gNnSc1
•	•	k?
příjmí	příjmí	k?
•	•	k?
příjmení	příjmení	k1gNnSc2
•	•	k?
přezdívka	přezdívka	k1gFnSc1
•	•	k?
jméno	jméno	k1gNnSc1
po	po	k7c6
chalupě	chalupa	k1gFnSc6
•	•	k?
fiktonymum	fiktonymum	k1gInSc1
(	(	kIx(
<g/>
pseudonym	pseudonym	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
obyvatelské	obyvatelský	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
•	•	k?
rodinné	rodinný	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
•	•	k?
rodové	rodový	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
•	•	k?
etnonymum	etnonymum	k1gNnSc1
•	•	k?
theonymum	theonymum	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
zoonymum	zoonymum	k1gInSc1
•	•	k?
fytonymum	fytonymum	k1gInSc1
abionymum	abionymum	k1gInSc1
</s>
<s>
toponymum	toponymum	k1gNnSc1
(	(	kIx(
<g/>
choronymum	choronymum	k1gInSc1
•	•	k?
oikonymum	oikonymum	k1gInSc1
•	•	k?
anoikonymum	anoikonymum	k1gInSc1
–	–	k?
urbanonymum	urbanonymum	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
kosmonymum	kosmonymum	k1gInSc1
/	/	kIx~
astronymum	astronymum	k1gInSc1
•	•	k?
chrématonymum	chrématonymum	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
exonymum	exonymum	k1gInSc1
•	•	k?
endonymum	endonymum	k1gInSc1
•	•	k?
cizí	cizit	k5eAaImIp3nS
jméno	jméno	k1gNnSc4
•	•	k?
standardizované	standardizovaný	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4170445-9	4170445-9	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
6855	#num#	k4
</s>
