<s>
Aurore	Auror	k1gMnSc5
(	(	kIx(
<g/>
automobilka	automobilka	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
AuroreZákladní	AuroreZákladný	k2eAgMnPc1d1
údaje	údaj	k1gInSc2
Datum	datum	k1gNnSc1
založení	založení	k1gNnSc2
</s>
<s>
1898	#num#	k4
Datum	datum	k1gNnSc1
zániku	zánik	k1gInSc2
</s>
<s>
1916	#num#	k4
Adresa	adresa	k1gFnSc1
sídla	sídlo	k1gNnSc2
</s>
<s>
Budapešť	Budapešť	k1gFnSc1
<g/>
,	,	kIx,
Maďarsko	Maďarsko	k1gNnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Aurore	Auror	k1gMnSc5
Quadricycle	Quadricycl	k1gMnSc5
(	(	kIx(
<g/>
1902	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Aurore	Auror	k1gMnSc5
Quadricycle	Quadricycl	k1gMnSc5
</s>
<s>
Aurore	Auror	k1gMnSc5
byl	být	k5eAaImAgMnS
rakousko-uherský	rakousko-uherský	k2eAgMnSc1d1
výrobce	výrobce	k1gMnSc1
jízdních	jízdní	k2eAgFnPc2d1
kol	kola	k1gFnPc2
a	a	k8xC
automobilů	automobil	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
firmy	firma	k1gFnSc2
</s>
<s>
Společnost	společnost	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
založil	založit	k5eAaPmAgMnS
závodní	závodní	k2eAgMnSc1d1
cyklista	cyklista	k1gMnSc1
a	a	k8xC
konstruktér	konstruktér	k1gMnSc1
Nándor	Nándor	k1gMnSc1
Hóra	Hóra	k1gFnSc1
(	(	kIx(
<g/>
maďarsky	maďarsky	k6eAd1
Hóra	Hóra	k1gFnSc1
Nándor	Nándora	k1gFnPc2
<g/>
;	;	kIx,
30	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1876	#num#	k4
–	–	k?
30	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1963	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
,	,	kIx,
se	se	k3xPyFc4
v	v	k7c6
Budapešti	Budapešť	k1gFnSc6
zabývala	zabývat	k5eAaImAgFnS
nejprve	nejprve	k6eAd1
výrobou	výroba	k1gFnSc7
jízdních	jízdní	k2eAgFnPc2d1
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1898	#num#	k4
až	až	k9
1916	#num#	k4
vyráběla	vyrábět	k5eAaImAgFnS
automobily	automobil	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Vozidla	vozidlo	k1gNnPc1
</s>
<s>
Na	na	k7c4
zakázku	zakázka	k1gFnSc4
budapešťského	budapešťský	k2eAgInSc2d1
poštovního	poštovní	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
byla	být	k5eAaImAgNnP
vyvinuta	vyvinut	k2eAgNnPc1d1
malá	malý	k2eAgNnPc1d1
vozítka	vozítko	k1gNnPc1
pro	pro	k7c4
doručování	doručování	k1gNnSc4
balíků	balík	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zpočátku	zpočátku	k6eAd1
<g/>
,	,	kIx,
do	do	k7c2
roku	rok	k1gInSc2
1902	#num#	k4
<g/>
,	,	kIx,
šlo	jít	k5eAaImAgNnS
o	o	k7c4
tříkolky	tříkolka	k1gFnPc4
<g/>
,	,	kIx,
poté	poté	k6eAd1
čtyřkolová	čtyřkolový	k2eAgNnPc1d1
vozítka	vozítko	k1gNnPc1
podobající	podobající	k2eAgNnPc1d1
se	se	k3xPyFc4
vozům	vůz	k1gInPc3
Benz	Benz	k1gInSc4
Velo	velo	k1gNnSc1
nebo	nebo	k8xC
Quadricycle	Quadricycle	k1gNnSc1
firmy	firma	k1gFnSc2
De	De	k?
Dion-Bouton	Dion-Bouton	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Aurore	Auror	k1gInSc5
(	(	kIx(
<g/>
Automarke	Automarke	k1gNnPc6
<g/>
)	)	kIx)
na	na	k7c6
německé	německý	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
HÓRA	Hóra	k1gFnSc1
NÁNDOR	NÁNDOR	kA
-	-	kIx~
Kerékpár	Kerékpár	k1gMnSc1
Különlegességek	Különlegességek	k1gMnSc1
<g/>
,	,	kIx,
údaje	údaj	k1gInPc4
z	z	k7c2
MAGYAR	MAGYAR	kA
ÉLETRAJZI	ÉLETRAJZI	kA
LEXIKON	lexikon	k1gInSc4
1000	#num#	k4
<g/>
–	–	k?
<g/>
1990	#num#	k4
<g/>
,	,	kIx,
(	(	kIx(
<g/>
maďarsky	maďarsky	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
2017-01-02	2017-01-02	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Harald	Harald	k1gMnSc1
Linz	Linz	k1gMnSc1
<g/>
,	,	kIx,
Halwart	Halwart	k1gInSc1
Schrader	Schrader	k1gInSc1
<g/>
:	:	kIx,
Die	Die	k1gFnSc1
Internationale	Internationale	k1gFnSc1
Automobil-Enzyklopädie	Automobil-Enzyklopädie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
United	United	k1gMnSc1
Soft	Soft	k?
Media	medium	k1gNnSc2
Verlag	Verlaga	k1gFnPc2
<g/>
,	,	kIx,
München	Münchna	k1gFnPc2
2008	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
8032	#num#	k4
<g/>
-	-	kIx~
<g/>
9876	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Harald	Harald	k1gMnSc1
H.	H.	kA
Linz	Linz	k1gMnSc1
<g/>
,	,	kIx,
Halwart	Halwart	k1gInSc1
Schrader	Schrader	k1gInSc1
<g/>
:	:	kIx,
Die	Die	k1gFnSc1
große	große	k1gFnSc1
Automobil-Enzyklopädie	Automobil-Enzyklopädie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
BLV	BLV	kA
<g/>
,	,	kIx,
München	München	k1gInSc1
1986	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
3	#num#	k4
<g/>
-	-	kIx~
<g/>
405	#num#	k4
<g/>
-	-	kIx~
<g/>
12974	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Aurore	Auror	k1gInSc5
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
GTÜ	GTÜ	kA
Gesellschaft	Gesellschaft	k1gMnSc1
für	für	k?
Technische	Technische	k1gInSc1
Überwachung	Überwachung	k1gInSc1
mbH	mbH	k?
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Rakousko-uherské	rakousko-uherský	k2eAgFnSc2d1
automobilové	automobilový	k2eAgFnSc2d1
značky	značka	k1gFnSc2
</s>
<s>
Achilles	Achilles	k1gMnSc1
•	•	k?
Adam	Adam	k1gMnSc1
•	•	k?
Adler	Adler	k1gMnSc1
•	•	k?
Aero	aero	k1gNnSc4
•	•	k?
Aesculap	Aesculap	k1gInSc1
•	•	k?
Alba	alba	k1gFnSc1
•	•	k?
Albl	Albl	k1gInSc1
•	•	k?
Aurore	Auror	k1gInSc5
•	•	k?
Austro-Cyclecar	Austro-Cyclecar	k1gMnSc1
•	•	k?
Austro-Daimler	Austro-Daimler	k1gMnSc1
•	•	k?
Austro-Fiat	Austro-Fiat	k1gInSc1
•	•	k?
Bock	Bock	k1gInSc1
&	&	k?
Hollender	Hollender	k1gInSc1
•	•	k?
Bohrer	Bohrer	k1gInSc4
•	•	k?
Bory	bor	k1gInPc4
•	•	k?
Braun	Braun	k1gMnSc1
•	•	k?
Celeritas	Celeritas	k1gMnSc1
•	•	k?
Continental	Continental	k1gMnSc1
•	•	k?
Csonka	Csonka	k1gMnSc1
•	•	k?
Czech	Czech	k1gMnSc1
•	•	k?
Dedics	Dedics	k1gInSc1
•	•	k?
Dietrich	Dietrich	k1gMnSc1
&	&	k?
Urban	Urban	k1gMnSc1
•	•	k?
Egger-Lohner	Egger-Lohner	k1gMnSc1
•	•	k?
Elektrotechnická	elektrotechnický	k2eAgFnSc1d1
•	•	k?
ESA	eso	k1gNnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Friedmann-Knoller	Friedmann-Knoller	k1gMnSc1
•	•	k?
Ganz	Ganz	k1gMnSc1
•	•	k?
Gräf	Gräf	k1gMnSc1
•	•	k?
Gräf	Gräf	k1gMnSc1
&	&	k?
Stift	Stift	k1gMnSc1
•	•	k?
Hudec	Hudec	k1gMnSc1
•	•	k?
Kainz	Kainz	k1gMnSc1
•	•	k?
K.A.	K.A.	k1gMnSc1
<g/>
N.	N.	kA
•	•	k?
Kohout	Kohout	k1gMnSc1
•	•	k?
Křižík	Křižík	k1gMnSc1
•	•	k?
Kronos	Kronos	k1gMnSc1
•	•	k?
Lamprecht	Lamprecht	k1gMnSc1
•	•	k?
Laurin	Laurin	k1gInSc1
&	&	k?
Klement	Klement	k1gMnSc1
•	•	k?
Leesdorfer	Leesdorfer	k1gMnSc1
•	•	k?
Linser	Linser	k1gMnSc1
•	•	k?
Lohner	Lohner	k1gMnSc1
•	•	k?
Lohner-Porsche	Lohner-Porsche	k1gInSc1
•	•	k?
MAG	Maga	k1gFnPc2
•	•	k?
Maja	Maja	k1gFnSc1
•	•	k?
Mars	Mars	k1gInSc1
•	•	k?
Marta	Marta	k1gFnSc1
•	•	k?
Müller	Müller	k1gInSc1
•	•	k?
MWF	MWF	kA
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Nesselsdorf	Nesselsdorf	k1gMnSc1
•	•	k?
Neuhaus	Neuhaus	k1gMnSc1
&	&	k?
Pauer	Pauer	k1gMnSc1
•	•	k?
Niesner	Niesner	k1gMnSc1
•	•	k?
Omega	omega	k1gFnSc1
•	•	k?
Orion	orion	k1gInSc1
•	•	k?
PAT-PAF	PAT-PAF	k1gFnSc1
•	•	k?
Perfekt	perfektum	k1gNnPc2
•	•	k?
Phoenix	Phoenix	k1gInSc1
•	•	k?
Praga	Praga	k1gFnSc1
•	•	k?
Premier	Premier	k1gInSc1
•	•	k?
Puch	puch	k1gInSc1
•	•	k?
Rába	Ráb	k1gInSc2
•	•	k?
RAF	raf	k0
•	•	k?
Reform	Reform	k1gInSc1
•	•	k?
Regent	regent	k1gMnSc1
•	•	k?
Röck	Röck	k1gMnSc1
•	•	k?
Rösler	Rösler	k1gMnSc1
&	&	k?
Jauernig	Jauernig	k1gMnSc1
•	•	k?
Simmeringer	Simmeringer	k1gMnSc1
•	•	k?
Spitz	Spitz	k1gMnSc1
•	•	k?
Thein	thein	k1gInSc1
&	&	k?
Goldberger	Goldberger	k1gMnSc1
•	•	k?
Theyer	Theyer	k1gMnSc1
&	&	k?
Rothmund	Rothmund	k1gMnSc1
•	•	k?
Torpedo	Torpedo	k1gNnSc4
•	•	k?
Turicum	Turicum	k1gInSc1
•	•	k?
Věchet	věchet	k1gInSc1
•	•	k?
Velox	Velox	k1gInSc1
•	•	k?
WAF	WAF	kA
•	•	k?
Walter	Walter	k1gMnSc1
•	•	k?
Weiser	Weiser	k1gMnSc1
•	•	k?
Wiener	Wiener	k1gInSc1
Automobil	automobil	k1gInSc1
•	•	k?
Wohl	Wohl	k1gInSc1
Mag	Maga	k1gFnPc2
•	•	k?
Wyner	Wyner	k1gMnSc1
•	•	k?
Zimmermann	Zimmermann	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Automobil	automobil	k1gInSc1
|	|	kIx~
Cyklistika	cyklistika	k1gFnSc1
|	|	kIx~
Maďarsko	Maďarsko	k1gNnSc1
|	|	kIx~
Rakousko	Rakousko	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
VIAF	VIAF	kA
<g/>
:	:	kIx,
134269604	#num#	k4
</s>
