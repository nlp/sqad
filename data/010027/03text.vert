<p>
<s>
Bertrand	Bertrand	k1gInSc1	Bertrand
Arthur	Arthura	k1gFnPc2	Arthura
William	William	k1gInSc1	William
Russell	Russell	k1gMnSc1	Russell
(	(	kIx(	(
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1872	[number]	k4	1872
<g/>
,	,	kIx,	,
Trelleck	Trelleck	k1gInSc1	Trelleck
<g/>
,	,	kIx,	,
Monmouthshire	Monmouthshir	k1gMnSc5	Monmouthshir
<g/>
,	,	kIx,	,
Wales	Wales	k1gInSc1	Wales
–	–	k?	–
2	[number]	k4	2
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1970	[number]	k4	1970
<g/>
,	,	kIx,	,
Penrhyndeudraeth	Penrhyndeudraeth	k1gInSc4	Penrhyndeudraeth
<g/>
,	,	kIx,	,
Gwynedd	Gwynedd	k1gInSc4	Gwynedd
(	(	kIx(	(
<g/>
Merionethshire	Merionethshir	k1gMnSc5	Merionethshir
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Wales	Wales	k1gInSc1	Wales
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgMnSc1	třetí
hrabě	hrabě	k1gMnSc1	hrabě
Russell	Russell	k1gMnSc1	Russell
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
britský	britský	k2eAgMnSc1d1	britský
matematik	matematik	k1gMnSc1	matematik
<g/>
,	,	kIx,	,
filosof	filosof	k1gMnSc1	filosof
<g/>
,	,	kIx,	,
logik	logik	k1gMnSc1	logik
<g/>
,	,	kIx,	,
sociální	sociální	k2eAgMnSc1d1	sociální
kritik	kritik	k1gMnSc1	kritik
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
za	za	k7c4	za
rok	rok	k1gInSc4	rok
1950	[number]	k4	1950
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
matematice	matematika	k1gFnSc6	matematika
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
svým	svůj	k3xOyFgInSc7	svůj
paradoxem	paradox	k1gInSc7	paradox
v	v	k7c4	v
naivní	naivní	k2eAgFnSc4d1	naivní
teorii	teorie	k1gFnSc4	teorie
množin	množina	k1gFnPc2	množina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Bertrand	Bertrand	k1gInSc1	Bertrand
Arthur	Arthura	k1gFnPc2	Arthura
Wiliam	Wiliam	k1gInSc1	Wiliam
Russell	Russell	k1gMnSc1	Russell
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
roku	rok	k1gInSc2	rok
1872	[number]	k4	1872
<g/>
.	.	kIx.	.
</s>
<s>
Pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
aristokratického	aristokratický	k2eAgNnSc2d1	aristokratické
prostředí	prostředí	k1gNnSc2	prostředí
s	s	k7c7	s
významnými	významný	k2eAgFnPc7d1	významná
politickými	politický	k2eAgFnPc7d1	politická
vazbami	vazba	k1gFnPc7	vazba
(	(	kIx(	(
<g/>
jeho	jeho	k3xOp3gMnSc1	jeho
děd	děd	k1gMnSc1	děd
John	John	k1gMnSc1	John
Russell	Russell	k1gMnSc1	Russell
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
hrabě	hrabě	k1gMnSc1	hrabě
Russell	Russell	k1gMnSc1	Russell
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
dvakrát	dvakrát	k6eAd1	dvakrát
britským	britský	k2eAgMnSc7d1	britský
premiérem	premiér	k1gMnSc7	premiér
v	v	k7c6	v
letech	let	k1gInPc6	let
1846	[number]	k4	1846
<g/>
–	–	k?	–
<g/>
1852	[number]	k4	1852
a	a	k8xC	a
1865	[number]	k4	1865
<g/>
–	–	k?	–
<g/>
1866	[number]	k4	1866
<g/>
)	)	kIx)	)
a	a	k8xC	a
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
svého	svůj	k3xOyFgMnSc2	svůj
staršího	starý	k2eAgMnSc2d2	starší
bratra	bratr	k1gMnSc2	bratr
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1931	[number]	k4	1931
stal	stát	k5eAaPmAgInS	stát
3	[number]	k4	3
<g/>
.	.	kIx.	.
hrabětem	hrabě	k1gMnSc7	hrabě
Russellem	Russell	k1gMnSc7	Russell
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
věku	věk	k1gInSc6	věk
tří	tři	k4xCgNnPc2	tři
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Russell	Russellum	k1gNnPc2	Russellum
sirotkem	sirotek	k1gMnSc7	sirotek
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
vychováván	vychovávat	k5eAaImNgInS	vychovávat
právě	právě	k6eAd1	právě
svým	svůj	k3xOyFgMnSc7	svůj
dědečkem	dědeček	k1gMnSc7	dědeček
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
prokázal	prokázat	k5eAaPmAgInS	prokázat
své	svůj	k3xOyFgFnPc4	svůj
schopnosti	schopnost	k1gFnPc4	schopnost
pro	pro	k7c4	pro
matematiku	matematika	k1gFnSc4	matematika
a	a	k8xC	a
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
filozofii	filozofie	k1gFnSc4	filozofie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
absolvování	absolvování	k1gNnSc6	absolvování
Cambridgeské	cambridgeský	k2eAgFnSc2d1	Cambridgeská
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1894	[number]	k4	1894
(	(	kIx(	(
<g/>
studoval	studovat	k5eAaImAgMnS	studovat
zde	zde	k6eAd1	zde
matematiku	matematika	k1gFnSc4	matematika
a	a	k8xC	a
etiku	etika	k1gFnSc4	etika
<g/>
)	)	kIx)	)
pracoval	pracovat	k5eAaImAgMnS	pracovat
Russel	Russel	k1gMnSc1	Russel
krátce	krátce	k6eAd1	krátce
na	na	k7c6	na
britské	britský	k2eAgFnSc6d1	britská
ambasádě	ambasáda	k1gFnSc6	ambasáda
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1895	[number]	k4	1895
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
na	na	k7c4	na
Cambridgeskou	cambridgeský	k2eAgFnSc4d1	Cambridgeská
univerzitu	univerzita	k1gFnSc4	univerzita
a	a	k8xC	a
působil	působit	k5eAaImAgMnS	působit
zde	zde	k6eAd1	zde
na	na	k7c4	na
Trinity	Trinit	k2eAgInPc4d1	Trinit
College	Colleg	k1gInPc4	Colleg
jako	jako	k9	jako
odborný	odborný	k2eAgMnSc1d1	odborný
asistent	asistent	k1gMnSc1	asistent
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Proti	proti	k7c3	proti
přání	přání	k1gNnSc3	přání
své	svůj	k3xOyFgFnSc2	svůj
rodiny	rodina	k1gFnSc2	rodina
se	se	k3xPyFc4	se
Russell	Russell	k1gMnSc1	Russell
roku	rok	k1gInSc2	rok
1894	[number]	k4	1894
oženil	oženit	k5eAaPmAgInS	oženit
s	s	k7c7	s
americkou	americký	k2eAgFnSc7d1	americká
kvakerkou	kvakerka	k1gFnSc7	kvakerka
Alys	Alysa	k1gFnPc2	Alysa
Persall	Persall	k1gInSc4	Persall
Smithovou	Smithová	k1gFnSc4	Smithová
a	a	k8xC	a
odešel	odejít	k5eAaPmAgMnS	odejít
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
do	do	k7c2	do
Berlína	Berlín	k1gInSc2	Berlín
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
studoval	studovat	k5eAaImAgMnS	studovat
ekonomii	ekonomie	k1gFnSc3	ekonomie
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
také	také	k9	také
roku	rok	k1gInSc2	rok
1896	[number]	k4	1896
napsal	napsat	k5eAaBmAgInS	napsat
dílo	dílo	k1gNnSc4	dílo
Německá	německý	k2eAgFnSc1d1	německá
sociální	sociální	k2eAgFnSc1d1	sociální
demokracie	demokracie	k1gFnSc1	demokracie
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
ze	z	k7c2	z
svých	svůj	k3xOyFgMnPc2	svůj
více	hodně	k6eAd2	hodně
než	než	k8xS	než
devadesáti	devadesát	k4xCc2	devadesát
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
vyšla	vyjít	k5eAaPmAgFnS	vyjít
Russellova	Russellův	k2eAgFnSc1d1	Russellova
dizertační	dizertační	k2eAgFnSc1d1	dizertační
práce	práce	k1gFnSc1	práce
Esej	esej	k1gFnSc1	esej
o	o	k7c6	o
základech	základ	k1gInPc6	základ
geometrie	geometrie	k1gFnSc2	geometrie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k8xS	jako
mladý	mladý	k2eAgMnSc1d1	mladý
muž	muž	k1gMnSc1	muž
se	se	k3xPyFc4	se
Russell	Russell	k1gInSc4	Russell
přikláněl	přiklánět	k5eAaImAgInS	přiklánět
k	k	k7c3	k
platónismu	platónismus	k1gInSc3	platónismus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
další	další	k2eAgInSc1d1	další
jeho	jeho	k3xOp3gInSc1	jeho
filozofický	filozofický	k2eAgInSc1d1	filozofický
vývoj	vývoj	k1gInSc1	vývoj
ho	on	k3xPp3gMnSc4	on
však	však	k9	však
řadí	řadit	k5eAaImIp3nS	řadit
spíše	spíše	k9	spíše
do	do	k7c2	do
novopozitivistické	novopozitivistický	k2eAgFnSc2d1	novopozitivistický
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Russel	Russel	k1gMnSc1	Russel
vycházel	vycházet	k5eAaImAgMnS	vycházet
z	z	k7c2	z
názoru	názor	k1gInSc2	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
empirickou	empirický	k2eAgFnSc7d1	empirická
zkušeností	zkušenost	k1gFnSc7	zkušenost
lze	lze	k6eAd1	lze
bezprostředně	bezprostředně	k6eAd1	bezprostředně
poznávat	poznávat	k5eAaImF	poznávat
ideje	idea	k1gFnPc4	idea
či	či	k8xC	či
univerzálie	univerzálie	k1gFnPc4	univerzálie
<g/>
.	.	kIx.	.
</s>
<s>
Jediný	jediný	k2eAgMnSc1d1	jediný
skutečně	skutečně	k6eAd1	skutečně
účinný	účinný	k2eAgInSc4d1	účinný
nástroj	nástroj	k1gInSc4	nástroj
poznávání	poznávání	k1gNnSc2	poznávání
viděl	vidět	k5eAaImAgMnS	vidět
proto	proto	k8xC	proto
Russell	Russell	k1gMnSc1	Russell
v	v	k7c6	v
přírodních	přírodní	k2eAgFnPc6d1	přírodní
vědách	věda	k1gFnPc6	věda
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
podle	podle	k7c2	podle
něho	on	k3xPp3gMnSc2	on
vzorem	vzor	k1gInSc7	vzor
filozofie	filozofie	k1gFnSc1	filozofie
a	a	k8xC	a
nikoliv	nikoliv	k9	nikoliv
morálka	morálka	k1gFnSc1	morálka
nebo	nebo	k8xC	nebo
náboženství	náboženství	k1gNnSc1	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Filozofie	filozofie	k1gFnSc1	filozofie
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
pracovat	pracovat	k5eAaImF	pracovat
jen	jen	k9	jen
v	v	k7c6	v
těch	ten	k3xDgFnPc6	ten
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
dosud	dosud	k6eAd1	dosud
nejsou	být	k5eNaImIp3nP	být
zpřístupněny	zpřístupněn	k2eAgFnPc1d1	zpřístupněna
exaktnímu	exaktní	k2eAgNnSc3d1	exaktní
přírodovědeckému	přírodovědecký	k2eAgNnSc3d1	Přírodovědecké
bádání	bádání	k1gNnSc3	bádání
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
zde	zde	k6eAd1	zde
může	moct	k5eAaImIp3nS	moct
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
problémy	problém	k1gInPc4	problém
poukazovat	poukazovat	k5eAaImF	poukazovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
nikoli	nikoli	k9	nikoli
je	být	k5eAaImIp3nS	být
řešit	řešit	k5eAaImF	řešit
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
svých	svůj	k3xOyFgFnPc6	svůj
úvahách	úvaha	k1gFnPc6	úvaha
o	o	k7c6	o
problematice	problematika	k1gFnSc6	problematika
smyslu	smysl	k1gInSc2	smysl
a	a	k8xC	a
pravdivosti	pravdivost	k1gFnPc4	pravdivost
pak	pak	k6eAd1	pak
Russel	Russel	k1gInSc1	Russel
svůj	svůj	k3xOyFgInSc4	svůj
empirismus	empirismus	k1gInSc4	empirismus
dále	daleko	k6eAd2	daleko
rozvíjel	rozvíjet	k5eAaImAgInS	rozvíjet
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
koncepci	koncepce	k1gFnSc6	koncepce
logického	logický	k2eAgInSc2d1	logický
atomismu	atomismus	k1gInSc2	atomismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Svým	svůj	k3xOyFgInSc7	svůj
odborným	odborný	k2eAgInSc7d1	odborný
dílem	díl	k1gInSc7	díl
se	se	k3xPyFc4	se
Russell	Russell	k1gMnSc1	Russell
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
o	o	k7c4	o
rozvoj	rozvoj	k1gInSc4	rozvoj
matematické	matematický	k2eAgFnSc2d1	matematická
logiky	logika	k1gFnSc2	logika
<g/>
.	.	kIx.	.
</s>
<s>
Přispěly	přispět	k5eAaPmAgFnP	přispět
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
především	především	k9	především
jeho	jeho	k3xOp3gFnSc2	jeho
práce	práce	k1gFnSc2	práce
Principy	princip	k1gInPc1	princip
matematiky	matematika	k1gFnSc2	matematika
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1903	[number]	k4	1903
a	a	k8xC	a
třídílná	třídílný	k2eAgNnPc1d1	třídílné
Principia	principium	k1gNnPc1	principium
Mathematica	Mathematic	k2eAgNnPc1d1	Mathematic
(	(	kIx(	(
<g/>
1910	[number]	k4	1910
<g/>
–	–	k?	–
<g/>
1913	[number]	k4	1913
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
napsal	napsat	k5eAaPmAgMnS	napsat
společně	společně	k6eAd1	společně
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
učitelem	učitel	k1gMnSc7	učitel
z	z	k7c2	z
Cambridge	Cambridge	k1gFnSc2	Cambridge
Alfredem	Alfred	k1gMnSc7	Alfred
North	North	k1gInSc1	North
Whiteheadem	Whitehead	k1gInSc7	Whitehead
<g/>
.	.	kIx.	.
</s>
<s>
Exaktní	exaktní	k2eAgInPc4d1	exaktní
myšlenkové	myšlenkový	k2eAgInPc4d1	myšlenkový
postupy	postup	k1gInPc4	postup
aplikoval	aplikovat	k5eAaBmAgMnS	aplikovat
také	také	k9	také
v	v	k7c6	v
humanitních	humanitní	k2eAgFnPc6d1	humanitní
vědách	věda	k1gFnPc6	věda
a	a	k8xC	a
inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
americkou	americký	k2eAgFnSc4d1	americká
behavioristickou	behavioristický	k2eAgFnSc4d1	behavioristická
psychologii	psychologie	k1gFnSc4	psychologie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
dospěl	dochvít	k5eAaPmAgInS	dochvít
Russell	Russell	k1gInSc1	Russell
k	k	k7c3	k
nezvratnému	zvratný	k2eNgInSc3d1	nezvratný
pacifismu	pacifismus	k1gInSc3	pacifismus
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
prosazování	prosazování	k1gNnSc2	prosazování
jej	on	k3xPp3gMnSc4	on
přivedlo	přivést	k5eAaPmAgNnS	přivést
k	k	k7c3	k
občanské	občanský	k2eAgFnSc3d1	občanská
neposlušnosti	neposlušnost	k1gFnSc3	neposlušnost
a	a	k8xC	a
nakrátko	nakrátko	k6eAd1	nakrátko
i	i	k9	i
do	do	k7c2	do
vězení	vězení	k1gNnSc2	vězení
<g/>
.	.	kIx.	.
</s>
<s>
Působil	působit	k5eAaImAgMnS	působit
rovněž	rovněž	k9	rovněž
jako	jako	k9	jako
svobodomyslný	svobodomyslný	k2eAgMnSc1d1	svobodomyslný
pedagog	pedagog	k1gMnSc1	pedagog
a	a	k8xC	a
v	v	k7c6	v
letech	let	k1gInPc6	let
1927	[number]	k4	1927
<g/>
–	–	k?	–
<g/>
1932	[number]	k4	1932
vedl	vést	k5eAaImAgInS	vést
experimentální	experimentální	k2eAgFnSc4d1	experimentální
školu	škola	k1gFnSc4	škola
v	v	k7c6	v
Sussexu	Sussex	k1gInSc6	Sussex
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
byl	být	k5eAaImAgMnS	být
činný	činný	k2eAgMnSc1d1	činný
v	v	k7c6	v
kampani	kampaň	k1gFnSc6	kampaň
za	za	k7c4	za
jaderné	jaderný	k2eAgNnSc4d1	jaderné
odzbrojení	odzbrojení	k1gNnSc4	odzbrojení
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
ctěn	ctěn	k2eAgMnSc1d1	ctěn
jako	jako	k8xS	jako
duchovní	duchovní	k2eAgMnSc1d1	duchovní
otec	otec	k1gMnSc1	otec
nové	nový	k2eAgFnSc2d1	nová
vlny	vlna	k1gFnSc2	vlna
pacifismu	pacifismus	k1gInSc2	pacifismus
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Svým	svůj	k3xOyFgInSc7	svůj
obsáhlým	obsáhlý	k2eAgInSc7d1	obsáhlý
dílem	díl	k1gInSc7	díl
<g/>
,	,	kIx,	,
zaměřeným	zaměřený	k2eAgNnSc7d1	zaměřené
na	na	k7c6	na
logiku	logika	k1gFnSc4	logika
<g/>
,	,	kIx,	,
matematiku	matematika	k1gFnSc4	matematika
<g/>
,	,	kIx,	,
filozofii	filozofie	k1gFnSc4	filozofie
<g/>
,	,	kIx,	,
ekonomii	ekonomie	k1gFnSc4	ekonomie
<g/>
,	,	kIx,	,
sociologii	sociologie	k1gFnSc4	sociologie
<g/>
,	,	kIx,	,
psychologii	psychologie	k1gFnSc4	psychologie
<g/>
,	,	kIx,	,
náboženství	náboženství	k1gNnSc4	náboženství
<g/>
,	,	kIx,	,
etiku	etika	k1gFnSc4	etika
a	a	k8xC	a
politiku	politika	k1gFnSc4	politika
patří	patřit	k5eAaImIp3nP	patřit
Russell	Russell	k1gInSc4	Russell
k	k	k7c3	k
nejvýznamnějším	významný	k2eAgMnPc3d3	nejvýznamnější
myslitelům	myslitel	k1gMnPc3	myslitel
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Russelovo	Russelův	k2eAgNnSc1d1	Russelův
řešení	řešení	k1gNnSc1	řešení
logických	logický	k2eAgInPc2d1	logický
paradoxů	paradox	k1gInPc2	paradox
pomocí	pomocí	k7c2	pomocí
jednoduché	jednoduchý	k2eAgFnSc2d1	jednoduchá
a	a	k8xC	a
rozvětvené	rozvětvený	k2eAgFnSc2d1	rozvětvená
teorie	teorie	k1gFnSc2	teorie
logických	logický	k2eAgInPc2d1	logický
typů	typ	k1gInPc2	typ
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInPc4	jeho
teorie	teorie	k1gFnPc1	teorie
logických	logický	k2eAgFnPc2d1	logická
funkcí	funkce	k1gFnPc2	funkce
<g/>
,	,	kIx,	,
pojetí	pojetí	k1gNnSc1	pojetí
definic	definice	k1gFnPc2	definice
abstrakcí	abstrakce	k1gFnPc2	abstrakce
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
definicí	definice	k1gFnSc7	definice
přirozených	přirozený	k2eAgNnPc2d1	přirozené
čísel	číslo	k1gNnPc2	číslo
<g/>
,	,	kIx,	,
logická	logický	k2eAgFnSc1d1	logická
analýza	analýza	k1gFnSc1	analýza
relací	relace	k1gFnPc2	relace
a	a	k8xC	a
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgFnPc2d1	další
koncepcí	koncepce	k1gFnPc2	koncepce
tvoří	tvořit	k5eAaImIp3nP	tvořit
pevný	pevný	k2eAgInSc4d1	pevný
základ	základ	k1gInSc4	základ
soudobé	soudobý	k2eAgFnSc2d1	soudobá
formální	formální	k2eAgFnSc2d1	formální
logiky	logika	k1gFnSc2	logika
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
teorie	teorie	k1gFnSc1	teorie
popisů	popis	k1gInPc2	popis
<g/>
,	,	kIx,	,
spjatá	spjatý	k2eAgFnSc1d1	spjatá
s	s	k7c7	s
výkladem	výklad	k1gInSc7	výklad
určitého	určitý	k2eAgInSc2d1	určitý
i	i	k8xC	i
neurčitého	určitý	k2eNgInSc2d1	neurčitý
členu	člen	k1gInSc2	člen
<g/>
,	,	kIx,	,
analýza	analýza	k1gFnSc1	analýza
vlastních	vlastní	k2eAgNnPc2d1	vlastní
jmen	jméno	k1gNnPc2	jméno
<g/>
,	,	kIx,	,
smyslu	smysl	k1gInSc2	smysl
a	a	k8xC	a
významu	význam	k1gInSc2	význam
<g/>
,	,	kIx,	,
rozlišení	rozlišení	k1gNnSc4	rozlišení
tzv.	tzv.	kA	tzv.
úplných	úplný	k2eAgInPc2d1	úplný
a	a	k8xC	a
neúplných	úplný	k2eNgInPc2d1	neúplný
symbolů	symbol	k1gInPc2	symbol
ovlivnily	ovlivnit	k5eAaPmAgInP	ovlivnit
výzkumy	výzkum	k1gInPc1	výzkum
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
sémantiky	sémantika	k1gFnSc2	sémantika
přirozených	přirozený	k2eAgInPc2d1	přirozený
i	i	k8xC	i
umělých	umělý	k2eAgInPc2d1	umělý
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
exaktních	exaktní	k2eAgFnPc2d1	exaktní
věd	věda	k1gFnPc2	věda
se	se	k3xPyFc4	se
Russell	Russell	k1gMnSc1	Russell
věnoval	věnovat	k5eAaImAgMnS	věnovat
také	také	k9	také
sociálním	sociální	k2eAgFnPc3d1	sociální
a	a	k8xC	a
etickým	etický	k2eAgFnPc3d1	etická
otázkám	otázka	k1gFnPc3	otázka
<g/>
,	,	kIx,	,
dějinám	dějiny	k1gFnPc3	dějiny
filozofie	filozofie	k1gFnSc2	filozofie
a	a	k8xC	a
mravním	mravní	k2eAgInPc3d1	mravní
aspektům	aspekt	k1gInPc3	aspekt
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
udělena	udělen	k2eAgFnSc1d1	udělena
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
jako	jako	k8xC	jako
uznání	uznání	k1gNnSc4	uznání
za	za	k7c4	za
jeho	jeho	k3xOp3gNnSc4	jeho
mnohostranné	mnohostranný	k2eAgNnSc4d1	mnohostranné
a	a	k8xC	a
významné	významný	k2eAgNnSc4d1	významné
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
jako	jako	k8xC	jako
zastánce	zastánce	k1gMnPc4	zastánce
humanity	humanita	k1gFnSc2	humanita
a	a	k8xC	a
svobody	svoboda	k1gFnSc2	svoboda
myšlení	myšlení	k1gNnSc2	myšlení
(	(	kIx(	(
<g/>
citace	citace	k1gFnPc1	citace
z	z	k7c2	z
odůvodnění	odůvodnění	k1gNnSc2	odůvodnění
Švédské	švédský	k2eAgFnSc2d1	švédská
akademie	akademie	k1gFnSc2	akademie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
po	po	k7c6	po
Theodoru	Theodor	k1gMnSc6	Theodor
Mommsenovi	Mommsen	k1gMnSc6	Mommsen
<g/>
,	,	kIx,	,
Rudolfu	Rudolf	k1gMnSc6	Rudolf
Euckenovi	Eucken	k1gMnSc6	Eucken
a	a	k8xC	a
Henri	Henr	k1gMnSc6	Henr
Bergsonovi	Bergson	k1gMnSc6	Bergson
čtvrtým	čtvrtý	k4xOgMnSc7	čtvrtý
nositelem	nositel	k1gMnSc7	nositel
této	tento	k3xDgFnSc2	tento
ceny	cena	k1gFnSc2	cena
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
nebyl	být	k5eNaImAgMnS	být
spisovatelem	spisovatel	k1gMnSc7	spisovatel
krásné	krásný	k2eAgFnSc2d1	krásná
literatury	literatura	k1gFnSc2	literatura
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jiných	jiný	k2eAgInPc2d1	jiný
spisů	spis	k1gInPc2	spis
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
svou	svůj	k3xOyFgFnSc7	svůj
formou	forma	k1gFnSc7	forma
a	a	k8xC	a
pojetím	pojetí	k1gNnSc7	pojetí
mají	mít	k5eAaImIp3nP	mít
literární	literární	k2eAgFnSc4d1	literární
hodnotu	hodnota	k1gFnSc4	hodnota
(	(	kIx(	(
<g/>
jak	jak	k8xC	jak
to	ten	k3xDgNnSc1	ten
stojí	stát	k5eAaImIp3nS	stát
v	v	k7c6	v
příslušných	příslušný	k2eAgFnPc6d1	příslušná
stanovách	stanova	k1gFnPc6	stanova
k	k	k7c3	k
Nobelově	Nobelův	k2eAgFnSc3d1	Nobelova
ceně	cena	k1gFnSc3	cena
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bertrand	Bertrand	k1gInSc1	Bertrand
Arthur	Arthura	k1gFnPc2	Arthura
Wiliam	Wiliam	k1gInSc1	Wiliam
Russell	Russell	k1gMnSc1	Russell
zemřel	zemřít	k5eAaPmAgMnS	zemřít
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Russellův	Russellův	k2eAgInSc1d1	Russellův
logický	logický	k2eAgInSc1d1	logický
atomismus	atomismus	k1gInSc1	atomismus
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
Russella	Russell	k1gMnSc2	Russell
skutečno	skutečno	k1gNnSc4	skutečno
jsou	být	k5eAaImIp3nP	být
jednotlivá	jednotlivý	k2eAgNnPc4d1	jednotlivé
smyslová	smyslový	k2eAgNnPc4d1	smyslové
data	datum	k1gNnPc4	datum
(	(	kIx(	(
<g/>
sense-data	senseata	k1gFnSc1	sense-data
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
spolu	spolu	k6eAd1	spolu
navzájem	navzájem	k6eAd1	navzájem
souvisejí	souviset	k5eAaImIp3nP	souviset
logicky	logicky	k6eAd1	logicky
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
věcmi	věc	k1gFnPc7	věc
existují	existovat	k5eAaImIp3nP	existovat
vnitřní	vnitřní	k2eAgInPc4d1	vnitřní
vztahy	vztah	k1gInPc4	vztah
-	-	kIx~	-
pokud	pokud	k8xS	pokud
chceme	chtít	k5eAaImIp1nP	chtít
popsat	popsat	k5eAaPmF	popsat
svět	svět	k1gInSc4	svět
<g/>
,	,	kIx,	,
musíme	muset	k5eAaImIp1nP	muset
nejen	nejen	k6eAd1	nejen
vyjmenovat	vyjmenovat	k5eAaPmF	vyjmenovat
věci	věc	k1gFnPc4	věc
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
popsat	popsat	k5eAaPmF	popsat
právě	právě	k9	právě
tyto	tento	k3xDgInPc4	tento
vztahy	vztah	k1gInPc4	vztah
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc4	jejich
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
je	on	k3xPp3gMnPc4	on
dáme	dát	k5eAaPmIp1nP	dát
dohromady	dohromady	k6eAd1	dohromady
<g/>
,	,	kIx,	,
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
atomický	atomický	k2eAgInSc1d1	atomický
fakt	fakt	k1gInSc1	fakt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Neexistuje	existovat	k5eNaImIp3nS	existovat
ani	ani	k9	ani
hmota	hmota	k1gFnSc1	hmota
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
duch	duch	k1gMnSc1	duch
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
ani	ani	k9	ani
já	já	k3xPp1nSc1	já
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
pouze	pouze	k6eAd1	pouze
smyslová	smyslový	k2eAgNnPc1d1	smyslové
data	datum	k1gNnPc1	datum
(	(	kIx(	(
<g/>
vliv	vliv	k1gInSc1	vliv
pozitivismu	pozitivismus	k1gInSc2	pozitivismus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Přírodní	přírodní	k2eAgFnSc1d1	přírodní
věda	věda	k1gFnSc1	věda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
jediná	jediný	k2eAgFnSc1d1	jediná
je	být	k5eAaImIp3nS	být
pravým	pravý	k2eAgInSc7d1	pravý
zdrojem	zdroj	k1gInSc7	zdroj
poznávání	poznávání	k1gNnSc4	poznávání
a	a	k8xC	a
našeho	náš	k3xOp1gNnSc2	náš
vědění	vědění	k1gNnSc2	vědění
o	o	k7c6	o
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
poznává	poznávat	k5eAaImIp3nS	poznávat
smyslová	smyslový	k2eAgNnPc4d1	smyslové
data	datum	k1gNnPc4	datum
a	a	k8xC	a
jinak	jinak	k6eAd1	jinak
nic	nic	k3yNnSc1	nic
<g/>
.	.	kIx.	.
</s>
<s>
Nedává	dávat	k5eNaImIp3nS	dávat
prostor	prostor	k1gInSc4	prostor
žádné	žádný	k3yNgFnSc6	žádný
víře	víra	k1gFnSc6	víra
v	v	k7c4	v
nesmrtelnost	nesmrtelnost	k1gFnSc4	nesmrtelnost
či	či	k8xC	či
boha	bůh	k1gMnSc4	bůh
<g/>
(	(	kIx(	(
<g/>
y	y	k?	y
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Náboženství	náboženství	k1gNnSc1	náboženství
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
Russella	Russello	k1gNnSc2	Russello
postradatelné	postradatelný	k2eAgInPc4d1	postradatelný
<g/>
,	,	kIx,	,
ne	ne	k9	ne
<g/>
-li	i	k?	-li
dokonce	dokonce	k9	dokonce
zlem	zlo	k1gNnSc7	zlo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
typické	typický	k2eAgNnSc1d1	typické
pro	pro	k7c4	pro
nevyspělého	vyspělý	k2eNgMnSc4d1	nevyspělý
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
hierarchie	hierarchie	k1gFnSc1	hierarchie
hodnot	hodnota	k1gFnPc2	hodnota
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
nutně	nutně	k6eAd1	nutně
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
i	i	k9	i
přírodu	příroda	k1gFnSc4	příroda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
opravdová	opravdový	k2eAgFnSc1d1	opravdová
morálka	morálka	k1gFnSc1	morálka
se	se	k3xPyFc4	se
neopírá	opírat	k5eNaImIp3nS	opírat
o	o	k7c4	o
pověrečné	pověrečný	k2eAgFnPc4d1	pověrečná
představy	představa	k1gFnPc4	představa
<g/>
.	.	kIx.	.
</s>
<s>
Postačujícím	postačující	k2eAgInSc7d1	postačující
lidským	lidský	k2eAgInSc7d1	lidský
údělem	úděl	k1gInSc7	úděl
je	být	k5eAaImIp3nS	být
život	život	k1gInSc1	život
vedený	vedený	k2eAgInSc1d1	vedený
láskou	láska	k1gFnSc7	láska
a	a	k8xC	a
orientovaný	orientovaný	k2eAgInSc1d1	orientovaný
věděním	vědění	k1gNnSc7	vědění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Logický	logický	k2eAgInSc1d1	logický
atomismus	atomismus	k1gInSc1	atomismus
tedy	tedy	k9	tedy
zakládá	zakládat	k5eAaImIp3nS	zakládat
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
poznáváme	poznávat	k5eAaImIp1nP	poznávat
jen	jen	k9	jen
smyslová	smyslový	k2eAgNnPc4d1	smyslové
data	datum	k1gNnPc4	datum
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
naše	náš	k3xOp1gNnPc4	náš
vědomí	vědomí	k1gNnPc4	vědomí
reflektuje	reflektovat	k5eAaImIp3nS	reflektovat
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
jazykových	jazykový	k2eAgNnPc2d1	jazykové
vyjádření	vyjádření	k1gNnPc2	vyjádření
<g/>
.	.	kIx.	.
</s>
<s>
Vhodným	vhodný	k2eAgInSc7d1	vhodný
jazykem	jazyk	k1gInSc7	jazyk
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
symbolická	symbolický	k2eAgFnSc1d1	symbolická
logika	logika	k1gFnSc1	logika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
German	German	k1gMnSc1	German
Social	Social	k1gMnSc1	Social
Democracy	Democraca	k1gFnSc2	Democraca
(	(	kIx(	(
<g/>
1896	[number]	k4	1896
<g/>
,	,	kIx,	,
Německá	německý	k2eAgFnSc1d1	německá
sociální	sociální	k2eAgFnSc1d1	sociální
demokracie	demokracie	k1gFnSc1	demokracie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
An	An	k?	An
Essay	Essay	k1gInPc1	Essay
on	on	k3xPp3gMnSc1	on
the	the	k?	the
Foundations	Foundations	k1gInSc4	Foundations
of	of	k?	of
Geometry	geometr	k1gInPc4	geometr
(	(	kIx(	(
<g/>
1897	[number]	k4	1897
<g/>
,	,	kIx,	,
Esej	esej	k1gFnSc1	esej
o	o	k7c6	o
základech	základ	k1gInPc6	základ
geometrie	geometrie	k1gFnSc2	geometrie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
A	a	k9	a
Critical	Critical	k1gFnPc1	Critical
Exposition	Exposition	k1gInSc1	Exposition
of	of	k?	of
the	the	k?	the
Philosophy	Philosopha	k1gFnSc2	Philosopha
of	of	k?	of
Leibniz	Leibniz	k1gMnSc1	Leibniz
(	(	kIx(	(
<g/>
1900	[number]	k4	1900
<g/>
,	,	kIx,	,
Kritický	kritický	k2eAgInSc1d1	kritický
výklad	výklad	k1gInSc1	výklad
Leibnizovy	Leibnizův	k2eAgFnSc2d1	Leibnizova
filozofie	filozofie	k1gFnSc2	filozofie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
The	The	k?	The
Principles	Principles	k1gInSc1	Principles
of	of	k?	of
Mathematics	Mathematics	k1gInSc1	Mathematics
(	(	kIx(	(
<g/>
1903	[number]	k4	1903
<g/>
,	,	kIx,	,
Principy	princip	k1gInPc1	princip
matematiky	matematika	k1gFnSc2	matematika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dílo	dílo	k1gNnSc1	dílo
rozvíjející	rozvíjející	k2eAgInPc1d1	rozvíjející
názory	názor	k1gInPc1	názor
německého	německý	k2eAgMnSc2d1	německý
matematika	matematik	k1gMnSc2	matematik
Gottloba	Gottloba	k1gFnSc1	Gottloba
Fregea	Fregea	k1gFnSc1	Fregea
<g/>
,	,	kIx,	,
že	že	k8xS	že
matematika	matematika	k1gFnSc1	matematika
je	být	k5eAaImIp3nS	být
pokračováním	pokračování	k1gNnSc7	pokračování
logiky	logika	k1gFnSc2	logika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
On	on	k3xPp3gInSc1	on
Denoting	Denoting	k1gInSc1	Denoting
(	(	kIx(	(
<g/>
1905	[number]	k4	1905
<g/>
,	,	kIx,	,
O	o	k7c6	o
označení	označení	k1gNnSc6	označení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stať	stať	k1gFnSc1	stať
je	být	k5eAaImIp3nS	být
mezníkem	mezník	k1gInSc7	mezník
ve	v	k7c6	v
vývoji	vývoj	k1gInSc6	vývoj
Russellových	Russellův	k2eAgInPc2d1	Russellův
názorů	názor	k1gInPc2	názor
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
učení	učení	k1gNnSc4	učení
rakouského	rakouský	k2eAgMnSc2d1	rakouský
filosofa	filosof	k1gMnSc2	filosof
Alexiuse	Alexiuse	k1gFnSc2	Alexiuse
Meinonga	Meinong	k1gMnSc2	Meinong
o	o	k7c4	o
existenci	existence	k1gFnSc4	existence
logicky	logicky	k6eAd1	logicky
rozporných	rozporný	k2eAgInPc2d1	rozporný
objektů	objekt	k1gInPc2	objekt
a	a	k8xC	a
počíná	počínat	k5eAaImIp3nS	počínat
navazovat	navazovat	k5eAaImF	navazovat
na	na	k7c4	na
Fregeovu	Fregeův	k2eAgFnSc4d1	Fregeův
teorii	teorie	k1gFnSc4	teorie
popisů	popis	k1gInPc2	popis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Philosophical	Philosophicat	k5eAaPmAgInS	Philosophicat
Essays	Essays	k1gInSc1	Essays
(	(	kIx(	(
<g/>
1910	[number]	k4	1910
<g/>
,	,	kIx,	,
Filozofické	filozofický	k2eAgFnSc2d1	filozofická
eseje	esej	k1gFnSc2	esej
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Principia	principium	k1gNnPc1	principium
Mathematica	Mathematic	k2eAgNnPc1d1	Mathematic
(	(	kIx(	(
<g/>
1910	[number]	k4	1910
<g/>
–	–	k?	–
<g/>
1913	[number]	k4	1913
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
Alfredem	Alfred	k1gMnSc7	Alfred
North	North	k1gInSc4	North
Whiteheadem	Whitehead	k1gInSc7	Whitehead
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
třísvazkovou	třísvazkový	k2eAgFnSc4d1	třísvazková
knihu	kniha	k1gFnSc4	kniha
(	(	kIx(	(
<g/>
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
díly	díl	k1gInPc1	díl
1910	[number]	k4	1910
<g/>
,	,	kIx,	,
1912	[number]	k4	1912
a	a	k8xC	a
1913	[number]	k4	1913
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yRgFnPc4	který
se	se	k3xPyFc4	se
Russell	Russell	k1gInSc1	Russell
a	a	k8xC	a
Whitehead	Whitehead	k1gInSc4	Whitehead
pokusili	pokusit	k5eAaPmAgMnP	pokusit
upevnit	upevnit	k5eAaPmF	upevnit
a	a	k8xC	a
sjednotit	sjednotit	k5eAaPmF	sjednotit
logické	logický	k2eAgInPc4d1	logický
základy	základ	k1gInPc4	základ
matematiky	matematika	k1gFnSc2	matematika
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
dovršuje	dovršovat	k5eAaImIp3nS	dovršovat
základy	základ	k1gInPc4	základ
matematické	matematický	k2eAgFnSc2d1	matematická
logiky	logika	k1gFnSc2	logika
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
otevírá	otevírat	k5eAaImIp3nS	otevírat
cestu	cesta	k1gFnSc4	cesta
k	k	k7c3	k
rozpracování	rozpracování	k1gNnSc3	rozpracování
logické	logický	k2eAgFnSc2d1	logická
sémantiky	sémantika	k1gFnSc2	sémantika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
The	The	k?	The
Problems	Problems	k1gInSc1	Problems
of	of	k?	of
Philosophy	Philosopha	k1gFnSc2	Philosopha
(	(	kIx(	(
<g/>
1912	[number]	k4	1912
<g/>
,	,	kIx,	,
Problémy	problém	k1gInPc1	problém
filozofie	filozofie	k1gFnSc2	filozofie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Our	Our	k?	Our
Knowledge	Knowledge	k1gInSc1	Knowledge
of	of	k?	of
the	the	k?	the
External	External	k1gMnSc1	External
World	World	k1gMnSc1	World
as	as	k1gNnPc2	as
a	a	k8xC	a
Field	Fielda	k1gFnPc2	Fielda
for	forum	k1gNnPc2	forum
Scientific	Scientific	k1gMnSc1	Scientific
Method	Methoda	k1gFnPc2	Methoda
in	in	k?	in
Philosophy	Philosopha	k1gMnSc2	Philosopha
(	(	kIx(	(
<g/>
1914	[number]	k4	1914
<g/>
,	,	kIx,	,
Naše	náš	k3xOp1gFnSc1	náš
znalost	znalost	k1gFnSc1	znalost
vnějšího	vnější	k2eAgInSc2d1	vnější
světa	svět	k1gInSc2	svět
jako	jako	k8xC	jako
prostor	prostor	k1gInSc4	prostor
pro	pro	k7c4	pro
vědeckou	vědecký	k2eAgFnSc4d1	vědecká
metodu	metoda	k1gFnSc4	metoda
ve	v	k7c4	v
filozofii	filozofie	k1gFnSc4	filozofie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Principles	Principles	k1gInSc1	Principles
of	of	k?	of
Social	Social	k1gInSc1	Social
Reconstruction	Reconstruction	k1gInSc1	Reconstruction
(	(	kIx(	(
<g/>
1916	[number]	k4	1916
<g/>
,	,	kIx,	,
Principy	princip	k1gInPc1	princip
sociální	sociální	k2eAgFnSc2d1	sociální
přestavby	přestavba	k1gFnSc2	přestavba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Political	Politicat	k5eAaPmAgInS	Politicat
Ideals	Ideals	k1gInSc1	Ideals
(	(	kIx(	(
<g/>
1917	[number]	k4	1917
<g/>
,	,	kIx,	,
Politické	politický	k2eAgFnPc1d1	politická
ideje	idea	k1gFnPc1	idea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Mysticism	Mysticism	k1gMnSc1	Mysticism
and	and	k?	and
Logic	Logic	k1gMnSc1	Logic
and	and	k?	and
Other	Other	k1gInSc1	Other
Essays	Essays	k1gInSc1	Essays
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
Mystika	mystika	k1gFnSc1	mystika
a	a	k8xC	a
logika	logika	k1gFnSc1	logika
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
eseje	esej	k1gFnPc1	esej
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Roads	Roads	k6eAd1	Roads
to	ten	k3xDgNnSc4	ten
Freedom	Freedom	k1gInSc1	Freedom
<g/>
:	:	kIx,	:
Socialism	Socialism	k1gMnSc1	Socialism
<g/>
,	,	kIx,	,
Anarchism	Anarchism	k1gMnSc1	Anarchism
<g/>
,	,	kIx,	,
and	and	k?	and
Syndicalism	Syndicalism	k1gInSc1	Syndicalism
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
Cesty	cesta	k1gFnPc1	cesta
ke	k	k7c3	k
svobodě	svoboda	k1gFnSc3	svoboda
<g/>
:	:	kIx,	:
Socialismus	socialismus	k1gInSc1	socialismus
<g/>
,	,	kIx,	,
anarchismus	anarchismus	k1gInSc1	anarchismus
a	a	k8xC	a
syndikalismus	syndikalismus	k1gInSc1	syndikalismus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
The	The	k?	The
Philosophy	Philosopha	k1gFnPc4	Philosopha
of	of	k?	of
Logical	Logical	k1gMnSc1	Logical
Atomism	Atomism	k1gMnSc1	Atomism
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
Filozofie	filozofie	k1gFnSc1	filozofie
logického	logický	k2eAgInSc2d1	logický
atomismu	atomismus	k1gInSc2	atomismus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Introduction	Introduction	k1gInSc1	Introduction
to	ten	k3xDgNnSc1	ten
Mathematical	Mathematical	k1gFnSc3	Mathematical
Philosophy	Philosopha	k1gFnSc2	Philosopha
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
Úvod	úvod	k1gInSc1	úvod
do	do	k7c2	do
filozofie	filozofie	k1gFnSc2	filozofie
matematiky	matematika	k1gFnSc2	matematika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
The	The	k?	The
Practice	Practice	k1gFnSc1	Practice
and	and	k?	and
Theory	Theora	k1gFnSc2	Theora
of	of	k?	of
Bolshevism	Bolshevism	k1gMnSc1	Bolshevism
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
Praxe	praxe	k1gFnSc1	praxe
a	a	k8xC	a
teorie	teorie	k1gFnSc1	teorie
bolševizmu	bolševizmus	k1gInSc2	bolševizmus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
The	The	k?	The
Analysis	Analysis	k1gInSc1	Analysis
of	of	k?	of
Mind	Mind	k1gInSc1	Mind
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
<g/>
,	,	kIx,	,
Analýza	analýza	k1gFnSc1	analýza
myšlení	myšlení	k1gNnSc2	myšlení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
The	The	k?	The
Problem	Probl	k1gInSc7	Probl
of	of	k?	of
China	China	k1gFnSc1	China
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
,	,	kIx,	,
Čínský	čínský	k2eAgInSc1d1	čínský
problém	problém	k1gInSc1	problém
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
The	The	k?	The
ABC	ABC	kA	ABC
of	of	k?	of
Atoms	Atoms	k1gInSc1	Atoms
(	(	kIx(	(
1923	[number]	k4	1923
<g/>
,	,	kIx,	,
Abeceda	abeceda	k1gFnSc1	abeceda
atomů	atom	k1gInPc2	atom
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
The	The	k?	The
ABC	ABC	kA	ABC
of	of	k?	of
Relativity	relativita	k1gFnSc2	relativita
(	(	kIx(	(
1925	[number]	k4	1925
<g/>
,	,	kIx,	,
Abeceda	abeceda	k1gFnSc1	abeceda
relativity	relativita	k1gFnSc2	relativita
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
What	What	k5eAaPmF	What
I	i	k9	i
Believe	Belieev	k1gFnSc2	Belieev
(	(	kIx(	(
<g/>
1925	[number]	k4	1925
<g/>
,	,	kIx,	,
V	v	k7c4	v
co	co	k3yInSc4	co
věřím	věřit	k5eAaImIp1nS	věřit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
On	on	k3xPp3gInSc1	on
Education	Education	k1gInSc1	Education
<g/>
,	,	kIx,	,
Especially	Especiall	k1gInPc1	Especiall
in	in	k?	in
Early	earl	k1gMnPc4	earl
Childhood	Childhooda	k1gFnPc2	Childhooda
(	(	kIx(	(
<g/>
1926	[number]	k4	1926
<g/>
,	,	kIx,	,
O	o	k7c6	o
výchově	výchova	k1gFnSc6	výchova
zejména	zejména	k9	zejména
v	v	k7c6	v
raném	raný	k2eAgNnSc6d1	rané
dětství	dětství	k1gNnSc6	dětství
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
The	The	k?	The
Analys	analysa	k1gFnPc2	analysa
of	of	k?	of
Matter	Matter	k1gMnSc1	Matter
(	(	kIx(	(
<g/>
1927	[number]	k4	1927
<g/>
,	,	kIx,	,
Analýza	analýza	k1gFnSc1	analýza
hmoty	hmota	k1gFnSc2	hmota
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
An	An	k?	An
Outline	Outlin	k1gInSc5	Outlin
of	of	k?	of
Philosophy	Philosoph	k1gMnPc7	Philosoph
(	(	kIx(	(
<g/>
1927	[number]	k4	1927
<g/>
,	,	kIx,	,
Přehled	přehled	k1gInSc1	přehled
filozofie	filozofie	k1gFnSc1	filozofie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Why	Why	k?	Why
I	i	k9	i
Am	Am	k1gFnSc4	Am
Not	nota	k1gFnPc2	nota
a	a	k8xC	a
Christian	Christian	k1gMnSc1	Christian
(	(	kIx(	(
<g/>
1927	[number]	k4	1927
<g/>
,	,	kIx,	,
Proč	proč	k6eAd1	proč
nejsem	být	k5eNaImIp1nS	být
křesťanem	křesťan	k1gMnSc7	křesťan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Sceptical	Scepticat	k5eAaPmAgInS	Scepticat
Essays	Essays	k1gInSc1	Essays
(	(	kIx(	(
<g/>
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
Skeptické	skeptický	k2eAgFnSc2d1	skeptická
eseje	esej	k1gFnSc2	esej
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Marriage	Marriage	k1gFnSc1	Marriage
and	and	k?	and
Morals	Morals	k1gInSc1	Morals
(	(	kIx(	(
<g/>
1929	[number]	k4	1929
<g/>
,	,	kIx,	,
Manželství	manželství	k1gNnSc1	manželství
a	a	k8xC	a
mravnost	mravnost	k1gFnSc1	mravnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
The	The	k?	The
Conquest	Conquest	k1gInSc1	Conquest
of	of	k?	of
Happiness	Happiness	k1gInSc1	Happiness
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
Boj	boj	k1gInSc1	boj
o	o	k7c4	o
štěstí	štěstí	k1gNnSc4	štěstí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
knize	kniha	k1gFnSc6	kniha
se	se	k3xPyFc4	se
Russell	Russell	k1gInSc1	Russell
zabývá	zabývat	k5eAaImIp3nS	zabývat
podmínkami	podmínka	k1gFnPc7	podmínka
za	za	k7c2	za
nichž	jenž	k3xRgFnPc2	jenž
může	moct	k5eAaImIp3nS	moct
člověk	člověk	k1gMnSc1	člověk
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
štěstí	štěstí	k1gNnSc4	štěstí
<g/>
.	.	kIx.	.
</s>
<s>
Těmi	ten	k3xDgInPc7	ten
podle	podle	k7c2	podle
něj	on	k3xPp3gInSc2	on
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Nadšení	nadšení	k1gNnSc4	nadšení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
znamená	znamenat	k5eAaImIp3nS	znamenat
žízeň	žízeň	k1gFnSc1	žízeň
po	po	k7c6	po
životě	život	k1gInSc6	život
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc7	jeho
velkým	velký	k2eAgInSc7d1	velký
zdrojem	zdroj	k1gInSc7	zdroj
je	být	k5eAaImIp3nS	být
láska	láska	k1gFnSc1	láska
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
vedoucích	vedoucí	k2eAgFnPc2d1	vedoucí
příčin	příčina	k1gFnPc2	příčina
nedostatku	nedostatek	k1gInSc2	nedostatek
nadšení	nadšení	k1gNnSc2	nadšení
je	být	k5eAaImIp3nS	být
pocit	pocit	k1gInSc4	pocit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jedinec	jedinec	k1gMnSc1	jedinec
není	být	k5eNaImIp3nS	být
milován	milován	k2eAgMnSc1d1	milován
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
z	z	k7c2	z
opačného	opačný	k2eAgInSc2d1	opačný
úhlu	úhel	k1gInSc2	úhel
pohledu	pohled	k1gInSc2	pohled
pocit	pocit	k1gInSc4	pocit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
jedinec	jedinec	k1gMnSc1	jedinec
milován	milován	k2eAgMnSc1d1	milován
podporuje	podporovat	k5eAaImIp3nS	podporovat
nadšení	nadšení	k1gNnSc1	nadšení
víc	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
než	než	k8xS	než
cokoliv	cokoliv	k3yInSc4	cokoliv
jiného	jiný	k2eAgNnSc2d1	jiné
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
zdrojem	zdroj	k1gInSc7	zdroj
štěstí	štěstí	k1gNnSc2	štěstí
je	být	k5eAaImIp3nS	být
rodina	rodina	k1gFnSc1	rodina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
hraje	hrát	k5eAaImIp3nS	hrát
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
v	v	k7c6	v
jedincově	jedincův	k2eAgInSc6d1	jedincův
pocitu	pocit	k1gInSc6	pocit
bezpečí	bezpečí	k1gNnSc2	bezpečí
<g/>
.	.	kIx.	.
</s>
<s>
Třetím	třetí	k4xOgInSc7	třetí
zdrojem	zdroj	k1gInSc7	zdroj
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
neosobní	osobní	k2eNgInPc4d1	neosobní
zájmy	zájem	k1gInPc4	zájem
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
vyplnit	vyplnit	k5eAaPmF	vyplnit
volný	volný	k2eAgInSc4d1	volný
čas	čas	k1gInSc4	čas
a	a	k8xC	a
zahání	zahánět	k5eAaImIp3nP	zahánět
nudu	nuda	k1gFnSc4	nuda
<g/>
,	,	kIx,	,
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
navíc	navíc	k6eAd1	navíc
oddych	oddych	k1gInSc4	oddych
od	od	k7c2	od
napětí	napětí	k1gNnSc2	napětí
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
rodina	rodina	k1gFnSc1	rodina
<g/>
,	,	kIx,	,
práce	práce	k1gFnPc1	práce
či	či	k8xC	či
finance	finance	k1gFnPc1	finance
<g/>
.	.	kIx.	.
</s>
<s>
Russellův	Russellův	k2eAgInSc1d1	Russellův
pohled	pohled	k1gInSc1	pohled
na	na	k7c4	na
štěstí	štěstí	k1gNnSc4	štěstí
tedy	tedy	k9	tedy
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInPc4	jeho
zdroje	zdroj	k1gInPc4	zdroj
záleží	záležet	k5eAaImIp3nS	záležet
jednak	jednak	k8xC	jednak
na	na	k7c6	na
externích	externí	k2eAgFnPc6d1	externí
okolnostech	okolnost	k1gFnPc6	okolnost
a	a	k8xC	a
jednak	jednak	k8xC	jednak
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
jedinci	jedinec	k1gMnSc6	jedinec
samotném	samotný	k2eAgMnSc6d1	samotný
<g/>
.	.	kIx.	.
</s>
<s>
Štastný	Štastný	k2eAgMnSc1d1	Štastný
člověk	člověk	k1gMnSc1	člověk
je	být	k5eAaImIp3nS	být
ten	ten	k3xDgMnSc1	ten
který	který	k3yQgMnSc1	který
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
náklonnost	náklonnost	k1gFnSc4	náklonnost
k	k	k7c3	k
druhým	druhý	k4xOgMnPc3	druhý
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
široké	široký	k2eAgNnSc1d1	široké
pole	pole	k1gNnSc1	pole
zájmů	zájem	k1gInPc2	zájem
<g/>
,	,	kIx,	,
provádí	provádět	k5eAaImIp3nS	provádět
věci	věc	k1gFnPc4	věc
s	s	k7c7	s
nadšením	nadšení	k1gNnSc7	nadšení
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
netrpí	trpět	k5eNaImIp3nP	trpět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
The	The	k?	The
Scientific	Scientific	k1gMnSc1	Scientific
Outlook	Outlook	k1gInSc1	Outlook
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
<g/>
,	,	kIx,	,
Vědecké	vědecký	k2eAgFnPc4d1	vědecká
vyhlídky	vyhlídka	k1gFnPc4	vyhlídka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Education	Education	k1gInSc1	Education
and	and	k?	and
the	the	k?	the
Social	Social	k1gMnSc1	Social
Order	Order	k1gMnSc1	Order
(	(	kIx(	(
<g/>
1932	[number]	k4	1932
<g/>
,	,	kIx,	,
Vzdělání	vzdělání	k1gNnSc1	vzdělání
a	a	k8xC	a
společenský	společenský	k2eAgInSc1d1	společenský
systém	systém	k1gInSc1	systém
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Freedom	Freedom	k1gInSc1	Freedom
and	and	k?	and
Organization	Organization	k1gInSc1	Organization
<g/>
:	:	kIx,	:
1814	[number]	k4	1814
<g/>
–	–	k?	–
<g/>
1914	[number]	k4	1914
(	(	kIx(	(
<g/>
1934	[number]	k4	1934
<g/>
,	,	kIx,	,
Svoboda	svoboda	k1gFnSc1	svoboda
a	a	k8xC	a
organizace	organizace	k1gFnSc1	organizace
<g/>
:	:	kIx,	:
1814	[number]	k4	1814
<g/>
–	–	k?	–
<g/>
1914	[number]	k4	1914
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Religion	religion	k1gInSc1	religion
and	and	k?	and
Science	Science	k1gFnSc1	Science
(	(	kIx(	(
<g/>
1935	[number]	k4	1935
<g/>
,	,	kIx,	,
Náboženství	náboženství	k1gNnSc1	náboženství
a	a	k8xC	a
věda	věda	k1gFnSc1	věda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Power	Power	k1gMnSc1	Power
<g/>
:	:	kIx,	:
A	A	kA	A
New	New	k1gFnSc1	New
Social	Social	k1gMnSc1	Social
Analysis	Analysis	k1gInSc1	Analysis
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
Vláda	vláda	k1gFnSc1	vláda
<g/>
:	:	kIx,	:
nová	nový	k2eAgFnSc1d1	nová
společenská	společenský	k2eAgFnSc1d1	společenská
analýza	analýza	k1gFnSc1	analýza
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
An	An	k?	An
Inquiry	Inquira	k1gFnPc4	Inquira
into	into	k6eAd1	into
Meaning	Meaning	k1gInSc1	Meaning
and	and	k?	and
Truth	Truth	k1gInSc1	Truth
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
,	,	kIx,	,
Zkoumání	zkoumání	k1gNnSc1	zkoumání
o	o	k7c6	o
smyslu	smysl	k1gInSc6	smysl
a	a	k8xC	a
pravdivosti	pravdivost	k1gFnSc6	pravdivost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kniha	kniha	k1gFnSc1	kniha
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
z	z	k7c2	z
přednášek	přednáška	k1gFnPc2	přednáška
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc4	jenž
Russell	Russell	k1gInSc1	Russell
konal	konat	k5eAaImAgInS	konat
koncem	koncem	k7c2	koncem
třicátých	třicátý	k4xOgNnPc2	třicátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
na	na	k7c6	na
amerických	americký	k2eAgFnPc6d1	americká
univerzitách	univerzita	k1gFnPc6	univerzita
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
prozkoumat	prozkoumat	k5eAaPmF	prozkoumat
určité	určitý	k2eAgInPc4d1	určitý
problémy	problém	k1gInPc4	problém
týkající	týkající	k2eAgInPc4d1	týkající
se	se	k3xPyFc4	se
empirického	empirický	k2eAgNnSc2d1	empirické
poznání	poznání	k1gNnSc2	poznání
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
svých	svůj	k3xOyFgFnPc6	svůj
úvahách	úvaha	k1gFnPc6	úvaha
o	o	k7c6	o
problematice	problematika	k1gFnSc6	problematika
smyslu	smysl	k1gInSc2	smysl
a	a	k8xC	a
pravdivosti	pravdivost	k1gFnSc2	pravdivost
vychází	vycházet	k5eAaImIp3nS	vycházet
Rusell	Rusell	k1gInSc1	Rusell
z	z	k7c2	z
čistého	čistý	k2eAgInSc2d1	čistý
empirismu	empirismus	k1gInSc2	empirismus
a	a	k8xC	a
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
jej	on	k3xPp3gMnSc4	on
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
koncepci	koncepce	k1gFnSc6	koncepce
logického	logický	k2eAgInSc2d1	logický
atomismu	atomismus	k1gInSc2	atomismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
A	a	k9	a
History	Histor	k1gInPc1	Histor
of	of	k?	of
Western	Western	kA	Western
Philosophy	Philosopha	k1gFnSc2	Philosopha
(	(	kIx(	(
<g/>
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
Dějiny	dějiny	k1gFnPc1	dějiny
západní	západní	k2eAgFnSc2d1	západní
filozofie	filozofie	k1gFnSc2	filozofie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Philosophy	Philosoph	k1gInPc1	Philosoph
and	and	k?	and
Politics	Politics	k1gInSc1	Politics
(	(	kIx(	(
<g/>
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
Filozofie	filozofie	k1gFnSc1	filozofie
a	a	k8xC	a
politika	politika	k1gFnSc1	politika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Human	Human	k1gInSc1	Human
Knowledge	Knowledge	k1gNnSc1	Knowledge
<g/>
:	:	kIx,	:
Its	Its	k1gMnSc5	Its
Scope	Scop	k1gMnSc5	Scop
and	and	k?	and
Limits	Limits	k1gInSc1	Limits
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
Lidské	lidský	k2eAgNnSc1d1	lidské
vědění	vědění	k1gNnSc1	vědění
<g/>
:	:	kIx,	:
jeho	on	k3xPp3gInSc4	on
rozsah	rozsah	k1gInSc4	rozsah
a	a	k8xC	a
meze	mez	k1gFnPc4	mez
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Authority	Authorita	k1gFnPc1	Authorita
and	and	k?	and
Individual	Individual	k1gMnSc1	Individual
(	(	kIx(	(
<g/>
1949	[number]	k4	1949
<g/>
,	,	kIx,	,
Moc	moc	k6eAd1	moc
a	a	k8xC	a
jednotlivec	jednotlivec	k1gMnSc1	jednotlivec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Unpopular	Unpopular	k1gInSc1	Unpopular
Essays	Essays	k1gInSc1	Essays
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
Nepopulární	populární	k2eNgFnSc2d1	nepopulární
eseje	esej	k1gFnSc2	esej
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
New	New	k?	New
Hopes	Hopes	k1gMnSc1	Hopes
for	forum	k1gNnPc2	forum
a	a	k8xC	a
Changing	Changing	k1gInSc1	Changing
World	Worlda	k1gFnPc2	Worlda
(	(	kIx(	(
<g/>
1951	[number]	k4	1951
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgFnPc1d1	Nové
naděje	naděje	k1gFnPc1	naděje
pro	pro	k7c4	pro
měnící	měnící	k2eAgInSc4d1	měnící
se	se	k3xPyFc4	se
svět	svět	k1gInSc4	svět
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
The	The	k?	The
Impact	Impact	k1gInSc1	Impact
of	of	k?	of
Science	Science	k1gFnSc1	Science
on	on	k3xPp3gMnSc1	on
Society	societa	k1gFnPc1	societa
(	(	kIx(	(
<g/>
1952	[number]	k4	1952
<g/>
,	,	kIx,	,
Vliv	vliv	k1gInSc1	vliv
vědy	věda	k1gFnSc2	věda
na	na	k7c6	na
společnost	společnost	k1gFnSc1	společnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Satan	satan	k1gInSc1	satan
in	in	k?	in
the	the	k?	the
Suburbs	Suburbs	k1gInSc1	Suburbs
(	(	kIx(	(
<g/>
1953	[number]	k4	1953
<g/>
,	,	kIx,	,
Satan	satan	k1gInSc1	satan
na	na	k7c6	na
předměstí	předměstí	k1gNnSc6	předměstí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
soubor	soubor	k1gInSc4	soubor
povídek	povídka	k1gFnPc2	povídka
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Portraits	Portraits	k6eAd1	Portraits
from	from	k1gMnSc1	from
Memory	Memora	k1gFnSc2	Memora
and	and	k?	and
Other	Other	k1gInSc1	Other
Essays	Essays	k1gInSc1	Essays
(	(	kIx(	(
<g/>
1956	[number]	k4	1956
<g/>
,	,	kIx,	,
Portrety	Portreta	k1gFnPc1	Portreta
z	z	k7c2	z
paměti	paměť	k1gFnSc2	paměť
a	a	k8xC	a
jiné	jiný	k2eAgFnSc2d1	jiná
eseje	esej	k1gFnSc2	esej
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
My	my	k3xPp1nPc1	my
Philosophical	Philosophical	k1gMnSc7	Philosophical
Development	Development	k1gMnSc1	Development
(	(	kIx(	(
<g/>
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
Můj	můj	k3xOp1gInSc1	můj
filozofický	filozofický	k2eAgInSc1d1	filozofický
vývoj	vývoj	k1gInSc1	vývoj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Wisdom	Wisdom	k1gInSc1	Wisdom
of	of	k?	of
the	the	k?	the
West	West	k1gInSc1	West
(	(	kIx(	(
<g/>
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
Moudrost	moudrost	k1gFnSc1	moudrost
západu	západ	k1gInSc2	západ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
On	on	k3xPp3gMnSc1	on
the	the	k?	the
Philosophy	Philosopha	k1gFnPc4	Philosopha
of	of	k?	of
Science	Science	k1gFnSc1	Science
(	(	kIx(	(
<g/>
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
O	o	k7c4	o
filozofii	filozofie	k1gFnSc4	filozofie
vědy	věda	k1gFnSc2	věda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
War	War	k?	War
Crimes	Crimes	k1gInSc1	Crimes
in	in	k?	in
Vietnam	Vietnam	k1gInSc1	Vietnam
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
Válečné	válečný	k2eAgInPc4d1	válečný
zločiny	zločin	k1gInPc4	zločin
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
The	The	k?	The
Autobiography	Autobiograph	k1gInPc1	Autobiograph
of	of	k?	of
Bertrand	Bertrand	k1gInSc1	Bertrand
Russell	Russell	k1gMnSc1	Russell
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
–	–	k?	–
<g/>
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
Autobiografie	autobiografie	k1gFnSc1	autobiografie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tři	tři	k4xCgInPc4	tři
díly	díl	k1gInPc4	díl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Česká	český	k2eAgNnPc1d1	české
vydání	vydání	k1gNnPc1	vydání
==	==	k?	==
</s>
</p>
<p>
<s>
Problémy	problém	k1gInPc1	problém
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
,	,	kIx,	,
Čin	čin	k1gInSc1	čin
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1927	[number]	k4	1927
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
O.P.S.	O.P.S.	k1gFnSc1	O.P.S.
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Proč	proč	k6eAd1	proč
nejsem	být	k5eNaImIp1nS	být
křesťanem	křesťan	k1gMnSc7	křesťan
<g/>
,	,	kIx,	,
Volná	volný	k2eAgFnSc5d1	volná
myšlenka	myšlenka	k1gFnSc1	myšlenka
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Theorie	theorie	k1gFnSc1	theorie
poznání	poznání	k1gNnSc2	poznání
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Schützner	Schützner	k1gMnSc1	Schützner
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Josef	Josef	k1gMnSc1	Josef
Schützner	Schützner	k1gMnSc1	Schützner
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Boj	boj	k1gInSc1	boj
o	o	k7c4	o
štěstí	štěstí	k1gNnSc4	štěstí
<g/>
,	,	kIx,	,
Orbis	orbis	k1gInSc1	orbis
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1931	[number]	k4	1931
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Ladislav	Ladislav	k1gMnSc1	Ladislav
Vymětal	Vymětal	k1gMnSc1	Vymětal
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Manželství	manželství	k1gNnSc1	manželství
a	a	k8xC	a
mravnost	mravnost	k1gFnSc1	mravnost
<g/>
,	,	kIx,	,
Aventinum	Aventinum	k1gNnSc1	Aventinum
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1931	[number]	k4	1931
<g/>
,	,	kIx,	,
přeložili	přeložit	k5eAaPmAgMnP	přeložit
Otakar	Otakar	k1gMnSc1	Otakar
Vočadlo	vočadlo	k1gNnSc4	vočadlo
a	a	k8xC	a
Ludmila	Ludmila	k1gFnSc1	Ludmila
Vočadlová	Vočadlový	k2eAgFnSc1d1	Vočadlová
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
1947	[number]	k4	1947
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
O	o	k7c6	o
výchově	výchova	k1gFnSc6	výchova
zejména	zejména	k9	zejména
v	v	k7c6	v
raném	raný	k2eAgNnSc6d1	rané
dětství	dětství	k1gNnSc6	dětství
<g/>
,	,	kIx,	,
Orbis	orbis	k1gInSc4	orbis
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1932	[number]	k4	1932
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Josef	Josef	k1gMnSc1	Josef
Hrůša	Hrůša	k1gMnSc1	Hrůša
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Prospělo	prospět	k5eAaPmAgNnS	prospět
náboženství	náboženství	k1gNnSc4	náboženství
civilisaci	civilisace	k1gFnSc3	civilisace
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
,	,	kIx,	,
Volná	volný	k2eAgFnSc1d1	volná
myšlenka	myšlenka	k1gFnSc1	myšlenka
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1935	[number]	k4	1935
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Karel	Karel	k1gMnSc1	Karel
Plaňanský	Plaňanský	k2eAgMnSc1d1	Plaňanský
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Svoboda	svoboda	k1gFnSc1	svoboda
a	a	k8xC	a
organisace	organisace	k1gFnSc1	organisace
<g/>
:	:	kIx,	:
1814	[number]	k4	1814
<g/>
–	–	k?	–
<g/>
1914	[number]	k4	1914
<g/>
,	,	kIx,	,
Dělnické	dělnický	k2eAgNnSc1d1	dělnické
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Kříž	Kříž	k1gMnSc1	Kříž
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Proč	proč	k6eAd1	proč
nejsem	být	k5eNaImIp1nS	být
křesťanem	křesťan	k1gMnSc7	křesťan
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
eseje	esej	k1gFnPc1	esej
<g/>
,	,	kIx,	,
Orbis	orbis	k1gInSc1	orbis
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1961	[number]	k4	1961
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
František	František	k1gMnSc1	František
Kejdana	Kejdan	k1gMnSc2	Kejdan
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Logika	logika	k1gFnSc1	logika
<g/>
,	,	kIx,	,
jazyk	jazyk	k1gInSc1	jazyk
a	a	k8xC	a
věda	věda	k1gFnSc1	věda
<g/>
,	,	kIx,	,
Svoboda	svoboda	k1gFnSc1	svoboda
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
výbor	výbor	k1gInSc4	výbor
uspořádali	uspořádat	k5eAaPmAgMnP	uspořádat
a	a	k8xC	a
přeložili	přeložit	k5eAaPmAgMnP	přeložit
Karel	Karel	k1gMnSc1	Karel
Berka	Berka	k1gMnSc1	Berka
a	a	k8xC	a
Ladislav	Ladislav	k1gMnSc1	Ladislav
Tondl	Tondl	k1gMnSc1	Tondl
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Zkoumání	zkoumání	k1gNnSc1	zkoumání
o	o	k7c6	o
smyslu	smysl	k1gInSc6	smysl
a	a	k8xC	a
pravdivosti	pravdivost	k1gFnSc6	pravdivost
<g/>
,	,	kIx,	,
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Jindřich	Jindřich	k1gMnSc1	Jindřich
Husák	Husák	k1gMnSc1	Husák
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Logika	logika	k1gFnSc1	logika
<g/>
,	,	kIx,	,
věda	věda	k1gFnSc1	věda
<g/>
,	,	kIx,	,
filozofie	filozofie	k1gFnSc1	filozofie
<g/>
,	,	kIx,	,
společnost	společnost	k1gFnSc1	společnost
<g/>
,	,	kIx,	,
Svoboda	svoboda	k1gFnSc1	svoboda
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
výbor	výbor	k1gInSc4	výbor
uspořádal	uspořádat	k5eAaPmAgMnS	uspořádat
a	a	k8xC	a
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Karel	Karel	k1gMnSc1	Karel
Berka	Berka	k1gMnSc1	Berka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mystika	mystika	k1gFnSc1	mystika
a	a	k8xC	a
logika	logika	k1gFnSc1	logika
a	a	k8xC	a
jiné	jiný	k2eAgInPc1d1	jiný
eseje	esej	k1gInPc1	esej
<g/>
,	,	kIx,	,
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Antonín	Antonín	k1gMnSc1	Antonín
Otáhal	Otáhal	k1gMnSc1	Otáhal
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Russellův	Russellův	k2eAgInSc1d1	Russellův
paradox	paradox	k1gInSc1	paradox
</s>
</p>
<p>
<s>
Filosofie	filosofie	k1gFnSc1	filosofie
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Bertrand	Bertranda	k1gFnPc2	Bertranda
Russell	Russellum	k1gNnPc2	Russellum
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Autor	autor	k1gMnSc1	autor
Bertrand	Bertranda	k1gFnPc2	Bertranda
Russell	Russell	k1gMnSc1	Russell
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Bertrand	Bertrand	k1gInSc1	Bertrand
Russell	Russella	k1gFnPc2	Russella
</s>
</p>
<p>
<s>
Plné	plný	k2eAgInPc1d1	plný
texty	text	k1gInPc1	text
děl	dělo	k1gNnPc2	dělo
autora	autor	k1gMnSc2	autor
Bertrand	Bertranda	k1gFnPc2	Bertranda
Russell	Russell	k1gMnSc1	Russell
na	na	k7c6	na
projektu	projekt	k1gInSc6	projekt
Gutenberg	Gutenberg	k1gMnSc1	Gutenberg
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Bertrand	Bertrando	k1gNnPc2	Bertrando
Russell	Russellum	k1gNnPc2	Russellum
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Russellova	Russellův	k2eAgFnSc1d1	Russellova
genealogie	genealogie	k1gFnSc1	genealogie
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
B.	B.	kA	B.
Russell	Russell	k1gMnSc1	Russell
-	-	kIx~	-
Metoda	metoda	k1gFnSc1	metoda
vědy	věda	k1gFnSc2	věda
je	být	k5eAaImIp3nS	být
metodou	metoda	k1gFnSc7	metoda
exaktních	exaktní	k2eAgFnPc2d1	exaktní
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
procedur	procedura	k1gFnPc2	procedura
<g/>
,	,	kIx,	,
D.	D.	kA	D.
Slouková	Sloukový	k2eAgFnSc1d1	Slouková
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Nobel	Nobel	k1gMnSc1	Nobel
Prize	Prize	k1gFnSc2	Prize
Bio	Bio	k?	Bio
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Bertrand	Bertranda	k1gFnPc2	Bertranda
Russell	Russell	k1gMnSc1	Russell
Society	societa	k1gFnSc2	societa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
https://web.archive.org/web/20070703005035/http://www.kirjasto.sci.fi/brussell.htm	[url]	k6eAd1	https://web.archive.org/web/20070703005035/http://www.kirjasto.sci.fi/brussell.htm
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Bertrand	Bertranda	k1gFnPc2	Bertranda
Russell	Russell	k1gMnSc1	Russell
Archives	Archives	k1gMnSc1	Archives
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Russell	Russell	k1gMnSc1	Russell
v	v	k7c6	v
databázi	databáze	k1gFnSc6	databáze
matematiků	matematik	k1gMnPc2	matematik
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Bertrand	Bertrand	k1gInSc1	Bertrand
Russel	Russela	k1gFnPc2	Russela
<g/>
,	,	kIx,	,
vybrané	vybraný	k2eAgInPc4d1	vybraný
texty	text	k1gInPc4	text
on-line	onin	k1gInSc5	on-lin
</s>
</p>
