<s>
Kontinentální	kontinentální	k2eAgInSc4d1	kontinentální
pohár	pohár	k1gInSc4	pohár
v	v	k7c6	v
atletice	atletika	k1gFnSc6	atletika
(	(	kIx(	(
<g/>
oficiálně	oficiálně	k6eAd1	oficiálně
<g/>
:	:	kIx,	:
IAAF	IAAF	kA	IAAF
Continental	Continental	k1gMnSc1	Continental
Cup	cup	k1gInSc1	cup
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
atletický	atletický	k2eAgInSc1d1	atletický
šampionát	šampionát	k1gInSc1	šampionát
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
pořádán	pořádán	k2eAgInSc4d1	pořádán
federací	federace	k1gFnSc7	federace
IAAF	IAAF	kA	IAAF
nepravidelně	pravidelně	k6eNd1	pravidelně
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1977	[number]	k4	1977
<g/>
.	.	kIx.	.
</s>
