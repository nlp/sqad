<p>
<s>
Kontinentální	kontinentální	k2eAgInSc4d1	kontinentální
pohár	pohár	k1gInSc4	pohár
v	v	k7c6	v
atletice	atletika	k1gFnSc6	atletika
(	(	kIx(	(
<g/>
oficiálně	oficiálně	k6eAd1	oficiálně
<g/>
:	:	kIx,	:
IAAF	IAAF	kA	IAAF
Continental	Continental	k1gMnSc1	Continental
Cup	cup	k1gInSc1	cup
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
atletický	atletický	k2eAgInSc1d1	atletický
šampionát	šampionát	k1gInSc1	šampionát
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
pořádán	pořádán	k2eAgInSc4d1	pořádán
federací	federace	k1gFnSc7	federace
IAAF	IAAF	kA	IAAF
nepravidelně	pravidelně	k6eNd1	pravidelně
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1977	[number]	k4	1977
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
svého	svůj	k3xOyFgNnSc2	svůj
založení	založení	k1gNnSc2	založení
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
nesl	nést	k5eAaImAgInS	nést
název	název	k1gInSc1	název
Světový	světový	k2eAgInSc4d1	světový
pohár	pohár	k1gInSc4	pohár
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Formát	formát	k1gInSc1	formát
tohoto	tento	k3xDgInSc2	tento
šampionátu	šampionát	k1gInSc2	šampionát
je	být	k5eAaImIp3nS	být
atypický	atypický	k2eAgInSc1d1	atypický
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
týmovou	týmový	k2eAgFnSc4d1	týmová
soutěž	soutěž	k1gFnSc4	soutěž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
soutěží	soutěžit	k5eAaImIp3nS	soutěžit
atletické	atletický	k2eAgInPc4d1	atletický
týmy	tým	k1gInPc4	tým
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
Asie	Asie	k1gFnSc2	Asie
+	+	kIx~	+
Oceánie	Oceánie	k1gFnSc2	Oceánie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
disciplíně	disciplína	k1gFnSc6	disciplína
se	se	k3xPyFc4	se
představí	představit	k5eAaPmIp3nP	představit
2	[number]	k4	2
ženy	žena	k1gFnPc1	žena
a	a	k8xC	a
2	[number]	k4	2
muži	muž	k1gMnPc1	muž
za	za	k7c4	za
každý	každý	k3xTgInSc4	každý
tým	tým	k1gInSc4	tým
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přehled	přehled	k1gInSc4	přehled
šampionátů	šampionát	k1gInPc2	šampionát
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kontinentální	kontinentální	k2eAgInSc4d1	kontinentální
pohár	pohár	k1gInSc4	pohár
v	v	k7c6	v
atletice	atletika	k1gFnSc6	atletika
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc4d1	oficiální
webové	webový	k2eAgFnPc4d1	webová
stránky	stránka	k1gFnPc4	stránka
federace	federace	k1gFnSc2	federace
IAAF	IAAF	kA	IAAF
</s>
</p>
<p>
<s>
Kompletní	kompletní	k2eAgInSc1d1	kompletní
přehled	přehled	k1gInSc1	přehled
vítězů	vítěz	k1gMnPc2	vítěz
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
ročníků	ročník	k1gInPc2	ročník
</s>
</p>
