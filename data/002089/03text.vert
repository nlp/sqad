<s>
Edinburgh	Edinburgh	k1gInSc1	Edinburgh
(	(	kIx(	(
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
<g/>
(	(	kIx(	(
<g/>
ə	ə	k?	ə
<g/>
)	)	kIx)	)
<g/>
rə	rə	k?	rə
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
<g/>
,	,	kIx,	,
skotskou	skotská	k1gFnSc7	skotská
gaelštinou	gaelština	k1gFnSc7	gaelština
Dù	Dù	k1gFnSc7	Dù
È	È	k?	È
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
též	též	k9	též
Edinburk	Edinburk	k1gInSc1	Edinburk
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
univerzitní	univerzitní	k2eAgMnSc1d1	univerzitní
město	město	k1gNnSc4	město
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
ve	v	k7c6	v
Skotsku	Skotsko	k1gNnSc6	Skotsko
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
je	být	k5eAaImIp3nS	být
také	také	k9	také
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
východě	východ	k1gInSc6	východ
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Severního	severní	k2eAgNnSc2d1	severní
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
u	u	k7c2	u
zálivu	záliv	k1gInSc2	záliv
Firth	Firth	k1gMnSc1	Firth
of	of	k?	of
Forth	Forth	k1gMnSc1	Forth
<g/>
.	.	kIx.	.
</s>
<s>
Edinburgh	Edinburgh	k1gMnSc1	Edinburgh
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Glasgow	Glasgow	k1gNnSc7	Glasgow
<g/>
,	,	kIx,	,
administrativním	administrativní	k2eAgNnSc7d1	administrativní
a	a	k8xC	a
kulturním	kulturní	k2eAgNnSc7d1	kulturní
centrem	centrum	k1gNnSc7	centrum
Skotska	Skotsko	k1gNnSc2	Skotsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zasedá	zasedat	k5eAaImIp3nS	zasedat
Skotský	skotský	k2eAgInSc1d1	skotský
parlament	parlament	k1gInSc1	parlament
a	a	k8xC	a
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
každoročně	každoročně	k6eAd1	každoročně
koná	konat	k5eAaImIp3nS	konat
tradiční	tradiční	k2eAgInSc1d1	tradiční
festival	festival	k1gInSc1	festival
<g/>
,	,	kIx,	,
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
filmový	filmový	k2eAgInSc1d1	filmový
festival	festival	k1gInSc1	festival
a	a	k8xC	a
třeba	třeba	k6eAd1	třeba
také	také	k9	také
Edinburgh	Edinburgh	k1gMnSc1	Edinburgh
Military	Militara	k1gFnSc2	Militara
Tattoo	Tattoo	k1gMnSc1	Tattoo
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
každoročně	každoročně	k6eAd1	každoročně
navštíví	navštívit	k5eAaPmIp3nS	navštívit
na	na	k7c4	na
13	[number]	k4	13
miliónů	milión	k4xCgInPc2	milión
turistů	turist	k1gMnPc2	turist
<g/>
.	.	kIx.	.
</s>
<s>
Dominantou	dominanta	k1gFnSc7	dominanta
tohoto	tento	k3xDgNnSc2	tento
historického	historický	k2eAgNnSc2d1	historické
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
zdejší	zdejší	k2eAgInSc4d1	zdejší
hrad	hrad	k1gInSc4	hrad
<g/>
,	,	kIx,	,
Arturovo	Arturův	k2eAgNnSc4d1	Arturovo
sedlo	sedlo	k1gNnSc4	sedlo
a	a	k8xC	a
Scott	Scott	k2eAgInSc4d1	Scott
Monument	monument	k1gInSc4	monument
<g/>
.	.	kIx.	.
</s>
<s>
Edinburghu	Edinburgha	k1gFnSc4	Edinburgha
byl	být	k5eAaImAgMnS	být
přidělen	přidělen	k2eAgInSc4d1	přidělen
titul	titul	k1gInSc4	titul
královské	královský	k2eAgNnSc1d1	královské
město	město	k1gNnSc1	město
ve	v	k7c6	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
ještě	ještě	k9	ještě
součástí	součást	k1gFnSc7	součást
hrabství	hrabství	k1gNnSc2	hrabství
Střední	střední	k1gMnSc1	střední
Lothian	Lothian	k1gMnSc1	Lothian
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
Edinburgh	Edinburgh	k1gInSc1	Edinburgh
samostatná	samostatný	k2eAgFnSc1d1	samostatná
správní	správní	k2eAgFnSc1d1	správní
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
chladným	chladný	k2eAgNnSc7d1	chladné
a	a	k8xC	a
větrným	větrný	k2eAgNnSc7d1	větrné
podnebím	podnebí	k1gNnSc7	podnebí
(	(	kIx(	(
<g/>
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
zimních	zimní	k2eAgInPc6d1	zimní
měsících	měsíc	k1gInPc6	měsíc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
posledního	poslední	k2eAgNnSc2d1	poslední
sčítání	sčítání	k1gNnSc2	sčítání
k	k	k7c3	k
30	[number]	k4	30
<g/>
.	.	kIx.	.
červnu	červen	k1gInSc3	červen
2007	[number]	k4	2007
žije	žít	k5eAaImIp3nS	žít
ve	v	k7c6	v
městě	město	k1gNnSc6	město
477	[number]	k4	477
380	[number]	k4	380
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
zapsáno	zapsat	k5eAaPmNgNnS	zapsat
na	na	k7c4	na
Seznam	seznam	k1gInSc4	seznam
světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Edinburgh	Edinburgh	k1gInSc4	Edinburgh
založili	založit	k5eAaPmAgMnP	založit
Římané	Říman	k1gMnPc1	Říman
<g/>
,	,	kIx,	,
když	když	k8xS	když
zde	zde	k6eAd1	zde
vybudovali	vybudovat	k5eAaPmAgMnP	vybudovat
vojenský	vojenský	k2eAgInSc4d1	vojenský
tábor	tábor	k1gInSc4	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc1	hrad
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
za	za	k7c2	za
krále	král	k1gMnSc2	král
Edwina	Edwin	k1gMnSc2	Edwin
z	z	k7c2	z
Northumberlandu	Northumberland	k1gInSc2	Northumberland
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
jeho	jeho	k3xOp3gFnPc1	jeho
části	část	k1gFnPc1	část
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1100	[number]	k4	1100
<g/>
.	.	kIx.	.
</s>
<s>
Malcolm	Malcolm	k1gMnSc1	Malcolm
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
skotského	skotský	k2eAgInSc2d1	skotský
královského	královský	k2eAgInSc2d1	královský
rodu	rod	k1gInSc2	rod
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
ho	on	k3xPp3gMnSc4	on
jako	jako	k9	jako
první	první	k4xOgFnSc7	první
vybral	vybrat	k5eAaPmAgMnS	vybrat
za	za	k7c4	za
své	svůj	k3xOyFgNnSc4	svůj
sídlo	sídlo	k1gNnSc4	sídlo
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
sídlem	sídlo	k1gNnSc7	sídlo
skotských	skotská	k1gFnPc2	skotská
králů	král	k1gMnPc2	král
královský	královský	k2eAgInSc4d1	královský
palác	palác	k1gInSc4	palác
Holyroodhouse	Holyroodhouse	k1gFnSc2	Holyroodhouse
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
královny	královna	k1gFnSc2	královna
Marie	Maria	k1gFnSc2	Maria
Stuartovny	Stuartovna	k1gFnSc2	Stuartovna
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
popravena	popravit	k5eAaPmNgFnS	popravit
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
roku	rok	k1gInSc2	rok
1587	[number]	k4	1587
<g/>
.	.	kIx.	.
</s>
<s>
Holyrood	Holyrood	k1gInSc1	Holyrood
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
britským	britský	k2eAgInSc7d1	britský
korunním	korunní	k2eAgInSc7d1	korunní
majetkem	majetek	k1gInSc7	majetek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1437	[number]	k4	1437
byl	být	k5eAaImAgInS	být
Edinburgh	Edinburgh	k1gInSc1	Edinburgh
vyhlášen	vyhlášen	k2eAgInSc1d1	vyhlášen
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Skotska	Skotsko	k1gNnSc2	Skotsko
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1603	[number]	k4	1603
město	město	k1gNnSc1	město
přišlo	přijít	k5eAaPmAgNnS	přijít
o	o	k7c4	o
titul	titul	k1gInSc4	titul
sídelní	sídelní	k2eAgNnSc1d1	sídelní
město	město	k1gNnSc1	město
krále	král	k1gMnSc2	král
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1707	[number]	k4	1707
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
se	se	k3xPyFc4	se
ve	v	k7c6	v
městě	město	k1gNnSc6	město
nescházel	scházet	k5eNaImAgInS	scházet
ani	ani	k8xC	ani
skotský	skotský	k2eAgInSc1d1	skotský
parlament	parlament	k1gInSc1	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
18	[number]	k4	18
<g/>
.	.	kIx.	.
st.	st.	kA	st.
byly	být	k5eAaImAgFnP	být
vysušeny	vysušen	k2eAgFnPc4d1	vysušena
bažiny	bažina	k1gFnPc4	bažina
pod	pod	k7c7	pod
hradem	hrad	k1gInSc7	hrad
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
výstavba	výstavba	k1gFnSc1	výstavba
Nového	Nového	k2eAgNnSc2d1	Nového
Města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
New	New	k1gFnSc1	New
Town	Town	k1gMnSc1	Town
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Současnost	současnost	k1gFnSc1	současnost
je	být	k5eAaImIp3nS	být
spojená	spojený	k2eAgFnSc1d1	spojená
s	s	k7c7	s
turismem	turismus	k1gInSc7	turismus
a	a	k8xC	a
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
spojeným	spojený	k2eAgInSc7d1	spojený
obchodem	obchod	k1gInSc7	obchod
a	a	k8xC	a
službami	služba	k1gFnPc7	služba
<g/>
.	.	kIx.	.
</s>
<s>
Edinburgh	Edinburgh	k1gInSc1	Edinburgh
každoročně	každoročně	k6eAd1	každoročně
navštíví	navštívit	k5eAaPmIp3nS	navštívit
desítky	desítka	k1gFnPc1	desítka
tisíc	tisíc	k4xCgInSc1	tisíc
návštěvníků	návštěvník	k1gMnPc2	návštěvník
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
významné	významný	k2eAgNnSc4d1	významné
finančnictví	finančnictví	k1gNnSc4	finančnictví
<g/>
.	.	kIx.	.
</s>
<s>
Edinburgh	Edinburgh	k1gInSc1	Edinburgh
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
centrem	centrum	k1gNnSc7	centrum
politiky	politika	k1gFnSc2	politika
<g/>
,	,	kIx,	,
sídlí	sídlet	k5eAaImIp3nS	sídlet
zde	zde	k6eAd1	zde
skotský	skotský	k2eAgInSc1d1	skotský
parlament	parlament	k1gInSc1	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Centrem	centr	k1gInSc7	centr
Edinburghu	Edinburgh	k1gInSc2	Edinburgh
je	být	k5eAaImIp3nS	být
Staré	Staré	k2eAgNnSc1d1	Staré
Město	město	k1gNnSc1	město
(	(	kIx(	(
<g/>
Old	Olda	k1gFnPc2	Olda
Town	Town	k1gInSc1	Town
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
stavbou	stavba	k1gFnSc7	stavba
a	a	k8xC	a
hlavní	hlavní	k2eAgFnSc7d1	hlavní
dominantou	dominanta	k1gFnSc7	dominanta
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
Edinburský	Edinburský	k2eAgInSc4d1	Edinburský
hrad	hrad	k1gInSc4	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
skále	skála	k1gFnSc6	skála
vysoké	vysoký	k2eAgFnSc6d1	vysoká
135	[number]	k4	135
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k1gNnSc1	místo
bylo	být	k5eAaImAgNnS	být
osídlené	osídlený	k2eAgNnSc1d1	osídlené
již	již	k6eAd1	již
v	v	k7c6	v
době	doba	k1gFnSc6	doba
bronzové	bronzový	k2eAgFnSc6d1	bronzová
<g/>
,	,	kIx,	,
menší	malý	k2eAgFnSc4d2	menší
pevnost	pevnost	k1gFnSc4	pevnost
zde	zde	k6eAd1	zde
stála	stát	k5eAaImAgFnS	stát
již	již	k6eAd1	již
v	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
st.	st.	kA	st.
Na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
jsou	být	k5eAaImIp3nP	být
umístěny	umístit	k5eAaPmNgInP	umístit
skotské	skotský	k2eAgInPc1d1	skotský
korunovační	korunovační	k2eAgInPc1d1	korunovační
klenoty	klenot	k1gInPc1	klenot
<g/>
,	,	kIx,	,
Stone	ston	k1gInSc5	ston
of	of	k?	of
Destiny	Destina	k1gFnPc1	Destina
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgFnPc1d3	nejstarší
budovou	budova	k1gFnSc7	budova
hradu	hrad	k1gInSc2	hrad
je	být	k5eAaImIp3nS	být
-	-	kIx~	-
románská	románský	k2eAgFnSc1d1	románská
kaple	kaple	k1gFnSc1	kaple
sv.	sv.	kA	sv.
Markéty	Markéta	k1gFnSc2	Markéta
(	(	kIx(	(
<g/>
St	St	kA	St
Margarets	Margarets	k1gInSc1	Margarets
Chapel	Chapel	k1gInSc1	Chapel
<g/>
)	)	kIx)	)
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1090	[number]	k4	1090
<g/>
,	,	kIx,	,
zajímavá	zajímavý	k2eAgFnSc1d1	zajímavá
je	být	k5eAaImIp3nS	být
brána	brána	k1gFnSc1	brána
Porticullis	Porticullis	k1gFnSc1	Porticullis
Gate	Gate	k1gFnSc1	Gate
<g/>
,	,	kIx,	,
královská	královský	k2eAgFnSc1d1	královská
hradební	hradební	k2eAgFnSc1d1	hradební
věž	věž	k1gFnSc1	věž
King	King	k1gMnSc1	King
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Bastion	bastion	k1gInSc1	bastion
<g/>
,	,	kIx,	,
najdeme	najít	k5eAaPmIp1nP	najít
zde	zde	k6eAd1	zde
také	také	k9	také
Muzeum	muzeum	k1gNnSc1	muzeum
válek	válka	k1gFnPc2	válka
(	(	kIx(	(
<g/>
National	National	k1gFnSc1	National
War	War	k1gFnSc2	War
Museum	museum	k1gNnSc1	museum
of	of	k?	of
Scotland	Scotlanda	k1gFnPc2	Scotlanda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
vchodem	vchod	k1gInSc7	vchod
do	do	k7c2	do
hradu	hrad	k1gInSc2	hrad
je	být	k5eAaImIp3nS	být
rozhledna	rozhledna	k1gFnSc1	rozhledna
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
Camera	Camera	k1gFnSc1	Camera
obscura	obscura	k1gFnSc1	obscura
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
ulicí	ulice	k1gFnSc7	ulice
na	na	k7c6	na
Starém	starý	k2eAgNnSc6d1	staré
Městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
Královská	královský	k2eAgFnSc1d1	královská
míle	míle	k1gFnSc1	míle
(	(	kIx(	(
<g/>
Royal	Royal	k1gInSc1	Royal
Mile	mile	k6eAd1	mile
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
cesta	cesta	k1gFnSc1	cesta
vedoucí	vedoucí	k2eAgFnSc1d1	vedoucí
od	od	k7c2	od
Edinburského	Edinburský	k2eAgInSc2d1	Edinburský
hradu	hrad	k1gInSc2	hrad
kolem	kolem	k7c2	kolem
tří	tři	k4xCgInPc2	tři
kostelů	kostel	k1gInPc2	kostel
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
je	být	k5eAaImIp3nS	být
katedrála	katedrála	k1gFnSc1	katedrála
St	St	kA	St
Giles	Giles	k1gMnSc1	Giles
(	(	kIx(	(
<g/>
sv.	sv.	kA	sv.
Jiljí	Jiljí	k1gMnSc2	Jiljí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
až	až	k9	až
ke	k	k7c3	k
královskému	královský	k2eAgInSc3d1	královský
paláci	palác	k1gInSc3	palác
Holyroodhouse	Holyroodhouse	k1gFnSc2	Holyroodhouse
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
k	k	k7c3	k
opatství	opatství	k1gNnSc3	opatství
v	v	k7c6	v
Holyroodu	Holyrood	k1gInSc6	Holyrood
<g/>
.	.	kIx.	.
</s>
<s>
Katedrála	katedrála	k1gFnSc1	katedrála
St	St	kA	St
Giles	Giles	k1gMnSc1	Giles
(	(	kIx(	(
<g/>
High	High	k1gMnSc1	High
Kirk	Kirk	k1gMnSc1	Kirk
of	of	k?	of
Edinburgh	Edinburgh	k1gMnSc1	Edinburgh
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
založen	založit	k5eAaPmNgInS	založit
ve	v	k7c6	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
st.	st.	kA	st.
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
původního	původní	k2eAgInSc2d1	původní
menšího	malý	k2eAgInSc2d2	menší
kostela	kostel	k1gInSc2	kostel
z	z	k7c2	z
9	[number]	k4	9
<g/>
.	.	kIx.	.
st.	st.	kA	st.
Stavba	stavba	k1gFnSc1	stavba
je	být	k5eAaImIp3nS	být
65	[number]	k4	65
m	m	kA	m
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
a	a	k8xC	a
35	[number]	k4	35
m	m	kA	m
široká	široký	k2eAgFnSc1d1	široká
<g/>
,	,	kIx,	,
významná	významný	k2eAgFnSc1d1	významná
je	být	k5eAaImIp3nS	být
katedrální	katedrální	k2eAgFnSc1d1	katedrální
věž	věž	k1gFnSc1	věž
z	z	k7c2	z
15	[number]	k4	15
<g/>
.	.	kIx.	.
st.	st.	kA	st.
<g/>
,	,	kIx,	,
v	v	k7c6	v
interiérech	interiér	k1gInPc6	interiér
pak	pak	k6eAd1	pak
kaple	kaple	k1gFnSc2	kaple
Thistle	Thistle	k1gFnSc2	Thistle
Chapel	Chapela	k1gFnPc2	Chapela
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
hradem	hrad	k1gInSc7	hrad
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
prostranství	prostranství	k1gNnSc2	prostranství
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
každoročně	každoročně	k6eAd1	každoročně
koná	konat	k5eAaImIp3nS	konat
Military	Militara	k1gFnPc4	Militara
Tattoo	Tattoo	k6eAd1	Tattoo
-	-	kIx~	-
vojenská	vojenský	k2eAgFnSc1d1	vojenská
historicko-kulturní	historickoulturní	k2eAgFnSc1d1	historicko-kulturní
přehlídka	přehlídka	k1gFnSc1	přehlídka
<g/>
,	,	kIx,	,
nachází	nacházet	k5eAaImIp3nS	nacházet
i	i	k8xC	i
Cannonball	Cannonball	k1gMnSc1	Cannonball
House	house	k1gNnSc1	house
<g/>
,	,	kIx,	,
dům	dům	k1gInSc1	dům
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zůstala	zůstat	k5eAaPmAgFnS	zůstat
koule	koule	k1gFnSc1	koule
z	z	k7c2	z
děla	dělo	k1gNnSc2	dělo
z	z	k7c2	z
povstání	povstání	k1gNnSc2	povstání
Jakobitů	jakobita	k1gMnPc2	jakobita
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1745	[number]	k4	1745
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
významné	významný	k2eAgFnPc1d1	významná
stavby	stavba	k1gFnPc1	stavba
a	a	k8xC	a
místa	místo	k1gNnPc1	místo
<g/>
:	:	kIx,	:
Holyrood	Holyrood	k1gInSc1	Holyrood
Palace	Palace	k1gFnSc2	Palace
<g/>
,	,	kIx,	,
královský	královský	k2eAgInSc1d1	královský
palác	palác	k1gInSc1	palác
<g/>
,	,	kIx,	,
současná	současný	k2eAgFnSc1d1	současná
stavba	stavba	k1gFnSc1	stavba
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1670	[number]	k4	1670
-	-	kIx~	-
79	[number]	k4	79
a	a	k8xC	a
Holyroodské	Holyroodský	k2eAgNnSc4d1	Holyroodský
opatství	opatství	k1gNnSc4	opatství
(	(	kIx(	(
<g/>
Holyrood	Holyrood	k1gInSc1	Holyrood
Abbey	Abbea	k1gFnSc2	Abbea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zbytky	zbytek	k1gInPc7	zbytek
opatství	opatství	k1gNnSc2	opatství
založeného	založený	k2eAgNnSc2d1	založené
ve	v	k7c6	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
st.	st.	kA	st.
a	a	k8xC	a
poškozeného	poškozený	k2eAgInSc2d1	poškozený
a	a	k8xC	a
zříceného	zřícený	k2eAgInSc2d1	zřícený
v	v	k7c4	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
st.	st.	kA	st.
Courts	Courts	k1gInSc1	Courts
of	of	k?	of
Scotland	Scotland	k1gInSc1	Scotland
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
budova	budova	k1gFnSc1	budova
parlamentu	parlament	k1gInSc2	parlament
postavená	postavený	k2eAgFnSc1d1	postavená
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1632	[number]	k4	1632
-	-	kIx~	-
40	[number]	k4	40
<g/>
,	,	kIx,	,
přestavěná	přestavěný	k2eAgFnSc1d1	přestavěná
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
st.	st.	kA	st.
<g/>
,	,	kIx,	,
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
zde	zde	k6eAd1	zde
sídlí	sídlet	k5eAaImIp3nS	sídlet
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
Grassmarket	Grassmarket	k1gInSc1	Grassmarket
<g/>
,	,	kIx,	,
historický	historický	k2eAgInSc1d1	historický
trh	trh	k1gInSc1	trh
Canongate	Canongat	k1gInSc5	Canongat
Tolbooth	Tolbooth	k1gInSc1	Tolbooth
<g/>
,	,	kIx,	,
stará	starý	k2eAgFnSc1d1	stará
radnice	radnice	k1gFnSc1	radnice
založená	založený	k2eAgFnSc1d1	založená
roku	rok	k1gInSc2	rok
1143	[number]	k4	1143
<g/>
,	,	kIx,	,
současná	současný	k2eAgFnSc1d1	současná
stavba	stavba	k1gFnSc1	stavba
<g />
.	.	kIx.	.
</s>
<s>
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1591	[number]	k4	1591
City	city	k1gNnSc1	city
Chambers	Chambersa	k1gFnPc2	Chambersa
<g/>
,	,	kIx,	,
nová	nový	k2eAgFnSc1d1	nová
radnice	radnice	k1gFnSc1	radnice
<g/>
,	,	kIx,	,
postavena	postaven	k2eAgFnSc1d1	postavena
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1753	[number]	k4	1753
-	-	kIx~	-
61	[number]	k4	61
původně	původně	k6eAd1	původně
jako	jako	k8xS	jako
centrum	centrum	k1gNnSc1	centrum
obchodu	obchod	k1gInSc2	obchod
Tolbooth	Tolbooth	k1gMnSc1	Tolbooth
Kirk	Kirk	k1gMnSc1	Kirk
<g/>
,	,	kIx,	,
kostel	kostel	k1gInSc1	kostel
z	z	k7c2	z
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
věží	věžit	k5eAaImIp3nS	věžit
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
postavený	postavený	k2eAgInSc1d1	postavený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1840	[number]	k4	1840
Tron	Tron	k1gMnSc1	Tron
Kirk	Kirk	k1gMnSc1	Kirk
<g/>
,	,	kIx,	,
presbyteriánský	presbyteriánský	k2eAgInSc1d1	presbyteriánský
kostel	kostel	k1gInSc1	kostel
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1630	[number]	k4	1630
Greyfriars	Greyfriars	k1gInSc1	Greyfriars
Kirk	Kirk	k1gInSc4	Kirk
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
kostel	kostel	k1gInSc1	kostel
založený	založený	k2eAgInSc1d1	založený
roku	rok	k1gInSc2	rok
1602	[number]	k4	1602
John	John	k1gMnSc1	John
Knox	Knox	k1gInSc1	Knox
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
House	house	k1gNnSc1	house
<g/>
,	,	kIx,	,
dům	dům	k1gInSc1	dům
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1490	[number]	k4	1490
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
st.	st.	kA	st.
bydlel	bydlet	k5eAaImAgMnS	bydlet
kazatel	kazatel	k1gMnSc1	kazatel
J.	J.	kA	J.
Knox	Knox	k1gInSc1	Knox
Princess	Princess	k1gInSc1	Princess
Street	Street	k1gInSc1	Street
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgFnSc1d1	hlavní
ulice	ulice	k1gFnSc1	ulice
Nového	Nového	k2eAgNnSc2d1	Nového
Města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
New	New	k1gFnSc1	New
Town	Town	k1gMnSc1	Town
<g/>
)	)	kIx)	)
a	a	k8xC	a
Charlotte	Charlott	k1gInSc5	Charlott
Square	square	k1gInSc1	square
<g/>
,	,	kIx,	,
náměstí	náměstí	k1gNnSc1	náměstí
<g />
.	.	kIx.	.
</s>
<s>
z	z	k7c2	z
konce	konec	k1gInSc2	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
st.	st.	kA	st.
Královské	královský	k2eAgNnSc1d1	královské
muzeum	muzeum	k1gNnSc1	muzeum
(	(	kIx(	(
<g/>
Royal	Royal	k1gInSc1	Royal
Scottisch	Scottisch	k1gInSc1	Scottisch
Museum	museum	k1gNnSc1	museum
<g/>
)	)	kIx)	)
a	a	k8xC	a
National	National	k1gFnSc1	National
Gallery	Galler	k1gInPc1	Galler
of	of	k?	of
Scotland	Scotland	k1gInSc1	Scotland
(	(	kIx(	(
<g/>
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
sbírek	sbírka	k1gFnPc2	sbírka
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
umění	umění	k1gNnSc2	umění
ve	v	k7c6	v
Skotsku	Skotsko	k1gNnSc6	Skotsko
<g/>
,	,	kIx,	,
z	z	k7c2	z
období	období	k1gNnSc2	období
15	[number]	k4	15
<g/>
.	.	kIx.	.
až	až	k9	až
20	[number]	k4	20
<g/>
.	.	kIx.	.
st.	st.	kA	st.
<g/>
,	,	kIx,	,
díla	dílo	k1gNnSc2	dílo
Rembrandta	Rembrandto	k1gNnSc2	Rembrandto
<g/>
,	,	kIx,	,
Van	van	k1gInSc1	van
<g />
.	.	kIx.	.
</s>
<s>
Dycka	Dycka	k1gFnSc1	Dycka
<g/>
,	,	kIx,	,
Rubense	Rubense	k1gFnSc1	Rubense
<g/>
,	,	kIx,	,
britských	britský	k2eAgMnPc2d1	britský
malířů	malíř	k1gMnPc2	malíř
Reynoldse	Reynolds	k1gMnSc2	Reynolds
a	a	k8xC	a
Gainsborougha	Gainsborough	k1gMnSc2	Gainsborough
<g/>
)	)	kIx)	)
Balmoral	Balmoral	k1gFnSc1	Balmoral
Hotel	hotel	k1gInSc1	hotel
<g/>
,	,	kIx,	,
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
na	na	k7c4	na
Princess	Princess	k1gInSc4	Princess
St	St	kA	St
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
dominantních	dominantní	k2eAgFnPc2d1	dominantní
staveb	stavba	k1gFnPc2	stavba
na	na	k7c6	na
Novém	nový	k2eAgNnSc6d1	nové
Městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1902	[number]	k4	1902
ve	v	k7c6	v
victoriánském	victoriánský	k2eAgInSc6d1	victoriánský
stylu	styl	k1gInSc6	styl
Z	z	k7c2	z
nehistorických	historický	k2eNgFnPc2d1	nehistorická
atrakcí	atrakce	k1gFnPc2	atrakce
je	být	k5eAaImIp3nS	být
oblíbená	oblíbený	k2eAgFnSc1d1	oblíbená
místní	místní	k2eAgFnSc1d1	místní
zoo	zoo	k1gFnSc1	zoo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
lze	lze	k6eAd1	lze
spatřit	spatřit	k5eAaPmF	spatřit
mnoho	mnoho	k4c4	mnoho
různých	různý	k2eAgInPc2d1	různý
druhů	druh	k1gInPc2	druh
fauny	fauna	k1gFnSc2	fauna
<g/>
,	,	kIx,	,
od	od	k7c2	od
šimpanzů	šimpanz	k1gMnPc2	šimpanz
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
tygry	tygr	k1gMnPc4	tygr
<g/>
,	,	kIx,	,
žáby	žába	k1gFnSc2	žába
až	až	k9	až
k	k	k7c3	k
ptactvu	ptactvo	k1gNnSc3	ptactvo
<g/>
.	.	kIx.	.
</s>
<s>
Celé	celý	k2eAgNnSc1d1	celé
zoo	zoo	k1gNnSc1	zoo
je	být	k5eAaImIp3nS	být
vystavěno	vystavět	k5eAaPmNgNnS	vystavět
směrem	směr	k1gInSc7	směr
do	do	k7c2	do
kopce	kopec	k1gInSc2	kopec
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
návštěvníkům	návštěvník	k1gMnPc3	návštěvník
panoramatický	panoramatický	k2eAgInSc4d1	panoramatický
pohled	pohled	k1gInSc4	pohled
na	na	k7c4	na
historické	historický	k2eAgNnSc4d1	historické
centrum	centrum	k1gNnSc4	centrum
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Edinburghu	Edinburgh	k1gInSc6	Edinburgh
sídlí	sídlet	k5eAaImIp3nS	sídlet
skotský	skotský	k2eAgInSc1d1	skotský
parlament	parlament	k1gInSc1	parlament
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
kontroverzní	kontroverzní	k2eAgFnSc6d1	kontroverzní
novostavbě	novostavba	k1gFnSc6	novostavba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vzbudila	vzbudit	k5eAaPmAgFnS	vzbudit
rozruch	rozruch	k1gInSc4	rozruch
domácích	domácí	k1gMnPc2	domácí
jak	jak	k8xC	jak
svým	svůj	k3xOyFgInSc7	svůj
vzhledem	vzhled	k1gInSc7	vzhled
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
hlavně	hlavně	k9	hlavně
cenou	cena	k1gFnSc7	cena
<g/>
.	.	kIx.	.
</s>
<s>
Budova	budova	k1gFnSc1	budova
byla	být	k5eAaImAgFnS	být
zprovozněna	zprovoznit	k5eAaPmNgFnS	zprovoznit
v	v	k7c6	v
září	září	k1gNnSc6	září
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomika	ekonomika	k1gFnSc1	ekonomika
Edinburghu	Edinburgh	k1gInSc2	Edinburgh
těží	těžet	k5eAaImIp3nS	těžet
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
finančnictví	finančnictví	k1gNnSc2	finančnictví
<g/>
,	,	kIx,	,
turismu	turismus	k1gInSc2	turismus
a	a	k8xC	a
služeb	služba	k1gFnPc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
finančnictví	finančnictví	k1gNnSc2	finančnictví
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
druhé	druhý	k4xOgNnSc4	druhý
nejdůležitější	důležitý	k2eAgNnSc4d3	nejdůležitější
centrum	centrum	k1gNnSc4	centrum
po	po	k7c6	po
Londýnu	Londýn	k1gInSc3	Londýn
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
městě	město	k1gNnSc6	město
pouze	pouze	k6eAd1	pouze
2,2	[number]	k4	2,2
procenta	procento	k1gNnSc2	procento
<g/>
.	.	kIx.	.
</s>
<s>
Bankovnictví	bankovnictví	k1gNnSc1	bankovnictví
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
Edinburghu	Edinburgh	k1gInSc6	Edinburgh
bohatou	bohatý	k2eAgFnSc4d1	bohatá
historii	historie	k1gFnSc4	historie
již	již	k6eAd1	již
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
300	[number]	k4	300
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
počátku	počátek	k1gInSc6	počátek
stál	stát	k5eAaImAgInS	stát
vznik	vznik	k1gInSc1	vznik
Skotské	skotský	k2eAgFnSc2d1	skotská
banky	banka	k1gFnSc2	banka
(	(	kIx(	(
<g/>
Bank	bank	k1gInSc1	bank
of	of	k?	of
Scotland	Scotland	k1gInSc1	Scotland
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
Edinburgh	Edinburgh	k1gMnSc1	Edinburgh
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
šest	šest	k4xCc4	šest
nejvýznamnějších	významný	k2eAgNnPc2d3	nejvýznamnější
finančních	finanční	k2eAgNnPc2d1	finanční
center	centrum	k1gNnPc2	centrum
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Edinburghu	Edinburgh	k1gInSc6	Edinburgh
jsou	být	k5eAaImIp3nP	být
dva	dva	k4xCgInPc1	dva
profesionální	profesionální	k2eAgInPc1d1	profesionální
fotbalové	fotbalový	k2eAgInPc1d1	fotbalový
kluby	klub	k1gInPc1	klub
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
oba	dva	k4xCgInPc1	dva
hrají	hrát	k5eAaImIp3nP	hrát
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
ligu	liga	k1gFnSc4	liga
SPL	SPL	kA	SPL
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
Hibernian	Hibernian	k1gMnSc1	Hibernian
FC	FC	kA	FC
(	(	kIx(	(
<g/>
přezdívaní	přezdívaný	k2eAgMnPc1d1	přezdívaný
the	the	k?	the
Hibs	Hibs	k1gInSc4	Hibs
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Hibees	Hibees	k1gInSc1	Hibees
<g/>
)	)	kIx)	)
a	a	k8xC	a
Heart	Heart	k1gInSc1	Heart
of	of	k?	of
Midlothian	Midlothian	k1gInSc1	Midlothian
FC	FC	kA	FC
(	(	kIx(	(
<g/>
the	the	k?	the
Jambos	Jambos	k1gInSc1	Jambos
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
zde	zde	k6eAd1	zde
působil	působit	k5eAaImAgMnS	působit
klub	klub	k1gInSc4	klub
Meadowbank	Meadowbank	k1gMnSc1	Meadowbank
Thistle	Thistle	k1gMnSc1	Thistle
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
přesunul	přesunout	k5eAaPmAgMnS	přesunout
do	do	k7c2	do
města	město	k1gNnSc2	město
Livingston	Livingston	k1gInSc1	Livingston
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgInSc1d1	národní
rugbyový	rugbyový	k2eAgInSc1d1	rugbyový
tým	tým	k1gInSc1	tým
hraje	hrát	k5eAaImIp3nS	hrát
své	svůj	k3xOyFgInPc4	svůj
zápasy	zápas	k1gInPc4	zápas
na	na	k7c6	na
stadionu	stadion	k1gInSc6	stadion
Murrayfield	Murrayfielda	k1gFnPc2	Murrayfielda
<g/>
,	,	kIx,	,
patřící	patřící	k2eAgFnSc4d1	patřící
instituci	instituce	k1gFnSc4	instituce
Scottish	Scottish	k1gInSc1	Scottish
Rugby	rugby	k1gNnSc2	rugby
Union	union	k1gInSc1	union
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
populárními	populární	k2eAgInPc7d1	populární
sporty	sport	k1gInPc7	sport
jsou	být	k5eAaImIp3nP	být
kriket	kriket	k1gInSc4	kriket
a	a	k8xC	a
lední	lední	k2eAgInSc4d1	lední
hokej	hokej	k1gInSc4	hokej
<g/>
.	.	kIx.	.
</s>
<s>
Edinburgh	Edinburgh	k1gInSc1	Edinburgh
hostil	hostit	k5eAaImAgInS	hostit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
hry	hra	k1gFnSc2	hra
Commonwealthu	Commonwealth	k1gInSc2	Commonwealth
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
oficiálních	oficiální	k2eAgInPc2d1	oficiální
zdrojů	zdroj	k1gInPc2	zdroj
byl	být	k5eAaImAgInS	být
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
města	město	k1gNnSc2	město
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
477	[number]	k4	477
830	[number]	k4	830
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
počtem	počet	k1gInSc7	počet
se	se	k3xPyFc4	se
Edinburgh	Edinburgha	k1gFnPc2	Edinburgha
řadí	řadit	k5eAaImIp3nS	řadit
na	na	k7c6	na
sedmé	sedmý	k4xOgFnSc6	sedmý
místo	místo	k7c2	místo
největších	veliký	k2eAgFnPc2d3	veliký
měst	město	k1gNnPc2	město
Velké	velká	k1gFnSc2	velká
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
Severního	severní	k2eAgNnSc2d1	severní
Irska	Irsko	k1gNnSc2	Irsko
co	co	k9	co
do	do	k7c2	do
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
počtu	počet	k1gInSc2	počet
bylo	být	k5eAaImAgNnS	být
247	[number]	k4	247
736	[number]	k4	736
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
230	[number]	k4	230
094	[number]	k4	094
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgInSc7d3	nejznámější
festivalem	festival	k1gInSc7	festival
je	být	k5eAaImIp3nS	být
Edinburský	Edinburský	k2eAgInSc1d1	Edinburský
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
začíná	začínat	k5eAaImIp3nS	začínat
pravidelně	pravidelně	k6eAd1	pravidelně
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
posledním	poslední	k2eAgInSc7d1	poslední
týdnem	týden	k1gInSc7	týden
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
a	a	k8xC	a
trvá	trvat	k5eAaImIp3nS	trvat
do	do	k7c2	do
konce	konec	k1gInSc2	konec
srpna	srpen	k1gInSc2	srpen
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
začátku	začátek	k1gInSc2	začátek
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1947	[number]	k4	1947
lze	lze	k6eAd1	lze
během	během	k7c2	během
tohoto	tento	k3xDgInSc2	tento
festivalu	festival	k1gInSc2	festival
spatřit	spatřit	k5eAaPmF	spatřit
mnoho	mnoho	k6eAd1	mnoho
umělců	umělec	k1gMnPc2	umělec
a	a	k8xC	a
výtvorů	výtvor	k1gInPc2	výtvor
z	z	k7c2	z
různých	různý	k2eAgNnPc2d1	různé
odvětví	odvětví	k1gNnPc2	odvětví
kulturního	kulturní	k2eAgInSc2d1	kulturní
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
počínaje	počínaje	k7c7	počínaje
baletem	balet	k1gInSc7	balet
a	a	k8xC	a
konče	konče	k7c7	konče
pouliční	pouliční	k2eAgFnSc7d1	pouliční
serenádou	serenáda	k1gFnSc7	serenáda
<g/>
.	.	kIx.	.
</s>
<s>
Bohatý	bohatý	k2eAgInSc1d1	bohatý
program	program	k1gInSc1	program
přiláká	přilákat	k5eAaPmIp3nS	přilákat
každoročně	každoročně	k6eAd1	každoročně
desetitisíce	desetitisíce	k1gInPc4	desetitisíce
turistů	turist	k1gMnPc2	turist
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
bylo	být	k5eAaImAgNnS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
na	na	k7c4	na
1867	[number]	k4	1867
představení	představení	k1gNnPc2	představení
na	na	k7c6	na
261	[number]	k4	261
různých	různý	k2eAgNnPc6d1	různé
místech	místo	k1gNnPc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Edinburghu	Edinburgh	k1gInSc6	Edinburgh
a	a	k8xC	a
okolí	okolí	k1gNnSc6	okolí
panují	panovat	k5eAaImIp3nP	panovat
podobné	podobný	k2eAgFnPc4d1	podobná
klimatické	klimatický	k2eAgFnPc4d1	klimatická
podmínky	podmínka	k1gFnPc4	podmínka
přímořského	přímořský	k2eAgInSc2d1	přímořský
charakteru	charakter	k1gInSc2	charakter
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
na	na	k7c6	na
zbytku	zbytek	k1gInSc6	zbytek
východního	východní	k2eAgNnSc2d1	východní
pobřeží	pobřeží	k1gNnSc2	pobřeží
Skotska	Skotsko	k1gNnSc2	Skotsko
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
mírné	mírný	k2eAgFnSc2d1	mírná
zimy	zima	k1gFnSc2	zima
a	a	k8xC	a
chladnější	chladný	k2eAgNnPc4d2	chladnější
léta	léto	k1gNnPc4	léto
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
ovlivněno	ovlivnit	k5eAaPmNgNnS	ovlivnit
především	především	k6eAd1	především
účinky	účinek	k1gInPc4	účinek
Golfského	golfský	k2eAgInSc2d1	golfský
proudu	proud	k1gInSc2	proud
<g/>
.	.	kIx.	.
</s>
<s>
Zimní	zimní	k2eAgFnPc1d1	zimní
teploty	teplota	k1gFnPc1	teplota
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
okolo	okolo	k7c2	okolo
nuly	nula	k1gFnSc2	nula
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
oproti	oproti	k7c3	oproti
územím	území	k1gNnPc3	území
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
klimatickém	klimatický	k2eAgInSc6d1	klimatický
pásu	pás	k1gInSc6	pás
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
,	,	kIx,	,
Labrador	Labrador	k1gInSc1	Labrador
<g/>
)	)	kIx)	)
velice	velice	k6eAd1	velice
mírné	mírný	k2eAgNnSc1d1	mírné
<g/>
.	.	kIx.	.
</s>
<s>
Zimy	zima	k1gFnPc1	zima
bývají	bývat	k5eAaImIp3nP	bývat
velmi	velmi	k6eAd1	velmi
větrné	větrný	k2eAgInPc1d1	větrný
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
tomu	ten	k3xDgNnSc3	ten
letní	letní	k2eAgFnPc1d1	letní
teploty	teplota	k1gFnPc1	teplota
jen	jen	k9	jen
málokdy	málokdy	k6eAd1	málokdy
překračují	překračovat	k5eAaImIp3nP	překračovat
23	[number]	k4	23
°	°	k?	°
<g/>
C.	C.	kA	C.
Golfský	golfský	k2eAgInSc1d1	golfský
proud	proud	k1gInSc1	proud
přináší	přinášet	k5eAaImIp3nS	přinášet
od	od	k7c2	od
jihozápadu	jihozápad	k1gInSc2	jihozápad
srážky	srážka	k1gFnSc2	srážka
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
jihovýchodní	jihovýchodní	k2eAgInPc1d1	jihovýchodní
větry	vítr	k1gInPc1	vítr
jsou	být	k5eAaImIp3nP	být
suché	suchý	k2eAgFnPc1d1	suchá
<g/>
,	,	kIx,	,
studeného	studený	k2eAgInSc2d1	studený
charakteru	charakter	k1gInSc2	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Značné	značný	k2eAgNnSc1d1	značné
proudění	proudění	k1gNnSc1	proudění
vzduchu	vzduch	k1gInSc2	vzduch
v	v	k7c6	v
těchto	tento	k3xDgNnPc6	tento
místech	místo	k1gNnPc6	místo
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
poloha	poloha	k1gFnSc1	poloha
města	město	k1gNnSc2	město
přímo	přímo	k6eAd1	přímo
u	u	k7c2	u
Severního	severní	k2eAgNnSc2d1	severní
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Alexander	Alexandra	k1gFnPc2	Alexandra
Graham	Graham	k1gMnSc1	Graham
Bell	bell	k1gInSc1	bell
-	-	kIx~	-
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
hlavně	hlavně	k9	hlavně
ve	v	k7c6	v
spojitosti	spojitost	k1gFnSc6	spojitost
se	s	k7c7	s
vznikem	vznik	k1gInSc7	vznik
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
rozšířením	rozšíření	k1gNnSc7	rozšíření
telefonu	telefon	k1gInSc2	telefon
Tony	Tony	k1gFnSc1	Tony
Blair	Blair	k1gMnSc1	Blair
-	-	kIx~	-
bývalý	bývalý	k2eAgMnSc1d1	bývalý
Ministerský	ministerský	k2eAgMnSc1d1	ministerský
předseda	předseda	k1gMnSc1	předseda
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
James	James	k1gMnSc1	James
Clerk	Clerk	k1gInSc4	Clerk
Maxwell	maxwell	k1gInSc1	maxwell
-	-	kIx~	-
fyzik	fyzik	k1gMnSc1	fyzik
Sir	sir	k1gMnSc1	sir
Arthur	Arthur	k1gMnSc1	Arthur
Conan	Conan	k1gMnSc1	Conan
Doyle	Doyle	k1gFnSc1	Doyle
-	-	kIx~	-
autor	autor	k1gMnSc1	autor
Sherlocka	Sherlocko	k1gNnSc2	Sherlocko
Holmese	Holmese	k1gFnSc2	Holmese
Sir	sir	k1gMnSc1	sir
Sean	Sean	k1gMnSc1	Sean
Connery	Conner	k1gMnPc4	Conner
-	-	kIx~	-
herec	herec	k1gMnSc1	herec
Ian	Ian	k1gMnSc1	Ian
Anderson	Anderson	k1gMnSc1	Anderson
-	-	kIx~	-
hudebník	hudebník	k1gMnSc1	hudebník
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
hudební	hudební	k2eAgFnSc2d1	hudební
skupiny	skupina	k1gFnSc2	skupina
Jethro	Jethra	k1gFnSc5	Jethra
Tull	Tull	k1gMnSc1	Tull
Wattie	Wattie	k1gFnSc2	Wattie
Buchan	Buchan	k1gMnSc1	Buchan
-	-	kIx~	-
zpěvák	zpěvák	k1gMnSc1	zpěvák
a	a	k8xC	a
zakládající	zakládající	k2eAgMnSc1d1	zakládající
člen	člen	k1gMnSc1	člen
skupiny	skupina	k1gFnSc2	skupina
The	The	k1gMnSc1	The
Exploited	Exploited	k1gMnSc1	Exploited
<g/>
.	.	kIx.	.
</s>
<s>
Isabella	Isabella	k1gMnSc1	Isabella
Glyn	Glyn	k1gMnSc1	Glyn
-	-	kIx~	-
(	(	kIx(	(
<g/>
1823	[number]	k4	1823
<g/>
-	-	kIx~	-
<g/>
1889	[number]	k4	1889
<g/>
)	)	kIx)	)
herečka	herečka	k1gFnSc1	herečka
David	David	k1gMnSc1	David
Hume	Hume	k1gInSc1	Hume
-	-	kIx~	-
filozof	filozof	k1gMnSc1	filozof
a	a	k8xC	a
historik	historik	k1gMnSc1	historik
James	James	k1gMnSc1	James
Hutton	Hutton	k1gInSc1	Hutton
-	-	kIx~	-
geolog	geolog	k1gMnSc1	geolog
Shirley	Shirlea	k1gFnSc2	Shirlea
Manson	Manson	k1gMnSc1	Manson
-	-	kIx~	-
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
skupiny	skupina	k1gFnSc2	skupina
Garbage	Garbage	k1gFnPc2	Garbage
John	John	k1gMnSc1	John
Napier	Napier	k1gMnSc1	Napier
-	-	kIx~	-
matematik	matematik	k1gMnSc1	matematik
Max	Max	k1gMnSc1	Max
Born	Born	k1gMnSc1	Born
-	-	kIx~	-
fyzik	fyzik	k1gMnSc1	fyzik
Ian	Ian	k1gMnSc1	Ian
Rankin	Rankin	k1gMnSc1	Rankin
-	-	kIx~	-
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
univerzitní	univerzitní	k2eAgMnSc1d1	univerzitní
profesor	profesor	k1gMnSc1	profesor
Walter	Walter	k1gMnSc1	Walter
Scott	Scott	k1gMnSc1	Scott
-	-	kIx~	-
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1771	[number]	k4	1771
<g/>
-	-	kIx~	-
<g/>
1832	[number]	k4	1832
<g/>
)	)	kIx)	)
spisovatel	spisovatel	k1gMnSc1	spisovatel
Adam	Adam	k1gMnSc1	Adam
Smith	Smith	k1gMnSc1	Smith
-	-	kIx~	-
ekonom	ekonom	k1gMnSc1	ekonom
Muriel	Muriel	k1gMnSc1	Muriel
Spark	Spark	k1gInSc1	Spark
-	-	kIx~	-
spisovatel	spisovatel	k1gMnSc1	spisovatel
Robert	Robert	k1gMnSc1	Robert
Louis	Louis	k1gMnSc1	Louis
Stevenson	Stevenson	k1gMnSc1	Stevenson
-	-	kIx~	-
spisovatel	spisovatel	k1gMnSc1	spisovatel
Robin	Robina	k1gFnPc2	Robina
Harper	Harper	k1gMnSc1	Harper
-	-	kIx~	-
politik	politik	k1gMnSc1	politik
Irvine	Irvin	k1gInSc5	Irvin
Welsh	Welsh	k1gMnSc1	Welsh
-	-	kIx~	-
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
scenárista	scenárista	k1gMnSc1	scenárista
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
předlohy	předloha	k1gFnSc2	předloha
k	k	k7c3	k
filmu	film	k1gInSc3	film
Trainspotting	Trainspotting	k1gInSc1	Trainspotting
Joanne	Joann	k1gInSc5	Joann
Rowlingová	Rowlingový	k2eAgFnSc1d1	Rowlingová
-	-	kIx~	-
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
Dylan	Dylana	k1gFnPc2	Dylana
Moran	Morana	k1gFnPc2	Morana
-	-	kIx~	-
irský	irský	k2eAgMnSc1d1	irský
komik	komik	k1gMnSc1	komik
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
scenárista	scenárista	k1gMnSc1	scenárista
Mnichov	Mnichov	k1gInSc1	Mnichov
-	-	kIx~	-
Německo	Německo	k1gNnSc1	Německo
Florencie	Florencie	k1gFnSc1	Florencie
-	-	kIx~	-
Itálie	Itálie	k1gFnSc1	Itálie
Nice	Nice	k1gFnSc1	Nice
-	-	kIx~	-
Francie	Francie	k1gFnSc1	Francie
Vancouver	Vancouver	k1gInSc1	Vancouver
-	-	kIx~	-
Kanada	Kanada	k1gFnSc1	Kanada
Kyjev	Kyjev	k1gInSc1	Kyjev
-	-	kIx~	-
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
Aalborg	Aalborg	k1gInSc1	Aalborg
-	-	kIx~	-
Dánsko	Dánsko	k1gNnSc1	Dánsko
San	San	k1gMnSc1	San
Diego	Diego	k1gMnSc1	Diego
-	-	kIx~	-
USA	USA	kA	USA
Dunedin	Dunedin	k2eAgInSc1d1	Dunedin
-	-	kIx~	-
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
Krakov	Krakov	k1gInSc1	Krakov
-	-	kIx~	-
Polsko	Polsko	k1gNnSc1	Polsko
Si	se	k3xPyFc3	se
<g/>
'	'	kIx"	'
<g/>
an	an	k?	an
-	-	kIx~	-
Čína	Čína	k1gFnSc1	Čína
</s>
