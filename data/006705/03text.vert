<s>
Huygensův	Huygensův	k2eAgInSc1d1	Huygensův
princip	princip	k1gInSc1	princip
popsaný	popsaný	k2eAgInSc1d1	popsaný
Christianem	Christian	k1gMnSc7	Christian
Huygensem	Huygens	k1gMnSc7	Huygens
popisuje	popisovat	k5eAaImIp3nS	popisovat
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
představ	představa	k1gFnPc2	představa
o	o	k7c4	o
šíření	šíření	k1gNnSc4	šíření
vlnění	vlnění	k1gNnSc2	vlnění
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
okamžiku	okamžik	k1gInSc6	okamžik
lze	lze	k6eAd1	lze
každý	každý	k3xTgInSc4	každý
bod	bod	k1gInSc4	bod
na	na	k7c6	na
čele	čelo	k1gNnSc6	čelo
šířící	šířící	k2eAgFnSc2d1	šířící
se	se	k3xPyFc4	se
vlny	vlna	k1gFnSc2	vlna
chápat	chápat	k5eAaImF	chápat
jako	jako	k8xC	jako
nový	nový	k2eAgInSc4d1	nový
zdroj	zdroj	k1gInSc4	zdroj
vlnění	vlnění	k1gNnSc2	vlnění
(	(	kIx(	(
<g/>
sekundárních	sekundární	k2eAgFnPc2d1	sekundární
vln	vlna	k1gFnPc2	vlna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
tvar	tvar	k1gInSc1	tvar
čela	čelo	k1gNnSc2	čelo
vlny	vlna	k1gFnSc2	vlna
v	v	k7c6	v
čase	čas	k1gInSc6	čas
o	o	k7c4	o
malý	malý	k2eAgInSc4d1	malý
okamžik	okamžik	k1gInSc4	okamžik
pozdějším	pozdní	k2eAgNnSc6d2	pozdější
lze	lze	k6eAd1	lze
pak	pak	k6eAd1	pak
určit	určit	k5eAaPmF	určit
jako	jako	k9	jako
vnější	vnější	k2eAgFnSc4d1	vnější
obálku	obálka	k1gFnSc4	obálka
vln	vlna	k1gFnPc2	vlna
<g/>
,	,	kIx,	,
šířících	šířící	k2eAgMnPc2d1	šířící
se	se	k3xPyFc4	se
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Huygensův	Huygensův	k2eAgInSc1d1	Huygensův
princip	princip	k1gInSc1	princip
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
správný	správný	k2eAgInSc1d1	správný
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
například	například	k6eAd1	například
vlna	vlna	k1gFnSc1	vlna
procházející	procházející	k2eAgFnSc2d1	procházející
vzduchem	vzduch	k1gInSc7	vzduch
či	či	k8xC	či
vodou	voda	k1gFnSc7	voda
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
bodů	bod	k1gInPc2	bod
vracela	vracet	k5eAaImAgFnS	vracet
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
zdroje	zdroj	k1gInSc2	zdroj
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
odrazila	odrazit	k5eAaPmAgFnS	odrazit
od	od	k7c2	od
nějaké	nějaký	k3yIgFnSc2	nějaký
překážky	překážka	k1gFnSc2	překážka
<g/>
.	.	kIx.	.
</s>
<s>
Huygensovy	Huygensův	k2eAgFnSc2d1	Huygensova
představy	představa	k1gFnSc2	představa
doplnil	doplnit	k5eAaPmAgMnS	doplnit
francouzský	francouzský	k2eAgMnSc1d1	francouzský
fyzik	fyzik	k1gMnSc1	fyzik
Augustin-Jean	Augustin-Jean	k1gMnSc1	Augustin-Jean
Fresnel	Fresnel	k1gMnSc1	Fresnel
<g/>
.	.	kIx.	.
</s>
<s>
Upřesněný	upřesněný	k2eAgInSc1d1	upřesněný
Huygensův-Fresnelův	Huygensův-Fresnelův	k2eAgInSc1d1	Huygensův-Fresnelův
princip	princip	k1gInSc1	princip
doplňuje	doplňovat	k5eAaImIp3nS	doplňovat
původní	původní	k2eAgFnSc4d1	původní
představu	představa	k1gFnSc4	představa
o	o	k7c4	o
interferenci	interference	k1gFnSc4	interference
sekundárních	sekundární	k2eAgFnPc2d1	sekundární
vln	vlna	k1gFnPc2	vlna
a	a	k8xC	a
zavádí	zavádět	k5eAaImIp3nS	zavádět
tzv.	tzv.	kA	tzv.
inklinační	inklinační	k2eAgInSc1d1	inklinační
faktor	faktor	k1gInSc1	faktor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
K	k	k7c3	k
(	(	kIx(	(
θ	θ	k?	θ
)	)	kIx)	)
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
1	[number]	k4	1
+	+	kIx~	+
cos	cos	kA	cos
:	:	kIx,	:
θ	θ	k?	θ
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
K	k	k7c3	k
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
theta	theta	k1gFnSc1	theta
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
cos	cos	kA	cos
\	\	kIx~	\
<g/>
theta	theta	k1gMnSc1	theta
)	)	kIx)	)
<g/>
}	}	kIx)	}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
θ	θ	k?	θ
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
theta	theto	k1gNnSc2	theto
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
odchylka	odchylka	k1gFnSc1	odchylka
od	od	k7c2	od
původního	původní	k2eAgInSc2d1	původní
směru	směr	k1gInSc2	směr
šíření	šíření	k1gNnSc2	šíření
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
bod	bod	k1gInSc1	bod
vlnoplochy	vlnoplocha	k1gFnSc2	vlnoplocha
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
existuje	existovat	k5eAaImIp3nS	existovat
vazba	vazba	k1gFnSc1	vazba
<g/>
,	,	kIx,	,
a	a	k8xC	a
do	do	k7c2	do
něhož	jenž	k3xRgNnSc2	jenž
v	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
okamžiku	okamžik	k1gInSc6	okamžik
dospělo	dochvít	k5eAaPmAgNnS	dochvít
postupné	postupný	k2eAgNnSc1d1	postupné
vlnění	vlnění	k1gNnSc1	vlnění
izotropního	izotropní	k2eAgNnSc2d1	izotropní
prostředí	prostředí	k1gNnSc2	prostředí
můžeme	moct	k5eAaImIp1nP	moct
pokládat	pokládat	k5eAaImF	pokládat
za	za	k7c4	za
zdroj	zdroj	k1gInSc4	zdroj
elementárního	elementární	k2eAgNnSc2d1	elementární
vlnění	vlnění	k1gNnSc2	vlnění
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
z	z	k7c2	z
něho	on	k3xPp3gNnSc2	on
šíří	šířit	k5eAaImIp3nP	šířit
v	v	k7c6	v
elementárních	elementární	k2eAgFnPc6d1	elementární
vlnoplochách	vlnoplocha	k1gFnPc6	vlnoplocha
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
vlnoplocha	vlnoplocha	k1gFnSc1	vlnoplocha
v	v	k7c6	v
dalším	další	k2eAgInSc6d1	další
časovém	časový	k2eAgInSc6d1	časový
okamžiku	okamžik	k1gInSc6	okamžik
je	být	k5eAaImIp3nS	být
vnější	vnější	k2eAgFnSc1d1	vnější
obálka	obálka	k1gFnSc1	obálka
všech	všecek	k3xTgFnPc2	všecek
elementárních	elementární	k2eAgFnPc2d1	elementární
vlnoploch	vlnoplocha	k1gFnPc2	vlnoplocha
a	a	k8xC	a
kolmice	kolmice	k1gFnSc2	kolmice
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
jednoznačně	jednoznačně	k6eAd1	jednoznačně
určuje	určovat	k5eAaImIp3nS	určovat
směr	směr	k1gInSc4	směr
šíření	šíření	k1gNnSc2	šíření
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
Huygensovu	Huygensův	k2eAgInSc3d1	Huygensův
principu	princip	k1gInSc3	princip
můžeme	moct	k5eAaImIp1nP	moct
zkonstruovat	zkonstruovat	k5eAaPmF	zkonstruovat
vlnoplochu	vlnoplocha	k1gFnSc4	vlnoplocha
v	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
okamžiku	okamžik	k1gInSc6	okamžik
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
známá	známý	k2eAgFnSc1d1	známá
její	její	k3xOp3gFnSc1	její
poloha	poloha	k1gFnSc1	poloha
a	a	k8xC	a
tvar	tvar	k1gInSc1	tvar
v	v	k7c6	v
některém	některý	k3yIgInSc6	některý
předcházejícím	předcházející	k2eAgInSc6d1	předcházející
okamžiku	okamžik	k1gInSc6	okamžik
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
také	také	k9	také
podle	podle	k7c2	podle
něj	on	k3xPp3gNnSc2	on
odvodit	odvodit	k5eAaPmF	odvodit
princip	princip	k1gInSc4	princip
odrazu	odraz	k1gInSc2	odraz
a	a	k8xC	a
lomu	lom	k1gInSc2	lom
vlnění	vlnění	k1gNnSc2	vlnění
<g/>
.	.	kIx.	.
</s>
<s>
Vlnění	vlnění	k1gNnSc1	vlnění
Vlnoplocha	vlnoplocha	k1gFnSc1	vlnoplocha
Christian	Christian	k1gMnSc1	Christian
Huygens	Huygens	k1gInSc1	Huygens
</s>
