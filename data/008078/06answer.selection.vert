<s>
Chmel	chmel	k1gInSc1	chmel
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
základních	základní	k2eAgFnPc2d1	základní
surovin	surovina	k1gFnPc2	surovina
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
piva	pivo	k1gNnSc2	pivo
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
mu	on	k3xPp3gMnSc3	on
dodává	dodávat	k5eAaImIp3nS	dodávat
charakteristickou	charakteristický	k2eAgFnSc4d1	charakteristická
nahořklou	nahořklý	k2eAgFnSc4d1	nahořklá
chuť	chuť	k1gFnSc4	chuť
pomocí	pomocí	k7c2	pomocí
chmelových	chmelový	k2eAgFnPc2d1	chmelová
pryskyřic	pryskyřice	k1gFnPc2	pryskyřice
a	a	k8xC	a
chmelové	chmelový	k2eAgNnSc1d1	chmelové
aroma	aroma	k1gNnSc1	aroma
vlivem	vlivem	k7c2	vlivem
silic	silice	k1gFnPc2	silice
<g/>
.	.	kIx.	.
</s>
