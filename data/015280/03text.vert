<s>
Su	Su	k?
Nuraxi	Nuraxe	k1gFnSc4
di	di	k?
Barumini	Barumin	k2eAgMnPc1d1
</s>
<s>
Su	Su	k?
Nuraxi	Nuraxe	k1gFnSc4
di	di	k?
BaruminiSvětové	BaruminiSvětové	k2eAgNnSc6d1
dědictví	dědictví	k1gNnSc6
UNESCO	UNESCO	kA
Archeologická	archeologický	k2eAgFnSc1d1
lokalita	lokalita	k1gFnSc1
u	u	k7c2
Barumini	Barumin	k2eAgMnPc1d1
v	v	k7c6
ziměSmluvní	ziměSmluvný	k2eAgMnPc5d1
stát	stát	k1gInSc1
</s>
<s>
Itálie	Itálie	k1gFnSc1
Itálie	Itálie	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
39	#num#	k4
<g/>
°	°	k?
<g/>
42	#num#	k4
<g/>
′	′	k?
<g/>
10	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
9	#num#	k4
<g/>
°	°	k?
<g/>
0	#num#	k4
<g/>
′	′	k?
<g/>
0	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Typ	typ	k1gInSc1
</s>
<s>
kulturní	kulturní	k2eAgNnSc4d1
dědictví	dědictví	k1gNnSc4
Kritérium	kritérium	k1gNnSc1
</s>
<s>
i	i	k9
<g/>
,	,	kIx,
iii	iii	k?
<g/>
,	,	kIx,
iv	iv	k?
Odkaz	odkaz	k1gInSc1
</s>
<s>
833	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Zařazení	zařazení	k1gNnSc1
do	do	k7c2
seznamu	seznam	k1gInSc2
Zařazení	zařazení	k1gNnSc1
</s>
<s>
1997	#num#	k4
(	(	kIx(
<g/>
21	#num#	k4
<g/>
.	.	kIx.
zasedání	zasedání	k1gNnSc6
<g/>
)	)	kIx)
</s>
<s>
Su	Su	k?
Nuraxi	Nuraxe	k1gFnSc4
di	di	k?
Barumini	Barumin	k2eAgMnPc1d1
je	on	k3xPp3gFnPc4
významná	významný	k2eAgFnSc1d1
archeologická	archeologický	k2eAgFnSc1d1
lokalita	lokalita	k1gFnSc1
poblíž	poblíž	k6eAd1
Barumini	Barumin	k2eAgMnPc1d1
v	v	k7c6
provincii	provincie	k1gFnSc6
Jižní	jižní	k2eAgFnSc2d1
Sardinie	Sardinie	k1gFnSc2
na	na	k7c6
ostrově	ostrov	k1gInSc6
Sardinii	Sardinie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Centrem	centrum	k1gNnSc7
zdejšího	zdejší	k2eAgInSc2d1
komplexu	komplex	k1gInSc2
ruin	ruina	k1gFnPc2
je	být	k5eAaImIp3nS
třípatrová	třípatrový	k2eAgFnSc1d1
stavba	stavba	k1gFnSc1
nuraghů	nuragh	k1gInPc2
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gInSc1
vznik	vznik	k1gInSc1
se	se	k3xPyFc4
datuje	datovat	k5eAaImIp3nS
do	do	k7c2
doby	doba	k1gFnSc2
kolem	kolem	k7c2
roku	rok	k1gInSc2
1500	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
</s>
<s>
Su	Su	k?
Nuraxi	Nuraxe	k1gFnSc4
di	di	k?
Barumini	Barumin	k2eAgMnPc1d1
jsou	být	k5eAaImIp3nP
od	od	k7c2
roku	rok	k1gInSc2
1997	#num#	k4
součástí	součást	k1gFnPc2
světového	světový	k2eAgNnSc2d1
dědictví	dědictví	k1gNnSc2
UNESCO	UNESCO	kA
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Su	Su	k?
Nuraxi	Nuraxe	k1gFnSc6
di	di	k?
Barumini	Barumin	k2eAgMnPc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Italské	italský	k2eAgFnSc2d1
památky	památka	k1gFnSc2
na	na	k7c6
seznamu	seznam	k1gInSc6
světového	světový	k2eAgNnSc2d1
dědictví	dědictví	k1gNnSc2
UNESCO	UNESCO	kA
</s>
<s>
Agrigento	Agrigento	k1gNnSc1
•	•	k?
Alberobello	Alberobello	k1gNnSc1
•	•	k?
Amalfinské	Amalfinský	k2eAgNnSc4d1
pobřeží	pobřeží	k1gNnSc4
•	•	k?
Aquileia	Aquileius	k1gMnSc2
•	•	k?
Assisi	Assise	k1gFnSc4
•	•	k?
Benátky	Benátky	k1gFnPc1
a	a	k8xC
jejich	jejich	k3xOp3gFnSc1
laguna	laguna	k1gFnSc1
•	•	k?
Benátské	benátský	k2eAgFnSc2d1
obranné	obranný	k2eAgFnSc2d1
stavby	stavba	k1gFnSc2
•	•	k?
Botanická	botanický	k2eAgFnSc1d1
zahrada	zahrada	k1gFnSc1
v	v	k7c6
Padově	Padova	k1gFnSc6
•	•	k?
Castel	Castela	k1gFnPc2
del	del	k?
Monte	Mont	k1gInSc5
•	•	k?
Cerveteri	Cerveter	k1gFnSc2
a	a	k8xC
Tarquinia	Tarquinium	k1gNnSc2
•	•	k?
Cilento	Cilento	k1gNnSc1
<g/>
,	,	kIx,
Vallo	Vallo	k1gNnSc1
di	di	k?
Diano	Diana	k1gFnSc5
<g/>
,	,	kIx,
Paestum	Paestum	k1gNnSc1
<g/>
,	,	kIx,
Velia	Velia	k1gFnSc1
a	a	k8xC
Certosa	Certosa	k1gFnSc1
di	di	k?
Padula	Padula	k1gFnSc1
•	•	k?
Crespi	Cresp	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
d	d	k?
<g/>
'	'	kIx"
<g/>
Adda	Add	k2eAgFnSc1d1
•	•	k?
Dolomity	Dolomity	k1gInPc4
•	•	k?
Etna	Etna	k1gFnSc1
•	•	k?
Ferrara	Ferrara	k1gFnSc1
•	•	k?
Florencie	Florencie	k1gFnSc2
•	•	k?
Hadriánova	Hadriánův	k2eAgFnSc1d1
vila	vila	k1gFnSc1
•	•	k?
Ivrea	Ivrea	k1gFnSc1
•	•	k?
Janov	Janov	k1gInSc1
<g/>
:	:	kIx,
Le	Le	k1gFnSc1
Strade	Strad	k1gInSc5
Nuove	Nuoev	k1gFnPc4
a	a	k8xC
systém	systém	k1gInSc4
Palazzi	Palazze	k1gFnSc4
dei	dei	k?
Rolli	Rolle	k1gFnSc3
•	•	k?
Kopce	kopec	k1gInSc2
prosecca	prosecca	k6eAd1
Conegliano	Conegliana	k1gFnSc5
a	a	k8xC
Valdobbiadene	Valdobbiaden	k1gInSc5
•	•	k?
královský	královský	k2eAgInSc1d1
palác	palác	k1gInSc1
Caserta	Caserta	k1gFnSc1
•	•	k?
Liparské	Liparský	k2eAgInPc4d1
ostrovy	ostrov	k1gInPc4
•	•	k?
Mantova	Mantova	k1gFnSc1
a	a	k8xC
Sabbioneta	Sabbionet	k2eAgFnSc1d1
•	•	k?
Matera	Matera	k1gFnSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
mocenská	mocenský	k2eAgFnSc1d1
střediska	středisko	k1gNnPc1
Langobardů	Langobard	k1gMnPc2
v	v	k7c6
Itálii	Itálie	k1gFnSc6
•	•	k?
katedrála	katedrála	k1gFnSc1
Nanebevzetí	nanebevzetí	k1gNnPc2
Panny	Panna	k1gFnSc2
Marie	Marie	k1gFnSc1
<g/>
,	,	kIx,
brána	brána	k1gFnSc1
Torre	torr	k1gInSc5
Civica	Civica	k1gFnSc1
a	a	k8xC
náměstí	náměstí	k1gNnSc1
Piazza	Piazz	k1gMnSc2
Grande	grand	k1gMnSc5
v	v	k7c6
Modeně	Modena	k1gFnSc6
•	•	k?
Monte	Mont	k1gMnSc5
San	San	k1gMnSc5
Giorgio	Giorgia	k1gMnSc5
•	•	k?
Neapol	Neapol	k1gFnSc1
•	•	k?
Palermo	Palermo	k1gNnSc1
a	a	k8xC
katedrály	katedrála	k1gFnPc1
v	v	k7c6
Cefalú	Cefalú	k1gFnSc6
a	a	k8xC
v	v	k7c6
Monreale	Monreala	k1gFnSc6
•	•	k?
Piazza	Piazza	k1gFnSc1
del	del	k?
Duomo	Duoma	k1gFnSc5
<g/>
,	,	kIx,
Pisa	Pisa	k1gFnSc1
•	•	k?
Pienza	Pienza	k1gFnSc1
•	•	k?
Pompeje	Pompeje	k1gInPc1
<g/>
,	,	kIx,
Herculaneum	Herculaneum	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
a	a	k8xC
Torre	torr	k1gInSc5
Annunziata	Annunziat	k2eAgFnSc1d1
•	•	k?
Portovenere	Portovener	k1gMnSc5
<g/>
,	,	kIx,
Cinque	Cinquus	k1gMnSc5
Terre	Terr	k1gMnSc5
a	a	k8xC
ostrovy	ostrov	k1gInPc1
Palmaria	Palmarium	k1gNnSc2
<g/>
,	,	kIx,
Tino	Tina	k1gFnSc5
a	a	k8xC
Tinetto	Tinett	k2eAgNnSc4d1
•	•	k?
prehistorická	prehistorický	k2eAgNnPc1d1
kůlová	kůlový	k2eAgNnPc1d1
obydlí	obydlí	k1gNnPc1
v	v	k7c6
Alpách	Alpy	k1gFnPc6
•	•	k?
Původní	původní	k2eAgInPc4d1
bukové	bukový	k2eAgInPc4d1
lesy	les	k1gInPc4
Karpat	Karpaty	k1gInPc2
a	a	k8xC
dalších	další	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
Evropy	Evropa	k1gFnSc2
•	•	k?
Ravenna	Ravenna	k1gFnSc1
•	•	k?
rezidence	rezidence	k1gFnSc2
královského	královský	k2eAgInSc2d1
rodu	rod	k1gInSc2
Savojských	savojský	k2eAgInPc2d1
•	•	k?
Rhétská	Rhétský	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
•	•	k?
Řím	Řím	k1gInSc1
•	•	k?
Sacri	Sacr	k1gFnSc2
Monti	Monť	k1gFnSc2
•	•	k?
San	San	k1gMnSc1
Gimignano	Gimignana	k1gFnSc5
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Santa	Santa	k1gFnSc1
Maria	Maria	k1gFnSc1
delle	delle	k1gFnSc1
Grazie	Grazie	k1gFnSc1
•	•	k?
Siena	Siena	k1gFnSc1
•	•	k?
skalní	skalní	k2eAgFnPc4d1
kresby	kresba	k1gFnPc4
ve	v	k7c4
Val	val	k1gInSc4
Camonice	Camonice	k1gFnSc2
•	•	k?
Su	Su	k?
Nuraxi	Nuraxe	k1gFnSc4
di	di	k?
Barumini	Barumin	k2eAgMnPc1d1
•	•	k?
Syrakusy	Syrakusy	k1gFnPc4
a	a	k8xC
Pantalica	Pantalica	k1gMnSc1
•	•	k?
Urbino	Urbino	k1gNnSc4
•	•	k?
Val	val	k1gInSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
Orcia	Orcia	k1gFnSc1
•	•	k?
Val	val	k1gInSc4
di	di	k?
Noto	nota	k1gFnSc5
•	•	k?
Verona	Verona	k1gFnSc1
•	•	k?
Vicenza	Vicenz	k1gMnSc2
a	a	k8xC
Palladiovy	Palladiovy	k?
vily	vila	k1gFnSc2
v	v	k7c6
Benátsku	Benátsko	k1gNnSc6
•	•	k?
Villa	Villa	k1gMnSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
Este	Est	k1gMnSc5
•	•	k?
Villa	Vill	k1gMnSc4
Romana	Roman	k1gMnSc4
del	del	k?
Casale	Casala	k1gFnSc6
•	•	k?
vily	vila	k1gFnSc2
a	a	k8xC
zahrady	zahrada	k1gFnSc2
Medicejských	Medicejský	k2eAgMnPc2d1
v	v	k7c6
Toskánsku	Toskánsko	k1gNnSc6
•	•	k?
vinařská	vinařský	k2eAgFnSc1d1
krajina	krajina	k1gFnSc1
Piemontu	Piemont	k1gInSc2
</s>
