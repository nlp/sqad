<p>
<s>
Kia	Kia	k?	Kia
Motors	Motors	k1gInSc1	Motors
Slovakia	Slovakia	k1gFnSc1	Slovakia
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
je	být	k5eAaImIp3nS	být
výrobní	výrobní	k2eAgInSc1d1	výrobní
závod	závod	k1gInSc1	závod
korejské	korejský	k2eAgFnSc2d1	Korejská
automobilové	automobilový	k2eAgFnSc2d1	automobilová
společnosti	společnost	k1gFnSc2	společnost
Kia	Kia	k1gFnPc2	Kia
Motors	Motors	k1gInSc1	Motors
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
u	u	k7c2	u
Žiliny	Žilina	k1gFnSc2	Žilina
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Teplička	tepličko	k1gNnSc2	tepličko
nad	nad	k7c4	nad
Váhom	Váhom	k1gInSc4	Váhom
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
první	první	k4xOgFnSc6	první
a	a	k8xC	a
zatím	zatím	k6eAd1	zatím
<g/>
[	[	kIx(	[
<g/>
kdy	kdy	k6eAd1	kdy
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
jediný	jediný	k2eAgInSc1d1	jediný
závod	závod	k1gInSc1	závod
této	tento	k3xDgFnSc2	tento
společnosti	společnost	k1gFnSc2	společnost
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Základní	základní	k2eAgInSc1d1	základní
kámen	kámen	k1gInSc1	kámen
byl	být	k5eAaImAgInS	být
položen	položit	k5eAaPmNgInS	položit
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
samotná	samotný	k2eAgFnSc1d1	samotná
výstavba	výstavba	k1gFnSc1	výstavba
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
ukončena	ukončit	k5eAaPmNgFnS	ukončit
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Zkušební	zkušební	k2eAgFnSc1d1	zkušební
výroba	výroba	k1gFnSc1	výroba
byla	být	k5eAaImAgFnS	být
spuštěna	spustit	k5eAaPmNgFnS	spustit
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
sériová	sériový	k2eAgFnSc1d1	sériová
výroba	výroba	k1gFnSc1	výroba
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Výrobní	výrobní	k2eAgFnSc1d1	výrobní
kapacita	kapacita	k1gFnSc1	kapacita
je	být	k5eAaImIp3nS	být
300	[number]	k4	300
tisíc	tisíc	k4xCgInPc2	tisíc
automobilů	automobil	k1gInPc2	automobil
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Investice	investice	k1gFnSc1	investice
představovala	představovat	k5eAaImAgFnS	představovat
1	[number]	k4	1
miliardu	miliarda	k4xCgFnSc4	miliarda
eur	euro	k1gNnPc2	euro
<g/>
.	.	kIx.	.
</s>
<s>
Rozloha	rozloha	k1gFnSc1	rozloha
závodu	závod	k1gInSc2	závod
je	být	k5eAaImIp3nS	být
166	[number]	k4	166
hektarů	hektar	k1gInPc2	hektar
<g/>
,	,	kIx,	,
v	v	k7c6	v
sousedství	sousedství	k1gNnSc6	sousedství
areálu	areál	k1gInSc2	areál
sídlí	sídlet	k5eAaImIp3nS	sídlet
dodavatel	dodavatel	k1gMnSc1	dodavatel
modulů	modul	k1gInPc2	modul
a	a	k8xC	a
systémů	systém	k1gInPc2	systém
<g/>
,	,	kIx,	,
firma	firma	k1gFnSc1	firma
Mobis	Mobis	k1gFnSc1	Mobis
Slovakia	Slovakia	k1gFnSc1	Slovakia
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
byla	být	k5eAaImAgFnS	být
zastavěná	zastavěný	k2eAgFnSc1d1	zastavěná
plocha	plocha	k1gFnSc1	plocha
56,8	[number]	k4	56,8
ha	ha	kA	ha
<g/>
,	,	kIx,	,
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
pracovalo	pracovat	k5eAaImAgNnS	pracovat
přibližně	přibližně	k6eAd1	přibližně
3	[number]	k4	3
800	[number]	k4	800
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
vyrobeno	vyrobit	k5eAaPmNgNnS	vyrobit
292	[number]	k4	292
050	[number]	k4	050
aut	auto	k1gNnPc2	auto
<g/>
.	.	kIx.	.
</s>
<s>
Vyráběny	vyráběn	k2eAgFnPc1d1	vyráběna
byly	být	k5eAaImAgFnP	být
4	[number]	k4	4
modely	model	k1gInPc7	model
automobilů	automobil	k1gInPc2	automobil
a	a	k8xC	a
4	[number]	k4	4
typy	typa	k1gFnSc2	typa
motorů	motor	k1gInPc2	motor
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
patřila	patřit	k5eAaImAgFnS	patřit
společnost	společnost	k1gFnSc1	společnost
Kia	Kia	k1gMnPc2	Kia
mezi	mezi	k7c4	mezi
tři	tři	k4xCgMnPc4	tři
největší	veliký	k2eAgMnPc4d3	veliký
plátce	plátce	k1gMnPc4	plátce
cla	clo	k1gNnSc2	clo
a	a	k8xC	a
DPH	DPH	kA	DPH
<g/>
.	.	kIx.	.
</s>
<s>
Výška	výška	k1gFnSc1	výška
daně	daň	k1gFnSc2	daň
odvedené	odvedený	k2eAgFnSc2d1	odvedená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
byla	být	k5eAaImAgFnS	být
83	[number]	k4	83
452	[number]	k4	452
000	[number]	k4	000
€	€	k?	€
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
firmu	firma	k1gFnSc4	firma
Kia	Kia	k1gFnSc3	Kia
Motors	Motorsa	k1gFnPc2	Motorsa
Slovakia	Slovakium	k1gNnSc2	Slovakium
řadí	řadit	k5eAaImIp3nS	řadit
na	na	k7c4	na
druhé	druhý	k4xOgNnSc4	druhý
místo	místo	k1gNnSc4	místo
za	za	k7c7	za
společností	společnost	k1gFnSc7	společnost
Eustream	Eustream	k1gInSc1	Eustream
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
</s>
</p>
<p>
<s>
Vyráběna	vyráběn	k2eAgFnSc1d1	vyráběna
modelová	modelový	k2eAgFnSc1d1	modelová
řada	řada	k1gFnSc1	řada
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
sestávala	sestávat	k5eAaImAgFnS	sestávat
z	z	k7c2	z
vozů	vůz	k1gInPc2	vůz
Sportage	Sportage	k1gNnSc2	Sportage
<g/>
,	,	kIx,	,
Ceed	Ceed	k1gMnSc1	Ceed
<g/>
,	,	kIx,	,
Cee	Cee	k1gMnSc1	Cee
<g/>
'	'	kIx"	'
<g/>
d	d	k?	d
sportswagon	sportswagon	k1gInSc1	sportswagon
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
cee	cee	k?	cee
<g/>
'	'	kIx"	'
<g/>
d	d	k?	d
a	a	k8xC	a
Venga	Venga	k1gFnSc1	Venga
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
vyráběn	vyrábět	k5eAaImNgInS	vyrábět
i	i	k9	i
Hyundai	Hyundai	k1gNnSc7	Hyundai
Tucson	Tucsona	k1gFnPc2	Tucsona
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
bylo	být	k5eAaImAgNnS	být
vyrobeno	vyrobit	k5eAaPmNgNnS	vyrobit
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
celkem	celkem	k6eAd1	celkem
335	[number]	k4	335
600	[number]	k4	600
vozidel	vozidlo	k1gNnPc2	vozidlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fotogalerie	Fotogalerie	k1gFnSc2	Fotogalerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Kia	Kia	k1gFnSc2	Kia
Motors	Motorsa	k1gFnPc2	Motorsa
Slovakia	Slovakium	k1gNnSc2	Slovakium
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Kia	Kia	k?	Kia
Motors	Motors	k1gInSc1	Motors
Slovakia	Slovakia	k1gFnSc1	Slovakia
</s>
</p>
<p>
<s>
http://www.mobis.sk/	[url]	k?	http://www.mobis.sk/
</s>
</p>
