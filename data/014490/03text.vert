<s>
Klaus	Klaus	k1gMnSc1
Lüdicke	Lüdick	k1gFnSc2
</s>
<s>
Klaus	Klaus	k1gMnSc1
Lüdicke	Lüdick	k1gMnSc2
Narození	narození	k1gNnSc2
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1943	#num#	k4
(	(	kIx(
<g/>
77	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Jelenia	Jelenium	k1gNnPc4
Góra	Gór	k2eAgNnPc4d1
Povolání	povolání	k1gNnSc4
</s>
<s>
teolog	teolog	k1gMnSc1
a	a	k8xC
vysokoškolský	vysokoškolský	k2eAgMnSc1d1
učitel	učitel	k1gMnSc1
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybit	k5eAaPmIp3nS,k5eAaImIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Klaus	Klaus	k1gMnSc1
Lüdicke	Lüdick	k1gFnSc2
(	(	kIx(
<g/>
*	*	kIx~
18	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1943	#num#	k4
Bad	Bad	k1gMnSc1
Warmbrunn	Warmbrunn	k1gMnSc1
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
městská	městský	k2eAgFnSc1d1
čtvrť	čtvrť	k1gFnSc1
Jelenie	Jelenie	k1gFnSc2
Góry	Góra	k1gFnSc2
v	v	k7c6
Polsku	Polsko	k1gNnSc6
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
německý	německý	k2eAgMnSc1d1
římskokatolický	římskokatolický	k2eAgMnSc1d1
teolog	teolog	k1gMnSc1
a	a	k8xC
kanonista	kanonista	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS
katolickou	katolický	k2eAgFnSc4d1
teologii	teologie	k1gFnSc4
a	a	k8xC
právní	právní	k2eAgFnPc4d1
vědy	věda	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1980	#num#	k4
až	až	k9
2008	#num#	k4
byl	být	k5eAaImAgMnS
profesorem	profesor	k1gMnSc7
Institutu	institut	k1gInSc2
kanonického	kanonický	k2eAgNnSc2d1
práva	právo	k1gNnSc2
na	na	k7c4
Westfälische	Westfälische	k1gNnSc4
Wilhelms-Universität	Wilhelms-Universitäta	k1gFnPc2
v	v	k7c6
Münsteru	Münster	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dílo	dílo	k1gNnSc1
(	(	kIx(
<g/>
výběr	výběr	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Psychisch	Psychisch	k1gInSc4
bedingte	bedingte	k5eAaPmIp2nP
Eheunfähigkeit	Eheunfähigkeit	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Begriffe	Begriff	k1gInSc5
–	–	k?
Abgrenzungen	Abgrenzungen	k1gInSc1
–	–	k?
Kriterien	Kriterien	k1gInSc1
(	(	kIx(
<g/>
=	=	kIx~
Europäische	Europäische	k1gInSc4
Hochschulschriften	Hochschulschriften	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reihe	Reih	k1gInSc2
23	#num#	k4
<g/>
:	:	kIx,
Theologie	theologie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bd	Bd	k1gFnSc1
<g/>
.	.	kIx.
105	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lang	Lang	k1gMnSc1
<g/>
,	,	kIx,
Frankfurt	Frankfurt	k1gInSc1
am	am	k?
Main	Main	k1gInSc1
u.	u.	k?
a.	a.	k?
1978	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
3-261-02494-1	3-261-02494-1	k4
(	(	kIx(
<g/>
Zugleich	Zugleich	k1gInSc1
<g/>
:	:	kIx,
München	München	k1gInSc1
<g/>
,	,	kIx,
Universität	Universität	k1gInSc1
<g/>
,	,	kIx,
Dissertation	Dissertation	k1gInSc1
<g/>
,	,	kIx,
1977	#num#	k4
<g/>
/	/	kIx~
<g/>
1978	#num#	k4
<g/>
:	:	kIx,
Psychisch	Psychischa	k1gFnPc2
bedingte	bedingte	k5eAaPmIp2nP
Unfähigkeit	Unfähigkeit	k2eAgInSc4d1
zu	zu	k?
Ehevertrag	Ehevertrag	k1gInSc4
und	und	k?
Eheführung	Eheführung	k1gInSc1
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Eherecht	Eherecht	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Codex	Codex	k1gInSc1
iuris	iuris	k1gFnSc2
canonici	canonice	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Canones	Canones	k1gInSc1
1055	#num#	k4
<g/>
–	–	k?
<g/>
1165	#num#	k4
(	(	kIx(
<g/>
=	=	kIx~
Kommentar	Kommentar	k1gInSc1
für	für	k?
Studium	studium	k1gNnSc1
und	und	k?
Praxis	Praxis	k1gFnSc1
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ludgerus-Verlag	Ludgerus-Verlag	k1gInSc1
<g/>
,	,	kIx,
Essen	Essen	k1gInSc1
1983	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
3	#num#	k4
<g/>
-	-	kIx~
<g/>
87497	#num#	k4
<g/>
-	-	kIx~
<g/>
165	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Familienplanung	Familienplanung	k1gInSc1
und	und	k?
Ehewille	Ehewille	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Der	drát	k5eAaImRp2nS
Ausschluß	Ausschluß	k1gMnSc1
der	drát	k5eAaImRp2nS
Nachkommenschaft	Nachkommenschaft	k1gInSc1
im	im	k?
nachkonziliaren	nachkonziliarna	k1gFnPc2
kanonischen	kanonischen	k2eAgInSc4d1
Eherecht	Eherecht	k1gInSc4
(	(	kIx(
<g/>
=	=	kIx~
Münsterische	Münsterische	k1gInSc1
Beiträge	Beiträg	k1gFnSc2
zur	zur	k?
Theologie	theologie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
H.	H.	kA
50	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aschendorff	Aschendorff	k1gInSc1
<g/>
,	,	kIx,
Münster	Münster	k1gInSc1
1983	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
3	#num#	k4
<g/>
-	-	kIx~
<g/>
402	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3955	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Der	drát	k5eAaImRp2nS
kirchliche	kirchliche	k1gFnSc1
Ehenichtigkeitsprozeß	Ehenichtigkeitsprozeß	k1gMnSc1
nach	nach	k1gInSc1
dem	dem	k?
Codex	Codex	k1gInSc1
Iuris	Iuris	k1gFnSc2
Canonici	Canonice	k1gFnSc3
von	von	k1gInSc1
1983	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Normen	Normen	k2eAgInSc1d1
und	und	k?
Kommentar	Kommentar	k1gInSc1
(	(	kIx(
<g/>
=	=	kIx~
Münsterischer	Münsterischra	k1gFnPc2
Kommentar	Kommentar	k1gInSc1
zum	zum	k?
Codex	Codex	k1gInSc1
iuris	iuris	k1gFnSc2
canonici	canonice	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Beiheft	Beiheft	k1gInSc1
<g/>
.	.	kIx.
10	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ludgerus-Verlag	Ludgerus-Verlag	k1gInSc1
<g/>
,	,	kIx,
Essen	Essen	k1gInSc1
1994	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
3	#num#	k4
<g/>
-	-	kIx~
<g/>
87497	#num#	k4
<g/>
-	-	kIx~
<g/>
199	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
„	„	k?
<g/>
Dignitas	Dignitas	k1gInSc4
connubii	connubie	k1gFnSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Die	Die	k1gMnSc1
Eheprozeßordnung	Eheprozeßordnung	k1gMnSc1
der	drát	k5eAaImRp2nS
katholischen	katholischen	k1gInSc4
Kirche	Kirch	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Text	text	k1gInSc1
und	und	k?
Kommentar	Kommentar	k1gInSc1
(	(	kIx(
<g/>
=	=	kIx~
Münsterischer	Münsterischra	k1gFnPc2
Kommentar	Kommentar	k1gInSc1
zum	zum	k?
Codex	Codex	k1gInSc1
iuris	iuris	k1gFnSc2
canonici	canonice	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Beiheft	Beiheft	k1gInSc1
<g/>
.	.	kIx.
42	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ludgerus-Verlag	Ludgerus-Verlag	k1gInSc1
<g/>
,	,	kIx,
Essen	Essen	k1gInSc1
2005	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
3	#num#	k4
<g/>
-	-	kIx~
<g/>
87497	#num#	k4
<g/>
-	-	kIx~
<g/>
252	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Klaus	Klaus	k1gMnSc1
Lüdicke	Lüdick	k1gFnSc2
na	na	k7c6
německé	německý	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kirchensite	Kirchensit	k1gInSc5
<g/>
:	:	kIx,
<g/>
Kirchenrechtler	Kirchenrechtler	k1gMnSc1
Professor	Professor	k1gMnSc1
Klaus	Klaus	k1gMnSc1
Lüdicke	Lüdick	k1gFnSc2
aus	aus	k?
Münster	Münster	k1gMnSc1
wird	wird	k1gMnSc1
emeritiert	emeritiert	k1gMnSc1
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Klaus	Klaus	k1gMnSc1
Lüdicke	Lüdick	k1gFnSc2
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
databázi	databáze	k1gFnSc6
Německé	německý	k2eAgFnSc2d1
národní	národní	k2eAgFnSc2d1
knihovny	knihovna	k1gFnSc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Klaus	Klaus	k1gMnSc1
Lüdicke	Lüdick	k1gFnSc2
</s>
<s>
Universität	Universität	k1gMnSc1
Münster	Münster	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Klaus	Klaus	k1gMnSc1
Lüdicke	Lüdick	k1gMnSc2
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Initiative	Initiativ	k1gInSc5
Kirche	Kirch	k1gMnSc2
von	von	k1gInSc1
unten	unten	k2eAgInSc1d1
<g/>
:	:	kIx,
Exkommunikation	Exkommunikation	k1gInSc1
von	von	k1gInSc1
Frauen	Frauen	k2eAgInSc1d1
aufgrund	aufgrund	k1gInSc1
der	drát	k5eAaImRp2nS
Priesterweihe	Priesterweihe	k1gNnSc4
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
mzk	mzk	k?
<g/>
2015884030	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
136326358	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2284	#num#	k4
0498	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
78046315	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
109270035	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
78046315	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Křesťanství	křesťanství	k1gNnSc1
</s>
