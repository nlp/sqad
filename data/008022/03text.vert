<s>
Trajekt	trajekt	k1gInSc1	trajekt
je	být	k5eAaImIp3nS	být
loď	loď	k1gFnSc4	loď
pro	pro	k7c4	pro
dopravu	doprava	k1gFnSc4	doprava
osobních	osobní	k2eAgInPc2d1	osobní
i	i	k8xC	i
nákladních	nákladní	k2eAgInPc2d1	nákladní
automobilů	automobil	k1gInPc2	automobil
a	a	k8xC	a
vlaků	vlak	k1gInPc2	vlak
s	s	k7c7	s
cestujícími	cestující	k1gFnPc7	cestující
i	i	k8xC	i
samostatných	samostatný	k2eAgFnPc2d1	samostatná
osob	osoba	k1gFnPc2	osoba
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
mořskými	mořský	k2eAgInPc7d1	mořský
břehy	břeh	k1gInPc7	břeh
na	na	k7c6	na
kratší	krátký	k2eAgFnSc6d2	kratší
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Principielně	principielně	k6eAd1	principielně
podobné	podobný	k2eAgNnSc4d1	podobné
plavidlo	plavidlo	k1gNnSc4	plavidlo
pro	pro	k7c4	pro
propojení	propojení	k1gNnSc4	propojení
říčních	říční	k2eAgInPc2d1	říční
břehů	břeh	k1gInPc2	břeh
nazýváme	nazývat	k5eAaImIp1nP	nazývat
přívoz	přívoz	k1gInSc4	přívoz
<g/>
.	.	kIx.	.
</s>
<s>
Trajekty	trajekt	k1gInPc1	trajekt
jsou	být	k5eAaImIp3nP	být
konstruovány	konstruovat	k5eAaImNgInP	konstruovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
dolního	dolní	k2eAgNnSc2d1	dolní
patra	patro	k1gNnSc2	patro
(	(	kIx(	(
<g/>
pater	patro	k1gNnPc2	patro
<g/>
)	)	kIx)	)
vjedou	vjet	k5eAaPmIp3nP	vjet
auta	auto	k1gNnPc1	auto
<g/>
,	,	kIx,	,
cestující	cestující	k1gMnSc1	cestující
si	se	k3xPyFc3	se
vystoupí	vystoupit	k5eAaPmIp3nS	vystoupit
a	a	k8xC	a
plavbu	plavba	k1gFnSc4	plavba
tráví	trávit	k5eAaImIp3nP	trávit
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
a	a	k8xC	a
v	v	k7c6	v
horních	horní	k2eAgNnPc6d1	horní
patrech	patro	k1gNnPc6	patro
lodi	loď	k1gFnSc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
pobyt	pobyt	k1gInSc4	pobyt
cestujících	cestující	k1gMnPc2	cestující
bývá	bývat	k5eAaImIp3nS	bývat
vybavena	vybavit	k5eAaPmNgFnS	vybavit
obchody	obchod	k1gInPc7	obchod
<g/>
,	,	kIx,	,
restauracemi	restaurace	k1gFnPc7	restaurace
<g/>
,	,	kIx,	,
kajutami	kajuta	k1gFnPc7	kajuta
<g/>
,	,	kIx,	,
vyhlídkovými	vyhlídkový	k2eAgFnPc7d1	vyhlídková
plošinami	plošina	k1gFnPc7	plošina
apod.	apod.	kA	apod.
Největší	veliký	k2eAgInPc1d3	veliký
trajekty	trajekt	k1gInPc1	trajekt
pojmou	pojmout	k5eAaPmIp3nP	pojmout
i	i	k8xC	i
stovky	stovka	k1gFnPc1	stovka
aut	auto	k1gNnPc2	auto
<g/>
,	,	kIx,	,
vč.	vč.	k?	vč.
kamionů	kamion	k1gInPc2	kamion
<g/>
,	,	kIx,	,
na	na	k7c6	na
delších	dlouhý	k2eAgFnPc6d2	delší
trasách	trasa	k1gFnPc6	trasa
se	se	k3xPyFc4	se
přepravují	přepravovat	k5eAaImIp3nP	přepravovat
i	i	k9	i
samostatné	samostatný	k2eAgInPc1d1	samostatný
návěsy	návěs	k1gInPc1	návěs
<g/>
.	.	kIx.	.
</s>
<s>
Nejrušnější	rušný	k2eAgFnSc1d3	nejrušnější
trajektová	trajektový	k2eAgFnSc1d1	trajektová
relace	relace	k1gFnSc1	relace
křižuje	křižovat	k5eAaImIp3nS	křižovat
Lamanšský	lamanšský	k2eAgInSc4d1	lamanšský
průliv	průliv	k1gInSc4	průliv
mezi	mezi	k7c7	mezi
Doverem	Dovero	k1gNnSc7	Dovero
a	a	k8xC	a
Calais	Calais	k1gNnSc7	Calais
<g/>
.	.	kIx.	.
</s>
<s>
Nejkratší	krátký	k2eAgFnPc1d3	nejkratší
frekventované	frekventovaný	k2eAgFnPc1d1	frekventovaná
trajektové	trajektový	k2eAgFnPc1d1	trajektová
relace	relace	k1gFnPc1	relace
jsou	být	k5eAaImIp3nP	být
postupně	postupně	k6eAd1	postupně
nahrazovány	nahrazovat	k5eAaImNgInP	nahrazovat
mosty	most	k1gInPc1	most
nebo	nebo	k8xC	nebo
podmořskými	podmořský	k2eAgInPc7d1	podmořský
tunely	tunel	k1gInPc7	tunel
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
trajektů	trajekt	k1gInPc2	trajekt
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
přepravujících	přepravující	k2eAgInPc2d1	přepravující
i	i	k8xC	i
vlaky	vlak	k1gInPc7	vlak
zaniklo	zaniknout	k5eAaPmAgNnS	zaniknout
s	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
letecké	letecký	k2eAgFnSc2d1	letecká
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
s	s	k7c7	s
trajektem	trajekt	k1gInSc7	trajekt
převážejícím	převážející	k2eAgInSc7d1	převážející
i	i	k8xC	i
osobní	osobní	k2eAgInPc1d1	osobní
vlaky	vlak	k1gInPc1	vlak
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
např.	např.	kA	např.
mezi	mezi	k7c7	mezi
Itálií	Itálie	k1gFnSc7	Itálie
a	a	k8xC	a
Sicílií	Sicílie	k1gFnSc7	Sicílie
nebo	nebo	k8xC	nebo
Německem	Německo	k1gNnSc7	Německo
a	a	k8xC	a
Dánskem	Dánsko	k1gNnSc7	Dánsko
<g/>
.	.	kIx.	.
</s>
<s>
Baltské	baltský	k2eAgNnSc1d1	Baltské
moře	moře	k1gNnSc1	moře
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
spojů	spoj	k1gInPc2	spoj
na	na	k7c4	na
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Vasa	Vasa	k1gMnSc1	Vasa
-	-	kIx~	-
Umeå	Umeå	k1gMnSc1	Umeå
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
RG	RG	kA	RG
Line	linout	k5eAaImIp3nS	linout
(	(	kIx(	(
<g/>
http://www.rgline.com/	[url]	k?	http://www.rgline.com/
<g/>
)	)	kIx)	)
Turku	turek	k1gInSc6	turek
-	-	kIx~	-
Marienham	Marienham	k1gInSc1	Marienham
-	-	kIx~	-
Stockholm	Stockholm	k1gInSc1	Stockholm
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
Tallink	Tallink	k1gInSc4	Tallink
Silja	Silj	k1gInSc2	Silj
a	a	k8xC	a
Viking	Viking	k1gMnSc1	Viking
Line	linout	k5eAaImIp3nS	linout
(	(	kIx(	(
<g/>
http://www.tallinksilja.com/en	[url]	k1gInSc1	http://www.tallinksilja.com/en
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
http://www.vikingline.fi	[url]	k6eAd1	http://www.vikingline.fi
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Turku	Turek	k1gMnSc3	Turek
-	-	kIx~	-
Kapelskär	Kapelskär	k1gMnSc1	Kapelskär
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
Tallink	Tallink	k1gInSc4	Tallink
Silja	Silj	k1gInSc2	Silj
<g/>
(	(	kIx(	(
<g/>
http://www.tallinksilja.com/en	[url]	k1gInSc1	http://www.tallinksilja.com/en
<g/>
)	)	kIx)	)
Turku	Turek	k1gMnSc6	Turek
-	-	kIx~	-
Lå	Lå	k1gFnSc2	Lå
-	-	kIx~	-
Stockholm	Stockholm	k1gInSc1	Stockholm
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
Viking	Viking	k1gMnSc1	Viking
Line	linout	k5eAaImIp3nS	linout
(	(	kIx(	(
<g/>
http://www.vikingline.fi	[url]	k1gMnPc1	http://www.vikingline.fi
<g/>
)	)	kIx)	)
Naantali	Naantali	k1gMnSc1	Naantali
-	-	kIx~	-
Kapelskär	Kapelskär	k1gMnSc1	Kapelskär
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
Finn	Finn	k1gMnSc1	Finn
Link	Link	k1gMnSc1	Link
(	(	kIx(	(
<g/>
http://www.finnlink.fi	[url]	k1gNnSc1	http://www.finnlink.fi
<g/>
)	)	kIx)	)
Marienham	Marienham	k1gInSc1	Marienham
-	-	kIx~	-
Kapelskär	Kapelskär	k1gMnSc1	Kapelskär
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
Viking	Viking	k1gMnSc1	Viking
Line	linout	k5eAaImIp3nS	linout
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
http://www.vikingline.fi	[url]	k1gMnPc5	http://www.vikingline.fi
<g/>
)	)	kIx)	)
Helsinki	Helsinki	k1gNnSc4	Helsinki
-	-	kIx~	-
Marienham	Marienham	k1gInSc1	Marienham
-	-	kIx~	-
Stockholm	Stockholm	k1gInSc1	Stockholm
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
Tallink	Tallink	k1gInSc4	Tallink
Silja	Silj	k1gInSc2	Silj
a	a	k8xC	a
Viking	Viking	k1gMnSc1	Viking
Line	linout	k5eAaImIp3nS	linout
(	(	kIx(	(
<g/>
http://www.tallinksilja.com/en	[url]	k1gInSc1	http://www.tallinksilja.com/en
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
http://www.vikingline.fi	[url]	k1gMnPc5	http://www.vikingline.fi
<g/>
)	)	kIx)	)
Helsinki	Helsinki	k1gNnSc4	Helsinki
-	-	kIx~	-
Tallinn	Tallinn	k1gMnSc1	Tallinn
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
Tallink	Tallink	k1gInSc4	Tallink
Silja	Siljum	k1gNnSc2	Siljum
<g/>
,	,	kIx,	,
Viking	Viking	k1gMnSc1	Viking
Line	linout	k5eAaImIp3nS	linout
a	a	k8xC	a
ECKERÖ	ECKERÖ	kA	ECKERÖ
LINE	linout	k5eAaImIp3nS	linout
(	(	kIx(	(
<g/>
http://www.tallinksilja.com/en	[url]	k1gInSc1	http://www.tallinksilja.com/en
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
http://www.vikingline.fi	[url]	k6eAd1	http://www.vikingline.fi
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
http://www.eckeroline.fi/en/	[url]	k?	http://www.eckeroline.fi/en/
<g/>
)	)	kIx)	)
Helsinki	Helsink	k1gFnSc2	Helsink
-	-	kIx~	-
Rostock	Rostock	k1gMnSc1	Rostock
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
Tallink	Tallink	k1gInSc4	Tallink
Silja	Silj	k1gInSc2	Silj
<g/>
(	(	kIx(	(
<g/>
http://www.tallinksilja.com/en	[url]	k1gInSc1	http://www.tallinksilja.com/en
<g/>
)	)	kIx)	)
Tallinn	Tallinn	k1gInSc1	Tallinn
-	-	kIx~	-
Marienham	Marienham	k1gInSc1	Marienham
-	-	kIx~	-
Stockholm	Stockholm	k1gInSc1	Stockholm
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
Tallink	Tallink	k1gInSc4	Tallink
Silja	Silj	k1gInSc2	Silj
<g/>
(	(	kIx(	(
<g/>
http://www.tallinksilja.com/en	[url]	k1gInSc1	http://www.tallinksilja.com/en
<g/>
)	)	kIx)	)
Ventspils	Ventspils	k1gInSc1	Ventspils
-	-	kIx~	-
Nynäshamn	Nynäshamn	k1gMnSc1	Nynäshamn
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
<g />
.	.	kIx.	.
</s>
<s>
Scandlines	Scandlines	k1gInSc1	Scandlines
(	(	kIx(	(
<g/>
http://www.scandlines.dk	[url]	k1gInSc1	http://www.scandlines.dk
<g/>
)	)	kIx)	)
Ventspils	Ventspils	k1gInSc1	Ventspils
-	-	kIx~	-
Rostock	Rostock	k1gMnSc1	Rostock
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
Scandlines	Scandlines	k1gMnSc1	Scandlines
(	(	kIx(	(
<g/>
http://www.scandlines.dk	[url]	k1gInSc1	http://www.scandlines.dk
<g/>
)	)	kIx)	)
Paldiski	Paldiski	k1gNnSc1	Paldiski
-	-	kIx~	-
Kapelskär	Kapelskär	k1gMnSc1	Kapelskär
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
Tallink	Tallink	k1gInSc4	Tallink
Silja	Silj	k1gInSc2	Silj
<g/>
(	(	kIx(	(
<g/>
http://www.tallinksilja.com/en	[url]	k1gInSc1	http://www.tallinksilja.com/en
<g/>
)	)	kIx)	)
Riga	Riga	k1gFnSc1	Riga
-	-	kIx~	-
Stockholm	Stockholm	k1gInSc1	Stockholm
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
Tallink	Tallink	k1gInSc4	Tallink
Silja	Silj	k1gInSc2	Silj
<g/>
(	(	kIx(	(
<g/>
http://www.tallinksilja.com/en	[url]	k1gInSc1	http://www.tallinksilja.com/en
<g/>
)	)	kIx)	)
Gdynia	Gdynium	k1gNnSc2	Gdynium
-	-	kIx~	-
Karlskrona	Karlskrona	k1gFnSc1	Karlskrona
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
dopravce	dopravce	k1gMnSc1	dopravce
Stena	Sten	k1gInSc2	Sten
Line	linout	k5eAaImIp3nS	linout
(	(	kIx(	(
<g/>
http://www.stenaline.com/stena-line/	[url]	k?	http://www.stenaline.com/stena-line/
<g/>
)	)	kIx)	)
Gdańsk	Gdańsk	k1gInSc1	Gdańsk
-	-	kIx~	-
Nynäshamn	Nynäshamn	k1gMnSc1	Nynäshamn
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
Polferries	Polferries	k1gMnSc1	Polferries
(	(	kIx(	(
<g/>
http://www.polferries.pl/en/home	[url]	k1gInSc5	http://www.polferries.pl/en/home
<g/>
)	)	kIx)	)
Świnoujście	Świnoujście	k1gFnSc2	Świnoujście
-	-	kIx~	-
Rø	Rø	k1gMnSc1	Rø
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
Polferries	Polferries	k1gMnSc1	Polferries
(	(	kIx(	(
<g/>
http://www.polferries.pl/en/home	[url]	k1gInSc5	http://www.polferries.pl/en/home
<g/>
)	)	kIx)	)
Świnoujście	Świnoujście	k1gFnSc1	Świnoujście
-	-	kIx~	-
Ystad	Ystad	k1gInSc1	Ystad
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
Polferries	Polferries	k1gMnSc1	Polferries
a	a	k8xC	a
UNITY	unita	k1gMnSc2	unita
LINE	linout	k5eAaImIp3nS	linout
(	(	kIx(	(
<g/>
http://www.polferries.pl/en/home	[url]	k1gInSc5	http://www.polferries.pl/en/home
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
http://www.unityline.pl/	[url]	k?	http://www.unityline.pl/
<g/>
)	)	kIx)	)
Świnoujście	Świnoujście	k1gFnSc1	Świnoujście
-	-	kIx~	-
Kodaň	Kodaň	k1gFnSc1	Kodaň
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
Polferries	Polferries	k1gMnSc1	Polferries
(	(	kIx(	(
<g/>
http://www.polferries.pl/en/home	[url]	k1gInSc5	http://www.polferries.pl/en/home
<g/>
)	)	kIx)	)
Sassnitz	Sassnitz	k1gMnSc1	Sassnitz
-	-	kIx~	-
Trelleborg	Trelleborg	k1gMnSc1	Trelleborg
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
Scandlines	Scandlines	k1gMnSc1	Scandlines
(	(	kIx(	(
<g/>
http://www.scandlines.dk	[url]	k1gMnSc1	http://www.scandlines.dk
<g/>
)	)	kIx)	)
Rostock	Rostock	k1gMnSc1	Rostock
-	-	kIx~	-
Trelleborg	Trelleborg	k1gMnSc1	Trelleborg
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
Scandlines	Scandlines	k1gMnSc1	Scandlines
(	(	kIx(	(
<g/>
http://www.scandlines.dk	[url]	k1gMnSc1	http://www.scandlines.dk
<g/>
)	)	kIx)	)
Rostock	Rostock	k1gMnSc1	Rostock
-	-	kIx~	-
Gedser	Gedser	k1gMnSc1	Gedser
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
Scandlines	Scandlines	k1gMnSc1	Scandlines
(	(	kIx(	(
<g/>
http://www.scandlines.dk	[url]	k1gMnSc1	http://www.scandlines.dk
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Travemünde	Travemünd	k1gInSc5	Travemünd
-	-	kIx~	-
Trelleborg	Trelleborg	k1gMnSc1	Trelleborg
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
TT-Link	TT-Link	k1gInSc1	TT-Link
(	(	kIx(	(
<g/>
http://www.ttline.com/	[url]	k?	http://www.ttline.com/
<g/>
)	)	kIx)	)
Travemünde	Travemünd	k1gMnSc5	Travemünd
-	-	kIx~	-
Malmö	Malmö	k1gMnSc6	Malmö
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
Nordö	Nordö	k1gMnSc1	Nordö
Link	Link	k1gMnSc1	Link
(	(	kIx(	(
<g/>
http://www.nordoe-link.com/	[url]	k?	http://www.nordoe-link.com/
<g/>
)	)	kIx)	)
Kiel	Kiel	k1gInSc1	Kiel
-	-	kIx~	-
Göteborg	Göteborg	k1gMnSc1	Göteborg
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
Stena	Steno	k1gNnSc2	Steno
Line	linout	k5eAaImIp3nS	linout
(	(	kIx(	(
<g/>
http://www.stenaline.com/stena-line/	[url]	k?	http://www.stenaline.com/stena-line/
<g/>
)	)	kIx)	)
Kiel	Kiel	k1gInSc1	Kiel
-	-	kIx~	-
Oslo	Oslo	k1gNnSc1	Oslo
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
Color	Color	k1gMnSc1	Color
Line	linout	k5eAaImIp3nS	linout
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
http://www.colorline.com/	[url]	k?	http://www.colorline.com/
<g/>
)	)	kIx)	)
Klajpeda	Klajpeda	k1gMnSc1	Klajpeda
-	-	kIx~	-
Kiel	Kiel	k1gInSc1	Kiel
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
DFDS	DFDS	kA	DFDS
LISCO	LISCO	kA	LISCO
(	(	kIx(	(
<g/>
http://www.dfdslisco.lt/en/on-board/	[url]	k?	http://www.dfdslisco.lt/en/on-board/
<g/>
)	)	kIx)	)
Klajpeda	Klajpeda	k1gMnSc1	Klajpeda
-	-	kIx~	-
Karlshamn	Karlshamn	k1gInSc1	Karlshamn
(	(	kIx(	(
<g/>
Švédsko	Švédsko	k1gNnSc1	Švédsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
DFDS	DFDS	kA	DFDS
LISCO	LISCO	kA	LISCO
(	(	kIx(	(
<g/>
http://www.dfdslisco.lt/en/on-board/	[url]	k?	http://www.dfdslisco.lt/en/on-board/
<g/>
)	)	kIx)	)
Klajpeda	Klajpeda	k1gMnSc1	Klajpeda
-	-	kIx~	-
Sassnitz	Sassnitz	k1gMnSc1	Sassnitz
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
DFDS	DFDS	kA	DFDS
LISCO	LISCO	kA	LISCO
(	(	kIx(	(
<g/>
http://www.dfdslisco.lt/en/on-board/	[url]	k?	http://www.dfdslisco.lt/en/on-board/
<g/>
)	)	kIx)	)
Puttgarten	Puttgarten	k2eAgMnSc1d1	Puttgarten
-	-	kIx~	-
Rodby	Rodb	k1gMnPc4	Rodb
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
Scandlines	Scandlines	k1gMnSc1	Scandlines
(	(	kIx(	(
<g/>
http://www.scandlines.dk	[url]	k1gMnSc1	http://www.scandlines.dk
<g/>
)	)	kIx)	)
Helsingø	Helsingø	k1gMnSc1	Helsingø
-	-	kIx~	-
Helsingborg	Helsingborg	k1gMnSc1	Helsingborg
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
Scandlines	Scandlines	k1gMnSc1	Scandlines
(	(	kIx(	(
<g/>
http://www.scandlines.dk	[url]	k1gMnSc1	http://www.scandlines.dk
<g/>
)	)	kIx)	)
Kodaň	Kodaň	k1gFnSc1	Kodaň
-	-	kIx~	-
Oslo	Oslo	k1gNnSc1	Oslo
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
DFDS	DFDS	kA	DFDS
SEAWAYS	SEAWAYS	kA	SEAWAYS
(	(	kIx(	(
<g/>
http://www.dfdsseaways.co.uk/	[url]	k?	http://www.dfdsseaways.co.uk/
<g/>
)	)	kIx)	)
Grenaa	Grenaa	k1gMnSc1	Grenaa
-	-	kIx~	-
Varberg	Varberg	k1gMnSc1	Varberg
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
Stena	Steno	k1gNnSc2	Steno
Line	linout	k5eAaImIp3nS	linout
(	(	kIx(	(
<g/>
http://www.stenaline.com/stena-line/	[url]	k?	http://www.stenaline.com/stena-line/
<g/>
)	)	kIx)	)
Frederikshavn	Frederikshavn	k1gMnSc1	Frederikshavn
-	-	kIx~	-
Göteborg	Göteborg	k1gMnSc1	Göteborg
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc2	dopravce
Stena	Sten	k1gMnSc2	Sten
Line	linout	k5eAaImIp3nS	linout
(	(	kIx(	(
<g/>
http://www.stenaline.com/stena-line/	[url]	k?	http://www.stenaline.com/stena-line/
<g/>
)	)	kIx)	)
Frederikshavn	Frederikshavn	k1gNnSc1	Frederikshavn
-	-	kIx~	-
Oslo	Oslo	k1gNnSc1	Oslo
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
Stena	Steno	k1gNnSc2	Steno
Line	linout	k5eAaImIp3nS	linout
(	(	kIx(	(
<g/>
http://www.stenaline.com/stena-line/	[url]	k?	http://www.stenaline.com/stena-line/
<g/>
)	)	kIx)	)
Langesund	Langesund	k1gMnSc1	Langesund
-	-	kIx~	-
Strø	Strø	k1gMnSc1	Strø
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
kystlink	kystlink	k1gInSc1	kystlink
(	(	kIx(	(
<g/>
http://www.kystlink.com/	[url]	k?	http://www.kystlink.com/
<g/>
)	)	kIx)	)
Sandefjord	Sandefjord	k1gMnSc1	Sandefjord
-	-	kIx~	-
Strø	Strø	k1gMnSc1	Strø
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
Color	Color	k1gMnSc1	Color
Line	linout	k5eAaImIp3nS	linout
(	(	kIx(	(
<g/>
http://www.colorline.com/	[url]	k?	http://www.colorline.com/
<g/>
)	)	kIx)	)
Hanstholm	Hanstholm	k1gMnSc1	Hanstholm
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
Kristiansand	Kristiansand	k1gInSc1	Kristiansand
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
Master	master	k1gMnSc1	master
Ferries	Ferries	k1gMnSc1	Ferries
(	(	kIx(	(
<g/>
http://www.masterferries.com/	[url]	k?	http://www.masterferries.com/
<g/>
)	)	kIx)	)
Hirsthals	Hirsthals	k1gInSc1	Hirsthals
-	-	kIx~	-
Oslo	Oslo	k1gNnSc1	Oslo
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
Color	Color	k1gMnSc1	Color
Line	linout	k5eAaImIp3nS	linout
(	(	kIx(	(
<g/>
http://www.colorline.com/	[url]	k?	http://www.colorline.com/
<g/>
)	)	kIx)	)
Hirsthals	Hirsthals	k1gInSc1	Hirsthals
-	-	kIx~	-
Larvik	Larvik	k1gMnSc1	Larvik
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
Color	Color	k1gMnSc1	Color
Line	linout	k5eAaImIp3nS	linout
(	(	kIx(	(
<g/>
http://www.colorline.com/	[url]	k?	http://www.colorline.com/
<g/>
)	)	kIx)	)
Hirsthals	Hirsthals	k1gInSc1	Hirsthals
-	-	kIx~	-
Kristiansand	Kristiansand	k1gInSc1	Kristiansand
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
Color	Color	k1gMnSc1	Color
Line	linout	k5eAaImIp3nS	linout
(	(	kIx(	(
<g/>
http://www.colorline.com/	[url]	k?	http://www.colorline.com/
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Anglie	Anglie	k1gFnSc1	Anglie
-	-	kIx~	-
Irsko	Irsko	k1gNnSc1	Irsko
Scrabster	Scrabstra	k1gFnPc2	Scrabstra
-	-	kIx~	-
Stromness	Stromness	k1gInSc1	Stromness
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
North	North	k1gMnSc1	North
Link	Link	k1gMnSc1	Link
(	(	kIx(	(
<g/>
http://www.northlinkferries.com/Timetables.html	[url]	k1gMnSc1	http://www.northlinkferries.com/Timetables.html
<g/>
)	)	kIx)	)
Aberdeen	Aberdeen	k2eAgMnSc1d1	Aberdeen
-	-	kIx~	-
Kirkwall	Kirkwall	k1gMnSc1	Kirkwall
-	-	kIx~	-
Lerwick	Lerwick	k1gMnSc1	Lerwick
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
North	North	k1gMnSc1	North
Link	Link	k1gMnSc1	Link
(	(	kIx(	(
<g/>
http://www.northlinkferries.com/Timetables.html	[url]	k1gInSc1	http://www.northlinkferries.com/Timetables.html
<g/>
)	)	kIx)	)
Troon	Troon	k1gNnSc1	Troon
-	-	kIx~	-
Larne	Larn	k1gMnSc5	Larn
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
P	P	kA	P
<g/>
&	&	k?	&
<g/>
O	o	k7c4	o
Ferries	Ferries	k1gInSc4	Ferries
(	(	kIx(	(
<g/>
http://www.poferries.com/tourist/	[url]	k?	http://www.poferries.com/tourist/
<g/>
)	)	kIx)	)
Stranair	Stranair	k1gMnSc1	Stranair
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
Belfast	Belfast	k1gInSc1	Belfast
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
Stena	Steno	k1gNnSc2	Steno
Line	linout	k5eAaImIp3nS	linout
(	(	kIx(	(
<g/>
http://www.stenaline.com/stena-line/	[url]	k?	http://www.stenaline.com/stena-line/
<g/>
)	)	kIx)	)
Fleetwood-Larne	Fleetwood-Larn	k1gInSc5	Fleetwood-Larn
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc2	dopravce
Stena	Sten	k1gMnSc2	Sten
Line	linout	k5eAaImIp3nS	linout
(	(	kIx(	(
<g/>
http://www.stenaline.com/stena-line/	[url]	k?	http://www.stenaline.com/stena-line/
<g/>
)	)	kIx)	)
Cairnryan	Cairnryan	k1gMnSc1	Cairnryan
–	–	k?	–
L	L	kA	L
arne	arne	k1gInSc1	arne
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
P	P	kA	P
<g/>
&	&	k?	&
<g/>
O	o	k7c4	o
Ferries	Ferries	k1gInSc4	Ferries
(	(	kIx(	(
<g/>
http://www.poferries.com/tourist/	[url]	k?	http://www.poferries.com/tourist/
<g/>
)	)	kIx)	)
Liverpoo	Liverpoo	k1gMnSc1	Liverpoo
-	-	kIx~	-
Belfast	Belfast	k1gInSc1	Belfast
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
norfolkline	norfolklin	k1gInSc5	norfolklin
(	(	kIx(	(
<g/>
http://www.norfolkline.com/ferry/	[url]	k?	http://www.norfolkline.com/ferry/
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Liverpool	Liverpool	k1gInSc1	Liverpool
-	-	kIx~	-
Dublin	Dublin	k1gInSc1	Dublin
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
P	P	kA	P
<g/>
&	&	k?	&
<g/>
O	o	k7c4	o
Ferries	Ferries	k1gInSc4	Ferries
a	a	k8xC	a
norfolkline	norfolklin	k1gInSc5	norfolklin
(	(	kIx(	(
<g/>
http://www.poferries.com/tourist/	[url]	k?	http://www.poferries.com/tourist/
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
http://www.norfolkline.com/ferry/	[url]	k?	http://www.norfolkline.com/ferry/
<g/>
)	)	kIx)	)
Holyhead	Holyhead	k1gInSc1	Holyhead
-	-	kIx~	-
Dublin	Dublin	k1gInSc1	Dublin
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
irishferries	irishferries	k1gMnSc1	irishferries
a	a	k8xC	a
Stena	Stena	k1gFnSc1	Stena
Line	linout	k5eAaImIp3nS	linout
(	(	kIx(	(
<g/>
http://www.irishferries.com/	[url]	k?	http://www.irishferries.com/
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
http://www.stenaline.com/stena-line/	[url]	k?	http://www.stenaline.com/stena-line/
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Holyhead	Holyhead	k6eAd1	Holyhead
-	-	kIx~	-
Dun	duna	k1gFnPc2	duna
Laoghaire	Laoghair	k1gInSc5	Laoghair
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc2	dopravce
Stena	Sten	k1gMnSc2	Sten
Line	linout	k5eAaImIp3nS	linout
(	(	kIx(	(
<g/>
http://www.stenaline.com/stena-line/	[url]	k?	http://www.stenaline.com/stena-line/
<g/>
)	)	kIx)	)
Pembroke	Pembroke	k1gNnSc1	Pembroke
-	-	kIx~	-
Rosslare	Rosslar	k1gMnSc5	Rosslar
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
irishferries	irishferries	k1gMnSc1	irishferries
(	(	kIx(	(
<g/>
http://www.irishferries.com/	[url]	k?	http://www.irishferries.com/
<g/>
)	)	kIx)	)
Fishguard	Fishguard	k1gInSc4	Fishguard
-	-	kIx~	-
Rosslare	Rosslar	k1gMnSc5	Rosslar
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc2	dopravce
Stena	Sten	k1gMnSc2	Sten
Line	linout	k5eAaImIp3nS	linout
(	(	kIx(	(
<g/>
http://www.stenaline.com/stena-line/	[url]	k?	http://www.stenaline.com/stena-line/
<g/>
)	)	kIx)	)
Irsko	Irsko	k1gNnSc1	Irsko
-	-	kIx~	-
Evropa	Evropa	k1gFnSc1	Evropa
Rosslare	Rosslar	k1gMnSc5	Rosslar
-	-	kIx~	-
Cherbourg	Cherbourg	k1gMnSc1	Cherbourg
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
irishferries	irishferries	k1gMnSc1	irishferries
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
http://www.irishferries.com/	[url]	k?	http://www.irishferries.com/
<g/>
)	)	kIx)	)
Rosslare	Rosslar	k1gMnSc5	Rosslar
-	-	kIx~	-
Roscoff	Roscoff	k1gMnSc1	Roscoff
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
irishferries	irishferries	k1gMnSc1	irishferries
(	(	kIx(	(
<g/>
http://www.irishferries.com/	[url]	k?	http://www.irishferries.com/
<g/>
)	)	kIx)	)
Cork	Cork	k1gMnSc1	Cork
-	-	kIx~	-
Roscoff	Roscoff	k1gMnSc1	Roscoff
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
Brittaby	Brittaba	k1gFnSc2	Brittaba
Ferries	Ferries	k1gMnSc1	Ferries
(	(	kIx(	(
<g/>
http://www.brittany-ferries.co.co.uk/	[url]	k?	http://www.brittany-ferries.co.co.uk/
<g/>
)	)	kIx)	)
Anglie	Anglie	k1gFnSc1	Anglie
–	–	k?	–
Evropa	Evropa	k1gFnSc1	Evropa
Newcastle	Newcastle	k1gMnSc1	Newcastle
-	-	kIx~	-
Stavanger	Stavanger	k1gMnSc1	Stavanger
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
DFDS	DFDS	kA	DFDS
SEAWAYS	SEAWAYS	kA	SEAWAYS
(	(	kIx(	(
<g/>
http://www.dfdsseaways.co.uk/	[url]	k?	http://www.dfdsseaways.co.uk/
<g/>
)	)	kIx)	)
Newcastle	Newcastle	k1gMnSc1	Newcastle
-	-	kIx~	-
Amsterdam	Amsterdam	k1gInSc1	Amsterdam
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
dopravce	dopravce	k1gMnSc1	dopravce
DFDS	DFDS	kA	DFDS
SEAWAYS	SEAWAYS	kA	SEAWAYS
(	(	kIx(	(
<g/>
http://www.dfdsseaways.co.uk/	[url]	k?	http://www.dfdsseaways.co.uk/
<g/>
)	)	kIx)	)
Hull	Hull	k1gMnSc1	Hull
-	-	kIx~	-
Amsterdam	Amsterdam	k1gInSc1	Amsterdam
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
P	P	kA	P
<g/>
&	&	k?	&
<g/>
O	o	k7c4	o
Ferries	Ferries	k1gInSc4	Ferries
(	(	kIx(	(
<g/>
http://www.poferries.com/tourist/	[url]	k?	http://www.poferries.com/tourist/
<g/>
)	)	kIx)	)
Hull	Hull	k1gMnSc1	Hull
-	-	kIx~	-
Zeebrugge	Zeebrugge	k1gInSc1	Zeebrugge
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
P	P	kA	P
<g/>
&	&	k?	&
<g/>
O	o	k7c4	o
Ferries	Ferries	k1gInSc4	Ferries
(	(	kIx(	(
<g/>
http://www.poferries.com/tourist/	[url]	k?	http://www.poferries.com/tourist/
<g/>
)	)	kIx)	)
Harwich	Harwich	k1gMnSc1	Harwich
-	-	kIx~	-
Esbjerg	Esbjerg	k1gMnSc1	Esbjerg
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
DFDS	DFDS	kA	DFDS
SEAWAYS	SEAWAYS	kA	SEAWAYS
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
http://www.dfdsseaways.co.uk/	[url]	k?	http://www.dfdsseaways.co.uk/
<g/>
)	)	kIx)	)
Harwich	Harwich	k1gMnSc1	Harwich
-	-	kIx~	-
Hoek	Hoek	k1gMnSc1	Hoek
van	vana	k1gFnPc2	vana
Holland	Holland	k1gInSc1	Holland
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
Stena	Steno	k1gNnSc2	Steno
Line	linout	k5eAaImIp3nS	linout
(	(	kIx(	(
<g/>
http://www.stenaline.com/stena-line/	[url]	k?	http://www.stenaline.com/stena-line/
<g/>
)	)	kIx)	)
Ramsgate	Ramsgat	k1gInSc5	Ramsgat
-	-	kIx~	-
Oostende	Oostend	k1gMnSc5	Oostend
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
TRANSEUROPA	TRANSEUROPA	kA	TRANSEUROPA
FERRIES	FERRIES	kA	FERRIES
(	(	kIx(	(
<g/>
http://www.transeuropaferries.com/	[url]	k?	http://www.transeuropaferries.com/
<g/>
)	)	kIx)	)
Dover	Dover	k1gMnSc1	Dover
-	-	kIx~	-
Dunkerque	Dunkerque	k1gInSc1	Dunkerque
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
norfolkline	norfolklin	k1gInSc5	norfolklin
(	(	kIx(	(
<g/>
http://www.norfolkline.com/ferry/	[url]	k?	http://www.norfolkline.com/ferry/
<g/>
)	)	kIx)	)
Dover	Dover	k1gMnSc1	Dover
-	-	kIx~	-
Callais	Callais	k1gInSc1	Callais
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
dopravce	dopravce	k1gMnSc1	dopravce
P	P	kA	P
<g/>
&	&	k?	&
<g/>
O	o	k7c4	o
Ferries	Ferries	k1gInSc4	Ferries
a	a	k8xC	a
SEAFRANCE	SEAFRANCE	kA	SEAFRANCE
(	(	kIx(	(
<g/>
http://www.poferries.com/tourist/	[url]	k?	http://www.poferries.com/tourist/
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
http://www.seafrance.com/seafrance/opencms/uk/en/passenger/	[url]	k?	http://www.seafrance.com/seafrance/opencms/uk/en/passenger/
<g/>
)	)	kIx)	)
Dover	Dover	k1gMnSc1	Dover
-	-	kIx~	-
Boulogne-sur-Mer	Boulogneur-Mer	k1gMnSc1	Boulogne-sur-Mer
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
SPEEDFERRIES	SPEEDFERRIES	kA	SPEEDFERRIES
(	(	kIx(	(
<g/>
http://www.speedferries.com/	[url]	k?	http://www.speedferries.com/
<g/>
)	)	kIx)	)
Newhaven	Newhavna	k1gFnPc2	Newhavna
-	-	kIx~	-
Dieppe	Diepp	k1gMnSc5	Diepp
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
TRANSMANCHE	TRANSMANCHE	kA	TRANSMANCHE
FERRIES	FERRIES	kA	FERRIES
(	(	kIx(	(
<g/>
http://www.transmancheferries.com/	[url]	k?	http://www.transmancheferries.com/
<g/>
)	)	kIx)	)
Newhaven	Newhavna	k1gFnPc2	Newhavna
-	-	kIx~	-
Le	Le	k1gMnSc5	Le
Havre	Havr	k1gMnSc5	Havr
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
TRANSMANCHE	TRANSMANCHE	kA	TRANSMANCHE
FERRIES	FERRIES	kA	FERRIES
(	(	kIx(	(
<g/>
http://www.transmancheferries.com/	[url]	k?	http://www.transmancheferries.com/
<g/>
)	)	kIx)	)
Portsmouth-	Portsmouth-	k1gMnSc5	Portsmouth-
Le	Le	k1gMnSc5	Le
Havre	Havr	k1gMnSc5	Havr
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
TRANSMANCHE	TRANSMANCHE	kA	TRANSMANCHE
FERRIES	FERRIES	kA	FERRIES
(	(	kIx(	(
<g/>
http://www.transmancheferries.com/	[url]	k?	http://www.transmancheferries.com/
<g/>
)	)	kIx)	)
Portsmouth	Portsmouth	k1gMnSc1	Portsmouth
-	-	kIx~	-
Caen	Caen	k1gMnSc1	Caen
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
Brittaby	Brittaba	k1gFnSc2	Brittaba
Ferries	Ferries	k1gMnSc1	Ferries
(	(	kIx(	(
<g/>
http://www.brittany-ferries.co.uk/	[url]	k?	http://www.brittany-ferries.co.uk/
<g/>
)	)	kIx)	)
Portsmouth	Portsmouth	k1gMnSc1	Portsmouth
-	-	kIx~	-
Cherbourg	Cherbourg	k1gMnSc1	Cherbourg
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
Brittaby	Brittaba	k1gFnSc2	Brittaba
Ferries	Ferries	k1gMnSc1	Ferries
a	a	k8xC	a
Condorferries	Condorferries	k1gMnSc1	Condorferries
(	(	kIx(	(
<g/>
http://www.brittany-ferries.co.uk/	[url]	k?	http://www.brittany-ferries.co.uk/
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
http://www.condorferries.com/uk/home.aspx	[url]	k1gInSc1	http://www.condorferries.com/uk/home.aspx
<g/>
)	)	kIx)	)
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
jen	jen	k9	jen
v	v	k7c6	v
lete	let	k1gInSc5	let
<g/>
,	,	kIx,	,
Portsmouth	Portsmouth	k1gMnSc1	Portsmouth
-	-	kIx~	-
St.	st.	kA	st.
<g/>
Malo	Malo	k1gMnSc1	Malo
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
Brittaby	Brittaba	k1gFnSc2	Brittaba
Ferries	Ferries	k1gMnSc1	Ferries
(	(	kIx(	(
<g/>
http://www.brittany-ferries.co.uk/	[url]	k?	http://www.brittany-ferries.co.uk/
<g/>
)	)	kIx)	)
Portsmouth	Portsmouth	k1gMnSc1	Portsmouth
-	-	kIx~	-
Guernsey-	Guernsey-	k1gMnSc1	Guernsey-
Jersey-	Jersey-	k1gMnSc1	Jersey-
Portsmouth	Portsmouth	k1gMnSc1	Portsmouth
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
Condorferries	Condorferries	k1gMnSc1	Condorferries
(	(	kIx(	(
<g/>
http://www.condorferries.com/uk/home.aspx	[url]	k1gInSc1	http://www.condorferries.com/uk/home.aspx
<g/>
)	)	kIx)	)
Portsmouth	Portsmouth	k1gMnSc1	Portsmouth
-	-	kIx~	-
Bilbao	Bilbao	k1gMnSc1	Bilbao
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
<g />
.	.	kIx.	.
</s>
<s>
P	P	kA	P
<g/>
&	&	k?	&
<g/>
O	o	k7c4	o
Ferries	Ferries	k1gInSc4	Ferries
(	(	kIx(	(
<g/>
http://www.poferries.com/tourist/	[url]	k?	http://www.poferries.com/tourist/
<g/>
)	)	kIx)	)
Poole	Poole	k1gInSc1	Poole
-	-	kIx~	-
Guernsey	Guernsea	k1gFnPc1	Guernsea
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
Condorferries	Condorferries	k1gMnSc1	Condorferries
(	(	kIx(	(
<g/>
http://www.condorferries.com/uk/home.aspx	[url]	k1gInSc1	http://www.condorferries.com/uk/home.aspx
<g/>
)	)	kIx)	)
Poole	Poole	k1gInSc1	Poole
-	-	kIx~	-
Jersey	Jersea	k1gFnPc1	Jersea
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
Condorferries	Condorferries	k1gMnSc1	Condorferries
(	(	kIx(	(
<g/>
http://www.condorferries.com/uk/home.aspx	[url]	k1gInSc1	http://www.condorferries.com/uk/home.aspx
<g/>
)	)	kIx)	)
Poole	Poole	k1gFnSc1	Poole
-	-	kIx~	-
Cherbourg	Cherbourg	k1gMnSc1	Cherbourg
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
Brittaby	Brittaba	k1gFnSc2	Brittaba
Ferries	Ferries	k1gMnSc1	Ferries
(	(	kIx(	(
<g/>
http://www.brittanyferries.com/	[url]	k?	http://www.brittanyferries.com/
<g/>
)	)	kIx)	)
Poole	Poole	k1gFnSc1	Poole
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
St.	st.	kA	st.
Malo	Malo	k1gMnSc1	Malo
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
Condorferries	Condorferries	k1gMnSc1	Condorferries
(	(	kIx(	(
<g/>
http://www.condorferries.com/uk/home.aspx	[url]	k1gInSc1	http://www.condorferries.com/uk/home.aspx
<g/>
)	)	kIx)	)
Weymouth	Weymouth	k1gInSc1	Weymouth
-	-	kIx~	-
Guernsey	Guernsea	k1gFnPc1	Guernsea
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
Condorferries	Condorferries	k1gMnSc1	Condorferries
(	(	kIx(	(
<g/>
http://www.condorferries.com/uk/home.aspx	[url]	k1gInSc1	http://www.condorferries.com/uk/home.aspx
<g/>
)	)	kIx)	)
Weymouth	Weymouth	k1gInSc1	Weymouth
-	-	kIx~	-
Jersey	Jersea	k1gFnPc1	Jersea
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
Condorferries	Condorferries	k1gMnSc1	Condorferries
(	(	kIx(	(
<g/>
http://www.condorferries.com/uk/home.aspx	[url]	k1gInSc1	http://www.condorferries.com/uk/home.aspx
<g/>
)	)	kIx)	)
Weymouth	Weymouth	k1gMnSc1	Weymouth
-	-	kIx~	-
St.	st.	kA	st.
Malo	Malo	k1gMnSc1	Malo
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
Condorferries	Condorferries	k1gMnSc1	Condorferries
(	(	kIx(	(
<g/>
http://www.condorferries.com/uk/home.aspx	[url]	k1gInSc1	http://www.condorferries.com/uk/home.aspx
<g/>
)	)	kIx)	)
St.	st.	kA	st.
Malo	Malo	k1gNnSc1	Malo
-	-	kIx~	-
Jersey	Jerse	k2eAgFnPc1d1	Jerse
-	-	kIx~	-
Guernsey	Guernsea	k1gFnPc1	Guernsea
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
hdferries	hdferries	k1gMnSc1	hdferries
(	(	kIx(	(
<g/>
http://www.hdferries.com/	[url]	k?	http://www.hdferries.com/
Plymouth	Plymouth	k1gMnSc1	Plymouth
-	-	kIx~	-
Roscoff	Roscoff	k1gMnSc1	Roscoff
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
Brittaby	Brittaba	k1gFnSc2	Brittaba
Ferries	Ferries	k1gMnSc1	Ferries
(	(	kIx(	(
<g/>
http://www.brittany-ferries.co.uk/	[url]	k?	http://www.brittany-ferries.co.uk/
<g/>
)	)	kIx)	)
Plymouth	Plymouth	k1gMnSc1	Plymouth
-	-	kIx~	-
Santander	Santander	k1gMnSc1	Santander
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
Brittaby	Brittaba	k1gFnSc2	Brittaba
Ferries	Ferries	k1gMnSc1	Ferries
(	(	kIx(	(
<g/>
http://www.brittany-ferries.co.uk/	[url]	k?	http://www.brittany-ferries.co.uk/
<g/>
)	)	kIx)	)
Přívozy	přívoz	k1gInPc1	přívoz
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
Přívoz	přívoz	k1gInSc1	přívoz
</s>
