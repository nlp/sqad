<s>
Josef	Josef	k1gMnSc1
Karel	Karel	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
Habsbursko-Lotrinský	habsbursko-lotrinský	k2eAgMnSc1d1
</s>
<s>
Josef	Josef	k1gMnSc1
Karel	Karel	k1gMnSc1
LudvíkHabsbursko-Lotrinský	LudvíkHabsbursko-Lotrinský	k2eAgMnSc1d1
arcivévoda	arcivévoda	k1gMnSc1
Josef	Josef	k1gMnSc1
Karel	Karel	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
Narození	narození	k1gNnPc2
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1833	#num#	k4
<g/>
Bratislava	Bratislava	k1gFnSc1
Úmrtí	úmrtí	k1gNnSc2
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1905	#num#	k4
<g/>
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
77	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Rijeka	Rijeka	k1gFnSc1
Manžel	manžel	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ka	ka	k?
<g/>
)	)	kIx)
</s>
<s>
Klotylda	Klotylda	k1gFnSc1
Sasko-Kobursko-Saalfeldská	Sasko-Kobursko-Saalfeldský	k2eAgFnSc1d1
Děti	dítě	k1gFnPc1
</s>
<s>
AlžbětaMarie	AlžbětaMarie	k1gFnSc1
DoroteaMarkéta	DoroteaMarkéta	k1gMnSc1
KlementinaJosef	KlementinaJosef	k1gMnSc1
AugustLadislav	AugustLadislav	k1gMnSc1
FilipAlžběta	FilipAlžběta	k1gMnSc1
KlotyldaKlotylda	KlotyldaKlotylda	k1gMnSc1
Rodiče	rodič	k1gMnPc1
</s>
<s>
Josef	Josef	k1gMnSc1
Habsbursko-LotrinskýMarie	Habsbursko-LotrinskýMarie	k1gFnSc2
Dorotea	Dorotea	k1gFnSc1
Württemberská	Württemberský	k2eAgFnSc1d1
Rod	rod	k1gInSc4
</s>
<s>
Habsbursko-lotrinská	habsbursko-lotrinský	k2eAgFnSc1d1
dynastie	dynastie	k1gFnSc1
Funkce	funkce	k1gFnSc1
</s>
<s>
člen	člen	k1gMnSc1
Panské	panský	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
(	(	kIx(
<g/>
od	od	k7c2
1861	#num#	k4
<g/>
)	)	kIx)
<g/>
uherský	uherský	k2eAgMnSc1d1
palatin	palatin	k1gMnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Josef	Josef	k1gMnSc1
Karel	Karel	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
Habsbursko-Lotrinský	habsbursko-lotrinský	k2eAgMnSc1d1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1833	#num#	k4
<g/>
,	,	kIx,
Bratislava	Bratislava	k1gFnSc1
–	–	k?
13	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1905	#num#	k4
<g/>
,	,	kIx,
Rijeka	Rijeka	k1gFnSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
rakouský	rakouský	k2eAgMnSc1d1
arcivévoda	arcivévoda	k1gMnSc1
z	z	k7c2
uherské	uherský	k2eAgFnSc2d1
větvě	větvit	k5eAaImSgInS
Habsbursko-Lotrinské	habsbursko-lotrinský	k2eAgFnPc4d1
dynastie	dynastie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Původ	původ	k1gInSc1
a	a	k8xC
mládí	mládí	k1gNnSc1
</s>
<s>
Byl	být	k5eAaImAgMnS
druhým	druhý	k4xOgMnSc7
synem	syn	k1gMnSc7
uherského	uherský	k2eAgMnSc2d1
palatina	palatin	k1gMnSc2
Josefa	Josef	k1gMnSc2
Antonína	Antonín	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc7
matkou	matka	k1gFnSc7
byla	být	k5eAaImAgFnS
otcova	otcův	k2eAgFnSc1d1
třetí	třetí	k4xOgFnSc1
manželka	manželka	k1gFnSc1
Marie	Maria	k1gFnSc2
Dorotea	Dorotea	k1gFnSc1
Württemberská	Württemberský	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnSc7
strýcem	strýc	k1gMnSc7
byl	být	k5eAaImAgMnS
císař	císař	k1gMnSc1
František	František	k1gMnSc1
I.	I.	kA
Získal	získat	k5eAaPmAgMnS
výborné	výborný	k2eAgNnSc4d1
vzdělání	vzdělání	k1gNnSc4
<g/>
,	,	kIx,
zejména	zejména	k9
od	od	k7c2
svého	svůj	k3xOyFgMnSc2
učitele	učitel	k1gMnSc2
<g/>
,	,	kIx,
benediktinského	benediktinský	k2eAgMnSc2d1
mnicha	mnich	k1gMnSc2
a	a	k8xC
historika	historik	k1gMnSc2
Flórise	Flórise	k1gFnSc2
Rómera	Rómer	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
1860	#num#	k4
byl	být	k5eAaImAgInS
generálmajorem	generálmajor	k1gMnSc7
a	a	k8xC
o	o	k7c4
šest	šest	k4xCc4
let	léto	k1gNnPc2
později	pozdě	k6eAd2
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1866	#num#	k4
ho	on	k3xPp3gMnSc4
čekalo	čekat	k5eAaImAgNnS
povýšení	povýšení	k1gNnSc1
na	na	k7c4
polního	polní	k2eAgMnSc4d1
podmaršálka	podmaršálek	k1gMnSc4
<g/>
,	,	kIx,
za	za	k7c4
statečnost	statečnost	k1gFnSc4
a	a	k8xC
řízení	řízení	k1gNnSc4
vojenských	vojenský	k2eAgFnPc2d1
akcí	akce	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1869	#num#	k4
převzal	převzít	k5eAaPmAgMnS
vrchní	vrchní	k1gMnSc1
velení	velení	k1gNnSc2
uherské	uherský	k2eAgFnSc2d1
zeměbrany	zeměbrana	k1gFnSc2
a	a	k8xC
od	od	k7c2
roku	rok	k1gInSc2
1874	#num#	k4
byl	být	k5eAaImAgMnS
generálem	generál	k1gMnSc7
jezdectva	jezdectvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Vyznal	vyznat	k5eAaPmAgMnS,k5eAaBmAgMnS
se	se	k3xPyFc4
jak	jak	k8xC,k8xS
v	v	k7c6
přírodních	přírodní	k2eAgFnPc6d1
<g/>
,	,	kIx,
tak	tak	k9
společenských	společenský	k2eAgFnPc6d1
vědách	věda	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Napsal	napsat	k5eAaBmAgMnS,k5eAaPmAgMnS
několik	několik	k4yIc1
spisů	spis	k1gInPc2
<g/>
,	,	kIx,
např.	např.	kA
o	o	k7c6
gramatice	gramatika	k1gFnSc6
cikánského	cikánský	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
a	a	k8xC
cikánské	cikánský	k2eAgFnSc3d1
etnografii	etnografie	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1888	#num#	k4
byl	být	k5eAaImAgInS
čestným	čestný	k2eAgMnSc7d1
členem	člen	k1gMnSc7
Uherské	uherský	k2eAgFnSc2d1
akademie	akademie	k1gFnSc2
věd	věda	k1gFnPc2
a	a	k8xC
roku	rok	k1gInSc2
1896	#num#	k4
získal	získat	k5eAaPmAgInS
čestný	čestný	k2eAgInSc1d1
doktorát	doktorát	k1gInSc1
univerzity	univerzita	k1gFnSc2
v	v	k7c6
Budapešti	Budapešť	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1897	#num#	k4
obdržel	obdržet	k5eAaPmAgInS
doktorát	doktorát	k1gInSc1
univerzity	univerzita	k1gFnSc2
Františka	František	k1gMnSc2
Josefa	Josef	k1gMnSc2
v	v	k7c6
Cluji	Cluj	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zemřel	zemřít	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1905	#num#	k4
v	v	k7c6
Rijece	Rijeka	k1gFnSc6
a	a	k8xC
byl	být	k5eAaImAgMnS
pohřben	pohřbít	k5eAaPmNgMnS
v	v	k7c6
Budapešti	Budapešť	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Manželka	manželka	k1gFnSc1
a	a	k8xC
děti	dítě	k1gFnPc1
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
1864	#num#	k4
byl	být	k5eAaImAgMnS
ženatý	ženatý	k2eAgMnSc1d1
s	s	k7c7
princeznou	princezna	k1gFnSc7
Klotyldou	Klotylda	k1gFnSc7
Sasko-Kobursko-Saalfeldskou	Sasko-Kobursko-Saalfeldský	k2eAgFnSc7d1
(	(	kIx(
<g/>
1846	#num#	k4
<g/>
–	–	k?
<g/>
1927	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
se	s	k7c7
kterou	který	k3yIgFnSc7,k3yRgFnSc7,k3yQgFnSc7
měl	mít	k5eAaImAgMnS
sedm	sedm	k4xCc4
dětí	dítě	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
(	(	kIx(
<g/>
1865	#num#	k4
<g/>
–	–	k?
<g/>
1866	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Marie	Marie	k1gFnSc1
Dorotea	Dorotea	k1gFnSc1
(	(	kIx(
<g/>
1867	#num#	k4
<g/>
–	–	k?
<g/>
1932	#num#	k4
<g/>
)	)	kIx)
∞	∞	k?
1896	#num#	k4
vévoda	vévoda	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
Filip	Filip	k1gMnSc1
Robert	Robert	k1gMnSc1
Orleánský	orleánský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1869	#num#	k4
<g/>
–	–	k?
<g/>
1926	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Markéta	Markéta	k1gFnSc1
Klementina	Klementina	k1gFnSc1
(	(	kIx(
<g/>
1870	#num#	k4
<g/>
–	–	k?
<g/>
1955	#num#	k4
<g/>
)	)	kIx)
∞	∞	k?
1890	#num#	k4
kníže	kníže	k1gMnSc1
Albert	Albert	k1gMnSc1
z	z	k7c2
Thurn-Taxisu	Thurn-Taxis	k1gInSc2
(	(	kIx(
<g/>
1867	#num#	k4
<g/>
–	–	k?
<g/>
1952	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Josef	Josef	k1gMnSc1
August	August	k1gMnSc1
(	(	kIx(
<g/>
1872	#num#	k4
<g/>
–	–	k?
<g/>
1962	#num#	k4
<g/>
)	)	kIx)
∞	∞	k?
1893	#num#	k4
Augusta	August	k1gMnSc2
Bavorská	bavorský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1875	#num#	k4
<g/>
–	–	k?
<g/>
1964	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vnučka	vnučka	k1gFnSc1
Františka	František	k1gMnSc2
Josefa	Josef	k1gMnSc2
I.	I.	kA
</s>
<s>
Ladislav	Ladislav	k1gMnSc1
Filip	Filip	k1gMnSc1
(	(	kIx(
<g/>
1875	#num#	k4
<g/>
–	–	k?
<g/>
1895	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zemřel	zemřít	k5eAaPmAgMnS
při	při	k7c6
nehodě	nehoda	k1gFnSc6
na	na	k7c6
lovu	lov	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
Klotylda	Klotylda	k1gFnSc1
(	(	kIx(
<g/>
1883	#num#	k4
<g/>
–	–	k?
<g/>
1958	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
svobodná	svobodný	k2eAgFnSc1d1
</s>
<s>
Klotylda	Klotylda	k1gFnSc1
(	(	kIx(
<g/>
1884	#num#	k4
<g/>
–	–	k?
<g/>
1903	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
svobodná	svobodný	k2eAgFnSc1d1
</s>
<s>
Výběr	výběr	k1gInSc1
díla	dílo	k1gNnSc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Fundamentum	Fundamentum	k1gNnSc1
linguae	lingua	k1gMnSc2
Zingaricae	Zingarica	k1gMnSc2
<g/>
,	,	kIx,
I.	I.	kA
I.	I.	kA
<g/>
M.	M.	kA
Koritschhnyák	Koritschhnyák	k1gMnSc1
<g/>
,	,	kIx,
Anno	Anna	k1gFnSc5
1806	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
<g/>
:	:	kIx,
Egyetemes	Egyetemes	k1gInSc1
Philológiai	Philológia	k1gFnSc2
Közlöny	Közlöna	k1gFnSc2
<g/>
,	,	kIx,
Budapest	Budapest	k1gFnSc1
1887	#num#	k4
<g/>
,	,	kIx,
11	#num#	k4
<g/>
,	,	kIx,
705	#num#	k4
<g/>
–	–	k?
<g/>
731	#num#	k4
</s>
<s>
Arborethum	Arborethum	k1gInSc1
Alcusthiense	Alcusthiense	k1gFnSc2
(	(	kIx(
<g/>
katalog	katalog	k1gInSc1
rostlin	rostlina	k1gFnPc2
a	a	k8xC
keřů	keř	k1gInPc2
pěstovaných	pěstovaný	k2eAgInPc2d1
v	v	k7c6
alcsutském	alcsutský	k2eAgNnSc6d1
arboretu	arboretum	k1gNnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Kolozsvár	Kolozsvár	k1gMnSc1
(	(	kIx(
<g/>
Cluj	Cluj	k1gMnSc1
<g/>
)	)	kIx)
1892	#num#	k4
<g/>
,	,	kIx,
305	#num#	k4
str	str	kA
<g/>
.	.	kIx.
</s>
<s>
Czigány	Czigána	k1gFnPc1
nyelvtan	nyelvtan	k1gInSc1
(	(	kIx(
<g/>
gramatika	gramatika	k1gFnSc1
cikánštiny	cikánština	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Budapest	Budapest	k1gMnSc1
XXIII	XXIII	kA
<g/>
.	.	kIx.
1888	#num#	k4
<g/>
,	,	kIx,
377	#num#	k4
str	str	kA
<g/>
.	.	kIx.
</s>
<s>
A	a	k9
cigányokról	cigányokról	k1gInSc1
(	(	kIx(
<g/>
dějiny	dějiny	k1gFnPc1
<g/>
,	,	kIx,
způsob	způsob	k1gInSc1
života	život	k1gInSc2
<g/>
,	,	kIx,
pověry	pověra	k1gFnSc2
<g/>
,	,	kIx,
lidová	lidový	k2eAgFnSc1d1
básnická	básnický	k2eAgFnSc1d1
tvorba	tvorba	k1gFnSc1
<g/>
,	,	kIx,
hudba	hudba	k1gFnSc1
<g/>
,	,	kIx,
řeč	řeč	k1gFnSc1
a	a	k8xC
literatura	literatura	k1gFnSc1
cikánů	cikán	k1gMnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Budapest	Budapest	k1gFnSc1
1894	#num#	k4
<g/>
,	,	kIx,
97	#num#	k4
str	str	kA
<g/>
.	.	kIx.
</s>
<s>
Zigeunergrammatik	Zigeunergrammatik	k1gMnSc1
<g/>
,	,	kIx,
Budapest	Budapest	k1gFnSc1
1902	#num#	k4
<g/>
,	,	kIx,
160	#num#	k4
str	str	kA
<g/>
.	.	kIx.
</s>
<s>
Vývod	vývod	k1gInSc1
z	z	k7c2
předků	předek	k1gInPc2
</s>
<s>
Leopold	Leopold	k1gMnSc1
Josef	Josef	k1gMnSc1
Lotrinský	lotrinský	k2eAgMnSc1d1
</s>
<s>
František	František	k1gMnSc1
I.	I.	kA
Štěpán	Štěpán	k1gMnSc1
Lotrinský	lotrinský	k2eAgMnSc1d1
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
Charlotta	Charlotta	k1gFnSc1
Orléanská	Orléanský	k2eAgFnSc1d1
</s>
<s>
Leopold	Leopold	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Karel	Karel	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
</s>
<s>
Marie	Marie	k1gFnSc1
Terezie	Terezie	k1gFnSc2
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
Kristýna	Kristýna	k1gFnSc1
Brunšvicko-Wolfenbüttelská	Brunšvicko-Wolfenbüttelský	k2eAgFnSc1d1
</s>
<s>
Josef	Josef	k1gMnSc1
Habsbursko-Lotrinský	habsbursko-lotrinský	k2eAgMnSc1d1
</s>
<s>
Filip	Filip	k1gMnSc1
V.	V.	kA
Španělský	španělský	k2eAgMnSc1d1
</s>
<s>
Karel	Karel	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Španělský	španělský	k2eAgInSc1d1
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
Parmská	parmský	k2eAgFnSc1d1
</s>
<s>
Marie	Marie	k1gFnSc1
Ludovika	Ludovikum	k1gNnSc2
Španělská	španělský	k2eAgFnSc1d1
</s>
<s>
August	August	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Polský	polský	k2eAgInSc1d1
</s>
<s>
Marie	Marie	k1gFnSc1
Amálie	Amálie	k1gFnSc2
Saská	saský	k2eAgFnSc1d1
</s>
<s>
Marie	Marie	k1gFnSc1
Josefa	Josefa	k1gFnSc1
Habsburská	habsburský	k2eAgFnSc1d1
</s>
<s>
'	'	kIx"
<g/>
Josef	Josef	k1gMnSc1
Karel	Karel	k1gMnSc1
Habsbursko-Lotrinský	habsbursko-lotrinský	k2eAgMnSc1d1
<g/>
'	'	kIx"
</s>
<s>
Karel	Karel	k1gMnSc1
Alexandr	Alexandr	k1gMnSc1
Württemberský	Württemberský	k2eAgMnSc1d1
</s>
<s>
Fridrich	Fridrich	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Evžen	Evžen	k1gMnSc1
Württemberský	Württemberský	k2eAgInSc1d1
</s>
<s>
Marie	Marie	k1gFnSc1
Augusta	August	k1gMnSc2
Thurn-Taxis	Thurn-Taxis	k1gFnSc2
</s>
<s>
Ludvík	Ludvík	k1gMnSc1
Württemberský	Württemberský	k2eAgMnSc1d1
</s>
<s>
Fridrich	Fridrich	k1gMnSc1
Vilém	Vilém	k1gMnSc1
Braniborsko-Schwedtský	Braniborsko-Schwedtský	k2eAgMnSc1d1
</s>
<s>
Bedřiška	Bedřiška	k1gFnSc1
Braniborsko-Schwedtská	Braniborsko-Schwedtský	k2eAgFnSc1d1
</s>
<s>
Žofie	Žofie	k1gFnSc1
Dorota	Dorota	k1gFnSc1
Pruská	pruský	k2eAgFnSc1d1
</s>
<s>
Marie	Marie	k1gFnSc1
Dorotea	Dorote	k1gInSc2
Württemberská	Württemberský	k2eAgFnSc1d1
</s>
<s>
Karel	Karel	k1gMnSc1
August	August	k1gMnSc1
Nasavsko-Weilburský	Nasavsko-Weilburský	k2eAgMnSc1d1
</s>
<s>
Karel	Karel	k1gMnSc1
Kristián	Kristián	k1gMnSc1
Nasavsko-Weilburský	Nasavsko-Weilburský	k2eAgMnSc1d1
</s>
<s>
Augusta	Augusta	k1gMnSc1
Bedřiška	Bedřiška	k1gFnSc1
Nasavsko-Idsteinská	Nasavsko-Idsteinský	k2eAgFnSc5d1
</s>
<s>
Henrietta	Henrietta	k1gFnSc1
Nasavsko-Weilburská	Nasavsko-Weilburský	k2eAgFnSc1d1
</s>
<s>
Vilém	Vilém	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oranžský	oranžský	k2eAgInSc1d1
</s>
<s>
Karolína	Karolína	k1gFnSc1
Oranžsko-Nasavská	Oranžsko-Nasavský	k2eAgFnSc1d1
</s>
<s>
Anna	Anna	k1gFnSc1
Hannoverská	hannoverský	k2eAgFnSc1d1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
HAMANNOVÁ	HAMANNOVÁ	kA
<g/>
,	,	kIx,
Brigitte	Brigitte	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Habsburkové	Habsburk	k1gMnPc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Životopisná	životopisný	k2eAgFnSc1d1
encyklopedie	encyklopedie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
BRÁNA	brána	k1gFnSc1
<g/>
,	,	kIx,
Knižní	knižní	k2eAgInSc1d1
klub	klub	k1gInSc1
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
.	.	kIx.
408	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85946	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
S.	S.	kA
188	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Hamannová	Hamannová	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
232	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Josef	Josef	k1gMnSc1
Karel	Karel	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
Habsbursko-Lotrinský	habsbursko-lotrinský	k2eAgMnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Záznam	záznam	k1gInSc1
o	o	k7c6
narození	narození	k1gNnSc6
a	a	k8xC
křtu	křest	k1gInSc6
v	v	k7c6
matrice	matrika	k1gFnSc6
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Stručný	stručný	k2eAgInSc1d1
životopis	životopis	k1gInSc1
na	na	k7c6
stránkách	stránka	k1gFnPc6
The	The	k1gFnSc2
Peerage	Peerag	k1gFnSc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Rakouští	rakouský	k2eAgMnPc1d1
arcivévodové	arcivévoda	k1gMnPc1
1	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Fridrich	Fridrich	k1gMnSc1
V.	V.	kA
•	•	k?
Albrecht	Albrecht	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
•	•	k?
Zikmund	Zikmund	k1gMnSc1
•	•	k?
Ladislav	Ladislav	k1gMnSc1
Pohrobek	pohrobek	k1gMnSc1
Znak	znak	k1gInSc4
rakouské	rakouský	k2eAgFnSc2d1
císařské	císařský	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
2	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Maxmilián	Maxmilián	k1gMnSc1
I.	I.	kA
3	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Filip	Filip	k1gMnSc1
I.	I.	kA
<g/>
*	*	kIx~
4	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Karel	Karel	k1gMnSc1
V.	V.	kA
<g/>
*	*	kIx~
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
I.	I.	kA
Habsburský	habsburský	k2eAgMnSc1d1
<g/>
*	*	kIx~
5	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Filip	Filip	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Španělský	španělský	k2eAgInSc1d1
<g/>
*	*	kIx~
•	•	k?
Maxmilián	Maxmilián	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyrolský	tyrolský	k2eAgMnSc1d1
•	•	k?
Karel	Karel	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Štýrský	štýrský	k2eAgInSc4d1
6	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Filip	Filip	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
<g/>
*	*	kIx~
•	•	k?
Rudolf	Rudolf	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
•	•	k?
Matyáš	Matyáš	k1gMnSc1
Habsburský	habsburský	k2eAgMnSc1d1
•	•	k?
Maxmilián	Maxmilián	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
•	•	k?
Albrecht	Albrecht	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
•	•	k?
Maxmilián	Maxmilián	k1gMnSc1
Arnošt	Arnošt	k1gMnSc1
•	•	k?
Leopold	Leopold	k1gMnSc1
V.	V.	kA
•	•	k?
Karel	Karel	k1gMnSc1
7	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Filip	Filip	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
<g/>
*	*	kIx~
•	•	k?
Jan	Jan	k1gMnSc1
Karel	Karel	k1gMnSc1
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
•	•	k?
Leopold	Leopold	k1gMnSc1
I.	I.	kA
Vilém	Vilém	k1gMnSc1
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
Karel	Karel	k1gMnSc1
Tyrolský	tyrolský	k2eAgMnSc1d1
•	•	k?
Zikmund	Zikmund	k1gMnSc1
František	František	k1gMnSc1
Tyrolský	tyrolský	k2eAgMnSc1d1
8	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Karel	Karel	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
<g/>
*	*	kIx~
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
•	•	k?
Leopold	Leopold	k1gMnSc1
I.	I.	kA
•	•	k?
Karel	Karel	k1gMnSc1
I.	I.	kA
Josef	Josef	k1gMnSc1
Habsburský	habsburský	k2eAgMnSc1d1
9	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Josef	Josef	k1gMnSc1
I.	I.	kA
•	•	k?
Leopold	Leopold	k1gMnSc1
Josef	Josef	k1gMnSc1
•	•	k?
Karel	Karel	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Leopold	Leopold	k1gMnSc1
Josef	Josef	k1gMnSc1
•	•	k?
František	František	k1gMnSc1
I.	I.	kA
Štěpán	Štěpán	k1gMnSc1
Lotrinský	lotrinský	k2eAgMnSc1d1
11	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Josef	Josef	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
<g/>
**	**	k?
•	•	k?
Karel	Karel	k1gMnSc1
Josef	Josef	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Leopold	Leopolda	k1gFnPc2
II	II	kA
<g/>
.	.	kIx.
<g/>
**	**	k?
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
Karel	Karel	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Maxmilián	Maxmilián	k1gMnSc1
František	František	k1gMnSc1
<g/>
**	**	k?
12	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
František	František	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
<g/>
**	**	k?
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toskánský	toskánský	k2eAgInSc1d1
<g/>
**	**	k?
•	•	k?
Karel	Karel	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
Těšínský	Těšínský	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Alexandr	Alexandr	k1gMnSc1
Leopold	Leopold	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Josef	Josef	k1gMnSc1
Antonín	Antonín	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Antonín	Antonín	k1gMnSc1
Viktor	Viktor	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Jan	Jan	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Rainer	Rainer	k1gMnSc1
Josef	Josef	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Ludvík	Ludvík	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Rudolf	Rudolf	k1gMnSc1
Jan	Jan	k1gMnSc1
<g/>
**	**	k?
•	•	k?
František	František	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Modenský	modenský	k2eAgInSc1d1
<g/>
***	***	k?
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
Karel	Karel	k1gMnSc1
Josef	Josef	k1gMnSc1
<g/>
***	***	k?
•	•	k?
Maxmilián	Maxmilián	k1gMnSc1
Josef	Josef	k1gMnSc1
<g/>
***	***	k?
•	•	k?
Karel	Karel	k1gMnSc1
Ambrož	Ambrož	k1gMnSc1
<g/>
***	***	k?
13	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1
I.	I.	kA
Dobrotivý	dobrotivý	k2eAgInSc1d1
•	•	k?
František	františek	k1gInSc1
Karel	Karel	k1gMnSc1
•	•	k?
Leopold	Leopold	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toskánský	toskánský	k2eAgInSc1d1
<g/>
**	**	k?
•	•	k?
Albrecht	Albrecht	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
•	•	k?
Karel	Karel	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
•	•	k?
Fridrich	Fridrich	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
•	•	k?
Vilém	Vilém	k1gMnSc1
Těšínský	Těšínský	k1gMnSc1
•	•	k?
Štěpán	Štěpán	k1gMnSc1
Viktor	Viktor	k1gMnSc1
•	•	k?
Josef	Josef	k1gMnSc1
Karel	Karel	k1gMnSc1
•	•	k?
Leopold	Leopold	k1gMnSc1
•	•	k?
Arnošt	Arnošt	k1gMnSc1
•	•	k?
Zikmund	Zikmund	k1gMnSc1
•	•	k?
Rainer	Rainer	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
•	•	k?
Jindřich	Jindřich	k1gMnSc1
•	•	k?
František	František	k1gMnSc1
V.	V.	kA
Modenský	modenský	k2eAgInSc1d1
<g/>
***	***	k?
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
Karel	Karel	k1gMnSc1
Viktor	Viktor	k1gMnSc1
<g/>
***	***	k?
14	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
I.	I.	kA
•	•	k?
Maxmilián	Maxmilián	k1gMnSc1
I.	I.	kA
Mexický	mexický	k2eAgMnSc1d1
•	•	k?
Karel	Karel	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
•	•	k?
Ludvík	Ludvík	k1gMnSc1
Viktor	Viktor	k1gMnSc1
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toskánský	toskánský	k2eAgInSc1d1
<g/>
**	**	k?
•	•	k?
Karel	Karel	k1gMnSc1
Salvátor	Salvátor	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Ludvík	Ludvík	k1gMnSc1
Salvátor	Salvátor	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Jan	Jan	k1gMnSc1
Salvátor	Salvátor	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Bedřich	Bedřich	k1gMnSc1
Rakousko-Těšínský	rakousko-těšínský	k2eAgMnSc1d1
•	•	k?
Karel	Karel	k1gMnSc1
Štěpán	Štěpán	k1gMnSc1
•	•	k?
Evžen	Evžen	k1gMnSc1
•	•	k?
Josef	Josef	k1gMnSc1
August	August	k1gMnSc1
•	•	k?
Ladislav	Ladislav	k1gMnSc1
Filip	Filip	k1gMnSc1
15	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Korunní	korunní	k2eAgMnSc1d1
princ	princ	k1gMnSc1
Rudolf	Rudolf	k1gMnSc1
•	•	k?
František	František	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
Este	Est	k1gMnSc5
<g/>
***	***	k?
•	•	k?
Ota	Ota	k1gMnSc1
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
Karel	Karel	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
•	•	k?
Leopold	Leopold	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
Salvátor	Salvátor	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Josef	Josef	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Petr	Petr	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Jindřich	Jindřich	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Leopold	Leopold	k1gMnSc1
Salvátor	Salvátor	k1gMnSc1
<g/>
**	**	k?
•	•	k?
František	František	k1gMnSc1
Salvátor	Salvátor	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Albrecht	Albrecht	k1gMnSc1
Salvátor	Salvátor	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Albrecht	Albrecht	k1gMnSc1
•	•	k?
Karel	Karel	k1gMnSc1
Albrecht	Albrecht	k1gMnSc1
•	•	k?
Leo	Leo	k1gMnSc1
Karel	Karel	k1gMnSc1
•	•	k?
Vilém	Vilém	k1gMnSc1
František	František	k1gMnSc1
•	•	k?
Josef	Josef	k1gMnSc1
František	František	k1gMnSc1
•	•	k?
Ladislav	Ladislav	k1gMnSc1
Luitpold	Luitpold	k1gMnSc1
16	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Karel	Karel	k1gMnSc1
I.	I.	kA
•	•	k?
Maxmilián	Maxmilián	k1gMnSc1
Evžen	Evžen	k1gMnSc1
•	•	k?
Gottfried	Gottfried	k1gMnSc1
Toskánský	toskánský	k2eAgMnSc1d1
<g/>
**	**	k?
•	•	k?
Jiří	Jiří	k1gMnSc1
Toskánský	toskánský	k2eAgMnSc1d1
<g/>
**	**	k?
•	•	k?
Rainer	Rainer	k1gMnSc1
Karel	Karel	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Antonín	Antonín	k1gMnSc1
<g/>
**	**	k?
•	•	k?
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
Toskánský	toskánský	k2eAgMnSc1d1
<g/>
**	**	k?
•	•	k?
Karel	Karel	k1gMnSc1
Pius	Pius	k1gMnSc1
<g/>
**	**	k?
•	•	k?
František	František	k1gMnSc1
Karel	Karel	k1gMnSc1
Salvátor	Salvátor	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Hubert	Hubert	k1gMnSc1
Salvátor	Salvátor	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Theodor	Theodor	k1gMnSc1
Salvátor	Salvátor	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Klement	Klement	k1gMnSc1
Salvátor	Salvátor	k1gMnSc1
<g/>
**	**	k?
17	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Otto	Otto	k1gMnSc1
von	von	k1gInSc4
Habsburg	Habsburg	k1gMnSc1
•	•	k?
Robert	Robert	k1gMnSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
Este	Est	k1gMnSc5
<g/>
***	***	k?
•	•	k?
Felix	Felix	k1gMnSc1
Rakouský	rakouský	k2eAgMnSc1d1
•	•	k?
Karel	Karel	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
*	*	kIx~
také	také	k9
španělský	španělský	k2eAgMnSc1d1
princ	princ	k1gMnSc1
(	(	kIx(
<g/>
infant	infant	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
**	**	k?
také	také	k9
toskánský	toskánský	k2eAgMnSc1d1
princ	princ	k1gMnSc1
<g/>
,	,	kIx,
***	***	k?
také	také	k9
modenský	modenský	k2eAgMnSc1d1
princ	princ	k1gMnSc1
<g/>
,	,	kIx,
kurzívou	kurzíva	k1gFnSc7
jsou	být	k5eAaImIp3nP
rakouští	rakouský	k2eAgMnPc1d1
panovníci	panovník	k1gMnPc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
118713124	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
7139	#num#	k4
081X	081X	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
2002157936	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
52484206	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
2002157936	#num#	k4
</s>
