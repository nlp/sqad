<s>
Kation	kation	k1gInSc1
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
též	též	k9
kationt	kationt	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
kladně	kladně	k6eAd1
nabitý	nabitý	k2eAgInSc1d1
ion	ion	k1gInSc1
<g/>
,	,	kIx,
obvykle	obvykle	k6eAd1
atom	atom	k1gInSc1
nebo	nebo	k8xC
molekula	molekula	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
odevzdala	odevzdat	k5eAaPmAgFnS
elektron	elektron	k1gInSc4
<g/>
,	,	kIx,
(	(	kIx(
<g/>
nebo	nebo	k8xC
pohltila	pohltit	k5eAaPmAgFnS
kationty	kation	k1gInPc4
vodíku	vodík	k1gInSc2
<g/>
,	,	kIx,
volné	volný	k2eAgInPc1d1
protony	proton	k1gInPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>