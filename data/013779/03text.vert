<s>
Kation	kation	k1gInSc1
</s>
<s>
Kation	kation	k1gInSc1
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
též	též	k9
kationt	kationt	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
kladně	kladně	k6eAd1
nabitý	nabitý	k2eAgInSc1d1
ion	ion	k1gInSc1
<g/>
,	,	kIx,
obvykle	obvykle	k6eAd1
atom	atom	k1gInSc1
nebo	nebo	k8xC
molekula	molekula	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
odevzdala	odevzdat	k5eAaPmAgFnS
elektron	elektron	k1gInSc4
<g/>
,	,	kIx,
(	(	kIx(
<g/>
nebo	nebo	k8xC
pohltila	pohltit	k5eAaPmAgFnS
kationty	kation	k1gInPc4
vodíku	vodík	k1gInSc2
<g/>
,	,	kIx,
volné	volný	k2eAgInPc1d1
protony	proton	k1gInPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kation	kation	k1gInSc1
má	mít	k5eAaImIp3nS
v	v	k7c6
elektronovém	elektronový	k2eAgInSc6d1
obalu	obal	k1gInSc6
méně	málo	k6eAd2
elektronů	elektron	k1gInPc2
než	než	k8xS
odpovídající	odpovídající	k2eAgInSc4d1
atom	atom	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
elektrolýze	elektrolýza	k1gFnSc6
putuje	putovat	k5eAaImIp3nS
směrem	směr	k1gInSc7
ke	k	k7c3
katodě	katoda	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Vznik	vznik	k1gInSc1
</s>
<s>
Kationty	kation	k1gInPc1
většinou	většinou	k6eAd1
vznikají	vznikat	k5eAaImIp3nP
z	z	k7c2
elektropozitivních	elektropozitivní	k2eAgInPc2d1
prvků	prvek	k1gInPc2
<g/>
,	,	kIx,
například	například	k6eAd1
draslíku	draslík	k1gInSc2
<g/>
,	,	kIx,
hořčíku	hořčík	k1gInSc2
nebo	nebo	k8xC
kobaltu	kobalt	k1gInSc2
<g/>
:	:	kIx,
</s>
<s>
K	k	k7c3
→	→	k?
K	K	kA
<g/>
+	+	kIx~
+	+	kIx~
e	e	k0
<g/>
−	−	k?
<g/>
.	.	kIx.
</s>
<s>
Mohou	moct	k5eAaImIp3nP
však	však	k9
také	také	k9
vznikat	vznikat	k5eAaImF
z	z	k7c2
molekul	molekula	k1gFnPc2
<g/>
,	,	kIx,
např.	např.	kA
amonný	amonný	k2eAgInSc1d1
kation	kation	k1gInSc1
z	z	k7c2
molekuly	molekula	k1gFnSc2
amoniaku	amoniak	k1gInSc2
<g/>
:	:	kIx,
</s>
<s>
NH3	NH3	k4
+	+	kIx~
H	H	kA
<g/>
+	+	kIx~
→	→	k?
NH	NH	kA
<g/>
4	#num#	k4
<g/>
+	+	kIx~
</s>
<s>
nebo	nebo	k8xC
oxoniový	oxoniový	k2eAgInSc1d1
kation	kation	k1gInSc1
z	z	k7c2
vody	voda	k1gFnSc2
<g/>
:	:	kIx,
</s>
<s>
H2O	H2O	k4
+	+	kIx~
H	H	kA
<g/>
+	+	kIx~
→	→	k?
H	H	kA
<g/>
3	#num#	k4
<g/>
O	O	kA
<g/>
+	+	kIx~
<g/>
.	.	kIx.
</s>
<s>
Příklady	příklad	k1gInPc1
</s>
<s>
(	(	kIx(
<g/>
jedno	jeden	k4xCgNnSc1
<g/>
)	)	kIx)
<g/>
atomové	atomový	k2eAgInPc1d1
kationty	kation	k1gInPc1
</s>
<s>
sodný	sodný	k2eAgInSc1d1
kation	kation	k1gInSc1
Na	na	k7c6
<g/>
+	+	kIx~
<g/>
I	i	k9
</s>
<s>
vápenatý	vápenatý	k2eAgInSc1d1
kation	kation	k1gInSc1
Ca	ca	kA
<g/>
+	+	kIx~
<g/>
II	II	kA
</s>
<s>
hlinitý	hlinitý	k2eAgInSc1d1
kation	kation	k1gInSc1
Al	ala	k1gFnPc2
<g/>
+	+	kIx~
<g/>
III	III	kA
</s>
<s>
titaničitý	titaničitý	k2eAgInSc4d1
kation	kation	k1gInSc4
Ti	ten	k3xDgMnPc1
<g/>
+	+	kIx~
<g/>
IV	IV	kA
</s>
<s>
molekulové	molekulový	k2eAgInPc1d1
kationty	kation	k1gInPc1
</s>
<s>
amonný	amonný	k2eAgInSc1d1
kation	kation	k1gInSc1
NH	NH	kA
<g/>
4	#num#	k4
<g/>
+	+	kIx~
</s>
<s>
oxoniový	oxoniový	k2eAgInSc1d1
kation	kation	k1gInSc1
H	H	kA
<g/>
3	#num#	k4
<g/>
O	o	k7c4
<g/>
+	+	kIx~
</s>
<s>
uranylový	uranylový	k2eAgInSc1d1
kation	kation	k1gInSc1
UO	UO	kA
<g/>
2	#num#	k4
<g/>
+	+	kIx~
<g/>
II	II	kA
</s>
<s>
Poznámka	poznámka	k1gFnSc1
</s>
<s>
V	v	k7c6
1	#num#	k4
<g/>
.	.	kIx.
pádě	pád	k1gInSc6
jednotného	jednotný	k2eAgNnSc2d1
čísla	číslo	k1gNnSc2
se	s	k7c7
"	"	kIx"
<g/>
t	t	k?
<g/>
"	"	kIx"
na	na	k7c6
konci	konec	k1gInSc6
slova	slovo	k1gNnSc2
kation	kation	k1gInSc1
nepíše	psát	k5eNaImIp3nS
ani	ani	k8xC
nevyslovuje	vyslovovat	k5eNaImIp3nS
<g/>
,	,	kIx,
v	v	k7c6
ostatních	ostatní	k2eAgInPc6d1
pádech	pád	k1gInPc6
a	a	k8xC
v	v	k7c6
množném	množný	k2eAgNnSc6d1
čísle	číslo	k1gNnSc6
však	však	k9
zůstalo	zůstat	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Chemie	chemie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
</s>
