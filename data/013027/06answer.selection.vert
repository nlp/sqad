<s>
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
Universitas	Universitas	k1gInSc1	Universitas
Masarykiana	Masarykiana	k1gFnSc1	Masarykiana
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	let	k1gInPc6	let
1960	[number]	k4	1960
<g/>
–	–	k?	–
<g/>
1990	[number]	k4	1990
Univerzita	univerzita	k1gFnSc1	univerzita
Jana	Jan	k1gMnSc2	Jan
Evangelisty	evangelista	k1gMnSc2	evangelista
Purkyně	Purkyně	k1gFnSc1	Purkyně
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
univerzita	univerzita	k1gFnSc1	univerzita
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
