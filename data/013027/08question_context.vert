<s>
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
Universitas	Universitas	k1gInSc1	Universitas
Masarykiana	Masarykiana	k1gFnSc1	Masarykiana
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	let	k1gInPc6	let
1960	[number]	k4	1960
<g/>
–	–	k?	–
<g/>
1990	[number]	k4	1990
Univerzita	univerzita	k1gFnSc1	univerzita
Jana	Jan	k1gMnSc2	Jan
Evangelisty	evangelista	k1gMnSc2	evangelista
Purkyně	Purkyně	k1gFnSc1	Purkyně
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
univerzita	univerzita	k1gFnSc1	univerzita
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Založena	založen	k2eAgFnSc1d1	založena
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
jako	jako	k9	jako
druhá	druhý	k4xOgFnSc1	druhý
česká	český	k2eAgFnSc1d1	Česká
univerzita	univerzita	k1gFnSc1	univerzita
a	a	k8xC	a
počtem	počet	k1gInSc7	počet
studentů	student	k1gMnPc2	student
v	v	k7c6	v
akreditovaných	akreditovaný	k2eAgInPc6d1	akreditovaný
studijních	studijní	k2eAgInPc6d1	studijní
programech	program	k1gInPc6	program
je	být	k5eAaImIp3nS	být
druhou	druhý	k4xOgFnSc4	druhý
největší	veliký	k2eAgFnSc4d3	veliký
vysokou	vysoký	k2eAgFnSc4d1	vysoká
školu	škola	k1gFnSc4	škola
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
