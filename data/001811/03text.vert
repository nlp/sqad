<s>
Voltaire	Voltair	k1gMnSc5	Voltair
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
François	François	k1gFnSc2	François
Marie	Marie	k1gFnSc1	Marie
Arouet	Arouet	k1gMnSc1	Arouet
(	(	kIx(	(
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1694	[number]	k4	1694
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
-	-	kIx~	-
30	[number]	k4	30
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1778	[number]	k4	1778
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
osvícenský	osvícenský	k2eAgMnSc1d1	osvícenský
francouzský	francouzský	k2eAgMnSc1d1	francouzský
filozof	filozof	k1gMnSc1	filozof
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
kráter	kráter	k1gInSc1	kráter
Voltaire	Voltair	k1gInSc5	Voltair
na	na	k7c6	na
Deimosu	Deimos	k1gInSc3	Deimos
<g/>
,	,	kIx,	,
měsíci	měsíc	k1gInSc3	měsíc
planety	planeta	k1gFnSc2	planeta
Mars	Mars	k1gInSc1	Mars
<g/>
.	.	kIx.	.
</s>
<s>
Vyrostl	vyrůst	k5eAaPmAgMnS	vyrůst
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
notáře	notář	k1gMnSc2	notář
Françoise	Françoise	k1gFnSc2	Françoise
Aroueta	Arouet	k1gMnSc2	Arouet
<g/>
,	,	kIx,	,
sám	sám	k3xTgMnSc1	sám
však	však	k9	však
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gMnSc7	jeho
skutečným	skutečný	k2eAgMnSc7d1	skutečný
otcem	otec	k1gMnSc7	otec
byl	být	k5eAaImAgMnS	být
blíže	blízce	k6eAd2	blízce
neznámý	známý	k2eNgMnSc1d1	neznámý
důstojník	důstojník	k1gMnSc1	důstojník
jménem	jméno	k1gNnSc7	jméno
Rochebrune	Rochebrun	k1gInSc5	Rochebrun
<g/>
.	.	kIx.	.
</s>
<s>
Matku	matka	k1gFnSc4	matka
Marii	Maria	k1gFnSc4	Maria
Marguerite	Marguerit	k1gInSc5	Marguerit
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Aumart	Aumart	k1gInSc1	Aumart
ztratil	ztratit	k5eAaPmAgInS	ztratit
<g/>
,	,	kIx,	,
když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
sedm	sedm	k4xCc4	sedm
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
vztah	vztah	k1gInSc1	vztah
k	k	k7c3	k
otci	otec	k1gMnSc3	otec
i	i	k8xC	i
bratru	bratr	k1gMnSc3	bratr
Armandovi	Armand	k1gMnSc3	Armand
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
toho	ten	k3xDgNnSc2	ten
jinak	jinak	k6eAd1	jinak
celkem	celkem	k6eAd1	celkem
nic	nic	k3yNnSc1	nic
neví	vědět	k5eNaImIp3nS	vědět
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
chladný	chladný	k2eAgInSc1d1	chladný
a	a	k8xC	a
lhostejný	lhostejný	k2eAgInSc1d1	lhostejný
<g/>
.	.	kIx.	.
</s>
<s>
Skutečným	skutečný	k2eAgMnSc7d1	skutečný
vychovatelem	vychovatel	k1gMnSc7	vychovatel
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
stal	stát	k5eAaPmAgInS	stát
jeho	on	k3xPp3gMnSc4	on
křestní	křestní	k2eAgMnSc1d1	křestní
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
abbé	abbé	k1gMnSc1	abbé
de	de	k?	de
Châteauneuf	Châteauneuf	k1gMnSc1	Châteauneuf
<g/>
,	,	kIx,	,
skeptik	skeptik	k1gMnSc1	skeptik
<g/>
,	,	kIx,	,
požitkář	požitkář	k1gMnSc1	požitkář
a	a	k8xC	a
milovník	milovník	k1gMnSc1	milovník
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgMnSc1	tento
kněz	kněz	k1gMnSc1	kněz
jej	on	k3xPp3gMnSc4	on
také	také	k6eAd1	také
uvedl	uvést	k5eAaPmAgMnS	uvést
do	do	k7c2	do
vyšší	vysoký	k2eAgFnSc2d2	vyšší
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
když	když	k8xS	když
jej	on	k3xPp3gMnSc4	on
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
vlivnou	vlivný	k2eAgFnSc7d1	vlivná
kurtizánou	kurtizána	k1gFnSc7	kurtizána
Ninon	Ninona	k1gFnPc2	Ninona
de	de	k?	de
Lenclos	Lenclosa	k1gFnPc2	Lenclosa
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
tehdy	tehdy	k6eAd1	tehdy
bylo	být	k5eAaImAgNnS	být
84	[number]	k4	84
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
s	s	k7c7	s
výborným	výborný	k2eAgInSc7d1	výborný
prospěchem	prospěch	k1gInSc7	prospěch
jezuitskou	jezuitský	k2eAgFnSc4d1	jezuitská
Kolej	kolej	k1gFnSc4	kolej
Ludvíka	Ludvík	k1gMnSc2	Ludvík
Velikého	veliký	k2eAgMnSc2d1	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
dojem	dojem	k1gInSc1	dojem
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
učinily	učinit	k5eAaImAgInP	učinit
poslední	poslední	k2eAgInPc1d1	poslední
roky	rok	k1gInPc1	rok
vlády	vláda	k1gFnSc2	vláda
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
náboženské	náboženský	k2eAgNnSc4d1	náboženské
pronásledování	pronásledování	k1gNnSc4	pronásledování
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
dokončil	dokončit	k5eAaPmAgMnS	dokončit
studia	studio	k1gNnSc2	studio
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
tajemníkem	tajemník	k1gMnSc7	tajemník
francouzského	francouzský	k2eAgNnSc2d1	francouzské
velvyslanectví	velvyslanectví	k1gNnSc2	velvyslanectví
v	v	k7c6	v
Haagu	Haag	k1gInSc6	Haag
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
však	však	k9	však
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
padlo	padnout	k5eAaImAgNnS	padnout
podezření	podezření	k1gNnSc1	podezření
z	z	k7c2	z
milostné	milostný	k2eAgFnSc2d1	milostná
pletky	pletka	k1gFnSc2	pletka
s	s	k7c7	s
velvyslancovou	velvyslancův	k2eAgFnSc7d1	velvyslancova
dcerou	dcera	k1gFnSc7	dcera
<g/>
,	,	kIx,	,
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
odeslán	odeslat	k5eAaPmNgInS	odeslat
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
záhy	záhy	k6eAd1	záhy
stal	stát	k5eAaPmAgInS	stát
známým	známý	k2eAgMnSc7d1	známý
autorem	autor	k1gMnSc7	autor
epigramů	epigram	k1gInPc2	epigram
a	a	k8xC	a
posměšných	posměšný	k2eAgInPc2d1	posměšný
pamfletů	pamflet	k1gInPc2	pamflet
<g/>
.	.	kIx.	.
</s>
<s>
Přestřelil	přestřelit	k5eAaPmAgMnS	přestřelit
ovšem	ovšem	k9	ovšem
sepsáním	sepsání	k1gNnSc7	sepsání
posměšku	posměšek	k1gInSc2	posměšek
na	na	k7c4	na
účet	účet	k1gInSc4	účet
vévody	vévoda	k1gMnSc2	vévoda
Filipa	Filip	k1gMnSc2	Filip
II	II	kA	II
<g/>
.	.	kIx.	.
z	z	k7c2	z
Orleánsu	Orleáns	k1gInSc2	Orleáns
<g/>
,	,	kIx,	,
po	po	k7c6	po
jehož	jehož	k3xOyRp3gInSc6	jehož
zásahu	zásah	k1gInSc6	zásah
byl	být	k5eAaImAgInS	být
téměř	téměř	k6eAd1	téměř
na	na	k7c4	na
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
1717	[number]	k4	1717
uvězněn	uvěznit	k5eAaPmNgMnS	uvěznit
v	v	k7c6	v
Bastile	Bastila	k1gFnSc6	Bastila
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
však	však	k9	však
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
omylu	omyl	k1gInSc3	omyl
<g/>
:	:	kIx,	:
vévoda	vévoda	k1gMnSc1	vévoda
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
vděčným	vděčný	k2eAgInSc7d1	vděčný
terčem	terč	k1gInSc7	terč
satiry	satira	k1gFnSc2	satira
a	a	k8xC	a
Voltaire	Voltair	k1gInSc5	Voltair
strávil	strávit	k5eAaPmAgMnS	strávit
11	[number]	k4	11
měsíců	měsíc	k1gInPc2	měsíc
v	v	k7c6	v
žaláři	žalář	k1gInSc6	žalář
za	za	k7c4	za
spisek	spisek	k1gInSc4	spisek
z	z	k7c2	z
pera	pero	k1gNnSc2	pero
jiného	jiný	k2eAgNnSc2d1	jiné
<g/>
,	,	kIx,	,
neodhaleného	odhalený	k2eNgMnSc2d1	neodhalený
autora	autor	k1gMnSc2	autor
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
rovněž	rovněž	k9	rovněž
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
své	svůj	k3xOyFgNnSc4	svůj
autorské	autorský	k2eAgNnSc4d1	autorské
jméno	jméno	k1gNnSc4	jméno
<g/>
,	,	kIx,	,
údajně	údajně	k6eAd1	údajně
přesmyčkou	přesmyčka	k1gFnSc7	přesmyčka
písmen	písmeno	k1gNnPc2	písmeno
latinského	latinský	k2eAgInSc2d1	latinský
zápisu	zápis	k1gInSc2	zápis
jeho	jeho	k3xOp3gNnSc2	jeho
vlastního	vlastní	k2eAgNnSc2d1	vlastní
jména	jméno	k1gNnSc2	jméno
<g/>
,	,	kIx,	,
Arovet	Arovet	k1gMnSc1	Arovet
L.I.	L.I.	k1gMnSc1	L.I.
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
le	le	k?	le
jeune	jeunout	k5eAaPmIp3nS	jeunout
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
mladší	mladý	k2eAgFnSc1d2	mladší
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k9	už
jeho	jeho	k3xOp3gNnSc1	jeho
první	první	k4xOgNnSc1	první
drama	drama	k1gNnSc1	drama
Oidipus	Oidipus	k1gMnSc1	Oidipus
(	(	kIx(	(
<g/>
1719	[number]	k4	1719
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
setkalo	setkat	k5eAaPmAgNnS	setkat
se	s	k7c7	s
značným	značný	k2eAgInSc7d1	značný
ohlasem	ohlas	k1gInSc7	ohlas
<g/>
.	.	kIx.	.
</s>
<s>
Povzbuzen	povzbuzen	k2eAgInSc1d1	povzbuzen
chválou	chvála	k1gFnSc7	chvála
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgInS	začít
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
poémě	poéma	k1gFnSc6	poéma
Henriada	Henriada	k1gFnSc1	Henriada
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
díle	dílo	k1gNnSc6	dílo
zůstala	zůstat	k5eAaPmAgFnS	zůstat
jako	jako	k9	jako
literární	literární	k2eAgInSc4d1	literární
poklesek	poklesek	k1gInSc4	poklesek
imitující	imitující	k2eAgInSc4d1	imitující
styl	styl	k1gInSc4	styl
antických	antický	k2eAgFnPc2d1	antická
poém	poéma	k1gFnPc2	poéma
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
v	v	k7c6	v
pařížských	pařížský	k2eAgInPc6d1	pařížský
salónech	salón	k1gInPc6	salón
tříbil	tříbit	k5eAaImAgInS	tříbit
jeho	jeho	k3xOp3gInSc1	jeho
politický	politický	k2eAgInSc1d1	politický
i	i	k8xC	i
náboženský	náboženský	k2eAgInSc1d1	náboženský
světonázor	světonázor	k1gInSc1	světonázor
<g/>
,	,	kIx,	,
rostlo	růst	k5eAaImAgNnS	růst
Voltairovo	Voltairův	k2eAgNnSc1d1	Voltairovo
zaujetí	zaujetí	k1gNnSc1	zaujetí
svobodomyslnou	svobodomyslný	k2eAgFnSc7d1	svobodomyslná
cizinou	cizina	k1gFnSc7	cizina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nastavovala	nastavovat	k5eAaImAgFnS	nastavovat
zrcadlo	zrcadlo	k1gNnSc4	zrcadlo
nespravedlným	spravedlný	k2eNgMnPc3d1	spravedlný
sociálním	sociální	k2eAgMnPc3d1	sociální
pořádkům	pořádek	k1gInPc3	pořádek
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
rodné	rodný	k2eAgFnSc6d1	rodná
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
při	při	k7c6	při
druhém	druhý	k4xOgInSc6	druhý
delším	dlouhý	k2eAgInSc6d2	delší
pobytu	pobyt	k1gInSc6	pobyt
v	v	k7c6	v
Holandsku	Holandsko	k1gNnSc6	Holandsko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1722	[number]	k4	1722
zakusil	zakusit	k5eAaPmAgMnS	zakusit
ducha	duch	k1gMnSc4	duch
tolerance	tolerance	k1gFnSc2	tolerance
a	a	k8xC	a
svobodného	svobodný	k2eAgNnSc2d1	svobodné
podnikání	podnikání	k1gNnSc2	podnikání
<g/>
,	,	kIx,	,
dostalo	dostat	k5eAaPmAgNnS	dostat
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
zde	zde	k6eAd1	zde
i	i	k8xC	i
vřelého	vřelý	k2eAgNnSc2d1	vřelé
přijetí	přijetí	k1gNnSc2	přijetí
v	v	k7c6	v
literárních	literární	k2eAgInPc6d1	literární
<g/>
,	,	kIx,	,
obchodních	obchodní	k2eAgInPc6d1	obchodní
a	a	k8xC	a
šlechtických	šlechtický	k2eAgInPc6d1	šlechtický
kruzích	kruh	k1gInPc6	kruh
<g/>
.	.	kIx.	.
</s>
<s>
Zcela	zcela	k6eAd1	zcela
pak	pak	k6eAd1	pak
propadl	propadnout	k5eAaPmAgMnS	propadnout
kouzlu	kouzlo	k1gNnSc3	kouzlo
Británie	Británie	k1gFnSc2	Británie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
dobu	doba	k1gFnSc4	doba
mimořádně	mimořádně	k6eAd1	mimořádně
tolerantní	tolerantní	k2eAgMnSc1d1	tolerantní
a	a	k8xC	a
nábožensky	nábožensky	k6eAd1	nábožensky
nezávislá	závislý	k2eNgFnSc1d1	nezávislá
na	na	k7c6	na
vlivu	vliv	k1gInSc6	vliv
Říma	Řím	k1gInSc2	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
se	se	k3xPyFc4	se
učit	učit	k5eAaImF	učit
anglicky	anglicky	k6eAd1	anglicky
<g/>
,	,	kIx,	,
navštívil	navštívit	k5eAaPmAgMnS	navštívit
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
toryovského	toryovský	k2eAgMnSc2d1	toryovský
politika	politik	k1gMnSc2	politik
a	a	k8xC	a
filozofa	filozof	k1gMnSc2	filozof
vikomta	vikomt	k1gMnSc2	vikomt
Bolingbroka	Bolingbroek	k1gMnSc2	Bolingbroek
(	(	kIx(	(
<g/>
1678	[number]	k4	1678
<g/>
-	-	kIx~	-
<g/>
1751	[number]	k4	1751
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
učinil	učinit	k5eAaPmAgInS	učinit
ohromný	ohromný	k2eAgInSc1d1	ohromný
dojem	dojem	k1gInSc1	dojem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1726	[number]	k4	1726
se	se	k3xPyFc4	se
Voltaire	Voltair	k1gInSc5	Voltair
opět	opět	k6eAd1	opět
zapletl	zaplést	k5eAaPmAgInS	zaplést
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
vtip	vtip	k1gInSc1	vtip
tentokrát	tentokrát	k6eAd1	tentokrát
trefil	trefit	k5eAaPmAgInS	trefit
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejmocnějších	mocný	k2eAgMnPc2d3	nejmocnější
příslušníků	příslušník	k1gMnPc2	příslušník
pařížské	pařížský	k2eAgFnSc2d1	Pařížská
nobility	nobilita	k1gFnSc2	nobilita
<g/>
,	,	kIx,	,
známého	známý	k2eAgMnSc2d1	známý
knížete	kníže	k1gMnSc2	kníže
Rohana	Rohan	k1gMnSc2	Rohan
<g/>
.	.	kIx.	.
</s>
<s>
Voltaire	Voltair	k1gMnSc5	Voltair
byl	být	k5eAaImAgInS	být
zbit	zbít	k5eAaPmNgMnS	zbít
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
vržen	vrhnout	k5eAaPmNgMnS	vrhnout
do	do	k7c2	do
Bastily	Bastila	k1gFnSc2	Bastila
a	a	k8xC	a
vypovězen	vypovědět	k5eAaPmNgMnS	vypovědět
z	z	k7c2	z
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Logickým	logický	k2eAgInSc7d1	logický
cílem	cíl	k1gInSc7	cíl
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Británie	Británie	k1gFnSc1	Británie
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
odcestoval	odcestovat	k5eAaPmAgMnS	odcestovat
5	[number]	k4	5
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1726	[number]	k4	1726
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
téměř	téměř	k6eAd1	téměř
tříletého	tříletý	k2eAgInSc2d1	tříletý
pobytu	pobyt	k1gInSc2	pobyt
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
(	(	kIx(	(
<g/>
1726	[number]	k4	1726
<g/>
-	-	kIx~	-
<g/>
1729	[number]	k4	1729
<g/>
)	)	kIx)	)
poznal	poznat	k5eAaPmAgMnS	poznat
anglickou	anglický	k2eAgFnSc4d1	anglická
společnost	společnost	k1gFnSc4	společnost
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
významné	významný	k2eAgFnSc2d1	významná
osobnosti	osobnost	k1gFnSc2	osobnost
<g/>
,	,	kIx,	,
Popea	Pope	k1gInSc2	Pope
a	a	k8xC	a
Swifta	Swift	k1gInSc2	Swift
<g/>
,	,	kIx,	,
studoval	studovat	k5eAaImAgMnS	studovat
anglickou	anglický	k2eAgFnSc4d1	anglická
filozofii	filozofie	k1gFnSc4	filozofie
<g/>
,	,	kIx,	,
vědu	věda	k1gFnSc4	věda
i	i	k8xC	i
písemnictví	písemnictví	k1gNnSc4	písemnictví
<g/>
,	,	kIx,	,
četl	číst	k5eAaImAgMnS	číst
Locka	Locek	k1gMnSc4	Locek
<g/>
,	,	kIx,	,
Bacona	Bacon	k1gMnSc4	Bacon
<g/>
,	,	kIx,	,
Berkeleye	Berkeley	k1gMnSc4	Berkeley
<g/>
,	,	kIx,	,
Newtona	Newton	k1gMnSc4	Newton
<g/>
,	,	kIx,	,
studoval	studovat	k5eAaImAgMnS	studovat
trochu	trochu	k6eAd1	trochu
matematiky	matematik	k1gMnPc4	matematik
a	a	k8xC	a
mechaniky	mechanik	k1gMnPc4	mechanik
<g/>
,	,	kIx,	,
zajímal	zajímat	k5eAaImAgInS	zajímat
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
o	o	k7c4	o
shakespearovské	shakespearovský	k2eAgNnSc4d1	shakespearovské
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
se	se	k3xPyFc4	se
realizuje	realizovat	k5eAaBmIp3nS	realizovat
především	především	k9	především
jako	jako	k9	jako
dramatik	dramatik	k1gMnSc1	dramatik
<g/>
,	,	kIx,	,
zajímá	zajímat	k5eAaImIp3nS	zajímat
se	se	k3xPyFc4	se
také	také	k9	také
o	o	k7c4	o
dějiny	dějiny	k1gFnPc4	dějiny
a	a	k8xC	a
píše	psát	k5eAaImIp3nS	psát
Historii	historie	k1gFnSc4	historie
Karla	Karel	k1gMnSc2	Karel
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnPc1	jeho
dramata	drama	k1gNnPc1	drama
(	(	kIx(	(
<g/>
Smrt	smrt	k1gFnSc1	smrt
Césara	César	k1gMnSc2	César
<g/>
,	,	kIx,	,
Zaira	Zair	k1gMnSc2	Zair
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
měla	mít	k5eAaImAgFnS	mít
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
velký	velký	k2eAgInSc4d1	velký
ohlas	ohlas	k1gInSc4	ohlas
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
jsou	být	k5eAaImIp3nP	být
nicméně	nicméně	k8xC	nicméně
pozapomenuta	pozapomenout	k5eAaPmNgFnS	pozapomenout
jen	jen	k6eAd1	jen
s	s	k7c7	s
malou	malý	k2eAgFnSc7d1	malá
nadějí	naděje	k1gFnSc7	naděje
na	na	k7c4	na
opětovné	opětovný	k2eAgNnSc4d1	opětovné
vzkříšení	vzkříšení	k1gNnSc4	vzkříšení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1734	[number]	k4	1734
Voltaire	Voltair	k1gInSc5	Voltair
veřejně	veřejně	k6eAd1	veřejně
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
jako	jako	k9	jako
nezávislý	závislý	k2eNgMnSc1d1	nezávislý
myslitel	myslitel	k1gMnSc1	myslitel
otištěním	otištění	k1gNnSc7	otištění
Filosofických	filosofický	k2eAgInPc2d1	filosofický
listů	list	k1gInPc2	list
o	o	k7c6	o
Angličanech	Angličan	k1gMnPc6	Angličan
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
hlavním	hlavní	k2eAgInSc7d1	hlavní
terčem	terč	k1gInSc7	terč
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
nesvoboda	nesvoboda	k1gFnSc1	nesvoboda
<g/>
,	,	kIx,	,
politický	politický	k2eAgInSc4d1	politický
útlak	útlak	k1gInSc4	útlak
a	a	k8xC	a
náboženské	náboženský	k2eAgNnSc4d1	náboženské
tmářství	tmářství	k1gNnSc4	tmářství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
podobném	podobný	k2eAgMnSc6d1	podobný
duchu	duch	k1gMnSc6	duch
jako	jako	k8xC	jako
Hobbes	Hobbes	k1gInSc4	Hobbes
i	i	k8xC	i
Voltaire	Voltair	k1gInSc5	Voltair
útočí	útočit	k5eAaImIp3nS	útočit
na	na	k7c4	na
scholastickou	scholastický	k2eAgFnSc4d1	scholastická
metafyziku	metafyzika	k1gFnSc4	metafyzika
<g/>
,	,	kIx,	,
zařazuje	zařazovat	k5eAaImIp3nS	zařazovat
i	i	k9	i
nesmrtelný	smrtelný	k2eNgInSc1d1	nesmrtelný
výpad	výpad	k1gInSc1	výpad
proti	proti	k7c3	proti
tehdy	tehdy	k6eAd1	tehdy
populární	populární	k2eAgFnSc3d1	populární
filosofii	filosofie	k1gFnSc3	filosofie
Pascalově	Pascalův	k2eAgFnSc3d1	Pascalova
<g/>
.	.	kIx.	.
</s>
<s>
Encyclopæ	Encyclopæ	k?	Encyclopæ
Britannica	Britannica	k1gFnSc1	Britannica
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
touto	tento	k3xDgFnSc7	tento
knihou	kniha	k1gFnSc7	kniha
určil	určit	k5eAaPmAgInS	určit
podstatné	podstatný	k2eAgInPc4d1	podstatný
rysy	rys	k1gInPc4	rys
a	a	k8xC	a
směr	směr	k1gInSc4	směr
moderního	moderní	k2eAgNnSc2d1	moderní
myšlení	myšlení	k1gNnSc2	myšlení
<g/>
.	.	kIx.	.
</s>
<s>
Filosofické	filosofický	k2eAgInPc1d1	filosofický
listy	list	k1gInPc1	list
vyvolaly	vyvolat	k5eAaPmAgInP	vyvolat
skandál	skandál	k1gInSc1	skandál
<g/>
:	:	kIx,	:
pařížský	pařížský	k2eAgInSc1d1	pařížský
parlament	parlament	k1gInSc1	parlament
nařídil	nařídit	k5eAaPmAgInS	nařídit
jejich	jejich	k3xOp3gNnSc4	jejich
spálení	spálení	k1gNnSc4	spálení
a	a	k8xC	a
vypátrání	vypátrání	k1gNnSc4	vypátrání
dosud	dosud	k6eAd1	dosud
anonymního	anonymní	k2eAgMnSc2d1	anonymní
autora	autor	k1gMnSc2	autor
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
těchto	tento	k3xDgFnPc2	tento
okolností	okolnost	k1gFnPc2	okolnost
našel	najít	k5eAaPmAgInS	najít
Voltaire	Voltair	k1gInSc5	Voltair
útočiště	útočiště	k1gNnSc4	útočiště
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
markýzy	markýza	k1gFnSc2	markýza
de	de	k?	de
Châtelet	Châtelet	k1gInSc1	Châtelet
na	na	k7c6	na
pomezí	pomezí	k1gNnSc6	pomezí
Champagne	Champagn	k1gInSc5	Champagn
a	a	k8xC	a
Lotrinska	Lotrinsko	k1gNnSc2	Lotrinsko
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
velmi	velmi	k6eAd1	velmi
půvabná	půvabný	k2eAgFnSc1d1	půvabná
žena	žena	k1gFnSc1	žena
<g/>
,	,	kIx,	,
studovaná	studovaný	k2eAgFnSc1d1	studovaná
v	v	k7c6	v
matematice	matematika	k1gFnSc6	matematika
a	a	k8xC	a
fyzice	fyzika	k1gFnSc6	fyzika
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
jeho	jeho	k3xOp3gFnSc7	jeho
dlouholetou	dlouholetý	k2eAgFnSc7d1	dlouholetá
múzou	múza	k1gFnSc7	múza
a	a	k8xC	a
ochránkyní	ochránkyně	k1gFnSc7	ochránkyně
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
sepsal	sepsat	k5eAaPmAgMnS	sepsat
mj.	mj.	kA	mj.
své	svůj	k3xOyFgNnSc4	svůj
drama	drama	k1gNnSc4	drama
Mohamed	Mohamed	k1gMnSc1	Mohamed
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejprudších	prudký	k2eAgMnPc2d3	nejprudší
útoků	útok	k1gInPc2	útok
na	na	k7c4	na
zjevená	zjevený	k2eAgNnPc4d1	zjevené
náboženství	náboženství	k1gNnPc4	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
odvrátil	odvrátit	k5eAaPmAgMnS	odvrátit
útoky	útok	k1gInPc4	útok
jezuitů	jezuita	k1gMnPc2	jezuita
<g/>
,	,	kIx,	,
věnoval	věnovat	k5eAaImAgMnS	věnovat
autor	autor	k1gMnSc1	autor
své	svůj	k3xOyFgNnSc4	svůj
dílo	dílo	k1gNnSc4	dílo
pokrytecky	pokrytecky	k6eAd1	pokrytecky
papeži	papež	k1gMnSc3	papež
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
i	i	k9	i
Voltairova	Voltairův	k2eAgFnSc1d1	Voltairova
La	la	k1gNnSc4	la
Pucelle	Pucelle	k1gInSc1	Pucelle
<g/>
,	,	kIx,	,
rouhavá	rouhavý	k2eAgFnSc1d1	rouhavá
satira	satira	k1gFnSc1	satira
na	na	k7c4	na
účet	účet	k1gInSc4	účet
francouzské	francouzský	k2eAgFnSc2d1	francouzská
národní	národní	k2eAgFnSc2d1	národní
hrdinky	hrdinka	k1gFnSc2	hrdinka
Johanky	Johanka	k1gFnSc2	Johanka
z	z	k7c2	z
Arku	arkus	k1gInSc2	arkus
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc7	který
se	se	k3xPyFc4	se
myslitel	myslitel	k1gMnSc1	myslitel
osmělil	osmělit	k5eAaPmAgMnS	osmělit
uveřejnit	uveřejnit	k5eAaPmF	uveřejnit
až	až	k9	až
po	po	k7c6	po
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Legenda	legenda	k1gFnSc1	legenda
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Voltaire	Voltair	k1gInSc5	Voltair
měl	mít	k5eAaImAgInS	mít
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
vždy	vždy	k6eAd1	vždy
připravené	připravený	k2eAgMnPc4d1	připravený
koně	kůň	k1gMnPc4	kůň
pro	pro	k7c4	pro
případný	případný	k2eAgInSc4d1	případný
útěk	útěk	k1gInSc4	útěk
(	(	kIx(	(
<g/>
Voltairovi	Voltair	k1gMnSc3	Voltair
se	se	k3xPyFc4	se
ostatně	ostatně	k6eAd1	ostatně
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gInSc4	jeho
neklidný	klidný	k2eNgInSc4d1	neklidný
život	život	k1gInSc4	život
poznamenaný	poznamenaný	k2eAgInSc4d1	poznamenaný
po	po	k7c4	po
téměř	téměř	k6eAd1	téměř
60	[number]	k4	60
let	léto	k1gNnPc2	léto
vynucenými	vynucený	k2eAgFnPc7d1	vynucená
změnami	změna	k1gFnPc7	změna
působiště	působiště	k1gNnSc2	působiště
přezdívalo	přezdívat	k5eAaImAgNnS	přezdívat
"	"	kIx"	"
<g/>
létající	létající	k2eAgMnSc1d1	létající
filosof	filosof	k1gMnSc1	filosof
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtyřicátých	čtyřicátý	k4xOgNnPc6	čtyřicátý
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Voltairova	Voltairův	k2eAgFnSc1d1	Voltairova
sláva	sláva	k1gFnSc1	sláva
a	a	k8xC	a
rostoucí	rostoucí	k2eAgFnSc1d1	rostoucí
prestiž	prestiž	k1gFnSc1	prestiž
změnila	změnit	k5eAaPmAgFnS	změnit
jeho	on	k3xPp3gInSc4	on
poměr	poměr	k1gInSc4	poměr
ke	k	k7c3	k
dvoru	dvůr	k1gInSc3	dvůr
(	(	kIx(	(
<g/>
směl	smět	k5eAaImAgMnS	smět
se	se	k3xPyFc4	se
vrátit	vrátit	k5eAaPmF	vrátit
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgInPc4d1	jiný
i	i	k9	i
členem	člen	k1gInSc7	člen
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
akademie	akademie	k1gFnSc2	akademie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
navázal	navázat	k5eAaPmAgInS	navázat
kontakt	kontakt	k1gInSc4	kontakt
s	s	k7c7	s
pruským	pruský	k2eAgMnSc7d1	pruský
princem	princ	k1gMnSc7	princ
Fridrichem	Fridrich	k1gMnSc7	Fridrich
<g/>
,	,	kIx,	,
pozdějším	pozdní	k2eAgMnSc7d2	pozdější
králem	král	k1gMnSc7	král
vší	všecek	k3xTgFnSc6	všecek
Prusi	Pruse	k1gFnSc6	Pruse
Fridrichem	Fridrich	k1gMnSc7	Fridrich
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Velikým	veliký	k2eAgInSc7d1	veliký
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gInSc6	jehož
dvoře	dvůr	k1gInSc6	dvůr
v	v	k7c6	v
Postupimi	Postupim	k1gFnSc6	Postupim
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1750	[number]	k4	1750
<g/>
-	-	kIx~	-
<g/>
1753	[number]	k4	1753
<g/>
.	.	kIx.	.
</s>
<s>
Obdržel	obdržet	k5eAaPmAgInS	obdržet
zde	zde	k6eAd1	zde
vysokou	vysoký	k2eAgFnSc4d1	vysoká
královskou	královský	k2eAgFnSc4d1	královská
penzi	penze	k1gFnSc4	penze
a	a	k8xC	a
okouzlen	okouzlen	k2eAgInSc1d1	okouzlen
svobodným	svobodný	k2eAgNnSc7d1	svobodné
prostředím	prostředí	k1gNnSc7	prostředí
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgMnS	věnovat
divadlu	divadlo	k1gNnSc3	divadlo
a	a	k8xC	a
psaní	psaní	k1gNnSc3	psaní
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
spolupráce	spolupráce	k1gFnSc1	spolupráce
však	však	k9	však
skončila	skončit	k5eAaPmAgFnS	skončit
roztržkou	roztržka	k1gFnSc7	roztržka
a	a	k8xC	a
definitivním	definitivní	k2eAgNnSc7d1	definitivní
odcizením	odcizení	k1gNnSc7	odcizení
filosofa	filosof	k1gMnSc2	filosof
a	a	k8xC	a
monarchy	monarcha	k1gMnSc2	monarcha
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
svého	svůj	k3xOyFgMnSc4	svůj
hosta	host	k1gMnSc4	host
v	v	k7c6	v
narážce	narážka	k1gFnSc6	narážka
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
dohasínající	dohasínající	k2eAgFnSc4d1	dohasínající
slávu	sláva	k1gFnSc4	sláva
nazval	nazvat	k5eAaBmAgInS	nazvat
vymačkaným	vymačkaný	k2eAgInSc7d1	vymačkaný
pomerančem	pomeranč	k1gInSc7	pomeranč
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc1	jehož
slupku	slupka	k1gFnSc4	slupka
po	po	k7c6	po
vypití	vypití	k1gNnSc6	vypití
šťávy	šťáva	k1gFnSc2	šťáva
odhodíme	odhodit	k5eAaPmIp1nP	odhodit
<g/>
.	.	kIx.	.
</s>
<s>
Voltaire	Voltair	k1gMnSc5	Voltair
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
<g/>
,	,	kIx,	,
že	že	k8xS	že
panovníkovy	panovníkův	k2eAgInPc1d1	panovníkův
"	"	kIx"	"
<g/>
dobré	dobrý	k2eAgInPc1d1	dobrý
úmysly	úmysl	k1gInPc1	úmysl
ulétaly	ulétat	k5eAaImAgInP	ulétat
již	již	k6eAd1	již
při	při	k7c6	při
prvním	první	k4xOgNnSc6	první
zaznění	zaznění	k1gNnSc6	zaznění
polnice	polnice	k1gFnSc1	polnice
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Voltairova	Voltairův	k2eAgFnSc1d1	Voltairova
pozice	pozice	k1gFnSc1	pozice
u	u	k7c2	u
pařížského	pařížský	k2eAgInSc2d1	pařížský
dvora	dvůr	k1gInSc2	dvůr
byla	být	k5eAaImAgFnS	být
velmi	velmi	k6eAd1	velmi
vratká	vratký	k2eAgFnSc1d1	vratká
až	až	k9	až
do	do	k7c2	do
jeho	jeho	k3xOp3gFnSc2	jeho
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
přes	přes	k7c4	přes
podporu	podpora	k1gFnSc4	podpora
a	a	k8xC	a
ochranu	ochrana	k1gFnSc4	ochrana
mocné	mocný	k2eAgFnSc2d1	mocná
markýzy	markýza	k1gFnSc2	markýza
de	de	k?	de
Pompadour	Pompadour	k1gMnSc1	Pompadour
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
zřejmě	zřejmě	k6eAd1	zřejmě
tou	ten	k3xDgFnSc7	ten
pravou	pravý	k2eAgFnSc7d1	pravá
překážkou	překážka	k1gFnSc7	překážka
<g/>
,	,	kIx,	,
stojící	stojící	k2eAgMnSc1d1	stojící
francouzským	francouzský	k2eAgMnPc3d1	francouzský
katolickým	katolický	k2eAgMnPc3d1	katolický
kruhům	kruh	k1gInPc3	kruh
v	v	k7c6	v
cestě	cesta	k1gFnSc6	cesta
k	k	k7c3	k
Voltairovu	Voltairův	k2eAgNnSc3d1	Voltairovo
zavraždění	zavraždění	k1gNnSc3	zavraždění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1764	[number]	k4	1764
publikuje	publikovat	k5eAaBmIp3nS	publikovat
svůj	svůj	k3xOyFgInSc4	svůj
Filosofický	filosofický	k2eAgInSc4d1	filosofický
slovník	slovník	k1gInSc4	slovník
<g/>
,	,	kIx,	,
z	z	k7c2	z
části	část	k1gFnSc2	část
založený	založený	k2eAgInSc4d1	založený
na	na	k7c6	na
vyprávěních	vyprávění	k1gNnPc6	vyprávění
přednášených	přednášený	k2eAgNnPc2d1	přednášené
k	k	k7c3	k
pobavení	pobavení	k1gNnSc3	pobavení
společnosti	společnost	k1gFnSc2	společnost
na	na	k7c6	na
pruském	pruský	k2eAgInSc6d1	pruský
císařském	císařský	k2eAgInSc6d1	císařský
dvoře	dvůr	k1gInSc6	dvůr
<g/>
,	,	kIx,	,
a	a	k8xC	a
historie	historie	k1gFnSc1	historie
Filosofických	filosofický	k2eAgInPc2d1	filosofický
listů	list	k1gInPc2	list
se	se	k3xPyFc4	se
opakuje	opakovat	k5eAaImIp3nS	opakovat
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Sotva	sotva	k6eAd1	sotva
byl	být	k5eAaImAgInS	být
slovník	slovník	k1gInSc1	slovník
vytištěn	vytištěn	k2eAgInSc1d1	vytištěn
<g/>
,	,	kIx,	,
vzbudil	vzbudit	k5eAaPmAgInS	vzbudit
takový	takový	k3xDgInSc1	takový
rozruch	rozruch	k1gInSc1	rozruch
mezi	mezi	k7c7	mezi
vzdělanci	vzdělanec	k1gMnPc7	vzdělanec
a	a	k8xC	a
odpor	odpor	k1gInSc4	odpor
ve	v	k7c6	v
vládních	vládní	k2eAgInPc6d1	vládní
a	a	k8xC	a
kněžských	kněžský	k2eAgInPc6d1	kněžský
kruzích	kruh	k1gInPc6	kruh
<g/>
,	,	kIx,	,
že	že	k8xS	že
tím	ten	k3xDgNnSc7	ten
byl	být	k5eAaImAgMnS	být
Voltaire	Voltair	k1gInSc5	Voltair
zastrašen	zastrašit	k5eAaPmNgMnS	zastrašit
a	a	k8xC	a
neváhal	váhat	k5eNaImAgMnS	váhat
své	svůj	k3xOyFgNnSc4	svůj
autorství	autorství	k1gNnSc4	autorství
zapříti	zapřít	k5eAaPmF	zapřít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
svém	své	k1gNnSc6	své
dopise	dopis	k1gInSc6	dopis
paní	paní	k1gFnSc2	paní
de	de	k?	de
Deffand	Deffand	k1gInSc1	Deffand
píše	psát	k5eAaImIp3nS	psát
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Kdybych	kdyby	kYmCp1nS	kdyby
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
obviněn	obvinit	k5eAaPmNgMnS	obvinit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsem	být	k5eAaImIp1nS	být
napsal	napsat	k5eAaBmAgMnS	napsat
Filozofický	filozofický	k2eAgInSc4d1	filozofický
slovník	slovník	k1gInSc4	slovník
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
by	by	kYmCp3nS	by
mi	já	k3xPp1nSc3	já
lépe	dobře	k6eAd2	dobře
<g/>
,	,	kIx,	,
kdybych	kdyby	kYmCp1nS	kdyby
se	se	k3xPyFc4	se
nenarodil	narodit	k5eNaPmAgMnS	narodit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
A	a	k9	a
na	na	k7c6	na
jiném	jiný	k2eAgNnSc6d1	jiné
místě	místo	k1gNnSc6	místo
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Alembertovi	Alembert	k1gMnSc3	Alembert
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g />
.	.	kIx.	.
</s>
<s>
<g/>
A	a	k9	a
když	když	k8xS	když
již	již	k6eAd1	již
mluvíme	mluvit	k5eAaImIp1nP	mluvit
o	o	k7c6	o
morálce	morálka	k1gFnSc6	morálka
<g/>
:	:	kIx,	:
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
zpropadeném	zpropadený	k2eAgInSc6d1	zpropadený
slovníku	slovník	k1gInSc6	slovník
je	být	k5eAaImIp3nS	být
jí	on	k3xPp3gFnSc3	on
tolik	tolik	k6eAd1	tolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
třesu	třást	k5eAaImIp1nS	třást
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dílo	dílo	k1gNnSc1	dílo
i	i	k8xC	i
autor	autor	k1gMnSc1	autor
nebyli	být	k5eNaImAgMnP	být
upáleni	upálit	k5eAaPmNgMnP	upálit
přičiněním	přičinění	k1gNnSc7	přičinění
nepřátel	nepřítel	k1gMnPc2	nepřítel
morálky	morálka	k1gFnSc2	morálka
a	a	k8xC	a
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
září	září	k1gNnSc6	září
1764	[number]	k4	1764
byl	být	k5eAaImAgInS	být
Filozofický	filozofický	k2eAgInSc1d1	filozofický
slovník	slovník	k1gInSc1	slovník
v	v	k7c6	v
Ženevě	Ženeva	k1gFnSc6	Ženeva
spálen	spálen	k2eAgInSc4d1	spálen
rukou	ruka	k1gFnSc7	ruka
katovou	katův	k2eAgFnSc7d1	Katova
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
jako	jako	k8xC	jako
kniha	kniha	k1gFnSc1	kniha
bezbožná	bezbožný	k2eAgFnSc1d1	bezbožná
<g/>
,	,	kIx,	,
nemorální	morální	k2eNgFnSc1d1	nemorální
a	a	k8xC	a
svatokrádežná	svatokrádežný	k2eAgFnSc1d1	svatokrádežná
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
uvádí	uvádět	k5eAaImIp3nS	uvádět
v	v	k7c4	v
posměch	posměch	k1gInSc4	posměch
mystéria	mystérium	k1gNnSc2	mystérium
<g/>
,	,	kIx,	,
dogmata	dogma	k1gNnPc4	dogma
<g/>
,	,	kIx,	,
kázeň	kázeň	k1gFnSc4	kázeň
<g/>
,	,	kIx,	,
autoritu	autorita	k1gFnSc4	autorita
božskou	božský	k2eAgFnSc4d1	božská
i	i	k8xC	i
lidskou	lidský	k2eAgFnSc4d1	lidská
atd.	atd.	kA	atd.
Rok	rok	k1gInSc1	rok
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
byl	být	k5eAaImAgInS	být
dán	dát	k5eAaPmNgInS	dát
papežem	papež	k1gMnSc7	papež
na	na	k7c4	na
index	index	k1gInSc4	index
a	a	k8xC	a
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
byl	být	k5eAaImAgInS	být
z	z	k7c2	z
nařízení	nařízení	k1gNnSc2	nařízení
pařížského	pařížský	k2eAgInSc2d1	pařížský
parlamentu	parlament	k1gInSc2	parlament
spálen	spálit	k5eAaPmNgInS	spálit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Rousseauovými	Rousseauová	k1gFnPc7	Rousseauová
Lettres	Lettres	k1gMnSc1	Lettres
écrites	écrites	k1gMnSc1	écrites
de	de	k?	de
la	la	k1gNnSc4	la
Montagne	montagne	k1gFnSc2	montagne
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
pak	pak	k6eAd1	pak
r.	r.	kA	r.
1766	[number]	k4	1766
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
upálen	upálit	k5eAaPmNgMnS	upálit
nešťastný	šťastný	k2eNgMnSc1d1	nešťastný
rytíř	rytíř	k1gMnSc1	rytíř
de	de	k?	de
la	la	k1gNnPc2	la
Barre	Barr	k1gInSc5	Barr
pro	pro	k7c4	pro
pouhé	pouhý	k2eAgNnSc4d1	pouhé
podezření	podezření	k1gNnSc4	podezření
z	z	k7c2	z
rouhání	rouhání	k1gNnSc2	rouhání
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
současně	současně	k6eAd1	současně
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
vydán	vydat	k5eAaPmNgInS	vydat
plamenům	plamen	k1gInPc3	plamen
i	i	k8xC	i
Filozofický	filozofický	k2eAgInSc1d1	filozofický
slovník	slovník	k1gInSc1	slovník
<g/>
,	,	kIx,	,
nalezený	nalezený	k2eAgMnSc1d1	nalezený
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
knihovně	knihovna	k1gFnSc6	knihovna
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1758	[number]	k4	1758
zakoupil	zakoupit	k5eAaPmAgInS	zakoupit
Voltaire	Voltair	k1gInSc5	Voltair
panství	panství	k1gNnSc1	panství
Ferney	Ferne	k2eAgInPc1d1	Ferne
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
ještě	ještě	k9	ještě
na	na	k7c6	na
francouzské	francouzský	k2eAgFnSc6d1	francouzská
půdě	půda	k1gFnSc6	půda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
při	při	k7c6	při
hranicích	hranice	k1gFnPc6	hranice
se	s	k7c7	s
Švýcarskem	Švýcarsko	k1gNnSc7	Švýcarsko
(	(	kIx(	(
<g/>
s	s	k7c7	s
kantonem	kanton	k1gInSc7	kanton
Ženeva	Ženeva	k1gFnSc1	Ženeva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
cítil	cítit	k5eAaImAgMnS	cítit
v	v	k7c6	v
bezpečí	bezpečí	k1gNnSc6	bezpečí
<g/>
,	,	kIx,	,
panství	panství	k1gNnSc6	panství
zvelebil	zvelebit	k5eAaPmAgMnS	zvelebit
<g/>
,	,	kIx,	,
postavil	postavit	k5eAaPmAgMnS	postavit
ve	v	k7c6	v
Ferney	Ferne	k2eAgFnPc1d1	Ferne
zámek	zámek	k1gInSc4	zámek
a	a	k8xC	a
kostel	kostel	k1gInSc4	kostel
<g/>
,	,	kIx,	,
podporoval	podporovat	k5eAaImAgInS	podporovat
tu	tu	k6eAd1	tu
hodinářský	hodinářský	k2eAgInSc1d1	hodinářský
průmysl	průmysl	k1gInSc1	průmysl
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Od	od	k7c2	od
r.	r.	kA	r.
1760	[number]	k4	1760
bydlí	bydlet	k5eAaImIp3nS	bydlet
tu	tu	k6eAd1	tu
pravidelně	pravidelně	k6eAd1	pravidelně
a	a	k8xC	a
žije	žít	k5eAaImIp3nS	žít
životem	život	k1gInSc7	život
knížecím	knížecí	k2eAgInSc7d1	knížecí
<g/>
,	,	kIx,	,
hostě	hostit	k5eAaImSgInS	hostit
občas	občas	k6eAd1	občas
šedesát	šedesát	k4xCc4	šedesát
až	až	k9	až
osmdesát	osmdesát	k4xCc4	osmdesát
lidí	člověk	k1gMnPc2	člověk
kromě	kromě	k7c2	kromě
domácího	domácí	k2eAgNnSc2d1	domácí
služebnictva	služebnictvo	k1gNnSc2	služebnictvo
a	a	k8xC	a
vítaje	vítat	k5eAaImSgInS	vítat
<g />
.	.	kIx.	.
</s>
<s>
u	u	k7c2	u
sebe	sebe	k3xPyFc4	sebe
členy	člen	k1gMnPc7	člen
evropské	evropský	k2eAgFnSc2d1	Evropská
šlechty	šlechta	k1gFnSc2	šlechta
rodové	rodový	k2eAgFnSc2d1	rodová
i	i	k8xC	i
duchové	duchový	k2eAgFnSc2d1	duchová
<g/>
:	:	kIx,	:
Voltaire	Voltair	k1gInSc5	Voltair
jest	být	k5eAaImIp3nS	být
evropskou	evropský	k2eAgFnSc7d1	Evropská
pamětihodností	pamětihodnost	k1gFnSc7	pamětihodnost
<g/>
,	,	kIx,	,
Ferney	Ferne	k2eAgInPc4d1	Ferne
Mekkou	Mekka	k1gFnSc7	Mekka
svobodných	svobodný	k2eAgMnPc2d1	svobodný
duchů	duch	k1gMnPc2	duch
a	a	k8xC	a
citlivých	citlivý	k2eAgFnPc2d1	citlivá
duší	duše	k1gFnPc2	duše
<g/>
;	;	kIx,	;
"	"	kIx"	"
<g/>
patriarcha	patriarcha	k1gMnSc1	patriarcha
ferneyský	ferneyský	k1gMnSc1	ferneyský
<g/>
"	"	kIx"	"
stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
předmětem	předmět	k1gInSc7	předmět
kultu	kult	k1gInSc2	kult
a	a	k8xC	a
středem	střed	k1gInSc7	střed
legendy	legenda	k1gFnSc2	legenda
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
se	se	k3xPyFc4	se
Voltaire	Voltair	k1gInSc5	Voltair
vrátil	vrátit	k5eAaPmAgInS	vrátit
až	až	k9	až
na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1778	[number]	k4	1778
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dohlédl	dohlédnout	k5eAaPmAgMnS	dohlédnout
na	na	k7c6	na
uvedení	uvedení	k1gNnSc6	uvedení
své	svůj	k3xOyFgFnSc2	svůj
hry	hra	k1gFnSc2	hra
Iréne	Irén	k1gInSc5	Irén
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
příjezd	příjezd	k1gInSc1	příjezd
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
prvořadou	prvořadý	k2eAgFnSc7d1	prvořadá
událostí	událost	k1gFnSc7	událost
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
přivítal	přivítat	k5eAaPmAgMnS	přivítat
ho	on	k3xPp3gInSc4	on
aplaudující	aplaudující	k2eAgInSc4d1	aplaudující
dav	dav	k1gInSc4	dav
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
byl	být	k5eAaImAgInS	být
oslavným	oslavný	k2eAgNnSc7d1	oslavné
skandováním	skandování	k1gNnSc7	skandování
přijat	přijmout	k5eAaPmNgInS	přijmout
na	na	k7c6	na
půdě	půda	k1gFnSc6	půda
Akademie	akademie	k1gFnSc2	akademie
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
pokojně	pokojně	k6eAd1	pokojně
a	a	k8xC	a
bezbolestně	bezbolestně	k6eAd1	bezbolestně
skonal	skonat	k5eAaPmAgMnS	skonat
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
ostatky	ostatek	k1gInPc1	ostatek
byly	být	k5eAaImAgInP	být
pohřbeny	pohřbít	k5eAaPmNgInP	pohřbít
na	na	k7c6	na
půdě	půda	k1gFnSc6	půda
opatství	opatství	k1gNnSc2	opatství
Scelliè	Scelliè	k1gFnPc2	Scelliè
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
byly	být	k5eAaImAgFnP	být
přeneseny	přenést	k5eAaPmNgFnP	přenést
během	během	k7c2	během
francouzské	francouzský	k2eAgFnSc2d1	francouzská
revoluce	revoluce	k1gFnSc2	revoluce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1791	[number]	k4	1791
do	do	k7c2	do
Pantheonu	Pantheon	k1gInSc2	Pantheon
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
tam	tam	k6eAd1	tam
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
<g/>
.	.	kIx.	.
</s>
<s>
Filozoficky	filozoficky	k6eAd1	filozoficky
byl	být	k5eAaImAgInS	být
Voltaire	Voltair	k1gInSc5	Voltair
eklektik	eklektik	k1gMnSc1	eklektik
<g/>
.	.	kIx.	.
</s>
<s>
Voltairova	Voltairův	k2eAgFnSc1d1	Voltairova
filozofie	filozofie	k1gFnSc1	filozofie
se	se	k3xPyFc4	se
formovala	formovat	k5eAaImAgFnS	formovat
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
Lockova	Lockův	k2eAgInSc2d1	Lockův
senzualismu	senzualismus	k1gInSc2	senzualismus
<g/>
,	,	kIx,	,
Baylova	Baylův	k2eAgInSc2d1	Baylův
náboženského	náboženský	k2eAgInSc2d1	náboženský
skepticismu	skepticismus	k1gInSc2	skepticismus
<g/>
,	,	kIx,	,
Newtonovy	Newtonův	k2eAgFnSc2d1	Newtonova
fyziky	fyzika	k1gFnSc2	fyzika
a	a	k8xC	a
systémů	systém	k1gInPc2	systém
Bolingbroka	Bolingbroek	k1gMnSc2	Bolingbroek
<g/>
,	,	kIx,	,
Shaftesburyho	Shaftesbury	k1gMnSc2	Shaftesbury
i	i	k8xC	i
dalších	další	k2eAgMnPc2d1	další
anglických	anglický	k2eAgMnPc2d1	anglický
deistů	deista	k1gMnPc2	deista
17	[number]	k4	17
<g/>
.	.	kIx.	.
-	-	kIx~	-
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
racionalistická	racionalistický	k2eAgFnSc1d1	racionalistická
a	a	k8xC	a
naturalistická	naturalistický	k2eAgFnSc1d1	naturalistická
filozofie	filozofie	k1gFnSc1	filozofie
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
ní	on	k3xPp3gFnSc6	on
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
i	i	k8xC	i
francouzská	francouzský	k2eAgFnSc1d1	francouzská
nezávislost	nezávislost	k1gFnSc1	nezávislost
myšlení	myšlení	k1gNnSc2	myšlení
<g/>
.	.	kIx.	.
</s>
<s>
Náboženským	náboženský	k2eAgNnSc7d1	náboženské
přesvědčením	přesvědčení	k1gNnSc7	přesvědčení
byl	být	k5eAaImAgMnS	být
deista	deista	k1gMnSc1	deista
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
jeho	jeho	k3xOp3gInSc1	jeho
deismus	deismus	k1gInSc1	deismus
byl	být	k5eAaImAgInS	být
příčinou	příčina	k1gFnSc7	příčina
ostrých	ostrý	k2eAgFnPc2d1	ostrá
kritik	kritika	k1gFnPc2	kritika
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
jeho	on	k3xPp3gMnSc2	on
současníka	současník	k1gMnSc2	současník
<g/>
,	,	kIx,	,
Holbacha	Holbach	k1gMnSc2	Holbach
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
pohledy	pohled	k1gInPc1	pohled
na	na	k7c4	na
svět	svět	k1gInSc4	svět
silně	silně	k6eAd1	silně
ovlivnil	ovlivnit	k5eAaPmAgMnS	ovlivnit
John	John	k1gMnSc1	John
Locke	Lock	k1gInSc2	Lock
<g/>
.	.	kIx.	.
</s>
<s>
Reprezentoval	reprezentovat	k5eAaImAgInS	reprezentovat
humanismus	humanismus	k1gInSc1	humanismus
a	a	k8xC	a
racionalismus	racionalismus	k1gInSc1	racionalismus
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Zastával	zastávat	k5eAaImAgMnS	zastávat
princip	princip	k1gInSc4	princip
tolerance	tolerance	k1gFnSc2	tolerance
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
poznání	poznání	k1gNnSc3	poznání
docházíme	docházet	k5eAaImIp1nP	docházet
zkušeností	zkušenost	k1gFnSc7	zkušenost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
otázkách	otázka	k1gFnPc6	otázka
metafyzických	metafyzický	k2eAgInPc2d1	metafyzický
byl	být	k5eAaImAgInS	být
skeptikem	skeptik	k1gMnSc7	skeptik
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Napišme	napsat	k5eAaPmRp1nP	napsat
na	na	k7c4	na
konec	konec	k1gInSc4	konec
téměř	téměř	k6eAd1	téměř
všech	všecek	k3xTgFnPc2	všecek
kapitol	kapitola	k1gFnPc2	kapitola
o	o	k7c6	o
metafyzice	metafyzika	k1gFnSc6	metafyzika
dvě	dva	k4xCgNnPc4	dva
písmena	písmeno	k1gNnPc4	písmeno
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc4	jenž
psali	psát	k5eAaImAgMnP	psát
římští	římský	k2eAgMnPc1d1	římský
soudcové	soudce	k1gMnPc1	soudce
<g/>
,	,	kIx,	,
když	když	k8xS	když
nerozuměli	rozumět	k5eNaImAgMnP	rozumět
věci	věc	k1gFnPc4	věc
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
N.	N.	kA	N.
L.	L.	kA	L.
<g/>
,	,	kIx,	,
non	non	k?	non
liquet	liquet	k1gInSc1	liquet
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
jasno	jasno	k6eAd1	jasno
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Člověk	člověk	k1gMnSc1	člověk
jako	jako	k8xC	jako
rozumná	rozumný	k2eAgFnSc1d1	rozumná
bytost	bytost	k1gFnSc1	bytost
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgInSc1d1	schopen
díky	díky	k7c3	díky
vědě	věda	k1gFnSc3	věda
<g/>
,	,	kIx,	,
technice	technika	k1gFnSc3	technika
<g/>
,	,	kIx,	,
kultuře	kultura	k1gFnSc3	kultura
a	a	k8xC	a
správnému	správný	k2eAgInSc3d1	správný
společenskému	společenský	k2eAgInSc3d1	společenský
řádu	řád	k1gInSc3	řád
vést	vést	k5eAaImF	vést
svůj	svůj	k3xOyFgInSc4	svůj
osud	osud	k1gInSc4	osud
k	k	k7c3	k
lepší	dobrý	k2eAgFnSc3d2	lepší
budoucnosti	budoucnost	k1gFnSc3	budoucnost
<g/>
.	.	kIx.	.
</s>
<s>
Spravedlnost	spravedlnost	k1gFnSc1	spravedlnost
je	být	k5eAaImIp3nS	být
hodnota	hodnota	k1gFnSc1	hodnota
univerzální	univerzální	k2eAgFnSc1d1	univerzální
<g/>
,	,	kIx,	,
nadřazená	nadřazený	k2eAgFnSc1d1	nadřazená
jednotlivým	jednotlivý	k2eAgFnPc3d1	jednotlivá
kulturám	kultura	k1gFnPc3	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Ctnost	ctnost	k1gFnSc1	ctnost
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
sklon	sklon	k1gInSc1	sklon
k	k	k7c3	k
činění	činění	k1gNnSc3	činění
dobrého	dobré	k1gNnSc2	dobré
<g/>
,	,	kIx,	,
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
individuálních	individuální	k2eAgFnPc2d1	individuální
potřeb	potřeba	k1gFnPc2	potřeba
člověka	člověk	k1gMnSc2	člověk
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc2	jeho
zájmů	zájem	k1gInPc2	zájem
uvnitř	uvnitř	k7c2	uvnitř
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Náboženskou	náboženský	k2eAgFnSc4d1	náboženská
představu	představa	k1gFnSc4	představa
"	"	kIx"	"
<g/>
Absolutního	absolutní	k2eAgNnSc2d1	absolutní
Dobra	dobro	k1gNnSc2	dobro
<g/>
"	"	kIx"	"
však	však	k9	však
označuje	označovat	k5eAaImIp3nS	označovat
za	za	k7c4	za
ekvivalent	ekvivalent	k1gInSc4	ekvivalent
"	"	kIx"	"
<g/>
absolutního	absolutní	k2eAgNnSc2d1	absolutní
ragú	ragú	k1gNnSc2	ragú
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
odpůrce	odpůrce	k1gMnSc1	odpůrce
absolutismu	absolutismus	k1gInSc2	absolutismus
<g/>
.	.	kIx.	.
</s>
<s>
Voltaire	Voltair	k1gMnSc5	Voltair
bývá	bývat	k5eAaImIp3nS	bývat
označován	označovat	k5eAaImNgMnS	označovat
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
bezbožníků	bezbožník	k1gMnPc2	bezbožník
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Bezbožství	bezbožství	k1gNnSc1	bezbožství
Voltairovo	Voltairův	k2eAgNnSc1d1	Voltairovo
nezakládá	zakládat	k5eNaImIp3nS	zakládat
se	se	k3xPyFc4	se
výhradně	výhradně	k6eAd1	výhradně
...	...	k?	...
na	na	k7c6	na
víře	víra	k1gFnSc6	víra
v	v	k7c4	v
rozum	rozum	k1gInSc4	rozum
a	a	k8xC	a
na	na	k7c6	na
principu	princip	k1gInSc6	princip
vědy	věda	k1gFnSc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
Pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
povahy	povaha	k1gFnSc2	povaha
dychtící	dychtící	k2eAgFnSc1d1	dychtící
po	po	k7c6	po
požitcích	požitek	k1gInPc6	požitek
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dědicem	dědic	k1gMnSc7	dědic
epikurejské	epikurejský	k2eAgFnSc2d1	epikurejská
tradice	tradice	k1gFnSc2	tradice
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
během	během	k7c2	během
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
hájí	hájit	k5eAaImIp3nS	hájit
pud	pud	k1gInSc1	pud
rozkoše	rozkoš	k1gFnSc2	rozkoš
proti	proti	k7c3	proti
křesťanství	křesťanství	k1gNnSc3	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Náboženství	náboženství	k1gNnSc1	náboženství
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
činí	činit	k5eAaImIp3nP	činit
překážky	překážka	k1gFnPc1	překážka
přírodě	příroda	k1gFnSc3	příroda
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
touhu	touha	k1gFnSc4	touha
a	a	k8xC	a
potěšení	potěšení	k1gNnSc4	potěšení
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
hříšné	hříšný	k2eAgNnSc1d1	hříšné
<g/>
,	,	kIx,	,
činí	činit	k5eAaImIp3nS	činit
naň	naň	k?	naň
dojem	dojem	k1gInSc4	dojem
nestvůrného	stvůrný	k2eNgInSc2d1	nestvůrný
nesmyslu	nesmysl	k1gInSc2	nesmysl
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Voltairovo	Voltairův	k2eAgNnSc1d1	Voltairovo
dílo	dílo	k1gNnSc1	dílo
bylo	být	k5eAaImAgNnS	být
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
školou	škola	k1gFnSc7	škola
nevážnosti	nevážnost	k1gFnSc2	nevážnost
a	a	k8xC	a
nevěry	nevěra	k1gFnSc2	nevěra
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
tragédii	tragédie	k1gFnSc6	tragédie
Oidipus	Oidipus	k1gMnSc1	Oidipus
(	(	kIx(	(
<g/>
Oedipe	Oedip	k1gMnSc5	Oedip
<g/>
,	,	kIx,	,
1718	[number]	k4	1718
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
verše	verš	k1gInPc4	verš
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nejsou	být	k5eNaImIp3nP	být
kněží	kněz	k1gMnPc1	kněz
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
hloupý	hloupý	k2eAgInSc1d1	hloupý
dav	dav	k1gInSc1	dav
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
vidí	vidět	k5eAaImIp3nS	vidět
<g/>
,	,	kIx,	,
jich	on	k3xPp3gMnPc2	on
celou	celý	k2eAgFnSc4d1	celá
sílu	síla	k1gFnSc4	síla
dělá	dělat	k5eAaImIp3nS	dělat
jen	jen	k9	jen
důvěřivost	důvěřivost	k1gFnSc1	důvěřivost
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Tragédie	tragédie	k1gFnSc1	tragédie
Mohamed	Mohamed	k1gMnSc1	Mohamed
(	(	kIx(	(
<g/>
Mahomet	Mahomet	k1gInSc1	Mahomet
<g/>
,	,	kIx,	,
1741	[number]	k4	1741
<g/>
)	)	kIx)	)
vyzněla	vyznít	k5eAaPmAgFnS	vyznít
jako	jako	k9	jako
útok	útok	k1gInSc4	útok
na	na	k7c4	na
náboženství	náboženství	k1gNnPc4	náboženství
vůbec	vůbec	k9	vůbec
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
jméno	jméno	k1gNnSc4	jméno
jeho	jeho	k3xOp3gMnSc2	jeho
proroka	prorok	k1gMnSc2	prorok
<g/>
,	,	kIx,	,
a	a	k8xC	a
z	z	k7c2	z
děje	děj	k1gInSc2	děj
se	se	k3xPyFc4	se
dalo	dát	k5eAaPmAgNnS	dát
usuzovat	usuzovat	k5eAaImF	usuzovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
každé	každý	k3xTgNnSc1	každý
náboženství	náboženství	k1gNnSc1	náboženství
se	se	k3xPyFc4	se
zakládá	zakládat	k5eAaImIp3nS	zakládat
na	na	k7c6	na
prohnanosti	prohnanost	k1gFnSc6	prohnanost
jedněch	jeden	k4xCgMnPc2	jeden
a	a	k8xC	a
hlouposti	hloupost	k1gFnPc4	hloupost
druhých	druhý	k4xOgMnPc2	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
Voltaire	Voltair	k1gInSc5	Voltair
nebývá	bývat	k5eNaImIp3nS	bývat
označován	označovat	k5eAaImNgMnS	označovat
za	za	k7c4	za
ateistu	ateista	k1gMnSc4	ateista
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
za	za	k7c4	za
deistu	deista	k1gMnSc4	deista
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
"	"	kIx"	"
<g/>
Pojednání	pojednání	k1gNnSc6	pojednání
o	o	k7c6	o
metafyzice	metafyzika	k1gFnSc6	metafyzika
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Traité	Traitý	k2eAgNnSc1d1	Traité
de	de	k?	de
métaphysique	métaphysique	k1gNnSc1	métaphysique
<g/>
,	,	kIx,	,
1734	[number]	k4	1734
<g/>
)	)	kIx)	)
dospěl	dochvít	k5eAaPmAgMnS	dochvít
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
jsme	být	k5eAaImIp1nP	být
<g/>
-li	i	k?	-li
toho	ten	k3xDgNnSc2	ten
mínění	mínění	k1gNnSc2	mínění
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
Bůh	bůh	k1gMnSc1	bůh
<g/>
,	,	kIx,	,
narážíme	narážet	k5eAaImIp1nP	narážet
na	na	k7c4	na
obtíže	obtíž	k1gFnPc4	obtíž
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
při	při	k7c6	při
opačném	opačný	k2eAgNnSc6d1	opačné
mínění	mínění	k1gNnSc6	mínění
docházíme	docházet	k5eAaImIp1nP	docházet
k	k	k7c3	k
absurdnostem	absurdnost	k1gFnPc3	absurdnost
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
cosi	cosi	k3yInSc1	cosi
věčného	věčný	k2eAgNnSc2d1	věčné
<g/>
;	;	kIx,	;
předpokládáme	předpokládat	k5eAaImIp1nP	předpokládat
tvůrce	tvůrce	k1gMnSc1	tvůrce
všeho	všecek	k3xTgNnSc2	všecek
<g/>
,	,	kIx,	,
uspořádání	uspořádání	k1gNnSc1	uspořádání
světa	svět	k1gInSc2	svět
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
pořadatele	pořadatel	k1gMnPc4	pořadatel
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
vlastnosti	vlastnost	k1gFnPc1	vlastnost
Boží	božit	k5eAaImIp3nP	božit
už	už	k6eAd1	už
tak	tak	k6eAd1	tak
zřejmé	zřejmý	k2eAgFnPc1d1	zřejmá
nejsou	být	k5eNaImIp3nP	být
<g/>
,	,	kIx,	,
nevíme	vědět	k5eNaImIp1nP	vědět
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
nekonečný	konečný	k2eNgInSc1d1	nekonečný
<g/>
,	,	kIx,	,
všudypřítomný	všudypřítomný	k2eAgInSc1d1	všudypřítomný
atd.	atd.	kA	atd.
Musíme	muset	k5eAaImIp1nP	muset
tedy	tedy	k9	tedy
přiznat	přiznat	k5eAaPmF	přiznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
povahu	povaha	k1gFnSc4	povaha
Boha	bůh	k1gMnSc2	bůh
neznáme	neznat	k5eAaImIp1nP	neznat
<g/>
,	,	kIx,	,
a	a	k8xC	a
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jaký	jaký	k3yRgInSc4	jaký
jest	být	k5eAaImIp3nS	být
Bůh	bůh	k1gMnSc1	bůh
a	a	k8xC	a
jaký	jaký	k3yQgMnSc1	jaký
je	být	k5eAaImIp3nS	být
jeho	on	k3xPp3gInSc4	on
poměr	poměr	k1gInSc4	poměr
ke	k	k7c3	k
světu	svět	k1gInSc3	svět
<g/>
,	,	kIx,	,
nic	nic	k3yNnSc1	nic
nevíme	vědět	k5eNaImIp1nP	vědět
<g/>
.	.	kIx.	.
</s>
<s>
Nemáme	mít	k5eNaImIp1nP	mít
žádnou	žádný	k3yNgFnSc4	žádný
přiměřenou	přiměřený	k2eAgFnSc4d1	přiměřená
představu	představa	k1gFnSc4	představa
o	o	k7c4	o
božství	božství	k1gNnSc4	božství
a	a	k8xC	a
jsme	být	k5eAaImIp1nP	být
neustále	neustále	k6eAd1	neustále
v	v	k7c6	v
nebezpečí	nebezpečí	k1gNnSc6	nebezpečí
<g/>
,	,	kIx,	,
že	že	k8xS	že
budeme	být	k5eAaImBp1nP	být
Bohu	bůh	k1gMnSc3	bůh
přisuzovat	přisuzovat	k5eAaImF	přisuzovat
lidská	lidský	k2eAgNnPc4d1	lidské
určení	určení	k1gNnSc4	určení
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
základě	základ	k1gInSc6	základ
Voltaire	Voltair	k1gInSc5	Voltair
odmítá	odmítat	k5eAaImIp3nS	odmítat
teologické	teologický	k2eAgFnSc2d1	teologická
diskuse	diskuse	k1gFnSc2	diskuse
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
to	ten	k3xDgNnSc1	ten
jsou	být	k5eAaImIp3nP	být
diskuse	diskuse	k1gFnPc1	diskuse
o	o	k7c6	o
nedokazatelných	dokazatelný	k2eNgFnPc6d1	nedokazatelná
spekulacích	spekulace	k1gFnPc6	spekulace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
práci	práce	k1gFnSc4	práce
"	"	kIx"	"
<g/>
Dialogy	dialog	k1gInPc1	dialog
mezi	mezi	k7c7	mezi
A	A	kA	A
<g/>
,	,	kIx,	,
B	B	kA	B
a	a	k8xC	a
C	C	kA	C
neboli	neboli	k8xC	neboli
ABCda	ABCda	k1gFnSc1	ABCda
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
A	A	kA	A
<g/>
,	,	kIx,	,
B	B	kA	B
<g/>
,	,	kIx,	,
C	C	kA	C
<g/>
,	,	kIx,	,
ou	ou	k0	ou
dialogues	dialogues	k1gMnSc1	dialogues
entre	entr	k1gInSc5	entr
A	A	kA	A
<g/>
,	,	kIx,	,
B	B	kA	B
<g/>
,	,	kIx,	,
C	C	kA	C
<g/>
;	;	kIx,	;
1769	[number]	k4	1769
<g/>
)	)	kIx)	)
Voltaire	Voltair	k1gInSc5	Voltair
píše	psát	k5eAaImIp3nS	psát
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Věřím	věřit	k5eAaImIp1nS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
rozumná	rozumný	k2eAgFnSc1d1	rozumná
bytost	bytost	k1gFnSc1	bytost
<g/>
,	,	kIx,	,
tvořivá	tvořivý	k2eAgFnSc1d1	tvořivá
moc	moc	k1gFnSc1	moc
<g/>
,	,	kIx,	,
Bůh	bůh	k1gMnSc1	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
všem	všecek	k3xTgNnSc6	všecek
ostatním	ostatní	k1gNnSc6	ostatní
tápu	tápat	k5eAaImIp1nS	tápat
v	v	k7c6	v
temnotách	temnota	k1gFnPc6	temnota
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Voltaire	Voltair	k1gMnSc5	Voltair
nikdy	nikdy	k6eAd1	nikdy
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
uvěřit	uvěřit	k5eAaPmF	uvěřit
ani	ani	k8xC	ani
v	v	k7c6	v
existenci	existence	k1gFnSc6	existence
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
v	v	k7c4	v
nesmrtelnost	nesmrtelnost	k1gFnSc4	nesmrtelnost
lidské	lidský	k2eAgFnSc2d1	lidská
duše	duše	k1gFnSc2	duše
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
"	"	kIx"	"
<g/>
Pojednání	pojednání	k1gNnSc6	pojednání
o	o	k7c6	o
metafyzice	metafyzika	k1gFnSc6	metafyzika
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Traité	Traitý	k2eAgNnSc1d1	Traité
de	de	k?	de
métaphysique	métaphysique	k1gNnSc1	métaphysique
<g/>
,	,	kIx,	,
1734	[number]	k4	1734
<g/>
)	)	kIx)	)
napsal	napsat	k5eAaBmAgInS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Neujišťuji	ujišťovat	k5eNaImIp1nS	ujišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mám	mít	k5eAaImIp1nS	mít
přísný	přísný	k2eAgInSc4d1	přísný
důkaz	důkaz	k1gInSc4	důkaz
proti	proti	k7c3	proti
duchovnosti	duchovnost	k1gFnSc3	duchovnost
a	a	k8xC	a
nesmrtelnosti	nesmrtelnost	k1gFnSc3	nesmrtelnost
duše	duše	k1gFnSc2	duše
<g/>
;	;	kIx,	;
ale	ale	k8xC	ale
všechny	všechen	k3xTgFnPc1	všechen
pravděpodobnosti	pravděpodobnost	k1gFnPc1	pravděpodobnost
mluví	mluvit	k5eAaImIp3nP	mluvit
proti	proti	k7c3	proti
tomu	ten	k3xDgMnSc3	ten
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
stejně	stejně	k6eAd1	stejně
nespravedlivé	spravedlivý	k2eNgNnSc1d1	nespravedlivé
a	a	k8xC	a
nerozumné	rozumný	k2eNgNnSc1d1	nerozumné
požadovat	požadovat	k5eAaImF	požadovat
přísný	přísný	k2eAgInSc4d1	přísný
důkaz	důkaz	k1gInSc4	důkaz
v	v	k7c6	v
otázce	otázka	k1gFnSc6	otázka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
připouští	připouštět	k5eAaImIp3nS	připouštět
jen	jen	k9	jen
dohady	dohad	k1gInPc1	dohad
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Dle	dle	k7c2	dle
profesora	profesor	k1gMnSc2	profesor
Kopala	Kopal	k1gMnSc2	Kopal
"	"	kIx"	"
<g/>
Voltaire	Voltair	k1gInSc5	Voltair
sice	sice	k8xC	sice
někdy	někdy	k6eAd1	někdy
mluvil	mluvit	k5eAaImAgMnS	mluvit
o	o	k7c6	o
posmrtném	posmrtný	k2eAgInSc6d1	posmrtný
životě	život	k1gInSc6	život
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
uznával	uznávat	k5eAaImAgInS	uznávat
víru	víra	k1gFnSc4	víra
v	v	k7c4	v
něj	on	k3xPp3gMnSc4	on
jako	jako	k9	jako
hypotézu	hypotéza	k1gFnSc4	hypotéza
užitečnou	užitečný	k2eAgFnSc4d1	užitečná
pro	pro	k7c4	pro
druhé	druhý	k4xOgMnPc4	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
se	se	k3xPyFc4	se
bez	bez	k7c2	bez
ní	on	k3xPp3gFnSc2	on
obešel	obejít	k5eAaPmAgMnS	obejít
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Voltaire	Voltair	k1gMnSc5	Voltair
byl	být	k5eAaImAgInS	být
nesmiřitelným	smiřitelný	k2eNgMnSc7d1	nesmiřitelný
nepřítelem	nepřítel	k1gMnSc7	nepřítel
dogmat	dogma	k1gNnPc2	dogma
<g/>
,	,	kIx,	,
obřadů	obřad	k1gInPc2	obřad
<g/>
,	,	kIx,	,
zjevených	zjevený	k2eAgNnPc2d1	zjevené
náboženství	náboženství	k1gNnPc2	náboženství
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc4	ten
vše	všechen	k3xTgNnSc4	všechen
zahrnoval	zahrnovat	k5eAaImAgMnS	zahrnovat
pod	pod	k7c7	pod
potupným	potupný	k2eAgNnSc7d1	potupné
jménem	jméno	k1gNnSc7	jméno
"	"	kIx"	"
<g/>
pověra	pověra	k1gFnSc1	pověra
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
"	"	kIx"	"
<g/>
Filozofickém	filozofický	k2eAgInSc6d1	filozofický
slovníku	slovník	k1gInSc6	slovník
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Dictionnaire	Dictionnair	k1gMnSc5	Dictionnair
philosophique	philosophiquus	k1gMnSc5	philosophiquus
portatif	portatif	k1gMnSc1	portatif
<g/>
,	,	kIx,	,
1764	[number]	k4	1764
<g/>
)	)	kIx)	)
napsal	napsat	k5eAaBmAgInS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Téměř	téměř	k6eAd1	téměř
vše	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
sahá	sahat	k5eAaImIp3nS	sahat
za	za	k7c4	za
hranice	hranice	k1gFnPc4	hranice
uctívání	uctívání	k1gNnSc2	uctívání
Nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
bytosti	bytost	k1gFnSc2	bytost
a	a	k8xC	a
podřízení	podřízení	k1gNnSc1	podřízení
se	se	k3xPyFc4	se
jejímu	její	k3xOp3gNnSc3	její
věčnému	věčné	k1gNnSc3	věčné
řádu	řád	k1gInSc2	řád
<g/>
,	,	kIx,	,
jest	být	k5eAaImIp3nS	být
pověra	pověra	k1gFnSc1	pověra
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Náboženství	náboženství	k1gNnPc4	náboženství
považoval	považovat	k5eAaImAgInS	považovat
za	za	k7c4	za
výtvor	výtvor	k1gInSc4	výtvor
lidský	lidský	k2eAgInSc1d1	lidský
<g/>
,	,	kIx,	,
dílo	dílo	k1gNnSc1	dílo
podvodných	podvodný	k2eAgMnPc2d1	podvodný
proroků	prorok	k1gMnPc2	prorok
a	a	k8xC	a
kněží	kněz	k1gMnPc2	kněz
<g/>
.	.	kIx.	.
</s>
<s>
Prudce	prudko	k6eAd1	prudko
útočil	útočit	k5eAaImAgMnS	útočit
na	na	k7c4	na
katolicismus	katolicismus	k1gInSc4	katolicismus
a	a	k8xC	a
jeho	jeho	k3xOp3gNnPc4	jeho
dogmata	dogma	k1gNnPc4	dogma
<g/>
.	.	kIx.	.
</s>
<s>
Poukazoval	poukazovat	k5eAaImAgMnS	poukazovat
na	na	k7c4	na
rozpory	rozpor	k1gInPc4	rozpor
v	v	k7c6	v
bibli	bible	k1gFnSc6	bible
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
dokazoval	dokazovat	k5eAaImAgMnS	dokazovat
její	její	k3xOp3gFnSc4	její
nevěrohodnost	nevěrohodnost	k1gFnSc4	nevěrohodnost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
kněze	kněz	k1gMnPc4	kněz
se	se	k3xPyFc4	se
díval	dívat	k5eAaImAgMnS	dívat
s	s	k7c7	s
despektem	despekt	k1gInSc7	despekt
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
nástrojem	nástroj	k1gInSc7	nástroj
tyranie	tyranie	k1gFnSc2	tyranie
<g/>
,	,	kIx,	,
strůjcem	strůjce	k1gMnSc7	strůjce
krvavých	krvavý	k2eAgFnPc2d1	krvavá
náboženských	náboženský	k2eAgFnPc2d1	náboženská
válek	válka	k1gFnPc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Kněží	kněz	k1gMnPc1	kněz
nepovznášejí	povznášet	k5eNaImIp3nP	povznášet
lid	lid	k1gInSc4	lid
mravně	mravně	k6eAd1	mravně
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
jej	on	k3xPp3gMnSc4	on
ohlupují	ohlupovat	k5eAaImIp3nP	ohlupovat
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
však	však	k9	však
neznamená	znamenat	k5eNaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
odmítal	odmítat	k5eAaImAgMnS	odmítat
jakékoliv	jakýkoliv	k3yIgNnSc4	jakýkoliv
náboženství	náboženství	k1gNnSc4	náboženství
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Chceme	chtít	k5eAaImIp1nP	chtít
náboženství	náboženství	k1gNnPc1	náboženství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
náboženství	náboženství	k1gNnSc1	náboženství
prosté	prostý	k2eAgNnSc1d1	prosté
<g/>
,	,	kIx,	,
moudré	moudrý	k2eAgNnSc1d1	moudré
a	a	k8xC	a
vznešené	vznešený	k2eAgNnSc1d1	vznešené
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Při	při	k7c6	při
posuzování	posuzování	k1gNnSc6	posuzování
náboženství	náboženství	k1gNnSc2	náboženství
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
primát	primát	k1gInSc1	primát
praxe	praxe	k1gFnSc2	praxe
<g/>
,	,	kIx,	,
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
pro	pro	k7c4	pro
Voltairovo	Voltairův	k2eAgNnSc4d1	Voltairovo
myšlení	myšlení	k1gNnSc4	myšlení
<g/>
.	.	kIx.	.
</s>
<s>
Náboženství	náboženství	k1gNnSc1	náboženství
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
Voltaira	Voltair	k1gInSc2	Voltair
akceptovatelné	akceptovatelný	k2eAgNnSc1d1	akceptovatelné
pouze	pouze	k6eAd1	pouze
potud	potud	k6eAd1	potud
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
shodě	shoda	k1gFnSc6	shoda
s	s	k7c7	s
morálkou	morálka	k1gFnSc7	morálka
a	a	k8xC	a
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
stejně	stejně	k6eAd1	stejně
obecné	obecný	k2eAgInPc4d1	obecný
jako	jako	k8xS	jako
ona	onen	k3xDgFnSc1	onen
<g/>
;	;	kIx,	;
náboženské	náboženský	k2eAgFnPc4d1	náboženská
představy	představa	k1gFnPc4	představa
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
odporují	odporovat	k5eAaImIp3nP	odporovat
morálce	morálka	k1gFnSc3	morálka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
třeba	třeba	k6eAd1	třeba
odmítnout	odmítnout	k5eAaPmF	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
vysoce	vysoce	k6eAd1	vysoce
oceňoval	oceňovat	k5eAaImAgInS	oceňovat
čínský	čínský	k2eAgInSc1d1	čínský
konfucianismus	konfucianismus	k1gInSc1	konfucianismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
práci	práce	k1gFnSc6	práce
"	"	kIx"	"
<g/>
Nevědomý	vědomý	k2eNgMnSc1d1	nevědomý
filozof	filozof	k1gMnSc1	filozof
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Le	Le	k1gMnSc1	Le
Philosophe	Philosoph	k1gFnSc2	Philosoph
ignorant	ignorant	k1gMnSc1	ignorant
<g/>
,	,	kIx,	,
1766	[number]	k4	1766
<g/>
)	)	kIx)	)
píše	psát	k5eAaImIp3nS	psát
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nikde	nikde	k6eAd1	nikde
nebylo	být	k5eNaImAgNnS	být
ctění	ctění	k1gNnSc1	ctění
Boha	bůh	k1gMnSc2	bůh
tak	tak	k9	tak
čisté	čistý	k2eAgInPc1d1	čistý
a	a	k8xC	a
svaté	svatý	k2eAgInPc1d1	svatý
jako	jako	k8xC	jako
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
...	...	k?	...
Nemluvím	mluvit	k5eNaImIp1nS	mluvit
o	o	k7c6	o
lidových	lidový	k2eAgFnPc6d1	lidová
sektách	sekta	k1gFnPc6	sekta
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
o	o	k7c4	o
náboženství	náboženství	k1gNnSc4	náboženství
císařů	císař	k1gMnPc2	císař
<g/>
,	,	kIx,	,
tribunálů	tribunál	k1gInPc2	tribunál
a	a	k8xC	a
všeho	všecek	k3xTgNnSc2	všecek
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
není	být	k5eNaImIp3nS	být
lidem	člověk	k1gMnPc3	člověk
<g/>
.	.	kIx.	.
...	...	k?	...
Počítají	počítat	k5eAaImIp3nP	počítat
často	často	k6eAd1	často
velkého	velký	k2eAgMnSc4d1	velký
Kung-Fu-Tse	Kung-Fu-Ts	k1gMnSc4	Kung-Fu-Ts
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
nazýváme	nazývat	k5eAaImIp1nP	nazývat
Konfucius	Konfucius	k1gInSc4	Konfucius
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
starobylé	starobylý	k2eAgMnPc4d1	starobylý
zákonodárce	zákonodárce	k1gMnPc4	zákonodárce
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
zakladatele	zakladatel	k1gMnSc2	zakladatel
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
;	;	kIx,	;
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
velký	velký	k2eAgInSc1d1	velký
omyl	omyl	k1gInSc1	omyl
<g/>
.	.	kIx.	.
</s>
<s>
Kung-Fu-Tse	Kung-Fu-Tse	k1gFnSc1	Kung-Fu-Tse
...	...	k?	...
nikdy	nikdy	k6eAd1	nikdy
neustanovil	ustanovit	k5eNaPmAgInS	ustanovit
žádný	žádný	k3yNgInSc4	žádný
kult	kult	k1gInSc4	kult
ani	ani	k8xC	ani
rituál	rituál	k1gInSc4	rituál
<g/>
,	,	kIx,	,
nikdy	nikdy	k6eAd1	nikdy
o	o	k7c6	o
sobě	se	k3xPyFc3	se
neřekl	říct	k5eNaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
inspirovaný	inspirovaný	k2eAgInSc1d1	inspirovaný
nebo	nebo	k8xC	nebo
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
prorok	prorok	k1gMnSc1	prorok
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
sebral	sebrat	k5eAaPmAgInS	sebrat
do	do	k7c2	do
jedné	jeden	k4xCgFnSc2	jeden
sbírky	sbírka	k1gFnSc2	sbírka
starobylé	starobylý	k2eAgInPc4d1	starobylý
morální	morální	k2eAgInPc4d1	morální
zákony	zákon	k1gInPc4	zákon
<g/>
.	.	kIx.	.
...	...	k?	...
Doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
dobro	dobro	k1gNnSc1	dobro
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jednej	jednat	k5eAaImRp2nS	jednat
s	s	k7c7	s
druhým	druhý	k4xOgMnSc7	druhý
tak	tak	k9	tak
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
chceš	chtít	k5eAaImIp2nS	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
jednali	jednat	k5eAaImAgMnP	jednat
s	s	k7c7	s
tebou	ty	k3xPp2nSc7	ty
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
"	"	kIx"	"
Voltaire	Voltair	k1gMnSc5	Voltair
odmítal	odmítat	k5eAaImAgMnS	odmítat
ateismus	ateismus	k1gInSc4	ateismus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
morálky	morálka	k1gFnSc2	morálka
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
jevil	jevit	k5eAaImAgInS	jevit
ateismus	ateismus	k1gInSc1	ateismus
méně	málo	k6eAd2	málo
nebezpečný	bezpečný	k2eNgMnSc1d1	nebezpečný
než	než	k8xS	než
fanatismus	fanatismus	k1gInSc1	fanatismus
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vyrůstá	vyrůstat	k5eAaImIp3nS	vyrůstat
z	z	k7c2	z
víry	víra	k1gFnSc2	víra
ve	v	k7c6	v
zjevení	zjevení	k1gNnSc6	zjevení
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
"	"	kIx"	"
<g/>
Filozofickém	filozofický	k2eAgInSc6d1	filozofický
slovníku	slovník	k1gInSc6	slovník
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Dictionnaire	Dictionnair	k1gMnSc5	Dictionnair
philosophique	philosophiquus	k1gMnSc5	philosophiquus
portatif	portatif	k1gMnSc1	portatif
<g/>
,	,	kIx,	,
1764	[number]	k4	1764
<g/>
)	)	kIx)	)
napsal	napsat	k5eAaBmAgInS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Fanatismus	fanatismus	k1gInSc1	fanatismus
je	být	k5eAaImIp3nS	být
rozhodně	rozhodně	k6eAd1	rozhodně
tisíckrát	tisíckrát	k6eAd1	tisíckrát
zhoubnější	zhoubný	k2eAgMnSc1d2	zhoubnější
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
ateismus	ateismus	k1gInSc1	ateismus
nevnuká	vnukat	k5eNaImIp3nS	vnukat
vražedných	vražedný	k2eAgFnPc2d1	vražedná
vášní	vášeň	k1gFnPc2	vášeň
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
fanatismus	fanatismus	k1gInSc1	fanatismus
je	on	k3xPp3gNnSc4	on
vnuká	vnukat	k5eAaImIp3nS	vnukat
<g/>
;	;	kIx,	;
ateismus	ateismus	k1gInSc4	ateismus
nestaví	stavit	k5eNaBmIp3nS	stavit
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
zločinům	zločin	k1gMnPc3	zločin
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
fanatismus	fanatismus	k1gInSc1	fanatismus
je	on	k3xPp3gNnSc4	on
páše	páchat	k5eAaImIp3nS	páchat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
mezi	mezi	k7c7	mezi
vírou	víra	k1gFnSc7	víra
v	v	k7c4	v
Boha	bůh	k1gMnSc4	bůh
a	a	k8xC	a
ateismem	ateismus	k1gInSc7	ateismus
provádí	provádět	k5eAaImIp3nS	provádět
Voltaire	Voltair	k1gInSc5	Voltair
z	z	k7c2	z
praktického	praktický	k2eAgNnSc2d1	praktické
hlediska	hledisko	k1gNnSc2	hledisko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zájmu	zájem	k1gInSc6	zájem
morálky	morálka	k1gFnSc2	morálka
<g/>
,	,	kIx,	,
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
a	a	k8xC	a
státního	státní	k2eAgInSc2d1	státní
řádu	řád	k1gInSc2	řád
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
lidem	lid	k1gInSc7	lid
vštípit	vštípit	k5eAaPmF	vštípit
víru	víra	k1gFnSc4	víra
v	v	k7c4	v
Boha	bůh	k1gMnSc4	bůh
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c4	v
Boha	bůh	k1gMnSc4	bůh
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
odměňuje	odměňovat	k5eAaImIp3nS	odměňovat
a	a	k8xC	a
trestá	trestat	k5eAaImIp3nS	trestat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
práci	práce	k1gFnSc6	práce
"	"	kIx"	"
<g/>
Pojednání	pojednání	k1gNnSc1	pojednání
o	o	k7c6	o
snášenlivosti	snášenlivost	k1gFnSc6	snášenlivost
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Traité	Traitý	k2eAgNnSc1d1	Traité
sur	sur	k?	sur
la	la	k1gNnSc1	la
tolérance	tolérance	k1gFnSc2	tolérance
<g/>
,	,	kIx,	,
1763	[number]	k4	1763
<g/>
)	)	kIx)	)
píše	psát	k5eAaImIp3nS	psát
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Lidské	lidský	k2eAgNnSc1d1	lidské
pokolení	pokolení	k1gNnSc1	pokolení
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
takovou	takový	k3xDgFnSc7	takový
slabostí	slabost	k1gFnSc7	slabost
a	a	k8xC	a
takovou	takový	k3xDgFnSc7	takový
mravní	mravní	k2eAgFnSc7d1	mravní
zkažeností	zkaženost	k1gFnSc7	zkaženost
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
pro	pro	k7c4	pro
ně	on	k3xPp3gInPc4	on
bylo	být	k5eAaImAgNnS	být
nepochybně	pochybně	k6eNd1	pochybně
prospěšnější	prospěšný	k2eAgNnSc1d2	prospěšnější
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
spoutáno	spoutat	k5eAaPmNgNnS	spoutat
nejrůznějšími	různý	k2eAgFnPc7d3	nejrůznější
pověrami	pověra	k1gFnPc7	pověra
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
ne	ne	k9	ne
takovými	takový	k3xDgNnPc7	takový
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
by	by	kYmCp3nP	by
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
vraždám	vražda	k1gFnPc3	vražda
<g/>
,	,	kIx,	,
než	než	k8xS	než
aby	aby	kYmCp3nS	aby
žilo	žít	k5eAaImAgNnS	žít
bez	bez	k7c2	bez
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
vždy	vždy	k6eAd1	vždy
potřeboval	potřebovat	k5eAaImAgMnS	potřebovat
mít	mít	k5eAaImF	mít
uzdu	uzda	k1gFnSc4	uzda
<g/>
;	;	kIx,	;
...	...	k?	...
Všude	všude	k6eAd1	všude
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
lidé	člověk	k1gMnPc1	člověk
žijí	žít	k5eAaImIp3nP	žít
ve	v	k7c6	v
společenství	společenství	k1gNnSc6	společenství
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
náboženství	náboženství	k1gNnSc1	náboženství
nezbytné	zbytný	k2eNgNnSc1d1	nezbytné
<g/>
;	;	kIx,	;
zákony	zákon	k1gInPc1	zákon
chrání	chránit	k5eAaImIp3nP	chránit
před	před	k7c7	před
zločiny	zločin	k1gInPc7	zločin
obecně	obecně	k6eAd1	obecně
známými	známý	k2eAgInPc7d1	známý
<g/>
,	,	kIx,	,
náboženství	náboženství	k1gNnSc1	náboženství
pak	pak	k6eAd1	pak
před	před	k7c7	před
zločiny	zločin	k1gInPc7	zločin
skrytými	skrytý	k2eAgInPc7d1	skrytý
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
A	a	k9	a
ve	v	k7c6	v
"	"	kIx"	"
<g/>
Filozofickém	filozofický	k2eAgInSc6d1	filozofický
slovníku	slovník	k1gInSc6	slovník
<g/>
"	"	kIx"	"
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
naprosto	naprosto	k6eAd1	naprosto
nutno	nutno	k6eAd1	nutno
pro	pro	k7c4	pro
panovníky	panovník	k1gMnPc4	panovník
i	i	k9	i
pro	pro	k7c4	pro
národy	národ	k1gInPc4	národ
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
představa	představa	k1gFnSc1	představa
o	o	k7c6	o
Nejvyšší	vysoký	k2eAgFnSc6d3	nejvyšší
Bytosti	bytost	k1gFnSc6	bytost
<g/>
,	,	kIx,	,
tvořící	tvořící	k2eAgFnSc4d1	tvořící
<g/>
,	,	kIx,	,
spravující	spravující	k2eAgFnSc4d1	spravující
<g/>
,	,	kIx,	,
odměňující	odměňující	k2eAgFnSc4d1	odměňující
a	a	k8xC	a
trestající	trestající	k2eAgFnSc4d1	trestající
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
hluboce	hluboko	k6eAd1	hluboko
vryta	vrýt	k5eAaPmNgFnS	vrýt
do	do	k7c2	do
mysli	mysl	k1gFnSc2	mysl
všech	všecek	k3xTgMnPc2	všecek
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Vlivem	vliv	k1gInSc7	vliv
Bolingbrokovým	Bolingbrokový	k2eAgFnPc3d1	Bolingbrokový
chápal	chápat	k5eAaImAgInS	chápat
Voltaire	Voltair	k1gInSc5	Voltair
osvícenství	osvícenství	k1gNnSc2	osvícenství
aristokraticky	aristokraticky	k6eAd1	aristokraticky
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
nevěřil	věřit	k5eNaImAgMnS	věřit
v	v	k7c4	v
možnost	možnost	k1gFnSc4	možnost
vzdělání	vzdělání	k1gNnSc2	vzdělání
lidu	lid	k1gInSc2	lid
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Na	na	k7c4	na
prostý	prostý	k2eAgInSc4d1	prostý
lid	lid	k1gInSc4	lid
se	se	k3xPyFc4	se
díval	dívat	k5eAaImAgMnS	dívat
pohrdavě	pohrdavě	k6eAd1	pohrdavě
<g/>
.	.	kIx.	.
</s>
<s>
Mluví	mluvit	k5eAaImIp3nS	mluvit
o	o	k7c6	o
něm	on	k3xPp3gNnSc6	on
často	často	k6eAd1	často
jako	jako	k8xS	jako
o	o	k7c6	o
nevzdělané	vzdělaný	k2eNgFnSc6d1	nevzdělaná
holotě	holota	k1gFnSc6	holota
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
canaille	canaille	k1gFnSc1	canaille
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
sběři	sběř	k1gFnSc3	sběř
a	a	k8xC	a
luze	luza	k1gFnSc3	luza
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
-	-	kIx~	-
jak	jak	k8xS	jak
napsal	napsat	k5eAaPmAgMnS	napsat
Damilavillovi	Damilavill	k1gMnSc3	Damilavill
-	-	kIx~	-
"	"	kIx"	"
<g/>
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
jen	jen	k9	jen
vedena	vést	k5eAaImNgFnS	vést
<g/>
,	,	kIx,	,
a	a	k8xC	a
ne	ne	k9	ne
vzdělávána	vzděláván	k2eAgFnSc1d1	vzdělávána
<g/>
"	"	kIx"	"
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Podle	podle	k7c2	podle
mého	můj	k3xOp1gInSc2	můj
názoru	názor	k1gInSc2	názor
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
služba	služba	k1gFnSc1	služba
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
lze	lze	k6eAd1	lze
lidstvu	lidstvo	k1gNnSc3	lidstvo
prokázat	prokázat	k5eAaPmF	prokázat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
oddělit	oddělit	k5eAaPmF	oddělit
navždy	navždy	k6eAd1	navždy
hloupý	hloupý	k2eAgInSc1d1	hloupý
lid	lid	k1gInSc1	lid
od	od	k7c2	od
vzdělanců	vzdělanec	k1gMnPc2	vzdělanec
<g/>
.	.	kIx.	.
</s>
<s>
Nelze	lze	k6eNd1	lze
strpět	strpět	k5eAaPmF	strpět
opovážlivost	opovážlivost	k1gFnSc4	opovážlivost
těch	ten	k3xDgMnPc2	ten
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
:	:	kIx,	:
Chci	chtít	k5eAaImIp1nS	chtít
<g/>
,	,	kIx,	,
abyste	aby	kYmCp2nP	aby
smýšlel	smýšlet	k5eAaImAgInS	smýšlet
jako	jako	k8xS	jako
váš	váš	k3xOp2gMnSc1	váš
krejčí	krejčí	k1gMnSc1	krejčí
a	a	k8xC	a
vaše	váš	k3xOp2gFnSc1	váš
pradlena	pradlena	k1gFnSc1	pradlena
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
píše	psát	k5eAaImIp3nS	psát
Voltaire	Voltair	k1gInSc5	Voltair
d	d	k?	d
<g/>
́	́	k?	́
<g/>
Argentalovi	Argentalův	k2eAgMnPc1d1	Argentalův
roku	rok	k1gInSc2	rok
1765	[number]	k4	1765
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
A	a	k9	a
právě	právě	k9	právě
pro	pro	k7c4	pro
lid	lid	k1gInSc4	lid
je	být	k5eAaImIp3nS	být
dle	dle	k7c2	dle
Voltaira	Voltair	k1gInSc2	Voltair
náboženství	náboženství	k1gNnSc2	náboženství
nezbytně	zbytně	k6eNd1	zbytně
nutné	nutný	k2eAgNnSc4d1	nutné
<g/>
<g />
.	.	kIx.	.
</s>
<s>
"	"	kIx"	"
<g/>
Chci	chtít	k5eAaImIp1nS	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
můj	můj	k3xOp1gMnSc1	můj
správce	správce	k1gMnSc1	správce
<g/>
,	,	kIx,	,
můj	můj	k3xOp1gMnSc1	můj
krejčí	krejčí	k1gMnSc1	krejčí
<g/>
,	,	kIx,	,
lokaj	lokaj	k1gMnSc1	lokaj
a	a	k8xC	a
má	můj	k3xOp1gFnSc1	můj
žena	žena	k1gFnSc1	žena
věřili	věřit	k5eAaImAgMnP	věřit
v	v	k7c6	v
Boha	bůh	k1gMnSc2	bůh
<g/>
;	;	kIx,	;
myslím	myslet	k5eAaImIp1nS	myslet
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
budu	být	k5eAaImBp1nS	být
méně	málo	k6eAd2	málo
okrádán	okrádán	k2eAgMnSc1d1	okrádán
a	a	k8xC	a
méně	málo	k6eAd2	málo
podváděn	podváděn	k2eAgMnSc1d1	podváděn
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
píše	psát	k5eAaImIp3nS	psát
Voltaire	Voltair	k1gInSc5	Voltair
v	v	k7c4	v
práci	práce	k1gFnSc4	práce
"	"	kIx"	"
<g/>
Dialogy	dialog	k1gInPc1	dialog
mezi	mezi	k7c7	mezi
A	A	kA	A
<g/>
,	,	kIx,	,
B	B	kA	B
a	a	k8xC	a
C	C	kA	C
neboli	neboli	k8xC	neboli
ABCda	ABCda	k1gFnSc1	ABCda
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
A	A	kA	A
<g/>
,	,	kIx,	,
B	B	kA	B
<g/>
,	,	kIx,	,
C	C	kA	C
<g/>
,	,	kIx,	,
ou	ou	k0	ou
dialogues	dialogues	k1gMnSc1	dialogues
entre	entr	k1gInSc5	entr
A	A	kA	A
<g/>
,	,	kIx,	,
B	B	kA	B
<g/>
,	,	kIx,	,
C	C	kA	C
<g/>
;	;	kIx,	;
1769	[number]	k4	1769
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
práci	práce	k1gFnSc6	práce
"	"	kIx"	"
<g/>
Bůh	bůh	k1gMnSc1	bůh
a	a	k8xC	a
lidé	člověk	k1gMnPc1	člověk
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Dieu	Dieus	k1gInSc2	Dieus
et	et	k?	et
les	les	k1gInSc1	les
hommes	hommesa	k1gFnPc2	hommesa
<g/>
,	,	kIx,	,
1769	[number]	k4	1769
<g/>
)	)	kIx)	)
napsal	napsat	k5eAaBmAgInS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Žádná	žádný	k3yNgFnSc1	žádný
společnost	společnost	k1gFnSc1	společnost
nemůže	moct	k5eNaImIp3nS	moct
existovat	existovat	k5eAaImF	existovat
bez	bez	k7c2	bez
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
<g/>
;	;	kIx,	;
hlásejme	hlásat	k5eAaImRp1nP	hlásat
proto	proto	k8xC	proto
spravedlivého	spravedlivý	k2eAgMnSc2d1	spravedlivý
Boha	bůh	k1gMnSc2	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
zákon	zákon	k1gInSc1	zákon
státu	stát	k1gInSc2	stát
trestá	trestat	k5eAaImIp3nS	trestat
známé	známý	k2eAgInPc4d1	známý
zločiny	zločin	k1gInPc4	zločin
<g/>
,	,	kIx,	,
hlásejme	hlásat	k5eAaImRp1nP	hlásat
Boha	bůh	k1gMnSc4	bůh
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
bude	být	k5eAaImBp3nS	být
trestat	trestat	k5eAaImF	trestat
neznámé	známý	k2eNgInPc4d1	neznámý
zločiny	zločin	k1gInPc4	zločin
<g/>
.	.	kIx.	.
</s>
<s>
Filozof	filozof	k1gMnSc1	filozof
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
spinozistou	spinozistý	k2eAgFnSc4d1	spinozistý
<g/>
,	,	kIx,	,
když	když	k8xS	když
chce	chtít	k5eAaImIp3nS	chtít
-	-	kIx~	-
ale	ale	k8xC	ale
státník	státník	k1gMnSc1	státník
nechť	nechť	k9	nechť
je	být	k5eAaImIp3nS	být
theistou	theista	k1gMnSc7	theista
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Ateistická	ateistický	k2eAgFnSc1d1	ateistická
společnost	společnost	k1gFnSc1	společnost
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
možná	možná	k9	možná
jen	jen	k9	jen
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nS	kdyby
byla	být	k5eAaImAgFnS	být
složena	složen	k2eAgFnSc1d1	složena
z	z	k7c2	z
filozofů	filozof	k1gMnPc2	filozof
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
dbají	dbát	k5eAaImIp3nP	dbát
zákonů	zákon	k1gInPc2	zákon
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
rozumového	rozumový	k2eAgNnSc2d1	rozumové
nahlédnutí	nahlédnutí	k1gNnSc2	nahlédnutí
o	o	k7c6	o
jejich	jejich	k3xOp3gFnSc6	jejich
prospěšnosti	prospěšnost	k1gFnSc6	prospěšnost
<g/>
.	.	kIx.	.
</s>
<s>
Prostí	prostý	k2eAgMnPc1d1	prostý
lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
však	však	k9	však
nikdy	nikdy	k6eAd1	nikdy
filozofy	filozof	k1gMnPc4	filozof
nestanou	stanout	k5eNaPmIp3nP	stanout
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Pochybuji	pochybovat	k5eAaImIp1nS	pochybovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
tato	tento	k3xDgFnSc1	tento
třída	třída	k1gFnSc1	třída
občanů	občan	k1gMnPc2	občan
mít	mít	k5eAaImF	mít
někdy	někdy	k6eAd1	někdy
čas	čas	k1gInSc4	čas
se	se	k3xPyFc4	se
vzdělávat	vzdělávat	k5eAaImF	vzdělávat
<g/>
,	,	kIx,	,
umřeli	umřít	k5eAaPmAgMnP	umřít
by	by	kYmCp3nP	by
dříve	dříve	k6eAd2	dříve
hladem	hlad	k1gInSc7	hlad
<g/>
,	,	kIx,	,
než	než	k8xS	než
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
filozofy	filozof	k1gMnPc7	filozof
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
píše	psát	k5eAaImIp3nS	psát
Damilavillovi	Damilavill	k1gMnSc3	Damilavill
<g/>
.	.	kIx.	.
</s>
<s>
Profesor	profesor	k1gMnSc1	profesor
Drtina	drtina	k1gFnSc1	drtina
výstižně	výstižně	k6eAd1	výstižně
napsal	napsat	k5eAaPmAgInS	napsat
<g/>
,	,	kIx,	,
že	že	k8xS	že
dle	dle	k7c2	dle
Voltaira	Voltair	k1gInSc2	Voltair
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
náboženství	náboženství	k1gNnPc2	náboženství
uzdou	uzda	k1gFnSc7	uzda
v	v	k7c6	v
tlamě	tlama	k1gFnSc6	tlama
smyslného	smyslný	k2eAgInSc2d1	smyslný
davu	dav	k1gInSc2	dav
<g/>
.	.	kIx.	.
</s>
<s>
Voltaire	Voltair	k1gMnSc5	Voltair
sám	sám	k3xTgMnSc1	sám
Boha	bůh	k1gMnSc4	bůh
nepotřebuje	potřebovat	k5eNaImIp3nS	potřebovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
"	"	kIx"	"
<g/>
jeho	jeho	k3xOp3gMnSc1	jeho
krejčí	krejčí	k1gMnSc1	krejčí
ano	ano	k9	ano
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ho	on	k3xPp3gInSc4	on
neokrádal	okrádat	k5eNaImAgInS	okrádat
<g/>
.	.	kIx.	.
</s>
<s>
Voltaire	Voltair	k1gMnSc5	Voltair
snížil	snížit	k5eAaPmAgInS	snížit
prostě	prostě	k9	prostě
náboženství	náboženství	k1gNnSc4	náboženství
na	na	k7c4	na
policejní	policejní	k2eAgInSc4d1	policejní
prostředek	prostředek	k1gInSc4	prostředek
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Dle	dle	k7c2	dle
známého	známý	k2eAgInSc2d1	známý
Voltairova	Voltairův	k2eAgInSc2d1	Voltairův
bonmotu	bonmot	k1gInSc2	bonmot
"	"	kIx"	"
<g/>
kdyby	kdyby	kYmCp3nS	kdyby
Bůh	bůh	k1gMnSc1	bůh
neexistoval	existovat	k5eNaImAgMnS	existovat
<g/>
,	,	kIx,	,
museli	muset	k5eAaImAgMnP	muset
bychom	by	kYmCp1nP	by
jej	on	k3xPp3gNnSc4	on
vymyslet	vymyslet	k5eAaPmF	vymyslet
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
John	John	k1gMnSc1	John
Gray	Graa	k1gFnSc2	Graa
píše	psát	k5eAaImIp3nS	psát
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
Voltaira	Voltair	k1gMnSc4	Voltair
byla	být	k5eAaImAgFnS	být
moderní	moderní	k2eAgFnSc1d1	moderní
společnost	společnost	k1gFnSc1	společnost
neslučitelná	slučitelný	k2eNgFnSc1d1	neslučitelná
s	s	k7c7	s
mocnou	mocný	k2eAgFnSc7d1	mocná
církví	církev	k1gFnSc7	církev
a	a	k8xC	a
kultem	kult	k1gInSc7	kult
mystérií	mystérie	k1gFnPc2	mystérie
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgNnPc6	jenž
je	být	k5eAaImIp3nS	být
založeno	založen	k2eAgNnSc1d1	založeno
křesťanství	křesťanství	k1gNnSc1	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgFnSc1d1	moderní
společnost	společnost	k1gFnSc1	společnost
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
společností	společnost	k1gFnSc7	společnost
laiků	laik	k1gMnPc2	laik
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neměla	mít	k5eNaImAgFnS	mít
být	být	k5eAaImF	být
zbavena	zbaven	k2eAgFnSc1d1	zbavena
základního	základní	k2eAgInSc2d1	základní
náboženského	náboženský	k2eAgInSc2d1	náboženský
citu	cit	k1gInSc2	cit
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
"	"	kIx"	"
<g/>
Velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
jeho	on	k3xPp3gNnSc2	on
díla	dílo	k1gNnSc2	dílo
spočívá	spočívat	k5eAaImIp3nS	spočívat
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
vynalézt	vynalézt	k5eAaPmF	vynalézt
náboženství	náboženství	k1gNnSc4	náboženství
pro	pro	k7c4	pro
moderní	moderní	k2eAgMnPc4d1	moderní
lidi	člověk	k1gMnPc4	člověk
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
"	"	kIx"	"
Voltaira	Voltaira	k1gFnSc1	Voltaira
velmi	velmi	k6eAd1	velmi
tížil	tížit	k5eAaImAgInS	tížit
špatný	špatný	k2eAgInSc1d1	špatný
<g/>
,	,	kIx,	,
ba	ba	k9	ba
hanebný	hanebný	k2eAgInSc1d1	hanebný
stav	stav	k1gInSc1	stav
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
francouzské	francouzský	k2eAgFnSc2d1	francouzská
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
profesora	profesor	k1gMnSc2	profesor
Richarda	Richard	k1gMnSc2	Richard
H.	H.	kA	H.
Popkina	Popkin	k1gMnSc2	Popkin
"	"	kIx"	"
<g/>
velký	velký	k2eAgInSc1d1	velký
díl	díl	k1gInSc1	díl
zla	zlo	k1gNnSc2	zlo
připisoval	připisovat	k5eAaImAgInS	připisovat
náboženské	náboženský	k2eAgFnSc3d1	náboženská
tradici	tradice	k1gFnSc3	tradice
a	a	k8xC	a
institucím	instituce	k1gFnPc3	instituce
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
ovládaly	ovládat	k5eAaImAgFnP	ovládat
výchovu	výchova	k1gFnSc4	výchova
a	a	k8xC	a
kulturní	kulturní	k2eAgInSc4d1	kulturní
život	život	k1gInSc4	život
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
problému	problém	k1gInSc2	problém
vysledoval	vysledovat	k5eAaImAgInS	vysledovat
ke	k	k7c3	k
křesťanské	křesťanský	k2eAgFnSc3d1	křesťanská
víře	víra	k1gFnSc3	víra
<g/>
,	,	kIx,	,
k	k	k7c3	k
jejím	její	k3xOp3gFnPc3	její
institucím	instituce	k1gFnPc3	instituce
a	a	k8xC	a
k	k	k7c3	k
jejímu	její	k3xOp3gMnSc3	její
původci	původce	k1gMnSc3	původce
<g/>
,	,	kIx,	,
starověkému	starověký	k2eAgInSc3d1	starověký
judaismu	judaismus	k1gInSc3	judaismus
<g/>
.	.	kIx.	.
</s>
<s>
Voltaire	Voltair	k1gMnSc5	Voltair
zobrazoval	zobrazovat	k5eAaImAgMnS	zobrazovat
biblické	biblický	k2eAgNnSc1d1	biblické
náboženství	náboženství	k1gNnSc1	náboženství
po	po	k7c6	po
historické	historický	k2eAgFnSc6d1	historická
i	i	k8xC	i
teologické	teologický	k2eAgFnSc6d1	teologická
stránce	stránka	k1gFnSc6	stránka
jako	jako	k8xS	jako
zdroj	zdroj	k1gInSc4	zdroj
chorob	choroba	k1gFnPc2	choroba
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
postihly	postihnout	k5eAaPmAgFnP	postihnout
evropské	evropský	k2eAgNnSc4d1	Evropské
lidstvo	lidstvo	k1gNnSc4	lidstvo
<g/>
.	.	kIx.	.
</s>
<s>
Biblické	biblický	k2eAgFnPc1d1	biblická
dějiny	dějiny	k1gFnPc1	dějiny
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
starozákonní	starozákonní	k2eAgInSc1d1	starozákonní
<g/>
,	,	kIx,	,
popisoval	popisovat	k5eAaImAgInS	popisovat
jako	jako	k9	jako
skandální	skandální	k2eAgNnSc4d1	skandální
zachycení	zachycení	k1gNnSc4	zachycení
nelidské	lidský	k2eNgFnSc2d1	nelidská
nemravnosti	nemravnost	k1gFnSc2	nemravnost
<g/>
,	,	kIx,	,
pověrčivého	pověrčivý	k2eAgInSc2d1	pověrčivý
nevědomého	vědomý	k2eNgInSc2d1	nevědomý
přístupu	přístup	k1gInSc2	přístup
ke	k	k7c3	k
světu	svět	k1gInSc3	svět
a	a	k8xC	a
uctívání	uctívání	k1gNnSc3	uctívání
antropomorfní	antropomorfní	k2eAgFnSc2d1	antropomorfní
modly	modla	k1gFnSc2	modla
...	...	k?	...
Nelítostně	lítostně	k6eNd1	lítostně
tepal	tepat	k5eAaImAgMnS	tepat
náboženské	náboženský	k2eAgNnSc4d1	náboženské
stanovisko	stanovisko	k1gNnSc4	stanovisko
bible	bible	k1gFnSc2	bible
<g/>
.	.	kIx.	.
</s>
<s>
Teology	teolog	k1gMnPc4	teolog
chápal	chápat	k5eAaImAgMnS	chápat
jako	jako	k8xS	jako
původce	původce	k1gMnSc1	původce
racionalizované	racionalizovaný	k2eAgFnSc2d1	racionalizovaná
a	a	k8xC	a
institucionalizované	institucionalizovaný	k2eAgFnSc2d1	institucionalizovaná
verze	verze	k1gFnSc2	verze
této	tento	k3xDgFnSc2	tento
podivné	podivný	k2eAgFnSc2d1	podivná
<g/>
,	,	kIx,	,
zlovolné	zlovolný	k2eAgFnSc2d1	zlovolná
starověké	starověký	k2eAgFnSc2d1	starověká
nauky	nauka	k1gFnSc2	nauka
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
ji	on	k3xPp3gFnSc4	on
využili	využít	k5eAaPmAgMnP	využít
k	k	k7c3	k
ovládnutí	ovládnutí	k1gNnSc3	ovládnutí
a	a	k8xC	a
manipulaci	manipulace	k1gFnSc3	manipulace
lidu	lid	k1gInSc2	lid
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Voltairův	Voltairův	k2eAgInSc1d1	Voltairův
boj	boj	k1gInSc1	boj
proti	proti	k7c3	proti
křesťanství	křesťanství	k1gNnSc1	křesťanství
vyvrcholil	vyvrcholit	k5eAaPmAgInS	vyvrcholit
v	v	k7c6	v
době	doba	k1gFnSc6	doba
jeho	on	k3xPp3gInSc2	on
pobytu	pobyt	k1gInSc2	pobyt
ve	v	k7c4	v
Ferney	Fernea	k1gFnPc4	Fernea
(	(	kIx(	(
<g/>
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1758	[number]	k4	1758
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
cítil	cítit	k5eAaImAgMnS	cítit
dost	dost	k6eAd1	dost
v	v	k7c6	v
bezpečí	bezpečí	k1gNnSc6	bezpečí
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
boj	boj	k1gInSc1	boj
vede	vést	k5eAaImIp3nS	vést
nyní	nyní	k6eAd1	nyní
Voltaire	Voltair	k1gInSc5	Voltair
mnohem	mnohem	k6eAd1	mnohem
důsledněji	důsledně	k6eAd2	důsledně
a	a	k8xC	a
odvážněji	odvážně	k6eAd2	odvážně
<g/>
.	.	kIx.	.
</s>
<s>
Útočí	útočit	k5eAaImIp3nS	útočit
na	na	k7c6	na
bibli	bible	k1gFnSc6	bible
<g/>
,	,	kIx,	,
na	na	k7c4	na
evangelia	evangelium	k1gNnPc4	evangelium
<g/>
,	,	kIx,	,
na	na	k7c4	na
jejich	jejich	k3xOp3gFnSc4	jejich
autentičnost	autentičnost	k1gFnSc4	autentičnost
a	a	k8xC	a
hodnověrnost	hodnověrnost	k1gFnSc4	hodnověrnost
<g/>
.	.	kIx.	.
</s>
<s>
Provádí	provádět	k5eAaImIp3nS	provádět
kritiku	kritika	k1gFnSc4	kritika
historickou	historický	k2eAgFnSc4d1	historická
a	a	k8xC	a
filologickou	filologický	k2eAgFnSc4d1	filologická
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nP	snažit
se	se	k3xPyFc4	se
řešit	řešit	k5eAaImF	řešit
otázky	otázka	k1gFnPc4	otázka
<g/>
:	:	kIx,	:
Jak	jak	k6eAd1	jak
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
utvořila	utvořit	k5eAaPmAgFnS	utvořit
bible	bible	k1gFnSc1	bible
<g/>
?	?	kIx.	?
</s>
<s>
Jaká	jaký	k3yQgFnSc1	jaký
je	být	k5eAaImIp3nS	být
komposice	komposice	k1gFnSc1	komposice
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
jest	být	k5eAaImIp3nS	být
složena	složen	k2eAgFnSc1d1	složena
<g/>
?	?	kIx.	?
</s>
<s>
Kdy	kdy	k6eAd1	kdy
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
knihy	kniha	k1gFnPc1	kniha
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
<g/>
?	?	kIx.	?
</s>
<s>
Jaká	jaký	k3yQgFnSc1	jaký
je	být	k5eAaImIp3nS	být
relativní	relativní	k2eAgFnSc1d1	relativní
hodnota	hodnota	k1gFnSc1	hodnota
a	a	k8xC	a
autorita	autorita	k1gFnSc1	autorita
různých	různý	k2eAgFnPc2d1	různá
redakcí	redakce	k1gFnPc2	redakce
<g/>
?	?	kIx.	?
</s>
<s>
Voltaire	Voltair	k1gMnSc5	Voltair
tak	tak	k9	tak
nastolil	nastolit	k5eAaPmAgInS	nastolit
mnohá	mnohý	k2eAgNnPc4d1	mnohé
témata	téma	k1gNnPc4	téma
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
stala	stát	k5eAaPmAgFnS	stát
předmětem	předmět	k1gInSc7	předmět
vědecké	vědecký	k2eAgFnSc2d1	vědecká
historické	historický	k2eAgFnSc2d1	historická
kritiky	kritika	k1gFnSc2	kritika
bible	bible	k1gFnSc2	bible
a	a	k8xC	a
náboženství	náboženství	k1gNnSc2	náboženství
v	v	k7c6	v
XIX	XIX	kA	XIX
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
Dle	dle	k7c2	dle
Voltaira	Voltair	k1gInSc2	Voltair
v	v	k7c6	v
křesťanství	křesťanství	k1gNnSc6	křesťanství
není	být	k5eNaImIp3nS	být
nic	nic	k3yNnSc1	nic
původního	původní	k2eAgMnSc2d1	původní
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
odvar	odvar	k1gInSc1	odvar
východních	východní	k2eAgNnPc2d1	východní
náboženství	náboženství	k1gNnPc2	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Křesťanství	křesťanství	k1gNnSc1	křesťanství
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
uprostřed	uprostřed	k7c2	uprostřed
luzy	luza	k1gFnSc2	luza
jako	jako	k8xS	jako
tkanivo	tkanivo	k1gNnSc4	tkanivo
nesmyslů	nesmysl	k1gInPc2	nesmysl
určených	určený	k2eAgInPc2d1	určený
pro	pro	k7c4	pro
nízký	nízký	k2eAgInSc4d1	nízký
dav	dav	k1gInSc4	dav
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
v	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
tvořil	tvořit	k5eAaImAgMnS	tvořit
církev	církev	k1gFnSc4	církev
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Voltairově	Voltairův	k2eAgFnSc6d1	Voltairova
kritice	kritika	k1gFnSc6	kritika
bible	bible	k1gFnSc2	bible
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Starého	Starého	k2eAgInSc2d1	Starého
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
též	též	k9	též
projevuje	projevovat	k5eAaImIp3nS	projevovat
jeho	jeho	k3xOp3gInSc4	jeho
pohrdavý	pohrdavý	k2eAgInSc4d1	pohrdavý
a	a	k8xC	a
štítivý	štítivý	k2eAgInSc4d1	štítivý
odpor	odpor	k1gInSc4	odpor
proti	proti	k7c3	proti
Židům	Žid	k1gMnPc3	Žid
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
považoval	považovat	k5eAaImAgInS	považovat
za	za	k7c4	za
málo	málo	k6eAd1	málo
vzdělaný	vzdělaný	k2eAgMnSc1d1	vzdělaný
a	a	k8xC	a
pověrečný	pověrečný	k2eAgInSc1d1	pověrečný
arabský	arabský	k2eAgInSc1d1	arabský
kmen	kmen	k1gInSc1	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1762	[number]	k4	1762
vydal	vydat	k5eAaPmAgMnS	vydat
Voltaire	Voltair	k1gInSc5	Voltair
Testament	testament	k1gInSc4	testament
de	de	k?	de
J.	J.	kA	J.
Meslier	Meslier	k1gInSc1	Meslier
[	[	kIx(	[
<g/>
Závěť	závěť	k1gFnSc1	závěť
J.	J.	kA	J.
Mesliera	Mesliera	k1gFnSc1	Mesliera
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
spis	spis	k1gInSc4	spis
Jeana	Jean	k1gMnSc2	Jean
Mesliera	Meslier	k1gMnSc2	Meslier
(	(	kIx(	(
<g/>
1664	[number]	k4	1664
<g/>
-	-	kIx~	-
<g/>
1729	[number]	k4	1729
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
katolického	katolický	k2eAgMnSc4d1	katolický
kněze	kněz	k1gMnSc4	kněz
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
odřekl	odřeknout	k5eAaPmAgMnS	odřeknout
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
víry	víra	k1gFnSc2	víra
a	a	k8xC	a
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
"	"	kIx"	"
<g/>
Závěti	závěť	k1gFnSc2	závěť
<g/>
"	"	kIx"	"
vyvrací	vyvracet	k5eAaImIp3nP	vyvracet
křesťanství	křesťanství	k1gNnSc4	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Voltaire	Voltair	k1gMnSc5	Voltair
si	se	k3xPyFc3	se
tento	tento	k3xDgInSc4	tento
spis	spis	k1gInSc4	spis
velmi	velmi	k6eAd1	velmi
cenil	cenit	k5eAaImAgMnS	cenit
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
něhož	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
neobejdou	obejít	k5eNaPmIp3nP	obejít
démoni	démon	k1gMnPc1	démon
<g/>
,	,	kIx,	,
výtečný	výtečný	k2eAgInSc1d1	výtečný
katechismus	katechismus	k1gInSc1	katechismus
Belzebuba	Belzebub	k1gMnSc2	Belzebub
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
píše	psát	k5eAaImIp3nS	psát
hraběti	hrabě	k1gMnSc3	hrabě
d	d	k?	d
<g/>
́	́	k?	́
<g/>
Argental	Argental	k1gMnSc1	Argental
<g/>
.	.	kIx.	.
</s>
<s>
D	D	kA	D
<g/>
'	'	kIx"	'
<g/>
Alembertovi	Alembert	k1gMnSc3	Alembert
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Svědectví	svědectví	k1gNnSc1	svědectví
faráře	farář	k1gMnSc2	farář
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
umíraje	umírat	k5eAaImSgInS	umírat
prosí	prosit	k5eAaImIp3nS	prosit
Boha	bůh	k1gMnSc4	bůh
za	za	k7c4	za
odpuštění	odpuštění	k1gNnSc4	odpuštění
<g/>
,	,	kIx,	,
že	že	k8xS	že
hlásal	hlásat	k5eAaImAgInS	hlásat
křesťanství	křesťanství	k1gNnSc4	křesťanství
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
prokázat	prokázat	k5eAaPmF	prokázat
velikou	veliký	k2eAgFnSc4d1	veliká
službu	služba	k1gFnSc4	služba
volnomyšlenkářům	volnomyšlenkář	k1gMnPc3	volnomyšlenkář
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
A	a	k9	a
v	v	k7c6	v
dopise	dopis	k1gInSc6	dopis
Marmontelovi	Marmontelův	k2eAgMnPc1d1	Marmontelův
píše	psát	k5eAaImIp3nS	psát
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
To	ten	k3xDgNnSc1	ten
dílo	dílo	k1gNnSc1	dílo
na	na	k7c4	na
mne	já	k3xPp1nSc4	já
vždycky	vždycky	k6eAd1	vždycky
silně	silně	k6eAd1	silně
působilo	působit	k5eAaImAgNnS	působit
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
by	by	kYmCp3nP	by
je	on	k3xPp3gNnPc4	on
měli	mít	k5eAaImAgMnP	mít
znát	znát	k5eAaImF	znát
<g/>
...	...	k?	...
Musíme	muset	k5eAaImIp1nP	muset
sloužit	sloužit	k5eAaImF	sloužit
rozumu	rozum	k1gInSc2	rozum
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
sil	síla	k1gFnPc2	síla
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
náš	náš	k3xOp1gMnSc1	náš
král	král	k1gMnSc1	král
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
dosud	dosud	k6eAd1	dosud
mnoho	mnoho	k4c1	mnoho
nepřátel	nepřítel	k1gMnPc2	nepřítel
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Kritice	kritika	k1gFnSc3	kritika
křesťanství	křesťanství	k1gNnSc2	křesťanství
věnoval	věnovat	k5eAaPmAgInS	věnovat
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
zejména	zejména	k9	zejména
následující	následující	k2eAgFnPc4d1	následující
práce	práce	k1gFnPc4	práce
<g/>
:	:	kIx,	:
Sermon	sermon	k1gInSc1	sermon
des	des	k1gNnSc2	des
cinquante	cinquant	k1gMnSc5	cinquant
<g/>
,	,	kIx,	,
1762	[number]	k4	1762
[	[	kIx(	[
<g/>
Kázání	kázání	k1gNnPc4	kázání
<g />
.	.	kIx.	.
</s>
<s>
padesáti	padesát	k4xCc6	padesát
<g/>
]	]	kIx)	]
<g/>
;	;	kIx,	;
Saiil	Saiil	k1gMnSc1	Saiil
<g/>
,	,	kIx,	,
1763	[number]	k4	1763
[	[	kIx(	[
<g/>
Saul	Saul	k1gMnSc1	Saul
<g/>
]	]	kIx)	]
<g/>
;	;	kIx,	;
Dialogue	Dialogue	k1gInSc1	Dialogue
du	du	k?	du
chapon	chapon	k1gInSc1	chapon
et	et	k?	et
de	de	k?	de
la	la	k0	la
poularde	poulard	k1gMnSc5	poulard
<g/>
,	,	kIx,	,
1763	[number]	k4	1763
[	[	kIx(	[
<g/>
Rozhovor	rozhovor	k1gInSc1	rozhovor
kapouna	kapoun	k1gMnSc2	kapoun
a	a	k8xC	a
pularda	pulard	k1gMnSc2	pulard
<g/>
]	]	kIx)	]
<g/>
;	;	kIx,	;
Questions	Questions	k1gInSc1	Questions
sur	sur	k?	sur
les	les	k1gInSc1	les
miracles	miracles	k1gInSc1	miracles
(	(	kIx(	(
<g/>
1765	[number]	k4	1765
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
Otázky	otázka	k1gFnPc1	otázka
o	o	k7c6	o
zázracích	zázrak	k1gInPc6	zázrak
<g />
.	.	kIx.	.
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
;	;	kIx,	;
Examen	examen	k1gInSc1	examen
important	important	k1gMnSc1	important
de	de	k?	de
Milord	Milord	k1gMnSc1	Milord
Bolingbroke	Bolingbroke	k1gNnSc1	Bolingbroke
<g/>
,	,	kIx,	,
1767	[number]	k4	1767
[	[	kIx(	[
<g/>
Důležité	důležitý	k2eAgNnSc1d1	důležité
zkoumání	zkoumání	k1gNnSc1	zkoumání
mylorda	mylord	k1gMnSc2	mylord
Bolingbroka	Bolingbroek	k1gMnSc2	Bolingbroek
<g/>
]	]	kIx)	]
<g/>
;	;	kIx,	;
Le	Le	k1gMnSc1	Le
Dîner	Dîner	k1gMnSc1	Dîner
du	du	k?	du
Comte	Comt	k1gInSc5	Comt
de	de	k?	de
Boulainvilliers	Boulainvilliers	k1gInSc1	Boulainvilliers
(	(	kIx(	(
<g/>
1767	[number]	k4	1767
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
Oběd	oběd	k1gInSc1	oběd
u	u	k7c2	u
hraběte	hrabě	k1gMnSc2	hrabě
de	de	k?	de
Boulainvilliers	Boulainvilliers	k1gInSc1	Boulainvilliers
<g/>
]	]	kIx)	]
<g/>
;	;	kIx,	;
Dieu	Dieus	k1gInSc2	Dieus
et	et	k?	et
les	les	k1gInSc1	les
hommes	hommes	k1gInSc1	hommes
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
1769	[number]	k4	1769
[	[	kIx(	[
<g/>
Bůh	bůh	k1gMnSc1	bůh
a	a	k8xC	a
lidé	člověk	k1gMnPc1	člověk
<g/>
]	]	kIx)	]
<g/>
;	;	kIx,	;
Collection	Collection	k1gInSc1	Collection
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
anciens	anciens	k1gInSc1	anciens
Évangiles	Évangilesa	k1gFnPc2	Évangilesa
<g/>
,	,	kIx,	,
1769	[number]	k4	1769
[	[	kIx(	[
<g/>
Sbírka	sbírka	k1gFnSc1	sbírka
starých	starý	k2eAgNnPc2d1	staré
evangelií	evangelium	k1gNnPc2	evangelium
<g/>
]	]	kIx)	]
<g/>
;	;	kIx,	;
La	la	k1gNnSc1	la
Bible	bible	k1gFnSc2	bible
enfin	enfina	k1gFnPc2	enfina
expliquée	expliqué	k1gFnSc2	expliqué
<g/>
,	,	kIx,	,
1776	[number]	k4	1776
[	[	kIx(	[
<g/>
Bible	bible	k1gFnSc1	bible
konečně	konečně	k6eAd1	konečně
vysvětlená	vysvětlený	k2eAgFnSc1d1	vysvětlená
<g/>
]	]	kIx)	]
<g/>
;	;	kIx,	;
Un	Un	k1gFnSc1	Un
Chrétien	Chrétina	k1gFnPc2	Chrétina
contre	contr	k1gInSc5	contr
six	six	k?	six
Juifs	Juifsa	k1gFnPc2	Juifsa
<g/>
,	,	kIx,	,
1776	[number]	k4	1776
[	[	kIx(	[
<g/>
Jeden	jeden	k4xCgMnSc1	jeden
křesťan	křesťan	k1gMnSc1	křesťan
proti	proti	k7c3	proti
šesti	šest	k4xCc6	šest
Židům	Žid	k1gMnPc3	Žid
<g/>
]	]	kIx)	]
<g/>
;	;	kIx,	;
Histoire	Histoir	k1gMnSc5	Histoir
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
établissement	établissement	k1gMnSc1	établissement
du	du	k?	du
christianisme	christianismus	k1gInSc5	christianismus
<g/>
,	,	kIx,	,
1777	[number]	k4	1777
[	[	kIx(	[
<g/>
Dějiny	dějiny	k1gFnPc1	dějiny
vzniku	vznik	k1gInSc2	vznik
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Křesťanství	křesťanství	k1gNnSc4	křesťanství
ovšem	ovšem	k9	ovšem
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
i	i	k9	i
ve	v	k7c6	v
Slovníku	slovník	k1gInSc6	slovník
filozofickém	filozofický	k2eAgInSc6d1	filozofický
(	(	kIx(	(
<g/>
Dictionnaire	Dictionnair	k1gMnSc5	Dictionnair
philosophique	philosophiquus	k1gMnSc5	philosophiquus
portatif	portatif	k1gMnSc1	portatif
<g/>
,	,	kIx,	,
1764	[number]	k4	1764
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
pracích	práce	k1gFnPc6	práce
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
práce	práce	k1gFnPc1	práce
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
silně	silně	k6eAd1	silně
protikřesťanské	protikřesťanský	k2eAgFnPc1d1	protikřesťanská
<g/>
,	,	kIx,	,
Voltaire	Voltair	k1gInSc5	Voltair
nepodepisoval	podepisovat	k5eNaImAgMnS	podepisovat
svým	svůj	k3xOyFgNnSc7	svůj
jménem	jméno	k1gNnSc7	jméno
<g/>
,	,	kIx,	,
používal	používat	k5eAaImAgInS	používat
četných	četný	k2eAgInPc2d1	četný
pseudonymů	pseudonym	k1gInPc2	pseudonym
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
životopisců	životopisec	k1gMnPc2	životopisec
vypočetl	vypočíst	k5eAaPmAgMnS	vypočíst
37	[number]	k4	37
pseudonymů	pseudonym	k1gInPc2	pseudonym
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgInPc7	jenž
se	se	k3xPyFc4	se
maskoval	maskovat	k5eAaBmAgMnS	maskovat
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
ovšem	ovšem	k9	ovšem
poznali	poznat	k5eAaPmAgMnP	poznat
jeho	jeho	k3xOp3gNnPc4	jeho
autorství	autorství	k1gNnPc4	autorství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Voltaire	Voltair	k1gInSc5	Voltair
to	ten	k3xDgNnSc4	ten
důrazně	důrazně	k6eAd1	důrazně
popíral	popírat	k5eAaImAgMnS	popírat
<g/>
;	;	kIx,	;
nikoho	nikdo	k3yNnSc4	nikdo
to	ten	k3xDgNnSc4	ten
však	však	k9	však
zpravidla	zpravidla	k6eAd1	zpravidla
neoklamalo	oklamat	k5eNaPmAgNnS	oklamat
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
spisů	spis	k1gInPc2	spis
bylo	být	k5eAaImAgNnS	být
odsouzeno	odsoudit	k5eAaPmNgNnS	odsoudit
ke	k	k7c3	k
spálení	spálení	k1gNnSc3	spálení
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
profesora	profesor	k1gMnSc2	profesor
Weischedela	Weischedel	k1gMnSc2	Weischedel
"	"	kIx"	"
<g/>
Voltairovým	Voltairův	k2eAgMnSc7d1	Voltairův
hlavním	hlavní	k2eAgMnSc7d1	hlavní
protivníkem	protivník	k1gMnSc7	protivník
je	být	k5eAaImIp3nS	být
církev	církev	k1gFnSc1	církev
<g/>
.	.	kIx.	.
</s>
<s>
Nikdy	nikdy	k6eAd1	nikdy
není	být	k5eNaImIp3nS	být
unaven	unavit	k5eAaPmNgMnS	unavit
dokazováním	dokazování	k1gNnSc7	dokazování
podivností	podivnost	k1gFnSc7	podivnost
křesťanského	křesťanský	k2eAgNnSc2d1	křesťanské
učení	učení	k1gNnSc2	učení
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
nebezpečného	bezpečný	k2eNgInSc2d1	nebezpečný
obrazu	obraz	k1gInSc2	obraz
svatých	svatý	k2eAgFnPc2d1	svatá
lží	lež	k1gFnPc2	lež
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgFnPc7	jenž
je	být	k5eAaImIp3nS	být
země	země	k1gFnSc1	země
naplněná	naplněný	k2eAgFnSc1d1	naplněná
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Církev	církev	k1gFnSc1	církev
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
místo	místo	k1gNnSc4	místo
rozumného	rozumný	k2eAgMnSc2d1	rozumný
Boha	bůh	k1gMnSc2	bůh
"	"	kIx"	"
<g/>
monstrum	monstrum	k1gNnSc1	monstrum
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
musíme	muset	k5eAaImIp1nP	muset
nenávidět	nenávidět	k5eAaImF	nenávidět
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Bůh	bůh	k1gMnSc1	bůh
stvořil	stvořit	k5eAaPmAgMnS	stvořit
svět	svět	k1gInSc4	svět
a	a	k8xC	a
pak	pak	k6eAd1	pak
ho	on	k3xPp3gMnSc4	on
utopil	utopit	k5eAaPmAgMnS	utopit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
lepší	dobrý	k2eAgNnSc4d2	lepší
pokolení	pokolení	k1gNnSc4	pokolení
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ho	on	k3xPp3gMnSc4	on
zabydlel	zabydlet	k5eAaPmAgMnS	zabydlet
loupežníky	loupežník	k1gMnPc4	loupežník
a	a	k8xC	a
tyrany	tyran	k1gMnPc4	tyran
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
potom	potom	k6eAd1	potom
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
utopil	utopit	k5eAaPmAgMnS	utopit
otce	otec	k1gMnSc4	otec
<g/>
,	,	kIx,	,
zemřel	zemřít	k5eAaPmAgMnS	zemřít
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gFnPc4	jejich
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
bez	bez	k7c2	bez
úspěchu	úspěch	k1gInSc2	úspěch
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
potrestal	potrestat	k5eAaPmAgInS	potrestat
stovky	stovka	k1gFnPc4	stovka
národů	národ	k1gInPc2	národ
za	za	k7c4	za
nevědomost	nevědomost	k1gFnSc4	nevědomost
o	o	k7c6	o
své	svůj	k3xOyFgFnSc6	svůj
smrti	smrt	k1gFnSc6	smrt
na	na	k7c6	na
kříži	kříž	k1gInSc6	kříž
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
je	být	k5eAaImIp3nS	být
přece	přece	k9	přece
sám	sám	k3xTgMnSc1	sám
udržoval	udržovat	k5eAaImAgMnS	udržovat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Tento	tento	k3xDgMnSc1	tento
Vládce	vládce	k1gMnSc1	vládce
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
<g />
.	.	kIx.	.
</s>
<s>
má	mít	k5eAaImIp3nS	mít
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
spravedlností	spravedlnost	k1gFnSc7	spravedlnost
<g/>
,	,	kIx,	,
v	v	k7c6	v
rozmařilé	rozmařilý	k2eAgFnSc6d1	rozmařilá
plnosti	plnost	k1gFnSc6	plnost
<g/>
,	,	kIx,	,
tento	tento	k3xDgMnSc1	tento
Otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
své	svůj	k3xOyFgFnPc4	svůj
děti	dítě	k1gFnPc4	dítě
nekonečně	konečně	k6eNd1	konečně
miluje	milovat	k5eAaImIp3nS	milovat
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc4	tento
Všemohoucí	Všemohoucí	k1gMnSc1	Všemohoucí
stvořil	stvořit	k5eAaPmAgMnS	stvořit
prý	prý	k9	prý
tyto	tento	k3xDgFnPc4	tento
bytosti	bytost	k1gFnPc4	bytost
podle	podle	k7c2	podle
svého	svůj	k3xOyFgInSc2	svůj
obrazu	obraz	k1gInSc2	obraz
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
je	on	k3xPp3gMnPc4	on
hned	hned	k6eAd1	hned
nato	nato	k6eAd1	nato
nechal	nechat	k5eAaPmAgMnS	nechat
uvést	uvést	k5eAaPmF	uvést
zlým	zlý	k1gMnSc7	zlý
duchem	duch	k1gMnSc7	duch
do	do	k7c2	do
pokušení	pokušení	k1gNnSc2	pokušení
a	a	k8xC	a
tomuto	tento	k3xDgNnSc3	tento
pokušení	pokušení	k1gNnSc3	pokušení
je	být	k5eAaImIp3nS	být
nechal	nechat	k5eAaPmAgMnS	nechat
podlehnout	podlehnout	k5eAaPmF	podlehnout
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
pak	pak	k6eAd1	pak
tyto	tento	k3xDgFnPc1	tento
bytosti	bytost	k1gFnPc1	bytost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
stvořil	stvořit	k5eAaPmAgMnS	stvořit
jako	jako	k8xC	jako
nesmrtelné	nesmrtelný	k1gMnPc4	nesmrtelný
<g/>
,	,	kIx,	,
nechal	nechat	k5eAaPmAgMnS	nechat
umřít	umřít	k5eAaPmF	umřít
a	a	k8xC	a
aby	aby	kYmCp3nS	aby
jejich	jejich	k3xOp3gNnSc4	jejich
potomstvo	potomstvo	k1gNnSc4	potomstvo
zahrnul	zahrnout	k5eAaPmAgInS	zahrnout
neštěstím	neštěstí	k1gNnSc7	neštěstí
a	a	k8xC	a
zločinem	zločin	k1gInSc7	zločin
<g/>
?	?	kIx.	?
</s>
<s>
A	a	k9	a
to	ten	k3xDgNnSc1	ten
ještě	ještě	k9	ještě
není	být	k5eNaImIp3nS	být
ten	ten	k3xDgInSc4	ten
nejvíce	hodně	k6eAd3	hodně
pobuřující	pobuřující	k2eAgInSc4d1	pobuřující
rozpor	rozpor	k1gInSc4	rozpor
pro	pro	k7c4	pro
náš	náš	k3xOp1gInSc4	náš
nechápající	chápající	k2eNgInSc4d1	nechápající
rozum	rozum	k1gInSc4	rozum
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
může	moct	k5eAaImIp3nS	moct
Bůh	bůh	k1gMnSc1	bůh
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
později	pozdě	k6eAd2	pozdě
spasil	spasit	k5eAaPmAgMnS	spasit
lidský	lidský	k2eAgInSc4d1	lidský
rod	rod	k1gInSc4	rod
smrtí	smrt	k1gFnPc2	smrt
svého	svůj	k3xOyFgMnSc2	svůj
jediného	jediný	k2eAgMnSc2d1	jediný
Syna	syn	k1gMnSc2	syn
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
přesněji	přesně	k6eAd2	přesně
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
sám	sám	k3xTgMnSc1	sám
stal	stát	k5eAaPmAgMnS	stát
člověkem	člověk	k1gMnSc7	člověk
a	a	k8xC	a
za	za	k7c4	za
lidstvo	lidstvo	k1gNnSc4	lidstvo
umřel	umřít	k5eAaPmAgMnS	umřít
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
tedy	tedy	k9	tedy
může	moct	k5eAaImIp3nS	moct
vystavit	vystavit	k5eAaPmF	vystavit
svou	svůj	k3xOyFgFnSc7	svůj
vlastní	vlastní	k2eAgFnSc7d1	vlastní
smrtí	smrt	k1gFnSc7	smrt
skoro	skoro	k6eAd1	skoro
celý	celý	k2eAgInSc1d1	celý
lidský	lidský	k2eAgInSc1d1	lidský
rod	rod	k1gInSc1	rod
hrůzám	hrůza	k1gFnPc3	hrůza
věčného	věčný	k2eAgNnSc2d1	věčné
trápení	trápení	k1gNnSc2	trápení
<g/>
?	?	kIx.	?
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
posuzuje	posuzovat	k5eAaImIp3nS	posuzovat
toto	tento	k3xDgNnSc4	tento
učení	učení	k1gNnSc4	učení
filosof	filosof	k1gMnSc1	filosof
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
zajisté	zajisté	k9	zajisté
obludné	obludný	k2eAgNnSc1d1	obludné
a	a	k8xC	a
odpuzující	odpuzující	k2eAgNnSc1d1	odpuzující
<g/>
.	.	kIx.	.
</s>
<s>
Neboť	neboť	k8xC	neboť
dělá	dělat	k5eAaImIp3nS	dělat
z	z	k7c2	z
Boha	bůh	k1gMnSc2	bůh
zlo	zlo	k1gNnSc4	zlo
samo	sám	k3xTgNnSc1	sám
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
To	ten	k3xDgNnSc1	ten
jsou	být	k5eAaImIp3nP	být
dva	dva	k4xCgInPc1	dva
příklady	příklad	k1gInPc1	příklad
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
<g/>
.	.	kIx.	.
</s>
<s>
Voltaire	Voltair	k1gMnSc5	Voltair
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
strhnout	strhnout	k5eAaPmF	strhnout
křesťanskému	křesťanský	k2eAgNnSc3d1	křesťanské
učení	učení	k1gNnSc3	učení
masku	maska	k1gFnSc4	maska
jakožto	jakožto	k8xS	jakožto
pověře	pověra	k1gFnSc6	pověra
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
vládla	vládnout	k5eAaImAgFnS	vládnout
celé	celý	k2eAgFnPc4d1	celá
církevní	církevní	k2eAgFnPc4d1	církevní
dějiny	dějiny	k1gFnPc4	dějiny
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Pověra	pověra	k1gFnSc1	pověra
<g/>
,	,	kIx,	,
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
v	v	k7c6	v
pohanství	pohanství	k1gNnSc6	pohanství
a	a	k8xC	a
převzatá	převzatý	k2eAgFnSc1d1	převzatá
židovstvím	židovství	k1gNnSc7	židovství
<g/>
,	,	kIx,	,
zamořila	zamořit	k5eAaPmAgFnS	zamořit
křesťanskou	křesťanský	k2eAgFnSc4d1	křesťanská
církev	církev	k1gFnSc4	církev
od	od	k7c2	od
samého	samý	k3xTgInSc2	samý
počátku	počátek	k1gInSc2	počátek
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
směšná	směšný	k2eAgNnPc1d1	směšné
a	a	k8xC	a
hodná	hodný	k2eAgNnPc1d1	hodné
opovržení	opovržení	k1gNnPc1	opovržení
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
nejhorší	zlý	k2eAgMnSc1d3	nejhorší
nepřítel	nepřítel	k1gMnSc1	nepřítel
čistého	čistý	k2eAgNnSc2d1	čisté
uctívání	uctívání	k1gNnSc2	uctívání
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
jsme	být	k5eAaImIp1nP	být
dlužni	dlužen	k2eAgMnPc1d1	dlužen
Nejvyšší	vysoký	k2eAgFnPc1d3	nejvyšší
bytosti	bytost	k1gFnSc3	bytost
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
dopisu	dopis	k1gInSc6	dopis
pruskému	pruský	k2eAgMnSc3d1	pruský
králi	král	k1gMnSc3	král
píše	psát	k5eAaImIp3nS	psát
Voltaire	Voltair	k1gInSc5	Voltair
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1767	[number]	k4	1767
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Dokud	dokud	k8xS	dokud
budou	být	k5eAaImBp3nP	být
darebáci	darebák	k1gMnPc1	darebák
a	a	k8xC	a
hlupáci	hlupák	k1gMnPc1	hlupák
<g/>
,	,	kIx,	,
budou	být	k5eAaImBp3nP	být
i	i	k9	i
náboženství	náboženství	k1gNnPc1	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Naše	náš	k3xOp1gNnSc1	náš
je	být	k5eAaImIp3nS	být
nepochybně	pochybně	k6eNd1	pochybně
nejsměšnější	směšný	k2eAgFnPc4d3	nejsměšnější
<g/>
,	,	kIx,	,
nejabsurdnější	absurdní	k2eAgFnPc4d3	nejabsurdnější
a	a	k8xC	a
nejkrvavější	krvavý	k2eAgFnPc4d3	nejkrvavější
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
kdy	kdy	k6eAd1	kdy
nakazilo	nakazit	k5eAaPmAgNnS	nakazit
svět	svět	k1gInSc4	svět
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Voltaire	Voltair	k1gMnSc5	Voltair
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
křesťanské	křesťanský	k2eAgNnSc1d1	křesťanské
náboženství	náboženství	k1gNnSc1	náboženství
stálo	stát	k5eAaImAgNnS	stát
lidstvo	lidstvo	k1gNnSc4	lidstvo
více	hodně	k6eAd2	hodně
než	než	k8xS	než
sedmnáct	sedmnáct	k4xCc1	sedmnáct
milionů	milion	k4xCgInPc2	milion
lidských	lidský	k2eAgMnPc2d1	lidský
životů	život	k1gInPc2	život
<g/>
,	,	kIx,	,
počítáme	počítat	k5eAaImIp1nP	počítat
<g/>
-li	i	k?	-li
jeden	jeden	k4xCgMnSc1	jeden
milion	milion	k4xCgInSc1	milion
za	za	k7c4	za
století	století	k1gNnSc4	století
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
Voltaira	Voltair	k1gInSc2	Voltair
je	být	k5eAaImIp3nS	být
i	i	k9	i
islám	islám	k1gInSc1	islám
lepší	dobrý	k2eAgInSc1d2	lepší
náboženství	náboženství	k1gNnSc2	náboženství
než	než	k8xS	než
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Voltaire	Voltair	k1gMnSc5	Voltair
nepopírá	popírat	k5eNaImIp3nS	popírat
agresivitu	agresivita	k1gFnSc4	agresivita
islámu	islám	k1gInSc2	islám
v	v	k7c6	v
době	doba	k1gFnSc6	doba
jeho	jeho	k3xOp3gInSc2	jeho
vzniku	vznik	k1gInSc2	vznik
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
avšak	avšak	k8xC	avšak
pro	pro	k7c4	pro
pozdější	pozdní	k2eAgNnSc4d2	pozdější
období	období	k1gNnSc4	období
chválí	chválit	k5eAaImIp3nS	chválit
jeho	jeho	k3xOp3gFnSc4	jeho
toleranci	tolerance	k1gFnSc4	tolerance
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
zřejmě	zřejmě	k6eAd1	zřejmě
zakládá	zakládat	k5eAaImIp3nS	zakládat
na	na	k7c6	na
faktu	fakt	k1gInSc6	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
islám	islám	k1gInSc1	islám
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
si	se	k3xPyFc3	se
jeho	jeho	k3xOp3gMnPc1	jeho
vyznavači	vyznavač	k1gMnPc1	vyznavač
podrobili	podrobit	k5eAaPmAgMnP	podrobit
<g/>
,	,	kIx,	,
činil	činit	k5eAaImAgInS	činit
na	na	k7c6	na
jinověrce	jinověrka	k1gFnSc6	jinověrka
většinou	většinou	k6eAd1	většinou
jen	jen	k9	jen
hospodářský	hospodářský	k2eAgInSc4d1	hospodářský
nátlak	nátlak	k1gInSc4	nátlak
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
něj	on	k3xPp3gMnSc2	on
podle	podle	k7c2	podle
Voltaira	Voltair	k1gInSc2	Voltair
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
zpočátku	zpočátku	k6eAd1	zpočátku
šířené	šířený	k2eAgNnSc1d1	šířené
v	v	k7c6	v
pokoře	pokora	k1gFnSc6	pokora
a	a	k8xC	a
pokoji	pokoj	k1gInSc6	pokoj
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
později	pozdě	k6eAd2	pozdě
tím	ten	k3xDgInSc7	ten
nejméně	málo	k6eAd3	málo
tolerantním	tolerantní	k2eAgInSc7d1	tolerantní
a	a	k8xC	a
nejbarbarštějším	barbarský	k2eAgInSc7d3	barbarský
<g/>
.	.	kIx.	.
</s>
<s>
Ono	onen	k3xDgNnSc1	onen
také	také	k9	také
zavedlo	zavést	k5eAaPmAgNnS	zavést
náboženské	náboženský	k2eAgFnPc4d1	náboženská
války	válka	k1gFnPc4	válka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
charakteristické	charakteristický	k2eAgInPc1d1	charakteristický
nikoli	nikoli	k9	nikoli
pro	pro	k7c4	pro
pohanství	pohanství	k1gNnSc4	pohanství
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
pro	pro	k7c4	pro
křesťanství	křesťanství	k1gNnSc4	křesťanství
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
tažením	tažení	k1gNnSc7	tažení
proti	proti	k7c3	proti
křesťanství	křesťanství	k1gNnSc3	křesťanství
vede	vést	k5eAaImIp3nS	vést
Voltaire	Voltair	k1gInSc5	Voltair
boj	boj	k1gInSc4	boj
s	s	k7c7	s
fanatismem	fanatismus	k1gInSc7	fanatismus
<g/>
,	,	kIx,	,
s	s	k7c7	s
náboženskou	náboženský	k2eAgFnSc7d1	náboženská
nesnášenlivostí	nesnášenlivost	k1gFnSc7	nesnášenlivost
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
bojem	boj	k1gInSc7	boj
jsou	být	k5eAaImIp3nP	být
spojeny	spojit	k5eAaPmNgInP	spojit
známé	známý	k2eAgInPc1d1	známý
a	a	k8xC	a
snad	snad	k9	snad
i	i	k9	i
nejzáslužnější	záslužní	k2eAgFnPc4d3	záslužní
Voltairovy	Voltairův	k2eAgFnPc4d1	Voltairova
práce	práce	k1gFnPc4	práce
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
napsal	napsat	k5eAaBmAgMnS	napsat
na	na	k7c4	na
obranu	obrana	k1gFnSc4	obrana
obětí	oběť	k1gFnPc2	oběť
fanatismu	fanatismus	k1gInSc2	fanatismus
a	a	k8xC	a
nesnášenlivosti	nesnášenlivost	k1gFnSc2	nesnášenlivost
<g/>
:	:	kIx,	:
Traité	Traitý	k2eAgNnSc1d1	Traité
sur	sur	k?	sur
la	la	k1gNnSc1	la
Tolérance	Tolérance	k1gFnSc1	Tolérance
<g/>
,	,	kIx,	,
à	à	k?	à
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
occasion	occasion	k1gInSc1	occasion
de	de	k?	de
la	la	k1gNnSc1	la
mort	mort	k1gMnSc1	mort
de	de	k?	de
Jean	Jean	k1gMnSc1	Jean
Calas	Calas	k1gMnSc1	Calas
<g/>
,	,	kIx,	,
1763	[number]	k4	1763
[	[	kIx(	[
<g/>
Pojednání	pojednání	k1gNnSc1	pojednání
o	o	k7c6	o
snášenlivosti	snášenlivost	k1gFnSc6	snášenlivost
<g />
.	.	kIx.	.
</s>
<s>
napsané	napsaný	k2eAgNnSc4d1	napsané
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
smrti	smrt	k1gFnSc2	smrt
Jana	Jan	k1gMnSc2	Jan
Calase	Calasa	k1gFnSc6	Calasa
<g/>
]	]	kIx)	]
<g/>
;	;	kIx,	;
Relation	Relation	k1gInSc1	Relation
de	de	k?	de
la	la	k1gNnSc1	la
mort	mort	k1gMnSc1	mort
du	du	k?	du
chevalier	chevalier	k1gMnSc1	chevalier
de	de	k?	de
La	la	k0	la
Barre	Barr	k1gMnSc5	Barr
<g/>
,	,	kIx,	,
1766	[number]	k4	1766
[	[	kIx(	[
<g/>
Zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
smrti	smrt	k1gFnSc6	smrt
rytíře	rytíř	k1gMnSc2	rytíř
de	de	k?	de
La	la	k1gNnSc1	la
Barre	Barr	k1gInSc5	Barr
<g/>
]	]	kIx)	]
<g/>
;	;	kIx,	;
La	la	k1gNnSc1	la
méprise	méprise	k1gFnSc2	méprise
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Arras	Arras	k1gMnSc1	Arras
<g/>
,	,	kIx,	,
1771	[number]	k4	1771
[	[	kIx(	[
<g/>
Omyl	omýt	k5eAaPmAgMnS	omýt
arrasský	arrasský	k1gMnSc1	arrasský
<g/>
]	]	kIx)	]
<g/>
;	;	kIx,	;
Prix	Prix	k1gInSc1	Prix
de	de	k?	de
la	la	k1gNnSc7	la
Justice	justice	k1gFnSc2	justice
et	et	k?	et
de	de	k?	de
l	l	kA	l
<g/>
́	́	k?	́
<g/>
Humanité	Humanitý	k2eAgInPc1d1	Humanitý
<g/>
,	,	kIx,	,
1777	[number]	k4	1777
[	[	kIx(	[
<g/>
Cena	cena	k1gFnSc1	cena
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
a	a	k8xC	a
lidskosti	lidskost	k1gFnSc2	lidskost
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
Voltaire	Voltair	k1gInSc5	Voltair
nehlásal	hlásat	k5eNaImAgMnS	hlásat
ateismus	ateismus	k1gInSc4	ateismus
a	a	k8xC	a
uznával	uznávat	k5eAaImAgInS	uznávat
i	i	k9	i
potřebnost	potřebnost	k1gFnSc4	potřebnost
obrozeného	obrozený	k2eAgNnSc2d1	obrozené
<g/>
,	,	kIx,	,
rozumného	rozumný	k2eAgNnSc2d1	rozumné
náboženství	náboženství	k1gNnSc2	náboženství
pro	pro	k7c4	pro
lid	lid	k1gInSc4	lid
<g/>
,	,	kIx,	,
přece	přece	k9	přece
jen	jen	k9	jen
značně	značně	k6eAd1	značně
přispěl	přispět	k5eAaPmAgMnS	přispět
k	k	k7c3	k
otřesení	otřesení	k1gNnSc3	otřesení
pozic	pozice	k1gFnPc2	pozice
náboženské	náboženský	k2eAgFnSc2d1	náboženská
víry	víra	k1gFnSc2	víra
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
profesora	profesor	k1gMnSc2	profesor
Lansona	Lanson	k1gMnSc2	Lanson
"	"	kIx"	"
<g/>
on	on	k3xPp3gMnSc1	on
učil	učít	k5eAaPmAgMnS	učít
nevěřiti	věřit	k5eNaImF	věřit
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
však	však	k9	však
zacházeti	zacházet	k5eAaImF	zacházet
s	s	k7c7	s
vírou	víra	k1gFnSc7	víra
jako	jako	k8xS	jako
s	s	k7c7	s
hloupostí	hloupost	k1gFnSc7	hloupost
a	a	k8xC	a
s	s	k7c7	s
věřícím	věřící	k2eAgMnSc7d1	věřící
jako	jako	k8xC	jako
s	s	k7c7	s
hlupákem	hlupák	k1gMnSc7	hlupák
<g/>
.	.	kIx.	.
...	...	k?	...
V	v	k7c6	v
hnutí	hnutí	k1gNnSc6	hnutí
rozumovém	rozumový	k2eAgNnSc6d1	rozumové
směřuje	směřovat	k5eAaImIp3nS	směřovat
Voltaire	Voltair	k1gInSc5	Voltair
hlavně	hlavně	k9	hlavně
ku	k	k7c3	k
rozšíření	rozšíření	k1gNnSc3	rozšíření
nevěry	nevěra	k1gFnSc2	nevěra
od	od	k7c2	od
nejhořejších	horní	k2eAgFnPc2d3	nejhornější
až	až	k6eAd1	až
ku	k	k7c3	k
nejdolejším	dolní	k2eAgFnPc3d3	dolní
vrstvám	vrstva	k1gFnPc3	vrstva
společnosti	společnost	k1gFnSc2	společnost
francouzské	francouzský	k2eAgNnSc4d1	francouzské
<g/>
.	.	kIx.	.
</s>
<s>
Šlechta	šlechta	k1gFnSc1	šlechta
byla	být	k5eAaImAgFnS	být
událostmi	událost	k1gFnPc7	událost
přivedena	přiveden	k2eAgFnSc1d1	přivedena
nazpět	nazpět	k6eAd1	nazpět
k	k	k7c3	k
víře	víra	k1gFnSc3	víra
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
měšťanstvo	měšťanstvo	k1gNnSc1	měšťanstvo
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
v	v	k7c6	v
celku	celek	k1gInSc6	celek
voltarianským	voltarianský	k2eAgMnSc7d1	voltarianský
a	a	k8xC	a
lid	lid	k1gInSc1	lid
se	se	k3xPyFc4	se
jím	on	k3xPp3gMnSc7	on
stal	stát	k5eAaPmAgMnS	stát
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
skutečně	skutečně	k6eAd1	skutečně
Voltaire	Voltair	k1gInSc5	Voltair
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
zabil	zabít	k5eAaPmAgMnS	zabít
náboženství	náboženství	k1gNnSc4	náboženství
...	...	k?	...
<g/>
"	"	kIx"	"
Voltaire	Voltair	k1gMnSc5	Voltair
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
jako	jako	k9	jako
katolík	katolík	k1gMnSc1	katolík
a	a	k8xC	a
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
koleji	kolej	k1gFnSc6	kolej
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
řízena	řídit	k5eAaImNgFnS	řídit
jezuity	jezuita	k1gMnPc7	jezuita
<g/>
.	.	kIx.	.
</s>
<s>
Formálně	formálně	k6eAd1	formálně
byl	být	k5eAaImAgInS	být
až	až	k9	až
do	do	k7c2	do
smrti	smrt	k1gFnSc2	smrt
příslušníkem	příslušník	k1gMnSc7	příslušník
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
exkomunikován	exkomunikován	k2eAgMnSc1d1	exkomunikován
nikdy	nikdy	k6eAd1	nikdy
nebyl	být	k5eNaImAgInS	být
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
Ferney	Fernea	k1gFnPc4	Fernea
dal	dát	k5eAaPmAgMnS	dát
vybudovat	vybudovat	k5eAaPmF	vybudovat
nový	nový	k2eAgInSc4d1	nový
kostel	kostel	k1gInSc4	kostel
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
starý	starý	k2eAgInSc1d1	starý
byl	být	k5eAaImAgInS	být
již	již	k6eAd1	již
zchátralý	zchátralý	k2eAgInSc1d1	zchátralý
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
vděčnosti	vděčnost	k1gFnSc2	vděčnost
za	za	k7c4	za
služby	služba	k1gFnPc4	služba
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
Voltaire	Voltair	k1gInSc5	Voltair
prokazoval	prokazovat	k5eAaImAgMnS	prokazovat
kapucínům	kapucín	k1gMnPc3	kapucín
v	v	k7c6	v
Gexu	Gexum	k1gNnSc6	Gexum
<g/>
,	,	kIx,	,
svým	svůj	k3xOyFgMnPc3	svůj
sousedům	soused	k1gMnPc3	soused
<g/>
,	,	kIx,	,
s	s	k7c7	s
nimiž	jenž	k3xRgInPc7	jenž
dobře	dobře	k6eAd1	dobře
vycházel	vycházet	k5eAaImAgMnS	vycházet
<g/>
,	,	kIx,	,
zaslal	zaslat	k5eAaPmAgMnS	zaslat
generál	generál	k1gMnSc1	generál
řádu	řád	k1gInSc2	řád
svatého	svatý	k2eAgMnSc2d1	svatý
Františka	František	k1gMnSc2	František
Voltairovi	Voltairův	k2eAgMnPc1d1	Voltairův
diplom	diplom	k1gInSc4	diplom
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
ho	on	k3xPp3gMnSc4	on
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
"	"	kIx"	"
<g/>
čestným	čestný	k2eAgMnSc7d1	čestný
kapucínem	kapucín	k1gMnSc7	kapucín
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Voltaire	Voltair	k1gMnSc5	Voltair
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
v	v	k7c6	v
dopisech	dopis	k1gInPc6	dopis
občas	občas	k6eAd1	občas
šibalsky	šibalsky	k6eAd1	šibalsky
podepisoval	podepisovat	k5eAaImAgMnS	podepisovat
"	"	kIx"	"
<g/>
fráter	fráter	k1gMnSc1	fráter
Voltaire	Voltair	k1gInSc5	Voltair
<g/>
,	,	kIx,	,
nehodný	hodný	k2eNgMnSc1d1	nehodný
kapucín	kapucín	k1gMnSc1	kapucín
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Navenek	navenek	k6eAd1	navenek
Voltaire	Voltair	k1gInSc5	Voltair
plnil	plnit	k5eAaImAgMnS	plnit
povinnosti	povinnost	k1gFnSc2	povinnost
katolického	katolický	k2eAgMnSc2d1	katolický
křesťana	křesťan	k1gMnSc2	křesťan
<g/>
,	,	kIx,	,
už	už	k9	už
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
mohl	moct	k5eAaImAgMnS	moct
mít	mít	k5eAaImF	mít
řádný	řádný	k2eAgInSc4d1	řádný
pohřeb	pohřeb	k1gInSc4	pohřeb
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
Velikonocích	Velikonoce	k1gFnPc6	Velikonoce
chodil	chodit	k5eAaImAgMnS	chodit
ke	k	k7c3	k
svatému	svatý	k2eAgNnSc3d1	svaté
přijímání	přijímání	k1gNnSc3	přijímání
a	a	k8xC	a
dával	dávat	k5eAaImAgMnS	dávat
se	se	k3xPyFc4	se
zaopatřit	zaopatřit	k5eAaPmF	zaopatřit
<g/>
,	,	kIx,	,
kdykoli	kdykoli	k6eAd1	kdykoli
se	se	k3xPyFc4	se
povážlivě	povážlivě	k6eAd1	povážlivě
roznemohl	roznemoct	k5eAaPmAgMnS	roznemoct
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
jednou	jednou	k6eAd1	jednou
taková	takový	k3xDgFnSc1	takový
zpověď	zpověď	k1gFnSc1	zpověď
pohoršila	pohoršit	k5eAaPmAgFnS	pohoršit
jeho	jeho	k3xOp3gMnPc4	jeho
přátele	přítel	k1gMnPc4	přítel
<g/>
,	,	kIx,	,
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
v	v	k7c6	v
dopisech	dopis	k1gInPc6	dopis
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Co	co	k3yInSc1	co
jsem	být	k5eAaImIp1nS	být
učinil	učinit	k5eAaPmAgMnS	učinit
letos	letos	k6eAd1	letos
<g/>
,	,	kIx,	,
učinil	učinit	k5eAaImAgMnS	učinit
jsem	být	k5eAaImIp1nS	být
již	již	k6eAd1	již
častěji	často	k6eAd2	často
<g/>
,	,	kIx,	,
a	a	k8xC	a
dá	dát	k5eAaPmIp3nS	dát
<g/>
-li	i	k?	-li
Bůh	bůh	k1gMnSc1	bůh
<g/>
,	,	kIx,	,
budu	být	k5eAaImBp1nS	být
činit	činit	k5eAaImF	činit
i	i	k9	i
napříště	napříště	k6eAd1	napříště
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yRnSc1	co
jiného	jiný	k2eAgNnSc2d1	jiné
zbývá	zbývat	k5eAaImIp3nS	zbývat
mudrcům	mudrc	k1gMnPc3	mudrc
<g/>
,	,	kIx,	,
obklopeným	obklopený	k2eAgMnPc3d1	obklopený
barbary	barbar	k1gMnPc4	barbar
<g/>
?	?	kIx.	?
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
chvíle	chvíle	k1gFnPc4	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
napodobit	napodobit	k5eAaPmF	napodobit
jejich	jejich	k3xOp3gInPc4	jejich
posunky	posunek	k1gInPc4	posunek
a	a	k8xC	a
mluvit	mluvit	k5eAaImF	mluvit
jejich	jejich	k3xOp3gInSc7	jejich
jazykem	jazyk	k1gInSc7	jazyk
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Nelze	lze	k6eNd1	lze
lépe	dobře	k6eAd2	dobře
projevit	projevit	k5eAaPmF	projevit
opovržení	opovržení	k1gNnPc4	opovržení
k	k	k7c3	k
takovým	takový	k3xDgFnPc3	takový
komediím	komedie	k1gFnPc3	komedie
než	než	k8xS	než
hrát	hrát	k5eAaImF	hrát
v	v	k7c6	v
nich	on	k3xPp3gMnPc6	on
<g/>
;	;	kIx,	;
kdo	kdo	k3yRnSc1	kdo
se	se	k3xPyFc4	se
jich	on	k3xPp3gNnPc2	on
straní	stranit	k5eAaImIp3nS	stranit
<g/>
,	,	kIx,	,
dává	dávat	k5eAaImIp3nS	dávat
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jich	on	k3xPp3gMnPc2	on
bojí	bát	k5eAaImIp3nS	bát
<g/>
.	.	kIx.	.
<g/>
<g />
.	.	kIx.	.
</s>
<s>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Kdybych	kdyby	kYmCp1nS	kdyby
měl	mít	k5eAaImAgMnS	mít
sto	sto	k4xCgNnSc4	sto
tisíc	tisíc	k4xCgInSc1	tisíc
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
vím	vědět	k5eAaImIp1nS	vědět
dobře	dobře	k6eAd1	dobře
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
bych	by	kYmCp1nS	by
udělal	udělat	k5eAaPmAgMnS	udělat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
protože	protože	k8xS	protože
je	on	k3xPp3gInPc4	on
nemám	mít	k5eNaImIp1nS	mít
<g/>
,	,	kIx,	,
budu	být	k5eAaImBp1nS	být
chodit	chodit	k5eAaImF	chodit
o	o	k7c6	o
Velikonocích	Velikonoce	k1gFnPc6	Velikonoce
k	k	k7c3	k
přijímání	přijímání	k1gNnSc3	přijímání
a	a	k8xC	a
nazývejte	nazývat	k5eAaImRp2nP	nazývat
si	se	k3xPyFc3	se
mě	já	k3xPp1nSc4	já
pokrytcem	pokrytec	k1gMnSc7	pokrytec
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
chcete	chtít	k5eAaImIp2nP	chtít
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Jeho	jeho	k3xOp3gMnSc1	jeho
životopisec	životopisec	k1gMnSc1	životopisec
Orieux	Orieux	k1gInSc4	Orieux
uvádí	uvádět	k5eAaImIp3nS	uvádět
tuto	tento	k3xDgFnSc4	tento
příhodu	příhoda	k1gFnSc4	příhoda
<g />
.	.	kIx.	.
</s>
<s>
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Když	když	k8xS	když
se	se	k3xPyFc4	se
ho	on	k3xPp3gInSc4	on
jeho	jeho	k3xOp3gMnSc1	jeho
sekretář	sekretář	k1gMnSc1	sekretář
Wagniè	Wagniè	k1gMnSc1	Wagniè
ptal	ptat	k5eAaImAgMnS	ptat
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
by	by	kYmCp3nS	by
udělal	udělat	k5eAaPmAgMnS	udělat
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nS	kdyby
žil	žít	k5eAaImAgMnS	žít
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
pod	pod	k7c7	pod
dohledem	dohled	k1gInSc7	dohled
a	a	k8xC	a
ve	v	k7c6	v
spárech	spár	k1gInPc6	spár
inkvizice	inkvizice	k1gFnSc2	inkvizice
a	a	k8xC	a
chtěl	chtít	k5eAaImAgMnS	chtít
si	se	k3xPyFc3	se
zachovat	zachovat	k5eAaPmF	zachovat
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Obstaral	obstarat	k5eAaPmAgMnS	obstarat
bych	by	kYmCp1nS	by
si	se	k3xPyFc3	se
velký	velký	k2eAgInSc4d1	velký
růženec	růženec	k1gInSc4	růženec
<g/>
,	,	kIx,	,
chodil	chodit	k5eAaImAgMnS	chodit
bych	by	kYmCp1nS	by
každý	každý	k3xTgMnSc1	každý
<g />
.	.	kIx.	.
</s>
<s>
den	den	k1gInSc4	den
na	na	k7c4	na
mši	mše	k1gFnSc4	mše
<g/>
,	,	kIx,	,
líbal	líbat	k5eAaImAgMnS	líbat
bych	by	kYmCp1nS	by
rukávy	rukáv	k1gInPc4	rukáv
všech	všecek	k3xTgMnPc2	všecek
mnichů	mnich	k1gMnPc2	mnich
a	a	k8xC	a
pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
se	se	k3xPyFc4	se
zapálit	zapálit	k5eAaPmF	zapálit
všechny	všechen	k3xTgInPc4	všechen
jejich	jejich	k3xOp3gInPc4	jejich
kláštery	klášter	k1gInPc4	klášter
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
"	"	kIx"	"
Ve	v	k7c6	v
spisech	spis	k1gInPc6	spis
<g/>
,	,	kIx,	,
které	který	k3yQgFnSc2	který
publikoval	publikovat	k5eAaBmAgMnS	publikovat
pod	pod	k7c7	pod
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
<g/>
,	,	kIx,	,
Voltaire	Voltair	k1gInSc5	Voltair
dogmata	dogma	k1gNnPc4	dogma
katolické	katolický	k2eAgFnSc2d1	katolická
věrouky	věrouka	k1gFnSc2	věrouka
neodmítal	odmítat	k5eNaImAgMnS	odmítat
<g/>
,	,	kIx,	,
spíše	spíše	k9	spíše
je	být	k5eAaImIp3nS	být
ironizoval	ironizovat	k5eAaImAgMnS	ironizovat
a	a	k8xC	a
nepřímo	přímo	k6eNd1	přímo
zpochybňoval	zpochybňovat	k5eAaImAgMnS	zpochybňovat
<g/>
.	.	kIx.	.
</s>
<s>
Otevřeně	otevřeně	k6eAd1	otevřeně
protikřesťanské	protikřesťanský	k2eAgInPc4d1	protikřesťanský
spisy	spis	k1gInPc4	spis
nevydával	vydávat	k5eNaImAgMnS	vydávat
pod	pod	k7c7	pod
svým	svůj	k3xOyFgNnSc7	svůj
jménem	jméno	k1gNnSc7	jméno
a	a	k8xC	a
popíral	popírat	k5eAaImAgMnS	popírat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gMnSc7	jejich
autorem	autor	k1gMnSc7	autor
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
byl	být	k5eAaImAgInS	být
jeho	jeho	k3xOp3gInSc1	jeho
kritický	kritický	k2eAgInSc1d1	kritický
postoj	postoj	k1gInSc1	postoj
k	k	k7c3	k
církvi	církev	k1gFnSc3	církev
a	a	k8xC	a
ke	k	k7c3	k
katolické	katolický	k2eAgFnSc3d1	katolická
věrouce	věrouka	k1gFnSc3	věrouka
dobře	dobře	k6eAd1	dobře
znám	znám	k2eAgInSc1d1	znám
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
když	když	k8xS	když
už	už	k6eAd1	už
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
smrt	smrt	k1gFnSc4	smrt
nemocen	nemocen	k2eAgMnSc1d1	nemocen
<g/>
,	,	kIx,	,
podnikl	podniknout	k5eAaPmAgMnS	podniknout
proto	proto	k8xC	proto
Voltaire	Voltair	k1gInSc5	Voltair
další	další	k2eAgInPc4d1	další
kroky	krok	k1gInPc4	krok
ke	k	k7c3	k
smíření	smíření	k1gNnSc3	smíření
s	s	k7c7	s
církví	církev	k1gFnSc7	církev
<g/>
.	.	kIx.	.
</s>
<s>
Odůvodňoval	odůvodňovat	k5eAaImAgInS	odůvodňovat
to	ten	k3xDgNnSc1	ten
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nechce	chtít	k5eNaImIp3nS	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgMnS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
jako	jako	k8xS	jako
pes	pes	k1gMnSc1	pes
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jeho	jeho	k3xOp3gNnSc4	jeho
tělo	tělo	k1gNnSc4	tělo
hodili	hodit	k5eAaPmAgMnP	hodit
na	na	k7c4	na
mrchoviště	mrchoviště	k1gNnSc4	mrchoviště
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
27	[number]	k4	27
<g/>
.	.	kIx.	.
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
1778	[number]	k4	1778
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
dostavil	dostavit	k5eAaPmAgMnS	dostavit
kněz	kněz	k1gMnSc1	kněz
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
přivolal	přivolat	k5eAaPmAgMnS	přivolat
<g/>
,	,	kIx,	,
a	a	k8xC	a
Voltaire	Voltair	k1gInSc5	Voltair
podepsal	podepsat	k5eAaPmAgMnS	podepsat
před	před	k7c7	před
svědky	svědek	k1gMnPc7	svědek
odvolání	odvolání	k1gNnSc2	odvolání
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
požadovala	požadovat	k5eAaImAgFnS	požadovat
církev	církev	k1gFnSc1	církev
<g/>
,	,	kIx,	,
a	a	k8xC	a
vyzpovídal	vyzpovídat	k5eAaPmAgMnS	vyzpovídat
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
svatému	svatý	k2eAgNnSc3d1	svaté
přijímání	přijímání	k1gNnSc3	přijímání
však	však	k9	však
nepřistoupil	přistoupit	k5eNaPmAgInS	přistoupit
a	a	k8xC	a
dle	dle	k7c2	dle
jednoho	jeden	k4xCgNnSc2	jeden
svědectví	svědectví	k1gNnSc2	svědectví
to	ten	k3xDgNnSc4	ten
odůvodnil	odůvodnit	k5eAaPmAgMnS	odůvodnit
slovy	slovo	k1gNnPc7	slovo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Pane	Pan	k1gMnSc5	Pan
abbé	abbé	k1gMnSc5	abbé
<g/>
,	,	kIx,	,
uvědomte	uvědomit	k5eAaPmRp2nP	uvědomit
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
plivu	plivu	k?	plivu
krev	krev	k1gFnSc4	krev
a	a	k8xC	a
že	že	k8xS	že
si	se	k3xPyFc3	se
musíme	muset	k5eAaImIp1nP	muset
dát	dát	k5eAaPmF	dát
pozor	pozor	k1gInSc4	pozor
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
moje	můj	k3xOp1gFnSc1	můj
krev	krev	k1gFnSc1	krev
nesmísila	smísit	k5eNaPmAgFnS	smísit
s	s	k7c7	s
krví	krev	k1gFnSc7	krev
Pána	pán	k1gMnSc2	pán
Boha	bůh	k1gMnSc2	bůh
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Někteří	některý	k3yIgMnPc1	některý
životopisci	životopisec	k1gMnPc1	životopisec
též	též	k9	též
uvádějí	uvádět	k5eAaImIp3nP	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
každý	každý	k3xTgMnSc1	každý
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c4	v
náboženství	náboženství	k1gNnSc4	náboženství
svých	svůj	k3xOyFgMnPc2	svůj
otců	otec	k1gMnPc2	otec
<g/>
.	.	kIx.	.
</s>
<s>
Kdybych	kdyby	kYmCp1nS	kdyby
žil	žít	k5eAaImAgMnS	žít
na	na	k7c6	na
březích	břeh	k1gInPc6	břeh
Gangy	Ganga	k1gFnSc2	Ganga
<g/>
,	,	kIx,	,
přál	přát	k5eAaImAgMnS	přát
bych	by	kYmCp1nS	by
si	se	k3xPyFc3	se
zemřít	zemřít	k5eAaPmF	zemřít
s	s	k7c7	s
kravským	kravský	k2eAgInSc7d1	kravský
ocasem	ocas	k1gInSc7	ocas
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Den	den	k1gInSc1	den
před	před	k7c7	před
smrtí	smrt	k1gFnSc7	smrt
<g/>
,	,	kIx,	,
dne	den	k1gInSc2	den
29	[number]	k4	29
<g/>
.	.	kIx.	.
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
1778	[number]	k4	1778
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
lůžku	lůžko	k1gNnSc3	lůžko
znovu	znovu	k6eAd1	znovu
dostavili	dostavit	k5eAaPmAgMnP	dostavit
kněží	kněz	k1gMnPc1	kněz
a	a	k8xC	a
přinesli	přinést	k5eAaPmAgMnP	přinést
s	s	k7c7	s
sebou	se	k3xPyFc7	se
důkladnější	důkladný	k2eAgFnSc4d2	důkladnější
odvolání	odvolání	k1gNnSc3	odvolání
<g/>
,	,	kIx,	,
než	než	k8xS	než
bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
první	první	k4xOgNnSc1	první
<g/>
,	,	kIx,	,
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
omluvu	omluva	k1gFnSc4	omluva
a	a	k8xC	a
podrobné	podrobný	k2eAgNnSc4d1	podrobné
vyznání	vyznání	k1gNnSc4	vyznání
víry	víra	k1gFnSc2	víra
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
podpisu	podpis	k1gInSc3	podpis
těchto	tento	k3xDgInPc2	tento
dokumentů	dokument	k1gInPc2	dokument
však	však	k9	však
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
;	;	kIx,	;
Voltaire	Voltair	k1gMnSc5	Voltair
již	již	k6eAd1	již
nebyl	být	k5eNaImAgInS	být
zcela	zcela	k6eAd1	zcela
při	při	k7c6	při
smyslech	smysl	k1gInPc6	smysl
a	a	k8xC	a
říkal	říkat	k5eAaImAgMnS	říkat
různé	různý	k2eAgFnPc4d1	různá
nesouvislé	souvislý	k2eNgFnPc4d1	nesouvislá
věty	věta	k1gFnPc4	věta
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
ho	on	k3xPp3gInSc4	on
kněz	kněz	k1gMnSc1	kněz
ptal	ptat	k5eAaImAgMnS	ptat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
uznává	uznávat	k5eAaImIp3nS	uznávat
božský	božský	k2eAgInSc4d1	božský
původ	původ	k1gInSc4	původ
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
<g/>
,	,	kIx,	,
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
mu	on	k3xPp3gMnSc3	on
prý	prý	k9	prý
Voltaire	Voltair	k1gInSc5	Voltair
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Ježíš	Ježíš	k1gMnSc1	Ježíš
Kristus	Kristus	k1gMnSc1	Kristus
<g/>
?	?	kIx.	?
</s>
<s>
Ježíš	Ježíš	k1gMnSc1	Ježíš
Kristus	Kristus	k1gMnSc1	Kristus
<g/>
?	?	kIx.	?
</s>
<s>
Nechte	nechat	k5eAaPmRp2nP	nechat
mě	já	k3xPp1nSc2	já
umřít	umřít	k5eAaPmF	umřít
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
Kolem	kolem	k7c2	kolem
Voltairovy	Voltairův	k2eAgFnSc2d1	Voltairova
smrti	smrt	k1gFnSc2	smrt
koluje	kolovat	k5eAaImIp3nS	kolovat
mnoho	mnoho	k4c1	mnoho
pověstí	pověst	k1gFnPc2	pověst
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
asi	asi	k9	asi
zcela	zcela	k6eAd1	zcela
nepodložených	podložený	k2eNgInPc2d1	nepodložený
<g/>
.	.	kIx.	.
</s>
<s>
Abbé	abbé	k1gMnPc1	abbé
Depery	Depera	k1gFnSc2	Depera
například	například	k6eAd1	například
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
prý	prý	k9	prý
na	na	k7c6	na
základě	základ	k1gInSc6	základ
vyprávění	vyprávění	k1gNnSc2	vyprávění
očitého	očitý	k2eAgMnSc4d1	očitý
svědka	svědek	k1gMnSc4	svědek
<g/>
,	,	kIx,	,
že	že	k8xS	že
Voltaire	Voltair	k1gInSc5	Voltair
viděl	vidět	k5eAaImAgMnS	vidět
u	u	k7c2	u
své	svůj	k3xOyFgFnSc2	svůj
postele	postel	k1gFnSc2	postel
ďábla	ďábel	k1gMnSc2	ďábel
a	a	k8xC	a
že	že	k8xS	že
křičel	křičet	k5eAaImAgMnS	křičet
jako	jako	k9	jako
opravdový	opravdový	k2eAgMnSc1d1	opravdový
zatracenec	zatracenec	k1gMnSc1	zatracenec
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
<g/>
,	,	kIx,	,
chce	chtít	k5eAaImIp3nS	chtít
se	se	k3xPyFc4	se
mě	já	k3xPp1nSc2	já
zmocnit	zmocnit	k5eAaPmF	zmocnit
<g/>
,	,	kIx,	,
vidím	vidět	k5eAaImIp1nS	vidět
peklo	peklo	k1gNnSc1	peklo
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
Kněžím	kněz	k1gMnPc3	kněz
údajně	údajně	k6eAd1	údajně
nebylo	být	k5eNaImAgNnS	být
dovoleno	dovolit	k5eAaPmNgNnS	dovolit
vstoupit	vstoupit	k5eAaPmF	vstoupit
do	do	k7c2	do
jeho	on	k3xPp3gInSc2	on
pokoje	pokoj	k1gInSc2	pokoj
<g/>
.	.	kIx.	.
</s>
<s>
Voltaire	Voltair	k1gMnSc5	Voltair
zemřel	zemřít	k5eAaPmAgMnS	zemřít
dne	den	k1gInSc2	den
30	[number]	k4	30
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
1778	[number]	k4	1778
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
synovci	synovec	k1gMnPc1	synovec
požádali	požádat	k5eAaPmAgMnP	požádat
církevní	církevní	k2eAgInPc4d1	církevní
úřady	úřad	k1gInPc4	úřad
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gNnSc3	on
byl	být	k5eAaImAgInS	být
povolen	povolit	k5eAaPmNgInS	povolit
katolický	katolický	k2eAgInSc1d1	katolický
pohřeb	pohřeb	k1gInSc1	pohřeb
<g/>
;	;	kIx,	;
poukazovali	poukazovat	k5eAaImAgMnP	poukazovat
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
odvolání	odvolání	k1gNnSc4	odvolání
a	a	k8xC	a
na	na	k7c4	na
zpověď	zpověď	k1gFnSc4	zpověď
<g/>
.	.	kIx.	.
</s>
<s>
Církevní	církevní	k2eAgMnPc1d1	církevní
představitelé	představitel	k1gMnPc1	představitel
však	však	k9	však
byli	být	k5eAaImAgMnP	být
toho	ten	k3xDgNnSc2	ten
mínění	mínění	k1gNnSc2	mínění
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgInPc4	tento
úkony	úkon	k1gInPc4	úkon
nemínil	mínit	k5eNaImAgMnS	mínit
Voltaire	Voltair	k1gInSc5	Voltair
zcela	zcela	k6eAd1	zcela
vážně	vážně	k6eAd1	vážně
<g/>
,	,	kIx,	,
a	a	k8xC	a
křesťanský	křesťanský	k2eAgInSc4d1	křesťanský
pohřeb	pohřeb	k1gInSc4	pohřeb
odepřeli	odepřít	k5eAaPmAgMnP	odepřít
<g/>
;	;	kIx,	;
příslušný	příslušný	k2eAgMnSc1d1	příslušný
farář	farář	k1gMnSc1	farář
od	od	k7c2	od
Svatého	svatý	k2eAgMnSc2d1	svatý
Sulpicia	Sulpicius	k1gMnSc2	Sulpicius
odmítl	odmítnout	k5eAaPmAgInS	odmítnout
provést	provést	k5eAaPmF	provést
církevní	církevní	k2eAgInPc4d1	církevní
obřady	obřad	k1gInPc4	obřad
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
Ludvík	Ludvík	k1gMnSc1	Ludvík
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
nechtěl	chtít	k5eNaImAgMnS	chtít
zasáhnout	zasáhnout	k5eAaPmF	zasáhnout
a	a	k8xC	a
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
nezbývá	zbývat	k5eNaImIp3nS	zbývat
nic	nic	k3yNnSc1	nic
jiného	jiný	k2eAgNnSc2d1	jiné
<g/>
,	,	kIx,	,
než	než	k8xS	než
ponechat	ponechat	k5eAaPmF	ponechat
rozhodnutí	rozhodnutí	k1gNnPc4	rozhodnutí
kněžím	kněz	k1gMnPc3	kněz
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
byla	být	k5eAaImAgFnS	být
obava	obava	k1gFnSc1	obava
<g/>
,	,	kIx,	,
že	že	k8xS	že
ani	ani	k8xC	ani
biskup	biskup	k1gMnSc1	biskup
annecyský	annecyský	k1gMnSc1	annecyský
<g/>
,	,	kIx,	,
pod	pod	k7c4	pod
kterého	který	k3yQgMnSc4	který
spadala	spadat	k5eAaImAgFnS	spadat
církevní	církevní	k2eAgFnSc1d1	církevní
správa	správa	k1gFnSc1	správa
ve	v	k7c4	v
Ferney	Ferne	k1gMnPc4	Ferne
a	a	k8xC	a
s	s	k7c7	s
nímž	jenž	k3xRgMnSc7	jenž
měl	mít	k5eAaImAgMnS	mít
Voltaire	Voltair	k1gInSc5	Voltair
spory	spor	k1gInPc7	spor
<g/>
,	,	kIx,	,
nepovolí	povolit	k5eNaPmIp3nS	povolit
pohřeb	pohřeb	k1gInSc4	pohřeb
na	na	k7c6	na
ferneyském	ferneyský	k2eAgNnSc6d1	ferneyský
panství	panství	k1gNnSc6	panství
<g/>
,	,	kIx,	,
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
syn	syn	k1gMnSc1	syn
Voltairovy	Voltairův	k2eAgFnPc4d1	Voltairova
sestry	sestra	k1gFnPc4	sestra
<g/>
,	,	kIx,	,
abbé	abbé	k1gMnPc4	abbé
Mignot	Mignota	k1gFnPc2	Mignota
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
jiné	jiný	k2eAgNnSc4d1	jiné
řešení	řešení	k1gNnSc4	řešení
<g/>
.	.	kIx.	.
</s>
<s>
Ostatky	ostatek	k1gInPc1	ostatek
Voltairovy	Voltairův	k2eAgInPc1d1	Voltairův
byly	být	k5eAaImAgFnP	být
převezeny	převézt	k5eAaPmNgFnP	převézt
do	do	k7c2	do
cisterciáckého	cisterciácký	k2eAgInSc2d1	cisterciácký
kláštera	klášter	k1gInSc2	klášter
ve	v	k7c6	v
Scelliè	Scelliè	k1gFnSc6	Scelliè
u	u	k7c2	u
Troyes	Troyesa	k1gFnPc2	Troyesa
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
byl	být	k5eAaImAgInS	být
abbé	abbé	k1gMnSc7	abbé
Mignot	Mignota	k1gFnPc2	Mignota
obročníkem	obročník	k1gMnSc7	obročník
<g/>
,	,	kIx,	,
a	a	k8xC	a
převor	převor	k1gMnSc1	převor
tohoto	tento	k3xDgInSc2	tento
kláštera	klášter	k1gInSc2	klášter
vykonal	vykonat	k5eAaPmAgInS	vykonat
pohřební	pohřební	k2eAgInSc1d1	pohřební
obřad	obřad	k1gInSc1	obřad
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
pohřbu	pohřeb	k1gInSc3	pohřeb
protestoval	protestovat	k5eAaBmAgMnS	protestovat
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
pařížský	pařížský	k2eAgMnSc1d1	pařížský
u	u	k7c2	u
biskupa	biskup	k1gMnSc2	biskup
v	v	k7c4	v
Troyes	Troyes	k1gInSc4	Troyes
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
převora	převor	k1gMnSc4	převor
sesadil	sesadit	k5eAaPmAgMnS	sesadit
(	(	kIx(	(
<g/>
Voltairovi	Voltairův	k2eAgMnPc1d1	Voltairův
synovci	synovec	k1gMnPc1	synovec
pak	pak	k6eAd1	pak
převora	převor	k1gMnSc4	převor
finančně	finančně	k6eAd1	finančně
odškodnili	odškodnit	k5eAaPmAgMnP	odškodnit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Biskup	biskup	k1gMnSc1	biskup
původně	původně	k6eAd1	původně
požadoval	požadovat	k5eAaImAgMnS	požadovat
též	též	k9	též
exhumaci	exhumace	k1gFnSc3	exhumace
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
.	.	kIx.	.
</s>
<s>
Exhumace	exhumace	k1gFnSc1	exhumace
byla	být	k5eAaImAgFnS	být
provedena	provést	k5eAaPmNgFnS	provést
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1791	[number]	k4	1791
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgInP	být
Voltairovy	Voltairův	k2eAgInPc1d1	Voltairův
ostatky	ostatek	k1gInPc1	ostatek
přeneseny	přenesen	k2eAgInPc1d1	přenesen
do	do	k7c2	do
Pantheonu	Pantheon	k1gInSc2	Pantheon
<g/>
.	.	kIx.	.
</s>
<s>
Voltairovy	Voltairův	k2eAgFnPc1d1	Voltairova
myšlenky	myšlenka	k1gFnPc1	myšlenka
a	a	k8xC	a
snahy	snaha	k1gFnPc1	snaha
o	o	k7c4	o
vytvoření	vytvoření	k1gNnSc4	vytvoření
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
na	na	k7c6	na
rozumu	rozum	k1gInSc6	rozum
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
inspirovaly	inspirovat	k5eAaBmAgInP	inspirovat
tvůrce	tvůrce	k1gMnSc1	tvůrce
nových	nový	k2eAgNnPc2d1	nové
náboženství	náboženství	k1gNnSc2	náboženství
v	v	k7c6	v
době	doba	k1gFnSc6	doba
Velké	velký	k2eAgFnSc2d1	velká
francouzské	francouzský	k2eAgFnSc2d1	francouzská
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
:	:	kIx,	:
kultu	kult	k1gInSc2	kult
Rozumu	rozum	k1gInSc2	rozum
a	a	k8xC	a
kultu	kult	k1gInSc2	kult
Nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
bytosti	bytost	k1gFnSc2	bytost
<g/>
.	.	kIx.	.
</s>
<s>
Nepochybně	pochybně	k6eNd1	pochybně
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nejvíce	nejvíce	k6eAd1	nejvíce
citovaných	citovaný	k2eAgInPc2d1	citovaný
"	"	kIx"	"
<g/>
výroků	výrok	k1gInPc2	výrok
<g/>
"	"	kIx"	"
Voltaira	Voltaira	k1gMnSc1	Voltaira
<g/>
.	.	kIx.	.
</s>
<s>
Nejde	jít	k5eNaImIp3nS	jít
však	však	k9	však
o	o	k7c4	o
citát	citát	k1gInSc4	citát
autentický	autentický	k2eAgInSc4d1	autentický
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
totiž	totiž	k9	totiž
známa	znám	k2eAgFnSc1d1	známa
žádná	žádný	k3yNgFnSc1	žádný
Voltairova	Voltairův	k2eAgFnSc1d1	Voltairova
písemnost	písemnost	k1gFnSc1	písemnost
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
by	by	kYmCp3nP	by
byla	být	k5eAaImAgFnS	být
zmíněná	zmíněný	k2eAgFnSc1d1	zmíněná
věta	věta	k1gFnSc1	věta
uvedena	uvést	k5eAaPmNgFnS	uvést
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
anekdoticky	anekdoticky	k6eAd1	anekdoticky
psaného	psaný	k2eAgInSc2d1	psaný
Voltairova	Voltairův	k2eAgInSc2d1	Voltairův
životopisu	životopis	k1gInSc2	životopis
<g/>
,	,	kIx,	,
z	z	k7c2	z
knihy	kniha	k1gFnSc2	kniha
The	The	k1gFnSc2	The
Friends	Friends	k1gInSc1	Friends
of	of	k?	of
Voltaire	Voltair	k1gInSc5	Voltair
[	[	kIx(	[
<g/>
Přátelé	přítel	k1gMnPc1	přítel
Voltaira	Voltair	k1gInSc2	Voltair
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
publikována	publikovat	k5eAaBmNgFnS	publikovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
a	a	k8xC	a
jejímž	jejíž	k3xOyRp3gMnSc7	jejíž
autorem	autor	k1gMnSc7	autor
byl	být	k5eAaImAgMnS	být
údajně	údajně	k6eAd1	údajně
S.	S.	kA	S.
G.	G.	kA	G.
Tallentyre	Tallentyr	k1gInSc5	Tallentyr
<g/>
;	;	kIx,	;
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
pseudonym	pseudonym	k1gInSc4	pseudonym
<g/>
,	,	kIx,	,
skutečnou	skutečný	k2eAgFnSc7d1	skutečná
autorkou	autorka	k1gFnSc7	autorka
knihy	kniha	k1gFnSc2	kniha
je	být	k5eAaImIp3nS	být
anglická	anglický	k2eAgFnSc1d1	anglická
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
Evelyn	Evelyn	k1gInSc1	Evelyn
Beatrice	Beatrice	k1gFnSc1	Beatrice
Hall	Hall	k1gMnSc1	Hall
<g/>
(	(	kIx(	(
<g/>
ová	ová	k?	ová
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
1868	[number]	k4	1868
<g/>
–	–	k?	–
<g/>
1956	[number]	k4	1956
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Touto	tento	k3xDgFnSc7	tento
větou	věta	k1gFnSc7	věta
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
I	i	k9	i
disapprove	disapprov	k1gInSc5	disapprov
of	of	k?	of
what	what	k1gInSc1	what
you	you	k?	you
say	say	k?	say
<g/>
,	,	kIx,	,
but	but	k?	but
I	i	k8xC	i
will	will	k1gMnSc1	will
defend	defend	k1gMnSc1	defend
to	ten	k3xDgNnSc1	ten
the	the	k?	the
death	death	k1gInSc1	death
your	your	k1gMnSc1	your
right	right	k1gMnSc1	right
to	ten	k3xDgNnSc4	ten
say	say	k?	say
it	it	k?	it
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
chtěla	chtít	k5eAaImAgFnS	chtít
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
Voltairův	Voltairův	k2eAgInSc4d1	Voltairův
postoj	postoj	k1gInSc4	postoj
k	k	k7c3	k
nesnášenlivosti	nesnášenlivost	k1gFnSc3	nesnášenlivost
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Citát	citát	k1gInSc1	citát
<g/>
"	"	kIx"	"
byl	být	k5eAaImAgInS	být
později	pozdě	k6eAd2	pozdě
přeložen	přeložit	k5eAaPmNgInS	přeložit
do	do	k7c2	do
francouzštiny	francouzština	k1gFnSc2	francouzština
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Je	být	k5eAaImIp3nS	být
ne	ne	k9	ne
suis	suis	k6eAd1	suis
pas	pas	k1gInSc4	pas
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
accord	accord	k1gInSc1	accord
avec	avec	k1gInSc1	avec
ce	ce	k?	ce
que	que	k?	que
vous	vous	k1gInSc1	vous
dites	dites	k1gInSc1	dites
<g/>
,	,	kIx,	,
mais	mais	k6eAd1	mais
je	být	k5eAaImIp3nS	být
me	me	k?	me
battrai	battrai	k6eAd1	battrai
jusqu	jusqu	k6eAd1	jusqu
<g/>
'	'	kIx"	'
<g/>
à	à	k?	à
la	la	k1gNnSc1	la
mort	mort	k1gMnSc1	mort
pour	pour	k1gMnSc1	pour
que	que	k?	que
vous	vous	k1gInSc1	vous
puissiez	puissiez	k1gInSc1	puissiez
le	le	k?	le
dire	dire	k1gInSc1	dire
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
do	do	k7c2	do
dalších	další	k2eAgInPc2d1	další
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Uvedený	uvedený	k2eAgInSc1d1	uvedený
"	"	kIx"	"
<g/>
výrok	výrok	k1gInSc1	výrok
<g/>
"	"	kIx"	"
má	mít	k5eAaImIp3nS	mít
ještě	ještě	k9	ještě
jednu	jeden	k4xCgFnSc4	jeden
podobu	podoba	k1gFnSc4	podoba
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Pane	Pan	k1gMnSc5	Pan
abbé	abbé	k1gMnSc5	abbé
<g/>
,	,	kIx,	,
nenávidím	návidět	k5eNaImIp1nS	návidět
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
píšete	psát	k5eAaImIp2nP	psát
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
položím	položit	k5eAaPmIp1nS	položit
život	život	k1gInSc4	život
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
abyste	aby	kYmCp2nP	aby
to	ten	k3xDgNnSc4	ten
mohl	moct	k5eAaImAgInS	moct
dál	daleko	k6eAd2	daleko
psát	psát	k5eAaImF	psát
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Monsieur	Monsieur	k1gMnSc1	Monsieur
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
abbé	abbé	k1gMnSc2	abbé
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
déteste	détest	k1gInSc5	détest
ce	ce	k?	ce
que	que	k?	que
vous	vous	k1gInSc1	vous
écrivez	écrivez	k1gInSc1	écrivez
<g/>
,	,	kIx,	,
mais	mais	k6eAd1	mais
je	být	k5eAaImIp3nS	být
donnerai	donnerae	k1gFnSc4	donnerae
ma	ma	k?	ma
vie	vie	k?	vie
pour	pour	k1gInSc1	pour
que	que	k?	que
vous	vous	k1gInSc1	vous
puissiez	puissiez	k1gMnSc1	puissiez
continuer	continuer	k1gMnSc1	continuer
à	à	k?	à
écrire	écrir	k1gInSc5	écrir
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
verzí	verze	k1gFnSc7	verze
přišel	přijít	k5eAaPmAgInS	přijít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
Norbert	Norbert	k1gMnSc1	Norbert
Guteman	Guteman	k1gMnSc1	Guteman
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
A	a	k9	a
Book	Book	k1gMnSc1	Book
of	of	k?	of
French	French	k1gInSc1	French
Quotations	Quotations	k1gInSc1	Quotations
<g/>
.	.	kIx.	.
</s>
<s>
Guteman	Guteman	k1gMnSc1	Guteman
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
uvedenou	uvedený	k2eAgFnSc4d1	uvedená
větu	věta	k1gFnSc4	věta
napsal	napsat	k5eAaPmAgMnS	napsat
Voltaire	Voltair	k1gInSc5	Voltair
v	v	k7c6	v
dopisu	dopis	k1gInSc2	dopis
abbému	abbé	k1gMnSc3	abbé
Le	Le	k1gMnSc1	Le
Riche	Rich	k1gMnSc2	Rich
(	(	kIx(	(
<g/>
psáno	psát	k5eAaImNgNnS	psát
i	i	k9	i
Leriche	Leriche	k1gFnSc1	Leriche
<g/>
)	)	kIx)	)
dne	den	k1gInSc2	den
6	[number]	k4	6
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1770	[number]	k4	1770
<g/>
.	.	kIx.	.
</s>
<s>
Dopis	dopis	k1gInSc1	dopis
opravdu	opravdu	k6eAd1	opravdu
existuje	existovat	k5eAaImIp3nS	existovat
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
mnohokrát	mnohokrát	k6eAd1	mnohokrát
otištěn	otisknout	k5eAaPmNgInS	otisknout
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
snadno	snadno	k6eAd1	snadno
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
zmíněná	zmíněný	k2eAgFnSc1d1	zmíněná
věta	věta	k1gFnSc1	věta
není	být	k5eNaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
neautentické	autentický	k2eNgInPc1d1	neautentický
"	"	kIx"	"
<g/>
citáty	citát	k1gInPc1	citát
<g/>
"	"	kIx"	"
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
zřejmě	zřejmě	k6eAd1	zřejmě
z	z	k7c2	z
potřeby	potřeba	k1gFnSc2	potřeba
vystihnout	vystihnout	k5eAaPmF	vystihnout
krátce	krátce	k6eAd1	krátce
a	a	k8xC	a
jednoznačně	jednoznačně	k6eAd1	jednoznačně
Voltairovo	Voltairův	k2eAgNnSc4d1	Voltairovo
stanovisko	stanovisko	k1gNnSc4	stanovisko
k	k	k7c3	k
intoleranci	intolerance	k1gFnSc3	intolerance
<g/>
.	.	kIx.	.
</s>
<s>
Voltaire	Voltair	k1gMnSc5	Voltair
napsal	napsat	k5eAaPmAgInS	napsat
proti	proti	k7c3	proti
náboženskému	náboženský	k2eAgInSc3d1	náboženský
fanatismu	fanatismus	k1gInSc3	fanatismus
a	a	k8xC	a
nesnášenlivosti	nesnášenlivost	k1gFnSc3	nesnášenlivost
řadu	řad	k1gInSc2	řad
spisů	spis	k1gInPc2	spis
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
skutečné	skutečný	k2eAgInPc1d1	skutečný
citáty	citát	k1gInPc1	citát
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
zpravidla	zpravidla	k6eAd1	zpravidla
nejsou	být	k5eNaImIp3nP	být
tak	tak	k6eAd1	tak
stručné	stručný	k2eAgInPc1d1	stručný
<g/>
,	,	kIx,	,
jasné	jasný	k2eAgInPc1d1	jasný
a	a	k8xC	a
patetické	patetický	k2eAgInPc1d1	patetický
<g/>
.	.	kIx.	.
</s>
<s>
Známé	známá	k1gFnPc4	známá
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
jeho	jeho	k3xOp3gNnSc4	jeho
Pojednání	pojednání	k1gNnSc4	pojednání
o	o	k7c6	o
snášenlivosti	snášenlivost	k1gFnSc6	snášenlivost
(	(	kIx(	(
<g/>
Traité	Traitý	k2eAgNnSc1d1	Traité
sur	sur	k?	sur
la	la	k1gNnSc1	la
tolérance	tolérance	k1gFnSc2	tolérance
<g/>
,	,	kIx,	,
1763	[number]	k4	1763
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dostupné	dostupný	k2eAgInPc1d1	dostupný
i	i	k9	i
v	v	k7c6	v
českých	český	k2eAgInPc6d1	český
překladech	překlad	k1gInPc6	překlad
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Voltaire	Voltair	k1gInSc5	Voltair
mj.	mj.	kA	mj.
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Příroda	příroda	k1gFnSc1	příroda
říká	říkat	k5eAaImIp3nS	říkat
všem	všecek	k3xTgMnPc3	všecek
lidem	člověk	k1gMnPc3	člověk
<g/>
:	:	kIx,	:
Nechala	nechat	k5eAaPmAgFnS	nechat
jsem	být	k5eAaImIp1nS	být
vás	vy	k3xPp2nPc4	vy
všechny	všechen	k3xTgInPc4	všechen
narodit	narodit	k5eAaPmF	narodit
slabé	slabý	k2eAgNnSc1d1	slabé
a	a	k8xC	a
nevědomé	vědomý	k2eNgNnSc1d1	nevědomé
<g/>
,	,	kIx,	,
abyste	aby	kYmCp2nP	aby
živořili	živořit	k5eAaImAgMnP	živořit
pár	pár	k4xCyI	pár
minut	minuta	k1gFnPc2	minuta
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
a	a	k8xC	a
abyste	aby	kYmCp2nP	aby
ji	on	k3xPp3gFnSc4	on
pohnojili	pohnojit	k5eAaPmAgMnP	pohnojit
svými	svůj	k3xOyFgFnPc7	svůj
mrtvolami	mrtvola	k1gFnPc7	mrtvola
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
jste	být	k5eAaImIp2nP	být
slabí	slabý	k2eAgMnPc1d1	slabý
<g/>
,	,	kIx,	,
podporujte	podporovat	k5eAaImRp2nP	podporovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
když	když	k8xS	když
jste	být	k5eAaImIp2nP	být
nevědomí	vědomý	k2eNgMnPc1d1	nevědomý
<g/>
,	,	kIx,	,
poučujte	poučovat	k5eAaImRp2nP	poučovat
se	se	k3xPyFc4	se
a	a	k8xC	a
buďte	budit	k5eAaImRp2nP	budit
k	k	k7c3	k
sobě	se	k3xPyFc3	se
shovívaví	shovívavý	k2eAgMnPc1d1	shovívavý
<g/>
.	.	kIx.	.
</s>
<s>
Kdybyste	kdyby	kYmCp2nP	kdyby
byli	být	k5eAaImAgMnP	být
všichni	všechen	k3xTgMnPc1	všechen
stejného	stejný	k2eAgNnSc2d1	stejné
smýšlení	smýšlení	k1gNnSc2	smýšlení
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
jistě	jistě	k9	jistě
nestane	stanout	k5eNaPmIp3nS	stanout
nikdy	nikdy	k6eAd1	nikdy
<g/>
,	,	kIx,	,
a	a	k8xC	a
kdyby	kdyby	kYmCp3nS	kdyby
byl	být	k5eAaImAgInS	být
pouze	pouze	k6eAd1	pouze
jediný	jediný	k2eAgMnSc1d1	jediný
člověk	člověk	k1gMnSc1	člověk
smýšlení	smýšlení	k1gNnSc2	smýšlení
opačného	opačný	k2eAgInSc2d1	opačný
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
by	by	kYmCp3nS	by
vaší	váš	k3xOp2gFnSc3	váš
povinností	povinnost	k1gFnSc7	povinnost
mu	on	k3xPp3gMnSc3	on
odpustit	odpustit	k5eAaPmF	odpustit
<g/>
;	;	kIx,	;
neboť	neboť	k8xC	neboť
právě	právě	k9	právě
já	já	k3xPp1nSc1	já
způsobuji	způsobovat	k5eAaImIp1nS	způsobovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
smýšlí	smýšlet	k5eAaImIp3nS	smýšlet
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
právě	právě	k6eAd1	právě
smýšlí	smýšlet	k5eAaImIp3nS	smýšlet
<g/>
.	.	kIx.	.
</s>
<s>
Dala	dát	k5eAaPmAgFnS	dát
jsem	být	k5eAaImIp1nS	být
vám	vy	k3xPp2nPc3	vy
ruce	ruka	k1gFnPc4	ruka
<g/>
,	,	kIx,	,
abyste	aby	kYmCp2nP	aby
vzdělávali	vzdělávat	k5eAaImAgMnP	vzdělávat
zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
a	a	k8xC	a
jiskřičku	jiskřička	k1gFnSc4	jiskřička
rozumu	rozum	k1gInSc2	rozum
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
vás	vy	k3xPp2nPc4	vy
vedla	vést	k5eAaImAgFnS	vést
<g/>
;	;	kIx,	;
do	do	k7c2	do
vašich	váš	k3xOp2gNnPc2	váš
srdcí	srdce	k1gNnPc2	srdce
jsem	být	k5eAaImIp1nS	být
vložila	vložit	k5eAaPmAgFnS	vložit
zárodek	zárodek	k1gInSc4	zárodek
soucitu	soucit	k1gInSc2	soucit
<g/>
,	,	kIx,	,
abyste	aby	kYmCp2nP	aby
si	se	k3xPyFc3	se
navzájem	navzájem	k6eAd1	navzájem
pomáhali	pomáhat	k5eAaImAgMnP	pomáhat
snášet	snášet	k5eAaImF	snášet
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Nezaduste	zadusit	k5eNaPmRp2nP	zadusit
tento	tento	k3xDgInSc4	tento
zárodek	zárodek	k1gInSc4	zárodek
<g/>
,	,	kIx,	,
neporušte	porušit	k5eNaPmRp2nP	porušit
jej	on	k3xPp3gNnSc4	on
<g/>
;	;	kIx,	;
vězte	vědět	k5eAaImRp2nP	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
Boha	bůh	k1gMnSc2	bůh
<g/>
,	,	kIx,	,
a	a	k8xC	a
nenahrazujte	nahrazovat	k5eNaImRp2nP	nahrazovat
volání	volání	k1gNnSc4	volání
přírody	příroda	k1gFnSc2	příroda
ubohým	ubohý	k2eAgNnSc7d1	ubohé
školáckým	školácký	k2eAgNnSc7d1	školácké
běsněním	běsnění	k1gNnSc7	běsnění
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Napsal	napsat	k5eAaPmAgInS	napsat
přes	přes	k7c4	přes
padesát	padesát	k4xCc4	padesát
literárních	literární	k2eAgNnPc2d1	literární
děl	dělo	k1gNnPc2	dělo
<g/>
:	:	kIx,	:
povídek	povídka	k1gFnPc2	povídka
<g/>
,	,	kIx,	,
románů	román	k1gInPc2	román
<g/>
,	,	kIx,	,
divadelních	divadelní	k2eAgFnPc2d1	divadelní
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
lyrických	lyrický	k2eAgFnPc2d1	lyrická
i	i	k8xC	i
epických	epický	k2eAgFnPc2d1	epická
básní	báseň	k1gFnPc2	báseň
<g/>
,	,	kIx,	,
filozofických	filozofický	k2eAgFnPc2d1	filozofická
úvah	úvaha	k1gFnPc2	úvaha
<g/>
,	,	kIx,	,
esejí	esej	k1gFnPc2	esej
a	a	k8xC	a
historických	historický	k2eAgNnPc2d1	historické
studií	studio	k1gNnPc2	studio
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
Voltairových	Voltairův	k2eAgFnPc2d1	Voltairova
prací	práce	k1gFnPc2	práce
zařadila	zařadit	k5eAaPmAgFnS	zařadit
církev	církev	k1gFnSc1	církev
na	na	k7c4	na
Index	index	k1gInSc4	index
zakázaných	zakázaný	k2eAgFnPc2d1	zakázaná
knih	kniha	k1gFnPc2	kniha
(	(	kIx(	(
<g/>
Filozofický	filozofický	k2eAgInSc1d1	filozofický
slovník	slovník	k1gInSc1	slovník
<g/>
,	,	kIx,	,
Filozofické	filozofický	k2eAgInPc1d1	filozofický
listy	list	k1gInPc1	list
<g/>
,	,	kIx,	,
Pojednání	pojednání	k1gNnPc1	pojednání
o	o	k7c4	o
snášenlivosti	snášenlivost	k1gFnPc4	snášenlivost
<g/>
,	,	kIx,	,
Pannu	Panna	k1gFnSc4	Panna
Orleánskou	orleánský	k2eAgFnSc4d1	Orleánská
<g/>
,	,	kIx,	,
Candide	Candid	k1gMnSc5	Candid
<g/>
,	,	kIx,	,
Století	století	k1gNnSc1	století
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jeho	on	k3xPp3gNnSc2	on
rozsáhlého	rozsáhlý	k2eAgNnSc2d1	rozsáhlé
literárního	literární	k2eAgNnSc2d1	literární
díla	dílo	k1gNnSc2	dílo
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
ceněny	cenit	k5eAaImNgFnP	cenit
především	především	k9	především
satiry	satira	k1gFnPc1	satira
a	a	k8xC	a
filosofické	filosofický	k2eAgFnPc1d1	filosofická
eseje	esej	k1gFnPc1	esej
na	na	k7c4	na
politická	politický	k2eAgNnPc4d1	politické
<g/>
,	,	kIx,	,
historická	historický	k2eAgNnPc1d1	historické
<g/>
,	,	kIx,	,
vědecká	vědecký	k2eAgNnPc1d1	vědecké
a	a	k8xC	a
morální	morální	k2eAgNnPc1d1	morální
témata	téma	k1gNnPc1	téma
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
vrchol	vrchol	k1gInSc4	vrchol
své	svůj	k3xOyFgFnSc2	svůj
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
,	,	kIx,	,
s	s	k7c7	s
čímž	což	k3yRnSc7	což
nelze	lze	k6eNd1	lze
nesouhlasit	souhlasit	k5eNaImF	souhlasit
<g/>
,	,	kIx,	,
považoval	považovat	k5eAaImAgMnS	považovat
sám	sám	k3xTgMnSc1	sám
Pannu	Panna	k1gFnSc4	Panna
Orleánskou	orleánský	k2eAgFnSc7d1	Orleánská
<g/>
,	,	kIx,	,
groteskní	groteskní	k2eAgFnSc7d1	groteskní
i	i	k9	i
historicky	historicky	k6eAd1	historicky
a	a	k8xC	a
filosoficky	filosoficky	k6eAd1	filosoficky
fundovaný	fundovaný	k2eAgInSc1d1	fundovaný
epos	epos	k1gInSc1	epos
vydaný	vydaný	k2eAgInSc1d1	vydaný
pod	pod	k7c7	pod
pseudonymem	pseudonym	k1gInSc7	pseudonym
abbé	abbé	k1gMnSc2	abbé
Trithéme	Trithém	k1gInSc5	Trithém
<g/>
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
voltairiánství	voltairiánství	k1gNnSc2	voltairiánství
označuje	označovat	k5eAaImIp3nS	označovat
jemnou	jemný	k2eAgFnSc4d1	jemná
ironii	ironie	k1gFnSc4	ironie
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
jeho	jeho	k3xOp3gFnSc1	jeho
tvorba	tvorba	k1gFnSc1	tvorba
dýchá	dýchat	k5eAaImIp3nS	dýchat
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
literárně	literárně	k6eAd1	literárně
(	(	kIx(	(
<g/>
především	především	k9	především
ve	v	k7c6	v
vrcholném	vrcholný	k2eAgInSc6d1	vrcholný
díle	díl	k1gInSc6	díl
Panna	Panna	k1gFnSc1	Panna
Orleánská	orleánský	k2eAgFnSc1d1	Orleánská
<g/>
)	)	kIx)	)
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c6	na
tradici	tradice	k1gFnSc6	tradice
Lúkiana	Lúkian	k1gMnSc2	Lúkian
ze	z	k7c2	z
Samosaty	Samosata	k1gFnSc2	Samosata
a	a	k8xC	a
Lodovica	Lodovic	k2eAgFnSc1d1	Lodovic
Ariosta	Ariosta	k1gFnSc1	Ariosta
<g/>
;	;	kIx,	;
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
autorů	autor	k1gMnPc2	autor
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
jinými	jiný	k2eAgNnPc7d1	jiné
G.	G.	kA	G.
G.	G.	kA	G.
Byrona	Byrona	k1gFnSc1	Byrona
(	(	kIx(	(
<g/>
Don	Don	k1gMnSc1	Don
Juan	Juan	k1gMnSc1	Juan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Anatola	Anatola	k1gFnSc1	Anatola
France	Franc	k1gMnSc2	Franc
(	(	kIx(	(
<g/>
Ostrov	ostrov	k1gInSc1	ostrov
Tučňáků	tučňák	k1gMnPc2	tučňák
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
moderních	moderní	k2eAgNnPc2d1	moderní
Sinclaira	Sinclairo	k1gNnSc2	Sinclairo
Lewise	Lewise	k1gFnSc1	Lewise
(	(	kIx(	(
<g/>
Elmer	Elmer	k1gInSc1	Elmer
Gantry	Gantr	k1gInPc1	Gantr
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Josefa	Josef	k1gMnSc2	Josef
Svatopluka	Svatopluk	k1gMnSc2	Svatopluk
Machara	Machar	k1gMnSc2	Machar
(	(	kIx(	(
<g/>
Boží	božit	k5eAaImIp3nP	božit
bojovníci	bojovník	k1gMnPc1	bojovník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Uvádíme	uvádět	k5eAaImIp1nP	uvádět
některá	některý	k3yIgNnPc4	některý
novější	nový	k2eAgNnPc4d2	novější
vydání	vydání	k1gNnPc4	vydání
<g/>
:	:	kIx,	:
VOLTAIRE	VOLTAIRE	kA	VOLTAIRE
<g/>
.	.	kIx.	.
</s>
<s>
Candide	Candid	k1gMnSc5	Candid
<g/>
.	.	kIx.	.
</s>
<s>
Přeložil	přeložit	k5eAaPmAgMnS	přeložit
Radovan	Radovan	k1gMnSc1	Radovan
Krátký	Krátký	k1gMnSc1	Krátký
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
XYZ	XYZ	kA	XYZ
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
204	[number]	k4	204
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7388	[number]	k4	7388
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Filozoficky	filozoficky	k6eAd1	filozoficky
orientovaná	orientovaný	k2eAgFnSc1d1	orientovaná
novela	novela	k1gFnSc1	novela
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
podává	podávat	k5eAaImIp3nS	podávat
všestranný	všestranný	k2eAgInSc4d1	všestranný
obraz	obraz	k1gInSc4	obraz
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Voltaire	Voltair	k1gMnSc5	Voltair
dává	dávat	k5eAaImIp3nS	dávat
Candidovi	Candid	k1gMnSc3	Candid
<g/>
,	,	kIx,	,
naivnímu	naivní	k2eAgMnSc3d1	naivní
mladému	mladý	k1gMnSc3	mladý
muži	muž	k1gMnSc3	muž
<g/>
,	,	kIx,	,
obcovat	obcovat	k5eAaImF	obcovat
s	s	k7c7	s
filozofy	filozof	k1gMnPc7	filozof
<g/>
,	,	kIx,	,
poznávat	poznávat	k5eAaImF	poznávat
vojska	vojsko	k1gNnPc4	vojsko
<g/>
,	,	kIx,	,
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
inkvisice	inkvisice	k1gFnSc2	inkvisice
<g/>
,	,	kIx,	,
panství	panství	k1gNnSc1	panství
jesuitů	jesuita	k1gMnPc2	jesuita
v	v	k7c6	v
Paraguayi	Paraguay	k1gFnSc6	Paraguay
<g/>
,	,	kIx,	,
krádeže	krádež	k1gFnPc4	krádež
<g/>
,	,	kIx,	,
smilstvo	smilstvo	k1gNnSc4	smilstvo
<g/>
,	,	kIx,	,
zřízení	zřízení	k1gNnSc4	zřízení
některých	některý	k3yIgFnPc2	některý
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ukázal	ukázat	k5eAaPmAgMnS	ukázat
nesmyslnost	nesmyslnost	k1gFnSc4	nesmyslnost
a	a	k8xC	a
zvrácenost	zvrácenost	k1gFnSc4	zvrácenost
uspořádání	uspořádání	k1gNnSc2	uspořádání
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Candide	Candid	k1gMnSc5	Candid
je	být	k5eAaImIp3nS	být
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgNnPc2d3	veliký
a	a	k8xC	a
nejlepších	dobrý	k2eAgNnPc2d3	nejlepší
Voltairových	Voltairův	k2eAgNnPc2d1	Voltairovo
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
zesměšněna	zesměšněn	k2eAgFnSc1d1	zesměšněna
filozofie	filozofie	k1gFnSc1	filozofie
přehnaného	přehnaný	k2eAgInSc2d1	přehnaný
metafysického	metafysický	k2eAgInSc2d1	metafysický
optimismu	optimismus	k1gInSc2	optimismus
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
vše	všechen	k3xTgNnSc1	všechen
na	na	k7c6	na
tom	ten	k3xDgInSc6	ten
světě	svět	k1gInSc6	svět
je	být	k5eAaImIp3nS	být
dobré	dobrý	k2eAgNnSc1d1	dobré
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
konfrontována	konfrontován	k2eAgFnSc1d1	konfrontována
se	s	k7c7	s
sociální	sociální	k2eAgFnSc7d1	sociální
skutečností	skutečnost	k1gFnSc7	skutečnost
<g/>
.	.	kIx.	.
</s>
<s>
Novela	novela	k1gFnSc1	novela
vyznívá	vyznívat	k5eAaImIp3nS	vyznívat
jako	jako	k9	jako
rozhodný	rozhodný	k2eAgInSc4d1	rozhodný
protest	protest	k1gInSc4	protest
proti	proti	k7c3	proti
životnímu	životní	k2eAgNnSc3d1	životní
zlu	zlo	k1gNnSc3	zlo
<g/>
.	.	kIx.	.
</s>
<s>
VOLTAIRE	VOLTAIRE	kA	VOLTAIRE
a	a	k8xC	a
KOPAL	Kopal	k1gMnSc1	Kopal
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
,	,	kIx,	,
ed	ed	k?	ed
<g/>
.	.	kIx.	.
</s>
<s>
Romány	román	k1gInPc1	román
a	a	k8xC	a
povídky	povídka	k1gFnPc1	povídka
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
v	v	k7c6	v
SNKLHU	SNKLHU	kA	SNKLHU
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Státní	státní	k2eAgNnSc1d1	státní
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
krásné	krásný	k2eAgFnSc2d1	krásná
literatury	literatura	k1gFnSc2	literatura
<g/>
,	,	kIx,	,
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
umění	umění	k1gNnSc1	umění
<g/>
,	,	kIx,	,
1960	[number]	k4	1960
<g/>
.	.	kIx.	.
546	[number]	k4	546
s.	s.	k?	s.
Knihovna	knihovna	k1gFnSc1	knihovna
klasiků	klasik	k1gMnPc2	klasik
<g/>
.	.	kIx.	.
</s>
<s>
Spisy	spis	k1gInPc1	spis
Voltairovy	Voltairův	k2eAgInPc1d1	Voltairův
<g/>
;	;	kIx,	;
sv.	sv.	kA	sv.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
beletristického	beletristický	k2eAgNnSc2d1	beletristické
díla	dílo	k1gNnSc2	dílo
Voltairova	Voltairov	k1gInSc2	Voltairov
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
formou	forma	k1gFnSc7	forma
pohádek	pohádka	k1gFnPc2	pohádka
<g/>
,	,	kIx,	,
orientálních	orientální	k2eAgInPc2d1	orientální
a	a	k8xC	a
dobrodružných	dobrodružný	k2eAgInPc2d1	dobrodružný
příběhů	příběh	k1gInPc2	příběh
kritizuje	kritizovat	k5eAaImIp3nS	kritizovat
stav	stav	k1gInSc1	stav
společnosti	společnost	k1gFnSc2	společnost
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
do	do	k7c2	do
svazku	svazek	k1gInSc2	svazek
zařazeny	zařadit	k5eAaPmNgFnP	zařadit
tyto	tento	k3xDgFnPc1	tento
rozsáhlejší	rozsáhlý	k2eAgFnPc1d2	rozsáhlejší
práce	práce	k1gFnPc1	práce
<g/>
:	:	kIx,	:
Zadig	Zadig	k1gInSc1	Zadig
<g/>
,	,	kIx,	,
Candide	Candid	k1gMnSc5	Candid
<g/>
,	,	kIx,	,
Prosťáček	prosťáček	k1gMnSc1	prosťáček
<g/>
,	,	kIx,	,
Babylónská	babylónský	k2eAgFnSc1d1	Babylónská
princezna	princezna	k1gFnSc1	princezna
<g/>
,	,	kIx,	,
Mikromegas	Mikromegas	k1gInSc1	Mikromegas
<g/>
,	,	kIx,	,
Uši	ucho	k1gNnPc1	ucho
hraběte	hrabě	k1gMnSc2	hrabě
z	z	k7c2	z
Chesterfieldu	Chesterfield	k1gInSc2	Chesterfield
a	a	k8xC	a
kaplan	kaplan	k1gMnSc1	kaplan
Goudman	Goudman	k1gMnSc1	Goudman
<g/>
.	.	kIx.	.
</s>
<s>
Připojeno	připojen	k2eAgNnSc1d1	připojeno
je	být	k5eAaImIp3nS	být
12	[number]	k4	12
povídek	povídka	k1gFnPc2	povídka
<g/>
.	.	kIx.	.
</s>
<s>
VOLTAIRE	VOLTAIRE	kA	VOLTAIRE
a	a	k8xC	a
KOPAL	Kopal	k1gMnSc1	Kopal
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
,	,	kIx,	,
ed	ed	k?	ed
<g/>
.	.	kIx.	.
</s>
<s>
Zaira	Zaira	k1gFnSc1	Zaira
<g/>
;	;	kIx,	;
Verše	verš	k1gInSc2	verš
<g/>
;	;	kIx,	;
Výběr	výběr	k1gInSc1	výběr
z	z	k7c2	z
prací	práce	k1gFnPc2	práce
dějepisných	dějepisný	k2eAgInPc2d1	dějepisný
<g/>
;	;	kIx,	;
Korespondence	korespondence	k1gFnPc1	korespondence
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
v	v	k7c6	v
Odeonu	odeon	k1gInSc6	odeon
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
1974	[number]	k4	1974
<g/>
.	.	kIx.	.
308	[number]	k4	308
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
5	[number]	k4	5
<g/>
]	]	kIx)	]
s.	s.	k?	s.
Voltaire	Voltair	k1gInSc5	Voltair
<g/>
:	:	kIx,	:
Spisy	spis	k1gInPc7	spis
<g/>
;	;	kIx,	;
Sv.	sv.	kA	sv.
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Knihovna	knihovna	k1gFnSc1	knihovna
klasiků	klasik	k1gMnPc2	klasik
<g/>
.	.	kIx.	.
</s>
<s>
Svazek	svazek	k1gInSc1	svazek
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
proslulou	proslulý	k2eAgFnSc4d1	proslulá
tragédii	tragédie	k1gFnSc4	tragédie
Zaira	Zair	k1gInSc2	Zair
<g/>
,	,	kIx,	,
lokalizovanou	lokalizovaný	k2eAgFnSc7d1	lokalizovaná
do	do	k7c2	do
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
doby	doba	k1gFnSc2	doba
křížových	křížový	k2eAgFnPc2d1	křížová
válek	válka	k1gFnPc2	válka
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
zaznívají	zaznívat	k5eAaImIp3nP	zaznívat
ozvuky	ozvuk	k1gInPc1	ozvuk
na	na	k7c4	na
zápletku	zápletka	k1gFnSc4	zápletka
ze	z	k7c2	z
Shakespearova	Shakespearův	k2eAgMnSc2d1	Shakespearův
Othella	Othello	k1gMnSc2	Othello
<g/>
;	;	kIx,	;
výbor	výbor	k1gInSc1	výbor
z	z	k7c2	z
básnického	básnický	k2eAgNnSc2d1	básnické
díla	dílo	k1gNnSc2	dílo
<g/>
;	;	kIx,	;
výběr	výběr	k1gInSc1	výběr
z	z	k7c2	z
dějepisných	dějepisný	k2eAgFnPc2d1	dějepisná
prací	práce	k1gFnPc2	práce
<g/>
,	,	kIx,	,
reprezentovaný	reprezentovaný	k2eAgInSc4d1	reprezentovaný
studií	studie	k1gFnSc7	studie
Století	století	k1gNnPc2	století
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
autor	autor	k1gMnSc1	autor
tento	tento	k3xDgInSc4	tento
věk	věk	k1gInSc4	věk
po	po	k7c6	po
diskreditaci	diskreditace	k1gFnSc6	diskreditace
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
rehabilituje	rehabilitovat	k5eAaBmIp3nS	rehabilitovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
spisem	spis	k1gInSc7	spis
Esej	esej	k1gInSc1	esej
o	o	k7c6	o
mravech	mrav	k1gInPc6	mrav
a	a	k8xC	a
duchu	duch	k1gMnSc6	duch
národů	národ	k1gInPc2	národ
<g/>
;	;	kIx,	;
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
korespondenci	korespondence	k1gFnSc4	korespondence
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
přibližuje	přibližovat	k5eAaImIp3nS	přibližovat
Voltairův	Voltairův	k2eAgInSc4d1	Voltairův
lidský	lidský	k2eAgInSc4d1	lidský
profil	profil	k1gInSc4	profil
<g/>
.	.	kIx.	.
</s>
<s>
VOLTAIRE	VOLTAIRE	kA	VOLTAIRE
<g/>
.	.	kIx.	.
</s>
<s>
Panna	Panna	k1gFnSc1	Panna
<g/>
.	.	kIx.	.
</s>
<s>
Přeložil	přeložit	k5eAaPmAgMnS	přeložit
Radovan	Radovan	k1gMnSc1	Radovan
Krátký	Krátký	k1gMnSc1	Krátký
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
v	v	k7c6	v
SNKLU	SNKLU	kA	SNKLU
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
SNKLU	SNKLU	kA	SNKLU
<g/>
,	,	kIx,	,
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
338	[number]	k4	338
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
5	[number]	k4	5
<g/>
]	]	kIx)	]
s.	s.	k?	s.
Nesmrtelní	nesmrtelný	k1gMnPc1	nesmrtelný
<g/>
;	;	kIx,	;
Sv.	sv.	kA	sv.
64	[number]	k4	64
<g/>
.	.	kIx.	.
</s>
<s>
Slavný	slavný	k2eAgInSc1d1	slavný
pamflet	pamflet	k1gInSc1	pamflet
(	(	kIx(	(
<g/>
veršovaná	veršovaný	k2eAgFnSc1d1	veršovaná
satira	satira	k1gFnSc1	satira
<g/>
)	)	kIx)	)
na	na	k7c6	na
církví	církev	k1gFnPc2	církev
vytvořenou	vytvořený	k2eAgFnSc4d1	vytvořená
legendu	legenda	k1gFnSc4	legenda
o	o	k7c4	o
panenství	panenství	k1gNnSc4	panenství
Jany	Jana	k1gFnSc2	Jana
z	z	k7c2	z
Arcu	Arcus	k1gInSc2	Arcus
<g/>
,	,	kIx,	,
panny	panna	k1gFnSc2	panna
Orleanské	Orleanský	k2eAgFnSc2d1	Orleanská
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
jediná	jediný	k2eAgFnSc1d1	jediná
svou	svůj	k3xOyFgFnSc7	svůj
neposkvrněnou	poskvrněný	k2eNgFnSc7d1	neposkvrněná
čistotou	čistota	k1gFnSc7	čistota
mohla	moct	k5eAaImAgFnS	moct
zachránit	zachránit	k5eAaPmF	zachránit
před	před	k7c7	před
Angličany	Angličan	k1gMnPc7	Angličan
Orleans	Orleans	k1gInSc4	Orleans
a	a	k8xC	a
pak	pak	k6eAd1	pak
i	i	k9	i
bojovat	bojovat	k5eAaImF	bojovat
za	za	k7c4	za
osvobození	osvobození	k1gNnSc4	osvobození
celé	celý	k2eAgFnSc2d1	celá
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Voltaire	Voltair	k1gMnSc5	Voltair
přirozeně	přirozeně	k6eAd1	přirozeně
vydatně	vydatně	k6eAd1	vydatně
rozeznívá	rozeznívat	k5eAaImIp3nS	rozeznívat
strunu	struna	k1gFnSc4	struna
erotiky	erotika	k1gFnSc2	erotika
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
to	ten	k3xDgNnSc1	ten
už	už	k6eAd1	už
dovoluje	dovolovat	k5eAaImIp3nS	dovolovat
a	a	k8xC	a
přímo	přímo	k6eAd1	přímo
žádá	žádat	k5eAaImIp3nS	žádat
sám	sám	k3xTgInSc1	sám
námět	námět	k1gInSc1	námět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
především	především	k6eAd1	především
různými	různý	k2eAgFnPc7d1	různá
situacemi	situace	k1gFnPc7	situace
<g/>
,	,	kIx,	,
do	do	k7c2	do
nichž	jenž	k3xRgInPc2	jenž
uvádí	uvádět	k5eAaImIp3nS	uvádět
přísnou	přísný	k2eAgFnSc4d1	přísná
strážkyni	strážkyně	k1gFnSc4	strážkyně
"	"	kIx"	"
<g/>
toho	ten	k3xDgNnSc2	ten
nejcennějšího	cenný	k2eAgNnSc2d3	nejcennější
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
dívka	dívka	k1gFnSc1	dívka
má	mít	k5eAaImIp3nS	mít
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
zesměšňuje	zesměšňovat	k5eAaImIp3nS	zesměšňovat
svatozář	svatozář	k1gFnSc4	svatozář
<g/>
,	,	kIx,	,
vytvořenou	vytvořený	k2eAgFnSc4d1	vytvořená
kolem	kolem	k7c2	kolem
ní	on	k3xPp3gFnSc2	on
právě	právě	k9	právě
církví	církev	k1gFnPc2	církev
<g/>
,	,	kIx,	,
a	a	k8xC	a
pošetilost	pošetilost	k1gFnSc4	pošetilost
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
jsou	být	k5eAaImIp3nP	být
ochotni	ochoten	k2eAgMnPc1d1	ochoten
jí	on	k3xPp3gFnSc2	on
věřit	věřit	k5eAaImF	věřit
<g/>
.	.	kIx.	.
</s>
<s>
VOLTAIRE	VOLTAIRE	kA	VOLTAIRE
<g/>
.	.	kIx.	.
</s>
<s>
Filosofický	filosofický	k2eAgInSc1d1	filosofický
slovník	slovník	k1gInSc1	slovník
<g/>
:	:	kIx,	:
čili	čili	k8xC	čili
rozum	rozum	k1gInSc4	rozum
podle	podle	k7c2	podle
abecedy	abeceda	k1gFnSc2	abeceda
<g/>
.	.	kIx.	.
</s>
<s>
Přeložila	přeložit	k5eAaPmAgFnS	přeložit
Emma	Emma	k1gFnSc1	Emma
Horká	Horká	k1gFnSc1	Horká
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
prvního	první	k4xOgNnSc2	první
vydání	vydání	k1gNnSc2	vydání
nakladatelské	nakladatelský	k2eAgFnSc2d1	nakladatelská
společnosti	společnost	k1gFnSc2	společnost
Jana	Jan	k1gMnSc2	Jan
Laichtera	Laichter	k1gMnSc2	Laichter
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Votobia	Votobia	k1gFnSc1	Votobia
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
277	[number]	k4	277
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7220	[number]	k4	7220
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
61	[number]	k4	61
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
VOLTAIRE	VOLTAIRE	kA	VOLTAIRE
<g/>
,	,	kIx,	,
François	François	k1gFnSc1	François
Marie	Marie	k1gFnSc1	Marie
<g/>
.	.	kIx.	.
</s>
<s>
Výbor	výbor	k1gInSc1	výbor
z	z	k7c2	z
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Hana	Hana	k1gFnSc1	Hana
Horská	Horská	k1gFnSc1	Horská
<g/>
.	.	kIx.	.
</s>
<s>
Vyd	Vyd	k?	Vyd
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
246	[number]	k4	246
s.	s.	k?	s.
Filozofické	filozofický	k2eAgNnSc4d1	filozofické
dědictví	dědictví	k1gNnSc4	dědictví
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
205	[number]	k4	205
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
Kromě	kromě	k7c2	kromě
vybraných	vybraný	k2eAgNnPc2d1	vybrané
hesel	heslo	k1gNnPc2	heslo
z	z	k7c2	z
Filozofického	filozofický	k2eAgInSc2d1	filozofický
slovníku	slovník	k1gInSc2	slovník
a	a	k8xC	a
ukázek	ukázka	k1gFnPc2	ukázka
z	z	k7c2	z
Pojednání	pojednání	k1gNnSc2	pojednání
o	o	k7c6	o
metafyzice	metafyzika	k1gFnSc6	metafyzika
a	a	k8xC	a
z	z	k7c2	z
Elementů	element	k1gInPc2	element
Newtonovy	Newtonův	k2eAgFnSc2d1	Newtonova
filozofie	filozofie	k1gFnSc2	filozofie
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
výbor	výbor	k1gInSc1	výbor
výňatky	výňatek	k1gInPc7	výňatek
z	z	k7c2	z
Myšlenek	myšlenka	k1gFnPc2	myšlenka
o	o	k7c6	o
vládě	vláda	k1gFnSc6	vláda
a	a	k8xC	a
ze	z	k7c2	z
Závěrů	závěr	k1gInPc2	závěr
z	z	k7c2	z
dějin	dějiny	k1gFnPc2	dějiny
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
seznamují	seznamovat	k5eAaImIp3nP	seznamovat
s	s	k7c7	s
Voltairovými	Voltairův	k2eAgInPc7d1	Voltairův
názory	názor	k1gInPc7	názor
filozoficko-historickými	filozofickoistorický	k2eAgInPc7d1	filozoficko-historický
<g/>
,	,	kIx,	,
náboženskými	náboženský	k2eAgInPc7d1	náboženský
a	a	k8xC	a
politickými	politický	k2eAgInPc7d1	politický
<g/>
.	.	kIx.	.
</s>
<s>
Ukázky	ukázka	k1gFnPc1	ukázka
jsou	být	k5eAaImIp3nP	být
doplněny	doplnit	k5eAaPmNgFnP	doplnit
úvodní	úvodní	k2eAgFnSc7d1	úvodní
studií	studie	k1gFnSc7	studie
o	o	k7c6	o
Voltairově	Voltairův	k2eAgInSc6d1	Voltairův
životě	život	k1gInSc6	život
a	a	k8xC	a
o	o	k7c6	o
významu	význam	k1gInSc6	význam
jeho	jeho	k3xOp3gNnSc2	jeho
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
<s>
VOLTAIRE	VOLTAIRE	kA	VOLTAIRE
<g/>
.	.	kIx.	.
</s>
<s>
Voltaire	Voltair	k1gMnSc5	Voltair
<g/>
:	:	kIx,	:
myslitel	myslitel	k1gMnSc1	myslitel
a	a	k8xC	a
bojovník	bojovník	k1gMnSc1	bojovník
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
svazek	svazek	k1gInSc1	svazek
<g/>
.	.	kIx.	.
</s>
<s>
Filosofie	filosofie	k1gFnSc1	filosofie
[	[	kIx(	[
<g/>
a	a	k8xC	a
<g/>
]	]	kIx)	]
náboženství	náboženství	k1gNnSc1	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Vyd	Vyd	k?	Vyd
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Státní	státní	k2eAgNnSc1d1	státní
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
politické	politický	k2eAgFnSc2d1	politická
literatury	literatura	k1gFnSc2	literatura
<g/>
,	,	kIx,	,
1957	[number]	k4	1957
<g/>
.	.	kIx.	.
599	[number]	k4	599
s.	s.	k?	s.
První	první	k4xOgInSc1	první
svazek	svazek	k1gInSc1	svazek
je	být	k5eAaImIp3nS	být
uveden	uvést	k5eAaPmNgInS	uvést
studií	studie	k1gFnSc7	studie
Ivana	Ivan	k1gMnSc2	Ivan
Svitáka	Sviták	k1gMnSc2	Sviták
"	"	kIx"	"
<g/>
Humanista	humanista	k1gMnSc1	humanista
Voltaire	Voltair	k1gInSc5	Voltair
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
str	str	kA	str
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
104	[number]	k4	104
<g/>
)	)	kIx)	)
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
Voltairovy	Voltairov	k1gInPc4	Voltairov
stěžejní	stěžejní	k2eAgFnSc2d1	stěžejní
filosofické	filosofický	k2eAgFnSc2d1	filosofická
práce	práce	k1gFnSc2	práce
<g/>
:	:	kIx,	:
Filosofické	filosofický	k2eAgFnSc2d1	filosofická
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
neboli	neboli	k8xC	neboli
Anglické	anglický	k2eAgInPc1d1	anglický
<g/>
)	)	kIx)	)
listy	list	k1gInPc1	list
<g/>
,	,	kIx,	,
Pojednání	pojednání	k1gNnPc1	pojednání
o	o	k7c6	o
metafysice	metafysika	k1gFnSc6	metafysika
<g/>
,	,	kIx,	,
Výklad	výklad	k1gInSc1	výklad
knihy	kniha	k1gFnSc2	kniha
o	o	k7c6	o
základech	základ	k1gInPc6	základ
fysiky	fysika	k1gFnSc2	fysika
<g/>
,	,	kIx,	,
Báseň	báseň	k1gFnSc1	báseň
o	o	k7c6	o
pohromě	pohroma	k1gFnSc6	pohroma
lisabonské	lisabonský	k2eAgFnSc6d1	Lisabonská
v	v	k7c6	v
r.	r.	kA	r.
1755	[number]	k4	1755
<g/>
,	,	kIx,	,
Nevědomý	vědomý	k2eNgMnSc1d1	nevědomý
filosof	filosof	k1gMnSc1	filosof
<g/>
,	,	kIx,	,
Je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
se	se	k3xPyFc4	se
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
<g/>
,	,	kIx,	,
vybraná	vybraný	k2eAgNnPc1d1	vybrané
hesla	heslo	k1gNnPc1	heslo
z	z	k7c2	z
Filosofického	filosofický	k2eAgInSc2d1	filosofický
slovníku	slovník	k1gInSc2	slovník
a	a	k8xC	a
Dialogy	dialog	k1gInPc1	dialog
mezi	mezi	k7c7	mezi
A	A	kA	A
<g/>
,	,	kIx,	,
B	B	kA	B
a	a	k8xC	a
C.	C.	kA	C.
Voltaire	Voltair	k1gInSc5	Voltair
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
informuje	informovat	k5eAaBmIp3nS	informovat
o	o	k7c6	o
hospodářských	hospodářský	k2eAgInPc6d1	hospodářský
a	a	k8xC	a
politických	politický	k2eAgInPc6d1	politický
poměrech	poměr	k1gInPc6	poměr
v	v	k7c6	v
soudobé	soudobý	k2eAgFnSc6d1	soudobá
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
,	,	kIx,	,
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
své	svůj	k3xOyFgInPc4	svůj
názory	názor	k1gInPc4	názor
na	na	k7c4	na
různé	různý	k2eAgInPc4d1	různý
filosofické	filosofický	k2eAgInPc4d1	filosofický
<g/>
,	,	kIx,	,
náboženské	náboženský	k2eAgInPc4d1	náboženský
a	a	k8xC	a
přírodovědecké	přírodovědecký	k2eAgInPc4d1	přírodovědecký
problémy	problém	k1gInPc4	problém
a	a	k8xC	a
seznamuje	seznamovat	k5eAaImIp3nS	seznamovat
s	s	k7c7	s
předními	přední	k2eAgMnPc7d1	přední
představiteli	představitel	k1gMnPc7	představitel
filosofie	filosofie	k1gFnSc2	filosofie
a	a	k8xC	a
vědy	věda	k1gFnSc2	věda
17	[number]	k4	17
<g/>
.	.	kIx.	.
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
VOLTAIRE	VOLTAIRE	kA	VOLTAIRE
<g/>
.	.	kIx.	.
</s>
<s>
Voltaire	Voltair	k1gMnSc5	Voltair
<g/>
:	:	kIx,	:
myslitel	myslitel	k1gMnSc1	myslitel
a	a	k8xC	a
bojovník	bojovník	k1gMnSc1	bojovník
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
svazek	svazek	k1gInSc1	svazek
<g/>
.	.	kIx.	.
</s>
<s>
Církev	církev	k1gFnSc1	církev
<g/>
,	,	kIx,	,
stát	stát	k1gInSc1	stát
a	a	k8xC	a
právo	právo	k1gNnSc1	právo
<g/>
.	.	kIx.	.
</s>
<s>
Vyd	Vyd	k?	Vyd
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Státní	státní	k2eAgNnSc1d1	státní
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
politické	politický	k2eAgFnSc2d1	politická
literatury	literatura	k1gFnSc2	literatura
<g/>
,	,	kIx,	,
1957	[number]	k4	1957
<g/>
.	.	kIx.	.
513	[number]	k4	513
s.	s.	k?	s.
Druhý	druhý	k4xOgInSc1	druhý
díl	díl	k1gInSc1	díl
výboru	výbor	k1gInSc2	výbor
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
díla	dílo	k1gNnSc2	dílo
brojící	brojící	k2eAgFnSc7d1	brojící
proti	proti	k7c3	proti
náboženskému	náboženský	k2eAgInSc3d1	náboženský
fanatismu	fanatismus	k1gInSc3	fanatismus
a	a	k8xC	a
justičním	justiční	k2eAgFnPc3d1	justiční
vraždám	vražda	k1gFnPc3	vražda
(	(	kIx(	(
<g/>
Pojednání	pojednání	k1gNnPc4	pojednání
o	o	k7c4	o
snášenlivosti	snášenlivost	k1gFnPc4	snášenlivost
<g/>
,	,	kIx,	,
Zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
smrti	smrt	k1gFnSc6	smrt
rytíře	rytíř	k1gMnSc2	rytíř
de	de	k?	de
la	la	k0	la
Barre	Barr	k1gMnSc5	Barr
<g/>
,	,	kIx,	,
Omyl	omýt	k5eAaPmAgMnS	omýt
arraský	arraský	k1gMnSc1	arraský
<g/>
,	,	kIx,	,
Cena	cena	k1gFnSc1	cena
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
a	a	k8xC	a
lidskosti	lidskost	k1gFnSc2	lidskost
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
práce	práce	k1gFnPc1	práce
historické	historický	k2eAgFnSc2d1	historická
(	(	kIx(	(
<g/>
Republikánské	republikánský	k2eAgFnSc2d1	republikánská
myšlenky	myšlenka	k1gFnSc2	myšlenka
ženevského	ženevský	k2eAgMnSc2d1	ženevský
občana	občan	k1gMnSc2	občan
<g/>
,	,	kIx,	,
Poznámky	poznámka	k1gFnSc2	poznámka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
doplňují	doplňovat	k5eAaImIp3nP	doplňovat
Essay	Essaa	k1gFnPc4	Essaa
o	o	k7c6	o
mravech	mrav	k1gInPc6	mrav
a	a	k8xC	a
duchu	duch	k1gMnSc6	duch
národů	národ	k1gInPc2	národ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
díla	dílo	k1gNnPc1	dílo
národohospodářská	národohospodářský	k2eAgNnPc1d1	národohospodářské
(	(	kIx(	(
<g/>
Rozmluva	rozmluva	k1gFnSc1	rozmluva
filosofa	filosof	k1gMnSc2	filosof
s	s	k7c7	s
ministrem	ministr	k1gMnSc7	ministr
financí	finance	k1gFnPc2	finance
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
díla	dílo	k1gNnSc2	dílo
zamýšlející	zamýšlející	k2eAgMnSc1d1	zamýšlející
se	se	k3xPyFc4	se
nad	nad	k7c7	nad
vývojem	vývoj	k1gInSc7	vývoj
církve	církev	k1gFnSc2	církev
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
jejím	její	k3xOp3gInSc7	její
vztahem	vztah	k1gInSc7	vztah
ke	k	k7c3	k
státu	stát	k1gInSc3	stát
(	(	kIx(	(
<g/>
vybraná	vybraný	k2eAgNnPc1d1	vybrané
hesla	heslo	k1gNnPc1	heslo
z	z	k7c2	z
Filosofického	filosofický	k2eAgInSc2d1	filosofický
slovníku	slovník	k1gInSc2	slovník
<g/>
)	)	kIx)	)
i	i	k9	i
nad	nad	k7c7	nad
společenským	společenský	k2eAgInSc7d1	společenský
významem	význam	k1gInSc7	význam
umění	umění	k1gNnSc2	umění
(	(	kIx(	(
<g/>
Rozmluva	rozmluva	k1gFnSc1	rozmluva
pana	pan	k1gMnSc2	pan
intendanta	intendant	k1gMnSc2	intendant
drobných	drobný	k2eAgFnPc2d1	drobná
zábav	zábava	k1gFnPc2	zábava
královských	královský	k2eAgFnPc2d1	královská
s	s	k7c7	s
abbé	abbé	k1gMnSc7	abbé
Grizelem	Grizel	k1gMnSc7	Grizel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
-	-	kIx~	-
Výbor	výbor	k1gInSc1	výbor
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
Voltaira	Voltair	k1gMnSc4	Voltair
jako	jako	k8xS	jako
předbojovníka	předbojovník	k1gMnSc4	předbojovník
francouzské	francouzský	k2eAgFnSc2d1	francouzská
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
postavil	postavit	k5eAaPmAgMnS	postavit
proti	proti	k7c3	proti
hlavním	hlavní	k2eAgFnPc3d1	hlavní
překážkám	překážka	k1gFnPc3	překážka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
stály	stát	k5eAaImAgFnP	stát
v	v	k7c6	v
cestě	cesta	k1gFnSc6	cesta
kritickému	kritický	k2eAgInSc3d1	kritický
rozumu	rozum	k1gInSc3	rozum
<g/>
:	:	kIx,	:
proti	proti	k7c3	proti
dogmatismu	dogmatismus	k1gInSc3	dogmatismus
v	v	k7c6	v
myšlení	myšlení	k1gNnSc6	myšlení
a	a	k8xC	a
proti	proti	k7c3	proti
despocii	despocie	k1gFnSc3	despocie
v	v	k7c6	v
politice	politika	k1gFnSc6	politika
<g/>
.	.	kIx.	.
</s>
<s>
VOLTAIRE	VOLTAIRE	kA	VOLTAIRE
a	a	k8xC	a
André	André	k1gMnSc1	André
MAUROIS	MAUROIS	kA	MAUROIS
<g/>
.	.	kIx.	.
</s>
<s>
Nesmrtelné	smrtelný	k2eNgFnPc4d1	nesmrtelná
stránky	stránka	k1gFnPc4	stránka
z	z	k7c2	z
Voltaira	Voltair	k1gInSc2	Voltair
<g/>
:	:	kIx,	:
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
vybral	vybrat	k5eAaPmAgMnS	vybrat
a	a	k8xC	a
vysvětlil	vysvětlit	k5eAaPmAgMnS	vysvětlit
André	André	k1gMnSc1	André
Maurois	Maurois	k1gFnSc2	Maurois
<g/>
.	.	kIx.	.
</s>
<s>
Přeložila	přeložit	k5eAaPmAgFnS	přeložit
Eva	Eva	k1gFnSc1	Eva
Formanová	Formanová	k1gFnSc1	Formanová
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Fr.	Fr.	k1gMnSc1	Fr.
Borový	borový	k2eAgMnSc1d1	borový
<g/>
,	,	kIx,	,
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
133	[number]	k4	133
s.	s.	k?	s.
VOLTAIRE	VOLTAIRE	kA	VOLTAIRE
<g/>
,	,	kIx,	,
François	François	k1gFnSc1	François
Marie	Marie	k1gFnSc1	Marie
<g/>
.	.	kIx.	.
</s>
<s>
Všetečné	všetečný	k2eAgFnPc1d1	všetečná
a	a	k8xC	a
bezbožné	bezbožný	k2eAgFnPc1d1	bezbožná
otázky	otázka	k1gFnPc1	otázka
nábožného	nábožný	k2eAgMnSc2d1	nábožný
doktora	doktor	k1gMnSc2	doktor
theologie	theologie	k1gFnSc2	theologie
Zapaty	Zapata	k1gFnSc2	Zapata
<g/>
.	.	kIx.	.
</s>
<s>
Přeložil	přeložit	k5eAaPmAgMnS	přeložit
Karel	Karel	k1gMnSc1	Karel
Pelant	Pelant	k1gMnSc1	Pelant
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Volná	volný	k2eAgFnSc1d1	volná
myšlenka	myšlenka	k1gFnSc1	myšlenka
<g/>
,	,	kIx,	,
1910	[number]	k4	1910
<g/>
.	.	kIx.	.
26	[number]	k4	26
s.	s.	k?	s.
</s>
