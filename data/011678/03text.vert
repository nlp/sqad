<p>
<s>
Gémozac	Gémozac	k1gFnSc1	Gémozac
je	být	k5eAaImIp3nS	být
francouzská	francouzský	k2eAgFnSc1d1	francouzská
obec	obec	k1gFnSc1	obec
v	v	k7c6	v
departementu	departement	k1gInSc6	departement
Charente-Maritime	Charente-Maritim	k1gInSc5	Charente-Maritim
v	v	k7c6	v
regionu	region	k1gInSc6	region
Nová	nový	k2eAgFnSc1d1	nová
Akvitánie	Akvitánie	k1gFnSc1	Akvitánie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
2	[number]	k4	2
802	[number]	k4	802
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poloha	poloha	k1gFnSc1	poloha
obce	obec	k1gFnSc2	obec
==	==	k?	==
</s>
</p>
<p>
<s>
Sousední	sousední	k2eAgFnPc1d1	sousední
obce	obec	k1gFnPc1	obec
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Cravans	Cravans	k1gInSc1	Cravans
<g/>
,	,	kIx,	,
Champagnolles	Champagnolles	k1gInSc1	Champagnolles
<g/>
,	,	kIx,	,
Jazennes	Jazennes	k1gInSc1	Jazennes
<g/>
,	,	kIx,	,
Saint-Germain-du-Seudre	Saint-Germainu-Seudr	k1gInSc5	Saint-Germain-du-Seudr
<g/>
,	,	kIx,	,
Saint-André-de-Lidon	Saint-Andrée-Lidon	k1gNnSc1	Saint-André-de-Lidon
<g/>
,	,	kIx,	,
Saint-Simon-de-Pellouaille	Saint-Simone-Pellouaille	k1gNnSc1	Saint-Simon-de-Pellouaille
<g/>
,	,	kIx,	,
Tanzac	Tanzac	k1gInSc1	Tanzac
<g/>
,	,	kIx,	,
Villars-en-Pons	Villarsn-Pons	k1gInSc1	Villars-en-Pons
a	a	k8xC	a
Virollet	Virollet	k1gInSc1	Virollet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vývoj	vývoj	k1gInSc1	vývoj
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
==	==	k?	==
</s>
</p>
<p>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
obcí	obec	k1gFnPc2	obec
v	v	k7c6	v
departementu	departement	k1gInSc6	departement
Charente-Maritime	Charente-Maritim	k1gMnSc5	Charente-Maritim
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Gémozac	Gémozac	k1gInSc1	Gémozac
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
obce	obec	k1gFnSc2	obec
</s>
</p>
