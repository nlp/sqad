<s>
IOS	IOS	kA
</s>
<s>
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
IOS	IOS	kA
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
iOS	iOS	k?
</s>
<s>
Logo	logo	k1gNnSc1
iOS	iOS	k?
14	#num#	k4
<g/>
Web	web	k1gInSc1
</s>
<s>
www.apple.com/ios	www.apple.com/ios	k1gInSc1
Vyvíjí	vyvíjet	k5eAaImIp3nS
</s>
<s>
Apple	Apple	kA
Inc	Inc	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInPc1
vydání	vydání	k1gNnSc6
</s>
<s>
29	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2007	#num#	k4
Aktuální	aktuální	k2eAgFnSc2d1
verze	verze	k1gFnSc2
</s>
<s>
14.5	14.5	k4
<g/>
.1	.1	k4
/	/	kIx~
3	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2021	#num#	k4
Způsob	způsob	k1gInSc1
aktualizace	aktualizace	k1gFnSc2
</s>
<s>
OTA	Ota	k1gMnSc1
nebo	nebo	k8xC
iTunes	iTunes	k1gMnSc1
Typ	typ	k1gInSc4
jádra	jádro	k1gNnSc2
</s>
<s>
Hybrid	hybrid	k1gInSc1
(	(	kIx(
<g/>
XNU	XNU	kA
<g/>
)	)	kIx)
Programovací	programovací	k2eAgInSc1d1
jazyk	jazyk	k1gInSc1
</s>
<s>
C	C	kA
<g/>
,	,	kIx,
C	C	kA
<g/>
++	++	k?
<g/>
,	,	kIx,
Objective-C	Objective-C	k1gMnSc1
<g/>
,	,	kIx,
Swift	Swift	k2eAgMnSc1d1
Výchozí	výchozí	k2eAgMnSc1d1
uživatelské	uživatelský	k2eAgNnSc4d1
rozhraní	rozhraní	k1gNnSc4
</s>
<s>
Cocoa	Cocoa	k1gMnSc1
Touch	Touch	k1gMnSc1
(	(	kIx(
<g/>
multi-touch	multi-touch	k1gMnSc1
<g/>
,	,	kIx,
GUI	GUI	kA
<g/>
)	)	kIx)
</s>
<s>
iOS	iOS	kA
je	být	k5eAaImIp3nS
mobilní	mobilní	k2eAgInSc1d1
operační	operační	k2eAgInSc1d1
systém	systém	k1gInSc1
pro	pro	k7c4
telefony	telefon	k1gInPc4
iPhone	iPhone	k1gInSc1
společnosti	společnost	k1gFnSc2
Apple	Apple	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejdříve	dříve	k6eAd3
byl	být	k5eAaImAgInS
jako	jako	k9
iPhone	iPhon	k1gInSc5
OS	OS	kA
vyvinut	vyvinout	k5eAaPmNgInS
na	na	k7c6
základě	základ	k1gInSc6
macOS	macOS	k?
pro	pro	k7c4
původní	původní	k2eAgFnSc4d1
iPhone	iPhon	k1gInSc5
uvedený	uvedený	k2eAgInSc4d1
na	na	k7c4
trh	trh	k1gInSc4
v	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
<g/>
,	,	kIx,
později	pozdě	k6eAd2
byl	být	k5eAaImAgInS
upraven	upravit	k5eAaPmNgInS
i	i	k9
pro	pro	k7c4
další	další	k2eAgNnSc4d1
mobilní	mobilní	k2eAgNnSc4d1
zařízení	zařízení	k1gNnSc4
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
multimediální	multimediální	k2eAgInSc1d1
přehrávač	přehrávač	k1gInSc1
iPod	iPod	k6eAd1
touch	touch	k1gInSc4
a	a	k8xC
tablet	tablet	k1gInSc4
iPad	iPada	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nyní	nyní	k6eAd1
jsou	být	k5eAaImIp3nP
z	z	k7c2
něj	on	k3xPp3gNnSc2
odvozené	odvozený	k2eAgInPc1d1
samostatné	samostatný	k2eAgInPc1d1
operační	operační	k2eAgInPc1d1
systémy	systém	k1gInPc1
iPadOS	iPadOS	k?
pro	pro	k7c4
iPady	iPad	k1gMnPc4
<g/>
,	,	kIx,
tvOS	tvOS	k?
pro	pro	k7c4
mediální	mediální	k2eAgFnSc4d1
konzoli	konzole	k1gFnSc4
Apple	Apple	kA
TV	TV	kA
a	a	k8xC
watchOS	watchOS	k?
pro	pro	k7c4
hodinky	hodinka	k1gFnPc4
Apple	Apple	kA
Watch	Watch	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Apple	Apple	kA
pravidelně	pravidelně	k6eAd1
každý	každý	k3xTgInSc1
rok	rok	k1gInSc1
vydává	vydávat	k5eAaImIp3nS,k5eAaPmIp3nS
novou	nový	k2eAgFnSc4d1
verzi	verze	k1gFnSc4
svého	svůj	k3xOyFgInSc2
operačního	operační	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
iOS	iOS	k?
je	být	k5eAaImIp3nS
druhý	druhý	k4xOgInSc1
nejrozšířenější	rozšířený	k2eAgInSc1d3
mobilní	mobilní	k2eAgInSc1d1
systém	systém	k1gInSc1
(	(	kIx(
<g/>
asi	asi	k9
14	#num#	k4
<g/>
%	%	kIx~
podíl	podíl	k1gInSc4
v	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jinak	jinak	k6eAd1
na	na	k7c6
chytrých	chytrý	k2eAgInPc6d1
telefonech	telefon	k1gInPc6
dominuje	dominovat	k5eAaImIp3nS
Android	android	k1gInSc1
společnosti	společnost	k1gFnSc2
Google	Google	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
Androidu	android	k1gInSc2
systém	systém	k1gInSc1
iOS	iOS	k?
není	být	k5eNaImIp3nS
určen	určit	k5eAaPmNgInS
pro	pro	k7c4
ostatní	ostatní	k2eAgMnPc4d1
výrobce	výrobce	k1gMnPc4
<g/>
,	,	kIx,
lze	lze	k6eAd1
jej	on	k3xPp3gMnSc4
instalovat	instalovat	k5eAaBmF
pouze	pouze	k6eAd1
na	na	k7c4
výrobky	výrobek	k1gInPc4
Apple	Apple	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyznačuje	vyznačovat	k5eAaImIp3nS
se	se	k3xPyFc4
také	také	k9
větší	veliký	k2eAgFnSc7d2
uzavřeností	uzavřenost	k1gFnSc7
<g/>
,	,	kIx,
například	například	k6eAd1
jej	on	k3xPp3gMnSc4
nelze	lze	k6eNd1
přizpůsobit	přizpůsobit	k5eAaPmF
pomocí	pomocí	k7c2
nástaveb	nástavba	k1gFnPc2
nebo	nebo	k8xC
do	do	k7c2
něj	on	k3xPp3gMnSc2
instalovat	instalovat	k5eAaBmF
jiné	jiný	k2eAgFnPc1d1
aplikace	aplikace	k1gFnPc1
než	než	k8xS
schválené	schválený	k2eAgFnPc1d1
Applem	Appl	k1gInSc7
a	a	k8xC
publikované	publikovaný	k2eAgFnPc4d1
v	v	k7c6
obchodě	obchod	k1gInSc6
App	App	k1gMnSc5
Store	Stor	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
prolomení	prolomení	k1gNnSc3
těchto	tento	k3xDgNnPc2
omezení	omezení	k1gNnPc2
usilují	usilovat	k5eAaImIp3nP
softwarové	softwarový	k2eAgInPc1d1
nástroje	nástroj	k1gInPc1
zvané	zvaný	k2eAgInPc1d1
jailbreak	jailbreak	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Název	název	k1gInSc1
</s>
<s>
Pojmenování	pojmenování	k1gNnSc1
iOS	iOS	k?
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
až	až	k9
od	od	k7c2
čtvrté	čtvrtá	k1gFnSc2
verze	verze	k1gFnSc2
tohoto	tento	k3xDgInSc2
systému	systém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
byl	být	k5eAaImAgInS
oficiálně	oficiálně	k6eAd1
nazýván	nazývat	k5eAaImNgInS
iPhone	iPhon	k1gInSc5
OS	OS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nový	nový	k2eAgInSc1d1
název	název	k1gInSc1
iOS	iOS	k?
je	být	k5eAaImIp3nS
v	v	k7c6
souladu	soulad	k1gInSc6
s	s	k7c7
politikou	politika	k1gFnSc7
pojmenovávání	pojmenovávání	k1gNnSc2
produktů	produkt	k1gInPc2
(	(	kIx(
<g/>
iPod	iPod	k1gMnSc1
<g/>
,	,	kIx,
iPhone	iPhon	k1gMnSc5
<g/>
,	,	kIx,
iPad	iPad	k6eAd1
<g/>
,	,	kIx,
…	…	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Ihned	ihned	k6eAd1
po	po	k7c6
zveřejnění	zveřejnění	k1gNnSc6
nového	nový	k2eAgInSc2d1
názvu	název	k1gInSc2
iOS	iOS	k?
byla	být	k5eAaImAgFnS
na	na	k7c4
Apple	Apple	kA
podána	podán	k2eAgFnSc1d1
žaloba	žaloba	k1gFnSc1
od	od	k7c2
společnosti	společnost	k1gFnSc2
Cisco	Cisco	k6eAd1
Systems	Systems	k1gInSc4
<g/>
,	,	kIx,
která	který	k3yIgNnPc4,k3yRgNnPc4,k3yQgNnPc4
název	název	k1gInSc1
IOS	IOS	kA
používá	používat	k5eAaImIp3nS
pro	pro	k7c4
označení	označení	k1gNnSc4
softwaru	software	k1gInSc2
na	na	k7c6
svých	svůj	k3xOyFgInPc6
routerech	router	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aby	aby	kYmCp3nS
zabránila	zabránit	k5eAaPmAgFnS
žalobě	žaloba	k1gFnSc3
<g/>
,	,	kIx,
licencovala	licencovat	k5eAaBmAgFnS
si	se	k3xPyFc3
společnost	společnost	k1gFnSc1
Apple	Apple	kA
použití	použití	k1gNnSc2
tohoto	tento	k3xDgInSc2
názvu	název	k1gInSc2
pro	pro	k7c4
svá	svůj	k3xOyFgNnPc4
zařízení	zařízení	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s>
Aktuální	aktuální	k2eAgFnPc1d1
verze	verze	k1gFnPc1
</s>
<s>
Legenda	legenda	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
Vývoj	vývoj	k1gInSc1
ukončen	ukončen	k2eAgInSc1d1
</s>
<s>
Aktuální	aktuální	k2eAgFnPc1d1
</s>
<s>
Beta	beta	k1gNnSc1
</s>
<s>
Verze	verze	k1gFnSc1
</s>
<s>
Vydáno	vydán	k2eAgNnSc1d1
</s>
<s>
Nejvyšší	vysoký	k2eAgNnSc1d3
pro	pro	k7c4
</s>
<s>
3.1	3.1	k4
<g/>
.3	.3	k4
</s>
<s>
20100202	#num#	k4
<g/>
a	a	k8xC
<g/>
2	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2010	#num#	k4
</s>
<s>
iPhone	iPhon	k1gMnSc5
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
iPod	iPod	k1gMnSc1
Touch	Touch	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
4.2	4.2	k4
<g/>
.1	.1	k4
</s>
<s>
20101122	#num#	k4
<g/>
a	a	k8xC
<g/>
22	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2010	#num#	k4
</s>
<s>
iPhone	iPhon	k1gMnSc5
3	#num#	k4
<g/>
G	G	kA
<g/>
,	,	kIx,
iPod	iPod	k1gMnSc1
Touch	Touch	k1gMnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
5.1	5.1	k4
<g/>
.1	.1	k4
</s>
<s>
20120507	#num#	k4
<g/>
a	a	k8xC
<g/>
7	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2012	#num#	k4
</s>
<s>
iPod	iPod	k1gMnSc1
Touch	Touch	k1gMnSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
6.1	6.1	k4
<g/>
.6	.6	k4
</s>
<s>
20140221	#num#	k4
<g/>
a	a	k8xC
<g/>
21	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2014	#num#	k4
</s>
<s>
iPhone	iPhon	k1gMnSc5
3	#num#	k4
<g/>
GS	GS	kA
<g/>
,	,	kIx,
iPod	iPod	k1gMnSc1
Touch	Touch	k1gMnSc1
(	(	kIx(
<g/>
4	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
7.1	7.1	k4
<g/>
.2	.2	k4
</s>
<s>
20140630	#num#	k4
<g/>
a	a	k8xC
<g/>
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2014	#num#	k4
</s>
<s>
iPhone	iPhon	k1gMnSc5
4	#num#	k4
</s>
<s>
9.3	9.3	k4
<g/>
.5	.5	k4
</s>
<s>
20160825	#num#	k4
<g/>
a	a	k8xC
<g/>
25	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2016	#num#	k4
</s>
<s>
iPhone	iPhon	k1gMnSc5
4	#num#	k4
<g/>
S	s	k7c7
<g/>
,	,	kIx,
iPad	iPad	k1gInSc1
2	#num#	k4
<g/>
,	,	kIx,
iPad	iPad	k1gInSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
iPod	iPod	k1gMnSc1
Touch	Touch	k1gMnSc1
(	(	kIx(
<g/>
5	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
iPad	iPad	k6eAd1
mini	mini	k2eAgFnSc1d1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
10.3	10.3	k4
<g/>
.3	.3	k4
</s>
<s>
20170719	#num#	k4
<g/>
a	a	k8xC
<g/>
19	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2017	#num#	k4
</s>
<s>
iPhone	iPhon	k1gMnSc5
5	#num#	k4
<g/>
S	s	k7c7
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
6	#num#	k4
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
6	#num#	k4
Plus	plus	k1gInSc1
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
6	#num#	k4
<g/>
S	s	k7c7
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
6S	6S	k4
Plus	plus	k1gInSc1
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
SE	se	k3xPyFc4
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
7	#num#	k4
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
7	#num#	k4
Plus	plus	k1gInSc1
<g/>
,	,	kIx,
iPod	iPod	k1gMnSc1
Touch	Touch	k1gMnSc1
(	(	kIx(
<g/>
6	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
iPad	iPad	k1gMnSc1
Air	Air	k1gMnSc1
<g/>
,	,	kIx,
iPad	iPad	k1gMnSc1
Air	Air	k1gFnSc2
2	#num#	k4
<g/>
,	,	kIx,
iPad	iPad	k1gInSc1
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
iPad	iPad	k6eAd1
mini	mini	k2eAgFnSc1d1
2	#num#	k4
<g/>
,	,	kIx,
iPad	iPad	k6eAd1
mini	mini	k2eAgFnSc1d1
3	#num#	k4
<g/>
,	,	kIx,
iPad	iPad	k6eAd1
Pro	pro	k7c4
</s>
<s>
11.4	11.4	k4
<g/>
.1	.1	k4
</s>
<s>
20190709	#num#	k4
<g/>
a	a	k8xC
<g/>
9	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2019	#num#	k4
</s>
<s>
iPhone	iPhon	k1gMnSc5
5	#num#	k4
<g/>
S	s	k7c7
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
6	#num#	k4
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
6	#num#	k4
Plus	plus	k1gInSc1
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
6	#num#	k4
<g/>
S	s	k7c7
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
6S	6S	k4
Plus	plus	k1gInSc1
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
SE	se	k3xPyFc4
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
7	#num#	k4
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
7	#num#	k4
Plus	plus	k1gInSc1
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
8	#num#	k4
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
X	X	kA
<g/>
,	,	kIx,
iPod	iPod	k1gMnSc1
Touch	Touch	k1gMnSc1
(	(	kIx(
<g/>
6	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
iPad	iPad	k1gMnSc1
Air	Air	k1gMnSc1
<g/>
,	,	kIx,
iPad	iPad	k1gMnSc1
Air	Air	k1gFnSc2
2	#num#	k4
<g/>
,	,	kIx,
iPad	iPad	k1gInSc1
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
iPad	iPad	k6eAd1
mini	mini	k2eAgFnSc1d1
2	#num#	k4
<g/>
,	,	kIx,
iPad	iPad	k6eAd1
mini	mini	k2eAgFnSc1d1
3	#num#	k4
<g/>
,	,	kIx,
iPad	iPad	k6eAd1
mini	mini	k2eAgFnSc1d1
4	#num#	k4
<g/>
,	,	kIx,
iPad	iPad	k6eAd1
Pro	pro	k7c4
</s>
<s>
12.4	12.4	k4
<g/>
.4	.4	k4
</s>
<s>
20191210	#num#	k4
<g/>
a	a	k8xC
<g/>
10	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2019	#num#	k4
</s>
<s>
iPhone	iPhon	k1gMnSc5
5	#num#	k4
<g/>
S	s	k7c7
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
6	#num#	k4
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
6	#num#	k4
Plus	plus	k1gInSc1
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
6	#num#	k4
<g/>
S	s	k7c7
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
6S	6S	k4
Plus	plus	k1gInSc1
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
SE	se	k3xPyFc4
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
7	#num#	k4
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
7	#num#	k4
Plus	plus	k1gInSc1
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
8	#num#	k4
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
X	X	kA
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
XR	XR	kA
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
iPhone	iPhon	k1gInSc5
XS	XS	kA
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
XS	XS	kA
Max	Max	k1gMnSc1
<g/>
,	,	kIx,
iPod	iPod	k1gMnSc1
Touch	Touch	k1gMnSc1
(	(	kIx(
<g/>
6	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
iPad	iPad	k1gMnSc1
Air	Air	k1gMnSc1
<g/>
,	,	kIx,
iPad	iPad	k1gMnSc1
Air	Air	k1gFnSc2
2	#num#	k4
<g/>
,	,	kIx,
iPad	iPad	k1gInSc1
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
iPad	iPad	k6eAd1
mini	mini	k2eAgFnSc1d1
2	#num#	k4
<g/>
,	,	kIx,
iPad	iPad	k6eAd1
mini	mini	k2eAgFnSc1d1
3	#num#	k4
<g/>
,	,	kIx,
iPad	iPad	k6eAd1
mini	mini	k2eAgFnSc1d1
4	#num#	k4
<g/>
,	,	kIx,
iPad	iPad	k6eAd1
Pro	pro	k7c4
</s>
<s>
13.5	13.5	k4
<g/>
.1	.1	k4
</s>
<s>
20200601	#num#	k4
<g/>
a	a	k8xC
<g/>
1	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2020	#num#	k4
</s>
<s>
iPhone	iPhon	k1gMnSc5
6	#num#	k4
<g/>
S	s	k7c7
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
6S	6S	k4
Plus	plus	k1gInSc1
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
SE	s	k7c7
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
2	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
7	#num#	k4
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
7	#num#	k4
Plus	plus	k1gInSc1
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
8	#num#	k4
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
X	X	kA
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
XR	XR	kA
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
XS	XS	kA
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
XS	XS	kA
Max	Max	k1gMnSc1
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
11	#num#	k4
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
11	#num#	k4
Pro	pro	k7c4
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
11	#num#	k4
Pro	pro	k7c4
Max	max	kA
iPod	iPod	k1gMnSc1
Touch	Touch	k1gMnSc1
(	(	kIx(
<g/>
7	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
13.6	13.6	k4
beta	beta	k1gNnSc1
2	#num#	k4
</s>
<s>
20200609	#num#	k4
<g/>
a	a	k8xC
<g/>
9	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2020	#num#	k4
</s>
<s>
iPhone	iPhon	k1gMnSc5
6	#num#	k4
<g/>
S	s	k7c7
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
6S	6S	k4
Plus	plus	k1gInSc1
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
7	#num#	k4
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
7	#num#	k4
Plus	plus	k1gInSc1
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
SE	s	k7c7
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
2	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
8	#num#	k4
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
X	X	kA
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
XR	XR	kA
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
XS	XS	kA
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
XS	XS	kA
Max	Max	k1gMnSc1
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
11	#num#	k4
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
11	#num#	k4
Pro	pro	k7c4
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
11	#num#	k4
Pro	pro	k7c4
Max	max	kA
iPod	iPod	k1gMnSc1
Touch	Touch	k1gMnSc1
(	(	kIx(
<g/>
7	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
14.0	14.0	k4
beta	beta	k1gNnSc1
1	#num#	k4
</s>
<s>
20200622	#num#	k4
<g/>
a	a	k8xC
<g/>
22	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2020	#num#	k4
</s>
<s>
iPhone	iPhon	k1gMnSc5
6	#num#	k4
<g/>
S	s	k7c7
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
6S	6S	k4
Plus	plus	k1gInSc1
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
7	#num#	k4
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
7	#num#	k4
Plus	plus	k1gInSc1
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
SE	s	k7c7
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
2	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
8	#num#	k4
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
X	X	kA
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
XR	XR	kA
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
XS	XS	kA
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
XS	XS	kA
Max	Max	k1gMnSc1
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
11	#num#	k4
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
11	#num#	k4
Pro	pro	k7c4
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
11	#num#	k4
Pro	pro	k7c4
Max	max	kA
iPod	iPod	k1gMnSc1
Touch	Touch	k1gMnSc1
(	(	kIx(
<g/>
7	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
verzí	verze	k1gFnPc2
</s>
<s>
Přenosná	přenosný	k2eAgNnPc1d1
zařízení	zařízení	k1gNnPc1
(	(	kIx(
<g/>
iPhone	iPhon	k1gMnSc5
<g/>
,	,	kIx,
iPad	iPad	k6eAd1
<g/>
,	,	kIx,
iPod	iPod	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Legenda	legenda	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
Zastaralý	zastaralý	k2eAgInSc1d1
</s>
<s>
Ukončený	ukončený	k2eAgInSc1d1
</s>
<s>
Stávající	stávající	k2eAgFnSc1d1
</s>
<s>
Beta	beta	k1gNnSc1
</s>
<s>
iPhone	iPhon	k1gMnSc5
OS	OS	kA
1	#num#	k4
<g/>
.	.	kIx.
<g/>
x	x	k?
</s>
<s>
První	první	k4xOgFnSc1
verze	verze	k1gFnSc1
dotykového	dotykový	k2eAgInSc2d1
operačního	operační	k2eAgInSc2d1
systému	systém	k1gInSc2
společnosti	společnost	k1gFnSc2
Apple	Apple	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
jeho	jeho	k3xOp3gNnSc6
počátečním	počáteční	k2eAgNnSc6d1
vydání	vydání	k1gNnSc6
nebylo	být	k5eNaImAgNnS
stanoveno	stanovit	k5eAaPmNgNnS
žádné	žádný	k3yNgNnSc1
oficiální	oficiální	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2008	#num#	k4
s	s	k7c7
vydáním	vydání	k1gNnSc7
iPhone	iPhon	k1gInSc5
software	software	k1gInSc1
development	development	k1gInSc1
kit	kit	k1gInSc4
(	(	kIx(
<g/>
iPhone	iPhon	k1gMnSc5
SDK	SDK	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Apple	Apple	kA
oficiálně	oficiálně	k6eAd1
systém	systém	k1gInSc4
pojmenoval	pojmenovat	k5eAaPmAgMnS
jako	jako	k8xC,k8xS
iPhone	iPhon	k1gInSc5
OS	OS	kA
(	(	kIx(
<g/>
Systém	systém	k1gInSc1
byl	být	k5eAaImAgInS
přejmenován	přejmenovat	k5eAaPmNgInS
na	na	k7c4
"	"	kIx"
<g/>
iOS	iOS	k?
<g/>
"	"	kIx"
7	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2010	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Tabulka	tabulka	k1gFnSc1
verzí	verze	k1gFnPc2
<g/>
:	:	kIx,
iPhone	iPhon	k1gInSc5
OS	OS	kA
1	#num#	k4
<g/>
.	.	kIx.
<g/>
x	x	k?
–	–	k?
Přenosná	přenosný	k2eAgFnSc1d1
zařízení	zařízení	k1gNnPc1
</s>
<s>
Verze	verze	k1gFnSc1
</s>
<s>
Build	Build	k6eAd1
</s>
<s>
Baseband	Baseband	k1gInSc1
</s>
<s>
Vydáno	vydán	k2eAgNnSc1d1
</s>
<s>
1.0	1.0	k4
</s>
<s>
1	#num#	k4
<g/>
A	a	k8xC
<g/>
543	#num#	k4
<g/>
a	a	k8xC
</s>
<s>
0	#num#	k4
<g/>
3.11	3.11	k4
<g/>
.02	.02	k4
<g/>
_G	_G	k?
</s>
<s>
20070629	#num#	k4
<g/>
a	a	k8xC
<g/>
29	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2007	#num#	k4
</s>
<s>
1.0	1.0	k4
<g/>
.1	.1	k4
</s>
<s>
1C25	1C25	k4
</s>
<s>
0	#num#	k4
<g/>
3.12	3.12	k4
<g/>
.08	.08	k4
<g/>
_G	_G	k?
</s>
<s>
20070631	#num#	k4
<g/>
a	a	k8xC
<g/>
31	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2007	#num#	k4
</s>
<s>
1.0	1.0	k4
<g/>
.2	.2	k4
</s>
<s>
1C28	1C28	k4
</s>
<s>
0	#num#	k4
<g/>
3.14	3.14	k4
<g/>
.08	.08	k4
<g/>
_G	_G	k?
</s>
<s>
20070821	#num#	k4
<g/>
a	a	k8xC
<g/>
21	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2007	#num#	k4
</s>
<s>
1.1	1.1	k4
</s>
<s>
3	#num#	k4
<g/>
A	a	k8xC
<g/>
100	#num#	k4
<g/>
a	a	k8xC
</s>
<s>
3	#num#	k4
<g/>
A	a	k8xC
<g/>
101	#num#	k4
<g/>
a	a	k8xC
</s>
<s>
20070914	#num#	k4
<g/>
a	a	k8xC
<g/>
14	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2007	#num#	k4
</s>
<s>
1.1	1.1	k4
<g/>
.1	.1	k4
</s>
<s>
3	#num#	k4
<g/>
A	a	k8xC
<g/>
109	#num#	k4
<g/>
a	a	k8xC
</s>
<s>
3	#num#	k4
<g/>
A	a	k8xC
<g/>
110	#num#	k4
<g/>
a	a	k8xC
</s>
<s>
0	#num#	k4
<g/>
4.01	4.01	k4
<g/>
.13	.13	k4
<g/>
_G	_G	k?
</s>
<s>
20070927	#num#	k4
<g/>
a	a	k8xC
<g/>
27	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2007	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1.1	1.1	k4
<g/>
.2	.2	k4
</s>
<s>
3	#num#	k4
<g/>
B	B	kA
<g/>
48	#num#	k4
<g/>
b	b	k?
</s>
<s>
0	#num#	k4
<g/>
4.02	4.02	k4
<g/>
.13	.13	k4
<g/>
_G	_G	k?
</s>
<s>
20071112	#num#	k4
<g/>
a	a	k8xC
<g/>
12	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2007	#num#	k4
</s>
<s>
1.1	1.1	k4
<g/>
.3	.3	k4
</s>
<s>
4A93	4A93	k4
</s>
<s>
0	#num#	k4
<g/>
4.03	4.03	k4
<g/>
.13	.13	k4
<g/>
_G	_G	k?
</s>
<s>
20080115	#num#	k4
<g/>
a	a	k8xC
<g/>
15	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2008	#num#	k4
</s>
<s>
1.1	1.1	k4
<g/>
.4	.4	k4
</s>
<s>
4A102	4A102	k4
</s>
<s>
0	#num#	k4
<g/>
4.04	4.04	k4
<g/>
.05	.05	k4
<g/>
_G	_G	k?
</s>
<s>
20080226	#num#	k4
<g/>
a	a	k8xC
<g/>
26	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2008	#num#	k4
</s>
<s>
1.1	1.1	k4
<g/>
.5	.5	k4
</s>
<s>
4B1	4B1	k4
</s>
<s>
20080715	#num#	k4
<g/>
a	a	k8xC
<g/>
15	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2008	#num#	k4
</s>
<s>
Verze	verze	k1gFnSc1
</s>
<s>
Build	Build	k6eAd1
</s>
<s>
Baseband	Baseband	k1gInSc1
</s>
<s>
Vydáno	vydán	k2eAgNnSc1d1
</s>
<s>
iPhone	iPhon	k1gMnSc5
OS	OS	kA
2	#num#	k4
<g/>
.	.	kIx.
<g/>
x	x	k?
</s>
<s>
2.0	2.0	k4
<g/>
,	,	kIx,
druhá	druhý	k4xOgFnSc1
hlavní	hlavní	k2eAgFnSc1d1
verze	verze	k1gFnSc1
iOS	iOS	k?
<g/>
,	,	kIx,
vyšla	vyjít	k5eAaPmAgFnS
11	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2008	#num#	k4
s	s	k7c7
vydáním	vydání	k1gNnSc7
iPhonu	iPhon	k1gInSc2
3	#num#	k4
<g/>
G.	G.	kA
Zařízení	zařízení	k1gNnSc1
běžící	běžící	k2eAgFnSc7d1
na	na	k7c4
1	#num#	k4
<g/>
.	.	kIx.
<g/>
x	x	k?
lze	lze	k6eAd1
upgradovat	upgradovat	k5eAaImF
na	na	k7c4
tuto	tento	k3xDgFnSc4
verzi	verze	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
verze	verze	k1gFnSc1
systému	systém	k1gInSc2
přináší	přinášet	k5eAaImIp3nS
App	App	k1gMnSc5
Store	Stor	k1gMnSc5
<g/>
,	,	kIx,
takže	takže	k8xS
aplikace	aplikace	k1gFnPc1
třetích	třetí	k4xOgFnPc2
stran	strana	k1gFnPc2
jsou	být	k5eAaImIp3nP
k	k	k7c3
dispozici	dispozice	k1gFnSc3
pro	pro	k7c4
iPhone	iPhon	k1gInSc5
a	a	k8xC
iPod	iPod	k1gMnSc1
Touch	Touch	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Tabulka	tabulka	k1gFnSc1
verzí	verze	k1gFnPc2
<g/>
:	:	kIx,
iPhone	iPhon	k1gInSc5
OS	OS	kA
2	#num#	k4
<g/>
.	.	kIx.
<g/>
x	x	k?
–	–	k?
Přenosná	přenosný	k2eAgFnSc1d1
zařízení	zařízení	k1gNnPc1
</s>
<s>
Verze	verze	k1gFnSc1
</s>
<s>
Build	Build	k6eAd1
</s>
<s>
Baseband	Baseband	k1gInSc1
</s>
<s>
Vydáno	vydán	k2eAgNnSc1d1
</s>
<s>
2.0	2.0	k4
</s>
<s>
5A347	5A347	k4
</s>
<s>
0	#num#	k4
<g/>
4.05	4.05	k4
<g/>
.04	.04	k4
<g/>
_G	_G	k?
</s>
<s>
0	#num#	k4
<g/>
1.45	1.45	k4
<g/>
.00	.00	k4
</s>
<s>
20080711	#num#	k4
<g/>
a	a	k8xC
<g/>
11	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2008	#num#	k4
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2.0	2.0	k4
<g/>
.1	.1	k4
</s>
<s>
5B108	5B108	k4
</s>
<s>
0	#num#	k4
<g/>
4.05	4.05	k4
<g/>
.04	.04	k4
<g/>
_G	_G	k?
</s>
<s>
0	#num#	k4
<g/>
1.48	1.48	k4
<g/>
.02	.02	k4
</s>
<s>
20080804	#num#	k4
<g/>
a	a	k8xC
<g/>
4	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2008	#num#	k4
</s>
<s>
2.0	2.0	k4
<g/>
.2	.2	k4
</s>
<s>
5C1	5C1	k4
</s>
<s>
0	#num#	k4
<g/>
4.05	4.05	k4
<g/>
.04	.04	k4
<g/>
_G	_G	k?
</s>
<s>
0	#num#	k4
<g/>
2.08	2.08	k4
<g/>
.01	.01	k4
</s>
<s>
20080818	#num#	k4
<g/>
a	a	k8xC
<g/>
18	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2008	#num#	k4
</s>
<s>
2.1	2.1	k4
</s>
<s>
5F136	5F136	k4
</s>
<s>
5F137	5F137	k4
</s>
<s>
5F138	5F138	k4
</s>
<s>
9M2517	9M2517	k4
</s>
<s>
0	#num#	k4
<g/>
4.05	4.05	k4
<g/>
.04	.04	k4
<g/>
_G	_G	k?
</s>
<s>
0	#num#	k4
<g/>
2.11	2.11	k4
<g/>
.07	.07	k4
</s>
<s>
20080909	#num#	k4
<g/>
a	a	k8xC
<g/>
9	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2008	#num#	k4
</s>
<s>
20080912	#num#	k4
<g/>
a	a	k8xC
<g/>
12	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2008	#num#	k4
</s>
<s>
2.2	2.2	k4
</s>
<s>
5G77	5G77	k4
</s>
<s>
5	#num#	k4
<g/>
G	G	kA
<g/>
77	#num#	k4
<g/>
a	a	k8xC
</s>
<s>
0	#num#	k4
<g/>
4.05	4.05	k4
<g/>
.04	.04	k4
<g/>
_G	_G	k?
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
0	#num#	k4
<g/>
2.28	2.28	k4
<g/>
.00	.00	k4
</s>
<s>
20081121	#num#	k4
<g/>
a	a	k8xC
<g/>
21	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2008	#num#	k4
</s>
<s>
2.2	2.2	k4
<g/>
.1	.1	k4
</s>
<s>
5H11	5H11	k4
</s>
<s>
5	#num#	k4
<g/>
H	H	kA
<g/>
11	#num#	k4
<g/>
a	a	k8xC
</s>
<s>
9	#num#	k4
<g/>
M	M	kA
<g/>
2621	#num#	k4
<g/>
a	a	k8xC
</s>
<s>
0	#num#	k4
<g/>
4.05	4.05	k4
<g/>
.04	.04	k4
<g/>
_G	_G	k?
</s>
<s>
0	#num#	k4
<g/>
2.30	2.30	k4
<g/>
.03	.03	k4
</s>
<s>
20090127	#num#	k4
<g/>
a	a	k8xC
<g/>
27	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2009	#num#	k4
</s>
<s>
Verze	verze	k1gFnSc1
</s>
<s>
Build	Build	k6eAd1
</s>
<s>
Baseband	Baseband	k1gInSc1
</s>
<s>
Vydáno	vydán	k2eAgNnSc1d1
</s>
<s>
iPhone	iPhon	k1gMnSc5
OS	OS	kA
3	#num#	k4
<g/>
.	.	kIx.
<g/>
x	x	k?
</s>
<s>
3.0	3.0	k4
<g/>
,	,	kIx,
třetí	třetí	k4xOgFnSc1
verze	verze	k1gFnSc1
operačního	operační	k2eAgInSc2d1
systému	systém	k1gInSc2
iOS	iOS	k?
vyšla	vyjít	k5eAaPmAgFnS
17	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
verze	verze	k1gFnPc4
přidává	přidávat	k5eAaImIp3nS
funkce	funkce	k1gFnSc1
jako	jako	k9
je	být	k5eAaImIp3nS
kopírování	kopírování	k1gNnSc1
a	a	k8xC
vkládání	vkládání	k1gNnSc1
a	a	k8xC
MMS	MMS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgFnPc1
nové	nový	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
nejsou	být	k5eNaImIp3nP
dostupné	dostupný	k2eAgFnPc1d1
pro	pro	k7c4
iPhone	iPhon	k1gInSc5
první	první	k4xOgFnPc1
generace	generace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zařízení	zařízení	k1gNnSc1
běžící	běžící	k2eAgFnSc2d1
na	na	k7c6
2	#num#	k4
<g/>
.	.	kIx.
<g/>
x	x	k?
lze	lze	k6eAd1
upgradovat	upgradovat	k5eAaImF
na	na	k7c4
tuto	tento	k3xDgFnSc4
verzi	verze	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tabulka	tabulka	k1gFnSc1
verzí	verze	k1gFnPc2
<g/>
:	:	kIx,
iPhone	iPhon	k1gInSc5
OS	OS	kA
3	#num#	k4
<g/>
.	.	kIx.
<g/>
x	x	k?
–	–	k?
Přenosná	přenosný	k2eAgFnSc1d1
zařízení	zařízení	k1gNnPc1
</s>
<s>
Verze	verze	k1gFnSc1
</s>
<s>
Build	Build	k6eAd1
</s>
<s>
Baseband	Baseband	k1gInSc1
</s>
<s>
Vydáno	vydán	k2eAgNnSc1d1
</s>
<s>
iPhone	iPhon	k1gMnSc5
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
3	#num#	k4
<g/>
G	G	kA
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
3GS	3GS	k4
a	a	k8xC
iPod	iPod	k1gMnSc1
Touch	Touch	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
2	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
3.0	3.0	k4
</s>
<s>
7A341	7A341	k4
</s>
<s>
0	#num#	k4
<g/>
4.05	4.05	k4
<g/>
.04	.04	k4
<g/>
_G	_G	k?
</s>
<s>
0	#num#	k4
<g/>
4.26	4.26	k4
<g/>
.08	.08	k4
</s>
<s>
20090617	#num#	k4
<g/>
a	a	k8xC
<g/>
17	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2009	#num#	k4
</s>
<s>
3.0	3.0	k4
<g/>
.1	.1	k4
</s>
<s>
7A400	7A400	k4
</s>
<s>
0	#num#	k4
<g/>
4.05	4.05	k4
<g/>
.04	.04	k4
<g/>
_G	_G	k?
</s>
<s>
0	#num#	k4
<g/>
4.26	4.26	k4
<g/>
.08	.08	k4
</s>
<s>
20090731	#num#	k4
<g/>
a	a	k8xC
<g/>
31	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2009	#num#	k4
</s>
<s>
iPhone	iPhon	k1gMnSc5
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
3	#num#	k4
<g/>
G	G	kA
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
3GS	3GS	k4
a	a	k8xC
iPod	iPod	k1gMnSc1
Touch	Touch	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
2	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
3	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
3.1	3.1	k4
</s>
<s>
7C144	7C144	k4
</s>
<s>
7C145	7C145	k4
</s>
<s>
7C146	7C146	k4
</s>
<s>
0	#num#	k4
<g/>
4.05	4.05	k4
<g/>
.04	.04	k4
<g/>
_G	_G	k?
</s>
<s>
0	#num#	k4
<g/>
5.11	5.11	k4
<g/>
.07	.07	k4
</s>
<s>
20090909	#num#	k4
<g/>
a	a	k8xC
<g/>
9	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2009	#num#	k4
</s>
<s>
3.1	3.1	k4
<g/>
.2	.2	k4
</s>
<s>
7D11	7D11	k4
</s>
<s>
0	#num#	k4
<g/>
4.05	4.05	k4
<g/>
.04	.04	k4
<g/>
_G	_G	k?
</s>
<s>
0	#num#	k4
<g/>
5.11	5.11	k4
<g/>
.07	.07	k4
</s>
<s>
20091008	#num#	k4
<g/>
a	a	k8xC
<g/>
8	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2009	#num#	k4
</s>
<s>
3.1	3.1	k4
<g/>
.3	.3	k4
</s>
<s>
7E18	7E18	k4
</s>
<s>
0	#num#	k4
<g/>
4.05	4.05	k4
<g/>
.04	.04	k4
<g/>
_G	_G	k?
<g/>
0	#num#	k4
<g/>
5.12	5.12	k4
<g/>
.01	.01	k4
</s>
<s>
20100202	#num#	k4
<g/>
a	a	k8xC
<g/>
2	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2010	#num#	k4
</s>
<s>
Pouze	pouze	k6eAd1
iPad	iPad	k6eAd1
</s>
<s>
3.2	3.2	k4
</s>
<s>
7B367	7B367	k4
</s>
<s>
0	#num#	k4
<g/>
6.15	6.15	k4
<g/>
.00	.00	k4
</s>
<s>
20100403	#num#	k4
<g/>
a	a	k8xC
<g/>
3	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2010	#num#	k4
</s>
<s>
3.2	3.2	k4
<g/>
.1	.1	k4
</s>
<s>
7B405	7B405	k4
</s>
<s>
0	#num#	k4
<g/>
6.15	6.15	k4
<g/>
.00	.00	k4
</s>
<s>
20100715	#num#	k4
<g/>
a	a	k8xC
<g/>
15	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2010	#num#	k4
</s>
<s>
3.2	3.2	k4
<g/>
.2	.2	k4
</s>
<s>
7B500	7B500	k4
</s>
<s>
0	#num#	k4
<g/>
6.15	6.15	k4
<g/>
.00	.00	k4
</s>
<s>
20100811	#num#	k4
<g/>
a	a	k8xC
<g/>
11	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2010	#num#	k4
</s>
<s>
Verze	verze	k1gFnSc1
</s>
<s>
Build	Build	k6eAd1
</s>
<s>
Baseband	Baseband	k1gInSc1
</s>
<s>
Vydáno	vydán	k2eAgNnSc1d1
</s>
<s>
iOS	iOS	k?
4	#num#	k4
<g/>
.	.	kIx.
<g/>
x	x	k?
</s>
<s>
Logo	logo	k1gNnSc1
iOS	iOS	k?
4	#num#	k4
</s>
<s>
iOS	iOS	k?
4	#num#	k4
byl	být	k5eAaImAgInS
zpřístupněn	zpřístupněn	k2eAgInSc1d1
veřejnosti	veřejnost	k1gFnSc3
pro	pro	k7c4
iPhone	iPhon	k1gInSc5
a	a	k8xC
iPod	iPod	k1gMnSc1
Touch	Touch	k1gMnSc1
21	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2010	#num#	k4
a	a	k8xC
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
první	první	k4xOgFnSc1
verze	verze	k1gFnSc1
přejmenována	přejmenován	k2eAgFnSc1d1
jednoduše	jednoduše	k6eAd1
na	na	k7c4
"	"	kIx"
<g/>
iOS	iOS	k?
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
je	být	k5eAaImIp3nS
první	první	k4xOgFnSc1
verze	verze	k1gFnSc1
iOS	iOS	k?
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
ukončuje	ukončovat	k5eAaImIp3nS
podporu	podpora	k1gFnSc4
některým	některý	k3yIgNnPc3
zařízením	zařízení	k1gNnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
také	také	k9
první	první	k4xOgFnSc1
verze	verze	k1gFnSc1
<g/>
,	,	kIx,
za	za	k7c4
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
uživatelé	uživatel	k1gMnPc1
iPodů	iPod	k1gMnPc2
Touch	Touch	k1gInSc1
nemusí	muset	k5eNaImIp3nS
platit	platit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
iPhone	iPhon	k1gMnSc5
3G	3G	k4
a	a	k8xC
iPod	iPod	k1gMnSc1
Touch	Touch	k1gMnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc2
<g/>
)	)	kIx)
mají	mít	k5eAaImIp3nP
omezené	omezený	k2eAgFnPc1d1
funkce	funkce	k1gFnPc1
<g/>
,	,	kIx,
včetně	včetně	k7c2
nedostatečné	dostatečný	k2eNgFnSc2d1
možnosti	možnost	k1gFnSc2
multitaskingu	multitasking	k1gInSc2
a	a	k8xC
možnosti	možnost	k1gFnSc2
nastavit	nastavit	k5eAaPmF
tapetu	tapeta	k1gFnSc4
na	na	k7c6
domovské	domovský	k2eAgFnSc6d1
obrazovce	obrazovka	k1gFnSc6
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
u	u	k7c2
iPhone	iPhon	k1gMnSc5
4	#num#	k4
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
3	#num#	k4
<g/>
GS	GS	kA
<g/>
,	,	kIx,
iPod	iPod	k1gMnSc1
Touch	Touch	k1gMnSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
4	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc2
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
všechny	všechen	k3xTgFnPc1
funkce	funkce	k1gFnPc1
aktivovány	aktivován	k2eAgFnPc1d1
<g/>
.	.	kIx.
iPhone	iPhon	k1gInSc5
a	a	k8xC
iPod	iPod	k1gMnSc1
Touch	Touch	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc2
<g/>
)	)	kIx)
nepodporují	podporovat	k5eNaImIp3nP
iOS	iOS	k?
4.0	4.0	k4
a	a	k8xC
vyšší	vysoký	k2eAgMnSc1d2
<g/>
.	.	kIx.
</s>
<s>
iOS	iOS	k?
4.2	4.2	k4
<g/>
.1	.1	k4
<g/>
,	,	kIx,
vydaný	vydaný	k2eAgInSc4d1
22	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2010	#num#	k4
<g/>
,	,	kIx,
přidává	přidávat	k5eAaImIp3nS
podporu	podpora	k1gFnSc4
iPadu	iPad	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
také	také	k9
poslední	poslední	k2eAgFnSc1d1
verze	verze	k1gFnSc1
podporovaná	podporovaný	k2eAgFnSc1d1
iPhonem	iPhon	k1gInSc7
3G	3G	k4
a	a	k8xC
iPodem	iPod	k1gMnSc7
Touch	Touch	k1gMnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Tabulka	tabulka	k1gFnSc1
verzí	verze	k1gFnPc2
<g/>
:	:	kIx,
iOS	iOS	k?
4	#num#	k4
<g/>
.	.	kIx.
<g/>
x	x	k?
–	–	k?
Přenosná	přenosný	k2eAgFnSc1d1
zařízení	zařízení	k1gNnPc1
</s>
<s>
Verze	verze	k1gFnSc1
</s>
<s>
Build	Build	k6eAd1
</s>
<s>
Baseband	Baseband	k1gInSc1
</s>
<s>
Vydáno	vydán	k2eAgNnSc1d1
</s>
<s>
iPhone	iPhon	k1gMnSc5
3	#num#	k4
<g/>
G	G	kA
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
3	#num#	k4
<g/>
GS	GS	kA
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
4	#num#	k4
a	a	k8xC
iPod	iPod	k1gMnSc1
Touch	Touch	k1gMnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
3	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
4.0	4.0	k4
</s>
<s>
8A293	8A293	k4
</s>
<s>
0	#num#	k4
<g/>
5.13	5.13	k4
<g/>
.04	.04	k4
0	#num#	k4
<g/>
1.59	1.59	k4
<g/>
.00	.00	k4
</s>
<s>
20100621	#num#	k4
<g/>
a	a	k8xC
<g/>
21	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2010	#num#	k4
</s>
<s>
4.0	4.0	k4
<g/>
.1	.1	k4
</s>
<s>
8A306	8A306	k4
</s>
<s>
0	#num#	k4
<g/>
5.13	5.13	k4
<g/>
.04	.04	k4
0	#num#	k4
<g/>
1.59	1.59	k4
<g/>
.00	.00	k4
</s>
<s>
20100715	#num#	k4
<g/>
a	a	k8xC
<g/>
15	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2010	#num#	k4
</s>
<s>
4.0	4.0	k4
<g/>
.2	.2	k4
</s>
<s>
8A400	8A400	k4
</s>
<s>
0	#num#	k4
<g/>
5.13	5.13	k4
<g/>
.04	.04	k4
0	#num#	k4
<g/>
1.59	1.59	k4
<g/>
.00	.00	k4
</s>
<s>
20100811	#num#	k4
<g/>
a	a	k8xC
<g/>
11	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2010	#num#	k4
</s>
<s>
iPhone	iPhon	k1gMnSc5
3	#num#	k4
<g/>
G	G	kA
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
3	#num#	k4
<g/>
GS	GS	kA
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
4	#num#	k4
a	a	k8xC
iPod	iPod	k1gMnSc1
Touch	Touch	k1gMnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
3	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
4	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
4.1	4.1	k4
</s>
<s>
8B117	8B117	k4
</s>
<s>
0	#num#	k4
<g/>
5.14	5.14	k4
<g/>
.02	.02	k4
0	#num#	k4
<g/>
2.10	2.10	k4
<g/>
.04	.04	k4
</s>
<s>
20100908	#num#	k4
<g/>
a	a	k8xC
<g/>
8	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2010	#num#	k4
</s>
<s>
iPhone	iPhon	k1gMnSc5
3	#num#	k4
<g/>
G	G	kA
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
3	#num#	k4
<g/>
GS	GS	kA
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
4	#num#	k4
<g/>
,	,	kIx,
iPod	iPod	k1gMnSc1
Touch	Touch	k1gMnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
3	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
4	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
iPad	iPad	k6eAd1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
4.2	4.2	k4
</s>
<s>
8C134	8C134	k4
8	#num#	k4
<g/>
C	C	kA
<g/>
134	#num#	k4
<g/>
b	b	k?
</s>
<s>
0	#num#	k4
<g/>
5.15	5.15	k4
<g/>
.04	.04	k4
0	#num#	k4
<g/>
3.10	3.10	k4
<g/>
.01	.01	k4
0	#num#	k4
<g/>
7.10	7.10	k4
<g/>
.00	.00	k4
</s>
<s>
nevyšla	vyjít	k5eNaPmAgFnS
<g/>
,	,	kIx,
okamžitě	okamžitě	k6eAd1
nahrazena	nahrazen	k2eAgFnSc1d1
iOS	iOS	k?
4.2	4.2	k4
<g/>
.1	.1	k4
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
4.2	4.2	k4
<g/>
.1	.1	k4
</s>
<s>
8C148	8C148	k4
8	#num#	k4
<g/>
C	C	kA
<g/>
148	#num#	k4
<g/>
a	a	k8xC
</s>
<s>
0	#num#	k4
<g/>
5.15	5.15	k4
<g/>
.04	.04	k4
0	#num#	k4
<g/>
3.10	3.10	k4
<g/>
.01	.01	k4
0	#num#	k4
<g/>
7.10	7.10	k4
<g/>
.00	.00	k4
</s>
<s>
20101122	#num#	k4
<g/>
a	a	k8xC
<g/>
22	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2010	#num#	k4
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
4.2	4.2	k4
<g/>
.5	.5	k4
</s>
<s>
8E128	8E128	k4
(	(	kIx(
<g/>
Pouze	pouze	k6eAd1
CDMA	CDMA	kA
(	(	kIx(
<g/>
Verizon	Verizon	k1gNnSc1
<g/>
)	)	kIx)
iPhone	iPhon	k1gMnSc5
4	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1.0	1.0	k4
<g/>
.05	.05	k4
</s>
<s>
20110207	#num#	k4
<g/>
a	a	k8xC
<g/>
7	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2011	#num#	k4
</s>
<s>
4.2	4.2	k4
<g/>
.6	.6	k4
</s>
<s>
8E200	8E200	k4
(	(	kIx(
<g/>
Pouze	pouze	k6eAd1
CDMA	CDMA	kA
(	(	kIx(
<g/>
Verizon	Verizon	k1gNnSc1
<g/>
)	)	kIx)
iPhone	iPhon	k1gMnSc5
4	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1.0	1.0	k4
<g/>
.05	.05	k4
</s>
<s>
20110210	#num#	k4
<g/>
a	a	k8xC
<g/>
10	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2011	#num#	k4
</s>
<s>
4.2	4.2	k4
<g/>
.7	.7	k4
</s>
<s>
8E303	8E303	k4
(	(	kIx(
<g/>
Pouze	pouze	k6eAd1
CDMA	CDMA	kA
(	(	kIx(
<g/>
Verizon	Verizon	k1gNnSc1
<g/>
)	)	kIx)
iPhone	iPhon	k1gMnSc5
4	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1.0	1.0	k4
<g/>
.06	.06	k4
</s>
<s>
20110414	#num#	k4
<g/>
a	a	k8xC
<g/>
14	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2011	#num#	k4
</s>
<s>
4.2	4.2	k4
<g/>
.8	.8	k4
</s>
<s>
8E401	8E401	k4
(	(	kIx(
<g/>
Pouze	pouze	k6eAd1
CDMA	CDMA	kA
(	(	kIx(
<g/>
Verizon	Verizon	k1gNnSc1
<g/>
)	)	kIx)
iPhone	iPhon	k1gMnSc5
4	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1.0	1.0	k4
<g/>
.06	.06	k4
</s>
<s>
20110504	#num#	k4
<g/>
a	a	k8xC
<g/>
4	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2011	#num#	k4
</s>
<s>
4.2	4.2	k4
<g/>
.9	.9	k4
</s>
<s>
8E501	8E501	k4
(	(	kIx(
<g/>
Pouze	pouze	k6eAd1
CDMA	CDMA	kA
(	(	kIx(
<g/>
Verizon	Verizon	k1gNnSc1
<g/>
)	)	kIx)
iPhone	iPhon	k1gMnSc5
4	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1.0	1.0	k4
<g/>
.06	.06	k4
</s>
<s>
20110715	#num#	k4
<g/>
a	a	k8xC
<g/>
15	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2011	#num#	k4
</s>
<s>
4.2	4.2	k4
<g/>
.10	.10	k4
</s>
<s>
8E600	8E600	k4
(	(	kIx(
<g/>
Pouze	pouze	k6eAd1
CDMA	CDMA	kA
(	(	kIx(
<g/>
Verizon	Verizon	k1gNnSc1
<g/>
)	)	kIx)
iPhone	iPhon	k1gMnSc5
4	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1.0	1.0	k4
<g/>
.06	.06	k4
</s>
<s>
20110725	#num#	k4
<g/>
a	a	k8xC
<g/>
25	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2011	#num#	k4
</s>
<s>
iPhone	iPhon	k1gMnSc5
3	#num#	k4
<g/>
GS	GS	kA
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
4	#num#	k4
<g/>
,	,	kIx,
iPod	iPod	k1gMnSc1
Touch	Touch	k1gMnSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
4	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
iPad	iPad	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
iPad	iPad	k6eAd1
2	#num#	k4
</s>
<s>
4.3	4.3	k4
</s>
<s>
8F190	8F190	k4
8F191	8F191	k4
</s>
<s>
0	#num#	k4
<g/>
5.16	5.16	k4
<g/>
.01	.01	k4
0	#num#	k4
<g/>
4.10	4.10	k4
<g/>
.01	.01	k4
0	#num#	k4
<g/>
7.11	7.11	k4
<g/>
.00	.00	k4
2.00	2.00	k4
<g/>
.4	.4	k4
</s>
<s>
20110309	#num#	k4
<g/>
a	a	k8xC
<g/>
9	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2011	#num#	k4
</s>
<s>
4.3	4.3	k4
<g/>
.1	.1	k4
</s>
<s>
8G4	8G4	k4
</s>
<s>
0	#num#	k4
<g/>
5.16	5.16	k4
<g/>
.02	.02	k4
0	#num#	k4
<g/>
4.10	4.10	k4
<g/>
.01	.01	k4
0	#num#	k4
<g/>
7.11	7.11	k4
<g/>
.01	.01	k4
2.00	2.00	k4
<g/>
.4	.4	k4
</s>
<s>
20110325	#num#	k4
<g/>
a	a	k8xC
<g/>
25	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2011	#num#	k4
</s>
<s>
4.3	4.3	k4
<g/>
.2	.2	k4
</s>
<s>
8H7	8H7	k4
8H8	8H8	k4
</s>
<s>
0	#num#	k4
<g/>
5.16	5.16	k4
<g/>
.02	.02	k4
0	#num#	k4
<g/>
4.10	4.10	k4
<g/>
.01	.01	k4
0	#num#	k4
<g/>
7.11	7.11	k4
<g/>
.01	.01	k4
2.00	2.00	k4
<g/>
.4	.4	k4
</s>
<s>
20110414	#num#	k4
<g/>
a	a	k8xC
<g/>
14	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2011	#num#	k4
</s>
<s>
4.3	4.3	k4
<g/>
.3	.3	k4
</s>
<s>
8J2	8J2	k4
8J3	8J3	k4
</s>
<s>
0	#num#	k4
<g/>
5.16	5.16	k4
<g/>
.02	.02	k4
0	#num#	k4
<g/>
4.10	4.10	k4
<g/>
.01	.01	k4
0	#num#	k4
<g/>
4.10	4.10	k4
<g/>
.01	.01	k4
0	#num#	k4
<g/>
7.11	7.11	k4
<g/>
.01	.01	k4
2.00	2.00	k4
<g/>
.4	.4	k4
</s>
<s>
20110504	#num#	k4
<g/>
a	a	k8xC
<g/>
4	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2011	#num#	k4
</s>
<s>
4.3	4.3	k4
<g/>
.4	.4	k4
</s>
<s>
8K2	8K2	k4
</s>
<s>
0	#num#	k4
<g/>
5.16	5.16	k4
<g/>
.02	.02	k4
0	#num#	k4
<g/>
4.10	4.10	k4
<g/>
.01	.01	k4
0	#num#	k4
<g/>
7.11	7.11	k4
<g/>
.01	.01	k4
2.00	2.00	k4
<g/>
.4	.4	k4
</s>
<s>
20110715	#num#	k4
<g/>
a	a	k8xC
<g/>
15	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2011	#num#	k4
</s>
<s>
4.3	4.3	k4
<g/>
.5	.5	k4
</s>
<s>
8L1	8L1	k4
</s>
<s>
0	#num#	k4
<g/>
5.16	5.16	k4
<g/>
.02	.02	k4
0	#num#	k4
<g/>
4.10	4.10	k4
<g/>
.01	.01	k4
0	#num#	k4
<g/>
7.11	7.11	k4
<g/>
.01	.01	k4
2.00	2.00	k4
<g/>
.4	.4	k4
</s>
<s>
20110725	#num#	k4
<g/>
a	a	k8xC
<g/>
25	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2011	#num#	k4
</s>
<s>
Verze	verze	k1gFnSc1
</s>
<s>
Build	Build	k6eAd1
</s>
<s>
Baseband	Baseband	k1gInSc1
</s>
<s>
Vydáno	vydán	k2eAgNnSc1d1
</s>
<s>
iOS	iOS	k?
5	#num#	k4
<g/>
.	.	kIx.
<g/>
x	x	k?
</s>
<s>
iOS	iOS	k?
5	#num#	k4
byl	být	k5eAaImAgInS
představen	představit	k5eAaPmNgInS
veřejnosti	veřejnost	k1gFnSc2
6	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2011	#num#	k4
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
finálně	finálně	k6eAd1
vyšel	vyjít	k5eAaPmAgInS
pro	pro	k7c4
iPhone	iPhon	k1gInSc5
3	#num#	k4
<g/>
GS	GS	kA
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
4	#num#	k4
(	(	kIx(
<g/>
GSM	GSM	kA
a	a	k8xC
CDMA	CDMA	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
4	#num#	k4
<g/>
S	s	k7c7
<g/>
,	,	kIx,
iPod	iPod	k1gMnSc1
Touch	Touch	k1gMnSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
&	&	k?
4	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
iPad	iPad	k1gInSc1
<g/>
,	,	kIx,
a	a	k8xC
iPad	iPad	k6eAd1
2	#num#	k4
12	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2011	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
iOS	iOS	k?
5.1	5.1	k4
<g/>
.1	.1	k4
je	být	k5eAaImIp3nS
poslední	poslední	k2eAgFnSc1d1
verze	verze	k1gFnSc1
iOS	iOS	k?
podporujcí	podporujcí	k1gMnSc1
iPad	iPad	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
iPod	iPod	k1gMnSc1
Touch	Touch	k1gMnSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Tabulka	tabulka	k1gFnSc1
verzí	verze	k1gFnPc2
<g/>
:	:	kIx,
iOS	iOS	k?
5	#num#	k4
<g/>
.	.	kIx.
<g/>
x	x	k?
–	–	k?
Přenosná	přenosný	k2eAgFnSc1d1
zařízení	zařízení	k1gNnPc1
</s>
<s>
Verze	verze	k1gFnSc1
</s>
<s>
Build	Build	k6eAd1
</s>
<s>
Baseband	Baseband	k1gInSc1
</s>
<s>
Vydáno	vydán	k2eAgNnSc1d1
</s>
<s>
iPhone	iPhon	k1gMnSc5
3	#num#	k4
<g/>
GS	GS	kA
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
4	#num#	k4
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
4	#num#	k4
<g/>
S	s	k7c7
<g/>
,	,	kIx,
iPod	iPod	k1gMnSc1
Touch	Touch	k1gMnSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
4	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
iPad	iPad	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
iPad	iPad	k6eAd1
2	#num#	k4
</s>
<s>
5.0	5.0	k4
</s>
<s>
9A334	9A334	k4
</s>
<s>
0	#num#	k4
<g/>
4.11	4.11	k4
<g/>
.08	.08	k4
(	(	kIx(
<g/>
GSM	GSM	kA
<g/>
)	)	kIx)
0	#num#	k4
<g/>
5.16	5.16	k4
<g/>
.05	.05	k4
0	#num#	k4
<g/>
7.11	7.11	k4
<g/>
.01	.01	k4
1.0	1.0	k4
<g/>
.11	.11	k4
(	(	kIx(
<g/>
GSM	GSM	kA
<g/>
)	)	kIx)
3.00	3.00	k4
<g/>
.03	.03	k4
(	(	kIx(
<g/>
CDMA	CDMA	kA
<g/>
)	)	kIx)
</s>
<s>
20111012	#num#	k4
<g/>
a	a	k8xC
<g/>
12	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2011	#num#	k4
</s>
<s>
5.0	5.0	k4
<g/>
.1	.1	k4
</s>
<s>
9A405	9A405	k4
9A406	9A406	k4
(	(	kIx(
<g/>
Dodatečný	dodatečný	k2eAgInSc1d1
build	build	k1gInSc1
pro	pro	k7c4
iPhone	iPhon	k1gInSc5
4	#num#	k4
<g/>
S	s	k7c7
<g/>
)	)	kIx)
</s>
<s>
0	#num#	k4
<g/>
4.11	4.11	k4
<g/>
.08	.08	k4
(	(	kIx(
<g/>
GSM	GSM	kA
<g/>
)	)	kIx)
0	#num#	k4
<g/>
5.16	5.16	k4
<g/>
.05	.05	k4
0	#num#	k4
<g/>
7.11	7.11	k4
<g/>
.01	.01	k4
1.0	1.0	k4
<g/>
.13	.13	k4
<g/>
(	(	kIx(
<g/>
9	#num#	k4
<g/>
A	a	k9
<g/>
405	#num#	k4
<g/>
)	)	kIx)
1.0	1.0	k4
<g/>
.14	.14	k4
<g/>
(	(	kIx(
<g/>
9	#num#	k4
<g/>
A	a	k9
<g/>
406	#num#	k4
<g/>
)	)	kIx)
3.00	3.00	k4
<g/>
.03	.03	k4
(	(	kIx(
<g/>
CDMA	CDMA	kA
<g/>
)	)	kIx)
</s>
<s>
20111110	#num#	k4
<g/>
a	a	k8xC
<g/>
10	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2011	#num#	k4
(	(	kIx(
<g/>
9	#num#	k4
<g/>
A	a	k9
<g/>
405	#num#	k4
<g/>
)	)	kIx)
20111212	#num#	k4
<g/>
a	a	k8xC
<g/>
12	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2011	#num#	k4
(	(	kIx(
<g/>
9	#num#	k4
<g/>
A	a	k9
<g/>
406	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
iPhone	iPhon	k1gMnSc5
3	#num#	k4
<g/>
GS	GS	kA
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
4	#num#	k4
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
4	#num#	k4
<g/>
S	s	k7c7
<g/>
,	,	kIx,
iPod	iPod	k1gMnSc1
Touch	Touch	k1gMnSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
4	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
iPad	iPad	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
iPad	iPad	k1gInSc1
2	#num#	k4
a	a	k8xC
iPad	iPad	k6eAd1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
5.1	5.1	k4
</s>
<s>
9B176	9B176	k4
9B179	9B179	k4
</s>
<s>
0	#num#	k4
<g/>
5.16	5.16	k4
<g/>
.05	.05	k4
0	#num#	k4
<g/>
7.11	7.11	k4
<g/>
.01	.01	k4
0	#num#	k4
<g/>
4.12	4.12	k4
<g/>
.01	.01	k4
3.0	3.0	k4
<g/>
.04	.04	k4
1.0	1.0	k4
<g/>
.10	.10	k4
2.0	2.0	k4
<g/>
.10	.10	k4
</s>
<s>
20120307	#num#	k4
<g/>
a	a	k8xC
<g/>
7	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2012	#num#	k4
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
5.1	5.1	k4
<g/>
.1	.1	k4
</s>
<s>
9B206	9B206	k4
9B208	9B208	k4
(	(	kIx(
<g/>
Dodatečný	dodatečný	k2eAgInSc1d1
build	build	k1gInSc1
pro	pro	k7c4
iPhone	iPhon	k1gInSc5
4	#num#	k4
GSM	GSM	kA
<g/>
)	)	kIx)
</s>
<s>
0	#num#	k4
<g/>
5.16	5.16	k4
<g/>
.05	.05	k4
0	#num#	k4
<g/>
7.11	7.11	k4
<g/>
.01	.01	k4
0	#num#	k4
<g/>
4.12	4.12	k4
<g/>
.01	.01	k4
3.0	3.0	k4
<g/>
.04	.04	k4
1.0	1.0	k4
<g/>
.11	.11	k4
2.0	2.0	k4
<g/>
.12	.12	k4
</s>
<s>
20120507	#num#	k4
<g/>
a	a	k8xC
<g/>
7	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2012	#num#	k4
20120525	#num#	k4
<g/>
a	a	k8xC
<g/>
25	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2012	#num#	k4
</s>
<s>
Verze	verze	k1gFnSc1
</s>
<s>
Build	Build	k6eAd1
</s>
<s>
Baseband	Baseband	k1gInSc1
</s>
<s>
Vydáno	vydán	k2eAgNnSc1d1
</s>
<s>
iOS	iOS	k?
6	#num#	k4
<g/>
.	.	kIx.
<g/>
x	x	k?
</s>
<s>
iOS	iOS	k?
6	#num#	k4
byl	být	k5eAaImAgInS
představen	představit	k5eAaPmNgInS
11	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2012	#num#	k4
při	při	k7c6
WWDC	WWDC	kA
2012	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
byl	být	k5eAaImAgInS
vydán	vydat	k5eAaPmNgInS
na	na	k7c4
podzim	podzim	k1gInSc4
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
vzoru	vzor	k1gInSc6
předchozích	předchozí	k2eAgFnPc2d1
verzí	verze	k1gFnPc2
iOS	iOS	k?
<g/>
,	,	kIx,
přestaly	přestat	k5eAaPmAgInP
být	být	k5eAaImF
některé	některý	k3yIgInPc1
starší	starý	k2eAgInPc1d2
přístroje	přístroj	k1gInPc1
podporovány	podporovat	k5eAaImNgInP
<g/>
,	,	kIx,
konkrétně	konkrétně	k6eAd1
iPod	iPod	k1gMnSc1
Touch	Touch	k1gMnSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
iPad	iPad	k6eAd1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podporované	podporovaný	k2eAgFnPc4d1
zařízení	zařízení	k1gNnPc1
byly	být	k5eAaImAgFnP
iPhone	iPhon	k1gInSc5
3GS	3GS	k4
a	a	k8xC
pozdější	pozdní	k2eAgMnSc1d2
<g/>
;	;	kIx,
iPod	iPod	k1gMnSc1
Touch	Touch	k1gMnSc1
(	(	kIx(
<g/>
4	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
pozdější	pozdní	k2eAgMnSc1d2
<g/>
;	;	kIx,
a	a	k8xC
iPad	iPad	k1gInSc1
2	#num#	k4
a	a	k8xC
pozdější	pozdní	k2eAgMnSc1d2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2012	#num#	k4
byly	být	k5eAaImAgInP
v	v	k7c6
sanfranciském	sanfranciský	k2eAgInSc6d1
Yerba	Yerb	k1gMnSc2
Buena	Buen	k1gInSc2
Center	centrum	k1gNnPc2
pro	pro	k7c4
umělce	umělec	k1gMnPc4
<g/>
,	,	kIx,
mimo	mimo	k7c4
jiné	jiný	k2eAgFnPc4d1
odhaleny	odhalen	k2eAgFnPc4d1
tři	tři	k4xCgFnPc4
věci	věc	k1gFnPc4
související	související	k2eAgFnSc4d1
s	s	k7c7
iOS	iOS	k?
<g/>
:	:	kIx,
další	další	k2eAgFnPc1d1
generace	generace	k1gFnPc1
iPhone	iPhon	k1gInSc5
5	#num#	k4
<g/>
,	,	kIx,
nový	nový	k2eAgMnSc1d1
předělaný	předělaný	k2eAgMnSc1d1
iPod	iPod	k1gMnSc1
Touch	Touch	k1gMnSc1
(	(	kIx(
<g/>
5	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
zpráva	zpráva	k1gFnSc1
o	o	k7c4
vydání	vydání	k1gNnSc4
iOS	iOS	k?
6	#num#	k4
příští	příští	k2eAgInSc4d1
týden	týden	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
iOS	iOS	k?
6	#num#	k4
by	by	kYmCp3nS
pro	pro	k7c4
veřejnost	veřejnost	k1gFnSc4
vydán	vydán	k2eAgInSc1d1
dne	den	k1gInSc2
19	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2012	#num#	k4
<g/>
,	,	kIx,
přes	přes	k7c4
iTunes	iTunes	k1gInSc4
a	a	k8xC
přes	přes	k7c4
OTA	Oto	k1gMnSc4
(	(	kIx(
<g/>
over-the-air	over-the-air	k1gMnSc1
<g/>
)	)	kIx)
aktualizace	aktualizace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
iOS	iOS	k?
6.1	6.1	k4
<g/>
.6	.6	k4
je	být	k5eAaImIp3nS
poslední	poslední	k2eAgFnSc1d1
verze	verze	k1gFnSc1
iOS	iOS	k?
podporující	podporující	k2eAgFnPc4d1
iPhone	iPhon	k1gInSc5
3GS	3GS	k4
a	a	k8xC
iPod	iPod	k1gMnSc1
Touch	Touch	k1gMnSc1
(	(	kIx(
<g/>
4	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Tabulka	tabulka	k1gFnSc1
verzí	verze	k1gFnPc2
<g/>
:	:	kIx,
iOS	iOS	k?
6	#num#	k4
<g/>
.	.	kIx.
<g/>
x	x	k?
–	–	k?
Přenosná	přenosný	k2eAgFnSc1d1
zařízení	zařízení	k1gNnPc1
</s>
<s>
Verze	verze	k1gFnSc1
</s>
<s>
Build	Build	k6eAd1
</s>
<s>
Baseband	Baseband	k1gInSc1
</s>
<s>
Vydáno	vydán	k2eAgNnSc1d1
</s>
<s>
iPhone	iPhon	k1gMnSc5
3	#num#	k4
<g/>
GS	GS	kA
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
4	#num#	k4
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
4	#num#	k4
<g/>
S	s	k7c7
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
5	#num#	k4
<g/>
,	,	kIx,
iPod	iPod	k1gMnSc1
Touch	Touch	k1gMnSc1
(	(	kIx(
<g/>
4	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
iPod	iPod	k1gMnSc1
Touch	Touch	k1gMnSc1
(	(	kIx(
<g/>
5	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
iPad	iPad	k1gInSc1
2	#num#	k4
<g/>
,	,	kIx,
iPad	iPad	k1gInSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
iPad	iPad	k1gMnSc1
(	(	kIx(
<g/>
4	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
iPad	iPad	k6eAd1
mini	mini	k2eAgFnSc1d1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
6.0	6.0	k4
</s>
<s>
10A403	10A403	k4
10A405	10A405	k4
10A406	10A406	k4
</s>
<s>
0	#num#	k4
<g/>
5.16	5.16	k4
<g/>
.07	.07	k4
1.00	1.00	k4
<g/>
.16	.16	k4
3.0	3.0	k4
<g/>
.04	.04	k4
2.0	2.0	k4
<g/>
.02	.02	k4
0	#num#	k4
<g/>
4.12	4.12	k4
<g/>
.02	.02	k4
3.0	3.0	k4
<g/>
.04	.04	k4
0	#num#	k4
<g/>
4.12	4.12	k4
<g/>
.02	.02	k4
3.0	3.0	k4
<g/>
.04	.04	k4
</s>
<s>
20120919	#num#	k4
<g/>
a	a	k8xC
<g/>
19	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2012	#num#	k4
</s>
<s>
6.0	6.0	k4
<g/>
.1	.1	k4
</s>
<s>
10A523	10A523	k4
10A525	10A525	k4
</s>
<s>
0	#num#	k4
<g/>
5.16	5.16	k4
<g/>
.07	.07	k4
1.01	1.01	k4
<g/>
.00	.00	k4
3.0	3.0	k4
<g/>
.04	.04	k4
2.0	2.0	k4
<g/>
.02	.02	k4
0	#num#	k4
<g/>
4.12	4.12	k4
<g/>
.02	.02	k4
3.0	3.0	k4
<g/>
.04	.04	k4
0	#num#	k4
<g/>
4.12	4.12	k4
<g/>
.02	.02	k4
3.0	3.0	k4
<g/>
.04	.04	k4
</s>
<s>
20121101	#num#	k4
<g/>
a	a	k8xC
<g/>
1	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2012	#num#	k4
</s>
<s>
6.0	6.0	k4
<g/>
.2	.2	k4
</s>
<s>
10A551	10A551	k4
</s>
<s>
1.01	1.01	k4
<g/>
.00	.00	k4
</s>
<s>
20121218	#num#	k4
<g/>
a	a	k8xC
<g/>
18	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2012	#num#	k4
</s>
<s>
6.1	6.1	k4
</s>
<s>
10B141	10B141	k4
10B142	10B142	k4
10B143	10B143	k4
10B144	10B144	k4
</s>
<s>
0	#num#	k4
<g/>
5.16	5.16	k4
<g/>
.08	.08	k4
3.04	3.04	k4
<g/>
.25	.25	k4
3.4	3.4	k4
<g/>
.01	.01	k4
2.0	2.0	k4
<g/>
.02	.02	k4
2.3	2.3	k4
<g/>
.03	.03	k4
3.0	3.0	k4
<g/>
.04	.04	k4
0	#num#	k4
<g/>
4.12	4.12	k4
<g/>
.05	.05	k4
3.0	3.0	k4
<g/>
.04	.04	k4
</s>
<s>
20130128	#num#	k4
<g/>
a	a	k8xC
<g/>
28	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2013	#num#	k4
</s>
<s>
6.1	6.1	k4
<g/>
.1	.1	k4
</s>
<s>
10	#num#	k4
<g/>
B	B	kA
<g/>
145	#num#	k4
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
3.4	3.4	k4
<g/>
.02	.02	k4
</s>
<s>
20130206	#num#	k4
<g/>
a	a	k8xC
<g/>
6	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2013	#num#	k4
</s>
<s>
6.1	6.1	k4
<g/>
.2	.2	k4
</s>
<s>
10B146	10B146	k4
10B147	10B147	k4
</s>
<s>
0	#num#	k4
<g/>
5.16	5.16	k4
<g/>
.08	.08	k4
3.4	3.4	k4
<g/>
.02	.02	k4
0	#num#	k4
<g/>
4.12	4.12	k4
<g/>
.05	.05	k4
3.04	3.04	k4
<g/>
.25	.25	k4
2.3	2.3	k4
<g/>
.03	.03	k4
</s>
<s>
20130219	#num#	k4
<g/>
a	a	k8xC
<g/>
19	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2013	#num#	k4
</s>
<s>
6.1	6.1	k4
<g/>
.3	.3	k4
</s>
<s>
10B329	10B329	k4
</s>
<s>
0	#num#	k4
<g/>
5.16	5.16	k4
<g/>
.08	.08	k4
3.4	3.4	k4
<g/>
.03	.03	k4
0	#num#	k4
<g/>
4.12	4.12	k4
<g/>
.05	.05	k4
3.04	3.04	k4
<g/>
.25	.25	k4
2.3	2.3	k4
<g/>
.04	.04	k4
</s>
<s>
20130319	#num#	k4
<g/>
a	a	k8xC
<g/>
19	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2013	#num#	k4
</s>
<s>
6.1	6.1	k4
<g/>
.4	.4	k4
</s>
<s>
10B350	10B350	k4
</s>
<s>
3.04	3.04	k4
<g/>
.25	.25	k4
</s>
<s>
20130502	#num#	k4
<g/>
a	a	k8xC
<g/>
2	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2013	#num#	k4
</s>
<s>
6.1	6.1	k4
<g/>
.5	.5	k4
</s>
<s>
10B400	10B400	k4
</s>
<s>
20131114	#num#	k4
<g/>
a	a	k8xC
<g/>
14	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2013	#num#	k4
</s>
<s>
6.1	6.1	k4
<g/>
.6	.6	k4
</s>
<s>
10B500	10B500	k4
</s>
<s>
20140221	#num#	k4
<g/>
a	a	k8xC
<g/>
21	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2014	#num#	k4
</s>
<s>
Verze	verze	k1gFnSc1
</s>
<s>
Build	Build	k6eAd1
</s>
<s>
Baseband	Baseband	k1gInSc1
</s>
<s>
Vydáno	vydán	k2eAgNnSc1d1
</s>
<s>
iOS	iOS	k?
7	#num#	k4
<g/>
.	.	kIx.
<g/>
x	x	k?
</s>
<s>
Logo	logo	k1gNnSc1
iOS	iOS	k?
7	#num#	k4
</s>
<s>
Apple	Apple	kA
představil	představit	k5eAaPmAgInS
iOS	iOS	k?
7	#num#	k4
10	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2013	#num#	k4
na	na	k7c6
své	svůj	k3xOyFgFnSc6
výroční	výroční	k2eAgFnSc6d1
konferenci	konference	k1gFnSc6
Apple	Apple	kA
WWDC	WWDC	kA
2013	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
vydání	vydání	k1gNnSc1
pro	pro	k7c4
veřejnost	veřejnost	k1gFnSc4
oznámil	oznámit	k5eAaPmAgMnS
na	na	k7c4
podzim	podzim	k1gInSc4
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Finální	finální	k2eAgFnSc1d1
verze	verze	k1gFnSc1
pro	pro	k7c4
veřejnost	veřejnost	k1gFnSc4
byla	být	k5eAaImAgFnS
vydána	vydán	k2eAgFnSc1d1
společně	společně	k6eAd1
s	s	k7c7
novým	nový	k2eAgNnSc7d1
iPhone	iPhon	k1gMnSc5
5S	5S	k4
a	a	k8xC
iPhone	iPhon	k1gInSc5
5C	5C	k4
10	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
verze	verze	k1gFnSc1
iOS	iOS	k?
není	být	k5eNaImIp3nS
podporována	podporovat	k5eAaImNgFnS
iPhonem	iPhon	k1gInSc7
3GS	3GS	k4
a	a	k8xC
iPodem	iPod	k1gMnSc7
Touch	Touch	k1gMnSc1
(	(	kIx(
<g/>
4	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
podporované	podporovaný	k2eAgNnSc4d1
zařízení	zařízení	k1gNnSc4
patří	patřit	k5eAaImIp3nS
iPhone	iPhon	k1gInSc5
4	#num#	k4
a	a	k8xC
novější	nový	k2eAgFnSc2d2
<g/>
,	,	kIx,
iPod	iPod	k1gMnSc1
Touch	Touch	k1gMnSc1
(	(	kIx(
<g/>
5	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
iPad	iPad	k1gInSc1
2	#num#	k4
a	a	k8xC
novější	nový	k2eAgInSc4d2
a	a	k8xC
iPad	iPad	k6eAd1
mini	mini	k2eAgFnSc1d1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
novější	nový	k2eAgInSc4d2
<g/>
.	.	kIx.
</s>
<s>
iOS	iOS	k?
7.1	7.1	k4
<g/>
.2	.2	k4
je	být	k5eAaImIp3nS
poslední	poslední	k2eAgFnSc7d1
verzí	verze	k1gFnSc7
podporující	podporující	k2eAgFnSc4d1
iPhone	iPhon	k1gInSc5
4	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tabulka	tabulka	k1gFnSc1
verzí	verze	k1gFnPc2
<g/>
:	:	kIx,
iOS	iOS	k?
7	#num#	k4
<g/>
.	.	kIx.
<g/>
x	x	k?
–	–	k?
Přenosná	přenosný	k2eAgFnSc1d1
zařízení	zařízení	k1gNnPc1
</s>
<s>
Verze	verze	k1gFnSc1
</s>
<s>
Build	Build	k6eAd1
</s>
<s>
Baseband	Baseband	k1gInSc1
</s>
<s>
Vydáno	vydán	k2eAgNnSc1d1
</s>
<s>
iPhone	iPhon	k1gMnSc5
4	#num#	k4
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
4	#num#	k4
<g/>
S	s	k7c7
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
5	#num#	k4
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
5	#num#	k4
<g/>
C	C	kA
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
5	#num#	k4
<g/>
S	s	k7c7
<g/>
,	,	kIx,
iPod	iPod	k1gMnSc1
Touch	Touch	k1gMnSc1
(	(	kIx(
<g/>
5	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
iPad	iPad	k1gInSc1
2	#num#	k4
<g/>
,	,	kIx,
iPad	iPad	k1gInSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
iPad	iPad	k1gMnSc1
(	(	kIx(
<g/>
4	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
iPad	iPad	k6eAd1
mini	mini	k2eAgFnSc1d1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
7.0	7.0	k4
</s>
<s>
11A465	11A465	k4
</s>
<s>
11A466	11A466	k4
(	(	kIx(
<g/>
iPhone	iPhon	k1gInSc5
5S	5S	k4
a	a	k8xC
5C	5C	k4
shipped	shipped	k1gMnSc1
with	with	k1gMnSc1
this	this	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
5.0.005.00	5.0.005.00	k4
<g/>
.011	.011	k4
<g/>
.00	.00	k4
<g/>
.06	.06	k4
</s>
<s>
20130918	#num#	k4
<g/>
a	a	k8xC
<g/>
18	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2013	#num#	k4
</s>
<s>
7.0	7.0	k4
<g/>
.1	.1	k4
</s>
<s>
11	#num#	k4
<g/>
A	a	k8xC
<g/>
470	#num#	k4
<g/>
a	a	k8xC
(	(	kIx(
<g/>
pouze	pouze	k6eAd1
iPhone	iPhon	k1gInSc5
5S	5S	k4
a	a	k8xC
5	#num#	k4
<g/>
C	C	kA
<g/>
)	)	kIx)
</s>
<s>
1.00	1.00	k4
<g/>
.06	.06	k4
</s>
<s>
20130919	#num#	k4
<g/>
a	a	k8xC
<g/>
19	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2013	#num#	k4
</s>
<s>
7.0	7.0	k4
<g/>
.2	.2	k4
</s>
<s>
11A501	11A501	k4
</s>
<s>
5.0.005.00	5.0.005.00	k4
<g/>
.011	.011	k4
<g/>
.00	.00	k4
<g/>
.06	.06	k4
</s>
<s>
20130926	#num#	k4
<g/>
a	a	k8xC
<g/>
26	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2013	#num#	k4
</s>
<s>
iPhone	iPhon	k1gMnSc5
4	#num#	k4
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
4	#num#	k4
<g/>
S	s	k7c7
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
5	#num#	k4
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
5	#num#	k4
<g/>
S	s	k7c7
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
5	#num#	k4
<g/>
C	C	kA
<g/>
,	,	kIx,
iPod	iPod	k1gMnSc1
Touch	Touch	k1gMnSc1
(	(	kIx(
<g/>
5	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
iPad	iPad	k1gInSc1
2	#num#	k4
<g/>
,	,	kIx,
iPad	iPad	k1gInSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
iPad	iPad	k1gMnSc1
(	(	kIx(
<g/>
4	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
iPad	iPad	k1gMnSc1
Air	Air	k1gMnSc1
<g/>
,	,	kIx,
iPad	iPad	k1gMnSc1
mini	mini	k2eAgMnSc1d1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
iPad	iPad	k1gInSc1
mini	mini	k2eAgInSc1d1
2	#num#	k4
</s>
<s>
7.0	7.0	k4
<g/>
.3	.3	k4
</s>
<s>
11B511	11B511	k4
</s>
<s>
5.0.025.02	5.0.025.02	k4
<g/>
.001	.001	k4
<g/>
.02	.02	k4
<g/>
.02	.02	k4
</s>
<s>
20131022	#num#	k4
<g/>
a	a	k8xC
<g/>
22	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2013	#num#	k4
</s>
<s>
7.0	7.0	k4
<g/>
.4	.4	k4
</s>
<s>
11	#num#	k4
<g/>
B	B	kA
<g/>
554	#num#	k4
<g/>
a	a	k8xC
</s>
<s>
5.0.025.02	5.0.025.02	k4
<g/>
.001	.001	k4
<g/>
.03	.03	k4
<g/>
.01	.01	k4
</s>
<s>
20131114	#num#	k4
<g/>
a	a	k8xC
<g/>
14	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2013	#num#	k4
</s>
<s>
7.0	7.0	k4
<g/>
.5	.5	k4
</s>
<s>
11B601	11B601	k4
(	(	kIx(
<g/>
Pouze	pouze	k6eAd1
iPhone	iPhon	k1gInSc5
5	#num#	k4
<g/>
S	s	k7c7
<g/>
)	)	kIx)
</s>
<s>
1.03	1.03	k4
<g/>
.02	.02	k4
</s>
<s>
20140129	#num#	k4
<g/>
a	a	k8xC
<g/>
29	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2014	#num#	k4
</s>
<s>
7.0	7.0	k4
<g/>
.6	.6	k4
</s>
<s>
11B651	11B651	k4
</s>
<s>
5.0.025.02	5.0.025.02	k4
<g/>
.001	.001	k4
<g/>
.03	.03	k4
<g/>
.02	.02	k4
</s>
<s>
20140221	#num#	k4
<g/>
a	a	k8xC
<g/>
21	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2014	#num#	k4
</s>
<s>
7.1	7.1	k4
</s>
<s>
11D167	11D167	k4
/	/	kIx~
11D169	11D169	k4
/	/	kIx~
11	#num#	k4
<g/>
D	D	kA
<g/>
169	#num#	k4
<g/>
b	b	k?
</s>
<s>
5.2.006.02	5.2.006.02	k4
<g/>
.002	.002	k4
<g/>
.18	.18	k4
<g/>
.02	.02	k4
</s>
<s>
20140310	#num#	k4
<g/>
a	a	k8xC
<g/>
10	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2014	#num#	k4
</s>
<s>
7.1	7.1	k4
<g/>
.1	.1	k4
</s>
<s>
11D201	11D201	k4
</s>
<s>
5.2.006.02	5.2.006.02	k4
<g/>
.002	.002	k4
<g/>
.18	.18	k4
<g/>
.02	.02	k4
</s>
<s>
20140422	#num#	k4
<g/>
a	a	k8xC
<g/>
22	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2014	#num#	k4
</s>
<s>
7.1	7.1	k4
<g/>
.2	.2	k4
</s>
<s>
11D257	11D257	k4
</s>
<s>
5.2.006.02	5.2.006.02	k4
<g/>
.002	.002	k4
<g/>
.18	.18	k4
<g/>
.02	.02	k4
</s>
<s>
20140630	#num#	k4
<g/>
a	a	k8xC
<g/>
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2014	#num#	k4
</s>
<s>
Verze	verze	k1gFnSc1
</s>
<s>
Build	Build	k6eAd1
</s>
<s>
Baseband	Baseband	k1gInSc1
</s>
<s>
Vydáno	vydán	k2eAgNnSc1d1
</s>
<s>
iOS	iOS	k?
8	#num#	k4
<g/>
.	.	kIx.
<g/>
x	x	k?
</s>
<s>
Logo	logo	k1gNnSc1
iOS	iOS	k?
8	#num#	k4
</s>
<s>
Apple	Apple	kA
představil	představit	k5eAaPmAgInS
iOS	iOS	k?
8	#num#	k4
2	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2014	#num#	k4
na	na	k7c6
své	svůj	k3xOyFgFnSc6
výroční	výroční	k2eAgFnSc6d1
konferenci	konference	k1gFnSc6
Apple	Apple	kA
WWDC	WWDC	kA
2014	#num#	k4
a	a	k8xC
finální	finální	k2eAgNnSc4d1
vydání	vydání	k1gNnPc4
oznámil	oznámit	k5eAaPmAgInS
na	na	k7c4
podzim	podzim	k1gInSc4
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Finální	finální	k2eAgFnSc1d1
verze	verze	k1gFnSc1
pro	pro	k7c4
veřejnost	veřejnost	k1gFnSc4
byla	být	k5eAaImAgFnS
vydána	vydán	k2eAgFnSc1d1
společně	společně	k6eAd1
s	s	k7c7
novým	nový	k2eAgNnSc7d1
iPhone	iPhon	k1gMnSc5
6	#num#	k4
a	a	k8xC
iPhone	iPhon	k1gInSc5
6	#num#	k4
Plus	plus	k1gInSc1
17	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Verze	verze	k1gFnSc1
iOS	iOS	k?
8.1	8.1	k4
byla	být	k5eAaImAgFnS
oficiálně	oficiálně	k6eAd1
vydána	vydán	k2eAgFnSc1d1
20	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
s	s	k7c7
touto	tento	k3xDgFnSc7
verzí	verze	k1gFnSc7
Apple	Apple	kA
pokračuje	pokračovat	k5eAaImIp3nS
v	v	k7c6
cyklu	cyklus	k1gInSc6
ukončování	ukončování	k1gNnSc2
podpory	podpora	k1gFnSc2
nejstarších	starý	k2eAgFnPc2d3
zařízení	zařízení	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
tentokrát	tentokrát	k6eAd1
byla	být	k5eAaImAgFnS
ukončena	ukončen	k2eAgFnSc1d1
podpora	podpora	k1gFnSc1
jenom	jenom	k9
jednomu	jeden	k4xCgNnSc3
zařízení	zařízení	k1gNnPc2
a	a	k8xC
to	ten	k3xDgNnSc4
iPhonu	iPhona	k1gFnSc4
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podporovaná	podporovaný	k2eAgNnPc1d1
zařízení	zařízení	k1gNnPc1
jsou	být	k5eAaImIp3nP
iPhone	iPhon	k1gInSc5
4S	4S	k4
a	a	k8xC
novější	nový	k2eAgFnSc2d2
<g/>
,	,	kIx,
iPod	iPod	k1gMnSc1
Touch	Touch	k1gMnSc1
(	(	kIx(
<g/>
5	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
iPad	iPad	k1gInSc1
2	#num#	k4
a	a	k8xC
novější	nový	k2eAgInSc4d2
a	a	k8xC
iPad	iPad	k6eAd1
mini	mini	k2eAgFnSc1d1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
novější	nový	k2eAgInSc4d2
<g/>
.	.	kIx.
iPad	iPad	k1gInSc4
2	#num#	k4
je	být	k5eAaImIp3nS
nyní	nyní	k6eAd1
nejdéle	dlouho	k6eAd3
podporované	podporovaný	k2eAgNnSc1d1
zařízení	zařízení	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
podporuje	podporovat	k5eAaImIp3nS
5	#num#	k4
verzí	verze	k1gFnPc2
iOS	iOS	k?
a	a	k8xC
to	ten	k3xDgNnSc1
iOS	iOS	k?
4	#num#	k4
<g/>
,	,	kIx,
iOS	iOS	k?
5	#num#	k4
<g/>
,	,	kIx,
iOS	iOS	k?
6	#num#	k4
<g/>
,	,	kIx,
iOS	iOS	k?
7	#num#	k4
a	a	k8xC
nový	nový	k2eAgInSc4d1
iOS	iOS	k?
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Tabulka	tabulka	k1gFnSc1
verzí	verze	k1gFnPc2
<g/>
:	:	kIx,
iOS	iOS	k?
8	#num#	k4
<g/>
.	.	kIx.
<g/>
x	x	k?
–	–	k?
Přenosná	přenosný	k2eAgFnSc1d1
zařízení	zařízení	k1gNnPc1
</s>
<s>
Verze	verze	k1gFnSc1
</s>
<s>
Build	Build	k6eAd1
</s>
<s>
Baseband	Baseband	k1gInSc1
</s>
<s>
Vydáno	vydán	k2eAgNnSc1d1
</s>
<s>
iPhone	iPhon	k1gMnSc5
4	#num#	k4
<g/>
S	s	k7c7
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
5	#num#	k4
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
5	#num#	k4
<g/>
C	C	kA
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
5	#num#	k4
<g/>
S	s	k7c7
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
6	#num#	k4
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
6	#num#	k4
Plus	plus	k1gInSc1
<g/>
,	,	kIx,
iPod	iPod	k1gMnSc1
Touch	Touch	k1gMnSc1
(	(	kIx(
<g/>
5	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
iPad	iPad	k1gInSc1
2	#num#	k4
<g/>
,	,	kIx,
iPad	iPad	k1gInSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
iPad	iPad	k1gMnSc1
(	(	kIx(
<g/>
4	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
iPad	iPad	k1gMnSc1
Air	Air	k1gMnSc1
<g/>
,	,	kIx,
iPad	iPad	k1gMnSc1
Air	Air	k1gFnSc2
2	#num#	k4
<g/>
,	,	kIx,
iPad	iPad	k6eAd1
mini	mini	k2eAgFnSc1d1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
iPad	iPad	k6eAd1
mini	mini	k2eAgFnSc1d1
2	#num#	k4
<g/>
,	,	kIx,
iPad	iPad	k6eAd1
mini	mini	k2eAgFnPc2d1
3	#num#	k4
</s>
<s>
8.0	8.0	k4
</s>
<s>
12A365	12A365	k4
12	#num#	k4
<g/>
A	a	k9
<g/>
365	#num#	k4
<g/>
b	b	k?
12A366	12A366	k4
</s>
<s>
0	#num#	k4
<g/>
4.12	4.12	k4
<g/>
.09	.09	k4
3.0	3.0	k4
<g/>
.04	.04	k4
5.4	5.4	k4
<g/>
.00	.00	k4
5.3	5.3	k4
<g/>
.00	.00	k4
7.03	7.03	k4
<g/>
.00	.00	k4
3.09	3.09	k4
<g/>
.01	.01	k4
1.00	1.00	k4
<g/>
.05	.05	k4
</s>
<s>
20140917	#num#	k4
<g/>
a	a	k8xC
<g/>
17	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2014	#num#	k4
</s>
<s>
8.0	8.0	k4
<g/>
.1	.1	k4
</s>
<s>
12A402	12A402	k4
</s>
<s>
0	#num#	k4
<g/>
4.12	4.12	k4
<g/>
.09	.09	k4
3.0	3.0	k4
<g/>
.04	.04	k4
5.4	5.4	k4
<g/>
.00	.00	k4
5.3	5.3	k4
<g/>
.00	.00	k4
7.03	7.03	k4
<g/>
.00	.00	k4
3.09	3.09	k4
<g/>
.01	.01	k4
1.00	1.00	k4
<g/>
.05	.05	k4
</s>
<s>
20140924	#num#	k4
<g/>
a	a	k8xC
<g/>
24	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2014	#num#	k4
</s>
<s>
8.0	8.0	k4
<g/>
.2	.2	k4
</s>
<s>
12A405	12A405	k4
</s>
<s>
0	#num#	k4
<g/>
4.12	4.12	k4
<g/>
.09	.09	k4
3.0	3.0	k4
<g/>
.04	.04	k4
5.4	5.4	k4
<g/>
.00	.00	k4
5.3	5.3	k4
<g/>
.00	.00	k4
7.03	7.03	k4
<g/>
.00	.00	k4
3.09	3.09	k4
<g/>
.01	.01	k4
1.00	1.00	k4
<g/>
.05	.05	k4
</s>
<s>
20140925	#num#	k4
<g/>
a	a	k8xC
<g/>
25	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2014	#num#	k4
</s>
<s>
8.1	8.1	k4
</s>
<s>
12B410	12B410	k4
12B411	12B411	k4
</s>
<s>
0	#num#	k4
<g/>
4.12	4.12	k4
<g/>
.09	.09	k4
3.0	3.0	k4
<g/>
.04	.04	k4
5.4	5.4	k4
<g/>
.00	.00	k4
5.3	5.3	k4
<g/>
.00	.00	k4
7.03	7.03	k4
<g/>
.00	.00	k4
3.11	3.11	k4
<g/>
.00	.00	k4
1.04	1.04	k4
<g/>
.00	.00	k4
</s>
<s>
20141020	#num#	k4
<g/>
a	a	k8xC
<g/>
20	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2014	#num#	k4
</s>
<s>
8.1	8.1	k4
<g/>
.1	.1	k4
</s>
<s>
12B435	12B435	k4
12B436	12B436	k4
</s>
<s>
0	#num#	k4
<g/>
4.12	4.12	k4
<g/>
.09	.09	k4
3.0	3.0	k4
<g/>
.04	.04	k4
5.4	5.4	k4
<g/>
.00	.00	k4
5.3	5.3	k4
<g/>
.00	.00	k4
7.03	7.03	k4
<g/>
.00	.00	k4
3.11	3.11	k4
<g/>
.00	.00	k4
1.04	1.04	k4
<g/>
.00	.00	k4
</s>
<s>
20141117	#num#	k4
<g/>
a	a	k8xC
<g/>
17	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2014	#num#	k4
</s>
<s>
8.1	8.1	k4
<g/>
.2	.2	k4
</s>
<s>
12B440	12B440	k4
</s>
<s>
0	#num#	k4
<g/>
4.12	4.12	k4
<g/>
.09	.09	k4
3.0	3.0	k4
<g/>
.04	.04	k4
5.4	5.4	k4
<g/>
.00	.00	k4
5.3	5.3	k4
<g/>
.00	.00	k4
7.03	7.03	k4
<g/>
.00	.00	k4
3.11	3.11	k4
<g/>
.00	.00	k4
1.04	1.04	k4
<g/>
.00	.00	k4
</s>
<s>
20141209	#num#	k4
<g/>
a	a	k8xC
<g/>
9	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2014	#num#	k4
</s>
<s>
8.1	8.1	k4
<g/>
.3	.3	k4
</s>
<s>
12B466	12B466	k4
</s>
<s>
0	#num#	k4
<g/>
4.12	4.12	k4
<g/>
.09	.09	k4
3.0	3.0	k4
<g/>
.04	.04	k4
5.4	5.4	k4
<g/>
.00	.00	k4
5.3	5.3	k4
<g/>
.00	.00	k4
7.03	7.03	k4
<g/>
.00	.00	k4
3.11	3.11	k4
<g/>
.00	.00	k4
1.04	1.04	k4
<g/>
.00	.00	k4
</s>
<s>
20150127	#num#	k4
<g/>
a	a	k8xC
<g/>
27	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2015	#num#	k4
</s>
<s>
8.2	8.2	k4
</s>
<s>
12D508	12D508	k4
</s>
<s>
0	#num#	k4
<g/>
4.12	4.12	k4
<g/>
.09	.09	k4
5.4	5.4	k4
<g/>
.00	.00	k4
5.3	5.3	k4
<g/>
.00	.00	k4
7.03	7.03	k4
<g/>
.00	.00	k4
3.11	3.11	k4
<g/>
.00	.00	k4
1.04	1.04	k4
<g/>
.00	.00	k4
</s>
<s>
20150309	#num#	k4
<g/>
a	a	k8xC
<g/>
9	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2015	#num#	k4
</s>
<s>
8.3	8.3	k4
</s>
<s>
12F69	12F69	k4
</s>
<s>
0	#num#	k4
<g/>
4.12	4.12	k4
<g/>
.09	.09	k4
5.4	5.4	k4
<g/>
.00	.00	k4
8.01	8.01	k4
<g/>
.00	.00	k4
4.01	4.01	k4
<g/>
.00	.00	k4
2.23	2.23	k4
<g/>
.03	.03	k4
</s>
<s>
20150408	#num#	k4
<g/>
a	a	k8xC
<g/>
8	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2015	#num#	k4
</s>
<s>
8.4	8.4	k4
</s>
<s>
12H143	12H143	k4
</s>
<s>
0	#num#	k4
<g/>
4.12	4.12	k4
<g/>
.09	.09	k4
5.4	5.4	k4
<g/>
.00	.00	k4
5.5	5.5	k4
<g/>
.00	.00	k4
8.02	8.02	k4
<g/>
.00	.00	k4
4.03	4.03	k4
<g/>
.00	.00	k4
</s>
<s>
20150630	#num#	k4
<g/>
a	a	k8xC
<g/>
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2015	#num#	k4
</s>
<s>
8.4	8.4	k4
<g/>
.1	.1	k4
</s>
<s>
12H321	12H321	k4
</s>
<s>
0	#num#	k4
<g/>
4.12	4.12	k4
<g/>
.09	.09	k4
5.4	5.4	k4
<g/>
.00	.00	k4
5.5	5.5	k4
<g/>
.00	.00	k4
8.02	8.02	k4
<g/>
.00	.00	k4
4.03	4.03	k4
<g/>
.00	.00	k4
</s>
<s>
20150813	#num#	k4
<g/>
a	a	k8xC
<g/>
13	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2015	#num#	k4
</s>
<s>
Verze	verze	k1gFnSc1
</s>
<s>
Build	Build	k6eAd1
</s>
<s>
Baseband	Baseband	k1gInSc1
</s>
<s>
Vydáno	vydán	k2eAgNnSc1d1
</s>
<s>
iOS	iOS	k?
9	#num#	k4
<g/>
.	.	kIx.
<g/>
x	x	k?
</s>
<s>
Logo	logo	k1gNnSc1
iOS	iOS	k?
9	#num#	k4
</s>
<s>
Apple	Apple	kA
představil	představit	k5eAaPmAgInS
iOS	iOS	k?
9	#num#	k4
8	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2015	#num#	k4
na	na	k7c6
své	svůj	k3xOyFgFnSc6
výroční	výroční	k2eAgFnSc6d1
konferenci	konference	k1gFnSc6
Apple	Apple	kA
WWDC	WWDC	kA
2015	#num#	k4
a	a	k8xC
systém	systém	k1gInSc1
vydal	vydat	k5eAaPmAgInS
16	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Systém	systém	k1gInSc1
podporuje	podporovat	k5eAaImIp3nS
všechna	všechen	k3xTgNnPc4
zařízení	zařízení	k1gNnPc4
<g/>
,	,	kIx,
které	který	k3yQgNnSc4,k3yIgNnSc4,k3yRgNnSc4
podporoval	podporovat	k5eAaImAgMnS
iOS	iOS	k?
8	#num#	k4
<g/>
,	,	kIx,
tedy	tedy	k9
iPhone	iPhon	k1gInSc5
4S	4S	k4
a	a	k8xC
novější	nový	k2eAgFnSc2d2
<g/>
,	,	kIx,
iPod	iPod	k1gMnSc1
Touch	Touch	k1gMnSc1
(	(	kIx(
<g/>
5	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
iPad	iPad	k1gInSc1
2	#num#	k4
a	a	k8xC
novější	nový	k2eAgInSc4d2
a	a	k8xC
iPad	iPad	k6eAd1
mini	mini	k2eAgFnSc1d1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
novější	nový	k2eAgInSc4d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Veze	vézt	k5eAaImIp3nS
9.3	9.3	k4
<g/>
.5	.5	k4
je	být	k5eAaImIp3nS
poslední	poslední	k2eAgFnSc1d1
aktualizace	aktualizace	k1gFnSc1
pro	pro	k7c4
zařízení	zařízení	k1gNnSc4
iPhone	iPhon	k1gInSc5
4	#num#	k4
<g/>
S	s	k7c7
<g/>
,	,	kIx,
iPad	iPad	k1gInSc1
2	#num#	k4
<g/>
,	,	kIx,
iPad	iPad	k1gInSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
iPod	iPod	k1gMnSc1
Touch	Touch	k1gMnSc1
(	(	kIx(
<g/>
5	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
iPad	iPad	k6eAd1
mini	mini	k2eAgFnSc1d1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Tabulka	tabulka	k1gFnSc1
verzí	verze	k1gFnPc2
<g/>
:	:	kIx,
iOS	iOS	k?
9	#num#	k4
<g/>
.	.	kIx.
<g/>
x	x	k?
–	–	k?
Přenosná	přenosný	k2eAgFnSc1d1
zařízení	zařízení	k1gNnPc1
</s>
<s>
Verze	verze	k1gFnSc1
</s>
<s>
Build	Build	k6eAd1
</s>
<s>
Vydáno	vydán	k2eAgNnSc1d1
</s>
<s>
iPhone	iPhon	k1gMnSc5
4	#num#	k4
<g/>
S	s	k7c7
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
5	#num#	k4
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
5	#num#	k4
<g/>
C	C	kA
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
5	#num#	k4
<g/>
S	s	k7c7
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
6	#num#	k4
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
6	#num#	k4
Plus	plus	k1gInSc1
<g/>
,	,	kIx,
iPod	iPod	k1gMnSc1
Touch	Touch	k1gMnSc1
(	(	kIx(
<g/>
5	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
iPad	iPad	k1gInSc1
2	#num#	k4
<g/>
,	,	kIx,
iPad	iPad	k1gInSc1
(	(	kIx(
<g/>
3	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
iPad	iPad	k1gMnSc1
(	(	kIx(
<g/>
4	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
iPad	iPad	k1gMnSc1
Air	Air	k1gMnSc1
<g/>
,	,	kIx,
iPad	iPad	k1gMnSc1
Air	Air	k1gFnSc2
2	#num#	k4
<g/>
,	,	kIx,
iPad	iPad	k6eAd1
mini	mini	k2eAgFnSc1d1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
iPad	iPad	k6eAd1
mini	mini	k2eAgFnSc1d1
2	#num#	k4
<g/>
,	,	kIx,
iPad	iPad	k6eAd1
mini	mini	k2eAgFnSc1d1
3	#num#	k4
<g/>
,	,	kIx,
iPad	iPad	k6eAd1
mini	mini	k2eAgFnSc1d1
4	#num#	k4
<g/>
,	,	kIx,
později	pozdě	k6eAd2
iPad	iPad	k1gInSc1
Pro	pro	k7c4
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
SE	se	k3xPyFc4
</s>
<s>
9.0	9.0	k4
</s>
<s>
13A34013A34213A34313A344	13A34013A34213A34313A344	k4
</s>
<s>
20150916	#num#	k4
<g/>
a	a	k8xC
<g/>
16	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2015	#num#	k4
</s>
<s>
9.0	9.0	k4
<g/>
.1	.1	k4
</s>
<s>
13A40413A405	13A40413A405	k4
</s>
<s>
20150923	#num#	k4
<g/>
a	a	k8xC
<g/>
23	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2015	#num#	k4
</s>
<s>
9.0	9.0	k4
<g/>
.2	.2	k4
</s>
<s>
13A452	13A452	k4
</s>
<s>
20150930	#num#	k4
<g/>
a	a	k8xC
<g/>
30	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2015	#num#	k4
</s>
<s>
9.1	9.1	k4
</s>
<s>
13B143	13B143	k4
</s>
<s>
20151015	#num#	k4
<g/>
a	a	k8xC
<g/>
15	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2015	#num#	k4
</s>
<s>
9.2	9.2	k4
</s>
<s>
13C75	13C75	k4
</s>
<s>
20151208	#num#	k4
<g/>
a	a	k8xC
<g/>
8	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2015	#num#	k4
</s>
<s>
9.2	9.2	k4
<g/>
.1	.1	k4
</s>
<s>
13D1513D20	13D1513D20	k4
</s>
<s>
20160116	#num#	k4
<g/>
a	a	k8xC
<g/>
16	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2016	#num#	k4
</s>
<s>
9.3	9.3	k4
</s>
<s>
13E23313E23413E23613E237	13E23313E23413E23613E237	k4
</s>
<s>
20160321	#num#	k4
<g/>
a	a	k8xC
<g/>
21	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2016	#num#	k4
</s>
<s>
9.3	9.3	k4
<g/>
.1	.1	k4
</s>
<s>
13E238	13E238	k4
</s>
<s>
20160331	#num#	k4
<g/>
a	a	k8xC
<g/>
31	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2016	#num#	k4
</s>
<s>
9.3	9.3	k4
<g/>
.2	.2	k4
</s>
<s>
13F6913F72	13F6913F72	k4
</s>
<s>
20160516	#num#	k4
<g/>
a	a	k8xC
<g/>
16	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2016	#num#	k4
</s>
<s>
9.3	9.3	k4
<g/>
.3	.3	k4
</s>
<s>
13G34	13G34	k4
</s>
<s>
20160718	#num#	k4
<g/>
a	a	k8xC
<g/>
18	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2016	#num#	k4
</s>
<s>
9.3	9.3	k4
<g/>
.4	.4	k4
</s>
<s>
13G35	13G35	k4
</s>
<s>
20160804	#num#	k4
<g/>
a	a	k8xC
<g/>
4	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2016	#num#	k4
</s>
<s>
9.3	9.3	k4
<g/>
.5	.5	k4
</s>
<s>
13G36	13G36	k4
</s>
<s>
20160825	#num#	k4
<g/>
a	a	k8xC
<g/>
25	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2016	#num#	k4
</s>
<s>
9.3	9.3	k4
<g/>
.6	.6	k4
</s>
<s>
13G37	13G37	k4
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
<g/>
června	červen	k1gInSc2
2019	#num#	k4
</s>
<s>
iOS	iOS	k?
10	#num#	k4
<g/>
.	.	kIx.
<g/>
x	x	k?
</s>
<s>
Apple	Apple	kA
představil	představit	k5eAaPmAgInS
iOS	iOS	k?
10	#num#	k4
13	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2016	#num#	k4
na	na	k7c6
své	svůj	k3xOyFgFnSc6
výroční	výroční	k2eAgFnSc6d1
konferenci	konference	k1gFnSc6
Apple	Apple	kA
WWDC	WWDC	kA
2017	#num#	k4
a	a	k8xC
systém	systém	k1gInSc1
vydal	vydat	k5eAaPmAgInS
13	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Systém	systém	k1gInSc1
podporuje	podporovat	k5eAaImIp3nS
zařízení	zařízení	k1gNnSc4
od	od	k7c2
iPhonu	iPhon	k1gInSc2
5	#num#	k4
do	do	k7c2
iPhone	iPhon	k1gInSc5
7	#num#	k4
Plus	plus	k1gNnPc1
či	či	k8xC
šestou	šestý	k4xOgFnSc4
generaci	generace	k1gFnSc4
iPod	iPod	k6eAd1
Touch	Touch	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
podporuje	podporovat	k5eAaImIp3nS
zařízení	zařízení	k1gNnSc4
iPad	iPada	k1gFnPc2
od	od	k7c2
čtvrté	čtvrtá	k1gFnSc2
generace	generace	k1gFnSc2
iPadu	iPad	k1gInSc2
do	do	k7c2
iPadu	iPad	k1gInSc2
Pro	pro	k7c4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Veze	vézt	k5eAaImIp3nS
10.3	10.3	k4
<g/>
.4	.4	k4
je	být	k5eAaImIp3nS
poslední	poslední	k2eAgFnSc7d1
aktualizací	aktualizace	k1gFnSc7
systému	systém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
iOS	iOS	k?
11	#num#	k4
<g/>
.	.	kIx.
<g/>
x	x	k?
</s>
<s>
Apple	Apple	kA
představil	představit	k5eAaPmAgInS
iOS	iOS	k?
11	#num#	k4
5	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2017	#num#	k4
<g/>
.	.	kIx.
iOS	iOS	k?
11	#num#	k4
je	být	k5eAaImIp3nS
zatím	zatím	k6eAd1
veřejností	veřejnost	k1gFnSc7
odsuzován	odsuzovat	k5eAaImNgMnS
za	za	k7c4
nestabilitu	nestabilita	k1gFnSc4
a	a	k8xC
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
historicky	historicky	k6eAd1
nejkritizovanější	kritizovaný	k2eAgInSc1d3
iOS	iOS	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Apple	Apple	kA
slibuje	slibovat	k5eAaImIp3nS
nestabilitu	nestabilita	k1gFnSc4
opravit	opravit	k5eAaPmF
v	v	k7c6
budoucích	budoucí	k2eAgFnPc6d1
aktualizacích	aktualizace	k1gFnPc6
systému	systém	k1gInSc2
iOS	iOS	k?
11	#num#	k4
<g/>
.	.	kIx.
iOS	iOS	k?
11	#num#	k4
mimo	mimo	k7c4
jiné	jiné	k1gNnSc4
ukončuje	ukončovat	k5eAaImIp3nS
podporu	podpora	k1gFnSc4
veškerým	veškerý	k3xTgInSc7
32	#num#	k4
<g/>
bitovým	bitový	k2eAgFnPc3d1
aplikacím	aplikace	k1gFnPc3
a	a	k8xC
32	#num#	k4
<g/>
bitovým	bitový	k2eAgNnSc7d1
jádrům	jádro	k1gNnPc3
přístrojů	přístroj	k1gInPc2
(	(	kIx(
<g/>
např.	např.	kA
iPhone	iPhon	k1gInSc5
5	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Verze	verze	k1gFnSc1
11.4	11.4	k4
<g/>
.1	.1	k4
je	být	k5eAaImIp3nS
poslední	poslední	k2eAgFnSc7d1
aktualizací	aktualizace	k1gFnSc7
systému	systém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
iOS	iOS	k?
12	#num#	k4
<g/>
.	.	kIx.
<g/>
x	x	k?
</s>
<s>
iOS	iOS	k?
12	#num#	k4
byl	být	k5eAaImAgInS
oznámen	oznámit	k5eAaPmNgInS
4	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2018	#num#	k4
a	a	k8xC
vydán	vydán	k2eAgInSc1d1
17	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
2018	#num#	k4
spolu	spolu	k6eAd1
s	s	k7c7
telefony	telefon	k1gInPc7
iPhone	iPhon	k1gInSc5
XS	XS	kA
a	a	k8xC
iPhone	iPhon	k1gInSc5
XS	XS	kA
Max	max	kA
<g/>
.	.	kIx.
Tímto	tento	k3xDgNnSc7
vydáním	vydání	k1gNnSc7
nebyla	být	k5eNaImAgFnS
ukončena	ukončit	k5eAaPmNgFnS
podpora	podpora	k1gFnSc1
žádnému	žádný	k3yNgNnSc3
zařízení	zařízení	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Verze	verze	k1gFnSc1
12.5	12.5	k4
<g/>
.3	.3	k4
je	být	k5eAaImIp3nS
aktuálně	aktuálně	k6eAd1
poslední	poslední	k2eAgFnSc7d1
aktualizací	aktualizace	k1gFnSc7
systému	systém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
iOS	iOS	k?
13	#num#	k4
<g/>
.	.	kIx.
<g/>
x	x	k?
</s>
<s>
Systém	systém	k1gInSc1
iOS	iOS	k?
13	#num#	k4
byl	být	k5eAaImAgInS
představen	představit	k5eAaPmNgInS
více	hodně	k6eAd2
prezidentem	prezident	k1gMnSc7
pro	pro	k7c4
software	software	k1gInSc4
Craigem	Craigo	k1gNnSc7
Federighim	Federighima	k1gFnPc2
3	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2019	#num#	k4
na	na	k7c6
konferenci	konference	k1gFnSc6
WWDC	WWDC	kA
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
uživatele	uživatel	k1gMnSc4
byl	být	k5eAaImAgInS
zpřístupněn	zpřístupněn	k2eAgInSc1d1
společně	společně	k6eAd1
s	s	k7c7
telefony	telefon	k1gInPc7
iPhone	iPhon	k1gInSc5
11	#num#	k4
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
11	#num#	k4
Pro	pro	k7c4
a	a	k8xC
Pro	pro	k7c4
Max	max	kA
19	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuální	aktuální	k2eAgFnSc7d1
verzí	verze	k1gFnSc7
je	být	k5eAaImIp3nS
iOS	iOS	k?
13.5	13.5	k4
<g/>
.1	.1	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Systém	systém	k1gInSc1
dostal	dostat	k5eAaPmAgInS
pozitivní	pozitivní	k2eAgInPc4d1
ohlasy	ohlas	k1gInPc4
jak	jak	k8xC,k8xS
od	od	k7c2
uživatelů	uživatel	k1gMnPc2
<g/>
,	,	kIx,
tak	tak	k6eAd1
recenzérů	recenzér	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skončila	skončit	k5eAaPmAgFnS
podpora	podpora	k1gFnSc1
pro	pro	k7c4
telefony	telefon	k1gInPc4
s	s	k7c7
méně	málo	k6eAd2
jak	jak	k8xC,k8xS
2	#num#	k4
GB	GB	kA
RAM	RAM	kA
-	-	kIx~
iPhone	iPhon	k1gInSc5
5	#num#	k4
<g/>
S	s	k7c7
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
6	#num#	k4
a	a	k8xC
6	#num#	k4
Plus	plus	k1gInSc1
<g/>
,	,	kIx,
iPod	iPod	k1gMnSc1
Touch	Touch	k1gMnSc1
(	(	kIx(
<g/>
6	#num#	k4
generace	generace	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Verze	verze	k1gFnSc1
13.7	13.7	k4
je	být	k5eAaImIp3nS
poslední	poslední	k2eAgFnSc7d1
aktualizací	aktualizace	k1gFnSc7
systému	systém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
iOS	iOS	k?
14	#num#	k4
<g/>
.	.	kIx.
<g/>
x	x	k?
</s>
<s>
Apple	Apple	kA
představil	představit	k5eAaPmAgInS
iOS	iOS	k?
14	#num#	k4
na	na	k7c4
WWDC	WWDC	kA
2020	#num#	k4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
odehrávala	odehrávat	k5eAaImAgFnS
poprvé	poprvé	k6eAd1
v	v	k7c6
historii	historie	k1gFnSc6
digitálně	digitálně	k6eAd1
<g/>
,	,	kIx,
dne	den	k1gInSc2
22	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostrá	ostrý	k2eAgFnSc1d1
verze	verze	k1gFnSc1
systému	systém	k1gInSc2
byla	být	k5eAaImAgFnS
uvolněna	uvolnit	k5eAaPmNgFnS
pro	pro	k7c4
veřejnost	veřejnost	k1gFnSc4
16.9	16.9	k4
<g/>
.2020	.2020	k4
(	(	kIx(
<g/>
netradičně	tradičně	k6eNd1
dříve	dříve	k6eAd2
než	než	k8xS
byla	být	k5eAaImAgFnS
představena	představit	k5eAaPmNgFnS
nová	nový	k2eAgFnSc1d1
řada	řada	k1gFnSc1
iPhonů	iPhon	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuální	aktuální	k2eAgFnSc7d1
verzí	verze	k1gFnSc7
aktualizací	aktualizace	k1gFnPc2
systému	systém	k1gInSc2
je	být	k5eAaImIp3nS
14.5	14.5	k4
<g/>
.1	.1	k4
<g/>
.	.	kIx.
</s>
<s>
iOS	iOS	k?
15	#num#	k4
<g/>
.	.	kIx.
<g/>
x	x	k?
</s>
<s>
Představení	představení	k1gNnSc1
iOS	iOS	k?
15	#num#	k4
by	by	kYmCp3nS
mělo	mít	k5eAaImAgNnS
proběhnout	proběhnout	k5eAaPmF
na	na	k7c4
WWDC	WWDC	kA
2021	#num#	k4
<g/>
(	(	kIx(
<g/>
7	#num#	k4
<g/>
.	.	kIx.
<g/>
června	červen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Architektura	architektura	k1gFnSc1
</s>
<s>
iOS	iOS	k?
je	být	k5eAaImIp3nS
odlehčenou	odlehčený	k2eAgFnSc7d1
verzí	verze	k1gFnSc7
operačního	operační	k2eAgInSc2d1
systému	systém	k1gInSc2
macOS	macOS	k?
<g/>
,	,	kIx,
používaného	používaný	k2eAgNnSc2d1
v	v	k7c6
počítačích	počítač	k1gInPc6
společnosti	společnost	k1gFnSc2
Apple	Apple	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
tedy	tedy	k9
o	o	k7c4
systém	systém	k1gInSc4
UNIXového	unixový	k2eAgInSc2d1
typu	typ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jelikož	jelikož	k8xS
je	být	k5eAaImIp3nS
určen	určit	k5eAaPmNgInS
pro	pro	k7c4
mobilní	mobilní	k2eAgNnSc4d1
zařízení	zařízení	k1gNnSc4
<g/>
,	,	kIx,
neobsahuje	obsahovat	k5eNaImIp3nS
veškerou	veškerý	k3xTgFnSc4
funkcionalitu	funkcionalita	k1gFnSc4
OS	OS	kA
X	X	kA
<g/>
,	,	kIx,
na	na	k7c4
druhou	druhý	k4xOgFnSc4
stranu	strana	k1gFnSc4
ale	ale	k8xC
přidává	přidávat	k5eAaImIp3nS
podporu	podpora	k1gFnSc4
dotykového	dotykový	k2eAgNnSc2d1
ovládání	ovládání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Systém	systém	k1gInSc1
se	se	k3xPyFc4
dělí	dělit	k5eAaImIp3nS
na	na	k7c4
čtyři	čtyři	k4xCgFnPc4
základní	základní	k2eAgFnPc4d1
vrstvy	vrstva	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
zajišťují	zajišťovat	k5eAaImIp3nP
základní	základní	k2eAgFnSc4d1
funkčnost	funkčnost	k1gFnSc4
a	a	k8xC
poskytují	poskytovat	k5eAaImIp3nP
vývojářům	vývojář	k1gMnPc3
API	API	kA
a	a	k8xC
frameworky	frameworka	k1gFnSc2
potřebné	potřebný	k2eAgFnSc2d1
k	k	k7c3
vývoji	vývoj	k1gInSc3
aplikací	aplikace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Vrstva	vrstva	k1gFnSc1
Cocoa	Cocoa	k1gMnSc1
Touch	Touch	k1gMnSc1
</s>
<s>
Tato	tento	k3xDgFnSc1
vrstva	vrstva	k1gFnSc1
obsahuje	obsahovat	k5eAaImIp3nS
nejdůležitější	důležitý	k2eAgInPc4d3
frameworky	frameworek	k1gInPc4
při	při	k7c6
vývoji	vývoj	k1gInSc6
aplikací	aplikace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Technologie	technologie	k1gFnSc1
dostupné	dostupný	k2eAgNnSc4d1
v	v	k7c6
této	tento	k3xDgFnSc6
vrstvě	vrstva	k1gFnSc6
poskytují	poskytovat	k5eAaImIp3nP
infrastrukturu	infrastruktura	k1gFnSc4
pro	pro	k7c4
implementaci	implementace	k1gFnSc4
grafického	grafický	k2eAgNnSc2d1
rozhraní	rozhraní	k1gNnSc2
aplikace	aplikace	k1gFnSc2
a	a	k8xC
interakci	interakce	k1gFnSc4
s	s	k7c7
uživatelem	uživatel	k1gMnSc7
a	a	k8xC
poskytuje	poskytovat	k5eAaImIp3nS
vysokoúrovňové	vysokoúrovňový	k2eAgFnPc4d1
systémové	systémový	k2eAgFnPc4d1
služby	služba	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
vývoji	vývoj	k1gInSc6
aplikací	aplikace	k1gFnPc2
je	být	k5eAaImIp3nS
vhodné	vhodný	k2eAgNnSc1d1
začínat	začínat	k5eAaImF
právě	právě	k9
s	s	k7c7
touto	tento	k3xDgFnSc7
vrstvou	vrstva	k1gFnSc7
a	a	k8xC
nižší	nízký	k2eAgMnSc1d2
používat	používat	k5eAaImF
pouze	pouze	k6eAd1
v	v	k7c6
případě	případ	k1gInSc6
potřeby	potřeba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Vysokoúrovňové	vysokoúrovňový	k2eAgFnPc1d1
služby	služba	k1gFnPc1
Cocoa	Cocoa	k1gMnSc1
Touch	Touch	k1gMnSc1
</s>
<s>
Multitasking	multitasking	k1gInSc1
</s>
<s>
Až	až	k9
do	do	k7c2
verze	verze	k1gFnSc2
iOS	iOS	k?
4.0	4.0	k4
nebylo	být	k5eNaImAgNnS
možné	možný	k2eAgNnSc1d1
psát	psát	k5eAaImF
aplikace	aplikace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
by	by	kYmCp3nP
byly	být	k5eAaImAgFnP
schopné	schopný	k2eAgFnPc1d1
běhu	běh	k1gInSc6
na	na	k7c6
pozadí	pozadí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stisk	stisk	k1gInSc1
tlačítka	tlačítko	k1gNnSc2
Home	Hom	k1gFnSc2
vyvolal	vyvolat	k5eAaPmAgInS
ukončení	ukončení	k1gNnSc4
aplikace	aplikace	k1gFnSc2
<g/>
.	.	kIx.
iOS	iOS	k?
4.0	4.0	k4
sice	sice	k8xC
nepřinesl	přinést	k5eNaPmAgInS
možnost	možnost	k1gFnSc4
plnohodnotného	plnohodnotný	k2eAgInSc2d1
běhu	běh	k1gInSc2
na	na	k7c6
pozadí	pozadí	k1gNnSc6
<g/>
,	,	kIx,
avšak	avšak	k8xC
poskytuje	poskytovat	k5eAaImIp3nS
služby	služba	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
umožňují	umožňovat	k5eAaImIp3nP
provádění	provádění	k1gNnSc4
některých	některý	k3yIgFnPc2
činností	činnost	k1gFnPc2
<g/>
,	,	kIx,
i	i	k9
když	když	k8xS
aplikace	aplikace	k1gFnSc1
neběží	běžet	k5eNaImIp3nS
na	na	k7c6
popředí	popředí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Ochrana	ochrana	k1gFnSc1
dat	datum	k1gNnPc2
</s>
<s>
Aplikace	aplikace	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
ukládají	ukládat	k5eAaImIp3nP
citlivá	citlivý	k2eAgNnPc4d1
data	datum	k1gNnPc4
<g/>
,	,	kIx,
mohou	moct	k5eAaImIp3nP
využívat	využívat	k5eAaPmF,k5eAaImF
vestavěné	vestavěný	k2eAgFnPc4d1
podpory	podpora	k1gFnPc4
šifrování	šifrování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
aplikace	aplikace	k1gFnPc1
označí	označit	k5eAaPmIp3nP
soubor	soubor	k1gInSc4
jako	jako	k8xC,k8xS
chráněný	chráněný	k2eAgInSc4d1
<g/>
,	,	kIx,
systém	systém	k1gInSc1
ho	on	k3xPp3gInSc4
automaticky	automaticky	k6eAd1
ukládá	ukládat	k5eAaImIp3nS
na	na	k7c4
disk	disk	k1gInSc4
v	v	k7c6
zašifrované	zašifrovaný	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dokud	dokud	k8xS
je	být	k5eAaImIp3nS
zařízení	zařízení	k1gNnSc1
uzamčené	uzamčený	k2eAgNnSc1d1
<g/>
,	,	kIx,
obsah	obsah	k1gInSc1
souboru	soubor	k1gInSc2
je	být	k5eAaImIp3nS
nedostupný	dostupný	k2eNgInSc1d1
jak	jak	k8xC,k8xS
aplikaci	aplikace	k1gFnSc6
<g/>
,	,	kIx,
tak	tak	k6eAd1
případnému	případný	k2eAgMnSc3d1
útočníkovi	útočník	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k8xS
uživatel	uživatel	k1gMnSc1
zařízení	zařízení	k1gNnSc4
odemkne	odemknout	k5eAaPmIp3nS
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
vygenerován	vygenerován	k2eAgInSc4d1
dešifrovací	dešifrovací	k2eAgInSc4d1
klíč	klíč	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
aplikaci	aplikace	k1gFnSc4
umožní	umožnit	k5eAaPmIp3nS
soubor	soubor	k1gInSc1
přečíst	přečíst	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zařízení	zařízení	k1gNnPc1
však	však	k8xC
(	(	kIx(
<g/>
i	i	k9
v	v	k7c6
iOS	iOS	k?
6.1	6.1	k4
<g/>
)	)	kIx)
lze	lze	k6eAd1
snadno	snadno	k6eAd1
odemknout	odemknout	k5eAaPmF
a	a	k8xC
volat	volat	k5eAaImF
i	i	k9
bez	bez	k7c2
znalosti	znalost	k1gFnSc2
hesla	heslo	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Push	Push	k1gInSc1
notifikace	notifikace	k1gFnSc2
</s>
<s>
Od	od	k7c2
verze	verze	k1gFnSc2
3.0	3.0	k4
umožňuje	umožňovat	k5eAaImIp3nS
iOS	iOS	k?
posílání	posílání	k1gNnSc1
tzv.	tzv.	kA
push	pusha	k1gFnPc2
notifikací	notifikace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
mechanismus	mechanismus	k1gInSc1
umožňuje	umožňovat	k5eAaImIp3nS
upozorňovat	upozorňovat	k5eAaImF
uživatele	uživatel	k1gMnSc4
na	na	k7c4
nové	nový	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
aniž	aniž	k8xC,k8xS
by	by	kYmCp3nS
musela	muset	k5eAaImAgFnS
být	být	k5eAaImF
daná	daný	k2eAgFnSc1d1
aplikace	aplikace	k1gFnSc1
právě	právě	k6eAd1
spuštěna	spustit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uživateli	uživatel	k1gMnPc7
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
zobrazit	zobrazit	k5eAaPmF
krátkou	krátký	k2eAgFnSc4d1
textovou	textový	k2eAgFnSc4d1
informaci	informace	k1gFnSc4
<g/>
,	,	kIx,
přehrát	přehrát	k5eAaPmF
zvuk	zvuk	k1gInSc4
či	či	k8xC
aktualizovat	aktualizovat	k5eAaBmF
číselnou	číselný	k2eAgFnSc4d1
značku	značka	k1gFnSc4
(	(	kIx(
<g/>
badge	badge	k1gFnSc1
<g/>
,	,	kIx,
odznak	odznak	k1gInSc1
<g/>
)	)	kIx)
na	na	k7c6
ikoně	ikona	k1gFnSc6
aplikace	aplikace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Iniciovat	iniciovat	k5eAaBmF
odeslání	odeslání	k1gNnPc4
push	pusha	k1gFnPc2
notifikace	notifikace	k1gFnSc1
musí	muset	k5eAaImIp3nS
server	server	k1gInSc4
výrobce	výrobce	k1gMnSc2
aplikací	aplikace	k1gFnPc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
následně	následně	k6eAd1
komunikuje	komunikovat	k5eAaImIp3nS
se	s	k7c7
servery	server	k1gInPc7
společnosti	společnost	k1gFnSc2
Apple	Apple	kA
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
se	se	k3xPyFc4
pokusí	pokusit	k5eAaPmIp3nP
o	o	k7c4
doručení	doručení	k1gNnSc4
na	na	k7c4
mobilní	mobilní	k2eAgNnSc4d1
zařízení	zařízení	k1gNnSc4
(	(	kIx(
<g/>
doručení	doručení	k1gNnSc4
není	být	k5eNaImIp3nS
garantováno	garantován	k2eAgNnSc1d1
<g/>
)	)	kIx)
pomocí	pomocí	k7c2
jeho	jeho	k3xOp3gInSc2,k3xPp3gInSc2
unikátního	unikátní	k2eAgInSc2d1
identifikátoru	identifikátor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Lokální	lokální	k2eAgFnSc1d1
notifikace	notifikace	k1gFnSc1
</s>
<s>
Verze	verze	k1gFnSc1
iOS	iOS	k?
4	#num#	k4
a	a	k8xC
vyšší	vysoký	k2eAgInPc1d2
doplňují	doplňovat	k5eAaImIp3nP
mechanismus	mechanismus	k1gInSc4
push	push	k1gInSc1
notifikací	notifikace	k1gFnPc2
o	o	k7c6
tzv.	tzv.	kA
lokální	lokální	k2eAgFnPc4d1
notifikace	notifikace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ten	k3xDgInPc1
nevyžadují	vyžadovat	k5eNaImIp3nP
žádné	žádný	k3yNgNnSc4
připojení	připojení	k1gNnSc4
k	k	k7c3
serveru	server	k1gInSc2
a	a	k8xC
údaje	údaj	k1gInPc4
o	o	k7c6
notifikacích	notifikace	k1gFnPc6
ukládají	ukládat	k5eAaImIp3nP
lokálně	lokálně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aplikace	aplikace	k1gFnSc1
aktuálně	aktuálně	k6eAd1
běžící	běžící	k2eAgFnSc1d1
v	v	k7c6
pozadí	pozadí	k1gNnSc6
(	(	kIx(
<g/>
např.	např.	kA
autonavigace	autonavigace	k1gFnSc2
<g/>
)	)	kIx)
mohou	moct	k5eAaImIp3nP
v	v	k7c6
případě	případ	k1gInSc6
potřeby	potřeba	k1gFnSc2
upozornit	upozornit	k5eAaPmF
uživatele	uživatel	k1gMnSc4
na	na	k7c4
důležité	důležitý	k2eAgFnPc4d1
události	událost	k1gFnPc4
(	(	kIx(
<g/>
např.	např.	kA
o	o	k7c6
blížící	blížící	k2eAgFnSc6d1
se	se	k3xPyFc4
zatáčce	zatáčka	k1gFnSc6
<g/>
)	)	kIx)
ihned	ihned	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
je	být	k5eAaImIp3nS
také	také	k9
možné	možný	k2eAgNnSc1d1
naplánovat	naplánovat	k5eAaBmF
notifikaci	notifikace	k1gFnSc4
na	na	k7c4
určitý	určitý	k2eAgInSc4d1
datum	datum	k1gInSc4
a	a	k8xC
čas	čas	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Taková	takový	k3xDgFnSc1
notifikace	notifikace	k1gFnPc4
je	být	k5eAaImIp3nS
již	již	k6eAd1
uložena	uložit	k5eAaPmNgFnS
v	v	k7c6
systému	systém	k1gInSc6
a	a	k8xC
aplikace	aplikace	k1gFnSc2
v	v	k7c4
požadovaný	požadovaný	k2eAgInSc4d1
čas	čas	k1gInSc4
nemusí	muset	k5eNaImIp3nS
běžet	běžet	k5eAaImF
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
se	se	k3xPyFc4
uživateli	uživatel	k1gMnSc3
dané	daný	k2eAgNnSc1d1
upozornění	upozornění	k1gNnSc1
zobrazilo	zobrazit	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s>
Rozpoznávání	rozpoznávání	k1gNnSc1
gest	gesto	k1gNnPc2
</s>
<s>
V	v	k7c6
iOS	iOS	k?
před	před	k7c7
verzí	verze	k1gFnSc7
3.2	3.2	k4
bylo	být	k5eAaImAgNnS
nutné	nutný	k2eAgNnSc1d1
ručně	ručně	k6eAd1
zachytávat	zachytávat	k5eAaImF
a	a	k8xC
zpracovávat	zpracovávat	k5eAaImF
dotykové	dotykový	k2eAgFnPc4d1
události	událost	k1gFnPc4
a	a	k8xC
komplikovaně	komplikovaně	k6eAd1
rozpoznávat	rozpoznávat	k5eAaImF
gesta	gesto	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
verze	verze	k1gFnSc2
3.2	3.2	k4
však	však	k9
existuje	existovat	k5eAaImIp3nS
možnost	možnost	k1gFnSc4
systémového	systémový	k2eAgNnSc2d1
rozpoznávání	rozpoznávání	k1gNnSc2
definovaných	definovaný	k2eAgNnPc2d1
gest	gesto	k1gNnPc2
a	a	k8xC
jejich	jejich	k3xOp3gNnSc2
předávání	předávání	k1gNnSc2
aplikaci	aplikace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Možná	možná	k9
gesta	gesto	k1gNnPc1
jsou	být	k5eAaImIp3nP
ťuknutí	ťuknutí	k1gNnPc4
(	(	kIx(
<g/>
možno	možno	k6eAd1
i	i	k9
vícenásobné	vícenásobný	k2eAgFnPc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
sevření	sevření	k1gNnSc1
a	a	k8xC
rozevření	rozevření	k1gNnSc1
prstů	prst	k1gInPc2
<g/>
,	,	kIx,
přetahování	přetahování	k1gNnSc2
objektů	objekt	k1gInPc2
<g/>
,	,	kIx,
švihnutí	švihnutí	k1gNnSc1
(	(	kIx(
<g/>
swipe	swipat	k5eAaPmIp3nS
<g/>
)	)	kIx)
<g/>
,	,	kIx,
rotace	rotace	k1gFnSc1
<g/>
,	,	kIx,
dlouhý	dlouhý	k2eAgInSc1d1
stisk	stisk	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
nadefinovat	nadefinovat	k5eAaPmF
rozpoznávání	rozpoznávání	k1gNnSc4
vlastních	vlastní	k2eAgNnPc2d1
gest	gesto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Sdílení	sdílení	k1gNnSc1
souborů	soubor	k1gInPc2
</s>
<s>
Aplikace	aplikace	k1gFnSc1
může	moct	k5eAaImIp3nS
poskytnout	poskytnout	k5eAaPmF
přístup	přístup	k1gInSc4
ke	k	k7c3
svým	svůj	k3xOyFgNnPc3
datům	datum	k1gNnPc3
pomocí	pomocí	k7c2
programu	program	k1gInSc2
iTunes	iTunesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
to	ten	k3xDgNnSc1
aplikace	aplikace	k1gFnSc1
umožní	umožnit	k5eAaPmIp3nS
<g/>
,	,	kIx,
lze	lze	k6eAd1
přes	přes	k7c4
iTunes	iTunes	k1gInSc4
nahrávat	nahrávat	k5eAaImF
soubory	soubor	k1gInPc4
do	do	k7c2
definované	definovaný	k2eAgFnSc2d1
složky	složka	k1gFnSc2
v	v	k7c4
aplikaci	aplikace	k1gFnSc4
a	a	k8xC
soubory	soubor	k1gInPc4
z	z	k7c2
ní	on	k3xPp3gFnSc2
naopak	naopak	k6eAd1
kopírovat	kopírovat	k5eAaImF
do	do	k7c2
počítače	počítač	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
služba	služba	k1gFnSc1
neumožňuje	umožňovat	k5eNaImIp3nS
sdílení	sdílení	k1gNnSc4
dokumentů	dokument	k1gInPc2
mezi	mezi	k7c7
aplikacemi	aplikace	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Peer	peer	k1gMnSc1
to	ten	k3xDgNnSc4
peer	peer	k1gMnSc1
</s>
<s>
Od	od	k7c2
verze	verze	k1gFnSc2
3.0	3.0	k4
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
používat	používat	k5eAaImF
peer-to-peer	peer-to-peer	k1gInSc4
konektivitu	konektivita	k1gFnSc4
mezi	mezi	k7c7
více	hodně	k6eAd2
zařízeními	zařízení	k1gNnPc7
pomocí	pomocí	k7c2
technologie	technologie	k1gFnSc2
Bluetooth	Bluetootha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
funkcionalita	funkcionalita	k1gFnSc1
je	být	k5eAaImIp3nS
využívána	využívat	k5eAaPmNgFnS,k5eAaImNgFnS
převážně	převážně	k6eAd1
k	k	k7c3
tvorbě	tvorba	k1gFnSc3
her	hra	k1gFnPc2
pro	pro	k7c4
více	hodně	k6eAd2
hráčů	hráč	k1gMnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
ji	on	k3xPp3gFnSc4
využít	využít	k5eAaPmF
i	i	k9
jinak	jinak	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Standardní	standardní	k2eAgInPc4d1
systémové	systémový	k2eAgInPc4d1
view	view	k?
controllery	controller	k1gInPc4
</s>
<s>
Mnoho	mnoho	k4c1
frameworků	frameworek	k1gInPc2
použitých	použitý	k2eAgInPc2d1
v	v	k7c6
systému	systém	k1gInSc6
používá	používat	k5eAaImIp3nS
standardizované	standardizovaný	k2eAgInPc4d1
komponenty	komponent	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
rámci	rámec	k1gInSc6
zachování	zachování	k1gNnSc2
konzistentního	konzistentní	k2eAgInSc2d1
uživatelského	uživatelský	k2eAgInSc2d1
zážitku	zážitek	k1gInSc2
je	být	k5eAaImIp3nS
proto	proto	k8xC
vhodné	vhodný	k2eAgNnSc1d1
používat	používat	k5eAaImF
tyto	tento	k3xDgInPc4
standardizované	standardizovaný	k2eAgInPc4d1
komponenty	komponent	k1gInPc4
i	i	k9
v	v	k7c6
aplikacích	aplikace	k1gFnPc6
třetích	třetí	k4xOgInPc2
stran	strana	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c4
hlavní	hlavní	k2eAgInSc4d1
controller	controller	k1gInSc4
patří	patřit	k5eAaImIp3nS
Adresář	adresář	k1gInSc1
(	(	kIx(
<g/>
zobrazení	zobrazení	k1gNnSc1
kontaktních	kontaktní	k2eAgFnPc2d1
informací	informace	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Kalendář	kalendář	k1gInSc1
<g/>
,	,	kIx,
Psaní	psaní	k1gNnSc1
e-mailu	e-mail	k1gInSc2
<g/>
/	/	kIx~
<g/>
SMS	SMS	kA
<g/>
,	,	kIx,
Otevření	otevření	k1gNnSc1
souboru	soubor	k1gInSc2
<g/>
,	,	kIx,
Výběr	výběr	k1gInSc1
obrázku	obrázek	k1gInSc2
z	z	k7c2
knihovny	knihovna	k1gFnSc2
<g/>
/	/	kIx~
<g/>
fotoaparátu	fotoaparát	k1gInSc2
apod.	apod.	kA
</s>
<s>
Podpora	podpora	k1gFnSc1
externích	externí	k2eAgNnPc2d1
zobrazovacích	zobrazovací	k2eAgNnPc2d1
zařízení	zařízení	k1gNnPc2
</s>
<s>
Od	od	k7c2
verze	verze	k1gFnSc2
3.2	3.2	k4
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
připojit	připojit	k5eAaPmF
pomocí	pomocí	k7c2
speciálního	speciální	k2eAgNnSc2d1
příslušenství	příslušenství	k1gNnSc2
externí	externí	k2eAgNnSc4d1
zobrazovací	zobrazovací	k2eAgNnSc4d1
zařízení	zařízení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takovéto	takovýto	k3xDgNnSc1
externí	externí	k2eAgNnSc1d1
zařízení	zařízení	k1gNnSc1
se	se	k3xPyFc4
pak	pak	k6eAd1
používá	používat	k5eAaImIp3nS
jako	jako	k9
druhé	druhý	k4xOgNnSc4
okno	okno	k1gNnSc4
<g/>
,	,	kIx,
kam	kam	k6eAd1
aplikace	aplikace	k1gFnSc1
zobrazuje	zobrazovat	k5eAaImIp3nS
obsah	obsah	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
používat	používat	k5eAaImF
režim	režim	k1gInSc4
zrcadlení	zrcadlení	k1gNnSc2
(	(	kIx(
<g/>
mirror	mirror	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
nutné	nutný	k2eAgNnSc1d1
zobrazovat	zobrazovat	k5eAaImF
stejný	stejný	k2eAgInSc1d1
obsah	obsah	k1gInSc1
do	do	k7c2
dvou	dva	k4xCgNnPc2
oken	okno	k1gNnPc2
zároveň	zároveň	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Frameworky	Frameworek	k1gInPc1
dostupné	dostupný	k2eAgInPc1d1
v	v	k7c4
Cocoa	Cocoum	k1gNnPc4
Touch	Toucha	k1gFnPc2
</s>
<s>
Framework	Framework	k1gInSc1
</s>
<s>
Použití	použití	k1gNnSc1
</s>
<s>
Address	Address	k6eAd1
Book	Book	k1gInSc1
UI	UI	kA
Framework	Framework	k1gInSc1
</s>
<s>
Tento	tento	k3xDgInSc1
framework	framework	k1gInSc1
obsahuje	obsahovat	k5eAaImIp3nS
standardizované	standardizovaný	k2eAgNnSc4d1
rozhraní	rozhraní	k1gNnSc4
pro	pro	k7c4
zobrazování	zobrazování	k1gNnSc4
a	a	k8xC
úpravu	úprava	k1gFnSc4
kontaktních	kontaktní	k2eAgFnPc2d1
informací	informace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Event	Event	k1gInSc1
Kit	kit	k1gInSc1
UI	UI	kA
Framework	Framework	k1gInSc1
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS
controllery	controller	k1gInPc4
pro	pro	k7c4
práci	práce	k1gFnSc4
s	s	k7c7
událostmi	událost	k1gFnPc7
(	(	kIx(
<g/>
položky	položka	k1gFnPc1
v	v	k7c6
kalendáři	kalendář	k1gInSc6
<g/>
)	)	kIx)
</s>
<s>
iAd	iAd	k?
Framework	Framework	k1gInSc1
</s>
<s>
Umožňuje	umožňovat	k5eAaImIp3nS
zobrazení	zobrazení	k1gNnSc1
bannerové	bannerové	k2eAgFnSc2d1
reklamy	reklama	k1gFnSc2
v	v	k7c6
aplikaci	aplikace	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Game	game	k1gInSc1
Kit	kit	k1gInSc1
Framework	Framework	k1gInSc4
</s>
<s>
Poskytuje	poskytovat	k5eAaImIp3nS
podporu	podpora	k1gFnSc4
pro	pro	k7c4
peer	peer	k1gMnSc1
to	ten	k3xDgNnSc1
peer	peer	k1gMnSc1
komunikaci	komunikace	k1gFnSc4
mezi	mezi	k7c7
zařízeními	zařízení	k1gNnPc7
pomocí	pomocí	k7c2
protokolu	protokol	k1gInSc2
Bonjour	Bonjoura	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Map	mapa	k1gFnPc2
Kit	kit	k1gInSc1
Framework	Framework	k1gInSc1
</s>
<s>
Umožňuje	umožňovat	k5eAaImIp3nS
používat	používat	k5eAaImF
mapovou	mapový	k2eAgFnSc4d1
komponentu	komponenta	k1gFnSc4
pro	pro	k7c4
zobrazovaní	zobrazovaný	k2eAgMnPc1d1
map	mapa	k1gFnPc2
<g/>
,	,	kIx,
případně	případně	k6eAd1
dalších	další	k2eAgFnPc2d1
informací	informace	k1gFnPc2
o	o	k7c4
umístění	umístění	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Message	Message	k1gInSc1
UI	UI	kA
Framework	Framework	k1gInSc1
</s>
<s>
Umožňuje	umožňovat	k5eAaImIp3nS
vytvářet	vytvářet	k5eAaImF
a	a	k8xC
odesílat	odesílat	k5eAaImF
e-maily	e-mail	k1gInPc4
a	a	k8xC
SMS	SMS	kA
zprávy	zpráva	k1gFnSc2
pomocí	pomocí	k7c2
standardizovaných	standardizovaný	k2eAgNnPc2d1
uživatelských	uživatelský	k2eAgNnPc2d1
rozhraní	rozhraní	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Media	medium	k1gNnPc1
layer	layero	k1gNnPc2
</s>
<s>
Tato	tento	k3xDgFnSc1
vrstva	vrstva	k1gFnSc1
umožňuje	umožňovat	k5eAaImIp3nS
vytváření	vytváření	k1gNnSc4
graficky	graficky	k6eAd1
a	a	k8xC
zvukově	zvukově	k6eAd1
propracovaných	propracovaný	k2eAgFnPc2d1
aplikací	aplikace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
technologie	technologie	k1gFnPc1
umožňují	umožňovat	k5eAaImIp3nP
plynulé	plynulý	k2eAgNnSc4d1
přehrávání	přehrávání	k1gNnSc4
animací	animace	k1gFnPc2
<g/>
,	,	kIx,
videí	video	k1gNnPc2
a	a	k8xC
zvuků	zvuk	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Grafické	grafický	k2eAgFnPc1d1
technologie	technologie	k1gFnPc1
</s>
<s>
Kvalitní	kvalitní	k2eAgFnSc1d1
grafika	grafika	k1gFnSc1
je	být	k5eAaImIp3nS
důležitou	důležitý	k2eAgFnSc7d1
součástí	součást	k1gFnSc7
systému	systém	k1gInSc2
iOS	iOS	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejjednoduššího	jednoduchý	k2eAgNnSc2d3
a	a	k8xC
nejefektivnějšího	efektivní	k2eAgNnSc2d3
vytváření	vytváření	k1gNnSc2
aplikací	aplikace	k1gFnPc2
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
dosáhnout	dosáhnout	k5eAaPmF
používáním	používání	k1gNnSc7
standardních	standardní	k2eAgInPc2d1
předrenderovaných	předrenderovaný	k2eAgInPc2d1
obrázků	obrázek	k1gInPc2
a	a	k8xC
komponent	komponenta	k1gFnPc2
a	a	k8xC
nechat	nechat	k5eAaPmF
systém	systém	k1gInSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
vše	všechen	k3xTgNnSc4
obstaral	obstarat	k5eAaPmAgMnS
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
v	v	k7c6
některých	některý	k3yIgFnPc6
situacích	situace	k1gFnPc6
není	být	k5eNaImIp3nS
toto	tento	k3xDgNnSc1
řešení	řešení	k1gNnSc1
možné	možný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
takových	takový	k3xDgFnPc6
situacích	situace	k1gFnPc6
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
použít	použít	k5eAaPmF
následující	následující	k2eAgFnPc4d1
technologie	technologie	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Technologie	technologie	k1gFnSc1
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Core	Core	k1gInSc1
Graphics	Graphics	k1gInSc1
(	(	kIx(
<g/>
Quartz	Quartz	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Stará	starat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c6
kreslení	kreslení	k1gNnSc6
nativních	nativní	k2eAgInPc2d1
2D	2D	k4
vektorů	vektor	k1gInPc2
a	a	k8xC
renderování	renderování	k1gNnSc2
obrázků	obrázek	k1gInPc2
</s>
<s>
Core	Core	k6eAd1
Animation	Animation	k1gInSc1
</s>
<s>
Pokročilá	pokročilý	k2eAgFnSc1d1
podpora	podpora	k1gFnSc1
animací	animace	k1gFnPc2
</s>
<s>
OpenGL	OpenGL	k?
ES	ES	kA
</s>
<s>
Hardwarově	hardwarově	k6eAd1
akcelerované	akcelerovaný	k2eAgNnSc1d1
vykreslování	vykreslování	k1gNnSc1
2	#num#	k4
<g/>
D	D	kA
<g/>
/	/	kIx~
<g/>
3	#num#	k4
<g/>
D	D	kA
objektů	objekt	k1gInPc2
</s>
<s>
Core	Core	k6eAd1
Text	text	k1gInSc1
</s>
<s>
Sofistikovaný	sofistikovaný	k2eAgMnSc1d1
engine	enginout	k5eAaPmIp3nS
pro	pro	k7c4
vykreslování	vykreslování	k1gNnSc4
textu	text	k1gInSc2
</s>
<s>
Image	image	k1gInSc1
I	i	k9
<g/>
/	/	kIx~
<g/>
O	o	k7c6
</s>
<s>
Čtení	čtení	k1gNnSc1
a	a	k8xC
zápis	zápis	k1gInSc1
většiny	většina	k1gFnSc2
rozšířených	rozšířený	k2eAgInPc2d1
grafických	grafický	k2eAgInPc2d1
formátů	formát	k1gInPc2
</s>
<s>
The	The	k?
Assets	Assets	k1gInSc4
Library	Librara	k1gFnSc2
framework	framework	k1gInSc1
</s>
<s>
Přístup	přístup	k1gInSc1
k	k	k7c3
obrázkové	obrázkový	k2eAgFnSc3d1
knihovně	knihovna	k1gFnSc3
uživatele	uživatel	k1gMnSc2
</s>
<s>
Technologie	technologie	k1gFnSc1
pro	pro	k7c4
zvuk	zvuk	k1gInSc4
</s>
<s>
Tyto	tento	k3xDgFnPc1
technologie	technologie	k1gFnPc1
umožňují	umožňovat	k5eAaImIp3nP
přehrávat	přehrávat	k5eAaImF
kvalitní	kvalitní	k2eAgInPc4d1
audiozáznamy	audiozáznam	k1gInPc4
a	a	k8xC
používat	používat	k5eAaImF
vibrace	vibrace	k1gFnPc4
(	(	kIx(
<g/>
na	na	k7c6
zařízeních	zařízení	k1gNnPc6
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
to	ten	k3xDgNnSc1
umožňují	umožňovat	k5eAaImIp3nP
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Systém	systém	k1gInSc1
nabízí	nabízet	k5eAaImIp3nS
několik	několik	k4yIc4
možností	možnost	k1gFnPc2
<g/>
,	,	kIx,
jak	jak	k6eAd1
přehrávat	přehrávat	k5eAaImF
či	či	k8xC
zaznamenávat	zaznamenávat	k5eAaImF
zvuk	zvuk	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vysokoúrovňové	vysokoúrovňový	k2eAgInPc1d1
frameworky	frameworek	k1gInPc1
velice	velice	k6eAd1
zjednodušují	zjednodušovat	k5eAaImIp3nP
vývoj	vývoj	k1gInSc4
<g/>
,	,	kIx,
avšak	avšak	k8xC
neumožňují	umožňovat	k5eNaImIp3nP
takovou	takový	k3xDgFnSc4
míru	míra	k1gFnSc4
ovlivňování	ovlivňování	k1gNnSc4
operací	operace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následující	následující	k2eAgInPc1d1
frameworky	frameworek	k1gInPc1
jsou	být	k5eAaImIp3nP
seřazeny	seřadit	k5eAaPmNgInP
a	a	k8xC
od	od	k7c2
vysokoúrovňových	vysokoúrovňový	k2eAgInPc2d1
po	po	k7c6
nízkoúrovňové	nízkoúrovňová	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
The	The	k?
Media	medium	k1gNnPc1
Player	Player	k1gInSc1
framework	framework	k1gInSc1
–	–	k?
poskytuje	poskytovat	k5eAaImIp3nS
přístup	přístup	k1gInSc1
k	k	k7c3
iTunes	iTunes	k1gMnSc1
knihovně	knihovně	k6eAd1
a	a	k8xC
přehrávaní	přehrávaný	k2eAgMnPc1d1
skladeb	skladba	k1gFnPc2
</s>
<s>
AV	AV	kA
Foundation	Foundation	k1gInSc1
–	–	k?
poskytuje	poskytovat	k5eAaImIp3nS
sadu	sada	k1gFnSc4
Objective-C	Objective-C	k1gFnSc2
rozhraní	rozhraní	k1gNnSc2
pro	pro	k7c4
správu	správa	k1gFnSc4
přehrávání	přehrávání	k1gNnSc2
a	a	k8xC
záznamu	záznam	k1gInSc2
zvuku	zvuk	k1gInSc2
</s>
<s>
OpenAL	OpenAL	k?
–	–	k?
poskytuje	poskytovat	k5eAaImIp3nS
sadu	sada	k1gFnSc4
multiplatformních	multiplatformní	k2eAgNnPc2d1
rozhraní	rozhraní	k1gNnPc2
pro	pro	k7c4
pozicovaný	pozicovaný	k2eAgInSc4d1
zvuk	zvuk	k1gInSc4
(	(	kIx(
<g/>
3	#num#	k4
<g/>
D	D	kA
<g/>
)	)	kIx)
</s>
<s>
Core	Core	k6eAd1
Audio	audio	k2eAgInSc1d1
framework	framework	k1gInSc1
–	–	k?
poskytuje	poskytovat	k5eAaImIp3nS
rozhraní	rozhraní	k1gNnSc4
pro	pro	k7c4
přehrávání	přehrávání	k1gNnSc4
a	a	k8xC
záznam	záznam	k1gInSc4
zvuků	zvuk	k1gInPc2
<g/>
,	,	kIx,
umožňuje	umožňovat	k5eAaImIp3nS
přehrávat	přehrávat	k5eAaImF
systémové	systémový	k2eAgInPc4d1
zvuky	zvuk	k1gInPc4
a	a	k8xC
upozornění	upozornění	k1gNnPc4
<g/>
,	,	kIx,
vibrovat	vibrovat	k5eAaImF
a	a	k8xC
přehrávat	přehrávat	k5eAaImF
vícekanálový	vícekanálový	k2eAgInSc4d1
či	či	k8xC
streamovaný	streamovaný	k2eAgInSc4d1
zvuk	zvuk	k1gInSc4
</s>
<s>
iOS	iOS	k?
podporuje	podporovat	k5eAaImIp3nS
tyto	tento	k3xDgInPc4
zvukové	zvukový	k2eAgInPc4d1
formáty	formát	k1gInPc4
<g/>
:	:	kIx,
AAC	AAC	kA
<g/>
,	,	kIx,
ALAC	ALAC	kA
<g/>
,	,	kIx,
A-law	A-law	k1gFnSc1
<g/>
,	,	kIx,
IMA	IMA	kA
<g/>
/	/	kIx~
<g/>
ADPCM	ADPCM	kA
(	(	kIx(
<g/>
IMA	IMA	kA
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Linear	Linear	k1gMnSc1
PCM	PCM	kA
<g/>
,	,	kIx,
µ	µ	k?
<g/>
,	,	kIx,
DVI	DVI	kA
<g/>
/	/	kIx~
<g/>
Intel	Intel	kA
IMA	IMA	kA
ADPCM	ADPCM	kA
<g/>
,	,	kIx,
Microsoft	Microsoft	kA
GSM	GSM	kA
6.10	6.10	k4
<g/>
,	,	kIx,
AES3-2003	AES3-2003	k1gFnSc1
</s>
<s>
Technologie	technologie	k1gFnSc1
pro	pro	k7c4
video	video	k1gNnSc4
</s>
<s>
Tyto	tento	k3xDgFnPc1
technologie	technologie	k1gFnPc1
umožňují	umožňovat	k5eAaImIp3nP
přehrávat	přehrávat	k5eAaImF
kvalitní	kvalitní	k2eAgNnSc4d1
video	video	k1gNnSc4
záznam	záznam	k1gInSc1
nebo	nebo	k8xC
je	být	k5eAaImIp3nS
pořizovat	pořizovat	k5eAaImF
(	(	kIx(
<g/>
na	na	k7c6
zařízeních	zařízení	k1gNnPc6
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
to	ten	k3xDgNnSc1
umožňují	umožňovat	k5eAaImIp3nP
<g/>
)	)	kIx)
a	a	k8xC
pracovat	pracovat	k5eAaImF
s	s	k7c7
ním	on	k3xPp3gMnSc7
v	v	k7c4
aplikaci	aplikace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Systém	systém	k1gInSc1
nabízí	nabízet	k5eAaImIp3nS
několik	několik	k4yIc4
možností	možnost	k1gFnPc2
<g/>
,	,	kIx,
jak	jak	k6eAd1
přehrávat	přehrávat	k5eAaImF
či	či	k8xC
zaznamenávat	zaznamenávat	k5eAaImF
video	video	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vysokoúrovňové	vysokoúrovňový	k2eAgInPc1d1
frameworky	frameworek	k1gInPc1
velice	velice	k6eAd1
zjednodušují	zjednodušovat	k5eAaImIp3nP
vývoj	vývoj	k1gInSc4
<g/>
,	,	kIx,
avšak	avšak	k8xC
neumožňují	umožňovat	k5eNaImIp3nP
takovou	takový	k3xDgFnSc4
míru	míra	k1gFnSc4
ovlivňování	ovlivňování	k1gNnSc4
operací	operace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následující	následující	k2eAgInPc1d1
frameworky	frameworek	k1gInPc1
jsou	být	k5eAaImIp3nP
seřazeny	seřadit	k5eAaPmNgInP
od	od	k7c2
vysokoúrovňových	vysokoúrovňový	k2eAgFnPc2d1
po	po	k7c6
nízkoúrovňové	nízkoúrovňová	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Media	medium	k1gNnPc1
Player	Playero	k1gNnPc2
framework	framework	k1gInSc1
–	–	k?
umožňuje	umožňovat	k5eAaImIp3nS
přehrávání	přehrávání	k1gNnSc4
videí	video	k1gNnPc2
(	(	kIx(
<g/>
buď	buď	k8xC
přes	přes	k7c4
celou	celý	k2eAgFnSc4d1
obrazovku	obrazovka	k1gFnSc4
<g/>
,	,	kIx,
nebo	nebo	k8xC
pouze	pouze	k6eAd1
částečně	částečně	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
AV	AV	kA
Foundation	Foundation	k1gInSc1
–	–	k?
sada	sada	k1gFnSc1
Objective-C	Objective-C	k1gFnSc2
rozhraní	rozhraní	k1gNnSc2
pro	pro	k7c4
záznam	záznam	k1gInSc4
a	a	k8xC
přehrávání	přehrávání	k1gNnSc4
videa	video	k1gNnSc2
</s>
<s>
Core	Core	k1gNnSc1
Media	medium	k1gNnSc2
–	–	k?
popisuje	popisovat	k5eAaImIp3nS
nízkoúrovňové	nízkoúrovňový	k2eAgInPc4d1
typy	typ	k1gInPc4
a	a	k8xC
rozhraní	rozhraní	k1gNnSc4
používané	používaný	k2eAgNnSc4d1
ve	v	k7c6
vysoko	vysoko	k6eAd1
úrovňových	úrovňový	k2eAgInPc6d1
frameworcích	frameworek	k1gInPc6
</s>
<s>
Core	Core	k6eAd1
Services	Services	k1gMnSc1
layer	layer	k1gMnSc1
</s>
<s>
Vysokoúrovňové	vysokoúrovňový	k2eAgFnPc1d1
služby	služba	k1gFnPc1
poskytované	poskytovaný	k2eAgFnSc2d1
Core	Cor	k1gFnSc2
Services	Services	k1gMnSc1
</s>
<s>
Block	Block	k6eAd1
objekty	objekt	k1gInPc1
</s>
<s>
Od	od	k7c2
verze	verze	k1gFnSc2
4.0	4.0	k4
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
používat	používat	k5eAaImF
objekty	objekt	k1gInPc4
typu	typ	k1gInSc2
Block	Blocko	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
jazykový	jazykový	k2eAgInSc4d1
konstrukt	konstrukt	k1gInSc4
jazyka	jazyk	k1gInSc2
C	C	kA
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
možný	možný	k2eAgInSc4d1
používat	používat	k5eAaImF
ve	v	k7c6
stávajícím	stávající	k2eAgInSc6d1
C	C	kA
nebo	nebo	k8xC
Objective-C	Objective-C	k1gFnSc2
kódu	kód	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Block	Block	k1gInSc1
objekt	objekt	k1gInSc4
reprezentuje	reprezentovat	k5eAaImIp3nS
anonymní	anonymní	k2eAgFnSc4d1
funkci	funkce	k1gFnSc4
a	a	k8xC
související	související	k2eAgNnPc4d1
data	datum	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takový	takový	k3xDgInSc1
konstrukt	konstrukt	k1gInSc1
je	být	k5eAaImIp3nS
v	v	k7c6
jiných	jiný	k2eAgInPc6d1
jazycích	jazyk	k1gInPc6
často	často	k6eAd1
nazýván	nazýván	k2eAgMnSc1d1
closure	closur	k1gMnSc5
nebo	nebo	k8xC
lambda	lambda	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Block	Blocka	k1gFnPc2
objekty	objekt	k1gInPc1
se	se	k3xPyFc4
hodí	hodit	k5eAaPmIp3nP,k5eAaImIp3nP
jako	jako	k9
callback	callback	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Grand	grand	k1gMnSc1
Central	Central	k1gMnSc1
Dispatch	Dispatch	k1gMnSc1
</s>
<s>
Ve	v	k7c6
verzi	verze	k1gFnSc6
4.0	4.0	k4
byla	být	k5eAaImAgFnS
přidána	přidán	k2eAgFnSc1d1
technologie	technologie	k1gFnSc1
Grand	grand	k1gMnSc1
Central	Central	k1gMnSc1
Dispatch	Dispatcha	k1gFnPc2
postavená	postavený	k2eAgFnSc1d1
na	na	k7c4
BSD	BSD	kA
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
umožňuje	umožňovat	k5eAaImIp3nS
správu	správa	k1gFnSc4
úloh	úloha	k1gFnPc2
v	v	k7c6
aplikaci	aplikace	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
GCD	GCD	kA
kombinuje	kombinovat	k5eAaImIp3nS
asynchronní	asynchronní	k2eAgInSc1d1
model	model	k1gInSc1
programování	programování	k1gNnSc2
s	s	k7c7
vysoce	vysoce	k6eAd1
optimalizovaným	optimalizovaný	k2eAgNnSc7d1
jádrem	jádro	k1gNnSc7
a	a	k8xC
poskytuje	poskytovat	k5eAaImIp3nS
tak	tak	k6eAd1
jednoduchou	jednoduchý	k2eAgFnSc4d1
a	a	k8xC
zároveň	zároveň	k6eAd1
efektivní	efektivní	k2eAgFnSc4d1
alternativu	alternativa	k1gFnSc4
k	k	k7c3
vláknovému	vláknový	k2eAgNnSc3d1
programování	programování	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s>
In	In	k?
App	App	k1gMnSc5
Purchase	Purchas	k1gMnSc5
</s>
<s>
Od	od	k7c2
verze	verze	k1gFnSc2
3.0	3.0	k4
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
provádět	provádět	k5eAaImF
platby	platba	k1gFnPc4
uvnitř	uvnitř	k7c2
aplikace	aplikace	k1gFnSc2
za	za	k7c4
dodatečný	dodatečný	k2eAgInSc4d1
obsah	obsah	k1gInSc4
či	či	k8xC
zrušení	zrušení	k1gNnSc4
reklamy	reklama	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Lokační	lokační	k2eAgFnPc1d1
služby	služba	k1gFnPc1
</s>
<s>
Umožňují	umožňovat	k5eAaImIp3nP
sledovat	sledovat	k5eAaImF
aktuální	aktuální	k2eAgFnSc4d1
polohu	poloha	k1gFnSc4
uživatele	uživatel	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Služby	služba	k1gFnPc1
využívají	využívat	k5eAaImIp3nP,k5eAaPmIp3nP
k	k	k7c3
určení	určení	k1gNnSc3
polohy	poloha	k1gFnSc2
veškerý	veškerý	k3xTgInSc4
dostupný	dostupný	k2eAgInSc4d1
hardware	hardware	k1gInSc4
(	(	kIx(
<g/>
Wi-Fi	Wi-Fe	k1gFnSc4
<g/>
,	,	kIx,
telefonní	telefonní	k2eAgFnSc4d1
síť	síť	k1gFnSc4
<g/>
,	,	kIx,
GPS	GPS	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aplikace	aplikace	k1gFnPc1
tak	tak	k8xS,k8xC
mohou	moct	k5eAaImIp3nP
uživateli	uživatel	k1gMnSc3
nabídnout	nabídnout	k5eAaPmF
data	datum	k1gNnPc4
relevantní	relevantní	k2eAgNnPc4d1
k	k	k7c3
jeho	jeho	k3xOp3gFnSc3
poloze	poloha	k1gFnSc3
(	(	kIx(
<g/>
např.	např.	kA
nejbližší	blízký	k2eAgFnSc1d3
restaurace	restaurace	k1gFnSc1
apod.	apod.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
SQLite	SQLit	k1gMnSc5
</s>
<s>
Odlehčená	odlehčený	k2eAgFnSc1d1
SQL	SQL	kA
databáze	databáze	k1gFnSc1
umožňuje	umožňovat	k5eAaImIp3nS
ukládání	ukládání	k1gNnSc1
uživatelských	uživatelský	k2eAgNnPc2d1
dat	datum	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Podpora	podpora	k1gFnSc1
XML	XML	kA
</s>
<s>
Podpora	podpora	k1gFnSc1
pro	pro	k7c4
zpracování	zpracování	k1gNnSc4
XML	XML	kA
dokumentů	dokument	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Čas	čas	k1gInSc1
u	u	k7c2
Obrazovky	obrazovka	k1gFnSc2
</s>
<s>
Sleduje	sledovat	k5eAaImIp3nS
dobu	doba	k1gFnSc4
u	u	k7c2
aplikací	aplikace	k1gFnPc2
a	a	k8xC
obrazovky	obrazovka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Poskytované	poskytovaný	k2eAgFnPc1d1
frameworky	frameworka	k1gFnPc1
</s>
<s>
Framework	Framework	k1gInSc1
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Address	Address	k6eAd1
Book	Book	k1gInSc1
</s>
<s>
Přístup	přístup	k1gInSc1
k	k	k7c3
databázi	databáze	k1gFnSc3
kontaktů	kontakt	k1gInPc2
uživatele	uživatel	k1gMnPc4
</s>
<s>
CFNetwork	CFNetwork	k1gInSc1
</s>
<s>
Komunikace	komunikace	k1gFnSc1
pomocí	pomocí	k7c2
síťového	síťový	k2eAgNnSc2d1
rozhraní	rozhraní	k1gNnSc2
</s>
<s>
Core	Core	k1gFnSc1
Data	datum	k1gNnSc2
</s>
<s>
Ukládání	ukládání	k1gNnSc1
strukturovaných	strukturovaný	k2eAgNnPc2d1
dat	datum	k1gNnPc2
a	a	k8xC
mapování	mapování	k1gNnSc2
na	na	k7c4
programové	programový	k2eAgInPc4d1
objekty	objekt	k1gInPc4
</s>
<s>
Core	Core	k6eAd1
Foundation	Foundation	k1gInSc1
</s>
<s>
Základní	základní	k2eAgFnPc4d1
služby	služba	k1gFnPc4
pro	pro	k7c4
práci	práce	k1gFnSc4
s	s	k7c7
řetězci	řetězec	k1gInPc7
<g/>
,	,	kIx,
daty	datum	k1gNnPc7
<g/>
,	,	kIx,
URL	URL	kA
<g/>
,	,	kIx,
vlákny	vlákna	k1gFnPc1
<g/>
,	,	kIx,
porty	port	k1gInPc1
<g/>
,	,	kIx,
…	…	k?
</s>
<s>
Core	Core	k6eAd1
Location	Location	k1gInSc1
</s>
<s>
Hledání	hledání	k1gNnSc1
aktuální	aktuální	k2eAgFnSc2d1
geografické	geografický	k2eAgFnSc2d1
polohy	poloha	k1gFnSc2
uživatele	uživatel	k1gMnSc2
</s>
<s>
Core	Core	k1gFnSc1
Media	medium	k1gNnSc2
</s>
<s>
Nízkoúrovňový	Nízkoúrovňový	k2eAgInSc1d1
přístup	přístup	k1gInSc1
k	k	k7c3
audio	audio	k2eAgFnSc3d1
<g/>
/	/	kIx~
<g/>
video	video	k1gNnSc1
</s>
<s>
Core	Core	k1gFnSc1
Telephony	Telephona	k1gFnSc2
</s>
<s>
Přístup	přístup	k1gInSc1
k	k	k7c3
informacím	informace	k1gFnPc3
o	o	k7c6
mobilní	mobilní	k2eAgFnSc6d1
síti	síť	k1gFnSc6
</s>
<s>
Event	Event	k1gInSc1
Kit	kit	k1gInSc1
</s>
<s>
Přístup	přístup	k1gInSc1
k	k	k7c3
událostem	událost	k1gFnPc3
v	v	k7c6
kalendáři	kalendář	k1gInSc6
</s>
<s>
Foundation	Foundation	k1gInSc1
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS
Objetive-C	Objetive-C	k1gFnSc4
wrappery	wrappera	k1gFnSc2
pro	pro	k7c4
funkce	funkce	k1gFnPc4
z	z	k7c2
Core	Cor	k1gFnSc2
Foundation	Foundation	k1gInSc1
</s>
<s>
Mobile	mobile	k1gNnSc1
Core	Cor	k1gFnSc2
Services	Servicesa	k1gFnPc2
</s>
<s>
Nízkoúrovňové	Nízkoúrovňový	k2eAgInPc1d1
typy	typ	k1gInPc1
pro	pro	k7c4
UTI	UTI	kA
</s>
<s>
Quick	Quick	k1gMnSc1
Look	Look	k1gMnSc1
</s>
<s>
Poskytuje	poskytovat	k5eAaImIp3nS
možnost	možnost	k1gFnSc4
vytvořit	vytvořit	k5eAaPmF
náhled	náhled	k1gInSc4
obsahu	obsah	k1gInSc2
souboru	soubor	k1gInSc2
<g/>
,	,	kIx,
se	s	k7c7
kterým	který	k3yQgInSc7,k3yRgInSc7,k3yIgInSc7
samotná	samotný	k2eAgFnSc1d1
aplikace	aplikace	k1gFnSc1
není	být	k5eNaImIp3nS
schopna	schopen	k2eAgFnSc1d1
pracovat	pracovat	k5eAaImF
</s>
<s>
Store	Stor	k1gMnSc5
Kit	kit	k1gInSc4
</s>
<s>
Poskytuje	poskytovat	k5eAaImIp3nS
přístup	přístup	k1gInSc1
k	k	k7c3
iTunes	iTunes	k1gMnSc1
Storu	Stor	k1gInSc2
a	a	k8xC
možnost	možnost	k1gFnSc4
nákupů	nákup	k1gInPc2
</s>
<s>
System	Syst	k1gInSc7
Configuration	Configuration	k1gInSc4
</s>
<s>
Umožňuje	umožňovat	k5eAaImIp3nS
zjišťovat	zjišťovat	k5eAaImF
dostupnost	dostupnost	k1gFnSc4
připojení	připojení	k1gNnSc2
k	k	k7c3
internetu	internet	k1gInSc3
a	a	k8xC
jeho	jeho	k3xOp3gNnSc3
nastavení	nastavení	k1gNnSc3
</s>
<s>
Vrstva	vrstva	k1gFnSc1
Core	Cor	k1gFnSc2
OS	OS	kA
</s>
<s>
Vrstva	vrstva	k1gFnSc1
Core	Cor	k1gFnSc2
OS	OS	kA
poskytuje	poskytovat	k5eAaImIp3nS
nízkoúrovňové	nízkoúrovňový	k2eAgFnPc4d1
funkce	funkce	k1gFnPc4
ostatním	ostatní	k2eAgFnPc3d1
technologiím	technologie	k1gFnPc3
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
jsou	být	k5eAaImIp3nP
na	na	k7c6
ní	on	k3xPp3gFnSc6
postaveny	postavit	k5eAaPmNgInP
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
když	když	k8xS
nejsou	být	k5eNaImIp3nP
většinou	většina	k1gFnSc7
v	v	k7c6
aplikacích	aplikace	k1gFnPc6
využívány	využíván	k2eAgMnPc4d1
přímo	přímo	k6eAd1
<g/>
,	,	kIx,
velice	velice	k6eAd1
pravděpodobně	pravděpodobně	k6eAd1
je	on	k3xPp3gFnPc4
využívají	využívat	k5eAaPmIp3nP,k5eAaImIp3nP
vysokoúrovňové	vysokoúrovňový	k2eAgFnPc4d1
komponenty	komponenta	k1gFnPc4
systému	systém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Accelerate	Accelerat	k1gMnSc5
Framework	Framework	k1gInSc4
</s>
<s>
Poskytuje	poskytovat	k5eAaImIp3nS
rozhraní	rozhraní	k1gNnSc1
pro	pro	k7c4
práci	práce	k1gFnSc4
s	s	k7c7
matematickými	matematický	k2eAgFnPc7d1
funkcemi	funkce	k1gFnPc7
(	(	kIx(
<g/>
obdoba	obdoba	k1gFnSc1
java	java	k1gMnSc1
<g/>
.	.	kIx.
<g/>
math	math	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
velkými	velký	k2eAgNnPc7d1
čísly	číslo	k1gNnPc7
<g/>
,	,	kIx,
výpočty	výpočet	k1gInPc7
DSP	DSP	kA
apod.	apod.	kA
Výhodou	výhoda	k1gFnSc7
tohoto	tento	k3xDgInSc2
frameworku	frameworek	k1gInSc2
oproti	oproti	k7c3
vlastní	vlastní	k2eAgFnSc3d1
implementaci	implementace	k1gFnSc3
těchto	tento	k3xDgFnPc2
funkcí	funkce	k1gFnPc2
je	být	k5eAaImIp3nS
fakt	fakt	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
různých	různý	k2eAgFnPc6d1
verzích	verze	k1gFnPc6
iOS	iOS	k?
určených	určený	k2eAgInPc2d1
pro	pro	k7c4
různá	různý	k2eAgNnPc4d1
zařízení	zařízení	k1gNnPc4
je	být	k5eAaImIp3nS
tento	tento	k3xDgInSc1
framework	framework	k1gInSc1
optimalizován	optimalizován	k2eAgInSc1d1
pro	pro	k7c4
daný	daný	k2eAgInSc4d1
hardware	hardware	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
External	Externat	k5eAaImAgMnS,k5eAaPmAgMnS
Accessory	Accessor	k1gMnPc4
Framework	Framework	k1gInSc4
</s>
<s>
Tento	tento	k3xDgInSc1
framework	framework	k1gInSc1
poskytuje	poskytovat	k5eAaImIp3nS
podporu	podpora	k1gFnSc4
pro	pro	k7c4
komunikaci	komunikace	k1gFnSc4
s	s	k7c7
externími	externí	k2eAgNnPc7d1
zařízeními	zařízení	k1gNnPc7
připojenými	připojený	k2eAgFnPc7d1
přes	přes	k7c4
Bluetooth	Bluetooth	k1gInSc4
nebo	nebo	k8xC
třicetipinový	třicetipinový	k2eAgInSc4d1
konektor	konektor	k1gInSc4
zařízení	zařízení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Framework	Framework	k1gInSc4
také	také	k9
umožňuje	umožňovat	k5eAaImIp3nS
získávat	získávat	k5eAaImF
informace	informace	k1gFnPc4
o	o	k7c6
dostupném	dostupný	k2eAgNnSc6d1
příslušenství	příslušenství	k1gNnSc6
a	a	k8xC
navázat	navázat	k5eAaPmF
komunikaci	komunikace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Security	Securita	k1gFnPc1
Framework	Framework	k1gInSc1
</s>
<s>
Kromě	kromě	k7c2
vestavěných	vestavěný	k2eAgFnPc2d1
bezpečnostních	bezpečnostní	k2eAgFnPc2d1
vlastností	vlastnost	k1gFnPc2
iOS	iOS	k?
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
využít	využít	k5eAaPmF
Security	Securita	k1gFnPc4
framework	framework	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
dokáže	dokázat	k5eAaPmIp3nS
zaručit	zaručit	k5eAaPmF
bezpečnost	bezpečnost	k1gFnSc4
citlivých	citlivý	k2eAgNnPc2d1
dat	datum	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
dispozici	dispozice	k1gFnSc3
jsou	být	k5eAaImIp3nP
rozhraní	rozhraní	k1gNnSc4
pro	pro	k7c4
certifikáty	certifikát	k1gInPc4
<g/>
,	,	kIx,
soukromé	soukromý	k2eAgInPc4d1
a	a	k8xC
veřejné	veřejný	k2eAgInPc4d1
klíče	klíč	k1gInPc4
<g/>
,	,	kIx,
generování	generování	k1gNnSc1
kryptografických	kryptografický	k2eAgNnPc2d1
pseudonáhodných	pseudonáhodný	k2eAgNnPc2d1
čísel	číslo	k1gNnPc2
apod.	apod.	kA
Dále	daleko	k6eAd2
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
ukládat	ukládat	k5eAaImF
data	datum	k1gNnPc4
do	do	k7c2
zašifrovaného	zašifrovaný	k2eAgNnSc2d1
centrálního	centrální	k2eAgNnSc2d1
úložiště	úložiště	k1gNnSc2
svazku	svazek	k1gInSc2
klíčů	klíč	k1gInPc2
(	(	kIx(
<g/>
keychain	keychain	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
úložišti	úložiště	k1gNnSc6
je	být	k5eAaImIp3nS
navíc	navíc	k6eAd1
možné	možný	k2eAgInPc4d1
údaje	údaj	k1gInPc4
sdílet	sdílet	k5eAaImF
mezi	mezi	k7c7
aplikacemi	aplikace	k1gFnPc7
(	(	kIx(
<g/>
pouze	pouze	k6eAd1
v	v	k7c6
případě	případ	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
aplikace	aplikace	k1gFnSc1
zkompilována	zkompilován	k2eAgFnSc1d1
s	s	k7c7
příslušným	příslušný	k2eAgNnSc7d1
nastavením	nastavení	k1gNnSc7
oprávnění	oprávnění	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vývoj	vývoj	k1gInSc1
pro	pro	k7c4
platformu	platforma	k1gFnSc4
iOS	iOS	k?
</s>
<s>
V	v	k7c6
iOS	iOS	k?
je	být	k5eAaImIp3nS
možné	možný	k2eAgFnPc4d1
spouště	spoušť	k1gFnPc4
aplikace	aplikace	k1gFnSc2
napsané	napsaný	k2eAgFnSc2d1
v	v	k7c6
jazyku	jazyk	k1gInSc6
C	C	kA
nebo	nebo	k8xC
pokročilejším	pokročilý	k2eAgMnPc3d2
Objective-C	Objective-C	k1gMnPc3
<g/>
/	/	kIx~
<g/>
Swiftu	Swift	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dlouhou	dlouhý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
bylo	být	k5eAaImAgNnS
možné	možný	k2eAgNnSc1d1
vyvíjet	vyvíjet	k5eAaImF
pouze	pouze	k6eAd1
v	v	k7c6
aplikaci	aplikace	k1gFnSc6
XCode	XCod	k1gMnSc5
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
vývojové	vývojový	k2eAgNnSc4d1
prostředí	prostředí	k1gNnSc4
od	od	k7c2
firmy	firma	k1gFnSc2
Apple	Apple	kA
(	(	kIx(
<g/>
nabízené	nabízený	k2eAgNnSc1d1
zdarma	zdarma	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
prostředí	prostředí	k1gNnSc1
je	být	k5eAaImIp3nS
však	však	k9
dostupné	dostupný	k2eAgNnSc1d1
pouze	pouze	k6eAd1
pro	pro	k7c4
operační	operační	k2eAgInSc4d1
systém	systém	k1gInSc4
Mac	Mac	kA
OS	OS	kA
X	X	kA
<g/>
,	,	kIx,
takže	takže	k8xS
vývoj	vývoj	k1gInSc1
např.	např.	kA
ve	v	k7c6
Windows	Windows	kA
či	či	k8xC
Linuxu	linux	k1gInSc2
není	být	k5eNaImIp3nS
možný	možný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Tento	tento	k3xDgInSc1
problém	problém	k1gInSc1
se	se	k3xPyFc4
pokusilo	pokusit	k5eAaPmAgNnS
řešit	řešit	k5eAaImF
několik	několik	k4yIc1
projektů	projekt	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
se	se	k3xPyFc4
snažily	snažit	k5eAaImAgInP
kompilovat	kompilovat	k5eAaImF
programy	program	k1gInPc1
napsané	napsaný	k2eAgInPc1d1
v	v	k7c6
jiných	jiný	k2eAgInPc6d1
jazycích	jazyk	k1gInPc6
do	do	k7c2
nativního	nativní	k2eAgInSc2d1
kódu	kód	k1gInSc2
Objective-	Objective-	k1gMnPc1
<g/>
C.	C.	kA
Asi	asi	k9
největším	veliký	k2eAgInSc7d3
počinem	počin	k1gInSc7
v	v	k7c6
této	tento	k3xDgFnSc6
oblasti	oblast	k1gFnSc6
je	být	k5eAaImIp3nS
krok	krok	k1gInSc1
společnosti	společnost	k1gFnSc2
Adobe	Adobe	kA
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
v	v	k7c6
nové	nový	k2eAgFnSc6d1
verzi	verze	k1gFnSc6
svého	svůj	k3xOyFgInSc2
nástroje	nástroj	k1gInSc2
pro	pro	k7c4
vývoj	vývoj	k1gInSc4
aplikací	aplikace	k1gFnPc2
Flash	Flash	k1gMnSc1
umožňuje	umožňovat	k5eAaImIp3nS
kompilovat	kompilovat	k5eAaImF
právě	právě	k9
do	do	k7c2
programu	program	k1gInSc2
určeného	určený	k2eAgInSc2d1
pro	pro	k7c4
iOS	iOS	k?
<g/>
.	.	kIx.
</s>
<s>
Tento	tento	k3xDgInSc1
(	(	kIx(
<g/>
a	a	k8xC
podobné	podobný	k2eAgInPc4d1
<g/>
)	)	kIx)
nástroje	nástroj	k1gInPc4
však	však	k9
byly	být	k5eAaImAgFnP
zakázány	zakázat	k5eAaPmNgFnP
v	v	k7c6
licenčním	licenční	k2eAgNnSc6d1
ujednání	ujednání	k1gNnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
po	po	k7c6
velké	velký	k2eAgFnSc6d1
nevoli	nevole	k1gFnSc6
ze	z	k7c2
strany	strana	k1gFnSc2
vývojářů	vývojář	k1gMnPc2
byly	být	k5eAaImAgFnP
opět	opět	k6eAd1
povoleny	povolen	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Jailbreak	Jailbreak	k6eAd1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Jailbreak	Jailbreak	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
iOS	iOS	k?
je	být	k5eAaImIp3nS
poměrně	poměrně	k6eAd1
uzavřený	uzavřený	k2eAgInSc1d1
systém	systém	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
neumožňuje	umožňovat	k5eNaImIp3nS
uživateli	uživatel	k1gMnSc3
přístup	přístup	k1gInSc4
do	do	k7c2
systému	systém	k1gInSc2
a	a	k8xC
také	také	k9
omezuje	omezovat	k5eAaImIp3nS
možnost	možnost	k1gFnSc4
instalace	instalace	k1gFnSc1
aplikací	aplikace	k1gFnPc2
–	–	k?
jediná	jediný	k2eAgFnSc1d1
možnost	možnost	k1gFnSc1
je	být	k5eAaImIp3nS
přes	přes	k7c4
oficiální	oficiální	k2eAgFnSc4d1
App	App	k1gFnSc4
Store	Stor	k1gMnSc5
<g/>
,	,	kIx,
kde	kde	k6eAd1
aplikace	aplikace	k1gFnPc1
procházejí	procházet	k5eAaImIp3nP
schvalovacím	schvalovací	k2eAgInSc7d1
procesem	proces	k1gInSc7
ze	z	k7c2
strany	strana	k1gFnSc2
společnosti	společnost	k1gFnSc2
Apple	Apple	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Právě	právě	k6eAd1
nedostupnost	nedostupnost	k1gFnSc4
některých	některý	k3yIgFnPc2
aplikací	aplikace	k1gFnPc2
a	a	k8xC
nemožnost	nemožnost	k1gFnSc4
přístupu	přístup	k1gInSc2
do	do	k7c2
systému	systém	k1gInSc2
bývají	bývat	k5eAaImIp3nP
motivem	motiv	k1gInSc7
k	k	k7c3
tzv.	tzv.	kA
jailbreaku	jailbreak	k1gInSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
proces	proces	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
modifikuje	modifikovat	k5eAaBmIp3nS
systém	systém	k1gInSc4
a	a	k8xC
umožní	umožnit	k5eAaPmIp3nS
nahrávat	nahrávat	k5eAaImF
neautorizované	autorizovaný	k2eNgFnPc4d1
aplikace	aplikace	k1gFnPc4
a	a	k8xC
přistupovat	přistupovat	k5eAaImF
ke	k	k7c3
chráněným	chráněný	k2eAgInPc3d1
souborům	soubor	k1gInPc3
systému	systém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
těchto	tento	k3xDgFnPc2
výhod	výhoda	k1gFnPc2
ale	ale	k8xC
přináší	přinášet	k5eAaImIp3nS
rizika	riziko	k1gNnPc4
v	v	k7c6
podobě	podoba	k1gFnSc6
snížení	snížení	k1gNnSc2
výkonu	výkon	k1gInSc2
<g/>
,	,	kIx,
zvýšení	zvýšení	k1gNnSc4
spotřeby	spotřeba	k1gFnSc2
energie	energie	k1gFnSc2
a	a	k8xC
zvýšeného	zvýšený	k2eAgNnSc2d1
rizika	riziko	k1gNnSc2
napadení	napadení	k1gNnSc2
telefonu	telefon	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnoho	mnoho	k6eAd1
uživatelů	uživatel	k1gMnPc2
také	také	k9
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
možnosti	možnost	k1gFnPc4
nahrávat	nahrávat	k5eAaImF
do	do	k7c2
zařízení	zařízení	k1gNnSc2
aplikace	aplikace	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc4,k3yQgFnPc4,k3yIgFnPc4
‚	‚	k?
<g/>
ukradli	ukradnout	k5eAaPmAgMnP
<g/>
‘	‘	k?
z	z	k7c2
internetu	internet	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společnost	společnost	k1gFnSc1
Apple	Apple	kA
se	se	k3xPyFc4
snažila	snažit	k5eAaImAgFnS
bránit	bránit	k5eAaImF
tomuto	tento	k3xDgInSc3
procesu	proces	k1gInSc3
u	u	k7c2
soudu	soud	k1gInSc2
<g/>
,	,	kIx,
avšak	avšak	k8xC
nejnovější	nový	k2eAgInSc1d3
verdikt	verdikt	k1gInSc1
zní	znět	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
legální	legální	k2eAgInSc1d1
zásah	zásah	k1gInSc1
a	a	k8xC
nepoškozuje	poškozovat	k5eNaImIp3nS
copyright	copyright	k1gInSc1
společnosti	společnost	k1gFnSc2
Apple	Apple	kA
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
vydání	vydání	k1gNnSc6
nové	nový	k2eAgFnSc2d1
verze	verze	k1gFnSc2
operačního	operační	k2eAgInSc2d1
systému	systém	k1gInSc2
iOS	iOS	k?
8.1	8.1	k4
<g/>
.3	.3	k4
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
opravena	opravit	k5eAaPmNgFnS
spousta	spousta	k1gFnSc1
bezpečnostních	bezpečnostní	k2eAgFnPc2d1
děr	děra	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
doposud	doposud	k6eAd1
umožňovaly	umožňovat	k5eAaImAgFnP
nainstalovat	nainstalovat	k5eAaPmF
Jailbreak	Jailbreak	k1gInSc4
na	na	k7c4
zařízení	zařízení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kompletní	kompletní	k2eAgFnPc4d1
opravy	oprava	k1gFnPc4
těchto	tento	k3xDgFnPc2
děr	děra	k1gFnPc2
by	by	kYmCp3nP
se	se	k3xPyFc4
měly	mít	k5eAaImAgFnP
objevit	objevit	k5eAaPmF
až	až	k9
v	v	k7c6
přicházející	přicházející	k2eAgFnSc6d1
nové	nový	k2eAgFnSc6d1
verzi	verze	k1gFnSc6
<g/>
,	,	kIx,
iOS	iOS	k?
9	#num#	k4
<g/>
,	,	kIx,
kde	kde	k6eAd1
definitivně	definitivně	k6eAd1
by	by	kYmCp3nS
mělo	mít	k5eAaImAgNnS
být	být	k5eAaImF
nemožné	možný	k2eNgNnSc1d1
si	se	k3xPyFc3
na	na	k7c4
zařízení	zařízení	k1gNnSc4
nainstalovat	nainstalovat	k5eAaPmF
jailbreak	jailbreak	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Zadní	zadní	k2eAgNnPc4d1
vrátka	vrátka	k1gNnPc4
iOS	iOS	k?
</s>
<s>
Výzkumník	výzkumník	k1gMnSc1
cyber-bezpečnosti	cyber-bezpečnost	k1gFnSc2
<g/>
,	,	kIx,
Christopher	Christophra	k1gFnPc2
Soghoian	Soghoiana	k1gFnPc2
<g/>
,	,	kIx,
upozornil	upozornit	k5eAaPmAgMnS
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
říjnu	říjen	k1gInSc6
2011	#num#	k4
byl	být	k5eAaImAgMnS
v	v	k7c6
prostředí	prostředí	k1gNnSc6
indické	indický	k2eAgFnSc2d1
vojenské	vojenský	k2eAgFnSc2d1
rozvědky	rozvědka	k1gFnSc2
rozeslán	rozeslán	k2eAgInSc1d1
vnitřní	vnitřní	k2eAgInSc1d1
oběžník	oběžník	k1gInSc1
<g/>
,	,	kIx,
vydaný	vydaný	k2eAgInSc1d1
plukovníkem	plukovník	k1gMnSc7
Išwarem	Išwar	k1gMnSc7
Singhem	Singh	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
byl	být	k5eAaImAgInS
později	pozdě	k6eAd2
získán	získat	k5eAaPmNgInS
hackery	hacker	k1gMnPc7
a	a	k8xC
zveřejněn	zveřejnit	k5eAaPmNgInS
na	na	k7c6
internetu	internet	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
oběžníku	oběžník	k1gInSc6
se	se	k3xPyFc4
píše	psát	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
výměnou	výměna	k1gFnSc7
za	za	k7c4
přítomnost	přítomnost	k1gFnSc4
na	na	k7c6
indickém	indický	k2eAgMnSc6d1
(	(	kIx(
<g/>
mobilním	mobilní	k2eAgInSc6d1
<g/>
)	)	kIx)
trhu	trh	k1gInSc6
<g/>
“	“	k?
výrobci	výrobce	k1gMnPc1
mobilních	mobilní	k2eAgNnPc2d1
zařízení	zařízení	k1gNnPc2
zahrnující	zahrnující	k2eAgFnSc1d1
korporace	korporace	k1gFnSc1
RIM	RIM	kA
<g/>
,	,	kIx,
Nokia	Nokia	kA
a	a	k8xC
Apple	Apple	kA
<g />
.	.	kIx.
</s>
<s hack="1">
souhlasili	souhlasit	k5eAaImAgMnP
s	s	k7c7
umožněním	umožnění	k1gNnSc7
a	a	k8xC
poskytnutím	poskytnutí	k1gNnSc7
přístupu	přístup	k1gInSc2
„	„	k?
<g/>
zadními	zadní	k2eAgInPc7d1
vrátky	vrátek	k1gInPc7
<g/>
“	“	k?
na	na	k7c4
jimi	on	k3xPp3gMnPc7
vyráběných	vyráběný	k2eAgNnPc2d1
zařízeních	zařízení	k1gNnPc6
indické	indický	k2eAgFnSc3d1
vládě	vláda	k1gFnSc3
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
následně	následně	k6eAd1
tento	tento	k3xDgInSc4
přístup	přístup	k1gInSc4
informací	informace	k1gFnPc2
utilizovala	utilizovat	k5eAaBmAgFnS,k5eAaImAgFnS,k5eAaPmAgFnS
pro	pro	k7c4
interní	interní	k2eAgInPc4d1
e-maily	e-mail	k1gInPc4
americko-čínské	americko-čínský	k2eAgFnSc2d1
ekonomické	ekonomický	k2eAgFnSc2d1
bezpečnostní	bezpečnostní	k2eAgFnSc2d1
a	a	k8xC
výzkumné	výzkumný	k2eAgFnSc2d1
komise	komise	k1gFnSc2
a	a	k8xC
orgánům	orgán	k1gMnPc3
vlády	vláda	k1gFnSc2
USA	USA	kA
s	s	k7c7
mandátem	mandát	k1gInSc7
monitorovat	monitorovat	k5eAaImF
<g/>
,	,	kIx,
vyšetřovat	vyšetřovat	k5eAaImF
a	a	k8xC
podávat	podávat	k5eAaImF
hlášení	hlášení	k1gNnPc4
Kongresu	kongres	k1gInSc2
na	na	k7c4
téma	téma	k1gNnSc4
implikací	implikace	k1gFnPc2
národní	národní	k2eAgFnSc2d1
bezpečnosti	bezpečnost	k1gFnSc2
o	o	k7c4
ekonomického	ekonomický	k2eAgMnSc4d1
vztahu	vztah	k1gInSc6
a	a	k8xC
oboustranném	oboustranný	k2eAgInSc6d1
obchodu	obchod	k1gInSc6
mezi	mezi	k7c7
USA	USA	kA
a	a	k8xC
Čínou	Čína	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
https://mobilizujeme.cz/clanky/android-a-ios-jiz-maji-999-podil-ostatni-systemy-jsou-mrtve	https://mobilizujeme.cz/clanky/android-a-ios-jiz-maji-999-podil-ostatni-systemy-jsou-mrtvat	k5eAaPmIp3nS
<g/>
↑	↑	k?
Apple	Apple	kA
Releases	Releases	k1gMnSc1
iPhone	iPhon	k1gInSc5
1.1	1.1	k4
<g/>
.1	.1	k4
Update	update	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MacRumors	MacRumors	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
iPod	iPod	k1gInSc1
Touch	Touch	k1gInSc1
1.1	1.1	k4
<g/>
.1	.1	k4
Update	update	k1gInSc1
(	(	kIx(
<g/>
Screen	Screen	k2eAgInSc1d1
Fix	fix	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MacRumors	MacRumors	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Apple	Apple	kA
Introduces	Introduces	k1gMnSc1
the	the	k?
New	New	k1gMnSc5
iPhone	iPhon	k1gMnSc5
3G	3G	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2008-06-09	2008-06-09	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
IPhone	IPhon	k1gInSc5
2.0	2.0	k4
software	software	k1gInSc1
will	wilnout	k5eAaPmAgInS
be	be	k?
available	available	k6eAd1
on	on	k3xPp3gMnSc1
July	Jula	k1gMnPc7
11	#num#	k4
as	as	k1gNnSc1
a	a	k8xC
free	free	k1gInSc1
software	software	k1gInSc1
update	update	k1gInSc4
via	via	k7c4
iTunes	iTunes	k1gInSc4
7.7	7.7	k4
or	or	k?
later	latero	k1gNnPc2
for	forum	k1gNnPc2
all	all	k?
iPhone	iPhon	k1gInSc5
customers	customersa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
CaziSoft	CaziSoft	k1gInSc1
–	–	k?
More	mor	k1gInSc5
about	about	k2eAgInSc1d1
iPhone	iPhon	k1gInSc5
2.2	2.2	k4
<g/>
.	.	kIx.
cazisoft	cazisofta	k1gFnPc2
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
30	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
2014	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
21	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
2009	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Apple	Apple	kA
Inc	Inc	k1gFnSc2
<g/>
..	..	k?
iPhone	iPhon	k1gInSc5
—	—	k?
New	New	k1gMnSc1
features	features	k1gMnSc1
in	in	k?
the	the	k?
iPhone	iPhon	k1gInSc5
3.1	3.1	k4
Software	software	k1gInSc1
Update	update	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Apple	Apple	kA
Inc	Inc	k1gFnPc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
SHIELS	SHIELS	kA
<g/>
,	,	kIx,
Maggie	Maggie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mixed	Mixed	k1gInSc1
reaction	reaction	k1gInSc4
to	ten	k3xDgNnSc1
iPhone	iPhon	k1gInSc5
update	update	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
News	Newsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
June	jun	k1gMnSc5
9	#num#	k4
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Apple	Apple	kA
issues	issues	k1gInSc1
iOS	iOS	k?
4.2	4.2	k4
<g/>
.1	.1	k4
golden	goldna	k1gFnPc2
master	master	k1gMnSc1
for	forum	k1gNnPc2
iPhone	iPhon	k1gMnSc5
<g/>
,	,	kIx,
iPad	iPad	k6eAd1
to	ten	k3xDgNnSc1
developers	developers	k6eAd1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
AppleInsider	AppleInsidra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Apple	Apple	kA
<g/>
’	’	k?
<g/>
s	s	k7c7
iOS	iOS	k?
4.2	4.2	k4
Available	Available	k1gMnSc2
Today	Todaa	k1gMnSc2
for	forum	k1gNnPc2
iPad	iPad	k6eAd1
<g/>
,	,	kIx,
iPhone	iPhon	k1gInSc5
&	&	k?
iPod	iPod	k1gMnSc1
Touch	Touch	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Apple	Apple	kA
<g/>
,	,	kIx,
22	#num#	k4
Nov	nov	k1gInSc1
2010	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Apple	Apple	kA
to	ten	k3xDgNnSc1
Unveil	Unveil	k1gInSc1
Next	Next	k2eAgInSc1d1
Generation	Generation	k1gInSc1
Software	software	k1gInSc1
at	at	k?
Keynote	Keynot	k1gInSc5
Address	Address	k1gInSc1
on	on	k3xPp3gMnSc1
Monday	Monday	k1gInPc1
<g/>
,	,	kIx,
June	jun	k1gMnSc5
6	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Apple	Apple	kA
Inc	Inc	k1gFnPc2
<g/>
.	.	kIx.
<g/>
,	,	kIx,
2011-05-31	2011-05-31	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
iOS	iOS	k?
5	#num#	k4
Software	software	k1gInSc1
Update	update	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Apple	Apple	kA
Inc	Inc	k1gFnPc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Apple	Apple	kA
announces	announces	k1gInSc1
iOS	iOS	k?
5.1	5.1	k4
http://mashable.com/2012/03/07/apple-ios-51-update	http://mashable.com/2012/03/07/apple-ios-51-updat	k1gInSc5
<g/>
↑	↑	k?
iOS	iOS	k?
6	#num#	k4
Software	software	k1gInSc1
Update	update	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Apple	Apple	kA
Inc	Inc	k1gFnPc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
POLLICINO	POLLICINO	kA
<g/>
,	,	kIx,
Joe	Joe	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Apple	Apple	kA
<g/>
'	'	kIx"
<g/>
s	s	k7c7
September	September	k1gInSc1
12	#num#	k4
<g/>
th	th	k?
event	event	k1gMnSc1
roundup	roundup	k1gMnSc1
<g/>
:	:	kIx,
iPhone	iPhon	k1gInSc5
5	#num#	k4
<g/>
,	,	kIx,
new	new	k?
iPods	iPods	k1gInSc1
<g/>
,	,	kIx,
iOS	iOS	k?
6	#num#	k4
<g/>
,	,	kIx,
Lightning	Lightning	k1gInSc1
and	and	k?
everything	everything	k1gInSc1
else	else	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Engadget	Engadgeta	k1gFnPc2
<g/>
,	,	kIx,
2012-09-12	2012-09-12	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
iOS	iOS	k?
6.1	6.1	k4
<g/>
.1	.1	k4
Software	software	k1gInSc1
Update	update	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Apple	Apple	kA
Inc	Inc	k1gFnPc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Apple	Apple	kA
-	-	kIx~
iOS	iOS	k?
7	#num#	k4
-	-	kIx~
Features	Features	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Apple	Apple	kA
Inc	Inc	k1gFnPc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
http://crypto-world.info/news/index.php?prispevek=21800&	http://crypto-world.info/news/index.php?prispevek=21800&	k1gInSc1
-	-	kIx~
Hack	Hack	k1gInSc1
umožňuje	umožňovat	k5eAaImIp3nS
přístup	přístup	k1gInSc4
k	k	k7c3
datům	datum	k1gNnPc3
uvnitř	uvnitř	k7c2
iPhone	iPhon	k1gInSc5
s	s	k7c7
iOS	iOS	k?
<g/>
.6	.6	k4
<g/>
.	.	kIx.
<g/>
x	x	k?
<g/>
↑	↑	k?
http://imgur.com/a/8XoGf#4	http://imgur.com/a/8XoGf#4	k4
<g/>
↑	↑	k?
Leaked	Leaked	k1gMnSc1
Memo	Memo	k1gMnSc1
Says	Saysa	k1gFnPc2
Apple	Apple	kA
Provides	Provides	k1gMnSc1
Backdoor	Backdoor	k1gMnSc1
To	to	k9
Governments	Governments	k1gInSc4
<g/>
↑	↑	k?
http://slashdot.org/story/12/01/08/069204/leaked-memo-says-apple-provides-backdoor-to-governments	http://slashdot.org/story/12/01/08/069204/leaked-memo-says-apple-provides-backdoor-to-governments	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Apple	Apple	kA
Inc	Inc	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historie	historie	k1gFnSc1
Zakladatelé	zakladatel	k1gMnPc1
</s>
<s>
Steve	Steve	k1gMnSc1
Jobs	Jobsa	k1gFnPc2
</s>
<s>
Steve	Steve	k1gMnSc1
Wozniak	Wozniak	k1gMnSc1
</s>
<s>
Ronald	Ronald	k1gMnSc1
Wayne	Wayn	k1gMnSc5
Představenstvo	představenstvo	k1gNnSc1
</s>
<s>
Současné	současný	k2eAgNnSc1d1
</s>
<s>
Bill	Bill	k1gMnSc1
Campbell	Campbell	k1gMnSc1
</s>
<s>
Millard	Millard	k1gMnSc1
Drexler	Drexler	k1gMnSc1
</s>
<s>
Al	ala	k1gFnPc2
Gore	Gore	k1gFnPc2
</s>
<s>
Tim	Tim	k?
Cook	Cook	k1gMnSc1
(	(	kIx(
<g/>
CEO	CEO	kA
<g/>
)	)	kIx)
</s>
<s>
Andrea	Andrea	k1gFnSc1
Jung	Jung	k1gMnSc1
</s>
<s>
Arthur	Arthur	k1gMnSc1
D.	D.	kA
Levinson	Levinson	k1gMnSc1
</s>
<s>
Ronald	Ronald	k1gMnSc1
Sugar	Sugar	k1gMnSc1
</s>
<s>
Robert	Robert	k1gMnSc1
Iger	Iger	k1gMnSc1
Bývalé	bývalý	k2eAgNnSc1d1
</s>
<s>
Steve	Steve	k1gMnSc1
Jobs	Jobsa	k1gFnPc2
</s>
<s>
Jerry	Jerra	k1gFnPc1
York	York	k1gInSc1
</s>
<s>
Eric	Eric	k6eAd1
Schmidt	Schmidt	k1gMnSc1
</s>
<s>
Larry	Larra	k1gFnPc1
Ellison	Ellisona	k1gFnPc2
</s>
<s>
Vedení	vedení	k1gNnSc1
</s>
<s>
Současné	současný	k2eAgNnSc1d1
</s>
<s>
Tim	Tim	k?
Cook	Cook	k1gMnSc1
</s>
<s>
Angela	Angela	k1gFnSc1
Ahrendts	Ahrendtsa	k1gFnPc2
</s>
<s>
Eddy	Edda	k1gFnPc1
Cue	Cue	k1gFnSc2
</s>
<s>
Craig	Craig	k1gInSc1
Federighi	Federigh	k1gFnSc2
</s>
<s>
Jonathan	Jonathan	k1gMnSc1
Ive	Ive	k1gMnSc1
</s>
<s>
Luca	Luca	k1gFnSc1
Maestri	Maestr	k1gFnSc2
</s>
<s>
Dan	Dan	k1gMnSc1
Riccio	Riccio	k1gMnSc1
</s>
<s>
Phil	Phil	k1gMnSc1
Schiller	Schiller	k1gMnSc1
</s>
<s>
Bruce	Bruce	k1gMnSc1
Sewell	Sewell	k1gMnSc1
</s>
<s>
Jeff	Jeff	k1gInSc1
Williams	Williamsa	k1gFnPc2
Bývalé	bývalý	k2eAgFnSc2d1
</s>
<s>
Steve	Steve	k1gMnSc1
Jobs	Jobsa	k1gFnPc2
</s>
<s>
Ron	Ron	k1gMnSc1
Johnson	Johnson	k1gMnSc1
</s>
<s>
John	John	k1gMnSc1
Browett	Browett	k1gMnSc1
</s>
<s>
Scott	Scott	k1gMnSc1
Forstall	Forstall	k1gMnSc1
</s>
<s>
Tony	Tony	k1gMnSc1
Fadell	Fadell	k1gMnSc1
</s>
<s>
Peter	Peter	k1gMnSc1
Oppenheimer	Oppenheimer	k1gMnSc1
</s>
<s>
Mark	Mark	k1gMnSc1
Papermaster	Papermaster	k1gMnSc1
</s>
<s>
Bertrand	Bertrand	k1gInSc1
Serlet	Serlet	k1gInSc1
</s>
<s>
Sina	sino	k1gNnPc4
Tamaddon	Tamaddon	k1gInSc1
</s>
<s>
Avie	Avia	k1gFnPc1
Tevanian	Tevaniany	k1gInPc2
</s>
<s>
Jon	Jon	k?
Rubinstein	Rubinstein	k1gInSc1
</s>
<s>
Nancy	Nancy	k1gFnSc4
R.	R.	kA
Heinen	Heinen	k2eAgMnSc1d1
</s>
<s>
Fred	Fred	k1gMnSc1
D.	D.	kA
Anderson	Anderson	k1gMnSc1
</s>
<s>
Obchody	obchod	k1gInPc1
</s>
<s>
Apple	Apple	kA
Store	Stor	k1gMnSc5
</s>
<s>
online	onlinout	k5eAaPmIp3nS
</s>
<s>
App	App	k?
Store	Stor	k1gMnSc5
</s>
<s>
iBooks	iBooksit	k5eAaPmRp2nS
Store	Stor	k1gMnSc5
</s>
<s>
iTunes	iTunes	k1gMnSc1
Store	Stor	k1gMnSc5
</s>
<s>
Mac	Mac	kA
App	App	k1gFnSc1
Store	Stor	k1gMnSc5
Služby	služba	k1gFnPc1
</s>
<s>
Apple	Apple	kA
ID	ido	k1gNnPc2
</s>
<s>
Apple	Apple	kA
Pay	Pay	k1gFnSc1
</s>
<s>
Developer	developer	k1gMnSc1
</s>
<s>
Game	game	k1gInSc1
Center	centrum	k1gNnPc2
</s>
<s>
iAd	iAd	k?
</s>
<s>
iBooks	iBooks	k6eAd1
</s>
<s>
Cloud	Cloud	k6eAd1
</s>
<s>
iCloud	iCloud	k6eAd1
</s>
<s>
iWork	iWork	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
</s>
<s>
MobileMe	MobileMe	k6eAd1
</s>
<s>
Podpora	podpora	k1gFnSc1
</s>
<s>
AppleCare	AppleCar	k1gMnSc5
</s>
<s>
Apple	Apple	kA
Specialist	Specialist	k1gMnSc1
</s>
<s>
Certifikace	certifikace	k1gFnSc1
</s>
<s>
Genius	genius	k1gMnSc1
Bar	bar	k1gInSc1
</s>
<s>
One	One	k?
to	ten	k3xDgNnSc1
One	One	k1gFnPc3
</s>
<s>
ProCare	ProCar	k1gMnSc5
Produkty	produkt	k1gInPc1
</s>
<s>
Software	software	k1gInSc1
</s>
<s>
Mac	Mac	kA
OS	OS	kA
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
OS	OS	kA
X	X	kA
</s>
<s>
OS	OS	kA
X	X	kA
Server	server	k1gInSc1
</s>
<s>
iOS	iOS	k?
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Core	Core	k6eAd1
Foundation	Foundation	k1gInSc1
</s>
<s>
Developer	developer	k1gMnSc1
Tools	Toolsa	k1gFnPc2
</s>
<s>
Final	Final	k1gInSc1
Cut	Cut	k1gFnPc2
Studio	studio	k1gNnSc1
</s>
<s>
Logic	Logic	k1gMnSc1
Studio	studio	k1gNnSc1
</s>
<s>
QuickTime	QuickTimat	k5eAaPmIp3nS
</s>
<s>
Ukončený	ukončený	k2eAgInSc1d1
software	software	k1gInSc1
Hardware	hardware	k1gInSc1
</s>
<s>
Macintosh	Macintosh	kA
</s>
<s>
iPod	iPod	k6eAd1
</s>
<s>
iPhone	iPhon	k1gMnSc5
</s>
<s>
iPad	iPad	k6eAd1
</s>
<s>
Apple	Apple	kA
Watch	Watch	k1gMnSc1
</s>
<s>
Apple	Apple	kA
SIM	SIM	kA
</s>
<s>
Příslušenství	příslušenství	k1gNnSc1
</s>
<s>
Ukončený	ukončený	k2eAgInSc1d1
hardware	hardware	k1gInSc1
</s>
<s>
Společnosti	společnost	k1gFnPc1
</s>
<s>
Samostatné	samostatný	k2eAgNnSc1d1
</s>
<s>
Beats	Beats	k6eAd1
Electronics	Electronics	k1gInSc1
</s>
<s>
Beats	Beats	k1gInSc1
Music	Musice	k1gInPc2
</s>
<s>
Braeburn	Braeburn	k1gMnSc1
Capital	Capital	k1gMnSc1
</s>
<s>
FileMaker	FileMaker	k1gMnSc1
Inc	Inc	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Získané	získaný	k2eAgFnPc4d1
</s>
<s>
Anobit	Anobit	k5eAaImF,k5eAaPmF
</s>
<s>
AuthenTec	AuthenTec	k1gMnSc1
</s>
<s>
Beats	Beats	k6eAd1
Electronics	Electronics	k1gInSc1
</s>
<s>
Beats	Beats	k1gInSc1
Music	Musice	k1gInPc2
</s>
<s>
Cue	Cue	k?
</s>
<s>
Emagic	Emagic	k1gMnSc1
</s>
<s>
FingerWorks	FingerWorks	k6eAd1
</s>
<s>
Intrinsity	Intrinsit	k1gInPc1
</s>
<s>
Lala	Lala	k6eAd1
</s>
<s>
NeXT	NeXT	k?
</s>
<s>
Nothing	Nothing	k1gInSc1
Real	Real	k1gInSc1
</s>
<s>
P.	P.	kA
<g/>
A.	A.	kA
Semi	Sem	k1gMnPc1
</s>
<s>
PrimeSense	PrimeSense	k6eAd1
</s>
<s>
Siri	Siri	k6eAd1
</s>
<s>
Spotsetter	Spotsetter	k1gMnSc1
</s>
<s>
Topsy	Topsa	k1gFnPc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ph	ph	kA
<g/>
730192	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
7845187-5	7845187-5	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
2010039603	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
184460395	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
2010039603	#num#	k4
</s>
