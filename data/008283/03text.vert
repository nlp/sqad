<p>
<s>
Har	Har	k?	Har
Jiftach	Jiftach	k1gMnSc1	Jiftach
<g/>
'	'	kIx"	'
<g/>
el	ela	k1gFnPc2	ela
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
<g/>
:	:	kIx,	:
ה	ה	k?	ה
י	י	k?	י
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vrch	vrch	k1gInSc4	vrch
o	o	k7c6	o
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
225	[number]	k4	225
metrů	metr	k1gInPc2	metr
v	v	k7c6	v
severním	severní	k2eAgInSc6d1	severní
Izraeli	Izrael	k1gInSc6	Izrael
<g/>
,	,	kIx,	,
v	v	k7c6	v
Dolní	dolní	k2eAgFnSc6d1	dolní
Galileji	Galilea	k1gFnSc6	Galilea
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
cca	cca	kA	cca
9	[number]	k4	9
kilometrů	kilometr	k1gInPc2	kilometr
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
Nazaret	Nazaret	k1gInSc1	Nazaret
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
podobu	podoba	k1gFnSc4	podoba
zalesněného	zalesněný	k2eAgNnSc2d1	zalesněné
návrší	návrší	k1gNnSc2	návrší
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnPc4	jehož
vrcholové	vrcholový	k2eAgFnPc4d1	vrcholová
partie	partie	k1gFnPc4	partie
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
zástavba	zástavba	k1gFnSc1	zástavba
vesnice	vesnice	k1gFnSc2	vesnice
Alon	Alono	k1gNnPc2	Alono
ha-Galil	ha-Galit	k5eAaImAgInS	ha-Galit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
straně	strana	k1gFnSc6	strana
terén	terén	k1gInSc1	terén
prudce	prudko	k6eAd1	prudko
spadá	spadat	k5eAaImIp3nS	spadat
do	do	k7c2	do
kaňonu	kaňon	k1gInSc2	kaňon
vádí	vádí	k1gNnSc2	vádí
Nachal	Nachal	k1gMnSc1	Nachal
Jiftach	Jiftach	k1gMnSc1	Jiftach
<g/>
'	'	kIx"	'
<g/>
el	ela	k1gFnPc2	ela
nedaleko	nedaleko	k7c2	nedaleko
jeho	on	k3xPp3gInSc2	on
výtoku	výtok	k1gInSc2	výtok
z	z	k7c2	z
údolí	údolí	k1gNnSc2	údolí
Bejt	Bejt	k?	Bejt
Netofa	Netof	k1gMnSc2	Netof
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
soutěsce	soutěska	k1gFnSc6	soutěska
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
archeologická	archeologický	k2eAgFnSc1d1	archeologická
lokalita	lokalita	k1gFnSc1	lokalita
Churvat	Churvat	k1gFnSc2	Churvat
Jiftach	Jiftacha	k1gFnPc2	Jiftacha
<g/>
'	'	kIx"	'
<g/>
el	ela	k1gFnPc2	ela
(	(	kIx(	(
<g/>
ח	ח	k?	ח
י	י	k?	י
<g/>
)	)	kIx)	)
se	s	k7c7	s
sídelní	sídelní	k2eAgFnSc7d1	sídelní
tradicí	tradice	k1gFnSc7	tradice
sahající	sahající	k2eAgFnSc7d1	sahající
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
bronzové	bronzový	k2eAgNnSc1d1	bronzové
až	až	k9	až
do	do	k7c2	do
byzantského	byzantský	k2eAgNnSc2d1	byzantské
období	období	k1gNnSc2	období
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
tu	tu	k6eAd1	tu
je	být	k5eAaImIp3nS	být
pramen	pramen	k1gInSc4	pramen
Ejn	Ejn	k1gFnPc2	Ejn
Jiftach	Jiftacha	k1gFnPc2	Jiftacha
<g/>
'	'	kIx"	'
<g/>
el	ela	k1gFnPc2	ela
(	(	kIx(	(
<g/>
ע	ע	k?	ע
י	י	k?	י
<g/>
)	)	kIx)	)
s	s	k7c7	s
kapacitou	kapacita	k1gFnSc7	kapacita
téměř	téměř	k6eAd1	téměř
200	[number]	k4	200
metrů	metr	k1gInPc2	metr
kubických	kubický	k2eAgInPc2d1	kubický
za	za	k7c4	za
hodinu	hodina	k1gFnSc4	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Vádí	vádí	k1gNnSc1	vádí
Nachal	Nachal	k1gMnSc1	Nachal
Jiftach	Jiftach	k1gMnSc1	Jiftach
<g/>
'	'	kIx"	'
<g/>
el	ela	k1gFnPc2	ela
pak	pak	k6eAd1	pak
na	na	k7c6	na
jihovýchodním	jihovýchodní	k2eAgNnSc6d1	jihovýchodní
úpatí	úpatí	k1gNnSc6	úpatí
hory	hora	k1gFnSc2	hora
ústí	ústit	k5eAaImIp3nS	ústit
do	do	k7c2	do
vádí	vádí	k1gNnSc2	vádí
Nachal	Nachal	k1gMnSc1	Nachal
Cipori	Cipor	k1gFnSc2	Cipor
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
poté	poté	k6eAd1	poté
obtéká	obtékat	k5eAaImIp3nS	obtékat
vrch	vrch	k1gInSc1	vrch
z	z	k7c2	z
jižní	jižní	k2eAgFnSc2d1	jižní
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
straně	strana	k1gFnSc6	strana
horu	hora	k1gFnSc4	hora
přetíná	přetínat	k5eAaImIp3nS	přetínat
trasa	trasa	k1gFnSc1	trasa
dálnice	dálnice	k1gFnSc2	dálnice
číslo	číslo	k1gNnSc1	číslo
79	[number]	k4	79
a	a	k8xC	a
za	za	k7c2	za
ní	on	k3xPp3gFnSc2	on
pak	pak	k6eAd1	pak
začíná	začínat	k5eAaImIp3nS	začínat
město	město	k1gNnSc1	město
Bir	Bir	k1gFnSc2	Bir
al-Maksur	al-Maksura	k1gFnPc2	al-Maksura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Zarzir	Zarzir	k1gMnSc1	Zarzir
</s>
</p>
