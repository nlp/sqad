<p>
<s>
Migmatity	Migmatit	k1gInPc1	Migmatit
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
migma	migmum	k1gNnSc2	migmum
–	–	k?	–
směs	směs	k1gFnSc1	směs
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
přeměněné	přeměněný	k2eAgFnSc2d1	přeměněná
horniny	hornina	k1gFnSc2	hornina
složené	složený	k2eAgFnSc2d1	složená
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
složek	složka	k1gFnPc2	složka
<g/>
,	,	kIx,	,
granitové	granitový	k2eAgFnPc1d1	granitová
a	a	k8xC	a
rulové	rulový	k2eAgFnPc1d1	rulová
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
mívají	mívat	k5eAaImIp3nP	mívat
podobu	podoba	k1gFnSc4	podoba
páskovaných	páskovaný	k2eAgFnPc2d1	páskovaná
rul	rula	k1gFnPc2	rula
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
při	při	k7c6	při
okrajích	okraj	k1gInPc6	okraj
granitových	granitový	k2eAgFnPc2d1	granitová
intruzí	intruze	k1gFnPc2	intruze
nebo	nebo	k8xC	nebo
v	v	k7c6	v
zónách	zóna	k1gFnPc6	zóna
nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
stupně	stupeň	k1gInSc2	stupeň
přeměny	přeměna	k1gFnSc2	přeměna
(	(	kIx(	(
<g/>
ultrametamorfismu	ultrametamorfismus	k1gInSc2	ultrametamorfismus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
již	již	k6eAd1	již
začíná	začínat	k5eAaImIp3nS	začínat
částečná	částečný	k2eAgNnPc4d1	částečné
anatexe	anatex	k1gInSc5	anatex
(	(	kIx(	(
<g/>
roztavení	roztavení	k1gNnPc4	roztavení
horniny	hornina	k1gFnSc2	hornina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vznikají	vznikat	k5eAaImIp3nP	vznikat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
magmatu	magma	k1gNnSc2	magma
unikající	unikající	k2eAgInPc1d1	unikající
plyny	plyn	k1gInPc1	plyn
a	a	k8xC	a
horké	horký	k2eAgInPc1d1	horký
roztoky	roztok	k1gInPc1	roztok
vnikají	vnikat	k5eAaImIp3nP	vnikat
po	po	k7c6	po
plochách	plocha	k1gFnPc6	plocha
břidličnatosti	břidličnatost	k1gFnSc2	břidličnatost
do	do	k7c2	do
přeměněných	přeměněný	k2eAgFnPc2d1	přeměněná
hornin	hornina	k1gFnPc2	hornina
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ochlazení	ochlazení	k1gNnSc6	ochlazení
se	se	k3xPyFc4	se
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
vyloučí	vyloučit	k5eAaPmIp3nP	vyloučit
minerály	minerál	k1gInPc1	minerál
živec	živec	k1gInSc1	živec
a	a	k8xC	a
křemen	křemen	k1gInSc1	křemen
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
vytvoří	vytvořit	k5eAaPmIp3nP	vytvořit
v	v	k7c6	v
tmavé	tmavý	k2eAgFnSc6d1	tmavá
hornině	hornina	k1gFnSc6	hornina
(	(	kIx(	(
<g/>
substrátu	substrát	k1gInSc6	substrát
<g/>
)	)	kIx)	)
světlé	světlý	k2eAgFnPc1d1	světlá
proužky	proužka	k1gFnPc1	proužka
(	(	kIx(	(
<g/>
metatekt	metatekt	k1gInSc1	metatekt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Minerálním	minerální	k2eAgNnSc7d1	minerální
složením	složení	k1gNnSc7	složení
jsou	být	k5eAaImIp3nP	být
migmatity	migmatit	k1gInPc1	migmatit
blízké	blízký	k2eAgInPc1d1	blízký
pararulám	pararula	k1gFnPc3	pararula
<g/>
,	,	kIx,	,
způsobem	způsob	k1gInSc7	způsob
vzniku	vznik	k1gInSc2	vznik
zase	zase	k9	zase
magmatickým	magmatický	k2eAgFnPc3d1	magmatická
horninám	hornina	k1gFnPc3	hornina
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
mají	mít	k5eAaImIp3nP	mít
podobu	podoba	k1gFnSc4	podoba
páskovaných	páskovaný	k2eAgFnPc2d1	páskovaná
rul	rula	k1gFnPc2	rula
<g/>
,	,	kIx,	,
od	od	k7c2	od
kterých	který	k3yRgInPc2	který
je	být	k5eAaImIp3nS	být
makroskopicky	makroskopicky	k6eAd1	makroskopicky
často	často	k6eAd1	často
nelze	lze	k6eNd1	lze
rozlišit	rozlišit	k5eAaPmF	rozlišit
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
přeměněných	přeměněný	k2eAgFnPc2d1	přeměněná
hornin	hornina	k1gFnPc2	hornina
řazeny	řadit	k5eAaImNgFnP	řadit
do	do	k7c2	do
zvláštní	zvláštní	k2eAgFnSc2d1	zvláštní
skupiny	skupina	k1gFnSc2	skupina
smíšených	smíšený	k2eAgFnPc2d1	smíšená
hornin	hornina	k1gFnPc2	hornina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Migmatit	Migmatit	k5eAaPmF	Migmatit
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
hornina	hornina	k1gFnSc1	hornina
hybridního	hybridní	k2eAgInSc2d1	hybridní
charakteru	charakter	k1gInSc2	charakter
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nP	tvořit
hranici	hranice	k1gFnSc4	hranice
mezi	mezi	k7c7	mezi
přeměněnými	přeměněný	k2eAgFnPc7d1	přeměněná
a	a	k8xC	a
magmatickými	magmatický	k2eAgFnPc7d1	magmatická
horninami	hornina	k1gFnPc7	hornina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
migmatity	migmatit	k1gInPc4	migmatit
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
používalo	používat	k5eAaImAgNnS	používat
několik	několik	k4yIc1	několik
různých	různý	k2eAgNnPc2d1	různé
označení	označení	k1gNnPc2	označení
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
migmatit	migmatit	k5eAaImF	migmatit
poprvé	poprvé	k6eAd1	poprvé
použil	použít	k5eAaPmAgMnS	použít
finský	finský	k2eAgMnSc1d1	finský
petrolog	petrolog	k1gMnSc1	petrolog
J.	J.	kA	J.
J.	J.	kA	J.
Sederholm	Sederholm	k1gMnSc1	Sederholm
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1907	[number]	k4	1907
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zkoumal	zkoumat	k5eAaImAgInS	zkoumat
migmatity	migmatit	k1gInPc4	migmatit
v	v	k7c4	v
oblasti	oblast	k1gFnPc4	oblast
Baltského	baltský	k2eAgInSc2d1	baltský
štítu	štít	k1gInSc2	štít
v	v	k7c6	v
jižním	jižní	k2eAgNnSc6d1	jižní
Finsku	Finsko	k1gNnSc6	Finsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
==	==	k?	==
</s>
</p>
<p>
<s>
Migmatity	Migmatit	k1gInPc1	Migmatit
vznikají	vznikat	k5eAaImIp3nP	vznikat
procesem	proces	k1gInSc7	proces
migmatitizace	migmatitizace	k1gFnSc2	migmatitizace
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
proces	proces	k1gInSc1	proces
blízký	blízký	k2eAgInSc1d1	blízký
metamorfní	metamorfní	k2eAgFnSc3d1	metamorfní
diferenciaci	diferenciace	k1gFnSc3	diferenciace
až	až	k8xS	až
částečnému	částečný	k2eAgNnSc3d1	částečné
natavení	natavení	k1gNnSc3	natavení
(	(	kIx(	(
<g/>
anatexia	anatexia	k1gFnSc1	anatexia
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
těmto	tento	k3xDgInPc3	tento
procesům	proces	k1gInPc3	proces
dochází	docházet	k5eAaImIp3nS	docházet
často	často	k6eAd1	často
na	na	k7c6	na
okrajích	okraj	k1gInPc6	okraj
granitových	granitový	k2eAgFnPc2d1	granitová
intruzí	intruze	k1gFnPc2	intruze
nebo	nebo	k8xC	nebo
v	v	k7c6	v
zónách	zóna	k1gFnPc6	zóna
mobilizace	mobilizace	k1gFnSc2	mobilizace
a	a	k8xC	a
migrace	migrace	k1gFnSc2	migrace
látek	látka	k1gFnPc2	látka
při	při	k7c6	při
tektonických	tektonický	k2eAgInPc6d1	tektonický
a	a	k8xC	a
metamorfních	metamorfní	k2eAgInPc6d1	metamorfní
procesech	proces	k1gInPc6	proces
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
procesy	proces	k1gInPc1	proces
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
různě	různě	k6eAd1	různě
kombinovat	kombinovat	k5eAaImF	kombinovat
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
různých	různý	k2eAgInPc2d1	různý
druhů	druh	k1gInPc2	druh
migmatitů	migmatit	k1gInPc2	migmatit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Injekce	injekce	k1gFnSc1	injekce
zbytkových	zbytkový	k2eAgMnPc2d1	zbytkový
granitoidních	granitoidní	k2eAgMnPc2d1	granitoidní
(	(	kIx(	(
<g/>
žulové	žulový	k2eAgFnSc2d1	Žulová
<g/>
,	,	kIx,	,
granodioritové	granodioritový	k2eAgFnSc2d1	granodioritový
nebo	nebo	k8xC	nebo
pegmatitové	pegmatitový	k2eAgFnSc2d1	pegmatitová
taveniny	tavenina	k1gFnSc2	tavenina
<g/>
)	)	kIx)	)
roztoků	roztok	k1gInPc2	roztok
mohou	moct	k5eAaImIp3nP	moct
pronikat	pronikat	k5eAaImF	pronikat
podél	podél	k7c2	podél
ploch	plocha	k1gFnPc2	plocha
břidličnatosti	břidličnatost	k1gFnSc2	břidličnatost
nebo	nebo	k8xC	nebo
příčných	příčný	k2eAgFnPc2d1	příčná
puklin	puklina	k1gFnPc2	puklina
krystalických	krystalický	k2eAgFnPc2d1	krystalická
břidlic	břidlice	k1gFnPc2	břidlice
a	a	k8xC	a
vznikají	vznikat	k5eAaImIp3nP	vznikat
tak	tak	k6eAd1	tak
injekční	injekční	k2eAgInPc4d1	injekční
migmatity	migmatit	k1gInPc4	migmatit
<g/>
,	,	kIx,	,
tvořící	tvořící	k2eAgFnSc4d1	tvořící
aureolu	aureola	k1gFnSc4	aureola
kolem	kolem	k7c2	kolem
plutonů	pluton	k1gInPc2	pluton
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ochlazení	ochlazení	k1gNnSc6	ochlazení
z	z	k7c2	z
horkých	horký	k2eAgInPc2d1	horký
roztoků	roztok	k1gInPc2	roztok
vykrystalizují	vykrystalizovat	k5eAaPmIp3nP	vykrystalizovat
minerály	minerál	k1gInPc1	minerál
živec	živec	k1gInSc1	živec
a	a	k8xC	a
křemen	křemen	k1gInSc1	křemen
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
vytvoří	vytvořit	k5eAaPmIp3nP	vytvořit
v	v	k7c6	v
tmavé	tmavý	k2eAgFnSc6d1	tmavá
hornině	hornina	k1gFnSc6	hornina
(	(	kIx(	(
<g/>
označované	označovaný	k2eAgFnPc1d1	označovaná
jako	jako	k8xC	jako
paleosom	paleosom	k1gInSc1	paleosom
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
substrát	substrát	k1gInSc1	substrát
<g/>
)	)	kIx)	)
světlé	světlý	k2eAgFnSc2d1	světlá
proužky	proužka	k1gFnSc2	proužka
(	(	kIx(	(
<g/>
leukosom	leukosom	k1gInSc1	leukosom
či	či	k8xC	či
metatekt	metatekt	k1gInSc1	metatekt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tmavé	tmavý	k2eAgInPc4d1	tmavý
pruhy	pruh	k1gInPc4	pruh
s	s	k7c7	s
relikty	relikt	k1gInPc7	relikt
minerálů	minerál	k1gInPc2	minerál
původní	původní	k2eAgFnSc2d1	původní
horniny	hornina	k1gFnSc2	hornina
tvoří	tvořit	k5eAaImIp3nS	tvořit
tzv.	tzv.	kA	tzv.
melanosom	melanosom	k1gInSc1	melanosom
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
pruhy	pruh	k1gInPc1	pruh
bývají	bývat	k5eAaImIp3nP	bývat
zvrásněné	zvrásněný	k2eAgInPc1d1	zvrásněný
<g/>
.	.	kIx.	.
</s>
<s>
Granitové	granitový	k2eAgInPc1d1	granitový
pruhy	pruh	k1gInPc1	pruh
vznikají	vznikat	k5eAaImIp3nP	vznikat
dílčím	dílčí	k2eAgNnSc7d1	dílčí
natavováním	natavování	k1gNnSc7	natavování
mateřské	mateřský	k2eAgFnSc2d1	mateřská
horniny	hornina	k1gFnSc2	hornina
při	při	k7c6	při
teplotách	teplota	k1gFnPc6	teplota
těsně	těsně	k6eAd1	těsně
pod	pod	k7c7	pod
bodem	bod	k1gInSc7	bod
tání	tání	k1gNnSc2	tání
krystalických	krystalický	k2eAgFnPc2d1	krystalická
břidlic	břidlice	k1gFnPc2	břidlice
nebo	nebo	k8xC	nebo
rul	rula	k1gFnPc2	rula
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Granitová	granitový	k2eAgFnSc1d1	granitová
tavenina	tavenina	k1gFnSc1	tavenina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
na	na	k7c6	na
vzniku	vznik	k1gInSc6	vznik
migmatitu	migmatit	k1gInSc2	migmatit
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
i	i	k9	i
metamorfní	metamorfní	k2eAgInSc4d1	metamorfní
původ	původ	k1gInSc4	původ
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
vzniká	vznikat	k5eAaImIp3nS	vznikat
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
selektivním	selektivní	k2eAgNnSc7d1	selektivní
tavením	tavení	k1gNnSc7	tavení
některých	některý	k3yIgFnPc2	některý
složek	složka	k1gFnPc2	složka
horniny	hornina	k1gFnSc2	hornina
<g/>
.	.	kIx.	.
</s>
<s>
Nově	nově	k6eAd1	nově
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
páskovaná	páskovaný	k2eAgFnSc1d1	páskovaná
textura	textura	k1gFnSc1	textura
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
paralelní	paralelní	k2eAgInSc1d1	paralelní
s	s	k7c7	s
metamorfní	metamorfní	k2eAgFnSc7d1	metamorfní
foliací	foliace	k1gFnSc7	foliace
původní	původní	k2eAgFnSc2d1	původní
horniny	hornina	k1gFnSc2	hornina
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
vzniku	vznik	k1gInSc6	vznik
migmatitů	migmatit	k1gInPc2	migmatit
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
mohou	moct	k5eAaImIp3nP	moct
podílet	podílet	k5eAaImF	podílet
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Injekce	injekce	k1gFnSc1	injekce
granitového	granitový	k2eAgNnSc2d1	granitové
magmatu	magma	k1gNnSc2	magma
vnikající	vnikající	k2eAgNnSc4d1	vnikající
do	do	k7c2	do
metamorfitů	metamorfit	k1gInPc2	metamorfit
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
nabývají	nabývat	k5eAaImIp3nP	nabývat
páskovaného	páskovaný	k2eAgMnSc4d1	páskovaný
vzhledu	vzhled	k1gInSc2	vzhled
</s>
</p>
<p>
<s>
Granitové	granitový	k2eAgNnSc1d1	granitové
magma	magma	k1gNnSc1	magma
vznikající	vznikající	k2eAgNnSc1d1	vznikající
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
selektivním	selektivní	k2eAgNnSc7d1	selektivní
tavením	tavení	k1gNnSc7	tavení
některých	některý	k3yIgFnPc2	některý
složek	složka	k1gFnPc2	složka
horniny	hornina	k1gFnSc2	hornina
</s>
</p>
<p>
<s>
Granitové	granitový	k2eAgFnPc1d1	granitová
vrstvičky	vrstvička	k1gFnPc1	vrstvička
vznikající	vznikající	k2eAgFnSc2d1	vznikající
metamorfní	metamorfní	k2eAgFnSc2d1	metamorfní
diferenciací	diferenciace	k1gFnSc7	diferenciace
tj.	tj.	kA	tj.
redistribucí	redistribuce	k1gFnSc7	redistribuce
minerálů	minerál	k1gInPc2	minerál
rekrystalizací	rekrystalizace	k1gFnPc2	rekrystalizace
v	v	k7c6	v
pevném	pevný	k2eAgInSc6d1	pevný
stavu	stav	k1gInSc6	stav
</s>
</p>
<p>
<s>
Granitové	granitový	k2eAgFnPc1d1	granitová
vrstvičky	vrstvička	k1gFnPc1	vrstvička
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vznikají	vznikat	k5eAaImIp3nP	vznikat
selektivní	selektivní	k2eAgFnSc7d1	selektivní
metamorfózou	metamorfóza	k1gFnSc7	metamorfóza
(	(	kIx(	(
<g/>
zatlačením	zatlačení	k1gNnSc7	zatlačení
<g/>
)	)	kIx)	)
vrstviček	vrstvička	k1gFnPc2	vrstvička
původní	původní	k2eAgFnSc2d1	původní
horniny	hornina	k1gFnSc2	hornina
</s>
</p>
<p>
<s>
==	==	k?	==
Dělení	dělení	k1gNnSc1	dělení
a	a	k8xC	a
označení	označení	k1gNnSc1	označení
migmatitů	migmatit	k1gInPc2	migmatit
==	==	k?	==
</s>
</p>
<p>
<s>
Při	při	k7c6	při
výzkumu	výzkum	k1gInSc6	výzkum
migmatitů	migmatit	k1gInPc2	migmatit
bylo	být	k5eAaImAgNnS	být
vytvořeno	vytvořit	k5eAaPmNgNnS	vytvořit
více	hodně	k6eAd2	hodně
druhů	druh	k1gMnPc2	druh
klasifikací	klasifikace	k1gFnPc2	klasifikace
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgNnSc1d2	podrobnější
členění	členění	k1gNnSc1	členění
se	se	k3xPyFc4	se
opírá	opírat	k5eAaImIp3nS	opírat
o	o	k7c4	o
formy	forma	k1gFnPc4	forma
a	a	k8xC	a
vztahy	vztah	k1gInPc1	vztah
obou	dva	k4xCgFnPc2	dva
hlavních	hlavní	k2eAgFnPc2d1	hlavní
složek	složka	k1gFnPc2	složka
tj.	tj.	kA	tj.
pevné	pevný	k2eAgFnPc4d1	pevná
složky	složka	k1gFnPc4	složka
a	a	k8xC	a
mobilní	mobilní	k2eAgFnPc4d1	mobilní
fáze	fáze	k1gFnPc4	fáze
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgInPc1d1	hlavní
typy	typ	k1gInPc1	typ
migmatitů	migmatit	k1gInPc2	migmatit
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Arterity	Arterit	k1gInPc1	Arterit
-	-	kIx~	-
mají	mít	k5eAaImIp3nP	mít
páskovanou	páskovaný	k2eAgFnSc4d1	páskovaná
texturu	textura	k1gFnSc4	textura
<g/>
,	,	kIx,	,
tvořenou	tvořený	k2eAgFnSc4d1	tvořená
střídáním	střídání	k1gNnSc7	střídání
světlých	světlý	k2eAgFnPc2d1	světlá
poloh	poloha	k1gFnPc2	poloha
neosomu	neosom	k1gInSc2	neosom
<g/>
,	,	kIx,	,
složeného	složený	k2eAgNnSc2d1	složené
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
křemene	křemen	k1gInSc2	křemen
<g/>
,	,	kIx,	,
živců	živec	k1gInPc2	živec
<g/>
,	,	kIx,	,
a	a	k8xC	a
tmavých	tmavý	k2eAgFnPc2d1	tmavá
poloh	poloha	k1gFnPc2	poloha
paleosomu	paleosom	k1gInSc2	paleosom
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
původní	původní	k2eAgFnSc3d1	původní
pararulové	pararulový	k2eAgFnSc3d1	pararulový
hornině	hornina	k1gFnSc3	hornina
<g/>
.	.	kIx.	.
</s>
<s>
Světlé	světlý	k2eAgFnPc1d1	světlá
polohy	poloha	k1gFnPc1	poloha
tvoří	tvořit	k5eAaImIp3nP	tvořit
do	do	k7c2	do
pararuly	pararula	k1gFnSc2	pararula
injektovaná	injektovaný	k2eAgFnSc1d1	injektovaná
cizorodá	cizorodý	k2eAgFnSc1d1	cizorodá
granitoidní	granitoidní	k2eAgFnSc1d1	granitoidní
tavenina	tavenina	k1gFnSc1	tavenina
pronikající	pronikající	k2eAgFnSc1d1	pronikající
podle	podle	k7c2	podle
ploch	plocha	k1gFnPc2	plocha
břidličnatosti	břidličnatost	k1gFnSc2	břidličnatost
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
tloušťka	tloušťka	k1gFnSc1	tloušťka
kolísá	kolísat	k5eAaImIp3nS	kolísat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
od	od	k7c2	od
1	[number]	k4	1
mm	mm	kA	mm
do	do	k7c2	do
několika	několik	k4yIc2	několik
decimetrů	decimetr	k1gInPc2	decimetr
<g/>
.	.	kIx.	.
</s>
<s>
Světlé	světlý	k2eAgFnPc1d1	světlá
a	a	k8xC	a
tmavé	tmavý	k2eAgFnPc1d1	tmavá
polohy	poloha	k1gFnPc1	poloha
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
výrazně	výrazně	k6eAd1	výrazně
ohraničeny	ohraničit	k5eAaPmNgInP	ohraničit
ostrými	ostrý	k2eAgFnPc7d1	ostrá
liniemi	linie	k1gFnPc7	linie
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
podobné	podobný	k2eAgInPc1d1	podobný
venitům	venit	k1gInPc3	venit
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
granitoidní	granitoidní	k2eAgInSc1d1	granitoidní
materiál	materiál	k1gInSc1	materiál
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
parciálním	parciální	k2eAgNnSc7d1	parciální
tavením	tavení	k1gNnSc7	tavení
původní	původní	k2eAgFnSc2d1	původní
horniny	hornina	k1gFnSc2	hornina
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ptygmatity	Ptygmatit	k1gInPc1	Ptygmatit
-	-	kIx~	-
mají	mít	k5eAaImIp3nP	mít
světlé	světlý	k2eAgFnPc1d1	světlá
polohy	poloha	k1gFnPc1	poloha
neosomu	neosom	k1gInSc2	neosom
detailně	detailně	k6eAd1	detailně
zvrásněné	zvrásněný	k2eAgFnPc1d1	zvrásněná
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
ptygmatické	ptygmatický	k2eAgFnPc4d1	ptygmatický
žilky	žilka	k1gFnPc4	žilka
je	být	k5eAaImIp3nS	být
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
rychlá	rychlý	k2eAgFnSc1d1	rychlá
změna	změna	k1gFnSc1	změna
tloušťky	tloušťka	k1gFnSc2	tloušťka
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
ptygmatických	ptygmatický	k2eAgFnPc2d1	ptygmatický
žilek	žilka	k1gFnPc2	žilka
se	se	k3xPyFc4	se
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
deformací	deformace	k1gFnSc7	deformace
před	před	k7c7	před
jejich	jejich	k3xOp3gNnSc7	jejich
ztuhnutím	ztuhnutí	k1gNnSc7	ztuhnutí
<g/>
,	,	kIx,	,
za	za	k7c2	za
plastického	plastický	k2eAgInSc2d1	plastický
stavu	stav	k1gInSc2	stav
horniny	hornina	k1gFnSc2	hornina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Agmatity	Agmatit	k1gInPc1	Agmatit
(	(	kIx(	(
<g/>
také	také	k9	také
merismitický	merismitický	k2eAgMnSc1d1	merismitický
migmatit	migmatit	k5eAaImF	migmatit
<g/>
)	)	kIx)	)
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
typ	typ	k1gInSc1	typ
migmatitů	migmatit	k1gInPc2	migmatit
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgMnPc6	který
injektovaná	injektovaný	k2eAgFnSc1d1	injektovaná
světlá	světlý	k2eAgFnSc1d1	světlá
složka	složka	k1gFnSc1	složka
(	(	kIx(	(
<g/>
leukosom	leukosom	k1gInSc1	leukosom
<g/>
)	)	kIx)	)
tmelí	tmelit	k5eAaImIp3nP	tmelit
brekciové	brekciový	k2eAgInPc1d1	brekciový
úlomky	úlomek	k1gInPc1	úlomek
původní	původní	k2eAgFnSc2d1	původní
horniny	hornina	k1gFnSc2	hornina
(	(	kIx(	(
<g/>
amfibolitu	amfibolit	k1gInSc2	amfibolit
<g/>
,	,	kIx,	,
biotitických	biotitický	k2eAgFnPc2d1	biotitická
rul	rula	k1gFnPc2	rula
<g/>
)	)	kIx)	)
a	a	k8xC	a
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
síť	síť	k1gFnSc4	síť
žil	žíla	k1gFnPc2	žíla
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nebulity	Nebulita	k1gFnPc1	Nebulita
(	(	kIx(	(
<g/>
také	také	k9	také
diatexity	diatexit	k2eAgFnPc1d1	diatexit
<g/>
)	)	kIx)	)
-	-	kIx~	-
představují	představovat	k5eAaImIp3nP	představovat
pokročilejší	pokročilý	k2eAgNnSc1d2	pokročilejší
stadium	stadium	k1gNnSc1	stadium
migmatitizace	migmatitizace	k1gFnSc2	migmatitizace
než	než	k8xS	než
agmatity	agmatit	k1gInPc4	agmatit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nebulitech	nebulit	k1gInPc6	nebulit
tvoří	tvořit	k5eAaImIp3nP	tvořit
původní	původní	k2eAgInSc4d1	původní
substrát	substrát	k1gInSc4	substrát
v	v	k7c6	v
luekosomu	luekosom	k1gInSc6	luekosom
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
málo	málo	k6eAd1	málo
výrazné	výrazný	k2eAgFnPc4d1	výrazná
tmavší	tmavý	k2eAgFnPc4d2	tmavší
šmouhy	šmouha	k1gFnPc4	šmouha
<g/>
,	,	kIx,	,
skvrny	skvrna	k1gFnPc4	skvrna
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nP	tvořit
hranici	hranice	k1gFnSc4	hranice
mezi	mezi	k7c7	mezi
migmatity	migmatit	k1gInPc7	migmatit
a	a	k8xC	a
granity	granit	k1gInPc7	granit
anatexického	anatexický	k2eAgInSc2d1	anatexický
původu	původ	k1gInSc2	původ
(	(	kIx(	(
<g/>
S-typ	Syp	k1gInSc1	S-typ
granitů	granit	k1gInPc2	granit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stiktolity	Stiktolit	k1gInPc1	Stiktolit
-	-	kIx~	-
představují	představovat	k5eAaImIp3nP	představovat
velmi	velmi	k6eAd1	velmi
pokročilý	pokročilý	k2eAgInSc4d1	pokročilý
stupeň	stupeň	k1gInSc4	stupeň
migmatitizace	migmatitizace	k1gFnSc2	migmatitizace
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
granitická	granitický	k2eAgFnSc1d1	granitická
složka	složka	k1gFnSc1	složka
výrazně	výrazně	k6eAd1	výrazně
převládá	převládat	k5eAaImIp3nS	převládat
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
původní	původní	k2eAgFnSc2d1	původní
metamorfovaných	metamorfovaný	k2eAgFnPc2d1	metamorfovaná
horniny	hornina	k1gFnSc2	hornina
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
už	už	k6eAd1	už
jen	jen	k9	jen
některé	některý	k3yIgInPc1	některý
minerální	minerální	k2eAgInPc1d1	minerální
relikty	relikt	k1gInPc1	relikt
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
textury	textura	k1gFnSc2	textura
se	se	k3xPyFc4	se
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
také	také	k9	také
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Metatexity	Metatexit	k2eAgInPc1d1	Metatexit
nebo	nebo	k8xC	nebo
stromatolitické	stromatolitický	k2eAgInPc1d1	stromatolitický
migmatity	migmatit	k1gInPc1	migmatit
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
mají	mít	k5eAaImIp3nP	mít
ostré	ostrý	k2eAgNnSc4d1	ostré
ohraničení	ohraničení	k1gNnSc4	ohraničení
světlých	světlý	k2eAgInPc2d1	světlý
a	a	k8xC	a
tmavých	tmavý	k2eAgInPc2d1	tmavý
pásů	pás	k1gInPc2	pás
</s>
</p>
<p>
<s>
Oftalmitické	Oftalmitický	k2eAgInPc1d1	Oftalmitický
migmatity	migmatit	k1gInPc1	migmatit
mají	mít	k5eAaImIp3nP	mít
okatě	okatě	k6eAd1	okatě
až	až	k9	až
čočkovou	čočkový	k2eAgFnSc4d1	čočková
texturu	textura	k1gFnSc4	textura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Merismitické	Merismitický	k2eAgInPc1d1	Merismitický
migmatity	migmatit	k1gInPc1	migmatit
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
Migmatity	Migmatit	k1gInPc1	Migmatit
jsou	být	k5eAaImIp3nP	být
časté	častý	k2eAgMnPc4d1	častý
v	v	k7c6	v
štítových	štítový	k2eAgFnPc6d1	štítová
oblastech	oblast	k1gFnPc6	oblast
prekambrického	prekambrický	k2eAgInSc2d1	prekambrický
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
u	u	k7c2	u
města	město	k1gNnSc2	město
Kristiansand	Kristiansanda	k1gFnPc2	Kristiansanda
v	v	k7c6	v
jižním	jižní	k2eAgNnSc6d1	jižní
Norsku	Norsko	k1gNnSc6	Norsko
<g/>
,	,	kIx,	,
východně	východně	k6eAd1	východně
od	od	k7c2	od
Stockholmu	Stockholm	k1gInSc2	Stockholm
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
či	či	k8xC	či
na	na	k7c6	na
více	hodně	k6eAd2	hodně
místech	místo	k1gNnPc6	místo
v	v	k7c6	v
jihovýchodním	jihovýchodní	k2eAgNnSc6d1	jihovýchodní
Finsku	Finsko	k1gNnSc6	Finsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
Migmatity	Migmatit	k1gInPc1	Migmatit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
ruly	rula	k1gFnSc2	rula
<g/>
,	,	kIx,	,
využívají	využívat	k5eAaImIp3nP	využívat
jako	jako	k9	jako
lomový	lomový	k2eAgInSc4d1	lomový
kámen	kámen	k1gInSc4	kámen
a	a	k8xC	a
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
drceného	drcený	k2eAgNnSc2d1	drcené
kameniva	kamenivo	k1gNnSc2	kamenivo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
těžit	těžit	k5eAaImF	těžit
i	i	k9	i
větší	veliký	k2eAgInPc4d2	veliký
bloky	blok	k1gInPc4	blok
nepopraskaných	popraskaný	k2eNgFnPc2d1	nepopraskaná
hornin	hornina	k1gFnPc2	hornina
je	být	k5eAaImIp3nS	být
lze	lze	k6eAd1	lze
využít	využít	k5eAaPmF	využít
jako	jako	k9	jako
dekorační	dekorační	k2eAgInSc4d1	dekorační
kámen	kámen	k1gInSc4	kámen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Migmatit	Migmatit	k1gFnSc2	Migmatit
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
migmatit	migmatit	k5eAaImF	migmatit
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
