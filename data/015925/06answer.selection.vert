<s>
Sociálnědemokratická	sociálnědemokratický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Německa	Německo	k1gNnSc2
(	(	kIx(
<g/>
německy	německy	k6eAd1
Sozialdemokratische	Sozialdemokratische	k1gNnSc1
Partei	Partei	k1gNnSc2
Deutschlands	Deutschlandsa	k1gFnPc2
<g/>
,	,	kIx,
zkratkou	zkratka	k1gFnSc7
SPD	SPD	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
německá	německý	k2eAgFnSc1d1
levicová	levicový	k2eAgFnSc1d1
politická	politický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
,	,	kIx,
nejstarší	starý	k2eAgFnSc1d3
a	a	k8xC
nejpočetnější	početní	k2eAgFnSc1d3
strana	strana	k1gFnSc1
v	v	k7c6
Německu	Německo	k1gNnSc6
a	a	k8xC
jedna	jeden	k4xCgFnSc1
z	z	k7c2
nejstarších	starý	k2eAgFnPc2d3
až	až	k9
do	do	k7c2
současnosti	současnost	k1gFnSc2
existujících	existující	k2eAgFnPc2d1
stran	strana	k1gFnPc2
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>