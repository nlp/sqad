<s>
Sociálnědemokratická	sociálnědemokratický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Německa	Německo	k1gNnSc2
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojován	ozdrojován	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yIgFnPc4,k3yRgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Sociálnědemokratická	sociálnědemokratický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
NěmeckaSozialdemokratische	NěmeckaSozialdemokratisch	k1gInSc2
Partei	Parte	k1gFnSc2
Deutschlands	Deutschlandsa	k1gFnPc2
Zkratka	zkratka	k1gFnSc1
</s>
<s>
SPD	SPD	kA
Datum	datum	k1gNnSc1
založení	založení	k1gNnSc2
</s>
<s>
23	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1863	#num#	k4
(	(	kIx(
ADAV	ADAV	kA
<g/>
)	)	kIx)
7	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1869	#num#	k4
(	(	kIx(
<g/>
SDAP	SDAP	kA
<g/>
)	)	kIx)
Předseda	předseda	k1gMnSc1
</s>
<s>
Norbert	Norbert	k1gMnSc1
Walter-Borjans	Walter-Borjans	k1gInSc1
a	a	k8xC
Saskia	Saskia	k1gFnSc1
Eskenová	Eskenový	k2eAgFnSc1d1
Sídlo	sídlo	k1gNnSc4
</s>
<s>
Willy-Brandt	Willy-Brandt	k2eAgMnSc1d1
-Haus	-Haus	k1gMnSc1
D-10911	D-10911	k1gFnSc2
Berlín	Berlín	k1gInSc1
Ideologie	ideologie	k1gFnSc2
</s>
<s>
sociální	sociální	k2eAgFnSc1d1
demokracie	demokracie	k1gFnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
proevropanismus	proevropanismus	k1gInSc1
frakce	frakce	k1gFnSc2
<g/>
:	:	kIx,
sociální	sociální	k2eAgInSc1d1
liberalismus	liberalismus	k1gInSc1
demokratický	demokratický	k2eAgInSc1d1
socialismus	socialismus	k1gInSc1
Politická	politický	k2eAgFnSc1d1
pozice	pozice	k1gFnSc1
</s>
<s>
středolevice	středolevice	k1gFnSc1
Mezinárodní	mezinárodní	k2eAgFnSc2d1
org	org	k?
<g/>
.	.	kIx.
</s>
<s>
Progresivní	progresivní	k2eAgFnSc1d1
aliance	aliance	k1gFnSc1
Evropská	evropský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
</s>
<s>
Strana	strana	k1gFnSc1
evropských	evropský	k2eAgMnPc2d1
socialistů	socialist	k1gMnPc2
Politická	politický	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
EP	EP	kA
</s>
<s>
Pokrokové	pokrokový	k2eAgNnSc4d1
spojenectví	spojenectví	k1gNnSc4
socialistů	socialist	k1gMnPc2
a	a	k8xC
demokratů	demokrat	k1gMnPc2
Počet	počet	k1gInSc1
členů	člen	k1gMnPc2
</s>
<s>
502	#num#	k4
062	#num#	k4
(	(	kIx(
<g/>
Do	do	k7c2
konce	konec	k1gInSc2
roku	rok	k1gInSc2
2010	#num#	k4
<g/>
)	)	kIx)
Barvy	barva	k1gFnSc2
</s>
<s>
červená	červenat	k5eAaImIp3nS
Volební	volební	k2eAgInSc1d1
výsledek	výsledek	k1gInSc1
</s>
<s>
27,3	27,3	k4
%	%	kIx~
(	(	kIx(
<g/>
EP	EP	kA
2014	#num#	k4
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
www.spd.de	www.spd.de	k6eAd1
</s>
<s>
Vlajka	vlajka	k1gFnSc1
strany	strana	k1gFnSc2
Zisk	zisk	k1gInSc4
mandátů	mandát	k1gInPc2
ve	v	k7c6
volbách	volba	k1gFnPc6
Německý	německý	k2eAgInSc1d1
spolkový	spolkový	k2eAgInSc1d1
sněm	sněm	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
153	#num#	k4
<g/>
/	/	kIx~
<g/>
709	#num#	k4
</s>
<s>
Zemský	zemský	k2eAgInSc1d1
sněm	sněm	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
601	#num#	k4
<g/>
/	/	kIx~
<g/>
1857	#num#	k4
</s>
<s>
Evropský	evropský	k2eAgInSc1d1
parlament	parlament	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
16	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
</s>
<s>
Sociálnědemokratická	sociálnědemokratický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Německa	Německo	k1gNnSc2
(	(	kIx(
<g/>
německy	německy	k6eAd1
Sozialdemokratische	Sozialdemokratische	k1gNnSc1
Partei	Partei	k1gNnSc2
Deutschlands	Deutschlandsa	k1gFnPc2
<g/>
,	,	kIx,
zkratkou	zkratka	k1gFnSc7
SPD	SPD	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
německá	německý	k2eAgFnSc1d1
levicová	levicový	k2eAgFnSc1d1
politická	politický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
,	,	kIx,
nejstarší	starý	k2eAgFnSc1d3
a	a	k8xC
nejpočetnější	početní	k2eAgFnSc1d3
strana	strana	k1gFnSc1
v	v	k7c6
Německu	Německo	k1gNnSc6
a	a	k8xC
jedna	jeden	k4xCgFnSc1
z	z	k7c2
nejstarších	starý	k2eAgFnPc2d3
až	až	k9
do	do	k7c2
současnosti	současnost	k1gFnSc2
existujících	existující	k2eAgFnPc2d1
stran	strana	k1gFnPc2
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Ačkoliv	ačkoliv	k8xS
má	mít	k5eAaImIp3nS
strana	strana	k1gFnSc1
své	svůj	k3xOyFgInPc4
kořeny	kořen	k1gInPc4
v	v	k7c6
dělnickém	dělnický	k2eAgNnSc6d1
hnutí	hnutí	k1gNnSc6
(	(	kIx(
<g/>
je	být	k5eAaImIp3nS
například	například	k6eAd1
členem	člen	k1gMnSc7
Socialistické	socialistický	k2eAgFnSc2d1
internacionály	internacionála	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
mění	měnit	k5eAaImIp3nS
se	se	k3xPyFc4
zejména	zejména	k9
od	od	k7c2
doby	doba	k1gFnSc2
své	svůj	k3xOyFgFnSc2
účasti	účast	k1gFnSc2
na	na	k7c6
západoněmeckých	západoněmecký	k2eAgFnPc6d1
vládách	vláda	k1gFnPc6
ve	v	k7c4
stranu	strana	k1gFnSc4
liberálně	liberálně	k6eAd1
středolevé	středolevý	k2eAgFnSc2d1
orientace	orientace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
SPD	SPD	kA
má	mít	k5eAaImIp3nS
i	i	k9
svou	svůj	k3xOyFgFnSc4
organizaci	organizace	k1gFnSc4
pro	pro	k7c4
mladé	mladá	k1gFnPc4
-	-	kIx~
členové	člen	k1gMnPc1
mladší	mladý	k2eAgMnPc1d2
35	#num#	k4
let	léto	k1gNnPc2
jsou	být	k5eAaImIp3nP
organizováni	organizován	k2eAgMnPc1d1
jako	jako	k8xS,k8xC
Jusos	Jusos	k1gMnSc1
(	(	kIx(
<g/>
Mladí	mladý	k2eAgMnPc1d1
socialisté	socialist	k1gMnPc1
v	v	k7c6
SPD	SPD	kA
<g/>
,	,	kIx,
německy	německy	k6eAd1
Jungsozialisten	Jungsozialisten	k2eAgInSc1d1
in	in	k?
der	drát	k5eAaImRp2nS
SPD	SPD	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Spolupředsedy	spolupředseda	k1gMnPc7
strany	strana	k1gFnSc2
jsou	být	k5eAaImIp3nP
od	od	k7c2
prosince	prosinec	k1gInSc2
2019	#num#	k4
Norbert	Norberta	k1gFnPc2
Walter-Borjans	Walter-Borjansa	k1gFnPc2
a	a	k8xC
Saskia	Saskia	k1gFnSc1
Eskenová	Eskenová	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Do	do	k7c2
vzniku	vznik	k1gInSc2
republiky	republika	k1gFnSc2
(	(	kIx(
<g/>
1863	#num#	k4
<g/>
–	–	k?
<g/>
1919	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Strana	strana	k1gFnSc1
za	za	k7c4
datum	datum	k1gNnSc4
svého	svůj	k3xOyFgNnSc2
založení	založení	k1gNnSc2
považuje	považovat	k5eAaImIp3nS
23	#num#	k4
<g/>
.	.	kIx.
květen	květen	k1gInSc4
1863	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byl	být	k5eAaImAgInS
v	v	k7c6
Eisenachu	Eisenach	k1gInSc6
založen	založit	k5eAaPmNgInS
Ferdinandem	Ferdinand	k1gMnSc7
Lassallem	Lassall	k1gMnSc7
liberálnědemokratický	liberálnědemokratický	k2eAgInSc4d1
Sněm	sněm	k1gInSc4
německých	německý	k2eAgInPc2d1
dělnických	dělnický	k2eAgInPc2d1
spolků	spolek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1869	#num#	k4
pak	pak	k6eAd1
byl	být	k5eAaImAgInS
Augustem	August	k1gMnSc7
Bebelem	Bebel	k1gMnSc7
přetvořen	přetvořit	k5eAaPmNgMnS
na	na	k7c4
Sociálnědemokratickou	sociálnědemokratický	k2eAgFnSc4d1
dělnickou	dělnický	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tzv.	tzv.	kA
protisocialistickými	protisocialistický	k2eAgInPc7d1
zákony	zákon	k1gInPc7
Otto	Otto	k1gMnSc1
von	von	k1gInSc4
Bismarcka	Bismarcko	k1gNnSc2
z	z	k7c2
roku	rok	k1gInSc2
1878	#num#	k4
byly	být	k5eAaImAgFnP
socialisté	socialist	k1gMnPc1
zakázáni	zakázán	k2eAgMnPc1d1
<g/>
,	,	kIx,
především	především	k6eAd1
kvůli	kvůli	k7c3
jejich	jejich	k3xOp3gInPc3
prorevolučním	prorevoluční	k2eAgInPc3d1
a	a	k8xC
protimonarchistickým	protimonarchistický	k2eAgInPc3d1
názorům	názor	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1890	#num#	k4
byla	být	k5eAaImAgFnS
však	však	k9
strana	strana	k1gFnSc1
opět	opět	k6eAd1
povolena	povolit	k5eAaPmNgFnS
a	a	k8xC
změnila	změnit	k5eAaPmAgFnS
své	svůj	k3xOyFgNnSc4
jméno	jméno	k1gNnSc4
na	na	k7c6
SPD	SPD	kA
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
reakci	reakce	k1gFnSc6
na	na	k7c4
vyloučení	vyloučení	k1gNnSc4
z	z	k7c2
politického	politický	k2eAgNnSc2d1
spektra	spektrum	k1gNnSc2
byl	být	k5eAaImAgInS
nový	nový	k2eAgInSc1d1
Erfurtský	erfurtský	k2eAgInSc1d1
program	program	k1gInSc1
SPD	SPD	kA
z	z	k7c2
roku	rok	k1gInSc2
1891	#num#	k4
mnohem	mnohem	k6eAd1
radikálnější	radikální	k2eAgInSc4d2
než	než	k8xS
původní	původní	k2eAgInSc4d1
Gothajský	gothajský	k2eAgInSc4d1
program	program	k1gInSc4
z	z	k7c2
roku	rok	k1gInSc2
1875	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
přelomu	přelom	k1gInSc6
století	století	k1gNnSc2
v	v	k7c6
SPD	SPD	kA
zuřil	zuřit	k5eAaImAgInS
ideový	ideový	k2eAgInSc1d1
boj	boj	k1gInSc1
mezi	mezi	k7c7
orthodoxním	orthodoxní	k2eAgInSc7d1
marxismem	marxismus	k1gInSc7
(	(	kIx(
<g/>
Karl	Karl	k1gMnSc1
Kautsky	Kautsky	k1gMnSc1
a	a	k8xC
Rosa	Rosa	k1gFnSc1
Luxemburgová	Luxemburgový	k2eAgFnSc1d1
<g/>
)	)	kIx)
a	a	k8xC
revizionismem	revizionismus	k1gInSc7
(	(	kIx(
<g/>
Eduard	Eduard	k1gMnSc1
Bernstein	Bernstein	k1gMnSc1
<g/>
,	,	kIx,
Friedrich	Friedrich	k1gMnSc1
Ebert	Ebert	k1gInSc1
a	a	k8xC
Philipp	Philipp	k1gMnSc1
Scheidemann	Scheidemann	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1903	#num#	k4
byli	být	k5eAaImAgMnP
sice	sice	k8xC
revizionisté	revizionista	k1gMnPc1
poraženi	poražen	k2eAgMnPc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
jejich	jejich	k3xOp3gFnSc1
pozice	pozice	k1gFnSc1
stále	stále	k6eAd1
sílila	sílit	k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s>
Síla	síla	k1gFnSc1
socialistů	socialist	k1gMnPc2
postupně	postupně	k6eAd1
narůstala	narůstat	k5eAaImAgFnS
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1912	#num#	k4
se	se	k3xPyFc4
stali	stát	k5eAaPmAgMnP
nejsilnější	silný	k2eAgFnSc7d3
stranou	strana	k1gFnSc7
v	v	k7c6
parlamentu	parlament	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
však	však	k9
neměli	mít	k5eNaImAgMnP
žádný	žádný	k3yNgInSc4
podíl	podíl	k1gInSc4
na	na	k7c6
vládě	vláda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
jedna	jeden	k4xCgFnSc1
z	z	k7c2
nejsilnějších	silný	k2eAgFnPc2d3
stran	strana	k1gFnPc2
musela	muset	k5eAaImAgFnS
SPD	SPD	kA
převzít	převzít	k5eAaPmF
díl	díl	k1gInSc4
odpovědnosti	odpovědnost	k1gFnSc2
za	za	k7c4
financování	financování	k1gNnSc4
válečného	válečný	k2eAgNnSc2d1
úsilí	úsilí	k1gNnSc2
Německa	Německo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
novým	nový	k2eAgNnSc7d1
dělicím	dělicí	k2eAgNnSc7d1
kritériem	kritérion	k1gNnSc7
<g/>
,	,	kIx,
když	když	k8xS
i	i	k9
někteří	některý	k3yIgMnPc1
bývalí	bývalý	k2eAgMnPc1d1
revizionisté	revizionista	k1gMnPc1
(	(	kIx(
<g/>
Bernstein	Bernstein	k1gMnSc1
<g/>
)	)	kIx)
byli	být	k5eAaImAgMnP
pacifisty	pacifista	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
vedlo	vést	k5eAaImAgNnS
k	k	k7c3
rozkolu	rozkol	k1gInSc3
v	v	k7c6
SPD	SPD	kA
<g/>
,	,	kIx,
v	v	k7c6
jehož	jehož	k3xOyRp3gInSc6
důsledku	důsledek	k1gInSc6
4	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1917	#num#	k4
založila	založit	k5eAaPmAgFnS
skupina	skupina	k1gFnSc1
vyloučených	vyloučený	k2eAgMnPc2d1
členů	člen	k1gMnPc2
Nezávislou	závislý	k2eNgFnSc4d1
sociálně	sociálně	k6eAd1
demokratickou	demokratický	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
Německa	Německo	k1gNnSc2
(	(	kIx(
<g/>
Unabhängige	Unabhängige	k1gNnSc1
Sozialdemokratische	Sozialdemokratisch	k1gInSc2
Partei	Parte	k1gFnSc2
Deutschlands	Deutschlandsa	k1gFnPc2
<g/>
,	,	kIx,
USPD	USPD	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc1
předsedou	předseda	k1gMnSc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
Hugo	Hugo	k1gMnSc1
Hasse	Hasse	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
August	August	k1gMnSc1
Bebel	Bebel	k1gMnSc1
(	(	kIx(
<g/>
1863	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1
Lassalle	Lassalle	k1gFnSc2
(	(	kIx(
<g/>
1860	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Spolkový	spolkový	k2eAgMnSc1d1
kancléř	kancléř	k1gMnSc1
Willy	Willa	k1gFnSc2
Brandt	Brandt	k1gMnSc1
po	po	k7c6
svém	svůj	k3xOyFgNnSc6
odstoupení	odstoupení	k1gNnSc6
(	(	kIx(
<g/>
1980	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Současný	současný	k2eAgMnSc1d1
spolkový	spolkový	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
Frank-Walter	Frank-Walter	k1gMnSc1
Steinmeier	Steinmeier	k1gMnSc1
</s>
<s>
Výmarská	výmarský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
(	(	kIx(
<g/>
1919	#num#	k4
<g/>
–	–	k?
<g/>
1933	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Po	po	k7c6
vzniku	vznik	k1gInSc6
republiky	republika	k1gFnSc2
se	se	k3xPyFc4
radikálnější	radikální	k2eAgFnSc1d2
část	část	k1gFnSc1
nezávislých	závislý	k2eNgMnPc2d1
sociálních	sociální	k2eAgMnPc2d1
demokratů	demokrat	k1gMnPc2
oddělila	oddělit	k5eAaPmAgFnS
od	od	k7c2
USPD	USPD	kA
a	a	k8xC
založila	založit	k5eAaPmAgFnS
Komunistickou	komunistický	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
Německa	Německo	k1gNnSc2
(	(	kIx(
<g/>
Kommunistische	Kommunistische	k1gInSc1
Partei	Partei	k1gNnSc1
Deutschlands	Deutschlands	k1gInSc1
<g/>
,	,	kIx,
KPD	KPD	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
později	pozdě	k6eAd2
stala	stát	k5eAaPmAgFnS
největším	veliký	k2eAgMnSc7d3
protivníkem	protivník	k1gMnSc7
sociálních	sociální	k2eAgMnPc2d1
demokratů	demokrat	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
let	léto	k1gNnPc2
1919	#num#	k4
<g/>
–	–	k?
<g/>
1920	#num#	k4
však	však	k9
zůstala	zůstat	k5eAaPmAgFnS
hlavním	hlavní	k2eAgMnSc7d1
oponentem	oponent	k1gMnSc7
SPD	SPD	kA
stále	stále	k6eAd1
významnější	významný	k2eAgFnSc4d2
USPD	USPD	kA
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
získala	získat	k5eAaPmAgFnS
na	na	k7c4
úkor	úkor	k1gInSc4
SPD	SPD	kA
750	#num#	k4
000	#num#	k4
členů	člen	k1gMnPc2
a	a	k8xC
srovnatelný	srovnatelný	k2eAgInSc1d1
volební	volební	k2eAgInSc1d1
výsledek	výsledek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
roku	rok	k1gInSc2
1920	#num#	k4
proběhla	proběhnout	k5eAaPmAgFnS
v	v	k7c6
USPD	USPD	kA
debata	debata	k1gFnSc1
o	o	k7c6
vztahu	vztah	k1gInSc6
ke	k	k7c3
Kominterně	Kominterna	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
důsledku	důsledek	k1gInSc6
toho	ten	k3xDgNnSc2
se	se	k3xPyFc4
od	od	k7c2
USPD	USPD	kA
oddělilo	oddělit	k5eAaPmAgNnS
levé	levý	k2eAgNnSc1d1
křídlo	křídlo	k1gNnSc1
vedené	vedený	k2eAgNnSc1d1
Ernstem	Ernst	k1gMnSc7
Thälmannem	Thälmann	k1gMnSc7
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
se	se	k3xPyFc4
sloučilo	sloučit	k5eAaPmAgNnS
s	s	k7c7
KPD	KPD	kA
<g/>
,	,	kIx,
a	a	k8xC
vznikla	vzniknout	k5eAaPmAgFnS
tak	tak	k6eAd1
Sjednocená	sjednocený	k2eAgFnSc1d1
komunistická	komunistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Německa	Německo	k1gNnSc2
(	(	kIx(
<g/>
Vereinigte	Vereinigt	k1gMnSc5
Kommunistische	Kommunistischus	k1gMnSc5
Partei	Parte	k1gInSc3
Deutschlands	Deutschlands	k1gInSc1
<g/>
,	,	kIx,
VKPD	VKPD	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
z	z	k7c2
níž	jenž	k3xRgFnSc2
se	se	k3xPyFc4
později	pozdě	k6eAd2
stala	stát	k5eAaPmAgFnS
masová	masový	k2eAgFnSc1d1
KPD	KPD	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
ze	z	k7c2
zbývajících	zbývající	k2eAgMnPc2d1
členů	člen	k1gMnPc2
USPD	USPD	kA
se	s	k7c7
24	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1922	#num#	k4
sloučila	sloučit	k5eAaPmAgFnS
s	s	k7c7
SPD	SPD	kA
<g/>
,	,	kIx,
dali	dát	k5eAaPmAgMnP
tak	tak	k6eAd1
vzniknout	vzniknout	k5eAaPmF
VSPD	VSPD	kA
(	(	kIx(
<g/>
Vereinigte	Vereinigt	k1gMnSc5
Sozialdemokratische	Sozialdemokratischus	k1gMnSc5
Partei	Parte	k1gInSc3
Deutschlands	Deutschlands	k1gInSc1
<g/>
,	,	kIx,
Sjednocená	sjednocený	k2eAgFnSc1d1
sociálnědemokratická	sociálnědemokratický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Německa	Německo	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
SPD	SPD	kA
byla	být	k5eAaImAgFnS
součástí	součást	k1gFnSc7
tzv.	tzv.	kA
výmarské	výmarský	k2eAgFnSc2d1
koalice	koalice	k1gFnSc2
podporující	podporující	k2eAgNnSc4d1
republikánské	republikánský	k2eAgNnSc4d1
zřízení	zřízení	k1gNnSc4
<g/>
,	,	kIx,
komunisté	komunista	k1gMnPc1
se	se	k3xPyFc4
stavěli	stavět	k5eAaImAgMnP
proti	proti	k7c3
novému	nový	k2eAgInSc3d1
režimu	režim	k1gInSc3
<g/>
.	.	kIx.
</s>
<s>
Nacistické	nacistický	k2eAgNnSc1d1
období	období	k1gNnSc1
(	(	kIx(
<g/>
1933	#num#	k4
<g/>
–	–	k?
<g/>
1945	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Po	po	k7c6
nástupu	nástup	k1gInSc6
Adolfa	Adolf	k1gMnSc2
Hitlera	Hitler	k1gMnSc2
k	k	k7c3
moci	moc	k1gFnSc3
byla	být	k5eAaImAgFnS
strana	strana	k1gFnSc1
v	v	k7c6
dubnu	duben	k1gInSc6
1933	#num#	k4
rozpuštěna	rozpustit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gMnSc1
představitelé	představitel	k1gMnPc1
byli	být	k5eAaImAgMnP
vězněni	vězněn	k2eAgMnPc1d1
v	v	k7c6
koncentračních	koncentrační	k2eAgInPc6d1
táborech	tábor	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Část	část	k1gFnSc1
členů	člen	k1gInPc2
SPD	SPD	kA
bojovala	bojovat	k5eAaImAgFnS
ve	v	k7c6
španělské	španělský	k2eAgFnSc6d1
občanské	občanský	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Poválečné	poválečný	k2eAgNnSc1d1
období	období	k1gNnSc1
od	od	k7c2
roku	rok	k1gInSc2
1945	#num#	k4
</s>
<s>
Strana	strana	k1gFnSc1
byla	být	k5eAaImAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1945	#num#	k4
obnovena	obnoven	k2eAgNnPc4d1
ve	v	k7c6
všech	všecek	k3xTgFnPc6
okupačních	okupační	k2eAgFnPc6d1
zónách	zóna	k1gFnPc6
Německa	Německo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
sovětské	sovětský	k2eAgFnSc6d1
zóně	zóna	k1gFnSc6
však	však	k8xC
byla	být	k5eAaImAgFnS
pod	pod	k7c7
nátlakem	nátlak	k1gInSc7
sloučena	sloučen	k2eAgFnSc1d1
s	s	k7c7
Komunistickou	komunistický	k2eAgFnSc7d1
stranou	strana	k1gFnSc7
Německa	Německo	k1gNnSc2
(	(	kIx(
<g/>
KPD	KPD	kA
<g/>
)	)	kIx)
a	a	k8xC
z	z	k7c2
toho	ten	k3xDgNnSc2
vzniklá	vzniklý	k2eAgFnSc1d1
Sjednocená	sjednocený	k2eAgFnSc1d1
socialistická	socialistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Německa	Německo	k1gNnSc2
(	(	kIx(
<g/>
SED	sed	k1gInSc1
<g/>
)	)	kIx)
vládla	vládnout	k5eAaImAgFnS
v	v	k7c6
NDR	NDR	kA
pod	pod	k7c7
sovětským	sovětský	k2eAgInSc7d1
vlivem	vliv	k1gInSc7
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1989	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Západním	západní	k2eAgNnSc6d1
Německu	Německo	k1gNnSc6
byla	být	k5eAaImAgFnS
SPD	SPD	kA
dlouhá	dlouhý	k2eAgNnPc4d1
léta	léto	k1gNnPc4
v	v	k7c6
opozici	opozice	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1966	#num#	k4
vstoupila	vstoupit	k5eAaPmAgFnS
s	s	k7c7
CDU	CDU	kA
a	a	k8xC
CSU	CSU	kA
do	do	k7c2
tzv.	tzv.	kA
velké	velký	k2eAgFnSc2d1
koalice	koalice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoliv	ačkoliv	k8xS
nevyhrála	vyhrát	k5eNaPmAgFnS
volby	volba	k1gFnPc4
<g/>
,	,	kIx,
vytvořila	vytvořit	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1969	#num#	k4
koalici	koalice	k1gFnSc4
s	s	k7c7
FDP	FDP	kA
a	a	k8xC
její	její	k3xOp3gMnSc1
předseda	předseda	k1gMnSc1
Willy	Willa	k1gFnSc2
Brandt	Brandt	k1gMnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
spolkovým	spolkový	k2eAgMnSc7d1
kancléřem	kancléř	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1972	#num#	k4
(	(	kIx(
<g/>
poprvé	poprvé	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
1949	#num#	k4
<g/>
)	)	kIx)
vyhrála	vyhrát	k5eAaPmAgFnS
volby	volba	k1gFnPc4
a	a	k8xC
vládla	vládnout	k5eAaImAgFnS
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1982	#num#	k4
v	v	k7c6
koalici	koalice	k1gFnSc6
s	s	k7c7
liberály	liberál	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
Brandtově	Brandtův	k2eAgNnSc6d1
odstoupení	odstoupení	k1gNnSc6
v	v	k7c6
roce	rok	k1gInSc6
1974	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
Helmut	Helmut	k1gMnSc1
Schmidt	Schmidt	k1gMnSc1
druhým	druhý	k4xOgNnSc7
poválečným	poválečný	k2eAgMnSc7d1
kancléřem	kancléř	k1gMnSc7
z	z	k7c2
řad	řada	k1gFnPc2
SPD	SPD	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
tento	tento	k3xDgInSc4
úřad	úřad	k1gInSc4
přišel	přijít	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1982	#num#	k4
<g/>
,	,	kIx,
když	když	k8xS
FDP	FDP	kA
pod	pod	k7c7
vedením	vedení	k1gNnSc7
ministra	ministr	k1gMnSc2
zahraničí	zahraničí	k1gNnSc2
Hanse-Dietricha	Hanse-Dietrich	k1gMnSc2
Genschera	Genscher	k1gMnSc2
změnila	změnit	k5eAaPmAgFnS
strany	strana	k1gFnSc2
a	a	k8xC
dopomohla	dopomoct	k5eAaPmAgFnS
Helmutu	Helmut	k1gMnSc3
Kohlovi	Kohl	k1gMnSc3
k	k	k7c3
vedení	vedení	k1gNnSc3
vlády	vláda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Znovu	znovu	k6eAd1
k	k	k7c3
moci	moc	k1gFnSc3
se	se	k3xPyFc4
SPD	SPD	kA
na	na	k7c6
sedm	sedm	k4xCc4
let	léto	k1gNnPc2
dostala	dostat	k5eAaPmAgFnS
až	až	k9
v	v	k7c6
roce	rok	k1gInSc6
1998	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
koalici	koalice	k1gFnSc6
se	s	k7c7
Zelenými	zelený	k2eAgFnPc7d1
se	se	k3xPyFc4
svým	svůj	k3xOyFgMnSc7
předsedou	předseda	k1gMnSc7
Gerhardem	Gerhard	k1gMnSc7
Schröderem	Schröder	k1gMnSc7
jako	jako	k8xS,k8xC
kancléřem	kancléř	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Schröder	Schröder	k1gInSc1
ztratil	ztratit	k5eAaPmAgInS
tento	tento	k3xDgInSc4
úřad	úřad	k1gInSc4
po	po	k7c6
těsné	těsný	k2eAgFnSc6d1
porážce	porážka	k1gFnSc6
v	v	k7c6
předčasných	předčasný	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
ve	v	k7c4
prospěch	prospěch	k1gInSc4
současné	současný	k2eAgFnSc2d1
kancléřky	kancléřka	k1gFnSc2
Angely	Angela	k1gFnSc2
Merkelové	Merkelová	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
2005	#num#	k4
<g/>
–	–	k?
<g/>
2009	#num#	k4
byla	být	k5eAaImAgFnS
SPD	SPD	kA
zastoupena	zastoupit	k5eAaPmNgNnP
ve	v	k7c6
spolkové	spolkový	k2eAgFnSc6d1
vládě	vláda	k1gFnSc6
ve	v	k7c6
velké	velký	k2eAgFnSc6d1
koalici	koalice	k1gFnSc6
s	s	k7c7
CDU	CDU	kA
<g/>
/	/	kIx~
<g/>
CSU	CSU	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
volbách	volba	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
přešla	přejít	k5eAaPmAgFnS
do	do	k7c2
opozice	opozice	k1gFnSc2
kvůli	kvůli	k7c3
vítězství	vítězství	k1gNnSc3
CDU	CDU	kA
<g/>
/	/	kIx~
<g/>
CSU	CSU	kA
a	a	k8xC
FDP	FDP	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nová	nový	k2eAgFnSc1d1
příležitost	příležitost	k1gFnSc1
pro	pro	k7c4
účast	účast	k1gFnSc4
na	na	k7c6
vládě	vláda	k1gFnSc6
se	se	k3xPyFc4
pro	pro	k7c4
SPD	SPD	kA
naskytla	naskytnout	k5eAaPmAgFnS
po	po	k7c6
volbách	volba	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
FDP	FDP	kA
nedostala	dostat	k5eNaPmAgFnS
do	do	k7c2
Spolkového	spolkový	k2eAgInSc2d1
sněmu	sněm	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
třetí	třetí	k4xOgFnSc6
vládě	vláda	k1gFnSc6
Angely	Angela	k1gFnSc2
Merkelové	Merkelová	k1gFnSc2
obdržel	obdržet	k5eAaPmAgMnS
tehdejší	tehdejší	k2eAgMnSc1d1
předseda	předseda	k1gMnSc1
SPD	SPD	kA
Sigmar	Sigmar	k1gMnSc1
Gabriel	Gabriel	k1gMnSc1
křeslo	křeslo	k1gNnSc4
ministra	ministr	k1gMnSc2
hospodářství	hospodářství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Frank-Walter	Frank-Walter	k1gMnSc1
Steinmeier	Steinmeier	k1gMnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
nejprve	nejprve	k6eAd1
ministrem	ministr	k1gMnSc7
zahraničí	zahraničí	k1gNnSc2
<g/>
,	,	kIx,
začátkem	začátkem	k7c2
roku	rok	k1gInSc2
2017	#num#	k4
spolkovým	spolkový	k2eAgMnSc7d1
prezidentem	prezident	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Andrea	Andrea	k1gFnSc1
Nahlesová	Nahlesový	k2eAgFnSc1d1
<g/>
,	,	kIx,
předsedkyně	předsedkyně	k1gFnSc1
SPD	SPD	kA
po	po	k7c6
sjezdu	sjezd	k1gInSc6
strany	strana	k1gFnSc2
v	v	k7c6
dubnu	duben	k1gInSc6
2018	#num#	k4
</s>
<s>
Volby	volba	k1gFnPc1
do	do	k7c2
Německého	německý	k2eAgInSc2d1
spolkového	spolkový	k2eAgInSc2d1
sněmu	sněm	k1gInSc2
2017	#num#	k4
</s>
<s>
Ve	v	k7c6
volbách	volba	k1gFnPc6
do	do	k7c2
Německého	německý	k2eAgInSc2d1
spolkového	spolkový	k2eAgInSc2d1
sněmu	sněm	k1gInSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgFnPc1
se	se	k3xPyFc4
konaly	konat	k5eAaImAgFnP
dne	den	k1gInSc2
24	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2017	#num#	k4
<g/>
,	,	kIx,
se	s	k7c7
SPD	SPD	kA
<g/>
,	,	kIx,
tehdy	tehdy	k6eAd1
pod	pod	k7c7
vedením	vedení	k1gNnSc7
předsedy	předseda	k1gMnSc2
Martina	Martin	k1gMnSc2
Schulze	Schulz	k1gMnSc2
<g/>
,	,	kIx,
umístila	umístit	k5eAaPmAgFnS
s	s	k7c7
neuspokojivým	uspokojivý	k2eNgInSc7d1
ziskem	zisk	k1gInSc7
20,5	20,5	k4
%	%	kIx~
odevzdaných	odevzdaný	k2eAgInPc2d1
hlasů	hlas	k1gInPc2
na	na	k7c6
druhém	druhý	k4xOgNnSc6
místě	místo	k1gNnSc6
za	za	k7c4
CDU	CDU	kA
<g/>
/	/	kIx~
<g/>
CSU	CSU	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Strana	strana	k1gFnSc1
získala	získat	k5eAaPmAgFnS
pouze	pouze	k6eAd1
153	#num#	k4
poslaneckých	poslanecký	k2eAgInPc2d1
mandátů	mandát	k1gInPc2
<g/>
,	,	kIx,
podstatně	podstatně	k6eAd1
méně	málo	k6eAd2
než	než	k8xS
ve	v	k7c6
volbách	volba	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
volbách	volba	k1gFnPc6
2017	#num#	k4
se	se	k3xPyFc4
SPD	SPD	kA
nejprve	nejprve	k6eAd1
rozhodla	rozhodnout	k5eAaPmAgFnS
odejít	odejít	k5eAaPmF
do	do	k7c2
opozice	opozice	k1gFnSc2
a	a	k8xC
nezúčastnit	zúčastnit	k5eNaPmF
se	se	k3xPyFc4
jednání	jednání	k1gNnPc1
o	o	k7c6
nové	nový	k2eAgFnSc6d1
vládní	vládní	k2eAgFnSc6d1
koalici	koalice	k1gFnSc6
s	s	k7c7
CDU	CDU	kA
<g/>
/	/	kIx~
<g/>
CSU	CSU	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
některých	některý	k3yIgInPc6
postech	post	k1gInPc6
ve	v	k7c6
vedení	vedení	k1gNnSc6
strany	strana	k1gFnSc2
byly	být	k5eAaImAgFnP
brzy	brzy	k6eAd1
po	po	k7c6
parlamentních	parlamentní	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
provedeny	proveden	k2eAgFnPc1d1
personální	personální	k2eAgFnPc4d1
změny	změna	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
byla	být	k5eAaImAgFnS
do	do	k7c2
důležité	důležitý	k2eAgFnSc2d1
funkce	funkce	k1gFnSc2
předsedkyně	předsedkyně	k1gFnSc2
poslaneckého	poslanecký	k2eAgInSc2d1
klubu	klub	k1gInSc2
ve	v	k7c6
Spolkovém	spolkový	k2eAgInSc6d1
sněmu	sněm	k1gInSc6
a	a	k8xC
posléze	posléze	k6eAd1
22	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2018	#num#	k4
také	také	k9
na	na	k7c4
post	post	k1gInSc4
předsedkyně	předsedkyně	k1gFnSc2
SPD	SPD	kA
zvolena	zvolen	k2eAgFnSc1d1
Andrea	Andrea	k1gFnSc1
Nahlesová	Nahlesový	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgNnPc1
byla	být	k5eAaImAgNnP
až	až	k6eAd1
do	do	k7c2
svého	svůj	k3xOyFgNnSc2
zvolení	zvolení	k1gNnSc2
předsedkyní	předsedkyně	k1gFnPc2
poslaneckého	poslanecký	k2eAgInSc2d1
klubu	klub	k1gInSc2
spolkovou	spolkový	k2eAgFnSc7d1
ministryní	ministryně	k1gFnSc7
práce	práce	k1gFnSc2
a	a	k8xC
sociálních	sociální	k2eAgFnPc2d1
věcí	věc	k1gFnPc2
<g/>
;	;	kIx,
z	z	k7c2
tohoto	tento	k3xDgInSc2
úřadu	úřad	k1gInSc2
poté	poté	k6eAd1
odstoupila	odstoupit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
ztroskotání	ztroskotání	k1gNnSc6
pokusu	pokus	k1gInSc2
CDU	CDU	kA
<g/>
/	/	kIx~
<g/>
CSU	CSU	kA
o	o	k7c4
tzv.	tzv.	kA
jamajskou	jamajský	k2eAgFnSc4d1
koalici	koalice	k1gFnSc4
s	s	k7c7
FDP	FDP	kA
a	a	k8xC
Zelenými	zelený	k2eAgFnPc7d1
se	se	k3xPyFc4
však	však	k9
SPD	SPD	kA
rozhodla	rozhodnout	k5eAaPmAgFnS
jinak	jinak	k6eAd1
<g/>
,	,	kIx,
a	a	k8xC
sice	sice	k8xC
přece	přece	k9
jen	jen	k9
pro	pro	k7c4
tzv.	tzv.	kA
„	„	k?
<g/>
velkou	velký	k2eAgFnSc4d1
koalici	koalice	k1gFnSc4
<g/>
“	“	k?
s	s	k7c7
křesťanskými	křesťanský	k2eAgFnPc7d1
stranami	strana	k1gFnPc7
pod	pod	k7c7
vedením	vedení	k1gNnSc7
úřadující	úřadující	k2eAgFnSc2d1
kancléřky	kancléřka	k1gFnSc2
Angely	Angela	k1gFnSc2
Merkelové	Merkelová	k1gFnSc2
(	(	kIx(
<g/>
Große	Große	k1gFnSc1
Koalition	Koalition	k1gInSc1
<g/>
,	,	kIx,
všeobecně	všeobecně	k6eAd1
zvaná	zvaný	k2eAgFnSc1d1
Groko	Groko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
dlouhém	dlouhý	k2eAgNnSc6d1
tzv.	tzv.	kA
sondování	sondování	k1gNnSc4
o	o	k7c6
společném	společný	k2eAgInSc6d1
vládním	vládní	k2eAgInSc6d1
programu	program	k1gInSc6
dostalo	dostat	k5eAaPmAgNnS
vedení	vedení	k1gNnSc4
SPD	SPD	kA
od	od	k7c2
mimořádného	mimořádný	k2eAgInSc2d1
sjezdu	sjezd	k1gInSc2
strany	strana	k1gFnSc2
dne	den	k1gInSc2
21	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2018	#num#	k4
přes	přes	k7c4
odpor	odpor	k1gInSc4
více	hodně	k6eAd2
než	než	k8xS
40	#num#	k4
%	%	kIx~
delegátů	delegát	k1gMnPc2
<g/>
,	,	kIx,
zvláště	zvláště	k6eAd1
organizace	organizace	k1gFnSc1
Mladí	mladý	k2eAgMnPc1d1
socialisté	socialist	k1gMnPc1
pod	pod	k7c7
vedením	vedení	k1gNnSc7
Kevina	Kevin	k1gMnSc2
Kühnerta	Kühnert	k1gMnSc2
<g/>
,	,	kIx,
svolení	svolení	k1gNnSc1
k	k	k7c3
vlastnímu	vlastní	k2eAgNnSc3d1
jednání	jednání	k1gNnSc3
s	s	k7c7
CDU	CDU	kA
<g/>
/	/	kIx~
<g/>
CSU	CSU	kA
o	o	k7c6
programu	program	k1gInSc6
<g/>
,	,	kIx,
personálním	personální	k2eAgNnSc6d1
obsazení	obsazení	k1gNnSc6
ministerstev	ministerstvo	k1gNnPc2
a	a	k8xC
dalších	další	k2eAgFnPc6d1
otázkách	otázka	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Situace	situace	k1gFnSc1
SPD	SPD	kA
v	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
</s>
<s>
V	v	k7c6
průzkumech	průzkum	k1gInPc6
volebních	volební	k2eAgFnPc2d1
preferencí	preference	k1gFnPc2
<g/>
,	,	kIx,
konaných	konaný	k2eAgFnPc2d1
v	v	k7c6
průběhu	průběh	k1gInSc6
roku	rok	k1gInSc2
2018	#num#	k4
<g/>
,	,	kIx,
poklesla	poklesnout	k5eAaPmAgFnS
SPD	SPD	kA
v	v	k7c6
celém	celý	k2eAgNnSc6d1
Německu	Německo	k1gNnSc6
na	na	k7c6
zhruba	zhruba	k6eAd1
18	#num#	k4
%	%	kIx~
<g/>
,	,	kIx,
a	a	k8xC
později	pozdě	k6eAd2
dokonce	dokonce	k9
na	na	k7c4
15	#num#	k4
%	%	kIx~
<g/>
,	,	kIx,
tedy	tedy	k9
na	na	k7c4
nejnižší	nízký	k2eAgInSc4d3
stav	stav	k1gInSc4
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
historii	historie	k1gFnSc6
od	od	k7c2
roku	rok	k1gInSc2
1945	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
podzim	podzim	k1gInSc4
2018	#num#	k4
získala	získat	k5eAaPmAgFnS
SPD	SPD	kA
v	v	k7c6
Bavorsku	Bavorsko	k1gNnSc6
ve	v	k7c6
volbách	volba	k1gFnPc6
do	do	k7c2
zemského	zemský	k2eAgInSc2d1
sněmu	sněm	k1gInSc2
pouze	pouze	k6eAd1
9,8	9,8	k4
%	%	kIx~
hlasů	hlas	k1gInPc2
<g/>
,	,	kIx,
ve	v	k7c6
volbách	volba	k1gFnPc6
v	v	k7c6
Hesensku	Hesensko	k1gNnSc6
pak	pak	k6eAd1
19,8	19,8	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s>
SPD	SPD	kA
součástí	součást	k1gFnSc7
kabinetu	kabinet	k1gInSc2
Merkel	Merkel	k1gInSc1
IV	IV	kA
</s>
<s>
Dne	den	k1gInSc2
9	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2018	#num#	k4
oznámilo	oznámit	k5eAaPmAgNnS
vedení	vedení	k1gNnSc1
SPD	SPD	kA
jména	jméno	k1gNnSc2
šesti	šest	k4xCc2
osob	osoba	k1gFnPc2
<g/>
,	,	kIx,
tří	tři	k4xCgFnPc2
političek	politička	k1gFnPc2
a	a	k8xC
tří	tři	k4xCgMnPc2
politiků	politik	k1gMnPc2
<g/>
,	,	kIx,
které	který	k3yIgMnPc4,k3yRgMnPc4,k3yQgMnPc4
strana	strana	k1gFnSc1
nominuje	nominovat	k5eAaBmIp3nS
jako	jako	k9
ministry	ministr	k1gMnPc4
ve	v	k7c6
čtvrté	čtvrtý	k4xOgFnSc6
vládě	vláda	k1gFnSc6
Angely	Angela	k1gFnSc2
Merkelové	Merkelová	k1gFnSc2
(	(	kIx(
<g/>
Kabinett	Kabinett	k2eAgInSc1d1
Merkel	Merkel	k1gInSc1
IV	IV	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
SPD	SPD	kA
pro	pro	k7c4
sebe	sebe	k3xPyFc4
v	v	k7c6
jednáních	jednání	k1gNnPc6
s	s	k7c7
CDU	CDU	kA
<g/>
/	/	kIx~
<g/>
CSU	CSU	kA
získala	získat	k5eAaPmAgFnS
velmi	velmi	k6eAd1
důležité	důležitý	k2eAgNnSc4d1
ministerstvo	ministerstvo	k1gNnSc4
financí	finance	k1gFnPc2
<g/>
;	;	kIx,
tento	tento	k3xDgInSc1
ústupek	ústupek	k1gInSc1
je	být	k5eAaImIp3nS
považován	považován	k2eAgInSc1d1
za	za	k7c4
osobní	osobní	k2eAgInSc4d1
neúspěch	neúspěch	k1gInSc4
kancléřky	kancléřka	k1gFnSc2
Merkelové	Merkelová	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ministrem	ministr	k1gMnSc7
financí	finance	k1gFnPc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
Olaf	Olaf	k1gMnSc1
Scholz	Scholz	k1gMnSc1
<g/>
,	,	kIx,
dosavadní	dosavadní	k2eAgMnSc1d1
primátor	primátor	k1gMnSc1
Hamburku	Hamburk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k8xS
dva	dva	k4xCgMnPc1
potenciální	potenciální	k2eAgMnPc1d1
kandidáti	kandidát	k1gMnPc1
na	na	k7c4
post	post	k1gInSc4
ministra	ministr	k1gMnSc2
zahraničí	zahraničí	k1gNnSc2
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
Schulz	Schulz	k1gMnSc1
a	a	k8xC
Sigmar	Sigmar	k1gMnSc1
Gabriel	Gabriel	k1gMnSc1
<g/>
,	,	kIx,
vypadli	vypadnout	k5eAaPmAgMnP
ze	z	k7c2
hry	hra	k1gFnSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
novým	nový	k2eAgMnSc7d1
ministrem	ministr	k1gMnSc7
tohoto	tento	k3xDgInSc2
resortu	resort	k1gInSc2
stal	stát	k5eAaPmAgInS
Heiko	Heiko	k1gNnSc4
Maas	Maasa	k1gFnPc2
<g/>
,	,	kIx,
dosavadní	dosavadní	k2eAgMnSc1d1
ministr	ministr	k1gMnSc1
spravedlnosti	spravedlnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc1
resort	resort	k1gInSc4
převzala	převzít	k5eAaPmAgFnS
právnička	právnička	k1gFnSc1
a	a	k8xC
bývalá	bývalý	k2eAgFnSc1d1
soudkyně	soudkyně	k1gFnSc1
Katarina	Katarino	k1gNnSc2
Barley	Barlea	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
již	již	k6eAd1
byla	být	k5eAaImAgFnS
členkou	členka	k1gFnSc7
kabinetu	kabinet	k1gInSc2
Merkel	Merkela	k1gFnPc2
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novými	nový	k2eAgMnPc7d1
ministry	ministr	k1gMnPc7
a	a	k8xC
ministryněmi	ministryně	k1gFnPc7
jsou	být	k5eAaImIp3nP
Hubertus	hubertus	k1gInSc4
Heil	Heila	k1gFnPc2
<g/>
,	,	kIx,
bývalý	bývalý	k2eAgMnSc1d1
generální	generální	k2eAgMnSc1d1
sekretář	sekretář	k1gMnSc1
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
ministr	ministr	k1gMnSc1
práce	práce	k1gFnSc2
a	a	k8xC
sociálních	sociální	k2eAgFnPc2d1
věcí	věc	k1gFnPc2
<g/>
,	,	kIx,
Svenja	Svenjus	k1gMnSc2
Schulze	Schulz	k1gMnSc2
jako	jako	k8xS,k8xC
ministryně	ministryně	k1gFnSc1
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
a	a	k8xC
Franziska	Franzisko	k1gNnSc2
Giffey	Giffea	k1gFnSc2
jako	jako	k8xC,k8xS
ministryně	ministryně	k1gFnSc2
pro	pro	k7c4
rodinu	rodina	k1gFnSc4
<g/>
,	,	kIx,
seniory	senior	k1gMnPc4
a	a	k8xC
mládež	mládež	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Posledně	posledně	k6eAd1
jmenovaná	jmenovaný	k2eAgFnSc1d1
žena	žena	k1gFnSc1
je	být	k5eAaImIp3nS
ve	v	k7c6
vrcholné	vrcholný	k2eAgFnSc6d1
politice	politika	k1gFnSc6
zcela	zcela	k6eAd1
nová	nový	k2eAgFnSc1d1
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
však	však	k9
politické	politický	k2eAgFnPc4d1
zkušenosti	zkušenost	k1gFnPc4
jako	jako	k8xC,k8xS
starostka	starostka	k1gFnSc1
ve	v	k7c6
velmi	velmi	k6eAd1
problémové	problémový	k2eAgFnSc6d1
berlínské	berlínský	k2eAgFnSc6d1
městské	městský	k2eAgFnSc6d1
části	část	k1gFnSc6
Neukölln	Neukölln	k1gMnSc1
(	(	kIx(
<g/>
330	#num#	k4
000	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
zároveň	zároveň	k6eAd1
jedinou	jediný	k2eAgFnSc7d1
osobou	osoba	k1gFnSc7
původem	původ	k1gInSc7
z	z	k7c2
bývalé	bývalý	k2eAgFnSc2d1
Německé	německý	k2eAgFnSc2d1
demokratické	demokratický	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
ministryní	ministryně	k1gFnPc2
v	v	k7c6
celoněmecké	celoněmecký	k2eAgFnSc6d1
vládě	vláda	k1gFnSc6
<g/>
,	,	kIx,
nehledě	hledět	k5eNaImSgMnS
na	na	k7c4
samotnou	samotný	k2eAgFnSc4d1
kancléřku	kancléřka	k1gFnSc4
Merkelovou	Merkelový	k2eAgFnSc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Současní	současný	k2eAgMnPc1d1
významní	významný	k2eAgMnPc1d1
politikové	politik	k1gMnPc1
a	a	k8xC
političky	politička	k1gFnPc1
SPD	SPD	kA
2018	#num#	k4
</s>
<s>
Katarina	Katarina	k1gFnSc1
Barley	Barlea	k1gFnSc2
<g/>
,	,	kIx,
spolková	spolkový	k2eAgFnSc1d1
ministryně	ministryně	k1gFnSc1
spravedlnosti	spravedlnost	k1gFnSc2
</s>
<s>
Malu	Malu	k5eAaPmIp1nS
Dreyer	Dreyer	k1gInSc1
<g/>
,	,	kIx,
současná	současný	k2eAgFnSc1d1
ministerská	ministerský	k2eAgFnSc1d1
předsedkyně	předsedkyně	k1gFnSc1
spolkové	spolkový	k2eAgFnSc2d1
země	zem	k1gFnSc2
Porýní-Falc	Porýní-Falc	k1gFnSc1
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Franziska	Franziska	k1gFnSc1
Giffey	Giffea	k1gFnSc2
<g/>
,	,	kIx,
spolková	spolkový	k2eAgFnSc1d1
ministryně	ministryně	k1gFnSc1
pro	pro	k7c4
rodinu	rodina	k1gFnSc4
<g/>
,	,	kIx,
seniory	senior	k1gMnPc4
a	a	k8xC
mládež	mládež	k1gFnSc4
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Hubertus	hubertus	k1gInSc1
Heil	Heil	k1gInSc1
<g/>
,	,	kIx,
spolkový	spolkový	k2eAgMnSc1d1
ministr	ministr	k1gMnSc1
práce	práce	k1gFnSc2
a	a	k8xC
sociálních	sociální	k2eAgFnPc2d1
věcí	věc	k1gFnPc2
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Lars	Lars	k6eAd1
Klingbeil	Klingbeil	k1gMnSc1
<g/>
,	,	kIx,
generální	generální	k2eAgMnSc1d1
sekretář	sekretář	k1gMnSc1
SPD	SPD	kA
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Heiko	Heiko	k6eAd1
Maas	Maas	k1gInSc1
<g/>
,	,	kIx,
spolkový	spolkový	k2eAgMnSc1d1
ministr	ministr	k1gMnSc1
zahraničí	zahraničí	k1gNnSc2
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Olaf	Olaf	k1gMnSc1
Scholz	Scholz	k1gMnSc1
<g/>
,	,	kIx,
bývalý	bývalý	k2eAgMnSc1d1
primátor	primátor	k1gMnSc1
Hamburku	Hamburk	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
současnosti	současnost	k1gFnSc6
spolkový	spolkový	k2eAgMnSc1d1
ministr	ministr	k1gMnSc1
financí	finance	k1gFnPc2
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Svenja	Svenja	k1gFnSc1
Schulze	Schulz	k1gMnSc2
<g/>
,	,	kIx,
spolková	spolkový	k2eAgFnSc1d1
ministryně	ministryně	k1gFnSc1
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Manuela	Manuela	k1gFnSc1
Schwesig	Schwesiga	k1gFnPc2
<g/>
,	,	kIx,
současná	současný	k2eAgFnSc1d1
ministerská	ministerský	k2eAgFnSc1d1
předsedkyně	předsedkyně	k1gFnSc1
spolkové	spolkový	k2eAgFnSc2d1
země	zem	k1gFnSc2
Meklenbursko-Přední	meklenbursko-přední	k2eAgNnSc4d1
Pomořansko	Pomořansko	k1gNnSc4
</s>
<s>
Politici	politik	k1gMnPc1
SPD	SPD	kA
</s>
<s>
Kancléři	kancléř	k1gMnPc1
</s>
<s>
Friedrich	Friedrich	k1gMnSc1
Ebert	Eberta	k1gFnPc2
-	-	kIx~
1918	#num#	k4
</s>
<s>
Philipp	Philipp	k1gMnSc1
Scheidemann	Scheidemann	k1gMnSc1
-	-	kIx~
1919	#num#	k4
</s>
<s>
Gustav	Gustav	k1gMnSc1
Bauer	Bauer	k1gMnSc1
-	-	kIx~
1919-1920	1919-1920	k4
</s>
<s>
Hermann	Hermann	k1gMnSc1
Müller	Müller	k1gMnSc1
-	-	kIx~
1920	#num#	k4
a	a	k8xC
1928-1930	1928-1930	k4
</s>
<s>
Willy	Will	k1gInPc1
Brandt	Brandta	k1gFnPc2
-	-	kIx~
1969-1974	1969-1974	k4
</s>
<s>
Helmut	Helmut	k1gMnSc1
Schmidt	Schmidt	k1gMnSc1
-	-	kIx~
1974-1982	1974-1982	k4
</s>
<s>
Gerhard	Gerhard	k1gMnSc1
Schröder	Schröder	k1gMnSc1
-	-	kIx~
1998-2005	1998-2005	k4
</s>
<s>
Prezidenti	prezident	k1gMnPc1
</s>
<s>
Friedrich	Friedrich	k1gMnSc1
Ebert	Eberta	k1gFnPc2
-	-	kIx~
1919-1925	1919-1925	k4
</s>
<s>
Gustav	Gustav	k1gMnSc1
Heinemann	Heinemann	k1gMnSc1
-	-	kIx~
1969-1974	1969-1974	k4
</s>
<s>
Johannes	Johannes	k1gMnSc1
Rau	Rau	k1gMnSc1
-	-	kIx~
1999-2004	1999-2004	k4
</s>
<s>
Frank-Walter	Frank-Walter	k1gMnSc1
Steinmeier	Steinmeier	k1gMnSc1
-	-	kIx~
od	od	k7c2
2017	#num#	k4
</s>
<s>
Volební	volební	k2eAgInPc1d1
výsledky	výsledek	k1gInPc1
</s>
<s>
Podíl	podíl	k1gInSc1
SPD	SPD	kA
ve	v	k7c6
druhém	druhý	k4xOgNnSc6
hlasování	hlasování	k1gNnSc6
ve	v	k7c6
volbách	volba	k1gFnPc6
do	do	k7c2
Spolkového	spolkový	k2eAgInSc2d1
sněmu	sněm	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
</s>
<s>
Volební	volební	k2eAgInPc1d1
výsledky	výsledek	k1gInPc1
od	od	k7c2
roku	rok	k1gInSc2
1949	#num#	k4
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Počet	počet	k1gInSc1
%	%	kIx~
</s>
<s>
Počet	počet	k1gInSc1
křesel	křeslo	k1gNnPc2
</s>
<s>
Kandidát	kandidát	k1gMnSc1
na	na	k7c4
kancléře	kancléř	k1gMnPc4
</s>
<s>
194929,2	194929,2	k4
%	%	kIx~
<g/>
131	#num#	k4
<g/>
Kurt	kurta	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
</s>
<s>
195328,8	195328,8	k4
%	%	kIx~
<g/>
151	#num#	k4
<g/>
Erich	Erich	k1gMnSc1
Ollenhauer	Ollenhauer	k1gMnSc1
</s>
<s>
195731,8	195731,8	k4
%	%	kIx~
<g/>
169	#num#	k4
<g/>
Erich	Erich	k1gMnSc1
Ollenhauer	Ollenhauer	k1gMnSc1
</s>
<s>
196136,2	196136,2	k4
%	%	kIx~
<g/>
190	#num#	k4
<g/>
Willy	Willa	k1gFnSc2
Brandt	Brandta	k1gFnPc2
</s>
<s>
196539,3	196539,3	k4
%	%	kIx~
<g/>
202	#num#	k4
<g/>
Willy	Willa	k1gFnSc2
Brandt	Brandta	k1gFnPc2
</s>
<s>
196942,7	196942,7	k4
%	%	kIx~
<g/>
224	#num#	k4
<g/>
Willy	Willa	k1gFnSc2
Brandt	Brandta	k1gFnPc2
</s>
<s>
197245,8	197245,8	k4
%	%	kIx~
<g/>
230	#num#	k4
<g/>
Willy	Willa	k1gFnSc2
Brandt	Brandta	k1gFnPc2
</s>
<s>
197642,6	197642,6	k4
%	%	kIx~
<g/>
214	#num#	k4
<g/>
Helmut	Helmut	k1gMnSc1
Schmidt	Schmidt	k1gMnSc1
</s>
<s>
198042,9	198042,9	k4
%	%	kIx~
<g/>
218	#num#	k4
<g/>
Helmut	Helmut	k1gMnSc1
Schmidt	Schmidt	k1gMnSc1
</s>
<s>
198338,2	198338,2	k4
%	%	kIx~
<g/>
193	#num#	k4
<g/>
Hans-Jochen	Hans-Jochno	k1gNnPc2
Vogel	Vogel	k1gMnSc1
</s>
<s>
198737,0	198737,0	k4
%	%	kIx~
<g/>
186	#num#	k4
<g/>
Johannes	Johannes	k1gMnSc1
Rau	Rau	k1gMnSc1
</s>
<s>
199033,5	199033,5	k4
%	%	kIx~
<g/>
239	#num#	k4
<g/>
Oskar	Oskar	k1gMnSc1
Lafontaine	Lafontain	k1gMnSc5
</s>
<s>
199436,4	199436,4	k4
%	%	kIx~
<g/>
252	#num#	k4
<g/>
Rudolf	Rudolfa	k1gFnPc2
Scharping	Scharping	k1gInSc4
</s>
<s>
199840,9	199840,9	k4
%	%	kIx~
<g/>
298	#num#	k4
<g/>
Gerhard	Gerhard	k1gMnSc1
Schröder	Schröder	k1gMnSc1
</s>
<s>
200238,5	200238,5	k4
%	%	kIx~
<g/>
251	#num#	k4
<g/>
Gerhard	Gerhard	k1gMnSc1
Schröder	Schröder	k1gMnSc1
</s>
<s>
200534,2	200534,2	k4
%	%	kIx~
<g/>
222	#num#	k4
<g/>
Gerhard	Gerhard	k1gMnSc1
Schröder	Schröder	k1gMnSc1
</s>
<s>
200923,0	200923,0	k4
%	%	kIx~
<g/>
146	#num#	k4
<g/>
Frank-Walter	Frank-Walter	k1gMnSc1
Steinmeier	Steinmeier	k1gMnSc1
</s>
<s>
201325,7	201325,7	k4
%	%	kIx~
<g/>
193	#num#	k4
<g/>
Peer	peer	k1gMnSc1
Steinbrück	Steinbrück	k1gMnSc1
</s>
<s>
201720,5	201720,5	k4
%	%	kIx~
<g/>
153	#num#	k4
<g/>
Martin	Martin	k1gMnSc1
Schulz	Schulz	k1gMnSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Strany	strana	k1gFnPc1
a	a	k8xC
volby	volba	k1gFnPc1
(	(	kIx(
<g/>
Německy	německy	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
VÖLLINGER	VÖLLINGER	kA
<g/>
,	,	kIx,
Veronika	Veronika	k1gFnSc1
<g/>
;	;	kIx,
KLORMANN	KLORMANN	kA
<g/>
,	,	kIx,
Sybille	Sybille	k1gFnSc1
<g/>
;	;	kIx,
GEIL	GEIL	kA
<g/>
,	,	kIx,
Karin	Karina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bundestagswahl	Bundestagswahl	k1gMnSc1
<g/>
:	:	kIx,
Christian	Christian	k1gMnSc1
Lindner	Lindner	k1gMnSc1
zum	zum	k?
Fraktionsvorsitzenden	Fraktionsvorsitzendno	k1gNnPc2
gewählt	gewähltum	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Die	Die	k1gMnSc1
Zeit	Zeit	k1gMnSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
44	#num#	k4
<g/>
-	-	kIx~
<g/>
2070	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Oslabená	oslabený	k2eAgFnSc1d1
CDU	CDU	kA
<g/>
/	/	kIx~
<g/>
CSU	CSU	kA
obhájila	obhájit	k5eAaPmAgFnS
prvenství	prvenství	k1gNnSc4
<g/>
,	,	kIx,
posílili	posílit	k5eAaPmAgMnP
pravicoví	pravicový	k2eAgMnPc1d1
populisté	populista	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konečné	Konečné	k2eAgInPc1d1
výsledky	výsledek	k1gInPc1
voleb	volba	k1gFnPc2
v	v	k7c6
Německu	Německo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Kabinett	Kabinett	k1gMnSc1
vollzählig	vollzählig	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Das	Das	k1gMnSc1
sind	sind	k1gMnSc1
die	die	k?
SPD-Minister	SPD-Minister	k1gMnSc1
für	für	k?
die	die	k?
neue	uat	k5eNaPmIp3nS
Regierung	Regierung	k1gInSc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Kabinet	kabinet	k1gInSc1
je	být	k5eAaImIp3nS
úplný	úplný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
jsou	být	k5eAaImIp3nP
ministři	ministr	k1gMnPc1
SPD	SPD	kA
pro	pro	k7c4
novou	nový	k2eAgFnSc4d1
vládu	vláda	k1gFnSc4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
T-Online	T-Onlin	k1gInSc5
<g/>
,	,	kIx,
http://www.t-online.de/nachrichten/deutschland/innenpolitik/id_83363172/groko-kabinett-das-sind-die-spd-minister-fuer-die-neue-regierung.html,	http://www.t-online.de/nachrichten/deutschland/innenpolitik/id_83363172/groko-kabinett-das-sind-die-spd-minister-fuer-die-neue-regierung.html,	k4
9	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2018	#num#	k4
<g/>
,	,	kIx,
10	#num#	k4
<g/>
:	:	kIx,
<g/>
57	#num#	k4
hod	hod	k1gInSc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
SOBĚHART	SOBĚHART	kA
<g/>
,	,	kIx,
Radek	Radek	k1gMnSc1
<g/>
.	.	kIx.
„	„	k?
<g/>
Generation	Generation	k1gInSc1
Godesberg	Godesberg	k1gInSc1
<g/>
“	“	k?
<g/>
:	:	kIx,
Der	drát	k5eAaImRp2nS
Weg	Weg	k1gFnSc2
der	drát	k5eAaImRp2nS
SPD	SPD	kA
zur	zur	k?
Volkspartei	Volksparte	k1gFnSc2
in	in	k?
den	den	k1gInSc1
60	#num#	k4
<g/>
er	er	k?
Jahren	Jahrna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prague	Prague	k1gNnSc1
Papers	Papersa	k1gFnPc2
on	on	k3xPp3gMnSc1
the	the	k?
History	Histor	k1gInPc4
of	of	k?
International	International	k1gFnSc2
Relations	Relationsa	k1gFnPc2
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
11	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
469	#num#	k4
<g/>
-	-	kIx~
<g/>
475	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
PDF	PDF	kA
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7308	#num#	k4
<g/>
-	-	kIx~
<g/>
208	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Sociálnědemokratická	sociálnědemokratický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Německa	Německo	k1gNnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
SPD	SPD	kA
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
kn	kn	k?
<g/>
20010711415	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
2022139-3	2022139-3	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2353	#num#	k4
4548	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
80067141	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
151021191	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
80067141	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Německo	Německo	k1gNnSc1
|	|	kIx~
Politika	politika	k1gFnSc1
</s>
