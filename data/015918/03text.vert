<s>
Lítla	lítnout	k5eAaImAgFnS
Dímun	Dímun	k1gNnSc4
</s>
<s>
Lítla	lítnout	k5eAaImAgFnS
DímunLille	DímunLille	k1gFnSc1
Dimon	Dimon	k1gMnSc1
(	(	kIx(
<g/>
dánsky	dánsky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Stát	stát	k1gInSc1
</s>
<s>
Dánsko	Dánsko	k1gNnSc1
Dánsko	Dánsko	k1gNnSc1
•	•	k?
závislé	závislý	k2eAgNnSc1d1
území	území	k1gNnSc4
</s>
<s>
Faerské	Faerský	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
Faerské	Faerský	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc4
Topografie	topografie	k1gFnSc1
Rozloha	rozloha	k1gFnSc1
</s>
<s>
0,82	0,82	k4
km²	km²	k?
Zeměpisné	zeměpisný	k2eAgFnSc2d1
souřadnice	souřadnice	k1gFnSc2
</s>
<s>
61	#num#	k4
<g/>
°	°	k?
<g/>
38	#num#	k4
<g/>
′	′	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
6	#num#	k4
<g/>
°	°	k?
<g/>
42	#num#	k4
<g/>
′	′	k?
z.	z.	k?
d.	d.	k?
Nejvyšší	vysoký	k2eAgInSc4d3
vrchol	vrchol	k1gInSc4
</s>
<s>
Rávan	Rávan	k1gInSc1
(	(	kIx(
<g/>
414	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
Osídlení	osídlení	k1gNnSc1
Počet	počet	k1gInSc4
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
0	#num#	k4
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
0	#num#	k4
obyv	obyv	k1gInSc1
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Lítla	Lítla	k1gInSc1
Dímun	Dímun	k1gInSc1
(	(	kIx(
<g/>
dán	dát	k5eAaPmNgInS
<g/>
.	.	kIx.
<g/>
:	:	kIx,
Lille	Lille	k1gFnSc1
Dimon	Dimon	k1gInSc1
<g/>
,	,	kIx,
čes.	čes.	k?
<g/>
:	:	kIx,
Malý	Malý	k1gMnSc1
Dímun	Dímun	k1gMnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
nejmenší	malý	k2eAgInSc4d3
a	a	k8xC
jediný	jediný	k2eAgInSc4d1
neobydlený	obydlený	k2eNgInSc4d1
Faerský	Faerský	k2eAgInSc4d1
ostrov	ostrov	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
rozlohu	rozloha	k1gFnSc4
0,82	0,82	k4
km²	km²	k?
<g/>
.	.	kIx.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
mezi	mezi	k7c4
ostrovy	ostrov	k1gInPc4
Stóra	stóra	k1gFnSc1
Dímun	Dímun	k1gNnSc1
5	#num#	k4
km	km	kA
severně	severně	k6eAd1
a	a	k8xC
Suð	Suð	k2eAgInPc1d1
11	#num#	k4
km	km	kA
západně	západně	k6eAd1
od	od	k7c2
ostrova	ostrov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostrov	ostrov	k1gInSc1
je	být	k5eAaImIp3nS
možno	možno	k6eAd1
vidět	vidět	k5eAaImF
z	z	k7c2
ostrova	ostrov	k1gInSc2
Suð	Suð	k1gFnSc2
z	z	k7c2
vesnic	vesnice	k1gFnPc2
Hvalba	Hvalba	k1gFnSc1
a	a	k8xC
Sandvík	Sandvík	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostrov	ostrov	k1gInSc1
má	mít	k5eAaImIp3nS
pouze	pouze	k6eAd1
jeden	jeden	k4xCgInSc4
vrchol	vrchol	k1gInSc4
se	s	k7c7
jménem	jméno	k1gNnSc7
Rávan	Rávan	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
414	#num#	k4
metrů	metr	k1gInPc2
vysoký	vysoký	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostrov	ostrov	k1gInSc1
je	být	k5eAaImIp3nS
obývaný	obývaný	k2eAgInSc1d1
pouze	pouze	k6eAd1
ovcemi	ovce	k1gFnPc7
a	a	k8xC
ptáky	pták	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s>
Lítla	lítnout	k5eAaImAgFnS
Dímun	Dímun	k1gNnSc4
</s>
<s>
Lítla	lítnout	k5eAaImAgFnS
Dímun	Dímun	k1gNnSc4
</s>
<s>
Galerie	galerie	k1gFnSc1
Lítla	lítnout	k5eAaImAgFnS
Dímun	Dímun	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Ostrovy	ostrov	k1gInPc1
Faerských	Faerský	k2eAgInPc2d1
ostrovů	ostrov	k1gInPc2
Ostrovy	ostrov	k1gInPc1
</s>
<s>
Borð	Borð	k2eAgInPc1d1
•	•	k?
Eysturoy	Eysturoy	k1gInPc1
•	•	k?
Fugloy	Fugloa	k1gFnSc2
•	•	k?
Hestur	Hestura	k1gFnPc2
•	•	k?
Kalsoy	Kalsoy	k1gInPc1
•	•	k?
Koltur	Koltura	k1gFnPc2
•	•	k?
Kunoy	Kunoa	k1gFnSc2
•	•	k?
Lítla	lítnout	k5eAaImAgFnS
Dímun	Dímun	k1gNnSc4
•	•	k?
Mykines	Mykines	k1gInSc1
•	•	k?
Nólsoy	Nólsoa	k1gFnSc2
•	•	k?
Sandoy	Sandoa	k1gFnSc2
•	•	k?
Skúvoy	Skúvoa	k1gFnSc2
•	•	k?
Stóra	stóra	k1gFnSc1
Dímun	Dímuno	k1gNnPc2
•	•	k?
Streymoy	Streymoa	k1gMnSc2
•	•	k?
Suð	Suð	k1gMnSc2
•	•	k?
Svínoy	Svínoa	k1gMnSc2
•	•	k?
Vágar	Vágar	k1gMnSc1
•	•	k?
Við	Við	k2eAgInPc4d1
Ostrůvky	ostrůvek	k1gInPc4
</s>
<s>
Baglhólmur	Baglhólmur	k1gMnSc1
•	•	k?
Gáshólmur	Gáshólmur	k1gMnSc1
•	•	k?
Hovshólmur	Hovshólmur	k1gMnSc1
•	•	k?
Hoyvíkshólmur	Hoyvíkshólmur	k1gMnSc1
•	•	k?
Kirkjubø	Kirkjubø	k1gMnSc1
•	•	k?
Lopranshólmur	Lopranshólmur	k1gMnSc1
•	•	k?
Mykineshólmur	Mykineshólmur	k1gMnSc1
•	•	k?
Sumbiarhólmur	Sumbiarhólmur	k1gMnSc1
•	•	k?
Tindhólmur	Tindhólmur	k1gMnSc1
•	•	k?
Tjaldavíkshólmur	Tjaldavíkshólmur	k1gMnSc1
•	•	k?
Trø	Trø	k1gMnPc5
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
Faerské	Faerský	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
</s>
