<p>
<s>
Jessica	Jessica	k1gFnSc1	Jessica
"	"	kIx"	"
<g/>
Jessie	Jessie	k1gFnSc1	Jessie
<g/>
"	"	kIx"	"
Collins	Collins	k1gInSc1	Collins
(	(	kIx(	(
<g/>
*	*	kIx~	*
8	[number]	k4	8
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1983	[number]	k4	1983
<g/>
,	,	kIx,	,
San	San	k1gMnSc1	San
Antonio	Antonio	k1gMnSc1	Antonio
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americká	americký	k2eAgFnSc1d1	americká
herečka	herečka	k1gFnSc1	herečka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
města	město	k1gNnSc2	město
San	San	k1gMnSc1	San
Antonio	Antonio	k1gMnSc1	Antonio
v	v	k7c4	v
Bexar	Bexar	k1gInSc4	Bexar
County	Counta	k1gFnSc2	Counta
v	v	k7c6	v
Texasu	Texas	k1gInSc6	Texas
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
sester	sestra	k1gFnPc2	sestra
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
všechny	všechen	k3xTgFnPc1	všechen
věnují	věnovat	k5eAaPmIp3nP	věnovat
umění	umění	k1gNnSc4	umění
<g/>
.	.	kIx.	.
</s>
<s>
Vystudovala	vystudovat	k5eAaPmAgFnS	vystudovat
Juilliard	Juilliard	k1gInSc4	Juilliard
School	Schoola	k1gFnPc2	Schoola
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc7	její
nejznámější	známý	k2eAgFnSc7d3	nejznámější
rolí	role	k1gFnSc7	role
je	být	k5eAaImIp3nS	být
postava	postava	k1gFnSc1	postava
Natalie	Natalie	k1gFnSc1	Natalie
Davis	Davis	k1gFnSc1	Davis
pod	pod	k7c7	pod
přezdívkou	přezdívka	k1gFnSc7	přezdívka
The	The	k1gFnSc2	The
Miniature	Miniatur	k1gMnSc5	Miniatur
Killer	Killer	k1gInSc4	Killer
v	v	k7c6	v
Kriminálce	kriminálka	k1gFnSc6	kriminálka
Las	laso	k1gNnPc2	laso
Vegas	Vegas	k1gMnSc1	Vegas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Jessie	Jessie	k1gFnSc1	Jessie
Collins	Collinsa	k1gFnPc2	Collinsa
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
