<s>
Victoria	Victorium	k1gNnPc1	Victorium
byla	být	k5eAaImAgNnP	být
loď	loď	k1gFnSc4	loď
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
patřila	patřit	k5eAaImAgFnS	patřit
do	do	k7c2	do
výpravy	výprava	k1gFnSc2	výprava
Fernã	Fernã	k1gMnSc2	Fernã
de	de	k?	de
Magalhã	Magalhã	k1gMnSc2	Magalhã
a	a	k8xC	a
jako	jako	k8xC	jako
jediná	jediný	k2eAgFnSc1d1	jediná
absolvovala	absolvovat	k5eAaPmAgFnS	absolvovat
celou	celý	k2eAgFnSc4d1	celá
plavbu	plavba	k1gFnSc4	plavba
kolem	kolem	k7c2	kolem
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
1519	[number]	k4	1519
<g/>
-	-	kIx~	-
<g/>
1522	[number]	k4	1522
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gMnSc7	její
původním	původní	k2eAgMnSc7d1	původní
velitelem	velitel	k1gMnSc7	velitel
byl	být	k5eAaImAgMnS	být
Luis	Luisa	k1gFnPc2	Luisa
de	de	k?	de
Mendoza	Mendoz	k1gMnSc2	Mendoz
<g/>
,	,	kIx,	,
zabitý	zabitý	k2eAgInSc1d1	zabitý
při	při	k7c6	při
vzpouře	vzpoura	k1gFnSc6	vzpoura
v	v	k7c6	v
zálivu	záliv	k1gInSc6	záliv
sv.	sv.	kA	sv.
Juliána	Julián	k1gMnSc2	Julián
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
v	v	k7c6	v
kapitánských	kapitánský	k2eAgFnPc6d1	kapitánská
funkcích	funkce	k1gFnPc6	funkce
vystřídali	vystřídat	k5eAaPmAgMnP	vystřídat
ještě	ještě	k6eAd1	ještě
tři	tři	k4xCgMnPc1	tři
muži	muž	k1gMnPc1	muž
-	-	kIx~	-
Duarte	Duart	k1gInSc5	Duart
Barbosa	Barbosa	k1gFnSc1	Barbosa
<g/>
,	,	kIx,	,
Magalhã	Magalhã	k1gMnSc1	Magalhã
švagr	švagr	k1gMnSc1	švagr
<g/>
,	,	kIx,	,
Gonzalo	Gonzalo	k1gMnSc1	Gonzalo
Gómez	Gómez	k1gMnSc1	Gómez
de	de	k?	de
Espinosa	Espinosa	k1gFnSc1	Espinosa
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
Juan	Juan	k1gMnSc1	Juan
Sebastián	Sebastián	k1gMnSc1	Sebastián
del	del	k?	del
Cano	Cano	k1gMnSc1	Cano
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
se	se	k3xPyFc4	se
v	v	k7c6	v
září	září	k1gNnSc6	září
1522	[number]	k4	1522
podařilo	podařit	k5eAaPmAgNnS	podařit
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
španělských	španělský	k2eAgInPc2d1	španělský
břehů	břeh	k1gInPc2	břeh
a	a	k8xC	a
cestu	cesta	k1gFnSc4	cesta
dokončit	dokončit	k5eAaPmF	dokončit
<g/>
.	.	kIx.	.
</s>
<s>
Victoria	Victorium	k1gNnSc2	Victorium
byla	být	k5eAaImAgFnS	být
28	[number]	k4	28
metrů	metr	k1gInPc2	metr
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
,	,	kIx,	,
8,5	[number]	k4	8,5
metru	metr	k1gInSc2	metr
široká	široký	k2eAgFnSc1d1	široká
a	a	k8xC	a
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
měla	mít	k5eAaImAgFnS	mít
výtlak	výtlak	k1gInSc4	výtlak
asi	asi	k9	asi
80-90	[number]	k4	80-90
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
své	svůj	k3xOyFgFnSc6	svůj
největší	veliký	k2eAgFnSc6d3	veliký
cestě	cesta	k1gFnSc6	cesta
byla	být	k5eAaImAgFnS	být
loď	loď	k1gFnSc1	loď
opravena	opravit	k5eAaPmNgFnS	opravit
a	a	k8xC	a
ještě	ještě	k6eAd1	ještě
dvakrát	dvakrát	k6eAd1	dvakrát
se	se	k3xPyFc4	se
plavila	plavit	k5eAaImAgFnS	plavit
do	do	k7c2	do
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
druhé	druhý	k4xOgFnSc6	druhý
plavbě	plavba	k1gFnSc6	plavba
však	však	k9	však
zchátralý	zchátralý	k2eAgInSc4d1	zchátralý
trup	trup	k1gInSc4	trup
nevydržel	vydržet	k5eNaPmAgMnS	vydržet
a	a	k8xC	a
loď	loď	k1gFnSc1	loď
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
obeplula	obeplout	k5eAaPmAgFnS	obeplout
svět	svět	k1gInSc4	svět
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
potopila	potopit	k5eAaPmAgFnS	potopit
<g/>
.	.	kIx.	.
</s>
<s>
KNOX-JOHNSTON	KNOX-JOHNSTON	k?	KNOX-JOHNSTON
<g/>
,	,	kIx,	,
Robin	robin	k2eAgInSc1d1	robin
<g/>
.	.	kIx.	.
</s>
<s>
Mys	mys	k1gInSc1	mys
Horn	Horn	k1gMnSc1	Horn
-	-	kIx~	-
Z	z	k7c2	z
dějin	dějiny	k1gFnPc2	dějiny
námořnictví	námořnictví	k1gNnSc2	námořnictví
<g/>
.	.	kIx.	.
</s>
<s>
Trango	Trango	k1gMnSc1	Trango
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
