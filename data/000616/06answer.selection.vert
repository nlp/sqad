<s>
Victoria	Victorium	k1gNnPc1	Victorium
byla	být	k5eAaImAgNnP	být
loď	loď	k1gFnSc4	loď
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
patřila	patřit	k5eAaImAgFnS	patřit
do	do	k7c2	do
výpravy	výprava	k1gFnSc2	výprava
Fernã	Fernã	k1gMnSc2	Fernã
de	de	k?	de
Magalhã	Magalhã	k1gMnSc2	Magalhã
a	a	k8xC	a
jako	jako	k8xS	jako
jediná	jediný	k2eAgFnSc1d1	jediná
absolvovala	absolvovat	k5eAaPmAgFnS	absolvovat
celou	celý	k2eAgFnSc4d1	celá
plavbu	plavba	k1gFnSc4	plavba
kolem	kolem	k7c2	kolem
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
1519	[number]	k4	1519
<g/>
-	-	kIx~	-
<g/>
1522	[number]	k4	1522
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
