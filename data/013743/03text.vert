<s>
Rubikova	Rubikův	k2eAgFnSc1d1
pomsta	pomsta	k1gFnSc1
</s>
<s>
Rubikova	Rubikův	k2eAgFnSc1d1
kostka	kostka	k1gFnSc1
4	#num#	k4
<g/>
x	x	k?
<g/>
4	#num#	k4
<g/>
x	x	k?
<g/>
4	#num#	k4
</s>
<s>
Autor	autor	k1gMnSc1
<g/>
:	:	kIx,
</s>
<s>
Péter	Péter	k1gInSc4
Sebestény	Sebesténa	k1gFnSc2
Datum	datum	k1gNnSc4
vydání	vydání	k1gNnSc2
<g/>
:	:	kIx,
</s>
<s>
1982	#num#	k4
Počet	počet	k1gInSc4
hráčů	hráč	k1gMnPc2
<g/>
:	:	kIx,
</s>
<s>
1	#num#	k4
Doporučený	doporučený	k2eAgInSc1d1
věk	věk	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
od	od	k7c2
8	#num#	k4
let	léto	k1gNnPc2
Tento	tento	k3xDgInSc4
box	box	k1gInSc4
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Rubikova	Rubikův	k2eAgFnSc1d1
kostka	kostka	k1gFnSc1
4	#num#	k4
<g/>
x	x	kIx~
<g/>
4	#num#	k4
<g/>
x	x	kIx~
<g/>
4	#num#	k4
<g/>
,	,	kIx,
známá	známý	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
Rubikova	Rubikův	k2eAgFnSc1d1
pomsta	pomsta	k1gFnSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
nástupcem	nástupce	k1gMnSc7
nejslavnějšího	slavný	k2eAgInSc2d3
Rubikova	Rubikův	k2eAgInSc2d1
hlavolamu	hlavolam	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původně	původně	k6eAd1
se	se	k3xPyFc4
kostka	kostka	k1gFnSc1
měla	mít	k5eAaImAgFnS
jmenovat	jmenovat	k5eAaImF,k5eAaBmF
Sebestényho	Sebestény	k1gMnSc4
kostka	kostka	k1gFnSc1
podle	podle	k7c2
svého	svůj	k3xOyFgMnSc2
vynálezce	vynálezce	k1gMnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
od	od	k7c2
tohoto	tento	k3xDgInSc2
názvu	název	k1gInSc2
se	se	k3xPyFc4
ustoupilo	ustoupit	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s>
Složení	složení	k1gNnSc1
kostky	kostka	k1gFnSc2
</s>
<s>
Kostka	kostka	k1gFnSc1
sama	sám	k3xTgFnSc1
je	být	k5eAaImIp3nS
složena	složit	k5eAaPmNgFnS
z	z	k7c2
96	#num#	k4
barevných	barevný	k2eAgFnPc2d1
nálepek	nálepka	k1gFnPc2
na	na	k7c6
56	#num#	k4
kostičkách	kostička	k1gFnPc6
(	(	kIx(
<g/>
24	#num#	k4
středových	středový	k2eAgMnPc2d1
<g/>
,	,	kIx,
24	#num#	k4
hranových	hranový	k2eAgFnPc2d1
a	a	k8xC
8	#num#	k4
rohových	rohový	k2eAgFnPc2d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgInSc1d1
rozdíl	rozdíl	k1gInSc1
proti	proti	k7c3
původní	původní	k2eAgFnSc3d1
kostce	kostka	k1gFnSc3
je	být	k5eAaImIp3nS
v	v	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
tato	tento	k3xDgFnSc1
verze	verze	k1gFnSc1
nemá	mít	k5eNaImIp3nS
žádné	žádný	k3yNgFnPc4
pevné	pevný	k2eAgFnPc4d1
kostky	kostka	k1gFnPc4
a	a	k8xC
se	s	k7c7
vším	všecek	k3xTgNnSc7
se	se	k3xPyFc4
dá	dát	k5eAaPmIp3nS
otočit	otočit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úkol	úkol	k1gInSc1
je	být	k5eAaImIp3nS
samozřejmě	samozřejmě	k6eAd1
stejný	stejný	k2eAgInSc1d1
jako	jako	k8xS,k8xC
u	u	k7c2
klasické	klasický	k2eAgFnSc2d1
Rubikovy	Rubikův	k2eAgFnSc2d1
kostky	kostka	k1gFnSc2
3	#num#	k4
<g/>
x	x	k?
<g/>
3	#num#	k4
<g/>
x	x	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc4
poskládat	poskládat	k5eAaPmF
na	na	k7c6
každé	každý	k3xTgFnSc6
z	z	k7c2
šesti	šest	k4xCc3
stěn	stěna	k1gFnPc2
jednu	jeden	k4xCgFnSc4
barvu	barva	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kostka	kostka	k1gFnSc1
sama	sám	k3xTgFnSc1
se	se	k3xPyFc4
většinou	většinou	k6eAd1
skládá	skládat	k5eAaImIp3nS
ze	z	k7c2
středového	středový	k2eAgInSc2d1
mechanismu	mechanismus	k1gInSc2
<g/>
,	,	kIx,
ke	k	k7c3
kterému	který	k3yRgMnSc3,k3yIgMnSc3,k3yQgMnSc3
jsou	být	k5eAaImIp3nP
ukotveny	ukotven	k2eAgFnPc1d1
ostatní	ostatní	k2eAgFnPc1d1
kostky	kostka	k1gFnPc1
a	a	k8xC
posouváním	posouvání	k1gNnPc3
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
otáčet	otáčet	k5eAaImF
pouze	pouze	k6eAd1
celým	celý	k2eAgNnSc7d1
patrem	patro	k1gNnSc7
naráz	naráz	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Řešení	řešení	k1gNnSc1
</s>
<s>
Kostka	kostka	k1gFnSc1
sama	sám	k3xTgFnSc1
má	mít	k5eAaImIp3nS
velmi	velmi	k6eAd1
mnoho	mnoho	k4c4
možných	možný	k2eAgFnPc2d1
permutací	permutace	k1gFnPc2
<g/>
,	,	kIx,
přesně	přesně	k6eAd1
7	#num#	k4
401	#num#	k4
196	#num#	k4
841	#num#	k4
564	#num#	k4
901	#num#	k4
869	#num#	k4
874	#num#	k4
093	#num#	k4
974	#num#	k4
498	#num#	k4
574	#num#	k4
336	#num#	k4
000	#num#	k4
000	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Existuje	existovat	k5eAaImIp3nS
samozřejmě	samozřejmě	k6eAd1
mnoho	mnoho	k4c1
metod	metoda	k1gFnPc2
od	od	k7c2
"	"	kIx"
<g/>
patro	patro	k1gNnSc1
po	po	k7c6
patru	patro	k1gNnSc6
<g/>
"	"	kIx"
(	(	kIx(
<g/>
složí	složit	k5eAaPmIp3nS
se	se	k3xPyFc4
jedna	jeden	k4xCgFnSc1
strana	strana	k1gFnSc1
<g/>
,	,	kIx,
pak	pak	k6eAd1
první	první	k4xOgNnSc1
patro	patro	k1gNnSc1
u	u	k7c2
této	tento	k3xDgFnSc2
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
pak	pak	k6eAd1
druhé	druhý	k4xOgNnSc1
<g/>
,	,	kIx,
třetí	třetí	k4xOgNnSc4
a	a	k8xC
nakonec	nakonec	k6eAd1
zbytek	zbytek	k1gInSc1
kostky	kostka	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nebo	nebo	k8xC
způsob	způsob	k1gInSc1
speedcubery	speedcubera	k1gFnSc2
oblíbenější	oblíbený	k2eAgFnSc2d2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
spojí	spojit	k5eAaPmIp3nP
dohromady	dohromady	k6eAd1
středy	střed	k1gInPc1
stejných	stejný	k2eAgFnPc2d1
barev	barva	k1gFnPc2
<g/>
,	,	kIx,
pak	pak	k6eAd1
hrany	hrana	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
mají	mít	k5eAaImIp3nP
obě	dva	k4xCgFnPc1
barvy	barva	k1gFnPc1
stejné	stejný	k2eAgFnPc1d1
a	a	k8xC
nakonec	nakonec	k6eAd1
se	se	k3xPyFc4
kostka	kostka	k1gFnSc1
celá	celý	k2eAgFnSc1d1
skládá	skládat	k5eAaImIp3nS
jako	jako	k9
původní	původní	k2eAgFnSc1d1
Rubikova	Rubikův	k2eAgFnSc1d1
kostka	kostka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Problémem	problém	k1gInSc7
u	u	k7c2
tohoto	tento	k3xDgInSc2
hlavolamu	hlavolam	k1gInSc2
je	být	k5eAaImIp3nS
problém	problém	k1gInSc1
parity	parita	k1gFnSc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
znaménko	znaménko	k1gNnSc4
permutace	permutace	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
nemůže	moct	k5eNaImIp3nS
nastat	nastat	k5eAaPmF
u	u	k7c2
menších	malý	k2eAgFnPc2d2
kostek	kostka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existují	existovat	k5eAaImIp3nP
i	i	k9
metody	metoda	k1gFnPc1
skládání	skládání	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
se	se	k3xPyFc4
těmto	tento	k3xDgInPc3
problémům	problém	k1gInPc3
snaží	snažit	k5eAaImIp3nS
předcházet	předcházet	k5eAaImF
<g/>
,	,	kIx,
ale	ale	k8xC
samotné	samotný	k2eAgNnSc1d1
řešení	řešení	k1gNnSc1
problému	problém	k1gInSc2
bývá	bývat	k5eAaImIp3nS
zpravidla	zpravidla	k6eAd1
jednodušší	jednoduchý	k2eAgMnSc1d2
<g/>
.	.	kIx.
</s>
<s>
Nejrychlejší	rychlý	k2eAgNnSc1d3
složení	složení	k1gNnSc1
</s>
<s>
Světový	světový	k2eAgInSc1d1
rekord	rekord	k1gInSc1
17,42	17,42	k4
sekundy	sekunda	k1gFnSc2
dosáhl	dosáhnout	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
2019	#num#	k4
Sebastian	Sebastian	k1gMnSc1
Weyer	Weyer	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgInPc4d1
rekordy	rekord	k1gInPc4
držel	držet	k5eAaImAgMnS
v	v	k7c6
roce	rok	k1gInSc6
2019	#num#	k4
Matěj	Matěj	k1gMnSc1
Grohmann	Grohmann	k1gMnSc1
s	s	k7c7
nejlepším	dobrý	k2eAgInSc7d3
časem	čas	k1gInSc7
25,20	25,20	k4
a	a	k8xC
průměrem	průměr	k1gInSc7
30,02	30,02	k4
sekund	sekunda	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
World	World	k1gInSc1
Cube	Cube	k1gNnSc1
Association	Association	k1gInSc1
Official	Official	k1gMnSc1
Results	Resultsa	k1gFnPc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Rubikova	Rubikův	k2eAgFnSc1d1
pomsta	pomsta	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
