<s desamb="1">
Pohonný	pohonný	k2eAgInSc1d1
systém	systém	k1gInSc1
tvoří	tvořit	k5eAaImIp3nS
dva	dva	k4xCgInPc4
diesely	diesel	k1gInPc4
Zvezda	Zvezdo	k1gNnSc2
M	M	kA
<g/>
401	#num#	k4
<g/>
B	B	kA
<g/>
,	,	kIx,
každý	každý	k3xTgMnSc1
o	o	k7c6
výkonu	výkon	k1gInSc6
1100	#num#	k4
hp	hp	k?
<g/>
,	,	kIx,
pohánějící	pohánějící	k2eAgInPc4d1
dva	dva	k4xCgInPc4
lodní	lodní	k2eAgInPc4d1
šrouby	šroub	k1gInPc4
s	s	k7c7
pevnými	pevný	k2eAgFnPc7d1
lopatkami	lopatka	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Projekt	projekt	k1gInSc1
12130	#num#	k4
(	(	kIx(
<g/>
jinak	jinak	k6eAd1
též	též	k9
třída	třída	k1gFnSc1
Ogonek	Ogonky	k1gFnPc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
třída	třída	k1gFnSc1
říčních	říční	k2eAgFnPc2d1
hlídkových	hlídkový	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
ruské	ruský	k2eAgFnSc2d1
pobřežní	pobřežní	k2eAgFnSc2d1
stráže	stráž	k1gFnSc2
<g/>
.	.	kIx.
</s>