<s>
Platón	Platón	k1gMnSc1	Platón
založil	založit	k5eAaPmAgMnS	založit
athénskou	athénský	k2eAgFnSc4d1	Athénská
Akademii	akademie	k1gFnSc4	akademie
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
vzorem	vzor	k1gInSc7	vzor
evropským	evropský	k2eAgFnPc3d1	Evropská
univerzitám	univerzita	k1gFnPc3	univerzita
a	a	k8xC	a
vědeckým	vědecký	k2eAgFnPc3d1	vědecká
institucím	instituce	k1gFnPc3	instituce
<g/>
.	.	kIx.	.
</s>
