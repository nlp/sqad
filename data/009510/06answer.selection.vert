<s>
Roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
přistáli	přistát	k5eAaImAgMnP	přistát
Neil	Neil	k1gMnSc1	Neil
Armstrong	Armstrong	k1gMnSc1	Armstrong
a	a	k8xC	a
Edwin	Edwin	k2eAgInSc1d1	Edwin
Aldrin	aldrin	k1gInSc1	aldrin
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
programu	program	k1gInSc2	program
Apollo	Apollo	k1gMnSc1	Apollo
jako	jako	k8xS	jako
první	první	k4xOgMnPc1	první
lidé	člověk	k1gMnPc1	člověk
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
i	i	k9	i
prvními	první	k4xOgMnPc7	první
lidmi	člověk	k1gMnPc7	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
stanuli	stanout	k5eAaPmAgMnP	stanout
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
jiného	jiný	k2eAgNnSc2d1	jiné
vesmírného	vesmírný	k2eAgNnSc2d1	vesmírné
tělesa	těleso	k1gNnSc2	těleso
než	než	k8xS	než
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
