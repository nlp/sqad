<p>
<s>
Dodoma	Dodoma	k1gFnSc1	Dodoma
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
potopila	potopit	k5eAaPmAgFnS	potopit
se	se	k3xPyFc4	se
<g/>
"	"	kIx"	"
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
Gogo	Gogo	k6eAd1	Gogo
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Tanzanie	Tanzanie	k1gFnSc2	Tanzanie
<g/>
,	,	kIx,	,
s	s	k7c7	s
324	[number]	k4	324
347	[number]	k4	347
obyvateli	obyvatel	k1gMnPc7	obyvatel
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
třetí	třetí	k4xOgFnSc7	třetí
největší	veliký	k2eAgFnSc7d3	veliký
město	město	k1gNnSc4	město
země	zem	k1gFnSc2	zem
a	a	k8xC	a
také	také	k9	také
správní	správní	k2eAgNnSc1d1	správní
středisko	středisko	k1gNnSc1	středisko
regionu	region	k1gInSc2	region
Dodoma	Dodoma	k1gFnSc1	Dodoma
<g/>
.	.	kIx.	.
</s>
<s>
Plány	plán	k1gInPc1	plán
na	na	k7c4	na
přesunutí	přesunutí	k1gNnSc4	přesunutí
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
do	do	k7c2	do
Dodomy	Dodom	k1gInPc1	Dodom
byly	být	k5eAaImAgInP	být
vytvořeny	vytvořit	k5eAaPmNgInP	vytvořit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
<g/>
.	.	kIx.	.
</s>
<s>
Tanzanské	tanzanský	k2eAgNnSc1d1	tanzanské
Národní	národní	k2eAgNnSc1d1	národní
shromáždění	shromáždění	k1gNnSc1	shromáždění
se	se	k3xPyFc4	se
sem	sem	k6eAd1	sem
přesunulo	přesunout	k5eAaPmAgNnS	přesunout
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
mnoho	mnoho	k4c1	mnoho
vládních	vládní	k2eAgInPc2d1	vládní
úřadů	úřad	k1gInPc2	úřad
přetrvává	přetrvávat	k5eAaImIp3nS	přetrvávat
v	v	k7c6	v
předchozím	předchozí	k2eAgNnSc6d1	předchozí
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
Dar	dar	k1gInSc4	dar
es	es	k1gNnSc2	es
Salaamu	Salaam	k1gInSc2	Salaam
(	(	kIx(	(
<g/>
který	který	k3yQgInSc1	který
zároveň	zároveň	k6eAd1	zároveň
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
obchodním	obchodní	k2eAgNnSc7d1	obchodní
centrem	centrum	k1gNnSc7	centrum
státu	stát	k1gInSc2	stát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dodoma	Dodoma	k1gFnSc1	Dodoma
je	být	k5eAaImIp3nS	být
obývána	obývat	k5eAaImNgFnS	obývat
především	především	k6eAd1	především
třemi	tři	k4xCgInPc7	tři
kmeny	kmen	k1gInPc7	kmen
s	s	k7c7	s
převahou	převaha	k1gFnSc7	převaha
příslušníků	příslušník	k1gMnPc2	příslušník
kmenů	kmen	k1gInPc2	kmen
Gogo	Gogo	k1gMnSc1	Gogo
(	(	kIx(	(
<g/>
Wagogo	Wagogo	k1gMnSc1	Wagogo
<g/>
)	)	kIx)	)
a	a	k8xC	a
Warangi	Warang	k1gFnSc2	Warang
a	a	k8xC	a
minoritním	minoritní	k2eAgInSc7d1	minoritní
kmenem	kmen	k1gInSc7	kmen
Sandawe	Sandaw	k1gInSc2	Sandaw
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
jazyk	jazyk	k1gInSc1	jazyk
je	být	k5eAaImIp3nS	být
příbuzný	příbuzný	k2eAgInSc1d1	příbuzný
Křovákům	Křovák	k1gMnPc3	Křovák
z	z	k7c2	z
pouště	poušť	k1gFnSc2	poušť
Kalahari	Kalahar	k1gFnSc2	Kalahar
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poloha	poloha	k1gFnSc1	poloha
==	==	k?	==
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c4	na
6	[number]	k4	6
<g/>
°	°	k?	°
<g/>
10	[number]	k4	10
<g/>
'	'	kIx"	'
<g/>
23	[number]	k4	23
<g/>
"	"	kIx"	"
j.š.	j.š.	k?	j.š.
a	a	k8xC	a
35	[number]	k4	35
<g/>
°	°	k?	°
<g/>
44	[number]	k4	44
<g/>
'	'	kIx"	'
<g/>
31	[number]	k4	31
<g/>
"	"	kIx"	"
v.d.	v.d.	k?	v.d.
<g/>
,	,	kIx,	,
uprostřed	uprostřed	k7c2	uprostřed
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
486	[number]	k4	486
km	km	kA	km
západně	západně	k6eAd1	západně
od	od	k7c2	od
bývalého	bývalý	k2eAgNnSc2d1	bývalé
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Dar	dar	k1gInSc4	dar
es	es	k1gNnSc2	es
Salaamu	Salaam	k1gInSc2	Salaam
a	a	k8xC	a
441	[number]	k4	441
km	km	kA	km
od	od	k7c2	od
Arushy	Arusha	k1gFnSc2	Arusha
<g/>
,	,	kIx,	,
ústředí	ústředí	k1gNnSc2	ústředí
Východoafrické	východoafrický	k2eAgFnSc2d1	východoafrická
komunity	komunita	k1gFnSc2	komunita
<g/>
.	.	kIx.	.
</s>
<s>
Rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
se	se	k3xPyFc4	se
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
2	[number]	k4	2
669	[number]	k4	669
kilometrů	kilometr	k1gInPc2	kilometr
čtverečních	čtvereční	k2eAgInPc2d1	čtvereční
<g/>
,	,	kIx,	,
z	z	k7c2	z
kterých	který	k3yQgInPc2	který
je	být	k5eAaImIp3nS	být
625	[number]	k4	625
kilometrů	kilometr	k1gInPc2	kilometr
čtverečních	čtvereční	k2eAgInPc2d1	čtvereční
zastavěno	zastavěn	k2eAgNnSc4d1	zastavěno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Demografie	demografie	k1gFnSc2	demografie
==	==	k?	==
</s>
</p>
<p>
<s>
Z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
je	být	k5eAaImIp3nS	být
157	[number]	k4	157
469	[number]	k4	469
(	(	kIx(	(
<g/>
48,5	[number]	k4	48,5
%	%	kIx~	%
<g/>
)	)	kIx)	)
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
166	[number]	k4	166
878	[number]	k4	878
(	(	kIx(	(
<g/>
51,5	[number]	k4	51,5
%	%	kIx~	%
<g/>
)	)	kIx)	)
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Odhadovaný	odhadovaný	k2eAgInSc1d1	odhadovaný
celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
domácností	domácnost	k1gFnPc2	domácnost
je	být	k5eAaImIp3nS	být
74	[number]	k4	74
914	[number]	k4	914
<g/>
,	,	kIx,	,
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
průměrně	průměrně	k6eAd1	průměrně
žije	žít	k5eAaImIp3nS	žít
4,3	[number]	k4	4,3
obyvatele	obyvatel	k1gMnSc4	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
církev	církev	k1gFnSc1	církev
udává	udávat	k5eAaImIp3nS	udávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
19,2	[number]	k4	19,2
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
jsou	být	k5eAaImIp3nP	být
římští	římský	k2eAgMnPc1d1	římský
katolíci	katolík	k1gMnPc1	katolík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
Dodoma	Dodomum	k1gNnSc2	Dodomum
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
během	během	k7c2	během
německé	německý	k2eAgFnSc2d1	německá
koloniální	koloniální	k2eAgFnSc2d1	koloniální
vlády	vláda	k1gFnSc2	vláda
společně	společně	k6eAd1	společně
se	s	k7c7	s
stavbou	stavba	k1gFnSc7	stavba
Tanzanské	tanzanský	k2eAgFnSc2d1	tanzanská
centrální	centrální	k2eAgFnSc2d1	centrální
železnice	železnice	k1gFnSc2	železnice
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
obsazení	obsazení	k1gNnSc6	obsazení
města	město	k1gNnSc2	město
Brity	Brit	k1gMnPc7	Brit
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Dodoma	Dodoma	k1gFnSc1	Dodoma
regionálním	regionální	k2eAgMnSc7d1	regionální
administrativním	administrativní	k2eAgMnSc7d1	administrativní
centrem	centr	k1gMnSc7	centr
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnPc3	který
zůstala	zůstat	k5eAaPmAgFnS	zůstat
až	až	k9	až
do	do	k7c2	do
vyhlášení	vyhlášení	k1gNnSc2	vyhlášení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
Tanzanie	Tanzanie	k1gFnSc2	Tanzanie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
pozici	pozice	k1gFnSc3	pozice
uprostřed	uprostřed	k7c2	uprostřed
země	zem	k1gFnSc2	zem
bylo	být	k5eAaImAgNnS	být
referendem	referendum	k1gNnSc7	referendum
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
o	o	k7c4	o
přesunutí	přesunutí	k1gNnSc4	přesunutí
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
do	do	k7c2	do
Dodomy	Dodom	k1gInPc1	Dodom
z	z	k7c2	z
Dar	dar	k1gInSc1	dar
es	es	k1gNnSc7	es
Salaamu	Salaam	k1gInSc2	Salaam
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgNnSc1d1	národní
shromáždění	shromáždění	k1gNnSc1	shromáždění
se	se	k3xPyFc4	se
přemístilo	přemístit	k5eAaPmAgNnS	přemístit
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
doby	doba	k1gFnSc2	doba
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
vládních	vládní	k2eAgInPc2d1	vládní
úřadů	úřad	k1gInPc2	úřad
v	v	k7c6	v
bývalém	bývalý	k2eAgNnSc6d1	bývalé
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Infrastruktura	infrastruktura	k1gFnSc1	infrastruktura
==	==	k?	==
</s>
</p>
<p>
<s>
Páteřní	páteřní	k2eAgFnSc1d1	páteřní
silnice	silnice	k1gFnSc1	silnice
spojuje	spojovat	k5eAaImIp3nS	spojovat
Dodomu	Dodom	k1gInSc2	Dodom
s	s	k7c7	s
bývalým	bývalý	k2eAgNnSc7d1	bývalé
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Dar	dar	k1gInSc1	dar
es	es	k1gNnSc6	es
Salaamem	Salaam	k1gInSc7	Salaam
přes	přes	k7c4	přes
region	region	k1gInSc4	region
Morogoro	Morogora	k1gFnSc5	Morogora
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
západ	západ	k1gInSc4	západ
vedou	vést	k5eAaImIp3nP	vést
silnice	silnice	k1gFnPc1	silnice
do	do	k7c2	do
Mwanzy	Mwanza	k1gFnSc2	Mwanza
a	a	k8xC	a
Kigomy	Kigom	k1gInPc4	Kigom
přes	přes	k7c4	přes
Taboru	Tabora	k1gFnSc4	Tabora
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
severní	severní	k2eAgFnSc1d1	severní
silnice	silnice	k1gFnSc1	silnice
spojuje	spojovat	k5eAaImIp3nS	spojovat
město	město	k1gNnSc4	město
s	s	k7c7	s
Arushou	Arusha	k1gFnSc7	Arusha
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
.	.	kIx.	.
</s>
<s>
Městu	město	k1gNnSc3	město
také	také	k9	také
slouží	sloužit	k5eAaImIp3nS	sloužit
Centrální	centrální	k2eAgFnSc1d1	centrální
železnice	železnice	k1gFnSc1	železnice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ho	on	k3xPp3gMnSc4	on
spojuje	spojovat	k5eAaImIp3nS	spojovat
se	s	k7c7	s
465	[number]	k4	465
kilometrů	kilometr	k1gInPc2	kilometr
vzdáleným	vzdálený	k2eAgInSc7d1	vzdálený
Dar	dar	k1gInSc1	dar
es	es	k1gNnSc2	es
Salaamem	Salaam	k1gInSc7	Salaam
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
má	mít	k5eAaImIp3nS	mít
letiště	letiště	k1gNnSc4	letiště
řízené	řízený	k2eAgNnSc1d1	řízené
Civilním	civilní	k2eAgInSc7d1	civilní
leteckým	letecký	k2eAgInSc7d1	letecký
úřadem	úřad	k1gInSc7	úřad
Tanzanie	Tanzanie	k1gFnSc2	Tanzanie
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
velikost	velikost	k1gFnSc4	velikost
dopravních	dopravní	k2eAgInPc2d1	dopravní
prostředků	prostředek	k1gInPc2	prostředek
omezena	omezit	k5eAaPmNgFnS	omezit
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
malá	malý	k2eAgNnPc4d1	malé
soukromá	soukromý	k2eAgNnPc4d1	soukromé
letadla	letadlo	k1gNnPc4	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
Plánuje	plánovat	k5eAaImIp3nS	plánovat
se	se	k3xPyFc4	se
vybudovat	vybudovat	k5eAaPmF	vybudovat
nové	nový	k2eAgNnSc4d1	nové
letiště	letiště	k1gNnSc4	letiště
za	za	k7c7	za
městem	město	k1gNnSc7	město
s	s	k7c7	s
vyšším	vysoký	k2eAgInSc7d2	vyšší
a	a	k8xC	a
rozměrnějším	rozměrný	k2eAgInSc7d2	rozměrnější
terminálem	terminál	k1gInSc7	terminál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vzdělání	vzdělání	k1gNnSc1	vzdělání
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Dodomě	Dodomě	k1gFnSc6	Dodomě
dvě	dva	k4xCgFnPc4	dva
univerzity	univerzita	k1gFnPc4	univerzita
<g/>
,	,	kIx,	,
první	první	k4xOgNnSc4	první
St.	st.	kA	st.
John	John	k1gMnSc1	John
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
University	universita	k1gFnSc2	universita
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
The	The	k1gFnSc1	The
University	universita	k1gFnSc2	universita
Dodoma	Dodomum	k1gNnSc2	Dodomum
neboli	neboli	k8xC	neboli
zkráceně	zkráceně	k6eAd1	zkráceně
"	"	kIx"	"
<g/>
UDOM	UDOM	kA	UDOM
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
UDOM	UDOM	kA	UDOM
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
září	září	k1gNnSc6	září
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
má	mít	k5eAaImIp3nS	mít
1	[number]	k4	1
500	[number]	k4	500
studentů	student	k1gMnPc2	student
a	a	k8xC	a
za	za	k7c4	za
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
roky	rok	k1gInPc4	rok
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
kapacitu	kapacita	k1gFnSc4	kapacita
40	[number]	k4	40
000	[number]	k4	000
studentů	student	k1gMnPc2	student
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Partnerská	partnerský	k2eAgNnPc1d1	partnerské
města	město	k1gNnPc1	město
==	==	k?	==
</s>
</p>
<p>
<s>
Bangui	Bangui	k1gNnSc1	Bangui
<g/>
,	,	kIx,	,
Středoafrická	středoafrický	k2eAgFnSc1d1	Středoafrická
republika	republika	k1gFnSc1	republika
</s>
</p>
<p>
<s>
Džajpur	Džajpur	k1gMnSc1	Džajpur
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc1	Indie
</s>
</p>
<p>
<s>
Watsa	Watsa	k1gFnSc1	Watsa
<g/>
,	,	kIx,	,
Demokratická	demokratický	k2eAgFnSc1d1	demokratická
republika	republika	k1gFnSc1	republika
Kongo	Kongo	k1gNnSc1	Kongo
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Dodoma	Dodomum	k1gNnSc2	Dodomum
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
