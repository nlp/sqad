<s>
Indie	Indie	k1gFnSc1	Indie
<g/>
,	,	kIx,	,
oficiálním	oficiální	k2eAgInSc7d1	oficiální
názvem	název	k1gInSc7	název
Indická	indický	k2eAgFnSc1d1	indická
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Republic	Republice	k1gFnPc2	Republice
of	of	k?	of
India	indium	k1gNnPc1	indium
<g/>
;	;	kIx,	;
hindsky	hindsky	k6eAd1	hindsky
भ	भ	k?	भ
<g/>
ा	ा	k?	ा
<g/>
र	र	k?	र
ग	ग	k?	ग
<g/>
ा	ा	k?	ा
<g/>
ज	ज	k?	ज
<g/>
्	्	k?	्
<g/>
य	य	k?	य
<g/>
,	,	kIx,	,
Bhárat	Bhárat	k1gMnSc1	Bhárat
ganarádžja	ganarádžja	k1gMnSc1	ganarádžja
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
sedmá	sedmý	k4xOgFnSc1	sedmý
největší	veliký	k2eAgFnSc1d3	veliký
a	a	k8xC	a
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
miliardou	miliarda	k4xCgFnSc7	miliarda
obyvatel	obyvatel	k1gMnPc2	obyvatel
druhá	druhý	k4xOgFnSc1	druhý
nejlidnatější	lidnatý	k2eAgFnSc1d3	nejlidnatější
země	země	k1gFnSc1	země
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
rozkládající	rozkládající	k2eAgMnSc1d1	rozkládající
se	se	k3xPyFc4	se
na	na	k7c6	na
Indickém	indický	k2eAgInSc6d1	indický
subkontinentu	subkontinent	k1gInSc6	subkontinent
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
