<s>
Indie	Indie	k1gFnSc1	Indie
<g/>
,	,	kIx,	,
oficiálním	oficiální	k2eAgInSc7d1	oficiální
názvem	název	k1gInSc7	název
Indická	indický	k2eAgFnSc1d1	indická
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Republic	Republice	k1gFnPc2	Republice
of	of	k?	of
India	indium	k1gNnPc1	indium
<g/>
;	;	kIx,	;
hindsky	hindsky	k6eAd1	hindsky
भ	भ	k?	भ
<g/>
ा	ा	k?	ा
<g/>
र	र	k?	र
ग	ग	k?	ग
<g/>
ा	ा	k?	ा
<g/>
ज	ज	k?	ज
<g/>
्	्	k?	्
<g/>
य	य	k?	य
<g/>
,	,	kIx,	,
Bhárat	Bhárat	k1gMnSc1	Bhárat
ganarádžja	ganarádžja	k1gMnSc1	ganarádžja
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
sedmá	sedmý	k4xOgFnSc1	sedmý
největší	veliký	k2eAgFnSc1d3	veliký
a	a	k8xC	a
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
miliardou	miliarda	k4xCgFnSc7	miliarda
obyvatel	obyvatel	k1gMnPc2	obyvatel
druhá	druhý	k4xOgFnSc1	druhý
nejlidnatější	lidnatý	k2eAgFnSc1d3	nejlidnatější
země	země	k1gFnSc1	země
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
rozkládající	rozkládající	k2eAgMnSc1d1	rozkládající
se	se	k3xPyFc4	se
na	na	k7c6	na
Indickém	indický	k2eAgInSc6d1	indický
subkontinentu	subkontinent	k1gInSc6	subkontinent
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
politického	politický	k2eAgNnSc2d1	politické
hlediska	hledisko	k1gNnSc2	hledisko
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
svazový	svazový	k2eAgInSc4d1	svazový
stát	stát	k1gInSc4	stát
(	(	kIx(	(
<g/>
federaci	federace	k1gFnSc4	federace
<g/>
)	)	kIx)	)
se	s	k7c7	s
socialistickým	socialistický	k2eAgMnSc7d1	socialistický
<g/>
,	,	kIx,	,
demokratickým	demokratický	k2eAgMnSc7d1	demokratický
<g/>
,	,	kIx,	,
parlamentním	parlamentní	k2eAgNnSc7d1	parlamentní
zřízením	zřízení	k1gNnSc7	zřízení
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
největší	veliký	k2eAgFnSc1d3	veliký
demokracie	demokracie	k1gFnSc1	demokracie
světa	svět	k1gInSc2	svět
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
Commonwealthu	Commonwealth	k1gInSc2	Commonwealth
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
jaderné	jaderný	k2eAgFnPc4d1	jaderná
zbraně	zbraň	k1gFnPc4	zbraň
a	a	k8xC	a
disponuje	disponovat	k5eAaBmIp3nS	disponovat
vlastním	vlastní	k2eAgInSc7d1	vlastní
kosmickým	kosmický	k2eAgInSc7d1	kosmický
programem	program	k1gInSc7	program
<g/>
;	;	kIx,	;
její	její	k3xOp3gFnSc1	její
ekonomika	ekonomika	k1gFnSc1	ekonomika
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
po	po	k7c6	po
Číně	Čína	k1gFnSc6	Čína
největší	veliký	k2eAgInSc4d3	veliký
růst	růst	k1gInSc4	růst
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
regionální	regionální	k2eAgFnSc4d1	regionální
mocnost	mocnost	k1gFnSc4	mocnost
a	a	k8xC	a
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
potenciálních	potenciální	k2eAgFnPc2d1	potenciální
supervelmocí	supervelmoc	k1gFnPc2	supervelmoc
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
východě	východ	k1gInSc6	východ
Indie	Indie	k1gFnSc2	Indie
hraničí	hraničit	k5eAaImIp3nP	hraničit
s	s	k7c7	s
Bangladéšem	Bangladéš	k1gInSc7	Bangladéš
(	(	kIx(	(
<g/>
4053	[number]	k4	4053
km	km	kA	km
<g/>
)	)	kIx)	)
a	a	k8xC	a
Barmou	Barma	k1gFnSc7	Barma
(	(	kIx(	(
<g/>
1463	[number]	k4	1463
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
a	a	k8xC	a
severovýchodě	severovýchod	k1gInSc6	severovýchod
hraničí	hraničit	k5eAaImIp3nP	hraničit
s	s	k7c7	s
Čínou	Čína	k1gFnSc7	Čína
(	(	kIx(	(
<g/>
3380	[number]	k4	3380
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bhutánem	Bhután	k1gMnSc7	Bhután
(	(	kIx(	(
<g/>
605	[number]	k4	605
km	km	kA	km
<g/>
)	)	kIx)	)
a	a	k8xC	a
Nepálem	Nepál	k1gInSc7	Nepál
(	(	kIx(	(
<g/>
1690	[number]	k4	1690
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
sousedí	sousedit	k5eAaImIp3nS	sousedit
s	s	k7c7	s
Pákistánem	Pákistán	k1gInSc7	Pákistán
(	(	kIx(	(
<g/>
2912	[number]	k4	2912
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
za	za	k7c7	za
Palkovým	Palkův	k2eAgInSc7d1	Palkův
průlivem	průliv	k1gInSc7	průliv
leží	ležet	k5eAaImIp3nS	ležet
65	[number]	k4	65
km	km	kA	km
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
od	od	k7c2	od
indických	indický	k2eAgInPc2d1	indický
břehů	břeh	k1gInPc2	břeh
ostrovní	ostrovní	k2eAgNnSc1d1	ostrovní
stát	stát	k5eAaImF	stát
Šrí	Šrí	k1gFnSc4	Šrí
Lanka	lanko	k1gNnSc2	lanko
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jihozápadu	jihozápad	k1gInSc2	jihozápad
<g/>
,	,	kIx,	,
jihu	jih	k1gInSc2	jih
a	a	k8xC	a
jihovýchodu	jihovýchod	k1gInSc2	jihovýchod
omývá	omývat	k5eAaImIp3nS	omývat
indické	indický	k2eAgInPc4d1	indický
břehy	břeh	k1gInPc4	břeh
Indický	indický	k2eAgInSc1d1	indický
oceán	oceán	k1gInSc1	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Indie	Indie	k1gFnSc2	Indie
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Nové	Nové	k2eAgNnSc1d1	Nové
Dillí	Dillí	k1gNnSc1	Dillí
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
Indie	Indie	k1gFnSc2	Indie
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
staroperské	staroperský	k2eAgFnSc2d1	staroperský
verze	verze	k1gFnSc2	verze
slova	slovo	k1gNnSc2	slovo
Sindhu	Sindh	k1gInSc2	Sindh
<g/>
,	,	kIx,	,
historického	historický	k2eAgNnSc2d1	historické
místního	místní	k2eAgNnSc2d1	místní
jména	jméno	k1gNnSc2	jméno
pro	pro	k7c4	pro
řeku	řeka	k1gFnSc4	řeka
Indus	Indus	k1gInSc1	Indus
<g/>
.	.	kIx.	.
</s>
<s>
Ústava	ústava	k1gFnSc1	ústava
Indie	Indie	k1gFnSc2	Indie
uznává	uznávat	k5eAaImIp3nS	uznávat
jako	jako	k9	jako
běžné	běžný	k2eAgNnSc1d1	běžné
používání	používání	k1gNnSc1	používání
i	i	k9	i
výraz	výraz	k1gInSc1	výraz
Bhárat	Bhárat	k1gInSc1	Bhárat
(	(	kIx(	(
<g/>
hin	hin	k1gInSc1	hin
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
<g/>
भ	भ	k?	भ
<g/>
ा	ा	k?	ा
<g/>
र	र	k?	र
/	/	kIx~	/
bh	bh	k?	bh
<g/>
̪	̪	k?	̪
/	/	kIx~	/
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
oficiální	oficiální	k2eAgNnSc4d1	oficiální
pojmenování	pojmenování	k1gNnSc4	pojmenování
stejného	stejný	k2eAgInSc2d1	stejný
statusu	status	k1gInSc2	status
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
sanskrtského	sanskrtský	k2eAgNnSc2d1	sanskrtské
jména	jméno	k1gNnSc2	jméno
krále	král	k1gMnSc2	král
Bharaty	Bharat	k1gInPc4	Bharat
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
příběh	příběh	k1gInSc1	příběh
je	být	k5eAaImIp3nS	být
zmíněn	zmínit	k5eAaPmNgInS	zmínit
v	v	k7c6	v
epické	epický	k2eAgFnSc6d1	epická
básni	báseň	k1gFnSc6	báseň
Mahábhárata	Mahábhárata	k1gFnSc1	Mahábhárata
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgNnSc1	třetí
jméno	jméno	k1gNnSc1	jméno
<g/>
,	,	kIx,	,
Hindustán	Hindustán	k1gInSc1	Hindustán
(	(	kIx(	(
<g/>
hin	hin	k1gInSc1	hin
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
<g/>
ह	ह	k?	ह
<g/>
ि	ि	k?	ि
<g/>
न	न	k?	न
<g/>
्	्	k?	्
<g/>
द	द	k?	द
<g/>
ु	ु	k?	ु
<g/>
स	स	k?	स
<g/>
्	्	k?	्
<g/>
त	त	k?	त
<g/>
ा	ा	k?	ा
<g/>
न	न	k?	न
/	/	kIx~	/
hin	hin	k1gInSc1	hin
<g/>
̪	̪	k?	̪
<g/>
d	d	k?	d
<g/>
̪	̪	k?	̪
<g/>
ust	ust	k?	ust
<g/>
̪	̪	k?	̪
<g/>
ɑ	ɑ	k?	ɑ
/	/	kIx~	/
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
per	pero	k1gNnPc2	pero
<g/>
.	.	kIx.	.
</s>
<s>
Hindská	hindský	k2eAgFnSc1d1	hindská
země	země	k1gFnSc1	země
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
od	od	k7c2	od
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
ačkoli	ačkoli	k8xS	ačkoli
jeho	jeho	k3xOp3gNnSc1	jeho
současné	současný	k2eAgNnSc1d1	současné
použití	použití	k1gNnSc1	použití
je	být	k5eAaImIp3nS	být
omezeno	omezit	k5eAaPmNgNnS	omezit
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
vnitrostátních	vnitrostátní	k2eAgInPc2d1	vnitrostátní
konfliktů	konflikt	k1gInPc2	konflikt
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
toto	tento	k3xDgNnSc1	tento
slovo	slovo	k1gNnSc1	slovo
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc4	dějiny
Indie	Indie	k1gFnSc2	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnPc1d3	nejstarší
známky	známka	k1gFnPc1	známka
lidské	lidský	k2eAgFnSc2d1	lidská
kultury	kultura	k1gFnSc2	kultura
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
sahají	sahat	k5eAaImIp3nP	sahat
do	do	k7c2	do
3	[number]	k4	3
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
2	[number]	k4	2
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
byla	být	k5eAaImAgFnS	být
Indie	Indie	k1gFnSc1	Indie
obydlena	obydlet	k5eAaPmNgFnS	obydlet
indoevropským	indoevropský	k2eAgNnSc7d1	indoevropské
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Indoevropské	indoevropský	k2eAgInPc1d1	indoevropský
kmeny	kmen	k1gInPc1	kmen
ze	z	k7c2	z
střední	střední	k2eAgFnSc2d1	střední
Asie	Asie	k1gFnSc2	Asie
a	a	k8xC	a
východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
migrovaly	migrovat	k5eAaImAgFnP	migrovat
do	do	k7c2	do
Indie	Indie	k1gFnSc2	Indie
v	v	k7c6	v
několika	několik	k4yIc6	několik
vlnách	vlna	k1gFnPc6	vlna
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
smísily	smísit	k5eAaPmAgFnP	smísit
s	s	k7c7	s
Drávidy	Drávid	k1gMnPc7	Drávid
nebo	nebo	k8xC	nebo
je	on	k3xPp3gFnPc4	on
vytlačily	vytlačit	k5eAaPmAgFnP	vytlačit
na	na	k7c4	na
jih	jih	k1gInSc4	jih
Indie	Indie	k1gFnSc2	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
taktéž	taktéž	k?	taktéž
datuje	datovat	k5eAaImIp3nS	datovat
první	první	k4xOgNnSc4	první
sociální	sociální	k2eAgNnSc4d1	sociální
rozdělení	rozdělení	k1gNnSc4	rozdělení
do	do	k7c2	do
kast	kasta	k1gFnPc2	kasta
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
šířil	šířit	k5eAaImAgInS	šířit
buddhismus	buddhismus	k1gInSc1	buddhismus
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
vytlačen	vytlačit	k5eAaPmNgInS	vytlačit
hinduismem	hinduismus	k1gInSc7	hinduismus
až	až	k6eAd1	až
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
severoindické	severoindický	k2eAgFnSc2d1	severoindická
dynastie	dynastie	k1gFnSc2	dynastie
Guptovců	Guptovec	k1gMnPc2	Guptovec
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
od	od	k7c2	od
západu	západ	k1gInSc2	západ
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
islám	islám	k1gInSc4	islám
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
politicky	politicky	k6eAd1	politicky
reprezentován	reprezentovat	k5eAaImNgMnS	reprezentovat
Mughalskou	Mughalský	k2eAgFnSc7d1	Mughalská
říší	říš	k1gFnSc7	říš
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
císaře	císař	k1gMnSc2	císař
Aurangzéba	Aurangzéb	k1gMnSc2	Aurangzéb
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
vyznačoval	vyznačovat	k5eAaImAgInS	vyznačovat
netolerancí	netolerance	k1gFnSc7	netolerance
k	k	k7c3	k
nemuslimům	nemuslim	k1gInPc3	nemuslim
<g/>
,	,	kIx,	,
ovládla	ovládnout	k5eAaPmAgFnS	ovládnout
Mughalská	Mughalský	k2eAgFnSc1d1	Mughalská
říše	říše	k1gFnSc1	říše
téměř	téměř	k6eAd1	téměř
celý	celý	k2eAgInSc4d1	celý
indický	indický	k2eAgInSc4d1	indický
subkontinent	subkontinent	k1gInSc4	subkontinent
<g/>
,	,	kIx,	,
musela	muset	k5eAaImAgFnS	muset
ale	ale	k8xC	ale
potlačovat	potlačovat	k5eAaImF	potlačovat
povstání	povstání	k1gNnSc4	povstání
Rádžputů	Rádžput	k1gMnPc2	Rádžput
<g/>
,	,	kIx,	,
Sikhů	sikh	k1gMnPc2	sikh
a	a	k8xC	a
především	především	k9	především
Maráthů	Maráth	k1gMnPc2	Maráth
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnPc4	jejichž
vůdce	vůdce	k1gMnPc4	vůdce
Šivádží	Šivádž	k1gFnPc2	Šivádž
založil	založit	k5eAaPmAgInS	založit
nezávislý	závislý	k2eNgInSc1d1	nezávislý
hinduistický	hinduistický	k2eAgInSc1d1	hinduistický
stát	stát	k1gInSc1	stát
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Indii	Indie	k1gFnSc6	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
konce	konec	k1gInSc2	konec
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
na	na	k7c6	na
indickém	indický	k2eAgNnSc6d1	indické
pobřeží	pobřeží	k1gNnSc6	pobřeží
zakládaly	zakládat	k5eAaImAgFnP	zakládat
první	první	k4xOgFnPc4	první
evropské	evropský	k2eAgFnPc4d1	Evropská
obchodní	obchodní	k2eAgFnPc4d1	obchodní
osady	osada	k1gFnPc4	osada
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
kolonie	kolonie	k1gFnSc2	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1600	[number]	k4	1600
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
britská	britský	k2eAgFnSc1d1	britská
Východoindická	východoindický	k2eAgFnSc1d1	Východoindická
společnost	společnost	k1gFnSc1	společnost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
započala	započnout	k5eAaPmAgFnS	započnout
se	s	k7c7	s
systematickou	systematický	k2eAgFnSc7d1	systematická
kolonizací	kolonizace	k1gFnSc7	kolonizace
celého	celý	k2eAgInSc2d1	celý
Indického	indický	k2eAgInSc2d1	indický
subkontinentu	subkontinent	k1gInSc2	subkontinent
<g/>
.	.	kIx.	.
</s>
<s>
Vyvrcholením	vyvrcholení	k1gNnSc7	vyvrcholení
tohoto	tento	k3xDgInSc2	tento
procesu	proces	k1gInSc2	proces
byl	být	k5eAaImAgInS	být
vznik	vznik	k1gInSc1	vznik
Britské	britský	k2eAgFnSc2d1	britská
Indie	Indie	k1gFnSc2	Indie
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
území	území	k1gNnPc4	území
spravovaná	spravovaný	k2eAgNnPc4d1	spravované
přímo	přímo	k6eAd1	přímo
britskými	britský	k2eAgInPc7d1	britský
úřady	úřad	k1gInPc7	úřad
a	a	k8xC	a
na	na	k7c4	na
několik	několik	k4yIc4	několik
set	sto	k4xCgNnPc2	sto
domorodých	domorodý	k2eAgMnPc2d1	domorodý
knížecích	knížecí	k2eAgMnPc2d1	knížecí
států	stát	k1gInPc2	stát
pod	pod	k7c7	pod
britským	britský	k2eAgInSc7d1	britský
vlivem	vliv	k1gInSc7	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Koloniální	koloniální	k2eAgFnSc7d1	koloniální
mocí	moc	k1gFnSc7	moc
otřáslo	otřást	k5eAaPmAgNnS	otřást
velké	velký	k2eAgNnSc1d1	velké
indické	indický	k2eAgNnSc1d1	indické
povstání	povstání	k1gNnSc1	povstání
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
koloniální	koloniální	k2eAgFnSc3d1	koloniální
nadvládě	nadvláda	k1gFnSc3	nadvláda
Británie	Británie	k1gFnSc2	Británie
zvedl	zvednout	k5eAaPmAgMnS	zvednout
lidový	lidový	k2eAgInSc4d1	lidový
odpor	odpor	k1gInSc4	odpor
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc4	jehož
nenásilnou	násilný	k2eNgFnSc4d1	nenásilná
podobu	podoba	k1gFnSc4	podoba
symbolizoval	symbolizovat	k5eAaImAgMnS	symbolizovat
Mahátma	Mahátm	k1gMnSc4	Mahátm
Gándhí	Gándhí	k1gNnSc2	Gándhí
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
zahubil	zahubit	k5eAaPmAgInS	zahubit
hladomor	hladomor	k1gInSc1	hladomor
v	v	k7c6	v
Bengálsku	Bengálsko	k1gNnSc6	Bengálsko
až	až	k6eAd1	až
tři	tři	k4xCgInPc4	tři
miliony	milion	k4xCgInPc4	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
byla	být	k5eAaImAgFnS	být
Britská	britský	k2eAgFnSc1d1	britská
Indie	Indie	k1gFnSc1	Indie
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
převážně	převážně	k6eAd1	převážně
muslimský	muslimský	k2eAgInSc4d1	muslimský
Západní	západní	k2eAgInSc4d1	západní
Pákistán	Pákistán	k1gInSc4	Pákistán
a	a	k8xC	a
Východní	východní	k2eAgInSc4d1	východní
Pákistán	Pákistán	k1gInSc4	Pákistán
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
nezávislý	závislý	k2eNgInSc1d1	nezávislý
Bangladéš	Bangladéš	k1gInSc1	Bangladéš
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c4	na
převážně	převážně	k6eAd1	převážně
hinduistickou	hinduistický	k2eAgFnSc4d1	hinduistická
Indii	Indie	k1gFnSc4	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozdělení	rozdělení	k1gNnSc6	rozdělení
Indie	Indie	k1gFnSc2	Indie
na	na	k7c6	na
základě	základ	k1gInSc6	základ
náboženství	náboženství	k1gNnSc2	náboženství
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
masakrům	masakr	k1gInPc3	masakr
a	a	k8xC	a
své	svůj	k3xOyFgInPc4	svůj
domovy	domov	k1gInPc4	domov
opustilo	opustit	k5eAaPmAgNnS	opustit
až	až	k9	až
15	[number]	k4	15
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Ústavou	ústava	k1gFnSc7	ústava
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
je	být	k5eAaImIp3nS	být
samostatná	samostatný	k2eAgFnSc1d1	samostatná
Indie	Indie	k1gFnSc1	Indie
socialistickou	socialistický	k2eAgFnSc7d1	socialistická
demokratickou	demokratický	k2eAgFnSc7d1	demokratická
republikou	republika	k1gFnSc7	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
obsadila	obsadit	k5eAaPmAgFnS	obsadit
indická	indický	k2eAgFnSc1d1	indická
armáda	armáda	k1gFnSc1	armáda
po	po	k7c6	po
krátké	krátký	k2eAgFnSc6d1	krátká
válce	válka	k1gFnSc6	válka
území	území	k1gNnSc2	území
Portugalské	portugalský	k2eAgFnSc2d1	portugalská
Indie	Indie	k1gFnSc2	Indie
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
bylo	být	k5eAaImAgNnS	být
anektováno	anektován	k2eAgNnSc1d1	anektováno
himálajské	himálajský	k2eAgNnSc1d1	himálajské
knížectví	knížectví	k1gNnSc1	knížectví
Sikkim	Sikkima	k1gFnPc2	Sikkima
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
multietnický	multietnický	k2eAgInSc4d1	multietnický
a	a	k8xC	a
multináboženský	multináboženský	k2eAgInSc4d1	multináboženský
stát	stát	k1gInSc4	stát
zažila	zažít	k5eAaPmAgFnS	zažít
Indie	Indie	k1gFnSc1	Indie
několik	několik	k4yIc4	několik
náboženských	náboženský	k2eAgFnPc2d1	náboženská
násilností	násilnost	k1gFnPc2	násilnost
a	a	k8xC	a
povstání	povstání	k1gNnSc2	povstání
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
částech	část	k1gFnPc6	část
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
to	ten	k3xDgNnSc1	ten
ji	on	k3xPp3gFnSc4	on
neoslabilo	oslabit	k5eNaPmAgNnS	oslabit
a	a	k8xC	a
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
jako	jako	k9	jako
suverénní	suverénní	k2eAgFnSc1d1	suverénní
demokracie	demokracie	k1gFnSc1	demokracie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
byla	být	k5eAaImAgFnS	být
sikhskými	sikhský	k2eAgFnPc7d1	sikhská
separatisty	separatista	k1gMnSc2	separatista
zavražděna	zavražděn	k2eAgFnSc1d1	zavražděna
ministerská	ministerský	k2eAgFnSc1d1	ministerská
předsedkyně	předsedkyně	k1gFnSc1	předsedkyně
Indira	Indira	k1gFnSc1	Indira
Gándhíová	Gándhíová	k1gFnSc1	Gándhíová
a	a	k8xC	a
separatistické	separatistický	k2eAgInPc1d1	separatistický
a	a	k8xC	a
etnické	etnický	k2eAgInPc1d1	etnický
konflikty	konflikt	k1gInPc1	konflikt
dodnes	dodnes	k6eAd1	dodnes
sužují	sužovat	k5eAaImIp3nP	sužovat
Ásám	Ásám	k1gInSc4	Ásám
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
státy	stát	k1gInPc4	stát
v	v	k7c6	v
severovýchodní	severovýchodní	k2eAgFnSc6d1	severovýchodní
Indii	Indie	k1gFnSc6	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Indie	Indie	k1gFnSc1	Indie
má	mít	k5eAaImIp3nS	mít
několik	několik	k4yIc4	několik
otevřených	otevřený	k2eAgInPc2d1	otevřený
sporů	spor	k1gInPc2	spor
o	o	k7c4	o
hranice	hranice	k1gFnPc4	hranice
s	s	k7c7	s
Čínou	Čína	k1gFnSc7	Čína
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
vystupňovaly	vystupňovat	k5eAaPmAgInP	vystupňovat
do	do	k7c2	do
krátké	krátký	k2eAgFnSc2d1	krátká
čínsko-indické	čínskondický	k2eAgFnSc2d1	čínsko-indický
války	válka	k1gFnSc2	válka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
a	a	k8xC	a
s	s	k7c7	s
Pákistánem	Pákistán	k1gInSc7	Pákistán
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
vyústily	vyústit	k5eAaPmAgFnP	vyústit
do	do	k7c2	do
válek	válka	k1gFnPc2	válka
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
1971	[number]	k4	1971
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
v	v	k7c6	v
Kargilu	Kargil	k1gInSc6	Kargil
<g/>
.	.	kIx.	.
</s>
<s>
Problémem	problém	k1gInSc7	problém
mezi	mezi	k7c7	mezi
Pákistánem	Pákistán	k1gInSc7	Pákistán
a	a	k8xC	a
Indií	Indie	k1gFnPc2	Indie
byla	být	k5eAaImAgFnS	být
po	po	k7c4	po
celou	celá	k1gFnSc4	celá
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovinu	polovina	k1gFnSc4	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
oblast	oblast	k1gFnSc4	oblast
Kašmíru	Kašmír	k1gInSc2	Kašmír
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yIgInSc4	který
si	se	k3xPyFc3	se
dělaly	dělat	k5eAaImAgFnP	dělat
nárok	nárok	k1gInSc4	nárok
obě	dva	k4xCgFnPc1	dva
země	země	k1gFnSc1	země
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
je	být	k5eAaImIp3nS	být
Kašmír	Kašmír	k1gInSc1	Kašmír
politicky	politicky	k6eAd1	politicky
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
Indie	Indie	k1gFnSc1	Indie
provedla	provést	k5eAaPmAgFnS	provést
podzemní	podzemní	k2eAgFnPc4d1	podzemní
zkoušky	zkouška	k1gFnPc4	zkouška
jaderných	jaderný	k2eAgFnPc2d1	jaderná
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ji	on	k3xPp3gFnSc4	on
neoficiálně	neoficiálně	k6eAd1	neoficiálně
dělá	dělat	k5eAaImIp3nS	dělat
členem	člen	k1gInSc7	člen
"	"	kIx"	"
<g/>
jaderného	jaderný	k2eAgInSc2d1	jaderný
klubu	klub	k1gInSc2	klub
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
následovala	následovat	k5eAaImAgFnS	následovat
série	série	k1gFnSc1	série
dalších	další	k2eAgInPc2d1	další
pěti	pět	k4xCc2	pět
testů	test	k1gInPc2	test
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgFnPc1d1	významná
ekonomické	ekonomický	k2eAgFnPc1d1	ekonomická
reformy	reforma	k1gFnPc1	reforma
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
začaly	začít	k5eAaPmAgFnP	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
transformovaly	transformovat	k5eAaBmAgFnP	transformovat
Indii	Indie	k1gFnSc4	Indie
mezi	mezi	k7c4	mezi
země	zem	k1gFnPc4	zem
s	s	k7c7	s
nejrychleji	rychle	k6eAd3	rychle
rostoucím	rostoucí	k2eAgNnSc7d1	rostoucí
hospodářstvím	hospodářství	k1gNnSc7	hospodářství
na	na	k7c6	na
světě	svět	k1gInSc6	svět
a	a	k8xC	a
přidala	přidat	k5eAaPmAgFnS	přidat
si	se	k3xPyFc3	se
na	na	k7c6	na
celosvětovém	celosvětový	k2eAgInSc6d1	celosvětový
a	a	k8xC	a
regionálním	regionální	k2eAgInSc6d1	regionální
vlivu	vliv	k1gInSc6	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Indie	Indie	k1gFnSc1	Indie
je	být	k5eAaImIp3nS	být
suverénní	suverénní	k2eAgFnSc1d1	suverénní
<g/>
,	,	kIx,	,
sekulární	sekulární	k2eAgFnSc1d1	sekulární
a	a	k8xC	a
demokratická	demokratický	k2eAgFnSc1d1	demokratická
republika	republika	k1gFnSc1	republika
s	s	k7c7	s
kvazi-federální	kvaziederální	k2eAgFnSc7d1	kvazi-federální
formou	forma	k1gFnSc7	forma
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
dvoukomorovým	dvoukomorový	k2eAgInSc7d1	dvoukomorový
parlamentem	parlament	k1gInSc7	parlament
fungujícím	fungující	k2eAgInSc7d1	fungující
podle	podle	k7c2	podle
Westminsterského	Westminsterský	k2eAgInSc2d1	Westminsterský
parlamentního	parlamentní	k2eAgInSc2d1	parlamentní
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgFnPc4	tři
formy	forma	k1gFnPc4	forma
řízení	řízení	k1gNnSc2	řízení
<g/>
:	:	kIx,	:
legislativu	legislativa	k1gFnSc4	legislativa
<g/>
,	,	kIx,	,
exekutivu	exekutiva	k1gFnSc4	exekutiva
a	a	k8xC	a
soudní	soudní	k2eAgFnSc4d1	soudní
moc	moc	k1gFnSc4	moc
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
je	být	k5eAaImIp3nS	být
hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc2	stát
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
reprezentativní	reprezentativní	k2eAgFnSc4d1	reprezentativní
úlohu	úloha	k1gFnSc4	úloha
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
interpretování	interpretování	k1gNnSc4	interpretování
ústavy	ústava	k1gFnSc2	ústava
<g/>
,	,	kIx,	,
podepisování	podepisování	k1gNnSc2	podepisování
zákonů	zákon	k1gInPc2	zákon
<g/>
,	,	kIx,	,
udělování	udělování	k1gNnSc3	udělování
milostí	milost	k1gFnPc2	milost
a	a	k8xC	a
vyhlašování	vyhlašování	k1gNnSc2	vyhlašování
stavu	stav	k1gInSc2	stav
nouze	nouze	k1gFnSc2	nouze
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
i	i	k9	i
vrchním	vrchní	k2eAgMnSc7d1	vrchní
velitelem	velitel	k1gMnSc7	velitel
Indických	indický	k2eAgFnPc2d1	indická
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
a	a	k8xC	a
viceprezident	viceprezident	k1gMnSc1	viceprezident
jsou	být	k5eAaImIp3nP	být
voleni	volit	k5eAaImNgMnP	volit
nepřímo	přímo	k6eNd1	přímo
voliči	volič	k1gMnPc1	volič
na	na	k7c4	na
pětileté	pětiletý	k2eAgNnSc4d1	pětileté
období	období	k1gNnSc4	období
<g/>
.	.	kIx.	.
</s>
<s>
Premiér	premiér	k1gMnSc1	premiér
je	být	k5eAaImIp3nS	být
předsedou	předseda	k1gMnSc7	předseda
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
nejvíce	nejvíce	k6eAd1	nejvíce
výkonných	výkonný	k2eAgFnPc2d1	výkonná
pravomocí	pravomoc	k1gFnPc2	pravomoc
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
volen	volit	k5eAaImNgMnS	volit
zákonodárci	zákonodárce	k1gMnPc7	zákonodárce
politických	politický	k2eAgFnPc2d1	politická
stran	strana	k1gFnPc2	strana
nebo	nebo	k8xC	nebo
koalicí	koalice	k1gFnPc2	koalice
majících	mající	k2eAgFnPc2d1	mající
parlamentní	parlamentní	k2eAgFnSc4d1	parlamentní
většinu	většina	k1gFnSc4	většina
a	a	k8xC	a
slouží	sloužit	k5eAaImIp3nS	sloužit
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Ústava	ústava	k1gFnSc1	ústava
otevřeně	otevřeně	k6eAd1	otevřeně
nestanoví	stanovit	k5eNaPmIp3nS	stanovit
funkci	funkce	k1gFnSc4	funkce
zástupce	zástupce	k1gMnSc2	zástupce
předsedy	předseda	k1gMnSc2	předseda
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tato	tento	k3xDgFnSc1	tento
možnost	možnost	k1gFnSc1	možnost
byla	být	k5eAaImAgFnS	být
občas	občas	k6eAd1	občas
praktikovaná	praktikovaný	k2eAgFnSc1d1	praktikovaná
<g/>
.	.	kIx.	.
</s>
<s>
Zákonodárný	zákonodárný	k2eAgInSc1d1	zákonodárný
sbor	sbor	k1gInSc1	sbor
Indie	Indie	k1gFnSc2	Indie
je	být	k5eAaImIp3nS	být
dvoukomorový	dvoukomorový	k2eAgInSc1d1	dvoukomorový
parlament	parlament	k1gInSc1	parlament
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
horní	horní	k2eAgFnSc2d1	horní
komory	komora	k1gFnSc2	komora
zvané	zvaný	k2eAgFnSc2d1	zvaná
Rádžja	Rádžja	k1gFnSc1	Rádžja
Sabhá	Sabhý	k2eAgFnSc1d1	Sabhá
(	(	kIx(	(
<g/>
Rada	rada	k1gFnSc1	rada
států	stát	k1gInPc2	stát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
dolní	dolní	k2eAgFnSc2d1	dolní
sněmovny	sněmovna	k1gFnSc2	sněmovna
zvané	zvaný	k2eAgFnSc2d1	zvaná
Lók	Lók	k1gFnSc2	Lók
Sabhá	Sabhý	k2eAgFnSc1d1	Sabhá
(	(	kIx(	(
<g/>
Sněmovna	sněmovna	k1gFnSc1	sněmovna
lidu	lid	k1gInSc2	lid
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rádžja	Rádžja	k6eAd1	Rádžja
Sabhá	Sabhý	k2eAgFnSc1d1	Sabhá
má	mít	k5eAaImIp3nS	mít
245	[number]	k4	245
člennů	členn	k1gInPc2	členn
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
volena	volit	k5eAaImNgFnS	volit
nepřímo	přímo	k6eNd1	přímo
voliči	volič	k1gMnPc1	volič
na	na	k7c4	na
funkční	funkční	k2eAgNnSc4d1	funkční
období	období	k1gNnSc4	období
šesti	šest	k4xCc2	šest
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Lók	Lók	k?	Lók
Sabhá	Sabhý	k2eAgFnSc1d1	Sabhá
má	mít	k5eAaImIp3nS	mít
545	[number]	k4	545
členů	člen	k1gMnPc2	člen
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
volena	volit	k5eAaImNgFnS	volit
přímo	přímo	k6eAd1	přímo
lidovým	lidový	k2eAgNnSc7d1	lidové
hlasováním	hlasování	k1gNnSc7	hlasování
na	na	k7c4	na
pětileté	pětiletý	k2eAgNnSc4d1	pětileté
období	období	k1gNnSc4	období
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
ustavujícím	ustavující	k2eAgInSc6d1	ustavující
složkou	složka	k1gFnSc7	složka
politické	politický	k2eAgFnSc2d1	politická
síly	síla	k1gFnSc2	síla
a	a	k8xC	a
vládního	vládní	k2eAgNnSc2d1	vládní
seskupení	seskupení	k1gNnSc2	seskupení
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
indičtí	indický	k2eAgMnPc1d1	indický
obyvatelé	obyvatel	k1gMnPc1	obyvatel
nad	nad	k7c4	nad
18	[number]	k4	18
let	léto	k1gNnPc2	léto
mají	mít	k5eAaImIp3nP	mít
právo	právo	k1gNnSc4	právo
volit	volit	k5eAaImF	volit
<g/>
.	.	kIx.	.
</s>
<s>
Premiér	premiér	k1gMnSc1	premiér
předsedá	předsedat	k5eAaImIp3nS	předsedat
Radě	rada	k1gFnSc3	rada
ministrů	ministr	k1gMnPc2	ministr
<g/>
.	.	kIx.	.
</s>
<s>
Exekutiva	exekutiva	k1gFnSc1	exekutiva
je	být	k5eAaImIp3nS	být
zastoupena	zastoupit	k5eAaPmNgFnS	zastoupit
prezidentem	prezident	k1gMnSc7	prezident
<g/>
,	,	kIx,	,
viceprezidentem	viceprezident	k1gMnSc7	viceprezident
a	a	k8xC	a
ministerským	ministerský	k2eAgInSc7d1	ministerský
kabinetem	kabinet	k1gInSc7	kabinet
<g/>
,	,	kIx,	,
vedeným	vedený	k2eAgMnSc7d1	vedený
předsedou	předseda	k1gMnSc7	předseda
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
ministr	ministr	k1gMnSc1	ministr
spravující	spravující	k2eAgInSc1d1	spravující
ministerský	ministerský	k2eAgInSc1d1	ministerský
úřad	úřad	k1gInSc1	úřad
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
členem	člen	k1gMnSc7	člen
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
komor	komora	k1gFnPc2	komora
parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
indickém	indický	k2eAgInSc6d1	indický
parlamentním	parlamentní	k2eAgInSc6d1	parlamentní
systému	systém	k1gInSc6	systém
je	být	k5eAaImIp3nS	být
výkonná	výkonný	k2eAgFnSc1d1	výkonná
moc	moc	k1gFnSc1	moc
podřízená	podřízený	k2eAgFnSc1d1	podřízená
zákonodárné	zákonodárný	k2eAgFnSc3d1	zákonodárná
<g/>
.	.	kIx.	.
</s>
<s>
Nezávislá	závislý	k2eNgFnSc1d1	nezávislá
soudní	soudní	k2eAgFnSc1d1	soudní
moc	moc	k1gFnSc1	moc
Indie	Indie	k1gFnSc2	Indie
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
předsedou	předseda	k1gMnSc7	předseda
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc4	dva
<g/>
,	,	kIx,	,
prvostupňovou	prvostupňový	k2eAgFnSc4d1	prvostupňová
jurisdikci	jurisdikce	k1gFnSc4	jurisdikce
nad	nad	k7c7	nad
spory	spor	k1gInPc7	spor
mezi	mezi	k7c4	mezi
státy	stát	k1gInPc4	stát
a	a	k8xC	a
odvolací	odvolací	k2eAgFnSc4d1	odvolací
jurisdikci	jurisdikce	k1gFnSc4	jurisdikce
nad	nad	k7c7	nad
vrchními	vrchní	k2eAgInPc7d1	vrchní
soudy	soud	k1gInPc7	soud
<g/>
.	.	kIx.	.
</s>
<s>
Indie	Indie	k1gFnSc1	Indie
má	mít	k5eAaImIp3nS	mít
18	[number]	k4	18
<g/>
.	.	kIx.	.
odvolacích	odvolací	k2eAgInPc2d1	odvolací
vrchních	vrchní	k2eAgInPc2d1	vrchní
soudů	soud	k1gInPc2	soud
a	a	k8xC	a
každý	každý	k3xTgMnSc1	každý
má	mít	k5eAaImIp3nS	mít
jurisdikci	jurisdikce	k1gFnSc4	jurisdikce
nad	nad	k7c7	nad
svazovým	svazový	k2eAgInSc7d1	svazový
státem	stát	k1gInSc7	stát
nebo	nebo	k8xC	nebo
skupinou	skupina	k1gFnSc7	skupina
menších	malý	k2eAgInPc2d2	menší
svazových	svazový	k2eAgInPc2d1	svazový
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
států	stát	k1gInPc2	stát
má	mít	k5eAaImIp3nS	mít
odstupňovaný	odstupňovaný	k2eAgInSc1d1	odstupňovaný
systém	systém	k1gInSc1	systém
nižších	nízký	k2eAgInPc2d2	nižší
soudů	soud	k1gInPc2	soud
<g/>
.	.	kIx.	.
</s>
<s>
Případný	případný	k2eAgInSc1d1	případný
konflikt	konflikt	k1gInSc1	konflikt
mezi	mezi	k7c7	mezi
legislativou	legislativa	k1gFnSc7	legislativa
a	a	k8xC	a
justicí	justice	k1gFnSc7	justice
je	být	k5eAaImIp3nS	být
přenechán	přenechat	k5eAaPmNgInS	přenechat
prezidentovi	prezident	k1gMnSc3	prezident
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
země	zem	k1gFnSc2	zem
stojí	stát	k5eAaImIp3nS	stát
strana	strana	k1gFnSc1	strana
Indická	indický	k2eAgFnSc1d1	indická
národní	národní	k2eAgFnSc1d1	národní
kongres	kongres	k1gInSc1	kongres
(	(	kIx(	(
<g/>
INC	INC	kA	INC
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
si	se	k3xPyFc3	se
udržuje	udržovat	k5eAaImIp3nS	udržovat
svůj	svůj	k3xOyFgInSc4	svůj
vliv	vliv	k1gInSc4	vliv
už	už	k6eAd1	už
od	od	k7c2	od
vzniku	vznik	k1gInSc2	vznik
nezávislé	závislý	k2eNgFnSc2d1	nezávislá
Indie	Indie	k1gFnSc2	Indie
<g/>
.	.	kIx.	.
</s>
<s>
INC	INC	kA	INC
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
vedená	vedený	k2eAgFnSc1d1	vedená
členem	člen	k1gInSc7	člen
rodiny	rodina	k1gFnSc2	rodina
Nehrú	Nehrú	k1gFnSc1	Nehrú
-	-	kIx~	-
Gándhí	Gándhí	k1gFnSc1	Gándhí
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
těšila	těšit	k5eAaImAgFnS	těšit
parlamentní	parlamentní	k2eAgFnSc1d1	parlamentní
většině	většina	k1gFnSc3	většina
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
krátkých	krátký	k2eAgNnPc2d1	krátké
období	období	k1gNnPc2	období
během	během	k7c2	během
70	[number]	k4	70
<g/>
.	.	kIx.	.
a	a	k8xC	a
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1996	[number]	k4	1996
a	a	k8xC	a
1998	[number]	k4	1998
nastalo	nastat	k5eAaPmAgNnS	nastat
období	období	k1gNnSc1	období
politické	politický	k2eAgFnSc2d1	politická
nestability	nestabilita	k1gFnSc2	nestabilita
a	a	k8xC	a
vláda	vláda	k1gFnSc1	vláda
byla	být	k5eAaImAgFnS	být
nejprve	nejprve	k6eAd1	nejprve
sestavena	sestavit	k5eAaPmNgFnS	sestavit
pravicovou	pravicový	k2eAgFnSc7d1	pravicová
nacionalistickou	nacionalistický	k2eAgFnSc7d1	nacionalistická
stranou	strana	k1gFnSc7	strana
Bháratíja	Bháratíj	k1gInSc2	Bháratíj
džantá	džantat	k5eAaPmIp3nS	džantat
(	(	kIx(	(
<g/>
BJ	BJ	kA	BJ
<g/>
)	)	kIx)	)
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
levicovou	levicový	k2eAgFnSc7d1	levicová
koalicí	koalice	k1gFnSc7	koalice
United	United	k1gMnSc1	United
Front	front	k1gInSc1	front
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
BJ	BJ	kA	BJ
s	s	k7c7	s
menšími	malý	k2eAgFnPc7d2	menší
regionálními	regionální	k2eAgFnPc7d1	regionální
stranami	strana	k1gFnPc7	strana
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
Národně	národně	k6eAd1	národně
demokratickou	demokratický	k2eAgFnSc4d1	demokratická
alianci	aliance	k1gFnSc4	aliance
(	(	kIx(	(
<g/>
NDA	NDA	kA	NDA
<g/>
)	)	kIx)	)
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	s	k7c7	s
první	první	k4xOgFnSc7	první
nekongresovou	kongresový	k2eNgFnSc7d1	kongresový
stranou	strana	k1gFnSc7	strana
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
dokončila	dokončit	k5eAaPmAgFnS	dokončit
celé	celý	k2eAgNnSc4d1	celé
pětileté	pětiletý	k2eAgNnSc4d1	pětileté
volební	volební	k2eAgNnSc4d1	volební
období	období	k1gNnSc4	období
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
Národní	národní	k2eAgFnSc1d1	národní
kongresová	kongresový	k2eAgFnSc1d1	kongresová
strana	strana	k1gFnSc1	strana
a	a	k8xC	a
získala	získat	k5eAaPmAgFnS	získat
dostatečný	dostatečný	k2eAgInSc4d1	dostatečný
počet	počet	k1gInSc4	počet
křesel	křeslo	k1gNnPc2	křeslo
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohla	moct	k5eAaImAgFnS	moct
vytvořit	vytvořit	k5eAaPmF	vytvořit
koalici	koalice	k1gFnSc4	koalice
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
levicových	levicový	k2eAgFnPc2d1	levicová
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Indie	Indie	k1gFnSc1	Indie
si	se	k3xPyFc3	se
udržuje	udržovat	k5eAaImIp3nS	udržovat
přátelské	přátelský	k2eAgInPc4d1	přátelský
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
většinou	většina	k1gFnSc7	většina
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
následovaly	následovat	k5eAaImAgInP	následovat
po	po	k7c6	po
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
poznamenány	poznamenat	k5eAaPmNgInP	poznamenat
neustálými	neustálý	k2eAgInPc7d1	neustálý
konflikty	konflikt	k1gInPc7	konflikt
s	s	k7c7	s
Pákistánem	Pákistán	k1gInSc7	Pákistán
a	a	k8xC	a
Čínou	Čína	k1gFnSc7	Čína
o	o	k7c6	o
území	území	k1gNnSc6	území
Kašmíru	Kašmír	k1gInSc2	Kašmír
a	a	k8xC	a
Arunáčalpradéše	Arunáčalpradéše	k1gFnSc2	Arunáčalpradéše
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
Indie	Indie	k1gFnSc1	Indie
pokusila	pokusit	k5eAaPmAgFnS	pokusit
udržet	udržet	k5eAaPmF	udržet
si	se	k3xPyFc3	se
svou	svůj	k3xOyFgFnSc4	svůj
neutralitu	neutralita	k1gFnSc4	neutralita
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
zakládajících	zakládající	k2eAgInPc2d1	zakládající
členů	člen	k1gInPc2	člen
Hnutí	hnutí	k1gNnSc2	hnutí
nezúčastněných	zúčastněný	k2eNgFnPc2d1	nezúčastněná
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
čínsko-indické	čínskondický	k2eAgFnSc6d1	čínsko-indický
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
se	se	k3xPyFc4	se
zlepšily	zlepšit	k5eAaPmAgInP	zlepšit
vzájemné	vzájemný	k2eAgInPc1d1	vzájemný
vztahy	vztah	k1gInPc1	vztah
Indie	Indie	k1gFnSc2	Indie
se	s	k7c7	s
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ale	ale	k9	ale
narušilo	narušit	k5eAaPmAgNnS	narušit
vztahy	vztah	k1gInPc4	vztah
se	s	k7c7	s
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Stav	stav	k1gInSc1	stav
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
ustálil	ustálit	k5eAaPmAgMnS	ustálit
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
přátelské	přátelský	k2eAgFnSc2d1	přátelská
nabídky	nabídka	k1gFnSc2	nabídka
předkládané	předkládaný	k2eAgNnSc1d1	předkládané
Indickou	indický	k2eAgFnSc7d1	indická
vládou	vláda	k1gFnSc7	vláda
posílily	posílit	k5eAaPmAgFnP	posílit
vztahy	vztah	k1gInPc1	vztah
se	s	k7c7	s
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
<g/>
,	,	kIx,	,
Čínou	Čína	k1gFnSc7	Čína
a	a	k8xC	a
Pákistánem	Pákistán	k1gInSc7	Pákistán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ekonomické	ekonomický	k2eAgFnSc6d1	ekonomická
sféře	sféra	k1gFnSc6	sféra
má	mít	k5eAaImIp3nS	mít
Indie	Indie	k1gFnSc2	Indie
blízký	blízký	k2eAgInSc1d1	blízký
vztah	vztah	k1gInSc1	vztah
s	s	k7c7	s
jinými	jiný	k2eAgFnPc7d1	jiná
rozvojovými	rozvojový	k2eAgFnPc7d1	rozvojová
zeměmi	zem	k1gFnPc7	zem
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
s	s	k7c7	s
Brazílií	Brazílie	k1gFnSc7	Brazílie
a	a	k8xC	a
Mexikem	Mexiko	k1gNnSc7	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
Indie	Indie	k1gFnSc1	Indie
je	být	k5eAaImIp3nS	být
také	také	k9	také
zakládající	zakládající	k2eAgMnSc1d1	zakládající
člen	člen	k1gMnSc1	člen
OSN	OSN	kA	OSN
a	a	k8xC	a
do	do	k7c2	do
velké	velký	k2eAgFnSc2d1	velká
míry	míra	k1gFnSc2	míra
přispěla	přispět	k5eAaPmAgFnS	přispět
k	k	k7c3	k
úspěchu	úspěch	k1gInSc3	úspěch
operací	operace	k1gFnPc2	operace
OSN	OSN	kA	OSN
probíhajících	probíhající	k2eAgFnPc2d1	probíhající
na	na	k7c6	na
čtyřech	čtyři	k4xCgInPc6	čtyři
kontinentech	kontinent	k1gInPc6	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
55	[number]	k4	55
000	[number]	k4	000
indických	indický	k2eAgMnPc2d1	indický
příslušníků	příslušník	k1gMnPc2	příslušník
bojových	bojový	k2eAgFnPc2d1	bojová
a	a	k8xC	a
policejních	policejní	k2eAgFnPc2d1	policejní
sil	síla	k1gFnPc2	síla
sloužilo	sloužit	k5eAaImAgNnS	sloužit
ve	v	k7c6	v
35	[number]	k4	35
mírových	mírový	k2eAgFnPc6d1	mírová
misích	mise	k1gFnPc6	mise
Spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Indie	Indie	k1gFnSc1	Indie
je	být	k5eAaImIp3nS	být
také	také	k6eAd1	také
součástí	součást	k1gFnSc7	součást
BRICS	BRICS	kA	BRICS
<g/>
.	.	kIx.	.
</s>
<s>
Indie	Indie	k1gFnSc1	Indie
si	se	k3xPyFc3	se
udržuje	udržovat	k5eAaImIp3nS	udržovat
třetí	třetí	k4xOgFnPc4	třetí
největší	veliký	k2eAgFnPc4d3	veliký
ozbrojené	ozbrojený	k2eAgFnPc4d1	ozbrojená
síly	síla	k1gFnPc4	síla
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Skládají	skládat	k5eAaImIp3nP	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
letectva	letectvo	k1gNnSc2	letectvo
<g/>
,	,	kIx,	,
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
pomocných	pomocný	k2eAgFnPc2d1	pomocná
složek	složka	k1gFnPc2	složka
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgMnSc7d3	nejvyšší
velitelem	velitel	k1gMnSc7	velitel
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
je	být	k5eAaImIp3nS	být
indický	indický	k2eAgMnSc1d1	indický
prezident	prezident	k1gMnSc1	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
hlavní	hlavní	k2eAgMnPc1d1	hlavní
indičtí	indický	k2eAgMnPc1d1	indický
rivalové	rival	k1gMnPc1	rival
Čína	Čína	k1gFnSc1	Čína
a	a	k8xC	a
Pákistán	Pákistán	k1gInSc1	Pákistán
vlastní	vlastní	k2eAgFnSc2d1	vlastní
jaderné	jaderný	k2eAgFnSc2d1	jaderná
zbraně	zbraň	k1gFnSc2	zbraň
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc4	tento
typ	typ	k1gInSc4	typ
zbraní	zbraň	k1gFnPc2	zbraň
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
také	také	k9	také
Indie	Indie	k1gFnSc1	Indie
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
jadernou	jaderný	k2eAgFnSc4d1	jaderná
zkoušku	zkouška	k1gFnSc4	zkouška
provedla	provést	k5eAaPmAgFnS	provést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastní	k2eAgFnPc1d1	vlastní
balistické	balistický	k2eAgFnPc1d1	balistická
rakety	raketa	k1gFnPc1	raketa
nesoucí	nesoucí	k2eAgFnSc2d1	nesoucí
jaderné	jaderný	k2eAgFnSc2d1	jaderná
hlavice	hlavice	k1gFnSc2	hlavice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
se	se	k3xPyFc4	se
Indie	Indie	k1gFnSc1	Indie
stala	stát	k5eAaPmAgFnS	stát
největším	veliký	k2eAgMnSc7d3	veliký
světovým	světový	k2eAgMnSc7d1	světový
importérem	importér	k1gMnSc7	importér
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Dovezla	dovézt	k5eAaPmAgFnS	dovézt
přitom	přitom	k6eAd1	přitom
zbraně	zbraň	k1gFnPc1	zbraň
za	za	k7c4	za
3,3	[number]	k4	3,3
miliardy	miliarda	k4xCgFnSc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgMnSc7d1	hlavní
zahraničním	zahraniční	k2eAgMnSc7d1	zahraniční
dodavatelem	dodavatel	k1gMnSc7	dodavatel
je	být	k5eAaImIp3nS	být
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
kterým	který	k3yRgInSc7	který
země	země	k1gFnSc1	země
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
na	na	k7c6	na
řadě	řada	k1gFnSc6	řada
zbrojních	zbrojní	k2eAgInPc2d1	zbrojní
programů	program	k1gInPc2	program
(	(	kIx(	(
<g/>
např.	např.	kA	např.
stíhací	stíhací	k2eAgInPc1d1	stíhací
letouny	letoun	k1gInPc1	letoun
Su-	Su-	k1gFnSc1	Su-
<g/>
30	[number]	k4	30
<g/>
MKI	MKI	kA	MKI
<g/>
,	,	kIx,	,
protilodní	protilodní	k2eAgFnPc4d1	protilodní
střely	střela	k1gFnPc4	střela
BrahMos	BrahMos	k1gInSc1	BrahMos
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgFnPc1d1	moderní
zbraně	zbraň	k1gFnPc1	zbraň
vyvíjí	vyvíjet	k5eAaImIp3nP	vyvíjet
rovněž	rovněž	k9	rovněž
domácí	domácí	k2eAgFnPc1d1	domácí
společnosti	společnost	k1gFnPc1	společnost
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Defence	Defence	k1gFnSc1	Defence
Research	Research	k1gInSc1	Research
and	and	k?	and
Development	Development	k1gInSc1	Development
Organisation	Organisation	k1gInSc1	Organisation
(	(	kIx(	(
<g/>
DRDO	Drda	k1gMnSc5	Drda
<g/>
)	)	kIx)	)
a	a	k8xC	a
Hindustan	Hindustan	k1gInSc1	Hindustan
Aeronautics	Aeronauticsa	k1gFnPc2	Aeronauticsa
(	(	kIx(	(
<g/>
HAL	hala	k1gFnPc2	hala
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
tím	ten	k3xDgNnSc7	ten
postupně	postupně	k6eAd1	postupně
snižují	snižovat	k5eAaImIp3nP	snižovat
závislost	závislost	k1gFnSc4	závislost
země	zem	k1gFnSc2	zem
na	na	k7c6	na
dodávkách	dodávka	k1gFnPc6	dodávka
ze	z	k7c2	z
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Domácí	domácí	k2eAgInPc1d1	domácí
zbraňové	zbraňový	k2eAgInPc1d1	zbraňový
systémy	systém	k1gInPc1	systém
zastupují	zastupovat	k5eAaImIp3nP	zastupovat
například	například	k6eAd1	například
stíhací	stíhací	k2eAgInPc1d1	stíhací
letouny	letoun	k1gInPc1	letoun
HAL	hala	k1gFnPc2	hala
Tejas	Tejasa	k1gFnPc2	Tejasa
<g/>
,	,	kIx,	,
vrtulníky	vrtulník	k1gInPc1	vrtulník
HAL	hala	k1gFnPc2	hala
Dhruv	Dhruva	k1gFnPc2	Dhruva
<g/>
,	,	kIx,	,
balistické	balistický	k2eAgFnPc1d1	balistická
rakety	raketa	k1gFnPc1	raketa
Agni	Agn	k1gFnPc1	Agn
<g/>
,	,	kIx,	,
tanky	tank	k1gInPc1	tank
Arjun	Arjuna	k1gFnPc2	Arjuna
či	či	k8xC	či
torpédoborce	torpédoborec	k1gInSc2	torpédoborec
třídy	třída	k1gFnSc2	třída
Delhi	Delh	k1gFnSc2	Delh
<g/>
.	.	kIx.	.
</s>
<s>
Vyvíjí	vyvíjet	k5eAaImIp3nP	vyvíjet
například	například	k6eAd1	například
dvě	dva	k4xCgFnPc1	dva
letadlové	letadlový	k2eAgFnPc1d1	letadlová
lodě	loď	k1gFnPc1	loď
třídy	třída	k1gFnSc2	třída
Vikrant	Vikrant	k1gMnSc1	Vikrant
a	a	k8xC	a
jaderné	jaderný	k2eAgFnPc1d1	jaderná
raketonosné	raketonosný	k2eAgFnPc1d1	raketonosná
ponorky	ponorka	k1gFnPc1	ponorka
třídy	třída	k1gFnSc2	třída
Arihant	Arihanta	k1gFnPc2	Arihanta
<g/>
.	.	kIx.	.
</s>
<s>
Vizte	vidět	k5eAaImRp2nP	vidět
též	též	k9	též
hlavní	hlavní	k2eAgInSc4d1	hlavní
články	článek	k1gInPc4	článek
<g/>
:	:	kIx,	:
Indické	indický	k2eAgInPc4d1	indický
státy	stát	k1gInPc4	stát
a	a	k8xC	a
teritoria	teritorium	k1gNnPc4	teritorium
Seznam	seznam	k1gInSc4	seznam
indických	indický	k2eAgInPc2d1	indický
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
vícejazyčně	vícejazyčně	k6eAd1	vícejazyčně
<g/>
)	)	kIx)	)
–	–	k?	–
indické	indický	k2eAgInPc4d1	indický
spolkové	spolkový	k2eAgInPc4d1	spolkový
státy	stát	k1gInPc4	stát
a	a	k8xC	a
Svazová	svazový	k2eAgNnPc4d1	svazové
teritoria	teritorium	k1gNnPc4	teritorium
Seznam	seznam	k1gInSc1	seznam
indických	indický	k2eAgInPc2d1	indický
států	stát	k1gInPc2	stát
–	–	k?	–
indické	indický	k2eAgInPc4d1	indický
spolkové	spolkový	k2eAgInPc4d1	spolkový
státy	stát	k1gInPc4	stát
a	a	k8xC	a
svazová	svazový	k2eAgNnPc4d1	svazové
teritoria	teritorium	k1gNnPc4	teritorium
s	s	k7c7	s
údaji	údaj	k1gInPc7	údaj
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
<g/>
,	,	kIx,	,
počtu	počet	k1gInSc6	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
hustotě	hustota	k1gFnSc6	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
Indická	indický	k2eAgFnSc1d1	indická
republika	republika	k1gFnSc1	republika
je	být	k5eAaImIp3nS	být
rozdělena	rozdělen	k2eAgNnPc4d1	rozděleno
<g />
.	.	kIx.	.
</s>
<s>
na	na	k7c4	na
29	[number]	k4	29
spolkových	spolkový	k2eAgInPc2d1	spolkový
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
state	status	k1gInSc5	status
<g/>
,	,	kIx,	,
pl.	pl.	k?	pl.
states	states	k1gInSc1	states
<g/>
,	,	kIx,	,
hindsky	hindsky	k6eAd1	hindsky
प	प	k?	प
<g/>
्	्	k?	्
<g/>
र	र	k?	र
<g/>
ा	ा	k?	ा
<g/>
ं	ं	k?	ं
<g/>
त	त	k?	त
/	/	kIx~	/
prā	prā	k?	prā
<g/>
)	)	kIx)	)
s	s	k7c7	s
vlastní	vlastní	k2eAgFnSc7d1	vlastní
volenou	volený	k2eAgFnSc7d1	volená
vládou	vláda	k1gFnSc7	vláda
<g/>
,	,	kIx,	,
7	[number]	k4	7
Svazových	svazový	k2eAgNnPc2d1	svazové
teritorií	teritorium	k1gNnPc2	teritorium
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
union	union	k1gInSc4	union
territory	territor	k1gInPc7	territor
<g/>
,	,	kIx,	,
pl.	pl.	k?	pl.
union	union	k1gInSc1	union
territories	territories	k1gInSc1	territories
<g/>
,	,	kIx,	,
hindsky	hindsky	k6eAd1	hindsky
क	क	k?	क
<g/>
े	े	k?	े
<g/>
न	न	k?	न
<g/>
्	्	k?	्
<g/>
द	द	k?	द
<g/>
्	्	k?	्
<g/>
र	र	k?	र
<g/>
ी	ी	k?	ी
<g/>
य	य	k?	य
स	स	k?	स
<g/>
ा	ा	k?	ा
<g/>
र	र	k?	र
/	/	kIx~	/
kendrī	kendrī	k?	kendrī
sarkā	sarkā	k?	sarkā
<g/>
)	)	kIx)	)
včetně	včetně	k7c2	včetně
území	území	k1gNnSc2	území
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
593	[number]	k4	593
okresů	okres	k1gInPc2	okres
(	(	kIx(	(
<g/>
district	district	k1gMnSc1	district
<g/>
,	,	kIx,	,
pl.	pl.	k?	pl.
districts	districts	k1gInSc1	districts
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Svazová	svazový	k2eAgNnPc4d1	svazové
teritoria	teritorium	k1gNnPc4	teritorium
Související	související	k2eAgFnPc4d1	související
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Geografie	geografie	k1gFnSc2	geografie
Indie	Indie	k1gFnSc2	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Indie	Indie	k1gFnSc1	Indie
<g/>
,	,	kIx,	,
zabírající	zabírající	k2eAgFnSc4d1	zabírající
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
indického	indický	k2eAgInSc2d1	indický
subkontinentu	subkontinent	k1gInSc2	subkontinent
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Asie	Asie	k1gFnSc2	Asie
od	od	k7c2	od
8	[number]	k4	8
<g/>
°	°	k?	°
až	až	k6eAd1	až
k	k	k7c3	k
37	[number]	k4	37
<g/>
°	°	k?	°
severní	severní	k2eAgFnSc2d1	severní
šířky	šířka	k1gFnSc2	šířka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
svých	svůj	k3xOyFgMnPc2	svůj
sousedů	soused	k1gMnPc2	soused
dělena	dělit	k5eAaImNgFnS	dělit
Himálajským	himálajský	k2eAgNnSc7d1	himálajské
pohořím	pohoří	k1gNnSc7	pohoří
<g/>
;	;	kIx,	;
indický	indický	k2eAgInSc4d1	indický
subkontinent	subkontinent	k1gInSc4	subkontinent
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
jih	jih	k1gInSc4	jih
probíhá	probíhat	k5eAaImIp3nS	probíhat
do	do	k7c2	do
špičky	špička	k1gFnSc2	špička
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
obklopen	obklopit	k5eAaPmNgInS	obklopit
Indickým	indický	k2eAgInSc7d1	indický
oceánem	oceán	k1gInSc7	oceán
<g/>
:	:	kIx,	:
na	na	k7c6	na
západě	západ	k1gInSc6	západ
Arabským	arabský	k2eAgNnSc7d1	arabské
mořem	moře	k1gNnSc7	moře
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
Bengálským	bengálský	k2eAgInSc7d1	bengálský
zálivem	záliv	k1gInSc7	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Indií	Indie	k1gFnSc7	Indie
protékají	protékat	k5eAaImIp3nP	protékat
tři	tři	k4xCgFnPc1	tři
velké	velký	k2eAgFnPc1d1	velká
řeky	řeka	k1gFnPc1	řeka
Indus	Indus	k1gInSc1	Indus
<g/>
,	,	kIx,	,
Ganga	Ganga	k1gFnSc1	Ganga
a	a	k8xC	a
Brahmaputra	Brahmaputra	k1gFnSc1	Brahmaputra
<g/>
.	.	kIx.	.
</s>
<s>
Geologicky	geologicky	k6eAd1	geologicky
lze	lze	k6eAd1	lze
povrch	povrch	k1gInSc4	povrch
Indie	Indie	k1gFnSc2	Indie
rozdělit	rozdělit	k5eAaPmF	rozdělit
od	od	k7c2	od
jihu	jih	k1gInSc2	jih
k	k	k7c3	k
severu	sever	k1gInSc3	sever
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
hlavní	hlavní	k2eAgFnPc4d1	hlavní
části	část	k1gFnPc4	část
<g/>
:	:	kIx,	:
stará	starat	k5eAaImIp3nS	starat
centrální	centrální	k2eAgFnSc1d1	centrální
krystalická	krystalický	k2eAgFnSc1d1	krystalická
Dekánská	Dekánský	k2eAgFnSc1d1	Dekánská
plošina	plošina	k1gFnSc1	plošina
(	(	kIx(	(
<g/>
též	též	k9	též
Dakkhin	Dakkhin	k1gInSc1	Dakkhin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Indoganžská	Indoganžský	k2eAgFnSc1d1	Indoganžská
nížina	nížina	k1gFnSc1	nížina
a	a	k8xC	a
velehorská	velehorský	k2eAgFnSc1d1	velehorská
oblast	oblast	k1gFnSc1	oblast
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
.	.	kIx.	.
</s>
<s>
Dekánská	Dekánský	k2eAgFnSc1d1	Dekánská
plošina	plošina	k1gFnSc1	plošina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
rozčleněná	rozčleněný	k2eAgFnSc1d1	rozčleněná
zlomy	zlom	k1gInPc7	zlom
a	a	k8xC	a
poklesy	pokles	k1gInPc7	pokles
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
mírně	mírně	k6eAd1	mírně
svažuje	svažovat	k5eAaImIp3nS	svažovat
od	od	k7c2	od
pohoří	pohoří	k1gNnSc2	pohoří
Západní	západní	k2eAgInSc4d1	západní
Ghát	Ghát	k1gInSc4	Ghát
na	na	k7c6	na
západě	západ	k1gInSc6	západ
k	k	k7c3	k
Východnímu	východní	k2eAgInSc3d1	východní
Ghátu	Ghát	k1gInSc3	Ghát
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
Dekánskou	Dekánský	k2eAgFnSc7d1	Dekánská
plošinou	plošina	k1gFnSc7	plošina
a	a	k8xC	a
velehorami	velehora	k1gFnPc7	velehora
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
rozlehlá	rozlehlý	k2eAgFnSc1d1	rozlehlá
Indoganžská	Indoganžský	k2eAgFnSc1d1	Indoganžská
nížina	nížina	k1gFnSc1	nížina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
západě	západ	k1gInSc6	západ
od	od	k7c2	od
nížiny	nížina	k1gFnSc2	nížina
leží	ležet	k5eAaImIp3nS	ležet
stepní	stepní	k2eAgFnSc1d1	stepní
a	a	k8xC	a
pouštní	pouštní	k2eAgFnSc1d1	pouštní
oblast	oblast	k1gFnSc1	oblast
Thár	Thár	k1gInSc1	Thár
<g/>
,	,	kIx,	,
vyplněná	vyplněná	k1gFnSc1	vyplněná
koryty	koryto	k1gNnPc7	koryto
občasných	občasný	k2eAgMnPc2d1	občasný
toků	tok	k1gInPc2	tok
a	a	k8xC	a
písečnými	písečný	k2eAgInPc7d1	písečný
přesypy	přesyp	k1gInPc7	přesyp
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
a	a	k8xC	a
severu	sever	k1gInSc6	sever
státu	stát	k1gInSc2	stát
se	se	k3xPyFc4	se
vypíná	vypínat	k5eAaImIp3nS	vypínat
předhůří	předhůří	k1gNnSc1	předhůří
Siválik	Siválika	k1gFnPc2	Siválika
a	a	k8xC	a
mohutné	mohutný	k2eAgInPc4d1	mohutný
velehorské	velehorský	k2eAgInPc4d1	velehorský
masivy	masiv	k1gInPc4	masiv
Himálaje	Himálaj	k1gFnSc2	Himálaj
a	a	k8xC	a
Karákoramu	Karákoram	k1gInSc2	Karákoram
<g/>
,	,	kIx,	,
pokryté	pokrytý	k2eAgInPc1d1	pokrytý
horskými	horský	k2eAgInPc7d1	horský
ledovci	ledovec	k1gInPc7	ledovec
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
v	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
Karákoram	Karákoram	k1gInSc1	Karákoram
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
Indie	Indie	k1gFnSc1	Indie
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
světa	svět	k1gInSc2	svět
K	k	k7c3	k
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
měřící	měřící	k2eAgFnSc1d1	měřící
8611	[number]	k4	8611
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Tyto	tento	k3xDgFnPc1	tento
velehory	velehora	k1gFnPc1	velehora
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
tektonickým	tektonický	k2eAgNnSc7d1	tektonické
vyzdvižením	vyzdvižení	k1gNnSc7	vyzdvižení
povrchu	povrch	k1gInSc2	povrch
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
podsunula	podsunout	k5eAaPmAgFnS	podsunout
Indická	indický	k2eAgFnSc1d1	indická
litosférická	litosférický	k2eAgFnSc1d1	litosférická
deska	deska	k1gFnSc1	deska
pod	pod	k7c7	pod
Asijskou	asijský	k2eAgFnSc7d1	asijská
<g/>
.	.	kIx.	.
</s>
<s>
Současný	současný	k2eAgInSc1d1	současný
tvar	tvar	k1gInSc1	tvar
získaly	získat	k5eAaPmAgFnP	získat
třetihorním	třetihorní	k2eAgNnSc7d1	třetihorní
alpínsko-himálajským	alpínskoimálajský	k2eAgNnSc7d1	alpínsko-himálajský
vrásněním	vrásnění	k1gNnSc7	vrásnění
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
táhnou	táhnout	k5eAaImIp3nP	táhnout
hraniční	hraniční	k2eAgNnPc1d1	hraniční
pohoří	pohoří	k1gNnPc1	pohoří
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
Patkai	Patkae	k1gFnSc4	Patkae
Range	Range	k1gNnSc1	Range
a	a	k8xC	a
Letha	Letha	k1gFnSc1	Letha
Range	Range	k1gFnPc2	Range
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
přesahují	přesahovat	k5eAaImIp3nP	přesahovat
3800	[number]	k4	3800
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
a	a	k8xC	a
náhorní	náhorní	k2eAgFnSc1d1	náhorní
plošina	plošina	k1gFnSc1	plošina
Khasi	Khase	k1gFnSc4	Khase
Hills	Hillsa	k1gFnPc2	Hillsa
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ekonomika	ekonomika	k1gFnSc1	ekonomika
Indie	Indie	k1gFnSc1	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Indie	Indie	k1gFnSc1	Indie
má	mít	k5eAaImIp3nS	mít
velké	velký	k2eAgNnSc4d1	velké
přírodní	přírodní	k2eAgNnSc4d1	přírodní
bohatství	bohatství	k1gNnSc4	bohatství
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
úrodné	úrodný	k2eAgFnSc3d1	úrodná
půdě	půda	k1gFnSc3	půda
a	a	k8xC	a
příhodnému	příhodný	k2eAgNnSc3d1	příhodné
klimatu	klima	k1gNnSc3	klima
má	mít	k5eAaImIp3nS	mít
dobré	dobrý	k2eAgFnPc4d1	dobrá
podmínky	podmínka	k1gFnPc4	podmínka
pro	pro	k7c4	pro
pěstování	pěstování	k1gNnSc4	pěstování
velkého	velký	k2eAgNnSc2d1	velké
množství	množství	k1gNnSc2	množství
zemědělských	zemědělský	k2eAgFnPc2d1	zemědělská
plodin	plodina	k1gFnPc2	plodina
<g/>
,	,	kIx,	,
produkce	produkce	k1gFnSc2	produkce
potravin	potravina	k1gFnPc2	potravina
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
surovin	surovina	k1gFnPc2	surovina
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
země	země	k1gFnSc1	země
potýká	potýkat	k5eAaImIp3nS	potýkat
se	s	k7c7	s
značnou	značný	k2eAgFnSc7d1	značná
částí	část	k1gFnSc7	část
obyvatel	obyvatel	k1gMnPc2	obyvatel
žijících	žijící	k2eAgMnPc2d1	žijící
v	v	k7c6	v
chudobě	chudoba	k1gFnSc6	chudoba
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
velkého	velký	k2eAgNnSc2d1	velké
množství	množství	k1gNnSc2	množství
podvyživených	podvyživený	k2eAgFnPc2d1	podvyživená
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Občasné	občasný	k2eAgFnPc1d1	občasná
přírodní	přírodní	k2eAgFnPc1d1	přírodní
katastrofy	katastrofa	k1gFnPc1	katastrofa
jako	jako	k8xS	jako
krutá	krutý	k2eAgNnPc1d1	kruté
sucha	sucho	k1gNnPc1	sucho
<g/>
,	,	kIx,	,
zemětřesení	zemětřesení	k1gNnPc1	zemětřesení
a	a	k8xC	a
povodně	povodně	k6eAd1	povodně
situaci	situace	k1gFnSc4	situace
dále	daleko	k6eAd2	daleko
zhoršují	zhoršovat	k5eAaImIp3nP	zhoršovat
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgInPc4d1	hlavní
problémy	problém	k1gInPc4	problém
zemědělství	zemědělství	k1gNnSc2	zemědělství
patří	patřit	k5eAaImIp3nS	patřit
eroze	eroze	k1gFnSc1	eroze
půdy	půda	k1gFnSc2	půda
<g/>
,	,	kIx,	,
rapidní	rapidní	k2eAgNnSc1d1	rapidní
snižování	snižování	k1gNnSc1	snižování
úrovní	úroveň	k1gFnPc2	úroveň
spodních	spodní	k2eAgFnPc2d1	spodní
vod	voda	k1gFnPc2	voda
a	a	k8xC	a
nadměrné	nadměrný	k2eAgNnSc1d1	nadměrné
vyčerpávání	vyčerpávání	k1gNnSc1	vyčerpávání
živin	živina	k1gFnPc2	živina
půdy	půda	k1gFnSc2	půda
intenzifikovaným	intenzifikovaný	k2eAgNnSc7d1	intenzifikovaný
zemědělstvím	zemědělství	k1gNnSc7	zemědělství
s	s	k7c7	s
přílišným	přílišný	k2eAgNnSc7d1	přílišné
používáním	používání	k1gNnSc7	používání
chemikálií	chemikálie	k1gFnPc2	chemikálie
<g/>
.	.	kIx.	.
</s>
<s>
Rozloha	rozloha	k1gFnSc1	rozloha
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
půdy	půda	k1gFnSc2	půda
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
značná	značný	k2eAgFnSc1d1	značná
část	část	k1gFnSc1	část
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
není	být	k5eNaImIp3nS	být
přirozeně	přirozeně	k6eAd1	přirozeně
úrodná	úrodný	k2eAgFnSc1d1	úrodná
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Indie	Indie	k1gFnSc1	Indie
získala	získat	k5eAaPmAgFnS	získat
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
mnoho	mnoho	k6eAd1	mnoho
podniklo	podniknout	k5eAaPmAgNnS	podniknout
k	k	k7c3	k
posílení	posílení	k1gNnSc3	posílení
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
produkce	produkce	k1gFnSc2	produkce
<g/>
:	:	kIx,	:
především	především	k6eAd1	především
byly	být	k5eAaImAgInP	být
postaveny	postavit	k5eAaPmNgInP	postavit
různé	různý	k2eAgInPc1d1	různý
zavlažovací	zavlažovací	k2eAgInPc1d1	zavlažovací
systémy	systém	k1gInPc1	systém
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
úkolem	úkol	k1gInSc7	úkol
je	být	k5eAaImIp3nS	být
přivádět	přivádět	k5eAaImF	přivádět
vodu	voda	k1gFnSc4	voda
do	do	k7c2	do
suchých	suchý	k2eAgFnPc2d1	suchá
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Zemědělská	zemědělský	k2eAgNnPc4d1	zemědělské
hospodářství	hospodářství	k1gNnPc4	hospodářství
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
rodinná	rodinný	k2eAgFnSc1d1	rodinná
a	a	k8xC	a
tedy	tedy	k9	tedy
příliš	příliš	k6eAd1	příliš
malá	malý	k2eAgFnSc1d1	malá
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohla	moct	k5eAaImAgFnS	moct
obhospodařovat	obhospodařovat	k5eAaImF	obhospodařovat
větší	veliký	k2eAgNnSc4d2	veliký
území	území	k1gNnSc4	území
a	a	k8xC	a
využívat	využívat	k5eAaImF	využívat
moderní	moderní	k2eAgInPc4d1	moderní
stroje	stroj	k1gInPc4	stroj
a	a	k8xC	a
technologie	technologie	k1gFnPc4	technologie
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
mnoho	mnoho	k4c1	mnoho
těchto	tento	k3xDgFnPc2	tento
farem	farma	k1gFnPc2	farma
ještě	ještě	k9	ještě
zmenšuje	zmenšovat	k5eAaImIp3nS	zmenšovat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
hindské	hindský	k2eAgInPc4d1	hindský
zákony	zákon	k1gInPc4	zákon
stanoví	stanovit	k5eAaPmIp3nS	stanovit
<g/>
,	,	kIx,	,
že	že	k8xS	že
půda	půda	k1gFnSc1	půda
se	se	k3xPyFc4	se
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
rodičů	rodič	k1gMnPc2	rodič
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
rovným	rovný	k2eAgInSc7d1	rovný
dílem	díl	k1gInSc7	díl
mezi	mezi	k7c4	mezi
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
Indové	Ind	k1gMnPc1	Ind
chtějí	chtít	k5eAaImIp3nP	chtít
změnu	změna	k1gFnSc4	změna
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
statky	statek	k1gInPc1	statek
mohly	moct	k5eAaImAgInP	moct
být	být	k5eAaImF	být
větší	veliký	k2eAgInPc1d2	veliký
a	a	k8xC	a
produktivnější	produktivní	k2eAgInPc1d2	produktivnější
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgFnPc4d3	nejvýznamnější
pěstované	pěstovaný	k2eAgFnPc4d1	pěstovaná
plodiny	plodina	k1gFnPc4	plodina
patří	patřit	k5eAaImIp3nS	patřit
rýže	rýže	k1gFnSc1	rýže
<g/>
,	,	kIx,	,
bavlna	bavlna	k1gFnSc1	bavlna
<g/>
,	,	kIx,	,
juta	juta	k1gFnSc1	juta
<g/>
,	,	kIx,	,
moruše	moruše	k1gFnSc1	moruše
<g/>
,	,	kIx,	,
obilniny	obilnina	k1gFnPc1	obilnina
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
pšenice	pšenice	k1gFnSc2	pšenice
a	a	k8xC	a
proso	proso	k1gNnSc4	proso
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
subtropické	subtropický	k2eAgNnSc4d1	subtropické
a	a	k8xC	a
tropické	tropický	k2eAgNnSc4d1	tropické
ovoce	ovoce	k1gNnSc4	ovoce
(	(	kIx(	(
<g/>
banány	banán	k1gInPc1	banán
<g/>
,	,	kIx,	,
citrusy	citrus	k1gInPc1	citrus
<g/>
,	,	kIx,	,
mango	mango	k1gNnSc1	mango
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
pěstují	pěstovat	k5eAaImIp3nP	pěstovat
fazole	fazole	k1gFnPc1	fazole
<g/>
,	,	kIx,	,
ořechy	ořech	k1gInPc1	ořech
<g/>
,	,	kIx,	,
čajovník	čajovník	k1gInSc1	čajovník
<g/>
,	,	kIx,	,
sezamová	sezamový	k2eAgNnPc1d1	sezamové
semínka	semínko	k1gNnPc1	semínko
a	a	k8xC	a
betelové	betelový	k2eAgInPc1d1	betelový
ořechy	ořech	k1gInPc1	ořech
<g/>
.	.	kIx.	.
62	[number]	k4	62
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nS	živit
zemědělstvím	zemědělství	k1gNnSc7	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
Indická	indický	k2eAgFnSc1d1	indická
vláda	vláda	k1gFnSc1	vláda
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
o	o	k7c4	o
rozvoj	rozvoj	k1gInSc4	rozvoj
zaostalých	zaostalý	k2eAgFnPc2d1	zaostalá
oblastí	oblast	k1gFnPc2	oblast
země	zem	k1gFnSc2	zem
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
přenáší	přenášet	k5eAaImIp3nS	přenášet
továrny	továrna	k1gFnPc4	továrna
do	do	k7c2	do
méně	málo	k6eAd2	málo
obydlených	obydlený	k2eAgFnPc2d1	obydlená
oblastí	oblast	k1gFnPc2	oblast
Indie	Indie	k1gFnSc2	Indie
s	s	k7c7	s
úmyslem	úmysl	k1gInSc7	úmysl
podpořit	podpořit	k5eAaPmF	podpořit
tam	tam	k6eAd1	tam
podnikatelskou	podnikatelský	k2eAgFnSc4d1	podnikatelská
infrastrukturu	infrastruktura	k1gFnSc4	infrastruktura
a	a	k8xC	a
zvýšit	zvýšit	k5eAaPmF	zvýšit
počet	počet	k1gInSc4	počet
pracovních	pracovní	k2eAgNnPc2d1	pracovní
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Úrodu	úroda	k1gFnSc4	úroda
měly	mít	k5eAaImAgFnP	mít
zvýšit	zvýšit	k5eAaPmF	zvýšit
i	i	k9	i
experimenty	experiment	k1gInPc1	experiment
s	s	k7c7	s
novými	nový	k2eAgMnPc7d1	nový
geneticky	geneticky	k6eAd1	geneticky
modifikovanými	modifikovaný	k2eAgNnPc7d1	modifikované
semeny	semeno	k1gNnPc7	semeno
a	a	k8xC	a
hnojivy	hnojivo	k1gNnPc7	hnojivo
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
tyto	tento	k3xDgInPc1	tento
pokusy	pokus	k1gInPc1	pokus
nejen	nejen	k6eAd1	nejen
že	že	k8xS	že
nezvýšily	zvýšit	k5eNaPmAgInP	zvýšit
zemědělskou	zemědělský	k2eAgFnSc4d1	zemědělská
produkci	produkce	k1gFnSc4	produkce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
podryly	podrýt	k5eAaPmAgFnP	podrýt
tradiční	tradiční	k2eAgFnPc1d1	tradiční
trvale	trvale	k6eAd1	trvale
udržitelné	udržitelný	k2eAgNnSc1d1	udržitelné
indické	indický	k2eAgNnSc1d1	indické
zemědělství	zemědělství	k1gNnSc1	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
dostala	dostat	k5eAaPmAgFnS	dostat
nadnárodní	nadnárodní	k2eAgFnSc1d1	nadnárodní
korporace	korporace	k1gFnSc1	korporace
Monsanto	Monsanta	k1gFnSc5	Monsanta
povolení	povolení	k1gNnPc4	povolení
uvést	uvést	k5eAaPmF	uvést
do	do	k7c2	do
Indie	Indie	k1gFnSc2	Indie
bavlnu	bavlna	k1gFnSc4	bavlna
(	(	kIx(	(
<g/>
BT	BT	kA	BT
cotton	cotton	k1gInSc1	cotton
<g/>
)	)	kIx)	)
geneticky	geneticky	k6eAd1	geneticky
modifikovanou	modifikovaný	k2eAgFnSc4d1	modifikovaná
o	o	k7c6	o
bakterii	bakterie	k1gFnSc6	bakterie
ničící	ničící	k2eAgMnPc4d1	ničící
parazity	parazit	k1gMnPc4	parazit
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
bavlně	bavlna	k1gFnSc6	bavlna
přirozeně	přirozeně	k6eAd1	přirozeně
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
Produkce	produkce	k1gFnSc1	produkce
bavlny	bavlna	k1gFnSc2	bavlna
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
významně	významně	k6eAd1	významně
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mnoho	mnoho	k4c1	mnoho
indických	indický	k2eAgMnPc2d1	indický
rolníků	rolník	k1gMnPc2	rolník
se	se	k3xPyFc4	se
záhy	záhy	k6eAd1	záhy
ocitlo	ocitnout	k5eAaPmAgNnS	ocitnout
v	v	k7c6	v
bezvýchodné	bezvýchodný	k2eAgFnSc6d1	bezvýchodná
situaci	situace	k1gFnSc6	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jsou	být	k5eAaImIp3nP	být
nuceni	nutit	k5eAaImNgMnP	nutit
vynakládat	vynakládat	k5eAaImF	vynakládat
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
z	z	k7c2	z
klesajících	klesající	k2eAgInPc2d1	klesající
výnosů	výnos	k1gInPc2	výnos
do	do	k7c2	do
hnojiv	hnojivo	k1gNnPc2	hnojivo
<g/>
,	,	kIx,	,
pesticidů	pesticid	k1gInPc2	pesticid
a	a	k8xC	a
insekticidů	insekticid	k1gInPc2	insekticid
pro	pro	k7c4	pro
udržení	udržení	k1gNnSc4	udržení
produkce	produkce	k1gFnSc2	produkce
<g/>
.	.	kIx.	.
</s>
<s>
Finanční	finanční	k2eAgFnSc1d1	finanční
tíseň	tíseň	k1gFnSc1	tíseň
a	a	k8xC	a
dluhy	dluh	k1gInPc1	dluh
dohnaly	dohnat	k5eAaPmAgFnP	dohnat
překvapivě	překvapivě	k6eAd1	překvapivě
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
indických	indický	k2eAgMnPc2d1	indický
zemědělců	zemědělec	k1gMnPc2	zemědělec
až	až	k6eAd1	až
k	k	k7c3	k
sebevraždě	sebevražda	k1gFnSc3	sebevražda
–	–	k?	–
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1997	[number]	k4	1997
a	a	k8xC	a
2008	[number]	k4	2008
si	se	k3xPyFc3	se
jich	on	k3xPp3gMnPc2	on
vzalo	vzít	k5eAaPmAgNnS	vzít
život	život	k1gInSc4	život
kolem	kolem	k7c2	kolem
200	[number]	k4	200
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
rozvinula	rozvinout	k5eAaPmAgFnS	rozvinout
distribuční	distribuční	k2eAgFnSc1d1	distribuční
síť	síť	k1gFnSc1	síť
základních	základní	k2eAgFnPc2d1	základní
potravin	potravina	k1gFnPc2	potravina
(	(	kIx(	(
<g/>
PDS	PDS	kA	PDS
–	–	k?	–
Public	publicum	k1gNnPc2	publicum
Distribution	Distribution	k1gInSc1	Distribution
System	Syst	k1gMnSc7	Syst
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
řešila	řešit	k5eAaImAgFnS	řešit
nerovnoměrné	rovnoměrný	k2eNgNnSc4d1	nerovnoměrné
zásobování	zásobování	k1gNnSc4	zásobování
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
některé	některý	k3yIgInPc1	některý
regiony	region	k1gInPc1	region
Indie	Indie	k1gFnSc2	Indie
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
relativní	relativní	k2eAgFnSc6d1	relativní
hojnosti	hojnost	k1gFnSc6	hojnost
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
jiné	jiný	k2eAgFnPc1d1	jiná
trpěly	trpět	k5eAaImAgFnP	trpět
nedostatkem	nedostatek	k1gInSc7	nedostatek
<g/>
.	.	kIx.	.
</s>
<s>
Univerzální	univerzální	k2eAgInSc1d1	univerzální
systém	systém	k1gInSc1	systém
potravinové	potravinový	k2eAgFnSc2d1	potravinová
distribuce	distribuce	k1gFnSc2	distribuce
indickým	indický	k2eAgMnSc7d1	indický
zemědělcům	zemědělec	k1gMnPc3	zemědělec
zaručoval	zaručovat	k5eAaImAgInS	zaručovat
odbytiště	odbytiště	k1gNnPc4	odbytiště
pro	pro	k7c4	pro
plodiny	plodina	k1gFnPc4	plodina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
vypěstují	vypěstovat	k5eAaPmIp3nP	vypěstovat
<g/>
;	;	kIx,	;
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
rámci	rámec	k1gInSc6	rámec
existovala	existovat	k5eAaImAgFnS	existovat
obdoba	obdoba	k1gFnSc1	obdoba
přídělového	přídělový	k2eAgInSc2d1	přídělový
systému	systém	k1gInSc2	systém
potravin	potravina	k1gFnPc2	potravina
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
toho	ten	k3xDgNnSc2	ten
funguje	fungovat	k5eAaImIp3nS	fungovat
Food	Food	k1gInSc1	Food
Corporation	Corporation	k1gInSc1	Corporation
of	of	k?	of
India	indium	k1gNnSc2	indium
distribuující	distribuující	k2eAgFnSc2d1	distribuující
základní	základní	k2eAgFnSc2d1	základní
potraviny	potravina	k1gFnSc2	potravina
do	do	k7c2	do
i	i	k8xC	i
těch	ten	k3xDgFnPc2	ten
nejzapadlejších	zapadlý	k2eAgFnPc2d3	nejzapadlejší
vesniček	vesnička	k1gFnPc2	vesnička
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Indii	Indie	k1gFnSc6	Indie
a	a	k8xC	a
za	za	k7c4	za
dotované	dotovaný	k2eAgFnPc4d1	dotovaná
ceny	cena	k1gFnPc4	cena
<g/>
.	.	kIx.	.
</s>
<s>
Koordinuje	koordinovat	k5eAaBmIp3nS	koordinovat
tak	tak	k6eAd1	tak
největší	veliký	k2eAgInSc1d3	veliký
potravinový	potravinový	k2eAgInSc1d1	potravinový
program	program	k1gInSc1	program
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
a	a	k8xC	a
co	co	k3yRnSc1	co
do	do	k7c2	do
celkového	celkový	k2eAgNnSc2d1	celkové
množství	množství	k1gNnSc2	množství
potravin	potravina	k1gFnPc2	potravina
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
přes	přes	k7c4	přes
60	[number]	k4	60
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
obilí	obilí	k1gNnSc2	obilí
a	a	k8xC	a
rýže	rýže	k1gFnSc2	rýže
<g/>
)	)	kIx)	)
i	i	k9	i
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
však	však	k9	však
Světová	světový	k2eAgFnSc1d1	světová
banka	banka	k1gFnSc1	banka
donutila	donutit	k5eAaPmAgFnS	donutit
Indii	Indie	k1gFnSc4	Indie
první	první	k4xOgMnSc1	první
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
systémů	systém	k1gInPc2	systém
zrušit	zrušit	k5eAaPmF	zrušit
a	a	k8xC	a
druhý	druhý	k4xOgMnSc1	druhý
výrazně	výrazně	k6eAd1	výrazně
omezit	omezit	k5eAaPmF	omezit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
oba	dva	k4xCgMnPc1	dva
zásadně	zásadně	k6eAd1	zásadně
zasahovaly	zasahovat	k5eAaImAgFnP	zasahovat
do	do	k7c2	do
volného	volný	k2eAgInSc2d1	volný
trhu	trh	k1gInSc2	trh
s	s	k7c7	s
obilím	obilí	k1gNnSc7	obilí
<g/>
,	,	kIx,	,
rýží	rýže	k1gFnSc7	rýže
a	a	k8xC	a
ostatními	ostatní	k2eAgFnPc7d1	ostatní
obchodovanými	obchodovaný	k2eAgFnPc7d1	obchodovaná
potravinami	potravina	k1gFnPc7	potravina
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
změnil	změnit	k5eAaPmAgInS	změnit
průmysl	průmysl	k1gInSc4	průmysl
indická	indický	k2eAgNnPc4d1	indické
města	město	k1gNnPc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
tradičních	tradiční	k2eAgInPc2d1	tradiční
výrobků	výrobek	k1gInPc2	výrobek
<g/>
,	,	kIx,	,
např.	např.	kA	např.
bavlny	bavlna	k1gFnSc2	bavlna
a	a	k8xC	a
hedvábí	hedvábí	k1gNnSc2	hedvábí
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
těžké	těžký	k2eAgInPc1d1	těžký
stroje	stroj	k1gInPc1	stroj
a	a	k8xC	a
elektrické	elektrický	k2eAgNnSc4d1	elektrické
zboží	zboží	k1gNnSc4	zboží
<g/>
.	.	kIx.	.
</s>
<s>
Bohatá	bohatý	k2eAgNnPc1d1	bohaté
ložiska	ložisko	k1gNnPc1	ložisko
ropy	ropa	k1gFnSc2	ropa
a	a	k8xC	a
uhlí	uhlí	k1gNnSc2	uhlí
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
energii	energie	k1gFnSc4	energie
továrnám	továrna	k1gFnPc3	továrna
a	a	k8xC	a
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
více	hodně	k6eAd2	hodně
než	než	k8xS	než
polovinu	polovina	k1gFnSc4	polovina
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
spotřebuje	spotřebovat	k5eAaPmIp3nS	spotřebovat
(	(	kIx(	(
<g/>
o	o	k7c6	o
importu	import	k1gInSc6	import
energie	energie	k1gFnSc2	energie
vyjednává	vyjednávat	k5eAaImIp3nS	vyjednávat
indická	indický	k2eAgFnSc1d1	indická
vláda	vláda	k1gFnSc1	vláda
ve	v	k7c6	v
velkých	velký	k2eAgInPc6d1	velký
projektech	projekt	k1gInPc6	projekt
jako	jako	k8xC	jako
např.	např.	kA	např.
Transafghánský	Transafghánský	k2eAgInSc4d1	Transafghánský
plynovod	plynovod	k1gInSc4	plynovod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
posledních	poslední	k2eAgNnPc2d1	poslední
zhruba	zhruba	k6eAd1	zhruba
tří	tři	k4xCgFnPc2	tři
dekád	dekáda	k1gFnPc2	dekáda
se	se	k3xPyFc4	se
také	také	k9	také
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
význam	význam	k1gInSc1	význam
služeb	služba	k1gFnPc2	služba
a	a	k8xC	a
čím	co	k3yQnSc7	co
dál	daleko	k6eAd2	daleko
více	hodně	k6eAd2	hodně
Indů	Ind	k1gMnPc2	Ind
pracuje	pracovat	k5eAaImIp3nS	pracovat
v	v	k7c6	v
cestovním	cestovní	k2eAgInSc6d1	cestovní
ruchu	ruch	k1gInSc6	ruch
<g/>
,	,	kIx,	,
bankovnictví	bankovnictví	k1gNnSc6	bankovnictví
a	a	k8xC	a
spojích	spoj	k1gInPc6	spoj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
jsou	být	k5eAaImIp3nP	být
Indové	Ind	k1gMnPc1	Ind
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc2	jejich
firmy	firma	k1gFnSc2	firma
úspěšní	úspěšný	k2eAgMnPc1d1	úspěšný
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
IT	IT	kA	IT
<g/>
,	,	kIx,	,
talentovaní	talentovaný	k2eAgMnPc1d1	talentovaný
indičtí	indický	k2eAgMnPc1d1	indický
vědci	vědec	k1gMnPc1	vědec
si	se	k3xPyFc3	se
nezřídka	nezřídka	k6eAd1	nezřídka
volí	volit	k5eAaImIp3nP	volit
kariéru	kariéra	k1gFnSc4	kariéra
v	v	k7c6	v
Silicon	Silicon	kA	Silicon
Valley	Valle	k1gMnPc7	Valle
nebo	nebo	k8xC	nebo
prestižních	prestižní	k2eAgInPc6d1	prestižní
amerických	americký	k2eAgInPc6d1	americký
vědeckých	vědecký	k2eAgInPc6d1	vědecký
ústavech	ústav	k1gInPc6	ústav
<g/>
;	;	kIx,	;
fenoménem	fenomén	k1gInSc7	fenomén
se	se	k3xPyFc4	se
též	též	k9	též
stal	stát	k5eAaPmAgInS	stát
outsourcing	outsourcing	k1gInSc1	outsourcing
indických	indický	k2eAgMnPc2d1	indický
telemarketerů	telemarketer	k1gMnPc2	telemarketer
a	a	k8xC	a
telefonních	telefonní	k2eAgMnPc2d1	telefonní
operátorů	operátor	k1gMnPc2	operátor
pro	pro	k7c4	pro
větší	veliký	k2eAgFnPc4d2	veliký
americké	americký	k2eAgFnPc4d1	americká
korporace	korporace	k1gFnPc4	korporace
–	–	k?	–
zejména	zejména	k9	zejména
v	v	k7c6	v
první	první	k4xOgFnSc6	první
dekádě	dekáda	k1gFnSc6	dekáda
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
o	o	k7c6	o
potenciálu	potenciál	k1gInSc6	potenciál
Indie	Indie	k1gFnSc2	Indie
(	(	kIx(	(
<g/>
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Číny	Čína	k1gFnSc2	Čína
<g/>
)	)	kIx)	)
začíná	začínat	k5eAaImIp3nS	začínat
mluvit	mluvit	k5eAaImF	mluvit
jako	jako	k9	jako
o	o	k7c6	o
obrovském	obrovský	k2eAgMnSc6d1	obrovský
"	"	kIx"	"
<g/>
vynořujícím	vynořující	k2eAgInSc6d1	vynořující
se	se	k3xPyFc4	se
trhu	trh	k1gInSc3	trh
<g/>
"	"	kIx"	"
s	s	k7c7	s
pevně	pevně	k6eAd1	pevně
rostoucí	rostoucí	k2eAgFnSc7d1	rostoucí
ekonomikou	ekonomika	k1gFnSc7	ekonomika
<g/>
,	,	kIx,	,
zvyšující	zvyšující	k2eAgFnSc7d1	zvyšující
se	s	k7c7	s
životní	životní	k2eAgFnSc7d1	životní
úrovní	úroveň	k1gFnSc7	úroveň
i	i	k8xC	i
kupní	kupní	k2eAgFnSc7d1	kupní
silou	síla	k1gFnSc7	síla
více	hodně	k6eAd2	hodně
než	než	k8xS	než
miliardy	miliarda	k4xCgFnPc4	miliarda
jejích	její	k3xOp3gMnPc2	její
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
se	se	k3xPyFc4	se
Indie	Indie	k1gFnSc1	Indie
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
volného	volný	k2eAgNnSc2d1	volné
hospodářského	hospodářský	k2eAgNnSc2d1	hospodářské
uskupení	uskupení	k1gNnSc2	uskupení
známého	známý	k2eAgNnSc2d1	známé
pod	pod	k7c7	pod
akronymem	akronym	k1gInSc7	akronym
BRICS	BRICS	kA	BRICS
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1969	[number]	k4	1969
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
Indická	indický	k2eAgFnSc1d1	indická
kosmická	kosmický	k2eAgFnSc1d1	kosmická
agentura	agentura	k1gFnSc1	agentura
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
ISRO	ISRO	kA	ISRO
–	–	k?	–
Indian	Indiana	k1gFnPc2	Indiana
Space	Space	k1gFnSc1	Space
Research	Research	k1gInSc1	Research
Organisation	Organisation	k1gInSc1	Organisation
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
pracuje	pracovat	k5eAaImIp3nS	pracovat
kolem	kolem	k7c2	kolem
20	[number]	k4	20
tisíc	tisíc	k4xCgInPc2	tisíc
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
řídí	řídit	k5eAaImIp3nS	řídit
několik	několik	k4yIc4	několik
misí	mise	k1gFnPc2	mise
současně	současně	k6eAd1	současně
<g/>
.	.	kIx.	.
</s>
<s>
14	[number]	k4	14
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2008	[number]	k4	2008
dopadla	dopadnout	k5eAaPmAgFnS	dopadnout
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
Měsíce	měsíc	k1gInSc2	měsíc
první	první	k4xOgFnSc1	první
indická	indický	k2eAgFnSc1d1	indická
sonda	sonda	k1gFnSc1	sonda
<g/>
.	.	kIx.	.
</s>
<s>
Oddělila	oddělit	k5eAaPmAgFnS	oddělit
se	se	k3xPyFc4	se
z	z	k7c2	z
družice	družice	k1gFnSc2	družice
Čandraján-	Čandraján-	k1gFnSc2	Čandraján-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
vyslána	vyslán	k2eAgFnSc1d1	vyslána
22	[number]	k4	22
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Indie	Indie	k1gFnSc1	Indie
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stala	stát	k5eAaPmAgFnS	stát
další	další	k2eAgFnSc7d1	další
mocností	mocnost	k1gFnSc7	mocnost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
dokázala	dokázat	k5eAaPmAgFnS	dokázat
přistání	přistání	k1gNnSc4	přistání
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
zvládnout	zvládnout	k5eAaPmF	zvládnout
<g/>
.	.	kIx.	.
</s>
<s>
Indický	indický	k2eAgInSc1d1	indický
solární	solární	k2eAgInSc1d1	solární
program	program	k1gInSc1	program
plánuje	plánovat	k5eAaImIp3nS	plánovat
výstavbu	výstavba	k1gFnSc4	výstavba
solárních	solární	k2eAgNnPc2d1	solární
zařízení	zařízení	k1gNnPc2	zařízení
o	o	k7c6	o
výkonu	výkon	k1gInSc6	výkon
200	[number]	k4	200
gigawattů	gigawatt	k1gInPc2	gigawatt
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2050	[number]	k4	2050
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
o	o	k7c4	o
třetinu	třetina	k1gFnSc4	třetina
více	hodně	k6eAd2	hodně
než	než	k8xS	než
byla	být	k5eAaImAgFnS	být
celková	celkový	k2eAgFnSc1d1	celková
indická	indický	k2eAgFnSc1d1	indická
energetická	energetický	k2eAgFnSc1d1	energetická
výrobní	výrobní	k2eAgFnSc1d1	výrobní
kapacita	kapacita	k1gFnSc1	kapacita
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
má	mít	k5eAaImIp3nS	mít
přinést	přinést	k5eAaPmF	přinést
až	až	k9	až
100	[number]	k4	100
tisíc	tisíc	k4xCgInSc1	tisíc
nových	nový	k2eAgNnPc2d1	nové
pracovních	pracovní	k2eAgNnPc2d1	pracovní
míst	místo	k1gNnPc2	místo
a	a	k8xC	a
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
dokončení	dokončení	k1gNnSc6	dokončení
zabránit	zabránit	k5eAaPmF	zabránit
každoročně	každoročně	k6eAd1	každoročně
emisím	emise	k1gFnPc3	emise
asi	asi	k9	asi
434	[number]	k4	434
milionů	milion	k4xCgInPc2	milion
tun	tuna	k1gFnPc2	tuna
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
<g/>
.	.	kIx.	.
</s>
<s>
Celkové	celkový	k2eAgInPc1d1	celkový
náklady	náklad	k1gInPc1	náklad
jsou	být	k5eAaImIp3nP	být
odhadovány	odhadován	k2eAgInPc1d1	odhadován
asi	asi	k9	asi
na	na	k7c4	na
20	[number]	k4	20
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Mars	Mars	k1gInSc1	Mars
Orbiter	Orbiter	k1gInSc1	Orbiter
Mission	Mission	k1gInSc4	Mission
je	být	k5eAaImIp3nS	být
mise	mise	k1gFnSc1	mise
ISRO	ISRO	kA	ISRO
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
pomocí	pomocí	k7c2	pomocí
sondy	sonda	k1gFnSc2	sonda
Mangalaján	Mangalaján	k1gInSc1	Mangalaján
průzkum	průzkum	k1gInSc1	průzkum
planety	planeta	k1gFnSc2	planeta
Mars	Mars	k1gMnSc1	Mars
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
její	její	k3xOp3gFnSc2	její
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
plánování	plánování	k1gNnSc1	plánování
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
byla	být	k5eAaImAgFnS	být
započata	započat	k2eAgFnSc1d1	započata
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
a	a	k8xC	a
indická	indický	k2eAgFnSc1d1	indická
vláda	vláda	k1gFnSc1	vláda
ji	on	k3xPp3gFnSc4	on
schválila	schválit	k5eAaPmAgFnS	schválit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
rozpočet	rozpočet	k1gInSc1	rozpočet
je	být	k5eAaImIp3nS	být
4,54	[number]	k4	4,54
miliardy	miliarda	k4xCgFnSc2	miliarda
INR	INR	kA	INR
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
relativně	relativně	k6eAd1	relativně
malých	malý	k2eAgInPc2d1	malý
<g/>
)	)	kIx)	)
74	[number]	k4	74
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Demografie	demografie	k1gFnSc2	demografie
Indie	Indie	k1gFnSc2	Indie
<g/>
,	,	kIx,	,
Indové	Ind	k1gMnPc1	Ind
<g/>
,	,	kIx,	,
Jazyky	jazyk	k1gInPc1	jazyk
Indie	Indie	k1gFnSc2	Indie
a	a	k8xC	a
Náboženství	náboženství	k1gNnSc2	náboženství
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
1	[number]	k4	1
210	[number]	k4	210
193	[number]	k4	193
422	[number]	k4	422
obyvateli	obyvatel	k1gMnPc7	obyvatel
podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
je	být	k5eAaImIp3nS	být
Indie	Indie	k1gFnSc2	Indie
druhá	druhý	k4xOgFnSc1	druhý
nejlidnatější	lidnatý	k2eAgFnSc1d3	nejlidnatější
země	země	k1gFnSc1	země
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
2001	[number]	k4	2001
<g/>
–	–	k?	–
<g/>
2011	[number]	k4	2011
její	její	k3xOp3gFnSc2	její
populace	populace	k1gFnSc2	populace
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
o	o	k7c4	o
17,64	[number]	k4	17,64
<g/>
%	%	kIx~	%
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
předchozí	předchozí	k2eAgFnSc7d1	předchozí
dekádou	dekáda	k1gFnSc7	dekáda
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
–	–	k?	–
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
růst	růst	k1gInSc1	růst
21,54	[number]	k4	21,54
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
361	[number]	k4	361
milionu	milion	k4xCgInSc2	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Indie	Indie	k1gFnSc1	Indie
je	být	k5eAaImIp3nS	být
země	zem	k1gFnPc4	zem
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yRgFnPc4	který
žije	žít	k5eAaImIp3nS	žít
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
různých	různý	k2eAgFnPc2d1	různá
etnických	etnický	k2eAgFnPc2d1	etnická
skupin	skupina	k1gFnPc2	skupina
a	a	k8xC	a
hovoří	hovořit	k5eAaImIp3nS	hovořit
se	se	k3xPyFc4	se
v	v	k7c6	v
nich	on	k3xPp3gMnPc6	on
asi	asi	k9	asi
tisíci	tisíc	k4xCgInPc7	tisíc
jazyky	jazyk	k1gInPc7	jazyk
a	a	k8xC	a
nářečími	nářečí	k1gNnPc7	nářečí
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
dvě	dva	k4xCgFnPc1	dva
hlavní	hlavní	k2eAgFnPc1d1	hlavní
jazykové	jazykový	k2eAgFnPc1d1	jazyková
rodiny	rodina	k1gFnPc1	rodina
<g/>
:	:	kIx,	:
Indoíránská	indoíránský	k2eAgFnSc1d1	indoíránský
(	(	kIx(	(
<g/>
hovoří	hovořit	k5eAaImIp3nS	hovořit
ji	on	k3xPp3gFnSc4	on
asi	asi	k9	asi
74	[number]	k4	74
<g/>
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
)	)	kIx)	)
a	a	k8xC	a
drávidská	drávidský	k2eAgFnSc1d1	drávidská
(	(	kIx(	(
<g/>
24	[number]	k4	24
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgInPc4d1	další
používané	používaný	k2eAgInPc4d1	používaný
jazyky	jazyk	k1gInPc4	jazyk
patří	patřit	k5eAaImIp3nP	patřit
austroasijské	austroasijský	k2eAgInPc1d1	austroasijský
a	a	k8xC	a
sinotibetské	sinotibetský	k2eAgInPc1d1	sinotibetský
jazyky	jazyk	k1gInPc1	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Úředními	úřední	k2eAgInPc7d1	úřední
jazyky	jazyk	k1gInPc7	jazyk
na	na	k7c6	na
federální	federální	k2eAgFnSc6d1	federální
úrovni	úroveň	k1gFnSc6	úroveň
je	být	k5eAaImIp3nS	být
hindština	hindština	k1gFnSc1	hindština
a	a	k8xC	a
angličtina	angličtina	k1gFnSc1	angličtina
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc4	každý
stát	stát	k1gInSc4	stát
federace	federace	k1gFnSc2	federace
si	se	k3xPyFc3	se
dále	daleko	k6eAd2	daleko
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
svůj	svůj	k3xOyFgInSc4	svůj
úřední	úřední	k2eAgInSc4d1	úřední
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yQnSc3	což
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
přes	přes	k7c4	přes
dvacet	dvacet	k4xCc4	dvacet
oficiálně	oficiálně	k6eAd1	oficiálně
uznaných	uznaný	k2eAgInPc2d1	uznaný
jazyků	jazyk	k1gInPc2	jazyk
k	k	k7c3	k
místní	místní	k2eAgFnSc3d1	místní
úřední	úřední	k2eAgFnSc3d1	úřední
komunikaci	komunikace	k1gFnSc3	komunikace
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
recognized	recognized	k1gMnSc1	recognized
languages	languages	k1gMnSc1	languages
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
nich	on	k3xPp3gInPc2	on
se	se	k3xPyFc4	se
však	však	k9	však
mluví	mluvit	k5eAaImIp3nS	mluvit
řadou	řada	k1gFnSc7	řada
místních	místní	k2eAgInPc2d1	místní
dialektů	dialekt	k1gInPc2	dialekt
a	a	k8xC	a
kmenových	kmenový	k2eAgInPc2d1	kmenový
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
také	také	k9	také
vyznávají	vyznávat	k5eAaImIp3nP	vyznávat
mnoho	mnoho	k6eAd1	mnoho
různých	různý	k2eAgNnPc2d1	různé
náboženství	náboženství	k1gNnPc2	náboženství
<g/>
,	,	kIx,	,
skoro	skoro	k6eAd1	skoro
čtyři	čtyři	k4xCgFnPc1	čtyři
pětiny	pětina	k1gFnPc1	pětina
(	(	kIx(	(
<g/>
79,8	[number]	k4	79,8
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
jsou	být	k5eAaImIp3nP	být
hinduisté	hinduista	k1gMnPc1	hinduista
<g/>
,	,	kIx,	,
následovaní	následovaný	k2eAgMnPc1d1	následovaný
14,23	[number]	k4	14,23
<g/>
%	%	kIx~	%
muslimů	muslim	k1gMnPc2	muslim
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
zbývající	zbývající	k2eAgFnSc4d1	zbývající
patří	patřit	k5eAaImIp3nP	patřit
křesťané	křesťan	k1gMnPc1	křesťan
s	s	k7c7	s
2,3	[number]	k4	2,3
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
sikhové	sikh	k1gMnPc1	sikh
s	s	k7c7	s
1,9	[number]	k4	1,9
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
buddhisté	buddhista	k1gMnPc1	buddhista
(	(	kIx(	(
<g/>
0,70	[number]	k4	0,70
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
džinisté	džinista	k1gMnPc1	džinista
(	(	kIx(	(
<g/>
0,36	[number]	k4	0,36
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
hinduistické	hinduistický	k2eAgFnSc2d1	hinduistická
tradice	tradice	k1gFnSc2	tradice
se	se	k3xPyFc4	se
lidé	člověk	k1gMnPc1	člověk
rodí	rodit	k5eAaImIp3nP	rodit
do	do	k7c2	do
sociálních	sociální	k2eAgFnPc2d1	sociální
vrstev	vrstva	k1gFnPc2	vrstva
zvaných	zvaný	k2eAgFnPc2d1	zvaná
kasty	kasta	k1gFnSc2	kasta
<g/>
.	.	kIx.	.
</s>
<s>
Přísná	přísný	k2eAgNnPc1d1	přísné
náboženská	náboženský	k2eAgNnPc1d1	náboženské
pravidla	pravidlo	k1gNnPc1	pravidlo
přikazují	přikazovat	k5eAaImIp3nP	přikazovat
každé	každý	k3xTgFnSc3	každý
kastě	kasta	k1gFnSc3	kasta
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
má	mít	k5eAaImIp3nS	mít
jíst	jíst	k5eAaImF	jíst
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
si	se	k3xPyFc3	se
oblékat	oblékat	k5eAaImF	oblékat
i	i	k8xC	i
jakou	jaký	k3yQgFnSc4	jaký
vykonávat	vykonávat	k5eAaImF	vykonávat
práci	práce	k1gFnSc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
toto	tento	k3xDgNnSc4	tento
rozdělení	rozdělení	k1gNnSc4	rozdělení
na	na	k7c6	na
samém	samý	k3xTgInSc6	samý
dně	dno	k1gNnSc6	dno
indické	indický	k2eAgFnSc2d1	indická
společnosti	společnost	k1gFnSc2	společnost
stojí	stát	k5eAaImIp3nS	stát
nedotknutelní	nedotknutelní	k?	nedotknutelní
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterým	který	k3yRgFnPc3	který
náleží	náležet	k5eAaImIp3nS	náležet
asi	asi	k9	asi
160	[number]	k4	160
milionů	milion	k4xCgInPc2	milion
Indů	Indus	k1gInPc2	Indus
<g/>
.	.	kIx.	.
</s>
<s>
Rodinná	rodinný	k2eAgNnPc4d1	rodinné
pouta	pouto	k1gNnPc4	pouto
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
velmi	velmi	k6eAd1	velmi
důležitá	důležitý	k2eAgFnSc1d1	důležitá
a	a	k8xC	a
na	na	k7c4	na
sňatek	sňatek	k1gInSc4	sňatek
se	se	k3xPyFc4	se
často	často	k6eAd1	často
pohlíží	pohlížet	k5eAaImIp3nS	pohlížet
spíše	spíše	k9	spíše
jako	jako	k9	jako
na	na	k7c4	na
spojení	spojení	k1gNnSc4	spojení
dvou	dva	k4xCgFnPc2	dva
rodin	rodina	k1gFnPc2	rodina
než	než	k8xS	než
jako	jako	k9	jako
na	na	k7c4	na
svazek	svazek	k1gInSc4	svazek
dvou	dva	k4xCgMnPc2	dva
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zvykem	zvyk	k1gInSc7	zvyk
<g/>
,	,	kIx,	,
že	že	k8xS	že
rodiče	rodič	k1gMnPc1	rodič
vybírají	vybírat	k5eAaImIp3nP	vybírat
svým	svůj	k3xOyFgFnPc3	svůj
dětem	dítě	k1gFnPc3	dítě
životní	životní	k2eAgFnSc1d1	životní
partnery	partner	k1gMnPc7	partner
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
někteří	některý	k3yIgMnPc1	některý
Indové	Ind	k1gMnPc1	Ind
snaží	snažit	k5eAaImIp3nP	snažit
tato	tento	k3xDgNnPc4	tento
pravidla	pravidlo	k1gNnPc4	pravidlo
odstranit	odstranit	k5eAaPmF	odstranit
a	a	k8xC	a
povzbuzují	povzbuzovat	k5eAaImIp3nP	povzbuzovat
mladé	mladý	k2eAgMnPc4d1	mladý
lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
si	se	k3xPyFc3	se
sami	sám	k3xTgMnPc1	sám
hledali	hledat	k5eAaImAgMnP	hledat
manžela	manžel	k1gMnSc4	manžel
či	či	k8xC	či
manželku	manželka	k1gFnSc4	manželka
<g/>
.	.	kIx.	.
</s>
<s>
Život	život	k1gInSc1	život
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
indických	indický	k2eAgFnPc6d1	indická
vesnicích	vesnice	k1gFnPc6	vesnice
se	se	k3xPyFc4	se
po	po	k7c4	po
staletí	staletí	k1gNnPc4	staletí
nezměnil	změnit	k5eNaPmAgInS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
si	se	k3xPyFc3	se
chodí	chodit	k5eAaImIp3nP	chodit
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
pro	pro	k7c4	pro
vodu	voda	k1gFnSc4	voda
do	do	k7c2	do
studně	studně	k1gFnSc2	studně
a	a	k8xC	a
osvětlují	osvětlovat	k5eAaImIp3nP	osvětlovat
si	se	k3xPyFc3	se
domovy	domov	k1gInPc4	domov
olejovými	olejový	k2eAgFnPc7d1	olejová
lampičkami	lampička	k1gFnPc7	lampička
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
růstu	růst	k1gInSc6	růst
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
úrovně	úroveň	k1gFnSc2	úroveň
se	se	k3xPyFc4	se
do	do	k7c2	do
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
vesnic	vesnice	k1gFnPc2	vesnice
zavádí	zavádět	k5eAaImIp3nS	zavádět
voda	voda	k1gFnSc1	voda
a	a	k8xC	a
elektřina	elektřina	k1gFnSc1	elektřina
<g/>
.	.	kIx.	.
41	[number]	k4	41
%	%	kIx~	%
všech	všecek	k3xTgMnPc2	všecek
chudých	chudý	k2eAgMnPc2d1	chudý
lidí	člověk	k1gMnPc2	člověk
světa	svět	k1gInSc2	svět
ale	ale	k8xC	ale
stále	stále	k6eAd1	stále
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
Indie	Indie	k1gFnSc1	Indie
zaznamenává	zaznamenávat	k5eAaImIp3nS	zaznamenávat
fenomén	fenomén	k1gInSc1	fenomén
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
obyvatelé	obyvatel	k1gMnPc1	obyvatel
vesnic	vesnice	k1gFnPc2	vesnice
přicházejí	přicházet	k5eAaImIp3nP	přicházet
ve	v	k7c6	v
stále	stále	k6eAd1	stále
větší	veliký	k2eAgFnSc6d2	veliký
míře	míra	k1gFnSc6	míra
do	do	k7c2	do
měst	město	k1gNnPc2	město
(	(	kIx(	(
<g/>
urbanizace	urbanizace	k1gFnSc2	urbanizace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městech	město	k1gNnPc6	město
se	se	k3xPyFc4	se
velký	velký	k2eAgInSc1d1	velký
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
tísní	tísnit	k5eAaImIp3nS	tísnit
v	v	k7c6	v
přeplněných	přeplněný	k2eAgFnPc6d1	přeplněná
chatrčích	chatrč	k1gFnPc6	chatrč
na	na	k7c6	na
předměstích	předměstí	k1gNnPc6	předměstí
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
mnoho	mnoho	k4c1	mnoho
bohatších	bohatý	k2eAgMnPc2d2	bohatší
Indů	Ind	k1gMnPc2	Ind
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
patrný	patrný	k2eAgInSc1d1	patrný
silný	silný	k2eAgInSc4d1	silný
západní	západní	k2eAgInSc4d1	západní
vliv	vliv	k1gInSc4	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
lidé	člověk	k1gMnPc1	člověk
nosí	nosit	k5eAaImIp3nP	nosit
oblečení	oblečení	k1gNnSc4	oblečení
podle	podle	k7c2	podle
západní	západní	k2eAgFnSc2d1	západní
módy	móda	k1gFnSc2	móda
<g/>
,	,	kIx,	,
jiní	jiný	k2eAgMnPc1d1	jiný
dávají	dávat	k5eAaImIp3nP	dávat
přednost	přednost	k1gFnSc4	přednost
tradičním	tradiční	k2eAgInPc3d1	tradiční
oděvům	oděv	k1gInPc3	oděv
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
mnoho	mnoho	k4c1	mnoho
Indek	Indka	k1gFnPc2	Indka
nosí	nosit	k5eAaImIp3nP	nosit
jasně	jasně	k6eAd1	jasně
barevná	barevný	k2eAgNnPc1d1	barevné
sárí	sárí	k1gNnPc1	sárí
a	a	k8xC	a
bindí	bindí	k1gNnPc1	bindí
<g/>
.	.	kIx.	.
</s>
<s>
Společenský	společenský	k2eAgInSc1d1	společenský
život	život	k1gInSc1	život
se	se	k3xPyFc4	se
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
soustřeďuje	soustřeďovat	k5eAaImIp3nS	soustřeďovat
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
neustále	neustále	k6eAd1	neustále
zaplněna	zaplnit	k5eAaPmNgFnS	zaplnit
lidmi	člověk	k1gMnPc7	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
si	se	k3xPyFc3	se
sem	sem	k6eAd1	sem
chodí	chodit	k5eAaImIp3nP	chodit
vyměňovat	vyměňovat	k5eAaImF	vyměňovat
novinky	novinka	k1gFnPc4	novinka
a	a	k8xC	a
názory	názor	k1gInPc4	názor
za	za	k7c2	za
zvuků	zvuk	k1gInPc2	zvuk
hlasitě	hlasitě	k6eAd1	hlasitě
hrající	hrající	k2eAgFnSc2d1	hrající
populární	populární	k2eAgFnSc2d1	populární
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
klaksonů	klakson	k1gInPc2	klakson
aut	auto	k1gNnPc2	auto
a	a	k8xC	a
volání	volání	k1gNnPc2	volání
pouličních	pouliční	k2eAgMnPc2d1	pouliční
prodavačů	prodavač	k1gMnPc2	prodavač
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c4	v
Indii	Indie	k1gFnSc4	Indie
právě	právě	k9	právě
díky	díky	k7c3	díky
stěhování	stěhování	k1gNnSc3	stěhování
lidí	člověk	k1gMnPc2	člověk
do	do	k7c2	do
měst	město	k1gNnPc2	město
k	k	k7c3	k
rapidnímu	rapidní	k2eAgInSc3d1	rapidní
růstu	růst	k1gInSc3	růst
mnohých	mnohý	k2eAgFnPc2d1	mnohá
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
tak	tak	k6eAd1	tak
má	mít	k5eAaImIp3nS	mít
Indie	Indie	k1gFnSc1	Indie
mnoho	mnoho	k4c4	mnoho
sídel	sídlo	k1gNnPc2	sídlo
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
milionem	milion	k4xCgInSc7	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Životní	životní	k2eAgFnSc1d1	životní
úroveň	úroveň	k1gFnSc1	úroveň
většiny	většina	k1gFnSc2	většina
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
oblastech	oblast	k1gFnPc6	oblast
však	však	k9	však
není	být	k5eNaImIp3nS	být
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1991	[number]	k4	1991
a	a	k8xC	a
2001	[number]	k4	2001
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
podíl	podíl	k1gInSc1	podíl
Indů	Indus	k1gInPc2	Indus
žijících	žijící	k2eAgInPc2d1	žijící
v	v	k7c4	v
městský	městský	k2eAgInSc4d1	městský
oblastech	oblast	k1gFnPc6	oblast
o	o	k7c4	o
31,2	[number]	k4	31,2
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
žilo	žít	k5eAaImAgNnS	žít
nadále	nadále	k6eAd1	nadále
přes	přes	k7c4	přes
70	[number]	k4	70
<g/>
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
tradiční	tradiční	k2eAgMnPc1d1	tradiční
indičtí	indický	k2eAgMnPc1d1	indický
zemědělci	zemědělec	k1gMnPc1	zemědělec
–	–	k?	–
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
pěstitelé	pěstitel	k1gMnPc1	pěstitel
bavlny	bavlna	k1gFnSc2	bavlna
a	a	k8xC	a
sójových	sójový	k2eAgInPc2d1	sójový
bobů	bob	k1gInPc2	bob
–	–	k?	–
v	v	k7c6	v
rostoucí	rostoucí	k2eAgFnSc6d1	rostoucí
míře	míra	k1gFnSc6	míra
přecházejí	přecházet	k5eAaImIp3nP	přecházet
na	na	k7c6	na
pěstování	pěstování	k1gNnSc3	pěstování
geneticky	geneticky	k6eAd1	geneticky
modifikovaných	modifikovaný	k2eAgFnPc2d1	modifikovaná
potravin	potravina	k1gFnPc2	potravina
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
od	od	k7c2	od
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
korporací	korporace	k1gFnPc2	korporace
(	(	kIx(	(
<g/>
Monsanto	Monsanta	k1gFnSc5	Monsanta
<g/>
)	)	kIx)	)
kupují	kupovat	k5eAaImIp3nP	kupovat
GM	GM	kA	GM
semena	semeno	k1gNnSc2	semeno
a	a	k8xC	a
ad	ad	k7c4	ad
hoc	hoc	k?	hoc
vyvinuté	vyvinutý	k2eAgInPc4d1	vyvinutý
herbicidy	herbicid	k1gInPc4	herbicid
(	(	kIx(	(
<g/>
Roundup	Roundup	k1gInSc4	Roundup
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Indická	indický	k2eAgFnSc1d1	indická
kultura	kultura	k1gFnSc1	kultura
<g/>
.	.	kIx.	.
</s>
<s>
India	indium	k1gNnPc1	indium
měla	mít	k5eAaImAgNnP	mít
bohaté	bohatý	k2eAgNnSc4d1	bohaté
a	a	k8xC	a
unikátní	unikátní	k2eAgNnSc4d1	unikátní
kulturní	kulturní	k2eAgNnSc4d1	kulturní
dědictví	dědictví	k1gNnSc4	dědictví
a	a	k8xC	a
dokázala	dokázat	k5eAaPmAgFnS	dokázat
si	se	k3xPyFc3	se
zachovat	zachovat	k5eAaPmF	zachovat
své	svůj	k3xOyFgFnPc4	svůj
tradice	tradice	k1gFnPc4	tradice
během	během	k7c2	během
historie	historie	k1gFnSc2	historie
i	i	k9	i
přes	přes	k7c4	přes
přebírání	přebírání	k1gNnSc4	přebírání
nových	nový	k2eAgInPc2d1	nový
zvyků	zvyk	k1gInPc2	zvyk
<g/>
,	,	kIx,	,
tradic	tradice	k1gFnPc2	tradice
a	a	k8xC	a
myšlenek	myšlenka	k1gFnPc2	myšlenka
zároveň	zároveň	k6eAd1	zároveň
od	od	k7c2	od
útočníků	útočník	k1gMnPc2	útočník
i	i	k8xC	i
imigrantů	imigrant	k1gMnPc2	imigrant
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
zvyků	zvyk	k1gInPc2	zvyk
<g/>
,	,	kIx,	,
jazyků	jazyk	k1gInPc2	jazyk
a	a	k8xC	a
monumentů	monument	k1gInPc2	monument
je	být	k5eAaImIp3nS	být
důkazem	důkaz	k1gInSc7	důkaz
vzájemného	vzájemný	k2eAgNnSc2d1	vzájemné
míchání	míchání	k1gNnSc2	míchání
se	se	k3xPyFc4	se
vlivů	vliv	k1gInPc2	vliv
během	během	k7c2	během
staletí	staletí	k1gNnSc2	staletí
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
slavný	slavný	k2eAgMnSc1d1	slavný
Tádžmahal	Tádžmahal	k1gMnSc1	Tádžmahal
nebo	nebo	k8xC	nebo
jiné	jiný	k2eAgFnPc1d1	jiná
památky	památka	k1gFnPc1	památka
jsou	být	k5eAaImIp3nP	být
příkladem	příklad	k1gInSc7	příklad
islámém	islámý	k2eAgMnSc6d1	islámý
inspirované	inspirovaný	k2eAgFnPc4d1	inspirovaná
architektury	architektura	k1gFnPc4	architektura
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
zděděna	zdědit	k5eAaPmNgFnS	zdědit
po	po	k7c6	po
Mughalech	Mughal	k1gInPc6	Mughal
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
výsledkem	výsledek	k1gInSc7	výsledek
synkretické	synkretický	k2eAgFnSc2d1	synkretická
tradice	tradice	k1gFnSc2	tradice
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
kombinuje	kombinovat	k5eAaImIp3nS	kombinovat
elementy	element	k1gInPc4	element
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
částí	část	k1gFnPc2	část
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Indická	indický	k2eAgFnSc1d1	indická
společnost	společnost	k1gFnSc1	společnost
je	být	k5eAaImIp3nS	být
široce	široko	k6eAd1	široko
pluralitní	pluralitní	k2eAgFnSc1d1	pluralitní
<g/>
,	,	kIx,	,
mnohojazyčná	mnohojazyčný	k2eAgFnSc1d1	mnohojazyčná
a	a	k8xC	a
mnohonárodnostní	mnohonárodnostní	k2eAgFnSc1d1	mnohonárodnostní
<g/>
.	.	kIx.	.
</s>
<s>
Indické	indický	k2eAgNnSc1d1	indické
tradiční	tradiční	k2eAgNnSc1d1	tradiční
oblečení	oblečení	k1gNnSc1	oblečení
je	být	k5eAaImIp3nS	být
odlišné	odlišný	k2eAgNnSc1d1	odlišné
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
regiony	region	k1gInPc7	region
<g/>
.	.	kIx.	.
</s>
<s>
Značně	značně	k6eAd1	značně
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
ať	ať	k9	ať
už	už	k6eAd1	už
barvami	barva	k1gFnPc7	barva
nebo	nebo	k8xC	nebo
styly	styl	k1gInPc7	styl
<g/>
.	.	kIx.	.
</s>
<s>
Záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
různých	různý	k2eAgInPc6d1	různý
faktorech	faktor	k1gInPc6	faktor
jako	jako	k8xC	jako
podnebí	podnebí	k1gNnSc2	podnebí
<g/>
.	.	kIx.	.
</s>
<s>
Nejpopulárnější	populární	k2eAgInSc1d3	nejpopulárnější
oděv	oděv	k1gInSc1	oděv
pro	pro	k7c4	pro
ženy	žena	k1gFnPc4	žena
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
sárí	sárí	k1gNnSc7	sárí
a	a	k8xC	a
nejtradičnějším	tradiční	k2eAgInSc7d3	nejtradičnější
oděvem	oděv	k1gInSc7	oděv
mužů	muž	k1gMnPc2	muž
je	být	k5eAaImIp3nS	být
dhótí	dhótí	k1gNnSc1	dhótí
(	(	kIx(	(
<g/>
řasená	řasený	k2eAgFnSc1d1	řasená
bederní	bederní	k2eAgFnSc1d1	bederní
rouška	rouška	k1gFnSc1	rouška
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzdělání	vzdělání	k1gNnSc1	vzdělání
je	být	k5eAaImIp3nS	být
vysoce	vysoce	k6eAd1	vysoce
ceněno	cenit	k5eAaImNgNnS	cenit
členy	člen	k1gMnPc4	člen
každé	každý	k3xTgFnSc2	každý
socioekonomické	socioekonomický	k2eAgFnSc2d1	socioekonomická
vrstvy	vrstva	k1gFnSc2	vrstva
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Tradiční	tradiční	k2eAgFnPc1d1	tradiční
indické	indický	k2eAgFnPc1d1	indická
hodnoty	hodnota	k1gFnPc1	hodnota
jako	jako	k8xS	jako
rodina	rodina	k1gFnSc1	rodina
jsou	být	k5eAaImIp3nP	být
vysoce	vysoce	k6eAd1	vysoce
respektovány	respektován	k2eAgInPc1d1	respektován
a	a	k8xC	a
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
posvátné	posvátný	k2eAgNnSc4d1	posvátné
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
městské	městský	k2eAgFnPc1d1	městská
rodiny	rodina	k1gFnPc1	rodina
začínají	začínat	k5eAaImIp3nP	začínat
preferovat	preferovat	k5eAaImF	preferovat
rodinu	rodina	k1gFnSc4	rodina
dvou	dva	k4xCgMnPc2	dva
partnerů	partner	k1gMnPc2	partner
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
socioekonomických	socioekonomický	k2eAgNnPc2d1	socioekonomické
omezení	omezení	k1gNnPc2	omezení
daných	daný	k2eAgNnPc2d1	dané
systémem	systém	k1gInSc7	systém
skupinové	skupinový	k2eAgFnPc4d1	skupinová
rodiny	rodina	k1gFnPc4	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Náboženství	náboženství	k1gNnSc1	náboženství
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
společenskou	společenský	k2eAgFnSc7d1	společenská
událostí	událost	k1gFnSc7	událost
se	s	k7c7	s
spoustou	spousta	k1gFnSc7	spousta
praktik	praktika	k1gFnPc2	praktika
provázených	provázený	k2eAgFnPc2d1	provázená
okázalostí	okázalost	k1gFnPc2	okázalost
a	a	k8xC	a
vitalitou	vitalita	k1gFnSc7	vitalita
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nedílnou	dílný	k2eNgFnSc7d1	nedílná
součástí	součást	k1gFnSc7	součást
každodenního	každodenní	k2eAgInSc2d1	každodenní
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
literární	literární	k2eAgFnPc1d1	literární
zmínky	zmínka	k1gFnPc1	zmínka
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
měly	mít	k5eAaImAgInP	mít
většinou	většinou	k6eAd1	většinou
ústní	ústní	k2eAgFnSc4d1	ústní
formu	forma	k1gFnSc4	forma
a	a	k8xC	a
až	až	k9	až
později	pozdě	k6eAd2	pozdě
byly	být	k5eAaImAgFnP	být
převedeny	převést	k5eAaPmNgFnP	převést
do	do	k7c2	do
písemné	písemný	k2eAgFnSc2d1	písemná
podoby	podoba	k1gFnSc2	podoba
jako	jako	k8xC	jako
například	například	k6eAd1	například
Védy	véd	k1gInPc4	véd
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
eposy	epos	k1gInPc4	epos
Mahábhárata	Mahábhárata	k1gFnSc1	Mahábhárata
a	a	k8xC	a
Rámájana	Rámájana	k1gFnSc1	Rámájana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
je	být	k5eAaImIp3nS	být
také	také	k9	také
množství	množství	k1gNnSc1	množství
významných	významný	k2eAgMnPc2d1	významný
a	a	k8xC	a
moderních	moderní	k2eAgMnPc2d1	moderní
spisovatelů	spisovatel	k1gMnPc2	spisovatel
píšících	píšící	k2eAgMnPc2d1	píšící
v	v	k7c6	v
indických	indický	k2eAgInPc6d1	indický
jazycích	jazyk	k1gInPc6	jazyk
a	a	k8xC	a
angličtině	angličtina	k1gFnSc6	angličtina
<g/>
.	.	kIx.	.
</s>
<s>
Jediným	jediný	k2eAgMnSc7d1	jediný
nositelem	nositel	k1gMnSc7	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
byl	být	k5eAaImAgMnS	být
bengálský	bengálský	k2eAgMnSc1d1	bengálský
spisovatel	spisovatel	k1gMnSc1	spisovatel
Rabíndranáth	Rabíndranáth	k1gMnSc1	Rabíndranáth
Thákur	Thákur	k1gMnSc1	Thákur
<g/>
.	.	kIx.	.
</s>
<s>
Indie	Indie	k1gFnSc1	Indie
ročně	ročně	k6eAd1	ročně
vyprodukuje	vyprodukovat	k5eAaPmIp3nS	vyprodukovat
největší	veliký	k2eAgInSc1d3	veliký
počet	počet	k1gInSc1	počet
filmů	film	k1gInPc2	film
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Filmová	filmový	k2eAgFnSc1d1	filmová
produkce	produkce	k1gFnSc1	produkce
má	mít	k5eAaImIp3nS	mít
svou	svůj	k3xOyFgFnSc4	svůj
základnu	základna	k1gFnSc4	základna
v	v	k7c6	v
Bombaji	Bombaj	k1gFnSc6	Bombaj
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Bollywood	Bollywood	k1gInSc1	Bollywood
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Indická	indický	k2eAgFnSc1d1	indická
hudba	hudba	k1gFnSc1	hudba
je	být	k5eAaImIp3nS	být
reprezentována	reprezentovat	k5eAaImNgFnS	reprezentovat
širokým	široký	k2eAgNnSc7d1	široké
spektrem	spektrum	k1gNnSc7	spektrum
forem	forma	k1gFnPc2	forma
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc1	dva
hlavní	hlavní	k2eAgInPc1d1	hlavní
proudy	proud	k1gInPc1	proud
klasické	klasický	k2eAgFnSc2d1	klasická
hudby	hudba	k1gFnSc2	hudba
jsou	být	k5eAaImIp3nP	být
karnátacká	karnátacký	k2eAgFnSc1d1	karnátacký
z	z	k7c2	z
Jižní	jižní	k2eAgFnSc2d1	jižní
Indie	Indie	k1gFnSc2	Indie
a	a	k8xC	a
hindustanská	hindustanský	k2eAgFnSc1d1	hindustanský
ze	z	k7c2	z
Severní	severní	k2eAgFnSc2d1	severní
Indie	Indie	k1gFnSc2	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Populární	populární	k2eAgFnPc1d1	populární
formy	forma	k1gFnPc1	forma
hudby	hudba	k1gFnSc2	hudba
také	také	k9	také
převládají	převládat	k5eAaImIp3nP	převládat
<g/>
,	,	kIx,	,
za	za	k7c4	za
zmínku	zmínka	k1gFnSc4	zmínka
stojí	stát	k5eAaImIp3nS	stát
například	například	k6eAd1	například
filmová	filmový	k2eAgFnSc1d1	filmová
hudba	hudba	k1gFnSc1	hudba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
také	také	k9	také
existuje	existovat	k5eAaImIp3nS	existovat
množství	množství	k1gNnSc1	množství
druhů	druh	k1gInPc2	druh
klasických	klasický	k2eAgInPc2d1	klasický
tanců	tanec	k1gInPc2	tanec
včetně	včetně	k7c2	včetně
takových	takový	k3xDgFnPc2	takový
jako	jako	k9	jako
Bharata	Bharata	k1gFnSc1	Bharata
Natyam	Natyam	k1gInSc1	Natyam
<g/>
,	,	kIx,	,
Kathakali	Kathakali	k1gMnSc1	Kathakali
a	a	k8xC	a
Kathak	Kathak	k1gMnSc1	Kathak
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
mají	mít	k5eAaImIp3nP	mít
formu	forma	k1gFnSc4	forma
příběhu	příběh	k1gInSc2	příběh
a	a	k8xC	a
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
duchovní	duchovní	k2eAgInPc4d1	duchovní
a	a	k8xC	a
náboženské	náboženský	k2eAgInPc4d1	náboženský
prvky	prvek	k1gInPc4	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Indie	Indie	k1gFnSc1	Indie
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
rozmanité	rozmanitý	k2eAgInPc4d1	rozmanitý
festivaly	festival	k1gInPc4	festival
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
je	být	k5eAaImIp3nS	být
mnoho	mnoho	k4c1	mnoho
oslavovaných	oslavovaný	k2eAgMnPc2d1	oslavovaný
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
kastu	kasta	k1gFnSc4	kasta
a	a	k8xC	a
vyznání	vyznání	k1gNnSc4	vyznání
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnPc1d3	nejznámější
a	a	k8xC	a
nejpopulárnější	populární	k2eAgFnPc1d3	nejpopulárnější
oslavy	oslava	k1gFnPc1	oslava
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
hinduistické	hinduistický	k2eAgInPc4d1	hinduistický
festivaly	festival	k1gInPc4	festival
Diwali	Diwali	k1gFnSc3	Diwali
<g/>
,	,	kIx,	,
Holi	hole	k1gFnSc3	hole
a	a	k8xC	a
Dussehra	Dussehra	k1gFnSc1	Dussehra
a	a	k8xC	a
muslimské	muslimský	k2eAgFnPc1d1	muslimská
oslavy	oslava	k1gFnPc1	oslava
Eid	Eid	k1gFnSc2	Eid
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgInSc1d1	národní
sport	sport	k1gInSc1	sport
Indie	Indie	k1gFnSc2	Indie
je	být	k5eAaImIp3nS	být
pozemní	pozemní	k2eAgInSc4d1	pozemní
hokej	hokej	k1gInSc4	hokej
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nyní	nyní	k6eAd1	nyní
je	být	k5eAaImIp3nS	být
i	i	k9	i
přesto	přesto	k8xC	přesto
velmi	velmi	k6eAd1	velmi
populárním	populární	k2eAgInSc7d1	populární
sportem	sport	k1gInSc7	sport
kriket	kriket	k1gInSc1	kriket
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nejpopulárnějším	populární	k2eAgInSc7d3	nejpopulárnější
sportem	sport	k1gInSc7	sport
fotbal	fotbal	k1gInSc1	fotbal
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
široce	široko	k6eAd1	široko
sledován	sledovat	k5eAaImNgInS	sledovat
i	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Indie	Indie	k1gFnSc1	Indie
umístila	umístit	k5eAaPmAgFnS	umístit
mimo	mimo	k7c4	mimo
top	topit	k5eAaImRp2nS	topit
100	[number]	k4	100
národních	národní	k2eAgInPc2d1	národní
týmů	tým	k1gInPc2	tým
ve	v	k7c6	v
světovém	světový	k2eAgInSc6d1	světový
žebříčku	žebříček	k1gInSc6	žebříček
FIFA	FIFA	kA	FIFA
<g/>
.	.	kIx.	.
</s>
<s>
Indie	Indie	k1gFnSc1	Indie
mě	já	k3xPp1nSc4	já
také	také	k9	také
silné	silný	k2eAgNnSc4d1	silné
zastoupení	zastoupení	k1gNnSc4	zastoupení
v	v	k7c6	v
šachu	šach	k1gInSc6	šach
s	s	k7c7	s
několika	několik	k4yIc7	několik
hráči	hráč	k1gMnPc7	hráč
s	s	k7c7	s
mezinárodním	mezinárodní	k2eAgInSc7d1	mezinárodní
statusem	status	k1gInSc7	status
včetně	včetně	k7c2	včetně
Višvanáthana	Višvanáthan	k1gMnSc2	Višvanáthan
Ánanda	Ánand	k1gMnSc2	Ánand
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
šampionem	šampion	k1gMnSc7	šampion
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
populární	populární	k2eAgInPc1d1	populární
sporty	sport	k1gInPc1	sport
jsou	být	k5eAaImIp3nP	být
kulečník	kulečník	k1gInSc1	kulečník
a	a	k8xC	a
střelba	střelba	k1gFnSc1	střelba
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
jednou	jeden	k4xCgFnSc7	jeden
stříbrnou	stříbrná	k1gFnSc7	stříbrná
a	a	k8xC	a
dvěma	dva	k4xCgFnPc7	dva
bronzovými	bronzový	k2eAgFnPc7d1	bronzová
medailemi	medaile	k1gFnPc7	medaile
na	na	k7c6	na
posledních	poslední	k2eAgFnPc6d1	poslední
třech	tři	k4xCgFnPc6	tři
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
má	mít	k5eAaImIp3nS	mít
India	indium	k1gNnPc4	indium
jen	jen	k9	jen
málo	málo	k4c4	málo
sportovních	sportovní	k2eAgInPc2d1	sportovní
úspěchů	úspěch	k1gInPc2	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
osmkrát	osmkrát	k6eAd1	osmkrát
zlatou	zlatý	k2eAgFnSc4d1	zlatá
medaili	medaile	k1gFnSc4	medaile
v	v	k7c6	v
pozemním	pozemní	k2eAgInSc6d1	pozemní
hokeji	hokej	k1gInSc6	hokej
<g/>
.	.	kIx.	.
</s>
<s>
Tradiční	tradiční	k2eAgInPc1d1	tradiční
domorodé	domorodý	k2eAgInPc1d1	domorodý
sporty	sport	k1gInPc1	sport
jsou	být	k5eAaImIp3nP	být
polo	polo	k6eAd1	polo
<g/>
,	,	kIx,	,
kabadi	kabad	k1gMnPc1	kabad
a	a	k8xC	a
gilli-danda	gillianda	k1gFnSc1	gilli-danda
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
hrají	hrát	k5eAaImIp3nP	hrát
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Šachy	šach	k1gInPc4	šach
a	a	k8xC	a
badminton	badminton	k1gInSc4	badminton
se	se	k3xPyFc4	se
považují	považovat	k5eAaImIp3nP	považovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Kulečník	kulečník	k1gInSc4	kulečník
a	a	k8xC	a
badminton	badminton	k1gInSc4	badminton
přinesly	přinést	k5eAaPmAgInP	přinést
Indii	Indie	k1gFnSc4	Indie
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
úspěch	úspěch	k1gInSc4	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Formule	formule	k1gFnSc1	formule
1	[number]	k4	1
se	se	k3xPyFc4	se
také	také	k9	také
těší	těšit	k5eAaImIp3nS	těšit
popularitě	popularita	k1gFnSc3	popularita
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
jen	jen	k9	jen
v	v	k7c6	v
městských	městský	k2eAgFnPc6d1	městská
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Indická	indický	k2eAgFnSc1d1	indická
kuchyně	kuchyně	k1gFnSc1	kuchyně
je	být	k5eAaImIp3nS	být
neobyčejně	obyčejně	k6eNd1	obyčejně
rozmanitá	rozmanitý	k2eAgFnSc1d1	rozmanitá
<g/>
;	;	kIx,	;
ingredience	ingredience	k1gFnSc1	ingredience
<g/>
,	,	kIx,	,
koření	koření	k1gNnSc1	koření
a	a	k8xC	a
kuchařské	kuchařský	k2eAgFnPc1d1	kuchařská
metody	metoda	k1gFnPc1	metoda
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
každým	každý	k3xTgInSc7	každý
regionem	region	k1gInSc7	region
<g/>
.	.	kIx.	.
</s>
<s>
Rýže	rýže	k1gFnSc1	rýže
a	a	k8xC	a
pšenice	pšenice	k1gFnSc1	pšenice
tvoří	tvořit	k5eAaImIp3nS	tvořit
základní	základní	k2eAgFnSc4d1	základní
potravinovou	potravinový	k2eAgFnSc4d1	potravinová
složku	složka	k1gFnSc4	složka
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Indie	Indie	k1gFnSc1	Indie
je	být	k5eAaImIp3nS	být
také	také	k9	také
pozoruhodná	pozoruhodný	k2eAgFnSc1d1	pozoruhodná
pro	pro	k7c4	pro
pestrost	pestrost	k1gFnSc4	pestrost
vegetariánské	vegetariánský	k2eAgFnSc2d1	vegetariánská
a	a	k8xC	a
nevegetariánské	vegetariánský	k2eNgFnSc2d1	nevegetariánská
kuchyně	kuchyně	k1gFnSc2	kuchyně
<g/>
.	.	kIx.	.
</s>
<s>
Kořeněná	kořeněný	k2eAgNnPc4d1	kořeněné
jídla	jídlo	k1gNnPc4	jídlo
a	a	k8xC	a
sladkosti	sladkost	k1gFnPc4	sladkost
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgFnPc1d1	populární
<g/>
.	.	kIx.	.
</s>
