<s>
Eliška	Eliška	k1gFnSc1
je	být	k5eAaImIp3nS
ženské	ženský	k2eAgNnSc1d1
rodné	rodný	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
pocházející	pocházející	k2eAgNnSc4d1
z	z	k7c2
hebrejského	hebrejský	k2eAgInSc2d1
eliševah	eliševah	k1gFnPc2
(	(	kIx(
<g/>
א	א	kIx~
<g/>
ֱ	ֱ	kIx~
<g/>
ל	ל	kIx~
<g/>
ִ	ִ	kIx~
<g/>
י	י	kIx~
<g/>
ֶ	ֶ	kIx~
<g/>
ׁ	ׁ	kIx~
<g/>
ב	ב	kIx~
<g/>
ַ	ַ	kIx~
<g/>
ע	ע	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
což	což	k3yRnSc1
znamená	znamenat	k5eAaImIp3nS
„	„	kIx"
<g/>
Bůh	bůh	k1gMnSc1
je	on	k3xPp3gInPc4
má	mít	k5eAaImIp3nS
přísaha	přísaha	k1gFnSc1
(	(	kIx(
<g/>
věřím	věřit	k5eAaImIp1nS
<g/>
)	)	kIx)
<g/>
“	“	kIx"
<g/>
.	.	kIx.
</s>