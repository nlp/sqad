<s>
Sémiotika	sémiotika	k1gFnSc1	sémiotika
(	(	kIx(	(
<g/>
z	z	k7c2	z
řec.	řec.	k?	řec.
σ	σ	k?	σ
sémeion	sémeion	k1gInSc1	sémeion
<g/>
,	,	kIx,	,
znak	znak	k1gInSc1	znak
<g/>
,	,	kIx,	,
označení	označení	k1gNnSc1	označení
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
znakových	znakový	k2eAgInPc6d1	znakový
systémech	systém	k1gInPc6	systém
<g/>
.	.	kIx.	.
</s>
<s>
Oblastí	oblast	k1gFnSc7	oblast
jejího	její	k3xOp3gInSc2	její
zájmu	zájem	k1gInSc2	zájem
nejsou	být	k5eNaImIp3nP	být
jen	jen	k9	jen
jazykové	jazykový	k2eAgInPc4d1	jazykový
znaky	znak	k1gInPc4	znak
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obecně	obecně	k6eAd1	obecně
i	i	k9	i
všechny	všechen	k3xTgInPc1	všechen
ostatní	ostatní	k2eAgInPc1d1	ostatní
znakové	znakový	k2eAgInPc1d1	znakový
systémy	systém	k1gInPc1	systém
(	(	kIx(	(
<g/>
piktogramy	piktogram	k1gInPc1	piktogram
<g/>
,	,	kIx,	,
dopravní	dopravní	k2eAgFnPc1d1	dopravní
značky	značka	k1gFnPc1	značka
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastní	k2eAgInPc1d1	vlastní
znakové	znakový	k2eAgInPc1d1	znakový
systémy	systém	k1gInPc1	systém
s	s	k7c7	s
pravidly	pravidlo	k1gNnPc7	pravidlo
gramatickými	gramatický	k2eAgNnPc7d1	gramatické
i	i	k9	i
sémantickými	sémantický	k2eAgFnPc7d1	sémantická
mají	mít	k5eAaImIp3nP	mít
mj.	mj.	kA	mj.
umělecké	umělecký	k2eAgInPc1d1	umělecký
obory	obor	k1gInPc1	obor
<g/>
,	,	kIx,	,
náboženství	náboženství	k1gNnPc1	náboženství
<g/>
,	,	kIx,	,
hry	hra	k1gFnPc1	hra
nebo	nebo	k8xC	nebo
rituály	rituál	k1gInPc1	rituál
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
zakladatele	zakladatel	k1gMnSc2	zakladatel
moderní	moderní	k2eAgFnSc2d1	moderní
sémiotiky	sémiotika	k1gFnSc2	sémiotika
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
americký	americký	k2eAgMnSc1d1	americký
filosof	filosof	k1gMnSc1	filosof
Charles	Charles	k1gMnSc1	Charles
Peirce	Peirce	k1gMnSc1	Peirce
(	(	kIx(	(
<g/>
1839	[number]	k4	1839
<g/>
–	–	k?	–
<g/>
1914	[number]	k4	1914
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
rozdělil	rozdělit	k5eAaPmAgInS	rozdělit
znaky	znak	k1gInPc4	znak
na	na	k7c4	na
ikony	ikona	k1gFnPc4	ikona
<g/>
,	,	kIx,	,
indexy	index	k1gInPc4	index
a	a	k8xC	a
symboly	symbol	k1gInPc4	symbol
<g/>
.	.	kIx.	.
</s>
<s>
Sémiotika	sémiotika	k1gFnSc1	sémiotika
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
obecně	obecně	k6eAd1	obecně
uznávaného	uznávaný	k2eAgNnSc2d1	uznávané
rozdělení	rozdělení	k1gNnSc2	rozdělení
Charlese	Charles	k1gMnSc2	Charles
Morrise	Morrise	k1gFnSc2	Morrise
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c4	na
sémantiku	sémantika	k1gFnSc4	sémantika
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
významem	význam	k1gInSc7	význam
znaků	znak	k1gInPc2	znak
syntaktiku	syntaktika	k1gFnSc4	syntaktika
<g/>
,	,	kIx,	,
syntax	syntax	k1gFnSc4	syntax
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
vzájemné	vzájemný	k2eAgInPc4d1	vzájemný
vztahy	vztah	k1gInPc4	vztah
mezi	mezi	k7c7	mezi
znaky	znak	k1gInPc7	znak
pragmatiku	pragmatik	k1gMnSc3	pragmatik
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
náplní	náplň	k1gFnSc7	náplň
je	být	k5eAaImIp3nS	být
užívání	užívání	k1gNnSc1	užívání
znaků	znak	k1gInPc2	znak
<g/>
,	,	kIx,	,
vztahy	vztah	k1gInPc4	vztah
mezi	mezi	k7c7	mezi
znaky	znak	k1gInPc7	znak
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc7	jejich
uživateli	uživatel	k1gMnPc7	uživatel
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
de	de	k?	de
Saussure	Saussur	k1gMnSc5	Saussur
<g/>
,	,	kIx,	,
považovaný	považovaný	k2eAgInSc4d1	považovaný
za	za	k7c2	za
otce	otec	k1gMnSc2	otec
moderní	moderní	k2eAgFnSc2d1	moderní
lingvistiky	lingvistika	k1gFnSc2	lingvistika
<g/>
,	,	kIx,	,
používal	používat	k5eAaImAgMnS	používat
pro	pro	k7c4	pro
sémiotiku	sémiotika	k1gFnSc4	sémiotika
termínu	termín	k1gInSc2	termín
sémiologie	sémiologie	k1gFnSc2	sémiologie
a	a	k8xC	a
její	její	k3xOp3gInSc1	její
náplň	náplň	k1gFnSc4	náplň
definoval	definovat	k5eAaBmAgInS	definovat
jako	jako	k9	jako
zkoumání	zkoumání	k1gNnSc4	zkoumání
"	"	kIx"	"
<g/>
života	život	k1gInSc2	život
znaků	znak	k1gInPc2	znak
v	v	k7c6	v
lidské	lidský	k2eAgFnSc6d1	lidská
společnosti	společnost	k1gFnSc6	společnost
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
znakových	znakový	k2eAgInPc2d1	znakový
systémů	systém	k1gInPc2	systém
zahrnoval	zahrnovat	k5eAaImAgInS	zahrnovat
mj.	mj.	kA	mj.
i	i	k8xC	i
Morseovu	Morseův	k2eAgFnSc4d1	Morseova
abecedu	abeceda	k1gFnSc4	abeceda
<g/>
,	,	kIx,	,
znakový	znakový	k2eAgInSc4d1	znakový
jazyk	jazyk	k1gInSc4	jazyk
neslyšících	slyšící	k2eNgInPc2d1	Neslyšící
nebo	nebo	k8xC	nebo
hru	hra	k1gFnSc4	hra
v	v	k7c4	v
šachy	šach	k1gInPc4	šach
<g/>
,	,	kIx,	,
pozdější	pozdní	k2eAgMnPc1d2	pozdější
strukturalisté	strukturalista	k1gMnPc1	strukturalista
připojili	připojit	k5eAaPmAgMnP	připojit
řadu	řada	k1gFnSc4	řada
dalších	další	k2eAgFnPc2d1	další
(	(	kIx(	(
<g/>
těsnopis	těsnopis	k1gInSc1	těsnopis
<g/>
,	,	kIx,	,
lidové	lidový	k2eAgInPc1d1	lidový
kroje	kroj	k1gInPc1	kroj
ad	ad	k7c4	ad
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lingvistika	lingvistika	k1gFnSc1	lingvistika
pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
disciplínou	disciplína	k1gFnSc7	disciplína
sémiologii	sémiologie	k1gFnSc4	sémiologie
podřazenou	podřazený	k2eAgFnSc7d1	podřazená
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
jako	jako	k9	jako
věda	věda	k1gFnSc1	věda
zabývá	zabývat	k5eAaImIp3nS	zabývat
jen	jen	k9	jen
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
znakových	znakový	k2eAgInPc2d1	znakový
systémů	systém	k1gInPc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Pojmu	pojmout	k5eAaPmIp1nS	pojmout
"	"	kIx"	"
<g/>
sémiologie	sémiologie	k1gFnSc1	sémiologie
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
jako	jako	k9	jako
synonymu	synonymum	k1gNnSc6	synonymum
k	k	k7c3	k
sémiotice	sémiotika	k1gFnSc3	sémiotika
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
státech	stát	k1gInPc6	stát
dává	dávat	k5eAaImIp3nS	dávat
přednost	přednost	k1gFnSc4	přednost
dodnes	dodnes	k6eAd1	dodnes
(	(	kIx(	(
<g/>
např.	např.	kA	např.
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roland	Roland	k1gInSc1	Roland
Barthes	Barthesa	k1gFnPc2	Barthesa
Umberto	Umberta	k1gFnSc5	Umberta
Eco	Eco	k1gFnSc1	Eco
Algirdas	Algirdas	k1gInSc4	Algirdas
Julien	Julien	k2eAgInSc1d1	Julien
Greimas	Greimas	k1gInSc1	Greimas
Groupe	Group	k1gInSc5	Group
μ	μ	k?	μ
Louis	Louis	k1gMnSc1	Louis
Hjelmslev	Hjelmslev	k1gMnSc1	Hjelmslev
Jurij	Jurij	k1gMnSc1	Jurij
Michajlovič	Michajlovič	k1gMnSc1	Michajlovič
Lotman	Lotman	k1gMnSc1	Lotman
Charles	Charles	k1gMnSc1	Charles
W.	W.	kA	W.
Morris	Morris	k1gFnPc1	Morris
Ivo	Ivo	k1gMnSc1	Ivo
Osolsobě	Osolsobě	k1gMnSc1	Osolsobě
Charles	Charles	k1gMnSc1	Charles
Peirce	Peirce	k1gMnSc1	Peirce
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
de	de	k?	de
Saussure	Saussur	k1gMnSc5	Saussur
Thomas	Thomas	k1gMnSc1	Thomas
A.	A.	kA	A.
Sebeok	Sebeok	k1gInSc1	Sebeok
Valentin	Valentina	k1gFnPc2	Valentina
Vološinov	Vološinovo	k1gNnPc2	Vološinovo
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
sémiotika	sémiotika	k1gFnSc1	sémiotika
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
sémiotika	sémiotika	k1gFnSc1	sémiotika
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Sémiotika	sémiotika	k1gFnSc1	sémiotika
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
terminologické	terminologický	k2eAgFnSc6d1	terminologická
databázi	databáze	k1gFnSc6	databáze
knihovnictví	knihovnictví	k1gNnSc2	knihovnictví
a	a	k8xC	a
informační	informační	k2eAgFnSc2d1	informační
vědy	věda	k1gFnSc2	věda
(	(	kIx(	(
<g/>
TDKIV	TDKIV	kA	TDKIV
<g/>
)	)	kIx)	)
</s>
