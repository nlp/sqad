<s>
Sémiotika	sémiotika	k1gFnSc1
se	se	k3xPyFc4
podle	podle	k7c2
obecně	obecně	k6eAd1
uznávaného	uznávaný	k2eAgNnSc2d1
rozdělení	rozdělení	k1gNnSc2
Charlese	Charles	k1gMnSc2
Morrise	Morrise	k1gFnSc2
dělí	dělit	k5eAaImIp3nP
na	na	k7c4
sémantiku	sémantika	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1
se	se	k3xPyFc4
zabývá	zabývat	k5eAaImIp3nS
významem	význam	k1gInSc7
znaků	znak	k1gInPc2
syntaktiku	syntaktika	k1gFnSc4
<g/>
,	,	kIx,
syntax	syntax	k1gFnSc4
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
zkoumá	zkoumat	k5eAaImIp3nS
vzájemné	vzájemný	k2eAgInPc4d1
vztahy	vztah	k1gInPc4
mezi	mezi	k7c7
znaky	znak	k1gInPc7
pragmatiku	pragmatik	k1gMnSc3
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc7
náplní	náplň	k1gFnSc7
je	být	k5eAaImIp3nS
užívání	užívání	k1gNnSc1
znaků	znak	k1gInPc2
<g/>
,	,	kIx,
vztahy	vztah	k1gInPc4
mezi	mezi	k7c7
znaky	znak	k1gInPc7
a	a	k8xC
jejich	jejich	k3xOp3gMnPc7
uživateli	uživatel	k1gMnPc7
Ferdinand	Ferdinand	k1gMnSc1
de	de	k?
Saussure	Saussur	k1gMnSc5
<g/>
,	,	kIx,
považovaný	považovaný	k2eAgInSc4d1
za	za	k7c2
otce	otec	k1gMnSc2
moderní	moderní	k2eAgFnSc2d1
lingvistiky	lingvistika	k1gFnSc2
<g/>
,	,	kIx,
používal	používat	k5eAaImAgMnS
pro	pro	k7c4
sémiotiku	sémiotika	k1gFnSc4
termínu	termín	k1gInSc2
sémiologie	sémiologie	k1gFnSc2
a	a	k8xC
její	její	k3xOp3gInSc1
náplň	náplň	k1gFnSc4
definoval	definovat	k5eAaBmAgInS
jako	jako	k9
zkoumání	zkoumání	k1gNnSc4
"	"	kIx"
<g/>
života	život	k1gInSc2
znaků	znak	k1gInPc2
v	v	k7c6
lidské	lidský	k2eAgFnSc6d1
společnosti	společnost	k1gFnSc6
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>