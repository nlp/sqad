<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
má	mít	k5eAaImIp3nS	mít
podobu	podoba	k1gFnSc4	podoba
červeného	červený	k2eAgInSc2d1	červený
čtvercového	čtvercový	k2eAgInSc2d1	čtvercový
listu	list	k1gInSc2	list
s	s	k7c7	s
bílým	bílý	k2eAgInSc7d1	bílý
rovnoramenným	rovnoramenný	k2eAgInSc7d1	rovnoramenný
křížem	kříž	k1gInSc7	kříž
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
<g/>
.	.	kIx.	.
</s>
<s>
Švýcarská	švýcarský	k2eAgFnSc1d1	švýcarská
vlajka	vlajka	k1gFnSc1	vlajka
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
čtvercových	čtvercový	k2eAgFnPc2d1	čtvercová
vlajek	vlajka	k1gFnPc2	vlajka
států	stát	k1gInPc2	stát
–	–	k?	–
tou	ten	k3xDgFnSc7	ten
druhou	druhý	k4xOgFnSc7	druhý
je	být	k5eAaImIp3nS	být
vlajka	vlajka	k1gFnSc1	vlajka
Vatikánu	Vatikán	k1gInSc2	Vatikán
<g/>
.	.	kIx.	.
</s>
<s>
Formálně	formálně	k6eAd1	formálně
je	být	k5eAaImIp3nS	být
stanovena	stanovit	k5eAaPmNgFnS	stanovit
pouze	pouze	k6eAd1	pouze
podoba	podoba	k1gFnSc1	podoba
kříže	kříž	k1gInSc2	kříž
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1889	[number]	k4	1889
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Znakem	znak	k1gInSc7	znak
Federace	federace	k1gFnSc2	federace
je	být	k5eAaImIp3nS	být
kolmý	kolmý	k2eAgInSc1d1	kolmý
bílý	bílý	k2eAgInSc1d1	bílý
kříž	kříž	k1gInSc1	kříž
v	v	k7c6	v
červeném	červený	k2eAgNnSc6d1	červené
poli	pole	k1gNnSc6	pole
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgNnSc2	jenž
ramena	rameno	k1gNnSc2	rameno
mají	mít	k5eAaImIp3nP	mít
délku	délka	k1gFnSc4	délka
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
šestinu	šestina	k1gFnSc4	šestina
delší	dlouhý	k2eAgNnSc4d2	delší
jež	jenž	k3xRgNnSc1	jenž
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gFnSc1	jejich
šířka	šířka	k1gFnSc1	šířka
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Federální	federální	k2eAgNnSc1d1	federální
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
z	z	k7c2	z
12	[number]	k4	12
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1889	[number]	k4	1889
o	o	k7c6	o
podobě	podoba	k1gFnSc6	podoba
státního	státní	k2eAgInSc2d1	státní
znaku	znak	k1gInSc2	znak
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rozměr	rozměr	k1gInSc1	rozměr
kříže	kříž	k1gInSc2	kříž
na	na	k7c6	na
vlajce	vlajka	k1gFnSc6	vlajka
nebo	nebo	k8xC	nebo
na	na	k7c6	na
štítu	štít	k1gInSc6	štít
není	být	k5eNaImIp3nS	být
formálně	formálně	k6eAd1	formálně
stanoven	stanovit	k5eAaPmNgInS	stanovit
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
reálně	reálně	k6eAd1	reálně
užívá	užívat	k5eAaImIp3nS	užívat
poměr	poměr	k1gInSc1	poměr
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
nebo	nebo	k8xC	nebo
7	[number]	k4	7
<g/>
:	:	kIx,	:
<g/>
10	[number]	k4	10
vůči	vůči	k7c3	vůči
rozměru	rozměr	k1gInSc3	rozměr
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
přesný	přesný	k2eAgInSc1d1	přesný
odstín	odstín	k1gInSc1	odstín
červené	červený	k2eAgFnSc2d1	červená
barvy	barva	k1gFnSc2	barva
není	být	k5eNaImIp3nS	být
určen	určit	k5eAaPmNgInS	určit
<g/>
.	.	kIx.	.
</s>
<s>
Švýcarská	švýcarský	k2eAgFnSc1d1	švýcarská
národní	národní	k2eAgFnSc1d1	národní
vlajka	vlajka	k1gFnSc1	vlajka
byla	být	k5eAaImAgFnS	být
inspirována	inspirovat	k5eAaBmNgFnS	inspirovat
podobou	podoba	k1gFnSc7	podoba
vlajky	vlajka	k1gFnSc2	vlajka
Kantonu	Kanton	k1gInSc2	Kanton
Schwyz	Schwyz	k1gInSc1	Schwyz
<g/>
.	.	kIx.	.
</s>
<s>
Švýcarské	švýcarský	k2eAgFnPc1d1	švýcarská
lodě	loď	k1gFnPc1	loď
užívají	užívat	k5eAaImIp3nP	užívat
námořní	námořní	k2eAgFnSc4d1	námořní
vlajku	vlajka	k1gFnSc4	vlajka
s	s	k7c7	s
tradičními	tradiční	k2eAgInPc7d1	tradiční
rozměry	rozměr	k1gInPc7	rozměr
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Symbol	symbol	k1gInSc1	symbol
červeného	červený	k2eAgInSc2d1	červený
kříže	kříž	k1gInSc2	kříž
<g/>
,	,	kIx,	,
užívaný	užívaný	k2eAgInSc1d1	užívaný
Mezinárodním	mezinárodní	k2eAgInSc7d1	mezinárodní
výborem	výbor	k1gInSc7	výbor
Červeného	Červeného	k2eAgInSc2d1	Červeného
kříže	kříž	k1gInSc2	kříž
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
švýcarské	švýcarský	k2eAgFnSc2d1	švýcarská
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Červený	červený	k2eAgInSc1d1	červený
kříž	kříž	k1gInSc1	kříž
v	v	k7c6	v
bílém	bílý	k2eAgNnSc6d1	bílé
poli	pole	k1gNnSc6	pole
byl	být	k5eAaImAgMnS	být
prohlášen	prohlásit	k5eAaPmNgMnS	prohlásit
za	za	k7c4	za
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
ochranný	ochranný	k2eAgInSc4d1	ochranný
symbol	symbol	k1gInSc4	symbol
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1864	[number]	k4	1864
Ženevskou	ženevský	k2eAgFnSc7d1	Ženevská
úmluvou	úmluva	k1gFnSc7	úmluva
o	o	k7c4	o
zlepšení	zlepšení	k1gNnSc4	zlepšení
osudu	osud	k1gInSc2	osud
raněných	raněný	k1gMnPc2	raněný
a	a	k8xC	a
nemocných	mocný	k2eNgMnPc2d1	nemocný
příslušníků	příslušník	k1gMnPc2	příslušník
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
v	v	k7c6	v
poli	pole	k1gNnSc6	pole
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
podoba	podoba	k1gFnSc1	podoba
byla	být	k5eAaImAgFnS	být
přijata	přijmout	k5eAaPmNgFnS	přijmout
jako	jako	k9	jako
pocta	pocta	k1gFnSc1	pocta
zakladateli	zakladatel	k1gMnSc3	zakladatel
MVČK	MVČK	kA	MVČK
Švýcaru	Švýcar	k1gMnSc3	Švýcar
Henry	Henry	k1gMnSc1	Henry
Dunantovi	Dunant	k1gMnSc3	Dunant
<g/>
.	.	kIx.	.
</s>
<s>
Vlajky	vlajka	k1gFnPc1	vlajka
všech	všecek	k3xTgMnPc2	všecek
šestadvaceti	šestadvacet	k4xCc2	šestadvacet
švýcarských	švýcarský	k2eAgInPc2d1	švýcarský
kantonů	kanton	k1gInPc2	kanton
mají	mít	k5eAaImIp3nP	mít
poměr	poměr	k1gInSc4	poměr
stran	strana	k1gFnPc2	strana
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
na	na	k7c4	na
několik	několik	k4yIc4	několik
drobných	drobný	k2eAgFnPc2d1	drobná
výjimek	výjimka	k1gFnPc2	výjimka
jsou	být	k5eAaImIp3nP	být
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
tvaru	tvar	k1gInSc2	tvar
<g/>
)	)	kIx)	)
shodné	shodný	k2eAgInPc4d1	shodný
se	s	k7c7	s
znaky	znak	k1gInPc7	znak
kantonů	kanton	k1gInPc2	kanton
<g/>
.	.	kIx.	.
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc4	Švýcarsko
Švýcarská	švýcarský	k2eAgFnSc1d1	švýcarská
hymna	hymna	k1gFnSc1	hymna
<g/>
,	,	kIx,	,
Švýcarský	švýcarský	k2eAgInSc1d1	švýcarský
znak	znak	k1gInSc1	znak
</s>
