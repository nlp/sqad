<s>
Petřínská	petřínský	k2eAgFnSc1d1	Petřínská
rozhledna	rozhledna	k1gFnSc1	rozhledna
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
výškou	výška	k1gFnSc7	výška
přes	přes	k7c4	přes
65,5	[number]	k4	65,5
metrů	metr	k1gInPc2	metr
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgFnPc2d3	nejznámější
dominant	dominanta	k1gFnPc2	dominanta
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
základna	základna	k1gFnSc1	základna
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
324	[number]	k4	324
m	m	kA	m
<g/>
,	,	kIx,	,
v	v	k7c6	v
zahradě	zahrada	k1gFnSc6	zahrada
U	u	k7c2	u
rozhledny	rozhledna	k1gFnSc2	rozhledna
na	na	k7c6	na
temeni	temeno	k1gNnSc6	temeno
kopce	kopec	k1gInSc2	kopec
Petřín	Petřín	k1gInSc1	Petřín
na	na	k7c6	na
Malé	Malé	k2eAgFnSc6d1	Malé
Straně	strana	k1gFnSc6	strana
v	v	k7c6	v
městské	městský	k2eAgFnSc6d1	městská
části	část	k1gFnSc6	část
Praha	Praha	k1gFnSc1	Praha
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
r.	r.	kA	r.
1889	[number]	k4	1889
byli	být	k5eAaImAgMnP	být
členové	člen	k1gMnPc1	člen
Klubu	klub	k1gInSc2	klub
českých	český	k2eAgMnPc2d1	český
turistů	turist	k1gMnPc2	turist
na	na	k7c6	na
světové	světový	k2eAgFnSc6d1	světová
výstavě	výstava	k1gFnSc6	výstava
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
tak	tak	k9	tak
nadchli	nadchnout	k5eAaPmAgMnP	nadchnout
pohledem	pohled	k1gInSc7	pohled
na	na	k7c4	na
slavnou	slavný	k2eAgFnSc4d1	slavná
Eiffelovu	Eiffelův	k2eAgFnSc4d1	Eiffelova
věž	věž	k1gFnSc4	věž
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
vytvořit	vytvořit	k5eAaPmF	vytvořit
podobnou	podobný	k2eAgFnSc4d1	podobná
dominantu	dominanta	k1gFnSc4	dominanta
nad	nad	k7c7	nad
městem	město	k1gNnSc7	město
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Založili	založit	k5eAaPmAgMnP	založit
Družstvo	družstvo	k1gNnSc4	družstvo
pro	pro	k7c4	pro
výstavbu	výstavba	k1gFnSc4	výstavba
Petřínské	petřínský	k2eAgFnSc2d1	Petřínská
rozhledny	rozhledna	k1gFnSc2	rozhledna
<g/>
,	,	kIx,	,
dali	dát	k5eAaPmAgMnP	dát
do	do	k7c2	do
něj	on	k3xPp3gMnSc2	on
první	první	k4xOgInPc4	první
peníze	peníz	k1gInPc4	peníz
a	a	k8xC	a
od	od	k7c2	od
magistrátu	magistrát	k1gInSc2	magistrát
získali	získat	k5eAaPmAgMnP	získat
pozemek	pozemek	k1gInSc4	pozemek
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
pětkrát	pětkrát	k6eAd1	pětkrát
menší	malý	k2eAgFnSc4d2	menší
napodobeninu	napodobenina	k1gFnSc4	napodobenina
Eiffelovy	Eiffelův	k2eAgFnSc2d1	Eiffelova
věže	věž	k1gFnSc2	věž
byl	být	k5eAaImAgInS	být
vybrán	vybrán	k2eAgInSc1d1	vybrán
vrch	vrch	k1gInSc1	vrch
Petřín	Petřín	k1gInSc1	Petřín
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
318	[number]	k4	318
m.	m.	k?	m.
V	v	k7c6	v
r.	r.	kA	r.
1890	[number]	k4	1890
byla	být	k5eAaImAgFnS	být
provedena	provést	k5eAaPmNgFnS	provést
projektová	projektový	k2eAgFnSc1d1	projektová
příprava	příprava	k1gFnSc1	příprava
a	a	k8xC	a
zajištěny	zajištěn	k2eAgInPc1d1	zajištěn
potřebné	potřebný	k2eAgInPc1d1	potřebný
prostředky	prostředek	k1gInPc1	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1891	[number]	k4	1891
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
arch	archa	k1gFnPc2	archa
<g/>
.	.	kIx.	.
</s>
<s>
Vratislava	Vratislava	k1gFnSc1	Vratislava
Pasovského	Pasovského	k2eAgNnPc2d1	Pasovského
<g/>
,	,	kIx,	,
autory	autor	k1gMnPc7	autor
konstrukce	konstrukce	k1gFnSc2	konstrukce
byli	být	k5eAaImAgMnP	být
Ing.	ing.	kA	ing.
František	František	k1gMnSc1	František
Prášil	Prášil	k1gMnSc1	Prášil
a	a	k8xC	a
Ing.	ing.	kA	ing.
Julius	Julius	k1gMnSc1	Julius
Souček	Souček	k1gMnSc1	Souček
z	z	k7c2	z
Českomoravské	českomoravský	k2eAgFnSc2d1	Českomoravská
strojírny	strojírna	k1gFnSc2	strojírna
<g/>
.	.	kIx.	.
</s>
<s>
Rozhledna	rozhledna	k1gFnSc1	rozhledna
byla	být	k5eAaImAgFnS	být
vystavěna	vystavět	k5eAaPmNgFnS	vystavět
pro	pro	k7c4	pro
Zemskou	zemský	k2eAgFnSc4d1	zemská
jubilejní	jubilejní	k2eAgFnSc4d1	jubilejní
výstavu	výstava	k1gFnSc4	výstava
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1891	[number]	k4	1891
na	na	k7c4	na
podnět	podnět	k1gInSc4	podnět
zakladatelů	zakladatel	k1gMnPc2	zakladatel
Klubu	klub	k1gInSc2	klub
českých	český	k2eAgMnPc2d1	český
turistů	turist	k1gMnPc2	turist
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Viléma	Vilém	k1gMnSc2	Vilém
Kurze	kurz	k1gInSc6	kurz
a	a	k8xC	a
Vratislava	Vratislava	k1gFnSc1	Vratislava
Pasovského	Pasovského	k2eAgFnSc1d1	Pasovského
jako	jako	k8xS	jako
volná	volný	k2eAgFnSc1d1	volná
kopie	kopie	k1gFnSc1	kopie
Eiffelovy	Eiffelův	k2eAgFnSc2d1	Eiffelova
věže	věž	k1gFnSc2	věž
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
projektem	projekt	k1gInSc7	projekt
seznámil	seznámit	k5eAaPmAgMnS	seznámit
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Vilém	Vilém	k1gMnSc1	Vilém
Kurz	Kurz	k1gMnSc1	Kurz
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
r.	r.	kA	r.
1890	[number]	k4	1890
českou	český	k2eAgFnSc4d1	Česká
veřejnost	veřejnost	k1gFnSc4	veřejnost
článkem	článek	k1gInSc7	článek
"	"	kIx"	"
<g/>
Rozhledna	rozhledna	k1gFnSc1	rozhledna
na	na	k7c6	na
Petříně	Petřín	k1gInSc6	Petřín
<g/>
,	,	kIx,	,
obrázek	obrázek	k1gInSc1	obrázek
z	z	k7c2	z
blízké	blízký	k2eAgFnSc2d1	blízká
budoucnosti	budoucnost	k1gFnSc2	budoucnost
Prahy	Praha	k1gFnSc2	Praha
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Práce	práce	k1gFnPc1	práce
podle	podle	k7c2	podle
plánů	plán	k1gInPc2	plán
inženýrů	inženýr	k1gMnPc2	inženýr
Františka	František	k1gMnSc2	František
Prášila	Prášil	k1gMnSc2	Prášil
a	a	k8xC	a
Julia	Julius	k1gMnSc2	Julius
Součka	Souček	k1gMnSc2	Souček
byly	být	k5eAaImAgFnP	být
započaty	započat	k2eAgFnPc4d1	započata
16	[number]	k4	16
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
a	a	k8xC	a
dokončeny	dokončen	k2eAgInPc1d1	dokončen
20	[number]	k4	20
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
roku	rok	k1gInSc2	rok
1891	[number]	k4	1891
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
rozhledna	rozhledna	k1gFnSc1	rozhledna
slavnostně	slavnostně	k6eAd1	slavnostně
otevřena	otevřen	k2eAgFnSc1d1	otevřena
<g/>
.	.	kIx.	.
</s>
<s>
5	[number]	k4	5
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
1938	[number]	k4	1938
vypukl	vypuknout	k5eAaPmAgInS	vypuknout
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
rozhledny	rozhledna	k1gFnSc2	rozhledna
požár	požár	k1gInSc1	požár
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc1	jehož
příčinou	příčina	k1gFnSc7	příčina
byl	být	k5eAaImAgInS	být
zřejmě	zřejmě	k6eAd1	zřejmě
elektrický	elektrický	k2eAgInSc1d1	elektrický
zkrat	zkrat	k1gInSc1	zkrat
ve	v	k7c6	v
výtahové	výtahový	k2eAgFnSc6d1	výtahová
kabině	kabina	k1gFnSc6	kabina
<g/>
.	.	kIx.	.
</s>
<s>
Evakuováno	evakuován	k2eAgNnSc1d1	evakuováno
bylo	být	k5eAaImAgNnS	být
sto	sto	k4xCgNnSc1	sto
návštěvníků	návštěvník	k1gMnPc2	návštěvník
rozhledny	rozhledna	k1gFnSc2	rozhledna
<g/>
.	.	kIx.	.
</s>
<s>
Hašení	hašení	k1gNnSc1	hašení
požáru	požár	k1gInSc2	požár
bylo	být	k5eAaImAgNnS	být
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
výšce	výška	k1gFnSc3	výška
rozhledny	rozhledna	k1gFnSc2	rozhledna
komplikované	komplikovaný	k2eAgFnPc1d1	komplikovaná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
půl	půl	k1xP	půl
hodině	hodina	k1gFnSc6	hodina
se	se	k3xPyFc4	se
hasičům	hasič	k1gMnPc3	hasič
podařilo	podařit	k5eAaPmAgNnS	podařit
požár	požár	k1gInSc1	požár
dřevěné	dřevěný	k2eAgFnSc2d1	dřevěná
konstrukce	konstrukce	k1gFnSc2	konstrukce
pod	pod	k7c7	pod
oplechovanou	oplechovaný	k2eAgFnSc7d1	oplechovaná
střechou	střecha	k1gFnSc7	střecha
uhasit	uhasit	k5eAaPmF	uhasit
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
srpna	srpen	k1gInSc2	srpen
1938	[number]	k4	1938
byla	být	k5eAaImAgFnS	být
rozhledna	rozhledna	k1gFnSc1	rozhledna
opravena	opravit	k5eAaPmNgFnS	opravit
a	a	k8xC	a
zpřístupněna	zpřístupněn	k2eAgFnSc1d1	zpřístupněna
veřejnosti	veřejnost	k1gFnSc3	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
návštěvy	návštěva	k1gFnSc2	návštěva
okupované	okupovaný	k2eAgFnSc2d1	okupovaná
Prahy	Praha	k1gFnSc2	Praha
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
až	až	k9	až
16	[number]	k4	16
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
Adolf	Adolf	k1gMnSc1	Adolf
Hitler	Hitler	k1gMnSc1	Hitler
přání	přání	k1gNnSc4	přání
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
Petřínská	petřínský	k2eAgFnSc1d1	Petřínská
rozhledna	rozhledna	k1gFnSc1	rozhledna
zbourána	zbourán	k2eAgFnSc1d1	zbourána
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
údajně	údajně	k6eAd1	údajně
kazila	kazit	k5eAaImAgFnS	kazit
výhled	výhled	k1gInSc4	výhled
z	z	k7c2	z
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Přání	přání	k1gNnSc1	přání
ale	ale	k9	ale
nebylo	být	k5eNaImAgNnS	být
nikdy	nikdy	k6eAd1	nikdy
během	během	k7c2	během
okupace	okupace	k1gFnSc2	okupace
vyhověno	vyhověn	k2eAgNnSc1d1	vyhověno
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1953	[number]	k4	1953
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
rozhledně	rozhledně	k6eAd1	rozhledně
uveden	uvést	k5eAaPmNgMnS	uvést
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
televizní	televizní	k2eAgInSc1d1	televizní
vysílač	vysílač	k1gInSc1	vysílač
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
byl	být	k5eAaImAgInS	být
zrušen	zrušit	k5eAaPmNgInS	zrušit
výtah	výtah	k1gInSc1	výtah
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
tubus	tubus	k1gInSc4	tubus
vyplnily	vyplnit	k5eAaPmAgInP	vyplnit
kabely	kabel	k1gInPc1	kabel
a	a	k8xC	a
napáječe	napáječ	k1gInPc1	napáječ
<g/>
.	.	kIx.	.
</s>
<s>
Kabina	kabina	k1gFnSc1	kabina
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
patře	patro	k1gNnSc6	patro
také	také	k9	také
začala	začít	k5eAaPmAgFnS	začít
sloužit	sloužit	k5eAaImF	sloužit
spojům	spoj	k1gInPc3	spoj
<g/>
,	,	kIx,	,
kterým	který	k3yQgInPc3	který
byla	být	k5eAaImAgFnS	být
celá	celý	k2eAgFnSc1d1	celá
věž	věž	k1gFnSc1	věž
předána	předat	k5eAaPmNgFnS	předat
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
královské	královský	k2eAgFnSc2d1	královská
koruny	koruna	k1gFnSc2	koruna
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
umístěna	umístěn	k2eAgFnSc1d1	umístěna
televizní	televizní	k2eAgFnSc1d1	televizní
anténa	anténa	k1gFnSc1	anténa
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
anténní	anténní	k2eAgInSc4d1	anténní
nástavec	nástavec	k1gInSc4	nástavec
tubusu	tubus	k1gInSc2	tubus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byly	být	k5eAaImAgInP	být
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
rozhledny	rozhledna	k1gFnSc2	rozhledna
umístěny	umístěn	k2eAgFnPc4d1	umístěna
další	další	k2eAgFnPc4d1	další
vysílací	vysílací	k2eAgFnPc4d1	vysílací
antény	anténa	k1gFnPc4	anténa
pro	pro	k7c4	pro
televizní	televizní	k2eAgNnSc4d1	televizní
a	a	k8xC	a
rozhlasové	rozhlasový	k2eAgNnSc4d1	rozhlasové
vysílání	vysílání	k1gNnSc4	vysílání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1979	[number]	k4	1979
byla	být	k5eAaImAgFnS	být
rozhledna	rozhledna	k1gFnSc1	rozhledna
uzavřena	uzavřít	k5eAaPmNgFnS	uzavřít
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
pro	pro	k7c4	pro
špatný	špatný	k2eAgInSc4d1	špatný
technický	technický	k2eAgInSc4d1	technický
stav	stav	k1gInSc4	stav
<g/>
.	.	kIx.	.
</s>
<s>
Rozhledna	rozhledna	k1gFnSc1	rozhledna
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
nejnutnějších	nutný	k2eAgFnPc6d3	nejnutnější
opravách	oprava	k1gFnPc6	oprava
znovu	znovu	k6eAd1	znovu
otevřena	otevřen	k2eAgFnSc1d1	otevřena
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
ke	k	k7c3	k
100	[number]	k4	100
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
Zemské	zemský	k2eAgFnSc2d1	zemská
jubilejní	jubilejní	k2eAgFnSc2d1	jubilejní
výstavy	výstava	k1gFnSc2	výstava
konala	konat	k5eAaImAgFnS	konat
Všeobecná	všeobecný	k2eAgFnSc1d1	všeobecná
československá	československý	k2eAgFnSc1d1	Československá
výstava	výstava	k1gFnSc1	výstava
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
skončilo	skončit	k5eAaPmAgNnS	skončit
vysílání	vysílání	k1gNnSc1	vysílání
televize	televize	k1gFnSc2	televize
<g/>
,	,	kIx,	,
zůstaly	zůstat	k5eAaPmAgInP	zůstat
pouze	pouze	k6eAd1	pouze
rozhlasové	rozhlasový	k2eAgInPc1d1	rozhlasový
vysílače	vysílač	k1gInPc1	vysílač
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
převzala	převzít	k5eAaPmAgFnS	převzít
správu	správa	k1gFnSc4	správa
rozhledny	rozhledna	k1gFnSc2	rozhledna
Pražská	pražský	k2eAgFnSc1d1	Pražská
informační	informační	k2eAgFnSc1d1	informační
služba	služba	k1gFnSc1	služba
a	a	k8xC	a
v	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
započala	započnout	k5eAaPmAgFnS	započnout
generální	generální	k2eAgFnSc1d1	generální
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
rozhledny	rozhledna	k1gFnSc2	rozhledna
<g/>
.	.	kIx.	.
</s>
<s>
Rozhledna	rozhledna	k1gFnSc1	rozhledna
byla	být	k5eAaImAgFnS	být
znovu	znovu	k6eAd1	znovu
otevřena	otevřen	k2eAgFnSc1d1	otevřena
24	[number]	k4	24
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Odstranění	odstranění	k1gNnSc1	odstranění
televizního	televizní	k2eAgInSc2d1	televizní
vysílače	vysílač	k1gInSc2	vysílač
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
uvolnění	uvolnění	k1gNnSc1	uvolnění
tubusu	tubus	k1gInSc2	tubus
výtahu	výtah	k1gInSc2	výtah
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
rekonstrukci	rekonstrukce	k1gFnSc4	rekonstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
výtah	výtah	k1gInSc1	výtah
byl	být	k5eAaImAgInS	být
uveden	uvést	k5eAaPmNgInS	uvést
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
kapacita	kapacita	k1gFnSc1	kapacita
je	být	k5eAaImIp3nS	být
šest	šest	k4xCc4	šest
osob	osoba	k1gFnPc2	osoba
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
ho	on	k3xPp3gMnSc4	on
využívat	využívat	k5eAaImF	využívat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
doprovodu	doprovod	k1gInSc6	doprovod
zaměstnance	zaměstnanec	k1gMnSc2	zaměstnanec
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
místo	místo	k7c2	místo
dřívějšího	dřívější	k2eAgInSc2d1	dřívější
obedněného	obedněný	k2eAgInSc2d1	obedněný
balkonu	balkon	k1gInSc2	balkon
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
20	[number]	k4	20
metrů	metr	k1gInPc2	metr
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
ochoz	ochoz	k1gInSc1	ochoz
přístupný	přístupný	k2eAgInSc1d1	přístupný
i	i	k9	i
turistům	turist	k1gMnPc3	turist
na	na	k7c6	na
invalidním	invalidní	k2eAgInSc6d1	invalidní
vozíku	vozík	k1gInSc6	vozík
<g/>
.	.	kIx.	.
</s>
<s>
Nově	nově	k6eAd1	nově
vybudované	vybudovaný	k2eAgNnSc1d1	vybudované
přízemí	přízemí	k1gNnSc1	přízemí
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
vstupní	vstupní	k2eAgFnSc4d1	vstupní
halu	hala	k1gFnSc4	hala
<g/>
,	,	kIx,	,
bufet	bufet	k1gNnSc4	bufet
a	a	k8xC	a
expozici	expozice	k1gFnSc4	expozice
Klubu	klub	k1gInSc2	klub
českých	český	k2eAgMnPc2d1	český
turistů	turist	k1gMnPc2	turist
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
podzemních	podzemní	k2eAgFnPc6d1	podzemní
prostorách	prostora	k1gFnPc6	prostora
rozhledny	rozhledna	k1gFnSc2	rozhledna
byla	být	k5eAaImAgNnP	být
do	do	k7c2	do
března	březen	k1gInSc2	březen
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
umístěna	umístěn	k2eAgFnSc1d1	umístěna
výstava	výstava	k1gFnSc1	výstava
artefaktů	artefakt	k1gInPc2	artefakt
spojených	spojený	k2eAgInPc2d1	spojený
s	s	k7c7	s
životem	život	k1gInSc7	život
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
dubna	duben	k1gInSc2	duben
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
do	do	k7c2	do
ledna	leden	k1gInSc2	leden
2013	[number]	k4	2013
se	se	k3xPyFc4	se
ve	v	k7c6	v
stejných	stejný	k2eAgFnPc6d1	stejná
prostorách	prostora	k1gFnPc6	prostora
nacházela	nacházet	k5eAaImAgFnS	nacházet
interaktivní	interaktivní	k2eAgFnSc1d1	interaktivní
výstava	výstava	k1gFnSc1	výstava
Igráčků	Igráček	k1gMnPc2	Igráček
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
srpna	srpen	k1gInSc2	srpen
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
převzala	převzít	k5eAaPmAgFnS	převzít
od	od	k7c2	od
Pražské	pražský	k2eAgFnSc2d1	Pražská
informační	informační	k2eAgFnSc2d1	informační
služby	služba	k1gFnSc2	služba
správu	správa	k1gFnSc4	správa
Petřínské	petřínský	k2eAgFnSc2d1	Petřínská
rozhledny	rozhledna	k1gFnSc2	rozhledna
i	i	k8xC	i
dalších	další	k2eAgFnPc2d1	další
věží	věž	k1gFnPc2	věž
společnost	společnost	k1gFnSc1	společnost
Mark	Mark	k1gMnSc1	Mark
<g/>
2	[number]	k4	2
Corporation	Corporation	k1gInSc1	Corporation
Czech	Czech	k1gInSc4	Czech
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
ABL	ABL	kA	ABL
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
správě	správa	k1gFnSc6	správa
MC	MC	kA	MC
<g/>
.	.	kIx.	.
<g/>
C	C	kA	C
byly	být	k5eAaImAgFnP	být
věže	věž	k1gFnPc1	věž
až	až	k6eAd1	až
do	do	k7c2	do
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
správu	správa	k1gFnSc4	správa
převzalo	převzít	k5eAaPmAgNnS	převzít
Muzeum	muzeum	k1gNnSc1	muzeum
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
března	březen	k1gInSc2	březen
2013	[number]	k4	2013
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
umístěna	umístěn	k2eAgFnSc1d1	umístěna
výstava	výstava	k1gFnSc1	výstava
stavebnice	stavebnice	k1gFnSc2	stavebnice
Merkur	Merkur	k1gInSc1	Merkur
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
suterénu	suterén	k1gInSc6	suterén
rozhledny	rozhledna	k1gFnSc2	rozhledna
je	být	k5eAaImIp3nS	být
umístěno	umístit	k5eAaPmNgNnS	umístit
také	také	k9	také
několik	několik	k4yIc1	několik
panelů	panel	k1gInPc2	panel
mapujících	mapující	k2eAgInPc2d1	mapující
historii	historie	k1gFnSc4	historie
Klubu	klub	k1gInSc2	klub
českých	český	k2eAgMnPc2d1	český
turistů	turist	k1gMnPc2	turist
a	a	k8xC	a
historii	historie	k1gFnSc4	historie
rozhledny	rozhledna	k1gFnSc2	rozhledna
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
historických	historický	k2eAgFnPc2d1	historická
fotografií	fotografia	k1gFnPc2	fotografia
rozhledny	rozhledna	k1gFnSc2	rozhledna
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
nalezení	nalezení	k1gNnSc3	nalezení
ve	v	k7c6	v
vstupní	vstupní	k2eAgFnSc6d1	vstupní
kruhové	kruhový	k2eAgFnSc6d1	kruhová
hale	hala	k1gFnSc6	hala
<g/>
;	;	kIx,	;
na	na	k7c6	na
plošině	plošina	k1gFnSc6	plošina
ve	v	k7c6	v
dvaceti	dvacet	k4xCc6	dvacet
metrech	metro	k1gNnPc6	metro
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
historické	historický	k2eAgFnPc1d1	historická
kresby	kresba	k1gFnPc1	kresba
výhledů	výhled	k1gInPc2	výhled
z	z	k7c2	z
rozhledny	rozhledna	k1gFnSc2	rozhledna
s	s	k7c7	s
popisem	popis	k1gInSc7	popis
<g/>
.	.	kIx.	.
</s>
<s>
Rozhledna	rozhledna	k1gFnSc1	rozhledna
má	mít	k5eAaImIp3nS	mít
11	[number]	k4	11
metrů	metr	k1gInPc2	metr
hluboké	hluboký	k2eAgInPc4d1	hluboký
základy	základ	k1gInPc4	základ
a	a	k8xC	a
na	na	k7c6	na
nich	on	k3xPp3gFnPc6	on
stojí	stát	k5eAaImIp3nS	stát
63,5	[number]	k4	63,5
metrů	metr	k1gInPc2	metr
vysoká	vysoký	k2eAgFnSc1d1	vysoká
ocelová	ocelový	k2eAgFnSc1d1	ocelová
konstrukce	konstrukce	k1gFnSc1	konstrukce
vážící	vážící	k2eAgFnSc1d1	vážící
175	[number]	k4	175
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
Jádrem	jádro	k1gNnSc7	jádro
konstrukce	konstrukce	k1gFnSc2	konstrukce
je	být	k5eAaImIp3nS	být
osmiboký	osmiboký	k2eAgInSc1d1	osmiboký
tubus	tubus	k1gInSc1	tubus
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
je	být	k5eAaImIp3nS	být
umístěn	umístěn	k2eAgInSc1d1	umístěn
výtah	výtah	k1gInSc1	výtah
<g/>
;	;	kIx,	;
okolo	okolo	k7c2	okolo
něj	on	k3xPp3gInSc2	on
se	se	k3xPyFc4	se
vinou	vinout	k5eAaImIp3nP	vinout
dvě	dva	k4xCgNnPc1	dva
točitá	točitý	k2eAgNnPc1d1	točité
schodiště	schodiště	k1gNnPc1	schodiště
s	s	k7c7	s
299	[number]	k4	299
schody	schod	k1gInPc7	schod
-	-	kIx~	-
jedno	jeden	k4xCgNnSc1	jeden
pro	pro	k7c4	pro
chůzi	chůze	k1gFnSc4	chůze
nahoru	nahoru	k6eAd1	nahoru
<g/>
,	,	kIx,	,
druhé	druhý	k4xOgFnSc6	druhý
pro	pro	k7c4	pro
chůzi	chůze	k1gFnSc4	chůze
dolů	dol	k1gInPc2	dol
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
výše	výše	k1gFnSc1	výše
rozhledny	rozhledna	k1gFnSc2	rozhledna
je	být	k5eAaImIp3nS	být
65,5	[number]	k4	65,5
m.	m.	k?	m.
Rozhledna	rozhledna	k1gFnSc1	rozhledna
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc4	dva
vyhlídkové	vyhlídkový	k2eAgFnPc4d1	vyhlídková
plošiny	plošina	k1gFnPc4	plošina
<g/>
,	,	kIx,	,
horní	horní	k2eAgFnSc1d1	horní
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
55	[number]	k4	55
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
rozhledu	rozhled	k1gInSc2	rozhled
na	na	k7c4	na
panoráma	panoráma	k1gNnSc4	panoráma
Prahy	Praha	k1gFnSc2	Praha
včetně	včetně	k7c2	včetně
blízkého	blízký	k2eAgInSc2d1	blízký
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
se	se	k3xPyFc4	se
z	z	k7c2	z
rozhledny	rozhledna	k1gFnSc2	rozhledna
otevírá	otevírat	k5eAaImIp3nS	otevírat
za	za	k7c2	za
jasného	jasný	k2eAgNnSc2d1	jasné
počasí	počasí	k1gNnSc2	počasí
daleký	daleký	k2eAgInSc4d1	daleký
rozhled	rozhled	k1gInSc4	rozhled
po	po	k7c6	po
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
lze	lze	k6eAd1	lze
spatřit	spatřit	k5eAaPmF	spatřit
Říp	Říp	k1gInSc4	Říp
a	a	k8xC	a
České	český	k2eAgNnSc1d1	české
středohoří	středohoří	k1gNnSc1	středohoří
<g/>
,	,	kIx,	,
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
masiv	masiv	k1gInSc4	masiv
Krkonoš	Krkonoše	k1gFnPc2	Krkonoše
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
je	být	k5eAaImIp3nS	být
rozhled	rozhled	k1gInSc1	rozhled
poměrně	poměrně	k6eAd1	poměrně
omezený	omezený	k2eAgInSc1d1	omezený
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jej	on	k3xPp3gInSc4	on
uzavírají	uzavírat	k5eAaImIp3nP	uzavírat
Brdy	Brdy	k1gInPc1	Brdy
<g/>
.	.	kIx.	.
</s>
