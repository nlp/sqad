<s>
Sídlem	sídlo	k1gNnSc7	sídlo
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
je	být	k5eAaImIp3nS	být
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
soud	soud	k1gInSc1	soud
je	být	k5eAaImIp3nS	být
umístěn	umístit	k5eAaPmNgInS	umístit
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
bývalé	bývalý	k2eAgFnSc2d1	bývalá
Moravské	moravský	k2eAgFnSc2d1	Moravská
Zemské	zemský	k2eAgFnSc2d1	zemská
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
sídlil	sídlit	k5eAaImAgInS	sídlit
již	již	k6eAd1	již
Ústavní	ústavní	k2eAgInSc1d1	ústavní
soud	soud	k1gInSc1	soud
ČSFR	ČSFR	kA	ČSFR
<g/>
.	.	kIx.	.
</s>
