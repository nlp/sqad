<s>
Ústavní	ústavní	k2eAgInSc1d1	ústavní
soud	soud	k1gInSc1	soud
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
je	být	k5eAaImIp3nS	být
specializovaná	specializovaný	k2eAgFnSc1d1	specializovaná
česká	český	k2eAgFnSc1d1	Česká
soudní	soudní	k2eAgFnSc1d1	soudní
instituce	instituce	k1gFnSc1	instituce
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
stojí	stát	k5eAaImIp3nS	stát
mimo	mimo	k7c4	mimo
systém	systém	k1gInSc4	systém
obecných	obecný	k2eAgInPc2d1	obecný
soudů	soud	k1gInPc2	soud
a	a	k8xC	a
jejímž	jejíž	k3xOyRp3gNnSc7	jejíž
základním	základní	k2eAgNnSc7d1	základní
posláním	poslání	k1gNnSc7	poslání
je	být	k5eAaImIp3nS	být
garantovat	garantovat	k5eAaBmF	garantovat
ústavnost	ústavnost	k1gFnSc4	ústavnost
právního	právní	k2eAgInSc2d1	právní
řádu	řád	k1gInSc2	řád
a	a	k8xC	a
poskytovat	poskytovat	k5eAaImF	poskytovat
ochranu	ochrana	k1gFnSc4	ochrana
základních	základní	k2eAgNnPc2d1	základní
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
fyzických	fyzický	k2eAgFnPc2d1	fyzická
a	a	k8xC	a
právnických	právnický	k2eAgFnPc2d1	právnická
osob	osoba	k1gFnPc2	osoba
na	na	k7c6	na
základě	základ	k1gInSc6	základ
ústavního	ústavní	k2eAgInSc2d1	ústavní
pořádku	pořádek	k1gInSc2	pořádek
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
proti	proti	k7c3	proti
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
obecných	obecný	k2eAgInPc2d1	obecný
soudů	soud	k1gInPc2	soud
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
nebyla	být	k5eNaImAgFnS	být
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
ústavním	ústavní	k2eAgInSc7d1	ústavní
pořádkem	pořádek	k1gInSc7	pořádek
<g/>
.	.	kIx.	.
</s>
<s>
Sídlem	sídlo	k1gNnSc7	sídlo
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
je	být	k5eAaImIp3nS	být
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
soud	soud	k1gInSc1	soud
je	být	k5eAaImIp3nS	být
umístěn	umístit	k5eAaPmNgInS	umístit
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
bývalé	bývalý	k2eAgFnSc2d1	bývalá
Moravské	moravský	k2eAgFnSc2d1	Moravská
Zemské	zemský	k2eAgFnSc2d1	zemská
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
sídlil	sídlit	k5eAaImAgInS	sídlit
již	již	k6eAd1	již
Ústavní	ústavní	k2eAgInSc1d1	ústavní
soud	soud	k1gInSc1	soud
ČSFR	ČSFR	kA	ČSFR
<g/>
.	.	kIx.	.
</s>
<s>
Shromáždění	shromáždění	k1gNnSc1	shromáždění
občanů	občan	k1gMnPc2	občan
v	v	k7c6	v
okruhu	okruh	k1gInSc6	okruh
100	[number]	k4	100
m	m	kA	m
od	od	k7c2	od
budov	budova	k1gFnPc2	budova
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
nebo	nebo	k8xC	nebo
od	od	k7c2	od
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Ústavní	ústavní	k2eAgInSc1d1	ústavní
soud	soud	k1gInSc1	soud
jedná	jednat	k5eAaImIp3nS	jednat
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
zakázána	zakázán	k2eAgFnSc1d1	zakázána
<g/>
.	.	kIx.	.
</s>
<s>
Ústavní	ústavní	k2eAgInSc1d1	ústavní
soud	soud	k1gInSc1	soud
Československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
existoval	existovat	k5eAaImAgInS	existovat
už	už	k6eAd1	už
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
<g/>
,	,	kIx,	,
institut	institut	k1gInSc1	institut
ústavní	ústavní	k2eAgFnSc2d1	ústavní
stížnosti	stížnost	k1gFnSc2	stížnost
ale	ale	k8xC	ale
nebyl	být	k5eNaImAgInS	být
zaveden	zavést	k5eAaPmNgInS	zavést
<g/>
.	.	kIx.	.
</s>
<s>
Soud	soud	k1gInSc1	soud
sídlil	sídlit	k5eAaImAgInS	sídlit
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
sedmičlenný	sedmičlenný	k2eAgMnSc1d1	sedmičlenný
a	a	k8xC	a
funkční	funkční	k2eAgNnSc1d1	funkční
období	období	k1gNnSc1	období
bylo	být	k5eAaImAgNnS	být
desetileté	desetiletý	k2eAgNnSc1d1	desetileté
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
předsedou	předseda	k1gMnSc7	předseda
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Karel	Karel	k1gMnSc1	Karel
Baxa	Baxa	k1gMnSc1	Baxa
<g/>
.	.	kIx.	.
</s>
<s>
Činnost	činnost	k1gFnSc1	činnost
prvorepublikového	prvorepublikový	k2eAgInSc2d1	prvorepublikový
ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
je	být	k5eAaImIp3nS	být
hodnocena	hodnotit	k5eAaImNgFnS	hodnotit
jako	jako	k9	jako
nevýznamná	významný	k2eNgFnSc1d1	nevýznamná
a	a	k8xC	a
málo	málo	k6eAd1	málo
frekventovaná	frekventovaný	k2eAgNnPc1d1	frekventované
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
uplynutí	uplynutí	k1gNnSc6	uplynutí
prvního	první	k4xOgNnSc2	první
funkčního	funkční	k2eAgNnSc2d1	funkční
období	období	k1gNnSc2	období
nedošlo	dojít	k5eNaPmAgNnS	dojít
sedm	sedm	k4xCc1	sedm
let	léto	k1gNnPc2	léto
k	k	k7c3	k
jeho	jeho	k3xOp3gNnPc3	jeho
znovuobnovení	znovuobnovení	k1gNnSc3	znovuobnovení
<g/>
,	,	kIx,	,
ustavující	ustavující	k2eAgFnSc2d1	ustavující
schůze	schůze	k1gFnSc2	schůze
krátce	krátce	k6eAd1	krátce
působícího	působící	k2eAgInSc2d1	působící
druhého	druhý	k4xOgInSc2	druhý
ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc7	jeho
předsedou	předseda	k1gMnSc7	předseda
byl	být	k5eAaImAgMnS	být
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Krejčí	Krejčí	k1gMnSc1	Krejčí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
a	a	k8xC	a
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
svou	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
už	už	k6eAd1	už
neobnovil	obnovit	k5eNaPmAgMnS	obnovit
<g/>
.	.	kIx.	.
</s>
<s>
Ústava	ústava	k1gFnSc1	ústava
Československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
z	z	k7c2	z
9	[number]	k4	9
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1948	[number]	k4	1948
ústavní	ústavní	k2eAgInSc4d1	ústavní
soud	soud	k1gInSc4	soud
nezřídila	zřídit	k5eNaPmAgFnS	zřídit
<g/>
.	.	kIx.	.
</s>
<s>
Navrhovatelé	navrhovatel	k1gMnPc1	navrhovatel
to	ten	k3xDgNnSc4	ten
zdůvodnili	zdůvodnit	k5eAaPmAgMnP	zdůvodnit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
Národní	národní	k2eAgNnSc1d1	národní
shromáždění	shromáždění	k1gNnSc1	shromáždění
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
opravdu	opravdu	k6eAd1	opravdu
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
orgánem	orgán	k1gInSc7	orgán
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
<g/>
"	"	kIx"	"
a	a	k8xC	a
proto	proto	k8xC	proto
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
odstraňujeme	odstraňovat	k5eAaImIp1nP	odstraňovat
všechny	všechen	k3xTgInPc1	všechen
zvláštní	zvláštní	k2eAgInPc1d1	zvláštní
orgány	orgán	k1gInPc1	orgán
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgInP	být
více	hodně	k6eAd2	hodně
méně	málo	k6eAd2	málo
byrokratické	byrokratický	k2eAgFnSc2d1	byrokratická
povahy	povaha	k1gFnSc2	povaha
a	a	k8xC	a
které	který	k3yQgFnPc1	který
fakticky	fakticky	k6eAd1	fakticky
stály	stát	k5eAaImAgFnP	stát
nad	nad	k7c7	nad
Národním	národní	k2eAgNnSc7d1	národní
shromážděním	shromáždění	k1gNnSc7	shromáždění
<g/>
.	.	kIx.	.
</s>
<s>
Mám	mít	k5eAaImIp1nS	mít
na	na	k7c6	na
mysli	mysl	k1gFnSc6	mysl
především	především	k9	především
ústavní	ústavní	k2eAgInSc4d1	ústavní
soud	soud	k1gInSc4	soud
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
přičemž	přičemž	k6eAd1	přičemž
parlament	parlament	k1gInSc1	parlament
<g/>
,	,	kIx,	,
prezident	prezident	k1gMnSc1	prezident
a	a	k8xC	a
vláda	vláda	k1gFnSc1	vláda
"	"	kIx"	"
<g/>
mají	mít	k5eAaImIp3nP	mít
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
jako	jako	k8xS	jako
orgány	orgán	k1gInPc4	orgán
jednotné	jednotný	k2eAgFnSc2d1	jednotná
lidové	lidový	k2eAgFnSc2d1	lidová
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
pracovat	pracovat	k5eAaImF	pracovat
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
jako	jako	k9	jako
představitelé	představitel	k1gMnPc1	představitel
nějakých	nějaký	k3yIgFnPc2	nějaký
zvláštních	zvláštní	k2eAgFnPc2d1	zvláštní
mocí	moc	k1gFnPc2	moc
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
v	v	k7c6	v
Ústavě	ústava	k1gFnSc6	ústava
Československé	československý	k2eAgFnSc2d1	Československá
socialistické	socialistický	k2eAgFnSc2d1	socialistická
republiky	republika	k1gFnSc2	republika
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
se	se	k3xPyFc4	se
s	s	k7c7	s
ústavním	ústavní	k2eAgInSc7d1	ústavní
soudem	soud	k1gInSc7	soud
nepočítalo	počítat	k5eNaImAgNnS	počítat
<g/>
.	.	kIx.	.
</s>
<s>
Institut	institut	k1gInSc1	institut
ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
federace	federace	k1gFnSc2	federace
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
ústavních	ústavní	k2eAgInPc2d1	ústavní
soudů	soud	k1gInPc2	soud
obou	dva	k4xCgNnPc2	dva
republik	republika	k1gFnPc2	republika
byl	být	k5eAaImAgInS	být
zaveden	zavést	k5eAaPmNgInS	zavést
až	až	k6eAd1	až
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1969	[number]	k4	1969
Ústavním	ústavní	k2eAgInSc7d1	ústavní
zákonem	zákon	k1gInSc7	zákon
o	o	k7c6	o
československé	československý	k2eAgFnSc6d1	Československá
federaci	federace	k1gFnSc6	federace
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
článku	článek	k1gInSc6	článek
101	[number]	k4	101
stanovil	stanovit	k5eAaPmAgMnS	stanovit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
socialistické	socialistický	k2eAgFnSc6d1	socialistická
republice	republika	k1gFnSc6	republika
a	a	k8xC	a
v	v	k7c6	v
Slovenské	slovenský	k2eAgFnSc6d1	slovenská
socialistické	socialistický	k2eAgFnSc6d1	socialistická
republice	republika	k1gFnSc6	republika
působí	působit	k5eAaImIp3nP	působit
ústavní	ústavní	k2eAgInPc1d1	ústavní
soudy	soud	k1gInPc1	soud
republik	republika	k1gFnPc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
působnost	působnost	k1gFnSc1	působnost
a	a	k8xC	a
zásady	zásada	k1gFnPc1	zásada
organizace	organizace	k1gFnSc2	organizace
stanoví	stanovit	k5eAaPmIp3nP	stanovit
ústavní	ústavní	k2eAgInPc1d1	ústavní
zákony	zákon	k1gInPc1	zákon
národních	národní	k2eAgFnPc2d1	národní
rad	rada	k1gFnPc2	rada
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Toto	tento	k3xDgNnSc1	tento
ustanovení	ustanovení	k1gNnSc1	ustanovení
československé	československý	k2eAgFnSc2d1	Československá
ústavy	ústava	k1gFnSc2	ústava
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
pouze	pouze	k6eAd1	pouze
formální	formální	k2eAgMnSc1d1	formální
<g/>
,	,	kIx,	,
nikdy	nikdy	k6eAd1	nikdy
nebylo	být	k5eNaImAgNnS	být
naplněno	naplnit	k5eAaPmNgNnS	naplnit
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
tehdejší	tehdejší	k2eAgNnSc1d1	tehdejší
ustavení	ustavení	k1gNnSc1	ustavení
federálního	federální	k2eAgInSc2d1	federální
ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
<g/>
.	.	kIx.	.
</s>
<s>
Ústavní	ústavní	k2eAgInSc1d1	ústavní
soud	soud	k1gInSc1	soud
České	český	k2eAgFnSc2d1	Česká
a	a	k8xC	a
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
Federativní	federativní	k2eAgFnSc2d1	federativní
Republiky	republika	k1gFnSc2	republika
nakonec	nakonec	k6eAd1	nakonec
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
na	na	k7c6	na
základě	základ	k1gInSc6	základ
zvláštního	zvláštní	k2eAgInSc2d1	zvláštní
ústavního	ústavní	k2eAgInSc2d1	ústavní
zákona	zákon	k1gInSc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
na	na	k7c6	na
základě	základ	k1gInSc6	základ
Ústavy	ústava	k1gFnSc2	ústava
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
s	s	k7c7	s
účinností	účinnost	k1gFnSc7	účinnost
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1992	[number]	k4	1992
Ústavní	ústavní	k2eAgInSc1d1	ústavní
soud	soud	k1gInSc1	soud
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
naplněn	naplněn	k2eAgInSc1d1	naplněn
byl	být	k5eAaImAgInS	být
až	až	k9	až
po	po	k7c6	po
zániku	zánik	k1gInSc6	zánik
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Ústavní	ústavní	k2eAgInSc1d1	ústavní
soud	soud	k1gInSc1	soud
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
ustavený	ustavený	k2eAgInSc4d1	ustavený
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1993	[number]	k4	1993
a	a	k8xC	a
naplněný	naplněný	k2eAgMnSc1d1	naplněný
počátkem	počátkem	k7c2	počátkem
tohoto	tento	k3xDgInSc2	tento
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
nemohl	moct	k5eNaImAgMnS	moct
navázat	navázat	k5eAaPmF	navázat
na	na	k7c6	na
ústavní	ústavní	k2eAgFnSc6d1	ústavní
historii	historie	k1gFnSc6	historie
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
federace	federace	k1gFnSc2	federace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
personálně	personálně	k6eAd1	personálně
i	i	k9	i
názorově	názorově	k6eAd1	názorově
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
navázal	navázat	k5eAaPmAgInS	navázat
na	na	k7c4	na
Ústavní	ústavní	k2eAgInSc4d1	ústavní
soud	soud	k1gInSc4	soud
ČSFR	ČSFR	kA	ČSFR
<g/>
.	.	kIx.	.
</s>
<s>
Ústavní	ústavní	k2eAgInSc1d1	ústavní
soud	soud	k1gInSc1	soud
dnes	dnes	k6eAd1	dnes
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
kdysi	kdysi	k6eAd1	kdysi
domem	dům	k1gInSc7	dům
Moravského	moravský	k2eAgInSc2d1	moravský
zemského	zemský	k2eAgInSc2d1	zemský
sněmu	sněm	k1gInSc2	sněm
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
de	de	k?	de
facto	facto	k1gNnSc4	facto
jedinou	jediný	k2eAgFnSc7d1	jediná
parlamentní	parlamentní	k2eAgFnSc7d1	parlamentní
stavbou	stavba	k1gFnSc7	stavba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
jako	jako	k9	jako
taková	takový	k3xDgFnSc1	takový
v	v	k7c6	v
Českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
od	od	k7c2	od
základu	základ	k1gInSc2	základ
postavena	postaven	k2eAgFnSc1d1	postavena
a	a	k8xC	a
jako	jako	k9	jako
sněmovna	sněmovna	k1gFnSc1	sněmovna
i	i	k9	i
koncipována	koncipovat	k5eAaBmNgFnS	koncipovat
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
tím	ten	k3xDgInSc7	ten
účelem	účel	k1gInSc7	účel
byla	být	k5eAaImAgFnS	být
uspořádána	uspořádat	k5eAaPmNgFnS	uspořádat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1872	[number]	k4	1872
druhá	druhý	k4xOgFnSc1	druhý
architektonická	architektonický	k2eAgFnSc1d1	architektonická
soutěž	soutěž	k1gFnSc1	soutěž
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
vítězem	vítěz	k1gMnSc7	vítěz
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Robert	Robert	k1gMnSc1	Robert
Raschka	Raschka	k1gMnSc1	Raschka
a	a	k8xC	a
podle	podle	k7c2	podle
jehož	jenž	k3xRgInSc2	jenž
projektu	projekt	k1gInSc2	projekt
byla	být	k5eAaImAgFnS	být
monumentální	monumentální	k2eAgFnSc1d1	monumentální
nemovitost	nemovitost	k1gFnSc1	nemovitost
také	také	k9	také
postavena	postaven	k2eAgFnSc1d1	postavena
<g/>
.	.	kIx.	.
</s>
<s>
Sloužila	sloužit	k5eAaImAgFnS	sloužit
jako	jako	k9	jako
místo	místo	k7c2	místo
moravského	moravský	k2eAgNnSc2d1	Moravské
sněmování	sněmování	k1gNnSc2	sněmování
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
<g/>
.	.	kIx.	.
</s>
<s>
Sochy	socha	k1gFnPc1	socha
na	na	k7c6	na
horní	horní	k2eAgFnSc6d1	horní
římse	římsa	k1gFnSc6	římsa
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
sochař	sochař	k1gMnSc1	sochař
Adolf	Adolf	k1gMnSc1	Adolf
Loos	Loos	k1gInSc1	Loos
<g/>
,	,	kIx,	,
otec	otec	k1gMnSc1	otec
slavného	slavný	k2eAgMnSc2d1	slavný
architekta	architekt	k1gMnSc2	architekt
stejného	stejný	k2eAgNnSc2d1	stejné
jména	jméno	k1gNnSc2	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Úkolem	úkol	k1gInSc7	úkol
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
je	být	k5eAaImIp3nS	být
dbát	dbát	k5eAaImF	dbát
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
ústavnosti	ústavnost	k1gFnSc2	ústavnost
<g/>
.	.	kIx.	.
</s>
<s>
Ústavní	ústavní	k2eAgInSc1d1	ústavní
soud	soud	k1gInSc1	soud
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
<g/>
:	:	kIx,	:
o	o	k7c4	o
zrušení	zrušení	k1gNnSc4	zrušení
zákonů	zákon	k1gInPc2	zákon
nebo	nebo	k8xC	nebo
jejich	jejich	k3xOp3gNnPc2	jejich
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
ustanovení	ustanovení	k1gNnPc2	ustanovení
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
<g/>
-li	i	k?	-li
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
ústavním	ústavní	k2eAgInSc7d1	ústavní
pořádkem	pořádek	k1gInSc7	pořádek
<g/>
,	,	kIx,	,
o	o	k7c4	o
zrušení	zrušení	k1gNnSc4	zrušení
jiných	jiný	k2eAgInPc2d1	jiný
právních	právní	k2eAgInPc2d1	právní
předpisů	předpis	k1gInPc2	předpis
nebo	nebo	k8xC	nebo
jejich	jejich	k3xOp3gNnPc2	jejich
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
ustanovení	ustanovení	k1gNnPc2	ustanovení
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
<g/>
-li	i	k?	-li
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
ústavním	ústavní	k2eAgInSc7d1	ústavní
pořádkem	pořádek	k1gInSc7	pořádek
nebo	nebo	k8xC	nebo
zákonem	zákon	k1gInSc7	zákon
<g/>
,	,	kIx,	,
o	o	k7c4	o
ústavní	ústavní	k2eAgFnPc4d1	ústavní
stížnosti	stížnost	k1gFnPc4	stížnost
<g />
.	.	kIx.	.
</s>
<s>
orgánů	orgán	k1gInPc2	orgán
územní	územní	k2eAgFnSc2d1	územní
samosprávy	samospráva	k1gFnSc2	samospráva
proti	proti	k7c3	proti
nezákonnému	zákonný	k2eNgInSc3d1	nezákonný
zásahu	zásah	k1gInSc3	zásah
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
o	o	k7c6	o
ústavní	ústavní	k2eAgFnSc6d1	ústavní
stížnosti	stížnost	k1gFnSc6	stížnost
proti	proti	k7c3	proti
pravomocnému	pravomocný	k2eAgNnSc3d1	pravomocné
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
a	a	k8xC	a
jinému	jiný	k2eAgInSc3d1	jiný
zásahu	zásah	k1gInSc3	zásah
orgánů	orgán	k1gInPc2	orgán
veřejné	veřejný	k2eAgFnSc2d1	veřejná
moci	moc	k1gFnSc2	moc
do	do	k7c2	do
ústavně	ústavně	k6eAd1	ústavně
zaručených	zaručený	k2eAgNnPc2d1	zaručené
základních	základní	k2eAgNnPc2d1	základní
práv	právo	k1gNnPc2	právo
a	a	k8xC	a
svobod	svoboda	k1gFnPc2	svoboda
<g/>
,	,	kIx,	,
o	o	k7c6	o
opravném	opravný	k2eAgInSc6d1	opravný
prostředku	prostředek	k1gInSc6	prostředek
proti	proti	k7c3	proti
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
ve	v	k7c6	v
věci	věc	k1gFnSc6	věc
ověření	ověření	k1gNnSc1	ověření
volby	volba	k1gFnSc2	volba
poslance	poslanec	k1gMnSc2	poslanec
nebo	nebo	k8xC	nebo
senátora	senátor	k1gMnSc2	senátor
<g/>
,	,	kIx,	,
v	v	k7c6	v
pochybnostech	pochybnost	k1gFnPc6	pochybnost
o	o	k7c6	o
ztrátě	ztráta	k1gFnSc6	ztráta
volitelnosti	volitelnost	k1gFnSc2	volitelnost
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
o	o	k7c6	o
neslučitelnosti	neslučitelnost	k1gFnSc6	neslučitelnost
výkonu	výkon	k1gInSc2	výkon
funkcí	funkce	k1gFnSc7	funkce
poslance	poslanec	k1gMnSc2	poslanec
nebo	nebo	k8xC	nebo
senátora	senátor	k1gMnSc2	senátor
podle	podle	k7c2	podle
čl	čl	kA	čl
<g/>
.	.	kIx.	.
25	[number]	k4	25
Ústavy	ústava	k1gFnSc2	ústava
<g/>
,	,	kIx,	,
o	o	k7c6	o
ústavní	ústavní	k2eAgFnSc6d1	ústavní
žalobě	žaloba	k1gFnSc6	žaloba
Senátu	senát	k1gInSc2	senát
proti	proti	k7c3	proti
prezidentu	prezident	k1gMnSc3	prezident
republiky	republika	k1gFnSc2	republika
podle	podle	k7c2	podle
čl	čl	kA	čl
<g/>
.	.	kIx.	.
65	[number]	k4	65
odst	odsta	k1gFnPc2	odsta
<g/>
.	.	kIx.	.
2	[number]	k4	2
Ústavy	ústava	k1gFnSc2	ústava
<g/>
,	,	kIx,	,
o	o	k7c6	o
návrhu	návrh	k1gInSc6	návrh
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
na	na	k7c4	na
zrušení	zrušení	k1gNnSc4	zrušení
usnesení	usnesení	k1gNnSc2	usnesení
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
a	a	k8xC	a
Senátu	senát	k1gInSc2	senát
podle	podle	k7c2	podle
čl	čl	kA	čl
<g/>
.	.	kIx.	.
66	[number]	k4	66
<g />
.	.	kIx.	.
</s>
<s>
Ústavy	ústava	k1gFnPc1	ústava
<g/>
,	,	kIx,	,
o	o	k7c6	o
opatřeních	opatření	k1gNnPc6	opatření
nezbytných	nezbytný	k2eAgNnPc2d1	nezbytný
k	k	k7c3	k
provedení	provedení	k1gNnSc3	provedení
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
soudu	soud	k1gInSc2	soud
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
Českou	český	k2eAgFnSc4d1	Česká
republiku	republika	k1gFnSc4	republika
závazné	závazný	k2eAgFnPc1d1	závazná
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
nelze	lze	k6eNd1	lze
provést	provést	k5eAaPmF	provést
jinak	jinak	k6eAd1	jinak
<g/>
,	,	kIx,	,
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
o	o	k7c6	o
rozpuštění	rozpuštění	k1gNnSc6	rozpuštění
politické	politický	k2eAgFnSc2d1	politická
strany	strana	k1gFnSc2	strana
nebo	nebo	k8xC	nebo
jiné	jiný	k2eAgNnSc4d1	jiné
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
týkající	týkající	k2eAgFnSc2d1	týkající
se	se	k3xPyFc4	se
činnosti	činnost	k1gFnSc2	činnost
politické	politický	k2eAgFnSc2d1	politická
strany	strana	k1gFnSc2	strana
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
shodě	shoda	k1gFnSc6	shoda
s	s	k7c7	s
<g />
.	.	kIx.	.
</s>
<s>
ústavními	ústavní	k2eAgInPc7d1	ústavní
nebo	nebo	k8xC	nebo
jinými	jiný	k2eAgInPc7d1	jiný
zákony	zákon	k1gInPc7	zákon
<g/>
,	,	kIx,	,
spory	spor	k1gInPc7	spor
o	o	k7c4	o
rozsah	rozsah	k1gInSc4	rozsah
kompetencí	kompetence	k1gFnPc2	kompetence
státních	státní	k2eAgInPc2d1	státní
orgánů	orgán	k1gInPc2	orgán
a	a	k8xC	a
orgánů	orgán	k1gInPc2	orgán
územní	územní	k2eAgFnSc2d1	územní
samosprávy	samospráva	k1gFnSc2	samospráva
<g/>
,	,	kIx,	,
nepřísluší	příslušet	k5eNaImIp3nS	příslušet
<g/>
-li	i	k?	-li
podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
jinému	jiný	k2eAgInSc3d1	jiný
orgánu	orgán	k1gInSc3	orgán
<g/>
,	,	kIx,	,
o	o	k7c6	o
souladu	soulad	k1gInSc6	soulad
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
smlouvy	smlouva	k1gFnSc2	smlouva
podle	podle	k7c2	podle
čl	čl	kA	čl
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
a	a	k8xC	a
a	a	k8xC	a
čl	čl	kA	čl
<g/>
.	.	kIx.	.
49	[number]	k4	49
s	s	k7c7	s
ústavním	ústavní	k2eAgInSc7d1	ústavní
pořádkem	pořádek	k1gInSc7	pořádek
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
před	před	k7c7	před
její	její	k3xOp3gFnSc7	její
ratifikací	ratifikace	k1gFnSc7	ratifikace
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
Ústavní	ústavní	k2eAgInSc1d1	ústavní
soud	soud	k1gInSc1	soud
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
judikatuře	judikatura	k1gFnSc6	judikatura
mnohokrát	mnohokrát	k6eAd1	mnohokrát
zdůraznil	zdůraznit	k5eAaPmAgMnS	zdůraznit
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
článkem	článek	k1gInSc7	článek
systému	systém	k1gInSc2	systém
obecných	obecný	k2eAgInPc2d1	obecný
soudů	soud	k1gInPc2	soud
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
poslední	poslední	k2eAgFnSc1d1	poslední
vnitrostátní	vnitrostátní	k2eAgFnSc1d1	vnitrostátní
instance	instance	k1gFnSc1	instance
de	de	k?	de
facto	facto	k1gNnSc1	facto
slouží	sloužit	k5eAaImIp3nS	sloužit
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Proti	proti	k7c3	proti
jeho	jeho	k3xOp3gNnSc3	jeho
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
existuje	existovat	k5eAaImIp3nS	existovat
jediný	jediný	k2eAgInSc4d1	jediný
další	další	k2eAgInSc4d1	další
prostředek	prostředek	k1gInSc4	prostředek
nápravy	náprava	k1gFnSc2	náprava
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
stížnost	stížnost	k1gFnSc1	stížnost
k	k	k7c3	k
Evropskému	evropský	k2eAgInSc3d1	evropský
soudu	soud	k1gInSc3	soud
pro	pro	k7c4	pro
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
ve	v	k7c6	v
Štrasburku	Štrasburk	k1gInSc6	Štrasburk
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgMnPc1	tři
z	z	k7c2	z
15	[number]	k4	15
soudců	soudce	k1gMnPc2	soudce
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
jsou	být	k5eAaImIp3nP	být
přímo	přímo	k6eAd1	přímo
při	při	k7c6	při
jmenování	jmenování	k1gNnSc6	jmenování
prezidentem	prezident	k1gMnSc7	prezident
republiky	republika	k1gFnSc2	republika
určeni	určit	k5eAaPmNgMnP	určit
jako	jako	k9	jako
funkcionáři	funkcionář	k1gMnPc1	funkcionář
ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
jeden	jeden	k4xCgMnSc1	jeden
předseda	předseda	k1gMnSc1	předseda
a	a	k8xC	a
dva	dva	k4xCgMnPc1	dva
místopředsedové	místopředseda	k1gMnPc1	místopředseda
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgInPc1d1	vnitřní
předpisy	předpis	k1gInPc1	předpis
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
dva	dva	k4xCgInPc4	dva
místopředsedy	místopředseda	k1gMnPc7	místopředseda
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
místopředsedu	místopředseda	k1gMnSc4	místopředseda
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
místopředsedkyni	místopředsedkyně	k1gFnSc4	místopředsedkyně
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ostatních	ostatní	k2eAgInPc2d1	ostatní
12	[number]	k4	12
soudců	soudce	k1gMnPc2	soudce
je	být	k5eAaImIp3nS	být
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
do	do	k7c2	do
čtyř	čtyři	k4xCgInPc2	čtyři
tříčlenných	tříčlenný	k2eAgInPc2d1	tříčlenný
senátů	senát	k1gInPc2	senát
<g/>
,	,	kIx,	,
označovaných	označovaný	k2eAgInPc2d1	označovaný
římskými	římský	k2eAgNnPc7d1	římské
čísly	číslo	k1gNnPc7	číslo
I	i	k8xC	i
až	až	k9	až
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
</s>
<s>
Předsedové	předseda	k1gMnPc1	předseda
senátů	senát	k1gInPc2	senát
jsou	být	k5eAaImIp3nP	být
předsedou	předseda	k1gMnSc7	předseda
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
jmenováni	jmenovat	k5eAaImNgMnP	jmenovat
zpravidla	zpravidla	k6eAd1	zpravidla
na	na	k7c4	na
dobu	doba	k1gFnSc4	doba
jednoho	jeden	k4xCgInSc2	jeden
kalendářního	kalendářní	k2eAgInSc2d1	kalendářní
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
žádný	žádný	k3yNgMnSc1	žádný
soudce	soudce	k1gMnSc1	soudce
nesmí	smět	k5eNaImIp3nS	smět
být	být	k5eAaImF	být
předsedou	předseda	k1gMnSc7	předseda
senátu	senát	k1gInSc2	senát
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
po	po	k7c6	po
sobě	sebe	k3xPyFc6	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Předseda	předseda	k1gMnSc1	předseda
a	a	k8xC	a
místopředseda	místopředseda	k1gMnSc1	místopředseda
soudu	soud	k1gInSc2	soud
nemohou	moct	k5eNaImIp3nP	moct
být	být	k5eAaImF	být
stálými	stálý	k2eAgMnPc7d1	stálý
členy	člen	k1gInPc4	člen
žádného	žádný	k3yNgInSc2	žádný
senátu	senát	k1gInSc2	senát
<g/>
.	.	kIx.	.
</s>
<s>
Nepřítomného	přítomný	k2eNgMnSc4d1	nepřítomný
člena	člen	k1gMnSc4	člen
senátu	senát	k1gInSc2	senát
zastoupí	zastoupit	k5eAaPmIp3nS	zastoupit
soudce	soudce	k1gMnPc4	soudce
určený	určený	k2eAgInSc1d1	určený
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
senát	senát	k1gInSc4	senát
rozvrhem	rozvrh	k1gInSc7	rozvrh
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
zastupujícím	zastupující	k2eAgMnSc7d1	zastupující
členem	člen	k1gMnSc7	člen
senátu	senát	k1gInSc2	senát
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
i	i	k9	i
předseda	předseda	k1gMnSc1	předseda
nebo	nebo	k8xC	nebo
místopředseda	místopředseda	k1gMnSc1	místopředseda
soudu	soud	k1gInSc2	soud
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
bylo	být	k5eAaImAgNnS	být
obsazení	obsazení	k1gNnSc4	obsazení
senátů	senát	k1gInPc2	senát
stabilní	stabilní	k2eAgFnSc7d1	stabilní
<g/>
,	,	kIx,	,
ke	k	k7c3	k
změnám	změna	k1gFnPc3	změna
docházelo	docházet	k5eAaImAgNnS	docházet
jen	jen	k9	jen
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
zániku	zánik	k1gInSc2	zánik
mandátu	mandát	k1gInSc2	mandát
soudce	soudce	k1gMnSc2	soudce
<g/>
,	,	kIx,	,
od	od	k7c2	od
ledna	leden	k1gInSc2	leden
2016	[number]	k4	2016
byl	být	k5eAaImAgInS	být
však	však	k9	však
zvolen	zvolen	k2eAgInSc1d1	zvolen
systém	systém	k1gInSc1	systém
rotace	rotace	k1gFnSc1	rotace
-	-	kIx~	-
každý	každý	k3xTgInSc4	každý
druhý	druhý	k4xOgInSc4	druhý
rok	rok	k1gInSc4	rok
přechází	přecházet	k5eAaImIp3nS	přecházet
dosavadní	dosavadní	k2eAgMnSc1d1	dosavadní
předseda	předseda	k1gMnSc1	předseda
senátu	senát	k1gInSc2	senát
k	k	k7c3	k
senátu	senát	k1gInSc3	senát
s	s	k7c7	s
vyšším	vysoký	k2eAgNnSc7d2	vyšší
číslem	číslo	k1gNnSc7	číslo
(	(	kIx(	(
<g/>
ze	z	k7c2	z
čtvrtého	čtvrtý	k4xOgInSc2	čtvrtý
senátu	senát	k1gInSc2	senát
k	k	k7c3	k
prvnímu	první	k4xOgMnSc3	první
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stanovených	stanovený	k2eAgInPc6d1	stanovený
závažných	závažný	k2eAgInPc6d1	závažný
nebo	nebo	k8xC	nebo
sporných	sporný	k2eAgInPc6d1	sporný
případech	případ	k1gInPc6	případ
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
Ústavní	ústavní	k2eAgInSc4d1	ústavní
soud	soud	k1gInSc4	soud
v	v	k7c6	v
plénu	plénum	k1gNnSc6	plénum
(	(	kIx(	(
<g/>
hlasují	hlasovat	k5eAaImIp3nP	hlasovat
všichni	všechen	k3xTgMnPc1	všechen
ústavní	ústavní	k2eAgMnPc1d1	ústavní
soudci	soudce	k1gMnPc1	soudce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
běžnou	běžný	k2eAgFnSc4d1	běžná
agendu	agenda	k1gFnSc4	agenda
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
senátní	senátní	k2eAgFnPc4d1	senátní
věci	věc	k1gFnPc4	věc
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
ústavní	ústavní	k2eAgMnPc1d1	ústavní
soudci	soudce	k1gMnPc1	soudce
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
rozděleni	rozdělit	k5eAaPmNgMnP	rozdělit
do	do	k7c2	do
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
senátů	senát	k1gInPc2	senát
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
rozhodují	rozhodovat	k5eAaImIp3nP	rozhodovat
samostatně	samostatně	k6eAd1	samostatně
<g/>
.	.	kIx.	.
</s>
<s>
Plénum	plénum	k1gNnSc1	plénum
je	být	k5eAaImIp3nS	být
usnášeníschopné	usnášeníschopný	k2eAgNnSc1d1	usnášeníschopné
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
nejméně	málo	k6eAd3	málo
10	[number]	k4	10
soudců	soudce	k1gMnPc2	soudce
<g/>
.	.	kIx.	.
</s>
<s>
Plénu	pléna	k1gFnSc4	pléna
jsou	být	k5eAaImIp3nP	být
zákonem	zákon	k1gInSc7	zákon
o	o	k7c6	o
Ústavním	ústavní	k2eAgInSc6d1	ústavní
soudu	soud	k1gInSc6	soud
vyhrazena	vyhradit	k5eAaPmNgFnS	vyhradit
rozhodování	rozhodování	k1gNnSc4	rozhodování
o	o	k7c4	o
zrušení	zrušení	k1gNnSc4	zrušení
zákonů	zákon	k1gInPc2	zákon
nebo	nebo	k8xC	nebo
jiných	jiný	k2eAgInPc2d1	jiný
právních	právní	k2eAgInPc2d1	právní
předpisů	předpis	k1gInPc2	předpis
nebo	nebo	k8xC	nebo
jejich	jejich	k3xOp3gNnPc1	jejich
ustanovení	ustanovení	k1gNnPc1	ustanovení
<g/>
,	,	kIx,	,
o	o	k7c6	o
ústavní	ústavní	k2eAgFnSc6d1	ústavní
žalobě	žaloba	k1gFnSc6	žaloba
Senátu	senát	k1gInSc2	senát
proti	proti	k7c3	proti
prezidentu	prezident	k1gMnSc3	prezident
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
o	o	k7c6	o
návrhu	návrh	k1gInSc6	návrh
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
na	na	k7c4	na
zrušení	zrušení	k1gNnSc4	zrušení
usnesení	usnesení	k1gNnSc2	usnesení
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
nebo	nebo	k8xC	nebo
Senátu	senát	k1gInSc2	senát
<g/>
,	,	kIx,	,
o	o	k7c6	o
ústavnosti	ústavnost	k1gFnSc6	ústavnost
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
týkajího	týkají	k2eAgNnSc2d1	týkají
se	se	k3xPyFc4	se
rozpuštění	rozpuštění	k1gNnSc2	rozpuštění
nebo	nebo	k8xC	nebo
činnosti	činnost	k1gFnSc2	činnost
politické	politický	k2eAgFnSc2d1	politická
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
o	o	k7c6	o
některých	některý	k3yIgFnPc6	některý
záležitostech	záležitost	k1gFnPc6	záležitost
týkajících	týkající	k2eAgFnPc6d1	týkající
se	se	k3xPyFc4	se
přistoupení	přistoupení	k1gNnSc3	přistoupení
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
k	k	k7c3	k
Evropské	evropský	k2eAgFnSc3d1	Evropská
unii	unie	k1gFnSc3	unie
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
plénum	plénum	k1gNnSc1	plénum
příslušné	příslušný	k2eAgNnSc1d1	příslušné
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
o	o	k7c6	o
senátních	senátní	k2eAgFnPc6d1	senátní
věcech	věc	k1gFnPc6	věc
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgInPc6	který
soudní	soudní	k2eAgInPc1d1	soudní
senát	senát	k1gInSc1	senát
nerozhodl	rozhodnout	k5eNaPmAgInS	rozhodnout
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
žádný	žádný	k3yNgInSc4	žádný
návrh	návrh	k1gInSc4	návrh
na	na	k7c4	na
usnesení	usnesení	k1gNnSc4	usnesení
nezískal	získat	k5eNaPmAgInS	získat
většinu	většina	k1gFnSc4	většina
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Plénum	plénum	k1gNnSc1	plénum
je	být	k5eAaImIp3nS	být
dále	daleko	k6eAd2	daleko
příslušné	příslušný	k2eAgNnSc1d1	příslušné
rozhodovat	rozhodovat	k5eAaImF	rozhodovat
o	o	k7c6	o
právním	právní	k2eAgInSc6d1	právní
názoru	názor	k1gInSc6	názor
senátu	senát	k1gInSc2	senát
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
odchyluje	odchylovat	k5eAaImIp3nS	odchylovat
od	od	k7c2	od
předchozího	předchozí	k2eAgInSc2d1	předchozí
nálezu	nález	k1gInSc2	nález
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
o	o	k7c6	o
obnovení	obnovení	k1gNnSc6	obnovení
řízení	řízení	k1gNnSc2	řízení
a	a	k8xC	a
o	o	k7c6	o
všech	všecek	k3xTgFnPc6	všecek
věcech	věc	k1gFnPc6	věc
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterých	který	k3yIgInPc6	který
si	se	k3xPyFc3	se
rozhodování	rozhodování	k1gNnSc1	rozhodování
vyhradí	vyhradit	k5eAaPmIp3nS	vyhradit
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
plénum	plénum	k1gNnSc1	plénum
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
o	o	k7c6	o
organizačních	organizační	k2eAgFnPc6d1	organizační
záležitostech	záležitost	k1gFnPc6	záležitost
senátu	senát	k1gInSc2	senát
<g/>
,	,	kIx,	,
jmenování	jmenování	k1gNnSc4	jmenování
senátů	senát	k1gInPc2	senát
a	a	k8xC	a
pravidlech	pravidlo	k1gNnPc6	pravidlo
rozdělení	rozdělení	k1gNnSc2	rozdělení
agendy	agenda	k1gFnSc2	agenda
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gMnPc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Senátní	senátní	k2eAgFnPc1d1	senátní
i	i	k8xC	i
plenární	plenární	k2eAgFnPc1d1	plenární
věci	věc	k1gFnPc1	věc
se	se	k3xPyFc4	se
přidělují	přidělovat	k5eAaImIp3nP	přidělovat
soudcům	soudce	k1gMnPc3	soudce
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
podle	podle	k7c2	podle
cyklu	cyklus	k1gInSc2	cyklus
vyplývajícího	vyplývající	k2eAgInSc2d1	vyplývající
z	z	k7c2	z
abecedního	abecední	k2eAgNnSc2d1	abecední
pořadí	pořadí	k1gNnSc2	pořadí
jejich	jejich	k3xOp3gNnPc2	jejich
jmen	jméno	k1gNnPc2	jméno
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
též	též	k9	též
abecedního	abecední	k2eAgNnSc2d1	abecední
pořadí	pořadí	k1gNnSc2	pořadí
jmen	jméno	k1gNnPc2	jméno
či	či	k8xC	či
názvů	název	k1gInPc2	název
navrhovatelů	navrhovatel	k1gMnPc2	navrhovatel
v	v	k7c6	v
případě	případ	k1gInSc6	případ
návrhů	návrh	k1gInPc2	návrh
podaných	podaný	k2eAgInPc2d1	podaný
během	během	k7c2	během
téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
předmět	předmět	k1gInSc4	předmět
rozhodování	rozhodování	k1gNnSc2	rozhodování
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
každém	každý	k3xTgInSc6	každý
druhém	druhý	k4xOgInSc6	druhý
přidělovacím	přidělovací	k2eAgNnSc6d1	přidělovací
kole	kolo	k1gNnSc6	kolo
se	se	k3xPyFc4	se
vynechává	vynechávat	k5eAaImIp3nS	vynechávat
předseda	předseda	k1gMnSc1	předseda
soudu	soud	k1gInSc2	soud
a	a	k8xC	a
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
čtvrtém	čtvrtý	k4xOgInSc6	čtvrtý
kole	kolo	k1gNnSc6	kolo
místopředsedové	místopředseda	k1gMnPc1	místopředseda
soudu	soud	k1gInSc2	soud
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
stanovený	stanovený	k2eAgMnSc1d1	stanovený
soudce	soudce	k1gMnSc1	soudce
zpravodaj	zpravodaj	k1gMnSc1	zpravodaj
může	moct	k5eAaImIp3nS	moct
plénu	pléna	k1gFnSc4	pléna
navrhnout	navrhnout	k5eAaPmF	navrhnout
předání	předání	k1gNnSc4	předání
případu	případ	k1gInSc2	případ
jinému	jiný	k1gMnSc3	jiný
soudci	soudce	k1gMnSc3	soudce
<g/>
,	,	kIx,	,
souhlasí	souhlasit	k5eAaImIp3nS	souhlasit
<g/>
-li	i	k?	-li
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
dostane	dostat	k5eAaPmIp3nS	dostat
soudce	soudce	k1gMnSc1	soudce
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
případu	případ	k1gInSc3	případ
takto	takto	k6eAd1	takto
zbavil	zbavit	k5eAaPmAgMnS	zbavit
<g/>
,	,	kIx,	,
náhradou	náhrada	k1gFnSc7	náhrada
ten	ten	k3xDgInSc1	ten
následující	následující	k2eAgInSc1d1	následující
případ	případ	k1gInSc1	případ
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
by	by	kYmCp3nS	by
jinak	jinak	k6eAd1	jinak
dostal	dostat	k5eAaPmAgMnS	dostat
ten	ten	k3xDgMnSc1	ten
soudce	soudce	k1gMnSc1	soudce
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
za	za	k7c4	za
něj	on	k3xPp3gInSc4	on
případ	případ	k1gInSc4	případ
převzal	převzít	k5eAaPmAgMnS	převzít
<g/>
.	.	kIx.	.
</s>
<s>
Předseda	předseda	k1gMnSc1	předseda
ÚS	ÚS	kA	ÚS
může	moct	k5eAaImIp3nS	moct
se	s	k7c7	s
souhlasem	souhlas	k1gInSc7	souhlas
pléna	plénum	k1gNnSc2	plénum
vyrovnávat	vyrovnávat	k5eAaImF	vyrovnávat
nerovnoměrnosti	nerovnoměrnost	k1gFnPc4	nerovnoměrnost
v	v	k7c4	v
zatížení	zatížení	k1gNnSc4	zatížení
soudců	soudce	k1gMnPc2	soudce
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
senátní	senátní	k2eAgFnSc1d1	senátní
věc	věc	k1gFnSc1	věc
přidělena	přidělit	k5eAaPmNgFnS	přidělit
soudci	soudce	k1gMnPc7	soudce
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
členem	člen	k1gInSc7	člen
některého	některý	k3yIgInSc2	některý
ze	z	k7c2	z
senátů	senát	k1gInPc2	senát
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gInSc1	jeho
senát	senát	k1gInSc1	senát
příslušný	příslušný	k2eAgInSc1d1	příslušný
k	k	k7c3	k
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
řízení	řízení	k1gNnSc3	řízení
o	o	k7c6	o
věcech	věc	k1gFnPc6	věc
přidělených	přidělený	k2eAgInPc2d1	přidělený
předsedovi	předseda	k1gMnSc3	předseda
ÚS	ÚS	kA	ÚS
jsou	být	k5eAaImIp3nP	být
střídavě	střídavě	k6eAd1	střídavě
příslušné	příslušný	k2eAgInPc1d1	příslušný
I.	I.	kA	I.
<g/>
,	,	kIx,	,
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
III	III	kA	III
<g/>
.	.	kIx.	.
a	a	k8xC	a
IV	IV	kA	IV
<g/>
.	.	kIx.	.
senát	senát	k1gInSc1	senát
<g/>
,	,	kIx,	,
k	k	k7c3	k
věcem	věc	k1gFnPc3	věc
přiděleným	přidělený	k2eAgFnPc3d1	přidělená
místopředsedkyni	místopředsedkyně	k1gFnSc3	místopředsedkyně
ÚS	ÚS	kA	ÚS
se	se	k3xPyFc4	se
střídá	střídat	k5eAaImIp3nS	střídat
příslušnost	příslušnost	k1gFnSc1	příslušnost
I.	I.	kA	I.
a	a	k8xC	a
II	II	kA	II
<g/>
.	.	kIx.	.
senátu	senát	k1gInSc2	senát
<g/>
,	,	kIx,	,
k	k	k7c3	k
věcem	věc	k1gFnPc3	věc
přiděleným	přidělený	k2eAgFnPc3d1	přidělená
místopředsedovi	místopředseda	k1gMnSc3	místopředseda
ÚS	ÚS	kA	ÚS
se	se	k3xPyFc4	se
střídá	střídat	k5eAaImIp3nS	střídat
příslušnost	příslušnost	k1gFnSc1	příslušnost
III	III	kA	III
<g/>
.	.	kIx.	.
a	a	k8xC	a
IV	IV	kA	IV
<g/>
.	.	kIx.	.
senátu	senát	k1gInSc2	senát
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
v	v	k7c6	v
takto	takto	k6eAd1	takto
přidělených	přidělený	k2eAgFnPc6d1	přidělená
věcech	věc	k1gFnPc6	věc
funkcionáři	funkcionář	k1gMnPc1	funkcionář
ÚS	ÚS	kA	ÚS
zastupují	zastupovat	k5eAaImIp3nP	zastupovat
soudce	soudce	k1gMnSc1	soudce
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
době	doba	k1gFnSc6	doba
nápadu	nápad	k1gInSc2	nápad
věci	věc	k1gFnSc2	věc
předsedou	předseda	k1gMnSc7	předseda
příslušného	příslušný	k2eAgInSc2d1	příslušný
senátu	senát	k1gInSc2	senát
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
hlasováním	hlasování	k1gNnSc7	hlasování
v	v	k7c6	v
plénu	plénum	k1gNnSc6	plénum
je	být	k5eAaImIp3nS	být
každý	každý	k3xTgMnSc1	každý
soudce	soudce	k1gMnSc1	soudce
oprávněn	oprávněn	k2eAgMnSc1d1	oprávněn
podat	podat	k5eAaPmF	podat
návrh	návrh	k1gInSc4	návrh
usnesení	usnesení	k1gNnSc2	usnesení
<g/>
,	,	kIx,	,
při	při	k7c6	při
hlasování	hlasování	k1gNnSc6	hlasování
pak	pak	k6eAd1	pak
každý	každý	k3xTgMnSc1	každý
soudce	soudce	k1gMnSc1	soudce
musí	muset	k5eAaImIp3nS	muset
hlasovat	hlasovat	k5eAaImF	hlasovat
pro	pro	k7c4	pro
některý	některý	k3yIgInSc4	některý
z	z	k7c2	z
podaných	podaný	k2eAgInPc2d1	podaný
návrhů	návrh	k1gInPc2	návrh
<g/>
.	.	kIx.	.
</s>
<s>
Nezíská	získat	k5eNaPmIp3nS	získat
<g/>
-li	i	k?	-li
žádný	žádný	k3yNgInSc4	žádný
z	z	k7c2	z
návrhů	návrh	k1gInPc2	návrh
většinu	většina	k1gFnSc4	většina
<g/>
,	,	kIx,	,
hlasování	hlasování	k1gNnSc1	hlasování
se	se	k3xPyFc4	se
opakuje	opakovat	k5eAaImIp3nS	opakovat
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
před	před	k7c7	před
hlasováním	hlasování	k1gNnSc7	hlasování
se	se	k3xPyFc4	se
soudci	soudce	k1gMnPc1	soudce
vyjádří	vyjádřit	k5eAaPmIp3nP	vyjádřit
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
na	na	k7c6	na
svých	svůj	k3xOyFgInPc6	svůj
návrzích	návrh	k1gInPc6	návrh
setrvávají	setrvávat	k5eAaImIp3nP	setrvávat
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
mohou	moct	k5eAaImIp3nP	moct
podat	podat	k5eAaPmF	podat
nové	nový	k2eAgInPc4d1	nový
návrhy	návrh	k1gInPc4	návrh
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
ani	ani	k8xC	ani
opakovaným	opakovaný	k2eAgNnSc7d1	opakované
hlasováním	hlasování	k1gNnSc7	hlasování
nedojde	dojít	k5eNaPmIp3nS	dojít
k	k	k7c3	k
rozhodování	rozhodování	k1gNnSc3	rozhodování
<g/>
,	,	kIx,	,
hlasuje	hlasovat	k5eAaImIp3nS	hlasovat
se	se	k3xPyFc4	se
potřetí	potřetí	k4xO	potřetí
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
pouze	pouze	k6eAd1	pouze
o	o	k7c6	o
těch	ten	k3xDgInPc6	ten
dvou	dva	k4xCgInPc6	dva
návrzích	návrh	k1gInPc6	návrh
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
hlasování	hlasování	k1gNnSc6	hlasování
získaly	získat	k5eAaPmAgFnP	získat
nejvíce	nejvíce	k6eAd1	nejvíce
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
plénu	plénum	k1gNnSc6	plénum
je	být	k5eAaImIp3nS	být
usnesení	usnesení	k1gNnSc1	usnesení
přijato	přijmout	k5eAaPmNgNnS	přijmout
<g/>
,	,	kIx,	,
hlasovala	hlasovat	k5eAaImAgFnS	hlasovat
<g/>
-li	i	k?	-li
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
většina	většina	k1gFnSc1	většina
přítomných	přítomný	k2eAgMnPc2d1	přítomný
soudců	soudce	k1gMnPc2	soudce
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
vybrané	vybraný	k2eAgInPc4d1	vybraný
druhy	druh	k1gInPc4	druh
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
zrušení	zrušení	k1gNnSc1	zrušení
zákona	zákon	k1gInSc2	zákon
či	či	k8xC	či
jeho	on	k3xPp3gNnSc2	on
ustanovení	ustanovení	k1gNnSc2	ustanovení
nebo	nebo	k8xC	nebo
revize	revize	k1gFnSc1	revize
právního	právní	k2eAgInSc2d1	právní
názoru	názor	k1gInSc2	názor
obsaženého	obsažený	k2eAgInSc2d1	obsažený
v	v	k7c6	v
dřívějším	dřívější	k2eAgInSc6d1	dřívější
nálezu	nález	k1gInSc6	nález
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
<g/>
)	)	kIx)	)
však	však	k9	však
zákon	zákon	k1gInSc1	zákon
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
souhlas	souhlas	k1gInSc4	souhlas
nejméně	málo	k6eAd3	málo
9	[number]	k4	9
ústavních	ústavní	k2eAgMnPc2d1	ústavní
soudců	soudce	k1gMnPc2	soudce
<g/>
.	.	kIx.	.
</s>
<s>
Soudce	soudce	k1gMnSc1	soudce
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
s	s	k7c7	s
usnesením	usnesení	k1gNnSc7	usnesení
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
usnesení	usnesení	k1gNnSc2	usnesení
o	o	k7c6	o
vnitřních	vnitřní	k2eAgFnPc6d1	vnitřní
věcech	věc	k1gFnPc6	věc
soudu	soud	k1gInSc2	soud
<g/>
)	)	kIx)	)
nesouhlasí	souhlasit	k5eNaImIp3nS	souhlasit
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
právo	právo	k1gNnSc4	právo
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jeho	jeho	k3xOp3gNnSc1	jeho
odlišné	odlišný	k2eAgNnSc1d1	odlišné
stanovisko	stanovisko	k1gNnSc1	stanovisko
bylo	být	k5eAaImAgNnS	být
s	s	k7c7	s
jeho	jeho	k3xOp3gNnSc7	jeho
jménem	jméno	k1gNnSc7	jméno
připojeno	připojit	k5eAaPmNgNnS	připojit
k	k	k7c3	k
protokolu	protokol	k1gInSc3	protokol
<g/>
.	.	kIx.	.
</s>
<s>
Senát	senát	k1gInSc1	senát
je	být	k5eAaImIp3nS	být
usnášeníschopný	usnášeníschopný	k2eAgInSc1d1	usnášeníschopný
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
plném	plný	k2eAgInSc6d1	plný
počtu	počet	k1gInSc6	počet
tří	tři	k4xCgInPc2	tři
členů	člen	k1gInPc2	člen
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
stálí	stálý	k2eAgMnPc1d1	stálý
členové	člen	k1gMnPc1	člen
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
zastoupeni	zastoupit	k5eAaPmNgMnP	zastoupit
jiným	jiný	k2eAgMnSc7d1	jiný
soudcem	soudce	k1gMnSc7	soudce
<g/>
.	.	kIx.	.
</s>
<s>
Senát	senát	k1gInSc1	senát
se	se	k3xPyFc4	se
usnáší	usnášet	k5eAaImIp3nS	usnášet
většinou	většina	k1gFnSc7	většina
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
odmítnutí	odmítnutí	k1gNnSc2	odmítnutí
návrhu	návrh	k1gInSc2	návrh
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
zákon	zákon	k1gInSc1	zákon
souhlas	souhlas	k1gInSc4	souhlas
všech	všecek	k3xTgInPc2	všecek
tří	tři	k4xCgInPc2	tři
členů	člen	k1gInPc2	člen
senátu	senát	k1gInSc2	senát
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
takto	takto	k6eAd1	takto
přijatý	přijatý	k2eAgInSc4d1	přijatý
právní	právní	k2eAgInSc4d1	právní
názor	názor	k1gInSc4	názor
senátu	senát	k1gInSc2	senát
odlišný	odlišný	k2eAgInSc4d1	odlišný
od	od	k7c2	od
dřívějšího	dřívější	k2eAgInSc2d1	dřívější
nálezu	nález	k1gInSc2	nález
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
otázka	otázka	k1gFnSc1	otázka
předložena	předložen	k2eAgFnSc1d1	předložena
plénu	pléna	k1gFnSc4	pléna
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc7	jeho
stanoviskem	stanovisko	k1gNnSc7	stanovisko
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
senát	senát	k1gInSc1	senát
v	v	k7c6	v
dalším	další	k2eAgNnSc6d1	další
řízení	řízení	k1gNnSc6	řízení
vázán	vázat	k5eAaImNgMnS	vázat
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
člen	člen	k1gMnSc1	člen
senátu	senát	k1gInSc2	senát
je	být	k5eAaImIp3nS	být
před	před	k7c7	před
hlasováním	hlasování	k1gNnSc7	hlasování
oprávněn	oprávněn	k2eAgMnSc1d1	oprávněn
podat	podat	k5eAaPmF	podat
návrh	návrh	k1gInSc4	návrh
usnesení	usnesení	k1gNnSc2	usnesení
a	a	k8xC	a
každý	každý	k3xTgMnSc1	každý
člen	člen	k1gMnSc1	člen
musí	muset	k5eAaImIp3nS	muset
při	při	k7c6	při
hlasování	hlasování	k1gNnSc6	hlasování
hlasovat	hlasovat	k5eAaImF	hlasovat
pro	pro	k7c4	pro
některý	některý	k3yIgInSc4	některý
z	z	k7c2	z
podaných	podaný	k2eAgInPc2d1	podaný
návrhů	návrh	k1gInPc2	návrh
<g/>
.	.	kIx.	.
</s>
<s>
Hlasuje	hlasovat	k5eAaImIp3nS	hlasovat
se	se	k3xPyFc4	se
tak	tak	k9	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
každý	každý	k3xTgMnSc1	každý
člen	člen	k1gMnSc1	člen
senátu	senát	k1gInSc2	senát
ústně	ústně	k6eAd1	ústně
sdělí	sdělit	k5eAaPmIp3nS	sdělit
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yQgInSc7	který
z	z	k7c2	z
podaných	podaný	k2eAgInPc2d1	podaný
návrhů	návrh	k1gInPc2	návrh
souhlasí	souhlasit	k5eAaImIp3nS	souhlasit
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
při	při	k7c6	při
rozhodnutí	rozhodnutí	k1gNnSc6	rozhodnutí
ve	v	k7c6	v
věci	věc	k1gFnSc6	věc
samé	samý	k3xTgNnSc1	samý
nezíská	získat	k5eNaPmIp3nS	získat
žádný	žádný	k3yNgInSc1	žádný
návrh	návrh	k1gInSc1	návrh
potřebnou	potřebný	k2eAgFnSc4d1	potřebná
většinu	většina	k1gFnSc4	většina
<g/>
,	,	kIx,	,
předloží	předložit	k5eAaPmIp3nS	předložit
předseda	předseda	k1gMnSc1	předseda
senátu	senát	k1gInSc2	senát
věc	věc	k1gFnSc1	věc
bez	bez	k7c2	bez
zbytečného	zbytečný	k2eAgInSc2d1	zbytečný
odkladu	odklad	k1gInSc2	odklad
k	k	k7c3	k
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
plénu	plénum	k1gNnSc6	plénum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ostatních	ostatní	k2eAgFnPc6d1	ostatní
věcech	věc	k1gFnPc6	věc
(	(	kIx(	(
<g/>
procedurálních	procedurální	k2eAgNnPc6d1	procedurální
rozhodnutích	rozhodnutí	k1gNnPc6	rozhodnutí
<g/>
)	)	kIx)	)
při	při	k7c6	při
rovnosti	rovnost	k1gFnSc6	rovnost
hlasů	hlas	k1gInPc2	hlas
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
hlas	hlas	k1gInSc4	hlas
předsedy	předseda	k1gMnSc2	předseda
senátu	senát	k1gInSc2	senát
<g/>
.	.	kIx.	.
</s>
<s>
Člen	člen	k1gMnSc1	člen
senátu	senát	k1gInSc2	senát
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
s	s	k7c7	s
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
ve	v	k7c6	v
věci	věc	k1gFnSc6	věc
nebo	nebo	k8xC	nebo
s	s	k7c7	s
jeho	jeho	k3xOp3gNnSc7	jeho
odůvodněním	odůvodnění	k1gNnSc7	odůvodnění
nesouhlasí	souhlasit	k5eNaImIp3nS	souhlasit
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
právo	právo	k1gNnSc4	právo
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
jeho	jeho	k3xOp3gNnSc4	jeho
odlišné	odlišný	k2eAgNnSc4d1	odlišné
stanovisko	stanovisko	k1gNnSc4	stanovisko
s	s	k7c7	s
uvedením	uvedení	k1gNnSc7	uvedení
svého	svůj	k3xOyFgNnSc2	svůj
jména	jméno	k1gNnSc2	jméno
bylo	být	k5eAaImAgNnS	být
připojeno	připojit	k5eAaPmNgNnS	připojit
k	k	k7c3	k
protokolu	protokol	k1gInSc3	protokol
o	o	k7c4	o
jednání	jednání	k1gNnSc4	jednání
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Seznam	seznam	k1gInSc4	seznam
soudců	soudce	k1gMnPc2	soudce
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
Seznam	seznam	k1gInSc1	seznam
předsedů	předseda	k1gMnPc2	předseda
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Patnáct	patnáct	k4xCc1	patnáct
soudců	soudce	k1gMnPc2	soudce
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
se	se	k3xPyFc4	se
souhlasem	souhlas	k1gInSc7	souhlas
Senátu	senát	k1gInSc2	senát
(	(	kIx(	(
<g/>
poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
uplatnilo	uplatnit	k5eAaPmAgNnS	uplatnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
funkční	funkční	k2eAgNnSc4d1	funkční
období	období	k1gNnSc4	období
ústavního	ústavní	k2eAgMnSc2d1	ústavní
soudce	soudce	k1gMnSc2	soudce
je	být	k5eAaImIp3nS	být
10	[number]	k4	10
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Soudcem	soudce	k1gMnSc7	soudce
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
jmenován	jmenován	k2eAgMnSc1d1	jmenován
bezúhonný	bezúhonný	k2eAgMnSc1d1	bezúhonný
občan	občan	k1gMnSc1	občan
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
volitelný	volitelný	k2eAgInSc1d1	volitelný
do	do	k7c2	do
Senátu	senát	k1gInSc2	senát
(	(	kIx(	(
<g/>
věk	věk	k1gInSc1	věk
nad	nad	k7c4	nad
40	[number]	k4	40
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
vysokoškolské	vysokoškolský	k2eAgNnSc4d1	vysokoškolské
právnické	právnický	k2eAgNnSc4d1	právnické
vzdělání	vzdělání	k1gNnSc4	vzdělání
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
nejméně	málo	k6eAd3	málo
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
činný	činný	k2eAgInSc1d1	činný
v	v	k7c6	v
právnickém	právnický	k2eAgNnSc6d1	právnické
povolání	povolání	k1gNnSc6	povolání
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
opakované	opakovaný	k2eAgNnSc1d1	opakované
jmenování	jmenování	k1gNnSc1	jmenování
téže	tenže	k3xDgFnSc2	tenže
osoby	osoba	k1gFnSc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Soudce	soudce	k1gMnSc1	soudce
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
se	se	k3xPyFc4	se
ujímá	ujímat	k5eAaImIp3nS	ujímat
své	svůj	k3xOyFgFnSc2	svůj
funkce	funkce	k1gFnSc2	funkce
složením	složení	k1gNnSc7	složení
tohoto	tento	k3xDgInSc2	tento
slibu	slib	k1gInSc2	slib
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Slibuji	slibovat	k5eAaImIp1nS	slibovat
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
čest	čest	k1gFnSc4	čest
a	a	k8xC	a
svědomí	svědomí	k1gNnSc4	svědomí
<g/>
,	,	kIx,	,
že	že	k8xS	že
budu	být	k5eAaImBp1nS	být
chránit	chránit	k5eAaImF	chránit
neporušitelnost	neporušitelnost	k1gFnSc4	neporušitelnost
přirozených	přirozený	k2eAgNnPc2d1	přirozené
práv	právo	k1gNnPc2	právo
člověka	člověk	k1gMnSc2	člověk
a	a	k8xC	a
práv	práv	k2eAgMnSc1d1	práv
občana	občan	k1gMnSc4	občan
<g/>
,	,	kIx,	,
řídit	řídit	k5eAaImF	řídit
se	se	k3xPyFc4	se
ústavními	ústavní	k2eAgInPc7d1	ústavní
zákony	zákon	k1gInPc7	zákon
a	a	k8xC	a
rozhodovat	rozhodovat	k5eAaImF	rozhodovat
podle	podle	k7c2	podle
svého	svůj	k3xOyFgNnSc2	svůj
nejlepšího	dobrý	k2eAgNnSc2d3	nejlepší
přesvědčení	přesvědčení	k1gNnSc2	přesvědčení
nezávisle	závisle	k6eNd1	závisle
a	a	k8xC	a
nestranně	nestranně	k6eAd1	nestranně
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Předseda	předseda	k1gMnSc1	předseda
soudu	soud	k1gInSc2	soud
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
každému	každý	k3xTgMnSc3	každý
soudci	soudce	k1gMnSc3	soudce
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
návrh	návrh	k1gInSc4	návrh
asistenty	asistent	k1gMnPc7	asistent
soudce	soudce	k1gMnSc1	soudce
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
době	doba	k1gFnSc6	doba
prezidentství	prezidentství	k1gNnPc2	prezidentství
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
prezidentem	prezident	k1gMnSc7	prezident
a	a	k8xC	a
Senátem	senát	k1gInSc7	senát
v	v	k7c6	v
otázce	otázka	k1gFnSc6	otázka
jmenování	jmenování	k1gNnSc2	jmenování
neobjevily	objevit	k5eNaPmAgFnP	objevit
vážnější	vážní	k2eAgFnPc1d2	vážnější
rozpory	rozpora	k1gFnPc1	rozpora
<g/>
,	,	kIx,	,
po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
Václava	Václav	k1gMnSc2	Václav
Klause	Klaus	k1gMnSc2	Klaus
se	se	k3xPyFc4	se
o	o	k7c6	o
jmenování	jmenování	k1gNnSc6	jmenování
ústavních	ústavní	k2eAgMnPc2d1	ústavní
soudců	soudce	k1gMnPc2	soudce
rozhořel	rozhořet	k5eAaPmAgInS	rozhořet
ostrý	ostrý	k2eAgInSc1d1	ostrý
konflikt	konflikt	k1gInSc1	konflikt
<g/>
,	,	kIx,	,
když	když	k8xS	když
Senát	senát	k1gInSc1	senát
postupně	postupně	k6eAd1	postupně
odmítl	odmítnout	k5eAaPmAgInS	odmítnout
čtyři	čtyři	k4xCgInPc4	čtyři
prezidentem	prezident	k1gMnSc7	prezident
navržené	navržený	k2eAgFnPc4d1	navržená
kandidáty	kandidát	k1gMnPc7	kandidát
<g/>
,	,	kIx,	,
Aleše	Aleš	k1gMnSc2	Aleš
Pejchala	Pejchala	k1gMnSc2	Pejchala
dokonce	dokonce	k9	dokonce
dvakrát	dvakrát	k6eAd1	dvakrát
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
Senátem	senát	k1gInSc7	senát
zamítnuté	zamítnutý	k2eAgMnPc4d1	zamítnutý
kandidáty	kandidát	k1gMnPc4	kandidát
patřili	patřit	k5eAaImAgMnP	patřit
například	například	k6eAd1	například
Václav	Václav	k1gMnSc1	Václav
Pavlíček	Pavlíček	k1gMnSc1	Pavlíček
(	(	kIx(	(
<g/>
22	[number]	k4	22
hlasů	hlas	k1gInPc2	hlas
ze	z	k7c2	z
76	[number]	k4	76
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Balaš	Balaš	k1gMnSc1	Balaš
(	(	kIx(	(
<g/>
30	[number]	k4	30
ze	z	k7c2	z
78	[number]	k4	78
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Klára	Klára	k1gFnSc1	Klára
Samková	Samková	k1gFnSc1	Samková
(	(	kIx(	(
<g/>
29	[number]	k4	29
ze	z	k7c2	z
77	[number]	k4	77
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
Galvas	Galvas	k1gMnSc1	Galvas
(	(	kIx(	(
<g/>
25	[number]	k4	25
ze	z	k7c2	z
70	[number]	k4	70
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
žádost	žádost	k1gFnSc4	žádost
Senátu	senát	k1gInSc2	senát
o	o	k7c4	o
navrhování	navrhování	k1gNnSc4	navrhování
většího	veliký	k2eAgInSc2d2	veliký
počtu	počet	k1gInSc2	počet
lépe	dobře	k6eAd2	dobře
vyhovujících	vyhovující	k2eAgMnPc2d1	vyhovující
kandidátů	kandidát	k1gMnPc2	kandidát
označil	označit	k5eAaPmAgInS	označit
za	za	k7c4	za
skandální	skandální	k2eAgNnSc4d1	skandální
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Senát	senát	k1gInSc1	senát
prezidentovi	prezident	k1gMnSc3	prezident
vytkl	vytknout	k5eAaPmAgMnS	vytknout
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
své	svůj	k3xOyFgInPc4	svůj
návrhy	návrh	k1gInPc4	návrh
předem	předem	k6eAd1	předem
nekonzultuje	konzultovat	k5eNaImIp3nS	konzultovat
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
července	červenec	k1gInSc2	červenec
2003	[number]	k4	2003
do	do	k7c2	do
prosince	prosinec	k1gInSc2	prosinec
2005	[number]	k4	2005
tak	tak	k6eAd1	tak
Ústavní	ústavní	k2eAgInSc1d1	ústavní
soud	soud	k1gInSc1	soud
neměl	mít	k5eNaImAgInS	mít
plný	plný	k2eAgInSc1d1	plný
počet	počet	k1gInSc1	počet
členů	člen	k1gMnPc2	člen
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
čemuž	což	k3yQnSc3	což
například	například	k6eAd1	například
senátor	senátor	k1gMnSc1	senátor
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Bárta	Bárta	k1gMnSc1	Bárta
označil	označit	k5eAaPmAgMnS	označit
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
chyběli	chybět	k5eAaImAgMnP	chybět
čtyři	čtyři	k4xCgMnPc1	čtyři
soudci	soudce	k1gMnPc1	soudce
a	a	k8xC	a
soud	soud	k1gInSc1	soud
tedy	tedy	k9	tedy
nemohl	moct	k5eNaImAgInS	moct
rozhodovat	rozhodovat	k5eAaImF	rozhodovat
závažnější	závažný	k2eAgFnPc4d2	závažnější
věci	věc	k1gFnPc4	věc
v	v	k7c6	v
plénu	plénum	k1gNnSc6	plénum
<g/>
,	,	kIx,	,
prezidenta	prezident	k1gMnSc2	prezident
Klause	Klaus	k1gMnSc2	Klaus
za	za	k7c4	za
velezrádce	velezrádce	k1gMnSc4	velezrádce
<g/>
,	,	kIx,	,
Richard	Richard	k1gMnSc1	Richard
Falbr	Falbr	k1gMnSc1	Falbr
hovořil	hovořit	k5eAaImAgMnS	hovořit
o	o	k7c4	o
"	"	kIx"	"
<g/>
ústavní	ústavní	k2eAgFnSc4d1	ústavní
krizi	krize	k1gFnSc4	krize
zaviněné	zaviněný	k2eAgFnSc2d1	zaviněná
jen	jen	k8xS	jen
Klausem	Klaus	k1gMnSc7	Klaus
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
Senát	senát	k1gInSc1	senát
nesouhlasil	souhlasit	k5eNaImAgInS	souhlasit
se	s	k7c7	s
jmenováním	jmenování	k1gNnSc7	jmenování
Klausem	Klaus	k1gMnSc7	Klaus
navrženého	navržený	k2eAgMnSc2d1	navržený
Jana	Jan	k1gMnSc2	Jan
Sváčka	Sváček	k1gMnSc2	Sváček
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Koudelky	Koudelka	k1gMnSc2	Koudelka
<g/>
,	,	kIx,	,
počet	počet	k1gInSc1	počet
soudců	soudce	k1gMnPc2	soudce
proto	proto	k8xC	proto
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2012	[number]	k4	2012
klesl	klesnout	k5eAaPmAgInS	klesnout
na	na	k7c4	na
13	[number]	k4	13
<g/>
.	.	kIx.	.
</s>
<s>
Novému	nový	k2eAgMnSc3d1	nový
prezidentovi	prezident	k1gMnSc3	prezident
Miloši	Miloš	k1gMnSc3	Miloš
Zemanovi	Zeman	k1gMnSc3	Zeman
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
Ústavní	ústavní	k2eAgInSc4d1	ústavní
soud	soud	k1gInSc4	soud
doplnit	doplnit	k5eAaPmF	doplnit
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
navržený	navržený	k2eAgMnSc1d1	navržený
Jan	Jan	k1gMnSc1	Jan
Sváček	Sváček	k1gMnSc1	Sváček
ale	ale	k8xC	ale
opět	opět	k6eAd1	opět
neprošel	projít	k5eNaPmAgInS	projít
(	(	kIx(	(
<g/>
30	[number]	k4	30
hlasů	hlas	k1gInPc2	hlas
ze	z	k7c2	z
75	[number]	k4	75
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
jmenování	jmenování	k1gNnSc4	jmenování
tří	tři	k4xCgMnPc2	tři
bývalých	bývalý	k2eAgMnPc2d1	bývalý
politiků	politik	k1gMnPc2	politik
(	(	kIx(	(
<g/>
Pavla	Pavel	k1gMnSc2	Pavel
Rychetského	Rychetský	k1gMnSc2	Rychetský
<g/>
,	,	kIx,	,
Miloslava	Miloslav	k1gMnSc2	Miloslav
Výborného	Výborný	k1gMnSc2	Výborný
a	a	k8xC	a
Dagmar	Dagmar	k1gFnSc2	Dagmar
Lastovecké	Lastovecký	k2eAgFnSc2d1	Lastovecká
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
pochybnosti	pochybnost	k1gFnPc1	pochybnost
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
nově	nově	k6eAd1	nově
obsazený	obsazený	k2eAgInSc1d1	obsazený
Ústavní	ústavní	k2eAgInSc1d1	ústavní
soud	soud	k1gInSc1	soud
bude	být	k5eAaImBp3nS	být
imunní	imunní	k2eAgInSc1d1	imunní
vůči	vůči	k7c3	vůči
vlivu	vliv	k1gInSc3	vliv
politických	politický	k2eAgFnPc2d1	politická
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2010	[number]	k4	2010
Ústavní	ústavní	k2eAgInSc1d1	ústavní
soud	soud	k1gInSc1	soud
zveřejnil	zveřejnit	k5eAaPmAgInS	zveřejnit
politickou	politický	k2eAgFnSc4d1	politická
minulost	minulost	k1gFnSc4	minulost
svých	svůj	k3xOyFgMnPc2	svůj
soudců	soudce	k1gMnPc2	soudce
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2009	[number]	k4	2009
sám	sám	k3xTgMnSc1	sám
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
veřejnost	veřejnost	k1gFnSc1	veřejnost
má	mít	k5eAaImIp3nS	mít
právo	právo	k1gNnSc4	právo
znát	znát	k5eAaImF	znát
politickou	politický	k2eAgFnSc4d1	politická
minulost	minulost	k1gFnSc4	minulost
soudců	soudce	k1gMnPc2	soudce
na	na	k7c6	na
všech	všecek	k3xTgFnPc6	všecek
úrovních	úroveň	k1gFnPc6	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
těchto	tento	k3xDgInPc2	tento
údajů	údaj	k1gInPc2	údaj
čtyři	čtyři	k4xCgMnPc4	čtyři
ústavní	ústavní	k2eAgMnPc1d1	ústavní
soudci	soudce	k1gMnPc1	soudce
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
členy	člen	k1gInPc4	člen
KSČ	KSČ	kA	KSČ
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc4	dva
byli	být	k5eAaImAgMnP	být
členy	člen	k1gInPc4	člen
ČSL	ČSL	kA	ČSL
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
soudkyně	soudkyně	k1gFnSc1	soudkyně
v	v	k7c6	v
ODA	ODA	kA	ODA
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
soudce	soudce	k1gMnSc1	soudce
v	v	k7c6	v
KAN	KAN	kA	KAN
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
soudkyně	soudkyně	k1gFnSc1	soudkyně
v	v	k7c6	v
ODS	ODS	kA	ODS
<g/>
.	.	kIx.	.
</s>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Rychetský	Rychetský	k1gMnSc1	Rychetský
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
KSČ	KSČ	kA	KSČ
<g/>
,	,	kIx,	,
OF	OF	kA	OF
<g/>
,	,	kIx,	,
OH	OH	kA	OH
a	a	k8xC	a
ČSSD	ČSSD	kA	ČSSD
<g/>
.	.	kIx.	.
</s>
<s>
Šest	šest	k4xCc1	šest
tehdejších	tehdejší	k2eAgMnPc2d1	tehdejší
ústavních	ústavní	k2eAgMnPc2d1	ústavní
soudců	soudce	k1gMnPc2	soudce
nikdy	nikdy	k6eAd1	nikdy
v	v	k7c6	v
žádné	žádný	k3yNgFnSc6	žádný
politické	politický	k2eAgFnSc6d1	politická
straně	strana	k1gFnSc6	strana
nebylo	být	k5eNaImAgNnS	být
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
soud	soud	k1gInSc1	soud
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
prvním	první	k4xOgNnSc6	první
veřejném	veřejný	k2eAgNnSc6d1	veřejné
zasedání	zasedání	k1gNnSc6	zasedání
zamítl	zamítnout	k5eAaPmAgInS	zamítnout
návrh	návrh	k1gInSc1	návrh
skupiny	skupina	k1gFnSc2	skupina
41	[number]	k4	41
opozičních	opoziční	k2eAgMnPc2d1	opoziční
poslanců	poslanec	k1gMnPc2	poslanec
na	na	k7c4	na
zrušení	zrušení	k1gNnSc4	zrušení
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
protiprávnosti	protiprávnost	k1gFnSc6	protiprávnost
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
a	a	k8xC	a
o	o	k7c6	o
odporu	odpor	k1gInSc6	odpor
proti	proti	k7c3	proti
němu	on	k3xPp3gInSc3	on
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
prezidenta	prezident	k1gMnSc4	prezident
Václava	Václav	k1gMnSc4	Václav
Havla	Havel	k1gMnSc4	Havel
zrušil	zrušit	k5eAaPmAgMnS	zrušit
část	část	k1gFnSc4	část
trestního	trestní	k2eAgInSc2d1	trestní
zákona	zákon	k1gInSc2	zákon
týkajícího	týkající	k2eAgInSc2d1	týkající
se	se	k3xPyFc4	se
hanobení	hanobení	k1gNnSc4	hanobení
parlamentu	parlament	k1gInSc2	parlament
<g/>
,	,	kIx,	,
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
<g/>
,	,	kIx,	,
trestnost	trestnost	k1gFnSc1	trestnost
hanobení	hanobení	k1gNnSc2	hanobení
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
hlavy	hlava	k1gFnSc2	hlava
státu	stát	k1gInSc2	stát
ponechal	ponechat	k5eAaPmAgMnS	ponechat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
koaličních	koaliční	k2eAgMnPc2d1	koaliční
poslanců	poslanec	k1gMnPc2	poslanec
zrušil	zrušit	k5eAaPmAgInS	zrušit
podmínku	podmínka	k1gFnSc4	podmínka
trvalého	trvalý	k2eAgInSc2d1	trvalý
pobytu	pobyt	k1gInSc2	pobyt
na	na	k7c6	na
území	území	k1gNnSc6	území
ČR	ČR	kA	ČR
pro	pro	k7c4	pro
restituenty	restituent	k1gMnPc4	restituent
nezemědělského	zemědělský	k2eNgInSc2d1	nezemědělský
majetku	majetek	k1gInSc2	majetek
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
zrušil	zrušit	k5eAaPmAgInS	zrušit
i	i	k9	i
původně	původně	k6eAd1	původně
dané	daný	k2eAgFnPc4d1	daná
lhůty	lhůta	k1gFnPc4	lhůta
pro	pro	k7c4	pro
uplatnění	uplatnění	k1gNnSc4	uplatnění
restitučních	restituční	k2eAgInPc2d1	restituční
nároků	nárok	k1gInPc2	nárok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
zamítl	zamítnout	k5eAaPmAgInS	zamítnout
návrh	návrh	k1gInSc4	návrh
na	na	k7c4	na
zrušení	zrušení	k1gNnSc4	zrušení
Benešova	Benešův	k2eAgInSc2d1	Benešův
dekretu	dekret	k1gInSc2	dekret
o	o	k7c6	o
konfiskaci	konfiskace	k1gFnSc6	konfiskace
nepřátelského	přátelský	k2eNgInSc2d1	nepřátelský
majetku	majetek	k1gInSc2	majetek
a	a	k8xC	a
fondech	fond	k1gInPc6	fond
národní	národní	k2eAgFnSc2d1	národní
obnovy	obnova	k1gFnSc2	obnova
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
kauza	kauza	k1gFnSc1	kauza
Dreithaler	Dreithaler	k1gMnSc1	Dreithaler
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zrušil	zrušit	k5eAaPmAgInS	zrušit
zákaz	zákaz	k1gInSc1	zákaz
podnikání	podnikání	k1gNnSc2	podnikání
politických	politický	k2eAgFnPc2d1	politická
stran	strana	k1gFnPc2	strana
a	a	k8xC	a
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
kontrolní	kontrolní	k2eAgInSc1d1	kontrolní
úřad	úřad	k1gInSc1	úřad
nesmí	smět	k5eNaImIp3nS	smět
kontrolovat	kontrolovat	k5eAaImF	kontrolovat
hospodaření	hospodaření	k1gNnSc1	hospodaření
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
a	a	k8xC	a
zrušil	zrušit	k5eAaPmAgInS	zrušit
podmínku	podmínka	k1gFnSc4	podmínka
trvalého	trvalý	k2eAgInSc2d1	trvalý
pobytu	pobyt	k1gInSc2	pobyt
na	na	k7c6	na
území	území	k1gNnSc6	území
ČR	ČR	kA	ČR
pro	pro	k7c4	pro
restituenty	restituent	k1gMnPc4	restituent
zemědělského	zemědělský	k2eAgInSc2d1	zemědělský
majetku	majetek	k1gInSc2	majetek
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
zrušil	zrušit	k5eAaPmAgInS	zrušit
i	i	k9	i
původně	původně	k6eAd1	původně
dané	daný	k2eAgFnPc4d1	daná
lhůty	lhůta	k1gFnPc4	lhůta
pro	pro	k7c4	pro
uplatnění	uplatnění	k1gNnSc4	uplatnění
restitučních	restituční	k2eAgInPc2d1	restituční
nároků	nárok	k1gInPc2	nárok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
potvrdil	potvrdit	k5eAaPmAgInS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nárok	nárok	k1gInSc1	nárok
na	na	k7c4	na
restituci	restituce	k1gFnSc4	restituce
mají	mít	k5eAaImIp3nP	mít
občané	občan	k1gMnPc1	občan
německé	německý	k2eAgFnSc2d1	německá
a	a	k8xC	a
maďarské	maďarský	k2eAgFnSc2d1	maďarská
národnosti	národnost	k1gFnSc2	národnost
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
za	za	k7c2	za
okupace	okupace	k1gFnSc2	okupace
po	po	k7c6	po
zbyli	zbýt	k5eAaPmAgMnP	zbýt
československé	československý	k2eAgNnSc4d1	Československé
státní	státní	k2eAgNnSc4d1	státní
občanství	občanství	k1gNnSc4	občanství
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
je	být	k5eAaImIp3nS	být
opět	opět	k6eAd1	opět
nabyli	nabýt	k5eAaPmAgMnP	nabýt
<g/>
;	;	kIx,	;
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
celková	celkový	k2eAgFnSc1d1	celková
doba	doba	k1gFnSc1	doba
zadržení	zadržení	k1gNnSc2	zadržení
v	v	k7c6	v
případě	případ	k1gInSc6	případ
trestního	trestní	k2eAgNnSc2d1	trestní
stíhání	stíhání	k1gNnSc2	stíhání
nesmí	smět	k5eNaImIp3nS	smět
překročit	překročit	k5eAaPmF	překročit
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
do	do	k7c2	do
lhůty	lhůta	k1gFnSc2	lhůta
se	se	k3xPyFc4	se
započítává	započítávat	k5eAaImIp3nS	započítávat
i	i	k9	i
doba	doba	k1gFnSc1	doba
<g/>
,	,	kIx,	,
po	po	k7c4	po
niž	jenž	k3xRgFnSc4	jenž
byla	být	k5eAaImAgFnS	být
dotčená	dotčený	k2eAgFnSc1d1	dotčená
osoba	osoba	k1gFnSc1	osoba
zadržována	zadržován	k2eAgFnSc1d1	zadržována
Policií	policie	k1gFnSc7	policie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
vyjasnil	vyjasnit	k5eAaPmAgMnS	vyjasnit
počítání	počítání	k1gNnSc3	počítání
času	čas	k1gInSc2	čas
v	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
ústavním	ústavní	k2eAgNnSc6d1	ústavní
právu	právo	k1gNnSc6	právo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
Dagmar	Dagmar	k1gFnSc1	Dagmar
Lastovecká	Lastovecký	k2eAgFnSc1d1	Lastovecká
byla	být	k5eAaImAgFnS	být
platně	platně	k6eAd1	platně
zvolena	zvolit	k5eAaPmNgFnS	zvolit
senátorkou	senátorka	k1gFnSc7	senátorka
<g/>
,	,	kIx,	,
když	když	k8xS	když
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
označil	označit	k5eAaPmAgMnS	označit
její	její	k3xOp3gFnSc4	její
volbu	volba	k1gFnSc4	volba
za	za	k7c4	za
neplatnou	platný	k2eNgFnSc4d1	neplatná
<g/>
;	;	kIx,	;
zrušil	zrušit	k5eAaPmAgInS	zrušit
odejmutí	odejmutí	k1gNnSc4	odejmutí
tzv.	tzv.	kA	tzv.
čtrnáctého	čtrnáctý	k4xOgNnSc2	čtrnáctý
platu	plat	k1gInSc3	plat
soudcům	soudce	k1gMnPc3	soudce
za	za	k7c4	za
rok	rok	k1gInSc4	rok
1998	[number]	k4	1998
a	a	k8xC	a
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
Demokratické	demokratický	k2eAgFnSc2d1	demokratická
unie	unie	k1gFnSc2	unie
zrušil	zrušit	k5eAaPmAgMnS	zrušit
podmínku	podmínka	k1gFnSc4	podmínka
nejméně	málo	k6eAd3	málo
3	[number]	k4	3
<g/>
%	%	kIx~	%
získaných	získaný	k2eAgInPc2d1	získaný
hlasů	hlas	k1gInPc2	hlas
nutnou	nutný	k2eAgFnSc4d1	nutná
pro	pro	k7c4	pro
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
politická	politický	k2eAgFnSc1d1	politická
strana	strana	k1gFnSc1	strana
získala	získat	k5eAaPmAgFnS	získat
od	od	k7c2	od
státu	stát	k1gInSc2	stát
příspěvek	příspěvek	k1gInSc4	příspěvek
na	na	k7c4	na
úhradu	úhrada	k1gFnSc4	úhrada
volebních	volební	k2eAgInPc2d1	volební
nákladů	náklad	k1gInPc2	náklad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
přiznal	přiznat	k5eAaPmAgMnS	přiznat
ČSSD	ČSSD	kA	ČSSD
právo	právo	k1gNnSc4	právo
na	na	k7c6	na
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
Lidového	lidový	k2eAgInSc2d1	lidový
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
zrušil	zrušit	k5eAaPmAgMnS	zrušit
části	část	k1gFnPc4	část
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
obcích	obec	k1gFnPc6	obec
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
nichž	jenž	k3xRgFnPc2	jenž
mohla	moct	k5eAaImAgFnS	moct
Poslanecká	poslanecký	k2eAgFnSc1d1	Poslanecká
sněmovna	sněmovna	k1gFnSc1	sněmovna
rušit	rušit	k5eAaImF	rušit
sporná	sporný	k2eAgFnSc1d1	sporná
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
obecních	obecní	k2eAgFnPc2d1	obecní
samospráv	samospráva	k1gFnPc2	samospráva
<g/>
;	;	kIx,	;
zrušil	zrušit	k5eAaPmAgInS	zrušit
vyhlášku	vyhláška	k1gFnSc4	vyhláška
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
financí	finance	k1gFnPc2	finance
o	o	k7c4	o
regulaci	regulace	k1gFnSc4	regulace
nájemného	nájemné	k1gNnSc2	nájemné
z	z	k7c2	z
bytů	byt	k1gInPc2	byt
a	a	k8xC	a
nevyhověl	vyhovět	k5eNaPmAgInS	vyhovět
žádostem	žádost	k1gFnPc3	žádost
na	na	k7c4	na
přiznání	přiznání	k1gNnSc4	přiznání
tzv.	tzv.	kA	tzv.
čtrnáctého	čtrnáctý	k4xOgNnSc2	čtrnáctý
platu	plat	k1gInSc3	plat
soudcům	soudce	k1gMnPc3	soudce
za	za	k7c4	za
roky	rok	k1gInPc1	rok
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
a	a	k8xC	a
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
rozsah	rozsah	k1gInSc1	rozsah
ústavního	ústavní	k2eAgInSc2d1	ústavní
pořádku	pořádek	k1gInSc2	pořádek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
soud	soud	k1gInSc1	soud
vyjasnil	vyjasnit	k5eAaPmAgInS	vyjasnit
vztah	vztah	k1gInSc4	vztah
mezi	mezi	k7c7	mezi
národním	národní	k2eAgNnSc7d1	národní
a	a	k8xC	a
komunitárním	komunitární	k2eAgNnSc7d1	komunitární
právem	právo	k1gNnSc7	právo
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
kauze	kauza	k1gFnSc6	kauza
cukerné	cukerný	k2eAgFnPc1d1	cukerná
kvóty	kvóta	k1gFnPc1	kvóta
III	III	kA	III
či	či	k8xC	či
ve	v	k7c6	v
věci	věc	k1gFnSc6	věc
návrhu	návrh	k1gInSc2	návrh
na	na	k7c4	na
zrušení	zrušení	k1gNnSc4	zrušení
eurozatykače	eurozatykač	k1gInSc2	eurozatykač
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2007	[number]	k4	2007
a	a	k8xC	a
2008	[number]	k4	2008
soud	soud	k1gInSc1	soud
řešil	řešit	k5eAaImAgInS	řešit
problematiku	problematika	k1gFnSc4	problematika
tzv.	tzv.	kA	tzv.
legislativních	legislativní	k2eAgInPc2d1	legislativní
přílepků	přílepek	k1gInPc2	přílepek
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
významné	významný	k2eAgFnPc4d1	významná
kauzy	kauza	k1gFnPc4	kauza
patřilo	patřit	k5eAaImAgNnS	patřit
například	například	k6eAd1	například
rozhodování	rozhodování	k1gNnSc1	rozhodování
o	o	k7c6	o
souladu	soulad	k1gInSc6	soulad
Lisabonské	lisabonský	k2eAgFnSc2d1	Lisabonská
smlouvy	smlouva	k1gFnSc2	smlouva
s	s	k7c7	s
ústavním	ústavní	k2eAgInSc7d1	ústavní
pořádkem	pořádek	k1gInSc7	pořádek
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
na	na	k7c4	na
senátní	senátní	k2eAgInSc4d1	senátní
podnět	podnět	k1gInSc4	podnět
z	z	k7c2	z
dubna	duben	k1gInSc2	duben
2008	[number]	k4	2008
vydal	vydat	k5eAaPmAgInS	vydat
soud	soud	k1gInSc1	soud
nález	nález	k1gInSc1	nález
ze	z	k7c2	z
dne	den	k1gInSc2	den
26	[number]	k4	26
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
na	na	k7c4	na
podnět	podnět	k1gInSc4	podnět
skupiny	skupina	k1gFnSc2	skupina
senátorů	senátor	k1gMnPc2	senátor
pak	pak	k6eAd1	pak
další	další	k2eAgInSc4d1	další
nález	nález	k1gInSc4	nález
ze	z	k7c2	z
dne	den	k1gInSc2	den
3	[number]	k4	3
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Průlomovým	průlomový	k2eAgNnSc7d1	průlomové
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
bylo	být	k5eAaImAgNnS	být
zrušení	zrušení	k1gNnSc1	zrušení
ústavního	ústavní	k2eAgInSc2d1	ústavní
zákona	zákon	k1gInSc2	zákon
o	o	k7c4	o
zkrácení	zkrácení	k1gNnSc4	zkrácení
volebního	volební	k2eAgNnSc2d1	volební
období	období	k1gNnSc2	období
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
Ústavní	ústavní	k2eAgInSc1d1	ústavní
soud	soud	k1gInSc1	soud
v	v	k7c6	v
září	září	k1gNnSc6	září
2009	[number]	k4	2009
zrušil	zrušit	k5eAaPmAgInS	zrušit
již	již	k6eAd1	již
vyhlášené	vyhlášený	k2eAgFnPc1d1	vyhlášená
a	a	k8xC	a
chystané	chystaný	k2eAgFnPc1d1	chystaná
předčasné	předčasný	k2eAgFnPc1d1	předčasná
volby	volba	k1gFnPc1	volba
do	do	k7c2	do
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
politici	politik	k1gMnPc1	politik
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Jiří	Jiří	k1gMnSc1	Jiří
Paroubek	Paroubka	k1gFnPc2	Paroubka
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Pospíšil	Pospíšil	k1gMnSc1	Pospíšil
nebo	nebo	k8xC	nebo
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Jičínský	jičínský	k2eAgMnSc1d1	jičínský
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
s	s	k7c7	s
různými	různý	k2eAgNnPc7d1	různé
zdůvodněními	zdůvodnění	k1gNnPc7	zdůvodnění
označily	označit	k5eAaPmAgFnP	označit
za	za	k7c4	za
překročení	překročení	k1gNnSc4	překročení
pravomocí	pravomoc	k1gFnPc2	pravomoc
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2008	[number]	k4	2008
Ústavní	ústavní	k2eAgInSc1d1	ústavní
soud	soud	k1gInSc1	soud
v	v	k7c6	v
plénu	plénum	k1gNnSc6	plénum
velmi	velmi	k6eAd1	velmi
těsným	těsný	k2eAgInSc7d1	těsný
poměrem	poměr	k1gInSc7	poměr
hlasů	hlas	k1gInPc2	hlas
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
:	:	kIx,	:
<g/>
7	[number]	k4	7
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
placení	placení	k1gNnSc4	placení
tzv.	tzv.	kA	tzv.
regulačních	regulační	k2eAgInPc2d1	regulační
poplatků	poplatek	k1gInPc2	poplatek
ve	v	k7c6	v
zdravotnictví	zdravotnictví	k1gNnSc6	zdravotnictví
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
ústavní	ústavní	k2eAgFnSc7d1	ústavní
garancí	garance	k1gFnSc7	garance
bezplatné	bezplatný	k2eAgFnSc2d1	bezplatná
zdravotní	zdravotní	k2eAgFnSc2d1	zdravotní
péče	péče	k1gFnSc2	péče
na	na	k7c6	na
základě	základ	k1gInSc6	základ
pojištění	pojištění	k1gNnSc2	pojištění
(	(	kIx(	(
<g/>
čl	čl	kA	čl
<g/>
.	.	kIx.	.
31	[number]	k4	31
Listiny	listina	k1gFnSc2	listina
základních	základní	k2eAgNnPc2d1	základní
práv	právo	k1gNnPc2	právo
a	a	k8xC	a
svobod	svoboda	k1gFnPc2	svoboda
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Občané	občan	k1gMnPc1	občan
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
základě	základ	k1gInSc6	základ
veřejného	veřejný	k2eAgNnSc2d1	veřejné
pojištění	pojištění	k1gNnSc2	pojištění
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
bezplatnou	bezplatný	k2eAgFnSc4d1	bezplatná
zdravotní	zdravotní	k2eAgFnSc4d1	zdravotní
péči	péče	k1gFnSc4	péče
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
na	na	k7c4	na
zdravotní	zdravotní	k2eAgFnPc4d1	zdravotní
pomůcky	pomůcka	k1gFnPc4	pomůcka
za	za	k7c2	za
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
stanoví	stanovit	k5eAaPmIp3nP	stanovit
zákon	zákon	k1gInSc4	zákon
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
relativizovaný	relativizovaný	k2eAgMnSc1d1	relativizovaný
ustanovením	ustanovení	k1gNnSc7	ustanovení
čl	čl	kA	čl
<g/>
.	.	kIx.	.
4	[number]	k4	4
odst	odsta	k1gFnPc2	odsta
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Při	při	k7c6	při
používání	používání	k1gNnSc6	používání
ustanovení	ustanovení	k1gNnSc2	ustanovení
o	o	k7c6	o
mezích	mez	k1gFnPc6	mez
základních	základní	k2eAgNnPc2d1	základní
práv	právo	k1gNnPc2	právo
a	a	k8xC	a
svobod	svoboda	k1gFnPc2	svoboda
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
šetřeno	šetřen	k2eAgNnSc1d1	šetřeno
jejich	jejich	k3xOp3gFnPc4	jejich
podstaty	podstata	k1gFnPc4	podstata
a	a	k8xC	a
smyslu	smysl	k1gInSc6	smysl
<g/>
.	.	kIx.	.
</s>
<s>
Taková	takový	k3xDgNnPc1	takový
omezení	omezení	k1gNnPc1	omezení
nesmějí	smát	k5eNaImIp3nP	smát
být	být	k5eAaImF	být
zneužívána	zneužíván	k2eAgNnPc4d1	zneužíváno
k	k	k7c3	k
jiným	jiný	k2eAgInPc3d1	jiný
účelům	účel	k1gInPc3	účel
<g/>
,	,	kIx,	,
než	než	k8xS	než
pro	pro	k7c4	pro
jaké	jaký	k3yIgInPc4	jaký
byla	být	k5eAaImAgFnS	být
stanovena	stanovit	k5eAaPmNgFnS	stanovit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
Většinové	většinový	k2eAgNnSc4d1	většinové
stanovisko	stanovisko	k1gNnSc4	stanovisko
soudu	soud	k1gInSc2	soud
sepsal	sepsat	k5eAaPmAgMnS	sepsat
zpravodaj	zpravodaj	k1gMnSc1	zpravodaj
Stanislav	Stanislav	k1gMnSc1	Stanislav
Balík	Balík	k1gMnSc1	Balík
a	a	k8xC	a
klíčovým	klíčový	k2eAgInSc7d1	klíčový
argumentem	argument	k1gInSc7	argument
soudcovská	soudcovský	k2eAgFnSc1d1	soudcovská
zdrženlivost	zdrženlivost	k1gFnSc1	zdrženlivost
a	a	k8xC	a
minimalizace	minimalizace	k1gFnSc1	minimalizace
zásahů	zásah	k1gInPc2	zásah
do	do	k7c2	do
pravomocí	pravomoc	k1gFnPc2	pravomoc
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
zákonodárce	zákonodárce	k1gMnSc1	zákonodárce
v	v	k7c6	v
otázkách	otázka	k1gFnPc6	otázka
"	"	kIx"	"
<g/>
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
politických	politický	k2eAgFnPc2d1	politická
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
primárně	primárně	k6eAd1	primárně
spadá	spadat	k5eAaImIp3nS	spadat
i	i	k9	i
celá	celý	k2eAgFnSc1d1	celá
oblast	oblast	k1gFnSc1	oblast
tzv.	tzv.	kA	tzv.
sociálních	sociální	k2eAgNnPc2d1	sociální
práv	právo	k1gNnPc2	právo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ústavní	ústavní	k2eAgInSc1d1	ústavní
soud	soud	k1gInSc1	soud
ve	v	k7c6	v
zdůvodnění	zdůvodnění	k1gNnSc6	zdůvodnění
dodal	dodat	k5eAaPmAgMnS	dodat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
postupoval	postupovat	k5eAaImAgMnS	postupovat
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
jakékoliv	jakýkoliv	k3yIgFnSc3	jakýkoliv
reformě	reforma	k1gFnSc3	reforma
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
i	i	k9	i
reformě	reforma	k1gFnSc3	reforma
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
<g/>
,	,	kIx,	,
příliš	příliš	k6eAd1	příliš
aktivisticky	aktivisticky	k6eAd1	aktivisticky
<g/>
,	,	kIx,	,
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
by	by	kYmCp3nS	by
zajisté	zajisté	k9	zajisté
posléze	posléze	k6eAd1	posléze
judikaturu	judikatura	k1gFnSc4	judikatura
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
a	a	k9	a
priori	priori	k6eAd1	priori
zavírala	zavírat	k5eAaImAgFnS	zavírat
dveře	dveře	k1gFnPc4	dveře
jakýmkoliv	jakýkoliv	k3yIgFnPc3	jakýkoliv
reformním	reformní	k2eAgFnPc3d1	reformní
snahám	snaha	k1gFnPc3	snaha
<g/>
.	.	kIx.	.
</s>
<s>
Prakticky	prakticky	k6eAd1	prakticky
stejnou	stejný	k2eAgFnSc4d1	stejná
situaci	situace	k1gFnSc4	situace
posuzoval	posuzovat	k5eAaImAgMnS	posuzovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
i	i	k8xC	i
slovenský	slovenský	k2eAgInSc1d1	slovenský
ústavní	ústavní	k2eAgInSc1d1	ústavní
soud	soud	k1gInSc1	soud
a	a	k8xC	a
k	k	k7c3	k
volnému	volný	k2eAgInSc3d1	volný
výkladu	výklad	k1gInSc3	výklad
ústavy	ústava	k1gFnSc2	ústava
(	(	kIx(	(
<g/>
připouštějícímu	připouštějící	k2eAgNnSc3d1	připouštějící
regulační	regulační	k2eAgInSc4d1	regulační
poplatky	poplatek	k1gInPc4	poplatek
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
přiklonil	přiklonit	k5eAaPmAgInS	přiklonit
poměrem	poměr	k1gInSc7	poměr
hlasů	hlas	k1gInPc2	hlas
9	[number]	k4	9
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
pro	pro	k7c4	pro
nález	nález	k1gInSc4	nález
byli	být	k5eAaImAgMnP	být
<g/>
:	:	kIx,	:
Stanislav	Stanislav	k1gMnSc1	Stanislav
Balík	Balík	k1gMnSc1	Balík
(	(	kIx(	(
<g/>
soudce	soudce	k1gMnSc1	soudce
zpravodaj	zpravodaj	k1gMnSc1	zpravodaj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vlasta	Vlasta	k1gFnSc1	Vlasta
<g />
.	.	kIx.	.
</s>
<s>
Formánková	Formánková	k1gFnSc1	Formánková
<g/>
,	,	kIx,	,
Ivana	Ivana	k1gFnSc1	Ivana
Janů	Janů	k1gMnSc1	Janů
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Kůrka	kůrka	k1gFnSc1	kůrka
<g/>
,	,	kIx,	,
Dagmar	Dagmar	k1gFnSc1	Dagmar
Lastovecká	Lastovecký	k2eAgFnSc1d1	Lastovecká
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Mucha	Mucha	k1gMnSc1	Mucha
<g/>
,	,	kIx,	,
Miloslav	Miloslav	k1gMnSc1	Miloslav
Výborný	Výborný	k1gMnSc1	Výborný
<g/>
,	,	kIx,	,
Michaela	Michaela	k1gFnSc1	Michaela
Židlická	Židlická	k1gFnSc1	Židlická
proti	proti	k7c3	proti
nálezu	nález	k1gInSc3	nález
byli	být	k5eAaImAgMnP	být
<g/>
:	:	kIx,	:
Pavel	Pavel	k1gMnSc1	Pavel
Rychetský	Rychetský	k1gMnSc1	Rychetský
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Nykodým	Nykodý	k2eAgFnPc3d1	Nykodý
<g/>
,	,	kIx,	,
Eliška	Eliška	k1gFnSc1	Eliška
Wagnerová	Wagnerová	k1gFnSc1	Wagnerová
<g/>
,	,	kIx,	,
Vojen	vojna	k1gFnPc2	vojna
Güttler	Güttler	k1gMnSc1	Güttler
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Duchoň	Duchoň	k1gMnSc1	Duchoň
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Musil	Musil	k1gMnSc1	Musil
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Holländer	Holländer	k1gMnSc1	Holländer
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
soud	soud	k1gInSc1	soud
vyjasnil	vyjasnit	k5eAaPmAgInS	vyjasnit
vydávání	vydávání	k1gNnSc4	vydávání
zákonů	zákon	k1gInPc2	zákon
ve	v	k7c6	v
stavu	stav	k1gInSc6	stav
legislativní	legislativní	k2eAgFnSc2d1	legislativní
nouze	nouze	k1gFnSc2	nouze
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2011	[number]	k4	2011
Ústavní	ústavní	k2eAgInSc1d1	ústavní
soud	soud	k1gInSc1	soud
ČR	ČR	kA	ČR
v	v	k7c6	v
plénu	plénum	k1gNnSc6	plénum
poměrem	poměr	k1gInSc7	poměr
9	[number]	k4	9
<g/>
:	:	kIx,	:
<g/>
6	[number]	k4	6
schválil	schválit	k5eAaPmAgInS	schválit
nález	nález	k1gInSc1	nález
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgNnSc7	jenž
uplynutím	uplynutí	k1gNnSc7	uplynutí
dne	den	k1gInSc2	den
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2011	[number]	k4	2011
zrušil	zrušit	k5eAaPmAgInS	zrušit
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
347	[number]	k4	347
<g/>
/	/	kIx~	/
<g/>
2010	[number]	k4	2010
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nP	měnit
některé	některý	k3yIgInPc1	některý
zákony	zákon	k1gInPc1	zákon
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
úspornými	úsporný	k2eAgNnPc7d1	úsporné
opatřeními	opatření	k1gNnPc7	opatření
v	v	k7c6	v
působnosti	působnost	k1gFnSc6	působnost
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
sociálních	sociální	k2eAgFnPc2d1	sociální
věcí	věc	k1gFnPc2	věc
<g/>
.	.	kIx.	.
</s>
<s>
Konstatoval	konstatovat	k5eAaBmAgMnS	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
zneužit	zneužit	k2eAgInSc1d1	zneužit
stav	stav	k1gInSc1	stav
legislativní	legislativní	k2eAgFnSc2d1	legislativní
nouze	nouze	k1gFnSc2	nouze
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
pravicová	pravicový	k2eAgFnSc1d1	pravicová
koalice	koalice	k1gFnSc1	koalice
ve	v	k7c6	v
sněmovně	sněmovna	k1gFnSc6	sněmovna
použila	použít	k5eAaPmAgFnS	použít
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
urychlila	urychlit	k5eAaPmAgFnS	urychlit
projednání	projednání	k1gNnSc3	projednání
sociálních	sociální	k2eAgInPc2d1	sociální
škrtů	škrt	k1gInPc2	škrt
ve	v	k7c6	v
sněmovně	sněmovna	k1gFnSc6	sněmovna
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jej	on	k3xPp3gInSc4	on
senát	senát	k1gInSc4	senát
mohl	moct	k5eAaImAgInS	moct
projednat	projednat	k5eAaPmF	projednat
ještě	ještě	k9	ještě
ve	v	k7c6	v
starém	starý	k2eAgNnSc6d1	staré
složení	složení	k1gNnSc6	složení
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
koalice	koalice	k1gFnSc1	koalice
měla	mít	k5eAaImAgFnS	mít
převahu	převaha	k1gFnSc4	převaha
<g/>
,	,	kIx,	,
ještě	ještě	k9	ještě
než	než	k8xS	než
se	se	k3xPyFc4	se
ujali	ujmout	k5eAaPmAgMnP	ujmout
funkcí	funkce	k1gFnSc7	funkce
nově	nově	k6eAd1	nově
zvolení	zvolený	k2eAgMnPc1d1	zvolený
senátoři	senátor	k1gMnPc1	senátor
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgMnPc7	jenž
výrazně	výrazně	k6eAd1	výrazně
převážila	převážit	k5eAaPmAgFnS	převážit
levicová	levicový	k2eAgFnSc1d1	levicová
ČSSD	ČSSD	kA	ČSSD
<g/>
.	.	kIx.	.
</s>
<s>
Zpravodajka	zpravodajka	k1gFnSc1	zpravodajka
Eliška	Eliška	k1gFnSc1	Eliška
Wagnerová	Wagnerová	k1gFnSc1	Wagnerová
do	do	k7c2	do
zdůvodnění	zdůvodnění	k1gNnSc2	zdůvodnění
napsala	napsat	k5eAaPmAgFnS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Mezi	mezi	k7c4	mezi
nejzákladnější	základní	k2eAgNnPc4d3	nejzákladnější
práva	právo	k1gNnPc4	právo
v	v	k7c6	v
parlamentní	parlamentní	k2eAgFnSc6d1	parlamentní
demokracii	demokracie	k1gFnSc6	demokracie
patří	patřit	k5eAaImIp3nS	patřit
právo	právo	k1gNnSc4	právo
opozice	opozice	k1gFnSc2	opozice
účastnit	účastnit	k5eAaImF	účastnit
se	se	k3xPyFc4	se
parlamentních	parlamentní	k2eAgFnPc2d1	parlamentní
procedur	procedura	k1gFnPc2	procedura
i	i	k8xC	i
právo	právo	k1gNnSc4	právo
blokovat	blokovat	k5eAaImF	blokovat
či	či	k8xC	či
oddalovat	oddalovat	k5eAaImF	oddalovat
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
většiny	většina	k1gFnSc2	většina
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
pro	pro	k7c4	pro
nález	nález	k1gInSc4	nález
byli	být	k5eAaImAgMnP	být
<g/>
:	:	kIx,	:
Eliška	Eliška	k1gFnSc1	Eliška
Wagnerová	Wagnerová	k1gFnSc1	Wagnerová
(	(	kIx(	(
<g/>
soudce	soudce	k1gMnSc1	soudce
zpravodaj	zpravodaj	k1gMnSc1	zpravodaj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g />
.	.	kIx.	.
</s>
<s>
Duchoň	Duchoň	k1gMnSc1	Duchoň
<g/>
,	,	kIx,	,
Vlasta	Vlasta	k1gFnSc1	Vlasta
Formánková	Formánková	k1gFnSc1	Formánková
<g/>
,	,	kIx,	,
Vojen	vojna	k1gFnPc2	vojna
Güttler	Güttler	k1gMnSc1	Güttler
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Holländer	Holländer	k1gMnSc1	Holländer
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Musil	Musil	k1gMnSc1	Musil
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Nykodým	Nykodý	k2eAgFnPc3d1	Nykodý
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Rychetský	Rychetský	k1gMnSc1	Rychetský
<g/>
,	,	kIx,	,
Miloslav	Miloslav	k1gMnSc1	Miloslav
Výborný	Výborný	k1gMnSc1	Výborný
proti	proti	k7c3	proti
nálezu	nález	k1gInSc3	nález
byli	být	k5eAaImAgMnP	být
<g/>
:	:	kIx,	:
Stanislav	Stanislav	k1gMnSc1	Stanislav
Balík	Balík	k1gMnSc1	Balík
<g/>
,	,	kIx,	,
Ivana	Ivana	k1gFnSc1	Ivana
Janů	Janů	k1gMnSc1	Janů
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Kůrka	kůrka	k1gFnSc1	kůrka
<g/>
,	,	kIx,	,
Dagmar	Dagmar	k1gFnSc1	Dagmar
Lastovecká	Lastovecký	k2eAgFnSc1d1	Lastovecká
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Mucha	Mucha	k1gMnSc1	Mucha
<g/>
,	,	kIx,	,
Michaela	Michaela	k1gFnSc1	Michaela
Židlická	Židlická	k1gFnSc1	Židlická
V	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
těchto	tento	k3xDgInPc2	tento
dvou	dva	k4xCgInPc2	dva
nálezů	nález	k1gInPc2	nález
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
spojuje	spojovat	k5eAaImIp3nS	spojovat
podobný	podobný	k2eAgInSc1d1	podobný
právní	právní	k2eAgInSc1d1	právní
problém	problém	k1gInSc1	problém
i	i	k8xC	i
podobná	podobný	k2eAgFnSc1d1	podobná
věcná	věcný	k2eAgFnSc1d1	věcná
problematika	problematika	k1gFnSc1	problematika
(	(	kIx(	(
<g/>
finance	finance	k1gFnPc1	finance
a	a	k8xC	a
sociální	sociální	k2eAgFnSc1d1	sociální
problematika	problematika	k1gFnSc1	problematika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
ústavní	ústavní	k2eAgMnPc1d1	ústavní
soudci	soudce	k1gMnPc1	soudce
dva	dva	k4xCgInPc4	dva
ucelené	ucelený	k2eAgInPc4d1	ucelený
názorové	názorový	k2eAgInPc4d1	názorový
bloky	blok	k1gInPc4	blok
a	a	k8xC	a
jazýčkem	jazýček	k1gInSc7	jazýček
na	na	k7c6	na
vahách	váha	k1gFnPc6	váha
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
Vlasta	Vlasta	k1gFnSc1	Vlasta
Formánková	Formánková	k1gFnSc1	Formánková
a	a	k8xC	a
Miloslav	Miloslav	k1gMnSc1	Miloslav
Výborný	Výborný	k1gMnSc1	Výborný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
soud	soud	k1gInSc1	soud
opět	opět	k6eAd1	opět
rešil	rešit	k5eAaPmAgInS	rešit
otázku	otázka	k1gFnSc4	otázka
vztahu	vztah	k1gInSc2	vztah
národního	národní	k2eAgNnSc2d1	národní
a	a	k8xC	a
komunitárního	komunitární	k2eAgNnSc2d1	komunitární
práva	právo	k1gNnSc2	právo
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
kauze	kauza	k1gFnSc6	kauza
slovenské	slovenský	k2eAgInPc1d1	slovenský
důchody	důchod	k1gInPc1	důchod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
rozhodoval	rozhodovat	k5eAaImAgInS	rozhodovat
Ústavní	ústavní	k2eAgInSc1d1	ústavní
soud	soud	k1gInSc1	soud
o	o	k7c6	o
žalobě	žaloba	k1gFnSc6	žaloba
z	z	k7c2	z
velezrady	velezrada	k1gFnSc2	velezrada
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
podal	podat	k5eAaPmAgInS	podat
Senát	senát	k1gInSc1	senát
Parlamentu	parlament	k1gInSc2	parlament
ČR	ČR	kA	ČR
na	na	k7c4	na
bývalého	bývalý	k2eAgMnSc4d1	bývalý
prezidenta	prezident	k1gMnSc4	prezident
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
Václava	Václav	k1gMnSc2	Václav
Klause	Klaus	k1gMnSc2	Klaus
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
žaloby	žaloba	k1gFnSc2	žaloba
bylo	být	k5eAaImAgNnS	být
nedovršení	nedovršení	k1gNnSc1	nedovršení
ratifikace	ratifikace	k1gFnSc1	ratifikace
Rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
Evropské	evropský	k2eAgFnSc2d1	Evropská
rady	rada	k1gFnSc2	rada
<g/>
,	,	kIx,	,
nečinnost	nečinnost	k1gFnSc4	nečinnost
při	při	k7c6	při
obsazování	obsazování	k1gNnSc6	obsazování
postupně	postupně	k6eAd1	postupně
uvolňovaných	uvolňovaný	k2eAgNnPc2d1	uvolňované
míst	místo	k1gNnPc2	místo
soudců	soudce	k1gMnPc2	soudce
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
<g/>
,	,	kIx,	,
vyhlášení	vyhlášení	k1gNnSc4	vyhlášení
aboliční	aboliční	k2eAgFnSc2d1	aboliční
části	část	k1gFnSc2	část
amnestie	amnestie	k1gFnSc2	amnestie
<g/>
,	,	kIx,	,
nerespektování	nerespektování	k1gNnSc1	nerespektování
soudního	soudní	k2eAgNnSc2d1	soudní
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
ve	v	k7c6	v
věci	věc	k1gFnSc6	věc
návrhu	návrh	k1gInSc2	návrh
na	na	k7c4	na
jmenování	jmenování	k1gNnSc4	jmenování
někdejšího	někdejší	k2eAgMnSc2d1	někdejší
justičního	justiční	k2eAgMnSc2d1	justiční
čekatele	čekatel	k1gMnSc2	čekatel
JUDr.	JUDr.	kA	JUDr.
Petra	Petr	k1gMnSc2	Petr
Langera	Langer	k1gMnSc2	Langer
<g/>
,	,	kIx,	,
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
,	,	kIx,	,
soudcem	soudce	k1gMnSc7	soudce
a	a	k8xC	a
nečinnost	nečinnost	k1gFnSc4	nečinnost
při	při	k7c6	při
ratifikaci	ratifikace	k1gFnSc6	ratifikace
Dodatkového	dodatkový	k2eAgInSc2d1	dodatkový
protokolu	protokol	k1gInSc2	protokol
k	k	k7c3	k
Evropské	evropský	k2eAgFnSc3d1	Evropská
sociální	sociální	k2eAgFnSc3d1	sociální
chartě	charta	k1gFnSc3	charta
<g/>
.	.	kIx.	.
</s>
<s>
Ústavní	ústavní	k2eAgInSc1d1	ústavní
soud	soud	k1gInSc1	soud
se	se	k3xPyFc4	se
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
vyjádření	vyjádření	k1gNnSc6	vyjádření
nezabýval	zabývat	k5eNaImAgMnS	zabývat
podanými	podaný	k2eAgNnPc7d1	podané
fakty	faktum	k1gNnPc7	faktum
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
podle	podle	k7c2	podle
většinového	většinový	k2eAgNnSc2d1	většinové
stanoviska	stanovisko	k1gNnSc2	stanovisko
byla	být	k5eAaImAgFnS	být
vyloučena	vyloučit	k5eAaPmNgFnS	vyloučit
možnost	možnost	k1gFnSc1	možnost
řízení	řízení	k1gNnSc2	řízení
o	o	k7c6	o
ústavní	ústavní	k2eAgFnSc6d1	ústavní
žalobě	žaloba	k1gFnSc6	žaloba
po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
prezidentského	prezidentský	k2eAgInSc2d1	prezidentský
mandátu	mandát	k1gInSc2	mandát
<g/>
.	.	kIx.	.
</s>
<s>
Soud	soud	k1gInSc1	soud
proto	proto	k8xC	proto
řízení	řízení	k1gNnSc4	řízení
zastavil	zastavit	k5eAaPmAgMnS	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
Odlišné	odlišný	k2eAgNnSc4d1	odlišné
stanovisko	stanovisko	k1gNnSc4	stanovisko
vznesla	vznést	k5eAaPmAgFnS	vznést
soudkyně	soudkyně	k1gFnSc1	soudkyně
Ivana	Ivan	k1gMnSc2	Ivan
Janů	Janů	k1gMnSc2	Janů
a	a	k8xC	a
Stanislav	Stanislav	k1gMnSc1	Stanislav
Balík	Balík	k1gMnSc1	Balík
<g/>
.	.	kIx.	.
</s>
<s>
Ústavní	ústavní	k2eAgInSc1d1	ústavní
soud	soud	k1gInSc1	soud
také	také	k9	také
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
ve	v	k7c6	v
věci	věc	k1gFnSc6	věc
církevních	církevní	k2eAgFnPc2d1	církevní
restitucí	restituce	k1gFnPc2	restituce
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
návrhu	návrh	k1gInSc2	návrh
na	na	k7c6	na
jejich	jejich	k3xOp3gNnSc6	jejich
zrušení	zrušení	k1gNnSc6	zrušení
rozsáhlým	rozsáhlý	k2eAgInSc7d1	rozsáhlý
nálezem	nález	k1gInSc7	nález
nevyhověl	vyhovět	k5eNaPmAgMnS	vyhovět
<g/>
.	.	kIx.	.
</s>
<s>
Soudcem	soudce	k1gMnSc7	soudce
zpravodajem	zpravodaj	k1gMnSc7	zpravodaj
byl	být	k5eAaImAgMnS	být
Stanislav	Stanislav	k1gMnSc1	Stanislav
Balík	Balík	k1gMnSc1	Balík
<g/>
,	,	kIx,	,
odlišná	odlišný	k2eAgNnPc4d1	odlišné
stanoviska	stanovisko	k1gNnPc4	stanovisko
zaujali	zaujmout	k5eAaPmAgMnP	zaujmout
soudci	soudce	k1gMnPc1	soudce
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Fenyk	Fenyk	k1gMnSc1	Fenyk
<g/>
,	,	kIx,	,
Vojen	vojna	k1gFnPc2	vojna
Güttler	Güttler	k1gMnSc1	Güttler
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Musil	Musil	k1gMnSc1	Musil
a	a	k8xC	a
Pavel	Pavel	k1gMnSc1	Pavel
Rychetský	Rychetský	k1gMnSc1	Rychetský
<g/>
.	.	kIx.	.
</s>
