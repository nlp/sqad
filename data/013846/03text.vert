<s>
Todi	Todi	k6eAd1
zelený	zelený	k2eAgInSc1d1
</s>
<s>
Todi	Todi	k6eAd1
zelený	zelený	k2eAgInSc1d1
Todi	Tod	k1gFnSc3
zelený	zelený	k2eAgInSc1d1
Stupeň	stupeň	k1gInSc1
ohrožení	ohrožení	k1gNnSc2
podle	podle	k7c2
IUCN	IUCN	kA
</s>
<s>
málo	málo	k6eAd1
dotčený	dotčený	k2eAgMnSc1d1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
Říše	říš	k1gFnSc2
</s>
<s>
živočichové	živočich	k1gMnPc1
(	(	kIx(
<g/>
Animalia	Animalia	k1gFnSc1
<g/>
)	)	kIx)
Kmen	kmen	k1gInSc1
</s>
<s>
strunatci	strunatec	k1gMnPc1
(	(	kIx(
<g/>
Chordata	Chordata	k1gFnSc1
<g/>
)	)	kIx)
Podkmen	podkmen	k1gInSc1
</s>
<s>
obratlovci	obratlovec	k1gMnPc1
(	(	kIx(
<g/>
Vertebrata	Vertebrat	k2eAgFnSc1d1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
ptáci	pták	k1gMnPc1
(	(	kIx(
<g/>
Aves	Aves	k1gInSc1
<g/>
)	)	kIx)
Podtřída	podtřída	k1gFnSc1
</s>
<s>
letci	letec	k1gMnSc3
(	(	kIx(
<g/>
Neognathae	Neognathae	k1gFnSc1
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
srostloprstí	srostloprstý	k2eAgMnPc1d1
(	(	kIx(
<g/>
Coraciiiformes	Coraciiiformes	k1gMnSc1
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc1
</s>
<s>
todiovití	todiovití	k1gMnPc1
(	(	kIx(
<g/>
Todidae	Todidae	k1gInSc1
<g/>
)	)	kIx)
Rod	rod	k1gInSc1
</s>
<s>
todi	todi	k1gNnSc1
(	(	kIx(
<g/>
Todus	Todus	k1gInSc1
<g/>
)	)	kIx)
Binomické	binomický	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
</s>
<s>
Todus	Todus	k1gMnSc1
todus	todus	k1gMnSc1
<g/>
(	(	kIx(
<g/>
Linné	Linné	k1gNnSc1
<g/>
,	,	kIx,
1758	#num#	k4
<g/>
)	)	kIx)
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Todi	Todi	k6eAd1
zelený	zelený	k2eAgInSc1d1
(	(	kIx(
<g/>
Todus	Todus	k1gInSc1
todus	todus	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
malý	malý	k2eAgMnSc1d1
pták	pták	k1gMnSc1
z	z	k7c2
čeledi	čeleď	k1gFnSc2
todiovitých	todiovitý	k2eAgInPc2d1
obývající	obývající	k2eAgInPc4d1
lesní	lesní	k2eAgInPc4d1
porosty	porost	k1gInPc4
Jamajky	Jamajka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Todi	Todi	k6eAd1
zelený	zelený	k2eAgMnSc1d1
dorůstá	dorůstat	k5eAaImIp3nS
11	#num#	k4
cm	cm	kA
<g/>
,	,	kIx,
na	na	k7c6
horní	horní	k2eAgFnSc6d1
straně	strana	k1gFnSc6
je	být	k5eAaImIp3nS
jasně	jasně	k6eAd1
zelený	zelený	k2eAgInSc1d1
<g/>
,	,	kIx,
spodinu	spodina	k1gFnSc4
těla	tělo	k1gNnSc2
má	mít	k5eAaImIp3nS
světlejší	světlý	k2eAgFnSc4d2
a	a	k8xC
hrdlo	hrdlo	k1gNnSc4
zářivě	zářivě	k6eAd1
červené	červená	k1gNnSc4
se	s	k7c7
světle	světle	k6eAd1
modrými	modrý	k2eAgInPc7d1
znaky	znak	k1gInPc7
po	po	k7c6
jeho	jeho	k3xOp3gFnPc6
stranách	strana	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
poměru	poměr	k1gInSc3
těla	tělo	k1gNnSc2
má	mít	k5eAaImIp3nS
velkou	velký	k2eAgFnSc4d1
hlavu	hlava	k1gFnSc4
a	a	k8xC
dlouhý	dlouhý	k2eAgInSc4d1
<g/>
,	,	kIx,
zašpičatělý	zašpičatělý	k2eAgInSc4d1
zobák	zobák	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
vhodné	vhodný	k2eAgFnSc2d1
pozorovatelny	pozorovatelna	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
bývá	bývat	k5eAaImIp3nS
nejčastěji	často	k6eAd3
větev	větev	k1gFnSc4
<g/>
,	,	kIx,
loví	lovit	k5eAaImIp3nP
hmyz	hmyz	k1gInSc4
a	a	k8xC
jeho	jeho	k3xOp3gFnPc4
larvy	larva	k1gFnPc4
<g/>
,	,	kIx,
příležitostně	příležitostně	k6eAd1
se	se	k3xPyFc4
živí	živit	k5eAaImIp3nP
i	i	k9
plody	plod	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
hnízdění	hnízdění	k1gNnPc4
si	se	k3xPyFc3
ve	v	k7c6
strmých	strmý	k2eAgInPc6d1
bahnitých	bahnitý	k2eAgInPc6d1
březích	břeh	k1gInPc6
v	v	k7c6
blízkosti	blízkost	k1gFnSc6
vod	voda	k1gFnPc2
hloubí	hloubit	k5eAaImIp3nS
dlouhou	dlouhý	k2eAgFnSc4d1
noru	nora	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Jamaican	Jamaican	k1gInSc1
Tody	Toda	k1gFnSc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
The	The	k1gFnSc1
IUCN	IUCN	kA
Red	Red	k1gFnSc1
List	list	k1gInSc1
of	of	k?
Threatened	Threatened	k1gInSc1
Species	species	k1gFnSc1
2021.1	2021.1	k4
<g/>
.	.	kIx.
25	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
KHOLOVÁ	Kholová	k1gFnSc1
<g/>
,	,	kIx,
Helena	Helena	k1gFnSc1
(	(	kIx(
<g/>
autorka	autorka	k1gFnSc1
českého	český	k2eAgInSc2d1
překladu	překlad	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ptáci	pták	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Euromedia	Euromedium	k1gNnPc1
Group	Group	k1gMnSc1
<g/>
,	,	kIx,
k.	k.	k?
s.	s.	k?
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9788024222356	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
todi	todi	k6eAd1
zelený	zelený	k2eAgMnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Taxon	taxon	k1gInSc1
Todus	Todus	k1gInSc1
todus	todus	k1gInSc1
ve	v	k7c6
Wikidruzích	Wikidruze	k1gFnPc6
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Ptáci	pták	k1gMnPc1
</s>
