<p>
<s>
Neil	Neil	k1gMnSc1	Neil
Percival	Percival	k1gMnSc1	Percival
Young	Young	k1gMnSc1	Young
(	(	kIx(	(
<g/>
*	*	kIx~	*
12	[number]	k4	12
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1945	[number]	k4	1945
Toronto	Toronto	k1gNnSc1	Toronto
<g/>
,	,	kIx,	,
Ontario	Ontario	k1gNnSc1	Ontario
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
kanadský	kanadský	k2eAgMnSc1d1	kanadský
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
písničkář	písničkář	k1gMnSc1	písničkář
<g/>
,	,	kIx,	,
hudebník	hudebník	k1gMnSc1	hudebník
a	a	k8xC	a
filmový	filmový	k2eAgMnSc1d1	filmový
režisér	režisér	k1gMnSc1	režisér
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Youngovo	Youngův	k2eAgNnSc1d1	Youngovo
dílo	dílo	k1gNnSc1	dílo
je	být	k5eAaImIp3nS	být
typické	typický	k2eAgNnSc1d1	typické
hluboce	hluboko	k6eAd1	hluboko
osobními	osobní	k2eAgInPc7d1	osobní
texty	text	k1gInPc7	text
<g/>
,	,	kIx,	,
charakteristickým	charakteristický	k2eAgInSc7d1	charakteristický
stylem	styl	k1gInSc7	styl
hry	hra	k1gFnSc2	hra
na	na	k7c4	na
kytaru	kytara	k1gFnSc4	kytara
a	a	k8xC	a
příznačným	příznačný	k2eAgInSc7d1	příznačný
nosovým	nosový	k2eAgInSc7d1	nosový
zpěvem	zpěv	k1gInSc7	zpěv
(	(	kIx(	(
<g/>
tenorem	tenor	k1gInSc7	tenor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
se	se	k3xPyFc4	se
doprovází	doprovázet	k5eAaImIp3nS	doprovázet
na	na	k7c4	na
několik	několik	k4yIc4	několik
různých	různý	k2eAgInPc2d1	různý
nástrojů	nástroj	k1gInPc2	nástroj
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
piana	piano	k1gNnSc2	piano
a	a	k8xC	a
harmoniky	harmonika	k1gFnSc2	harmonika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnPc1	jeho
styl	styl	k1gInSc4	styl
hry	hra	k1gFnSc2	hra
na	na	k7c4	na
akustickou	akustický	k2eAgFnSc4d1	akustická
kytaru	kytara	k1gFnSc4	kytara
a	a	k8xC	a
časté	častý	k2eAgNnSc1d1	časté
výstřední	výstřední	k2eAgNnSc1d1	výstřední
sólování	sólování	k1gNnSc1	sólování
na	na	k7c4	na
elektrickou	elektrický	k2eAgFnSc4d1	elektrická
kytaru	kytara	k1gFnSc4	kytara
jsou	být	k5eAaImIp3nP	být
klíčovými	klíčový	k2eAgInPc7d1	klíčový
momenty	moment	k1gInPc7	moment
někdy	někdy	k6eAd1	někdy
drsného	drsný	k2eAgInSc2d1	drsný
<g/>
,	,	kIx,	,
jindy	jindy	k6eAd1	jindy
uhlazeného	uhlazený	k2eAgInSc2d1	uhlazený
zvuku	zvuk	k1gInSc2	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Young	Young	k1gMnSc1	Young
za	za	k7c2	za
své	svůj	k3xOyFgFnSc2	svůj
hudební	hudební	k2eAgFnSc2d1	hudební
kariéry	kariéra	k1gFnSc2	kariéra
experimentoval	experimentovat	k5eAaImAgMnS	experimentovat
s	s	k7c7	s
různými	různý	k2eAgInPc7d1	různý
hudebními	hudební	k2eAgInPc7d1	hudební
styly	styl	k1gInPc7	styl
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
swingu	swing	k1gInSc2	swing
<g/>
,	,	kIx,	,
jazzu	jazz	k1gInSc2	jazz
<g/>
,	,	kIx,	,
rockabilly	rockabilla	k1gFnSc2	rockabilla
<g/>
,	,	kIx,	,
blues	blues	k1gNnSc2	blues
a	a	k8xC	a
elektronické	elektronický	k2eAgFnSc2d1	elektronická
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnPc1	jeho
nejznámější	známý	k2eAgNnPc1d3	nejznámější
díla	dílo	k1gNnPc1	dílo
však	však	k9	však
náleží	náležet	k5eAaImIp3nP	náležet
do	do	k7c2	do
dvou	dva	k4xCgInPc2	dva
stylů	styl	k1gInPc2	styl
<g/>
:	:	kIx,	:
folkově	folkově	k6eAd1	folkově
akustického	akustický	k2eAgInSc2d1	akustický
rocku	rock	k1gInSc2	rock
(	(	kIx(	(
<g/>
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
slyšet	slyšet	k5eAaImF	slyšet
v	v	k7c6	v
"	"	kIx"	"
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Heart	Heart	k1gInSc1	Heart
of	of	k?	of
Gold	Gold	k1gInSc1	Gold
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Harvest	Harvest	k1gMnSc1	Harvest
Moon	Moon	k1gMnSc1	Moon
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Old	Olda	k1gFnPc2	Olda
Man	mana	k1gFnPc2	mana
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
elektrikou	elektrika	k1gFnSc7	elektrika
nabitého	nabitý	k2eAgInSc2d1	nabitý
hard	hard	k6eAd1	hard
rocku	rock	k1gInSc3	rock
(	(	kIx(	(
<g/>
v	v	k7c6	v
písních	píseň	k1gFnPc6	píseň
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
Cinnamon	Cinnamon	k1gNnSc1	Cinnamon
Girl	girl	k1gFnSc2	girl
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Rockin	Rockin	k1gInSc1	Rockin
<g/>
'	'	kIx"	'
in	in	k?	in
the	the	k?	the
Free	Free	k1gInSc1	Free
World	World	k1gInSc1	World
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Hey	Hey	k1gMnSc1	Hey
Hey	Hey	k1gMnSc1	Hey
<g/>
,	,	kIx,	,
My	my	k3xPp1nPc1	my
My	my	k3xPp1nPc1	my
(	(	kIx(	(
<g/>
Into	Into	k6eAd1	Into
the	the	k?	the
Black	Black	k1gInSc1	Black
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdějších	pozdní	k2eAgNnPc6d2	pozdější
letech	léto	k1gNnPc6	léto
začal	začít	k5eAaPmAgInS	začít
Young	Young	k1gInSc1	Young
přebírat	přebírat	k5eAaImF	přebírat
novější	nový	k2eAgInPc1d2	novější
styly	styl	k1gInPc1	styl
hudby	hudba	k1gFnSc2	hudba
jako	jako	k8xC	jako
industriál	industriál	k1gInSc1	industriál
<g/>
,	,	kIx,	,
alternative	alternativ	k1gInSc5	alternativ
country	country	k2eAgNnSc6d1	country
a	a	k8xC	a
grunge	grunge	k1gFnPc6	grunge
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
prohloubil	prohloubit	k5eAaPmAgMnS	prohloubit
svůj	svůj	k3xOyFgInSc4	svůj
vlastní	vlastní	k2eAgInSc4d1	vlastní
styl	styl	k1gInSc4	styl
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mu	on	k3xPp3gMnSc3	on
přineslo	přinést	k5eAaPmAgNnS	přinést
titul	titul	k1gInSc4	titul
"	"	kIx"	"
<g/>
Kmotr	kmotr	k1gMnSc1	kmotr
grunge	grung	k1gFnSc2	grung
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Young	Young	k1gMnSc1	Young
režíroval	režírovat	k5eAaImAgMnS	režírovat
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
spolurežíroval	spolurežírovat	k5eAaImAgMnS	spolurežírovat
<g/>
)	)	kIx)	)
několik	několik	k4yIc1	několik
filmů	film	k1gInPc2	film
pod	pod	k7c7	pod
pseudonymem	pseudonym	k1gInSc7	pseudonym
Bernard	Bernard	k1gMnSc1	Bernard
Shakey	Shakey	k1gInPc4	Shakey
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Journey	Journea	k1gFnSc2	Journea
Through	Through	k1gInSc1	Through
the	the	k?	the
Past	past	k1gFnSc1	past
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rust	Rust	k2eAgInSc1d1	Rust
Never	Never	k1gInSc1	Never
Sleeps	Sleepsa	k1gFnPc2	Sleepsa
(	(	kIx(	(
<g/>
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Human	Human	k1gMnSc1	Human
Highway	Highwaa	k1gFnSc2	Highwaa
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
Greendale	Greendala	k1gFnSc3	Greendala
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Ačkoliv	ačkoliv	k8xS	ačkoliv
Young	Young	k1gMnSc1	Young
často	často	k6eAd1	často
zpívá	zpívat	k5eAaImIp3nS	zpívat
o	o	k7c6	o
amerických	americký	k2eAgFnPc6d1	americká
legendách	legenda	k1gFnPc6	legenda
a	a	k8xC	a
mýtech	mýtus	k1gInPc6	mýtus
(	(	kIx(	(
<g/>
Pocahontas	Pocahontas	k1gMnSc1	Pocahontas
<g/>
,	,	kIx,	,
vesmírné	vesmírný	k2eAgFnPc1d1	vesmírná
stanice	stanice	k1gFnPc1	stanice
a	a	k8xC	a
osidlování	osidlování	k1gNnSc1	osidlování
amerického	americký	k2eAgInSc2d1	americký
západu	západ	k1gInSc2	západ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
kanadským	kanadský	k2eAgMnSc7d1	kanadský
občanem	občan	k1gMnSc7	občan
a	a	k8xC	a
nikdy	nikdy	k6eAd1	nikdy
nechtěl	chtít	k5eNaImAgMnS	chtít
občanství	občanství	k1gNnSc4	občanství
měnit	měnit	k5eAaImF	měnit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biografie	biografie	k1gFnSc2	biografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Mládí	mládí	k1gNnSc1	mládí
===	===	k?	===
</s>
</p>
<p>
<s>
Neil	Neil	k1gMnSc1	Neil
Young	Young	k1gMnSc1	Young
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
Torontu	Toronto	k1gNnSc6	Toronto
(	(	kIx(	(
<g/>
Ontario	Ontario	k1gNnSc1	Ontario
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
)	)	kIx)	)
sportovnímu	sportovní	k2eAgInSc3d1	sportovní
novináři	novinář	k1gMnSc6	novinář
a	a	k8xC	a
spisovateli	spisovatel	k1gMnSc6	spisovatel
Scottovi	Scott	k1gMnSc6	Scott
Youngovi	Young	k1gMnSc6	Young
a	a	k8xC	a
Edně	Edna	k1gFnSc6	Edna
Raglandové	Raglandový	k2eAgNnSc1d1	Raglandový
(	(	kIx(	(
<g/>
známé	známý	k2eAgInPc1d1	známý
jako	jako	k8xC	jako
Rassy	Rass	k1gInPc1	Rass
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
do	do	k7c2	do
Toronta	Toronto	k1gNnSc2	Toronto
přestěhovali	přestěhovat	k5eAaPmAgMnP	přestěhovat
z	z	k7c2	z
Manitoby	Manitoba	k1gFnSc2	Manitoba
<g/>
.	.	kIx.	.
</s>
<s>
Neil	Neil	k1gMnSc1	Neil
strávil	strávit	k5eAaPmAgMnS	strávit
své	svůj	k3xOyFgNnSc4	svůj
mládí	mládí	k1gNnSc4	mládí
v	v	k7c6	v
malém	malý	k2eAgNnSc6d1	malé
městečku	městečko	k1gNnSc6	městečko
Omemee	Omeme	k1gFnSc2	Omeme
v	v	k7c6	v
Ontariu	Ontario	k1gNnSc6	Ontario
<g/>
,	,	kIx,	,
130	[number]	k4	130
km	km	kA	km
severně	severně	k6eAd1	severně
od	od	k7c2	od
Toronta	Toronto	k1gNnSc2	Toronto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Už	už	k6eAd1	už
jako	jako	k8xC	jako
dítěti	dítě	k1gNnSc6	dítě
byla	být	k5eAaImAgNnP	být
Youngovi	Youngův	k2eAgMnPc1d1	Youngův
diagnostikována	diagnostikován	k2eAgFnSc1d1	diagnostikována
cukrovka	cukrovka	k1gFnSc1	cukrovka
a	a	k8xC	a
kvůli	kvůli	k7c3	kvůli
obrně	obrna	k1gFnSc3	obrna
<g/>
,	,	kIx,	,
prodělané	prodělaný	k2eAgNnSc4d1	prodělané
v	v	k7c6	v
šesti	šest	k4xCc6	šest
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
oslabenou	oslabený	k2eAgFnSc4d1	oslabená
levou	levý	k2eAgFnSc4d1	levá
polovinu	polovina	k1gFnSc4	polovina
těla	tělo	k1gNnSc2	tělo
<g/>
;	;	kIx,	;
dodnes	dodnes	k6eAd1	dodnes
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
nohu	noha	k1gFnSc4	noha
mírně	mírně	k6eAd1	mírně
kulhá	kulhat	k5eAaImIp3nS	kulhat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
Youngovi	Youngův	k2eAgMnPc1d1	Youngův
12	[number]	k4	12
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnPc1	jeho
rodiče	rodič	k1gMnPc1	rodič
se	se	k3xPyFc4	se
rozvedli	rozvést	k5eAaPmAgMnP	rozvést
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
se	se	k3xPyFc4	se
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
matkou	matka	k1gFnSc7	matka
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
rodinného	rodinný	k2eAgInSc2d1	rodinný
domu	dům	k1gInSc2	dům
ve	v	k7c6	v
Winnipegu	Winnipeg	k1gInSc6	Winnipeg
<g/>
,	,	kIx,	,
Manitoba	Manitoba	k1gFnSc1	Manitoba
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
začala	začít	k5eAaPmAgFnS	začít
jeho	jeho	k3xOp3gFnSc1	jeho
kariéra	kariéra	k1gFnSc1	kariéra
hudebníka	hudebník	k1gMnSc2	hudebník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
Neil	Neil	k1gMnSc1	Neil
Young	Young	k1gMnSc1	Young
přišel	přijít	k5eAaPmAgMnS	přijít
Z	z	k7c2	z
Ontaria	Ontario	k1gNnSc2	Ontario
do	do	k7c2	do
Winnipegu	Winnipeg	k1gInSc2	Winnipeg
<g/>
,	,	kIx,	,
věděl	vědět	k5eAaImAgMnS	vědět
už	už	k6eAd1	už
co	co	k3yQnSc1	co
to	ten	k3xDgNnSc4	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
být	být	k5eAaImF	být
vykořeněný	vykořeněný	k2eAgMnSc1d1	vykořeněný
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jejich	jejich	k3xOp3gFnSc1	jejich
rodina	rodina	k1gFnSc1	rodina
cestovala	cestovat	k5eAaImAgFnS	cestovat
všude	všude	k6eAd1	všude
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
je	on	k3xPp3gFnPc4	on
zavedla	zavést	k5eAaPmAgFnS	zavést
kariéra	kariéra	k1gFnSc1	kariéra
jejich	jejich	k3xOp3gMnSc2	jejich
otce	otec	k1gMnSc2	otec
novináře	novinář	k1gMnSc2	novinář
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
manželství	manželství	k1gNnSc2	manželství
rodičů	rodič	k1gMnPc2	rodič
se	se	k3xPyFc4	se
Neil	Neil	k1gInSc1	Neil
usadil	usadit	k5eAaPmAgInS	usadit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
matkou	matka	k1gFnSc7	matka
v	v	k7c6	v
dělnické	dělnický	k2eAgFnSc6d1	Dělnická
čtvrti	čtvrt	k1gFnSc6	čtvrt
Fort	Fort	k?	Fort
Rouge	rouge	k1gFnSc2	rouge
v	v	k7c6	v
Manitobě	Manitoba	k1gFnSc6	Manitoba
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jako	jako	k9	jako
stydlivý	stydlivý	k2eAgMnSc1d1	stydlivý
mladík	mladík	k1gMnSc1	mladík
se	s	k7c7	s
suchým	suchý	k2eAgInSc7d1	suchý
humorem	humor	k1gInSc7	humor
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
Earl	earl	k1gMnSc1	earl
Grey	Grea	k1gFnSc2	Grea
Junior	junior	k1gMnSc1	junior
High	High	k1gMnSc1	High
School	School	k1gInSc4	School
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
také	také	k9	také
potkal	potkat	k5eAaPmAgInS	potkat
Kena	Kenus	k1gMnSc4	Kenus
Kobluna	Kobluna	k1gFnSc1	Kobluna
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yIgMnSc7	který
později	pozdě	k6eAd2	pozdě
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
The	The	k1gFnSc6	The
Squires	Squiresa	k1gFnPc2	Squiresa
a	a	k8xC	a
kde	kde	k6eAd1	kde
pak	pak	k6eAd1	pak
založil	založit	k5eAaPmAgMnS	založit
i	i	k8xC	i
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
skupinu	skupina	k1gFnSc4	skupina
Jades	Jadesa	k1gFnPc2	Jadesa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
navštěvoval	navštěvovat	k5eAaImAgInS	navštěvovat
Kelvin	kelvin	k1gInSc1	kelvin
High	High	k1gInSc1	High
School	School	k1gInSc1	School
ve	v	k7c6	v
Winnipegu	Winnipeg	k1gInSc6	Winnipeg
<g/>
,	,	kIx,	,
hrál	hrát	k5eAaImAgMnS	hrát
v	v	k7c6	v
několika	několik	k4yIc6	několik
instrumentálních	instrumentální	k2eAgFnPc6d1	instrumentální
rockových	rockový	k2eAgFnPc6d1	rocková
skupinách	skupina	k1gFnPc6	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Youngovou	Youngový	k2eAgFnSc4d1	Youngová
první	první	k4xOgFnSc7	první
stabilní	stabilní	k2eAgFnSc7d1	stabilní
skupinou	skupina	k1gFnSc7	skupina
byli	být	k5eAaImAgMnP	být
Squires	Squires	k1gMnSc1	Squires
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
měli	mít	k5eAaImAgMnP	mít
lokální	lokální	k2eAgInSc4d1	lokální
hit	hit	k1gInSc4	hit
nazvaný	nazvaný	k2eAgInSc4d1	nazvaný
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Sultan	Sultan	k1gInSc1	Sultan
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Young	Young	k1gMnSc1	Young
vypadl	vypadnout	k5eAaPmAgMnS	vypadnout
ze	z	k7c2	z
střední	střední	k2eAgFnSc2d1	střední
školy	škola	k1gFnSc2	škola
a	a	k8xC	a
hrál	hrát	k5eAaImAgMnS	hrát
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
také	také	k9	také
ve	v	k7c6	v
Fort	Fort	k?	Fort
William	William	k1gInSc1	William
v	v	k7c6	v
Ontariu	Ontario	k1gNnSc6	Ontario
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nahráli	nahrát	k5eAaBmAgMnP	nahrát
několik	několik	k4yIc4	několik
demosnímků	demosnímek	k1gInPc2	demosnímek
produkovaných	produkovaný	k2eAgInPc2d1	produkovaný
místním	místní	k2eAgFnPc3d1	místní
producentem	producent	k1gMnSc7	producent
(	(	kIx(	(
<g/>
Ray	Ray	k1gMnSc1	Ray
Dee	Dee	k1gMnSc1	Dee
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterému	který	k3yRgMnSc3	který
Young	Young	k1gInSc1	Young
říkal	říkat	k5eAaImAgInS	říkat
"	"	kIx"	"
<g/>
the	the	k?	the
original	originat	k5eAaImAgMnS	originat
Briggs	Briggs	k1gInSc4	Briggs
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
v	v	k7c4	v
Thunder	Thunder	k1gInSc4	Thunder
Bay	Bay	k1gMnSc1	Bay
<g/>
,	,	kIx,	,
Young	Young	k1gMnSc1	Young
poprvé	poprvé	k6eAd1	poprvé
potkal	potkat	k5eAaPmAgMnS	potkat
Stephena	Stephen	k2eAgMnSc4d1	Stephen
Stillse	Stills	k1gMnSc4	Stills
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Heart	Hearta	k1gFnPc2	Hearta
of	of	k?	of
Gold	Gold	k1gMnSc1	Gold
Young	Young	k1gMnSc1	Young
vypravuje	vypravovat	k5eAaImIp3nS	vypravovat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
trávil	trávit	k5eAaImAgMnS	trávit
čas	čas	k1gInSc4	čas
jako	jako	k9	jako
teenager	teenager	k1gMnSc1	teenager
ve	v	k7c4	v
Falcon	Falcon	k1gNnSc4	Falcon
Lake	Lak	k1gFnSc2	Lak
v	v	k7c6	v
Manitobě	Manitoba	k1gFnSc6	Manitoba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
donekonečna	donekonečna	k6eAd1	donekonečna
vhazoval	vhazovat	k5eAaImAgMnS	vhazovat
mince	mince	k1gFnPc4	mince
do	do	k7c2	do
jukeboxu	jukebox	k1gInSc2	jukebox
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
slyšel	slyšet	k5eAaImAgMnS	slyšet
"	"	kIx"	"
<g/>
Four	Four	k1gInSc1	Four
Strong	Strong	k1gMnSc1	Strong
Winds	Winds	k1gInSc1	Winds
od	od	k7c2	od
Iana	Ianus	k1gMnSc2	Ianus
Tysona	Tyson	k1gMnSc2	Tyson
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
co	co	k3yQnSc1	co
odešel	odejít	k5eAaPmAgMnS	odejít
od	od	k7c2	od
Squires	Squiresa	k1gFnPc2	Squiresa
<g/>
,	,	kIx,	,
pracoval	pracovat	k5eAaImAgMnS	pracovat
Neil	Neil	k1gMnSc1	Neil
ve	v	k7c6	v
folkovém	folkový	k2eAgInSc6d1	folkový
klubu	klub	k1gInSc6	klub
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
poprvé	poprvé	k6eAd1	poprvé
potkal	potkat	k5eAaPmAgInS	potkat
Joni	Jone	k1gFnSc4	Jone
Mitchellovou	Mitchellový	k2eAgFnSc4d1	Mitchellová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tam	tam	k6eAd1	tam
napsal	napsat	k5eAaPmAgMnS	napsat
některé	některý	k3yIgFnPc4	některý
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
nejranějších	raný	k2eAgFnPc2d3	nejranější
a	a	k8xC	a
nejtrvalejších	trvalý	k2eAgFnPc2d3	nejtrvalejší
folkových	folkový	k2eAgFnPc2d1	folková
písní	píseň	k1gFnPc2	píseň
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
klasická	klasický	k2eAgFnSc1d1	klasická
Sugar	Sugara	k1gFnPc2	Sugara
Mountain	Mountain	k1gInSc1	Mountain
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
zpívá	zpívat	k5eAaImIp3nS	zpívat
o	o	k7c6	o
svém	svůj	k3xOyFgNnSc6	svůj
ztraceném	ztracený	k2eAgNnSc6d1	ztracené
mládí	mládí	k1gNnSc6	mládí
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
odpověď	odpověď	k1gFnSc4	odpověď
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
napsala	napsat	k5eAaPmAgFnS	napsat
Joni	Jone	k1gFnSc4	Jone
Mitchell	Mitchella	k1gFnPc2	Mitchella
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Circle	Circle	k1gInSc1	Circle
Game	game	k1gInSc1	game
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
Young	Young	k1gMnSc1	Young
cestoval	cestovat	k5eAaImAgMnS	cestovat
po	po	k7c6	po
Kanadě	Kanada	k1gFnSc6	Kanada
jako	jako	k8xC	jako
sólový	sólový	k2eAgMnSc1d1	sólový
umělec	umělec	k1gMnSc1	umělec
a	a	k8xC	a
skládal	skládat	k5eAaImAgMnS	skládat
hudbu	hudba	k1gFnSc4	hudba
na	na	k7c4	na
objednávku	objednávka	k1gFnSc4	objednávka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
se	se	k3xPyFc4	se
připojil	připojit	k5eAaPmAgMnS	připojit
k	k	k7c3	k
frontmanovi	frontman	k1gMnSc3	frontman
Mynah	Mynah	k1gInSc4	Mynah
Birds	Birdsa	k1gFnPc2	Birdsa
<g/>
,	,	kIx,	,
Ricku	Ricek	k1gMnSc6	Ricek
Jamesovi	James	k1gMnSc6	James
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
si	se	k3xPyFc3	se
zabezpečila	zabezpečit	k5eAaPmAgFnS	zabezpečit
nahrávání	nahrávání	k1gNnSc4	nahrávání
u	u	k7c2	u
značky	značka	k1gFnSc2	značka
Motown	Motowna	k1gFnPc2	Motowna
<g/>
.	.	kIx.	.
</s>
<s>
Bohužel	bohužel	k9	bohužel
<g/>
,	,	kIx,	,
když	když	k8xS	když
natočili	natočit	k5eAaBmAgMnP	natočit
první	první	k4xOgNnSc4	první
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
James	James	k1gMnSc1	James
byl	být	k5eAaImAgMnS	být
žalářován	žalářovat	k5eAaImNgMnS	žalářovat
za	za	k7c4	za
dezerci	dezerce	k1gFnSc4	dezerce
z	z	k7c2	z
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
Mynah	Mynah	k1gInSc1	Mynah
Birds	Birdsa	k1gFnPc2	Birdsa
rozpadli	rozpadnout	k5eAaPmAgMnP	rozpadnout
<g/>
,	,	kIx,	,
Young	Young	k1gMnSc1	Young
a	a	k8xC	a
baskytarista	baskytarista	k1gMnSc1	baskytarista
Bruce	Bruce	k1gMnSc1	Bruce
Palmer	Palmer	k1gMnSc1	Palmer
přesídlili	přesídlit	k5eAaPmAgMnP	přesídlit
do	do	k7c2	do
Los	los	k1gInSc1	los
Angeles	Angeles	k1gInSc1	Angeles
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
.	.	kIx.	.
</s>
<s>
Young	Young	k1gMnSc1	Young
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
interview	interview	k1gNnSc6	interview
přiznal	přiznat	k5eAaPmAgMnS	přiznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
pobýval	pobývat	k5eAaImAgMnS	pobývat
ilegálně	ilegálně	k6eAd1	ilegálně
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dostal	dostat	k5eAaPmAgMnS	dostat
zelenou	zelený	k2eAgFnSc4d1	zelená
kartu	karta	k1gFnSc4	karta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
Neil	Neil	k1gMnSc1	Neil
Young	Young	k1gMnSc1	Young
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Everybody	Everybod	k1gInPc1	Everybod
Knows	Knowsa	k1gFnPc2	Knowsa
This	Thisa	k1gFnPc2	Thisa
Is	Is	k1gMnSc5	Is
Nowhere	Nowher	k1gMnSc5	Nowher
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
After	After	k1gMnSc1	After
the	the	k?	the
Gold	Gold	k1gMnSc1	Gold
Rush	Rush	k1gMnSc1	Rush
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Harvest	Harvest	k1gFnSc1	Harvest
(	(	kIx(	(
<g/>
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Time	Time	k1gInSc1	Time
Fades	fadesa	k1gFnPc2	fadesa
Away	Awaa	k1gFnSc2	Awaa
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
On	on	k3xPp3gInSc1	on
the	the	k?	the
Beach	Beach	k1gInSc1	Beach
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tonight	Tonight	k1gInSc1	Tonight
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
the	the	k?	the
Night	Night	k1gInSc1	Night
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zuma	Zuma	k1gFnSc1	Zuma
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
American	American	k1gInSc1	American
Stars	Stars	k1gInSc1	Stars
'	'	kIx"	'
<g/>
n	n	k0	n
Bars	Bars	k?	Bars
(	(	kIx(	(
<g/>
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Comes	Comes	k1gInSc1	Comes
a	a	k8xC	a
Time	Time	k1gInSc1	Time
(	(	kIx(	(
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Rust	Rust	k2eAgInSc1d1	Rust
Never	Never	k1gInSc1	Never
Sleeps	Sleepsa	k1gFnPc2	Sleepsa
(	(	kIx(	(
<g/>
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hawks	Hawks	k1gInSc1	Hawks
&	&	k?	&
Doves	Doves	k1gInSc1	Doves
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Re-ac-tor	Recor	k1gInSc1	Re-ac-tor
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Trans	trans	k1gInSc1	trans
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Everybody	Everyboda	k1gFnPc1	Everyboda
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Rockin	Rockina	k1gFnPc2	Rockina
<g/>
'	'	kIx"	'
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Old	Olda	k1gFnPc2	Olda
Ways	Waysa	k1gFnPc2	Waysa
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Landing	Landing	k1gInSc4	Landing
on	on	k3xPp3gMnSc1	on
Water	Water	k1gMnSc1	Water
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Life	Life	k1gFnSc1	Life
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
This	This	k1gInSc1	This
Note	Note	k1gInSc1	Note
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
for	forum	k1gNnPc2	forum
You	You	k1gFnSc1	You
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Eldorado	Eldorada	k1gFnSc5	Eldorada
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Freedom	Freedom	k1gInSc1	Freedom
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ragged	Ragged	k1gInSc1	Ragged
Glory	Glora	k1gFnSc2	Glora
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Harvest	Harvest	k1gMnSc1	Harvest
Moon	Moon	k1gMnSc1	Moon
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sleeps	Sleeps	k6eAd1	Sleeps
with	with	k1gInSc1	with
Angels	Angelsa	k1gFnPc2	Angelsa
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mirror	Mirror	k1gMnSc1	Mirror
Ball	Ball	k1gMnSc1	Ball
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Broken	Broken	k2eAgMnSc1d1	Broken
Arrow	Arrow	k1gMnSc1	Arrow
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Silver	Silver	k1gMnSc1	Silver
&	&	k?	&
Gold	Gold	k1gMnSc1	Gold
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Are	ar	k1gInSc5	ar
You	You	k1gMnSc5	You
Passionate	Passionat	k1gMnSc5	Passionat
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Greendale	Greendale	k6eAd1	Greendale
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Prairie	Prairie	k1gFnSc1	Prairie
Wind	Winda	k1gFnPc2	Winda
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Living	Living	k1gInSc1	Living
with	with	k1gMnSc1	with
War	War	k1gMnSc1	War
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Living	Living	k1gInSc1	Living
with	with	k1gMnSc1	with
War	War	k1gMnSc1	War
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
In	In	k1gFnSc1	In
the	the	k?	the
Beginning	Beginning	k1gInSc1	Beginning
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Chrome	chromat	k5eAaImIp3nS	chromat
Dreams	Dreams	k1gInSc1	Dreams
II	II	kA	II
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Fork	Fork	k1gInSc1	Fork
in	in	k?	in
the	the	k?	the
Road	Road	k1gInSc1	Road
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Le	Le	k?	Le
Noise	Noise	k1gFnSc1	Noise
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Americana	Americana	k1gFnSc1	Americana
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Psychedelic	Psychedelice	k1gFnPc2	Psychedelice
Pill	Pilla	k1gFnPc2	Pilla
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
A	a	k9	a
Letter	Letter	k1gInSc1	Letter
Home	Home	k1gFnPc2	Home
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Storytone	Storyton	k1gInSc5	Storyton
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
The	The	k?	The
Monsanto	Monsanta	k1gFnSc5	Monsanta
Years	Yearsa	k1gFnPc2	Yearsa
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Peace	Peace	k1gMnSc1	Peace
Trail	Trail	k1gMnSc1	Trail
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hitchhiker	Hitchhiker	k1gInSc1	Hitchhiker
(	(	kIx(	(
<g/>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
The	The	k?	The
Visitor	Visitor	k1gInSc1	Visitor
(	(	kIx(	(
<g/>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Neil	Neil	k1gMnSc1	Neil
Young	Young	k1gMnSc1	Young
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Skinker	Skinker	k1gInSc1	Skinker
<g/>
,	,	kIx,	,
Chris	Chris	k1gInSc1	Chris
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Neil	Neil	k1gMnSc1	Neil
Young	Young	k1gMnSc1	Young
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
The	The	k1gMnSc1	The
Encyclopedia	Encyclopedium	k1gNnSc2	Encyclopedium
of	of	k?	of
Country	country	k2eAgFnPc2d1	country
Music	Musice	k1gFnPc2	Musice
<g/>
.	.	kIx.	.
</s>
<s>
Paul	Paul	k1gMnSc1	Paul
Kingsbury	Kingsbura	k1gFnSc2	Kingsbura
<g/>
,	,	kIx,	,
Editor	editor	k1gInSc1	editor
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
Oxford	Oxford	k1gInSc1	Oxford
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
.	.	kIx.	.
p.	p.	k?	p.
607	[number]	k4	607
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Adria	Adria	k1gFnSc1	Adria
<g/>
,	,	kIx,	,
Marco	Marco	k1gNnSc1	Marco
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
My	my	k3xPp1nPc1	my
Crazy	Craza	k1gFnSc2	Craza
Old	Olda	k1gFnPc2	Olda
Uncle	Uncle	k1gInSc1	Uncle
<g/>
,	,	kIx,	,
Neil	Neil	k1gMnSc1	Neil
Young	Young	k1gMnSc1	Young
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
Music	Music	k1gMnSc1	Music
of	of	k?	of
Our	Our	k1gMnSc1	Our
Times	Times	k1gMnSc1	Times
<g/>
:	:	kIx,	:
Eight	Eight	k2eAgInSc1d1	Eight
Canadian	Canadian	k1gInSc1	Canadian
Singer-Songwriters	Singer-Songwriters	k1gInSc1	Singer-Songwriters
(	(	kIx(	(
<g/>
Toronto	Toronto	k1gNnSc1	Toronto
<g/>
:	:	kIx,	:
Lorimer	Lorimer	k1gMnSc1	Lorimer
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pp	pp	k?	pp
<g/>
.	.	kIx.	.
45	[number]	k4	45
<g/>
–	–	k?	–
<g/>
64	[number]	k4	64
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Clanging	Clanging	k1gInSc4	Clanging
New	New	k1gMnPc2	New
York	York	k1gInSc1	York
Subways	Subwaysa	k1gFnPc2	Subwaysa
<g/>
,	,	kIx,	,
Screeches	Screeches	k1gMnSc1	Screeches
Intact	Intact	k1gMnSc1	Intact
<g/>
,	,	kIx,	,
Go	Go	k1gMnSc5	Go
Miniature	Miniatur	k1gMnSc5	Miniatur
"	"	kIx"	"
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
Michael	Michael	k1gMnSc1	Michael
Brick	Brick	k1gMnSc1	Brick
<g/>
;	;	kIx,	;
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
Times	Times	k1gInSc1	Times
<g/>
,	,	kIx,	,
September	September	k1gInSc1	September
21	[number]	k4	21
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
</s>
</p>
<p>
<s>
Shakey	Shakey	k1gInPc1	Shakey
<g/>
:	:	kIx,	:
Neil	Neil	k1gMnSc1	Neil
Young	Young	k1gMnSc1	Young
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Biography	Biograph	k1gInPc7	Biograph
<g/>
,	,	kIx,	,
Jimmy	Jimmo	k1gNnPc7	Jimmo
McDonough	McDonough	k1gInSc1	McDonough
<g/>
,	,	kIx,	,
<g/>
pp	pp	k?	pp
347	[number]	k4	347
<g/>
–	–	k?	–
<g/>
360	[number]	k4	360
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Neil	Neil	k1gMnSc1	Neil
Young	Young	k1gMnSc1	Young
<g/>
,	,	kIx,	,
the	the	k?	the
Rolling	Rolling	k1gInSc1	Rolling
Stones	Stones	k1gMnSc1	Stones
Files	Files	k1gMnSc1	Files
<g/>
:	:	kIx,	:
the	the	k?	the
Ultimate	Ultimat	k1gInSc5	Ultimat
Compendium	Compendium	k1gNnSc4	Compendium
of	of	k?	of
Interviews	Interviews	k1gInSc1	Interviews
<g/>
,	,	kIx,	,
Articles	Articles	k1gInSc1	Articles
<g/>
,	,	kIx,	,
Facts	Facts	k1gInSc1	Facts
<g/>
,	,	kIx,	,
and	and	k?	and
Opinions	Opinions	k1gInSc1	Opinions
from	from	k1gMnSc1	from
the	the	k?	the
Files	Files	k1gMnSc1	Files
of	of	k?	of
Rolling	Rolling	k1gInSc1	Rolling
Stone	ston	k1gInSc5	ston
<g/>
,	,	kIx,	,
published	published	k1gInSc1	published
by	by	kYmCp3nS	by
Rolling	Rolling	k1gInSc4	Rolling
Stone	ston	k1gInSc5	ston
Press	Press	k1gInSc4	Press
in	in	k?	in
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
0-7868-8043-0	[number]	k4	0-7868-8043-0
</s>
</p>
<p>
<s>
The	The	k?	The
Faber	Faber	k1gMnSc1	Faber
Encyclopedia	Encyclopedium	k1gNnSc2	Encyclopedium
of	of	k?	of
Rock	rock	k1gInSc1	rock
<g/>
,	,	kIx,	,
Phil	Phil	k1gMnSc1	Phil
Hardy	Harda	k1gFnSc2	Harda
<g/>
,	,	kIx,	,
Dave	Dav	k1gInSc5	Dav
Laing	Laing	k1gInSc1	Laing
(	(	kIx(	(
<g/>
editors	editors	k1gInSc1	editors
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Don	Don	k1gMnSc1	Don
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Be	Be	k1gMnSc1	Be
Denied	Denied	k1gMnSc1	Denied
<g/>
:	:	kIx,	:
the	the	k?	the
Canadian	Canadian	k1gInSc1	Canadian
Years	Years	k1gInSc1	Years
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Einarson	Einarson	k1gMnSc1	Einarson
<g/>
,	,	kIx,	,
published	published	k1gMnSc1	published
by	by	kYmCp3nP	by
Quarry	Quarra	k1gFnPc1	Quarra
Press	Pressa	k1gFnPc2	Pressa
in	in	k?	in
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
1-55082-044-3	[number]	k4	1-55082-044-3
</s>
</p>
<p>
<s>
A	a	k9	a
Dreamer	Dreamer	k1gMnSc1	Dreamer
of	of	k?	of
Pictures	Pictures	k1gMnSc1	Pictures
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
Downing	Downing	k1gInSc1	Downing
<g/>
,	,	kIx,	,
published	published	k1gMnSc1	published
by	by	kYmCp3nS	by
Bloomsbury	Bloomsbur	k1gInPc7	Bloomsbur
in	in	k?	in
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
0-7475-1881-5	[number]	k4	0-7475-1881-5
</s>
</p>
<p>
<s>
Neil	Neil	k1gMnSc1	Neil
and	and	k?	and
Me	Me	k1gMnSc1	Me
<g/>
,	,	kIx,	,
Scott	Scott	k1gMnSc1	Scott
Young	Young	k1gMnSc1	Young
<g/>
,	,	kIx,	,
published	published	k1gMnSc1	published
by	by	kYmCp3nS	by
McClelland	McClellanda	k1gFnPc2	McClellanda
and	and	k?	and
Stewart	Stewart	k1gMnSc1	Stewart
in	in	k?	in
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
0-7710-9099-4	[number]	k4	0-7710-9099-4
</s>
</p>
<p>
<s>
Neil	Neil	k1gMnSc1	Neil
Young	Young	k1gMnSc1	Young
<g/>
:	:	kIx,	:
Zero	Zero	k1gMnSc1	Zero
to	ten	k3xDgNnSc4	ten
Sixty	Sixta	k1gMnPc4	Sixta
<g/>
:	:	kIx,	:
A	a	k9	a
Critical	Critical	k1gFnSc4	Critical
Biography	Biographa	k1gFnSc2	Biographa
<g/>
,	,	kIx,	,
Johnny	Johnna	k1gFnPc1	Johnna
Rogan	Rogan	k1gMnSc1	Rogan
<g/>
,	,	kIx,	,
published	published	k1gMnSc1	published
by	by	kYmCp3nS	by
Omnibus	omnibus	k1gInSc1	omnibus
Press	Press	k1gInSc4	Press
in	in	k?	in
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
0-9529540-4-4	[number]	k4	0-9529540-4-4
</s>
</p>
<p>
<s>
Neil	Neil	k1gMnSc1	Neil
Young	Young	k1gMnSc1	Young
:	:	kIx,	:
reflections	reflections	k1gInSc1	reflections
in	in	k?	in
broken	broken	k2eAgInSc1d1	broken
glass	glass	k1gInSc1	glass
<g/>
,	,	kIx,	,
Sylvie	Sylvie	k1gFnSc1	Sylvie
Simmons	Simmons	k1gInSc1	Simmons
<g/>
,	,	kIx,	,
published	published	k1gMnSc1	published
by	by	kYmCp3nS	by
MOJO	MOJO	kA	MOJO
Books	Books	k1gInSc1	Books
in	in	k?	in
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
1-84195-084-X	[number]	k4	1-84195-084-X
</s>
</p>
<p>
<s>
Neil	Neil	k1gMnSc1	Neil
Young	Young	k1gMnSc1	Young
Nation	Nation	k1gInSc4	Nation
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
Kevin	Kevin	k1gMnSc1	Kevin
Chong	Chong	k1gMnSc1	Chong
<g/>
;	;	kIx,	;
published	published	k1gMnSc1	published
by	by	kYmCp3nS	by
Greystone	Greyston	k1gInSc5	Greyston
Books	Booksa	k1gFnPc2	Booksa
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
1-55365-116-2	[number]	k4	1-55365-116-2
</s>
</p>
<p>
<s>
Neil	Neinout	k5eAaPmAgMnS	Neinout
on	on	k3xPp3gMnSc1	on
himself	himself	k1gMnSc1	himself
<g/>
:	:	kIx,	:
Neil	Neil	k1gMnSc1	Neil
Young	Young	k1gMnSc1	Young
<g/>
:	:	kIx,	:
In	In	k1gMnSc1	In
His	his	k1gNnSc4	his
Own	Own	k1gMnSc2	Own
Words	Words	k1gInSc4	Words
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
Michael	Michael	k1gMnSc1	Michael
Heatley	Heatlea	k1gFnSc2	Heatlea
<g/>
;	;	kIx,	;
published	published	k1gInSc1	published
by	by	kYmCp3nS	by
Omnibus	omnibus	k1gInSc4	omnibus
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
0-7119-6161-1	[number]	k4	0-7119-6161-1
</s>
</p>
<p>
<s>
Neil	Neinout	k5eAaPmAgMnS	Neinout
on	on	k3xPp3gMnSc1	on
himself	himself	k1gMnSc1	himself
<g/>
:	:	kIx,	:
Greendale	Greendala	k1gFnSc6	Greendala
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Book	Book	k1gMnSc1	Book
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
Neil	Neil	k1gMnSc1	Neil
Young	Young	k1gMnSc1	Young
<g/>
,	,	kIx,	,
James	James	k1gMnSc1	James
Mazzeo	Mazzeo	k1gMnSc1	Mazzeo
<g/>
;	;	kIx,	;
published	published	k1gMnSc1	published
by	by	kYmCp3nS	by
Sanctuary	Sanctuara	k1gFnPc1	Sanctuara
Publishing	Publishing	k1gInSc1	Publishing
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
1-86074-622-5	[number]	k4	1-86074-622-5
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Neil	Neila	k1gFnPc2	Neila
Young	Younga	k1gFnPc2	Younga
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Neil	Neil	k1gMnSc1	Neil
Young	Young	k1gMnSc1	Young
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Neil	Neil	k1gMnSc1	Neil
Young	Young	k1gMnSc1	Young
official	official	k1gMnSc1	official
website	websit	k1gInSc5	websit
</s>
</p>
<p>
<s>
Crosby	Crosba	k1gFnPc1	Crosba
<g/>
,	,	kIx,	,
Stills	Stills	k1gInSc1	Stills
<g/>
,	,	kIx,	,
Nash	Nash	k1gMnSc1	Nash
and	and	k?	and
Young	Young	k1gMnSc1	Young
Official	Official	k1gMnSc1	Official
site	sitat	k5eAaPmIp3nS	sitat
</s>
</p>
<p>
<s>
Sugar	Sugar	k1gMnSc1	Sugar
Mountain	Mountain	k1gMnSc1	Mountain
-	-	kIx~	-
Neil	Neil	k1gMnSc1	Neil
Young	Young	k1gMnSc1	Young
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
concert	concert	k1gInSc1	concert
performance	performance	k1gFnSc2	performance
set	sto	k4xCgNnPc2	sto
lists	listsa	k1gFnPc2	listsa
</s>
</p>
