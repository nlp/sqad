<p>
<s>
Rovnodennost	rovnodennost	k1gFnSc1	rovnodennost
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
aequinoctium	aequinoctium	k1gNnSc4	aequinoctium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
astronomii	astronomie	k1gFnSc6	astronomie
okamžik	okamžik	k1gInSc4	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
Slunce	slunce	k1gNnSc1	slunce
v	v	k7c6	v
rovině	rovina	k1gFnSc6	rovina
zemského	zemský	k2eAgInSc2d1	zemský
rovníku	rovník	k1gInSc2	rovník
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
jeho	jeho	k3xOp3gInPc1	jeho
paprsky	paprsek	k1gInPc1	paprsek
dopadají	dopadat	k5eAaImIp3nP	dopadat
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
kolmo	kolmo	k6eAd1	kolmo
k	k	k7c3	k
její	její	k3xOp3gFnSc3	její
ose	osa	k1gFnSc3	osa
<g/>
.	.	kIx.	.
</s>
<s>
Rovnodennost	rovnodennost	k1gFnSc1	rovnodennost
nastává	nastávat	k5eAaImIp3nS	nastávat
dvakrát	dvakrát	k6eAd1	dvakrát
do	do	k7c2	do
roka	rok	k1gInSc2	rok
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
20	[number]	k4	20
<g/>
.	.	kIx.	.
až	až	k9	až
21	[number]	k4	21
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
střed	střed	k1gInSc1	střed
obrazu	obraz	k1gInSc2	obraz
Slunce	slunce	k1gNnSc2	slunce
na	na	k7c6	na
nebeské	nebeský	k2eAgFnSc6d1	nebeská
sféře	sféra	k1gFnSc6	sféra
prochází	procházet	k5eAaImIp3nS	procházet
jarním	jarní	k2eAgInSc7d1	jarní
bodem	bod	k1gInSc7	bod
<g/>
,	,	kIx,	,
a	a	k8xC	a
</s>
</p>
<p>
<s>
20	[number]	k4	20
<g/>
.	.	kIx.	.
až	až	k9	až
23	[number]	k4	23
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
prochází	procházet	k5eAaImIp3nS	procházet
podzimním	podzimní	k2eAgInSc7d1	podzimní
bodem	bod	k1gInSc7	bod
<g/>
.	.	kIx.	.
</s>
<s>
Březnová	březnový	k2eAgFnSc1d1	březnová
rovnodennost	rovnodennost	k1gFnSc1	rovnodennost
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
přechod	přechod	k1gInSc4	přechod
z	z	k7c2	z
astronomické	astronomický	k2eAgFnSc2d1	astronomická
zimy	zima	k1gFnSc2	zima
do	do	k7c2	do
jara	jaro	k1gNnSc2	jaro
a	a	k8xC	a
mluvíme	mluvit	k5eAaImIp1nP	mluvit
tedy	tedy	k9	tedy
o	o	k7c6	o
jarní	jarní	k2eAgFnSc6d1	jarní
rovnodennosti	rovnodennost	k1gFnSc6	rovnodennost
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
v	v	k7c6	v
září	září	k1gNnSc6	září
přechází	přecházet	k5eAaImIp3nS	přecházet
léto	léto	k1gNnSc1	léto
v	v	k7c4	v
podzim	podzim	k1gInSc4	podzim
a	a	k8xC	a
mluvme	mluvit	k5eAaImRp1nP	mluvit
o	o	k7c4	o
rovnodennosti	rovnodennost	k1gFnPc4	rovnodennost
podzimní	podzimní	k2eAgFnPc4d1	podzimní
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
začíná	začínat	k5eAaImIp3nS	začínat
jaro	jaro	k1gNnSc4	jaro
v	v	k7c4	v
září	září	k1gNnSc4	září
a	a	k8xC	a
podzim	podzim	k1gInSc4	podzim
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc1	popis
rovnodenností	rovnodennost	k1gFnPc2	rovnodennost
==	==	k?	==
</s>
</p>
<p>
<s>
Rovnodennosti	rovnodennost	k1gFnPc1	rovnodennost
nemají	mít	k5eNaImIp3nP	mít
vztah	vztah	k1gInSc4	vztah
se	s	k7c7	s
vzdáleností	vzdálenost	k1gFnSc7	vzdálenost
Země	zem	k1gFnSc2	zem
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
či	či	k8xC	či
polohou	poloha	k1gFnSc7	poloha
vůči	vůči	k7c3	vůči
apsidám	apsida	k1gFnPc3	apsida
dráhy	dráha	k1gFnSc2	dráha
Země	zem	k1gFnSc2	zem
</s>
</p>
<p>
<s>
Jako	jako	k9	jako
astronomické	astronomický	k2eAgInPc1d1	astronomický
jevy	jev	k1gInPc1	jev
neurčují	určovat	k5eNaImIp3nP	určovat
kalendářové	kalendářový	k2eAgInPc1d1	kalendářový
přechody	přechod	k1gInPc1	přechod
na	na	k7c4	na
letní	letní	k2eAgInSc4d1	letní
čas	čas	k1gInSc4	čas
a	a	k8xC	a
zpět	zpět	k6eAd1	zpět
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
stanovují	stanovovat	k5eAaImIp3nP	stanovovat
administrativně	administrativně	k6eAd1	administrativně
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
čas	čas	k1gInSc1	čas
východu	východ	k1gInSc2	východ
a	a	k8xC	a
západu	západ	k1gInSc2	západ
Slunce	slunce	k1gNnSc2	slunce
se	se	k3xPyFc4	se
nemění	měnit	k5eNaImIp3nS	měnit
symetricky	symetricky	k6eAd1	symetricky
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
to	ten	k3xDgNnSc1	ten
poslední	poslední	k2eAgInSc1d1	poslední
neděli	neděle	k1gFnSc4	neděle
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
a	a	k8xC	a
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
</s>
</p>
<p>
<s>
Slunce	slunce	k1gNnSc1	slunce
o	o	k7c6	o
rovnodennosti	rovnodennost	k1gFnSc6	rovnodennost
vychází	vycházet	k5eAaImIp3nS	vycházet
přesně	přesně	k6eAd1	přesně
na	na	k7c6	na
východě	východ	k1gInSc6	východ
a	a	k8xC	a
zapadá	zapadat	k5eAaImIp3nS	zapadat
na	na	k7c6	na
západě	západ	k1gInSc6	západ
(	(	kIx(	(
<g/>
ovšem	ovšem	k9	ovšem
horizont	horizont	k1gInSc1	horizont
pohledu	pohled	k1gInSc2	pohled
by	by	kYmCp3nS	by
musel	muset	k5eAaImAgInS	muset
být	být	k5eAaImF	být
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
0	[number]	k4	0
m	m	kA	m
nad	nad	k7c7	nad
Středozemním	středozemní	k2eAgNnSc7d1	středozemní
mořem	moře	k1gNnSc7	moře
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
rovníku	rovník	k1gInSc6	rovník
přechází	přecházet	k5eAaImIp3nS	přecházet
v	v	k7c4	v
poledne	poledne	k1gNnSc4	poledne
přes	přes	k7c4	přes
nadhlavník	nadhlavník	k1gInSc4	nadhlavník
</s>
</p>
<p>
<s>
V	v	k7c4	v
den	den	k1gInSc4	den
rovnodennosti	rovnodennost	k1gFnSc2	rovnodennost
Slunce	slunce	k1gNnSc2	slunce
osvěcuje	osvěcovat	k5eAaImIp3nS	osvěcovat
oba	dva	k4xCgInPc4	dva
zemské	zemský	k2eAgInPc4d1	zemský
póly	pól	k1gInPc4	pól
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
střídá	střídat	k5eAaImIp3nS	střídat
polární	polární	k2eAgInSc4d1	polární
den	den	k1gInSc4	den
a	a	k8xC	a
noc	noc	k1gFnSc4	noc
</s>
</p>
<p>
<s>
Slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
rovnodennost	rovnodennost	k1gFnSc1	rovnodennost
<g/>
"	"	kIx"	"
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
den	den	k1gInSc1	den
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
stejně	stejně	k6eAd1	stejně
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
jako	jako	k8xC	jako
noc	noc	k1gFnSc4	noc
<g/>
.	.	kIx.	.
</s>
<s>
Jenže	jenže	k8xC	jenže
sluneční	sluneční	k2eAgInSc1d1	sluneční
kotouč	kotouč	k1gInSc1	kotouč
není	být	k5eNaImIp3nS	být
bod	bod	k1gInSc1	bod
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc1	jeho
paprsky	paprsek	k1gInPc1	paprsek
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
ohýbají	ohýbat	k5eAaImIp3nP	ohýbat
(	(	kIx(	(
<g/>
refrakce	refrakce	k1gFnSc1	refrakce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
v	v	k7c4	v
den	den	k1gInSc4	den
rovnodennosti	rovnodennost	k1gFnSc2	rovnodennost
je	být	k5eAaImIp3nS	být
Slunce	slunce	k1gNnSc1	slunce
viditelné	viditelný	k2eAgNnSc1d1	viditelné
nad	nad	k7c7	nad
obzorem	obzor	k1gInSc7	obzor
přibližně	přibližně	k6eAd1	přibližně
12	[number]	k4	12
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
10	[number]	k4	10
minut	minuta	k1gFnPc2	minuta
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
obzorem	obzor	k1gInSc7	obzor
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
11	[number]	k4	11
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
50	[number]	k4	50
minut	minuta	k1gFnPc2	minuta
</s>
</p>
<p>
<s>
===	===	k?	===
Březnová	březnový	k2eAgFnSc1d1	březnová
rovnodennost	rovnodennost	k1gFnSc1	rovnodennost
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
a	a	k8xC	a
na	na	k7c6	na
celé	celý	k2eAgFnSc6d1	celá
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
jarní	jarní	k2eAgFnSc4d1	jarní
rovnodennost	rovnodennost	k1gFnSc4	rovnodennost
(	(	kIx(	(
<g/>
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
podzimní	podzimní	k2eAgFnSc1d1	podzimní
rovnodennost	rovnodennost	k1gFnSc1	rovnodennost
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nastává	nastávat	k5eAaImIp3nS	nastávat
nejčastěji	často	k6eAd3	často
20	[number]	k4	20
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
</s>
</p>
<p>
<s>
Dříve	dříve	k6eAd2	dříve
nastávala	nastávat	k5eAaImAgFnS	nastávat
často	často	k6eAd1	často
i	i	k8xC	i
21	[number]	k4	21
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
to	ten	k3xDgNnSc1	ten
nastalo	nastat	k5eAaPmAgNnS	nastat
naposledy	naposledy	k6eAd1	naposledy
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
výskyt	výskyt	k1gInSc1	výskyt
bude	být	k5eAaImBp3nS	být
až	až	k6eAd1	až
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2102	[number]	k4	2102
</s>
</p>
<p>
<s>
Vzácně	vzácně	k6eAd1	vzácně
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
i	i	k9	i
19	[number]	k4	19
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
(	(	kIx(	(
<g/>
poslední	poslední	k2eAgInSc1d1	poslední
výskyt	výskyt	k1gInSc1	výskyt
roku	rok	k1gInSc2	rok
1796	[number]	k4	1796
<g/>
,	,	kIx,	,
příští	příští	k2eAgInSc1d1	příští
výskyt	výskyt	k1gInSc1	výskyt
2048	[number]	k4	2048
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Na	na	k7c6	na
severním	severní	k2eAgNnSc6d1	severní
pólu	pólo	k1gNnSc6	pólo
Slunce	slunce	k1gNnSc2	slunce
vyjde	vyjít	k5eAaPmIp3nS	vyjít
–	–	k?	–
začíná	začínat	k5eAaImIp3nS	začínat
polární	polární	k2eAgInSc4d1	polární
den	den	k1gInSc4	den
</s>
</p>
<p>
<s>
Na	na	k7c6	na
jižním	jižní	k2eAgNnSc6d1	jižní
pólu	pólo	k1gNnSc6	pólo
Slunce	slunce	k1gNnSc2	slunce
zapadne	zapadnout	k5eAaPmIp3nS	zapadnout
–	–	k?	–
začíná	začínat	k5eAaImIp3nS	začínat
polární	polární	k2eAgFnSc4d1	polární
noc	noc	k1gFnSc4	noc
</s>
</p>
<p>
<s>
===	===	k?	===
Zářijová	zářijový	k2eAgFnSc1d1	zářijová
rovnodennost	rovnodennost	k1gFnSc1	rovnodennost
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
a	a	k8xC	a
na	na	k7c6	na
celé	celý	k2eAgFnSc6d1	celá
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
podzimní	podzimní	k2eAgFnSc4d1	podzimní
rovnodennost	rovnodennost	k1gFnSc4	rovnodennost
(	(	kIx(	(
<g/>
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jarní	jarní	k2eAgFnSc1d1	jarní
rovnodennost	rovnodennost	k1gFnSc1	rovnodennost
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nastává	nastávat	k5eAaImIp3nS	nastávat
obvykle	obvykle	k6eAd1	obvykle
22	[number]	k4	22
<g/>
.	.	kIx.	.
či	či	k8xC	či
23	[number]	k4	23
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
</s>
</p>
<p>
<s>
Vzácně	vzácně	k6eAd1	vzácně
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
i	i	k9	i
21	[number]	k4	21
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
(	(	kIx(	(
<g/>
poslední	poslední	k2eAgInSc1d1	poslední
výskyt	výskyt	k1gInSc1	výskyt
roku	rok	k1gInSc2	rok
643	[number]	k4	643
<g/>
,	,	kIx,	,
příští	příští	k2eAgInSc1d1	příští
výskyt	výskyt	k1gInSc1	výskyt
2092	[number]	k4	2092
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
24	[number]	k4	24
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
(	(	kIx(	(
<g/>
poslední	poslední	k2eAgInSc1d1	poslední
výskyt	výskyt	k1gInSc1	výskyt
roku	rok	k1gInSc2	rok
1931	[number]	k4	1931
<g/>
,	,	kIx,	,
příští	příští	k2eAgInSc1d1	příští
výskyt	výskyt	k1gInSc1	výskyt
2303	[number]	k4	2303
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Na	na	k7c6	na
severním	severní	k2eAgNnSc6d1	severní
pólu	pólo	k1gNnSc6	pólo
Slunce	slunce	k1gNnSc2	slunce
zapadne	zapadnout	k5eAaPmIp3nS	zapadnout
–	–	k?	–
začíná	začínat	k5eAaImIp3nS	začínat
polární	polární	k2eAgFnSc4d1	polární
noc	noc	k1gFnSc4	noc
</s>
</p>
<p>
<s>
Na	na	k7c6	na
jižním	jižní	k2eAgNnSc6d1	jižní
pólu	pólo	k1gNnSc6	pólo
Slunce	slunce	k1gNnSc2	slunce
vyjde	vyjít	k5eAaPmIp3nS	vyjít
–	–	k?	–
začíná	začínat	k5eAaImIp3nS	začínat
polární	polární	k2eAgInSc4d1	polární
den	den	k1gInSc4	den
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
a	a	k8xC	a
význam	význam	k1gInSc1	význam
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
slunovratu	slunovrat	k1gInSc2	slunovrat
se	se	k3xPyFc4	se
délka	délka	k1gFnSc1	délka
dne	den	k1gInSc2	den
mění	měnit	k5eAaImIp3nS	měnit
velmi	velmi	k6eAd1	velmi
pomalu	pomalu	k6eAd1	pomalu
<g/>
,	,	kIx,	,
a	a	k8xC	a
určit	určit	k5eAaPmF	určit
den	den	k1gInSc4	den
slunovratu	slunovrat	k1gInSc2	slunovrat
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
obtížné	obtížný	k2eAgNnSc1d1	obtížné
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
rovnodennost	rovnodennost	k1gFnSc1	rovnodennost
lze	lze	k6eAd1	lze
s	s	k7c7	s
přesností	přesnost	k1gFnSc7	přesnost
jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
určit	určit	k5eAaPmF	určit
daleko	daleko	k6eAd1	daleko
jednoduššími	jednoduchý	k2eAgInPc7d2	jednodušší
prostředky	prostředek	k1gInPc7	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
sluneční	sluneční	k2eAgInPc1d1	sluneční
paprsky	paprsek	k1gInPc1	paprsek
dopadají	dopadat	k5eAaImIp3nP	dopadat
za	za	k7c4	za
rovnodennosti	rovnodennost	k1gFnPc4	rovnodennost
na	na	k7c4	na
zemský	zemský	k2eAgInSc4d1	zemský
povrch	povrch	k1gInSc4	povrch
kolmo	kolmo	k6eAd1	kolmo
k	k	k7c3	k
zemské	zemský	k2eAgFnSc3d1	zemská
ose	osa	k1gFnSc3	osa
<g/>
,	,	kIx,	,
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
se	se	k3xPyFc4	se
stín	stín	k1gInSc1	stín
libovolného	libovolný	k2eAgInSc2d1	libovolný
pevného	pevný	k2eAgInSc2d1	pevný
bodu	bod	k1gInSc2	bod
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
vrcholu	vrchol	k1gInSc6	vrchol
jakékoli	jakýkoli	k3yIgFnSc2	jakýkoli
tyče	tyč	k1gFnSc2	tyč
<g/>
)	)	kIx)	)
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
dne	den	k1gInSc2	den
po	po	k7c6	po
přímce	přímka	k1gFnSc6	přímka
(	(	kIx(	(
<g/>
přesněji	přesně	k6eAd2	přesně
řečeno	říct	k5eAaPmNgNnS	říct
po	po	k7c6	po
rovnoběžce	rovnoběžka	k1gFnSc6	rovnoběžka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stačí	stačit	k5eAaBmIp3nS	stačit
tedy	tedy	k9	tedy
zaznamenat	zaznamenat	k5eAaPmF	zaznamenat
tři	tři	k4xCgFnPc4	tři
polohy	poloha	k1gFnPc4	poloha
stínu	stín	k1gInSc2	stín
téhož	týž	k3xTgInSc2	týž
bodu	bod	k1gInSc2	bod
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
a	a	k8xC	a
pokud	pokud	k8xS	pokud
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
přímce	přímka	k1gFnSc6	přímka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
právě	právě	k9	právě
rovnodennost	rovnodennost	k1gFnSc1	rovnodennost
<g/>
.	.	kIx.	.
<g/>
Je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejstarší	starý	k2eAgFnPc1d3	nejstarší
kultury	kultura	k1gFnPc1	kultura
zakládaly	zakládat	k5eAaImAgFnP	zakládat
svůj	svůj	k3xOyFgInSc4	svůj
kalendář	kalendář	k1gInSc4	kalendář
právě	právě	k9	právě
na	na	k7c6	na
rovnodennosti	rovnodennost	k1gFnSc6	rovnodennost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
kulturách	kultura	k1gFnPc6	kultura
určovala	určovat	k5eAaImAgFnS	určovat
počátek	počátek	k1gInSc4	počátek
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
například	například	k6eAd1	například
ve	v	k7c6	v
Starém	starý	k2eAgInSc6d1	starý
zákoně	zákon	k1gInSc6	zákon
(	(	kIx(	(
<g/>
Ex	ex	k6eAd1	ex
12	[number]	k4	12
<g/>
,	,	kIx,	,
2	[number]	k4	2
(	(	kIx(	(
<g/>
Kral	Kral	k1gMnSc1	Kral
<g/>
,	,	kIx,	,
ČEP	Čep	k1gMnSc1	Čep
<g/>
))	))	k?	))
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
ovšem	ovšem	k9	ovšem
zachoval	zachovat	k5eAaPmAgInS	zachovat
i	i	k9	i
jinou	jiný	k2eAgFnSc4d1	jiná
tradici	tradice	k1gFnSc4	tradice
podzimního	podzimní	k2eAgInSc2d1	podzimní
počátku	počátek	k1gInSc2	počátek
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jarní	jarní	k2eAgFnSc6d1	jarní
rovnodennosti	rovnodennost	k1gFnSc6	rovnodennost
dodnes	dodnes	k6eAd1	dodnes
závisí	záviset	k5eAaImIp3nS	záviset
i	i	k9	i
proměnné	proměnný	k2eAgNnSc1d1	proměnné
datum	datum	k1gNnSc1	datum
židovských	židovský	k2eAgFnPc2d1	židovská
i	i	k8xC	i
křesťanských	křesťanský	k2eAgFnPc2d1	křesťanská
Velikonoc	Velikonoce	k1gFnPc2	Velikonoce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Ekliptika	ekliptika	k1gFnSc1	ekliptika
</s>
</p>
<p>
<s>
Kalendář	kalendář	k1gInSc1	kalendář
</s>
</p>
<p>
<s>
Slunovrat	slunovrat	k1gInSc1	slunovrat
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Ottův	Ottův	k2eAgInSc1d1	Ottův
slovník	slovník	k1gInSc1	slovník
naučný	naučný	k2eAgInSc1d1	naučný
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
Aequinoctium	Aequinoctium	k1gNnSc1	Aequinoctium
<g/>
.	.	kIx.	.
</s>
<s>
Sv.	sv.	kA	sv.
1	[number]	k4	1
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
271	[number]	k4	271
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
rovnodennost	rovnodennost	k1gFnSc1	rovnodennost
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
rovnodennost	rovnodennost	k1gFnSc1	rovnodennost
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Aequinoctium	Aequinoctium	k1gNnSc4	Aequinoctium
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Začátky	začátek	k1gInPc1	začátek
ročních	roční	k2eAgFnPc2d1	roční
dob	doba	k1gFnPc2	doba
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1000	[number]	k4	1000
př.n.l.	př.n.l.	k?	př.n.l.
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2999	[number]	k4	2999
n.	n.	k?	n.
<g/>
l.	l.	k?	l.
</s>
</p>
