<p>
<s>
Litr	litr	k1gInSc1	litr
je	být	k5eAaImIp3nS	být
metrická	metrický	k2eAgFnSc1d1	metrická
jednotka	jednotka	k1gFnSc1	jednotka
objemu	objem	k1gInSc2	objem
odpovídající	odpovídající	k2eAgInSc1d1	odpovídající
krychlovému	krychlový	k2eAgInSc3d1	krychlový
decimetru	decimetr	k1gInSc3	decimetr
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
jednotkou	jednotka	k1gFnSc7	jednotka
SI	si	k1gNnSc2	si
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
akceptováno	akceptován	k2eAgNnSc1d1	akceptováno
její	její	k3xOp3gNnSc4	její
používání	používání	k1gNnSc4	používání
společně	společně	k6eAd1	společně
s	s	k7c7	s
SI	si	k1gNnSc7	si
jednotkami	jednotka	k1gFnPc7	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Značí	značit	k5eAaImIp3nS	značit
se	se	k3xPyFc4	se
malým	malý	k2eAgNnSc7d1	malé
písmenem	písmeno	k1gNnSc7	písmeno
l	l	kA	l
<g/>
.	.	kIx.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
možnosti	možnost	k1gFnSc3	možnost
záměny	záměna	k1gFnSc2	záměna
s	s	k7c7	s
číslicí	číslice	k1gFnSc7	číslice
1	[number]	k4	1
a	a	k8xC	a
písmenem	písmeno	k1gNnSc7	písmeno
i	i	k9	i
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
smí	smět	k5eAaImIp3nS	smět
značit	značit	k5eAaImF	značit
i	i	k8xC	i
velkým	velký	k2eAgNnSc7d1	velké
písmenem	písmeno	k1gNnSc7	písmeno
L	L	kA	L
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
velká	velký	k2eAgNnPc1d1	velké
písmena	písmeno	k1gNnPc1	písmeno
jsou	být	k5eAaImIp3nP	být
jinak	jinak	k6eAd1	jinak
rezervována	rezervovat	k5eAaBmNgFnS	rezervovat
pro	pro	k7c4	pro
jednotky	jednotka	k1gFnPc4	jednotka
odvozené	odvozený	k2eAgFnPc4d1	odvozená
ze	z	k7c2	z
jmen	jméno	k1gNnPc2	jméno
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
též	též	k9	též
(	(	kIx(	(
<g/>
třebaže	třebaže	k8xS	třebaže
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
SI	si	k1gNnSc7	si
<g/>
)	)	kIx)	)
používá	používat	k5eAaImIp3nS	používat
malé	malý	k2eAgNnSc1d1	malé
psací	psací	k2eAgNnSc1d1	psací
písmeno	písmeno	k1gNnSc1	písmeno
l	l	kA	l
(	(	kIx(	(
<g/>
U	u	k7c2	u
<g/>
+	+	kIx~	+
<g/>
2113	[number]	k4	2113
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Definice	definice	k1gFnSc2	definice
==	==	k?	==
</s>
</p>
<p>
<s>
Litr	litr	k1gInSc1	litr
je	být	k5eAaImIp3nS	být
roven	roven	k2eAgInSc1d1	roven
</s>
</p>
<p>
<s>
1	[number]	k4	1
dm3	dm3	k4	dm3
</s>
</p>
<p>
<s>
0,001	[number]	k4	0,001
m3	m3	k4	m3
</s>
</p>
<p>
<s>
1000	[number]	k4	1000
cm3	cm3	k4	cm3
</s>
</p>
<p>
<s>
objemu	objem	k1gInSc3	objem
krychle	krychle	k1gFnSc1	krychle
o	o	k7c6	o
hraně	hrana	k1gFnSc6	hrana
1	[number]	k4	1
dm	dm	kA	dm
(	(	kIx(	(
<g/>
=	=	kIx~	=
<g/>
10	[number]	k4	10
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
Pro	pro	k7c4	pro
porovnání	porovnání	k1gNnSc4	porovnání
objemů	objem	k1gInPc2	objem
viz	vidět	k5eAaImRp2nS	vidět
1	[number]	k4	1
E-3	E-3	k1gFnPc7	E-3
m3	m3	k4	m3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odvozené	odvozený	k2eAgFnPc4d1	odvozená
jednotky	jednotka	k1gFnPc4	jednotka
objemu	objem	k1gInSc2	objem
==	==	k?	==
</s>
</p>
<p>
<s>
Litr	litr	k1gInSc1	litr
se	se	k3xPyFc4	se
pomocí	pomocí	k7c2	pomocí
předpon	předpona	k1gFnPc2	předpona
soustavy	soustava	k1gFnSc2	soustava
SI	se	k3xPyFc3	se
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
do	do	k7c2	do
dalších	další	k2eAgFnPc2d1	další
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Mililitr	mililitr	k1gInSc1	mililitr
<g/>
,	,	kIx,	,
značený	značený	k2eAgInSc1d1	značený
ml	ml	kA	ml
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
metrická	metrický	k2eAgFnSc1d1	metrická
jednotka	jednotka	k1gFnSc1	jednotka
objemu	objem	k1gInSc2	objem
<g/>
,	,	kIx,	,
rovná	rovnat	k5eAaImIp3nS	rovnat
jedné	jeden	k4xCgFnSc3	jeden
tisícině	tisícina	k1gFnSc3	tisícina
litru	litr	k1gInSc2	litr
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
6	[number]	k4	6
m3	m3	k4	m3
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
přesně	přesně	k6eAd1	přesně
rovno	roven	k2eAgNnSc4d1	rovno
1	[number]	k4	1
cm3	cm3	k4	cm3
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
je	být	k5eAaImIp3nS	být
složeninou	složenina	k1gFnSc7	složenina
předpony	předpona	k1gFnSc2	předpona
soustavy	soustava	k1gFnSc2	soustava
SI	se	k3xPyFc3	se
mili	mili	k6eAd1	mili
(	(	kIx(	(
<g/>
=	=	kIx~	=
tisícina	tisícina	k1gFnSc1	tisícina
<g/>
)	)	kIx)	)
a	a	k8xC	a
názvu	název	k1gInSc2	název
základnější	základní	k2eAgFnSc2d2	základnější
jednotky	jednotka	k1gFnSc2	jednotka
litr	litr	k1gInSc1	litr
<g/>
.	.	kIx.	.
<g/>
Pro	pro	k7c4	pro
porovnání	porovnání	k1gNnSc4	porovnání
objemů	objem	k1gInPc2	objem
řádově	řádově	k6eAd1	řádově
jednoho	jeden	k4xCgInSc2	jeden
mililitru	mililitr	k1gInSc2	mililitr
viz	vidět	k5eAaImRp2nS	vidět
1	[number]	k4	1
E-6	E-6	k1gFnPc7	E-6
m3	m3	k4	m3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Centilitr	centilitr	k1gInSc1	centilitr
<g/>
,	,	kIx,	,
značený	značený	k2eAgInSc1d1	značený
cl	cl	k?	cl
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
metrická	metrický	k2eAgFnSc1d1	metrická
jednotka	jednotka	k1gFnSc1	jednotka
objemu	objem	k1gInSc2	objem
<g/>
,	,	kIx,	,
rovná	rovnat	k5eAaImIp3nS	rovnat
jedné	jeden	k4xCgFnSc3	jeden
setině	setina	k1gFnSc3	setina
litru	litr	k1gInSc2	litr
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
5	[number]	k4	5
m3	m3	k4	m3
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
je	být	k5eAaImIp3nS	být
složeninou	složenina	k1gFnSc7	složenina
předpony	předpona	k1gFnSc2	předpona
soustavy	soustava	k1gFnSc2	soustava
SI	si	k1gNnSc2	si
centi	cenť	k1gFnSc2	cenť
(	(	kIx(	(
<g/>
=	=	kIx~	=
setina	setina	k1gFnSc1	setina
<g/>
)	)	kIx)	)
a	a	k8xC	a
názvu	název	k1gInSc2	název
základnější	základní	k2eAgFnSc2d2	základnější
jednotky	jednotka	k1gFnSc2	jednotka
litr	litr	k1gInSc1	litr
<g/>
.	.	kIx.	.
<g/>
Pro	pro	k7c4	pro
porovnání	porovnání	k1gNnSc4	porovnání
objemů	objem	k1gInPc2	objem
řádově	řádově	k6eAd1	řádově
jednoho	jeden	k4xCgInSc2	jeden
centilitru	centilitr	k1gInSc2	centilitr
viz	vidět	k5eAaImRp2nS	vidět
1	[number]	k4	1
E-5	E-5	k1gFnPc7	E-5
m3	m3	k4	m3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Decilitr	decilitr	k1gInSc1	decilitr
<g/>
,	,	kIx,	,
značený	značený	k2eAgInSc1d1	značený
dl	dl	k?	dl
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jednotka	jednotka	k1gFnSc1	jednotka
objemu	objem	k1gInSc2	objem
rovná	rovnat	k5eAaImIp3nS	rovnat
jedné	jeden	k4xCgFnSc6	jeden
desetině	desetina	k1gFnSc6	desetina
litru	litr	k1gInSc2	litr
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
4	[number]	k4	4
m3	m3	k4	m3
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
je	být	k5eAaImIp3nS	být
složeninou	složenina	k1gFnSc7	složenina
předpony	předpona	k1gFnSc2	předpona
soustavy	soustava	k1gFnSc2	soustava
SI	si	k1gNnSc2	si
deci	deci	k1gNnSc2	deci
(	(	kIx(	(
<g/>
=	=	kIx~	=
desetina	desetina	k1gFnSc1	desetina
<g/>
)	)	kIx)	)
a	a	k8xC	a
názvu	název	k1gInSc2	název
základnější	základní	k2eAgFnSc2d2	základnější
jednotky	jednotka	k1gFnSc2	jednotka
litr	litr	k1gInSc1	litr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lidovém	lidový	k2eAgInSc6d1	lidový
úzu	úzus	k1gInSc6	úzus
se	se	k3xPyFc4	se
decilitru	decilitr	k1gInSc3	decilitr
říká	říkat	k5eAaImIp3nS	říkat
deci	deci	k1gNnSc1	deci
a	a	k8xC	a
často	často	k6eAd1	často
se	se	k3xPyFc4	se
značí	značit	k5eAaImIp3nS	značit
nesprávně	správně	k6eNd1	správně
jako	jako	k9	jako
dcl	dcl	k?	dcl
<g/>
.	.	kIx.	.
<g/>
Jeden	jeden	k4xCgInSc1	jeden
decilitr	decilitr	k1gInSc1	decilitr
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
objem	objem	k1gInSc4	objem
běžného	běžný	k2eAgInSc2d1	běžný
šálku	šálek	k1gInSc2	šálek
kávy	káva	k1gFnSc2	káva
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
porovnání	porovnání	k1gNnSc4	porovnání
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
řádově	řádově	k6eAd1	řádově
stejnými	stejný	k2eAgFnPc7d1	stejná
objemy	objem	k1gInPc1	objem
viz	vidět	k5eAaImRp2nS	vidět
1	[number]	k4	1
E-4	E-4	k1gFnPc7	E-4
m3	m3	k4	m3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hektolitr	hektolitr	k1gInSc1	hektolitr
<g/>
,	,	kIx,	,
značený	značený	k2eAgInSc1d1	značený
hl	hl	k?	hl
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
metrická	metrický	k2eAgFnSc1d1	metrická
jednotka	jednotka	k1gFnSc1	jednotka
objemu	objem	k1gInSc2	objem
<g/>
,	,	kIx,	,
rovná	rovnat	k5eAaImIp3nS	rovnat
100	[number]	k4	100
litrům	litr	k1gInPc3	litr
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
1	[number]	k4	1
m3	m3	k4	m3
neboli	neboli	k8xC	neboli
0,1	[number]	k4	0,1
m3	m3	k4	m3
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
je	být	k5eAaImIp3nS	být
složeninou	složenina	k1gFnSc7	složenina
předpony	předpona	k1gFnSc2	předpona
soustavy	soustava	k1gFnSc2	soustava
SI	si	k1gNnSc2	si
hekto	hekt	k2eAgNnSc1d1	hekt
(	(	kIx(	(
<g/>
=	=	kIx~	=
sto	sto	k4xCgNnSc4	sto
<g/>
)	)	kIx)	)
a	a	k8xC	a
názvu	název	k1gInSc2	název
základnější	základní	k2eAgFnSc2d2	základnější
jednotky	jednotka	k1gFnSc2	jednotka
litr	litr	k1gInSc1	litr
<g/>
.	.	kIx.	.
<g/>
Pro	pro	k7c4	pro
porovnání	porovnání	k1gNnSc4	porovnání
objemů	objem	k1gInPc2	objem
řádově	řádově	k6eAd1	řádově
jednoho	jeden	k4xCgInSc2	jeden
hektolitru	hektolitr	k1gInSc2	hektolitr
viz	vidět	k5eAaImRp2nS	vidět
1	[number]	k4	1
E-1	E-1	k1gFnPc7	E-1
m3	m3	k4	m3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Litr	litr	k1gInSc1	litr
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1793	[number]	k4	1793
zaveden	zavést	k5eAaPmNgMnS	zavést
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
jako	jako	k9	jako
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nových	nový	k2eAgFnPc2d1	nová
"	"	kIx"	"
<g/>
republikánských	republikánský	k2eAgFnPc2d1	republikánská
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
zavedených	zavedený	k2eAgFnPc2d1	zavedená
za	za	k7c2	za
francouzské	francouzský	k2eAgFnSc2d1	francouzská
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
definován	definovat	k5eAaBmNgInS	definovat
jako	jako	k9	jako
jeden	jeden	k4xCgInSc1	jeden
decimetr	decimetr	k1gInSc1	decimetr
krychlový	krychlový	k2eAgInSc1d1	krychlový
<g/>
;	;	kIx,	;
jeho	jeho	k3xOp3gNnSc1	jeho
jméno	jméno	k1gNnSc1	jméno
bylo	být	k5eAaImAgNnS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
z	z	k7c2	z
jména	jméno	k1gNnSc2	jméno
starší	starý	k2eAgFnSc2d2	starší
francouzské	francouzský	k2eAgFnSc2d1	francouzská
jednotky	jednotka	k1gFnSc2	jednotka
litron	litron	k1gMnSc1	litron
(	(	kIx(	(
<g/>
pocházejícího	pocházející	k2eAgMnSc2d1	pocházející
z	z	k7c2	z
řeckého	řecký	k2eAgInSc2d1	řecký
λ	λ	k?	λ
(	(	kIx(	(
<g/>
litra	litra	k1gMnSc1	litra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
značícího	značící	k2eAgInSc2d1	značící
jednotku	jednotka	k1gFnSc4	jednotka
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1901	[number]	k4	1901
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
3	[number]	k4	3
<g/>
.	.	kIx.	.
konferenci	konference	k1gFnSc6	konference
CGPM	CGPM	kA	CGPM
litr	litr	k1gInSc4	litr
předefinován	předefinován	k2eAgInSc1d1	předefinován
jako	jako	k8xS	jako
objem	objem	k1gInSc1	objem
1	[number]	k4	1
kilogramu	kilogram	k1gInSc2	kilogram
čisté	čistý	k2eAgFnSc2d1	čistá
vody	voda	k1gFnSc2	voda
za	za	k7c2	za
její	její	k3xOp3gFnSc2	její
maximální	maximální	k2eAgFnSc2d1	maximální
hustoty	hustota	k1gFnSc2	hustota
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
při	při	k7c6	při
3,98	[number]	k4	3,98
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
při	při	k7c6	při
standardním	standardní	k2eAgInSc6d1	standardní
tlaku	tlak	k1gInSc6	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
hodnota	hodnota	k1gFnSc1	hodnota
je	být	k5eAaImIp3nS	být
právě	právě	k9	právě
1	[number]	k4	1
dm3	dm3	k4	dm3
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
však	však	k9	však
zjistilo	zjistit	k5eAaPmAgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
kvůli	kvůli	k7c3	kvůli
chybě	chyba	k1gFnSc3	chyba
měření	měření	k1gNnSc2	měření
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
objem	objem	k1gInSc4	objem
1	[number]	k4	1
kg	kg	kA	kg
vody	voda	k1gFnSc2	voda
1,000	[number]	k4	1,000
<g/>
0	[number]	k4	0
<g/>
28	[number]	k4	28
dm3	dm3	k4	dm3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
na	na	k7c4	na
12	[number]	k4	12
<g/>
.	.	kIx.	.
konferenci	konference	k1gFnSc6	konference
CGPM	CGPM	kA	CGPM
vrátila	vrátit	k5eAaPmAgFnS	vrátit
původní	původní	k2eAgFnSc1d1	původní
definice	definice	k1gFnSc1	definice
litru	litr	k1gInSc2	litr
jako	jako	k8xS	jako
jiného	jiný	k2eAgInSc2d1	jiný
názvu	název	k1gInSc2	název
pro	pro	k7c4	pro
decimetr	decimetr	k1gInSc4	decimetr
krychlový	krychlový	k2eAgInSc4d1	krychlový
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
však	však	k9	však
doporučeno	doporučit	k5eAaPmNgNnS	doporučit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
jednotka	jednotka	k1gFnSc1	jednotka
používala	používat	k5eAaImAgFnS	používat
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
obchod	obchod	k1gInSc4	obchod
<g/>
,	,	kIx,	,
ne	ne	k9	ne
pro	pro	k7c4	pro
vědecké	vědecký	k2eAgInPc4d1	vědecký
účely	účel	k1gInPc4	účel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
litr	litr	k1gInSc4	litr
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
litr	litr	k1gInSc1	litr
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
