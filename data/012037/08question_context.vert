<s>
Litr	litr	k1gInSc1	litr
je	být	k5eAaImIp3nS	být
metrická	metrický	k2eAgFnSc1d1	metrická
jednotka	jednotka	k1gFnSc1	jednotka
objemu	objem	k1gInSc2	objem
odpovídající	odpovídající	k2eAgInSc1d1	odpovídající
krychlovému	krychlový	k2eAgInSc3d1	krychlový
decimetru	decimetr	k1gInSc3	decimetr
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
jednotkou	jednotka	k1gFnSc7	jednotka
SI	si	k1gNnSc2	si
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
akceptováno	akceptován	k2eAgNnSc1d1	akceptováno
její	její	k3xOp3gNnSc4	její
používání	používání	k1gNnSc4	používání
společně	společně	k6eAd1	společně
s	s	k7c7	s
SI	si	k1gNnSc7	si
jednotkami	jednotka	k1gFnPc7	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Značí	značit	k5eAaImIp3nS	značit
se	se	k3xPyFc4	se
malým	malý	k2eAgNnSc7d1	malé
písmenem	písmeno	k1gNnSc7	písmeno
l	l	kA	l
<g/>
.	.	kIx.
</s>
