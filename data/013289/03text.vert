<p>
<s>
Louis	Louis	k1gMnSc1	Louis
Dudek	Dudek	k1gMnSc1	Dudek
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1918	[number]	k4	1918
–	–	k?	–
23	[number]	k4	23
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
kanadský	kanadský	k2eAgMnSc1d1	kanadský
básník	básník	k1gMnSc1	básník
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
do	do	k7c2	do
rodiny	rodina	k1gFnSc2	rodina
polských	polský	k2eAgMnPc2d1	polský
emigrantů	emigrant	k1gMnPc2	emigrant
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Montréal	Montréal	k1gInSc1	Montréal
v	v	k7c6	v
kanadské	kanadský	k2eAgFnSc6d1	kanadská
provincii	provincie	k1gFnSc6	provincie
Québec	Québec	k1gInSc4	Québec
<g/>
.	.	kIx.	.
</s>
<s>
Studoval	studovat	k5eAaImAgInS	studovat
na	na	k7c6	na
montréalské	montréalský	k2eAgFnSc6d1	montréalský
McGillově	McGillův	k2eAgFnSc6d1	McGillova
universitě	universita	k1gFnSc6	universita
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
úspěšně	úspěšně	k6eAd1	úspěšně
dokončil	dokončit	k5eAaPmAgMnS	dokončit
získáním	získání	k1gNnSc7	získání
titulu	titul	k1gInSc2	titul
B.A.	B.A.	k1gMnSc2	B.A.
Později	pozdě	k6eAd2	pozdě
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
newyorské	newyorský	k2eAgFnSc6d1	newyorská
Columbia	Columbia	k1gFnSc1	Columbia
University	universita	k1gFnPc1	universita
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
získal	získat	k5eAaPmAgMnS	získat
titul	titul	k1gInSc4	titul
PhD	PhD	k1gFnSc2	PhD
<g/>
.	.	kIx.	.
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
se	se	k3xPyFc4	se
věnovat	věnovat	k5eAaImF	věnovat
pedagogické	pedagogický	k2eAgFnSc3d1	pedagogická
činnosti	činnost	k1gFnSc3	činnost
a	a	k8xC	a
psaní	psaní	k1gNnSc3	psaní
básní	báseň	k1gFnPc2	báseň
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
čtyřicátých	čtyřicátý	k4xOgNnPc2	čtyřicátý
let	léto	k1gNnPc2	léto
si	se	k3xPyFc3	se
dopisoval	dopisovat	k5eAaImAgInS	dopisovat
s	s	k7c7	s
básníkem	básník	k1gMnSc7	básník
Ezrou	Ezra	k1gMnSc7	Ezra
Poundem	pound	k1gInSc7	pound
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
měl	mít	k5eAaImAgInS	mít
značný	značný	k2eAgInSc1d1	značný
vliv	vliv	k1gInSc1	vliv
a	a	k8xC	a
se	s	k7c7	s
kterým	který	k3yIgMnSc7	který
se	se	k3xPyFc4	se
také	také	k9	také
později	pozdě	k6eAd2	pozdě
osobně	osobně	k6eAd1	osobně
setkal	setkat	k5eAaPmAgMnS	setkat
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1951	[number]	k4	1951
Dudek	Dudek	k1gMnSc1	Dudek
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
na	na	k7c6	na
McGillově	McGillův	k2eAgFnSc6d1	McGillova
universitě	universita	k1gFnSc6	universita
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
83	[number]	k4	83
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
synem	syn	k1gMnSc7	syn
je	být	k5eAaImIp3nS	být
informatik	informatik	k1gMnSc1	informatik
Gregory	Gregor	k1gMnPc7	Gregor
Dudek	Dudek	k1gMnSc1	Dudek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
