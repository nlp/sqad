<s>
Sir	sir	k1gMnSc1	sir
Isaac	Isaac	k1gFnSc1	Isaac
Newton	Newton	k1gMnSc1	Newton
(	(	kIx(	(
<g/>
[	[	kIx(	[
<g/>
ˌ	ˌ	k?	ˌ
ˈ	ˈ	k?	ˈ
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
4	[number]	k4	4
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1643	[number]	k4	1643
-	-	kIx~	-
31	[number]	k4	31
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1727	[number]	k4	1727
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
anglický	anglický	k2eAgMnSc1d1	anglický
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
,	,	kIx,	,
matematik	matematik	k1gMnSc1	matematik
(	(	kIx(	(
<g/>
profesor	profesor	k1gMnSc1	profesor
naturální	naturální	k2eAgFnSc2d1	naturální
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
astronom	astronom	k1gMnSc1	astronom
<g/>
,	,	kIx,	,
alchymista	alchymista	k1gMnSc1	alchymista
a	a	k8xC	a
teolog	teolog	k1gMnSc1	teolog
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
bývá	bývat	k5eAaImIp3nS	bývat
často	často	k6eAd1	často
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejvlivnějších	vlivný	k2eAgFnPc2d3	nejvlivnější
osob	osoba	k1gFnPc2	osoba
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
lidstva	lidstvo	k1gNnSc2	lidstvo
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
publikace	publikace	k1gFnPc1	publikace
Philosophiæ	Philosophiæ	k1gFnSc2	Philosophiæ
Naturalis	Naturalis	k1gFnSc2	Naturalis
Principia	principium	k1gNnSc2	principium
Mathematica	Mathematic	k2eAgFnSc1d1	Mathematica
<g/>
,	,	kIx,	,
vydaná	vydaný	k2eAgFnSc1d1	vydaná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1687	[number]	k4	1687
<g/>
,	,	kIx,	,
položila	položit	k5eAaPmAgFnS	položit
základy	základ	k1gInPc4	základ
klasické	klasický	k2eAgFnSc2d1	klasická
mechaniky	mechanika	k1gFnSc2	mechanika
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
bývá	bývat	k5eAaImIp3nS	bývat
řazena	řadit	k5eAaImNgFnS	řadit
mezi	mezi	k7c4	mezi
nejdůležitější	důležitý	k2eAgFnPc4d3	nejdůležitější
knihy	kniha	k1gFnPc4	kniha
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
vědy	věda	k1gFnSc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
Newton	Newton	k1gMnSc1	Newton
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
popisuje	popisovat	k5eAaImIp3nS	popisovat
zákon	zákon	k1gInSc1	zákon
všeobecné	všeobecný	k2eAgFnSc2d1	všeobecná
gravitace	gravitace	k1gFnSc2	gravitace
a	a	k8xC	a
tři	tři	k4xCgInPc4	tři
zákony	zákon	k1gInPc4	zákon
pohybu	pohyb	k1gInSc2	pohyb
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
na	na	k7c4	na
další	další	k2eAgNnPc4d1	další
tři	tři	k4xCgNnPc4	tři
staletí	staletí	k1gNnPc4	staletí
staly	stát	k5eAaPmAgFnP	stát
základem	základ	k1gInSc7	základ
vědeckého	vědecký	k2eAgInSc2d1	vědecký
pohledu	pohled	k1gInSc2	pohled
na	na	k7c4	na
fyzický	fyzický	k2eAgInSc4d1	fyzický
vesmír	vesmír	k1gInSc4	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Newton	Newton	k1gMnSc1	Newton
propojil	propojit	k5eAaPmAgMnS	propojit
Keplerovy	Keplerův	k2eAgInPc4d1	Keplerův
zákony	zákon	k1gInPc4	zákon
pohybu	pohyb	k1gInSc2	pohyb
planet	planeta	k1gFnPc2	planeta
s	s	k7c7	s
vlastní	vlastní	k2eAgFnSc7d1	vlastní
teorií	teorie	k1gFnSc7	teorie
gravitace	gravitace	k1gFnSc2	gravitace
a	a	k8xC	a
dokázal	dokázat	k5eAaPmAgMnS	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pohyb	pohyb	k1gInSc1	pohyb
předmětů	předmět	k1gInPc2	předmět
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
se	se	k3xPyFc4	se
řídí	řídit	k5eAaImIp3nP	řídit
stejnými	stejný	k2eAgNnPc7d1	stejné
pravidly	pravidlo	k1gNnPc7	pravidlo
jako	jako	k8xS	jako
pohyb	pohyb	k1gInSc4	pohyb
vesmírných	vesmírný	k2eAgNnPc2d1	vesmírné
těles	těleso	k1gNnPc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
smetl	smetnout	k5eAaPmAgMnS	smetnout
poslední	poslední	k2eAgFnPc4d1	poslední
pochyby	pochyba	k1gFnPc4	pochyba
o	o	k7c6	o
heliocentrismu	heliocentrismus	k1gInSc6	heliocentrismus
a	a	k8xC	a
přispěl	přispět	k5eAaPmAgInS	přispět
k	k	k7c3	k
vědecké	vědecký	k2eAgFnSc3d1	vědecká
revoluci	revoluce	k1gFnSc3	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Newton	Newton	k1gMnSc1	Newton
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
považován	považován	k2eAgInSc1d1	považován
dokonce	dokonce	k9	dokonce
za	za	k7c2	za
zakladatele	zakladatel	k1gMnSc2	zakladatel
exaktní	exaktní	k2eAgFnSc2d1	exaktní
vědy	věda	k1gFnSc2	věda
jako	jako	k8xC	jako
zcela	zcela	k6eAd1	zcela
nového	nový	k2eAgInSc2d1	nový
pohledu	pohled	k1gInSc2	pohled
na	na	k7c4	na
reálný	reálný	k2eAgInSc4d1	reálný
svět	svět	k1gInSc4	svět
<g/>
,	,	kIx,	,
umožňujícího	umožňující	k2eAgMnSc2d1	umožňující
rozvoj	rozvoj	k1gInSc4	rozvoj
(	(	kIx(	(
<g/>
moderní	moderní	k2eAgFnSc2d1	moderní
<g/>
)	)	kIx)	)
matematizované	matematizovaný	k2eAgFnSc2d1	matematizovaná
vědy	věda	k1gFnSc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mechanice	mechanika	k1gFnSc6	mechanika
Newton	Newton	k1gMnSc1	Newton
formuloval	formulovat	k5eAaImAgMnS	formulovat
teorii	teorie	k1gFnSc4	teorie
o	o	k7c4	o
zachování	zachování	k1gNnSc4	zachování
hybnosti	hybnost	k1gFnSc2	hybnost
a	a	k8xC	a
momentu	moment	k1gInSc2	moment
hybnosti	hybnost	k1gFnSc2	hybnost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
poli	pole	k1gNnSc6	pole
optiky	optika	k1gFnSc2	optika
sestavil	sestavit	k5eAaPmAgInS	sestavit
první	první	k4xOgInSc4	první
zrcadlový	zrcadlový	k2eAgInSc4d1	zrcadlový
dalekohled	dalekohled	k1gInSc4	dalekohled
a	a	k8xC	a
na	na	k7c6	na
základě	základ	k1gInSc6	základ
pozorování	pozorování	k1gNnSc2	pozorování
<g/>
,	,	kIx,	,
že	že	k8xS	že
optické	optický	k2eAgInPc1d1	optický
hranoly	hranol	k1gInPc1	hranol
rozkládají	rozkládat	k5eAaImIp3nP	rozkládat
bílé	bílý	k2eAgNnSc4d1	bílé
světlo	světlo	k1gNnSc4	světlo
do	do	k7c2	do
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
barev	barva	k1gFnPc2	barva
viditelného	viditelný	k2eAgNnSc2d1	viditelné
spektra	spektrum	k1gNnSc2	spektrum
<g/>
,	,	kIx,	,
rozvedl	rozvést	k5eAaPmAgMnS	rozvést
teorii	teorie	k1gFnSc4	teorie
barev	barva	k1gFnPc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
vyslovil	vyslovit	k5eAaPmAgInS	vyslovit
zákon	zákon	k1gInSc1	zákon
chladnutí	chladnutí	k1gNnSc2	chladnutí
a	a	k8xC	a
zkoumal	zkoumat	k5eAaImAgMnS	zkoumat
rychlost	rychlost	k1gFnSc4	rychlost
zvuku	zvuk	k1gInSc2	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
matematice	matematika	k1gFnSc6	matematika
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
s	s	k7c7	s
Gottfriedem	Gottfried	k1gMnSc7	Gottfried
Leibnizem	Leibniz	k1gMnSc7	Leibniz
o	o	k7c4	o
zásluhy	zásluha	k1gFnPc4	zásluha
na	na	k7c6	na
objevu	objev	k1gInSc6	objev
integrálního	integrální	k2eAgInSc2d1	integrální
počtu	počet	k1gInSc2	počet
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
zobecnil	zobecnit	k5eAaPmAgMnS	zobecnit
binomickou	binomický	k2eAgFnSc4d1	binomická
větu	věta	k1gFnSc4	věta
<g/>
,	,	kIx,	,
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
takzvanou	takzvaný	k2eAgFnSc4d1	takzvaná
"	"	kIx"	"
<g/>
Newtonovu	Newtonův	k2eAgFnSc4d1	Newtonova
metodu	metoda	k1gFnSc4	metoda
<g/>
"	"	kIx"	"
řešení	řešení	k1gNnPc1	řešení
soustav	soustava	k1gFnPc2	soustava
nelineárních	lineární	k2eNgFnPc2d1	nelineární
rovnic	rovnice	k1gFnPc2	rovnice
a	a	k8xC	a
přispěl	přispět	k5eAaPmAgMnS	přispět
k	k	k7c3	k
výzkumu	výzkum	k1gInSc3	výzkum
mocninných	mocninný	k2eAgFnPc2d1	mocninná
řad	řada	k1gFnPc2	řada
<g/>
.	.	kIx.	.
</s>
<s>
Newton	Newton	k1gMnSc1	Newton
byl	být	k5eAaImAgMnS	být
horlivě	horlivě	k6eAd1	horlivě
věřícím	věřící	k2eAgMnSc7d1	věřící
křesťanem	křesťan	k1gMnSc7	křesťan
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
zastával	zastávat	k5eAaImAgMnS	zastávat
místy	místy	k6eAd1	místy
nekonvenční	konvenční	k2eNgInPc4d1	nekonvenční
názory	názor	k1gInPc4	názor
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
vzpomínán	vzpomínán	k2eAgInSc1d1	vzpomínán
především	především	k9	především
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
přínos	přínos	k1gInSc4	přínos
vědě	věda	k1gFnSc3	věda
<g/>
,	,	kIx,	,
větší	veliký	k2eAgFnSc1d2	veliký
část	část	k1gFnSc1	část
svých	svůj	k3xOyFgInPc2	svůj
textů	text	k1gInPc2	text
věnoval	věnovat	k5eAaImAgMnS	věnovat
výkladům	výklad	k1gInPc3	výklad
Bible	bible	k1gFnSc2	bible
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
odborníci	odborník	k1gMnPc1	odborník
se	se	k3xPyFc4	se
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
měl	mít	k5eAaImAgMnS	mít
Aspergerův	Aspergerův	k2eAgInSc4d1	Aspergerův
syndrom	syndrom	k1gInSc4	syndrom
<g/>
.	.	kIx.	.
</s>
<s>
Isaac	Isaac	k1gInSc1	Isaac
Newton	newton	k1gInSc1	newton
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgInS	narodit
4	[number]	k4	4
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1643	[number]	k4	1643
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
tehdy	tehdy	k6eAd1	tehdy
užívaného	užívaný	k2eAgInSc2d1	užívaný
Juliánského	juliánský	k2eAgInSc2d1	juliánský
kalendáře	kalendář	k1gInSc2	kalendář
25	[number]	k4	25
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1642	[number]	k4	1642
<g/>
)	)	kIx)	)
ve	v	k7c6	v
Woolsthorpu	Woolsthorp	k1gInSc6	Woolsthorp
poblíž	poblíž	k7c2	poblíž
Granthamu	Grantham	k1gInSc2	Grantham
v	v	k7c6	v
Lincolnshire	Lincolnshir	k1gInSc5	Lincolnshir
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
také	také	k9	také
Isaac	Isaac	k1gFnSc4	Isaac
Newton	newton	k1gInSc4	newton
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
zámožným	zámožný	k2eAgMnSc7d1	zámožný
vlastníkem	vlastník	k1gMnSc7	vlastník
půdy	půda	k1gFnSc2	půda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neměl	mít	k5eNaImAgMnS	mít
žádné	žádný	k3yNgNnSc4	žádný
vzdělání	vzdělání	k1gNnSc4	vzdělání
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
tři	tři	k4xCgInPc4	tři
měsíce	měsíc	k1gInPc4	měsíc
před	před	k7c7	před
narozením	narození	k1gNnSc7	narození
syna	syn	k1gMnSc2	syn
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byly	být	k5eAaImAgFnP	být
Newtonovi	Newton	k1gMnSc6	Newton
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
provdala	provdat	k5eAaPmAgFnS	provdat
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
<g/>
,	,	kIx,	,
Hannah	Hannah	k1gMnSc1	Hannah
Ayscough	Ayscough	k1gMnSc1	Ayscough
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
jednou	jednou	k6eAd1	jednou
a	a	k8xC	a
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
se	se	k3xPyFc4	se
do	do	k7c2	do
sousední	sousední	k2eAgFnSc2d1	sousední
vesnice	vesnice	k1gFnSc2	vesnice
North	North	k1gInSc4	North
Withamu	Witham	k1gInSc2	Witham
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
manžela	manžel	k1gMnSc2	manžel
si	se	k3xPyFc3	se
vzala	vzít	k5eAaPmAgFnS	vzít
rev	rev	k?	rev
<g/>
.	.	kIx.	.
</s>
<s>
Barnabase	Barnabase	k6eAd1	Barnabase
Smithe	Smithe	k1gInSc1	Smithe
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
až	až	k9	až
do	do	k7c2	do
otčímovy	otčímův	k2eAgFnSc2d1	otčímův
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
o	o	k7c4	o
Newtona	Newton	k1gMnSc4	Newton
starali	starat	k5eAaImAgMnP	starat
matčini	matčin	k2eAgMnPc1d1	matčin
rodiče	rodič	k1gMnPc1	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Rodina	rodina	k1gFnSc1	rodina
Ayscoughů	Ayscough	k1gMnPc2	Ayscough
stála	stát	k5eAaImAgFnS	stát
výše	vysoce	k6eAd2	vysoce
na	na	k7c6	na
společenském	společenský	k2eAgInSc6d1	společenský
žebříčku	žebříček	k1gInSc6	žebříček
než	než	k8xS	než
Newtonové	Newtonová	k1gFnSc6	Newtonová
(	(	kIx(	(
<g/>
Newtonův	Newtonův	k2eAgMnSc1d1	Newtonův
dědeček	dědeček	k1gMnSc1	dědeček
James	James	k1gMnSc1	James
Ayscough	Ayscough	k1gMnSc1	Ayscough
byl	být	k5eAaImAgInS	být
šlechticem	šlechtic	k1gMnSc7	šlechtic
a	a	k8xC	a
matčin	matčin	k2eAgMnSc1d1	matčin
bratr	bratr	k1gMnSc1	bratr
William	William	k1gInSc4	William
farářem	farář	k1gMnSc7	farář
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c4	na
Trinity	Trinita	k1gFnPc4	Trinita
College	Colleg	k1gFnSc2	Colleg
v	v	k7c6	v
Cambridge	Cambridge	k1gFnSc1	Cambridge
<g/>
)	)	kIx)	)
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
nim	on	k3xPp3gFnPc3	on
Newtonovi	Newton	k1gMnSc6	Newton
dostalo	dostat	k5eAaPmAgNnS	dostat
vzdělání	vzdělání	k1gNnSc3	vzdělání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1653	[number]	k4	1653
<g/>
,	,	kIx,	,
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
druhého	druhý	k4xOgMnSc2	druhý
manžela	manžel	k1gMnSc2	manžel
se	se	k3xPyFc4	se
matka	matka	k1gFnSc1	matka
vrátila	vrátit	k5eAaPmAgFnS	vrátit
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Woolsthorpu	Woolsthorp	k1gInSc2	Woolsthorp
a	a	k8xC	a
desetiletý	desetiletý	k2eAgInSc4d1	desetiletý
Isaac	Isaac	k1gInSc4	Isaac
opět	opět	k6eAd1	opět
bydlel	bydlet	k5eAaImAgMnS	bydlet
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
matkou	matka	k1gFnSc7	matka
a	a	k8xC	a
se	s	k7c7	s
třemi	tři	k4xCgMnPc7	tři
nevlastními	vlastní	k2eNgMnPc7d1	nevlastní
sourozenci	sourozenec	k1gMnPc7	sourozenec
(	(	kIx(	(
<g/>
Mary	Mary	k1gFnSc4	Mary
<g/>
,	,	kIx,	,
Benjamin	Benjamin	k1gMnSc1	Benjamin
a	a	k8xC	a
Hannah	Hannah	k1gMnSc1	Hannah
Smithovi	Smithův	k2eAgMnPc1d1	Smithův
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
deseti	deset	k4xCc6	deset
letech	let	k1gInPc6	let
začal	začít	k5eAaPmAgMnS	začít
chodit	chodit	k5eAaImF	chodit
do	do	k7c2	do
vesnické	vesnický	k2eAgFnSc2d1	vesnická
školy	škola	k1gFnSc2	škola
v	v	k7c6	v
Skillingtonu	Skillington	k1gInSc6	Skillington
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
ve	v	k7c6	v
Stoku	stok	k1gInSc6	stok
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1655	[number]	k4	1655
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
v	v	k7c6	v
Granthamu	Grantham	k1gInSc6	Grantham
tamější	tamější	k2eAgNnSc1d1	tamější
gymnázium	gymnázium	k1gNnSc1	gymnázium
<g/>
.	.	kIx.	.
</s>
<s>
Ubytován	ubytován	k2eAgMnSc1d1	ubytován
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
domácnosti	domácnost	k1gFnSc6	domácnost
lékárníka	lékárník	k1gMnSc2	lékárník
pana	pan	k1gMnSc2	pan
Clarka	Clarek	k1gMnSc2	Clarek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1658	[number]	k4	1658
<g/>
-	-	kIx~	-
<g/>
1661	[number]	k4	1661
se	se	k3xPyFc4	se
v	v	k7c6	v
Grathamu	Gratham	k1gInSc6	Gratham
připravoval	připravovat	k5eAaImAgMnS	připravovat
na	na	k7c4	na
studia	studio	k1gNnPc4	studio
v	v	k7c6	v
Cambridge	Cambridge	k1gFnSc1	Cambridge
<g/>
.	.	kIx.	.
</s>
<s>
5	[number]	k4	5
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1661	[number]	k4	1661
za	za	k7c2	za
podpory	podpora	k1gFnSc2	podpora
svého	svůj	k3xOyFgMnSc2	svůj
strýce	strýc	k1gMnSc2	strýc
Williama	William	k1gMnSc2	William
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
Newton	Newton	k1gMnSc1	Newton
jako	jako	k8xC	jako
osmnáctiletý	osmnáctiletý	k2eAgMnSc1d1	osmnáctiletý
studovat	studovat	k5eAaImF	studovat
na	na	k7c4	na
Trinity	Trinit	k2eAgFnPc4d1	Trinita
College	Colleg	k1gFnPc4	Colleg
v	v	k7c4	v
Cambridge	Cambridge	k1gFnPc4	Cambridge
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
byl	být	k5eAaImAgMnS	být
jeho	jeho	k3xOp3gMnSc1	jeho
učitelem	učitel	k1gMnSc7	učitel
známý	známý	k2eAgMnSc1d1	známý
fyzik	fyzik	k1gMnSc1	fyzik
a	a	k8xC	a
matematik	matematik	k1gMnSc1	matematik
Isaac	Isaac	k1gFnSc4	Isaac
Barrow	Barrow	k1gFnSc2	Barrow
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
studia	studio	k1gNnSc2	studio
zde	zde	k6eAd1	zde
sice	sice	k8xC	sice
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byly	být	k5eAaImAgFnP	být
ještě	ještě	k6eAd1	ještě
Aristotelovy	Aristotelův	k2eAgFnPc1d1	Aristotelova
myšlenky	myšlenka	k1gFnPc1	myšlenka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Newton	Newton	k1gMnSc1	Newton
se	se	k3xPyFc4	se
také	také	k9	také
zajímal	zajímat	k5eAaImAgMnS	zajímat
o	o	k7c4	o
modernější	moderní	k2eAgMnPc4d2	modernější
myslitele	myslitel	k1gMnPc4	myslitel
<g/>
,	,	kIx,	,
četl	číst	k5eAaImAgMnS	číst
díla	dílo	k1gNnPc4	dílo
G.	G.	kA	G.
Galilea	Galilea	k1gFnSc1	Galilea
a	a	k8xC	a
R.	R.	kA	R.
Descarta	Descarta	k1gFnSc1	Descarta
<g/>
.	.	kIx.	.
</s>
<s>
Přečetl	přečíst	k5eAaPmAgMnS	přečíst
Keplerovo	Keplerův	k2eAgNnSc4d1	Keplerovo
dílo	dílo	k1gNnSc4	dílo
"	"	kIx"	"
<g/>
Optics	Optics	k1gInSc1	Optics
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
seznámil	seznámit	k5eAaPmAgMnS	seznámit
se	se	k3xPyFc4	se
s	s	k7c7	s
Euklidovými	Euklidův	k2eAgInPc7d1	Euklidův
"	"	kIx"	"
<g/>
Základy	základ	k1gInPc7	základ
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
srpna	srpen	k1gInSc2	srpen
1665	[number]	k4	1665
do	do	k7c2	do
dubna	duben	k1gInSc2	duben
1667	[number]	k4	1667
byla	být	k5eAaImAgFnS	být
univerzita	univerzita	k1gFnSc1	univerzita
uzavřena	uzavřít	k5eAaPmNgFnS	uzavřít
kvůli	kvůli	k7c3	kvůli
morové	morový	k2eAgFnSc3d1	morová
epidemii	epidemie	k1gFnSc3	epidemie
a	a	k8xC	a
Newton	newton	k1gInSc1	newton
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
do	do	k7c2	do
rodného	rodný	k2eAgInSc2d1	rodný
Woolsthorpu	Woolsthorp	k1gInSc2	Woolsthorp
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mu	on	k3xPp3gNnSc3	on
podle	podle	k7c2	podle
pověsti	pověst	k1gFnSc2	pověst
spadlo	spadnout	k5eAaPmAgNnS	spadnout
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
jablko	jablko	k1gNnSc4	jablko
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
přišel	přijít	k5eAaPmAgMnS	přijít
na	na	k7c4	na
myšlenku	myšlenka	k1gFnSc4	myšlenka
gravitace	gravitace	k1gFnSc2	gravitace
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
těchto	tento	k3xDgNnPc2	tento
dvou	dva	k4xCgNnPc2	dva
let	léto	k1gNnPc2	léto
pobytu	pobyt	k1gInSc2	pobyt
na	na	k7c6	na
rodném	rodný	k2eAgInSc6d1	rodný
statku	statek	k1gInSc6	statek
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
svůj	svůj	k3xOyFgInSc4	svůj
diferenciální	diferenciální	k2eAgInSc4d1	diferenciální
počet	počet	k1gInSc4	počet
<g/>
,	,	kIx,	,
rozvinul	rozvinout	k5eAaPmAgInS	rozvinout
svou	svůj	k3xOyFgFnSc4	svůj
teorii	teorie	k1gFnSc4	teorie
gravitace	gravitace	k1gFnSc2	gravitace
a	a	k8xC	a
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
vlastní	vlastní	k2eAgFnSc4d1	vlastní
teorii	teorie	k1gFnSc4	teorie
podstaty	podstata	k1gFnSc2	podstata
světla	světlo	k1gNnSc2	světlo
a	a	k8xC	a
barev	barva	k1gFnPc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1665	[number]	k4	1665
získal	získat	k5eAaPmAgInS	získat
bakalářský	bakalářský	k2eAgInSc1d1	bakalářský
titul	titul	k1gInSc1	titul
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1667	[number]	k4	1667
stálé	stálý	k2eAgFnSc2d1	stálá
místo	místo	k1gNnSc4	místo
na	na	k7c4	na
Trinity	Trinit	k2eAgFnPc4d1	Trinita
College	Colleg	k1gFnPc4	Colleg
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1668	[number]	k4	1668
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
magistrem	magister	k1gMnSc7	magister
svobodných	svobodný	k2eAgNnPc2d1	svobodné
umění	umění	k1gNnPc2	umění
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1669	[number]	k4	1669
lukasiánským	lukasiánský	k2eAgMnSc7d1	lukasiánský
profesorem	profesor	k1gMnSc7	profesor
matematiky	matematika	k1gFnSc2	matematika
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
po	po	k7c6	po
svém	svůj	k3xOyFgMnSc6	svůj
učiteli	učitel	k1gMnSc6	učitel
Isaacu	Isaac	k1gMnSc6	Isaac
Barrowovi	Barrowa	k1gMnSc6	Barrowa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
sestrojil	sestrojit	k5eAaPmAgMnS	sestrojit
svůj	svůj	k3xOyFgInSc4	svůj
zrcadlový	zrcadlový	k2eAgInSc4d1	zrcadlový
dalekohled	dalekohled	k1gInSc4	dalekohled
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
řešil	řešit	k5eAaImAgInS	řešit
problém	problém	k1gInSc4	problém
barevné	barevný	k2eAgFnSc2d1	barevná
aberace	aberace	k1gFnSc2	aberace
u	u	k7c2	u
dalekohledů	dalekohled	k1gInPc2	dalekohled
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1672	[number]	k4	1672
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
základě	základ	k1gInSc6	základ
svého	svůj	k3xOyFgInSc2	svůj
vynálezu	vynález	k1gInSc2	vynález
zvolen	zvolit	k5eAaPmNgMnS	zvolit
členem	člen	k1gMnSc7	člen
Královské	královský	k2eAgFnSc2d1	královská
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
19	[number]	k4	19
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1672	[number]	k4	1672
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c4	v
Philosophical	Philosophical	k1gFnSc4	Philosophical
Transactions	Transactionsa	k1gFnPc2	Transactionsa
jeho	jeho	k3xOp3gFnSc2	jeho
první	první	k4xOgFnSc2	první
fyzikální	fyzikální	k2eAgFnSc2d1	fyzikální
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
téma	téma	k1gNnSc4	téma
teorie	teorie	k1gFnSc2	teorie
barev	barva	k1gFnPc2	barva
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1703	[number]	k4	1703
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
předsedou	předseda	k1gMnSc7	předseda
Královské	královský	k2eAgFnSc2d1	královská
společnosti	společnost	k1gFnSc2	společnost
a	a	k8xC	a
zůstal	zůstat	k5eAaPmAgMnS	zůstat
jím	jíst	k5eAaImIp1nS	jíst
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1727	[number]	k4	1727
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1705	[number]	k4	1705
byl	být	k5eAaImAgInS	být
královnou	královna	k1gFnSc7	královna
Annou	Anna	k1gFnSc7	Anna
povýšen	povýšit	k5eAaPmNgInS	povýšit
do	do	k7c2	do
rytířského	rytířský	k2eAgInSc2d1	rytířský
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
poslancem	poslanec	k1gMnSc7	poslanec
anglického	anglický	k2eAgInSc2d1	anglický
parlamentu	parlament	k1gInSc2	parlament
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
zájmu	zájem	k1gInSc6	zájem
Anglie	Anglie	k1gFnSc2	Anglie
odporoval	odporovat	k5eAaImAgInS	odporovat
i	i	k9	i
králi	král	k1gMnPc7	král
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1696	[number]	k4	1696
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
dozorcem	dozorce	k1gMnSc7	dozorce
v	v	k7c6	v
královské	královský	k2eAgFnSc6d1	královská
mincovně	mincovna	k1gFnSc6	mincovna
v	v	k7c6	v
londýnském	londýnský	k2eAgNnSc6d1	Londýnské
Toweru	Towero	k1gNnSc6	Towero
a	a	k8xC	a
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
jejím	její	k3xOp3gMnSc7	její
ministrem	ministr	k1gMnSc7	ministr
<g/>
.	.	kIx.	.
</s>
<s>
Proslavil	proslavit	k5eAaPmAgMnS	proslavit
se	se	k3xPyFc4	se
bojem	boj	k1gInSc7	boj
proti	proti	k7c3	proti
penězokazům	penězokaz	k1gMnPc3	penězokaz
a	a	k8xC	a
dostal	dostat	k5eAaPmAgMnS	dostat
jich	on	k3xPp3gFnPc2	on
několik	několik	k4yIc1	několik
na	na	k7c4	na
popraviště	popraviště	k1gNnSc4	popraviště
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
také	také	k9	také
zavedl	zavést	k5eAaPmAgInS	zavést
matematickou	matematický	k2eAgFnSc4d1	matematická
definici	definice	k1gFnSc4	definice
nové	nový	k2eAgFnSc2d1	nová
měny	měna	k1gFnSc2	měna
guiney	guinea	k1gFnSc2	guinea
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k1gNnSc1	místo
v	v	k7c6	v
královské	královský	k2eAgFnSc6d1	královská
mincovně	mincovna	k1gFnSc6	mincovna
bylo	být	k5eAaImAgNnS	být
spojeno	spojit	k5eAaPmNgNnS	spojit
s	s	k7c7	s
příjmem	příjem	k1gInSc7	příjem
1500	[number]	k4	1500
liber	libra	k1gFnPc2	libra
sterlinků	sterlink	k1gInPc2	sterlink
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
zlepšily	zlepšit	k5eAaPmAgInP	zlepšit
jeho	jeho	k3xOp3gFnPc4	jeho
hmotné	hmotný	k2eAgFnPc4d1	hmotná
poměry	poměra	k1gFnPc4	poměra
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vzdal	vzdát	k5eAaPmAgMnS	vzdát
profesury	profesura	k1gFnSc2	profesura
v	v	k7c6	v
Cambridgi	Cambridge	k1gFnSc6	Cambridge
<g/>
.	.	kIx.	.
</s>
<s>
Přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
se	se	k3xPyFc4	se
natrvalo	natrvalo	k6eAd1	natrvalo
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
o	o	k7c4	o
jeho	jeho	k3xOp3gInSc4	jeho
byt	byt	k1gInSc4	byt
starala	starat	k5eAaImAgFnS	starat
jeho	jeho	k3xOp3gFnSc1	jeho
neteř	neteř	k1gFnSc1	neteř
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
jeho	jeho	k3xOp3gFnSc2	jeho
sestry	sestra	k1gFnSc2	sestra
Hannah	Hannah	k1gMnSc1	Hannah
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1727	[number]	k4	1727
naposledy	naposledy	k6eAd1	naposledy
předsedal	předsedat	k5eAaImAgMnS	předsedat
Královské	královský	k2eAgFnSc2d1	královská
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byl	být	k5eAaImAgInS	být
již	již	k6eAd1	již
vážně	vážně	k6eAd1	vážně
nemocen	nemocen	k2eAgMnSc1d1	nemocen
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1722	[number]	k4	1722
trpěl	trpět	k5eAaImAgInS	trpět
dnou	dna	k1gFnSc7	dna
a	a	k8xC	a
ledvinovými	ledvinový	k2eAgInPc7d1	ledvinový
i	i	k8xC	i
žlučníkovými	žlučníkový	k2eAgInPc7d1	žlučníkový
kameny	kámen	k1gInPc7	kámen
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
nemoc	nemoc	k1gFnSc1	nemoc
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
cestou	cesta	k1gFnSc7	cesta
z	z	k7c2	z
Kensingtonu	Kensington	k1gInSc2	Kensington
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
zhoršila	zhoršit	k5eAaPmAgFnS	zhoršit
<g/>
.	.	kIx.	.
</s>
<s>
Newton	newton	k1gInSc1	newton
jí	on	k3xPp3gFnSc2	on
podlehl	podlehnout	k5eAaPmAgInS	podlehnout
31	[number]	k4	31
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1727	[number]	k4	1727
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
podle	podle	k7c2	podle
tehdejšího	tehdejší	k2eAgInSc2d1	tehdejší
Juliánského	juliánský	k2eAgInSc2d1	juliánský
kalendáře	kalendář	k1gInSc2	kalendář
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
ve	v	k7c6	v
Westminsterském	Westminsterský	k2eAgNnSc6d1	Westminsterské
opatství	opatství	k1gNnSc6	opatství
<g/>
.	.	kIx.	.
</s>
<s>
Třebaže	třebaže	k8xS	třebaže
byl	být	k5eAaImAgMnS	být
Newton	Newton	k1gMnSc1	Newton
hluboce	hluboko	k6eAd1	hluboko
věřícím	věřící	k2eAgMnSc7d1	věřící
křesťanem	křesťan	k1gMnSc7	křesťan
<g/>
,	,	kIx,	,
stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gNnSc1	jeho
pojetí	pojetí	k1gNnSc1	pojetí
světa	svět	k1gInSc2	svět
základem	základ	k1gInSc7	základ
racionalismu	racionalismus	k1gInSc2	racionalismus
<g/>
,	,	kIx,	,
osvícenství	osvícenství	k1gNnSc2	osvícenství
<g/>
,	,	kIx,	,
a	a	k8xC	a
mechanického	mechanický	k2eAgInSc2d1	mechanický
materialismu	materialismus	k1gInSc2	materialismus
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
začali	začít	k5eAaPmAgMnP	začít
s	s	k7c7	s
publikováním	publikování	k1gNnSc7	publikování
ve	v	k7c6	v
vědeckých	vědecký	k2eAgInPc6d1	vědecký
časopisech	časopis	k1gInPc6	časopis
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Impakt	Impakt	k1gInSc1	Impakt
faktor	faktor	k1gInSc1	faktor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Úspěšně	úspěšně	k6eAd1	úspěšně
vedl	vést	k5eAaImAgMnS	vést
anglickou	anglický	k2eAgFnSc4d1	anglická
Královskou	královský	k2eAgFnSc4d1	královská
společnost	společnost	k1gFnSc4	společnost
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
době	doba	k1gFnSc6	doba
nejprestižnější	prestižní	k2eAgFnSc7d3	nejprestižnější
vědeckou	vědecký	k2eAgFnSc7d1	vědecká
institucí	instituce	k1gFnSc7	instituce
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgInSc7d3	veliký
Newtonovým	Newtonův	k2eAgInSc7d1	Newtonův
počinem	počin	k1gInSc7	počin
je	být	k5eAaImIp3nS	být
založení	založení	k1gNnSc1	založení
exaktní	exaktní	k2eAgFnSc2d1	exaktní
vědy	věda	k1gFnSc2	věda
jako	jako	k8xC	jako
zcela	zcela	k6eAd1	zcela
nového	nový	k2eAgInSc2d1	nový
pohledu	pohled	k1gInSc2	pohled
na	na	k7c4	na
reálný	reálný	k2eAgInSc4d1	reálný
svět	svět	k1gInSc4	svět
<g/>
,	,	kIx,	,
umožňujícího	umožňující	k2eAgMnSc2d1	umožňující
rozvoj	rozvoj	k1gInSc4	rozvoj
(	(	kIx(	(
<g/>
moderní	moderní	k2eAgFnSc2d1	moderní
<g/>
)	)	kIx)	)
matematizované	matematizovaný	k2eAgFnSc2d1	matematizovaná
deduktivní	deduktivní	k2eAgFnSc2d1	deduktivní
vědy	věda	k1gFnSc2	věda
<g/>
..	..	k?	..
K	k	k7c3	k
tomu	ten	k3xDgInSc3	ten
účelu	účel	k1gInSc3	účel
musel	muset	k5eAaImAgMnS	muset
učinit	učinit	k5eAaPmF	učinit
dva	dva	k4xCgInPc4	dva
zásadní	zásadní	k2eAgInPc4d1	zásadní
objevy	objev	k1gInPc4	objev
<g/>
:	:	kIx,	:
Definovat	definovat	k5eAaBmF	definovat
nový	nový	k2eAgInSc4d1	nový
<g/>
,	,	kIx,	,
dosud	dosud	k6eAd1	dosud
neznámý	známý	k2eNgInSc4d1	neznámý
filtr	filtr	k1gInSc4	filtr
poznání	poznání	k1gNnSc2	poznání
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
diskrétní	diskrétní	k2eAgInSc1d1	diskrétní
filtr	filtr	k1gInSc1	filtr
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
filtru	filtr	k1gInSc2	filtr
přirozeného	přirozený	k2eAgNnSc2d1	přirozené
poznání	poznání	k1gNnSc2	poznání
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
je	být	k5eAaImIp3nS	být
vágnost	vágnost	k1gFnSc1	vágnost
<g/>
.	.	kIx.	.
</s>
<s>
Vybudovat	vybudovat	k5eAaPmF	vybudovat
umělý	umělý	k2eAgInSc4d1	umělý
formální	formální	k2eAgInSc4d1	formální
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgInSc6	který
je	být	k5eAaImIp3nS	být
postaveno	postavit	k5eAaPmNgNnS	postavit
Newtonovo	Newtonův	k2eAgNnSc1d1	Newtonovo
umělé	umělý	k2eAgNnSc1d1	umělé
poznání	poznání	k1gNnSc1	poznání
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
potřeboval	potřebovat	k5eAaImAgMnS	potřebovat
pro	pro	k7c4	pro
své	svůj	k3xOyFgInPc4	svůj
zákony	zákon	k1gInPc4	zákon
mechaniky	mechanika	k1gFnSc2	mechanika
jazyk	jazyk	k1gInSc1	jazyk
schopný	schopný	k2eAgInSc1d1	schopný
popisovat	popisovat	k5eAaImF	popisovat
spojitý	spojitý	k2eAgInSc1d1	spojitý
dynamický	dynamický	k2eAgInSc1d1	dynamický
(	(	kIx(	(
<g/>
s	s	k7c7	s
uvažováním	uvažování	k1gNnSc7	uvažování
vlivu	vliv	k1gInSc2	vliv
setrvačnosti	setrvačnost	k1gFnSc2	setrvačnost
<g/>
)	)	kIx)	)
pohyb	pohyb	k1gInSc1	pohyb
(	(	kIx(	(
<g/>
odehrávající	odehrávající	k2eAgMnSc1d1	odehrávající
se	se	k3xPyFc4	se
po	po	k7c6	po
nekonečně	konečně	k6eNd1	konečně
malých	malý	k2eAgInPc6d1	malý
přírůstcích	přírůstek	k1gInPc6	přírůstek
jak	jak	k8xS	jak
dotyčné	dotyčný	k2eAgFnPc4d1	dotyčná
veličiny	veličina	k1gFnPc4	veličina
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
času	čas	k1gInSc2	čas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
teorii	teorie	k1gFnSc4	teorie
fluxí	fluxe	k1gFnPc2	fluxe
(	(	kIx(	(
<g/>
teorie	teorie	k1gFnSc1	teorie
plynoucího	plynoucí	k2eAgNnSc2d1	plynoucí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poněkud	poněkud	k6eAd1	poněkud
nemotorný	motorný	k2eNgMnSc1d1	nemotorný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
funkční	funkční	k2eAgInSc1d1	funkční
integro-diferenciální	integroiferenciální	k2eAgInSc1d1	integro-diferenciální
počet	počet	k1gInSc1	počet
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
bylo	být	k5eAaImAgNnS	být
možno	možno	k9	možno
např.	např.	kA	např.
symbolicky	symbolicky	k6eAd1	symbolicky
zapsat	zapsat	k5eAaPmF	zapsat
<g/>
,	,	kIx,	,
že	že	k8xS	že
rychlost	rychlost	k1gFnSc1	rychlost
pohybu	pohyb	k1gInSc2	pohyb
je	být	k5eAaImIp3nS	být
derivací	derivace	k1gFnSc7	derivace
dráhy	dráha	k1gFnSc2	dráha
podle	podle	k7c2	podle
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
formálně	formálně	k6eAd1	formálně
matematicky	matematicky	k6eAd1	matematicky
odvozovat	odvozovat	k5eAaImF	odvozovat
nové	nový	k2eAgInPc4d1	nový
<g/>
,	,	kIx,	,
hledané	hledaný	k2eAgInPc4d1	hledaný
vztahy	vztah	k1gInPc4	vztah
dynamických	dynamický	k2eAgInPc2d1	dynamický
zákonů	zákon	k1gInPc2	zákon
pohybu	pohyb	k1gInSc2	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
Newton	Newton	k1gMnSc1	Newton
položil	položit	k5eAaPmAgMnS	položit
základy	základ	k1gInPc4	základ
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
moderní	moderní	k2eAgFnPc1d1	moderní
<g/>
,	,	kIx,	,
ucelené	ucelený	k2eAgFnPc1d1	ucelená
<g/>
,	,	kIx,	,
deduktivní	deduktivní	k2eAgFnPc1d1	deduktivní
vědy	věda	k1gFnPc1	věda
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
dnes	dnes	k6eAd1	dnes
nazývané	nazývaný	k2eAgFnPc4d1	nazývaná
klasická	klasický	k2eAgFnSc1d1	klasická
fyzika	fyzika	k1gFnSc1	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mechanice	mechanika	k1gFnSc6	mechanika
především	především	k9	především
dokázal	dokázat	k5eAaPmAgMnS	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
fyzikální	fyzikální	k2eAgInPc1d1	fyzikální
zákony	zákon	k1gInPc1	zákon
platí	platit	k5eAaImIp3nP	platit
nejen	nejen	k6eAd1	nejen
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
ale	ale	k8xC	ale
i	i	k9	i
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Newtonovým	Newtonův	k2eAgInSc7d1	Newtonův
nejznámějším	známý	k2eAgInSc7d3	nejznámější
objevem	objev	k1gInSc7	objev
byly	být	k5eAaImAgInP	být
jeho	jeho	k3xOp3gInPc1	jeho
tři	tři	k4xCgInPc1	tři
pohybové	pohybový	k2eAgInPc1d1	pohybový
zákony	zákon	k1gInPc1	zákon
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
pohybový	pohybový	k2eAgInSc1d1	pohybový
zákon	zákon	k1gInSc1	zákon
-	-	kIx~	-
Zákon	zákon	k1gInSc1	zákon
setrvačnosti	setrvačnost	k1gFnSc2	setrvačnost
2	[number]	k4	2
<g/>
.	.	kIx.	.
pohybový	pohybový	k2eAgInSc1d1	pohybový
zákon	zákon	k1gInSc1	zákon
-	-	kIx~	-
Zákon	zákon	k1gInSc1	zákon
síly	síla	k1gFnSc2	síla
3	[number]	k4	3
<g/>
.	.	kIx.	.
pohybový	pohybový	k2eAgInSc1d1	pohybový
zákon	zákon	k1gInSc1	zákon
-	-	kIx~	-
Zákon	zákon	k1gInSc1	zákon
akce	akce	k1gFnSc2	akce
a	a	k8xC	a
reakce	reakce	k1gFnSc2	reakce
Dále	daleko	k6eAd2	daleko
objevil	objevit	k5eAaPmAgMnS	objevit
zákony	zákon	k1gInPc4	zákon
všeobecné	všeobecný	k2eAgFnSc2d1	všeobecná
gravitace	gravitace	k1gFnSc2	gravitace
(	(	kIx(	(
<g/>
Newtonův	Newtonův	k2eAgInSc1d1	Newtonův
gravitační	gravitační	k2eAgInSc1d1	gravitační
zákon	zákon	k1gInSc1	zákon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Klasická	klasický	k2eAgFnSc1d1	klasická
mechanika	mechanika	k1gFnSc1	mechanika
se	se	k3xPyFc4	se
dodnes	dodnes	k6eAd1	dodnes
opírá	opírat	k5eAaImIp3nS	opírat
o	o	k7c4	o
jím	on	k3xPp3gNnSc7	on
zavedené	zavedený	k2eAgInPc4d1	zavedený
pojmy	pojem	k1gInPc4	pojem
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
,	,	kIx,	,
setrvačnosti	setrvačnost	k1gFnSc2	setrvačnost
<g/>
,	,	kIx,	,
síly	síla	k1gFnSc2	síla
a	a	k8xC	a
interakce	interakce	k1gFnSc2	interakce
<g/>
.	.	kIx.	.
</s>
<s>
Objevil	objevit	k5eAaPmAgMnS	objevit
mnoho	mnoho	k4c4	mnoho
zákonů	zákon	k1gInPc2	zákon
speciální	speciální	k2eAgFnSc2d1	speciální
povahy	povaha	k1gFnSc2	povaha
týkajících	týkající	k2eAgFnPc2d1	týkající
se	se	k3xPyFc4	se
pohybu	pohyb	k1gInSc3	pohyb
planet	planeta	k1gFnPc2	planeta
<g/>
,	,	kIx,	,
pohybu	pohyb	k1gInSc2	pohyb
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
s	s	k7c7	s
odporem	odpor	k1gInSc7	odpor
<g/>
,	,	kIx,	,
rotujících	rotující	k2eAgFnPc2d1	rotující
kapalin	kapalina	k1gFnPc2	kapalina
atd.	atd.	kA	atd.
V	v	k7c6	v
optice	optika	k1gFnSc6	optika
objevil	objevit	k5eAaPmAgMnS	objevit
<g/>
,	,	kIx,	,
že	že	k8xS	že
světlo	světlo	k1gNnSc1	světlo
je	být	k5eAaImIp3nS	být
složené	složený	k2eAgNnSc1d1	složené
a	a	k8xC	a
skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
barevného	barevný	k2eAgNnSc2d1	barevné
spektra	spektrum	k1gNnSc2	spektrum
(	(	kIx(	(
<g/>
rozklad	rozklad	k1gInSc1	rozklad
na	na	k7c6	na
optickém	optický	k2eAgInSc6d1	optický
hranolu	hranol	k1gInSc6	hranol
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vysvětlil	vysvětlit	k5eAaPmAgMnS	vysvětlit
barvy	barva	k1gFnPc4	barva
tenkých	tenký	k2eAgFnPc2d1	tenká
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
,	,	kIx,	,
objevil	objevit	k5eAaPmAgInS	objevit
zobrazovací	zobrazovací	k2eAgFnPc4d1	zobrazovací
rovnici	rovnice	k1gFnSc4	rovnice
<g/>
,	,	kIx,	,
nalezl	nalézt	k5eAaBmAgInS	nalézt
slitinu	slitina	k1gFnSc4	slitina
vhodnou	vhodný	k2eAgFnSc4d1	vhodná
ke	k	k7c3	k
konstrukci	konstrukce	k1gFnSc3	konstrukce
zrcadel	zrcadlo	k1gNnPc2	zrcadlo
a	a	k8xC	a
sestrojil	sestrojit	k5eAaPmAgInS	sestrojit
první	první	k4xOgInSc4	první
zrcadlový	zrcadlový	k2eAgInSc4d1	zrcadlový
dalekohled	dalekohled	k1gInSc4	dalekohled
(	(	kIx(	(
<g/>
vlastní	vlastní	k2eAgNnSc4d1	vlastní
uspořádání	uspořádání	k1gNnSc4	uspořádání
primárního	primární	k2eAgNnSc2d1	primární
a	a	k8xC	a
sekundárního	sekundární	k2eAgNnSc2d1	sekundární
zrcadla	zrcadlo	k1gNnSc2	zrcadlo
<g/>
)	)	kIx)	)
viz	vidět	k5eAaImRp2nS	vidět
Newtonův	Newtonův	k2eAgInSc4d1	Newtonův
dalekohled	dalekohled	k1gInSc4	dalekohled
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
objevil	objevit	k5eAaPmAgInS	objevit
sextant	sextant	k1gInSc1	sextant
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
svůj	svůj	k3xOyFgInSc4	svůj
objev	objev	k1gInSc4	objev
nepublikoval	publikovat	k5eNaBmAgMnS	publikovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
matematice	matematika	k1gFnSc6	matematika
položil	položit	k5eAaPmAgMnS	položit
základy	základ	k1gInPc1	základ
diferenciálního	diferenciální	k2eAgMnSc2d1	diferenciální
a	a	k8xC	a
integrálního	integrální	k2eAgInSc2d1	integrální
počtu	počet	k1gInSc2	počet
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Kalkulus	Kalkulus	k1gInSc1	Kalkulus
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
základy	základ	k1gInPc1	základ
diferenciálních	diferenciální	k2eAgFnPc2d1	diferenciální
rovnic	rovnice	k1gFnPc2	rovnice
<g/>
.	.	kIx.	.
</s>
<s>
Nalezl	naleznout	k5eAaPmAgMnS	naleznout
také	také	k9	také
metodu	metoda	k1gFnSc4	metoda
pro	pro	k7c4	pro
numerické	numerický	k2eAgNnSc4d1	numerické
řešení	řešení	k1gNnSc4	řešení
transcendentních	transcendentní	k2eAgFnPc2d1	transcendentní
rovnic	rovnice	k1gFnPc2	rovnice
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1665	[number]	k4	1665
zobecnil	zobecnit	k5eAaPmAgInS	zobecnit
binomickou	binomický	k2eAgFnSc4d1	binomická
větu	věta	k1gFnSc4	věta
v	v	k7c4	v
binomickou	binomický	k2eAgFnSc4d1	binomická
řadu	řada	k1gFnSc4	řada
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
objevu	objev	k1gInSc6	objev
fluxí	fluxe	k1gFnPc2	fluxe
(	(	kIx(	(
<g/>
jak	jak	k6eAd1	jak
nazýval	nazývat	k5eAaImAgInS	nazývat
svůj	svůj	k3xOyFgInSc4	svůj
objev	objev	k1gInSc4	objev
diferenciálního	diferenciální	k2eAgInSc2d1	diferenciální
počtu	počet	k1gInSc2	počet
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
Newton	Newton	k1gMnSc1	Newton
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
deníku	deník	k1gInSc6	deník
20	[number]	k4	20
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1665	[number]	k4	1665
a	a	k8xC	a
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
pomocí	pomocí	k7c2	pomocí
svého	svůj	k3xOyFgInSc2	svůj
diferenciálního	diferenciální	k2eAgInSc2d1	diferenciální
počtu	počet	k1gInSc2	počet
určil	určit	k5eAaPmAgInS	určit
obsah	obsah	k1gInSc1	obsah
plochy	plocha	k1gFnSc2	plocha
ohraničené	ohraničený	k2eAgFnSc2d1	ohraničená
hyperbolou	hyperbola	k1gFnSc7	hyperbola
<g/>
.	.	kIx.	.
</s>
<s>
Newton	Newton	k1gMnSc1	Newton
také	také	k9	také
věnoval	věnovat	k5eAaImAgMnS	věnovat
mnoho	mnoho	k4c4	mnoho
času	čas	k1gInSc2	čas
alchymii	alchymie	k1gFnSc4	alchymie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Cambridgi	Cambridge	k1gFnSc6	Cambridge
si	se	k3xPyFc3	se
zařídil	zařídit	k5eAaPmAgMnS	zařídit
laboratoř	laboratoř	k1gFnSc4	laboratoř
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
ověřoval	ověřovat	k5eAaImAgMnS	ověřovat
popsané	popsaný	k2eAgInPc4d1	popsaný
postupy	postup	k1gInPc4	postup
a	a	k8xC	a
procedury	procedura	k1gFnPc4	procedura
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Kniha	kniha	k1gFnSc1	kniha
Matematické	matematický	k2eAgFnSc2d1	matematická
principy	princip	k1gInPc4	princip
přírodní	přírodní	k2eAgFnSc1d1	přírodní
filozofie	filozofie	k1gFnSc1	filozofie
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
nejdůležitější	důležitý	k2eAgFnSc4d3	nejdůležitější
práci	práce	k1gFnSc4	práce
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
historii	historie	k1gFnSc6	historie
moderní	moderní	k2eAgFnSc2d1	moderní
vědy	věda	k1gFnSc2	věda
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
popsán	popsat	k5eAaPmNgInS	popsat
například	například	k6eAd1	například
zákon	zákon	k1gInSc1	zákon
všeobecné	všeobecný	k2eAgFnSc2d1	všeobecná
gravitace	gravitace	k1gFnSc2	gravitace
a	a	k8xC	a
Newtonovy	Newtonův	k2eAgInPc1d1	Newtonův
pohybové	pohybový	k2eAgInPc1d1	pohybový
zákony	zákon	k1gInPc1	zákon
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
položily	položit	k5eAaPmAgInP	položit
základy	základ	k1gInPc4	základ
klasické	klasický	k2eAgFnSc2d1	klasická
mechaniky	mechanika	k1gFnSc2	mechanika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
svého	svůj	k3xOyFgInSc2	svůj
výzkumu	výzkum	k1gInSc2	výzkum
také	také	k9	také
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Gottfriedem	Gottfried	k1gMnSc7	Gottfried
Leibnizem	Leibniz	k1gMnSc7	Leibniz
<g/>
;	;	kIx,	;
o	o	k7c6	o
prvenství	prvenství	k1gNnSc6	prvenství
vedli	vést	k5eAaImAgMnP	vést
nesmiřitelný	smiřitelný	k2eNgInSc4d1	nesmiřitelný
spor	spor	k1gInSc4	spor
<g/>
)	)	kIx)	)
základy	základ	k1gInPc1	základ
diferenciálního	diferenciální	k2eAgMnSc2d1	diferenciální
a	a	k8xC	a
integrálního	integrální	k2eAgInSc2d1	integrální
počtu	počet	k1gInSc2	počet
<g/>
.	.	kIx.	.
</s>
<s>
Rukopis	rukopis	k1gInSc1	rukopis
prvního	první	k4xOgInSc2	první
dílu	díl	k1gInSc2	díl
byl	být	k5eAaImAgInS	být
předložen	předložen	k2eAgInSc1d1	předložen
Královské	královský	k2eAgFnSc3d1	královská
společnosti	společnost	k1gFnSc3	společnost
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1686	[number]	k4	1686
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Newton	Newton	k1gMnSc1	Newton
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
svoji	svůj	k3xOyFgFnSc4	svůj
práci	práce	k1gFnSc4	práce
vypilovat	vypilovat	k5eAaPmF	vypilovat
k	k	k7c3	k
dokonalosti	dokonalost	k1gFnSc3	dokonalost
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
s	s	k7c7	s
vydáním	vydání	k1gNnSc7	vydání
otálel	otálet	k5eAaImAgInS	otálet
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
Edmund	Edmund	k1gMnSc1	Edmund
Halley	Halley	k1gMnSc1	Halley
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
jeho	jeho	k3xOp3gFnSc7	jeho
prací	práce	k1gFnSc7	práce
nadšen	nadchnout	k5eAaPmNgMnS	nadchnout
a	a	k8xC	a
tušil	tušit	k5eAaImAgMnS	tušit
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
bude	být	k5eAaImBp3nS	být
tento	tento	k3xDgInSc1	tento
spis	spis	k1gInSc1	spis
znamenat	znamenat	k5eAaImF	znamenat
pro	pro	k7c4	pro
rozvoj	rozvoj	k1gInSc4	rozvoj
fyziky	fyzika	k1gFnSc2	fyzika
a	a	k8xC	a
astronomie	astronomie	k1gFnSc2	astronomie
<g/>
,	,	kIx,	,
vydal	vydat	k5eAaPmAgInS	vydat
Newtonova	Newtonův	k2eAgNnSc2d1	Newtonovo
Principia	principium	k1gNnSc2	principium
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1687	[number]	k4	1687
vlastním	vlastní	k2eAgInSc7d1	vlastní
nákladem	náklad	k1gInSc7	náklad
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
druhému	druhý	k4xOgInSc3	druhý
vydání	vydání	k1gNnPc4	vydání
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1713	[number]	k4	1713
a	a	k8xC	a
ke	k	k7c3	k
třetímu	třetí	k4xOgMnSc3	třetí
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1726	[number]	k4	1726
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1704	[number]	k4	1704
vydal	vydat	k5eAaPmAgInS	vydat
spis	spis	k1gInSc1	spis
Opticks	Opticks	k1gInSc1	Opticks
<g/>
,	,	kIx,	,
or	or	k?	or
a	a	k8xC	a
treatise	treatise	k1gFnSc1	treatise
of	of	k?	of
the	the	k?	the
reflexions	reflexions	k1gInSc1	reflexions
<g/>
,	,	kIx,	,
refractions	refractions	k1gInSc1	refractions
<g/>
,	,	kIx,	,
inflexions	inflexions	k1gInSc1	inflexions
and	and	k?	and
colours	colours	k1gInSc1	colours
of	of	k?	of
light	light	k1gInSc1	light
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
popsal	popsat	k5eAaPmAgMnS	popsat
své	svůj	k3xOyFgInPc4	svůj
optické	optický	k2eAgInPc4d1	optický
objevy	objev	k1gInPc4	objev
<g/>
.	.	kIx.	.
</s>
<s>
Newton	Newton	k1gMnSc1	Newton
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgMnS	zabývat
chronologií	chronologie	k1gFnSc7	chronologie
bible	bible	k1gFnSc2	bible
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
práce	práce	k1gFnSc2	práce
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1728	[number]	k4	1728
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Chronology	chronolog	k1gMnPc4	chronolog
of	of	k?	of
Ancient	Ancient	k1gInSc4	Ancient
Kingdoms	Kingdomsa	k1gFnPc2	Kingdomsa
amended	amended	k1gMnSc1	amended
<g/>
.	.	kIx.	.
</s>
<s>
Method	Method	k1gInSc1	Method
of	of	k?	of
Fluxions	Fluxions	k1gInSc1	Fluxions
(	(	kIx(	(
<g/>
1671	[number]	k4	1671
<g/>
)	)	kIx)	)
Of	Of	k1gFnPc2	Of
Natures	Natures	k1gInSc1	Natures
Obvious	Obvious	k1gMnSc1	Obvious
Laws	Laws	k1gInSc1	Laws
&	&	k?	&
Processes	Processes	k1gInSc1	Processes
in	in	k?	in
Vegetation	Vegetation	k1gInSc1	Vegetation
(	(	kIx(	(
<g/>
nepublikováno	publikován	k2eNgNnSc1d1	nepublikováno
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
1671	[number]	k4	1671
<g/>
-	-	kIx~	-
<g/>
1675	[number]	k4	1675
<g/>
)	)	kIx)	)
De	De	k?	De
Motu	moto	k1gNnSc6	moto
Corporum	Corporum	k1gNnSc4	Corporum
in	in	k?	in
Gyrum	Gyrum	k1gInSc1	Gyrum
(	(	kIx(	(
<g/>
1684	[number]	k4	1684
<g/>
)	)	kIx)	)
Philosophiae	Philosophia	k1gInSc2	Philosophia
Naturalis	Naturalis	k1gInSc1	Naturalis
Principia	principium	k1gNnSc2	principium
Mathematica	Mathematic	k1gInSc2	Mathematic
(	(	kIx(	(
<g/>
1687	[number]	k4	1687
<g/>
)	)	kIx)	)
Opticks	Opticks	k1gInSc1	Opticks
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
or	or	k?	or
a	a	k8xC	a
treatise	treatise	k1gFnSc1	treatise
of	of	k?	of
the	the	k?	the
reflexions	reflexions	k1gInSc1	reflexions
<g/>
,	,	kIx,	,
refractions	refractions	k1gInSc1	refractions
<g/>
,	,	kIx,	,
inflexions	inflexions	k1gInSc1	inflexions
and	and	k?	and
colours	colours	k1gInSc1	colours
of	of	k?	of
light	light	k1gInSc1	light
(	(	kIx(	(
<g/>
1704	[number]	k4	1704
<g/>
)	)	kIx)	)
Reports	Reportsa	k1gFnPc2	Reportsa
as	as	k1gInSc1	as
Master	master	k1gMnSc1	master
of	of	k?	of
the	the	k?	the
Mint	Mint	k1gInSc1	Mint
(	(	kIx(	(
<g/>
1701	[number]	k4	1701
<g/>
-	-	kIx~	-
<g/>
1725	[number]	k4	1725
<g/>
)	)	kIx)	)
Arithmetica	Arithmetic	k2eAgFnSc1d1	Arithmetic
Universalis	Universalis	k1gFnSc1	Universalis
(	(	kIx(	(
<g/>
1707	[number]	k4	1707
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
System	Syst	k1gInSc7	Syst
of	of	k?	of
the	the	k?	the
World	World	k1gInSc1	World
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Optical	Optical	k1gMnSc1	Optical
Lectures	Lectures	k1gMnSc1	Lectures
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Chronology	chronolog	k1gMnPc4	chronolog
of	of	k?	of
Ancient	Ancient	k1gInSc4	Ancient
Kingdoms	Kingdomsa	k1gFnPc2	Kingdomsa
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Amended	Amended	k1gInSc1	Amended
<g/>
)	)	kIx)	)
a	a	k8xC	a
De	De	k?	De
mundi	mundit	k5eAaPmRp2nS	mundit
systemate	systemat	k1gInSc5	systemat
(	(	kIx(	(
<g/>
vydáno	vydat	k5eAaPmNgNnS	vydat
posmrtně	posmrtně	k6eAd1	posmrtně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1728	[number]	k4	1728
<g/>
)	)	kIx)	)
Observations	Observations	k1gInSc4	Observations
on	on	k3xPp3gMnSc1	on
Daniel	Daniel	k1gMnSc1	Daniel
and	and	k?	and
The	The	k1gMnSc1	The
Apocalypse	Apocalypse	k1gFnSc2	Apocalypse
of	of	k?	of
St.	st.	kA	st.
John	John	k1gMnSc1	John
(	(	kIx(	(
<g/>
1733	[number]	k4	1733
<g/>
)	)	kIx)	)
An	An	k1gMnSc1	An
Historical	Historical	k1gMnSc1	Historical
Account	Account	k1gMnSc1	Account
of	of	k?	of
Two	Two	k1gMnSc1	Two
Notable	Notable	k1gMnSc1	Notable
Corruptions	Corruptionsa	k1gFnPc2	Corruptionsa
of	of	k?	of
Scripture	Scriptur	k1gMnSc5	Scriptur
(	(	kIx(	(
<g/>
1754	[number]	k4	1754
<g/>
)	)	kIx)	)
</s>
<s>
Newton	Newton	k1gMnSc1	Newton
se	se	k3xPyFc4	se
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
přel	přít	k5eAaImAgMnS	přít
s	s	k7c7	s
Gottfriedem	Gottfried	k1gMnSc7	Gottfried
Leibnizem	Leibniz	k1gMnSc7	Leibniz
v	v	k7c6	v
otázce	otázka	k1gFnSc6	otázka
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
objevil	objevit	k5eAaPmAgMnS	objevit
kalkulus	kalkulus	k1gMnSc1	kalkulus
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
Newton	newton	k1gInSc4	newton
podle	podle	k7c2	podle
svých	svůj	k3xOyFgNnPc2	svůj
slov	slovo	k1gNnPc2	slovo
začal	začít	k5eAaPmAgMnS	začít
na	na	k7c6	na
jisté	jistý	k2eAgFnSc6d1	jistá
podobě	podoba	k1gFnSc6	podoba
kalkulu	kalkul	k1gInSc2	kalkul
pracovat	pracovat	k5eAaImF	pracovat
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1666	[number]	k4	1666
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
výsledky	výsledek	k1gInPc4	výsledek
publikoval	publikovat	k5eAaBmAgMnS	publikovat
až	až	k9	až
o	o	k7c4	o
desítky	desítka	k1gFnPc4	desítka
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
navíc	navíc	k6eAd1	navíc
jen	jen	k9	jen
jako	jako	k9	jako
drobnou	drobný	k2eAgFnSc4d1	drobná
poznámku	poznámka	k1gFnSc4	poznámka
na	na	k7c6	na
konci	konec	k1gInSc6	konec
jedné	jeden	k4xCgFnSc2	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
publikací	publikace	k1gFnPc2	publikace
<g/>
.	.	kIx.	.
</s>
<s>
Leibniz	Leibniz	k1gMnSc1	Leibniz
zahájil	zahájit	k5eAaPmAgMnS	zahájit
práce	práce	k1gFnPc4	práce
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
podobě	podoba	k1gFnSc6	podoba
kalkulu	kalkul	k1gInSc2	kalkul
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1674	[number]	k4	1674
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1684	[number]	k4	1684
vydal	vydat	k5eAaPmAgMnS	vydat
první	první	k4xOgNnSc4	první
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jej	on	k3xPp3gMnSc4	on
popsal	popsat	k5eAaPmAgMnS	popsat
<g/>
.	.	kIx.	.
</s>
<s>
Britská	britský	k2eAgFnSc1d1	britská
Královská	královský	k2eAgFnSc1d1	královská
společnost	společnost	k1gFnSc1	společnost
později	pozdě	k6eAd2	pozdě
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
studii	studie	k1gFnSc6	studie
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
skutečným	skutečný	k2eAgMnSc7d1	skutečný
objevitelem	objevitel	k1gMnSc7	objevitel
kalkulu	kalkul	k1gInSc2	kalkul
je	být	k5eAaImIp3nS	být
Newton	newton	k1gInSc1	newton
<g/>
,	,	kIx,	,
a	a	k8xC	a
označila	označit	k5eAaPmAgFnS	označit
Leibnize	Leibnize	k1gFnSc1	Leibnize
za	za	k7c4	za
plagiátora	plagiátor	k1gMnSc4	plagiátor
<g/>
.	.	kIx.	.
</s>
<s>
Studie	studie	k1gFnSc1	studie
však	však	k9	však
byla	být	k5eAaImAgFnS	být
nakonec	nakonec	k6eAd1	nakonec
zpochybněna	zpochybnit	k5eAaPmNgFnS	zpochybnit
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
zjistilo	zjistit	k5eAaPmAgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pasáže	pasáž	k1gFnPc1	pasáž
odsuzující	odsuzující	k2eAgFnSc2d1	odsuzující
Leibnize	Leibnize	k1gFnSc2	Leibnize
napsal	napsat	k5eAaPmAgMnS	napsat
sám	sám	k3xTgMnSc1	sám
Newton	Newton	k1gMnSc1	Newton
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
rozhořel	rozhořet	k5eAaPmAgInS	rozhořet
hořký	hořký	k2eAgInSc1d1	hořký
spor	spor	k1gInSc1	spor
o	o	k7c4	o
kalkulus	kalkulus	k1gInSc4	kalkulus
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
komplikoval	komplikovat	k5eAaBmAgInS	komplikovat
životy	život	k1gInPc4	život
obou	dva	k4xCgMnPc2	dva
filosofů	filosof	k1gMnPc2	filosof
až	až	k9	až
do	do	k7c2	do
Leibnizovy	Leibnizův	k2eAgFnSc2d1	Leibnizova
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1716	[number]	k4	1716
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
dnešních	dnešní	k2eAgMnPc2d1	dnešní
historiků	historik	k1gMnPc2	historik
věří	věřit	k5eAaImIp3nS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Newton	newton	k1gInSc4	newton
a	a	k8xC	a
Leibniz	Leibniz	k1gInSc4	Leibniz
objevili	objevit	k5eAaPmAgMnP	objevit
kalkulus	kalkulus	k1gInSc4	kalkulus
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
sobě	sebe	k3xPyFc6	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Newton	Newton	k1gMnSc1	Newton
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
sporu	spor	k1gInSc2	spor
i	i	k9	i
s	s	k7c7	s
Robertem	Robert	k1gMnSc7	Robert
Hookem	Hooek	k1gMnSc7	Hooek
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
Hooke	Hooke	k1gInSc1	Hooke
kritizoval	kritizovat	k5eAaImAgInS	kritizovat
Newtonovy	Newtonův	k2eAgFnPc4d1	Newtonova
práce	práce	k1gFnPc4	práce
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
optiky	optika	k1gFnSc2	optika
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
Newtona	Newton	k1gMnSc4	Newton
natolik	natolik	k6eAd1	natolik
urazilo	urazit	k5eAaPmAgNnS	urazit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
na	na	k7c4	na
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
stáhl	stáhnout	k5eAaPmAgInS	stáhnout
z	z	k7c2	z
veřejné	veřejný	k2eAgFnSc2d1	veřejná
debaty	debata	k1gFnSc2	debata
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1679	[number]	k4	1679
až	až	k9	až
1680	[number]	k4	1680
pak	pak	k6eAd1	pak
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
zajímavé	zajímavý	k2eAgFnSc3d1	zajímavá
výměně	výměna	k1gFnSc3	výměna
korespondence	korespondence	k1gFnSc2	korespondence
<g/>
.	.	kIx.	.
</s>
<s>
Hooke	Hooke	k1gInSc1	Hooke
byl	být	k5eAaImAgInS	být
tehdy	tehdy	k6eAd1	tehdy
Královskou	královský	k2eAgFnSc7d1	královská
společností	společnost	k1gFnSc7	společnost
pověřen	pověřit	k5eAaPmNgMnS	pověřit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
staral	starat	k5eAaImAgMnS	starat
o	o	k7c6	o
korespondenci	korespondence	k1gFnSc6	korespondence
<g/>
,	,	kIx,	,
tázal	tázat	k5eAaImAgMnS	tázat
se	se	k3xPyFc4	se
ostatních	ostatní	k2eAgInPc2d1	ostatní
členů	člen	k1gInPc2	člen
<g/>
,	,	kIx,	,
na	na	k7c6	na
čem	co	k3yInSc6	co
pracují	pracovat	k5eAaImIp3nP	pracovat
a	a	k8xC	a
žádal	žádat	k5eAaImAgMnS	žádat
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
komentáře	komentář	k1gInPc4	komentář
k	k	k7c3	k
práci	práce	k1gFnSc3	práce
ostatních	ostatní	k2eAgFnPc2d1	ostatní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dopisech	dopis	k1gInPc6	dopis
se	se	k3xPyFc4	se
Newtona	Newton	k1gMnSc2	Newton
ptal	ptat	k5eAaImAgMnS	ptat
na	na	k7c4	na
jeho	on	k3xPp3gInSc4	on
názor	názor	k1gInSc4	názor
na	na	k7c4	na
řadu	řada	k1gFnSc4	řada
myšlenek	myšlenka	k1gFnPc2	myšlenka
jak	jak	k8xS	jak
vlastních	vlastní	k2eAgInPc2d1	vlastní
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
jiných	jiný	k2eAgMnPc2d1	jiný
autorů	autor	k1gMnPc2	autor
<g/>
,	,	kIx,	,
a	a	k8xC	a
tato	tento	k3xDgFnSc1	tento
korespondenční	korespondenční	k2eAgFnSc1d1	korespondenční
výměna	výměna	k1gFnSc1	výměna
nakonec	nakonec	k6eAd1	nakonec
Newtona	Newton	k1gMnSc2	Newton
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
formulaci	formulace	k1gFnSc3	formulace
zákona	zákon	k1gInSc2	zákon
všeobecné	všeobecný	k2eAgFnSc2d1	všeobecná
gravitace	gravitace	k1gFnSc2	gravitace
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
pak	pak	k6eAd1	pak
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1686	[number]	k4	1686
prezentoval	prezentovat	k5eAaBmAgMnS	prezentovat
Newton	Newton	k1gMnSc1	Newton
Královské	královský	k2eAgFnSc2d1	královská
společnosti	společnost	k1gFnSc2	společnost
první	první	k4xOgFnSc2	první
knihu	kniha	k1gFnSc4	kniha
svého	svůj	k3xOyFgNnSc2	svůj
díla	dílo	k1gNnSc2	dílo
Principia	principium	k1gNnSc2	principium
Mathematica	Mathematicum	k1gNnSc2	Mathematicum
<g/>
,	,	kIx,	,
Hooke	Hooke	k1gInSc4	Hooke
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
a	a	k8xC	a
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
některé	některý	k3yIgFnPc4	některý
myšlenky	myšlenka	k1gFnPc4	myšlenka
převzal	převzít	k5eAaPmAgMnS	převzít
Newton	Newton	k1gMnSc1	Newton
od	od	k7c2	od
něj	on	k3xPp3gInSc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
filosofové	filosof	k1gMnPc1	filosof
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
až	až	k9	až
do	do	k7c2	do
Hookovy	Hookův	k2eAgFnSc2d1	Hookův
smrti	smrt	k1gFnSc2	smrt
neměli	mít	k5eNaImAgMnP	mít
příliš	příliš	k6eAd1	příliš
v	v	k7c6	v
lásce	láska	k1gFnSc6	láska
<g/>
.	.	kIx.	.
</s>
