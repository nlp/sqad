<s>
Alexandr	Alexandr	k1gMnSc1
Litviněnko	Litviněnka	k1gFnSc5
</s>
<s>
Alexandr	Alexandr	k1gMnSc1
Valtěrovič	Valtěrovič	k1gMnSc1
Litviněnko	Litviněnka	k1gFnSc5
Narození	narození	k1gNnSc3
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1962	#num#	k4
nebo	nebo	k8xC
30	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1962	#num#	k4
<g/>
Voroněž	Voroněž	k1gFnSc1
Úmrtí	úmrtí	k1gNnSc2
</s>
<s>
23	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2006	#num#	k4
<g/>
University	universita	k1gFnSc2
College	Colleg	k1gFnSc2
Hospital	Hospital	k1gMnSc1
Příčina	příčina	k1gFnSc1
úmrtí	úmrtí	k1gNnPc2
</s>
<s>
radiační	radiační	k2eAgInSc1d1
syndrom	syndrom	k1gInSc1
Místo	místo	k7c2
pohřbení	pohřbení	k1gNnSc2
</s>
<s>
hřbitov	hřbitov	k1gInSc1
Highgate	Highgat	k1gInSc5
Alma	alma	k1gFnSc1
mater	mater	k1gFnPc5
</s>
<s>
Ordžonikidzské	ordžonikidzský	k2eAgNnSc1d1
vyšší	vysoký	k2eAgNnSc1d2
vševojskové	vševojskový	k2eAgNnSc1d1
velitelské	velitelský	k2eAgNnSc1d1
učiliště	učiliště	k1gNnSc1
(	(	kIx(
<g/>
do	do	k7c2
1985	#num#	k4
<g/>
)	)	kIx)
Povolání	povolání	k1gNnSc2
</s>
<s>
spisovatel	spisovatel	k1gMnSc1
<g/>
,	,	kIx,
novinář	novinář	k1gMnSc1
<g/>
,	,	kIx,
voják	voják	k1gMnSc1
a	a	k8xC
špión	špión	k1gMnSc1
Nábož	Nábož	k1gFnSc1
<g/>
.	.	kIx.
vyznání	vyznání	k1gNnSc1
</s>
<s>
islám	islám	k1gInSc1
Manžel	manžel	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ka	ka	k?
<g/>
)	)	kIx)
</s>
<s>
Marina	Marina	k1gFnSc1
Litviněnková	Litviněnková	k1gFnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybět	k5eAaImIp3nS,k5eAaPmIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Alexandr	Alexandr	k1gMnSc1
Valterovič	Valterovič	k1gMnSc1
Litviněnko	Litviněnko	k1gMnSc1xF
<g/>
,	,	kIx,
rusky	rusky	k6eAd1
А	А	k?
В	В	k?
Л	Л	k?
<g/>
,	,	kIx,
(	(	kIx(
<g/>
4	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1962	#num#	k4
<g/>
,	,	kIx,
Voroněž	Voroněž	k1gFnSc1
<g/>
,	,	kIx,
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
–	–	kIx~
23	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2006	#num#	k4
<g/>
,	,	kIx,
Londýn	Londýn	k1gInSc1
<g/>
,	,	kIx,
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
agent	agent	k1gMnSc1
KGB	KGB	kA
a	a	k8xC
FSB	FSB	kA
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
zběhl	zběhnout	k5eAaPmAgInS
do	do	k7c2
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
a	a	k8xC
začal	začít	k5eAaPmAgInS
pracovat	pracovat	k5eAaImF
pro	pro	k7c4
britskou	britský	k2eAgFnSc4d1
MI	já	kA
<g/>
6	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Litviněnko	Litviněnka	k1gFnSc5
začal	začít	k5eAaPmAgInS
sympatizovat	sympatizovat	k5eAaImF
s	s	k7c7
čečenskými	čečenský	k2eAgMnPc7d1
džihádisty	džihádista	k1gMnPc7
a	a	k8xC
přijal	přijmout	k5eAaPmAgMnS
jejich	jejich	k3xOp3gFnSc4
formu	forma	k1gFnSc4
islámu	islám	k1gInSc2
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
ostrým	ostrý	k2eAgMnSc7d1
kritikem	kritik	k1gMnSc7
prezidenta	prezident	k1gMnSc2
Putina	putin	k2eAgFnSc1d1
<g/>
,	,	kIx,
především	především	k9
kvůli	kvůli	k7c3
jeho	jeho	k3xOp3gFnSc3
politice	politika	k1gFnSc3
v	v	k7c6
Čečensku	Čečensko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
emigroval	emigrovat	k5eAaBmAgMnS
do	do	k7c2
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
získal	získat	k5eAaPmAgMnS
azyl	azyl	k1gInSc4
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
také	také	k6eAd1
britské	britský	k2eAgNnSc4d1
občanství	občanství	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
začátku	začátek	k1gInSc6
listopadu	listopad	k1gInSc2
2006	#num#	k4
byl	být	k5eAaImAgInS
otráven	otráven	k2eAgInSc1d1
radioaktivním	radioaktivní	k2eAgNnSc7d1
poloniem	polonium	k1gNnSc7
a	a	k8xC
23	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
na	na	k7c4
následky	následek	k1gInPc4
otravy	otrava	k1gFnSc2
zemřel	zemřít	k5eAaPmAgInS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1988	#num#	k4
začal	začít	k5eAaPmAgMnS
pracovat	pracovat	k5eAaImF
v	v	k7c6
kontrarozvědce	kontrarozvědka	k1gFnSc6
KGB	KGB	kA
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
1991	#num#	k4
sloužil	sloužit	k5eAaImAgMnS
v	v	k7c6
centrále	centrála	k1gFnSc6
FSB	FSB	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
účinnou	účinný	k2eAgFnSc4d1
činnost	činnost	k1gFnSc4
při	při	k7c6
boji	boj	k1gInSc6
s	s	k7c7
organizovaným	organizovaný	k2eAgInSc7d1
zločinem	zločin	k1gInSc7
byl	být	k5eAaImAgInS
vyznamenán	vyznamenat	k5eAaPmNgInS
titulem	titul	k1gInSc7
veterána	veterán	k1gMnSc2
MUR	mura	k1gFnPc2
(	(	kIx(
<g/>
Kriminalistické	kriminalistický	k2eAgNnSc1d1
oddělení	oddělení	k1gNnSc1
moskevské	moskevský	k2eAgFnSc2d1
milice	milice	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1997	#num#	k4
byl	být	k5eAaImAgInS
přeřazen	přeřazen	k2eAgInSc1d1
do	do	k7c2
přísně	přísně	k6eAd1
tajné	tajný	k2eAgFnSc2d1
správy	správa	k1gFnSc2
pro	pro	k7c4
rozpracovávání	rozpracovávání	k1gNnSc4
zločineckých	zločinecký	k2eAgFnPc2d1
organizací	organizace	k1gFnPc2
FSB	FSB	kA
na	na	k7c6
funkci	funkce	k1gFnSc6
staršího	starý	k2eAgMnSc2d2
operativního	operativní	k2eAgMnSc2d1
spolupracovníka	spolupracovník	k1gMnSc2
a	a	k8xC
zástupce	zástupce	k1gMnSc2
náčelníka	náčelník	k1gMnSc2
7	#num#	k4
<g/>
.	.	kIx.
oddělení	oddělení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
tiskové	tiskový	k2eAgFnSc6d1
konferenci	konference	k1gFnSc6
v	v	k7c6
listopadu	listopad	k1gInSc6
1998	#num#	k4
zveřejnil	zveřejnit	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
dostával	dostávat	k5eAaImAgMnS
rozkazy	rozkaz	k1gInPc4
v	v	k7c6
rozporu	rozpor	k1gInSc6
se	s	k7c7
zákony	zákon	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
březnu	březen	k1gInSc6
1999	#num#	k4
byl	být	k5eAaImAgMnS
zatčen	zatknout	k5eAaPmNgMnS
a	a	k8xC
umístěn	umístit	k5eAaPmNgMnS
ve	v	k7c6
vazební	vazební	k2eAgFnSc6d1
věznici	věznice	k1gFnSc6
Lefortovo	Lefortův	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soudní	soudní	k2eAgInSc4d1
jednání	jednání	k1gNnSc1
skončilo	skončit	k5eAaPmAgNnS
osvobozením	osvobození	k1gNnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
hned	hned	k6eAd1
poté	poté	k6eAd1
byl	být	k5eAaImAgMnS
v	v	k7c6
budově	budova	k1gFnSc6
soudu	soud	k1gInSc2
zatčen	zatčen	k2eAgInSc1d1
FSB	FSB	kA
s	s	k7c7
jiným	jiný	k2eAgNnSc7d1
obviněním	obvinění	k1gNnSc7
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
bylo	být	k5eAaImAgNnS
nakonec	nakonec	k6eAd1
uzavřeno	uzavřít	k5eAaPmNgNnS
<g/>
,	,	kIx,
aniž	aniž	k8xC,k8xS
by	by	kYmCp3nS
bylo	být	k5eAaImAgNnS
zahájeno	zahájit	k5eAaPmNgNnS
soudní	soudní	k2eAgNnSc1d1
řízení	řízení	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
propuštění	propuštění	k1gNnSc6
z	z	k7c2
druhé	druhý	k4xOgFnSc2
vazby	vazba	k1gFnSc2
a	a	k8xC
po	po	k7c6
písemném	písemný	k2eAgInSc6d1
závazku	závazek	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
neopustí	opustit	k5eNaPmIp3nS
zemi	zem	k1gFnSc4
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
proti	proti	k7c3
němu	on	k3xPp3gMnSc3
zahájeno	zahájit	k5eAaPmNgNnS
další	další	k2eAgNnSc1d1
šetření	šetření	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přes	přes	k7c4
závazek	závazek	k1gInSc4
brzy	brzy	k6eAd1
s	s	k7c7
pomocí	pomoc	k1gFnSc7
předsedy	předseda	k1gMnSc2
Nadace	nadace	k1gFnSc2
občanských	občanský	k2eAgFnPc2d1
svobod	svoboda	k1gFnPc2
Alexandra	Alexandr	k1gMnSc2
Goldfarba	Goldfarb	k1gMnSc2
odjel	odjet	k5eAaPmAgMnS
z	z	k7c2
Ruska	Rusko	k1gNnSc2
a	a	k8xC
v	v	k7c6
květnu	květen	k1gInSc6
2001	#num#	k4
obdržel	obdržet	k5eAaPmAgInS
politický	politický	k2eAgInSc1d1
azyl	azyl	k1gInSc1
ve	v	k7c6
Velké	velký	k2eAgFnSc6d1
Británii	Británie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
knize	kniha	k1gFnSc6
FSB	FSB	kA
vyhazuje	vyhazovat	k5eAaImIp3nS
Rusko	Rusko	k1gNnSc1
do	do	k7c2
povětří	povětří	k1gNnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
FSB	FSB	kA
blows	blowsa	k1gFnPc2
up	up	k?
Russia	Russius	k1gMnSc2
<g/>
,	,	kIx,
rusky	rusky	k6eAd1
Ф	Ф	k?
в	в	k?
Р	Р	k?
<g/>
)	)	kIx)
napsané	napsaný	k2eAgFnPc1d1
spolu	spolu	k6eAd1
s	s	k7c7
Jurijem	Jurij	k1gMnSc7
Felštinským	Felštinský	k2eAgMnSc7d1
uvedl	uvést	k5eAaPmAgMnS
verzi	verze	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
teroristické	teroristický	k2eAgInPc1d1
atentáty	atentát	k1gInPc1
v	v	k7c6
roce	rok	k1gInSc6
1999	#num#	k4
na	na	k7c4
obytné	obytný	k2eAgInPc4d1
domy	dům	k1gInPc4
v	v	k7c6
Moskvě	Moskva	k1gFnSc6
a	a	k8xC
Volgodoňsku	Volgodoňsko	k1gNnSc6
byly	být	k5eAaImAgInP
připraveny	připravit	k5eAaPmNgInP
a	a	k8xC
provedeny	provést	k5eAaPmNgInP
agenty	agent	k1gMnPc4
FSB	FSB	kA
s	s	k7c7
úmyslem	úmysl	k1gInSc7
svést	svést	k5eAaPmF
odpovědnost	odpovědnost	k1gFnSc4
na	na	k7c4
Čečeny	Čečena	k1gFnPc4
a	a	k8xC
získat	získat	k5eAaPmF
tak	tak	k6eAd1
záminku	záminka	k1gFnSc4
k	k	k7c3
obnovení	obnovení	k1gNnSc3
čečenské	čečenský	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Litviněnko	Litviněnka	k1gFnSc5
také	také	k9
tvrdil	tvrdit	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
Vladimir	Vladimir	k1gMnSc1
Putin	putin	k2eAgMnSc1d1
stál	stát	k5eAaImAgMnS
za	za	k7c7
vraždou	vražda	k1gFnSc7
novinářky	novinářka	k1gFnSc2
Anny	Anna	k1gFnSc2
Politkovské	Politkovský	k2eAgFnSc2d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Otrava	otrava	k1gFnSc1
a	a	k8xC
smrt	smrt	k1gFnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Otrava	otrava	k1gFnSc1
Alexandra	Alexandra	k1gFnSc1
Litviněnka	Litviněnka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Litviněnkův	Litviněnkův	k2eAgInSc1d1
hrob	hrob	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2006	#num#	k4
Litviněnko	Litviněnka	k1gFnSc5
náhle	náhle	k6eAd1
onemocněl	onemocnět	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinářům	novinář	k1gMnPc3
řekl	říct	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
ten	ten	k3xDgInSc1
den	den	k1gInSc1
setkal	setkat	k5eAaPmAgInS
se	s	k7c7
dvěma	dva	k4xCgInPc7
bývalými	bývalý	k2eAgInPc7d1
agenty	agens	k1gInPc7
KGB	KGB	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeden	jeden	k4xCgMnSc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
byl	být	k5eAaImAgMnS
Dmitrij	Dmitrij	k1gFnSc7
Kovtun	Kovtuna	k1gFnPc2
a	a	k8xC
druhým	druhý	k4xOgNnSc7
Andrej	Andrej	k1gMnSc1
Lugovoj	Lugovoj	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
býval	bývat	k5eAaImAgMnS
bodyguardem	bodyguard	k1gMnSc7
ruského	ruský	k2eAgMnSc2d1
expremiéra	expremiér	k1gMnSc2
Jegora	Jegor	k1gMnSc2
Gajdara	Gajdar	k1gMnSc2
(	(	kIx(
<g/>
i	i	k9
ten	ten	k3xDgMnSc1
byl	být	k5eAaImAgMnS
v	v	k7c6
listopadu	listopad	k1gInSc6
2006	#num#	k4
otráven	otrávit	k5eAaPmNgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
Později	pozdě	k6eAd2
se	se	k3xPyFc4
Litviněnko	Litviněnka	k1gFnSc5
setkal	setkat	k5eAaPmAgMnS
též	též	k9
s	s	k7c7
italským	italský	k2eAgMnSc7d1
akademikem	akademik	k1gMnSc7
Mariem	Mario	k1gMnSc7
Scaramellou	Scaramella	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
mu	on	k3xPp3gMnSc3
předal	předat	k5eAaPmAgMnS
informace	informace	k1gFnPc4
o	o	k7c6
smrti	smrt	k1gFnSc6
Anny	Anna	k1gFnSc2
Politkovské	Politkovský	k2eAgFnSc2d1
<g/>
,	,	kIx,
ruské	ruský	k2eAgFnSc2d1
novinářky	novinářka	k1gFnSc2
<g/>
,	,	kIx,
zastřelené	zastřelený	k2eAgFnSc2d1
v	v	k7c6
říjnu	říjen	k1gInSc6
2006	#num#	k4
v	v	k7c6
Moskvě	Moskva	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Scaramella	Scaramella	k1gMnSc1
tvrdil	tvrdit	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
s	s	k7c7
Litviněnkem	Litviněnko	k1gNnSc7
setkal	setkat	k5eAaPmAgMnS
proto	proto	k8xC
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
ho	on	k3xPp3gMnSc4
varoval	varovat	k5eAaImAgMnS
před	před	k7c7
ruskou	ruský	k2eAgFnSc7d1
rozvědkou	rozvědka	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
oba	dva	k4xCgInPc4
chtěla	chtít	k5eAaImAgFnS
údajně	údajně	k6eAd1
zlikvidovat	zlikvidovat	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Scarammela	Scarammela	k1gFnSc1
byl	být	k5eAaImAgInS
v	v	k7c6
prosinci	prosinec	k1gInSc6
televizí	televize	k1gFnSc7
Sky	Sky	k1gFnSc2
News	News	k1gInSc1
označen	označit	k5eAaPmNgInS
za	za	k7c4
další	další	k2eAgFnSc4d1
oběť	oběť	k1gFnSc4
otravy	otrava	k1gFnSc2
poloniem	polonium	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dne	den	k1gInSc2
23	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2006	#num#	k4
Litviněnko	Litviněnka	k1gFnSc5
na	na	k7c4
následky	následek	k1gInPc4
otravy	otrava	k1gFnSc2
zemřel	zemřít	k5eAaPmAgInS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
svém	svůj	k3xOyFgNnSc6
posledním	poslední	k2eAgNnSc6d1
prohlášení	prohlášení	k1gNnSc6
obvinil	obvinit	k5eAaPmAgMnS
ze	z	k7c2
své	svůj	k3xOyFgFnSc2
smrti	smrt	k1gFnSc2
ruského	ruský	k2eAgMnSc2d1
prezidenta	prezident	k1gMnSc2
Putina	putin	k2eAgMnSc2d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Krátce	krátce	k6eAd1
před	před	k7c7
svou	svůj	k3xOyFgFnSc7
smrtí	smrt	k1gFnSc7
konvertoval	konvertovat	k5eAaBmAgInS
k	k	k7c3
islámu	islám	k1gInSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dne	den	k1gInSc2
22	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2007	#num#	k4
vyšla	vyjít	k5eAaPmAgFnS
zpráva	zpráva	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
britský	britský	k2eAgInSc1d1
vyšetřovací	vyšetřovací	k2eAgInSc1d1
tým	tým	k1gInSc1
obvinil	obvinit	k5eAaPmAgInS
z	z	k7c2
vraždy	vražda	k1gFnSc2
ruského	ruský	k2eAgMnSc2d1
obchodníka	obchodník	k1gMnSc2
Andreje	Andrej	k1gMnSc2
Lugového	Lugový	k2eAgMnSc2d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Veřejné	veřejný	k2eAgNnSc1d1
soudní	soudní	k2eAgNnSc1d1
vyšetřování	vyšetřování	k1gNnSc1
</s>
<s>
V	v	k7c6
lednu	leden	k1gInSc6
2015	#num#	k4
začalo	začít	k5eAaPmAgNnS
v	v	k7c6
Británii	Británie	k1gFnSc6
veřejné	veřejný	k2eAgNnSc1d1
soudní	soudní	k2eAgNnSc1d1
vyšetřování	vyšetřování	k1gNnSc1
Litviněnkovy	Litviněnkův	k2eAgFnSc2d1
smrti	smrt	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
červenci	červenec	k1gInSc6
bylo	být	k5eAaImAgNnS
veřejné	veřejný	k2eAgNnSc1d1
vyšetřování	vyšetřování	k1gNnSc1
ukončeno	ukončit	k5eAaPmNgNnS
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
jeho	jeho	k3xOp3gInPc1
závěry	závěr	k1gInPc1
mají	mít	k5eAaImIp3nP
být	být	k5eAaImF
kompletně	kompletně	k6eAd1
zveřejněny	zveřejnit	k5eAaPmNgInP
koncem	koncem	k7c2
roku	rok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
právníků	právník	k1gMnPc2
není	být	k5eNaImIp3nS
pochyb	pochyba	k1gFnPc2
o	o	k7c6
přímém	přímý	k2eAgNnSc6d1
zapojení	zapojení	k1gNnSc6
Ruska	Rusko	k1gNnSc2
do	do	k7c2
vraždy	vražda	k1gFnSc2
Alexandra	Alexandra	k1gFnSc1
Litviněnka	Litviněnka	k1gFnSc1
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
lednu	leden	k1gInSc6
2016	#num#	k4
soudce	soudce	k1gMnSc1
Robert	Robert	k1gMnSc1
Owen	Owen	k1gMnSc1
dospěl	dochvít	k5eAaPmAgMnS
k	k	k7c3
závěru	závěr	k1gInSc3
<g/>
,	,	kIx,
že	že	k8xS
operaci	operace	k1gFnSc4
FSB	FSB	kA
s	s	k7c7
cílem	cíl	k1gInSc7
Litviněnka	Litviněnka	k1gFnSc1
zabít	zabít	k5eAaPmF
pravděpodobně	pravděpodobně	k6eAd1
posvětil	posvětit	k5eAaPmAgMnS
její	její	k3xOp3gMnSc1
tehdejší	tehdejší	k2eAgMnSc1d1
šéf	šéf	k1gMnSc1
Nikolaj	Nikolaj	k1gMnSc1
Patrušev	Patrušev	k1gMnSc1
a	a	k8xC
nejspíš	nejspíš	k9
ji	on	k3xPp3gFnSc4
odsouhlasil	odsouhlasit	k5eAaPmAgMnS
také	také	k9
prezident	prezident	k1gMnSc1
Vladimir	Vladimir	k1gMnSc1
Putin	putin	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
pachatele	pachatel	k1gMnSc4
vraždy	vražda	k1gFnSc2
označil	označit	k5eAaPmAgInS
soud	soud	k1gInSc1
Andreje	Andrej	k1gMnSc2
Lugového	Lugový	k2eAgMnSc2d1
a	a	k8xC
Dmitrije	Dmitrije	k1gMnSc2
Kovtuna	Kovtuna	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dokumentární	dokumentární	k2eAgInSc1d1
film	film	k1gInSc1
k	k	k7c3
případu	případ	k1gInSc3
Litviněnko	Litviněnka	k1gFnSc5
</s>
<s>
Dokumentární	dokumentární	k2eAgInSc1d1
film	film	k1gInSc1
„	„	k?
<g/>
Kauza	kauza	k1gFnSc1
Litviněnko	Litviněnka	k1gFnSc5
<g/>
“	“	k?
(	(	kIx(
<g/>
Rebellion	Rebellion	k?
<g/>
:	:	kIx,
The	The	k1gMnSc1
Litvinenko	Litvinenka	k1gFnSc5
Case	Case	k1gNnPc7
<g/>
)	)	kIx)
natočil	natočit	k5eAaBmAgMnS
filmový	filmový	k2eAgMnSc1d1
režisér	režisér	k1gMnSc1
Andrej	Andrej	k1gMnSc1
Někrasov	Někrasov	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
doprovázel	doprovázet	k5eAaImAgMnS
Litviněnka	Litviněnka	k1gFnSc1
v	v	k7c6
obou	dva	k4xCgNnPc6
posledních	poslední	k2eAgNnPc6d1
letech	léto	k1gNnPc6
před	před	k7c7
jeho	jeho	k3xOp3gFnSc7
smrtí	smrt	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Film	film	k1gInSc1
byl	být	k5eAaImAgInS
uveden	uvést	k5eAaPmNgInS
26	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2007	#num#	k4
na	na	k7c6
filmovém	filmový	k2eAgInSc6d1
festivalu	festival	k1gInSc6
v	v	k7c6
Cannes	Cannes	k1gNnSc6
<g/>
,	,	kIx,
do	do	k7c2
českých	český	k2eAgNnPc2d1
kin	kino	k1gNnPc2
vstoupil	vstoupit	k5eAaPmAgInS
25	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Dílo	dílo	k1gNnSc1
</s>
<s>
Aleksander	Aleksander	k1gMnSc1
Litviněnko	Litviněnka	k1gFnSc5
<g/>
,	,	kIx,
Jurij	Jurij	k1gMnSc1
Felštinskij	Felštinskij	k1gMnSc1
<g/>
,	,	kIx,
FSB	FSB	kA
blows	blows	k1gInSc1
up	up	k?
Russia	Russia	k1gFnSc1
<g/>
,	,	kIx,
Liberty	Libert	k1gInPc1
Publishing	Publishing	k1gInSc1
House	house	k1gNnSc4
<g/>
,	,	kIx,
New	New	k1gFnSc1
York	York	k1gInSc1
2002	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
0-914481-63-0	0-914481-63-0	k4
</s>
<s>
Teror	teror	k1gInSc1
v	v	k7c6
Rusku	Rusko	k1gNnSc6
</s>
<s>
Rusko	Rusko	k1gNnSc1
a	a	k8xC
teror	teror	k1gInSc1
</s>
<s>
Překlady	překlad	k1gInPc1
<g/>
:	:	kIx,
</s>
<s>
Závan	závan	k1gInSc1
nad	nad	k7c7
Ruskem	Rusko	k1gNnSc7
<g/>
:	:	kIx,
Teror	teror	k1gInSc1
od	od	k7c2
Withina	Withino	k1gNnSc2
<g/>
,	,	kIx,
S	s	k7c7
P	P	kA
I	i	k9
Books	Books	k1gInSc1
<g/>
,	,	kIx,
srpen	srpen	k1gInSc1
2002	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
1-56171-938-2	1-56171-938-2	k4
</s>
<s>
Závan	závan	k1gInSc1
nad	nad	k7c7
Ruskem	Rusko	k1gNnSc7
<g/>
:	:	kIx,
Tajemství	tajemství	k1gNnSc1
Plota	Plota	k1gFnSc1
-	-	kIx~
návrat	návrat	k1gInSc1
teroru	teror	k1gInSc2
KGB	KGB	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gardners	Gardners	k1gInSc1
Books	Books	k1gInSc4
<g/>
,	,	kIx,
Leden	leden	k1gInSc4
2007	#num#	k4
ISBN	ISBN	kA
1-903933-95-1	1-903933-95-1	k4
</s>
<s>
Závan	závan	k1gInSc1
nad	nad	k7c7
Ruskem	Rusko	k1gNnSc7
<g/>
:	:	kIx,
Tajemství	tajemství	k1gNnSc1
Plota	Plota	k1gFnSc1
-	-	kIx~
návrat	návrat	k1gInSc1
teroru	teror	k1gInSc2
KGB	KGB	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ecounter	Ecounter	k1gInSc1
Books	Books	k1gInSc4
<g/>
,	,	kIx,
duben	duben	k1gInSc1
2007	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
1-59403-201-7	1-59403-201-7	k4
</s>
<s>
Alexander	Alexandra	k1gFnPc2
Litviněnko	Litviněnka	k1gFnSc5
<g/>
,	,	kIx,
Lublaňská	lublaňský	k2eAgFnSc1d1
přestupní	přestupní	k2eAgFnSc1d1
stanice	stanice	k1gFnSc1
(	(	kIx(
<g/>
Gang	gang	k1gInSc1
z	z	k7c2
Lublaňky	Lublaňka	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.2002	.2002	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
internetu	internet	k1gInSc6
<g/>
:	:	kIx,
Litviněnko	Litviněnka	k1gFnSc5
</s>
<s>
Litviněnko-Lublaňka	Litviněnko-Lublaňka	k1gFnSc1
<g/>
,	,	kIx,
kniha	kniha	k1gFnSc1
o	o	k7c6
vzestupu	vzestup	k1gInSc6
Putina	putin	k2eAgFnSc1d1
a	a	k8xC
FSB	FSB	kA
-	-	kIx~
byla	být	k5eAaImAgFnS
v	v	k7c6
přípravě	příprava	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
In	In	k?
Memoriam	Memoriam	k1gInSc4
vyšly	vyjít	k5eAaPmAgInP
<g/>
:	:	kIx,
</s>
<s>
Litviněnko	Litviněnka	k1gFnSc5
<g/>
,	,	kIx,
Marina	Marina	k1gFnSc1
<g/>
;	;	kIx,
Goldbarf	Goldbarf	k1gMnSc1
<g/>
,	,	kIx,
Alex	Alex	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Death	Death	k1gInSc1
of	of	k?
a	a	k8xC
dissident	dissident	k1gMnSc1
<g/>
:	:	kIx,
the	the	k?
poisoning	poisoning	k1gInSc1
of	of	k?
Alexander	Alexandra	k1gFnPc2
Litvinenko	Litvinenka	k1gFnSc5
and	and	k?
the	the	k?
return	return	k1gInSc1
of	of	k?
the	the	k?
KGB	KGB	kA
<g/>
.	.	kIx.
</s>
<s>
Překlady	překlad	k1gInPc1
<g/>
:	:	kIx,
</s>
<s>
Litviněnková	Litviněnková	k1gFnSc1
<g/>
,	,	kIx,
Goldbarf	Goldbarf	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Smrt	smrt	k1gFnSc1
disidenta	disident	k1gMnSc2
<g/>
:	:	kIx,
Otrávení	otrávení	k1gNnSc1
Alexandra	Alexandr	k1gMnSc2
Litviněnka	Litviněnka	k1gFnSc1
a	a	k8xC
návrat	návrat	k1gInSc1
KGB	KGB	kA
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jota	jota	k1gNnSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
408	#num#	k4
S.	S.	kA
ISBN	ISBN	kA
978-80-7217-505-5	978-80-7217-505-5	k4
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Litvinenko	Litvinenka	k1gFnSc5
death	death	k1gInSc1
<g/>
:	:	kIx,
Russian	Russian	k1gInSc1
spy	spy	k?
'	'	kIx"
<g/>
was	was	k?
working	working	k1gInSc1
for	forum	k1gNnPc2
MI	já	k3xPp1nSc3
<g/>
6	#num#	k4
<g/>
'	'	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
News	Newsa	k1gFnPc2
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Poisoned	Poisoned	k1gInSc1
spy	spy	k?
became	becam	k1gInSc5
Muslim	muslim	k1gMnSc1
on	on	k3xPp3gMnSc1
deathbed	deathbed	k1gMnSc1
<g/>
,	,	kIx,
says	says	k6eAd1
Chechen	Chechen	k2eAgMnSc1d1
dissident	dissident	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Independent	independent	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-09-22	2011-09-22	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
V	v	k7c6
těle	tělo	k1gNnSc6
exšpióna	exšpióna	k1gFnSc1
Litviněnka	Litviněnka	k1gFnSc1
nalezli	nalézt	k5eAaBmAgMnP,k5eAaPmAgMnP
radioaktivní	radioaktivní	k2eAgNnSc4d1
polónium	polónium	k1gNnSc4
Archivováno	archivovat	k5eAaBmNgNnS
10	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2007	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
↑	↑	k?
Secrets	Secrets	k1gInSc1
and	and	k?
spies	spies	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Guardian	Guardian	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Guardian	Guardian	k1gInSc1
News	News	k1gInSc4
&	&	k?
Media	medium	k1gNnSc2
<g/>
,	,	kIx,
2007-01-21	2007-01-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
https://www.respekt.cz/politika/litvinenkovu-vrazdu-pravdepodobne-schvalil-putin-napsal-britsky-soudce	https://www.respekt.cz/politika/litvinenkovu-vrazdu-pravdepodobne-schvalil-putin-napsal-britsky-soudce	k1gMnSc1
<g/>
↑	↑	k?
Lékaři	lékař	k1gMnPc1
<g/>
:	:	kIx,
Putinův	Putinův	k2eAgMnSc1d1
sok	sok	k1gMnSc1
Gajdar	Gajdar	k1gMnSc1
byl	být	k5eAaImAgMnS
otráven	otrávit	k5eAaPmNgMnS
<g/>
,	,	kIx,
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
↑	↑	k?
Poloniem	polonium	k1gNnSc7
byl	být	k5eAaImAgInS
otráven	otráven	k2eAgInSc1d1
také	také	k9
italský	italský	k2eAgInSc1d1
kontakt	kontakt	k1gInSc1
Litviněnka	Litviněnka	k1gFnSc1
<g/>
,	,	kIx,
Lidové	lidový	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2006	#num#	k4
<g/>
↑	↑	k?
Polonium	polonium	k1gNnSc1
otrávilo	otrávit	k5eAaPmAgNnS
dalšího	další	k2eAgMnSc4d1
muže	muž	k1gMnSc4
<g/>
,	,	kIx,
lidovky	lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
↑	↑	k?
Ruský	ruský	k2eAgMnSc1d1
agent	agent	k1gMnSc1
zemřel	zemřít	k5eAaPmAgMnS
<g/>
,	,	kIx,
podlehl	podlehnout	k5eAaPmAgMnS
otravě	otrava	k1gFnSc3
<g/>
,	,	kIx,
lidovky	lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
↑	↑	k?
Litviněnko	Litviněnka	k1gFnSc5
před	před	k7c7
smrtí	smrt	k1gFnSc7
přešel	přejít	k5eAaPmAgInS
k	k	k7c3
islámu	islám	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Economia	Economia	k1gFnSc1
<g/>
,	,	kIx,
2006-12-04	2006-12-04	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Britové	Brit	k1gMnPc1
obviní	obvinit	k5eAaPmIp3nP
z	z	k7c2
vraždy	vražda	k1gFnSc2
Litviněnka	Litviněnka	k1gFnSc1
ruského	ruský	k2eAgMnSc2d1
obchodníka	obchodník	k1gMnSc2
<g/>
,	,	kIx,
iDnes	iDnes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
↑	↑	k?
http://news.bbc.co.uk/1/hi/uk/6678887.stm	http://news.bbc.co.uk/1/hi/uk/6678887.stmo	k1gNnPc2
BBC	BBC	kA
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
uk	uk	k?
<g/>
↑	↑	k?
Manžel	manžel	k1gMnSc1
pracoval	pracovat	k5eAaImAgMnS
pro	pro	k7c4
britskou	britský	k2eAgFnSc4d1
MI	já	k3xPp1nSc3
<g/>
6	#num#	k4
<g/>
,	,	kIx,
říká	říkat	k5eAaImIp3nS
Litviněnková	Litviněnková	k1gFnSc1
v	v	k7c6
rozhovoru	rozhovor	k1gInSc6
pro	pro	k7c4
ČRo	ČRo	k1gFnSc4
<g/>
↑	↑	k?
Pokyn	pokyn	k1gInSc4
k	k	k7c3
zabití	zabití	k1gNnSc3
Litviněnka	Litviněnka	k1gFnSc1
vzešel	vzejít	k5eAaPmAgInS
z	z	k7c2
Ruska	Rusko	k1gNnSc2
<g/>
,	,	kIx,
ukazuje	ukazovat	k5eAaImIp3nS
prý	prý	k9
vyšetřování	vyšetřování	k1gNnSc6
<g/>
↑	↑	k?
Vraždu	vražda	k1gFnSc4
agenta	agent	k1gMnSc4
Litviněnka	Litviněnka	k1gFnSc1
schválil	schválit	k5eAaPmAgInS
podle	podle	k7c2
britského	britský	k2eAgMnSc2d1
soudce	soudce	k1gMnSc2
zřejmě	zřejmě	k6eAd1
prezident	prezident	k1gMnSc1
Putin	putin	k2eAgMnSc1d1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
GOLDFARB	GOLDFARB	kA
<g/>
,	,	kIx,
Alex	Alex	k1gMnSc1
<g/>
,	,	kIx,
LITVINĚNKOVÁ	LITVINĚNKOVÁ	kA
<g/>
,	,	kIx,
Marina	Marina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Smrt	smrt	k1gFnSc1
disidenta	disident	k1gMnSc2
<g/>
:	:	kIx,
otrávení	otrávení	k1gNnSc1
Alexandra	Alexandr	k1gMnSc2
Litviněnka	Litviněnka	k1gFnSc1
a	a	k8xC
návrat	návrat	k1gInSc1
KGB	KGB	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
angl.	angl.	k?
originálu	originál	k1gInSc2
přel	přít	k5eAaImAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jan	Jan	k1gMnSc1
Kozák	Kozák	k1gMnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Jota	jota	k1gNnSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
407	#num#	k4
S.	S.	kA
ISBN	ISBN	kA
978-80-7217-505-5	978-80-7217-505-5	k4
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Putinismus	Putinismus	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Alexandr	Alexandr	k1gMnSc1
Valtěrovič	Valtěrovič	k1gMnSc1
Litviněnko	Litviněnka	k1gFnSc5
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Litvinenko	Litvinenka	k1gFnSc5
inquiry	inquir	k1gInPc7
<g/>
:	:	kIx,
the	the	k?
proof	proof	k1gInSc1
Russia	Russius	k1gMnSc2
was	was	k?
involved	involved	k1gMnSc1
in	in	k?
dissident	dissident	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
murder	murder	k1gInSc1
</s>
<s>
Americká	americký	k2eAgFnSc1d1
NSA	NSA	kA
<g/>
:	:	kIx,
Smrt	smrt	k1gFnSc1
Litviněnka	Litviněnka	k1gFnSc1
byla	být	k5eAaImAgFnS
Ruskem	Rusko	k1gNnSc7
nařízená	nařízený	k2eAgFnSc1d1
„	„	k?
<g/>
státní	státní	k2eAgFnSc1d1
poprava	poprava	k1gFnSc1
<g/>
“	“	k?
</s>
<s>
Alexander	Alexandra	k1gFnPc2
Litvinenko	Litvinenka	k1gFnSc5
<g/>
:	:	kIx,
Profile	profil	k1gInSc5
of	of	k?
murdered	murdered	k1gMnSc1
Russian	Russian	k1gMnSc1
spy	spy	k?
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
mzk	mzk	k?
<g/>
2007401301	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
125006160	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
1556	#num#	k4
3381	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
nr	nr	k?
<g/>
2002019416	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
15086351	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-nr	lccn-nr	k1gInSc1
<g/>
2002019416	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
|	|	kIx~
Rusko	Rusko	k1gNnSc1
</s>
