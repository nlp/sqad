<s>
Správa	správa	k1gFnSc1
produktových	produktový	k2eAgFnPc2d1
informací	informace	k1gFnPc2
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojován	ozdrojován	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yRgFnPc4,k3yQgFnPc4,k3yIgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Komentář	komentář	k1gInSc1
<g/>
:	:	kIx,
článek	článek	k1gInSc1
neobsahuje	obsahovat	k5eNaImIp3nS
žádné	žádný	k3yNgFnPc4
reference	reference	k1gFnPc4
<g/>
,	,	kIx,
pouze	pouze	k6eAd1
jednu	jeden	k4xCgFnSc4
citaci	citace	k1gFnSc4
literatury	literatura	k1gFnSc2
bez	bez	k7c2
uvedení	uvedení	k1gNnSc2
detailů	detail	k1gInPc2
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaPmF,k5eAaImF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Konkrétní	konkrétní	k2eAgInPc4d1
problémy	problém	k1gInPc4
<g/>
:	:	kIx,
delkková	delkkový	k2eAgFnSc1d1
wikifikace	wikifikace	k1gFnSc1
<g/>
,	,	kIx,
-pevné	-pevný	k2eAgNnSc1d1
px	px	k?
<g/>
>	>	kIx)
<g/>
upright	upright	k1gInSc1
<g/>
,	,	kIx,
zdroje	zdroj	k1gInPc1
<g/>
,	,	kIx,
styl	styl	k1gInSc1
<g/>
/	/	kIx~
<g/>
vl	vl	k?
výzkum	výzkum	k1gInSc1
</s>
<s>
Správa	správa	k1gFnSc1
produktových	produktový	k2eAgFnPc2d1
informací	informace	k1gFnPc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Product	Product	k2eAgInSc1d1
Information	Information	k2eAgInSc1d1
Management	management	k1gInSc1
<g/>
,	,	kIx,
PIM	PIM	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
označení	označení	k1gNnSc1
pro	pro	k7c4
software	software	k2eAgInSc4d1
správy	správa	k1gInSc4
všech	všecek	k3xTgFnPc2
důležitých	důležitý	k2eAgFnPc2d1
informací	informace	k1gFnPc2
o	o	k7c6
určitém	určitý	k2eAgInSc6d1
produktu	produkt	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
PIM	PIM	kA
zahrnuje	zahrnovat	k5eAaImIp3nS
procesy	proces	k1gInPc4
a	a	k8xC
technologie	technologie	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
pomáhají	pomáhat	k5eAaImIp3nP
sbírat	sbírat	k5eAaImF
<g/>
,	,	kIx,
centralizovat	centralizovat	k5eAaBmF
informace	informace	k1gFnPc4
o	o	k7c6
produktech	produkt	k1gInPc6
a	a	k8xC
řídit	řídit	k5eAaImF
jejich	jejich	k3xOp3gFnSc4
distribuci	distribuce	k1gFnSc4
do	do	k7c2
různých	různý	k2eAgInPc2d1
marketingových	marketingový	k2eAgInPc2d1
kanálů	kanál	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
O	o	k7c6
systému	systém	k1gInSc6
</s>
<s>
Celý	celý	k2eAgInSc1d1
princip	princip	k1gInSc1
PIM	PIM	kA
se	se	k3xPyFc4
začal	začít	k5eAaPmAgInS
rozvíjet	rozvíjet	k5eAaImF
kolem	kolem	k7c2
roku	rok	k1gInSc2
2003	#num#	k4
především	především	k6eAd1
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavním	hlavní	k2eAgInSc7d1
impulsem	impuls	k1gInSc7
byl	být	k5eAaImAgInS
rychlý	rychlý	k2eAgInSc1d1
rozvoj	rozvoj	k1gInSc1
informačních	informační	k2eAgFnPc2d1
technologií	technologie	k1gFnPc2
a	a	k8xC
také	také	k9
nastupující	nastupující	k2eAgInSc1d1
trend	trend	k1gInSc1
prodeje	prodej	k1gInSc2
většiny	většina	k1gFnSc2
produktů	produkt	k1gInPc2
přes	přes	k7c4
internet	internet	k1gInSc4
nebo	nebo	k8xC
na	na	k7c6
mobilních	mobilní	k2eAgFnPc6d1
aplikacích	aplikace	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
posledních	poslední	k2eAgNnPc6d1
letech	léto	k1gNnPc6
proniká	pronikat	k5eAaImIp3nS
PIM	PIM	kA
pomalu	pomalu	k6eAd1
také	také	k9
do	do	k7c2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Prodej	prodej	k1gInSc1
online	onlinout	k5eAaPmIp3nS
klade	klást	k5eAaImIp3nS
zvýšené	zvýšený	k2eAgInPc4d1
nároky	nárok	k1gInPc4
na	na	k7c4
kvalitu	kvalita	k1gFnSc4
a	a	k8xC
množství	množství	k1gNnSc4
produktových	produktový	k2eAgFnPc2d1
informací	informace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ten	k3xDgInPc1
jsou	být	k5eAaImIp3nP
nezbytné	nezbytný	k2eAgNnSc4d1,k2eNgNnSc4d1
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
zákazník	zákazník	k1gMnSc1
produkt	produkt	k1gInSc1
na	na	k7c6
internetu	internet	k1gInSc6
nalezl	naleznout	k5eAaPmAgMnS,k5eAaBmAgMnS
a	a	k8xC
mohl	moct	k5eAaImAgMnS
porovnávat	porovnávat	k5eAaImF
jeho	jeho	k3xOp3gInPc4
parametry	parametr	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
tyto	tento	k3xDgFnPc4
informace	informace	k1gFnPc4
patří	patřit	k5eAaImIp3nS
název	název	k1gInSc1
produktu	produkt	k1gInSc2
<g/>
,	,	kIx,
cena	cena	k1gFnSc1
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gFnSc2
kategorie	kategorie	k1gFnSc2
<g/>
,	,	kIx,
složení	složení	k1gNnSc2
<g/>
,	,	kIx,
marketingové	marketingový	k2eAgFnSc2d1
popisky	popiska	k1gFnSc2
<g/>
,	,	kIx,
recenze	recenze	k1gFnPc1
apod.	apod.	kA
</s>
<s>
Navíc	navíc	k6eAd1
se	se	k3xPyFc4
informace	informace	k1gFnPc1
o	o	k7c6
produktech	produkt	k1gInPc6
již	již	k6eAd1
nenabízejí	nabízet	k5eNaImIp3nP
jen	jen	k9
prostřednictvím	prostřednictvím	k7c2
webu	web	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
rozsáhlým	rozsáhlý	k2eAgInSc7d1
souborem	soubor	k1gInSc7
kanálů	kanál	k1gInPc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
jsou	být	k5eAaImIp3nP
mobilní	mobilní	k2eAgInPc1d1
telefony	telefon	k1gInPc1
<g/>
,	,	kIx,
tablety	tableta	k1gFnPc1
<g/>
,	,	kIx,
kamenné	kamenný	k2eAgInPc1d1
obchody	obchod	k1gInPc1
<g/>
,	,	kIx,
tištěné	tištěný	k2eAgInPc1d1
katalogy	katalog	k1gInPc1
<g/>
,	,	kIx,
letáky	leták	k1gInPc1
atd.	atd.	kA
Tento	tento	k3xDgInSc4
růst	růst	k1gInSc4
klade	klást	k5eAaImIp3nS
zvýšené	zvýšený	k2eAgInPc4d1
nároky	nárok	k1gInPc4
na	na	k7c4
synchronizaci	synchronizace	k1gFnSc4
informací	informace	k1gFnPc2
napříč	napříč	k7c7
distribučními	distribuční	k2eAgInPc7d1
kanály	kanál	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
S	s	k7c7
produktovými	produktový	k2eAgFnPc7d1
informacemi	informace	k1gFnPc7
se	se	k3xPyFc4
pracuje	pracovat	k5eAaImIp3nS
v	v	k7c6
celé	celý	k2eAgFnSc6d1
řadě	řada	k1gFnSc6
oddělení	oddělení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obchodní	obchodní	k2eAgInSc4d1
oddělení	oddělení	k1gNnSc1
se	se	k3xPyFc4
stará	starat	k5eAaImIp3nS
o	o	k7c4
ceny	cena	k1gFnPc4
<g/>
,	,	kIx,
marketingové	marketingový	k2eAgNnSc4d1
oddělení	oddělení	k1gNnSc4
o	o	k7c4
popisky	popiska	k1gFnPc4
nebo	nebo	k8xC
obrázky	obrázek	k1gInPc4
<g/>
,	,	kIx,
produktové	produktový	k2eAgNnSc1d1
oddělení	oddělení	k1gNnSc1
o	o	k7c6
zakládání	zakládání	k1gNnSc6
produktových	produktový	k2eAgFnPc2d1
karet	kareta	k1gFnPc2
v	v	k7c6
ERP	ERP	kA
<g/>
,	,	kIx,
oddělení	oddělení	k1gNnSc1
nákupu	nákup	k1gInSc2
o	o	k7c4
příjem	příjem	k1gInSc4
dat	datum	k1gNnPc2
od	od	k7c2
dodavatelů	dodavatel	k1gMnPc2
<g/>
,	,	kIx,
IT	IT	kA
o	o	k7c4
udržitelnou	udržitelný	k2eAgFnSc4d1
správu	správa	k1gFnSc4
dat	datum	k1gNnPc2
atd.	atd.	kA
Tato	tento	k3xDgFnSc1
rozptýlenost	rozptýlenost	k1gFnSc1
informací	informace	k1gFnPc2
o	o	k7c6
produktech	produkt	k1gInPc6
mezi	mezi	k7c4
různé	různý	k2eAgMnPc4d1
zaměstnance	zaměstnanec	k1gMnPc4
<g/>
,	,	kIx,
složky	složka	k1gFnPc4
<g/>
,	,	kIx,
soubory	soubor	k1gInPc4
značně	značně	k6eAd1
ztěžuje	ztěžovat	k5eAaImIp3nS
nezbytnou	zbytný	k2eNgFnSc4d1,k2eAgFnSc4d1
spolupráci	spolupráce	k1gFnSc4
mezi	mezi	k7c7
odděleními	oddělení	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cílem	cíl	k1gInSc7
PIM	PIM	kA
systémů	systém	k1gInPc2
je	být	k5eAaImIp3nS
vytvoření	vytvoření	k1gNnSc4
jedné	jeden	k4xCgFnSc2
centrální	centrální	k2eAgFnSc2d1
platformy	platforma	k1gFnSc2
pro	pro	k7c4
všechny	všechen	k3xTgMnPc4
oddělení	oddělený	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Procesy	proces	k1gInPc1
</s>
<s>
Práce	práce	k1gFnSc1
s	s	k7c7
produktovými	produktový	k2eAgFnPc7d1
informacemi	informace	k1gFnPc7
v	v	k7c6
softwerech	softwer	k1gMnPc6
PIM	PIM	kA
se	se	k3xPyFc4
dělí	dělit	k5eAaImIp3nS
do	do	k7c2
4	#num#	k4
procesů	proces	k1gInPc2
<g/>
:	:	kIx,
</s>
<s>
Shromáždění	shromáždění	k1gNnSc1
<g/>
:	:	kIx,
Produktové	produktový	k2eAgFnPc1d1
informace	informace	k1gFnPc1
se	se	k3xPyFc4
vytvářejí	vytvářet	k5eAaImIp3nP
v	v	k7c6
rámci	rámec	k1gInSc6
celé	celý	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
i	i	k9
mimo	mimo	k7c4
ni	on	k3xPp3gFnSc4
a	a	k8xC
jsou	být	k5eAaImIp3nP
často	často	k6eAd1
uložené	uložený	k2eAgInPc1d1
v	v	k7c6
řadě	řada	k1gFnSc6
odlišných	odlišný	k2eAgInPc2d1
systémů	systém	k1gInPc2
a	a	k8xC
složek	složka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
PIM	PIM	kA
systém	systém	k1gInSc1
by	by	kYmCp3nS
měl	mít	k5eAaImAgInS
tyto	tento	k3xDgFnPc4
informace	informace	k1gFnPc4
shromažďovat	shromažďovat	k5eAaImF
na	na	k7c6
jednom	jeden	k4xCgNnSc6
místě	místo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Konsolidace	konsolidace	k1gFnSc1
<g/>
:	:	kIx,
Systém	systém	k1gInSc1
kontroluje	kontrolovat	k5eAaImIp3nS
vznik	vznik	k1gInSc4
a	a	k8xC
existenci	existence	k1gFnSc4
duplikací	duplikace	k1gFnPc2
nebo	nebo	k8xC
neúplných	úplný	k2eNgInPc2d1
záznamů	záznam	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Umožňuje	umožňovat	k5eAaImIp3nS
tzv.	tzv.	kA
fuzzy	fuzza	k1gFnSc2
searching	searching	k1gInSc1
(	(	kIx(
<g/>
hledání	hledání	k1gNnSc1
výrazů	výraz	k1gInPc2
na	na	k7c6
základě	základ	k1gInSc6
částečné	částečný	k2eAgFnSc2d1
shody	shoda	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Obohacení	obohacení	k1gNnSc1
<g/>
:	:	kIx,
Základní	základní	k2eAgFnPc1d1
informace	informace	k1gFnPc1
o	o	k7c6
produktech	produkt	k1gInPc6
z	z	k7c2
ERP	ERP	kA
systému	systém	k1gInSc2
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
obohacovány	obohacovat	k5eAaImNgFnP
o	o	k7c4
další	další	k2eAgFnPc4d1
informace	informace	k1gFnPc4
–	–	k?
např.	např.	kA
dlouhá	dlouhý	k2eAgFnSc1d1
textová	textový	k2eAgFnSc1d1
pole	pole	k1gFnSc1
<g/>
,	,	kIx,
fotografie	fotografie	k1gFnSc1
v	v	k7c6
tiskové	tiskový	k2eAgFnSc6d1
kvalitě	kvalita	k1gFnSc6
<g/>
,	,	kIx,
videa	video	k1gNnSc2
<g/>
,	,	kIx,
příručky	příručka	k1gFnSc2
<g/>
,	,	kIx,
výkresy	výkres	k1gInPc7
CAD	CAD	kA
<g/>
/	/	kIx~
<g/>
CAM	CAM	kA
atd.	atd.	kA
Dochází	docházet	k5eAaImIp3nS
ke	k	k7c3
kategorizaci	kategorizace	k1gFnSc3
a	a	k8xC
klasifikaci	klasifikace	k1gFnSc4
produktů	produkt	k1gInPc2
do	do	k7c2
skupin	skupina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klasifikační	klasifikační	k2eAgInSc1d1
systém	systém	k1gInSc1
pomáhá	pomáhat	k5eAaImIp3nS
se	s	k7c7
správou	správa	k1gFnSc7
atributů	atribut	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Atributy	atribut	k1gInPc7
popisují	popisovat	k5eAaImIp3nP
přesně	přesně	k6eAd1
každý	každý	k3xTgInSc4
produkt	produkt	k1gInSc4
<g/>
,	,	kIx,
jeho	on	k3xPp3gInSc4,k3xOp3gInSc4
název	název	k1gInSc4
<g/>
,	,	kIx,
hmotnost	hmotnost	k1gFnSc4
<g/>
,	,	kIx,
výšku	výška	k1gFnSc4
<g/>
,	,	kIx,
délku	délka	k1gFnSc4
<g/>
,	,	kIx,
šířku	šířka	k1gFnSc4
atd.	atd.	kA
Jedním	jeden	k4xCgInSc7
ze	z	k7c2
základních	základní	k2eAgInPc2d1
principů	princip	k1gInPc2
<g/>
,	,	kIx,
na	na	k7c6
kterých	který	k3yQgFnPc6,k3yIgFnPc6,k3yRgFnPc6
stojí	stát	k5eAaImIp3nS
systém	systém	k1gInSc1
PIM	PIM	kA
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
umožnit	umožnit	k5eAaPmF
běžným	běžný	k2eAgMnPc3d1
uživatelům	uživatel	k1gMnPc3
přidávat	přidávat	k5eAaImF
vlastní	vlastní	k2eAgInPc4d1
atributy	atribut	k1gInPc4
produktům	produkt	k1gInPc3
bez	bez	k7c2
podpory	podpora	k1gFnSc2
ze	z	k7c2
strany	strana	k1gFnSc2
pracovníků	pracovník	k1gMnPc2
IT.	IT.	k1gFnSc2
Jejich	jejich	k3xOp3gFnSc1
správa	správa	k1gFnSc1
je	být	k5eAaImIp3nS
zásadní	zásadní	k2eAgFnSc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
počet	počet	k1gInSc1
atributů	atribut	k1gInPc2
může	moct	k5eAaImIp3nS
rychle	rychle	k6eAd1
růst	růst	k1gInSc1
a	a	k8xC
sledované	sledovaný	k2eAgInPc1d1
atributy	atribut	k1gInPc1
u	u	k7c2
různých	různý	k2eAgInPc2d1
výrobků	výrobek	k1gInPc2
bývají	bývat	k5eAaImIp3nP
odlišné	odlišný	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zajímavou	zajímavý	k2eAgFnSc7d1
komerční	komerční	k2eAgFnSc7d1
funkcí	funkce	k1gFnSc7
systémů	systém	k1gInPc2
PIM	PIM	kA
je	být	k5eAaImIp3nS
možnost	možnost	k1gFnSc4
vytvářet	vytvářet	k5eAaImF
vztahy	vztah	k1gInPc4
mezi	mezi	k7c7
produkty	produkt	k1gInPc7
<g/>
,	,	kIx,
čehož	což	k3yRnSc2,k3yQnSc2
se	se	k3xPyFc4
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
v	v	k7c6
řízení	řízení	k1gNnSc6
up-selling	up-selling	k1gInSc4
a	a	k8xC
cross-selling	cross-selling	k1gInSc4
produktů	produkt	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Distribuce	distribuce	k1gFnSc1
<g/>
:	:	kIx,
Určuje	určovat	k5eAaImIp3nS
se	se	k3xPyFc4
jaká	jaký	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
informace	informace	k1gFnSc1
se	se	k3xPyFc4
zveřejní	zveřejnit	k5eAaPmIp3nS
v	v	k7c6
konkrétním	konkrétní	k2eAgInSc6d1
distribučním	distribuční	k2eAgInSc6d1
kanálu	kanál	k1gInSc6
–	–	k?
např.	např.	kA
webech	web	k1gInPc6
<g/>
,	,	kIx,
mobilních	mobilní	k2eAgInPc6d1
telefonech	telefon	k1gInPc6
<g/>
,	,	kIx,
tabletech	tablet	k1gInPc6
<g/>
,	,	kIx,
místech	místo	k1gNnPc6
prodeje	prodej	k1gInSc2
<g/>
,	,	kIx,
tištěných	tištěný	k2eAgInPc6d1
katalozích	katalog	k1gInPc6
<g/>
,	,	kIx,
letácích	leták	k1gInPc6
atd.	atd.	kA
</s>
<s>
Pozice	pozice	k1gFnSc1
PIM	PIM	kA
v	v	k7c6
podniku	podnik	k1gInSc6
</s>
<s>
Díky	díky	k7c3
jasnému	jasný	k2eAgNnSc3d1
zaměření	zaměření	k1gNnSc3
systémů	systém	k1gInPc2
PIM	PIM	kA
je	být	k5eAaImIp3nS
zřejmé	zřejmý	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
jen	jen	k9
jeden	jeden	k4xCgInSc1
systém	systém	k1gInSc1
z	z	k7c2
mnoha	mnoho	k4c2
<g/>
,	,	kIx,
které	který	k3yRgInPc4,k3yIgInPc4,k3yQgInPc4
organizace	organizace	k1gFnSc1
potřebuje	potřebovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
se	se	k3xPyFc4
ovšem	ovšem	k9
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
produktové	produktový	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
systém	systém	k1gInSc1
PIM	PIM	kA
má	mít	k5eAaImIp3nS
centrální	centrální	k2eAgFnSc4d1
roli	role	k1gFnSc4
mezi	mezi	k7c7
různými	různý	k2eAgInPc7d1
zdroji	zdroj	k1gInPc7
informací	informace	k1gFnPc2
o	o	k7c6
produktech	produkt	k1gInPc6
a	a	k8xC
distribučními	distribuční	k2eAgInPc7d1
kanály	kanál	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
jakýmsi	jakýsi	k3yIgInSc7
mezičlánkem	mezičlánek	k1gInSc7
mezi	mezi	k7c7
ERP	ERP	kA
systémem	systém	k1gInSc7
a	a	k8xC
distribucí	distribuce	k1gFnSc7
informací	informace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
PIM	PIM	kA
systémům	systém	k1gInPc3
je	být	k5eAaImIp3nS
předpovídáno	předpovídat	k5eAaImNgNnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
v	v	k7c6
příštích	příští	k2eAgInPc6d1
letech	let	k1gInPc6
stanou	stanout	k5eAaPmIp3nP
standardní	standardní	k2eAgFnSc7d1
součástí	součást	k1gFnSc7
softwarového	softwarový	k2eAgNnSc2d1
vybavení	vybavení	k1gNnSc2
ve	v	k7c6
většině	většina	k1gFnSc6
velkých	velký	k2eAgFnPc2d1
firem	firma	k1gFnPc2
a	a	k8xC
zařadí	zařadit	k5eAaPmIp3nS
se	se	k3xPyFc4
po	po	k7c4
bok	bok	k1gInSc4
hromadně	hromadně	k6eAd1
používaných	používaný	k2eAgInPc2d1
ERP	ERP	kA
<g/>
,	,	kIx,
CRM	CRM	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
PIM	PIM	kA
v	v	k7c6
ČR	ČR	kA
</s>
<s>
V	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
je	být	k5eAaImIp3nS
pojem	pojem	k1gInSc1
PIM	PIM	kA
zatím	zatím	k6eAd1
poměrně	poměrně	k6eAd1
neznámý	známý	k2eNgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Možná	možná	k6eAd1
proto	proto	k8xC
se	se	k3xPyFc4
o	o	k7c6
problematice	problematika	k1gFnSc6
produktových	produktový	k2eAgFnPc2d1
informací	informace	k1gFnPc2
hovoří	hovořit	k5eAaImIp3nS
zejména	zejména	k9
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
e-commerce	e-commerka	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
PIM	PIM	kA
ve	v	k7c6
světě	svět	k1gInSc6
</s>
<s>
V	v	k7c6
zahraničí	zahraničí	k1gNnSc6
již	již	k6eAd1
většina	většina	k1gFnSc1
velkých	velký	k2eAgFnPc2d1
firem	firma	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
nabízejí	nabízet	k5eAaImIp3nP
širokou	široký	k2eAgFnSc4d1
škálu	škála	k1gFnSc4
produktů	produkt	k1gInPc2
<g/>
,	,	kIx,
systémy	systém	k1gInPc4
PIM	PIM	kA
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
trend	trend	k1gInSc1
se	se	k3xPyFc4
díky	díky	k7c3
nadnárodním	nadnárodní	k2eAgFnPc3d1
společnostem	společnost	k1gFnPc3
pomalu	pomalu	k6eAd1
začíná	začínat	k5eAaImIp3nS
dostávat	dostávat	k5eAaImF
i	i	k9
do	do	k7c2
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
největší	veliký	k2eAgMnPc4d3
poskytovatele	poskytovatel	k1gMnPc4
PIM	PIM	kA
řešení	řešení	k1gNnSc4
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
patří	patřit	k5eAaImIp3nS
SAP	sapa	k1gFnPc2
Hybris	Hybris	k1gFnSc2
<g/>
,	,	kIx,
Stibo	Stiba	k1gFnSc5
Systems	Systems	k1gInSc1
<g/>
,	,	kIx,
IBM	IBM	kA
<g/>
,	,	kIx,
Informatica	Informaticum	k1gNnSc2
nebo	nebo	k8xC
Riversand	Riversanda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
PIM	PIM	kA
základ	základ	k1gInSc4
automatizace	automatizace	k1gFnSc2
sazby	sazba	k1gFnSc2
tiskovin	tiskovina	k1gFnPc2
</s>
<s>
Systém	systém	k1gInSc1
PIM	PIM	kA
<g/>
,	,	kIx,
jakožto	jakožto	k8xS
centrální	centrální	k2eAgFnSc1d1
databáze	databáze	k1gFnSc1
produktových	produktový	k2eAgFnPc2d1
informací	informace	k1gFnPc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
nejčastějším	častý	k2eAgInSc7d3
nástrojem	nástroj	k1gInSc7
úplné	úplný	k2eAgFnSc2d1
automatizace	automatizace	k1gFnSc2
sazby	sazba	k1gFnSc2
tiskovin	tiskovina	k1gFnPc2
ADTP	ADTP	kA
(	(	kIx(
<g/>
Automated	Automated	k1gInSc1
desktop	desktop	k1gInSc1
publishing	publishing	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Software	software	k1gInSc1
automatizované	automatizovaný	k2eAgFnSc2d1
sazby	sazba	k1gFnSc2
lze	lze	k6eAd1
propojit	propojit	k5eAaPmF
s	s	k7c7
PIM	PIM	kA
systémem	systém	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Data	datum	k1gNnPc1
se	se	k3xPyFc4
podle	podle	k7c2
zvoleného	zvolený	k2eAgNnSc2d1
nastavení	nastavení	k1gNnSc2
hromadně	hromadně	k6eAd1
překopírují	překopírovat	k5eAaPmIp3nP,k5eAaImIp3nP
z	z	k7c2
PIM	PIM	kA
do	do	k7c2
grafického	grafický	k2eAgInSc2d1
souboru	soubor	k1gInSc2
(	(	kIx(
<g/>
InPesign	InPesign	k1gInSc1
<g/>
,	,	kIx,
QuarkXPress	QuarkXPress	k1gInSc1
<g/>
)	)	kIx)
bez	bez	k7c2
zásahu	zásah	k1gInSc2
grafika	grafik	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
proces	proces	k1gInSc1
značně	značně	k6eAd1
zrychluje	zrychlovat	k5eAaImIp3nS
grafické	grafický	k2eAgFnPc4d1
práce	práce	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Strojař	strojař	k1gMnSc1
<g/>
:	:	kIx,
časopis	časopis	k1gInSc1
Masarykovy	Masarykův	k2eAgFnSc2d1
akademie	akademie	k1gFnSc2
práce	práce	k1gFnSc2
<g/>
,	,	kIx,
prosinec	prosinec	k1gInSc1
2016	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
<g/>
XXV	XXV	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
dvojčíslo	dvojčíslo	k1gNnSc1
3	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
,	,	kIx,
<g/>
ISSN	ISSN	kA
1213	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
591	#num#	k4
<g/>
,	,	kIx,
36	#num#	k4
s.	s.	k?
(	(	kIx(
<g/>
str	str	kA
<g/>
.1	.1	k4
-	-	kIx~
7	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
Vratislav	Vratislav	k1gFnSc1
Preclík	preclík	k1gInSc1
<g/>
,	,	kIx,
Jitka	Jitka	k1gFnSc1
Nevečeřalová-Preclíková	Nevečeřalová-Preclíková	k1gFnSc1
<g/>
:	:	kIx,
"	"	kIx"
<g/>
Navrhování	navrhování	k1gNnSc1
nových	nový	k2eAgInPc2d1
výrobních	výrobní	k2eAgInPc2d1
procesů	proces	k1gInPc2
a	a	k8xC
systémů	systém	k1gInPc2
<g/>
"	"	kIx"
<g/>
,	,	kIx,
str	str	kA
<g/>
.17	.17	k4
-	-	kIx~
23	#num#	k4
<g/>
,	,	kIx,
Vratislav	Vratislav	k1gMnSc1
Preclík	preclík	k1gInSc1
<g/>
:	:	kIx,
"	"	kIx"
<g/>
Koncepce	koncepce	k1gFnSc1
objednacích	objednací	k2eAgInPc2d1
systémů	systém	k1gInPc2
v	v	k7c6
logistickém	logistický	k2eAgNnSc6d1
řízení	řízení	k1gNnSc6
<g/>
"	"	kIx"
<g/>
)	)	kIx)
,	,	kIx,
registrace	registrace	k1gFnSc1
ministerstva	ministerstvo	k1gNnSc2
kultury	kultura	k1gFnSc2
ČR	ČR	kA
č.	č.	k?
E	E	kA
13559	#num#	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Jorij	Jorít	k5eAaPmRp2nS
Abraham	Abraham	k1gMnSc1
<g/>
:	:	kIx,
Product	Product	k2eAgInSc1d1
Information	Information	k1gInSc1
Management	management	k1gInSc1
<g/>
:	:	kIx,
Theory	Theora	k1gFnPc1
and	and	k?
Practise	Practise	k1gFnSc1
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Řízení	řízení	k1gNnSc1
výrobkových	výrobkový	k2eAgNnPc2d1
dat	datum	k1gNnPc2
(	(	kIx(
<g/>
Product	Product	k1gInSc1
Data	datum	k1gNnSc2
Management	management	k1gInSc1
<g/>
,	,	kIx,
PDM	PDM	kA
<g/>
)	)	kIx)
</s>
<s>
Řízení	řízení	k1gNnSc1
životního	životní	k2eAgInSc2d1
cyklu	cyklus	k1gInSc2
výrobku	výrobek	k1gInSc2
(	(	kIx(
<g/>
Product	Product	k1gInSc1
Lifecycle	Lifecycle	k1gNnSc1
Management	management	k1gInSc1
<g/>
,	,	kIx,
PLM	PLM	kA
<g/>
)	)	kIx)
</s>
