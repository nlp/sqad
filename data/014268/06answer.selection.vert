<s>
Správa	správa	k1gFnSc1
produktových	produktový	k2eAgFnPc2d1
informací	informace	k1gFnPc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Product	Product	k2eAgInSc1d1
Information	Information	k2eAgInSc1d1
Management	management	k1gInSc1
<g/>
,	,	kIx,
PIM	PIM	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
označení	označení	k1gNnSc1
pro	pro	k7c4
software	software	k2eAgInSc4d1
správy	správa	k1gInSc4
všech	všecek	k3xTgFnPc2
důležitých	důležitý	k2eAgFnPc2d1
informací	informace	k1gFnPc2
o	o	k7c6
určitém	určitý	k2eAgInSc6d1
produktu	produkt	k1gInSc6
<g/>
.	.	kIx.
</s>