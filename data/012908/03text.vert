<p>
<s>
Nocaima	Nocaima	k1gFnSc1	Nocaima
je	být	k5eAaImIp3nS	být
obec	obec	k1gFnSc1	obec
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Cundinamarca	Cundinamarc	k1gInSc2	Cundinamarc
v	v	k7c6	v
Kolumbii	Kolumbie	k1gFnSc6	Kolumbie
<g/>
,	,	kIx,	,
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Bogoty	Bogota	k1gFnSc2	Bogota
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
první	první	k4xOgFnPc4	první
stálé	stálý	k2eAgFnPc4d1	stálá
osady	osada	k1gFnPc4	osada
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
španělské	španělský	k2eAgFnSc2d1	španělská
kolonizace	kolonizace	k1gFnSc2	kolonizace
ze	z	k7c2	z
17	[number]	k4	17
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
oblíbeným	oblíbený	k2eAgInSc7d1	oblíbený
turistickým	turistický	k2eAgInSc7d1	turistický
cílem	cíl	k1gInSc7	cíl
obyvatel	obyvatel	k1gMnSc1	obyvatel
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bogotě	Bogota	k1gFnSc6	Bogota
a	a	k8xC	a
jejím	její	k3xOp3gNnSc6	její
okolí	okolí	k1gNnSc6	okolí
je	být	k5eAaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
jako	jako	k8xS	jako
město	město	k1gNnSc1	město
učitelů	učitel	k1gMnPc2	učitel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografické	geografický	k2eAgInPc1d1	geografický
údaje	údaj	k1gInPc1	údaj
==	==	k?	==
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
svahu	svah	k1gInSc6	svah
údolí	údolí	k1gNnSc2	údolí
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
řeky	řeka	k1gFnSc2	řeka
<g/>
,	,	kIx,	,
67	[number]	k4	67
km	km	kA	km
od	od	k7c2	od
hranice	hranice	k1gFnSc2	hranice
s	s	k7c7	s
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Bogotou	Bogota	k1gFnSc7	Bogota
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
všechna	všechen	k3xTgNnPc1	všechen
sídla	sídlo	k1gNnPc1	sídlo
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
rozloha	rozloha	k1gFnSc1	rozloha
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
značném	značný	k2eAgNnSc6d1	značné
rozmezí	rozmezí	k1gNnSc6	rozmezí
nadmořské	nadmořský	k2eAgFnSc2d1	nadmořská
výšky	výška	k1gFnSc2	výška
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zhruba	zhruba	k6eAd1	zhruba
mezi	mezi	k7c7	mezi
1	[number]	k4	1
050	[number]	k4	050
a	a	k8xC	a
1	[number]	k4	1
200	[number]	k4	200
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Náměstí	náměstí	k1gNnSc1	náměstí
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
1	[number]	k4	1
104	[number]	k4	104
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Na	na	k7c6	na
jižním	jižní	k2eAgInSc6d1	jižní
okraji	okraj	k1gInSc6	okraj
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
rozšířené	rozšířený	k2eAgFnSc2d1	rozšířená
venkovské	venkovský	k2eAgFnSc2d1	venkovská
oblasti	oblast	k1gFnSc2	oblast
probíhá	probíhat	k5eAaImIp3nS	probíhat
dálnice	dálnice	k1gFnSc1	dálnice
Bogota	Bogota	k1gFnSc1	Bogota
–	–	k?	–
Medellín	Medellín	k1gInSc1	Medellín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
městského	městský	k2eAgInSc2d1	městský
celku	celek	k1gInSc2	celek
je	být	k5eAaImIp3nS	být
zahrnuta	zahrnut	k2eAgFnSc1d1	zahrnuta
plocha	plocha	k1gFnSc1	plocha
o	o	k7c6	o
celkové	celkový	k2eAgFnSc6d1	celková
rozloze	rozloha	k1gFnSc6	rozloha
3	[number]	k4	3
km2	km2	k4	km2
<g/>
,	,	kIx,	,
celková	celkový	k2eAgFnSc1d1	celková
působnost	působnost	k1gFnSc1	působnost
je	být	k5eAaImIp3nS	být
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
na	na	k7c4	na
venkovské	venkovský	k2eAgNnSc4d1	venkovské
okolí	okolí	k1gNnSc4	okolí
s	s	k7c7	s
celkovou	celkový	k2eAgFnSc7d1	celková
plochou	plocha	k1gFnSc7	plocha
69	[number]	k4	69
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Venkovský	venkovský	k2eAgInSc1d1	venkovský
sektor	sektor	k1gInSc1	sektor
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
sídla	sídlo	k1gNnSc2	sídlo
Baquero	Baquero	k1gNnSc4	Baquero
<g/>
,	,	kIx,	,
Naranjal	Naranjal	k1gMnSc1	Naranjal
<g/>
,	,	kIx,	,
Canutal	Canutal	k1gMnSc1	Canutal
<g/>
,	,	kIx,	,
San	San	k1gMnSc1	San
Agustin	Agustin	k1gMnSc1	Agustin
<g/>
,	,	kIx,	,
Centro	Centro	k1gNnSc1	Centro
<g/>
,	,	kIx,	,
San	San	k1gFnSc1	San
Cayetano	Cayetana	k1gFnSc5	Cayetana
<g/>
,	,	kIx,	,
Cocunche	Cocunche	k1gFnSc5	Cocunche
<g/>
,	,	kIx,	,
Santa	Santa	k1gMnSc1	Santa
Barbara	Barbara	k1gFnSc1	Barbara
<g/>
,	,	kIx,	,
Fical	Fical	k1gInSc1	Fical
<g/>
,	,	kIx,	,
San	San	k1gFnPc1	San
Pablo	Pablo	k1gNnSc1	Pablo
<g/>
,	,	kIx,	,
Jagual	Jagual	k1gInSc1	Jagual
<g/>
,	,	kIx,	,
San	San	k1gFnSc1	San
Jose	Jose	k1gFnSc1	Jose
<g/>
,	,	kIx,	,
La	la	k1gNnSc1	la
Florida	Florida	k1gFnSc1	Florida
<g/>
,	,	kIx,	,
San	San	k1gMnSc2	San
Juanito	Juanit	k2eAgNnSc1d1	Juanito
<g/>
,	,	kIx,	,
La	la	k1gNnPc7	la
Conception	Conception	k1gInSc1	Conception
<g/>
,	,	kIx,	,
Tobia	Tobia	k1gFnSc1	Tobia
<g/>
,	,	kIx,	,
Lomalarga	Lomalarga	k1gFnSc1	Lomalarga
<g/>
,	,	kIx,	,
Vilauta	Vilaut	k2eAgFnSc1d1	Vilaut
<g/>
,	,	kIx,	,
El	Ela	k1gFnPc2	Ela
Cajon	Cajona	k1gFnPc2	Cajona
<g/>
,	,	kIx,	,
Volcano	Volcana	k1gFnSc5	Volcana
<g/>
,	,	kIx,	,
Mercedes	mercedes	k1gInSc1	mercedes
<g/>
.	.	kIx.	.
</s>
<s>
Osídlení	osídlení	k1gNnSc1	osídlení
včetně	včetně	k7c2	včetně
venkovské	venkovský	k2eAgFnSc2d1	venkovská
zóny	zóna	k1gFnSc2	zóna
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
8000	[number]	k4	8000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
v	v	k7c6	v
městské	městský	k2eAgFnSc6d1	městská
části	část	k1gFnSc6	část
podle	podle	k7c2	podle
údajů	údaj	k1gInPc2	údaj
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
1858	[number]	k4	1858
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
vysoké	vysoký	k2eAgFnSc3d1	vysoká
nadmořské	nadmořský	k2eAgFnSc3d1	nadmořská
výšce	výška	k1gFnSc3	výška
je	být	k5eAaImIp3nS	být
i	i	k9	i
při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
zeměpisné	zeměpisný	k2eAgFnSc6d1	zeměpisná
šířce	šířka	k1gFnSc6	šířka
oblastí	oblast	k1gFnPc2	oblast
s	s	k7c7	s
celoročně	celoročně	k6eAd1	celoročně
příjemným	příjemný	k2eAgNnSc7d1	příjemné
klimatem	klima	k1gNnSc7	klima
s	s	k7c7	s
celoročním	celoroční	k2eAgInSc7d1	celoroční
teplotním	teplotní	k2eAgInSc7d1	teplotní
průměrem	průměr	k1gInSc7	průměr
24	[number]	k4	24
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
jen	jen	k9	jen
ojediněle	ojediněle	k6eAd1	ojediněle
převyšující	převyšující	k2eAgInPc4d1	převyšující
30	[number]	k4	30
°	°	k?	°
<g/>
C.	C.	kA	C.
Při	při	k7c6	při
průměrném	průměrný	k2eAgInSc6d1	průměrný
ročním	roční	k2eAgInSc6d1	roční
srážkovém	srážkový	k2eAgInSc6d1	srážkový
úhrnu	úhrn	k1gInSc6	úhrn
1	[number]	k4	1
760	[number]	k4	760
mm	mm	kA	mm
je	být	k5eAaImIp3nS	být
předurčena	předurčen	k2eAgFnSc1d1	předurčena
k	k	k7c3	k
úspěšnému	úspěšný	k2eAgNnSc3d1	úspěšné
pěstování	pěstování	k1gNnSc3	pěstování
cukrové	cukrový	k2eAgFnSc2d1	cukrová
třtiny	třtina	k1gFnSc2	třtina
a	a	k8xC	a
tropického	tropický	k2eAgNnSc2d1	tropické
ovoce	ovoce	k1gNnSc2	ovoce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
předkolumbijských	předkolumbijský	k2eAgFnPc6d1	předkolumbijský
dobách	doba	k1gFnPc6	doba
bylo	být	k5eAaImAgNnS	být
území	území	k1gNnSc1	území
oblasti	oblast	k1gFnSc2	oblast
osídleno	osídlit	k5eAaPmNgNnS	osídlit
indiány	indián	k1gMnPc4	indián
bojovného	bojovný	k2eAgInSc2d1	bojovný
a	a	k8xC	a
agresivního	agresivní	k2eAgInSc2d1	agresivní
kmene	kmen	k1gInSc2	kmen
Panches	Panchesa	k1gFnPc2	Panchesa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nocaima	Nocaima	k1gFnSc1	Nocaima
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
3	[number]	k4	3
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1605	[number]	k4	1605
v	v	k7c6	v
období	období	k1gNnSc6	období
španělské	španělský	k2eAgFnSc2d1	španělská
kolonizace	kolonizace	k1gFnSc2	kolonizace
místodržícím	místodržící	k1gMnSc7	místodržící
v	v	k7c6	v
Bogotě	Bogota	k1gFnSc6	Bogota
jménem	jméno	k1gNnSc7	jméno
Alonso	Alonsa	k1gFnSc5	Alonsa
Vásquez	Vásqueza	k1gFnPc2	Vásqueza
de	de	k?	de
Cisneros	Cisnerosa	k1gFnPc2	Cisnerosa
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
za	za	k7c2	za
podpory	podpora	k1gFnSc2	podpora
vojenských	vojenský	k2eAgInPc2d1	vojenský
oddílů	oddíl	k1gInPc2	oddíl
vedených	vedený	k2eAgFnPc2d1	vedená
Diegem	Dieg	k1gInSc7	Dieg
de	de	k?	de
Herrera	Herrera	k1gFnSc1	Herrera
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
středisko	středisko	k1gNnSc4	středisko
obchodu	obchod	k1gInSc2	obchod
mezi	mezi	k7c7	mezi
Španěly	Španěl	k1gMnPc7	Španěl
a	a	k8xC	a
indiánskými	indiánský	k2eAgInPc7d1	indiánský
kmeny	kmen	k1gInPc7	kmen
Nocaima	Nocaimum	k1gNnSc2	Nocaimum
<g/>
,	,	kIx,	,
Chapaima	Chapaim	k1gMnSc4	Chapaim
<g/>
,	,	kIx,	,
Ubaima	Ubaim	k1gMnSc4	Ubaim
<g/>
,	,	kIx,	,
Calamoima	Calamoim	k1gMnSc4	Calamoim
<g/>
,	,	kIx,	,
Pinzaima	Pinzaim	k1gMnSc4	Pinzaim
<g/>
,	,	kIx,	,
Nimima	Nimim	k1gMnSc4	Nimim
a	a	k8xC	a
Chipcha	Chipch	k1gMnSc4	Chipch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1732	[number]	k4	1732
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
ustanovena	ustanoven	k2eAgFnSc1d1	ustanovena
farní	farní	k2eAgFnSc1d1	farní
obec	obec	k1gFnSc1	obec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
prudkému	prudký	k2eAgInSc3d1	prudký
rozvoji	rozvoj	k1gInSc3	rozvoj
těžby	těžba	k1gFnSc2	těžba
sulfidů	sulfid	k1gInPc2	sulfid
mědi	měď	k1gFnSc2	měď
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
dochází	docházet	k5eAaImIp3nS	docházet
i	i	k9	i
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
drobných	drobný	k2eAgFnPc2d1	drobná
hutí	huť	k1gFnPc2	huť
s	s	k7c7	s
bezproblémovým	bezproblémový	k2eAgInSc7d1	bezproblémový
odbytem	odbyt	k1gInSc7	odbyt
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
zvonů	zvon	k1gInPc2	zvon
a	a	k8xC	a
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
Doly	dol	k1gInPc1	dol
pokrývaly	pokrývat	k5eAaImAgFnP	pokrývat
velké	velká	k1gFnPc1	velká
území	území	k1gNnSc2	území
a	a	k8xC	a
nacházely	nacházet	k5eAaImAgFnP	nacházet
se	se	k3xPyFc4	se
ve	v	k7c6	v
vesnicích	vesnice	k1gFnPc6	vesnice
Cocunche	Cocunche	k1gInSc1	Cocunche
<g/>
,	,	kIx,	,
La	la	k1gNnPc1	la
Florida	Florida	k1gFnSc1	Florida
<g/>
,	,	kIx,	,
El	Ela	k1gFnPc2	Ela
Fical	Fical	k1gInSc1	Fical
<g/>
,	,	kIx,	,
San	San	k1gFnSc1	San
Jose	Jose	k1gFnSc1	Jose
<g/>
,	,	kIx,	,
Boquero	Boquero	k1gNnSc1	Boquero
a	a	k8xC	a
las	laso	k1gNnPc2	laso
Mercedes	mercedes	k1gInSc1	mercedes
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výrazně	výrazně	k6eAd1	výrazně
do	do	k7c2	do
dějin	dějiny	k1gFnPc2	dějiny
Kolumbie	Kolumbie	k1gFnSc2	Kolumbie
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
Nocaima	Nocaima	k1gFnSc1	Nocaima
5	[number]	k4	5
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1899	[number]	k4	1899
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zde	zde	k6eAd1	zde
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
významné	významný	k2eAgFnSc3d1	významná
bitvě	bitva	k1gFnSc3	bitva
mezi	mezi	k7c7	mezi
revolučními	revoluční	k2eAgFnPc7d1	revoluční
silami	síla	k1gFnPc7	síla
generála	generál	k1gMnSc2	generál
Zenona	Zenon	k1gMnSc2	Zenon
Figuereda	Figuered	k1gMnSc2	Figuered
a	a	k8xC	a
konzervativci	konzervativec	k1gMnPc1	konzervativec
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
generála	generál	k1gMnSc2	generál
Atanasia	Atanasius	k1gMnSc2	Atanasius
Martineze	Martineze	k1gFnSc2	Martineze
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stavba	stavba	k1gFnSc1	stavba
současného	současný	k2eAgInSc2d1	současný
kostela	kostel	k1gInSc2	kostel
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
del	del	k?	del
Rosario	Rosario	k6eAd1	Rosario
de	de	k?	de
Chiquinquirá	Chiquinquirý	k2eAgFnSc1d1	Chiquinquirý
byla	být	k5eAaImAgFnS	být
zahájena	zahájen	k2eAgFnSc1d1	zahájena
3	[number]	k4	3
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1934	[number]	k4	1934
farním	farní	k2eAgInSc7d1	farní
knězem	kněz	k1gMnSc7	kněz
jménem	jméno	k1gNnSc7	jméno
Juan	Juan	k1gMnSc1	Juan
Antonio	Antonio	k1gMnSc1	Antonio
Garzón	Garzón	k1gMnSc1	Garzón
de	de	k?	de
la	la	k1gNnPc2	la
Torre	torr	k1gInSc5	torr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hospodářství	hospodářství	k1gNnSc1	hospodářství
==	==	k?	==
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
hospodářskou	hospodářský	k2eAgFnSc7d1	hospodářská
aktivitou	aktivita	k1gFnSc7	aktivita
v	v	k7c6	v
Nocaimě	Nocaima	k1gFnSc6	Nocaima
je	být	k5eAaImIp3nS	být
pěstování	pěstování	k1gNnSc1	pěstování
cukrové	cukrový	k2eAgFnSc2d1	cukrová
třtiny	třtina	k1gFnSc2	třtina
a	a	k8xC	a
její	její	k3xOp3gNnSc4	její
domácí	domácí	k2eAgNnSc4d1	domácí
zpracování	zpracování	k1gNnSc4	zpracování
do	do	k7c2	do
pětikilových	pětikilový	k2eAgInPc2d1	pětikilový
kvádrů	kvádr	k1gInPc2	kvádr
polotovaru	polotovar	k1gInSc2	polotovar
zvaného	zvaný	k2eAgMnSc2d1	zvaný
panela	panel	k1gMnSc2	panel
<g/>
.	.	kIx.	.
</s>
<s>
Starý	starý	k2eAgInSc1d1	starý
řemeslný	řemeslný	k2eAgInSc1d1	řemeslný
proces	proces	k1gInSc1	proces
výroby	výroba	k1gFnPc1	výroba
panely	panel	k1gInPc1	panel
je	být	k5eAaImIp3nS	být
prováděn	prováděn	k2eAgInSc1d1	prováděn
především	především	k9	především
o	o	k7c6	o
víkendech	víkend	k1gInPc6	víkend
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
obci	obec	k1gFnSc6	obec
<g/>
.	.	kIx.	.
</s>
<s>
Polotovar	polotovar	k1gInSc1	polotovar
je	být	k5eAaImIp3nS	být
dále	daleko	k6eAd2	daleko
odvážen	odvážet	k5eAaImNgInS	odvážet
do	do	k7c2	do
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
výrazném	výrazný	k2eAgNnSc6d1	výrazné
množství	množství	k1gNnSc6	množství
se	se	k3xPyFc4	se
produkuje	produkovat	k5eAaImIp3nS	produkovat
ovoce	ovoce	k1gNnSc1	ovoce
<g/>
,	,	kIx,	,
především	především	k9	především
banány	banán	k1gInPc1	banán
<g/>
,	,	kIx,	,
ananas	ananas	k1gInSc1	ananas
a	a	k8xC	a
mango	mango	k1gNnSc1	mango
<g/>
.	.	kIx.	.
</s>
<s>
Produkce	produkce	k1gFnSc1	produkce
z	z	k7c2	z
kávových	kávový	k2eAgFnPc2d1	kávová
plantáží	plantáž	k1gFnPc2	plantáž
zásobuje	zásobovat	k5eAaImIp3nS	zásobovat
světoznámého	světoznámý	k2eAgMnSc4d1	světoznámý
výrobce	výrobce	k1gMnSc4	výrobce
kávy	káva	k1gFnSc2	káva
Juan	Juan	k1gMnSc1	Juan
Valdéz	Valdéza	k1gFnPc2	Valdéza
v	v	k7c6	v
Bogotě	Bogota	k1gFnSc6	Bogota
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Turistika	turistika	k1gFnSc1	turistika
==	==	k?	==
</s>
</p>
<p>
<s>
Přínosy	přínos	k1gInPc1	přínos
z	z	k7c2	z
turistického	turistický	k2eAgInSc2d1	turistický
ruchu	ruch	k1gInSc2	ruch
se	se	k3xPyFc4	se
neustále	neustále	k6eAd1	neustále
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
<g/>
.	.	kIx.	.
</s>
<s>
Blízkost	blízkost	k1gFnSc1	blízkost
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
činí	činit	k5eAaImIp3nS	činit
z	z	k7c2	z
Nocaimy	Nocaima	k1gFnSc2	Nocaima
oblíbené	oblíbený	k2eAgFnSc2d1	oblíbená
místo	místo	k1gNnSc4	místo
pro	pro	k7c4	pro
pořádání	pořádání	k1gNnSc4	pořádání
oslav	oslava	k1gFnPc2	oslava
<g/>
,	,	kIx,	,
konferencí	konference	k1gFnPc2	konference
i	i	k8xC	i
festivalů	festival	k1gInPc2	festival
<g/>
.	.	kIx.	.
</s>
<s>
Populární	populární	k2eAgMnPc1d1	populární
jsou	být	k5eAaImIp3nP	být
obchodní	obchodní	k2eAgFnPc4d1	obchodní
výstavy	výstava	k1gFnPc4	výstava
koní	kůň	k1gMnPc2	kůň
<g/>
,	,	kIx,	,
chovaných	chovaný	k2eAgMnPc2d1	chovaný
pro	pro	k7c4	pro
výcvik	výcvik	k1gInSc4	výcvik
charakteristického	charakteristický	k2eAgInSc2d1	charakteristický
kolumbijského	kolumbijský	k2eAgInSc2d1	kolumbijský
klusu	klus	k1gInSc2	klus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
==	==	k?	==
</s>
</p>
<p>
<s>
Nocaima	Nocaima	k1gFnSc1	Nocaima
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
pedagogické	pedagogický	k2eAgNnSc4d1	pedagogické
centrum	centrum	k1gNnSc4	centrum
Kolumbie	Kolumbie	k1gFnSc2	Kolumbie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
zde	zde	k6eAd1	zde
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
Pedagogický	pedagogický	k2eAgInSc1d1	pedagogický
institut	institut	k1gInSc1	institut
se	s	k7c7	s
speciální	speciální	k2eAgFnSc7d1	speciální
čtvrtí	čtvrt	k1gFnSc7	čtvrt
pro	pro	k7c4	pro
studenty	student	k1gMnPc4	student
<g/>
,	,	kIx,	,
pedagogy	pedagog	k1gMnPc4	pedagog
institutu	institut	k1gInSc2	institut
i	i	k9	i
absolventy	absolvent	k1gMnPc4	absolvent
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
učitelů	učitel	k1gMnPc2	učitel
v	v	k7c6	v
osmimiliónové	osmimiliónový	k2eAgFnSc6d1	osmimiliónový
Bogotě	Bogota	k1gFnSc6	Bogota
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Nocaimy	Nocaima	k1gFnSc2	Nocaima
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
učitelů	učitel	k1gMnPc2	učitel
zde	zde	k6eAd1	zde
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
"	"	kIx"	"
<g/>
Učitelském	učitelský	k2eAgNnSc6d1	učitelské
městečku	městečko	k1gNnSc6	městečko
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
do	do	k7c2	do
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
dojíždí	dojíždět	k5eAaImIp3nS	dojíždět
ve	v	k7c4	v
výukové	výukový	k2eAgInPc4d1	výukový
dny	den	k1gInPc4	den
a	a	k8xC	a
na	na	k7c4	na
víkend	víkend	k1gInSc4	víkend
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
do	do	k7c2	do
Nocaimy	Nocaima	k1gFnSc2	Nocaima
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Tradice	tradice	k1gFnSc1	tradice
==	==	k?	==
</s>
</p>
<p>
<s>
Každoročně	každoročně	k6eAd1	každoročně
3	[number]	k4	3
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
a	a	k8xC	a
5	[number]	k4	5
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
se	se	k3xPyFc4	se
ve	v	k7c6	v
městě	město	k1gNnSc6	město
pořádají	pořádat	k5eAaImIp3nP	pořádat
oslavy	oslava	k1gFnPc4	oslava
ke	k	k7c3	k
dni	den	k1gInSc3	den
založení	založení	k1gNnSc2	založení
Nocaimy	Nocaima	k1gFnSc2	Nocaima
a	a	k8xC	a
ke	k	k7c3	k
dni	den	k1gInSc3	den
bitvy	bitva	k1gFnSc2	bitva
u	u	k7c2	u
Nocaimy	Nocaima	k1gFnSc2	Nocaima
<g/>
,	,	kIx,	,
vrcholící	vrcholící	k2eAgInSc1d1	vrcholící
volbou	volba	k1gFnSc7	volba
Miss	miss	k1gFnSc1	miss
Nocaima	Nocaima	k1gFnSc1	Nocaima
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
se	se	k3xPyFc4	se
pořádá	pořádat	k5eAaImIp3nS	pořádat
průvod	průvod	k1gInSc1	průvod
Učitelským	učitelský	k2eAgNnSc7d1	učitelské
městečkem	městečko	k1gNnSc7	městečko
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
jeho	jeho	k3xOp3gNnSc2	jeho
založení	založení	k1gNnSc2	založení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velké	velký	k2eAgFnSc3d1	velká
popularitě	popularita	k1gFnSc3	popularita
se	se	k3xPyFc4	se
těší	těšit	k5eAaImIp3nS	těšit
každoroční	každoroční	k2eAgFnSc2d1	každoroční
cesty	cesta	k1gFnSc2	cesta
motocyklistů	motocyklista	k1gMnPc2	motocyklista
na	na	k7c6	na
trase	trasa	k1gFnSc6	trasa
Bogota	Bogot	k1gMnSc2	Bogot
–	–	k?	–
Nocaima	Nocaim	k1gMnSc2	Nocaim
<g/>
,	,	kIx,	,
při	při	k7c6	při
nichž	jenž	k3xRgNnPc6	jenž
za	za	k7c7	za
účasti	účast	k1gFnSc3	účast
všech	všecek	k3xTgFnPc2	všecek
věkových	věkový	k2eAgFnPc2d1	věková
skupin	skupina	k1gFnPc2	skupina
v	v	k7c6	v
nejširším	široký	k2eAgInSc6d3	nejširší
možném	možný	k2eAgInSc6d1	možný
rozsahu	rozsah	k1gInSc6	rozsah
kubatur	kubatura	k1gFnPc2	kubatura
vznikají	vznikat	k5eAaImIp3nP	vznikat
až	až	k9	až
několikakilometrové	několikakilometrový	k2eAgFnPc1d1	několikakilometrová
kolony	kolona	k1gFnPc1	kolona
jednostopých	jednostopý	k2eAgNnPc2d1	jednostopé
vozidel	vozidlo	k1gNnPc2	vozidlo
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
členů	člen	k1gInPc2	člen
tohoto	tento	k3xDgInSc2	tento
peletonu	peleton	k1gInSc2	peleton
navštěvuje	navštěvovat	k5eAaImIp3nS	navštěvovat
své	svůj	k3xOyFgMnPc4	svůj
oblíbené	oblíbený	k2eAgMnPc4d1	oblíbený
učitele	učitel	k1gMnPc4	učitel
ze	z	k7c2	z
základních	základní	k2eAgFnPc2d1	základní
škol	škola	k1gFnPc2	škola
v	v	k7c6	v
Učitelském	učitelský	k2eAgNnSc6d1	učitelské
městečku	městečko	k1gNnSc6	městečko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
má	mít	k5eAaImIp3nS	mít
Nocaima	Nocaima	k1gFnSc1	Nocaima
vlastní	vlastní	k2eAgFnSc4d1	vlastní
hymnu	hymna	k1gFnSc4	hymna
na	na	k7c4	na
hudbu	hudba	k1gFnSc4	hudba
i	i	k8xC	i
slova	slovo	k1gNnSc2	slovo
Huga	Hugo	k1gMnSc4	Hugo
Edilberta	Edilbert	k1gMnSc4	Edilbert
Ramose	Ramosa	k1gFnSc6	Ramosa
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ve	v	k7c6	v
čtyřech	čtyři	k4xCgFnPc6	čtyři
slokách	sloka	k1gFnPc6	sloka
dokonale	dokonale	k6eAd1	dokonale
popisuje	popisovat	k5eAaImIp3nS	popisovat
historii	historie	k1gFnSc4	historie
<g/>
,	,	kIx,	,
přítomnost	přítomnost	k1gFnSc4	přítomnost
i	i	k8xC	i
budoucnost	budoucnost	k1gFnSc4	budoucnost
Nocaimy	Nocaima	k1gFnSc2	Nocaima
<g/>
.	.	kIx.	.
</s>
<s>
Refrén	refrén	k1gInSc1	refrén
opěvuje	opěvovat	k5eAaImIp3nS	opěvovat
přírodu	příroda	k1gFnSc4	příroda
<g/>
,	,	kIx,	,
mír	mír	k1gInSc4	mír
a	a	k8xC	a
lásku	láska	k1gFnSc4	láska
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
sloka	sloka	k1gFnSc1	sloka
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
zakladatele	zakladatel	k1gMnSc2	zakladatel
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
sloka	sloka	k1gFnSc1	sloka
opěvuje	opěvovat	k5eAaImIp3nS	opěvovat
výrobu	výroba	k1gFnSc4	výroba
cukru	cukr	k1gInSc2	cukr
a	a	k8xC	a
těžbu	těžba	k1gFnSc4	těžba
mědi	měď	k1gFnSc2	měď
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgFnSc1	třetí
sloka	sloka	k1gFnSc1	sloka
připomíná	připomínat	k5eAaImIp3nS	připomínat
čest	čest	k1gFnSc4	čest
a	a	k8xC	a
slávu	sláva	k1gFnSc4	sláva
kolumbijských	kolumbijský	k2eAgMnPc2d1	kolumbijský
učitelů	učitel	k1gMnPc2	učitel
a	a	k8xC	a
čtvrtá	čtvrtý	k4xOgFnSc1	čtvrtý
sloka	sloka	k1gFnSc1	sloka
připomíná	připomínat	k5eAaImIp3nS	připomínat
plantáže	plantáž	k1gFnPc4	plantáž
cukrové	cukrový	k2eAgFnSc2d1	cukrová
třtiny	třtina	k1gFnSc2	třtina
jako	jako	k8xS	jako
hlavní	hlavní	k2eAgInSc4d1	hlavní
zdroj	zdroj	k1gInSc4	zdroj
obživy	obživa	k1gFnSc2	obživa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
F.	F.	kA	F.
Mayorga	Mayorga	k1gFnSc1	Mayorga
García	García	k1gFnSc1	García
<g/>
:	:	kIx,	:
La	la	k1gNnSc1	la
Audiencia	Audiencius	k1gMnSc2	Audiencius
de	de	k?	de
Santafé	Santafá	k1gFnSc2	Santafá
en	en	k?	en
los	los	k1gInSc1	los
siglos	siglos	k1gMnSc1	siglos
XVI	XVI	kA	XVI
y	y	k?	y
XVII	XVII	kA	XVII
<g/>
,	,	kIx,	,
Bogotá	Bogotá	k1gFnSc1	Bogotá
<g/>
,	,	kIx,	,
Instituto	Institut	k2eAgNnSc1d1	Instituto
Colombiano	Colombiana	k1gFnSc5	Colombiana
de	de	k?	de
Cultura	Cultura	k1gFnSc1	Cultura
Hispánica	Hispánica	k1gFnSc1	Hispánica
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
50	[number]	k4	50
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
G.	G.	kA	G.
Lohmann	Lohmann	k1gInSc1	Lohmann
Villena	Villena	k1gFnSc1	Villena
<g/>
,	,	kIx,	,
<g/>
:	:	kIx,	:
<g/>
Los	los	k1gInSc1	los
americanos	americanos	k1gMnSc1	americanos
en	en	k?	en
las	laso	k1gNnPc2	laso
órdenes	órdenes	k1gMnSc1	órdenes
nobiliarias	nobiliarias	k1gMnSc1	nobiliarias
(	(	kIx(	(
<g/>
1529	[number]	k4	1529
<g/>
-	-	kIx~	-
<g/>
1900	[number]	k4	1900
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bogotá	Bogotá	k1gFnSc1	Bogotá
<g/>
,	,	kIx,	,
Instituto	Institut	k2eAgNnSc1d1	Instituto
Colombiano	Colombiana	k1gFnSc5	Colombiana
de	de	k?	de
Cultura	Cultura	k1gFnSc1	Cultura
Hispánica	Hispánica	k1gFnSc1	Hispánica
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
145	[number]	k4	145
stran	strana	k1gFnPc2	strana
</s>
</p>
<p>
<s>
El	Ela	k1gFnPc2	Ela
juez	juez	k1gMnSc1	juez
visitador	visitador	k1gMnSc1	visitador
<g/>
,	,	kIx,	,
Alonso	Alonsa	k1gFnSc5	Alonsa
Vásquez	Vásqueza	k1gFnPc2	Vásqueza
de	de	k?	de
Cisneros	Cisnerosa	k1gFnPc2	Cisnerosa
<g/>
,	,	kIx,	,
Ediciones	Edicionesa	k1gFnPc2	Edicionesa
del	del	k?	del
Centro	Centro	k1gNnSc4	Centro
de	de	k?	de
Historia	Historium	k1gNnSc2	Historium
del	del	k?	del
Estado	Estada	k1gFnSc5	Estada
Trujillo	Trujillo	k1gNnSc1	Trujillo
<g/>
,	,	kIx,	,
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
zpráva	zpráva	k1gFnSc1	zpráva
31	[number]	k4	31
stran	strana	k1gFnPc2	strana
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Nocaima	Nocaimum	k1gNnSc2	Nocaimum
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
