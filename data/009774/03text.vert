<p>
<s>
Matthew	Matthew	k?	Matthew
Webb	Webb	k1gInSc1	Webb
(	(	kIx(	(
<g/>
*	*	kIx~	*
19	[number]	k4	19
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1848	[number]	k4	1848
Dawley	Dawlea	k1gFnSc2	Dawlea
<g/>
,	,	kIx,	,
Shropshire	Shropshir	k1gInSc5	Shropshir
–	–	k?	–
24	[number]	k4	24
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1883	[number]	k4	1883
Niagara	Niagara	k1gFnSc1	Niagara
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
anglický	anglický	k2eAgMnSc1d1	anglický
plavec	plavec	k1gMnSc1	plavec
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
jako	jako	k8xS	jako
první	první	k4xOgMnSc1	první
člověk	člověk	k1gMnSc1	člověk
překonal	překonat	k5eAaPmAgMnS	překonat
před	před	k7c7	před
svědky	svědek	k1gMnPc7	svědek
vlastními	vlastní	k2eAgInPc7d1	vlastní
silami	síla	k1gFnPc7	síla
Lamanšský	lamanšský	k2eAgInSc4d1	lamanšský
průliv	průliv	k1gInSc4	průliv
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
také	také	k9	také
členem	člen	k1gMnSc7	člen
zednářské	zednářský	k2eAgFnSc2d1	zednářská
lóže	lóže	k1gFnSc2	lóže
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
sedmi	sedm	k4xCc2	sedm
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
učil	učít	k5eAaPmAgInS	učít
plavat	plavat	k5eAaImF	plavat
v	v	k7c6	v
řece	řeka	k1gFnSc6	řeka
Severn	Severna	k1gFnPc2	Severna
<g/>
,	,	kIx,	,
ve	v	k7c6	v
dvanácti	dvanáct	k4xCc6	dvanáct
letech	léto	k1gNnPc6	léto
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
k	k	k7c3	k
britskému	britský	k2eAgNnSc3d1	Britské
obchodnímu	obchodní	k2eAgNnSc3d1	obchodní
námořnictvu	námořnictvo	k1gNnSc3	námořnictvo
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1873	[number]	k4	1873
sloužil	sloužit	k5eAaImAgMnS	sloužit
jako	jako	k9	jako
lodní	lodní	k2eAgMnSc1d1	lodní
kapitán	kapitán	k1gMnSc1	kapitán
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
hrdinství	hrdinství	k1gNnSc4	hrdinství
prokázané	prokázaný	k2eAgNnSc4d1	prokázané
při	při	k7c6	při
pokusu	pokus	k1gInSc6	pokus
o	o	k7c4	o
záchranu	záchrana	k1gFnSc4	záchrana
tonoucího	tonoucí	k1gMnSc2	tonoucí
obdržel	obdržet	k5eAaPmAgMnS	obdržet
Stanhope	Stanhop	k1gInSc5	Stanhop
Medal	Medal	k1gInSc1	Medal
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1875	[number]	k4	1875
uplaval	uplavat	k5eAaPmAgMnS	uplavat
po	po	k7c6	po
Temži	Temže	k1gFnSc6	Temže
osmnáct	osmnáct	k4xCc4	osmnáct
mil	míle	k1gFnPc2	míle
z	z	k7c2	z
Londýna	Londýn	k1gInSc2	Londýn
do	do	k7c2	do
Gravesendu	Gravesend	k1gInSc2	Gravesend
v	v	k7c6	v
rekordním	rekordní	k2eAgInSc6d1	rekordní
čase	čas	k1gInSc6	čas
4	[number]	k4	4
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
52	[number]	k4	52
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
první	první	k4xOgInSc1	první
pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
zdolání	zdolání	k1gNnSc4	zdolání
Lamanšského	lamanšský	k2eAgInSc2d1	lamanšský
průlivu	průliv	k1gInSc2	průliv
12	[number]	k4	12
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1875	[number]	k4	1875
byl	být	k5eAaImAgInS	být
neúspěšný	úspěšný	k2eNgInSc1d1	neúspěšný
kvůli	kvůli	k7c3	kvůli
rozbouřenému	rozbouřený	k2eAgNnSc3d1	rozbouřené
moři	moře	k1gNnSc3	moře
<g/>
,	,	kIx,	,
teprve	teprve	k6eAd1	teprve
ve	v	k7c6	v
dnech	den	k1gInPc6	den
24	[number]	k4	24
<g/>
.	.	kIx.	.
až	až	k9	až
25	[number]	k4	25
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
za	za	k7c4	za
21	[number]	k4	21
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
45	[number]	k4	45
minut	minuta	k1gFnPc2	minuta
doplavat	doplavat	k5eAaPmF	doplavat
z	z	k7c2	z
Doveru	Dover	k1gInSc2	Dover
do	do	k7c2	do
Calais	Calais	k1gNnSc2	Calais
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
urazil	urazit	k5eAaPmAgInS	urazit
64	[number]	k4	64
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
úspěchu	úspěch	k1gInSc6	úspěch
odešel	odejít	k5eAaPmAgMnS	odejít
od	od	k7c2	od
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
a	a	k8xC	a
živil	živit	k5eAaImAgMnS	živit
se	se	k3xPyFc4	se
účastí	účast	k1gFnSc7	účast
na	na	k7c6	na
plaveckých	plavecký	k2eAgFnPc6d1	plavecká
exhibicích	exhibice	k1gFnPc6	exhibice
<g/>
,	,	kIx,	,
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
závod	závod	k1gInSc4	závod
World	World	k1gMnSc1	World
Championship	Championship	k1gMnSc1	Championship
Race	Race	k1gFnSc4	Race
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
Nantasket	Nantasket	k1gInSc1	Nantasket
Beach	Beach	k1gInSc1	Beach
<g/>
.	.	kIx.	.
</s>
<s>
Vydal	vydat	k5eAaPmAgMnS	vydat
také	také	k9	také
úspěšnou	úspěšný	k2eAgFnSc4d1	úspěšná
knihu	kniha	k1gFnSc4	kniha
Plavecké	plavecký	k2eAgNnSc1d1	plavecké
umění	umění	k1gNnSc1	umění
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
třiceti	třicet	k4xCc2	třicet
pěti	pět	k4xCc3	pět
let	léto	k1gNnPc2	léto
při	při	k7c6	při
pokusu	pokus	k1gInSc6	pokus
přeplavat	přeplavat	k5eAaPmF	přeplavat
peřeje	peřej	k1gFnPc4	peřej
pod	pod	k7c7	pod
Niagarskými	niagarský	k2eAgInPc7d1	niagarský
vodopády	vodopád	k1gInPc7	vodopád
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rodném	rodný	k2eAgInSc6d1	rodný
Dawley	Dawley	k1gInPc4	Dawley
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgMnS	být
odhalen	odhalen	k2eAgInSc4d1	odhalen
pomník	pomník	k1gInSc4	pomník
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Betjeman	Betjeman	k1gMnSc1	Betjeman
mu	on	k3xPp3gMnSc3	on
věnoval	věnovat	k5eAaPmAgMnS	věnovat
báseň	báseň	k1gFnSc4	báseň
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
také	také	k9	také
uveden	uvést	k5eAaPmNgInS	uvést
do	do	k7c2	do
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
plavecké	plavecký	k2eAgFnSc2d1	plavecká
síně	síň	k1gFnSc2	síň
slávy	sláva	k1gFnSc2	sláva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Matthew	Matthew	k1gFnSc2	Matthew
Webb	Webb	k1gInSc1	Webb
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Dálkové	dálkový	k2eAgNnSc1d1	dálkové
plavání	plavání	k1gNnSc1	plavání
</s>
</p>
