<s>
Hugo	Hugo	k1gMnSc1	Hugo
Toxxx	Toxxx	k1gInSc1	Toxxx
(	(	kIx(	(
<g/>
Hack	Hack	k1gMnSc1	Hack
<g/>
,	,	kIx,	,
Hacker	hacker	k1gMnSc1	hacker
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
rodným	rodný	k2eAgNnSc7d1	rodné
jménem	jméno	k1gNnSc7	jméno
Jan	Jan	k1gMnSc1	Jan
Daněk	Daněk	k1gMnSc1	Daněk
(	(	kIx(	(
<g/>
*	*	kIx~	*
16	[number]	k4	16
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1982	[number]	k4	1982
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
rapper	rapper	k1gMnSc1	rapper
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
labelu	label	k1gInSc2	label
Bigg	Bigg	k1gMnSc1	Bigg
Boss	boss	k1gMnSc1	boss
a	a	k8xC	a
zakladatel	zakladatel	k1gMnSc1	zakladatel
labelu	labela	k1gFnSc4	labela
Hypno	Hypno	k6eAd1	Hypno
808	[number]	k4	808
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Jamesem	James	k1gMnSc7	James
Colem	Col	k1gMnSc7	Col
(	(	kIx(	(
<g/>
Phat	Phat	k1gMnSc1	Phat
<g/>
)	)	kIx)	)
založili	založit	k5eAaPmAgMnP	založit
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
K.O.	K.O.	k1gFnPc2	K.O.
Kru	kra	k1gFnSc4	kra
a	a	k8xC	a
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
legendární	legendární	k2eAgMnSc1d1	legendární
Supercrooo	Supercrooo	k1gMnSc1	Supercrooo
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
také	také	k9	také
členem	člen	k1gInSc7	člen
projektu	projekt	k1gInSc2	projekt
Dixxx	Dixxx	k1gInSc1	Dixxx
(	(	kIx(	(
<g/>
James	James	k1gInSc1	James
Cole	cola	k1gFnSc3	cola
<g/>
,	,	kIx,	,
Hugo	Hugo	k1gMnSc1	Hugo
Toxxx	Toxxx	k1gInSc1	Toxxx
<g/>
,	,	kIx,	,
Risto	Risto	k1gNnSc1	Risto
<g/>
,	,	kIx,	,
Lucas	Lucas	k1gMnSc1	Lucas
Skunkwalker	Skunkwalker	k1gMnSc1	Skunkwalker
<g/>
)	)	kIx)	)
Rok	rok	k1gInSc1	rok
psa	pes	k1gMnSc2	pes
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
Legální	legální	k2eAgFnSc2d1	legální
drogy	droga	k1gFnSc2	droga
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Ilegální	ilegální	k2eAgInPc1d1	ilegální
kecy	kec	k1gInPc1	kec
Mixtape	Mixtap	k1gInSc5	Mixtap
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Bauch	Bauch	k1gInSc1	Bauch
Money	Monea	k1gFnSc2	Monea
Mixtape	Mixtap	k1gInSc5	Mixtap
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
Tra	tra	k0	tra
<g/>
$	$	kIx~	$
<g/>
h	h	k?	h
Rap	rap	k1gMnSc1	rap
Mixtape	Mixtap	k1gInSc5	Mixtap
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
založil	založit	k5eAaPmAgInS	založit
label	label	k1gInSc1	label
Hypno	Hypno	k1gNnSc1	Hypno
808	[number]	k4	808
kam	kam	k6eAd1	kam
přijal	přijmout	k5eAaPmAgInS	přijmout
rappery	rappera	k1gFnSc2	rappera
Marata	Marat	k1gMnSc2	Marat
a	a	k8xC	a
Igora	Igor	k1gMnSc2	Igor
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
vydané	vydaný	k2eAgNnSc1d1	vydané
album	album	k1gNnSc1	album
pod	pod	k7c7	pod
tímto	tento	k3xDgInSc7	tento
labelem	label	k1gInSc7	label
bylo	být	k5eAaImAgNnS	být
double	double	k2eAgNnSc1d1	double
CD	CD	kA	CD
s	s	k7c7	s
názvem	název	k1gInSc7	název
Legální	legální	k2eAgFnSc2d1	legální
drogy	droga	k1gFnSc2	droga
/	/	kIx~	/
Ilegální	ilegální	k2eAgInPc1d1	ilegální
kecy	kec	k1gInPc1	kec
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Filmografie	filmografie	k1gFnSc1	filmografie
<g/>
:	:	kIx,	:
Česká	český	k2eAgFnSc1d1	Česká
RAPublika	RAPublika	k1gFnSc1	RAPublika
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
20	[number]	k4	20
<g/>
ers	ers	k?	ers
-	-	kIx~	-
Making	Making	k1gInSc1	Making
Of	Of	k1gFnSc2	Of
<g/>
...	...	k?	...
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
Dokument	dokument	k1gInSc1	dokument
Pena	Pen	k1gInSc2	Pen
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Paskvil	paskvil	k1gInSc1	paskvil
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
T-music	Tusice	k1gFnPc2	T-musice
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
Co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
děje	dít	k5eAaImIp3nS	dít
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
</s>
<s>
Mezi	mezi	k7c7	mezi
hosty	host	k1gMnPc7	host
alba	album	k1gNnSc2	album
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
nakonec	nakonec	k6eAd1	nakonec
19	[number]	k4	19
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
na	na	k7c6	na
labelu	label	k1gInSc6	label
Bigg	Bigg	k1gMnSc1	Bigg
Boss	boss	k1gMnSc1	boss
se	se	k3xPyFc4	se
objevili	objevit	k5eAaPmAgMnP	objevit
mimo	mimo	k7c4	mimo
Jamese	Jamese	k1gFnPc4	Jamese
například	například	k6eAd1	například
Vladimír	Vladimíra	k1gFnPc2	Vladimíra
518	[number]	k4	518
<g/>
,	,	kIx,	,
Dara	Dara	k1gFnSc1	Dara
Rolins	Rolinsa	k1gFnPc2	Rolinsa
nebo	nebo	k8xC	nebo
Vec	Vec	k1gFnPc2	Vec
a	a	k8xC	a
beaty	beat	k1gInPc4	beat
přispěli	přispět	k5eAaPmAgMnP	přispět
samotný	samotný	k2eAgMnSc1d1	samotný
Hugo	Hugo	k1gMnSc1	Hugo
<g/>
,	,	kIx,	,
DJ	DJ	kA	DJ
Wich	Wich	k1gMnSc1	Wich
<g/>
,	,	kIx,	,
Marko	Marko	k1gMnSc1	Marko
nebo	nebo	k8xC	nebo
Mike	Mike	k1gFnSc1	Mike
T.	T.	kA	T.
</s>
