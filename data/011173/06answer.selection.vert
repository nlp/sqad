<s>
Panslovanské	Panslovanský	k2eAgFnPc1d1	Panslovanský
barvy	barva	k1gFnPc1	barva
(	(	kIx(	(
<g/>
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
,	,	kIx,	,
modrá	modrý	k2eAgFnSc1d1	modrá
a	a	k8xC	a
červená	červený	k2eAgFnSc1d1	červená
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
stanoveny	stanovit	k5eAaPmNgFnP	stanovit
na	na	k7c6	na
všeslovanském	všeslovanský	k2eAgInSc6d1	všeslovanský
kongresu	kongres	k1gInSc6	kongres
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
a	a	k8xC	a
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
skutečně	skutečně	k6eAd1	skutečně
objevují	objevovat	k5eAaImIp3nP	objevovat
na	na	k7c6	na
vlajkách	vlajka	k1gFnPc6	vlajka
většiny	většina	k1gFnSc2	většina
států	stát	k1gInPc2	stát
s	s	k7c7	s
majoritou	majorita	k1gFnSc7	majorita
slovanskojazyčného	slovanskojazyčný	k2eAgNnSc2d1	slovanskojazyčný
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
