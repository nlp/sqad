<p>
<s>
Panslovanské	Panslovanský	k2eAgFnPc1d1	Panslovanský
barvy	barva	k1gFnPc1	barva
(	(	kIx(	(
<g/>
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
,	,	kIx,	,
modrá	modrý	k2eAgFnSc1d1	modrá
a	a	k8xC	a
červená	červený	k2eAgFnSc1d1	červená
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
stanoveny	stanovit	k5eAaPmNgFnP	stanovit
na	na	k7c6	na
všeslovanském	všeslovanský	k2eAgInSc6d1	všeslovanský
kongresu	kongres	k1gInSc6	kongres
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
a	a	k8xC	a
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
skutečně	skutečně	k6eAd1	skutečně
objevují	objevovat	k5eAaImIp3nP	objevovat
na	na	k7c6	na
vlajkách	vlajka	k1gFnPc6	vlajka
většiny	většina	k1gFnSc2	většina
států	stát	k1gInPc2	stát
s	s	k7c7	s
majoritou	majorita	k1gFnSc7	majorita
slovanskojazyčného	slovanskojazyčný	k2eAgNnSc2d1	slovanskojazyčný
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
byly	být	k5eAaImAgFnP	být
tyto	tento	k3xDgFnPc1	tento
barvy	barva	k1gFnPc1	barva
zvoleny	zvolen	k2eAgFnPc1d1	zvolena
ke	k	k7c3	k
zdůraznění	zdůraznění	k1gNnSc3	zdůraznění
sounáležitosti	sounáležitost	k1gFnSc2	sounáležitost
slovanských	slovanský	k2eAgInPc2d1	slovanský
národů	národ	k1gInPc2	národ
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
panslavismu	panslavismus	k1gInSc2	panslavismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Předobraz	předobraz	k1gInSc1	předobraz
panslovanských	panslovanský	k2eAgFnPc2d1	panslovanská
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
barvách	barva	k1gFnPc6	barva
ruských	ruský	k2eAgMnPc2d1	ruský
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
zavedl	zavést	k5eAaPmAgMnS	zavést
ruský	ruský	k2eAgMnSc1d1	ruský
car	car	k1gMnSc1	car
Petr	Petr	k1gMnSc1	Petr
I.	I.	kA	I.
Veliký	veliký	k2eAgMnSc1d1	veliký
<g/>
,	,	kIx,	,
snad	snad	k9	snad
odvozené	odvozený	k2eAgInPc1d1	odvozený
z	z	k7c2	z
vlajky	vlajka	k1gFnSc2	vlajka
Spojených	spojený	k2eAgFnPc2d1	spojená
nizozemských	nizozemský	k2eAgFnPc2d1	nizozemská
provincií	provincie	k1gFnPc2	provincie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
car	car	k1gMnSc1	car
strávil	strávit	k5eAaPmAgMnS	strávit
nějaký	nějaký	k3yIgInSc4	nějaký
čas	čas	k1gInSc4	čas
při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
cestě	cesta	k1gFnSc6	cesta
po	po	k7c6	po
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Stejná	stejný	k2eAgFnSc1d1	stejná
skladba	skladba	k1gFnSc1	skladba
barev	barva	k1gFnPc2	barva
jako	jako	k8xC	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
nizozemské	nizozemský	k2eAgFnSc2d1	nizozemská
vlajky	vlajka	k1gFnSc2	vlajka
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
náhodná	náhodný	k2eAgFnSc1d1	náhodná
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
se	se	k3xPyFc4	se
již	již	k6eAd1	již
předtím	předtím	k6eAd1	předtím
používaly	používat	k5eAaImAgFnP	používat
stejné	stejný	k2eAgFnPc1d1	stejná
barvy	barva	k1gFnPc1	barva
v	v	k7c6	v
jiném	jiný	k2eAgNnSc6d1	jiné
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přejímání	přejímání	k1gNnPc1	přejímání
barev	barva	k1gFnPc2	barva
==	==	k?	==
</s>
</p>
<p>
<s>
Jako	jako	k8xS	jako
první	první	k4xOgInSc4	první
národ	národ	k1gInSc4	národ
rozšířili	rozšířit	k5eAaPmAgMnP	rozšířit
roku	rok	k1gInSc2	rok
1835	[number]	k4	1835
Srbové	Srb	k1gMnPc1	Srb
svoji	svůj	k3xOyFgFnSc4	svůj
národní	národní	k2eAgFnSc4d1	národní
vlajku	vlajka	k1gFnSc4	vlajka
o	o	k7c4	o
bílou	bílý	k2eAgFnSc4d1	bílá
barvu	barva	k1gFnSc4	barva
(	(	kIx(	(
<g/>
na	na	k7c4	na
červenou	červená	k1gFnSc4	červená
<g/>
,	,	kIx,	,
modrou	modrý	k2eAgFnSc7d1	modrá
a	a	k8xC	a
bílou	bílý	k2eAgFnSc7d1	bílá
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
podob	podoba	k1gFnPc2	podoba
srbských	srbský	k2eAgFnPc2d1	Srbská
vlajek	vlajka	k1gFnPc2	vlajka
v	v	k7c6	v
barvách	barva	k1gFnPc6	barva
červené	červený	k2eAgFnSc2d1	červená
a	a	k8xC	a
modré	modrý	k2eAgFnSc2d1	modrá
je	být	k5eAaImIp3nS	být
doložena	doložen	k2eAgFnSc1d1	doložena
již	již	k6eAd1	již
ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Srbské	srbský	k2eAgNnSc4d1	srbské
království	království	k1gNnSc4	království
Černá	černý	k2eAgFnSc1d1	černá
Hora	hora	k1gFnSc1	hora
přejalo	přejmout	k5eAaPmAgNnS	přejmout
panslovanské	panslovanský	k2eAgFnPc4d1	panslovanská
barvy	barva	k1gFnPc4	barva
v	v	k7c6	v
srbském	srbský	k2eAgNnSc6d1	srbské
pořadí	pořadí	k1gNnSc6	pořadí
(	(	kIx(	(
<g/>
červená	červený	k2eAgFnSc1d1	červená
<g/>
,	,	kIx,	,
modrá	modrý	k2eAgFnSc1d1	modrá
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
)	)	kIx)	)
beze	beze	k7c2	beze
změny	změna	k1gFnSc2	změna
<g/>
.	.	kIx.	.
</s>
<s>
Slovinci	Slovinec	k1gMnPc1	Slovinec
převzali	převzít	k5eAaPmAgMnP	převzít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1867	[number]	k4	1867
barvy	barva	k1gFnSc2	barva
v	v	k7c6	v
původním	původní	k2eAgNnSc6d1	původní
ruském	ruský	k2eAgNnSc6d1	ruské
pořadí	pořadí	k1gNnSc6	pořadí
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
,	,	kIx,	,
modrá	modrý	k2eAgFnSc1d1	modrá
a	a	k8xC	a
červená	červený	k2eAgFnSc1d1	červená
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
následovaly	následovat	k5eAaImAgFnP	následovat
vlajky	vlajka	k1gFnPc1	vlajka
slovenská	slovenský	k2eAgFnSc1d1	slovenská
a	a	k8xC	a
česká	český	k2eAgFnSc1d1	Česká
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chorvaté	Chorvat	k1gMnPc1	Chorvat
sice	sice	k8xC	sice
roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
přijali	přijmout	k5eAaPmAgMnP	přijmout
červeno-bílo-modrou	červenoíloodrý	k2eAgFnSc4d1	červeno-bílo-modrá
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
v	v	k7c6	v
jiném	jiný	k2eAgMnSc6d1	jiný
<g/>
,	,	kIx,	,
než	než	k8xS	než
panslovanském	panslovanský	k2eAgNnSc6d1	panslovanský
pořadí	pořadí	k1gNnSc6	pořadí
barev	barva	k1gFnPc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Odráží	odrážet	k5eAaImIp3nP	odrážet
barvy	barva	k1gFnPc1	barva
znaků	znak	k1gInPc2	znak
Chorvatska	Chorvatsko	k1gNnSc2	Chorvatsko
<g/>
,	,	kIx,	,
Dalmácie	Dalmácie	k1gFnSc2	Dalmácie
a	a	k8xC	a
Slavonie	Slavonie	k1gFnSc2	Slavonie
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
historických	historický	k2eAgFnPc2d1	historická
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
dnešní	dnešní	k2eAgNnSc4d1	dnešní
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Tinktura	tinktura	k1gFnSc1	tinktura
==	==	k?	==
</s>
</p>
<p>
<s>
Ruské	ruský	k2eAgFnPc1d1	ruská
barvy	barva	k1gFnPc1	barva
a	a	k8xC	a
tedy	tedy	k9	tedy
i	i	k9	i
některé	některý	k3yIgFnPc1	některý
další	další	k2eAgFnPc1d1	další
panslovanské	panslovanský	k2eAgFnPc1d1	panslovanská
vlajky	vlajka	k1gFnPc1	vlajka
však	však	k9	však
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
prohřešek	prohřešek	k1gInSc4	prohřešek
proti	proti	k7c3	proti
heraldickým	heraldický	k2eAgNnPc3d1	heraldické
pravidlům	pravidlo	k1gNnPc3	pravidlo
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
nichž	jenž	k3xRgFnPc2	jenž
červená	červenat	k5eAaImIp3nS	červenat
a	a	k8xC	a
modrá	modrý	k2eAgFnSc1d1	modrá
nesmějí	smát	k5eNaImIp3nP	smát
ležet	ležet	k5eAaImF	ležet
vedle	vedle	k7c2	vedle
sebe	se	k3xPyFc2	se
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c6	o
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
barvy	barva	k1gFnSc2	barva
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
protiklad	protiklad	k1gInSc4	protiklad
k	k	k7c3	k
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
kovům	kov	k1gInPc3	kov
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bílá	bílý	k2eAgFnSc1d1	bílá
představuje	představovat	k5eAaImIp3nS	představovat
stříbro	stříbro	k1gNnSc4	stříbro
a	a	k8xC	a
žlutá	žlutat	k5eAaImIp3nS	žlutat
zlato	zlato	k1gNnSc1	zlato
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
vlajek	vlajka	k1gFnPc2	vlajka
==	==	k?	==
</s>
</p>
<p>
<s>
Historické	historický	k2eAgFnPc1d1	historická
vlajky	vlajka	k1gFnPc1	vlajka
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Panslawische	Panslawisch	k1gFnSc2	Panslawisch
Farben	Farbna	k1gFnPc2	Farbna
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Ruské	ruský	k2eAgFnSc2d1	ruská
federace	federace	k1gFnSc2	federace
na	na	k7c4	na
FOTW	FOTW	kA	FOTW
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
*	*	kIx~	*
Ж	Ж	k?	Ж
А	А	k?	А
С	С	k?	С
ц	ц	k?	ц
//	//	k?	//
Г	Г	k?	Г
<g/>
.	.	kIx.	.
–	–	k?	–
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
–	–	k?	–
No	no	k9	no
<g/>
2	[number]	k4	2
(	(	kIx(	(
<g/>
94	[number]	k4	94
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
–	–	k?	–
С	С	k?	С
24	[number]	k4	24
<g/>
-	-	kIx~	-
<g/>
29	[number]	k4	29
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
ukr	ukr	k?	ukr
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
