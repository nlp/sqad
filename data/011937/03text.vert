<p>
<s>
Kroj	kroj	k1gInSc1	kroj
<g/>
,	,	kIx,	,
také	také	k9	také
označován	označován	k2eAgInSc4d1	označován
jako	jako	k8xC	jako
lidový	lidový	k2eAgInSc4d1	lidový
kroj	kroj	k1gInSc4	kroj
<g/>
,	,	kIx,	,
lidový	lidový	k2eAgInSc4d1	lidový
oděv	oděv	k1gInSc4	oděv
nebo	nebo	k8xC	nebo
lidový	lidový	k2eAgInSc4d1	lidový
oblek	oblek	k1gInSc4	oblek
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
charakteristický	charakteristický	k2eAgInSc4d1	charakteristický
oděv	oděv	k1gInSc4	oděv
určité	určitý	k2eAgFnSc2d1	určitá
skupiny	skupina	k1gFnSc2	skupina
-	-	kIx~	-
etnografické	etnografický	k2eAgInPc1d1	etnografický
<g/>
,	,	kIx,	,
profesní	profesní	k2eAgInPc1d1	profesní
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
například	například	k6eAd1	například
cechy	cech	k1gInPc1	cech
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oděv	oděv	k1gInSc1	oděv
konkrétního	konkrétní	k2eAgInSc2d1	konkrétní
spolku	spolek	k1gInSc2	spolek
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
lidových	lidový	k2eAgFnPc2d1	lidová
komunit	komunita	k1gFnPc2	komunita
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
kroj	kroj	k1gInSc1	kroj
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
nezaměnitelnými	zaměnitelný	k2eNgInPc7d1	nezaměnitelný
prvky	prvek	k1gInPc7	prvek
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
druh	druh	k1gInSc1	druh
výšivky	výšivka	k1gFnSc2	výšivka
<g/>
,	,	kIx,	,
krajky	krajka	k1gFnSc2	krajka
<g/>
,	,	kIx,	,
barva	barva	k1gFnSc1	barva
použitého	použitý	k2eAgInSc2d1	použitý
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
materiál	materiál	k1gInSc1	materiál
samotný	samotný	k2eAgInSc1d1	samotný
<g/>
,	,	kIx,	,
střih	střih	k1gInSc1	střih
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
dědil	dědit	k5eAaImAgMnS	dědit
(	(	kIx(	(
<g/>
a	a	k8xC	a
stále	stále	k6eAd1	stále
dědí	dědit	k5eAaImIp3nS	dědit
<g/>
)	)	kIx)	)
z	z	k7c2	z
generace	generace	k1gFnSc2	generace
na	na	k7c4	na
generaci	generace	k1gFnSc4	generace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
regionálních	regionální	k2eAgInPc2d1	regionální
typů	typ	k1gInPc2	typ
lze	lze	k6eAd1	lze
kroje	kroj	k1gInSc2	kroj
členit	členit	k5eAaImF	členit
dle	dle	k7c2	dle
postavení	postavení	k1gNnSc2	postavení
jejich	jejich	k3xOp3gMnSc2	jejich
nositele	nositel	k1gMnSc2	nositel
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
rozpoznat	rozpoznat	k5eAaPmF	rozpoznat
kroje	kroj	k1gInPc1	kroj
pracovní	pracovní	k2eAgInPc1d1	pracovní
od	od	k7c2	od
svátečních	sváteční	k2eAgInPc2d1	sváteční
<g/>
,	,	kIx,	,
svatebních	svatební	k2eAgInPc2d1	svatební
nebo	nebo	k8xC	nebo
obřadních	obřadní	k2eAgInPc2d1	obřadní
<g/>
.	.	kIx.	.
</s>
<s>
Jinak	jinak	k6eAd1	jinak
vypadá	vypadat	k5eAaPmIp3nS	vypadat
kroj	kroj	k1gInSc4	kroj
pro	pro	k7c4	pro
svobodné	svobodný	k2eAgNnSc4d1	svobodné
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
pro	pro	k7c4	pro
vdané	vdaný	k2eAgInPc4d1	vdaný
a	a	k8xC	a
ženaté	ženatý	k2eAgInPc4d1	ženatý
<g/>
.	.	kIx.	.
</s>
<s>
Kroj	kroj	k1gInSc1	kroj
se	se	k3xPyFc4	se
také	také	k9	také
liší	lišit	k5eAaImIp3nS	lišit
podle	podle	k7c2	podle
ročního	roční	k2eAgNnSc2d1	roční
období	období	k1gNnSc2	období
–	–	k?	–
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
přibývají	přibývat	k5eAaImIp3nP	přibývat
kratší	krátký	k2eAgFnPc1d2	kratší
nebo	nebo	k8xC	nebo
delší	dlouhý	k2eAgInPc1d2	delší
kabáty	kabát	k1gInPc1	kabát
<g/>
,	,	kIx,	,
živůtky	živůtek	k1gInPc1	živůtek
a	a	k8xC	a
různé	různý	k2eAgFnPc1d1	různá
další	další	k2eAgFnPc1d1	další
součásti	součást	k1gFnPc1	součást
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
například	například	k6eAd1	například
určit	určit	k5eAaPmF	určit
<g/>
,	,	kIx,	,
jakého	jaký	k3yQgNnSc2	jaký
zaměstnání	zaměstnání	k1gNnSc2	zaměstnání
byli	být	k5eAaImAgMnP	být
jeho	jeho	k3xOp3gInPc2	jeho
nositelé	nositel	k1gMnPc1	nositel
(	(	kIx(	(
<g/>
nejvýraznějším	výrazný	k2eAgInSc7d3	nejvýraznější
příkladem	příklad	k1gInSc7	příklad
jsou	být	k5eAaImIp3nP	být
mlynáři	mlynář	k1gMnPc1	mlynář
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kroj	kroj	k1gInSc1	kroj
chudších	chudý	k2eAgFnPc2d2	chudší
<g/>
,	,	kIx,	,
prostých	prostý	k2eAgMnPc2d1	prostý
lidí	člověk	k1gMnPc2	člověk
byl	být	k5eAaImAgMnS	být
často	často	k6eAd1	často
archaičtějšího	archaický	k2eAgInSc2d2	archaičtější
materiálu	materiál	k1gInSc2	materiál
(	(	kIx(	(
<g/>
režné	režný	k2eAgNnSc1d1	režné
plátno	plátno	k1gNnSc1	plátno
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
i	i	k9	i
výzdoby	výzdoba	k1gFnSc2	výzdoba
(	(	kIx(	(
<g/>
batika	batika	k1gFnSc1	batika
<g/>
,	,	kIx,	,
modrotisk	modrotisk	k1gInSc1	modrotisk
<g/>
,	,	kIx,	,
geometrická	geometrický	k2eAgFnSc1d1	geometrická
výšivka	výšivka	k1gFnSc1	výšivka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
kroj	kroj	k1gInSc1	kroj
selských	selský	k2eAgFnPc2d1	selská
vrstev	vrstva	k1gFnPc2	vrstva
byl	být	k5eAaImAgInS	být
vyroben	vyroben	k2eAgInSc1d1	vyroben
z	z	k7c2	z
dražších	drahý	k2eAgFnPc2d2	dražší
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
i	i	k9	i
s	s	k7c7	s
větší	veliký	k2eAgFnSc7d2	veliký
zdobností	zdobnost	k1gFnSc7	zdobnost
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
barevné	barevný	k2eAgInPc4d1	barevný
korálky	korálek	k1gInPc4	korálek
<g/>
,	,	kIx,	,
bohatší	bohatý	k2eAgFnPc4d2	bohatší
výšivky	výšivka	k1gFnPc4	výšivka
a	a	k8xC	a
krajky	krajek	k1gInPc4	krajek
<g/>
,	,	kIx,	,
dražší	drahý	k2eAgInSc1d2	dražší
materiál	materiál	k1gInSc1	materiál
–	–	k?	–
brokát	brokát	k1gInSc1	brokát
<g/>
,	,	kIx,	,
satén	satén	k1gInSc1	satén
<g/>
,	,	kIx,	,
samet	samet	k1gInSc1	samet
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
kroj	kroj	k1gInSc1	kroj
symbolem	symbol	k1gInSc7	symbol
stále	stále	k6eAd1	stále
udržovaných	udržovaný	k2eAgFnPc2d1	udržovaná
nebo	nebo	k8xC	nebo
obnovených	obnovený	k2eAgFnPc2d1	obnovená
lidových	lidový	k2eAgFnPc2d1	lidová
oslav	oslava	k1gFnPc2	oslava
a	a	k8xC	a
obyčejů	obyčej	k1gInPc2	obyčej
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
hody	hod	k1gInPc4	hod
a	a	k8xC	a
fašank	fašank	k?	fašank
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
posvícení	posvícení	k1gNnSc1	posvícení
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
)	)	kIx)	)
a	a	k8xC	a
národní	národní	k2eAgFnSc2d1	národní
hrdosti	hrdost	k1gFnSc2	hrdost
<g/>
.	.	kIx.	.
</s>
<s>
Setkáme	setkat	k5eAaPmIp1nP	setkat
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
na	na	k7c6	na
folklórních	folklórní	k2eAgFnPc6d1	folklórní
slavnostech	slavnost	k1gFnPc6	slavnost
a	a	k8xC	a
festivalech	festival	k1gInPc6	festival
<g/>
,	,	kIx,	,
v	v	k7c6	v
(	(	kIx(	(
<g/>
nejen	nejen	k6eAd1	nejen
<g/>
)	)	kIx)	)
regionálních	regionální	k2eAgFnPc2d1	regionální
muzeích	muzeum	k1gNnPc6	muzeum
atd.	atd.	kA	atd.
Poměrně	poměrně	k6eAd1	poměrně
novodobou	novodobý	k2eAgFnSc7d1	novodobá
záležitostí	záležitost	k1gFnSc7	záležitost
(	(	kIx(	(
<g/>
vznik	vznik	k1gInSc1	vznik
a	a	k8xC	a
rozmach	rozmach	k1gInSc1	rozmach
před	před	k7c7	před
2	[number]	k4	2
<g/>
.	.	kIx.	.
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
krojové	krojový	k2eAgInPc4d1	krojový
plesy	ples	k1gInPc4	ples
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Česko	Česko	k1gNnSc1	Česko
==	==	k?	==
</s>
</p>
<p>
<s>
Tato	tento	k3xDgFnSc1	tento
kapitola	kapitola	k1gFnSc1	kapitola
hesla	heslo	k1gNnSc2	heslo
dává	dávat	k5eAaImIp3nS	dávat
nahlédnout	nahlédnout	k5eAaPmF	nahlédnout
do	do	k7c2	do
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
národopisných	národopisný	k2eAgInPc2d1	národopisný
regionů	region	k1gInPc2	region
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
Moravy	Morava	k1gFnSc2	Morava
a	a	k8xC	a
Slezska	Slezsko	k1gNnSc2	Slezsko
a	a	k8xC	a
představuje	představovat	k5eAaImIp3nS	představovat
jejich	jejich	k3xOp3gInPc4	jejich
kroje	kroj	k1gInPc4	kroj
<g/>
.	.	kIx.	.
</s>
<s>
Postihuje	postihovat	k5eAaImIp3nS	postihovat
pouze	pouze	k6eAd1	pouze
jejich	jejich	k3xOp3gInPc1	jejich
nejvýraznější	výrazný	k2eAgInPc1d3	nejvýraznější
regionální	regionální	k2eAgInPc1d1	regionální
rysy	rys	k1gInPc1	rys
a	a	k8xC	a
odlišnosti	odlišnost	k1gFnPc1	odlišnost
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
popisována	popisován	k2eAgFnSc1d1	popisována
současná	současný	k2eAgFnSc1d1	současná
nebo	nebo	k8xC	nebo
nejdéle	dlouho	k6eAd3	dlouho
zachovaná	zachovaný	k2eAgFnSc1d1	zachovaná
základní	základní	k2eAgFnSc1d1	základní
forma	forma	k1gFnSc1	forma
krojového	krojový	k2eAgInSc2d1	krojový
typu	typ	k1gInSc2	typ
a	a	k8xC	a
regionální	regionální	k2eAgInSc4d1	regionální
názvy	název	k1gInPc4	název
pro	pro	k7c4	pro
části	část	k1gFnPc4	část
oděvu	oděv	k1gInSc2	oděv
<g/>
.	.	kIx.	.
</s>
<s>
Dělení	dělení	k1gNnSc4	dělení
národopisných	národopisný	k2eAgNnPc2d1	Národopisné
(	(	kIx(	(
<g/>
či	či	k8xC	či
tzv.	tzv.	kA	tzv.
etnografických	etnografický	k2eAgFnPc2d1	etnografická
<g/>
)	)	kIx)	)
oblastí	oblast	k1gFnPc2	oblast
a	a	k8xC	a
podoblastí	podoblast	k1gFnPc2	podoblast
se	se	k3xPyFc4	se
v	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
a	a	k8xC	a
dalších	další	k2eAgInPc6d1	další
pramenech	pramen	k1gInPc6	pramen
mnohdy	mnohdy	k6eAd1	mnohdy
různí	různý	k2eAgMnPc1d1	různý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Čechy	Čech	k1gMnPc7	Čech
===	===	k?	===
</s>
</p>
<p>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
historických	historický	k2eAgFnPc2d1	historická
oblastí	oblast	k1gFnPc2	oblast
Českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
několika	několik	k4yIc2	několik
národopisných	národopisný	k2eAgInPc2d1	národopisný
regionů	region	k1gInPc2	region
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Praha	Praha	k1gFnSc1	Praha
====	====	k?	====
</s>
</p>
<p>
<s>
Oblast	oblast	k1gFnSc1	oblast
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
a	a	k8xC	a
její	její	k3xOp3gNnSc1	její
okolí	okolí	k1gNnSc1	okolí
(	(	kIx(	(
<g/>
Mělnicko	Mělnicko	k1gNnSc1	Mělnicko
<g/>
,	,	kIx,	,
Nymbursko	Nymbursko	k1gNnSc1	Nymbursko
a	a	k8xC	a
Kladensko	Kladensko	k1gNnSc1	Kladensko
<g/>
)	)	kIx)	)
brzy	brzy	k6eAd1	brzy
zaměnila	zaměnit	k5eAaPmAgFnS	zaměnit
lidový	lidový	k2eAgInSc4d1	lidový
kroj	kroj	k1gInSc4	kroj
za	za	k7c4	za
městský	městský	k2eAgInSc4d1	městský
šat	šat	k1gInSc4	šat
<g/>
.	.	kIx.	.
</s>
<s>
Stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
<g/>
,	,	kIx,	,
že	že	k8xS	že
o	o	k7c6	o
krojích	kroj	k1gInPc6	kroj
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
oblastí	oblast	k1gFnPc2	oblast
je	být	k5eAaImIp3nS	být
nedostatek	nedostatek	k1gInSc1	nedostatek
záznamů	záznam	k1gInPc2	záznam
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgFnPc1d1	dnešní
podoby	podoba	k1gFnPc1	podoba
krojů	kroj	k1gInPc2	kroj
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
oblastí	oblast	k1gFnPc2	oblast
jsou	být	k5eAaImIp3nP	být
tedy	tedy	k9	tedy
zpravidla	zpravidla	k6eAd1	zpravidla
rekonstrukcí	rekonstrukce	k1gFnSc7	rekonstrukce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ženský	ženský	k2eAgInSc1d1	ženský
kroj	kroj	k1gInSc1	kroj
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
košile	košile	k1gFnSc2	košile
bílé	bílý	k2eAgFnSc2d1	bílá
barvy	barva	k1gFnSc2	barva
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
kterou	který	k3yRgFnSc4	který
se	se	k3xPyFc4	se
obléká	oblékat	k5eAaImIp3nS	oblékat
živůtek	živůtek	k1gInSc1	živůtek
(	(	kIx(	(
<g/>
mnohdy	mnohdy	k6eAd1	mnohdy
se	s	k7c7	s
šněrováním	šněrování	k1gNnSc7	šněrování
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Spodní	spodní	k2eAgFnSc4d1	spodní
část	část	k1gFnSc4	část
tvoří	tvořit	k5eAaImIp3nP	tvořit
sukně	sukně	k1gFnPc1	sukně
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
dílu	díl	k1gInSc2	díl
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
kterou	který	k3yRgFnSc4	který
se	se	k3xPyFc4	se
uvazuje	uvazovat	k5eAaImIp3nS	uvazovat
zástěra	zástěra	k1gFnSc1	zástěra
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
kroji	kroj	k1gInSc3	kroj
patří	patřit	k5eAaImIp3nS	patřit
bílé	bílý	k2eAgFnSc2d1	bílá
punčochy	punčocha	k1gFnSc2	punčocha
a	a	k8xC	a
černé	černý	k2eAgInPc1d1	černý
střevíce	střevíc	k1gInPc1	střevíc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mužský	mužský	k2eAgInSc1d1	mužský
kroj	kroj	k1gInSc1	kroj
je	být	k5eAaImIp3nS	být
zastoupen	zastoupit	k5eAaPmNgInS	zastoupit
bílou	bílý	k2eAgFnSc7d1	bílá
košilí	košile	k1gFnSc7	košile
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
patří	patřit	k5eAaImIp3nS	patřit
vesta	vesta	k1gFnSc1	vesta
do	do	k7c2	do
pasu	pas	k1gInSc2	pas
<g/>
,	,	kIx,	,
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
strany	strana	k1gFnSc2	strana
ozdobena	ozdobit	k5eAaPmNgFnS	ozdobit
řadou	řada	k1gFnSc7	řada
knoflíků	knoflík	k1gInPc2	knoflík
<g/>
.	.	kIx.	.
</s>
<s>
Spodní	spodní	k2eAgFnSc1d1	spodní
část	část	k1gFnSc1	část
kroje	kroj	k1gInSc2	kroj
tvoří	tvořit	k5eAaImIp3nS	tvořit
kalhoty	kalhoty	k1gFnPc4	kalhoty
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
pod	pod	k7c4	pod
kolena	koleno	k1gNnPc4	koleno
<g/>
,	,	kIx,	,
k	k	k7c3	k
nimž	jenž	k3xRgMnPc3	jenž
se	se	k3xPyFc4	se
nosí	nosit	k5eAaImIp3nS	nosit
černé	černý	k2eAgFnPc4d1	černá
vysoké	vysoký	k2eAgFnPc4d1	vysoká
boty	bota	k1gFnPc4	bota
a	a	k8xC	a
bílé	bílý	k2eAgFnPc4d1	bílá
punčochy	punčocha	k1gFnPc4	punčocha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Posázaví	Posázaví	k1gNnSc1	Posázaví
====	====	k?	====
</s>
</p>
<p>
<s>
Tato	tento	k3xDgFnSc1	tento
oblast	oblast	k1gFnSc1	oblast
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
podobá	podobat	k5eAaImIp3nS	podobat
předchozí	předchozí	k2eAgMnSc1d1	předchozí
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
Praha	Praha	k1gFnSc1	Praha
a	a	k8xC	a
okolí	okolí	k1gNnSc1	okolí
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
Posázaví	Posázaví	k1gNnSc1	Posázaví
(	(	kIx(	(
<g/>
s	s	k7c7	s
podoblastmi	podoblast	k1gFnPc7	podoblast
Benešovsko	Benešovsko	k1gNnSc1	Benešovsko
a	a	k8xC	a
Vlašimsko	Vlašimsko	k1gNnSc1	Vlašimsko
<g/>
)	)	kIx)	)
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
Středočeského	středočeský	k2eAgInSc2d1	středočeský
kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
jedním	jeden	k4xCgInSc7	jeden
obecným	obecný	k2eAgInSc7d1	obecný
typem	typ	k1gInSc7	typ
kroje	kroj	k1gInSc2	kroj
<g/>
,	,	kIx,	,
samozřejmě	samozřejmě	k6eAd1	samozřejmě
s	s	k7c7	s
oblastními	oblastní	k2eAgFnPc7d1	oblastní
odlišnostmi	odlišnost	k1gFnPc7	odlišnost
(	(	kIx(	(
<g/>
př	př	kA	př
<g/>
.	.	kIx.	.
ve	v	k7c6	v
výšivce	výšivka	k1gFnSc6	výšivka
<g/>
,	,	kIx,	,
barevnosti	barevnost	k1gFnSc3	barevnost
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kroje	kroj	k1gInPc1	kroj
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
oblasti	oblast	k1gFnSc2	oblast
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
většinou	většinou	k6eAd1	většinou
rekonstrukcí	rekonstrukce	k1gFnPc2	rekonstrukce
těch	ten	k3xDgInPc2	ten
původních	původní	k2eAgInPc2d1	původní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Podkrkonoší	Podkrkonoší	k1gNnSc6	Podkrkonoší
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
žádné	žádný	k3yNgFnSc6	žádný
oblasti	oblast	k1gFnSc6	oblast
(	(	kIx(	(
<g/>
Jičínsko	Jičínsko	k1gNnSc1	Jičínsko
<g/>
,	,	kIx,	,
Orlicko	Orlicko	k1gNnSc1	Orlicko
a	a	k8xC	a
Náchodsko	Náchodsko	k1gNnSc1	Náchodsko
<g/>
)	)	kIx)	)
Podkrkonoší	Podkrkonoší	k1gNnSc1	Podkrkonoší
nepřežil	přežít	k5eNaPmAgInS	přežít
kroj	kroj	k1gInSc4	kroj
do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Zanikal	zanikat	k5eAaImAgMnS	zanikat
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
jeho	jeho	k3xOp3gFnSc1	jeho
ženská	ženský	k2eAgFnSc1d1	ženská
forma	forma	k1gFnSc1	forma
<g/>
.	.	kIx.	.
</s>
<s>
Mužský	mužský	k2eAgInSc1d1	mužský
kroj	kroj	k1gInSc1	kroj
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
ještě	ještě	k9	ještě
o	o	k7c4	o
něco	něco	k3yInSc4	něco
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ženský	ženský	k2eAgInSc1d1	ženský
kroj	kroj	k1gInSc1	kroj
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
vápeníku	vápeník	k1gMnSc3	vápeník
(	(	kIx(	(
<g/>
bílého	bílý	k2eAgInSc2d1	bílý
šátku	šátek	k1gInSc2	šátek
s	s	k7c7	s
drobnými	drobný	k2eAgInPc7d1	drobný
uzlíčky	uzlíček	k1gInPc7	uzlíček
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
košilka	košilka	k1gFnSc1	košilka
s	s	k7c7	s
ozdobnou	ozdobný	k2eAgFnSc7d1	ozdobná
náprsenkou	náprsenka	k1gFnSc7	náprsenka
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
niž	jenž	k3xRgFnSc4	jenž
se	se	k3xPyFc4	se
nosí	nosit	k5eAaImIp3nS	nosit
šátek	šátek	k1gInSc1	šátek
na	na	k7c4	na
krk	krk	k1gInSc4	krk
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
kroji	kroj	k1gInSc3	kroj
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
modrá	modrý	k2eAgFnSc1d1	modrá
vyšívaná	vyšívaný	k2eAgFnSc1d1	vyšívaná
zástěra	zástěra	k1gFnSc1	zástěra
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
obléká	oblékat	k5eAaImIp3nS	oblékat
na	na	k7c4	na
plátěnou	plátěný	k2eAgFnSc4d1	plátěná
sukni	sukně	k1gFnSc4	sukně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Základem	základ	k1gInSc7	základ
mužského	mužský	k2eAgInSc2d1	mužský
kroje	kroj	k1gInSc2	kroj
je	být	k5eAaImIp3nS	být
tmavomodré	tmavomodrý	k2eAgNnSc1d1	tmavomodré
sukno	sukno	k1gNnSc1	sukno
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
bílou	bílý	k2eAgFnSc4d1	bílá
košili	košile	k1gFnSc4	košile
se	se	k3xPyFc4	se
obléká	oblékat	k5eAaImIp3nS	oblékat
lajbl	lajbl	k?	lajbl
(	(	kIx(	(
<g/>
vesta	vesta	k1gFnSc1	vesta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zdobený	zdobený	k2eAgInSc1d1	zdobený
jednou	jednou	k6eAd1	jednou
řadou	řada	k1gFnSc7	řada
knoflíků	knoflík	k1gInPc2	knoflík
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
kabát	kabát	k1gInSc4	kabát
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
kroje	kroj	k1gInSc2	kroj
jsou	být	k5eAaImIp3nP	být
koženky	koženka	k1gFnPc1	koženka
(	(	kIx(	(
<g/>
kalhoty	kalhoty	k1gFnPc1	kalhoty
pod	pod	k7c4	pod
kolena	koleno	k1gNnPc4	koleno
<g/>
)	)	kIx)	)
hnědé	hnědý	k2eAgNnSc4d1	hnědé
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
černé	černý	k2eAgFnPc1d1	černá
barvy	barva	k1gFnPc1	barva
<g/>
,	,	kIx,	,
či	či	k8xC	či
pozdější	pozdní	k2eAgInPc1d2	pozdější
pantalóny	pantalóny	k1gInPc1	pantalóny
(	(	kIx(	(
<g/>
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
kalhoty	kalhoty	k1gFnPc1	kalhoty
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
kroji	kroj	k1gInSc3	kroj
patří	patřit	k5eAaImIp3nP	patřit
vázanky	vázanka	k1gFnPc1	vázanka
na	na	k7c4	na
krk	krk	k1gInSc4	krk
a	a	k8xC	a
šněrovací	šněrovací	k2eAgFnPc4d1	šněrovací
boty	bota	k1gFnPc4	bota
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Pojizeří	Pojizeří	k1gNnSc2	Pojizeří
====	====	k?	====
</s>
</p>
<p>
<s>
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
oblasti	oblast	k1gFnSc2	oblast
se	se	k3xPyFc4	se
nejlépe	dobře	k6eAd3	dobře
zachoval	zachovat	k5eAaPmAgInS	zachovat
kroj	kroj	k1gInSc1	kroj
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
Mladoboleslavska	Mladoboleslavsko	k1gNnSc2	Mladoboleslavsko
<g/>
.	.	kIx.	.
</s>
<s>
Kroje	kroj	k1gInPc1	kroj
ostatních	ostatní	k2eAgFnPc2d1	ostatní
podoblastí	podoblast	k1gFnPc2	podoblast
(	(	kIx(	(
<g/>
Turnovsko	Turnovsko	k1gNnSc1	Turnovsko
a	a	k8xC	a
Českodubsko	Českodubsko	k1gNnSc1	Českodubsko
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
známé	známý	k2eAgNnSc1d1	známé
méně	málo	k6eAd2	málo
a	a	k8xC	a
z	z	k7c2	z
lidového	lidový	k2eAgNnSc2d1	lidové
prostředí	prostředí	k1gNnSc2	prostředí
vymizely	vymizet	k5eAaPmAgInP	vymizet
velmi	velmi	k6eAd1	velmi
brzy	brzy	k6eAd1	brzy
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ženskému	ženský	k2eAgInSc3d1	ženský
kroji	kroj	k1gInSc3	kroj
dominuje	dominovat	k5eAaImIp3nS	dominovat
čepec	čepec	k1gInSc1	čepec
<g/>
,	,	kIx,	,
mnohdy	mnohdy	k6eAd1	mnohdy
s	s	k7c7	s
bílou	bílý	k2eAgFnSc7d1	bílá
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
dracounovou	dracounový	k2eAgFnSc7d1	dracounový
výšivkou	výšivka	k1gFnSc7	výšivka
<g/>
.	.	kIx.	.
</s>
<s>
Vrchní	vrchní	k2eAgFnSc1d1	vrchní
část	část	k1gFnSc1	část
kroje	kroj	k1gInSc2	kroj
tvoří	tvořit	k5eAaImIp3nS	tvořit
šněrovačka	šněrovačka	k1gFnSc1	šněrovačka
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c4	pod
ni	on	k3xPp3gFnSc4	on
se	se	k3xPyFc4	se
obléká	oblékat	k5eAaImIp3nS	oblékat
košilka	košilka	k1gFnSc1	košilka
s	s	k7c7	s
vyšitými	vyšitý	k2eAgInPc7d1	vyšitý
balonovými	balonový	k2eAgInPc7d1	balonový
rukávy	rukáv	k1gInPc7	rukáv
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
dudy	dudy	k1gFnPc1	dudy
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
k	k	k7c3	k
vrchní	vrchní	k2eAgFnSc3d1	vrchní
části	část	k1gFnSc3	část
přidává	přidávat	k5eAaImIp3nS	přidávat
i	i	k9	i
jemný	jemný	k2eAgInSc1d1	jemný
šátek	šátek	k1gInSc1	šátek
do	do	k7c2	do
výstřihu	výstřih	k1gInSc2	výstřih
<g/>
.	.	kIx.	.
</s>
<s>
Nosí	nosit	k5eAaImIp3nS	nosit
se	se	k3xPyFc4	se
i	i	k9	i
drobné	drobný	k2eAgInPc1d1	drobný
korálky	korálek	k1gInPc1	korálek
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
český	český	k2eAgInSc1d1	český
granát	granát	k1gInSc1	granát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Spodní	spodní	k2eAgFnSc4d1	spodní
část	část	k1gFnSc4	část
tvoří	tvořit	k5eAaImIp3nP	tvořit
sukně	sukně	k1gFnPc1	sukně
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
kusu	kus	k1gInSc2	kus
látky	látka	k1gFnSc2	látka
<g/>
,	,	kIx,	,
pod	pod	k7c4	pod
ni	on	k3xPp3gFnSc4	on
se	se	k3xPyFc4	se
nosí	nosit	k5eAaImIp3nS	nosit
několik	několik	k4yIc1	několik
škrobených	škrobený	k2eAgFnPc2d1	škrobená
spodniček	spodnička	k1gFnPc2	spodnička
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
sukni	sukně	k1gFnSc6	sukně
se	se	k3xPyFc4	se
obléká	oblékat	k5eAaImIp3nS	oblékat
fěrtoch	fěrtoch	k1gInSc1	fěrtoch
(	(	kIx(	(
<g/>
zástěra	zástěra	k1gFnSc1	zástěra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mnohdy	mnohdy	k6eAd1	mnohdy
bílé	bílý	k2eAgFnPc1d1	bílá
barvy	barva	k1gFnPc1	barva
s	s	k7c7	s
vyšíváním	vyšívání	k1gNnSc7	vyšívání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mužský	mužský	k2eAgInSc1d1	mužský
kroj	kroj	k1gInSc1	kroj
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
koženek	koženka	k1gFnPc2	koženka
(	(	kIx(	(
<g/>
kalhoty	kalhoty	k1gFnPc1	kalhoty
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
šedavého	šedavý	k2eAgInSc2d1	šedavý
kabátu	kabát	k1gInSc2	kabát
tzv.	tzv.	kA	tzv.
namlouváku	namlouvák	k1gInSc2	namlouvák
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
ním	on	k3xPp3gMnSc7	on
je	být	k5eAaImIp3nS	být
plátěná	plátěný	k2eAgFnSc1d1	plátěná
košile	košile	k1gFnSc1	košile
vyšívaná	vyšívaný	k2eAgFnSc1d1	vyšívaná
dracounem	dracoun	k1gInSc7	dracoun
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
kroje	kroj	k1gInSc2	kroj
je	být	k5eAaImIp3nS	být
beranice	beranice	k1gFnSc1	beranice
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
obuv	obuv	k1gFnSc1	obuv
slouží	sloužit	k5eAaImIp3nS	sloužit
vysoké	vysoký	k2eAgFnPc4d1	vysoká
boty	bota	k1gFnPc4	bota
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Polabí	Polabí	k1gNnSc2	Polabí
====	====	k?	====
</s>
</p>
<p>
<s>
Polabí	Polabí	k1gNnSc1	Polabí
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
do	do	k7c2	do
několika	několik	k4yIc2	několik
podoblastí	podoblast	k1gFnPc2	podoblast
(	(	kIx(	(
<g/>
Hradecko	Hradecko	k1gNnSc1	Hradecko
<g/>
,	,	kIx,	,
Pardubicko	Pardubicko	k1gNnSc1	Pardubicko
<g/>
,	,	kIx,	,
Chrudimsko	Chrudimsko	k1gNnSc1	Chrudimsko
<g/>
,	,	kIx,	,
Litomyšlsko	Litomyšlsko	k1gNnSc1	Litomyšlsko
<g/>
,	,	kIx,	,
Hlinecko	Hlinecko	k1gNnSc1	Hlinecko
a	a	k8xC	a
Podřipsko	Podřipsko	k1gNnSc1	Podřipsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
se	se	k3xPyFc4	se
nejdéle	dlouho	k6eAd3	dlouho
zachoval	zachovat	k5eAaPmAgInS	zachovat
kroj	kroj	k1gInSc1	kroj
litomyšlský	litomyšlský	k2eAgInSc1d1	litomyšlský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ženský	ženský	k2eAgInSc1d1	ženský
kroj	kroj	k1gInSc1	kroj
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
šněrovačky	šněrovačka	k1gFnSc2	šněrovačka
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
bruslíček	bruslíček	k1gInSc1	bruslíček
<g/>
)	)	kIx)	)
z	z	k7c2	z
tmavých	tmavý	k2eAgInPc2d1	tmavý
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
bílých	bílý	k2eAgNnPc2d1	bílé
suken	sukno	k1gNnPc2	sukno
<g/>
,	,	kIx,	,
pod	pod	k7c4	pod
niž	jenž	k3xRgFnSc4	jenž
se	se	k3xPyFc4	se
obléká	oblékat	k5eAaImIp3nS	oblékat
košile	košile	k1gFnSc2	košile
s	s	k7c7	s
krejzlíčkem	krejzlíčkem	k?	krejzlíčkem
(	(	kIx(	(
<g/>
límec	límec	k1gInSc4	límec
z	z	k7c2	z
bohatě	bohatě	k6eAd1	bohatě
nařasené	nařasený	k2eAgFnSc2d1	nařasená
krajky	krajka	k1gFnSc2	krajka
<g/>
)	)	kIx)	)
u	u	k7c2	u
krku	krk	k1gInSc2	krk
<g/>
.	.	kIx.	.
</s>
<s>
Sukni	sukně	k1gFnSc4	sukně
tvoří	tvořit	k5eAaImIp3nS	tvořit
jeden	jeden	k4xCgInSc1	jeden
kus	kus	k1gInSc1	kus
látky	látka	k1gFnSc2	látka
z	z	k7c2	z
vlněné	vlněný	k2eAgFnSc2d1	vlněná
nebo	nebo	k8xC	nebo
polovlněné	polovlněný	k2eAgFnSc2d1	polovlněný
tkaniny	tkanina	k1gFnSc2	tkanina
<g/>
.	.	kIx.	.
</s>
<s>
Dole	dole	k6eAd1	dole
je	být	k5eAaImIp3nS	být
lemována	lemovat	k5eAaImNgFnS	lemovat
stuhou	stuha	k1gFnSc7	stuha
<g/>
.	.	kIx.	.
</s>
<s>
Fěrtoch	fěrtoch	k1gInSc1	fěrtoch
(	(	kIx(	(
<g/>
zástěra	zástěra	k1gFnSc1	zástěra
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
plátěný	plátěný	k2eAgMnSc1d1	plátěný
s	s	k7c7	s
bohatou	bohatý	k2eAgFnSc7d1	bohatá
bílou	bílý	k2eAgFnSc7d1	bílá
výšivkou	výšivka	k1gFnSc7	výšivka
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
kroji	kroj	k1gInSc3	kroj
se	se	k3xPyFc4	se
nosí	nosit	k5eAaImIp3nS	nosit
šátek	šátek	k1gInSc1	šátek
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
vlasy	vlas	k1gInPc7	vlas
upravují	upravovat	k5eAaImIp3nP	upravovat
do	do	k7c2	do
drdolu	drdol	k1gInSc2	drdol
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
jsou	být	k5eAaImIp3nP	být
červené	červený	k2eAgFnPc1d1	červená
punčochy	punčocha	k1gFnPc1	punčocha
a	a	k8xC	a
střevíce	střevíc	k1gInPc1	střevíc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mužský	mužský	k2eAgInSc1d1	mužský
kroj	kroj	k1gInSc1	kroj
tvoří	tvořit	k5eAaImIp3nS	tvořit
šedavé	šedavý	k2eAgFnPc4d1	šedavá
nebo	nebo	k8xC	nebo
tmavozelené	tmavozelený	k2eAgFnPc4d1	tmavozelená
kalhoty	kalhoty	k1gFnPc4	kalhoty
zdobené	zdobený	k2eAgNnSc1d1	zdobené
bohatou	bohatý	k2eAgFnSc7d1	bohatá
výšivkou	výšivka	k1gFnSc7	výšivka
<g/>
.	.	kIx.	.
</s>
<s>
Obdobnou	obdobný	k2eAgFnSc4d1	obdobná
barvu	barva	k1gFnSc4	barva
i	i	k8xC	i
výšivku	výšivka	k1gFnSc4	výšivka
lze	lze	k6eAd1	lze
najít	najít	k5eAaPmF	najít
na	na	k7c6	na
vestě	vesta	k1gFnSc6	vesta
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c4	pod
tu	ten	k3xDgFnSc4	ten
se	se	k3xPyFc4	se
obléká	oblékat	k5eAaImIp3nS	oblékat
bílá	bílý	k2eAgFnSc1d1	bílá
košile	košile	k1gFnSc1	košile
z	z	k7c2	z
plátna	plátno	k1gNnSc2	plátno
se	s	k7c7	s
zapínáním	zapínání	k1gNnSc7	zapínání
na	na	k7c6	na
zádech	záda	k1gNnPc6	záda
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
kroje	kroj	k1gInSc2	kroj
jsou	být	k5eAaImIp3nP	být
černé	černý	k2eAgInPc1d1	černý
střevíce	střevíc	k1gInPc1	střevíc
a	a	k8xC	a
k	k	k7c3	k
nim	on	k3xPp3gInPc3	on
bílé	bílý	k2eAgFnPc1d1	bílá
nebo	nebo	k8xC	nebo
modré	modrý	k2eAgFnPc1d1	modrá
punčochy	punčocha	k1gFnPc1	punčocha
<g/>
.	.	kIx.	.
</s>
<s>
Výrazným	výrazný	k2eAgInSc7d1	výrazný
prvkem	prvek	k1gInSc7	prvek
je	být	k5eAaImIp3nS	být
také	také	k9	také
klobouk	klobouk	k1gInSc1	klobouk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Pošumaví	Pošumaví	k1gNnSc2	Pošumaví
====	====	k?	====
</s>
</p>
<p>
<s>
Pošumaví	Pošumaví	k1gNnSc1	Pošumaví
lze	lze	k6eAd1	lze
rozdělit	rozdělit	k5eAaPmF	rozdělit
do	do	k7c2	do
několika	několik	k4yIc2	několik
podoblastí	podoblast	k1gFnPc2	podoblast
(	(	kIx(	(
<g/>
Chodsko	Chodsko	k1gNnSc1	Chodsko
<g/>
,	,	kIx,	,
Prácheňsko	Prácheňsko	k1gNnSc1	Prácheňsko
a	a	k8xC	a
Klatovsko	Klatovsko	k1gNnSc1	Klatovsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýraznějším	výrazný	k2eAgInSc7d3	nejvýraznější
krojem	kroj	k1gInSc7	kroj
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
přetrval	přetrvat	k5eAaPmAgInS	přetrvat
v	v	k7c6	v
živé	živý	k2eAgFnSc6d1	živá
formě	forma	k1gFnSc6	forma
až	až	k9	až
do	do	k7c2	do
dneška	dnešek	k1gInSc2	dnešek
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
kroj	kroj	k1gInSc1	kroj
chodský	chodský	k2eAgInSc1d1	chodský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ženský	ženský	k2eAgInSc1d1	ženský
kroj	kroj	k1gInSc1	kroj
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
bílé	bílý	k2eAgFnSc2d1	bílá
plátěné	plátěný	k2eAgFnSc2d1	plátěná
košilky	košilka	k1gFnSc2	košilka
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
obléká	oblékat	k5eAaImIp3nS	oblékat
šněrovaný	šněrovaný	k2eAgInSc1d1	šněrovaný
živůtek	živůtek	k1gInSc1	živůtek
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
i	i	k9	i
červená	červený	k2eAgFnSc1d1	červená
plizovaná	plizovaný	k2eAgFnSc1d1	plizovaná
sukně	sukně	k1gFnSc1	sukně
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
obléká	oblékat	k5eAaImIp3nS	oblékat
barevná	barevný	k2eAgFnSc1d1	barevná
hedvábná	hedvábný	k2eAgFnSc1d1	hedvábná
zástěra	zástěra	k1gFnSc1	zástěra
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
se	se	k3xPyFc4	se
nosí	nosit	k5eAaImIp3nP	nosit
šátky	šátek	k1gInPc1	šátek
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
slavnostní	slavnostní	k2eAgFnPc4d1	slavnostní
příležitosti	příležitost	k1gFnPc4	příležitost
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
šátky	šátek	k1gInPc1	šátek
černé	černý	k2eAgFnSc2d1	černá
<g/>
,	,	kIx,	,
bohatě	bohatě	k6eAd1	bohatě
vyšívané	vyšívaný	k2eAgFnPc1d1	vyšívaná
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
kroji	kroj	k1gInSc3	kroj
patří	patřit	k5eAaImIp3nS	patřit
červené	červený	k2eAgFnSc2d1	červená
punčochy	punčocha	k1gFnSc2	punčocha
a	a	k8xC	a
černé	černý	k2eAgInPc1d1	černý
střevíce	střevíc	k1gInPc1	střevíc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mužský	mužský	k2eAgInSc1d1	mužský
kroj	kroj	k1gInSc1	kroj
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
bílé	bílý	k2eAgFnSc2d1	bílá
košile	košile	k1gFnSc2	košile
<g/>
,	,	kIx,	,
modré	modrý	k2eAgFnSc2d1	modrá
vesty	vesta	k1gFnSc2	vesta
ke	k	k7c3	k
krku	krk	k1gInSc3	krk
s	s	k7c7	s
vyšíváním	vyšívání	k1gNnSc7	vyšívání
a	a	k8xC	a
řadou	řada	k1gFnSc7	řada
knoflíků	knoflík	k1gMnPc2	knoflík
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
oblékají	oblékat	k5eAaImIp3nP	oblékat
žluté	žlutý	k2eAgFnPc4d1	žlutá
kalhoty	kalhoty	k1gFnPc4	kalhoty
pod	pod	k7c4	pod
kolena	koleno	k1gNnPc4	koleno
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nim	on	k3xPp3gInPc3	on
patří	patřit	k5eAaImIp3nP	patřit
bílé	bílý	k2eAgFnPc1d1	bílá
punčochy	punčocha	k1gFnPc1	punčocha
a	a	k8xC	a
novější	nový	k2eAgInSc1d2	novější
typ	typ	k1gInSc1	typ
obuvi	obuv	k1gFnSc2	obuv
<g/>
,	,	kIx,	,
holínky	holínka	k1gFnSc2	holínka
<g/>
.	.	kIx.	.
</s>
<s>
Kroj	kroj	k1gInSc1	kroj
doplňuje	doplňovat	k5eAaImIp3nS	doplňovat
beranice	beranice	k1gFnPc4	beranice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Povltaví	Povltaví	k1gNnSc2	Povltaví
====	====	k?	====
</s>
</p>
<p>
<s>
Oblast	oblast	k1gFnSc1	oblast
Povltaví	Povltaví	k1gNnSc2	Povltaví
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
několik	několik	k4yIc4	několik
podoblastí	podoblast	k1gFnPc2	podoblast
(	(	kIx(	(
<g/>
Budějovicko	Budějovicko	k1gNnSc1	Budějovicko
<g/>
,	,	kIx,	,
Doudlebsko	Doudlebsko	k1gNnSc1	Doudlebsko
<g/>
,	,	kIx,	,
Blatsko	Blatsko	k1gNnSc1	Blatsko
<g/>
,	,	kIx,	,
Písecko	Písecko	k1gNnSc1	Písecko
<g/>
,	,	kIx,	,
Milevsko	Milevsko	k1gNnSc1	Milevsko
<g/>
,	,	kIx,	,
Táborsko	Táborsko	k1gNnSc1	Táborsko
<g/>
,	,	kIx,	,
Soběslavsko	Soběslavsko	k1gNnSc1	Soběslavsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýraznějšími	výrazný	k2eAgInPc7d3	nejvýraznější
krojovými	krojový	k2eAgInPc7d1	krojový
typy	typ	k1gInPc7	typ
je	být	k5eAaImIp3nS	být
kroj	kroj	k1gInSc1	kroj
blatský	blatský	k2eAgInSc1d1	blatský
(	(	kIx(	(
<g/>
či	či	k8xC	či
blaťácký	blaťácký	k2eAgMnSc1d1	blaťácký
<g/>
)	)	kIx)	)
a	a	k8xC	a
táborský	táborský	k2eAgMnSc1d1	táborský
(	(	kIx(	(
<g/>
či	či	k8xC	či
kozácký	kozácký	k2eAgInSc1d1	kozácký
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ženský	ženský	k2eAgInSc1d1	ženský
blaťácký	blaťácký	k2eAgInSc1d1	blaťácký
kroj	kroj	k1gInSc1	kroj
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
bohatě	bohatě	k6eAd1	bohatě
vyšívaného	vyšívaný	k2eAgInSc2d1	vyšívaný
šátku	šátek	k1gInSc2	šátek
<g/>
,	,	kIx,	,
stejnou	stejný	k2eAgFnSc7d1	stejná
výšivkou	výšivka	k1gFnSc7	výšivka
opatřené	opatřený	k2eAgInPc1d1	opatřený
bíle	bíle	k6eAd1	bíle
košilky	košilka	k1gFnPc4	košilka
a	a	k8xC	a
vesty	vesta	k1gFnPc4	vesta
tmavé	tmavý	k2eAgFnSc2d1	tmavá
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Spodní	spodní	k2eAgFnSc1d1	spodní
část	část	k1gFnSc1	část
kroje	kroj	k1gInSc2	kroj
tvoří	tvořit	k5eAaImIp3nS	tvořit
sukně	sukně	k1gFnPc4	sukně
tmavé	tmavý	k2eAgFnSc2d1	tmavá
barvy	barva	k1gFnSc2	barva
s	s	k7c7	s
vyšíváním	vyšívání	k1gNnSc7	vyšívání
či	či	k8xC	či
našitou	našitý	k2eAgFnSc7d1	našitá
vyšívanou	vyšívaný	k2eAgFnSc7d1	vyšívaná
vsadkou	vsadka	k1gFnSc7	vsadka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
se	se	k3xPyFc4	se
obléká	oblékat	k5eAaImIp3nS	oblékat
bílá	bílý	k2eAgFnSc1d1	bílá
zástěra	zástěra	k1gFnSc1	zástěra
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
kroji	kroj	k1gInSc3	kroj
patří	patřit	k5eAaImIp3nP	patřit
černé	černý	k2eAgInPc1d1	černý
střevíce	střevíc	k1gInPc1	střevíc
a	a	k8xC	a
červené	červený	k2eAgFnPc1d1	červená
punčochy	punčocha	k1gFnPc1	punčocha
<g/>
.	.	kIx.	.
</s>
<s>
Ženský	ženský	k2eAgInSc1d1	ženský
kroj	kroj	k1gInSc1	kroj
táborský	táborský	k2eAgInSc1d1	táborský
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
bílé	bílý	k2eAgFnSc2d1	bílá
košilky	košilka	k1gFnSc2	košilka
s	s	k7c7	s
balonovými	balonový	k2eAgInPc7d1	balonový
rukávy	rukáv	k1gInPc7	rukáv
na	na	k7c6	na
koncích	konec	k1gInPc6	konec
s	s	k7c7	s
krajkou	krajka	k1gFnSc7	krajka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
se	se	k3xPyFc4	se
obléká	oblékat	k5eAaImIp3nS	oblékat
vesta	vesta	k1gFnSc1	vesta
s	s	k7c7	s
vykrojenými	vykrojený	k2eAgInPc7d1	vykrojený
průramky	průramek	k1gInPc7	průramek
různého	různý	k2eAgInSc2d1	různý
materiálu	materiál	k1gInSc2	materiál
i	i	k8xC	i
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
kroje	kroj	k1gInSc2	kroj
je	být	k5eAaImIp3nS	být
barevná	barevný	k2eAgFnSc1d1	barevná
(	(	kIx(	(
<g/>
pruhovaná	pruhovaný	k2eAgFnSc1d1	pruhovaná
či	či	k8xC	či
jednobarevná	jednobarevný	k2eAgFnSc1d1	jednobarevná
<g/>
)	)	kIx)	)
sukně	sukně	k1gFnSc1	sukně
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
niž	jenž	k3xRgFnSc4	jenž
se	se	k3xPyFc4	se
nosí	nosit	k5eAaImIp3nS	nosit
bílá	bílý	k2eAgFnSc1d1	bílá
bohatě	bohatě	k6eAd1	bohatě
vyšívaná	vyšívaný	k2eAgFnSc1d1	vyšívaná
zástěra	zástěra	k1gFnSc1	zástěra
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
kroji	kroj	k1gInSc3	kroj
se	se	k3xPyFc4	se
nosí	nosit	k5eAaImIp3nP	nosit
černé	černý	k2eAgInPc1d1	černý
střevíce	střevíc	k1gInPc1	střevíc
a	a	k8xC	a
bílé	bílý	k2eAgFnPc1d1	bílá
punčochy	punčocha	k1gFnPc1	punčocha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mužský	mužský	k2eAgMnSc1d1	mužský
blaťácky	blaťácky	k6eAd1	blaťácky
kroj	kroj	k1gInSc4	kroj
tvoří	tvořit	k5eAaImIp3nS	tvořit
bílá	bílý	k2eAgFnSc1d1	bílá
plátěná	plátěný	k2eAgFnSc1d1	plátěná
košile	košile	k1gFnSc1	košile
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
kterou	který	k3yRgFnSc4	který
se	se	k3xPyFc4	se
nosí	nosit	k5eAaImIp3nS	nosit
vesta	vesta	k1gFnSc1	vesta
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
kroje	kroj	k1gInSc2	kroj
jsou	být	k5eAaImIp3nP	být
žluté	žlutý	k2eAgFnPc1d1	žlutá
koženky	koženka	k1gFnPc1	koženka
(	(	kIx(	(
<g/>
kalhoty	kalhoty	k1gFnPc1	kalhoty
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
k	k	k7c3	k
nimž	jenž	k3xRgInPc3	jenž
patří	patřit	k5eAaImIp3nP	patřit
černé	černý	k2eAgFnPc1d1	černá
holínky	holínka	k1gFnPc1	holínka
a	a	k8xC	a
bílé	bílý	k2eAgFnPc1d1	bílá
punčochy	punčocha	k1gFnPc1	punčocha
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
i	i	k9	i
černý	černý	k2eAgInSc1d1	černý
širák	širák	k1gInSc1	širák
(	(	kIx(	(
<g/>
klobouk	klobouk	k1gInSc1	klobouk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
beranice	beranice	k1gFnSc1	beranice
<g/>
.	.	kIx.	.
</s>
<s>
Mužský	mužský	k2eAgInSc1d1	mužský
táborský	táborský	k2eAgInSc1d1	táborský
kroj	kroj	k1gInSc1	kroj
je	být	k5eAaImIp3nS	být
složen	složit	k5eAaPmNgInS	složit
s	s	k7c7	s
bílé	bílý	k2eAgFnSc2d1	bílá
vyšívané	vyšívaný	k2eAgFnSc2d1	vyšívaná
košile	košile	k1gFnSc2	košile
a	a	k8xC	a
soukenné	soukenný	k2eAgFnSc2d1	soukenná
vesty	vesta	k1gFnSc2	vesta
s	s	k7c7	s
jednou	jeden	k4xCgFnSc7	jeden
řadou	řada	k1gFnSc7	řada
knoflíků	knoflík	k1gInPc2	knoflík
<g/>
.	.	kIx.	.
</s>
<s>
Doplňují	doplňovat	k5eAaImIp3nP	doplňovat
ho	on	k3xPp3gNnSc4	on
tzv.	tzv.	kA	tzv.
praštěnky	praštěnky	k1gFnPc4	praštěnky
(	(	kIx(	(
<g/>
kalhoty	kalhoty	k1gFnPc4	kalhoty
<g/>
)	)	kIx)	)
světlé	světlý	k2eAgFnPc4d1	světlá
barvy	barva	k1gFnPc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Krojovou	krojový	k2eAgFnSc7d1	krojová
obuví	obuv	k1gFnSc7	obuv
jsou	být	k5eAaImIp3nP	být
černé	černý	k2eAgFnPc1d1	černá
holínky	holínka	k1gFnPc1	holínka
<g/>
,	,	kIx,	,
do	do	k7c2	do
nichž	jenž	k3xRgFnPc2	jenž
se	se	k3xPyFc4	se
nosí	nosit	k5eAaImIp3nP	nosit
modré	modrý	k2eAgFnPc1d1	modrá
nebo	nebo	k8xC	nebo
bílé	bílý	k2eAgFnPc1d1	bílá
punčochy	punčocha	k1gFnPc1	punčocha
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
patří	patřit	k5eAaImIp3nS	patřit
koženková	koženkový	k2eAgFnSc1d1	koženková
čepice	čepice	k1gFnSc1	čepice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Západní	západní	k2eAgFnPc1d1	západní
Čechy	Čechy	k1gFnPc1	Čechy
====	====	k?	====
</s>
</p>
<p>
<s>
Nejznámějším	známý	k2eAgInSc7d3	nejznámější
a	a	k8xC	a
nejzachovalejším	zachovalý	k2eAgInSc7d3	nejzachovalejší
krojem	kroj	k1gInSc7	kroj
této	tento	k3xDgFnSc2	tento
oblasti	oblast	k1gFnSc2	oblast
je	být	k5eAaImIp3nS	být
bezesporu	bezesporu	k9	bezesporu
kroj	kroj	k1gInSc1	kroj
plzeňský	plzeňský	k2eAgInSc1d1	plzeňský
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
doby	doba	k1gFnSc2	doba
Národního	národní	k2eAgNnSc2d1	národní
obrození	obrození	k1gNnSc2	obrození
byl	být	k5eAaImAgInS	být
dokonce	dokonce	k9	dokonce
chápán	chápat	k5eAaImNgInS	chápat
jako	jako	k8xS	jako
český	český	k2eAgInSc1d1	český
národní	národní	k2eAgInSc1d1	národní
kroj	kroj	k1gInSc1	kroj
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
Plzeňska	Plzeňsko	k1gNnSc2	Plzeňsko
lze	lze	k6eAd1	lze
v	v	k7c6	v
Západních	západní	k2eAgFnPc6d1	západní
Čechách	Čechy	k1gFnPc6	Čechy
najít	najít	k5eAaPmF	najít
další	další	k2eAgFnSc2d1	další
podoblasti	podoblast	k1gFnSc2	podoblast
(	(	kIx(	(
<g/>
Berounsko	Berounsko	k1gNnSc1	Berounsko
<g/>
,	,	kIx,	,
Příbramsko	Příbramsko	k1gNnSc1	Příbramsko
<g/>
,	,	kIx,	,
Rakovnicko	Rakovnicko	k1gNnSc1	Rakovnicko
<g/>
,	,	kIx,	,
Kralovicko	Kralovicko	k1gNnSc1	Kralovicko
<g/>
,	,	kIx,	,
Slánsko	Slánsko	k1gNnSc1	Slánsko
<g/>
,	,	kIx,	,
Karlovarsko	Karlovarsko	k1gNnSc1	Karlovarsko
<g/>
,	,	kIx,	,
Stříbrsko	Stříbrsko	k1gNnSc1	Stříbrsko
<g/>
,	,	kIx,	,
Plassko	Plassko	k1gNnSc1	Plassko
a	a	k8xC	a
Chebsko	Chebsko	k1gNnSc1	Chebsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgInPc6	který
se	se	k3xPyFc4	se
ale	ale	k9	ale
kroj	kroj	k1gInSc1	kroj
většinou	většinou	k6eAd1	většinou
zachoval	zachovat	k5eAaPmAgInS	zachovat
jen	jen	k9	jen
částečně	částečně	k6eAd1	částečně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ženský	ženský	k2eAgInSc1d1	ženský
kroj	kroj	k1gInSc1	kroj
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
košilky	košilka	k1gFnSc2	košilka
z	z	k7c2	z
bílého	bílý	k2eAgNnSc2d1	bílé
plátna	plátno	k1gNnSc2	plátno
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
krajkový	krajkový	k2eAgInSc4d1	krajkový
krejzlík	krejzlík	k?	krejzlík
a	a	k8xC	a
balonové	balonový	k2eAgInPc4d1	balonový
rukávy	rukáv	k1gInPc4	rukáv
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
se	se	k3xPyFc4	se
obléká	oblékat	k5eAaImIp3nS	oblékat
živůtek	živůtek	k1gInSc1	živůtek
kolem	kolem	k7c2	kolem
pasu	pas	k1gInSc2	pas
s	s	k7c7	s
tzv.	tzv.	kA	tzv.
honzíkem	honzík	k1gMnSc7	honzík
(	(	kIx(	(
<g/>
vycpaný	vycpaný	k2eAgInSc1d1	vycpaný
pruh	pruh	k1gInSc1	pruh
látky	látka	k1gFnSc2	látka
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yQgInSc6	který
drží	držet	k5eAaImIp3nP	držet
sukně	sukně	k1gFnPc1	sukně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Spodní	spodní	k2eAgFnSc1d1	spodní
část	část	k1gFnSc1	část
kroje	kroj	k1gInSc2	kroj
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
jednobarevné	jednobarevný	k2eAgFnSc2d1	jednobarevná
sukně	sukně	k1gFnSc2	sukně
lemované	lemovaný	k2eAgFnPc1d1	lemovaná
pentlemi	pentle	k1gFnPc7	pentle
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
podkládá	podkládat	k5eAaImIp3nS	podkládat
několika	několik	k4yIc7	několik
spodnicemi	spodnice	k1gFnPc7	spodnice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
sukni	sukně	k1gFnSc6	sukně
se	se	k3xPyFc4	se
obléká	oblékat	k5eAaImIp3nS	oblékat
zástěra	zástěra	k1gFnSc1	zástěra
z	z	k7c2	z
rozličných	rozličný	k2eAgInPc2d1	rozličný
materiálů	materiál	k1gInPc2	materiál
a	a	k8xC	a
různých	různý	k2eAgFnPc2d1	různá
barev	barva	k1gFnPc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
kroje	kroj	k1gInSc2	kroj
je	být	k5eAaImIp3nS	být
čepec	čepec	k1gInSc1	čepec
s	s	k7c7	s
dýnkem	dýnko	k1gNnSc7	dýnko
a	a	k8xC	a
holubinkou	holubinka	k1gFnSc7	holubinka
bílé	bílý	k2eAgFnSc2d1	bílá
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Obuví	obuv	k1gFnSc7	obuv
ke	k	k7c3	k
kroji	kroj	k1gInSc3	kroj
jsou	být	k5eAaImIp3nP	být
černé	černý	k2eAgInPc1d1	černý
střevíce	střevíc	k1gInPc1	střevíc
<g/>
,	,	kIx,	,
či	či	k8xC	či
pozdější	pozdní	k2eAgFnPc4d2	pozdější
šněrovací	šněrovací	k2eAgFnPc4d1	šněrovací
boty	bota	k1gFnPc4	bota
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
nich	on	k3xPp3gInPc2	on
se	se	k3xPyFc4	se
nosí	nosit	k5eAaImIp3nS	nosit
červené	červený	k2eAgNnSc1d1	červené
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
později	pozdě	k6eAd2	pozdě
bílé	bílý	k2eAgFnPc4d1	bílá
punčochy	punčocha	k1gFnPc4	punčocha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mužský	mužský	k2eAgInSc1d1	mužský
kroj	kroj	k1gInSc1	kroj
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
modrá	modrý	k2eAgFnSc1d1	modrá
vesta	vesta	k1gFnSc1	vesta
s	s	k7c7	s
vyšíváním	vyšívání	k1gNnSc7	vyšívání
a	a	k8xC	a
jednou	jednou	k6eAd1	jednou
řadou	řada	k1gFnSc7	řada
knoflíků	knoflík	k1gInPc2	knoflík
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c4	pod
ni	on	k3xPp3gFnSc4	on
se	se	k3xPyFc4	se
nosí	nosit	k5eAaImIp3nS	nosit
bílá	bílý	k2eAgFnSc1d1	bílá
plátěná	plátěný	k2eAgFnSc1d1	plátěná
košile	košile	k1gFnSc1	košile
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
kroji	kroj	k1gInSc3	kroj
patří	patřit	k5eAaImIp3nP	patřit
také	také	k9	také
žluté	žlutý	k2eAgFnPc1d1	žlutá
koženky	koženka	k1gFnPc1	koženka
(	(	kIx(	(
<g/>
kalhoty	kalhoty	k1gFnPc1	kalhoty
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
k	k	k7c3	k
nim	on	k3xPp3gInPc3	on
bílé	bílý	k2eAgFnPc1d1	bílá
punčochy	punčocha	k1gFnPc1	punčocha
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
se	se	k3xPyFc4	se
nosí	nosit	k5eAaImIp3nS	nosit
černý	černý	k2eAgInSc1d1	černý
klobouk	klobouk	k1gInSc1	klobouk
nebo	nebo	k8xC	nebo
kožešinová	kožešinový	k2eAgFnSc1d1	kožešinová
čepice	čepice	k1gFnSc1	čepice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Horácko	Horácko	k1gNnSc1	Horácko
====	====	k?	====
</s>
</p>
<p>
<s>
Horácko	Horácko	k1gNnSc1	Horácko
je	být	k5eAaImIp3nS	být
region	region	k1gInSc4	region
na	na	k7c4	na
rozhraní	rozhraní	k1gNnSc4	rozhraní
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Svými	svůj	k3xOyFgInPc7	svůj
kroji	kroj	k1gInPc7	kroj
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
spíše	spíše	k9	spíše
řadí	řadit	k5eAaImIp3nS	řadit
do	do	k7c2	do
české	český	k2eAgFnSc2d1	Česká
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
množství	množství	k1gNnSc2	množství
podoblastí	podoblast	k1gFnPc2	podoblast
(	(	kIx(	(
<g/>
Boskovicko	Boskovicko	k1gNnSc1	Boskovicko
<g/>
,	,	kIx,	,
Blatensko	Blatensko	k1gNnSc1	Blatensko
<g/>
,	,	kIx,	,
Třebíčsko	Třebíčsko	k1gNnSc1	Třebíčsko
<g/>
,	,	kIx,	,
Pelhřimovsko	Pelhřimovsko	k1gNnSc1	Pelhřimovsko
<g/>
,	,	kIx,	,
Jihlavsko	Jihlavsko	k1gNnSc1	Jihlavsko
<g/>
,	,	kIx,	,
Dačicko	Dačicko	k1gNnSc1	Dačicko
a	a	k8xC	a
Žďársko	Žďársko	k1gNnSc1	Žďársko
<g/>
)	)	kIx)	)
lze	lze	k6eAd1	lze
usuzovat	usuzovat	k5eAaImF	usuzovat
na	na	k7c4	na
kroje	kroj	k1gInPc4	kroj
s	s	k7c7	s
množstvím	množství	k1gNnSc7	množství
oblastních	oblastní	k2eAgNnPc2d1	oblastní
specifik	specifikon	k1gNnPc2	specifikon
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgInPc4d1	mnohý
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
krojů	kroj	k1gInPc2	kroj
se	se	k3xPyFc4	se
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
původní	původní	k2eAgFnSc6d1	původní
formě	forma	k1gFnSc6	forma
nezachovaly	zachovat	k5eNaPmAgFnP	zachovat
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
krojové	krojový	k2eAgInPc1d1	krojový
typy	typ	k1gInPc1	typ
jsou	být	k5eAaImIp3nP	být
ale	ale	k9	ale
dnes	dnes	k6eAd1	dnes
rekonstruovány	rekonstruován	k2eAgFnPc1d1	rekonstruována
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ženský	ženský	k2eAgInSc1d1	ženský
kroj	kroj	k1gInSc1	kroj
zaznamenává	zaznamenávat	k5eAaImIp3nS	zaznamenávat
největší	veliký	k2eAgInSc1d3	veliký
rozdíl	rozdíl	k1gInSc1	rozdíl
v	v	k7c6	v
oblastní	oblastní	k2eAgFnSc6d1	oblastní
úpravě	úprava	k1gFnSc6	úprava
šátků	šátek	k1gInPc2	šátek
(	(	kIx(	(
<g/>
hedvábné	hedvábný	k2eAgInPc4d1	hedvábný
šátky	šátek	k1gInPc4	šátek
<g/>
,	,	kIx,	,
dýnkové	dýnkový	k2eAgInPc4d1	dýnkový
čepce	čepec	k1gInPc4	čepec
s	s	k7c7	s
tzv.	tzv.	kA	tzv.
holubinkami	holubinka	k1gFnPc7	holubinka
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
a	a	k8xC	a
vlasů	vlas	k1gInPc2	vlas
obecně	obecně	k6eAd1	obecně
<g/>
.	.	kIx.	.
</s>
<s>
Kroj	kroj	k1gInSc1	kroj
jako	jako	k8xS	jako
takový	takový	k3xDgMnSc1	takový
se	se	k3xPyFc4	se
skládal	skládat	k5eAaImAgMnS	skládat
z	z	k7c2	z
tzv.	tzv.	kA	tzv.
lajblíčku	lajblíčku	k?	lajblíčku
(	(	kIx(	(
<g/>
šněrovačka	šněrovačka	k1gFnSc1	šněrovačka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
součástí	součást	k1gFnSc7	součást
byla	být	k5eAaImAgFnS	být
sukně	sukně	k1gFnSc1	sukně
tvořená	tvořený	k2eAgFnSc1d1	tvořená
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
k	k	k7c3	k
sobě	se	k3xPyFc3	se
nesešitých	sešitý	k2eNgInPc2d1	sešitý
kusů	kus	k1gInPc2	kus
<g/>
,	,	kIx,	,
zadní	zadní	k2eAgFnPc4d1	zadní
z	z	k7c2	z
plátna	plátno	k1gNnSc2	plátno
<g/>
,	,	kIx,	,
přední	přední	k2eAgFnSc1d1	přední
zástěra	zástěra	k1gFnSc1	zástěra
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
bílé	bílý	k2eAgFnPc4d1	bílá
barvy	barva	k1gFnPc4	barva
<g/>
)	)	kIx)	)
z	z	k7c2	z
hedvábí	hedvábí	k1gNnSc2	hedvábí
<g/>
.	.	kIx.	.
</s>
<s>
Krojovou	krojový	k2eAgFnSc7d1	krojová
obuví	obuv	k1gFnSc7	obuv
jsou	být	k5eAaImIp3nP	být
kotníčkové	kotníčkový	k2eAgFnPc1d1	kotníčková
šněrovací	šněrovací	k2eAgFnPc1d1	šněrovací
boty	bota	k1gFnPc1	bota
a	a	k8xC	a
do	do	k7c2	do
nich	on	k3xPp3gInPc2	on
bílé	bílý	k2eAgFnPc1d1	bílá
punčochy	punčocha	k1gFnSc2	punčocha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mužský	mužský	k2eAgInSc1d1	mužský
kroj	kroj	k1gInSc1	kroj
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
bílé	bílý	k2eAgFnSc2d1	bílá
košile	košile	k1gFnSc2	košile
zapínané	zapínaný	k2eAgFnSc2d1	zapínaná
na	na	k7c4	na
zádech	zádech	k1gInSc4	zádech
a	a	k8xC	a
vesty	vesta	k1gFnPc4	vesta
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
barevné	barevný	k2eAgNnSc1d1	barevné
<g/>
)	)	kIx)	)
ozdobenou	ozdobený	k2eAgFnSc7d1	ozdobená
jednou	jeden	k4xCgFnSc7	jeden
řadou	řada	k1gFnSc7	řada
knoflíků	knoflík	k1gInPc2	knoflík
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
jsou	být	k5eAaImIp3nP	být
polovlněné	polovlněný	k2eAgFnPc1d1	polovlněný
šerky	šerka	k1gFnPc1	šerka
<g/>
,	,	kIx,	,
či	či	k8xC	či
cajky	cajk	k1gInPc1	cajk
(	(	kIx(	(
<g/>
kalhoty	kalhoty	k1gFnPc1	kalhoty
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nim	on	k3xPp3gInPc3	on
patří	patřit	k5eAaImIp3nP	patřit
vysoké	vysoký	k2eAgFnPc1d1	vysoká
boty	bota	k1gFnPc1	bota
a	a	k8xC	a
regionálně	regionálně	k6eAd1	regionálně
rozličné	rozličný	k2eAgInPc4d1	rozličný
typy	typ	k1gInPc4	typ
klobouků	klobouk	k1gInPc2	klobouk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Morava	Morava	k1gFnSc1	Morava
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
se	se	k3xPyFc4	se
kroje	kroj	k1gInPc1	kroj
zachovaly	zachovat	k5eAaPmAgInP	zachovat
ve	v	k7c6	v
větší	veliký	k2eAgFnSc6d2	veliký
míře	míra	k1gFnSc6	míra
<g/>
,	,	kIx,	,
než	než	k8xS	než
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
moravských	moravský	k2eAgInPc6d1	moravský
regionech	region	k1gInPc6	region
jsou	být	k5eAaImIp3nP	být
běžnou	běžný	k2eAgFnSc7d1	běžná
součástí	součást	k1gFnSc7	součást
lidových	lidový	k2eAgFnPc2d1	lidová
oslav	oslava	k1gFnPc2	oslava
a	a	k8xC	a
stále	stále	k6eAd1	stále
se	se	k3xPyFc4	se
dědí	dědit	k5eAaImIp3nS	dědit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Brněnsko	Brněnsko	k1gNnSc1	Brněnsko
====	====	k?	====
</s>
</p>
<p>
<s>
Kroj	kroj	k1gInSc1	kroj
této	tento	k3xDgFnSc2	tento
oblasti	oblast	k1gFnSc2	oblast
je	být	k5eAaImIp3nS	být
výrazně	výrazně	k6eAd1	výrazně
podobný	podobný	k2eAgInSc1d1	podobný
tehdejšímu	tehdejší	k2eAgNnSc3d1	tehdejší
městskému	městský	k2eAgNnSc3d1	Městské
oblečení	oblečení	k1gNnSc3	oblečení
<g/>
.	.	kIx.	.
</s>
<s>
Jím	on	k3xPp3gNnSc7	on
byl	být	k5eAaImAgInS	být
také	také	k9	také
brzy	brzy	k6eAd1	brzy
vytlačen	vytlačen	k2eAgMnSc1d1	vytlačen
<g/>
.	.	kIx.	.
</s>
<s>
Liší	lišit	k5eAaImIp3nS	lišit
se	se	k3xPyFc4	se
dle	dle	k7c2	dle
podoblasti	podoblast	k1gFnSc2	podoblast
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yQgFnSc2	který
pochází	pocházet	k5eAaImIp3nP	pocházet
(	(	kIx(	(
<g/>
Slavkovsko	Slavkovsko	k1gNnSc1	Slavkovsko
a	a	k8xC	a
Vyškovsko	Vyškovsko	k1gNnSc1	Vyškovsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
krajové	krajový	k2eAgFnPc1d1	krajová
varianty	varianta	k1gFnPc1	varianta
kroje	kroj	k1gInSc2	kroj
již	již	k6eAd1	již
vymizely	vymizet	k5eAaPmAgFnP	vymizet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ženský	ženský	k2eAgInSc1d1	ženský
kroj	kroj	k1gInSc1	kroj
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
kabátku	kabátek	k1gInSc2	kabátek
(	(	kIx(	(
<g/>
regionálně	regionálně	k6eAd1	regionálně
marýnka	marýnka	k1gFnSc1	marýnka
<g/>
,	,	kIx,	,
duša	duša	k1gFnSc1	duša
<g/>
,	,	kIx,	,
planták	planták	k1gInSc1	planták
<g/>
)	)	kIx)	)
s	s	k7c7	s
výstřihem	výstřih	k1gInSc7	výstřih
<g/>
,	,	kIx,	,
do	do	k7c2	do
nějž	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
nosí	nosit	k5eAaImIp3nS	nosit
šátek	šátek	k1gInSc1	šátek
nebo	nebo	k8xC	nebo
bílá	bílý	k2eAgFnSc1d1	bílá
vyšívaná	vyšívaný	k2eAgFnSc1d1	vyšívaná
tylová	tylový	k2eAgFnSc1d1	tylová
půlka	půlka	k1gFnSc1	půlka
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
prosazuje	prosazovat	k5eAaImIp3nS	prosazovat
jupka	jupka	k1gFnSc1	jupka
–	–	k?	–
propínací	propínací	k2eAgInSc4d1	propínací
živůtek	živůtek	k1gInSc4	živůtek
s	s	k7c7	s
dlouhými	dlouhý	k2eAgInPc7d1	dlouhý
rukávy	rukáv	k1gInPc7	rukáv
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sukni	sukně	k1gFnSc4	sukně
opět	opět	k6eAd1	opět
tvoří	tvořit	k5eAaImIp3nP	tvořit
dvě	dva	k4xCgFnPc1	dva
části	část	k1gFnPc1	část
–	–	k?	–
zadní	zadní	k2eAgInSc1d1	zadní
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
šorec	šorec	k1gInSc1	šorec
<g/>
,	,	kIx,	,
šorán	šorán	k1gInSc1	šorán
a	a	k8xC	a
přední	přední	k2eAgFnSc1d1	přední
zástěra	zástěra	k1gFnSc1	zástěra
(	(	kIx(	(
<g/>
černá	černý	k2eAgFnSc1d1	černá
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
modrotisková	modrotiskový	k2eAgFnSc1d1	modrotisková
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
čepec	čepec	k1gInSc1	čepec
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
modrý	modrý	k2eAgInSc1d1	modrý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mužský	mužský	k2eAgInSc1d1	mužský
kroj	kroj	k1gInSc1	kroj
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
bavlněná	bavlněný	k2eAgFnSc1d1	bavlněná
košile	košile	k1gFnSc1	košile
zdobená	zdobený	k2eAgFnSc1d1	zdobená
sámky	sámek	k1gInPc7	sámek
a	a	k8xC	a
bílou	bílý	k2eAgFnSc7d1	bílá
výšivkou	výšivka	k1gFnSc7	výšivka
a	a	k8xC	a
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
nošená	nošený	k2eAgFnSc1d1	nošená
kordula	kordula	k1gFnSc1	kordula
(	(	kIx(	(
<g/>
vesta	vesta	k1gFnSc1	vesta
<g/>
)	)	kIx)	)
ze	z	k7c2	z
sukna	sukno	k1gNnSc2	sukno
<g/>
,	,	kIx,	,
s	s	k7c7	s
jednou	jednou	k6eAd1	jednou
řadou	řada	k1gFnSc7	řada
knoflíků	knoflík	k1gInPc2	knoflík
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
ke	k	k7c3	k
kroji	kroj	k1gInSc3	kroj
oblékají	oblékat	k5eAaImIp3nP	oblékat
kůženice	kůženice	k1gFnPc1	kůženice
(	(	kIx(	(
<g/>
kalhoty	kalhoty	k1gFnPc1	kalhoty
<g/>
)	)	kIx)	)
později	pozdě	k6eAd2	pozdě
bílé	bílý	k2eAgFnPc4d1	bílá
barvy	barva	k1gFnPc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
kroji	kroj	k1gInSc3	kroj
patří	patřit	k5eAaImIp3nS	patřit
vysoké	vysoký	k2eAgFnSc2d1	vysoká
boty	bota	k1gFnSc2	bota
černé	černý	k2eAgFnSc2d1	černá
barvy	barva	k1gFnSc2	barva
a	a	k8xC	a
klobouk	klobouk	k1gInSc1	klobouk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Podhorácko	Podhorácko	k1gNnSc1	Podhorácko
====	====	k?	====
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
region	region	k1gInSc1	region
s	s	k7c7	s
podoblastmi	podoblast	k1gFnPc7	podoblast
Moravskokrumlovsko	Moravskokrumlovsko	k1gNnSc1	Moravskokrumlovsko
a	a	k8xC	a
Znojemsko	Znojemsko	k1gNnSc1	Znojemsko
se	s	k7c7	s
krojovým	krojový	k2eAgInSc7d1	krojový
typem	typ	k1gInSc7	typ
nápadně	nápadně	k6eAd1	nápadně
podobá	podobat	k5eAaImIp3nS	podobat
výše	vysoce	k6eAd2	vysoce
popisovanému	popisovaný	k2eAgNnSc3d1	popisované
Brněnsku	Brněnsko	k1gNnSc3	Brněnsko
<g/>
.	.	kIx.	.
</s>
<s>
Kroj	kroj	k1gInSc1	kroj
zde	zde	k6eAd1	zde
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgFnPc4	svůj
regionální	regionální	k2eAgFnPc4d1	regionální
odlišnosti	odlišnost	k1gFnPc4	odlišnost
<g/>
,	,	kIx,	,
v	v	k7c6	v
celkových	celkový	k2eAgInPc6d1	celkový
rysech	rys	k1gInPc6	rys
ve	v	k7c6	v
však	však	k9	však
od	od	k7c2	od
kroje	kroj	k1gInSc2	kroj
brněnského	brněnský	k2eAgInSc2d1	brněnský
národopisného	národopisný	k2eAgInSc2d1	národopisný
regionu	region	k1gInSc2	region
příliš	příliš	k6eAd1	příliš
neliší	lišit	k5eNaImIp3nP	lišit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Haná	Haná	k1gFnSc1	Haná
====	====	k?	====
</s>
</p>
<p>
<s>
Ženský	ženský	k2eAgInSc1d1	ženský
kroj	kroj	k1gInSc1	kroj
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
rubáče	rubáče	k?	rubáče
(	(	kIx(	(
<g/>
=	=	kIx~	=
<g/>
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
spodní	spodní	k2eAgNnSc4d1	spodní
prádlo	prádlo	k1gNnSc4	prádlo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
z	z	k7c2	z
rukávců	rukávec	k1gInPc2	rukávec
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
bílé	bílý	k2eAgInPc1d1	bílý
<g/>
,	,	kIx,	,
na	na	k7c6	na
koncích	konec	k1gInPc6	konec
mají	mít	k5eAaImIp3nP	mít
žlutě	žlutě	k6eAd1	žlutě
vyšívané	vyšívaný	k2eAgFnPc4d1	vyšívaná
tacle	tacle	k1gFnPc4	tacle
(	(	kIx(	(
<g/>
konce	konec	k1gInSc2	konec
rukávců	rukávec	k1gInPc2	rukávec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rukávce	rukávec	k1gInPc4	rukávec
se	se	k3xPyFc4	se
připíná	připínat	k5eAaImIp3nS	připínat
bohatý	bohatý	k2eAgMnSc1d1	bohatý
krajkový	krajkový	k2eAgMnSc1d1	krajkový
krézl	kréznout	k5eAaPmAgMnS	kréznout
(	(	kIx(	(
<g/>
límec	límec	k1gInSc1	límec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vrchní	vrchní	k2eAgFnSc1d1	vrchní
část	část	k1gFnSc1	část
kroje	kroj	k1gInSc2	kroj
dotváří	dotvářet	k5eAaImIp3nS	dotvářet
kordulka	kordulka	k1gFnSc1	kordulka
či	či	k8xC	či
frydka	frydka	k1gFnSc1	frydka
(	(	kIx(	(
<g/>
živůtek	živůtek	k1gInSc1	živůtek
<g/>
,	,	kIx,	,
vesta	vesta	k1gFnSc1	vesta
<g/>
)	)	kIx)	)
zdobená	zdobený	k2eAgFnSc1d1	zdobená
stuhami	stuha	k1gFnPc7	stuha
a	a	k8xC	a
s	s	k7c7	s
květinovým	květinový	k2eAgInSc7d1	květinový
motivem	motiv	k1gInSc7	motiv
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
kroje	kroj	k1gInSc2	kroj
jsou	být	k5eAaImIp3nP	být
dvě	dva	k4xCgFnPc1	dva
nesešité	sešitý	k2eNgFnPc1d1	nesešitá
zástěry	zástěra	k1gFnPc1	zástěra
tvořící	tvořící	k2eAgFnPc1d1	tvořící
dohromady	dohromady	k6eAd1	dohromady
sukni	sukně	k1gFnSc4	sukně
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
fěrtoch	fěrtoch	k1gInSc1	fěrtoch
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
(	(	kIx(	(
<g/>
přední	přední	k2eAgFnSc1d1	přední
zástěra	zástěra	k1gFnSc1	zástěra
<g/>
)	)	kIx)	)
fěrtušek	fěrtušek	k1gInSc1	fěrtušek
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
vzhled	vzhled	k1gInSc1	vzhled
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
dle	dle	k7c2	dle
podoblasti	podoblast	k1gFnSc2	podoblast
<g/>
,	,	kIx,	,
použitého	použitý	k2eAgInSc2d1	použitý
materiálu	materiál	k1gInSc2	materiál
atd.	atd.	kA	atd.
Na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
je	být	k5eAaImIp3nS	být
uvazován	uvazován	k2eAgInSc1d1	uvazován
velký	velký	k2eAgInSc1d1	velký
červený	červený	k2eAgInSc1d1	červený
šátek	šátek	k1gInSc1	šátek
typem	typ	k1gInSc7	typ
úvazu	úvaz	k1gInSc2	úvaz
tzv.	tzv.	kA	tzv.
na	na	k7c4	na
záušnice	záušnice	k1gFnPc4	záušnice
<g/>
.	.	kIx.	.
</s>
<s>
Obuví	obuv	k1gFnSc7	obuv
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
kroji	kroj	k1gInSc3	kroj
jsou	být	k5eAaImIp3nP	být
černé	černý	k2eAgInPc1d1	černý
střevíce	střevíc	k1gInPc1	střevíc
ze	z	k7c2	z
semiše	semiš	k1gInSc2	semiš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mužský	mužský	k2eAgInSc1d1	mužský
kroj	kroj	k1gInSc1	kroj
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
tenčiče	tenčič	k1gInSc2	tenčič
(	(	kIx(	(
<g/>
košile	košile	k1gFnSc2	košile
<g/>
)	)	kIx)	)
s	s	k7c7	s
černou	černý	k2eAgFnSc7d1	černá
výšivkou	výšivka	k1gFnSc7	výšivka
na	na	k7c6	na
límečku	límeček	k1gInSc6	límeček
a	a	k8xC	a
manžetách	manžeta	k1gFnPc6	manžeta
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
také	také	k6eAd1	také
černý	černý	k2eAgInSc4d1	černý
hedvábný	hedvábný	k2eAgInSc4d1	hedvábný
šátek	šátek	k1gInSc4	šátek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
košili	košile	k1gFnSc6	košile
patří	patřit	k5eAaImIp3nS	patřit
kordula	kordula	k1gFnSc1	kordula
nebo	nebo	k8xC	nebo
frydka	frydka	k1gFnSc1	frydka
(	(	kIx(	(
<g/>
vesta	vesta	k1gFnSc1	vesta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vyšívaná	vyšívaný	k2eAgFnSc1d1	vyšívaná
geometrickou	geometrický	k2eAgFnSc7d1	geometrická
výšivkou	výšivka	k1gFnSc7	výšivka
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
květinovými	květinový	k2eAgInPc7d1	květinový
motivy	motiv	k1gInPc7	motiv
<g/>
.	.	kIx.	.
</s>
<s>
Výšivka	výšivka	k1gFnSc1	výšivka
i	i	k8xC	i
barva	barva	k1gFnSc1	barva
(	(	kIx(	(
<g/>
modrá	modrý	k2eAgFnSc1d1	modrá
<g/>
,	,	kIx,	,
zelená	zelený	k2eAgFnSc1d1	zelená
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
dle	dle	k7c2	dle
konkrétní	konkrétní	k2eAgFnSc2d1	konkrétní
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
kroje	kroj	k1gInSc2	kroj
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
kalhoty	kalhoty	k1gFnPc1	kalhoty
s	s	k7c7	s
výšivkou	výšivka	k1gFnSc7	výšivka
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
regionu	region	k1gInSc2	region
koženky	koženka	k1gFnSc2	koženka
<g/>
,	,	kIx,	,
baně	baně	k1gFnSc2	baně
<g/>
,	,	kIx,	,
červenice	červenice	k1gFnSc2	červenice
nebo	nebo	k8xC	nebo
žlutice	žlutice	k1gFnSc2	žlutice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obuví	obuv	k1gFnSc7	obuv
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
kroji	kroj	k1gInSc3	kroj
jsou	být	k5eAaImIp3nP	být
černé	černý	k2eAgFnPc4d1	černá
vysoké	vysoký	k2eAgFnPc4d1	vysoká
boty	bota	k1gFnPc4	bota
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
prosvítají	prosvítat	k5eAaImIp3nP	prosvítat
bílé	bílý	k2eAgFnPc1d1	bílá
podléčky	podléčka	k1gFnPc1	podléčka
(	(	kIx(	(
<g/>
pozůstatek	pozůstatek	k1gInSc1	pozůstatek
plátěných	plátěný	k2eAgFnPc2d1	plátěná
punčoch	punčocha	k1gFnPc2	punčocha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
kroje	kroj	k1gInSc2	kroj
je	být	k5eAaImIp3nS	být
klobouk	klobouk	k1gInSc1	klobouk
(	(	kIx(	(
<g/>
člun	člun	k1gInSc1	člun
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
střípek	střípek	k1gInSc4	střípek
–	–	k?	–
dle	dle	k7c2	dle
typu	typ	k1gInSc2	typ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Každá	každý	k3xTgFnSc1	každý
podoblast	podoblast	k1gFnSc1	podoblast
regionu	region	k1gInSc2	region
(	(	kIx(	(
<g/>
Prostějovsko	Prostějovsko	k1gNnSc1	Prostějovsko
<g/>
,	,	kIx,	,
Tovačovsko	Tovačovsko	k1gNnSc1	Tovačovsko
a	a	k8xC	a
Kroměřížsko	Kroměřížsko	k1gNnSc1	Kroměřížsko
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
svůj	svůj	k3xOyFgInSc4	svůj
typický	typický	k2eAgInSc4d1	typický
kroj	kroj	k1gInSc4	kroj
s	s	k7c7	s
konkrétními	konkrétní	k2eAgInPc7d1	konkrétní
prvky	prvek	k1gInPc7	prvek
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
kterých	který	k3yRgInPc2	který
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
kroj	kroj	k1gInSc4	kroj
blíže	blízce	k6eAd2	blízce
určit	určit	k5eAaPmF	určit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Hanácké	hanácký	k2eAgNnSc1d1	Hanácké
Slovácko	Slovácko	k1gNnSc1	Slovácko
====	====	k?	====
</s>
</p>
<p>
<s>
Ženský	ženský	k2eAgInSc1d1	ženský
kroj	kroj	k1gInSc1	kroj
tvoří	tvořit	k5eAaImIp3nS	tvořit
spodní	spodní	k2eAgFnPc4d1	spodní
košile	košile	k1gFnPc4	košile
s	s	k7c7	s
rukávy	rukáv	k1gInPc7	rukáv
<g/>
.	.	kIx.	.
</s>
<s>
Rukávce	rukávec	k1gInPc1	rukávec
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
další	další	k2eAgFnSc4d1	další
část	část	k1gFnSc4	část
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
barevně	barevně	k6eAd1	barevně
vyšité	vyšitý	k2eAgFnPc1d1	vyšitá
na	na	k7c6	na
límci	límec	k1gInSc6	límec
a	a	k8xC	a
na	na	k7c6	na
rukávech	rukáv	k1gInPc6	rukáv
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
kanafaska	kanafaska	k1gFnSc1	kanafaska
(	(	kIx(	(
<g/>
sukně	sukně	k1gFnPc1	sukně
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
bílá	bílý	k2eAgFnSc1d1	bílá
plátěná	plátěný	k2eAgFnSc1d1	plátěná
sukně	sukně	k1gFnSc1	sukně
<g/>
,	,	kIx,	,
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
se	se	k3xPyFc4	se
obléká	oblékat	k5eAaImIp3nS	oblékat
modrá	modrý	k2eAgFnSc1d1	modrá
zástěra	zástěra	k1gFnSc1	zástěra
(	(	kIx(	(
<g/>
výzdoba	výzdoba	k1gFnSc1	výzdoba
dle	dle	k7c2	dle
oblasti	oblast	k1gFnSc2	oblast
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
kordulka	kordulka	k1gFnSc1	kordulka
(	(	kIx(	(
<g/>
vesta	vesta	k1gFnSc1	vesta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
oblasti	oblast	k1gFnSc2	oblast
je	být	k5eAaImIp3nS	být
červená	červený	k2eAgFnSc1d1	červená
nebo	nebo	k8xC	nebo
tmavá	tmavý	k2eAgFnSc1d1	tmavá
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
kroji	kroj	k1gInSc3	kroj
patří	patřit	k5eAaImIp3nS	patřit
červené	červený	k2eAgFnSc2d1	červená
punčochy	punčocha	k1gFnSc2	punčocha
a	a	k8xC	a
střevíce	střevíc	k1gInSc2	střevíc
<g/>
,	,	kIx,	,
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
se	se	k3xPyFc4	se
váže	vázat	k5eAaImIp3nS	vázat
červený	červený	k2eAgInSc1d1	červený
šátek	šátek	k1gInSc1	šátek
tzv.	tzv.	kA	tzv.
na	na	k7c4	na
záušnice	záušnice	k1gFnPc4	záušnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mužský	mužský	k2eAgInSc1d1	mužský
kroj	kroj	k1gInSc1	kroj
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
košile	košile	k1gFnSc2	košile
barevně	barevně	k6eAd1	barevně
vyšité	vyšitý	k2eAgFnPc1d1	vyšitá
na	na	k7c6	na
ramenou	rameno	k1gNnPc6	rameno
a	a	k8xC	a
zápěstí	zápěstí	k1gNnSc6	zápěstí
<g/>
,	,	kIx,	,
kordule	kordula	k1gFnSc6	kordula
(	(	kIx(	(
<g/>
světle	světlo	k1gNnSc6	světlo
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
tmavě	tmavě	k6eAd1	tmavě
modrá	modrý	k2eAgFnSc1d1	modrá
<g/>
,	,	kIx,	,
červená	červenat	k5eAaImIp3nS	červenat
–	–	k?	–
dle	dle	k7c2	dle
oblasti	oblast	k1gFnSc2	oblast
<g/>
)	)	kIx)	)
zdobené	zdobený	k2eAgFnPc1d1	zdobená
výšivkou	výšivka	k1gFnSc7	výšivka
a	a	k8xC	a
mnoha	mnoho	k4c7	mnoho
knoflíky	knoflík	k1gInPc7	knoflík
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
krk	krk	k1gInSc4	krk
se	se	k3xPyFc4	se
váže	vázat	k5eAaImIp3nS	vázat
černý	černý	k2eAgInSc1d1	černý
šátek	šátek	k1gInSc1	šátek
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
kroji	kroj	k1gInSc3	kroj
patří	patřit	k5eAaImIp3nP	patřit
koženky	koženka	k1gFnPc1	koženka
(	(	kIx(	(
<g/>
kalhoty	kalhoty	k1gFnPc1	kalhoty
<g/>
)	)	kIx)	)
a	a	k8xC	a
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
vystupující	vystupující	k2eAgFnPc1d1	vystupující
modré	modrý	k2eAgFnPc1d1	modrá
punčochy	punčocha	k1gFnPc1	punčocha
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
klobouk	klobouk	k1gInSc1	klobouk
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
goralák	goralák	k1gInSc1	goralák
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
šmukáč	šmukáč	k1gInSc1	šmukáč
a	a	k8xC	a
vysoké	vysoký	k2eAgFnPc1d1	vysoká
boty	bota	k1gFnPc1	bota
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
čižmy	čižma	k1gFnPc1	čižma
<g/>
,	,	kIx,	,
kordovánky	kordovánky	k1gFnPc1	kordovánky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kroj	kroj	k1gInSc1	kroj
se	se	k3xPyFc4	se
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
barevnosti	barevnost	k1gFnSc6	barevnost
<g/>
,	,	kIx,	,
druhu	druh	k1gInSc3	druh
výšivky	výšivka	k1gFnSc2	výšivka
a	a	k8xC	a
v	v	k7c6	v
ostatních	ostatní	k2eAgFnPc6d1	ostatní
drobnostech	drobnost	k1gFnPc6	drobnost
liší	lišit	k5eAaImIp3nS	lišit
dle	dle	k7c2	dle
podoblasti	podoblast	k1gFnSc2	podoblast
(	(	kIx(	(
<g/>
Kloboucko	Kloboucko	k1gNnSc1	Kloboucko
<g/>
,	,	kIx,	,
Mikulovsko	Mikulovsko	k1gNnSc1	Mikulovsko
a	a	k8xC	a
Ždánsko	Ždánsko	k1gNnSc1	Ždánsko
<g/>
)	)	kIx)	)
regionu	region	k1gInSc2	region
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Moravské	moravský	k2eAgNnSc1d1	Moravské
Slovácko	Slovácko	k1gNnSc1	Slovácko
====	====	k?	====
</s>
</p>
<p>
<s>
Kroje	kroj	k1gInPc1	kroj
Moravského	moravský	k2eAgNnSc2d1	Moravské
Slovácka	Slovácko	k1gNnSc2	Slovácko
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
několik	několik	k4yIc4	několik
naprosto	naprosto	k6eAd1	naprosto
odlišných	odlišný	k2eAgInPc2d1	odlišný
krojových	krojový	k2eAgInPc2d1	krojový
typů	typ	k1gInPc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
podoblastech	podoblast	k1gFnPc6	podoblast
tohoto	tento	k3xDgInSc2	tento
regionu	region	k1gInSc2	region
(	(	kIx(	(
<g/>
Horňácko	Horňácko	k1gNnSc1	Horňácko
<g/>
,	,	kIx,	,
Dolňácko	Dolňácko	k1gNnSc1	Dolňácko
<g/>
,	,	kIx,	,
Podluží	Podluží	k1gNnSc1	Podluží
<g/>
,	,	kIx,	,
Moravské	moravský	k2eAgFnPc1d1	Moravská
Kopanice	Kopanice	k1gFnPc1	Kopanice
a	a	k8xC	a
Luhačovické	luhačovický	k2eAgNnSc1d1	Luhačovické
Zálesí	Zálesí	k1gNnSc1	Zálesí
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
kroje	kroj	k1gInPc1	kroj
zachovaly	zachovat	k5eAaPmAgInP	zachovat
v	v	k7c6	v
živé	živý	k2eAgFnSc6d1	živá
tradici	tradice	k1gFnSc6	tradice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ženský	ženský	k2eAgInSc1d1	ženský
kroj	kroj	k1gInSc1	kroj
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
spodní	spodní	k2eAgFnSc2d1	spodní
části	část	k1gFnSc2	část
plátěného	plátěný	k2eAgInSc2d1	plátěný
rubáče	rubáče	k?	rubáče
s	s	k7c7	s
výšivkou	výšivka	k1gFnSc7	výšivka
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
níže	nízce	k6eAd2	nízce
položených	položený	k2eAgFnPc6d1	položená
oblastech	oblast	k1gFnPc6	oblast
se	se	k3xPyFc4	se
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
rubáč	rubáč	k?	rubáč
(	(	kIx(	(
<g/>
zde	zde	k6eAd1	zde
pouze	pouze	k6eAd1	pouze
plátěný	plátěný	k2eAgMnSc1d1	plátěný
bez	bez	k7c2	bez
výšivky	výšivka	k1gFnSc2	výšivka
<g/>
)	)	kIx)	)
uvazuje	uvazovat	k5eAaImIp3nS	uvazovat
několik	několik	k4yIc1	několik
(	(	kIx(	(
<g/>
3	[number]	k4	3
a	a	k8xC	a
více	hodně	k6eAd2	hodně
<g/>
)	)	kIx)	)
naškrobených	naškrobený	k2eAgFnPc2d1	naškrobená
spodniček	spodnička	k1gFnPc2	spodnička
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
součástí	součást	k1gFnSc7	součást
jsou	být	k5eAaImIp3nP	být
rukávce	rukávec	k1gInPc1	rukávec
na	na	k7c6	na
límci	límec	k1gInSc6	límec
a	a	k8xC	a
koncích	konec	k1gInPc6	konec
rukávů	rukáv	k1gInPc2	rukáv
bohatě	bohatě	k6eAd1	bohatě
vyšívané	vyšívaný	k2eAgNnSc4d1	vyšívané
oblastní	oblastní	k2eAgNnSc4d1	oblastní
výšivkou	výšivka	k1gFnSc7	výšivka
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
ně	on	k3xPp3gMnPc4	on
se	se	k3xPyFc4	se
obléká	oblékat	k5eAaImIp3nS	oblékat
kordula	kordula	k1gFnSc1	kordula
<g/>
,	,	kIx,	,
či	či	k8xC	či
kordulka	kordulka	k1gFnSc1	kordulka
(	(	kIx(	(
<g/>
vesta	vesta	k1gFnSc1	vesta
<g/>
)	)	kIx)	)
barevně	barevně	k6eAd1	barevně
i	i	k9	i
materiálově	materiálově	k6eAd1	materiálově
odlišná	odlišný	k2eAgFnSc1d1	odlišná
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Spodní	spodní	k2eAgFnSc1d1	spodní
část	část	k1gFnSc1	část
kroje	kroj	k1gInSc2	kroj
tvoří	tvořit	k5eAaImIp3nS	tvořit
zadní	zadní	k2eAgFnPc4d1	zadní
sukně	sukně	k1gFnPc4	sukně
fěrtoch	fěrtoch	k1gInSc4	fěrtoch
<g/>
,	,	kIx,	,
či	či	k8xC	či
šorec	šorec	k1gInSc1	šorec
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
skládaná	skládaný	k2eAgFnSc1d1	skládaná
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
<g/>
,	,	kIx,	,
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
,	,	kIx,	,
žlutá	žlutý	k2eAgFnSc1d1	žlutá
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
černá	černý	k2eAgFnSc1d1	černá
<g/>
.	.	kIx.	.
</s>
<s>
Přední	přední	k2eAgFnPc1d1	přední
sukně	sukně	k1gFnPc1	sukně
fěrtúšek	fěrtúšek	k1gInSc1	fěrtúšek
<g/>
,	,	kIx,	,
fěrtoška	fěrtoška	k1gFnSc1	fěrtoška
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
bohatě	bohatě	k6eAd1	bohatě
vyšívaná	vyšívaný	k2eAgFnSc1d1	vyšívaná
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
potištěná	potištěný	k2eAgFnSc1d1	potištěná
<g/>
.	.	kIx.	.
</s>
<s>
Materiálem	materiál	k1gInSc7	materiál
je	být	k5eAaImIp3nS	být
plátno	plátno	k1gNnSc1	plátno
<g/>
,	,	kIx,	,
brokát	brokát	k1gInSc1	brokát
<g/>
,	,	kIx,	,
krajka	krajka	k1gFnSc1	krajka
aj.	aj.	kA	aj.
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
se	se	k3xPyFc4	se
obléká	oblékat	k5eAaImIp3nS	oblékat
celá	celý	k2eAgFnSc1d1	celá
sešitá	sešitý	k2eAgFnSc1d1	sešitá
sukně	sukně	k1gFnSc1	sukně
(	(	kIx(	(
<g/>
regionálně	regionálně	k6eAd1	regionálně
př	př	kA	př
<g/>
.	.	kIx.	.
pávka	pávek	k1gMnSc2	pávek
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
bohatě	bohatě	k6eAd1	bohatě
vyšívaná	vyšívaný	k2eAgFnSc1d1	vyšívaná
zástěra	zástěra	k1gFnSc1	zástěra
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
kroje	kroj	k1gInSc2	kroj
jsou	být	k5eAaImIp3nP	být
černé	černý	k2eAgFnPc1d1	černá
vysoké	vysoký	k2eAgFnPc1d1	vysoká
boty	bota	k1gFnPc1	bota
s	s	k7c7	s
výšivkou	výšivka	k1gFnSc7	výšivka
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
krpce	krpec	k1gInPc4	krpec
(	(	kIx(	(
<g/>
kožená	kožený	k2eAgFnSc1d1	kožená
nízká	nízký	k2eAgFnSc1d1	nízká
obuv	obuv	k1gFnSc1	obuv
s	s	k7c7	s
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
řemínkem	řemínek	k1gInSc7	řemínek
pro	pro	k7c4	pro
upevnění	upevnění	k1gNnSc4	upevnění
kolem	kolem	k7c2	kolem
lýtka	lýtko	k1gNnSc2	lýtko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
patří	patřit	k5eAaImIp3nS	patřit
šátek	šátek	k1gInSc1	šátek
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
červené	červený	k2eAgFnPc4d1	červená
barvy	barva	k1gFnPc4	barva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mužský	mužský	k2eAgInSc1d1	mužský
kroj	kroj	k1gInSc1	kroj
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
plátěné	plátěný	k2eAgFnSc2d1	plátěná
košile	košile	k1gFnSc2	košile
(	(	kIx(	(
<g/>
regionálně	regionálně	k6eAd1	regionálně
košela	košet	k5eAaPmAgFnS	košet
<g/>
,	,	kIx,	,
či	či	k8xC	či
košula	košula	k1gFnSc1	košula
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
vyšívaná	vyšívaný	k2eAgFnSc1d1	vyšívaná
kolem	kolem	k7c2	kolem
krku	krk	k1gInSc2	krk
<g/>
,	,	kIx,	,
v	v	k7c6	v
určitých	určitý	k2eAgFnPc6d1	určitá
oblastech	oblast	k1gFnPc6	oblast
i	i	k8xC	i
na	na	k7c6	na
koncích	konec	k1gInPc6	konec
rukávů	rukáv	k1gInPc2	rukáv
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
také	také	k9	také
objevuje	objevovat	k5eAaImIp3nS	objevovat
krajka	krajka	k1gFnSc1	krajka
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
děrování	děrování	k1gNnSc1	děrování
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
ni	on	k3xPp3gFnSc4	on
se	se	k3xPyFc4	se
obléká	oblékat	k5eAaImIp3nS	oblékat
kordula	kordula	k1gFnSc1	kordula
(	(	kIx(	(
<g/>
vesta	vesta	k1gFnSc1	vesta
<g/>
)	)	kIx)	)
různé	různý	k2eAgFnSc2d1	různá
délky	délka	k1gFnSc2	délka
<g/>
,	,	kIx,	,
barvy	barva	k1gFnSc2	barva
<g/>
,	,	kIx,	,
výšivky	výšivka	k1gFnSc2	výšivka
i	i	k8xC	i
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Mnohdy	mnohdy	k6eAd1	mnohdy
jsou	být	k5eAaImIp3nP	být
její	její	k3xOp3gFnSc7	její
součástí	součást	k1gFnSc7	součást
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
vyšívané	vyšívaný	k2eAgFnSc2d1	vyšívaná
pentle	pentle	k1gFnSc2	pentle
<g/>
.	.	kIx.	.
</s>
<s>
Spodní	spodní	k2eAgFnSc1d1	spodní
část	část	k1gFnSc1	část
kroje	kroj	k1gInSc2	kroj
tvoří	tvořit	k5eAaImIp3nS	tvořit
nohavice	nohavice	k1gFnSc1	nohavice
(	(	kIx(	(
<g/>
kalhoty	kalhoty	k1gFnPc1	kalhoty
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
barvy	barva	k1gFnSc2	barva
regionálně	regionálně	k6eAd1	regionálně
žlutice	žlutice	k1gFnSc1	žlutice
<g/>
,	,	kIx,	,
červenice	červenice	k1gFnSc1	červenice
<g/>
,	,	kIx,	,
modřice	modřice	k1gFnSc1	modřice
<g/>
)	)	kIx)	)
s	s	k7c7	s
výšivkou	výšivka	k1gFnSc7	výšivka
<g/>
,	,	kIx,	,
do	do	k7c2	do
nichž	jenž	k3xRgFnPc2	jenž
si	se	k3xPyFc3	se
svobodní	svobodný	k2eAgMnPc1d1	svobodný
provlékají	provlékat	k5eAaImIp3nP	provlékat
vyšívaný	vyšívaný	k2eAgInSc4d1	vyšívaný
šátek	šátek	k1gInSc4	šátek
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
kroje	kroj	k1gInSc2	kroj
jsou	být	k5eAaImIp3nP	být
černé	černý	k2eAgFnPc4d1	černá
vysoké	vysoký	k2eAgFnPc4d1	vysoká
boty	bota	k1gFnPc4	bota
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
s	s	k7c7	s
výšivkou	výšivka	k1gFnSc7	výšivka
a	a	k8xC	a
klobouk	klobouk	k1gInSc1	klobouk
(	(	kIx(	(
<g/>
regionálně	regionálně	k6eAd1	regionálně
klobúk	klobúk	k1gInSc1	klobúk
<g/>
,	,	kIx,	,
širák	širák	k1gInSc1	širák
<g/>
,	,	kIx,	,
šmukáč	šmukáč	k1gInSc1	šmukáč
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
====	====	k?	====
Valašsko	Valašsko	k1gNnSc1	Valašsko
====	====	k?	====
</s>
</p>
<p>
<s>
Ženský	ženský	k2eAgInSc1d1	ženský
kroj	kroj	k1gInSc1	kroj
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
rubáče	rubáče	k?	rubáče
(	(	kIx(	(
<g/>
nejspodnější	spodní	k2eAgFnSc6d3	nejspodnější
části	část	k1gFnSc6	část
<g/>
)	)	kIx)	)
a	a	k8xC	a
rukávců	rukávec	k1gInPc2	rukávec
(	(	kIx(	(
<g/>
vyšité	vyšitý	k2eAgFnPc4d1	vyšitá
na	na	k7c6	na
koncích	konec	k1gInPc6	konec
rukávů	rukáv	k1gInPc2	rukáv
a	a	k8xC	a
stojáčku	stojáček	k1gInSc2	stojáček
kolem	kolem	k7c2	kolem
krku	krk	k1gInSc2	krk
modrou	modrý	k2eAgFnSc7d1	modrá
výšivkou	výšivka	k1gFnSc7	výšivka
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
opatřené	opatřený	k2eAgNnSc1d1	opatřené
bílou	bílý	k2eAgFnSc7d1	bílá
krajkou	krajka	k1gFnSc7	krajka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
spíná	spínat	k5eAaImIp3nS	spínat
kotula	kotula	k1gFnSc1	kotula
(	(	kIx(	(
<g/>
brož	brož	k1gFnSc1	brož
z	z	k7c2	z
perleti	perleť	k1gFnSc2	perleť
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
se	se	k3xPyFc4	se
obléká	oblékat	k5eAaImIp3nS	oblékat
kordulka	kordulka	k1gFnSc1	kordulka
(	(	kIx(	(
<g/>
vesta	vesta	k1gFnSc1	vesta
<g/>
)	)	kIx)	)
červené	červený	k2eAgFnPc1d1	červená
barvy	barva	k1gFnPc1	barva
zdobená	zdobený	k2eAgFnSc1d1	zdobená
po	po	k7c6	po
jedné	jeden	k4xCgFnSc6	jeden
nebo	nebo	k8xC	nebo
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
řadou	řada	k1gFnSc7	řada
knoflíků	knoflík	k1gInPc2	knoflík
<g/>
,	,	kIx,	,
okraje	okraj	k1gInPc1	okraj
jsou	být	k5eAaImIp3nP	být
lemovány	lemován	k2eAgInPc1d1	lemován
zelenou	zelený	k2eAgFnSc7d1	zelená
portou	porta	k1gFnSc7	porta
<g/>
.	.	kIx.	.
</s>
<s>
Spodní	spodní	k2eAgFnSc1d1	spodní
část	část	k1gFnSc1	část
kroje	kroj	k1gInSc2	kroj
tvoří	tvořit	k5eAaImIp3nS	tvořit
zadní	zadní	k2eAgFnPc4d1	zadní
sukně	sukně	k1gFnPc4	sukně
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
kasanka	kasanka	k?	kasanka
černé	černá	k1gFnSc2	černá
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
bílé	bílý	k2eAgFnPc4d1	bílá
barvy	barva	k1gFnPc4	barva
a	a	k8xC	a
fěrtoch	fěrtoch	k1gInSc4	fěrtoch
(	(	kIx(	(
<g/>
přední	přední	k2eAgFnPc4d1	přední
sukně	sukně	k1gFnPc4	sukně
<g/>
)	)	kIx)	)
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
oblastí	oblast	k1gFnPc2	oblast
s	s	k7c7	s
modrotiskovým	modrotiskový	k2eAgInSc7d1	modrotiskový
vzorem	vzor	k1gInSc7	vzor
<g/>
.	.	kIx.	.
</s>
<s>
Obuví	obuv	k1gFnSc7	obuv
ke	k	k7c3	k
kroji	kroj	k1gInSc3	kroj
jsou	být	k5eAaImIp3nP	být
krpce	krpec	k1gInPc4	krpec
(	(	kIx(	(
<g/>
kožená	kožený	k2eAgFnSc1d1	kožená
nízká	nízký	k2eAgFnSc1d1	nízká
obuv	obuv	k1gFnSc1	obuv
s	s	k7c7	s
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
řemínkem	řemínek	k1gInSc7	řemínek
pro	pro	k7c4	pro
upevnění	upevnění	k1gNnSc4	upevnění
kolem	kolem	k7c2	kolem
lýtka	lýtko	k1gNnSc2	lýtko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
do	do	k7c2	do
nichž	jenž	k3xRgFnPc2	jenž
se	se	k3xPyFc4	se
nosí	nosit	k5eAaImIp3nP	nosit
vlněné	vlněný	k2eAgFnPc1d1	vlněná
ponožky	ponožka	k1gFnPc1	ponožka
s	s	k7c7	s
vyplétaným	vyplétaný	k2eAgInSc7d1	vyplétaný
vzorem	vzor	k1gInSc7	vzor
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
červené	červený	k2eAgInPc4d1	červený
semišové	semišový	k2eAgInPc4d1	semišový
střevíce	střevíc	k1gInPc4	střevíc
<g/>
,	,	kIx,	,
či	či	k8xC	či
vyšší	vysoký	k2eAgFnSc1d2	vyšší
(	(	kIx(	(
<g/>
nad	nad	k7c4	nad
kotník	kotník	k1gInSc4	kotník
<g/>
)	)	kIx)	)
černé	černý	k2eAgInPc1d1	černý
šněrovací	šněrovací	k2eAgInPc1d1	šněrovací
střevíce	střevíc	k1gInPc1	střevíc
<g/>
.	.	kIx.	.
</s>
<s>
Hlava	hlava	k1gFnSc1	hlava
se	se	k3xPyFc4	se
zakrývá	zakrývat	k5eAaImIp3nS	zakrývat
šátkem	šátek	k1gInSc7	šátek
(	(	kIx(	(
<g/>
červený	červený	k2eAgInSc1d1	červený
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
bílý	bílý	k1gMnSc1	bílý
dle	dle	k7c2	dle
regionu	region	k1gInSc2	region
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
kroje	kroj	k1gInSc2	kroj
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vyšívaný	vyšívaný	k2eAgInSc4d1	vyšívaný
kapesníček	kapesníček	k1gInSc4	kapesníček
za	za	k7c7	za
pasem	pas	k1gInSc7	pas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
mužskému	mužský	k2eAgInSc3d1	mužský
kroji	kroj	k1gInSc3	kroj
patří	patřit	k5eAaImIp3nS	patřit
plátěná	plátěný	k2eAgFnSc1d1	plátěná
košile	košile	k1gFnSc1	košile
s	s	k7c7	s
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
modrou	modrý	k2eAgFnSc7d1	modrá
geometrickou	geometrický	k2eAgFnSc7d1	geometrická
<g/>
)	)	kIx)	)
výšivkou	výšivka	k1gFnSc7	výšivka
zapínaná	zapínaný	k2eAgNnPc1d1	zapínané
na	na	k7c4	na
kotulu	kotula	k1gFnSc4	kotula
(	(	kIx(	(
<g/>
brož	brož	k1gFnSc4	brož
z	z	k7c2	z
perleti	perleť	k1gFnSc2	perleť
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
ni	on	k3xPp3gFnSc4	on
se	se	k3xPyFc4	se
převléká	převlékat	k5eAaImIp3nS	převlékat
brunclek	brunclek	k1gInSc1	brunclek
(	(	kIx(	(
<g/>
vesta	vesta	k1gFnSc1	vesta
<g/>
)	)	kIx)	)
červené	červený	k2eAgFnPc1d1	červená
<g/>
,	,	kIx,	,
zelené	zelený	k2eAgFnPc1d1	zelená
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
bílé	bílý	k2eAgFnPc1d1	bílá
barvy	barva	k1gFnPc1	barva
dle	dle	k7c2	dle
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
kroje	kroj	k1gInSc2	kroj
jsou	být	k5eAaImIp3nP	být
vlněné	vlněný	k2eAgFnPc4d1	vlněná
nohavice	nohavice	k1gFnPc4	nohavice
(	(	kIx(	(
<g/>
kalhoty	kalhoty	k1gFnPc4	kalhoty
<g/>
)	)	kIx)	)
bílé	bílý	k2eAgFnPc4d1	bílá
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
vínové	vínový	k2eAgFnPc1d1	vínová
barvy	barva	k1gFnPc1	barva
dle	dle	k7c2	dle
regionálního	regionální	k2eAgInSc2d1	regionální
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
jsou	být	k5eAaImIp3nP	být
přepásány	přepásat	k5eAaPmNgFnP	přepásat
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
<g/>
,	,	kIx,	,
několikrát	několikrát	k6eAd1	několikrát
kolem	kolem	k6eAd1	kolem
omotaným	omotaný	k2eAgInSc7d1	omotaný
koženým	kožený	k2eAgInSc7d1	kožený
řemenem	řemen	k1gInSc7	řemen
<g/>
.	.	kIx.	.
</s>
<s>
Obuví	obuv	k1gFnSc7	obuv
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
kroji	kroj	k1gInSc3	kroj
jsou	být	k5eAaImIp3nP	být
buď	buď	k8xC	buď
černé	černý	k2eAgFnPc1d1	černá
vysoké	vysoký	k2eAgFnPc1d1	vysoká
boty	bota	k1gFnPc1	bota
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
častěji	často	k6eAd2	často
krpce	krpec	k1gInSc2	krpec
(	(	kIx(	(
<g/>
kožená	kožený	k2eAgFnSc1d1	kožená
nízká	nízký	k2eAgFnSc1d1	nízká
obuv	obuv	k1gFnSc1	obuv
s	s	k7c7	s
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
řemínkem	řemínek	k1gInSc7	řemínek
pro	pro	k7c4	pro
upevnění	upevnění	k1gNnSc4	upevnění
kolem	kolem	k7c2	kolem
lýtka	lýtko	k1gNnSc2	lýtko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
do	do	k7c2	do
nichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
nosí	nosit	k5eAaImIp3nS	nosit
kopica	kopic	k2eAgFnSc1d1	kopic
(	(	kIx(	(
<g/>
vlněná	vlněný	k2eAgFnSc1d1	vlněná
zapínací	zapínací	k2eAgFnSc1d1	zapínací
"	"	kIx"	"
<g/>
ponožka	ponožka	k1gFnSc1	ponožka
<g/>
"	"	kIx"	"
podobná	podobný	k2eAgFnSc1d1	podobná
papuči	papuče	k1gFnSc4	papuče
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
kroje	kroj	k1gInSc2	kroj
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vysoký	vysoký	k2eAgInSc1d1	vysoký
černý	černý	k2eAgInSc1d1	černý
klobouk	klobouk	k1gInSc1	klobouk
s	s	k7c7	s
vyšívanou	vyšívaný	k2eAgFnSc7d1	vyšívaná
pentlí	pentle	k1gFnSc7	pentle
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Valašský	valašský	k2eAgInSc1d1	valašský
kroj	kroj	k1gInSc1	kroj
se	se	k3xPyFc4	se
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
podoblasti	podoblast	k1gFnSc6	podoblast
(	(	kIx(	(
<g/>
Zlínsko	Zlínsko	k1gNnSc1	Zlínsko
<g/>
,	,	kIx,	,
Vizovicko	Vizovicko	k1gNnSc1	Vizovicko
<g/>
,	,	kIx,	,
Rožnovsko	Rožnovsko	k1gNnSc1	Rožnovsko
<g/>
,	,	kIx,	,
Valašskokloboucko	Valašskokloboucko	k1gNnSc1	Valašskokloboucko
<g/>
,	,	kIx,	,
Valašskomeziříčsko	Valašskomeziříčsko	k1gNnSc1	Valašskomeziříčsko
<g/>
,	,	kIx,	,
Frenštátsko	Frenštátsko	k1gNnSc1	Frenštátsko
a	a	k8xC	a
Vsacko	Vsacko	k1gNnSc1	Vsacko
<g/>
)	)	kIx)	)
tohoto	tento	k3xDgInSc2	tento
regionu	region	k1gInSc2	region
objevuje	objevovat	k5eAaImIp3nS	objevovat
v	v	k7c6	v
několika	několik	k4yIc6	několik
variantách	varianta	k1gFnPc6	varianta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Slezsko	Slezsko	k1gNnSc4	Slezsko
===	===	k?	===
</s>
</p>
<p>
<s>
Slezské	slezský	k2eAgInPc1d1	slezský
kroje	kroj	k1gInPc1	kroj
jsou	být	k5eAaImIp3nP	být
mnohdy	mnohdy	k6eAd1	mnohdy
inspirovány	inspirovat	k5eAaBmNgInP	inspirovat
kroji	kroj	k1gInPc7	kroj
polskými	polský	k2eAgInPc7d1	polský
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
se	se	k3xPyFc4	se
na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
území	území	k1gNnSc6	území
zachovaly	zachovat	k5eAaPmAgFnP	zachovat
spíše	spíše	k9	spíše
ženské	ženský	k2eAgInPc4d1	ženský
kroje	kroj	k1gInPc4	kroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Lašsko	Lašsko	k1gNnSc1	Lašsko
====	====	k?	====
</s>
</p>
<p>
<s>
Ženský	ženský	k2eAgInSc1d1	ženský
kroj	kroj	k1gInSc1	kroj
tvoří	tvořit	k5eAaImIp3nS	tvořit
spodní	spodní	k2eAgFnSc1d1	spodní
košilka	košilka	k1gFnSc1	košilka
z	z	k7c2	z
bílého	bílý	k2eAgNnSc2d1	bílé
plátna	plátno	k1gNnSc2	plátno
<g/>
,	,	kIx,	,
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
se	se	k3xPyFc4	se
navléká	navlékat	k5eAaImIp3nS	navlékat
kabotek	kabotek	k1gInSc1	kabotek
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
obdoba	obdoba	k1gFnSc1	obdoba
rukávců	rukávec	k1gInPc2	rukávec
<g/>
,	,	kIx,	,
zdobený	zdobený	k2eAgInSc1d1	zdobený
bílou	bílý	k2eAgFnSc7d1	bílá
výšivkou	výšivka	k1gFnSc7	výšivka
nebo	nebo	k8xC	nebo
sámkováním	sámkování	k1gNnSc7	sámkování
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
ke	k	k7c3	k
kroji	kroj	k1gInSc3	kroj
patří	patřit	k5eAaImIp3nS	patřit
tzv.	tzv.	kA	tzv.
lajbik	lajbik	k1gInSc1	lajbik
(	(	kIx(	(
<g/>
vesta	vesta	k1gFnSc1	vesta
<g/>
)	)	kIx)	)
sametová	sametový	k2eAgFnSc1d1	sametová
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
soukenná	soukenný	k2eAgFnSc1d1	soukenná
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
kraje	kraj	k1gInSc2	kraj
tmavá	tmavý	k2eAgFnSc1d1	tmavá
nebo	nebo	k8xC	nebo
světlá	světlý	k2eAgFnSc1d1	světlá
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
regionální	regionální	k2eAgFnPc1d1	regionální
varianty	varianta	k1gFnPc1	varianta
mají	mít	k5eAaImIp3nP	mít
tuto	tento	k3xDgFnSc4	tento
vestu	vesta	k1gFnSc4	vesta
sešitou	sešitý	k2eAgFnSc4d1	sešitá
se	s	k7c7	s
sukní	sukně	k1gFnSc7	sukně
<g/>
.	.	kIx.	.
</s>
<s>
Spodní	spodní	k2eAgFnSc1d1	spodní
část	část	k1gFnSc1	část
tvoří	tvořit	k5eAaImIp3nS	tvořit
několik	několik	k4yIc4	několik
naškrobených	naškrobený	k2eAgFnPc2d1	naškrobená
spodniček	spodnička	k1gFnPc2	spodnička
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
ně	on	k3xPp3gMnPc4	on
se	se	k3xPyFc4	se
navléká	navlékat	k5eAaImIp3nS	navlékat
samotná	samotný	k2eAgFnSc1d1	samotná
sukně	sukně	k1gFnSc1	sukně
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
se	se	k3xPyFc4	se
obléká	oblékat	k5eAaImIp3nS	oblékat
zástěrka	zástěrka	k1gFnSc1	zástěrka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
plátěná	plátěný	k2eAgFnSc1d1	plátěná
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
barevná	barevný	k2eAgFnSc1d1	barevná
hedvábná	hedvábný	k2eAgFnSc1d1	hedvábná
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
kroje	kroj	k1gInSc2	kroj
je	být	k5eAaImIp3nS	být
vyšívaný	vyšívaný	k2eAgInSc4d1	vyšívaný
čepec	čepec	k1gInSc4	čepec
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
čopka	čopek	k1gInSc2	čopek
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
který	který	k3yRgInSc4	který
se	se	k3xPyFc4	se
dává	dávat	k5eAaImIp3nS	dávat
bohatě	bohatě	k6eAd1	bohatě
vyšívaný	vyšívaný	k2eAgInSc4d1	vyšívaný
šátek	šátek	k1gInSc4	šátek
<g/>
.	.	kIx.	.
</s>
<s>
Obuví	obuv	k1gFnSc7	obuv
ke	k	k7c3	k
kroji	kroj	k1gInSc3	kroj
jsou	být	k5eAaImIp3nP	být
černé	černý	k2eAgInPc1d1	černý
střevíce	střevíc	k1gInPc1	střevíc
nebo	nebo	k8xC	nebo
šněrovací	šněrovací	k2eAgFnPc1d1	šněrovací
boty	bota	k1gFnPc1	bota
<g/>
,	,	kIx,	,
do	do	k7c2	do
nichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
nosí	nosit	k5eAaImIp3nS	nosit
bíle	bíle	k6eAd1	bíle
bavlněné	bavlněný	k2eAgFnPc4d1	bavlněná
punčochy	punčocha	k1gFnPc4	punčocha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mužský	mužský	k2eAgInSc1d1	mužský
kroj	kroj	k1gInSc1	kroj
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
byl	být	k5eAaImAgInS	být
postupně	postupně	k6eAd1	postupně
vytlačen	vytlačen	k2eAgInSc1d1	vytlačen
městským	městský	k2eAgNnSc7d1	Městské
oblečením	oblečení	k1gNnSc7	oblečení
<g/>
,	,	kIx,	,
vypadá	vypadat	k5eAaPmIp3nS	vypadat
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
Plátěná	plátěný	k2eAgFnSc1d1	plátěná
košile	košile	k1gFnSc1	košile
s	s	k7c7	s
vyšívaným	vyšívaný	k2eAgInSc7d1	vyšívaný
límečkem	límeček	k1gInSc7	límeček
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
krku	krk	k1gInSc2	krk
se	se	k3xPyFc4	se
váže	vázat	k5eAaImIp3nS	vázat
tzv.	tzv.	kA	tzv.
lipščunk	lipščunk	k1gInSc1	lipščunk
(	(	kIx(	(
<g/>
šátek	šátek	k1gInSc1	šátek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
košili	košile	k1gFnSc4	košile
se	se	k3xPyFc4	se
nosí	nosit	k5eAaImIp3nS	nosit
bruslek	bruslek	k1gInSc1	bruslek
(	(	kIx(	(
<g/>
vesta	vesta	k1gFnSc1	vesta
<g/>
)	)	kIx)	)
z	z	k7c2	z
fialově	fialově	k6eAd1	fialově
červeného	červený	k2eAgInSc2d1	červený
<g/>
,	,	kIx,	,
černého	černý	k2eAgInSc2d1	černý
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
modrošedého	modrošedý	k2eAgNnSc2d1	modrošedé
sukna	sukno	k1gNnSc2	sukno
(	(	kIx(	(
<g/>
opět	opět	k6eAd1	opět
dle	dle	k7c2	dle
oblastní	oblastní	k2eAgFnSc2d1	oblastní
varianty	varianta	k1gFnSc2	varianta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zdobená	zdobený	k2eAgFnSc1d1	zdobená
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
řadou	řada	k1gFnSc7	řada
knoflíků	knoflík	k1gInPc2	knoflík
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
kroje	kroj	k1gInSc2	kroj
jsou	být	k5eAaImIp3nP	být
galaty	galaty	k?	galaty
(	(	kIx(	(
<g/>
kalhoty	kalhoty	k1gFnPc4	kalhoty
<g/>
)	)	kIx)	)
z	z	k7c2	z
modrého	modrý	k2eAgNnSc2d1	modré
sukna	sukno	k1gNnSc2	sukno
<g/>
.	.	kIx.	.
</s>
<s>
Obuví	obuv	k1gFnSc7	obuv
ke	k	k7c3	k
kroji	kroj	k1gInSc3	kroj
jsou	být	k5eAaImIp3nP	být
černé	černý	k2eAgFnPc4d1	černá
vysoké	vysoký	k2eAgFnPc4d1	vysoká
boty	bota	k1gFnPc4	bota
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
kroji	kroj	k1gInSc3	kroj
patří	patřit	k5eAaImIp3nS	patřit
také	také	k9	také
klobouk	klobouk	k1gInSc1	klobouk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
I	i	k9	i
Slezsko	Slezsko	k1gNnSc1	Slezsko
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgFnPc4	svůj
podoblasti	podoblast	k1gFnPc4	podoblast
(	(	kIx(	(
<g/>
Ostravsko	Ostravsko	k1gNnSc4	Ostravsko
a	a	k8xC	a
Frýdecko	Frýdecko	k1gNnSc4	Frýdecko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
kroje	kroj	k1gInPc1	kroj
nepatrně	patrně	k6eNd1	patrně
liší	lišit	k5eAaImIp3nP	lišit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Těšínské	Těšínské	k2eAgNnSc4d1	Těšínské
Slezsko	Slezsko	k1gNnSc4	Slezsko
====	====	k?	====
</s>
</p>
<p>
<s>
Ženský	ženský	k2eAgInSc1d1	ženský
kroj	kroj	k1gInSc1	kroj
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
životku	životek	k1gInSc2	životek
(	(	kIx(	(
<g/>
živůtku	živůtek	k1gInSc2	živůtek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
sešit	sešit	k1gInSc1	sešit
se	s	k7c7	s
sukní	sukně	k1gFnSc7	sukně
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
kamlotka	kamlotka	k1gFnSc1	kamlotka
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
životek	životek	k1gInSc1	životek
je	být	k5eAaImIp3nS	být
zdoben	zdobit	k5eAaImNgInS	zdobit
stříbrnými	stříbrný	k2eAgFnPc7d1	stříbrná
nitmi	nit	k1gFnPc7	nit
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
rostlinnými	rostlinný	k2eAgInPc7d1	rostlinný
ornamenty	ornament	k1gInPc7	ornament
na	na	k7c6	na
zádech	záda	k1gNnPc6	záda
<g/>
,	,	kIx,	,
v	v	k7c6	v
přední	přední	k2eAgFnSc6d1	přední
části	část	k1gFnSc6	část
pak	pak	k6eAd1	pak
lemován	lemovat	k5eAaImNgInS	lemovat
širokou	široký	k2eAgFnSc7d1	široká
zlatou	zlatý	k2eAgFnSc7d1	zlatá
portou	porta	k1gFnSc7	porta
<g/>
,	,	kIx,	,
časem	časem	k6eAd1	časem
se	se	k3xPyFc4	se
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
našívání	našívání	k1gNnSc1	našívání
hočků	hoček	k1gMnPc2	hoček
(	(	kIx(	(
<g/>
háčků	háček	k1gMnPc2	háček
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterými	který	k3yIgInPc7	který
se	se	k3xPyFc4	se
provlékal	provlékat	k5eAaImAgInS	provlékat
stříbrný	stříbrný	k2eAgInSc1d1	stříbrný
řetízek	řetízek	k1gInSc1	řetízek
<g/>
.	.	kIx.	.
</s>
<s>
Spodní	spodní	k2eAgFnSc7d1	spodní
součástí	součást	k1gFnSc7	součást
kroje	kroj	k1gInSc2	kroj
je	být	k5eAaImIp3nS	být
těsnocha	těsnoch	k1gMnSc4	těsnoch
(	(	kIx(	(
<g/>
spodní	spodní	k2eAgFnSc2d1	spodní
plátěné	plátěný	k2eAgFnSc2d1	plátěná
šaty	šata	k1gFnSc2	šata
na	na	k7c4	na
ramínka	ramínko	k1gNnPc4	ramínko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
obléká	oblékat	k5eAaImIp3nS	oblékat
košulka	košulka	k1gFnSc1	košulka
<g/>
,	,	kIx,	,
či	či	k8xC	či
kabotek	kabotek	k1gInSc1	kabotek
(	(	kIx(	(
<g/>
rukávce	rukávec	k1gInPc1	rukávec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sukně	sukně	k1gFnSc1	sukně
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
zadní	zadní	k2eAgFnSc1d1	zadní
je	být	k5eAaImIp3nS	být
plátěná	plátěný	k2eAgFnSc1d1	plátěná
<g/>
,	,	kIx,	,
přední	přední	k2eAgFnSc1d1	přední
zástěrka	zástěrka	k1gFnSc1	zástěrka
je	být	k5eAaImIp3nS	být
batistová	batistový	k2eAgFnSc1d1	Batistová
nebo	nebo	k8xC	nebo
hedvábná	hedvábný	k2eAgFnSc1d1	hedvábná
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
patří	patřit	k5eAaImIp3nS	patřit
šátek	šátek	k1gInSc1	šátek
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
šatka	šatka	k1gFnSc1	šatka
dle	dle	k7c2	dle
úvazu	úvaz	k1gInSc2	úvaz
rožková	rožkový	k2eAgFnSc1d1	rožkový
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
spuščková	spuščková	k1gFnSc1	spuščková
<g/>
,	,	kIx,	,
pod	pod	k7c4	pod
něj	on	k3xPp3gInSc4	on
vyšívaný	vyšívaný	k2eAgInSc4d1	vyšívaný
čepec	čepec	k1gInSc4	čepec
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
jsou	být	k5eAaImIp3nP	být
černé	černý	k2eAgInPc1d1	černý
střevíce	střevíc	k1gInPc1	střevíc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mužský	mužský	k2eAgInSc1d1	mužský
kroj	kroj	k1gInSc1	kroj
(	(	kIx(	(
<g/>
brzy	brzy	k6eAd1	brzy
vymizel	vymizet	k5eAaPmAgMnS	vymizet
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
košile	košile	k1gFnSc2	košile
a	a	k8xC	a
soukenné	soukenný	k2eAgFnSc2d1	soukenná
vesty	vesta	k1gFnSc2	vesta
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
doplňuje	doplňovat	k5eAaImIp3nS	doplňovat
černý	černý	k2eAgInSc4d1	černý
šátek	šátek	k1gInSc4	šátek
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhé	Dlouhé	k2eAgFnPc1d1	Dlouhé
kalhoty	kalhoty	k1gFnPc1	kalhoty
se	se	k3xPyFc4	se
zastrkávají	zastrkávat	k5eAaImIp3nP	zastrkávat
do	do	k7c2	do
vysokých	vysoký	k2eAgFnPc2d1	vysoká
bot	bota	k1gFnPc2	bota
černé	černý	k2eAgFnSc2d1	černá
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Celému	celý	k2eAgInSc3d1	celý
kroji	kroj	k1gInSc3	kroj
dominuje	dominovat	k5eAaImIp3nS	dominovat
modré	modrý	k2eAgNnSc1d1	modré
sukno	sukno	k1gNnSc1	sukno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Opavské	opavský	k2eAgNnSc4d1	Opavské
Slezsko	Slezsko	k1gNnSc4	Slezsko
====	====	k?	====
</s>
</p>
<p>
<s>
Ženský	ženský	k2eAgInSc1d1	ženský
kroj	kroj	k1gInSc1	kroj
tvoří	tvořit	k5eAaImIp3nS	tvořit
rubáč	rubáč	k?	rubáč
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
se	se	k3xPyFc4	se
oblékají	oblékat	k5eAaImIp3nP	oblékat
rukávce	rukávec	k1gInPc1	rukávec
na	na	k7c6	na
koncích	konec	k1gInPc6	konec
rukávů	rukáv	k1gInPc2	rukáv
s	s	k7c7	s
drobnou	drobný	k2eAgFnSc7d1	drobná
výšivkou	výšivka	k1gFnSc7	výšivka
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
zoubkovanou	zoubkovaný	k2eAgFnSc7d1	zoubkovaná
krajkou	krajka	k1gFnSc7	krajka
<g/>
.	.	kIx.	.
</s>
<s>
Sukně	sukně	k1gFnSc1	sukně
se	se	k3xPyFc4	se
skládala	skládat	k5eAaImAgFnS	skládat
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
částí	část	k1gFnPc2	část
<g/>
:	:	kIx,	:
drajdrutky	drajdrutka	k1gFnPc1	drajdrutka
(	(	kIx(	(
<g/>
vrchní	vrchní	k2eAgFnSc1d1	vrchní
sukně	sukně	k1gFnSc1	sukně
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vlněná	vlněný	k2eAgFnSc1d1	vlněná
a	a	k8xC	a
barevně	barevně	k6eAd1	barevně
velmi	velmi	k6eAd1	velmi
rozličná	rozličný	k2eAgFnSc1d1	rozličná
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c4	pod
ně	on	k3xPp3gMnPc4	on
se	se	k3xPyFc4	se
obléká	oblékat	k5eAaImIp3nS	oblékat
několik	několik	k4yIc1	několik
naškrobených	naškrobený	k2eAgFnPc2d1	naškrobená
spodnic	spodnice	k1gFnPc2	spodnice
<g/>
.	.	kIx.	.
</s>
<s>
Zástěrka	zástěrka	k1gFnSc1	zástěrka
byla	být	k5eAaImAgFnS	být
mnohdy	mnohdy	k6eAd1	mnohdy
přišita	přišit	k2eAgFnSc1d1	přišita
k	k	k7c3	k
lajbiku	lajbik	k1gInSc3	lajbik
(	(	kIx(	(
<g/>
vestě	vesta	k1gFnSc3	vesta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
kroje	kroj	k1gInSc2	kroj
je	být	k5eAaImIp3nS	být
kaptur	kaptura	k1gFnPc2	kaptura
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
karkule	karkule	k1gFnSc1	karkule
(	(	kIx(	(
<g/>
čepec	čepec	k1gInSc1	čepec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
bohatě	bohatě	k6eAd1	bohatě
zdoben	zdoben	k2eAgInSc1d1	zdoben
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mužský	mužský	k2eAgInSc1d1	mužský
kroj	kroj	k1gInSc1	kroj
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
bílé	bílý	k2eAgFnSc2d1	bílá
plátěné	plátěný	k2eAgFnSc2d1	plátěná
košile	košile	k1gFnSc2	košile
<g/>
,	,	kIx,	,
šátku	šátek	k1gInSc2	šátek
kolem	kolem	k7c2	kolem
krku	krk	k1gInSc2	krk
a	a	k8xC	a
červené	červený	k2eAgFnSc2d1	červená
soukenné	soukenný	k2eAgFnSc2d1	soukenná
vesty	vesta	k1gFnSc2	vesta
s	s	k7c7	s
jednou	jeden	k4xCgFnSc7	jeden
řadou	řada	k1gFnSc7	řada
knoflíků	knoflík	k1gInPc2	knoflík
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
kroje	kroj	k1gInSc2	kroj
jsou	být	k5eAaImIp3nP	být
kalhoty	kalhoty	k1gFnPc1	kalhoty
z	z	k7c2	z
manšestru	manšestr	k1gInSc2	manšestr
<g/>
,	,	kIx,	,
plátna	plátno	k1gNnSc2	plátno
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
sukna	sukno	k1gNnPc1	sukno
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
jsou	být	k5eAaImIp3nP	být
ale	ale	k8xC	ale
tmavé	tmavý	k2eAgFnPc1d1	tmavá
barvy	barva	k1gFnPc1	barva
<g/>
.	.	kIx.	.
</s>
<s>
Zasunují	zasunovat	k5eAaImIp3nP	zasunovat
se	se	k3xPyFc4	se
do	do	k7c2	do
holínek	holínka	k1gFnPc2	holínka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
==	==	k?	==
</s>
</p>
<p>
<s>
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
má	mít	k5eAaImIp3nS	mít
bohatou	bohatý	k2eAgFnSc4d1	bohatá
tradici	tradice	k1gFnSc4	tradice
lidových	lidový	k2eAgInPc2d1	lidový
krojů	kroj	k1gInPc2	kroj
<g/>
,	,	kIx,	,
známá	známá	k1gFnSc1	známá
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
vyšyvanka	vyšyvanka	k1gFnSc1	vyšyvanka
<g/>
,	,	kIx,	,
košile	košile	k1gFnSc1	košile
zdobená	zdobený	k2eAgFnSc1d1	zdobená
vyšívanými	vyšívaný	k2eAgInPc7d1	vyšívaný
ornamenty	ornament	k1gInPc7	ornament
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BROUČEK	Brouček	k1gMnSc1	Brouček
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
a	a	k8xC	a
Richard	Richard	k1gMnSc1	Richard
JEŘÁBEK	Jeřábek	k1gMnSc1	Jeřábek
(	(	kIx(	(
<g/>
eds	eds	k?	eds
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lidová	lidový	k2eAgFnSc1d1	lidová
kultura	kultura	k1gFnSc1	kultura
<g/>
:	:	kIx,	:
Národopisná	národopisný	k2eAgFnSc1d1	národopisná
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
Moravy	Morava	k1gFnSc2	Morava
a	a	k8xC	a
Slezska	Slezsko	k1gNnSc2	Slezsko
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
a	a	k8xC	a
3	[number]	k4	3
<g/>
.	.	kIx.	.
svazek	svazek	k1gInSc1	svazek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HABARTOVÁ	Habartová	k1gFnSc1	Habartová
<g/>
,	,	kIx,	,
Romana	Romana	k1gFnSc1	Romana
(	(	kIx(	(
<g/>
ed	ed	k?	ed
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
paletě	paleta	k1gFnSc6	paleta
krojů	kroj	k1gInPc2	kroj
<g/>
.	.	kIx.	.
</s>
<s>
Uherské	uherský	k2eAgNnSc1d1	Uherské
Hradiště	Hradiště	k1gNnSc1	Hradiště
<g/>
:	:	kIx,	:
Nadace	nadace	k1gFnSc1	nadace
Děti-kultura-sport	Dětiulturaport	k1gInSc1	Děti-kultura-sport
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
JEŘÁBKOVÁ	Jeřábková	k1gFnSc1	Jeřábková
<g/>
,	,	kIx,	,
Alena	Alena	k1gFnSc1	Alena
(	(	kIx(	(
<g/>
ed	ed	k?	ed
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lidová	lidový	k2eAgFnSc1d1	lidová
oděvní	oděvní	k2eAgFnSc1d1	oděvní
kultura	kultura	k1gFnSc1	kultura
<g/>
:	:	kIx,	:
Příspěvky	příspěvek	k1gInPc1	příspěvek
k	k	k7c3	k
ikonografii	ikonografie	k1gFnSc3	ikonografie
<g/>
,	,	kIx,	,
typologii	typologie	k1gFnSc3	typologie
a	a	k8xC	a
metodologii	metodologie	k1gFnSc3	metodologie
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
STAŇKOVÁ	Staňková	k1gFnSc1	Staňková
<g/>
,	,	kIx,	,
Jitka	Jitka	k1gFnSc1	Jitka
a	a	k8xC	a
Ludvík	Ludvík	k1gMnSc1	Ludvík
BARAN	Baran	k1gMnSc1	Baran
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgInPc1d1	český
a	a	k8xC	a
slovenské	slovenský	k2eAgInPc1d1	slovenský
lidové	lidový	k2eAgInPc1d1	lidový
kroje	kroj	k1gInPc1	kroj
<g/>
:	:	kIx,	:
Díl	díl	k1gInSc1	díl
I.	I.	kA	I.
Čechy	Čechy	k1gFnPc4	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ottovo	Ottův	k2eAgNnSc1d1	Ottovo
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
STRÁNSKÁ	Stránská	k1gFnSc1	Stránská
<g/>
,	,	kIx,	,
Drahomíra	Drahomíra	k1gFnSc1	Drahomíra
<g/>
.	.	kIx.	.
</s>
<s>
Lidové	lidový	k2eAgInPc1d1	lidový
kroje	kroj	k1gInPc1	kroj
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ottovo	Ottův	k2eAgNnSc1d1	Ottovo
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
1949	[number]	k4	1949
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Etnografický	etnografický	k2eAgInSc1d1	etnografický
region	region	k1gInSc1	region
<g/>
:	:	kIx,	:
Folklorní	folklorní	k2eAgNnPc1d1	folklorní
sdružení	sdružení	k1gNnPc1	sdružení
ČR	ČR	kA	ČR
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2016	[number]	k4	2016
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
18	[number]	k4	18
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgInPc4d1	dostupný
z	z	k7c2	z
<g/>
:	:	kIx,	:
http://archiv.fos.cz/encyklopedie/seznam.phtml?id_statu=1&	[url]	k?	http://archiv.fos.cz/encyklopedie/seznam.phtml?id_statu=1&
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Sarafán	sarafán	k1gInSc1	sarafán
</s>
</p>
<p>
<s>
Kanduš	Kanduš	k?	Kanduš
</s>
</p>
<p>
<s>
Dirndl	dirndl	k1gInSc1	dirndl
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
kroj	kroj	k1gInSc1	kroj
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Baráčníci	baráčník	k1gMnPc1	baráčník
<g/>
:	:	kIx,	:
Národní	národní	k2eAgFnSc1d1	národní
kroj	kroj	k1gInSc1	kroj
-	-	kIx~	-
hrdost	hrdost	k1gFnSc1	hrdost
národa	národ	k1gInSc2	národ
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
<g/>
:	:	kIx,	:
Kroje	kroj	k1gInPc1	kroj
</s>
</p>
<p>
<s>
hanácký	hanácký	k2eAgInSc1d1	hanácký
kroj	kroj	k1gInSc1	kroj
</s>
</p>
<p>
<s>
Petr	Petr	k1gMnSc1	Petr
Bogatyrev	Bogatyrev	k1gFnSc1	Bogatyrev
<g/>
:	:	kIx,	:
Kroj	kroj	k1gInSc1	kroj
jako	jako	k8xC	jako
znak	znak	k1gInSc1	znak
(	(	kIx(	(
<g/>
Funkční	funkční	k2eAgNnSc1d1	funkční
a	a	k8xC	a
strukturální	strukturální	k2eAgNnSc1d1	strukturální
pojetí	pojetí	k1gNnSc1	pojetí
v	v	k7c6	v
národopisu	národopis	k1gInSc6	národopis
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
VAVROUŠEK	VAVROUŠEK	kA	VAVROUŠEK
<g/>
,	,	kIx,	,
B.	B.	kA	B.
Malebné	malebný	k2eAgNnSc1d1	malebné
Slovensko	Slovensko	k1gNnSc1	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Historické	historický	k2eAgInPc1d1	historický
a	a	k8xC	a
staviteľské	staviteľský	k2eAgInPc1d1	staviteľský
pamiatky	pamiatek	k1gInPc1	pamiatek
<g/>
,	,	kIx,	,
ukážky	ukážka	k1gFnPc1	ukážka
rázovitých	rázovitý	k2eAgNnPc2d1	rázovité
krojov	krojovo	k1gNnPc2	krojovo
<g/>
,	,	kIx,	,
stavieb	staviba	k1gFnPc2	staviba
<g/>
,	,	kIx,	,
umenia	umenium	k1gNnSc2	umenium
a	a	k8xC	a
života	život	k1gInSc2	život
ľudu	ľudus	k1gInSc2	ľudus
slovenského	slovenský	k2eAgInSc2d1	slovenský
a	a	k8xC	a
prírodnej	prírodnat	k5eAaPmRp2nS	prírodnat
krásy	krása	k1gFnPc4	krása
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
B.	B.	kA	B.
Koči	Koči	k?	Koči
<g/>
,	,	kIx,	,
1920	[number]	k4	1920
<g/>
.	.	kIx.	.
96	[number]	k4	96
s.	s.	k?	s.
-	-	kIx~	-
dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
Digitální	digitální	k2eAgFnSc6d1	digitální
knihovně	knihovna	k1gFnSc6	knihovna
UKB	UKB	kA	UKB
</s>
</p>
