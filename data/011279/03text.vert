<p>
<s>
Státní	státní	k2eAgFnPc1d1	státní
barvy	barva	k1gFnPc1	barva
jsou	být	k5eAaImIp3nP	být
jeden	jeden	k4xCgInSc4	jeden
ze	z	k7c2	z
státních	státní	k2eAgInPc2d1	státní
symbolů	symbol	k1gInPc2	symbol
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
,	,	kIx,	,
červená	červený	k2eAgFnSc1d1	červená
a	a	k8xC	a
modrá	modrý	k2eAgFnSc1d1	modrá
v	v	k7c6	v
uvedeném	uvedený	k2eAgNnSc6d1	uvedené
pořadí	pořadí	k1gNnSc6	pořadí
a	a	k8xC	a
nazývají	nazývat	k5eAaImIp3nP	nazývat
se	se	k3xPyFc4	se
trikolóra	trikolóra	k1gFnSc1	trikolóra
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
neoficiální	neoficiální	k2eAgFnSc2d1	neoficiální
interpretace	interpretace	k1gFnSc2	interpretace
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
bílá	bílý	k2eAgFnSc1d1	bílá
barva	barva	k1gFnSc1	barva
čistotu	čistota	k1gFnSc4	čistota
<g/>
,	,	kIx,	,
červená	červený	k2eAgFnSc1d1	červená
krev	krev	k1gFnSc1	krev
prolitou	prolitý	k2eAgFnSc4d1	prolitá
vlastenci	vlastenec	k1gMnPc1	vlastenec
za	za	k7c4	za
svobodu	svoboda	k1gFnSc4	svoboda
vlasti	vlast	k1gFnSc2	vlast
a	a	k8xC	a
modrá	modrat	k5eAaImIp3nS	modrat
bezmračnou	bezmračný	k2eAgFnSc4d1	bezmračná
oblohu	obloha	k1gFnSc4	obloha
<g/>
.	.	kIx.	.
</s>
<s>
Trikolóra	trikolóra	k1gFnSc1	trikolóra
zachovává	zachovávat	k5eAaImIp3nS	zachovávat
oficiální	oficiální	k2eAgNnSc4d1	oficiální
pořadí	pořadí	k1gNnSc4	pořadí
státních	státní	k2eAgFnPc2d1	státní
barev	barva	k1gFnPc2	barva
<g/>
,	,	kIx,	,
bílý	bílý	k2eAgInSc1d1	bílý
pruh	pruh	k1gInSc1	pruh
má	mít	k5eAaImIp3nS	mít
vždy	vždy	k6eAd1	vždy
nahoře	nahoře	k6eAd1	nahoře
nebo	nebo	k8xC	nebo
vlevo	vlevo	k6eAd1	vlevo
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
pozorovatele	pozorovatel	k1gMnSc2	pozorovatel
<g/>
.	.	kIx.	.
</s>
<s>
Vymezení	vymezení	k1gNnSc1	vymezení
odstínů	odstín	k1gInPc2	odstín
státních	státní	k2eAgFnPc2d1	státní
barev	barva	k1gFnPc2	barva
není	být	k5eNaImIp3nS	být
zákonem	zákon	k1gInSc7	zákon
upraveno	upraven	k2eAgNnSc1d1	upraveno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Tradičními	tradiční	k2eAgFnPc7d1	tradiční
barvami	barva	k1gFnPc7	barva
českého	český	k2eAgInSc2d1	český
soustátí	soustátí	k1gNnSc3	soustátí
jsou	být	k5eAaImIp3nP	být
bílá	bílý	k2eAgFnSc1d1	bílá
a	a	k8xC	a
červená	červený	k2eAgFnSc1d1	červená
<g/>
,	,	kIx,	,
jenže	jenže	k8xC	jenže
tyto	tento	k3xDgFnPc4	tento
barvy	barva	k1gFnPc4	barva
po	po	k7c6	po
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
používalo	používat	k5eAaImAgNnS	používat
i	i	k9	i
sousedící	sousedící	k2eAgNnSc1d1	sousedící
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
Československý	československý	k2eAgInSc1d1	československý
stát	stát	k1gInSc1	stát
navíc	navíc	k6eAd1	navíc
potřeboval	potřebovat	k5eAaImAgInS	potřebovat
barvy	barva	k1gFnPc4	barva
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
by	by	kYmCp3nP	by
reprezentovaly	reprezentovat	k5eAaImAgInP	reprezentovat
i	i	k9	i
Slováky	Slováky	k1gInPc1	Slováky
ve	v	k7c6	v
společném	společný	k2eAgInSc6d1	společný
státě	stát	k1gInSc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
tedy	tedy	k9	tedy
nutné	nutný	k2eAgNnSc1d1	nutné
připojit	připojit	k5eAaPmF	připojit
třetí	třetí	k4xOgFnSc4	třetí
barvu	barva	k1gFnSc4	barva
–	–	k?	–
modrou	modrý	k2eAgFnSc4d1	modrá
<g/>
.	.	kIx.	.
</s>
<s>
Modrá	modrý	k2eAgFnSc1d1	modrá
barva	barva	k1gFnSc1	barva
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
tradiční	tradiční	k2eAgFnSc7d1	tradiční
dvojicí	dvojice	k1gFnSc7	dvojice
vytvářela	vytvářet	k5eAaImAgFnS	vytvářet
tzv.	tzv.	kA	tzv.
slovanskou	slovanský	k2eAgFnSc4d1	Slovanská
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
ruskou	ruský	k2eAgFnSc4d1	ruská
trikolóru	trikolóra	k1gFnSc4	trikolóra
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ruské	ruský	k2eAgFnSc2d1	ruská
trikolory	trikolora	k1gFnSc2	trikolora
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
ta	ten	k3xDgFnSc1	ten
česká	český	k2eAgFnSc1d1	Česká
modrou	modrý	k2eAgFnSc4d1	modrá
barvu	barva	k1gFnSc4	barva
vespod	vespod	k6eAd1	vespod
<g/>
,	,	kIx,	,
ne	ne	k9	ne
uprostřed	uprostřed	k6eAd1	uprostřed
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
vlajky	vlajka	k1gFnPc1	vlajka
tehdejších	tehdejší	k2eAgMnPc2d1	tehdejší
spojenců	spojenec	k1gMnPc2	spojenec
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
tyto	tento	k3xDgFnPc4	tento
barvy	barva	k1gFnPc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Zvýšení	zvýšení	k1gNnSc1	zvýšení
počtu	počet	k1gInSc2	počet
státních	státní	k2eAgFnPc2d1	státní
barev	barva	k1gFnPc2	barva
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
se	se	k3xPyFc4	se
odrazilo	odrazit	k5eAaPmAgNnS	odrazit
také	také	k9	také
na	na	k7c6	na
podobě	podoba	k1gFnSc6	podoba
státní	státní	k2eAgFnSc2d1	státní
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
německé	německý	k2eAgFnSc2d1	německá
okupace	okupace	k1gFnSc2	okupace
Českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
Protektorátu	protektorát	k1gInSc2	protektorát
Čechy	Čechy	k1gFnPc1	Čechy
a	a	k8xC	a
Morava	Morava	k1gFnSc1	Morava
v	v	k7c6	v
letech	let	k1gInPc6	let
1939	[number]	k4	1939
–	–	k?	–
1945	[number]	k4	1945
byla	být	k5eAaImAgFnS	být
<g/>
,	,	kIx,	,
po	po	k7c6	po
přijetí	přijetí	k1gNnSc6	přijetí
zákona	zákon	k1gInSc2	zákon
číslo	číslo	k1gNnSc4	číslo
222	[number]	k4	222
<g/>
/	/	kIx~	/
<g/>
1939	[number]	k4	1939
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
O	o	k7c6	o
státních	státní	k2eAgInPc6d1	státní
symbolech	symbol	k1gInPc6	symbol
<g/>
,	,	kIx,	,
novou	nový	k2eAgFnSc7d1	nová
vlajkou	vlajka	k1gFnSc7	vlajka
právě	právě	k9	právě
česká	český	k2eAgFnSc1d1	Česká
trikolóra	trikolóra	k1gFnSc1	trikolóra
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
horizontálními	horizontální	k2eAgInPc7d1	horizontální
pruhy	pruh	k1gInPc7	pruh
–	–	k?	–
bílým	bílé	k1gNnSc7	bílé
<g/>
,	,	kIx,	,
červeným	červený	k2eAgNnSc7d1	červené
a	a	k8xC	a
modrým	modrý	k2eAgNnSc7d1	modré
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
osvobození	osvobození	k1gNnSc6	osvobození
se	se	k3xPyFc4	se
Československá	československý	k2eAgFnSc1d1	Československá
republika	republika	k1gFnSc1	republika
vrátila	vrátit	k5eAaPmAgFnS	vrátit
opět	opět	k6eAd1	opět
k	k	k7c3	k
bílo-červené	bílo-červený	k2eAgFnSc3d1	bílo-červená
vlajce	vlajka	k1gFnSc3	vlajka
s	s	k7c7	s
modrým	modrý	k2eAgInSc7d1	modrý
klínem	klín	k1gInSc7	klín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Se	s	k7c7	s
socialistickou	socialistický	k2eAgFnSc7d1	socialistická
ústavou	ústava	k1gFnSc7	ústava
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
státní	státní	k2eAgFnSc2d1	státní
barvy	barva	k1gFnSc2	barva
jako	jako	k8xS	jako
symbol	symbol	k1gInSc1	symbol
státu	stát	k1gInSc2	stát
zmizely	zmizet	k5eAaPmAgInP	zmizet
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
ve	v	k7c6	v
vyhrocených	vyhrocený	k2eAgFnPc6d1	vyhrocená
situacích	situace	k1gFnPc6	situace
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1968	[number]	k4	1968
<g/>
–	–	k?	–
<g/>
1969	[number]	k4	1969
a	a	k8xC	a
za	za	k7c2	za
Sametové	sametový	k2eAgFnSc2d1	sametová
revoluce	revoluce	k1gFnSc2	revoluce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
používali	používat	k5eAaImAgMnP	používat
občané	občan	k1gMnPc1	občan
k	k	k7c3	k
vyjádření	vyjádření	k1gNnSc3	vyjádření
nesouhlasu	nesouhlas	k1gInSc2	nesouhlas
se	s	k7c7	s
stávajícími	stávající	k2eAgInPc7d1	stávající
poměry	poměr	k1gInPc7	poměr
českou	český	k2eAgFnSc4d1	Česká
trikolóru	trikolóra	k1gFnSc4	trikolóra
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
samostatné	samostatný	k2eAgFnSc2d1	samostatná
ČR	ČR	kA	ČR
se	se	k3xPyFc4	se
české	český	k2eAgFnSc2d1	Česká
státní	státní	k2eAgFnSc2d1	státní
barvy	barva	k1gFnSc2	barva
opět	opět	k6eAd1	opět
staly	stát	k5eAaPmAgInP	stát
státním	státní	k2eAgInSc7d1	státní
symbolem	symbol	k1gInSc7	symbol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Používání	používání	k1gNnSc1	používání
barev	barva	k1gFnPc2	barva
==	==	k?	==
</s>
</p>
<p>
<s>
České	český	k2eAgFnPc1d1	Česká
státní	státní	k2eAgFnPc1d1	státní
barvy	barva	k1gFnPc1	barva
se	se	k3xPyFc4	se
užívají	užívat	k5eAaImIp3nP	užívat
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
trikolóry	trikolóra	k1gFnSc2	trikolóra
k	k	k7c3	k
obecnému	obecný	k2eAgNnSc3d1	obecné
označení	označení	k1gNnSc3	označení
státního	státní	k2eAgInSc2d1	státní
majetku	majetek	k1gInSc2	majetek
či	či	k8xC	či
k	k	k7c3	k
dekoračním	dekorační	k2eAgInPc3d1	dekorační
účelům	účel	k1gInPc3	účel
při	při	k7c6	při
slavnostních	slavnostní	k2eAgFnPc6d1	slavnostní
příležitostech	příležitost	k1gFnPc6	příležitost
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
na	na	k7c6	na
stuhách	stuha	k1gFnPc6	stuha
k	k	k7c3	k
medailím	medaile	k1gFnPc3	medaile
a	a	k8xC	a
smutečním	smuteční	k2eAgInPc3d1	smuteční
věncům	věnec	k1gInPc3	věnec
a	a	k8xC	a
na	na	k7c6	na
různých	různý	k2eAgFnPc6d1	různá
linkách	linka	k1gFnPc6	linka
a	a	k8xC	a
šňůrkách	šňůrka	k1gFnPc6	šňůrka
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
oslavách	oslava	k1gFnPc6	oslava
státních	státní	k2eAgInPc2d1	státní
svátků	svátek	k1gInPc2	svátek
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
významných	významný	k2eAgFnPc2d1	významná
událostí	událost	k1gFnPc2	událost
si	se	k3xPyFc3	se
je	on	k3xPp3gNnPc4	on
lidé	člověk	k1gMnPc1	člověk
připínají	připínat	k5eAaImIp3nP	připínat
na	na	k7c4	na
oděv	oděv	k1gInSc4	oděv
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zákonné	zákonný	k2eAgNnSc4d1	zákonné
ošetření	ošetření	k1gNnSc4	ošetření
==	==	k?	==
</s>
</p>
<p>
<s>
O	o	k7c6	o
způsobu	způsob	k1gInSc6	způsob
užívání	užívání	k1gNnSc2	užívání
státních	státní	k2eAgFnPc2d1	státní
barev	barva	k1gFnPc2	barva
zákon	zákon	k1gInSc1	zákon
nic	nic	k3yNnSc4	nic
nestanoví	stanovit	k5eNaPmIp3nS	stanovit
a	a	k8xC	a
pokud	pokud	k8xS	pokud
by	by	kYmCp3nP	by
tedy	tedy	k9	tedy
nebyly	být	k5eNaImAgFnP	být
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
používány	používat	k5eAaImNgFnP	používat
praeter	praeter	k1gInSc1	praeter
legem	lego	k1gNnSc7	lego
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnSc1	jejich
zakotvení	zakotvení	k1gNnSc1	zakotvení
v	v	k7c6	v
právním	právní	k2eAgInSc6d1	právní
řádu	řád	k1gInSc6	řád
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
jen	jen	k9	jen
obsoletní	obsoletní	k2eAgNnSc1d1	obsoletní
ustanovení	ustanovení	k1gNnSc1	ustanovení
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgFnPc1d1	státní
barvy	barva	k1gFnPc1	barva
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
například	například	k6eAd1	například
národní	národní	k2eAgInSc1d1	národní
strom	strom	k1gInSc1	strom
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
případě	případ	k1gInSc6	případ
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
lípa	lípa	k1gFnSc1	lípa
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
podle	podle	k7c2	podle
některých	některý	k3yIgMnPc2	některý
právních	právní	k2eAgMnPc2d1	právní
laiků	laik	k1gMnPc2	laik
navíc	navíc	k6eAd1	navíc
ani	ani	k8xC	ani
neměly	mít	k5eNaImAgFnP	mít
být	být	k5eAaImF	být
předmětem	předmět	k1gInSc7	předmět
právní	právní	k2eAgFnSc2d1	právní
regulace	regulace	k1gFnSc2	regulace
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
podle	podle	k7c2	podle
profesora	profesor	k1gMnSc2	profesor
Jana	Jan	k1gMnSc2	Jan
Filipa	Filip	k1gMnSc2	Filip
slouží	sloužit	k5eAaImIp3nS	sloužit
např.	např.	kA	např.
k	k	k7c3	k
označení	označení	k1gNnSc3	označení
státního	státní	k2eAgInSc2d1	státní
majetku	majetek	k1gInSc2	majetek
<g/>
,	,	kIx,	,
k	k	k7c3	k
výzdobě	výzdoba	k1gFnSc3	výzdoba
při	při	k7c6	při
významných	významný	k2eAgFnPc6d1	významná
státních	státní	k2eAgFnPc6d1	státní
událostech	událost	k1gFnPc6	událost
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
východiskem	východisko	k1gNnSc7	východisko
pro	pro	k7c4	pro
konstrukci	konstrukce	k1gFnSc4	konstrukce
státní	státní	k2eAgFnSc2d1	státní
vlajky	vlajka	k1gFnSc2	vlajka
i	i	k9	i
vlajky	vlajka	k1gFnPc1	vlajka
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
pak	pak	k6eAd1	pak
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
dle	dle	k7c2	dle
něj	on	k3xPp3gMnSc2	on
důvodem	důvod	k1gInSc7	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Ústavě	ústava	k1gFnSc6	ústava
(	(	kIx(	(
<g/>
čl	čl	kA	čl
<g/>
.	.	kIx.	.
14	[number]	k4	14
odst	odsta	k1gFnPc2	odsta
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
)	)	kIx)	)
uvedeny	uvést	k5eAaPmNgInP	uvést
před	před	k7c7	před
nimi	on	k3xPp3gMnPc7	on
<g/>
.	.	kIx.	.
<g/>
Vymezení	vymezení	k1gNnSc3	vymezení
odstínů	odstín	k1gInPc2	odstín
státních	státní	k2eAgFnPc2d1	státní
barev	barva	k1gFnPc2	barva
není	být	k5eNaImIp3nS	být
zákonem	zákon	k1gInSc7	zákon
upraveno	upraven	k2eAgNnSc1d1	upraveno
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
stanovení	stanovení	k1gNnSc6	stanovení
odstínů	odstín	k1gInPc2	odstín
státních	státní	k2eAgFnPc2d1	státní
barev	barva	k1gFnPc2	barva
se	se	k3xPyFc4	se
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
barevné	barevný	k2eAgFnSc2d1	barevná
přílohy	příloha	k1gFnSc2	příloha
(	(	kIx(	(
<g/>
přílohy	příloha	k1gFnSc2	příloha
č.	č.	k?	č.
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
zákona	zákon	k1gInSc2	zákon
číslo	číslo	k1gNnSc1	číslo
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
1993	[number]	k4	1993
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
o	o	k7c6	o
státních	státní	k2eAgInPc6d1	státní
symbolech	symbol	k1gInPc6	symbol
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
symboly	symbol	k1gInPc7	symbol
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Státní	státní	k2eAgFnSc2d1	státní
barvy	barva	k1gFnSc2	barva
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc2	jejich
použití	použití	k1gNnSc2	použití
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
Symboly	symbol	k1gInPc1	symbol
české	český	k2eAgFnSc2d1	Česká
státnosti	státnost	k1gFnSc2	státnost
</s>
</p>
