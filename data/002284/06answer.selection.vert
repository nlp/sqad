<s>
Peking	Peking	k1gInSc1	Peking
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
Číny	Čína	k1gFnSc2	Čína
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
úplně	úplně	k6eAd1	úplně
obklopen	obklopit	k5eAaPmNgMnS	obklopit
provincií	provincie	k1gFnSc7	provincie
Che-pej	Cheej	k1gMnPc2	Che-pej
<g/>
,	,	kIx,	,
jen	jen	k9	jen
na	na	k7c6	na
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
straně	strana	k1gFnSc6	strana
hraničí	hraničit	k5eAaImIp3nS	hraničit
s	s	k7c7	s
městem	město	k1gNnSc7	město
Tchien-ťin	Tchien-ťina	k1gFnPc2	Tchien-ťina
<g/>
,	,	kIx,	,
dalším	další	k2eAgFnPc3d1	další
ze	z	k7c2	z
čtveřice	čtveřice	k1gFnSc2	čtveřice
statutárních	statutární	k2eAgNnPc2d1	statutární
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
postavených	postavený	k2eAgMnPc2d1	postavený
na	na	k7c4	na
roveň	roveň	k1gFnSc4	roveň
provinciím	provincie	k1gFnPc3	provincie
<g/>
.	.	kIx.	.
</s>
