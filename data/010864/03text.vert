<p>
<s>
Chile	Chile	k1gNnSc1	Chile
se	se	k3xPyFc4	se
účastnilo	účastnit	k5eAaImAgNnS	účastnit
Letní	letní	k2eAgNnSc1d1	letní
olympiády	olympiáda	k1gFnSc2	olympiáda
2000	[number]	k4	2000
ve	v	k7c6	v
14	[number]	k4	14
sportech	sport	k1gInPc6	sport
<g/>
.	.	kIx.	.
</s>
<s>
Zastupovalo	zastupovat	k5eAaImAgNnS	zastupovat
ho	on	k3xPp3gNnSc4	on
50	[number]	k4	50
sportovců	sportovec	k1gMnPc2	sportovec
(	(	kIx(	(
<g/>
43	[number]	k4	43
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
7	[number]	k4	7
žen	žena	k1gFnPc2	žena
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Medailisté	medailista	k1gMnPc5	medailista
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Chile	Chile	k1gNnSc1	Chile
na	na	k7c4	na
LOH	LOH	kA	LOH
2000	[number]	k4	2000
</s>
</p>
