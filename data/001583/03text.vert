<s>
Otto	Otto	k1gMnSc1	Otto
Stern	sternum	k1gNnPc2	sternum
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1888	[number]	k4	1888
<g/>
,	,	kIx,	,
Żory	Żora	k1gFnPc1	Żora
-	-	kIx~	-
17	[number]	k4	17
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1969	[number]	k4	1969
Berkeley	Berkelea	k1gFnSc2	Berkelea
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
německý	německý	k2eAgMnSc1d1	německý
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
obdržel	obdržet	k5eAaPmAgMnS	obdržet
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
za	za	k7c4	za
příspěvek	příspěvek	k1gInSc4	příspěvek
k	k	k7c3	k
vývoji	vývoj	k1gInSc3	vývoj
metody	metoda	k1gFnSc2	metoda
molekulárních	molekulární	k2eAgInPc2d1	molekulární
paprsků	paprsek	k1gInPc2	paprsek
a	a	k8xC	a
objev	objev	k1gInSc1	objev
magnetického	magnetický	k2eAgInSc2d1	magnetický
momentu	moment	k1gInSc2	moment
protonu	proton	k1gInSc2	proton
<g/>
.	.	kIx.	.
</s>
<s>
Otto	Otto	k1gMnSc1	Otto
Stern	sternum	k1gNnPc2	sternum
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
pruském	pruský	k2eAgNnSc6d1	pruské
městě	město	k1gNnSc6	město
Sohrau	Sohraus	k1gInSc2	Sohraus
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
Żory	Żora	k1gFnPc1	Żora
v	v	k7c6	v
jižním	jižní	k2eAgNnSc6d1	jižní
Slezsku	Slezsko	k1gNnSc6	Slezsko
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
,	,	kIx,	,
a	a	k8xC	a
studoval	studovat	k5eAaImAgMnS	studovat
ve	v	k7c6	v
Vratislavi	Vratislav	k1gFnSc6	Vratislav
(	(	kIx(	(
<g/>
Wroclav	Wroclav	k1gFnSc6	Wroclav
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Svá	svůj	k3xOyFgNnPc4	svůj
studia	studio	k1gNnPc4	studio
dokončil	dokončit	k5eAaPmAgMnS	dokončit
na	na	k7c6	na
Vratislavské	vratislavský	k2eAgFnSc6d1	Vratislavská
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
doktorátem	doktorát	k1gInSc7	doktorát
z	z	k7c2	z
Fyzikální	fyzikální	k2eAgFnSc2d1	fyzikální
chemie	chemie	k1gFnSc2	chemie
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
následoval	následovat	k5eAaImAgInS	následovat
Alberta	Albert	k1gMnSc4	Albert
Einsteina	Einstein	k1gMnSc4	Einstein
na	na	k7c4	na
Karlovu	Karlův	k2eAgFnSc4d1	Karlova
univerzitu	univerzita	k1gFnSc4	univerzita
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
na	na	k7c4	na
Spolkovou	spolkový	k2eAgFnSc4d1	spolková
vysokou	vysoký	k2eAgFnSc4d1	vysoká
technickou	technický	k2eAgFnSc4d1	technická
školu	škola	k1gFnSc4	škola
do	do	k7c2	do
švýcarského	švýcarský	k2eAgInSc2d1	švýcarský
Curychu	Curych	k1gInSc2	Curych
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
se	se	k3xPyFc4	se
Stern	sternum	k1gNnPc2	sternum
habilitoval	habilitovat	k5eAaBmAgMnS	habilitovat
na	na	k7c6	na
Univerzitě	univerzita	k1gFnSc6	univerzita
Johanna	Johann	k1gMnSc4	Johann
Wolfganga	Wolfgang	k1gMnSc2	Wolfgang
Goetheho	Goethe	k1gMnSc2	Goethe
ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
profesorem	profesor	k1gMnSc7	profesor
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Rostocku	Rostock	k1gInSc6	Rostock
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1923	[number]	k4	1923
však	však	k9	však
toto	tento	k3xDgNnSc4	tento
místo	místo	k1gNnSc4	místo
opustil	opustit	k5eAaPmAgMnS	opustit
a	a	k8xC	a
odešel	odejít	k5eAaPmAgMnS	odejít
pracovat	pracovat	k5eAaImF	pracovat
do	do	k7c2	do
nově	nově	k6eAd1	nově
založeného	založený	k2eAgInSc2d1	založený
Institutu	institut	k1gInSc2	institut
fyzikální	fyzikální	k2eAgFnSc2d1	fyzikální
chemie	chemie	k1gFnSc2	chemie
na	na	k7c6	na
Hamburské	hamburský	k2eAgFnSc6d1	hamburská
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
nacismu	nacismus	k1gInSc2	nacismus
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
se	se	k3xPyFc4	se
však	však	k9	však
svého	svůj	k3xOyFgNnSc2	svůj
místa	místo	k1gNnSc2	místo
v	v	k7c6	v
Hamburku	Hamburk	k1gInSc6	Hamburk
vzdal	vzdát	k5eAaPmAgMnS	vzdát
a	a	k8xC	a
přijal	přijmout	k5eAaPmAgMnS	přijmout
místo	místo	k1gNnSc4	místo
profesora	profesor	k1gMnSc2	profesor
fyziky	fyzika	k1gFnSc2	fyzika
na	na	k7c4	na
Carnegieho	Carnegie	k1gMnSc4	Carnegie
technickém	technický	k2eAgInSc6d1	technický
institutu	institut	k1gInSc6	institut
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
Pittsburghu	Pittsburgh	k1gInSc6	Pittsburgh
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
na	na	k7c6	na
Kalifornské	kalifornský	k2eAgFnSc6d1	kalifornská
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c4	v
Berkeley	Berkele	k1gMnPc4	Berkele
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
experimentální	experimentální	k2eAgMnSc1d1	experimentální
fyzik	fyzik	k1gMnSc1	fyzik
přispěl	přispět	k5eAaPmAgMnS	přispět
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
společně	společně	k6eAd1	společně
s	s	k7c7	s
Waltherem	Walther	k1gMnSc7	Walther
Gerlachem	Gerlach	k1gMnSc7	Gerlach
při	při	k7c6	při
Stern-Gerlachově	Stern-Gerlachův	k2eAgInSc6d1	Stern-Gerlachův
pokusu	pokus	k1gInSc6	pokus
k	k	k7c3	k
objevu	objev	k1gInSc3	objev
a	a	k8xC	a
důkazu	důkaz	k1gInSc3	důkaz
prostorového	prostorový	k2eAgNnSc2d1	prostorové
kvantování	kvantování	k1gNnSc2	kvantování
magnetického	magnetický	k2eAgInSc2d1	magnetický
momentu	moment	k1gInSc2	moment
atomu	atom	k1gInSc2	atom
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
obdržel	obdržet	k5eAaPmAgMnS	obdržet
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
první	první	k4xOgFnSc1	první
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
oboru	obor	k1gInSc6	obor
udělená	udělený	k2eAgFnSc1d1	udělená
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
</s>
<s>
Stern	sternum	k1gNnPc2	sternum
obdržel	obdržet	k5eAaPmAgMnS	obdržet
tuto	tento	k3xDgFnSc4	tento
cenu	cena	k1gFnSc4	cena
sám	sám	k3xTgInSc1	sám
a	a	k8xC	a
při	při	k7c6	při
jejím	její	k3xOp3gNnSc6	její
předávání	předávání	k1gNnSc6	předávání
nepadla	padnout	k5eNaPmAgFnS	padnout
žádná	žádný	k3yNgFnSc1	žádný
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
Stern-Gerlachově	Stern-Gerlachův	k2eAgInSc6d1	Stern-Gerlachův
pokusu	pokus	k1gInSc6	pokus
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Walther	Walthra	k1gFnPc2	Walthra
Gerlach	Gerlacha	k1gFnPc2	Gerlacha
zůstal	zůstat	k5eAaPmAgInS	zůstat
v	v	k7c6	v
nacistickém	nacistický	k2eAgNnSc6d1	nacistické
Německu	Německo	k1gNnSc6	Německo
a	a	k8xC	a
aktivně	aktivně	k6eAd1	aktivně
se	se	k3xPyFc4	se
spolupodílel	spolupodílet	k5eAaImAgMnS	spolupodílet
na	na	k7c6	na
německém	německý	k2eAgInSc6d1	německý
jaderném	jaderný	k2eAgInSc6d1	jaderný
výzkumu	výzkum	k1gInSc6	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Otto	Otto	k1gMnSc1	Otto
Stern	sternum	k1gNnPc2	sternum
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Lubomír	Lubomír	k1gMnSc1	Lubomír
Sodomka	Sodomka	k1gFnSc1	Sodomka
<g/>
,	,	kIx,	,
Magdalena	Magdalena	k1gFnSc1	Magdalena
Sodomková	Sodomková	k1gFnSc1	Sodomková
<g/>
,	,	kIx,	,
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
SET	set	k1gInSc1	set
OUT	OUT	kA	OUT
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-902058-5-2	[number]	k4	80-902058-5-2
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Otto	Otto	k1gMnSc1	Otto
Stern	sternum	k1gNnPc2	sternum
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
