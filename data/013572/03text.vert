<s>
Kněžice	kněžice	k1gFnSc1
zelená	zelený	k2eAgFnSc1d1
</s>
<s>
Kněžice	kněžice	k1gFnSc1
zelená	zelenat	k5eAaImIp3nS
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
Říše	říš	k1gFnSc2
</s>
<s>
živočichové	živočich	k1gMnPc1
(	(	kIx(
<g/>
Animalia	Animalia	k1gFnSc1
<g/>
)	)	kIx)
Kmen	kmen	k1gInSc1
</s>
<s>
členovci	členovec	k1gMnPc1
(	(	kIx(
<g/>
Arthropoda	Arthropoda	k1gMnSc1
<g/>
)	)	kIx)
Podkmen	podkmen	k1gInSc1
</s>
<s>
šestinozí	šestinohý	k2eAgMnPc1d1
(	(	kIx(
<g/>
Hexapoda	Hexapoda	k1gMnSc1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
hmyz	hmyz	k1gInSc1
(	(	kIx(
<g/>
Insecta	Insecta	k1gFnSc1
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
polokřídlí	polokřídlý	k2eAgMnPc1d1
(	(	kIx(
<g/>
Hemiptera	Hemipter	k1gMnSc2
<g/>
)	)	kIx)
Podřád	podřád	k1gInSc1
</s>
<s>
ploštice	ploštice	k1gFnSc2
(	(	kIx(
<g/>
Heteroptera	Heteropter	k1gMnSc2
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc1
</s>
<s>
kněžicovití	kněžicovitý	k2eAgMnPc1d1
(	(	kIx(
<g/>
Pentatomidae	Pentatomidae	k1gNnSc7
<g/>
)	)	kIx)
Rod	rod	k1gInSc1
</s>
<s>
(	(	kIx(
<g/>
Palomena	Palomen	k2eAgFnSc1d1
<g/>
)	)	kIx)
Binomické	binomický	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
</s>
<s>
Palomena	Palomen	k2eAgFnSc1d1
viridissima	viridissima	k1gFnSc1
<g/>
(	(	kIx(
<g/>
Poda	Poda	k1gMnSc1
<g/>
,	,	kIx,
1761	#num#	k4
<g/>
)	)	kIx)
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Kněžice	kněžice	k1gFnSc1
zelená	zelený	k2eAgFnSc1d1
(	(	kIx(
<g/>
Palomena	Palomen	k2eAgFnSc1d1
viridissima	viridissima	k1gFnSc1
<g/>
)	)	kIx)
patří	patřit	k5eAaImIp3nS
do	do	k7c2
čeledě	čeleď	k1gFnSc2
kněžicovití	kněžicovitý	k2eAgMnPc1d1
(	(	kIx(
<g/>
Pentatomidae	Pentatomidae	k1gNnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Synonyma	synonymum	k1gNnPc1
</s>
<s>
Palomena	Palomen	k2eAgFnSc1d1
amplificata	amplificata	k1gFnSc1
Distant	Distant	k1gMnSc1
<g/>
,	,	kIx,
1880	#num#	k4
</s>
<s>
Palomena	Palomen	k2eAgFnSc1d1
rubicunda	rubicunda	k1gFnSc1
Westhoff	Westhoff	k1gInSc1
<g/>
,	,	kIx,
1884	#num#	k4
</s>
<s>
Palomena	Palomen	k2eAgFnSc1d1
simulans	simulans	k6eAd1
Puton	Putona	k1gFnPc2
<g/>
,	,	kIx,
1881	#num#	k4
</s>
<s>
Pentatoma	Pentatoma	k1gFnSc1
rotundicollis	rotundicollis	k1gFnSc2
Westwood	Westwooda	k1gFnPc2
<g/>
,	,	kIx,
1837	#num#	k4
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Kněžice	kněžice	k1gFnPc1
zelené	zelená	k1gFnSc2
jsou	být	k5eAaImIp3nP
od	od	k7c2
11,0	11,0	k4
do	do	k7c2
14,0	14,0	k4
mm	mm	kA
dlouhé	dlouhý	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbarvené	zbarvený	k2eAgInPc1d1
jsou	být	k5eAaImIp3nP
světle	světle	k6eAd1
zeleně	zeleně	k6eAd1
<g/>
,	,	kIx,
až	až	k9
olivově	olivově	k6eAd1
hnědě	hnědě	k6eAd1
<g/>
,	,	kIx,
často	často	k6eAd1
s	s	k7c7
fialovým	fialový	k2eAgInSc7d1
leskem	lesk	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
přezimování	přezimování	k1gNnSc6
jsou	být	k5eAaImIp3nP
znovu	znovu	k6eAd1
zelené	zelený	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc7
hlavová	hlavový	k2eAgFnSc1d1
deska	deska	k1gFnSc1
je	být	k5eAaImIp3nS
zakřivená	zakřivený	k2eAgFnSc1d1
na	na	k7c6
předním	přední	k2eAgInSc6d1
okraji	okraj	k1gInSc6
navenek	navenek	k6eAd1
konvexně	konvexně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
odlišuje	odlišovat	k5eAaImIp3nS
tento	tento	k3xDgInSc1
druh	druh	k1gInSc1
podobné	podobný	k2eAgFnSc2d1
kněžice	kněžice	k1gFnSc2
trávozelené	trávozelený	k2eAgNnSc1d1
(	(	kIx(
<g/>
Palomena	Palomen	k2eAgFnSc1d1
prasina	prasina	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc1
hrana	hrana	k1gFnSc1
je	být	k5eAaImIp3nS
konkávně	konkávně	k6eAd1
zakřivená	zakřivený	k2eAgNnPc4d1
směrem	směr	k1gInSc7
dovnitř	dovnitř	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
obou	dva	k4xCgInPc2
druhů	druh	k1gInPc2
má	mít	k5eAaImIp3nS
štítek	štítek	k1gInSc1
(	(	kIx(
<g/>
scutellum	scutellum	k1gInSc1
<g/>
)	)	kIx)
má	mít	k5eAaImIp3nS
stejnou	stejný	k2eAgFnSc4d1
barvu	barva	k1gFnSc4
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
barva	barva	k1gFnSc1
těla	tělo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhý	druhý	k4xOgInSc1
článek	článek	k1gInSc1
tykadla	tykadlo	k1gNnSc2
je	být	k5eAaImIp3nS
u	u	k7c2
kněžice	kněžice	k1gFnSc2
zelené	zelená	k1gFnSc2
1,6	1,6	k4
až	až	k9
2	#num#	k4
krát	krát	k6eAd1
větší	veliký	k2eAgFnSc1d2
než	než	k8xS
třetí	třetí	k4xOgFnSc1
<g/>
;	;	kIx,
u	u	k7c2
kněžice	kněžice	k1gFnSc2
trávozelené	trávozelený	k2eAgFnSc2d1
jsou	být	k5eAaImIp3nP
tyto	tento	k3xDgInPc1
dva	dva	k4xCgInPc1
členy	člen	k1gInPc1
přibližně	přibližně	k6eAd1
stejně	stejně	k6eAd1
dlouhé	dlouhý	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Areál	areál	k1gInSc1
rozšíření	rozšíření	k1gNnSc2
</s>
<s>
Tento	tento	k3xDgInSc1
druh	druh	k1gInSc1
je	být	k5eAaImIp3nS
rozšířený	rozšířený	k2eAgInSc1d1
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
,	,	kIx,
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
britských	britský	k2eAgMnPc2d1
ostrovů	ostrov	k1gInPc2
a	a	k8xC
severu	sever	k1gInSc2
Skandinávie	Skandinávie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
východ	východ	k1gInSc4
je	být	k5eAaImIp3nS
rozšířena	rozšířit	k5eAaPmNgFnS
až	až	k6eAd1
do	do	k7c2
Střední	střední	k2eAgFnSc2d1
Asie	Asie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Německu	Německo	k1gNnSc6
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
hlavně	hlavně	k9
na	na	k7c6
jihu	jih	k1gInSc6
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
však	však	k9
mnohem	mnohem	k6eAd1
vzácnější	vzácný	k2eAgFnSc1d2
než	než	k8xS
kněžice	kněžice	k1gFnSc1
trávozelená	trávozelený	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
severu	sever	k1gInSc6
Německa	Německo	k1gNnSc2
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
jen	jen	k9
sporadicky	sporadicky	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Životní	životní	k2eAgInSc1d1
cyklus	cyklus	k1gInSc1
</s>
<s>
Stejně	stejně	k6eAd1
jako	jako	k9
kněžice	kněžice	k1gFnSc1
trávozelená	trávozelený	k2eAgFnSc1d1
kolonizuje	kolonizovat	k5eAaBmIp3nS
mnoho	mnoho	k4c1
různých	různý	k2eAgNnPc2d1
stanovišť	stanoviště	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ale	ale	k8xC
především	především	k6eAd1
se	se	k3xPyFc4
nalézají	nalézat	k5eAaImIp3nP
v	v	k7c6
bylinném	bylinný	k2eAgNnSc6d1
patře	patro	k1gNnSc6
a	a	k8xC
zřídka	zřídka	k6eAd1
na	na	k7c6
listnatých	listnatý	k2eAgInPc6d1
stromech	strom	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
kněžice	kněžice	k1gFnSc1
trávozelená	trávozelený	k2eAgFnSc1d1
žije	žít	k5eAaImIp3nS
polyfágně	polyfágně	k6eAd1
na	na	k7c6
různých	různý	k2eAgFnPc6d1
rostlinách	rostlina	k1gFnPc6
a	a	k8xC
má	mít	k5eAaImIp3nS
každoročně	každoročně	k6eAd1
jednu	jeden	k4xCgFnSc4
generaci	generace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Zajímavosti	zajímavost	k1gFnPc1
</s>
<s>
Při	při	k7c6
ohrožení	ohrožení	k1gNnSc6
vylučuje	vylučovat	k5eAaImIp3nS
páchnoucí	páchnoucí	k2eAgFnSc4d1
tekutinu	tekutina	k1gFnSc4
–	–	k?
u	u	k7c2
citlivých	citlivý	k2eAgFnPc2d1
osob	osoba	k1gFnPc2
vyvolává	vyvolávat	k5eAaImIp3nS
alergickou	alergický	k2eAgFnSc4d1
reakci	reakce	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
kněžice	kněžice	k1gFnSc2
zelená	zelenat	k5eAaImIp3nS
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Miroslav	Miroslav	k1gMnSc1
Deml	Deml	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
BioLib	BioLib	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
–	–	k?
(	(	kIx(
<g/>
kněžice	kněžice	k1gFnSc1
zelená	zelený	k2eAgFnSc1d1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
BioLib	BioLib	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
