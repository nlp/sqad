<s>
Kolibříci	kolibřík	k1gMnPc1	kolibřík
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
čeleď	čeleď	k1gFnSc4	čeleď
kolibříkovití	kolibříkovitý	k2eAgMnPc1d1	kolibříkovitý
(	(	kIx(	(
<g/>
Trochilidae	Trochilidae	k1gNnSc7	Trochilidae
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
malí	malý	k2eAgMnPc1d1	malý
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
velmi	velmi	k6eAd1	velmi
nápadně	nápadně	k6eAd1	nápadně
zbarvení	zbarvený	k2eAgMnPc1d1	zbarvený
ptáci	pták	k1gMnPc1	pták
<g/>
.	.	kIx.	.
</s>
