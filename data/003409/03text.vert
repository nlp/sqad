<s>
Kolibříci	kolibřík	k1gMnPc1	kolibřík
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
čeleď	čeleď	k1gFnSc4	čeleď
kolibříkovití	kolibříkovitý	k2eAgMnPc1d1	kolibříkovitý
(	(	kIx(	(
<g/>
Trochilidae	Trochilidae	k1gNnSc7	Trochilidae
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
malí	malý	k2eAgMnPc1d1	malý
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
velmi	velmi	k6eAd1	velmi
nápadně	nápadně	k6eAd1	nápadně
zbarvení	zbarvený	k2eAgMnPc1d1	zbarvený
ptáci	pták	k1gMnPc1	pták
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
také	také	k9	také
někdy	někdy	k6eAd1	někdy
říká	říkat	k5eAaImIp3nS	říkat
"	"	kIx"	"
<g/>
létající	létající	k2eAgInPc1d1	létající
drahokamy	drahokam	k1gInPc1	drahokam
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
endemické	endemický	k2eAgMnPc4d1	endemický
živočichy	živočich	k1gMnPc4	živočich
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
jen	jen	k9	jen
na	na	k7c6	na
americkém	americký	k2eAgInSc6d1	americký
kontinentě	kontinent	k1gInSc6	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
a	a	k8xC	a
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
tento	tento	k3xDgInSc1	tento
ptačí	ptačí	k2eAgInSc1d1	ptačí
druh	druh	k1gInSc1	druh
snadno	snadno	k6eAd1	snadno
zaměněn	zaměnit	k5eAaPmNgInS	zaměnit
za	za	k7c2	za
dlouhozobku	dlouhozobek	k1gInSc2	dlouhozobek
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
motýl	motýl	k1gMnSc1	motýl
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
lišajovitých	lišajovití	k1gMnPc2	lišajovití
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
<s>
Kolibříci	kolibřík	k1gMnPc1	kolibřík
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
zdatnými	zdatný	k2eAgMnPc7d1	zdatný
letci	letec	k1gMnPc7	letec
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
mohou	moct	k5eAaImIp3nP	moct
mávnout	mávnout	k5eAaPmF	mávnout
křídly	křídlo	k1gNnPc7	křídlo
12	[number]	k4	12
<g/>
×	×	k?	×
až	až	k9	až
90	[number]	k4	90
<g/>
×	×	k?	×
(	(	kIx(	(
<g/>
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
druhu	druh	k1gInSc6	druh
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yQnSc3	což
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
doslova	doslova	k6eAd1	doslova
zastavit	zastavit	k5eAaPmF	zastavit
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Dokážou	dokázat	k5eAaPmIp3nP	dokázat
také	také	k9	také
vyvinout	vyvinout	k5eAaPmF	vyvinout
rychlost	rychlost	k1gFnSc4	rychlost
větší	veliký	k2eAgFnSc4d2	veliký
než	než	k8xS	než
15	[number]	k4	15
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
(	(	kIx(	(
<g/>
54	[number]	k4	54
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
jako	jako	k9	jako
jediní	jediný	k2eAgMnPc1d1	jediný
zástupci	zástupce	k1gMnPc1	zástupce
ptačí	ptačí	k2eAgFnSc2d1	ptačí
říše	říš	k1gFnSc2	říš
dokážou	dokázat	k5eAaPmIp3nP	dokázat
létat	létat	k5eAaImF	létat
pozpátku	pozpátku	k6eAd1	pozpátku
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
rozeznáváme	rozeznávat	k5eAaImIp1nP	rozeznávat
zhruba	zhruba	k6eAd1	zhruba
325	[number]	k4	325
<g/>
–	–	k?	–
<g/>
340	[number]	k4	340
druhů	druh	k1gInPc2	druh
(	(	kIx(	(
<g/>
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
taxonomickém	taxonomický	k2eAgNnSc6d1	taxonomické
zařazení	zařazení	k1gNnSc6	zařazení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kolibříci	kolibřík	k1gMnPc1	kolibřík
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
velikosti	velikost	k1gFnSc6	velikost
viditelně	viditelně	k6eAd1	viditelně
odlišní	odlišný	k2eAgMnPc1d1	odlišný
<g/>
,	,	kIx,	,
nejmenší	malý	k2eAgMnPc4d3	nejmenší
zástupce	zástupce	k1gMnPc4	zástupce
celé	celý	k2eAgFnSc2d1	celá
čeledi	čeleď	k1gFnSc2	čeleď
<g/>
,	,	kIx,	,
kalypta	kalypto	k1gNnPc4	kalypto
nejmenší	malý	k2eAgNnPc4d3	nejmenší
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
nejmenším	malý	k2eAgMnSc7d3	nejmenší
ptákem	pták	k1gMnSc7	pták
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
největším	veliký	k2eAgMnSc7d3	veliký
zástupcem	zástupce	k1gMnSc7	zástupce
je	být	k5eAaImIp3nS	být
naopak	naopak	k6eAd1	naopak
kolibřík	kolibřík	k1gMnSc1	kolibřík
velký	velký	k2eAgMnSc1d1	velký
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
nápadně	nápadně	k6eAd1	nápadně
zbarvené	zbarvený	k2eAgMnPc4d1	zbarvený
<g/>
,	,	kIx,	,
štíhlé	štíhlý	k2eAgMnPc4d1	štíhlý
ptáky	pták	k1gMnPc4	pták
s	s	k7c7	s
malými	malý	k2eAgFnPc7d1	malá
končetinami	končetina	k1gFnPc7	končetina
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
hmyzu	hmyz	k1gInSc2	hmyz
mají	mít	k5eAaImIp3nP	mít
kolibříci	kolibřík	k1gMnPc1	kolibřík
nejrychlejší	rychlý	k2eAgInSc4d3	nejrychlejší
metabolismus	metabolismus	k1gInSc4	metabolismus
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
živočišných	živočišný	k2eAgInPc2d1	živočišný
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
je	být	k5eAaImIp3nS	být
zejména	zejména	k9	zejména
velmi	velmi	k6eAd1	velmi
rychlý	rychlý	k2eAgInSc1d1	rychlý
a	a	k8xC	a
pro	pro	k7c4	pro
tělo	tělo	k1gNnSc4	tělo
značně	značně	k6eAd1	značně
vytížující	vytížující	k2eAgInSc1d1	vytížující
tlukot	tlukot	k1gInSc1	tlukot
křídel	křídlo	k1gNnPc2	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
přitom	přitom	k6eAd1	přitom
jeho	jeho	k3xOp3gNnSc4	jeho
srdce	srdce	k1gNnSc4	srdce
vykoná	vykonat	k5eAaPmIp3nS	vykonat
až	až	k9	až
1260	[number]	k4	1260
tepů	tep	k1gInPc2	tep
<g/>
,	,	kIx,	,
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gInSc4	jeho
metabolismus	metabolismus	k1gInSc4	metabolismus
značně	značně	k6eAd1	značně
zpomalen	zpomalen	k2eAgInSc4d1	zpomalen
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
výkon	výkon	k1gInSc1	výkon
může	moct	k5eAaImIp3nS	moct
klesnout	klesnout	k5eAaPmF	klesnout
až	až	k9	až
na	na	k7c4	na
50	[number]	k4	50
<g/>
–	–	k?	–
<g/>
180	[number]	k4	180
tepů	tep	k1gInPc2	tep
<g/>
/	/	kIx~	/
<g/>
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
Živí	živit	k5eAaImIp3nS	živit
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
nektarem	nektar	k1gInSc7	nektar
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemuž	což	k3yRnSc3	což
jim	on	k3xPp3gMnPc3	on
napomáhá	napomáhat	k5eAaImIp3nS	napomáhat
jejich	jejich	k3xOp3gInSc1	jejich
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
zobák	zobák	k1gInSc1	zobák
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
u	u	k7c2	u
různých	různý	k2eAgInPc2d1	různý
druhů	druh	k1gInPc2	druh
odlišný	odlišný	k2eAgInSc4d1	odlišný
a	a	k8xC	a
specializovaný	specializovaný	k2eAgInSc4d1	specializovaný
pro	pro	k7c4	pro
určitý	určitý	k2eAgInSc4d1	určitý
druh	druh	k1gInSc4	druh
květu	květ	k1gInSc2	květ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
potravě	potrava	k1gFnSc6	potrava
přitom	přitom	k6eAd1	přitom
zcela	zcela	k6eAd1	zcela
odmítá	odmítat	k5eAaImIp3nS	odmítat
nektar	nektar	k1gInSc1	nektar
s	s	k7c7	s
nižším	nízký	k2eAgNnSc7d2	nižší
zastoupením	zastoupení	k1gNnSc7	zastoupení
cukrů	cukr	k1gInPc2	cukr
než	než	k8xS	než
10	[number]	k4	10
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
nektar	nektar	k1gInSc1	nektar
málo	málo	k6eAd1	málo
výživný	výživný	k2eAgInSc1d1	výživný
<g/>
,	,	kIx,	,
požírá	požírat	k5eAaImIp3nS	požírat
také	také	k9	také
drobný	drobný	k2eAgInSc4d1	drobný
hmyz	hmyz	k1gInSc4	hmyz
a	a	k8xC	a
jiné	jiný	k2eAgMnPc4d1	jiný
bezobratlé	bezobratlý	k2eAgMnPc4d1	bezobratlý
živočichy	živočich	k1gMnPc4	živočich
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
pak	pak	k6eAd1	pak
v	v	k7c6	v
období	období	k1gNnSc6	období
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
<g/>
.	.	kIx.	.
</s>
<s>
Létáním	létání	k1gNnSc7	létání
a	a	k8xC	a
hledáním	hledání	k1gNnSc7	hledání
potravy	potrava	k1gFnSc2	potrava
tráví	trávit	k5eAaImIp3nP	trávit
kolibříci	kolibřík	k1gMnPc1	kolibřík
pouze	pouze	k6eAd1	pouze
10	[number]	k4	10
<g/>
–	–	k?	–
<g/>
15	[number]	k4	15
%	%	kIx~	%
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
zbývajících	zbývající	k2eAgInPc2d1	zbývající
75	[number]	k4	75
<g/>
–	–	k?	–
<g/>
80	[number]	k4	80
%	%	kIx~	%
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
odpočinek	odpočinek	k1gInSc4	odpočinek
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
však	však	k9	však
musí	muset	k5eAaImIp3nS	muset
denně	denně	k6eAd1	denně
pozřít	pozřít	k5eAaPmF	pozřít
značně	značně	k6eAd1	značně
větší	veliký	k2eAgNnSc4d2	veliký
množství	množství	k1gNnSc4	množství
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
celková	celkový	k2eAgFnSc1d1	celková
hmotnost	hmotnost	k1gFnSc1	hmotnost
a	a	k8xC	a
musí	muset	k5eAaImIp3nS	muset
tak	tak	k6eAd1	tak
denně	denně	k6eAd1	denně
navštívit	navštívit	k5eAaPmF	navštívit
stovky	stovka	k1gFnPc4	stovka
květů	květ	k1gInPc2	květ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
se	se	k3xPyFc4	se
samec	samec	k1gInSc1	samec
nijak	nijak	k6eAd1	nijak
nepodílí	podílet	k5eNaImIp3nS	podílet
na	na	k7c4	na
inkubaci	inkubace	k1gFnSc4	inkubace
vajec	vejce	k1gNnPc2	vejce
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
na	na	k7c6	na
krmení	krmení	k1gNnSc6	krmení
mláďat	mládě	k1gNnPc2	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
druhů	druh	k1gInPc2	druh
si	se	k3xPyFc3	se
staví	stavit	k5eAaImIp3nS	stavit
malé	malý	k2eAgFnPc4d1	malá
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
maskované	maskovaný	k2eAgNnSc4d1	maskované
hnízdo	hnízdo	k1gNnSc4	hnízdo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
buduje	budovat	k5eAaImIp3nS	budovat
na	na	k7c6	na
větvích	větev	k1gFnPc6	větev
stromů	strom	k1gInPc2	strom
nebo	nebo	k8xC	nebo
keřů	keř	k1gInPc2	keř
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
hnízda	hnízdo	k1gNnSc2	hnízdo
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
odlišná	odlišný	k2eAgFnSc1d1	odlišná
<g/>
,	,	kIx,	,
nejmenší	malý	k2eAgMnPc4d3	nejmenší
přitom	přitom	k6eAd1	přitom
mohou	moct	k5eAaImIp3nP	moct
dosahovat	dosahovat	k5eAaImF	dosahovat
velikosti	velikost	k1gFnSc2	velikost
jedné	jeden	k4xCgFnSc2	jeden
poloviny	polovina	k1gFnSc2	polovina
skořápky	skořápka	k1gFnSc2	skořápka
vlašského	vlašský	k2eAgInSc2d1	vlašský
ořechu	ořech	k1gInSc2	ořech
<g/>
.	.	kIx.	.
</s>
<s>
Kladou	klást	k5eAaImIp3nP	klást
obvykle	obvykle	k6eAd1	obvykle
2	[number]	k4	2
vejce	vejce	k1gNnSc2	vejce
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yRgFnPc6	který
sedí	sedit	k5eAaImIp3nP	sedit
12	[number]	k4	12
<g/>
–	–	k?	–
<g/>
19	[number]	k4	19
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
dožívají	dožívat	k5eAaImIp3nP	dožívat
průměrně	průměrně	k6eAd1	průměrně
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Čeleď	čeleď	k1gFnSc4	čeleď
kolibříkovitých	kolibříkovitý	k2eAgMnPc2d1	kolibříkovitý
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
podčeledi	podčeleď	k1gFnPc4	podčeleď
(	(	kIx(	(
<g/>
Hermit	Hermit	k1gInSc4	Hermit
a	a	k8xC	a
Trochilinae	Trochilinae	k1gInSc4	Trochilinae
<g/>
)	)	kIx)	)
které	který	k3yQgFnPc1	který
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
rodů	rod	k1gInPc2	rod
<g/>
,	,	kIx,	,
běžně	běžně	k6eAd1	běžně
jich	on	k3xPp3gMnPc2	on
bývá	bývat	k5eAaImIp3nS	bývat
uváděno	uvádět	k5eAaImNgNnS	uvádět
106	[number]	k4	106
<g/>
.	.	kIx.	.
</s>
<s>
Podčeleď	podčeleď	k1gFnSc1	podčeleď
Trochilinae	Trochilinae	k1gFnSc1	Trochilinae
<g/>
:	:	kIx,	:
Podčeleď	podčeleď	k1gFnSc1	podčeleď
Hermit	Hermita	k1gFnPc2	Hermita
<g/>
:	:	kIx,	:
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Hummingbird	Hummingbirda	k1gFnPc2	Hummingbirda
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc4	seznam
všech	všecek	k3xTgInPc2	všecek
obecně	obecně	k6eAd1	obecně
uznávaných	uznávaný	k2eAgInPc2d1	uznávaný
rodů	rod	k1gInPc2	rod
a	a	k8xC	a
druhů	druh	k1gInPc2	druh
čeledi	čeleď	k1gFnSc2	čeleď
kolibříkovití	kolibříkovitý	k2eAgMnPc1d1	kolibříkovitý
na	na	k7c4	na
BioLibu	BioLiba	k1gFnSc4	BioLiba
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
kolibříkovití	kolibříkovitý	k2eAgMnPc1d1	kolibříkovitý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
kolibřík	kolibřík	k1gMnSc1	kolibřík
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Galerie	galerie	k1gFnSc2	galerie
kolibříkovití	kolibříkovitý	k2eAgMnPc1d1	kolibříkovitý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
