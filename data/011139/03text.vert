<p>
<s>
Nymburk	Nymburk	k1gInSc1	Nymburk
(	(	kIx(	(
<g/>
něm.	něm.	k?	něm.
Nimburg	Nimburg	k1gInSc1	Nimburg
nebo	nebo	k8xC	nebo
Neuenburg	Neuenburg	k1gInSc1	Neuenburg
an	an	k?	an
der	drát	k5eAaImRp2nS	drát
Elbe	Elbus	k1gMnSc5	Elbus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
ve	v	k7c6	v
Středočeském	středočeský	k2eAgInSc6d1	středočeský
kraji	kraj	k1gInSc6	kraj
na	na	k7c6	na
Labi	Labe	k1gNnSc6	Labe
<g/>
,	,	kIx,	,
45	[number]	k4	45
km	km	kA	km
východně	východně	k6eAd1	východně
od	od	k7c2	od
Prahy	Praha	k1gFnSc2	Praha
a	a	k8xC	a
zhruba	zhruba	k6eAd1	zhruba
30	[number]	k4	30
km	km	kA	km
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
od	od	k7c2	od
Mladé	mladá	k1gFnSc2	mladá
Boleslavi	Boleslaev	k1gFnSc3	Boleslaev
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
přibližně	přibližně	k6eAd1	přibližně
15	[number]	k4	15
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
rozlohu	rozloha	k1gFnSc4	rozloha
20,54	[number]	k4	20,54
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Historické	historický	k2eAgNnSc1d1	historické
jádro	jádro	k1gNnSc1	jádro
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
městskou	městský	k2eAgFnSc7d1	městská
památkovou	památkový	k2eAgFnSc7d1	památková
zónou	zóna	k1gFnSc7	zóna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Vznik	vznik	k1gInSc1	vznik
města	město	k1gNnSc2	město
===	===	k?	===
</s>
</p>
<p>
<s>
Královské	královský	k2eAgNnSc1d1	královské
město	město	k1gNnSc1	město
Nymburk	Nymburk	k1gInSc1	Nymburk
vděčí	vděčit	k5eAaImIp3nS	vděčit
za	za	k7c4	za
své	svůj	k3xOyFgNnSc4	svůj
založení	založení	k1gNnSc4	založení
Přemyslu	Přemysl	k1gMnSc3	Přemysl
Otakaru	Otakar	k1gMnSc3	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Přesné	přesný	k2eAgNnSc4d1	přesné
datum	datum	k1gNnSc4	datum
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
započato	započnout	k5eAaPmNgNnS	započnout
s	s	k7c7	s
výstavbou	výstavba	k1gFnSc7	výstavba
Nymburka	Nymburk	k1gInSc2	Nymburk
<g/>
,	,	kIx,	,
však	však	k9	však
dodnes	dodnes	k6eAd1	dodnes
není	být	k5eNaImIp3nS	být
úplně	úplně	k6eAd1	úplně
známo	znám	k2eAgNnSc1d1	známo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
za	za	k7c4	za
vznik	vznik	k1gInSc4	vznik
města	město	k1gNnSc2	město
přijímán	přijímat	k5eAaImNgInS	přijímat
rok	rok	k1gInSc1	rok
1275	[number]	k4	1275
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Název	název	k1gInSc1	název
Nymburk	Nymburk	k1gInSc1	Nymburk
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
slova	slovo	k1gNnSc2	slovo
Neuburg	Neuburg	k1gInSc1	Neuburg
(	(	kIx(	(
<g/>
Nimburg	Nimburg	k1gInSc1	Nimburg
=	=	kIx~	=
Nymburg	Nymburg	k1gInSc1	Nymburg
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
Nymburk	Nymburk	k1gInSc1	Nymburk
<g/>
)	)	kIx)	)
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
původně	původně	k6eAd1	původně
osídlen	osídlit	k5eAaPmNgMnS	osídlit
německými	německý	k2eAgMnPc7d1	německý
a	a	k8xC	a
holandskými	holandský	k2eAgMnPc7d1	holandský
osadníky	osadník	k1gMnPc7	osadník
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
však	však	k9	však
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
staletí	staletí	k1gNnSc2	staletí
asimilovali	asimilovat	k5eAaBmAgMnP	asimilovat
mezi	mezi	k7c4	mezi
přicházející	přicházející	k2eAgInSc4d1	přicházející
český	český	k2eAgInSc4d1	český
živel	živel	k1gInSc4	živel
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Hájkovy	Hájkův	k2eAgFnSc2d1	Hájkova
kroniky	kronika	k1gFnSc2	kronika
se	se	k3xPyFc4	se
Nymburk	Nymburk	k1gInSc1	Nymburk
dříve	dříve	k6eAd2	dříve
jmenoval	jmenovat	k5eAaBmAgInS	jmenovat
Svinibrod	Svinibrod	k1gInSc1	Svinibrod
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
části	část	k1gFnSc6	část
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
za	za	k7c7	za
Labem	Labe	k1gNnSc7	Labe
<g/>
,	,	kIx,	,
pásly	pásnout	k5eAaImAgFnP	pásnout
svině	svině	k1gFnPc1	svině
(	(	kIx(	(
<g/>
prasata	prase	k1gNnPc1	prase
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
jazyk	jazyk	k1gInSc1	jazyk
převládl	převládnout	k5eAaPmAgInS	převládnout
nad	nad	k7c7	nad
německým	německý	k2eAgInSc7d1	německý
jazykem	jazyk	k1gInSc7	jazyk
až	až	k9	až
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
po	po	k7c6	po
husitských	husitský	k2eAgFnPc6d1	husitská
válkách	válka	k1gFnPc6	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Již	již	k6eAd1	již
na	na	k7c6	na
konci	konec	k1gInSc6	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
prvními	první	k4xOgMnPc7	první
osvícenci	osvícenec	k1gMnSc3	osvícenec
odmítnuto	odmítnut	k2eAgNnSc1d1	odmítnuto
založení	založení	k1gNnSc1	založení
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
Vícemílova	Vícemílův	k2eAgInSc2d1	Vícemílův
<g/>
)	)	kIx)	)
roku	rok	k1gInSc2	rok
779	[number]	k4	779
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
v	v	k7c6	v
Hájkově	Hájkův	k2eAgFnSc6d1	Hájkova
kronice	kronika	k1gFnSc6	kronika
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
století	století	k1gNnSc6	století
později	pozdě	k6eAd2	pozdě
bylo	být	k5eAaImAgNnS	být
vyloučeno	vyloučen	k2eAgNnSc1d1	vyloučeno
datum	datum	k1gNnSc1	datum
1219	[number]	k4	1219
(	(	kIx(	(
<g/>
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
se	se	k3xPyFc4	se
k	k	k7c3	k
Litoměřicím	Litoměřice	k1gInPc3	Litoměřice
<g/>
)	)	kIx)	)
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
rok	rok	k1gInSc4	rok
1257	[number]	k4	1257
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
datu	datum	k1gNnSc3	datum
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
nejstarší	starý	k2eAgFnSc1d3	nejstarší
dochovaná	dochovaný	k2eAgFnSc1d1	dochovaná
listina	listina	k1gFnSc1	listina
o	o	k7c6	o
městě	město	k1gNnSc6	město
Nymburce	Nymburk	k1gInSc6	Nymburk
vydaná	vydaný	k2eAgNnPc1d1	vydané
pro	pro	k7c4	pro
zdejší	zdejší	k2eAgInSc4d1	zdejší
dominikánský	dominikánský	k2eAgInSc4d1	dominikánský
klášter	klášter	k1gInSc4	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
text	text	k1gInSc1	text
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
rukopisu	rukopis	k1gInSc2	rukopis
ve	v	k7c6	v
formulářové	formulářový	k2eAgFnSc6d1	formulářová
sbírce	sbírka	k1gFnSc6	sbírka
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
protože	protože	k8xS	protože
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
text	text	k1gInSc4	text
nezachovaný	zachovaný	k2eNgInSc4d1	nezachovaný
v	v	k7c6	v
originální	originální	k2eAgFnSc6d1	originální
listině	listina	k1gFnSc6	listina
ani	ani	k8xC	ani
v	v	k7c6	v
nějakém	nějaký	k3yIgInSc6	nějaký
kopiáři	kopiář	k1gInSc6	kopiář
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
ve	v	k7c6	v
formulářové	formulářový	k2eAgFnSc6d1	formulářová
sbírce	sbírka	k1gFnSc6	sbírka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
přistupovat	přistupovat	k5eAaImF	přistupovat
obzvláště	obzvláště	k6eAd1	obzvláště
opatrně	opatrně	k6eAd1	opatrně
<g/>
.	.	kIx.	.
</s>
<s>
Důkladnému	důkladný	k2eAgInSc3d1	důkladný
kritickému	kritický	k2eAgInSc3d1	kritický
rozboru	rozbor	k1gInSc3	rozbor
bylo	být	k5eAaImAgNnS	být
podrobeno	podroben	k2eAgNnSc1d1	podrobeno
zejména	zejména	k9	zejména
údajné	údajný	k2eAgNnSc4d1	údajné
datum	datum	k1gNnSc4	datum
vzniku	vznik	k1gInSc2	vznik
zápisu	zápis	k1gInSc2	zápis
do	do	k7c2	do
sbírky	sbírka	k1gFnSc2	sbírka
–	–	k?	–
rok	rok	k1gInSc1	rok
1257	[number]	k4	1257
<g/>
.	.	kIx.	.
</s>
<s>
Historický	historický	k2eAgInSc1d1	historický
výzkum	výzkum	k1gInSc1	výzkum
toto	tento	k3xDgNnSc4	tento
datum	datum	k1gNnSc4	datum
zcela	zcela	k6eAd1	zcela
jistě	jistě	k6eAd1	jistě
vyloučil	vyloučit	k5eAaPmAgInS	vyloučit
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
klade	klást	k5eAaImIp3nS	klást
vznik	vznik	k1gInSc4	vznik
listiny	listina	k1gFnSc2	listina
pro	pro	k7c4	pro
dominikánský	dominikánský	k2eAgInSc4d1	dominikánský
klášter	klášter	k1gInSc4	klášter
až	až	k9	až
na	na	k7c4	na
samý	samý	k3xTgInSc4	samý
sklonek	sklonek	k1gInSc4	sklonek
vlády	vláda	k1gFnSc2	vláda
krále	král	k1gMnSc2	král
Přemysla	Přemysl	k1gMnSc2	Přemysl
Otakara	Otakar	k1gMnSc2	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
přesněji	přesně	k6eAd2	přesně
mezi	mezi	k7c7	mezi
25	[number]	k4	25
<g/>
.	.	kIx.	.
listopad	listopad	k1gInSc1	listopad
1274	[number]	k4	1274
a	a	k8xC	a
21	[number]	k4	21
<g/>
.	.	kIx.	.
listopad	listopad	k1gInSc1	listopad
1276	[number]	k4	1276
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
rok	rok	k1gInSc1	rok
1275	[number]	k4	1275
jeví	jevit	k5eAaImIp3nS	jevit
jako	jako	k9	jako
nejpravděpodobnější	pravděpodobný	k2eAgNnSc1d3	nejpravděpodobnější
datum	datum	k1gNnSc1	datum
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
král	král	k1gMnSc1	král
uložil	uložit	k5eAaPmAgMnS	uložit
lokátorovi	lokátor	k1gMnSc3	lokátor
Konrádovi	Konrád	k1gMnSc3	Konrád
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nP	uvádět
Kunrátovi	Kunrátův	k2eAgMnPc1d1	Kunrátův
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vyměřil	vyměřit	k5eAaPmAgMnS	vyměřit
a	a	k8xC	a
zřídil	zřídit	k5eAaPmAgMnS	zřídit
město	město	k1gNnSc4	město
Nymburk	Nymburk	k1gInSc1	Nymburk
<g/>
.	.	kIx.	.
</s>
<s>
Samotné	samotný	k2eAgNnSc1d1	samotné
založení	založení	k1gNnSc1	založení
bylo	být	k5eAaImAgNnS	být
však	však	k9	však
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
dokončeno	dokončit	k5eAaPmNgNnS	dokončit
až	až	k9	až
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
krále	král	k1gMnSc2	král
Václava	Václav	k1gMnSc2	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
Václava	Václav	k1gMnSc2	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
došlo	dojít	k5eAaPmAgNnS	dojít
též	též	k9	též
k	k	k7c3	k
výstavbě	výstavba	k1gFnSc3	výstavba
gotického	gotický	k2eAgInSc2d1	gotický
chrámu	chrám	k1gInSc2	chrám
sv.	sv.	kA	sv.
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Jiljí	Jiljí	k1gMnSc2	Jiljí
<g/>
)	)	kIx)	)
a	a	k8xC	a
dominikánského	dominikánský	k2eAgInSc2d1	dominikánský
kláštera	klášter	k1gInSc2	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
obehnáno	obehnat	k5eAaPmNgNnS	obehnat
hradbami	hradba	k1gFnPc7	hradba
z	z	k7c2	z
pálených	pálený	k2eAgFnPc2d1	pálená
cihel	cihla	k1gFnPc2	cihla
s	s	k7c7	s
asi	asi	k9	asi
padesáti	padesát	k4xCc7	padesát
věžemi	věž	k1gFnPc7	věž
a	a	k8xC	a
dvěma	dva	k4xCgInPc7	dva
obrannými	obranný	k2eAgInPc7d1	obranný
příkopy	příkop	k1gInPc7	příkop
napájenými	napájený	k2eAgInPc7d1	napájený
z	z	k7c2	z
Labe	Labe	k1gNnSc2	Labe
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
hradeb	hradba	k1gFnPc2	hradba
byly	být	k5eAaImAgInP	být
čtyři	čtyři	k4xCgFnPc4	čtyři
městské	městský	k2eAgFnPc4d1	městská
brány	brána	k1gFnPc4	brána
a	a	k8xC	a
branka	branka	k1gFnSc1	branka
k	k	k7c3	k
řece	řeka	k1gFnSc3	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
západ	západ	k1gInSc4	západ
vedla	vést	k5eAaImAgFnS	vést
brána	brána	k1gFnSc1	brána
Svatojiřská	svatojiřský	k2eAgFnSc1d1	Svatojiřská
<g/>
,	,	kIx,	,
na	na	k7c4	na
severozápad	severozápad	k1gInSc4	severozápad
Velibská	Velibský	k2eAgFnSc1d1	Velibský
či	či	k8xC	či
též	též	k9	též
zvaná	zvaný	k2eAgFnSc1d1	zvaná
Boleslavská	boleslavský	k2eAgFnSc1d1	Boleslavská
<g/>
,	,	kIx,	,
na	na	k7c4	na
severovýchod	severovýchod	k1gInSc4	severovýchod
pak	pak	k6eAd1	pak
Bobnická	Bobnický	k2eAgFnSc1d1	Bobnická
brána	brána	k1gFnSc1	brána
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
straně	strana	k1gFnSc6	strana
stála	stát	k5eAaImAgFnS	stát
Mostecká	mostecký	k2eAgFnSc1d1	Mostecká
neboli	neboli	k8xC	neboli
Labská	labský	k2eAgFnSc1d1	Labská
brána	brána	k1gFnSc1	brána
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yRgFnSc4	který
navazoval	navazovat	k5eAaImAgInS	navazovat
dřevěný	dřevěný	k2eAgInSc1d1	dřevěný
most	most	k1gInSc1	most
přes	přes	k7c4	přes
Labe	Labe	k1gNnSc4	Labe
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgInPc4d2	veliký
z	z	k7c2	z
věží	věž	k1gFnPc2	věž
této	tento	k3xDgFnSc2	tento
brány	brána	k1gFnSc2	brána
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
renesanci	renesance	k1gFnSc6	renesance
opatřena	opatřit	k5eAaPmNgFnS	opatřit
zdobnou	zdobný	k2eAgFnSc7d1	zdobná
fasádou	fasáda	k1gFnSc7	fasáda
<g/>
.	.	kIx.	.
</s>
<s>
Brány	brána	k1gFnPc1	brána
měly	mít	k5eAaImAgFnP	mít
hranolový	hranolový	k2eAgInSc4d1	hranolový
tvar	tvar	k1gInSc4	tvar
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
tří	tři	k4xCgFnPc2	tři
či	či	k8xC	či
čtyřpodlažní	čtyřpodlažní	k2eAgInPc1d1	čtyřpodlažní
<g/>
.	.	kIx.	.
</s>
<s>
Průjezd	průjezd	k1gInSc1	průjezd
v	v	k7c6	v
přízemí	přízemí	k1gNnSc6	přízemí
vytvářely	vytvářet	k5eAaImAgInP	vytvářet
gotické	gotický	k2eAgInPc1d1	gotický
lomené	lomený	k2eAgInPc1d1	lomený
vstupní	vstupní	k2eAgInPc1d1	vstupní
oblouky	oblouk	k1gInPc1	oblouk
<g/>
,	,	kIx,	,
na	na	k7c6	na
vnější	vnější	k2eAgFnSc6d1	vnější
straně	strana	k1gFnSc6	strana
doplněný	doplněný	k2eAgInSc1d1	doplněný
spouštěcí	spouštěcí	k2eAgFnSc7d1	spouštěcí
mříží	mříž	k1gFnSc7	mříž
a	a	k8xC	a
bytelnými	bytelný	k2eAgInPc7d1	bytelný
vraty	vrat	k1gInPc7	vrat
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Svatojiřské	svatojiřský	k2eAgFnSc2d1	Svatojiřská
brány	brána	k1gFnSc2	brána
bylo	být	k5eAaImAgNnS	být
navíc	navíc	k6eAd1	navíc
vybudováno	vybudovat	k5eAaPmNgNnS	vybudovat
předbraní	předbraní	k1gNnSc1	předbraní
s	s	k7c7	s
cimbuřím	cimbuří	k1gNnSc7	cimbuří
zasahující	zasahující	k2eAgFnSc4d1	zasahující
do	do	k7c2	do
obranného	obranný	k2eAgInSc2d1	obranný
příkopu	příkop	k1gInSc2	příkop
a	a	k8xC	a
padací	padací	k2eAgInSc4d1	padací
most	most	k1gInSc4	most
<g/>
.	.	kIx.	.
</s>
<s>
Mostecká	mostecký	k2eAgFnSc1d1	Mostecká
a	a	k8xC	a
Boleslavská	boleslavský	k2eAgFnSc1d1	Boleslavská
brána	brána	k1gFnSc1	brána
byla	být	k5eAaImAgFnS	být
stržena	strhnout	k5eAaPmNgFnS	strhnout
po	po	k7c6	po
velkém	velký	k2eAgInSc6d1	velký
požáru	požár	k1gInSc6	požár
města	město	k1gNnSc2	město
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1838	[number]	k4	1838
<g/>
.	.	kIx.	.
</s>
<s>
Svatojiřská	svatojiřský	k2eAgFnSc1d1	Svatojiřská
brána	brána	k1gFnSc1	brána
byla	být	k5eAaImAgFnS	být
zbořena	zbořen	k2eAgFnSc1d1	zbořena
roku	rok	k1gInSc2	rok
1871	[number]	k4	1871
a	a	k8xC	a
stejný	stejný	k2eAgInSc1d1	stejný
osud	osud	k1gInSc1	osud
potkal	potkat	k5eAaPmAgInS	potkat
i	i	k9	i
poslední	poslední	k2eAgFnSc4d1	poslední
Boleslavskou	boleslavský	k2eAgFnSc4d1	Boleslavská
bránu	brána	k1gFnSc4	brána
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1884	[number]	k4	1884
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Koncem	koncem	k7c2	koncem
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
Nymburce	Nymburk	k1gInSc6	Nymburk
zásluhou	zásluhou	k7c2	zásluhou
Samuela	Samuel	k1gMnSc2	Samuel
Ignáce	Ignác	k1gMnSc2	Ignác
Turnovského	turnovský	k2eAgMnSc2d1	turnovský
poštovní	poštovní	k2eAgFnSc2d1	poštovní
stanice	stanice	k1gFnPc1	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1634	[number]	k4	1634
město	město	k1gNnSc4	město
vypálili	vypálit	k5eAaPmAgMnP	vypálit
Sasové	Sas	k1gMnPc1	Sas
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
ve	v	k7c6	v
zdejším	zdejší	k2eAgInSc6d1	zdejší
kostele	kostel	k1gInSc6	kostel
povraždili	povraždit	k5eAaPmAgMnP	povraždit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
200	[number]	k4	200
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Telegraf	telegraf	k1gInSc1	telegraf
byl	být	k5eAaImAgInS	být
zaveden	zavést	k5eAaPmNgInS	zavést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1850	[number]	k4	1850
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
místní	místní	k2eAgInPc1d1	místní
telefony	telefon	k1gInPc1	telefon
byly	být	k5eAaImAgInP	být
instalovány	instalovat	k5eAaBmNgInP	instalovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1900	[number]	k4	1900
a	a	k8xC	a
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
i	i	k9	i
linky	linka	k1gFnPc4	linka
meziměstské	meziměstský	k2eAgFnPc4d1	meziměstská
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
začalo	začít	k5eAaPmAgNnS	začít
industrializovat	industrializovat	k5eAaBmF	industrializovat
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
nejvýznamnějším	významný	k2eAgInSc7d3	nejvýznamnější
podnikem	podnik	k1gInSc7	podnik
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
železnice	železnice	k1gFnSc1	železnice
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
správa	správa	k1gFnSc1	správa
v	v	k7c6	v
Nymburce	Nymburk	k1gInSc6	Nymburk
vybudovala	vybudovat	k5eAaPmAgFnS	vybudovat
své	svůj	k3xOyFgNnSc4	svůj
ředitelství	ředitelství	k1gNnSc4	ředitelství
<g/>
,	,	kIx,	,
dílny	dílna	k1gFnPc4	dílna
<g/>
,	,	kIx,	,
topírnu	topírna	k1gFnSc4	topírna
a	a	k8xC	a
železniční	železniční	k2eAgFnSc4d1	železniční
kolonii	kolonie	k1gFnSc4	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
ve	v	k7c6	v
městě	město	k1gNnSc6	město
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
cukrovar	cukrovar	k1gInSc1	cukrovar
<g/>
,	,	kIx,	,
pivovar	pivovar	k1gInSc1	pivovar
<g/>
,	,	kIx,	,
korkovna	korkovna	k1gFnSc1	korkovna
a	a	k8xC	a
místní	místní	k2eAgInPc1d1	místní
mlýny	mlýn	k1gInPc1	mlýn
byly	být	k5eAaImAgInP	být
zmodernizovány	zmodernizovat	k5eAaPmNgInP	zmodernizovat
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
vzrůstajícímu	vzrůstající	k2eAgInSc3d1	vzrůstající
blahobytu	blahobyt	k1gInSc3	blahobyt
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
industrializace	industrializace	k1gFnSc1	industrializace
přinesla	přinést	k5eAaPmAgFnS	přinést
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
město	město	k1gNnSc1	město
mohlo	moct	k5eAaImAgNnS	moct
dovolit	dovolit	k5eAaPmF	dovolit
budování	budování	k1gNnSc1	budování
veřejných	veřejný	k2eAgFnPc2d1	veřejná
staveb	stavba	k1gFnPc2	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
tak	tak	k9	tak
postavena	postaven	k2eAgFnSc1d1	postavena
nemocnice	nemocnice	k1gFnSc1	nemocnice
<g/>
,	,	kIx,	,
měšťanská	měšťanský	k2eAgFnSc1d1	měšťanská
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
sokolovna	sokolovna	k1gFnSc1	sokolovna
<g/>
,	,	kIx,	,
evangelický	evangelický	k2eAgInSc1d1	evangelický
kostel	kostel	k1gInSc1	kostel
ci	ci	k0	ci
synagoga	synagoga	k1gFnSc1	synagoga
<g/>
.	.	kIx.	.
</s>
<s>
Rozvoj	rozvoj	k1gInSc1	rozvoj
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
též	též	k9	též
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
úprava	úprava	k1gFnSc1	úprava
toku	tok	k1gInSc2	tok
Labe	Labe	k1gNnSc2	Labe
<g/>
,	,	kIx,	,
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
proslulá	proslulý	k2eAgFnSc1d1	proslulá
budova	budova	k1gFnSc1	budova
krematoria	krematorium	k1gNnSc2	krematorium
a	a	k8xC	a
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgFnPc2d1	další
moderních	moderní	k2eAgFnPc2d1	moderní
staveb	stavba	k1gFnPc2	stavba
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tehdejším	tehdejší	k2eAgNnSc7d1	tehdejší
érou	éra	k1gFnSc7	éra
Nymburka	Nymburk	k1gInSc2	Nymburk
byla	být	k5eAaImAgFnS	být
nerozlučně	rozlučně	k6eNd1	rozlučně
spojena	spojen	k2eAgFnSc1d1	spojena
tvorba	tvorba	k1gFnSc1	tvorba
slavného	slavný	k2eAgMnSc2d1	slavný
spisovatele	spisovatel	k1gMnSc2	spisovatel
Bohumila	Bohumil	k1gMnSc2	Bohumil
Hrabala	Hrabal	k1gMnSc2	Hrabal
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Územněsprávní	územněsprávní	k2eAgNnSc4d1	územněsprávní
začlenění	začlenění	k1gNnSc4	začlenění
===	===	k?	===
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
územněsprávního	územněsprávní	k2eAgNnSc2d1	územněsprávní
začleňování	začleňování	k1gNnSc2	začleňování
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
období	období	k1gNnSc4	období
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1850	[number]	k4	1850
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
chronologickém	chronologický	k2eAgInSc6d1	chronologický
přehledu	přehled	k1gInSc6	přehled
je	být	k5eAaImIp3nS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
územně	územně	k6eAd1	územně
administrativní	administrativní	k2eAgFnSc4d1	administrativní
příslušnost	příslušnost	k1gFnSc4	příslušnost
obce	obec	k1gFnSc2	obec
v	v	k7c6	v
roce	rok	k1gInSc6	rok
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
došlo	dojít	k5eAaPmAgNnS	dojít
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1850	[number]	k4	1850
země	země	k1gFnSc1	země
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
kraj	kraj	k1gInSc1	kraj
Jičín	Jičín	k1gInSc1	Jičín
<g/>
,	,	kIx,	,
politický	politický	k2eAgInSc1d1	politický
i	i	k8xC	i
soudní	soudní	k2eAgInSc1d1	soudní
okres	okres	k1gInSc1	okres
Nymburk	Nymburk	k1gInSc1	Nymburk
</s>
</p>
<p>
<s>
1855	[number]	k4	1855
země	země	k1gFnSc1	země
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
kraj	kraj	k1gInSc1	kraj
Mladá	mladá	k1gFnSc1	mladá
Boleslav	Boleslav	k1gMnSc1	Boleslav
<g/>
,	,	kIx,	,
soudní	soudní	k2eAgInSc1d1	soudní
okres	okres	k1gInSc1	okres
Nymburk	Nymburk	k1gInSc1	Nymburk
</s>
</p>
<p>
<s>
1868	[number]	k4	1868
země	země	k1gFnSc1	země
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
politický	politický	k2eAgInSc4d1	politický
okres	okres	k1gInSc4	okres
Poděbrady	Poděbrady	k1gInPc7	Poděbrady
<g/>
,	,	kIx,	,
soudní	soudní	k2eAgInSc1d1	soudní
okres	okres	k1gInSc1	okres
Nymburk	Nymburk	k1gInSc1	Nymburk
</s>
</p>
<p>
<s>
1936	[number]	k4	1936
země	země	k1gFnSc1	země
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
politický	politický	k2eAgInSc1d1	politický
i	i	k8xC	i
soudní	soudní	k2eAgInSc1d1	soudní
okres	okres	k1gInSc1	okres
Nymburk	Nymburk	k1gInSc1	Nymburk
</s>
</p>
<p>
<s>
1939	[number]	k4	1939
země	země	k1gFnSc1	země
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
Oberlandrat	Oberlandrat	k1gInSc1	Oberlandrat
Kolín	Kolín	k1gInSc1	Kolín
<g/>
,	,	kIx,	,
politický	politický	k2eAgInSc1d1	politický
i	i	k8xC	i
soudní	soudní	k2eAgInSc1d1	soudní
okres	okres	k1gInSc1	okres
Nymburk	Nymburk	k1gInSc1	Nymburk
</s>
</p>
<p>
<s>
1942	[number]	k4	1942
země	země	k1gFnSc1	země
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
Oberlandrat	Oberlandrat	k1gInSc1	Oberlandrat
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
,	,	kIx,	,
politický	politický	k2eAgInSc1d1	politický
i	i	k8xC	i
soudní	soudní	k2eAgInSc1d1	soudní
okres	okres	k1gInSc1	okres
Nymburk	Nymburk	k1gInSc1	Nymburk
</s>
</p>
<p>
<s>
1945	[number]	k4	1945
země	země	k1gFnSc1	země
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
správní	správní	k2eAgInSc1d1	správní
i	i	k8xC	i
soudní	soudní	k2eAgInSc1d1	soudní
okres	okres	k1gInSc1	okres
Nymburk	Nymburk	k1gInSc1	Nymburk
</s>
</p>
<p>
<s>
1949	[number]	k4	1949
Pražský	pražský	k2eAgInSc1d1	pražský
kraj	kraj	k1gInSc1	kraj
<g/>
,	,	kIx,	,
okres	okres	k1gInSc1	okres
Nymburk	Nymburk	k1gInSc1	Nymburk
</s>
</p>
<p>
<s>
1960	[number]	k4	1960
Středočeský	středočeský	k2eAgInSc1d1	středočeský
kraj	kraj	k1gInSc1	kraj
<g/>
,	,	kIx,	,
okres	okres	k1gInSc1	okres
Nymburk	Nymburk	k1gInSc1	Nymburk
</s>
</p>
<p>
<s>
2003	[number]	k4	2003
Středočeský	středočeský	k2eAgInSc1d1	středočeský
kraj	kraj	k1gInSc1	kraj
<g/>
,	,	kIx,	,
okres	okres	k1gInSc1	okres
Nymburk	Nymburk	k1gInSc1	Nymburk
<g/>
,	,	kIx,	,
obec	obec	k1gFnSc1	obec
s	s	k7c7	s
rozšířenou	rozšířený	k2eAgFnSc7d1	rozšířená
působností	působnost	k1gFnSc7	působnost
Nymburk	Nymburk	k1gInSc1	Nymburk
</s>
</p>
<p>
<s>
===	===	k?	===
Rok	rok	k1gInSc1	rok
1932	[number]	k4	1932
===	===	k?	===
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
Nymburk	Nymburk	k1gInSc1	Nymburk
(	(	kIx(	(
<g/>
11	[number]	k4	11
890	[number]	k4	890
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
evidovány	evidovat	k5eAaImNgFnP	evidovat
tyto	tento	k3xDgFnPc1	tento
živnosti	živnost	k1gFnPc1	živnost
a	a	k8xC	a
obchody	obchod	k1gInPc1	obchod
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Instituce	instituce	k1gFnSc1	instituce
<g/>
:	:	kIx,	:
okresní	okresní	k2eAgInSc1d1	okresní
soud	soud	k1gInSc1	soud
<g/>
,	,	kIx,	,
berní	berní	k2eAgInSc1d1	berní
úřad	úřad	k1gInSc1	úřad
<g/>
,	,	kIx,	,
katastrální	katastrální	k2eAgInSc1d1	katastrální
zeměměřičský	zeměměřičský	k2eAgInSc4d1	zeměměřičský
úřad	úřad	k1gInSc4	úřad
<g/>
,	,	kIx,	,
důchodkový	důchodkový	k2eAgInSc4d1	důchodkový
kontrolní	kontrolní	k2eAgInSc4d1	kontrolní
úřad	úřad	k1gInSc4	úřad
<g/>
,	,	kIx,	,
četnická	četnický	k2eAgFnSc1d1	četnická
stanice	stanice	k1gFnSc1	stanice
<g/>
,	,	kIx,	,
poštovní	poštovní	k2eAgInSc1d1	poštovní
úřad	úřad	k1gInSc1	úřad
<g/>
,	,	kIx,	,
telefonní	telefonní	k2eAgInSc1d1	telefonní
úřad	úřad	k1gInSc1	úřad
<g/>
,	,	kIx,	,
telegrafní	telegrafní	k2eAgInSc1d1	telegrafní
úřad	úřad	k1gInSc1	úřad
<g/>
,	,	kIx,	,
3	[number]	k4	3
katol	katola	k1gFnPc2	katola
<g/>
.	.	kIx.	.
kostely	kostel	k1gInPc1	kostel
<g/>
,	,	kIx,	,
evang	evang	k1gInSc1	evang
<g/>
.	.	kIx.	.
kostel	kostel	k1gInSc1	kostel
<g/>
,	,	kIx,	,
synagoga	synagoga	k1gFnSc1	synagoga
<g/>
,	,	kIx,	,
státní	státní	k2eAgFnSc1d1	státní
vyšší	vysoký	k2eAgFnSc1d2	vyšší
reálka	reálka	k1gFnSc1	reálka
<g/>
,	,	kIx,	,
městské	městský	k2eAgNnSc1d1	Městské
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
městské	městský	k2eAgNnSc1d1	Městské
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
,	,	kIx,	,
okresní	okresní	k2eAgFnSc1d1	okresní
nemocnice	nemocnice	k1gFnSc1	nemocnice
<g/>
,	,	kIx,	,
městský	městský	k2eAgInSc1d1	městský
chudobinec	chudobinec	k1gInSc1	chudobinec
<g/>
,	,	kIx,	,
krematorium	krematorium	k1gNnSc1	krematorium
<g/>
,	,	kIx,	,
sbor	sbor	k1gInSc1	sbor
dobrovolných	dobrovolný	k2eAgMnPc2d1	dobrovolný
hasičů	hasič	k1gMnPc2	hasič
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Živnosti	živnost	k1gFnPc1	živnost
a	a	k8xC	a
průmysl	průmysl	k1gInSc1	průmysl
<g/>
:	:	kIx,	:
obchodní	obchodní	k2eAgNnSc1d1	obchodní
grémium	grémium	k1gNnSc1	grémium
<g/>
,	,	kIx,	,
společenstvo	společenstvo	k1gNnSc1	společenstvo
cukrářů	cukrář	k1gMnPc2	cukrář
<g/>
,	,	kIx,	,
holičů	holič	k1gMnPc2	holič
<g/>
,	,	kIx,	,
hostinských	hostinský	k1gMnPc2	hostinský
<g/>
,	,	kIx,	,
krejčí	krejčí	k1gMnSc1	krejčí
<g/>
,	,	kIx,	,
malířů	malíř	k1gMnPc2	malíř
a	a	k8xC	a
lakýrníků	lakýrník	k1gMnPc2	lakýrník
<g/>
,	,	kIx,	,
obuvníků	obuvník	k1gMnPc2	obuvník
<g/>
,	,	kIx,	,
pekařů	pekař	k1gMnPc2	pekař
<g/>
,	,	kIx,	,
řezníků	řezník	k1gMnPc2	řezník
<g/>
,	,	kIx,	,
stavebních	stavební	k2eAgFnPc2d1	stavební
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
živností	živnost	k1gFnPc2	živnost
<g/>
,	,	kIx,	,
cukrovar	cukrovar	k1gInSc1	cukrovar
<g/>
,	,	kIx,	,
továrna	továrna	k1gFnSc1	továrna
na	na	k7c4	na
šamotová	šamotový	k2eAgNnPc4d1	šamotové
kamna	kamna	k1gNnPc4	kamna
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
továrna	továrna	k1gFnSc1	továrna
na	na	k7c4	na
korkové	korkový	k2eAgNnSc4d1	korkové
zboží	zboží	k1gNnSc4	zboží
<g/>
,	,	kIx,	,
likérka	likérka	k1gFnSc1	likérka
<g/>
,	,	kIx,	,
podniky	podnik	k1gInPc1	podnik
Griotte	Griott	k1gInSc5	Griott
<g/>
,	,	kIx,	,
továrna	továrna	k1gFnSc1	továrna
na	na	k7c4	na
mýdla	mýdlo	k1gNnPc4	mýdlo
<g/>
,	,	kIx,	,
pivovar	pivovar	k1gInSc1	pivovar
<g/>
,	,	kIx,	,
továrna	továrna	k1gFnSc1	továrna
na	na	k7c4	na
varné	varný	k2eAgFnPc4d1	varná
trubky	trubka	k1gFnPc4	trubka
<g/>
,	,	kIx,	,
betonové	betonový	k2eAgFnPc4d1	betonová
a	a	k8xC	a
železobetonové	železobetonový	k2eAgNnSc1d1	železobetonové
stavitelství	stavitelství	k1gNnSc1	stavitelství
<g/>
,	,	kIx,	,
3	[number]	k4	3
cihelny	cihelna	k1gFnPc4	cihelna
<g/>
,	,	kIx,	,
továrna	továrna	k1gFnSc1	továrna
na	na	k7c4	na
hořčice	hořčice	k1gFnPc4	hořčice
<g/>
,	,	kIx,	,
kotlárna	kotlárna	k1gFnSc1	kotlárna
a	a	k8xC	a
strojírna	strojírna	k1gFnSc1	strojírna
<g/>
,	,	kIx,	,
octárna	octárna	k1gFnSc1	octárna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Služby	služba	k1gFnPc1	služba
(	(	kIx(	(
<g/>
výběr	výběr	k1gInSc1	výběr
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
9	[number]	k4	9
lékařů	lékař	k1gMnPc2	lékař
<g/>
,	,	kIx,	,
2	[number]	k4	2
zubní	zubní	k2eAgMnPc1d1	zubní
lékaři	lékař	k1gMnPc1	lékař
<g/>
,	,	kIx,	,
2	[number]	k4	2
zvěrolékaři	zvěrolékař	k1gMnPc7	zvěrolékař
<g/>
,	,	kIx,	,
4	[number]	k4	4
advokáti	advokát	k1gMnPc1	advokát
<g/>
,	,	kIx,	,
notář	notář	k1gMnSc1	notář
<g/>
,	,	kIx,	,
Moravská	moravský	k2eAgFnSc1d1	Moravská
banka	banka	k1gFnSc1	banka
<g/>
,	,	kIx,	,
biograf	biograf	k1gMnSc1	biograf
Sokol	Sokol	k1gMnSc1	Sokol
<g/>
,	,	kIx,	,
bio	bio	k?	bio
invalidů	invalid	k1gInPc2	invalid
<g/>
,	,	kIx,	,
fotoateliér	fotoateliér	k1gInSc1	fotoateliér
<g/>
,	,	kIx,	,
geometr	geometr	k1gInSc1	geometr
<g/>
,	,	kIx,	,
hotely	hotel	k1gInPc1	hotel
(	(	kIx(	(
<g/>
Grandhotel	grandhotel	k1gInSc1	grandhotel
Bláha	Bláha	k1gMnSc1	Bláha
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Horák	Horák	k1gMnSc1	Horák
<g/>
,	,	kIx,	,
Na	na	k7c4	na
Knížecí	knížecí	k2eAgInSc4d1	knížecí
<g/>
,	,	kIx,	,
Národní	národní	k2eAgInSc4d1	národní
dům	dům	k1gInSc4	dům
<g/>
,	,	kIx,	,
Slavie	slavie	k1gFnSc1	slavie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2	[number]	k4	2
kavárny	kavárna	k1gFnPc4	kavárna
<g/>
,	,	kIx,	,
lázně	lázeň	k1gFnPc4	lázeň
<g/>
,	,	kIx,	,
2	[number]	k4	2
lékárny	lékárna	k1gFnSc2	lékárna
<g/>
,	,	kIx,	,
3	[number]	k4	3
restaurace	restaurace	k1gFnSc1	restaurace
<g/>
,	,	kIx,	,
Občanská	občanský	k2eAgFnSc1d1	občanská
záložna	záložna	k1gFnSc1	záložna
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
<g/>
,	,	kIx,	,
Okresní	okresní	k2eAgFnSc1d1	okresní
záložna	záložna	k1gFnSc1	záložna
<g/>
,	,	kIx,	,
Spořitelna	spořitelna	k1gFnSc1	spořitelna
města	město	k1gNnSc2	město
Nymburka	Nymburk	k1gInSc2	Nymburk
<g/>
,	,	kIx,	,
Živnostenská	živnostenský	k2eAgFnSc1d1	Živnostenská
záložna	záložna	k1gFnSc1	záložna
<g/>
,	,	kIx,	,
Lidová	lidový	k2eAgFnSc1d1	lidová
záložna	záložna	k1gFnSc1	záložna
<g/>
,	,	kIx,	,
2	[number]	k4	2
vinárny	vinárna	k1gFnSc2	vinárna
<g/>
,	,	kIx,	,
4	[number]	k4	4
zubní	zubní	k2eAgInPc1d1	zubní
ateliéry	ateliér	k1gInPc1	ateliér
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
obci	obec	k1gFnSc6	obec
Drahelice	Drahelice	k1gFnSc2	Drahelice
(	(	kIx(	(
<g/>
přísl	přísl	k1gInSc1	přísl
<g/>
.	.	kIx.	.
</s>
<s>
Komárno	Komárno	k1gNnSc1	Komárno
<g/>
,	,	kIx,	,
724	[number]	k4	724
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
samostatná	samostatný	k2eAgFnSc1d1	samostatná
obec	obec	k1gFnSc1	obec
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
Nymburka	Nymburk	k1gInSc2	Nymburk
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
evidovány	evidovat	k5eAaImNgFnP	evidovat
tyto	tento	k3xDgFnPc1	tento
živnosti	živnost	k1gFnPc1	živnost
a	a	k8xC	a
obchody	obchod	k1gInPc1	obchod
<g/>
:	:	kIx,	:
3	[number]	k4	3
hostince	hostinec	k1gInSc2	hostinec
<g/>
,	,	kIx,	,
kovář	kovář	k1gMnSc1	kovář
<g/>
,	,	kIx,	,
obchod	obchod	k1gInSc1	obchod
s	s	k7c7	s
mlékem	mléko	k1gNnSc7	mléko
<g/>
,	,	kIx,	,
2	[number]	k4	2
mlýny	mlýn	k1gInPc1	mlýn
<g/>
,	,	kIx,	,
2	[number]	k4	2
obuvníci	obuvník	k1gMnPc1	obuvník
<g/>
,	,	kIx,	,
pila	pila	k1gFnSc1	pila
<g/>
,	,	kIx,	,
4	[number]	k4	4
rolníci	rolník	k1gMnPc1	rolník
<g/>
,	,	kIx,	,
řezník	řezník	k1gMnSc1	řezník
<g/>
,	,	kIx,	,
obchod	obchod	k1gInSc1	obchod
se	s	k7c7	s
smíšeným	smíšený	k2eAgNnSc7d1	smíšené
zbožím	zboží	k1gNnSc7	zboží
<g/>
,	,	kIx,	,
tesařský	tesařský	k2eAgMnSc1d1	tesařský
mistr	mistr	k1gMnSc1	mistr
<g/>
,	,	kIx,	,
2	[number]	k4	2
trafiky	trafika	k1gFnSc2	trafika
<g/>
,	,	kIx,	,
truhlář	truhlář	k1gMnSc1	truhlář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Průmysl	průmysl	k1gInSc1	průmysl
==	==	k?	==
</s>
</p>
<p>
<s>
Nymburk	Nymburk	k1gInSc1	Nymburk
je	být	k5eAaImIp3nS	být
důležitou	důležitý	k2eAgFnSc7d1	důležitá
železniční	železniční	k2eAgFnSc7d1	železniční
křižovatkou	křižovatka	k1gFnSc7	křižovatka
s	s	k7c7	s
depem	depo	k1gNnSc7	depo
kolejových	kolejový	k2eAgNnPc2d1	kolejové
vozidel	vozidlo	k1gNnPc2	vozidlo
ČD	ČD	kA	ČD
<g/>
.	.	kIx.	.
</s>
<s>
Důležitým	důležitý	k2eAgInSc7d1	důležitý
výrobním	výrobní	k2eAgInSc7d1	výrobní
podnikem	podnik	k1gInSc7	podnik
je	být	k5eAaImIp3nS	být
firma	firma	k1gFnSc1	firma
JDK	JDK	kA	JDK
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
chladicí	chladicí	k2eAgNnPc4d1	chladicí
zařízení	zařízení	k1gNnPc4	zařízení
a	a	k8xC	a
vyváží	vyvážet	k5eAaImIp3nS	vyvážet
je	on	k3xPp3gInPc4	on
do	do	k7c2	do
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
poboček	pobočka	k1gFnPc2	pobočka
nadnárodní	nadnárodní	k2eAgFnSc2d1	nadnárodní
společnosti	společnost	k1gFnSc2	společnost
Magna	Magn	k1gInSc2	Magn
Exteriors	Exteriors	k1gInSc1	Exteriors
&	&	k?	&
Interiors	Interiors	k1gInSc1	Interiors
(	(	kIx(	(
<g/>
Bohemia	bohemia	k1gFnSc1	bohemia
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zabývající	zabývající	k2eAgFnSc2d1	zabývající
se	se	k3xPyFc4	se
výrobou	výroba	k1gFnSc7	výroba
součástí	součást	k1gFnPc2	součást
pro	pro	k7c4	pro
automobilový	automobilový	k2eAgInSc4d1	automobilový
průmysl	průmysl	k1gInSc4	průmysl
<g/>
,	,	kIx,	,
a	a	k8xC	a
již	již	k6eAd1	již
zmíněný	zmíněný	k2eAgInSc1d1	zmíněný
Pivovar	pivovar	k1gInSc1	pivovar
Nymburk	Nymburk	k1gInSc1	Nymburk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
v	v	k7c6	v
Nymburce	Nymburk	k1gInSc6	Nymburk
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
evropskou	evropský	k2eAgFnSc4d1	Evropská
pobočku	pobočka	k1gFnSc4	pobočka
založila	založit	k5eAaPmAgFnS	založit
čínská	čínský	k2eAgFnSc1d1	čínská
firma	firma	k1gFnSc1	firma
Changhong	Changhonga	k1gFnPc2	Changhonga
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ve	v	k7c6	v
zdejším	zdejší	k2eAgInSc6d1	zdejší
výrobním	výrobní	k2eAgInSc6d1	výrobní
závodě	závod	k1gInSc6	závod
provádí	provádět	k5eAaImIp3nS	provádět
finální	finální	k2eAgFnSc1d1	finální
montáž	montáž	k1gFnSc1	montáž
LCD	LCD	kA	LCD
televizorů	televizor	k1gInPc2	televizor
pro	pro	k7c4	pro
celý	celý	k2eAgInSc4d1	celý
evropský	evropský	k2eAgInSc4d1	evropský
trh	trh	k1gInSc4	trh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
AZOS	AZOS	kA	AZOS
CZ	CZ	kA	CZ
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
nymburské	nymburský	k2eAgFnSc6d1	Nymburská
částí	část	k1gFnPc2	část
Zálabí	zálabí	k1gNnPc2	zálabí
zahájila	zahájit	k5eAaPmAgFnS	zahájit
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
2013	[number]	k4	2013
a	a	k8xC	a
2014	[number]	k4	2014
provoz	provoz	k1gInSc1	provoz
zinkovny	zinkovna	k1gFnSc2	zinkovna
firma	firma	k1gFnSc1	firma
AZOS	AZOS	kA	AZOS
CZ	CZ	kA	CZ
<g/>
,	,	kIx,	,
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
Místní	místní	k2eAgMnPc1d1	místní
občané	občan	k1gMnPc1	občan
si	se	k3xPyFc3	se
stěžovali	stěžovat	k5eAaImAgMnP	stěžovat
na	na	k7c4	na
obtěžující	obtěžující	k2eAgInSc4d1	obtěžující
zápach	zápach	k1gInSc4	zápach
a	a	k8xC	a
na	na	k7c4	na
zdravotní	zdravotní	k2eAgInPc4d1	zdravotní
problémy	problém	k1gInPc4	problém
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
majitel	majitel	k1gMnSc1	majitel
firmy	firma	k1gFnSc2	firma
Petr	Petr	k1gMnSc1	Petr
Šimák	Šimák	k1gMnSc1	Šimák
získal	získat	k5eAaPmAgMnS	získat
všechna	všechen	k3xTgNnPc4	všechen
potřebná	potřebný	k2eAgNnPc4d1	potřebné
povolení	povolení	k1gNnPc4	povolení
a	a	k8xC	a
ani	ani	k8xC	ani
měření	měření	k1gNnSc1	měření
imisí	imise	k1gFnPc2	imise
státním	státní	k2eAgInSc7d1	státní
zdravotním	zdravotní	k2eAgInSc7d1	zdravotní
ústavem	ústav	k1gInSc7	ústav
neprokázalo	prokázat	k5eNaPmAgNnS	prokázat
žádné	žádný	k3yNgFnPc4	žádný
nadlimitní	nadlimitní	k2eAgFnPc4d1	nadlimitní
koncentrace	koncentrace	k1gFnPc4	koncentrace
<g/>
.	.	kIx.	.
<g/>
Radnice	radnice	k1gFnSc1	radnice
Nymburka	Nymburk	k1gInSc2	Nymburk
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2014	[number]	k4	2014
přislíbila	přislíbit	k5eAaPmAgFnS	přislíbit
situaci	situace	k1gFnSc4	situace
prověřit	prověřit	k5eAaPmF	prověřit
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
Krajský	krajský	k2eAgInSc4d1	krajský
úřad	úřad	k1gInSc4	úřad
Středočeského	středočeský	k2eAgInSc2d1	středočeský
kraje	kraj	k1gInSc2	kraj
přizval	přizvat	k5eAaPmAgMnS	přizvat
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
nezávislého	závislý	k2eNgMnSc2d1	nezávislý
experta	expert	k1gMnSc2	expert
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
shledal	shledat	k5eAaPmAgMnS	shledat
v	v	k7c6	v
čištění	čištění	k1gNnSc6	čištění
emisí	emise	k1gFnPc2	emise
několik	několik	k4yIc1	několik
nedostatků	nedostatek	k1gInPc2	nedostatek
a	a	k8xC	a
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
původ	původ	k1gInSc4	původ
zápachu	zápach	k1gInSc2	zápach
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
prokázal	prokázat	k5eAaPmAgInS	prokázat
Státní	státní	k2eAgInSc1d1	státní
zdravotní	zdravotní	k2eAgInSc1d1	zdravotní
ústav	ústav	k1gInSc1	ústav
úniky	únik	k1gInPc1	únik
těkavých	těkavý	k2eAgFnPc2d1	těkavá
chemických	chemický	k2eAgFnPc2d1	chemická
látek	látka	k1gFnPc2	látka
z	z	k7c2	z
provozu	provoz	k1gInSc2	provoz
kataforézní	kataforézní	k2eAgFnSc2d1	kataforézní
linky	linka	k1gFnSc2	linka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Hradby	hradba	k1gFnPc1	hradba
===	===	k?	===
</s>
</p>
<p>
<s>
Symbolem	symbol	k1gInSc7	symbol
města	město	k1gNnSc2	město
jsou	být	k5eAaImIp3nP	být
cihlové	cihlový	k2eAgFnPc1d1	cihlová
hradby	hradba	k1gFnPc1	hradba
s	s	k7c7	s
hranolovými	hranolový	k2eAgFnPc7d1	hranolová
baštami	bašta	k1gFnPc7	bašta
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
město	město	k1gNnSc1	město
o	o	k7c6	o
ledvinovitém	ledvinovitý	k2eAgInSc6d1	ledvinovitý
půdorysu	půdorys	k1gInSc6	půdorys
obepínala	obepínat	k5eAaImAgFnS	obepínat
linie	linie	k1gFnSc1	linie
dvou	dva	k4xCgFnPc2	dva
hradeb	hradba	k1gFnPc2	hradba
<g/>
,	,	kIx,	,
bílé	bílý	k2eAgFnPc4d1	bílá
opukové	opukový	k2eAgFnPc4d1	opuková
a	a	k8xC	a
červené	červený	k2eAgFnPc4d1	červená
cihlové	cihlový	k2eAgFnPc4d1	cihlová
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
se	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
příkopy	příkop	k1gInPc7	příkop
(	(	kIx(	(
<g/>
zachovány	zachován	k2eAgFnPc1d1	zachována
dodnes	dodnes	k6eAd1	dodnes
coby	coby	k?	coby
Malé	Malé	k2eAgInPc1d1	Malé
a	a	k8xC	a
Velké	velký	k2eAgInPc1d1	velký
Valy	val	k1gInPc1	val
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hradeb	hradba	k1gFnPc2	hradba
dnes	dnes	k6eAd1	dnes
zůstaly	zůstat	k5eAaPmAgInP	zůstat
jen	jen	k9	jen
zbytky	zbytek	k1gInPc1	zbytek
<g/>
,	,	kIx,	,
úsek	úsek	k1gInSc1	úsek
na	na	k7c6	na
snímku	snímek	k1gInSc6	snímek
byl	být	k5eAaImAgMnS	být
počátkem	počátek	k1gInSc7	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
romanticky	romanticky	k6eAd1	romanticky
zrekonstruován	zrekonstruován	k2eAgMnSc1d1	zrekonstruován
<g/>
,	,	kIx,	,
další	další	k2eAgInPc1d1	další
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
zdí	zeď	k1gFnPc2	zeď
nalezneme	naleznout	k5eAaPmIp1nP	naleznout
v	v	k7c6	v
Hradební	hradební	k2eAgFnSc6d1	hradební
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kolonie	kolonie	k1gFnSc2	kolonie
===	===	k?	===
</s>
</p>
<p>
<s>
U	u	k7c2	u
hlavního	hlavní	k2eAgNnSc2d1	hlavní
nádraží	nádraží	k1gNnSc2	nádraží
dnes	dnes	k6eAd1	dnes
ohrožené	ohrožený	k2eAgInPc1d1	ohrožený
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
"	"	kIx"	"
<g/>
Zahradního	zahradní	k2eAgNnSc2d1	zahradní
města	město	k1gNnSc2	město
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
dělnické	dělnický	k2eAgFnSc2d1	Dělnická
železniční	železniční	k2eAgFnSc2d1	železniční
kolonie	kolonie	k1gFnSc2	kolonie
–	–	k?	–
společné	společný	k2eAgNnSc4d1	společné
dílo	dílo	k1gNnSc4	dílo
Carla	Carl	k1gMnSc2	Carl
Schlimpa	Schlimp	k1gMnSc2	Schlimp
a	a	k8xC	a
urbanistického	urbanistický	k2eAgMnSc2d1	urbanistický
vizionáře	vizionář	k1gMnSc2	vizionář
Camilla	Camill	k1gMnSc2	Camill
Sitteho	Sitte	k1gMnSc2	Sitte
<g/>
.	.	kIx.	.
</s>
<s>
Harmonické	harmonický	k2eAgNnSc1d1	harmonické
prostředí	prostředí	k1gNnSc1	prostředí
zeleně	zeleň	k1gFnSc2	zeleň
<g/>
,	,	kIx,	,
na	na	k7c4	na
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
kvalitní	kvalitní	k2eAgInSc4d1	kvalitní
životní	životní	k2eAgInSc4d1	životní
standard	standard	k1gInSc4	standard
a	a	k8xC	a
občanská	občanský	k2eAgFnSc1d1	občanská
vybavenost	vybavenost	k1gFnSc1	vybavenost
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
místě	místo	k1gNnSc6	místo
–	–	k?	–
to	ten	k3xDgNnSc1	ten
vše	všechen	k3xTgNnSc1	všechen
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
soustředilo	soustředit	k5eAaPmAgNnS	soustředit
v	v	k7c6	v
progresivní	progresivní	k2eAgFnSc6d1	progresivní
urbanistické	urbanistický	k2eAgFnSc6d1	urbanistická
myšlence	myšlenka	k1gFnSc6	myšlenka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Železničářská	železničářský	k2eAgFnSc1d1	Železničářská
kolonie	kolonie	k1gFnSc1	kolonie
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
samostatně	samostatně	k6eAd1	samostatně
hospodařícím	hospodařící	k2eAgInSc7d1	hospodařící
městským	městský	k2eAgInSc7d1	městský
celkem	celek	k1gInSc7	celek
s	s	k7c7	s
centrálním	centrální	k2eAgNnSc7d1	centrální
zásobováním	zásobování	k1gNnSc7	zásobování
potravinami	potravina	k1gFnPc7	potravina
a	a	k8xC	a
službami	služba	k1gFnPc7	služba
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
model	model	k1gInSc1	model
později	pozdě	k6eAd2	pozdě
převzal	převzít	k5eAaPmAgInS	převzít
i	i	k9	i
Baťa	Baťa	k1gMnSc1	Baťa
ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1916	[number]	k4	1916
byla	být	k5eAaImAgFnS	být
vybudována	vybudovat	k5eAaPmNgFnS	vybudovat
první	první	k4xOgFnSc1	první
část	část	k1gFnSc1	část
zahradního	zahradní	k2eAgNnSc2d1	zahradní
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
jednoposchoďové	jednoposchoďový	k2eAgInPc4d1	jednoposchoďový
dělnické	dělnický	k2eAgInPc4d1	dělnický
domy	dům	k1gInPc4	dům
<g/>
,	,	kIx,	,
domy	dům	k1gInPc4	dům
pro	pro	k7c4	pro
vyšší	vysoký	k2eAgMnPc4d2	vyšší
a	a	k8xC	a
nižší	nízký	k2eAgMnPc4d2	nižší
drážní	drážní	k2eAgMnPc4d1	drážní
úředníky	úředník	k1gMnPc4	úředník
<g/>
,	,	kIx,	,
prádelna	prádelna	k1gFnSc1	prádelna
<g/>
,	,	kIx,	,
koloniál	koloniál	k1gInSc1	koloniál
a	a	k8xC	a
dřevěné	dřevěný	k2eAgInPc1d1	dřevěný
domy	dům	k1gInPc1	dům
pro	pro	k7c4	pro
dělníky	dělník	k1gMnPc4	dělník
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
začala	začít	k5eAaPmAgFnS	začít
výstavba	výstavba	k1gFnSc1	výstavba
druhé	druhý	k4xOgFnSc2	druhý
části	část	k1gFnSc2	část
zahradního	zahradní	k2eAgNnSc2d1	zahradní
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
postaveny	postaven	k2eAgFnPc1d1	postavena
byly	být	k5eAaImAgFnP	být
přízemní	přízemní	k2eAgInPc4d1	přízemní
domky	domek	k1gInPc4	domek
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
zde	zde	k6eAd1	zde
bydlelo	bydlet	k5eAaImAgNnS	bydlet
222	[number]	k4	222
rodin	rodina	k1gFnPc2	rodina
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
počtu	počet	k1gInSc2	počet
přibližně	přibližně	k6eAd1	přibližně
1	[number]	k4	1
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
původním	původní	k2eAgInSc6d1	původní
návrhu	návrh	k1gInSc6	návrh
bylo	být	k5eAaImAgNnS	být
plánováno	plánovat	k5eAaImNgNnS	plánovat
i	i	k9	i
vybudování	vybudování	k1gNnSc3	vybudování
kostela	kostel	k1gInSc2	kostel
pro	pro	k7c4	pro
celý	celý	k2eAgInSc4d1	celý
komplex	komplex	k1gInSc4	komplex
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
sešlo	sejít	k5eAaPmAgNnS	sejít
a	a	k8xC	a
potřeby	potřeba	k1gFnSc2	potřeba
věřících	věřící	k1gMnPc2	věřící
uspokojila	uspokojit	k5eAaPmAgFnS	uspokojit
kaple	kaple	k1gFnSc1	kaple
v	v	k7c6	v
novostavbě	novostavba	k1gFnSc6	novostavba
gymnázia	gymnázium	k1gNnSc2	gymnázium
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1933	[number]	k4	1933
byly	být	k5eAaImAgInP	být
definitivně	definitivně	k6eAd1	definitivně
odstraněny	odstranit	k5eAaPmNgInP	odstranit
dřevěné	dřevěný	k2eAgInPc1d1	dřevěný
domky	domek	k1gInPc1	domek
(	(	kIx(	(
<g/>
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
dnešního	dnešní	k2eAgInSc2d1	dnešní
"	"	kIx"	"
<g/>
mrakodrapu	mrakodrap	k1gInSc2	mrakodrap
<g/>
"	"	kIx"	"
bývalého	bývalý	k2eAgNnSc2d1	bývalé
železničního	železniční	k2eAgNnSc2d1	železniční
učiliště	učiliště	k1gNnSc2	učiliště
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Celý	celý	k2eAgInSc1d1	celý
komplex	komplex	k1gInSc1	komplex
zahradního	zahradní	k2eAgNnSc2d1	zahradní
města	město	k1gNnSc2	město
přešel	přejít	k5eAaPmAgInS	přejít
pod	pod	k7c4	pod
správu	správa	k1gFnSc4	správa
místního	místní	k2eAgInSc2d1	místní
národního	národní	k2eAgInSc2d1	národní
výboru	výbor	k1gInSc2	výbor
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
byly	být	k5eAaImAgFnP	být
zbourány	zbourat	k5eAaPmNgFnP	zbourat
už	už	k6eAd1	už
zhruba	zhruba	k6eAd1	zhruba
dvě	dva	k4xCgFnPc1	dva
třetiny	třetina	k1gFnPc1	třetina
ojedinělého	ojedinělý	k2eAgInSc2d1	ojedinělý
urbanistického	urbanistický	k2eAgInSc2d1	urbanistický
pokusu	pokus	k1gInSc2	pokus
<g/>
.	.	kIx.	.
</s>
<s>
Zůstalo	zůstat	k5eAaPmAgNnS	zůstat
10	[number]	k4	10
"	"	kIx"	"
<g/>
čtyřdomků	čtyřdomek	k1gInPc2	čtyřdomek
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
si	se	k3xPyFc3	se
jejich	jejich	k3xOp3gMnPc1	jejich
majitelé	majitel	k1gMnPc1	majitel
s	s	k7c7	s
větším	veliký	k2eAgInSc7d2	veliký
či	či	k8xC	či
menším	malý	k2eAgInSc7d2	menší
citem	cit	k1gInSc7	cit
opravují	opravovat	k5eAaImIp3nP	opravovat
<g/>
.	.	kIx.	.
</s>
<s>
Zachováno	zachován	k2eAgNnSc1d1	zachováno
a	a	k8xC	a
opraveno	opraven	k2eAgNnSc1d1	opraveno
bylo	být	k5eAaImAgNnS	být
pět	pět	k4xCc4	pět
domů	dům	k1gInPc2	dům
původně	původně	k6eAd1	původně
určených	určený	k2eAgFnPc2d1	určená
pro	pro	k7c4	pro
vyšší	vysoký	k2eAgMnPc4d2	vyšší
drážní	drážní	k2eAgMnPc4d1	drážní
úředníky	úředník	k1gMnPc4	úředník
<g/>
.	.	kIx.	.
</s>
<s>
Připomínkou	připomínka	k1gFnSc7	připomínka
staré	starý	k2eAgFnSc2d1	stará
krásy	krása	k1gFnSc2	krása
je	být	k5eAaImIp3nS	být
opravená	opravený	k2eAgFnSc1d1	opravená
budova	budova	k1gFnSc1	budova
prádelny	prádelna	k1gFnSc2	prádelna
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgFnSc1d1	poslední
původní	původní	k2eAgFnSc1d1	původní
budova	budova	k1gFnSc1	budova
v	v	k7c6	v
Nádražní	nádražní	k2eAgFnSc6d1	nádražní
ulici	ulice	k1gFnSc6	ulice
dostala	dostat	k5eAaPmAgFnS	dostat
novou	nový	k2eAgFnSc4d1	nová
střechu	střecha	k1gFnSc4	střecha
a	a	k8xC	a
fasádu	fasáda	k1gFnSc4	fasáda
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2017	[number]	k4	2017
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
další	další	k2eAgFnSc1d1	další
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
<g/>
,	,	kIx,	,
budova	budova	k1gFnSc1	budova
konečně	konečně	k6eAd1	konečně
našla	najít	k5eAaPmAgFnS	najít
nové	nový	k2eAgNnSc4d1	nové
uplatnění	uplatnění	k1gNnSc4	uplatnění
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
dialyzační	dialyzační	k2eAgNnSc1d1	dialyzační
středisko	středisko	k1gNnSc1	středisko
společnosti	společnost	k1gFnSc2	společnost
Fresenius	Fresenius	k1gInSc1	Fresenius
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Významné	významný	k2eAgFnPc1d1	významná
kulturní	kulturní	k2eAgFnPc1d1	kulturní
památky	památka	k1gFnPc1	památka
===	===	k?	===
</s>
</p>
<p>
<s>
Dominantou	dominanta	k1gFnSc7	dominanta
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
gotický	gotický	k2eAgInSc4d1	gotický
cihlový	cihlový	k2eAgInSc4d1	cihlový
kostel	kostel	k1gInSc4	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Jiljí	Jiljí	k1gMnSc2	Jiljí
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
měl	mít	k5eAaImAgMnS	mít
dvě	dva	k4xCgFnPc4	dva
věže	věž	k1gFnPc4	věž
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
však	však	k9	však
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
požáru	požár	k1gInSc6	požár
stržena	stržen	k2eAgFnSc1d1	stržena
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
má	mít	k5eAaImIp3nS	mít
zbývající	zbývající	k2eAgFnSc1d1	zbývající
věž	věž	k1gFnSc1	věž
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
pozici	pozice	k1gFnSc4	pozice
mimo	mimo	k7c4	mimo
osu	osa	k1gFnSc4	osa
chrámu	chrám	k1gInSc2	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
kostel	kostel	k1gInSc1	kostel
je	být	k5eAaImIp3nS	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dochovanými	dochovaný	k2eAgFnPc7d1	dochovaná
stavbami	stavba	k1gFnPc7	stavba
nymburského	nymburský	k2eAgInSc2d1	nymburský
opevnění	opevnění	k1gNnSc1	opevnění
ojedinělým	ojedinělý	k2eAgInSc7d1	ojedinělý
příkladem	příklad	k1gInSc7	příklad
cihlové	cihlový	k2eAgFnSc2d1	cihlová
gotiky	gotika	k1gFnSc2	gotika
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
severoněmecké	severoněmecký	k2eAgFnPc4d1	Severoněmecká
<g/>
)	)	kIx)	)
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Dominantou	dominanta	k1gFnSc7	dominanta
náměstí	náměstí	k1gNnSc2	náměstí
je	být	k5eAaImIp3nS	být
vzácná	vzácný	k2eAgFnSc1d1	vzácná
renesanční	renesanční	k2eAgFnSc1d1	renesanční
radnice	radnice	k1gFnSc1	radnice
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejzajímavějších	zajímavý	k2eAgFnPc2d3	nejzajímavější
novodobých	novodobý	k2eAgFnPc2d1	novodobá
staveb	stavba	k1gFnPc2	stavba
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
architektonicky	architektonicky	k6eAd1	architektonicky
významné	významný	k2eAgNnSc4d1	významné
puristické	puristický	k2eAgNnSc4d1	puristické
krematorium	krematorium	k1gNnSc4	krematorium
od	od	k7c2	od
Bedřicha	Bedřich	k1gMnSc2	Bedřich
Feuersteina	Feuerstein	k1gMnSc2	Feuerstein
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
hradeb	hradba	k1gFnPc2	hradba
městu	město	k1gNnSc3	město
také	také	k9	také
vévodí	vévodit	k5eAaImIp3nP	vévodit
silniční	silniční	k2eAgInSc4d1	silniční
most	most	k1gInSc4	most
(	(	kIx(	(
<g/>
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
spojuje	spojovat	k5eAaImIp3nS	spojovat
centrum	centrum	k1gNnSc1	centrum
města	město	k1gNnSc2	město
s	s	k7c7	s
částí	část	k1gFnSc7	část
zvanou	zvaný	k2eAgFnSc4d1	zvaná
Zálabí	zálabí	k1gNnSc4	zálabí
<g/>
.	.	kIx.	.
</s>
<s>
Most	most	k1gInSc1	most
přes	přes	k7c4	přes
Labe	Labe	k1gNnSc4	Labe
byl	být	k5eAaImAgMnS	být
postavený	postavený	k2eAgMnSc1d1	postavený
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1912	[number]	k4	1912
až	až	k9	až
1913	[number]	k4	1913
architektem	architekt	k1gMnSc7	architekt
Františkem	František	k1gMnSc7	František
Roithem	Roith	k1gInSc7	Roith
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
ho	on	k3xPp3gInSc4	on
tři	tři	k4xCgInPc1	tři
železobetonové	železobetonový	k2eAgInPc1d1	železobetonový
oblouky	oblouk	k1gInPc1	oblouk
o	o	k7c6	o
rozponech	rozpon	k1gInPc6	rozpon
35	[number]	k4	35
<g/>
,	,	kIx,	,
40	[number]	k4	40
a	a	k8xC	a
35	[number]	k4	35
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
spočívají	spočívat	k5eAaImIp3nP	spočívat
na	na	k7c6	na
betonových	betonový	k2eAgInPc6d1	betonový
pilířích	pilíř	k1gInPc6	pilíř
<g/>
.	.	kIx.	.
</s>
<s>
Žulový	žulový	k2eAgInSc1d1	žulový
obklad	obklad	k1gInSc1	obklad
pilířů	pilíř	k1gInPc2	pilíř
je	být	k5eAaImIp3nS	být
ozdoben	ozdoben	k2eAgInSc4d1	ozdoben
štíty	štít	k1gInPc4	štít
s	s	k7c7	s
motivem	motiv	k1gInSc7	motiv
českých	český	k2eAgMnPc2d1	český
lvů	lev	k1gMnPc2	lev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Industriální	industriální	k2eAgFnPc1d1	industriální
památky	památka	k1gFnPc1	památka
===	===	k?	===
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
nachází	nacházet	k5eAaImIp3nS	nacházet
řada	řada	k1gFnSc1	řada
cenných	cenný	k2eAgInPc2d1	cenný
industriálních	industriální	k2eAgInPc2d1	industriální
a	a	k8xC	a
technických	technický	k2eAgFnPc2d1	technická
památek	památka	k1gFnPc2	památka
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
však	však	k9	však
ještě	ještě	k6eAd1	ještě
stále	stále	k6eAd1	stále
nejsou	být	k5eNaImIp3nP	být
vhodně	vhodně	k6eAd1	vhodně
využity	využít	k5eAaPmNgInP	využít
a	a	k8xC	a
rozvíjeny	rozvíjet	k5eAaImNgInP	rozvíjet
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
skýtají	skýtat	k5eAaImIp3nP	skýtat
značný	značný	k2eAgInSc4d1	značný
potenciál	potenciál	k1gInSc4	potenciál
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k6eAd1	především
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
zbořen	zbořen	k2eAgInSc1d1	zbořen
proslulý	proslulý	k2eAgInSc1d1	proslulý
cukrovar	cukrovar	k1gInSc1	cukrovar
na	na	k7c6	na
východě	východ	k1gInSc6	východ
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
dvěma	dva	k4xCgFnPc7	dva
těsně	těsně	k6eAd1	těsně
vedle	vedle	k6eAd1	vedle
sebe	sebe	k3xPyFc4	sebe
umístěnými	umístěný	k2eAgInPc7d1	umístěný
komíny	komín	k1gInPc7	komín
<g/>
.	.	kIx.	.
</s>
<s>
Cukrovar	cukrovar	k1gInSc1	cukrovar
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1870	[number]	k4	1870
místními	místní	k2eAgMnPc7d1	místní
rolníky	rolník	k1gMnPc7	rolník
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1882	[number]	k4	1882
stal	stát	k5eAaPmAgInS	stát
prvním	první	k4xOgInSc7	první
elektrifikovaným	elektrifikovaný	k2eAgInSc7d1	elektrifikovaný
cukrovarem	cukrovar	k1gInSc7	cukrovar
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Podnik	podnik	k1gInSc1	podnik
byl	být	k5eAaImAgInS	být
nerozlučně	rozlučně	k6eNd1	rozlučně
spjat	spjat	k2eAgInSc1d1	spjat
s	s	k7c7	s
působením	působení	k1gNnSc7	působení
vynikajícího	vynikající	k2eAgMnSc2d1	vynikající
cukrovarníka	cukrovarník	k1gMnSc2	cukrovarník
<g/>
,	,	kIx,	,
světově	světově	k6eAd1	světově
proslulého	proslulý	k2eAgMnSc2d1	proslulý
Hanuše	Hanuš	k1gMnSc2	Hanuš
Karlíka	Karlík	k1gMnSc2	Karlík
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
zde	zde	k6eAd1	zde
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
vyzkoušel	vyzkoušet	k5eAaPmAgInS	vyzkoušet
svou	svůj	k3xOyFgFnSc4	svůj
převratnou	převratný	k2eAgFnSc4d1	převratná
metodu	metoda	k1gFnSc4	metoda
výroby	výroba	k1gFnSc2	výroba
cukru	cukr	k1gInSc2	cukr
formou	forma	k1gFnSc7	forma
trojnásobné	trojnásobný	k2eAgFnSc2d1	trojnásobná
saturace	saturace	k1gFnSc2	saturace
bez	bez	k7c2	bez
spodia	spodium	k1gNnSc2	spodium
<g/>
.	.	kIx.	.
</s>
<s>
Cukrovar	cukrovar	k1gInSc1	cukrovar
patřil	patřit	k5eAaImAgInS	patřit
mezi	mezi	k7c4	mezi
největší	veliký	k2eAgNnSc4d3	veliký
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
<g/>
Mezi	mezi	k7c4	mezi
nejhodnotnější	hodnotný	k2eAgFnPc4d3	nejhodnotnější
dochované	dochovaný	k2eAgFnPc4d1	dochovaná
industriální	industriální	k2eAgFnPc4d1	industriální
památky	památka	k1gFnPc4	památka
v	v	k7c6	v
Nymburce	Nymburk	k1gInSc6	Nymburk
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Nymburský	nymburský	k2eAgInSc1d1	nymburský
pivovar	pivovar	k1gInSc1	pivovar
<g/>
,	,	kIx,	,
postavený	postavený	k2eAgInSc1d1	postavený
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1895	[number]	k4	1895
až	až	k9	až
1898	[number]	k4	1898
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
v	v	k7c6	v
první	první	k4xOgFnSc6	první
roce	rok	k1gInSc6	rok
provozu	provoz	k1gInSc2	provoz
vyprodukoval	vyprodukovat	k5eAaPmAgInS	vyprodukovat
21	[number]	k4	21
075	[number]	k4	075
hektolitrů	hektolitr	k1gInPc2	hektolitr
piva	pivo	k1gNnSc2	pivo
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1927	[number]	k4	1927
přistoupila	přistoupit	k5eAaPmAgFnS	přistoupit
správní	správní	k2eAgFnSc1d1	správní
rada	rada	k1gFnSc1	rada
pivovaru	pivovar	k1gInSc2	pivovar
k	k	k7c3	k
renovaci	renovace	k1gFnSc3	renovace
strojů	stroj	k1gInPc2	stroj
a	a	k8xC	a
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
funkční	funkční	k2eAgNnSc1d1	funkční
a	a	k8xC	a
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
dobrém	dobrý	k2eAgInSc6d1	dobrý
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
<g/>
Ludvík	Ludvík	k1gMnSc1	Ludvík
Wantoch	Wantoch	k1gMnSc1	Wantoch
<g/>
,	,	kIx,	,
továrna	továrna	k1gFnSc1	továrna
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
likérů	likér	k1gInPc2	likér
<g/>
,	,	kIx,	,
postavena	postaven	k2eAgFnSc1d1	postavena
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1910	[number]	k4	1910
až	až	k9	až
1911	[number]	k4	1911
ve	v	k7c6	v
Zbožské	Zbožský	k2eAgFnSc6d1	Zbožská
ulici	ulice	k1gFnSc6	ulice
u	u	k7c2	u
železnici	železnice	k1gFnSc3	železnice
na	na	k7c6	na
západě	západ	k1gInSc6	západ
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
dostavba	dostavba	k1gFnSc1	dostavba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
</s>
<s>
Výroba	výroba	k1gFnSc1	výroba
lihovin	lihovina	k1gFnPc2	lihovina
končila	končit	k5eAaImAgFnS	končit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
je	být	k5eAaImIp3nS	být
zachovalý	zachovalý	k2eAgInSc1d1	zachovalý
areál	areál	k1gInSc1	areál
s	s	k7c7	s
dominantou	dominanta	k1gFnSc7	dominanta
25	[number]	k4	25
metrů	metr	k1gInPc2	metr
vysokého	vysoký	k2eAgInSc2d1	vysoký
cihlového	cihlový	k2eAgInSc2d1	cihlový
komína	komín	k1gInSc2	komín
nevyužitý	využitý	k2eNgMnSc1d1	nevyužitý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obilní	obilní	k2eAgNnSc1d1	obilní
skladiště	skladiště	k1gNnSc1	skladiště
a	a	k8xC	a
mlýn	mlýn	k1gInSc1	mlýn
Hospodářského	hospodářský	k2eAgNnSc2d1	hospodářské
družstva	družstvo	k1gNnSc2	družstvo
ve	v	k7c6	v
Zbožské	Zbožský	k2eAgFnSc6d1	Zbožská
ulici	ulice	k1gFnSc6	ulice
<g/>
,	,	kIx,	,
založené	založený	k2eAgInPc4d1	založený
roku	rok	k1gInSc2	rok
v	v	k7c6	v
letech	let	k1gInPc6	let
1899	[number]	k4	1899
<g/>
–	–	k?	–
<g/>
1904	[number]	k4	1904
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1924	[number]	k4	1924
<g/>
–	–	k?	–
<g/>
1927	[number]	k4	1927
byl	být	k5eAaImAgMnS	být
k	k	k7c3	k
netradičně	tradičně	k6eNd1	tradičně
řešenému	řešený	k2eAgInSc3d1	řešený
areálu	areál	k1gInSc3	areál
přistavěn	přistavěn	k2eAgInSc1d1	přistavěn
mlýn	mlýn	k1gInSc1	mlýn
s	s	k7c7	s
kapacitou	kapacita	k1gFnSc7	kapacita
100	[number]	k4	100
tun	tuna	k1gFnPc2	tuna
obilí	obilí	k1gNnSc2	obilí
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
družstevní	družstevní	k2eAgFnSc1d1	družstevní
mlékárna	mlékárna	k1gFnSc1	mlékárna
v	v	k7c6	v
Nymburce	Nymburk	k1gInSc6	Nymburk
<g/>
,	,	kIx,	,
postavená	postavený	k2eAgFnSc1d1	postavená
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1934	[number]	k4	1934
až	až	k9	až
1935	[number]	k4	1935
v	v	k7c6	v
Jičínské	jičínský	k2eAgFnSc6d1	Jičínská
ulici	ulice	k1gFnSc6	ulice
u	u	k7c2	u
železnice	železnice	k1gFnSc2	železnice
na	na	k7c6	na
západě	západ	k1gInSc6	západ
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Mlékárna	mlékárna	k1gFnSc1	mlékárna
produkovala	produkovat	k5eAaImAgFnS	produkovat
pasterizované	pasterizovaný	k2eAgNnSc4d1	pasterizované
mléko	mléko	k1gNnSc4	mléko
v	v	k7c6	v
lahvích	lahev	k1gFnPc6	lahev
<g/>
,	,	kIx,	,
jogurty	jogurt	k1gInPc4	jogurt
<g/>
,	,	kIx,	,
sýry	sýr	k1gInPc4	sýr
<g/>
,	,	kIx,	,
máslo	máslo	k1gNnSc4	máslo
a	a	k8xC	a
šlehačku	šlehačka	k1gFnSc4	šlehačka
a	a	k8xC	a
dodávala	dodávat	k5eAaImAgFnS	dodávat
denně	denně	k6eAd1	denně
5	[number]	k4	5
300	[number]	k4	300
litrů	litr	k1gInPc2	litr
mléka	mléko	k1gNnSc2	mléko
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
2	[number]	k4	2
000	[number]	k4	000
litrů	litr	k1gInPc2	litr
bylo	být	k5eAaImAgNnS	být
strojně	strojně	k6eAd1	strojně
zpracováno	zpracovat	k5eAaPmNgNnS	zpracovat
na	na	k7c4	na
máslo	máslo	k1gNnSc4	máslo
<g/>
,	,	kIx,	,
tvaroh	tvaroh	k1gInSc4	tvaroh
a	a	k8xC	a
sýry	sýr	k1gInPc4	sýr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
již	již	k9	již
byla	být	k5eAaImAgFnS	být
mlékárna	mlékárna	k1gFnSc1	mlékárna
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
v	v	k7c6	v
republice	republika	k1gFnSc6	republika
a	a	k8xC	a
produkovala	produkovat	k5eAaImAgFnS	produkovat
30	[number]	k4	30
000	[number]	k4	000
litrů	litr	k1gInPc2	litr
mléka	mléko	k1gNnSc2	mléko
denně	denně	k6eAd1	denně
a	a	k8xC	a
500	[number]	k4	500
kilogramů	kilogram	k1gInPc2	kilogram
másla	máslo	k1gNnSc2	máslo
a	a	k8xC	a
sýrů	sýr	k1gInPc2	sýr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
byla	být	k5eAaImAgFnS	být
přestavěna	přestavět	k5eAaPmNgFnS	přestavět
na	na	k7c4	na
tavírnu	tavírna	k1gFnSc4	tavírna
sýrů	sýr	k1gInPc2	sýr
a	a	k8xC	a
takto	takto	k6eAd1	takto
fungovala	fungovat	k5eAaImAgFnS	fungovat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
objekt	objekt	k1gInSc1	objekt
využívá	využívat	k5eAaImIp3nS	využívat
ke	k	k7c3	k
skladování	skladování	k1gNnSc3	skladování
<g/>
.	.	kIx.	.
</s>
<s>
Urbanisticky	urbanisticky	k6eAd1	urbanisticky
jedinečně	jedinečně	k6eAd1	jedinečně
uspořádaný	uspořádaný	k2eAgInSc1d1	uspořádaný
areál	areál	k1gInSc1	areál
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
dvěma	dva	k4xCgFnPc7	dva
staršími	starý	k2eAgFnPc7d2	starší
budovami	budova	k1gFnPc7	budova
s	s	k7c7	s
valbovou	valbový	k2eAgFnSc7d1	valbová
a	a	k8xC	a
jehlancovou	jehlancový	k2eAgFnSc7d1	jehlancová
mansardovou	mansardový	k2eAgFnSc7d1	mansardová
střechou	střecha	k1gFnSc7	střecha
<g/>
,	,	kIx,	,
doplněnými	doplněný	k2eAgInPc7d1	doplněný
o	o	k7c4	o
pozdější	pozdní	k2eAgFnSc4d2	pozdější
modernistickou	modernistický	k2eAgFnSc4d1	modernistická
dostavbu	dostavba	k1gFnSc4	dostavba
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
ulici	ulice	k1gFnSc3	ulice
<g/>
.	.	kIx.	.
<g/>
Josef	Josef	k1gMnSc1	Josef
Tekl	téct	k5eAaImAgMnS	téct
<g/>
,	,	kIx,	,
korkové	korkový	k2eAgFnPc4d1	korková
isolace	isolace	k1gFnPc4	isolace
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Dvorské	Dvorské	k2eAgFnSc6d1	Dvorské
ulici	ulice	k1gFnSc6	ulice
na	na	k7c6	na
západě	západ	k1gInSc6	západ
města	město	k1gNnSc2	město
při	při	k7c6	při
železnici	železnice	k1gFnSc6	železnice
<g/>
.	.	kIx.	.
</s>
<s>
Továrna	továrna	k1gFnSc1	továrna
postavena	postaven	k2eAgFnSc1d1	postavena
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1914	[number]	k4	1914
majitelem	majitel	k1gMnSc7	majitel
válcového	válcový	k2eAgInSc2d1	válcový
mlýna	mlýn	k1gInSc2	mlýn
Josefem	Josef	k1gMnSc7	Josef
Teklem	Tekl	k1gMnSc7	Tekl
<g/>
,	,	kIx,	,
vyráběl	vyrábět	k5eAaImAgInS	vyrábět
zde	zde	k6eAd1	zde
korkové	korkový	k2eAgFnSc2d1	korková
izolace	izolace	k1gFnSc2	izolace
a	a	k8xC	a
korkové	korkový	k2eAgFnSc2d1	korková
zátky	zátka	k1gFnSc2	zátka
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
v	v	k7c6	v
částečně	částečně	k6eAd1	částečně
zchátralé	zchátralý	k2eAgFnSc6d1	zchátralá
budově	budova	k1gFnSc6	budova
působí	působit	k5eAaImIp3nS	působit
pneuservis	pneuservis	k1gInSc4	pneuservis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Železniční	železniční	k2eAgFnPc1d1	železniční
dílny	dílna	k1gFnPc1	dílna
Severozápadní	severozápadní	k2eAgFnSc2d1	severozápadní
dráhy	dráha	k1gFnSc2	dráha
na	na	k7c6	na
severním	severní	k2eAgInSc6d1	severní
okraji	okraj	k1gInSc6	okraj
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Plánovány	plánován	k2eAgFnPc1d1	plánována
byly	být	k5eAaImAgFnP	být
již	již	k9	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1867	[number]	k4	1867
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1872	[number]	k4	1872
až	až	k9	až
1874	[number]	k4	1874
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
první	první	k4xOgFnPc1	první
dílny	dílna	k1gFnPc1	dílna
a	a	k8xC	a
nové	nový	k2eAgFnPc1d1	nová
budovy	budova	k1gFnPc1	budova
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
unikátním	unikátní	k2eAgInSc6d1	unikátní
areálu	areál	k1gInSc6	areál
v	v	k7c6	v
několika	několik	k4yIc6	několik
etapách	etapa	k1gFnPc6	etapa
přistavovány	přistavován	k2eAgFnPc4d1	přistavována
až	až	k9	až
do	do	k7c2	do
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Dominantou	dominanta	k1gFnSc7	dominanta
rozlehlého	rozlehlý	k2eAgInSc2d1	rozlehlý
areálu	areál	k1gInSc2	areál
je	být	k5eAaImIp3nS	být
několik	několik	k4yIc1	několik
komínů	komín	k1gInPc2	komín
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
140	[number]	k4	140
metrů	metr	k1gInPc2	metr
vysokého	vysoký	k2eAgNnSc2d1	vysoké
betonového	betonový	k2eAgNnSc2d1	betonové
a	a	k8xC	a
72	[number]	k4	72
metrů	metr	k1gInPc2	metr
vysokého	vysoký	k2eAgInSc2d1	vysoký
cihlového	cihlový	k2eAgInSc2d1	cihlový
<g/>
.	.	kIx.	.
</s>
<s>
Železniční	železniční	k2eAgFnSc2d1	železniční
opravny	opravna	k1gFnSc2	opravna
a	a	k8xC	a
strojírny	strojírna	k1gFnSc2	strojírna
Nymburk	Nymburk	k1gInSc1	Nymburk
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
provádějí	provádět	k5eAaImIp3nP	provádět
kompletní	kompletní	k2eAgInPc1d1	kompletní
opravy	oprava	k1gFnPc4	oprava
a	a	k8xC	a
rekonstrukce	rekonstrukce	k1gFnPc4	rekonstrukce
železničních	železniční	k2eAgNnPc2d1	železniční
kolejových	kolejový	k2eAgNnPc2d1	kolejové
vozidel	vozidlo	k1gNnPc2	vozidlo
<g/>
.	.	kIx.	.
<g/>
Parní	parní	k2eAgFnSc1d1	parní
vodárna	vodárna	k1gFnSc1	vodárna
s	s	k7c7	s
cihlovým	cihlový	k2eAgInSc7d1	cihlový
komínem	komín	k1gInSc7	komín
mezi	mezi	k7c7	mezi
železnicí	železnice	k1gFnSc7	železnice
a	a	k8xC	a
ulicí	ulice	k1gFnSc7	ulice
P.	P.	kA	P.
Bezruče	Bezruče	k1gNnSc4	Bezruče
na	na	k7c6	na
severu	sever	k1gInSc6	sever
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
roku	rok	k1gInSc2	rok
1872	[number]	k4	1872
<g/>
.	.	kIx.	.
<g/>
železobetonový	železobetonový	k2eAgInSc1d1	železobetonový
silniční	silniční	k2eAgInSc1d1	silniční
most	most	k1gInSc1	most
přes	přes	k7c4	přes
Labe	Labe	k1gNnSc4	Labe
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1912	[number]	k4	1912
</s>
</p>
<p>
<s>
Železniční	železniční	k2eAgInSc1d1	železniční
most	most	k1gInSc1	most
přes	přes	k7c4	přes
Labe	Labe	k1gNnSc4	Labe
<g/>
,	,	kIx,	,
postaven	postavit	k5eAaPmNgInS	postavit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1882	[number]	k4	1882
<g/>
.	.	kIx.	.
</s>
<s>
Jednokolejný	jednokolejný	k2eAgInSc4d1	jednokolejný
most	most	k1gInSc4	most
o	o	k7c6	o
čtyřech	čtyři	k4xCgNnPc6	čtyři
polích	pole	k1gNnPc6	pole
s	s	k7c7	s
rozpětím	rozpětí	k1gNnSc7	rozpětí
42	[number]	k4	42
metrů	metr	k1gInPc2	metr
tvořila	tvořit	k5eAaImAgFnS	tvořit
nýtovaná	nýtovaný	k2eAgFnSc1d1	nýtovaná
příhradová	příhradový	k2eAgFnSc1d1	příhradová
konstrukce	konstrukce	k1gFnSc1	konstrukce
<g/>
,	,	kIx,	,
návodní	návodní	k2eAgInPc1d1	návodní
pilíře	pilíř	k1gInPc1	pilíř
byly	být	k5eAaImAgInP	být
obloženy	obložen	k2eAgInPc1d1	obložen
žulovými	žulový	k2eAgInPc7d1	žulový
kvádry	kvádr	k1gInPc7	kvádr
<g/>
,	,	kIx,	,
pobřežní	pobřežní	k2eAgInPc1d1	pobřežní
pískovcovými	pískovcový	k2eAgInPc7d1	pískovcový
<g/>
.	.	kIx.	.
</s>
<s>
Mostní	mostní	k2eAgFnSc1d1	mostní
konstrukce	konstrukce	k1gFnSc1	konstrukce
byla	být	k5eAaImAgFnS	být
vyměněna	vyměnit	k5eAaPmNgFnS	vyměnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zdymadlo	zdymadlo	k1gNnSc1	zdymadlo
a	a	k8xC	a
plavební	plavební	k2eAgFnSc1d1	plavební
komora	komora	k1gFnSc1	komora
byly	být	k5eAaImAgFnP	být
postaveny	postavit	k5eAaPmNgFnP	postavit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1914	[number]	k4	1914
až	až	k9	až
1922	[number]	k4	1922
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
byla	být	k5eAaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
též	též	k9	též
hydroelektrárna	hydroelektrárna	k1gFnSc1	hydroelektrárna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
elektrárně	elektrárna	k1gFnSc6	elektrárna
jsou	být	k5eAaImIp3nP	být
umístěny	umístit	k5eAaPmNgFnP	umístit
čtyři	čtyři	k4xCgFnPc1	čtyři
Francisovy	Francisův	k2eAgFnPc1d1	Francisova
turbíny	turbína	k1gFnPc1	turbína
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
Kaplanova	Kaplanův	k2eAgFnSc1d1	Kaplanova
turbína	turbína	k1gFnSc1	turbína
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
stále	stále	k6eAd1	stále
funkční	funkční	k2eAgFnPc1d1	funkční
<g/>
.	.	kIx.	.
</s>
<s>
Zdymadlo	zdymadlo	k1gNnSc1	zdymadlo
<g/>
,	,	kIx,	,
také	také	k9	také
architektonicky	architektonicky	k6eAd1	architektonicky
řešené	řešený	k2eAgFnSc2d1	řešená
architektem	architekt	k1gMnSc7	architekt
Františkem	František	k1gMnSc7	František
Roithem	Roith	k1gInSc7	Roith
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgNnPc4	tři
pole	pole	k1gNnPc4	pole
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Městská	městský	k2eAgFnSc1d1	městská
vodárna	vodárna	k1gFnSc1	vodárna
a	a	k8xC	a
vodárenská	vodárenský	k2eAgFnSc1d1	vodárenská
věž	věž	k1gFnSc1	věž
<g/>
,	,	kIx,	,
postavená	postavený	k2eAgFnSc1d1	postavená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1903	[number]	k4	1903
v	v	k7c6	v
Jízdecké	jízdecký	k2eAgFnSc6d1	Jízdecká
ulici	ulice	k1gFnSc6	ulice
na	na	k7c6	na
severu	sever	k1gInSc6	sever
historického	historický	k2eAgNnSc2d1	historické
jádra	jádro	k1gNnSc2	jádro
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Stavbu	stavba	k1gFnSc4	stavba
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
realizace	realizace	k1gFnSc2	realizace
nového	nový	k2eAgInSc2d1	nový
městského	městský	k2eAgInSc2d1	městský
vodovodu	vodovod	k1gInSc2	vodovod
projektoval	projektovat	k5eAaBmAgMnS	projektovat
slavný	slavný	k2eAgMnSc1d1	slavný
architekt	architekt	k1gMnSc1	architekt
Osvald	Osvald	k1gMnSc1	Osvald
Polívka	Polívka	k1gMnSc1	Polívka
a	a	k8xC	a
proslulý	proslulý	k2eAgMnSc1d1	proslulý
inženýr	inženýr	k1gMnSc1	inženýr
Vladimír	Vladimír	k1gMnSc1	Vladimír
Hráský	Hráský	k1gMnSc1	Hráský
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
technicky	technicky	k6eAd1	technicky
vybavena	vybavit	k5eAaPmNgFnS	vybavit
firmami	firma	k1gFnPc7	firma
Škoda	škoda	k1gFnSc1	škoda
Plzeň	Plzeň	k1gFnSc1	Plzeň
a	a	k8xC	a
Breitfeld	Breitfeld	k1gMnSc1	Breitfeld
<g/>
,	,	kIx,	,
Daněk	Daněk	k1gMnSc1	Daněk
a	a	k8xC	a
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
vyzdobena	vyzdoben	k2eAgFnSc1d1	vyzdobena
štukováním	štukování	k1gNnSc7	štukování
<g/>
,	,	kIx,	,
bosáží	bosáž	k1gFnSc7	bosáž
a	a	k8xC	a
chrliči	chrlič	k1gMnPc7	chrlič
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
démonů	démon	k1gMnPc2	démon
<g/>
.	.	kIx.	.
</s>
<s>
Věž	věž	k1gFnSc1	věž
o	o	k7c6	o
výšce	výška	k1gFnSc6	výška
37	[number]	k4	37
metrů	metr	k1gInPc2	metr
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
dominant	dominanta	k1gFnPc2	dominanta
města	město	k1gNnSc2	město
a	a	k8xC	a
slouží	sloužit	k5eAaImIp3nS	sloužit
stále	stále	k6eAd1	stále
svému	svůj	k3xOyFgInSc3	svůj
účelu	účel	k1gInSc3	účel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sport	sport	k1gInSc1	sport
==	==	k?	==
</s>
</p>
<p>
<s>
Nejvýznamnějším	významný	k2eAgInSc7d3	nejvýznamnější
sportem	sport	k1gInSc7	sport
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
městě	město	k1gNnSc6	město
jednoznačně	jednoznačně	k6eAd1	jednoznačně
basketbal	basketbal	k1gInSc4	basketbal
<g/>
,	,	kIx,	,
mužský	mužský	k2eAgInSc4d1	mužský
tým	tým	k1gInSc4	tým
ČEZ	ČEZ	kA	ČEZ
Basketball	Basketball	k1gInSc1	Basketball
Nymburk	Nymburk	k1gInSc1	Nymburk
je	být	k5eAaImIp3nS	být
aktuálně	aktuálně	k6eAd1	aktuálně
mistrem	mistr	k1gMnSc7	mistr
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dalších	další	k2eAgInPc2d1	další
sportů	sport	k1gInPc2	sport
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
zastoupen	zastoupit	k5eAaPmNgMnS	zastoupit
na	na	k7c6	na
prvoligové	prvoligový	k2eAgFnSc6d1	prvoligová
úrovni	úroveň	k1gFnSc6	úroveň
také	také	k9	také
ženský	ženský	k2eAgInSc4d1	ženský
fotbal	fotbal	k1gInSc4	fotbal
<g/>
,	,	kIx,	,
muži	muž	k1gMnPc1	muž
hrají	hrát	k5eAaImIp3nP	hrát
krajský	krajský	k2eAgInSc4d1	krajský
přebor	přebor	k1gInSc4	přebor
<g/>
.	.	kIx.	.
</s>
<s>
Hokejisté	hokejista	k1gMnPc1	hokejista
Nymburka	Nymburk	k1gInSc2	Nymburk
hrají	hrát	k5eAaImIp3nP	hrát
2	[number]	k4	2
<g/>
.	.	kIx.	.
ligu	liga	k1gFnSc4	liga
<g/>
.	.	kIx.	.
</s>
<s>
Skvělou	skvělý	k2eAgFnSc4d1	skvělá
úroveň	úroveň	k1gFnSc4	úroveň
mají	mít	k5eAaImIp3nP	mít
i	i	k9	i
rychlostní	rychlostní	k2eAgMnPc1d1	rychlostní
kanoisté	kanoista	k1gMnPc1	kanoista
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
mistrem	mistr	k1gMnSc7	mistr
světa	svět	k1gInSc2	svět
a	a	k8xC	a
olympionikem	olympionik	k1gMnSc7	olympionik
Petrem	Petr	k1gMnSc7	Petr
Fuksou	Fuksa	k1gMnSc7	Fuksa
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tamního	tamní	k2eAgInSc2d1	tamní
oddílu	oddíl	k1gInSc2	oddíl
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
Labe	Labe	k1gNnSc2	Labe
pochází	pocházet	k5eAaImIp3nS	pocházet
nejeden	nejeden	k4xCyIgMnSc1	nejeden
úspěšný	úspěšný	k2eAgMnSc1d1	úspěšný
reprezentant	reprezentant	k1gMnSc1	reprezentant
<g/>
,	,	kIx,	,
mimo	mimo	k6eAd1	mimo
jiných	jiný	k2eAgNnPc2d1	jiné
i	i	k8xC	i
další	další	k2eAgMnSc1d1	další
světový	světový	k2eAgMnSc1d1	světový
medailista	medailista	k1gMnSc1	medailista
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
také	také	k9	také
německý	německý	k2eAgMnSc1d1	německý
reprezentant	reprezentant	k1gMnSc1	reprezentant
v	v	k7c6	v
kanoistickém	kanoistický	k2eAgInSc6d1	kanoistický
maratonu	maraton	k1gInSc6	maraton
Petr	Petr	k1gMnSc1	Petr
Kubíček	Kubíček	k1gMnSc1	Kubíček
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
odehrál	odehrát	k5eAaPmAgMnS	odehrát
mezinárodní	mezinárodní	k2eAgMnSc1d1	mezinárodní
rink	rink	k1gMnSc1	rink
bandy	banda	k1gFnSc2	banda
turnaj	turnaj	k1gInSc1	turnaj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kultura	kultura	k1gFnSc1	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
působí	působit	k5eAaImIp3nS	působit
smíšený	smíšený	k2eAgInSc1d1	smíšený
komorní	komorní	k2eAgInSc1d1	komorní
pěvecký	pěvecký	k2eAgInSc1d1	pěvecký
sbor	sbor	k1gInSc1	sbor
Vox	Vox	k1gFnSc2	Vox
Nymburgensis	Nymburgensis	k1gFnSc2	Nymburgensis
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Nymburce	Nymburk	k1gInSc6	Nymburk
se	se	k3xPyFc4	se
také	také	k9	také
koná	konat	k5eAaImIp3nS	konat
každoročně	každoročně	k6eAd1	každoročně
finále	finále	k1gNnSc1	finále
festivalu	festival	k1gInSc2	festival
Mateřinka	mateřinka	k1gFnSc1	mateřinka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Členění	členění	k1gNnSc1	členění
města	město	k1gNnSc2	město
==	==	k?	==
</s>
</p>
<p>
<s>
Nymburk	Nymburk	k1gInSc1	Nymburk
se	se	k3xPyFc4	se
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
skládá	skládat	k5eAaImIp3nS	skládat
za	za	k7c2	za
dvou	dva	k4xCgFnPc2	dva
částí	část	k1gFnPc2	část
na	na	k7c6	na
dvou	dva	k4xCgNnPc6	dva
katastrálních	katastrální	k2eAgNnPc6d1	katastrální
územích	území	k1gNnPc6	území
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Nymburk	Nymburk	k1gInSc1	Nymburk
</s>
</p>
<p>
<s>
Zálabí	zálabí	k1gNnSc1	zálabí
(	(	kIx(	(
<g/>
čtvrť	čtvrť	k1gFnSc1	čtvrť
za	za	k7c7	za
Labem	Labe	k1gNnSc7	Labe
a	a	k8xC	a
centrem	centrum	k1gNnSc7	centrum
města	město	k1gNnSc2	město
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Habeš	Habeš	k1gFnSc1	Habeš
(	(	kIx(	(
<g/>
nymburská	nymburský	k2eAgFnSc1d1	Nymburská
čtvrť	čtvrť	k1gFnSc1	čtvrť
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jankovice	Jankovice	k1gFnSc1	Jankovice
(	(	kIx(	(
<g/>
čtvrť	čtvrť	k1gFnSc1	čtvrť
nymburského	nymburský	k2eAgNnSc2d1	nymburské
sídliště	sídliště	k1gNnSc2	sídliště
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Drahelice	Drahelice	k1gFnSc1	Drahelice
</s>
</p>
<p>
<s>
==	==	k?	==
Správní	správní	k2eAgNnPc1d1	správní
území	území	k1gNnPc1	území
==	==	k?	==
</s>
</p>
<p>
<s>
Nymburk	Nymburk	k1gInSc1	Nymburk
byl	být	k5eAaImAgInS	být
dříve	dříve	k6eAd2	dříve
okresním	okresní	k2eAgNnSc7d1	okresní
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
obcí	obec	k1gFnSc7	obec
s	s	k7c7	s
rozšířenou	rozšířený	k2eAgFnSc7d1	rozšířená
působností	působnost	k1gFnSc7	působnost
a	a	k8xC	a
pověřeným	pověřený	k2eAgInSc7d1	pověřený
obecním	obecní	k2eAgInSc7d1	obecní
úřadem	úřad	k1gInSc7	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Okres	okres	k1gInSc1	okres
Nymburk	Nymburk	k1gInSc1	Nymburk
ale	ale	k8xC	ale
stále	stále	k6eAd1	stále
existuje	existovat	k5eAaImIp3nS	existovat
a	a	k8xC	a
skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
ze	z	k7c2	z
87	[number]	k4	87
obcí	obec	k1gFnPc2	obec
<g/>
,	,	kIx,	,
ORP	ORP	kA	ORP
z	z	k7c2	z
39	[number]	k4	39
obcí	obec	k1gFnPc2	obec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Doprava	doprava	k1gFnSc1	doprava
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Dopravní	dopravní	k2eAgFnSc1d1	dopravní
síť	síť	k1gFnSc1	síť
===	===	k?	===
</s>
</p>
<p>
<s>
Pozemní	pozemní	k2eAgFnSc1d1	pozemní
komunikace	komunikace	k1gFnSc1	komunikace
</s>
</p>
<p>
<s>
Městem	město	k1gNnSc7	město
procházejí	procházet	k5eAaImIp3nP	procházet
silnice	silnice	k1gFnPc1	silnice
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
38	[number]	k4	38
Kolín	Kolín	k1gInSc1	Kolín
<g/>
–	–	k?	–
<g/>
Nymburk	Nymburk	k1gInSc1	Nymburk
<g/>
–	–	k?	–
<g/>
Mladá	mladá	k1gFnSc1	mladá
Boleslav	Boleslav	k1gMnSc1	Boleslav
<g/>
,	,	kIx,	,
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
330	[number]	k4	330
Sadská	sadský	k2eAgFnSc1d1	Sadská
<g/>
–	–	k?	–
<g/>
Nymburk	Nymburk	k1gInSc1	Nymburk
<g/>
–	–	k?	–
<g/>
Činěves	Činěves	k1gInSc1	Činěves
a	a	k8xC	a
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
331	[number]	k4	331
Poděbrady	Poděbrady	k1gInPc7	Poděbrady
<g/>
–	–	k?	–
<g/>
Nymburk	Nymburk	k1gInSc1	Nymburk
<g/>
–	–	k?	–
<g/>
Lysá	Lysá	k1gFnSc1	Lysá
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
–	–	k?	–
<g/>
Stará	starat	k5eAaImIp3nS	starat
Boleslav	Boleslav	k1gMnSc1	Boleslav
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Železnice	železnice	k1gFnSc1	železnice
</s>
</p>
<p>
<s>
Nymburk	Nymburk	k1gInSc1	Nymburk
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
křižovatce	křižovatka	k1gFnSc6	křižovatka
těchto	tento	k3xDgFnPc2	tento
železničních	železniční	k2eAgFnPc2d1	železniční
tratí	trať	k1gFnPc2	trať
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
060	[number]	k4	060
Poříčany	Poříčan	k1gMnPc4	Poříčan
<g/>
–	–	k?	–
<g/>
Sadská	sadský	k2eAgFnSc1d1	Sadská
<g/>
–	–	k?	–
<g/>
Nymburk	Nymburk	k1gInSc1	Nymburk
je	být	k5eAaImIp3nS	být
jednokolejná	jednokolejný	k2eAgFnSc1d1	jednokolejná
elektrizovaná	elektrizovaný	k2eAgFnSc1d1	elektrizovaná
celostátní	celostátní	k2eAgFnSc1d1	celostátní
trať	trať	k1gFnSc1	trať
<g/>
,	,	kIx,	,
zahájení	zahájení	k1gNnSc1	zahájení
dopravy	doprava	k1gFnSc2	doprava
bylo	být	k5eAaImAgNnS	být
roku	rok	k1gInSc2	rok
1883	[number]	k4	1883
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
061	[number]	k4	061
Nymburk	Nymburk	k1gInSc1	Nymburk
<g/>
–	–	k?	–
<g/>
Kopidlno	Kopidlno	k1gNnSc4	Kopidlno
<g/>
–	–	k?	–
<g/>
Jičín	Jičín	k1gInSc1	Jičín
je	být	k5eAaImIp3nS	být
jednokolejná	jednokolejný	k2eAgFnSc1d1	jednokolejná
celostátní	celostátní	k2eAgFnSc1d1	celostátní
trať	trať	k1gFnSc1	trať
<g/>
,	,	kIx,	,
provoz	provoz	k1gInSc1	provoz
byl	být	k5eAaImAgInS	být
zahájen	zahájen	k2eAgInSc1d1	zahájen
roku	rok	k1gInSc2	rok
1881	[number]	k4	1881
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
071	[number]	k4	071
Nymburk	Nymburk	k1gInSc1	Nymburk
<g/>
–	–	k?	–
<g/>
Mladá	mladá	k1gFnSc1	mladá
Boleslav	Boleslav	k1gMnSc1	Boleslav
je	být	k5eAaImIp3nS	být
jednokolejná	jednokolejný	k2eAgFnSc1d1	jednokolejná
celostátní	celostátní	k2eAgFnSc1d1	celostátní
trať	trať	k1gFnSc1	trať
<g/>
,	,	kIx,	,
provoz	provoz	k1gInSc1	provoz
byl	být	k5eAaImAgInS	být
zahájen	zahájen	k2eAgInSc1d1	zahájen
roku	rok	k1gInSc2	rok
1870	[number]	k4	1870
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
231	[number]	k4	231
Praha	Praha	k1gFnSc1	Praha
<g/>
–	–	k?	–
<g/>
Čelákovice	Čelákovice	k1gFnPc4	Čelákovice
<g/>
–	–	k?	–
<g/>
Lysá	Lysá	k1gFnSc1	Lysá
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
–	–	k?	–
<g/>
Nymburk	Nymburk	k1gInSc4	Nymburk
<g/>
–	–	k?	–
<g/>
Poděbrady	Poděbrady	k1gInPc4	Poděbrady
<g/>
–	–	k?	–
<g/>
Kolín	Kolín	k1gInSc1	Kolín
je	být	k5eAaImIp3nS	být
dvoukolejná	dvoukolejný	k2eAgFnSc1d1	dvoukolejná
elektrizovaná	elektrizovaný	k2eAgFnSc1d1	elektrizovaná
celostátní	celostátní	k2eAgFnSc1d1	celostátní
trať	trať	k1gFnSc1	trať
zařazená	zařazený	k2eAgFnSc1d1	zařazená
do	do	k7c2	do
evropského	evropský	k2eAgInSc2d1	evropský
železničního	železniční	k2eAgInSc2d1	železniční
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
součást	součást	k1gFnSc4	součást
Rakouské	rakouský	k2eAgFnSc2d1	rakouská
severozápadní	severozápadní	k2eAgFnPc4d1	severozápadní
dráhy	dráha	k1gFnPc4	dráha
z	z	k7c2	z
Vídně	Vídeň	k1gFnSc2	Vídeň
přes	přes	k7c4	přes
Znojmo	Znojmo	k1gNnSc4	Znojmo
a	a	k8xC	a
Děčín	Děčín	k1gInSc1	Děčín
do	do	k7c2	do
Berlína	Berlín	k1gInSc2	Berlín
<g/>
;	;	kIx,	;
doprava	doprava	k1gFnSc1	doprava
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
úseku	úsek	k1gInSc6	úsek
Kolín	Kolín	k1gInSc1	Kolín
<g/>
–	–	k?	–
<g/>
Nymburk	Nymburk	k1gInSc1	Nymburk
zahájena	zahájen	k2eAgFnSc1d1	zahájena
roku	rok	k1gInSc2	rok
1870	[number]	k4	1870
<g/>
,	,	kIx,	,
v	v	k7c6	v
úseku	úsek	k1gInSc6	úsek
Nymburk	Nymburk	k1gInSc1	Nymburk
<g/>
–	–	k?	–
<g/>
Praha	Praha	k1gFnSc1	Praha
roku	rok	k1gInSc2	rok
1873	[number]	k4	1873
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
města	město	k1gNnSc2	město
leží	ležet	k5eAaImIp3nS	ležet
odbočná	odbočný	k2eAgFnSc1d1	odbočná
železniční	železniční	k2eAgFnSc1d1	železniční
stanice	stanice	k1gFnSc1	stanice
Nymburk	Nymburk	k1gInSc4	Nymburk
hlavní	hlavní	k2eAgNnSc1d1	hlavní
nádraží	nádraží	k1gNnSc1	nádraží
a	a	k8xC	a
odbočná	odbočný	k2eAgFnSc1d1	odbočná
železniční	železniční	k2eAgFnPc4d1	železniční
stanice	stanice	k1gFnPc4	stanice
Nymburk	Nymburk	k1gInSc4	Nymburk
město	město	k1gNnSc1	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Veřejná	veřejný	k2eAgFnSc1d1	veřejná
doprava	doprava	k1gFnSc1	doprava
2011	[number]	k4	2011
===	===	k?	===
</s>
</p>
<p>
<s>
Autobusová	autobusový	k2eAgFnSc1d1	autobusová
doprava	doprava	k1gFnSc1	doprava
</s>
</p>
<p>
<s>
Z	z	k7c2	z
města	město	k1gNnSc2	město
vedly	vést	k5eAaImAgInP	vést
autobusové	autobusový	k2eAgInPc1d1	autobusový
spoje	spoj	k1gInPc1	spoj
do	do	k7c2	do
těchto	tento	k3xDgInPc2	tento
cílů	cíl	k1gInPc2	cíl
<g/>
:	:	kIx,	:
Kolín	Kolín	k1gInSc1	Kolín
<g/>
,	,	kIx,	,
Košice	Košice	k1gInPc1	Košice
<g/>
,	,	kIx,	,
Kutná	kutný	k2eAgFnSc1d1	Kutná
Hora	hora	k1gFnSc1	hora
<g/>
,	,	kIx,	,
Liberec	Liberec	k1gInSc1	Liberec
<g/>
,	,	kIx,	,
Lysá	Lysá	k1gFnSc1	Lysá
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
,	,	kIx,	,
Městec	Městec	k1gInSc1	Městec
Králové	Králová	k1gFnSc2	Králová
<g/>
,	,	kIx,	,
Michalovce	Michalovce	k1gInPc1	Michalovce
<g/>
,	,	kIx,	,
Mladá	mladá	k1gFnSc1	mladá
Boleslav	Boleslav	k1gMnSc1	Boleslav
<g/>
,	,	kIx,	,
Pečky	Pečky	k1gFnPc1	Pečky
<g/>
,	,	kIx,	,
Poděbrady	Poděbrady	k1gInPc1	Poděbrady
<g/>
,	,	kIx,	,
Poprad	Poprad	k1gInSc1	Poprad
<g/>
,	,	kIx,	,
Prešov	Prešov	k1gInSc1	Prešov
<g/>
,	,	kIx,	,
Žilina	Žilina	k1gFnSc1	Žilina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Železniční	železniční	k2eAgFnSc1d1	železniční
doprava	doprava	k1gFnSc1	doprava
</s>
</p>
<p>
<s>
Po	po	k7c6	po
trati	trať	k1gFnSc6	trať
060	[number]	k4	060
vedla	vést	k5eAaImAgFnS	vést
linka	linka	k1gFnSc1	linka
S12	S12	k1gFnSc1	S12
(	(	kIx(	(
<g/>
Poříčany	Poříčan	k1gMnPc4	Poříčan
<g/>
–	–	k?	–
<g/>
Nymburk	Nymburk	k1gInSc1	Nymburk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
po	po	k7c6	po
trati	trať	k1gFnSc6	trať
231	[number]	k4	231
linka	linka	k1gFnSc1	linka
S2	S2	k1gFnSc1	S2
(	(	kIx(	(
<g/>
Praha	Praha	k1gFnSc1	Praha
<g/>
–	–	k?	–
<g/>
Lysá	Lysá	k1gFnSc1	Lysá
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
–	–	k?	–
<g/>
Kolín	Kolín	k1gInSc1	Kolín
<g/>
)	)	kIx)	)
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
pražského	pražský	k2eAgInSc2d1	pražský
systému	systém	k1gInSc2	systém
Esko	eska	k1gFnSc5	eska
<g/>
.	.	kIx.	.
</s>
<s>
Tratí	trať	k1gFnPc2	trať
060	[number]	k4	060
jezdilo	jezdit	k5eAaImAgNnS	jezdit
v	v	k7c6	v
pracovních	pracovní	k2eAgFnPc6d1	pracovní
dny	dna	k1gFnSc2	dna
22	[number]	k4	22
párů	pár	k1gInPc2	pár
osobních	osobní	k2eAgInPc2d1	osobní
vlaků	vlak	k1gInPc2	vlak
<g/>
,	,	kIx,	,
o	o	k7c6	o
víkendech	víkend	k1gInPc6	víkend
19	[number]	k4	19
párů	pár	k1gInPc2	pár
osobních	osobní	k2eAgInPc2d1	osobní
vlaků	vlak	k1gInPc2	vlak
<g/>
.	.	kIx.	.
</s>
<s>
Tratí	trať	k1gFnPc2	trať
061	[number]	k4	061
jezdilo	jezdit	k5eAaImAgNnS	jezdit
v	v	k7c4	v
pracovní	pracovní	k2eAgInPc4d1	pracovní
dny	den	k1gInPc4	den
16	[number]	k4	16
párů	pár	k1gInPc2	pár
osobních	osobní	k2eAgInPc2d1	osobní
vlaků	vlak	k1gInPc2	vlak
a	a	k8xC	a
1	[number]	k4	1
spěšný	spěšný	k2eAgInSc4d1	spěšný
vlak	vlak	k1gInSc4	vlak
<g/>
,	,	kIx,	,
o	o	k7c6	o
víkendu	víkend	k1gInSc6	víkend
13	[number]	k4	13
párů	pár	k1gInPc2	pár
osobních	osobní	k2eAgInPc2d1	osobní
vlaků	vlak	k1gInPc2	vlak
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
trati	trať	k1gFnSc6	trať
071	[number]	k4	071
jezdilo	jezdit	k5eAaImAgNnS	jezdit
v	v	k7c4	v
pracovní	pracovní	k2eAgInPc4d1	pracovní
dny	den	k1gInPc4	den
5	[number]	k4	5
párů	pár	k1gInPc2	pár
rychlíků	rychlík	k1gInPc2	rychlík
a	a	k8xC	a
10	[number]	k4	10
párů	pár	k1gInPc2	pár
osobních	osobní	k2eAgInPc2d1	osobní
vlaků	vlak	k1gInPc2	vlak
<g/>
,	,	kIx,	,
o	o	k7c6	o
víkendu	víkend	k1gInSc6	víkend
6	[number]	k4	6
párů	pár	k1gInPc2	pár
rychlíků	rychlík	k1gInPc2	rychlík
a	a	k8xC	a
9	[number]	k4	9
párů	pár	k1gInPc2	pár
osobních	osobní	k2eAgInPc2d1	osobní
vlaků	vlak	k1gInPc2	vlak
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
trati	trať	k1gFnSc6	trať
231	[number]	k4	231
Nymburkem	Nymburk	k1gInSc7	Nymburk
jezdilo	jezdit	k5eAaImAgNnS	jezdit
v	v	k7c4	v
pracovní	pracovní	k2eAgInPc4d1	pracovní
dny	den	k1gInPc4	den
27	[number]	k4	27
rychlíků	rychlík	k1gMnPc2	rychlík
a	a	k8xC	a
39	[number]	k4	39
osobních	osobní	k2eAgInPc2d1	osobní
vlaků	vlak	k1gInPc2	vlak
<g/>
,	,	kIx,	,
o	o	k7c6	o
víkendu	víkend	k1gInSc6	víkend
23	[number]	k4	23
rychlíků	rychlík	k1gMnPc2	rychlík
a	a	k8xC	a
20	[number]	k4	20
osobních	osobní	k2eAgInPc2d1	osobní
vlaků	vlak	k1gInPc2	vlak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osobnosti	osobnost	k1gFnPc1	osobnost
==	==	k?	==
</s>
</p>
<p>
<s>
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Matěj	Matěj	k1gMnSc1	Matěj
Černohorský	Černohorský	k1gMnSc1	Černohorský
(	(	kIx(	(
<g/>
1684	[number]	k4	1684
<g/>
–	–	k?	–
<g/>
1742	[number]	k4	1742
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
kněz	kněz	k1gMnSc1	kněz
<g/>
,	,	kIx,	,
varhaník	varhaník	k1gMnSc1	varhaník
a	a	k8xC	a
hudební	hudební	k2eAgMnSc1d1	hudební
pedagog	pedagog	k1gMnSc1	pedagog
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Bechyna	Bechyn	k1gMnSc2	Bechyn
(	(	kIx(	(
<g/>
1810	[number]	k4	1810
<g/>
–	–	k?	–
<g/>
1834	[number]	k4	1834
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vrah	vrah	k1gMnSc1	vrah
<g/>
,	,	kIx,	,
domnělý	domnělý	k2eAgMnSc1d1	domnělý
skladatel	skladatel	k1gMnSc1	skladatel
písně	píseň	k1gFnSc2	píseň
Horo	hora	k1gFnSc5	hora
<g/>
,	,	kIx,	,
horo	hora	k1gFnSc5	hora
<g/>
,	,	kIx,	,
vysoká	vysoká	k1gFnSc1	vysoká
jsi	být	k5eAaImIp2nS	být
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Seigerschmidt	Seigerschmidt	k1gMnSc1	Seigerschmidt
(	(	kIx(	(
<g/>
1816	[number]	k4	1816
<g/>
–	–	k?	–
<g/>
1884	[number]	k4	1884
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
starosta	starosta	k1gMnSc1	starosta
města	město	k1gNnSc2	město
a	a	k8xC	a
okresní	okresní	k2eAgMnSc1d1	okresní
starosta	starosta	k1gMnSc1	starosta
<g/>
,	,	kIx,	,
poslanec	poslanec	k1gMnSc1	poslanec
zemského	zemský	k2eAgInSc2d1	zemský
sněmu	sněm	k1gInSc2	sněm
</s>
</p>
<p>
<s>
Božena	Božena	k1gFnSc1	Božena
Němcová	Němcová	k1gFnSc1	Němcová
(	(	kIx(	(
<g/>
1820	[number]	k4	1820
<g/>
–	–	k?	–
<g/>
1862	[number]	k4	1862
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
<g/>
,	,	kIx,	,
napsala	napsat	k5eAaPmAgFnS	napsat
o	o	k7c6	o
Nymburce	Nymburk	k1gInSc6	Nymburk
knížku	knížka	k1gFnSc4	knížka
Kávová	kávový	k2eAgFnSc1d1	kávová
společnost	společnost	k1gFnSc1	společnost
a	a	k8xC	a
o	o	k7c6	o
nymburském	nymburský	k2eAgInSc6d1	nymburský
kraji	kraj	k1gInSc6	kraj
V	v	k7c6	v
zámku	zámek	k1gInSc6	zámek
a	a	k8xC	a
podzámčí	podzámčí	k1gNnSc6	podzámčí
</s>
</p>
<p>
<s>
Hanuš	Hanuš	k1gMnSc1	Hanuš
Karlík	Karlík	k1gMnSc1	Karlík
(	(	kIx(	(
<g/>
1850	[number]	k4	1850
<g/>
–	–	k?	–
<g/>
1927	[number]	k4	1927
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
cukrovarník	cukrovarník	k1gMnSc1	cukrovarník
<g/>
,	,	kIx,	,
čestný	čestný	k2eAgMnSc1d1	čestný
občan	občan	k1gMnSc1	občan
(	(	kIx(	(
<g/>
1909	[number]	k4	1909
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Antonín	Antonín	k1gMnSc1	Antonín
Janoušek	Janoušek	k1gMnSc1	Janoušek
(	(	kIx(	(
<g/>
1877	[number]	k4	1877
<g/>
–	–	k?	–
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rodák	rodák	k1gMnSc1	rodák
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
novinář	novinář	k1gMnSc1	novinář
a	a	k8xC	a
komunistický	komunistický	k2eAgMnSc1d1	komunistický
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
rad	rada	k1gFnPc2	rada
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Dostal	dostat	k5eAaPmAgMnS	dostat
(	(	kIx(	(
<g/>
1884	[number]	k4	1884
<g/>
–	–	k?	–
<g/>
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
herec	herec	k1gMnSc1	herec
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Rachlík	Rachlík	k?	Rachlík
(	(	kIx(	(
<g/>
1904	[number]	k4	1904
<g/>
–	–	k?	–
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
prozaik	prozaik	k1gMnSc1	prozaik
a	a	k8xC	a
dramatik	dramatik	k1gMnSc1	dramatik
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Seidel	Seidel	k1gMnSc1	Seidel
(	(	kIx(	(
<g/>
1908	[number]	k4	1908
<g/>
–	–	k?	–
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Veselý	Veselý	k1gMnSc1	Veselý
(	(	kIx(	(
<g/>
1910	[number]	k4	1910
<g/>
–	–	k?	–
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nymburský	nymburský	k2eAgMnSc1d1	nymburský
tenorista	tenorista	k1gMnSc1	tenorista
a	a	k8xC	a
úředník	úředník	k1gMnSc1	úředník
</s>
</p>
<p>
<s>
Milan	Milan	k1gMnSc1	Milan
Veselý	Veselý	k1gMnSc1	Veselý
(	(	kIx(	(
<g/>
1944	[number]	k4	1944
<g/>
–	–	k?	–
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
violoncellista	violoncellista	k1gMnSc1	violoncellista
<g/>
,	,	kIx,	,
dirigent	dirigent	k1gMnSc1	dirigent
Nymburského	nymburský	k2eAgInSc2d1	nymburský
komorního	komorní	k2eAgInSc2d1	komorní
orchestru	orchestr	k1gInSc2	orchestr
</s>
</p>
<p>
<s>
Bohumil	Bohumil	k1gMnSc1	Bohumil
Hrabal	Hrabal	k1gMnSc1	Hrabal
(	(	kIx(	(
<g/>
1914	[number]	k4	1914
<g/>
–	–	k?	–
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
prozaik	prozaik	k1gMnSc1	prozaik
<g/>
,	,	kIx,	,
vyrůstal	vyrůstat	k5eAaImAgMnS	vyrůstat
v	v	k7c6	v
místním	místní	k2eAgInSc6d1	místní
pivovaru	pivovar	k1gInSc6	pivovar
<g/>
,	,	kIx,	,
napsal	napsat	k5eAaPmAgMnS	napsat
o	o	k7c6	o
Nymburce	Nymburk	k1gInSc6	Nymburk
prózy	próza	k1gFnSc2	próza
např.	např.	kA	např.
Městečko	městečko	k1gNnSc1	městečko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
zastavil	zastavit	k5eAaPmAgInS	zastavit
čas	čas	k1gInSc1	čas
<g/>
,	,	kIx,	,
Postřižiny	postřižiny	k1gFnPc1	postřižiny
<g/>
,	,	kIx,	,
Krasosmutnění	krasosmutnění	k1gNnPc1	krasosmutnění
<g/>
,	,	kIx,	,
Harlekýnovy	harlekýnův	k2eAgInPc1d1	harlekýnův
miliony	milion	k4xCgInPc1	milion
<g/>
,	,	kIx,	,
Ostře	ostro	k6eAd1	ostro
sledované	sledovaný	k2eAgInPc1d1	sledovaný
vlaky	vlak	k1gInPc1	vlak
(	(	kIx(	(
<g/>
děj	děj	k1gInSc1	děj
z	z	k7c2	z
nedalekých	daleký	k2eNgFnPc2d1	nedaleká
Kostomlat	Kostomlat	k1gFnPc2	Kostomlat
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Miroslav	Miroslav	k1gMnSc1	Miroslav
Macháček	Macháček	k1gMnSc1	Macháček
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
–	–	k?	–
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
divadelní	divadelní	k2eAgMnSc1d1	divadelní
režisér	režisér	k1gMnSc1	režisér
narozen	narozen	k2eAgMnSc1d1	narozen
v	v	k7c6	v
Nymburce	Nymburk	k1gInSc6	Nymburk
v	v	k7c6	v
Eliščině	Eliščin	k2eAgFnSc6d1	Eliščina
třídě	třída	k1gFnSc6	třída
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
také	také	k9	také
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
výročí	výročí	k1gNnSc4	výročí
90	[number]	k4	90
<g/>
.	.	kIx.	.
narozenin	narozeniny	k1gFnPc2	narozeniny
odhalila	odhalit	k5eAaPmAgFnS	odhalit
jeho	jeho	k3xOp3gFnSc1	jeho
dcera	dcera	k1gFnSc1	dcera
Kateřina	Kateřina	k1gFnSc1	Kateřina
Macháčková	Macháčková	k1gFnSc1	Macháčková
pamětní	pamětní	k2eAgFnSc4d1	pamětní
desku	deska	k1gFnSc4	deska
</s>
</p>
<p>
<s>
Radek	Radek	k1gMnSc1	Radek
Bejbl	Bejbl	k1gMnSc1	Bejbl
(	(	kIx(	(
<g/>
*	*	kIx~	*
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fotbalový	fotbalový	k2eAgMnSc1d1	fotbalový
reprezentant	reprezentant	k1gMnSc1	reprezentant
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Navrátil	Navrátil	k1gMnSc1	Navrátil
(	(	kIx(	(
<g/>
1926	[number]	k4	1926
<g/>
–	–	k?	–
<g/>
2018	[number]	k4	2018
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
učitel	učitel	k1gMnSc1	učitel
<g/>
,	,	kIx,	,
hudebník	hudebník	k1gMnSc1	hudebník
a	a	k8xC	a
organizátor	organizátor	k1gMnSc1	organizátor
kulturního	kulturní	k2eAgInSc2d1	kulturní
a	a	k8xC	a
společenského	společenský	k2eAgInSc2d1	společenský
života	život	k1gInSc2	život
</s>
</p>
<p>
<s>
==	==	k?	==
Partnerská	partnerský	k2eAgNnPc1d1	partnerské
města	město	k1gNnPc1	město
==	==	k?	==
</s>
</p>
<p>
<s>
Mytišči	Mytišč	k1gFnSc3	Mytišč
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc4	Rusko
</s>
</p>
<p>
<s>
Neuruppin	Neuruppin	k1gInSc1	Neuruppin
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
</s>
</p>
<p>
<s>
Újfehértó	Újfehértó	k?	Újfehértó
<g/>
,	,	kIx,	,
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
</s>
</p>
<p>
<s>
Vrútky	Vrútek	k1gInPc1	Vrútek
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc1	Slovensko
</s>
</p>
<p>
<s>
Żarów	Żarów	k?	Żarów
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
</s>
</p>
<p>
<s>
==	==	k?	==
Fotogalerie	Fotogalerie	k1gFnSc2	Fotogalerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Okresní	okresní	k2eAgInSc1d1	okresní
soud	soud	k1gInSc1	soud
v	v	k7c6	v
Nymburce	Nymburk	k1gInSc6	Nymburk
</s>
</p>
<p>
<s>
Gymnázium	gymnázium	k1gNnSc1	gymnázium
Bohumila	Bohumil	k1gMnSc2	Bohumil
Hrabala	Hrabal	k1gMnSc2	Hrabal
v	v	k7c6	v
Nymburce	Nymburk	k1gInSc6	Nymburk
</s>
</p>
<p>
<s>
Mateřinka	mateřinka	k1gFnSc1	mateřinka
</s>
</p>
<p>
<s>
ČEZ	ČEZ	kA	ČEZ
Basketball	Basketball	k1gInSc1	Basketball
Nymburk	Nymburk	k1gInSc1	Nymburk
</s>
</p>
<p>
<s>
Městská	městský	k2eAgFnSc1d1	městská
autobusová	autobusový	k2eAgFnSc1d1	autobusová
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Nymburce	Nymburk	k1gInSc6	Nymburk
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Nymburk	Nymburk	k1gInSc1	Nymburk
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Nymburk	Nymburk	k1gInSc1	Nymburk
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Nimburk	Nimburk	k1gInSc1	Nimburk
ve	v	k7c6	v
Vlastenském	vlastenský	k2eAgInSc6d1	vlastenský
slovníku	slovník	k1gInSc6	slovník
historickém	historický	k2eAgInSc6d1	historický
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
Územně	územně	k6eAd1	územně
identifikační	identifikační	k2eAgInSc1d1	identifikační
registr	registr	k1gInSc1	registr
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
Nymburk	Nymburk	k1gInSc1	Nymburk
v	v	k7c6	v
Územně	územně	k6eAd1	územně
identifikačním	identifikační	k2eAgInSc6d1	identifikační
registru	registr	k1gInSc6	registr
ČR	ČR	kA	ČR
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
statistické	statistický	k2eAgInPc1d1	statistický
údaje	údaj	k1gInPc1	údaj
města	město	k1gNnSc2	město
Nymburk	Nymburk	k1gInSc1	Nymburk
–	–	k?	–
neplatný	platný	k2eNgInSc1d1	neplatný
odkaz	odkaz	k1gInSc1	odkaz
!	!	kIx.	!
</s>
</p>
