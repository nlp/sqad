<s>
Alkoholické	alkoholický	k2eAgInPc1d1	alkoholický
nápoje	nápoj	k1gInPc1	nápoj
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
nazývané	nazývaný	k2eAgInPc4d1	nazývaný
alkohol	alkohol	k1gInSc4	alkohol
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
nápoje	nápoj	k1gInPc4	nápoj
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgInSc4d1	obsahující
přes	přes	k7c4	přes
0,75	[number]	k4	0,75
<g/>
%	%	kIx~	%
objemových	objemový	k2eAgNnPc2d1	objemové
procent	procento	k1gNnPc2	procento
ethanolu	ethanol	k1gInSc2	ethanol
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
požití	požití	k1gNnSc1	požití
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
opilost	opilost	k1gFnSc4	opilost
<g/>
:	:	kIx,	:
v	v	k7c6	v
menších	malý	k2eAgFnPc6d2	menší
dávkách	dávka	k1gFnPc6	dávka
(	(	kIx(	(
<g/>
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
metabolismu	metabolismus	k1gInSc6	metabolismus
jedince	jedinec	k1gMnSc2	jedinec
<g/>
)	)	kIx)	)
uvolnění	uvolnění	k1gNnSc1	uvolnění
a	a	k8xC	a
euforické	euforický	k2eAgInPc1d1	euforický
stavy	stav	k1gInPc1	stav
<g/>
,	,	kIx,	,
ve	v	k7c6	v
větších	veliký	k2eAgFnPc6d2	veliký
dávkách	dávka	k1gFnPc6	dávka
útlum	útlum	k1gInSc4	útlum
<g/>
,	,	kIx,	,
nevolnost	nevolnost	k1gFnSc4	nevolnost
až	až	k8xS	až
otravu	otrava	k1gFnSc4	otrava
<g/>
.	.	kIx.	.
</s>
<s>
Alkohol	alkohol	k1gInSc1	alkohol
ethanol	ethanol	k1gInSc1	ethanol
je	být	k5eAaImIp3nS	být
omamná	omamný	k2eAgFnSc1d1	omamná
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
obsažená	obsažený	k2eAgFnSc1d1	obsažená
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
nápojích	nápoj	k1gInPc6	nápoj
<g/>
.	.	kIx.	.
</s>
<s>
Savci	savec	k1gMnPc1	savec
jsou	být	k5eAaImIp3nP	být
schopni	schopen	k2eAgMnPc1d1	schopen
díky	díky	k7c3	díky
enzymu	enzym	k1gInSc3	enzym
ADH4	ADH4	k1gFnSc2	ADH4
trávit	trávit	k5eAaImF	trávit
alkohol	alkohol	k1gInSc4	alkohol
již	již	k9	již
milióny	milión	k4xCgInPc1	milión
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Metabolizuje	metabolizovat	k5eAaImIp3nS	metabolizovat
na	na	k7c4	na
acetaldehyd	acetaldehyd	k1gInSc4	acetaldehyd
<g/>
.	.	kIx.	.
</s>
<s>
Užíval	užívat	k5eAaImAgInS	užívat
se	se	k3xPyFc4	se
při	při	k7c6	při
rituálech	rituál	k1gInPc6	rituál
či	či	k8xC	či
oslavách	oslava	k1gFnPc6	oslava
jako	jako	k8xC	jako
například	například	k6eAd1	například
mešní	mešní	k2eAgNnSc4d1	mešní
víno	víno	k1gNnSc4	víno
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
změny	změna	k1gFnPc4	změna
ve	v	k7c6	v
vnímání	vnímání	k1gNnSc6	vnímání
a	a	k8xC	a
chování	chování	k1gNnSc6	chování
<g/>
.	.	kIx.	.
</s>
<s>
Alkohol	alkohol	k1gInSc1	alkohol
je	být	k5eAaImIp3nS	být
návykový	návykový	k2eAgInSc1d1	návykový
(	(	kIx(	(
<g/>
tělo	tělo	k1gNnSc1	tělo
si	se	k3xPyFc3	se
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
zvykne	zvyknout	k5eAaPmIp3nS	zvyknout
a	a	k8xC	a
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
ho	on	k3xPp3gMnSc4	on
víc	hodně	k6eAd2	hodně
a	a	k8xC	a
víc	hodně	k6eAd2	hodně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
ho	on	k3xPp3gMnSc4	on
řadíme	řadit	k5eAaImIp1nP	řadit
mezi	mezi	k7c4	mezi
drogy	droga	k1gFnPc4	droga
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
vzniká	vznikat	k5eAaImIp3nS	vznikat
kvašením	kvašení	k1gNnSc7	kvašení
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yQgInSc6	který
kvasinky	kvasinka	k1gFnPc1	kvasinka
mění	měnit	k5eAaImIp3nP	měnit
cukr	cukr	k1gInSc4	cukr
na	na	k7c4	na
oxid	oxid	k1gInSc4	oxid
uhličitý	uhličitý	k2eAgInSc4d1	uhličitý
a	a	k8xC	a
alkohol	alkohol	k1gInSc4	alkohol
<g/>
.	.	kIx.	.
</s>
<s>
Začne	začít	k5eAaPmIp3nS	začít
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
nám	my	k3xPp1nPc3	my
kazit	kazit	k5eAaImF	kazit
jablečný	jablečný	k2eAgInSc4d1	jablečný
mošt	mošt	k1gInSc4	mošt
<g/>
,	,	kIx,	,
dělají	dělat	k5eAaImIp3nP	dělat
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
bublinky	bublinka	k1gFnPc1	bublinka
<g/>
.	.	kIx.	.
</s>
<s>
Mošt	mošt	k1gInSc1	mošt
kvasí	kvasit	k5eAaImIp3nS	kvasit
a	a	k8xC	a
přestane	přestat	k5eAaPmIp3nS	přestat
být	být	k5eAaImF	být
sladký	sladký	k2eAgInSc1d1	sladký
(	(	kIx(	(
<g/>
cukr	cukr	k1gInSc1	cukr
se	se	k3xPyFc4	se
přeměnil	přeměnit	k5eAaPmAgInS	přeměnit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nakysle	nakysle	k6eAd1	nakysle
trpký	trpký	k2eAgInSc1d1	trpký
<g/>
,	,	kIx,	,
říkáme	říkat	k5eAaImIp1nP	říkat
navinulý	navinulý	k2eAgInSc4d1	navinulý
<g/>
.	.	kIx.	.
</s>
<s>
Podobným	podobný	k2eAgInSc7d1	podobný
způsobem	způsob	k1gInSc7	způsob
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
jablečné	jablečný	k2eAgNnSc1d1	jablečné
víno	víno	k1gNnSc1	víno
nebo	nebo	k8xC	nebo
víno	víno	k1gNnSc1	víno
z	z	k7c2	z
vinných	vinný	k2eAgInPc2d1	vinný
hroznů	hrozen	k1gInPc2	hrozen
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
daly	dát	k5eAaPmAgInP	dát
nápoji	nápoj	k1gInSc3	nápoj
jméno	jméno	k1gNnSc1	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Víno	víno	k1gNnSc1	víno
běžně	běžně	k6eAd1	běžně
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
10	[number]	k4	10
až	až	k9	až
15	[number]	k4	15
%	%	kIx~	%
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
,	,	kIx,	,
pivo	pivo	k1gNnSc1	pivo
přibližně	přibližně	k6eAd1	přibližně
polovinu	polovina	k1gFnSc4	polovina
<g/>
,	,	kIx,	,
vyšších	vysoký	k2eAgFnPc2d2	vyšší
koncentrací	koncentrace	k1gFnPc2	koncentrace
již	již	k6eAd1	již
přirozenou	přirozený	k2eAgFnSc7d1	přirozená
fermentací	fermentace	k1gFnSc7	fermentace
nelze	lze	k6eNd1	lze
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
<g/>
,	,	kIx,	,
takzvané	takzvaný	k2eAgFnPc1d1	takzvaná
pálenky	pálenka	k1gFnPc1	pálenka
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
destilací	destilace	k1gFnSc7	destilace
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
konzumaci	konzumace	k1gFnSc6	konzumace
alkoholických	alkoholický	k2eAgInPc2d1	alkoholický
nápojů	nápoj	k1gInPc2	nápoj
nesmí	smět	k5eNaImIp3nP	smět
lidé	člověk	k1gMnPc1	člověk
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zemích	zem	k1gFnPc6	zem
řídit	řídit	k5eAaImF	řídit
dopravní	dopravní	k2eAgInPc4d1	dopravní
prostředky	prostředek	k1gInPc4	prostředek
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
by	by	kYmCp3nP	by
mohli	moct	k5eAaImAgMnP	moct
být	být	k5eAaImF	být
nebezpeční	bezpečný	k2eNgMnPc1d1	nebezpečný
sobě	se	k3xPyFc3	se
i	i	k9	i
druhým	druhý	k4xOgNnSc7	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Člověku	člověk	k1gMnSc3	člověk
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
alkohol	alkohol	k1gInSc4	alkohol
nikdy	nikdy	k6eAd1	nikdy
nepije	pít	k5eNaImIp3nS	pít
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
abstinent	abstinent	k1gMnSc1	abstinent
<g/>
.	.	kIx.	.
</s>
<s>
Soustavným	soustavný	k2eAgNnSc7d1	soustavné
požíváním	požívání	k1gNnSc7	požívání
alkoholických	alkoholický	k2eAgInPc2d1	alkoholický
nápojů	nápoj	k1gInPc2	nápoj
v	v	k7c6	v
nadměrné	nadměrný	k2eAgFnSc6d1	nadměrná
míře	míra	k1gFnSc6	míra
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
vyvinout	vyvinout	k5eAaPmF	vyvinout
(	(	kIx(	(
<g/>
jak	jak	k6eAd1	jak
duševní	duševní	k2eAgInSc1d1	duševní
tak	tak	k9	tak
i	i	k9	i
tělesné	tělesný	k2eAgNnSc4d1	tělesné
<g/>
)	)	kIx)	)
onemocnění	onemocnění	k1gNnSc4	onemocnění
–	–	k?	–
alkoholismus	alkoholismus	k1gInSc1	alkoholismus
<g/>
.	.	kIx.	.
</s>
<s>
Ohledně	ohledně	k6eAd1	ohledně
přiměřené	přiměřený	k2eAgFnPc1d1	přiměřená
dávky	dávka	k1gFnPc1	dávka
alkoholu	alkohol	k1gInSc2	alkohol
se	se	k3xPyFc4	se
studie	studie	k1gFnPc1	studie
rozcházejí	rozcházet	k5eAaImIp3nP	rozcházet
<g/>
.	.	kIx.	.
</s>
<s>
Půjde	jít	k5eAaImIp3nS	jít
však	však	k9	však
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
o	o	k7c4	o
vliv	vliv	k1gInSc4	vliv
systematické	systematický	k2eAgFnSc2d1	systematická
chyby	chyba	k1gFnSc2	chyba
studií	studio	k1gNnPc2	studio
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
abstinentů	abstinent	k1gMnPc2	abstinent
například	například	k6eAd1	například
spadají	spadat	k5eAaPmIp3nP	spadat
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
nepijí	pít	k5eNaImIp3nP	pít
ze	z	k7c2	z
zdravotních	zdravotní	k2eAgInPc2d1	zdravotní
důvodů	důvod	k1gInPc2	důvod
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
alkoholické	alkoholický	k2eAgInPc1d1	alkoholický
nápoje	nápoj	k1gInPc1	nápoj
(	(	kIx(	(
<g/>
např.	např.	kA	např.
víno	víno	k1gNnSc4	víno
<g/>
)	)	kIx)	)
mohou	moct	k5eAaImIp3nP	moct
částečně	částečně	k6eAd1	částečně
organismu	organismus	k1gInSc2	organismus
prospět	prospět	k5eAaPmF	prospět
(	(	kIx(	(
<g/>
jistými	jistý	k2eAgFnPc7d1	jistá
ovocnými	ovocný	k2eAgFnPc7d1	ovocná
látkami	látka	k1gFnPc7	látka
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
flavonoidy	flavonoid	k1gInPc4	flavonoid
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
podle	podle	k7c2	podle
výzkumu	výzkum	k1gInSc2	výzkum
Cincinnatské	Cincinnatský	k2eAgFnSc2d1	Cincinnatský
univerzity	univerzita	k1gFnSc2	univerzita
vlastní	vlastní	k2eAgInSc1d1	vlastní
alkohol	alkohol	k1gInSc1	alkohol
zdraví	zdraví	k1gNnSc2	zdraví
škodí	škodit	k5eAaImIp3nS	škodit
i	i	k9	i
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
míře	míra	k1gFnSc6	míra
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
typ	typ	k1gInSc4	typ
nápoje	nápoj	k1gInSc2	nápoj
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
malých	malý	k2eAgFnPc2d1	malá
dávek	dávka	k1gFnPc2	dávka
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
i	i	k9	i
například	například	k6eAd1	například
karcinom	karcinom	k1gInSc1	karcinom
prsu	prs	k1gInSc2	prs
–	–	k?	–
dvě	dva	k4xCgFnPc1	dva
alkoholové	alkoholový	k2eAgFnPc1d1	alkoholová
jednotky	jednotka	k1gFnPc1	jednotka
(	(	kIx(	(
<g/>
půl	půl	k1xP	půl
litru	litr	k1gInSc6	litr
piva	pivo	k1gNnSc2	pivo
s	s	k7c7	s
4	[number]	k4	4
%	%	kIx~	%
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
)	)	kIx)	)
za	za	k7c4	za
den	den	k1gInSc4	den
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
riziko	riziko	k1gNnSc4	riziko
této	tento	k3xDgFnSc2	tento
rakoviny	rakovina	k1gFnSc2	rakovina
o	o	k7c4	o
24	[number]	k4	24
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Malé	Malé	k2eAgFnPc1d1	Malé
dávky	dávka	k1gFnPc1	dávka
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
riziko	riziko	k1gNnSc4	riziko
i	i	k9	i
dalších	další	k2eAgInPc2d1	další
typů	typ	k1gInPc2	typ
rakovin	rakovina	k1gFnPc2	rakovina
<g/>
.	.	kIx.	.
</s>
<s>
Alkohol	alkohol	k1gInSc1	alkohol
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
přibližně	přibližně	k6eAd1	přibližně
6	[number]	k4	6
%	%	kIx~	%
rakovin	rakovina	k1gFnPc2	rakovina
a	a	k8xC	a
nepomáhá	pomáhat	k5eNaImIp3nS	pomáhat
proti	proti	k7c3	proti
kardiovaskulárním	kardiovaskulární	k2eAgNnPc3d1	kardiovaskulární
onemocněním	onemocnění	k1gNnPc3	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
agentura	agentura	k1gFnSc1	agentura
pro	pro	k7c4	pro
výzkum	výzkum	k1gInSc4	výzkum
rakoviny	rakovina	k1gFnSc2	rakovina
alkoholické	alkoholický	k2eAgInPc4d1	alkoholický
nápoje	nápoj	k1gInPc4	nápoj
klasifikuje	klasifikovat	k5eAaImIp3nS	klasifikovat
jako	jako	k9	jako
prokázané	prokázaný	k2eAgInPc4d1	prokázaný
karcinogeny	karcinogen	k1gInPc4	karcinogen
skupiny	skupina	k1gFnSc2	skupina
1	[number]	k4	1
(	(	kIx(	(
<g/>
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
i	i	k9	i
tabákový	tabákový	k2eAgInSc1d1	tabákový
kouř	kouř	k1gInSc1	kouř
či	či	k8xC	či
radioaktivita	radioaktivita	k1gFnSc1	radioaktivita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
efektivní	efektivní	k2eAgInSc4d1	efektivní
způsob	způsob	k1gInSc4	způsob
snížení	snížení	k1gNnSc4	snížení
rizika	riziko	k1gNnSc2	riziko
rakoviny	rakovina	k1gFnSc2	rakovina
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
snížení	snížení	k1gNnSc1	snížení
konzumace	konzumace	k1gFnSc2	konzumace
alkoholických	alkoholický	k2eAgInPc2d1	alkoholický
nápojů	nápoj	k1gInPc2	nápoj
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
v	v	k7c6	v
jistých	jistý	k2eAgFnPc6d1	jistá
zemích	zem	k1gFnPc6	zem
přistupuje	přistupovat	k5eAaImIp3nS	přistupovat
k	k	k7c3	k
varovným	varovný	k2eAgInSc7d1	varovný
označování	označování	k1gNnSc4	označování
lahví	lahev	k1gFnPc2	lahev
podobných	podobný	k2eAgFnPc2d1	podobná
varováním	varování	k1gNnSc7	varování
na	na	k7c6	na
krabičkách	krabička	k1gFnPc6	krabička
cigaret	cigareta	k1gFnPc2	cigareta
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
negativním	negativní	k2eAgInSc7d1	negativní
důsledkem	důsledek	k1gInSc7	důsledek
požívání	požívání	k1gNnSc1	požívání
alkoholu	alkohol	k1gInSc2	alkohol
je	být	k5eAaImIp3nS	být
i	i	k9	i
zubní	zubní	k2eAgInSc4d1	zubní
kaz	kaz	k1gInSc4	kaz
<g/>
.	.	kIx.	.
pivo	pivo	k1gNnSc4	pivo
spontánně	spontánně	k6eAd1	spontánně
kvašená	kvašený	k2eAgNnPc1d1	kvašené
piva	pivo	k1gNnPc1	pivo
svrchně	svrchně	k6eAd1	svrchně
kvašená	kvašený	k2eAgNnPc1d1	kvašené
piva	pivo	k1gNnPc1	pivo
spodně	spodně	k6eAd1	spodně
kvašená	kvašený	k2eAgNnPc1d1	kvašené
piva	pivo	k1gNnPc1	pivo
(	(	kIx(	(
<g/>
ležáky	ležák	k1gInPc1	ležák
<g/>
)	)	kIx)	)
s	s	k7c7	s
pivem	pivo	k1gNnSc7	pivo
míchané	míchaná	k1gFnSc2	míchaná
nápoje	nápoj	k1gInSc2	nápoj
medovina	medovina	k1gFnSc1	medovina
víno	víno	k1gNnSc4	víno
šumivé	šumivý	k2eAgNnSc1d1	šumivé
víno	víno	k1gNnSc1	víno
–	–	k?	–
sekt	sekta	k1gFnPc2	sekta
šampaňské	šampaňský	k2eAgNnSc4d1	šampaňské
víno	víno	k1gNnSc4	víno
červené	červený	k2eAgNnSc1d1	červené
víno	víno	k1gNnSc1	víno
bílé	bílý	k2eAgNnSc1d1	bílé
víno	víno	k1gNnSc1	víno
vinné	vinný	k2eAgFnSc2d1	vinná
míchané	míchaná	k1gFnSc2	míchaná
nápoje	nápoj	k1gInSc2	nápoj
ovocná	ovocný	k2eAgNnPc1d1	ovocné
vína	víno	k1gNnPc1	víno
perlivá	perlivý	k2eAgFnSc1d1	perlivá
vína	vína	k1gFnSc1	vína
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
sekty	sekta	k1gFnPc1	sekta
burčák	burčák	k1gInSc1	burčák
saké	saké	k1gNnSc2	saké
lihoviny	lihovina	k1gFnSc2	lihovina
destiláty	destilát	k1gInPc4	destilát
vodka	vodka	k1gFnSc1	vodka
<g/>
,	,	kIx,	,
brandy	brandy	k1gFnSc1	brandy
(	(	kIx(	(
<g/>
koňak	koňak	k1gInSc1	koňak
<g/>
,	,	kIx,	,
Metaxa	Metaxa	k1gFnSc1	Metaxa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
gin	gin	k1gInSc1	gin
<g/>
,	,	kIx,	,
rum	rum	k1gInSc1	rum
<g/>
,	,	kIx,	,
whisky	whisky	k1gFnSc1	whisky
<g/>
,	,	kIx,	,
absint	absint	k1gInSc1	absint
<g/>
,	,	kIx,	,
slivovice	slivovice	k1gFnSc1	slivovice
<g/>
,	,	kIx,	,
ořechovka	ořechovka	k1gFnSc1	ořechovka
<g/>
,	,	kIx,	,
tequila	tequila	k1gFnSc1	tequila
<g/>
,	,	kIx,	,
arak	arak	k1gInSc1	arak
<g/>
,	,	kIx,	,
pastis	pastis	k1gFnPc1	pastis
<g/>
,	,	kIx,	,
rakije	rakija	k1gFnPc1	rakija
likéry	likér	k1gInPc4	likér
Becherovka	becherovka	k1gFnSc1	becherovka
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Praděd	praděd	k1gMnSc1	praděd
<g/>
,	,	kIx,	,
griotka	griotka	k1gFnSc1	griotka
<g/>
,	,	kIx,	,
Chartreuse	Chartreuse	k1gFnSc1	Chartreuse
<g/>
,	,	kIx,	,
fernet	fernet	k1gInSc1	fernet
<g/>
,	,	kIx,	,
Stará	starý	k2eAgFnSc1d1	stará
Myslivecká	myslivecký	k2eAgFnSc1d1	myslivecká
<g/>
,	,	kIx,	,
zelená	zelený	k2eAgFnSc1d1	zelená
<g/>
,	,	kIx,	,
vaječný	vaječný	k2eAgInSc1d1	vaječný
koňak	koňak	k1gInSc1	koňak
koktejly	koktejl	k1gInPc1	koktejl
–	–	k?	–
nápoje	nápoj	k1gInSc2	nápoj
smíchané	smíchaný	k2eAgFnSc2d1	smíchaná
z	z	k7c2	z
několika	několik	k4yIc2	několik
různých	různý	k2eAgInPc2d1	různý
alkoholických	alkoholický	k2eAgInPc2d1	alkoholický
i	i	k8xC	i
nealkoholických	alkoholický	k2eNgInPc2d1	nealkoholický
nápojů	nápoj	k1gInPc2	nápoj
Piñ	Piñ	k1gFnSc1	Piñ
Colada	Colada	k1gFnSc1	Colada
<g/>
,	,	kIx,	,
Big	Big	k1gFnSc1	Big
Apple	Apple	kA	Apple
<g/>
,	,	kIx,	,
Swiming	Swiming	k1gInSc1	Swiming
Pool	Pool	k1gInSc1	Pool
<g/>
,	,	kIx,	,
Tequila	tequila	k1gFnSc1	tequila
Sunrise	Sunrise	k1gFnSc1	Sunrise
<g/>
,	,	kIx,	,
červené	červený	k2eAgNnSc1d1	červené
víno	víno	k1gNnSc1	víno
s	s	k7c7	s
kolou	kola	k1gFnSc7	kola
aperitiv	aperitiv	k1gInSc4	aperitiv
aperitivy	aperitiv	k1gInPc4	aperitiv
lehké	lehký	k2eAgInPc4d1	lehký
aperitivy	aperitiv	k1gInPc4	aperitiv
sladké	sladký	k2eAgFnSc2d1	sladká
digestiv	digestit	k5eAaPmDgInS	digestit
</s>
