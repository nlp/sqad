<p>
<s>
Mixér	mixér	k1gInSc1	mixér
je	být	k5eAaImIp3nS	být
elektrický	elektrický	k2eAgInSc4d1	elektrický
kuchyňský	kuchyňský	k2eAgInSc4d1	kuchyňský
přístroj	přístroj	k1gInSc4	přístroj
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
mixování	mixování	k1gNnSc3	mixování
potravin	potravina	k1gFnPc2	potravina
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
k	k	k7c3	k
tvorbě	tvorba	k1gFnSc3	tvorba
pyré	pyré	k1gNnSc2	pyré
z	z	k7c2	z
ovoce	ovoce	k1gNnSc2	ovoce
<g/>
,	,	kIx,	,
zeleniny	zelenina	k1gFnSc2	zelenina
<g/>
,	,	kIx,	,
šlehání	šlehání	k1gNnSc1	šlehání
a	a	k8xC	a
mixování	mixování	k1gNnSc1	mixování
potravin	potravina	k1gFnPc2	potravina
a	a	k8xC	a
tekutin	tekutina	k1gFnPc2	tekutina
<g/>
,	,	kIx,	,
hnětení	hnětení	k1gNnSc2	hnětení
těsta	těsto	k1gNnSc2	těsto
či	či	k8xC	či
drcení	drcení	k1gNnSc2	drcení
ledu	led	k1gInSc2	led
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
nerezových	rezový	k2eNgInPc2d1	nerezový
sekacích	sekací	k2eAgInPc2d1	sekací
nožů	nůž	k1gInPc2	nůž
či	či	k8xC	či
šlehací	šlehací	k2eAgFnPc1d1	šlehací
metly	metla	k1gFnPc1	metla
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
pohání	pohánět	k5eAaImIp3nP	pohánět
elektrický	elektrický	k2eAgInSc4d1	elektrický
motor	motor	k1gInSc4	motor
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
je	být	k5eAaImIp3nS	být
umístěn	umístit	k5eAaPmNgInS	umístit
v	v	k7c6	v
plastovém	plastový	k2eAgInSc6d1	plastový
krytu	kryt	k1gInSc6	kryt
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
ovládací	ovládací	k2eAgNnPc1d1	ovládací
tlačítka	tlačítko	k1gNnPc1	tlačítko
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
mixérům	mixér	k1gInPc3	mixér
se	se	k3xPyFc4	se
přikládá	přikládat	k5eAaImIp3nS	přikládat
plastové	plastový	k2eAgFnSc3d1	plastová
či	či	k8xC	či
skleněná	skleněný	k2eAgFnSc1d1	skleněná
nádoba	nádoba	k1gFnSc1	nádoba
<g/>
,	,	kIx,	,
do	do	k7c2	do
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
vkládá	vkládat	k5eAaImIp3nS	vkládat
mixovaná	mixovaný	k2eAgFnSc1d1	mixovaná
potravina	potravina	k1gFnSc1	potravina
<g/>
.	.	kIx.	.
</s>
<s>
Mixéry	mixér	k1gInPc1	mixér
se	se	k3xPyFc4	se
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
dle	dle	k7c2	dle
konstrukce	konstrukce	k1gFnSc2	konstrukce
<g/>
,	,	kIx,	,
účelu	účel	k1gInSc3	účel
<g/>
,	,	kIx,	,
objemu	objem	k1gInSc3	objem
nádoby	nádoba	k1gFnSc2	nádoba
<g/>
,	,	kIx,	,
výkonu	výkon	k1gInSc2	výkon
přístroje	přístroj	k1gInSc2	přístroj
a	a	k8xC	a
počtu	počet	k1gInSc2	počet
rychlostí	rychlost	k1gFnPc2	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
Mixér	mixér	k1gInSc1	mixér
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
Stephen	Stephen	k2eAgMnSc1d1	Stephen
Poplawski	Poplawsk	k1gFnSc3	Poplawsk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Typy	typ	k1gInPc1	typ
==	==	k?	==
</s>
</p>
<p>
<s>
Tyčové	tyčový	k2eAgInPc1d1	tyčový
mixéry	mixér	k1gInPc1	mixér
slouží	sloužit	k5eAaImIp3nP	sloužit
k	k	k7c3	k
sekání	sekání	k1gNnSc3	sekání
<g/>
,	,	kIx,	,
šlehání	šlehání	k1gNnSc3	šlehání
<g/>
,	,	kIx,	,
drcení	drcení	k1gNnSc3	drcení
ledu	led	k1gInSc2	led
a	a	k8xC	a
míchání	míchání	k1gNnSc2	míchání
potravin	potravina	k1gFnPc2	potravina
<g/>
.	.	kIx.	.
</s>
<s>
Přístroj	přístroj	k1gInSc1	přístroj
je	být	k5eAaImIp3nS	být
vybaven	vybavit	k5eAaPmNgInS	vybavit
noži	nůž	k1gInPc7	nůž
k	k	k7c3	k
sekání	sekání	k1gNnSc3	sekání
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
přiložena	přiložen	k2eAgFnSc1d1	přiložena
výměnná	výměnný	k2eAgFnSc1d1	výměnná
šlehací	šlehací	k2eAgFnSc1d1	šlehací
metla	metla	k1gFnSc1	metla
a	a	k8xC	a
vhodná	vhodný	k2eAgFnSc1d1	vhodná
nádoba	nádoba	k1gFnSc1	nádoba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ruční	ruční	k2eAgInPc1d1	ruční
mixéry	mixér	k1gInPc1	mixér
(	(	kIx(	(
<g/>
též	též	k6eAd1	též
šlehače	šlehač	k1gInPc1	šlehač
<g/>
)	)	kIx)	)
slouží	sloužit	k5eAaImIp3nS	sloužit
především	především	k9	především
ke	k	k7c3	k
šlehání	šlehání	k1gNnSc3	šlehání
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
vybaveny	vybavit	k5eAaPmNgFnP	vybavit
šlehací	šlehací	k2eAgFnSc7d1	šlehací
metlou	metla	k1gFnSc7	metla
<g/>
.	.	kIx.	.
</s>
<s>
Příslušenství	příslušenství	k1gNnSc1	příslušenství
může	moct	k5eAaImIp3nS	moct
obsahovat	obsahovat	k5eAaImF	obsahovat
vhodnou	vhodný	k2eAgFnSc4d1	vhodná
nádobu	nádoba	k1gFnSc4	nádoba
a	a	k8xC	a
hnětací	hnětací	k2eAgInPc4d1	hnětací
háky	hák	k1gInPc4	hák
například	například	k6eAd1	například
k	k	k7c3	k
přípravě	příprava	k1gFnSc3	příprava
těst	těsto	k1gNnPc2	těsto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stolní	stolní	k2eAgInPc1d1	stolní
mixéry	mixér	k1gInPc1	mixér
jsou	být	k5eAaImIp3nP	být
určeny	určit	k5eAaPmNgInP	určit
k	k	k7c3	k
sekání	sekání	k1gNnSc3	sekání
<g/>
,	,	kIx,	,
šlehání	šlehání	k1gNnSc3	šlehání
či	či	k8xC	či
hnětení	hnětení	k1gNnSc3	hnětení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mixéry	mixér	k1gInPc1	mixér
na	na	k7c4	na
smoothie	smoothius	k1gMnPc4	smoothius
jsou	být	k5eAaImIp3nP	být
druh	druh	k1gInSc4	druh
stolního	stolní	k2eAgInSc2d1	stolní
mixéru	mixér	k1gInSc2	mixér
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
určeny	určit	k5eAaPmNgInP	určit
pro	pro	k7c4	pro
přípravu	příprava	k1gFnSc4	příprava
nápojů	nápoj	k1gInPc2	nápoj
z	z	k7c2	z
ovoce	ovoce	k1gNnSc2	ovoce
a	a	k8xC	a
zeleniny	zelenina	k1gFnSc2	zelenina
<g/>
.	.	kIx.	.
</s>
<s>
Potraviny	potravina	k1gFnPc1	potravina
rozmixují	rozmixovat	k5eAaPmIp3nP	rozmixovat
jemněji	jemně	k6eAd2	jemně
<g/>
.	.	kIx.	.
</s>
<s>
Mixovací	mixovací	k2eAgFnSc4d1	mixovací
nádobu	nádoba	k1gFnSc4	nádoba
lze	lze	k6eAd1	lze
odejmout	odejmout	k5eAaPmF	odejmout
a	a	k8xC	a
použít	použít	k5eAaPmF	použít
jako	jako	k8xS	jako
láhev	láhev	k1gFnSc4	láhev
na	na	k7c6	na
pití	pití	k1gNnSc6	pití
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Drtiče	drtič	k1gInPc1	drtič
a	a	k8xC	a
sekáčky	sekáček	k1gInPc1	sekáček
slouží	sloužit	k5eAaImIp3nP	sloužit
k	k	k7c3	k
nasekání	nasekání	k1gNnSc3	nasekání
cibule	cibule	k1gFnSc2	cibule
<g/>
,	,	kIx,	,
bylinek	bylinka	k1gFnPc2	bylinka
či	či	k8xC	či
masa	maso	k1gNnSc2	maso
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Standmixer	Standmixra	k1gFnPc2	Standmixra
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
