<p>
<s>
Lionel	Lionel	k1gInSc1	Lionel
Logue	Logu	k1gFnSc2	Logu
(	(	kIx(	(
<g/>
26	[number]	k4	26
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1880	[number]	k4	1880
<g/>
,	,	kIx,	,
Adelaide	Adelaid	k1gInSc5	Adelaid
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Austrálii	Austrálie	k1gFnSc6	Austrálie
–	–	k?	–
12	[number]	k4	12
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1953	[number]	k4	1953
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
známý	známý	k2eAgInSc1d1	známý
jako	jako	k8xC	jako
řečový	řečový	k2eAgMnSc1d1	řečový
terapeut	terapeut	k1gMnSc1	terapeut
<g/>
,	,	kIx,	,
pomohl	pomoct	k5eAaPmAgMnS	pomoct
odstranit	odstranit	k5eAaPmF	odstranit
vadu	vada	k1gFnSc4	vada
řeči	řeč	k1gFnSc2	řeč
anglickému	anglický	k2eAgMnSc3d1	anglický
králi	král	k1gMnSc3	král
Jiřímu	Jiří	k1gMnSc3	Jiří
VI	VI	kA	VI
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Původ	původ	k1gInSc1	původ
==	==	k?	==
</s>
</p>
<p>
<s>
Logue	Loguat	k5eAaPmIp3nS	Loguat
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
26	[number]	k4	26
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1880	[number]	k4	1880
v	v	k7c6	v
univerzitním	univerzitní	k2eAgNnSc6d1	univerzitní
městečku	městečko	k1gNnSc6	městečko
Adelaide	Adelaid	k1gInSc5	Adelaid
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Austrálii	Austrálie	k1gFnSc6	Austrálie
jako	jako	k8xC	jako
nejstarší	starý	k2eAgMnSc1d3	nejstarší
ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
Prince	princ	k1gMnSc2	princ
Alfred	Alfred	k1gMnSc1	Alfred
College	College	k1gFnSc1	College
a	a	k8xC	a
Elder	Elder	k1gMnSc1	Elder
Conservatorium	Conservatorium	k1gNnSc1	Conservatorium
of	of	k?	of
Music	Musice	k1gInPc2	Musice
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
žákem	žák	k1gMnSc7	žák
učitele	učitel	k1gMnSc2	učitel
řečnictví	řečnictví	k1gNnSc2	řečnictví
Edwarda	Edward	k1gMnSc2	Edward
Reevese	Reevese	k1gFnSc2	Reevese
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Počátky	počátek	k1gInPc1	počátek
kariéry	kariéra	k1gFnSc2	kariéra
-	-	kIx~	-
Austrálie	Austrálie	k1gFnSc2	Austrálie
==	==	k?	==
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
profesionální	profesionální	k2eAgFnSc1d1	profesionální
kariéra	kariéra	k1gFnSc1	kariéra
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
australském	australský	k2eAgInSc6d1	australský
Perthu	Perth	k1gInSc6	Perth
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
kromě	kromě	k7c2	kromě
výuky	výuka	k1gFnSc2	výuka
výslovnosti	výslovnost	k1gFnSc2	výslovnost
a	a	k8xC	a
řečnictví	řečnictví	k1gNnSc2	řečnictví
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
i	i	k9	i
obory	obora	k1gFnPc4	obora
herectví	herectví	k1gNnSc2	herectví
a	a	k8xC	a
umělecký	umělecký	k2eAgInSc4d1	umělecký
přednes	přednes	k1gInSc4	přednes
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc4	tento
obory	obor	k1gInPc4	obor
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c4	na
Elder	Elder	k1gInSc4	Elder
Conservatorium	Conservatorium	k1gNnSc4	Conservatorium
of	of	k?	of
Music	Musice	k1gFnPc2	Musice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1907	[number]	k4	1907
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Myrtle	Myrtle	k1gFnSc1	Myrtle
Gruenertovou	Gruenertová	k1gFnSc7	Gruenertová
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yIgFnSc7	který
měl	mít	k5eAaImAgMnS	mít
tři	tři	k4xCgMnPc4	tři
syny	syn	k1gMnPc4	syn
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
po	po	k7c6	po
svatbě	svatba	k1gFnSc6	svatba
se	se	k3xPyFc4	se
společně	společně	k6eAd1	společně
vydali	vydat	k5eAaPmAgMnP	vydat
na	na	k7c4	na
půlroční	půlroční	k2eAgNnSc4d1	půlroční
turné	turné	k1gNnSc4	turné
kolem	kolem	k7c2	kolem
světa	svět	k1gInSc2	svět
studovat	studovat	k5eAaImF	studovat
metody	metoda	k1gFnPc4	metoda
výuky	výuka	k1gFnSc2	výuka
řečnictví	řečnictví	k1gNnSc2	řečnictví
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
Logue	Logue	k1gNnSc2	Logue
uplatnil	uplatnit	k5eAaPmAgMnS	uplatnit
a	a	k8xC	a
také	také	k6eAd1	také
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
svou	svůj	k3xOyFgFnSc4	svůj
znalost	znalost	k1gFnSc4	znalost
léčby	léčba	k1gFnSc2	léčba
ztráty	ztráta	k1gFnSc2	ztráta
schopnosti	schopnost	k1gFnSc2	schopnost
mluvit	mluvit	k5eAaImF	mluvit
při	při	k7c6	při
terapii	terapie	k1gFnSc6	terapie
australských	australský	k2eAgMnPc2d1	australský
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
vracejících	vracející	k2eAgMnPc2d1	vracející
se	se	k3xPyFc4	se
z	z	k7c2	z
bojů	boj	k1gInPc2	boj
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
po	po	k7c6	po
traumatických	traumatický	k2eAgInPc6d1	traumatický
zážitcích	zážitek	k1gInPc6	zážitek
z	z	k7c2	z
bojů	boj	k1gInPc2	boj
či	či	k8xC	či
po	po	k7c6	po
zásahu	zásah	k1gInSc2	zásah
bojovým	bojový	k2eAgInSc7d1	bojový
plynem	plyn	k1gInSc7	plyn
přišli	přijít	k5eAaPmAgMnP	přijít
o	o	k7c4	o
schopnost	schopnost	k1gFnSc4	schopnost
mluvit	mluvit	k5eAaImF	mluvit
<g/>
.	.	kIx.	.
</s>
<s>
Tady	tady	k6eAd1	tady
Logue	Logue	k1gInSc1	Logue
získal	získat	k5eAaPmAgInS	získat
praktické	praktický	k2eAgFnPc4d1	praktická
zkušenosti	zkušenost	k1gFnPc4	zkušenost
léčby	léčba	k1gFnSc2	léčba
poruchy	porucha	k1gFnSc2	porucha
řeči	řeč	k1gFnSc2	řeč
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
metodu	metoda	k1gFnSc4	metoda
založil	založit	k5eAaPmAgMnS	založit
nejen	nejen	k6eAd1	nejen
na	na	k7c6	na
správném	správný	k2eAgNnSc6d1	správné
dýchání	dýchání	k1gNnSc6	dýchání
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k8xC	i
fyzickém	fyzický	k2eAgNnSc6d1	fyzické
cvičení	cvičení	k1gNnSc6	cvičení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
pacientům	pacient	k1gMnPc3	pacient
s	s	k7c7	s
dýcháním	dýchání	k1gNnSc7	dýchání
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
terapii	terapie	k1gFnSc4	terapie
Lionela	Lionel	k1gMnSc2	Lionel
Logua	Loguus	k1gMnSc2	Loguus
byl	být	k5eAaImAgInS	být
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
humor	humor	k1gInSc1	humor
<g/>
,	,	kIx,	,
nesmírná	smírný	k2eNgFnSc1d1	nesmírná
trpělivost	trpělivost	k1gFnSc1	trpělivost
a	a	k8xC	a
velká	velký	k2eAgFnSc1d1	velká
snaha	snaha	k1gFnSc1	snaha
o	o	k7c6	o
vytvoření	vytvoření	k1gNnSc6	vytvoření
důvěry	důvěra	k1gFnSc2	důvěra
mezi	mezi	k7c7	mezi
terapeutem	terapeut	k1gMnSc7	terapeut
a	a	k8xC	a
pacientem	pacient	k1gMnSc7	pacient
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Profesionální	profesionální	k2eAgFnSc1d1	profesionální
kariéra	kariéra	k1gFnSc1	kariéra
-	-	kIx~	-
Londýn	Londýn	k1gInSc1	Londýn
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
se	se	k3xPyFc4	se
Logue	Logue	k1gInSc1	Logue
vypravil	vypravit	k5eAaPmAgInS	vypravit
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
rodina	rodina	k1gFnSc1	rodina
plánovala	plánovat	k5eAaImAgFnS	plánovat
pouze	pouze	k6eAd1	pouze
krátký	krátký	k2eAgInSc4d1	krátký
<g/>
,	,	kIx,	,
dovolenkový	dovolenkový	k2eAgInSc4d1	dovolenkový
pobyt	pobyt	k1gInSc4	pobyt
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
zůstat	zůstat	k5eAaPmF	zůstat
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
známém	známý	k2eAgNnSc6d1	známé
lékařském	lékařský	k2eAgNnSc6d1	lékařské
centru	centrum	k1gNnSc6	centrum
–	–	k?	–
ulici	ulice	k1gFnSc3	ulice
Harley	harley	k1gInSc4	harley
Street	Street	k1gMnSc1	Street
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
otevřel	otevřít	k5eAaPmAgMnS	otevřít
ordinaci	ordinace	k1gFnSc4	ordinace
pro	pro	k7c4	pro
léčbu	léčba	k1gFnSc4	léčba
vad	vada	k1gFnPc2	vada
řeči	řeč	k1gFnSc2	řeč
<g/>
,	,	kIx,	,
učil	učit	k5eAaImAgInS	učit
také	také	k9	také
výslovnost	výslovnost	k1gFnSc4	výslovnost
na	na	k7c6	na
londýnských	londýnský	k2eAgFnPc6d1	londýnská
školách	škola	k1gFnPc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
metoda	metoda	k1gFnSc1	metoda
byla	být	k5eAaImAgFnS	být
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
a	a	k8xC	a
Longue	Longue	k1gInSc1	Longue
se	se	k3xPyFc4	se
stával	stávat	k5eAaImAgInS	stávat
známým	známý	k2eAgMnSc7d1	známý
řečovým	řečový	k2eAgMnSc7d1	řečový
terapeutem	terapeut	k1gMnSc7	terapeut
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Setkání	setkání	k1gNnSc1	setkání
s	s	k7c7	s
princem	princ	k1gMnSc7	princ
Albertem	Albert	k1gMnSc7	Albert
==	==	k?	==
</s>
</p>
<p>
<s>
Princ	princ	k1gMnSc1	princ
Albert	Albert	k1gMnSc1	Albert
<g/>
,	,	kIx,	,
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Yorku	York	k1gInSc2	York
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
obával	obávat	k5eAaImAgMnS	obávat
vystoupení	vystoupení	k1gNnSc4	vystoupení
na	na	k7c6	na
veřejnosti	veřejnost	k1gFnSc6	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Trpěl	trpět	k5eAaImAgMnS	trpět
těžkou	těžký	k2eAgFnSc7d1	těžká
vadou	vada	k1gFnSc7	vada
řeči	řeč	k1gFnSc2	řeč
<g/>
,	,	kIx,	,
silným	silný	k2eAgNnSc7d1	silné
koktáním	koktání	k1gNnSc7	koktání
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
následkem	následkem	k7c2	následkem
přeučování	přeučování	k1gNnSc2	přeučování
přirozeného	přirozený	k2eAgMnSc2d1	přirozený
leváka	levák	k1gMnSc2	levák
na	na	k7c6	na
používání	používání	k1gNnSc6	používání
pravé	pravý	k2eAgFnSc2d1	pravá
ruky	ruka	k1gFnSc2	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
veřejnosti	veřejnost	k1gFnSc6	veřejnost
prakticky	prakticky	k6eAd1	prakticky
nemohl	moct	k5eNaImAgMnS	moct
mluvit	mluvit	k5eAaImF	mluvit
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
závěrečná	závěrečný	k2eAgFnSc1d1	závěrečná
řeč	řeč	k1gFnSc1	řeč
na	na	k7c6	na
jubilejní	jubilejní	k2eAgFnSc6d1	jubilejní
výstavě	výstava	k1gFnSc6	výstava
britského	britský	k2eAgNnSc2d1	Britské
impéria	impérium	k1gNnSc2	impérium
ve	v	k7c4	v
Wembley	Wemblea	k1gFnPc4	Wemblea
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1925	[number]	k4	1925
se	se	k3xPyFc4	se
ukázala	ukázat	k5eAaPmAgNnP	ukázat
jako	jako	k9	jako
utrpení	utrpení	k1gNnPc1	utrpení
jak	jak	k8xS	jak
pro	pro	k7c4	pro
vévodu	vévoda	k1gMnSc4	vévoda
tak	tak	k9	tak
i	i	k9	i
pro	pro	k7c4	pro
posluchače	posluchač	k1gMnPc4	posluchač
<g/>
.	.	kIx.	.
</s>
<s>
Vévoda	vévoda	k1gMnSc1	vévoda
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
najít	najít	k5eAaPmF	najít
způsob	způsob	k1gInSc4	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
odstranit	odstranit	k5eAaPmF	odstranit
své	svůj	k3xOyFgNnSc4	svůj
koktání	koktání	k1gNnSc4	koktání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lord	lord	k1gMnSc1	lord
Stamfordham	Stamfordham	k1gInSc4	Stamfordham
doporučil	doporučit	k5eAaPmAgMnS	doporučit
vévodovi	vévoda	k1gMnSc6	vévoda
Lionela	Lionela	k1gFnSc1	Lionela
Logua	Logua	k1gFnSc1	Logua
<g/>
.	.	kIx.	.
</s>
<s>
Logue	Logue	k6eAd1	Logue
předepsal	předepsat	k5eAaPmAgMnS	předepsat
vévodovi	vévoda	k1gMnSc3	vévoda
denní	denní	k2eAgFnSc4d1	denní
hodinu	hodina	k1gFnSc4	hodina
cvičení	cvičení	k1gNnSc2	cvičení
a	a	k8xC	a
zpěvu	zpěv	k1gInSc2	zpěv
<g/>
,	,	kIx,	,
doporučil	doporučit	k5eAaPmAgInS	doporučit
relaxování	relaxování	k1gNnSc4	relaxování
<g/>
.	.	kIx.	.
</s>
<s>
Léčba	léčba	k1gFnSc1	léčba
byla	být	k5eAaImAgFnS	být
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1927	[number]	k4	1927
už	už	k6eAd1	už
vévoda	vévoda	k1gMnSc1	vévoda
mluvil	mluvit	k5eAaImAgMnS	mluvit
sebevědomě	sebevědomě	k6eAd1	sebevědomě
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc4	jeho
projev	projev	k1gInSc4	projev
na	na	k7c6	na
zahájení	zahájení	k1gNnSc6	zahájení
parlamentu	parlament	k1gInSc2	parlament
v	v	k7c6	v
Canbeře	Canbera	k1gFnSc6	Canbera
byl	být	k5eAaImAgInS	být
brilantní	brilantní	k2eAgInSc1d1	brilantní
<g/>
.	.	kIx.	.
</s>
<s>
Logue	Logue	k6eAd1	Logue
doprovázel	doprovázet	k5eAaImAgMnS	doprovázet
Alberta	Albert	k1gMnSc2	Albert
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
po	po	k7c6	po
abdikaci	abdikace	k1gFnSc6	abdikace
jeho	on	k3xPp3gMnSc2	on
bratra	bratr	k1gMnSc2	bratr
Eduarda	Eduard	k1gMnSc2	Eduard
(	(	kIx(	(
<g/>
abdikoval	abdikovat	k5eAaBmAgInS	abdikovat
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
vztah	vztah	k1gInSc4	vztah
ke	k	k7c3	k
dvakrát	dvakrát	k6eAd1	dvakrát
rozvedené	rozvedený	k2eAgFnSc3d1	rozvedená
Američance	Američanka	k1gFnSc3	Američanka
Wallis	Wallis	k1gFnSc2	Wallis
Simpsonové	Simpsonová	k1gFnSc2	Simpsonová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
král	král	k1gMnSc1	král
Jiří	Jiří	k1gMnSc1	Jiří
VI	VI	kA	VI
<g/>
.	.	kIx.	.
</s>
<s>
Přáteli	přítel	k1gMnPc7	přítel
zůstali	zůstat	k5eAaPmAgMnP	zůstat
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
svých	svůj	k3xOyFgInPc2	svůj
životů	život	k1gInPc2	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vnuk	vnuk	k1gMnSc1	vnuk
Lionela	Lionel	k1gMnSc2	Lionel
Logua	Logua	k1gMnSc1	Logua
Mark	Mark	k1gMnSc1	Mark
napsal	napsat	k5eAaBmAgMnS	napsat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Peterem	Peter	k1gMnSc7	Peter
Conradim	Conradi	k1gNnSc7	Conradi
knihu	kniha	k1gFnSc4	kniha
o	o	k7c6	o
vztahu	vztah	k1gInSc6	vztah
jeho	on	k3xPp3gMnSc2	on
dědečka	dědeček	k1gMnSc2	dědeček
a	a	k8xC	a
vévody	vévoda	k1gMnSc2	vévoda
z	z	k7c2	z
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
po	po	k7c6	po
abdikaci	abdikace	k1gFnSc6	abdikace
jeho	jeho	k3xOp3gMnSc2	jeho
staršího	starý	k2eAgMnSc2d2	starší
bratra	bratr	k1gMnSc2	bratr
Eduarda	Eduard	k1gMnSc2	Eduard
stal	stát	k5eAaPmAgInS	stát
králem	král	k1gMnSc7	král
Jiřím	Jiří	k1gMnSc7	Jiří
VI	VI	kA	VI
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
byla	být	k5eAaImAgFnS	být
použita	použít	k5eAaPmNgFnS	použít
jako	jako	k9	jako
předloha	předloha	k1gFnSc1	předloha
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
pro	pro	k7c4	pro
britský	britský	k2eAgInSc4d1	britský
film	film	k1gInSc4	film
The	The	k1gMnSc1	The
King	King	k1gMnSc1	King
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Speech	speech	k1gInSc1	speech
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
:	:	kIx,	:
Králova	Králův	k2eAgFnSc1d1	Králova
řeč	řeč	k1gFnSc1	řeč
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Scénář	scénář	k1gInSc4	scénář
napsal	napsat	k5eAaBmAgMnS	napsat
David	David	k1gMnSc1	David
Seidler	Seidler	k1gMnSc1	Seidler
<g/>
,	,	kIx,	,
Lionela	Lionel	k1gMnSc2	Lionel
Logua	Loguus	k1gMnSc2	Loguus
hrál	hrát	k5eAaImAgMnS	hrát
Geoffrey	Geoffrea	k1gMnSc2	Geoffrea
Rush	Rush	k1gMnSc1	Rush
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc4	jeho
pacienta	pacient	k1gMnSc4	pacient
<g/>
,	,	kIx,	,
vévodu	vévoda	k1gMnSc4	vévoda
z	z	k7c2	z
Yorku	York	k1gInSc2	York
Colin	Colin	k1gMnSc1	Colin
Firth	Firth	k1gMnSc1	Firth
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2011	[number]	k4	2011
The	The	k1gMnSc1	The
King	King	k1gMnSc1	King
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Speech	speech	k1gInSc1	speech
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
čtyři	čtyři	k4xCgMnPc4	čtyři
Oscary	Oscar	k1gMnPc4	Oscar
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
ceny	cena	k1gFnSc2	cena
pro	pro	k7c4	pro
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
film	film	k1gInSc4	film
a	a	k8xC	a
ceny	cena	k1gFnSc2	cena
pro	pro	k7c4	pro
nejlepšího	dobrý	k2eAgMnSc4d3	nejlepší
herce	herec	k1gMnSc4	herec
Colina	Colin	k1gMnSc4	Colin
Firtha	Firth	k1gMnSc4	Firth
v	v	k7c6	v
roli	role	k1gFnSc6	role
vévody	vévoda	k1gMnSc2	vévoda
z	z	k7c2	z
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
