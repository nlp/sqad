<s>
Finsko	Finsko	k1gNnSc1
na	na	k7c6
Letních	letní	k2eAgFnPc6d1
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
1928	#num#	k4
</s>
<s>
Finsko	Finsko	k1gNnSc1
na	na	k7c6
Letních	letní	k2eAgFnPc6d1
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
1928	#num#	k4
</s>
<s>
Vlajka	vlajka	k1gFnSc1
výpravyIOC	výpravyIOC	k?
kód	kód	k1gInSc4
</s>
<s>
FIN	Fin	k1gMnSc1
</s>
<s>
ZlatáStříbrnáBronzováCelkem	ZlatáStříbrnáBronzováCelek	k1gInSc7
</s>
<s>
8	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
25	#num#	k4
</s>
<s>
NOV	nov	k1gInSc1
</s>
<s>
Finský	finský	k2eAgInSc1d1
olympijský	olympijský	k2eAgInSc1d1
výbor	výbor	k1gInSc1
Účastníci	účastník	k1gMnPc1
</s>
<s>
69	#num#	k4
celkem	celkem	k6eAd1
(	(	kIx(
<g/>
67	#num#	k4
mužů	muž	k1gMnPc2
a	a	k8xC
2	#num#	k4
ženy	žena	k1gFnPc1
<g/>
)	)	kIx)
ve	v	k7c6
11	#num#	k4
sportech	sport	k1gInPc6
Vlajkonoš	vlajkonoš	k1gMnSc1
</s>
<s>
Akilles	Akilles	k1gInSc1
Järvinen	Järvinna	k1gFnPc2
</s>
<s>
Finsko	Finsko	k1gNnSc1
na	na	k7c6
Letních	letní	k2eAgFnPc6d1
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
1928	#num#	k4
v	v	k7c6
Amsterdamu	Amsterdam	k1gInSc6
reprezentovalo	reprezentovat	k5eAaImAgNnS
69	#num#	k4
sportovců	sportovec	k1gMnPc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
67	#num#	k4
mužů	muž	k1gMnPc2
a	a	k8xC
2	#num#	k4
ženy	žena	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejmladším	mladý	k2eAgNnSc7d3
účastníkem	účastník	k1gMnSc7
byl	být	k5eAaImAgInS
Ada	Ada	kA
Onnela	Onnela	k1gFnSc1
(	(	kIx(
<g/>
17	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
353	#num#	k4
dní	den	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nejstarší	starý	k2eAgMnSc1d3
pak	pak	k6eAd1
Bertel	Bertel	k1gMnSc1
Broman	Broman	k1gMnSc1
(	(	kIx(
<g/>
38	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
346	#num#	k4
dní	den	k1gInPc2
<g/>
)	)	kIx)
.	.	kIx.
</s>
<s desamb="1">
Reprezentanti	reprezentant	k1gMnPc1
vybojovali	vybojovat	k5eAaPmAgMnP
25	#num#	k4
medailí	medaile	k1gFnPc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
8	#num#	k4
zlatých	zlatá	k2eAgFnPc2d1
<g/>
,	,	kIx,
8	#num#	k4
stříbrných	stříbrný	k2eAgFnPc2d1
a	a	k8xC
9	#num#	k4
bronzových	bronzový	k2eAgFnPc2d1
<g/>
.	.	kIx.
</s>
<s>
Medailisté	medailista	k1gMnPc1
</s>
<s>
Medaile	medaile	k1gFnSc1
</s>
<s>
Jméno	jméno	k1gNnSc1
</s>
<s>
Sport	sport	k1gInSc1
</s>
<s>
Disciplína	disciplína	k1gFnSc1
</s>
<s>
ZlatoHarri	ZlatoHarri	k6eAd1
Larva	larva	k1gFnSc1
AtletikaMuži	AtletikaMuž	k1gFnSc6
běh	běh	k1gInSc1
na	na	k7c4
1	#num#	k4
500	#num#	k4
m	m	kA
</s>
<s>
ZlatoVille	ZlatoVille	k1gFnSc1
Ritola	Ritola	k1gFnSc1
AtletikaMuži	AtletikaMuž	k1gFnSc6
běh	běh	k1gInSc1
na	na	k7c4
5	#num#	k4
000	#num#	k4
m	m	kA
</s>
<s>
ZlatoPaavo	ZlatoPaava	k1gFnSc5
Nurmi	Nur	k1gFnPc7
AtletikaMuži	AtletikaMuž	k1gFnSc6
běh	běh	k1gInSc1
na	na	k7c4
10	#num#	k4
000	#num#	k4
m	m	kA
</s>
<s>
ZlatoToivo	ZlatoToivo	k1gNnSc1
Loukola	Loukola	k1gFnSc1
AtletikaMuži	AtletikaMuž	k1gFnSc6
běh	běh	k1gInSc1
na	na	k7c4
3000	#num#	k4
m	m	kA
překážek	překážka	k1gFnPc2
</s>
<s>
ZlatoPaavo	ZlatoPaava	k1gFnSc5
Yrjölä	Yrjölä	k1gFnSc5
AtletikaMuži	AtletikaMuž	k1gFnSc5
desetiboj	desetiboj	k1gInSc1
</s>
<s>
ZlatoVäinö	ZlatoVäinö	k?
Kokkinen	Kokkinen	k1gInSc4
Zápasřecko-římský	Zápasřecko-římský	k2eAgInSc4d1
do	do	k7c2
75	#num#	k4
kg	kg	kA
</s>
<s>
ZlatoKaarlo	ZlatoKaarlo	k1gNnSc4
Mäkinen	Mäkinen	k2eAgInSc1d1
Zápasvolný	Zápasvolný	k2eAgInSc1d1
styl	styl	k1gInSc1
nad	nad	k7c7
56	#num#	k4
kg	kg	kA
</s>
<s>
ZlatoArvo	ZlatoArvo	k6eAd1
Haavisto	Haavista	k1gMnSc5
Zápasvolný	Zápasvolný	k2eAgInSc1d1
styl	styl	k1gInSc4
do	do	k7c2
72	#num#	k4
kg	kg	kA
</s>
<s>
StříbroPaavo	StříbroPaava	k1gFnSc5
Nurmi	Nur	k1gFnPc7
AtletikaMuži	AtletikaMuž	k1gFnSc6
běh	běh	k1gInSc1
na	na	k7c4
5	#num#	k4
000	#num#	k4
m	m	kA
</s>
<s>
StříbroVille	StříbroVille	k1gFnSc1
Ritola	Ritola	k1gFnSc1
AtletikaMuži	AtletikaMuž	k1gFnSc6
běh	běh	k1gInSc1
na	na	k7c4
10	#num#	k4
000	#num#	k4
m	m	kA
</s>
<s>
StříbroPaavo	StříbroPaava	k1gFnSc5
Nurmi	Nur	k1gFnPc7
AtletikaMuži	AtletikaMuž	k1gFnSc6
běh	běh	k1gInSc1
na	na	k7c4
3000	#num#	k4
m	m	kA
překážek	překážka	k1gFnPc2
</s>
<s>
StříbroAntero	StříbroAntero	k1gNnSc1
Kivi	kivi	k1gMnSc1
AtletikaMuži	AtletikaMuž	k1gFnSc6
hod	hod	k1gInSc1
diskem	disk	k1gInSc7
</s>
<s>
StříbroAkilles	StříbroAkilles	k1gMnSc1
Järvinen	Järvinen	k1gInSc4
AtletikaMuži	AtletikaMuž	k1gFnSc3
desetiboj	desetiboj	k1gInSc1
</s>
<s>
StříbroHjalmar	StříbroHjalmar	k1gMnSc1
Nyström	Nyström	k1gInSc4
Zápasřecko-římský	Zápasřecko-římský	k2eAgInSc4d1
do	do	k7c2
82,5	82,5	k4
kg	kg	kA
</s>
<s>
StříbroKustaa	StříbroKustaa	k6eAd1
Pihlajamäki	Pihlajamäk	k1gFnSc3
Zápasvolný	Zápasvolný	k2eAgInSc4d1
styl	styl	k1gInSc4
do	do	k7c2
61	#num#	k4
kg	kg	kA
</s>
<s>
StříbroAukusti	StříbroAukust	k1gFnSc3
Sihvola	Sihvola	k1gFnSc1
Zápasvolný	Zápasvolný	k2eAgMnSc1d1
styl	styl	k1gInSc4
nad	nad	k7c4
87	#num#	k4
kg	kg	kA
</s>
<s>
BronzHeikki	BronzHeikki	k6eAd1
Savolainen	Savolainen	k2eAgMnSc1d1
Sportovní	sportovní	k2eAgMnSc1d1
gymnastikamuži	gymnastikamuzat	k5eAaPmIp1nS
kůň	kůň	k1gMnSc1
našíř	našíř	k6eAd1
</s>
<s>
BronzBertil	BronzBertit	k5eAaPmAgMnS
Broman	Broman	k1gMnSc1
Jachtingmuži	Jachtingmuž	k1gFnSc3
-	-	kIx~
třída	třída	k1gFnSc1
Finn	Finn	k1gMnSc1
</s>
<s>
BronzEino	BronzEino	k1gNnSc1
Purje	Purj	k1gFnSc2
AtletikaMuži	AtletikaMuž	k1gFnSc3
běh	běh	k1gInSc4
na	na	k7c4
1	#num#	k4
500	#num#	k4
m	m	kA
</s>
<s>
BronzMartti	BronzMartti	k1gNnSc1
Marttelin	Marttelina	k1gFnPc2
AtletikaMuži	AtletikaMuž	k1gFnSc3
maraton	maraton	k1gInSc4
</s>
<s>
BronzOve	BronzOvat	k5eAaPmIp3nS
Andersen	Andersen	k1gMnSc1
AtletikaMuži	AtletikaMuž	k1gFnSc6
běh	běh	k1gInSc1
na	na	k7c4
3000	#num#	k4
m	m	kA
překážek	překážka	k1gFnPc2
</s>
<s>
BronzVilho	BronzVilze	k6eAd1
Tuulos	Tuulos	k1gInSc4
AtletikaMuži	AtletikaMuž	k1gFnSc3
trojskok	trojskok	k1gInSc1
</s>
<s>
BronzEdvard	BronzEdvard	k1gMnSc1
Westerlund	Westerlund	k1gInSc4
Zápasřecko-římský	Zápasřecko-římský	k2eAgInSc4d1
do	do	k7c2
67,5	67,5	k4
kg	kg	kA
</s>
<s>
BronzOnni	BronzOneň	k1gFnSc3
Pellinen	Pellinen	k1gInSc4
Zápasřecko-římský	Zápasřecko-římský	k2eAgInSc4d1
do	do	k7c2
82,5	82,5	k4
kg	kg	kA
</s>
<s>
BronzEino	BronzEino	k1gNnSc4
Leino	Lein	k2eAgNnSc4d1
Zápasvolný	Zápasvolný	k2eAgInSc4d1
styl	styl	k1gInSc4
do	do	k7c2
66	#num#	k4
kg	kg	kA
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Finsko	Finsko	k1gNnSc1
na	na	k7c4
LOH	LOH	kA
1928	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Finsko	Finsko	k1gNnSc1
na	na	k7c6
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
Finsko	Finsko	k1gNnSc1
na	na	k7c4
LOH	LOH	kA
</s>
<s>
1906	#num#	k4
•	•	k?
1908	#num#	k4
•	•	k?
1912	#num#	k4
•	•	k?
1920	#num#	k4
•	•	k?
1924	#num#	k4
•	•	k?
1928	#num#	k4
•	•	k?
1932	#num#	k4
•	•	k?
1936	#num#	k4
•	•	k?
1948	#num#	k4
•	•	k?
1952	#num#	k4
•	•	k?
1956	#num#	k4
•	•	k?
1960	#num#	k4
•	•	k?
1964	#num#	k4
•	•	k?
1968	#num#	k4
•	•	k?
1972	#num#	k4
•	•	k?
1976	#num#	k4
•	•	k?
1980	#num#	k4
•	•	k?
1984	#num#	k4
•	•	k?
1988	#num#	k4
•	•	k?
1992	#num#	k4
•	•	k?
1996	#num#	k4
•	•	k?
2000	#num#	k4
•	•	k?
2004	#num#	k4
•	•	k?
2008	#num#	k4
•	•	k?
2012	#num#	k4
•	•	k?
2016	#num#	k4
•	•	k?
2020	#num#	k4
Finsko	Finsko	k1gNnSc4
na	na	k7c6
ZOH	ZOH	kA
</s>
<s>
1924	#num#	k4
•	•	k?
1928	#num#	k4
•	•	k?
1932	#num#	k4
•	•	k?
1936	#num#	k4
•	•	k?
1948	#num#	k4
•	•	k?
1952	#num#	k4
•	•	k?
1956	#num#	k4
•	•	k?
1960	#num#	k4
•	•	k?
1964	#num#	k4
•	•	k?
1968	#num#	k4
•	•	k?
1972	#num#	k4
•	•	k?
1976	#num#	k4
•	•	k?
1980	#num#	k4
•	•	k?
1984	#num#	k4
•	•	k?
1988	#num#	k4
•	•	k?
1992	#num#	k4
•	•	k?
1994	#num#	k4
•	•	k?
1998	#num#	k4
•	•	k?
2002	#num#	k4
•	•	k?
2006	#num#	k4
•	•	k?
2010	#num#	k4
•	•	k?
2014	#num#	k4
•	•	k?
2018	#num#	k4
•	•	k?
2022	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Finsko	Finsko	k1gNnSc1
|	|	kIx~
Sport	sport	k1gInSc1
</s>
