<p>
<s>
Howard	Howard	k1gMnSc1	Howard
Zinn	Zinn	k1gMnSc1	Zinn
(	(	kIx(	(
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1922	[number]	k4	1922
New	New	k1gFnPc2	New
York	York	k1gInSc4	York
–	–	k?	–
27	[number]	k4	27
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2010	[number]	k4	2010
Santa	Santa	k1gMnSc1	Santa
Monica	Monica	k1gMnSc1	Monica
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
historik	historik	k1gMnSc1	historik
<g/>
,	,	kIx,	,
socialista	socialista	k1gMnSc1	socialista
<g/>
,	,	kIx,	,
anarchista	anarchista	k1gMnSc1	anarchista
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
politických	politický	k2eAgFnPc2d1	politická
věd	věda	k1gFnPc2	věda
na	na	k7c6	na
Bostonské	bostonský	k2eAgFnSc6d1	Bostonská
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1964	[number]	k4	1964
až	až	k9	až
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
autorem	autor	k1gMnSc7	autor
více	hodně	k6eAd2	hodně
jak	jak	k6eAd1	jak
dvaceti	dvacet	k4xCc2	dvacet
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Dějiny	dějiny	k1gFnPc1	dějiny
lidu	lid	k1gInSc2	lid
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
(	(	kIx(	(
<g/>
A	a	k9	a
People	People	k1gFnSc1	People
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
History	Histor	k1gInPc7	Histor
of	of	k?	of
the	the	k?	the
United	United	k1gMnSc1	United
States	States	k1gMnSc1	States
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zinn	Zinn	k1gMnSc1	Zinn
se	se	k3xPyFc4	se
angažoval	angažovat	k5eAaBmAgMnS	angažovat
v	v	k7c6	v
ochraně	ochrana	k1gFnSc6	ochrana
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
<g/>
,	,	kIx,	,
svobod	svoboda	k1gFnPc2	svoboda
a	a	k8xC	a
protiválečného	protiválečný	k2eAgNnSc2d1	protiválečné
hnutí	hnutí	k1gNnSc2	hnutí
v	v	k7c6	v
USA	USA	kA	USA
–	–	k?	–
těmto	tento	k3xDgNnPc3	tento
tématům	téma	k1gNnPc3	téma
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgMnS	věnovat
i	i	k9	i
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
tvorbě	tvorba	k1gFnSc6	tvorba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Howard	Howard	k1gMnSc1	Howard
Zinn	Zinn	k1gMnSc1	Zinn
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Howard	Howarda	k1gFnPc2	Howarda
Zinn	Zinn	k1gNnSc4	Zinn
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Howard	Howard	k1gMnSc1	Howard
Zinn	Zinn	k1gMnSc1	Zinn
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Howard	Howarda	k1gFnPc2	Howarda
Zinn	Zinna	k1gFnPc2	Zinna
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
===	===	k?	===
Bibliografie	bibliografie	k1gFnSc2	bibliografie
===	===	k?	===
</s>
</p>
<p>
<s>
česky	česky	k6eAd1	česky
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
2013	[number]	k4	2013
<g/>
:	:	kIx,	:
Bomba	bomba	k1gFnSc1	bomba
<g/>
,	,	kIx,	,
Broken	Broken	k2eAgInSc1d1	Broken
Books	Books	k1gInSc1	Books
<g/>
:	:	kIx,	:
http://www.brokenbooks.cz	[url]	k1gInSc1	http://www.brokenbooks.cz
</s>
</p>
