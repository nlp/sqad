<s>
Howard	Howard	k1gMnSc1	Howard
Zinn	Zinn	k1gMnSc1	Zinn
(	(	kIx(	(
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1922	[number]	k4	1922
New	New	k1gFnPc2	New
York	York	k1gInSc4	York
–	–	k?	–
27	[number]	k4	27
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2010	[number]	k4	2010
Santa	Santa	k1gMnSc1	Santa
Monica	Monica	k1gMnSc1	Monica
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
historik	historik	k1gMnSc1	historik
<g/>
,	,	kIx,	,
socialista	socialista	k1gMnSc1	socialista
<g/>
,	,	kIx,	,
anarchista	anarchista	k1gMnSc1	anarchista
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
politických	politický	k2eAgFnPc2d1	politická
věd	věda	k1gFnPc2	věda
na	na	k7c6	na
Bostonské	bostonský	k2eAgFnSc6d1	Bostonská
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1964	[number]	k4	1964
až	až	k9	až
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
</s>
