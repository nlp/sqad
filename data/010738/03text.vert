<p>
<s>
Zbyněk	Zbyněk	k1gMnSc1	Zbyněk
Drda	Drda	k1gMnSc1	Drda
(	(	kIx(	(
<g/>
*	*	kIx~	*
29	[number]	k4	29
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1987	[number]	k4	1987
Ostrov	ostrov	k1gInSc1	ostrov
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
vítěz	vítěz	k1gMnSc1	vítěz
třetí	třetí	k4xOgFnSc2	třetí
řady	řada	k1gFnSc2	řada
televizní	televizní	k2eAgFnSc2d1	televizní
reality	realita	k1gFnSc2	realita
show	show	k1gFnSc2	show
Česko	Česko	k1gNnSc1	Česko
hledá	hledat	k5eAaImIp3nS	hledat
Superstar	superstar	k1gFnSc4	superstar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
česko-ruské	českouský	k2eAgFnSc2d1	česko-ruská
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
otec	otec	k1gMnSc1	otec
pracuje	pracovat	k5eAaImIp3nS	pracovat
v	v	k7c6	v
hotelovém	hotelový	k2eAgInSc6d1	hotelový
domě	dům	k1gInSc6	dům
v	v	k7c6	v
Karlových	Karlův	k2eAgInPc6d1	Karlův
Varech	Vary	k1gInPc6	Vary
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
Galina	Galina	k1gFnSc1	Galina
učí	učit	k5eAaImIp3nS	učit
ruštinu	ruština	k1gFnSc4	ruština
<g/>
;	;	kIx,	;
má	mít	k5eAaImIp3nS	mít
staršího	starý	k2eAgMnSc4d2	starší
bratra	bratr	k1gMnSc4	bratr
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
talentovaný	talentovaný	k2eAgMnSc1d1	talentovaný
na	na	k7c4	na
hudbu	hudba	k1gFnSc4	hudba
a	a	k8xC	a
sport	sport	k1gInSc4	sport
(	(	kIx(	(
<g/>
věnoval	věnovat	k5eAaPmAgMnS	věnovat
se	se	k3xPyFc4	se
karate	karate	k1gNnSc4	karate
<g/>
,	,	kIx,	,
volejbalu	volejbal	k1gInSc2	volejbal
a	a	k8xC	a
vodnímu	vodní	k2eAgNnSc3d1	vodní
záchranářství	záchranářství	k1gNnSc3	záchranářství
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Studoval	studovat	k5eAaImAgInS	studovat
Soukromou	soukromý	k2eAgFnSc4d1	soukromá
obchodní	obchodní	k2eAgFnSc4d1	obchodní
akademii	akademie	k1gFnSc4	akademie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
I	i	k9	i
když	když	k8xS	když
zpíval	zpívat	k5eAaImAgMnS	zpívat
od	od	k7c2	od
dětství	dětství	k1gNnSc2	dětství
<g/>
,	,	kIx,	,
známým	známý	k2eAgMnSc7d1	známý
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
<g/>
,	,	kIx,	,
až	až	k8xS	až
když	když	k8xS	když
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgInS	účastnit
třetího	třetí	k4xOgInSc2	třetí
ročníku	ročník	k1gInSc2	ročník
soutěže	soutěž	k1gFnSc2	soutěž
Česko	Česko	k1gNnSc1	Česko
hledá	hledat	k5eAaImIp3nS	hledat
Superstar	superstar	k1gFnSc4	superstar
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc4	třetí
řadu	řada	k1gFnSc4	řada
17	[number]	k4	17
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2006	[number]	k4	2006
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
vítězství	vítězství	k1gNnSc6	vítězství
natočil	natočit	k5eAaBmAgMnS	natočit
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
producenta	producent	k1gMnSc2	producent
Honzy	Honza	k1gMnSc2	Honza
Horáčka	Horáček	k1gMnSc2	Horáček
během	během	k7c2	během
tří	tři	k4xCgInPc2	tři
měsíců	měsíc	k1gInPc2	měsíc
desku	deska	k1gFnSc4	deska
Pohled	pohled	k1gInSc1	pohled
do	do	k7c2	do
očí	oko	k1gNnPc2	oko
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2007	[number]	k4	2007
a	a	k8xC	a
převzal	převzít	k5eAaPmAgMnS	převzít
za	za	k7c4	za
ni	on	k3xPp3gFnSc4	on
Desku	deska	k1gFnSc4	deska
roku	rok	k1gInSc2	rok
za	za	k7c4	za
nejprodávanější	prodávaný	k2eAgNnSc4d3	nejprodávanější
debutové	debutový	k2eAgNnSc4d1	debutové
album	album	k1gNnSc4	album
-	-	kIx~	-
aneb	aneb	k?	aneb
Objev	objev	k1gInSc1	objev
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nejvýznamnější	významný	k2eAgFnSc6d3	nejvýznamnější
české	český	k2eAgFnSc6d1	Česká
hudební	hudební	k2eAgFnSc6d1	hudební
anketě	anketa	k1gFnSc6	anketa
popularity	popularita	k1gFnSc2	popularita
Český	český	k2eAgMnSc1d1	český
slavík	slavík	k1gMnSc1	slavík
získal	získat	k5eAaPmAgMnS	získat
titul	titul	k1gInSc4	titul
Skokan	Skokan	k1gMnSc1	Skokan
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
když	když	k8xS	když
si	se	k3xPyFc3	se
proti	proti	k7c3	proti
předchozímu	předchozí	k2eAgInSc3d1	předchozí
ročníku	ročník	k1gInSc3	ročník
dokázal	dokázat	k5eAaPmAgInS	dokázat
polepšit	polepšit	k5eAaPmF	polepšit
o	o	k7c4	o
rekordních	rekordní	k2eAgFnPc2d1	rekordní
68	[number]	k4	68
příček	příčka	k1gFnPc2	příčka
<g/>
.	.	kIx.	.
</s>
<s>
Koncertuje	koncertovat	k5eAaImIp3nS	koncertovat
s	s	k7c7	s
kapelou	kapela	k1gFnSc7	kapela
a	a	k8xC	a
připravuje	připravovat	k5eAaImIp3nS	připravovat
novou	nový	k2eAgFnSc4d1	nová
desku	deska	k1gFnSc4	deska
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
vyjít	vyjít	k5eAaPmF	vyjít
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
2007	[number]	k4	2007
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Pohled	pohled	k1gInSc1	pohled
do	do	k7c2	do
očí	oko	k1gNnPc2	oko
<g/>
"	"	kIx"	"
BMG	BMG	kA	BMG
</s>
</p>
<p>
<s>
==	==	k?	==
Ocenění	ocenění	k1gNnSc1	ocenění
==	==	k?	==
</s>
</p>
<p>
<s>
17	[number]	k4	17
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
–	–	k?	–
Vítěz	vítěz	k1gMnSc1	vítěz
3	[number]	k4	3
<g/>
.	.	kIx.	.
řady	řada	k1gFnSc2	řada
Česko	Česko	k1gNnSc1	Česko
hledá	hledat	k5eAaImIp3nS	hledat
Superstar	superstar	k1gFnSc4	superstar
</s>
</p>
<p>
<s>
17	[number]	k4	17
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
2007	[number]	k4	2007
–	–	k?	–
"	"	kIx"	"
<g/>
TýTý	TýTý	k1gMnSc1	TýTý
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
Objev	objev	k1gInSc1	objev
roku	rok	k1gInSc2	rok
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Zpěvák	Zpěvák	k1gMnSc1	Zpěvák
</s>
</p>
<p>
<s>
2007	[number]	k4	2007
–	–	k?	–
"	"	kIx"	"
<g/>
Zlatý	zlatý	k2eAgMnSc1d1	zlatý
Otto	Otto	k1gMnSc1	Otto
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Newcomer	Newcomer	k1gInSc1	Newcomer
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
24	[number]	k4	24
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
2007	[number]	k4	2007
–	–	k?	–
Jetix	Jetix	k1gInSc1	Jetix
Kids	Kidsa	k1gFnPc2	Kidsa
Awards	Awardsa	k1gFnPc2	Awardsa
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Zpěvák	Zpěvák	k1gMnSc1	Zpěvák
</s>
</p>
<p>
<s>
0	[number]	k4	0
<g/>
8.12	[number]	k4	8.12
<g/>
.	.	kIx.	.
2007	[number]	k4	2007
-	-	kIx~	-
Skokan	Skokan	k1gMnSc1	Skokan
roku	rok	k1gInSc2	rok
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
Český	český	k2eAgMnSc1d1	český
slavík	slavík	k1gMnSc1	slavík
</s>
</p>
<p>
<s>
0	[number]	k4	0
<g/>
3.03	[number]	k4	3.03
<g/>
.	.	kIx.	.
2008	[number]	k4	2008
-	-	kIx~	-
Železná	železný	k2eAgFnSc1d1	železná
Česká	český	k2eAgFnSc1d1	Česká
dvanátka	dvanátka	k1gFnSc1	dvanátka
za	za	k7c4	za
singl	singl	k1gInSc4	singl
Srdce	srdce	k1gNnSc2	srdce
pátrají	pátrat	k5eAaImIp3nP	pátrat
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vedl	vést	k5eAaImAgInS	vést
v	v	k7c6	v
hitparádě	hitparáda	k1gFnSc6	hitparáda
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
Česká	český	k2eAgFnSc1d1	Česká
dvanáctka	dvanáctka	k1gFnSc1	dvanáctka
plných	plný	k2eAgInPc2d1	plný
41	[number]	k4	41
kol	kolo	k1gNnPc2	kolo
</s>
</p>
<p>
<s>
0	[number]	k4	0
<g/>
3.04	[number]	k4	3.04
<g/>
.	.	kIx.	.
2008	[number]	k4	2008
-	-	kIx~	-
Deska	deska	k1gFnSc1	deska
roku	rok	k1gInSc2	rok
za	za	k7c4	za
nejprodávanější	prodávaný	k2eAgNnSc4d3	nejprodávanější
debutové	debutový	k2eAgNnSc4d1	debutové
album	album	k1gNnSc4	album
interpreta	interpret	k1gMnSc2	interpret
-	-	kIx~	-
Objev	objev	k1gInSc1	objev
roku	rok	k1gInSc2	rok
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Použité	použitý	k2eAgInPc1d1	použitý
zdroje	zdroj	k1gInPc1	zdroj
===	===	k?	===
</s>
</p>
<p>
<s>
Zbyněk	Zbyněk	k1gMnSc1	Zbyněk
Drda	Drda	k1gMnSc1	Drda
-	-	kIx~	-
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Česko	Česko	k1gNnSc1	Česko
hledá	hledat	k5eAaImIp3nS	hledat
Superstar	superstar	k1gFnSc4	superstar
</s>
</p>
<p>
<s>
===	===	k?	===
Další	další	k2eAgInPc1d1	další
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Zbyněk	Zbyněk	k1gMnSc1	Zbyněk
Drda	Drda	k1gMnSc1	Drda
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
webové	webový	k2eAgFnPc1d1	webová
stránky	stránka	k1gFnPc1	stránka
Zbyňka	Zbyněk	k1gMnSc2	Zbyněk
Drdy	Drda	k1gMnSc2	Drda
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgInSc1d1	oficiální
fanklub	fanklub	k1gInSc1	fanklub
Zbyňka	Zbyněk	k1gMnSc2	Zbyněk
Drdy	Drda	k1gMnSc2	Drda
</s>
</p>
<p>
<s>
Česko	Česko	k1gNnSc1	Česko
hledá	hledat	k5eAaImIp3nS	hledat
Superstar	superstar	k1gFnSc4	superstar
2006	[number]	k4	2006
</s>
</p>
