<s>
Helsinky	Helsinky	k1gFnPc1	Helsinky
(	(	kIx(	(
<g/>
finsky	finsky	k6eAd1	finsky
Helsinki	Helsinki	k1gNnSc1	Helsinki
<g/>
,	,	kIx,	,
švédsky	švédsky	k6eAd1	švédsky
Helsingfors	Helsingfors	k1gInSc1	Helsingfors
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
hlavní	hlavní	k2eAgFnPc1d1	hlavní
a	a	k8xC	a
nejlidnatější	lidnatý	k2eAgNnSc1d3	nejlidnatější
město	město	k1gNnSc1	město
Finska	Finsko	k1gNnSc2	Finsko
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
největší	veliký	k2eAgInSc4d3	veliký
finský	finský	k2eAgInSc4d1	finský
přístav	přístav	k1gInSc4	přístav
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
založeny	založit	k5eAaPmNgFnP	založit
roku	rok	k1gInSc3	rok
1550	[number]	k4	1550
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
centrum	centrum	k1gNnSc1	centrum
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
malém	malé	k1gNnSc6	malé
poloostrovu	poloostrov	k1gInSc3	poloostrov
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
Finského	finský	k2eAgInSc2d1	finský
zálivu	záliv	k1gInSc2	záliv
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
ještě	ještě	k9	ještě
poměrně	poměrně	k6eAd1	poměrně
velké	velká	k1gFnSc3	velká
území	území	k1gNnSc6	území
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
východ	východ	k1gInSc4	východ
<g/>
.	.	kIx.	.
</s>
<s>
Rozkládají	rozkládat	k5eAaImIp3nP	rozkládat
se	se	k3xPyFc4	se
na	na	k7c4	na
715,55	[number]	k4	715,55
km2	km2	k4	km2
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
asi	asi	k9	asi
620	[number]	k4	620
982	[number]	k4	982
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
sousedními	sousední	k2eAgNnPc7d1	sousední
městy	město	k1gNnPc7	město
Vantaa	Vantaum	k1gNnSc2	Vantaum
<g/>
,	,	kIx,	,
Espoo	Espoo	k1gNnSc1	Espoo
a	a	k8xC	a
Kauniainen	Kauniainen	k1gInSc1	Kauniainen
tvoří	tvořit	k5eAaImIp3nS	tvořit
hustě	hustě	k6eAd1	hustě
propojené	propojený	k2eAgNnSc1d1	propojené
souměstí	souměstí	k1gNnSc1	souměstí
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
metropolitní	metropolitní	k2eAgFnSc1d1	metropolitní
oblast	oblast	k1gFnSc1	oblast
<g/>
)	)	kIx)	)
s	s	k7c7	s
miliónovou	miliónový	k2eAgFnSc7d1	miliónová
populací	populace	k1gFnSc7	populace
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
osmi	osm	k4xCc7	osm
dalšími	další	k2eAgNnPc7d1	další
městy	město	k1gNnPc7	město
tvoří	tvořit	k5eAaImIp3nS	tvořit
Velké	velký	k2eAgFnPc4d1	velká
Helsinky	Helsinky	k1gFnPc4	Helsinky
s	s	k7c7	s
1,3	[number]	k4	1,3
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
čtvrtina	čtvrtina	k1gFnSc1	čtvrtina
celého	celý	k2eAgNnSc2d1	celé
Finska	Finsko	k1gNnSc2	Finsko
<g/>
;	;	kIx,	;
Velké	velký	k2eAgFnPc1d1	velká
Helsinky	Helsinky	k1gFnPc1	Helsinky
produkují	produkovat	k5eAaImIp3nP	produkovat
třetinu	třetina	k1gFnSc4	třetina
finského	finský	k2eAgNnSc2d1	finské
HDP	HDP	kA	HDP
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
Finsko	Finsko	k1gNnSc4	Finsko
součástí	součást	k1gFnPc2	součást
Švédského	švédský	k2eAgNnSc2d1	švédské
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
Gustav	Gustav	k1gMnSc1	Gustav
I.	I.	kA	I.
Vasa	Vasa	k1gMnSc1	Vasa
založil	založit	k5eAaPmAgMnS	založit
Helsinky	Helsinky	k1gFnPc4	Helsinky
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
konkurovaly	konkurovat	k5eAaImAgFnP	konkurovat
hansovnímu	hansovní	k2eAgNnSc3d1	hansovní
městu	město	k1gNnSc3	město
Reval	Reval	k1gMnSc1	Reval
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgInSc1d1	dnešní
Tallinn	Tallinn	k1gInSc1	Tallinn
<g/>
)	)	kIx)	)
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
straně	strana	k1gFnSc6	strana
Finského	finský	k2eAgInSc2d1	finský
zálivu	záliv	k1gInSc2	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
datum	datum	k1gNnSc4	datum
jejich	jejich	k3xOp3gNnSc2	jejich
založení	založení	k1gNnSc2	založení
se	se	k3xPyFc4	se
pokládá	pokládat	k5eAaImIp3nS	pokládat
12	[number]	k4	12
<g/>
.	.	kIx.	.
červen	červen	k1gInSc1	červen
1550	[number]	k4	1550
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
král	král	k1gMnSc1	král
nakázal	nakázat	k5eAaBmAgMnS	nakázat
měšťanům	měšťan	k1gMnPc3	měšťan
z	z	k7c2	z
okolních	okolní	k2eAgFnPc2d1	okolní
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
do	do	k7c2	do
nového	nový	k2eAgNnSc2d1	nové
města	město	k1gNnSc2	město
přestěhovali	přestěhovat	k5eAaPmAgMnP	přestěhovat
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
Helsinky	Helsinky	k1gFnPc1	Helsinky
ležely	ležet	k5eAaImAgFnP	ležet
o	o	k7c4	o
několik	několik	k4yIc4	několik
kilometrů	kilometr	k1gInPc2	kilometr
severovýchodněji	severovýchodně	k6eAd2	severovýchodně
u	u	k7c2	u
ústí	ústí	k1gNnSc2	ústí
řeky	řeka	k1gFnSc2	řeka
Vantaa	Vanta	k1gInSc2	Vanta
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k1gNnSc1	místo
ale	ale	k8xC	ale
nebylo	být	k5eNaImAgNnS	být
zvoleno	zvolit	k5eAaPmNgNnS	zvolit
vhodně	vhodně	k6eAd1	vhodně
<g/>
,	,	kIx,	,
okolí	okolí	k1gNnSc1	okolí
bylo	být	k5eAaImAgNnS	být
plné	plný	k2eAgFnPc4d1	plná
bažin	bažina	k1gFnPc2	bažina
a	a	k8xC	a
nebylo	být	k5eNaImAgNnS	být
ani	ani	k8xC	ani
možno	možno	k6eAd1	možno
vybudovat	vybudovat	k5eAaPmF	vybudovat
velký	velký	k2eAgInSc4d1	velký
přístav	přístav	k1gInSc4	přístav
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
bylo	být	k5eAaImAgNnS	být
roku	rok	k1gInSc2	rok
1640	[number]	k4	1640
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
přesune	přesunout	k5eAaPmIp3nS	přesunout
na	na	k7c4	na
nynější	nynější	k2eAgNnSc4d1	nynější
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Švédský	švédský	k2eAgInSc1d1	švédský
název	název	k1gInSc1	název
Helsingfors	Helsingforsa	k1gFnPc2	Helsingforsa
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
původního	původní	k2eAgNnSc2d1	původní
místního	místní	k2eAgNnSc2d1	místní
jména	jméno	k1gNnSc2	jméno
Helsinge	Helsing	k1gMnSc2	Helsing
a	a	k8xC	a
švédského	švédský	k2eAgMnSc2d1	švédský
fors	fors	k6eAd1	fors
-	-	kIx~	-
peřej	peřej	k1gFnSc1	peřej
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
založení	založení	k1gNnSc6	založení
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
roku	rok	k1gInSc2	rok
1703	[number]	k4	1703
byl	být	k5eAaImAgInS	být
ruský	ruský	k2eAgInSc1d1	ruský
vliv	vliv	k1gInSc1	vliv
stále	stále	k6eAd1	stále
citelnější	citelný	k2eAgInSc1d2	citelnější
<g/>
.	.	kIx.	.
</s>
<s>
Vyvrcholením	vyvrcholení	k1gNnSc7	vyvrcholení
rusko-švédského	rusko-švédský	k2eAgNnSc2d1	rusko-švédský
soupeření	soupeření	k1gNnSc2	soupeření
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
velká	velký	k2eAgFnSc1d1	velká
severní	severní	k2eAgFnSc1d1	severní
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgFnP	být
Helsinky	Helsinky	k1gFnPc1	Helsinky
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1713	[number]	k4	1713
<g/>
-	-	kIx~	-
<g/>
1721	[number]	k4	1721
obsazeny	obsadit	k5eAaPmNgFnP	obsadit
ruským	ruský	k2eAgNnSc7d1	ruské
vojskem	vojsko	k1gNnSc7	vojsko
<g/>
,	,	kIx,	,
a	a	k8xC	a
opět	opět	k6eAd1	opět
za	za	k7c2	za
rusko-švédské	rusko-švédský	k2eAgFnSc2d1	rusko-švédská
války	válka	k1gFnSc2	válka
1741	[number]	k4	1741
<g/>
-	-	kIx~	-
<g/>
1743	[number]	k4	1743
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1748	[number]	k4	1748
se	se	k3xPyFc4	se
na	na	k7c4	na
obranu	obrana	k1gFnSc4	obrana
města	město	k1gNnSc2	město
začala	začít	k5eAaPmAgFnS	začít
na	na	k7c6	na
ostrůvcích	ostrůvek	k1gInPc6	ostrůvek
před	před	k7c7	před
přístavem	přístav	k1gInSc7	přístav
stavět	stavět	k5eAaImF	stavět
velká	velký	k2eAgFnSc1d1	velká
námořní	námořní	k2eAgFnSc1d1	námořní
pevnost	pevnost	k1gFnSc1	pevnost
Sveaborg	Sveaborg	k1gInSc1	Sveaborg
<g/>
,	,	kIx,	,
přezdívaná	přezdívaný	k2eAgFnSc1d1	přezdívaná
"	"	kIx"	"
<g/>
Gibraltar	Gibraltar	k1gInSc1	Gibraltar
severu	sever	k1gInSc2	sever
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
jako	jako	k8xC	jako
Suomenlinna	Suomenlinna	k1gFnSc1	Suomenlinna
oblíbeným	oblíbený	k2eAgNnSc7d1	oblíbené
výletním	výletní	k2eAgNnSc7d1	výletní
místem	místo	k1gNnSc7	místo
a	a	k8xC	a
turistickým	turistický	k2eAgNnSc7d1	turistické
lákadlem	lákadlo	k1gNnSc7	lákadlo
Helsinek	Helsinky	k1gFnPc2	Helsinky
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
Napoleon	Napoleon	k1gMnSc1	Napoleon
Bonaparte	bonapart	k1gInSc5	bonapart
a	a	k8xC	a
car	car	k1gMnSc1	car
Alexandr	Alexandr	k1gMnSc1	Alexandr
I.	I.	kA	I.
uzavřeli	uzavřít	k5eAaPmAgMnP	uzavřít
spojenectví	spojenectví	k1gNnSc2	spojenectví
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1808	[number]	k4	1808
vypovědělo	vypovědět	k5eAaPmAgNnS	vypovědět
proanglickému	proanglický	k2eAgInSc3d1	proanglický
Švédsku	Švédsko	k1gNnSc6	Švédsko
válku	válek	k1gInSc3	válek
<g/>
.	.	kIx.	.
</s>
<s>
Helsinky	Helsinky	k1gFnPc1	Helsinky
byly	být	k5eAaImAgFnP	být
okupovány	okupovat	k5eAaBmNgFnP	okupovat
ruským	ruský	k2eAgNnSc7d1	ruské
vojskem	vojsko	k1gNnSc7	vojsko
od	od	k7c2	od
prvních	první	k4xOgInPc2	první
dnů	den	k1gInPc2	den
války	válka	k1gFnSc2	válka
a	a	k8xC	a
pevnost	pevnost	k1gFnSc4	pevnost
po	po	k7c6	po
krátkém	krátký	k2eAgNnSc6d1	krátké
ostřelování	ostřelování	k1gNnSc6	ostřelování
kapitulovala	kapitulovat	k5eAaBmAgFnS	kapitulovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
uzavření	uzavření	k1gNnSc6	uzavření
míru	mír	k1gInSc2	mír
bylo	být	k5eAaImAgNnS	být
Finsko	Finsko	k1gNnSc1	Finsko
připojeno	připojen	k2eAgNnSc1d1	připojeno
k	k	k7c3	k
Rusku	Rusko	k1gNnSc3	Rusko
jako	jako	k8xS	jako
autonomní	autonomní	k2eAgNnSc4d1	autonomní
velkoknížectví	velkoknížectví	k1gNnSc4	velkoknížectví
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
dosavadní	dosavadní	k2eAgNnSc1d1	dosavadní
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Turku	turek	k1gInSc3	turek
bylo	být	k5eAaImAgNnS	být
příliš	příliš	k6eAd1	příliš
vzdáleno	vzdálit	k5eAaPmNgNnS	vzdálit
od	od	k7c2	od
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
<g/>
,	,	kIx,	,
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
Alexandr	Alexandr	k1gMnSc1	Alexandr
I.	I.	kA	I.
roku	rok	k1gInSc2	rok
1812	[number]	k4	1812
o	o	k7c6	o
jmenování	jmenování	k1gNnSc6	jmenování
Helsinek	Helsinky	k1gFnPc2	Helsinky
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Finska	Finsko	k1gNnSc2	Finsko
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
titulem	titul	k1gInSc7	titul
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
z	z	k7c2	z
Turku	turek	k1gInSc3	turek
přesunula	přesunout	k5eAaPmAgFnS	přesunout
i	i	k9	i
nejstarší	starý	k2eAgFnSc1d3	nejstarší
finská	finský	k2eAgFnSc1d1	finská
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
zničené	zničený	k2eAgNnSc1d1	zničené
město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
značně	značně	k6eAd1	značně
přestavěno	přestavěn	k2eAgNnSc1d1	přestavěno
<g/>
.	.	kIx.	.
</s>
<s>
Centrum	centrum	k1gNnSc1	centrum
získalo	získat	k5eAaPmAgNnS	získat
empírový	empírový	k2eAgInSc4d1	empírový
styl	styl	k1gInSc4	styl
<g/>
.	.	kIx.	.
</s>
<s>
Nejmonumentálnější	monumentální	k2eAgFnSc7d3	nejmonumentálnější
stavbou	stavba	k1gFnSc7	stavba
z	z	k7c2	z
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
je	být	k5eAaImIp3nS	být
evangelická	evangelický	k2eAgFnSc1d1	evangelická
katedrála	katedrála	k1gFnSc1	katedrála
dokončená	dokončený	k2eAgFnSc1d1	dokončená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1852	[number]	k4	1852
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
Helsinky	Helsinky	k1gFnPc1	Helsinky
rychle	rychle	k6eAd1	rychle
rostly	růst	k5eAaImAgFnP	růst
a	a	k8xC	a
staly	stát	k5eAaPmAgFnP	stát
se	se	k3xPyFc4	se
nejprůmyslovějším	průmyslový	k2eAgNnSc7d3	nejprůmyslovější
finským	finský	k2eAgNnSc7d1	finské
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
jejich	jejich	k3xOp3gMnPc2	jejich
obyvatel	obyvatel	k1gMnPc2	obyvatel
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
100	[number]	k4	100
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1862	[number]	k4	1862
byly	být	k5eAaImAgFnP	být
Helsinky	Helsinky	k1gFnPc1	Helsinky
spojeny	spojen	k2eAgFnPc1d1	spojena
železnicí	železnice	k1gFnSc7	železnice
s	s	k7c7	s
Hämeenlinnou	Hämeenlinný	k2eAgFnSc4d1	Hämeenlinný
na	na	k7c6	na
severu	sever	k1gInSc6	sever
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1870	[number]	k4	1870
také	také	k9	také
s	s	k7c7	s
Petrohradem	Petrohrad	k1gInSc7	Petrohrad
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgFnPc1d1	Nové
stavby	stavba	k1gFnPc1	stavba
vznikaly	vznikat	k5eAaImAgFnP	vznikat
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
západoevropské	západoevropský	k2eAgFnSc2d1	západoevropská
architektury	architektura	k1gFnSc2	architektura
a	a	k8xC	a
industrializace	industrializace	k1gFnSc2	industrializace
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
novorenesanční	novorenesanční	k2eAgNnSc1d1	novorenesanční
okolí	okolí	k1gNnSc1	okolí
tříd	třída	k1gFnPc2	třída
Esplanadi	Esplanad	k1gMnPc1	Esplanad
a	a	k8xC	a
Mannerheimintie	Mannerheimintie	k1gFnPc1	Mannerheimintie
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
přístavem	přístav	k1gInSc7	přístav
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1868	[number]	k4	1868
postaven	postaven	k2eAgInSc1d1	postaven
největší	veliký	k2eAgInSc1d3	veliký
pravoslavný	pravoslavný	k2eAgInSc1d1	pravoslavný
kostel	kostel	k1gInSc1	kostel
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
chrám	chrám	k1gInSc1	chrám
Zesnutí	zesnutí	k1gNnSc2	zesnutí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
:	:	kIx,	:
У	У	k?	У
с	с	k?	с
-	-	kIx~	-
Uspenskij	Uspenskij	k1gFnSc1	Uspenskij
sobor	sobor	k1gInSc1	sobor
<g/>
,	,	kIx,	,
finsky	finsky	k6eAd1	finsky
<g/>
:	:	kIx,	:
Uspenskin	Uspenskin	k1gInSc4	Uspenskin
katedraali	katedraat	k5eAaBmAgMnP	katedraat
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1890	[number]	k4	1890
pak	pak	k6eAd1	pak
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
finským	finský	k2eAgNnSc7d1	finské
národním	národní	k2eAgNnSc7d1	národní
obrozením	obrození	k1gNnSc7	obrození
překročil	překročit	k5eAaPmAgInS	překročit
počet	počet	k1gInSc1	počet
finsky	finsky	k6eAd1	finsky
mluvících	mluvící	k2eAgMnPc2d1	mluvící
obyvatel	obyvatel	k1gMnPc2	obyvatel
Helsinek	Helsinky	k1gFnPc2	Helsinky
počet	počet	k1gInSc1	počet
těch	ten	k3xDgFnPc2	ten
mluvících	mluvící	k2eAgFnPc2d1	mluvící
švédsky	švédsky	k6eAd1	švédsky
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
revolucích	revoluce	k1gFnPc6	revoluce
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
získalo	získat	k5eAaPmAgNnS	získat
Finsko	Finsko	k1gNnSc1	Finsko
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1917	[number]	k4	1917
samostatnost	samostatnost	k1gFnSc4	samostatnost
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
28	[number]	k4	28
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1918	[number]	k4	1918
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
finská	finský	k2eAgFnSc1d1	finská
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
Helsinky	Helsinky	k1gFnPc1	Helsinky
ihned	ihned	k6eAd1	ihned
obsadili	obsadit	k5eAaPmAgMnP	obsadit
rudí	rudý	k2eAgMnPc1d1	rudý
<g/>
;	;	kIx,	;
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
je	být	k5eAaImIp3nS	být
dobyly	dobýt	k5eAaPmAgFnP	dobýt
německé	německý	k2eAgFnPc4d1	německá
jednotky	jednotka	k1gFnPc4	jednotka
bojující	bojující	k2eAgFnPc4d1	bojující
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
bílých	bílý	k1gMnPc2	bílý
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Tampere	Tamper	k1gInSc5	Tamper
neutrpěly	utrpět	k5eNaPmAgInP	utrpět
Helsinky	Helsinky	k1gFnPc4	Helsinky
v	v	k7c6	v
bojích	boj	k1gInPc6	boj
větší	veliký	k2eAgFnSc2d2	veliký
škody	škoda	k1gFnSc2	škoda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Suomenlinně	Suomenlinně	k1gFnSc4	Suomenlinně
byl	být	k5eAaImAgInS	být
umístěn	umístit	k5eAaPmNgInS	umístit
největší	veliký	k2eAgInSc1d3	veliký
z	z	k7c2	z
táborů	tábor	k1gInPc2	tábor
pro	pro	k7c4	pro
zajaté	zajatý	k2eAgMnPc4d1	zajatý
rudé	rudý	k1gMnPc4	rudý
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
13	[number]	k4	13
tisíci	tisíc	k4xCgInPc7	tisíc
vězni	vězeň	k1gMnPc7	vězeň
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
konsolidaci	konsolidace	k1gFnSc6	konsolidace
poměrů	poměr	k1gInPc2	poměr
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
se	se	k3xPyFc4	se
Helsinky	Helsinky	k1gFnPc1	Helsinky
rychle	rychle	k6eAd1	rychle
rozvíjely	rozvíjet	k5eAaImAgFnP	rozvíjet
<g/>
.	.	kIx.	.
</s>
<s>
Architekturu	architektura	k1gFnSc4	architektura
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
především	především	k6eAd1	především
klasicismus	klasicismus	k1gInSc1	klasicismus
a	a	k8xC	a
funkcionalismus	funkcionalismus	k1gInSc1	funkcionalismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1934	[number]	k4	1934
<g/>
-	-	kIx~	-
<g/>
1938	[number]	k4	1938
byl	být	k5eAaImAgInS	být
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
helsinský	helsinský	k2eAgInSc1d1	helsinský
olympijský	olympijský	k2eAgInSc1d1	olympijský
stadión	stadión	k1gInSc1	stadión
pro	pro	k7c4	pro
letní	letní	k2eAgFnPc4d1	letní
olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
1940	[number]	k4	1940
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
však	však	k9	však
byly	být	k5eAaImAgInP	být
kvůli	kvůli	k7c3	kvůli
válce	válka	k1gFnSc3	válka
zrušeny	zrušen	k2eAgFnPc1d1	zrušena
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
Zimní	zimní	k2eAgFnSc2d1	zimní
a	a	k8xC	a
Pokračovací	pokračovací	k2eAgFnSc2d1	pokračovací
války	válka	k1gFnSc2	válka
Helsinky	Helsinky	k1gFnPc1	Helsinky
bombardovalo	bombardovat	k5eAaImAgNnS	bombardovat
sovětské	sovětský	k2eAgNnSc1d1	sovětské
letectvo	letectvo	k1gNnSc1	letectvo
<g/>
,	,	kIx,	,
nejintenzivněji	intenzivně	k6eAd3	intenzivně
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
díky	díky	k7c3	díky
dobré	dobrý	k2eAgFnSc3d1	dobrá
protivzdušné	protivzdušný	k2eAgFnSc3d1	protivzdušná
obraně	obrana	k1gFnSc3	obrana
dopadlo	dopadnout	k5eAaPmAgNnS	dopadnout
do	do	k7c2	do
obytných	obytný	k2eAgFnPc2d1	obytná
čtvrtí	čtvrt	k1gFnPc2	čtvrt
jen	jen	k6eAd1	jen
málo	málo	k4c1	málo
bomb	bomba	k1gFnPc2	bomba
<g/>
.	.	kIx.	.
</s>
<s>
Rozvoj	rozvoj	k1gInSc1	rozvoj
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
po	po	k7c6	po
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
ještě	ještě	k6eAd1	ještě
zrychlil	zrychlit	k5eAaPmAgMnS	zrychlit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
byly	být	k5eAaImAgInP	být
Helsinkám	Helsinky	k1gFnPc3	Helsinky
přiřčeny	přiřknout	k5eAaPmNgFnP	přiřknout
letní	letní	k2eAgFnPc1d1	letní
olympijské	olympijský	k2eAgFnPc1d1	olympijská
hry	hra	k1gFnPc1	hra
1952	[number]	k4	1952
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
rozsáhlé	rozsáhlý	k2eAgFnSc3d1	rozsáhlá
imigraci	imigrace	k1gFnSc3	imigrace
se	se	k3xPyFc4	se
vybudovala	vybudovat	k5eAaPmAgNnP	vybudovat
nová	nový	k2eAgNnPc1d1	nové
předměstí	předměstí	k1gNnPc1	předměstí
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejvýznamnějším	významný	k2eAgFnPc3d3	nejvýznamnější
moderním	moderní	k2eAgFnPc3d1	moderní
stavbám	stavba	k1gFnPc3	stavba
patří	patřit	k5eAaImIp3nS	patřit
velké	velký	k2eAgNnSc1d1	velké
kongresové	kongresový	k2eAgNnSc1d1	Kongresové
centrum	centrum	k1gNnSc1	centrum
Finlandia	Finlandium	k1gNnSc2	Finlandium
(	(	kIx(	(
<g/>
finsky	finsky	k6eAd1	finsky
<g/>
:	:	kIx,	:
Finlandia-talo	Finlandiaalo	k1gFnSc1	Finlandia-talo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
architekt	architekt	k1gMnSc1	architekt
Alvar	Alvar	k1gInSc4	Alvar
Aalto	Aalto	k1gNnSc1	Aalto
(	(	kIx(	(
<g/>
dokončené	dokončený	k2eAgNnSc1d1	dokončené
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nová	nový	k2eAgFnSc1d1	nová
budova	budova	k1gFnSc1	budova
Opery	opera	k1gFnSc2	opera
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Muzeum	muzeum	k1gNnSc1	muzeum
moderního	moderní	k2eAgNnSc2d1	moderní
umění	umění	k1gNnSc2	umění
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
zmínku	zmínka	k1gFnSc4	zmínka
též	též	k9	též
stojí	stát	k5eAaImIp3nP	stát
ojedinělý	ojedinělý	k2eAgInSc4d1	ojedinělý
skalní	skalní	k2eAgInSc4d1	skalní
kostel	kostel	k1gInSc4	kostel
Temppeliaukio	Temppeliaukio	k6eAd1	Temppeliaukio
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
plocha	plocha	k1gFnSc1	plocha
katastru	katastr	k1gInSc2	katastr
Helsinek	Helsinky	k1gFnPc2	Helsinky
je	být	k5eAaImIp3nS	být
715	[number]	k4	715
km2	km2	k4	km2
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
ale	ale	k8xC	ale
jen	jen	k6eAd1	jen
214	[number]	k4	214
km2	km2	k4	km2
je	být	k5eAaImIp3nS	být
pevná	pevný	k2eAgFnSc1d1	pevná
země	země	k1gFnSc1	země
a	a	k8xC	a
zbytek	zbytek	k1gInSc1	zbytek
vodní	vodní	k2eAgFnSc2d1	vodní
plochy	plocha	k1gFnSc2	plocha
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
moře	moře	k1gNnSc1	moře
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
pobřeží	pobřeží	k1gNnSc2	pobřeží
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
100	[number]	k4	100
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
městu	město	k1gNnSc3	město
náleží	náležet	k5eAaImIp3nS	náležet
315	[number]	k4	315
ostrovů	ostrov	k1gInPc2	ostrov
a	a	k8xC	a
ostrůvků	ostrůvek	k1gInPc2	ostrůvek
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
poloze	poloha	k1gFnSc3	poloha
u	u	k7c2	u
moře	moře	k1gNnSc2	moře
je	být	k5eAaImIp3nS	být
klima	klima	k1gNnSc1	klima
poměrně	poměrně	k6eAd1	poměrně
vyrovnané	vyrovnaný	k2eAgNnSc1d1	vyrovnané
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
extrémů	extrém	k1gInPc2	extrém
<g/>
:	:	kIx,	:
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
roční	roční	k2eAgFnSc1d1	roční
teplota	teplota	k1gFnSc1	teplota
je	být	k5eAaImIp3nS	být
5,6	[number]	k4	5,6
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
17	[number]	k4	17
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
v	v	k7c6	v
nejstudenějším	studený	k2eAgInSc6d3	nejstudenější
únoru	únor	k1gInSc6	únor
-5	-5	k4	-5
°	°	k?	°
<g/>
C.	C.	kA	C.
Jezera	jezero	k1gNnSc2	jezero
a	a	k8xC	a
moře	moře	k1gNnSc2	moře
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Helsinek	Helsinky	k1gFnPc2	Helsinky
pravidelně	pravidelně	k6eAd1	pravidelně
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
zamrzají	zamrzat	k5eAaImIp3nP	zamrzat
<g/>
.	.	kIx.	.
</s>
<s>
Roční	roční	k2eAgFnPc1d1	roční
srážky	srážka	k1gFnPc1	srážka
jsou	být	k5eAaImIp3nP	být
660	[number]	k4	660
mm	mm	kA	mm
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
(	(	kIx(	(
<g/>
červenec	červenec	k1gInSc1	červenec
<g/>
-	-	kIx~	-
<g/>
srpen	srpen	k1gInSc1	srpen
<g/>
)	)	kIx)	)
190	[number]	k4	190
mm	mm	kA	mm
a	a	k8xC	a
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
(	(	kIx(	(
<g/>
prosinec	prosinec	k1gInSc1	prosinec
<g/>
-	-	kIx~	-
<g/>
únor	únor	k1gInSc1	únor
<g/>
)	)	kIx)	)
140	[number]	k4	140
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Vítr	vítr	k1gInSc1	vítr
nejčastěji	často	k6eAd3	často
vane	vanout	k5eAaImIp3nS	vanout
z	z	k7c2	z
jihu	jih	k1gInSc2	jih
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
slunce	slunce	k1gNnSc2	slunce
svítí	svítit	k5eAaImIp3nS	svítit
přes	přes	k7c4	přes
270	[number]	k4	270
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
jen	jen	k9	jen
27	[number]	k4	27
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Helsinky	Helsinky	k1gFnPc1	Helsinky
jsou	být	k5eAaImIp3nP	být
turistickým	turistický	k2eAgInSc7d1	turistický
centrem	centr	k1gInSc7	centr
a	a	k8xC	a
většinou	většina	k1gFnSc7	většina
prvním	první	k4xOgNnSc7	první
místem	místo	k1gNnSc7	místo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
zahraniční	zahraniční	k2eAgMnPc1d1	zahraniční
turisté	turist	k1gMnPc1	turist
ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
navštíví	navštívit	k5eAaPmIp3nS	navštívit
<g/>
.	.	kIx.	.
</s>
<s>
Jádro	jádro	k1gNnSc1	jádro
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
bylo	být	k5eAaImAgNnS	být
vybudováno	vybudovat	k5eAaPmNgNnS	vybudovat
v	v	k7c6	v
empírovém	empírový	k2eAgInSc6d1	empírový
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
ostatních	ostatní	k2eAgFnPc2d1	ostatní
starších	starý	k2eAgFnPc2d2	starší
budov	budova	k1gFnPc2	budova
bylo	být	k5eAaImAgNnS	být
postaveno	postavit	k5eAaPmNgNnS	postavit
v	v	k7c6	v
neorenesančním	neorenesanční	k2eAgInSc6d1	neorenesanční
a	a	k8xC	a
neoklasicistním	neoklasicistní	k2eAgInSc6d1	neoklasicistní
slohu	sloh	k1gInSc6	sloh
<g/>
.	.	kIx.	.
</s>
<s>
Architekturu	architektura	k1gFnSc4	architektura
dnešních	dnešní	k2eAgFnPc2d1	dnešní
Helsinek	Helsinky	k1gFnPc2	Helsinky
značně	značně	k6eAd1	značně
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
funkcionalismus	funkcionalismus	k1gInSc1	funkcionalismus
a	a	k8xC	a
moderní	moderní	k2eAgNnSc1d1	moderní
umění	umění	k1gNnSc1	umění
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
mnoho	mnoho	k4c1	mnoho
zajímavých	zajímavý	k2eAgInPc2d1	zajímavý
kostelů	kostel	k1gInPc2	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnSc1d3	nejznámější
je	být	k5eAaImIp3nS	být
mohutná	mohutný	k2eAgFnSc1d1	mohutná
bílá	bílý	k2eAgFnSc1d1	bílá
lutheránská	lutheránský	k2eAgFnSc1d1	lutheránská
katedrála	katedrála	k1gFnSc1	katedrála
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Senaatintori	Senaatintor	k1gFnSc2	Senaatintor
<g/>
.	.	kIx.	.
</s>
<s>
Pravoslavná	pravoslavný	k2eAgFnSc1d1	pravoslavná
církev	církev	k1gFnSc1	církev
nedaleko	nedaleko	k7c2	nedaleko
od	od	k7c2	od
ní	on	k3xPp3gFnSc2	on
postavila	postavit	k5eAaPmAgFnS	postavit
velký	velký	k2eAgInSc4d1	velký
červený	červený	k2eAgInSc4d1	červený
chrám	chrám	k1gInSc4	chrám
Zesnutí	zesnutí	k1gNnSc2	zesnutí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
ve	v	k7c6	v
skalním	skalní	k2eAgInSc6d1	skalní
masivu	masiv	k1gInSc6	masiv
nedaleko	nedaleko	k7c2	nedaleko
finského	finský	k2eAgInSc2d1	finský
parlamentu	parlament	k1gInSc2	parlament
udivující	udivující	k2eAgInSc1d1	udivující
kostel	kostel	k1gInSc1	kostel
Temppeliaukio	Temppeliaukio	k6eAd1	Temppeliaukio
s	s	k7c7	s
excelentní	excelentní	k2eAgFnSc7d1	excelentní
akustikou	akustika	k1gFnSc7	akustika
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnSc7d3	nejznámější
turistickou	turistický	k2eAgFnSc7d1	turistická
atrakcí	atrakce	k1gFnSc7	atrakce
a	a	k8xC	a
oblíbeným	oblíbený	k2eAgNnSc7d1	oblíbené
místem	místo	k1gNnSc7	místo
na	na	k7c4	na
procházky	procházka	k1gFnPc4	procházka
je	být	k5eAaImIp3nS	být
bývalá	bývalý	k2eAgFnSc1d1	bývalá
námořní	námořní	k2eAgFnSc1d1	námořní
pevnost	pevnost	k1gFnSc1	pevnost
Suomenlinna	Suomenlinn	k1gInSc2	Suomenlinn
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
zapsaná	zapsaný	k2eAgFnSc1d1	zapsaná
na	na	k7c4	na
Seznam	seznam	k1gInSc4	seznam
kulturního	kulturní	k2eAgNnSc2d1	kulturní
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Opevněné	opevněný	k2eAgInPc1d1	opevněný
ostrovy	ostrov	k1gInPc1	ostrov
jsou	být	k5eAaImIp3nP	být
spojeny	spojit	k5eAaPmNgInP	spojit
s	s	k7c7	s
centrem	centr	k1gInSc7	centr
rychlým	rychlý	k2eAgInSc7d1	rychlý
trajektem	trajekt	k1gInSc7	trajekt
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Tržním	tržní	k2eAgNnSc6d1	tržní
náměstí	náměstí	k1gNnSc6	náměstí
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
socha	socha	k1gFnSc1	socha
Havis	Havis	k1gFnSc1	Havis
Amanda	Amanda	k1gFnSc1	Amanda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Kauppatori	Kauppator	k1gFnSc2	Kauppator
ve	v	k7c6	v
starém	starý	k2eAgInSc6d1	starý
přístavu	přístav	k1gInSc6	přístav
lze	lze	k6eAd1	lze
navštívit	navštívit	k5eAaPmF	navštívit
tradiční	tradiční	k2eAgInSc4d1	tradiční
trh	trh	k1gInSc4	trh
a	a	k8xC	a
nedaleko	daleko	k6eNd1	daleko
starou	starý	k2eAgFnSc4d1	stará
tržnici	tržnice	k1gFnSc4	tržnice
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
starého	starý	k2eAgInSc2d1	starý
přístavu	přístav	k1gInSc2	přístav
vyjíždějí	vyjíždět	k5eAaImIp3nP	vyjíždět
trajekty	trajekt	k1gInPc1	trajekt
na	na	k7c4	na
Korkeasaari	Korkeasaare	k1gFnSc4	Korkeasaare
a	a	k8xC	a
Suomenlinnu	Suomenlinna	k1gFnSc4	Suomenlinna
i	i	k8xC	i
okružní	okružní	k2eAgFnPc4d1	okružní
vyhlídkové	vyhlídkový	k2eAgFnPc4d1	vyhlídková
lodě	loď	k1gFnPc4	loď
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Korkeasaari	Korkeasaar	k1gFnSc2	Korkeasaar
je	být	k5eAaImIp3nS	být
zoo	zoo	k1gFnSc1	zoo
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
centra	centrum	k1gNnSc2	centrum
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
vozí	vozit	k5eAaImIp3nP	vozit
turisty	turist	k1gMnPc7	turist
také	také	k6eAd1	také
malý	malý	k2eAgInSc1d1	malý
trajekt	trajekt	k1gInSc1	trajekt
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
ostrov	ostrov	k1gInSc1	ostrov
Seurasaari	Seurasaar	k1gFnSc2	Seurasaar
se	s	k7c7	s
skanzenem	skanzen	k1gInSc7	skanzen
<g/>
.	.	kIx.	.
</s>
<s>
Nejpopulárnější	populární	k2eAgInSc1d3	nejpopulárnější
park	park	k1gInSc1	park
je	být	k5eAaImIp3nS	být
Kaivopuisto	Kaivopuista	k1gMnSc5	Kaivopuista
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
oblíbená	oblíbený	k2eAgFnSc1d1	oblíbená
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
i	i	k8xC	i
pláž	pláž	k1gFnSc1	pláž
Hietaniemi	Hietanie	k1gFnPc7	Hietanie
nedaleko	nedaleko	k7c2	nedaleko
centra	centrum	k1gNnSc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Městskou	městský	k2eAgFnSc7d1	městská
hromadnou	hromadný	k2eAgFnSc7d1	hromadná
dopravou	doprava	k1gFnSc7	doprava
je	být	k5eAaImIp3nS	být
dostupný	dostupný	k2eAgInSc1d1	dostupný
národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
Nuuksio	Nuuksio	k6eAd1	Nuuksio
plný	plný	k2eAgInSc1d1	plný
jezer	jezero	k1gNnPc2	jezero
<g/>
,	,	kIx,	,
skal	skála	k1gFnPc2	skála
<g/>
,	,	kIx,	,
bažin	bažina	k1gFnPc2	bažina
a	a	k8xC	a
lesů	les	k1gInPc2	les
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
31	[number]	k4	31
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc3	leden
2012	[number]	k4	2012
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
Helsinkách	Helsinky	k1gFnPc6	Helsinky
596	[number]	k4	596
233	[number]	k4	233
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Podíl	podíl	k1gInSc1	podíl
žen	žena	k1gFnPc2	žena
je	být	k5eAaImIp3nS	být
53	[number]	k4	53
%	%	kIx~	%
a	a	k8xC	a
mužů	muž	k1gMnPc2	muž
47	[number]	k4	47
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Finsky	finsky	k6eAd1	finsky
mluví	mluvit	k5eAaImIp3nS	mluvit
82,5	[number]	k4	82,5
%	%	kIx~	%
<g/>
,	,	kIx,	,
švédsky	švédsky	k6eAd1	švédsky
6	[number]	k4	6
%	%	kIx~	%
a	a	k8xC	a
jiným	jiný	k2eAgInSc7d1	jiný
jazykem	jazyk	k1gInSc7	jazyk
11,5	[number]	k4	11,5
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
produktivním	produktivní	k2eAgInSc6d1	produktivní
věku	věk	k1gInSc6	věk
je	být	k5eAaImIp3nS	být
423	[number]	k4	423
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
71	[number]	k4	71
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dětí	dítě	k1gFnPc2	dítě
je	být	k5eAaImIp3nS	být
81	[number]	k4	81
000	[number]	k4	000
(	(	kIx(	(
<g/>
13,5	[number]	k4	13,5
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
délka	délka	k1gFnSc1	délka
dožití	dožití	k1gNnSc2	dožití
je	být	k5eAaImIp3nS	být
75	[number]	k4	75
let	léto	k1gNnPc2	léto
pro	pro	k7c4	pro
muže	muž	k1gMnPc4	muž
a	a	k8xC	a
82	[number]	k4	82
let	léto	k1gNnPc2	léto
pro	pro	k7c4	pro
ženy	žena	k1gFnPc4	žena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
se	se	k3xPyFc4	se
v	v	k7c6	v
Helsinkách	Helsinky	k1gFnPc6	Helsinky
narodilo	narodit	k5eAaPmAgNnS	narodit
6	[number]	k4	6
065	[number]	k4	065
dětí	dítě	k1gFnPc2	dítě
(	(	kIx(	(
<g/>
v	v	k7c6	v
souměstí	souměstí	k1gNnSc6	souměstí
15	[number]	k4	15
0	[number]	k4	0
<g/>
10	[number]	k4	10
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
5	[number]	k4	5
176	[number]	k4	176
(	(	kIx(	(
<g/>
8814	[number]	k4	8814
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přistěhovalo	přistěhovat	k5eAaPmAgNnS	přistěhovat
se	se	k3xPyFc4	se
32	[number]	k4	32
064	[number]	k4	064
(	(	kIx(	(
<g/>
35	[number]	k4	35
507	[number]	k4	507
<g/>
)	)	kIx)	)
a	a	k8xC	a
odstěhovalo	odstěhovat	k5eAaPmAgNnS	odstěhovat
32	[number]	k4	32
940	[number]	k4	940
(	(	kIx(	(
<g/>
31	[number]	k4	31
310	[number]	k4	310
<g/>
)	)	kIx)	)
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Helsinkách	Helsinky	k1gFnPc6	Helsinky
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
300	[number]	k4	300
000	[number]	k4	000
domácností	domácnost	k1gFnPc2	domácnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
78	[number]	k4	78
%	%	kIx~	%
domácnostech	domácnost	k1gFnPc6	domácnost
žije	žít	k5eAaImIp3nS	žít
jen	jen	k9	jen
jeden	jeden	k4xCgMnSc1	jeden
nebo	nebo	k8xC	nebo
dva	dva	k4xCgMnPc1	dva
lidé	člověk	k1gMnPc1	člověk
<g/>
.	.	kIx.	.
46	[number]	k4	46
%	%	kIx~	%
domácností	domácnost	k1gFnPc2	domácnost
si	se	k3xPyFc3	se
byt	byt	k1gInSc4	byt
pronajímá	pronajímat	k5eAaImIp3nS	pronajímat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jednoho	jeden	k4xCgMnSc4	jeden
obyvatele	obyvatel	k1gMnSc4	obyvatel
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
připadá	připadat	k5eAaPmIp3nS	připadat
33	[number]	k4	33
m2	m2	k4	m2
bytu	byt	k1gInSc2	byt
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
měsíční	měsíční	k2eAgInSc1d1	měsíční
nájem	nájem	k1gInSc1	nájem
dvoupokojového	dvoupokojový	k2eAgInSc2d1	dvoupokojový
bytu	byt	k1gInSc2	byt
je	být	k5eAaImIp3nS	být
520	[number]	k4	520
€	€	k?	€
<g/>
;	;	kIx,	;
ceny	cena	k1gFnSc2	cena
starších	starý	k2eAgInPc2d2	starší
bytů	byt	k1gInPc2	byt
jsou	být	k5eAaImIp3nP	být
2	[number]	k4	2
260	[number]	k4	260
€	€	k?	€
za	za	k7c4	za
m2	m2	k4	m2
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
helsinských	helsinský	k2eAgFnPc6d1	Helsinská
nemocnicích	nemocnice	k1gFnPc6	nemocnice
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
2500	[number]	k4	2500
lůžek	lůžko	k1gNnPc2	lůžko
<g/>
.	.	kIx.	.
</s>
<s>
Denně	denně	k6eAd1	denně
přijde	přijít	k5eAaPmIp3nS	přijít
na	na	k7c6	na
pohotovosti	pohotovost	k1gFnSc6	pohotovost
asi	asi	k9	asi
300	[number]	k4	300
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
1	[number]	k4	1
100	[number]	k4	100
lidí	člověk	k1gMnPc2	člověk
jde	jít	k5eAaImIp3nS	jít
k	k	k7c3	k
zubaři	zubař	k1gMnSc3	zubař
<g/>
,	,	kIx,	,
41	[number]	k4	41
k	k	k7c3	k
psychiatrovi	psychiatr	k1gMnSc3	psychiatr
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
60	[number]	k4	60
lidí	člověk	k1gMnPc2	člověk
s	s	k7c7	s
psychiatrem	psychiatr	k1gMnSc7	psychiatr
konzultuje	konzultovat	k5eAaImIp3nS	konzultovat
nějaký	nějaký	k3yIgInSc4	nějaký
problém	problém	k1gInSc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
žije	žít	k5eAaImIp3nS	žít
1	[number]	k4	1
600	[number]	k4	600
mentálně	mentálně	k6eAd1	mentálně
postižených	postižený	k2eAgMnPc2d1	postižený
<g/>
.	.	kIx.	.
</s>
<s>
Nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
asi	asi	k9	asi
6	[number]	k4	6
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
85	[number]	k4	85
%	%	kIx~	%
pracovních	pracovní	k2eAgNnPc2d1	pracovní
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Helsinky	Helsinky	k1gFnPc1	Helsinky
náleží	náležet	k5eAaImIp3nP	náležet
do	do	k7c2	do
provincie	provincie	k1gFnSc2	provincie
Uusimaa	Uusima	k1gInSc2	Uusima
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Finska	Finsko	k1gNnSc2	Finsko
jsou	být	k5eAaImIp3nP	být
sídlem	sídlo	k1gNnSc7	sídlo
mnoha	mnoho	k4c2	mnoho
státních	státní	k2eAgFnPc2d1	státní
institucí	instituce	k1gFnPc2	instituce
<g/>
.	.	kIx.	.
</s>
<s>
Helsinské	helsinský	k2eAgNnSc1d1	Helsinské
zastupitelstvo	zastupitelstvo	k1gNnSc1	zastupitelstvo
má	mít	k5eAaImIp3nS	mít
85	[number]	k4	85
členů	člen	k1gInPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
Volby	volba	k1gFnPc1	volba
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
v	v	k7c6	v
přestupných	přestupný	k2eAgNnPc6d1	přestupné
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Rada	rada	k1gFnSc1	rada
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
starostu	starosta	k1gMnSc4	starosta
na	na	k7c4	na
sedmileté	sedmiletý	k2eAgNnSc4d1	sedmileté
období	období	k1gNnSc4	období
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Helsinkách	Helsinky	k1gFnPc6	Helsinky
je	být	k5eAaImIp3nS	být
osm	osm	k4xCc1	osm
univerzit	univerzita	k1gFnPc2	univerzita
se	s	k7c7	s
63000	[number]	k4	63000
studenty	student	k1gMnPc7	student
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
největší	veliký	k2eAgFnPc1d3	veliký
jsou	být	k5eAaImIp3nP	být
Helsinská	helsinský	k2eAgFnSc1d1	Helsinská
univerzita	univerzita	k1gFnSc1	univerzita
[	[	kIx(	[
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
a	a	k8xC	a
Helsinská	helsinský	k2eAgFnSc1d1	Helsinská
technická	technický	k2eAgFnSc1d1	technická
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c4	v
Espoo	Espoo	k1gNnSc4	Espoo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Helsinkách	Helsinky	k1gFnPc6	Helsinky
chodí	chodit	k5eAaImIp3nS	chodit
do	do	k7c2	do
školy	škola	k1gFnSc2	škola
66	[number]	k4	66
000	[number]	k4	000
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
50	[number]	k4	50
%	%	kIx~	%
předškolních	předškolní	k2eAgFnPc2d1	předškolní
dětí	dítě	k1gFnPc2	dítě
chodí	chodit	k5eAaImIp3nP	chodit
do	do	k7c2	do
školky	školka	k1gFnSc2	školka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
70	[number]	k4	70
dětských	dětský	k2eAgNnPc2d1	dětské
hřišť	hřiště	k1gNnPc2	hřiště
<g/>
.	.	kIx.	.
</s>
<s>
Metro	metro	k1gNnSc1	metro
v	v	k7c6	v
Helsinkách	Helsinky	k1gFnPc6	Helsinky
má	mít	k5eAaImIp3nS	mít
jednu	jeden	k4xCgFnSc4	jeden
rozvětvenou	rozvětvený	k2eAgFnSc4d1	rozvětvená
linku	linka	k1gFnSc4	linka
se	s	k7c7	s
17	[number]	k4	17
stanicemi	stanice	k1gFnPc7	stanice
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
je	být	k5eAaImIp3nS	být
jen	jen	k6eAd1	jen
šest	šest	k4xCc1	šest
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
podpovrchových	podpovrchový	k2eAgInPc2d1	podpovrchový
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc4	město
protkávají	protkávat	k5eAaImIp3nP	protkávat
koleje	kolej	k1gFnSc2	kolej
patnácti	patnáct	k4xCc2	patnáct
tramvajových	tramvajový	k2eAgFnPc2d1	tramvajová
linek	linka	k1gFnPc2	linka
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc1	zbytek
veřejné	veřejný	k2eAgFnSc2d1	veřejná
dopravy	doprava	k1gFnSc2	doprava
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
autobusy	autobus	k1gInPc1	autobus
(	(	kIx(	(
<g/>
100	[number]	k4	100
linek	linka	k1gFnPc2	linka
<g/>
)	)	kIx)	)
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
také	také	k9	také
přívozy	přívoz	k1gInPc1	přívoz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
souměstí	souměstí	k1gNnSc2	souměstí
jezdí	jezdit	k5eAaImIp3nP	jezdit
také	také	k9	také
rychlé	rychlý	k2eAgInPc4d1	rychlý
meziměstské	meziměstský	k2eAgInPc4d1	meziměstský
vlaky	vlak	k1gInPc4	vlak
<g/>
.	.	kIx.	.
</s>
<s>
Letecká	letecký	k2eAgFnSc1d1	letecká
doprava	doprava	k1gFnSc1	doprava
využívá	využívat	k5eAaPmIp3nS	využívat
dvě	dva	k4xCgNnPc4	dva
letiště	letiště	k1gNnPc4	letiště
-	-	kIx~	-
mezinárodní	mezinárodní	k2eAgNnPc4d1	mezinárodní
Vantaa	Vantaum	k1gNnPc4	Vantaum
severně	severně	k6eAd1	severně
od	od	k7c2	od
města	město	k1gNnSc2	město
s	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
terminály	terminál	k1gInPc7	terminál
<g/>
,	,	kIx,	,
sloužící	sloužící	k2eAgNnSc1d1	sloužící
ročně	ročně	k6eAd1	ročně
více	hodně	k6eAd2	hodně
než	než	k8xS	než
13	[number]	k4	13
milionům	milion	k4xCgInPc3	milion
pasažérů	pasažér	k1gMnPc2	pasažér
<g/>
,	,	kIx,	,
a	a	k8xC	a
starší	starší	k1gMnPc4	starší
Malmi	Mal	k1gFnPc7	Mal
<g/>
.	.	kIx.	.
</s>
<s>
Přímé	přímý	k2eAgFnPc1d1	přímá
linky	linka	k1gFnPc1	linka
spojují	spojovat	k5eAaImIp3nP	spojovat
Helsinky	Helsinky	k1gFnPc4	Helsinky
nejen	nejen	k6eAd1	nejen
s	s	k7c7	s
celou	celý	k2eAgFnSc7d1	celá
Evropou	Evropa	k1gFnSc7	Evropa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k6eAd1	také
s	s	k7c7	s
USA	USA	kA	USA
a	a	k8xC	a
východní	východní	k2eAgFnSc7d1	východní
Asií	Asie	k1gFnSc7	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
přístavních	přístavní	k2eAgInPc2d1	přístavní
terminálů	terminál	k1gInPc2	terminál
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
odbavení	odbavení	k1gNnSc1	odbavení
cestujících	cestující	k1gMnPc2	cestující
do	do	k7c2	do
a	a	k8xC	a
ze	z	k7c2	z
Stockholmu	Stockholm	k1gInSc2	Stockholm
a	a	k8xC	a
Tallinnu	Tallinn	k1gInSc2	Tallinn
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
rychlým	rychlý	k2eAgInSc7d1	rychlý
člunem	člun	k1gInSc7	člun
do	do	k7c2	do
Tallinnu	Tallinn	k1gInSc2	Tallinn
trvá	trvat	k5eAaImIp3nS	trvat
asi	asi	k9	asi
hodinu	hodina	k1gFnSc4	hodina
a	a	k8xC	a
půl	půl	k1xP	půl
<g/>
,	,	kIx,	,
velkým	velký	k2eAgInSc7d1	velký
trajektem	trajekt	k1gInSc7	trajekt
asi	asi	k9	asi
tři	tři	k4xCgFnPc4	tři
hodiny	hodina	k1gFnPc4	hodina
<g/>
,	,	kIx,	,
do	do	k7c2	do
Stockholmu	Stockholm	k1gInSc2	Stockholm
velké	velký	k2eAgInPc1d1	velký
trajekty	trajekt	k1gInPc1	trajekt
plují	plout	k5eAaImIp3nP	plout
asi	asi	k9	asi
dvanáct	dvanáct	k4xCc4	dvanáct
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Cestující	cestující	k1gMnPc1	cestující
do	do	k7c2	do
Tallinnu	Tallinn	k1gInSc2	Tallinn
mohou	moct	k5eAaImIp3nP	moct
také	také	k9	také
využít	využít	k5eAaPmF	využít
pravidelnou	pravidelný	k2eAgFnSc4d1	pravidelná
vrtulníkovou	vrtulníkový	k2eAgFnSc4d1	vrtulníková
linku	linka	k1gFnSc4	linka
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
trvá	trvat	k5eAaImIp3nS	trvat
pouhých	pouhý	k2eAgInPc2d1	pouhý
18	[number]	k4	18
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Spojení	spojení	k1gNnSc1	spojení
se	s	k7c7	s
zbytkem	zbytek	k1gInSc7	zbytek
Finska	Finsko	k1gNnSc2	Finsko
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
síť	síť	k1gFnSc1	síť
dálkových	dálkový	k2eAgInPc2d1	dálkový
autobusů	autobus	k1gInPc2	autobus
a	a	k8xC	a
rychlých	rychlý	k2eAgInPc2d1	rychlý
vlaků	vlak	k1gInPc2	vlak
<g/>
.	.	kIx.	.
</s>
<s>
Vlakem	vlak	k1gInSc7	vlak
Pendolino	Pendolina	k1gFnSc5	Pendolina
trvá	trvat	k5eAaImIp3nS	trvat
cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
Tampere	Tamper	k1gInSc5	Tamper
asi	asi	k9	asi
hodinu	hodina	k1gFnSc4	hodina
a	a	k8xC	a
půl	půl	k1xP	půl
<g/>
,	,	kIx,	,
cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
Turku	Turek	k1gMnSc6	Turek
zabere	zabrat	k5eAaPmIp3nS	zabrat
o	o	k7c4	o
dvacet	dvacet	k4xCc4	dvacet
minut	minuta	k1gFnPc2	minuta
více	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Denně	denně	k6eAd1	denně
jsou	být	k5eAaImIp3nP	být
vypravovány	vypravován	k2eAgInPc1d1	vypravován
dva	dva	k4xCgInPc1	dva
rychlíky	rychlík	k1gInPc1	rychlík
do	do	k7c2	do
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
<g/>
.	.	kIx.	.
</s>
<s>
Nočním	noční	k2eAgInSc7d1	noční
Santa	Santa	k1gMnSc1	Santa
Claus	Claus	k1gMnSc1	Claus
expresem	expres	k1gInSc7	expres
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
dojet	dojet	k5eAaPmF	dojet
až	až	k9	až
do	do	k7c2	do
Rovaniemi	Rovanie	k1gFnPc7	Rovanie
na	na	k7c4	na
severní	severní	k2eAgInSc4d1	severní
polární	polární	k2eAgInSc4d1	polární
kruh	kruh	k1gInSc4	kruh
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
terminálů	terminál	k1gInPc2	terminál
letiště	letiště	k1gNnSc2	letiště
ve	v	k7c6	v
Vantaa	Vanta	k1gInSc2	Vanta
je	být	k5eAaImIp3nS	být
vyhrazen	vyhradit	k5eAaPmNgMnS	vyhradit
domácím	domácí	k2eAgInPc3d1	domácí
letům	let	k1gInPc3	let
<g/>
.	.	kIx.	.
</s>
<s>
Výhledově	výhledově	k6eAd1	výhledově
se	se	k3xPyFc4	se
zvažuje	zvažovat	k5eAaImIp3nS	zvažovat
podmořský	podmořský	k2eAgInSc1d1	podmořský
tunel	tunel	k1gInSc1	tunel
do	do	k7c2	do
Estonska	Estonsko	k1gNnSc2	Estonsko
<g/>
;	;	kIx,	;
stavba	stavba	k1gFnSc1	stavba
by	by	kYmCp3nS	by
ale	ale	k9	ale
byla	být	k5eAaImAgFnS	být
velmi	velmi	k6eAd1	velmi
náročná	náročný	k2eAgFnSc1d1	náročná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Helsinkách	Helsinky	k1gFnPc6	Helsinky
je	být	k5eAaImIp3nS	být
přes	přes	k7c4	přes
80	[number]	k4	80
muzeí	muzeum	k1gNnPc2	muzeum
a	a	k8xC	a
galerií	galerie	k1gFnPc2	galerie
včetně	včetně	k7c2	včetně
národních	národní	k2eAgInPc2d1	národní
a	a	k8xC	a
Národní	národní	k2eAgFnSc1d1	národní
opera	opera	k1gFnSc1	opera
s	s	k7c7	s
novou	nový	k2eAgFnSc7d1	nová
budovou	budova	k1gFnSc7	budova
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
Známé	známý	k2eAgNnSc1d1	známé
je	být	k5eAaImIp3nS	být
i	i	k9	i
muzeum	muzeum	k1gNnSc1	muzeum
umění	umění	k1gNnSc2	umění
Ateneum	Ateneum	k1gNnSc4	Ateneum
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
technicky	technicky	k6eAd1	technicky
zaměřené	zaměřený	k2eAgMnPc4d1	zaměřený
návštěvníky	návštěvník	k1gMnPc4	návštěvník
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Helsinkách	Helsinky	k1gFnPc6	Helsinky
připravena	připraven	k2eAgFnSc1d1	připravena
Heureka	Heureka	k1gFnSc1	Heureka
<g/>
,	,	kIx,	,
vědecké	vědecký	k2eAgNnSc1d1	vědecké
zábavní	zábavní	k2eAgNnSc1d1	zábavní
centrum	centrum	k1gNnSc1	centrum
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
výstavami	výstava	k1gFnPc7	výstava
a	a	k8xC	a
třírozměrným	třírozměrný	k2eAgNnSc7d1	třírozměrné
kinem	kino	k1gNnSc7	kino
IMAX	IMAX	kA	IMAX
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
i	i	k9	i
velký	velký	k2eAgInSc1d1	velký
zábavní	zábavní	k2eAgInSc1d1	zábavní
park	park	k1gInSc1	park
Linnanmäki	Linnanmäk	k1gFnSc2	Linnanmäk
s	s	k7c7	s
několika	několik	k4yIc2	několik
horskými	horský	k2eAgFnPc7d1	horská
dráhami	dráha	k1gFnPc7	dráha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2007	[number]	k4	2007
se	se	k3xPyFc4	se
v	v	k7c6	v
Helsinkách	Helsinky	k1gFnPc6	Helsinky
konala	konat	k5eAaImAgFnS	konat
Velká	velký	k2eAgFnSc1d1	velká
cena	cena	k1gFnSc1	cena
Eurovize	Eurovize	k1gFnSc1	Eurovize
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
účastnil	účastnit	k5eAaImAgMnS	účastnit
i	i	k9	i
český	český	k2eAgMnSc1d1	český
zástupce	zástupce	k1gMnSc1	zástupce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Helsinkách	Helsinky	k1gFnPc6	Helsinky
je	být	k5eAaImIp3nS	být
řada	řada	k1gFnSc1	řada
českých	český	k2eAgMnPc2d1	český
hospod	hospod	k?	hospod
s	s	k7c7	s
poetickými	poetický	k2eAgInPc7d1	poetický
názvy	název	k1gInPc7	název
jako	jako	k8xC	jako
Hádanka	hádanka	k1gFnSc1	hádanka
či	či	k8xC	či
Milenka	Milenka	k1gFnSc1	Milenka
a	a	k8xC	a
nostalgicky	nostalgicky	k6eAd1	nostalgicky
pojmenovaný	pojmenovaný	k2eAgInSc1d1	pojmenovaný
bar	bar	k1gInSc1	bar
Zetor	zetor	k1gInSc1	zetor
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
druhů	druh	k1gInPc2	druh
cévnatých	cévnatý	k2eAgFnPc2d1	cévnatá
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
:	:	kIx,	:
1064	[number]	k4	1064
Počet	počet	k1gInSc4	počet
druhů	druh	k1gInPc2	druh
hnízdících	hnízdící	k2eAgMnPc2d1	hnízdící
ptáků	pták	k1gMnPc2	pták
<g/>
:	:	kIx,	:
164	[number]	k4	164
Symbolická	symbolický	k2eAgFnSc1d1	symbolická
rostlina	rostlina	k1gFnSc1	rostlina
města	město	k1gNnSc2	město
<g/>
:	:	kIx,	:
javor	javor	k1gInSc4	javor
Symbolické	symbolický	k2eAgNnSc1d1	symbolické
zvíře	zvíře	k1gNnSc1	zvíře
města	město	k1gNnSc2	město
<g/>
:	:	kIx,	:
veverka	veverka	k1gFnSc1	veverka
Exhalace	exhalace	k1gFnSc2	exhalace
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
v	v	k7c6	v
tunách	tuna	k1gFnPc6	tuna
<g/>
:	:	kIx,	:
SO2	SO2	k1gFnSc1	SO2
3	[number]	k4	3
762	[number]	k4	762
<g/>
,	,	kIx,	,
NOx	noxa	k1gFnPc2	noxa
10	[number]	k4	10
211	[number]	k4	211
<g/>
,	,	kIx,	,
prach	prach	k1gInSc1	prach
525	[number]	k4	525
<g/>
,	,	kIx,	,
CO	co	k6eAd1	co
14	[number]	k4	14
367	[number]	k4	367
<g/>
,	,	kIx,	,
CO2	CO2	k1gFnSc1	CO2
4	[number]	k4	4
816	[number]	k4	816
000	[number]	k4	000
Spotřeba	spotřeba	k1gFnSc1	spotřeba
vody	voda	k1gFnSc2	voda
na	na	k7c4	na
obyvatele	obyvatel	k1gMnPc4	obyvatel
a	a	k8xC	a
den	den	k1gInSc4	den
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
<g/>
:	:	kIx,	:
255	[number]	k4	255
litrů	litr	k1gInPc2	litr
Roční	roční	k2eAgInSc1d1	roční
odpad	odpad	k1gInSc1	odpad
na	na	k7c4	na
obyvatele	obyvatel	k1gMnSc4	obyvatel
<g/>
:	:	kIx,	:
0,7	[number]	k4	0,7
tuny	tuna	k1gFnSc2	tuna
Oslo	Oslo	k1gNnPc2	Oslo
<g/>
,	,	kIx,	,
Norsko	Norsko	k1gNnSc1	Norsko
Petrohrad	Petrohrad	k1gInSc1	Petrohrad
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
Sofie	Sofie	k1gFnSc1	Sofie
<g/>
,	,	kIx,	,
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Česko	Česko	k1gNnSc1	Česko
</s>
