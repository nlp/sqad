<s>
Krchleby	Krchleb	k1gInPc1
(	(	kIx(
<g/>
tvrz	tvrz	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Krchleby	Krchleba	k1gFnPc1
Základní	základní	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Sloh	sloha	k1gFnPc2
</s>
<s>
gotický	gotický	k2eAgInSc1d1
Výstavba	výstavba	k1gFnSc1
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
Zánik	zánik	k1gInSc1
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
Další	další	k2eAgMnPc1d1
majitelé	majitel	k1gMnPc1
</s>
<s>
Chrudim	Chrudim	k1gFnSc1
<g/>
,	,	kIx,
Pardubické	pardubický	k2eAgNnSc1d1
panství	panství	k1gNnSc1
Současný	současný	k2eAgMnSc1d1
majitel	majitel	k1gMnSc1
</s>
<s>
zaniklá	zaniklý	k2eAgFnSc1d1
Poloha	poloha	k1gFnSc1
Adresa	adresa	k1gFnSc1
</s>
<s>
Pardubice	Pardubice	k1gInPc1
<g/>
,	,	kIx,
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
2	#num#	k4
<g/>
′	′	k?
<g/>
5,02	5,02	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
15	#num#	k4
<g/>
°	°	k?
<g/>
41	#num#	k4
<g/>
′	′	k?
<g/>
18,27	18,27	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Tvrz	tvrz	k1gFnSc1
v	v	k7c6
Krchlebách	Krchleba	k1gFnPc6
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Tvrz	tvrz	k1gFnSc1
Krchleby	Krchleba	k1gFnSc2
je	být	k5eAaImIp3nS
společně	společně	k6eAd1
se	s	k7c7
stejnojmennou	stejnojmenný	k2eAgFnSc7d1
vesnicí	vesnice	k1gFnSc7
připomínána	připomínat	k5eAaImNgFnS
od	od	k7c2
čtrnáctého	čtrnáctý	k4xOgNnSc2
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1323	#num#	k4
je	být	k5eAaImIp3nS
zmiňován	zmiňován	k2eAgMnSc1d1
Beneš	Beneš	k1gMnSc1
z	z	k7c2
Krchleb	Krchlba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1346	#num#	k4
je	být	k5eAaImIp3nS
zaznamenán	zaznamenat	k5eAaPmNgInS
její	její	k3xOp3gInSc4
prodej	prodej	k1gInSc4
Janem	Jan	k1gMnSc7
Žákem	Žák	k1gMnSc7
z	z	k7c2
Krchleb	Krchlba	k1gFnPc2
Janovi	Janův	k2eAgMnPc1d1
a	a	k8xC
Jiříkovi	Jiříkův	k2eAgMnPc1d1
z	z	k7c2
Ohnišťan	Ohnišťan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1502	#num#	k4
byla	být	k5eAaImAgFnS
prodána	prodat	k5eAaPmNgFnS
městu	město	k1gNnSc3
Chrudim	Chrudim	k1gFnSc1
<g/>
,	,	kIx,
kterému	který	k3yIgMnSc3,k3yRgMnSc3,k3yQgMnSc3
ale	ale	k9
byla	být	k5eAaImAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1547	#num#	k4
odňata	odnít	k5eAaPmNgFnS
za	za	k7c4
účast	účast	k1gFnSc4
na	na	k7c6
stavovském	stavovský	k2eAgInSc6d1
odboji	odboj	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rok	rok	k1gInSc1
poté	poté	k6eAd1
jí	on	k3xPp3gFnSc3
koupil	koupit	k5eAaPmAgMnS
Jan	Jan	k1gMnSc1
z	z	k7c2
Pernštejna	Pernštejn	k1gInSc2
a	a	k8xC
přičlenil	přičlenit	k5eAaPmAgInS
k	k	k7c3
pardubickému	pardubický	k2eAgNnSc3d1
panství	panství	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
ovšem	ovšem	k9
ztratila	ztratit	k5eAaPmAgFnS
svou	svůj	k3xOyFgFnSc4
funkci	funkce	k1gFnSc4
a	a	k8xC
v	v	k7c6
sedmnáctém	sedmnáctý	k4xOgInSc6
století	století	k1gNnSc6
zanikla	zaniknout	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbytky	zbytek	k1gInPc1
tvrziště	tvrziště	k1gNnSc2
přetrvaly	přetrvat	k5eAaPmAgInP
až	až	k6eAd1
do	do	k7c2
období	období	k1gNnSc2
po	po	k7c6
druhé	druhý	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byly	být	k5eAaImAgInP
rozorány	rozorat	k5eAaPmNgInP
během	během	k7c2
kolektivizace	kolektivizace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Tvrz	tvrz	k1gFnSc1
spolu	spolu	k6eAd1
se	s	k7c7
vsí	ves	k1gFnSc7
stála	stát	k5eAaImAgFnS
mezi	mezi	k7c7
třemi	tři	k4xCgInPc7
dnes	dnes	k6eAd1
již	již	k6eAd1
zaniklými	zaniklý	k2eAgInPc7d1
rybníky	rybník	k1gInPc7
napájenými	napájený	k2eAgInPc7d1
Podolským	podolský	k2eAgInSc7d1
potokem	potok	k1gInSc7
resp.	resp.	kA
jeho	jeho	k3xOp3gInPc4
přítoky	přítok	k1gInPc4
z	z	k7c2
nichž	jenž	k3xRgInPc2
jeden	jeden	k4xCgInSc1
nesl	nést	k5eAaImAgInS
název	název	k1gInSc4
Tvrzný	Tvrzný	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gFnSc7
vodou	voda	k1gFnSc7
bylo	být	k5eAaImAgNnS
možné	možný	k2eAgNnSc1d1
zatápět	zatápět	k5eAaImF
obranný	obranný	k2eAgInSc1d1
příkop	příkop	k1gInSc1
obklopující	obklopující	k2eAgInSc1d1
val	val	k1gInSc1
o	o	k7c6
vejčitém	vejčitý	k2eAgInSc6d1
půdorysu	půdorys	k1gInSc6
a	a	k8xC
dokonce	dokonce	k9
i	i	k9
vnitřní	vnitřní	k2eAgInPc4d1
prostory	prostor	k1gInPc4
tvrze	tvrz	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vejčitý	vejčitý	k2eAgInSc1d1
půdorys	půdorys	k1gInSc1
objektu	objekt	k1gInSc2
je	být	k5eAaImIp3nS
na	na	k7c6
letecké	letecký	k2eAgFnSc6d1
mapě	mapa	k1gFnSc6
stále	stále	k6eAd1
viditelný	viditelný	k2eAgInSc1d1
na	na	k7c6
jihovýchodním	jihovýchodní	k2eAgInSc6d1
okraji	okraj	k1gInSc6
vsi	ves	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Uvnitř	uvnitř	k7c2
valu	val	k1gInSc2
se	se	k3xPyFc4
nacházely	nacházet	k5eAaImAgFnP
dvě	dva	k4xCgFnPc1
nezatápěné	zatápěný	k2eNgFnPc1d1
vyvýšeniny	vyvýšenina	k1gFnPc1
se	s	k7c7
dvěma	dva	k4xCgFnPc7
budovami	budova	k1gFnPc7
<g/>
,	,	kIx,
jednou	jednou	k6eAd1
kulatou	kulatý	k2eAgFnSc4d1
a	a	k8xC
jednou	jednou	k6eAd1
hranatou	hranatý	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
představ	představa	k1gFnPc2
historiků	historik	k1gMnPc2
mohly	moct	k5eAaImAgFnP
být	být	k5eAaImF
obě	dva	k4xCgFnPc1
budovy	budova	k1gFnPc1
propojeny	propojit	k5eAaPmNgFnP
dřevěnou	dřevěný	k2eAgFnSc7d1
lávkou	lávka	k1gFnSc7
či	či	k8xC
mostem	most	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
tvrzišti	tvrziště	k1gNnSc6
byl	být	k5eAaImAgInS
nalezen	nalezen	k2eAgInSc1d1
meč	meč	k1gInSc1
ze	z	k7c2
13	#num#	k4
<g/>
.	.	kIx.
nebo	nebo	k8xC
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Obrys	obrys	k1gInSc1
tvrziště	tvrziště	k1gNnSc2
na	na	k7c6
letecké	letecký	k2eAgFnSc6d1
mapě	mapa	k1gFnSc6
<g/>
↑	↑	k?
PhDr.	PhDr.	kA
František	František	k1gMnSc1
Šebek	Šebek	k1gMnSc1
-	-	kIx~
Toulky	toulka	k1gFnPc1
historií	historie	k1gFnPc2
Pardubic	Pardubice	k1gInPc2
(	(	kIx(
<g/>
Helios	Helios	k1gInSc1
Jiří	Jiří	k1gMnSc1
Razskazov	Razskazov	k1gInSc1
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
,	,	kIx,
EAN	EAN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85211	#num#	k4
<g/>
-	-	kIx~
<g/>
38	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Jiří	Jiří	k1gMnSc1
Hofman	Hofman	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
Paleček	Paleček	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
Řeháček	Řeháček	k1gMnSc1
-	-	kIx~
Historie	historie	k1gFnSc1
pardubických	pardubický	k2eAgFnPc2d1
městských	městský	k2eAgFnPc2d1
částí	část	k1gFnPc2
(	(	kIx(
<g/>
Klub	klub	k1gInSc1
přátel	přítel	k1gMnPc2
Pardubicka	Pardubicko	k1gNnSc2
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Heslo	heslo	k1gNnSc1
Krchleby	Krchleba	k1gFnSc2
na	na	k7c6
Parpedii	Parpedie	k1gFnSc6
</s>
<s>
Heslo	heslo	k1gNnSc1
Krchleby	Krchleba	k1gFnSc2
na	na	k7c6
Hradech	hrad	k1gInPc6
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
