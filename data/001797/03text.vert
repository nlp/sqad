<s>
Centrální	centrální	k2eAgFnSc1d1	centrální
procesorová	procesorový	k2eAgFnSc1d1	procesorová
jednotka	jednotka	k1gFnSc1	jednotka
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
CPU	CPU	kA	CPU
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
central	centrat	k5eAaImAgInS	centrat
processing	processing	k1gInSc1	processing
unit	unit	k1gInSc1	unit
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
informatice	informatika	k1gFnSc6	informatika
označení	označení	k1gNnSc2	označení
základní	základní	k2eAgFnSc2d1	základní
elektronické	elektronický	k2eAgFnSc2d1	elektronická
součásti	součást	k1gFnSc2	součást
v	v	k7c6	v
počítači	počítač	k1gInSc6	počítač
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
umí	umět	k5eAaImIp3nS	umět
vykonávat	vykonávat	k5eAaImF	vykonávat
strojové	strojový	k2eAgFnPc4d1	strojová
instrukce	instrukce	k1gFnPc4	instrukce
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yIgInPc2	který
je	být	k5eAaImIp3nS	být
tvořen	tvořen	k2eAgInSc1d1	tvořen
počítačový	počítačový	k2eAgInSc1d1	počítačový
program	program	k1gInSc1	program
a	a	k8xC	a
obsluhovat	obsluhovat	k5eAaImF	obsluhovat
jeho	jeho	k3xOp3gInPc4	jeho
vstupy	vstup	k1gInPc4	vstup
a	a	k8xC	a
výstupy	výstup	k1gInPc4	výstup
<g/>
.	.	kIx.	.
</s>
<s>
Historicky	historicky	k6eAd1	historicky
byl	být	k5eAaImAgInS	být
CPU	CPU	kA	CPU
složen	složen	k2eAgInSc4d1	složen
z	z	k7c2	z
velmi	velmi	k6eAd1	velmi
mnoha	mnoho	k4c2	mnoho
elektronických	elektronický	k2eAgFnPc2d1	elektronická
součástek	součástka	k1gFnPc2	součástka
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
byly	být	k5eAaImAgInP	být
všechny	všechen	k3xTgInPc1	všechen
potřebné	potřebný	k2eAgInPc1d1	potřebný
obvody	obvod	k1gInPc1	obvod
sloučeny	sloučen	k2eAgInPc1d1	sloučen
do	do	k7c2	do
jednoho	jeden	k4xCgInSc2	jeden
integrovaného	integrovaný	k2eAgInSc2d1	integrovaný
obvodu	obvod	k1gInSc2	obvod
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k9	jako
mikroprocesor	mikroprocesor	k1gInSc1	mikroprocesor
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc1d1	následující
článek	článek	k1gInSc1	článek
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
obecnou	obecný	k2eAgFnSc7d1	obecná
stavbou	stavba	k1gFnSc7	stavba
procesoru	procesor	k1gInSc2	procesor
<g/>
.	.	kIx.	.
</s>
<s>
Informace	informace	k1gFnPc1	informace
o	o	k7c6	o
současných	současný	k2eAgInPc6d1	současný
procesorech	procesor	k1gInPc6	procesor
najdete	najít	k5eAaPmIp2nP	najít
v	v	k7c6	v
článku	článek	k1gInSc6	článek
mikroprocesor	mikroprocesor	k1gInSc1	mikroprocesor
<g/>
.	.	kIx.	.
</s>
<s>
Procesor	procesor	k1gInSc1	procesor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nS	by
vykonával	vykonávat	k5eAaImAgInS	vykonávat
program	program	k1gInSc1	program
zapsaný	zapsaný	k2eAgInSc1d1	zapsaný
ve	v	k7c6	v
vyšším	vysoký	k2eAgInSc6d2	vyšší
programovacím	programovací	k2eAgInSc6d1	programovací
jazyku	jazyk	k1gInSc6	jazyk
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
příliš	příliš	k6eAd1	příliš
složitý	složitý	k2eAgInSc1d1	složitý
<g/>
,	,	kIx,	,
a	a	k8xC	a
především	především	k6eAd1	především
omezený	omezený	k2eAgInSc4d1	omezený
zaměřením	zaměření	k1gNnSc7	zaměření
daného	daný	k2eAgInSc2d1	daný
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
moderní	moderní	k2eAgInPc1d1	moderní
procesory	procesor	k1gInPc1	procesor
stále	stále	k6eAd1	stále
založeny	založit	k5eAaPmNgInP	založit
na	na	k7c4	na
zpracování	zpracování	k1gNnSc4	zpracování
strojového	strojový	k2eAgInSc2d1	strojový
kódu	kód	k1gInSc2	kód
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
jest	být	k5eAaImIp3nS	být
nevelké	velký	k2eNgFnPc4d1	nevelká
sady	sada	k1gFnPc4	sada
strojových	strojový	k2eAgFnPc2d1	strojová
instrukcí	instrukce	k1gFnPc2	instrukce
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
více	hodně	k6eAd2	hodně
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
způsobu	způsob	k1gInSc3	způsob
práce	práce	k1gFnSc2	práce
procesoru	procesor	k1gInSc2	procesor
(	(	kIx(	(
<g/>
např.	např.	kA	např.
přesun	přesun	k1gInSc1	přesun
čísla	číslo	k1gNnSc2	číslo
z	z	k7c2	z
registru	registr	k1gInSc2	registr
do	do	k7c2	do
registru	registr	k1gInSc2	registr
<g/>
)	)	kIx)	)
než	než	k8xS	než
způsobu	způsob	k1gInSc2	způsob
práce	práce	k1gFnSc2	práce
programátora	programátor	k1gMnSc2	programátor
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
architektura	architektura	k1gFnSc1	architektura
procesorů	procesor	k1gInPc2	procesor
definuje	definovat	k5eAaBmIp3nS	definovat
svůj	svůj	k3xOyFgInSc4	svůj
vlastní	vlastní	k2eAgInSc4d1	vlastní
strojový	strojový	k2eAgInSc4d1	strojový
jazyk	jazyk	k1gInSc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
procesory	procesor	k1gInPc1	procesor
x	x	k?	x
<g/>
86	[number]	k4	86
od	od	k7c2	od
firmy	firma	k1gFnSc2	firma
Intel	Intel	kA	Intel
používají	používat	k5eAaImIp3nP	používat
jiný	jiný	k2eAgInSc1d1	jiný
strojový	strojový	k2eAgInSc1d1	strojový
kód	kód	k1gInSc1	kód
než	než	k8xS	než
architektura	architektura	k1gFnSc1	architektura
ARM	ARM	kA	ARM
<g/>
.	.	kIx.	.
</s>
<s>
Programy	program	k1gInPc1	program
psané	psaný	k2eAgInPc1d1	psaný
ve	v	k7c6	v
vyšších	vysoký	k2eAgInPc6d2	vyšší
jazycích	jazyk	k1gInPc6	jazyk
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
překládají	překládat	k5eAaImIp3nP	překládat
do	do	k7c2	do
strojového	strojový	k2eAgInSc2d1	strojový
kódu	kód	k1gInSc2	kód
dané	daný	k2eAgFnSc2d1	daná
architektury	architektura	k1gFnSc2	architektura
–	–	k?	–
ať	ať	k8xS	ať
už	už	k6eAd1	už
předem	předem	k6eAd1	předem
(	(	kIx(	(
<g/>
pomoci	pomoct	k5eAaPmF	pomoct
tzv.	tzv.	kA	tzv.
překladače	překladač	k1gInPc4	překladač
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
až	až	k9	až
za	za	k7c2	za
běhu	běh	k1gInSc2	běh
programu	program	k1gInSc2	program
(	(	kIx(	(
<g/>
pomoci	pomoc	k1gFnSc2	pomoc
interpretu	interpret	k1gMnSc6	interpret
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Centrální	centrální	k2eAgFnSc1d1	centrální
procesorová	procesorový	k2eAgFnSc1d1	procesorová
jednotka	jednotka	k1gFnSc1	jednotka
(	(	kIx(	(
<g/>
CPU	CPU	kA	CPU
<g/>
)	)	kIx)	)
provádí	provádět	k5eAaImIp3nS	provádět
strojové	strojový	k2eAgFnPc4d1	strojová
instrukce	instrukce	k1gFnPc4	instrukce
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgFnPc4d1	hlavní
součásti	součást	k1gFnPc4	součást
procesoru	procesor	k1gInSc2	procesor
patří	patřit	k5eAaImIp3nS	patřit
aritmeticko-logická	aritmetickoogický	k2eAgFnSc1d1	aritmeticko-logická
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
,	,	kIx,	,
registry	registr	k1gInPc1	registr
a	a	k8xC	a
řadič	řadič	k1gInSc1	řadič
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
řídí	řídit	k5eAaImIp3nS	řídit
činnost	činnost	k1gFnSc4	činnost
procesoru	procesor	k1gInSc2	procesor
(	(	kIx(	(
<g/>
načítání	načítání	k1gNnSc4	načítání
strojových	strojový	k2eAgFnPc2d1	strojová
instrukcí	instrukce	k1gFnPc2	instrukce
z	z	k7c2	z
paměti	paměť	k1gFnSc2	paměť
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnSc3	jejich
dekódování	dekódování	k1gNnSc3	dekódování
<g/>
,	,	kIx,	,
provedení	provedení	k1gNnSc3	provedení
a	a	k8xC	a
uložení	uložení	k1gNnSc3	uložení
výsledků	výsledek	k1gInPc2	výsledek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současných	současný	k2eAgInPc6d1	současný
počítačích	počítač	k1gInPc6	počítač
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
několik	několik	k4yIc1	několik
dalších	další	k2eAgFnPc2d1	další
procesorových	procesorový	k2eAgFnPc2d1	procesorová
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
s	s	k7c7	s
hlavní	hlavní	k2eAgFnSc7d1	hlavní
procesorovou	procesorový	k2eAgFnSc7d1	procesorová
jednotkou	jednotka	k1gFnSc7	jednotka
spolupracují	spolupracovat	k5eAaImIp3nP	spolupracovat
(	(	kIx(	(
<g/>
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
například	například	k6eAd1	například
vstup	vstup	k1gInSc4	vstup
<g/>
/	/	kIx~	/
<g/>
výstup	výstup	k1gInSc4	výstup
<g/>
,	,	kIx,	,
příjem	příjem	k1gInSc4	příjem
GPS	GPS	kA	GPS
signálu	signál	k1gInSc2	signál
<g/>
,	,	kIx,	,
komunikaci	komunikace	k1gFnSc4	komunikace
s	s	k7c7	s
bezdrátovou	bezdrátový	k2eAgFnSc7d1	bezdrátová
sítí	síť	k1gFnSc7	síť
Wi-Fi	Wi-F	k1gFnSc2	Wi-F
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
<g/>
-li	i	k?	-li
obvod	obvod	k1gInSc4	obvod
v	v	k7c6	v
sobě	sebe	k3xPyFc6	sebe
více	hodně	k6eAd2	hodně
procesorových	procesorový	k2eAgFnPc2d1	procesorová
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k8xC	jako
vícejádrový	vícejádrový	k2eAgInSc4d1	vícejádrový
procesor	procesor	k1gInSc4	procesor
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stolních	stolní	k2eAgInPc6d1	stolní
počítačích	počítač	k1gInPc6	počítač
IBM	IBM	kA	IBM
PC	PC	kA	PC
kompatibilních	kompatibilní	k2eAgMnPc2d1	kompatibilní
se	se	k3xPyFc4	se
prosadily	prosadit	k5eAaPmAgFnP	prosadit
složité	složitý	k2eAgInPc1d1	složitý
CISC	CISC	kA	CISC
procesory	procesor	k1gInPc1	procesor
architektury	architektura	k1gFnSc2	architektura
x	x	k?	x
<g/>
86	[number]	k4	86
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přenosných	přenosný	k2eAgInPc6d1	přenosný
počítačích	počítač	k1gInPc6	počítač
naopak	naopak	k6eAd1	naopak
jednodušší	jednoduchý	k2eAgInPc1d2	jednodušší
RISC	RISC	kA	RISC
procesory	procesor	k1gInPc1	procesor
architektury	architektura	k1gFnSc2	architektura
ARM	ARM	kA	ARM
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
méně	málo	k6eAd2	málo
energeticky	energeticky	k6eAd1	energeticky
náročné	náročný	k2eAgNnSc1d1	náročné
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
malých	malý	k2eAgNnPc6d1	malé
zařízeních	zařízení	k1gNnPc6	zařízení
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
kardiostimulátory	kardiostimulátor	k1gInPc1	kardiostimulátor
nebo	nebo	k8xC	nebo
IoT	IoT	k1gFnPc1	IoT
jsou	být	k5eAaImIp3nP	být
používány	používán	k2eAgInPc1d1	používán
jednoduché	jednoduchý	k2eAgInPc1d1	jednoduchý
procesory	procesor	k1gInPc1	procesor
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
integrací	integrace	k1gFnSc7	integrace
(	(	kIx(	(
<g/>
system	systo	k1gNnSc7	systo
on	on	k3xPp3gMnSc1	on
a	a	k8xC	a
chip	chipit	k5eAaPmRp2nS	chipit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
jsou	být	k5eAaImIp3nP	být
používány	používán	k2eAgInPc1d1	používán
jednoúčelové	jednoúčelový	k2eAgInPc1d1	jednoúčelový
procesory	procesor	k1gInPc1	procesor
navržené	navržený	k2eAgInPc1d1	navržený
pro	pro	k7c4	pro
konkrétní	konkrétní	k2eAgNnSc4d1	konkrétní
použití	použití	k1gNnSc4	použití
(	(	kIx(	(
<g/>
např.	např.	kA	např.
digitální	digitální	k2eAgFnPc4d1	digitální
hodinky	hodinka	k1gFnPc4	hodinka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Procesory	procesor	k1gInPc1	procesor
prvních	první	k4xOgMnPc2	první
počítačů	počítač	k1gMnPc2	počítač
se	se	k3xPyFc4	se
skládaly	skládat	k5eAaImAgFnP	skládat
z	z	k7c2	z
obvodů	obvod	k1gInPc2	obvod
obsahujících	obsahující	k2eAgNnPc2d1	obsahující
množství	množství	k1gNnPc2	množství
tzv.	tzv.	kA	tzv.
diskrétních	diskrétní	k2eAgFnPc2d1	diskrétní
součástek	součástka	k1gFnPc2	součástka
–	–	k?	–
elektronek	elektronka	k1gFnPc2	elektronka
<g/>
,	,	kIx,	,
tranzistorů	tranzistor	k1gInPc2	tranzistor
<g/>
,	,	kIx,	,
rezistorů	rezistor	k1gInPc2	rezistor
a	a	k8xC	a
kondenzátorů	kondenzátor	k1gInPc2	kondenzátor
<g/>
.	.	kIx.	.
</s>
<s>
Takový	takový	k3xDgInSc1	takový
procesor	procesor	k1gInSc1	procesor
mohl	moct	k5eAaImAgInS	moct
zabírat	zabírat	k5eAaImF	zabírat
i	i	k9	i
několik	několik	k4yIc4	několik
skříní	skříň	k1gFnPc2	skříň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
používat	používat	k5eAaImF	používat
hybridní	hybridní	k2eAgInPc4d1	hybridní
i	i	k8xC	i
monolitické	monolitický	k2eAgInPc4d1	monolitický
integrované	integrovaný	k2eAgInPc4d1	integrovaný
obvody	obvod	k1gInPc4	obvod
<g/>
,	,	kIx,	,
kterých	který	k3yRgInPc2	který
bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
vytvoření	vytvoření	k1gNnSc4	vytvoření
CPU	cpát	k5eAaImIp1nS	cpát
potřeba	potřeba	k6eAd1	potřeba
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
nebo	nebo	k8xC	nebo
stovek	stovka	k1gFnPc2	stovka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
miniaturizace	miniaturizace	k1gFnSc2	miniaturizace
a	a	k8xC	a
zvyšování	zvyšování	k1gNnSc2	zvyšování
integrace	integrace	k1gFnSc2	integrace
dosáhly	dosáhnout	k5eAaPmAgInP	dosáhnout
stádia	stádium	k1gNnSc2	stádium
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
podstatnou	podstatný	k2eAgFnSc4d1	podstatná
část	část	k1gFnSc4	část
CPU	CPU	kA	CPU
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
umístit	umístit	k5eAaPmF	umístit
do	do	k7c2	do
jediného	jediný	k2eAgInSc2d1	jediný
integrovaného	integrovaný	k2eAgInSc2d1	integrovaný
obvodu	obvod	k1gInSc2	obvod
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
nazýváme	nazývat	k5eAaImIp1nP	nazývat
mikroprocesor	mikroprocesor	k1gInSc4	mikroprocesor
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
mikroprocesory	mikroprocesor	k1gInPc1	mikroprocesor
měly	mít	k5eAaImAgInP	mít
nepatrný	patrný	k2eNgInSc4d1	patrný
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
CPU	CPU	kA	CPU
tehdejších	tehdejší	k2eAgMnPc2d1	tehdejší
počítačů	počítač	k1gMnPc2	počítač
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
rozdíl	rozdíl	k1gInSc1	rozdíl
snižoval	snižovat	k5eAaImAgInS	snižovat
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
prakticky	prakticky	k6eAd1	prakticky
všechny	všechen	k3xTgInPc1	všechen
počítače	počítač	k1gInPc1	počítač
mají	mít	k5eAaImIp3nP	mít
CPU	CPU	kA	CPU
tvořené	tvořený	k2eAgInPc4d1	tvořený
mikroprocesory	mikroprocesor	k1gInPc4	mikroprocesor
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Mikroprocesor	mikroprocesor	k1gInSc1	mikroprocesor
<g/>
.	.	kIx.	.
</s>
<s>
Konstrukční	konstrukční	k2eAgFnSc1d1	konstrukční
složitost	složitost	k1gFnSc1	složitost
procesorů	procesor	k1gInPc2	procesor
byla	být	k5eAaImAgFnS	být
zvýšena	zvýšit	k5eAaPmNgFnS	zvýšit
různými	různý	k2eAgFnPc7d1	různá
technologiemi	technologie	k1gFnPc7	technologie
<g/>
,	,	kIx,	,
usnadňující	usnadňující	k2eAgNnSc1d1	usnadňující
budování	budování	k1gNnSc1	budování
menších	malý	k2eAgFnPc2d2	menší
a	a	k8xC	a
spolehlivější	spolehlivý	k2eAgInPc1d2	spolehlivější
elektronických	elektronický	k2eAgNnPc2d1	elektronické
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
takové	takový	k3xDgNnSc1	takový
zlepšení	zlepšení	k1gNnSc1	zlepšení
přišlo	přijít	k5eAaPmAgNnS	přijít
s	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
tranzistoru	tranzistor	k1gInSc2	tranzistor
<g/>
.	.	kIx.	.
</s>
<s>
Tranzistorové	tranzistorový	k2eAgInPc1d1	tranzistorový
procesory	procesor	k1gInPc1	procesor
už	už	k6eAd1	už
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
a	a	k8xC	a
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
nebyly	být	k5eNaImAgFnP	být
stavěny	stavit	k5eAaImNgFnP	stavit
z	z	k7c2	z
objemných	objemný	k2eAgInPc2d1	objemný
<g/>
,	,	kIx,	,
nespolehlivých	spolehlivý	k2eNgInPc2d1	nespolehlivý
<g/>
,	,	kIx,	,
a	a	k8xC	a
křehkých	křehký	k2eAgInPc2d1	křehký
spínacích	spínací	k2eAgInPc2d1	spínací
prvků	prvek	k1gInPc2	prvek
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
elektronky	elektronka	k1gFnPc4	elektronka
a	a	k8xC	a
elektromagnetická	elektromagnetický	k2eAgNnPc4d1	elektromagnetické
relé	relé	k1gNnPc4	relé
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
tranzistorů	tranzistor	k1gInPc2	tranzistor
byly	být	k5eAaImAgFnP	být
stavěny	stavit	k5eAaImNgInP	stavit
komplexnější	komplexní	k2eAgInPc1d2	komplexnější
a	a	k8xC	a
spolehlivější	spolehlivý	k2eAgInPc1d2	spolehlivější
procesory	procesor	k1gInPc1	procesor
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
nebo	nebo	k8xC	nebo
několika	několik	k4yIc6	několik
deskách	deska	k1gFnPc6	deska
s	s	k7c7	s
plošnými	plošný	k2eAgInPc7d1	plošný
spoji	spoj	k1gInPc7	spoj
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
obsahovaly	obsahovat	k5eAaImAgFnP	obsahovat
potřebné	potřebný	k2eAgFnPc1d1	potřebná
diskrétní	diskrétní	k2eAgFnPc1d1	diskrétní
součástky	součástka	k1gFnPc1	součástka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
představila	představit	k5eAaPmAgFnS	představit
firma	firma	k1gFnSc1	firma
IBM	IBM	kA	IBM
svou	svůj	k3xOyFgFnSc4	svůj
architekturu	architektura	k1gFnSc4	architektura
počítače	počítač	k1gInSc2	počítač
System	Systo	k1gNnSc7	Systo
<g/>
/	/	kIx~	/
<g/>
360	[number]	k4	360
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
v	v	k7c4	v
sérii	série	k1gFnSc4	série
počítačů	počítač	k1gMnPc2	počítač
schopných	schopný	k2eAgMnPc2d1	schopný
běhu	běh	k1gInSc6	běh
programů	program	k1gInPc2	program
s	s	k7c7	s
různou	různý	k2eAgFnSc7d1	různá
rychlostí	rychlost	k1gFnSc7	rychlost
a	a	k8xC	a
výkonem	výkon	k1gInSc7	výkon
<g/>
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc1	význam
spočíval	spočívat	k5eAaImAgInS	spočívat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
tehdy	tehdy	k6eAd1	tehdy
většina	většina	k1gFnSc1	většina
elektronických	elektronický	k2eAgMnPc2d1	elektronický
počítačů	počítač	k1gMnPc2	počítač
byla	být	k5eAaImAgFnS	být
vzájemně	vzájemně	k6eAd1	vzájemně
nekompatibilních	kompatibilní	k2eNgFnPc2d1	nekompatibilní
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
dokonce	dokonce	k9	dokonce
i	i	k9	i
počítače	počítač	k1gInPc1	počítač
dodávané	dodávaný	k2eAgInPc1d1	dodávaný
stejným	stejný	k2eAgMnSc7d1	stejný
výrobcem	výrobce	k1gMnSc7	výrobce
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
dosažení	dosažení	k1gNnSc4	dosažení
této	tento	k3xDgFnSc2	tento
výhody	výhoda	k1gFnSc2	výhoda
použila	použít	k5eAaPmAgFnS	použít
firma	firma	k1gFnSc1	firma
IBM	IBM	kA	IBM
mikroprogram	mikroprogram	k1gInSc4	mikroprogram
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
nazývané	nazývaný	k2eAgInPc1d1	nazývaný
"	"	kIx"	"
<g/>
mikrokód	mikrokód	k1gInSc1	mikrokód
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Architektura	architektura	k1gFnSc1	architektura
IBM	IBM	kA	IBM
System	Systo	k1gNnSc7	Systo
<g/>
/	/	kIx~	/
<g/>
360	[number]	k4	360
byla	být	k5eAaImAgFnS	být
tak	tak	k6eAd1	tak
populární	populární	k2eAgFnSc1d1	populární
že	že	k8xS	že
ovládala	ovládat	k5eAaImAgFnS	ovládat
trh	trh	k1gInSc4	trh
střediskových	střediskový	k2eAgInPc2d1	střediskový
počítačů	počítač	k1gInPc2	počítač
po	po	k7c4	po
celá	celý	k2eAgNnPc4d1	celé
desetiletí	desetiletí	k1gNnPc4	desetiletí
a	a	k8xC	a
zanechala	zanechat	k5eAaPmAgFnS	zanechat
dědictví	dědictví	k1gNnSc4	dědictví
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
ještě	ještě	k9	ještě
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
v	v	k7c6	v
podobných	podobný	k2eAgInPc6d1	podobný
moderních	moderní	k2eAgInPc6d1	moderní
počítačích	počítač	k1gInPc6	počítač
jako	jako	k8xC	jako
IBM	IBM	kA	IBM
System	Systo	k1gNnSc7	Systo
z.	z.	k?	z.
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
firma	firma	k1gFnSc1	firma
Digital	Digital	kA	Digital
Equipment	Equipment	k1gInSc1	Equipment
Corporation	Corporation	k1gInSc1	Corporation
(	(	kIx(	(
<g/>
DEC	DEC	kA	DEC
<g/>
)	)	kIx)	)
představila	představit	k5eAaPmAgFnS	představit
jiný	jiný	k2eAgInSc4d1	jiný
vlivný	vlivný	k2eAgInSc4d1	vlivný
počítač	počítač	k1gInSc4	počítač
mířící	mířící	k2eAgInSc4d1	mířící
na	na	k7c4	na
vědecký	vědecký	k2eAgInSc4d1	vědecký
a	a	k8xC	a
výzkumný	výzkumný	k2eAgInSc4d1	výzkumný
trh	trh	k1gInSc4	trh
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
byl	být	k5eAaImAgMnS	být
PDP-	PDP-	k1gMnSc1	PDP-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Tranzistorové	tranzistorový	k2eAgInPc1d1	tranzistorový
procesory	procesor	k1gInPc1	procesor
měly	mít	k5eAaImAgFnP	mít
několik	několik	k4yIc4	několik
výrazných	výrazný	k2eAgFnPc2d1	výrazná
výhod	výhoda	k1gFnPc2	výhoda
před	před	k7c7	před
jejich	jejich	k3xOp3gNnSc7	jejich
předchůdci	předchůdce	k1gMnPc1	předchůdce
–	–	k?	–
kromě	kromě	k7c2	kromě
zjednodušení	zjednodušení	k1gNnSc2	zjednodušení
<g/>
,	,	kIx,	,
zvýšené	zvýšený	k2eAgFnSc2d1	zvýšená
spolehlivosti	spolehlivost	k1gFnSc2	spolehlivost
a	a	k8xC	a
nižší	nízký	k2eAgFnSc2d2	nižší
spotřeby	spotřeba	k1gFnSc2	spotřeba
energie	energie	k1gFnSc2	energie
dovolily	dovolit	k5eAaPmAgInP	dovolit
tranzistory	tranzistor	k1gInPc1	tranzistor
také	také	k9	také
použít	použít	k5eAaPmF	použít
pro	pro	k7c4	pro
procesor	procesor	k1gInSc4	procesor
vyšší	vysoký	k2eAgFnSc4d2	vyšší
operační	operační	k2eAgFnSc4d1	operační
rychlost	rychlost	k1gFnSc4	rychlost
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
umožnila	umožnit	k5eAaPmAgFnS	umožnit
kratší	krátký	k2eAgFnSc1d2	kratší
přepínací	přepínací	k2eAgFnSc1d1	přepínací
doba	doba	k1gFnSc1	doba
tranzistoru	tranzistor	k1gInSc2	tranzistor
(	(	kIx(	(
<g/>
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
relé	relé	k1gNnSc7	relé
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
zvýšení	zvýšení	k1gNnSc3	zvýšení
spolehlivosti	spolehlivost	k1gFnSc2	spolehlivost
a	a	k8xC	a
zvýšené	zvýšený	k2eAgFnSc3d1	zvýšená
rychlosti	rychlost	k1gFnSc3	rychlost
spínacích	spínací	k2eAgInPc2d1	spínací
prvků	prvek	k1gInPc2	prvek
(	(	kIx(	(
<g/>
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
téměř	téměř	k6eAd1	téměř
výhradně	výhradně	k6eAd1	výhradně
tranzistory	tranzistor	k1gInPc1	tranzistor
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
vyvinuty	vyvinut	k2eAgInPc1d1	vyvinut
procesory	procesor	k1gInPc1	procesor
s	s	k7c7	s
taktováním	taktování	k1gNnSc7	taktování
v	v	k7c6	v
desítkách	desítka	k1gFnPc6	desítka
megahertz	megahertz	k1gInSc1	megahertz
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
vývoje	vývoj	k1gInSc2	vývoj
obsahovaly	obsahovat	k5eAaImAgInP	obsahovat
integrované	integrovaný	k2eAgInPc1d1	integrovaný
obvody	obvod	k1gInPc1	obvod
méně	málo	k6eAd2	málo
tranzistorů	tranzistor	k1gInPc2	tranzistor
na	na	k7c6	na
vymezeném	vymezený	k2eAgInSc6d1	vymezený
prostoru	prostor	k1gInSc6	prostor
(	(	kIx(	(
<g/>
malá	malý	k2eAgFnSc1d1	malá
polovodičová	polovodičový	k2eAgFnSc1d1	polovodičová
destička	destička	k1gFnSc1	destička
uvnitř	uvnitř	k7c2	uvnitř
integrovaného	integrovaný	k2eAgInSc2d1	integrovaný
obvodu	obvod	k1gInSc2	obvod
–	–	k?	–
die	die	k?	die
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
byly	být	k5eAaImAgInP	být
vyráběny	vyrábět	k5eAaImNgInP	vyrábět
jen	jen	k9	jen
základní	základní	k2eAgInPc1d1	základní
nespecializované	specializovaný	k2eNgInPc1d1	nespecializovaný
obvody	obvod	k1gInPc1	obvod
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
NOR	Nor	k1gMnSc1	Nor
hradla	hradlo	k1gNnSc2	hradlo
<g/>
.	.	kIx.	.
</s>
<s>
Procesory	procesor	k1gInPc4	procesor
založené	založený	k2eAgInPc4d1	založený
na	na	k7c6	na
těchto	tento	k3xDgInPc6	tento
jednoduchých	jednoduchý	k2eAgInPc6d1	jednoduchý
"	"	kIx"	"
<g/>
stavebních	stavební	k2eAgInPc6d1	stavební
kamenech	kámen	k1gInPc6	kámen
<g/>
"	"	kIx"	"
jsou	být	k5eAaImIp3nP	být
obecně	obecně	k6eAd1	obecně
označovány	označovat	k5eAaImNgInP	označovat
jako	jako	k8xS	jako
zařízení	zařízení	k1gNnPc2	zařízení
"	"	kIx"	"
<g/>
Integrace	integrace	k1gFnSc1	integrace
v	v	k7c6	v
malém	malý	k2eAgNnSc6d1	malé
měřítku	měřítko	k1gNnSc6	měřítko
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
SSI	SSI	kA	SSI
<g/>
,	,	kIx,	,
Small-scale	Smallcala	k1gFnSc6	Small-scala
integration	integration	k1gInSc1	integration
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Konstrukce	konstrukce	k1gFnSc1	konstrukce
procesoru	procesor	k1gInSc2	procesor
pomocí	pomocí	k7c2	pomocí
SSI	SSI	kA	SSI
obvodů	obvod	k1gInPc2	obvod
vyžadovalo	vyžadovat	k5eAaImAgNnS	vyžadovat
tisíce	tisíc	k4xCgInPc1	tisíc
samostatných	samostatný	k2eAgInPc2d1	samostatný
čipů	čip	k1gInPc2	čip
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přesto	přesto	k8xC	přesto
takové	takový	k3xDgInPc4	takový
řešení	řešení	k1gNnSc4	řešení
ve	v	k7c6	v
výsledku	výsledek	k1gInSc6	výsledek
zabralo	zabrat	k5eAaPmAgNnS	zabrat
mnohem	mnohem	k6eAd1	mnohem
méně	málo	k6eAd2	málo
prostoru	prostor	k1gInSc2	prostor
<g/>
,	,	kIx,	,
mělo	mít	k5eAaImAgNnS	mít
nižší	nízký	k2eAgInSc4d2	nižší
příkon	příkon	k1gInSc4	příkon
a	a	k8xC	a
vyšší	vysoký	k2eAgFnSc4d2	vyšší
spolehlivost	spolehlivost	k1gFnSc4	spolehlivost
<g/>
,	,	kIx,	,
než	než	k8xS	než
dřívější	dřívější	k2eAgInPc1d1	dřívější
návrhy	návrh	k1gInPc1	návrh
s	s	k7c7	s
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
tranzistory	tranzistor	k1gInPc7	tranzistor
<g/>
.	.	kIx.	.
</s>
<s>
Počítač	počítač	k1gInSc1	počítač
IBM	IBM	kA	IBM
System	Syst	k1gMnSc7	Syst
<g/>
/	/	kIx~	/
<g/>
370	[number]	k4	370
vycházející	vycházející	k2eAgFnSc1d1	vycházející
z	z	k7c2	z
IBM	IBM	kA	IBM
Systemu	System	k1gInSc2	System
<g/>
/	/	kIx~	/
<g/>
360	[number]	k4	360
používal	používat	k5eAaImAgInS	používat
více	hodně	k6eAd2	hodně
SSI	SSI	kA	SSI
obvodů	obvod	k1gInPc2	obvod
<g/>
,	,	kIx,	,
než	než	k8xS	než
diskrétních	diskrétní	k2eAgFnPc2d1	diskrétní
součástek	součástka	k1gFnPc2	součástka
(	(	kIx(	(
<g/>
tranzistorů	tranzistor	k1gInPc2	tranzistor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Počítač	počítač	k1gInSc1	počítač
DEC	DEC	kA	DEC
PDP-	PDP-	k1gFnSc1	PDP-
<g/>
8	[number]	k4	8
<g/>
/	/	kIx~	/
<g/>
I	i	k9	i
a	a	k8xC	a
KI10	KI10	k1gFnSc1	KI10
PDP-10	PDP-10	k1gMnPc2	PDP-10
také	také	k6eAd1	také
přešly	přejít	k5eAaPmAgInP	přejít
z	z	k7c2	z
konstrukce	konstrukce	k1gFnSc2	konstrukce
pomocí	pomocí	k7c2	pomocí
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
tranzistorů	tranzistor	k1gInPc2	tranzistor
(	(	kIx(	(
<g/>
použitých	použitý	k2eAgInPc2d1	použitý
v	v	k7c6	v
PDP-8	PDP-8	k1gFnSc6	PDP-8
a	a	k8xC	a
PDP-	PDP-	k1gFnSc6	PDP-
<g/>
10	[number]	k4	10
<g/>
)	)	kIx)	)
k	k	k7c3	k
integrovaným	integrovaný	k2eAgInPc3d1	integrovaný
obvodům	obvod	k1gInPc3	obvod
typu	typ	k1gInSc2	typ
SSI	SSI	kA	SSI
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgFnSc1d1	populární
série	série	k1gFnSc1	série
PDP-11	PDP-11	k1gFnPc2	PDP-11
byla	být	k5eAaImAgFnS	být
původně	původně	k6eAd1	původně
postavena	postaven	k2eAgFnSc1d1	postavena
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
integrovaných	integrovaný	k2eAgInPc2d1	integrovaný
obvodů	obvod	k1gInPc2	obvod
SSI	SSI	kA	SSI
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jakmile	jakmile	k8xS	jakmile
se	se	k3xPyFc4	se
osvědčily	osvědčit	k5eAaPmAgInP	osvědčit
LSI	LSI	kA	LSI
(	(	kIx(	(
<g/>
large-scale	largecale	k6eAd1	large-scale
integration	integration	k1gInSc1	integration
<g/>
)	)	kIx)	)
komponenty	komponenta	k1gFnPc1	komponenta
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
představen	představit	k5eAaPmNgInS	představit
model	model	k1gInSc1	model
PDP-	PDP-	k1gFnSc2	PDP-
<g/>
11	[number]	k4	11
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
(	(	kIx(	(
<g/>
LSI-	LSI-	k1gFnSc1	LSI-
<g/>
11	[number]	k4	11
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lee	Lea	k1gFnSc3	Lea
Boysel	Boysela	k1gFnPc2	Boysela
publikoval	publikovat	k5eAaBmAgMnS	publikovat
slavné	slavný	k2eAgInPc4d1	slavný
články	článek	k1gInPc4	článek
včetně	včetně	k7c2	včetně
"	"	kIx"	"
<g/>
manifestu	manifest	k1gInSc2	manifest
<g/>
"	"	kIx"	"
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
popisoval	popisovat	k5eAaImAgMnS	popisovat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
sestavit	sestavit	k5eAaPmF	sestavit
ekvivalent	ekvivalent	k1gInSc1	ekvivalent
32	[number]	k4	32
<g/>
bitového	bitový	k2eAgInSc2d1	bitový
sálového	sálový	k2eAgInSc2d1	sálový
počítače	počítač	k1gInSc2	počítač
z	z	k7c2	z
relativně	relativně	k6eAd1	relativně
malého	malý	k2eAgInSc2d1	malý
počtu	počet	k1gInSc2	počet
integrovaných	integrovaný	k2eAgInPc2d1	integrovaný
obvodů	obvod	k1gInPc2	obvod
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
úrovní	úroveň	k1gFnSc7	úroveň
integrace	integrace	k1gFnSc2	integrace
(	(	kIx(	(
<g/>
large-scale	largecale	k6eAd1	large-scale
integration	integration	k1gInSc1	integration
<g/>
,	,	kIx,	,
LSI	LSI	kA	LSI
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
tuto	tento	k3xDgFnSc4	tento
dobu	doba	k1gFnSc4	doba
jediným	jediný	k2eAgInSc7d1	jediný
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
vyrobit	vyrobit	k5eAaPmF	vyrobit
LSI	LSI	kA	LSI
čipy	čip	k1gInPc4	čip
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
čipy	čip	k1gInPc1	čip
se	se	k3xPyFc4	se
sto	sto	k4xCgNnSc4	sto
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
branami	brána	k1gFnPc7	brána
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
použití	použití	k1gNnSc4	použití
MOS	MOS	kA	MOS
čipů	čip	k1gInPc2	čip
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
s	s	k7c7	s
PMOS	PMOS	kA	PMOS
<g/>
,	,	kIx,	,
NMOS	NMOS	kA	NMOS
nebo	nebo	k8xC	nebo
CMOS	CMOS	kA	CMOS
logikou	logika	k1gFnSc7	logika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
však	však	k9	však
byly	být	k5eAaImAgFnP	být
pomalé	pomalý	k2eAgFnPc1d1	pomalá
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
některé	některý	k3yIgFnPc1	některý
společnosti	společnost	k1gFnPc1	společnost
i	i	k9	i
nadále	nadále	k6eAd1	nadále
stavěly	stavět	k5eAaImAgInP	stavět
procesory	procesor	k1gInPc1	procesor
pomocí	pomocí	k7c2	pomocí
bipolárních	bipolární	k2eAgInPc2d1	bipolární
čipů	čip	k1gInPc2	čip
a	a	k8xC	a
pomalé	pomalý	k2eAgInPc1d1	pomalý
MOS	MOS	kA	MOS
obvody	obvod	k1gInPc1	obvod
byly	být	k5eAaImAgInP	být
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
užitečné	užitečný	k2eAgNnSc4d1	užitečné
jen	jen	k9	jen
v	v	k7c6	v
několika	několik	k4yIc6	několik
specializovaných	specializovaný	k2eAgInPc6d1	specializovaný
nasazeních	nasazení	k1gNnPc6	nasazení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vyžadovaly	vyžadovat	k5eAaImAgFnP	vyžadovat
nízký	nízký	k2eAgInSc4d1	nízký
příkon	příkon	k1gInSc4	příkon
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
byly	být	k5eAaImAgFnP	být
konstruovány	konstruovat	k5eAaImNgInP	konstruovat
procesory	procesor	k1gInPc1	procesor
pro	pro	k7c4	pro
superpočítače	superpočítač	k1gInPc4	superpočítač
z	z	k7c2	z
obvodů	obvod	k1gInPc2	obvod
s	s	k7c7	s
malou	malý	k2eAgFnSc7d1	malá
integrací	integrace	k1gFnSc7	integrace
(	(	kIx(	(
<g/>
SSI	SSI	kA	SSI
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
z	z	k7c2	z
obvodů	obvod	k1gInPc2	obvod
s	s	k7c7	s
integrací	integrace	k1gFnSc7	integrace
středního	střední	k2eAgNnSc2d1	střední
měřítka	měřítko	k1gNnSc2	měřítko
(	(	kIx(	(
<g/>
MSI	MSI	kA	MSI
<g/>
)	)	kIx)	)
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
74	[number]	k4	74
<g/>
xx	xx	k?	xx
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
TTL	TTL	kA	TTL
hradel	hradlo	k1gNnPc2	hradlo
<g/>
.	.	kIx.	.
</s>
<s>
Firma	firma	k1gFnSc1	firma
Datapoint	Datapointa	k1gFnPc2	Datapointa
používala	používat	k5eAaImAgFnS	používat
TTL	TTL	kA	TTL
čipy	čip	k1gInPc1	čip
ještě	ještě	k9	ještě
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sedmdesátých	sedmdesátý	k4xOgNnPc6	sedmdesátý
letech	léto	k1gNnPc6	léto
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
vynálezce	vynálezce	k1gMnSc1	vynálezce
Federico	Federico	k1gMnSc1	Federico
Faggin	Faggin	k1gMnSc1	Faggin
svými	svůj	k3xOyFgInPc7	svůj
vynálezy	vynález	k1gInPc7	vynález
(	(	kIx(	(
<g/>
MOSFET	MOSFET	kA	MOSFET
<g/>
)	)	kIx)	)
navždy	navždy	k6eAd1	navždy
změnil	změnit	k5eAaPmAgInS	změnit
vzhled	vzhled	k1gInSc4	vzhled
a	a	k8xC	a
implementaci	implementace	k1gFnSc4	implementace
mikroprocesorů	mikroprocesor	k1gInPc2	mikroprocesor
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
představení	představení	k1gNnSc2	představení
prvního	první	k4xOgMnSc2	první
<g/>
,	,	kIx,	,
komerčně	komerčně	k6eAd1	komerčně
dostupného	dostupný	k2eAgInSc2d1	dostupný
<g/>
,	,	kIx,	,
mikroprocesoru	mikroprocesor	k1gInSc2	mikroprocesor
(	(	kIx(	(
<g/>
Intel	Intel	kA	Intel
4004	[number]	k4	4004
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
a	a	k8xC	a
široce	široko	k6eAd1	široko
využívaného	využívaný	k2eAgInSc2d1	využívaný
mikroprocesoru	mikroprocesor	k1gInSc2	mikroprocesor
Intel	Intel	kA	Intel
8080	[number]	k4	8080
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
kategorie	kategorie	k1gFnSc1	kategorie
procesorů	procesor	k1gInPc2	procesor
téměř	téměř	k6eAd1	téměř
naprosto	naprosto	k6eAd1	naprosto
převzala	převzít	k5eAaPmAgFnS	převzít
všechny	všechen	k3xTgFnPc4	všechen
ostatní	ostatní	k2eAgFnPc4d1	ostatní
metody	metoda	k1gFnPc4	metoda
realizace	realizace	k1gFnSc2	realizace
centrálních	centrální	k2eAgFnPc2d1	centrální
procesorových	procesorový	k2eAgFnPc2d1	procesorová
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Výrobci	výrobce	k1gMnPc1	výrobce
sálových	sálový	k2eAgInPc2d1	sálový
počítačů	počítač	k1gInPc2	počítač
a	a	k8xC	a
minipočítačů	minipočítač	k1gInPc2	minipočítač
spustili	spustit	k5eAaPmAgMnP	spustit
vývoj	vývoj	k1gInSc4	vývoj
proprietárních	proprietární	k2eAgFnPc2d1	proprietární
IC	IC	kA	IC
programů	program	k1gInPc2	program
pro	pro	k7c4	pro
vylepšení	vylepšení	k1gNnSc4	vylepšení
starších	starý	k2eAgFnPc2d2	starší
počítačových	počítačový	k2eAgFnPc2d1	počítačová
architektur	architektura	k1gFnPc2	architektura
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
vyrobili	vyrobit	k5eAaPmAgMnP	vyrobit
mikroprocesory	mikroprocesor	k1gInPc4	mikroprocesor
s	s	k7c7	s
kompatibilní	kompatibilní	k2eAgFnSc7d1	kompatibilní
instrukční	instrukční	k2eAgFnSc7d1	instrukční
sadou	sada	k1gFnSc7	sada
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgInP	být
zpětně	zpětně	k6eAd1	zpětně
kompatibilní	kompatibilní	k2eAgInPc4d1	kompatibilní
se	se	k3xPyFc4	se
starších	starý	k2eAgFnPc2d2	starší
hardwarem	hardware	k1gInSc7	hardware
a	a	k8xC	a
softwarem	software	k1gInSc7	software
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
úspěšným	úspěšný	k2eAgInSc7d1	úspěšný
příchodem	příchod	k1gInSc7	příchod
všudypřítomného	všudypřítomný	k2eAgInSc2d1	všudypřítomný
osobního	osobní	k2eAgInSc2d1	osobní
počítače	počítač	k1gInSc2	počítač
se	se	k3xPyFc4	se
termín	termín	k1gInSc1	termín
CPU	CPU	kA	CPU
používá	používat	k5eAaImIp3nS	používat
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
výlučně	výlučně	k6eAd1	výlučně
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
mikroprocesory	mikroprocesor	k1gInPc4	mikroprocesor
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc4	několik
procesorů	procesor	k1gInPc2	procesor
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
označovaných	označovaný	k2eAgNnPc2d1	označované
jako	jako	k8xC	jako
jádra	jádro	k1gNnPc1	jádro
<g/>
)	)	kIx)	)
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
kombinovány	kombinovat	k5eAaImNgFnP	kombinovat
do	do	k7c2	do
jednoho	jeden	k4xCgInSc2	jeden
procesorového	procesorový	k2eAgInSc2d1	procesorový
čipu	čip	k1gInSc2	čip
<g/>
.	.	kIx.	.
</s>
<s>
Předchozí	předchozí	k2eAgFnSc1d1	předchozí
generace	generace	k1gFnSc1	generace
procesorů	procesor	k1gInPc2	procesor
byly	být	k5eAaImAgFnP	být
realizovány	realizovat	k5eAaBmNgFnP	realizovat
jako	jako	k9	jako
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
komponenty	komponenta	k1gFnPc1	komponenta
(	(	kIx(	(
<g/>
elektronické	elektronický	k2eAgFnPc1d1	elektronická
součástky	součástka	k1gFnPc1	součástka
<g/>
)	)	kIx)	)
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
drobných	drobný	k2eAgInPc2d1	drobný
integrovaných	integrovaný	k2eAgInPc2d1	integrovaný
obvodů	obvod	k1gInPc2	obvod
(	(	kIx(	(
<g/>
IC	IC	kA	IC
<g/>
)	)	kIx)	)
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
na	na	k7c6	na
více	hodně	k6eAd2	hodně
obvodových	obvodový	k2eAgFnPc6d1	obvodová
deskách	deska	k1gFnPc6	deska
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
<g/>
,	,	kIx,	,
mikroprocesory	mikroprocesor	k1gInPc1	mikroprocesor
jsou	být	k5eAaImIp3nP	být
vyráběny	vyrábět	k5eAaImNgInP	vyrábět
na	na	k7c6	na
velmi	velmi	k6eAd1	velmi
malém	malý	k2eAgNnSc6d1	malé
množství	množství	k1gNnSc6	množství
integrovaných	integrovaný	k2eAgInPc2d1	integrovaný
obvodů	obvod	k1gInPc2	obvod
<g/>
,	,	kIx,	,
běžně	běžně	k6eAd1	běžně
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
menší	malý	k2eAgFnSc1d2	menší
velikost	velikost	k1gFnSc1	velikost
mikroprocesoru	mikroprocesor	k1gInSc2	mikroprocesor
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
výsledkem	výsledek	k1gInSc7	výsledek
jeho	jeho	k3xOp3gFnSc2	jeho
realizace	realizace	k1gFnSc2	realizace
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
čip	čip	k1gInSc4	čip
<g/>
,	,	kIx,	,
znamená	znamenat	k5eAaImIp3nS	znamenat
rychlejší	rychlý	k2eAgInSc4d2	rychlejší
přepínací	přepínací	k2eAgInSc4d1	přepínací
čas	čas	k1gInSc4	čas
díky	díky	k7c3	díky
fyzickým	fyzický	k2eAgInPc3d1	fyzický
faktorům	faktor	k1gInPc3	faktor
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
snížená	snížený	k2eAgFnSc1d1	snížená
hodnota	hodnota	k1gFnSc1	hodnota
parazitní	parazitní	k2eAgFnSc2d1	parazitní
kapacity	kapacita	k1gFnSc2	kapacita
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
synchronním	synchronní	k2eAgInPc3d1	synchronní
mikroprocesorům	mikroprocesor	k1gInPc3	mikroprocesor
mít	mít	k5eAaImF	mít
frekvenci	frekvence	k1gFnSc4	frekvence
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
od	od	k7c2	od
desítek	desítka	k1gFnPc2	desítka
MHz	Mhz	kA	Mhz
po	po	k7c4	po
několik	několik	k4yIc4	několik
GHz	GHz	k1gFnPc2	GHz
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
se	s	k7c7	s
schopností	schopnost	k1gFnSc7	schopnost
vytvořit	vytvořit	k5eAaPmF	vytvořit
extrémně	extrémně	k6eAd1	extrémně
malé	malý	k2eAgInPc4d1	malý
tranzistory	tranzistor	k1gInPc4	tranzistor
na	na	k7c6	na
integrovaných	integrovaný	k2eAgInPc6d1	integrovaný
obvodech	obvod	k1gInPc6	obvod
se	se	k3xPyFc4	se
také	také	k9	také
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
složitost	složitost	k1gFnSc1	složitost
a	a	k8xC	a
množství	množství	k1gNnSc1	množství
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
procesoru	procesor	k1gInSc6	procesor
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
přímo	přímo	k6eAd1	přímo
několikanásobně	několikanásobně	k6eAd1	několikanásobně
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
široce	široko	k6eAd1	široko
pozorovatelný	pozorovatelný	k2eAgInSc1d1	pozorovatelný
trend	trend	k1gInSc1	trend
je	být	k5eAaImIp3nS	být
popsán	popsat	k5eAaPmNgInS	popsat
Mooreovým	Mooreův	k2eAgInSc7d1	Mooreův
zákonem	zákon	k1gInSc7	zákon
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
prokázán	prokázat	k5eAaPmNgInS	prokázat
jako	jako	k8xS	jako
velmi	velmi	k6eAd1	velmi
přesným	přesný	k2eAgMnSc7d1	přesný
předpovídatelem	předpovídatel	k1gMnSc7	předpovídatel
pro	pro	k7c4	pro
růst	růst	k1gInSc4	růst
komplikovanosti	komplikovanost	k1gFnSc2	komplikovanost
a	a	k8xC	a
složitosti	složitost	k1gFnSc2	složitost
procesorů	procesor	k1gInPc2	procesor
a	a	k8xC	a
integrovaných	integrovaný	k2eAgInPc2d1	integrovaný
obvodů	obvod	k1gInPc2	obvod
<g/>
.	.	kIx.	.
</s>
<s>
Současné	současný	k2eAgInPc1d1	současný
procesory	procesor	k1gInPc1	procesor
zachovávají	zachovávat	k5eAaImIp3nP	zachovávat
tradiční	tradiční	k2eAgNnSc4d1	tradiční
vnitřní	vnitřní	k2eAgNnSc4d1	vnitřní
uspořádání	uspořádání	k1gNnSc4	uspořádání
procesoru	procesor	k1gInSc2	procesor
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
rozpoznatelné	rozpoznatelný	k2eAgNnSc1d1	rozpoznatelné
i	i	k9	i
u	u	k7c2	u
prvních	první	k4xOgInPc2	první
procesorů	procesor	k1gInPc2	procesor
<g/>
.	.	kIx.	.
</s>
<s>
Procesory	procesor	k1gInPc1	procesor
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
<g/>
:	:	kIx,	:
Řadič	řadič	k1gInSc1	řadič
nebo	nebo	k8xC	nebo
řídicí	řídicí	k2eAgFnSc1d1	řídicí
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
součinnost	součinnost	k1gFnSc4	součinnost
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
částí	část	k1gFnPc2	část
procesoru	procesor	k1gInSc2	procesor
dle	dle	k7c2	dle
prováděných	prováděný	k2eAgFnPc2d1	prováděná
strojových	strojový	k2eAgFnPc2d1	strojová
instrukcí	instrukce	k1gFnPc2	instrukce
(	(	kIx(	(
<g/>
jejich	jejich	k3xOp3gNnSc1	jejich
dekódování	dekódování	k1gNnSc1	dekódování
<g/>
,	,	kIx,	,
načítání	načítání	k1gNnSc1	načítání
operandů	operand	k1gInPc2	operand
instrukcí	instrukce	k1gFnPc2	instrukce
z	z	k7c2	z
operační	operační	k2eAgFnSc2d1	operační
paměti	paměť	k1gFnSc2	paměť
a	a	k8xC	a
ukládání	ukládání	k1gNnSc4	ukládání
výsledků	výsledek	k1gInPc2	výsledek
zpracování	zpracování	k1gNnSc2	zpracování
instrukcí	instrukce	k1gFnPc2	instrukce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sada	sada	k1gFnSc1	sada
registrů	registr	k1gInPc2	registr
pro	pro	k7c4	pro
uchování	uchování	k1gNnSc4	uchování
operandů	operand	k1gInPc2	operand
a	a	k8xC	a
mezivýsledků	mezivýsledek	k1gInPc2	mezivýsledek
<g/>
.	.	kIx.	.
</s>
<s>
Přístup	přístup	k1gInSc1	přístup
k	k	k7c3	k
registrům	registrum	k1gNnPc3	registrum
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
rychlejší	rychlý	k2eAgMnSc1d2	rychlejší
než	než	k8xS	než
přístup	přístup	k1gInSc1	přístup
do	do	k7c2	do
operační	operační	k2eAgFnSc2d1	operační
paměti	paměť	k1gFnSc2	paměť
připojené	připojený	k2eAgFnSc2d1	připojená
k	k	k7c3	k
procesoru	procesor	k1gInSc3	procesor
pomocí	pomocí	k7c2	pomocí
sběrnice	sběrnice	k1gFnSc2	sběrnice
<g/>
.	.	kIx.	.
</s>
<s>
Registry	registr	k1gInPc4	registr
dělíme	dělit	k5eAaImIp1nP	dělit
na	na	k7c6	na
obecné	obecná	k1gFnSc6	obecná
(	(	kIx(	(
<g/>
pracovní	pracovní	k2eAgFnSc7d1	pracovní
<g/>
,	,	kIx,	,
univerzální	univerzální	k2eAgFnSc7d1	univerzální
<g/>
)	)	kIx)	)
a	a	k8xC	a
řídící	řídící	k2eAgFnSc4d1	řídící
(	(	kIx(	(
<g/>
např.	např.	kA	např.
čítač	čítač	k1gInSc4	čítač
instrukcí	instrukce	k1gFnPc2	instrukce
<g/>
,	,	kIx,	,
stavové	stavový	k2eAgInPc4d1	stavový
registry	registr	k1gInPc4	registr
<g/>
,	,	kIx,	,
registr	registr	k1gInSc4	registr
vrcholu	vrchol	k1gInSc2	vrchol
zásobníku	zásobník	k1gInSc2	zásobník
<g/>
,	,	kIx,	,
indexregistry	indexregistra	k1gFnSc2	indexregistra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bitová	bitový	k2eAgFnSc1d1	bitová
šířka	šířka	k1gFnSc1	šířka
pracovních	pracovní	k2eAgInPc2d1	pracovní
registrů	registr	k1gInPc2	registr
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
základních	základní	k2eAgFnPc2d1	základní
charakteristik	charakteristika	k1gFnPc2	charakteristika
procesoru	procesor	k1gInSc2	procesor
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
aritmeticko-logických	aritmetickoogický	k2eAgFnPc2d1	aritmeticko-logická
jednotek	jednotka	k1gFnPc2	jednotka
(	(	kIx(	(
<g/>
ALU	ala	k1gFnSc4	ala
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
Arithmetic-Logic	Arithmetic-Logic	k1gMnSc1	Arithmetic-Logic
Unit	Unit	k1gMnSc1	Unit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
provádí	provádět	k5eAaImIp3nS	provádět
nad	nad	k7c7	nad
daty	datum	k1gNnPc7	datum
aritmetické	aritmetický	k2eAgFnPc1d1	aritmetická
a	a	k8xC	a
logické	logický	k2eAgFnPc1d1	logická
operace	operace	k1gFnPc1	operace
<g/>
.	.	kIx.	.
</s>
<s>
Současné	současný	k2eAgInPc1d1	současný
CPU	CPU	kA	CPU
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
také	také	k9	také
matematický	matematický	k2eAgInSc1d1	matematický
koprocesor	koprocesor	k1gInSc1	koprocesor
(	(	kIx(	(
<g/>
FPU	FPU	kA	FPU
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
Floating	Floating	k1gInSc1	Floating
Point	pointa	k1gFnPc2	pointa
Unit	Unita	k1gFnPc2	Unita
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
provádí	provádět	k5eAaImIp3nS	provádět
operace	operace	k1gFnPc4	operace
s	s	k7c7	s
desetinnými	desetinný	k2eAgNnPc7d1	desetinné
čísly	číslo	k1gNnPc7	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Současné	současný	k2eAgNnSc1d1	současné
CPU	cpát	k5eAaImIp1nS	cpát
většinou	většina	k1gFnSc7	většina
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
také	také	k9	také
vektorovou	vektorový	k2eAgFnSc4d1	vektorová
jednotku	jednotka	k1gFnSc4	jednotka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
matematickým	matematický	k2eAgInSc7d1	matematický
koprocesorem	koprocesor	k1gInSc7	koprocesor
optimalizovaným	optimalizovaný	k2eAgInSc7d1	optimalizovaný
pro	pro	k7c4	pro
operace	operace	k1gFnPc4	operace
s	s	k7c7	s
vektory	vektor	k1gInPc7	vektor
desetinných	desetinný	k2eAgNnPc2d1	desetinné
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
používaným	používaný	k2eAgNnSc7d1	používané
vyjádřením	vyjádření	k1gNnSc7	vyjádření
rychlosti	rychlost	k1gFnSc2	rychlost
procesoru	procesor	k1gInSc2	procesor
je	být	k5eAaImIp3nS	být
takt	takt	k1gInSc1	takt
procesoru	procesor	k1gInSc2	procesor
(	(	kIx(	(
<g/>
taktovací	taktovací	k2eAgFnSc1d1	taktovací
frekvence	frekvence	k1gFnSc1	frekvence
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
taktu	takt	k1gInSc6	takt
jsou	být	k5eAaImIp3nP	být
uvnitř	uvnitř	k7c2	uvnitř
procesoru	procesor	k1gInSc2	procesor
provedeny	provést	k5eAaPmNgFnP	provést
přesně	přesně	k6eAd1	přesně
definované	definovaný	k2eAgFnPc1d1	definovaná
operace	operace	k1gFnPc1	operace
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
různým	různý	k2eAgFnPc3d1	různá
technikám	technika	k1gFnPc3	technika
zvýšení	zvýšení	k1gNnSc2	zvýšení
výkonu	výkon	k1gInSc2	výkon
však	však	k9	však
již	již	k6eAd1	již
dnes	dnes	k6eAd1	dnes
není	být	k5eNaImIp3nS	být
frekvence	frekvence	k1gFnSc1	frekvence
rozhodujícím	rozhodující	k2eAgMnSc7d1	rozhodující
faktorem	faktor	k1gMnSc7	faktor
a	a	k8xC	a
nelze	lze	k6eNd1	lze
ji	on	k3xPp3gFnSc4	on
využít	využít	k5eAaPmF	využít
jako	jako	k9	jako
srovnání	srovnání	k1gNnSc4	srovnání
ani	ani	k8xC	ani
mezi	mezi	k7c7	mezi
kompatibilními	kompatibilní	k2eAgInPc7d1	kompatibilní
procesory	procesor	k1gInPc7	procesor
<g/>
.	.	kIx.	.
</s>
<s>
Skutečnou	skutečný	k2eAgFnSc4d1	skutečná
rychlost	rychlost	k1gFnSc4	rychlost
procesu	proces	k1gInSc2	proces
je	být	k5eAaImIp3nS	být
kombinace	kombinace	k1gFnSc1	kombinace
taktovací	taktovací	k2eAgFnSc2d1	taktovací
frekvence	frekvence	k1gFnSc2	frekvence
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
jeho	jeho	k3xOp3gFnPc2	jeho
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
procesorech	procesor	k1gInPc6	procesor
typu	typ	k1gInSc2	typ
RISC	RISC	kA	RISC
trvala	trvat	k5eAaImAgFnS	trvat
každá	každý	k3xTgFnSc1	každý
strojová	strojový	k2eAgFnSc1d1	strojová
instrukce	instrukce	k1gFnSc1	instrukce
právě	právě	k9	právě
jeden	jeden	k4xCgInSc1	jeden
takt	takt	k1gInSc1	takt
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
instrukce	instrukce	k1gFnPc1	instrukce
byly	být	k5eAaImAgFnP	být
velmi	velmi	k6eAd1	velmi
jednoduché	jednoduchý	k2eAgInPc1d1	jednoduchý
(	(	kIx(	(
<g/>
např.	např.	kA	např.
jen	jen	k9	jen
sečtení	sečtení	k1gNnSc2	sečtení
dvou	dva	k4xCgInPc2	dva
registrů	registr	k1gInPc2	registr
nebo	nebo	k8xC	nebo
přesun	přesun	k1gInSc1	přesun
z	z	k7c2	z
registru	registr	k1gInSc2	registr
do	do	k7c2	do
paměti	paměť	k1gFnSc2	paměť
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
v	v	k7c6	v
procesorech	procesor	k1gInPc6	procesor
typu	typ	k1gInSc2	typ
CISC	CISC	kA	CISC
trvala	trvat	k5eAaImAgFnS	trvat
každá	každý	k3xTgFnSc1	každý
strojová	strojový	k2eAgFnSc1d1	strojová
instrukce	instrukce	k1gFnPc4	instrukce
různý	různý	k2eAgInSc1d1	různý
počet	počet	k1gInSc1	počet
taktů	takt	k1gInPc2	takt
(	(	kIx(	(
<g/>
jeden	jeden	k4xCgMnSc1	jeden
až	až	k6eAd1	až
řádově	řádově	k6eAd1	řádově
desítky	desítka	k1gFnPc4	desítka
taktů	takt	k1gInPc2	takt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
instrukce	instrukce	k1gFnPc1	instrukce
byly	být	k5eAaImAgFnP	být
složitější	složitý	k2eAgFnSc4d2	složitější
(	(	kIx(	(
<g/>
např.	např.	kA	např.
vyzvednutí	vyzvednutí	k1gNnSc1	vyzvednutí
operandu	operand	k1gInSc2	operand
z	z	k7c2	z
paměti	paměť	k1gFnSc2	paměť
<g/>
,	,	kIx,	,
sečtení	sečtení	k1gNnSc2	sečtení
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
registru	registr	k1gInSc2	registr
a	a	k8xC	a
uložení	uložení	k1gNnSc2	uložení
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
paměti	paměť	k1gFnSc2	paměť
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
nelze	lze	k6eNd1	lze
tyto	tento	k3xDgFnPc4	tento
dvě	dva	k4xCgFnPc4	dva
různé	různý	k2eAgFnPc4d1	různá
rodiny	rodina	k1gFnPc4	rodina
procesorů	procesor	k1gInPc2	procesor
přímo	přímo	k6eAd1	přímo
porovnávat	porovnávat	k5eAaImF	porovnávat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
taktovací	taktovací	k2eAgFnSc2d1	taktovací
frekvence	frekvence	k1gFnSc2	frekvence
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
však	však	k9	však
rozdíly	rozdíl	k1gInPc1	rozdíl
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
architekturami	architektura	k1gFnPc7	architektura
stírají	stírat	k5eAaImIp3nP	stírat
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
díky	díky	k7c3	díky
superskalární	superskalární	k2eAgFnSc3d1	superskalární
architektuře	architektura	k1gFnSc3	architektura
může	moct	k5eAaImIp3nS	moct
CISC	CISC	kA	CISC
procesor	procesor	k1gInSc1	procesor
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
takt	takt	k1gInSc4	takt
vykonat	vykonat	k5eAaPmF	vykonat
i	i	k9	i
několik	několik	k4yIc4	několik
složitých	složitý	k2eAgFnPc2d1	složitá
instrukcí	instrukce	k1gFnPc2	instrukce
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
RISC	RISC	kA	RISC
procesory	procesor	k1gInPc1	procesor
mohou	moct	k5eAaImIp3nP	moct
vykonat	vykonat	k5eAaPmF	vykonat
za	za	k7c4	za
takt	takt	k1gInSc4	takt
až	až	k9	až
kolem	kolem	k7c2	kolem
dvanácti	dvanáct	k4xCc2	dvanáct
jednoduchých	jednoduchý	k2eAgFnPc2d1	jednoduchá
instrukcí	instrukce	k1gFnPc2	instrukce
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
procesoru	procesor	k1gInSc6	procesor
implementován	implementován	k2eAgInSc4d1	implementován
pipelining	pipelining	k1gInSc4	pipelining
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
procesoru	procesor	k1gInSc6	procesor
najednou	najednou	k6eAd1	najednou
v	v	k7c6	v
různém	různý	k2eAgInSc6d1	různý
stupni	stupeň	k1gInSc6	stupeň
rozpracování	rozpracování	k1gNnSc2	rozpracování
více	hodně	k6eAd2	hodně
strojových	strojový	k2eAgFnPc2d1	strojová
instrukcí	instrukce	k1gFnPc2	instrukce
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
současně	současně	k6eAd1	současně
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
i	i	k9	i
superskalarita	superskalarita	k1gFnSc1	superskalarita
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
strojové	strojový	k2eAgFnPc1d1	strojová
instrukce	instrukce	k1gFnPc1	instrukce
prováděny	prováděn	k2eAgFnPc1d1	prováděna
mimo	mimo	k7c4	mimo
pořadí	pořadí	k1gNnSc4	pořadí
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
out-of-order	outfrder	k1gInSc1	out-of-order
execution	execution	k1gInSc1	execution
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
pořadí	pořadí	k1gNnSc4	pořadí
provedení	provedení	k1gNnSc2	provedení
strojových	strojový	k2eAgFnPc2d1	strojová
instrukcí	instrukce	k1gFnPc2	instrukce
změněno	změnit	k5eAaPmNgNnS	změnit
(	(	kIx(	(
<g/>
při	při	k7c6	při
zachování	zachování	k1gNnSc6	zachování
správnosti	správnost	k1gFnSc2	správnost
výsledků	výsledek	k1gInPc2	výsledek
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
maximalizaci	maximalizace	k1gFnSc4	maximalizace
využití	využití	k1gNnSc2	využití
všech	všecek	k3xTgFnPc2	všecek
částí	část	k1gFnPc2	část
procesoru	procesor	k1gInSc2	procesor
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
pro	pro	k7c4	pro
zvýšení	zvýšení	k1gNnSc4	zvýšení
výkonu	výkon	k1gInSc2	výkon
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
procesoru	procesor	k1gInSc2	procesor
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
též	též	k9	též
přítomnost	přítomnost	k1gFnSc1	přítomnost
procesorové	procesorový	k2eAgFnSc2d1	procesorová
cache	cach	k1gFnSc2	cach
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
urychluje	urychlovat	k5eAaImIp3nS	urychlovat
přístupy	přístup	k1gInPc4	přístup
do	do	k7c2	do
operační	operační	k2eAgFnSc2d1	operační
paměti	paměť	k1gFnSc2	paměť
typu	typ	k1gInSc2	typ
RAM	RAM	kA	RAM
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
cache	cache	k1gFnSc1	cache
není	být	k5eNaImIp3nS	být
přítomna	přítomen	k2eAgFnSc1d1	přítomna
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
procesor	procesor	k1gInSc1	procesor
při	při	k7c6	při
čtení	čtení	k1gNnSc6	čtení
nebo	nebo	k8xC	nebo
zápisu	zápis	k1gInSc6	zápis
do	do	k7c2	do
paměti	paměť	k1gFnSc2	paměť
čekat	čekat	k5eAaImF	čekat
na	na	k7c4	na
dokončení	dokončení	k1gNnSc4	dokončení
této	tento	k3xDgFnSc2	tento
operace	operace	k1gFnSc2	operace
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
typicky	typicky	k6eAd1	typicky
několik	několik	k4yIc4	několik
taktů	takt	k1gInPc2	takt
sběrnice	sběrnice	k1gFnSc2	sběrnice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
paměť	paměť	k1gFnSc1	paměť
spojuje	spojovat	k5eAaImIp3nS	spojovat
s	s	k7c7	s
procesorem	procesor	k1gInSc7	procesor
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
této	tento	k3xDgFnSc2	tento
sběrnice	sběrnice	k1gFnSc2	sběrnice
je	být	k5eAaImIp3nS	být
typicky	typicky	k6eAd1	typicky
nižší	nízký	k2eAgFnSc1d2	nižší
<g/>
,	,	kIx,	,
než	než	k8xS	než
takt	takt	k1gInSc1	takt
procesoru	procesor	k1gInSc2	procesor
(	(	kIx(	(
<g/>
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
současných	současný	k2eAgInPc2d1	současný
procesorů	procesor	k1gInPc2	procesor
je	být	k5eAaImIp3nS	být
dokonce	dokonce	k9	dokonce
interní	interní	k2eAgInSc1d1	interní
takt	takt	k1gInSc1	takt
procesoru	procesor	k1gInSc2	procesor
násobkem	násobek	k1gInSc7	násobek
této	tento	k3xDgFnSc2	tento
frekvence	frekvence	k1gFnSc2	frekvence
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
toto	tento	k3xDgNnSc1	tento
zdržení	zdržení	k1gNnSc1	zdržení
velmi	velmi	k6eAd1	velmi
významné	významný	k2eAgNnSc1d1	významné
<g/>
.	.	kIx.	.
</s>
<s>
Vřazení	vřazení	k1gNnSc1	vřazení
rychlé	rychlý	k2eAgFnSc2d1	rychlá
cache	cach	k1gFnSc2	cach
mezi	mezi	k7c4	mezi
procesor	procesor	k1gInSc4	procesor
a	a	k8xC	a
paměť	paměť	k1gFnSc1	paměť
může	moct	k5eAaImIp3nS	moct
tyto	tento	k3xDgInPc4	tento
čekací	čekací	k2eAgInPc4d1	čekací
stavy	stav	k1gInPc4	stav
omezit	omezit	k5eAaPmF	omezit
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
úplně	úplně	k6eAd1	úplně
eliminovat	eliminovat	k5eAaBmF	eliminovat
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
úspěšnosti	úspěšnost	k1gFnSc6	úspěšnost
cache	cach	k1gInSc2	cach
v	v	k7c6	v
predikci	predikce	k1gFnSc6	predikce
následujících	následující	k2eAgFnPc2d1	následující
operací	operace	k1gFnPc2	operace
s	s	k7c7	s
pamětí	paměť	k1gFnSc7	paměť
a	a	k8xC	a
také	také	k9	také
na	na	k7c6	na
celkové	celkový	k2eAgFnSc6d1	celková
velikosti	velikost	k1gFnSc6	velikost
cache	cach	k1gInSc2	cach
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
mají	mít	k5eAaImIp3nP	mít
současné	současný	k2eAgInPc1d1	současný
procesory	procesor	k1gInPc1	procesor
integrovánu	integrován	k2eAgFnSc4d1	integrována
cache	cache	k1gFnSc4	cache
ve	v	k7c6	v
velikosti	velikost	k1gFnSc6	velikost
řádově	řádově	k6eAd1	řádově
jednotek	jednotka	k1gFnPc2	jednotka
MiB	MiB	k1gMnPc2	MiB
<g/>
.	.	kIx.	.
</s>
<s>
Cache	Cache	k1gFnSc1	Cache
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
několikastupňová	několikastupňový	k2eAgFnSc1d1	několikastupňová
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
L	L	kA	L
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
L3	L3	k1gFnSc1	L3
–	–	k?	–
čím	co	k3yQnSc7	co
nižší	nízký	k2eAgNnSc1d2	nižší
číslo	číslo	k1gNnSc1	číslo
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
blíže	blízce	k6eAd2	blízce
k	k	k7c3	k
procesoru	procesor	k1gInSc3	procesor
<g/>
)	)	kIx)	)
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
umístěna	umístit	k5eAaPmNgFnS	umístit
i	i	k9	i
na	na	k7c6	na
základní	základní	k2eAgFnSc6d1	základní
desce	deska	k1gFnSc6	deska
počítače	počítač	k1gInSc2	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
výkonnost	výkonnost	k1gFnSc4	výkonnost
cache	cache	k6eAd1	cache
má	mít	k5eAaImIp3nS	mít
též	též	k9	též
vliv	vliv	k1gInSc4	vliv
optimalizace	optimalizace	k1gFnSc2	optimalizace
programu	program	k1gInSc2	program
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
procesoru	procesor	k1gInSc2	procesor
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
faktorů	faktor	k1gInPc2	faktor
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
celkovou	celkový	k2eAgFnSc4d1	celková
rychlost	rychlost	k1gFnSc4	rychlost
počítače	počítač	k1gInSc2	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgInPc4d1	další
faktory	faktor	k1gInPc4	faktor
patří	patřit	k5eAaImIp3nS	patřit
velikost	velikost	k1gFnSc1	velikost
dostupné	dostupný	k2eAgFnSc2d1	dostupná
operační	operační	k2eAgFnSc2d1	operační
paměti	paměť	k1gFnSc2	paměť
RAM	RAM	kA	RAM
<g/>
,	,	kIx,	,
rychlost	rychlost	k1gFnSc1	rychlost
pevného	pevný	k2eAgInSc2d1	pevný
disku	disk	k1gInSc2	disk
<g/>
,	,	kIx,	,
propustnost	propustnost	k1gFnSc1	propustnost
propojujících	propojující	k2eAgFnPc2d1	propojující
sběrnic	sběrnice	k1gFnPc2	sběrnice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
rychlost	rychlost	k1gFnSc1	rychlost
dalších	další	k2eAgInPc2d1	další
vstupně	vstupně	k6eAd1	vstupně
<g/>
/	/	kIx~	/
<g/>
výstupních	výstupní	k2eAgFnPc2d1	výstupní
součástí	součást	k1gFnPc2	součást
počítače	počítač	k1gInSc2	počítač
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
hraní	hraní	k1gNnSc4	hraní
her	hra	k1gFnPc2	hra
například	například	k6eAd1	například
grafická	grafický	k2eAgFnSc1d1	grafická
karta	karta	k1gFnSc1	karta
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
režie	režie	k1gFnSc1	režie
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Historie	historie	k1gFnSc1	historie
procesorů	procesor	k1gInPc2	procesor
Mikroprocesor	mikroprocesor	k1gInSc1	mikroprocesor
System	Syst	k1gInSc7	Syst
on	on	k3xPp3gMnSc1	on
a	a	k8xC	a
chip	chip	k1gMnSc1	chip
</s>
