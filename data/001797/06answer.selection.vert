<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
miniaturizace	miniaturizace	k1gFnSc2	miniaturizace
a	a	k8xC	a
zvyšování	zvyšování	k1gNnSc2	zvyšování
integrace	integrace	k1gFnSc2	integrace
dosáhly	dosáhnout	k5eAaPmAgInP	dosáhnout
stádia	stádium	k1gNnSc2	stádium
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
podstatnou	podstatný	k2eAgFnSc4d1	podstatná
část	část	k1gFnSc4	část
CPU	CPU	kA	CPU
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
umístit	umístit	k5eAaPmF	umístit
do	do	k7c2	do
jediného	jediný	k2eAgInSc2d1	jediný
integrovaného	integrovaný	k2eAgInSc2d1	integrovaný
obvodu	obvod	k1gInSc2	obvod
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
nazýváme	nazývat	k5eAaImIp1nP	nazývat
mikroprocesor	mikroprocesor	k1gInSc4	mikroprocesor
<g/>
.	.	kIx.	.
</s>
