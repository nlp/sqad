<s>
Afrotetra	Afrotetra	k1gFnSc1
botswanská	botswanský	k2eAgFnSc1d1
</s>
<s>
Afrotetra	Afrotetra	k1gFnSc1
botswanská	botswanský	k2eAgFnSc1d1
Afrotetra	Afrotetra	k1gFnSc1
botswanská	botswanský	k2eAgFnSc1d1
chycená	chycený	k2eAgFnSc1d1
v	v	k7c6
horní	horní	k2eAgFnSc6d1
části	část	k1gFnSc6
řeky	řeka	k1gFnSc2
Zambezi	Zambezi	k1gNnSc2
Stupeň	stupeň	k1gInSc4
ohrožení	ohrožení	k1gNnSc2
podle	podle	k7c2
IUCN	IUCN	kA
</s>
<s>
málo	málo	k6eAd1
dotčený	dotčený	k2eAgMnSc1d1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
Říše	říš	k1gFnSc2
</s>
<s>
živočichové	živočich	k1gMnPc1
(	(	kIx(
<g/>
Animalia	Animalia	k1gFnSc1
<g/>
)	)	kIx)
Kmen	kmen	k1gInSc1
</s>
<s>
strunatci	strunatec	k1gMnPc1
(	(	kIx(
<g/>
Chordata	Chordat	k2eAgFnSc1d1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
paprskoploutví	paprskoploutvět	k5eAaImIp3nS,k5eAaPmIp3nS
(	(	kIx(
<g/>
Actinopterygii	Actinopterygie	k1gFnSc3
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
trnobřiší	trnobřiší	k1gNnSc1
(	(	kIx(
<g/>
Characiformes	Characiformes	k1gInSc1
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc1
</s>
<s>
afrotetrovití	afrotetrovitý	k2eAgMnPc1d1
(	(	kIx(
<g/>
Alestiidae	Alestiidae	k1gNnSc7
<g/>
)	)	kIx)
Rod	rod	k1gInSc1
</s>
<s>
afrotetra	afrotetra	k1gFnSc1
(	(	kIx(
<g/>
Hydrocynus	Hydrocynus	k1gInSc1
<g/>
)	)	kIx)
Binomické	binomický	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
</s>
<s>
Hydrocynus	Hydrocynus	k1gInSc1
vittatusCastelnau	vittatusCastelnaus	k1gInSc2
<g/>
,	,	kIx,
1861	#num#	k4
Synonyma	synonymum	k1gNnSc2
</s>
<s>
Hydrocinus	Hydrocinus	k1gMnSc1
vittatus	vittatus	k1gMnSc1
Castelnau	Castelnaa	k1gFnSc4
<g/>
,	,	kIx,
1861	#num#	k4
</s>
<s>
Hydrocion	Hydrocion	k1gInSc1
lineatus	lineatus	k1gInSc1
(	(	kIx(
<g/>
Schlegel	Schlegel	k1gInSc1
<g/>
,	,	kIx,
1863	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Hydrocyon	Hydrocyon	k1gInSc1
forskalii	forskalie	k1gFnSc3
(	(	kIx(
<g/>
non	non	k?
Cuvier	Cuvier	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Hydrocyon	Hydrocyon	k1gMnSc1
lineatus	lineatus	k1gMnSc1
Schlegel	Schlegel	k1gMnSc1
<g/>
,	,	kIx,
1863	#num#	k4
</s>
<s>
Hydrocyon	Hydrocyon	k1gMnSc1
vittatus	vittatus	k1gMnSc1
(	(	kIx(
<g/>
Castelnau	Castelnaus	k1gInSc2
<g/>
,	,	kIx,
1861	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Afrotetra	Afrotetra	k1gFnSc1
botswanská	botswanský	k2eAgFnSc1d1
<g/>
,	,	kIx,
nazývaná	nazývaný	k2eAgFnSc1d1
také	také	k9
binga	bingo	k1gNnPc1
pruhovaná	pruhovaný	k2eAgNnPc1d1
(	(	kIx(
<g/>
Hydrocynus	Hydrocynus	k1gMnSc1
vittatus	vittatus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
sladkovodní	sladkovodní	k2eAgInSc1d1
druh	druh	k1gInSc1
dravé	dravý	k2eAgFnSc2d1
ryby	ryba	k1gFnSc2
z	z	k7c2
čeledi	čeleď	k1gFnSc2
afrotetrovití	afrotetrovitý	k2eAgMnPc1d1
(	(	kIx(
<g/>
Alestiidae	Alestiidae	k1gNnSc7
<g/>
)	)	kIx)
žijící	žijící	k2eAgMnSc1d1
v	v	k7c6
Africe	Afrika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byly	být	k5eAaImAgInP
zaznamenány	zaznamenán	k2eAgInPc1d1
případy	případ	k1gInPc1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
tato	tento	k3xDgFnSc1
ryba	ryba	k1gFnSc1
vyskakuje	vyskakovat	k5eAaImIp3nS
z	z	k7c2
vody	voda	k1gFnSc2
a	a	k8xC
chytá	chytat	k5eAaImIp3nS
letící	letící	k2eAgFnPc4d1
vlaštovky	vlaštovka	k1gFnPc4
obecné	obecná	k1gFnSc2
(	(	kIx(
<g/>
Hirundo	Hirundo	k6eAd1
rustica	rustica	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Afrotetra	Afrotetra	k1gFnSc1
botswanské	botswanský	k2eAgNnSc1d1
je	být	k5eAaImIp3nS
stříbrně	stříbrně	k6eAd1
zbarvená	zbarvený	k2eAgFnSc1d1
ryba	ryba	k1gFnSc1
s	s	k7c7
tenkými	tenký	k2eAgInPc7d1
černými	černý	k2eAgInPc7d1
podélnými	podélný	k2eAgInPc7d1
pruhy	pruh	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
protáhlé	protáhlý	k2eAgNnSc4d1
tělo	tělo	k1gNnSc4
a	a	k8xC
červenou	červený	k2eAgFnSc4d1
rozeklanou	rozeklaný	k2eAgFnSc4d1
ocasní	ocasní	k2eAgFnSc4d1
ploutev	ploutev	k1gFnSc4
s	s	k7c7
černými	černý	k2eAgInPc7d1
konci	konec	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čelisti	čelist	k1gFnPc1
obsahují	obsahovat	k5eAaImIp3nP
osm	osm	k4xCc4
ostrých	ostrý	k2eAgInPc2d1
zubů	zub	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samci	Samek	k1gMnPc1
jsou	být	k5eAaImIp3nP
větší	veliký	k2eAgMnPc1d2
než	než	k8xS
samice	samice	k1gFnPc1
a	a	k8xC
dorůstají	dorůstat	k5eAaImIp3nP
až	až	k9
105	#num#	k4
centimetrů	centimetr	k1gInPc2
(	(	kIx(
<g/>
samice	samice	k1gFnSc1
74	#num#	k4
centimetry	centimetr	k1gInPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dožívají	dožívat	k5eAaImIp3nP
se	se	k3xPyFc4
osmi	osm	k4xCc2
let	léto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c6
velkém	velký	k2eAgNnSc6d1
území	území	k1gNnSc6
Afriky	Afrika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konkrétně	konkrétně	k6eAd1
v	v	k7c6
povodí	povodí	k1gNnSc6
řek	řeka	k1gFnPc2
Niger	Niger	k1gInSc1
<g/>
/	/	kIx~
<g/>
Benue	Benu	k1gFnPc1
<g/>
,	,	kIx,
Ouémé	Ouémý	k2eAgFnPc1d1
<g/>
,	,	kIx,
Senegal	Senegal	k1gInSc1
<g/>
,	,	kIx,
Nil	Nil	k1gInSc1
<g/>
,	,	kIx,
Omo	Omo	k1gFnSc1
<g/>
,	,	kIx,
Kongo	Kongo	k1gNnSc1
<g/>
,	,	kIx,
Lufira	Lufira	k1gFnSc1
<g/>
,	,	kIx,
Lualaba	Lualaba	k1gFnSc1
<g/>
,	,	kIx,
Luapula	Luapula	k1gFnSc1
<g/>
,	,	kIx,
Zambezi	Zambezi	k1gNnSc1
<g/>
,	,	kIx,
Limpopo	Limpopa	k1gFnSc5
<g/>
,	,	kIx,
Ruvuma	Ruvumum	k1gNnPc1
<g/>
,	,	kIx,
Shire	Shir	k1gMnSc5
<g/>
,	,	kIx,
Wami	Wami	k1gNnPc6
a	a	k8xC
v	v	k7c6
jezerech	jezero	k1gNnPc6
Bangweulu	Bangweul	k1gInSc2
<g/>
,	,	kIx,
Tanganika	Tanganika	k1gFnSc1
<g/>
,	,	kIx,
Rukwa	Rukwa	k1gFnSc1
<g/>
,	,	kIx,
Moéro	Moéro	k1gNnSc1
<g/>
,	,	kIx,
Upemba	Upemba	k1gFnSc1
a	a	k8xC
Malagarazi	Malagarah	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nalezena	nalezen	k2eAgFnSc1d1
byla	být	k5eAaImAgFnS
také	také	k9
v	v	k7c6
povodí	povodí	k1gNnSc6
řeky	řeka	k1gFnSc2
Okavango	Okavango	k6eAd1
či	či	k8xC
Pongola	Pongola	k1gFnSc1
a	a	k8xC
v	v	k7c6
přehradních	přehradní	k2eAgFnPc6d1
nádržích	nádrž	k1gFnPc6
Kariba	Karib	k1gMnSc2
a	a	k8xC
Schroda	Schrod	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Hydrocynus	Hydrocynus	k1gMnSc1
vittatus	vittatus	k1gMnSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
↑	↑	k?
The	The	k1gFnPc4
IUCN	IUCN	kA
Red	Red	k1gFnSc1
List	list	k1gInSc1
of	of	k?
Threatened	Threatened	k1gInSc1
Species	species	k1gFnSc1
2021.1	2021.1	k4
<g/>
.	.	kIx.
25	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
BioLib	BioLib	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
–	–	k?
Hydrocynus	Hydrocynus	k1gMnSc1
vittatus	vittatus	k1gMnSc1
(	(	kIx(
<g/>
afrotetra	afrotetra	k1gFnSc1
botswanská	botswanský	k2eAgFnSc1d1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
BioLib	BioLib	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
O	O	kA
<g/>
'	'	kIx"
<g/>
BRIAN	BRIAN	kA
<g/>
,	,	kIx,
G.C.	G.C.	k1gFnSc1
<g/>
,	,	kIx,
Jacobs	Jacobs	k1gInSc1
<g/>
,	,	kIx,
F.	F.	kA
<g/>
,	,	kIx,
Evans	Evans	k1gInSc1
<g/>
,	,	kIx,
S.	S.	kA
W.	W.	kA
&	&	k?
Smit	Smit	k1gInSc1
<g/>
,	,	kIx,
N.	N.	kA
J.	J.	kA
First	First	k1gFnSc1
observation	observation	k1gInSc1
of	of	k?
African	African	k1gInSc1
tigerfish	tigerfish	k1gMnSc1
Hydrocynus	Hydrocynus	k1gMnSc1
vittatus	vittatus	k1gMnSc1
predating	predating	k1gInSc4
on	on	k3xPp3gMnSc1
barn	barn	k1gMnSc1
swallows	swallows	k6eAd1
Hirundo	Hirundo	k6eAd1
rustica	rustica	k6eAd1
in	in	k?
flight	flight	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gMnSc1
of	of	k?
Fish	Fish	k1gMnSc1
Biology	biolog	k1gMnPc7
<g/>
.	.	kIx.
19	#num#	k4
<g/>
.	.	kIx.
prosinec	prosinec	k1gInSc1
2013	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
84	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
1	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
263	#num#	k4
<g/>
–	–	k?
<g/>
266	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Hydrocynus	Hydrocynus	k1gMnSc1
vittatus	vittatus	k1gMnSc1
<g/>
,	,	kIx,
Tiger	Tiger	k1gMnSc1
fish	fish	k1gMnSc1
:	:	kIx,
fisheries	fisheries	k1gMnSc1
<g/>
,	,	kIx,
gamefish	gamefish	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příprava	příprava	k1gFnSc1
vydání	vydání	k1gNnSc2
Rainer	Rainra	k1gFnPc2
Froese	Froese	k1gFnSc2
<g/>
,	,	kIx,
Daniel	Daniel	k1gMnSc1
Pauly	Paula	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
FishBase	FishBas	k1gInSc6
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Živočichové	živočich	k1gMnPc1
</s>
