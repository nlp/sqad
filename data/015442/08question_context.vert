<s>
Afrotetra	Afrotetra	k1gFnSc1
botswanská	botswanský	k2eAgFnSc1d1
<g/>
,	,	kIx,
nazývaná	nazývaný	k2eAgFnSc1d1
také	také	k9
binga	bingo	k1gNnPc1
pruhovaná	pruhovaný	k2eAgNnPc1d1
(	(	kIx(
<g/>
Hydrocynus	Hydrocynus	k1gMnSc1
vittatus	vittatus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
sladkovodní	sladkovodní	k2eAgInSc1d1
druh	druh	k1gInSc1
dravé	dravý	k2eAgFnSc2d1
ryby	ryba	k1gFnSc2
z	z	k7c2
čeledi	čeleď	k1gFnSc2
afrotetrovití	afrotetrovitý	k2eAgMnPc1d1
(	(	kIx(
<g/>
Alestiidae	Alestiidae	k1gNnSc7
<g/>
)	)	kIx)
žijící	žijící	k2eAgMnSc1d1
v	v	k7c6
Africe	Afrika	k1gFnSc6
<g/>
.	.	kIx.
</s>