<p>
<s>
Pionýrský	pionýrský	k2eAgInSc1d1	pionýrský
druh	druh	k1gInSc1	druh
je	být	k5eAaImIp3nS	být
biologický	biologický	k2eAgInSc1d1	biologický
druh	druh	k1gInSc1	druh
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
osídluje	osídlovat	k5eAaImIp3nS	osídlovat
nově	nově	k6eAd1	nově
vzniklá	vzniklý	k2eAgNnPc4d1	vzniklé
stanoviště	stanoviště	k1gNnPc4	stanoviště
<g/>
.	.	kIx.	.
</s>
<s>
Pionýrské	pionýrský	k2eAgInPc1d1	pionýrský
druhy	druh	k1gInPc1	druh
stojí	stát	k5eAaImIp3nP	stát
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
sukcese	sukcese	k1gFnSc2	sukcese
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
jejich	jejich	k3xOp3gInSc7	jejich
příchodem	příchod	k1gInSc7	příchod
na	na	k7c4	na
lokalitu	lokalita	k1gFnSc4	lokalita
místo	místo	k6eAd1	místo
tvoří	tvořit	k5eAaImIp3nP	tvořit
často	často	k6eAd1	často
jen	jen	k9	jen
neživá	živý	k2eNgFnSc1d1	neživá
příroda	příroda	k1gFnSc1	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Pionýrské	pionýrský	k2eAgInPc1d1	pionýrský
druhy	druh	k1gInPc1	druh
připravují	připravovat	k5eAaImIp3nP	připravovat
stanoviště	stanoviště	k1gNnSc4	stanoviště
pro	pro	k7c4	pro
další	další	k2eAgInPc4d1	další
druhy	druh	k1gInPc4	druh
organismů	organismus	k1gInPc2	organismus
a	a	k8xC	a
osidlují	osidlovat	k5eAaImIp3nP	osidlovat
stanoviště	stanoviště	k1gNnPc1	stanoviště
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jiným	jiný	k2eAgMnPc3d1	jiný
druhům	druh	k1gMnPc3	druh
nevyhovují	vyhovovat	k5eNaImIp3nP	vyhovovat
<g/>
.	.	kIx.	.
</s>
<s>
Pionýrskými	pionýrský	k2eAgInPc7d1	pionýrský
druhy	druh	k1gInPc7	druh
jsou	být	k5eAaImIp3nP	být
nejčastěji	často	k6eAd3	často
míněny	míněn	k2eAgFnPc1d1	míněna
rostliny	rostlina	k1gFnPc1	rostlina
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
nižší	nízký	k2eAgFnPc1d2	nižší
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
pionýrští	pionýrský	k2eAgMnPc1d1	pionýrský
živočichové	živočich	k1gMnPc1	živočich
nebo	nebo	k8xC	nebo
pionýrské	pionýrský	k2eAgInPc1d1	pionýrský
mikroorganismy	mikroorganismus	k1gInPc1	mikroorganismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Název	název	k1gInSc1	název
==	==	k?	==
</s>
</p>
<p>
<s>
Pionýrský	pionýrský	k2eAgInSc1d1	pionýrský
druh	druh	k1gInSc1	druh
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
pojmu	pojem	k1gInSc2	pojem
pionýr	pionýr	k1gMnSc1	pionýr
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
má	mít	k5eAaImIp3nS	mít
více	hodně	k6eAd2	hodně
významů	význam	k1gInPc2	význam
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
průkopník	průkopník	k1gMnSc1	průkopník
–	–	k?	–
osoba	osoba	k1gFnSc1	osoba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
první	první	k4xOgFnSc1	první
provádí	provádět	k5eAaImIp3nS	provádět
nějakou	nějaký	k3yIgFnSc4	nějaký
činnost	činnost	k1gFnSc4	činnost
nebo	nebo	k8xC	nebo
prosazuje	prosazovat	k5eAaImIp3nS	prosazovat
nějakou	nějaký	k3yIgFnSc4	nějaký
myšlenku	myšlenka	k1gFnSc4	myšlenka
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
jsou	být	k5eAaImIp3nP	být
uváděni	uvádět	k5eAaImNgMnP	uvádět
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
kontextu	kontext	k1gInSc6	kontext
američtí	americký	k2eAgMnPc1d1	americký
pionýři	pionýr	k1gMnPc1	pionýr
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
postupně	postupně	k6eAd1	postupně
osídlovali	osídlovat	k5eAaImAgMnP	osídlovat
západní	západní	k2eAgFnSc4d1	západní
část	část	k1gFnSc4	část
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
pionýrské	pionýrský	k2eAgInPc1d1	pionýrský
organismy	organismus	k1gInPc1	organismus
obsazují	obsazovat	k5eAaImIp3nP	obsazovat
volné	volný	k2eAgFnPc4d1	volná
prostory	prostora	k1gFnPc4	prostora
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Pionýrský	pionýrský	k2eAgInSc1d1	pionýrský
druh	druh	k1gInSc1	druh
často	často	k6eAd1	často
obsazuje	obsazovat	k5eAaImIp3nS	obsazovat
lokalitu	lokalita	k1gFnSc4	lokalita
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
kalamitě	kalamita	k1gFnSc3	kalamita
a	a	k8xC	a
lokalita	lokalita	k1gFnSc1	lokalita
je	být	k5eAaImIp3nS	být
dočasně	dočasně	k6eAd1	dočasně
neosídlená	osídlený	k2eNgFnSc1d1	neosídlená
<g/>
.	.	kIx.	.
</s>
<s>
Neosídlené	osídlený	k2eNgNnSc1d1	neosídlené
stanoviště	stanoviště	k1gNnSc1	stanoviště
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
častěji	často	k6eAd2	často
nepříznivé	příznivý	k2eNgNnSc1d1	nepříznivé
nevhodnou	vhodný	k2eNgFnSc7d1	nevhodná
kvalitou	kvalita	k1gFnSc7	kvalita
půdy	půda	k1gFnSc2	půda
s	s	k7c7	s
málo	málo	k6eAd1	málo
živinami	živina	k1gFnPc7	živina
a	a	k8xC	a
nevhodnými	vhodný	k2eNgFnPc7d1	nevhodná
klimatickými	klimatický	k2eAgFnPc7d1	klimatická
podmínkami	podmínka	k1gFnPc7	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Pionýrské	pionýrský	k2eAgInPc1d1	pionýrský
druhy	druh	k1gInPc1	druh
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
otužilé	otužilý	k2eAgFnPc1d1	otužilá
rostliny	rostlina	k1gFnPc1	rostlina
s	s	k7c7	s
adaptací	adaptace	k1gFnSc7	adaptace
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
dlouhé	dlouhý	k2eAgInPc1d1	dlouhý
kořeny	kořen	k1gInPc1	kořen
<g/>
,	,	kIx,	,
kořenové	kořenový	k2eAgFnPc1d1	kořenová
hlízky	hlízka	k1gFnPc1	hlízka
obsahující	obsahující	k2eAgFnPc4d1	obsahující
nitrofilní	nitrofilní	k2eAgFnPc4d1	nitrofilní
baktérie	baktérie	k1gFnPc4	baktérie
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
využívají	využívat	k5eAaImIp3nP	využívat
lépe	dobře	k6eAd2	dobře
vodu	voda	k1gFnSc4	voda
a	a	k8xC	a
neztrácejí	ztrácet	k5eNaImIp3nP	ztrácet
ji	on	k3xPp3gFnSc4	on
nadměrnou	nadměrný	k2eAgFnSc7d1	nadměrná
transpirací	transpirace	k1gFnSc7	transpirace
<g/>
.	.	kIx.	.
</s>
<s>
Pionýrský	pionýrský	k2eAgInSc1d1	pionýrský
druh	druh	k1gInSc1	druh
zánikem	zánik	k1gInSc7	zánik
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
prostředí	prostředí	k1gNnSc1	prostředí
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
vhodnější	vhodný	k2eAgNnSc1d2	vhodnější
pro	pro	k7c4	pro
kolonizaci	kolonizace	k1gFnSc4	kolonizace
náročnějšími	náročný	k2eAgInPc7d2	náročnější
organismy	organismus	k1gInPc7	organismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k8xS	jako
pionýrský	pionýrský	k2eAgInSc1d1	pionýrský
druh	druh	k1gInSc1	druh
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
označovány	označovat	k5eAaImNgFnP	označovat
traviny	travina	k1gFnPc1	travina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
rostou	růst	k5eAaImIp3nP	růst
na	na	k7c6	na
písečných	písečný	k2eAgFnPc6d1	písečná
dunách	duna	k1gFnPc6	duna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kamenitějších	kamenitý	k2eAgFnPc6d2	kamenitější
a	a	k8xC	a
vlhčích	vlhký	k2eAgFnPc6d2	vlhčí
podmínkách	podmínka	k1gFnPc6	podmínka
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
obvykle	obvykle	k6eAd1	obvykle
lišejníky	lišejník	k1gInPc1	lišejník
a	a	k8xC	a
řasy	řasa	k1gFnPc1	řasa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Typickými	typický	k2eAgInPc7d1	typický
pionýrskými	pionýrský	k2eAgInPc7d1	pionýrský
organismy	organismus	k1gInPc7	organismus
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
r-stratégové	rtratégový	k2eAgFnPc1d1	r-stratégový
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
r-stratég	rtratég	k1gInSc1	r-stratég
je	být	k5eAaImIp3nS	být
označován	označovat	k5eAaImNgInS	označovat
organismus	organismus	k1gInSc1	organismus
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
životní	životní	k2eAgFnSc6d1	životní
strategii	strategie	k1gFnSc6	strategie
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
vyšší	vysoký	k2eAgInSc4d2	vyšší
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
rozmnožování	rozmnožování	k1gNnSc4	rozmnožování
a	a	k8xC	a
mobilitu	mobilita	k1gFnSc4	mobilita
potomstva	potomstvo	k1gNnSc2	potomstvo
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
kvalita	kvalita	k1gFnSc1	kvalita
a	a	k8xC	a
konkurenceschopnost	konkurenceschopnost	k1gFnSc1	konkurenceschopnost
je	být	k5eAaImIp3nS	být
odsunuta	odsunout	k5eAaPmNgFnS	odsunout
do	do	k7c2	do
pozadí	pozadí	k1gNnSc2	pozadí
<g/>
.	.	kIx.	.
</s>
<s>
Písmeno	písmeno	k1gNnSc1	písmeno
"	"	kIx"	"
<g/>
r	r	kA	r
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
matematický	matematický	k2eAgInSc1d1	matematický
symbol	symbol	k1gInSc1	symbol
pro	pro	k7c4	pro
rychlost	rychlost	k1gFnSc4	rychlost
reprodukce	reprodukce	k1gFnSc2	reprodukce
(	(	kIx(	(
<g/>
rate	rate	k1gInSc1	rate
of	of	k?	of
reproduction	reproduction	k1gInSc1	reproduction
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
početným	početný	k2eAgNnSc7d1	početné
potomstvem	potomstvo	k1gNnSc7	potomstvo
r-stratéga	rtratéga	k1gFnSc1	r-stratéga
panuje	panovat	k5eAaImIp3nS	panovat
vysoká	vysoký	k2eAgFnSc1d1	vysoká
úmrtnost	úmrtnost	k1gFnSc1	úmrtnost
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
živočichů	živočich	k1gMnPc2	živočich
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
relativně	relativně	k6eAd1	relativně
mnoho	mnoho	k4c4	mnoho
mláďat	mládě	k1gNnPc2	mládě
a	a	k8xC	a
zredukovanou	zredukovaný	k2eAgFnSc4d1	zredukovaná
péči	péče	k1gFnSc4	péče
o	o	k7c4	o
ně	on	k3xPp3gMnPc4	on
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rostliny	rostlina	k1gFnPc1	rostlina
==	==	k?	==
</s>
</p>
<p>
<s>
Rostliny	rostlina	k1gFnPc1	rostlina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
speciálně	speciálně	k6eAd1	speciálně
přizpůsobené	přizpůsobený	k2eAgFnPc1d1	přizpůsobená
k	k	k7c3	k
extrémům	extrém	k1gInPc3	extrém
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
přizpůsobeny	přizpůsobit	k5eAaPmNgInP	přizpůsobit
daným	daný	k2eAgInSc7d1	daný
podmínkám	podmínka	k1gFnPc3	podmínka
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
převládnou	převládnout	k5eAaPmIp3nP	převládnout
nad	nad	k7c7	nad
jinými	jiný	k2eAgFnPc7d1	jiná
rostlinami	rostlina	k1gFnPc7	rostlina
a	a	k8xC	a
vytvořit	vytvořit	k5eAaPmF	vytvořit
klimaxové	klimaxový	k2eAgNnSc4d1	klimaxové
(	(	kIx(	(
<g/>
vrcholové	vrcholový	k2eAgNnSc4d1	vrcholové
<g/>
)	)	kIx)	)
společenství	společenství	k1gNnSc4	společenství
a	a	k8xC	a
zcela	zcela	k6eAd1	zcela
specifické	specifický	k2eAgFnPc4d1	specifická
biocenózy	biocenóza	k1gFnPc4	biocenóza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pionýrské	pionýrský	k2eAgInPc4d1	pionýrský
druhy	druh	k1gInPc4	druh
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaPmF	nalézt
také	také	k9	také
v	v	k7c6	v
sekundární	sekundární	k2eAgFnSc6d1	sekundární
sukcesi	sukcese	k1gFnSc6	sukcese
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
stanoviště	stanoviště	k1gNnPc1	stanoviště
po	po	k7c6	po
událostech	událost	k1gFnPc6	událost
jako	jako	k8xS	jako
lesní	lesní	k2eAgInSc4d1	lesní
požár	požár	k1gInSc4	požár
<g/>
,	,	kIx,	,
odlesňování	odlesňování	k1gNnSc3	odlesňování
nebo	nebo	k8xC	nebo
vyčištění	vyčištění	k1gNnSc3	vyčištění
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
rychle	rychle	k6eAd1	rychle
kolonizují	kolonizovat	k5eAaBmIp3nP	kolonizovat
volné	volný	k2eAgInPc4d1	volný
prostory	prostor	k1gInPc4	prostor
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgInP	být
dříve	dříve	k6eAd2	dříve
porostlé	porostlý	k2eAgInPc1d1	porostlý
jinou	jiný	k2eAgFnSc7d1	jiná
vegetací	vegetace	k1gFnSc7	vegetace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Běžnými	běžný	k2eAgInPc7d1	běžný
příklady	příklad	k1gInPc7	příklad
rostlin	rostlina	k1gFnPc2	rostlina
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
oblastech	oblast	k1gFnPc6	oblast
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Borovice	borovice	k1gFnSc1	borovice
lesní	lesní	k2eAgFnSc1d1	lesní
-	-	kIx~	-
Pinus	Pinus	k1gInSc1	Pinus
sylvestris	sylvestris	k1gFnSc2	sylvestris
</s>
</p>
<p>
<s>
Bříza	bříza	k1gFnSc1	bříza
-	-	kIx~	-
Betula	Betula	k1gFnSc1	Betula
spp	spp	k?	spp
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vrbovka	vrbovka	k1gFnSc1	vrbovka
úzkolistá	úzkolistý	k2eAgFnSc1d1	úzkolistá
Epilobium	Epilobium	k1gNnSc4	Epilobium
angustifolium	angustifolium	k1gNnSc4	angustifolium
</s>
</p>
<p>
<s>
Malina	Malina	k1gMnSc1	Malina
-	-	kIx~	-
Rubus	Rubus	k1gMnSc1	Rubus
spp	spp	k?	spp
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vřesovcovité	Vřesovcovitý	k2eAgFnSc3d1	Vřesovcovitý
-	-	kIx~	-
Ericaceae	Ericaceae	k1gFnSc3	Ericaceae
spp	spp	k?	spp
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Příklady	příklad	k1gInPc1	příklad
===	===	k?	===
</s>
</p>
<p>
<s>
Příklady	příklad	k1gInPc1	příklad
pionýrských	pionýrský	k2eAgFnPc2d1	Pionýrská
rostlin	rostlina	k1gFnPc2	rostlina
a	a	k8xC	a
fotosyntetizujících	fotosyntetizující	k2eAgInPc2d1	fotosyntetizující
organismů	organismus	k1gInPc2	organismus
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
kolonizují	kolonizovat	k5eAaBmIp3nP	kolonizovat
tyto	tyt	k2eAgNnSc4d1	tyto
stanoviště	stanoviště	k1gNnSc4	stanoviště
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
kamenité	kamenitý	k2eAgNnSc1d1	kamenité
prostředí	prostředí	k1gNnSc1	prostředí
-	-	kIx~	-
modro-zelené	modroelený	k2eAgFnPc1d1	modro-zelená
bakterie	bakterie	k1gFnPc1	bakterie
<g/>
,	,	kIx,	,
sinice	sinice	k1gFnPc1	sinice
<g/>
,	,	kIx,	,
lišejníky	lišejník	k1gInPc1	lišejník
<g/>
,	,	kIx,	,
mechy	mech	k1gInPc1	mech
</s>
</p>
<p>
<s>
písčité	písčitý	k2eAgNnSc1d1	písčité
prostředí	prostředí	k1gNnSc1	prostředí
(	(	kIx(	(
<g/>
pobřeží	pobřeží	k1gNnSc1	pobřeží
<g/>
,	,	kIx,	,
pohyblivé	pohyblivý	k2eAgInPc1d1	pohyblivý
písky	písek	k1gInPc1	písek
<g/>
)	)	kIx)	)
<g/>
-	-	kIx~	-
ječmenice	ječmenice	k1gFnSc1	ječmenice
písečná	písečný	k2eAgFnSc1d1	písečná
(	(	kIx(	(
<g/>
Leymus	Leymus	k1gMnSc1	Leymus
arenarius	arenarius	k1gMnSc1	arenarius
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pýrovník	pýrovník	k1gInSc4	pýrovník
psí	psí	k2eAgInSc4d1	psí
(	(	kIx(	(
<g/>
Agropyron	Agropyron	k1gInSc4	Agropyron
pungens	pungensa	k1gFnPc2	pungensa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kamýš	kamýš	k1gMnSc1	kamýš
(	(	kIx(	(
<g/>
Ammophila	Ammophila	k1gFnSc1	Ammophila
breviligulata	breviligule	k1gNnPc4	breviligule
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
slaná	slaný	k2eAgFnSc1d1	slaná
voda	voda	k1gFnSc1	voda
-	-	kIx~	-
zelené	zelený	k2eAgFnPc1d1	zelená
řasy	řasa	k1gFnPc1	řasa
<g/>
,	,	kIx,	,
vocha	vocha	k1gMnSc1	vocha
(	(	kIx(	(
<g/>
Zostera	Zoster	k1gMnSc4	Zoster
spp	spp	k?	spp
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
slanorožec	slanorožec	k1gInSc1	slanorožec
virginský	virginský	k2eAgInSc1d1	virginský
(	(	kIx(	(
<g/>
Salicornia	Salicornium	k1gNnSc2	Salicornium
virginica	virginic	k1gInSc2	virginic
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Spartina	Spartin	k2eAgFnSc1d1	Spartin
×	×	k?	×
townsendii	townsendie	k1gFnSc4	townsendie
a	a	k8xC	a
Spartina	Spartin	k2eAgNnPc4d1	Spartin
anglica	anglicum	k1gNnPc4	anglicum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
sladká	sladký	k2eAgFnSc1d1	sladká
voda	voda	k1gFnSc1	voda
-	-	kIx~	-
řasy	řasa	k1gFnPc1	řasa
<g/>
,	,	kIx,	,
mechy	mech	k1gInPc1	mech
<g/>
,	,	kIx,	,
zákruticha	zákrutich	k1gMnSc2	zákrutich
americká	americký	k2eAgFnSc1d1	americká
(	(	kIx(	(
<g/>
Vallisneria	Vallisnerium	k1gNnSc2	Vallisnerium
americana	americana	k1gFnSc1	americana
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
písek	písek	k1gInSc1	písek
-	-	kIx~	-
i	i	k9	i
přes	přes	k7c4	přes
nepřízeň	nepřízeň	k1gFnSc4	nepřízeň
počasí	počasí	k1gNnSc2	počasí
v	v	k7c6	v
písku	písek	k1gInSc6	písek
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
kolonie	kolonie	k1gFnSc1	kolonie
tvořená	tvořený	k2eAgFnSc1d1	tvořená
řasami	řasa	k1gFnPc7	řasa
a	a	k8xC	a
bakteriemi	bakterie	k1gFnPc7	bakterie
<g/>
,	,	kIx,	,
chráněné	chráněný	k2eAgFnSc2d1	chráněná
biofilmem	biofilm	k1gInSc7	biofilm
a	a	k8xC	a
slizovitými	slizovitý	k2eAgInPc7d1	slizovitý
látky	látka	k1gFnSc2	látka
vyrobené	vyrobený	k2eAgFnPc1d1	vyrobená
řasy	řasa	k1gFnPc1	řasa
<g/>
,	,	kIx,	,
bakterie	bakterie	k1gFnPc1	bakterie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Fauna	fauna	k1gFnSc1	fauna
==	==	k?	==
</s>
</p>
<p>
<s>
Fauna	fauna	k1gFnSc1	fauna
obvykle	obvykle	k6eAd1	obvykle
nezpůsobuje	způsobovat	k5eNaImIp3nS	způsobovat
počáteční	počáteční	k2eAgFnSc4d1	počáteční
kolonizaci	kolonizace	k1gFnSc4	kolonizace
území	území	k1gNnSc2	území
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kolonizuje	kolonizovat	k5eAaBmIp3nS	kolonizovat
prostor	prostor	k1gInSc4	prostor
po	po	k7c6	po
rostlinách	rostlina	k1gFnPc6	rostlina
a	a	k8xC	a
houbách	houba	k1gFnPc6	houba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oceány	oceán	k1gInPc1	oceán
a	a	k8xC	a
moře	moře	k1gNnSc1	moře
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
často	často	k6eAd1	často
krill	krill	k1gInSc1	krill
</s>
</p>
<p>
<s>
===	===	k?	===
Taxonomické	taxonomický	k2eAgNnSc1d1	taxonomické
dělení	dělení	k1gNnSc1	dělení
===	===	k?	===
</s>
</p>
<p>
<s>
nižší	nízký	k2eAgInPc1d2	nižší
rostlinylišejníky	rostlinylišejník	k1gInPc1	rostlinylišejník
</s>
</p>
<p>
<s>
mechy	mech	k1gInPc1	mech
</s>
</p>
<p>
<s>
zelené	zelený	k2eAgNnSc1d1	zelené
řasyvyšší	řasyvyšší	k1gNnSc1	řasyvyšší
rostlinybříza	rostlinybříz	k1gMnSc2	rostlinybříz
</s>
</p>
<p>
<s>
vrbovka	vrbovka	k1gFnSc1	vrbovka
úzkolistá	úzkolistý	k2eAgFnSc1d1	úzkolistá
</s>
</p>
<p>
<s>
malina	malina	k1gFnSc1	malina
</s>
</p>
<p>
<s>
paulovnie	paulovnie	k1gFnSc1	paulovnie
plstnatá	plstnatý	k2eAgFnSc1d1	plstnatá
</s>
</p>
<p>
<s>
trávy	tráva	k1gFnPc1	tráva
<g/>
,	,	kIx,	,
vřesy	vřes	k1gInPc1	vřes
a	a	k8xC	a
kosodřevinyživočichovéropucha	kosodřevinyživočichovéropucha	k1gFnSc1	kosodřevinyživočichovéropucha
zelená	zelený	k2eAgFnSc1d1	zelená
</s>
</p>
<p>
<s>
srnec	srnec	k1gMnSc1	srnec
obecnýmikroorganismysinice	obecnýmikroorganismysinice	k1gFnSc2	obecnýmikroorganismysinice
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Sukcese	sukcese	k1gFnSc1	sukcese
</s>
</p>
<p>
<s>
Teorie	teorie	k1gFnSc1	teorie
Gaia	Gai	k1gInSc2	Gai
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Pionýrský	pionýrský	k2eAgInSc4d1	pionýrský
druh	druh	k1gInSc4	druh
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
</s>
</p>
