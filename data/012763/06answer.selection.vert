<s>
Píseň	píseň	k1gFnSc1	píseň
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
divadelní	divadelní	k2eAgFnSc2d1	divadelní
hry	hra	k1gFnSc2	hra
Josefa	Josef	k1gMnSc2	Josef
Kajetána	Kajetán	k1gMnSc2	Kajetán
Tyla	tyla	k1gFnSc1	tyla
Fidlovačka	Fidlovačka	k1gFnSc1	Fidlovačka
aneb	aneb	k?	aneb
Žádný	žádný	k3yNgInSc4	žádný
hněv	hněv	k1gInSc4	hněv
a	a	k8xC	a
žádná	žádný	k3yNgFnSc1	žádný
rvačka	rvačka	k1gFnSc1	rvačka
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
uvedené	uvedený	k2eAgFnPc1d1	uvedená
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
dne	den	k1gInSc2	den
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1834	[number]	k4	1834
<g/>
.	.	kIx.	.
</s>
