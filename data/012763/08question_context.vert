<s>
První	první	k4xOgFnSc1	první
sloka	sloka	k1gFnSc1	sloka
písně	píseň	k1gFnSc2	píseň
Kde	kde	k6eAd1	kde
domov	domov	k1gInSc4	domov
můj	můj	k3xOp1gInSc4	můj
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
národní	národní	k2eAgFnSc1d1	národní
hymna	hymna	k1gFnSc1	hymna
<g/>
.	.	kIx.	.
</s>
<s>
Užívá	užívat	k5eAaImIp3nS	užívat
se	se	k3xPyFc4	se
od	od	k7c2	od
rozpadu	rozpad	k1gInSc2	rozpad
Československa	Československo	k1gNnSc2	Československo
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
však	však	k9	však
byla	být	k5eAaImAgFnS	být
oficiálně	oficiálně	k6eAd1	oficiálně
prohlášena	prohlásit	k5eAaPmNgFnS	prohlásit
i	i	k9	i
za	za	k7c4	za
hymnu	hymna	k1gFnSc4	hymna
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
uvnitř	uvnitř	k7c2	uvnitř
federace	federace	k1gFnSc2	federace
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
rozdělením	rozdělení	k1gNnSc7	rozdělení
tvořila	tvořit	k5eAaImAgFnS	tvořit
zároveň	zároveň	k6eAd1	zároveň
první	první	k4xOgFnSc4	první
část	část	k1gFnSc4	část
československé	československý	k2eAgFnSc2d1	Československá
hymny	hymna	k1gFnSc2	hymna
<g/>
,	,	kIx,	,
následována	následován	k2eAgFnSc1d1	následována
první	první	k4xOgFnSc7	první
slokou	sloka	k1gFnSc7	sloka
nynější	nynější	k2eAgFnSc2d1	nynější
hymny	hymna	k1gFnSc2	hymna
slovenské	slovenský	k2eAgNnSc1d1	slovenské
(	(	kIx(	(
<g/>
Nad	nad	k7c7	nad
Tatrou	Tatra	k1gFnSc7	Tatra
sa	sa	k?	sa
blýska	blýska	k1gFnSc1	blýska
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
státní	státní	k2eAgFnSc1d1	státní
hymna	hymna	k1gFnSc1	hymna
Kde	kde	k6eAd1	kde
domov	domov	k1gInSc1	domov
můj	můj	k3xOp1gInSc1	můj
patří	patřit	k5eAaImIp3nS	patřit
podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
1993	[number]	k4	1993
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
mezi	mezi	k7c4	mezi
státní	státní	k2eAgInPc4d1	státní
symboly	symbol	k1gInPc4	symbol
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
ty	ten	k3xDgInPc4	ten
lze	lze	k6eAd1	lze
podle	podle	k7c2	podle
§	§	k?	§
1	[number]	k4	1
zákona	zákon	k1gInSc2	zákon
352	[number]	k4	352
<g/>
/	/	kIx~	/
<g/>
2001	[number]	k4	2001
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
užívat	užívat	k5eAaImF	užívat
jen	jen	k9	jen
vhodným	vhodný	k2eAgInSc7d1	vhodný
a	a	k8xC	a
důstojným	důstojný	k2eAgInSc7d1	důstojný
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgFnSc4d1	státní
hymnu	hymna	k1gFnSc4	hymna
lze	lze	k6eAd1	lze
podle	podle	k7c2	podle
§	§	k?	§
12	[number]	k4	12
zákona	zákon	k1gInSc2	zákon
352	[number]	k4	352
<g/>
/	/	kIx~	/
<g/>
2001	[number]	k4	2001
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
hrát	hrát	k5eAaImF	hrát
i	i	k8xC	i
zpívat	zpívat	k5eAaImF	zpívat
při	při	k7c6	při
státních	státní	k2eAgInPc6d1	státní
svátcích	svátek	k1gInPc6	svátek
a	a	k8xC	a
při	při	k7c6	při
jiných	jiný	k2eAgFnPc6d1	jiná
příležitostech	příležitost	k1gFnPc6	příležitost
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
to	ten	k3xDgNnSc4	ten
obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
divadelní	divadelní	k2eAgFnSc2d1	divadelní
hry	hra	k1gFnSc2	hra
Josefa	Josef	k1gMnSc2	Josef
Kajetána	Kajetán	k1gMnSc2	Kajetán
Tyla	tyla	k1gFnSc1	tyla
Fidlovačka	Fidlovačka	k1gFnSc1	Fidlovačka
aneb	aneb	k?	aneb
Žádný	žádný	k3yNgInSc4	žádný
hněv	hněv	k1gInSc4	hněv
a	a	k8xC	a
žádná	žádný	k3yNgFnSc1	žádný
rvačka	rvačka	k1gFnSc1	rvačka
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
uvedené	uvedený	k2eAgFnPc1d1	uvedená
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
dne	den	k1gInSc2	den
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1834	[number]	k4	1834
<g/>
.	.	kIx.	.
</s>

