<s>
Enceladus	Enceladus	k1gInSc1	Enceladus
(	(	kIx(	(
<g/>
definitivní	definitivní	k2eAgNnSc1d1	definitivní
astronomické	astronomický	k2eAgNnSc1d1	astronomické
označení	označení	k1gNnSc1	označení
Saturn	Saturn	k1gInSc1	Saturn
II	II	kA	II
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
šestý	šestý	k4xOgInSc4	šestý
největší	veliký	k2eAgInSc4d3	veliký
měsíc	měsíc	k1gInSc4	měsíc
planety	planeta	k1gFnSc2	planeta
Saturn	Saturn	k1gInSc1	Saturn
<g/>
.	.	kIx.	.
</s>
<s>
Měsíc	měsíc	k1gInSc1	měsíc
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
téměř	téměř	k6eAd1	téměř
500	[number]	k4	500
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
desetina	desetina	k1gFnSc1	desetina
velikosti	velikost	k1gFnSc2	velikost
největšího	veliký	k2eAgInSc2d3	veliký
měsíce	měsíc	k1gInSc2	měsíc
Saturnu	Saturn	k1gInSc2	Saturn
<g/>
,	,	kIx,	,
Titanu	titan	k1gInSc2	titan
<g/>
.	.	kIx.	.
</s>
<s>
Enceladus	Enceladus	k1gInSc1	Enceladus
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
většiny	většina	k1gFnSc2	většina
pokryt	pokryt	k1gInSc1	pokryt
mladým	mladý	k1gMnPc3	mladý
a	a	k8xC	a
relativně	relativně	k6eAd1	relativně
čistým	čistý	k2eAgInSc7d1	čistý
ledem	led	k1gInSc7	led
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
odráží	odrážet	k5eAaImIp3nS	odrážet
téměř	téměř	k6eAd1	téměř
veškeré	veškerý	k3xTgNnSc4	veškerý
sluneční	sluneční	k2eAgNnSc4d1	sluneční
světlo	světlo	k1gNnSc4	světlo
dopadající	dopadající	k2eAgMnSc1d1	dopadající
na	na	k7c4	na
jeho	jeho	k3xOp3gInSc4	jeho
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
extrémně	extrémně	k6eAd1	extrémně
nízkou	nízký	k2eAgFnSc4d1	nízká
teplotu	teplota	k1gFnSc4	teplota
povrchu	povrch	k1gInSc2	povrch
pohybující	pohybující	k2eAgInSc1d1	pohybující
se	se	k3xPyFc4	se
až	až	k9	až
okolo	okolo	k7c2	okolo
-198	-198	k4	-198
°	°	k?	°
<g/>
C.	C.	kA	C.
I	i	k9	i
přes	přes	k7c4	přes
svou	svůj	k3xOyFgFnSc4	svůj
malou	malý	k2eAgFnSc4d1	malá
velikost	velikost	k1gFnSc4	velikost
se	se	k3xPyFc4	se
na	na	k7c6	na
Enceladu	Encelad	k1gInSc6	Encelad
nachází	nacházet	k5eAaImIp3nS	nacházet
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
povrchových	povrchový	k2eAgInPc2d1	povrchový
útvarů	útvar	k1gInPc2	útvar
různého	různý	k2eAgNnSc2d1	různé
stáří	stáří	k1gNnSc2	stáří
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
oblasti	oblast	k1gFnPc1	oblast
silně	silně	k6eAd1	silně
poseté	posetý	k2eAgInPc4d1	posetý
impaktními	impaktní	k2eAgInPc7d1	impaktní
krátery	kráter	k1gInPc7	kráter
a	a	k8xC	a
tedy	tedy	k9	tedy
staré	starý	k2eAgFnPc1d1	stará
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
oblasti	oblast	k1gFnSc2	oblast
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
před	před	k7c4	před
méně	málo	k6eAd2	málo
než	než	k8xS	než
100	[number]	k4	100
milióny	milión	k4xCgInPc7	milión
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
geologickou	geologický	k2eAgFnSc4d1	geologická
aktivitu	aktivita	k1gFnSc4	aktivita
vyvolávají	vyvolávat	k5eAaImIp3nP	vyvolávat
patrně	patrně	k6eAd1	patrně
slapové	slapový	k2eAgFnPc1d1	slapová
síly	síla	k1gFnPc1	síla
planety	planeta	k1gFnSc2	planeta
Saturn	Saturn	k1gMnSc1	Saturn
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
u	u	k7c2	u
Jupiterových	Jupiterův	k2eAgInPc2d1	Jupiterův
měsíců	měsíc	k1gInPc2	měsíc
Io	Io	k1gMnSc2	Io
a	a	k8xC	a
Europa	Europ	k1gMnSc2	Europ
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
vlastní	vlastní	k2eAgInPc4d1	vlastní
významné	významný	k2eAgInPc4d1	významný
zdroje	zdroj	k1gInPc4	zdroj
tepla	teplo	k1gNnSc2	teplo
z	z	k7c2	z
radioaktivního	radioaktivní	k2eAgInSc2d1	radioaktivní
rozpadu	rozpad	k1gInSc2	rozpad
mít	mít	k5eAaImF	mít
Enceladus	Enceladus	k1gInSc4	Enceladus
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
velikost	velikost	k1gFnSc4	velikost
nemůže	moct	k5eNaImIp3nS	moct
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
zahřívání	zahřívání	k1gNnSc3	zahřívání
přispívá	přispívat	k5eAaImIp3nS	přispívat
i	i	k9	i
rezonanční	rezonanční	k2eAgFnSc1d1	rezonanční
vazba	vazba	k1gFnSc1	vazba
Enceladu	Encelad	k1gInSc2	Encelad
s	s	k7c7	s
měsícem	měsíc	k1gInSc7	měsíc
Dione	Dion	k1gInSc5	Dion
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
1	[number]	k4	1
:	:	kIx,	:
2	[number]	k4	2
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
uvnitř	uvnitř	k7c2	uvnitř
obou	dva	k4xCgInPc2	dva
měsíců	měsíc	k1gInPc2	měsíc
dodatečné	dodatečný	k2eAgFnSc2d1	dodatečná
slapové	slapový	k2eAgFnSc2d1	slapová
síly	síla	k1gFnSc2	síla
<g/>
,	,	kIx,	,
a	a	k8xC	a
spolupůsobit	spolupůsobit	k5eAaImF	spolupůsobit
může	moct	k5eAaImIp3nS	moct
i	i	k9	i
měsíc	měsíc	k1gInSc4	měsíc
Mimas	Mimasa	k1gFnPc2	Mimasa
<g/>
.	.	kIx.	.
</s>
<s>
Vliv	vliv	k1gInSc1	vliv
slapových	slapový	k2eAgFnPc2d1	slapová
sil	síla	k1gFnPc2	síla
by	by	kYmCp3nS	by
však	však	k9	však
nestačil	stačit	k5eNaBmAgInS	stačit
k	k	k7c3	k
roztavení	roztavení	k1gNnSc3	roztavení
ledu	led	k1gInSc2	led
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
vědci	vědec	k1gMnPc1	vědec
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nitro	nitro	k1gNnSc4	nitro
Encelada	Encelada	k1gFnSc1	Encelada
musí	muset	k5eAaImIp3nS	muset
obsahovat	obsahovat	k5eAaImF	obsahovat
i	i	k9	i
jiné	jiný	k2eAgFnPc4d1	jiná
těkavé	těkavý	k2eAgFnPc4d1	těkavá
látky	látka	k1gFnPc4	látka
s	s	k7c7	s
nízkým	nízký	k2eAgInSc7d1	nízký
bodem	bod	k1gInSc7	bod
varu	var	k1gInSc2	var
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ledovém	ledový	k2eAgInSc6d1	ledový
povrchu	povrch	k1gInSc6	povrch
lze	lze	k6eAd1	lze
rozpoznat	rozpoznat	k5eAaPmF	rozpoznat
nejméně	málo	k6eAd3	málo
pět	pět	k4xCc4	pět
různých	různý	k2eAgInPc2d1	různý
typů	typ	k1gInPc2	typ
terénů	terén	k1gInPc2	terén
<g/>
:	:	kIx,	:
četné	četný	k2eAgFnPc1d1	četná
deformace	deformace	k1gFnPc1	deformace
<g/>
,	,	kIx,	,
trhliny	trhlina	k1gFnPc1	trhlina
a	a	k8xC	a
prolákliny	proláklina	k1gFnPc1	proláklina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
málo	málo	k4c4	málo
impaktních	impaktní	k2eAgInPc2d1	impaktní
kráterů	kráter	k1gInPc2	kráter
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
jsou	být	k5eAaImIp3nP	být
mnohé	mnohé	k1gNnSc4	mnohé
přetvořené	přetvořený	k2eAgFnSc2d1	přetvořená
plastickým	plastický	k2eAgNnSc7d1	plastické
tečením	tečení	k1gNnSc7	tečení
povrchových	povrchový	k2eAgFnPc2d1	povrchová
vrstev	vrstva	k1gFnPc2	vrstva
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
kráter	kráter	k1gInSc1	kráter
má	mít	k5eAaImIp3nS	mít
průměr	průměr	k1gInSc4	průměr
asi	asi	k9	asi
35	[number]	k4	35
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jejich	jejich	k3xOp3gFnSc2	jejich
absence	absence	k1gFnSc2	absence
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
povrch	povrch	k1gInSc1	povrch
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
mladý	mladý	k2eAgMnSc1d1	mladý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
do	do	k7c2	do
systému	systém	k1gInSc2	systém
Saturnu	Saturn	k1gInSc2	Saturn
sonda	sonda	k1gFnSc1	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
řadu	řada	k1gFnSc4	řada
blízkých	blízký	k2eAgInPc2d1	blízký
průletů	průlet	k1gInPc2	průlet
okolo	okolo	k7c2	okolo
měsíce	měsíc	k1gInSc2	měsíc
Enceladu	Encelad	k1gInSc2	Encelad
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
spatřit	spatřit	k5eAaPmF	spatřit
jeho	jeho	k3xOp3gInSc4	jeho
povrch	povrch	k1gInSc4	povrch
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
v	v	k7c6	v
nedostupném	dostupný	k2eNgNnSc6d1	nedostupné
rozlišení	rozlišení	k1gNnSc6	rozlišení
a	a	k8xC	a
lépe	dobře	k6eAd2	dobře
pochopit	pochopit	k5eAaPmF	pochopit
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
panují	panovat	k5eAaImIp3nP	panovat
<g/>
.	.	kIx.	.
</s>
<s>
Pozorování	pozorování	k1gNnSc4	pozorování
umožnila	umožnit	k5eAaPmAgFnS	umožnit
objevit	objevit	k5eAaPmF	objevit
mračna	mračna	k1gFnSc1	mračna
vodního	vodní	k2eAgInSc2d1	vodní
ledu	led	k1gInSc2	led
vznášející	vznášející	k2eAgFnSc2d1	vznášející
se	se	k3xPyFc4	se
nad	nad	k7c7	nad
oblastí	oblast	k1gFnSc7	oblast
jižního	jižní	k2eAgInSc2d1	jižní
pólu	pól	k1gInSc2	pól
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
objevení	objevení	k1gNnSc3	objevení
kryovulkanismu	kryovulkanismus	k1gInSc2	kryovulkanismus
vyvrhující	vyvrhující	k2eAgInSc4d1	vyvrhující
z	z	k7c2	z
více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
100	[number]	k4	100
gejzírů	gejzír	k1gInPc2	gejzír
směs	směsa	k1gFnPc2	směsa
vodního	vodní	k2eAgInSc2d1	vodní
ledu	led	k1gInSc2	led
<g/>
,	,	kIx,	,
dalších	další	k2eAgFnPc2d1	další
plynných	plynný	k2eAgFnPc2d1	plynná
látek	látka	k1gFnPc2	látka
a	a	k8xC	a
pevných	pevný	k2eAgFnPc2d1	pevná
částic	částice	k1gFnPc2	částice
včetně	včetně	k7c2	včetně
krystalků	krystalek	k1gInPc2	krystalek
soli	sůl	k1gFnSc2	sůl
<g/>
,	,	kIx,	,
do	do	k7c2	do
okolního	okolní	k2eAgInSc2d1	okolní
vesmíru	vesmír	k1gInSc2	vesmír
rychlostí	rychlost	k1gFnPc2	rychlost
kolem	kolem	k7c2	kolem
200	[number]	k4	200
kilogramů	kilogram	k1gInPc2	kilogram
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
vyvrženého	vyvržený	k2eAgInSc2d1	vyvržený
materiálu	materiál	k1gInSc2	materiál
dopadá	dopadat	k5eAaImIp3nS	dopadat
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
sněhu	sníh	k1gInSc2	sníh
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
má	mít	k5eAaImIp3nS	mít
dostatečně	dostatečně	k6eAd1	dostatečně
vysokou	vysoký	k2eAgFnSc4d1	vysoká
rychlost	rychlost	k1gFnSc4	rychlost
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
z	z	k7c2	z
gravitačního	gravitační	k2eAgNnSc2d1	gravitační
působení	působení	k1gNnSc2	působení
měsíce	měsíc	k1gInSc2	měsíc
unikla	uniknout	k5eAaPmAgNnP	uniknout
<g/>
.	.	kIx.	.
</s>
<s>
Uniklé	uniklý	k2eAgFnPc1d1	uniklá
částice	částice	k1gFnPc1	částice
pak	pak	k6eAd1	pak
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
vznik	vznik	k1gInSc4	vznik
Saturnovo	Saturnův	k2eAgNnSc1d1	Saturnovo
prstenci	prstenec	k1gInSc3	prstenec
E.	E.	kA	E.
Dle	dle	k7c2	dle
výzkumu	výzkum	k1gInSc2	výzkum
chemického	chemický	k2eAgNnSc2d1	chemické
složení	složení	k1gNnSc2	složení
materiálu	materiál	k1gInSc2	materiál
se	se	k3xPyFc4	se
jeví	jevit	k5eAaImIp3nS	jevit
<g/>
,	,	kIx,	,
že	že	k8xS	že
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
složení	složení	k1gNnSc4	složení
komet	kometa	k1gFnPc2	kometa
<g/>
.	.	kIx.	.
</s>
<s>
Výzkumy	výzkum	k1gInPc1	výzkum
navíc	navíc	k6eAd1	navíc
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
měsíce	měsíc	k1gInSc2	měsíc
nachází	nacházet	k5eAaImIp3nS	nacházet
rozsáhlý	rozsáhlý	k2eAgInSc1d1	rozsáhlý
oceán	oceán	k1gInSc1	oceán
tvořený	tvořený	k2eAgInSc1d1	tvořený
kapalnou	kapalný	k2eAgFnSc7d1	kapalná
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Aktivita	aktivita	k1gFnSc1	aktivita
gejzírů	gejzír	k1gInPc2	gejzír
společně	společně	k6eAd1	společně
s	s	k7c7	s
únikem	únik	k1gInSc7	únik
vnitřního	vnitřní	k2eAgNnSc2d1	vnitřní
tepla	teplo	k1gNnSc2	teplo
a	a	k8xC	a
velice	velice	k6eAd1	velice
malým	malý	k2eAgNnSc7d1	malé
množstvím	množství	k1gNnSc7	množství
impaktních	impaktní	k2eAgInPc2d1	impaktní
kráterů	kráter	k1gInPc2	kráter
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
jižního	jižní	k2eAgInSc2d1	jižní
pólů	pól	k1gInPc2	pól
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Enceladus	Enceladus	k1gInSc1	Enceladus
je	být	k5eAaImIp3nS	být
i	i	k9	i
dnes	dnes	k6eAd1	dnes
geologicky	geologicky	k6eAd1	geologicky
aktivním	aktivní	k2eAgNnSc7d1	aktivní
tělesem	těleso	k1gNnSc7	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
mnoho	mnoho	k4c4	mnoho
dalších	další	k2eAgInPc2d1	další
měsíců	měsíc	k1gInPc2	měsíc
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
plynných	plynný	k2eAgMnPc2d1	plynný
obrů	obr	k1gMnPc2	obr
<g/>
,	,	kIx,	,
i	i	k8xC	i
Enceladus	Enceladus	k1gInSc1	Enceladus
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
orbitální	orbitální	k2eAgFnSc6d1	orbitální
rezonanci	rezonance	k1gFnSc6	rezonance
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
s	s	k7c7	s
měsícem	měsíc	k1gInSc7	měsíc
Dione	Dion	k1gInSc5	Dion
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
rezonance	rezonance	k1gFnSc1	rezonance
pak	pak	k6eAd1	pak
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
udržování	udržování	k1gNnSc4	udržování
excentricity	excentricita	k1gFnSc2	excentricita
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
Enceladu	Encelad	k1gInSc2	Encelad
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
působení	působení	k1gNnSc4	působení
slapového	slapový	k2eAgNnSc2d1	slapové
zahřívání	zahřívání	k1gNnSc2	zahřívání
na	na	k7c4	na
vnitřek	vnitřek	k1gInSc4	vnitřek
měsíce	měsíc	k1gInSc2	měsíc
s	s	k7c7	s
následnou	následný	k2eAgFnSc7d1	následná
geologickou	geologický	k2eAgFnSc7d1	geologická
aktivitou	aktivita	k1gFnSc7	aktivita
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Měsíc	měsíc	k1gInSc1	měsíc
objevil	objevit	k5eAaPmAgInS	objevit
28	[number]	k4	28
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1789	[number]	k4	1789
německo-britský	německoritský	k2eAgInSc1d1	německo-britský
astronom	astronom	k1gMnSc1	astronom
William	William	k1gInSc4	William
Herschel	Herschela	k1gFnPc2	Herschela
<g/>
.	.	kIx.	.
</s>
<s>
Pojmenován	pojmenován	k2eAgInSc1d1	pojmenován
byl	být	k5eAaImAgInS	být
podle	podle	k7c2	podle
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
Titánů	Titán	k1gMnPc2	Titán
<g/>
,	,	kIx,	,
Enkelada	Enkelada	k1gFnSc1	Enkelada
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
zahubila	zahubit	k5eAaPmAgFnS	zahubit
bohyně	bohyně	k1gFnSc1	bohyně
Pallas	Pallas	k1gMnSc1	Pallas
Athéna	Athéna	k1gFnSc1	Athéna
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
něho	on	k3xPp3gMnSc4	on
hodila	hodit	k5eAaImAgFnS	hodit
ostrov	ostrov	k1gInSc1	ostrov
Sicílii	Sicílie	k1gFnSc4	Sicílie
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1847	[number]	k4	1847
objevitelův	objevitelův	k2eAgMnSc1d1	objevitelův
syn	syn	k1gMnSc1	syn
<g/>
,	,	kIx,	,
astronom	astronom	k1gMnSc1	astronom
John	John	k1gMnSc1	John
Herschel	Herschel	k1gMnSc1	Herschel
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
okolo	okolo	k7c2	okolo
měsíce	měsíc	k1gInSc2	měsíc
proletěla	proletět	k5eAaPmAgFnS	proletět
dvojice	dvojice	k1gFnSc1	dvojice
amerických	americký	k2eAgFnPc2d1	americká
planetárních	planetární	k2eAgFnPc2d1	planetární
sond	sonda	k1gFnPc2	sonda
Voyager	Voyagra	k1gFnPc2	Voyagra
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
o	o	k7c6	o
vzhledu	vzhled	k1gInSc6	vzhled
měsíce	měsíc	k1gInSc2	měsíc
známo	znám	k2eAgNnSc1d1	známo
jen	jen	k6eAd1	jen
málo	málo	k6eAd1	málo
<g/>
.	.	kIx.	.
</s>
<s>
Enceladus	Enceladus	k1gInSc1	Enceladus
byl	být	k5eAaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
28	[number]	k4	28
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1780	[number]	k4	1780
sirem	sir	k1gMnSc7	sir
Fredrickem	Fredricko	k1gNnSc7	Fredricko
Williamem	William	k1gInSc7	William
Herschelem	Herschel	k1gMnSc7	Herschel
<g/>
,	,	kIx,	,
během	během	k7c2	během
prvního	první	k4xOgNnSc2	první
použití	použití	k1gNnSc2	použití
jeho	jeho	k3xOp3gInSc2	jeho
nového	nový	k2eAgInSc2d1	nový
teleskopu	teleskop	k1gInSc2	teleskop
se	s	k7c7	s
zrcadlem	zrcadlo	k1gNnSc7	zrcadlo
o	o	k7c6	o
šířce	šířka	k1gFnSc6	šířka
1,2	[number]	k4	1,2
metru	metr	k1gInSc2	metr
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
tehdy	tehdy	k6eAd1	tehdy
představoval	představovat	k5eAaImAgInS	představovat
největší	veliký	k2eAgInSc1d3	veliký
teleskop	teleskop	k1gInSc1	teleskop
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
zdánlivá	zdánlivý	k2eAgFnSc1d1	zdánlivá
magnituda	magnituda	k1gFnSc1	magnituda
(	(	kIx(	(
<g/>
HV	HV	kA	HV
=	=	kIx~	=
+11.7	+11.7	k4	+11.7
<g/>
)	)	kIx)	)
a	a	k8xC	a
blízkost	blízkost	k1gFnSc1	blízkost
k	k	k7c3	k
mnohem	mnohem	k6eAd1	mnohem
jasnějšímu	jasný	k2eAgInSc3d2	jasnější
Saturnu	Saturn	k1gInSc3	Saturn
s	s	k7c7	s
prstenci	prstenec	k1gInPc7	prstenec
z	z	k7c2	z
Enceladu	Encelad	k1gInSc2	Encelad
dělala	dělat	k5eAaImAgFnS	dělat
obtížný	obtížný	k2eAgInSc4d1	obtížný
cíl	cíl	k1gInSc4	cíl
pro	pro	k7c4	pro
pozorování	pozorování	k1gNnSc4	pozorování
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
menších	malý	k2eAgInPc2d2	menší
teleskopů	teleskop	k1gInPc2	teleskop
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
mnoho	mnoho	k4c4	mnoho
jiných	jiný	k2eAgFnPc2d1	jiná
Saturnovo	Saturnův	k2eAgNnSc1d1	Saturnovo
měsíců	měsíc	k1gInPc2	měsíc
objevených	objevený	k2eAgFnPc6d1	objevená
před	před	k7c7	před
kosmickými	kosmický	k2eAgNnPc7d1	kosmické
lety	léto	k1gNnPc7	léto
<g/>
,	,	kIx,	,
i	i	k8xC	i
Enceladus	Enceladus	k1gInSc1	Enceladus
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
pozorován	pozorovat	k5eAaImNgInS	pozorovat
během	během	k7c2	během
Saturnovo	Saturnův	k2eAgNnSc4d1	Saturnovo
rovnodennosti	rovnodennost	k1gFnPc4	rovnodennost
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
Země	země	k1gFnSc1	země
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
rovině	rovina	k1gFnSc6	rovina
s	s	k7c7	s
prstenci	prstenec	k1gInPc7	prstenec
obklopujícími	obklopující	k2eAgInPc7d1	obklopující
tuto	tento	k3xDgFnSc4	tento
planetu	planeta	k1gFnSc4	planeta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
ten	ten	k3xDgInSc4	ten
moment	moment	k1gInSc4	moment
je	být	k5eAaImIp3nS	být
totiž	totiž	k9	totiž
měsíce	měsíc	k1gInPc4	měsíc
snazší	snadný	k2eAgInPc4d2	snadnější
pozorovat	pozorovat	k5eAaImF	pozorovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
prstence	prstenec	k1gInPc1	prstenec
jsou	být	k5eAaImIp3nP	být
téměř	téměř	k6eAd1	téměř
neviditelné	viditelný	k2eNgFnPc1d1	neviditelná
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
poklesá	poklesat	k5eAaImIp3nS	poklesat
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
vycházející	vycházející	k2eAgInSc1d1	vycházející
oslnění	oslnění	k1gNnSc3	oslnění
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
objevení	objevení	k1gNnSc2	objevení
až	až	k9	až
po	po	k7c4	po
průlet	průlet	k1gInSc4	průlet
planetárních	planetární	k2eAgFnPc2d1	planetární
sond	sonda	k1gFnPc2	sonda
Voyager	Voyagero	k1gNnPc2	Voyagero
byly	být	k5eAaImAgFnP	být
naše	náš	k3xOp1gFnPc1	náš
znalosti	znalost	k1gFnPc1	znalost
o	o	k7c6	o
Enceladu	Encelad	k1gInSc6	Encelad
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
stejné	stejný	k2eAgFnSc6d1	stejná
<g/>
.	.	kIx.	.
</s>
<s>
Známa	znám	k2eAgFnSc1d1	známa
byla	být	k5eAaImAgFnS	být
pouze	pouze	k6eAd1	pouze
jeho	jeho	k3xOp3gFnSc1	jeho
oběžná	oběžný	k2eAgFnSc1d1	oběžná
charakteristika	charakteristika	k1gFnSc1	charakteristika
a	a	k8xC	a
hrubé	hrubý	k2eAgInPc1d1	hrubý
odhady	odhad	k1gInPc1	odhad
jeho	jeho	k3xOp3gFnSc2	jeho
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
,	,	kIx,	,
hustoty	hustota	k1gFnSc2	hustota
a	a	k8xC	a
albeda	albed	k1gMnSc2	albed
<g/>
.	.	kIx.	.
</s>
<s>
Enceladus	Enceladus	k1gMnSc1	Enceladus
je	být	k5eAaImIp3nS	být
pojmenován	pojmenovat	k5eAaPmNgMnS	pojmenovat
po	po	k7c6	po
obrovi	obr	k1gMnSc6	obr
Enkeladovi	Enkelada	k1gMnSc6	Enkelada
z	z	k7c2	z
řecké	řecký	k2eAgFnSc2d1	řecká
mytologie	mytologie	k1gFnSc2	mytologie
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
ostatních	ostatní	k2eAgInPc2d1	ostatní
prvních	první	k4xOgInPc2	první
sedmi	sedm	k4xCc2	sedm
měsíců	měsíc	k1gInPc2	měsíc
Saturnu	Saturn	k1gInSc2	Saturn
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
navrženo	navrhnout	k5eAaPmNgNnS	navrhnout
synem	syn	k1gMnSc7	syn
Williama	William	k1gMnSc2	William
Herschela	Herschel	k1gMnSc2	Herschel
Johnem	John	k1gMnSc7	John
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
publikace	publikace	k1gFnSc1	publikace
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1847	[number]	k4	1847
Results	Results	k1gInSc1	Results
of	of	k?	of
Astronomical	Astronomical	k1gFnSc2	Astronomical
Observations	Observationsa	k1gFnPc2	Observationsa
made	mad	k1gFnSc2	mad
at	at	k?	at
the	the	k?	the
Cape	capat	k5eAaImIp3nS	capat
of	of	k?	of
Good	Good	k1gInSc1	Good
Hope	Hope	k1gInSc1	Hope
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
Herschel	Herschel	k1gMnSc1	Herschel
zvolil	zvolit	k5eAaPmAgMnS	zvolit
tato	tento	k3xDgNnPc4	tento
jména	jméno	k1gNnPc4	jméno
protože	protože	k8xS	protože
Saturn	Saturn	k1gInSc4	Saturn
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
v	v	k7c6	v
řecké	řecký	k2eAgFnSc6d1	řecká
mytologii	mytologie	k1gFnSc6	mytologie
jako	jako	k9	jako
Kronos	Kronos	k1gMnSc1	Kronos
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
vůdcem	vůdce	k1gMnSc7	vůdce
titánů	titán	k1gMnPc2	titán
<g/>
.	.	kIx.	.
</s>
<s>
Povrchové	povrchový	k2eAgInPc1d1	povrchový
útvary	útvar	k1gInPc1	útvar
na	na	k7c6	na
Enceladu	Encelad	k1gInSc6	Encelad
jsou	být	k5eAaImIp3nP	být
pojmenovány	pojmenován	k2eAgInPc1d1	pojmenován
Mezinárodní	mezinárodní	k2eAgFnSc7d1	mezinárodní
astronomickou	astronomický	k2eAgFnSc7d1	astronomická
unií	unie	k1gFnSc7	unie
(	(	kIx(	(
<g/>
IAU	IAU	kA	IAU
<g/>
)	)	kIx)	)
po	po	k7c6	po
postavách	postava	k1gFnPc6	postava
a	a	k8xC	a
místech	místo	k1gNnPc6	místo
z	z	k7c2	z
překladu	překlad	k1gInSc2	překlad
knihy	kniha	k1gFnSc2	kniha
Tisíc	tisíc	k4xCgInSc1	tisíc
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
noc	noc	k1gFnSc1	noc
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
vyhotovil	vyhotovit	k5eAaPmAgMnS	vyhotovit
Richard	Richard	k1gMnSc1	Richard
Francis	Francis	k1gFnSc2	Francis
Burton	Burton	k1gInSc1	Burton
<g/>
.	.	kIx.	.
</s>
<s>
Impaktní	Impaktní	k2eAgInPc1d1	Impaktní
krátery	kráter	k1gInPc1	kráter
jsou	být	k5eAaImIp3nP	být
pojmenovány	pojmenovat	k5eAaPmNgInP	pojmenovat
po	po	k7c6	po
postavách	postava	k1gFnPc6	postava
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
ostatní	ostatní	k2eAgInPc1d1	ostatní
typy	typ	k1gInPc1	typ
útvarů	útvar	k1gInPc2	útvar
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
dlouhé	dlouhý	k2eAgFnPc4d1	dlouhá
rovné	rovný	k2eAgFnPc4d1	rovná
deprese	deprese	k1gFnPc4	deprese
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
fossae	fossa	k1gFnSc2	fossa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hřebeny	hřeben	k1gInPc1	hřeben
(	(	kIx(	(
<g/>
dorsa	dorsa	k1gFnSc1	dorsa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pláně	pláně	k1gNnSc1	pláně
(	(	kIx(	(
<g/>
planitia	planitia	k1gFnSc1	planitia
<g/>
)	)	kIx)	)
či	či	k8xC	či
dlouhé	dlouhý	k2eAgFnPc4d1	dlouhá
rovnoběžné	rovnoběžný	k2eAgFnPc4d1	rovnoběžná
trhliny	trhlina	k1gFnPc4	trhlina
(	(	kIx(	(
<g/>
sulci	sulce	k1gFnSc4	sulce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
pojmenovány	pojmenovat	k5eAaPmNgInP	pojmenovat
po	po	k7c6	po
místech	místo	k1gNnPc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
2015	[number]	k4	2015
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Enceladu	Encelad	k1gInSc2	Encelad
oficiálně	oficiálně	k6eAd1	oficiálně
pojmenováno	pojmenovat	k5eAaPmNgNnS	pojmenovat
85	[number]	k4	85
povrchových	povrchový	k2eAgInPc2d1	povrchový
útvarů	útvar	k1gInPc2	útvar
<g/>
.	.	kIx.	.
</s>
<s>
Enceladus	Enceladus	k1gInSc1	Enceladus
je	být	k5eAaImIp3nS	být
společně	společně	k6eAd1	společně
s	s	k7c7	s
Dione	Dion	k1gInSc5	Dion
<g/>
,	,	kIx,	,
Tethysem	Tethyso	k1gNnSc7	Tethyso
a	a	k8xC	a
Mimasem	Mimas	k1gInSc7	Mimas
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
velkých	velký	k2eAgInPc2d1	velký
vnitřních	vnitřní	k2eAgInPc2d1	vnitřní
měsíců	měsíc	k1gInPc2	měsíc
Saturnu	Saturn	k1gInSc2	Saturn
<g/>
.	.	kIx.	.
</s>
<s>
Obíhá	obíhat	k5eAaImIp3nS	obíhat
238	[number]	k4	238
000	[number]	k4	000
km	km	kA	km
od	od	k7c2	od
středu	střed	k1gInSc2	střed
Saturnu	Saturn	k1gInSc2	Saturn
a	a	k8xC	a
180	[number]	k4	180
000	[number]	k4	000
km	km	kA	km
od	od	k7c2	od
vrcholku	vrcholek	k1gInSc2	vrcholek
jeho	jeho	k3xOp3gNnPc2	jeho
mračen	mračno	k1gNnPc2	mračno
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
oběžná	oběžný	k2eAgFnSc1d1	oběžná
dráha	dráha	k1gFnSc1	dráha
tak	tak	k6eAd1	tak
leží	ležet	k5eAaImIp3nS	ležet
mezi	mezi	k7c7	mezi
měsíci	měsíc	k1gInPc7	měsíc
Mimas	Mimasa	k1gFnPc2	Mimasa
a	a	k8xC	a
Tethys	Tethysa	k1gFnPc2	Tethysa
<g/>
.	.	kIx.	.
</s>
<s>
Enceladus	Enceladus	k1gInSc1	Enceladus
oběhne	oběhnout	k5eAaPmIp3nS	oběhnout
Saturn	Saturn	k1gInSc4	Saturn
jednou	jednou	k6eAd1	jednou
za	za	k7c4	za
32,9	[number]	k4	32,9
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
dostatečně	dostatečně	k6eAd1	dostatečně
rychle	rychle	k6eAd1	rychle
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jeho	jeho	k3xOp3gInSc1	jeho
pohyb	pohyb	k1gInSc1	pohyb
byl	být	k5eAaImAgInS	být
pozorovatelný	pozorovatelný	k2eAgInSc1d1	pozorovatelný
během	během	k7c2	během
jednoho	jeden	k4xCgNnSc2	jeden
nočního	noční	k2eAgNnSc2d1	noční
pozorování	pozorování	k1gNnSc2	pozorování
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
Enceladus	Enceladus	k1gInSc4	Enceladus
v	v	k7c6	v
oběžné	oběžný	k2eAgFnSc6d1	oběžná
rezonanci	rezonance	k1gFnSc6	rezonance
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
s	s	k7c7	s
měsícem	měsíc	k1gInSc7	měsíc
Dione	Dion	k1gInSc5	Dion
<g/>
;	;	kIx,	;
Enceladus	Enceladus	k1gInSc1	Enceladus
tak	tak	k6eAd1	tak
stihne	stihnout	k5eAaPmIp3nS	stihnout
vždy	vždy	k6eAd1	vždy
dvakrát	dvakrát	k6eAd1	dvakrát
oběhnout	oběhnout	k5eAaPmF	oběhnout
Saturn	Saturn	k1gInSc4	Saturn
než	než	k8xS	než
ho	on	k3xPp3gMnSc4	on
jednou	jednou	k6eAd1	jednou
oběhne	oběhnout	k5eAaPmIp3nS	oběhnout
Dione	Dion	k1gInSc5	Dion
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
rezonance	rezonance	k1gFnSc1	rezonance
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
udržování	udržování	k1gNnSc4	udržování
excentricity	excentricita	k1gFnSc2	excentricita
jeho	jeho	k3xOp3gFnPc4	jeho
oběžné	oběžný	k2eAgFnPc4d1	oběžná
dráhy	dráha	k1gFnPc4	dráha
(	(	kIx(	(
<g/>
0,0047	[number]	k4	0,0047
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Excentricita	excentricita	k1gFnSc1	excentricita
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
pak	pak	k6eAd1	pak
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
přítomnost	přítomnost	k1gFnSc1	přítomnost
slapových	slapový	k2eAgFnPc2d1	slapová
deformací	deformace	k1gFnPc2	deformace
na	na	k7c6	na
Enceladu	Encelad	k1gInSc6	Encelad
a	a	k8xC	a
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
spojenou	spojený	k2eAgFnSc4d1	spojená
produkci	produkce	k1gFnSc4	produkce
tepla	teplo	k1gNnSc2	teplo
vlivem	vlivem	k7c2	vlivem
slapového	slapový	k2eAgNnSc2d1	slapové
zahřívání	zahřívání	k1gNnSc2	zahřívání
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
právě	právě	k9	právě
takto	takto	k6eAd1	takto
produkované	produkovaný	k2eAgNnSc4d1	produkované
teplo	teplo	k1gNnSc4	teplo
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgFnSc7d1	hlavní
příčinou	příčina	k1gFnSc7	příčina
geologické	geologický	k2eAgFnSc2d1	geologická
aktivity	aktivita	k1gFnSc2	aktivita
pozorované	pozorovaný	k2eAgFnSc2d1	pozorovaná
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Enceladu	Encelad	k1gInSc2	Encelad
<g/>
.	.	kIx.	.
</s>
<s>
Enceladus	Enceladus	k1gInSc1	Enceladus
obíhá	obíhat	k5eAaImIp3nS	obíhat
v	v	k7c6	v
nejhustší	hustý	k2eAgFnSc6d3	nejhustší
části	část	k1gFnSc6	část
Saturnova	Saturnův	k2eAgInSc2d1	Saturnův
prstence	prstenec	k1gInSc2	prstenec
E	E	kA	E
<g/>
,	,	kIx,	,
nejvzdálenějšího	vzdálený	k2eAgInSc2d3	nejvzdálenější
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
Saturnovo	Saturnův	k2eAgNnSc1d1	Saturnovo
prstenců	prstenec	k1gInPc2	prstenec
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
současně	současně	k6eAd1	současně
i	i	k9	i
hlavním	hlavní	k2eAgInSc7d1	hlavní
zdrojem	zdroj	k1gInSc7	zdroj
materiálu	materiál	k1gInSc2	materiál
tvořícího	tvořící	k2eAgInSc2d1	tvořící
tento	tento	k3xDgInSc4	tento
prstenec	prstenec	k1gInSc4	prstenec
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
většina	většina	k1gFnSc1	většina
Saturnovo	Saturnův	k2eAgNnSc1d1	Saturnovo
větších	veliký	k2eAgInPc2d2	veliký
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
rotuje	rotovat	k5eAaImIp3nS	rotovat
i	i	k9	i
Enceladus	Enceladus	k1gInSc1	Enceladus
stejně	stejně	k6eAd1	stejně
rychle	rychle	k6eAd1	rychle
kolem	kolem	k7c2	kolem
své	svůj	k3xOyFgFnSc2	svůj
osy	osa	k1gFnSc2	osa
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
obíhá	obíhat	k5eAaImIp3nS	obíhat
mateřskou	mateřský	k2eAgFnSc4d1	mateřská
planetu	planeta	k1gFnSc4	planeta
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
Saturnu	Saturn	k1gInSc3	Saturn
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
stále	stále	k6eAd1	stále
přivrácena	přivrátit	k5eAaPmNgFnS	přivrátit
jen	jen	k9	jen
jedna	jeden	k4xCgFnSc1	jeden
strana	strana	k1gFnSc1	strana
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
pozemskému	pozemský	k2eAgInSc3d1	pozemský
Měsíci	měsíc	k1gInSc3	měsíc
ale	ale	k8xC	ale
Enceladus	Enceladus	k1gInSc1	Enceladus
nevykazuje	vykazovat	k5eNaImIp3nS	vykazovat
významnější	významný	k2eAgFnPc4d2	významnější
známky	známka	k1gFnPc4	známka
librace	librace	k1gFnSc2	librace
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
analýza	analýza	k1gFnSc1	analýza
tvaru	tvar	k1gInSc2	tvar
Ecenaldu	Ecenald	k1gInSc2	Ecenald
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
dříve	dříve	k6eAd2	dříve
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
byl	být	k5eAaImAgInS	být
Enceladus	Enceladus	k1gInSc1	Enceladus
ve	v	k7c4	v
vynucená	vynucený	k2eAgNnPc4d1	vynucené
sekundární	sekundární	k2eAgFnSc4d1	sekundární
spin-orbitální	spinrbitální	k2eAgFnSc4d1	spin-orbitální
libraci	librace	k1gFnSc4	librace
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
librace	librace	k1gFnSc1	librace
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
Enceladu	Encelad	k1gInSc3	Encelad
poskytovat	poskytovat	k5eAaImF	poskytovat
zdroj	zdroj	k1gInSc4	zdroj
tepla	teplo	k1gNnSc2	teplo
navíc	navíc	k6eAd1	navíc
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
prokázáno	prokázat	k5eAaPmNgNnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mračna	mračna	k1gFnSc1	mračna
stoupající	stoupající	k2eAgFnSc1d1	stoupající
z	z	k7c2	z
povrchu	povrch	k1gInSc2	povrch
Enceladu	Encelad	k1gInSc2	Encelad
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
mimochodem	mimochodem	k9	mimochodem
stejné	stejný	k2eAgNnSc4d1	stejné
složení	složení	k1gNnSc4	složení
jako	jako	k8xC	jako
komety	kometa	k1gFnPc4	kometa
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
zdrojem	zdroj	k1gInSc7	zdroj
materiálu	materiál	k1gInSc2	materiál
Saturnova	Saturnův	k2eAgInSc2d1	Saturnův
prstence	prstenec	k1gInSc2	prstenec
E.	E.	kA	E.
Prstenec	prstenec	k1gInSc1	prstenec
E	E	kA	E
je	být	k5eAaImIp3nS	být
nejširší	široký	k2eAgInPc4d3	nejširší
a	a	k8xC	a
nejvzdálenější	vzdálený	k2eAgInPc4d3	nejvzdálenější
z	z	k7c2	z
prstenců	prstenec	k1gInPc2	prstenec
Saturnu	Saturn	k1gInSc2	Saturn
(	(	kIx(	(
<g/>
nicméně	nicméně	k8xC	nicméně
nutno	nutno	k6eAd1	nutno
dodat	dodat	k5eAaPmF	dodat
<g/>
,	,	kIx,	,
že	že	k8xS	že
přibližně	přibližně	k6eAd1	přibližně
100	[number]	k4	100
<g/>
x	x	k?	x
dále	daleko	k6eAd2	daleko
od	od	k7c2	od
planety	planeta	k1gFnSc2	planeta
než	než	k8xS	než
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
klasické	klasický	k2eAgInPc1d1	klasický
prstence	prstenec	k1gInPc1	prstenec
<g/>
,	,	kIx,	,
existuje	existovat	k5eAaImIp3nS	existovat
ještě	ještě	k9	ještě
extrémně	extrémně	k6eAd1	extrémně
tenký	tenký	k2eAgInSc4d1	tenký
a	a	k8xC	a
řídký	řídký	k2eAgInSc4d1	řídký
prstenec	prstenec	k1gInSc4	prstenec
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
tvořený	tvořený	k2eAgInSc1d1	tvořený
materiálem	materiál	k1gInSc7	materiál
vyvrženým	vyvržený	k2eAgInSc7d1	vyvržený
z	z	k7c2	z
měsíce	měsíc	k1gInSc2	měsíc
Phoebe	Phoeb	k1gMnSc5	Phoeb
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
ale	ale	k9	ale
není	být	k5eNaImIp3nS	být
pozorovatelný	pozorovatelný	k2eAgInSc1d1	pozorovatelný
klasickými	klasický	k2eAgInPc7d1	klasický
dalekohledy	dalekohled	k1gInPc7	dalekohled
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prstenec	prstenec	k1gInSc1	prstenec
E	E	kA	E
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
mikroskopickými	mikroskopický	k2eAgFnPc7d1	mikroskopická
částicemi	částice	k1gFnPc7	částice
ledu	led	k1gInSc2	led
a	a	k8xC	a
prachu	prach	k1gInSc2	prach
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
jsou	být	k5eAaImIp3nP	být
rozprostřeny	rozprostřít	k5eAaPmNgInP	rozprostřít
mezi	mezi	k7c4	mezi
oběžné	oběžný	k2eAgFnPc4d1	oběžná
dráhy	dráha	k1gFnPc4	dráha
měsíců	měsíc	k1gInPc2	měsíc
Mimas	Mimasa	k1gFnPc2	Mimasa
a	a	k8xC	a
Titanu	titan	k1gInSc2	titan
<g/>
.	.	kIx.	.
</s>
<s>
Matematické	matematický	k2eAgInPc1d1	matematický
modely	model	k1gInPc1	model
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
prstenec	prstenec	k1gInSc1	prstenec
E	E	kA	E
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
čase	čas	k1gInSc6	čas
nestálý	stálý	k2eNgInSc4d1	nestálý
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
by	by	kYmCp3nS	by
zaniknout	zaniknout	k5eAaPmF	zaniknout
mezi	mezi	k7c4	mezi
10	[number]	k4	10
000	[number]	k4	000
až	až	k9	až
1	[number]	k4	1
000	[number]	k4	000
000	[number]	k4	000
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Částice	částice	k1gFnPc1	částice
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
prstenec	prstenec	k1gInSc1	prstenec
tvoří	tvořit	k5eAaImIp3nS	tvořit
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
neustále	neustále	k6eAd1	neustále
doplňovány	doplňován	k2eAgFnPc4d1	doplňována
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
Enceladus	Enceladus	k1gMnSc1	Enceladus
leží	ležet	k5eAaImIp3nS	ležet
uvnitř	uvnitř	k7c2	uvnitř
tohoto	tento	k3xDgInSc2	tento
prstence	prstenec	k1gInSc2	prstenec
(	(	kIx(	(
<g/>
v	v	k7c6	v
jeho	jeho	k3xOp3gInPc6	jeho
nejužší	úzký	k2eAgInPc1d3	nejužší
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
částice	částice	k1gFnPc4	částice
nejbohatší	bohatý	k2eAgFnSc2d3	nejbohatší
části	část	k1gFnSc2	část
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
objevovaly	objevovat	k5eAaImAgFnP	objevovat
se	se	k3xPyFc4	se
již	již	k6eAd1	již
od	od	k7c2	od
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
úvahy	úvaha	k1gFnSc2	úvaha
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
samotný	samotný	k2eAgInSc1d1	samotný
Enceladus	Enceladus	k1gInSc1	Enceladus
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
zdrojem	zdroj	k1gInSc7	zdroj
těchto	tento	k3xDgFnPc2	tento
částic	částice	k1gFnPc2	částice
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
hypotéza	hypotéza	k1gFnSc1	hypotéza
byla	být	k5eAaImAgFnS	být
potvrzena	potvrdit	k5eAaPmNgFnS	potvrdit
během	během	k7c2	během
prvních	první	k4xOgInPc2	první
dvou	dva	k4xCgInPc2	dva
průletů	průlet	k1gInPc2	průlet
americké	americký	k2eAgFnSc2d1	americká
planetární	planetární	k2eAgFnSc2d1	planetární
sondy	sonda	k1gFnSc2	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
uskutečněných	uskutečněný	k2eAgInPc2d1	uskutečněný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
sondou	sonda	k1gFnSc7	sonda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
spatřila	spatřit	k5eAaPmAgFnS	spatřit
povrch	povrch	k1gInSc4	povrch
Enceladu	Encelad	k1gInSc2	Encelad
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1981	[number]	k4	1981
americká	americký	k2eAgFnSc1d1	americká
planetární	planetární	k2eAgFnSc1d1	planetární
sonda	sonda	k1gFnSc1	sonda
Voyager	Voyager	k1gInSc1	Voyager
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
snímcích	snímek	k1gInPc6	snímek
ve	v	k7c6	v
vysokém	vysoký	k2eAgNnSc6d1	vysoké
rozlišení	rozlišení	k1gNnSc6	rozlišení
bylo	být	k5eAaImAgNnS	být
rozpoznatelné	rozpoznatelný	k2eAgNnSc1d1	rozpoznatelné
<g/>
,	,	kIx,	,
že	že	k8xS	že
povrch	povrch	k1gInSc1	povrch
měsíce	měsíc	k1gInSc2	měsíc
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
nejméně	málo	k6eAd3	málo
pěti	pět	k4xCc7	pět
různými	různý	k2eAgInPc7d1	různý
typy	typ	k1gInPc7	typ
terénů	terén	k1gInPc2	terén
<g/>
;	;	kIx,	;
jak	jak	k8xS	jak
oblastmi	oblast	k1gFnPc7	oblast
výrazně	výrazně	k6eAd1	výrazně
posetými	posetý	k2eAgInPc7d1	posetý
impaktními	impaktní	k2eAgInPc7d1	impaktní
krátery	kráter	k1gInPc7	kráter
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
i	i	k8xC	i
oblastmi	oblast	k1gFnPc7	oblast
hladkými	hladký	k2eAgFnPc7d1	hladká
a	a	k8xC	a
tedy	tedy	k9	tedy
relativně	relativně	k6eAd1	relativně
mladými	mladá	k1gFnPc7	mladá
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nacházela	nacházet	k5eAaImAgNnP	nacházet
místa	místo	k1gNnPc1	místo
protkaná	protkaný	k2eAgNnPc1d1	protkané
sítí	sítí	k1gNnSc4	sítí
trhlin	trhlina	k1gFnPc2	trhlina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
často	často	k6eAd1	často
obklopena	obklopit	k5eAaPmNgFnS	obklopit
hladkými	hladký	k2eAgFnPc7d1	hladká
oblastmi	oblast	k1gFnPc7	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
byly	být	k5eAaImAgFnP	být
pozorovány	pozorován	k2eAgFnPc1d1	pozorována
lineární	lineární	k2eAgFnPc1d1	lineární
extenzivní	extenzivní	k2eAgFnPc1d1	extenzivní
praskliny	prasklina	k1gFnPc1	prasklina
a	a	k8xC	a
srázy	sráz	k1gInPc1	sráz
<g/>
.	.	kIx.	.
</s>
<s>
Absence	absence	k1gFnSc1	absence
významného	významný	k2eAgInSc2d1	významný
počtu	počet	k1gInSc2	počet
impaktních	impaktní	k2eAgInPc2d1	impaktní
kráterů	kráter	k1gInPc2	kráter
na	na	k7c6	na
hladkých	hladký	k2eAgFnPc6d1	hladká
pláních	pláň	k1gFnPc6	pláň
nacházejících	nacházející	k2eAgFnPc2d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
těchto	tento	k3xDgFnPc2	tento
trhlin	trhlina	k1gFnPc2	trhlina
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgFnPc1	tento
oblasti	oblast	k1gFnPc1	oblast
jsou	být	k5eAaImIp3nP	být
méně	málo	k6eAd2	málo
než	než	k8xS	než
několik	několik	k4yIc1	několik
stovek	stovka	k1gFnPc2	stovka
miliónů	milión	k4xCgInPc2	milión
let	léto	k1gNnPc2	léto
staré	starý	k2eAgFnPc1d1	stará
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Enceladus	Enceladus	k1gMnSc1	Enceladus
musel	muset	k5eAaImAgMnS	muset
být	být	k5eAaImF	být
relativně	relativně	k6eAd1	relativně
nedávno	nedávno	k6eAd1	nedávno
geologicky	geologicky	k6eAd1	geologicky
aktivním	aktivní	k2eAgInSc7d1	aktivní
světem	svět	k1gInSc7	svět
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
například	například	k6eAd1	například
kryovulkanismus	kryovulkanismus	k1gInSc1	kryovulkanismus
či	či	k8xC	či
jiné	jiný	k2eAgInPc1d1	jiný
procesy	proces	k1gInPc1	proces
byly	být	k5eAaImAgInP	být
schopny	schopen	k2eAgInPc1d1	schopen
významně	významně	k6eAd1	významně
přetvořit	přetvořit	k5eAaPmF	přetvořit
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Obnovení	obnovení	k1gNnSc1	obnovení
povrchu	povrch	k1gInSc2	povrch
mělo	mít	k5eAaImAgNnS	mít
také	také	k9	také
za	za	k7c4	za
následek	následek	k1gInSc4	následek
<g/>
,	,	kIx,	,
že	že	k8xS	že
povrch	povrch	k1gInSc1	povrch
utváří	utvářet	k5eAaImIp3nS	utvářet
čerstvý	čerstvý	k2eAgInSc1d1	čerstvý
<g/>
,	,	kIx,	,
čistý	čistý	k2eAgInSc1d1	čistý
led	led	k1gInSc1	led
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgInSc1d1	schopen
odrážet	odrážet	k5eAaImF	odrážet
většinu	většina	k1gFnSc4	většina
dopadajícího	dopadající	k2eAgNnSc2d1	dopadající
slunečního	sluneční	k2eAgNnSc2d1	sluneční
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
z	z	k7c2	z
Enceladu	Encelad	k1gInSc2	Encelad
činní	činný	k2eAgMnPc1d1	činný
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nejvíce	nejvíce	k6eAd1	nejvíce
odrazivé	odrazivý	k2eAgNnSc4d1	odrazivé
těleso	těleso	k1gNnSc4	těleso
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
s	s	k7c7	s
vizuálním	vizuální	k2eAgInSc7d1	vizuální
geometrickým	geometrický	k2eAgInSc7d1	geometrický
albedem	albed	k1gInSc7	albed
1,38	[number]	k4	1,38
<g/>
.	.	kIx.	.
<g/>
>	>	kIx)	>
Kvůli	kvůli	k7c3	kvůli
vysoké	vysoký	k2eAgFnSc3d1	vysoká
odrazivosti	odrazivost	k1gFnSc3	odrazivost
pak	pak	k6eAd1	pak
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
měsíce	měsíc	k1gInPc4	měsíc
panují	panovat	k5eAaImIp3nP	panovat
extrémně	extrémně	k6eAd1	extrémně
nízké	nízký	k2eAgFnPc1d1	nízká
teploty	teplota	k1gFnPc1	teplota
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
mohou	moct	k5eAaImIp3nP	moct
pohybovat	pohybovat	k5eAaImF	pohybovat
až	až	k9	až
okolo	okolo	k7c2	okolo
-198	-198	k4	-198
°	°	k?	°
<g/>
C.	C.	kA	C.
Průměrné	průměrný	k2eAgFnPc4d1	průměrná
teploty	teplota	k1gFnPc4	teplota
na	na	k7c6	na
noční	noční	k2eAgFnSc6d1	noční
straně	strana	k1gFnSc6	strana
Enceladu	Encelad	k1gInSc2	Encelad
jsou	být	k5eAaImIp3nP	být
tak	tak	k6eAd1	tak
nižší	nízký	k2eAgFnPc1d2	nižší
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
u	u	k7c2	u
ostatních	ostatní	k2eAgInPc2d1	ostatní
měsíců	měsíc	k1gInPc2	měsíc
Saturnu	Saturn	k1gInSc2	Saturn
běžné	běžný	k2eAgFnPc1d1	běžná
<g/>
.	.	kIx.	.
</s>
<s>
Pozorování	pozorování	k1gNnSc1	pozorování
pořízená	pořízený	k2eAgFnSc1d1	pořízená
během	během	k7c2	během
průletů	průlet	k1gInPc2	průlet
sondy	sonda	k1gFnSc2	sonda
Cassiny	Cassina	k1gFnSc2	Cassina
ze	z	k7c2	z
17	[number]	k4	17
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
<g/>
,	,	kIx,	,
9	[number]	k4	9
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
a	a	k8xC	a
14	[number]	k4	14
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
umožnila	umožnit	k5eAaPmAgFnS	umožnit
spatřit	spatřit	k5eAaPmF	spatřit
povrch	povrch	k1gInSc4	povrch
Enceladu	Encelad	k1gInSc2	Encelad
v	v	k7c6	v
mnohem	mnohem	k6eAd1	mnohem
větším	veliký	k2eAgInSc6d2	veliký
detailu	detail	k1gInSc6	detail
<g/>
,	,	kIx,	,
než	než	k8xS	než
umožnily	umožnit	k5eAaPmAgFnP	umožnit
sondy	sonda	k1gFnPc1	sonda
Voyager	Voyagero	k1gNnPc2	Voyagero
1	[number]	k4	1
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Hladké	Hladké	k2eAgFnPc4d1	Hladké
pláně	pláň	k1gFnPc4	pláň
pozorované	pozorovaný	k2eAgFnPc4d1	pozorovaná
již	již	k6eAd1	již
na	na	k7c6	na
fotografiích	fotografia	k1gFnPc6	fotografia
od	od	k7c2	od
sonddy	sondda	k1gFnSc2	sondda
Voyager	Voyager	k1gInSc1	Voyager
2	[number]	k4	2
se	se	k3xPyFc4	se
na	na	k7c6	na
detailnějších	detailní	k2eAgInPc6d2	detailnější
snímcích	snímek	k1gInPc6	snímek
ukázaly	ukázat	k5eAaPmAgFnP	ukázat
jako	jako	k9	jako
oblasti	oblast	k1gFnPc1	oblast
bez	bez	k7c2	bez
významnějšího	významný	k2eAgNnSc2d2	významnější
množství	množství	k1gNnSc2	množství
impaktních	impaktní	k2eAgInPc2d1	impaktní
kráterů	kráter	k1gInPc2	kráter
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
množstvím	množství	k1gNnSc7	množství
hřebenů	hřeben	k1gInPc2	hřeben
a	a	k8xC	a
srázů	sráz	k1gInPc2	sráz
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
starších	starý	k2eAgFnPc6d2	starší
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
výrazněji	výrazně	k6eAd2	výrazně
poseté	posetý	k2eAgInPc4d1	posetý
impaktními	impaktní	k2eAgInPc7d1	impaktní
krátery	kráter	k1gInPc7	kráter
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
rozpoznáno	rozpoznán	k2eAgNnSc1d1	rozpoznáno
množství	množství	k1gNnSc1	množství
trhlin	trhlina	k1gFnPc2	trhlina
naznačující	naznačující	k2eAgNnSc1d1	naznačující
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgFnPc1	tento
oblasti	oblast	k1gFnPc1	oblast
musely	muset	k5eAaImAgFnP	muset
projít	projít	k5eAaPmF	projít
výraznou	výrazný	k2eAgFnSc7d1	výrazná
extenzivní	extenzivní	k2eAgFnSc7d1	extenzivní
deformací	deformace	k1gFnSc7	deformace
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
krátery	kráter	k1gInPc1	kráter
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
oblastech	oblast	k1gFnPc6	oblast
se	se	k3xPyFc4	se
nepodařilo	podařit	k5eNaPmAgNnS	podařit
objevit	objevit	k5eAaPmF	objevit
žádné	žádný	k3yNgInPc4	žádný
impaktní	impaktní	k2eAgInPc4d1	impaktní
krátery	kráter	k1gInPc4	kráter
<g/>
,	,	kIx,	,
tyto	tento	k3xDgFnPc1	tento
části	část	k1gFnPc1	část
povrchu	povrch	k1gInSc2	povrch
tak	tak	k6eAd1	tak
musely	muset	k5eAaImAgFnP	muset
projít	projít	k5eAaPmF	projít
kompletním	kompletní	k2eAgNnSc7d1	kompletní
přetvořením	přetvoření	k1gNnSc7	přetvoření
v	v	k7c6	v
geologicky	geologicky	k6eAd1	geologicky
nedávné	dávný	k2eNgFnSc6d1	nedávná
době	doba	k1gFnSc6	doba
<g/>
.	.	kIx.	.
</s>
<s>
Sonda	sonda	k1gFnSc1	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
také	také	k9	také
umožnila	umožnit	k5eAaPmAgFnS	umožnit
spatřit	spatřit	k5eAaPmF	spatřit
části	část	k1gFnPc4	část
povrchu	povrch	k1gInSc2	povrch
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
se	se	k3xPyFc4	se
nepodařilo	podařit	k5eNaPmAgNnS	podařit
sondám	sonda	k1gFnPc3	sonda
Voyager	Voyager	k1gMnSc1	Voyager
vyfotografovat	vyfotografovat	k5eAaPmF	vyfotografovat
v	v	k7c6	v
rozlišení	rozlišení	k1gNnSc6	rozlišení
umožňující	umožňující	k2eAgNnSc1d1	umožňující
detekovat	detekovat	k5eAaImF	detekovat
větší	veliký	k2eAgInPc4d2	veliký
povrchové	povrchový	k2eAgInPc4d1	povrchový
útvary	útvar	k1gInPc4	útvar
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
těchto	tento	k3xDgNnPc2	tento
míst	místo	k1gNnPc2	místo
byla	být	k5eAaImAgFnS	být
i	i	k9	i
zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
oblast	oblast	k1gFnSc1	oblast
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
jižního	jižní	k2eAgInSc2d1	jižní
pólu	pól	k1gInSc2	pól
<g/>
.	.	kIx.	.
</s>
<s>
Všechna	všechen	k3xTgNnPc1	všechen
tato	tento	k3xDgNnPc1	tento
pozorování	pozorování	k1gNnPc1	pozorování
naznačovala	naznačovat	k5eAaImAgNnP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vnitřek	vnitřek	k1gInSc1	vnitřek
Enceladu	Encelad	k1gInSc2	Encelad
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
do	do	k7c2	do
dnešních	dnešní	k2eAgInPc2d1	dnešní
dní	den	k1gInPc2	den
tekutý	tekutý	k2eAgMnSc1d1	tekutý
i	i	k8xC	i
přes	přes	k7c4	přes
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
takto	takto	k6eAd1	takto
malé	malý	k2eAgNnSc1d1	malé
těleso	těleso	k1gNnSc1	těleso
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
zcela	zcela	k6eAd1	zcela
zmrznout	zmrznout	k5eAaPmF	zmrznout
již	již	k6eAd1	již
před	před	k7c7	před
dlouhou	dlouhý	k2eAgFnSc7d1	dlouhá
dobou	doba	k1gFnSc7	doba
<g/>
.	.	kIx.	.
</s>
<s>
Srážky	srážka	k1gFnPc4	srážka
různě	různě	k6eAd1	různě
velkých	velký	k2eAgNnPc2d1	velké
těles	těleso	k1gNnPc2	těleso
a	a	k8xC	a
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
spojený	spojený	k2eAgInSc1d1	spojený
vznik	vznik	k1gInSc4	vznik
impaktních	impaktní	k2eAgInPc2d1	impaktní
kráterů	kráter	k1gInPc2	kráter
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
objektech	objekt	k1gInPc6	objekt
Sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
běžným	běžný	k2eAgInSc7d1	běžný
jevem	jev	k1gInSc7	jev
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
povrchu	povrch	k1gInSc2	povrch
Enceladu	Encelad	k1gInSc2	Encelad
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
poseta	posít	k5eAaPmNgFnS	posít
impaktními	impaktní	k2eAgInPc7d1	impaktní
krátery	kráter	k1gInPc7	kráter
o	o	k7c6	o
různé	různý	k2eAgFnSc6d1	různá
velikosti	velikost	k1gFnSc6	velikost
a	a	k8xC	a
o	o	k7c6	o
různém	různý	k2eAgInSc6d1	různý
stupni	stupeň	k1gInSc6	stupeň
degradace	degradace	k1gFnSc2	degradace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
o	o	k7c4	o
různé	různý	k2eAgFnPc4d1	různá
četnosti	četnost	k1gFnPc4	četnost
výskytu	výskyt	k1gInSc2	výskyt
<g/>
.	.	kIx.	.
</s>
<s>
Rozdílná	rozdílný	k2eAgFnSc1d1	rozdílná
četnost	četnost	k1gFnSc1	četnost
výskytu	výskyt	k1gInSc2	výskyt
impaktních	impaktní	k2eAgFnPc2d1	impaktní
kráteru	kráter	k1gInSc2	kráter
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
(	(	kIx(	(
<g/>
a	a	k8xC	a
tedy	tedy	k9	tedy
rozdílného	rozdílný	k2eAgNnSc2d1	rozdílné
stáří	stáří	k1gNnSc2	stáří
jeho	jeho	k3xOp3gFnPc2	jeho
částí	část	k1gFnPc2	část
<g/>
)	)	kIx)	)
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Enceladus	Enceladus	k1gMnSc1	Enceladus
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
přetvořen	přetvořen	k2eAgMnSc1d1	přetvořen
několikrát	několikrát	k6eAd1	několikrát
<g/>
.	.	kIx.	.
</s>
<s>
Sonda	sonda	k1gFnSc1	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
umožnila	umožnit	k5eAaPmAgFnS	umožnit
detailnější	detailní	k2eAgInSc4d2	detailnější
pohled	pohled	k1gInSc4	pohled
na	na	k7c4	na
rozložení	rozložení	k1gNnSc4	rozložení
četnosti	četnost	k1gFnSc2	četnost
impaktních	impaktní	k2eAgInPc2d1	impaktní
kráterů	kráter	k1gInPc2	kráter
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
velikost	velikost	k1gFnSc1	velikost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
odhalila	odhalit	k5eAaPmAgFnS	odhalit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mnoho	mnoho	k4c1	mnoho
impaktních	impaktní	k2eAgInPc2d1	impaktní
kráterů	kráter	k1gInPc2	kráter
je	být	k5eAaImIp3nS	být
výrazně	výrazně	k6eAd1	výrazně
degradováno	degradován	k2eAgNnSc1d1	degradováno
vlivem	vlivem	k7c2	vlivem
viskózní	viskózní	k2eAgFnSc2d1	viskózní
relaxace	relaxace	k1gFnSc2	relaxace
povrchu	povrch	k1gInSc2	povrch
a	a	k8xC	a
pozdějším	pozdní	k2eAgInSc7d2	pozdější
vznikem	vznik	k1gInSc7	vznik
povrchových	povrchový	k2eAgFnPc2d1	povrchová
trhlin	trhlina	k1gFnPc2	trhlina
<g/>
.	.	kIx.	.
</s>
<s>
Viskózní	viskózní	k2eAgFnSc1d1	viskózní
relaxace	relaxace	k1gFnSc1	relaxace
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
v	v	k7c6	v
měřítkách	měřítko	k1gNnPc6	měřítko
geologického	geologický	k2eAgInSc2d1	geologický
času	čas	k1gInSc2	čas
gravitaci	gravitace	k1gFnSc4	gravitace
deformovat	deformovat	k5eAaImF	deformovat
krátery	kráter	k1gInPc4	kráter
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
povrchové	povrchový	k2eAgInPc4d1	povrchový
útvary	útvar	k1gInPc4	útvar
vzniklé	vzniklý	k2eAgInPc4d1	vzniklý
na	na	k7c6	na
povrchu	povrch	k1gInSc3	povrch
tvořeným	tvořený	k2eAgInSc7d1	tvořený
vodním	vodní	k2eAgInSc7d1	vodní
ledem	led	k1gInSc7	led
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
jejich	jejich	k3xOp3gInSc2	jejich
tvaru	tvar	k1gInSc2	tvar
a	a	k8xC	a
topografie	topografie	k1gFnSc2	topografie
v	v	k7c6	v
čase	čas	k1gInSc6	čas
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
<g/>
,	,	kIx,	,
jakým	jaký	k3yIgInSc7	jaký
tento	tento	k3xDgInSc4	tento
proces	proces	k1gInSc1	proces
probíhá	probíhat	k5eAaImIp3nS	probíhat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
teplotě	teplota	k1gFnSc6	teplota
ledu	led	k1gInSc2	led
<g/>
.	.	kIx.	.
</s>
<s>
Teplejší	teplý	k2eAgInSc1d2	teplejší
a	a	k8xC	a
tedy	tedy	k9	tedy
měkčí	měkký	k2eAgInSc1d2	měkčí
led	led	k1gInSc1	led
je	být	k5eAaImIp3nS	být
totiž	totiž	k9	totiž
snazší	snadný	k2eAgNnSc1d2	snazší
deformovat	deformovat	k5eAaImF	deformovat
než	než	k8xS	než
tužší	tuhý	k2eAgInSc4d2	tužší
studený	studený	k2eAgInSc4d1	studený
led	led	k1gInSc4	led
<g/>
.	.	kIx.	.
</s>
<s>
Impaktní	Impaktní	k2eAgInPc1d1	Impaktní
krátery	kráter	k1gInPc1	kráter
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
prošly	projít	k5eAaPmAgFnP	projít
viskózní	viskózní	k2eAgFnSc7d1	viskózní
relaxací	relaxace	k1gFnSc7	relaxace
mají	mít	k5eAaImIp3nP	mít
tendenci	tendence	k1gFnSc4	tendence
mít	mít	k5eAaImF	mít
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
vyzvednutá	vyzvednutý	k2eAgNnPc1d1	vyzvednuté
dna	dno	k1gNnPc1	dno
<g/>
,	,	kIx,	,
či	či	k8xC	či
mít	mít	k5eAaImF	mít
vnitřek	vnitřek	k1gInSc4	vnitřek
kráteru	kráter	k1gInSc2	kráter
natolik	natolik	k6eAd1	natolik
zahlazený	zahlazený	k2eAgInSc1d1	zahlazený
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
existence	existence	k1gFnSc1	existence
kráteru	kráter	k1gInSc2	kráter
dá	dát	k5eAaPmIp3nS	dát
rozpoznat	rozpoznat	k5eAaPmF	rozpoznat
jen	jen	k9	jen
díky	díky	k7c3	díky
zachovalému	zachovalý	k2eAgInSc3d1	zachovalý
okraji	okraj	k1gInSc3	okraj
impaktního	impaktní	k2eAgInSc2d1	impaktní
kráteru	kráter	k1gInSc2	kráter
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
přítomností	přítomnost	k1gFnSc7	přítomnost
centrální	centrální	k2eAgFnSc2d1	centrální
deprese	deprese	k1gFnSc2	deprese
<g/>
.	.	kIx.	.
</s>
<s>
Ukázkovým	ukázkový	k2eAgInSc7d1	ukázkový
příkladem	příklad	k1gInSc7	příklad
takovéto	takovýto	k3xDgFnSc2	takovýto
změny	změna	k1gFnSc2	změna
na	na	k7c6	na
Enceladu	Encelad	k1gInSc6	Encelad
je	být	k5eAaImIp3nS	být
kráter	kráter	k1gInSc1	kráter
Dunyazad	Dunyazad	k1gInSc1	Dunyazad
<g/>
.	.	kIx.	.
</s>
<s>
Sonda	sonda	k1gFnSc1	sonda
Voyager	Voyager	k1gInSc1	Voyager
2	[number]	k4	2
objevila	objevit	k5eAaPmAgFnS	objevit
řadu	řada	k1gFnSc4	řada
útvarů	útvar	k1gInPc2	útvar
na	na	k7c6	na
Enceladu	Encelad	k1gInSc6	Encelad
vzniklých	vzniklý	k2eAgFnPc2d1	vzniklá
tektonickými	tektonický	k2eAgInPc7d1	tektonický
procesy	proces	k1gInPc7	proces
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
například	například	k6eAd1	například
o	o	k7c4	o
smykové	smykový	k2eAgFnPc4d1	smyková
plochy	plocha	k1gFnPc4	plocha
a	a	k8xC	a
pásy	pás	k1gInPc4	pás
rýh	rýha	k1gFnPc2	rýha
či	či	k8xC	či
hřebenů	hřeben	k1gInPc2	hřeben
<g/>
.	.	kIx.	.
</s>
<s>
Údaje	údaj	k1gInPc1	údaj
pořízené	pořízený	k2eAgInPc1d1	pořízený
sondou	sonda	k1gFnSc7	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
následně	následně	k6eAd1	následně
naznačily	naznačit	k5eAaPmAgInP	naznačit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tektonismus	tektonismus	k1gInSc1	tektonismus
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
důvodem	důvod	k1gInSc7	důvod
deformace	deformace	k1gFnSc2	deformace
povrchu	povrch	k1gInSc2	povrch
Enceladu	Encelad	k1gInSc2	Encelad
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
byly	být	k5eAaImAgInP	být
objeveny	objeven	k2eAgInPc1d1	objeven
i	i	k8xC	i
zvláštní	zvláštní	k2eAgInPc1d1	zvláštní
kaňony	kaňon	k1gInPc1	kaňon
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
rifty	rifta	k1gFnPc1	rifta
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
až	až	k9	až
200	[number]	k4	200
kilometrů	kilometr	k1gInPc2	kilometr
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
<g/>
,	,	kIx,	,
5	[number]	k4	5
až	až	k9	až
10	[number]	k4	10
kilometrů	kilometr	k1gInPc2	kilometr
široké	široký	k2eAgNnSc1d1	široké
a	a	k8xC	a
okolo	okolo	k7c2	okolo
1	[number]	k4	1
kilometru	kilometr	k1gInSc2	kilometr
hluboké	hluboký	k2eAgFnPc1d1	hluboká
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
útvary	útvar	k1gInPc1	útvar
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
geologicky	geologicky	k6eAd1	geologicky
velmi	velmi	k6eAd1	velmi
mladé	mladý	k2eAgNnSc1d1	mladé
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
protínají	protínat	k5eAaImIp3nP	protínat
jiné	jiný	k2eAgInPc1d1	jiný
povrchové	povrchový	k2eAgInPc1d1	povrchový
tektonické	tektonický	k2eAgInPc1d1	tektonický
útvary	útvar	k1gInPc1	útvar
a	a	k8xC	a
protože	protože	k8xS	protože
mají	mít	k5eAaImIp3nP	mít
ostrý	ostrý	k2eAgInSc4d1	ostrý
topografický	topografický	k2eAgInSc4d1	topografický
profil	profil	k1gInSc4	profil
s	s	k7c7	s
výraznými	výrazný	k2eAgInPc7d1	výrazný
výchozy	výchoz	k1gInPc7	výchoz
podél	podél	k7c2	podél
stěny	stěna	k1gFnSc2	stěna
útesů	útes	k1gInPc2	útes
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ještě	ještě	k6eAd1	ještě
nebyly	být	k5eNaImAgFnP	být
výrazněji	výrazně	k6eAd2	výrazně
erodovány	erodován	k2eAgFnPc1d1	erodována
<g/>
.	.	kIx.	.
</s>
<s>
Důkazy	důkaz	k1gInPc1	důkaz
o	o	k7c6	o
tektonické	tektonický	k2eAgFnSc6d1	tektonická
aktivitě	aktivita	k1gFnSc6	aktivita
na	na	k7c6	na
Enceladu	Encelad	k1gInSc6	Encelad
nabízejí	nabízet	k5eAaImIp3nP	nabízet
i	i	k9	i
oblasti	oblast	k1gFnPc4	oblast
tvořené	tvořený	k2eAgFnPc4d1	tvořená
zakřivenými	zakřivený	k2eAgInPc7d1	zakřivený
pásy	pás	k1gInPc7	pás
rýh	rýha	k1gFnPc2	rýha
a	a	k8xC	a
hřebenů	hřeben	k1gInPc2	hřeben
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
pásy	pás	k1gInPc1	pás
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
byly	být	k5eAaImAgInP	být
prvně	prvně	k?	prvně
pozorovány	pozorován	k2eAgFnPc1d1	pozorována
sondou	sonda	k1gFnSc7	sonda
Voyager	Voyagra	k1gFnPc2	Voyagra
2	[number]	k4	2
<g/>
,	,	kIx,	,
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
často	často	k6eAd1	často
oddělují	oddělovat	k5eAaImIp3nP	oddělovat
hladké	hladký	k2eAgFnPc4d1	hladká
pláně	pláň	k1gFnPc4	pláň
od	od	k7c2	od
oblastí	oblast	k1gFnPc2	oblast
výrazně	výrazně	k6eAd1	výrazně
poznamenané	poznamenaný	k2eAgInPc4d1	poznamenaný
impaktními	impaktní	k2eAgInPc7d1	impaktní
krátery	kráter	k1gInPc7	kráter
<g/>
.	.	kIx.	.
</s>
<s>
Rýhované	rýhovaný	k2eAgFnPc1d1	rýhovaná
oblasti	oblast	k1gFnPc1	oblast
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
Samarkand	Samarkand	k1gInSc1	Samarkand
Sulci	Sulce	k1gFnSc4	Sulce
<g/>
,	,	kIx,	,
připomínají	připomínat	k5eAaImIp3nP	připomínat
svým	svůj	k3xOyFgInSc7	svůj
vzhledem	vzhled	k1gInSc7	vzhled
zdánlivě	zdánlivě	k6eAd1	zdánlivě
některé	některý	k3yIgFnPc1	některý
místa	místo	k1gNnPc1	místo
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
měsíce	měsíc	k1gInSc2	měsíc
Ganymedu	Ganymed	k1gMnSc3	Ganymed
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
oproti	oproti	k7c3	oproti
nim	on	k3xPp3gFnPc3	on
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gNnSc4	jejich
rozložení	rozložení	k1gNnSc4	rozložení
složitější	složitý	k2eAgNnSc4d2	složitější
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
tak	tak	k6eAd1	tak
nevytváří	vytvářet	k5eNaImIp3nS	vytvářet
sady	sada	k1gFnPc4	sada
rovnoběžných	rovnoběžný	k2eAgInPc2d1	rovnoběžný
pásů	pás	k1gInPc2	pás
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spíše	spíše	k9	spíše
sady	sada	k1gFnPc1	sada
nahodile	nahodile	k6eAd1	nahodile
prohnutých	prohnutý	k2eAgInPc2d1	prohnutý
útvarů	útvar	k1gInPc2	útvar
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
sondě	sonda	k1gFnSc3	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
povedlo	povést	k5eAaPmAgNnS	povést
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Samarkand	Samarkanda	k1gFnPc2	Samarkanda
Sulci	Sulce	k1gFnSc4	Sulce
pozorovat	pozorovat	k5eAaImF	pozorovat
i	i	k9	i
tmavé	tmavý	k2eAgFnPc1d1	tmavá
oblasti	oblast	k1gFnPc1	oblast
(	(	kIx(	(
<g/>
125	[number]	k4	125
a	a	k8xC	a
750	[number]	k4	750
metrů	metr	k1gInPc2	metr
široké	široký	k2eAgFnPc1d1	široká
<g/>
)	)	kIx)	)
ležící	ležící	k2eAgFnPc1d1	ležící
rovnoběžně	rovnoběžně	k6eAd1	rovnoběžně
od	od	k7c2	od
rovných	rovný	k2eAgInPc2d1	rovný
zlomů	zlom	k1gInPc2	zlom
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
byly	být	k5eAaImAgFnP	být
později	pozdě	k6eAd2	pozdě
interpretovány	interpretován	k2eAgFnPc4d1	interpretována
jako	jako	k8xS	jako
malé	malý	k2eAgFnPc4d1	malá
deprese	deprese	k1gFnPc4	deprese
vzniklé	vzniklý	k2eAgFnPc4d1	vzniklá
kolapsem	kolaps	k1gInSc7	kolaps
ledového	ledový	k2eAgInSc2d1	ledový
povrchu	povrch	k1gInSc2	povrch
vystaveného	vystavený	k2eAgInSc2d1	vystavený
změně	změna	k1gFnSc3	změna
napětí	napětí	k1gNnSc2	napětí
kůry	kůra	k1gFnSc2	kůra
<g/>
.	.	kIx.	.
</s>
<s>
Vyjma	vyjma	k7c2	vyjma
těchto	tento	k3xDgInPc2	tento
útvarů	útvar	k1gInPc2	útvar
se	se	k3xPyFc4	se
na	na	k7c6	na
Enceladu	Encelad	k1gInSc6	Encelad
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
i	i	k9	i
další	další	k2eAgInPc1d1	další
typy	typ	k1gInPc1	typ
tektonických	tektonický	k2eAgFnPc2d1	tektonická
poruch	porucha	k1gFnPc2	porucha
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
těchto	tento	k3xDgFnPc2	tento
prasklin	prasklina	k1gFnPc2	prasklina
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
s	s	k7c7	s
množstvím	množství	k1gNnSc7	množství
impaktních	impaktní	k2eAgInPc2d1	impaktní
kráterů	kráter	k1gInPc2	kráter
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
praskliny	prasklina	k1gFnPc1	prasklina
sahají	sahat	k5eAaImIp3nP	sahat
do	do	k7c2	do
hloubky	hloubka	k1gFnSc2	hloubka
pouze	pouze	k6eAd1	pouze
několika	několik	k4yIc2	několik
stovek	stovka	k1gFnPc2	stovka
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
příkladem	příklad	k1gInSc7	příklad
jsou	být	k5eAaImIp3nP	být
lineární	lineární	k2eAgFnPc1d1	lineární
rýhy	rýha	k1gFnPc1	rýha
poprvé	poprvé	k6eAd1	poprvé
pozorované	pozorovaný	k2eAgFnPc1d1	pozorovaná
sondou	sonda	k1gFnSc7	sonda
Voyager	Voyager	k1gMnSc1	Voyager
2	[number]	k4	2
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
mnohem	mnohem	k6eAd1	mnohem
lepším	dobrý	k2eAgNnSc6d2	lepší
rozlišení	rozlišení	k1gNnSc6	rozlišení
sondou	sonda	k1gFnSc7	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
lineární	lineární	k2eAgFnPc1d1	lineární
rýhy	rýha	k1gFnPc1	rýha
představují	představovat	k5eAaImIp3nP	představovat
jedny	jeden	k4xCgInPc1	jeden
z	z	k7c2	z
nejmladších	mladý	k2eAgInPc2d3	nejmladší
útvarů	útvar	k1gInPc2	útvar
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Enceladu	Encelad	k1gInSc2	Encelad
nachází	nacházet	k5eAaImIp3nS	nacházet
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
tvrzení	tvrzení	k1gNnSc1	tvrzení
ale	ale	k9	ale
neplatí	platit	k5eNaImIp3nS	platit
beze	beze	k7c2	beze
zbytku	zbytek	k1gInSc2	zbytek
pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
<g/>
.	.	kIx.	.
</s>
<s>
Stratigrafická	Stratigrafický	k2eAgFnSc1d1	Stratigrafická
pozice	pozice	k1gFnSc1	pozice
některých	některý	k3yIgMnPc2	některý
totiž	totiž	k9	totiž
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
i	i	k9	i
velice	velice	k6eAd1	velice
staré	starý	k2eAgFnPc1d1	stará
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
pozorovanými	pozorovaný	k2eAgInPc7d1	pozorovaný
tektonickými	tektonický	k2eAgInPc7d1	tektonický
útvary	útvar	k1gInPc7	útvar
jsou	být	k5eAaImIp3nP	být
hřebeny	hřeben	k1gInPc7	hřeben
dosahující	dosahující	k2eAgFnSc2d1	dosahující
výšky	výška	k1gFnSc2	výška
až	až	k9	až
1	[number]	k4	1
kilometru	kilometr	k1gInSc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
nejsou	být	k5eNaImIp3nP	být
tak	tak	k6eAd1	tak
časté	častý	k2eAgFnPc1d1	častá
jako	jako	k8xC	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Jupiterovo	Jupiterův	k2eAgNnSc1d1	Jupiterovo
měsíce	měsíc	k1gInPc4	měsíc
Europy	Europa	k1gFnSc2	Europa
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
tyto	tento	k3xDgInPc4	tento
útvary	útvar	k1gInPc4	útvar
a	a	k8xC	a
na	na	k7c4	na
rozsah	rozsah	k1gInSc4	rozsah
přetvoření	přetvoření	k1gNnSc4	přetvoření
povrchu	povrch	k1gInSc2	povrch
Enceladu	Encelad	k1gInSc2	Encelad
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
očividné	očividný	k2eAgNnSc1d1	očividné
<g/>
,	,	kIx,	,
že	že	k8xS	že
tektonické	tektonický	k2eAgInPc1d1	tektonický
procesy	proces	k1gInPc1	proces
hrály	hrát	k5eAaImAgInP	hrát
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
po	po	k7c4	po
většinu	většina	k1gFnSc4	většina
doby	doba	k1gFnSc2	doba
existence	existence	k1gFnSc2	existence
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Sonda	sonda	k1gFnSc1	sonda
Voyager	Voyager	k1gInSc1	Voyager
2	[number]	k4	2
pozorovala	pozorovat	k5eAaImAgFnS	pozorovat
dvě	dva	k4xCgFnPc4	dva
oblasti	oblast	k1gFnPc4	oblast
tvořené	tvořený	k2eAgInPc1d1	tvořený
hladkými	hladký	k2eAgFnPc7d1	hladká
pláněmi	pláň	k1gFnPc7	pláň
s	s	k7c7	s
relativně	relativně	k6eAd1	relativně
plochým	plochý	k2eAgInSc7d1	plochý
reliéfem	reliéf	k1gInSc7	reliéf
a	a	k8xC	a
relativně	relativně	k6eAd1	relativně
malým	malý	k2eAgInSc7d1	malý
počtem	počet	k1gInSc7	počet
impaktních	impaktní	k2eAgInPc2d1	impaktní
kráterů	kráter	k1gInPc2	kráter
(	(	kIx(	(
<g/>
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
jinými	jiný	k2eAgFnPc7d1	jiná
oblastmi	oblast	k1gFnPc7	oblast
silně	silně	k6eAd1	silně
posetými	posetý	k2eAgInPc7d1	posetý
impaktními	impaktní	k2eAgInPc7d1	impaktní
krátery	kráter	k1gInPc7	kráter
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgFnPc1	tento
oblasti	oblast	k1gFnPc1	oblast
jsou	být	k5eAaImIp3nP	být
relativně	relativně	k6eAd1	relativně
mladé	mladý	k2eAgFnPc1d1	mladá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Sarandib	Sarandiba	k1gFnPc2	Sarandiba
Planitia	Planitia	k1gFnSc1	Planitia
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
na	na	k7c6	na
základě	základ	k1gInSc6	základ
tehdejších	tehdejší	k2eAgInPc2d1	tehdejší
snímků	snímek	k1gInPc2	snímek
nepodařilo	podařit	k5eNaPmAgNnS	podařit
objevit	objevit	k5eAaPmF	objevit
žádný	žádný	k3yNgInSc4	žádný
impaktní	impaktní	k2eAgInSc4d1	impaktní
kráter	kráter	k1gInSc4	kráter
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jiné	jiný	k2eAgFnSc6d1	jiná
oblasti	oblast	k1gFnSc6	oblast
ležící	ležící	k2eAgFnSc1d1	ležící
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
Sarandib	Sarandiba	k1gFnPc2	Sarandiba
Planitia	Planitium	k1gNnSc2	Planitium
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
řada	řada	k1gFnSc1	řada
vzájemně	vzájemně	k6eAd1	vzájemně
se	se	k3xPyFc4	se
protínajících	protínající	k2eAgFnPc2d1	protínající
trhlin	trhlina	k1gFnPc2	trhlina
<g/>
.	.	kIx.	.
</s>
<s>
Sonda	sonda	k1gFnSc1	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
tyto	tento	k3xDgFnPc4	tento
oblasti	oblast	k1gFnSc6	oblast
nasnímala	nasnímat	k5eAaPmAgFnS	nasnímat
s	s	k7c7	s
mnohem	mnohem	k6eAd1	mnohem
lepším	dobrý	k2eAgNnSc7d2	lepší
rozlišením	rozlišení	k1gNnSc7	rozlišení
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nacházejí	nacházet	k5eAaImIp3nP	nacházet
nízké	nízký	k2eAgInPc4d1	nízký
hřebeny	hřeben	k1gInPc4	hřeben
a	a	k8xC	a
trhliny	trhlina	k1gFnPc4	trhlina
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
střihovým	střihový	k2eAgNnSc7d1	střihové
napětím	napětí	k1gNnSc7	napětí
<g/>
.	.	kIx.	.
</s>
<s>
Snímky	snímek	k1gInPc1	snímek
ve	v	k7c6	v
vysokém	vysoký	k2eAgNnSc6d1	vysoké
rozlišení	rozlišení	k1gNnSc6	rozlišení
oblasti	oblast	k1gFnSc2	oblast
Sarandib	Sarandiba	k1gFnPc2	Sarandiba
Planitia	Planitium	k1gNnSc2	Planitium
pak	pak	k6eAd1	pak
odhalily	odhalit	k5eAaPmAgInP	odhalit
pouze	pouze	k6eAd1	pouze
malý	malý	k2eAgInSc4d1	malý
počet	počet	k1gInSc4	počet
impaktních	impaktní	k2eAgInPc2d1	impaktní
kráterů	kráter	k1gInPc2	kráter
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
naznačilo	naznačit	k5eAaPmAgNnS	naznačit
že	že	k9	že
jednotka	jednotka	k1gFnSc1	jednotka
je	být	k5eAaImIp3nS	být
stará	starý	k2eAgFnSc1d1	stará
buď	buď	k8xC	buď
před	před	k7c7	před
170	[number]	k4	170
milióny	milión	k4xCgInPc7	milión
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
3,7	[number]	k4	3,7
miliardami	miliarda	k4xCgFnPc7	miliarda
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Rozptyl	rozptyl	k1gInSc1	rozptyl
stáří	stáří	k1gNnSc2	stáří
je	být	k5eAaImIp3nS	být
dán	dát	k5eAaPmNgInS	dát
závislostí	závislost	k1gFnSc7	závislost
předpokládané	předpokládaný	k2eAgFnSc2d1	předpokládaná
četnosti	četnost	k1gFnSc2	četnost
srážek	srážka	k1gFnPc2	srážka
Enceladu	Encelad	k1gInSc2	Encelad
s	s	k7c7	s
okolními	okolní	k2eAgNnPc7d1	okolní
tělesy	těleso	k1gNnPc7	těleso
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
globální	globální	k2eAgNnSc4d1	globální
pokrytí	pokrytí	k1gNnSc4	pokrytí
povrchu	povrch	k1gInSc2	povrch
Enceladu	Encelad	k1gInSc2	Encelad
fotografiemi	fotografia	k1gFnPc7	fotografia
získanými	získaný	k2eAgFnPc7d1	získaná
sondou	sonda	k1gFnSc7	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
objevit	objevit	k5eAaPmF	objevit
další	další	k2eAgFnPc4d1	další
hladké	hladký	k2eAgFnPc4d1	hladká
pláně	pláň	k1gFnPc4	pláň
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
polokouli	polokoule	k1gFnSc6	polokoule
přivrácené	přivrácený	k2eAgFnSc6d1	přivrácená
po	po	k7c6	po
směru	směr	k1gInSc6	směr
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
opačné	opačný	k2eAgFnSc6d1	opačná
straně	strana	k1gFnSc6	strana
Enceladu	Encelad	k1gInSc2	Encelad
než	než	k8xS	než
oblasti	oblast	k1gFnSc2	oblast
Sarandib	Sarandiba	k1gFnPc2	Sarandiba
a	a	k8xC	a
Diyar	Diyara	k1gFnPc2	Diyara
Planitiae	Planitia	k1gFnSc2	Planitia
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
povrch	povrch	k1gInSc4	povrch
posetý	posetý	k2eAgInSc4d1	posetý
sítí	síť	k1gFnSc7	síť
mělkých	mělký	k2eAgFnPc2d1	mělká
propadlin	propadlina	k1gFnPc2	propadlina
a	a	k8xC	a
nízkých	nízký	k2eAgInPc2d1	nízký
hřebenů	hřeben	k1gInPc2	hřeben
<g/>
,	,	kIx,	,
tvarem	tvar	k1gInSc7	tvar
podobných	podobný	k2eAgInPc2d1	podobný
deformacím	deformace	k1gFnPc3	deformace
známým	známý	k2eAgFnPc3d1	známá
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
jižního	jižní	k2eAgInSc2d1	jižní
pólu	pól	k1gInSc2	pól
<g/>
.	.	kIx.	.
</s>
<s>
Naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
to	ten	k3xDgNnSc1	ten
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gInSc1	jejich
vhled	vhled	k1gInSc1	vhled
je	být	k5eAaImIp3nS	být
ovlivněn	ovlivněn	k2eAgInSc1d1	ovlivněn
slapovým	slapový	k2eAgNnSc7d1	slapové
působením	působení	k1gNnSc7	působení
Saturnu	Saturn	k1gInSc2	Saturn
na	na	k7c4	na
Enceladus	Enceladus	k1gInSc4	Enceladus
<g/>
.	.	kIx.	.
</s>
<s>
Snímky	snímek	k1gInPc4	snímek
pořízené	pořízený	k2eAgInPc4d1	pořízený
sondou	sonda	k1gFnSc7	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
během	během	k7c2	během
průletu	průlet	k1gInSc2	průlet
ze	z	k7c2	z
dne	den	k1gInSc2	den
14	[number]	k4	14
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2005	[number]	k4	2005
odhalily	odhalit	k5eAaPmAgInP	odhalit
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
tektonicky	tektonicky	k6eAd1	tektonicky
deformovanou	deformovaný	k2eAgFnSc4d1	deformovaná
oblast	oblast	k1gFnSc4	oblast
obklopující	obklopující	k2eAgInSc4d1	obklopující
jižní	jižní	k2eAgInSc4d1	jižní
pól	pól	k1gInSc4	pól
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
oblast	oblast	k1gFnSc1	oblast
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
od	od	k7c2	od
pólu	pól	k1gInSc2	pól
až	až	k6eAd1	až
k	k	k7c3	k
60	[number]	k4	60
<g/>
°	°	k?	°
jižní	jižní	k2eAgFnSc2d1	jižní
šířky	šířka	k1gFnSc2	šířka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pokryta	pokrýt	k5eAaPmNgFnS	pokrýt
tektonickými	tektonický	k2eAgFnPc7d1	tektonická
prasklinami	prasklina	k1gFnPc7	prasklina
a	a	k8xC	a
hřebeny	hřeben	k1gInPc7	hřeben
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nachází	nacházet	k5eAaImIp3nS	nacházet
jen	jen	k9	jen
minimum	minimum	k1gNnSc1	minimum
impaktních	impaktní	k2eAgInPc2d1	impaktní
kráterů	kráter	k1gInPc2	kráter
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
nejenom	nejenom	k6eAd1	nejenom
o	o	k7c4	o
nejmladší	mladý	k2eAgFnSc4d3	nejmladší
část	část	k1gFnSc4	část
povrchu	povrch	k1gInSc2	povrch
Enceladu	Encelad	k1gInSc2	Encelad
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
o	o	k7c4	o
nejmladší	mladý	k2eAgInSc4d3	nejmladší
povrch	povrch	k1gInSc4	povrch
na	na	k7c6	na
jakémkoliv	jakýkoliv	k3yIgNnSc6	jakýkoliv
středně	středně	k6eAd1	středně
velkém	velký	k2eAgInSc6d1	velký
ledovém	ledový	k2eAgInSc6d1	ledový
měsíci	měsíc	k1gInSc6	měsíc
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Odhady	odhad	k1gInPc1	odhad
stáří	stáří	k1gNnSc2	stáří
na	na	k7c6	na
základě	základ	k1gInSc6	základ
modelování	modelování	k1gNnSc2	modelování
pravděpodobnosti	pravděpodobnost	k1gFnPc4	pravděpodobnost
vzniku	vznik	k1gInSc2	vznik
impaktních	impaktní	k2eAgInPc2d1	impaktní
kráterů	kráter	k1gInPc2	kráter
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
oblast	oblast	k1gFnSc1	oblast
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
mladší	mladý	k2eAgFnSc1d2	mladší
méně	málo	k6eAd2	málo
než	než	k8xS	než
500	[number]	k4	500
000	[number]	k4	000
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Poblíž	poblíž	k6eAd1	poblíž
středu	středa	k1gFnSc4	středa
této	tento	k3xDgFnSc2	tento
oblasti	oblast	k1gFnSc2	oblast
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nP	nacházet
čtyři	čtyři	k4xCgFnPc1	čtyři
rozsáhlé	rozsáhlý	k2eAgFnPc1d1	rozsáhlá
praskliny	prasklina	k1gFnPc1	prasklina
ohraničené	ohraničený	k2eAgFnPc1d1	ohraničená
hřbety	hřbet	k1gInPc4	hřbet
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
neoficiálně	oficiálně	k6eNd1	oficiálně
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
tygří	tygří	k2eAgInPc4d1	tygří
pruhy	pruh	k1gInPc4	pruh
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaImIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
nejmladší	mladý	k2eAgInPc4d3	nejmladší
útvary	útvar	k1gInPc4	útvar
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
okolí	okolí	k1gNnSc1	okolí
tvoří	tvořit	k5eAaImIp3nS	tvořit
mentolově	mentolově	k6eAd1	mentolově
zelený	zelený	k2eAgInSc1d1	zelený
vodní	vodní	k2eAgInSc1d1	vodní
led	led	k1gInSc1	led
(	(	kIx(	(
<g/>
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
falešnou	falešný	k2eAgFnSc4d1	falešná
barvu	barva	k1gFnSc4	barva
viditelnou	viditelný	k2eAgFnSc4d1	viditelná
na	na	k7c6	na
infračervených	infračervený	k2eAgInPc6d1	infračervený
snímcích	snímek	k1gInPc6	snímek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
spatřit	spatřit	k5eAaPmF	spatřit
na	na	k7c6	na
výchozech	výchoz	k1gInPc6	výchoz
a	a	k8xC	a
na	na	k7c6	na
stěnách	stěna	k1gFnPc6	stěna
prasklin	prasklina	k1gFnPc2	prasklina
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
okolní	okolní	k2eAgFnPc1d1	okolní
hladké	hladký	k2eAgFnPc1d1	hladká
pláně	pláň	k1gFnPc1	pláň
jsou	být	k5eAaImIp3nP	být
pokryty	pokryt	k2eAgFnPc1d1	pokryta
"	"	kIx"	"
<g/>
modrým	modrý	k2eAgInSc7d1	modrý
<g/>
"	"	kIx"	"
ledem	led	k1gInSc7	led
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
napovídá	napovídat	k5eAaBmIp3nS	napovídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
oblast	oblast	k1gFnSc1	oblast
natolik	natolik	k6eAd1	natolik
mladá	mladý	k2eAgFnSc1d1	mladá
<g/>
,	,	kIx,	,
že	že	k8xS	že
nestihla	stihnout	k5eNaPmAgFnS	stihnout
být	být	k5eAaImF	být
ještě	ještě	k9	ještě
pokryta	pokrýt	k5eAaPmNgFnS	pokrýt
ledovými	ledový	k2eAgFnPc7d1	ledová
částicemi	částice	k1gFnPc7	částice
dopadajícími	dopadající	k2eAgFnPc7d1	dopadající
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
z	z	k7c2	z
prstence	prstenec	k1gInSc2	prstenec
E.	E.	kA	E.
Výsledky	výsledek	k1gInPc1	výsledek
pořízené	pořízený	k2eAgFnPc4d1	pořízená
spektrometrem	spektrometr	k1gInSc7	spektrometr
pro	pro	k7c4	pro
viditelné	viditelný	k2eAgNnSc4d1	viditelné
a	a	k8xC	a
infračervené	infračervený	k2eAgNnSc4d1	infračervené
pásmo	pásmo	k1gNnSc4	pásmo
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
VIMS	VIMS	kA	VIMS
<g/>
)	)	kIx)	)
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nazelenalý	nazelenalý	k2eAgInSc4d1	nazelenalý
materiál	materiál	k1gInSc4	materiál
obklopující	obklopující	k2eAgInPc4d1	obklopující
tygří	tygří	k2eAgInPc4d1	tygří
pruhy	pruh	k1gInPc4	pruh
je	být	k5eAaImIp3nS	být
chemický	chemický	k2eAgInSc1d1	chemický
odlišný	odlišný	k2eAgInSc1d1	odlišný
od	od	k7c2	od
zbytku	zbytek	k1gInSc2	zbytek
povrchu	povrch	k1gInSc2	povrch
měsíce	měsíc	k1gInPc1	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k7c2	uvnitř
pruhů	pruh	k1gInPc2	pruh
detekoval	detekovat	k5eAaImAgInS	detekovat
VIMS	VIMS	kA	VIMS
krystalický	krystalický	k2eAgInSc1d1	krystalický
vodní	vodní	k2eAgInSc1d1	vodní
led	led	k1gInSc1	led
svědčící	svědčící	k2eAgInSc1d1	svědčící
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
tento	tento	k3xDgInSc1	tento
materiál	materiál	k1gInSc1	materiál
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
mladší	mladý	k2eAgMnSc1d2	mladší
než	než	k8xS	než
1000	[number]	k4	1000
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
či	či	k8xC	či
by	by	kYmCp3nS	by
alternativně	alternativně	k6eAd1	alternativně
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
teplotně	teplotně	k6eAd1	teplotně
pozměněn	pozměnit	k5eAaPmNgInS	pozměnit
v	v	k7c6	v
nedávné	dávný	k2eNgFnSc6d1	nedávná
minulosti	minulost	k1gFnSc6	minulost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
VIMS	VIMS	kA	VIMS
taktéž	taktéž	k?	taktéž
detekoval	detekovat	k5eAaImAgInS	detekovat
jednoduché	jednoduchý	k2eAgFnPc4d1	jednoduchá
organické	organický	k2eAgFnPc4d1	organická
sloučeniny	sloučenina	k1gFnPc4	sloučenina
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
tygřích	tygří	k2eAgInPc2d1	tygří
pruhů	pruh	k1gInPc2	pruh
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
nebyly	být	k5eNaImAgInP	být
prozatím	prozatím	k6eAd1	prozatím
detekovány	detekovat	k5eAaImNgInP	detekovat
nikde	nikde	k6eAd1	nikde
jinde	jinde	k6eAd1	jinde
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Enceladu	Encelad	k1gInSc2	Encelad
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
průletu	průlet	k1gInSc6	průlet
ze	z	k7c2	z
dne	den	k1gInSc2	den
14	[number]	k4	14
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2005	[number]	k4	2005
byla	být	k5eAaImAgFnS	být
jedna	jeden	k4xCgFnSc1	jeden
oblast	oblast	k1gFnSc1	oblast
charakterizovaná	charakterizovaný	k2eAgFnSc1d1	charakterizovaná
přítomností	přítomnost	k1gFnSc7	přítomnost
"	"	kIx"	"
<g/>
modrého	modrý	k2eAgInSc2d1	modrý
<g/>
"	"	kIx"	"
ledu	led	k1gInSc2	led
nafocena	nafotit	k5eAaPmNgFnS	nafotit
ve	v	k7c6	v
vysokém	vysoký	k2eAgInSc6d1	vysoký
detailu	detail	k1gInSc6	detail
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
odhalit	odhalit	k5eAaPmF	odhalit
<g/>
,	,	kIx,	,
že	že	k8xS	že
oblast	oblast	k1gFnSc1	oblast
byla	být	k5eAaImAgFnS	být
významně	významně	k6eAd1	významně
tektonicky	tektonicky	k6eAd1	tektonicky
deformována	deformován	k2eAgFnSc1d1	deformována
a	a	k8xC	a
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
pokryta	pokrýt	k5eAaPmNgFnS	pokrýt
kameny	kámen	k1gInPc7	kámen
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
desítek	desítka	k1gFnPc2	desítka
až	až	k8xS	až
stovek	stovka	k1gFnPc2	stovka
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Hranice	hranice	k1gFnSc1	hranice
jižní	jižní	k2eAgFnSc2d1	jižní
polární	polární	k2eAgFnSc2d1	polární
oblasti	oblast	k1gFnSc2	oblast
je	být	k5eAaImIp3nS	být
ohraničena	ohraničit	k5eAaPmNgFnS	ohraničit
souborem	soubor	k1gInSc7	soubor
rovnoběžných	rovnoběžný	k2eAgInPc2d1	rovnoběžný
hřbetů	hřbet	k1gInPc2	hřbet
a	a	k8xC	a
údolí	údolí	k1gNnSc2	údolí
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
Y	Y	kA	Y
a	a	k8xC	a
V.	V.	kA	V.
Tvar	tvar	k1gInSc1	tvar
<g/>
,	,	kIx,	,
orientace	orientace	k1gFnSc1	orientace
a	a	k8xC	a
pozice	pozice	k1gFnSc1	pozice
těchto	tento	k3xDgInPc2	tento
útvarů	útvar	k1gInPc2	útvar
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
vlivem	vlivem	k7c2	vlivem
celkové	celkový	k2eAgFnSc2d1	celková
změny	změna	k1gFnSc2	změna
tvaru	tvar	k1gInSc2	tvar
měsíce	měsíc	k1gInSc2	měsíc
Enceladu	Encelad	k1gInSc2	Encelad
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
2006	[number]	k4	2006
existovaly	existovat	k5eAaImAgFnP	existovat
dvě	dva	k4xCgFnPc1	dva
hlavní	hlavní	k2eAgFnPc1d1	hlavní
teorie	teorie	k1gFnPc1	teorie
vysvětlující	vysvětlující	k2eAgFnPc1d1	vysvětlující
jejich	jejich	k3xOp3gInSc4	jejich
vznik	vznik	k1gInSc4	vznik
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgNnPc1	dva
dvě	dva	k4xCgNnPc1	dva
předpokládají	předpokládat	k5eAaImIp3nP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
oběžná	oběžný	k2eAgFnSc1d1	oběžná
dráha	dráha	k1gFnSc1	dráha
Enceladu	Encelad	k1gInSc2	Encelad
se	se	k3xPyFc4	se
posunula	posunout	k5eAaPmAgFnS	posunout
blíže	blízce	k6eAd2	blízce
k	k	k7c3	k
Saturnu	Saturn	k1gInSc2	Saturn
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
zrychlení	zrychlení	k1gNnSc3	zrychlení
rotace	rotace	k1gFnSc2	rotace
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Následkem	následkem	k7c2	následkem
toho	ten	k3xDgNnSc2	ten
bylo	být	k5eAaImAgNnS	být
buď	buď	k8xC	buď
zploštění	zploštění	k1gNnSc1	zploštění
měsíce	měsíc	k1gInSc2	měsíc
nebo	nebo	k8xC	nebo
nárůst	nárůst	k1gInSc1	nárůst
množství	množství	k1gNnSc2	množství
teplého	teplé	k1gNnSc2	teplé
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
hustého	hustý	k2eAgInSc2d1	hustý
materiálu	materiál	k1gInSc2	materiál
uvnitř	uvnitř	k7c2	uvnitř
Enceladu	Encelad	k1gInSc2	Encelad
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
začal	začít	k5eAaPmAgInS	začít
pohybovat	pohybovat	k5eAaImF	pohybovat
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
jižnímu	jižní	k2eAgInSc3d1	jižní
pólu	pól	k1gInSc3	pól
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Následkem	následkem	k7c2	následkem
čehož	což	k3yQnSc2	což
se	se	k3xPyFc4	se
tvar	tvar	k1gInSc1	tvar
měsíce	měsíc	k1gInSc2	měsíc
pokoušel	pokoušet	k5eAaImAgInS	pokoušet
této	tento	k3xDgFnSc6	tento
nové	nový	k2eAgFnSc6d1	nová
situaci	situace	k1gFnSc6	situace
přizpůsobit	přizpůsobit	k5eAaPmF	přizpůsobit
<g/>
.	.	kIx.	.
</s>
<s>
Problémem	problém	k1gInSc7	problém
této	tento	k3xDgFnSc2	tento
představy	představa	k1gFnSc2	představa
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
jak	jak	k8xS	jak
jižní	jižní	k2eAgFnSc1d1	jižní
oblast	oblast	k1gFnSc1	oblast
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
pólu	pól	k1gInSc2	pól
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
severní	severní	k2eAgFnSc1d1	severní
oblast	oblast	k1gFnSc1	oblast
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgFnP	mít
mít	mít	k5eAaImF	mít
podobné	podobný	k2eAgFnPc4d1	podobná
tektonické	tektonický	k2eAgFnPc4d1	tektonická
deformace	deformace	k1gFnPc4	deformace
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ale	ale	k9	ale
nepozorujeme	pozorovat	k5eNaImIp1nP	pozorovat
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
oblast	oblast	k1gFnSc1	oblast
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
severního	severní	k2eAgInSc2d1	severní
pólu	pól	k1gInSc2	pól
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
poseta	posít	k5eAaPmNgFnS	posít
impaktními	impaktní	k2eAgInPc7d1	impaktní
krátery	kráter	k1gInPc7	kráter
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
mnohem	mnohem	k6eAd1	mnohem
starší	starý	k2eAgMnSc1d2	starší
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
napovídá	napovídat	k5eAaBmIp3nS	napovídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
i	i	k9	i
značně	značně	k6eAd1	značně
tlustší	tlustý	k2eAgNnSc1d2	tlustší
<g/>
.	.	kIx.	.
</s>
<s>
Nabízí	nabízet	k5eAaImIp3nS	nabízet
se	se	k3xPyFc4	se
tak	tak	k9	tak
možnost	možnost	k1gFnSc1	možnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
právě	právě	k9	právě
rozdílná	rozdílný	k2eAgFnSc1d1	rozdílná
tloušťka	tloušťka	k1gFnSc1	tloušťka
litosféry	litosféra	k1gFnSc2	litosféra
měsíce	měsíc	k1gInSc2	měsíc
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
vysvětlení	vysvětlení	k1gNnSc2	vysvětlení
rozdílnosti	rozdílnost	k1gFnSc2	rozdílnost
ve	v	k7c6	v
vzhledu	vzhled	k1gInSc6	vzhled
severní	severní	k2eAgFnSc2d1	severní
a	a	k8xC	a
jižní	jižní	k2eAgFnSc2d1	jižní
polokoule	polokoule	k1gFnSc2	polokoule
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
misí	mise	k1gFnSc7	mise
Cassini	Cassin	k2eAgMnPc1d1	Cassin
jsme	být	k5eAaImIp1nP	být
věděli	vědět	k5eAaImAgMnP	vědět
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
málo	málo	k6eAd1	málo
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
vypadá	vypadat	k5eAaImIp3nS	vypadat
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
stavba	stavba	k1gFnSc1	stavba
Enceladu	Encelad	k1gInSc2	Encelad
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
změnilo	změnit	k5eAaPmAgNnS	změnit
s	s	k7c7	s
průlety	průlet	k1gInPc7	průlet
sondy	sonda	k1gFnSc2	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
poskytly	poskytnout	k5eAaPmAgInP	poskytnout
jak	jak	k6eAd1	jak
přesné	přesný	k2eAgFnPc1d1	přesná
informace	informace	k1gFnPc1	informace
o	o	k7c6	o
hmotnosti	hmotnost	k1gFnSc6	hmotnost
a	a	k8xC	a
tvaru	tvar	k1gInSc6	tvar
měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
detailní	detailní	k2eAgFnPc4d1	detailní
fotografie	fotografia	k1gFnPc4	fotografia
povrchu	povrch	k1gInSc2	povrch
a	a	k8xC	a
řadu	řad	k1gInSc2	řad
geofyzikálních	geofyzikální	k2eAgNnPc2d1	geofyzikální
měření	měření	k1gNnPc2	měření
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
přibližné	přibližný	k2eAgNnSc1d1	přibližné
určení	určení	k1gNnSc1	určení
hmotnosti	hmotnost	k1gFnSc2	hmotnost
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
udělat	udělat	k5eAaPmF	udělat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
průletů	průlet	k1gInPc2	průlet
sond	sonda	k1gFnPc2	sonda
Voyager	Voyagra	k1gFnPc2	Voyagra
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
nám	my	k3xPp1nPc3	my
dalo	dát	k5eAaPmAgNnS	dát
šanci	šance	k1gFnSc4	šance
odhadnout	odhadnout	k5eAaPmF	odhadnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
Enceladus	Enceladus	k1gMnSc1	Enceladus
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
tvořen	tvořit	k5eAaImNgInS	tvořit
téměř	téměř	k6eAd1	téměř
kompletně	kompletně	k6eAd1	kompletně
z	z	k7c2	z
vodního	vodní	k2eAgInSc2d1	vodní
ledu	led	k1gInSc2	led
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
na	na	k7c6	na
základě	základ	k1gInSc6	základ
gravitačního	gravitační	k2eAgNnSc2d1	gravitační
působení	působení	k1gNnSc2	působení
Enceladu	Encelad	k1gInSc2	Encelad
na	na	k7c4	na
sondu	sonda	k1gFnSc4	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
určit	určit	k5eAaPmF	určit
hmotnost	hmotnost	k1gFnSc4	hmotnost
měsíce	měsíc	k1gInSc2	měsíc
mnohem	mnohem	k6eAd1	mnohem
přesněji	přesně	k6eAd2	přesně
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
zpřesnit	zpřesnit	k5eAaPmF	zpřesnit
odhady	odhad	k1gInPc4	odhad
jeho	jeho	k3xOp3gFnSc2	jeho
hustoty	hustota	k1gFnSc2	hustota
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
tak	tak	k9	tak
k	k	k7c3	k
navýšení	navýšení	k1gNnSc3	navýšení
hustoty	hustota	k1gFnSc2	hustota
Enceladu	Encelad	k1gInSc2	Encelad
na	na	k7c4	na
1,61	[number]	k4	1,61
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
cm	cm	kA	cm
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
hustota	hustota	k1gFnSc1	hustota
ostatních	ostatní	k2eAgInPc2d1	ostatní
středně	středně	k6eAd1	středně
velkých	velký	k2eAgInPc2d1	velký
ledových	ledový	k2eAgInPc2d1	ledový
měsíců	měsíc	k1gInPc2	měsíc
Saturnu	Saturn	k1gInSc2	Saturn
<g/>
.	.	kIx.	.
</s>
<s>
Naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Enceladus	Enceladus	k1gInSc1	Enceladus
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
větší	veliký	k2eAgNnSc4d2	veliký
zastoupení	zastoupení	k1gNnSc4	zastoupení
silikátů	silikát	k1gInPc2	silikát
a	a	k8xC	a
železa	železo	k1gNnSc2	železo
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
nitru	nitro	k1gNnSc6	nitro
<g/>
.	.	kIx.	.
</s>
<s>
Vědci	vědec	k1gMnPc1	vědec
navrhli	navrhnout	k5eAaPmAgMnP	navrhnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
měsíc	měsíc	k1gInSc1	měsíc
Iapetus	Iapetus	k1gMnSc1	Iapetus
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
ledové	ledový	k2eAgInPc4d1	ledový
měsíce	měsíc	k1gInPc4	měsíc
Saturnu	Saturn	k1gInSc2	Saturn
včetně	včetně	k7c2	včetně
Enceladu	Encelad	k1gInSc2	Encelad
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
relativně	relativně	k6eAd1	relativně
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
zformování	zformování	k1gNnSc6	zformování
prachového	prachový	k2eAgNnSc2d1	prachové
mračna	mračno	k1gNnSc2	mračno
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yQgInSc2	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
Saturn	Saturn	k1gInSc1	Saturn
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
projevilo	projevit	k5eAaPmAgNnS	projevit
ve	v	k7c6	v
vysokém	vysoký	k2eAgNnSc6d1	vysoké
zastoupení	zastoupení	k1gNnSc6	zastoupení
radionuklidů	radionuklid	k1gInPc2	radionuklid
s	s	k7c7	s
krátkým	krátký	k2eAgInSc7d1	krátký
poločasem	poločas	k1gInSc7	poločas
rozpadu	rozpad	k1gInSc2	rozpad
v	v	k7c6	v
jejich	jejich	k3xOp3gNnSc6	jejich
nitru	nitro	k1gNnSc6	nitro
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
radionuklidy	radionuklid	k1gInPc1	radionuklid
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
například	například	k6eAd1	například
Al-	Al-	k1gFnPc1	Al-
<g/>
26	[number]	k4	26
a	a	k8xC	a
železo-	železo-	k?	železo-
<g/>
60	[number]	k4	60
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
krátkou	krátký	k2eAgFnSc4d1	krátká
životnost	životnost	k1gFnSc4	životnost
<g/>
,	,	kIx,	,
během	během	k7c2	během
které	který	k3yRgFnSc2	který
ale	ale	k9	ale
produkují	produkovat	k5eAaImIp3nP	produkovat
relativně	relativně	k6eAd1	relativně
rychle	rychle	k6eAd1	rychle
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
tepla	teplo	k1gNnSc2	teplo
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
zastoupení	zastoupení	k1gNnSc2	zastoupení
těchto	tento	k3xDgInPc2	tento
radionuklidů	radionuklid	k1gInPc2	radionuklid
s	s	k7c7	s
krátkou	krátký	k2eAgFnSc7d1	krátká
životností	životnost	k1gFnSc7	životnost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
tak	tak	k6eAd1	tak
doplnily	doplnit	k5eAaPmAgFnP	doplnit
produkcí	produkce	k1gFnSc7	produkce
tepla	teplo	k1gNnSc2	teplo
radionuklidy	radionuklid	k1gInPc7	radionuklid
s	s	k7c7	s
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
poločasem	poločas	k1gInSc7	poločas
rozpadu	rozpad	k1gInSc2	rozpad
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
Enceladus	Enceladus	k1gInSc1	Enceladus
neměl	mít	k5eNaImAgInS	mít
dostatek	dostatek	k1gInSc4	dostatek
tepla	teplo	k1gNnSc2	teplo
pro	pro	k7c4	pro
zabránění	zabránění	k1gNnSc4	zabránění
rychlého	rychlý	k2eAgNnSc2d1	rychlé
zamrznutí	zamrznutí	k1gNnSc2	zamrznutí
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
relativně	relativně	k6eAd1	relativně
vysoký	vysoký	k2eAgInSc4d1	vysoký
podíl	podíl	k1gInSc4	podíl
hornin	hornina	k1gFnPc2	hornina
ve	v	k7c6	v
stavbě	stavba	k1gFnSc6	stavba
Enceladu	Encelad	k1gInSc2	Encelad
odrážející	odrážející	k2eAgFnSc2d1	odrážející
se	se	k3xPyFc4	se
ve	v	k7c6	v
vysokém	vysoký	k2eAgNnSc6d1	vysoké
nabohacení	nabohacení	k1gNnSc6	nabohacení
právě	právě	k9	právě
o	o	k7c6	o
sup	sup	k1gMnSc1	sup
<g/>
>	>	kIx)	>
<g/>
26	[number]	k4	26
<g/>
Al	ala	k1gFnPc2	ala
a	a	k8xC	a
<g/>
60	[number]	k4	60
<g/>
Fe	Fe	k1gFnPc2	Fe
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
měsíc	měsíc	k1gInSc1	měsíc
prošel	projít	k5eAaPmAgInS	projít
vnitřní	vnitřní	k2eAgFnSc7d1	vnitřní
diferenciací	diferenciace	k1gFnSc7	diferenciace
<g/>
,	,	kIx,	,
během	během	k7c2	během
které	který	k3yRgFnSc2	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
ledový	ledový	k2eAgInSc1d1	ledový
plášť	plášť	k1gInSc1	plášť
a	a	k8xC	a
kamenité	kamenitý	k2eAgNnSc1d1	kamenité
jádro	jádro	k1gNnSc1	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Následné	následný	k2eAgNnSc1d1	následné
teplo	teplo	k1gNnSc1	teplo
uvolňované	uvolňovaný	k2eAgNnSc1d1	uvolňované
radioaktivním	radioaktivní	k2eAgInSc7d1	radioaktivní
rozpadem	rozpad	k1gInSc7	rozpad
prvků	prvek	k1gInPc2	prvek
s	s	k7c7	s
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
poločasem	poločas	k1gInSc7	poločas
rozpadu	rozpad	k1gInSc2	rozpad
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
s	s	k7c7	s
teplem	teplo	k1gNnSc7	teplo
vznikajícím	vznikající	k2eAgNnSc7d1	vznikající
během	během	k7c2	během
slapového	slapový	k2eAgNnSc2d1	slapové
zahřívání	zahřívání	k1gNnSc2	zahřívání
mohlo	moct	k5eAaImAgNnS	moct
zvýšit	zvýšit	k5eAaPmF	zvýšit
teplotu	teplota	k1gFnSc4	teplota
jádra	jádro	k1gNnSc2	jádro
až	až	k9	až
na	na	k7c4	na
1000	[number]	k4	1000
K	K	kA	K
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
by	by	kYmCp3nS	by
stačilo	stačit	k5eAaBmAgNnS	stačit
k	k	k7c3	k
roztavení	roztavení	k1gNnSc3	roztavení
vnitřního	vnitřní	k2eAgInSc2d1	vnitřní
ledového	ledový	k2eAgInSc2d1	ledový
pláště	plášť	k1gInSc2	plášť
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
Enceladus	Enceladus	k1gInSc1	Enceladus
stále	stále	k6eAd1	stále
aktivní	aktivní	k2eAgMnSc1d1	aktivní
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
jádra	jádro	k1gNnSc2	jádro
měsíce	měsíc	k1gInSc2	měsíc
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
roztavená	roztavený	k2eAgFnSc1d1	roztavená
a	a	k8xC	a
schopna	schopen	k2eAgFnSc1d1	schopna
tak	tak	k9	tak
pohybu	pohyb	k1gInSc3	pohyb
při	při	k7c6	při
působení	působení	k1gNnSc6	působení
slapových	slapový	k2eAgFnPc2d1	slapová
sil	síla	k1gFnPc2	síla
Saturnu	Saturn	k1gInSc2	Saturn
<g/>
.	.	kIx.	.
</s>
<s>
Slapové	slapový	k2eAgNnSc4d1	slapové
zahřívání	zahřívání	k1gNnSc4	zahřívání
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
už	už	k6eAd1	už
způsobené	způsobený	k2eAgNnSc1d1	způsobené
rezonancí	rezonance	k1gFnSc7	rezonance
s	s	k7c7	s
Dione	Dion	k1gInSc5	Dion
nebo	nebo	k8xC	nebo
librací	librace	k1gFnSc7	librace
měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
stále	stále	k6eAd1	stále
udržovat	udržovat	k5eAaImF	udržovat
tyto	tento	k3xDgFnPc4	tento
horké	horký	k2eAgFnPc4d1	horká
oblasti	oblast	k1gFnPc4	oblast
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
a	a	k8xC	a
tak	tak	k6eAd1	tak
umožňovat	umožňovat	k5eAaImF	umožňovat
současnou	současný	k2eAgFnSc4d1	současná
geologickou	geologický	k2eAgFnSc4d1	geologická
aktivitu	aktivita	k1gFnSc4	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Současný	současný	k2eAgInSc1d1	současný
tvar	tvar	k1gInSc1	tvar
měsíce	měsíc	k1gInSc2	měsíc
napovídá	napovídat	k5eAaBmIp3nS	napovídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Enceladus	Enceladus	k1gInSc1	Enceladus
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
hydostratické	hydostratický	k2eAgFnSc6d1	hydostratický
rovnováze	rovnováha	k1gFnSc6	rovnováha
<g/>
.	.	kIx.	.
</s>
<s>
Měsíc	měsíc	k1gInSc1	měsíc
tak	tak	k9	tak
nejspíše	nejspíše	k9	nejspíše
dříve	dříve	k6eAd2	dříve
rotoval	rotovat	k5eAaImAgMnS	rotovat
rychleji	rychle	k6eAd2	rychle
než	než	k8xS	než
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
<s>
Gravitační	gravitační	k2eAgNnPc1d1	gravitační
měření	měření	k1gNnPc1	měření
provedená	provedený	k2eAgNnPc1d1	provedené
sondou	sonda	k1gFnSc7	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
ukázala	ukázat	k5eAaPmAgNnP	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
hustota	hustota	k1gFnSc1	hustota
jádra	jádro	k1gNnSc2	jádro
je	být	k5eAaImIp3nS	být
nízká	nízký	k2eAgFnSc1d1	nízká
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
napovídá	napovídat	k5eAaBmIp3nS	napovídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
vyjma	vyjma	k7c2	vyjma
silikátových	silikátový	k2eAgFnPc2d1	silikátová
hornin	hornina	k1gFnPc2	hornina
i	i	k8xC	i
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
Důkazy	důkaz	k1gInPc1	důkaz
o	o	k7c6	o
přítomnosti	přítomnost	k1gFnSc6	přítomnost
kapalné	kapalný	k2eAgFnSc2d1	kapalná
vody	voda	k1gFnSc2	voda
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
Enceladu	Encelad	k1gInSc2	Encelad
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
hromadit	hromadit	k5eAaImF	hromadit
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vědci	vědec	k1gMnPc1	vědec
objevili	objevit	k5eAaPmAgMnP	objevit
mračna	mračna	k1gFnSc1	mračna
obsahující	obsahující	k2eAgFnSc4d1	obsahující
vodní	vodní	k2eAgFnSc4d1	vodní
páru	pára	k1gFnSc4	pára
vznášející	vznášející	k2eAgFnSc2d1	vznášející
se	se	k3xPyFc4	se
nad	nad	k7c7	nad
oblastí	oblast	k1gFnSc7	oblast
jižního	jižní	k2eAgInSc2d1	jižní
pólu	pól	k1gInSc2	pól
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
vyvrhovány	vyvrhovat	k5eAaImNgInP	vyvrhovat
z	z	k7c2	z
povrchu	povrch	k1gInSc2	povrch
do	do	k7c2	do
okolního	okolní	k2eAgInSc2d1	okolní
prostoru	prostor	k1gInSc2	prostor
rychlostí	rychlost	k1gFnSc7	rychlost
až	až	k6eAd1	až
2189	[number]	k4	2189
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
a	a	k8xC	a
v	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
množství	množství	k1gNnSc1	množství
okolo	okolo	k7c2	okolo
250	[number]	k4	250
kg	kg	kA	kg
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
sonda	sonda	k1gFnSc1	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
odhalila	odhalit	k5eAaPmAgFnS	odhalit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Enceladus	Enceladus	k1gInSc1	Enceladus
je	být	k5eAaImIp3nS	být
zdrojem	zdroj	k1gInSc7	zdroj
částic	částice	k1gFnPc2	částice
Saturnovo	Saturnův	k2eAgNnSc4d1	Saturnovo
prstence	prstenec	k1gInPc4	prstenec
E.	E.	kA	E.
Pří	přít	k5eAaImIp3nS	přít
průzkumu	průzkum	k1gInSc3	průzkum
povrchu	povrch	k1gInSc2	povrch
měsíce	měsíc	k1gInSc2	měsíc
bylo	být	k5eAaImAgNnS	být
objeveno	objevit	k5eAaPmNgNnS	objevit
<g/>
,	,	kIx,	,
že	že	k8xS	že
povrch	povrch	k1gInSc4	povrch
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
tygřích	tygří	k2eAgInPc2d1	tygří
pruhů	pruh	k1gInPc2	pruh
je	být	k5eAaImIp3nS	být
nerovnoměrně	rovnoměrně	k6eNd1	rovnoměrně
pokryt	pokrýt	k5eAaPmNgMnS	pokrýt
částečkami	částečka	k1gFnPc7	částečka
solí	sůl	k1gFnPc2	sůl
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
materiál	materiál	k1gInSc1	materiál
prstence	prstenec	k1gInSc2	prstenec
E	E	kA	E
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
převážně	převážně	k6eAd1	převážně
krystalky	krystalek	k1gInPc4	krystalek
vodního	vodní	k2eAgInSc2d1	vodní
ledu	led	k1gInSc2	led
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
dáno	dát	k5eAaPmNgNnS	dát
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
částice	částice	k1gFnPc1	částice
soli	sůl	k1gFnSc2	sůl
jsou	být	k5eAaImIp3nP	být
těžší	těžký	k2eAgMnSc1d2	těžší
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
nemohou	moct	k5eNaImIp3nP	moct
tak	tak	k6eAd1	tak
snadno	snadno	k6eAd1	snadno
uniknout	uniknout	k5eAaPmF	uniknout
z	z	k7c2	z
gravitačního	gravitační	k2eAgNnSc2d1	gravitační
působení	působení	k1gNnSc2	působení
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Přítomnost	přítomnost	k1gFnSc1	přítomnost
solí	solit	k5eAaImIp3nS	solit
tak	tak	k6eAd1	tak
ale	ale	k9	ale
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
nachází	nacházet	k5eAaImIp3nS	nacházet
slaný	slaný	k2eAgInSc1d1	slaný
oceán	oceán	k1gInSc1	oceán
kapalné	kapalný	k2eAgFnSc2d1	kapalná
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Sondě	sonda	k1gFnSc3	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
se	se	k3xPyFc4	se
taktéž	taktéž	k?	taktéž
podařilo	podařit	k5eAaPmAgNnS	podařit
v	v	k7c6	v
mračnech	mračno	k1gNnPc6	mračno
objevit	objevit	k5eAaPmF	objevit
známky	známka	k1gFnPc4	známka
jednoduchých	jednoduchý	k2eAgFnPc2d1	jednoduchá
organických	organický	k2eAgFnPc2d1	organická
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Gravimetrická	gravimetrický	k2eAgNnPc1d1	gravimetrické
data	datum	k1gNnPc1	datum
z	z	k7c2	z
prosince	prosinec	k1gInSc2	prosinec
2010	[number]	k4	2010
pořízená	pořízený	k2eAgFnSc1d1	pořízená
během	během	k7c2	během
průletu	průlet	k1gInSc2	průlet
sondy	sonda	k1gFnSc2	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
nad	nad	k7c7	nad
Enceladem	Encelad	k1gInSc7	Encelad
ukázala	ukázat	k5eAaPmAgFnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
zmrzlým	zmrzlý	k2eAgInSc7d1	zmrzlý
povrchem	povrch	k1gInSc7	povrch
měsíce	měsíc	k1gInSc2	měsíc
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nachází	nacházet	k5eAaImIp3nS	nacházet
oceán	oceán	k1gInSc1	oceán
kapalné	kapalný	k2eAgFnSc2d1	kapalná
vody	voda	k1gFnSc2	voda
situovaný	situovaný	k2eAgInSc1d1	situovaný
pod	pod	k7c7	pod
oblastí	oblast	k1gFnSc7	oblast
jižního	jižní	k2eAgInSc2d1	jižní
pólu	pól	k1gInSc2	pól
<g/>
.	.	kIx.	.
</s>
<s>
Oceán	oceán	k1gInSc1	oceán
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
nacházet	nacházet	k5eAaImF	nacházet
nejspíše	nejspíše	k9	nejspíše
pod	pod	k7c7	pod
30	[number]	k4	30
až	až	k9	až
40	[number]	k4	40
kilometry	kilometr	k1gInPc4	kilometr
ledu	led	k1gInSc2	led
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
mocnost	mocnost	k1gFnSc1	mocnost
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
minimálně	minimálně	k6eAd1	minimálně
10	[number]	k4	10
kilometrů	kilometr	k1gInPc2	kilometr
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
jižního	jižní	k2eAgInSc2d1	jižní
pólu	pól	k1gInSc2	pól
<g/>
.	.	kIx.	.
</s>
<s>
Měření	měření	k1gNnSc1	měření
librace	librace	k1gFnSc2	librace
Enceladu	Encelad	k1gInSc2	Encelad
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
celá	celý	k2eAgFnSc1d1	celá
ledová	ledový	k2eAgFnSc1d1	ledová
kůra	kůra	k1gFnSc1	kůra
měsíce	měsíc	k1gInSc2	měsíc
oddělena	oddělit	k5eAaPmNgFnS	oddělit
od	od	k7c2	od
kamenitého	kamenitý	k2eAgNnSc2d1	kamenité
jádra	jádro	k1gNnSc2	jádro
měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
napovídá	napovídat	k5eAaBmIp3nS	napovídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
podpovrchové	podpovrchový	k2eAgNnSc1d1	podpovrchové
těleso	těleso	k1gNnSc1	těleso
kapalné	kapalný	k2eAgFnSc2d1	kapalná
vody	voda	k1gFnSc2	voda
má	mít	k5eAaImIp3nS	mít
globální	globální	k2eAgInSc4d1	globální
charakter	charakter	k1gInSc4	charakter
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
amount	amount	k1gInSc1	amount
of	of	k?	of
libration	libration	k1gInSc1	libration
(	(	kIx(	(
<g/>
0.120	[number]	k4	0.120
<g/>
°	°	k?	°
±	±	k?	±
0.014	[number]	k4	0.014
<g/>
°	°	k?	°
<g/>
)	)	kIx)	)
implies	implies	k1gMnSc1	implies
that	that	k1gMnSc1	that
this	this	k6eAd1	this
global	globat	k5eAaImAgMnS	globat
ocean	ocean	k1gMnSc1	ocean
is	is	k?	is
about	about	k1gMnSc1	about
26	[number]	k4	26
to	ten	k3xDgNnSc1	ten
31	[number]	k4	31
kilometers	kilometers	k1gInSc1	kilometers
deep	deep	k1gInSc4	deep
<g/>
.	.	kIx.	.
</s>
<s>
For	forum	k1gNnPc2	forum
comparison	comparisona	k1gFnPc2	comparisona
<g/>
,	,	kIx,	,
Earth	Eartha	k1gFnPc2	Eartha
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
ocean	ocean	k1gInSc1	ocean
has	hasit	k5eAaImRp2nS	hasit
an	an	k?	an
average	average	k1gNnPc2	average
depth	depth	k1gInSc1	depth
of	of	k?	of
3.7	[number]	k4	3.7
kilometers	kilometersa	k1gFnPc2	kilometersa
<g/>
.	.	kIx.	.
</s>
<s>
Odhadujeme	odhadovat	k5eAaImIp1nP	odhadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
oceán	oceán	k1gInSc1	oceán
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
slanou	slaný	k2eAgFnSc7d1	slaná
vodou	voda	k1gFnSc7	voda
(	(	kIx(	(
<g/>
-Na	-Na	k?	-Na
<g/>
,	,	kIx,	,
-Cl	-Cl	k?	-Cl
<g/>
,	,	kIx,	,
-CO	-CO	k?	-CO
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
a	a	k8xC	a
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
výrazně	výrazně	k6eAd1	výrazně
zásaditý	zásaditý	k2eAgInSc1d1	zásaditý
(	(	kIx(	(
<g/>
hodnota	hodnota	k1gFnSc1	hodnota
pH	ph	kA	ph
11	[number]	k4	11
až	až	k9	až
12	[number]	k4	12
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
hodnota	hodnota	k1gFnSc1	hodnota
pH	ph	kA	ph
je	být	k5eAaImIp3nS	být
vysvětlována	vysvětlován	k2eAgFnSc1d1	vysvětlována
jako	jako	k8xS	jako
důsledek	důsledek	k1gInSc1	důsledek
serpentinizace	serpentinizace	k1gFnSc2	serpentinizace
chondritických	chondritický	k2eAgFnPc2d1	chondritický
hornin	hornina	k1gFnPc2	hornina
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vede	vést	k5eAaImIp3nS	vést
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
molekul	molekula	k1gFnPc2	molekula
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Významného	významný	k2eAgInSc2d1	významný
geochemického	geochemický	k2eAgInSc2d1	geochemický
zdroje	zdroj	k1gInSc2	zdroj
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
podporovat	podporovat	k5eAaImF	podporovat
jak	jak	k6eAd1	jak
abiotickou	abiotický	k2eAgFnSc4d1	abiotická
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
biologickou	biologický	k2eAgFnSc4d1	biologická
syntézu	syntéza	k1gFnSc4	syntéza
organických	organický	k2eAgFnPc2d1	organická
molekul	molekula	k1gFnPc2	molekula
shodných	shodný	k2eAgFnPc2d1	shodná
s	s	k7c7	s
těmi	ten	k3xDgMnPc7	ten
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
byly	být	k5eAaImAgFnP	být
detekovány	detekovat	k5eAaImNgFnP	detekovat
v	v	k7c6	v
mračnech	mračno	k1gNnPc6	mračno
vyvrhovaných	vyvrhovaný	k2eAgFnPc2d1	vyvrhovaná
gejzíry	gejzír	k1gInPc7	gejzír
nad	nad	k7c4	nad
povrch	povrch	k1gInSc4	povrch
Enceladu	Encelad	k1gInSc2	Encelad
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Kryovulkanismus	Kryovulkanismus	k1gInSc1	Kryovulkanismus
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
průletu	průlet	k1gInSc6	průlet
sond	sonda	k1gFnPc2	sonda
Voyager	Voyager	k1gInSc4	Voyager
okolo	okolo	k7c2	okolo
Enceladu	Encelad	k1gInSc2	Encelad
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
začali	začít	k5eAaPmAgMnP	začít
vědci	vědec	k1gMnPc1	vědec
domnívat	domnívat	k5eAaImF	domnívat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
mladého	mladý	k2eAgInSc2d1	mladý
vzhledu	vzhled	k1gInSc2	vzhled
povrchu	povrch	k1gInSc2	povrch
měsíce	měsíc	k1gInPc4	měsíc
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc4	jeho
vysoké	vysoký	k2eAgFnPc4d1	vysoká
odrazivosti	odrazivost	k1gFnPc4	odrazivost
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mohlo	moct	k5eAaImAgNnS	moct
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
geologicky	geologicky	k6eAd1	geologicky
aktivní	aktivní	k2eAgNnSc4d1	aktivní
těleso	těleso	k1gNnSc4	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
si	se	k3xPyFc3	se
povšimli	povšimnout	k5eAaPmAgMnP	povšimnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
měsíc	měsíc	k1gInSc1	měsíc
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c4	v
nejužší	úzký	k2eAgNnSc4d3	nejužší
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
částice	částice	k1gFnPc4	částice
nejbohatší	bohatý	k2eAgFnSc2d3	nejbohatší
části	část	k1gFnSc2	část
prstence	prstenec	k1gInSc2	prstenec
E	E	kA	E
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	on	k3xPp3gMnPc4	on
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
úvaze	úvaha	k1gFnSc3	úvaha
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
měsíc	měsíc	k1gInSc1	měsíc
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
zdrojem	zdroj	k1gInSc7	zdroj
tohoto	tento	k3xDgInSc2	tento
materiálu	materiál	k1gInSc2	materiál
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
vyvrhování	vyvrhování	k1gNnSc2	vyvrhování
vodní	vodní	k2eAgFnSc2d1	vodní
páry	pára	k1gFnSc2	pára
do	do	k7c2	do
vesmírného	vesmírný	k2eAgInSc2d1	vesmírný
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
předpoklad	předpoklad	k1gInSc1	předpoklad
se	se	k3xPyFc4	se
potvrdil	potvrdit	k5eAaPmAgInS	potvrdit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
dat	datum	k1gNnPc2	datum
získaných	získaný	k2eAgInPc2d1	získaný
sondou	sonda	k1gFnSc7	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
<g/>
,	,	kIx,	,
když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
snímcích	snímek	k1gInPc6	snímek
kamery	kamera	k1gFnSc2	kamera
ISS	ISS	kA	ISS
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
a	a	k8xC	a
únoru	únor	k1gInSc6	únor
2005	[number]	k4	2005
zachycena	zachycen	k2eAgFnSc1d1	zachycena
mračna	mračna	k1gFnSc1	mračna
směsi	směs	k1gFnSc2	směs
ledu	led	k1gInSc2	led
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
látek	látka	k1gFnPc2	látka
aktivního	aktivní	k2eAgInSc2d1	aktivní
kryovulkanismu	kryovulkanismus	k1gInSc2	kryovulkanismus
<g/>
,	,	kIx,	,
zvláštní	zvláštní	k2eAgFnSc2d1	zvláštní
formy	forma	k1gFnSc2	forma
sopečné	sopečný	k2eAgFnSc2d1	sopečná
činnosti	činnost	k1gFnSc2	činnost
<g/>
,	,	kIx,	,
během	během	k7c2	během
které	který	k3yIgFnSc2	který
není	být	k5eNaImIp3nS	být
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
vyvrhované	vyvrhovaný	k2eAgNnSc4d1	vyvrhované
magma	magma	k1gNnSc4	magma
tvořené	tvořený	k2eAgInPc4d1	tvořený
silikáty	silikát	k1gInPc4	silikát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
směs	směs	k1gFnSc1	směs
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
různých	různý	k2eAgFnPc2d1	různá
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
prvního	první	k4xOgNnSc2	první
pozorování	pozorování	k1gNnSc2	pozorování
mračen	mračna	k1gFnPc2	mračna
vyvstala	vyvstat	k5eAaPmAgFnS	vyvstat
otázka	otázka	k1gFnSc1	otázka
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
se	se	k3xPyFc4	se
nejedná	jednat	k5eNaImIp3nS	jednat
o	o	k7c4	o
artefakt	artefakt	k1gInSc4	artefakt
vzniklý	vzniklý	k2eAgInSc4d1	vzniklý
během	během	k7c2	během
pořízení	pořízení	k1gNnPc2	pořízení
snímku	snímek	k1gInSc2	snímek
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
na	na	k7c6	na
základě	základ	k1gInSc6	základ
údajů	údaj	k1gInPc2	údaj
z	z	k7c2	z
magnetometru	magnetometr	k1gInSc2	magnetometr
z	z	k7c2	z
února	únor	k1gInSc2	únor
2005	[number]	k4	2005
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mračna	mračno	k1gNnPc4	mračno
představují	představovat	k5eAaImIp3nP	představovat
skutečné	skutečný	k2eAgInPc1d1	skutečný
objekty	objekt	k1gInPc1	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Magnetometr	magnetometr	k1gInSc1	magnetometr
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
nárůst	nárůst	k1gInSc4	nárůst
intenzity	intenzita	k1gFnSc2	intenzita
iontových	iontový	k2eAgFnPc2d1	iontová
cyklotronových	cyklotronový	k2eAgFnPc2d1	cyklotronový
vln	vlna	k1gFnPc2	vlna
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Enceladu	Encelad	k1gInSc2	Encelad
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
vlny	vlna	k1gFnPc1	vlna
jsou	být	k5eAaImIp3nP	být
produkovány	produkovat	k5eAaImNgFnP	produkovat
vlivem	vlivem	k7c2	vlivem
interakce	interakce	k1gFnSc2	interakce
ionizovaných	ionizovaný	k2eAgFnPc2d1	ionizovaná
částic	částice	k1gFnPc2	částice
s	s	k7c7	s
magnetickým	magnetický	k2eAgNnSc7d1	magnetické
polem	pole	k1gNnSc7	pole
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc7	jejich
frekvence	frekvence	k1gFnSc1	frekvence
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
použita	použít	k5eAaPmNgFnS	použít
k	k	k7c3	k
určení	určení	k1gNnSc3	určení
chemického	chemický	k2eAgNnSc2d1	chemické
složení	složení	k1gNnSc2	složení
ionizovaného	ionizovaný	k2eAgInSc2d1	ionizovaný
plynu	plyn	k1gInSc2	plyn
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
vodní	vodní	k2eAgInPc1d1	vodní
páry	pár	k1gInPc1	pár
<g/>
.	.	kIx.	.
<g/>
>	>	kIx)	>
Následující	následující	k2eAgInPc1d1	následující
průlety	průlet	k1gInPc1	průlet
pak	pak	k6eAd1	pak
umožnily	umožnit	k5eAaPmAgInP	umožnit
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
hustota	hustota	k1gFnSc1	hustota
atmosféry	atmosféra	k1gFnSc2	atmosféra
Enceladu	Encelad	k1gInSc2	Encelad
klesá	klesat	k5eAaImIp3nS	klesat
se	s	k7c7	s
vzdáleností	vzdálenost	k1gFnSc7	vzdálenost
od	od	k7c2	od
jižního	jižní	k2eAgInSc2d1	jižní
pólu	pól	k1gInSc2	pól
<g/>
.	.	kIx.	.
</s>
<s>
Shodou	shoda	k1gFnSc7	shoda
náhod	náhoda	k1gFnPc2	náhoda
se	se	k3xPyFc4	se
sondě	sonda	k1gFnSc3	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
podařilo	podařit	k5eAaPmAgNnS	podařit
14	[number]	k4	14
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2005	[number]	k4	2005
proletět	proletět	k5eAaPmF	proletět
skrze	skrze	k?	skrze
mračno	mračno	k1gNnSc4	mračno
vyvržených	vyvržený	k2eAgInPc2d1	vyvržený
plynů	plyn	k1gInPc2	plyn
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
řadě	řada	k1gFnSc6	řada
instrumentů	instrument	k1gInPc2	instrument
provést	provést	k5eAaPmF	provést
detailní	detailní	k2eAgNnSc4d1	detailní
měření	měření	k1gNnSc4	měření
jeho	on	k3xPp3gNnSc2	on
složení	složení	k1gNnSc2	složení
<g/>
.	.	kIx.	.
</s>
<s>
Zjistili	zjistit	k5eAaPmAgMnP	zjistit
jsme	být	k5eAaImIp1nP	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
mračno	mračno	k1gNnSc1	mračno
tvoří	tvořit	k5eAaImIp3nS	tvořit
hlavně	hlavně	k9	hlavně
vodní	vodní	k2eAgFnSc1d1	vodní
pára	pára	k1gFnSc1	pára
společně	společně	k6eAd1	společně
s	s	k7c7	s
dusíkem	dusík	k1gInSc7	dusík
<g/>
,	,	kIx,	,
metanem	metan	k1gInSc7	metan
a	a	k8xC	a
oxidem	oxid	k1gInSc7	oxid
uhličitým	uhličitý	k2eAgInSc7d1	uhličitý
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
potvrdit	potvrdit	k5eAaPmF	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
koncentrace	koncentrace	k1gFnSc1	koncentrace
částic	částice	k1gFnPc2	částice
významně	významně	k6eAd1	významně
narůstá	narůstat	k5eAaImIp3nS	narůstat
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
Enceladu	Encelad	k1gInSc3	Encelad
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
potvrdilo	potvrdit	k5eAaPmAgNnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
měsíc	měsíc	k1gInSc1	měsíc
je	být	k5eAaImIp3nS	být
zdrojem	zdroj	k1gInSc7	zdroj
těchto	tento	k3xDgFnPc2	tento
částic	částice	k1gFnPc2	částice
tvořících	tvořící	k2eAgFnPc2d1	tvořící
prstenec	prstenec	k1gInSc1	prstenec
E.	E.	kA	E.
Analýza	analýza	k1gFnSc1	analýza
dat	datum	k1gNnPc2	datum
následně	následně	k6eAd1	následně
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mračno	mračno	k1gNnSc1	mračno
tvořené	tvořený	k2eAgNnSc1d1	tvořené
hlavně	hlavně	k6eAd1	hlavně
vodní	vodní	k2eAgFnSc7d1	vodní
párou	pára	k1gFnSc7	pára
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnPc3	který
sonda	sonda	k1gFnSc1	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
proletěla	proletět	k5eAaPmAgFnS	proletět
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
výsledkem	výsledek	k1gInSc7	výsledek
povrchových	povrchový	k2eAgInPc2d1	povrchový
projevů	projev	k1gInPc2	projev
kryovulkanismu	kryovulkanismus	k1gInSc2	kryovulkanismus
odehrávajícího	odehrávající	k2eAgInSc2d1	odehrávající
se	se	k3xPyFc4	se
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
jižního	jižní	k2eAgInSc2d1	jižní
pólu	pól	k1gInSc2	pól
<g/>
.	.	kIx.	.
</s>
<s>
Přímé	přímý	k2eAgNnSc1d1	přímé
pozorování	pozorování	k1gNnSc1	pozorování
gejzírů	gejzír	k1gInPc2	gejzír
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yQgFnPc2	který
se	se	k3xPyFc4	se
do	do	k7c2	do
okolí	okolí	k1gNnSc2	okolí
dostávají	dostávat	k5eAaImIp3nP	dostávat
nad	nad	k7c7	nad
jižním	jižní	k2eAgInSc7d1	jižní
pólem	pól	k1gInSc7	pól
mračna	mračno	k1gNnSc2	mračno
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
sondě	sonda	k1gFnSc3	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
podařilo	podařit	k5eAaPmAgNnS	podařit
uskutečnit	uskutečnit	k5eAaPmF	uskutečnit
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Nicméně	nicméně	k8xC	nicméně
samotná	samotný	k2eAgFnSc1d1	samotná
mračna	mračna	k1gFnSc1	mračna
byla	být	k5eAaImAgFnS	být
zachycena	zachytit	k5eAaPmNgFnS	zachytit
na	na	k7c6	na
fotografiích	fotografia	k1gFnPc6	fotografia
již	již	k6eAd1	již
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
a	a	k8xC	a
únoru	únor	k1gInSc6	únor
tohoto	tento	k3xDgInSc2	tento
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
ale	ale	k9	ale
potřeba	potřeba	k6eAd1	potřeba
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
se	se	k3xPyFc4	se
nejedná	jednat	k5eNaImIp3nS	jednat
o	o	k7c4	o
umělý	umělý	k2eAgInSc4d1	umělý
jev	jev	k1gInSc4	jev
způsobený	způsobený	k2eAgInSc4d1	způsobený
odleskem	odlesk	k1gInSc7	odlesk
slunce	slunce	k1gNnSc1	slunce
na	na	k7c6	na
čočce	čočka	k1gFnSc6	čočka
kamery	kamera	k1gFnSc2	kamera
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
listopadových	listopadový	k2eAgFnPc6d1	listopadová
fotografiích	fotografia	k1gFnPc6	fotografia
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
podařilo	podařit	k5eAaPmAgNnS	podařit
rozpoznat	rozpoznat	k5eAaPmF	rozpoznat
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
výtrysky	výtrysk	k1gInPc4	výtrysk
materiálu	materiál	k1gInSc2	materiál
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
pocházejících	pocházející	k2eAgMnPc2d1	pocházející
z	z	k7c2	z
několika	několik	k4yIc2	několik
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
i	i	k9	i
určit	určit	k5eAaPmF	určit
<g/>
,	,	kIx,	,
že	že	k8xS	že
částice	částice	k1gFnPc1	částice
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
střední	střední	k2eAgFnSc7d1	střední
rychlostí	rychlost	k1gFnSc7	rychlost
1,25	[number]	k4	1,25
<g/>
±	±	k?	±
<g/>
0,1	[number]	k4	0,1
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Další	další	k2eAgInPc4d1	další
poznatky	poznatek	k1gInPc4	poznatek
přinesl	přinést	k5eAaPmAgInS	přinést
průlet	průlet	k1gInSc1	průlet
z	z	k7c2	z
12	[number]	k4	12
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
odhalil	odhalit	k5eAaPmAgMnS	odhalit
chemické	chemický	k2eAgNnSc4d1	chemické
složení	složení	k1gNnSc4	složení
mračen	mračno	k1gNnPc2	mračno
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
mračnu	mračno	k1gNnSc6	mračno
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
jednoduché	jednoduchý	k2eAgInPc1d1	jednoduchý
uhlovodíky	uhlovodík	k1gInPc1	uhlovodík
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
metan	metan	k1gInSc1	metan
<g/>
,	,	kIx,	,
propan	propan	k1gInSc1	propan
<g/>
,	,	kIx,	,
acetylén	acetylén	k1gInSc1	acetylén
a	a	k8xC	a
formaldehyd	formaldehyd	k1gInSc1	formaldehyd
<g/>
.	.	kIx.	.
</s>
<s>
Složení	složení	k1gNnSc1	složení
mračen	mračno	k1gNnPc2	mračno
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
velice	velice	k6eAd1	velice
podobalo	podobat	k5eAaImAgNnS	podobat
chemickému	chemický	k2eAgNnSc3d1	chemické
složení	složení	k1gNnSc3	složení
většiny	většina	k1gFnSc2	většina
pozorovaných	pozorovaný	k2eAgFnPc2d1	pozorovaná
komet	kometa	k1gFnPc2	kometa
<g/>
.	.	kIx.	.
</s>
<s>
Kombinovaná	kombinovaný	k2eAgFnSc1d1	kombinovaná
analýza	analýza	k1gFnSc1	analýza
pořízených	pořízený	k2eAgInPc2d1	pořízený
snímků	snímek	k1gInPc2	snímek
<g/>
,	,	kIx,	,
hmotnostní	hmotnostní	k2eAgFnSc2d1	hmotnostní
spektrometrie	spektrometrie	k1gFnSc2	spektrometrie
a	a	k8xC	a
magnetosférických	magnetosférický	k2eAgNnPc2d1	magnetosférický
dat	datum	k1gNnPc2	datum
naznačila	naznačit	k5eAaPmAgFnS	naznačit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mračna	mračno	k1gNnPc1	mračno
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
z	z	k7c2	z
natlakovaných	natlakovaný	k2eAgFnPc2d1	natlakovaná
podpovrchových	podpovrchový	k2eAgFnPc2d1	podpovrchová
prostor	prostora	k1gFnPc2	prostora
podobající	podobající	k2eAgNnPc4d1	podobající
se	se	k3xPyFc4	se
prostorám	prostora	k1gFnPc3	prostora
pod	pod	k7c7	pod
pozemskými	pozemský	k2eAgInPc7d1	pozemský
gejzíry	gejzír	k1gInPc7	gejzír
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mechanismus	mechanismus	k1gInSc1	mechanismus
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
pohání	pohánět	k5eAaImIp3nS	pohánět
a	a	k8xC	a
udržuje	udržovat	k5eAaImIp3nS	udržovat
erupce	erupce	k1gFnSc1	erupce
zůstal	zůstat	k5eAaPmAgInS	zůstat
nejasný	jasný	k2eNgInSc1d1	nejasný
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
síla	síla	k1gFnSc1	síla
erupcí	erupce	k1gFnPc2	erupce
se	se	k3xPyFc4	se
v	v	k7c6	v
čase	čas	k1gInSc6	čas
mění	měnit	k5eAaImIp3nS	měnit
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c4	na
pozici	pozice	k1gFnSc4	pozice
Enceladu	Encelad	k1gInSc2	Encelad
během	během	k7c2	během
oběhu	oběh	k1gInSc2	oběh
okolo	okolo	k7c2	okolo
Saturnu	Saturn	k1gInSc2	Saturn
<g/>
.	.	kIx.	.
</s>
<s>
Mračna	mračno	k1gNnPc1	mračno
jsou	být	k5eAaImIp3nP	být
přibližně	přibližně	k6eAd1	přibližně
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
jasnější	jasný	k2eAgFnSc1d2	jasnější
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
Enceladus	Enceladus	k1gInSc4	Enceladus
nejdále	daleko	k6eAd3	daleko
od	od	k7c2	od
Saturnu	Saturn	k1gInSc2	Saturn
než	než	k8xS	než
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
nejblíže	blízce	k6eAd3	blízce
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
chování	chování	k1gNnSc1	chování
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
shodě	shoda	k1gFnSc6	shoda
s	s	k7c7	s
geofyzikálními	geofyzikální	k2eAgInPc7d1	geofyzikální
výpočty	výpočet	k1gInPc7	výpočet
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
praskliny	prasklina	k1gFnPc1	prasklina
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
jižního	jižní	k2eAgInSc2d1	jižní
pólu	pól	k1gInSc2	pól
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
v	v	k7c6	v
kompresi	komprese	k1gFnSc6	komprese
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
se	se	k3xPyFc4	se
uzavírat	uzavírat	k5eAaImF	uzavírat
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
měsíc	měsíc	k1gInSc4	měsíc
nejblíže	blízce	k6eAd3	blízce
k	k	k7c3	k
Saturnu	Saturn	k1gInSc3	Saturn
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
v	v	k7c6	v
periapsidě	periapsida	k1gFnSc6	periapsida
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
v	v	k7c6	v
tenzi	tenze	k1gFnSc6	tenze
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
se	se	k3xPyFc4	se
otevírat	otevírat	k5eAaImF	otevírat
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
Saturnu	Saturn	k1gInSc2	Saturn
nejdále	daleko	k6eAd3	daleko
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
v	v	k7c6	v
apoapsidě	apoapsida	k1gFnSc6	apoapsida
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
materiálu	materiál	k1gInSc2	materiál
mračen	mračna	k1gFnPc2	mračna
je	být	k5eAaImIp3nS	být
vyvrhována	vyvrhovat	k5eAaImNgFnS	vyvrhovat
během	během	k7c2	během
erupcí	erupce	k1gFnPc2	erupce
probíhající	probíhající	k2eAgMnSc1d1	probíhající
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
části	část	k1gFnSc6	část
rozsáhlých	rozsáhlý	k2eAgFnPc2d1	rozsáhlá
trhlin	trhlina	k1gFnPc2	trhlina
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
tedy	tedy	k9	tedy
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
izolovanými	izolovaný	k2eAgInPc7d1	izolovaný
gejzíry	gejzír	k1gInPc7	gejzír
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mohlo	moct	k5eAaImAgNnS	moct
z	z	k7c2	z
fotografií	fotografia	k1gFnPc2	fotografia
zdát	zdát	k5eAaImF	zdát
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
snímcích	snímek	k1gInPc6	snímek
jsme	být	k5eAaImIp1nP	být
totiž	totiž	k9	totiž
svědky	svědek	k1gMnPc4	svědek
optického	optický	k2eAgInSc2d1	optický
klamu	klam	k1gInSc2	klam
způsobeným	způsobený	k2eAgInSc7d1	způsobený
směrem	směr	k1gInSc7	směr
pozorování	pozorování	k1gNnPc2	pozorování
a	a	k8xC	a
lokální	lokální	k2eAgFnSc7d1	lokální
geometrií	geometrie	k1gFnSc7	geometrie
prasklin	prasklina	k1gFnPc2	prasklina
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
průletu	průlet	k1gInSc2	průlet
uskutečněného	uskutečněný	k2eAgInSc2d1	uskutečněný
14	[number]	k4	14
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2005	[number]	k4	2005
zjistila	zjistit	k5eAaPmAgFnS	zjistit
sonda	sonda	k1gFnSc1	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
infračerveného	infračervený	k2eAgInSc2d1	infračervený
spektrometru	spektrometr	k1gInSc2	spektrometr
<g/>
,	,	kIx,	,
že	že	k8xS	že
okolí	okolí	k1gNnSc1	okolí
jižního	jižní	k2eAgInSc2d1	jižní
pólu	pól	k1gInSc2	pól
je	být	k5eAaImIp3nS	být
abnormálně	abnormálně	k6eAd1	abnormálně
teplé	teplý	k2eAgNnSc1d1	teplé
<g/>
.	.	kIx.	.
</s>
<s>
Teploty	teplota	k1gFnPc1	teplota
zde	zde	k6eAd1	zde
většinou	většinou	k6eAd1	většinou
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
mezi	mezi	k7c7	mezi
85	[number]	k4	85
až	až	k9	až
90	[number]	k4	90
K	K	kA	K
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
několik	několik	k4yIc4	několik
prostorově	prostorově	k6eAd1	prostorově
malých	malý	k2eAgFnPc2d1	malá
oblastí	oblast	k1gFnPc2	oblast
má	mít	k5eAaImIp3nS	mít
teplotu	teplota	k1gFnSc4	teplota
až	až	k9	až
157	[number]	k4	157
K.	K.	kA	K.
Příliš	příliš	k6eAd1	příliš
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
výsledek	výsledek	k1gInSc4	výsledek
zahřátí	zahřátí	k1gNnSc4	zahřátí
povrchu	povrch	k1gInSc2	povrch
Sluncem	slunce	k1gNnSc7	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
vysoké	vysoký	k2eAgFnPc1d1	vysoká
teploty	teplota	k1gFnPc1	teplota
tak	tak	k6eAd1	tak
naznačily	naznačit	k5eAaPmAgFnP	naznačit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
povrch	povrch	k1gInSc4	povrch
ohříván	ohříván	k2eAgInSc4d1	ohříván
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
procesů	proces	k1gInPc2	proces
odehrávajících	odehrávající	k2eAgInPc2d1	odehrávající
se	se	k3xPyFc4	se
uvnitř	uvnitř	k7c2	uvnitř
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
panuje	panovat	k5eAaImIp3nS	panovat
obecná	obecný	k2eAgFnSc1d1	obecná
shoda	shoda	k1gFnSc1	shoda
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
nachází	nacházet	k5eAaImIp3nS	nacházet
kapalný	kapalný	k2eAgInSc1d1	kapalný
oceán	oceán	k1gInSc1	oceán
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
ani	ani	k8xC	ani
jeho	jeho	k3xOp3gFnSc1	jeho
přítomnost	přítomnost	k1gFnSc1	přítomnost
nevysvětluje	vysvětlovat	k5eNaImIp3nS	vysvětlovat
zdroj	zdroj	k1gInSc4	zdroj
tepla	teplo	k1gNnSc2	teplo
s	s	k7c7	s
tepelným	tepelný	k2eAgInSc7d1	tepelný
tokem	tok	k1gInSc7	tok
okolo	okolo	k7c2	okolo
200	[number]	k4	200
mW	mW	k?	mW
<g/>
/	/	kIx~	/
<g/>
m	m	kA	m
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
10	[number]	k4	10
krát	krát	k6eAd1	krát
více	hodně	k6eAd2	hodně
tepla	teplo	k1gNnSc2	teplo
<g/>
,	,	kIx,	,
než	než	k8xS	než
jsou	být	k5eAaImIp3nP	být
schopny	schopen	k2eAgInPc1d1	schopen
poskytnout	poskytnout	k5eAaPmF	poskytnout
radioaktivní	radioaktivní	k2eAgInPc4d1	radioaktivní
prvky	prvek	k1gInPc4	prvek
svým	svůj	k3xOyFgInSc7	svůj
přirozeným	přirozený	k2eAgInSc7d1	přirozený
rozpadem	rozpad	k1gInSc7	rozpad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
let	léto	k1gNnPc2	léto
bylo	být	k5eAaImAgNnS	být
navrženo	navrhnout	k5eAaPmNgNnS	navrhnout
několik	několik	k4yIc1	několik
vysvětlení	vysvětlení	k1gNnPc2	vysvětlení
pro	pro	k7c4	pro
pozorované	pozorovaný	k2eAgFnPc4d1	pozorovaná
teplotní	teplotní	k2eAgFnPc4d1	teplotní
anomálie	anomálie	k1gFnPc4	anomálie
a	a	k8xC	a
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
spojenými	spojený	k2eAgInPc7d1	spojený
gejzíry	gejzír	k1gInPc7	gejzír
a	a	k8xC	a
mračny	mračno	k1gNnPc7	mračno
<g/>
,	,	kIx,	,
např.	např.	kA	např.
série	série	k1gFnSc1	série
prasklin	prasklina	k1gFnPc2	prasklina
propojující	propojující	k2eAgInSc1d1	propojující
podpovrchový	podpovrchový	k2eAgInSc1d1	podpovrchový
oceán	oceán	k1gInSc1	oceán
kapalné	kapalný	k2eAgFnSc2d1	kapalná
vody	voda	k1gFnSc2	voda
s	s	k7c7	s
povrchem	povrch	k1gInSc7	povrch
<g/>
,	,	kIx,	,
sublimace	sublimace	k1gFnSc1	sublimace
ledu	led	k1gInSc2	led
dekomprese	dekomprese	k1gFnSc1	dekomprese
a	a	k8xC	a
disociace	disociace	k1gFnSc1	disociace
metan	metan	k1gInSc1	metan
hydrátu	hydrát	k1gInSc2	hydrát
<g/>
,	,	kIx,	,
střižné	střižný	k2eAgNnSc1d1	střižné
zahřívání	zahřívání	k1gNnSc1	zahřívání
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
vědecká	vědecký	k2eAgFnSc1d1	vědecká
diskuse	diskuse	k1gFnSc1	diskuse
o	o	k7c6	o
možných	možný	k2eAgInPc6d1	možný
zdrojích	zdroj	k1gInPc6	zdroj
tepla	teplo	k1gNnSc2	teplo
Enceladu	Encelad	k1gInSc2	Encelad
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
běhu	běh	k1gInSc6	běh
<g/>
.	.	kIx.	.
</s>
<s>
Zahřívání	zahřívání	k1gNnSc1	zahřívání
Enceladu	Encelad	k1gInSc2	Encelad
probíhalo	probíhat	k5eAaImAgNnS	probíhat
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
času	čas	k1gInSc2	čas
různými	různý	k2eAgInPc7d1	různý
mechanismy	mechanismus	k1gInPc7	mechanismus
<g/>
.	.	kIx.	.
</s>
<s>
Radioaktivní	radioaktivní	k2eAgInSc1d1	radioaktivní
rozpad	rozpad	k1gInSc1	rozpad
některých	některý	k3yIgInPc2	některý
prvků	prvek	k1gInPc2	prvek
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
jádře	jádro	k1gNnSc6	jádro
mohl	moct	k5eAaImAgMnS	moct
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
tento	tento	k3xDgInSc4	tento
měsíc	měsíc	k1gInSc4	měsíc
zahřát	zahřát	k5eAaPmNgMnS	zahřát
<g/>
.	.	kIx.	.
vlivem	vlivem	k7c2	vlivem
čehož	což	k3yRnSc2	což
mohlo	moct	k5eAaImAgNnS	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
teplé	teplý	k2eAgNnSc1d1	teplé
jádro	jádro	k1gNnSc1	jádro
a	a	k8xC	a
podpovrchový	podpovrchový	k2eAgInSc1d1	podpovrchový
oceán	oceán	k1gInSc1	oceán
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
udržován	udržovat	k5eAaImNgInS	udržovat
před	před	k7c7	před
zamrznutím	zamrznutí	k1gNnSc7	zamrznutí
neznámým	známý	k2eNgInSc7d1	neznámý
mechanismem	mechanismus	k1gInSc7	mechanismus
<g/>
.	.	kIx.	.
</s>
<s>
Geofyzikální	geofyzikální	k2eAgInPc1d1	geofyzikální
modely	model	k1gInPc1	model
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
slapové	slapový	k2eAgNnSc4d1	slapové
zahřívání	zahřívání	k1gNnSc4	zahřívání
představuje	představovat	k5eAaImIp3nS	představovat
hlavní	hlavní	k2eAgInSc1d1	hlavní
současný	současný	k2eAgInSc1d1	současný
zdroj	zdroj	k1gInSc1	zdroj
tepla	teplo	k1gNnSc2	teplo
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
ještě	ještě	k6eAd1	ještě
dodatečně	dodatečně	k6eAd1	dodatečně
podporován	podporovat	k5eAaImNgInS	podporovat
radioaktivním	radioaktivní	k2eAgInSc7d1	radioaktivní
rozpadem	rozpad	k1gInSc7	rozpad
některých	některý	k3yIgInPc2	některý
prvků	prvek	k1gInPc2	prvek
a	a	k8xC	a
nějakou	nějaký	k3yIgFnSc4	nějaký
nám	my	k3xPp1nPc3	my
zatím	zatím	k6eAd1	zatím
neznámou	známý	k2eNgFnSc7d1	neznámá
chemickou	chemický	k2eAgFnSc7d1	chemická
reakcí	reakce	k1gFnSc7	reakce
produkující	produkující	k2eAgNnSc4d1	produkující
teplo	teplo	k1gNnSc4	teplo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
vyšla	vyjít	k5eAaPmAgFnS	vyjít
studie	studie	k1gFnSc1	studie
zaměřující	zaměřující	k2eAgFnSc1d1	zaměřující
se	se	k3xPyFc4	se
na	na	k7c4	na
vnitřní	vnitřní	k2eAgNnSc4d1	vnitřní
teplo	teplo	k1gNnSc4	teplo
Enceladu	Encelad	k1gInSc2	Encelad
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yQgFnSc2	který
vyplynulo	vyplynout	k5eAaPmAgNnS	vyplynout
<g/>
,	,	kIx,	,
že	že	k8xS	že
množství	množství	k1gNnSc1	množství
tepla	teplo	k1gNnSc2	teplo
produkovaného	produkovaný	k2eAgInSc2d1	produkovaný
slapovým	slapový	k2eAgNnSc7d1	slapové
zahříváním	zahřívání	k1gNnSc7	zahřívání
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
větší	veliký	k2eAgInSc4d2	veliký
než	než	k8xS	než
1,1	[number]	k4	1,1
gigawattu	gigawatt	k1gInSc2	gigawatt
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
pozorováním	pozorování	k1gNnSc7	pozorování
<g/>
.	.	kIx.	.
</s>
<s>
Sonda	sonda	k1gFnSc1	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
totiž	totiž	k8xC	totiž
zjistila	zjistit	k5eAaPmAgFnS	zjistit
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
infračerveného	infračervený	k2eAgInSc2d1	infračervený
spektrometru	spektrometr	k1gInSc2	spektrometr
zkoumající	zkoumající	k2eAgFnPc4d1	zkoumající
po	po	k7c4	po
16	[number]	k4	16
měsíců	měsíc	k1gInPc2	měsíc
oblast	oblast	k1gFnSc1	oblast
jižního	jižní	k2eAgInSc2d1	jižní
pólu	pól	k1gInSc2	pól
<g/>
,	,	kIx,	,
že	že	k8xS	že
množství	množství	k1gNnSc1	množství
tepla	teplo	k1gNnSc2	teplo
produkovaného	produkovaný	k2eAgInSc2d1	produkovaný
uvnitř	uvnitř	k7c2	uvnitř
Enceladu	Encelad	k1gInSc2	Encelad
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
okolo	okolo	k7c2	okolo
4,7	[number]	k4	4,7
gigawattu	gigawatt	k1gInSc2	gigawatt
a	a	k8xC	a
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
měsíc	měsíc	k1gInSc4	měsíc
v	v	k7c6	v
teplotním	teplotní	k2eAgNnSc6d1	teplotní
equilibriu	equilibrium	k1gNnSc6	equilibrium
<g/>
.	.	kIx.	.
</s>
<s>
Pozorovaný	pozorovaný	k2eAgInSc1d1	pozorovaný
teplotní	teplotní	k2eAgInSc1d1	teplotní
výstup	výstup	k1gInSc1	výstup
4,7	[number]	k4	4,7
gigawattů	gigawatta	k1gMnPc2	gigawatta
je	být	k5eAaImIp3nS	být
obtížné	obtížný	k2eAgNnSc1d1	obtížné
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
jen	jen	k9	jen
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
slapového	slapový	k2eAgNnSc2d1	slapové
zahřívání	zahřívání	k1gNnSc2	zahřívání
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
hlavní	hlavní	k2eAgInSc1d1	hlavní
zdroj	zdroj	k1gInSc1	zdroj
tepla	teplo	k1gNnSc2	teplo
Enceladu	Encelad	k1gInSc2	Encelad
tak	tak	k6eAd1	tak
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
záhadou	záhada	k1gFnSc7	záhada
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
vědců	vědec	k1gMnPc2	vědec
se	se	k3xPyFc4	se
domnívá	domnívat	k5eAaImIp3nS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pozorovaný	pozorovaný	k2eAgInSc1d1	pozorovaný
tepelný	tepelný	k2eAgInSc1d1	tepelný
tok	tok	k1gInSc1	tok
není	být	k5eNaImIp3nS	být
dostatečný	dostatečný	k2eAgInSc1d1	dostatečný
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
udržel	udržet	k5eAaPmAgInS	udržet
podpovrchový	podpovrchový	k2eAgInSc1d1	podpovrchový
oceán	oceán	k1gInSc1	oceán
kapalný	kapalný	k2eAgInSc1d1	kapalný
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
jeví	jevit	k5eAaImIp3nS	jevit
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
pozůstatek	pozůstatek	k1gInSc4	pozůstatek
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
měsíc	měsíc	k1gInSc1	měsíc
měl	mít	k5eAaImAgInS	mít
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgFnSc4d2	veliký
excentricitu	excentricita	k1gFnSc4	excentricita
a	a	k8xC	a
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
spojené	spojený	k2eAgNnSc1d1	spojené
větší	veliký	k2eAgNnSc1d2	veliký
slapové	slapový	k2eAgNnSc1d1	slapové
zahřívání	zahřívání	k1gNnSc1	zahřívání
než	než	k8xS	než
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
<s>
Případně	případně	k6eAd1	případně
dříve	dříve	k6eAd2	dříve
musel	muset	k5eAaImAgInS	muset
být	být	k5eAaImF	být
mnohem	mnohem	k6eAd1	mnohem
aktivnější	aktivní	k2eAgInSc4d2	aktivnější
jiný	jiný	k2eAgInSc4d1	jiný
zdroj	zdroj	k1gInSc4	zdroj
produkce	produkce	k1gFnSc2	produkce
tepla	teplo	k1gNnSc2	teplo
<g/>
.	.	kIx.	.
</s>
<s>
Slapové	slapový	k2eAgNnSc4d1	slapové
zahřívání	zahřívání	k1gNnSc4	zahřívání
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
procesy	proces	k1gInPc1	proces
slapového	slapový	k2eAgNnSc2d1	slapové
tření	tření	k1gNnSc2	tření
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
oběžná	oběžný	k2eAgFnSc1d1	oběžná
a	a	k8xC	a
rotační	rotační	k2eAgFnSc1d1	rotační
energie	energie	k1gFnSc1	energie
přeměňována	přeměňován	k2eAgFnSc1d1	přeměňována
v	v	k7c6	v
kůře	kůra	k1gFnSc6	kůra
měsíce	měsíc	k1gInPc4	měsíc
na	na	k7c4	na
teplo	teplo	k1gNnSc4	teplo
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
tepla	teplo	k1gNnSc2	teplo
a	a	k8xC	a
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
může	moct	k5eAaImIp3nS	moct
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
ještě	ještě	k9	ještě
librace	librace	k1gFnPc4	librace
tělesa	těleso	k1gNnSc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
Enceladu	Encelad	k1gInSc2	Encelad
je	být	k5eAaImIp3nS	být
slapové	slapový	k2eAgNnSc4d1	slapové
zahřívání	zahřívání	k1gNnSc4	zahřívání
ledové	ledový	k2eAgFnSc2d1	ledová
kůry	kůra	k1gFnSc2	kůra
tak	tak	k6eAd1	tak
významné	významný	k2eAgNnSc4d1	významné
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc4	tento
měsíc	měsíc	k1gInSc4	měsíc
má	mít	k5eAaImIp3nS	mít
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
oceán	oceán	k1gInSc1	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Vědecké	vědecký	k2eAgInPc1d1	vědecký
modely	model	k1gInPc1	model
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
množství	množství	k1gNnSc1	množství
tepla	teplo	k1gNnSc2	teplo
<g/>
,	,	kIx,	,
které	který	k3yRgFnSc2	který
Enceladus	Enceladus	k1gInSc4	Enceladus
díky	díky	k7c3	díky
slapovému	slapový	k2eAgNnSc3d1	slapové
zahřívání	zahřívání	k1gNnSc3	zahřívání
dostává	dostávat	k5eAaImIp3nS	dostávat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nestačí	stačit	k5eNaBmIp3nS	stačit
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
udržel	udržet	k5eAaPmAgInS	udržet
oceán	oceán	k1gInSc1	oceán
kapalný	kapalný	k2eAgInSc1d1	kapalný
po	po	k7c4	po
více	hodně	k6eAd2	hodně
než	než	k8xS	než
30	[number]	k4	30
miliónů	milión	k4xCgInPc2	milión
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
oceán	oceán	k1gInSc1	oceán
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
vyšší	vysoký	k2eAgNnSc4d2	vyšší
zastoupení	zastoupení	k1gNnSc4	zastoupení
látek	látka	k1gFnPc2	látka
snižující	snižující	k2eAgFnSc2d1	snižující
teplotu	teplota	k1gFnSc4	teplota
tuhnutí	tuhnutí	k1gNnSc2	tuhnutí
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
domníváme	domnívat	k5eAaImIp1nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Enceladus	Enceladus	k1gInSc1	Enceladus
měl	mít	k5eAaImAgInS	mít
dříve	dříve	k6eAd2	dříve
více	hodně	k6eAd2	hodně
excentrickou	excentrický	k2eAgFnSc4d1	excentrická
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
umožňovalo	umožňovat	k5eAaImAgNnS	umožňovat
významnější	významný	k2eAgNnSc1d2	významnější
působení	působení	k1gNnSc1	působení
slapového	slapový	k2eAgNnSc2d1	slapové
zahřívání	zahřívání	k1gNnSc2	zahřívání
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
vyšší	vysoký	k2eAgFnSc4d2	vyšší
produkci	produkce	k1gFnSc4	produkce
tepla	teplo	k1gNnSc2	teplo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
hloubka	hloubka	k1gFnSc1	hloubka
oceánu	oceán	k1gInSc2	oceán
na	na	k7c6	na
Enceladu	Encelad	k1gInSc6	Encelad
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
může	moct	k5eAaImIp3nS	moct
periodicky	periodicky	k6eAd1	periodicky
měnit	měnit	k5eAaImF	měnit
v	v	k7c6	v
čase	čas	k1gInSc6	čas
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
pravidelných	pravidelný	k2eAgFnPc6d1	pravidelná
změnách	změna	k1gFnPc6	změna
excentricity	excentricita	k1gFnSc2	excentricita
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Předchozí	předchozí	k2eAgInPc1d1	předchozí
modely	model	k1gInPc1	model
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
perturbace	perturbace	k1gFnSc1	perturbace
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
způsobovat	způsobovat	k5eAaImF	způsobovat
měsíc	měsíc	k1gInSc4	měsíc
Dione	Dion	k1gMnSc5	Dion
<g/>
.	.	kIx.	.
</s>
<s>
Povrch	povrch	k1gInSc1	povrch
Enceladu	Encelad	k1gInSc2	Encelad
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
celý	celý	k2eAgInSc4d1	celý
měsíc	měsíc	k1gInSc4	měsíc
prošel	projít	k5eAaPmAgMnS	projít
obdobím	období	k1gNnSc7	období
vyššího	vysoký	k2eAgInSc2d2	vyšší
tepelného	tepelný	k2eAgInSc2d1	tepelný
toku	tok	k1gInSc2	tok
<g/>
.	.	kIx.	.
</s>
<s>
Enceladus	Enceladus	k1gMnSc1	Enceladus
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
v	v	k7c6	v
době	doba	k1gFnSc6	doba
svého	svůj	k3xOyFgInSc2	svůj
vzniku	vznik	k1gInSc2	vznik
měl	mít	k5eAaImAgMnS	mít
vyšší	vysoký	k2eAgNnSc4d2	vyšší
zastoupení	zastoupení	k1gNnSc4	zastoupení
radioaktivních	radioaktivní	k2eAgInPc2d1	radioaktivní
prvků	prvek	k1gInPc2	prvek
s	s	k7c7	s
krátkým	krátký	k2eAgInSc7d1	krátký
poločasem	poločas	k1gInSc7	poločas
rozpadu	rozpad	k1gInSc2	rozpad
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
izotop	izotop	k1gInSc1	izotop
hliníku-	hliníku-	k?	hliníku-
<g/>
26	[number]	k4	26
<g/>
,	,	kIx,	,
železa-	železa-	k?	železa-
<g/>
60	[number]	k4	60
nebo	nebo	k8xC	nebo
hořčíku-	hořčíku-	k?	hořčíku-
<g/>
60	[number]	k4	60
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc7	jejich
rozpadem	rozpad	k1gInSc7	rozpad
bylo	být	k5eAaImAgNnS	být
uvolněné	uvolněný	k2eAgNnSc1d1	uvolněné
enormní	enormní	k2eAgNnSc1d1	enormní
množství	množství	k1gNnSc1	množství
tepla	teplo	k1gNnSc2	teplo
v	v	k7c6	v
relativně	relativně	k6eAd1	relativně
krátké	krátký	k2eAgFnSc6d1	krátká
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
během	během	k7c2	během
7	[number]	k4	7
miliónů	milión	k4xCgInPc2	milión
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
vznik	vznik	k1gInSc4	vznik
kamenitého	kamenitý	k2eAgNnSc2d1	kamenité
jádra	jádro	k1gNnSc2	jádro
obklopeného	obklopený	k2eAgInSc2d1	obklopený
ledovým	ledový	k2eAgInSc7d1	ledový
pláštěm	plášť	k1gInSc7	plášť
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
množství	množství	k1gNnSc1	množství
produkovaného	produkovaný	k2eAgNnSc2d1	produkované
tepla	teplo	k1gNnSc2	teplo
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
radioaktivních	radioaktivní	k2eAgInPc2d1	radioaktivní
zdrojů	zdroj	k1gInPc2	zdroj
časem	časem	k6eAd1	časem
významně	významně	k6eAd1	významně
klesalo	klesat	k5eAaImAgNnS	klesat
<g/>
,	,	kIx,	,
kombinace	kombinace	k1gFnSc1	kombinace
radioaktivního	radioaktivní	k2eAgNnSc2d1	radioaktivní
tepla	teplo	k1gNnSc2	teplo
s	s	k7c7	s
teplem	teplo	k1gNnSc7	teplo
vzniklým	vzniklý	k2eAgNnSc7d1	vzniklé
slapovým	slapový	k2eAgNnSc7d1	slapové
zahříváním	zahřívání	k1gNnSc7	zahřívání
mohlo	moct	k5eAaImAgNnS	moct
zabránit	zabránit	k5eAaPmF	zabránit
zamrznutí	zamrznutí	k1gNnSc4	zamrznutí
podpovrchového	podpovrchový	k2eAgInSc2d1	podpovrchový
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
radioaktivní	radioaktivní	k2eAgNnSc4d1	radioaktivní
zahřívání	zahřívání	k1gNnSc4	zahřívání
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
okolo	okolo	k7c2	okolo
3,2	[number]	k4	3,2
×	×	k?	×
1015	[number]	k4	1015
ergs	ergsa	k1gFnPc2	ergsa
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
-	-	kIx~	-
za	za	k7c2	za
předpokladu	předpoklad	k1gInSc2	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
Enceladus	Enceladus	k1gInSc1	Enceladus
je	být	k5eAaImIp3nS	být
složený	složený	k2eAgInSc1d1	složený
z	z	k7c2	z
ledu	led	k1gInSc2	led
<g/>
,	,	kIx,	,
železa	železo	k1gNnSc2	železo
a	a	k8xC	a
křemičitých	křemičitý	k2eAgFnPc2d1	křemičitá
hornin	hornina	k1gFnPc2	hornina
<g/>
.	.	kIx.	.
</s>
<s>
Zahřívání	zahřívání	k1gNnSc1	zahřívání
radioaktivními	radioaktivní	k2eAgInPc7d1	radioaktivní
prvky	prvek	k1gInPc7	prvek
s	s	k7c7	s
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
poločasem	poločas	k1gInSc7	poločas
rozpadu	rozpad	k1gInSc2	rozpad
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
uran-	uran-	k?	uran-
<g/>
238	[number]	k4	238
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
uran-	uran-	k?	uran-
<g/>
235	[number]	k4	235
<g/>
,,	,,	k?	,,
thorium-	thorium-	k?	thorium-
<g/>
232	[number]	k4	232
a	a	k8xC	a
draslík-	draslík-	k?	draslík-
<g/>
40	[number]	k4	40
<g/>
,	,	kIx,	,
produkuje	produkovat	k5eAaImIp3nS	produkovat
uvnitř	uvnitř	k7c2	uvnitř
měsíce	měsíc	k1gInSc2	měsíc
přibližně	přibližně	k6eAd1	přibližně
0,3	[number]	k4	0,3
gigawattů	gigawatt	k1gInPc2	gigawatt
<g/>
.	.	kIx.	.
</s>
<s>
Přítomnost	přítomnost	k1gFnSc1	přítomnost
hlubokého	hluboký	k2eAgInSc2d1	hluboký
oceánu	oceán	k1gInSc2	oceán
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
Enceladu	Encelad	k1gInSc2	Encelad
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tepelný	tepelný	k2eAgInSc1d1	tepelný
tok	tok	k1gInSc1	tok
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
desetkrát	desetkrát	k6eAd1	desetkrát
vyšší	vysoký	k2eAgFnSc1d2	vyšší
<g/>
,	,	kIx,	,
než	než	k8xS	než
může	moct	k5eAaImIp3nS	moct
radioaktivní	radioaktivní	k2eAgInSc1d1	radioaktivní
rozpad	rozpad	k1gInSc1	rozpad
prvků	prvek	k1gInPc2	prvek
uvnitř	uvnitř	k7c2	uvnitř
jádra	jádro	k1gNnSc2	jádro
poskytnout	poskytnout	k5eAaPmF	poskytnout
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
nebyl	být	k5eNaImAgInS	být
zpočátku	zpočátku	k6eAd1	zpočátku
detekován	detekovat	k5eAaImNgInS	detekovat
žádný	žádný	k3yNgInSc1	žádný
čpavek	čpavek	k1gInSc1	čpavek
ve	v	k7c6	v
vyvrhovaných	vyvrhovaný	k2eAgNnPc6d1	vyvrhované
mračnech	mračno	k1gNnPc6	mračno
přístroji	přístroj	k1gInSc3	přístroj
sondy	sonda	k1gFnPc4	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
by	by	kYmCp3nS	by
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
nemrznoucí	mrznoucí	k2eNgFnSc4d1	nemrznoucí
směs	směs	k1gFnSc4	směs
<g/>
,	,	kIx,	,
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
nachází	nacházet	k5eAaImIp3nS	nacházet
prostory	prostora	k1gFnPc4	prostora
vyplněné	vyplněný	k2eAgFnPc4d1	vyplněná
téměř	téměř	k6eAd1	téměř
čistou	čistá	k1gFnSc7	čistá
vodou	voda	k1gFnSc7	voda
o	o	k7c6	o
teplotě	teplota	k1gFnSc6	teplota
minimálně	minimálně	k6eAd1	minimálně
270	[number]	k4	270
K.	K.	kA	K.
Čistá	čistý	k2eAgFnSc1d1	čistá
voda	voda	k1gFnSc1	voda
totiž	totiž	k9	totiž
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
vyšší	vysoký	k2eAgFnSc4d2	vyšší
teplotu	teplota	k1gFnSc4	teplota
k	k	k7c3	k
udržení	udržení	k1gNnSc3	udržení
v	v	k7c6	v
kapalném	kapalný	k2eAgNnSc6d1	kapalné
skupenství	skupenství	k1gNnSc6	skupenství
<g/>
.	.	kIx.	.
</s>
<s>
Stopy	stopa	k1gFnPc1	stopa
čpavku	čpavek	k1gInSc2	čpavek
se	se	k3xPyFc4	se
povedlo	povést	k5eAaPmAgNnS	povést
objevit	objevit	k5eAaPmF	objevit
během	během	k7c2	během
průletu	průlet	k1gInSc2	průlet
sondy	sonda	k1gFnSc2	sonda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
významně	významně	k6eAd1	významně
změnilo	změnit	k5eAaPmAgNnS	změnit
naše	náš	k3xOp1gFnPc4	náš
představy	představa	k1gFnPc4	představa
<g/>
.	.	kIx.	.
</s>
<s>
Přítomnost	přítomnost	k1gFnSc1	přítomnost
čpavku	čpavek	k1gInSc2	čpavek
totiž	totiž	k9	totiž
naznačila	naznačit	k5eAaPmAgFnS	naznačit
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
potřeba	potřeba	k1gFnSc1	potřeba
nižší	nízký	k2eAgFnSc2d2	nižší
teploty	teplota	k1gFnSc2	teplota
k	k	k7c3	k
udržení	udržení	k1gNnSc3	udržení
kapalné	kapalný	k2eAgFnSc2d1	kapalná
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
vzniku	vznik	k1gInSc3	vznik
mračen	mračna	k1gFnPc2	mračna
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
nachází	nacházet	k5eAaImIp3nS	nacházet
vrstva	vrstva	k1gFnSc1	vrstva
směsi	směs	k1gFnSc2	směs
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
čpavku	čpavek	k1gInSc2	čpavek
o	o	k7c6	o
teplotě	teplota	k1gFnSc6	teplota
pouhých	pouhý	k2eAgInPc2d1	pouhý
170	[number]	k4	170
K	K	kA	K
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
by	by	kYmCp3nS	by
znamenalo	znamenat	k5eAaImAgNnS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
mnohem	mnohem	k6eAd1	mnohem
méně	málo	k6eAd2	málo
energie	energie	k1gFnSc1	energie
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
mračen	mračna	k1gFnPc2	mračna
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
tepelný	tepelný	k2eAgInSc1d1	tepelný
tok	tok	k1gInSc1	tok
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
4,7	[number]	k4	4,7
gigawattů	gigawatt	k1gInPc2	gigawatt
je	být	k5eAaImIp3nS	být
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
kryovulkanismu	kryovulkanismus	k1gInSc2	kryovulkanismus
dostatečný	dostatečný	k2eAgMnSc1d1	dostatečný
i	i	k8xC	i
bez	bez	k7c2	bez
přítomnosti	přítomnost	k1gFnSc2	přítomnost
čpavku	čpavek	k1gInSc2	čpavek
<g/>
.	.	kIx.	.
</s>
<s>
Enceladus	Enceladus	k1gInSc1	Enceladus
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
malý	malý	k2eAgInSc4d1	malý
měsíc	měsíc	k1gInSc4	měsíc
tvořený	tvořený	k2eAgInSc4d1	tvořený
ledem	led	k1gInSc7	led
a	a	k8xC	a
kamením	kamení	k1gNnSc7	kamení
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
nepravidelný	pravidelný	k2eNgInSc1d1	nepravidelný
elipsoidní	elipsoidní	k2eAgInSc1d1	elipsoidní
tvar	tvar	k1gInSc1	tvar
<g/>
,	,	kIx,	,
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
směru	směr	k1gInSc6	směr
má	mít	k5eAaImIp3nS	mít
průměr	průměr	k1gInSc4	průměr
513	[number]	k4	513
km	km	kA	km
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
polokoulí	polokoule	k1gFnSc7	polokoule
přivrácenou	přivrácený	k2eAgFnSc7d1	přivrácená
a	a	k8xC	a
odvrácenou	odvrácený	k2eAgFnSc7d1	odvrácená
k	k	k7c3	k
Saturnu	Saturn	k1gInSc3	Saturn
pak	pak	k9	pak
503	[number]	k4	503
km	km	kA	km
a	a	k8xC	a
mezi	mezi	k7c7	mezi
severním	severní	k2eAgInSc7d1	severní
a	a	k8xC	a
jižním	jižní	k2eAgInSc7d1	jižní
pólem	pól	k1gInSc7	pól
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
průměru	průměr	k1gInSc2	průměr
497	[number]	k4	497
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Enceladus	Enceladus	k1gInSc1	Enceladus
tak	tak	k9	tak
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
přibližně	přibližně	k6eAd1	přibližně
jedné	jeden	k4xCgFnSc2	jeden
sedminy	sedmina	k1gFnSc2	sedmina
velikosti	velikost	k1gFnSc2	velikost
pozemského	pozemský	k2eAgInSc2d1	pozemský
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
šestý	šestý	k4xOgInSc4	šestý
největší	veliký	k2eAgInSc4d3	veliký
a	a	k8xC	a
šestý	šestý	k4xOgInSc4	šestý
nejhmotnější	hmotný	k2eAgInSc4d3	hmotný
měsíc	měsíc	k1gInSc4	měsíc
Saturnu	Saturn	k1gInSc2	Saturn
<g/>
.	.	kIx.	.
</s>
<s>
Měsíc	měsíc	k1gInSc1	měsíc
Mimas	Mimas	k1gInSc1	Mimas
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
leží	ležet	k5eAaImIp3nS	ležet
k	k	k7c3	k
Saturnu	Saturn	k1gInSc3	Saturn
blíže	blíž	k1gFnSc2	blíž
než	než	k8xS	než
Enceladus	Enceladus	k1gInSc1	Enceladus
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
geologicky	geologicky	k6eAd1	geologicky
mrtvým	mrtvý	k2eAgInSc7d1	mrtvý
světem	svět	k1gInSc7	svět
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
přes	přes	k7c4	přes
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
bližší	blízký	k2eAgFnSc3d2	bližší
poloze	poloha	k1gFnSc3	poloha
k	k	k7c3	k
Saturnu	Saturn	k1gMnSc3	Saturn
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
vystaven	vystavit	k5eAaPmNgMnS	vystavit
silnějším	silný	k2eAgInPc3d2	silnější
slapovým	slapový	k2eAgInPc3d1	slapový
jevům	jev	k1gInPc3	jev
než	než	k8xS	než
Enceladus	Enceladus	k1gInSc4	Enceladus
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
paradox	paradox	k1gInSc1	paradox
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vysvětlen	vysvětlit	k5eAaPmNgInS	vysvětlit
z	z	k7c2	z
části	část	k1gFnSc2	část
závislosti	závislost	k1gFnSc2	závislost
chování	chování	k1gNnSc2	chování
vodního	vodní	k2eAgInSc2d1	vodní
ledu	led	k1gInSc2	led
na	na	k7c6	na
teplotě	teplota	k1gFnSc6	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Tedy	tedy	k9	tedy
látky	látka	k1gFnPc1	látka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
převážnou	převážný	k2eAgFnSc4d1	převážná
část	část	k1gFnSc4	část
Mimase	Mimasa	k1gFnSc3	Mimasa
i	i	k8xC	i
Enceladu	Encelad	k1gInSc3	Encelad
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
slapového	slapový	k2eAgNnSc2d1	slapové
zahřívání	zahřívání	k1gNnSc2	zahřívání
na	na	k7c4	na
jednotku	jednotka	k1gFnSc4	jednotka
hmotnosti	hmotnost	k1gFnSc2	hmotnost
je	být	k5eAaImIp3nS	být
dána	dát	k5eAaPmNgFnS	dát
vztahem	vztah	k1gInSc7	vztah
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
q	q	k?	q
:	:	kIx,	:
t	t	k?	t
i	i	k8xC	i
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
63	[number]	k4	63
ρ	ρ	k?	ρ
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
5	[number]	k4	5
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
r	r	kA	r
:	:	kIx,	:
4	[number]	k4	4
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
38	[number]	k4	38
μ	μ	k?	μ
Q	Q	kA	Q
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
tid	tid	k?	tid
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
63	[number]	k4	63
<g/>
\	\	kIx~	\
<g/>
rho	rho	k?	rho
n	n	k0	n
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
5	[number]	k4	5
<g/>
}	}	kIx)	}
<g/>
r	r	kA	r
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
e	e	k0	e
<g/>
^	^	kIx~	^
<g />
.	.	kIx.	.
</s>
<s>
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
38	[number]	k4	38
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
Q	Q	kA	Q
<g/>
}}}	}}}	k?	}}}
,	,	kIx,	,
kdeρ	kdeρ	k?	kdeρ
je	být	k5eAaImIp3nS	být
hustota	hustota	k1gFnSc1	hustota
měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
n	n	k0	n
je	on	k3xPp3gFnPc4	on
střední	střední	k2eAgFnSc1d1	střední
úhlová	úhlový	k2eAgFnSc1d1	úhlová
rychlost	rychlost	k1gFnSc1	rychlost
<g/>
,	,	kIx,	,
r	r	kA	r
je	být	k5eAaImIp3nS	být
poloměr	poloměr	k1gInSc4	poloměr
měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
e	e	k0	e
je	být	k5eAaImIp3nS	být
excentricita	excentricita	k1gFnSc1	excentricita
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
měsíce	měsíc	k1gInSc2	měsíc
,	,	kIx,	,
μ	μ	k?	μ
je	být	k5eAaImIp3nS	být
modul	modul	k1gInSc1	modul
pružnosti	pružnost	k1gFnSc2	pružnost
ve	v	k7c6	v
smyku	smyk	k1gInSc6	smyk
a	a	k8xC	a
Q	Q	kA	Q
je	být	k5eAaImIp3nS	být
bezrozměrný	bezrozměrný	k2eAgInSc1d1	bezrozměrný
disipační	disipační	k2eAgInSc1d1	disipační
faktor	faktor	k1gInSc1	faktor
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
stejné	stejný	k2eAgNnSc4d1	stejné
teplotní	teplotní	k2eAgNnSc4d1	teplotní
přiblížení	přiblížení	k1gNnSc4	přiblížení
vychází	vycházet	k5eAaImIp3nS	vycházet
<g/>
,	,	kIx,	,
že	že	k8xS	že
očekávaná	očekávaný	k2eAgFnSc1d1	očekávaná
hodnota	hodnota	k1gFnSc1	hodnota
qtid	qtid	k6eAd1	qtid
pro	pro	k7c4	pro
Mimas	Mimas	k1gInSc4	Mimas
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
40	[number]	k4	40
násobná	násobný	k2eAgFnSc1d1	násobná
než	než	k8xS	než
u	u	k7c2	u
Enceladu	Encelad	k1gInSc2	Encelad
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
parametry	parametr	k1gInPc1	parametr
μ	μ	k?	μ
a	a	k8xC	a
Q	Q	kA	Q
jsou	být	k5eAaImIp3nP	být
teplotně	teplotně	k6eAd1	teplotně
závislé	závislý	k2eAgInPc1d1	závislý
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vysokých	vysoký	k2eAgFnPc2d1	vysoká
teplot	teplota	k1gFnPc2	teplota
(	(	kIx(	(
<g/>
blízko	blízko	k7c2	blízko
bodu	bod	k1gInSc2	bod
tání	tání	k1gNnSc2	tání
<g/>
)	)	kIx)	)
μ	μ	k?	μ
a	a	k8xC	a
Q	Q	kA	Q
jsou	být	k5eAaImIp3nP	být
nízké	nízký	k2eAgFnPc1d1	nízká
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
slapové	slapový	k2eAgNnSc1d1	slapové
zahřívání	zahřívání	k1gNnSc1	zahřívání
je	být	k5eAaImIp3nS	být
silné	silný	k2eAgNnSc1d1	silné
<g/>
.	.	kIx.	.
</s>
<s>
Modelování	modelování	k1gNnSc1	modelování
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
Enceladus	Enceladus	k1gInSc4	Enceladus
je	být	k5eAaImIp3nS	být
jak	jak	k6eAd1	jak
základní	základní	k2eAgInSc1d1	základní
nízkoenergetický	nízkoenergetický	k2eAgInSc1d1	nízkoenergetický
teplotní	teplotní	k2eAgInSc1d1	teplotní
stav	stav	k1gInSc1	stav
s	s	k7c7	s
malým	malý	k2eAgInSc7d1	malý
vnitřním	vnitřní	k2eAgInSc7d1	vnitřní
teplotním	teplotní	k2eAgInSc7d1	teplotní
gradientem	gradient	k1gInSc7	gradient
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
excitovaný	excitovaný	k2eAgInSc1d1	excitovaný
vysokoenergetický	vysokoenergetický	k2eAgInSc1d1	vysokoenergetický
teplotní	teplotní	k2eAgInSc1d1	teplotní
stav	stav	k1gInSc1	stav
s	s	k7c7	s
významným	významný	k2eAgInSc7d1	významný
teplotním	teplotní	k2eAgInSc7d1	teplotní
gradientem	gradient	k1gInSc7	gradient
a	a	k8xC	a
doprovodnou	doprovodný	k2eAgFnSc7d1	doprovodná
konvekcí	konvekce	k1gFnSc7	konvekce
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
jejímu	její	k3xOp3gInSc3	její
vzniku	vznik	k1gInSc3	vznik
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
stabilní	stabilní	k2eAgFnSc1d1	stabilní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
Mimasu	Mimas	k1gInSc2	Mimas
se	se	k3xPyFc4	se
zdá	zdát	k5eAaPmIp3nS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
pouze	pouze	k6eAd1	pouze
nízkoenergetický	nízkoenergetický	k2eAgInSc1d1	nízkoenergetický
stav	stav	k1gInSc1	stav
je	být	k5eAaImIp3nS	být
stabilní	stabilní	k2eAgNnSc4d1	stabilní
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
přes	přes	k7c4	přes
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Mimas	Mimas	k1gInSc1	Mimas
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
Saturnu	Saturn	k1gInSc3	Saturn
blíže	blíž	k1gFnSc2	blíž
<g/>
.	.	kIx.	.
</s>
<s>
Geofyzikální	geofyzikální	k2eAgInPc1d1	geofyzikální
modely	model	k1gInPc1	model
tak	tak	k9	tak
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
nízkoenergetický	nízkoenergetický	k2eAgInSc4d1	nízkoenergetický
teplotní	teplotní	k2eAgInSc4d1	teplotní
stav	stav	k1gInSc4	stav
pro	pro	k7c4	pro
Mimas	Mimas	k1gInSc4	Mimas
(	(	kIx(	(
<g/>
hodnoty	hodnota	k1gFnPc1	hodnota
μ	μ	k?	μ
a	a	k8xC	a
Q	Q	kA	Q
jsou	být	k5eAaImIp3nP	být
vyšší	vysoký	k2eAgFnPc1d2	vyšší
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
možný	možný	k2eAgInSc1d1	možný
vyšší	vysoký	k2eAgInSc1d2	vyšší
teplotní	teplotní	k2eAgInSc1d1	teplotní
stav	stav	k1gInSc1	stav
pro	pro	k7c4	pro
Enceladus	Enceladus	k1gInSc4	Enceladus
(	(	kIx(	(
<g/>
hodnoty	hodnota	k1gFnPc1	hodnota
μ	μ	k?	μ
a	a	k8xC	a
Q	Q	kA	Q
jsou	být	k5eAaImIp3nP	být
nízké	nízký	k2eAgFnPc1d1	nízká
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
informace	informace	k1gFnPc1	informace
z	z	k7c2	z
historie	historie	k1gFnSc2	historie
Enceladu	Encelad	k1gInSc2	Encelad
jsou	být	k5eAaImIp3nP	být
potřeba	potřeba	k6eAd1	potřeba
k	k	k7c3	k
vysvětlení	vysvětlení	k1gNnSc3	vysvětlení
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
mohl	moct	k5eAaImAgInS	moct
Enceladus	Enceladus	k1gInSc4	Enceladus
vůbec	vůbec	k9	vůbec
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
vysokoenergetického	vysokoenergetický	k2eAgInSc2d1	vysokoenergetický
stavu	stav	k1gInSc2	stav
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
radioaktivním	radioaktivní	k2eAgNnSc7d1	radioaktivní
zahříváním	zahřívání	k1gNnSc7	zahřívání
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
vyšší	vysoký	k2eAgFnSc7d2	vyšší
excentricitou	excentricita	k1gFnSc7	excentricita
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Významně	významně	k6eAd1	významně
vyšší	vysoký	k2eAgFnSc1d2	vyšší
hustota	hustota	k1gFnSc1	hustota
Enceladu	Encelad	k1gInSc2	Encelad
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
Mimasem	Mimas	k1gMnSc7	Mimas
(	(	kIx(	(
<g/>
1,61	[number]	k4	1,61
vs	vs	k?	vs
<g/>
.	.	kIx.	.
1,15	[number]	k4	1,15
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
cm	cm	kA	cm
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
vyšší	vysoký	k2eAgNnSc4d2	vyšší
zastoupení	zastoupení	k1gNnSc4	zastoupení
hornin	hornina	k1gFnPc2	hornina
a	a	k8xC	a
tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
i	i	k9	i
vyšší	vysoký	k2eAgFnSc4d2	vyšší
produkci	produkce	k1gFnSc4	produkce
tepla	tepnout	k5eAaPmAgFnS	tepnout
radioaktivním	radioaktivní	k2eAgNnSc7d1	radioaktivní
zahříváním	zahřívání	k1gNnSc7	zahřívání
po	po	k7c6	po
zformování	zformování	k1gNnSc2	zformování
těchto	tento	k3xDgNnPc2	tento
těles	těleso	k1gNnPc2	těleso
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
uváděna	uvádět	k5eAaImNgFnS	uvádět
jako	jako	k8xS	jako
významný	významný	k2eAgInSc4d1	významný
faktor	faktor	k1gInSc4	faktor
ve	v	k7c4	v
vyřešení	vyřešení	k1gNnSc4	vyřešení
tohoto	tento	k3xDgInSc2	tento
paradoxu	paradox	k1gInSc2	paradox
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
navrženo	navrhnout	k5eAaPmNgNnS	navrhnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
aby	aby	kYmCp3nP	aby
ledové	ledový	k2eAgInPc1d1	ledový
měsíce	měsíc	k1gInPc1	měsíc
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
Mimase	Mimasa	k1gFnSc6	Mimasa
nebo	nebo	k8xC	nebo
Enceladu	Encelad	k1gInSc6	Encelad
mohly	moct	k5eAaImAgFnP	moct
vstoupit	vstoupit	k5eAaPmF	vstoupit
do	do	k7c2	do
excitovaného	excitovaný	k2eAgInSc2d1	excitovaný
stavu	stav	k1gInSc2	stav
slapového	slapový	k2eAgNnSc2d1	slapové
zahřívání	zahřívání	k1gNnSc2	zahřívání
a	a	k8xC	a
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
vzniku	vznik	k1gInSc3	vznik
doprovodné	doprovodný	k2eAgFnPc4d1	doprovodná
konvektivního	konvektivní	k2eAgNnSc2d1	konvektivní
proudění	proudění	k1gNnSc2	proudění
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nP	muset
vstoupit	vstoupit	k5eAaPmF	vstoupit
do	do	k7c2	do
oběžné	oběžný	k2eAgFnSc2d1	oběžná
rezonance	rezonance	k1gFnSc2	rezonance
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
ztratí	ztratit	k5eAaPmIp3nS	ztratit
příliš	příliš	k6eAd1	příliš
mnoho	mnoho	k6eAd1	mnoho
počátečního	počáteční	k2eAgNnSc2d1	počáteční
tepla	teplo	k1gNnSc2	teplo
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
Mimas	Mimas	k1gInSc1	Mimas
menší	malý	k2eAgInSc1d2	menší
než	než	k8xS	než
Enceladus	Enceladus	k1gInSc1	Enceladus
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
chladl	chladnout	k5eAaImAgMnS	chladnout
a	a	k8xC	a
zmrzl	zmrznout	k5eAaPmAgMnS	zmrznout
mnohem	mnohem	k6eAd1	mnohem
rychleji	rychle	k6eAd2	rychle
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
by	by	kYmCp3nS	by
znamenalo	znamenat	k5eAaImAgNnS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
příležitost	příležitost	k1gFnSc1	příležitost
pro	pro	k7c4	pro
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
oběžné	oběžný	k2eAgFnSc2d1	oběžná
rezonance	rezonance	k1gFnSc2	rezonance
s	s	k7c7	s
jiným	jiný	k2eAgNnSc7d1	jiné
tělesem	těleso	k1gNnSc7	těleso
byla	být	k5eAaImAgFnS	být
mnohem	mnohem	k6eAd1	mnohem
kratší	krátký	k2eAgFnSc1d2	kratší
<g/>
.	.	kIx.	.
</s>
<s>
Enceladus	Enceladus	k1gMnSc1	Enceladus
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
svou	svůj	k3xOyFgFnSc4	svůj
hmotnost	hmotnost	k1gFnSc4	hmotnost
rychlostí	rychlost	k1gFnPc2	rychlost
přibližně	přibližně	k6eAd1	přibližně
200	[number]	k4	200
kg	kg	kA	kg
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Kdyby	kdyby	kYmCp3nS	kdyby
měsíc	měsíc	k1gInSc1	měsíc
ztrácel	ztrácet	k5eAaImAgInS	ztrácet
svou	svůj	k3xOyFgFnSc4	svůj
hmotnost	hmotnost	k1gFnSc4	hmotnost
takovouto	takovýto	k3xDgFnSc7	takovýto
rychlostí	rychlost	k1gFnSc7	rychlost
po	po	k7c4	po
4,5	[number]	k4	4,5
miliardy	miliarda	k4xCgFnSc2	miliarda
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
by	by	kYmCp3nS	by
během	během	k7c2	během
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
ztratil	ztratit	k5eAaPmAgInS	ztratit
přibližně	přibližně	k6eAd1	přibližně
30	[number]	k4	30
%	%	kIx~	%
své	svůj	k3xOyFgFnSc2	svůj
původní	původní	k2eAgFnSc2d1	původní
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
Podobné	podobný	k2eAgFnPc4d1	podobná
hodnoty	hodnota	k1gFnPc4	hodnota
je	být	k5eAaImIp3nS	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
za	za	k7c2	za
předpokladu	předpoklad	k1gInSc2	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
počáteční	počáteční	k2eAgFnSc1d1	počáteční
hustota	hustota	k1gFnSc1	hustota
Enceladu	Encelad	k1gInSc2	Encelad
a	a	k8xC	a
Mimase	Mimasa	k1gFnSc6	Mimasa
byla	být	k5eAaImAgFnS	být
shodná	shodný	k2eAgFnSc1d1	shodná
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tektonismus	tektonismus	k1gInSc1	tektonismus
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
jižního	jižní	k2eAgInSc2d1	jižní
pólu	pól	k1gInSc2	pól
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
spojen	spojit	k5eAaPmNgInS	spojit
s	s	k7c7	s
poklesem	pokles	k1gInSc7	pokles
a	a	k8xC	a
doprovodnou	doprovodný	k2eAgFnSc7d1	doprovodná
subdukcí	subdukce	k1gFnSc7	subdukce
způsobenou	způsobený	k2eAgFnSc4d1	způsobená
procesy	proces	k1gInPc7	proces
spojenými	spojený	k2eAgInPc7d1	spojený
se	s	k7c7	s
ztrátou	ztráta	k1gFnSc7	ztráta
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
vyšla	vyjít	k5eAaPmAgFnS	vyjít
studie	studie	k1gFnSc1	studie
zkoumající	zkoumající	k2eAgFnSc4d1	zkoumající
změnu	změna	k1gFnSc4	změna
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
Saturnovo	Saturnův	k2eAgNnSc1d1	Saturnovo
měsíců	měsíc	k1gInPc2	měsíc
na	na	k7c6	na
základě	základ	k1gInSc6	základ
slapového	slapový	k2eAgNnSc2d1	slapové
působení	působení	k1gNnSc2	působení
mateřské	mateřský	k2eAgFnSc2d1	mateřská
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
všechny	všechen	k3xTgInPc4	všechen
měsíce	měsíc	k1gInPc4	měsíc
ležící	ležící	k2eAgInPc4d1	ležící
mezi	mezi	k7c7	mezi
Saturnem	Saturn	k1gInSc7	Saturn
a	a	k8xC	a
Titánem	Titán	k1gMnSc7	Titán
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Enceladu	Encelad	k1gInSc2	Encelad
<g/>
,	,	kIx,	,
mohly	moct	k5eAaImAgFnP	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
relativně	relativně	k6eAd1	relativně
nedávno	nedávno	k6eAd1	nedávno
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
přibližně	přibližně	k6eAd1	přibližně
100	[number]	k4	100
milióny	milión	k4xCgInPc7	milión
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Enceladus	Enceladus	k1gInSc1	Enceladus
vyvrhuje	vyvrhovat	k5eAaImIp3nS	vyvrhovat
mračna	mračna	k1gFnSc1	mračna
slané	slaný	k2eAgFnSc2d1	slaná
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
nepatrná	nepatrný	k2eAgNnPc4d1	nepatrný
zrna	zrno	k1gNnPc4	zrno
křemičitého	křemičitý	k2eAgInSc2d1	křemičitý
písku	písek	k1gInSc2	písek
<g/>
,	,	kIx,	,
dusíku	dusík	k1gInSc2	dusík
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
čpavku	čpavek	k1gInSc2	čpavek
<g/>
,	,	kIx,	,
a	a	k8xC	a
organických	organický	k2eAgFnPc2d1	organická
molekul	molekula	k1gFnPc2	molekula
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
stop	stop	k2eAgInSc2d1	stop
metanu	metan	k1gInSc2	metan
<g/>
,	,	kIx,	,
propanu	propan	k1gInSc2	propan
<g/>
,	,	kIx,	,
acetylenu	acetylen	k1gInSc2	acetylen
a	a	k8xC	a
formaldehydu	formaldehyd	k1gInSc2	formaldehyd
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
měsíce	měsíc	k1gInSc2	měsíc
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
hydrotermální	hydrotermální	k2eAgFnSc3d1	hydrotermální
aktivitě	aktivita	k1gFnSc3	aktivita
a	a	k8xC	a
že	že	k8xS	že
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
tedy	tedy	k9	tedy
nachází	nacházet	k5eAaImIp3nS	nacházet
energetický	energetický	k2eAgInSc1d1	energetický
zdroj	zdroj	k1gInSc1	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
geofyzikální	geofyzikální	k2eAgInPc1d1	geofyzikální
modely	model	k1gInPc1	model
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kamenité	kamenitý	k2eAgNnSc1d1	kamenité
jádro	jádro	k1gNnSc1	jádro
je	být	k5eAaImIp3nS	být
porézní	porézní	k2eAgNnSc1d1	porézní
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vodě	voda	k1gFnSc3	voda
do	do	k7c2	do
něj	on	k3xPp3gNnSc2	on
snadno	snadno	k6eAd1	snadno
pronikat	pronikat	k5eAaImF	pronikat
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
z	z	k7c2	z
jádra	jádro	k1gNnSc2	jádro
odvádět	odvádět	k5eAaImF	odvádět
teplo	teplo	k1gNnSc4	teplo
<g/>
.	.	kIx.	.
</s>
<s>
Molekulární	molekulární	k2eAgInSc1d1	molekulární
vodík	vodík	k1gInSc1	vodík
(	(	kIx(	(
<g/>
Šablona	šablona	k1gFnSc1	šablona
<g/>
:	:	kIx,	:
<g/>
Chem	Chem	k1gInSc1	Chem
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
geochemický	geochemický	k2eAgInSc4d1	geochemický
zdroj	zdroj	k1gInSc4	zdroj
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
metabolizován	metabolizován	k2eAgInSc4d1	metabolizován
metanogoneními	metanogonení	k1gNnPc7	metanogonení
bakteriemi	bakterie	k1gFnPc7	bakterie
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
dle	dle	k7c2	dle
modelů	model	k1gInPc2	model
ve	v	k7c6	v
slaném	slaný	k2eAgInSc6d1	slaný
oceánu	oceán	k1gInSc6	oceán
Enceladu	Encelad	k1gInSc2	Encelad
přítomen	přítomno	k1gNnPc2	přítomno
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
oceán	oceán	k1gInSc1	oceán
zásadité	zásaditý	k2eAgFnSc2d1	zásaditá
pH	ph	kA	ph
vlivem	vlivem	k7c2	vlivem
serprentinizace	serprentinizace	k1gFnSc2	serprentinizace
chondritických	chondritický	k2eAgFnPc2d1	chondritický
hornin	hornina	k1gFnPc2	hornina
<g/>
.	.	kIx.	.
</s>
<s>
Přítomnost	přítomnost	k1gFnSc1	přítomnost
podpovrchového	podpovrchový	k2eAgInSc2d1	podpovrchový
globálního	globální	k2eAgInSc2d1	globální
slaného	slaný	k2eAgInSc2d1	slaný
oceánu	oceán	k1gInSc2	oceán
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgNnSc6	který
se	se	k3xPyFc4	se
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nachází	nacházet	k5eAaImIp3nS	nacházet
globální	globální	k2eAgFnPc4d1	globální
cirkulace	cirkulace	k1gFnPc4	cirkulace
<g/>
,	,	kIx,	,
zdroj	zdroj	k1gInSc4	zdroj
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
jednoduché	jednoduchý	k2eAgFnPc1d1	jednoduchá
organické	organický	k2eAgFnPc1d1	organická
sloučeniny	sloučenina	k1gFnPc1	sloučenina
a	a	k8xC	a
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
voda	voda	k1gFnSc1	voda
v	v	k7c6	v
kontaktu	kontakt	k1gInSc6	kontakt
s	s	k7c7	s
kamenitým	kamenitý	k2eAgNnSc7d1	kamenité
jádrem	jádro	k1gNnSc7	jádro
měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
představuje	představovat	k5eAaImIp3nS	představovat
vhodné	vhodný	k2eAgNnSc1d1	vhodné
prostředí	prostředí	k1gNnSc1	prostředí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
případně	případně	k6eAd1	případně
hledat	hledat	k5eAaImF	hledat
po	po	k7c6	po
stopách	stopa	k1gFnPc6	stopa
možného	možný	k2eAgInSc2d1	možný
mimozemského	mimozemský	k2eAgInSc2d1	mimozemský
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
bylo	být	k5eAaImAgNnS	být
navrženo	navrhnout	k5eAaPmNgNnS	navrhnout
několik	několik	k4yIc1	několik
dalších	další	k2eAgFnPc2d1	další
robotických	robotický	k2eAgFnPc2d1	robotická
misí	mise	k1gFnPc2	mise
zaměřených	zaměřený	k2eAgFnPc2d1	zaměřená
na	na	k7c4	na
výzkum	výzkum	k1gInSc4	výzkum
Enceladu	Encelad	k1gInSc2	Encelad
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc2	jeho
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Dvojice	dvojice	k1gFnSc1	dvojice
sond	sonda	k1gFnPc2	sonda
Voyager	Voyagero	k1gNnPc2	Voyagero
byly	být	k5eAaImAgFnP	být
prvními	první	k4xOgFnPc7	první
sondami	sonda	k1gFnPc7	sonda
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
pořídily	pořídit	k5eAaPmAgFnP	pořídit
první	první	k4xOgFnPc4	první
detailní	detailní	k2eAgFnPc4d1	detailní
snímky	snímka	k1gFnPc4	snímka
Enceladu	Encelad	k1gInSc2	Encelad
z	z	k7c2	z
bezprostřední	bezprostřední	k2eAgFnSc2d1	bezprostřední
blízkosti	blízkost	k1gFnSc2	blízkost
<g/>
.	.	kIx.	.
</s>
<s>
Voyager	Voyager	k1gInSc4	Voyager
1	[number]	k4	1
proletěla	proletět	k5eAaPmAgFnS	proletět
kolem	kolem	k7c2	kolem
měsíce	měsíc	k1gInSc2	měsíc
jako	jako	k8xS	jako
první	první	k4xOgInSc4	první
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1980	[number]	k4	1980
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
202,000	[number]	k4	202,000
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Snímky	snímek	k1gInPc4	snímek
pořízené	pořízený	k2eAgInPc4d1	pořízený
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
měly	mít	k5eAaImAgFnP	mít
velice	velice	k6eAd1	velice
nízké	nízký	k2eAgNnSc4d1	nízké
rozlišení	rozlišení	k1gNnSc4	rozlišení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
přes	přes	k7c4	přes
to	ten	k3xDgNnSc4	ten
umožnily	umožnit	k5eAaPmAgInP	umožnit
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vysoce	vysoce	k6eAd1	vysoce
odrazivý	odrazivý	k2eAgInSc4d1	odrazivý
povrch	povrch	k1gInSc4	povrch
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
málo	málo	k6eAd1	málo
posetý	posetý	k2eAgInSc1d1	posetý
impaktními	impaktní	k2eAgInPc7d1	impaktní
krátery	kráter	k1gInPc7	kráter
naznačující	naznačující	k2eAgFnSc2d1	naznačující
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
mladý	mladý	k2eAgMnSc1d1	mladý
<g/>
.	.	kIx.	.
</s>
<s>
Voyager	Voyager	k1gInSc1	Voyager
1	[number]	k4	1
taktéž	taktéž	k?	taktéž
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Enceladus	Enceladus	k1gInSc1	Enceladus
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
nejhustší	hustý	k2eAgFnSc6d3	nejhustší
části	část	k1gFnSc6	část
Saturnovo	Saturnův	k2eAgNnSc4d1	Saturnovo
prstence	prstenec	k1gInPc4	prstenec
E.	E.	kA	E.
To	to	k9	to
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
mladým	mladý	k2eAgInSc7d1	mladý
vzhledem	vzhled	k1gInSc7	vzhled
povrchu	povrch	k1gInSc2	povrch
<g/>
,	,	kIx,	,
vedlo	vést	k5eAaImAgNnS	vést
vědce	vědec	k1gMnSc4	vědec
k	k	k7c3	k
domněnce	domněnka	k1gFnSc3	domněnka
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
částice	částice	k1gFnPc1	částice
prstence	prstenec	k1gInSc2	prstenec
E	E	kA	E
mohly	moct	k5eAaImAgInP	moct
pocházet	pocházet	k5eAaImF	pocházet
z	z	k7c2	z
povrchu	povrch	k1gInSc2	povrch
Enceladu	Encelad	k1gInSc2	Encelad
<g/>
.	.	kIx.	.
</s>
<s>
Sonda	sonda	k1gFnSc1	sonda
Voyager	Voyager	k1gInSc1	Voyager
2	[number]	k4	2
proletěla	proletět	k5eAaPmAgFnS	proletět
26	[number]	k4	26
<g/>
.	.	kIx.	.
</s>
<s>
Srpna	srpen	k1gInSc2	srpen
1981	[number]	k4	1981
kolem	kolem	k7c2	kolem
Enceladu	Encelad	k1gInSc2	Encelad
blíže	blíž	k1gFnSc2	blíž
než	než	k8xS	než
její	její	k3xOp3gFnSc1	její
sesterská	sesterský	k2eAgFnSc1d1	sesterská
sonda	sonda	k1gFnSc1	sonda
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
87,010	[number]	k4	87,010
km	km	kA	km
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
pořídit	pořídit	k5eAaPmF	pořídit
snímky	snímek	k1gInPc4	snímek
s	s	k7c7	s
vyšším	vysoký	k2eAgNnSc7d2	vyšší
rozlišením	rozlišení	k1gNnSc7	rozlišení
<g/>
.	.	kIx.	.
</s>
<s>
These	these	k1gFnSc1	these
images	images	k1gMnSc1	images
showed	showed	k1gMnSc1	showed
a	a	k8xC	a
young	young	k1gMnSc1	young
surface	surface	k1gFnSc2	surface
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgFnPc1d1	Nové
fotografie	fotografia	k1gFnPc1	fotografia
pomohly	pomoct	k5eAaPmAgFnP	pomoct
odhalit	odhalit	k5eAaPmF	odhalit
oblasti	oblast	k1gFnPc1	oblast
s	s	k7c7	s
různým	různý	k2eAgInSc7d1	různý
vzhledem	vzhled	k1gInSc7	vzhled
<g/>
,	,	kIx,	,
množstvím	množství	k1gNnSc7	množství
kráterů	kráter	k1gInPc2	kráter
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
rozdílným	rozdílný	k2eAgInSc7d1	rozdílný
věkem	věk	k1gInSc7	věk
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
krátery	kráter	k1gInPc1	kráter
silně	silně	k6eAd1	silně
poseté	posetý	k2eAgFnSc2d1	posetá
oblasti	oblast	k1gFnSc2	oblast
se	se	k3xPyFc4	se
našly	najít	k5eAaPmAgFnP	najít
převážně	převážně	k6eAd1	převážně
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
pólu	pól	k1gInSc3	pól
<g/>
,	,	kIx,	,
tak	tak	k9	tak
oblasti	oblast	k1gFnSc2	oblast
impaktními	impaktní	k2eAgInPc7d1	impaktní
krátery	kráter	k1gInPc7	kráter
málo	málo	k6eAd1	málo
postižené	postižený	k2eAgFnPc1d1	postižená
byly	být	k5eAaImAgFnP	být
nalezeny	nalezen	k2eAgFnPc1d1	nalezena
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
rovníku	rovník	k1gInSc2	rovník
<g/>
.	.	kIx.	.
</s>
<s>
Enceladus	Enceladus	k1gMnSc1	Enceladus
tak	tak	k9	tak
svým	svůj	k3xOyFgInSc7	svůj
vzhledem	vzhled	k1gInSc7	vzhled
na	na	k7c4	na
první	první	k4xOgInSc4	první
pohled	pohled	k1gInSc4	pohled
neodpovídal	odpovídat	k5eNaImAgMnS	odpovídat
impaktními	impaktní	k2eAgInPc7d1	impaktní
krátery	kráter	k1gInPc7	kráter
silně	silně	k6eAd1	silně
posetému	posetý	k2eAgInSc3d1	posetý
Mimasu	Mimas	k1gInSc3	Mimas
<g/>
,	,	kIx,	,
dalšímu	další	k2eAgInSc3d1	další
měsíci	měsíc	k1gInSc3	měsíc
Saturnu	Saturn	k1gInSc2	Saturn
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
o	o	k7c4	o
trochu	trochu	k6eAd1	trochu
menší	malý	k2eAgInSc4d2	menší
než	než	k8xS	než
Enceladus	Enceladus	k1gInSc4	Enceladus
<g/>
.	.	kIx.	.
</s>
<s>
Objevení	objevení	k1gNnSc1	objevení
oblastí	oblast	k1gFnPc2	oblast
s	s	k7c7	s
malým	malý	k2eAgNnSc7d1	malé
množstvím	množství	k1gNnSc7	množství
impaktních	impaktní	k2eAgInPc2d1	impaktní
kráterů	kráter	k1gInPc2	kráter
vědeckou	vědecký	k2eAgFnSc4d1	vědecká
obec	obec	k1gFnSc4	obec
překvapil	překvapit	k5eAaPmAgInS	překvapit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
žádná	žádný	k3yNgFnSc1	žádný
vědecká	vědecký	k2eAgFnSc1d1	vědecká
teorie	teorie	k1gFnSc1	teorie
nepředpokládala	předpokládat	k5eNaImAgFnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
takto	takto	k6eAd1	takto
malý	malý	k2eAgInSc1d1	malý
a	a	k8xC	a
chladný	chladný	k2eAgInSc1d1	chladný
měsíc	měsíc	k1gInSc1	měsíc
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
takto	takto	k6eAd1	takto
vypadat	vypadat	k5eAaPmF	vypadat
<g/>
.	.	kIx.	.
</s>
<s>
Zodpovězení	zodpovězený	k2eAgMnPc1d1	zodpovězený
mnoha	mnoho	k4c2	mnoho
otázek	otázka	k1gFnPc2	otázka
musela	muset	k5eAaImAgFnS	muset
počkat	počkat	k5eAaPmF	počkat
až	až	k9	až
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
soustavy	soustava	k1gFnSc2	soustava
Saturnu	Saturn	k1gInSc2	Saturn
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2004	[number]	k4	2004
úspěšně	úspěšně	k6eAd1	úspěšně
navedena	naveden	k2eAgFnSc1d1	navedena
sonda	sonda	k1gFnSc1	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
poznatky	poznatek	k1gInPc4	poznatek
sond	sonda	k1gFnPc2	sonda
Voyager	Voyagra	k1gFnPc2	Voyagra
představoval	představovat	k5eAaImAgInS	představovat
Enceladus	Enceladus	k1gInSc1	Enceladus
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgInPc2d1	hlavní
vědeckých	vědecký	k2eAgInPc2d1	vědecký
cílů	cíl	k1gInPc2	cíl
této	tento	k3xDgFnSc2	tento
nové	nový	k2eAgFnSc2d1	nová
mise	mise	k1gFnSc2	mise
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
odrazilo	odrazit	k5eAaPmAgNnS	odrazit
v	v	k7c6	v
naplánování	naplánování	k1gNnSc6	naplánování
několika	několik	k4yIc2	několik
poměrně	poměrně	k6eAd1	poměrně
blízkých	blízký	k2eAgInPc2d1	blízký
průletů	průlet	k1gInPc2	průlet
v	v	k7c4	v
okolí	okolí	k1gNnSc4	okolí
tohoto	tento	k3xDgInSc2	tento
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Data	datum	k1gNnPc1	datum
pořízená	pořízený	k2eAgNnPc1d1	pořízené
během	během	k7c2	během
průletů	průlet	k1gInPc2	průlet
pak	pak	k6eAd1	pak
umožnila	umožnit	k5eAaPmAgFnS	umožnit
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
objevů	objev	k1gInPc2	objev
umožňující	umožňující	k2eAgInPc1d1	umožňující
lépe	dobře	k6eAd2	dobře
pochopit	pochopit	k5eAaPmF	pochopit
procesy	proces	k1gInPc4	proces
odehrávající	odehrávající	k2eAgInPc4d1	odehrávající
se	se	k3xPyFc4	se
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
měsíci	měsíc	k1gInSc6	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
objev	objev	k1gInSc1	objev
aktivního	aktivní	k2eAgInSc2d1	aktivní
kryovulkanismu	kryovulkanismus	k1gInSc2	kryovulkanismus
na	na	k7c6	na
Enceladu	Encelad	k1gInSc6	Encelad
způsobil	způsobit	k5eAaPmAgMnS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
mise	mise	k1gFnSc2	mise
dostalo	dostat	k5eAaPmAgNnS	dostat
Enceladu	Encelad	k1gInSc3	Encelad
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgFnSc3d2	veliký
pozornosti	pozornost	k1gFnSc3	pozornost
a	a	k8xC	a
naplánování	naplánování	k1gNnSc3	naplánování
dalších	další	k2eAgInPc2d1	další
průletů	průlet	k1gInPc2	průlet
(	(	kIx(	(
<g/>
některých	některý	k3yIgInPc2	některý
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
pouhých	pouhý	k2eAgInPc2d1	pouhý
48	[number]	k4	48
až	až	k9	až
50	[number]	k4	50
km	km	kA	km
nad	nad	k7c7	nad
povrchem	povrch	k1gInSc7	povrch
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
skončila	skončit	k5eAaPmAgFnS	skončit
primární	primární	k2eAgFnSc1d1	primární
fáze	fáze	k1gFnSc1	fáze
mise	mise	k1gFnSc1	mise
Cassini	Cassin	k2eAgMnPc1d1	Cassin
a	a	k8xC	a
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
prodloužení	prodloužení	k1gNnSc3	prodloužení
mise	mise	k1gFnSc2	mise
<g/>
,	,	kIx,	,
Enceladus	Enceladus	k1gInSc1	Enceladus
byl	být	k5eAaImAgInS	být
stále	stále	k6eAd1	stále
vědecky	vědecky	k6eAd1	vědecky
zajímavým	zajímavý	k2eAgInSc7d1	zajímavý
cílem	cíl	k1gInSc7	cíl
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
bylo	být	k5eAaImAgNnS	být
zařazeno	zařadit	k5eAaPmNgNnS	zařadit
dalších	další	k2eAgInPc2d1	další
7	[number]	k4	7
průletů	průlet	k1gInPc2	průlet
okolo	okolo	k7c2	okolo
tohoto	tento	k3xDgNnSc2	tento
tělesa	těleso	k1gNnSc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Sonda	sonda	k1gFnSc1	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
poskytla	poskytnout	k5eAaPmAgFnS	poskytnout
množství	množství	k1gNnSc2	množství
důkazů	důkaz	k1gInPc2	důkaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
Enceladus	Enceladus	k1gInSc1	Enceladus
má	mít	k5eAaImIp3nS	mít
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
oceán	oceán	k1gInSc1	oceán
kapalné	kapalný	k2eAgFnSc2d1	kapalná
vody	voda	k1gFnSc2	voda
se	se	k3xPyFc4	se
zdrojem	zdroj	k1gInSc7	zdroj
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
řadou	řada	k1gFnSc7	řada
anorganických	anorganický	k2eAgFnPc2d1	anorganická
i	i	k8xC	i
organických	organický	k2eAgFnPc2d1	organická
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
měsíce	měsíc	k1gInSc2	měsíc
dělá	dělat	k5eAaImIp3nS	dělat
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejvhodnějších	vhodný	k2eAgNnPc2d3	nejvhodnější
míst	místo	k1gNnPc2	místo
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
pro	pro	k7c4	pro
pátrání	pátrání	k1gNnSc4	pátrání
po	po	k7c6	po
případných	případný	k2eAgNnPc6d1	případné
prostředích	prostředí	k1gNnPc6	prostředí
umožňujících	umožňující	k2eAgInPc2d1	umožňující
existenci	existence	k1gFnSc4	existence
mimozemského	mimozemský	k2eAgInSc2d1	mimozemský
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Vodní	vodní	k2eAgInSc1d1	vodní
oceán	oceán	k1gInSc1	oceán
na	na	k7c6	na
Europě	Europa	k1gFnSc6	Europa
je	být	k5eAaImIp3nS	být
totiž	totiž	k9	totiž
skryt	skryt	k1gInSc1	skryt
pod	pod	k7c7	pod
mnohem	mnohem	k6eAd1	mnohem
tlustší	tlustý	k2eAgFnSc7d2	tlustší
vrstvou	vrstva	k1gFnSc7	vrstva
ledu	led	k1gInSc2	led
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ztěžuje	ztěžovat	k5eAaImIp3nS	ztěžovat
jeho	jeho	k3xOp3gInSc4	jeho
případný	případný	k2eAgInSc4d1	případný
průzkum	průzkum	k1gInSc4	průzkum
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
objevů	objev	k1gInPc2	objev
sondy	sonda	k1gFnSc2	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
přimělo	přimět	k5eAaPmAgNnS	přimět
vědce	vědec	k1gMnPc4	vědec
k	k	k7c3	k
předložení	předložení	k1gNnSc3	předložení
dalších	další	k2eAgInPc2d1	další
návrhů	návrh	k1gInPc2	návrh
sond	sonda	k1gFnPc2	sonda
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
by	by	kYmCp3nP	by
na	na	k7c6	na
úspěchy	úspěch	k1gInPc1	úspěch
Cassiny	Cassina	k1gFnSc2	Cassina
navázaly	navázat	k5eAaPmAgInP	navázat
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
navržených	navržený	k2eAgInPc2d1	navržený
konceptů	koncept	k1gInPc2	koncept
NASA	NASA	kA	NASA
tak	tak	k6eAd1	tak
byla	být	k5eAaImAgFnS	být
sonda	sonda	k1gFnSc1	sonda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
schopna	schopen	k2eAgFnSc1d1	schopna
nejenom	nejenom	k6eAd1	nejenom
průletu	průlet	k1gInSc3	průlet
skrz	skrz	k6eAd1	skrz
mračna	mračno	k1gNnPc4	mračno
gejzíry	gejzír	k1gInPc4	gejzír
vyvrženého	vyvržený	k2eAgInSc2d1	vyvržený
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k8xC	i
odebrání	odebrání	k1gNnSc4	odebrání
jejich	jejich	k3xOp3gInPc2	jejich
vzorků	vzorek	k1gInPc2	vzorek
s	s	k7c7	s
následným	následný	k2eAgInSc7d1	následný
návratem	návrat	k1gInSc7	návrat
zpět	zpět	k6eAd1	zpět
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
Další	další	k2eAgInSc4d1	další
návrh	návrh	k1gInSc4	návrh
(	(	kIx(	(
<g/>
pracovní	pracovní	k2eAgNnSc4d1	pracovní
označení	označení	k1gNnSc4	označení
Journey	Journea	k1gFnSc2	Journea
to	ten	k3xDgNnSc1	ten
Enceladus	Enceladus	k1gInSc1	Enceladus
and	and	k?	and
Titan	titan	k1gInSc1	titan
<g/>
)	)	kIx)	)
předpokládal	předpokládat	k5eAaImAgInS	předpokládat
průlet	průlet	k1gInSc1	průlet
skrze	skrze	k?	skrze
<g />
.	.	kIx.	.
</s>
<s>
mračno	mračno	k1gNnSc1	mračno
a	a	k8xC	a
následný	následný	k2eAgInSc1d1	následný
výzkum	výzkum	k1gInSc1	výzkum
zachyceného	zachycený	k2eAgInSc2d1	zachycený
materiálu	materiál	k1gInSc2	materiál
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
či	či	k8xC	či
přistávací	přistávací	k2eAgInSc1d1	přistávací
modul	modul	k1gInSc1	modul
(	(	kIx(	(
<g/>
Enceladus	Enceladus	k1gMnSc1	Enceladus
Explorer	Explorer	k1gMnSc1	Explorer
<g/>
)	)	kIx)	)
navržený	navržený	k2eAgMnSc1d1	navržený
německým	německý	k2eAgMnSc7d1	německý
DLR	DLR	kA	DLR
umožňující	umožňující	k2eAgInSc4d1	umožňující
výzkum	výzkum	k1gInSc4	výzkum
případné	případný	k2eAgFnSc2d1	případná
obyvatelnosti	obyvatelnost	k1gFnSc2	obyvatelnost
podpovrchového	podpovrchový	k2eAgInSc2d1	podpovrchový
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
and	and	k?	and
two	two	k?	two
astrobiology-oriented	astrobiologyriented	k1gInSc1	astrobiology-oriented
mission	mission	k1gInSc1	mission
concepts	concepts	k1gInSc4	concepts
(	(	kIx(	(
<g/>
the	the	k?	the
Enceladus	Enceladus	k1gMnSc1	Enceladus
Life	Lif	k1gFnSc2	Lif
Finder	Finder	k1gMnSc1	Finder
and	and	k?	and
Life	Life	k1gInSc1	Life
Investigation	Investigation	k1gInSc1	Investigation
For	forum	k1gNnPc2	forum
Enceladus	Enceladus	k1gInSc1	Enceladus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
vesmírná	vesmírný	k2eAgFnSc1d1	vesmírná
agentura	agentura	k1gFnSc1	agentura
ESA	eso	k1gNnSc2	eso
taktéž	taktéž	k?	taktéž
pracuje	pracovat	k5eAaImIp3nS	pracovat
na	na	k7c6	na
koncepčních	koncepční	k2eAgFnPc6d1	koncepční
studiích	studie	k1gFnPc6	studie
k	k	k7c3	k
vyslání	vyslání	k1gNnSc3	vyslání
sondy	sonda	k1gFnSc2	sonda
k	k	k7c3	k
Enceladu	Encelad	k1gInSc3	Encelad
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
kombinované	kombinovaný	k2eAgFnSc2d1	kombinovaná
mise	mise	k1gFnSc2	mise
TandEM	tandem	k1gInSc1	tandem
(	(	kIx(	(
<g/>
Titan	titan	k1gInSc1	titan
and	and	k?	and
Enceladus	Enceladus	k1gInSc1	Enceladus
Mission	Mission	k1gInSc1	Mission
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mise	mise	k1gFnSc1	mise
Titan	titan	k1gInSc1	titan
Saturn	Saturn	k1gInSc1	Saturn
System	Syst	k1gInSc7	Syst
Mission	Mission	k1gInSc1	Mission
(	(	kIx(	(
<g/>
TSSM	TSSM	kA	TSSM
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
plánovaná	plánovaný	k2eAgFnSc1d1	plánovaná
jako	jako	k8xC	jako
společná	společný	k2eAgFnSc1d1	společná
mise	mise	k1gFnSc1	mise
NASA	NASA	kA	NASA
<g/>
/	/	kIx~	/
<g/>
ESA	eso	k1gNnPc1	eso
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
průzkumu	průzkum	k1gInSc2	průzkum
Saturnových	Saturnův	k2eAgMnPc2d1	Saturnův
měsíců	měsíc	k1gInPc2	měsíc
s	s	k7c7	s
důrazem	důraz	k1gInSc7	důraz
na	na	k7c4	na
výzkum	výzkum	k1gInSc4	výzkum
Enceladu	Encelad	k1gInSc2	Encelad
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
návrh	návrh	k1gInSc1	návrh
mise	mise	k1gFnSc2	mise
TSSM	TSSM	kA	TSSM
soupeřil	soupeřit	k5eAaImAgMnS	soupeřit
o	o	k7c4	o
finance	finance	k1gFnPc4	finance
a	a	k8xC	a
tedy	tedy	k9	tedy
realizaci	realizace	k1gFnSc4	realizace
s	s	k7c7	s
návrhem	návrh	k1gInSc7	návrh
mise	mise	k1gFnSc2	mise
Europa	Europa	k1gFnSc1	Europa
Jupiter	Jupiter	k1gMnSc1	Jupiter
System	Syst	k1gInSc7	Syst
Mission	Mission	k1gInSc1	Mission
(	(	kIx(	(
<g/>
EJSM	EJSM	kA	EJSM
<g/>
)	)	kIx)	)
k	k	k7c3	k
výzkumu	výzkum	k1gInSc3	výzkum
Europy	Europa	k1gFnSc2	Europa
<g/>
,	,	kIx,	,
měsíce	měsíc	k1gInPc1	měsíc
Jupitera	Jupiter	k1gMnSc2	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2009	[number]	k4	2009
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
NASA	NASA	kA	NASA
<g/>
/	/	kIx~	/
<g/>
ESA	eso	k1gNnSc2	eso
dá	dát	k5eAaPmIp3nS	dát
přednost	přednost	k1gFnSc4	přednost
misi	mise	k1gFnSc4	mise
EJSM	EJSM	kA	EJSM
před	před	k7c7	před
misí	mise	k1gFnSc7	mise
TSSM	TSSM	kA	TSSM
<g/>
.	.	kIx.	.
</s>
