<s>
Downing	Downing	k1gInSc1	Downing
Street	Streeta	k1gFnPc2	Streeta
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
ulice	ulice	k1gFnSc1	ulice
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
Londýna	Londýn	k1gInSc2	Londýn
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
budovy	budova	k1gFnPc1	budova
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
slouží	sloužit	k5eAaImIp3nP	sloužit
již	již	k6eAd1	již
více	hodně	k6eAd2	hodně
jak	jak	k8xS	jak
dvě	dva	k4xCgNnPc1	dva
století	století	k1gNnPc2	století
jako	jako	k8xS	jako
sídlo	sídlo	k1gNnSc1	sídlo
dvou	dva	k4xCgMnPc2	dva
nejdůležitějších	důležitý	k2eAgMnPc2d3	nejdůležitější
představitelů	představitel	k1gMnPc2	představitel
vlády	vláda	k1gFnSc2	vláda
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
–	–	k?	–
předsedy	předseda	k1gMnPc4	předseda
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
ministra	ministr	k1gMnSc2	ministr
financí	finance	k1gFnPc2	finance
(	(	kIx(	(
<g/>
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
britské	britský	k2eAgFnSc3d1	britská
historii	historie	k1gFnSc3	historie
se	se	k3xPyFc4	se
také	také	k9	také
pro	pro	k7c4	pro
tyto	tento	k3xDgFnPc4	tento
funkce	funkce	k1gFnPc4	funkce
používalo	používat	k5eAaImAgNnS	používat
označení	označení	k1gNnSc1	označení
První	první	k4xOgFnSc1	první
lord	lord	k1gMnSc1	lord
strážce	strážce	k1gMnSc2	strážce
pokladu	poklad	k1gInSc2	poklad
a	a	k8xC	a
Druhý	druhý	k4xOgMnSc1	druhý
lord	lord	k1gMnSc1	lord
strážce	strážce	k1gMnSc1	strážce
pokladu	poklást	k5eAaPmIp1nS	poklást
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnSc7d3	nejznámější
adresou	adresa	k1gFnSc7	adresa
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
ulici	ulice	k1gFnSc6	ulice
je	být	k5eAaImIp3nS	být
Downing	Downing	k1gInSc4	Downing
Street	Streeta	k1gFnPc2	Streeta
číslo	číslo	k1gNnSc1	číslo
10	[number]	k4	10
–	–	k?	–
oficiální	oficiální	k2eAgNnSc4d1	oficiální
sídlo	sídlo	k1gNnSc4	sídlo
britského	britský	k2eAgMnSc2d1	britský
premiéra	premiér	k1gMnSc2	premiér
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
úřad	úřad	k1gInSc4	úřad
předsedy	předseda	k1gMnSc2	předseda
vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
také	také	k9	také
občas	občas	k6eAd1	občas
používá	používat	k5eAaImIp3nS	používat
pojem	pojem	k1gInSc1	pojem
Downing	Downing	k1gInSc1	Downing
Street	Street	k1gInSc1	Street
nebo	nebo	k8xC	nebo
Number	Number	k1gInSc1	Number
10	[number]	k4	10
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
pro	pro	k7c4	pro
ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
financí	finance	k1gFnPc2	finance
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pojem	pojem	k1gInSc1	pojem
Number	Number	k1gInSc1	Number
11	[number]	k4	11
<g/>
.	.	kIx.	.
</s>
<s>
Downing	Downing	k1gInSc1	Downing
Street	Street	k1gInSc1	Street
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
Whitehallu	Whitehall	k1gInSc6	Whitehall
<g/>
,	,	kIx,	,
nedaleko	daleko	k6eNd1	daleko
od	od	k7c2	od
Parlamentu	parlament	k1gInSc2	parlament
sídlícího	sídlící	k2eAgInSc2d1	sídlící
ve	v	k7c6	v
Westminsterském	Westminsterský	k2eAgInSc6d1	Westminsterský
paláci	palác	k1gInSc6	palác
a	a	k8xC	a
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
pozemků	pozemek	k1gInPc2	pozemek
obklopujících	obklopující	k2eAgInPc2d1	obklopující
Buckinghamský	buckinghamský	k2eAgInSc4d1	buckinghamský
palác	palác	k1gInSc4	palác
<g/>
.	.	kIx.	.
</s>
<s>
Ulice	ulice	k1gFnSc1	ulice
byla	být	k5eAaImAgFnS	být
vybudována	vybudovat	k5eAaPmNgFnS	vybudovat
a	a	k8xC	a
pojmenována	pojmenovat	k5eAaPmNgFnS	pojmenovat
po	po	k7c6	po
siru	sir	k1gMnSc6	sir
Georgeovi	Georgeus	k1gMnSc6	Georgeus
Downingovi	Downing	k1gMnSc6	Downing
(	(	kIx(	(
<g/>
1632	[number]	k4	1632
<g/>
–	–	k?	–
<g/>
1689	[number]	k4	1689
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Downing	Downing	k1gInSc4	Downing
byl	být	k5eAaImAgMnS	být
voják	voják	k1gMnSc1	voják
sloužící	sloužící	k1gMnSc1	sloužící
pod	pod	k7c7	pod
Oliverem	Oliver	k1gMnSc7	Oliver
Cromwellem	Cromwell	k1gMnSc7	Cromwell
a	a	k8xC	a
Karlem	Karel	k1gMnSc7	Karel
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
služby	služba	k1gFnPc4	služba
králi	král	k1gMnSc3	král
byl	být	k5eAaImAgInS	být
odměněn	odměněn	k2eAgInSc1d1	odměněn
pozemkem	pozemek	k1gInSc7	pozemek
sousedícím	sousedící	k2eAgMnSc7d1	sousedící
s	s	k7c7	s
St.	st.	kA	st.
James	James	k1gInSc1	James
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Parkem	park	k1gInSc7	park
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
ulice	ulice	k1gFnSc2	ulice
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
sídla	sídlo	k1gNnPc1	sídlo
premiéra	premiér	k1gMnSc2	premiér
<g/>
,	,	kIx,	,
ministra	ministr	k1gMnSc2	ministr
financí	finance	k1gFnPc2	finance
a	a	k8xC	a
úřadu	úřad	k1gInSc2	úřad
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
ulice	ulice	k1gFnSc2	ulice
vyplňují	vyplňovat	k5eAaImIp3nP	vyplňovat
budovy	budova	k1gFnPc1	budova
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
,	,	kIx,	,
vybudované	vybudovaný	k2eAgNnSc1d1	vybudované
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Plány	plán	k1gInPc1	plán
z	z	k7c2	z
50	[number]	k4	50
<g/>
.	.	kIx.	.
a	a	k8xC	a
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
na	na	k7c4	na
demolici	demolice	k1gFnSc4	demolice
těchto	tento	k3xDgInPc2	tento
domů	dům	k1gInPc2	dům
a	a	k8xC	a
vybudování	vybudování	k1gNnSc4	vybudování
staveb	stavba	k1gFnPc2	stavba
modernějšího	moderní	k2eAgInSc2d2	modernější
charakteru	charakter	k1gInSc2	charakter
nebyly	být	k5eNaImAgFnP	být
realizovány	realizovat	k5eAaBmNgFnP	realizovat
<g/>
.	.	kIx.	.
</s>
<s>
Downing	Downing	k1gInSc1	Downing
Street	Streeta	k1gFnPc2	Streeta
číslo	číslo	k1gNnSc1	číslo
9	[number]	k4	9
–	–	k?	–
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
sídlo	sídlo	k1gNnSc1	sídlo
úřadu	úřad	k1gInSc2	úřad
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Downing	Downing	k1gInSc1	Downing
Street	Streeta	k1gFnPc2	Streeta
číslo	číslo	k1gNnSc1	číslo
10	[number]	k4	10
–	–	k?	–
oficiální	oficiální	k2eAgNnSc4d1	oficiální
sídlo	sídlo	k1gNnSc4	sídlo
britského	britský	k2eAgMnSc2d1	britský
premiéra	premiér	k1gMnSc2	premiér
<g/>
.	.	kIx.	.
</s>
<s>
Downing	Downing	k1gInSc1	Downing
Street	Streeta	k1gFnPc2	Streeta
číslo	číslo	k1gNnSc1	číslo
11	[number]	k4	11
–	–	k?	–
oficiální	oficiální	k2eAgNnSc4d1	oficiální
sídlo	sídlo	k1gNnSc4	sídlo
ministra	ministr	k1gMnSc4	ministr
financí	finance	k1gFnPc2	finance
<g/>
.	.	kIx.	.
</s>
<s>
Downing	Downing	k1gInSc1	Downing
Street	Streeta	k1gFnPc2	Streeta
číslo	číslo	k1gNnSc1	číslo
12	[number]	k4	12
–	–	k?	–
dříve	dříve	k6eAd2	dříve
sídlo	sídlo	k1gNnSc1	sídlo
úřadu	úřad	k1gInSc2	úřad
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
sídlo	sídlo	k1gNnSc4	sídlo
tiskového	tiskový	k2eAgMnSc2d1	tiskový
<g/>
,	,	kIx,	,
komunikačního	komunikační	k2eAgMnSc2d1	komunikační
<g/>
,	,	kIx,	,
informačního	informační	k2eAgNnSc2d1	informační
a	a	k8xC	a
výzkumného	výzkumný	k2eAgNnSc2d1	výzkumné
oddělení	oddělení	k1gNnSc2	oddělení
předsedy	předseda	k1gMnSc2	předseda
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
doby	doba	k1gFnSc2	doba
používali	používat	k5eAaImAgMnP	používat
jednotliví	jednotlivý	k2eAgMnPc1d1	jednotlivý
premiéři	premiér	k1gMnPc1	premiér
a	a	k8xC	a
ministři	ministr	k1gMnPc1	ministr
financí	finance	k1gFnPc2	finance
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
domy	dům	k1gInPc1	dům
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
to	ten	k3xDgNnSc4	ten
považovali	považovat	k5eAaImAgMnP	považovat
za	za	k7c4	za
nutné	nutný	k2eAgNnSc4d1	nutné
<g/>
.	.	kIx.	.
</s>
<s>
William	William	k6eAd1	William
Gladstone	Gladston	k1gInSc5	Gladston
<g/>
,	,	kIx,	,
v	v	k7c6	v
období	období	k1gNnSc6	období
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
několikrát	několikrát	k6eAd1	několikrát
v	v	k7c6	v
úřadu	úřad	k1gInSc6	úřad
<g/>
,	,	kIx,	,
obýval	obývat	k5eAaImAgMnS	obývat
domy	dům	k1gInPc4	dům
č.	č.	k?	č.
10	[number]	k4	10
<g/>
,	,	kIx,	,
č.	č.	k?	č.
11	[number]	k4	11
i	i	k8xC	i
č.	č.	k?	č.
12	[number]	k4	12
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
byl	být	k5eAaImAgInS	být
premiérem	premiér	k1gMnSc7	premiér
i	i	k8xC	i
ministrem	ministr	k1gMnSc7	ministr
financí	finance	k1gFnPc2	finance
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
Tony	Tony	k1gMnSc1	Tony
Blair	Blair	k1gMnSc1	Blair
<g/>
,	,	kIx,	,
ženatý	ženatý	k2eAgMnSc1d1	ženatý
premiér	premiér	k1gMnSc1	premiér
se	s	k7c7	s
třemi	tři	k4xCgFnPc7	tři
dětmi	dítě	k1gFnPc7	dítě
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
bydlí	bydlet	k5eAaImIp3nP	bydlet
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
<g/>
,	,	kIx,	,
obsadil	obsadit	k5eAaPmAgInS	obsadit
dům	dům	k1gInSc1	dům
číslo	číslo	k1gNnSc1	číslo
11	[number]	k4	11
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
prostornější	prostorný	k2eAgInSc1d2	prostornější
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jej	on	k3xPp3gInSc4	on
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
postu	post	k1gInSc6	post
vystřídal	vystřídat	k5eAaPmAgMnS	vystřídat
Gordon	Gordon	k1gMnSc1	Gordon
Brown	Brown	k1gMnSc1	Brown
<g/>
,	,	kIx,	,
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
svobodný	svobodný	k2eAgMnSc1d1	svobodný
ministr	ministr	k1gMnSc1	ministr
financí	finance	k1gFnPc2	finance
<g/>
,	,	kIx,	,
bydlící	bydlící	k2eAgMnPc1d1	bydlící
v	v	k7c6	v
domě	dům	k1gInSc6	dům
číslo	číslo	k1gNnSc1	číslo
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
byly	být	k5eAaImAgFnP	být
u	u	k7c2	u
vstupu	vstup	k1gInSc2	vstup
do	do	k7c2	do
Downing	Downing	k1gInSc1	Downing
Street	Street	k1gMnSc1	Street
instalovány	instalován	k2eAgFnPc4d1	instalována
kovové	kovový	k2eAgFnPc4d1	kovová
vstupní	vstupní	k2eAgFnPc4d1	vstupní
brány	brána	k1gFnPc4	brána
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
chránit	chránit	k5eAaImF	chránit
předsedu	předseda	k1gMnSc4	předseda
vlády	vláda	k1gFnSc2	vláda
(	(	kIx(	(
<g/>
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
Margaret	Margareta	k1gFnPc2	Margareta
Thatcherovou	Thatcherová	k1gFnSc7	Thatcherová
<g/>
)	)	kIx)	)
před	před	k7c7	před
teroristickými	teroristický	k2eAgInPc7d1	teroristický
útoky	útok	k1gInPc7	útok
IRA	Ir	k1gMnSc2	Ir
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
ulice	ulice	k1gFnSc1	ulice
přístupná	přístupný	k2eAgFnSc1d1	přístupná
veřejnosti	veřejnost	k1gFnSc3	veřejnost
a	a	k8xC	a
lidé	člověk	k1gMnPc1	člověk
si	se	k3xPyFc3	se
mohli	moct	k5eAaImAgMnP	moct
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
St.	st.	kA	st.
James	Jamesa	k1gFnPc2	Jamesa
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Parku	park	k1gInSc6	park
zkracovat	zkracovat	k5eAaImF	zkracovat
průchodem	průchod	k1gInSc7	průchod
kolem	kolem	k7c2	kolem
sídla	sídlo	k1gNnSc2	sídlo
britského	britský	k2eAgMnSc2d1	britský
premiéra	premiér	k1gMnSc2	premiér
<g/>
.	.	kIx.	.
</s>
<s>
Dům	dům	k1gInSc1	dům
číslo	číslo	k1gNnSc1	číslo
10	[number]	k4	10
byl	být	k5eAaImAgMnS	být
sídlem	sídlo	k1gNnSc7	sídlo
britského	britský	k2eAgMnSc2d1	britský
premiéra	premiér	k1gMnSc2	premiér
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
král	král	k1gMnSc1	král
Jiří	Jiří	k1gMnSc1	Jiří
II	II	kA	II
<g/>
.	.	kIx.	.
daroval	darovat	k5eAaPmAgMnS	darovat
tento	tento	k3xDgInSc4	tento
dům	dům	k1gInSc4	dům
Robertu	Roberta	k1gFnSc4	Roberta
Walpole	Walpole	k1gFnSc2	Walpole
za	za	k7c4	za
jeho	jeho	k3xOp3gFnPc4	jeho
služby	služba	k1gFnPc4	služba
vlasti	vlast	k1gFnSc2	vlast
a	a	k8xC	a
trůnu	trůn	k1gInSc2	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Walpole	Walpole	k1gFnPc4	Walpole
dar	dar	k1gInSc1	dar
přijal	přijmout	k5eAaPmAgInS	přijmout
s	s	k7c7	s
podmínkou	podmínka	k1gFnSc7	podmínka
<g/>
,	,	kIx,	,
že	že	k8xS	že
dům	dům	k1gInSc1	dům
bude	být	k5eAaImBp3nS	být
sloužit	sloužit	k5eAaImF	sloužit
premiérovi	premiér	k1gMnSc3	premiér
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bude	být	k5eAaImBp3nS	být
v	v	k7c6	v
úřadu	úřad	k1gInSc6	úřad
a	a	k8xC	a
ne	ne	k9	ne
jen	jen	k6eAd1	jen
jemu	on	k3xPp3gNnSc3	on
osobně	osobně	k6eAd1	osobně
<g/>
.	.	kIx.	.
</s>
<s>
Dům	dům	k1gInSc1	dům
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1731	[number]	k4	1731
používán	používat	k5eAaImNgInS	používat
jako	jako	k8xC	jako
sídlo	sídlo	k1gNnSc1	sídlo
britského	britský	k2eAgMnSc2d1	britský
premiéra	premiér	k1gMnSc2	premiér
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
dům	dům	k1gInSc1	dům
číslo	číslo	k1gNnSc1	číslo
10	[number]	k4	10
zdál	zdát	k5eAaImAgMnS	zdát
být	být	k5eAaImF	být
příliš	příliš	k6eAd1	příliš
malý	malý	k2eAgInSc4d1	malý
<g/>
,	,	kIx,	,
nezajímavý	zajímavý	k2eNgInSc4d1	nezajímavý
a	a	k8xC	a
neodpovídající	odpovídající	k2eNgInSc4d1	neodpovídající
standardu	standard	k1gInSc3	standard
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
požadovali	požadovat	k5eAaImAgMnP	požadovat
někteří	některý	k3yIgMnPc1	některý
premiéři	premiér	k1gMnPc1	premiér
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
nejznámějším	známý	k2eAgInPc3d3	nejznámější
byl	být	k5eAaImAgMnS	být
Artur	Artur	k1gMnSc1	Artur
Wellesley	Welleslea	k1gFnSc2	Welleslea
<g/>
,	,	kIx,	,
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Wellingtonu	Wellington	k1gInSc2	Wellington
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
bydleli	bydlet	k5eAaImAgMnP	bydlet
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
rezidencích	rezidence	k1gFnPc6	rezidence
a	a	k8xC	a
tento	tento	k3xDgInSc4	tento
dům	dům	k1gInSc4	dům
nepoužívali	používat	k5eNaImAgMnP	používat
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
Ramsay	Ramsaa	k1gFnPc1	Ramsaa
MacDonald	Macdonald	k1gMnSc1	Macdonald
<g/>
,	,	kIx,	,
premiér	premiér	k1gMnSc1	premiér
zvolený	zvolený	k2eAgMnSc1d1	zvolený
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
za	za	k7c4	za
labouristickou	labouristický	k2eAgFnSc4d1	labouristická
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
řešil	řešit	k5eAaImAgInS	řešit
opačný	opačný	k2eAgInSc1d1	opačný
problém	problém	k1gInSc1	problém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
do	do	k7c2	do
Downing	Downing	k1gInSc4	Downing
Street	Street	k1gInSc1	Street
číslo	číslo	k1gNnSc1	číslo
10	[number]	k4	10
nastěhoval	nastěhovat	k5eAaPmAgMnS	nastěhovat
jako	jako	k9	jako
premiér	premiér	k1gMnSc1	premiér
a	a	k8xC	a
kdy	kdy	k6eAd1	kdy
premiéři	premiér	k1gMnPc1	premiér
dostávali	dostávat	k5eAaImAgMnP	dostávat
pouze	pouze	k6eAd1	pouze
minimální	minimální	k2eAgFnSc4d1	minimální
mzdu	mzda	k1gFnSc4	mzda
<g/>
,	,	kIx,	,
musel	muset	k5eAaImAgMnS	muset
přispívat	přispívat	k5eAaImF	přispívat
ze	z	k7c2	z
svých	svůj	k3xOyFgInPc2	svůj
soukromých	soukromý	k2eAgInPc2d1	soukromý
prostředků	prostředek	k1gInPc2	prostředek
na	na	k7c4	na
vybavení	vybavení	k1gNnSc4	vybavení
domu	dům	k1gInSc2	dům
nábytkem	nábytek	k1gInSc7	nábytek
a	a	k8xC	a
na	na	k7c4	na
plat	plat	k1gInSc4	plat
služebnictva	služebnictvo	k1gNnSc2	služebnictvo
z	z	k7c2	z
nichž	jenž	k3xRgNnPc2	jenž
někteří	některý	k3yIgMnPc1	některý
i	i	k9	i
při	při	k7c6	při
svém	svůj	k3xOyFgInSc6	svůj
nízkém	nízký	k2eAgInSc6d1	nízký
platu	plat	k1gInSc6	plat
vydělávali	vydělávat	k5eAaImAgMnP	vydělávat
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
on	on	k3xPp3gMnSc1	on
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
40	[number]	k4	40
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
ekonomických	ekonomický	k2eAgFnPc2d1	ekonomická
změn	změna	k1gFnPc2	změna
změnil	změnit	k5eAaPmAgInS	změnit
styl	styl	k1gInSc1	styl
využívání	využívání	k1gNnSc2	využívání
Downig	Downig	k1gMnSc1	Downig
Street	Street	k1gMnSc1	Street
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
původní	původní	k2eAgFnSc2d1	původní
rozlehlé	rozlehlý	k2eAgFnSc2d1	rozlehlá
rezidence	rezidence	k1gFnSc2	rezidence
se	s	k7c7	s
služebnictvem	služebnictvo	k1gNnSc7	služebnictvo
byl	být	k5eAaImAgInS	být
upraven	upraven	k2eAgInSc1d1	upraven
na	na	k7c4	na
kanceláře	kancelář	k1gFnPc4	kancelář
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
pracovna	pracovna	k1gFnSc1	pracovna
a	a	k8xC	a
byt	byt	k1gInSc1	byt
premiéra	premiér	k1gMnSc2	premiér
byly	být	k5eAaImAgInP	být
vytvořeny	vytvořit	k5eAaPmNgInP	vytvořit
z	z	k7c2	z
pokojů	pokoj	k1gInPc2	pokoj
pro	pro	k7c4	pro
služebnictvo	služebnictvo	k1gNnSc4	služebnictvo
na	na	k7c6	na
poschodí	poschodí	k1gNnSc6	poschodí
<g/>
.	.	kIx.	.
</s>
<s>
Stísněnost	stísněnost	k1gFnSc1	stísněnost
bytu	byt	k1gInSc2	byt
vytvořeného	vytvořený	k2eAgInSc2d1	vytvořený
z	z	k7c2	z
pokojů	pokoj	k1gInPc2	pokoj
pro	pro	k7c4	pro
služebnictvo	služebnictvo	k1gNnSc4	služebnictvo
vedla	vést	k5eAaImAgFnS	vést
některé	některý	k3yIgFnPc4	některý
premiéry	premiéra	k1gFnPc4	premiéra
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
tajně	tajně	k6eAd1	tajně
bydleli	bydlet	k5eAaImAgMnP	bydlet
jinde	jinde	k6eAd1	jinde
i	i	k9	i
když	když	k8xS	když
tato	tento	k3xDgFnSc1	tento
skutečnost	skutečnost	k1gFnSc1	skutečnost
nebyla	být	k5eNaImAgFnS	být
z	z	k7c2	z
bezpečnostních	bezpečnostní	k2eAgInPc2d1	bezpečnostní
důvodů	důvod	k1gInPc2	důvod
zveřejňována	zveřejňován	k2eAgFnSc1d1	zveřejňována
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
vchodem	vchod	k1gInSc7	vchod
do	do	k7c2	do
domu	dům	k1gInSc2	dům
číslo	číslo	k1gNnSc1	číslo
10	[number]	k4	10
tradičně	tradičně	k6eAd1	tradičně
stojí	stát	k5eAaImIp3nS	stát
policista	policista	k1gMnSc1	policista
<g/>
.	.	kIx.	.
</s>
<s>
Vstupní	vstupní	k2eAgFnPc4d1	vstupní
dveře	dveře	k1gFnPc4	dveře
lze	lze	k6eAd1	lze
otevřít	otevřít	k5eAaPmF	otevřít
pouze	pouze	k6eAd1	pouze
zevnitř	zevnitř	k6eAd1	zevnitř
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
obou	dva	k4xCgInPc6	dva
koncích	konec	k1gInPc6	konec
Downing	Downing	k1gInSc4	Downing
Street	Streeta	k1gFnPc2	Streeta
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
době	doba	k1gFnSc6	doba
úřadování	úřadování	k1gNnSc2	úřadování
Margaret	Margareta	k1gFnPc2	Margareta
Thatcherové	Thatcherová	k1gFnSc2	Thatcherová
instalovány	instalován	k2eAgFnPc4d1	instalována
kovové	kovový	k2eAgFnPc4d1	kovová
brány	brána	k1gFnPc4	brána
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
ochrana	ochrana	k1gFnSc1	ochrana
před	před	k7c7	před
útoky	útok	k1gInPc7	útok
teroristů	terorista	k1gMnPc2	terorista
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
IRA	Ir	k1gMnSc2	Ir
podnikla	podniknout	k5eAaPmAgFnS	podniknout
úspěšný	úspěšný	k2eAgInSc4d1	úspěšný
atentát	atentát	k1gInSc4	atentát
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yIgNnSc6	který
vybuchla	vybuchnout	k5eAaPmAgFnS	vybuchnout
u	u	k7c2	u
zadní	zadní	k2eAgFnSc2d1	zadní
stěny	stěna	k1gFnSc2	stěna
domu	dům	k1gInSc2	dům
bomba	bomba	k1gFnSc1	bomba
v	v	k7c6	v
dodávkovém	dodávkový	k2eAgNnSc6d1	dodávkové
autě	auto	k1gNnSc6	auto
zaparkovaném	zaparkovaný	k2eAgNnSc6d1	zaparkované
na	na	k7c4	na
Whitehall	Whitehall	k1gInSc4	Whitehall
<g/>
.	.	kIx.	.
</s>
<s>
Bomba	bomba	k1gFnSc1	bomba
vybuchla	vybuchnout	k5eAaPmAgFnS	vybuchnout
v	v	k7c6	v
době	doba	k1gFnSc6	doba
kdy	kdy	k6eAd1	kdy
premiér	premiér	k1gMnSc1	premiér
John	John	k1gMnSc1	John
Major	major	k1gMnSc1	major
vedl	vést	k5eAaImAgMnS	vést
jednání	jednání	k1gNnSc4	jednání
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
opravy	oprava	k1gFnSc2	oprava
budovy	budova	k1gFnSc2	budova
bylo	být	k5eAaImAgNnS	být
sídlo	sídlo	k1gNnSc4	sídlo
premiéra	premiéra	k1gFnSc1	premiéra
přemístěno	přemístěn	k2eAgNnSc4d1	přemístěno
do	do	k7c2	do
nedalekého	daleký	k2eNgInSc2d1	nedaleký
Domu	dům	k1gInSc2	dům
Admirality	admiralita	k1gFnSc2	admiralita
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
jako	jako	k8xC	jako
záložní	záložní	k2eAgNnSc1d1	záložní
sídlo	sídlo	k1gNnSc1	sídlo
premiéra	premiér	k1gMnSc2	premiér
v	v	k7c6	v
době	doba	k1gFnSc6	doba
oprav	oprava	k1gFnPc2	oprava
nebo	nebo	k8xC	nebo
z	z	k7c2	z
bezpečnostních	bezpečnostní	k2eAgInPc2d1	bezpečnostní
důvodů	důvod	k1gInPc2	důvod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
dům	dům	k1gInSc1	dům
číslo	číslo	k1gNnSc1	číslo
10	[number]	k4	10
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
hrozí	hrozit	k5eAaImIp3nS	hrozit
zhroucením	zhroucení	k1gNnSc7	zhroucení
a	a	k8xC	a
nutně	nutně	k6eAd1	nutně
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
opravu	oprava	k1gFnSc4	oprava
(	(	kIx(	(
<g/>
pilíře	pilíř	k1gInPc1	pilíř
v	v	k7c6	v
kanceláři	kancelář	k1gFnSc6	kancelář
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
podepírají	podepírat	k5eAaImIp3nP	podepírat
strop	strop	k1gInSc4	strop
<g/>
,	,	kIx,	,
držely	držet	k5eAaImAgFnP	držet
jen	jen	k9	jen
na	na	k7c4	na
200	[number]	k4	200
let	léto	k1gNnPc2	léto
staré	stará	k1gFnSc2	stará
omítce	omítka	k1gFnSc3	omítka
<g/>
,	,	kIx,	,
když	když	k8xS	když
dřevěné	dřevěný	k2eAgNnSc1d1	dřevěné
jádro	jádro	k1gNnSc1	jádro
pilířů	pilíř	k1gInPc2	pilíř
bylo	být	k5eAaImAgNnS	být
provrtané	provrtaný	k2eAgNnSc1d1	provrtané
červotoči	červotoč	k1gMnPc1	červotoč
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
při	při	k7c6	při
opravě	oprava	k1gFnSc6	oprava
zachovat	zachovat	k5eAaPmF	zachovat
vnější	vnější	k2eAgInSc4d1	vnější
vzhled	vzhled	k1gInSc4	vzhled
obnovením	obnovení	k1gNnSc7	obnovení
fasády	fasáda	k1gFnPc1	fasáda
a	a	k8xC	a
interiér	interiér	k1gInSc1	interiér
domu	dům	k1gInSc2	dům
vybourat	vybourat	k5eAaPmF	vybourat
a	a	k8xC	a
obnovit	obnovit	k5eAaPmF	obnovit
podle	podle	k7c2	podle
původního	původní	k2eAgInSc2d1	původní
stavu	stav	k1gInSc2	stav
<g/>
,	,	kIx,	,
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
moderních	moderní	k2eAgInPc2d1	moderní
ocelových	ocelový	k2eAgInPc2d1	ocelový
a	a	k8xC	a
betonových	betonový	k2eAgInPc2d1	betonový
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
obnově	obnova	k1gFnSc6	obnova
fasády	fasáda	k1gFnSc2	fasáda
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
původní	původní	k2eAgFnSc1d1	původní
barva	barva	k1gFnSc1	barva
omítky	omítka	k1gFnSc2	omítka
byla	být	k5eAaImAgFnS	být
žlutá	žlutý	k2eAgFnSc1d1	žlutá
a	a	k8xC	a
dvousetletým	dvousetletý	k2eAgNnPc3d1	dvousetleté
působením	působení	k1gNnPc3	působení
znečištění	znečištění	k1gNnSc2	znečištění
zčernala	zčernat	k5eAaPmAgFnS	zčernat
<g/>
.	.	kIx.	.
</s>
<s>
Barva	barva	k1gFnSc1	barva
obnovené	obnovený	k2eAgFnSc2d1	obnovená
fasády	fasáda	k1gFnSc2	fasáda
<g/>
,	,	kIx,	,
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
zachování	zachování	k1gNnSc4	zachování
známého	známý	k2eAgInSc2d1	známý
vzhledu	vzhled	k1gInSc2	vzhled
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
upravena	upravit	k5eAaPmNgFnS	upravit
na	na	k7c4	na
šedavou	šedavý	k2eAgFnSc4d1	šedavá
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Downing	Downing	k1gInSc1	Downing
Street	Street	k1gInSc4	Street
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
http://www.number-10.gov.uk/	[url]	k4	http://www.number-10.gov.uk/
http://www.downingstreetsays.org/	[url]	k?	http://www.downingstreetsays.org/
</s>
