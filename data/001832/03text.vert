<s>
Balliol	Balliol	k1gInSc1	Balliol
College	Colleg	k1gInSc2	Colleg
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
roku	rok	k1gInSc2	rok
1263	[number]	k4	1263
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
zakládajících	zakládající	k2eAgFnPc2d1	zakládající
kolejí	kolej	k1gFnPc2	kolej
Oxfordské	oxfordský	k2eAgFnSc2d1	Oxfordská
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
ale	ale	k9	ale
založili	založit	k5eAaPmAgMnP	založit
skotští	skotský	k2eAgMnPc1d1	skotský
akademici	akademik	k1gMnPc1	akademik
<g/>
.	.	kIx.	.
</s>
<s>
Kolej	kolej	k1gFnSc1	kolej
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1263	[number]	k4	1263
Johnem	John	k1gMnSc7	John
Balliolem	Balliol	k1gMnSc7	Balliol
za	za	k7c2	za
podpory	podpora	k1gFnSc2	podpora
biskupa	biskup	k1gMnSc2	biskup
durhamského	durhamský	k2eAgMnSc2d1	durhamský
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
roku	rok	k1gInSc2	rok
1268	[number]	k4	1268
jeho	jeho	k3xOp3gFnSc4	jeho
vdova	vdova	k1gFnSc1	vdova
přijala	přijmout	k5eAaPmAgFnS	přijmout
opatření	opatření	k1gNnSc4	opatření
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
tato	tento	k3xDgFnSc1	tento
kolej	kolej	k1gFnSc1	kolej
mohla	moct	k5eAaImAgFnS	moct
existovat	existovat	k5eAaImF	existovat
i	i	k9	i
nadále	nadále	k6eAd1	nadále
<g/>
.	.	kIx.	.
</s>
<s>
Poskytla	poskytnout	k5eAaPmAgFnS	poskytnout
koleji	kolej	k1gFnSc3	kolej
kapitál	kapitála	k1gFnPc2	kapitála
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1282	[number]	k4	1282
formulovala	formulovat	k5eAaImAgFnS	formulovat
kolejní	kolejní	k2eAgFnPc4d1	kolejní
stanovy	stanova	k1gFnPc4	stanova
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnPc1	její
absolventi	absolvent	k1gMnPc1	absolvent
se	se	k3xPyFc4	se
často	často	k6eAd1	často
začlenili	začlenit	k5eAaPmAgMnP	začlenit
do	do	k7c2	do
společnosti	společnost	k1gFnSc2	společnost
jako	jako	k8xC	jako
aktivní	aktivní	k2eAgMnPc1d1	aktivní
politikové	politik	k1gMnPc1	politik
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
škole	škola	k1gFnSc6	škola
studovali	studovat	k5eAaImAgMnP	studovat
tři	tři	k4xCgMnPc1	tři
premiéři	premiér	k1gMnPc1	premiér
<g/>
;	;	kIx,	;
H.	H.	kA	H.
H.	H.	kA	H.
Asquith	Asquith	k1gMnSc1	Asquith
<g/>
,	,	kIx,	,
Harold	Harold	k1gMnSc1	Harold
Macmillan	Macmillan	k1gMnSc1	Macmillan
a	a	k8xC	a
Edward	Edward	k1gMnSc1	Edward
Heath	Heath	k1gMnSc1	Heath
<g/>
,	,	kIx,	,
a	a	k8xC	a
jejím	její	k3xOp3gNnSc7	její
možná	možná	k9	možná
nejznámějším	známý	k2eAgMnSc7d3	nejznámější
absolventem	absolvent	k1gMnSc7	absolvent
je	být	k5eAaImIp3nS	být
zakladatel	zakladatel	k1gMnSc1	zakladatel
moderní	moderní	k2eAgFnSc2d1	moderní
ekonomie	ekonomie	k1gFnSc2	ekonomie
Adam	Adam	k1gMnSc1	Adam
Smith	Smith	k1gMnSc1	Smith
<g/>
.	.	kIx.	.
</s>
<s>
Známým	známý	k2eAgMnSc7d1	známý
fiktivním	fiktivní	k2eAgMnSc7d1	fiktivní
absolventem	absolvent	k1gMnSc7	absolvent
je	být	k5eAaImIp3nS	být
sir	sir	k1gMnSc1	sir
Humphrey	Humphre	k2eAgInPc4d1	Humphre
Appleby	Appleb	k1gInPc4	Appleb
<g/>
.	.	kIx.	.
</s>
