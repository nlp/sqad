<s>
Balliol	Balliol	k1gInSc1	Balliol
College	Colleg	k1gInSc2	Colleg
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
roku	rok	k1gInSc2	rok
1263	[number]	k4	1263
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
zakládajících	zakládající	k2eAgFnPc2d1	zakládající
kolejí	kolej	k1gFnPc2	kolej
Oxfordské	oxfordský	k2eAgFnSc2d1	Oxfordská
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
ale	ale	k9	ale
založili	založit	k5eAaPmAgMnP	založit
skotští	skotský	k2eAgMnPc1d1	skotský
akademici	akademik	k1gMnPc1	akademik
<g/>
.	.	kIx.	.
</s>
<s>
Kolej	kolej	k1gFnSc1	kolej
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1263	[number]	k4	1263
Johnem	John	k1gMnSc7	John
Balliolem	Balliol	k1gMnSc7	Balliol
za	za	k7c2	za
podpory	podpora	k1gFnSc2	podpora
biskupa	biskup	k1gMnSc2	biskup
durhamského	durhamský	k2eAgMnSc2d1	durhamský
<g/>
.	.	kIx.	.
</s>

