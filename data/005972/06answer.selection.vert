<s>
Znaky	znak	k1gInPc4	znak
písma	písmo	k1gNnSc2	písmo
bopomofo	bopomofo	k6eAd1	bopomofo
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
filolog	filolog	k1gMnSc1	filolog
a	a	k8xC	a
kaligraf	kaligraf	k1gMnSc1	kaligraf
Čang	Čang	k1gMnSc1	Čang
Ping-lin	Pingin	k1gInSc4	Ping-lin
zjednodušením	zjednodušení	k1gNnSc7	zjednodušení
čínských	čínský	k2eAgMnPc2d1	čínský
znaků	znak	k1gInPc2	znak
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
různých	různý	k2eAgFnPc2d1	různá
starobylých	starobylý	k2eAgFnPc2d1	starobylá
a	a	k8xC	a
zaniklých	zaniklý	k2eAgFnPc2d1	zaniklá
variant	varianta	k1gFnPc2	varianta
<g/>
.	.	kIx.	.
</s>
