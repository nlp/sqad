<p>
<s>
Čertova	čertův	k2eAgFnSc1d1	Čertova
pláň	pláň	k1gFnSc1	pláň
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Teufelsplan	Teufelsplan	k1gInSc1	Teufelsplan
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vrchol	vrchol	k1gInSc4	vrchol
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
ležící	ležící	k2eAgFnSc1d1	ležící
v	v	k7c6	v
Krkonoších	Krkonoše	k1gFnPc6	Krkonoše
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geomorfologické	geomorfologický	k2eAgNnSc4d1	Geomorfologické
zařazení	zařazení	k1gNnSc4	zařazení
==	==	k?	==
</s>
</p>
<p>
<s>
Hora	hora	k1gFnSc1	hora
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
geomorfologickém	geomorfologický	k2eAgInSc6d1	geomorfologický
celku	celek	k1gInSc6	celek
Krkonoše	Krkonoše	k1gFnPc1	Krkonoše
<g/>
,	,	kIx,	,
podcelku	podcelka	k1gMnSc4	podcelka
Krkonošské	krkonošský	k2eAgFnSc2d1	Krkonošská
rozsochy	rozsocha	k1gFnSc2	rozsocha
<g/>
,	,	kIx,	,
okrsku	okrsek	k1gInSc2	okrsek
Vilémovská	vilémovský	k2eAgFnSc1d1	Vilémovská
hornatina	hornatina	k1gFnSc1	hornatina
a	a	k8xC	a
podokrsku	podokrsko	k1gNnSc6	podokrsko
Rokytnická	rokytnický	k2eAgFnSc1d1	rokytnická
hornatina	hornatina	k1gFnSc1	hornatina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poloha	poloha	k1gFnSc1	poloha
==	==	k?	==
</s>
</p>
<p>
<s>
Čertova	čertův	k2eAgFnSc1d1	Čertova
pláň	pláň	k1gFnSc1	pláň
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
rozsochy	rozsoch	k1gInPc1	rozsoch
vybíhající	vybíhající	k2eAgInPc1d1	vybíhající
z	z	k7c2	z
Lysé	Lysé	k2eAgFnSc2d1	Lysé
hory	hora	k1gFnSc2	hora
západním	západní	k2eAgInSc7d1	západní
směrem	směr	k1gInSc7	směr
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
3,5	[number]	k4	3,5
km	km	kA	km
na	na	k7c4	na
severozápad	severozápad	k1gInSc4	severozápad
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Harrachov	Harrachov	k1gInSc1	Harrachov
<g/>
,	,	kIx,	,
přímo	přímo	k6eAd1	přímo
pod	pod	k7c7	pod
jižním	jižní	k2eAgInSc7d1	jižní
svahem	svah	k1gInSc7	svah
pak	pak	k6eAd1	pak
Rokytnice	Rokytnice	k1gFnSc1	Rokytnice
nad	nad	k7c7	nad
Jizerou	Jizera	k1gFnSc7	Jizera
<g/>
.	.	kIx.	.
</s>
<s>
Východním	východní	k2eAgInSc7d1	východní
směrem	směr	k1gInSc7	směr
od	od	k7c2	od
vrcholu	vrchol	k1gInSc2	vrchol
se	se	k3xPyFc4	se
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
asi	asi	k9	asi
700	[number]	k4	700
metrů	metr	k1gInPc2	metr
nachází	nacházet	k5eAaImIp3nS	nacházet
sedlo	sedlo	k1gNnSc1	sedlo
Ručičky	ručička	k1gFnSc2	ručička
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
ji	on	k3xPp3gFnSc4	on
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
od	od	k7c2	od
masívu	masív	k1gInSc2	masív
Lysé	Lysé	k2eAgFnSc2d1	Lysé
hory	hora	k1gFnSc2	hora
<g/>
.	.	kIx.	.
</s>
<s>
Klesání	klesání	k1gNnSc1	klesání
do	do	k7c2	do
tohoto	tento	k3xDgNnSc2	tento
sedla	sedlo	k1gNnSc2	sedlo
je	být	k5eAaImIp3nS	být
minimální	minimální	k2eAgNnSc1d1	minimální
počítané	počítaný	k2eAgNnSc1d1	počítané
v	v	k7c6	v
jednotkách	jednotka	k1gFnPc6	jednotka
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
západně	západně	k6eAd1	západně
položené	položený	k2eAgFnSc2d1	položená
Studené	Studená	k1gFnSc2	Studená
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
Čertovu	čertův	k2eAgFnSc4d1	Čertova
pláň	pláň	k1gFnSc4	pláň
sedlo	sednout	k5eAaPmAgNnS	sednout
výraznější	výrazný	k2eAgNnSc1d2	výraznější
s	s	k7c7	s
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
917	[number]	k4	917
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jižním	jižní	k2eAgInSc6d1	jižní
svahu	svah	k1gInSc6	svah
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
boční	boční	k2eAgInSc4d1	boční
vrchol	vrchol	k1gInSc4	vrchol
s	s	k7c7	s
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
858	[number]	k4	858
metrů	metr	k1gInPc2	metr
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
Kostelní	kostelní	k2eAgInSc1d1	kostelní
vrch	vrch	k1gInSc1	vrch
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgInSc1d1	severní
svah	svah	k1gInSc1	svah
je	být	k5eAaImIp3nS	být
prudký	prudký	k2eAgMnSc1d1	prudký
a	a	k8xC	a
se	s	k7c7	s
značnějším	značný	k2eAgNnSc7d2	značnější
převýšením	převýšení	k1gNnSc7	převýšení
<g/>
.	.	kIx.	.
</s>
<s>
Vrchol	vrchol	k1gInSc1	vrchol
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
území	území	k1gNnSc6	území
Krkonošského	krkonošský	k2eAgInSc2d1	krkonošský
národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgInSc1d1	jižní
svah	svah	k1gInSc1	svah
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
ochranném	ochranný	k2eAgNnSc6d1	ochranné
pásmu	pásmo	k1gNnSc6	pásmo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vodstvo	vodstvo	k1gNnSc1	vodstvo
==	==	k?	==
</s>
</p>
<p>
<s>
Studená	Studená	k1gFnSc1	Studená
spadá	spadat	k5eAaImIp3nS	spadat
do	do	k7c2	do
povodí	povodí	k1gNnSc2	povodí
Jizery	Jizera	k1gFnSc2	Jizera
<g/>
.	.	kIx.	.
</s>
<s>
Rýžovištní	rýžovištní	k2eAgInSc1d1	rýžovištní
potok	potok	k1gInSc1	potok
protékající	protékající	k2eAgInSc1d1	protékající
pod	pod	k7c7	pod
severním	severní	k2eAgInSc7d1	severní
svahem	svah	k1gInSc7	svah
je	být	k5eAaImIp3nS	být
levým	levý	k2eAgInSc7d1	levý
přítokem	přítok	k1gInSc7	přítok
Mumlavy	Mumlavy	k?	Mumlavy
<g/>
.	.	kIx.	.
</s>
<s>
Potoky	potok	k1gInPc1	potok
stékající	stékající	k2eAgInPc1d1	stékající
z	z	k7c2	z
jižního	jižní	k2eAgInSc2d1	jižní
svahu	svah	k1gInSc2	svah
jsou	být	k5eAaImIp3nP	být
pravými	pravý	k2eAgInPc7d1	pravý
přítoky	přítok	k1gInPc7	přítok
Huťského	huťský	k2eAgInSc2d1	huťský
potoka	potok	k1gInSc2	potok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vegetace	vegetace	k1gFnSc2	vegetace
==	==	k?	==
</s>
</p>
<p>
<s>
Vrcholové	vrcholový	k2eAgFnPc1d1	vrcholová
partie	partie	k1gFnPc1	partie
Studené	Studená	k1gFnSc2	Studená
jsou	být	k5eAaImIp3nP	být
souvisle	souvisle	k6eAd1	souvisle
zalesněny	zalesněn	k2eAgMnPc4d1	zalesněn
<g/>
,	,	kIx,	,
převahu	převaha	k1gFnSc4	převaha
zde	zde	k6eAd1	zde
má	mít	k5eAaImIp3nS	mít
smrk	smrk	k1gInSc1	smrk
ztepilý	ztepilý	k2eAgInSc1d1	ztepilý
<g/>
.	.	kIx.	.
</s>
<s>
Luční	luční	k2eAgFnSc1d1	luční
enkláva	enkláva	k1gFnSc1	enkláva
Hoření	hoření	k2eAgInSc4d1	hoření
domky	domek	k1gInPc4	domek
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
do	do	k7c2	do
jihovýchodního	jihovýchodní	k2eAgInSc2d1	jihovýchodní
svahu	svah	k1gInSc2	svah
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Komunikace	komunikace	k1gFnSc1	komunikace
==	==	k?	==
</s>
</p>
<p>
<s>
Přímo	přímo	k6eAd1	přímo
přes	přes	k7c4	přes
vrchol	vrchol	k1gInSc4	vrchol
je	být	k5eAaImIp3nS	být
vedena	vést	k5eAaImNgFnS	vést
asfaltová	asfaltový	k2eAgFnSc1d1	asfaltová
pro	pro	k7c4	pro
běžný	běžný	k2eAgInSc4d1	běžný
provoz	provoz	k1gInSc4	provoz
uzavřená	uzavřený	k2eAgFnSc1d1	uzavřená
komunikace	komunikace	k1gFnSc1	komunikace
z	z	k7c2	z
Dvoraček	Dvoračka	k1gFnPc2	Dvoračka
do	do	k7c2	do
Studenova	Studenův	k2eAgNnSc2d1	Studenův
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
sledována	sledován	k2eAgNnPc4d1	sledováno
modře	modro	k6eAd1	modro
značenou	značený	k2eAgFnSc7d1	značená
trasou	trasa	k1gFnSc7	trasa
1894	[number]	k4	1894
<g/>
.	.	kIx.	.
</s>
<s>
Jižním	jižní	k2eAgInSc7d1	jižní
svahem	svah	k1gInSc7	svah
přes	přes	k7c4	přes
sedlo	sedlo	k1gNnSc4	sedlo
Kostelního	kostelní	k2eAgInSc2d1	kostelní
vrchu	vrch	k1gInSc2	vrch
klesá	klesat	k5eAaImIp3nS	klesat
žlutě	žlutě	k6eAd1	žlutě
značená	značený	k2eAgFnSc1d1	značená
trasa	trasa	k1gFnSc1	trasa
7307	[number]	k4	7307
z	z	k7c2	z
Vosecké	Vosecké	k2eAgFnSc2d1	Vosecké
boudy	bouda	k1gFnSc2	bouda
do	do	k7c2	do
Rokytnice	Rokytnice	k1gFnSc2	Rokytnice
nad	nad	k7c7	nad
Jizerou	Jizera	k1gFnSc7	Jizera
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
