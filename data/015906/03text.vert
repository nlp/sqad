<s>
Křekov	Křekov	k1gInSc1
</s>
<s>
Křekov	Křekov	k1gInSc1
Knihovna	knihovna	k1gFnSc1
<g/>
,	,	kIx,
kříž	kříž	k1gInSc1
a	a	k8xC
kaplička	kaplička	k1gFnSc1
</s>
<s>
znakvlajka	znakvlajka	k1gFnSc1
Lokalita	lokalita	k1gFnSc1
Status	status	k1gInSc1
</s>
<s>
obec	obec	k1gFnSc1
LAU	LAU	kA
2	#num#	k4
(	(	kIx(
<g/>
obec	obec	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
CZ0724	CZ0724	k4
586960	#num#	k4
Pověřená	pověřený	k2eAgFnSc1d1
obec	obec	k1gFnSc1
a	a	k8xC
obec	obec	k1gFnSc1
s	s	k7c7
rozšířenou	rozšířený	k2eAgFnSc7d1
působností	působnost	k1gFnSc7
</s>
<s>
Valašské	valašský	k2eAgInPc1d1
Klobouky	Klobouky	k1gInPc1
Okres	okres	k1gInSc1
(	(	kIx(
<g/>
LAU	LAU	kA
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Zlín	Zlín	k1gInSc1
(	(	kIx(
<g/>
CZ	CZ	kA
<g/>
0	#num#	k4
<g/>
724	#num#	k4
<g/>
)	)	kIx)
Kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
NUTS	NUTS	kA
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Zlínský	zlínský	k2eAgMnSc1d1
(	(	kIx(
<g/>
CZ	CZ	kA
<g/>
0	#num#	k4
<g/>
72	#num#	k4
<g/>
)	)	kIx)
Historická	historický	k2eAgFnSc1d1
země	země	k1gFnSc1
</s>
<s>
Morava	Morava	k1gFnSc1
Zeměpisné	zeměpisný	k2eAgFnSc2d1
souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
7	#num#	k4
<g/>
′	′	k?
<g/>
46	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
17	#num#	k4
<g/>
°	°	k?
<g/>
58	#num#	k4
<g/>
′	′	k?
<g/>
4	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Základní	základní	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Počet	počet	k1gInSc4
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
185	#num#	k4
(	(	kIx(
<g/>
2021	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Rozloha	rozloha	k1gFnSc1
</s>
<s>
3,84	3,84	k4
km²	km²	k?
Katastrální	katastrální	k2eAgNnSc4d1
území	území	k1gNnSc4
</s>
<s>
Křekov	Křekov	k1gInSc1
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
384	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
PSČ	PSČ	kA
</s>
<s>
766	#num#	k4
01	#num#	k4
Počet	počet	k1gInSc1
částí	část	k1gFnPc2
obce	obec	k1gFnSc2
</s>
<s>
1	#num#	k4
Počet	počet	k1gInSc1
k.	k.	k?
ú.	ú.	k?
</s>
<s>
1	#num#	k4
Počet	počet	k1gInSc1
ZSJ	ZSJ	kA
</s>
<s>
1	#num#	k4
Kontakt	kontakt	k1gInSc1
Adresa	adresa	k1gFnSc1
obecního	obecní	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
</s>
<s>
Křekov	Křekov	k1gInSc1
5766	#num#	k4
01	#num#	k4
Valašské	valašský	k2eAgInPc1d1
Klobouky	Klobouky	k1gInPc1
oukrekov@cbox.cz	oukrekov@cbox.cza	k1gFnPc2
Starostka	starostka	k1gFnSc1
</s>
<s>
Bc.	Bc.	k?
Martina	Martina	k1gFnSc1
Burdová	Burdová	k1gFnSc1
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
<g/>
:	:	kIx,
www.krekov.cz	www.krekov.cz	k1gInSc1
</s>
<s>
Křekov	Křekov	k1gInSc1
</s>
<s>
Další	další	k2eAgInPc1d1
údaje	údaj	k1gInPc1
Kód	kód	k1gInSc4
obce	obec	k1gFnPc1
</s>
<s>
586960	#num#	k4
Kód	kód	k1gInSc1
části	část	k1gFnSc2
obce	obec	k1gFnSc2
</s>
<s>
84085	#num#	k4
Geodata	Geodata	k1gFnSc1
(	(	kIx(
<g/>
OSM	osm	k4xCc1
<g/>
)	)	kIx)
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Zdroje	zdroj	k1gInSc2
k	k	k7c3
infoboxu	infobox	k1gInSc3
a	a	k8xC
českým	český	k2eAgNnPc3d1
sídlům	sídlo	k1gNnPc3
<g/>
.	.	kIx.
<g/>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Obec	obec	k1gFnSc1
Křekov	Křekov	k1gInSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
okrese	okres	k1gInSc6
Zlín	Zlín	k1gInSc1
ve	v	k7c6
Zlínském	zlínský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žije	žít	k5eAaImIp3nS
zde	zde	k6eAd1
185	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc1
písemná	písemný	k2eAgFnSc1d1
zmínka	zmínka	k1gFnSc1
o	o	k7c6
obci	obec	k1gFnSc6
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
roku	rok	k1gInSc2
1371	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Pamětihodnosti	pamětihodnost	k1gFnPc1
</s>
<s>
Přírodní	přírodní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
Podskaličí	Podskaličí	k2eAgFnSc1d1
–	–	k?
lokalita	lokalita	k1gFnSc1
šafránu	šafrán	k1gInSc2
bělokvětého	bělokvětý	k2eAgInSc2d1
</s>
<s>
Hložecká	Hložecký	k2eAgFnSc1d1
kaple	kaple	k1gFnSc1
-	-	kIx~
kaple	kaple	k1gFnSc1
z	z	k7c2
roku	rok	k1gInSc2
1953	#num#	k4
na	na	k7c6
místě	místo	k1gNnSc6
dřívější	dřívější	k2eAgFnSc2d1
mariánské	mariánský	k2eAgFnSc2d1
kaple	kaple	k1gFnSc2
na	na	k7c6
kopci	kopec	k1gInSc6
Hložec	Hložec	k1gMnSc1
2	#num#	k4
km	km	kA
jihovýchodně	jihovýchodně	k6eAd1
od	od	k7c2
obce	obec	k1gFnSc2
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Tabule	tabule	k1gFnSc1
</s>
<s>
Náves	náves	k1gFnSc1
</s>
<s>
Dům	dům	k1gInSc1
se	s	k7c7
samostatně	samostatně	k6eAd1
stojícím	stojící	k2eAgInSc7d1
sklípkem	sklípek	k1gInSc7
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1
ulice	ulice	k1gFnSc1
směr	směr	k1gInSc4
Valašské	valašský	k2eAgInPc1d1
Klobouky	Klobouky	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
:	:	kIx,
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
v	v	k7c6
obcích	obec	k1gFnPc6
-	-	kIx~
k	k	k7c3
1.1	1.1	k4
<g/>
.2021	.2021	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
.	.	kIx.
30	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Archivováno	archivován	k2eAgNnSc1d1
2	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2016	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
Turistické	turistický	k2eAgFnPc1d1
zajímavosti	zajímavost	k1gFnPc4
na	na	k7c6
stránkách	stránka	k1gFnPc6
obce	obec	k1gFnSc2
Křekov	Křekov	k1gInSc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Vlachovice	Vlachovice	k1gFnSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Křekov	Křekov	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gNnSc7
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Křekov	Křekov	k1gInSc1
</s>
<s>
Křekov	Křekov	k1gInSc1
v	v	k7c6
Registru	registrum	k1gNnSc6
územní	územní	k2eAgFnSc2d1
identifikace	identifikace	k1gFnSc2
<g/>
,	,	kIx,
adres	adresa	k1gFnPc2
a	a	k8xC
nemovitostí	nemovitost	k1gFnPc2
(	(	kIx(
<g/>
RÚIAN	RÚIAN	kA
<g/>
)	)	kIx)
</s>
<s>
Webové	webový	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
obce	obec	k1gFnSc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Města	město	k1gNnSc2
<g/>
,	,	kIx,
městys	městys	k1gInSc1
a	a	k8xC
obce	obec	k1gFnPc1
okresu	okres	k1gInSc2
Zlín	Zlín	k1gInSc1
</s>
<s>
Bělov	Bělov	k1gInSc1
•	•	k?
Biskupice	Biskupice	k1gFnSc2
•	•	k?
Bohuslavice	Bohuslavice	k1gFnPc1
nad	nad	k7c4
Vláří	Vláří	k2eAgFnPc4d1
•	•	k?
Bohuslavice	Bohuslavice	k1gFnPc4
u	u	k7c2
Zlína	Zlín	k1gInSc2
•	•	k?
Bratřejov	Bratřejov	k1gInSc1
•	•	k?
Brumov-Bylnice	Brumov-Bylnice	k1gFnSc2
•	•	k?
Březnice	Březnice	k1gFnSc2
•	•	k?
Březová	březový	k2eAgFnSc1d1
•	•	k?
Březůvky	Březůvka	k1gFnSc2
•	•	k?
Dešná	Dešný	k2eAgFnSc1d1
•	•	k?
Dobrkovice	Dobrkovice	k1gFnSc1
•	•	k?
Dolní	dolní	k2eAgFnSc1d1
Lhota	Lhota	k1gFnSc1
•	•	k?
Doubravy	Doubrava	k1gFnSc2
•	•	k?
Drnovice	Drnovice	k1gInPc1
•	•	k?
Držková	držkový	k2eAgFnSc1d1
•	•	k?
Fryšták	Fryšták	k1gInSc1
•	•	k?
Halenkovice	Halenkovice	k1gFnPc4
•	•	k?
Haluzice	Haluzice	k1gFnSc2
•	•	k?
Horní	horní	k2eAgFnSc1d1
Lhota	Lhota	k1gFnSc1
•	•	k?
Hostišová	Hostišová	k1gFnSc1
•	•	k?
Hrobice	Hrobice	k1gFnSc2
•	•	k?
Hřivínův	Hřivínův	k2eAgInSc1d1
Újezd	Újezd	k1gInSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Hvozdná	Hvozdný	k2eAgFnSc1d1
•	•	k?
Jasenná	Jasenný	k2eAgFnSc1d1
•	•	k?
Jestřabí	Jestřabí	k1gNnPc2
•	•	k?
Kaňovice	Kaňovice	k1gFnSc2
•	•	k?
Karlovice	Karlovice	k1gFnPc4
•	•	k?
Kašava	Kašava	k1gFnSc1
•	•	k?
Kelníky	Kelník	k1gInPc1
•	•	k?
Komárov	Komárov	k1gInSc1
•	•	k?
Křekov	Křekov	k1gInSc1
•	•	k?
Lhota	Lhota	k1gFnSc1
•	•	k?
Lhotsko	Lhotsko	k1gNnSc1
•	•	k?
Lípa	lípa	k1gFnSc1
•	•	k?
Lipová	lipový	k2eAgFnSc1d1
•	•	k?
Loučka	loučka	k1gFnSc1
•	•	k?
Ludkovice	Ludkovice	k1gFnPc4
•	•	k?
Luhačovice	Luhačovice	k1gFnPc4
•	•	k?
Lukov	Lukov	k1gInSc1
•	•	k?
Lukoveček	Lukoveček	k1gInSc1
•	•	k?
Lutonina	Lutonina	k1gFnSc1
•	•	k?
Machová	Machová	k1gFnSc1
•	•	k?
Mysločovice	Mysločovice	k1gFnSc1
•	•	k?
Napajedla	napajedlo	k1gNnSc2
•	•	k?
Návojná	Návojný	k2eAgFnSc1d1
•	•	k?
Nedašov	Nedašov	k1gInSc1
•	•	k?
Nedašova	Nedašův	k2eAgFnSc1d1
Lhota	Lhota	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Neubuz	Neubuz	k1gMnSc1
•	•	k?
Oldřichovice	Oldřichovice	k1gFnSc2
•	•	k?
Ostrata	Ostrata	k1gFnSc1
•	•	k?
Otrokovice	Otrokovice	k1gFnPc4
•	•	k?
Petrůvka	Petrůvka	k1gFnSc1
•	•	k?
Podhradí	Podhradí	k1gNnSc2
•	•	k?
Podkopná	podkopný	k2eAgFnSc1d1
Lhota	Lhota	k1gFnSc1
•	•	k?
Pohořelice	Pohořelice	k1gFnPc4
•	•	k?
Poteč	téct	k5eAaImRp2nS
•	•	k?
Pozlovice	Pozlovice	k1gFnSc2
•	•	k?
Provodov	Provodov	k1gInSc1
•	•	k?
Racková	Racková	k1gFnSc1
•	•	k?
Rokytnice	Rokytnice	k1gFnSc2
•	•	k?
Rudimov	Rudimov	k1gInSc1
•	•	k?
Sazovice	Sazovice	k1gFnSc2
•	•	k?
Sehradice	Sehradice	k1gFnSc2
•	•	k?
Slavičín	Slavičín	k1gMnSc1
•	•	k?
Slopné	Slopný	k2eAgNnSc1d1
•	•	k?
Slušovice	Slušovice	k1gFnPc4
•	•	k?
Spytihněv	Spytihněv	k1gMnSc1
•	•	k?
Šanov	Šanovo	k1gNnPc2
•	•	k?
Šarovy	Šarův	k2eAgFnSc2d1
•	•	k?
Štítná	štítný	k2eAgFnSc1d1
nad	nad	k7c4
Vláří-Popov	Vláří-Popov	k1gInSc4
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Študlov	Študlov	k1gInSc1
•	•	k?
Tečovice	Tečovice	k1gFnSc2
•	•	k?
Tichov	Tichov	k1gInSc1
•	•	k?
Tlumačov	Tlumačov	k1gInSc1
•	•	k?
Trnava	Trnava	k1gFnSc1
•	•	k?
Ublo	Ublo	k6eAd1
•	•	k?
Újezd	Újezd	k1gInSc1
•	•	k?
Valašské	valašský	k2eAgInPc1d1
Klobouky	Klobouky	k1gInPc1
•	•	k?
Valašské	valašský	k2eAgInPc1d1
Příkazy	příkaz	k1gInPc1
•	•	k?
Velký	velký	k2eAgInSc1d1
Ořechov	Ořechov	k1gInSc1
•	•	k?
Veselá	Veselá	k1gFnSc1
•	•	k?
Vizovice	Vizovice	k1gFnPc4
•	•	k?
Vlachova	Vlachův	k2eAgFnSc1d1
Lhota	Lhota	k1gFnSc1
•	•	k?
Vlachovice	Vlachovice	k1gFnSc2
•	•	k?
Vlčková	vlčkový	k2eAgFnSc1d1
•	•	k?
Všemina	Všemin	k2eAgFnSc1d1
•	•	k?
Vysoké	vysoký	k2eAgFnSc2d1
Pole	pole	k1gFnSc2
•	•	k?
Zádveřice-Raková	Zádveřice-Rakový	k2eAgFnSc1d1
•	•	k?
Zlín	Zlín	k1gInSc1
•	•	k?
Želechovice	Želechovice	k1gFnPc4
nad	nad	k7c7
Dřevnicí	Dřevnice	k1gFnSc7
•	•	k?
Žlutava	žlutavo	k1gNnSc2
legenda	legenda	k1gFnSc1
<g/>
:	:	kIx,
město	město	k1gNnSc1
<g/>
,	,	kIx,
městys	městys	k1gInSc1
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
xx	xx	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
41371	#num#	k4
</s>
