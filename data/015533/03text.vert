<s>
Seznam	seznam	k1gInSc1
zkratek	zkratka	k1gFnPc2
v	v	k7c6
online	onlin	k1gInSc5
diskusích	diskuse	k1gFnPc6
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
zřejmě	zřejmě	k6eAd1
obsahuje	obsahovat	k5eAaImIp3nS
výsledky	výsledek	k1gInPc4
vlastního	vlastní	k2eAgInSc2d1
výzkumu	výzkum	k1gInSc2
nebo	nebo	k8xC
neověřená	ověřený	k2eNgNnPc1d1
tvrzení	tvrzení	k1gNnPc1
<g/>
.	.	kIx.
<g/>
Můžete	moct	k5eAaImIp2nP
pomoci	pomoct	k5eAaPmF
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
přidáte	přidat	k5eAaPmIp2nP
reference	reference	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podívejte	podívat	k5eAaPmRp2nP,k5eAaImRp2nP
se	se	k3xPyFc4
na	na	k7c4
diskusní	diskusní	k2eAgFnSc4d1
stránku	stránka	k1gFnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
uvedeny	uveden	k2eAgInPc1d1
další	další	k2eAgInPc1d1
detaily	detail	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
vkladatele	vkladatel	k1gMnPc4
šablony	šablona	k1gFnSc2
<g/>
:	:	kIx,
Na	na	k7c6
diskusní	diskusní	k2eAgFnSc6d1
stránce	stránka	k1gFnSc6
zdůvodněte	zdůvodnit	k5eAaPmRp2nP
vložení	vložení	k1gNnSc4
šablony	šablona	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Tento	tento	k3xDgInSc1
seznam	seznam	k1gInSc1
obsahuje	obsahovat	k5eAaImIp3nS
výběr	výběr	k1gInSc4
zkratek	zkratka	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
vznikly	vzniknout	k5eAaPmAgFnP
v	v	k7c6
různých	různý	k2eAgNnPc6d1
chatových	chatový	k2eAgNnPc6d1
fórech	fórum	k1gNnPc6
a	a	k8xC
jsou	být	k5eAaImIp3nP
používány	používat	k5eAaImNgFnP
v	v	k7c6
online	onlin	k1gInSc5
diskusích	diskuse	k1gFnPc6
v	v	k7c6
internetu	internet	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
#	#	kIx~
<g/>
shorttoc	shorttoc	k1gInSc1
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
#	#	kIx~
<g/>
shorttoc	shorttoc	k1gFnSc1
.	.	kIx.
<g/>
toc	toc	k?
<g/>
{	{	kIx(
<g/>
width	width	k1gMnSc1
<g/>
:	:	kIx,
<g/>
100	#num#	k4
<g/>
%	%	kIx~
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
#	#	kIx~
<g/>
shorttoc	shorttoc	k1gFnSc1
.	.	kIx.
<g/>
toc	toc	k?
.	.	kIx.
<g/>
toctitle	toctitle	k1gFnSc1
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
#	#	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
shorttoc	shorttoc	k1gFnSc1
h	h	k?
<g/>
2	#num#	k4
span	spana	k1gFnPc2
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
#	#	kIx~
<g/>
shorttoc	shorttoc	k1gInSc1
ul	ul	kA
span	span	k1gInSc1
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
#	#	kIx~
<g/>
shorttoc	shorttoc	k1gInSc1
li	li	k8xS
span	span	k1gInSc1
<g/>
{	{	kIx(
<g/>
float	float	k1gInSc1
<g/>
:	:	kIx,
<g/>
left	left	k5eAaPmF
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
#	#	kIx~
<g/>
shorttoc	shorttoc	k1gFnSc1
.	.	kIx.
<g/>
toc	toc	k?
span	span	k1gInSc1
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
tocnumber	tocnumber	k1gInSc1
<g/>
{	{	kIx(
<g/>
display	display	k1gInPc1
<g/>
:	:	kIx,
<g/>
none	none	k1gFnSc1
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
#	#	kIx~
<g/>
shorttoc	shorttoc	k1gFnSc1
span	span	k1gMnSc1
<g/>
.	.	kIx.
<g/>
toctogglespan	toctogglespan	k1gMnSc1
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
#	#	kIx~
<g/>
shorttoc	shorttoc	k1gInSc1
h	h	k?
<g/>
2	#num#	k4
span	spana	k1gFnPc2
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
#	#	kIx~
<g/>
shorttoc	shorttoc	k1gInSc1
ul	ul	kA
span	span	k1gInSc1
<g/>
{	{	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
margin	margin	k1gInSc1
<g/>
:	:	kIx,
<g/>
0	#num#	k4
.5	.5	k4
<g/>
em	em	k?
0	#num#	k4
0	#num#	k4
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
#	#	kIx~
<g/>
shorttoc	shorttoc	k1gInSc1
ul	ul	kA
<g/>
{	{	kIx(
<g/>
list-style-type	list-style-typ	k1gInSc5
<g/>
:	:	kIx,
<g/>
none	none	k1gNnSc3
<g/>
;	;	kIx,
<g/>
list-style-image	list-style-image	k1gNnSc3
<g/>
:	:	kIx,
<g/>
none	none	k1gNnSc3
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
#	#	kIx~
<g/>
shorttoc	shorttoc	k1gInSc1
li	li	k8xS
<g/>
{	{	kIx(
<g/>
margin-left	margin-left	k1gInSc1
<g/>
:	:	kIx,
<g/>
.5	.5	k4
<g/>
em	em	k?
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
#	#	kIx~
<g/>
shorttoc	shorttoc	k1gInSc1
td	td	k?
<g/>
{	{	kIx(
<g/>
padding	padding	k1gInSc1
<g/>
:	:	kIx,
<g/>
.5	.5	k4
<g/>
em	em	k?
0	#num#	k4
.5	.5	k4
<g/>
em	em	k?
.5	.5	k4
<g/>
em	em	k?
<g/>
}	}	kIx)
</s>
<s>
0	#num#	k4
<g/>
–	–	k?
<g/>
9	#num#	k4
</s>
<s>
zkratka	zkratka	k1gFnSc1
</s>
<s>
anglicky	anglicky	k6eAd1
</s>
<s>
česky	česky	k6eAd1
</s>
<s>
2B	2B	k4
|	|	kIx~
!	!	kIx.
</s>
<s desamb="1">
<g/>
2	#num#	k4
<g/>
BTo	BTo	k1gFnPc2
Be	Be	k1gFnPc2
Or	Or	k1gMnPc2
Not	nota	k1gFnPc2
To	to	k9
BeBýt	BeBýt	k1gInSc4
či	či	k8xC
nebýt	být	k5eNaImF
(	(	kIx(
<g/>
odvozeno	odvodit	k5eAaPmNgNnS
podle	podle	k7c2
jazyka	jazyk	k1gInSc2
C	C	kA
<g/>
)	)	kIx)
</s>
<s>
2	#num#	k4
<g/>
MRWtomorrowzítra	MRWtomorrowzítra	k1gFnSc1
</s>
<s>
2	#num#	k4
<g/>
UTo	UTo	k1gFnPc2
youtobě	youtobě	k6eAd1
</s>
<s>
4	#num#	k4
<g/>
UFor	UFora	k1gFnPc2
Youpro	Youpro	k1gNnSc4
tebe	ty	k3xPp2nSc2
</s>
<s>
4	#num#	k4
<g/>
EForevernavždy	EForevernavždo	k1gNnPc7
</s>
<s>
4	#num#	k4
<g/>
EKfórek	EKfórek	k1gInSc1
(	(	kIx(
<g/>
vtip	vtip	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
4	#num#	k4
<g/>
EVERForevernavždy	EVERForevernavždo	k1gNnPc7
</s>
<s>
4	#num#	k4
<g/>
YEOFor	YEOFor	k1gMnSc1
Your	Your	k1gMnSc1
Eyes	Eyes	k1gInSc4
Onlysoukromé	Onlysoukromý	k2eAgFnPc1d1
<g/>
,	,	kIx,
tajné	tajný	k2eAgFnPc1d1
(	(	kIx(
<g/>
doslova	doslova	k6eAd1
"	"	kIx"
<g/>
jen	jen	k9
pro	pro	k7c4
tvé	tvůj	k3xOp2gNnSc4
oči	oko	k1gNnPc4
<g/>
"	"	kIx"
<g/>
)	)	kIx)
</s>
<s>
2	#num#	k4
<g/>
L	L	kA
<g/>
8	#num#	k4
<g/>
Too	Too	k1gFnSc2
Late	lat	k1gInSc5
<g/>
(	(	kIx(
<g/>
příliš	příliš	k6eAd1
<g/>
)	)	kIx)
pozdě	pozdě	k6eAd1
</s>
<s>
2	#num#	k4
<g/>
H	H	kA
<g/>
4	#num#	k4
<g/>
UToo	UToo	k1gMnSc1
Hard	Hard	k1gMnSc1
For	forum	k1gNnPc2
Youpro	Youpra	k1gFnSc5
Tebe	ty	k3xPp2nSc4
moc	moc	k6eAd1
těžký	těžký	k2eAgInSc1d1
</s>
<s>
2	#num#	k4
<g/>
MTomorrowzítra	MTomorrowzítra	k1gFnSc1
</s>
<s>
2	#num#	k4
<g/>
DTodaydneska	DTodaydneska	k1gFnSc1
</s>
<s>
+1	+1	k4
<g/>
Souhlas	souhlas	k1gInSc1
</s>
<s>
@	@	kIx~
<g/>
atv	atv	k?
(	(	kIx(
<g/>
třeba	třeba	k6eAd1
ve	v	k7c6
4	#num#	k4
<g/>
h	h	k?
<g/>
)	)	kIx)
</s>
<s>
A	a	k9
</s>
<s>
zkratka	zkratka	k1gFnSc1
</s>
<s>
anglicky	anglicky	k6eAd1
</s>
<s>
česky	česky	k6eAd1
</s>
<s>
AAMOFAs	AAMOFAs	k6eAd1
A	a	k9
Matter	Matter	k1gMnSc1
of	of	k?
Factvlastně	Factvlastně	k1gMnSc1
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
</s>
<s>
ACABAll	ACABAll	k1gInSc1
Cops	Copsa	k1gFnPc2
Are	ar	k1gInSc5
Bastardsvšichni	Bastardsvšichni	k1gFnSc3
poldové	polda	k1gMnPc1
jsou	být	k5eAaImIp3nP
bastardi	bastard	k1gMnPc1
</s>
<s>
ACCAccountúčet	ACCAccountúčet	k5eAaImF,k5eAaPmF
</s>
<s>
AFAICAs	AFAICAs	k1gInSc1
Far	fara	k1gFnPc2
As	as	k9
I	I	kA
<g/>
'	'	kIx"
<g/>
m	m	kA
Concernedco	Concernedco	k1gNnSc1
se	se	k3xPyFc4
týká	týkat	k5eAaImIp3nS
mne	já	k3xPp1nSc2
</s>
<s>
AFAICTAs	AFAICTAs	k1gInSc1
Far	fara	k1gFnPc2
As	as	k9
I	i	k9
Can	Can	k1gMnSc1
Tellpokud	Tellpokud	k1gMnSc1
mohu	moct	k5eAaImIp1nS
říci	říct	k5eAaPmF
</s>
<s>
AFAIKAs	AFAIKAs	k1gInSc1
Far	fara	k1gFnPc2
As	as	k1gInSc1
I	i	k8xC
Knowpokud	Knowpokud	k1gInSc1
se	se	k3xPyFc4
nepletu	plést	k5eNaImIp1nS
</s>
<s>
AFAIRAs	AFAIRAs	k1gInSc1
Far	fara	k1gFnPc2
As	as	k9
I	i	k9
Rememberpokud	Rememberpokud	k1gMnSc1
si	se	k3xPyFc3
vzpomínám	vzpomínat	k5eAaImIp1nS
</s>
<s>
AFKAway	AFKAwaa	k1gFnPc1
From	Froma	k1gFnPc2
Keyboardnejsem	Keyboardnejs	k1gInSc7
tu	tu	k6eAd1
(	(	kIx(
<g/>
doslova	doslova	k6eAd1
„	„	k?
<g/>
pryč	pryč	k6eAd1
od	od	k7c2
klávesnice	klávesnice	k1gFnSc2
<g/>
“	“	k?
<g/>
)	)	kIx)
</s>
<s>
AFKAdept	AFKAdept	k1gInSc1
For	forum	k1gNnPc2
Killadept	Killadepta	k1gFnPc2
na	na	k7c4
zabití	zabití	k1gNnSc4
</s>
<s>
AISAs	AISAs	k6eAd1
I	i	k9
Saidjak	Saidjak	k1gMnSc1
jsem	být	k5eAaImIp1nS
již	již	k6eAd1
řekl	říct	k5eAaPmAgMnS
</s>
<s>
AKAAlso	AKAAlsa	k1gFnSc5
Known	Knowna	k1gFnPc2
Astaky	Astak	k1gMnPc4
známý	známý	k2eAgInSc4d1
jako	jako	k8xS,k8xC
<g/>
,	,	kIx,
alias	alias	k9
</s>
<s>
ASAPAs	ASAPAs	k6eAd1
Soon	Soon	k1gInSc1
As	as	k9
Possibleco	Possibleco	k6eAd1
možná	možná	k9
nejdříve	dříve	k6eAd3
</s>
<s>
ASLAge	ASLAge	k6eAd1
<g/>
,	,	kIx,
sex	sex	k1gInSc4
<g/>
,	,	kIx,
land	land	k1gInSc4
<g/>
/	/	kIx~
<g/>
locationvěk	locationvěka	k1gFnPc2
<g/>
,	,	kIx,
pohlaví	pohlaví	k1gNnSc2
a	a	k8xC
země	zem	k1gFnSc2
pobytu	pobyt	k1gInSc2
</s>
<s>
ATBAll	ATBAll	k1gMnSc1
the	the	k?
Bestvše	Bestvše	k1gMnSc1
nejlepší	dobrý	k2eAgMnSc1d3
</s>
<s>
ATLTAnd	ATLTAnd	k1gInSc1
Things	Thingsa	k1gFnPc2
like	likat	k5eAaPmIp3nS
thata	thata	k1gFnSc1
věci	věc	k1gFnSc2
jako	jako	k8xS,k8xC
tato	tento	k3xDgFnSc1
</s>
<s>
ATMAt	ATMAt	k1gInSc1
the	the	k?
momentprávě	momentprávě	k6eAd1
nyní	nyní	k6eAd1
</s>
<s>
ATYSAnything	ATYSAnything	k1gInSc1
You	You	k1gMnSc2
Saycokoliv	Saycokoliv	k1gMnSc2
říkáš	říkat	k5eAaImIp2nS
</s>
<s>
AVNAle	AVNAle	k6eAd1
vůbec	vůbec	k9
ne	ne	k9
<g/>
!	!	kIx.
</s>
<s>
AWOLAbsence	AWOLAbsence	k1gFnSc1
without	without	k5eAaPmF,k5eAaImF
leaveodchod	leaveodchod	k1gInSc4
bez	bez	k7c2
povolení	povolení	k1gNnSc2
</s>
<s>
AYHAre	AYHAr	k1gMnSc5
You	You	k1gMnSc5
Here	Herus	k1gMnSc5
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
jste	být	k5eAaImIp2nP
tady	tady	k6eAd1
<g/>
?	?	kIx.
</s>
<s>
AYORAt	AYORAt	k2eAgInSc1d1
Your	Your	k1gInSc1
Own	Own	k1gFnSc2
Riskna	Riskn	k1gInSc2
vlastní	vlastní	k2eAgNnSc4d1
nebezpečí	nebezpečí	k1gNnSc4
<g/>
!	!	kIx.
</s>
<s>
AYWAs	AYWAs	k6eAd1
You	You	k1gMnSc1
Wishjak	Wishjak	k1gMnSc1
si	se	k3xPyFc3
přeješ	přát	k5eAaImIp2nS
</s>
<s>
B	B	kA
</s>
<s>
zkratka	zkratka	k1gFnSc1
</s>
<s>
anglicky	anglicky	k6eAd1
</s>
<s>
česky	česky	k6eAd1
</s>
<s>
B	B	kA
<g/>
2	#num#	k4
<g/>
BBusiness	BBusiness	k1gInSc4
To	to	k9
Businessod	Businessod	k1gInSc4
firmy	firma	k1gFnSc2
k	k	k7c3
firmě	firma	k1gFnSc3
</s>
<s>
B	B	kA
<g/>
2	#num#	k4
<g/>
CBusiness	CBusiness	k1gInSc4
To	to	k9
Customerod	Customerod	k1gInSc4
firmy	firma	k1gFnSc2
k	k	k7c3
zákazníkovi	zákazník	k1gMnSc3
</s>
<s>
B	B	kA
<g/>
2	#num#	k4
<g/>
TBack	TBacka	k1gFnPc2
To	to	k9
Topiczpátky	Topiczpátek	k1gInPc4
k	k	k7c3
věci	věc	k1gFnSc3
</s>
<s>
B	B	kA
<g/>
4	#num#	k4
<g/>
Beforepředtím	Beforepředtí	k2eAgMnSc7d1
</s>
<s>
B	B	kA
<g/>
4	#num#	k4
<g/>
NBye	NBy	k1gFnSc2
For	forum	k1gNnPc2
Nowzatím	Nowzatím	k1gMnSc1
ahoj	ahoj	k0
<g/>
,	,	kIx,
tak	tak	k9
zatím	zatím	k6eAd1
<g/>
...	...	k?
</s>
<s>
B	B	kA
<g/>
8	#num#	k4
<g/>
Bebejt	Bebejta	k1gFnPc2
(	(	kIx(
<g/>
nespisovné	spisovný	k2eNgInPc1d1
"	"	kIx"
<g/>
být	být	k5eAaImF
<g/>
"	"	kIx"
<g/>
)	)	kIx)
</s>
<s>
BBBye	BBBye	k1gFnSc1
Byepápá	Byepápá	k1gMnSc2
<g/>
,	,	kIx,
ahoj	ahoj	k0
<g/>
,	,	kIx,
nashle	nashle	k6eAd1
</s>
<s>
BBBBlah	BBBBlah	k1gInSc4
Blah	blaho	k1gNnPc2
BlahBla	BlahBl	k1gMnSc2
bla	bla	k?
bla	bla	k?
</s>
<s>
BBC	BBC	kA
</s>
<s>
Big	Big	k?
Black	Black	k1gMnSc1
Cock	Cock	k1gMnSc1
</s>
<s>
Velké	velký	k2eAgNnSc1d1
černé	černý	k2eAgNnSc1d1
přirození	přirození	k1gNnSc1
</s>
<s>
BBLBe	BBLB	k1gFnSc2
Back	Backa	k1gFnPc2
Latervrátím	Latervrátí	k1gNnSc7
se	se	k3xPyFc4
později	pozdě	k6eAd2
</s>
<s>
BBSBe	BBSBe	k6eAd1
Back	Back	k1gInSc1
Soonbrzy	Soonbrza	k1gFnSc2
se	se	k3xPyFc4
vrátím	vrátit	k5eAaPmIp1nS
</s>
<s>
BBFNBye	BBFNBye	k1gNnSc1
Bye	Bye	k1gFnSc2
For	forum	k1gNnPc2
Nowzatím	Nowzatím	k1gMnSc1
ahoj	ahoj	k0
<g/>
,	,	kIx,
tak	tak	k9
zatím	zatím	k6eAd1
<g/>
...	...	k?
</s>
<s>
BCSBecauseprotože	BCSBecauseprotože	k6eAd1
</s>
<s>
BDAYBirthdaynarozeniny	BDAYBirthdaynarozenina	k1gFnPc1
</s>
<s>
BDUBrain	BDUBrain	k2eAgMnSc1d1
Dead	Dead	k1gMnSc1
Useruživatel	Useruživatel	k1gMnSc1
bez	bez	k7c2
mozku	mozek	k1gInSc2
</s>
<s>
BFBoyfriendpřítel	BFBoyfriendpřítel	k1gInSc1
dívky	dívka	k1gFnSc2
</s>
<s>
BFBest	BFBest	k1gFnSc1
Friendsnejlepší	Friendsnejlepší	k2eAgMnPc1d1
přátelé	přítel	k1gMnPc1
</s>
<s>
BFABloody	BFABlooda	k1gFnPc1
Fucking	Fucking	k1gInSc1
Adminzatracený	Adminzatracený	k2eAgMnSc1d1
administrátor	administrátor	k1gMnSc1
</s>
<s>
BFFBest	BFFBest	k1gFnSc1
Friends	Friendsa	k1gFnPc2
Forevernejlepší	Forevernejlepší	k2eAgMnPc1d1
přátelé	přítel	k1gMnPc1
navždy	navždy	k6eAd1
</s>
<s>
BFFLBest	BFFLBest	k1gMnSc1
Friend	Friend	k1gMnSc1
For	forum	k1gNnPc2
Lifenejlepší	Lifenejlepší	k1gNnPc2
přítel	přítel	k1gMnSc1
života	život	k1gInSc2
</s>
<s>
BFGBig	BFGBig	k1gInSc1
Fucking	Fucking	k1gInSc1
Gunvýraz	Gunvýraz	k1gInSc1
pocházející	pocházející	k2eAgInSc1d1
ze	z	k7c2
hry	hra	k1gFnSc2
DOOM	DOOM	kA
<g/>
,	,	kIx,
znamená	znamenat	k5eAaImIp3nS
Velká	velký	k2eAgFnSc1d1
Zasraná	zasraný	k2eAgFnSc1d1
Pistole	pistole	k1gFnSc1
<g/>
/	/	kIx~
<g/>
puška	puška	k1gFnSc1
<g/>
/	/	kIx~
<g/>
zbraň	zbraň	k1gFnSc1
</s>
<s>
BFIBrute	BFIBrut	k1gMnSc5
Force	force	k1gFnPc1
and	and	k?
Ignorancesurovost	Ignorancesurovost	k1gFnSc1
a	a	k8xC
ignorance	ignorance	k1gFnSc1
</s>
<s>
BFNBye	BFNBye	k1gFnSc1
For	forum	k1gNnPc2
Nowzatím	Nowzatím	k1gFnSc2
ahoj	ahoj	k0
</s>
<s>
BGBackGroundpozadí	BGBackGroundpozadit	k5eAaPmIp3nS
</s>
<s>
BHDGod	BHDGod	k1gInSc1
Thanksbohudíky	Thanksbohudík	k1gInPc1
</s>
<s>
BHZLbohužel	BHZLbohužet	k5eAaImAgMnS,k5eAaPmAgMnS
<g/>
,	,	kIx,
naneštěstí	naneštěstí	k9
</s>
<s>
BIONBelieve	BIONBelievat	k5eAaPmIp3nS
It	It	k1gMnSc1
Or	Or	k1gMnSc1
Notvěř	Notvěř	k1gMnSc1
tomu	ten	k3xDgNnSc3
nebo	nebo	k8xC
ne	ne	k9
</s>
<s>
BKBackzpět	BKBackzpět	k5eAaPmF,k5eAaImF
</s>
<s>
BOTBack	BOTBack	k6eAd1
On	on	k3xPp3gMnSc1
Topicabych	Topicabych	k1gMnSc1
se	se	k3xPyFc4
vrátil	vrátit	k5eAaPmAgMnS
k	k	k7c3
věci	věc	k1gFnSc3
</s>
<s>
BOOrProtože	BOOrProtozat	k5eAaPmIp3nS
/	/	kIx~
nebo	nebo	k8xC
</s>
<s>
BOWBest	BOWBest	k1gInSc1
Of	Of	k1gFnSc2
Worldnejlepší	Worldnejlepší	k2eAgFnSc2d1
na	na	k7c6
světě	svět	k1gInSc6
</s>
<s>
BRBBe	BRBBe	k6eAd1
Right	Right	k1gInSc1
Backbudu	Backbud	k1gInSc2
hned	hned	k6eAd1
zpátky	zpátky	k6eAd1
/	/	kIx~
buď	buď	k8xC
hned	hned	k6eAd1
zpátky	zpátky	k6eAd1
</s>
<s>
BROBrotherbratr	BROBrotherbratr	k1gMnSc1
<g/>
,	,	kIx,
bratře	bratr	k1gMnSc5
</s>
<s>
BSODBlue	BSODBlue	k6eAd1
Screen	Screen	k1gInSc1
of	of	k?
Deathmodrá	Deathmodrý	k2eAgFnSc1d1
obrazovka	obrazovka	k1gFnSc1
smrti	smrt	k1gFnSc2
(	(	kIx(
<g/>
známá	známá	k1gFnSc1
uživatelům	uživatel	k1gMnPc3
operačního	operační	k2eAgInSc2d1
systému	systém	k1gInSc2
Windows	Windows	kA
<g/>
)	)	kIx)
</s>
<s>
BTWBy	BTWBa	k1gFnSc2
The	The	k1gFnPc1
Waymimochodem	Waymimochod	k1gInSc7
</s>
<s>
BTWBetweenmezi	BTWBetweenmeze	k1gFnSc4
<g/>
,	,	kIx,
spolu	spolu	k6eAd1
</s>
<s>
BUROpak	BUROpak	k1gMnSc1
slova	slovo	k1gNnSc2
KEK	KEK	kA
=	=	kIx~
LOL	LOL	kA
</s>
<s>
BVŽGod	BVŽGoda	k1gFnPc2
For	forum	k1gNnPc2
YouBůh	YouBůh	k1gMnSc1
vám	vy	k3xPp2nPc3
žehnej	žehnat	k5eAaImRp2nS
</s>
<s>
C	C	kA
</s>
<s>
zkratka	zkratka	k1gFnSc1
</s>
<s>
anglicky	anglicky	k6eAd1
</s>
<s>
česky	česky	k6eAd1
</s>
<s>
CCčau	CCčau	k6eAd1
čau	čau	k0
</s>
<s>
CCconcedeuznání	CCconcedeuznání	k1gNnSc1
prohry	prohra	k1gFnSc2
<g/>
,	,	kIx,
porážky	porážka	k1gFnSc2
(	(	kIx(
<g/>
ve	v	k7c6
hře	hra	k1gFnSc6
<g/>
)	)	kIx)
</s>
<s>
ccz	ccz	k?
původního	původní	k2eAgInSc2d1
carbon	carbon	k1gInSc1
copy	cop	k1gInPc5
</s>
<s>
poslat	poslat	k5eAaPmF
v	v	k7c6
kopii	kopie	k1gFnSc6
(	(	kIx(
<g/>
e-mail	e-mail	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
CG	cg	kA
<g/>
(	(	kIx(
<g/>
T	T	kA
<g/>
)	)	kIx)
<g/>
Congratulationsgratuluji	Congratulationsgratuluji	k1gMnSc1
</s>
<s>
CMIIWCorrect	CMIIWCorrect	k2eAgMnSc1d1
Me	Me	k1gMnSc1
If	If	k1gFnSc2
I	I	kA
<g/>
'	'	kIx"
<g/>
m	m	kA
Wrongoprav	Wrongoprava	k1gFnPc2
mne	já	k3xPp1nSc4
<g/>
,	,	kIx,
pokud	pokud	k8xS
se	se	k3xPyFc4
mýlím	mýlit	k5eAaImIp1nS
</s>
<s>
CNEEConsigneepříjemce	CNEEConsigneepříjemka	k1gFnSc3
</s>
<s>
CNNcelkem	CNNcelek	k1gInSc7
nic	nic	k6eAd1
nového	nový	k2eAgInSc2d1
</s>
<s>
CNRCould	CNRCould	k1gMnSc1
Not	nota	k1gFnPc2
Resistnedokázal	Resistnedokázal	k1gMnSc1
jsem	být	k5eAaImIp1nS
odolat	odolat	k5eAaPmF
</s>
<s>
COSbecauseprotože	COSbecauseprotože	k6eAd1
</s>
<s>
CSčus	CSčus	k1gMnSc1
</s>
<s>
CTJco	CTJco	k6eAd1
to	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
<g/>
?	?	kIx.
</s>
<s>
CUSee	CUSee	k1gFnSc1
Younashle	Younashle	k1gMnSc2
<g/>
,	,	kIx,
ahoj	ahoj	k0
<g/>
,	,	kIx,
uvidíme	uvidět	k5eAaPmIp1nP
se	se	k3xPyFc4
</s>
<s>
CU	CU	kA
<g/>
2	#num#	k4
<g/>
See	See	k1gFnSc2
You	You	k1gFnPc2
<g/>
,	,	kIx,
Tootaky	Tootak	k1gInPc1
nashle	nashle	k6eAd1
...	...	k?
</s>
<s>
CU	CU	kA
<g/>
2	#num#	k4
<g/>
M	M	kA
<g/>
(	(	kIx(
<g/>
oro	oro	k?
<g/>
)	)	kIx)
<g/>
See	See	k1gMnSc2
you	you	k?
tomorrowUvidíme	tomorrowUvidit	k5eAaImIp1nP,k5eAaPmIp1nP
se	s	k7c7
zítra	zítra	k6eAd1
</s>
<s>
CUSSee	CUSSee	k6eAd1
You	You	k1gMnSc1
Soonuvidíme	Soonuvidíme	k1gMnSc1
se	se	k3xPyFc4
brzo	brzo	k6eAd1
</s>
<s>
CYASee	CYASee	k1gFnSc1
Yauvidíme	Yauvidíme	k1gFnPc2
se	se	k3xPyFc4
</s>
<s>
ČKDčasto	ČKDčasto	k6eAd1
kladené	kladený	k2eAgInPc1d1
dotazy	dotaz	k1gInPc1
</s>
<s>
D	D	kA
</s>
<s>
zkratka	zkratka	k1gFnSc1
</s>
<s>
anglicky	anglicky	k6eAd1
</s>
<s>
česky	česky	k6eAd1
</s>
<s>
DNDDo	DNDDo	k1gMnSc1
Not	nota	k1gFnPc2
Disturbnerušte	Disturbnerušte	k1gMnSc1
</s>
<s>
DMSNdle	DMSNdle	k6eAd1
mého	můj	k3xOp1gInSc2
skromného	skromný	k2eAgInSc2d1
názoru	názor	k1gInSc2
</s>
<s>
DIYDo	DIYDo	k1gMnSc1
It	It	k1gMnSc1
Yourselfudělej	Yourselfudělej	k1gMnSc1
si	se	k3xPyFc3
sám	sám	k3xTgMnSc1
</s>
<s>
DCLdocela	DCLdocet	k5eAaImAgFnS,k5eAaPmAgFnS
</s>
<s>
DDDobrý	DDDobrý	k2eAgInSc4d1
den	den	k1gInSc4
</s>
<s>
DNR	DNR	kA
</s>
<s>
do	do	k7c2
not	nota	k1gFnPc2
resuscitate	resuscitat	k1gInSc5
</s>
<s>
neresuscitovat	resuscitovat	k5eNaImF
<g/>
,	,	kIx,
neoživovat	oživovat	k5eNaImF
</s>
<s>
DRThat	DRThat	k5eAaBmF,k5eAaPmF,k5eAaImF
<g/>
’	’	k?
<g/>
s	s	k7c7
rightto	rightt	k2eAgNnSc4d1
jo	jo	k9
<g/>
,	,	kIx,
no	no	k9
</s>
<s>
D	D	kA
<g/>
8	#num#	k4
<g/>
Daterande	Daterand	k1gInSc5
</s>
<s>
E	E	kA
</s>
<s>
zkratka	zkratka	k1gFnSc1
</s>
<s>
anglicky	anglicky	k6eAd1
</s>
<s>
česky	česky	k6eAd1
</s>
<s>
EODEnd	EODEnd	k1gMnSc1
Of	Of	k1gFnSc2
Discussionkonec	Discussionkonec	k1gMnSc1
diskuse	diskuse	k1gFnSc1
</s>
<s>
EOFEnd	EOFEnd	k1gMnSc1
Of	Of	k1gMnPc2
Filekonec	Filekonec	k1gMnSc1
souboru	soubor	k1gInSc2
</s>
<s>
EOMEnd	EOMEnd	k1gMnSc1
of	of	k?
Messagekonec	Messagekonec	k1gMnSc1
zprávy	zpráva	k1gFnSc2
</s>
<s>
EOTEnd	EOTEnd	k1gMnSc1
of	of	k?
Topickonec	Topickonec	k1gMnSc1
tématu	téma	k1gNnSc2
</s>
<s>
NNne	NNne	k6eAd1
ne	ne	k9
</s>
<s>
ETAEstimated	ETAEstimated	k1gMnSc1
Time	Tim	k1gFnSc2
of	of	k?
Arrivalpředpokládaný	Arrivalpředpokládaný	k2eAgInSc4d1
čas	čas	k1gInSc4
příjezdu	příjezd	k1gInSc2
</s>
<s>
ETSEstimated	ETSEstimated	k1gMnSc1
Time	Tim	k1gFnSc2
of	of	k?
Shippingpředpokládaný	Shippingpředpokládaný	k2eAgInSc4d1
čas	čas	k1gInSc4
odeslání	odeslání	k1gNnSc2
</s>
<s>
F	F	kA
</s>
<s>
zkratka	zkratka	k1gFnSc1
</s>
<s>
anglicky	anglicky	k6eAd1
</s>
<s>
česky	česky	k6eAd1
</s>
<s>
FAForever	FAForever	k1gInSc1
alonevěčný	alonevěčný	k2eAgMnSc1d1
samotář	samotář	k1gMnSc1
</s>
<s>
F	F	kA
<g/>
2	#num#	k4
<g/>
FFace-to-Facetváří	FFace-to-Facetvář	k1gFnPc2
v	v	k7c4
tvář	tvář	k1gFnSc4
</s>
<s>
F	F	kA
<g/>
2	#num#	k4
<g/>
PFree-to-PlayDruh	PFree-to-PlayDruh	k1gInSc1
online	onlinout	k5eAaPmIp3nS
hry	hra	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
zadarmo	zadarmo	k6eAd1
</s>
<s>
FAQFrequently	FAQFrequentnout	k5eAaPmAgFnP
Asked	Asked	k1gInSc1
Questionsčasto	Questionsčasta	k1gMnSc5
kladené	kladený	k2eAgMnPc4d1
otázky	otázka	k1gFnPc4
</s>
<s>
FAGFaggotTeplouš	FAGFaggotTeplouš	k5eAaPmIp2nS
</s>
<s>
FEFor	FEFor	k1gInSc1
Examplenapříklad	Examplenapříklad	k1gInSc1
</s>
<s>
FFFireFoxinternetový	FFFireFoxinternetový	k2eAgMnSc1d1
prohlížeč	prohlížeč	k1gMnSc1
Mozilla	Mozilla	k1gMnSc1
Firefox	Firefox	k1gInSc4
</s>
<s>
FFforfeituznání	FFforfeituznání	k1gNnSc1
prohry	prohra	k1gFnSc2
<g/>
,	,	kIx,
porážky	porážka	k1gFnSc2
(	(	kIx(
<g/>
ve	v	k7c6
hře	hra	k1gFnSc6
<g/>
)	)	kIx)
</s>
<s>
FFFor	FFFor	k1gInSc4
FunPro	FunPro	k1gNnSc1
Zábavu	zábav	k1gInSc2
</s>
<s>
FFSFor	FFSFor	k1gMnSc1
Fuck	Fuck	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Sakejde	Sakejd	k1gInSc5
o	o	k7c4
výraz	výraz	k1gInSc4
zoufalství	zoufalství	k1gNnSc2
<g/>
,	,	kIx,
synonymum	synonymum	k1gNnSc1
OMG	OMG	kA
(	(	kIx(
<g/>
odvozeno	odvodit	k5eAaPmNgNnS
zvulgarizováním	zvulgarizování	k1gNnSc7
For	forum	k1gNnPc2
God	God	k1gFnSc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Sake	Sake	k1gFnSc7
-	-	kIx~
pro	pro	k7c4
lásku	láska	k1gFnSc4
<g/>
/	/	kIx~
<g/>
milost	milost	k1gFnSc1
boží	boží	k2eAgFnSc1d1
<g/>
)	)	kIx)
</s>
<s>
FCIFree	FCIFree	k6eAd1
Cool	Cool	k1gInSc1
Inčesky	Inčeska	k1gFnSc2
<g/>
:	:	kIx,
frikulín	frikulín	k1gInSc1
-	-	kIx~
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
někdo	někdo	k3yInSc1
Free	Free	k1gNnSc2
Cool	Coolum	k1gNnPc2
a	a	k8xC
In	In	k1gFnPc2
</s>
<s>
FIFOfirst	FIFOfirst	k1gFnSc1
in	in	k?
first	first	k5eAaPmF
outkdo	outkdo	k1gNnSc1
dřív	dříve	k6eAd2
přijde	přijít	k5eAaPmIp3nS
<g/>
,	,	kIx,
ten	ten	k3xDgMnSc1
dřív	dříve	k6eAd2
mele	mlít	k5eAaImIp3nS
(	(	kIx(
<g/>
typ	typ	k1gInSc1
datového	datový	k2eAgInSc2d1
zásobníku	zásobník	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
FISHfirst	FISHfirst	k1gMnSc1
in	in	k?
still	still	k1gMnSc1
herezůstal	herezůstat	k5eAaImAgMnS,k5eAaPmAgMnS
jsem	být	k5eAaImIp1nS
tady	tady	k6eAd1
trčet	trčet	k5eAaImF
(	(	kIx(
<g/>
ironická	ironický	k2eAgFnSc1d1
obdoba	obdoba	k1gFnSc1
FIFO	FIFO	kA
<g/>
)	)	kIx)
</s>
<s>
FPSFirst-person	FPSFirst-person	k1gMnSc1
Shooterstřílečka	Shooterstřílečka	k1gFnSc1
z	z	k7c2
pohledu	pohled	k1gInSc2
první	první	k4xOgFnSc2
osoby	osoba	k1gFnSc2
</s>
<s>
FPSFrames	FPSFramesa	k1gFnPc2
Per	pero	k1gNnPc2
Secondsnímků	Secondsnímek	k1gInPc2
za	za	k7c4
sekundu	sekunda	k1gFnSc4
</s>
<s>
FOAFFriend	FOAFFriend	k1gMnSc1
Of	Of	k1gMnSc1
A	a	k8xC
Friendpřítel	Friendpřítel	k1gMnSc1
přítele	přítel	k1gMnSc2
(	(	kIx(
<g/>
ve	v	k7c6
smyslu	smysl	k1gInSc6
<g/>
:	:	kIx,
nezaručená	zaručený	k2eNgFnSc1d1
zpráva	zpráva	k1gFnSc1
<g/>
,	,	kIx,
jen	jen	k9
z	z	k7c2
doslechu	doslech	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
FTWFor	FTWFor	k1gInSc1
the	the	k?
windůrazný	windůrazný	k2eAgInSc1d1
projev	projev	k1gInSc1
nadšení	nadšení	k1gNnSc2
na	na	k7c6
konci	konec	k1gInSc6
zprávy	zpráva	k1gFnSc2
<g/>
/	/	kIx~
<g/>
komentáře	komentář	k1gInPc1
<g/>
,	,	kIx,
občas	občas	k6eAd1
i	i	k9
sarkastický	sarkastický	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
FUFuck	FUFuck	k6eAd1
Youjdi	Youjd	k1gMnPc1
do	do	k7c2
prdele	prdel	k1gFnSc2
(	(	kIx(
<g/>
seru	srát	k5eAaImIp1nS
na	na	k7c4
tebe	ty	k3xPp2nSc4
<g/>
)	)	kIx)
</s>
<s>
FUDFear	FUDFear	k1gInSc1
<g/>
,	,	kIx,
Uncertainity	Uncertainit	k1gInPc1
<g/>
,	,	kIx,
Doubtstrach	Doubtstrach	k1gInSc1
<g/>
,	,	kIx,
nejistota	nejistota	k1gFnSc1
<g/>
,	,	kIx,
pochyby	pochyba	k1gFnPc1
</s>
<s>
FUBARFucked	FUBARFucked	k1gMnSc1
Up	Up	k1gMnSc1
Beyond	Beyond	k1gMnSc1
All	All	k1gMnSc1
RepairSituace	RepairSituace	k1gFnSc1
<g/>
/	/	kIx~
<g/>
předmět	předmět	k1gInSc1
je	být	k5eAaImIp3nS
nenávratně	návratně	k6eNd1
(	(	kIx(
<g/>
neopravitelně	opravitelně	k6eNd1
<g/>
)	)	kIx)
zkurvená	zkurvený	k2eAgFnSc1d1
</s>
<s>
FUMTUFucked	FUMTUFucked	k1gMnSc1
Up	Up	k1gMnSc1
More	mor	k1gInSc5
Than	Than	k1gNnSc1
UsualV	UsualV	k1gFnSc3
prdeli	prdel	k1gFnSc3
vice	vika	k1gFnSc3
nez	nez	k?
obvykle	obvykle	k6eAd1
</s>
<s>
FWIWFor	FWIWFor	k1gMnSc1
what	what	k1gMnSc1
it	it	k?
<g/>
'	'	kIx"
<g/>
s	s	k7c7
worth	worth	k1gInSc1
</s>
<s>
FYIFor	FYIFor	k1gMnSc1
Your	Your	k1gMnSc1
Informationpro	Informationpro	k1gNnSc4
tvou	tvůj	k3xOp2gFnSc4
<g/>
/	/	kIx~
<g/>
vaši	váš	k3xOp2gFnSc4
informaci	informace	k1gFnSc4
</s>
<s>
FYEOFor	FYEOFor	k1gMnSc1
Your	Your	k1gMnSc1
Eyes	Eyes	k1gInSc4
Onlytajné	Onlytajný	k2eAgFnPc1d1
(	(	kIx(
<g/>
doslovně	doslovně	k6eAd1
jen	jen	k9
pro	pro	k7c4
tvé	tvůj	k3xOp2gNnSc4
oči	oko	k1gNnPc4
<g/>
)	)	kIx)
</s>
<s>
G	G	kA
</s>
<s>
zkratka	zkratka	k1gFnSc1
</s>
<s>
anglicky	anglicky	k6eAd1
</s>
<s>
česky	česky	k6eAd1
</s>
<s>
G	G	kA
<g/>
8	#num#	k4
<g/>
GateBrána	GateBrán	k2eAgNnPc4d1
</s>
<s>
GR	GR	kA
<g/>
8	#num#	k4
<g/>
Greatsuper	Greatsupero	k1gNnPc2
</s>
<s>
GFGood	GFGood	k1gInSc1
fight	fight	k2eAgInSc1d1
/	/	kIx~
Girlfrienddobrý	Girlfrienddobrý	k2eAgInSc1d1
boj	boj	k1gInSc1
/	/	kIx~
přítelkyně	přítelkyně	k1gFnSc1
</s>
<s>
GGGood	GGGood	k1gInSc1
gamedobrá	gamedobrat	k5eAaPmIp3nS
hra	hra	k1gFnSc1
<g/>
,	,	kIx,
<g/>
pěknou	pěkný	k2eAgFnSc4d1
hru	hra	k1gFnSc4
</s>
<s>
GH	GH	kA
</s>
<s>
Good	Good	k1gInSc1
hunt	hunt	k1gInSc1
/	/	kIx~
Good	Good	k1gInSc1
half	halfa	k1gFnPc2
</s>
<s>
Dobrý	dobrý	k2eAgInSc1d1
lov	lov	k1gInSc1
/	/	kIx~
dobrý	dobrý	k2eAgInSc1d1
poločas	poločas	k1gInSc1
<g/>
,	,	kIx,
doslovně	doslovně	k6eAd1
-	-	kIx~
Dobrá	dobrý	k2eAgFnSc1d1
půle	půle	k1gFnSc1
</s>
<s>
GJGood	GJGood	k1gInSc1
Jobdobrá	Jobdobrý	k2eAgFnSc1d1
práce	práce	k1gFnSc1
</s>
<s>
GLGood	GLGood	k1gInSc1
Luckmnoho	Luckmno	k1gMnSc2
štěstí	štěstí	k1gNnSc2
</s>
<s>
GLGrenade	GLGrenást	k5eAaPmIp3nS
Launchergranátomet	Launchergranátomet	k1gInSc1
</s>
<s>
GLHFGood	GLHFGood	k1gInSc1
Luck	Lucka	k1gFnPc2
Have	Have	k1gFnPc2
Funmnoho	Funmno	k1gMnSc2
štěstí	štěstí	k1gNnSc2
a	a	k8xC
zábavy	zábava	k1gFnSc2
</s>
<s>
GMGame	GMGam	k1gInSc5
MasterSprávce	MasterSprávka	k1gFnSc6
hry	hra	k1gFnPc4
<g/>
,	,	kIx,
doslovný	doslovný	k2eAgInSc4d1
překlad	překlad	k1gInSc4
-	-	kIx~
Herní	herní	k2eAgMnSc1d1
Mistr	mistr	k1gMnSc1
</s>
<s>
GM	GM	kA
</s>
<s>
Good	Good	k6eAd1
Morning	Morning	k1gInSc1
</s>
<s>
Dobré	dobrý	k2eAgNnSc1d1
ráno	ráno	k1gNnSc1
</s>
<s>
GN	GN	kA
/	/	kIx~
GN8	GN8	k1gMnSc2
/	/	kIx~
GNITEGood	GNITEGood	k1gInSc4
Nightdobrou	Nightdobrý	k2eAgFnSc4d1
noc	noc	k1gFnSc4
</s>
<s>
GNSDGood	GNSDGood	k1gInSc1
Night	Night	k1gInSc1
Sweet	Sweet	k1gInSc1
Dreamsdobrou	Dreamsdobrý	k2eAgFnSc4d1
noc	noc	k1gFnSc4
sladké	sladký	k2eAgInPc1d1
sny	sen	k1gInPc1
</s>
<s>
G	G	kA
<g/>
2	#num#	k4
<g/>
G	G	kA
<g/>
(	(	kIx(
<g/>
I	I	kA
<g/>
'	'	kIx"
<g/>
ve	v	k7c4
<g/>
)	)	kIx)
got	got	k?
to	ten	k3xDgNnSc1
go	go	k?
<g/>
!	!	kIx.
</s>
<s desamb="1">
/	/	kIx~
Gotta	Gott	k1gInSc2
go	go	k?
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
musím	muset	k5eAaImIp1nS
pryč	pryč	k6eAd1
</s>
<s>
GOKGod	GOKGod	k1gInSc1
Only	Onla	k1gFnSc2
Knowsto	Knowsta	k1gMnSc5
ví	vědět	k5eAaImIp3nS
jen	jen	k9
bůh	bůh	k1gMnSc1
</s>
<s>
GR	GR	kA
</s>
<s>
Great	Great	k1gInSc1
/	/	kIx~
Good	Good	k1gInSc1
round	round	k1gInSc1
</s>
<s>
dobré	dobrý	k2eAgNnSc1d1
kolo	kolo	k1gNnSc1
(	(	kIx(
<g/>
v	v	k7c6
počítačové	počítačový	k2eAgFnSc6d1
hře	hra	k1gFnSc6
<g/>
)	)	kIx)
</s>
<s>
GIYFGoogle	GIYFGoogle	k1gFnSc1
is	is	k?
your	your	k1gInSc1
friendGoogle	friendGoogle	k1gNnSc2
je	být	k5eAaImIp3nS
tvým	tvůj	k3xOp2gMnSc7
přítelem	přítel	k1gMnSc7
</s>
<s>
GWGood	GWGood	k1gInSc1
Workdobrá	Workdobrý	k2eAgFnSc1d1
práce	práce	k1gFnSc1
</s>
<s>
GTGGot	GTGGot	k1gMnSc1
to	ten	k3xDgNnSc4
gomusím	gomusit	k5eAaPmIp1nS
jít	jít	k5eAaImF
</s>
<s>
GTBGo	GTBGo	k6eAd1
to	ten	k3xDgNnSc1
bedjdu	bedjdu	k6eAd1
do	do	k7c2
postele	postel	k1gFnSc2
</s>
<s>
GSGo	GSGo	k6eAd1
sleepjdu	sleepjdu	k6eAd1
spát	spát	k5eAaImF
</s>
<s>
GYDěkuji	GYDěkovat	k5eAaBmIp1nS,k5eAaPmIp1nS,k5eAaImIp1nS
</s>
<s>
GZGratzGratulace	GZGratzGratulace	k1gFnSc1
</s>
<s>
H	H	kA
</s>
<s>
zkratka	zkratka	k1gFnSc1
</s>
<s>
anglicky	anglicky	k6eAd1
</s>
<s>
česky	česky	k6eAd1
</s>
<s>
HFHave	HFHavat	k5eAaPmIp3nS
funBav	funBav	k1gInSc1
se	s	k7c7
</s>
<s>
HFHate	HFHat	k1gMnSc5
For	forum	k1gNnPc2
(	(	kIx(
<g/>
something	something	k1gInSc1
<g/>
)	)	kIx)
<g/>
Nenávist	nenávist	k1gFnSc1
k	k	k7c3
(	(	kIx(
<g/>
něčemu	něco	k3yInSc3
<g/>
)	)	kIx)
</s>
<s>
HMHand	HMHand	k1gInSc1
Maderučně	Maderučně	k1gFnSc2
dělané	dělaný	k2eAgFnSc2d1
</s>
<s>
HSIKHow	HSIKHow	k?
Should	Should	k1gInSc1
I	i	k9
Knowa	Knowus	k1gMnSc4
jak	jak	k8xC,k8xS
to	ten	k3xDgNnSc4
mám	mít	k5eAaImIp1nS
vědět	vědět	k5eAaImF
<g/>
?	?	kIx.
</s>
<s>
HLhele	HLhele	k6eAd1
</s>
<s>
HTHHope	HTHHop	k1gMnSc5
This	This	k1gInSc1
Helpsdoufám	Helpsdoufa	k1gFnPc3
<g/>
,	,	kIx,
že	že	k8xS
to	ten	k3xDgNnSc1
pomůže	pomoct	k5eAaPmIp3nS
</s>
<s>
HUI	HUI	kA
Hate	Hate	k1gFnSc1
YouNesnáším	YouNesnáším	k1gMnSc1
tě	ty	k3xPp2nSc4
</s>
<s>
HDYDHow	HDYDHow	k?
Do	do	k7c2
You	You	k1gFnPc2
DoJak	DoJak	k1gMnSc1
se	se	k3xPyFc4
máte	mít	k5eAaImIp2nP
<g/>
?	?	kIx.
</s>
<s desamb="1">
/	/	kIx~
Těší	těšit	k5eAaImIp3nP
mě	já	k3xPp1nSc4
(	(	kIx(
<g/>
při	při	k7c6
představování	představování	k1gNnSc6
<g/>
)	)	kIx)
</s>
<s>
HVNHovno	HVNHovna	k1gFnSc5
</s>
<s>
I	i	k9
</s>
<s>
zkratka	zkratka	k1gFnSc1
</s>
<s>
anglicky	anglicky	k6eAd1
</s>
<s>
česky	česky	k6eAd1
</s>
<s>
I2I	I2I	k4
tooJá	tooJat	k5eAaPmIp3nS,k5eAaImIp3nS,k5eAaBmIp3nS
taky	taky	k6eAd1
</s>
<s>
I	i	k9
H8	H8	k1gMnPc1
UI	UI	kA
hate	hate	k6eAd1
younenávidím	younenávidit	k5eAaImIp1nS,k5eAaPmIp1nS
tě	ty	k3xPp2nSc4
</s>
<s>
IAEIn	IAEIn	k1gMnSc1
Any	Any	k1gMnSc1
Eventza	Eventza	k1gFnSc1
všech	všecek	k3xTgFnPc2
okolností	okolnost	k1gFnPc2
</s>
<s>
IANALI	IANALI	kA
Am	Am	k1gFnSc1
Not	nota	k1gFnPc2
A	a	k8xC
Lawyernejsem	Lawyernejs	k1gMnSc7
právník	právník	k1gMnSc1
</s>
<s>
ICQI	ICQI	kA
Seek	Seek	k1gMnSc1
YouHledám	YouHleda	k1gFnPc3
tě	ty	k3xPp2nSc4
</s>
<s>
ICI	ICI	kA
See	See	k1gMnPc2
<g/>
..	..	k?
<g/>
rozumím	rozumět	k5eAaImIp1nS
<g/>
,	,	kIx,
aha	aha	k0
</s>
<s>
IDCI	IDCI	kA
Don	Don	k1gMnSc1
<g/>
'	'	kIx"
<g/>
t	t	k?
Carenezájem	Carenezáj	k1gMnSc7
</s>
<s>
IDGII	IDGII	kA
Don	dona	k1gFnPc2
<g/>
'	'	kIx"
<g/>
t	t	k?
Get	Get	k1gFnSc7
Ittomu	Ittom	k1gInSc2
nerozumím	rozumět	k5eNaImIp1nS
</s>
<s>
IDKI	IDKI	kA
Don	Don	k1gMnSc1
<g/>
'	'	kIx"
<g/>
t	t	k?
Knownevím	Knowneev	k1gFnPc3
<g/>
,	,	kIx,
neznám	neznat	k5eAaImIp1nS,k5eNaImIp1nS
</s>
<s>
IEInternet	IEInternet	k5eAaPmF,k5eAaBmF,k5eAaImF
Explorerwebový	Explorerwebový	k2eAgInSc4d1
prohlížeč	prohlížeč	k1gInSc4
Internet	Internet	k1gInSc1
Explorer	Explorer	k1gInSc1
</s>
<s>
IFAInvisible	IFAInvisible	k6eAd1
for	forum	k1gNnPc2
allNeviditelný	allNeviditelný	k2eAgInSc1d1
pro	pro	k7c4
všechny	všechen	k3xTgMnPc4
(	(	kIx(
<g/>
Z	z	k7c2
icq	icq	k?
klienta	klient	k1gMnSc2
QIP	QIP	kA
<g/>
)	)	kIx)
</s>
<s>
IIANMIf	IIANMIf	k1gInSc4
I	i	k8xC
Am	Am	k1gFnSc4
Not	nota	k1gFnPc2
Mistakenpokud	Mistakenpokuda	k1gFnPc2
se	se	k3xPyFc4
nepletu	plést	k5eNaImIp1nS
</s>
<s>
IIRCIf	IIRCIf	k1gInSc4
I	i	k8xC
Recall	Recall	k1gInSc4
<g/>
/	/	kIx~
<g/>
Remember	Remembra	k1gFnPc2
Correctlypokud	Correctlypokudo	k1gNnPc2
si	se	k3xPyFc3
správně	správně	k6eAd1
vzpomínám	vzpomínat	k5eAaImIp1nS
</s>
<s>
IKIWISII	IKIWISII	kA
<g/>
'	'	kIx"
<g/>
ll	ll	k?
Know	Know	k1gMnSc1
It	It	k1gMnSc1
When	When	k1gMnSc1
I	i	k8xC
See	See	k1gMnSc1
Itto	Itto	k1gMnSc1
budu	být	k5eAaImBp1nS
vědět	vědět	k5eAaImF
<g/>
,	,	kIx,
až	až	k8xS
to	ten	k3xDgNnSc1
uvidím	uvidět	k5eAaPmIp1nS
</s>
<s>
IKR	IKR	kA
</s>
<s>
I	i	k9
Know	Know	k1gMnSc1
<g/>
,	,	kIx,
Right	Right	k1gMnSc1
</s>
<s>
Vím	vědět	k5eAaImIp1nS
dobře	dobře	k6eAd1
</s>
<s>
ILU	ILU	kA
(	(	kIx(
<g/>
ILY	ILY	kA
<g/>
)	)	kIx)
<g/>
I	i	k9
Love	lov	k1gInSc5
Youmiluji	Youmiluji	k1gMnSc5
tě	ty	k3xPp2nSc4
</s>
<s>
IMAOIn	IMAOIn	k1gMnSc1
My	my	k3xPp1nPc1
Arrogant	Arrogant	k1gInSc4
Opinionpodle	Opinionpodle	k1gFnSc1
mého	můj	k3xOp1gInSc2
arogantního	arogantní	k2eAgInSc2d1
názoru	názor	k1gInSc2
</s>
<s>
IMCOIn	IMCOIn	k1gMnSc1
My	my	k3xPp1nPc1
Considered	Considered	k1gInSc1
Opinionpodle	Opinionpodle	k1gMnPc1
mého	můj	k3xOp1gInSc2
dobře	dobře	k6eAd1
zváženého	zvážený	k2eAgInSc2d1
názoru	názor	k1gInSc2
</s>
<s>
IMHOIn	IMHOIn	k1gMnSc1
My	my	k3xPp1nPc1
Humble	Humble	k1gMnPc4
Opinionpodle	Opinionpodle	k1gFnSc7
mého	můj	k3xOp1gInSc2
skromného	skromný	k2eAgInSc2d1
názoru	názor	k1gInSc2
</s>
<s>
IMNSHOIn	IMNSHOIn	k1gMnSc1
My	my	k3xPp1nPc1
Not	nota	k1gFnPc2
So	So	kA
Humble	Humble	k1gMnSc2
Opinionpodle	Opinionpodle	k1gMnSc2
mého	můj	k1gMnSc2
ne	ne	k9
tak	tak	k6eAd1
skromného	skromný	k2eAgInSc2d1
názoru	názor	k1gInSc2
</s>
<s>
IMOIn	IMOIn	k1gMnSc1
My	my	k3xPp1nPc1
Opinionpodle	Opinionpodle	k1gFnPc1
mého	můj	k3xOp1gInSc2
názoru	názor	k1gInSc2
</s>
<s>
IOWIn	IOWIn	k1gNnSc1
Other	Othra	k1gFnPc2
Wordsjinými	Wordsjin	k2eAgNnPc7d1
slovy	slovo	k1gNnPc7
</s>
<s>
IRLIn	IRLIn	k1gInSc1
Real	Real	k1gInSc1
Lifev	Lifev	k1gFnSc4
reálném	reálný	k2eAgInSc6d1
životě	život	k1gInSc6
</s>
<s>
J	J	kA
</s>
<s>
zkratka	zkratka	k1gFnSc1
</s>
<s>
anglicky	anglicky	k6eAd1
</s>
<s>
česky	česky	k6eAd1
</s>
<s>
JFTRJust	JFTRJust	k1gFnSc1
For	forum	k1gNnPc2
The	The	k1gFnSc2
Recorddo	Recorddo	k1gNnSc1
protokolu	protokol	k1gInSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
nezapomnělo	zapomnět	k5eNaImAgNnS,k5eNaPmAgNnS
</s>
<s>
JFYIJust	JFYIJust	k1gFnSc1
For	forum	k1gNnPc2
Your	Youra	k1gFnPc2
Informationjen	Informationjna	k1gFnPc2
pro	pro	k7c4
tvou	tvůj	k3xOp2gFnSc4
<g/>
/	/	kIx~
<g/>
vaši	váš	k3xOp2gFnSc4
informaci	informace	k1gFnSc4
</s>
<s>
JJ	JJ	kA
(	(	kIx(
<g/>
J	J	kA
<g/>
)	)	kIx)
<g/>
jo	jo	k9
jo	jo	k9
/	/	kIx~
jo	jo	k9
<g/>
,	,	kIx,
jasně	jasně	k6eAd1
</s>
<s>
JK	JK	kA
</s>
<s>
Just	just	k6eAd1
Kiding	Kiding	k1gInSc1
</s>
<s>
Jen	jen	k9
si	se	k3xPyFc3
dělám	dělat	k5eAaImIp1nS
srandu	sranda	k1gFnSc4
/	/	kIx~
Jen	jen	k9
vtípkuji	vtípkovat	k5eAaImIp1nS,k5eAaBmIp1nS,k5eAaPmIp1nS
</s>
<s>
JNjo	JNjo	k6eAd1
no	no	k9
</s>
<s>
JJPTjo	JJPTjo	k6eAd1
jo	jo	k9
<g/>
,	,	kIx,
přesně	přesně	k6eAd1
tak	tak	k6eAd1
</s>
<s>
JPPjedna	JPPjedna	k1gFnSc1
paní	paní	k1gFnSc1
povídala	povídat	k5eAaImAgFnS
</s>
<s>
JXVD	JXVD	kA
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
Jak	jak	k6eAd1
se	se	k3xPyFc4
vede	vést	k5eAaImIp3nS
<g/>
?	?	kIx.
</s>
<s>
JSNJá	JSNJá	k1gFnSc1
se	se	k3xPyFc4
nudím	nudit	k5eAaImIp1nS
</s>
<s>
JŽŠ	JŽŠ	kA
</s>
<s>
Ježíši	Ježíš	k1gMnSc3
</s>
<s>
K	k	k7c3
</s>
<s>
zkratka	zkratka	k1gFnSc1
</s>
<s>
anglicky	anglicky	k6eAd1
</s>
<s>
česky	česky	k6eAd1
</s>
<s>
K	k	k7c3
<g/>
8	#num#	k4
<g/>
Kate	kat	k1gMnSc5
</s>
<s>
Kokv	Kokv	k1gInSc1
pořádku	pořádek	k1gInSc2
<g/>
/	/	kIx~
<g/>
v	v	k7c6
pohodě	pohoda	k1gFnSc6
<g/>
/	/	kIx~
<g/>
dobře	dobře	k6eAd1
</s>
<s>
KYokeyv	KYokeyv	k1gInSc1
pořádku	pořádek	k1gInSc2
<g/>
/	/	kIx~
<g/>
v	v	k7c6
pohodě	pohoda	k1gFnSc6
<g/>
/	/	kIx~
<g/>
dobře	dobře	k6eAd1
</s>
<s>
KKok	KKok	k1gInSc1
okv	okv	k?
pořádku	pořádek	k1gInSc2
<g/>
/	/	kIx~
<g/>
v	v	k7c6
pohodě	pohoda	k1gFnSc6
<g/>
/	/	kIx~
<g/>
dobře	dobře	k6eAd1
</s>
<s>
KKT	KKT	kA
</s>
<s>
kokot	kokot	k1gInSc1
<g/>
(	(	kIx(
<g/>
e	e	k0
<g/>
)	)	kIx)
</s>
<s>
KCKeep	KCKeep	k1gMnSc1
coolzachovej	coolzachovat	k5eAaPmRp2nS,k5eAaImRp2nS
klid	klid	k1gInSc1
</s>
<s>
KCHKolik	KCHKolik	k1gMnSc1
chceš	chtít	k5eAaImIp2nS
<g/>
?	?	kIx.
</s>
<s desamb="1">
(	(	kIx(
<g/>
Pochvala	pochvala	k1gFnSc1
za	za	k7c4
dobře	dobře	k6eAd1
vykonaný	vykonaný	k2eAgInSc4d1
komický	komický	k2eAgInSc4d1
výstup	výstup	k1gInSc4
<g/>
/	/	kIx~
<g/>
skutek	skutek	k1gInSc1
<g/>
,	,	kIx,
alternativa	alternativa	k1gFnSc1
k	k	k7c3
LOL	LOL	kA
<g/>
)	)	kIx)
</s>
<s>
KISSKeep	KISSKeep	k1gInSc1
it	it	k?
simple	simple	k6eAd1
<g/>
,	,	kIx,
stupidudělej	stupidudělat	k5eAaPmRp2nS,k5eAaImRp2nS
to	ten	k3xDgNnSc1
jednoduché	jednoduchý	k2eAgNnSc1d1
<g/>
,	,	kIx,
hlupáku	hlupák	k1gMnSc5
</s>
<s>
KOKnock	KOKnock	k1gInSc1
Outvyřadit	Outvyřadit	k1gFnSc2
<g/>
(	(	kIx(
<g/>
výhrou	výhra	k1gFnSc7
ze	z	k7c2
soutěže	soutěž	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
například	například	k6eAd1
v	v	k7c6
boxu	box	k1gInSc6
</s>
<s>
KPDKills	KPDKills	k6eAd1
per	pero	k1gNnPc2
deathpoměr	deathpoměr	k1gInSc4
zabitých	zabitý	k1gMnPc2
ku	k	k7c3
úmrtí	úmrtí	k1gNnSc3
</s>
<s>
KEKPůvodem	KEKPůvod	k1gInSc7
z	z	k7c2
World	Worlda	k1gFnPc2
of	of	k?
Warcraft	Warcrafta	k1gFnPc2
-	-	kIx~
když	když	k8xS
člen	člen	k1gMnSc1
Hordy	horda	k1gFnSc2
napíše	napsat	k5eAaBmIp3nS,k5eAaPmIp3nS
lol	lol	k?
<g/>
,	,	kIx,
hráčovi	hráč	k1gMnSc3
Aliance	aliance	k1gFnSc2
se	se	k3xPyFc4
objeví	objevit	k5eAaPmIp3nS
kek	kek	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
podstatě	podstata	k1gFnSc6
tedy	tedy	k9
to	ten	k3xDgNnSc1
samé	samý	k3xTgNnSc1
jako	jako	k9
lol	lol	k?
<g/>
.	.	kIx.
</s>
<s>
KYS	KYS	kA
</s>
<s>
Kill	Kill	k1gMnSc1
yourself	yourself	k1gMnSc1
</s>
<s>
Zabij	zabít	k5eAaPmRp2nS
sám	sám	k3xTgMnSc1
sebe	sebe	k3xPyFc4
/	/	kIx~
Spáchat	spáchat	k5eAaPmF
sebevraždu	sebevražda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
KS	ks	kA
</s>
<s>
kill	kill	k1gMnSc1
steal	steal	k1gMnSc1
/	/	kIx~
kill	kill	k1gMnSc1
secured	secured	k1gMnSc1
</s>
<s>
ukradený	ukradený	k2eAgInSc1d1
kill	kill	k1gInSc1
<g/>
,	,	kIx,
ukradené	ukradený	k2eAgNnSc1d1
zabití	zabití	k1gNnSc1
/	/	kIx~
zajištění	zajištění	k1gNnSc1
zabití	zabití	k1gNnSc1
</s>
<s>
K	K	kA
<g/>
$$	$$	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kisses	Kissesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Líbá	líbat	k5eAaImIp3nS
</s>
<s>
L	L	kA
</s>
<s>
zkratka	zkratka	k1gFnSc1
</s>
<s>
anglicky	anglicky	k6eAd1
</s>
<s>
česky	česky	k6eAd1
</s>
<s>
L	L	kA
</s>
<s>
Loser	Loser	k1gMnSc1
</s>
<s>
Loser	Loser	k1gMnSc1
</s>
<s>
LAWLJiný	LAWLJiný	k2eAgInSc1d1
výraz	výraz	k1gInSc1
pro	pro	k7c4
hodně	hodně	k6eAd1
smíchu	smích	k1gInSc2
<g/>
,	,	kIx,
podobné	podobný	k2eAgFnPc4d1
LoL	LoL	k1gFnPc4
</s>
<s>
LIFOlast-in	LIFOlast-in	k1gMnSc1
<g/>
,	,	kIx,
first-out	first-out	k1gMnSc1
<g/>
(	(	kIx(
<g/>
také	také	k9
typ	typ	k1gInSc1
datového	datový	k2eAgInSc2d1
zásobníku	zásobník	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
LILOlast-in	LILOlast-in	k1gInSc1
<g/>
,	,	kIx,
last-outkdo	last-outkdo	k1gNnSc1
pozdě	pozdě	k6eAd1
chodí	chodit	k5eAaImIp3nS
<g/>
,	,	kIx,
sám	sám	k3xTgMnSc1
sobě	se	k3xPyFc3
škodí	škodit	k5eAaImIp3nS
</s>
<s>
LTWLike	LTWLike	k1gFnSc1
that	thata	k1gFnPc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
workJako	workJako	k6eAd1
by	by	kYmCp3nS
to	ten	k3xDgNnSc1
pomohlo	pomoct	k5eAaPmAgNnS
<g/>
/	/	kIx~
<g/>
k	k	k7c3
něčemu	něco	k3yInSc3
bylo	být	k5eAaImAgNnS
</s>
<s>
LMAOLaughing	LMAOLaughing	k1gInSc1
My	my	k3xPp1nPc1
Ass	Ass	k1gMnSc4
Offposeru	Offposera	k1gFnSc4
se	s	k7c7
smíchem	smích	k1gInSc7
</s>
<s>
LMFAOLaughing	LMFAOLaughing	k1gInSc1
My	my	k3xPp1nPc1
Fucking	Fucking	k1gInSc1
Ass	Ass	k1gMnSc4
Offposeru	Offposer	k1gInSc2
se	s	k7c7
smíchem	smích	k1gInSc7
</s>
<s>
LOLXVýraz	LOLXVýraz	k1gInSc1
používaný	používaný	k2eAgInSc1d1
asiaty	asiat	k1gMnPc7
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
lolzx	lolzx	k1gInSc4
a	a	k8xC
lolz	lolz	k1gInSc4
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
LOL	LOL	kA
<g/>
)	)	kIx)
</s>
<s>
LOL	LOL	kA
</s>
<s>
Lots	Lots	k1gInSc1
Of	Of	k1gFnSc2
Love	lov	k1gInSc5
</s>
<s>
hodně	hodně	k6eAd1
lásky	láska	k1gFnSc2
</s>
<s>
LOLLaughing	LOLLaughing	k1gInSc1
Out	Out	k1gFnSc2
Loud	louda	k1gFnPc2
<g/>
/	/	kIx~
<g/>
Lot	lot	k1gInSc1
Of	Of	k1gFnSc2
Laughinghlasitě	Laughinghlasita	k1gFnSc3
se	se	k3xPyFc4
směju	smát	k5eAaImIp1nS
<g/>
/	/	kIx~
<g/>
hodně	hodně	k6eAd1
se	se	k3xPyFc4
směju	smát	k5eAaImIp1nS
(	(	kIx(
<g/>
často	často	k6eAd1
myšleno	myslet	k5eAaImNgNnS
ironicky	ironicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
LOL	LOL	kA
</s>
<s>
League	League	k1gFnSc1
of	of	k?
Legends	Legends	k1gInSc1
</s>
<s>
Liga	liga	k1gFnSc1
legend	legenda	k1gFnPc2
(	(	kIx(
<g/>
MOBARPG	MOBARPG	kA
Počítačová	počítačový	k2eAgFnSc1d1
hra	hra	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
LULove	LULov	k1gInSc5
Youmiluju	Youmiluju	k1gFnPc2
tě	ty	k3xPp2nSc4
</s>
<s>
L	L	kA
<g/>
4	#num#	k4
<g/>
Looking	Looking	k1gInSc4
ForHledám	ForHleda	k1gFnPc3
...	...	k?
</s>
<s>
LFLooking	LFLooking	k1gInSc1
ForHledám	ForHleda	k1gFnPc3
...	...	k?
</s>
<s>
LFLonely	LFLonel	k1gInPc1
FaceSmutný	FaceSmutný	k2eAgInSc1d1
smajlík	smajlík	k?
:	:	kIx,
<g/>
-	-	kIx~
<g/>
(	(	kIx(
</s>
<s>
LFPLooking	LFPLooking	k1gInSc1
for	forum	k1gNnPc2
partyhledám	partyhledat	k5eAaPmIp1nS,k5eAaImIp1nS
partu	parta	k1gFnSc4
(	(	kIx(
<g/>
skupinu	skupina	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
LFGLooking	LFGLooking	k1gInSc1
For	forum	k1gNnPc2
GroupHledám	GroupHleda	k1gFnPc3
partu	part	k1gInSc6
</s>
<s>
LFMLooking	LFMLooking	k1gInSc1
For	forum	k1gNnPc2
MembersHledám	MembersHleda	k1gFnPc3
členy	člen	k1gMnPc4
(	(	kIx(
<g/>
do	do	k7c2
party	parta	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
L	L	kA
<g/>
8	#num#	k4
<g/>
RLaterPozději	RLaterPozději	k1gFnSc2
</s>
<s>
L	L	kA
<g/>
2	#num#	k4
<g/>
PLearn	PLearno	k1gNnPc2
to	ten	k3xDgNnSc1
playNauč	playNaučet	k5eAaPmRp2nS
se	se	k3xPyFc4
(	(	kIx(
<g/>
to	ten	k3xDgNnSc4
<g/>
)	)	kIx)
hrát	hrát	k5eAaImF
</s>
<s>
L	L	kA
<g/>
2	#num#	k4
<g/>
Lineage	Lineag	k1gInSc2
2	#num#	k4
<g/>
název	název	k1gInSc1
mmorpg	mmorpg	k1gInSc4
hry	hra	k1gFnSc2
</s>
<s>
M	M	kA
</s>
<s>
zkratka	zkratka	k1gFnSc1
</s>
<s>
anglicky	anglicky	k6eAd1
</s>
<s>
česky	česky	k6eAd1
</s>
<s>
M	M	kA
<g/>
2	#num#	k4
<g/>
Me	Me	k1gFnPc6
toojá	toojat	k5eAaPmIp3nS,k5eAaImIp3nS,k5eAaBmIp3nS
taky	taky	k6eAd1
</s>
<s>
M	M	kA
<g/>
8	#num#	k4
<g/>
matekamaráde	matekamaráde	k6eAd1
</s>
<s>
MFGMat-Fyz	MFGMat-Fyz	k1gInSc1
Girl	girl	k1gFnSc2
</s>
<s>
MFGmy	MFGma	k1gFnPc1
fucking	fucking	k1gInSc1
Godmůj	Godmůj	k1gInSc1
zasraný	zasraný	k2eAgInSc1d1
Bože	Boža	k1gFnSc3
(	(	kIx(
<g/>
ó	ó	k0
<g/>
,	,	kIx,
můj	můj	k1gMnSc5
Bože	bůh	k1gMnSc5
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
</s>
<s>
MBLMumbleMumble	MBLMumbleMumble	k6eAd1
</s>
<s>
MIAmissing	MIAmissing	k1gInSc1
in	in	k?
actionnezvěstný	actionnezvěstný	k2eAgInSc1d1
</s>
<s>
MMCH	MMCH	kA
/	/	kIx~
MMCHDmimochodem	MMCHDmimochod	k1gInSc7
</s>
<s>
MMNT	MMNT	kA
/	/	kIx~
MMTmoment	MMTmoment	k1gMnSc1
</s>
<s>
MODEMMOdulator	MODEMMOdulator	k1gMnSc1
<g/>
/	/	kIx~
<g/>
DEModulatorMOdulator	DEModulatorMOdulator	k1gMnSc1
<g/>
/	/	kIx~
<g/>
DEModulator	DEModulator	k1gMnSc1
</s>
<s>
MBMail	MBMail	k1gMnSc1
Backodepiš	Backodepiš	k1gMnSc1
</s>
<s>
MSIEMicrosoft	MSIEMicrosoft	k2eAgInSc1d1
Internet	Internet	k1gInSc1
ExplorerMicrosoft	ExplorerMicrosoft	k2eAgInSc1d1
Internet	Internet	k1gInSc4
Explorer	Explorero	k1gNnPc2
</s>
<s>
msg	msg	k?
<g/>
(	(	kIx(
<g/>
r	r	kA
<g/>
)	)	kIx)
<g/>
message	message	k1gInSc1
<g/>
(	(	kIx(
<g/>
messenger	messenger	k1gInSc1
<g/>
)	)	kIx)
<g/>
zpráva	zpráva	k1gFnSc1
(	(	kIx(
<g/>
mesendžr	mesendžr	k1gInSc1
<g/>
,	,	kIx,
IM	IM	kA
klient	klient	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
MTmiluji	MTmilovat	k5eAaBmIp1nS,k5eAaPmIp1nS,k5eAaImIp1nS
tě	ty	k3xPp2nSc4
</s>
<s>
MTBmiluji	MTBmilovat	k5eAaBmIp1nS,k5eAaPmIp1nS,k5eAaImIp1nS
tě	ty	k3xPp2nSc4
broučku	brouček	k1gMnSc6
</s>
<s>
MTRmám	MTRmat	k5eAaPmIp1nS
tě	ty	k3xPp2nSc4
rád	rád	k6eAd1
<g/>
(	(	kIx(
<g/>
a	a	k8xC
<g/>
)	)	kIx)
</s>
<s>
MTMRmám	MTMRmat	k5eAaPmIp1nS
tě	ty	k3xPp2nSc4
moc	moc	k6eAd1
rád	rád	k2eAgMnSc1d1
<g/>
/	/	kIx~
<g/>
a	a	k8xC
</s>
<s>
MTMmoc	MTMmoc	k6eAd1
tě	ty	k3xPp2nSc4
miluji	milovat	k5eAaImIp1nS
</s>
<s>
MTSMRmám	MTSMRmat	k5eAaPmIp1nS
tě	ty	k3xPp2nSc4
strašně	strašně	k6eAd1
moc	moc	k6eAd1
rád	rád	k2eAgMnSc1d1
<g/>
/	/	kIx~
<g/>
a	a	k8xC
</s>
<s>
MYOBMind	MYOBMind	k1gMnSc1
your	your	k1gMnSc1
own	own	k?
businesshleď	businesshledit	k5eAaPmRp2nS
si	se	k3xPyFc3
svého	svůj	k3xOyFgMnSc2
</s>
<s>
MTTmám	MTTmat	k5eAaPmIp1nS
takové	takový	k3xDgNnSc1
tušení	tušení	k1gNnSc1
</s>
<s>
MZCmazec	MZCmazec	k1gMnSc1
</s>
<s>
N	N	kA
</s>
<s>
zkratka	zkratka	k1gFnSc1
</s>
<s>
anglicky	anglicky	k6eAd1
</s>
<s>
česky	česky	k6eAd1
</s>
<s>
N	N	kA
<g/>
1	#num#	k4
<g/>
Nice	Nice	k1gFnSc2
onehezké	onehezká	k1gFnSc2
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
se	se	k3xPyFc4
povedlo	povést	k5eAaPmAgNnS
</s>
<s>
N	N	kA
<g/>
1	#num#	k4
<g/>
Number	Number	k1gInSc1
oneJsi	oneJse	k1gFnSc3
<g/>
/	/	kIx~
<g/>
je	být	k5eAaImIp3nS
nejlepší	dobrý	k2eAgFnSc1d3
(	(	kIx(
<g/>
osoba	osoba	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
N	N	kA
<g/>
8	#num#	k4
<g/>
Nightdobrou	Nightdobrý	k2eAgFnSc4d1
noc	noc	k1gFnSc4
</s>
<s>
N	N	kA
<g/>
/	/	kIx~
<g/>
ANot	ANot	k2eAgInSc1d1
Availablenedostupný	Availablenedostupný	k2eAgInSc1d1
</s>
<s>
NAKNot	NAKNot	k1gMnSc1
Acknowledgednesouhlasím	Acknowledgednesouhlase	k1gFnPc3
</s>
<s>
NANNot	NANNot	k1gInSc1
a	a	k8xC
numberčíslo	numberčísnout	k5eAaPmAgNnS
nesdělím	sdělit	k5eNaPmIp1nS
<g/>
,	,	kIx,
nemám	mít	k5eNaImIp1nS
číslo	číslo	k1gNnSc1
</s>
<s>
nbnebo	nbneba	k1gMnSc5
</s>
<s>
NCNo	NCNo	k6eAd1
commentbez	commentbez	k1gInSc1
komentáře	komentář	k1gInSc2
</s>
<s>
NJ	NJ	kA
(	(	kIx(
<g/>
N	N	kA
<g/>
)	)	kIx)
<g/>
no	no	k9
jo	jo	k9
(	(	kIx(
<g/>
no	no	k9
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ne	ne	k9
</s>
<s>
nnno	nnna	k1gMnSc5
nonene	nonen	k1gMnSc5
</s>
<s>
NnNighty	NnNighta	k1gFnPc1
nightdobrou	nightdobrý	k2eAgFnSc4d1
noc	noc	k1gFnSc4
(	(	kIx(
<g/>
dobrou	dobrý	k2eAgFnSc4d1
<g/>
)	)	kIx)
</s>
<s>
NOOB	NOOB	kA
(	(	kIx(
<g/>
N	N	kA
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
B	B	kA
<g/>
)	)	kIx)
<g/>
Newbie	Newbie	k1gFnSc1
(	(	kIx(
<g/>
Noobie	Noobie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
začátečník	začátečník	k1gMnSc1
<g/>
,	,	kIx,
nováček	nováček	k1gMnSc1
<g/>
,	,	kIx,
nezkušený	zkušený	k2eNgMnSc1d1
hráč	hráč	k1gMnSc1
<g/>
,	,	kIx,
lama	lama	k1gMnSc1
</s>
<s>
NPNot	NPNot	k1gInSc1
a	a	k8xC
Problembez	Problembez	k1gInSc1
problému	problém	k1gInSc2
</s>
<s>
NPKCna	NPKCna	k6eAd1
pokec	pokec	k1gInSc1
(	(	kIx(
<g/>
vizte	vidět	k5eAaImRp2nP
též	též	k9
Kompost	kompost	k1gInSc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
)	)	kIx)
</s>
<s>
NRNNo	NRNNo	k1gNnSc1
Reply	Repla	k1gMnSc2
Necessaryodpověď	Necessaryodpověď	k1gMnSc2
není	být	k5eNaImIp3nS
nutná	nutný	k2eAgFnSc1d1
</s>
<s>
NSNice	NSNice	k1gInPc1
shotskvělý	shotskvělý	k2eAgInSc1d1
zásah	zásah	k1gInSc4
<g/>
/	/	kIx~
<g/>
úder	úder	k1gInSc4
</s>
<s>
NTNice	NTNice	k1gInPc1
Trydobrý	Trydobrý	k2eAgInSc1d1
pokus	pokus	k1gInSc4
</s>
<s>
NTno	NTno	k6eAd1
timenemám	timenemat	k5eAaPmIp1nS
čas	čas	k1gInSc4
</s>
<s>
NTSFWnot	NTSFWnota	k1gFnPc2
safe	safe	k1gInSc1
for	forum	k1gNnPc2
worknehodící	worknehodící	k2eAgInPc4d1
se	se	k3xPyFc4
do	do	k7c2
práce	práce	k1gFnSc2
(	(	kIx(
<g/>
neotvírat	otvírat	k5eNaImF
v	v	k7c6
práci	práce	k1gFnSc6
<g/>
)	)	kIx)
</s>
<s>
NSFWnot	NSFWnota	k1gFnPc2
safe	safe	k1gInSc1
for	forum	k1gNnPc2
worknehodící	worknehodící	k2eAgInPc4d1
se	se	k3xPyFc4
do	do	k7c2
práce	práce	k1gFnSc2
(	(	kIx(
<g/>
neotvírat	otvírat	k5eNaImF
v	v	k7c6
práci	práce	k1gFnSc6
<g/>
)	)	kIx)
</s>
<s>
NSFL	NSFL	kA
</s>
<s>
not	nota	k1gFnPc2
safe	safe	k1gInSc1
for	forum	k1gNnPc2
life	lifat	k5eAaPmIp3nS
</s>
<s>
nehodící	hodící	k2eNgFnSc1d1
se	se	k3xPyFc4
do	do	k7c2
života	život	k1gInSc2
(	(	kIx(
<g/>
ve	v	k7c6
smyslu	smysl	k1gInSc6
<g/>
:	:	kIx,
Opravdu	opravdu	k6eAd1
to	ten	k3xDgNnSc1
nechceš	chtít	k5eNaImIp2nS
vidět	vidět	k5eAaImF
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
</s>
<s>
NVMNever	NVMNever	k1gInSc1
mindnevadí	mindnevadí	k1gNnSc2
(	(	kIx(
<g/>
hovorově	hovorově	k6eAd1
"	"	kIx"
<g/>
neřeš	řešit	k5eNaImRp2nS
<g/>
"	"	kIx"
<g/>
)	)	kIx)
</s>
<s>
NVMnevím	NVMnevit	k5eAaImIp1nS,k5eAaPmIp1nS
</s>
<s>
NWMNo	NWMNo	k1gMnSc1
worries	worries	k1gMnSc1
<g/>
,	,	kIx,
Man	Man	k1gMnSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
neměj	mít	k5eNaImRp2nS
strachy	strach	k1gInPc7
<g/>
,	,	kIx,
člověče	člověk	k1gMnSc5
<g/>
!	!	kIx.
</s>
<s>
NZnemáš	NZnemat	k5eAaPmIp2nS,k5eAaBmIp2nS,k5eAaImIp2nS
<g/>
/	/	kIx~
<g/>
není	být	k5eNaImIp3nS
zač	zač	k6eAd1
</s>
<s>
O	o	k7c6
</s>
<s>
zkratka	zkratka	k1gFnSc1
</s>
<s>
anglicky	anglicky	k6eAd1
</s>
<s>
česky	česky	k6eAd1
</s>
<s>
O	o	k7c4
<g/>
5	#num#	k4
<g/>
opět	opět	k6eAd1
</s>
<s>
O-OOver	O-OOver	k1gInSc1
and	and	k?
Outpřepínám	Outpřepína	k1gFnPc3
a	a	k8xC
končím	končit	k5eAaImIp1nS
</s>
<s>
OFCOf	OFCOf	k1gMnSc1
Coursesamozřejmě	Coursesamozřejma	k1gFnSc3
</s>
<s>
OCOf	OCOf	k1gMnSc1
Coursesamozřejmě	Coursesamozřejma	k1gFnSc3
</s>
<s>
OICOh	OICOh	k1gMnSc1
I	i	k8xC
seeaha	seeaha	k1gMnSc1
</s>
<s>
OKIdobře	OKIdobřít	k5eAaPmIp3nS
(	(	kIx(
<g/>
OK	oko	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
OMGOh	OMGOh	k1gMnSc1
my	my	k3xPp1nPc1
GodÓ	GodÓ	k1gMnSc5
můj	můj	k1gMnSc5
bože	bůh	k1gMnSc5
</s>
<s>
OMWOn	OMWOn	k1gMnSc1
My	my	k3xPp1nPc1
WayNa	WayNum	k1gNnPc4
cestě	cesta	k1gFnSc6
</s>
<s>
ONOh	ONOh	k1gMnSc1
NoÓ	NoÓ	k1gMnSc1
né	né	k?
<g/>
!	!	kIx.
</s>
<s>
OOKOOKpřepis	OOKOOKpřepis	k1gFnSc1
OK	oka	k1gFnPc2
používaný	používaný	k2eAgInSc1d1
fanoušky	fanoušek	k1gMnPc4
Zeměplochy	Zeměploch	k1gInPc4
<g/>
,	,	kIx,
narážka	narážka	k1gFnSc1
na	na	k7c4
Knihovníkovo	knihovníkův	k2eAgNnSc4d1
Ook	Ook	k1gFnSc3
</s>
<s>
OTOff	OTOff	k1gMnSc1
Topicnepatří	Topicnepatří	k1gMnSc1
k	k	k7c3
věci	věc	k1gFnSc3
</s>
<s>
OTOHOn	OTOHOn	k1gMnSc1
The	The	k1gFnSc2
Other	Other	k1gMnSc1
Handna	Handna	k1gFnSc1
druhé	druhý	k4xOgFnSc6
straně	strana	k1gFnSc6
</s>
<s>
OKISdobře	OKISdobřít	k5eAaPmIp3nS
(	(	kIx(
<g/>
OK	oko	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
ORLY	Orel	k1gMnPc4
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
Oh	oh	k0
Really	Reallo	k1gNnPc7
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
Opravdu	opravdu	k6eAd1
<g/>
?	?	kIx.
</s>
<s>
P	P	kA
</s>
<s>
zkratka	zkratka	k1gFnSc1
</s>
<s>
anglicky	anglicky	k6eAd1
</s>
<s>
česky	česky	k6eAd1
</s>
<s>
P	P	kA
<g/>
2	#num#	k4
<g/>
Ppeer	Ppeero	k1gNnPc2
to	ten	k3xDgNnSc1
peertyp	peertyp	k1gMnSc1
komunikačních	komunikační	k2eAgFnPc2d1
sítí	síť	k1gFnPc2
určených	určený	k2eAgFnPc2d1
především	především	k6eAd1
ke	k	k7c3
sdílení	sdílení	k1gNnSc3
souborů	soubor	k1gInPc2
</s>
<s>
P	P	kA
<g/>
2	#num#	k4
<g/>
Ppay	Ppaa	k1gFnSc2
to	ten	k3xDgNnSc4
playplacení	playplacení	k1gNnSc4
za	za	k7c2
hry	hra	k1gFnSc2
online	onlinout	k5eAaPmIp3nS
typu	typ	k1gInSc2
</s>
<s>
PACPoněvadž	PACPoněvadž	k1gFnSc1
(	(	kIx(
<g/>
jako	jako	k8xS,k8xC
protože	protože	k8xS
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
"	"	kIx"
<g/>
páč	páč	k?
<g/>
"	"	kIx"
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
i	i	k9
v	v	k7c6
řeči	řeč	k1gFnSc6
</s>
<s>
PEBKACProblem	PEBKACProbl	k1gMnSc7
Exists	Existsa	k1gFnPc2
Between	Between	k1gInSc1
Keyboard	Keyboard	k1gMnSc1
And	Anda	k1gFnPc2
Chairproblém	Chairproblý	k2eAgInSc6d1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
mezi	mezi	k7c7
klávesnicí	klávesnice	k1gFnSc7
a	a	k8xC
židlí	židle	k1gFnSc7
</s>
<s>
PKPlayer	PKPlayer	k1gMnSc1
killerZabiják	killerZabiják	k1gMnSc1
hráčů	hráč	k1gMnPc2
</s>
<s>
PLSPleaseprosím	PLSPleaseprosit	k5eAaPmIp1nS
</s>
<s>
PSMprosím	PSMprosit	k5eAaPmIp1nS
</s>
<s>
PSPPlaystation	PSPPlaystation	k1gInSc1
Portable	portable	k1gInSc1
</s>
<s>
PTZ	PTZ	kA
<g/>
,	,	kIx,
ptžeprotože	ptžeprotože	k6eAd1
</s>
<s>
PTpartyparta	PTpartyparta	k1gFnSc1
(	(	kIx(
<g/>
např.	např.	kA
ve	v	k7c6
smyslu	smysl	k1gInSc6
"	"	kIx"
<g/>
dej	dát	k5eAaPmRp2nS
mi	já	k3xPp1nSc3
PT	PT	kA
<g/>
"	"	kIx"
<g/>
)	)	kIx)
</s>
<s>
PVPPlayer	PVPPlayer	k1gInSc1
versus	versus	k7c1
playerhráč	playerhráč	k1gMnSc1
proti	proti	k7c3
hráči	hráč	k1gMnSc3
</s>
<s>
PvPParty	PvPParta	k1gFnPc1
versus	versus	k7c1
Playerparta	Playerpart	k1gMnSc2
proti	proti	k7c3
hráči	hráč	k1gMnSc3
(	(	kIx(
<g/>
nejčastěji	často	k6eAd3
ve	v	k7c6
hře	hra	k1gFnSc6
World	World	k1gMnSc1
of	of	k?
Warcraft	Warcraft	k1gMnSc1
v	v	k7c6
BattleGroundu	BattleGround	k1gInSc6
horda	horda	k1gFnSc1
proti	proti	k7c3
alíkovi	alíek	k1gMnSc3
<g/>
)	)	kIx)
</s>
<s>
PVEPlayer	PVEPlayer	k1gInSc1
versus	versus	k7c1
environmenthráč	environmenthráč	k1gInSc4
proti	proti	k7c3
prostředí	prostředí	k1gNnSc3
(	(	kIx(
<g/>
proti	proti	k7c3
NPC	NPC	kA
<g/>
,	,	kIx,
mobům	mob	k1gMnPc3
<g/>
,	,	kIx,
creepum	creepum	k1gInSc1
<g/>
,	,	kIx,
zkrátka	zkrátka	k6eAd1
postavám	postava	k1gFnPc3
ovládaným	ovládaný	k2eAgFnPc3d1
hrou	hra	k1gFnSc7
<g/>
)	)	kIx)
</s>
<s>
PWNPreklep	PWNPreklep	k1gMnSc1
slova	slovo	k1gNnSc2
OWN	OWN	kA
viď	vidět	k5eAaImRp2nS
PWND	PWND	kA
</s>
<s>
PWNDpreklep	PWNDpreklep	k1gMnSc1
slova	slovo	k1gNnSc2
OWND	OWND	kA
<g/>
(	(	kIx(
<g/>
OWNED	OWNED	kA
<g/>
)	)	kIx)
<g/>
-mám	-mám	k1gMnSc1
ťa	ťa	k0
<g/>
,	,	kIx,
dostal	dostat	k5eAaPmAgMnS
som	soma	k1gFnPc2
ťa	ťa	k0
</s>
<s>
Q	Q	kA
</s>
<s>
zkratka	zkratka	k1gFnSc1
</s>
<s>
anglicky	anglicky	k6eAd1
</s>
<s>
česky	česky	k6eAd1
</s>
<s>
Q	Q	kA
<g/>
/	/	kIx~
<g/>
A	A	kA
</s>
<s>
QNA	QNA	kA
Q	Q	kA
<g/>
&	&	k?
<g/>
A	a	k9
</s>
<s>
Question	Question	k1gInSc1
and	and	k?
AnswerQuestions	AnswerQuestions	k1gInSc1
and	and	k?
Answersotázka	Answersotázka	k1gFnSc1
a	a	k8xC
odpověďotázky	odpověďotázka	k1gFnPc1
a	a	k8xC
odpovědi	odpověď	k1gFnPc1
</s>
<s>
QATCQuestion	QATCQuestion	k1gInSc1
Answered	Answered	k1gInSc1
<g/>
,	,	kIx,
Thread	Thread	k1gInSc1
ClosedOtázka	ClosedOtázka	k1gFnSc1
zodpovězena	zodpovědět	k5eAaPmNgFnS
<g/>
,	,	kIx,
téma	téma	k1gNnSc4
zamknuto	zamknout	k5eAaPmNgNnS
</s>
<s>
R	R	kA
</s>
<s>
zkratka	zkratka	k1gFnSc1
</s>
<s>
anglicky	anglicky	k6eAd1
</s>
<s>
česky	česky	k6eAd1
</s>
<s>
RRightrozumím	RRightrozumět	k5eAaPmIp1nS
</s>
<s>
RFN	RFN	kA
</s>
<s>
Right	Right	k2eAgInSc1d1
Fucking	Fucking	k1gInSc1
Now	Now	k1gFnSc2
</s>
<s>
Právě	právě	k9
teď	teď	k6eAd1
kurva	kurva	k1gFnSc1
</s>
<s>
ROFL	ROFL	kA
<g/>
,	,	kIx,
ROTFLRolling	ROTFLRolling	k1gInSc1
On	on	k3xPp3gMnSc1
(	(	kIx(
<g/>
The	The	k1gMnSc1
<g/>
)	)	kIx)
Floor	Floor	k1gMnSc1
Laughingválím	Laughingválím	k1gMnSc1
se	se	k3xPyFc4
po	po	k7c6
podlaze	podlaha	k1gFnSc6
smíchy	smíchy	k6eAd1
</s>
<s>
ROTFLOLRolling	ROTFLOLRolling	k1gInSc4
On	on	k3xPp3gMnSc1
The	The	k1gMnSc1
Floor	Floora	k1gFnPc2
Laughing	Laughing	k1gInSc1
Out	Out	k1gMnSc1
Loudválím	Loudválím	k1gMnSc1
se	se	k3xPyFc4
po	po	k7c6
podlaze	podlaha	k1gFnSc6
s	s	k7c7
hlasitým	hlasitý	k2eAgInSc7d1
smíchem	smích	k1gInSc7
</s>
<s>
RES	RES	kA
<g/>
/	/	kIx~
<g/>
RESSRessurectionVzkříšení	RESSRessurectionVzkříšení	k1gNnSc1
<g/>
,	,	kIx,
resuscitace	resuscitace	k1gFnSc1
(	(	kIx(
<g/>
často	často	k6eAd1
u	u	k7c2
MMORPG-ress	MMORPG-ressa	k1gFnPc2
me	me	k?
pls	pls	k?
-	-	kIx~
oživ	oživa	k1gFnPc2
mě	já	k3xPp1nSc4
prosím	prosit	k5eAaImIp1nS
<g/>
)	)	kIx)
</s>
<s>
RIPRecovery	RIPRecovera	k1gFnPc1
Is	Is	k1gMnSc2
PossibleRecovery	PossibleRecovera	k1gFnSc2
Is	Is	k1gMnSc2
Possible	Possible	k1gMnSc2
(	(	kIx(
<g/>
RIP	RIP	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tak	tak	k6eAd1
se	se	k3xPyFc4
jmenuje	jmenovat	k5eAaBmIp3nS,k5eAaImIp3nS
systém	systém	k1gInSc1
zálohování	zálohování	k1gNnSc2
<g/>
,	,	kIx,
záchrany	záchrana	k1gFnSc2
a	a	k8xC
obnovení	obnovení	k1gNnSc2
počítačových	počítačový	k2eAgNnPc2d1
dat	datum	k1gNnPc2
</s>
<s>
R.	R.	kA
<g/>
I.	I.	kA
<g/>
P.	P.	kA
<g/>
Rest	rest	k6eAd1
In	In	k1gFnSc1
PeaceOdpočívej	PeaceOdpočívej	k1gFnSc2
v	v	k7c6
pokoji	pokoj	k1gInSc6
</s>
<s>
RLReal	RLReal	k1gInSc1
LifeReálný	LifeReálný	k2eAgInSc4d1
život	život	k1gInSc4
</s>
<s>
RN	RN	kA
</s>
<s>
Right	Right	k1gMnSc1
Now	Now	k1gMnSc1
</s>
<s>
Právě	právě	k9
teď	teď	k6eAd1
</s>
<s>
RSRuneScapeMMORPG	RSRuneScapeMMORPG	k?
hra	hra	k1gFnSc1
</s>
<s>
R.	R.	kA
<g/>
S.	S.	kA
<g/>
S.	S.	kA
<g/>
,	,	kIx,
rssrádo	rssráda	k1gFnSc5
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
</s>
<s>
RTRoger	RTRoger	k1gMnSc1
Thatrozumím	Thatrozumí	k1gNnPc3
tomu	ten	k3xDgNnSc3
<g/>
/	/	kIx~
<g/>
chápu	chápat	k5eAaImIp1nS
to	ten	k3xDgNnSc1
<g/>
/	/	kIx~
<g/>
porozuměl	porozumět	k5eAaPmAgInS
jsem	být	k5eAaImIp1nS
</s>
<s>
RTFMReboot	RTFMReboot	k1gInSc1
this	this	k6eAd1
fucking	fucking	k1gInSc1
machine	machinout	k5eAaPmIp3nS
<g/>
/	/	kIx~
<g/>
Read	Read	k1gInSc4
The	The	k1gFnSc2
Fucking	Fucking	k1gInSc4
ManualRestartuj	ManualRestartuj	k1gFnSc2
ten	ten	k3xDgInSc1
zasraný	zasraný	k2eAgInSc1d1
stroj	stroj	k1gInSc1
(	(	kIx(
<g/>
počítač	počítač	k1gInSc1
<g/>
)	)	kIx)
<g/>
/	/	kIx~
<g/>
Přečtěte	přečíst	k5eAaPmRp2nP
si	se	k3xPyFc3
kurva	kurva	k1gFnSc1
příručku	příručka	k1gFnSc4
</s>
<s>
RUOK	RUOK	kA
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
Are	ar	k1gInSc5
You	You	k1gMnPc6
O.K.	O.K.	k1gFnSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
Jsi	být	k5eAaImIp2nS
v	v	k7c6
pohodě	pohoda	k1gFnSc6
<g/>
?	?	kIx.
</s>
<s>
S	s	k7c7
</s>
<s>
zkratka	zkratka	k1gFnSc1
</s>
<s>
anglicky	anglicky	k6eAd1
</s>
<s>
česky	česky	k6eAd1
</s>
<s>
S8N	S8N	k4
</s>
<s>
Satan	Satan	k1gMnSc1
</s>
<s>
Satan	Satan	k1gMnSc1
<g/>
,	,	kIx,
čert	čert	k1gMnSc1
<g/>
,	,	kIx,
ďábel	ďábel	k1gMnSc1
</s>
<s>
SAFSee	SAFSee	k6eAd1
attached	attached	k1gInSc1
fileviz	fileviza	k1gFnPc2
přílohu	příloh	k1gInSc2
</s>
<s>
SBISo	SBISo	k6eAd1
Be	Be	k1gFnSc1
ItBudiž	ItBudiž	k1gFnSc1
</s>
<s>
SJC	SJC	kA
Staré	Staré	k2eAgFnPc1d1
Jak	jak	k8xS,k8xC
Cyp	Cyp	k1gFnPc1
</s>
<s>
SNAFUSituation	SNAFUSituation	k1gInSc1
Normal	Normal	k1gInSc1
<g/>
,	,	kIx,
All	All	k1gMnSc1
is	is	k?
Fucked	Fucked	k1gMnSc1
UpSituace	UpSituace	k1gFnSc2
normální	normální	k2eAgFnSc2d1
<g/>
,	,	kIx,
<g/>
všechno	všechen	k3xTgNnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
prdeli	prdel	k1gFnSc6
</s>
<s>
SOHOSmall	SOHOSmall	k1gMnSc1
Office	Office	kA
Home	Home	k1gFnSc1
Officemalá	Officemalý	k2eAgFnSc1d1
kancelář	kancelář	k1gFnSc1
domácí	domácí	k2eAgFnSc1d1
kancelář	kancelář	k1gFnSc1
-	-	kIx~
označení	označení	k1gNnSc1
pro	pro	k7c4
práci	práce	k1gFnSc4
z	z	k7c2
domova	domov	k1gInSc2
</s>
<s>
SOSSeal	SOSSeal	k1gInSc1
of	of	k?
starpečeť	starpečetit	k5eAaPmRp2nS
hvězd	hvězda	k1gFnPc2
<g/>
/	/	kIx~
<g/>
y	y	k?
(	(	kIx(
<g/>
ze	z	k7c2
hry	hra	k1gFnSc2
silkroad	silkroad	k6eAd1
online	onlinout	k5eAaPmIp3nS
<g/>
)	)	kIx)
</s>
<s>
SOMSeal	SOMSeal	k1gInSc1
of	of	k?
moonpečeť	moonpečetit	k5eAaPmRp2nS
měsíce	měsíc	k1gInPc4
(	(	kIx(
<g/>
ze	z	k7c2
hry	hra	k1gFnSc2
silkroad	silkroad	k6eAd1
online	onlinout	k5eAaPmIp3nS
<g/>
)	)	kIx)
</s>
<s>
SOSunSeal	SOSunSeal	k1gInSc1
of	of	k?
sunpečeť	sunpečetit	k5eAaPmRp2nS
slunce	slunce	k1gNnSc4
(	(	kIx(
<g/>
ze	z	k7c2
hry	hra	k1gFnSc2
silkroad	silkroad	k6eAd1
online	onlinout	k5eAaPmIp3nS
<g/>
)	)	kIx)
</s>
<s>
SROSilkRoad	SROSilkRoad	k6eAd1
onlinesoučasná	onlinesoučasný	k2eAgFnSc1d1
MMORPG	MMORPG	kA
(	(	kIx(
<g/>
hedvábná	hedvábný	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
SRYSorryOmlouvám	SRYSorryOmlouvat	k5eAaImIp1nS,k5eAaPmIp1nS
se	se	k3xPyFc4
</s>
<s>
SUSFUSituation	SUSFUSituation	k1gInSc1
Unchanged	Unchanged	k1gInSc1
<g/>
,	,	kIx,
Still	Still	k1gMnSc1
Fucked	Fucked	k1gMnSc1
UpSituace	UpSituace	k1gFnSc1
beze	beze	k7c2
změn	změna	k1gFnPc2
<g/>
,	,	kIx,
stále	stále	k6eAd1
v	v	k7c6
prdeli	prdel	k1gFnSc6
</s>
<s>
SUYNShut	SUYNShut	k1gInSc1
up	up	k?
You	You	k1gFnSc2
N	N	kA
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
BDrž	BDrž	k1gFnSc1
hubu	huba	k1gFnSc4
ty	ty	k3xPp2nSc5
n	n	k0
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
be	be	k?
(	(	kIx(
<g/>
n	n	k0
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
b	b	k?
:	:	kIx,
Označuje	označovat	k5eAaImIp3nS
hráče	hráč	k1gMnSc4
začátečníka	začátečník	k1gMnSc4
<g/>
.	.	kIx.
<g/>
Též	též	k9
Lama	lama	k1gFnSc1
(	(	kIx(
<g/>
počítačový	počítačový	k2eAgInSc1d1
žargon	žargon	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
SWEINSo	SWEINSo	k6eAd1
What	What	k2eAgInSc1d1
Else	Elsa	k1gFnSc3
Is	Is	k1gFnSc2
New	New	k1gFnSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
Tak	tak	k6eAd1
co	co	k3yInSc1,k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
ještě	ještě	k9
nového	nový	k2eAgMnSc4d1
<g/>
?	?	kIx.
</s>
<s>
T	T	kA
</s>
<s>
zkratka	zkratka	k1gFnSc1
</s>
<s>
anglicky	anglicky	k6eAd1
</s>
<s>
česky	česky	k6eAd1
</s>
<s>
TANJThere	TANJThrat	k5eAaPmIp3nS
Ain	Ain	k1gFnSc1
<g/>
'	'	kIx"
<g/>
t	t	k?
No	no	k9
Justicenení	Justicenení	k1gNnSc1
spravedlnosti	spravedlnost	k1gFnSc2
</s>
<s>
TBDTo	TBDTo	k6eAd1
Be	Be	k1gFnSc1
Doneje	Donej	k1gInSc2
nutno	nutno	k6eAd1
ještě	ještě	k6eAd1
udělat	udělat	k5eAaPmF
</s>
<s>
TBTKtobě	TBTKtobě	k6eAd1
taky	taky	k6eAd1
</s>
<s>
TGBADQTry	TGBADQTra	k1gFnPc1
Google	Google	k1gFnSc2
Before	Befor	k1gInSc5
Asking	Asking	k1gInSc1
Dumb	Dumb	k1gMnSc1
QuestionsZkus	QuestionsZkus	k1gMnSc1
google	google	k6eAd1
<g/>
,	,	kIx,
než	než	k8xS
se	se	k3xPyFc4
začneš	začít	k5eAaPmIp2nS
hloupě	hloupě	k6eAd1
ptát	ptát	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
TGIFThank	TGIFThank	k1gInSc1
god	god	k?
it	it	k?
<g/>
'	'	kIx"
<g/>
s	s	k7c7
FridayDíky	FridayDík	k1gMnPc7
Bohu	bůh	k1gMnSc3
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
už	už	k6eAd1
pátek	pátek	k1gInSc4
</s>
<s>
TBHTo	TBHTo	k1gMnSc1
Be	Be	k1gMnSc1
Honestabych	Honestabych	k1gMnSc1
byl	být	k5eAaImAgMnS
upřímný	upřímný	k2eAgMnSc1d1
</s>
<s>
THCThanksdíky	THCThanksdík	k1gInPc1
(	(	kIx(
<g/>
stejné	stejné	k1gNnSc1
jako	jako	k8xS,k8xC
THX	THX	kA
<g/>
,	,	kIx,
ale	ale	k8xC
méně	málo	k6eAd2
užívané	užívaný	k2eAgInPc1d1
<g/>
)	)	kIx)
</s>
<s>
THXThanksdíky	THXThanksdík	k1gInPc1
</s>
<s>
THX	THX	kA
<g/>
1	#num#	k4
<g/>
E	E	kA
<g/>
6	#num#	k4
<g/>
Thanks	Thanksa	k1gFnPc2
a	a	k8xC
millionděkuji	millionděkovat	k5eAaImIp1nS,k5eAaBmIp1nS,k5eAaPmIp1nS
milionkrát	milionkrát	k6eAd1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
E	E	kA
<g/>
6	#num#	k4
=	=	kIx~
1	#num#	k4
000	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
TIAThanks	TIAThanks	k6eAd1
In	In	k1gMnSc1
Advanceděkuji	Advanceděkuji	k1gMnSc1
předem	předem	k6eAd1
</s>
<s>
TINAThere	TINAThrat	k5eAaPmIp3nS
Is	Is	k1gFnPc4
No	no	k9
Alternativek	Alternativek	k1gInSc1
tomu	ten	k3xDgMnSc3
není	být	k5eNaImIp3nS
žádné	žádný	k3yNgFnPc4
alternativy	alternativa	k1gFnPc4
</s>
<s>
TINARThis	TINARThis	k1gInSc4
Is	Is	k1gMnPc3
Not	nota	k1gFnPc2
A	a	k9
Recommendationtoto	Recommendationtota	k1gFnSc5
není	být	k5eNaImIp3nS
žádné	žádný	k3yNgNnSc4
doporučení	doporučení	k1gNnSc4
</s>
<s>
TJNTo	TJNTo	k6eAd1
jo	jo	k9
no	no	k9
</s>
<s>
TK	TK	kA
</s>
<s>
tak	tak	k6eAd1
</s>
<s>
TKCTak	TKCTak	k6eAd1
co	co	k3yQnSc4,k3yInSc4,k3yRnSc4
</s>
<s>
TLAThree	TLAThreat	k5eAaPmIp3nS
Letter	Letter	k1gInSc1
Acronymtřípísmenná	Acronymtřípísmenný	k2eAgFnSc1d1
zkratka	zkratka	k1gFnSc1
</s>
<s>
tl	tl	k?
<g/>
;	;	kIx,
<g/>
dr	dr	kA
(	(	kIx(
<g/>
TLDR	TLDR	kA
<g/>
)	)	kIx)
<g/>
too	too	k?
long	long	k1gInSc1
<g/>
;	;	kIx,
didn	didn	k1gInSc1
<g/>
'	'	kIx"
<g/>
t	t	k?
readmoc	readmoc	k6eAd1
dlouhé	dlouhý	k2eAgNnSc1d1
<g/>
,	,	kIx,
nečetl	číst	k5eNaImAgMnS
jsem	být	k5eAaImIp1nS
(	(	kIx(
<g/>
ve	v	k7c6
smyslu	smysl	k1gInSc6
<g/>
:	:	kIx,
„	„	k?
<g/>
ve	v	k7c6
zkratce	zkratka	k1gFnSc6
<g/>
:	:	kIx,
<g/>
“	“	k?
<g/>
)	)	kIx)
</s>
<s>
TLFMoment	TLFMoment	k1gInSc4
<g/>
,	,	kIx,
telefonuji	telefonovat	k5eAaImIp1nS
</s>
<s>
TTFNTa-ta	TTFNTa-to	k1gNnSc2
for	forum	k1gNnPc2
nowzatím	nowzatit	k5eAaImIp1nS,k5eAaPmIp1nS
pa-pa	pa-p	k1gMnSc4
</s>
<s>
TTJtak	TTJtak	k6eAd1
to	ten	k3xDgNnSc1
jo	jo	k9
</s>
<s>
TTKtak	TTKtak	k6eAd1
to	ten	k3xDgNnSc1
každopádně	každopádně	k6eAd1
</s>
<s>
TTMtaky	TTMtak	k1gInPc4
tě	ty	k3xPp2nSc4
miluju	milovat	k5eAaImIp1nS
</s>
<s>
TTTthis	TTTthis	k1gFnSc1
time	time	k1gFnSc1
tomorrowzítra	tomorrowzítra	k1gFnSc1
touhle	tenhle	k3xDgFnSc7
dobou	doba	k1gFnSc7
</s>
<s>
TTYLTalk	TTYLTalk	k6eAd1
To	ten	k3xDgNnSc1
You	You	k1gFnSc3
LaterPokecáme	LaterPokecáme	k1gFnSc2
později	pozdě	k6eAd2
</s>
<s>
TYThank	TYThank	k6eAd1
youděkuji	youděkovat	k5eAaImIp1nS,k5eAaBmIp1nS,k5eAaPmIp1nS
</s>
<s>
THXThanksdíky	THXThanksdík	k1gInPc1
</s>
<s>
TSTeam	TSTeam	k6eAd1
Speak	Speak	k1gInSc1
</s>
<s>
TTDNto	TTDNto	k1gNnSc1
teda	teda	k?
ne	ne	k9
</s>
<s>
TTPJtak	TTPJtak	k6eAd1
to	ten	k3xDgNnSc1
potom	potom	k6eAd1
jo	jo	k9
</s>
<s>
TXThanksdíky	TXThanksdík	k1gInPc1
</s>
<s>
U	u	k7c2
</s>
<s>
zkratka	zkratka	k1gFnSc1
</s>
<s>
anglicky	anglicky	k6eAd1
</s>
<s>
česky	česky	k6eAd1
</s>
<s>
U	u	k7c2
</s>
<s>
you	you	k?
</s>
<s>
ty	ten	k3xDgFnPc1
</s>
<s>
U	u	k7c2
<g/>
2	#num#	k4
<g/>
You	You	k1gFnSc2
tooty	toota	k1gFnSc2
<g/>
/	/	kIx~
<g/>
tobě	ty	k3xPp2nSc3
taky	taky	k6eAd1
</s>
<s>
UGBADQUse	UGBADQUs	k1gMnSc5
Google	Googl	k1gMnSc5
Before	Befor	k1gMnSc5
Asking	Asking	k1gInSc1
Dumb	Dumb	k1gInSc1
Questionspoužij	Questionspoužij	k1gMnSc2
Google	Googl	k1gMnSc2
namísto	namísto	k7c2
ptaní	ptaní	k1gNnSc2
se	se	k3xPyFc4
na	na	k7c4
hloupé	hloupý	k2eAgFnPc4d1
otázky	otázka	k1gFnPc4
</s>
<s>
UPAúplně	UPAúplně	k6eAd1
</s>
<s>
UPEúplně	UPEúplně	k6eAd1
</s>
<s>
UFO	UFO	kA
/	/	kIx~
UFLOUnidentified	UFLOUnidentified	k1gInSc1
Ferrari-looking	Ferrari-looking	k1gInSc1
Objectneidentifikovatelný	Objectneidentifikovatelný	k2eAgMnSc1d1
jako	jako	k8xS,k8xC
Ferrari	Ferrari	k1gMnSc1
vypadající	vypadající	k2eAgInSc4d1
objekt	objekt	k1gInSc4
</s>
<s>
UHOuniverzální	UHOuniverzální	k2eAgFnSc1d1
hnědá	hnědý	k2eAgFnSc1d1
omáčka	omáčka	k1gFnSc1
</s>
<s>
URYou	URYa	k1gFnSc7
arety	areta	k1gFnSc2
jsi	být	k5eAaImIp2nS
<g/>
/	/	kIx~
<g/>
vy	vy	k3xPp2nPc1
jste	být	k5eAaImIp2nP
<g/>
...	...	k?
</s>
<s>
URWYou	URWYou	k6eAd1
Are	ar	k1gInSc5
Welcomerádo	Welcomeráda	k1gFnSc5
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
</s>
<s>
UNHAUniverzální	UNHAUniverzální	k2eAgInSc1d1
Hadr	hadr	k1gInSc1
na	na	k7c4
všechno	všechen	k3xTgNnSc4
</s>
<s>
UTFGUse	UTFGUse	k1gFnSc1
The	The	k1gMnPc2
Fucking	Fucking	k1gInSc4
Googlepoužij	Googlepoužij	k1gFnPc2
<g/>
/	/	kIx~
<g/>
používej	používat	k5eAaImRp2nS
ten	ten	k3xDgMnSc1
zas	zas	k6eAd1
<g/>
...	...	k?
Google	Google	k1gFnSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
(	(	kIx(
<g/>
význam	význam	k1gInSc4
UGBADQ	UGBADQ	kA
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
</s>
<s>
zkratka	zkratka	k1gFnSc1
</s>
<s>
anglicky	anglicky	k6eAd1
</s>
<s>
česky	česky	k6eAd1
</s>
<s>
VT	VT	kA
<g/>
,	,	kIx,
VNTventrilo	VNTventrila	k1gFnSc5
</s>
<s>
VBCvůbec	VBCvůbec	k1gMnSc1
</s>
<s>
W	W	kA
</s>
<s>
zkratka	zkratka	k1gFnSc1
</s>
<s>
anglicky	anglicky	k6eAd1
</s>
<s>
česky	česky	k6eAd1
</s>
<s>
W	W	kA
<g/>
8	#num#	k4
<g/>
Waitpočkej	Waitpočkej	k1gFnSc2
</s>
<s>
WAIDWhat	WAIDWhat	k5eAaPmF,k5eAaBmF,k5eAaImF
An	An	k1gMnSc3
Interesting	Interesting	k1gInSc4
DevelopmentJaký	DevelopmentJaký	k2eAgInSc4d1
to	ten	k3xDgNnSc4
ale	ale	k8xC
zajímavý	zajímavý	k2eAgInSc1d1
vývoj	vývoj	k1gInSc1
událostí	událost	k1gFnPc2
</s>
<s>
WBWelcome	WBWelcom	k1gInSc5
Backvítej	Backvítej	k1gFnSc6
zpět	zpět	k6eAd1
</s>
<s>
WCGWo	WCGWo	k6eAd1
co	co	k9
go	go	k?
<g/>
?	?	kIx.
</s>
<s desamb="1">
O	o	k7c4
co	co	k3yQnSc4,k3yInSc4,k3yRnSc4
jde	jít	k5eAaImIp3nS
<g/>
?	?	kIx.
</s>
<s>
WDWell	WDWell	k1gMnSc1
Donevýborně	Donevýborně	k1gMnSc1
</s>
<s>
WIYFWikipedia	WIYFWikipedium	k1gNnSc2
is	is	k?
your	your	k1gInSc4
friendWikipedie	friendWikipedie	k1gFnSc2
je	být	k5eAaImIp3nS
tvůj	tvůj	k3xOp2gMnSc1
přítel	přítel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
WPWell	WPWell	k1gInSc4
PlayedDobře	PlayedDobř	k1gFnSc2
zahráno	zahrát	k5eAaPmNgNnS
(	(	kIx(
<g/>
Používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
MP	MP	kA
hrách	hrách	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
WOWWorld	WOWWorld	k6eAd1
of	of	k?
WarcraftMMORPG	WarcraftMMORPG	k1gFnSc1
hra	hra	k1gFnSc1
</s>
<s>
WTFWhat	WTFWhat	k5eAaImF,k5eAaPmF,k5eAaBmF
The	The	k1gFnSc4
FuckCo	FuckCo	k6eAd1
to	ten	k3xDgNnSc4
sakra	sakra	k0
(	(	kIx(
<g/>
je	být	k5eAaImIp3nS
<g/>
/	/	kIx~
<g/>
znamená	znamenat	k5eAaImIp3nS
<g/>
)	)	kIx)
<g/>
?	?	kIx.
</s>
<s>
WTGWay	WTGWaa	k1gFnPc4
To	to	k9
GoTakhle	GoTakhle	k1gFnPc4
se	se	k3xPyFc4
to	ten	k3xDgNnSc1
má	mít	k5eAaImIp3nS
dělat	dělat	k5eAaImF
<g/>
!	!	kIx.
</s>
<s desamb="1">
(	(	kIx(
<g/>
ocenění	ocenění	k1gNnSc4
něčího	něčí	k3xOyIgInSc2
výkonu	výkon	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
WTS	WTS	kA
<g/>
…	…	k?
<g/>
Want	Want	k1gMnSc1
to	ten	k3xDgNnSc4
sell	selnout	k5eAaPmAgMnS
<g/>
…	…	k?
<g/>
chci	chtít	k5eAaImIp1nS
prodat	prodat	k5eAaPmF
<g/>
…	…	k?
</s>
<s>
WTB	WTB	kA
<g/>
…	…	k?
<g/>
Want	Want	k2eAgMnSc1d1
to	ten	k3xDgNnSc1
buy	buy	k?
<g/>
...	...	k?
<g/>
chci	chtít	k5eAaImIp1nS
koupit	koupit	k5eAaPmF
<g/>
…	…	k?
</s>
<s>
WTT	WTT	kA
<g/>
…	…	k?
<g/>
Want	Want	k1gInSc1
to	ten	k3xDgNnSc4
trade	trade	k6eAd1
<g/>
...	...	k?
<g/>
chci	chtít	k5eAaImIp1nS
vyměnit	vyměnit	k5eAaPmF
<g/>
…	…	k?
</s>
<s>
WTZWo	WTZWo	k6eAd1
tým	tým	k1gInSc1
žádná	žádný	k3yNgNnPc4
<g/>
!	!	kIx.
</s>
<s desamb="1">
(	(	kIx(
<g/>
=	=	kIx~
O	o	k7c6
tom	ten	k3xDgNnSc6
žádná	žádný	k3yNgFnSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
,	,	kIx,
vizte	vidět	k5eAaImRp2nP
též	též	k9
Kompost	kompost	k1gInSc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
)	)	kIx)
</s>
<s>
WYSIWYGWhat	WYSIWYGWhat	k5eAaImF,k5eAaPmF,k5eAaBmF
you	you	k?
see	see	k?
is	is	k?
what	what	k1gInSc1
you	you	k?
getCo	getCo	k6eAd1
vidíš	vidět	k5eAaImIp2nS
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
dostaneš	dostat	k5eAaPmIp2nS
<g/>
.	.	kIx.
</s>
<s>
WYSIWYMWhat	WYSIWYMWhat	k5eAaBmF,k5eAaPmF,k5eAaImF
you	you	k?
see	see	k?
is	is	k?
what	what	k1gInSc1
you	you	k?
meanCo	meanCo	k6eAd1
vidíš	vidět	k5eAaImIp2nS
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
máš	mít	k5eAaImIp2nS
na	na	k7c6
mysli	mysl	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
WWJDWhat	WWJDWhat	k5eAaImF,k5eAaBmF,k5eAaPmF
would	would	k6eAd1
Jesus	Jesus	k1gInSc4
do	do	k7c2
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
Co	co	k3yInSc1,k3yRnSc1,k3yQnSc1
by	by	kYmCp3nS
udělal	udělat	k5eAaPmAgMnS
Ježíš	Ježíš	k1gMnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nS
se	se	k3xPyFc4
v	v	k7c6
této	tento	k3xDgFnSc6
situaci	situace	k1gFnSc6
zachoval	zachovat	k5eAaPmAgMnS
<g/>
?	?	kIx.
</s>
<s>
WWDDWhat	WWDDWhat	k5eAaBmF,k5eAaPmF,k5eAaImF
would	would	k1gInSc4
Devil	Devila	k1gFnPc2
do	do	k7c2
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
Co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
by	by	kYmCp3nS
udělal	udělat	k5eAaPmAgMnS
Ďábel	ďábel	k1gMnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nS
se	se	k3xPyFc4
v	v	k7c6
této	tento	k3xDgFnSc6
situaci	situace	k1gFnSc6
zachoval	zachovat	k5eAaPmAgMnS
<g/>
?	?	kIx.
</s>
<s>
X	X	kA
</s>
<s>
zkratka	zkratka	k1gFnSc1
</s>
<s>
anglicky	anglicky	k6eAd1
</s>
<s>
česky	česky	k6eAd1
</s>
<s>
XOXOhugs	XOXOhugs	k1gInSc1
and	and	k?
kisses	kisses	k1gInSc1
</s>
<s>
objetí	objetí	k1gNnSc1
a	a	k8xC
polibky	polibek	k1gInPc1
</s>
<s>
Y	Y	kA
</s>
<s>
zkratka	zkratka	k1gFnSc1
</s>
<s>
anglicky	anglicky	k6eAd1
</s>
<s>
česky	česky	k6eAd1
</s>
<s>
YHBTYou	YHBTYou	k6eAd1
have	havat	k5eAaPmIp3nS
been	been	k1gInSc1
Trolledněkdo	Trolledněkdo	k1gNnSc1
si	se	k3xPyFc3
z	z	k7c2
tebe	ty	k3xPp2nSc4
utahuje	utahovat	k5eAaImIp3nS
<g/>
!	!	kIx.
</s>
<s>
YHLYou	YHLYou	k6eAd1
Have	Have	k1gFnSc1
Lostprohrál	Lostprohrála	k1gFnPc2
jsi	být	k5eAaImIp2nS
</s>
<s>
YMMDYou	YMMDYý	k2eAgFnSc4d1
Made	Made	k1gFnSc4
My	my	k3xPp1nPc1
Dayzachránil	Dayzachránil	k1gMnSc2
jsi	být	k5eAaImIp2nS
mi	já	k3xPp1nSc3
tento	tento	k3xDgInSc4
den	den	k1gInSc4
</s>
<s>
YOMyou	YOMyou	k6eAd1
owe	owe	k?
medlužíš	medlužit	k5eAaPmIp2nS
mi	já	k3xPp1nSc3
</s>
<s>
YWyou	YWyít	k5eAaPmIp3nP,k5eAaBmIp3nP
<g/>
'	'	kIx"
<g/>
re	re	k9
welcomenemáte	welcomenemat	k5eAaPmIp2nP,k5eAaImIp2nP
zač	zač	k6eAd1
</s>
<s>
YKWIMYou	YKWIMYou	k6eAd1
know	know	k?
what	whata	k1gFnPc2
I	i	k8xC
meanTy	meanTa	k1gFnSc2
víš	vědět	k5eAaImIp2nS
co	co	k9
mám	mít	k5eAaImIp1nS
namysli	namyslet	k5eAaPmRp2nS,k5eAaBmRp2nS
(	(	kIx(
<g/>
Ty	ty	k3xPp2nSc1
víš	vědět	k5eAaImIp2nS
co	co	k9
myslím	myslet	k5eAaImIp1nS
<g/>
)	)	kIx)
</s>
<s>
Z	z	k7c2
</s>
<s>
zkratka	zkratka	k1gFnSc1
</s>
<s>
anglicky	anglicky	k6eAd1
</s>
<s>
česky	česky	k6eAd1
</s>
<s>
Z	z	k7c2
<g/>
5	#num#	k4
<g/>
Zpět	zpět	k6eAd1
</s>
<s>
ZTTZjisti	ZTTZjist	k1gMnPc1
to	ten	k3xDgNnSc4
</s>
<s>
Bibliografie	bibliografie	k1gFnSc1
</s>
<s>
DOSTÁLOVÁ	Dostálová	k1gFnSc1
<g/>
,	,	kIx,
Alena	Alena	k1gFnSc1
<g/>
,	,	kIx,
Výrazy	výraz	k1gInPc1
přejaté	přejatý	k2eAgInPc1d1
z	z	k7c2
angličtiny	angličtina	k1gFnSc2
v	v	k7c6
současných	současný	k2eAgInPc6d1
časopisech	časopis	k1gInPc6
pro	pro	k7c4
mládež	mládež	k1gFnSc4
a	a	k8xC
jejich	jejich	k3xOp3gFnSc1
recepce	recepce	k1gFnSc1
<g/>
,	,	kIx,
Bakalářská	bakalářský	k2eAgFnSc1d1
práce	práce	k1gFnSc1
<g/>
,	,	kIx,
FSS	FSS	kA
MUNI	MUNI	k?
<g/>
,	,	kIx,
https://is.muni.cz/auth/th/215926/pedf_b/Bakalarska_prace.pdf	https://is.muni.cz/auth/th/215926/pedf_b/Bakalarska_prace.pdf	k1gMnSc1
</s>
<s>
HERMANOVÁ	Hermanová	k1gFnSc1
<g/>
,	,	kIx,
Ludmila	Ludmila	k1gFnSc1
<g/>
,	,	kIx,
Český	český	k2eAgInSc1d1
a	a	k8xC
ruský	ruský	k2eAgInSc1d1
studentský	studentský	k2eAgInSc1d1
slang	slang	k1gInSc1
<g/>
,	,	kIx,
Diplomová	diplomový	k2eAgFnSc1d1
práce	práce	k1gFnSc1
<g/>
,	,	kIx,
FSS	FSS	kA
MUNI	MUNI	k?
<g/>
,	,	kIx,
https://is.muni.cz/auth/th/362834/ff_m/Cesky_a_rusky_studentsky_slang.pdf	https://is.muni.cz/auth/th/362834/ff_m/Cesky_a_rusky_studentsky_slang.pdf	k1gMnSc1
</s>
