<s>
Ústav	ústav	k1gInSc1
pro	pro	k7c4
jazyk	jazyk	k1gInSc4
(	(	kIx(
<g/>
Sarajevo	Sarajevo	k1gNnSc4
<g/>
)	)	kIx)
</s>
<s>
Ústav	ústav	k1gInSc1
pro	pro	k7c4
jazyk	jazyk	k1gInSc4
(	(	kIx(
<g/>
srbochorvatsky	srbochorvatsky	k6eAd1
<g/>
/	/	kIx~
<g/>
bosensky	bosensky	k6eAd1
Institut	institut	k1gInSc1
za	za	k7c4
jezik	jezik	k1gInSc4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
vědecké	vědecký	k2eAgNnSc4d1
pracoviště	pracoviště	k1gNnSc4
v	v	k7c6
Sarajevu	Sarajevo	k1gNnSc6
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
se	se	k3xPyFc4
věnuje	věnovat	k5eAaPmIp3nS,k5eAaImIp3nS
převážně	převážně	k6eAd1
studiu	studio	k1gNnSc3
bosenského	bosenský	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
srbochorvatštiny	srbochorvatština	k1gFnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Dějiny	dějiny	k1gFnPc1
</s>
<s>
Organizace	organizace	k1gFnSc1
vznikla	vzniknout	k5eAaPmAgFnS
roku	rok	k1gInSc2
1972	#num#	k4
na	na	k7c6
základě	základ	k1gInSc6
Zákona	zákon	k1gInSc2
o	o	k7c6
Ústavu	ústav	k1gInSc6
pro	pro	k7c4
jazyk	jazyk	k1gInSc4
(	(	kIx(
<g/>
Zakonom	Zakonom	k1gInSc4
o	o	k7c6
Institutu	institut	k1gInSc6
za	za	k7c4
jezik	jezik	k1gInSc4
<g/>
,	,	kIx,
Službeni	Služben	k2eAgMnPc1d1
list	lista	k1gFnPc2
SR	SR	kA
BiH	BiH	k1gFnSc2
č.	č.	k?
4	#num#	k4
<g/>
/	/	kIx~
<g/>
72	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1977	#num#	k4
se	se	k3xPyFc4
ústav	ústav	k1gInSc1
stal	stát	k5eAaPmAgInS
součástí	součást	k1gFnSc7
Filozofické	filozofický	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
Univerzity	univerzita	k1gFnSc2
v	v	k7c6
Sarajevu	Sarajevo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1991	#num#	k4
se	se	k3xPyFc4
instituce	instituce	k1gFnSc1
rozdělila	rozdělit	k5eAaPmAgFnS
na	na	k7c4
Ústav	ústav	k1gInSc4
pro	pro	k7c4
jazyk	jazyk	k1gInSc4
a	a	k8xC
Ústav	ústav	k1gInSc1
pro	pro	k7c4
literaturu	literatura	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
roku	rok	k1gInSc2
byla	být	k5eAaImAgFnS
organizace	organizace	k1gFnSc1
financována	financovat	k5eAaBmNgFnS
ze	z	k7c2
státního	státní	k2eAgInSc2d1
rozpočtu	rozpočet	k1gInSc2
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
1994	#num#	k4
Federací	federace	k1gFnSc7
Bosny	Bosna	k1gFnSc2
a	a	k8xC
Hercegoviny	Hercegovina	k1gFnSc2
a	a	k8xC
konec	konec	k1gInSc1
od	od	k7c2
roku	rok	k1gInSc2
1996	#num#	k4
Kantonem	Kanton	k1gInSc7
Sarajevo	Sarajevo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
2016	#num#	k4
v	v	k7c6
ústavu	ústav	k1gInSc6
pracovalo	pracovat	k5eAaImAgNnS
9	#num#	k4
osob	osoba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rada	rada	k1gFnSc1
organizace	organizace	k1gFnSc2
čítá	čítat	k5eAaImIp3nS
5	#num#	k4
členů	člen	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Ústav	ústav	k1gInSc1
vydává	vydávat	k5eAaPmIp3nS,k5eAaImIp3nS
časopis	časopis	k1gInSc1
Književni	Književni	k1gFnSc2
jezik	jezika	k1gFnPc2
(	(	kIx(
<g/>
Spisovný	spisovný	k2eAgInSc1d1
jazyk	jazyk	k1gInSc1
<g/>
,	,	kIx,
1972	#num#	k4
<g/>
–	–	k?
<g/>
1991	#num#	k4
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
–	–	k?
<g/>
2004	#num#	k4
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
)	)	kIx)
a	a	k8xC
odborné	odborný	k2eAgFnPc4d1
publikace	publikace	k1gFnPc4
různého	různý	k2eAgNnSc2d1
zaměření	zaměření	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Členění	členění	k1gNnSc1
ústavu	ústav	k1gInSc2
</s>
<s>
Sekce	sekce	k1gFnSc1
vědecká	vědecký	k2eAgFnSc1d1
a	a	k8xC
výzkumná	výzkumný	k2eAgFnSc1d1
(	(	kIx(
<g/>
Naučnoistraživački	Naučnoistraživački	k1gNnPc2
sektor	sektor	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Oddělení	oddělení	k1gNnSc1
pro	pro	k7c4
studium	studium	k1gNnSc4
normativního	normativní	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
(	(	kIx(
<g/>
Odjeljenje	Odjeljenje	k1gFnSc1
za	za	k7c2
proučavanje	proučavanj	k1gInSc2
standardnog	standardnog	k1gInSc1
jezika	jezika	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Oddělení	oddělení	k1gNnSc1
pro	pro	k7c4
studium	studium	k1gNnSc4
dějin	dějiny	k1gFnPc2
jazyka	jazyk	k1gInSc2
(	(	kIx(
<g/>
Odjeljenje	Odjeljenje	k1gFnSc1
za	za	k7c4
proučavanje	proučavanj	k1gMnPc4
historije	historít	k5eAaPmIp3nS
jezika	jezika	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Oddělení	oddělení	k1gNnSc1
pro	pro	k7c4
studium	studium	k1gNnSc4
dialektologie	dialektologie	k1gFnSc2
(	(	kIx(
<g/>
Odjeljenje	Odjeljenje	k1gFnSc1
za	za	k7c4
proučavanje	proučavanj	k1gMnPc4
dijalektologije	dijalektologít	k5eAaPmIp3nS
<g/>
)	)	kIx)
</s>
<s>
Oddělení	oddělení	k1gNnSc4
lexikografie	lexikografie	k1gFnSc2
(	(	kIx(
<g/>
Odjeljenje	Odjeljenj	k1gFnSc2
za	za	k7c4
leksikografiju	leksikografít	k5eAaPmIp1nS
<g/>
)	)	kIx)
</s>
<s>
Sekce	sekce	k1gFnSc1
administrativně	administrativně	k6eAd1
správní	správní	k2eAgFnSc2d1
(	(	kIx(
<g/>
Upravno-administrativni	Upravno-administrativni	k1gFnSc2
sektor	sektor	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Sekce	sekce	k1gFnSc1
účetní	účetní	k1gFnSc1
(	(	kIx(
<g/>
Sektor	sektor	k1gInSc1
računovodstva	računovodstvo	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
Sekce	sekce	k1gFnSc1
nakladatelská	nakladatelský	k2eAgFnSc1d1
a	a	k8xC
knihovní	knihovní	k2eAgFnSc1d1
(	(	kIx(
<g/>
Sektor	sektor	k1gInSc1
izdavačke	izdavačk	k1gFnSc2
djelatnosti	djelatnost	k1gFnSc2
i	i	k8xC
biblioteke	bibliotek	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Ředitelé	ředitel	k1gMnPc1
</s>
<s>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
Josip	Josip	k1gMnSc1
Baotić	Baotić	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
<g/>
1942	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
Alen	Alena	k1gFnPc2
Kalajdžija	Kalajdžijum	k1gNnSc2
(	(	kIx(
<g/>
*	*	kIx~
<g/>
1977	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
