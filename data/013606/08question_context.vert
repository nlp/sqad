<s>
Ústav	ústav	k1gInSc1
pro	pro	k7c4
jazyk	jazyk	k1gInSc4
(	(	kIx(
<g/>
srbochorvatsky	srbochorvatsky	k6eAd1
<g/>
/	/	kIx~
<g/>
bosensky	bosensky	k6eAd1
Institut	institut	k1gInSc1
za	za	k7c4
jezik	jezik	k1gInSc4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
vědecké	vědecký	k2eAgNnSc4d1
pracoviště	pracoviště	k1gNnSc4
v	v	k7c6
Sarajevu	Sarajevo	k1gNnSc6
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
se	se	k3xPyFc4
věnuje	věnovat	k5eAaPmIp3nS,k5eAaImIp3nS
převážně	převážně	k6eAd1
studiu	studio	k1gNnSc3
bosenského	bosenský	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
srbochorvatštiny	srbochorvatština	k1gFnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>