<s>
Livermorium	Livermorium	k1gNnSc1	Livermorium
<g/>
,	,	kIx,	,
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Lv	Lv	k1gFnSc1	Lv
<g/>
,	,	kIx,	,
pův	pův	k?	pův
<g/>
.	.	kIx.	.
ununhexium	ununhexium	k1gNnSc1	ununhexium
<g/>
,	,	kIx,	,
lat.	lat.	k?	lat.
Livermorium	Livermorium	k1gNnSc1	Livermorium
je	být	k5eAaImIp3nS	být
transuran	transuran	k1gInSc4	transuran
s	s	k7c7	s
protonovým	protonový	k2eAgNnSc7d1	protonové
číslem	číslo	k1gNnSc7	číslo
116	[number]	k4	116
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
IUPAC	IUPAC	kA	IUPAC
předložila	předložit	k5eAaPmAgFnS	předložit
k	k	k7c3	k
veřejné	veřejný	k2eAgFnSc3d1	veřejná
diskusi	diskuse	k1gFnSc3	diskuse
své	svůj	k3xOyFgNnSc4	svůj
doporučení	doporučení	k1gNnSc4	doporučení
pojmenovat	pojmenovat	k5eAaPmF	pojmenovat
prvek	prvek	k1gInSc1	prvek
livermorium	livermorium	k1gNnSc1	livermorium
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
sídla	sídlo	k1gNnSc2	sídlo
ústavu	ústav	k1gInSc2	ústav
Lawrence	Lawrence	k1gFnSc2	Lawrence
Livermore	Livermor	k1gInSc5	Livermor
National	National	k1gMnSc2	National
Laboratory	Laborator	k1gInPc4	Laborator
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
poprvé	poprvé	k6eAd1	poprvé
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
syntézu	syntéza	k1gFnSc4	syntéza
prvku	prvek	k1gInSc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc1	návrh
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
dohody	dohoda	k1gFnSc2	dohoda
objevitelů	objevitel	k1gMnPc2	objevitel
prvků	prvek	k1gInPc2	prvek
114	[number]	k4	114
a	a	k8xC	a
116	[number]	k4	116
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc2	jejich
společného	společný	k2eAgInSc2d1	společný
návrhu	návrh	k1gInSc2	návrh
<g/>
.	.	kIx.	.
</s>
<s>
Ruští	ruský	k2eAgMnPc1d1	ruský
objevitelé	objevitel	k1gMnPc1	objevitel
předtím	předtím	k6eAd1	předtím
navrhovali	navrhovat	k5eAaImAgMnP	navrhovat
jméno	jméno	k1gNnSc4	jméno
moscovium	moscovium	k1gNnSc1	moscovium
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
Moskevské	moskevský	k2eAgFnSc2d1	Moskevská
oblasti	oblast	k1gFnSc2	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
leží	ležet	k5eAaImIp3nS	ležet
sídlo	sídlo	k1gNnSc4	sídlo
Spojeného	spojený	k2eAgInSc2d1	spojený
ústavu	ústav	k1gInSc2	ústav
jaderných	jaderný	k2eAgInPc2d1	jaderný
výzkumů	výzkum	k1gInPc2	výzkum
v	v	k7c6	v
Dubně	Dubna	k1gFnSc6	Dubna
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgNnSc1d1	Nové
jméno	jméno	k1gNnSc1	jméno
bylo	být	k5eAaImAgNnS	být
oficiálně	oficiálně	k6eAd1	oficiálně
schváleno	schválit	k5eAaPmNgNnS	schválit
IUPAC	IUPAC	kA	IUPAC
30	[number]	k4	30
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
oznámili	oznámit	k5eAaPmAgMnP	oznámit
vědci	vědec	k1gMnPc1	vědec
z	z	k7c2	z
Lawrence	Lawrenec	k1gMnSc2	Lawrenec
Berkeley	Berkelea	k1gMnSc2	Berkelea
National	National	k1gMnSc2	National
Laboratory	Laborator	k1gInPc1	Laborator
objev	objev	k1gInSc1	objev
prvků	prvek	k1gInPc2	prvek
livermoria	livermorium	k1gNnSc2	livermorium
a	a	k8xC	a
oganessonu	oganesson	k1gInSc2	oganesson
<g/>
,	,	kIx,	,
článek	článek	k1gInSc1	článek
byl	být	k5eAaImAgInS	být
publikován	publikovat	k5eAaBmNgInS	publikovat
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Physical	Physical	k1gFnSc2	Physical
Review	Review	k1gFnSc2	Review
Letters	Lettersa	k1gFnPc2	Lettersa
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
svůj	svůj	k3xOyFgInSc4	svůj
objev	objev	k1gInSc4	objev
stáhli	stáhnout	k5eAaPmAgMnP	stáhnout
zpět	zpět	k6eAd1	zpět
na	na	k7c6	na
základě	základ	k1gInSc6	základ
faktu	fakt	k1gInSc2	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
přípravu	příprava	k1gFnSc4	příprava
nepovedlo	povést	k5eNaPmAgNnS	povést
provést	provést	k5eAaPmF	provést
v	v	k7c6	v
jiné	jiný	k2eAgFnSc6d1	jiná
laboratoři	laboratoř	k1gFnSc6	laboratoř
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
publikovali	publikovat	k5eAaBmAgMnP	publikovat
vědci	vědec	k1gMnPc1	vědec
ze	z	k7c2	z
Spojeného	spojený	k2eAgInSc2d1	spojený
ústavu	ústav	k1gInSc2	ústav
jaderných	jaderný	k2eAgInPc2d1	jaderný
výzkumů	výzkum	k1gInPc2	výzkum
v	v	k7c6	v
Dubně	Dubn	k1gInSc6	Dubn
přípravu	příprava	k1gFnSc4	příprava
292	[number]	k4	292
<g/>
Lv	Lv	k1gFnPc2	Lv
ostřelováním	ostřelování	k1gNnPc3	ostřelování
atomů	atom	k1gInPc2	atom
248	[number]	k4	248
<g/>
Cm	cm	kA	cm
atomy	atom	k1gInPc7	atom
48	[number]	k4	48
<g/>
Ca	ca	kA	ca
<g/>
,	,	kIx,	,
pozdější	pozdní	k2eAgMnSc1d2	pozdější
analýzou	analýza	k1gFnSc7	analýza
byl	být	k5eAaImAgInS	být
výsledný	výsledný	k2eAgInSc1d1	výsledný
izotop	izotop	k1gInSc1	izotop
změněn	změnit	k5eAaPmNgInS	změnit
na	na	k7c4	na
293	[number]	k4	293
<g/>
Lv	Lv	k1gFnPc2	Lv
(	(	kIx(	(
<g/>
namísto	namísto	k7c2	namísto
4	[number]	k4	4
neutronů	neutron	k1gInPc2	neutron
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
emitovány	emitovat	k5eAaBmNgFnP	emitovat
pouze	pouze	k6eAd1	pouze
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
20	[number]	k4	20
:	:	kIx,	:
:	:	kIx,	:
48	[number]	k4	48
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
C	C	kA	C
a	a	k8xC	a
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
96	[number]	k4	96
:	:	kIx,	:
:	:	kIx,	:
248	[number]	k4	248
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
C	C	kA	C
m	m	kA	m
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
116	[number]	k4	116
:	:	kIx,	:
:	:	kIx,	:
296	[number]	k4	296
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
L	L	kA	L
v	v	k7c6	v
:	:	kIx,	:
:	:	kIx,	:
∗	∗	k?	∗
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
116	[number]	k4	116
:	:	kIx,	:
:	:	kIx,	:
293	[number]	k4	293
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
L	L	kA	L
v	v	k7c6	v
:	:	kIx,	:
+	+	kIx~	+
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
20	[number]	k4	20
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
48	[number]	k4	48
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
Ca	ca	kA	ca
<g/>
}	}	kIx)	}
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
96	[number]	k4	96
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s>
<g/>
248	[number]	k4	248
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
Cm	cm	kA	cm
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc4	ten
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
116	[number]	k4	116
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
296	[number]	k4	296
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
Lv	Lv	k1gFnSc1	Lv
<g/>
}	}	kIx)	}
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
*	*	kIx~	*
<g/>
<g />
.	.	kIx.	.
</s>
<s>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc4	ten
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
116	[number]	k4	116
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
293	[number]	k4	293
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
Lv	Lv	k1gFnSc1	Lv
<g/>
}	}	kIx)	}
+3	+3	k4	+3
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
}	}	kIx)	}
:	:	kIx,	:
Jeho	jeho	k3xOp3gInSc1	jeho
poločas	poločas	k1gInSc1	poločas
rozpadu	rozpad	k1gInSc2	rozpad
je	být	k5eAaImIp3nS	být
18	[number]	k4	18
milisekund	milisekunda	k1gFnPc2	milisekunda
a	a	k8xC	a
rozpadá	rozpadat	k5eAaImIp3nS	rozpadat
se	se	k3xPyFc4	se
na	na	k7c4	na
288	[number]	k4	288
<g/>
Fl	Fl	k1gFnPc2	Fl
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2001	[number]	k4	2001
institut	institut	k1gInSc1	institut
oznámil	oznámit	k5eAaPmAgInS	oznámit
přípravu	příprava	k1gFnSc4	příprava
druhého	druhý	k4xOgInSc2	druhý
atomu	atom	k1gInSc2	atom
<g/>
.	.	kIx.	.
</s>
<s>
Zjištěné	zjištěný	k2eAgFnPc1d1	zjištěná
vlastnosti	vlastnost	k1gFnPc1	vlastnost
potvrdily	potvrdit	k5eAaPmAgFnP	potvrdit
oblast	oblast	k1gFnSc4	oblast
zvýšené	zvýšený	k2eAgFnSc2d1	zvýšená
stability	stabilita	k1gFnSc2	stabilita
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
ostrůvek	ostrůvek	k1gInSc4	ostrůvek
stability	stabilita	k1gFnSc2	stabilita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Doposud	doposud	k6eAd1	doposud
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
následující	následující	k2eAgInPc1d1	následující
izotopy	izotop	k1gInPc1	izotop
livermoria	livermorium	k1gNnSc2	livermorium
<g/>
:	:	kIx,	:
Chalkogeny	Chalkogen	k1gInPc4	Chalkogen
Jaderná	jaderný	k2eAgFnSc1d1	jaderná
fyzika	fyzika	k1gFnSc1	fyzika
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
livermorium	livermorium	k1gNnSc4	livermorium
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
livermorium	livermorium	k1gNnSc4	livermorium
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Galerie	galerie	k1gFnSc2	galerie
livermorium	livermorium	k1gNnSc4	livermorium
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
