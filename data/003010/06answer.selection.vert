<s>
První	první	k4xOgFnPc1	první
pitvy	pitva	k1gFnPc1	pitva
byly	být	k5eAaImAgFnP	být
prováděny	provádět	k5eAaImNgFnP	provádět
ve	v	k7c6	v
starém	starý	k2eAgInSc6d1	starý
Egyptě	Egypt	k1gInSc6	Egypt
jako	jako	k9	jako
součást	součást	k1gFnSc4	součást
přípravy	příprava	k1gFnSc2	příprava
těl	tělo	k1gNnPc2	tělo
zemřelých	zemřelý	k1gMnPc2	zemřelý
pro	pro	k7c4	pro
mumifikaci	mumifikace	k1gFnSc4	mumifikace
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
zároveň	zároveň	k6eAd1	zároveň
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
získávání	získávání	k1gNnSc3	získávání
poznatků	poznatek	k1gInPc2	poznatek
o	o	k7c6	o
anatomii	anatomie	k1gFnSc6	anatomie
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
