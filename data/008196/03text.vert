<p>
<s>
André	André	k1gMnSc1	André
Navarra	Navarra	k1gFnSc1	Navarra
<g/>
,	,	kIx,	,
celým	celý	k2eAgNnSc7d1	celé
jménem	jméno	k1gNnSc7	jméno
André-Nicolas	André-Nicolasa	k1gFnPc2	André-Nicolasa
Navarra	Navarra	k1gFnSc1	Navarra
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1911	[number]	k4	1911
Biarrit	Biarrita	k1gFnPc2	Biarrita
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
–	–	k?	–
31	[number]	k4	31
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1988	[number]	k4	1988
Siena	Siena	k1gFnSc1	Siena
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
violoncellista	violoncellista	k1gMnSc1	violoncellista
a	a	k8xC	a
hudební	hudební	k2eAgMnSc1d1	hudební
pedagog	pedagog	k1gMnSc1	pedagog
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
do	do	k7c2	do
hudebnické	hudebnický	k2eAgFnSc2d1	hudebnická
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
kontrabasista	kontrabasista	k1gMnSc1	kontrabasista
italského	italský	k2eAgInSc2d1	italský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
rodiče	rodič	k1gMnPc1	rodič
jej	on	k3xPp3gMnSc4	on
připravovali	připravovat	k5eAaImAgMnP	připravovat
na	na	k7c4	na
hudební	hudební	k2eAgFnSc4d1	hudební
dráhu	dráha	k1gFnSc4	dráha
<g/>
,	,	kIx,	,
pořídili	pořídit	k5eAaPmAgMnP	pořídit
mu	on	k3xPp3gMnSc3	on
violoncello	violoncello	k1gNnSc4	violoncello
a	a	k8xC	a
nechali	nechat	k5eAaPmAgMnP	nechat
jej	on	k3xPp3gNnSc4	on
cvičit	cvičit	k5eAaImF	cvičit
stupnice	stupnice	k1gFnSc2	stupnice
a	a	k8xC	a
studovat	studovat	k5eAaImF	studovat
hudební	hudební	k2eAgFnSc4d1	hudební
teorii	teorie	k1gFnSc4	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
sedmi	sedm	k4xCc2	sedm
let	léto	k1gNnPc2	léto
začal	začít	k5eAaPmAgInS	začít
studovat	studovat	k5eAaImF	studovat
hru	hra	k1gFnSc4	hra
na	na	k7c4	na
violoncello	violoncello	k1gNnSc4	violoncello
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
na	na	k7c4	na
Toulouskou	toulouský	k2eAgFnSc4d1	Toulouská
konzervatoř	konzervatoř	k1gFnSc4	konzervatoř
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
dokončil	dokončit	k5eAaPmAgInS	dokončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
<g/>
,	,	kIx,	,
ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
třinácti	třináct	k4xCc6	třináct
letech	léto	k1gNnPc6	léto
s	s	k7c7	s
nejvyšším	vysoký	k2eAgNnSc7d3	nejvyšší
oceněním	ocenění	k1gNnSc7	ocenění
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
ve	v	k7c6	v
studiích	studio	k1gNnPc6	studio
na	na	k7c6	na
Pařížské	pařížský	k2eAgFnSc6d1	Pařížská
konzervatoři	konzervatoř	k1gFnSc6	konzervatoř
u	u	k7c2	u
Julese-Leopolda	Julese-Leopold	k1gMnSc2	Julese-Leopold
Loeba	Loeb	k1gMnSc2	Loeb
a	a	k8xC	a
komorní	komorní	k2eAgFnSc4d1	komorní
hudbu	hudba	k1gFnSc4	hudba
u	u	k7c2	u
Charlese	Charles	k1gMnSc2	Charles
Tournemira	Tournemir	k1gMnSc2	Tournemir
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
<g/>
,	,	kIx,	,
opět	opět	k6eAd1	opět
s	s	k7c7	s
nejvyšším	vysoký	k2eAgNnSc7d3	nejvyšší
oceněním	ocenění	k1gNnSc7	ocenění
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
studií	studio	k1gNnPc2	studio
na	na	k7c6	na
Pařížské	pařížský	k2eAgFnSc6d1	Pařížská
konzervatoři	konzervatoř	k1gFnSc6	konzervatoř
<g/>
,	,	kIx,	,
poměrně	poměrně	k6eAd1	poměrně
neobvykle	obvykle	k6eNd1	obvykle
pro	pro	k7c4	pro
prvotřídní	prvotřídní	k2eAgMnPc4d1	prvotřídní
sólové	sólový	k2eAgMnPc4d1	sólový
hudebníky	hudebník	k1gMnPc4	hudebník
<g/>
,	,	kIx,	,
Navarra	Navarra	k1gFnSc1	Navarra
zcela	zcela	k6eAd1	zcela
přestal	přestat	k5eAaPmAgInS	přestat
brát	brát	k5eAaImF	brát
hodiny	hodina	k1gFnPc4	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
toho	ten	k3xDgInSc2	ten
vypracoval	vypracovat	k5eAaPmAgMnS	vypracovat
vlastní	vlastní	k2eAgInPc4d1	vlastní
studijní	studijní	k2eAgInPc4d1	studijní
kurzy	kurz	k1gInPc4	kurz
a	a	k8xC	a
zdokonaloval	zdokonalovat	k5eAaImAgMnS	zdokonalovat
se	se	k3xPyFc4	se
v	v	k7c6	v
technice	technika	k1gFnSc6	technika
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
zahrnovalo	zahrnovat	k5eAaImAgNnS	zahrnovat
například	například	k6eAd1	například
také	také	k9	také
transkribování	transkribování	k1gNnSc4	transkribování
množství	množství	k1gNnSc2	množství
houslových	houslový	k2eAgFnPc2d1	houslová
technik	technika	k1gFnPc2	technika
pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
violoncellových	violoncellový	k2eAgNnPc2d1	violoncellové
cvičení	cvičení	k1gNnPc2	cvičení
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
etud	etuda	k1gFnPc2	etuda
Carla	Carl	k1gMnSc2	Carl
Flesche	Flesch	k1gMnSc2	Flesch
a	a	k8xC	a
Otakara	Otakar	k1gMnSc2	Otakar
Ševčíka	Ševčík	k1gMnSc2	Ševčík
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
pozdějších	pozdní	k2eAgNnPc6d2	pozdější
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
na	na	k7c6	na
Pařížské	pařížský	k2eAgFnSc6d1	Pařížská
konzervatoři	konzervatoř	k1gFnSc6	konzervatoř
<g/>
,	,	kIx,	,
vedl	vést	k5eAaImAgMnS	vést
Navarra	Navarra	k1gFnSc1	Navarra
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1954	[number]	k4	1954
každoroční	každoroční	k2eAgInPc4d1	každoroční
mistrovské	mistrovský	k2eAgInPc4d1	mistrovský
kursy	kurs	k1gInPc4	kurs
na	na	k7c4	na
Accademia	Accademium	k1gNnPc4	Accademium
Musicale	musical	k1gInSc5	musical
Chigiana	Chigian	k1gMnSc4	Chigian
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
mezi	mezi	k7c7	mezi
jeho	jeho	k3xOp3gMnPc7	jeho
studenty	student	k1gMnPc7	student
patřili	patřit	k5eAaImAgMnP	patřit
např.	např.	kA	např.
mladý	mladý	k2eAgMnSc1d1	mladý
Saša	Saša	k1gMnSc1	Saša
Večtomov	Večtomov	k1gInSc1	Večtomov
<g/>
,	,	kIx,	,
podzimní	podzimní	k2eAgInPc1d1	podzimní
kurzy	kurz	k1gInPc1	kurz
v	v	k7c4	v
Saint-Jean-de-Luz	Saint-Jeane-Luz	k1gInSc4	Saint-Jean-de-Luz
<g/>
,	,	kIx,	,
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
přijal	přijmout	k5eAaPmAgInS	přijmout
také	také	k9	také
profesuru	profesura	k1gFnSc4	profesura
na	na	k7c6	na
vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
hudební	hudební	k2eAgFnSc6d1	hudební
v	v	k7c6	v
Detmoldu	Detmold	k1gInSc6	Detmold
<g/>
.	.	kIx.	.
</s>
<s>
Taktéž	Taktéž	k?	Taktéž
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
a	a	k8xC	a
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
Navarra	Navarra	k1gFnSc1	Navarra
natočil	natočit	k5eAaBmAgInS	natočit
Dvořákův	Dvořákův	k2eAgInSc1d1	Dvořákův
Violoncellový	violoncellový	k2eAgInSc1d1	violoncellový
koncert	koncert	k1gInSc1	koncert
h	h	k?	h
moll	moll	k1gNnSc7	moll
s	s	k7c7	s
londýnskou	londýnský	k2eAgFnSc7d1	londýnská
New	New	k1gFnSc7	New
Symphony	Symphona	k1gFnSc2	Symphona
Orchestra	orchestra	k1gFnSc1	orchestra
<g/>
,	,	kIx,	,
řízenou	řízený	k2eAgFnSc7d1	řízená
Rudolfem	Rudolf	k1gMnSc7	Rudolf
Schwarzem	Schwarz	k1gMnSc7	Schwarz
<g/>
;	;	kIx,	;
společnost	společnost	k1gFnSc1	společnost
Capitol	Capitola	k1gFnPc2	Capitola
Records	Records	k1gInSc4	Records
nahrávku	nahrávka	k1gFnSc4	nahrávka
vydala	vydat	k5eAaPmAgFnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
André	André	k1gMnSc1	André
Navarra	Navarra	k1gFnSc1	Navarra
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
