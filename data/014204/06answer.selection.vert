<s>
Keynesiánství	Keynesiánství	k1gNnSc1
<g/>
,	,	kIx,
keynesovství	keynesovství	k1gNnSc1
či	či	k8xC
Keynesova	Keynesův	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
je	být	k5eAaImIp3nS
ekonomický	ekonomický	k2eAgInSc4d1
směr	směr	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
ospravedlňuje	ospravedlňovat	k5eAaImIp3nS
zásahy	zásah	k1gInPc4
státu	stát	k1gInSc2
do	do	k7c2
veřejné	veřejný	k2eAgFnSc2d1
ekonomické	ekonomický	k2eAgFnSc2d1
sféry	sféra	k1gFnSc2
<g/>
.	.	kIx.
</s>