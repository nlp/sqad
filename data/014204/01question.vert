<s>
Jak	jak	k6eAd1
se	se	k3xPyFc4
jmenuje	jmenovat	k5eAaBmIp3nS,k5eAaImIp3nS
ekonomický	ekonomický	k2eAgInSc1d1
směr	směr	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
ospravedlňuje	ospravedlňovat	k5eAaImIp3nS
zásahy	zásah	k1gInPc4
státu	stát	k1gInSc2
do	do	k7c2
veřejné	veřejný	k2eAgFnSc2d1
ekonomické	ekonomický	k2eAgFnSc2d1
sféry	sféra	k1gFnSc2
<g/>
?	?	kIx.
</s>