<s>
Keynesiánství	Keynesiánství	k1gNnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojován	ozdrojován	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yQgFnPc4,k3yRgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Ekonomické	ekonomický	k2eAgInPc1d1
směry	směr	k1gInPc1
</s>
<s>
Ekonomie	ekonomie	k1gFnSc1
hlavního	hlavní	k2eAgInSc2d1
proudu	proud	k1gInSc2
(	(	kIx(
<g/>
ortodoxní	ortodoxní	k2eAgMnSc1d1
<g/>
)	)	kIx)
<g/>
:	:	kIx,
</s>
<s>
Neoklasická	neoklasický	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
</s>
<s>
Keynesiánství	Keynesiánství	k1gNnSc1
</s>
<s>
Neokeynesiánství	Neokeynesiánství	k1gNnSc1
</s>
<s>
Nová	nový	k2eAgFnSc1d1
keynesiánská	keynesiánský	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
</s>
<s>
Nová	nový	k2eAgFnSc1d1
klasická	klasický	k2eAgFnSc1d1
makroekonomie	makroekonomie	k1gFnSc1
</s>
<s>
Neoklasická	neoklasický	k2eAgFnSc1d1
syntéza	syntéza	k1gFnSc1
</s>
<s>
Heterodoxní	heterodoxní	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
Rakouská	rakouský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
</s>
<s>
Postkeynesiánství	Postkeynesiánství	k1gNnSc1
</s>
<s>
Ekologická	ekologický	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
</s>
<s>
Feministická	feministický	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
</s>
<s>
Georgismus	Georgismus	k1gInSc1
</s>
<s>
Institucionální	institucionální	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
</s>
<s>
Termoekonomie	Termoekonomie	k1gFnSc1
</s>
<s>
Zelená	zelený	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
</s>
<s>
Americká	americký	k2eAgFnSc1d1
institucionální	institucionální	k2eAgFnSc1d1
škola	škola	k1gFnSc1
</s>
<s>
A	a	k9
mnohem	mnohem	k6eAd1
více	hodně	k6eAd2
<g/>
...	...	k?
</s>
<s>
z	z	k7c2
•	•	k?
d	d	k?
•	•	k?
e	e	k0
</s>
<s>
Keynesiánství	Keynesiánství	k1gNnSc1
<g/>
,	,	kIx,
keynesovství	keynesovství	k1gNnSc1
či	či	k8xC
Keynesova	Keynesův	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
je	být	k5eAaImIp3nS
ekonomický	ekonomický	k2eAgInSc4d1
směr	směr	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
ospravedlňuje	ospravedlňovat	k5eAaImIp3nS
zásahy	zásah	k1gInPc4
státu	stát	k1gInSc2
do	do	k7c2
veřejné	veřejný	k2eAgFnSc2d1
ekonomické	ekonomický	k2eAgFnSc2d1
sféry	sféra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Keynesiánství	Keynesiánství	k1gNnSc1
je	být	k5eAaImIp3nS
tvořeno	tvořit	k5eAaImNgNnS
různými	různý	k2eAgFnPc7d1
makroekonomickými	makroekonomický	k2eAgFnPc7d1
teoriemi	teorie	k1gFnPc7
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
především	především	k9
v	v	k7c6
krátkodobém	krátkodobý	k2eAgInSc6d1
horizontu	horizont	k1gInSc6
<g/>
,	,	kIx,
a	a	k8xC
obzvláště	obzvláště	k6eAd1
v	v	k7c6
době	doba	k1gFnSc6
recese	recese	k1gFnSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
ekonomická	ekonomický	k2eAgFnSc1d1
produkce	produkce	k1gFnSc1
výrazně	výrazně	k6eAd1
ovlivněna	ovlivnit	k5eAaPmNgFnS
agregátní	agregátní	k2eAgFnSc7d1
poptávkou	poptávka	k1gFnSc7
(	(	kIx(
<g/>
celkovou	celkový	k2eAgFnSc7d1
poptávkou	poptávka	k1gFnSc7
v	v	k7c6
ekonomice	ekonomika	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
keynesiánství	keynesiánství	k1gNnPc2
se	se	k3xPyFc4
agregátní	agregátní	k2eAgFnSc1d1
poptávka	poptávka	k1gFnSc1
nemusí	muset	k5eNaImIp3nS
přímo	přímo	k6eAd1
rovnat	rovnat	k5eAaImF
kapacitě	kapacita	k1gFnSc3
produktivity	produktivita	k1gFnSc2
ekonomiky	ekonomika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Místo	místo	k7c2
toho	ten	k3xDgNnSc2
je	být	k5eAaImIp3nS
ovlivněna	ovlivnit	k5eAaPmNgFnS
řadou	řada	k1gFnSc7
faktorů	faktor	k1gInPc2
a	a	k8xC
někdy	někdy	k6eAd1
se	se	k3xPyFc4
chová	chovat	k5eAaImIp3nS
nevyzpytatelně	vyzpytatelně	k6eNd1
<g/>
,	,	kIx,
ovlivňuje	ovlivňovat	k5eAaImIp3nS
produkci	produkce	k1gFnSc4
<g/>
,	,	kIx,
zaměstnanost	zaměstnanost	k1gFnSc4
a	a	k8xC
inflaci	inflace	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Keynesiánství	Keynesiánství	k1gNnSc1
se	se	k3xPyFc4
vyvíjelo	vyvíjet	k5eAaImAgNnS
během	běh	k1gInSc7
a	a	k8xC
po	po	k7c6
Velké	velký	k2eAgFnSc6d1
hospodářské	hospodářský	k2eAgFnSc6d1
krizi	krize	k1gFnSc6
z	z	k7c2
idejí	idea	k1gFnPc2
prezentovaných	prezentovaný	k2eAgFnPc2d1
Johnem	John	k1gMnSc7
Maynard	Maynard	k1gMnSc1
Keynesem	Keynes	k1gMnSc7
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
knize	kniha	k1gFnSc6
z	z	k7c2
roku	rok	k1gInSc2
1936	#num#	k4
The	The	k1gFnSc2
General	General	k1gFnPc2
Theory	Theora	k1gFnSc2
of	of	k?
Employment	Employment	k1gMnSc1
<g/>
,	,	kIx,
Interest	Interest	k1gMnSc1
and	and	k?
Money	Monea	k1gFnSc2
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
(	(	kIx(
<g/>
Obecná	obecný	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
zaměstnanosti	zaměstnanost	k1gFnSc2
<g/>
,	,	kIx,
úroku	úrok	k1gInSc2
a	a	k8xC
peněz	peníze	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Keynesův	Keynesův	k2eAgInSc1d1
přístup	přístup	k1gInSc1
byl	být	k5eAaImAgInS
v	v	k7c6
kontrastu	kontrast	k1gInSc6
s	s	k7c7
jeho	jeho	k3xOp3gInSc7
přístupem	přístup	k1gInSc7
k	k	k7c3
agregátní	agregátní	k2eAgFnSc3d1
klasické	klasický	k2eAgFnSc3d1
ekonomice	ekonomika	k1gFnSc3
zaměřené	zaměřený	k2eAgFnSc2d1
na	na	k7c4
nabídku	nabídka	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
předcházela	předcházet	k5eAaImAgFnS
jeho	jeho	k3xOp3gFnSc3
knize	kniha	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Interpretace	interpretace	k1gFnSc1
Keynesovy	Keynesův	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
následovaly	následovat	k5eAaImAgInP
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
rozporuplné	rozporuplný	k2eAgInPc4d1
a	a	k8xC
několik	několik	k4yIc4
škol	škola	k1gFnPc2
ekonomického	ekonomický	k2eAgNnSc2d1
myšlení	myšlení	k1gNnSc2
si	se	k3xPyFc3
přivlastňuje	přivlastňovat	k5eAaImIp3nS
jeho	jeho	k3xOp3gInSc4,k3xPp3gInSc4
odkaz	odkaz	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Keynesiánští	keynesiánský	k2eAgMnPc1d1
ekonomové	ekonom	k1gMnPc1
obecně	obecně	k6eAd1
polemizují	polemizovat	k5eAaImIp3nP
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
jelikož	jelikož	k8xS
je	být	k5eAaImIp3nS
agregátní	agregátní	k2eAgFnSc1d1
poptávka	poptávka	k1gFnSc1
těkavá	těkavý	k2eAgFnSc1d1
a	a	k8xC
nestabilní	stabilní	k2eNgFnSc1d1
<g/>
,	,	kIx,
tržní	tržní	k2eAgNnSc1d1
hospodářství	hospodářství	k1gNnSc1
často	často	k6eAd1
pociťuje	pociťovat	k5eAaImIp3nS
neefektivní	efektivní	k2eNgInPc4d1
makroekonomické	makroekonomický	k2eAgInPc4d1
výstupy	výstup	k1gInPc4
ve	v	k7c6
formě	forma	k1gFnSc6
ekonomické	ekonomický	k2eAgFnSc2d1
recese	recese	k1gFnSc2
(	(	kIx(
<g/>
když	když	k8xS
je	být	k5eAaImIp3nS
poptávka	poptávka	k1gFnSc1
nízká	nízký	k2eAgFnSc1d1
<g/>
)	)	kIx)
a	a	k8xC
inflace	inflace	k1gFnSc2
(	(	kIx(
<g/>
když	když	k8xS
je	být	k5eAaImIp3nS
poptávka	poptávka	k1gFnSc1
vysoká	vysoký	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
zmírněny	zmírnit	k5eAaPmNgInP
reakcemi	reakce	k1gFnPc7
hospodářské	hospodářský	k2eAgFnSc2d1
politiky	politika	k1gFnSc2
<g/>
,	,	kIx,
zejména	zejména	k9
opatřeními	opatření	k1gNnPc7
monetární	monetární	k2eAgFnSc1d1
politiky	politika	k1gFnSc2
centrální	centrální	k2eAgFnSc1d1
bankou	banka	k1gFnSc7
a	a	k8xC
opatřeními	opatření	k1gNnPc7
fiskální	fiskální	k2eAgFnSc2d1
politiky	politika	k1gFnSc2
od	od	k7c2
vlády	vláda	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
mohou	moct	k5eAaImIp3nP
pomoci	pomoct	k5eAaPmF
stabilizovat	stabilizovat	k5eAaBmF
objem	objem	k1gInSc4
výroby	výroba	k1gFnSc2
po	po	k7c4
dobu	doba	k1gFnSc4
hospodářského	hospodářský	k2eAgInSc2d1
cyklu	cyklus	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Keynesiánští	keynesiánský	k2eAgMnPc1d1
ekonomové	ekonom	k1gMnPc1
většinou	většina	k1gFnSc7
zastávají	zastávat	k5eAaImIp3nP
řízené	řízený	k2eAgNnSc4d1
tržní	tržní	k2eAgNnSc4d1
hospodářství	hospodářství	k1gNnSc4
–	–	k?
převážně	převážně	k6eAd1
v	v	k7c6
soukromém	soukromý	k2eAgInSc6d1
sektoru	sektor	k1gInSc6
<g/>
,	,	kIx,
ale	ale	k8xC
s	s	k7c7
aktivní	aktivní	k2eAgFnSc7d1
účastí	účast	k1gFnSc7
vládních	vládní	k2eAgFnPc2d1
intervencí	intervence	k1gFnPc2
během	během	k7c2
recese	recese	k1gFnSc2
a	a	k8xC
deprese	deprese	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Keynesiánství	Keynesiánství	k1gNnSc1
představilo	představit	k5eAaPmAgNnS
standardní	standardní	k2eAgInSc4d1
ekonomický	ekonomický	k2eAgInSc4d1
model	model	k1gInSc4
v	v	k7c6
rozvinutých	rozvinutý	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
během	během	k7c2
pozdějšího	pozdní	k2eAgNnSc2d2
období	období	k1gNnSc2
Velké	velký	k2eAgFnSc2d1
hospodářské	hospodářský	k2eAgFnSc2d1
krize	krize	k1gFnSc2
<g/>
,	,	kIx,
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
a	a	k8xC
poválečné	poválečný	k2eAgFnSc2d1
ekonomické	ekonomický	k2eAgFnSc2d1
expanze	expanze	k1gFnSc2
(	(	kIx(
<g/>
1945	#num#	k4
<g/>
-	-	kIx~
<g/>
1973	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
i	i	k9
přestože	přestože	k8xS
ztratilo	ztratit	k5eAaPmAgNnS
nějaký	nějaký	k3yIgInSc4
vliv	vliv	k1gInSc4
během	během	k7c2
ropné	ropný	k2eAgFnSc2d1
krize	krize	k1gFnSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
mělo	mít	k5eAaImAgNnS
za	za	k7c4
důsledek	důsledek	k1gInSc4
stagflaci	stagflace	k1gFnSc4
70	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Vznik	vznik	k1gInSc1
finanční	finanční	k2eAgFnSc2d1
krize	krize	k1gFnSc2
v	v	k7c6
letech	léto	k1gNnPc6
2007-08	2007-08	k4
zapříčinilo	zapříčinit	k5eAaPmAgNnS
oživení	oživení	k1gNnSc1
keynesiánské	keynesiánský	k2eAgFnSc2d1
myšlenky	myšlenka	k1gFnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
pokračuje	pokračovat	k5eAaImIp3nS
jako	jako	k9
neokeynesiánství	neokeynesiánství	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Předkeynesiánská	Předkeynesiánský	k2eAgFnSc1d1
makroekonomika	makroekonomika	k1gFnSc1
</s>
<s>
Maokroekonomie	Maokroekonomie	k1gFnSc1
je	být	k5eAaImIp3nS
obor	obor	k1gInSc4
teoretické	teoretický	k2eAgFnSc2d1
ekonomie	ekonomie	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
zkoumá	zkoumat	k5eAaImIp3nS
ekonomický	ekonomický	k2eAgInSc1d1
systém	systém	k1gInSc1
jako	jako	k8xS,k8xC
celek	celek	k1gInSc1
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
například	například	k6eAd1
celková	celkový	k2eAgFnSc1d1
cenová	cenový	k2eAgFnSc1d1
hladina	hladina	k1gFnSc1
<g/>
,	,	kIx,
úroková	úrokový	k2eAgFnSc1d1
sazba	sazba	k1gFnSc1
a	a	k8xC
míra	míra	k1gFnSc1
nezaměstnanosti	nezaměstnanost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Klasicistní	klasicistní	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
částečné	částečný	k2eAgFnSc2d1
rovnováhy	rovnováha	k1gFnSc2
rozděluje	rozdělovat	k5eAaImIp3nS
ekonomiku	ekonomika	k1gFnSc4
do	do	k7c2
rozdělených	rozdělený	k2eAgInPc2d1
trhů	trh	k1gInPc2
<g/>
,	,	kIx,
a	a	k8xC
rovnovážné	rovnovážný	k2eAgFnPc4d1
kondice	kondice	k1gFnPc4
každého	každý	k3xTgInSc2
tohoto	tento	k3xDgInSc2
trhu	trh	k1gInSc2
by	by	kYmCp3nP
mohly	moct	k5eAaImAgFnP
být	být	k5eAaImF
vyjádřeny	vyjádřit	k5eAaPmNgFnP
jednoduchou	jednoduchý	k2eAgFnSc7d1
rovnicí	rovnice	k1gFnSc7
vyjadřující	vyjadřující	k2eAgFnSc4d1
jednu	jeden	k4xCgFnSc4
proměnnou	proměnná	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teoretický	teoretický	k2eAgInSc1d1
aparát	aparát	k1gInSc1
křivek	křivka	k1gFnPc2
poptávky	poptávka	k1gFnSc2
a	a	k8xC
nabídky	nabídka	k1gFnSc2
<g/>
,	,	kIx,
vyvinutý	vyvinutý	k2eAgInSc1d1
Fleemingem	Fleeming	k1gInSc7
Jenkinem	Jenkin	k1gInSc7
a	a	k8xC
Alfredem	Alfred	k1gMnSc7
Marshallem	Marshall	k1gMnSc7
<g/>
,	,	kIx,
nabídla	nabídnout	k5eAaPmAgFnS
sjednocení	sjednocení	k1gNnSc3
matematického	matematický	k2eAgInSc2d1
základu	základ	k1gInSc2
tohoto	tento	k3xDgInSc2
přístupu	přístup	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc4,k3yRgInSc4,k3yQgInSc4
Lausannská	lausannský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
zobecnila	zobecnit	k5eAaPmAgFnS
do	do	k7c2
obecné	obecný	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
rovnováhy	rovnováha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
makroekonomii	makroekonomie	k1gFnSc4
jsou	být	k5eAaImIp3nP
relevantními	relevantní	k2eAgFnPc7d1
dílčími	dílčí	k2eAgFnPc7d1
teoriemi	teorie	k1gFnPc7
<g/>
:	:	kIx,
Kvantitativní	kvantitativní	k2eAgFnPc1d1
teorie	teorie	k1gFnPc1
peněz	peníze	k1gInPc2
určující	určující	k2eAgFnSc4d1
cenovou	cenový	k2eAgFnSc4d1
hladinu	hladina	k1gFnSc4
<g/>
,	,	kIx,
Klasická	klasický	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
úrokové	úrokový	k2eAgFnSc2d1
míry	míra	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
pro	pro	k7c4
zaměstnanost	zaměstnanost	k1gFnSc4
podmínku	podmínka	k1gFnSc4
<g/>
,	,	kIx,
na	na	k7c4
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
Keynes	Keynes	k1gInSc1
odkazuje	odkazovat	k5eAaImIp3nS
jako	jako	k9
na	na	k7c4
“	“	k?
<g/>
první	první	k4xOgInSc1
postulát	postulát	k1gInSc1
klasické	klasický	k2eAgFnSc2d1
ekonomie	ekonomie	k1gFnSc2
<g/>
”	”	k?
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
se	se	k3xPyFc4
uvádí	uvádět	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
mzda	mzda	k1gFnSc1
se	se	k3xPyFc4
rovná	rovnat	k5eAaImIp3nS
meznímu	mezní	k2eAgInSc3d1
produktu	produkt	k1gInSc3
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
přímým	přímý	k2eAgNnSc7d1
uplatněním	uplatnění	k1gNnSc7
marginalistických	marginalistický	k2eAgInPc2d1
principů	princip	k1gInPc2
rozvinutých	rozvinutý	k2eAgInPc2d1
během	během	k7c2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
Obecná	obecný	k2eAgFnSc1d1
teorie	teorie	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Keynes	Keynes	k1gMnSc1
se	se	k3xPyFc4
snažil	snažit	k5eAaImAgMnS
nahradit	nahradit	k5eAaPmF
tyto	tento	k3xDgInPc4
tři	tři	k4xCgInPc4
aspekty	aspekt	k1gInPc4
klasické	klasický	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Příčiny	příčina	k1gFnPc1
vzniku	vznik	k1gInSc2
keynesiánství	keynesiánství	k1gNnSc2
</s>
<s>
Nejvýznamnější	významný	k2eAgFnSc7d3
knihou	kniha	k1gFnSc7
keynesiánců	keynesiánec	k1gMnPc2
je	být	k5eAaImIp3nS
kniha	kniha	k1gFnSc1
Obecná	obecný	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
zaměstnanosti	zaměstnanost	k1gFnSc2
<g/>
,	,	kIx,
úroku	úrok	k1gInSc2
a	a	k8xC
peněz	peníze	k1gInPc2
vydaná	vydaný	k2eAgNnPc1d1
v	v	k7c6
roce	rok	k1gInSc6
1936	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kniha	kniha	k1gFnSc1
byla	být	k5eAaImAgFnS
reakcí	reakce	k1gFnSc7
na	na	k7c4
velkou	velký	k2eAgFnSc4d1
hospodářskou	hospodářský	k2eAgFnSc4d1
krizi	krize	k1gFnSc4
z	z	k7c2
přelomu	přelom	k1gInSc2
dvacátých	dvacátý	k4xOgNnPc2
a	a	k8xC
třicátých	třicátý	k4xOgNnPc2
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ekonomie	ekonomie	k1gFnSc1
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
obecně	obecně	k6eAd1
věřila	věřit	k5eAaImAgFnS
<g/>
,	,	kIx,
že	že	k8xS
pokud	pokud	k8xS
není	být	k5eNaImIp3nS
volný	volný	k2eAgInSc1d1
trh	trh	k1gInSc1
deformován	deformovat	k5eAaImNgInS
státními	státní	k2eAgInPc7d1
zásahy	zásah	k1gInPc7
<g/>
,	,	kIx,
při	při	k7c6
jakékoliv	jakýkoliv	k3yIgFnSc6
změně	změna	k1gFnSc6
v	v	k7c6
prostředí	prostředí	k1gNnSc6
se	se	k3xPyFc4
tržní	tržní	k2eAgFnPc1d1
podmínky	podmínka	k1gFnPc1
automaticky	automaticky	k6eAd1
přizpůsobí	přizpůsobit	k5eAaPmIp3nP
dlouhodobě	dlouhodobě	k6eAd1
nejefektivnějšímu	efektivní	k2eAgInSc3d3
trendu	trend	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krize	krize	k1gFnSc1
však	však	k9
zasadila	zasadit	k5eAaPmAgFnS
ránu	rána	k1gFnSc4
neoklasické	neoklasický	k2eAgFnSc3d1
ekonomii	ekonomie	k1gFnSc3
(	(	kIx(
<g/>
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
tehdy	tehdy	k6eAd1
uznávaná	uznávaný	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
ekonomie	ekonomie	k1gFnSc1
hlavního	hlavní	k2eAgInSc2d1
proudu	proud	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Toto	tento	k3xDgNnSc1
selhání	selhání	k1gNnSc1
si	se	k3xPyFc3
ekonomové	ekonom	k1gMnPc1
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
nedokázali	dokázat	k5eNaPmAgMnP
vysvětlit	vysvětlit	k5eAaPmF
(	(	kIx(
<g/>
s	s	k7c7
alternativním	alternativní	k2eAgNnSc7d1
vysvětlením	vysvětlení	k1gNnSc7
přišel	přijít	k5eAaPmAgMnS
o	o	k7c4
40	#num#	k4
let	léto	k1gNnPc2
později	pozdě	k6eAd2
ekonom	ekonom	k1gMnSc1
Milton	Milton	k1gInSc4
Friedman	Friedman	k1gMnSc1
<g/>
,	,	kIx,
člen	člen	k1gMnSc1
chicagské	chicagský	k2eAgFnSc2d1
ekonomické	ekonomický	k2eAgFnSc2d1
školy	škola	k1gFnSc2
a	a	k8xC
duchovní	duchovní	k2eAgMnSc1d1
otec	otec	k1gMnSc1
teorie	teorie	k1gFnSc2
monetarismu	monetarismus	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Keynes	Keynes	k1gInSc4
tedy	tedy	k9
na	na	k7c6
základě	základ	k1gInSc6
makroekonomických	makroekonomický	k2eAgFnPc2d1
analýz	analýza	k1gFnPc2
dospěl	dochvít	k5eAaPmAgInS
k	k	k7c3
závěru	závěr	k1gInSc3
<g/>
,	,	kIx,
že	že	k8xS
existuje	existovat	k5eAaImIp3nS
ještě	ještě	k6eAd1
obecnější	obecní	k2eAgFnSc1d2
teorie	teorie	k1gFnSc1
<g/>
,	,	kIx,
a	a	k8xC
ekonomie	ekonomie	k1gFnSc1
neoklasická	neoklasický	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
pouze	pouze	k6eAd1
jejím	její	k3xOp3gInSc7
speciálním	speciální	k2eAgInSc7d1
případem	případ	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tedy	tedy	k8xC
vyslovil	vyslovit	k5eAaPmAgMnS
názor	názor	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
za	za	k7c2
určitých	určitý	k2eAgFnPc2d1
okolností	okolnost	k1gFnPc2
se	se	k3xPyFc4
nemusí	muset	k5eNaImIp3nS
trh	trh	k1gInSc1
nutně	nutně	k6eAd1
vždy	vždy	k6eAd1
přibližovat	přibližovat	k5eAaImF
nejefektivnějšímu	efektivní	k2eAgInSc3d3
bodu	bod	k1gInSc3
<g/>
,	,	kIx,
ale	ale	k8xC
může	moct	k5eAaImIp3nS
se	se	k3xPyFc4
ustálit	ustálit	k5eAaPmF
v	v	k7c6
situaci	situace	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
je	být	k5eAaImIp3nS
produkt	produkt	k1gInSc1
nízký	nízký	k2eAgInSc1d1
a	a	k8xC
nezaměstnanost	nezaměstnanost	k1gFnSc1
vysoká	vysoký	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takovými	takový	k3xDgFnPc7
podmínkami	podmínka	k1gFnPc7
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
např.	např.	kA
tzv.	tzv.	kA
spekulativní	spekulativní	k2eAgInSc4d1
motiv	motiv	k1gInSc4
poptávky	poptávka	k1gFnSc2
po	po	k7c6
penězích	peníze	k1gInPc6
či	či	k8xC
past	past	k1gFnSc1
na	na	k7c4
likviditu	likvidita	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
v	v	k7c6
určitých	určitý	k2eAgInPc6d1
případech	případ	k1gInPc6
(	(	kIx(
<g/>
např.	např.	kA
hluboká	hluboký	k2eAgFnSc1d1
a	a	k8xC
dlouho	dlouho	k6eAd1
trvající	trvající	k2eAgFnSc1d1
krize	krize	k1gFnSc1
<g/>
)	)	kIx)
může	moct	k5eAaImIp3nS
způsobit	způsobit	k5eAaPmF
totální	totální	k2eAgFnSc4d1
neúčinnost	neúčinnost	k1gFnSc4
monetární	monetární	k2eAgFnSc2d1
(	(	kIx(
<g/>
měnové	měnový	k2eAgFnSc2d1
<g/>
)	)	kIx)
politiky	politika	k1gFnSc2
(	(	kIx(
<g/>
ta	ten	k3xDgFnSc1
je	být	k5eAaImIp3nS
alfou	alfa	k1gFnSc7
a	a	k8xC
omegou	omega	k1gFnSc7
monetaristické	monetaristický	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
<g/>
,	,	kIx,
proto	proto	k8xC
jsou	být	k5eAaImIp3nP
tyto	tento	k3xDgInPc1
faktory	faktor	k1gInPc1
monetaristy	monetarista	k1gMnSc2
hlasitě	hlasitě	k6eAd1
odmítány	odmítán	k2eAgInPc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Právě	právě	k9
nezaměstnanost	nezaměstnanost	k1gFnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
očích	oko	k1gNnPc6
keynesiánských	keynesiánský	k2eAgFnPc2d1
ekonomů	ekonom	k1gMnPc2
největším	veliký	k2eAgNnSc7d3
zlem	zlo	k1gNnSc7
v	v	k7c6
ekonomice	ekonomika	k1gFnSc6
(	(	kIx(
<g/>
monetaristé	monetarista	k1gMnPc1
naopak	naopak	k6eAd1
za	za	k7c4
největší	veliký	k2eAgNnSc4d3
zlo	zlo	k1gNnSc4
považují	považovat	k5eAaImIp3nP
vysokou	vysoký	k2eAgFnSc4d1
inflaci	inflace	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
těchto	tento	k3xDgInPc2
důvodů	důvod	k1gInPc2
vidí	vidět	k5eAaImIp3nS
keynesiánská	keynesiánský	k2eAgFnSc1d1
politika	politika	k1gFnSc1
východisko	východisko	k1gNnSc1
v	v	k7c6
politice	politika	k1gFnSc6
fiskální	fiskální	k2eAgFnSc6d1
(	(	kIx(
<g/>
rozpočtové	rozpočtový	k2eAgFnSc6d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tímto	tento	k3xDgNnSc7
východiskem	východisko	k1gNnSc7
je	být	k5eAaImIp3nS
státní	státní	k2eAgFnSc1d1
rozpočtová	rozpočtový	k2eAgFnSc1d1
stimulace	stimulace	k1gFnSc1
agregátní	agregátní	k2eAgFnSc2d1
poptávky	poptávka	k1gFnSc2
v	v	k7c6
dobách	doba	k1gFnPc6
deprese	deprese	k1gFnSc2
a	a	k8xC
krytí	krytí	k1gNnSc4
jejího	její	k3xOp3gInSc2
schodku	schodek	k1gInSc2
v	v	k7c6
době	doba	k1gFnSc6
konjunktury	konjunktura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jinými	jiný	k2eAgNnPc7d1
slovy	slovo	k1gNnPc7
<g/>
,	,	kIx,
v	v	k7c6
době	doba	k1gFnSc6
ekonomické	ekonomický	k2eAgFnSc2d1
prosperity	prosperita	k1gFnSc2
stát	stát	k1gInSc1
vytváří	vytvářet	k5eAaImIp3nS,k5eAaPmIp3nS
přebytky	přebytek	k1gInPc7
<g/>
,	,	kIx,
kterými	který	k3yQgFnPc7,k3yIgFnPc7,k3yRgFnPc7
pak	pak	k6eAd1
stimuluje	stimulovat	k5eAaImIp3nS
ekonomiku	ekonomika	k1gFnSc4
v	v	k7c6
době	doba	k1gFnSc6
krize	krize	k1gFnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
ekonomika	ekonomika	k1gFnSc1
stagnuje	stagnovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
souladu	soulad	k1gInSc6
s	s	k7c7
touto	tento	k3xDgFnSc7
teorií	teorie	k1gFnSc7
byl	být	k5eAaImAgInS
rozpracováván	rozpracováván	k2eAgInSc1d1
i	i	k8xC
politicko-hospodářský	politicko-hospodářský	k2eAgInSc1d1
program	program	k1gInSc1
New	New	k1gFnPc2
Deal	Deala	k1gFnPc2
prezidenta	prezident	k1gMnSc2
USA	USA	kA
Franklina	Franklin	k2eAgFnSc1d1
D.	D.	kA
Roosevelta	Roosevelt	k1gMnSc4
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gMnSc1
součástí	součást	k1gFnSc7
byl	být	k5eAaImAgInS
i	i	k9
jeden	jeden	k4xCgInSc1
z	z	k7c2
největších	veliký	k2eAgInPc2d3
projektů	projekt	k1gInPc2
z	z	k7c2
veřejných	veřejný	k2eAgFnPc2d1
zakázek	zakázka	k1gFnPc2
v	v	k7c6
historii	historie	k1gFnSc6
lidstva	lidstvo	k1gNnSc2
<g/>
,	,	kIx,
mamutí	mamutí	k2eAgInSc1d1
projekt	projekt	k1gInSc1
TVA	TVA	kA
(	(	kIx(
<g/>
regulace	regulace	k1gFnSc1
břehu	břeh	k1gInSc2
řeky	řeka	k1gFnSc2
Tennessee	Tennesse	k1gFnSc2
s	s	k7c7
výstavbou	výstavba	k1gFnSc7
množství	množství	k1gNnSc2
přehrad	přehrada	k1gFnPc2
a	a	k8xC
vodních	vodní	k2eAgFnPc2d1
nádrží	nádrž	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Obecná	obecný	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
</s>
<s>
Ekonomické	ekonomický	k2eAgFnPc1d1
ideje	idea	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
se	se	k3xPyFc4
staly	stát	k5eAaPmAgFnP
základem	základ	k1gInSc7
Keynesiánské	keynesiánský	k2eAgFnSc2d1
ekonomiky	ekonomika	k1gFnSc2
<g/>
,	,	kIx,
byly	být	k5eAaImAgInP
předem	předem	k6eAd1
ustanoveny	ustanovit	k5eAaPmNgInP
v	v	k7c6
Keynesově	Keynesův	k2eAgFnSc6d1
hlavní	hlavní	k2eAgFnSc6d1
tezi	teze	k1gFnSc6
–	–	k?
Obecná	obecný	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
zaměstnání	zaměstnání	k1gNnSc2
<g/>
,	,	kIx,
úroků	úrok	k1gInPc2
a	a	k8xC
peněz	peníze	k1gInPc2
(	(	kIx(
<g/>
1936	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čítající	čítající	k2eAgFnSc1d1
téměř	téměř	k6eAd1
400	#num#	k4
stránek	stránka	k1gFnPc2
<g/>
,	,	kIx,
napsána	napsán	k2eAgFnSc1d1
během	během	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
co	co	k3yInSc4,k3yQnSc4,k3yRnSc4
byl	být	k5eAaImAgInS
svět	svět	k1gInSc1
ochromen	ochromit	k5eAaPmNgInS
Velkou	velký	k2eAgFnSc7d1
hospodářskou	hospodářský	k2eAgFnSc7d1
krizí	krize	k1gFnSc7
<g/>
,	,	kIx,
během	během	k7c2
které	který	k3yRgFnSc2,k3yIgFnSc2,k3yQgFnSc2
se	se	k3xPyFc4
nezaměstnanost	nezaměstnanost	k1gFnSc1
vyšplhala	vyšplhat	k5eAaPmAgFnS
na	na	k7c4
25	#num#	k4
%	%	kIx~
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
a	a	k8xC
až	až	k9
na	na	k7c4
33	#num#	k4
%	%	kIx~
v	v	k7c6
některých	některý	k3yIgFnPc6
zemích	zem	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
téměř	téměř	k6eAd1
plně	plně	k6eAd1
teoretické	teoretický	k2eAgFnSc2d1
povahy	povaha	k1gFnSc2
<g/>
,	,	kIx,
oživená	oživený	k2eAgFnSc1d1
příležitostnými	příležitostný	k2eAgFnPc7d1
pasážemi	pasáž	k1gFnPc7
satiry	satira	k1gFnSc2
a	a	k8xC
společenskými	společenský	k2eAgInPc7d1
komentáři	komentář	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kniha	kniha	k1gFnSc1
měla	mít	k5eAaImAgFnS
hluboký	hluboký	k2eAgInSc4d1
dopad	dopad	k1gInSc4
na	na	k7c4
ekonomické	ekonomický	k2eAgNnSc4d1
myšlení	myšlení	k1gNnSc4
<g/>
,	,	kIx,
a	a	k8xC
<g/>
,	,	kIx,
od	od	k7c2
doby	doba	k1gFnSc2
co	co	k9
byla	být	k5eAaImAgFnS
vydána	vydán	k2eAgFnSc1d1
<g/>
,	,	kIx,
se	se	k3xPyFc4
debatuje	debatovat	k5eAaImIp3nS
o	o	k7c6
jejím	její	k3xOp3gInSc6
významu	význam	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Keynes	Keynes	k1gInSc1
a	a	k8xC
klasické	klasický	k2eAgFnSc2d1
ekonomické	ekonomický	k2eAgFnSc2d1
školy	škola	k1gFnSc2
</s>
<s>
Keynes	Keynes	k1gInSc1
začíná	začínat	k5eAaImIp3nS
Obecnou	obecný	k2eAgFnSc4d1
teorii	teorie	k1gFnSc4
shrnutím	shrnutí	k1gNnSc7
klasické	klasický	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
zaměstnanosti	zaměstnanost	k1gFnSc2
tak	tak	k6eAd1
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
na	na	k7c4
ni	on	k3xPp3gFnSc4
on	on	k3xPp3gMnSc1
nahlíží	nahlížet	k5eAaImIp3nS
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
shrnuje	shrnovat	k5eAaImIp3nS
formulací	formulace	k1gFnSc7
Sayova	Sayův	k2eAgInSc2d1
zákona	zákon	k1gInSc2
pomocí	pomocí	k7c2
výroku	výrok	k1gInSc2
“	“	k?
<g/>
Nabídka	nabídka	k1gFnSc1
si	se	k3xPyFc3
vytváří	vytvářit	k5eAaPmIp3nS,k5eAaImIp3nS
svou	svůj	k3xOyFgFnSc4
vlastní	vlastní	k2eAgFnSc4d1
poptávku	poptávka	k1gFnSc4
<g/>
”	”	k?
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
klasické	klasický	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
je	být	k5eAaImIp3nS
mzdová	mzdový	k2eAgFnSc1d1
sazba	sazba	k1gFnSc1
určována	určovat	k5eAaImNgFnS
marginální	marginální	k2eAgFnSc7d1
produktivitou	produktivita	k1gFnSc7
práce	práce	k1gFnSc2
a	a	k8xC
rovností	rovnost	k1gFnPc2
mezi	mezi	k7c7
počtem	počet	k1gInSc7
zaměstnaných	zaměstnaný	k2eAgMnPc2d1
lidí	člověk	k1gMnPc2
a	a	k8xC
těch	ten	k3xDgMnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
jsou	být	k5eAaImIp3nP
ochotní	ochotný	k2eAgMnPc1d1
pracovat	pracovat	k5eAaImF
za	za	k7c4
tuto	tento	k3xDgFnSc4
sazbu	sazba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nezaměstnanost	nezaměstnanost	k1gFnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
způsobena	způsoben	k2eAgFnSc1d1
donucením	donucení	k1gNnSc7
nebo	nebo	k8xC
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
dobrovolná	dobrovolný	k2eAgFnSc1d1
ve	v	k7c6
smyslu	smysl	k1gInSc6
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
že	že	k8xS
vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
odmítání	odmítání	k1gNnSc2
zaměstnanosti	zaměstnanost	k1gFnSc2
v	v	k7c6
důsledku	důsledek	k1gInSc6
právních	právní	k2eAgInPc2d1
předpisů	předpis	k1gInPc2
nebo	nebo	k8xC
společenských	společenský	k2eAgFnPc2d1
praktik	praktika	k1gFnPc2
nebo	nebo	k8xC
z	z	k7c2
pouhé	pouhý	k2eAgFnSc2d1
lidské	lidský	k2eAgFnSc2d1
zatvrzelosti	zatvrzelost	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
“	“	k?
<g/>
klasické	klasický	k2eAgInPc4d1
postuláty	postulát	k1gInPc4
nepřipouští	připouštět	k5eNaImIp3nS
existenci	existence	k1gFnSc4
této	tento	k3xDgFnSc2
třetí	třetí	k4xOgFnSc2
kategorie	kategorie	k1gFnSc2
<g/>
”	”	k?
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
Keynes	Keynes	k1gInSc1
popisuje	popisovat	k5eAaImIp3nS
jako	jako	k9
“	“	k?
<g/>
nedobrovolná	dobrovolný	k2eNgFnSc1d1
nezaměstnanost	nezaměstnanost	k1gFnSc1
<g/>
”	”	k?
<g/>
.	.	kIx.
</s>
<s>
Keynes	Keynes	k1gMnSc1
vznáší	vznášet	k5eAaImIp3nS
dvě	dva	k4xCgFnPc4
námitky	námitka	k1gFnPc4
proti	proti	k7c3
předpokladu	předpoklad	k1gInSc3
klasické	klasický	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
<g/>
,	,	kIx,
že	že	k8xS
“	“	k?
<g/>
mzdové	mzdový	k2eAgNnSc1d1
vyjednávání	vyjednávání	k1gNnSc1
určuje	určovat	k5eAaImIp3nS
reálnou	reálný	k2eAgFnSc4d1
mzdu	mzda	k1gFnSc4
<g/>
”	”	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
námitka	námitka	k1gFnSc1
leží	ležet	k5eAaImIp3nS
v	v	k7c6
podstatě	podstata	k1gFnSc6
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
že	že	k8xS
“	“	k?
<g/>
práce	práce	k1gFnSc1
stanovuje	stanovovat	k5eAaImIp3nS
(	(	kIx(
<g/>
v	v	k7c6
rámci	rámec	k1gInSc6
omezení	omezení	k1gNnSc2
<g/>
)	)	kIx)
spíše	spíše	k9
pro	pro	k7c4
peněžitou	peněžitý	k2eAgFnSc4d1
mzdu	mzda	k1gFnSc4
<g/>
,	,	kIx,
než	než	k8xS
reálnou	reálný	k2eAgFnSc4d1
mzdu	mzda	k1gFnSc4
<g/>
”	”	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
druhá	druhý	k4xOgFnSc1
říká	říkat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
klasická	klasický	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
předpokládá	předpokládat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
“	“	k?
<g/>
reálná	reálný	k2eAgFnSc1d1
cena	cena	k1gFnSc1
práce	práce	k1gFnSc2
závisí	záviset	k5eAaImIp3nS
na	na	k7c6
mzdovém	mzdový	k2eAgNnSc6d1
vyjednávání	vyjednávání	k1gNnSc6
<g/>
,	,	kIx,
které	který	k3yIgNnSc4,k3yRgNnSc4,k3yQgNnSc4
práce	práce	k1gFnSc1
vytváří	vytvářet	k5eAaImIp3nS,k5eAaPmIp3nS
s	s	k7c7
podnikateli	podnikatel	k1gMnPc7
<g/>
”	”	k?
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
“	“	k?
<g/>
pokud	pokud	k8xS
se	se	k3xPyFc4
peněžitá	peněžitý	k2eAgFnSc1d1
mzda	mzda	k1gFnSc1
změní	změnit	k5eAaPmIp3nS
<g/>
,	,	kIx,
jeden	jeden	k4xCgMnSc1
by	by	k9
očekával	očekávat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
klasická	klasický	k2eAgFnSc1d1
škola	škola	k1gFnSc1
bude	být	k5eAaImBp3nS
tvrdit	tvrdit	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
ceny	cena	k1gFnPc1
budou	být	k5eAaImBp3nP
měnit	měnit	k5eAaImF
v	v	k7c6
skoro	skoro	k6eAd1
stejném	stejný	k2eAgInSc6d1
poměru	poměr	k1gInSc6
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
zanechá	zanechat	k5eAaPmIp3nS
reálnou	reálný	k2eAgFnSc4d1
mzdu	mzda	k1gFnSc4
a	a	k8xC
míru	míra	k1gFnSc4
nezaměstnanosti	nezaměstnanost	k1gFnSc2
prakticky	prakticky	k6eAd1
na	na	k7c6
stejné	stejný	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
jako	jako	k8xC,k8xS
předtím	předtím	k6eAd1
<g/>
”	”	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Keynes	Keynes	k1gMnSc1
považuje	považovat	k5eAaImIp3nS
svou	svůj	k3xOyFgFnSc4
druhou	druhý	k4xOgFnSc4
námitku	námitka	k1gFnSc4
za	za	k7c4
podstatnější	podstatný	k2eAgFnSc4d2
<g/>
,	,	kIx,
ale	ale	k8xC
jeho	jeho	k3xOp3gNnSc4
očekávání	očekávání	k1gNnSc4
ohledně	ohledně	k7c2
klasické	klasický	k2eAgFnSc2d1
školy	škola	k1gFnSc2
jsou	být	k5eAaImIp3nP
v	v	k7c6
rozporu	rozpor	k1gInSc6
s	s	k7c7
Kvantitativní	kvantitativní	k2eAgFnSc7d1
teorií	teorie	k1gFnSc7
peněz	peníze	k1gInPc2
<g/>
,	,	kIx,
a	a	k8xC
většina	většina	k1gFnSc1
komentátorů	komentátor	k1gMnPc2
se	se	k3xPyFc4
soustředila	soustředit	k5eAaPmAgFnS
na	na	k7c4
námitku	námitka	k1gFnSc4
první	první	k4xOgFnSc4
<g/>
.	.	kIx.
</s>
<s>
Nezaměstnanost	nezaměstnanost	k1gFnSc1
podle	podle	k7c2
Keynesiánství	Keynesiánství	k1gNnSc2
</s>
<s>
Úspory	úspora	k1gFnPc1
a	a	k8xC
investice	investice	k1gFnPc1
</s>
<s>
Úspory	úspora	k1gFnPc1
jsou	být	k5eAaImIp3nP
část	část	k1gFnSc4
příjmu	příjem	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
není	být	k5eNaImIp3nS
určena	určit	k5eAaPmNgFnS
ke	k	k7c3
spotřebě	spotřeba	k1gFnSc3
<g/>
,	,	kIx,
a	a	k8xC
spotřeba	spotřeba	k1gFnSc1
je	být	k5eAaImIp3nS
část	část	k1gFnSc4
výdajů	výdaj	k1gInPc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
není	být	k5eNaImIp3nS
určena	určit	k5eAaPmNgFnS
k	k	k7c3
investicím	investice	k1gFnPc3
např.	např.	kA
do	do	k7c2
zboží	zboží	k1gNnSc2
dlouhodobé	dlouhodobý	k2eAgFnSc2d1
spotřeby	spotřeba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spoření	spoření	k1gNnPc1
tedy	tedy	k8xC
zahrnuje	zahrnovat	k5eAaImIp3nS
hromadění	hromadění	k1gNnSc1
(	(	kIx(
<g/>
akumulace	akumulace	k1gFnSc1
příjmu	příjem	k1gInSc2
jako	jako	k8xS,k8xC
hotovosti	hotovost	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
nákup	nákup	k1gInSc4
zboží	zboží	k1gNnSc2
dlouhodobé	dlouhodobý	k2eAgFnSc2d1
spotřeby	spotřeba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existence	existence	k1gFnSc1
čistého	čistý	k2eAgNnSc2d1
hromadění	hromadění	k1gNnSc2
nebo	nebo	k8xC
poptávky	poptávka	k1gFnSc2
po	po	k7c6
hromadění	hromadění	k1gNnSc6
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
uznána	uznat	k5eAaPmNgFnS
zjednodušeným	zjednodušený	k2eAgInSc7d1
předpokladem	předpoklad	k1gInSc7
likvidity	likvidita	k1gFnSc2
Obecné	obecný	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Jakmile	jakmile	k8xS
Keynes	Keynes	k1gMnSc1
odmítl	odmítnout	k5eAaPmAgMnS
Klasickou	klasický	k2eAgFnSc4d1
teorii	teorie	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
nezaměstnanost	nezaměstnanost	k1gFnSc1
je	být	k5eAaImIp3nS
způsobena	způsobit	k5eAaPmNgFnS
nadměrnými	nadměrný	k2eAgInPc7d1
platy	plat	k1gInPc7
<g/>
,	,	kIx,
navrhl	navrhnout	k5eAaPmAgMnS
její	její	k3xOp3gFnSc4
alternativu	alternativa	k1gFnSc4
založenou	založený	k2eAgFnSc4d1
na	na	k7c6
vztahu	vztah	k1gInSc6
mezi	mezi	k7c7
úsporami	úspora	k1gFnPc7
a	a	k8xC
investicemi	investice	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
jeho	jeho	k3xOp3gInSc2,k3xPp3gInSc2
názoru	názor	k1gInSc2
vzniká	vznikat	k5eAaImIp3nS
nezaměstnanost	nezaměstnanost	k1gFnSc1
vždy	vždy	k6eAd1
<g/>
,	,	kIx,
když	když	k8xS
podněty	podnět	k1gInPc4
podnikatelů	podnikatel	k1gMnPc2
k	k	k7c3
investování	investování	k1gNnSc3
nedokáží	dokázat	k5eNaPmIp3nP
udržet	udržet	k5eAaPmF
krok	krok	k1gInSc4
se	s	k7c7
sklonem	sklon	k1gInSc7
společnosti	společnost	k1gFnSc2
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
si	se	k3xPyFc3
spořit	spořit	k5eAaImF
(	(	kIx(
<g/>
sklon	sklon	k1gInSc1
je	být	k5eAaImIp3nS
jedním	jeden	k4xCgNnSc7
z	z	k7c2
Keynesových	Keynesový	k2eAgNnPc2d1
synonym	synonymum	k1gNnPc2
pro	pro	k7c4
“	“	k?
<g/>
poptávku	poptávka	k1gFnSc4
<g/>
”	”	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úrovně	úroveň	k1gFnSc2
úspor	úspora	k1gFnPc2
a	a	k8xC
investic	investice	k1gFnPc2
musí	muset	k5eAaImIp3nP
být	být	k5eAaImF
stejné	stejný	k2eAgInPc1d1
a	a	k8xC
příjmy	příjem	k1gInPc1
jsou	být	k5eAaImIp3nP
proto	proto	k8xC
drženy	držen	k2eAgFnPc1d1
až	až	k9
na	na	k7c4
úroveň	úroveň	k1gFnSc4
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yRgFnSc6,k3yIgFnSc6,k3yQgFnSc6
touha	touha	k1gFnSc1
spořit	spořit	k5eAaImF
není	být	k5eNaImIp3nS
větší	veliký	k2eAgFnSc1d2
než	než	k8xS
popud	popud	k1gInSc4
k	k	k7c3
investování	investování	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s>
Motivace	motivace	k1gFnSc1
k	k	k7c3
investování	investování	k1gNnSc3
vzniká	vznikat	k5eAaImIp3nS
souhrou	souhra	k1gFnSc7
mezi	mezi	k7c7
fyzickou	fyzický	k2eAgFnSc7d1
situací	situace	k1gFnSc7
výroby	výroba	k1gFnSc2
a	a	k8xC
psychologickými	psychologický	k2eAgNnPc7d1
očekáváními	očekávání	k1gNnPc7
budoucí	budoucí	k2eAgFnSc1d1
ziskovosti	ziskovost	k1gFnSc3
<g/>
,	,	kIx,
ale	ale	k8xC
jakmile	jakmile	k8xS
jsou	být	k5eAaImIp3nP
tyto	tento	k3xDgFnPc1
věci	věc	k1gFnPc1
poskytnuty	poskytnut	k2eAgFnPc1d1
<g/>
,	,	kIx,
pobídka	pobídka	k1gFnSc1
je	být	k5eAaImIp3nS
nezávislá	závislý	k2eNgFnSc1d1
na	na	k7c6
příjmech	příjem	k1gInPc6
a	a	k8xC
závisí	záviset	k5eAaImIp3nS
pouze	pouze	k6eAd1
na	na	k7c6
úrokové	úrokový	k2eAgFnSc6d1
míře	míra	k1gFnSc6
r.	r.	kA
Keynes	Keynes	k1gInSc1
označuje	označovat	k5eAaImIp3nS
jejich	jejich	k3xOp3gFnSc4
hodnotu	hodnota	k1gFnSc4
jako	jako	k8xC,k8xS
funkci	funkce	k1gFnSc4
r	r	kA
jako	jako	k8xC,k8xS
“	“	k?
<g/>
plán	plán	k1gInSc4
mezní	mezní	k2eAgFnSc2d1
efektivnosti	efektivnost	k1gFnSc2
kapitálu	kapitál	k1gInSc2
<g/>
”	”	k?
<g/>
.	.	kIx.
</s>
<s>
Sklon	sklon	k1gInSc1
ke	k	k7c3
spoření	spoření	k1gNnSc3
se	se	k3xPyFc4
chová	chovat	k5eAaImIp3nS
zcela	zcela	k6eAd1
jinak	jinak	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úspory	úspora	k1gFnPc1
jsou	být	k5eAaImIp3nP
jednoduše	jednoduše	k6eAd1
ta	ten	k3xDgFnSc1
část	část	k1gFnSc1
příjmu	příjem	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
není	být	k5eNaImIp3nS
věnována	věnovat	k5eAaPmNgFnS,k5eAaImNgFnS
spotřebě	spotřeba	k1gFnSc3
<g/>
,	,	kIx,
a	a	k8xC
<g/>
:	:	kIx,
</s>
<s>
…	…	k?
<g/>
zdá	zdát	k5eAaPmIp3nS,k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
převažující	převažující	k2eAgInSc1d1
psychologický	psychologický	k2eAgInSc1d1
zákon	zákon	k1gInSc1
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
když	když	k8xS
se	se	k3xPyFc4
zvýší	zvýšit	k5eAaPmIp3nS
souhrnný	souhrnný	k2eAgInSc1d1
příjem	příjem	k1gInSc1
<g/>
,	,	kIx,
spotřební	spotřební	k2eAgInPc1d1
výdaje	výdaj	k1gInPc1
se	se	k3xPyFc4
také	také	k9
zvýší	zvýšit	k5eAaPmIp3nS
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c6
menší	malý	k2eAgFnSc6d2
míře	míra	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Keynes	Keynes	k1gMnSc1
dodává	dodávat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
“	“	k?
<g/>
tento	tento	k3xDgInSc4
psychologický	psychologický	k2eAgInSc4d1
zákon	zákon	k1gInSc4
byl	být	k5eAaImAgInS
nesmírně	smírně	k6eNd1
důležitý	důležitý	k2eAgInSc1d1
ve	v	k7c6
vývoji	vývoj	k1gInSc6
mé	můj	k3xOp1gFnSc2
vlastní	vlastní	k2eAgFnSc2d1
myšlenky	myšlenka	k1gFnSc2
<g/>
”	”	k?
<g/>
.	.	kIx.
</s>
<s>
Preference	preference	k1gFnSc1
likvidity	likvidita	k1gFnSc2
</s>
<s>
Odhad	odhad	k1gInSc1
příjmu	příjem	k1gInSc2
podle	podle	k7c2
Obecné	obecný	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Keynes	Keynes	k1gMnSc1
pohlížel	pohlížet	k5eAaImAgMnS
na	na	k7c4
finanční	finanční	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
jako	jako	k8xS,k8xC
na	na	k7c4
jeden	jeden	k4xCgInSc4
z	z	k7c2
hlavních	hlavní	k2eAgInPc2d1
determinantů	determinant	k1gInPc2
stavu	stav	k1gInSc2
reálné	reálný	k2eAgFnSc2d1
ekonomiky	ekonomika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Význam	význam	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
mu	on	k3xPp3gMnSc3
přikládal	přikládat	k5eAaImAgInS
je	být	k5eAaImIp3nS
jedna	jeden	k4xCgFnSc1
z	z	k7c2
inovativních	inovativní	k2eAgFnPc2d1
vlastností	vlastnost	k1gFnPc2
jeho	jeho	k3xOp3gFnSc2
práce	práce	k1gFnSc2
a	a	k8xC
ovlivnila	ovlivnit	k5eAaPmAgFnS
(	(	kIx(
<g/>
politicky	politicky	k6eAd1
nepřátelský	přátelský	k2eNgInSc1d1
<g/>
)	)	kIx)
monetarismus	monetarismus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Finanční	finanční	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
přichází	přicházet	k5eAaImIp3nS
na	na	k7c4
řadu	řada	k1gFnSc4
pomocí	pomocí	k7c2
funkce	funkce	k1gFnSc2
“	“	k?
<g/>
preference	preference	k1gFnSc1
likvidity	likvidita	k1gFnSc2
<g/>
”	”	k?
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
specifikuje	specifikovat	k5eAaBmIp3nS
obnos	obnos	k1gInSc4
peněz	peníze	k1gInPc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
u	u	k7c2
sebe	sebe	k3xPyFc4
chtějí	chtít	k5eAaImIp3nP
lidé	člověk	k1gMnPc1
mít	mít	k5eAaImF
v	v	k7c6
závislosti	závislost	k1gFnSc6
na	na	k7c6
stavu	stav	k1gInSc6
ekonomiky	ekonomika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Keynes	Keynesa	k1gFnPc2
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
první	první	k4xOgFnSc3
(	(	kIx(
<g/>
a	a	k8xC
nejjednodušší	jednoduchý	k2eAgFnSc6d3
<g/>
)	)	kIx)
úvaze	úvaha	k1gFnSc6
(	(	kIx(
<g/>
Kapitola	kapitola	k1gFnSc1
13	#num#	k4
<g/>
)	)	kIx)
uvádí	uvádět	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
preference	preference	k1gFnSc1
likvidity	likvidita	k1gFnSc2
je	být	k5eAaImIp3nS
založena	založit	k5eAaPmNgFnS
výhradně	výhradně	k6eAd1
na	na	k7c6
úrokové	úrokový	k2eAgFnSc6d1
sazbě	sazba	k1gFnSc6
r	r	kA
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
považována	považován	k2eAgFnSc1d1
za	za	k7c4
nevyplacené	vyplacený	k2eNgInPc4d1
příjmy	příjem	k1gInPc4
držením	držení	k1gNnSc7
bohatství	bohatství	k1gNnSc2
v	v	k7c6
likvidní	likvidní	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
–	–	k?
díky	díky	k7c3
tomu	ten	k3xDgNnSc3
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
preference	preference	k1gFnSc1
likvidity	likvidita	k1gFnSc2
zapsána	zapsán	k2eAgFnSc1d1
L	L	kA
<g/>
(	(	kIx(
<g/>
r	r	kA
<g/>
)	)	kIx)
a	a	k8xC
v	v	k7c6
rovnici	rovnice	k1gFnSc6
se	se	k3xPyFc4
musí	muset	k5eAaImIp3nS
rovnat	rovnat	k5eAaImF
externě	externě	k6eAd1
zajištěným	zajištěný	k2eAgInSc7d1
zdrojem	zdroj	k1gInSc7
peněz	peníze	k1gInPc2
M	M	kA
<g/>
̂	̂	k?
<g/>
.	.	kIx.
</s>
<s>
Keynesův	Keynesův	k2eAgInSc1d1
ekonomický	ekonomický	k2eAgInSc1d1
model	model	k1gInSc1
</s>
<s>
Peněžní	peněžní	k2eAgInSc1d1
přítok	přítok	k1gInSc1
<g/>
,	,	kIx,
úspory	úspora	k1gFnPc1
a	a	k8xC
investice	investice	k1gFnPc1
se	se	k3xPyFc4
kombinují	kombinovat	k5eAaImIp3nP
za	za	k7c7
účelem	účel	k1gInSc7
určení	určení	k1gNnSc2
úrovně	úroveň	k1gFnSc2
příjmu	příjem	k1gInSc2
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
je	být	k5eAaImIp3nS
znázorněno	znázorněn	k2eAgNnSc1d1
na	na	k7c6
diagramu	diagram	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
horní	horní	k2eAgInSc1d1
graf	graf	k1gInSc1
zobrazuje	zobrazovat	k5eAaImIp3nS
zásobu	zásoba	k1gFnSc4
peněz	peníze	k1gInPc2
(	(	kIx(
<g/>
na	na	k7c6
svislé	svislý	k2eAgFnSc6d1
ose	osa	k1gFnSc6
<g/>
)	)	kIx)
proti	proti	k7c3
úrokové	úrokový	k2eAgFnSc3d1
sazbě	sazba	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
M	M	kA
<g/>
̂	̂	k?
určuje	určovat	k5eAaImIp3nS
rozhodující	rozhodující	k2eAgFnSc4d1
úrokovou	úrokový	k2eAgFnSc4d1
sazbu	sazba	k1gFnSc4
r	r	kA
<g/>
̂	̂	k?
prostřednictvím	prostřednictvím	k7c2
funkce	funkce	k1gFnSc2
preference	preference	k1gFnSc2
likvidity	likvidita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úroková	úrokový	k2eAgFnSc1d1
míra	míra	k1gFnSc1
určuje	určovat	k5eAaImIp3nS
výši	výše	k1gFnSc4
investice	investice	k1gFnSc2
Î	Î	kA
v	v	k7c6
plánu	plán	k1gInSc6
marginální	marginální	k2eAgFnSc2d1
efektivnosti	efektivnost	k1gFnSc2
kapitálu	kapitál	k1gInSc2
<g/>
,	,	kIx,
zobrazené	zobrazený	k2eAgNnSc4d1
jako	jako	k8xS,k8xC
modrá	modrý	k2eAgFnSc1d1
křivka	křivka	k1gFnSc1
v	v	k7c6
dolním	dolní	k2eAgInSc6d1
grafu	graf	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Červené	Červené	k2eAgFnPc4d1
křivky	křivka	k1gFnPc4
ve	v	k7c6
stejném	stejný	k2eAgInSc6d1
diagramu	diagram	k1gInSc6
ukazují	ukazovat	k5eAaImIp3nP
<g/>
,	,	kIx,
jaké	jaký	k3yQgInPc4,k3yIgInPc4,k3yRgInPc4
sklony	sklon	k1gInPc4
k	k	k7c3
uložení	uložení	k1gNnSc3
budou	být	k5eAaImBp3nP
pro	pro	k7c4
různé	různý	k2eAgInPc4d1
příjmy	příjem	k1gInPc4
Y	Y	kA
;	;	kIx,
a	a	k8xC
příjem	příjem	k1gInSc1
Ŷ	Ŷ	k?
odpovídající	odpovídající	k2eAgInSc1d1
rovnovážnému	rovnovážný	k2eAgInSc3d1
stavu	stav	k1gInSc3
ekonomiky	ekonomika	k1gFnSc2
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
ten	ten	k3xDgMnSc1
<g/>
,	,	kIx,
pro	pro	k7c4
který	který	k3yQgInSc4,k3yRgInSc4,k3yIgInSc4
se	se	k3xPyFc4
implicitní	implicitní	k2eAgFnSc1d1
míra	míra	k1gFnSc1
úspor	úspora	k1gFnPc2
při	při	k7c6
stanovené	stanovený	k2eAgFnSc6d1
úrokové	úrokový	k2eAgFnSc6d1
sazbě	sazba	k1gFnSc6
rovná	rovnat	k5eAaImIp3nS
Î.	Î.	k1gFnSc1
</s>
<s>
Ve	v	k7c6
více	hodně	k6eAd2
komplikovanější	komplikovaný	k2eAgFnSc6d2
Keynesiánské	keynesiánský	k2eAgFnSc6d1
teorii	teorie	k1gFnSc6
preference	preference	k1gFnSc1
likvidity	likvidita	k1gFnSc2
(	(	kIx(
<g/>
prezentované	prezentovaný	k2eAgFnSc2d1
v	v	k7c6
kapitole	kapitola	k1gFnSc6
15	#num#	k4
Obecné	obecný	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
<g/>
)	)	kIx)
poptávka	poptávka	k1gFnSc1
po	po	k7c6
penězích	peníze	k1gInPc6
závisí	záviset	k5eAaImIp3nS
na	na	k7c6
příjmech	příjem	k1gInPc6
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
na	na	k7c6
úrokové	úrokový	k2eAgFnSc6d1
sazbě	sazba	k1gFnSc6
a	a	k8xC
analýza	analýza	k1gFnSc1
se	se	k3xPyFc4
stává	stávat	k5eAaImIp3nS
komplikovanější	komplikovaný	k2eAgFnSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Keynes	Keynes	k1gInSc1
nikdy	nikdy	k6eAd1
plně	plně	k6eAd1
nezahrnul	zahrnout	k5eNaPmAgMnS
svou	svůj	k3xOyFgFnSc4
druhou	druhý	k4xOgFnSc4
doktrínu	doktrína	k1gFnSc4
o	o	k7c6
preferenci	preference	k1gFnSc6
likvidity	likvidita	k1gFnSc2
se	s	k7c7
zbytkem	zbytek	k1gInSc7
své	svůj	k3xOyFgFnSc2
teorie	teorie	k1gFnSc2
<g/>
,	,	kIx,
nechal	nechat	k5eAaPmAgInS
tento	tento	k3xDgInSc1
úkol	úkol	k1gInSc1
dokončit	dokončit	k5eAaPmF
Johna	John	k1gMnSc4
Hickse	Hicks	k1gMnSc4
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
Model	model	k1gInSc4
IS-LM	IS-LM	k1gMnSc2
níže	níže	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Mzdová	mzdový	k2eAgFnSc1d1
rigidita	rigidita	k1gFnSc1
</s>
<s>
Přestože	přestože	k8xS
Keynes	Keynes	k1gInSc1
odmítá	odmítat	k5eAaImIp3nS
klasické	klasický	k2eAgNnSc4d1
vysvětlení	vysvětlení	k1gNnSc4
nezaměstnanosti	nezaměstnanost	k1gFnSc2
založené	založený	k2eAgFnSc2d1
na	na	k7c6
rigiditě	rigidita	k1gFnSc6
mezd	mzda	k1gFnPc2
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
jasné	jasný	k2eAgNnSc1d1
<g/>
,	,	kIx,
jaký	jaký	k3yQgInSc4,k3yIgInSc4,k3yRgInSc4
vliv	vliv	k1gInSc4
má	mít	k5eAaImIp3nS
mzdová	mzdový	k2eAgFnSc1d1
míra	míra	k1gFnSc1
na	na	k7c4
nezaměstnanost	nezaměstnanost	k1gFnSc4
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
vlastním	vlastní	k2eAgInSc6d1
systému	systém	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Považuje	považovat	k5eAaImIp3nS
mzdy	mzda	k1gFnSc2
všech	všecek	k3xTgMnPc2
pracovníků	pracovník	k1gMnPc2
úměrné	úměrná	k1gFnSc2
k	k	k7c3
jednotné	jednotný	k2eAgFnSc3d1
sazbě	sazba	k1gFnSc3
stanovené	stanovený	k2eAgFnSc2d1
kolektivním	kolektivní	k2eAgNnSc7d1
vyjednáváním	vyjednávání	k1gNnSc7
a	a	k8xC
volí	volit	k5eAaImIp3nP
své	svůj	k3xOyFgFnPc4
jednotky	jednotka	k1gFnPc4
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
tato	tento	k3xDgFnSc1
míra	míra	k1gFnSc1
nikdy	nikdy	k6eAd1
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
diskusi	diskuse	k1gFnSc6
neobjevila	objevit	k5eNaPmAgFnS
samostatně	samostatně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
prezentováno	prezentovat	k5eAaBmNgNnS
implicitně	implicitně	k6eAd1
v	v	k7c6
těch	ten	k3xDgNnPc6
množstvích	množství	k1gNnPc6
<g/>
,	,	kIx,
která	který	k3yQgNnPc1,k3yRgNnPc1,k3yIgNnPc1
jsou	být	k5eAaImIp3nP
vyjádřena	vyjádřit	k5eAaPmNgFnS
v	v	k7c6
mzdových	mzdový	k2eAgFnPc6d1
jednotkách	jednotka	k1gFnPc6
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
nejsou	být	k5eNaImIp3nP
vyjádřeny	vyjádřit	k5eAaPmNgInP
v	v	k7c6
penězích	peníze	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
proto	proto	k8xC
obtížné	obtížný	k2eAgNnSc1d1
zjistit	zjistit	k5eAaPmF
<g/>
,	,	kIx,
zda	zda	k8xS
a	a	k8xC
jakým	jaký	k3yIgInSc7,k3yQgInSc7,k3yRgInSc7
způsobem	způsob	k1gInSc7
by	by	kYmCp3nP
se	se	k3xPyFc4
jeho	jeho	k3xOp3gInPc1
výsledky	výsledek	k1gInPc1
lišily	lišit	k5eAaImAgInP
pro	pro	k7c4
jinou	jiný	k2eAgFnSc4d1
mzdovou	mzdový	k2eAgFnSc4d1
míru	míra	k1gFnSc4
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
ani	ani	k8xC
zcela	zcela	k6eAd1
jasné	jasný	k2eAgNnSc1d1
<g/>
,	,	kIx,
co	co	k3yQnSc4,k3yInSc4,k3yRnSc4
si	se	k3xPyFc3
o	o	k7c6
této	tento	k3xDgFnSc6
záležitosti	záležitost	k1gFnSc6
myslel	myslet	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s>
Opatření	opatření	k1gNnPc1
proti	proti	k7c3
nezaměstnanosti	nezaměstnanost	k1gFnSc2
</s>
<s>
Monetární	monetární	k2eAgNnPc1d1
opatření	opatření	k1gNnPc1
</s>
<s>
Zvýšení	zvýšení	k1gNnSc1
peněžních	peněžní	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
podle	podle	k7c2
Keynesovy	Keynesův	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
povede	vést	k5eAaImIp3nS,k5eAaPmIp3nS
ke	k	k7c3
snížení	snížení	k1gNnSc3
úrokových	úrokový	k2eAgFnPc2d1
sazeb	sazba	k1gFnPc2
a	a	k8xC
ke	k	k7c3
zvýšení	zvýšení	k1gNnSc3
objemu	objem	k1gInSc2
investic	investice	k1gFnPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
výnosné	výnosný	k2eAgNnSc1d1
a	a	k8xC
přinést	přinést	k5eAaPmF
celkové	celkový	k2eAgNnSc4d1
zvýšení	zvýšení	k1gNnSc4
příjmů	příjem	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Fiskální	fiskální	k2eAgNnPc1d1
opatření	opatření	k1gNnPc1
</s>
<s>
Jméno	jméno	k1gNnSc1
Johna	John	k1gMnSc2
Keynese	Keynese	k1gFnSc2
je	být	k5eAaImIp3nS
spojováno	spojovat	k5eAaImNgNnS
spíše	spíše	k9
s	s	k7c7
fiskálními	fiskální	k2eAgInPc7d1
než	než	k8xS
monetárními	monetární	k2eAgInPc7d1
přístupy	přístup	k1gInPc7
<g/>
,	,	kIx,
i	i	k8xC
přestože	přestože	k8xS
v	v	k7c6
Obecné	obecný	k2eAgFnSc6d1
teorii	teorie	k1gFnSc6
se	se	k3xPyFc4
jim	on	k3xPp3gFnPc3
dostává	dostávat	k5eAaImIp3nS
jen	jen	k9
malé	malý	k2eAgNnSc1d1
(	(	kIx(
<g/>
a	a	k8xC
často	často	k6eAd1
satirické	satirický	k2eAgFnSc2d1
<g/>
)	)	kIx)
pozornosti	pozornost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zmiňuje	zmiňovat	k5eAaImIp3nS
“	“	k?
<g/>
zvýšení	zvýšení	k1gNnSc4
veřejných	veřejný	k2eAgFnPc2d1
prací	práce	k1gFnPc2
<g/>
”	”	k?
jako	jako	k8xS,k8xC
příklad	příklad	k1gInSc1
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
co	co	k3yQnSc4,k3yInSc4,k3yRnSc4
může	moct	k5eAaImIp3nS
znásobit	znásobit	k5eAaPmF
zaměstnanost	zaměstnanost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
myšlenka	myšlenka	k1gFnSc1
ovšem	ovšem	k9
předchází	předcházet	k5eAaImIp3nS
vývoj	vývoj	k1gInSc1
jeho	jeho	k3xOp3gFnSc2
relevantní	relevantní	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
a	a	k8xC
už	už	k6eAd1
se	se	k3xPyFc4
jí	jíst	k5eAaImIp3nS
poté	poté	k6eAd1
dále	daleko	k6eAd2
nezabývá	zabývat	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Keynesiánské	keynesiánský	k2eAgInPc1d1
modely	model	k1gInPc1
a	a	k8xC
koncepty	koncept	k1gInPc1
</s>
<s>
Agregátní	agregátní	k2eAgFnSc1d1
poptávka	poptávka	k1gFnSc1
</s>
<s>
Keynesiánsko	Keynesiánsko	k1gNnSc1
<g/>
/	/	kIx~
<g/>
Samuelsonský	Samuelsonský	k2eAgInSc1d1
kříž	kříž	k1gInSc1
</s>
<s>
Keynesův	Keynesův	k2eAgInSc1d1
pohled	pohled	k1gInSc1
na	na	k7c4
úspory	úspora	k1gFnPc4
a	a	k8xC
investice	investice	k1gFnPc4
byl	být	k5eAaImAgInS
jeho	jeho	k3xOp3gInSc1
nejdůležitější	důležitý	k2eAgInSc1d3
odklon	odklon	k1gInSc1
od	od	k7c2
klasického	klasický	k2eAgInSc2d1
názoru	názor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
ilustrováno	ilustrovat	k5eAaBmNgNnS
pomocí	pomocí	k7c2
“	“	k?
<g/>
Keynesiánského	keynesiánský	k2eAgInSc2d1
kříže	kříž	k1gInSc2
<g/>
”	”	k?
<g/>
,	,	kIx,
který	který	k3yQgInSc4,k3yRgInSc4,k3yIgInSc4
vymyslel	vymyslet	k5eAaPmAgMnS
Paul	Paul	k1gMnSc1
Samuelson	Samuelson	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Horizontální	horizontální	k2eAgFnSc1d1
osa	osa	k1gFnSc1
označuje	označovat	k5eAaImIp3nS
celkový	celkový	k2eAgInSc4d1
příjem	příjem	k1gInSc4
a	a	k8xC
fialová	fialový	k2eAgFnSc1d1
křivka	křivka	k1gFnSc1
zobrazuje	zobrazovat	k5eAaImIp3nS
C	C	kA
<g/>
(	(	kIx(
<g/>
Y	Y	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
sklon	sklon	k1gInSc1
ke	k	k7c3
spotřebě	spotřeba	k1gFnSc3
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc1
doplněk	doplněk	k1gInSc1
S	s	k7c7
<g/>
(	(	kIx(
<g/>
Y	Y	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
sklon	sklon	k1gInSc4
k	k	k7c3
spoření	spoření	k1gNnSc3
<g/>
:	:	kIx,
součet	součet	k1gInSc4
těchto	tento	k3xDgFnPc2
dvou	dva	k4xCgFnPc2
funkcí	funkce	k1gFnPc2
se	se	k3xPyFc4
rovná	rovnat	k5eAaImIp3nS
celkovému	celkový	k2eAgInSc3d1
příjmu	příjem	k1gInSc3
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
zobrazen	zobrazit	k5eAaPmNgInS
přerušovanou	přerušovaný	k2eAgFnSc7d1
čárou	čára	k1gFnSc7
o	o	k7c4
45	#num#	k4
<g/>
°	°	k?
<g/>
.	.	kIx.
</s>
<s>
Horizontální	horizontální	k2eAgFnSc1d1
modrá	modrý	k2eAgFnSc1d1
čára	čára	k1gFnSc1
I	I	kA
<g/>
(	(	kIx(
<g/>
r	r	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
plán	plán	k1gInSc4
mezní	mezní	k2eAgFnSc2d1
efektivnosti	efektivnost	k1gFnSc2
kapitálu	kapitál	k1gInSc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc1
hodnota	hodnota	k1gFnSc1
je	být	k5eAaImIp3nS
nezávislá	závislý	k2eNgFnSc1d1
na	na	k7c4
Y.	Y.	kA
Keynes	Keynes	k1gMnSc1
to	ten	k3xDgNnSc4
interpretuje	interpretovat	k5eAaBmIp3nS
jako	jako	k8xC,k8xS
poptávku	poptávka	k1gFnSc4
po	po	k7c6
investicích	investice	k1gFnPc6
a	a	k8xC
označuje	označovat	k5eAaImIp3nS
součet	součet	k1gInSc1
požadavků	požadavek	k1gInPc2
na	na	k7c4
spotřebu	spotřeba	k1gFnSc4
a	a	k8xC
investice	investice	k1gFnPc4
jako	jako	k8xS,k8xC
“	“	k?
<g/>
agregátní	agregátní	k2eAgFnSc4d1
poptávku	poptávka	k1gFnSc4
<g/>
”	”	k?
<g/>
,	,	kIx,
v	v	k7c6
grafu	graf	k1gInSc6
vynesenou	vynesený	k2eAgFnSc4d1
jako	jako	k8xS,k8xC
samostatnou	samostatný	k2eAgFnSc4d1
křivku	křivka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Agregátní	agregátní	k2eAgFnSc1d1
poptávka	poptávka	k1gFnSc1
musí	muset	k5eAaImIp3nS
odpovídat	odpovídat	k5eAaImF
celkovým	celkový	k2eAgInPc3d1
příjmům	příjem	k1gInPc3
<g/>
,	,	kIx,
takže	takže	k8xS
rovnovážný	rovnovážný	k2eAgInSc1d1
příjem	příjem	k1gInSc1
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
určen	určit	k5eAaPmNgInS
podle	podle	k7c2
okamžiku	okamžik	k1gInSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
křivka	křivka	k1gFnSc1
agregátní	agregátní	k2eAgFnSc2d1
poptávky	poptávka	k1gFnSc2
překročí	překročit	k5eAaPmIp3nS
hranici	hranice	k1gFnSc4
45	#num#	k4
<g/>
°	°	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
je	být	k5eAaImIp3nS
stejná	stejný	k2eAgFnSc1d1
horizontální	horizontální	k2eAgFnSc1d1
poloha	poloha	k1gFnSc1
jako	jako	k8xC,k8xS
průsečík	průsečík	k1gInSc1
I	I	kA
<g/>
(	(	kIx(
<g/>
r	r	kA
<g/>
)	)	kIx)
s	s	k7c7
S	s	k7c7
<g/>
(	(	kIx(
<g/>
Y	Y	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Rovnice	rovnice	k1gFnSc1
I	I	kA
<g/>
(	(	kIx(
<g/>
r	r	kA
<g/>
)	)	kIx)
=	=	kIx~
S	s	k7c7
<g/>
(	(	kIx(
<g/>
Y	Y	kA
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
přijata	přijmout	k5eAaPmNgFnS
klasiky	klasik	k1gMnPc7
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
ji	on	k3xPp3gFnSc4
považovali	považovat	k5eAaImAgMnP
za	za	k7c4
podmínku	podmínka	k1gFnSc4
rovnováhy	rovnováha	k1gFnSc2
mezi	mezi	k7c7
nabídkou	nabídka	k1gFnSc7
a	a	k8xC
poptávkou	poptávka	k1gFnSc7
po	po	k7c6
investičních	investiční	k2eAgInPc6d1
fondech	fond	k1gInPc6
a	a	k8xC
stanovením	stanovení	k1gNnSc7
úrokové	úrokový	k2eAgFnSc2d1
sazby	sazba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
však	však	k9
měli	mít	k5eAaImAgMnP
koncept	koncept	k1gInSc4
agregátní	agregátní	k2eAgFnSc2d1
poptávky	poptávka	k1gFnSc2
<g/>
,	,	kIx,
viděli	vidět	k5eAaImAgMnP
poptávku	poptávka	k1gFnSc4
po	po	k7c6
investicích	investice	k1gFnPc6
jako	jako	k8xS,k8xC
S	s	k7c7
<g/>
(	(	kIx(
<g/>
Y	Y	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
pro	pro	k7c4
ně	on	k3xPp3gNnSc4
spoření	spoření	k1gNnSc1
bylo	být	k5eAaImAgNnS
pouze	pouze	k6eAd1
nepřímým	přímý	k2eNgInSc7d1
nákupem	nákup	k1gInSc7
investičního	investiční	k2eAgInSc2d1
majetku	majetek	k1gInSc2
<g/>
,	,	kIx,
takže	takže	k8xS
agregátní	agregátní	k2eAgFnSc1d1
poptávka	poptávka	k1gFnSc1
spolu	spolu	k6eAd1
s	s	k7c7
výsledkem	výsledek	k1gInSc7
<g/>
,	,	kIx,
že	že	k8xS
agregátní	agregátní	k2eAgFnSc1d1
poptávka	poptávka	k1gFnSc1
byla	být	k5eAaImAgFnS
rovna	roven	k2eAgFnSc1d1
celkovému	celkový	k2eAgInSc3d1
příjmu	příjem	k1gInSc3
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
podstatě	podstata	k1gFnSc6
<g/>
,	,	kIx,
spíš	spíš	k9
než	než	k8xS
rovnovážný	rovnovážný	k2eAgInSc4d1
stav	stav	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Keynes	Keynes	k1gInSc1
bere	brát	k5eAaImIp3nS
na	na	k7c6
vědomí	vědomí	k1gNnSc6
tento	tento	k3xDgInSc1
názor	názor	k1gInSc1
v	v	k7c6
kapitole	kapitola	k1gFnSc6
2	#num#	k4
<g/>
,	,	kIx,
kde	kde	k6eAd1
ho	on	k3xPp3gMnSc4
nalézá	nalézat	k5eAaImIp3nS
v	v	k7c6
brzkých	brzký	k2eAgInPc6d1
spisech	spis	k1gInPc6
Alfreda	Alfred	k1gMnSc2
Marshalla	Marshall	k1gMnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
dodává	dodávat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
“	“	k?
<g/>
doktrína	doktrína	k1gFnSc1
není	být	k5eNaImIp3nS
dnes	dnes	k6eAd1
v	v	k7c6
této	tento	k3xDgFnSc6
hrubé	hrubý	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
nikde	nikde	k6eAd1
uvedena	uveden	k2eAgFnSc1d1
<g/>
”	”	k?
<g/>
.	.	kIx.
</s>
<s>
Rovnice	rovnice	k1gFnSc1
I	I	kA
<g/>
(	(	kIx(
<g/>
r	r	kA
<g/>
)	)	kIx)
=	=	kIx~
S	s	k7c7
<g/>
(	(	kIx(
<g/>
Y	Y	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
přijata	přijmout	k5eAaPmNgFnS
Keynesem	Keynes	k1gInSc7
pro	pro	k7c4
některé	některý	k3yIgInPc4
nebo	nebo	k8xC
všechny	všechen	k3xTgInPc4
z	z	k7c2
následujících	následující	k2eAgInPc2d1
důvodů	důvod	k1gInPc2
<g/>
:	:	kIx,
</s>
<s>
V	v	k7c6
důsledku	důsledek	k1gInSc6
“	“	k?
<g/>
principu	princip	k1gInSc2
efektivní	efektivní	k2eAgFnSc2d1
poptávky	poptávka	k1gFnSc2
<g/>
”	”	k?
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
tvrdí	tvrdit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
agregátní	agregátní	k2eAgFnSc1d1
poptávka	poptávka	k1gFnSc1
musí	muset	k5eAaImIp3nS
odpovídat	odpovídat	k5eAaImF
celkovým	celkový	k2eAgInPc3d1
příjmům	příjem	k1gInPc3
(	(	kIx(
<g/>
Kapitola	kapitola	k1gFnSc1
3	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Jako	jako	k8xC,k8xS
důsledek	důsledek	k1gInSc1
identifikace	identifikace	k1gFnSc2
úspor	úspora	k1gFnPc2
s	s	k7c7
investicemi	investice	k1gFnPc7
(	(	kIx(
<g/>
kapitola	kapitola	k1gFnSc1
6	#num#	k4
<g/>
)	)	kIx)
spolu	spolu	k6eAd1
s	s	k7c7
rovnovážným	rovnovážný	k2eAgInSc7d1
předpokladem	předpoklad	k1gInSc7
<g/>
,	,	kIx,
že	že	k8xS
tato	tento	k3xDgNnPc1
množství	množství	k1gNnPc1
odpovídají	odpovídat	k5eAaImIp3nP
jejich	jejich	k3xOp3gInPc3
požadavkům	požadavek	k1gInPc3
<g/>
.	.	kIx.
</s>
<s>
Je	být	k5eAaImIp3nS
v	v	k7c6
souladu	soulad	k1gInSc6
s	s	k7c7
podstatou	podstata	k1gFnSc7
klasické	klasický	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
trhu	trh	k1gInSc2
s	s	k7c7
investičními	investiční	k2eAgInPc7d1
fondy	fond	k1gInPc7
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc1
závěr	závěr	k1gInSc1
považuje	považovat	k5eAaImIp3nS
klasiky	klasika	k1gFnPc4
za	za	k7c4
nesprávně	správně	k6eNd1
vyložené	vyložený	k2eAgInPc4d1
pomocí	pomocí	k7c2
kruhového	kruhový	k2eAgNnSc2d1
uvažování	uvažování	k1gNnSc2
(	(	kIx(
<g/>
Kapitola	kapitola	k1gFnSc1
14	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Tyto	tento	k3xDgInPc1
argumenty	argument	k1gInPc1
se	se	k3xPyFc4
vzájemně	vzájemně	k6eAd1
podporují	podporovat	k5eAaImIp3nP
podle	podle	k7c2
Keynesových	Keynesový	k2eAgInPc2d1
předpokladů	předpoklad	k1gInPc2
<g/>
,	,	kIx,
ale	ale	k8xC
nemusí	muset	k5eNaImIp3nS
nutně	nutně	k6eAd1
splňovat	splňovat	k5eAaImF
předpoklady	předpoklad	k1gInPc4
obecnější	obecní	k2eAgInPc4d2
<g/>
,	,	kIx,
např.	např.	kA
pokud	pokud	k8xS
se	se	k3xPyFc4
bavíme	bavit	k5eAaImIp1nP
o	o	k7c6
zahraničním	zahraniční	k2eAgInSc6d1
obchodu	obchod	k1gInSc6
jako	jako	k8xC,k8xS
v	v	k7c6
Mundell-Flemingově	Mundell-Flemingově	k1gFnSc6
modelu	model	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Keynesiánský	keynesiánský	k2eAgInSc1d1
multiplikátor	multiplikátor	k1gInSc1
(	(	kIx(
<g/>
násobitel	násobitel	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Keynes	Keynes	k1gMnSc1
představuje	představovat	k5eAaImIp3nS
svou	svůj	k3xOyFgFnSc4
diskuzi	diskuze	k1gFnSc4
o	o	k7c6
multiplikátoru	multiplikátor	k1gInSc6
v	v	k7c6
kapitole	kapitola	k1gFnSc6
10	#num#	k4
s	s	k7c7
odkazem	odkaz	k1gInSc7
na	na	k7c4
Kahnovu	Kahnův	k2eAgFnSc4d1
dřívější	dřívější	k2eAgFnSc4d1
práci	práce	k1gFnSc4
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
výše	výše	k1gFnSc2,k1gFnSc2wB
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kahnův	Kahnův	k2eAgInSc1d1
multiplikátor	multiplikátor	k1gInSc1
označuje	označovat	k5eAaImIp3nS
jako	jako	k9
“	“	k?
<g/>
multiplikátor	multiplikátor	k1gInSc1
zaměstnanosti	zaměstnanost	k1gFnSc2
<g/>
”	”	k?
odlišným	odlišný	k2eAgMnPc3d1
od	od	k7c2
svého	své	k1gNnSc2
“	“	k?
<g/>
investičního	investiční	k2eAgInSc2d1
multiplikátoru	multiplikátor	k1gInSc2
<g/>
”	”	k?
a	a	k8xC
říká	říkat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
oba	dva	k4xCgMnPc1
jsou	být	k5eAaImIp3nP
jen	jen	k6eAd1
“	“	k?
<g/>
trochu	trochu	k6eAd1
jiné	jiný	k2eAgNnSc4d1
<g/>
”	”	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kahnovu	Kahnov	k1gInSc3
multiplikátoru	multiplikátor	k1gInSc2
byla	být	k5eAaImAgFnS
v	v	k7c6
následujících	následující	k2eAgInPc6d1
Keynesiánských	keynesiánský	k2eAgInPc6d1
spisech	spis	k1gInPc6
přiřazena	přiřadit	k5eAaPmNgFnS
přední	přední	k2eAgFnSc1d1
úloha	úloha	k1gFnSc1
pro	pro	k7c4
vývoj	vývoj	k1gInSc4
Keynesovy	Keynesův	k2eAgFnSc2d1
vlastní	vlastní	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
–	–	k?
výklad	výklad	k1gInSc1
podpořený	podpořený	k2eAgInSc1d1
obtížnějším	obtížný	k2eAgNnSc7d2
pochopením	pochopení	k1gNnSc7
Keynesova	Keynesův	k2eAgNnSc2d1
podání	podání	k1gNnSc2
<g/>
..	..	k?
Kahnův	Kahnův	k2eAgInSc1d1
multiplikátor	multiplikátor	k1gInSc1
dává	dávat	k5eAaImIp3nS
název	název	k1gInSc4
(	(	kIx(
<g/>
“	“	k?
<g/>
Model	model	k1gInSc1
multiplikátoru	multiplikátor	k1gInSc2
<g/>
”	”	k?
<g/>
)	)	kIx)
na	na	k7c4
konto	konto	k1gNnSc4
Keynesiánské	keynesiánský	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
v	v	k7c6
Samuelsonově	Samuelsonův	k2eAgFnSc6d1
ekonomii	ekonomie	k1gFnSc6
a	a	k8xC
je	být	k5eAaImIp3nS
téměř	téměř	k6eAd1
tak	tak	k6eAd1
významný	významný	k2eAgInSc4d1
v	v	k7c6
díle	dílo	k1gNnSc6
“	“	k?
<g/>
A	a	k8xC
Guide	Guid	k1gInSc5
to	ten	k3xDgNnSc4
Keynes	Keynes	k1gMnSc1
<g/>
”	”	k?
od	od	k7c2
Alvina	Alvin	k2eAgFnSc1d1
Hansena	Hansen	k2eAgFnSc1d1
a	a	k8xC
v	v	k7c6
“	“	k?
<g/>
Introduction	Introduction	k1gInSc1
to	ten	k3xDgNnSc1
the	the	k?
Theory	Theor	k1gInPc1
of	of	k?
Employment	Employment	k1gInSc1
<g/>
”	”	k?
od	od	k7c2
Joany	Joana	k1gFnSc2
Robinsonové	Robinsonová	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Hodnota	hodnota	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
Keynes	Keynes	k1gMnSc1
přiřazuje	přiřazovat	k5eAaImIp3nS
Kahnově	Kahnov	k1gInSc6
multiplikátoru	multiplikátor	k1gInSc6
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
reciproční	reciproční	k2eAgInSc1d1
mezní	mezní	k2eAgInSc1d1
sklon	sklon	k1gInSc1
ke	k	k7c3
spoření	spoření	k1gNnSc3
<g/>
:	:	kIx,
k	k	k7c3
=	=	kIx~
1	#num#	k4
/	/	kIx~
S	s	k7c7
<g/>
'	'	kIx"
<g/>
(	(	kIx(
<g/>
Y	Y	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odpovídající	odpovídající	k2eAgInSc4d1
vzorec	vzorec	k1gInSc4
pro	pro	k7c4
Kahnův	Kahnův	k2eAgInSc4d1
násobitel	násobitel	k1gMnSc1
v	v	k7c6
uzavřené	uzavřený	k2eAgFnSc6d1
ekonomice	ekonomika	k1gFnSc6
by	by	kYmCp3nS
měl	mít	k5eAaImAgInS
být	být	k5eAaImF
k	k	k7c3
<g/>
'	'	kIx"
<g/>
=	=	kIx~
1	#num#	k4
/	/	kIx~
H	H	kA
<g/>
'	'	kIx"
(	(	kIx(
<g/>
Y	Y	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
H	H	kA
'	'	kIx"
<g/>
(	(	kIx(
<g/>
Y	Y	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
mezní	mezní	k2eAgInSc1d1
sklon	sklon	k1gInSc1
k	k	k7c3
hromadění	hromadění	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Keynesův	Keynesův	k2eAgInSc1d1
vzorec	vzorec	k1gInSc1
je	být	k5eAaImIp3nS
stejný	stejný	k2eAgInSc1d1
jako	jako	k8xS,k8xC
Khanův	Khanův	k2eAgInSc1d1
jedině	jedině	k6eAd1
v	v	k7c6
tom	ten	k3xDgInSc6
případě	případ	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
úspory	úspora	k1gFnPc1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
identifikovány	identifikován	k2eAgFnPc1d1
s	s	k7c7
hromaděním	hromadění	k1gNnSc7
zásob	zásoba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Keynes	Keynes	k1gInSc1
dal	dát	k5eAaPmAgInS
jeho	jeho	k3xOp3gInPc7
vzorci	vzorec	k1gInPc7
téměř	téměř	k6eAd1
status	status	k1gInSc4
definice	definice	k1gFnSc2
(	(	kIx(
<g/>
předkládá	předkládat	k5eAaImIp3nS
se	se	k3xPyFc4
před	před	k7c7
jakýmkoli	jakýkoli	k3yIgNnSc7
vysvětlením	vysvětlení	k1gNnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc4
násobitel	násobitel	k1gMnSc1
je	být	k5eAaImIp3nS
skutečně	skutečně	k6eAd1
hodnota	hodnota	k1gFnSc1
"	"	kIx"
<g/>
poměru	poměr	k1gInSc2
...	...	k?
mezi	mezi	k7c7
přírůstkem	přírůstek	k1gInSc7
investic	investice	k1gFnPc2
a	a	k8xC
odpovídajícím	odpovídající	k2eAgInSc7d1
přírůstkem	přírůstek	k1gInSc7
souhrnného	souhrnný	k2eAgInSc2d1
příjmu	příjem	k1gInSc2
<g/>
"	"	kIx"
podle	podle	k7c2
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
modelu	model	k1gInSc2
preference	preference	k1gFnSc1
likvidity	likvidita	k1gFnSc2
v	v	k7c6
kapitole	kapitola	k1gFnSc6
13	#num#	k4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
příjmy	příjem	k1gInPc1
musí	muset	k5eAaImIp3nP
nést	nést	k5eAaImF
celý	celý	k2eAgInSc4d1
účinek	účinek	k1gInSc4
změny	změna	k1gFnSc2
v	v	k7c6
investování	investování	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ale	ale	k8xC
<g/>
,	,	kIx,
podle	podle	k7c2
modelu	model	k1gInSc2
z	z	k7c2
kapitoly	kapitola	k1gFnSc2
15	#num#	k4
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
změna	změna	k1gFnSc1
v	v	k7c6
rozvrhu	rozvrh	k1gInSc6
marginální	marginální	k2eAgFnSc2d1
efektivnosti	efektivnost	k1gFnSc2
kapitálu	kapitál	k1gInSc2
účinek	účinek	k1gInSc1
sdílený	sdílený	k2eAgInSc1d1
mezi	mezi	k7c7
úrokovou	úrokový	k2eAgFnSc7d1
sazbou	sazba	k1gFnSc7
a	a	k8xC
příjmem	příjem	k1gInSc7
v	v	k7c6
poměrech	poměr	k1gInPc6
závisejících	závisející	k2eAgInPc6d1
na	na	k7c4
parciální	parciální	k2eAgFnSc6d1
derivacích	derivace	k1gFnPc6
funkce	funkce	k1gFnSc2
preference	preference	k1gFnSc2
likvidity	likvidita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledný	výsledný	k2eAgInSc1d1
násobitel	násobitel	k1gInSc1
má	mít	k5eAaImIp3nS
složitější	složitý	k2eAgInSc1d2
vzorec	vzorec	k1gInSc1
a	a	k8xC
menší	malý	k2eAgFnSc4d2
číselnou	číselný	k2eAgFnSc4d1
hodnotu	hodnota	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Past	past	k1gFnSc1
na	na	k7c4
likviditu	likvidita	k1gFnSc4
<g/>
:	:	kIx,
diagram	diagram	k1gInSc4
založený	založený	k2eAgInSc4d1
na	na	k7c6
Keynesovi	Keynes	k1gMnSc6
<g/>
.	.	kIx.
</s>
<s>
Past	past	k1gFnSc1
na	na	k7c4
likviditu	likvidita	k1gFnSc4
</s>
<s>
Past	past	k1gFnSc1
na	na	k7c4
likviditu	likvidita	k1gFnSc4
je	být	k5eAaImIp3nS
fenomén	fenomén	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
může	moct	k5eAaImIp3nS
překážet	překážet	k5eAaImF
efektivitě	efektivita	k1gFnSc3
měnové	měnový	k2eAgFnSc2d1
politiky	politika	k1gFnSc2
při	při	k7c6
snižování	snižování	k1gNnSc6
nezaměstnanosti	nezaměstnanost	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Graf	graf	k1gInSc1
modelu	model	k1gInSc2
IS_LMObecně	IS_LMObecně	k1gMnPc2
se	se	k3xPyFc4
má	mít	k5eAaImIp3nS
za	za	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
úroková	úrokový	k2eAgFnSc1d1
sazba	sazba	k1gFnSc1
nespadá	spadat	k5eNaPmIp3nS,k5eNaImIp3nS
pod	pod	k7c4
určitý	určitý	k2eAgInSc4d1
limit	limit	k1gInSc4
<g/>
,	,	kIx,
často	často	k6eAd1
považovaný	považovaný	k2eAgInSc1d1
za	za	k7c4
nulový	nulový	k2eAgInSc4d1
nebo	nebo	k8xC
mírně	mírně	k6eAd1
záporný	záporný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Keynes	Keynes	k1gMnSc1
navrhl	navrhnout	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
tento	tento	k3xDgInSc1
limit	limit	k1gInSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
značně	značně	k6eAd1
větší	veliký	k2eAgFnSc1d2
než	než	k8xS
nula	nula	k1gFnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
nepřidělil	přidělit	k5eNaPmAgMnS
mu	on	k3xPp3gMnSc3
příliš	příliš	k6eAd1
praktický	praktický	k2eAgInSc1d1
význam	význam	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Termín	termín	k1gInSc1
"	"	kIx"
<g/>
past	past	k1gFnSc1
likvidity	likvidita	k1gFnSc2
<g/>
"	"	kIx"
byl	být	k5eAaImAgInS
vytvořen	vytvořit	k5eAaPmNgInS
Dennisem	Dennis	k1gInSc7
Robertsonem	Robertson	k1gInSc7
v	v	k7c6
jeho	jeho	k3xOp3gInPc6
komentářích	komentář	k1gInPc6
k	k	k7c3
Obecné	obecný	k2eAgFnSc3d1
teorii	teorie	k1gFnSc3
<g/>
,	,	kIx,
ale	ale	k8xC
byl	být	k5eAaImAgMnS
to	ten	k3xDgNnSc4
John	John	k1gMnSc1
Hicks	Hicks	k1gInSc1
v	v	k7c6
"	"	kIx"
<g/>
Mr	Mr	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Keynes	Keynes	k1gMnSc1
and	and	k?
The	The	k1gFnSc2
Classics	Classicsa	k1gFnPc2
"	"	kIx"
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
v	v	k7c6
něm	on	k3xPp3gNnSc6
spatřil	spatřit	k5eAaPmAgMnS
trochu	trochu	k6eAd1
jiný	jiný	k2eAgInSc4d1
koncept	koncept	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Pokud	pokud	k8xS
je	být	k5eAaImIp3nS
ekonomika	ekonomika	k1gFnSc1
v	v	k7c6
takovém	takový	k3xDgInSc6
postavení	postavení	k1gNnSc6
<g/>
,	,	kIx,
že	že	k8xS
křivka	křivka	k1gFnSc1
preferencí	preference	k1gFnPc2
likvidity	likvidita	k1gFnSc2
je	být	k5eAaImIp3nS
téměř	téměř	k6eAd1
svislá	svislý	k2eAgFnSc1d1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
se	se	k3xPyFc4
musí	muset	k5eAaImIp3nS
stát	stát	k5eAaImF,k5eAaPmF
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
blíží	blížit	k5eAaImIp3nS
dolní	dolní	k2eAgFnSc1d1
hranice	hranice	k1gFnSc1
r	r	kA
<g/>
,	,	kIx,
potom	potom	k8xC
změna	změna	k1gFnSc1
peněžní	peněžní	k2eAgFnPc1d1
zásoby	zásoba	k1gFnPc1
M	M	kA
<g/>
̂	̂	k?
téměř	téměř	k6eAd1
nezmění	změnit	k5eNaPmIp3nS
rovnováhu	rovnováha	k1gFnSc4
úrokové	úrokový	k2eAgFnSc2d1
míry	míra	k1gFnSc2
r	r	kA
<g/>
̂	̂	k?
nebo	nebo	k8xC
<g/>
,	,	kIx,
pokud	pokud	k8xS
nedojde	dojít	k5eNaPmIp3nS
k	k	k7c3
kompenzování	kompenzování	k1gNnSc3
strmosti	strmost	k1gFnSc2
v	v	k7c6
ostatních	ostatní	k2eAgFnPc6d1
křivkách	křivka	k1gFnPc6
<g/>
,	,	kIx,
k	k	k7c3
výslednému	výsledný	k2eAgInSc3d1
příjmu	příjem	k1gInSc2
Ŷ	Ŷ	k?
Jak	jak	k6eAd1
uvedl	uvést	k5eAaPmAgMnS
Hicks	Hicks	k1gInSc4
<g/>
,	,	kIx,
"	"	kIx"
<g/>
peněžní	peněžní	k2eAgInPc1d1
prostředky	prostředek	k1gInPc1
nebudou	být	k5eNaImBp3nP
dále	daleko	k6eAd2
zvyšovat	zvyšovat	k5eAaImF
úrokovou	úrokový	k2eAgFnSc4d1
sazbu	sazba	k1gFnSc4
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s>
Paul	Paul	k1gMnSc1
Krugman	Krugman	k1gMnSc1
rozsáhle	rozsáhle	k6eAd1
pracoval	pracovat	k5eAaImAgMnS
na	na	k7c6
pasti	past	k1gFnSc6
likvidity	likvidita	k1gFnSc2
a	a	k8xC
tvrdil	tvrdit	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
problém	problém	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
stojí	stát	k5eAaImIp3nS
před	před	k7c7
japonskou	japonský	k2eAgFnSc7d1
ekonomikou	ekonomika	k1gFnSc7
kolem	kolem	k7c2
přelomu	přelom	k1gInSc2
tisíciletí	tisíciletí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
svých	svůj	k3xOyFgNnPc6
pozdějších	pozdní	k2eAgNnPc6d2
slovech	slovo	k1gNnPc6
<g/>
:	:	kIx,
</s>
<s>
“	“	k?
<g/>
Krátkodobé	krátkodobý	k2eAgFnPc4d1
úrokové	úrokový	k2eAgFnPc4d1
sazby	sazba	k1gFnPc4
byly	být	k5eAaImAgFnP
téměř	téměř	k6eAd1
nulové	nulový	k2eAgFnPc1d1
<g/>
,	,	kIx,
dlouhodobé	dlouhodobý	k2eAgFnPc1d1
sazby	sazba	k1gFnPc1
byly	být	k5eAaImAgFnP
na	na	k7c6
historických	historický	k2eAgNnPc6d1
minimech	minimum	k1gNnPc6
<g/>
,	,	kIx,
přesto	přesto	k8xC
soukromé	soukromý	k2eAgInPc4d1
investiční	investiční	k2eAgInPc4d1
výdaje	výdaj	k1gInPc4
zůstaly	zůstat	k5eAaPmAgFnP
nedostatečné	nedostatečná	k1gFnPc1
pro	pro	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
ekonomika	ekonomika	k1gFnSc1
vyhnula	vyhnout	k5eAaPmAgFnS
deflaci	deflace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
prostředí	prostředí	k1gNnSc6
byla	být	k5eAaImAgFnS
měnová	měnový	k2eAgFnSc1d1
politika	politika	k1gFnSc1
stejně	stejně	k6eAd1
neefektivní	efektivní	k2eNgFnSc1d1
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
popsal	popsat	k5eAaPmAgMnS
Keynes	Keynes	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokusy	pokus	k1gInPc7
Japonské	japonský	k2eAgFnSc2d1
Banky	banka	k1gFnSc2
zvýšit	zvýšit	k5eAaPmF
peněžní	peněžní	k2eAgFnPc1d1
zásoby	zásoba	k1gFnPc1
jednoduše	jednoduše	k6eAd1
přispěly	přispět	k5eAaPmAgFnP
k	k	k7c3
již	již	k6eAd1
velkým	velký	k2eAgFnPc3d1
bankovním	bankovní	k2eAgFnPc3d1
rezervám	rezerva	k1gFnPc3
a	a	k8xC
veřejným	veřejný	k2eAgNnSc7d1
vlastnictvím	vlastnictví	k1gNnSc7
hotovosti	hotovost	k1gFnSc2
…	…	k?
<g/>
”	”	k?
</s>
<s>
Model	model	k1gInSc1
IS-LM	IS-LM	k1gFnSc2
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
IS-LM	IS-LM	k1gFnSc1
model	model	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Hicks	Hicks	k1gInSc1
ukázal	ukázat	k5eAaPmAgInS
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
analyzovat	analyzovat	k5eAaImF
Keynesův	Keynesův	k2eAgInSc4d1
systém	systém	k1gInSc4
<g/>
,	,	kIx,
když	když	k8xS
preference	preference	k1gFnSc1
likvidity	likvidita	k1gFnSc2
je	být	k5eAaImIp3nS
funkcí	funkce	k1gFnSc7
příjmů	příjem	k1gInPc2
stejně	stejně	k6eAd1
jako	jako	k8xC,k8xS
úrokové	úrokový	k2eAgFnPc4d1
sazby	sazba	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Keynesovo	Keynesův	k2eAgNnSc1d1
přijetí	přijetí	k1gNnSc4
příjmů	příjem	k1gInPc2
jako	jako	k8xC,k8xS
vliv	vliv	k1gInSc4
na	na	k7c4
poptávku	poptávka	k1gFnSc4
po	po	k7c6
penězích	peníze	k1gInPc6
je	být	k5eAaImIp3nS
krok	krok	k1gInSc1
zpět	zpět	k6eAd1
směrem	směr	k1gInSc7
k	k	k7c3
klasické	klasický	k2eAgFnSc3d1
teorii	teorie	k1gFnSc3
a	a	k8xC
Hicks	Hicks	k1gInSc1
podniká	podnikat	k5eAaImIp3nS
další	další	k2eAgInSc4d1
krok	krok	k1gInSc4
stejným	stejný	k2eAgInSc7d1
směrem	směr	k1gInSc7
zobecněním	zobecnění	k1gNnSc7
sklonu	sklon	k1gInSc3
ke	k	k7c3
spoření	spoření	k1gNnSc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
vzal	vzít	k5eAaPmAgMnS
oba	dva	k4xCgInPc1
Y	Y	kA
a	a	k8xC
r	r	kA
jako	jako	k8xC,k8xS
argumenty	argument	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Méně	málo	k6eAd2
klasicky	klasicky	k6eAd1
rozšiřuje	rozšiřovat	k5eAaImIp3nS
tuto	tento	k3xDgFnSc4
generalizaci	generalizace	k1gFnSc4
na	na	k7c4
časový	časový	k2eAgInSc4d1
horizont	horizont	k1gInSc4
mezní	mezní	k2eAgFnSc2d1
výkonnosti	výkonnost	k1gFnSc2
kapitálu	kapitál	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Model	model	k1gInSc1
IS-LM	IS-LM	k1gFnSc2
používá	používat	k5eAaImIp3nS
k	k	k7c3
vyjádření	vyjádření	k1gNnSc3
Keynesova	Keynesův	k2eAgInSc2d1
modelu	model	k1gInSc2
dvě	dva	k4xCgFnPc1
rovnice	rovnice	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgMnSc1
<g/>
,	,	kIx,
psaná	psaný	k2eAgFnSc1d1
I	I	kA
<g/>
(	(	kIx(
<g/>
Y	Y	kA
<g/>
,	,	kIx,
<g/>
r	r	kA
)	)	kIx)
=	=	kIx~
S	s	k7c7
(	(	kIx(
<g/>
Y	Y	kA
<g/>
,	,	kIx,
<g/>
r	r	kA
)	)	kIx)
<g/>
,	,	kIx,
vyjadřuje	vyjadřovat	k5eAaImIp3nS
princip	princip	k1gInSc1
efektivní	efektivní	k2eAgFnSc2d1
poptávky	poptávka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Můžeme	moct	k5eAaImIp1nP
vytvořit	vytvořit	k5eAaPmF
graf	graf	k1gInSc4
na	na	k7c6
souřadnicích	souřadnice	k1gFnPc6
(	(	kIx(
<g/>
Y	Y	kA
<g/>
,	,	kIx,
r	r	kA
<g/>
)	)	kIx)
a	a	k8xC
nakreslit	nakreslit	k5eAaPmF
čáru	čára	k1gFnSc4
spojující	spojující	k2eAgMnSc1d1
ty	ten	k3xDgInPc4
body	bod	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
vyhovují	vyhovovat	k5eAaImIp3nP
dané	daný	k2eAgFnSc3d1
rovnici	rovnice	k1gFnSc3
<g/>
:	:	kIx,
toto	tento	k3xDgNnSc1
je	být	k5eAaImIp3nS
křivka	křivka	k1gFnSc1
IS	IS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejným	stejný	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
můžeme	moct	k5eAaImIp1nP
napsat	napsat	k5eAaPmF,k5eAaBmF
rovnici	rovnice	k1gFnSc4
rovnováhy	rovnováha	k1gFnSc2
mezi	mezi	k7c7
preferencí	preference	k1gFnSc7
likvidity	likvidita	k1gFnSc2
a	a	k8xC
peněžní	peněžní	k2eAgFnSc7d1
zásobou	zásoba	k1gFnSc7
jako	jako	k8xC,k8xS
L	L	kA
<g/>
(	(	kIx(
<g/>
Y	Y	kA
<g/>
,	,	kIx,
r	r	kA
<g/>
)	)	kIx)
=	=	kIx~
M	M	kA
<g/>
̂	̂	k?
a	a	k8xC
nakreslit	nakreslit	k5eAaPmF
druhou	druhý	k4xOgFnSc4
křivku	křivka	k1gFnSc4
–	–	k?
LM	LM	kA
křivku	křivka	k1gFnSc4
–	–	k?
spojující	spojující	k2eAgInPc4d1
body	bod	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
ji	on	k3xPp3gFnSc4
splňují	splňovat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rovnovážné	rovnovážný	k2eAgFnPc4d1
hodnoty	hodnota	k1gFnPc4
Ŷ	Ŷ	k?
celkových	celkový	k2eAgInPc2d1
příjmů	příjem	k1gInPc2
a	a	k8xC
r	r	kA
<g/>
̂	̂	k?
úrokových	úrokový	k2eAgFnPc2d1
sazeb	sazba	k1gFnPc2
jsou	být	k5eAaImIp3nP
pak	pak	k6eAd1
dány	dát	k5eAaPmNgFnP
průsečíkem	průsečík	k1gInSc7
dvou	dva	k4xCgFnPc2
křivek	křivka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Pokud	pokud	k8xS
budeme	být	k5eAaImBp1nP
následovat	následovat	k5eAaImF
Keynesův	Keynesův	k2eAgInSc4d1
počáteční	počáteční	k2eAgInSc4d1
účet	účet	k1gInSc4
<g/>
,	,	kIx,
podle	podle	k7c2
něhož	jenž	k3xRgMnSc2
preference	preference	k1gFnSc1
likvidity	likvidita	k1gFnSc2
závisí	záviset	k5eAaImIp3nS
pouze	pouze	k6eAd1
na	na	k7c6
úrokové	úrokový	k2eAgFnSc6d1
sazbě	sazba	k1gFnSc6
r	r	kA
<g/>
,	,	kIx,
pak	pak	k6eAd1
bude	být	k5eAaImBp3nS
LM	LM	kA
křivka	křivka	k1gFnSc1
horizontální	horizontální	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Joan	Joan	k1gMnSc1
Robinson	Robinson	k1gMnSc1
poznamenal	poznamenat	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
<g/>
:	:	kIx,
</s>
<s>
“	“	k?
<g/>
...	...	k?
moderní	moderní	k2eAgFnSc1d1
výuka	výuka	k1gFnSc1
byla	být	k5eAaImAgFnS
zmatena	zmást	k5eAaPmNgFnS
pokusem	pokus	k1gInSc7
J.	J.	kA
R.	R.	kA
Hickse	Hicks	k1gMnSc2
redukovat	redukovat	k5eAaBmF
obecnou	obecný	k2eAgFnSc4d1
teorii	teorie	k1gFnSc4
na	na	k7c6
verzi	verze	k1gFnSc6
statické	statický	k2eAgFnSc2d1
rovnováhy	rovnováha	k1gFnSc2
se	s	k7c7
vzorcem	vzorec	k1gInSc7
IS	IS	kA
<g/>
/	/	kIx~
<g/>
LM	LM	kA
<g/>
.	.	kIx.
<g/>
”	”	k?
</s>
<s>
Uplatnění	uplatnění	k1gNnSc1
ve	v	k7c6
světě	svět	k1gInSc6
</s>
<s>
Po	po	k7c6
druhé	druhý	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
přejaly	přejmout	k5eAaPmAgInP
keynesiánství	keynesiánství	k1gNnSc4
v	v	k7c6
podstatě	podstata	k1gFnSc6
veškeré	veškerý	k3xTgFnPc1
vyspělé	vyspělý	k2eAgFnPc1d1
země	zem	k1gFnPc1
a	a	k8xC
jejich	jejich	k3xOp3gMnPc1
představitelé	představitel	k1gMnPc1
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc7
inspirovali	inspirovat	k5eAaBmAgMnP
při	při	k7c6
sestavování	sestavování	k1gNnSc6
svých	svůj	k3xOyFgInPc2
politických	politický	k2eAgInPc2d1
programů	program	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
v	v	k7c6
70	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc4
utrpěla	utrpět	k5eAaPmAgFnS
tato	tento	k3xDgFnSc1
teorie	teorie	k1gFnSc1
zdrcující	zdrcující	k2eAgFnSc4d1
kritiku	kritika	k1gFnSc4
a	a	k8xC
byla	být	k5eAaImAgFnS
z	z	k7c2
větší	veliký	k2eAgFnSc2d2
části	část	k1gFnSc2
nahrazena	nahradit	k5eAaPmNgFnS
právě	právě	k9
monetarismem	monetarismus	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důvodem	důvod	k1gInSc7
byly	být	k5eAaImAgInP
nové	nový	k2eAgInPc1d1
jevy	jev	k1gInPc1
v	v	k7c6
ekonomii	ekonomie	k1gFnSc6
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
keynesiánci	keynesiánec	k1gMnPc1
nedokázali	dokázat	k5eNaPmAgMnP
vysvětlit	vysvětlit	k5eAaPmF
–	–	k?
například	například	k6eAd1
stagflace	stagflace	k1gFnSc2
či	či	k8xC
slumpflace	slumpflace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Aplikace	aplikace	k1gFnSc1
keynesiánství	keynesiánství	k1gNnSc2
</s>
<s>
Klasičtí	klasický	k2eAgMnPc1d1
ekonomové	ekonom	k1gMnPc1
tvrdí	tvrdit	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
keynesiánství	keynesiánství	k1gNnSc1
je	být	k5eAaImIp3nS
zodpovědné	zodpovědný	k2eAgNnSc1d1
za	za	k7c4
problémy	problém	k1gInPc4
státních	státní	k2eAgInPc2d1
rozpočtů	rozpočet	k1gInPc2
v	v	k7c6
60	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
70	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
(	(	kIx(
<g/>
staly	stát	k5eAaPmAgFnP
se	se	k3xPyFc4
hluboce	hluboko	k6eAd1
deficitními	deficitní	k2eAgFnPc7d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
bylo	být	k5eAaImAgNnS
keynesiánství	keynesiánství	k1gNnSc1
hlavním	hlavní	k2eAgInSc7d1
ekonomickým	ekonomický	k2eAgInSc7d1
proudem	proud	k1gInSc7
v	v	k7c6
západním	západní	k2eAgInSc6d1
světě	svět	k1gInSc6
(	(	kIx(
<g/>
zejména	zejména	k9
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
a	a	k8xC
Velké	velký	k2eAgFnSc6d1
Británii	Británie	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tehdy	tehdy	k6eAd1
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
velké	velký	k2eAgFnSc3d1
inflaci	inflace	k1gFnSc3
a	a	k8xC
problémům	problém	k1gInPc3
ekonomiky	ekonomika	k1gFnSc2
a	a	k8xC
v	v	k7c6
reakci	reakce	k1gFnSc6
na	na	k7c4
to	ten	k3xDgNnSc4
začal	začít	k5eAaPmAgMnS
převládat	převládat	k5eAaImF
vliv	vliv	k1gInSc4
(	(	kIx(
<g/>
neo	neo	k?
<g/>
)	)	kIx)
<g/>
klasických	klasický	k2eAgFnPc2d1
ekonomických	ekonomický	k2eAgFnPc2d1
škol	škola	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
poslední	poslední	k2eAgFnSc7d1
krizí	krize	k1gFnSc7
zkoušely	zkoušet	k5eAaImAgFnP
některé	některý	k3yIgFnPc1
vlády	vláda	k1gFnPc1
aplikovat	aplikovat	k5eAaBmF
některé	některý	k3yIgFnPc4
neokeynesiánské	okeynesiánský	k2eNgFnPc4d1
strategie	strategie	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Neokeynesiánství	Neokeynesiánství	k1gNnSc1
a	a	k8xC
neoliberalismus	neoliberalismus	k1gInSc1
(	(	kIx(
<g/>
klasické	klasický	k2eAgFnSc2d1
ekonomické	ekonomický	k2eAgFnSc2d1
školy	škola	k1gFnSc2
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
dnes	dnes	k6eAd1
dvěma	dva	k4xCgInPc7
hlavními	hlavní	k2eAgInPc7d1
soupeřícími	soupeřící	k2eAgInPc7d1
proudy	proud	k1gInPc7
ekonomie	ekonomie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
What	What	k1gMnSc1
Is	Is	k1gMnSc1
Keynesian	Keynesian	k1gMnSc1
Economics	Economics	k1gInSc4
<g/>
?	?	kIx.
</s>
<s desamb="1">
-	-	kIx~
Back	Back	k1gMnSc1
to	ten	k3xDgNnSc4
Basics	Basics	k1gInSc1
-	-	kIx~
Finance	finance	k1gFnPc1
&	&	k?
Development	Development	k1gInSc1
<g/>
,	,	kIx,
September	September	k1gInSc1
2014	#num#	k4
<g/>
.	.	kIx.
www.imf.org	www.imf.org	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
H.	H.	kA
<g/>
,	,	kIx,
Hunt	hunt	k1gInSc1
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
world	world	k1gMnSc1
transformed	transformed	k1gMnSc1
:	:	kIx,
1945	#num#	k4
to	ten	k3xDgNnSc1
the	the	k?
present	present	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Second	Second	k1gInSc1
edition	edition	k1gInSc4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
ISBN	ISBN	kA
9780199371020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
OCLC	OCLC	kA
907585907	#num#	k4
S.	S.	kA
80	#num#	k4
<g/>
..	..	k?
↑	↑	k?
ARTHUR	ARTHUR	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
O	O	kA
<g/>
'	'	kIx"
<g/>
Sullivan	Sullivan	k1gMnSc1
<g/>
,	,	kIx,
<g/>
.	.	kIx.
</s>
<s desamb="1">
Economics	Economics	k1gInSc1
:	:	kIx,
principles	principles	k1gInSc1
in	in	k?
action	action	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Needham	Needham	k1gInSc1
<g/>
,	,	kIx,
Mass	Mass	k1gInSc1
<g/>
.	.	kIx.
<g/>
:	:	kIx,
Prentice	Prentice	k1gFnSc1
Hall	Hall	k1gInSc1
xvi	xvi	k?
<g/>
,	,	kIx,
592	#num#	k4
pages	pages	k1gInSc1
s.	s.	k?
ISBN	ISBN	kA
0	#num#	k4
<g/>
130630853	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
9780130630858	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
OCLC	OCLC	kA
50237774	#num#	k4
↑	↑	k?
BLINDER	BLINDER	kA
<g/>
,	,	kIx,
Alan	Alan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
fall	fall	k1gMnSc1
and	and	k?
rise	rise	k1gInSc1
of	of	k?
Keynesian	Keynesian	k1gInSc1
economics	economics	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Routledge	Routledge	k1gFnSc1
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9780415157155	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
9780203443965	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
1942	#num#	k4
<g/>
-	-	kIx~
<g/>
,	,	kIx,
Fletcher	Fletchra	k1gFnPc2
<g/>
,	,	kIx,
Gordon	Gordon	k1gMnSc1
A.	A.	kA
<g/>
,	,	kIx,
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Keynesian	Keynesiana	k1gFnPc2
revolution	revolution	k1gInSc4
and	and	k?
its	its	k?
critics	critics	k1gInSc1
:	:	kIx,
issues	issues	k1gInSc1
of	of	k?
theory	theora	k1gFnSc2
and	and	k?
policy	policy	k1gInPc4
for	forum	k1gNnPc2
the	the	k?
monetary	monetara	k1gFnSc2
production	production	k1gInSc4
economy	econom	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
St.	st.	kA
Martin	Martin	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Press	Press	k1gInSc1
<g/>
,	,	kIx,
1987	#num#	k4
<g/>
.	.	kIx.
xxiii	xxiie	k1gFnSc6
<g/>
,	,	kIx,
348	#num#	k4
pages	pages	k1gInSc1
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
312452608	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
9780312452605	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
OCLC	OCLC	kA
13701337	#num#	k4
↑	↑	k?
Wage	Wag	k1gInSc2
moderation	moderation	k1gInSc1
prior	prior	k1gMnSc1
to	ten	k3xDgNnSc1
the	the	k?
crisis	crisis	k1gInSc1
and	and	k?
employment	employment	k1gInSc1
during	during	k1gInSc1
the	the	k?
crisis	crisis	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
OECD	OECD	kA
Economic	Economice	k1gFnPc2
Surveys	Surveysa	k1gFnPc2
<g/>
:	:	kIx,
Germany	German	k1gInPc7
2012	#num#	k4
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1999	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
251	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.178	10.178	k4
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
eco_surveys-deu-	eco_surveys-deu-	k?
<g/>
2012	#num#	k4
<g/>
-graph	-graph	k1gInSc1
<g/>
12	#num#	k4
<g/>
-en	-en	k?
<g/>
.	.	kIx.
↑	↑	k?
BREKKE	BREKKE	kA
<g/>
,	,	kIx,
Arnold	Arnold	k1gMnSc1
<g/>
;	;	kIx,
SAMUELSON	SAMUELSON	kA
<g/>
,	,	kIx,
Paul	Paul	k1gMnSc1
A.	A.	kA
Economics	Economics	k1gInSc1
<g/>
,	,	kIx,
an	an	k?
Introductory	Introductor	k1gInPc1
Analysis	Analysis	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gFnSc1
of	of	k?
Farm	Farm	k1gInSc1
Economics	Economics	k1gInSc1
<g/>
.	.	kIx.
1948	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
30	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
4	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
799	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1071	#num#	k4
<g/>
-	-	kIx~
<g/>
1031	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.230	10.230	k4
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
1232801	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Makroekonomické	makroekonomický	k2eAgInPc1d1
směry	směr	k1gInPc1
</s>
<s>
Keynesiánství	Keynesiánství	k1gNnSc4
•	•	k?
Monetarismus	Monetarismus	k1gInSc1
•	•	k?
Neoklasická	neoklasický	k2eAgFnSc1d1
makroekonomie	makroekonomie	k1gFnSc1
•	•	k?
Neokeynesiánství	Neokeynesiánství	k1gNnSc1
•	•	k?
Postkeynesiánství	Postkeynesiánství	k1gNnPc2
•	•	k?
Ekonomie	ekonomie	k1gFnSc2
strany	strana	k1gFnSc2
nabídky	nabídka	k1gFnSc2
•	•	k?
Nová	nový	k2eAgFnSc1d1
klasická	klasický	k2eAgFnSc1d1
makroekonomie	makroekonomie	k1gFnSc1
•	•	k?
Nová	nový	k2eAgFnSc1d1
keynesiánská	keynesiánský	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
•	•	k?
Teorie	teorie	k1gFnSc1
reálného	reálný	k2eAgInSc2d1
hospodářského	hospodářský	k2eAgInSc2d1
cyklu	cyklus	k1gInSc2
•	•	k?
Rakouská	rakouský	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
hospodářského	hospodářský	k2eAgInSc2d1
cyklu	cyklus	k1gInSc2
</s>
<s>
Ekonomické	ekonomický	k2eAgInPc1d1
směry	směr	k1gInPc1
Předmoderní	Předmoderní	k2eAgInPc1d1
</s>
<s>
Starověká	starověký	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
•	•	k?
Středověká	středověký	k2eAgFnSc1d1
islámská	islámský	k2eAgFnSc1d1
•	•	k?
Scholastika	scholastika	k1gFnSc1
Moderní	moderní	k2eAgFnSc1d1
éra	éra	k1gFnSc1
</s>
<s>
Raně	raně	k6eAd1
moderní	moderní	k2eAgFnSc1d1
</s>
<s>
Salamanská	Salamanský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
•	•	k?
Merkantilismus	merkantilismus	k1gInSc1
•	•	k?
Fyziokratismus	fyziokratismus	k1gInSc1
•	•	k?
Kameralismus	Kameralismus	k1gInSc1
Pozdně	pozdně	k6eAd1
moderní	moderní	k2eAgInSc1d1
</s>
<s>
Americká	americký	k2eAgFnSc1d1
<g/>
(	(	kIx(
<g/>
národní	národní	k2eAgFnSc1d1
<g/>
)	)	kIx)
•	•	k?
Anarchistická	anarchistický	k2eAgFnSc1d1
<g/>
(	(	kIx(
<g/>
Mutualismus	mutualismus	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Rakouská	rakouský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
•	•	k?
Birminghenská	Birminghenský	k2eAgFnSc1d1
•	•	k?
Klasická	klasický	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
•	•	k?
Ricardova	Ricardův	k2eAgFnSc1d1
škola	škola	k1gFnSc1
•	•	k?
Anglická	anglický	k2eAgFnSc1d1
historická	historický	k2eAgFnSc1d1
•	•	k?
Francouzská	francouzský	k2eAgFnSc1d1
liberální	liberální	k2eAgFnSc1d1
•	•	k?
Georgismus	Georgismus	k1gInSc1
•	•	k?
Německá	německý	k2eAgFnSc1d1
historická	historický	k2eAgFnSc1d1
•	•	k?
Malthusiánství	malthusiánství	k1gNnSc6
•	•	k?
Marginalismus	Marginalismus	k1gInSc1
•	•	k?
Marxistická	marxistický	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
•	•	k?
Neoklasická	neoklasický	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
(	(	kIx(
<g/>
Lausannská	lausannský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
<g/>
,	,	kIx,
Cambridgeská	cambridgeský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Křesťanský	křesťanský	k2eAgInSc4d1
socialismus	socialismus	k1gInSc4
Současná	současný	k2eAgFnSc1d1
20	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
21	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
Behavioární	Behavioární	k2eAgFnSc1d1
•	•	k?
Budhistická	Budhistický	k2eAgFnSc1d1
•	•	k?
Schopnostní	Schopnostní	k2eAgInSc1d1
přístup	přístup	k1gInSc1
•	•	k?
Carnegská	Carnegská	k1gFnSc1
•	•	k?
Chartalism	Chartalism	k1gInSc1
(	(	kIx(
Moderní	moderní	k2eAgFnSc1d1
monetární	monetární	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Chicagská	chicagský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
•	•	k?
Konstituční	konstituční	k2eAgFnSc1d1
•	•	k?
Rovnovážná	rovnovážný	k2eAgFnSc1d1
•	•	k?
Ekologická	ekologický	k2eAgFnSc1d1
•	•	k?
Enviromentální	Enviromentální	k2eAgFnSc1d1
•	•	k?
Evoluční	evoluční	k2eAgFnSc1d1
•	•	k?
Feministická	feministický	k2eAgFnSc1d1
•	•	k?
Freighburská	Freighburský	k2eAgFnSc1d1
•	•	k?
Instituciální	Instituciální	k2eAgNnSc1d1
•	•	k?
Keynesiánství	Keynesiánství	k1gNnSc1
(	(	kIx(
<g/>
Neo	Neo	k1gFnSc1
<g/>
,	,	kIx,
Neoklasická	neoklasický	k2eAgFnSc1d1
syntéza	syntéza	k1gFnSc1
<g/>
,	,	kIx,
Nová	nový	k2eAgFnSc1d1
<g/>
,	,	kIx,
Postkeynesiánství	Postkeynesiánství	k1gNnSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
Cirkulační	cirkulační	k2eAgInSc1d1
<g/>
)	)	kIx)
•	•	k?
Monetarismus	Monetarismus	k1gInSc1
(	(	kIx(
<g/>
Tržní	tržní	k2eAgInSc1d1
<g/>
)	)	kIx)
•	•	k?
Neo-Malthusiánství	Neo-Malthusiánství	k1gNnSc6
•	•	k?
Neo-marxistická	Neo-marxistický	k2eAgFnSc1d1
•	•	k?
Neo-Ricardiánství	Neo-Ricardiánství	k1gNnSc6
•	•	k?
Neoliberalismus	neoliberalismus	k1gInSc1
•	•	k?
Nová	Nová	k1gFnSc1
klasická	klasický	k2eAgFnSc1d1
<g/>
,	,	kIx,
(	(	kIx(
<g/>
Racionální	racionální	k2eAgNnPc4d1
očekávání	očekávání	k1gNnPc4
<g/>
,	,	kIx,
Teorie	teorie	k1gFnSc1
skutečného	skutečný	k2eAgInSc2d1
hospodářského	hospodářský	k2eAgInSc2d1
cyklu	cyklus	k1gInSc2
<g/>
)	)	kIx)
•	•	k?
Nová	nový	k2eAgFnSc1d1
institucionální	institucionální	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
•	•	k?
Nová	nový	k2eAgFnSc1d1
neoklasická	neoklasický	k2eAgFnSc1d1
syntéza	syntéza	k1gFnSc1
•	•	k?
Organizační	organizační	k2eAgFnSc1d1
•	•	k?
Teorie	teorie	k1gFnSc1
veřejné	veřejný	k2eAgFnSc2d1
volby	volba	k1gFnSc2
•	•	k?
Regulační	regulační	k2eAgNnSc1d1
•	•	k?
Slano	slano	k1gNnSc1
<g/>
/	/	kIx~
<g/>
Sladko	sladko	k6eAd1
vodní	vodní	k2eAgFnSc1d1
•	•	k?
Stockholmská	stockholmský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
•	•	k?
Struktuální	Struktuální	k2eAgFnSc1d1
•	•	k?
Ekonomie	ekonomie	k1gFnSc1
strany	strana	k1gFnSc2
nabídky	nabídka	k1gFnSc2
•	•	k?
Thermoekonomie	Thermoekonomie	k1gFnSc2
•	•	k?
Virginiánská	Virginiánský	k2eAgFnSc1d1
•	•	k?
Social	Social	k1gMnSc1
Credit	Credit	k1gMnSc1
</s>
<s>
Ekonomie	ekonomie	k1gFnSc1
•	•	k?
Heterodoxní	heterodoxní	k2eAgFnPc4d1
ekonomie	ekonomie	k1gFnPc4
•	•	k?
Historie	historie	k1gFnSc1
ekonomických	ekonomický	k2eAgInPc2d1
směrů	směr	k1gInPc2
</s>
<s>
Makroekonomie	makroekonomie	k1gFnPc1
Hlavní	hlavní	k2eAgFnPc1d1
koncepty	koncept	k1gInPc4
</s>
<s>
Agregátní	agregátní	k2eAgFnSc1d1
poptávka	poptávka	k1gFnSc1
•	•	k?
Agregátní	agregátní	k2eAgFnSc1d1
nabídka	nabídka	k1gFnSc1
•	•	k?
Hospodářský	hospodářský	k2eAgInSc1d1
cyklus	cyklus	k1gInSc1
•	•	k?
Deflace	deflace	k1gFnSc2
•	•	k?
Poptávkový	poptávkový	k2eAgInSc1d1
šok	šok	k1gInSc1
•	•	k?
Nabídkový	nabídkový	k2eAgInSc1d1
šok	šok	k1gInSc1
<g/>
•	•	k?
Dezinflace	Dezinflace	k1gFnSc2
•	•	k?
Efektivní	efektivní	k2eAgFnSc1d1
poptávka	poptávka	k1gFnSc1
•	•	k?
Očekávání	očekávání	k1gNnSc1
<g/>
(	(	kIx(
<g/>
Adaptivní	adaptivní	k2eAgFnPc1d1
•	•	k?
Racionální	racionální	k2eAgFnPc1d1
<g/>
)	)	kIx)
•	•	k?
Finanční	finanční	k2eAgFnSc2d1
krize	krize	k1gFnSc2
•	•	k?
Hospodářský	hospodářský	k2eAgInSc1d1
růst	růst	k1gInSc1
•	•	k?
Inflace	inflace	k1gFnSc1
<g/>
(	(	kIx(
<g/>
Tahem	tah	k1gInSc7
poptávky	poptávka	k1gFnSc2
•	•	k?
Nákladová	nákladový	k2eAgFnSc1d1
)	)	kIx)
•	•	k?
Úroková	úrokový	k2eAgFnSc1d1
sazba	sazba	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Investice	investice	k1gFnSc1
•	•	k?
Past	pasta	k1gFnPc2
na	na	k7c4
likviditu	likvidita	k1gFnSc4
•	•	k?
Národní	národní	k2eAgInSc1d1
důchod	důchod	k1gInSc1
<g/>
(	(	kIx(
<g/>
HDP	HDP	kA
•	•	k?
HNP	HNP	kA
•	•	k?
ČND	ČND	kA
<g/>
)	)	kIx)
•	•	k?
Microfoundations	Microfoundations	k1gInSc4
•	•	k?
Peníze	peníz	k1gInPc4
<g/>
(	(	kIx(
<g/>
Endogenní	endogenní	k2eAgInSc4d1
<g/>
)	)	kIx)
•	•	k?
Tvorba	tvorba	k1gFnSc1
peněz	peníze	k1gInPc2
•	•	k?
Poptávka	poptávka	k1gFnSc1
po	po	k7c6
penězích	peníze	k1gInPc6
•	•	k?
Preference	preference	k1gFnPc1
likvidity	likvidita	k1gFnSc2
•	•	k?
Peněžní	peněžní	k2eAgFnSc1d1
zásoba	zásoba	k1gFnSc1
•	•	k?
Národní	národní	k2eAgInPc4d1
účty	účet	k1gInPc4
<g/>
(	(	kIx(
<g/>
Systém	systém	k1gInSc1
národních	národní	k2eAgInPc2d1
účtů	účet	k1gInPc2
<g/>
)	)	kIx)
•	•	k?
Nominal	Nominal	k1gMnSc1
rigidity	rigidita	k1gFnSc2
•	•	k?
Cenová	cenový	k2eAgFnSc1d1
hladina	hladina	k1gFnSc1
•	•	k?
Recese	recese	k1gFnSc1
•	•	k?
Shrinkflation	Shrinkflation	k1gInSc1
•	•	k?
Stagflace	Stagflace	k1gFnSc2
•	•	k?
Úspory	úspora	k1gFnSc2
•	•	k?
Nezaměstnanost	nezaměstnanost	k1gFnSc1
Politiky	politika	k1gFnSc2
</s>
<s>
Fiskální	fiskální	k2eAgFnSc1d1
•	•	k?
Monetární	monetární	k2eAgFnSc1d1
•	•	k?
Obchodní	obchodní	k2eAgFnSc1d1
•	•	k?
Centrální	centrální	k2eAgFnSc1d1
banka	banka	k1gFnSc1
Modely	model	k1gInPc1
</s>
<s>
IS-LM	IS-LM	k?
•	•	k?
AD	ad	k7c4
<g/>
–	–	k?
<g/>
AS	as	k1gNnSc4
•	•	k?
Keynesiánský	keynesiánský	k2eAgInSc4d1
kříž	kříž	k1gInSc4
•	•	k?
Multiplikátor	multiplikátor	k1gInSc1
•	•	k?
Akcelerátorový	Akcelerátorový	k2eAgInSc1d1
efekt	efekt	k1gInSc1
•	•	k?
Phillipsova	Phillipsův	k2eAgFnSc1d1
křivka	křivka	k1gFnSc1
•	•	k?
Arrow	Arrow	k1gFnSc2
<g/>
–	–	k?
<g/>
Debreu	Debreus	k1gInSc2
•	•	k?
Harrod	Harroda	k1gFnPc2
<g/>
–	–	k?
<g/>
Domar	Domar	k1gMnSc1
•	•	k?
Solow	Solow	k1gMnSc1
<g/>
–	–	k?
<g/>
Swan	Swan	k1gMnSc1
•	•	k?
Ramsey	Ramsea	k1gFnSc2
<g/>
–	–	k?
<g/>
Cass	Cass	k1gInSc1
<g/>
–	–	k?
<g/>
Koopmans	Koopmans	k1gInSc1
•	•	k?
Model	modla	k1gFnPc2
překrývajících	překrývající	k2eAgFnPc2d1
se	se	k3xPyFc4
generací	generace	k1gFnPc2
•	•	k?
Obecná	obecný	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
rovnováhy	rovnováha	k1gFnSc2
•	•	k?
Teorie	teorie	k1gFnSc1
endogenního	endogenní	k2eAgInSc2d1
růstu	růst	k1gInSc2
•	•	k?
Teorie	teorie	k1gFnSc1
shody	shoda	k1gFnSc2
•	•	k?
Mundell	Mundell	k1gInSc1
<g/>
–	–	k?
<g/>
Fleming	Fleming	k1gInSc1
•	•	k?
Model	model	k1gInSc1
překročení	překročení	k1gNnSc2
•	•	k?
NAIRU	NAIRU	kA
Související	související	k2eAgFnSc1d1
</s>
<s>
Ekonometrie	Ekonometrie	k1gFnSc1
•	•	k?
Ekonomická	ekonomický	k2eAgFnSc1d1
statistika	statistika	k1gFnSc1
•	•	k?
Monetární	monetární	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
•	•	k?
Rozvojová	rozvojový	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
•	•	k?
Mezinárodní	mezinárodní	k2eAgFnSc2d1
ekonomiky	ekonomika	k1gFnSc2
Školy	škola	k1gFnSc2
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1
proud	proud	k1gInSc1
</s>
<s>
Keynesiánství	Keynesiánství	k1gNnSc1
(	(	kIx(
<g/>
Neo-Nová	Neo-Nová	k1gFnSc1
<g/>
)	)	kIx)
<g/>
•	•	k?
Monetarismus	Monetarismus	k1gInSc1
•	•	k?
Nová	nový	k2eAgFnSc1d1
klasická	klasický	k2eAgFnSc1d1
makroekonomie	makroekonomie	k1gFnSc1
(	(	kIx(
<g/>
Teorie	teorie	k1gFnSc1
reálného	reálný	k2eAgInSc2d1
obchdního	obchdní	k2eAgInSc2d1
cykklu	cykkl	k1gInSc2
<g/>
)	)	kIx)
•	•	k?
Stockholmská	stockholmský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
•	•	k?
Ekonomie	ekonomie	k1gFnSc2
strany	strana	k1gFnSc2
nabídky	nabídka	k1gFnSc2
•	•	k?
Nová	nový	k2eAgFnSc1d1
neoklasická	neoklasický	k2eAgFnSc1d1
syntéza	syntéza	k1gFnSc1
•	•	k?
Slano	slano	k1gNnSc1
<g/>
/	/	kIx~
<g/>
Sladko	sladko	k6eAd1
vodní	vodní	k2eAgFnSc2d1
ekonomie	ekonomie	k1gFnSc2
Heterodox	Heterodox	k1gInSc1
</s>
<s>
Rakouská	rakouský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
•	•	k?
Chartalism	Chartalism	k1gInSc1
(	(	kIx(
<g/>
Moderní	moderní	k2eAgFnSc1d1
monetární	monetární	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Rovnovážná	rovnovážný	k2eAgFnSc1d1
makroekonomie	makroekonomie	k1gFnSc1
•	•	k?
Marxistická	marxistický	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
•	•	k?
Postkeynesiánství	Postkeynesiánství	k1gNnSc1
<g/>
(	(	kIx(
<g/>
Cirkulační	cirkulační	k2eAgFnSc1d1
<g/>
)	)	kIx)
•	•	k?
Tržní	tržní	k2eAgInSc1d1
monetarismus	monetarismus	k1gInSc1
</s>
<s>
Významní	významný	k2eAgMnPc1d1
makroekonomové	makroekonom	k1gMnPc1
</s>
<s>
François	François	k1gFnSc1
Quesnay	Quesnaa	k1gFnSc2
•	•	k?
Adam	Adam	k1gMnSc1
Smith	Smith	k1gMnSc1
•	•	k?
Thomas	Thomas	k1gMnSc1
Robert	Robert	k1gMnSc1
Malthus	Malthus	k1gMnSc1
•	•	k?
Karl	Karl	k1gMnSc1
Marx	Marx	k1gMnSc1
•	•	k?
Léon	Léon	k1gMnSc1
Walras	Walras	k1gMnSc1
•	•	k?
Georg	Georg	k1gMnSc1
Friedrich	Friedrich	k1gMnSc1
Knapp	Knapp	k1gMnSc1
•	•	k?
Knut	knuta	k1gFnPc2
Wicksell	Wicksell	k1gMnSc1
•	•	k?
Irving	Irving	k1gInSc1
Fisher	Fishra	k1gFnPc2
•	•	k?
Wesley	Weslea	k1gFnSc2
Clair	Clair	k1gMnSc1
Mitchell	Mitchell	k1gMnSc1
•	•	k?
John	John	k1gMnSc1
Maynard	Maynard	k1gMnSc1
Keynes	Keynes	k1gMnSc1
•	•	k?
Alvin	Alvin	k1gMnSc1
Hansen	Hansen	k2eAgMnSc1d1
•	•	k?
Michał	Michał	k1gMnSc1
Kalecki	Kaleck	k1gFnSc2
•	•	k?
Gunnar	Gunnar	k1gMnSc1
Myrdal	Myrdal	k1gMnSc1
•	•	k?
Simon	Simon	k1gMnSc1
Kuznets	Kuznetsa	k1gFnPc2
•	•	k?
Joan	Joan	k1gMnSc1
Robinson	Robinson	k1gMnSc1
•	•	k?
Friedrich	Friedrich	k1gMnSc1
Hayek	Hayek	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
John	John	k1gMnSc1
Hicks	Hicksa	k1gFnPc2
•	•	k?
Richard	Richard	k1gMnSc1
Stone	ston	k1gInSc5
•	•	k?
Hyman	Hyman	k1gMnSc1
Minsky	minsky	k6eAd1
•	•	k?
Milton	Milton	k1gInSc1
Friedman	Friedman	k1gMnSc1
•	•	k?
Paul	Paul	k1gMnSc1
Samuelson	Samuelson	k1gMnSc1
•	•	k?
Lawrence	Lawrence	k1gFnSc2
Klein	Klein	k1gMnSc1
•	•	k?
Edmund	Edmund	k1gMnSc1
Phelps	Phelpsa	k1gFnPc2
•	•	k?
Robert	Robert	k1gMnSc1
Lucas	Lucas	k1gMnSc1
Jr	Jr	k1gMnSc1
<g/>
.	.	kIx.
•	•	k?
Edward	Edward	k1gMnSc1
C.	C.	kA
Prescott	Prescott	k1gMnSc1
•	•	k?
Peter	Peter	k1gMnSc1
Diamond	Diamond	k1gMnSc1
•	•	k?
William	William	k1gInSc1
Nordhaus	Nordhaus	k1gMnSc1
•	•	k?
Joseph	Joseph	k1gMnSc1
Stiglitz	Stiglitz	k1gMnSc1
•	•	k?
Thomas	Thomas	k1gMnSc1
J.	J.	kA
Sargent	Sargent	k1gMnSc1
•	•	k?
Paul	Paul	k1gMnSc1
Krugman	Krugman	k1gMnSc1
•	•	k?
Gregory	Gregor	k1gMnPc7
Mankiw	Mankiw	k1gFnPc7
K	k	k7c3
vidění	vidění	k1gNnSc3
</s>
<s>
Ekonomie	ekonomie	k1gFnSc1
•	•	k?
Ekonomika	ekonomik	k1gMnSc2
•	•	k?
Makroekonomický	makroekonomický	k2eAgInSc4d1
model	model	k1gInSc4
•	•	k?
Seznam	seznam	k1gInSc1
významných	významný	k2eAgFnPc2d1
ekonomických	ekonomický	k2eAgFnPc2d1
publikací	publikace	k1gFnPc2
•	•	k?
Mikroekonomie	mikroekonomie	k1gFnSc2
•	•	k?
Politická	politický	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
•	•	k?
Matematická	matematický	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Ekonomie	ekonomie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4030426-7	4030426-7	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
1531	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85072125	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85072125	#num#	k4
</s>
