<s>
Tato	tento	k3xDgFnSc1	tento
nemoc	nemoc	k1gFnSc1	nemoc
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
popsána	popsat	k5eAaPmNgFnS	popsat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1882	[number]	k4	1882
<g/>
,	,	kIx,	,
postihuje	postihovat	k5eAaImIp3nS	postihovat
kůži	kůže	k1gFnSc4	kůže
a	a	k8xC	a
podkoží	podkoží	k1gNnSc4	podkoží
genitálií	genitálie	k1gFnPc2	genitálie
a	a	k8xC	a
přilehlých	přilehlý	k2eAgFnPc2d1	přilehlá
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
