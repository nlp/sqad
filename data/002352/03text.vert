<s>
Granuloma	Granuloma	k1gFnSc1	Granuloma
inguinale	inguinale	k6eAd1	inguinale
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
též	též	k9	též
jako	jako	k8xC	jako
donovanóza	donovanóza	k1gFnSc1	donovanóza
<g/>
,	,	kIx,	,
granuloma	granuloma	k1gFnSc1	granuloma
venereum	venereum	k1gNnSc1	venereum
či	či	k8xC	či
inguinální	inguinální	k2eAgInSc1d1	inguinální
granulom	granulom	k1gInSc1	granulom
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
sexuálně	sexuálně	k6eAd1	sexuálně
přenosná	přenosný	k2eAgFnSc1d1	přenosná
nemoc	nemoc	k1gFnSc1	nemoc
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gMnSc7	jejíž
původcem	původce	k1gMnSc7	původce
je	být	k5eAaImIp3nS	být
bakterie	bakterie	k1gFnSc1	bakterie
Calymmatobacterium	Calymmatobacterium	k1gNnSc1	Calymmatobacterium
granulomatis	granulomatis	k1gFnSc2	granulomatis
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
nemoc	nemoc	k1gFnSc1	nemoc
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
popsána	popsat	k5eAaPmNgFnS	popsat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1882	[number]	k4	1882
<g/>
,	,	kIx,	,
postihuje	postihovat	k5eAaImIp3nS	postihovat
kůži	kůže	k1gFnSc4	kůže
a	a	k8xC	a
podkoží	podkoží	k1gNnSc4	podkoží
genitálií	genitálie	k1gFnPc2	genitálie
a	a	k8xC	a
přilehlých	přilehlý	k2eAgFnPc2d1	přilehlá
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
vzácnou	vzácný	k2eAgFnSc4d1	vzácná
pohlavní	pohlavní	k2eAgFnSc4d1	pohlavní
nemoc	nemoc	k1gFnSc4	nemoc
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
částech	část	k1gFnPc6	část
světa	svět	k1gInSc2	svět
endemická	endemický	k2eAgFnSc1d1	endemická
<g/>
,	,	kIx,	,
vyskytující	vyskytující	k2eAgFnSc1d1	vyskytující
se	se	k3xPyFc4	se
především	především	k9	především
v	v	k7c6	v
tropech	trop	k1gInPc6	trop
a	a	k8xC	a
subtropech	subtropy	k1gInPc6	subtropy
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
Karibiku	Karibik	k1gInSc2	Karibik
<g/>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc2	Indie
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgFnSc2d1	Nové
Guineje	Guinea	k1gFnSc2	Guinea
a	a	k8xC	a
dalších	další	k2eAgFnPc6d1	další
převážně	převážně	k6eAd1	převážně
rozvojových	rozvojový	k2eAgFnPc6d1	rozvojová
zemích	zem	k1gFnPc6	zem
(	(	kIx(	(
<g/>
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
zemích	zem	k1gFnPc6	zem
je	být	k5eAaImIp3nS	být
výskyt	výskyt	k1gInSc1	výskyt
způsobem	způsob	k1gInSc7	způsob
většinou	většinou	k6eAd1	většinou
díky	díky	k7c3	díky
migraci	migrace	k1gFnSc3	migrace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Inkubační	inkubační	k2eAgFnSc1d1	inkubační
doba	doba	k1gFnSc1	doba
nemoci	nemoc	k1gFnSc2	nemoc
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
1	[number]	k4	1
do	do	k7c2	do
12	[number]	k4	12
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
uplynutí	uplynutí	k1gNnSc6	uplynutí
inkubační	inkubační	k2eAgFnSc2d1	inkubační
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
začne	začít	k5eAaPmIp3nS	začít
tvořit	tvořit	k5eAaImF	tvořit
vřed	vřed	k1gInSc1	vřed
<g/>
,	,	kIx,	,
častěji	často	k6eAd2	často
větší	veliký	k2eAgNnSc4d2	veliký
množství	množství	k1gNnSc4	množství
vředů	vřed	k1gInPc2	vřed
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
se	se	k3xPyFc4	se
posléze	posléze	k6eAd1	posléze
spojují	spojovat	k5eAaImIp3nP	spojovat
<g/>
,	,	kIx,	,
praskají	praskat	k5eAaImIp3nP	praskat
<g/>
,	,	kIx,	,
krvácejí	krvácet	k5eAaImIp3nP	krvácet
a	a	k8xC	a
značně	značně	k6eAd1	značně
destruují	destruovat	k5eAaImIp3nP	destruovat
nejenom	nejenom	k6eAd1	nejenom
tkáň	tkáň	k1gFnSc4	tkáň
pohlavního	pohlavní	k2eAgNnSc2d1	pohlavní
ústrojí	ústrojí	k1gNnSc2	ústrojí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
tkáň	tkáň	k1gFnSc4	tkáň
řitního	řitní	k2eAgInSc2d1	řitní
otvoru	otvor	k1gInSc2	otvor
a	a	k8xC	a
třísel	tříslo	k1gNnPc2	tříslo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
stejnou	stejný	k2eAgFnSc4d1	stejná
dobu	doba	k1gFnSc4	doba
probíhá	probíhat	k5eAaImIp3nS	probíhat
hojení	hojení	k1gNnSc1	hojení
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
vazivových	vazivový	k2eAgFnPc2d1	vazivová
jizev	jizva	k1gFnPc2	jizva
<g/>
.	.	kIx.	.
</s>
<s>
Neléčená	léčený	k2eNgFnSc1d1	neléčená
nemoc	nemoc	k1gFnSc1	nemoc
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
vážné	vážný	k2eAgInPc4d1	vážný
následky	následek	k1gInPc4	následek
<g/>
.	.	kIx.	.
</s>
<s>
Terapie	terapie	k1gFnSc1	terapie
probíhá	probíhat	k5eAaImIp3nS	probíhat
podáváním	podávání	k1gNnSc7	podávání
antibiotik	antibiotikum	k1gNnPc2	antibiotikum
či	či	k8xC	či
sulfonamidů	sulfonamid	k1gInPc2	sulfonamid
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
však	však	k9	však
nezbytný	zbytný	k2eNgInSc1d1	zbytný
chirurgický	chirurgický	k2eAgInSc1d1	chirurgický
zákrok	zákrok	k1gInSc1	zákrok
<g/>
.	.	kIx.	.
</s>
