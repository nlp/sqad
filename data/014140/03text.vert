<s>
Smrček	Smrček	k1gMnSc1
(	(	kIx(
<g/>
Býšovec	Býšovec	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Smrček	smrček	k1gInSc1
Kaple	kaple	k1gFnSc2
svatého	svatý	k2eAgMnSc2d1
Antonína	Antonín	k1gMnSc2
PaduánskéhoLokalita	PaduánskéhoLokalita	k1gFnSc1
Charakter	charakter	k1gInSc1
</s>
<s>
malá	malý	k2eAgFnSc1d1
vesnice	vesnice	k1gFnSc1
Obec	obec	k1gFnSc1
</s>
<s>
Býšovec	Býšovec	k1gInSc1
Okres	okres	k1gInSc1
</s>
<s>
Žďár	Žďár	k1gInSc1
nad	nad	k7c7
Sázavou	Sázava	k1gFnSc7
Kraj	kraj	k7c2
</s>
<s>
Kraj	kraj	k1gInSc1
Vysočina	vysočina	k1gFnSc1
Historická	historický	k2eAgFnSc1d1
země	země	k1gFnSc1
</s>
<s>
Morava	Morava	k1gFnSc1
Zeměpisné	zeměpisný	k2eAgFnSc2d1
souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
27	#num#	k4
<g/>
′	′	k?
<g/>
32	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
16	#num#	k4
<g/>
°	°	k?
<g/>
18	#num#	k4
<g/>
′	′	k?
<g/>
11	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Základní	základní	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Počet	počet	k1gInSc4
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
55	#num#	k4
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Katastrální	katastrální	k2eAgNnPc1d1
území	území	k1gNnPc1
</s>
<s>
Smrček	smrček	k1gInSc1
(	(	kIx(
<g/>
2,68	2,68	k4
km²	km²	k?
<g/>
)	)	kIx)
PSČ	PSČ	kA
</s>
<s>
592	#num#	k4
62	#num#	k4
Počet	počet	k1gInSc1
domů	domů	k6eAd1
</s>
<s>
34	#num#	k4
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Smrček	Smrček	k1gMnSc1
</s>
<s>
Další	další	k2eAgInPc1d1
údaje	údaj	k1gInPc1
Kód	kód	k1gInSc4
části	část	k1gFnPc1
obce	obec	k1gFnSc2
</s>
<s>
17221	#num#	k4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Zdroje	zdroj	k1gInSc2
k	k	k7c3
infoboxu	infobox	k1gInSc3
a	a	k8xC
českým	český	k2eAgNnPc3d1
sídlům	sídlo	k1gNnPc3
<g/>
.	.	kIx.
<g/>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Smrček	smrček	k1gInSc1
(	(	kIx(
<g/>
německy	německy	k6eAd1
Smirtschek	Smirtschek	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
malá	malý	k2eAgFnSc1d1
vesnice	vesnice	k1gFnSc1
<g/>
,	,	kIx,
část	část	k1gFnSc1
obce	obec	k1gFnSc2
Býšovec	Býšovec	k1gInSc1
v	v	k7c6
okrese	okres	k1gInSc6
Žďár	Žďár	k1gInSc1
nad	nad	k7c7
Sázavou	Sázava	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
asi	asi	k9
2	#num#	k4
km	km	kA
na	na	k7c4
jihovýchod	jihovýchod	k1gInSc4
od	od	k7c2
Býšovce	Býšovec	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
zde	zde	k6eAd1
bylo	být	k5eAaImAgNnS
evidováno	evidovat	k5eAaImNgNnS
42	#num#	k4
adres	adresa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2001	#num#	k4
zde	zde	k6eAd1
trvale	trvale	k6eAd1
žilo	žít	k5eAaImAgNnS
51	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Smrček	smrček	k1gInSc1
je	být	k5eAaImIp3nS
také	také	k9
název	název	k1gInSc1
katastrálního	katastrální	k2eAgNnSc2d1
území	území	k1gNnSc2
o	o	k7c6
rozloze	rozloha	k1gFnSc6
2,68	2,68	k4
km	km	kA
<g/>
2	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc1
písemná	písemný	k2eAgFnSc1d1
zmínka	zmínka	k1gFnSc1
o	o	k7c6
obci	obec	k1gFnSc6
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
roku	rok	k1gInSc2
1406	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Fotogalerie	Fotogalerie	k1gFnSc1
</s>
<s>
Kaple	kaple	k1gFnSc1
svatého	svatý	k2eAgMnSc2d1
Antonína	Antonín	k1gMnSc2
Paduánského	paduánský	k2eAgInSc2d1
</s>
<s>
Kříže	kříž	k1gInPc1
před	před	k7c7
kaplí	kaple	k1gFnSc7
</s>
<s>
Památník	památník	k1gInSc1
obětem	oběť	k1gFnPc3
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
</s>
<s>
Památný	památný	k2eAgInSc4d1
smrk	smrk	k1gInSc4
vysazený	vysazený	k2eAgInSc4d1
k	k	k7c3
výročí	výročí	k1gNnSc3
660	#num#	k4
let	léto	k1gNnPc2
od	od	k7c2
první	první	k4xOgFnSc2
zmínky	zmínka	k1gFnSc2
o	o	k7c6
obci	obec	k1gFnSc6
</s>
<s>
Hasičská	hasičský	k2eAgFnSc1d1
zbrojnice	zbrojnice	k1gFnSc1
</s>
<s>
Hospodářské	hospodářský	k2eAgNnSc1d1
stavení	stavení	k1gNnSc1
</s>
<s>
Rybníček	rybníček	k1gInSc1
pod	pod	k7c7
vsí	ves	k1gFnSc7
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Historický	historický	k2eAgInSc1d1
lexikon	lexikon	k1gInSc1
obcí	obec	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
–	–	k?
1869	#num#	k4
<g/>
–	–	k?
<g/>
2011	#num#	k4
<g/>
.	.	kIx.
21	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Ministerstvo	ministerstvo	k1gNnSc1
vnitra	vnitro	k1gNnSc2
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Adresy	adresa	k1gFnSc2
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2009-10-10	2009-10-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sčítání	sčítání	k1gNnSc1
lidu	lid	k1gInSc2
<g/>
,	,	kIx,
domů	dům	k1gInPc2
a	a	k8xC
bytů	byt	k1gInPc2
2001	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2003-10-16	2003-10-16	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2003	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Územně	územně	k6eAd1
identifikační	identifikační	k2eAgInSc4d1
registr	registr	k1gInSc4
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Územně	územně	k6eAd1
identifikační	identifikační	k2eAgInSc1d1
registr	registr	k1gInSc1
ČR	ČR	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1999-01-01	1999-01-01	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historický	historický	k2eAgInSc1d1
lexikon	lexikon	k1gInSc1
obcí	obec	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
1869	#num#	k4
<g/>
–	–	k?
<g/>
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příprava	příprava	k1gFnSc1
vydání	vydání	k1gNnSc2
Balcar	Balcar	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
;	;	kIx,
Havel	Havel	k1gMnSc1
<g/>
,	,	kIx,
Radek	Radek	k1gMnSc1
<g/>
;	;	kIx,
Křídlo	křídlo	k1gNnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
;	;	kIx,
Pavlíková	Pavlíková	k1gFnSc1
<g/>
,	,	kIx,
Marie	Marie	k1gFnSc1
<g/>
;	;	kIx,
Růžková	Růžková	k1gFnSc1
<g/>
,	,	kIx,
Jiřina	Jiřina	k1gFnSc1
<g/>
;	;	kIx,
Šanda	Šanda	k1gMnSc1
<g/>
,	,	kIx,
Robert	Robert	k1gMnSc1
<g/>
;	;	kIx,
Škrabal	Škrabal	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svazek	svazek	k1gInSc1
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
svazky	svazek	k1gInPc4
(	(	kIx(
<g/>
760	#num#	k4
s.	s.	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
250	#num#	k4
<g/>
-	-	kIx~
<g/>
1311	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
596	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Smrček	smrčka	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Smrček	smrčka	k1gFnPc2
v	v	k7c6
Ottově	Ottův	k2eAgInSc6d1
slovníku	slovník	k1gInSc6
naučném	naučný	k2eAgInSc6d1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Katastrální	katastrální	k2eAgFnSc1d1
mapa	mapa	k1gFnSc1
katastru	katastr	k1gInSc2
Smrček	smrčka	k1gFnPc2
na	na	k7c6
webu	web	k1gInSc6
ČÚZK	ČÚZK	kA
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Části	část	k1gFnSc2
obce	obec	k1gFnSc2
Býšovec	Býšovec	k1gInSc1
Části	část	k1gFnSc2
obce	obec	k1gFnSc2
</s>
<s>
BýšovecSmrček	BýšovecSmrček	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
|	|	kIx~
Geografie	geografie	k1gFnSc1
</s>
