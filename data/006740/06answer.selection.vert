<s>
Za	za	k7c4	za
první	první	k4xOgFnSc4	první
fotografii	fotografia	k1gFnSc4	fotografia
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
snímek	snímek	k1gInSc1	snímek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zhotovil	zhotovit	k5eAaPmAgInS	zhotovit
roku	rok	k1gInSc2	rok
1826	[number]	k4	1826
francouzský	francouzský	k2eAgMnSc1d1	francouzský
vynálezce	vynálezce	k1gMnSc1	vynálezce
Joseph	Joseph	k1gMnSc1	Joseph
Nicéphore	Nicéphor	k1gInSc5	Nicéphor
Niépce	Niépce	k1gFnPc4	Niépce
–	–	k?	–
na	na	k7c4	na
vyleštěnou	vyleštěný	k2eAgFnSc4d1	vyleštěná
cínovou	cínový	k2eAgFnSc4d1	cínová
desku	deska	k1gFnSc4	deska
pokrytou	pokrytý	k2eAgFnSc4d1	pokrytá
petrolejovým	petrolejový	k2eAgInSc7d1	petrolejový
roztokem	roztok	k1gInSc7	roztok
asfaltu	asfalt	k1gInSc2	asfalt
<g/>
.	.	kIx.	.
</s>
