<s>
Historie	historie	k1gFnSc1	historie
a	a	k8xC	a
vývoj	vývoj	k1gInSc1	vývoj
fotografie	fotografia	k1gFnSc2	fotografia
se	se	k3xPyFc4	se
odvíjí	odvíjet	k5eAaImIp3nS	odvíjet
na	na	k7c6	na
základě	základ	k1gInSc6	základ
(	(	kIx(	(
<g/>
znovu	znovu	k6eAd1	znovu
<g/>
)	)	kIx)	)
<g/>
objevení	objevení	k1gNnSc1	objevení
technického	technický	k2eAgInSc2d1	technický
aspektu	aspekt	k1gInSc2	aspekt
principu	princip	k1gInSc2	princip
fungování	fungování	k1gNnSc2	fungování
camery	camera	k1gFnSc2	camera
obscury	obscura	k1gFnSc2	obscura
a	a	k8xC	a
laterny	laterna	k1gFnSc2	laterna
magicy	magica	k1gFnSc2	magica
<g/>
,	,	kIx,	,
vynálezu	vynález	k1gInSc2	vynález
camery	camera	k1gFnSc2	camera
lucidy	lucida	k1gFnSc2	lucida
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
panoramy	panoram	k1gInPc1	panoram
a	a	k8xC	a
dioramy	dioram	k1gInPc1	dioram
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
jednodušší	jednoduchý	k2eAgInSc4d2	jednodušší
časový	časový	k2eAgInSc4d1	časový
přehled	přehled	k1gInSc4	přehled
viz	vidět	k5eAaImRp2nS	vidět
článek	článek	k1gInSc4	článek
chronologie	chronologie	k1gFnPc1	chronologie
fotografie	fotografia	k1gFnSc2	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
Projekce	projekce	k1gFnSc1	projekce
obrazů	obraz	k1gInPc2	obraz
na	na	k7c4	na
plochu	plocha	k1gFnSc4	plocha
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
již	již	k9	již
po	po	k7c4	po
staletí	staletí	k1gNnPc4	staletí
<g/>
.	.	kIx.	.
</s>
<s>
Takzvané	takzvaný	k2eAgFnPc1d1	takzvaná
camera	camera	k1gFnSc1	camera
obscura	obscura	k1gFnSc1	obscura
a	a	k8xC	a
camera	camera	k1gFnSc1	camera
lucida	lucida	k1gFnSc1	lucida
byly	být	k5eAaImAgFnP	být
umělci	umělec	k1gMnPc7	umělec
využívány	využívat	k5eAaImNgInP	využívat
již	již	k6eAd1	již
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
jednoduché	jednoduchý	k2eAgInPc1d1	jednoduchý
přístroje	přístroj	k1gInPc1	přístroj
ovšem	ovšem	k9	ovšem
zachycený	zachycený	k2eAgInSc4d1	zachycený
obraz	obraz	k1gInSc4	obraz
neuměly	umět	k5eNaImAgFnP	umět
nijak	nijak	k6eAd1	nijak
ustálit	ustálit	k5eAaPmF	ustálit
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
promítaly	promítat	k5eAaImAgInP	promítat
objekty	objekt	k1gInPc1	objekt
před	před	k7c7	před
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
</s>
<s>
Camera	Camer	k1gMnSc4	Camer
obscura	obscur	k1gMnSc4	obscur
doslova	doslova	k6eAd1	doslova
přeloženo	přeložen	k2eAgNnSc1d1	přeloženo
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
temná	temný	k2eAgFnSc1d1	temná
místnost	místnost	k1gFnSc1	místnost
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
první	první	k4xOgFnSc4	první
fotografii	fotografia	k1gFnSc4	fotografia
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
snímek	snímek	k1gInSc1	snímek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zhotovil	zhotovit	k5eAaPmAgInS	zhotovit
roku	rok	k1gInSc2	rok
1826	[number]	k4	1826
francouzský	francouzský	k2eAgMnSc1d1	francouzský
vynálezce	vynálezce	k1gMnSc1	vynálezce
Joseph	Joseph	k1gMnSc1	Joseph
Nicéphore	Nicéphor	k1gInSc5	Nicéphor
Niépce	Niépce	k1gFnPc4	Niépce
–	–	k?	–
na	na	k7c4	na
vyleštěnou	vyleštěný	k2eAgFnSc4d1	vyleštěná
cínovou	cínový	k2eAgFnSc4d1	cínová
desku	deska	k1gFnSc4	deska
pokrytou	pokrytý	k2eAgFnSc4d1	pokrytá
petrolejovým	petrolejový	k2eAgInSc7d1	petrolejový
roztokem	roztok	k1gInSc7	roztok
asfaltu	asfalt	k1gInSc2	asfalt
<g/>
.	.	kIx.	.
</s>
<s>
Vznikl	vzniknout	k5eAaPmAgInS	vzniknout
ve	v	k7c6	v
fotopřístroji	fotopřístroj	k1gInSc6	fotopřístroj
<g/>
,	,	kIx,	,
a	a	k8xC	a
čas	čas	k1gInSc1	čas
expozice	expozice	k1gFnSc2	expozice
byl	být	k5eAaImAgInS	být
celých	celý	k2eAgFnPc2d1	celá
osm	osm	k4xCc1	osm
hodin	hodina	k1gFnPc2	hodina
za	za	k7c2	za
slunného	slunný	k2eAgInSc2d1	slunný
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
zdlouhavý	zdlouhavý	k2eAgInSc1d1	zdlouhavý
proces	proces	k1gInSc1	proces
se	se	k3xPyFc4	se
ukázal	ukázat	k5eAaPmAgInS	ukázat
býti	být	k5eAaImF	být
slepou	slepý	k2eAgFnSc7d1	slepá
uličkou	ulička	k1gFnSc7	ulička
a	a	k8xC	a
Niépce	Niépec	k1gMnSc2	Niépec
začal	začít	k5eAaPmAgMnS	začít
experimentovat	experimentovat	k5eAaImF	experimentovat
se	s	k7c7	s
sloučeninami	sloučenina	k1gFnPc7	sloučenina
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
vycházel	vycházet	k5eAaImAgMnS	vycházet
z	z	k7c2	z
poznatků	poznatek	k1gInPc2	poznatek
Joanna	Joann	k1gMnSc2	Joann
Heinricha	Heinrich	k1gMnSc2	Heinrich
Schultze	Schultze	k1gFnSc2	Schultze
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
směs	směs	k1gFnSc1	směs
křídy	křída	k1gFnSc2	křída
a	a	k8xC	a
stříbra	stříbro	k1gNnSc2	stříbro
tmavnou	tmavnout	k5eAaImIp3nP	tmavnout
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
osvětleny	osvětlit	k5eAaPmNgFnP	osvětlit
<g/>
.	.	kIx.	.
</s>
<s>
Niépce	Niépce	k1gFnSc1	Niépce
a	a	k8xC	a
umělec	umělec	k1gMnSc1	umělec
Jacques	Jacques	k1gMnSc1	Jacques
Daguerre	Daguerr	k1gInSc5	Daguerr
zdokonalili	zdokonalit	k5eAaPmAgMnP	zdokonalit
existující	existující	k2eAgInSc4d1	existující
proces	proces	k1gInSc4	proces
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
stříbra	stříbro	k1gNnSc2	stříbro
společně	společně	k6eAd1	společně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1833	[number]	k4	1833
Niépce	Niépec	k1gInSc2	Niépec
umírá	umírat	k5eAaImIp3nS	umírat
a	a	k8xC	a
necháva	cháva	k6eNd1	cháva
své	svůj	k3xOyFgFnPc4	svůj
poznámky	poznámka	k1gFnPc4	poznámka
Daguerrovi	Daguerrův	k2eAgMnPc1d1	Daguerrův
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
neměl	mít	k5eNaImAgMnS	mít
příliš	příliš	k6eAd1	příliš
zkušeností	zkušenost	k1gFnPc2	zkušenost
s	s	k7c7	s
vědou	věda	k1gFnSc7	věda
<g/>
,	,	kIx,	,
učinil	učinit	k5eAaImAgMnS	učinit
dva	dva	k4xCgInPc4	dva
klíčové	klíčový	k2eAgInPc4d1	klíčový
objevy	objev	k1gInPc4	objev
<g/>
.	.	kIx.	.
</s>
<s>
Zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
stříbro	stříbro	k1gNnSc1	stříbro
nejprve	nejprve	k6eAd1	nejprve
vystaví	vystavit	k5eAaPmIp3nS	vystavit
jódovým	jódový	k2eAgFnPc3d1	jódová
parám	para	k1gFnPc3	para
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
snímek	snímek	k1gInSc1	snímek
exponuje	exponovat	k5eAaImIp3nS	exponovat
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
nechá	nechat	k5eAaPmIp3nS	nechat
působit	působit	k5eAaImF	působit
rtuťové	rtuťový	k2eAgInPc4d1	rtuťový
výpary	výpar	k1gInPc4	výpar
<g/>
,	,	kIx,	,
získá	získat	k5eAaPmIp3nS	získat
viditelný	viditelný	k2eAgInSc1d1	viditelný
a	a	k8xC	a
nestálý	stálý	k2eNgInSc1d1	nestálý
obraz	obraz	k1gInSc1	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
pak	pak	k6eAd1	pak
lze	lze	k6eAd1	lze
ustálit	ustálit	k5eAaPmF	ustálit
ponořením	ponoření	k1gNnSc7	ponoření
desky	deska	k1gFnSc2	deska
do	do	k7c2	do
solné	solný	k2eAgFnSc2d1	solná
lázně	lázeň	k1gFnSc2	lázeň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1839	[number]	k4	1839
Daguerre	Daguerr	k1gInSc5	Daguerr
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
objevil	objevit	k5eAaPmAgInS	objevit
proces	proces	k1gInSc1	proces
využívající	využívající	k2eAgFnSc4d1	využívající
postříbřenou	postříbřený	k2eAgFnSc4d1	postříbřená
měděnou	měděný	k2eAgFnSc4d1	měděná
desku	deska	k1gFnSc4	deska
<g/>
,	,	kIx,	,
nazval	nazvat	k5eAaPmAgMnS	nazvat
jej	on	k3xPp3gMnSc4	on
daguerrotypie	daguerrotypie	k1gFnSc1	daguerrotypie
<g/>
.	.	kIx.	.
</s>
<s>
Podobný	podobný	k2eAgInSc4d1	podobný
proces	proces	k1gInSc4	proces
dodnes	dodnes	k6eAd1	dodnes
využívají	využívat	k5eAaPmIp3nP	využívat
fotoaparáty	fotoaparát	k1gInPc1	fotoaparát
Polaroid	polaroid	k1gInSc1	polaroid
<g/>
.	.	kIx.	.
</s>
<s>
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
vláda	vláda	k1gFnSc1	vláda
patent	patent	k1gInSc4	patent
koupila	koupit	k5eAaPmAgFnS	koupit
a	a	k8xC	a
dala	dát	k5eAaPmAgFnS	dát
jej	on	k3xPp3gMnSc4	on
ihned	ihned	k6eAd1	ihned
k	k	k7c3	k
volnému	volný	k2eAgNnSc3d1	volné
užití	užití	k1gNnSc3	užití
(	(	kIx(	(
<g/>
public	publicum	k1gNnPc2	publicum
domain	domaina	k1gFnPc2	domaina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
kanálu	kanál	k1gInSc2	kanál
La	la	k1gNnSc2	la
Manche	Manche	k1gNnSc1	Manche
<g/>
,	,	kIx,	,
William	William	k1gInSc1	William
Fox	fox	k1gInSc1	fox
Talbot	Talbot	k1gInSc1	Talbot
objevil	objevit	k5eAaPmAgInS	objevit
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
jiný	jiný	k2eAgInSc4d1	jiný
způsob	způsob	k1gInSc4	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
ustálit	ustálit	k5eAaPmF	ustálit
obraz	obraz	k1gInSc4	obraz
získaný	získaný	k2eAgInSc4d1	získaný
pomocí	pomocí	k7c2	pomocí
stříbrné	stříbrný	k2eAgFnSc2d1	stříbrná
expozice	expozice	k1gFnSc2	expozice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
udržoval	udržovat	k5eAaImAgMnS	udržovat
jej	on	k3xPp3gNnSc4	on
v	v	k7c6	v
tajnosti	tajnost	k1gFnSc6	tajnost
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
četl	číst	k5eAaImAgMnS	číst
o	o	k7c6	o
Daguerrově	Daguerrův	k2eAgInSc6d1	Daguerrův
vynálezu	vynález	k1gInSc6	vynález
<g/>
,	,	kIx,	,
Talbot	Talbot	k1gMnSc1	Talbot
svůj	svůj	k3xOyFgInSc4	svůj
proces	proces	k1gInSc4	proces
zdokonalil	zdokonalit	k5eAaPmAgMnS	zdokonalit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
dostatečně	dostatečně	k6eAd1	dostatečně
rychlý	rychlý	k2eAgInSc1d1	rychlý
a	a	k8xC	a
citlivý	citlivý	k2eAgInSc1d1	citlivý
pro	pro	k7c4	pro
snímání	snímání	k1gNnSc4	snímání
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1840	[number]	k4	1840
oznámil	oznámit	k5eAaPmAgMnS	oznámit
vynález	vynález	k1gInSc4	vynález
kalotypie	kalotypie	k1gFnSc2	kalotypie
<g/>
.	.	kIx.	.
</s>
<s>
Listy	lista	k1gFnPc4	lista
papíru	papír	k1gInSc2	papír
potáhl	potáhnout	k5eAaPmAgMnS	potáhnout
vrstvou	vrstva	k1gFnSc7	vrstva
chloridu	chlorid	k1gInSc2	chlorid
stříbrného	stříbrný	k1gInSc2	stříbrný
pro	pro	k7c4	pro
vytvoření	vytvoření	k1gNnSc4	vytvoření
okamžitého	okamžitý	k2eAgInSc2d1	okamžitý
negativního	negativní	k2eAgInSc2d1	negativní
obrazu	obraz	k1gInSc2	obraz
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
použit	použít	k5eAaPmNgInS	použít
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
libovolného	libovolný	k2eAgNnSc2d1	libovolné
množství	množství	k1gNnSc2	množství
kopií	kopie	k1gFnPc2	kopie
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
podobá	podobat	k5eAaImIp3nS	podobat
i	i	k8xC	i
dnešnímu	dnešní	k2eAgInSc3d1	dnešní
běžnému	běžný	k2eAgInSc3d1	běžný
negativnímu	negativní	k2eAgInSc3d1	negativní
procesu	proces	k1gInSc3	proces
<g/>
.	.	kIx.	.
</s>
<s>
Talbot	Talbot	k1gInSc4	Talbot
si	se	k3xPyFc3	se
proces	proces	k1gInSc4	proces
patentoval	patentovat	k5eAaBmAgInS	patentovat
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
značně	značně	k6eAd1	značně
omezil	omezit	k5eAaPmAgInS	omezit
jeho	jeho	k3xOp3gFnSc4	jeho
používanost	používanost	k1gFnSc4	používanost
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
zbytek	zbytek	k1gInSc4	zbytek
života	život	k1gInSc2	život
pak	pak	k6eAd1	pak
soudní	soudní	k2eAgFnSc7d1	soudní
cestou	cesta	k1gFnSc7	cesta
obhajoval	obhajovat	k5eAaImAgMnS	obhajovat
svůj	svůj	k3xOyFgInSc4	svůj
patent	patent	k1gInSc4	patent
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
svojí	svůj	k3xOyFgFnSc2	svůj
práce	práce	k1gFnSc2	práce
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
fotografie	fotografia	k1gFnSc2	fotografia
zanechal	zanechat	k5eAaPmAgMnS	zanechat
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
ale	ale	k8xC	ale
Talbotův	Talbotův	k2eAgInSc4d1	Talbotův
proces	proces	k1gInSc4	proces
zdokonalil	zdokonalit	k5eAaPmAgMnS	zdokonalit
George	George	k1gFnSc4	George
Eastman	Eastman	k1gMnSc1	Eastman
<g/>
,	,	kIx,	,
a	a	k8xC	a
ten	ten	k3xDgInSc1	ten
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
Hippolyte	Hippolyt	k1gInSc5	Hippolyt
Bayard	Bayarda	k1gFnPc2	Bayarda
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
fotografovat	fotografovat	k5eAaImF	fotografovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
oznámením	oznámení	k1gNnSc7	oznámení
vynálezu	vynález	k1gInSc2	vynález
se	se	k3xPyFc4	se
zpozdil	zpozdit	k5eAaPmAgMnS	zpozdit
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
proto	proto	k8xC	proto
počítán	počítat	k5eAaImNgMnS	počítat
mezi	mezi	k7c4	mezi
objevitele	objevitel	k1gMnSc4	objevitel
fotografie	fotografia	k1gFnSc2	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1851	[number]	k4	1851
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
Frederick	Frederick	k1gMnSc1	Frederick
Scott	Scotta	k1gFnPc2	Scotta
Archer	Archra	k1gFnPc2	Archra
mokrý	mokrý	k2eAgInSc1d1	mokrý
kolodiový	kolodiový	k2eAgInSc1d1	kolodiový
proces	proces	k1gInSc1	proces
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
použitý	použitý	k2eAgInSc1d1	použitý
Lewisem	Lewis	k1gInSc7	Lewis
Carrollem	Carrollo	k1gNnSc7	Carrollo
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Barevná	barevný	k2eAgFnSc1d1	barevná
fotografie	fotografie	k1gFnSc1	fotografie
<g/>
.	.	kIx.	.
</s>
<s>
Fyzikální	fyzikální	k2eAgInSc1d1	fyzikální
princip	princip	k1gInSc1	princip
barevné	barevný	k2eAgFnSc2d1	barevná
fotografie	fotografia	k1gFnSc2	fotografia
poprvé	poprvé	k6eAd1	poprvé
předvedl	předvést	k5eAaPmAgInS	předvést
James	James	k1gInSc1	James
Clerk	Clerk	k1gInSc1	Clerk
Maxwell	maxwell	k1gInSc1	maxwell
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
17	[number]	k4	17
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1861	[number]	k4	1861
<g/>
.	.	kIx.	.
</s>
<s>
Promítl	promítnout	k5eAaPmAgMnS	promítnout
na	na	k7c4	na
plátno	plátno	k1gNnSc4	plátno
současně	současně	k6eAd1	současně
tři	tři	k4xCgInPc4	tři
černobílé	černobílý	k2eAgInPc4d1	černobílý
snímky	snímek	k1gInPc4	snímek
barevné	barevný	k2eAgFnSc2d1	barevná
řádové	řádový	k2eAgFnSc2d1	řádová
stuhy	stuha	k1gFnSc2	stuha
přes	přes	k7c4	přes
červený	červený	k2eAgInSc4d1	červený
<g/>
,	,	kIx,	,
zelený	zelený	k2eAgInSc4d1	zelený
a	a	k8xC	a
modrý	modrý	k2eAgInSc4d1	modrý
filtr	filtr	k1gInSc4	filtr
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
předtím	předtím	k6eAd1	předtím
exponovány	exponován	k2eAgInPc4d1	exponován
přes	přes	k7c4	přes
filtry	filtr	k1gInPc4	filtr
stejných	stejný	k2eAgFnPc2d1	stejná
barev	barva	k1gFnPc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Prokázal	prokázat	k5eAaPmAgInS	prokázat
tak	tak	k9	tak
princip	princip	k1gInSc1	princip
aditivního	aditivní	k2eAgNnSc2d1	aditivní
míchání	míchání	k1gNnSc2	míchání
barev	barva	k1gFnPc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
však	však	k9	však
byla	být	k5eAaImAgFnS	být
použitá	použitý	k2eAgFnSc1d1	použitá
exponovaná	exponovaný	k2eAgFnSc1d1	exponovaná
fotocitlivá	fotocitlivý	k2eAgFnSc1d1	fotocitlivá
emulze	emulze	k1gFnSc1	emulze
necitlivá	citlivý	k2eNgFnSc1d1	necitlivá
na	na	k7c4	na
červenou	červený	k2eAgFnSc4d1	červená
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
červené	červená	k1gFnSc2	červená
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
snímku	snímek	k1gInSc6	snímek
přes	přes	k7c4	přes
červený	červený	k2eAgInSc4d1	červený
filtr	filtr	k1gInSc4	filtr
exponována	exponován	k2eAgFnSc1d1	exponována
okem	oke	k1gNnSc7	oke
neviditelná	viditelný	k2eNgFnSc1d1	neviditelná
ultrafialová	ultrafialový	k2eAgFnSc1d1	ultrafialová
část	část	k1gFnSc1	část
spektra	spektrum	k1gNnSc2	spektrum
–	–	k?	–
Prakticky	prakticky	k6eAd1	prakticky
však	však	k9	však
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
technika	technika	k1gFnSc1	technika
kvůli	kvůli	k7c3	kvůli
své	svůj	k3xOyFgFnSc3	svůj
komplikovanosti	komplikovanost	k1gFnSc3	komplikovanost
nepoužitelná	použitelný	k2eNgFnSc1d1	nepoužitelná
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
výzkumy	výzkum	k1gInPc4	výzkum
Jamese	Jamese	k1gFnSc2	Jamese
Maxwella	Maxwell	k1gMnSc2	Maxwell
navázal	navázat	k5eAaPmAgMnS	navázat
Louis	Louis	k1gMnSc1	Louis
Ducos	Ducos	k1gMnSc1	Ducos
du	du	k?	du
Hauron	Hauron	k1gMnSc1	Hauron
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1862	[number]	k4	1862
pracoval	pracovat	k5eAaImAgMnS	pracovat
několik	několik	k4yIc1	několik
let	léto	k1gNnPc2	léto
na	na	k7c6	na
praktickém	praktický	k2eAgInSc6d1	praktický
způsob	způsob	k1gInSc1	způsob
záznamu	záznam	k1gInSc2	záznam
barevných	barevný	k2eAgFnPc2d1	barevná
fotografií	fotografia	k1gFnPc2	fotografia
pomocí	pomocí	k7c2	pomocí
dvou	dva	k4xCgInPc2	dva
barevných	barevný	k2eAgInPc2d1	barevný
systémů	systém	k1gInPc2	systém
<g/>
:	:	kIx,	:
subtraktivního	subtraktivní	k2eAgMnSc2d1	subtraktivní
(	(	kIx(	(
<g/>
žlutá	žlutý	k2eAgFnSc1d1	žlutá
<g/>
,	,	kIx,	,
azurová	azurový	k2eAgFnSc1d1	azurová
<g/>
,	,	kIx,	,
purpurová	purpurový	k2eAgFnSc1d1	purpurová
<g/>
)	)	kIx)	)
a	a	k8xC	a
aditivního	aditivní	k2eAgMnSc2d1	aditivní
(	(	kIx(	(
<g/>
červená	červená	k1gFnSc1	červená
<g/>
,	,	kIx,	,
zelená	zelený	k2eAgFnSc1d1	zelená
<g/>
,	,	kIx,	,
modrá	modrý	k2eAgFnSc1d1	modrá
<g/>
)	)	kIx)	)
barevného	barevný	k2eAgInSc2d1	barevný
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1868	[number]	k4	1868
tyto	tento	k3xDgFnPc4	tento
metody	metoda	k1gFnPc4	metoda
patentoval	patentovat	k5eAaBmAgMnS	patentovat
<g/>
.	.	kIx.	.
</s>
<s>
Osvítil	osvítit	k5eAaPmAgInS	osvítit
bromostříbrnou	bromostříbrný	k2eAgFnSc4d1	bromostříbrný
kolodiovou	kolodiový	k2eAgFnSc4d1	kolodiová
desku	deska	k1gFnSc4	deska
výtažkovými	výtažkový	k2eAgInPc7d1	výtažkový
filtry	filtr	k1gInPc7	filtr
a	a	k8xC	a
zhotovil	zhotovit	k5eAaPmAgInS	zhotovit
tak	tak	k9	tak
diapozitivy	diapozitiv	k1gInPc4	diapozitiv
zabarvené	zabarvený	k2eAgInPc4d1	zabarvený
do	do	k7c2	do
červena	červeno	k1gNnSc2	červeno
<g/>
,	,	kIx,	,
modra	modro	k1gNnSc2	modro
a	a	k8xC	a
žluta	žluto	k1gNnSc2	žluto
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
tři	tři	k4xCgFnPc1	tři
části	část	k1gFnPc1	část
pak	pak	k6eAd1	pak
musely	muset	k5eAaImAgFnP	muset
být	být	k5eAaImF	být
k	k	k7c3	k
získání	získání	k1gNnSc3	získání
konečné	konečný	k2eAgFnSc2d1	konečná
fotografie	fotografia	k1gFnSc2	fotografia
zcela	zcela	k6eAd1	zcela
přesně	přesně	k6eAd1	přesně
položeny	položit	k5eAaPmNgFnP	položit
přes	přes	k7c4	přes
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
vysokým	vysoký	k2eAgInPc3d1	vysoký
nákladům	náklad	k1gInPc3	náklad
této	tento	k3xDgFnSc2	tento
metody	metoda	k1gFnSc2	metoda
se	se	k3xPyFc4	se
však	však	k9	však
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
mnoho	mnoho	k6eAd1	mnoho
nepoužívala	používat	k5eNaImAgFnS	používat
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
barevných	barevný	k2eAgFnPc2d1	barevná
fotografií	fotografia	k1gFnPc2	fotografia
je	být	k5eAaImIp3nS	být
Landscape	Landscap	k1gInSc5	Landscap
of	of	k?	of
Southern	Southern	k1gInSc4	Southern
France	Franc	k1gMnSc2	Franc
<g/>
,	,	kIx,	,
pořízená	pořízený	k2eAgFnSc1d1	pořízená
subtraktivní	subtraktivní	k2eAgFnSc7d1	subtraktivní
metodou	metoda	k1gFnSc7	metoda
r.	r.	kA	r.
1877	[number]	k4	1877
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
s	s	k7c7	s
Hauronem	Hauron	k1gInSc7	Hauron
objevil	objevit	k5eAaPmAgInS	objevit
podobný	podobný	k2eAgInSc1d1	podobný
systém	systém	k1gInSc1	systém
Charles	Charles	k1gMnSc1	Charles
Cros	Cros	k1gInSc1	Cros
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
později	pozdě	k6eAd2	pozdě
s	s	k7c7	s
Hauronem	Hauron	k1gInSc7	Hauron
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1888	[number]	k4	1888
F.	F.	kA	F.
E.	E.	kA	E.
Ives	Ives	k1gInSc1	Ives
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
tříbarevnou	tříbarevný	k2eAgFnSc4d1	tříbarevná
fotografii	fotografia	k1gFnSc4	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
Tu	ten	k3xDgFnSc4	ten
pak	pak	k6eAd1	pak
Němec	Němec	k1gMnSc1	Němec
Adolf	Adolf	k1gMnSc1	Adolf
Miethe	Miethe	k1gFnSc1	Miethe
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1903	[number]	k4	1903
používal	používat	k5eAaImAgMnS	používat
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
<g/>
.	.	kIx.	.
</s>
<s>
A.	A.	kA	A.
Miethe	Miethe	k1gInSc1	Miethe
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
také	také	k9	také
panchromatické	panchromatický	k2eAgNnSc4d1	panchromatický
zcitlivění	zcitlivění	k1gNnSc4	zcitlivění
pro	pro	k7c4	pro
reprodukci	reprodukce	k1gFnSc4	reprodukce
barevných	barevný	k2eAgInPc2d1	barevný
tónů	tón	k1gInPc2	tón
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1	další
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgMnS	zabývat
barevnou	barevný	k2eAgFnSc7d1	barevná
fotografií	fotografia	k1gFnSc7	fotografia
byli	být	k5eAaImAgMnP	být
roku	rok	k1gInSc2	rok
1904	[number]	k4	1904
v	v	k7c6	v
Lyonu	Lyon	k1gInSc6	Lyon
bratři	bratr	k1gMnPc1	bratr
Auguste	August	k1gMnSc5	August
a	a	k8xC	a
Louis	Louis	k1gMnSc1	Louis
Lumiè	Lumiè	k1gMnPc1	Lumiè
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
představili	představit	k5eAaPmAgMnP	představit
první	první	k4xOgFnPc4	první
autochromové	autochromový	k2eAgFnPc4d1	autochromový
desky	deska	k1gFnPc4	deska
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
daly	dát	k5eAaPmAgFnP	dát
reprodukovat	reprodukovat	k5eAaBmF	reprodukovat
barevným	barevný	k2eAgInSc7d1	barevný
tiskem	tisk	k1gInSc7	tisk
a	a	k8xC	a
umožnili	umožnit	k5eAaPmAgMnP	umožnit
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
vyrobit	vyrobit	k5eAaPmF	vyrobit
fotografii	fotografia	k1gFnSc4	fotografia
jedním	jeden	k4xCgInSc7	jeden
jediným	jediný	k2eAgInSc7d1	jediný
snímkem	snímek	k1gInSc7	snímek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1884	[number]	k4	1884
vyrobil	vyrobit	k5eAaPmAgMnS	vyrobit
George	George	k1gFnSc4	George
Eastman	Eastman	k1gMnSc1	Eastman
první	první	k4xOgInSc4	první
fotografický	fotografický	k2eAgInSc4d1	fotografický
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zbavil	zbavit	k5eAaPmAgMnS	zbavit
fotografy	fotograf	k1gMnPc4	fotograf
nutnosti	nutnost	k1gFnSc2	nutnost
nosit	nosit	k5eAaImF	nosit
s	s	k7c7	s
sebou	se	k3xPyFc7	se
těžké	těžký	k2eAgNnSc4d1	těžké
skleněné	skleněný	k2eAgFnPc4d1	skleněná
fotografické	fotografický	k2eAgFnPc4d1	fotografická
desky	deska	k1gFnPc4	deska
a	a	k8xC	a
jedovaté	jedovatý	k2eAgFnPc4d1	jedovatá
chemikálie	chemikálie	k1gFnPc4	chemikálie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1888	[number]	k4	1888
uvedl	uvést	k5eAaPmAgMnS	uvést
první	první	k4xOgInSc4	první
filmový	filmový	k2eAgInSc4d1	filmový
fotoaparát	fotoaparát	k1gInSc4	fotoaparát
pod	pod	k7c7	pod
obchodním	obchodní	k2eAgInSc7d1	obchodní
názvem	název	k1gInSc7	název
Kodak	Kodak	kA	Kodak
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
1925	[number]	k4	1925
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
trh	trh	k1gInSc4	trh
uveden	uvést	k5eAaPmNgInS	uvést
fotoaparát	fotoaparát	k1gInSc4	fotoaparát
Leica	Leicus	k1gMnSc2	Leicus
<g/>
,	,	kIx,	,
používající	používající	k2eAgFnSc4d1	používající
35	[number]	k4	35
<g/>
mm	mm	kA	mm
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
stal	stát	k5eAaPmAgInS	stát
standardem	standard	k1gInSc7	standard
maloformátové	maloformátový	k2eAgFnPc4d1	maloformátová
fotografie	fotografia	k1gFnPc4	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1935	[number]	k4	1935
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
i	i	k9	i
barevné	barevný	k2eAgInPc4d1	barevný
filmy	film	k1gInPc4	film
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
firma	firma	k1gFnSc1	firma
Polaroid	polaroid	k1gInSc4	polaroid
emulze	emulze	k1gFnSc2	emulze
umožňující	umožňující	k2eAgMnSc1d1	umožňující
vytvářet	vytvářet	k5eAaImF	vytvářet
barevné	barevný	k2eAgInPc4d1	barevný
snímky	snímek	k1gInPc4	snímek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
nepotřebovaly	potřebovat	k5eNaImAgInP	potřebovat
žádné	žádný	k3yNgNnSc4	žádný
další	další	k2eAgNnSc4d1	další
zpracování	zpracování	k1gNnSc4	zpracování
<g/>
,	,	kIx,	,
a	a	k8xC	a
fotografie	fotografie	k1gFnSc1	fotografie
se	se	k3xPyFc4	se
na	na	k7c6	na
nich	on	k3xPp3gMnPc6	on
objevila	objevit	k5eAaPmAgFnS	objevit
několik	několik	k4yIc4	několik
minut	minuta	k1gFnPc2	minuta
po	po	k7c6	po
expozici	expozice	k1gFnSc6	expozice
–	–	k?	–
tzv.	tzv.	kA	tzv.
okamžitá	okamžitý	k2eAgFnSc1d1	okamžitá
fotografie	fotografie	k1gFnSc1	fotografie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
vynalezli	vynaleznout	k5eAaPmAgMnP	vynaleznout
George	George	k1gNnSc4	George
Elwood	Elwooda	k1gFnPc2	Elwooda
Smith	Smith	k1gMnSc1	Smith
a	a	k8xC	a
Willard	Willard	k1gMnSc1	Willard
Boyle	Boyl	k1gMnSc2	Boyl
snímače	snímač	k1gInSc2	snímač
typu	typ	k1gInSc2	typ
CCD	CCD	kA	CCD
a	a	k8xC	a
v	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
zabudovali	zabudovat	k5eAaPmAgMnP	zabudovat
CCD	CCD	kA	CCD
do	do	k7c2	do
fotoaparátu	fotoaparát	k1gInSc2	fotoaparát
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
roku	rok	k1gInSc2	rok
1981	[number]	k4	1981
společnost	společnost	k1gFnSc1	společnost
Sony	Sony	kA	Sony
vyrobila	vyrobit	k5eAaPmAgFnS	vyrobit
první	první	k4xOgInSc4	první
fotoaparát	fotoaparát	k1gInSc4	fotoaparát
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
místo	místo	k7c2	místo
filmu	film	k1gInSc2	film
na	na	k7c6	na
chemickém	chemický	k2eAgInSc6d1	chemický
principu	princip	k1gInSc6	princip
zaznamenával	zaznamenávat	k5eAaImAgInS	zaznamenávat
obraz	obraz	k1gInSc1	obraz
na	na	k7c4	na
elektronické	elektronický	k2eAgInPc4d1	elektronický
prvky	prvek	k1gInPc4	prvek
CCD	CCD	kA	CCD
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
analogové	analogový	k2eAgInPc1d1	analogový
výstupy	výstup	k1gInPc1	výstup
se	se	k3xPyFc4	se
zapisovaly	zapisovat	k5eAaImAgInP	zapisovat
na	na	k7c4	na
disketu	disketa	k1gFnSc4	disketa
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
tahounem	tahoun	k1gInSc7	tahoun
vývoje	vývoj	k1gInSc2	vývoj
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
osmdesátých	osmdesátý	k4xOgNnPc6	osmdesátý
létech	léto	k1gNnPc6	léto
firma	firma	k1gFnSc1	firma
Kodak	Kodak	kA	Kodak
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
komerčně	komerčně	k6eAd1	komerčně
šířený	šířený	k2eAgInSc1d1	šířený
digitální	digitální	k2eAgInSc1d1	digitální
fotoaparát	fotoaparát	k1gInSc1	fotoaparát
byl	být	k5eAaImAgInS	být
Apple	Apple	kA	Apple
QuickTake	QuickTak	k1gInSc2	QuickTak
100	[number]	k4	100
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
běžném	běžný	k2eAgInSc6d1	běžný
prodeji	prodej	k1gInSc6	prodej
byly	být	k5eAaImAgInP	být
digitální	digitální	k2eAgInPc1d1	digitální
fotoaparáty	fotoaparát	k1gInPc1	fotoaparát
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
i	i	k8xC	i
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
aparáty	aparát	k1gInPc1	aparát
používající	používající	k2eAgInSc1d1	používající
digitální	digitální	k2eAgInSc4d1	digitální
záznam	záznam	k1gInSc4	záznam
začaly	začít	k5eAaPmAgFnP	začít
vytlačovat	vytlačovat	k5eAaImF	vytlačovat
běžné	běžný	k2eAgFnPc4d1	běžná
kinofilmové	kinofilmový	k2eAgFnPc4d1	kinofilmová
<g/>
.	.	kIx.	.
</s>
<s>
Chronologie	chronologie	k1gFnSc1	chronologie
fotografie	fotografie	k1gFnSc1	fotografie
Historie	historie	k1gFnSc1	historie
norské	norský	k2eAgFnSc2d1	norská
fotografie	fotografia	k1gFnSc2	fotografia
Pionýři	pionýr	k1gMnPc1	pionýr
fotografické	fotografický	k2eAgFnSc2d1	fotografická
techniky	technika	k1gFnSc2	technika
Fotografický	fotografický	k2eAgInSc4d1	fotografický
proces	proces	k1gInSc4	proces
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
historie	historie	k1gFnSc2	historie
fotografie	fotografia	k1gFnSc2	fotografia
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
