<p>
<s>
Foxy	fox	k1gInPc4	fox
Lady	lady	k1gFnPc2	lady
je	být	k5eAaImIp3nS	být
osmé	osmý	k4xOgNnSc1	osmý
studiové	studiový	k2eAgNnSc1d1	studiové
album	album	k1gNnSc1	album
americké	americký	k2eAgFnSc2d1	americká
zpěvačky	zpěvačka	k1gFnSc2	zpěvačka
a	a	k8xC	a
herečky	herečka	k1gFnSc2	herečka
Cher	Chera	k1gFnPc2	Chera
<g/>
,	,	kIx,	,
vydané	vydaný	k2eAgFnPc4d1	vydaná
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
u	u	k7c2	u
společnosti	společnost	k1gFnSc2	společnost
MCA	MCA	kA	MCA
Records	Records	k1gInSc1	Records
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
O	o	k7c6	o
Albu	album	k1gNnSc6	album
==	==	k?	==
</s>
</p>
<p>
<s>
Album	album	k1gNnSc1	album
Foxy	fox	k1gInPc4	fox
Lady	lady	k1gFnSc4	lady
přichází	přicházet	k5eAaImIp3nS	přicházet
po	po	k7c6	po
velmi	velmi	k6eAd1	velmi
úspěšném	úspěšný	k2eAgNnSc6d1	úspěšné
eponymním	eponymní	k2eAgNnSc6d1	eponymní
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
přejmenované	přejmenovaný	k2eAgInPc1d1	přejmenovaný
na	na	k7c4	na
Gypsys	Gypsys	k1gInSc4	Gypsys
<g/>
,	,	kIx,	,
Tramps	Tramps	k1gInSc1	Tramps
&	&	k?	&
Thives	Thives	k1gInSc4	Thives
<g/>
)	)	kIx)	)
albu	alba	k1gFnSc4	alba
z	z	k7c2	z
předchozího	předchozí	k2eAgInSc2d1	předchozí
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
období	období	k1gNnSc1	období
bylo	být	k5eAaImAgNnS	být
celkově	celkově	k6eAd1	celkově
pro	pro	k7c4	pro
Cher	Cher	k1gInSc4	Cher
velmi	velmi	k6eAd1	velmi
úspěšné	úspěšný	k2eAgFnPc1d1	úspěšná
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
úspěšnou	úspěšný	k2eAgFnSc7d1	úspěšná
zpěvačkou	zpěvačka	k1gFnSc7	zpěvačka
a	a	k8xC	a
nově	nově	k6eAd1	nově
i	i	k9	i
televizní	televizní	k2eAgFnSc7d1	televizní
hvězdou	hvězda	k1gFnSc7	hvězda
s	s	k7c7	s
show	show	k1gFnPc7	show
The	The	k1gMnSc1	The
Sonny	Sonna	k1gFnSc2	Sonna
&	&	k?	&
Cher	Cher	k1gInSc1	Cher
Comedy	Comeda	k1gMnSc2	Comeda
Hour	Hour	k1gInSc4	Hour
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
nová	nový	k2eAgFnSc1d1	nová
deska	deska	k1gFnSc1	deska
vyšla	vyjít	k5eAaPmAgFnS	vyjít
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
hudebních	hudební	k2eAgInPc6d1	hudební
žebříčcích	žebříček	k1gInPc6	žebříček
společně	společně	k6eAd1	společně
s	s	k7c7	s
předchozím	předchozí	k2eAgNnSc7d1	předchozí
albem	album	k1gNnSc7	album
a	a	k8xC	a
také	také	k9	také
s	s	k7c7	s
albem	album	k1gNnSc7	album
Sonnyho	Sonny	k1gMnSc2	Sonny
&	&	k?	&
Cher	Cher	k1gMnSc1	Cher
All	All	k1gMnSc1	All
I	i	k8xC	i
Ever	Ever	k1gMnSc1	Ever
Need	Need	k1gMnSc1	Need
Is	Is	k1gMnSc1	Is
You	You	k1gMnSc1	You
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Album	album	k1gNnSc4	album
produkoval	produkovat	k5eAaImAgMnS	produkovat
převážně	převážně	k6eAd1	převážně
Snuff	Snuff	k1gMnSc1	Snuff
Garrett	Garrett	k1gMnSc1	Garrett
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
stál	stát	k5eAaImAgInS	stát
i	i	k9	i
za	za	k7c7	za
předchozí	předchozí	k2eAgFnSc7d1	předchozí
deskou	deska	k1gFnSc7	deska
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgFnPc4	tři
písně	píseň	k1gFnPc4	píseň
koprodukoval	koprodukovat	k5eAaImAgMnS	koprodukovat
Sonny	Sonna	k1gFnPc1	Sonna
Bono	bona	k1gFnSc5	bona
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
to	ten	k3xDgNnSc1	ten
písně	píseň	k1gFnPc1	píseň
–	–	k?	–
"	"	kIx"	"
<g/>
A	a	k9	a
Song	song	k1gInSc1	song
For	forum	k1gNnPc2	forum
You	You	k1gFnSc2	You
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
First	First	k1gFnSc1	First
Time	Time	k1gFnSc1	Time
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Don	Don	k1gMnSc1	Don
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Hide	Hide	k1gInSc1	Hide
Your	Your	k1gInSc1	Your
Love	lov	k1gInSc5	lov
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
spolupráce	spolupráce	k1gFnSc1	spolupráce
však	však	k9	však
byla	být	k5eAaImAgFnS	být
pod	pod	k7c7	pod
takovým	takový	k3xDgInSc7	takový
tlakem	tlak	k1gInSc7	tlak
<g/>
,	,	kIx,	,
že	že	k8xS	že
nakonec	nakonec	k6eAd1	nakonec
Garrett	Garretta	k1gFnPc2	Garretta
po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
alba	album	k1gNnSc2	album
jako	jako	k8xS	jako
producent	producent	k1gMnSc1	producent
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
album	album	k1gNnSc1	album
Bittersweet	Bittersweeta	k1gFnPc2	Bittersweeta
White	Whit	k1gInSc5	Whit
Light	Light	k1gMnSc1	Light
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
tedy	tedy	k8xC	tedy
produkoval	produkovat	k5eAaImAgMnS	produkovat
Sonny	Sonna	k1gFnPc4	Sonna
Bono	bona	k1gFnSc5	bona
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
velký	velký	k2eAgInSc4d1	velký
neúspěch	neúspěch	k1gInSc4	neúspěch
<g/>
,	,	kIx,	,
Snuff	Snuff	k1gMnSc1	Snuff
Garrett	Garrett	k1gMnSc1	Garrett
byl	být	k5eAaImAgMnS	být
znovu	znovu	k6eAd1	znovu
osloven	osloven	k2eAgMnSc1d1	osloven
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
produkoval	produkovat	k5eAaImAgMnS	produkovat
další	další	k2eAgMnSc1d1	další
Cher	Cher	k1gMnSc1	Cher
alba	album	k1gNnSc2	album
Half-Breed	Half-Breed	k1gInSc1	Half-Breed
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
s	s	k7c7	s
podmínkou	podmínka	k1gFnSc7	podmínka
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Sonny	Sonna	k1gFnPc1	Sonna
Bono	bona	k1gFnSc5	bona
projektu	projekt	k1gInSc3	projekt
nezúčastní	zúčastnit	k5eNaPmIp3nP	zúčastnit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc7	první
stopou	stopa	k1gFnSc7	stopa
na	na	k7c6	na
albu	album	k1gNnSc6	album
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
Living	Living	k1gInSc1	Living
In	In	k1gFnSc2	In
A	a	k8xC	a
House	house	k1gNnSc1	house
Divided	Divided	k1gMnSc1	Divided
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
píseň	píseň	k1gFnSc4	píseň
o	o	k7c6	o
rozchodu	rozchod	k1gInSc6	rozchod
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
dvě	dva	k4xCgFnPc4	dva
coverze	coverze	k1gFnPc4	coverze
–	–	k?	–
"	"	kIx"	"
<g/>
A	a	k9	a
Song	song	k1gInSc1	song
For	forum	k1gNnPc2	forum
You	You	k1gFnSc2	You
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Never	Never	k1gInSc1	Never
Been	Been	k1gInSc1	Been
To	ten	k3xDgNnSc1	ten
Spain	Spain	k2eAgMnSc1d1	Spain
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
albu	album	k1gNnSc6	album
se	se	k3xPyFc4	se
také	také	k9	také
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
Bob	Bob	k1gMnSc1	Bob
Stone	ston	k1gInSc5	ston
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
megahitu	megahit	k1gInSc2	megahit
"	"	kIx"	"
<g/>
Gypsys	Gypsys	k1gInSc1	Gypsys
<g/>
,	,	kIx,	,
Tramps	Tramps	k1gInSc1	Tramps
&	&	k?	&
Thives	Thives	k1gInSc1	Thives
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
napsal	napsat	k5eAaPmAgInS	napsat
skladbu	skladba	k1gFnSc4	skladba
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
If	If	k1gMnSc1	If
I	i	k8xC	i
Knew	Knew	k1gMnSc1	Knew
Then	Then	k1gMnSc1	Then
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
písně	píseň	k1gFnPc1	píseň
z	z	k7c2	z
alba	album	k1gNnSc2	album
byly	být	k5eAaImAgInP	být
později	pozdě	k6eAd2	pozdě
přezpívány	přezpíván	k2eAgInPc1d1	přezpíván
americkou	americký	k2eAgFnSc7d1	americká
zpěvačkou	zpěvačka	k1gFnSc7	zpěvačka
Maureen	Maurena	k1gFnPc2	Maurena
McGovernem	McGoverno	k1gNnSc7	McGoverno
pro	pro	k7c4	pro
její	její	k3xOp3gNnSc4	její
debutové	debutový	k2eAgNnSc1d1	debutové
album	album	k1gNnSc1	album
The	The	k1gMnPc2	The
Morning	Morning	k1gInSc4	Morning
After	Aftra	k1gFnPc2	Aftra
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1973	[number]	k4	1973
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
pilotní	pilotní	k2eAgInSc1d1	pilotní
singl	singl	k1gInSc1	singl
"	"	kIx"	"
<g/>
Living	Living	k1gInSc1	Living
In	In	k1gFnSc2	In
A	a	k8xC	a
House	house	k1gNnSc1	house
Divided	Divided	k1gMnSc1	Divided
<g/>
"	"	kIx"	"
byl	být	k5eAaImAgInS	být
úspěšný	úspěšný	k2eAgInSc4d1	úspěšný
a	a	k8xC	a
prodeje	prodej	k1gInSc2	prodej
byly	být	k5eAaImAgInP	být
velmi	velmi	k6eAd1	velmi
dobré	dobrý	k2eAgInPc1d1	dobrý
<g/>
,	,	kIx,	,
album	album	k1gNnSc1	album
slavilo	slavit	k5eAaImAgNnS	slavit
pouze	pouze	k6eAd1	pouze
mírný	mírný	k2eAgInSc4d1	mírný
úspěch	úspěch	k1gInSc4	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Prodalo	prodat	k5eAaPmAgNnS	prodat
se	se	k3xPyFc4	se
ho	on	k3xPp3gInSc2	on
celosvětově	celosvětově	k6eAd1	celosvětově
asi	asi	k9	asi
750	[number]	k4	750
tisíc	tisíc	k4xCgInPc2	tisíc
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
Cher	Cher	k1gInSc4	Cher
zaneprázdněna	zaneprázdněn	k2eAgFnSc1d1	zaneprázdněna
svojí	svůj	k3xOyFgFnSc7	svůj
TV	TV	kA	TV
show	show	k1gNnSc4	show
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
u	u	k7c2	u
Mego	Mego	k6eAd1	Mego
Corporation	Corporation	k1gInSc1	Corporation
vydána	vydán	k2eAgFnSc1d1	vydána
kolekce	kolekce	k1gFnSc1	kolekce
Cher-panenek	Cheranenka	k1gFnPc2	Cher-panenka
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
outfitů	outfit	k1gInPc2	outfit
byl	být	k5eAaImAgMnS	být
věnovám	věnovat	k5eAaImIp1nS	věnovat
tomuto	tento	k3xDgNnSc3	tento
albu	album	k1gNnSc3	album
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
bylo	být	k5eAaImAgNnS	být
album	album	k1gNnSc1	album
vydáno	vydat	k5eAaPmNgNnS	vydat
společně	společně	k6eAd1	společně
s	s	k7c7	s
s	s	k7c7	s
předchozím	předchozí	k2eAgInSc7d1	předchozí
počinem	počin	k1gInSc7	počin
na	na	k7c6	na
kompaktním	kompaktní	k2eAgInSc6d1	kompaktní
disku	disk	k1gInSc6	disk
<g/>
.	.	kIx.	.
</s>
<s>
Samostatně	samostatně	k6eAd1	samostatně
album	album	k1gNnSc1	album
na	na	k7c6	na
CD	CD	kA	CD
nikdy	nikdy	k6eAd1	nikdy
nevyšlo	vyjít	k5eNaPmAgNnS	vyjít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Singly	singl	k1gInPc4	singl
==	==	k?	==
</s>
</p>
<p>
<s>
Z	z	k7c2	z
alba	album	k1gNnSc2	album
vzešly	vzejít	k5eAaPmAgInP	vzejít
dva	dva	k4xCgInPc1	dva
singly	singl	k1gInPc1	singl
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Living	Living	k1gInSc1	Living
In	In	k1gFnSc2	In
A	a	k8xC	a
House	house	k1gNnSc1	house
Divided	Divided	k1gMnSc1	Divided
<g/>
"	"	kIx"	"
vyšel	vyjít	k5eAaPmAgInS	vyjít
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
úspěšný	úspěšný	k2eAgMnSc1d1	úspěšný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
se	se	k3xPyFc4	se
umístil	umístit	k5eAaPmAgMnS	umístit
na	na	k7c4	na
22	[number]	k4	22
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
17	[number]	k4	17
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
vyšel	vyjít	k5eAaPmAgInS	vyjít
druhý	druhý	k4xOgInSc1	druhý
singl	singl	k1gInSc1	singl
"	"	kIx"	"
<g/>
Don	Don	k1gMnSc1	Don
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Hide	Hide	k1gInSc1	Hide
Your	Your	k1gInSc1	Your
Love	lov	k1gInSc5	lov
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
dostal	dostat	k5eAaPmAgInS	dostat
na	na	k7c4	na
46	[number]	k4	46
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Seznam	seznam	k1gInSc1	seznam
skladeb	skladba	k1gFnPc2	skladba
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Obsazení	obsazení	k1gNnSc1	obsazení
==	==	k?	==
</s>
</p>
<p>
<s>
Cher	Cher	k1gInSc1	Cher
–	–	k?	–
zpěvTechnická	zpěvTechnický	k2eAgFnSc1d1	zpěvTechnický
podporaSnuff	podporaSnuff	k1gInSc1	podporaSnuff
Garrett	Garrett	k1gMnSc1	Garrett
–	–	k?	–
producent	producent	k1gMnSc1	producent
</s>
</p>
<p>
<s>
Sonny	Sonna	k1gFnPc1	Sonna
Bono	bona	k1gFnSc5	bona
–	–	k?	–
producent	producent	k1gMnSc1	producent
a	a	k8xC	a
fotograf	fotograf	k1gMnSc1	fotograf
</s>
</p>
<p>
<s>
Lennie	Lennie	k1gFnSc1	Lennie
Roberts	Roberts	k1gInSc4	Roberts
–	–	k?	–
zvukový	zvukový	k2eAgMnSc1d1	zvukový
inženýr	inženýr	k1gMnSc1	inženýr
</s>
</p>
<p>
<s>
Al	ala	k1gFnPc2	ala
Capps	Cappsa	k1gFnPc2	Cappsa
–	–	k?	–
pomoc	pomoc	k1gFnSc4	pomoc
s	s	k7c7	s
aranžmá	aranžmá	k1gNnSc7	aranžmá
</s>
</p>
<p>
<s>
Gene	gen	k1gInSc5	gen
Page	Page	k1gFnPc3	Page
–	–	k?	–
pomoc	pomoc	k1gFnSc4	pomoc
s	s	k7c7	s
aranžmá	aranžmá	k1gNnSc7	aranžmá
</s>
</p>
<p>
<s>
Michel	Michel	k1gMnSc1	Michel
Rubini	Rubin	k1gMnPc1	Rubin
–	–	k?	–
pomoc	pomoc	k1gFnSc1	pomoc
s	s	k7c7	s
aranžmá	aranžmá	k1gNnSc7	aranžmá
</s>
</p>
<p>
<s>
Virginia	Virginium	k1gNnPc4	Virginium
Clark	Clark	k1gInSc1	Clark
–	–	k?	–
umělecký	umělecký	k2eAgMnSc1d1	umělecký
ředitel	ředitel	k1gMnSc1	ředitel
</s>
</p>
<p>
<s>
==	==	k?	==
Umístění	umístění	k1gNnSc1	umístění
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Foxy	fox	k1gInPc4	fox
Lady	lady	k1gFnSc1	lady
(	(	kIx(	(
<g/>
Cher	Cher	k1gInSc1	Cher
album	album	k1gNnSc1	album
<g/>
)	)	kIx)	)
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
zpěvačky	zpěvačka	k1gFnSc2	zpěvačka
</s>
</p>
