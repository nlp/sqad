<s>
SK	Sk	kA	Sk
Sigma	sigma	k1gNnSc1	sigma
Olomouc	Olomouc	k1gFnSc1	Olomouc
(	(	kIx(	(
<g/>
celým	celý	k2eAgInSc7d1	celý
názvem	název	k1gInSc7	název
<g/>
:	:	kIx,	:
Sportovní	sportovní	k2eAgInSc1d1	sportovní
klub	klub	k1gInSc1	klub
Sigma	sigma	k1gNnSc2	sigma
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgInSc1d1	český
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
moravském	moravský	k2eAgNnSc6d1	Moravské
městě	město	k1gNnSc6	město
Olomouc	Olomouc	k1gFnSc1	Olomouc
v	v	k7c6	v
Olomouckém	olomoucký	k2eAgInSc6d1	olomoucký
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
