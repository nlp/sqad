<p>
<s>
SK	Sk	kA	Sk
Sigma	sigma	k1gNnSc1	sigma
Olomouc	Olomouc	k1gFnSc1	Olomouc
(	(	kIx(	(
<g/>
celým	celý	k2eAgInSc7d1	celý
názvem	název	k1gInSc7	název
<g/>
:	:	kIx,	:
Sportovní	sportovní	k2eAgInSc1d1	sportovní
klub	klub	k1gInSc1	klub
Sigma	sigma	k1gNnSc2	sigma
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgInSc1d1	český
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
moravském	moravský	k2eAgNnSc6d1	Moravské
městě	město	k1gNnSc6	město
Olomouc	Olomouc	k1gFnSc1	Olomouc
v	v	k7c6	v
Olomouckém	olomoucký	k2eAgInSc6d1	olomoucký
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Klub	klub	k1gInSc1	klub
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
probojoval	probojovat	k5eAaPmAgMnS	probojovat
do	do	k7c2	do
první	první	k4xOgFnSc2	první
ligy	liga	k1gFnSc2	liga
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
působil	působit	k5eAaImAgMnS	působit
nepřetržitě	přetržitě	k6eNd1	přetržitě
od	od	k7c2	od
sezóny	sezóna	k1gFnSc2	sezóna
1984	[number]	k4	1984
<g/>
/	/	kIx~	/
<g/>
85	[number]	k4	85
do	do	k7c2	do
ročníku	ročník	k1gInSc2	ročník
2013	[number]	k4	2013
<g/>
/	/	kIx~	/
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dvou	dva	k4xCgInPc6	dva
sestupech	sestup	k1gInPc6	sestup
z	z	k7c2	z
elitní	elitní	k2eAgFnSc2d1	elitní
soutěže	soutěž	k1gFnSc2	soutěž
v	v	k7c6	v
ročnících	ročník	k1gInPc6	ročník
2013	[number]	k4	2013
<g/>
/	/	kIx~	/
<g/>
14	[number]	k4	14
a	a	k8xC	a
2015	[number]	k4	2015
<g/>
/	/	kIx~	/
<g/>
16	[number]	k4	16
se	se	k3xPyFc4	se
po	po	k7c6	po
dvou	dva	k4xCgFnPc6	dva
výhrách	výhra	k1gFnPc6	výhra
ve	v	k7c6	v
Fotbalové	fotbalový	k2eAgFnSc6d1	fotbalová
národní	národní	k2eAgFnSc6d1	národní
lize	liga	k1gFnSc6	liga
tým	tým	k1gInSc1	tým
pro	pro	k7c4	pro
sezónu	sezóna	k1gFnSc4	sezóna
2017	[number]	k4	2017
<g/>
/	/	kIx~	/
<g/>
18	[number]	k4	18
znovu	znovu	k6eAd1	znovu
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
největší	veliký	k2eAgInPc4d3	veliký
úspěchy	úspěch	k1gInPc4	úspěch
klubu	klub	k1gInSc2	klub
patří	patřit	k5eAaImIp3nP	patřit
zisk	zisk	k1gInSc4	zisk
českého	český	k2eAgInSc2d1	český
poháru	pohár	k1gInSc2	pohár
a	a	k8xC	a
českého	český	k2eAgInSc2d1	český
Superpoháru	superpohár	k1gInSc2	superpohár
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgInSc7d3	veliký
mezinárodním	mezinárodní	k2eAgInSc7d1	mezinárodní
úspěchem	úspěch	k1gInSc7	úspěch
klubu	klub	k1gInSc2	klub
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gFnSc4	jeho
účast	účast	k1gFnSc4	účast
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
Poháru	pohár	k1gInSc2	pohár
UEFA	UEFA	kA	UEFA
<g/>
.	.	kIx.	.
</s>
<s>
Svá	svůj	k3xOyFgNnPc4	svůj
domácí	domácí	k2eAgNnPc4d1	domácí
utkání	utkání	k1gNnPc4	utkání
hraje	hrát	k5eAaImIp3nS	hrát
tým	tým	k1gInSc4	tým
Sigmy	Sigma	k1gFnSc2	Sigma
Olomouc	Olomouc	k1gFnSc1	Olomouc
na	na	k7c6	na
Andrově	Andrův	k2eAgInSc6d1	Andrův
stadionu	stadion	k1gInSc6	stadion
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
kapacitu	kapacita	k1gFnSc4	kapacita
12	[number]	k4	12
541	[number]	k4	541
diváků	divák	k1gMnPc2	divák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Počátky	počátek	k1gInPc4	počátek
olomoucké	olomoucký	k2eAgFnSc2d1	olomoucká
kopané	kopaná	k1gFnSc2	kopaná
a	a	k8xC	a
založení	založení	k1gNnSc2	založení
klubu	klub	k1gInSc2	klub
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
se	se	k3xPyFc4	se
fotbal	fotbal	k1gInSc1	fotbal
hrál	hrát	k5eAaImAgInS	hrát
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zde	zde	k6eAd1	zde
povědomí	povědomí	k1gNnSc1	povědomí
o	o	k7c6	o
tomto	tento	k3xDgInSc6	tento
sportu	sport	k1gInSc6	sport
rozšířili	rozšířit	k5eAaPmAgMnP	rozšířit
bratří	bratr	k1gMnPc1	bratr
Tománkové	Tománková	k1gFnSc2	Tománková
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
klub	klub	k1gInSc1	klub
SK	Sk	kA	Sk
Olomouc	Olomouc	k1gFnSc1	Olomouc
a	a	k8xC	a
vedle	vedle	k7c2	vedle
něj	on	k3xPp3gInSc2	on
se	se	k3xPyFc4	se
záhy	záhy	k6eAd1	záhy
ustanovil	ustanovit	k5eAaPmAgInS	ustanovit
též	též	k9	též
německý	německý	k2eAgInSc1d1	německý
tým	tým	k1gInSc1	tým
DFC	DFC	kA	DFC
Olmütz	Olmütz	k1gInSc1	Olmütz
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
díky	díky	k7c3	díky
podpoře	podpora	k1gFnSc3	podpora
mecenáše	mecenáš	k1gMnPc4	mecenáš
olomouckého	olomoucký	k2eAgInSc2d1	olomoucký
fotbalu	fotbal	k1gInSc2	fotbal
<g/>
,	,	kIx,	,
podnikatele	podnikatel	k1gMnSc2	podnikatel
Josefa	Josef	k1gMnSc2	Josef
Andera	Ander	k1gMnSc2	Ander
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
tým	tým	k1gInSc1	tým
SK	Sk	kA	Sk
Olomouc	Olomouc	k1gFnSc1	Olomouc
ASO	ASO	kA	ASO
probojoval	probojovat	k5eAaPmAgMnS	probojovat
až	až	k6eAd1	až
do	do	k7c2	do
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
ligy	liga	k1gFnSc2	liga
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
v	v	k7c6	v
sezónách	sezóna	k1gFnPc6	sezóna
1941	[number]	k4	1941
<g/>
/	/	kIx~	/
<g/>
42	[number]	k4	42
<g/>
,	,	kIx,	,
1942	[number]	k4	1942
<g/>
/	/	kIx~	/
<g/>
44	[number]	k4	44
<g/>
,	,	kIx,	,
1943	[number]	k4	1943
<g/>
/	/	kIx~	/
<g/>
44	[number]	k4	44
a	a	k8xC	a
1946	[number]	k4	1946
<g/>
/	/	kIx~	/
<g/>
47	[number]	k4	47
<g/>
.	.	kIx.	.
</s>
<s>
Velkoobchodník	velkoobchodník	k1gMnSc1	velkoobchodník
Ander	Ander	k1gMnSc1	Ander
nechal	nechat	k5eAaPmAgMnS	nechat
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
zbudovat	zbudovat	k5eAaPmF	zbudovat
nový	nový	k2eAgInSc4d1	nový
stadion	stadion	k1gInSc4	stadion
pro	pro	k7c4	pro
20	[number]	k4	20
000	[number]	k4	000
diváků	divák	k1gMnPc2	divák
a	a	k8xC	a
SK	Sk	kA	Sk
Olomouc	Olomouc	k1gFnSc1	Olomouc
ASO	ASO	kA	ASO
se	se	k3xPyFc4	se
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
1939	[number]	k4	1939
<g/>
/	/	kIx~	/
<g/>
1940	[number]	k4	1940
stal	stát	k5eAaPmAgMnS	stát
první	první	k4xOgMnSc1	první
vítězem	vítěz	k1gMnSc7	vítěz
domácího	domácí	k2eAgInSc2d1	domácí
českého	český	k2eAgInSc2d1	český
poháru	pohár	k1gInSc2	pohár
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
války	válka	k1gFnSc2	válka
však	však	k9	však
olomoucký	olomoucký	k2eAgInSc4d1	olomoucký
stadion	stadion	k1gInSc4	stadion
zničila	zničit	k5eAaPmAgFnS	zničit
ustupující	ustupující	k2eAgFnSc1d1	ustupující
německá	německý	k2eAgFnSc1d1	německá
armáda	armáda	k1gFnSc1	armáda
<g/>
,	,	kIx,	,
na	na	k7c6	na
konci	konec	k1gInSc6	konec
40	[number]	k4	40
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
klub	klub	k1gInSc1	klub
SK	Sk	kA	Sk
Olomouc	Olomouc	k1gFnSc4	Olomouc
propadl	propadnout	k5eAaPmAgInS	propadnout
do	do	k7c2	do
nižších	nízký	k2eAgFnPc2d2	nižší
soutěží	soutěž	k1gFnPc2	soutěž
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
zcela	zcela	k6eAd1	zcela
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgNnSc7d1	další
olomouckým	olomoucký	k2eAgNnSc7d1	olomoucké
mužstvem	mužstvo	k1gNnSc7	mužstvo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
založení	založení	k1gNnSc6	založení
zařazeno	zařadit	k5eAaPmNgNnS	zařadit
do	do	k7c2	do
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
ligové	ligový	k2eAgFnSc2d1	ligová
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
celek	celek	k1gInSc4	celek
vojenského	vojenský	k2eAgNnSc2d1	vojenské
letectva	letectvo	k1gNnSc2	letectvo
Křídla	křídlo	k1gNnSc2	křídlo
vlasti	vlast	k1gFnSc2	vlast
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
Tým	tým	k1gInSc1	tým
Křídla	křídlo	k1gNnSc2	křídlo
vlasti	vlast	k1gFnSc2	vlast
později	pozdě	k6eAd2	pozdě
přejmenovaný	přejmenovaný	k2eAgInSc1d1	přejmenovaný
na	na	k7c6	na
VTJ	VTJ	kA	VTJ
Dukla	Dukla	k1gFnSc1	Dukla
Olomouc	Olomouc	k1gFnSc1	Olomouc
se	se	k3xPyFc4	se
ale	ale	k9	ale
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
soutěže	soutěž	k1gFnSc2	soutěž
účastnil	účastnit	k5eAaImAgInS	účastnit
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1953	[number]	k4	1953
a	a	k8xC	a
1954	[number]	k4	1954
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dnešní	dnešní	k2eAgInSc1d1	dnešní
klub	klub	k1gInSc1	klub
Sigma	sigma	k1gNnSc2	sigma
Olomouc	Olomouc	k1gFnSc1	Olomouc
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgInPc4	svůj
počátky	počátek	k1gInPc4	počátek
ve	v	k7c6	v
skromných	skromný	k2eAgInPc6d1	skromný
poměrech	poměr	k1gInPc6	poměr
předměstského	předměstský	k2eAgInSc2d1	předměstský
klubu	klub	k1gInSc2	klub
z	z	k7c2	z
Hejčína	Hejčín	k1gInSc2	Hejčín
<g/>
.	.	kIx.	.
</s>
<s>
Klub	klub	k1gInSc1	klub
byl	být	k5eAaImAgInS	být
pod	pod	k7c7	pod
původním	původní	k2eAgInSc7d1	původní
názvem	název	k1gInSc7	název
Fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
klub	klub	k1gInSc1	klub
Hejčín	Hejčín	k1gInSc1	Hejčín
Olomouc	Olomouc	k1gFnSc1	Olomouc
založen	založen	k2eAgInSc1d1	založen
roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
a	a	k8xC	a
u	u	k7c2	u
jeho	on	k3xPp3gInSc2	on
vzniku	vznik	k1gInSc2	vznik
stál	stát	k5eAaImAgInS	stát
tehdy	tehdy	k6eAd1	tehdy
šestnáctiletý	šestnáctiletý	k2eAgMnSc1d1	šestnáctiletý
chlapec	chlapec	k1gMnSc1	chlapec
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Tatíček	tatíček	k1gMnSc1	tatíček
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stal	stát	k5eAaPmAgInS	stát
i	i	k9	i
jeho	jeho	k3xOp3gInSc1	jeho
prvním	první	k4xOgMnSc7	první
předsedou	předseda	k1gMnSc7	předseda
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc4	první
utkání	utkání	k1gNnSc4	utkání
odehrál	odehrát	k5eAaPmAgMnS	odehrát
FK	FK	kA	FK
Hejčín	Hejčín	k1gMnSc1	Hejčín
proti	proti	k7c3	proti
týmu	tým	k1gInSc3	tým
Studentská	studentský	k2eAgFnSc1d1	studentská
jedenáctka	jedenáctka	k1gFnSc1	jedenáctka
a	a	k8xC	a
prohrál	prohrát	k5eAaPmAgMnS	prohrát
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
vysoko	vysoko	k6eAd1	vysoko
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
konkurenční	konkurenční	k2eAgFnSc2d1	konkurenční
Hejčínské	Hejčínský	k2eAgFnSc2d1	Hejčínská
jedenáctky	jedenáctka	k1gFnSc2	jedenáctka
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
sloučení	sloučení	k1gNnSc3	sloučení
obou	dva	k4xCgInPc2	dva
týmů	tým	k1gInPc2	tým
a	a	k8xC	a
přejmenování	přejmenování	k1gNnSc4	přejmenování
celku	celek	k1gInSc2	celek
na	na	k7c6	na
SK	Sk	kA	Sk
Hejčín	Hejčína	k1gFnPc2	Hejčína
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc4	první
opravdové	opravdový	k2eAgInPc4d1	opravdový
dresy	dres	k1gInPc4	dres
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgFnP	být
černé	černá	k1gFnPc4	černá
s	s	k7c7	s
bílými	bílý	k2eAgInPc7d1	bílý
límci	límec	k1gInPc7	límec
a	a	k8xC	a
manžetami	manžeta	k1gFnPc7	manžeta
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgInS	získat
tým	tým	k1gInSc1	tým
díky	díky	k7c3	díky
podpoře	podpora	k1gFnSc3	podpora
klubového	klubový	k2eAgMnSc2d1	klubový
sponzora	sponzor	k1gMnSc2	sponzor
<g/>
,	,	kIx,	,
legionáře	legionář	k1gMnSc2	legionář
Aloise	Alois	k1gMnSc2	Alois
Trefila	trefit	k5eAaPmAgFnS	trefit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
schválil	schválit	k5eAaPmAgInS	schválit
zemský	zemský	k2eAgInSc1d1	zemský
úřad	úřad	k1gInSc1	úřad
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
stanovy	stanova	k1gFnSc2	stanova
klubu	klub	k1gInSc2	klub
a	a	k8xC	a
Alois	Alois	k1gMnSc1	Alois
Trefil	trefit	k5eAaPmAgMnS	trefit
byl	být	k5eAaImAgMnS	být
následně	následně	k6eAd1	následně
zvolen	zvolit	k5eAaPmNgMnS	zvolit
jeho	jeho	k3xOp3gMnSc7	jeho
předsedou	předseda	k1gMnSc7	předseda
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
své	svůj	k3xOyFgNnSc4	svůj
původní	původní	k2eAgNnSc4d1	původní
hřiště	hřiště	k1gNnSc4	hřiště
využíval	využívat	k5eAaPmAgInS	využívat
tým	tým	k1gInSc1	tým
nerovný	rovný	k2eNgInSc4d1	nerovný
plácek	plácek	k1gInSc4	plácek
na	na	k7c6	na
vojenském	vojenský	k2eAgNnSc6d1	vojenské
cvičišti	cvičiště	k1gNnSc6	cvičiště
Na	na	k7c6	na
Šibeníku	šibeník	k1gMnSc6	šibeník
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
pozdější	pozdní	k2eAgNnPc4d2	pozdější
mistrovská	mistrovský	k2eAgNnPc4d1	mistrovské
utkání	utkání	k1gNnPc4	utkání
však	však	k8xC	však
musel	muset	k5eAaImAgMnS	muset
svá	svůj	k3xOyFgNnPc4	svůj
domácí	domácí	k2eAgNnPc4d1	domácí
utkání	utkání	k1gNnPc4	utkání
hrát	hrát	k5eAaImF	hrát
na	na	k7c6	na
pronajmutých	pronajmutý	k2eAgNnPc6d1	pronajmuté
hřištích	hřiště	k1gNnPc6	hřiště
a	a	k8xC	a
příležitostně	příležitostně	k6eAd1	příležitostně
proto	proto	k8xC	proto
černobílí	černobílý	k2eAgMnPc1d1	černobílý
hráli	hrát	k5eAaImAgMnP	hrát
i	i	k9	i
na	na	k7c6	na
Andrově	Andrův	k2eAgInSc6d1	Andrův
stadionu	stadion	k1gInSc6	stadion
<g/>
.	.	kIx.	.
</s>
<s>
Růst	růst	k1gInSc1	růst
klubu	klub	k1gInSc2	klub
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
pozvolný	pozvolný	k2eAgMnSc1d1	pozvolný
a	a	k8xC	a
v	v	k7c6	v
prvním	první	k4xOgNnSc6	první
půlstoletí	půlstoletí	k1gNnSc6	půlstoletí
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgMnS	účastnit
nižších	nízký	k2eAgFnPc2d2	nižší
soutěží	soutěž	k1gFnPc2	soutěž
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
jeho	jeho	k3xOp3gInPc1	jeho
vzestupy	vzestup	k1gInPc1	vzestup
byly	být	k5eAaImAgInP	být
pravidelně	pravidelně	k6eAd1	pravidelně
doprovázeny	doprovázen	k2eAgInPc4d1	doprovázen
následnými	následný	k2eAgInPc7d1	následný
pády	pád	k1gInPc7	pád
<g/>
.	.	kIx.	.
</s>
<s>
SK	Sk	kA	Sk
Hejčín	Hejčín	k1gInSc1	Hejčín
se	se	k3xPyFc4	se
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
počátcích	počátek	k1gInPc6	počátek
nejčastěji	často	k6eAd3	často
účastnil	účastnit	k5eAaImAgMnS	účastnit
župní	župní	k2eAgFnPc4d1	župní
třídy	třída	k1gFnPc4	třída
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
do	do	k7c2	do
I.	I.	kA	I.
B	B	kA	B
třídy	třída	k1gFnSc2	třída
se	se	k3xPyFc4	se
týmu	tým	k1gInSc3	tým
podařilo	podařit	k5eAaPmAgNnS	podařit
probojovat	probojovat	k5eAaPmF	probojovat
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
třicátých	třicátý	k4xOgNnPc2	třicátý
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
klubu	klub	k1gInSc2	klub
zakoupil	zakoupit	k5eAaPmAgMnS	zakoupit
pozemek	pozemek	k1gInSc4	pozemek
v	v	k7c6	v
Řepčíně	Řepčína	k1gFnSc6	Řepčína
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
následně	následně	k6eAd1	následně
vybudoval	vybudovat	k5eAaPmAgMnS	vybudovat
vlastní	vlastní	k2eAgInSc4d1	vlastní
stadion	stadion	k1gInSc4	stadion
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
v	v	k7c6	v
mistrovském	mistrovský	k2eAgInSc6d1	mistrovský
zápase	zápas	k1gInSc6	zápas
na	na	k7c6	na
vlastním	vlastní	k2eAgNnSc6d1	vlastní
hřišti	hřiště	k1gNnSc6	hřiště
utkali	utkat	k5eAaPmAgMnP	utkat
hráči	hráč	k1gMnPc1	hráč
SK	Sk	kA	Sk
Hejčín	Hejčín	k1gMnSc1	Hejčín
11	[number]	k4	11
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1946	[number]	k4	1946
s	s	k7c7	s
týmem	tým	k1gInSc7	tým
Moravan	Moravan	k1gMnSc1	Moravan
Prostějov	Prostějov	k1gInSc1	Prostějov
<g/>
,	,	kIx,	,
kterému	který	k3yRgInSc3	který
podlehli	podlehnout	k5eAaPmAgMnP	podlehnout
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vzestup	vzestup	k1gInSc4	vzestup
mezi	mezi	k7c4	mezi
elitu	elita	k1gFnSc4	elita
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
spojení	spojení	k1gNnSc3	spojení
s	s	k7c7	s
DTJ	DTJ	kA	DTJ
Hejčín	Hejčín	k1gInSc1	Hejčín
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
byl	být	k5eAaImAgInS	být
klub	klub	k1gInSc1	klub
přejmenován	přejmenovat	k5eAaPmNgInS	přejmenovat
dle	dle	k7c2	dle
svého	své	k1gNnSc2	své
nového	nový	k2eAgMnSc2d1	nový
hlavního	hlavní	k2eAgMnSc2d1	hlavní
sponzora	sponzor	k1gMnSc2	sponzor
<g/>
,	,	kIx,	,
Moravských	moravský	k2eAgFnPc2d1	Moravská
železáren	železárna	k1gFnPc2	železárna
<g/>
,	,	kIx,	,
na	na	k7c4	na
Hejčínský	Hejčínský	k2eAgInSc4d1	Hejčínský
sportovní	sportovní	k2eAgInSc4d1	sportovní
klub	klub	k1gInSc4	klub
Báňské	báňský	k2eAgFnPc1d1	báňská
a	a	k8xC	a
Hutní	hutní	k2eAgFnPc1d1	hutní
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
pak	pak	k6eAd1	pak
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
přejmenování	přejmenování	k1gNnSc3	přejmenování
na	na	k7c4	na
Základní	základní	k2eAgFnPc4d1	základní
sportovní	sportovní	k2eAgFnSc4d1	sportovní
jednotu	jednota	k1gFnSc4	jednota
Moravské	moravský	k2eAgFnSc2d1	Moravská
železárny	železárna	k1gFnSc2	železárna
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
finanční	finanční	k2eAgFnSc2d1	finanční
podpory	podpora	k1gFnSc2	podpora
Moravských	moravský	k2eAgFnPc2d1	Moravská
železáren	železárna	k1gFnPc2	železárna
přispěl	přispět	k5eAaPmAgInS	přispět
k	k	k7c3	k
zesílení	zesílení	k1gNnSc3	zesílení
klubu	klub	k1gInSc2	klub
také	také	k9	také
zánik	zánik	k1gInSc1	zánik
konkurenčního	konkurenční	k2eAgNnSc2d1	konkurenční
SK	Sk	kA	Sk
Olomouc	Olomouc	k1gFnSc1	Olomouc
ASO	ASO	kA	ASO
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
tým	tým	k1gInSc1	tým
posílilo	posílit	k5eAaPmAgNnS	posílit
množství	množství	k1gNnSc3	množství
zkušených	zkušený	k2eAgMnPc2d1	zkušený
hráčů	hráč	k1gMnPc2	hráč
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
vyhrálo	vyhrát	k5eAaPmAgNnS	vyhrát
první	první	k4xOgNnSc1	první
mužstvo	mužstvo	k1gNnSc1	mužstvo
s	s	k7c7	s
desetibodovým	desetibodový	k2eAgInSc7d1	desetibodový
náskokem	náskok	k1gInSc7	náskok
krajský	krajský	k2eAgInSc1d1	krajský
přebor	přebor	k1gInSc1	přebor
a	a	k8xC	a
poprvé	poprvé	k6eAd1	poprvé
postoupilo	postoupit	k5eAaPmAgNnS	postoupit
do	do	k7c2	do
divize	divize	k1gFnSc2	divize
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
opětovném	opětovný	k2eAgInSc6d1	opětovný
propadu	propad	k1gInSc6	propad
do	do	k7c2	do
krajského	krajský	k2eAgInSc2d1	krajský
přeboru	přebor	k1gInSc2	přebor
se	se	k3xPyFc4	se
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
1962	[number]	k4	1962
<g/>
/	/	kIx~	/
<g/>
1963	[number]	k4	1963
olomouckému	olomoucký	k2eAgMnSc3d1	olomoucký
týmu	tým	k1gInSc3	tým
podařilo	podařit	k5eAaPmAgNnS	podařit
zvítězit	zvítězit	k5eAaPmF	zvítězit
a	a	k8xC	a
poprvé	poprvé	k6eAd1	poprvé
postoupit	postoupit	k5eAaPmF	postoupit
do	do	k7c2	do
II	II	kA	II
<g/>
.	.	kIx.	.
ligy	liga	k1gFnSc2	liga
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
udržel	udržet	k5eAaPmAgMnS	udržet
dvě	dva	k4xCgFnPc4	dva
sezóny	sezóna	k1gFnPc4	sezóna
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
se	se	k3xPyFc4	se
novým	nový	k2eAgMnSc7d1	nový
sponzorem	sponzor	k1gMnSc7	sponzor
klubu	klub	k1gInSc2	klub
stal	stát	k5eAaPmAgMnS	stát
koncern	koncern	k1gInSc4	koncern
Sigma	sigma	k1gNnSc2	sigma
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
spolupodílel	spolupodílet	k5eAaImAgInS	spolupodílet
na	na	k7c6	na
dalším	další	k2eAgInSc6d1	další
vzestupu	vzestup	k1gInSc6	vzestup
mužstva	mužstvo	k1gNnSc2	mužstvo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
tohoto	tento	k3xDgInSc2	tento
roku	rok	k1gInSc2	rok
byl	být	k5eAaImAgInS	být
klub	klub	k1gInSc1	klub
znám	znám	k2eAgInSc1d1	znám
jako	jako	k8xC	jako
TJ	tj	kA	tj
Sigma	sigma	k1gNnPc2	sigma
MŽ	MŽ	kA	MŽ
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
se	s	k7c7	s
trenérem	trenér	k1gMnSc7	trenér
týmu	tým	k1gInSc2	tým
stal	stát	k5eAaPmAgMnS	stát
olomoucký	olomoucký	k2eAgMnSc1d1	olomoucký
odchovanec	odchovanec	k1gMnSc1	odchovanec
a	a	k8xC	a
bývalý	bývalý	k2eAgMnSc1d1	bývalý
hráč	hráč	k1gMnSc1	hráč
Moravských	moravský	k2eAgFnPc2d1	Moravská
železáren	železárna	k1gFnPc2	železárna
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Brückner	Brückner	k1gMnSc1	Brückner
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
jeho	jeho	k3xOp3gNnSc7	jeho
vedením	vedení	k1gNnSc7	vedení
se	se	k3xPyFc4	se
olomouckým	olomoucký	k2eAgMnSc7d1	olomoucký
hráčům	hráč	k1gMnPc3	hráč
podařilo	podařit	k5eAaPmAgNnS	podařit
v	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
ročníku	ročník	k1gInSc6	ročník
opět	opět	k6eAd1	opět
probojovat	probojovat	k5eAaPmF	probojovat
do	do	k7c2	do
III	III	kA	III
<g/>
.	.	kIx.	.
ligy	liga	k1gFnSc2	liga
a	a	k8xC	a
po	po	k7c6	po
reorganizaci	reorganizace	k1gFnSc6	reorganizace
domácích	domácí	k2eAgFnPc2d1	domácí
soutěží	soutěž	k1gFnPc2	soutěž
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
Olomouc	Olomouc	k1gFnSc1	Olomouc
probojovala	probojovat	k5eAaPmAgFnS	probojovat
do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
domácí	domácí	k2eAgFnSc2d1	domácí
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
,	,	kIx,	,
I.	I.	kA	I.
české	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
ligy	liga	k1gFnSc2	liga
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnPc1d1	původní
černobílé	černobílý	k2eAgFnPc1d1	černobílá
hejčínské	hejčínský	k2eAgFnPc1d1	hejčínská
barvy	barva	k1gFnPc1	barva
klubu	klub	k1gInSc2	klub
tehdy	tehdy	k6eAd1	tehdy
nahradila	nahradit	k5eAaPmAgFnS	nahradit
modrobílá	modrobílý	k2eAgFnSc1d1	modrobílá
kombinace	kombinace	k1gFnSc1	kombinace
a	a	k8xC	a
první	první	k4xOgInPc4	první
tým	tým	k1gInSc4	tým
se	se	k3xPyFc4	se
ze	z	k7c2	z
stadionu	stadion	k1gInSc2	stadion
v	v	k7c6	v
Řepčíně	Řepčína	k1gFnSc6	Řepčína
definitivně	definitivně	k6eAd1	definitivně
přesunul	přesunout	k5eAaPmAgMnS	přesunout
na	na	k7c4	na
bývalý	bývalý	k2eAgInSc4d1	bývalý
ligový	ligový	k2eAgInSc4d1	ligový
stadion	stadion	k1gInSc4	stadion
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
klub	klub	k1gInSc4	klub
tehdy	tehdy	k6eAd1	tehdy
nastupovali	nastupovat	k5eAaImAgMnP	nastupovat
hráči	hráč	k1gMnPc1	hráč
jako	jako	k8xS	jako
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
Palička	Palička	k1gMnSc1	Palička
<g/>
,	,	kIx,	,
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
Petržela	Petržela	k1gMnSc1	Petržela
<g/>
,	,	kIx,	,
Ivo	Ivo	k1gMnSc1	Ivo
Knoflíček	knoflíček	k1gInSc1	knoflíček
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Vít	Vít	k1gMnSc1	Vít
nebo	nebo	k8xC	nebo
bratři	bratr	k1gMnPc1	bratr
Fialové	Fialová	k1gFnSc2	Fialová
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
jména	jméno	k1gNnPc1	jméno
už	už	k6eAd1	už
přislibovala	přislibovat	k5eAaPmAgFnS	přislibovat
historický	historický	k2eAgInSc4d1	historický
úspěch	úspěch	k1gInSc4	úspěch
klubu	klub	k1gInSc2	klub
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
přišel	přijít	k5eAaPmAgInS	přijít
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
trenér	trenér	k1gMnSc1	trenér
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
Zeman	Zeman	k1gMnSc1	Zeman
dovedl	dovést	k5eAaPmAgMnS	dovést
mužstvo	mužstvo	k1gNnSc4	mužstvo
do	do	k7c2	do
1	[number]	k4	1
<g/>
.	.	kIx.	.
ligy	liga	k1gFnSc2	liga
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
premiérovém	premiérový	k2eAgInSc6d1	premiérový
zápase	zápas	k1gInSc6	zápas
v	v	k7c6	v
první	první	k4xOgFnSc6	první
lize	liga	k1gFnSc6	liga
remizovala	remizovat	k5eAaPmAgFnS	remizovat
Sigma	sigma	k1gNnSc4	sigma
22	[number]	k4	22
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1982	[number]	k4	1982
na	na	k7c6	na
domácím	domácí	k2eAgInSc6d1	domácí
stadionu	stadion	k1gInSc6	stadion
s	s	k7c7	s
Nitrou	Nitra	k1gFnSc7	Nitra
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
sezónu	sezóna	k1gFnSc4	sezóna
doplatila	doplatit	k5eAaPmAgFnS	doplatit
TJ	tj	kA	tj
Sigma	sigma	k1gNnPc2	sigma
na	na	k7c4	na
prvoligovou	prvoligový	k2eAgFnSc4d1	prvoligová
nezkušenost	nezkušenost	k1gFnSc4	nezkušenost
a	a	k8xC	a
na	na	k7c4	na
rok	rok	k1gInSc4	rok
se	se	k3xPyFc4	se
vrátila	vrátit	k5eAaPmAgFnS	vrátit
do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
ligy	liga	k1gFnSc2	liga
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
v	v	k7c6	v
příští	příští	k2eAgFnSc6d1	příští
sezóně	sezóna	k1gFnSc6	sezóna
se	se	k3xPyFc4	se
však	však	k9	však
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
vedená	vedený	k2eAgFnSc1d1	vedená
znovu	znovu	k6eAd1	znovu
trenérem	trenér	k1gMnSc7	trenér
Karlem	Karel	k1gMnSc7	Karel
Brücknerem	Brückner	k1gMnSc7	Brückner
<g/>
,	,	kIx,	,
vrátila	vrátit	k5eAaPmAgFnS	vrátit
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Výrazně	výrazně	k6eAd1	výrazně
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
pod	pod	k7c4	pod
její	její	k3xOp3gInSc4	její
návrat	návrat	k1gInSc4	návrat
mezi	mezi	k7c4	mezi
elitu	elita	k1gFnSc4	elita
podepsal	podepsat	k5eAaPmAgInS	podepsat
svými	svůj	k3xOyFgInPc7	svůj
góly	gól	k1gInPc7	gól
Vladislav	Vladislav	k1gMnSc1	Vladislav
Lauda	Lauda	k1gMnSc1	Lauda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1984	[number]	k4	1984
až	až	k9	až
1987	[number]	k4	1987
pak	pak	k6eAd1	pak
v	v	k7c6	v
útoku	útok	k1gInSc6	útok
Sigmy	Sigma	k1gFnSc2	Sigma
vynikal	vynikat	k5eAaImAgMnS	vynikat
také	také	k9	také
útočník	útočník	k1gMnSc1	útočník
Miroslav	Miroslav	k1gMnSc1	Miroslav
Příložný	příložný	k2eAgMnSc1d1	příložný
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
klubové	klubový	k2eAgFnPc4d1	klubová
legendy	legenda	k1gFnPc4	legenda
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
za	za	k7c2	za
Sigmy	Sigma	k1gFnSc2	Sigma
hrály	hrát	k5eAaImAgInP	hrát
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
rovněž	rovněž	k9	rovněž
Jan	Jan	k1gMnSc1	Jan
Maroši	Maroš	k1gMnPc7	Maroš
a	a	k8xC	a
Oldřich	Oldřich	k1gMnSc1	Oldřich
Machala	Machala	k1gMnSc1	Machala
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
v	v	k7c6	v
olomouckém	olomoucký	k2eAgInSc6d1	olomoucký
dresu	dres	k1gInSc6	dres
odehrál	odehrát	k5eAaPmAgMnS	odehrát
rekordních	rekordní	k2eAgFnPc2d1	rekordní
414	[number]	k4	414
ligových	ligový	k2eAgInPc2d1	ligový
zápasů	zápas	k1gInPc2	zápas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Třicet	třicet	k4xCc1	třicet
let	léto	k1gNnPc2	léto
v	v	k7c6	v
první	první	k4xOgFnSc6	první
lize	liga	k1gFnSc6	liga
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
druhém	druhý	k4xOgInSc6	druhý
postupu	postup	k1gInSc6	postup
do	do	k7c2	do
první	první	k4xOgFnSc2	první
ligy	liga	k1gFnSc2	liga
se	se	k3xPyFc4	se
Sigma	sigma	k1gNnSc1	sigma
Olomouc	Olomouc	k1gFnSc1	Olomouc
od	od	k7c2	od
ročníku	ročník	k1gInSc2	ročník
1984	[number]	k4	1984
<g/>
/	/	kIx~	/
<g/>
85	[number]	k4	85
napevno	napevno	k6eAd1	napevno
zabydlela	zabydlet	k5eAaPmAgNnP	zabydlet
mezi	mezi	k7c7	mezi
českou	český	k2eAgFnSc7d1	Česká
fotbalovou	fotbalový	k2eAgFnSc7d1	fotbalová
elitou	elita	k1gFnSc7	elita
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nepřetržitě	přetržitě	k6eNd1	přetržitě
držela	držet	k5eAaImAgFnS	držet
po	po	k7c4	po
třicet	třicet	k4xCc4	třicet
let	léto	k1gNnPc2	léto
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
přibývajícími	přibývající	k2eAgFnPc7d1	přibývající
zkušenostmi	zkušenost	k1gFnPc7	zkušenost
přišly	přijít	k5eAaPmAgFnP	přijít
i	i	k9	i
první	první	k4xOgInPc4	první
úspěchy	úspěch	k1gInPc4	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Čtyři	čtyři	k4xCgNnPc4	čtyři
třetí	třetí	k4xOgNnPc4	třetí
místa	místo	k1gNnPc4	místo
v	v	k7c6	v
sezónách	sezóna	k1gFnPc6	sezóna
1990	[number]	k4	1990
<g/>
/	/	kIx~	/
<g/>
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
/	/	kIx~	/
<g/>
1998	[number]	k4	1998
a	a	k8xC	a
2000	[number]	k4	2000
<g/>
/	/	kIx~	/
<g/>
2001	[number]	k4	2001
a	a	k8xC	a
jedno	jeden	k4xCgNnSc1	jeden
druhé	druhý	k4xOgNnSc4	druhý
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
1995	[number]	k4	1995
<g/>
/	/	kIx~	/
<g/>
1996	[number]	k4	1996
to	ten	k3xDgNnSc1	ten
jasně	jasně	k6eAd1	jasně
dokumentují	dokumentovat	k5eAaBmIp3nP	dokumentovat
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
v	v	k7c6	v
pohárových	pohárový	k2eAgFnPc6d1	pohárová
soutěžích	soutěž	k1gFnPc6	soutěž
se	s	k7c7	s
Sigma	sigma	k1gNnSc7	sigma
neztratila	ztratit	k5eNaPmAgFnS	ztratit
<g/>
.	.	kIx.	.
</s>
<s>
Premiérový	premiérový	k2eAgInSc4d1	premiérový
pohárový	pohárový	k2eAgInSc4d1	pohárový
zápas	zápas	k1gInSc4	zápas
odehrála	odehrát	k5eAaPmAgFnS	odehrát
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
stadionu	stadion	k1gInSc6	stadion
17	[number]	k4	17
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
prvního	první	k4xOgNnSc2	první
kola	kolo	k1gNnSc2	kolo
poháru	pohár	k1gInSc2	pohár
UEFA	UEFA	kA	UEFA
střetla	střetnout	k5eAaPmAgFnS	střetnout
s	s	k7c7	s
švédským	švédský	k2eAgNnSc7d1	švédské
IFK	IFK	kA	IFK
Göteborg	Göteborg	k1gInSc1	Göteborg
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
pozdějším	pozdní	k2eAgMnSc7d2	pozdější
vítězem	vítěz	k1gMnSc7	vítěz
soutěže	soutěž	k1gFnSc2	soutěž
tehdy	tehdy	k6eAd1	tehdy
remizovala	remizovat	k5eAaPmAgFnS	remizovat
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
osminásobné	osminásobný	k2eAgFnSc6d1	osminásobná
účasti	účast	k1gFnSc6	účast
v	v	k7c6	v
poháru	pohár	k1gInSc6	pohár
UEFA	UEFA	kA	UEFA
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
Sigma	sigma	k1gNnSc4	sigma
největší	veliký	k2eAgInSc1d3	veliký
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
úspěch	úspěch	k1gInSc1	úspěch
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
1991	[number]	k4	1991
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
dokázala	dokázat	k5eAaPmAgFnS	dokázat
přes	přes	k7c4	přes
týmy	tým	k1gInPc4	tým
Bangor	Bangor	k1gMnSc1	Bangor
City	City	k1gFnSc2	City
FC	FC	kA	FC
<g/>
,	,	kIx,	,
FK	FK	kA	FK
Torpedo	Torpedo	k1gNnSc4	Torpedo
Moskva	Moskva	k1gFnSc1	Moskva
a	a	k8xC	a
Hamburger	hamburger	k1gInSc1	hamburger
SV	sv	kA	sv
dostat	dostat	k5eAaPmF	dostat
až	až	k9	až
do	do	k7c2	do
čtvrtfinále	čtvrtfinále	k1gNnSc2	čtvrtfinále
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
Realu	Real	k1gInSc6	Real
Madrid	Madrid	k1gInSc1	Madrid
po	po	k7c6	po
vyrovnaných	vyrovnaný	k2eAgInPc6d1	vyrovnaný
výsledcích	výsledek	k1gInPc6	výsledek
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
a	a	k8xC	a
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
úspěchu	úspěch	k1gInSc2	úspěch
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
tým	tým	k1gInSc4	tým
rovněž	rovněž	k9	rovněž
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
v	v	k7c6	v
poháru	pohár	k1gInSc6	pohár
Intertoto	Intertota	k1gFnSc5	Intertota
Sigma	sigma	k1gNnSc1	sigma
probojovala	probojovat	k5eAaPmAgFnS	probojovat
až	až	k9	až
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
však	však	k9	však
v	v	k7c6	v
dvojzápase	dvojzápas	k1gInSc6	dvojzápas
nestačila	stačit	k5eNaBmAgFnS	stačit
na	na	k7c4	na
italské	italský	k2eAgFnPc4d1	italská
Udinese	Udinese	k1gFnPc4	Udinese
Calcio	Calcio	k6eAd1	Calcio
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
naposledy	naposledy	k6eAd1	naposledy
se	s	k7c7	s
Sigma	sigma	k1gNnSc7	sigma
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
evropských	evropský	k2eAgInPc2d1	evropský
pohárů	pohár	k1gInPc2	pohár
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2009	[number]	k4	2009
<g/>
/	/	kIx~	/
<g/>
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
trenéra	trenér	k1gMnSc2	trenér
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Psotky	Psotka	k1gMnSc2	Psotka
postoupila	postoupit	k5eAaPmAgFnS	postoupit
přes	přes	k7c4	přes
druhé	druhý	k4xOgNnSc4	druhý
a	a	k8xC	a
třetí	třetí	k4xOgNnSc4	třetí
předkolo	předkolo	k1gNnSc4	předkolo
do	do	k7c2	do
Play-off	Playff	k1gInSc4	Play-off
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
zde	zde	k6eAd1	zde
vypadla	vypadnout	k5eAaPmAgFnS	vypadnout
s	s	k7c7	s
anglickým	anglický	k2eAgInSc7d1	anglický
týmem	tým	k1gInSc7	tým
Everton	Everton	k1gInSc1	Everton
FC	FC	kA	FC
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dresu	dres	k1gInSc6	dres
olomoucké	olomoucký	k2eAgFnSc2d1	olomoucká
Sigmy	Sigma	k1gFnSc2	Sigma
nastupovala	nastupovat	k5eAaImAgFnS	nastupovat
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
a	a	k8xC	a
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
řada	řada	k1gFnSc1	řada
úspěšných	úspěšný	k2eAgMnPc2d1	úspěšný
hráčů	hráč	k1gMnPc2	hráč
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
prosadili	prosadit	k5eAaPmAgMnP	prosadit
i	i	k9	i
v	v	k7c6	v
zahraničích	zahraničí	k2eAgNnPc6d1	zahraničí
družstvech	družstvo	k1gNnPc6	družstvo
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
jmenovat	jmenovat	k5eAaBmF	jmenovat
odchovance	odchovanec	k1gMnPc4	odchovanec
nebo	nebo	k8xC	nebo
hráče	hráč	k1gMnPc4	hráč
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
prošli	projít	k5eAaPmAgMnP	projít
mládežnickými	mládežnický	k2eAgInPc7d1	mládežnický
týmy	tým	k1gInPc7	tým
klubu	klub	k1gInSc2	klub
<g/>
,	,	kIx,	,
jakými	jaký	k3yQgFnPc7	jaký
byli	být	k5eAaImAgMnP	být
například	například	k6eAd1	například
Pavel	Pavel	k1gMnSc1	Pavel
Hapal	Hapal	k1gMnSc1	Hapal
<g/>
,	,	kIx,	,
Radoslav	Radoslav	k1gMnSc1	Radoslav
Látal	Látal	k1gMnSc1	Látal
<g/>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
Ujfaluši	Ujfaluše	k1gFnSc4	Ujfaluše
<g/>
,	,	kIx,	,
Marek	Marek	k1gMnSc1	Marek
Heinz	Heinz	k1gMnSc1	Heinz
nebo	nebo	k8xC	nebo
David	David	k1gMnSc1	David
Kobylík	Kobylík	k1gMnSc1	Kobylík
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
Sigmu	Sigma	k1gFnSc4	Sigma
Olomouc	Olomouc	k1gFnSc4	Olomouc
nastupovali	nastupovat	k5eAaImAgMnP	nastupovat
také	také	k9	také
mnozí	mnohý	k2eAgMnPc1d1	mnohý
reprezentanti	reprezentant	k1gMnPc1	reprezentant
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Karel	Karel	k1gMnSc1	Karel
Rada	Rada	k1gMnSc1	Rada
<g/>
,	,	kIx,	,
Radek	Radek	k1gMnSc1	Radek
Drulák	Drulák	k1gMnSc1	Drulák
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
Rozehnal	Rozehnal	k1gMnSc1	Rozehnal
<g/>
,	,	kIx,	,
Radoslav	Radoslav	k1gMnSc1	Radoslav
Kováč	Kováč	k1gMnSc1	Kováč
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
Vlček	Vlček	k1gMnSc1	Vlček
<g/>
,	,	kIx,	,
Roman	Roman	k1gMnSc1	Roman
Hubník	hubník	k1gInSc1	hubník
či	či	k8xC	či
brankář	brankář	k1gMnSc1	brankář
Martin	Martin	k1gMnSc1	Martin
Vaniak	Vaniak	k1gMnSc1	Vaniak
<g/>
.	.	kIx.	.
</s>
<s>
Nejúspěšnějším	úspěšný	k2eAgMnSc7d3	nejúspěšnější
olomouckým	olomoucký	k2eAgMnSc7d1	olomoucký
trenérem	trenér	k1gMnSc7	trenér
je	být	k5eAaImIp3nS	být
Karel	Karel	k1gMnSc1	Karel
Brückner	Brückner	k1gMnSc1	Brückner
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
tým	tým	k1gInSc4	tým
vedl	vést	k5eAaImAgMnS	vést
v	v	k7c6	v
247	[number]	k4	247
ligových	ligový	k2eAgInPc6d1	ligový
zápasech	zápas	k1gInPc6	zápas
<g/>
,	,	kIx,	,
povedlo	povést	k5eAaPmAgNnS	povést
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
s	s	k7c7	s
klubem	klub	k1gInSc7	klub
umístit	umístit	k5eAaPmF	umístit
se	se	k3xPyFc4	se
na	na	k7c6	na
druhém	druhý	k4xOgNnSc6	druhý
a	a	k8xC	a
dvakrát	dvakrát	k6eAd1	dvakrát
na	na	k7c6	na
třetím	třetí	k4xOgNnSc6	třetí
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
nejvyšší	vysoký	k2eAgFnSc6d3	nejvyšší
lize	liga	k1gFnSc6	liga
a	a	k8xC	a
Olomouc	Olomouc	k1gFnSc1	Olomouc
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
jeho	jeho	k3xOp3gNnSc7	jeho
vedením	vedení	k1gNnSc7	vedení
probojovala	probojovat	k5eAaPmAgFnS	probojovat
do	do	k7c2	do
čtvrtfinále	čtvrtfinále	k1gNnSc2	čtvrtfinále
poháru	pohár	k1gInSc2	pohár
UEFA	UEFA	kA	UEFA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Zisk	zisk	k1gInSc1	zisk
prvních	první	k4xOgFnPc2	první
trofejí	trofej	k1gFnPc2	trofej
====	====	k?	====
</s>
</p>
<p>
<s>
Po	po	k7c6	po
vítězství	vítězství	k1gNnSc6	vítězství
v	v	k7c4	v
zimní	zimní	k2eAgInSc4d1	zimní
Tipsport	Tipsport	k1gInSc4	Tipsport
lize	liga	k1gFnSc6	liga
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
získala	získat	k5eAaPmAgFnS	získat
Sigma	sigma	k1gNnPc2	sigma
Olomouc	Olomouc	k1gFnSc4	Olomouc
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
první	první	k4xOgFnSc4	první
velkou	velký	k2eAgFnSc4d1	velká
domácí	domácí	k2eAgFnSc4d1	domácí
trofej	trofej	k1gFnSc4	trofej
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2011	[number]	k4	2011
<g/>
/	/	kIx~	/
<g/>
12	[number]	k4	12
dokázala	dokázat	k5eAaPmAgFnS	dokázat
vyhrát	vyhrát	k5eAaPmF	vyhrát
domácí	domácí	k2eAgInSc4d1	domácí
pohár	pohár	k1gInSc4	pohár
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
se	s	k7c7	s
2	[number]	k4	2
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2012	[number]	k4	2012
utkala	utkat	k5eAaPmAgFnS	utkat
na	na	k7c6	na
stadionu	stadion	k1gInSc6	stadion
ve	v	k7c6	v
Štruncových	Štruncových	k2eAgInPc6d1	Štruncových
sadech	sad	k1gInPc6	sad
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
se	s	k7c7	s
Spartou	Sparta	k1gFnSc7	Sparta
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
níž	jenž	k3xRgFnSc7	jenž
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
a	a	k8xC	a
získala	získat	k5eAaPmAgFnS	získat
tak	tak	k6eAd1	tak
poprvé	poprvé	k6eAd1	poprvé
putovní	putovní	k2eAgInSc1d1	putovní
Český	český	k2eAgInSc1d1	český
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
pohár	pohár	k1gInSc1	pohár
<g/>
.	.	kIx.	.
</s>
<s>
Jedinou	jediný	k2eAgFnSc4d1	jediná
branku	branka	k1gFnSc4	branka
utkání	utkání	k1gNnSc2	utkání
vsítil	vsítit	k5eAaPmAgMnS	vsítit
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
poločase	poločas	k1gInSc6	poločas
v	v	k7c6	v
47	[number]	k4	47
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
prudkou	prudký	k2eAgFnSc7d1	prudká
střelou	střela	k1gFnSc7	střela
z	z	k7c2	z
levé	levý	k2eAgFnSc2d1	levá
strany	strana	k1gFnSc2	strana
k	k	k7c3	k
pravé	pravý	k2eAgFnSc3d1	pravá
tyči	tyč	k1gFnSc3	tyč
Michal	Michal	k1gMnSc1	Michal
Vepřek	Vepřek	k1gInSc1	Vepřek
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
úspěchu	úspěch	k1gInSc2	úspěch
při	při	k7c6	při
zisku	zisk	k1gInSc6	zisk
domácího	domácí	k2eAgInSc2d1	domácí
poháru	pohár	k1gInSc2	pohár
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
Sigmě	Sigma	k1gFnSc3	Sigma
podařilo	podařit	k5eAaPmAgNnS	podařit
udržet	udržet	k5eAaPmF	udržet
se	se	k3xPyFc4	se
ve	v	k7c6	v
špatně	špatně	k6eAd1	špatně
rozehrané	rozehraný	k2eAgFnSc6d1	rozehraná
sezóně	sezóna	k1gFnSc6	sezóna
2011	[number]	k4	2011
<g/>
/	/	kIx~	/
<g/>
2012	[number]	k4	2012
v	v	k7c6	v
první	první	k4xOgFnSc6	první
lize	liga	k1gFnSc6	liga
<g/>
.	.	kIx.	.
</s>
<s>
Sigma	sigma	k1gNnSc1	sigma
Olomouc	Olomouc	k1gFnSc1	Olomouc
nakonec	nakonec	k6eAd1	nakonec
skončila	skončit	k5eAaPmAgFnS	skončit
na	na	k7c4	na
11	[number]	k4	11
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
tabulky	tabulka	k1gFnSc2	tabulka
i	i	k9	i
s	s	k7c7	s
odečtenými	odečtený	k2eAgNnPc7d1	odečtené
9	[number]	k4	9
body	bod	k1gInPc1	bod
<g/>
,	,	kIx,	,
kterými	který	k3yIgInPc7	který
ji	on	k3xPp3gFnSc4	on
fotbalová	fotbalový	k2eAgFnSc1d1	fotbalová
asociace	asociace	k1gFnSc1	asociace
potrestala	potrestat	k5eAaPmAgFnS	potrestat
za	za	k7c4	za
účast	účast	k1gFnSc4	účast
v	v	k7c6	v
korupční	korupční	k2eAgFnSc6d1	korupční
kauze	kauza	k1gFnSc6	kauza
s	s	k7c7	s
FK	FK	kA	FK
Bohemians	Bohemians	k1gInSc1	Bohemians
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Olomoucký	olomoucký	k2eAgMnSc1d1	olomoucký
trenér	trenér	k1gMnSc1	trenér
Petr	Petr	k1gMnSc1	Petr
Uličný	Uličný	k2eAgMnSc1d1	Uličný
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
po	po	k7c6	po
sezóně	sezóna	k1gFnSc6	sezóna
ukončil	ukončit	k5eAaPmAgInS	ukončit
svou	svůj	k3xOyFgFnSc4	svůj
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
trenérskou	trenérský	k2eAgFnSc4d1	trenérská
kariéru	kariéra	k1gFnSc4	kariéra
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
s	s	k7c7	s
Olomoucí	Olomouc	k1gFnSc7	Olomouc
získal	získat	k5eAaPmAgMnS	získat
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
trofej	trofej	k1gFnSc4	trofej
<g/>
.	.	kIx.	.
</s>
<s>
Radost	radost	k1gFnSc1	radost
z	z	k7c2	z
vítězství	vítězství	k1gNnSc2	vítězství
v	v	k7c6	v
domácím	domácí	k2eAgInSc6d1	domácí
poháru	pohár	k1gInSc6	pohár
však	však	k9	však
Olomouckým	olomoucký	k2eAgInSc7d1	olomoucký
pokazil	pokazit	k5eAaPmAgInS	pokazit
následný	následný	k2eAgInSc1d1	následný
verdikt	verdikt	k1gInSc1	verdikt
orgánů	orgán	k1gInPc2	orgán
UEFA	UEFA	kA	UEFA
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
kvůli	kvůli	k7c3	kvůli
nedořešené	dořešený	k2eNgFnSc3d1	nedořešená
kauze	kauza	k1gFnSc3	kauza
s	s	k7c7	s
FK	FK	kA	FK
Bohemians	Bohemians	k1gInSc1	Bohemians
Praha	Praha	k1gFnSc1	Praha
20	[number]	k4	20
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2012	[number]	k4	2012
zákazal	zákazat	k5eAaImAgInS	zákazat
Sigmě	Sigma	k1gFnSc3	Sigma
start	start	k1gInSc4	start
v	v	k7c6	v
předkole	předkolo	k1gNnSc6	předkolo
Evropské	evropský	k2eAgFnSc2d1	Evropská
ligy	liga	k1gFnSc2	liga
<g/>
.	.	kIx.	.
</s>
<s>
Ředitel	ředitel	k1gMnSc1	ředitel
olomouckého	olomoucký	k2eAgInSc2d1	olomoucký
klubu	klub	k1gInSc2	klub
Jaromír	Jaromír	k1gMnSc1	Jaromír
Gajda	Gajda	k1gMnSc1	Gajda
následně	následně	k6eAd1	následně
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
klub	klub	k1gInSc1	klub
proti	proti	k7c3	proti
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
neodvolá	odvolat	k5eNaPmIp3nS	odvolat
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
případnou	případný	k2eAgFnSc4d1	případná
účast	účast	k1gFnSc4	účast
v	v	k7c6	v
Evropské	evropský	k2eAgFnSc6d1	Evropská
lize	liga	k1gFnSc6	liga
pro	pro	k7c4	pro
další	další	k2eAgInSc4d1	další
český	český	k2eAgInSc4d1	český
klub	klub	k1gInSc4	klub
<g/>
,	,	kIx,	,
kterému	který	k3yRgMnSc3	který
mohla	moct	k5eAaImAgFnS	moct
Olomouc	Olomouc	k1gFnSc4	Olomouc
uvolnit	uvolnit	k5eAaPmF	uvolnit
pozici	pozice	k1gFnSc4	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
Sigmy	Sigma	k1gFnSc2	Sigma
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
do	do	k7c2	do
3	[number]	k4	3
<g/>
.	.	kIx.	.
předkola	předkolo	k1gNnSc2	předkolo
Evropské	evropský	k2eAgFnSc2d1	Evropská
ligy	liga	k1gFnSc2	liga
posunula	posunout	k5eAaPmAgFnS	posunout
pražská	pražský	k2eAgFnSc1d1	Pražská
Sparta	Sparta	k1gFnSc1	Sparta
a	a	k8xC	a
do	do	k7c2	do
2	[number]	k4	2
<g/>
.	.	kIx.	.
předkola	předkolo	k1gNnSc2	předkolo
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgInS	dostat
tým	tým	k1gInSc1	tým
ze	z	k7c2	z
4	[number]	k4	4
<g/>
.	.	kIx.	.
místa	místo	k1gNnSc2	místo
Gambrinus	gambrinus	k1gInSc4	gambrinus
ligy	liga	k1gFnSc2	liga
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2011	[number]	k4	2011
<g/>
/	/	kIx~	/
<g/>
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
FK	FK	kA	FK
Mladá	mladá	k1gFnSc1	mladá
Boleslav	Boleslav	k1gMnSc1	Boleslav
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Záhy	záhy	k6eAd1	záhy
po	po	k7c6	po
vítězství	vítězství	k1gNnSc6	vítězství
v	v	k7c6	v
domácím	domácí	k2eAgInSc6d1	domácí
poháru	pohár	k1gInSc6	pohár
získala	získat	k5eAaPmAgFnS	získat
Sigma	sigma	k1gNnPc2	sigma
další	další	k2eAgFnSc4d1	další
cennou	cenný	k2eAgFnSc4d1	cenná
trofej	trofej	k1gFnSc4	trofej
<g/>
,	,	kIx,	,
když	když	k8xS	když
20	[number]	k4	20
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2012	[number]	k4	2012
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
na	na	k7c6	na
stadionu	stadion	k1gInSc6	stadion
U	u	k7c2	u
Nisy	Nisa	k1gFnSc2	Nisa
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
nad	nad	k7c7	nad
mistrem	mistr	k1gMnSc7	mistr
Gambrinus	gambrinus	k1gInSc4	gambrinus
ligy	liga	k1gFnSc2	liga
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2011	[number]	k4	2011
<g/>
/	/	kIx~	/
<g/>
2012	[number]	k4	2012
Slovanem	Slovan	k1gInSc7	Slovan
Liberec	Liberec	k1gInSc1	Liberec
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
a	a	k8xC	a
získala	získat	k5eAaPmAgFnS	získat
tak	tak	k6eAd1	tak
poprvé	poprvé	k6eAd1	poprvé
Český	český	k2eAgInSc4d1	český
Superpohár	superpohár	k1gInSc4	superpohár
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
24	[number]	k4	24
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
vstřelil	vstřelit	k5eAaPmAgMnS	vstřelit
první	první	k4xOgInSc4	první
gól	gól	k1gInSc4	gól
zápasu	zápas	k1gInSc2	zápas
Martin	Martin	k1gMnSc1	Martin
Doležal	Doležal	k1gMnSc1	Doležal
<g/>
,	,	kIx,	,
o	o	k7c4	o
3	[number]	k4	3
minuty	minuta	k1gFnSc2	minuta
později	pozdě	k6eAd2	pozdě
fauloval	faulovat	k5eAaBmAgMnS	faulovat
v	v	k7c6	v
pokutovém	pokutový	k2eAgNnSc6d1	pokutové
území	území	k1gNnSc6	území
Liberce	Liberec	k1gInSc2	Liberec
Lukáš	Lukáš	k1gMnSc1	Lukáš
Vácha	Vácha	k1gMnSc1	Vácha
olomouckého	olomoucký	k2eAgMnSc2d1	olomoucký
Jana	Jan	k1gMnSc2	Jan
Navrátila	Navrátil	k1gMnSc2	Navrátil
a	a	k8xC	a
nařízený	nařízený	k2eAgInSc4d1	nařízený
pokutový	pokutový	k2eAgInSc4d1	pokutový
kop	kop	k1gInSc4	kop
proměnil	proměnit	k5eAaPmAgMnS	proměnit
Michal	Michal	k1gMnSc1	Michal
Ordoš	Ordoš	k1gMnSc1	Ordoš
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
této	tento	k3xDgFnSc2	tento
trofeje	trofej	k1gFnSc2	trofej
získala	získat	k5eAaPmAgFnS	získat
vítězná	vítězný	k2eAgFnSc1d1	vítězná
Sigma	sigma	k1gNnSc6	sigma
Olomouc	Olomouc	k1gFnSc1	Olomouc
navíc	navíc	k6eAd1	navíc
i	i	k8xC	i
prémii	prémie	k1gFnSc4	prémie
1	[number]	k4	1
000	[number]	k4	000
000	[number]	k4	000
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
poražený	poražený	k2eAgInSc4d1	poražený
FC	FC	kA	FC
Slovan	Slovan	k1gInSc1	Slovan
Liberec	Liberec	k1gInSc1	Liberec
polovinu	polovina	k1gFnSc4	polovina
této	tento	k3xDgFnSc2	tento
částky	částka	k1gFnSc2	částka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Opakovaný	opakovaný	k2eAgInSc4d1	opakovaný
sestup	sestup	k1gInSc4	sestup
a	a	k8xC	a
návrat	návrat	k1gInSc4	návrat
do	do	k7c2	do
první	první	k4xOgFnSc2	první
ligy	liga	k1gFnSc2	liga
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2012	[number]	k4	2012
<g/>
/	/	kIx~	/
<g/>
13	[number]	k4	13
předváděla	předvádět	k5eAaImAgFnS	předvádět
Sigma	sigma	k1gNnSc4	sigma
vedená	vedený	k2eAgFnSc1d1	vedená
až	až	k8xS	až
do	do	k7c2	do
25	[number]	k4	25
<g/>
.	.	kIx.	.
kola	kolo	k1gNnSc2	kolo
trenérem	trenér	k1gMnSc7	trenér
Romanem	Roman	k1gMnSc7	Roman
Pivarníkem	Pivarník	k1gInSc7	Pivarník
útočný	útočný	k2eAgInSc4d1	útočný
fotbal	fotbal	k1gInSc4	fotbal
<g/>
,	,	kIx,	,
bojovala	bojovat	k5eAaImAgFnS	bojovat
v	v	k7c6	v
popředí	popředí	k1gNnSc6	popředí
tabulky	tabulka	k1gFnSc2	tabulka
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
skončila	skončit	k5eAaPmAgFnS	skončit
těsně	těsně	k6eAd1	těsně
pod	pod	k7c7	pod
pohárovými	pohárový	k2eAgFnPc7d1	pohárová
příčkami	příčka	k1gFnPc7	příčka
<g/>
,	,	kIx,	,
když	když	k8xS	když
obsadila	obsadit	k5eAaPmAgFnS	obsadit
konečné	konečný	k2eAgNnSc4d1	konečné
páté	pátý	k4xOgNnSc4	pátý
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnSc1d1	následující
sezóna	sezóna	k1gFnSc1	sezóna
však	však	k9	však
dopadla	dopadnout	k5eAaPmAgFnS	dopadnout
mnohem	mnohem	k6eAd1	mnohem
hůře	zle	k6eAd2	zle
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Sigma	sigma	k1gNnSc1	sigma
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
se	se	k3xPyFc4	se
během	během	k7c2	během
třiceti	třicet	k4xCc2	třicet
ligových	ligový	k2eAgInPc2d1	ligový
zápasů	zápas	k1gInPc2	zápas
vystřídali	vystřídat	k5eAaPmAgMnP	vystřídat
tři	tři	k4xCgMnPc4	tři
hlavní	hlavní	k2eAgMnPc1d1	hlavní
trenéři	trenér	k1gMnPc1	trenér
<g/>
,	,	kIx,	,
nasbírala	nasbírat	k5eAaPmAgFnS	nasbírat
pouze	pouze	k6eAd1	pouze
29	[number]	k4	29
bodů	bod	k1gInPc2	bod
a	a	k8xC	a
z	z	k7c2	z
konečného	konečný	k2eAgNnSc2d1	konečné
předposledního	předposlední	k2eAgNnSc2d1	předposlední
místa	místo	k1gNnSc2	místo
proto	proto	k8xC	proto
po	po	k7c6	po
třiceti	třicet	k4xCc6	třicet
letech	léto	k1gNnPc6	léto
sestoupila	sestoupit	k5eAaPmAgFnS	sestoupit
z	z	k7c2	z
první	první	k4xOgFnSc2	první
ligy	liga	k1gFnSc2	liga
do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závěrečném	závěrečný	k2eAgNnSc6d1	závěrečné
kole	kolo	k1gNnSc6	kolo
remizovala	remizovat	k5eAaPmAgFnS	remizovat
na	na	k7c6	na
hřišti	hřiště	k1gNnSc6	hřiště
Slovanu	Slovan	k1gInSc2	Slovan
Liberec	Liberec	k1gInSc1	Liberec
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
a	a	k8xC	a
na	na	k7c4	na
záchranu	záchrana	k1gFnSc4	záchrana
jí	jíst	k5eAaImIp3nS	jíst
přitom	přitom	k6eAd1	přitom
tehdy	tehdy	k6eAd1	tehdy
stačilo	stačit	k5eAaBmAgNnS	stačit
vstřelit	vstřelit	k5eAaPmF	vstřelit
jeden	jeden	k4xCgInSc4	jeden
vítězný	vítězný	k2eAgInSc4d1	vítězný
gól	gól	k1gInSc4	gól
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2014	[number]	k4	2014
<g/>
/	/	kIx~	/
<g/>
15	[number]	k4	15
hrál	hrát	k5eAaImAgInS	hrát
proto	proto	k8xC	proto
klub	klub	k1gInSc1	klub
po	po	k7c6	po
30	[number]	k4	30
letech	léto	k1gNnPc6	léto
2	[number]	k4	2
<g/>
.	.	kIx.	.
ligu	liga	k1gFnSc4	liga
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
15	[number]	k4	15
podzimních	podzimní	k2eAgNnPc6d1	podzimní
kolech	kolo	k1gNnPc6	kolo
vedl	vést	k5eAaImAgInS	vést
tým	tým	k1gInSc1	tým
<g/>
,	,	kIx,	,
kterého	který	k3yQgInSc2	který
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgMnS	ujmout
kouč	kouč	k1gMnSc1	kouč
Leoš	Leoš	k1gMnSc1	Leoš
Kalvoda	Kalvoda	k1gMnSc1	Kalvoda
tabulku	tabulka	k1gFnSc4	tabulka
o	o	k7c6	o
4	[number]	k4	4
body	bod	k1gInPc4	bod
před	před	k7c7	před
Varnsdorfem	Varnsdorf	k1gInSc7	Varnsdorf
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yIgMnSc7	který
měl	mít	k5eAaImAgMnS	mít
hrát	hrát	k5eAaImF	hrát
zápas	zápas	k1gInSc4	zápas
16	[number]	k4	16
<g/>
.	.	kIx.	.
kola	kolo	k1gNnSc2	kolo
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
byl	být	k5eAaImAgInS	být
však	však	k9	však
několikrát	několikrát	k6eAd1	několikrát
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
důvodů	důvod	k1gInPc2	důvod
odložen	odložen	k2eAgMnSc1d1	odložen
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
odehrán	odehrát	k5eAaPmNgInS	odehrát
až	až	k9	až
3	[number]	k4	3
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
<g/>
.	.	kIx.	.
</s>
<s>
Domácí	domácí	k2eAgFnSc1d1	domácí
Olomouc	Olomouc	k1gFnSc1	Olomouc
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
a	a	k8xC	a
po	po	k7c6	po
dohrání	dohrání	k1gNnSc6	dohrání
podzimní	podzimní	k2eAgFnSc2d1	podzimní
části	část	k1gFnSc2	část
tak	tak	k6eAd1	tak
měla	mít	k5eAaImAgFnS	mít
již	již	k9	již
7	[number]	k4	7
<g/>
bodový	bodový	k2eAgInSc4d1	bodový
náskok	náskok	k1gInSc4	náskok
<g/>
.	.	kIx.	.
</s>
<s>
Olomouci	Olomouc	k1gFnSc3	Olomouc
se	se	k3xPyFc4	se
dařilo	dařit	k5eAaImAgNnS	dařit
i	i	k9	i
v	v	k7c6	v
jarní	jarní	k2eAgFnSc6d1	jarní
části	část	k1gFnSc6	část
soutěže	soutěž	k1gFnSc2	soutěž
a	a	k8xC	a
v	v	k7c6	v
27	[number]	k4	27
<g/>
.	.	kIx.	.
kole	kolo	k1gNnSc6	kolo
si	se	k3xPyFc3	se
definitivně	definitivně	k6eAd1	definitivně
zajistila	zajistit	k5eAaPmAgFnS	zajistit
návrat	návrat	k1gInSc4	návrat
do	do	k7c2	do
1	[number]	k4	1
<g/>
.	.	kIx.	.
ligy	liga	k1gFnSc2	liga
po	po	k7c6	po
výhře	výhra	k1gFnSc6	výhra
na	na	k7c6	na
hřišti	hřiště	k1gNnSc6	hřiště
Pardubic	Pardubice	k1gInPc2	Pardubice
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
vítězství	vítězství	k1gNnSc6	vítězství
ve	v	k7c6	v
Fotbalové	fotbalový	k2eAgFnSc6d1	fotbalová
národní	národní	k2eAgFnSc6d1	národní
lize	liga	k1gFnSc6	liga
se	se	k3xPyFc4	se
Sigma	sigma	k1gNnSc1	sigma
Olomouc	Olomouc	k1gFnSc4	Olomouc
pro	pro	k7c4	pro
sezónu	sezóna	k1gFnSc4	sezóna
2015	[number]	k4	2015
<g/>
/	/	kIx~	/
<g/>
16	[number]	k4	16
po	po	k7c6	po
roce	rok	k1gInSc6	rok
znovu	znovu	k6eAd1	znovu
vrátila	vrátit	k5eAaPmAgFnS	vrátit
do	do	k7c2	do
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
české	český	k2eAgFnSc2d1	Česká
fotbalové	fotbalový	k2eAgFnSc2d1	fotbalová
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Návrat	návrat	k1gInSc1	návrat
mezi	mezi	k7c4	mezi
nejlepší	dobrý	k2eAgInPc4d3	nejlepší
české	český	k2eAgInPc4d1	český
týmy	tým	k1gInPc4	tým
se	se	k3xPyFc4	se
však	však	k9	však
Sigmě	Sigma	k1gFnSc6	Sigma
vůbec	vůbec	k9	vůbec
nepovedl	povést	k5eNaPmAgMnS	povést
<g/>
.	.	kIx.	.
</s>
<s>
Trenéra	trenér	k1gMnSc4	trenér
Kalvodu	Kalvoda	k1gMnSc4	Kalvoda
nahradil	nahradit	k5eAaPmAgMnS	nahradit
po	po	k7c6	po
10	[number]	k4	10
<g/>
.	.	kIx.	.
kole	kolo	k1gNnSc6	kolo
kouč	kouč	k1gMnSc1	kouč
Václav	Václav	k1gMnSc1	Václav
Jílek	Jílek	k1gMnSc1	Jílek
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
ani	ani	k8xC	ani
tato	tento	k3xDgFnSc1	tento
změna	změna	k1gFnSc1	změna
nakonec	nakonec	k6eAd1	nakonec
k	k	k7c3	k
záchraně	záchrana	k1gFnSc3	záchrana
nepomohla	pomoct	k5eNaPmAgFnS	pomoct
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
sezóně	sezóna	k1gFnSc6	sezóna
nasbírala	nasbírat	k5eAaPmAgFnS	nasbírat
Sigma	sigma	k1gNnSc4	sigma
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
po	po	k7c4	po
většinu	většina	k1gFnSc4	většina
soutěže	soutěž	k1gFnSc2	soutěž
pohybovala	pohybovat	k5eAaImAgNnP	pohybovat
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
sestupových	sestupový	k2eAgFnPc2d1	sestupová
příček	příčka	k1gFnPc2	příčka
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
27	[number]	k4	27
bodů	bod	k1gInPc2	bod
za	za	k7c4	za
6	[number]	k4	6
výher	výhra	k1gFnPc2	výhra
<g/>
,	,	kIx,	,
9	[number]	k4	9
remíz	remíza	k1gFnPc2	remíza
a	a	k8xC	a
15	[number]	k4	15
proher	prohra	k1gFnPc2	prohra
a	a	k8xC	a
kvůli	kvůli	k7c3	kvůli
horšímu	horší	k1gNnSc3	horší
skóre	skóre	k1gNnSc2	skóre
než	než	k8xS	než
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
tabulce	tabulka	k1gFnSc6	tabulka
14	[number]	k4	14
<g/>
.	.	kIx.	.
</s>
<s>
Příbram	Příbram	k1gFnSc1	Příbram
proto	proto	k8xC	proto
po	po	k7c6	po
roce	rok	k1gInSc6	rok
znovu	znovu	k6eAd1	znovu
sestoupila	sestoupit	k5eAaPmAgFnS	sestoupit
do	do	k7c2	do
2	[number]	k4	2
<g/>
.	.	kIx.	.
ligy	liga	k1gFnSc2	liga
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhém	druhý	k4xOgInSc6	druhý
sestupu	sestup	k1gInSc6	sestup
klub	klub	k1gInSc1	klub
podržel	podržet	k5eAaPmAgMnS	podržet
kouče	kouč	k1gMnSc4	kouč
Jílka	Jílek	k1gMnSc4	Jílek
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
s	s	k7c7	s
týmem	tým	k1gInSc7	tým
tvořeným	tvořený	k2eAgInSc7d1	tvořený
především	především	k6eAd1	především
mladými	mladý	k2eAgMnPc7d1	mladý
odchovanci	odchovanec	k1gMnPc7	odchovanec
v	v	k7c6	v
následující	následující	k2eAgFnSc6d1	následující
sezóně	sezóna	k1gFnSc6	sezóna
znovu	znovu	k6eAd1	znovu
vybojoval	vybojovat	k5eAaPmAgMnS	vybojovat
návrat	návrat	k1gInSc4	návrat
mezi	mezi	k7c4	mezi
elitu	elita	k1gFnSc4	elita
<g/>
.	.	kIx.	.
</s>
<s>
Sigma	sigma	k1gNnSc1	sigma
si	se	k3xPyFc3	se
postup	postup	k1gInSc4	postup
do	do	k7c2	do
první	první	k4xOgFnSc2	první
ligy	liga	k1gFnSc2	liga
zajistila	zajistit	k5eAaPmAgFnS	zajistit
již	již	k6eAd1	již
po	po	k7c6	po
26	[number]	k4	26
<g/>
.	.	kIx.	.
kole	kolo	k1gNnSc6	kolo
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
v	v	k7c6	v
popředí	popředí	k1gNnSc6	popředí
tabulky	tabulka	k1gFnSc2	tabulka
osamostatnila	osamostatnit	k5eAaPmAgFnS	osamostatnit
s	s	k7c7	s
náskokem	náskok	k1gInSc7	náskok
16	[number]	k4	16
bodů	bod	k1gInPc2	bod
na	na	k7c4	na
třetí	třetí	k4xOgFnSc4	třetí
Opavu	Opava	k1gFnSc4	Opava
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
soutěž	soutěž	k1gFnSc1	soutěž
ovládla	ovládnout	k5eAaPmAgFnS	ovládnout
<g/>
,	,	kIx,	,
získala	získat	k5eAaPmAgFnS	získat
69	[number]	k4	69
bodů	bod	k1gInPc2	bod
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
druhým	druhý	k4xOgMnSc7	druhý
Baníkem	baník	k1gMnSc7	baník
Ostrava	Ostrava	k1gFnSc1	Ostrava
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
po	po	k7c6	po
roce	rok	k1gInSc6	rok
vrátila	vrátit	k5eAaPmAgFnS	vrátit
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Evropská	evropský	k2eAgFnSc1d1	Evropská
liga	liga	k1gFnSc1	liga
===	===	k?	===
</s>
</p>
<p>
<s>
Mladý	mladý	k2eAgInSc1d1	mladý
tým	tým	k1gInSc1	tým
překvapil	překvapit	k5eAaPmAgInS	překvapit
v	v	k7c6	v
podzimní	podzimní	k2eAgFnSc6d1	podzimní
části	část	k1gFnSc6	část
sezóny	sezóna	k1gFnSc2	sezóna
2017	[number]	k4	2017
<g/>
/	/	kIx~	/
<g/>
18	[number]	k4	18
atraktivní	atraktivní	k2eAgFnSc6d1	atraktivní
hrou	hra	k1gFnSc7	hra
a	a	k8xC	a
překvapivě	překvapivě	k6eAd1	překvapivě
se	se	k3xPyFc4	se
před	před	k7c7	před
jarním	jarní	k2eAgNnSc7d1	jarní
pokračováním	pokračování	k1gNnSc7	pokračování
zařadil	zařadit	k5eAaPmAgMnS	zařadit
na	na	k7c4	na
průběžné	průběžný	k2eAgNnSc4d1	průběžné
třetí	třetí	k4xOgNnSc4	třetí
místo	místo	k1gNnSc4	místo
za	za	k7c4	za
suverénní	suverénní	k2eAgFnSc4d1	suverénní
Plzeň	Plzeň	k1gFnSc4	Plzeň
a	a	k8xC	a
posílenou	posílený	k2eAgFnSc4d1	posílená
Slavii	slavie	k1gFnSc4	slavie
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Tým	tým	k1gInSc1	tým
Olomouce	Olomouc	k1gFnSc2	Olomouc
zakončil	zakončit	k5eAaPmAgInS	zakončit
úspěšnou	úspěšný	k2eAgFnSc4d1	úspěšná
sezónu	sezóna	k1gFnSc4	sezóna
celkovým	celkový	k2eAgInSc7d1	celkový
ziskem	zisk	k1gInSc7	zisk
55	[number]	k4	55
bodů	bod	k1gInPc2	bod
a	a	k8xC	a
umístil	umístit	k5eAaPmAgMnS	umístit
se	se	k3xPyFc4	se
na	na	k7c6	na
čtvrté	čtvrtý	k4xOgFnSc6	čtvrtý
pozici	pozice	k1gFnSc6	pozice
konečné	konečný	k2eAgFnSc2d1	konečná
tabulky	tabulka	k1gFnSc2	tabulka
HET	HET	kA	HET
ligy	liga	k1gFnSc2	liga
2017	[number]	k4	2017
<g/>
/	/	kIx~	/
<g/>
18	[number]	k4	18
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
níž	jenž	k3xRgFnSc3	jenž
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgInS	dostat
do	do	k7c2	do
předkol	předkolo	k1gNnPc2	předkolo
evropské	evropský	k2eAgFnSc2d1	Evropská
ligy	liga	k1gFnSc2	liga
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třetím	třetí	k4xOgNnSc6	třetí
předkole	předkolo	k1gNnSc6	předkolo
olomoucký	olomoucký	k2eAgInSc1d1	olomoucký
tým	tým	k1gInSc1	tým
vybojoval	vybojovat	k5eAaPmAgInS	vybojovat
postup	postup	k1gInSc4	postup
nad	nad	k7c7	nad
týmem	tým	k1gInSc7	tým
Kajrat	Kajrat	k1gInSc1	Kajrat
Almaty	Almata	k1gFnSc2	Almata
z	z	k7c2	z
Kazachstánu	Kazachstán	k1gInSc2	Kazachstán
a	a	k8xC	a
ve	v	k7c6	v
čtvrtém	čtvrtý	k4xOgInSc6	čtvrtý
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
Sigma	sigma	k1gNnSc2	sigma
střetla	střetnout	k5eAaPmAgFnS	střetnout
se	s	k7c7	s
španělským	španělský	k2eAgInSc7d1	španělský
klubem	klub	k1gInSc7	klub
FC	FC	kA	FC
Sevilla	Sevilla	k1gFnSc1	Sevilla
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
sice	sice	k8xC	sice
Sigma	sigma	k1gNnPc4	sigma
vypadla	vypadnout	k5eAaPmAgFnS	vypadnout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hlavně	hlavně	k9	hlavně
doma	doma	k6eAd1	doma
odehrála	odehrát	k5eAaPmAgFnS	odehrát
fantastický	fantastický	k2eAgInSc4d1	fantastický
zápas	zápas	k1gInSc4	zápas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historické	historický	k2eAgInPc1d1	historický
názvy	název	k1gInPc1	název
==	==	k?	==
</s>
</p>
<p>
<s>
Zdroj	zdroj	k1gInSc1	zdroj
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1919	[number]	k4	1919
–	–	k?	–
FK	FK	kA	FK
Hejčín	Hejčín	k1gInSc1	Hejčín
Olomouc	Olomouc	k1gFnSc1	Olomouc
(	(	kIx(	(
<g/>
Fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
klub	klub	k1gInSc1	klub
Hejčín	Hejčín	k1gInSc1	Hejčín
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1920	[number]	k4	1920
–	–	k?	–
SK	Sk	kA	Sk
Hejčín	Hejčín	k1gInSc1	Hejčín
Olomouc	Olomouc	k1gFnSc1	Olomouc
(	(	kIx(	(
<g/>
Sportovní	sportovní	k2eAgInSc1d1	sportovní
klub	klub	k1gInSc1	klub
Hejčín	Hejčín	k1gInSc1	Hejčín
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1947	[number]	k4	1947
–	–	k?	–
fúze	fúze	k1gFnSc1	fúze
s	s	k7c7	s
SK	Sk	kA	Sk
Sokol	Sokol	k1gMnSc1	Sokol
Hejčín	Hejčín	k1gMnSc1	Hejčín
a	a	k8xC	a
DTJ	DTJ	kA	DTJ
Hejčín	Hejčín	k1gInSc1	Hejčín
=	=	kIx~	=
<g/>
>	>	kIx)	>
HSK	HSK	kA	HSK
BH	BH	kA	BH
Olomouc	Olomouc	k1gFnSc1	Olomouc
(	(	kIx(	(
<g/>
Hejčínský	Hejčínský	k2eAgInSc1d1	Hejčínský
sportovní	sportovní	k2eAgInSc1d1	sportovní
klub	klub	k1gInSc1	klub
Banské	banský	k2eAgFnSc2d1	Banská
a	a	k8xC	a
Hutní	hutnit	k5eAaImIp3nS	hutnit
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1949	[number]	k4	1949
–	–	k?	–
ZSJ	ZSJ	kA	ZSJ
MŽ	MŽ	kA	MŽ
Olomouc	Olomouc	k1gFnSc1	Olomouc
(	(	kIx(	(
<g/>
Základní	základní	k2eAgFnSc1d1	základní
sportovní	sportovní	k2eAgFnSc1d1	sportovní
jednota	jednota	k1gFnSc1	jednota
Moravské	moravský	k2eAgFnSc2d1	Moravská
železárny	železárna	k1gFnSc2	železárna
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1953	[number]	k4	1953
–	–	k?	–
fúze	fúze	k1gFnSc1	fúze
s	s	k7c7	s
TJ	tj	kA	tj
Rudá	rudý	k2eAgFnSc1d1	rudá
hvězda	hvězda	k1gFnSc1	hvězda
Olomouc	Olomouc	k1gFnSc1	Olomouc
=	=	kIx~	=
<g/>
>	>	kIx)	>
DSO	DSO	kA	DSO
Baník	Baník	k1gInSc1	Baník
MŽ	MŽ	kA	MŽ
Olomouc	Olomouc	k1gFnSc1	Olomouc
(	(	kIx(	(
<g/>
Dobrovolná	dobrovolný	k2eAgFnSc1d1	dobrovolná
sportovní	sportovní	k2eAgFnSc1d1	sportovní
organizace	organizace	k1gFnSc1	organizace
Baník	Baník	k1gInSc1	Baník
Moravské	moravský	k2eAgFnSc2d1	Moravská
železárny	železárna	k1gFnSc2	železárna
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1954	[number]	k4	1954
–	–	k?	–
TJ	tj	kA	tj
Spartak	Spartak	k1gInSc1	Spartak
MŽ	MŽ	kA	MŽ
Olomouc	Olomouc	k1gFnSc1	Olomouc
(	(	kIx(	(
<g/>
Tělovýchovná	tělovýchovný	k2eAgFnSc1d1	Tělovýchovná
jednota	jednota	k1gFnSc1	jednota
Spartak	Spartak	k1gInSc1	Spartak
Moravské	moravský	k2eAgFnSc2d1	Moravská
železárny	železárna	k1gFnSc2	železárna
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1960	[number]	k4	1960
–	–	k?	–
TJ	tj	kA	tj
MŽ	MŽ	kA	MŽ
Olomouc	Olomouc	k1gFnSc1	Olomouc
(	(	kIx(	(
<g/>
Tělovýchovná	tělovýchovný	k2eAgFnSc1d1	Tělovýchovná
jednota	jednota	k1gFnSc1	jednota
Moravské	moravský	k2eAgFnSc2d1	Moravská
železárny	železárna	k1gFnSc2	železárna
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1966	[number]	k4	1966
–	–	k?	–
TJ	tj	kA	tj
Sigma	sigma	k1gNnPc2	sigma
MŽ	MŽ	kA	MŽ
Olomouc	Olomouc	k1gFnSc1	Olomouc
(	(	kIx(	(
<g/>
Tělovýchovná	tělovýchovný	k2eAgFnSc1d1	Tělovýchovná
jednota	jednota	k1gFnSc1	jednota
Sigma	sigma	k1gNnSc2	sigma
Moravské	moravský	k2eAgFnSc2d1	Moravská
železárny	železárna	k1gFnSc2	železárna
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1976	[number]	k4	1976
–	–	k?	–
fúze	fúze	k1gFnSc1	fúze
s	s	k7c7	s
TJ	tj	kA	tj
Slovan	Slovan	k1gMnSc1	Slovan
Černovír	Černovír	k1gMnSc1	Černovír
=	=	kIx~	=
<g/>
>	>	kIx)	>
název	název	k1gInSc1	název
nezměněn	změnit	k5eNaPmNgInS	změnit
</s>
</p>
<p>
<s>
1979	[number]	k4	1979
–	–	k?	–
TJ	tj	kA	tj
Sigma	sigma	k1gNnPc2	sigma
ZTS	ZTS	kA	ZTS
Olomouc	Olomouc	k1gFnSc1	Olomouc
(	(	kIx(	(
<g/>
Tělovýchovná	tělovýchovný	k2eAgFnSc1d1	Tělovýchovná
jednota	jednota	k1gFnSc1	jednota
Sigma	sigma	k1gNnSc2	sigma
Závody	závod	k1gInPc1	závod
těžkého	těžký	k2eAgNnSc2d1	těžké
strojírenství	strojírenství	k1gNnSc2	strojírenství
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1990	[number]	k4	1990
–	–	k?	–
SK	Sk	kA	Sk
Sigma	sigma	k1gNnPc2	sigma
MŽ	MŽ	kA	MŽ
Olomouc	Olomouc	k1gFnSc1	Olomouc
(	(	kIx(	(
<g/>
Sportovní	sportovní	k2eAgInSc1d1	sportovní
klub	klub	k1gInSc1	klub
Sigma	sigma	k1gNnSc2	sigma
Moravské	moravský	k2eAgFnSc2d1	Moravská
železárny	železárna	k1gFnSc2	železárna
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
a.	a.	k?	a.
s.	s.	k?	s.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1996	[number]	k4	1996
–	–	k?	–
SK	Sk	kA	Sk
Sigma	sigma	k1gNnSc3	sigma
Olomouc	Olomouc	k1gFnSc1	Olomouc
(	(	kIx(	(
<g/>
Sportovní	sportovní	k2eAgInSc1d1	sportovní
klub	klub	k1gInSc1	klub
Sigma	sigma	k1gNnSc2	sigma
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
a.	a.	k?	a.
s.	s.	k?	s.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Znak	znak	k1gInSc4	znak
==	==	k?	==
</s>
</p>
<p>
<s>
Současný	současný	k2eAgInSc1d1	současný
znak	znak	k1gInSc1	znak
Sigmy	Sigma	k1gFnSc2	Sigma
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
autorem	autor	k1gMnSc7	autor
je	být	k5eAaImIp3nS	být
Miloš	Miloš	k1gMnSc1	Miloš
Holec	holec	k1gMnSc1	holec
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
až	až	k9	až
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
do	do	k7c2	do
klubu	klub	k1gInSc2	klub
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
koncern	koncern	k1gInSc1	koncern
Sigma	sigma	k1gNnSc2	sigma
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
od	od	k7c2	od
něj	on	k3xPp3gNnSc2	on
získalo	získat	k5eAaPmAgNnS	získat
mužstvo	mužstvo	k1gNnSc1	mužstvo
klubové	klubový	k2eAgFnSc2d1	klubová
barvy	barva	k1gFnSc2	barva
–	–	k?	–
modrou	modrý	k2eAgFnSc4d1	modrá
a	a	k8xC	a
bílou	bílý	k2eAgFnSc4d1	bílá
<g/>
.	.	kIx.	.
</s>
<s>
Černá	černý	k2eAgFnSc1d1	černá
pěticípá	pěticípý	k2eAgFnSc1d1	pěticípá
hvězda	hvězda	k1gFnSc1	hvězda
uprostřed	uprostřed	k7c2	uprostřed
znaku	znak	k1gInSc2	znak
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
původní	původní	k2eAgFnPc4d1	původní
barvy	barva	k1gFnPc4	barva
SK	Sk	kA	Sk
Hejčín	Hejčín	k1gInSc4	Hejčín
a	a	k8xC	a
na	na	k7c4	na
Moravské	moravský	k2eAgFnPc4d1	Moravská
železárny	železárna	k1gFnPc4	železárna
<g/>
.	.	kIx.	.
</s>
<s>
Kruhový	kruhový	k2eAgInSc4d1	kruhový
tvar	tvar	k1gInSc4	tvar
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
spojení	spojení	k1gNnSc1	spojení
a	a	k8xC	a
pět	pět	k4xCc1	pět
svislých	svislý	k2eAgInPc2d1	svislý
modrých	modrý	k2eAgInPc2d1	modrý
pruhů	pruh	k1gInPc2	pruh
představuje	představovat	k5eAaImIp3nS	představovat
pět	pět	k4xCc4	pět
klubových	klubový	k2eAgInPc2d1	klubový
postupů	postup	k1gInPc2	postup
z	z	k7c2	z
I.	I.	kA	I.
A	a	k8xC	a
třídy	třída	k1gFnSc2	třída
až	až	k9	až
do	do	k7c2	do
první	první	k4xOgFnSc2	první
ligy	liga	k1gFnSc2	liga
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Úspěchy	úspěch	k1gInPc1	úspěch
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Vyhrané	vyhraná	k1gFnPc4	vyhraná
domácí	domácí	k2eAgFnSc2d1	domácí
soutěže	soutěž	k1gFnSc2	soutěž
===	===	k?	===
</s>
</p>
<p>
<s>
Pohár	pohár	k1gInSc1	pohár
České	český	k2eAgFnSc2d1	Česká
pošty	pošta	k1gFnSc2	pošta
(	(	kIx(	(
1	[number]	k4	1
<g/>
×	×	k?	×
)	)	kIx)	)
</s>
</p>
<p>
<s>
2011	[number]	k4	2011
<g/>
/	/	kIx~	/
<g/>
12	[number]	k4	12
</s>
</p>
<p>
<s>
Český	český	k2eAgInSc1d1	český
Superpohár	superpohár	k1gInSc1	superpohár
(	(	kIx(	(
1	[number]	k4	1
<g/>
×	×	k?	×
)	)	kIx)	)
</s>
</p>
<p>
<s>
2012	[number]	k4	2012
</s>
</p>
<p>
<s>
==	==	k?	==
Soupiska	soupiska	k1gFnSc1	soupiska
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Brankáři	brankář	k1gMnPc5	brankář
===	===	k?	===
</s>
</p>
<p>
<s>
Miloš	Miloš	k1gMnSc1	Miloš
Buchta	Buchta	k1gMnSc1	Buchta
</s>
</p>
<p>
<s>
Michal	Michal	k1gMnSc1	Michal
Reichl	Reichl	k1gMnSc1	Reichl
</s>
</p>
<p>
<s>
Aleš	Aleš	k1gMnSc1	Aleš
Mandous	Mandous	k1gMnSc1	Mandous
</s>
</p>
<p>
<s>
===	===	k?	===
Obránci	obránce	k1gMnPc5	obránce
===	===	k?	===
</s>
</p>
<p>
<s>
Michal	Michal	k1gMnSc1	Michal
Vepřek	Vepřek	k1gInSc1	Vepřek
(	(	kIx(	(
<g/>
kapitán	kapitán	k1gMnSc1	kapitán
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
Jemelka	Jemelka	k1gMnSc1	Jemelka
</s>
</p>
<p>
<s>
Martin	Martin	k1gMnSc1	Martin
Sladký	Sladký	k1gMnSc1	Sladký
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Štěrba	Štěrba	k1gMnSc1	Štěrba
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Kotouč	kotouč	k1gInSc4	kotouč
</s>
</p>
<p>
<s>
Michal	Michal	k1gMnSc1	Michal
Surzyn	Surzyn	k1gMnSc1	Surzyn
</s>
</p>
<p>
<s>
Roman	Roman	k1gMnSc1	Roman
Polom	polom	k1gInSc4	polom
</s>
</p>
<p>
<s>
Juraj	Juraj	k1gMnSc1	Juraj
Chvátal	Chvátal	k1gMnSc1	Chvátal
</s>
</p>
<p>
<s>
Vít	Vít	k1gMnSc1	Vít
Beneš	Beneš	k1gMnSc1	Beneš
</s>
</p>
<p>
<s>
===	===	k?	===
Záložníci	záložník	k1gMnPc5	záložník
===	===	k?	===
</s>
</p>
<p>
<s>
Jakub	Jakub	k1gMnSc1	Jakub
Plšek	Plšek	k1gMnSc1	Plšek
</s>
</p>
<p>
<s>
Martin	Martin	k1gMnSc1	Martin
Hála	Hála	k1gMnSc1	Hála
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
Pilař	Pilař	k1gMnSc1	Pilař
</s>
</p>
<p>
<s>
Šimon	Šimon	k1gMnSc1	Šimon
Falta	Falta	k1gMnSc1	Falta
</s>
</p>
<p>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Zahradníček	Zahradníček	k1gMnSc1	Zahradníček
</s>
</p>
<p>
<s>
Lukáš	Lukáš	k1gMnSc1	Lukáš
Kalvach	Kalvach	k1gMnSc1	Kalvach
</s>
</p>
<p>
<s>
David	David	k1gMnSc1	David
Houska	houska	k1gFnSc1	houska
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Texl	Texl	k1gMnSc1	Texl
</s>
</p>
<p>
<s>
Milan	Milan	k1gMnSc1	Milan
Lalkovič	Lalkovič	k1gMnSc1	Lalkovič
</s>
</p>
<p>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Zlatohlávek	zlatohlávek	k1gMnSc1	zlatohlávek
</s>
</p>
<p>
<s>
Ondřej	Ondřej	k1gMnSc1	Ondřej
Zmrzlý	zmrzlý	k2eAgMnSc1d1	zmrzlý
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Spáčil	Spáčil	k1gMnSc1	Spáčil
</s>
</p>
<p>
<s>
Jakub	Jakub	k1gMnSc1	Jakub
Matoušek	Matoušek	k1gMnSc1	Matoušek
</s>
</p>
<p>
<s>
===	===	k?	===
Útočníci	útočník	k1gMnPc1	útočník
===	===	k?	===
</s>
</p>
<p>
<s>
Martin	Martin	k1gMnSc1	Martin
Nešpor	Nešpor	k1gMnSc1	Nešpor
</s>
</p>
<p>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Dvořák	Dvořák	k1gMnSc1	Dvořák
</s>
</p>
<p>
<s>
Jakub	Jakub	k1gMnSc1	Jakub
Yunis	Yunis	k1gFnSc2	Yunis
</s>
</p>
<p>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Zifčák	Zifčák	k1gMnSc1	Zifčák
</s>
</p>
<p>
<s>
==	==	k?	==
Umístění	umístění	k1gNnSc1	umístění
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
sezonách	sezona	k1gFnPc6	sezona
==	==	k?	==
</s>
</p>
<p>
<s>
Stručný	stručný	k2eAgInSc1d1	stručný
přehledZdroj	přehledZdroj	k1gInSc1	přehledZdroj
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1951	[number]	k4	1951
<g/>
–	–	k?	–
<g/>
1954	[number]	k4	1954
<g/>
:	:	kIx,	:
Krajská	krajský	k2eAgFnSc1d1	krajská
soutěž	soutěž	k1gFnSc1	soutěž
-	-	kIx~	-
Olomouc	Olomouc	k1gFnSc1	Olomouc
</s>
</p>
<p>
<s>
1956	[number]	k4	1956
<g/>
:	:	kIx,	:
Oblastní	oblastní	k2eAgFnSc1d1	oblastní
soutěž	soutěž	k1gFnSc1	soutěž
-	-	kIx~	-
sk	sk	k?	sk
<g/>
.	.	kIx.	.
</s>
<s>
D	D	kA	D
</s>
</p>
<p>
<s>
1958	[number]	k4	1958
<g/>
–	–	k?	–
<g/>
1959	[number]	k4	1959
<g/>
:	:	kIx,	:
Oblastní	oblastní	k2eAgFnSc1d1	oblastní
soutěž	soutěž	k1gFnSc1	soutěž
-	-	kIx~	-
sk	sk	k?	sk
<g/>
.	.	kIx.	.
</s>
<s>
D	D	kA	D
</s>
</p>
<p>
<s>
1961	[number]	k4	1961
<g/>
–	–	k?	–
<g/>
1963	[number]	k4	1963
<g/>
:	:	kIx,	:
Severomoravský	severomoravský	k2eAgInSc1d1	severomoravský
krajský	krajský	k2eAgInSc1d1	krajský
přebor	přebor	k1gInSc1	přebor
</s>
</p>
<p>
<s>
1963	[number]	k4	1963
<g/>
–	–	k?	–
<g/>
1965	[number]	k4	1965
<g/>
:	:	kIx,	:
2	[number]	k4	2
<g/>
.	.	kIx.	.
liga	liga	k1gFnSc1	liga
-	-	kIx~	-
sk	sk	k?	sk
<g/>
.	.	kIx.	.
</s>
<s>
B	B	kA	B
</s>
</p>
<p>
<s>
1965	[number]	k4	1965
<g/>
–	–	k?	–
<g/>
1974	[number]	k4	1974
<g/>
:	:	kIx,	:
Divize	divize	k1gFnSc2	divize
D	D	kA	D
</s>
</p>
<p>
<s>
1974	[number]	k4	1974
<g/>
–	–	k?	–
<g/>
1977	[number]	k4	1977
<g/>
:	:	kIx,	:
3	[number]	k4	3
<g/>
.	.	kIx.	.
liga	liga	k1gFnSc1	liga
-	-	kIx~	-
sk	sk	k?	sk
<g/>
.	.	kIx.	.
</s>
<s>
B	B	kA	B
</s>
</p>
<p>
<s>
1977	[number]	k4	1977
<g/>
–	–	k?	–
<g/>
1981	[number]	k4	1981
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
ČNFL	ČNFL	kA	ČNFL
-	-	kIx~	-
sk	sk	k?	sk
<g/>
.	.	kIx.	.
</s>
<s>
B	B	kA	B
</s>
</p>
<p>
<s>
1981	[number]	k4	1981
<g/>
–	–	k?	–
<g/>
1982	[number]	k4	1982
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
ČNFL	ČNFL	kA	ČNFL
</s>
</p>
<p>
<s>
1982	[number]	k4	1982
<g/>
–	–	k?	–
<g/>
1983	[number]	k4	1983
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
liga	liga	k1gFnSc1	liga
</s>
</p>
<p>
<s>
1983	[number]	k4	1983
<g/>
–	–	k?	–
<g/>
1984	[number]	k4	1984
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
ČNFL	ČNFL	kA	ČNFL
</s>
</p>
<p>
<s>
1984	[number]	k4	1984
<g/>
–	–	k?	–
<g/>
1993	[number]	k4	1993
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
liga	liga	k1gFnSc1	liga
(	(	kIx(	(
<g/>
ČSR	ČSR	kA	ČSR
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1993	[number]	k4	1993
<g/>
–	–	k?	–
<g/>
2014	[number]	k4	2014
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
liga	liga	k1gFnSc1	liga
(	(	kIx(	(
<g/>
ČR	ČR	kA	ČR
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2014	[number]	k4	2014
<g/>
–	–	k?	–
<g/>
2015	[number]	k4	2015
<g/>
:	:	kIx,	:
Fotbalová	fotbalový	k2eAgFnSc1d1	fotbalová
národní	národní	k2eAgFnSc1d1	národní
liga	liga	k1gFnSc1	liga
</s>
</p>
<p>
<s>
2015	[number]	k4	2015
<g/>
–	–	k?	–
<g/>
2016	[number]	k4	2016
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
liga	liga	k1gFnSc1	liga
</s>
</p>
<p>
<s>
2016	[number]	k4	2016
<g/>
–	–	k?	–
<g/>
2017	[number]	k4	2017
<g/>
:	:	kIx,	:
Fotbalová	fotbalový	k2eAgFnSc1d1	fotbalová
národní	národní	k2eAgFnSc1d1	národní
liga	liga	k1gFnSc1	liga
</s>
</p>
<p>
<s>
2017	[number]	k4	2017
<g/>
–	–	k?	–
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
ligaJednotlivé	ligaJednotlivý	k2eAgInPc4d1	ligaJednotlivý
ročníkyZdroj	ročníkyZdroj	k1gInSc4	ročníkyZdroj
<g/>
:	:	kIx,	:
Legenda	legenda	k1gFnSc1	legenda
<g/>
:	:	kIx,	:
Z	Z	kA	Z
-	-	kIx~	-
zápasy	zápas	k1gInPc1	zápas
<g/>
,	,	kIx,	,
V	v	k7c6	v
-	-	kIx~	-
výhry	výhra	k1gFnPc4	výhra
<g/>
,	,	kIx,	,
R	R	kA	R
-	-	kIx~	-
remízy	remíz	k1gInPc1	remíz
<g/>
,	,	kIx,	,
P	P	kA	P
-	-	kIx~	-
porážky	porážka	k1gFnSc2	porážka
<g/>
,	,	kIx,	,
VG	VG	kA	VG
-	-	kIx~	-
vstřelené	vstřelený	k2eAgInPc1d1	vstřelený
góly	gól	k1gInPc1	gól
<g/>
,	,	kIx,	,
OG	OG	kA	OG
-	-	kIx~	-
obdržené	obdržený	k2eAgInPc4d1	obdržený
góly	gól	k1gInPc4	gól
<g/>
,	,	kIx,	,
+	+	kIx~	+
<g/>
/	/	kIx~	/
<g/>
-	-	kIx~	-
-	-	kIx~	-
rozdíl	rozdíl	k1gInSc1	rozdíl
skóre	skóre	k1gNnSc2	skóre
<g/>
,	,	kIx,	,
B	B	kA	B
-	-	kIx~	-
body	bod	k1gInPc1	bod
<g/>
,	,	kIx,	,
červené	červený	k2eAgNnSc1d1	červené
podbarvení	podbarvení	k1gNnSc1	podbarvení
-	-	kIx~	-
sestup	sestup	k1gInSc1	sestup
<g/>
,	,	kIx,	,
zelené	zelený	k2eAgNnSc1d1	zelené
podbarvení	podbarvení	k1gNnSc1	podbarvení
-	-	kIx~	-
postup	postup	k1gInSc1	postup
<g/>
,	,	kIx,	,
fialové	fialový	k2eAgNnSc1d1	fialové
podbarvení	podbarvení	k1gNnSc1	podbarvení
-	-	kIx~	-
reorganizace	reorganizace	k1gFnSc1	reorganizace
<g/>
,	,	kIx,	,
změna	změna	k1gFnSc1	změna
skupiny	skupina	k1gFnSc2	skupina
či	či	k8xC	či
soutěže	soutěž	k1gFnSc2	soutěž
</s>
</p>
<p>
<s>
Poznámky	poznámka	k1gFnPc1	poznámka
</s>
</p>
<p>
<s>
1968	[number]	k4	1968
<g/>
/	/	kIx~	/
<g/>
69	[number]	k4	69
<g/>
:	:	kIx,	:
Po	po	k7c6	po
sezóně	sezóna	k1gFnSc6	sezóna
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
reorganizaci	reorganizace	k1gFnSc3	reorganizace
soutěží	soutěž	k1gFnPc2	soutěž
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
Divize	divize	k1gFnSc1	divize
D	D	kA	D
stala	stát	k5eAaPmAgFnS	stát
jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
skupin	skupina	k1gFnPc2	skupina
4	[number]	k4	4
<g/>
.	.	kIx.	.
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
soutěže	soutěž	k1gFnSc2	soutěž
-	-	kIx~	-
prvních	první	k4xOgNnPc6	první
9	[number]	k4	9
mužstev	mužstvo	k1gNnPc2	mužstvo
postoupilo	postoupit	k5eAaPmAgNnS	postoupit
do	do	k7c2	do
3	[number]	k4	3
<g/>
.	.	kIx.	.
ligy	liga	k1gFnSc2	liga
-	-	kIx~	-
sk	sk	k?	sk
<g/>
.	.	kIx.	.
</s>
<s>
B	B	kA	B
<g/>
,	,	kIx,	,
zbylá	zbylý	k2eAgNnPc1d1	zbylé
mužstva	mužstvo	k1gNnPc1	mužstvo
byla	být	k5eAaImAgNnP	být
zařazena	zařadit	k5eAaPmNgNnP	zařadit
do	do	k7c2	do
Divize	divize	k1gFnSc2	divize
D	D	kA	D
</s>
</p>
<p>
<s>
1976	[number]	k4	1976
<g/>
/	/	kIx~	/
<g/>
77	[number]	k4	77
<g/>
:	:	kIx,	:
Po	po	k7c6	po
sezóně	sezóna	k1gFnSc6	sezóna
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
reorganizace	reorganizace	k1gFnSc1	reorganizace
nižších	nízký	k2eAgFnPc2d2	nižší
soutěží	soutěž	k1gFnPc2	soutěž
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
liga	liga	k1gFnSc1	liga
byla	být	k5eAaImAgFnS	být
zrušena	zrušit	k5eAaPmNgFnS	zrušit
-	-	kIx~	-
prvních	první	k4xOgNnPc6	první
9	[number]	k4	9
mužstev	mužstvo	k1gNnPc2	mužstvo
postupovalo	postupovat	k5eAaImAgNnS	postupovat
do	do	k7c2	do
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
ČNFL	ČNFL	kA	ČNFL
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
soutěž	soutěž	k1gFnSc1	soutěž
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zbylá	zbylý	k2eAgNnPc1d1	zbylé
mužstva	mužstvo	k1gNnPc1	mužstvo
sestoupila	sestoupit	k5eAaPmAgNnP	sestoupit
do	do	k7c2	do
příslušných	příslušný	k2eAgFnPc2d1	příslušná
divizních	divizní	k2eAgFnPc2d1	divizní
skupin	skupina	k1gFnPc2	skupina
(	(	kIx(	(
<g/>
nově	nově	k6eAd1	nově
3	[number]	k4	3
<g/>
.	.	kIx.	.
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
soutěž	soutěž	k1gFnSc1	soutěž
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2018	[number]	k4	2018
<g/>
/	/	kIx~	/
<g/>
19	[number]	k4	19
<g/>
:	:	kIx,	:
Klub	klub	k1gInSc1	klub
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgInS	zúčastnit
play-off	playff	k1gInSc1	play-off
o	o	k7c4	o
evropské	evropský	k2eAgInPc4d1	evropský
poháry	pohár	k1gInPc4	pohár
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
se	se	k3xPyFc4	se
utkal	utkat	k5eAaPmAgMnS	utkat
se	s	k7c7	s
Zlínem	Zlín	k1gInSc7	Zlín
<g/>
,	,	kIx,	,
kterému	který	k3yQgMnSc3	který
podlehl	podlehnout	k5eAaPmAgInS	podlehnout
až	až	k9	až
díky	díky	k7c3	díky
pravidlu	pravidlo	k1gNnSc3	pravidlo
venkovních	venkovní	k2eAgInPc2d1	venkovní
gólů	gól	k1gInPc2	gól
při	při	k7c6	při
celkové	celkový	k2eAgFnSc6d1	celková
remíze	remíza	k1gFnSc6	remíza
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
(	(	kIx(	(
<g/>
doma	doma	k6eAd1	doma
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
Sigma	sigma	k1gNnSc6	sigma
poměrem	poměr	k1gInSc7	poměr
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
tak	tak	k9	tak
Olomouc	Olomouc	k1gFnSc1	Olomouc
skončila	skončit	k5eAaPmAgFnS	skončit
na	na	k7c4	na
9	[number]	k4	9
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Stadion	stadion	k1gInSc1	stadion
==	==	k?	==
</s>
</p>
<p>
<s>
Se	s	k7c7	s
stavbou	stavba	k1gFnSc7	stavba
stadionu	stadion	k1gInSc2	stadion
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
je	být	k5eAaImIp3nS	být
spojen	spojen	k2eAgMnSc1d1	spojen
mecenáš	mecenáš	k1gMnSc1	mecenáš
olomouckého	olomoucký	k2eAgInSc2d1	olomoucký
fotbalu	fotbal	k1gInSc2	fotbal
a	a	k8xC	a
velkoobchodník	velkoobchodník	k1gMnSc1	velkoobchodník
Josef	Josef	k1gMnSc1	Josef
Ander	Ander	k1gMnSc1	Ander
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
získal	získat	k5eAaPmAgInS	získat
Andrův	Andrův	k2eAgInSc1d1	Andrův
stadion	stadion	k1gInSc1	stadion
své	svůj	k3xOyFgNnSc4	svůj
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
pracemi	práce	k1gFnPc7	práce
na	na	k7c6	na
stavbě	stavba	k1gFnSc6	stavba
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
byl	být	k5eAaImAgInS	být
stadion	stadion	k1gInSc1	stadion
s	s	k7c7	s
kapacitou	kapacita	k1gFnSc7	kapacita
20	[number]	k4	20
000	[number]	k4	000
diváků	divák	k1gMnPc2	divák
otevřen	otevřen	k2eAgInSc1d1	otevřen
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
války	válka	k1gFnSc2	válka
však	však	k9	však
ustupující	ustupující	k2eAgMnPc1d1	ustupující
němečtí	německý	k2eAgMnPc1d1	německý
vojáci	voják	k1gMnPc1	voják
vyhodili	vyhodit	k5eAaPmAgMnP	vyhodit
tribunu	tribuna	k1gFnSc4	tribuna
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
si	se	k3xPyFc3	se
předtím	předtím	k6eAd1	předtím
zřídili	zřídit	k5eAaPmAgMnP	zřídit
muniční	muniční	k2eAgInSc4d1	muniční
sklad	sklad	k1gInSc4	sklad
<g/>
,	,	kIx,	,
do	do	k7c2	do
povětří	povětří	k1gNnSc2	povětří
<g/>
.	.	kIx.	.
</s>
<s>
Provizorní	provizorní	k2eAgFnSc1d1	provizorní
dřevěná	dřevěný	k2eAgFnSc1d1	dřevěná
tribuna	tribuna	k1gFnSc1	tribuna
postavená	postavený	k2eAgFnSc1d1	postavená
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
sloužila	sloužit	k5eAaImAgFnS	sloužit
olomouckým	olomoucký	k2eAgMnPc3d1	olomoucký
fanouškům	fanoušek	k1gMnPc3	fanoušek
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1977	[number]	k4	1977
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
konečně	konečně	k6eAd1	konečně
začalo	začít	k5eAaPmAgNnS	začít
se	s	k7c7	s
stavbou	stavba	k1gFnSc7	stavba
nové	nový	k2eAgFnSc2d1	nová
hlavní	hlavní	k2eAgFnSc2d1	hlavní
tribuny	tribuna	k1gFnSc2	tribuna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
byla	být	k5eAaImAgFnS	být
přistavěna	přistavěn	k2eAgFnSc1d1	přistavěna
protilehlá	protilehlý	k2eAgFnSc1d1	protilehlá
tribuna	tribuna	k1gFnSc1	tribuna
a	a	k8xC	a
kapacita	kapacita	k1gFnSc1	kapacita
Androva	Andrův	k2eAgInSc2d1	Andrův
stadionu	stadion	k1gInSc2	stadion
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
na	na	k7c4	na
6	[number]	k4	6
000	[number]	k4	000
stojících	stojící	k2eAgMnPc2d1	stojící
diváků	divák	k1gMnPc2	divák
<g/>
.	.	kIx.	.
</s>
<s>
Klub	klub	k1gInSc1	klub
nadále	nadále	k6eAd1	nadále
modernizoval	modernizovat	k5eAaBmAgInS	modernizovat
areál	areál	k1gInSc1	areál
stadionu	stadion	k1gInSc2	stadion
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
byla	být	k5eAaImAgFnS	být
dostavěna	dostavěn	k2eAgFnSc1d1	dostavěna
severní	severní	k2eAgFnSc1d1	severní
tribuna	tribuna	k1gFnSc1	tribuna
a	a	k8xC	a
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
se	s	k7c7	s
stavbou	stavba	k1gFnSc7	stavba
tribuny	tribuna	k1gFnSc2	tribuna
jih	jih	k1gInSc1	jih
<g/>
.	.	kIx.	.
</s>
<s>
Kapacita	kapacita	k1gFnSc1	kapacita
Androva	Andrův	k2eAgInSc2d1	Andrův
stadionu	stadion	k1gInSc2	stadion
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
vyšplhala	vyšplhat	k5eAaPmAgFnS	vyšplhat
na	na	k7c4	na
12	[number]	k4	12
566	[number]	k4	566
diváků	divák	k1gMnPc2	divák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Účast	účast	k1gFnSc1	účast
v	v	k7c6	v
evropských	evropský	k2eAgInPc6d1	evropský
pohárech	pohár	k1gInPc6	pohár
==	==	k?	==
</s>
</p>
<p>
<s>
Legenda	legenda	k1gFnSc1	legenda
<g/>
:	:	kIx,	:
SEP	SEP	kA	SEP
–	–	k?	–
Středoevropský	středoevropský	k2eAgInSc4d1	středoevropský
pohár	pohár	k1gInSc4	pohár
<g/>
,	,	kIx,	,
VP	VP	kA	VP
–	–	k?	–
Veletržní	veletržní	k2eAgInSc4d1	veletržní
pohár	pohár	k1gInSc4	pohár
<g/>
,	,	kIx,	,
PMEZ	PMEZ	kA	PMEZ
–	–	k?	–
Pohár	pohár	k1gInSc1	pohár
mistrů	mistr	k1gMnPc2	mistr
evropských	evropský	k2eAgFnPc2d1	Evropská
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
PVP	PVP	kA	PVP
–	–	k?	–
Pohár	pohár	k1gInSc4	pohár
vítězů	vítěz	k1gMnPc2	vítěz
pohárů	pohár	k1gInPc2	pohár
<g/>
,	,	kIx,	,
LM	LM	kA	LM
–	–	k?	–
Liga	liga	k1gFnSc1	liga
mistrů	mistr	k1gMnPc2	mistr
UEFA	UEFA	kA	UEFA
<g/>
,	,	kIx,	,
UEFA	UEFA	kA	UEFA
–	–	k?	–
Pohár	pohár	k1gInSc1	pohár
UEFA	UEFA	kA	UEFA
<g/>
,	,	kIx,	,
EL	Ela	k1gFnPc2	Ela
–	–	k?	–
Evropská	evropský	k2eAgFnSc1d1	Evropská
liga	liga	k1gFnSc1	liga
UEFA	UEFA	kA	UEFA
<g/>
,	,	kIx,	,
SP	SP	kA	SP
–	–	k?	–
Superpohár	superpohár	k1gInSc1	superpohár
UEFA	UEFA	kA	UEFA
<g/>
,	,	kIx,	,
PI	pi	k0	pi
–	–	k?	–
Pohár	pohár	k1gInSc4	pohár
Intertoto	Intertota	k1gFnSc5	Intertota
<g/>
,	,	kIx,	,
IP	IP	kA	IP
–	–	k?	–
Interkontinentální	interkontinentální	k2eAgInSc4d1	interkontinentální
pohár	pohár	k1gInSc4	pohár
<g/>
,	,	kIx,	,
MS	MS	kA	MS
–	–	k?	–
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
klubů	klub	k1gInPc2	klub
</s>
</p>
<p>
<s>
UEFA	UEFA	kA	UEFA
1986	[number]	k4	1986
<g/>
/	/	kIx~	/
<g/>
1987	[number]	k4	1987
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
kolo	kolo	k1gNnSc1	kolo
</s>
</p>
<p>
<s>
UEFA	UEFA	kA	UEFA
1991	[number]	k4	1991
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
–	–	k?	–
Čtvrtfinále	čtvrtfinále	k1gNnSc1	čtvrtfinále
</s>
</p>
<p>
<s>
UEFA	UEFA	kA	UEFA
1992	[number]	k4	1992
<g/>
/	/	kIx~	/
<g/>
1993	[number]	k4	1993
–	–	k?	–
3	[number]	k4	3
<g/>
.	.	kIx.	.
kolo	kolo	k1gNnSc1	kolo
</s>
</p>
<p>
<s>
UEFA	UEFA	kA	UEFA
1996	[number]	k4	1996
<g/>
/	/	kIx~	/
<g/>
1997	[number]	k4	1997
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
kolo	kolo	k1gNnSc1	kolo
</s>
</p>
<p>
<s>
UEFA	UEFA	kA	UEFA
1998	[number]	k4	1998
<g/>
/	/	kIx~	/
<g/>
1999	[number]	k4	1999
–	–	k?	–
2	[number]	k4	2
<g/>
.	.	kIx.	.
kolo	kolo	k1gNnSc1	kolo
</s>
</p>
<p>
<s>
UEFA	UEFA	kA	UEFA
1999	[number]	k4	1999
<g/>
/	/	kIx~	/
<g/>
2000	[number]	k4	2000
–	–	k?	–
2	[number]	k4	2
<g/>
.	.	kIx.	.
kolo	kolo	k1gNnSc1	kolo
</s>
</p>
<p>
<s>
PI	pi	k0	pi
2000	[number]	k4	2000
–	–	k?	–
Finále	finále	k1gNnSc1	finále
</s>
</p>
<p>
<s>
UEFA	UEFA	kA	UEFA
2001	[number]	k4	2001
<g/>
/	/	kIx~	/
<g/>
2002	[number]	k4	2002
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
kolo	kolo	k1gNnSc1	kolo
</s>
</p>
<p>
<s>
UEFA	UEFA	kA	UEFA
2002	[number]	k4	2002
<g/>
/	/	kIx~	/
<g/>
2003	[number]	k4	2003
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
kolo	kolo	k1gNnSc1	kolo
</s>
</p>
<p>
<s>
UEFA	UEFA	kA	UEFA
2004	[number]	k4	2004
<g/>
/	/	kIx~	/
<g/>
2005	[number]	k4	2005
–	–	k?	–
2	[number]	k4	2
<g/>
.	.	kIx.	.
kolo	kolo	k1gNnSc1	kolo
</s>
</p>
<p>
<s>
PI	pi	k0	pi
2005	[number]	k4	2005
–	–	k?	–
Semifinále	semifinále	k1gNnSc2	semifinále
</s>
</p>
<p>
<s>
EL	Ela	k1gFnPc2	Ela
2009	[number]	k4	2009
<g/>
/	/	kIx~	/
<g/>
2010	[number]	k4	2010
–	–	k?	–
4	[number]	k4	4
<g/>
.	.	kIx.	.
předkolo	předkolo	k1gNnSc1	předkolo
</s>
</p>
<p>
<s>
EL	Ela	k1gFnPc2	Ela
2018	[number]	k4	2018
<g/>
/	/	kIx~	/
<g/>
2019	[number]	k4	2019
–	–	k?	–
4	[number]	k4	4
<g/>
.	.	kIx.	.
předkolo	předkolo	k1gNnSc1	předkolo
</s>
</p>
<p>
<s>
==	==	k?	==
Klubové	klubový	k2eAgFnPc1d1	klubová
statistiky	statistika	k1gFnPc1	statistika
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Mezinárodní	mezinárodní	k2eAgInPc4d1	mezinárodní
úspěchy	úspěch	k1gInPc4	úspěch
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Známí	známý	k2eAgMnPc1d1	známý
odchovanci	odchovanec	k1gMnPc1	odchovanec
/	/	kIx~	/
hráči	hráč	k1gMnPc1	hráč
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Nejúspěšnější	úspěšný	k2eAgMnPc1d3	nejúspěšnější
trenéři	trenér	k1gMnPc1	trenér
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Přehled	přehled	k1gInSc4	přehled
trenérů	trenér	k1gMnPc2	trenér
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
SK	Sk	kA	Sk
Sigma	sigma	k1gNnSc1	sigma
Olomouc	Olomouc	k1gFnSc1	Olomouc
"	"	kIx"	"
<g/>
B	B	kA	B
<g/>
"	"	kIx"	"
==	==	k?	==
</s>
</p>
<p>
<s>
SK	Sk	kA	Sk
Sigma	sigma	k1gNnSc1	sigma
Olomouc	Olomouc	k1gFnSc1	Olomouc
"	"	kIx"	"
<g/>
B	B	kA	B
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
rezervní	rezervní	k2eAgInSc4d1	rezervní
tým	tým	k1gInSc4	tým
olomoucké	olomoucký	k2eAgFnSc2d1	olomoucká
Sigmy	Sigma	k1gFnSc2	Sigma
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
působí	působit	k5eAaImIp3nS	působit
v	v	k7c6	v
Moravskoslezské	moravskoslezský	k2eAgFnSc6d1	Moravskoslezská
fotbalové	fotbalový	k2eAgFnSc6d1	fotbalová
lize	liga	k1gFnSc6	liga
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
soutěž	soutěž	k1gFnSc1	soutěž
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rezervní	rezervní	k2eAgInSc1d1	rezervní
tým	tým	k1gInSc1	tým
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
po	po	k7c6	po
sloučení	sloučení	k1gNnSc6	sloučení
se	s	k7c7	s
Slovanem	Slovan	k1gInSc7	Slovan
Černovír	Černovír	k1gMnSc1	Černovír
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2014	[number]	k4	2014
<g/>
/	/	kIx~	/
<g/>
15	[number]	k4	15
si	se	k3xPyFc3	se
klub	klub	k1gInSc1	klub
zajistil	zajistit	k5eAaPmAgInS	zajistit
postup	postup	k1gInSc4	postup
do	do	k7c2	do
2	[number]	k4	2
<g/>
.	.	kIx.	.
ligy	liga	k1gFnPc4	liga
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
v	v	k7c4	v
27	[number]	k4	27
<g/>
.	.	kIx.	.
kole	kolo	k1gNnSc6	kolo
MSFL	MSFL	kA	MSFL
porazil	porazit	k5eAaPmAgInS	porazit
Zábřeh	Zábřeh	k1gInSc1	Zábřeh
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
bylo	být	k5eAaImAgNnS	být
béčko	béčko	k1gNnSc1	béčko
dočasně	dočasně	k6eAd1	dočasně
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
kvůli	kvůli	k7c3	kvůli
existenci	existence	k1gFnSc3	existence
Juniorské	juniorský	k2eAgFnSc2d1	juniorská
ligy	liga	k1gFnSc2	liga
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
obnovení	obnovení	k1gNnSc3	obnovení
rezervního	rezervní	k2eAgInSc2d1	rezervní
týmu	tým	k1gInSc2	tým
došlo	dojít	k5eAaPmAgNnS	dojít
po	po	k7c6	po
zrušení	zrušení	k1gNnSc6	zrušení
této	tento	k3xDgFnSc2	tento
mládežnické	mládežnický	k2eAgFnSc2d1	mládežnická
ligy	liga	k1gFnSc2	liga
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2019	[number]	k4	2019
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Umístění	umístění	k1gNnSc1	umístění
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
sezonách	sezona	k1gFnPc6	sezona
===	===	k?	===
</s>
</p>
<p>
<s>
Stručný	stručný	k2eAgInSc1d1	stručný
přehledZdroj	přehledZdroj	k1gInSc1	přehledZdroj
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1984	[number]	k4	1984
<g/>
–	–	k?	–
<g/>
1991	[number]	k4	1991
<g/>
:	:	kIx,	:
Divize	divize	k1gFnSc2	divize
D	D	kA	D
</s>
</p>
<p>
<s>
1991	[number]	k4	1991
<g/>
–	–	k?	–
<g/>
2001	[number]	k4	2001
<g/>
:	:	kIx,	:
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1	Moravskoslezská
fotbalová	fotbalový	k2eAgFnSc1d1	fotbalová
liga	liga	k1gFnSc1	liga
</s>
</p>
<p>
<s>
2001	[number]	k4	2001
<g/>
–	–	k?	–
<g/>
2007	[number]	k4	2007
<g/>
:	:	kIx,	:
2	[number]	k4	2
<g/>
.	.	kIx.	.
liga	liga	k1gFnSc1	liga
</s>
</p>
<p>
<s>
2007	[number]	k4	2007
<g/>
–	–	k?	–
<g/>
2015	[number]	k4	2015
<g/>
:	:	kIx,	:
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1	Moravskoslezská
fotbalová	fotbalový	k2eAgFnSc1d1	fotbalová
liga	liga	k1gFnSc1	liga
</s>
</p>
<p>
<s>
2015	[number]	k4	2015
<g/>
–	–	k?	–
<g/>
2016	[number]	k4	2016
<g/>
:	:	kIx,	:
Fotbalová	fotbalový	k2eAgFnSc1d1	fotbalová
národní	národní	k2eAgFnSc1d1	národní
liga	liga	k1gFnSc1	liga
</s>
</p>
<p>
<s>
2016	[number]	k4	2016
<g/>
–	–	k?	–
<g/>
2017	[number]	k4	2017
<g/>
:	:	kIx,	:
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1	Moravskoslezská
fotbalová	fotbalový	k2eAgFnSc1d1	fotbalová
liga	liga	k1gFnSc1	liga
</s>
</p>
<p>
<s>
2017	[number]	k4	2017
<g/>
–	–	k?	–
<g/>
2019	[number]	k4	2019
<g/>
:	:	kIx,	:
bez	bez	k7c2	bez
soutěže	soutěž	k1gFnSc2	soutěž
</s>
</p>
<p>
<s>
2019	[number]	k4	2019
<g/>
–	–	k?	–
:	:	kIx,	:
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1	Moravskoslezská
fotbalová	fotbalový	k2eAgFnSc1d1	fotbalová
ligaJednotlivé	ligaJednotlivý	k2eAgInPc4d1	ligaJednotlivý
ročníkyZdroj	ročníkyZdroj	k1gInSc4	ročníkyZdroj
<g/>
:	:	kIx,	:
Legenda	legenda	k1gFnSc1	legenda
<g/>
:	:	kIx,	:
Z	Z	kA	Z
-	-	kIx~	-
zápasy	zápas	k1gInPc1	zápas
<g/>
,	,	kIx,	,
V	v	k7c6	v
-	-	kIx~	-
výhry	výhra	k1gFnPc4	výhra
<g/>
,	,	kIx,	,
R	R	kA	R
-	-	kIx~	-
remízy	remíz	k1gInPc1	remíz
<g/>
,	,	kIx,	,
P	P	kA	P
-	-	kIx~	-
porážky	porážka	k1gFnSc2	porážka
<g/>
,	,	kIx,	,
VG	VG	kA	VG
-	-	kIx~	-
vstřelené	vstřelený	k2eAgInPc1d1	vstřelený
góly	gól	k1gInPc1	gól
<g/>
,	,	kIx,	,
OG	OG	kA	OG
-	-	kIx~	-
obdržené	obdržený	k2eAgInPc4d1	obdržený
góly	gól	k1gInPc4	gól
<g/>
,	,	kIx,	,
+	+	kIx~	+
<g/>
/	/	kIx~	/
<g/>
-	-	kIx~	-
-	-	kIx~	-
rozdíl	rozdíl	k1gInSc1	rozdíl
skóre	skóre	k1gNnSc2	skóre
<g/>
,	,	kIx,	,
B	B	kA	B
-	-	kIx~	-
body	bod	k1gInPc1	bod
<g/>
,	,	kIx,	,
červené	červený	k2eAgNnSc1d1	červené
podbarvení	podbarvení	k1gNnSc1	podbarvení
-	-	kIx~	-
sestup	sestup	k1gInSc1	sestup
<g/>
,	,	kIx,	,
zelené	zelený	k2eAgNnSc1d1	zelené
podbarvení	podbarvení	k1gNnSc1	podbarvení
-	-	kIx~	-
postup	postup	k1gInSc1	postup
<g/>
,	,	kIx,	,
fialové	fialový	k2eAgNnSc1d1	fialové
podbarvení	podbarvení	k1gNnSc1	podbarvení
-	-	kIx~	-
reorganizace	reorganizace	k1gFnSc1	reorganizace
<g/>
,	,	kIx,	,
změna	změna	k1gFnSc1	změna
skupiny	skupina	k1gFnSc2	skupina
či	či	k8xC	či
soutěže	soutěž	k1gFnSc2	soutěž
</s>
</p>
<p>
<s>
Poznámky	poznámka	k1gFnPc1	poznámka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1990	[number]	k4	1990
<g/>
/	/	kIx~	/
<g/>
91	[number]	k4	91
<g/>
:	:	kIx,	:
Po	po	k7c6	po
sezóně	sezóna	k1gFnSc6	sezóna
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
reorganizaci	reorganizace	k1gFnSc3	reorganizace
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
,	,	kIx,	,
prvních	první	k4xOgNnPc6	první
5	[number]	k4	5
nejlepších	dobrý	k2eAgNnPc2d3	nejlepší
mužstev	mužstvo	k1gNnPc2	mužstvo
postoupilo	postoupit	k5eAaPmAgNnS	postoupit
do	do	k7c2	do
nově	nově	k6eAd1	nově
vzniknuvší	vzniknuvší	k2eAgFnSc2d1	vzniknuvší
MSFL	MSFL	kA	MSFL
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
soutěž	soutěž	k1gFnSc1	soutěž
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2007	[number]	k4	2007
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
:	:	kIx,	:
Rezerva	rezerva	k1gFnSc1	rezerva
olomoucké	olomoucký	k2eAgFnSc2d1	olomoucká
Sigmy	Sigma	k1gFnSc2	Sigma
prodala	prodat	k5eAaPmAgFnS	prodat
licenci	licence	k1gFnSc4	licence
na	na	k7c4	na
2	[number]	k4	2
<g/>
.	.	kIx.	.
ligu	liga	k1gFnSc4	liga
týmu	tým	k1gInSc2	tým
Karviné	Karviná	k1gFnSc2	Karviná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
HABRCETL	HABRCETL	kA	HABRCETL
<g/>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
<g/>
.	.	kIx.	.
</s>
<s>
Fotbalové	fotbalový	k2eAgInPc1d1	fotbalový
stadiony	stadion	k1gInPc1	stadion
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Volvox	Volvox	k1gInSc1	Volvox
Globator	Globator	k1gInSc1	Globator
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
319	[number]	k4	319
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7207	[number]	k4	7207
<g/>
-	-	kIx~	-
<g/>
448	[number]	k4	448
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
SK	Sk	kA	Sk
Sigma	sigma	k1gNnSc1	sigma
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
s.	s.	k?	s.
62	[number]	k4	62
–	–	k?	–
64	[number]	k4	64
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HORÁK	Horák	k1gMnSc1	Horák
<g/>
,	,	kIx,	,
Jindřich	Jindřich	k1gMnSc1	Jindřich
<g/>
;	;	kIx,	;
KRÁL	Král	k1gMnSc1	Král
<g/>
,	,	kIx,	,
Lubomír	Lubomír	k1gMnSc1	Lubomír
<g/>
.	.	kIx.	.
</s>
<s>
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
našeho	náš	k3xOp1gInSc2	náš
fotbalu	fotbal	k1gInSc2	fotbal
(	(	kIx(	(
<g/>
1896	[number]	k4	1896
–	–	k?	–
1996	[number]	k4	1996
<g/>
/	/	kIx~	/
<g/>
97	[number]	k4	97
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
701	[number]	k4	701
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85983	[number]	k4	85983
<g/>
-	-	kIx~	-
<g/>
22	[number]	k4	22
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
RŮŽIČKA	Růžička	k1gMnSc1	Růžička
<g/>
,	,	kIx,	,
Adolf	Adolf	k1gMnSc1	Adolf
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
SK	Sk	kA	Sk
Sigma	sigma	k1gNnSc1	sigma
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Computer	computer	k1gInSc1	computer
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
88	[number]	k4	88
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
251	[number]	k4	251
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
201	[number]	k4	201
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Olomouc	Olomouc	k1gFnSc1	Olomouc
</s>
</p>
<p>
<s>
SK	Sk	kA	Sk
Olomouc	Olomouc	k1gFnSc1	Olomouc
ASO	ASO	kA	ASO
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
SK	Sk	kA	Sk
Sigma	sigma	k1gNnSc3	sigma
Olomouc	Olomouc	k1gFnSc1	Olomouc
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
www.sigmafotbal.cz	www.sigmafotbal.cz	k1gInSc1	www.sigmafotbal.cz
–	–	k?	–
oficiální	oficiální	k2eAgFnPc4d1	oficiální
stránky	stránka	k1gFnPc4	stránka
klubu	klub	k1gInSc2	klub
</s>
</p>
<p>
<s>
Sigma	sigma	k1gNnSc1	sigma
Olomouc	Olomouc	k1gFnSc1	Olomouc
v	v	k7c6	v
evropských	evropský	k2eAgInPc6d1	evropský
fotbalových	fotbalový	k2eAgInPc6d1	fotbalový
pohárech	pohár	k1gInPc6	pohár
</s>
</p>
<p>
<s>
SK	Sk	kA	Sk
Sigma	sigma	k1gNnSc1	sigma
Olomouc	Olomouc	k1gFnSc1	Olomouc
</s>
</p>
