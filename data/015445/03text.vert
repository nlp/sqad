<s>
Rašeliníkovité	Rašeliníkovitý	k2eAgNnSc1d1
</s>
<s>
Rašeliníkovité	Rašeliníkovitý	k2eAgNnSc1d1
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc2
Říše	říš	k1gFnSc2
</s>
<s>
rostliny	rostlina	k1gFnPc1
(	(	kIx(
<g/>
Plantae	Plantae	k1gFnSc1
<g/>
)	)	kIx)
Podříše	podříše	k1gFnSc1
</s>
<s>
vyšší	vysoký	k2eAgFnPc1d2
rostliny	rostlina	k1gFnPc1
(	(	kIx(
<g/>
Embryobionta	Embryobionta	k1gFnSc1
<g/>
)	)	kIx)
Oddělení	oddělení	k1gNnSc1
</s>
<s>
mechy	mech	k1gInPc1
(	(	kIx(
<g/>
Bryophyta	Bryophyta	k1gFnSc1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
rašeliníky	rašeliník	k1gInPc1
(	(	kIx(
<g/>
Sphagnopsida	Sphagnopsida	k1gFnSc1
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
rašeliníkotvaré	rašeliníkotvarý	k2eAgFnPc1d1
(	(	kIx(
<g/>
Sphagnales	Sphagnales	k1gInSc1
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc1
</s>
<s>
rašeliníkovité	rašeliníkovitý	k2eAgInPc4d1
(	(	kIx(
<g/>
Sphagnaceae	Sphagnacea	k1gInPc4
<g/>
)	)	kIx)
Rody	rod	k1gInPc4
</s>
<s>
Sphagnum	Sphagnum	k1gInSc1
</s>
<s>
†	†	k?
<g/>
Sphagnophyllites	Sphagnophyllites	k1gInSc1
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Rašeliníkovité	Rašeliníkovitý	k2eAgInPc4d1
(	(	kIx(
<g/>
Sphagnaceae	Sphagnacea	k1gInPc4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
čeleď	čeleď	k1gFnSc1
mechů	mech	k1gInPc2
z	z	k7c2
monotypického	monotypický	k2eAgInSc2d1
řádu	řád	k1gInSc2
rašeliníkotvaré	rašeliníkotvarý	k2eAgFnSc2d1
(	(	kIx(
<g/>
Sphagnales	Sphagnalesa	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současnosti	současnost	k1gFnSc6
obsahuje	obsahovat	k5eAaImIp3nS
jediný	jediný	k2eAgInSc1d1
recentní	recentní	k2eAgInSc1d1
rod	rod	k1gInSc1
<g/>
,	,	kIx,
rašeliník	rašeliník	k1gInSc1
(	(	kIx(
<g/>
Sphagnum	Sphagnum	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
vyhynulým	vyhynulý	k2eAgInPc3d1
rodům	rod	k1gInPc3
této	tento	k3xDgFnSc2
čeledi	čeleď	k1gFnSc2
patří	patřit	k5eAaImIp3nS
například	například	k6eAd1
Sphagnophyllites	Sphagnophyllites	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rod	rod	k1gInSc1
rašeliník	rašeliník	k1gInSc4
zahrnuje	zahrnovat	k5eAaImIp3nS
asi	asi	k9
330	#num#	k4
druhů	druh	k1gInPc2
<g/>
,	,	kIx,
navzájem	navzájem	k6eAd1
velmi	velmi	k6eAd1
obtížně	obtížně	k6eAd1
rozlišitelných	rozlišitelný	k2eAgFnPc2d1
<g/>
.	.	kIx.
</s>
<s>
Charakteristika	charakteristika	k1gFnSc1
</s>
<s>
Protonema	Protonema	k1gFnSc1
(	(	kIx(
<g/>
prvoklíček	prvoklíček	k1gInSc1
<g/>
)	)	kIx)
těchto	tento	k3xDgInPc2
mechů	mech	k1gInPc2
je	být	k5eAaImIp3nS
lupenitá	lupenitý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mechové	mechový	k2eAgFnPc4d1
rostlinky	rostlinka	k1gFnPc4
jsou	být	k5eAaImIp3nP
bez	bez	k7c2
rhizoidů	rhizoid	k1gInPc2
<g/>
,	,	kIx,
lodyžky	lodyžka	k1gFnPc1
jsou	být	k5eAaImIp3nP
neukončeného	ukončený	k2eNgInSc2d1
růstu	růst	k1gInSc2
se	se	k3xPyFc4
svazečky	svazeček	k1gInPc1
větviček	větvička	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
jsou	být	k5eAaImIp3nP
na	na	k7c6
koncích	konec	k1gInPc6
lodyžek	lodyžka	k1gFnPc2
nahloučeny	nahloučit	k5eAaPmNgInP
v	v	k7c4
hlavičku	hlavička	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lístky	lístek	k1gInPc4
jsou	být	k5eAaImIp3nP
bezžebré	bezžebrý	k2eAgInPc4d1
<g/>
,	,	kIx,
tvořeny	tvořen	k2eAgInPc4d1
úzkými	úzký	k2eAgFnPc7d1
zelenými	zelený	k2eAgFnPc7d1
buňkami	buňka	k1gFnPc7
-	-	kIx~
chlorocysty	chlorocyst	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
obsahují	obsahovat	k5eAaImIp3nP
četné	četný	k2eAgInPc1d1
chloroplasty	chloroplast	k1gInPc1
a	a	k8xC
skládají	skládat	k5eAaImIp3nP
jakousi	jakýsi	k3yIgFnSc4
sít	sít	k5eAaImF
<g/>
,	,	kIx,
jejíchž	jejíž	k3xOyRp3gFnPc2
oka	oko	k1gNnPc1
jsou	být	k5eAaImIp3nP
vyplněna	vyplnit	k5eAaPmNgFnS
bezbarvými	bezbarvý	k2eAgFnPc7d1
mrtvými	mrtvý	k2eAgFnPc7d1
buňkami	buňka	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tobolka	tobolka	k1gFnSc1
je	být	k5eAaImIp3nS
kulovitá	kulovitý	k2eAgFnSc1d1
<g/>
,	,	kIx,
bez	bez	k7c2
čepičky	čepička	k1gFnSc2
<g/>
,	,	kIx,
uvnitř	uvnitř	k6eAd1
je	být	k5eAaImIp3nS
mohutná	mohutný	k2eAgFnSc1d1
kolumela	kolumela	k1gFnSc1
<g/>
,	,	kIx,
nad	nad	k7c7
níž	jenž	k3xRgFnSc7
je	být	k5eAaImIp3nS
rozprostřen	rozprostřen	k2eAgInSc1d1
archespor	archespor	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
rašeliníkovité	rašeliníkovitý	k2eAgFnPc4d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Rostliny	rostlina	k1gFnPc1
</s>
