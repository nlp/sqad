<s>
Hrvoje	Hrvoj	k1gInPc1
Šarinić	Šarinić	k1gFnSc2
</s>
<s>
Hrvoje	Hrvoj	k1gInPc1
Šarinić	Šarinić	k1gMnPc2
Narození	narození	k1gNnSc2
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1935	#num#	k4
<g/>
Sušak	Sušak	k1gInSc1
Úmrtí	úmrtí	k1gNnSc2
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2017	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
82	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Záhřeb	Záhřeb	k1gInSc1
Místo	místo	k7c2
pohřbení	pohřbení	k1gNnSc2
</s>
<s>
Mirogoj	Mirogoj	k1gInSc1
Národnost	národnost	k1gFnSc1
</s>
<s>
Chorvati	Chorvat	k1gMnPc1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
Záhřebská	záhřebský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
politik	politik	k1gMnSc1
a	a	k8xC
architekt	architekt	k1gMnSc1
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Řád	řád	k1gInSc1
Anteho	Ante	k1gMnSc2
Starčeviće	Starčević	k1gMnSc2
Politická	politický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
</s>
<s>
Chorvatské	chorvatský	k2eAgNnSc4d1
demokratické	demokratický	k2eAgNnSc4d1
společenství	společenství	k1gNnSc4
Funkce	funkce	k1gFnSc2
</s>
<s>
Prime	prim	k1gInSc5
Minister	Ministrum	k1gNnPc2
of	of	k?
Croatia	Croatius	k1gMnSc2
(	(	kIx(
<g/>
1992	#num#	k4
<g/>
–	–	k?
<g/>
1993	#num#	k4
<g/>
)	)	kIx)
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Hrvoje	Hrvoj	k1gInPc1
Šarinić	Šarinić	k1gFnSc2
(	(	kIx(
<g/>
17	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1935	#num#	k4
Rijeka	Rijeka	k1gFnSc1
–	–	k?
21	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2017	#num#	k4
Záhřeb	Záhřeb	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
chorvatský	chorvatský	k2eAgMnSc1d1
politik	politik	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
12	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1992	#num#	k4
do	do	k7c2
3	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1993	#num#	k4
zastával	zastávat	k5eAaImAgMnS
funkci	funkce	k1gFnSc4
premiéra	premiér	k1gMnSc2
Chorvatska	Chorvatsko	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc4
jakožto	jakožto	k8xS
představitel	představitel	k1gMnSc1
konzervativního	konzervativní	k2eAgNnSc2d1
Chorvatského	chorvatský	k2eAgNnSc2d1
demokratického	demokratický	k2eAgNnSc2d1
společenství	společenství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
letech	léto	k1gNnPc6
1963-1987	1963-1987	k4
působil	působit	k5eAaImAgMnS
ve	v	k7c6
Francii	Francie	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
získal	získat	k5eAaPmAgMnS
francouzské	francouzský	k2eAgNnSc4d1
občanství	občanství	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1990	#num#	k4
byl	být	k5eAaImAgInS
blízkým	blízký	k2eAgMnSc7d1
spolupracovníkem	spolupracovník	k1gMnSc7
a	a	k8xC
vedoucím	vedoucí	k1gMnSc7
kanceláře	kancelář	k1gFnSc2
prezidenta	prezident	k1gMnSc4
Franjo	Franjo	k1gMnSc1
Tuđmana	Tuđman	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1992	#num#	k4
ho	on	k3xPp3gInSc4
Tuđman	Tuđman	k1gMnSc1
jmenoval	jmenovat	k5eAaImAgMnS,k5eAaBmAgMnS
premiérem	premiér	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc1
vláda	vláda	k1gFnSc1
privatizovala	privatizovat	k5eAaImAgFnS
řadu	řada	k1gFnSc4
státních	státní	k2eAgFnPc2d1
firem	firma	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ekonomice	ekonomika	k1gFnSc6
se	se	k3xPyFc4
však	však	k9
nedařilo	dařit	k5eNaImAgNnS
a	a	k8xC
Šarinić	Šarinić	k1gFnPc1
se	se	k3xPyFc4
rychle	rychle	k6eAd1
stal	stát	k5eAaPmAgMnS
nepopulární	populární	k2eNgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
vedlo	vést	k5eAaImAgNnS
Tuđmana	Tuđman	k1gMnSc4
k	k	k7c3
jeho	jeho	k3xOp3gNnSc3
nahrazení	nahrazení	k1gNnSc3
Nikicou	Nikicý	k2eAgFnSc4d1
Valentićem	Valentić	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šarinić	Šarinić	k1gMnSc1
se	se	k3xPyFc4
pak	pak	k6eAd1
vrátil	vrátit	k5eAaPmAgMnS
na	na	k7c4
post	post	k1gInSc4
vedoucího	vedoucí	k1gMnSc2
prezidentské	prezidentský	k2eAgFnSc2d1
kanceláře	kancelář	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
s	s	k7c7
malými	malý	k2eAgFnPc7d1
přestávkami	přestávka	k1gFnPc7
držel	držet	k5eAaImAgMnS
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1998	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toho	ten	k3xDgInSc2
roku	rok	k1gInSc2
tvrdě	tvrdě	k6eAd1
zkritizoval	zkritizovat	k5eAaPmAgMnS
vysokého	vysoký	k2eAgMnSc4d1
představitele	představitel	k1gMnSc4
Chorvatského	chorvatský	k2eAgNnSc2d1
demokratického	demokratický	k2eAgNnSc2d1
společenství	společenství	k1gNnSc2
Iviću	Ivić	k2eAgFnSc4d1
Pašaliće	Pašaliće	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tuđman	Tuđman	k1gMnSc1
se	se	k3xPyFc4
ovšem	ovšem	k9
postavil	postavit	k5eAaPmAgMnS
ve	v	k7c6
sporu	spor	k1gInSc6
na	na	k7c4
Pašalićovu	Pašalićův	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
a	a	k8xC
Šarinić	Šarinić	k1gFnSc4
musel	muset	k5eAaImAgInS
prezidentskou	prezidentský	k2eAgFnSc4d1
kancelář	kancelář	k1gFnSc4
opustit	opustit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
spolu	spolu	k6eAd1
s	s	k7c7
dalšími	další	k2eAgMnPc7d1
nespokojenými	spokojený	k2eNgMnPc7d1
politiky	politik	k1gMnPc7
opustil	opustit	k5eAaPmAgMnS
Chorvatské	chorvatský	k2eAgNnSc4d1
demokratické	demokratický	k2eAgNnSc4d1
společenství	společenství	k1gNnSc4
a	a	k8xC
spoluzaložil	spoluzaložit	k5eAaPmAgInS
novou	nový	k2eAgFnSc4d1
středopravou	středopravý	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
Demokratický	demokratický	k2eAgInSc1d1
střed	střed	k1gInSc1
(	(	kIx(
<g/>
Demokratski	Demokratski	k1gNnSc1
centar	centara	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
však	však	k9
neuspěla	uspět	k5eNaPmAgFnS
a	a	k8xC
Šarinić	Šarinić	k1gFnSc1
se	se	k3xPyFc4
z	z	k7c2
politiky	politika	k1gFnSc2
stáhl	stáhnout	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Hrvoje	Hrvoj	k1gInSc2
Šarinić	Šarinić	k1gFnSc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Premiéři	premiér	k1gMnPc1
Chorvatska	Chorvatsko	k1gNnSc2
SR	SR	kA
Chorvatsko	Chorvatsko	k1gNnSc1
(	(	kIx(
<g/>
1945	#num#	k4
<g/>
–	–	k?
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
SR	SR	kA
Chorvatsko	Chorvatsko	k1gNnSc1
Vladimir	Vladimir	k1gMnSc1
Bakarić	Bakarić	k1gMnSc1
(	(	kIx(
<g/>
1945	#num#	k4
<g/>
–	–	k?
<g/>
1953	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Jakov	Jakov	k1gInSc1
Blažević	Blažević	k1gFnSc1
(	(	kIx(
<g/>
1953	#num#	k4
<g/>
–	–	k?
<g/>
1962	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Zvonko	Zvonko	k1gNnSc1
Brkić	Brkić	k1gMnSc1
(	(	kIx(
<g/>
1962	#num#	k4
<g/>
–	–	k?
<g/>
1963	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Mika	Mik	k1gMnSc4
Špiljak	Špiljak	k1gMnSc1
(	(	kIx(
<g/>
1963	#num#	k4
<g/>
–	–	k?
<g/>
1967	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Savka	savka	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Dabčević-Kučar	Dabčević-Kučar	k1gInSc1
(	(	kIx(
<g/>
1967	#num#	k4
<g/>
–	–	k?
<g/>
1969	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Dragutin	Dragutin	k1gMnSc1
Haramija	Haramija	k1gMnSc1
(	(	kIx(
<g/>
1969	#num#	k4
<g/>
–	–	k?
<g/>
1971	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ivo	Ivo	k1gMnSc1
Perišin	Perišin	k2eAgMnSc1d1
(	(	kIx(
<g/>
1971	#num#	k4
<g/>
–	–	k?
<g/>
1974	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Jakov	Jakov	k1gInSc1
Sirotković	Sirotković	k1gFnSc1
(	(	kIx(
<g/>
1974	#num#	k4
<g/>
–	–	k?
<g/>
1978	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Petar	Petar	k1gMnSc1
Fleković	Fleković	k1gMnSc1
(	(	kIx(
<g/>
1978	#num#	k4
<g/>
–	–	k?
<g/>
1982	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ante	Ant	k1gMnSc5
Marković	Marković	k1gMnSc5
(	(	kIx(
<g/>
1982	#num#	k4
<g/>
–	–	k?
<g/>
1985	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Antun	Antun	k1gMnSc1
Milović	Milović	k1gMnSc1
(	(	kIx(
<g/>
1986	#num#	k4
<g/>
–	–	k?
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
Republika	republika	k1gFnSc1
Chorvatsko	Chorvatsko	k1gNnSc4
(	(	kIx(
<g/>
od	od	k7c2
1990	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Republika	republika	k1gFnSc1
Chorvatsko	Chorvatsko	k1gNnSc1
Stjepan	Stjepan	k1gMnSc1
Mesić	Mesić	k1gMnSc1
(	(	kIx(
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Josip	Josip	k1gMnSc1
Manolić	Manolić	k1gMnSc1
(	(	kIx(
<g/>
1990	#num#	k4
<g/>
–	–	k?
<g/>
1991	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Franjo	Franjo	k1gMnSc1
Gregurić	Gregurić	k1gMnSc1
(	(	kIx(
<g/>
1991	#num#	k4
<g/>
–	–	k?
<g/>
1992	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Hrvoje	Hrvoj	k1gInSc2
Šarinić	Šarinić	k1gFnPc2
(	(	kIx(
<g/>
1992	#num#	k4
<g/>
–	–	k?
<g/>
1993	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Nikica	Nikica	k1gMnSc1
Valentić	Valentić	k1gMnSc1
(	(	kIx(
<g/>
1993	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
–	–	k?
<g/>
1995	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Zlatko	Zlatka	k1gFnSc5
Mateša	Matešum	k1gNnPc1
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
–	–	k?
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ivica	Ivica	k1gMnSc1
Račan	Račan	k1gMnSc1
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
–	–	k?
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ivo	Ivo	k1gMnSc1
Sanader	Sanader	k1gMnSc1
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
–	–	k?
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Jadranka	Jadranka	k1gFnSc1
Kosorová	Kosorová	k1gFnSc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
–	–	k?
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Zoran	Zoran	k1gMnSc1
Milanović	Milanović	k1gMnSc1
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
–	–	k?
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Tihomir	Tihomir	k1gMnSc1
Orešković	Orešković	k1gMnSc1
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Andrej	Andrej	k1gMnSc1
Plenković	Plenković	k1gMnSc1
(	(	kIx(
<g/>
od	od	k7c2
2016	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jo	jo	k9
<g/>
2013778279	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
121312402	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
5690	#num#	k4
5248	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
96037512	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
315237570	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
96037512	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Chorvatsko	Chorvatsko	k1gNnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
|	|	kIx~
Politika	politika	k1gFnSc1
</s>
