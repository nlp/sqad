<s>
Egejské	egejský	k2eAgNnSc1d1	Egejské
moře	moře	k1gNnSc1	moře
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
Α	Α	k?	Α
Π	Π	k?	Π
<g/>
,	,	kIx,	,
Aigaio	Aigaio	k1gMnSc1	Aigaio
Pelagos	Pelagos	k1gMnSc1	Pelagos
[	[	kIx(	[
<g/>
egeo	egeo	k1gMnSc1	egeo
pelagos	pelagos	k1gMnSc1	pelagos
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
turecky	turecky	k6eAd1	turecky
Ege	Ege	k1gFnPc4	Ege
Denizi	Denize	k1gFnSc4	Denize
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
polouzavřené	polouzavřený	k2eAgNnSc4d1	polouzavřené
moře	moře	k1gNnSc4	moře
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
Balkánským	balkánský	k2eAgInSc7d1	balkánský
poloostrovem	poloostrov	k1gInSc7	poloostrov
<g/>
,	,	kIx,	,
ostrovem	ostrov	k1gInSc7	ostrov
Krétou	Kréta	k1gFnSc7	Kréta
a	a	k8xC	a
Anatolií	Anatolie	k1gFnSc7	Anatolie
<g/>
,	,	kIx,	,
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
jeho	jeho	k3xOp3gFnSc2	jeho
plochy	plocha	k1gFnSc2	plocha
patří	patřit	k5eAaImIp3nP	patřit
Řecku	Řecko	k1gNnSc3	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
rozloha	rozloha	k1gFnSc1	rozloha
činí	činit	k5eAaImIp3nS	činit
190000	[number]	k4	190000
km2	km2	k4	km2
<g/>
,	,	kIx,	,
maximální	maximální	k2eAgFnSc1d1	maximální
hloubka	hloubka	k1gFnSc1	hloubka
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
2561	[number]	k4	2561
m	m	kA	m
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Egejském	egejský	k2eAgNnSc6d1	Egejské
moři	moře	k1gNnSc6	moře
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
,	,	kIx,	,
celá	celý	k2eAgFnSc1d1	celá
oblast	oblast	k1gFnSc1	oblast
Středozemí	Středozem	k1gFnPc2	Středozem
je	být	k5eAaImIp3nS	být
i	i	k9	i
neustále	neustále	k6eAd1	neustále
seismicky	seismicky	k6eAd1	seismicky
aktivní	aktivní	k2eAgFnSc1d1	aktivní
<g/>
.	.	kIx.	.
</s>
<s>
Drtivá	drtivý	k2eAgFnSc1d1	drtivá
většina	většina	k1gFnSc1	většina
ostrovů	ostrov	k1gInPc2	ostrov
v	v	k7c6	v
Egejském	egejský	k2eAgNnSc6d1	Egejské
moři	moře	k1gNnSc6	moře
jsou	být	k5eAaImIp3nP	být
řecké	řecký	k2eAgInPc1d1	řecký
ostrovy	ostrov	k1gInPc1	ostrov
(	(	kIx(	(
<g/>
Kyklady	Kyklady	k1gFnPc1	Kyklady
<g/>
,	,	kIx,	,
Sporady	Sporad	k1gInPc1	Sporad
<g/>
,	,	kIx,	,
ostrovy	ostrov	k1gInPc1	ostrov
Lémnos	Lémnosa	k1gFnPc2	Lémnosa
<g/>
,	,	kIx,	,
Lesbos	Lesbos	k1gInSc1	Lesbos
<g/>
,	,	kIx,	,
Chios	Chios	k1gInSc1	Chios
<g/>
,	,	kIx,	,
Ikaria	Ikarium	k1gNnPc1	Ikarium
<g/>
,	,	kIx,	,
Samos	Samos	k1gInSc1	Samos
<g/>
,	,	kIx,	,
Kós	Kós	k1gInSc1	Kós
<g/>
,	,	kIx,	,
Rhodos	Rhodos	k1gInSc1	Rhodos
<g/>
,	,	kIx,	,
Samothraké	Samothraký	k2eAgInPc1d1	Samothraký
a	a	k8xC	a
Thassos	Thassos	k1gInSc1	Thassos
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Turecka	Turecko	k1gNnSc2	Turecko
Menderes	Menderes	k1gInSc4	Menderes
Nehri	Nehri	k1gNnSc2	Nehri
Gediz	Gediza	k1gFnPc2	Gediza
Nehri	Nehr	k1gFnSc2	Nehr
-	-	kIx~	-
vlévá	vlévat	k5eAaImIp3nS	vlévat
se	se	k3xPyFc4	se
u	u	k7c2	u
İ	İ	k?	İ
Meriç	Meriç	k1gFnSc2	Meriç
Nehri	Nehr	k1gFnSc2	Nehr
(	(	kIx(	(
<g/>
Marica	Marica	k1gFnSc1	Marica
<g/>
)	)	kIx)	)
Z	z	k7c2	z
Řecka	Řecko	k1gNnSc2	Řecko
Néstros	Néstrosa	k1gFnPc2	Néstrosa
Strymón	Strymón	k1gMnSc1	Strymón
(	(	kIx(	(
<g/>
Struma	struma	k1gFnSc1	struma
<g/>
)	)	kIx)	)
Axiós	Axiós	k1gInSc1	Axiós
(	(	kIx(	(
<g/>
Vardar	Vardar	k1gInSc1	Vardar
<g/>
)	)	kIx)	)
-	-	kIx~	-
vlévá	vlévat	k5eAaImIp3nS	vlévat
se	se	k3xPyFc4	se
u	u	k7c2	u
Soluně	Soluň	k1gFnSc2	Soluň
Péneiós	Péneiósa	k1gFnPc2	Péneiósa
Na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Egejského	egejský	k2eAgNnSc2d1	Egejské
moře	moře	k1gNnSc2	moře
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
tyto	tento	k3xDgInPc1	tento
řecké	řecký	k2eAgInPc1d1	řecký
a	a	k8xC	a
turecké	turecký	k2eAgInPc1d1	turecký
přístavy	přístav	k1gInPc1	přístav
<g/>
:	:	kIx,	:
Izmir	Izmir	k1gInSc1	Izmir
Kabála	Kabál	k1gMnSc2	Kabál
Soluň	Soluň	k1gFnSc1	Soluň
Bólos	Bólos	k1gMnSc1	Bólos
Atény	Atény	k1gFnPc1	Atény
Iraklion	Iraklion	k1gInSc4	Iraklion
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
Egejské	egejský	k2eAgNnSc1d1	Egejské
moře	moře	k1gNnSc1	moře
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Egejské	egejský	k2eAgFnSc2d1	Egejská
moře	moře	k1gNnSc4	moře
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
