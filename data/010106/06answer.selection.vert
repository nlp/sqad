<s>
Při	při	k7c6	při
cestách	cesta	k1gFnPc6	cesta
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Schengenského	schengenský	k2eAgInSc2d1	schengenský
prostoru	prostor	k1gInSc2	prostor
a	a	k8xC	a
do	do	k7c2	do
některých	některý	k3yIgInPc2	některý
dalších	další	k2eAgInPc2d1	další
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yRgInPc7	který
má	mít	k5eAaImIp3nS	mít
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
uzavřenou	uzavřený	k2eAgFnSc4d1	uzavřená
příslušnou	příslušný	k2eAgFnSc4d1	příslušná
dohodu	dohoda	k1gFnSc4	dohoda
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
do	do	k7c2	do
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
místo	místo	k7c2	místo
pasu	pas	k1gInSc2	pas
použít	použít	k5eAaPmF	použít
také	také	k9	také
občanský	občanský	k2eAgInSc4d1	občanský
průkaz	průkaz	k1gInSc4	průkaz
<g/>
.	.	kIx.	.
</s>
