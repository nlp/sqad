<p>
<s>
Cestovní	cestovní	k2eAgInSc4d1	cestovní
pas	pas	k1gInSc4	pas
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
žádost	žádost	k1gFnSc4	žádost
vydán	vydat	k5eAaPmNgInS	vydat
kterémukoliv	kterýkoliv	k3yIgMnSc3	kterýkoliv
občanu	občan	k1gMnSc3	občan
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
jako	jako	k8xC	jako
primární	primární	k2eAgInSc1d1	primární
identifikační	identifikační	k2eAgInSc1d1	identifikační
doklad	doklad	k1gInSc1	doklad
pro	pro	k7c4	pro
cesty	cesta	k1gFnPc4	cesta
do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Pas	pas	k6eAd1	pas
lze	lze	k6eAd1	lze
současně	současně	k6eAd1	současně
použít	použít	k5eAaPmF	použít
jako	jako	k8xC	jako
doklad	doklad	k1gInSc4	doklad
potvrzující	potvrzující	k2eAgNnSc4d1	potvrzující
občanství	občanství	k1gNnSc4	občanství
a	a	k8xC	a
jako	jako	k9	jako
osobní	osobní	k2eAgInSc1d1	osobní
doklad	doklad	k1gInSc1	doklad
totožnosti	totožnost	k1gFnSc2	totožnost
při	při	k7c6	při
většině	většina	k1gFnSc6	většina
vnitrostátních	vnitrostátní	k2eAgInPc2d1	vnitrostátní
úkonů	úkon	k1gInPc2	úkon
vyžadujících	vyžadující	k2eAgNnPc2d1	vyžadující
předložení	předložení	k1gNnPc2	předložení
dokladu	doklad	k1gInSc2	doklad
totožnosti	totožnost	k1gFnSc2	totožnost
<g/>
.	.	kIx.	.
</s>
<s>
Pas	pas	k1gInSc1	pas
<g/>
,	,	kIx,	,
vydávaný	vydávaný	k2eAgInSc1d1	vydávaný
Ministerstvem	ministerstvo	k1gNnSc7	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
<g/>
,	,	kIx,	,
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
majetkem	majetek	k1gInSc7	majetek
státu	stát	k1gInSc2	stát
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
kdykoliv	kdykoliv	k6eAd1	kdykoliv
odebrán	odebrat	k5eAaPmNgInS	odebrat
<g/>
.	.	kIx.	.
</s>
<s>
Občan	občan	k1gMnSc1	občan
je	být	k5eAaImIp3nS	být
oprávněn	oprávnit	k5eAaPmNgMnS	oprávnit
mít	mít	k5eAaImF	mít
více	hodně	k6eAd2	hodně
platných	platný	k2eAgInPc2d1	platný
pasů	pas	k1gInPc2	pas
současně	současně	k6eAd1	současně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Platné	platný	k2eAgInPc1d1	platný
cestovní	cestovní	k2eAgInPc1d1	cestovní
pasy	pas	k1gInPc1	pas
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
jsou	být	k5eAaImIp3nP	být
vydávány	vydávat	k5eAaImNgFnP	vydávat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
329	[number]	k4	329
<g/>
/	/	kIx~	/
<g/>
1999	[number]	k4	1999
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
cestovních	cestovní	k2eAgInPc6d1	cestovní
dokladech	doklad	k1gInPc6	doklad
<g/>
,	,	kIx,	,
ve	v	k7c6	v
znění	znění	k1gNnSc6	znění
pozdějších	pozdní	k2eAgInPc2d2	pozdější
předpisů	předpis	k1gInPc2	předpis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
cestách	cesta	k1gFnPc6	cesta
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Schengenského	schengenský	k2eAgInSc2d1	schengenský
prostoru	prostor	k1gInSc2	prostor
a	a	k8xC	a
do	do	k7c2	do
některých	některý	k3yIgInPc2	některý
dalších	další	k2eAgInPc2d1	další
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yRgInPc7	který
má	mít	k5eAaImIp3nS	mít
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
uzavřenou	uzavřený	k2eAgFnSc4d1	uzavřená
příslušnou	příslušný	k2eAgFnSc4d1	příslušná
dohodu	dohoda	k1gFnSc4	dohoda
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
do	do	k7c2	do
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
místo	místo	k7c2	místo
pasu	pas	k1gInSc2	pas
použít	použít	k5eAaPmF	použít
také	také	k9	také
občanský	občanský	k2eAgInSc4d1	občanský
průkaz	průkaz	k1gInSc4	průkaz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dalšími	další	k2eAgInPc7d1	další
využívanými	využívaný	k2eAgInPc7d1	využívaný
cestovními	cestovní	k2eAgInPc7d1	cestovní
doklady	doklad	k1gInPc7	doklad
Česka	Česko	k1gNnSc2	Česko
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
pasy	pas	k1gInPc1	pas
<g/>
,	,	kIx,	,
vedle	vedle	k7c2	vedle
Cestovního	cestovní	k2eAgInSc2d1	cestovní
pasu	pas	k1gInSc2	pas
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
červený	červený	k2eAgInSc1d1	červený
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
též	též	k9	též
<g/>
:	:	kIx,	:
Diplomatický	diplomatický	k2eAgInSc4d1	diplomatický
pas	pas	k1gInSc4	pas
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
černý	černý	k2eAgInSc1d1	černý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Služební	služební	k2eAgInSc4d1	služební
pas	pas	k1gInSc4	pas
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
modrý	modrý	k2eAgInSc1d1	modrý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Event.	Event.	k1gMnSc1	Event.
blíže	blízce	k6eAd2	blízce
též	též	k9	též
<g/>
:	:	kIx,	:
Cestovní	cestovní	k2eAgInSc4d1	cestovní
průkaz	průkaz	k1gInSc4	průkaz
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Kdo	kdo	k3yQnSc1	kdo
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
může	moct	k5eAaImIp3nS	moct
o	o	k7c4	o
pas	pas	k1gInSc4	pas
zažádat	zažádat	k5eAaPmF	zažádat
==	==	k?	==
</s>
</p>
<p>
<s>
Občan	občan	k1gMnSc1	občan
starší	starší	k1gMnSc1	starší
15	[number]	k4	15
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
zletilosti	zletilost	k1gFnSc2	zletilost
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
do	do	k7c2	do
18	[number]	k4	18
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
k	k	k7c3	k
žádosti	žádost	k1gFnSc3	žádost
přiložit	přiložit	k5eAaPmF	přiložit
písemný	písemný	k2eAgInSc4d1	písemný
souhlas	souhlas	k1gInSc4	souhlas
zákonného	zákonný	k2eAgMnSc2d1	zákonný
zástupce	zástupce	k1gMnSc2	zástupce
s	s	k7c7	s
jeho	jeho	k3xOp3gInSc7	jeho
ověřeným	ověřený	k2eAgInSc7d1	ověřený
podpisem	podpis	k1gInSc7	podpis
<g/>
.	.	kIx.	.
</s>
<s>
Podpis	podpis	k1gInSc4	podpis
lze	lze	k6eAd1	lze
ověřit	ověřit	k5eAaPmF	ověřit
u	u	k7c2	u
orgánu	orgán	k1gInSc2	orgán
<g/>
,	,	kIx,	,
u	u	k7c2	u
něhož	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
podává	podávat	k5eAaImIp3nS	podávat
žádost	žádost	k1gFnSc1	žádost
<g/>
.	.	kIx.	.
</s>
<s>
Souhlas	souhlas	k1gInSc1	souhlas
zákonného	zákonný	k2eAgMnSc2d1	zákonný
zástupce	zástupce	k1gMnSc2	zástupce
se	se	k3xPyFc4	se
nevyžaduje	vyžadovat	k5eNaImIp3nS	vyžadovat
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnSc1	jeho
opatření	opatření	k1gNnSc1	opatření
spojeno	spojen	k2eAgNnSc1d1	spojeno
s	s	k7c7	s
překážkou	překážka	k1gFnSc7	překážka
těžko	těžko	k6eAd1	těžko
překonatelnou	překonatelný	k2eAgFnSc7d1	překonatelná
<g/>
.	.	kIx.	.
<g/>
Zákonný	zákonný	k2eAgMnSc1d1	zákonný
zástupce	zástupce	k1gMnSc1	zástupce
za	za	k7c2	za
občana	občan	k1gMnSc2	občan
mladšího	mladý	k2eAgMnSc2d2	mladší
15	[number]	k4	15
let	léto	k1gNnPc2	léto
<g/>
;	;	kIx,	;
místo	místo	k7c2	místo
zákonného	zákonný	k2eAgMnSc2d1	zákonný
zástupce	zástupce	k1gMnSc2	zástupce
může	moct	k5eAaImIp3nS	moct
podat	podat	k5eAaPmF	podat
pěstoun	pěstoun	k1gMnSc1	pěstoun
<g/>
,	,	kIx,	,
osoba	osoba	k1gFnSc1	osoba
<g/>
,	,	kIx,	,
které	který	k3yRgFnSc2	který
byl	být	k5eAaImAgMnS	být
občan	občan	k1gMnSc1	občan
mladší	mladý	k2eAgMnSc1d2	mladší
15	[number]	k4	15
let	léto	k1gNnPc2	léto
svěřen	svěřit	k5eAaPmNgInS	svěřit
do	do	k7c2	do
péče	péče	k1gFnSc2	péče
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
ředitel	ředitel	k1gMnSc1	ředitel
zařízení	zařízení	k1gNnSc4	zařízení
pro	pro	k7c4	pro
výkon	výkon	k1gInSc4	výkon
ústavní	ústavní	k2eAgFnSc2d1	ústavní
výchovy	výchova	k1gFnSc2	výchova
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
pečuje	pečovat	k5eAaImIp3nS	pečovat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
soudního	soudní	k2eAgNnSc2d1	soudní
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
o	o	k7c4	o
občana	občan	k1gMnSc4	občan
mladšího	mladý	k2eAgMnSc4d2	mladší
15	[number]	k4	15
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
osoby	osoba	k1gFnPc1	osoba
připojují	připojovat	k5eAaImIp3nP	připojovat
k	k	k7c3	k
žádosti	žádost	k1gFnSc3	žádost
souhlas	souhlas	k1gInSc1	souhlas
zákonného	zákonný	k2eAgMnSc2d1	zákonný
zástupce	zástupce	k1gMnSc2	zástupce
s	s	k7c7	s
jeho	jeho	k3xOp3gInSc7	jeho
ověřeným	ověřený	k2eAgInSc7d1	ověřený
podpisem	podpis	k1gInSc7	podpis
<g/>
.	.	kIx.	.
</s>
<s>
Souhlas	souhlas	k1gInSc1	souhlas
zákonného	zákonný	k2eAgMnSc2d1	zákonný
zástupce	zástupce	k1gMnSc2	zástupce
se	se	k3xPyFc4	se
nevyžaduje	vyžadovat	k5eNaImIp3nS	vyžadovat
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnSc1	jeho
opatření	opatření	k1gNnSc1	opatření
spojeno	spojen	k2eAgNnSc1d1	spojeno
s	s	k7c7	s
překážkou	překážka	k1gFnSc7	překážka
těžko	těžko	k6eAd1	těžko
překonatelnou	překonatelný	k2eAgFnSc7d1	překonatelná
<g/>
.	.	kIx.	.
<g/>
Soudem	soud	k1gInSc7	soud
určený	určený	k2eAgMnSc1d1	určený
opatrovník	opatrovník	k1gMnSc1	opatrovník
za	za	k7c2	za
občana	občan	k1gMnSc2	občan
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
svéprávnost	svéprávnost	k1gFnSc1	svéprávnost
je	být	k5eAaImIp3nS	být
omezena	omezit	k5eAaPmNgFnS	omezit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
způsobilý	způsobilý	k2eAgMnSc1d1	způsobilý
k	k	k7c3	k
podání	podání	k1gNnSc3	podání
žádosti	žádost	k1gFnSc2	žádost
<g/>
.	.	kIx.	.
<g/>
Za	za	k7c4	za
občana	občan	k1gMnSc4	občan
staršího	starší	k1gMnSc4	starší
15	[number]	k4	15
let	léto	k1gNnPc2	léto
člen	člen	k1gMnSc1	člen
domácnosti	domácnost	k1gFnSc2	domácnost
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
jehož	jenž	k3xRgNnSc4	jenž
oprávnění	oprávnění	k1gNnSc4	oprávnění
k	k	k7c3	k
zastupování	zastupování	k1gNnSc3	zastupování
občana	občan	k1gMnSc2	občan
bylo	být	k5eAaImAgNnS	být
schváleno	schválit	k5eAaPmNgNnS	schválit
soudem	soud	k1gInSc7	soud
<g/>
.	.	kIx.	.
<g/>
Úřad	úřad	k1gInSc1	úřad
pro	pro	k7c4	pro
mezinárodně	mezinárodně	k6eAd1	mezinárodně
právní	právní	k2eAgFnSc4d1	právní
ochranu	ochrana	k1gFnSc4	ochrana
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
<g/>
-li	i	k?	-li
o	o	k7c4	o
osvojení	osvojení	k1gNnSc4	osvojení
občana	občan	k1gMnSc2	občan
mladšího	mladý	k2eAgMnSc2d2	mladší
15	[number]	k4	15
let	léto	k1gNnPc2	léto
do	do	k7c2	do
ciziny	cizina	k1gFnSc2	cizina
<g/>
;	;	kIx,	;
jedná	jednat	k5eAaImIp3nS	jednat
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
o	o	k7c4	o
nezletilého	nezletilý	k1gMnSc4	nezletilý
občana	občan	k1gMnSc4	občan
staršího	starší	k1gMnSc4	starší
15	[number]	k4	15
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
dává	dávat	k5eAaImIp3nS	dávat
tento	tento	k3xDgInSc4	tento
úřad	úřad	k1gInSc4	úřad
k	k	k7c3	k
žádosti	žádost	k1gFnSc3	žádost
souhlas	souhlas	k1gInSc4	souhlas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vzory	vzor	k1gInPc1	vzor
pasů	pas	k1gInPc2	pas
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Cestovní	cestovní	k2eAgInSc4d1	cestovní
pas	pas	k1gInSc4	pas
bez	bez	k7c2	bez
strojově	strojově	k6eAd1	strojově
čitelných	čitelný	k2eAgInPc2d1	čitelný
údajů	údaj	k1gInPc2	údaj
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Cestovní	cestovní	k2eAgInSc4d1	cestovní
pas	pas	k1gInSc4	pas
vzor	vzor	k1gInSc1	vzor
1993	[number]	k4	1993
====	====	k?	====
</s>
</p>
<p>
<s>
Vydáván	vydáván	k2eAgInSc1d1	vydáván
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
do	do	k7c2	do
31	[number]	k4	31
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
2000	[number]	k4	2000
s	s	k7c7	s
dobou	doba	k1gFnSc7	doba
platnosti	platnost	k1gFnSc2	platnost
10	[number]	k4	10
let	léto	k1gNnPc2	léto
</s>
</p>
<p>
<s>
Datová	datový	k2eAgFnSc1d1	datová
strana	strana	k1gFnSc1	strana
je	být	k5eAaImIp3nS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
na	na	k7c6	na
zadní	zadní	k2eAgFnSc6d1	zadní
předsádce	předsádka	k1gFnSc6	předsádka
<g/>
,	,	kIx,	,
fotografie	fotografie	k1gFnSc1	fotografie
je	být	k5eAaImIp3nS	být
lepená	lepený	k2eAgFnSc1d1	lepená
a	a	k8xC	a
názvy	název	k1gInPc7	název
položek	položka	k1gFnPc2	položka
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
a	a	k8xC	a
anglickém	anglický	k2eAgInSc6d1	anglický
jazyce	jazyk	k1gInSc6	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Cestovní	cestovní	k2eAgInSc4d1	cestovní
pas	pas	k1gInSc4	pas
vzor	vzor	k1gInSc1	vzor
1998	[number]	k4	1998
====	====	k?	====
</s>
</p>
<p>
<s>
Vydáván	vydáván	k2eAgInSc1d1	vydáván
</s>
</p>
<p>
<s>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
do	do	k7c2	do
30	[number]	k4	30
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
2000	[number]	k4	2000
s	s	k7c7	s
dobou	doba	k1gFnSc7	doba
platnosti	platnost	k1gFnSc2	platnost
10	[number]	k4	10
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
2000	[number]	k4	2000
do	do	k7c2	do
31	[number]	k4	31
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
s	s	k7c7	s
dobou	doba	k1gFnSc7	doba
platnosti	platnost	k1gFnSc2	platnost
1	[number]	k4	1
rok	rok	k1gInSc1	rok
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
do	do	k7c2	do
31	[number]	k4	31
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
s	s	k7c7	s
dobou	doba	k1gFnSc7	doba
platnosti	platnost	k1gFnSc2	platnost
6	[number]	k4	6
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
občany	občan	k1gMnPc4	občan
mladší	mladý	k2eAgMnPc1d2	mladší
5	[number]	k4	5
let	léto	k1gNnPc2	léto
s	s	k7c7	s
dobou	doba	k1gFnSc7	doba
platnosti	platnost	k1gFnSc2	platnost
1	[number]	k4	1
rok	rok	k1gInSc1	rok
<g/>
.	.	kIx.	.
<g/>
Datová	datový	k2eAgFnSc1d1	datová
strana	strana	k1gFnSc1	strana
je	být	k5eAaImIp3nS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
na	na	k7c6	na
zadní	zadní	k2eAgFnSc6d1	zadní
předsádce	předsádka	k1gFnSc6	předsádka
<g/>
,	,	kIx,	,
fotografie	fotografie	k1gFnSc1	fotografie
je	být	k5eAaImIp3nS	být
lepená	lepený	k2eAgFnSc1d1	lepená
a	a	k8xC	a
názvy	název	k1gInPc7	název
položek	položka	k1gFnPc2	položka
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
a	a	k8xC	a
anglickém	anglický	k2eAgInSc6d1	anglický
jazyce	jazyk	k1gInSc6	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Cestovní	cestovní	k2eAgInSc4d1	cestovní
pas	pas	k1gInSc4	pas
vzor	vzor	k1gInSc1	vzor
2005	[number]	k4	2005
====	====	k?	====
</s>
</p>
<p>
<s>
Vydáván	vydáván	k2eAgInSc1d1	vydáván
</s>
</p>
<p>
<s>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
do	do	k7c2	do
31	[number]	k4	31
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
s	s	k7c7	s
dobou	doba	k1gFnSc7	doba
platnosti	platnost	k1gFnSc2	platnost
1	[number]	k4	1
rok	rok	k1gInSc1	rok
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
do	do	k7c2	do
31	[number]	k4	31
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
2009	[number]	k4	2009
s	s	k7c7	s
dobou	doba	k1gFnSc7	doba
platnosti	platnost	k1gFnSc2	platnost
6	[number]	k4	6
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
občany	občan	k1gMnPc4	občan
mladší	mladý	k2eAgMnPc1d2	mladší
5	[number]	k4	5
let	léto	k1gNnPc2	léto
s	s	k7c7	s
dobou	doba	k1gFnSc7	doba
platnosti	platnost	k1gFnSc2	platnost
1	[number]	k4	1
rok	rok	k1gInSc1	rok
<g/>
.	.	kIx.	.
<g/>
Datová	datový	k2eAgFnSc1d1	datová
strana	strana	k1gFnSc1	strana
je	být	k5eAaImIp3nS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
na	na	k7c6	na
zadní	zadní	k2eAgFnSc6d1	zadní
předsádce	předsádka	k1gFnSc6	předsádka
<g/>
,	,	kIx,	,
fotografie	fotografie	k1gFnSc1	fotografie
je	být	k5eAaImIp3nS	být
lepená	lepený	k2eAgFnSc1d1	lepená
a	a	k8xC	a
názvy	název	k1gInPc7	název
položek	položka	k1gFnPc2	položka
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
a	a	k8xC	a
anglickém	anglický	k2eAgInSc6d1	anglický
jazyce	jazyk	k1gInSc6	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Cestovní	cestovní	k2eAgInSc4d1	cestovní
pas	pas	k1gInSc4	pas
vzor	vzor	k1gInSc1	vzor
2007	[number]	k4	2007
====	====	k?	====
</s>
</p>
<p>
<s>
Vydáván	vydáván	k2eAgInSc1d1	vydáván
od	od	k7c2	od
února	únor	k1gInSc2	únor
2007	[number]	k4	2007
s	s	k7c7	s
dobou	doba	k1gFnSc7	doba
platnosti	platnost	k1gFnSc2	platnost
6	[number]	k4	6
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
občany	občan	k1gMnPc4	občan
mladší	mladý	k2eAgMnPc1d2	mladší
5	[number]	k4	5
let	léto	k1gNnPc2	léto
s	s	k7c7	s
dobou	doba	k1gFnSc7	doba
platnosti	platnost	k1gFnSc2	platnost
1	[number]	k4	1
rok	rok	k1gInSc1	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Datová	datový	k2eAgFnSc1d1	datová
strana	strana	k1gFnSc1	strana
je	být	k5eAaImIp3nS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
na	na	k7c6	na
zadní	zadní	k2eAgFnSc6d1	zadní
předsádce	předsádka	k1gFnSc6	předsádka
<g/>
,	,	kIx,	,
fotografie	fotografie	k1gFnSc1	fotografie
je	být	k5eAaImIp3nS	být
lepená	lepený	k2eAgFnSc1d1	lepená
a	a	k8xC	a
názvy	název	k1gInPc7	název
položek	položka	k1gFnPc2	položka
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
a	a	k8xC	a
anglickém	anglický	k2eAgInSc6d1	anglický
jazyce	jazyk	k1gInSc6	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Cestovní	cestovní	k2eAgInSc4d1	cestovní
pas	pas	k1gInSc4	pas
se	s	k7c7	s
strojově	strojově	k6eAd1	strojově
čitelnými	čitelný	k2eAgInPc7d1	čitelný
údaji	údaj	k1gInPc7	údaj
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Cestovní	cestovní	k2eAgInSc4d1	cestovní
pas	pas	k1gInSc4	pas
vzor	vzor	k1gInSc1	vzor
2000	[number]	k4	2000
====	====	k?	====
</s>
</p>
<p>
<s>
Vydáván	vydáván	k2eAgMnSc1d1	vydáván
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
2000	[number]	k4	2000
do	do	k7c2	do
15	[number]	k4	15
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
2005	[number]	k4	2005
s	s	k7c7	s
dobou	doba	k1gFnSc7	doba
platnosti	platnost	k1gFnSc2	platnost
10	[number]	k4	10
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
občanům	občan	k1gMnPc3	občan
mladším	mladý	k2eAgFnPc3d2	mladší
15	[number]	k4	15
let	léto	k1gNnPc2	léto
s	s	k7c7	s
dobou	doba	k1gFnSc7	doba
platnosti	platnost	k1gFnSc2	platnost
5	[number]	k4	5
let	léto	k1gNnPc2	léto
</s>
</p>
<p>
<s>
Datová	datový	k2eAgFnSc1d1	datová
strana	strana	k1gFnSc1	strana
je	být	k5eAaImIp3nS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
na	na	k7c6	na
zadní	zadní	k2eAgFnSc6d1	zadní
předsádce	předsádka	k1gFnSc6	předsádka
<g/>
,	,	kIx,	,
podoba	podoba	k1gFnSc1	podoba
držitele	držitel	k1gMnSc2	držitel
je	být	k5eAaImIp3nS	být
tištěná	tištěný	k2eAgFnSc1d1	tištěná
a	a	k8xC	a
názvy	název	k1gInPc7	název
položek	položka	k1gFnPc2	položka
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
a	a	k8xC	a
anglickém	anglický	k2eAgInSc6d1	anglický
jazyce	jazyk	k1gInSc6	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Cestovní	cestovní	k2eAgInSc4d1	cestovní
pas	pas	k1gInSc4	pas
vzor	vzor	k1gInSc1	vzor
2005	[number]	k4	2005
====	====	k?	====
</s>
</p>
<p>
<s>
Vydáván	vydáván	k2eAgMnSc1d1	vydáván
od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
2005	[number]	k4	2005
do	do	k7c2	do
31	[number]	k4	31
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
s	s	k7c7	s
dobou	doba	k1gFnSc7	doba
platnosti	platnost	k1gFnSc2	platnost
10	[number]	k4	10
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
občanům	občan	k1gMnPc3	občan
mladším	mladý	k2eAgFnPc3d2	mladší
15	[number]	k4	15
let	léto	k1gNnPc2	léto
s	s	k7c7	s
dobou	doba	k1gFnSc7	doba
platnosti	platnost	k1gFnSc2	platnost
5	[number]	k4	5
let	léto	k1gNnPc2	léto
</s>
</p>
<p>
<s>
Datová	datový	k2eAgFnSc1d1	datová
strana	strana	k1gFnSc1	strana
je	být	k5eAaImIp3nS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
č.	č.	k?	č.
2	[number]	k4	2
<g/>
,	,	kIx,	,
podoba	podoba	k1gFnSc1	podoba
držitele	držitel	k1gMnSc2	držitel
je	být	k5eAaImIp3nS	být
tištěná	tištěný	k2eAgFnSc1d1	tištěná
a	a	k8xC	a
názvy	název	k1gInPc7	název
položek	položka	k1gFnPc2	položka
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
českém	český	k2eAgMnSc6d1	český
<g/>
,	,	kIx,	,
anglickém	anglický	k2eAgInSc6d1	anglický
a	a	k8xC	a
francouzském	francouzský	k2eAgInSc6d1	francouzský
jazyce	jazyk	k1gInSc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
stranách	strana	k1gFnPc6	strana
č.	č.	k?	č.
6	[number]	k4	6
a	a	k8xC	a
7	[number]	k4	7
jsou	být	k5eAaImIp3nP	být
uvedeny	uveden	k2eAgInPc4d1	uveden
překlady	překlad	k1gInPc4	překlad
názvů	název	k1gInPc2	název
položek	položka	k1gFnPc2	položka
do	do	k7c2	do
18	[number]	k4	18
jazyků	jazyk	k1gInPc2	jazyk
států	stát	k1gInPc2	stát
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
a	a	k8xC	a
ruského	ruský	k2eAgInSc2d1	ruský
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Cestovní	cestovní	k2eAgInSc4d1	cestovní
pas	pas	k1gInSc4	pas
s	s	k7c7	s
biometrickými	biometrický	k2eAgInPc7d1	biometrický
údaji	údaj	k1gInPc7	údaj
vzor	vzor	k1gInSc1	vzor
2006	[number]	k4	2006
====	====	k?	====
</s>
</p>
<p>
<s>
Vydáván	vydáván	k2eAgMnSc1d1	vydáván
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
s	s	k7c7	s
dobou	doba	k1gFnSc7	doba
platnosti	platnost	k1gFnSc2	platnost
10	[number]	k4	10
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
občanům	občan	k1gMnPc3	občan
starším	starý	k2eAgFnPc3d2	starší
5	[number]	k4	5
let	léto	k1gNnPc2	léto
a	a	k8xC	a
mladším	mladý	k2eAgMnSc7d2	mladší
15	[number]	k4	15
let	léto	k1gNnPc2	léto
s	s	k7c7	s
dobou	doba	k1gFnSc7	doba
platnosti	platnost	k1gFnSc2	platnost
5	[number]	k4	5
let	léto	k1gNnPc2	léto
</s>
</p>
<p>
<s>
Datová	datový	k2eAgFnSc1d1	datová
stránka	stránka	k1gFnSc1	stránka
je	být	k5eAaImIp3nS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
na	na	k7c6	na
všité	všitý	k2eAgFnSc6d1	všitá
polykarbonátové	polykarbonátový	k2eAgFnSc6d1	polykarbonátová
kartě	karta	k1gFnSc6	karta
<g/>
,	,	kIx,	,
podoba	podoba	k1gFnSc1	podoba
držitele	držitel	k1gMnSc2	držitel
je	být	k5eAaImIp3nS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
laserovou	laserový	k2eAgFnSc7d1	laserová
perforací	perforace	k1gFnSc7	perforace
a	a	k8xC	a
názvy	název	k1gInPc7	název
položek	položka	k1gFnPc2	položka
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
českém	český	k2eAgMnSc6d1	český
<g/>
,	,	kIx,	,
anglickém	anglický	k2eAgInSc6d1	anglický
a	a	k8xC	a
francouzském	francouzský	k2eAgInSc6d1	francouzský
jazyce	jazyk	k1gInSc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
stranách	strana	k1gFnPc6	strana
č.	č.	k?	č.
6	[number]	k4	6
a	a	k8xC	a
7	[number]	k4	7
jsou	být	k5eAaImIp3nP	být
uvedeny	uveden	k2eAgInPc4d1	uveden
překlady	překlad	k1gInPc4	překlad
názvů	název	k1gInPc2	název
položek	položka	k1gFnPc2	položka
do	do	k7c2	do
18	[number]	k4	18
jazyků	jazyk	k1gInPc2	jazyk
států	stát	k1gInPc2	stát
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
a	a	k8xC	a
ruského	ruský	k2eAgInSc2d1	ruský
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pod	pod	k7c7	pod
ochranou	ochrana	k1gFnSc7	ochrana
státu	stát	k1gInSc2	stát
==	==	k?	==
</s>
</p>
<p>
<s>
Každý	každý	k3xTgMnSc1	každý
držitel	držitel	k1gMnSc1	držitel
českého	český	k2eAgInSc2d1	český
pasu	pas	k1gInSc2	pas
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
pod	pod	k7c7	pod
ochranou	ochrana	k1gFnSc7	ochrana
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
V	v	k7c6	v
odůvodněných	odůvodněný	k2eAgInPc6d1	odůvodněný
případech	případ	k1gInPc6	případ
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
při	při	k7c6	při
ztrátě	ztráta	k1gFnSc6	ztráta
pasu	pas	k1gInSc2	pas
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
zahraničí	zahraničí	k1gNnSc2	zahraničí
vydat	vydat	k5eAaPmF	vydat
občanovi	občan	k1gMnSc3	občan
Cestovní	cestovní	k2eAgInSc4d1	cestovní
průkaz	průkaz	k1gInSc4	průkaz
k	k	k7c3	k
jednotlivé	jednotlivý	k2eAgFnSc3d1	jednotlivá
cestě	cesta	k1gFnSc3	cesta
s	s	k7c7	s
územní	územní	k2eAgFnSc7d1	územní
a	a	k8xC	a
časovou	časový	k2eAgFnSc7d1	časová
platností	platnost	k1gFnSc7	platnost
omezenou	omezený	k2eAgFnSc4d1	omezená
účelem	účel	k1gInSc7	účel
cesty	cesta	k1gFnSc2	cesta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
úmrtní	úmrtní	k2eAgInSc1d1	úmrtní
list	list	k1gInSc1	list
</s>
</p>
<p>
<s>
občanský	občanský	k2eAgInSc1d1	občanský
průkaz	průkaz	k1gInSc1	průkaz
</s>
</p>
<p>
<s>
rodný	rodný	k2eAgInSc1d1	rodný
list	list	k1gInSc1	list
</s>
</p>
<p>
<s>
oddací	oddací	k2eAgInSc1d1	oddací
list	list	k1gInSc1	list
</s>
</p>
<p>
<s>
zbrojní	zbrojní	k2eAgInSc4d1	zbrojní
průkaz	průkaz	k1gInSc4	průkaz
</s>
</p>
<p>
<s>
řidičský	řidičský	k2eAgInSc4d1	řidičský
průkaz	průkaz	k1gInSc4	průkaz
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Cestovní	cestovní	k2eAgInSc4d1	cestovní
pas	pas	k1gInSc4	pas
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Zpráva	zpráva	k1gFnSc1	zpráva
Nové	Nová	k1gFnSc2	Nová
cestovní	cestovní	k2eAgInPc4d1	cestovní
pasy	pas	k1gInPc4	pas
od	od	k7c2	od
dubna	duben	k1gInSc2	duben
ve	v	k7c6	v
Wikizprávách	Wikizpráva	k1gFnPc6	Wikizpráva
</s>
</p>
<p>
<s>
Přehled	přehled	k1gInSc4	přehled
států	stát	k1gInPc2	stát
s	s	k7c7	s
bezvízovým	bezvízový	k2eAgInSc7d1	bezvízový
stykem	styk	k1gInSc7	styk
–	–	k?	–
Ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
</s>
</p>
<p>
<s>
Neplatné	platný	k2eNgInPc4d1	neplatný
doklady	doklad	k1gInPc4	doklad
(	(	kIx(	(
<g/>
databáze	databáze	k1gFnPc4	databáze
<g/>
)	)	kIx)	)
–	–	k?	–
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
</s>
</p>
<p>
<s>
Passport	Passport	k1gInSc1	Passport
Index	index	k1gInSc1	index
(	(	kIx(	(
<g/>
žebříček	žebříček	k1gInSc1	žebříček
pasů	pas	k1gInPc2	pas
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
pořadí	pořadí	k1gNnSc2	pořadí
<g/>
)	)	kIx)	)
–	–	k?	–
PassportIndex	PassportIndex	k1gInSc1	PassportIndex
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
(	(	kIx(	(
<g/>
Arton	Arton	k1gMnSc1	Arton
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Passport	Passport	k1gInSc1	Passport
Index	index	k1gInSc1	index
(	(	kIx(	(
<g/>
cestovní	cestovní	k2eAgInSc4d1	cestovní
pas	pas	k1gInSc4	pas
Česka	Česko	k1gNnSc2	Česko
<g/>
,	,	kIx,	,
víza	vízo	k1gNnSc2	vízo
<g/>
,	,	kIx,	,
srovnání	srovnání	k1gNnSc2	srovnání
<g/>
)	)	kIx)	)
–	–	k?	–
PassportIndex	PassportIndex	k1gInSc1	PassportIndex
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
(	(	kIx(	(
<g/>
Arton	Arton	k1gMnSc1	Arton
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Český	český	k2eAgInSc1d1	český
pas	pas	k1gInSc1	pas
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
'	'	kIx"	'
<g/>
nejmocnějším	mocný	k2eAgInSc6d3	nejmocnější
<g/>
'	'	kIx"	'
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Žebříčku	žebříček	k1gInSc2	žebříček
vévodí	vévodit	k5eAaImIp3nS	vévodit
Němci	Němec	k1gMnSc3	Němec
–	–	k?	–
Lidovky	Lidovky	k1gFnPc1	Lidovky
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
