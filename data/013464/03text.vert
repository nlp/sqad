<p>
<s>
Borelovská	Borelovský	k2eAgFnSc1d1	Borelovský
množina	množina	k1gFnSc1	množina
v	v	k7c6	v
matematice	matematika	k1gFnSc6	matematika
je	být	k5eAaImIp3nS	být
libovolná	libovolný	k2eAgFnSc1d1	libovolná
množina	množina	k1gFnSc1	množina
v	v	k7c6	v
topologickém	topologický	k2eAgInSc6d1	topologický
prostoru	prostor	k1gInSc6	prostor
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
lze	lze	k6eAd1	lze
získat	získat	k5eAaPmF	získat
z	z	k7c2	z
otevřených	otevřený	k2eAgFnPc2d1	otevřená
množin	množina	k1gFnPc2	množina
pomocí	pomocí	k7c2	pomocí
operací	operace	k1gFnPc2	operace
spočetného	spočetný	k2eAgNnSc2d1	spočetné
sjednocení	sjednocení	k1gNnSc2	sjednocení
<g/>
,	,	kIx,	,
spočetného	spočetný	k2eAgInSc2d1	spočetný
průniku	průnik	k1gInSc2	průnik
a	a	k8xC	a
relativního	relativní	k2eAgInSc2d1	relativní
doplňku	doplněk	k1gInSc2	doplněk
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
ekvivalentně	ekvivalentně	k6eAd1	ekvivalentně
z	z	k7c2	z
uzavřených	uzavřený	k2eAgFnPc2d1	uzavřená
množin	množina	k1gFnPc2	množina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc4	název
mají	mít	k5eAaImIp3nP	mít
po	po	k7c6	po
francouzském	francouzský	k2eAgInSc6d1	francouzský
matematikovi	matematik	k1gMnSc3	matematik
Émile	Émile	k1gNnSc2	Émile
Borelovi	Borel	k1gMnSc3	Borel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
libovolný	libovolný	k2eAgInSc4d1	libovolný
topologický	topologický	k2eAgInSc4d1	topologický
prostor	prostor	k1gInSc4	prostor
X	X	kA	X
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
kolekce	kolekce	k1gFnSc1	kolekce
všech	všecek	k3xTgFnPc2	všecek
borelovských	borelovský	k2eAgFnPc2d1	borelovský
množin	množina	k1gFnPc2	množina
na	na	k7c6	na
X	X	kA	X
σ	σ	k?	σ
známou	známá	k1gFnSc7	známá
jako	jako	k8xC	jako
borelovská	borelovský	k2eAgFnSc1d1	borelovský
algebra	algebra	k1gFnSc1	algebra
nebo	nebo	k8xC	nebo
borelovská	borelovský	k2eAgFnSc1d1	borelovský
σ	σ	k?	σ
<g/>
.	.	kIx.	.
</s>
<s>
Borelovská	Borelovský	k2eAgFnSc1d1	Borelovský
algebra	algebra	k1gFnSc1	algebra
na	na	k7c6	na
X	X	kA	X
je	být	k5eAaImIp3nS	být
nejmenší	malý	k2eAgFnSc1d3	nejmenší
σ	σ	k?	σ
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
všechny	všechen	k3xTgFnPc4	všechen
otevřené	otevřený	k2eAgFnPc4d1	otevřená
množiny	množina	k1gFnPc4	množina
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
ekvivalentně	ekvivalentně	k6eAd1	ekvivalentně
všechny	všechen	k3xTgFnPc1	všechen
uzavřené	uzavřený	k2eAgFnPc1d1	uzavřená
množiny	množina	k1gFnPc1	množina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Borelovské	Borelovský	k2eAgFnPc1d1	Borelovský
množiny	množina	k1gFnPc1	množina
jsou	být	k5eAaImIp3nP	být
důležité	důležitý	k2eAgFnPc1d1	důležitá
v	v	k7c6	v
teorii	teorie	k1gFnSc6	teorie
míry	míra	k1gFnSc2	míra
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
libovolná	libovolný	k2eAgFnSc1d1	libovolná
míra	míra	k1gFnSc1	míra
definovaná	definovaný	k2eAgFnSc1d1	definovaná
na	na	k7c6	na
otevřených	otevřený	k2eAgFnPc6d1	otevřená
množinách	množina	k1gFnPc6	množina
nějakého	nějaký	k3yIgInSc2	nějaký
prostoru	prostor	k1gInSc2	prostor
nebo	nebo	k8xC	nebo
na	na	k7c6	na
uzavřených	uzavřený	k2eAgFnPc6d1	uzavřená
množinách	množina	k1gFnPc6	množina
nějakého	nějaký	k3yIgInSc2	nějaký
prostoru	prostor	k1gInSc2	prostor
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
definovaná	definovaný	k2eAgFnSc1d1	definovaná
i	i	k9	i
na	na	k7c6	na
všech	všecek	k3xTgFnPc6	všecek
borelovských	borelovský	k2eAgFnPc6d1	borelovský
množinách	množina	k1gFnPc6	množina
tohoto	tento	k3xDgInSc2	tento
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Jakákoli	jakýkoli	k3yIgFnSc1	jakýkoli
míra	míra	k1gFnSc1	míra
definovaná	definovaný	k2eAgFnSc1d1	definovaná
na	na	k7c6	na
borelovských	borelovský	k2eAgFnPc6d1	borelovský
množinách	množina	k1gFnPc6	množina
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
borelovská	borelovský	k2eAgFnSc1d1	borelovský
míra	míra	k1gFnSc1	míra
<g/>
.	.	kIx.	.
</s>
<s>
Borelovské	Borelovský	k2eAgFnSc2d1	Borelovský
množiny	množina	k1gFnSc2	množina
a	a	k8xC	a
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
související	související	k2eAgFnSc1d1	související
borelovská	borelovský	k2eAgFnSc1d1	borelovský
hierarchie	hierarchie	k1gFnSc1	hierarchie
také	také	k9	také
hraje	hrát	k5eAaImIp3nS	hrát
stěžejní	stěžejní	k2eAgFnSc4d1	stěžejní
roli	role	k1gFnSc4	role
v	v	k7c6	v
deskriptivní	deskriptivní	k2eAgFnSc6d1	deskriptivní
teorii	teorie	k1gFnSc6	teorie
množin	množina	k1gFnPc2	množina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
kontextech	kontext	k1gInPc6	kontext
jsou	být	k5eAaImIp3nP	být
borelovské	borelovský	k2eAgFnPc1d1	borelovský
množiny	množina	k1gFnPc1	množina
definovány	definovat	k5eAaBmNgFnP	definovat
jako	jako	k8xC	jako
množiny	množina	k1gFnPc1	množina
generované	generovaný	k2eAgInPc1d1	generovaný
kompaktními	kompaktní	k2eAgFnPc7d1	kompaktní
množinami	množina	k1gFnPc7	množina
topologického	topologický	k2eAgInSc2d1	topologický
prostoru	prostor	k1gInSc2	prostor
<g/>
,	,	kIx,	,
místo	místo	k1gNnSc1	místo
otevřenými	otevřený	k2eAgFnPc7d1	otevřená
množinami	množina	k1gFnPc7	množina
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
dvě	dva	k4xCgFnPc1	dva
definice	definice	k1gFnPc1	definice
jsou	být	k5eAaImIp3nP	být
ekvivalentní	ekvivalentní	k2eAgFnPc1d1	ekvivalentní
pro	pro	k7c4	pro
mnoho	mnoho	k4c4	mnoho
rozumných	rozumný	k2eAgInPc2d1	rozumný
prostorů	prostor	k1gInPc2	prostor
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
všech	všecek	k3xTgInPc2	všecek
Hausdorffových	Hausdorffův	k2eAgInPc2d1	Hausdorffův
σ	σ	k?	σ
prostorů	prostor	k1gInPc2	prostor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
lišit	lišit	k5eAaImF	lišit
v	v	k7c6	v
patologičtějších	patologický	k2eAgInPc6d2	patologický
prostorech	prostor	k1gInPc6	prostor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Generování	generování	k1gNnSc3	generování
borelovské	borelovský	k2eAgFnSc2d1	borelovský
algebry	algebra	k1gFnSc2	algebra
==	==	k?	==
</s>
</p>
<p>
<s>
Jestliže	jestliže	k8xS	jestliže
X	X	kA	X
je	být	k5eAaImIp3nS	být
metrický	metrický	k2eAgInSc4d1	metrický
prostor	prostor	k1gInSc4	prostor
<g/>
,	,	kIx,	,
můžeme	moct	k5eAaImIp1nP	moct
borelovskou	borelovský	k2eAgFnSc4d1	borelovský
algebru	algebra	k1gFnSc4	algebra
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
smyslu	smysl	k1gInSc6	smysl
generativně	generativně	k6eAd1	generativně
popsat	popsat	k5eAaPmF	popsat
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
kolekci	kolekce	k1gFnSc4	kolekce
T	T	kA	T
podmnožin	podmnožina	k1gFnPc2	podmnožina
X	X	kA	X
(	(	kIx(	(
<g/>
to	ten	k3xDgNnSc1	ten
jest	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
libovolnou	libovolný	k2eAgFnSc4d1	libovolná
podmnožinu	podmnožina	k1gFnSc4	podmnožina
potenční	potenční	k2eAgFnSc2d1	potenční
množiny	množina	k1gFnSc2	množina
X	X	kA	X
<g/>
,	,	kIx,	,
P	P	kA	P
<g/>
(	(	kIx(	(
<g/>
X	X	kA	X
<g/>
))	))	k?	))
<g/>
,	,	kIx,	,
definujeme	definovat	k5eAaBmIp1nP	definovat
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
T	T	kA	T
</s>
</p>
<p>
</p>
<p>
<s>
σ	σ	k?	σ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
T_	T_	k1gMnPc6	T_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sigma	sigma	k1gNnSc1	sigma
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
quad	quad	k6eAd1	quad
}	}	kIx)	}
</s>
</p>
<p>
<s>
jsou	být	k5eAaImIp3nP	být
všechna	všechen	k3xTgNnPc1	všechen
spočetná	spočetný	k2eAgNnPc1d1	spočetné
sjednocení	sjednocení	k1gNnPc1	sjednocení
prvků	prvek	k1gInPc2	prvek
z	z	k7c2	z
T	T	kA	T
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
T	T	kA	T
</s>
</p>
<p>
</p>
<p>
<s>
δ	δ	k?	δ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
T_	T_	k1gMnPc6	T_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
delta	delta	k1gFnSc1	delta
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
quad	quad	k6eAd1	quad
}	}	kIx)	}
</s>
</p>
<p>
<s>
jsou	být	k5eAaImIp3nP	být
všechny	všechen	k3xTgInPc1	všechen
spočetné	spočetný	k2eAgInPc1d1	spočetný
průniky	průnik	k1gInPc1	průnik
prvků	prvek	k1gInPc2	prvek
z	z	k7c2	z
T	T	kA	T
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
T	T	kA	T
</s>
</p>
<p>
</p>
<p>
<s>
δ	δ	k?	δ
</s>
</p>
<p>
<s>
σ	σ	k?	σ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
T	T	kA	T
</s>
</p>
<p>
</p>
<p>
<s>
δ	δ	k?	δ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
<s>
σ	σ	k?	σ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
T_	T_	k1gMnPc6	T_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
delta	delta	k1gFnSc1	delta
\	\	kIx~	\
<g/>
sigma	sigma	k1gNnSc1	sigma
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
T_	T_	k1gFnSc1	T_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
delta	delta	k1gFnSc1	delta
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sigma	sigma	k1gNnSc1	sigma
}	}	kIx)	}
<g/>
.	.	kIx.	.
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Nyní	nyní	k6eAd1	nyní
definujeme	definovat	k5eAaBmIp1nP	definovat
pomocí	pomocí	k7c2	pomocí
transfinitní	transfinitní	k2eAgFnSc2d1	transfinitní
indukce	indukce	k1gFnSc2	indukce
posloupnost	posloupnost	k1gFnSc1	posloupnost
Gm	Gm	k1gFnSc1	Gm
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
m	m	kA	m
je	být	k5eAaImIp3nS	být
ordinální	ordinální	k2eAgNnSc1d1	ordinální
číslo	číslo	k1gNnSc1	číslo
<g/>
,	,	kIx,	,
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
základní	základní	k2eAgInSc4d1	základní
případ	případ	k1gInSc4	případ
definice	definice	k1gFnSc2	definice
<g/>
,	,	kIx,	,
nechť	nechť	k9	nechť
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
G	G	kA	G
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
kolekce	kolekce	k1gFnSc1	kolekce
otevřených	otevřený	k2eAgFnPc2d1	otevřená
podmnožin	podmnožina	k1gFnPc2	podmnožina
X.	X.	kA	X.
</s>
</p>
<p>
<s>
Jestliže	jestliže	k8xS	jestliže
i	i	k9	i
není	být	k5eNaImIp3nS	být
limitní	limitní	k2eAgFnSc7d1	limitní
ordinál	ordinála	k1gFnPc2	ordinála
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
i	i	k9	i
má	mít	k5eAaImIp3nS	mít
bezprostředně	bezprostředně	k6eAd1	bezprostředně
předcházející	předcházející	k2eAgFnSc1d1	předcházející
ordinál	ordinála	k1gFnPc2	ordinála
i	i	k9	i
−	−	k?	−
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Nechť	nechť	k9	nechť
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
[	[	kIx(	[
</s>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
]	]	kIx)	]
</s>
</p>
<p>
</p>
<p>
<s>
δ	δ	k?	δ
</s>
</p>
<p>
<s>
σ	σ	k?	σ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
G	G	kA	G
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
[	[	kIx(	[
<g/>
G	G	kA	G
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
i-	i-	k?	i-
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
]	]	kIx)	]
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
delta	delta	k1gFnSc1	delta
\	\	kIx~	\
<g/>
sigma	sigma	k1gNnSc1	sigma
}	}	kIx)	}
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
<s>
Jestliže	jestliže	k8xS	jestliže
i	i	k9	i
je	být	k5eAaImIp3nS	být
limitní	limitní	k2eAgFnSc7d1	limitní
ordinál	ordinála	k1gFnPc2	ordinála
<g/>
,	,	kIx,	,
množina	množina	k1gFnSc1	množina
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
⋃	⋃	k?	⋃
</s>
</p>
<p>
</p>
<p>
<s>
j	j	k?	j
</s>
</p>
<p>
<s>
<	<	kIx(	<
</s>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
<s>
j	j	k?	j
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
G	G	kA	G
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
bigcup	bigcup	k1gInSc1	bigcup
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
j	j	k?	j
<g/>
<	<	kIx(	<
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
G	G	kA	G
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
j	j	k?	j
<g/>
}	}	kIx)	}
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Tvrdíme	tvrdit	k5eAaImIp1nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
borelovská	borelovský	k2eAgFnSc1d1	borelovský
algebra	algebra	k1gFnSc1	algebra
je	být	k5eAaImIp3nS	být
Gω	Gω	k1gFnSc4	Gω
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ω	ω	k?	ω
<g/>
1	[number]	k4	1
je	být	k5eAaImIp3nS	být
první	první	k4xOgNnSc4	první
nespočetné	spočetný	k2eNgNnSc4d1	nespočetné
ordinální	ordinální	k2eAgNnSc4d1	ordinální
číslo	číslo	k1gNnSc4	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Tj.	tj.	kA	tj.
borelovská	borelovský	k2eAgFnSc1d1	borelovský
algebra	algebra	k1gFnSc1	algebra
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
generovaná	generovaný	k2eAgFnSc1d1	generovaná
z	z	k7c2	z
třídy	třída	k1gFnSc2	třída
otevřených	otevřený	k2eAgFnPc2d1	otevřená
množin	množina	k1gFnPc2	množina
iterováním	iterování	k1gNnSc7	iterování
operací	operace	k1gFnPc2	operace
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
<s>
↦	↦	k?	↦
</s>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
<s>
δ	δ	k?	δ
</s>
</p>
<p>
<s>
σ	σ	k?	σ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
G	G	kA	G
<g/>
\	\	kIx~	\
<g/>
mapsto	mapsto	k6eAd1	mapsto
G_	G_	k1gMnSc2	G_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
delta	delta	k1gFnSc1	delta
\	\	kIx~	\
<g/>
sigma	sigma	k1gNnSc1	sigma
}	}	kIx)	}
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
na	na	k7c4	na
první	první	k4xOgInSc4	první
nespočetný	spočetný	k2eNgInSc4d1	nespočetný
ordinál	ordinála	k1gFnPc2	ordinála
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
důkaz	důkaz	k1gInSc4	důkaz
tohoto	tento	k3xDgNnSc2	tento
tvrzení	tvrzení	k1gNnSc2	tvrzení
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
si	se	k3xPyFc3	se
uvědomit	uvědomit	k5eAaPmF	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
libovolnou	libovolný	k2eAgFnSc4d1	libovolná
otevřenou	otevřený	k2eAgFnSc4d1	otevřená
množinu	množina	k1gFnSc4	množina
v	v	k7c6	v
metrickém	metrický	k2eAgInSc6d1	metrický
prostoru	prostor	k1gInSc6	prostor
lze	lze	k6eAd1	lze
získat	získat	k5eAaPmF	získat
jako	jako	k9	jako
sjednocení	sjednocení	k1gNnSc4	sjednocení
rostoucí	rostoucí	k2eAgFnSc2d1	rostoucí
posloupnosti	posloupnost	k1gFnSc2	posloupnost
uzavřených	uzavřený	k2eAgFnPc2d1	uzavřená
množin	množina	k1gFnPc2	množina
<g/>
.	.	kIx.	.
</s>
<s>
Speciálně	speciálně	k6eAd1	speciálně
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
<g/>
,	,	kIx,	,
že	že	k8xS	že
doplňky	doplněk	k1gInPc1	doplněk
množin	množina	k1gFnPc2	množina
zobrazují	zobrazovat	k5eAaImIp3nP	zobrazovat
Gm	Gm	k1gMnPc1	Gm
do	do	k7c2	do
sebe	sebe	k3xPyFc4	sebe
pro	pro	k7c4	pro
libovolný	libovolný	k2eAgInSc4d1	libovolný
limitní	limitní	k2eAgInSc4d1	limitní
ordinál	ordinála	k1gFnPc2	ordinála
m	m	kA	m
<g/>
;	;	kIx,	;
pokud	pokud	k8xS	pokud
m	m	kA	m
je	být	k5eAaImIp3nS	být
nespočetný	spočetný	k2eNgInSc1d1	nespočetný
limitní	limitní	k2eAgFnSc7d1	limitní
ordinál	ordinála	k1gFnPc2	ordinála
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
Gm	Gm	k1gFnSc1	Gm
je	být	k5eAaImIp3nS	být
uzavřená	uzavřený	k2eAgFnSc1d1	uzavřená
na	na	k7c4	na
spočetná	spočetný	k2eAgNnPc4d1	spočetné
sjednocení	sjednocení	k1gNnSc4	sjednocení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
libovolnou	libovolný	k2eAgFnSc4d1	libovolná
borelovskou	borelovský	k2eAgFnSc4d1	borelovský
množinu	množina	k1gFnSc4	množina
B	B	kA	B
existuje	existovat	k5eAaImIp3nS	existovat
nějaký	nějaký	k3yIgInSc4	nějaký
spočetný	spočetný	k2eAgInSc4d1	spočetný
ordinál	ordinála	k1gFnPc2	ordinála
α	α	k?	α
takový	takový	k3xDgInSc4	takový
<g/>
,	,	kIx,	,
že	že	k8xS	že
B	B	kA	B
lze	lze	k6eAd1	lze
získat	získat	k5eAaPmF	získat
iterováním	iterování	k1gNnSc7	iterování
operace	operace	k1gFnSc2	operace
nad	nad	k7c7	nad
α	α	k?	α
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
B	B	kA	B
probíhá	probíhat	k5eAaImIp3nS	probíhat
všemi	všecek	k3xTgFnPc7	všecek
borelovskými	borelovský	k2eAgFnPc7d1	borelovský
množinami	množina	k1gFnPc7	množina
<g/>
,	,	kIx,	,
α	α	k?	α
bude	být	k5eAaImBp3nS	být
probíhat	probíhat	k5eAaImF	probíhat
všemi	všecek	k3xTgFnPc7	všecek
spočetnými	spočetný	k2eAgFnPc7d1	spočetná
ordinály	ordinála	k1gFnPc4	ordinála
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
prvním	první	k4xOgInSc7	první
ordinálem	ordinál	k1gInSc7	ordinál
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
který	který	k3yQgInSc4	který
získáme	získat	k5eAaPmIp1nP	získat
všechny	všechen	k3xTgFnPc1	všechen
borelovské	borelovský	k2eAgFnPc1d1	borelovský
množiny	množina	k1gFnPc1	množina
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ω	ω	k?	ω
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
první	první	k4xOgInSc4	první
nespočetný	spočetný	k2eNgInSc4d1	nespočetný
ordinál	ordinála	k1gFnPc2	ordinála
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Příklad	příklad	k1gInSc1	příklad
===	===	k?	===
</s>
</p>
<p>
<s>
Důležitý	důležitý	k2eAgInSc1d1	důležitý
příklad	příklad	k1gInSc1	příklad
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
teorii	teorie	k1gFnSc6	teorie
pravděpodobnosti	pravděpodobnost	k1gFnSc2	pravděpodobnost
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
borelovská	borelovský	k2eAgFnSc1d1	borelovský
algebra	algebra	k1gFnSc1	algebra
na	na	k7c6	na
množině	množina	k1gFnSc6	množina
reálných	reálný	k2eAgNnPc2d1	reálné
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
algebra	algebra	k1gFnSc1	algebra
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yQgFnPc4	který
je	být	k5eAaImIp3nS	být
definována	definovat	k5eAaBmNgFnS	definovat
borelovská	borelovský	k2eAgFnSc1d1	borelovský
míra	míra	k1gFnSc1	míra
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
dána	dán	k2eAgFnSc1d1	dána
reálná	reálný	k2eAgFnSc1d1	reálná
náhodná	náhodný	k2eAgFnSc1d1	náhodná
proměnná	proměnná	k1gFnSc1	proměnná
definována	definovat	k5eAaBmNgFnS	definovat
na	na	k7c6	na
pravděpodobnostním	pravděpodobnostní	k2eAgInSc6d1	pravděpodobnostní
prostoru	prostor	k1gInSc6	prostor
<g/>
,	,	kIx,	,
její	její	k3xOp3gNnSc1	její
rozdělení	rozdělení	k1gNnSc1	rozdělení
pravděpodobnosti	pravděpodobnost	k1gFnSc2	pravděpodobnost
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
definice	definice	k1gFnSc2	definice
také	také	k6eAd1	také
mírou	míra	k1gFnSc7	míra
na	na	k7c6	na
borelovské	borelovský	k2eAgFnSc6d1	borelovský
algebře	algebra	k1gFnSc6	algebra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Borelovská	Borelovský	k2eAgFnSc1d1	Borelovský
algebra	algebra	k1gFnSc1	algebra
na	na	k7c6	na
reálných	reálný	k2eAgNnPc6d1	reálné
číslech	číslo	k1gNnPc6	číslo
je	být	k5eAaImIp3nS	být
nejmenší	malý	k2eAgFnSc4d3	nejmenší
σ	σ	k?	σ
na	na	k7c4	na
R	R	kA	R
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
všechny	všechen	k3xTgInPc4	všechen
intervaly	interval	k1gInPc4	interval
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
konstrukci	konstrukce	k1gFnSc6	konstrukce
pomocí	pomocí	k7c2	pomocí
transfinitní	transfinitní	k2eAgFnSc2d1	transfinitní
indukce	indukce	k1gFnSc2	indukce
lze	lze	k6eAd1	lze
ukázat	ukázat	k5eAaPmF	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kardinalita	kardinalita	k1gFnSc1	kardinalita
systému	systém	k1gInSc2	systém
množin	množina	k1gFnPc2	množina
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
kroku	krok	k1gInSc6	krok
je	být	k5eAaImIp3nS	být
nejvýše	nejvýše	k6eAd1	nejvýše
rovna	roven	k2eAgFnSc1d1	rovna
potenci	potence	k1gFnSc4	potence
kontinua	kontinuum	k1gNnSc2	kontinuum
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
borelovských	borelovský	k2eAgFnPc2d1	borelovský
množin	množina	k1gFnPc2	množina
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgInSc1d2	menší
nebo	nebo	k8xC	nebo
roven	roven	k2eAgInSc1d1	roven
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
א	א	k?	א
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
×	×	k?	×
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
א	א	k?	א
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
א	א	k?	א
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
aleph	aleph	k1gInSc4	aleph
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
times	times	k1gInSc1	times
2	[number]	k4	2
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
aleph	aleph	k1gInSc1	aleph
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
=	=	kIx~	=
<g/>
2	[number]	k4	2
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
aleph	aleph	k1gInSc1	aleph
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
<g/>
.	.	kIx.	.
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Standardní	standardní	k2eAgInPc4d1	standardní
borelovské	borelovský	k2eAgInPc4d1	borelovský
prostory	prostor	k1gInPc4	prostor
a	a	k8xC	a
Kuratowski	Kuratowsk	k1gFnPc4	Kuratowsk
věty	věta	k1gFnSc2	věta
==	==	k?	==
</s>
</p>
<p>
<s>
George	George	k1gFnSc1	George
Mackey	Mackea	k1gFnSc2	Mackea
píše	psát	k5eAaImIp3nS	psát
<g/>
,	,	kIx,	,
že	že	k8xS	že
borelovský	borelovský	k2eAgInSc1d1	borelovský
prostor	prostor	k1gInSc1	prostor
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
množina	množina	k1gFnSc1	množina
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
význačnou	význačný	k2eAgFnSc7d1	význačná
σ	σ	k?	σ
svých	svůj	k3xOyFgFnPc2	svůj
podmnožin	podmnožina	k1gFnPc2	podmnožina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
borelovské	borelovský	k2eAgFnPc1d1	borelovský
množiny	množina	k1gFnPc1	množina
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
modernější	moderní	k2eAgFnSc1d2	modernější
terminologie	terminologie	k1gFnSc1	terminologie
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
takové	takový	k3xDgFnPc4	takový
prostory	prostora	k1gFnPc4	prostora
název	název	k1gInSc1	název
měřitelný	měřitelný	k2eAgInSc4d1	měřitelný
prostor	prostor	k1gInSc4	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
pro	pro	k7c4	pro
toto	tento	k3xDgNnSc4	tento
rozlišování	rozlišování	k1gNnSc4	rozlišování
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
Borelovská	Borelovský	k2eAgFnSc1d1	Borelovský
σ	σ	k?	σ
je	být	k5eAaImIp3nS	být
σ	σ	k?	σ
generovaná	generovaný	k2eAgFnSc1d1	generovaná
otevřenými	otevřený	k2eAgFnPc7d1	otevřená
množinami	množina	k1gFnPc7	množina
topologického	topologický	k2eAgInSc2d1	topologický
prostoru	prostor	k1gInSc2	prostor
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Mackeyova	Mackeyův	k2eAgFnSc1d1	Mackeyův
definice	definice	k1gFnSc1	definice
se	se	k3xPyFc4	se
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
na	na	k7c4	na
množiny	množina	k1gFnPc4	množina
opatřené	opatřený	k2eAgFnPc4d1	opatřená
libovolnou	libovolný	k2eAgFnSc7d1	libovolná
σ	σ	k?	σ
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
měřitelné	měřitelný	k2eAgInPc1d1	měřitelný
prostory	prostor	k1gInPc1	prostor
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
neborelovské	borelovský	k2eNgInPc1d1	borelovský
prostory	prostor	k1gInPc1	prostor
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
omezenějším	omezený	k2eAgInSc6d2	omezenější
topologickém	topologický	k2eAgInSc6d1	topologický
smyslu	smysl	k1gInSc6	smysl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Měřitelné	měřitelný	k2eAgFnPc1d1	měřitelná
prostory	prostora	k1gFnPc1	prostora
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
kategorie	kategorie	k1gFnPc4	kategorie
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgMnPc6	který
homomorfismy	homomorfismus	k1gInPc4	homomorfismus
jsou	být	k5eAaImIp3nP	být
měřitelnými	měřitelný	k2eAgFnPc7d1	měřitelná
funkcemi	funkce	k1gFnPc7	funkce
mezi	mezi	k7c7	mezi
měřitelnými	měřitelný	k2eAgInPc7d1	měřitelný
prostory	prostor	k1gInPc7	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Funkce	funkce	k1gFnSc1	funkce
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
<s>
:	:	kIx,	:
</s>
</p>
<p>
<s>
X	X	kA	X
</s>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
<s>
Y	Y	kA	Y
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
:	:	kIx,	:
<g/>
X	X	kA	X
<g/>
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
Y	Y	kA	Y
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
měřitelná	měřitelný	k2eAgFnSc1d1	měřitelná
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
vzorem	vzor	k1gInSc7	vzor
každé	každý	k3xTgFnSc2	každý
měřitelné	měřitelný	k2eAgFnSc2d1	měřitelná
množiny	množina	k1gFnSc2	množina
B	B	kA	B
v	v	k7c6	v
Y	Y	kA	Y
<g/>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
B	B	kA	B
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
B	B	kA	B
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
měřitelná	měřitelný	k2eAgFnSc1d1	měřitelná
množina	množina	k1gFnSc1	množina
v	v	k7c6	v
X.	X.	kA	X.
</s>
</p>
<p>
<s>
Věta	věta	k1gFnSc1	věta
<g/>
.	.	kIx.	.
</s>
<s>
Nechť	nechť	k9	nechť
X	X	kA	X
je	být	k5eAaImIp3nS	být
polský	polský	k2eAgInSc1d1	polský
prostor	prostor	k1gInSc1	prostor
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
topologický	topologický	k2eAgInSc1d1	topologický
prostor	prostor	k1gInSc1	prostor
takový	takový	k3xDgInSc1	takový
<g/>
,	,	kIx,	,
že	že	k8xS	že
existuje	existovat	k5eAaImIp3nS	existovat
metrika	metrika	k1gFnSc1	metrika
d	d	k?	d
na	na	k7c4	na
X	X	kA	X
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
definuje	definovat	k5eAaBmIp3nS	definovat
topologii	topologie	k1gFnSc4	topologie
X	X	kA	X
<g/>
,	,	kIx,	,
a	a	k8xC	a
díky	díky	k7c3	díky
které	který	k3yIgFnSc3	který
je	být	k5eAaImIp3nS	být
X	X	kA	X
úplný	úplný	k2eAgInSc1d1	úplný
separabilní	separabilní	k2eAgInSc1d1	separabilní
metrický	metrický	k2eAgInSc1d1	metrický
prostor	prostor	k1gInSc1	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k9	pak
X	X	kA	X
jako	jako	k8xS	jako
Borelovský	Borelovský	k2eAgInSc1d1	Borelovský
prostor	prostor	k1gInSc1	prostor
je	být	k5eAaImIp3nS	být
izomorfní	izomorfní	k2eAgInSc1d1	izomorfní
s	s	k7c7	s
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
prostorů	prostor	k1gInPc2	prostor
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
R	R	kA	R
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
Z	z	k7c2	z
nebo	nebo	k8xC	nebo
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
s	s	k7c7	s
konečným	konečný	k2eAgInSc7d1	konečný
prostorem	prostor	k1gInSc7	prostor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
Tento	tento	k3xDgInSc1	tento
výsledek	výsledek	k1gInSc1	výsledek
připomíná	připomínat	k5eAaImIp3nS	připomínat
Maharamovu	Maharamův	k2eAgFnSc4d1	Maharamův
větu	věta	k1gFnSc4	věta
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Reálná	reálný	k2eAgFnSc1d1	reálná
přímka	přímka	k1gFnSc1	přímka
R	R	kA	R
a	a	k8xC	a
sjednocení	sjednocení	k1gNnSc2	sjednocení
R	R	kA	R
s	s	k7c7	s
libovolnou	libovolný	k2eAgFnSc7d1	libovolná
spočetnou	spočetný	k2eAgFnSc7d1	spočetná
množinou	množina	k1gFnSc7	množina
jsou	být	k5eAaImIp3nP	být
jako	jako	k9	jako
borelovské	borelovský	k2eAgFnPc1d1	borelovský
prostory	prostora	k1gFnPc1	prostora
izomorfní	izomorfní	k2eAgFnPc1d1	izomorfní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Standardní	standardní	k2eAgInSc1d1	standardní
borelovský	borelovský	k2eAgInSc1d1	borelovský
prostor	prostor	k1gInSc1	prostor
je	být	k5eAaImIp3nS	být
borelovský	borelovský	k2eAgInSc1d1	borelovský
prostor	prostor	k1gInSc1	prostor
na	na	k7c6	na
polském	polský	k2eAgInSc6d1	polský
prostoru	prostor	k1gInSc6	prostor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jakýkoli	jakýkoli	k3yIgInSc1	jakýkoli
standardní	standardní	k2eAgInSc1d1	standardní
borelovský	borelovský	k2eAgInSc1d1	borelovský
prostor	prostor	k1gInSc1	prostor
je	být	k5eAaImIp3nS	být
definovaný	definovaný	k2eAgInSc1d1	definovaný
(	(	kIx(	(
<g/>
až	až	k9	až
na	na	k7c4	na
isomorfismus	isomorfismus	k1gInSc4	isomorfismus
<g/>
)	)	kIx)	)
svoji	svůj	k3xOyFgFnSc4	svůj
kardinalitou	kardinalita	k1gFnSc7	kardinalita
<g/>
,	,	kIx,	,
a	a	k8xC	a
libovolný	libovolný	k2eAgInSc4d1	libovolný
nespočetný	spočetný	k2eNgInSc4d1	nespočetný
standardní	standardní	k2eAgInSc4d1	standardní
borelovský	borelovský	k2eAgInSc4d1	borelovský
prostor	prostor	k1gInSc4	prostor
má	mít	k5eAaImIp3nS	mít
kardinalitu	kardinalita	k1gFnSc4	kardinalita
kontinua	kontinuum	k1gNnSc2	kontinuum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
podmnožiny	podmnožina	k1gFnPc4	podmnožina
polských	polský	k2eAgMnPc2d1	polský
prostorů	prostor	k1gInPc2	prostor
lze	lze	k6eAd1	lze
borelovské	borelovský	k2eAgFnSc2d1	borelovský
množiny	množina	k1gFnSc2	množina
charakterizovat	charakterizovat	k5eAaBmF	charakterizovat
jako	jako	k8xS	jako
množiny	množina	k1gFnSc2	množina
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
jsou	být	k5eAaImIp3nP	být
obory	obor	k1gInPc1	obor
hodnot	hodnota	k1gFnPc2	hodnota
spojitých	spojitý	k2eAgNnPc2d1	spojité
injektivních	injektivní	k2eAgNnPc2d1	injektivní
zobrazení	zobrazení	k1gNnPc2	zobrazení
definovaný	definovaný	k2eAgInSc1d1	definovaný
na	na	k7c6	na
polských	polský	k2eAgInPc6d1	polský
prostorech	prostor	k1gInPc6	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
obor	obor	k1gInSc1	obor
hodnot	hodnota	k1gFnPc2	hodnota
spojitého	spojitý	k2eAgNnSc2d1	spojité
neinjektivního	injektivní	k2eNgNnSc2d1	injektivní
zobrazení	zobrazení	k1gNnSc2	zobrazení
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
borelovský	borelovský	k2eAgMnSc1d1	borelovský
<g/>
.	.	kIx.	.
</s>
<s>
Viz	vidět	k5eAaImRp2nS	vidět
analytická	analytický	k2eAgFnSc1d1	analytická
množina	množina	k1gFnSc1	množina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Každá	každý	k3xTgFnSc1	každý
pravděpodobnostní	pravděpodobnostní	k2eAgFnSc1d1	pravděpodobnostní
míra	míra	k1gFnSc1	míra
na	na	k7c6	na
standardním	standardní	k2eAgInSc6d1	standardní
borelovském	borelovský	k2eAgInSc6d1	borelovský
prostoru	prostor	k1gInSc6	prostor
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
prostoru	prostor	k1gInSc2	prostor
standardní	standardní	k2eAgInSc4d1	standardní
pravděpodobnostní	pravděpodobnostní	k2eAgInSc4d1	pravděpodobnostní
prostor	prostor	k1gInSc4	prostor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Neborelovské	Neborelovský	k2eAgFnPc1d1	Neborelovský
množiny	množina	k1gFnPc1	množina
==	==	k?	==
</s>
</p>
<p>
<s>
Příklad	příklad	k1gInSc1	příklad
podmnožiny	podmnožina	k1gFnSc2	podmnožina
reálných	reálný	k2eAgNnPc2d1	reálné
čísel	číslo	k1gNnPc2	číslo
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
není	být	k5eNaImIp3nS	být
borelovská	borelovský	k2eAgFnSc1d1	borelovský
<g/>
,	,	kIx,	,
a	a	k8xC	a
který	který	k3yRgInSc1	který
ukázal	ukázat	k5eAaPmAgInS	ukázat
Luzin	Luzin	k2eAgMnSc1d1	Luzin
(	(	kIx(	(
<g/>
Sect	Sect	k1gMnSc1	Sect
<g/>
.	.	kIx.	.
62	[number]	k4	62
<g/>
,	,	kIx,	,
stránky	stránka	k1gFnPc1	stránka
76	[number]	k4	76
<g/>
-	-	kIx~	-
<g/>
78	[number]	k4	78
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
popsán	popsat	k5eAaPmNgInS	popsat
níže	nízce	k6eAd2	nízce
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
<g/>
,	,	kIx,	,
příklad	příklad	k1gInSc4	příklad
neměřitelné	měřitelný	k2eNgFnSc2d1	neměřitelná
množiny	množina	k1gFnSc2	množina
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
ukázán	ukázán	k2eAgMnSc1d1	ukázán
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
její	její	k3xOp3gFnSc1	její
existence	existence	k1gFnSc1	existence
dokázána	dokázán	k2eAgFnSc1d1	dokázána
být	být	k5eAaImF	být
může	moct	k5eAaImIp3nS	moct
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Každé	každý	k3xTgNnSc1	každý
iracionální	iracionální	k2eAgNnSc1d1	iracionální
číslo	číslo	k1gNnSc1	číslo
lze	lze	k6eAd1	lze
jednoznačně	jednoznačně	k6eAd1	jednoznačně
reprezentovat	reprezentovat	k5eAaImF	reprezentovat
řetězovým	řetězový	k2eAgInSc7d1	řetězový
zlomkem	zlomek	k1gInSc7	zlomek
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋱	⋱	k?	⋱
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
=	=	kIx~	=
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
cfrac	cfrac	k1gInSc1	cfrac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
cfrac	cfrac	k1gInSc1	cfrac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
<g />
.	.	kIx.	.
</s>
<s>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
cfrac	cfrac	k1gInSc1	cfrac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
cfrac	cfrac	k1gInSc1	cfrac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
ddots	ddots	k1gInSc1	ddots
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}}}}}}}}}	}}}}}}}}}	k?	}}}}}}}}}
</s>
</p>
<p>
<s>
kde	kde	k6eAd1	kde
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
nějaké	nějaký	k3yIgNnSc4	nějaký
celé	celý	k2eAgNnSc4d1	celé
číslo	číslo	k1gNnSc4	číslo
a	a	k8xC	a
všechna	všechen	k3xTgNnPc1	všechen
ostatní	ostatní	k2eAgNnPc1d1	ostatní
čísla	číslo	k1gNnPc1	číslo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
jsou	být	k5eAaImIp3nP	být
kladná	kladný	k2eAgNnPc4d1	kladné
celá	celý	k2eAgNnPc4d1	celé
čísla	číslo	k1gNnPc4	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Nechť	nechť	k9	nechť
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
A	a	k9	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
A	a	k9	a
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
množina	množina	k1gFnSc1	množina
všech	všecek	k3xTgNnPc2	všecek
iracionálních	iracionální	k2eAgNnPc2d1	iracionální
čísel	číslo	k1gNnPc2	číslo
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
posloupnostem	posloupnost	k1gFnPc3	posloupnost
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
...	...	k?	...
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
dots	dots	k6eAd1	dots
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
následující	následující	k2eAgFnSc4d1	následující
vlastnost	vlastnost	k1gFnSc4	vlastnost
<g/>
:	:	kIx,	:
existuje	existovat	k5eAaImIp3nS	existovat
nekonečná	konečný	k2eNgFnSc1d1	nekonečná
podposloupnost	podposloupnost	k1gFnSc1	podposloupnost
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
...	...	k?	...
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
k_	k_	k?	k_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
<g/>
,	,	kIx,	,
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
k_	k_	k?	k_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
dots	dots	k6eAd1	dots
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
taková	takový	k3xDgFnSc1	takový
<g/>
,	,	kIx,	,
že	že	k8xS	že
každý	každý	k3xTgInSc1	každý
prvek	prvek	k1gInSc1	prvek
je	být	k5eAaImIp3nS	být
dělitelem	dělitel	k1gInSc7	dělitel
svého	svůj	k3xOyFgMnSc2	svůj
následovníka	následovník	k1gMnSc2	následovník
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
množina	množina	k1gFnSc1	množina
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
A	a	k9	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
A	a	k9	a
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
není	být	k5eNaImIp3nS	být
borelovská	borelovský	k2eAgFnSc1d1	borelovský
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
analytická	analytický	k2eAgFnSc1d1	analytická
množina	množina	k1gFnSc1	množina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
kompletní	kompletní	k2eAgFnSc1d1	kompletní
ve	v	k7c6	v
třídě	třída	k1gFnSc6	třída
analytických	analytický	k2eAgFnPc2d1	analytická
množin	množina	k1gFnPc2	množina
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
podrobnosti	podrobnost	k1gFnPc1	podrobnost
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
od	od	k7c2	od
Kechrise	Kechrise	k1gFnSc2	Kechrise
o	o	k7c4	o
deskriptivní	deskriptivní	k2eAgFnSc4d1	deskriptivní
teorii	teorie	k1gFnSc4	teorie
množin	množina	k1gFnPc2	množina
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
ve	v	k7c6	v
cvičení	cvičení	k1gNnSc6	cvičení
(	(	kIx(	(
<g/>
27.2	[number]	k4	27.2
<g/>
)	)	kIx)	)
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
209	[number]	k4	209
<g/>
,	,	kIx,	,
v	v	k7c6	v
definici	definice	k1gFnSc6	definice
(	(	kIx(	(
<g/>
22.9	[number]	k4	22.9
<g/>
)	)	kIx)	)
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
169	[number]	k4	169
a	a	k8xC	a
cvičení	cvičení	k1gNnSc4	cvičení
(	(	kIx(	(
<g/>
3.4	[number]	k4	3.4
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
ii	ii	k?	ii
<g/>
)	)	kIx)	)
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
14	[number]	k4	14
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jinou	jiný	k2eAgFnSc7d1	jiná
neborelovskou	borelovský	k2eNgFnSc7d1	borelovský
množinou	množina	k1gFnSc7	množina
je	být	k5eAaImIp3nS	být
inverzní	inverzní	k2eAgInSc1d1	inverzní
obraz	obraz	k1gInSc1	obraz
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
[	[	kIx(	[
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
<s>
]	]	kIx)	]
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
[	[	kIx(	[
<g/>
0	[number]	k4	0
<g/>
]	]	kIx)	]
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
nekonečné	konečný	k2eNgFnPc1d1	nekonečná
paritní	paritní	k2eAgFnPc1d1	paritní
funkce	funkce	k1gFnPc1	funkce
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
<s>
:	:	kIx,	:
</s>
</p>
<p>
<s>
{	{	kIx(	{
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
<s>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
<s>
ω	ω	k?	ω
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
<s>
{	{	kIx(	{
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
\	\	kIx~	\
<g/>
colon	colon	k1gNnSc1	colon
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
0,1	[number]	k4	0,1
<g/>
\	\	kIx~	\
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
omega	omega	k1gFnSc1	omega
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
0,1	[number]	k4	0,1
<g/>
\	\	kIx~	\
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
Nicméně	nicméně	k8xC	nicméně
toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
důkaz	důkaz	k1gInSc1	důkaz
(	(	kIx(	(
<g/>
pomocí	pomocí	k7c2	pomocí
axiomu	axiom	k1gInSc2	axiom
výběru	výběr	k1gInSc2	výběr
<g/>
)	)	kIx)	)
existence	existence	k1gFnSc2	existence
<g/>
,	,	kIx,	,
ne	ne	k9	ne
explicitní	explicitní	k2eAgInSc4d1	explicitní
příklad	příklad	k1gInSc4	příklad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Alternativní	alternativní	k2eAgFnPc1d1	alternativní
neekvivalentní	ekvivalentní	k2eNgFnPc1d1	neekvivalentní
definice	definice	k1gFnPc1	definice
==	==	k?	==
</s>
</p>
<p>
<s>
Podmnožina	podmnožina	k1gFnSc1	podmnožina
lokálně	lokálně	k6eAd1	lokálně
kompaktního	kompaktní	k2eAgInSc2d1	kompaktní
Hausdorffova	Hausdorffův	k2eAgInSc2d1	Hausdorffův
topologického	topologický	k2eAgInSc2d1	topologický
prostoru	prostor	k1gInSc2	prostor
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
borelovská	borelovský	k2eAgFnSc1d1	borelovský
množina	množina	k1gFnSc1	množina
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
nejmenšího	malý	k2eAgInSc2d3	nejmenší
σ	σ	k?	σ
obsahujícího	obsahující	k2eAgInSc2d1	obsahující
všechny	všechen	k3xTgFnPc1	všechen
kompaktní	kompaktní	k2eAgFnPc1d1	kompaktní
množiny	množina	k1gFnPc1	množina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Borel	Borel	k1gInSc1	Borel
set	set	k1gInSc1	set
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Skvělé	skvělý	k2eAgNnSc1d1	skvělé
vysvětlení	vysvětlení	k1gNnSc1	vysvětlení
mechanismu	mechanismus	k1gInSc2	mechanismus
Polské	polský	k2eAgFnSc2d1	polská
topologie	topologie	k1gFnSc2	topologie
je	být	k5eAaImIp3nS	být
podané	podaný	k2eAgNnSc1d1	podané
v	v	k7c6	v
kapitole	kapitola	k1gFnSc6	kapitola
3	[number]	k4	3
následující	následující	k2eAgFnSc2d1	následující
publikace	publikace	k1gFnSc2	publikace
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
William	William	k6eAd1	William
Arveson	Arveson	k1gMnSc1	Arveson
<g/>
,	,	kIx,	,
An	An	k1gMnSc1	An
Invitation	Invitation	k1gInSc1	Invitation
to	ten	k3xDgNnSc4	ten
C	C	kA	C
<g/>
*	*	kIx~	*
<g/>
-algebras	lgebras	k1gMnSc1	-algebras
<g/>
,	,	kIx,	,
Springer-Verlag	Springer-Verlag	k1gMnSc1	Springer-Verlag
<g/>
,	,	kIx,	,
1981	[number]	k4	1981
</s>
</p>
<p>
<s>
Richard	Richard	k1gMnSc1	Richard
Dudley	Dudlea	k1gFnSc2	Dudlea
<g/>
,	,	kIx,	,
Real	Real	k1gInSc4	Real
Analysis	Analysis	k1gFnSc2	Analysis
and	and	k?	and
Probability	probabilita	k1gFnSc2	probabilita
<g/>
.	.	kIx.	.
</s>
<s>
Wadsworth	Wadsworth	k1gInSc1	Wadsworth
<g/>
,	,	kIx,	,
Brooks	Brooks	k1gInSc1	Brooks
and	and	k?	and
Cole	cola	k1gFnSc6	cola
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
</s>
</p>
<p>
<s>
HALMOS	HALMOS	kA	HALMOS
<g/>
,	,	kIx,	,
Paul	Paul	k1gMnSc1	Paul
R.	R.	kA	R.
Teorie	teorie	k1gFnSc1	teorie
míry	míra	k1gFnSc2	míra
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
D.	D.	kA	D.
van	van	k1gInSc1	van
Nostrand	Nostrand	k1gInSc1	Nostrand
Co	co	k8xS	co
<g/>
,	,	kIx,	,
1950	[number]	k4	1950
<g/>
.	.	kIx.	.
</s>
<s>
Viz	vidět	k5eAaImRp2nS	vidět
zvláště	zvláště	k9	zvláště
Sect	Sect	k1gInSc1	Sect
<g/>
.	.	kIx.	.
51	[number]	k4	51
"	"	kIx"	"
<g/>
borelovské	borelovský	k2eAgFnSc2d1	borelovský
množiny	množina	k1gFnSc2	množina
a	a	k8xC	a
Baireovy	Baireův	k2eAgFnSc2d1	Baireův
množiny	množina	k1gFnSc2	množina
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Halsey	Halsea	k1gFnPc1	Halsea
Royden	Roydna	k1gFnPc2	Roydna
<g/>
,	,	kIx,	,
Real	Real	k1gInSc4	Real
Analysis	Analysis	k1gFnSc2	Analysis
<g/>
,	,	kIx,	,
Prentice	Prentice	k1gFnSc2	Prentice
Hall	Halla	k1gFnPc2	Halla
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
</s>
</p>
<p>
<s>
Alexander	Alexandra	k1gFnPc2	Alexandra
S.	S.	kA	S.
Kechris	Kechris	k1gFnPc1	Kechris
<g/>
,	,	kIx,	,
Classical	Classical	k1gFnPc1	Classical
Descriptive	Descriptiv	k1gInSc5	Descriptiv
Set	set	k1gInSc1	set
Theory	Theor	k1gInPc1	Theor
(	(	kIx(	(
<g/>
Klasická	klasický	k2eAgFnSc1d1	klasická
deskriptivní	deskriptivní	k2eAgFnSc1d1	deskriptivní
teorie	teorie	k1gFnSc1	teorie
množin	množina	k1gFnPc2	množina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Springer-Verlag	Springer-Verlag	k1gMnSc1	Springer-Verlag
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
(	(	kIx(	(
<g/>
Graduate	Graduat	k1gInSc5	Graduat
texts	texts	k6eAd1	texts
in	in	k?	in
Math	Math	k1gInSc1	Math
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
156	[number]	k4	156
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Baireova	Baireův	k2eAgFnSc1d1	Baireův
množina	množina	k1gFnSc1	množina
</s>
</p>
<p>
<s>
Cylindrická	cylindrický	k2eAgFnSc1d1	cylindrická
σ	σ	k?	σ
</s>
</p>
<p>
<s>
Polský	polský	k2eAgInSc1d1	polský
prostor	prostor	k1gInSc1	prostor
</s>
</p>
<p>
<s>
Deskriptivní	deskriptivní	k2eAgFnSc1d1	deskriptivní
teorie	teorie	k1gFnSc1	teorie
množin	množina	k1gFnPc2	množina
</s>
</p>
<p>
<s>
Borelovská	Borelovský	k2eAgFnSc1d1	Borelovský
hierarchie	hierarchie	k1gFnSc1	hierarchie
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
HAZEWINKEL	HAZEWINKEL	kA	HAZEWINKEL
<g/>
,	,	kIx,	,
Michiel	Michiel	k1gInSc1	Michiel
<g/>
.	.	kIx.	.
</s>
<s>
Borel	Borel	k1gInSc1	Borel
set	set	k1gInSc1	set
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Springer	Springer	k1gMnSc1	Springer
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
55608	[number]	k4	55608
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Formální	formální	k2eAgFnSc1d1	formální
definice	definice	k1gFnSc1	definice
borelovské	borelovský	k2eAgFnSc2d1	borelovský
množiny	množina	k1gFnSc2	množina
v	v	k7c6	v
Mizarově	Mizarův	k2eAgInSc6d1	Mizarův
systému	systém	k1gInSc6	systém
a	a	k8xC	a
Seznam	seznam	k1gInSc1	seznam
vět	věta	k1gFnPc2	věta
o	o	k7c6	o
tomto	tento	k3xDgNnSc6	tento
tématu	téma	k1gNnSc6	téma
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
formálně	formálně	k6eAd1	formálně
dokázány	dokázán	k2eAgFnPc1d1	dokázána
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Borelovská	Borelovský	k2eAgFnSc1d1	Borelovský
množina	množina	k1gFnSc1	množina
v	v	k7c6	v
encyklopedii	encyklopedie	k1gFnSc6	encyklopedie
MathWorld	MathWorlda	k1gFnPc2	MathWorlda
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
