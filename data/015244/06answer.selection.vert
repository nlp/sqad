<s desamb="1">
Objevila	objevit	k5eAaPmAgFnS
se	se	k3xPyFc4
v	v	k7c6
polovině	polovina	k1gFnSc6
12	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
ve	v	k7c6
střední	střední	k2eAgFnSc6d1
Francii	Francie	k1gFnSc6
<g/>
,	,	kIx,
přesněji	přesně	k6eAd2
v	v	k7c6
Île-de-France	Île-de-France	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
odtud	odtud	k6eAd1
se	se	k3xPyFc4
postupně	postupně	k6eAd1
rozšířila	rozšířit	k5eAaPmAgFnS
do	do	k7c2
většiny	většina	k1gFnSc2
ostatních	ostatní	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
západní	západní	k2eAgFnSc2d1
a	a	k8xC
střední	střední	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
<g/>
.	.	kIx.
</s>