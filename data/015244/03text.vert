<s>
Gotická	gotický	k2eAgFnSc1d1
architektura	architektura	k1gFnSc1
</s>
<s>
Možná	možná	k9
hledáte	hledat	k5eAaImIp2nP
<g/>
:	:	kIx,
Pokud	pokud	k8xS
hledáte	hledat	k5eAaImIp2nP
obecné	obecný	k2eAgFnPc1d1
a	a	k8xC
podrobnější	podrobný	k2eAgFnPc1d2
informace	informace	k1gFnPc1
ke	k	k7c3
gotickému	gotický	k2eAgInSc3d1
slohu	sloh	k1gInSc3
<g/>
,	,	kIx,
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
je	on	k3xPp3gFnPc4
v	v	k7c6
článku	článek	k1gInSc6
gotika	gotika	k1gFnSc1
<g/>
.	.	kIx.
<g/>
Katedrála	katedrála	k1gFnSc1
v	v	k7c4
Saint	Saint	k1gInSc4
Denis	Denisa	k1gFnPc2
realizovaná	realizovaný	k2eAgFnSc1d1
opatem	opat	k1gMnSc7
Sugerem	Suger	k1gMnSc7
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
považována	považován	k2eAgFnSc1d1
za	za	k7c4
první	první	k4xOgFnSc4
čistě	čistě	k6eAd1
gotickou	gotický	k2eAgFnSc4d1
stavbu	stavba	k1gFnSc4
</s>
<s>
Gotická	gotický	k2eAgFnSc1d1
architektura	architektura	k1gFnSc1
je	být	k5eAaImIp3nS
architektonický	architektonický	k2eAgInSc4d1
sloh	sloh	k1gInSc4
převládající	převládající	k2eAgInSc4d1
v	v	k7c6
období	období	k1gNnSc6
vrcholného	vrcholný	k2eAgInSc2d1
a	a	k8xC
pozdního	pozdní	k2eAgInSc2d1
středověku	středověk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Objevila	objevit	k5eAaPmAgFnS
se	se	k3xPyFc4
v	v	k7c6
polovině	polovina	k1gFnSc6
12	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
ve	v	k7c6
střední	střední	k2eAgFnSc6d1
Francii	Francie	k1gFnSc6
<g/>
,	,	kIx,
přesněji	přesně	k6eAd2
v	v	k7c6
Île-de-France	Île-de-France	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
odtud	odtud	k6eAd1
se	se	k3xPyFc4
postupně	postupně	k6eAd1
rozšířila	rozšířit	k5eAaPmAgFnS
do	do	k7c2
většiny	většina	k1gFnSc2
ostatních	ostatní	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
západní	západní	k2eAgFnSc2d1
a	a	k8xC
střední	střední	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velice	velice	k6eAd1
brzy	brzy	k6eAd1
se	se	k3xPyFc4
objevila	objevit	k5eAaPmAgFnS
v	v	k7c6
Anglii	Anglie	k1gFnSc6
<g/>
,	,	kIx,
později	pozdě	k6eAd2
také	také	k9
na	na	k7c6
Pyrenejském	pyrenejský	k2eAgInSc6d1
poloostrově	poloostrov	k1gInSc6
<g/>
,	,	kIx,
v	v	k7c6
Německu	Německo	k1gNnSc6
a	a	k8xC
dalších	další	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
střední	střední	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
včetně	včetně	k7c2
českých	český	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zvláště	zvláště	k6eAd1
v	v	k7c6
Itálii	Itálie	k1gFnSc6
vytvořila	vytvořit	k5eAaPmAgFnS
gotika	gotika	k1gFnSc1
zvláštní	zvláštní	k2eAgFnSc4d1
směs	směs	k1gFnSc4
s	s	k7c7
románskými	románský	k2eAgInPc7d1
<g/>
,	,	kIx,
antickými	antický	k2eAgInPc7d1
a	a	k8xC
byzantskými	byzantský	k2eAgInPc7d1
vlivy	vliv	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Itálii	Itálie	k1gFnSc6
se	se	k3xPyFc4
také	také	k9
zrodila	zrodit	k5eAaPmAgFnS
renesance	renesance	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
postupně	postupně	k6eAd1
gotiku	gotika	k1gFnSc4
zcela	zcela	k6eAd1
vytlačila	vytlačit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ještě	ještě	k6eAd1
na	na	k7c6
počátku	počátek	k1gInSc6
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
však	však	k9
především	především	k6eAd1
ve	v	k7c6
střední	střední	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
vznikala	vznikat	k5eAaImAgFnS
v	v	k7c6
podstatě	podstata	k1gFnSc6
gotická	gotický	k2eAgNnPc4d1
stavební	stavební	k2eAgNnPc4d1
díla	dílo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s>
Název	název	k1gInSc1
„	„	k?
<g/>
gotika	gotika	k1gFnSc1
<g/>
“	“	k?
zavedli	zavést	k5eAaPmAgMnP
italští	italský	k2eAgMnPc1d1
humanisté	humanista	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
ji	on	k3xPp3gFnSc4
mylně	mylně	k6eAd1
spojovali	spojovat	k5eAaImAgMnP
s	s	k7c7
Góty	Gót	k1gMnPc7
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gNnSc1
umění	umění	k1gNnSc1
bylo	být	k5eAaImAgNnS
považováno	považován	k2eAgNnSc1d1
za	za	k7c4
barbarské	barbarský	k2eAgNnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tito	tento	k3xDgMnPc1
humanisté	humanista	k1gMnPc1
totiž	totiž	k9
za	za	k7c4
barbarskou	barbarský	k2eAgFnSc4d1
a	a	k8xC
„	„	k?
<g/>
odporující	odporující	k2eAgFnSc4d1
každému	každý	k3xTgInSc3
dobrému	dobrý	k2eAgInSc3d1
vkusu	vkus	k1gInSc3
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
považovali	považovat	k5eAaImAgMnP
i	i	k8xC
samu	sám	k3xTgFnSc4
gotiku	gotika	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
veliké	veliký	k2eAgNnSc4d1
pochopení	pochopení	k1gNnSc4
pro	pro	k7c4
gotiku	gotika	k1gFnSc4
měli	mít	k5eAaImAgMnP
někteří	některý	k3yIgMnPc1
barokní	barokní	k2eAgMnPc1d1
architekti	architekt	k1gMnPc1
<g/>
,	,	kIx,
například	například	k6eAd1
Francesco	Francesco	k6eAd1
Borromini	Borromin	k2eAgMnPc1d1
či	či	k8xC
Jan	Jan	k1gMnSc1
Blažej	Blažej	k1gMnSc1
Santini-Aichel	Santini-Aichel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tito	tento	k3xDgMnPc1
architekti	architekt	k1gMnPc1
se	se	k3xPyFc4
gotickými	gotický	k2eAgInPc7d1
prvky	prvek	k1gInPc7
a	a	k8xC
stavbami	stavba	k1gFnPc7
velmi	velmi	k6eAd1
výrazně	výrazně	k6eAd1
inspirovali	inspirovat	k5eAaBmAgMnP
ve	v	k7c6
svých	svůj	k3xOyFgNnPc6
dílech	dílo	k1gNnPc6
<g/>
,	,	kIx,
která	který	k3yQgNnPc1,k3yRgNnPc1,k3yIgNnPc1
jsou	být	k5eAaImIp3nP
proto	proto	k8xC
někdy	někdy	k6eAd1
poněkud	poněkud	k6eAd1
nesprávně	správně	k6eNd1
řazena	řadit	k5eAaImNgFnS
do	do	k7c2
tzv.	tzv.	kA
Barokní	barokní	k2eAgFnSc2d1
gotiky	gotika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všeobecně	všeobecně	k6eAd1
začala	začít	k5eAaPmAgFnS
být	být	k5eAaImF
gotika	gotika	k1gFnSc1
opět	opět	k6eAd1
přijímána	přijímat	k5eAaImNgNnP
v	v	k7c6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
také	také	k9
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
rekonstrukcím	rekonstrukce	k1gFnPc3
mnoha	mnoho	k4c2
gotických	gotický	k2eAgFnPc2d1
staveb	stavba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Vznik	vznik	k1gInSc1
gotické	gotický	k2eAgFnSc2d1
architektury	architektura	k1gFnSc2
</s>
<s>
Chórový	chórový	k2eAgInSc1d1
ochoz	ochoz	k1gInSc1
v	v	k7c6
Saint-Denis	Saint-Denis	k1gFnSc6
</s>
<s>
Pohled	pohled	k1gInSc1
od	od	k7c2
Seiny	Seina	k1gFnSc2
na	na	k7c4
katedrálu	katedrála	k1gFnSc4
Notre-Dame	Notre-Dam	k1gInSc5
v	v	k7c6
Paříži	Paříž	k1gFnSc6
</s>
<s>
Gotická	gotický	k2eAgFnSc1d1
architektura	architektura	k1gFnSc1
vznikla	vzniknout	k5eAaPmAgFnS
transformací	transformace	k1gFnSc7
<g/>
,	,	kIx,
monumentalizací	monumentalizace	k1gFnSc7
a	a	k8xC
domyšlením	domyšlení	k1gNnSc7
prvků	prvek	k1gInPc2
pozdně	pozdně	k6eAd1
románské	románský	k2eAgFnSc2d1
architektury	architektura	k1gFnSc2
Île-de-France	Île-de-France	k1gFnSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
výrazně	výrazně	k6eAd1
čerpá	čerpat	k5eAaImIp3nS
také	také	k9
z	z	k7c2
pozdně	pozdně	k6eAd1
románské	románský	k2eAgFnSc2d1
architektury	architektura	k1gFnSc2
Normandie	Normandie	k1gFnSc2
<g/>
,	,	kIx,
Burgundska	Burgundsko	k1gNnSc2
a	a	k8xC
dalších	další	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Lomený	lomený	k2eAgInSc4d1
oblouk	oblouk	k1gInSc4
přitom	přitom	k6eAd1
převzali	převzít	k5eAaPmAgMnP
Normané	Norman	k1gMnPc1
na	na	k7c6
konci	konec	k1gInSc6
jedenáctého	jedenáctý	k4xOgNnSc2
století	století	k1gNnSc2
na	na	k7c6
Sicílii	Sicílie	k1gFnSc6
od	od	k7c2
Arabů	Arab	k1gMnPc2
a	a	k8xC
žebrová	žebrový	k2eAgFnSc1d1
klenba	klenba	k1gFnSc1
se	se	k3xPyFc4
vyvinula	vyvinout	k5eAaPmAgFnS
v	v	k7c6
téže	týž	k3xTgFnSc6,k3xDgFnSc6
době	doba	k1gFnSc6
v	v	k7c6
italské	italský	k2eAgFnSc6d1
Lombardii	Lombardie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Saint-Denis	Saint-Denis	k1gFnSc1
</s>
<s>
Za	za	k7c4
první	první	k4xOgFnSc4
gotickou	gotický	k2eAgFnSc4d1
stavbu	stavba	k1gFnSc4
je	být	k5eAaImIp3nS
obecně	obecně	k6eAd1
považována	považován	k2eAgFnSc1d1
přestavba	přestavba	k1gFnSc1
chrámu	chrám	k1gInSc2
(	(	kIx(
<g/>
přesněji	přesně	k6eAd2
ovšem	ovšem	k9
pouze	pouze	k6eAd1
novostavba	novostavba	k1gFnSc1
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
západního	západní	k2eAgNnSc2d1
dvojvěžového	dvojvěžový	k2eAgNnSc2d1
průčelí	průčelí	k1gNnSc2
a	a	k8xC
chóru	chór	k1gInSc2
s	s	k7c7
chórovým	chórový	k2eAgInSc7d1
ochozem	ochoz	k1gInSc7
a	a	k8xC
věncem	věnec	k1gInSc7
kaplí	kaple	k1gFnPc2
<g/>
)	)	kIx)
benediktinského	benediktinský	k2eAgInSc2d1
kláštera	klášter	k1gInSc2
Saint-Denis	Saint-Denis	k1gFnSc2
nedaleko	nedaleko	k7c2
Paříže	Paříž	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
vznikl	vzniknout	k5eAaPmAgInS
před	před	k7c7
polovinou	polovina	k1gFnSc7
12	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Iniciátorem	iniciátor	k1gMnSc7
této	tento	k3xDgFnSc2
stavby	stavba	k1gFnSc2
byl	být	k5eAaImAgMnS
opat	opat	k1gMnSc1
Suger	Suger	k1gMnSc1
<g/>
,	,	kIx,
jeden	jeden	k4xCgMnSc1
z	z	k7c2
nejmocnějších	mocný	k2eAgMnPc2d3
mužů	muž	k1gMnPc2
tehdejší	tehdejší	k2eAgFnSc2d1
Francie	Francie	k1gFnSc2
a	a	k8xC
důvěrný	důvěrný	k2eAgMnSc1d1
přítel	přítel	k1gMnSc1
králů	král	k1gMnPc2
Ludvíka	Ludvík	k1gMnSc2
VI	VI	kA
<g/>
.	.	kIx.
a	a	k8xC
Ludvíka	Ludvík	k1gMnSc4
VII	VII	kA
<g/>
..	..	k?
Dochoval	dochovat	k5eAaPmAgMnS
se	se	k3xPyFc4
zcela	zcela	k6eAd1
unikátní	unikátní	k2eAgInSc1d1
text	text	k1gInSc1
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgNnSc6
opat	opat	k1gMnSc1
Suger	Suger	k1gMnSc1
popisuje	popisovat	k5eAaImIp3nS
průběh	průběh	k1gInSc4
stavby	stavba	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Tento	tento	k3xDgInSc1
text	text	k1gInSc1
je	být	k5eAaImIp3nS
dnes	dnes	k6eAd1
důležitým	důležitý	k2eAgInSc7d1
pramenem	pramen	k1gInSc7
pro	pro	k7c4
poznání	poznání	k1gNnSc4
a	a	k8xC
pochopení	pochopení	k1gNnSc4
středověkého	středověký	k2eAgNnSc2d1
myšlení	myšlení	k1gNnSc2
a	a	k8xC
ikonografie	ikonografie	k1gFnSc2
středověkých	středověký	k2eAgFnPc2d1
staveb	stavba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sugerovou	Sugerův	k2eAgFnSc7d1
snahou	snaha	k1gFnSc7
bylo	být	k5eAaImAgNnS
oslavit	oslavit	k5eAaPmF
významného	významný	k2eAgMnSc4d1
světce	světec	k1gMnSc4
a	a	k8xC
francouzského	francouzský	k2eAgMnSc2d1
národního	národní	k2eAgMnSc2d1
patrona	patron	k1gMnSc2
svatého	svatý	k2eAgMnSc2d1
Diviše	Diviš	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
zde	zde	k6eAd1
byl	být	k5eAaImAgMnS
pohřben	pohřbít	k5eAaPmNgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
kostele	kostel	k1gInSc6
je	být	k5eAaImIp3nS
proto	proto	k8xC
také	také	k9
dodnes	dodnes	k6eAd1
pohřebiště	pohřebiště	k1gNnSc4
francouzských	francouzský	k2eAgMnPc2d1
králů	král	k1gMnPc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
jeho	on	k3xPp3gInSc4,k3xOp3gInSc4
význam	význam	k1gInSc4
v	v	k7c6
Sugerově	Sugerův	k2eAgFnSc6d1
době	doba	k1gFnSc6
pochopitelně	pochopitelně	k6eAd1
ještě	ještě	k6eAd1
zvyšovalo	zvyšovat	k5eAaImAgNnS
<g/>
.	.	kIx.
</s>
<s>
Revolučnost	revolučnost	k1gFnSc1
Saint-Denis	Saint-Denis	k1gFnPc2
spočívá	spočívat	k5eAaImIp3nS
v	v	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
zde	zde	k6eAd1
bylo	být	k5eAaImAgNnS
novým	nový	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
spojeno	spojit	k5eAaPmNgNnS
několik	několik	k4yIc4
stavebních	stavební	k2eAgInPc2d1
prvků	prvek	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
byly	být	k5eAaImAgInP
samostatně	samostatně	k6eAd1
užity	užit	k2eAgInPc1d1
na	na	k7c6
některých	některý	k3yIgFnPc6
starších	starý	k2eAgFnPc6d2
stavbách	stavba	k1gFnPc6
Île-de-France	Île-de-France	k1gFnSc2
a	a	k8xC
také	také	k9
nedaleké	daleký	k2eNgFnSc2d1
Normandie	Normandie	k1gFnSc2
izolovaně	izolovaně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vznikl	vzniknout	k5eAaPmAgInS
tak	tak	k9
celek	celek	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
pravzorem	pravzor	k1gInSc7
všech	všecek	k3xTgFnPc2
ostatních	ostatní	k2eAgFnPc2d1
gotických	gotický	k2eAgFnPc2d1
katedrál	katedrála	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Především	především	k6eAd1
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
žebrová	žebrový	k2eAgFnSc1d1
křížová	křížový	k2eAgFnSc1d1
klenba	klenba	k1gFnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
západní	západní	k2eAgNnPc4d1
průčelí	průčelí	k1gNnPc4
s	s	k7c7
mohutnými	mohutný	k2eAgInPc7d1
vstupními	vstupní	k2eAgInPc7d1
portály	portál	k1gInPc7
a	a	k8xC
dvojicí	dvojice	k1gFnSc7
věží	věž	k1gFnPc2
<g/>
,	,	kIx,
chórový	chórový	k2eAgInSc1d1
ochoz	ochoz	k1gInSc1
(	(	kIx(
<g/>
v	v	k7c6
Saint-Denis	Saint-Denis	k1gFnSc6
nezvykle	zvykle	k6eNd1
dvojlodní	dvojlodní	k2eAgFnSc1d1
<g/>
)	)	kIx)
s	s	k7c7
věncem	věnec	k1gInSc7
kaplí	kaple	k1gFnPc2
<g/>
,	,	kIx,
použití	použití	k1gNnSc1
vitráží	vitráž	k1gFnPc2
osazených	osazený	k2eAgFnPc2d1
v	v	k7c6
rozměrných	rozměrný	k2eAgInPc6d1
oknech	okno	k1gNnPc6
a	a	k8xC
s	s	k7c7
tím	ten	k3xDgNnSc7
související	související	k2eAgNnSc1d1
použití	použití	k1gNnSc1
opěrného	opěrný	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Gotika	gotika	k1gFnSc1
jako	jako	k9
královský	královský	k2eAgInSc4d1
sloh	sloh	k1gInSc4
a	a	k8xC
vztah	vztah	k1gInSc4
gotiky	gotika	k1gFnSc2
k	k	k7c3
antice	antika	k1gFnSc3
</s>
<s>
Gotický	gotický	k2eAgInSc1d1
sloh	sloh	k1gInSc1
je	být	k5eAaImIp3nS
vedle	vedle	k7c2
antického	antický	k2eAgInSc2d1
systému	systém	k1gInSc2
sloupových	sloupový	k2eAgInPc2d1
řádů	řád	k1gInPc2
(	(	kIx(
<g/>
renesance	renesance	k1gFnSc1
<g/>
,	,	kIx,
baroko	baroko	k1gNnSc1
<g/>
,	,	kIx,
klasicismus	klasicismus	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
architektury	architektura	k1gFnSc2
klasické	klasický	k2eAgFnSc2d1
moderny	moderna	k1gFnSc2
(	(	kIx(
<g/>
purismus	purismus	k1gInSc1
<g/>
,	,	kIx,
konstruktivismus	konstruktivismus	k1gInSc1
<g/>
,	,	kIx,
funkcionalismus	funkcionalismus	k1gInSc1
<g/>
)	)	kIx)
nejvýznamnějším	významný	k2eAgInSc7d3
architektonickým	architektonický	k2eAgInSc7d1
systémem	systém	k1gInSc7
používaným	používaný	k2eAgInSc7d1
v	v	k7c6
evropském	evropský	k2eAgNnSc6d1
stavebnictví	stavebnictví	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoli	ačkoli	k8xS
byla	být	k5eAaImAgFnS
gotika	gotika	k1gFnSc1
často	často	k6eAd1
(	(	kIx(
<g/>
a	a	k8xC
oprávněně	oprávněně	k6eAd1
<g/>
)	)	kIx)
chápána	chápán	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
jistý	jistý	k2eAgInSc1d1
protiklad	protiklad	k1gInSc1
antické	antický	k2eAgFnSc2d1
stavební	stavební	k2eAgFnSc2d1
tradice	tradice	k1gFnSc2
<g/>
,	,	kIx,
původně	původně	k6eAd1
se	se	k3xPyFc4
paradoxně	paradoxně	k6eAd1
snažila	snažit	k5eAaImAgFnS
na	na	k7c6
ní	on	k3xPp3gFnSc6
v	v	k7c6
mnoha	mnoho	k4c6
věcech	věc	k1gFnPc6
navázat	navázat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dokládá	dokládat	k5eAaImIp3nS
to	ten	k3xDgNnSc1
například	například	k6eAd1
použití	použití	k1gNnSc1
řady	řada	k1gFnSc2
antikizujících	antikizující	k2eAgInPc2d1
sloupů	sloup	k1gInPc2
mezilodních	mezilodní	k2eAgFnPc2d1
arkád	arkáda	k1gFnPc2
v	v	k7c6
chórech	chór	k1gInPc6
a	a	k8xC
chrámových	chrámový	k2eAgFnPc6d1
lodích	loď	k1gFnPc6
raně	raně	k6eAd1
gotických	gotický	k2eAgFnPc2d1
katedrál	katedrála	k1gFnPc2
(	(	kIx(
<g/>
Saint-Denis	Saint-Denis	k1gInSc1
<g/>
,	,	kIx,
Laon	Laon	k1gInSc1
<g/>
,	,	kIx,
Soissons	Soissons	k1gInSc1
<g/>
,	,	kIx,
Notre-Dame	Notre-Dam	k1gInSc5
v	v	k7c6
Paříži	Paříž	k1gFnSc6
<g/>
,	,	kIx,
Remeš	Remeš	k1gMnSc1
<g/>
,	,	kIx,
atd.	atd.	kA
<g/>
)	)	kIx)
</s>
<s>
Toto	tento	k3xDgNnSc1
navázání	navázání	k1gNnSc1
na	na	k7c4
antiku	antika	k1gFnSc4
bylo	být	k5eAaImAgNnS
naprosto	naprosto	k6eAd1
cílené	cílený	k2eAgNnSc1d1
a	a	k8xC
souviselo	souviset	k5eAaImAgNnS
se	se	k3xPyFc4
snahou	snaha	k1gFnSc7
francouzských	francouzský	k2eAgMnPc2d1
králů	král	k1gMnPc2
z	z	k7c2
rodu	rod	k1gInSc2
Kapetovců	Kapetovec	k1gInPc2
(	(	kIx(
<g/>
stejně	stejně	k6eAd1
jako	jako	k8xC,k8xS
dříve	dříve	k6eAd2
Karel	Karel	k1gMnSc1
Veliký	veliký	k2eAgMnSc1d1
či	či	k8xC
Ota	Ota	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
<g/>
)	)	kIx)
legitimizovat	legitimizovat	k5eAaBmF
své	svůj	k3xOyFgNnSc4
mocenské	mocenský	k2eAgNnSc4d1
postavení	postavení	k1gNnSc4
odvoláním	odvolání	k1gNnSc7
se	se	k3xPyFc4
na	na	k7c4
antickou	antický	k2eAgFnSc4d1
římskou	římský	k2eAgFnSc4d1
říši	říše	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Období	období	k1gNnSc1
nástupu	nástup	k1gInSc2
gotiky	gotika	k1gFnSc2
se	se	k3xPyFc4
také	také	k9
pozoruhodně	pozoruhodně	k6eAd1
kryje	krýt	k5eAaImIp3nS
s	s	k7c7
obdobím	období	k1gNnSc7
jejich	jejich	k3xOp3gNnSc2
postupného	postupný	k2eAgNnSc2d1
rozšiřování	rozšiřování	k1gNnSc2
a	a	k8xC
upevňování	upevňování	k1gNnSc2
vládnoucí	vládnoucí	k2eAgFnSc2d1
moci	moc	k1gFnSc2
v	v	k7c6
celé	celý	k2eAgFnSc6d1
Francii	Francie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novostavby	novostavba	k1gFnSc2
obrovských	obrovský	k2eAgFnPc2d1
gotických	gotický	k2eAgFnPc2d1
katedrál	katedrála	k1gFnPc2
tak	tak	k6eAd1
jednoznačně	jednoznačně	k6eAd1
znamenají	znamenat	k5eAaImIp3nP
demonstraci	demonstrace	k1gFnSc4
moci	moc	k1gFnSc2
francouzských	francouzský	k2eAgMnPc2d1
králů	král	k1gMnPc2
a	a	k8xC
je	být	k5eAaImIp3nS
zcela	zcela	k6eAd1
oprávněné	oprávněný	k2eAgNnSc1d1
<g/>
,	,	kIx,
hovořit	hovořit	k5eAaImF
proto	proto	k8xC
o	o	k7c6
gotice	gotika	k1gFnSc6
jako	jako	k8xC,k8xS
o	o	k7c6
francouzském	francouzský	k2eAgInSc6d1
královském	královský	k2eAgInSc6d1
stavebním	stavební	k2eAgInSc6d1
slohu	sloh	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
také	také	k9
celé	celý	k2eAgNnSc4d1
působení	působení	k1gNnSc4
gotické	gotický	k2eAgFnSc2d1
architektury	architektura	k1gFnSc2
<g/>
,	,	kIx,
jejímž	jejíž	k3xOyRp3gInSc7
nejvlastnějším	vlastní	k2eAgInSc7d3
a	a	k8xC
hlavním	hlavní	k2eAgInSc7d1
plodem	plod	k1gInSc7
je	být	k5eAaImIp3nS
gotická	gotický	k2eAgFnSc1d1
katedrála	katedrála	k1gFnSc1
<g/>
,	,	kIx,
směřuje	směřovat	k5eAaImIp3nS
k	k	k7c3
pompéznosti	pompéznost	k1gFnSc3
<g/>
,	,	kIx,
nádheře	nádhera	k1gFnSc6
<g/>
,	,	kIx,
jasu	jas	k1gInSc6
<g/>
,	,	kIx,
„	„	k?
<g/>
nekonečné	konečný	k2eNgFnSc2d1
<g/>
“	“	k?
délce	délka	k1gFnSc6
<g/>
,	,	kIx,
šířce	šířka	k1gFnSc6
i	i	k8xC
výšce	výška	k1gFnSc6
staveb	stavba	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
doslova	doslova	k6eAd1
způsobují	způsobovat	k5eAaImIp3nP
závrať	závrať	k1gFnSc4
a	a	k8xC
jsou	být	k5eAaImIp3nP
vybudovány	vybudovat	k5eAaPmNgInP
v	v	k7c6
podstatě	podstata	k1gFnSc6
na	na	k7c6
samotné	samotný	k2eAgFnSc6d1
hranici	hranice	k1gFnSc6
únosnosti	únosnost	k1gFnSc2
použitých	použitý	k2eAgInPc2d1
stavebních	stavební	k2eAgInPc2d1
materiálů	materiál	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Interiér	interiér	k1gInSc1
katedrály	katedrála	k1gFnSc2
v	v	k7c6
Laonu	Laon	k1gInSc6
</s>
<s>
Le	Le	k?
Mans	Mansa	k1gFnPc2
<g/>
,	,	kIx,
Laon	Laona	k1gFnPc2
a	a	k8xC
další	další	k2eAgFnSc2d1
významné	významný	k2eAgFnSc2d1
stavby	stavba	k1gFnSc2
počátků	počátek	k1gInPc2
gotiky	gotika	k1gFnSc2
</s>
<s>
Účinek	účinek	k1gInSc1
Saint-Denis	Saint-Denis	k1gFnSc2
byl	být	k5eAaImAgInS
takřka	takřka	k6eAd1
okamžitý	okamžitý	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velice	velice	k6eAd1
rychle	rychle	k6eAd1
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
přejímání	přejímání	k1gNnSc3
a	a	k8xC
dalšímu	další	k2eAgNnSc3d1
rozvíjení	rozvíjení	k1gNnSc3
stavebního	stavební	k2eAgInSc2d1
systému	systém	k1gInSc2
ze	z	k7c2
Saint-Denis	Saint-Denis	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Významné	významný	k2eAgInPc1d1
impulsy	impuls	k1gInPc1
však	však	k9
přišly	přijít	k5eAaPmAgInP
také	také	k9
z	z	k7c2
jiných	jiný	k2eAgFnPc2d1
staveb	stavba	k1gFnPc2
<g/>
,	,	kIx,
například	například	k6eAd1
z	z	k7c2
katedrály	katedrála	k1gFnSc2
v	v	k7c6
Le	Le	k1gFnSc6
Mans	Mansa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
této	tento	k3xDgFnSc6
katedrále	katedrála	k1gFnSc6
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc4
novostavbu	novostavba	k1gFnSc4
podporoval	podporovat	k5eAaImAgMnS
také	také	k9
anglický	anglický	k2eAgMnSc1d1
král	král	k1gMnSc1
Jindřich	Jindřich	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plantagenet	Plantagenet	k1gInSc1
<g/>
,	,	kIx,
a	a	k8xC
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
pro	pro	k7c4
Plantagenety	Plantagenet	k1gInPc4
znamenala	znamenat	k5eAaImAgFnS
v	v	k7c6
podstatě	podstata	k1gFnSc6
totéž	týž	k3xTgNnSc1
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
Saint-Denis	Saint-Denis	k1gFnPc1
pro	pro	k7c4
Kapetovce	Kapetovec	k1gMnPc4
<g/>
,	,	kIx,
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
zajímavé	zajímavý	k2eAgFnSc3d1
syntéze	syntéza	k1gFnSc3
románského	románský	k2eAgInSc2d1
slohu	sloh	k1gInSc2
s	s	k7c7
prvky	prvek	k1gInPc7
nového	nový	k2eAgInSc2d1
slohu	sloh	k1gInSc2
gotického	gotický	k2eAgInSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celá	celý	k2eAgFnSc1d1
stavba	stavba	k1gFnSc1
tak	tak	k9
působí	působit	k5eAaImIp3nS
velice	velice	k6eAd1
těžce	těžce	k6eAd1
a	a	k8xC
mohutně	mohutně	k6eAd1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
sice	sice	k8xC
zcela	zcela	k6eAd1
opačné	opačný	k2eAgNnSc4d1
působení	působení	k1gNnSc4
než	než	k8xS
v	v	k7c6
Saint-Denis	Saint-Denis	k1gFnSc6
<g/>
,	,	kIx,
ovšem	ovšem	k9
přesto	přesto	k8xC
je	být	k5eAaImIp3nS
patrný	patrný	k2eAgInSc1d1
silný	silný	k2eAgInSc1d1
vliv	vliv	k1gInSc1
této	tento	k3xDgFnSc2
stavby	stavba	k1gFnSc2
na	na	k7c4
množství	množství	k1gNnSc4
dalších	další	k2eAgFnPc2d1
gotických	gotický	k2eAgFnPc2d1
katedrál	katedrála	k1gFnPc2
ve	v	k7c6
Francii	Francie	k1gFnSc6
<g/>
,	,	kIx,
včetně	včetně	k7c2
těch	ten	k3xDgMnPc2
nejvýznačnějších	význačný	k2eAgMnPc2d3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
K	k	k7c3
nejvýznačnějším	význačný	k2eAgFnPc3d3
stavbám	stavba	k1gFnPc3
z	z	k7c2
této	tento	k3xDgFnSc2
doby	doba	k1gFnSc2
konstituování	konstituování	k1gNnSc2
a	a	k8xC
hledání	hledání	k1gNnSc2
gotické	gotický	k2eAgFnSc2d1
architektury	architektura	k1gFnSc2
patří	patřit	k5eAaImIp3nS
dále	daleko	k6eAd2
katedrály	katedrála	k1gFnSc2
v	v	k7c4
Sens	Sens	k1gInSc4
<g/>
,	,	kIx,
Noyonu	Noyona	k1gFnSc4
a	a	k8xC
také	také	k9
katedrála	katedrála	k1gFnSc1
v	v	k7c6
Laonu	Laono	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
této	tento	k3xDgFnSc6
ohromné	ohromný	k2eAgFnSc6d1
stavbě	stavba	k1gFnSc6
<g/>
,	,	kIx,
budované	budovaný	k2eAgInPc4d1
mezi	mezi	k7c7
léty	léto	k1gNnPc7
1160	#num#	k4
<g/>
–	–	k?
<g/>
1200	#num#	k4
a	a	k8xC
sloužící	sloužící	k2eAgMnSc1d1
jako	jako	k8xS,k8xC
významný	významný	k2eAgInSc1d1
poutní	poutní	k2eAgInSc1d1
kostel	kostel	k1gInSc1
<g/>
,	,	kIx,
poprvé	poprvé	k6eAd1
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
dosažení	dosažení	k1gNnSc3
formální	formální	k2eAgFnSc2d1
jednoty	jednota	k1gFnSc2
všech	všecek	k3xTgInPc2
architektonických	architektonický	k2eAgInPc2d1
prvků	prvek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
pozdější	pozdní	k2eAgInSc4d2
vývoj	vývoj	k1gInSc4
gotických	gotický	k2eAgFnPc2d1
katedrál	katedrála	k1gFnPc2
je	být	k5eAaImIp3nS
důležité	důležitý	k2eAgNnSc1d1
především	především	k9
řešení	řešení	k1gNnSc1
západního	západní	k2eAgNnSc2d1
dvojvěžového	dvojvěžový	k2eAgNnSc2d1
průčelí	průčelí	k1gNnSc2
se	se	k3xPyFc4
zcela	zcela	k6eAd1
originálně	originálně	k6eAd1
utvářeným	utvářený	k2eAgNnSc7d1
zakončením	zakončení	k1gNnSc7
těchto	tento	k3xDgFnPc2
věží	věž	k1gFnPc2
a	a	k8xC
trojicí	trojice	k1gFnSc7
mohutných	mohutný	k2eAgInPc2d1
vstupních	vstupní	k2eAgInPc2d1
portálů	portál	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
velkoryse	velkoryse	k6eAd1
a	a	k8xC
v	v	k7c6
mohutných	mohutný	k2eAgFnPc6d1
formách	forma	k1gFnPc6
utvářené	utvářený	k2eAgNnSc1d1
průčelí	průčelí	k1gNnSc1
opatřené	opatřený	k2eAgNnSc1d1
obrovskou	obrovský	k2eAgFnSc7d1
kružbovou	kružbový	k2eAgFnSc7d1
rozetou	rozeta	k1gFnSc7
je	být	k5eAaImIp3nS
skutečně	skutečně	k6eAd1
mimořádným	mimořádný	k2eAgInSc7d1
architektonickým	architektonický	k2eAgInSc7d1
počinem	počin	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Západní	západní	k2eAgNnSc1d1
průčelí	průčelí	k1gNnSc1
katedrály	katedrála	k1gFnSc2
Notre-Dame	Notre-Dam	k1gInSc5
v	v	k7c6
Paříži	Paříž	k1gFnSc6
</s>
<s>
Notre-Dame	Notre-Dam	k1gInSc5
<g/>
,	,	kIx,
Chartres	Chartres	k1gMnSc1
a	a	k8xC
Remeš	Remeš	k1gMnSc1
</s>
<s>
Snad	snad	k9
nejvýznamnější	významný	k2eAgFnSc1d3
trojice	trojice	k1gFnSc1
francouzských	francouzský	k2eAgFnPc2d1
katedrál	katedrála	k1gFnPc2
představuje	představovat	k5eAaImIp3nS
také	také	k9
další	další	k2eAgFnSc4d1
fázi	fáze	k1gFnSc4
vývoje	vývoj	k1gInSc2
gotické	gotický	k2eAgFnSc2d1
architektury	architektura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Pětilodní	pětilodní	k2eAgFnSc1d1
katedrála	katedrála	k1gFnSc1
Notre-Dame	Notre-Dam	k1gInSc5
(	(	kIx(
<g/>
1163	#num#	k4
<g/>
–	–	k?
<g/>
1245	#num#	k4
<g/>
)	)	kIx)
v	v	k7c6
Paříži	Paříž	k1gFnSc6
je	být	k5eAaImIp3nS
stavbou	stavba	k1gFnSc7
mimořádnou	mimořádný	k2eAgFnSc7d1
už	už	k6eAd1
svou	svůj	k3xOyFgFnSc7
velikostí	velikost	k1gFnSc7
<g/>
,	,	kIx,
neboť	neboť	k8xC
je	být	k5eAaImIp3nS
130	#num#	k4
m	m	kA
dlouhá	dlouhý	k2eAgFnSc1d1
a	a	k8xC
výška	výška	k1gFnSc1
klenby	klenba	k1gFnSc2
dosahuje	dosahovat	k5eAaImIp3nS
35	#num#	k4
m.	m.	k?
Pozoruhodná	pozoruhodný	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
též	též	k9
šestipaprsčitá	šestipaprsčitý	k2eAgFnSc1d1
žebrová	žebrový	k2eAgFnSc1d1
klenba	klenba	k1gFnSc1
<g/>
,	,	kIx,
jíž	jenž	k3xRgFnSc3
je	být	k5eAaImIp3nS
zaklenuta	zaklenut	k2eAgFnSc1d1
hlavní	hlavní	k2eAgFnSc1d1
loď	loď	k1gFnSc1
<g/>
,	,	kIx,
chór	chór	k1gInSc1
i	i	k8xC
transept	transept	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pozoruhodné	pozoruhodný	k2eAgNnSc1d1
je	být	k5eAaImIp3nS
i	i	k9
západní	západní	k2eAgNnSc1d1
průčelí	průčelí	k1gNnSc1
opatřené	opatřený	k2eAgFnSc2d1
mj.	mj.	kA
„	„	k?
<g/>
královskou	královský	k2eAgFnSc7d1
galerií	galerie	k1gFnSc7
<g/>
“	“	k?
<g/>
,	,	kIx,
tj.	tj.	kA
řadu	řada	k1gFnSc4
soch	socha	k1gFnPc2
francouzských	francouzský	k2eAgMnPc2d1
králů	král	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Katedrála	katedrála	k1gFnSc1
v	v	k7c4
Chartres	Chartres	k1gInSc4
je	být	k5eAaImIp3nS
nejvýznamnější	významný	k2eAgFnSc7d3
mariánskou	mariánský	k2eAgFnSc7d1
svatyní	svatyně	k1gFnSc7
ve	v	k7c6
Francii	Francie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tomu	ten	k3xDgNnSc3
odpovídá	odpovídat	k5eAaImIp3nS
i	i	k9
architektonické	architektonický	k2eAgNnSc4d1
ztvárnění	ztvárnění	k1gNnSc4
katedrály	katedrála	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
začala	začít	k5eAaPmAgFnS
budovat	budovat	k5eAaImF
po	po	k7c6
požáru	požár	k1gInSc6
v	v	k7c6
roce	rok	k1gInSc6
1194	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
prakticky	prakticky	k6eAd1
každá	každý	k3xTgFnSc1
nová	nový	k2eAgFnSc1d1
katedrála	katedrála	k1gFnSc1
tohoto	tento	k3xDgNnSc2
období	období	k1gNnSc2
měla	mít	k5eAaImAgFnS
být	být	k5eAaImF
ještě	ještě	k9
velkolepější	velkolepý	k2eAgFnSc4d2
<g/>
,	,	kIx,
větší	veliký	k2eAgFnSc4d2
a	a	k8xC
krásnější	krásný	k2eAgFnSc4d2
než	než	k8xS
všechny	všechen	k3xTgFnPc1
katedrály	katedrála	k1gFnPc1
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
postavené	postavený	k2eAgNnSc1d1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
i	i	k9
Chartres	Chartres	k1gInSc1
větší	veliký	k2eAgInSc1d2
a	a	k8xC
vyšší	vysoký	k2eAgInSc1d2
než	než	k8xS
Notre-Dame	Notre-Dam	k1gInSc5
v	v	k7c6
Paříži	Paříž	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Působí	působit	k5eAaImIp3nS
však	však	k9
poměrně	poměrně	k6eAd1
těžce	těžce	k6eAd1
<g/>
,	,	kIx,
protože	protože	k8xS
zcela	zcela	k6eAd1
záměrně	záměrně	k6eAd1
používá	používat	k5eAaImIp3nS
velice	velice	k6eAd1
předimenzované	předimenzovaný	k2eAgInPc4d1
pilíře	pilíř	k1gInPc4
<g/>
,	,	kIx,
zdivo	zdivo	k1gNnSc4
a	a	k8xC
další	další	k2eAgInPc4d1
architektonické	architektonický	k2eAgInPc4d1
prvky	prvek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc7
interiér	interiér	k1gInSc1
je	být	k5eAaImIp3nS
také	také	k9
poměrně	poměrně	k6eAd1
temný	temný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
tomu	ten	k3xDgNnSc3
ovšem	ovšem	k9
působí	působit	k5eAaImIp3nS
také	také	k9
jako	jako	k9
pevná	pevný	k2eAgFnSc1d1
<g/>
,	,	kIx,
nepohnutelná	pohnutelný	k2eNgFnSc1d1
a	a	k8xC
prakticky	prakticky	k6eAd1
věčná	věčný	k2eAgFnSc1d1
stavba	stavba	k1gFnSc1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
byl	být	k5eAaImAgInS
také	také	k6eAd1
hlavní	hlavní	k2eAgInSc4d1
architektonický	architektonický	k2eAgInSc4d1
záměr	záměr	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Poslední	poslední	k2eAgNnSc4d1
z	z	k7c2
trojice	trojice	k1gFnSc2
katedrál	katedrála	k1gFnPc2
je	být	k5eAaImIp3nS
korunovační	korunovační	k2eAgInSc1d1
chrám	chrám	k1gInSc1
francouzských	francouzský	k2eAgMnPc2d1
králů	král	k1gMnPc2
v	v	k7c6
Remeši	Remeš	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Katedrála	katedrála	k1gFnSc1
v	v	k7c6
Remeši	Remeš	k1gFnSc6
byla	být	k5eAaImAgFnS
budována	budovat	k5eAaImNgFnS
po	po	k7c6
požáru	požár	k1gInSc6
předchozí	předchozí	k2eAgFnSc2d1
svatyně	svatyně	k1gFnSc2
roku	rok	k1gInSc2
1210	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chartres	Chartres	k1gInSc1
předstihuje	předstihovat	k5eAaImIp3nS
nikoli	nikoli	k9
rozměry	rozměr	k1gInPc4
<g/>
,	,	kIx,
ale	ale	k8xC
především	především	k9
mírou	míra	k1gFnSc7
použití	použití	k1gNnSc2
architektonické	architektonický	k2eAgFnSc2d1
i	i	k8xC
sochařské	sochařský	k2eAgFnSc2d1
dekorace	dekorace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
této	tento	k3xDgFnSc6
katedrále	katedrála	k1gFnSc6
byly	být	k5eAaImAgFnP
poprvé	poprvé	k6eAd1
použity	použít	k5eAaPmNgFnP
kružby	kružba	k1gFnPc1
a	a	k8xC
stavba	stavba	k1gFnSc1
je	být	k5eAaImIp3nS
také	také	k9
<g/>
,	,	kIx,
především	především	k9
na	na	k7c6
západním	západní	k2eAgNnSc6d1
průčelí	průčelí	k1gNnSc6
<g/>
,	,	kIx,
vysloveně	vysloveně	k6eAd1
přeplněna	přeplnit	k5eAaPmNgNnP
množstvím	množství	k1gNnSc7
mohutných	mohutný	k2eAgFnPc2d1
gotických	gotický	k2eAgFnPc2d1
soch	socha	k1gFnPc2
i	i	k8xC
nejrůznějších	různý	k2eAgInPc2d3
architektonických	architektonický	k2eAgInPc2d1
prvků	prvek	k1gInPc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
jsou	být	k5eAaImIp3nP
například	například	k6eAd1
veliké	veliký	k2eAgFnPc1d1
fiály	fiála	k1gFnPc1
kryjící	kryjící	k2eAgInPc4d1
tabernákly	tabernákl	k1gInPc4
se	s	k7c7
sochami	socha	k1gFnPc7
andělů	anděl	k1gMnPc2
a	a	k8xC
zdůrazňující	zdůrazňující	k2eAgMnPc4d1
už	už	k9
tak	tak	k6eAd1
velmi	velmi	k6eAd1
ozdobný	ozdobný	k2eAgInSc4d1
opěrný	opěrný	k2eAgInSc4d1
systém	systém	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dalšími	další	k2eAgFnPc7d1
významnými	významný	k2eAgFnPc7d1
katedrálami	katedrála	k1gFnPc7
tohoto	tento	k3xDgNnSc2
období	období	k1gNnSc2
jsou	být	k5eAaImIp3nP
katedrály	katedrál	k1gMnPc4
v	v	k7c4
Soissons	Soissons	k1gInSc4
<g/>
,	,	kIx,
Amiens	Amiens	k1gInSc4
<g/>
,	,	kIx,
Bourges	Bourges	k1gInSc4
či	či	k8xC
Beauvais	Beauvais	k1gInSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
dosáhla	dosáhnout	k5eAaPmAgFnS
štíhlost	štíhlost	k1gFnSc1
a	a	k8xC
odhmotnění	odhmotnění	k1gNnSc1
takových	takový	k3xDgInPc2
rozměrů	rozměr	k1gInPc2
<g/>
,	,	kIx,
že	že	k8xS
dokonce	dokonce	k9
dvakrát	dvakrát	k6eAd1
došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
zřícení	zřícení	k1gNnSc3
části	část	k1gFnSc2
stavby	stavba	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
ta	ten	k3xDgFnSc1
musela	muset	k5eAaImAgFnS
být	být	k5eAaImF
pak	pak	k6eAd1
nákladně	nákladně	k6eAd1
obnovována	obnovován	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Rayonantní	Rayonantní	k2eAgFnSc1d1
gotika	gotika	k1gFnSc1
v	v	k7c6
Saint-Denis	Saint-Denis	k1gFnSc6
</s>
<s>
Rayonantní	Rayonantní	k2eAgFnSc1d1
gotika	gotika	k1gFnSc1
jako	jako	k8xS,k8xC
vrchol	vrchol	k1gInSc1
gotické	gotický	k2eAgFnSc2d1
architektury	architektura	k1gFnSc2
</s>
<s>
V	v	k7c6
době	doba	k1gFnSc6
vlády	vláda	k1gFnSc2
francouzského	francouzský	k2eAgMnSc2d1
krále	král	k1gMnSc2
Ludvíka	Ludvík	k1gMnSc2
IX	IX	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1226	#num#	k4
<g/>
–	–	k?
<g/>
1270	#num#	k4
<g/>
)	)	kIx)
došla	dojít	k5eAaPmAgFnS
francouzská	francouzský	k2eAgFnSc1d1
rayonantní	rayonantní	k2eAgFnSc1d1
gotika	gotika	k1gFnSc1
(	(	kIx(
<g/>
cca	cca	kA
1240	#num#	k4
<g/>
-	-	kIx~
<g/>
1350	#num#	k4
<g/>
)	)	kIx)
svého	své	k1gNnSc2
vrcholu	vrchol	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavními	hlavní	k2eAgInPc7d1
znaky	znak	k1gInPc7
této	tento	k3xDgFnSc2
fáze	fáze	k1gFnSc2
gotiky	gotika	k1gFnSc2
je	být	k5eAaImIp3nS
použití	použití	k1gNnSc1
kružby	kružba	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
tím	ten	k3xDgNnSc7
mohla	moct	k5eAaImAgFnS
být	být	k5eAaImF
používána	používat	k5eAaImNgFnS
skutečně	skutečně	k6eAd1
veliká	veliký	k2eAgNnPc4d1
okna	okno	k1gNnPc4
<g/>
,	,	kIx,
takže	takže	k8xS
ve	v	k7c6
spojení	spojení	k1gNnSc6
s	s	k7c7
opěrným	opěrný	k2eAgInSc7d1
systémem	systém	k1gInSc7
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
maximálnímu	maximální	k2eAgNnSc3d1
odhmotnění	odhmotnění	k1gNnSc3
a	a	k8xC
prosvětlení	prosvětlení	k1gNnSc3
stavby	stavba	k1gFnSc2
(	(	kIx(
<g/>
ještě	ještě	k6eAd1
pařížská	pařížský	k2eAgFnSc1d1
Notre-Dame	Notre-Dam	k1gInSc5
či	či	k8xC
katedrála	katedrála	k1gFnSc1
v	v	k7c4
Chartres	Chartres	k1gInSc4
byly	být	k5eAaImAgInP
v	v	k7c6
interiéru	interiér	k1gInSc6
značně	značně	k6eAd1
temné	temný	k2eAgNnSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teprve	teprve	k6eAd1
v	v	k7c6
této	tento	k3xDgFnSc6
fázi	fáze	k1gFnSc6
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
maximálnímu	maximální	k2eAgNnSc3d1
potlačení	potlačení	k1gNnSc3
stěny	stěna	k1gFnSc2
a	a	k8xC
její	její	k3xOp3gFnSc3
redukci	redukce	k1gFnSc3
na	na	k7c4
pouhé	pouhý	k2eAgInPc4d1
pilíře	pilíř	k1gInPc4
<g/>
,	,	kIx,
takže	takže	k8xS
interiér	interiér	k1gInSc1
katedrály	katedrála	k1gFnSc2
připomíná	připomínat	k5eAaImIp3nS
v	v	k7c6
podstatě	podstata	k1gFnSc6
skeletovitý	skeletovitý	k2eAgInSc4d1
„	„	k?
<g/>
skleník	skleník	k1gInSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Upouštěno	upouštěn	k2eAgNnSc1d1
bylo	být	k5eAaImAgNnS
také	také	k6eAd1
od	od	k7c2
použití	použití	k1gNnSc2
sloupů	sloup	k1gInPc2
v	v	k7c6
mezilodních	mezilodní	k2eAgFnPc6d1
arkádách	arkáda	k1gFnPc6
ve	v	k7c4
prospěch	prospěch	k1gInSc4
důsledného	důsledný	k2eAgNnSc2d1
použití	použití	k1gNnSc2
svazkového	svazkový	k2eAgInSc2d1
pilíře	pilíř	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Nástup	nástup	k1gInSc1
rayonantní	rayonantní	k2eAgInSc1d1
(	(	kIx(
<g/>
paprskové	paprskový	k2eAgFnPc1d1
<g/>
)	)	kIx)
gotiky	gotika	k1gFnPc1
v	v	k7c6
podstatě	podstata	k1gFnSc6
znamená	znamenat	k5eAaImIp3nS
dokončení	dokončení	k1gNnSc4
procesu	proces	k1gInSc2
vznikání	vznikání	k1gNnSc2
gotického	gotický	k2eAgNnSc2d1
tvarosloví	tvarosloví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skončilo	skončit	k5eAaPmAgNnS
období	období	k1gNnSc1
hledání	hledání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nastalo	nastat	k5eAaPmAgNnS
období	období	k1gNnSc1
profesionalizace	profesionalizace	k1gFnSc2
a	a	k8xC
suverénní	suverénní	k2eAgNnSc1d1
používání	používání	k1gNnSc1
gotického	gotický	k2eAgInSc2d1
slohu	sloh	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Není	být	k5eNaImIp3nS
náhodou	náhoda	k1gFnSc7
<g/>
,	,	kIx,
že	že	k8xS
právě	právě	k9
v	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
zřejmě	zřejmě	k6eAd1
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
prvnímu	první	k4xOgInSc3
rozsáhlejšímu	rozsáhlý	k2eAgInSc3d2
používání	používání	k1gNnSc3
stavebních	stavební	k2eAgInPc2d1
výkresů	výkres	k1gInPc2
a	a	k8xC
plánů	plán	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Saint-Denis	Saint-Denis	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc7
významnou	významný	k2eAgFnSc7d1
realizací	realizace	k1gFnSc7
je	být	k5eAaImIp3nS
dostavba	dostavba	k1gFnSc1
Saint-Denis	Saint-Denis	k1gFnSc2
<g/>
,	,	kIx,
probíhající	probíhající	k2eAgFnSc2d1
od	od	k7c2
roku	rok	k1gInSc2
1231	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Architekt	architekt	k1gMnSc1
citlivě	citlivě	k6eAd1
zachoval	zachovat	k5eAaPmAgMnS
část	část	k1gFnSc4
Sugerovy	Sugerův	k2eAgFnSc2d1
stavby	stavba	k1gFnSc2
<g/>
,	,	kIx,
ovšem	ovšem	k9
původní	původní	k2eAgFnSc4d1
románskou	románský	k2eAgFnSc4d1
baziliku	bazilika	k1gFnSc4
zcela	zcela	k6eAd1
nahradil	nahradit	k5eAaPmAgInS
novostavbou	novostavba	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
takřka	takřka	k6eAd1
učebnicovým	učebnicový	k2eAgInSc7d1
příkladem	příklad	k1gInSc7
gotiky	gotika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Štíhlé	štíhlý	k2eAgFnPc4d1
přípory	přípora	k1gFnPc4
směřují	směřovat	k5eAaImIp3nP
bez	bez	k7c2
přerušení	přerušení	k1gNnSc2
až	až	k9
do	do	k7c2
klenby	klenba	k1gFnSc2
<g/>
,	,	kIx,
zdivo	zdivo	k1gNnSc1
je	být	k5eAaImIp3nS
maximálně	maximálně	k6eAd1
potlačeno	potlačit	k5eAaPmNgNnS
<g/>
,	,	kIx,
interiér	interiér	k1gInSc4
prosvětlují	prosvětlovat	k5eAaImIp3nP
obrovitá	obrovitý	k2eAgNnPc1d1
okna	okno	k1gNnPc1
opatřená	opatřený	k2eAgFnSc1d1
kružbou	kružba	k1gFnSc7
a	a	k8xC
osazená	osazený	k2eAgFnSc1d1
vitrajemi	vitraje	k1gFnPc7
<g/>
,	,	kIx,
včetně	včetně	k7c2
obrovitých	obrovitý	k2eAgFnPc2d1
rozet	rozeta	k1gFnPc2
v	v	k7c6
osvětlujících	osvětlující	k2eAgInPc2d1
transept	transept	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Saint-Germain-en-Laye	Saint-Germain-en-Laye	k6eAd1
</s>
<s>
Jedním	jeden	k4xCgInSc7
z	z	k7c2
vrcholů	vrchol	k1gInPc2
katedrální	katedrální	k2eAgFnSc2d1
gotiky	gotika	k1gFnSc2
je	být	k5eAaImIp3nS
paradoxně	paradoxně	k6eAd1
poměrně	poměrně	k6eAd1
malá	malý	k2eAgFnSc1d1
kaple	kaple	k1gFnSc1
královského	královský	k2eAgInSc2d1
zámku	zámek	k1gInSc2
v	v	k7c6
Saint-Germain-en-Laye	Saint-Germain-en-Laye	k1gFnSc6
u	u	k7c2
Paříže	Paříž	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Došlo	dojít	k5eAaPmAgNnS
zde	zde	k6eAd1
k	k	k7c3
maximálnímu	maximální	k2eAgNnSc3d1
odhmotnění	odhmotnění	k1gNnSc3
a	a	k8xC
prosvětlení	prosvětlení	k1gNnSc3
prostoru	prostor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Interiér	interiér	k1gInSc1
je	být	k5eAaImIp3nS
opatřen	opatřit	k5eAaPmNgInS
celou	celý	k2eAgFnSc7d1
řadou	řada	k1gFnSc7
drobných	drobná	k1gFnPc2
<g/>
,	,	kIx,
rafinovaných	rafinovaný	k2eAgFnPc2d1
architektonických	architektonický	k2eAgFnPc2d1
hříček	hříčka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednou	jednou	k6eAd1
z	z	k7c2
nich	on	k3xPp3gInPc2
je	být	k5eAaImIp3nS
například	například	k6eAd1
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
klenba	klenba	k1gFnSc1
nepřiléhá	přiléhat	k5eNaImIp3nS
až	až	k9
ke	k	k7c3
stěně	stěna	k1gFnSc3
prolomené	prolomený	k2eAgFnSc2d1
rozměrnými	rozměrný	k2eAgNnPc7d1
okny	okno	k1gNnPc7
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
jsou	být	k5eAaImIp3nP
zde	zde	k6eAd1
použity	použít	k5eAaPmNgInP
jakési	jakýsi	k3yIgInPc1
„	„	k?
<g/>
kapsy	kapsa	k1gFnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
díky	díky	k7c3
nimž	jenž	k3xRgFnPc3
může	moct	k5eAaImIp3nS
mít	mít	k5eAaImF
okno	okno	k1gNnSc4
prakticky	prakticky	k6eAd1
obdélný	obdélný	k2eAgInSc1d1
tvar	tvar	k1gInSc1
<g/>
,	,	kIx,
do	do	k7c2
něhož	jenž	k3xRgInSc2
je	být	k5eAaImIp3nS
vepsána	vepsán	k2eAgFnSc1d1
nesmírně	smírně	k6eNd1
subtilní	subtilní	k2eAgFnSc1d1
kružba	kružba	k1gFnSc1
<g/>
,	,	kIx,
prolomené	prolomený	k2eAgFnPc1d1
a	a	k8xC
prosklená	prosklený	k2eAgFnSc1d1
i	i	k9
ve	v	k7c6
cviklech	cvikel	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celá	celý	k2eAgFnSc1d1
kaple	kaple	k1gFnSc1
tak	tak	k9
připomíná	připomínat	k5eAaImIp3nS
téměř	téměř	k6eAd1
klenotnickou	klenotnický	k2eAgFnSc4d1
práci	práce	k1gFnSc4
<g/>
,	,	kIx,
jakýsi	jakýsi	k3yIgInSc4
drobný	drobný	k2eAgInSc4d1
skvost	skvost	k1gInSc4
pro	pro	k7c4
architektonické	architektonický	k2eAgMnPc4d1
labužníky	labužník	k1gMnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Interiér	interiér	k1gInSc1
horní	horní	k2eAgFnSc2d1
kaple	kaple	k1gFnSc2
v	v	k7c6
Sainte-Chapelle	Sainte-Chapella	k1gFnSc6
</s>
<s>
Sainte	Saint	k1gMnSc5
Chapelle	Chapell	k1gMnSc5
</s>
<s>
Pro	pro	k7c4
nejposvátnější	posvátný	k2eAgFnSc4d3
relikvii	relikvie	k1gFnSc4
celého	celý	k2eAgNnSc2d1
křesťanství	křesťanství	k1gNnSc2
<g/>
,	,	kIx,
domnělou	domnělý	k2eAgFnSc4d1
Kristovu	Kristův	k2eAgFnSc4d1
trnovou	trnový	k2eAgFnSc4d1
korunu	koruna	k1gFnSc4
nechal	nechat	k5eAaPmAgMnS
Ludvík	Ludvík	k1gMnSc1
IX	IX	kA
<g/>
.	.	kIx.
vybudovat	vybudovat	k5eAaPmF
v	v	k7c6
letech	let	k1gInPc6
1242	#num#	k4
<g/>
–	–	k?
<g/>
1248	#num#	k4
na	na	k7c6
svém	svůj	k3xOyFgInSc6
pařížském	pařížský	k2eAgInSc6d1
hradě	hrad	k1gInSc6
dvoupatrovou	dvoupatrový	k2eAgFnSc4d1
„	„	k?
<g/>
svatou	svatá	k1gFnSc4
<g/>
“	“	k?
kapli	kaple	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
patří	patřit	k5eAaImIp3nS
k	k	k7c3
tomu	ten	k3xDgNnSc3
nejnádhernějšímu	nádherný	k2eAgNnSc3d3
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yInSc4,k3yQnSc4
nám	my	k3xPp1nPc3
středověk	středověk	k1gInSc1
zanechal	zanechat	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
prostor	prostor	k1gInSc1
dolní	dolní	k2eAgFnSc2d1
kaple	kaple	k1gFnSc2
je	být	k5eAaImIp3nS
pochopitelně	pochopitelně	k6eAd1
nízký	nízký	k2eAgInSc1d1
a	a	k8xC
temný	temný	k2eAgInSc1d1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
prostor	prostor	k1gInSc1
nad	nad	k7c7
ní	on	k3xPp3gFnSc7
umístěné	umístěný	k2eAgFnPc4d1
horní	horní	k2eAgFnPc4d1
kaple	kaple	k1gFnPc4
ztělesněním	ztělesnění	k1gNnSc7
všeho	všecek	k3xTgNnSc2
<g/>
,	,	kIx,
co	co	k8xS
přinesla	přinést	k5eAaPmAgFnS
rayonantní	rayonantní	k2eAgFnSc1d1
gotika	gotika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mohutný	mohutný	k2eAgInSc1d1
<g/>
,	,	kIx,
zlatem	zlato	k1gNnSc7
a	a	k8xC
světlem	světlo	k1gNnSc7
vitráží	vitráž	k1gFnPc2
svítící	svítící	k2eAgInSc4d1
prostor	prostor	k1gInSc4
prolomený	prolomený	k2eAgInSc4d1
obrovskými	obrovský	k2eAgNnPc7d1
okny	okno	k1gNnPc7
a	a	k8xC
opatřený	opatřený	k2eAgMnSc1d1
dokonale	dokonale	k6eAd1
formovanou	formovaný	k2eAgFnSc7d1
žebrovou	žebrový	k2eAgFnSc7d1
klenbou	klenba	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
místě	místo	k1gNnSc6
oltáře	oltář	k1gInSc2
pak	pak	k6eAd1
stojí	stát	k5eAaImIp3nS
vyvýšená	vyvýšený	k2eAgFnSc1d1
podklenutá	podklenutý	k2eAgFnSc1d1
konstrukce	konstrukce	k1gFnSc1
s	s	k7c7
dvojicí	dvojice	k1gFnSc7
šnekových	šnekový	k2eAgNnPc2d1
schodišť	schodiště	k1gNnPc2
po	po	k7c6
stranách	strana	k1gFnPc6
<g/>
,	,	kIx,
na	na	k7c6
níž	jenž	k3xRgFnSc6
stojí	stát	k5eAaImIp3nS
mohutný	mohutný	k2eAgInSc1d1
baldachýn	baldachýn	k1gInSc1
<g/>
,	,	kIx,
pod	pod	k7c7
kterým	který	k3yQgInSc7,k3yRgInSc7,k3yIgInSc7
byla	být	k5eAaImAgFnS
nejposvátnější	posvátný	k2eAgFnSc1d3
relikvie	relikvie	k1gFnSc1
křesťanství	křesťanství	k1gNnSc2
uložena	uložen	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Součástí	součást	k1gFnPc2
kaple	kaple	k1gFnSc1
pak	pak	k6eAd1
byla	být	k5eAaImAgFnS
také	také	k9
podklenutá	podklenutý	k2eAgFnSc1d1
vnější	vnější	k2eAgFnSc1d1
terasa	terasa	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
sloužila	sloužit	k5eAaImAgFnS
k	k	k7c3
ukazování	ukazování	k1gNnSc3
relikvie	relikvie	k1gFnSc2
při	při	k7c6
velikých	veliký	k2eAgInPc6d1
církevních	církevní	k2eAgInPc6d1
svátcích	svátek	k1gInPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Další	další	k2eAgFnPc1d1
stavby	stavba	k1gFnPc1
</s>
<s>
Další	další	k2eAgFnSc7d1
stavbou	stavba	k1gFnSc7
rayonantní	rayonantní	k2eAgFnSc2d1
gotiky	gotika	k1gFnSc2
je	být	k5eAaImIp3nS
např.	např.	kA
dokončení	dokončení	k1gNnSc4
katedrály	katedrála	k1gFnSc2
Notre-Dame	Notre-Dam	k1gInSc5
(	(	kIx(
<g/>
transept	transept	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Styl	styl	k1gInSc1
rayonantní	rayonantní	k2eAgFnSc2d1
gotiky	gotika	k1gFnSc2
nebyl	být	k5eNaImAgInS
ve	v	k7c6
Francii	Francie	k1gFnSc6
opuštěn	opustit	k5eAaPmNgInS
až	až	k9
do	do	k7c2
samého	samý	k3xTgInSc2
sklonku	sklonek	k1gInSc2
středověku	středověk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Respektuje	respektovat	k5eAaImIp3nS
jej	on	k3xPp3gMnSc4
i	i	k9
řada	řada	k1gFnSc1
vysloveně	vysloveně	k6eAd1
pozdně	pozdně	k6eAd1
gotických	gotický	k2eAgFnPc2d1
staveb	stavba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
Francii	Francie	k1gFnSc6
vývoj	vývoj	k1gInSc1
vyústil	vyústit	k5eAaPmAgInS
v	v	k7c4
pozdně	pozdně	k6eAd1
gotickou	gotický	k2eAgFnSc4d1
flamboyantní	flamboyantní	k2eAgFnSc4d1
(	(	kIx(
<g/>
plaménkovou	plaménkový	k2eAgFnSc4d1
<g/>
,	,	kIx,
též	též	k9
planoucí	planoucí	k2eAgFnSc4d1
<g/>
)	)	kIx)
gotiku	gotika	k1gFnSc4
(	(	kIx(
<g/>
1340	#num#	k4
<g/>
-	-	kIx~
<g/>
1520	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
přetrvala	přetrvat	k5eAaPmAgFnS
až	až	k6eAd1
do	do	k7c2
prvních	první	k4xOgNnPc2
desetiletí	desetiletí	k1gNnPc2
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Chór	chór	k1gInSc1
opatského	opatský	k2eAgInSc2d1
kostela	kostel	k1gInSc2
ve	v	k7c4
Vézelay	Vézelaa	k1gFnPc4
</s>
<s>
Gotika	gotika	k1gFnSc1
mimo	mimo	k7c4
Île-de-France	Île-de-France	k1gFnPc4
</s>
<s>
Gotika	gotika	k1gFnSc1
v	v	k7c6
jiných	jiný	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
Francie	Francie	k1gFnSc2
</s>
<s>
Přímo	přímo	k6eAd1
ve	v	k7c6
Francii	Francie	k1gFnSc6
<g/>
,	,	kIx,
například	například	k6eAd1
v	v	k7c6
Burgundsku	Burgundsko	k1gNnSc6
nebo	nebo	k8xC
v	v	k7c6
Champagni	Champageň	k1gFnSc6
vzniká	vznikat	k5eAaImIp3nS
řada	řada	k1gFnSc1
zajímavých	zajímavý	k2eAgFnPc2d1
staveb	stavba	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
jsou	být	k5eAaImIp3nP
odlišné	odlišný	k2eAgFnPc1d1
od	od	k7c2
způsobu	způsob	k1gInSc2
stavění	stavění	k1gNnSc2
v	v	k7c4
Île-de-France	Île-de-France	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
všechny	všechen	k3xTgMnPc4
jmenujme	jmenovat	k5eAaImRp1nP,k5eAaBmRp1nP
poměrně	poměrně	k6eAd1
malý	malý	k2eAgInSc4d1
<g/>
,	,	kIx,
ale	ale	k8xC
krásný	krásný	k2eAgInSc4d1
a	a	k8xC
rafinovaně	rafinovaně	k6eAd1
utvářený	utvářený	k2eAgInSc4d1
chór	chór	k1gInSc4
opatského	opatský	k2eAgInSc2d1
kostela	kostel	k1gInSc2
Sainte-Medeleine	Sainte-Medelein	k1gInSc5
ve	v	k7c4
Vézelay	Vézelay	k1gInPc4
z	z	k7c2
poslední	poslední	k2eAgFnSc2d1
třetiny	třetina	k1gFnSc2
dvanáctého	dvanáctý	k4xOgNnSc2
století	století	k1gNnSc2
<g/>
,	,	kIx,
katedrály	katedrála	k1gFnSc2
v	v	k7c4
Auxerre	Auxerr	k1gInSc5
či	či	k8xC
kostel	kostel	k1gInSc4
Notre-Dame	Notre-Dam	k1gInSc5
v	v	k7c6
Dijonu	Dijona	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Pozoruhodné	pozoruhodný	k2eAgFnPc1d1
gotické	gotický	k2eAgFnPc1d1
stavby	stavba	k1gFnPc1
vznikaly	vznikat	k5eAaImAgFnP
i	i	k9
v	v	k7c6
Normandii	Normandie	k1gFnSc6
<g/>
,	,	kIx,
například	například	k6eAd1
katedrály	katedrála	k1gFnSc2
v	v	k7c4
Coutances	Coutances	k1gInSc4
<g/>
,	,	kIx,
Bayeux	Bayeux	k1gInSc4
<g/>
,	,	kIx,
Rouenu	Rouena	k1gFnSc4
nebo	nebo	k8xC
opatství	opatství	k1gNnSc4
na	na	k7c4
Mont-Saint-Michel	Mont-Saint-Michel	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
některých	některý	k3yIgFnPc6
oblastech	oblast	k1gFnPc6
Francie	Francie	k1gFnSc2
<g/>
,	,	kIx,
především	především	k6eAd1
na	na	k7c6
jihu	jih	k1gInSc6
však	však	k9
gotická	gotický	k2eAgFnSc1d1
architektura	architektura	k1gFnSc1
téměř	téměř	k6eAd1
nezakotvila	zakotvit	k5eNaPmAgFnS
<g/>
,	,	kIx,
nebo	nebo	k8xC
je	být	k5eAaImIp3nS
výrazně	výrazně	k6eAd1
ovlivněna	ovlivnit	k5eAaPmNgFnS
místní	místní	k2eAgFnSc7d1
architekturou	architektura	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příkladem	příklad	k1gInSc7
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
například	například	k6eAd1
velmi	velmi	k6eAd1
zvláštní	zvláštní	k2eAgFnSc1d1
katedrála	katedrála	k1gFnSc1
v	v	k7c6
Albi	Alb	k1gFnSc6
(	(	kIx(
<g/>
asi	asi	k9
1287	#num#	k4
<g/>
–	–	k?
<g/>
1400	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
připomíná	připomínat	k5eAaImIp3nS
spíše	spíše	k9
pevnost	pevnost	k1gFnSc1
<g/>
,	,	kIx,
nežli	nežli	k8xS
odhmotněnou	odhmotněný	k2eAgFnSc4d1
<g/>
,	,	kIx,
prosvětlenou	prosvětlený	k2eAgFnSc4d1
a	a	k8xC
opěrným	opěrný	k2eAgInSc7d1
systémem	systém	k1gInSc7
opatřenou	opatřený	k2eAgFnSc4d1
vertikální	vertikální	k2eAgFnSc4d1
stavbu	stavba	k1gFnSc4
z	z	k7c2
oblasti	oblast	k1gFnSc2
Île-de-France	Île-de-France	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Itálie	Itálie	k1gFnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Gotická	gotický	k2eAgFnSc1d1
architektura	architektura	k1gFnSc1
v	v	k7c6
Itálii	Itálie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Itálie	Itálie	k1gFnSc1
gotiku	gotika	k1gFnSc4
<g/>
,	,	kIx,
v	v	k7c6
té	ten	k3xDgFnSc6
podobě	podoba	k1gFnSc6
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
známe	znát	k5eAaImIp1nP
z	z	k7c2
Île-de-France	Île-de-France	k1gFnSc2
<g/>
,	,	kIx,
nikdy	nikdy	k6eAd1
nepřijala	přijmout	k5eNaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Došlo	dojít	k5eAaPmAgNnS
zde	zde	k6eAd1
ovšem	ovšem	k9
k	k	k7c3
vytvoření	vytvoření	k1gNnSc3
její	její	k3xOp3gNnSc1
zcela	zcela	k6eAd1
specifické	specifický	k2eAgFnPc1d1
varianty	varianta	k1gFnPc1
<g/>
,	,	kIx,
kombinující	kombinující	k2eAgInSc1d1
románský	románský	k2eAgInSc1d1
a	a	k8xC
gotický	gotický	k2eAgInSc1d1
sloh	sloh	k1gInSc1
<g/>
,	,	kIx,
mnohdy	mnohdy	k6eAd1
se	s	k7c7
silnými	silný	k2eAgInPc7d1
vlivy	vliv	k1gInPc7
byzantské	byzantský	k2eAgFnSc2d1
či	či	k8xC
arabské	arabský	k2eAgFnSc2d1
architektury	architektura	k1gFnSc2
(	(	kIx(
<g/>
Benátky	Benátky	k1gFnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
byla	být	k5eAaImAgFnS
Itálie	Itálie	k1gFnSc1
ve	v	k7c6
středověku	středověk	k1gInSc6
rozdělena	rozdělit	k5eAaPmNgFnS
na	na	k7c6
množství	množství	k1gNnSc6
samostatných	samostatný	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
,	,	kIx,
je	on	k3xPp3gInPc4
její	její	k3xOp3gFnSc1
podoba	podoba	k1gFnSc1
velmi	velmi	k6eAd1
různorodá	různorodý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Gotika	gotika	k1gFnSc1
v	v	k7c6
Itálii	Itálie	k1gFnSc6
si	se	k3xPyFc3
téměř	téměř	k6eAd1
vůbec	vůbec	k9
nevšímá	všímat	k5eNaImIp3nS
konstrukce	konstrukce	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Opěrný	opěrný	k2eAgInSc4d1
systém	systém	k1gInSc4
<g/>
,	,	kIx,
katedrální	katedrální	k2eAgInSc4d1
rozvrh	rozvrh	k1gInSc4
a	a	k8xC
skeletovou	skeletový	k2eAgFnSc4d1
konstrukci	konstrukce	k1gFnSc4
bychom	by	kYmCp1nP
zde	zde	k6eAd1
téměř	téměř	k6eAd1
nenašli	najít	k5eNaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
je	být	k5eAaImIp3nS
italská	italský	k2eAgFnSc1d1
gotika	gotika	k1gFnSc1
často	často	k6eAd1
velice	velice	k6eAd1
dekorativní	dekorativní	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
patrné	patrný	k2eAgNnSc1d1
už	už	k6eAd1
u	u	k7c2
staveb	stavba	k1gFnPc2
<g/>
,	,	kIx,
jakými	jaký	k3yQgNnPc7,k3yRgNnPc7,k3yIgNnPc7
je	být	k5eAaImIp3nS
San	San	k1gMnSc1
Francesco	Francesco	k1gMnSc1
v	v	k7c6
Assisi	Assise	k1gFnSc6
(	(	kIx(
<g/>
1228	#num#	k4
<g/>
–	–	k?
<g/>
53	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Italské	italský	k2eAgFnPc4d1
stavby	stavba	k1gFnPc4
však	však	k9
za	za	k7c7
francouzskými	francouzský	k2eAgNnPc7d1
nijak	nijak	k6eAd1
nezaostávají	zaostávat	k5eNaImIp3nP
v	v	k7c6
honbě	honba	k1gFnSc6
za	za	k7c7
velikostí	velikost	k1gFnSc7
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
dokládá	dokládat	k5eAaImIp3nS
příklad	příklad	k1gInSc1
dómů	dóm	k1gInPc2
v	v	k7c6
Sieně	Siena	k1gFnSc6
(	(	kIx(
<g/>
13	#num#	k4
<g/>
.	.	kIx.
stol.	stol.	k?
<g/>
-1348	-1348	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Florencii	Florencie	k1gFnSc4
(	(	kIx(
<g/>
1292	#num#	k4
<g/>
–	–	k?
<g/>
1436	#num#	k4
<g/>
)	)	kIx)
i	i	k8xC
Miláně	Milán	k1gInSc6
(	(	kIx(
<g/>
1387	#num#	k4
<g/>
–	–	k?
<g/>
1813	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pozoruhodnými	pozoruhodný	k2eAgFnPc7d1
světskými	světský	k2eAgFnPc7d1
stavbami	stavba	k1gFnPc7
jsou	být	k5eAaImIp3nP
pak	pak	k6eAd1
především	především	k6eAd1
paláce	palác	k1gInSc2
v	v	k7c6
Benátkách	Benátky	k1gFnPc6
<g/>
,	,	kIx,
například	například	k6eAd1
Ca	ca	kA
d	d	k?
<g/>
'	'	kIx"
<g/>
Oro	Oro	k1gMnSc1
(	(	kIx(
<g/>
1421	#num#	k4
<g/>
–	–	k?
<g/>
36	#num#	k4
<g/>
)	)	kIx)
nebo	nebo	k8xC
Dóžecí	dóžecí	k2eAgInSc1d1
palác	palác	k1gInSc1
(	(	kIx(
<g/>
1424	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Katedrála	katedrála	k1gFnSc1
ve	v	k7c6
městě	město	k1gNnSc6
Burgos	Burgosa	k1gFnPc2
</s>
<s>
Pyrenejský	pyrenejský	k2eAgInSc1d1
poloostrov	poloostrov	k1gInSc1
(	(	kIx(
<g/>
Španělsko	Španělsko	k1gNnSc1
a	a	k8xC
Portugalsko	Portugalsko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Gotická	gotický	k2eAgFnSc1d1
architektura	architektura	k1gFnSc1
na	na	k7c6
Pyrenejském	pyrenejský	k2eAgInSc6d1
poloostrově	poloostrov	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Království	království	k1gNnSc1
na	na	k7c6
Pyrenejském	pyrenejský	k2eAgInSc6d1
poloostrově	poloostrov	k1gInSc6
měla	mít	k5eAaImAgFnS
ve	v	k7c6
středověku	středověk	k1gInSc6
k	k	k7c3
Francii	Francie	k1gFnSc3
poměrně	poměrně	k6eAd1
blízko	blízko	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Francie	Francie	k1gFnSc1
byla	být	k5eAaImAgFnS
totiž	totiž	k9
politickým	politický	k2eAgInSc7d1
i	i	k8xC
vojenským	vojenský	k2eAgMnSc7d1
spojencem	spojenec	k1gMnSc7
proti	proti	k7c3
Arabům	Arab	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
se	se	k3xPyFc4
odráží	odrážet	k5eAaImIp3nS
i	i	k9
na	na	k7c6
celé	celý	k2eAgFnSc6d1
řadě	řada	k1gFnSc6
gotických	gotický	k2eAgFnPc2d1
staveb	stavba	k1gFnPc2
<g/>
,	,	kIx,
i	i	k9
když	když	k8xS
i	i	k9
zde	zde	k6eAd1
docházelo	docházet	k5eAaImAgNnS
k	k	k7c3
určitým	určitý	k2eAgInPc3d1
posunům	posun	k1gInPc3
v	v	k7c6
tvarosloví	tvarosloví	k1gNnSc6
a	a	k8xC
stavebních	stavební	k2eAgFnPc6d1
zvyklostech	zvyklost	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zajímavou	zajímavý	k2eAgFnSc7d1
stavbou	stavba	k1gFnSc7
z	z	k7c2
dvanáctého	dvanáctý	k4xOgNnSc2
století	století	k1gNnSc2
je	být	k5eAaImIp3nS
katedrála	katedrála	k1gFnSc1
v	v	k7c6
Ávile	Ávilo	k1gNnSc6
či	či	k8xC
ohromný	ohromný	k2eAgInSc1d1
cisterciácký	cisterciácký	k2eAgInSc1d1
klášter	klášter	k1gInSc1
v	v	k7c6
Alcobaçe	Alcobaçe	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
třináctém	třináctý	k4xOgInSc6
století	století	k1gNnSc6
byly	být	k5eAaImAgFnP
založeny	založit	k5eAaPmNgFnP
katedrály	katedrála	k1gFnPc1
v	v	k7c4
Burgos	Burgos	k1gInSc4
<g/>
,	,	kIx,
v	v	k7c6
Toledu	Toledo	k1gNnSc6
a	a	k8xC
v	v	k7c6
Leónu	León	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vlivem	vlivem	k7c2
velké	velká	k1gFnSc2
rozdrobeností	rozdrobenost	k1gFnPc2
území	území	k1gNnSc2
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
architektura	architektura	k1gFnSc1
také	také	k9
velmi	velmi	k6eAd1
různorodá	různorodý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zcela	zcela	k6eAd1
specifickou	specifický	k2eAgFnSc4d1
podobu	podoba	k1gFnSc4
má	mít	k5eAaImIp3nS
například	například	k6eAd1
monumentální	monumentální	k2eAgFnSc1d1
vrcholně	vrcholně	k6eAd1
gotická	gotický	k2eAgFnSc1d1
architektura	architektura	k1gFnSc1
Katalánska	Katalánsko	k1gNnSc2
(	(	kIx(
<g/>
katedrála	katedrála	k1gFnSc1
v	v	k7c6
Barceloně	Barcelona	k1gFnSc6
<g/>
,	,	kIx,
kostel	kostel	k1gInSc4
Santa	Sant	k1gInSc2
Maria	Maria	k1gFnSc1
del	del	k?
Mar	Mar	k1gMnPc3
tamtéž	tamtéž	k6eAd1
<g/>
,	,	kIx,
katedrála	katedrála	k1gFnSc1
v	v	k7c6
Gironě	Giron	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zajímavá	zajímavý	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
i	i	k9
architektura	architektura	k1gFnSc1
Mallorky	Mallorka	k1gFnSc2
(	(	kIx(
<g/>
Katedrála	katedrála	k1gFnSc1
v	v	k7c6
Palma	palma	k1gFnSc1
de	de	k?
Mallorca	Mallorca	k1gMnSc1
<g/>
,	,	kIx,
Castell	Castell	k1gMnSc1
Bellver	Bellver	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
Andalusie	Andalusie	k1gFnSc1
(	(	kIx(
<g/>
katedrála	katedrála	k1gFnSc1
v	v	k7c6
Seville	Sevilla	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
se	se	k3xPyFc4
v	v	k7c6
architektuře	architektura	k1gFnSc6
paradoxně	paradoxně	k6eAd1
velice	velice	k6eAd1
silně	silně	k6eAd1
uplatnil	uplatnit	k5eAaPmAgInS
také	také	k9
arabský	arabský	k2eAgInSc1d1
vliv	vliv	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Portugalskou	portugalský	k2eAgFnSc4d1
vrcholnou	vrcholný	k2eAgFnSc4d1
gotiku	gotika	k1gFnSc4
reprezentuje	reprezentovat	k5eAaImIp3nS
klášter	klášter	k1gInSc1
Batalha	Batalh	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Pozdní	pozdní	k2eAgFnSc4d1
gotiku	gotika	k1gFnSc4
zastupuje	zastupovat	k5eAaImIp3nS
ve	v	k7c6
Španělsku	Španělsko	k1gNnSc6
tzv.	tzv.	kA
Hispano-vlámský	Hispano-vlámský	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
později	pozdě	k6eAd2
tzv.	tzv.	kA
Styl	styl	k1gInSc1
katolických	katolický	k2eAgNnPc2d1
veličenstev	veličenstvo	k1gNnPc2
(	(	kIx(
<g/>
Isabelský	Isabelský	k2eAgInSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
kromě	kromě	k7c2
řady	řada	k1gFnSc2
sakrálních	sakrální	k2eAgFnPc2d1
staveb	stavba	k1gFnPc2
reprezentují	reprezentovat	k5eAaImIp3nP
také	také	k9
kastilské	kastilský	k2eAgInPc4d1
zámky	zámek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pozoruhodné	pozoruhodný	k2eAgInPc1d1
jsou	být	k5eAaImIp3nP
i	i	k9
katedrály	katedrála	k1gFnPc1
ze	z	k7c2
šestnáctého	šestnáctý	k4xOgNnSc2
století	století	k1gNnSc2
(	(	kIx(
<g/>
Astorga	Astorga	k1gFnSc1
<g/>
,	,	kIx,
Salamanca	Salamanca	k1gFnSc1
<g/>
,	,	kIx,
Palencia	Palencia	k1gFnSc1
<g/>
,	,	kIx,
Segovia	Segovia	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Portugalsku	Portugalsko	k1gNnSc6
se	se	k3xPyFc4
uplatňuje	uplatňovat	k5eAaImIp3nS
tzv.	tzv.	kA
Manuelský	Manuelský	k2eAgInSc1d1
styl	styl	k1gInSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gMnSc7
hlavním	hlavní	k2eAgMnSc7d1
reprezentantem	reprezentant	k1gMnSc7
je	být	k5eAaImIp3nS
klášter	klášter	k1gInSc1
jeronymitů	jeronymit	k1gInPc2
v	v	k7c6
Belémě	Beléma	k1gFnSc6
na	na	k7c6
předměstí	předměstí	k1gNnSc6
Lisabonu	Lisabon	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Katedrála	katedrála	k1gFnSc1
v	v	k7c4
Salisbury	Salisbura	k1gFnPc4
</s>
<s>
Anglie	Anglie	k1gFnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Gotická	gotický	k2eAgFnSc1d1
architektura	architektura	k1gFnSc1
v	v	k7c6
Anglii	Anglie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Gotika	gotika	k1gFnSc1
na	na	k7c6
anglickém	anglický	k2eAgNnSc6d1
území	území	k1gNnSc6
získala	získat	k5eAaPmAgFnS
nesmírně	smírně	k6eNd1
zajímavou	zajímavý	k2eAgFnSc4d1
<g/>
,	,	kIx,
zcela	zcela	k6eAd1
suverénní	suverénní	k2eAgFnSc1d1
a	a	k8xC
velmi	velmi	k6eAd1
nezvyklou	zvyklý	k2eNgFnSc4d1
podobu	podoba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Částečně	částečně	k6eAd1
zřejmě	zřejmě	k6eAd1
proto	proto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
byla	být	k5eAaImAgFnS
Francie	Francie	k1gFnSc1
pro	pro	k7c4
angličany	angličan	k1gMnPc4
největším	veliký	k2eAgMnSc7d3
politickým	politický	k2eAgMnSc7d1
rivalem	rival	k1gMnSc7
<g/>
,	,	kIx,
nedošlo	dojít	k5eNaPmAgNnS
nikdy	nikdy	k6eAd1
ke	k	k7c3
kompletnímu	kompletní	k2eAgNnSc3d1
převzetí	převzetí	k1gNnSc3
gotické	gotický	k2eAgFnSc2d1
architektury	architektura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Angličané	Angličan	k1gMnPc1
vždy	vždy	k6eAd1
pouze	pouze	k6eAd1
postupně	postupně	k6eAd1
a	a	k8xC
opatrně	opatrně	k6eAd1
přijímali	přijímat	k5eAaImAgMnP
jednotlivé	jednotlivý	k2eAgInPc4d1
prvky	prvek	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
však	však	k9
vzápětí	vzápětí	k6eAd1
zapojili	zapojit	k5eAaPmAgMnP
do	do	k7c2
vlastní	vlastní	k2eAgFnSc2d1
architektonické	architektonický	k2eAgFnSc2d1
tradice	tradice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
i	i	k9
v	v	k7c6
Anglii	Anglie	k1gFnSc6
působila	působit	k5eAaImAgFnS
řada	řada	k1gFnSc1
francouzských	francouzský	k2eAgMnPc2d1
mistrů	mistr	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Anglická	anglický	k2eAgFnSc1d1
gotika	gotika	k1gFnSc1
se	se	k3xPyFc4
dělí	dělit	k5eAaImIp3nS
na	na	k7c4
tři	tři	k4xCgNnPc4
základní	základní	k2eAgNnPc4d1
období	období	k1gNnPc4
<g/>
,	,	kIx,
která	který	k3yQgNnPc1,k3yIgNnPc1,k3yRgNnPc1
jsou	být	k5eAaImIp3nP
zcela	zcela	k6eAd1
specifická	specifický	k2eAgNnPc1d1
a	a	k8xC
nemají	mít	k5eNaImIp3nP
obdobu	obdoba	k1gFnSc4
v	v	k7c6
jiných	jiný	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
<g/>
:	:	kIx,
</s>
<s>
Early	earl	k1gMnPc4
English	Englisha	k1gFnPc2
(	(	kIx(
<g/>
přibližně	přibližně	k6eAd1
1170	#num#	k4
<g/>
–	–	k?
<g/>
1240	#num#	k4
<g/>
)	)	kIx)
–	–	k?
Toto	tento	k3xDgNnSc1
období	období	k1gNnSc1
reprezentuje	reprezentovat	k5eAaImIp3nS
především	především	k9
přestavba	přestavba	k1gFnSc1
katedrály	katedrála	k1gFnSc2
v	v	k7c4
Canterbury	Canterbura	k1gFnPc4
<g/>
,	,	kIx,
na	na	k7c6
níž	jenž	k3xRgFnSc6
se	se	k3xPyFc4
podílel	podílet	k5eAaImAgMnS
francouzský	francouzský	k2eAgMnSc1d1
mistr	mistr	k1gMnSc1
Vilém	Vilém	k1gMnSc1
ze	z	k7c2
Sensu	Sens	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
architektura	architektura	k1gFnSc1
vychází	vycházet	k5eAaImIp3nS
především	především	k9
z	z	k7c2
architektury	architektura	k1gFnSc2
Saint-Denis	Saint-Denis	k1gFnSc2
a	a	k8xC
podobných	podobný	k2eAgFnPc2d1
raněgotických	raněgotický	k2eAgFnPc2d1
francouzských	francouzský	k2eAgFnPc2d1
staveb	stavba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsahuje	obsahovat	k5eAaImIp3nS
však	však	k9
také	také	k9
mnoho	mnoho	k4c4
prvků	prvek	k1gInPc2
starší	starý	k2eAgFnSc2d2
anglické	anglický	k2eAgFnSc2d1
tradice	tradice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalšími	další	k2eAgFnPc7d1
stavbami	stavba	k1gFnPc7
jsou	být	k5eAaImIp3nP
katedrály	katedrála	k1gFnPc4
v	v	k7c6
Lincolnu	Lincoln	k1gMnSc6
<g/>
,	,	kIx,
v	v	k7c6
Yorku	York	k1gInSc6
(	(	kIx(
<g/>
severní	severní	k2eAgInSc4d1
transept	transept	k1gInSc4
<g/>
,	,	kIx,
Salisbury	Salisbura	k1gFnPc4
či	či	k8xC
Wellsu	Wellsa	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Decorated	Decorated	k1gInSc1
(	(	kIx(
<g/>
přibližně	přibližně	k6eAd1
1240	#num#	k4
<g/>
–	–	k?
<g/>
1330	#num#	k4
<g/>
)	)	kIx)
–	–	k?
Obdob	obdoba	k1gFnPc2
začíná	začínat	k5eAaImIp3nS
novostavbou	novostavba	k1gFnSc7
Westminsterského	Westminsterský	k2eAgNnSc2d1
opatství	opatství	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
bylo	být	k5eAaImAgNnS
na	na	k7c4
královo	králův	k2eAgNnSc4d1
přání	přání	k1gNnSc4
budováno	budovat	k5eAaImNgNnS
jako	jako	k8xS,k8xC
výrazně	výrazně	k6eAd1
francouzsky	francouzsky	k6eAd1
formulovaná	formulovaný	k2eAgFnSc1d1
stavba	stavba	k1gFnSc1
<g/>
,	,	kIx,
opírající	opírající	k2eAgFnPc1d1
se	se	k3xPyFc4
hlavně	hlavně	k9
o	o	k7c4
podobu	podoba	k1gFnSc4
katedrály	katedrála	k1gFnSc2
v	v	k7c6
Remeši	Remeš	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reaguje	reagovat	k5eAaBmIp3nS
tak	tak	k6eAd1
už	už	k6eAd1
na	na	k7c4
první	první	k4xOgInPc4
projevy	projev	k1gInPc4
francouzské	francouzský	k2eAgFnSc2d1
rayonantní	rayonantní	k2eAgFnSc2d1
gotiky	gotika	k1gFnSc2
<g/>
,	,	kIx,
především	především	k9
na	na	k7c4
použití	použití	k1gNnSc4
kružeb	kružba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stavby	stavba	k1gFnSc2
tohoto	tento	k3xDgNnSc2
období	období	k1gNnSc2
jsou	být	k5eAaImIp3nP
skutečně	skutečně	k6eAd1
velmi	velmi	k6eAd1
dekorativní	dekorativní	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalšími	další	k2eAgFnPc7d1
stavbami	stavba	k1gFnPc7
jsou	být	k5eAaImIp3nP
katedrály	katedrála	k1gFnSc2
(	(	kIx(
<g/>
nebo	nebo	k8xC
jejich	jejich	k3xOp3gFnPc1
části	část	k1gFnPc1
<g/>
)	)	kIx)
v	v	k7c6
Yorku	York	k1gInSc6
<g/>
,	,	kIx,
Exeteru	Exeter	k1gInSc6
<g/>
,	,	kIx,
Bristolu	Bristol	k1gInSc2
<g/>
,	,	kIx,
Ely	Ela	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Perpendicular	Perpendicular	k1gInSc1
(	(	kIx(
<g/>
přibližně	přibližně	k6eAd1
1330	#num#	k4
<g/>
–	–	k?
<g/>
1530	#num#	k4
<g/>
)	)	kIx)
–	–	k?
Vychází	vycházet	k5eAaImIp3nS
také	také	k9
z	z	k7c2
rayonantní	rayonantní	k2eAgFnSc2d1
gotiky	gotika	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c6
mnoha	mnoho	k4c6
věcech	věc	k1gFnPc6
se	se	k3xPyFc4
od	od	k7c2
ní	on	k3xPp3gFnSc2
liší	lišit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
také	také	k9
velmi	velmi	k6eAd1
zdobná	zdobný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedním	jeden	k4xCgInSc7
ze	z	k7c2
základních	základní	k2eAgInPc2d1
prvků	prvek	k1gInPc2
je	být	k5eAaImIp3nS
tzv.	tzv.	kA
vějířová	vějířový	k2eAgFnSc1d1
klenba	klenba	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stěny	stěna	k1gFnPc1
a	a	k8xC
okna	okno	k1gNnPc1
jsou	být	k5eAaImIp3nP
pak	pak	k6eAd1
členěna	členit	k5eAaImNgFnS
pruty	prut	k1gInPc7
<g/>
,	,	kIx,
takže	takže	k8xS
tvoří	tvořit	k5eAaImIp3nP
jakousi	jakýsi	k3yIgFnSc4
paneláž	paneláž	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stavbami	stavba	k1gFnPc7
tohoto	tento	k3xDgNnSc2
období	období	k1gNnSc2
jsou	být	k5eAaImIp3nP
především	především	k9
King	King	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
College	College	k1gNnSc7
Chapel	Chapela	k1gFnPc2
v	v	k7c4
Cambridge	Cambridge	k1gFnPc4
a	a	k8xC
kaple	kaple	k1gFnPc1
Jindřicha	Jindřich	k1gMnSc2
VII	VII	kA
<g/>
.	.	kIx.
ve	v	k7c6
Westminsterském	Westminsterský	k2eAgNnSc6d1
opatství	opatství	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Radnice	radnice	k1gFnSc1
v	v	k7c6
Lovani	Lovaň	k1gFnSc6
</s>
<s>
Benelux	Benelux	k1gInSc1
a	a	k8xC
Německo	Německo	k1gNnSc1
</s>
<s>
Gotika	gotika	k1gFnSc1
v	v	k7c6
Nizozemí	Nizozemí	k1gNnSc6
je	být	k5eAaImIp3nS
zvláštní	zvláštní	k2eAgFnSc7d1
směsí	směs	k1gFnSc7
francouzských	francouzský	k2eAgInPc2d1
a	a	k8xC
anglických	anglický	k2eAgInPc2d1
vlivů	vliv	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Významnými	významný	k2eAgFnPc7d1
stavbami	stavba	k1gFnPc7
je	být	k5eAaImIp3nS
například	například	k6eAd1
kostely	kostel	k1gInPc4
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
v	v	k7c6
Antverpách	Antverpy	k1gFnPc6
<g/>
,	,	kIx,
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
v	v	k7c6
Halle	Hallo	k1gNnSc6
či	či	k8xC
kostel	kostel	k1gInSc1
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
v	v	k7c6
Bredě	Breda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Významnými	významný	k2eAgFnPc7d1
gotickými	gotický	k2eAgFnPc7d1
stavbami	stavba	k1gFnPc7
<g/>
,	,	kIx,
vyjadřujícími	vyjadřující	k2eAgInPc7d1
nebývalou	nebývalý	k2eAgFnSc4d1,k2eNgFnSc4d1
moc	moc	k1gFnSc4
a	a	k8xC
význam	význam	k1gInSc4
měst	město	k1gNnPc2
a	a	k8xC
jejich	jejich	k3xOp3gMnPc2
obyvatel	obyvatel	k1gMnPc2
v	v	k7c6
Nizozemí	Nizozemí	k1gNnSc6
vyjadřují	vyjadřovat	k5eAaImIp3nP
také	také	k9
stavby	stavba	k1gFnPc1
cechovních	cechovní	k2eAgInPc2d1
domů	dům	k1gInPc2
a	a	k8xC
radnic	radnice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příkladem	příklad	k1gInSc7
je	být	k5eAaImIp3nS
Soukenický	soukenický	k2eAgInSc1d1
dům	dům	k1gInSc1
v	v	k7c6
Bruggách	Bruggy	k1gFnPc6
<g/>
,	,	kIx,
radnice	radnice	k1gFnSc1
v	v	k7c6
Arrasu	Arras	k1gInSc6
<g/>
,	,	kIx,
Bruselu	Brusel	k1gInSc6
či	či	k8xC
Lovani	Lovaň	k1gFnSc6
<g/>
,	,	kIx,
až	až	k9
na	na	k7c4
poslední	poslední	k2eAgFnPc4d1
jsou	být	k5eAaImIp3nP
tyto	tento	k3xDgFnPc4
stavby	stavba	k1gFnPc4
(	(	kIx(
<g/>
podobně	podobně	k6eAd1
jako	jako	k8xC,k8xS
u	u	k7c2
holandských	holandský	k2eAgInPc2d1
kostelů	kostel	k1gInPc2
<g/>
)	)	kIx)
opatřené	opatřený	k2eAgNnSc1d1
mohutnou	mohutný	k2eAgFnSc7d1
<g/>
,	,	kIx,
převysokou	převysoký	k2eAgFnSc7d1
věží	věž	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Detail	detail	k1gInSc1
západního	západní	k2eAgNnSc2d1
průčelí	průčelí	k1gNnSc2
katedrály	katedrála	k1gFnSc2
ve	v	k7c6
Štrasburku	Štrasburk	k1gInSc6
</s>
<s>
Gotika	gotika	k1gFnSc1
v	v	k7c6
Německu	Německo	k1gNnSc6
a	a	k8xC
ostatních	ostatní	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
střední	střední	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
</s>
<s>
Německy	německy	k6eAd1
hovořící	hovořící	k2eAgFnPc1d1
země	zem	k1gFnPc1
<g/>
,	,	kIx,
kam	kam	k6eAd1
ovšem	ovšem	k9
v	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
prakticky	prakticky	k6eAd1
musíme	muset	k5eAaImIp1nP
počítat	počítat	k5eAaImF
i	i	k9
Čechy	Čech	k1gMnPc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
ve	v	k7c6
středověku	středověk	k1gInSc6
kulturně	kulturně	k6eAd1
patří	patřit	k5eAaImIp3nS
zcela	zcela	k6eAd1
do	do	k7c2
této	tento	k3xDgFnSc2
oblasti	oblast	k1gFnSc2
jsou	být	k5eAaImIp3nP
místem	místo	k1gNnSc7
<g/>
,	,	kIx,
kde	kde	k6eAd1
byla	být	k5eAaImAgFnS
gotika	gotika	k1gFnSc1
přejata	přejmout	k5eAaPmNgFnS
z	z	k7c2
Francie	Francie	k1gFnSc2
nejkomplexněji	komplexně	k6eAd3
a	a	k8xC
dočkala	dočkat	k5eAaPmAgFnS
se	se	k3xPyFc4
tu	tu	k6eAd1
(	(	kIx(
<g/>
vedle	vedle	k7c2
Anglie	Anglie	k1gFnSc2
<g/>
)	)	kIx)
nejširšího	široký	k2eAgNnSc2d3
rozšíření	rozšíření	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Počátky	počátek	k1gInPc4
</s>
<s>
Na	na	k7c6
samém	samý	k3xTgInSc6
počátku	počátek	k1gInSc6
zde	zde	k6eAd1
stojí	stát	k5eAaImIp3nS
chór	chór	k1gInSc1
katedrály	katedrála	k1gFnSc2
v	v	k7c6
Magdeburgu	Magdeburg	k1gInSc6
<g/>
,	,	kIx,
budovaný	budovaný	k2eAgInSc1d1
po	po	k7c6
roce	rok	k1gInSc6
1207	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přestože	přestože	k8xS
obsahuje	obsahovat	k5eAaImIp3nS
ještě	ještě	k9
množství	množství	k1gNnSc1
románských	románský	k2eAgInPc2d1
prvků	prvek	k1gInPc2
<g/>
,	,	kIx,
používá	používat	k5eAaImIp3nS
už	už	k6eAd1
také	také	k9
řadu	řada	k1gFnSc4
prvků	prvek	k1gInPc2
gotických	gotický	k2eAgInPc2d1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
s	s	k7c7
postupující	postupující	k2eAgFnSc7d1
výstavbou	výstavba	k1gFnSc7
románských	románský	k2eAgInPc2d1
prvků	prvek	k1gInPc2
ubývá	ubývat	k5eAaImIp3nS
ve	v	k7c4
prospěch	prospěch	k1gInSc4
prvků	prvek	k1gInPc2
gotických	gotický	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kostely	kostel	k1gInPc4
v	v	k7c6
Trevíru	Trevír	k1gInSc6
<g/>
,	,	kIx,
Marburgu	Marburg	k1gInSc6
či	či	k8xC
katedrály	katedrála	k1gFnSc2
v	v	k7c6
Metách	Mety	k1gFnPc6
a	a	k8xC
Štrasburku	Štrasburk	k1gInSc6
přejímají	přejímat	k5eAaImIp3nP
gotiku	gotika	k1gFnSc4
už	už	k9
značně	značně	k6eAd1
komplexněji	komplexně	k6eAd2
<g/>
.	.	kIx.
<g/>
Katedrála	katedrála	k1gFnSc1
v	v	k7c6
Kolíně	Kolín	k1gInSc6
nad	nad	k7c7
Rýnem	Rýn	k1gInSc7
na	na	k7c4
fotografii	fotografia	k1gFnSc4
z	z	k7c2
roku	rok	k1gInSc2
1900	#num#	k4
</s>
<s>
Kolín	Kolín	k1gInSc1
nad	nad	k7c7
Rýnem	Rýn	k1gInSc7
a	a	k8xC
další	další	k2eAgFnPc4d1
stavby	stavba	k1gFnPc4
v	v	k7c6
Porýní	Porýní	k1gNnSc6
</s>
<s>
Prvním	první	k4xOgMnSc7
skutečně	skutečně	k6eAd1
důsledně	důsledně	k6eAd1
gotickým	gotický	k2eAgInSc7d1
kostelem	kostel	k1gInSc7
a	a	k8xC
zároveň	zároveň	k6eAd1
stavbou	stavba	k1gFnSc7
<g/>
,	,	kIx,
jíž	jenž	k3xRgFnSc2
se	se	k3xPyFc4
německá	německý	k2eAgFnSc1d1
gotika	gotika	k1gFnSc1
vzepjala	vzepnout	k5eAaPmAgFnS
naprosto	naprosto	k6eAd1
mimořádnému	mimořádný	k2eAgInSc3d1
výkonu	výkon	k1gInSc3
je	být	k5eAaImIp3nS
katedrála	katedrála	k1gFnSc1
v	v	k7c6
Kolíně	Kolín	k1gInSc6
nad	nad	k7c7
Rýnem	Rýn	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejen	nejen	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
1248	#num#	k4
budovaný	budovaný	k2eAgInSc1d1
prosvětlený	prosvětlený	k2eAgInSc1d1
<g/>
,	,	kIx,
štíhlý	štíhlý	k2eAgInSc1d1
a	a	k8xC
vysoký	vysoký	k2eAgInSc1d1
chór	chór	k1gInSc1
<g/>
,	,	kIx,
z	z	k7c2
vnějšku	vnějšek	k1gInSc2
opatřený	opatřený	k2eAgInSc4d1
z	z	k7c2
dálky	dálka	k1gFnSc2
viditelným	viditelný	k2eAgInSc7d1
opěrným	opěrný	k2eAgInSc7d1
systémem	systém	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
velmi	velmi	k6eAd1
přesně	přesně	k6eAd1
navazuje	navazovat	k5eAaImIp3nS
na	na	k7c4
francouzské	francouzský	k2eAgInPc4d1
vzory	vzor	k1gInPc4
rayonantní	rayonantní	k2eAgFnSc2d1
gotiky	gotika	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
ostatní	ostatní	k2eAgFnPc1d1
části	část	k1gFnPc1
katedrály	katedrála	k1gFnSc2
<g/>
,	,	kIx,
především	především	k6eAd1
dvojice	dvojice	k1gFnSc1
obrovitých	obrovitý	k2eAgFnPc2d1
západních	západní	k2eAgFnPc2d1
věží	věž	k1gFnPc2
(	(	kIx(
<g/>
zahájeny	zahájen	k2eAgInPc1d1
kolem	kolem	k7c2
roku	rok	k1gInSc2
1350	#num#	k4
<g/>
,	,	kIx,
dobudovány	dobudován	k2eAgInPc4d1
až	až	k6eAd1
v	v	k7c6
devatenáctém	devatenáctý	k4xOgInSc6
století	století	k1gNnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
už	už	k6eAd1
je	být	k5eAaImIp3nS
výsledkem	výsledek	k1gInSc7
domácího	domácí	k2eAgInSc2d1
vývoje	vývoj	k1gInSc2
gotického	gotický	k2eAgInSc2d1
slohu	sloh	k1gInSc2
je	být	k5eAaImIp3nS
čímsi	cosi	k3yInSc7
naprosto	naprosto	k6eAd1
mimořádným	mimořádný	k2eAgInPc3d1
<g/>
.	.	kIx.
</s>
<s>
Významnými	významný	k2eAgInPc7d1
kostely	kostel	k1gInPc7
je	být	k5eAaImIp3nS
i	i	k9
kostel	kostel	k1gInSc1
ve	v	k7c6
Freiburgu	Freiburg	k1gInSc6
im	im	k?
Breisgau	Breisgaus	k1gInSc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc1
obrovitá	obrovitý	k2eAgFnSc1d1
věž	věž	k1gFnSc1
(	(	kIx(
<g/>
1250	#num#	k4
<g/>
–	–	k?
<g/>
1320	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s	s	k7c7
kružbami	kružba	k1gFnPc7
prolamovanou	prolamovaný	k2eAgFnSc7d1
<g/>
,	,	kIx,
45	#num#	k4
metrů	metr	k1gInPc2
vysokou	vysoký	k2eAgFnSc7d1
jehlancovou	jehlancový	k2eAgFnSc7d1
helmicí	helmice	k1gFnSc7
<g/>
,	,	kIx,
údajně	údajně	k6eAd1
„	„	k?
<g/>
nejkrásnější	krásný	k2eAgFnSc1d3
v	v	k7c6
celém	celý	k2eAgInSc6d1
křesťanském	křesťanský	k2eAgInSc6d1
světě	svět	k1gInSc6
<g/>
“	“	k?
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
opět	opět	k6eAd1
vskutku	vskutku	k9
neuvěřitelným	uvěřitelný	k2eNgInSc7d1
stavebním	stavební	k2eAgInSc7d1
výkonem	výkon	k1gInSc7
<g/>
,	,	kIx,
dotýkajícím	dotýkající	k2eAgMnSc7d1
se	se	k3xPyFc4
samotné	samotný	k2eAgFnPc1d1
hranice	hranice	k1gFnPc1
stavebních	stavební	k2eAgInPc2d1
materiálů	materiál	k1gInPc2
i	i	k8xC
použitých	použitý	k2eAgInPc2d1
konstrukčních	konstrukční	k2eAgInPc2d1
principů	princip	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Podobným	podobný	k2eAgInSc7d1
výkonem	výkon	k1gInSc7
je	být	k5eAaImIp3nS
i	i	k9
jemnou	jemný	k2eAgFnSc7d1
kružbovou	kružbový	k2eAgFnSc7d1
sítí	síť	k1gFnSc7
pokryté	pokrytý	k2eAgNnSc1d1
západní	západní	k2eAgNnSc1d1
průčelí	průčelí	k1gNnSc1
katedrály	katedrála	k1gFnSc2
ve	v	k7c6
Štrasburku	Štrasburk	k1gInSc6
<g/>
,	,	kIx,
zahájené	zahájený	k2eAgInPc4d1
roku	rok	k1gInSc2
1277	#num#	k4
patrně	patrně	k6eAd1
pod	pod	k7c7
vedením	vedení	k1gNnSc7
Erwina	Erwin	k1gMnSc2
von	von	k1gInSc4
Steinbach	Steinbach	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Petr	Petr	k1gMnSc1
Parléř	Parléř	k1gInSc1
–	–	k?
jižní	jižní	k2eAgInSc1d1
portál	portál	k1gInSc1
katedrály	katedrála	k1gFnSc2
</s>
<s>
Jindřich	Jindřich	k1gMnSc1
a	a	k8xC
Petr	Petr	k1gMnSc1
Parléřové	Parléřová	k1gFnSc2
</s>
<s>
Ve	v	k7c6
Švábském	švábský	k2eAgInSc6d1
Gmündu	Gmünd	k1gInSc6
působili	působit	k5eAaImAgMnP
otec	otec	k1gMnSc1
Jindřich	Jindřich	k1gMnSc1
Parléř	Parléř	k1gMnSc1
a	a	k8xC
jeho	jeho	k3xOp3gMnSc1
velmi	velmi	k6eAd1
mladý	mladý	k2eAgMnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
už	už	k9
velmi	velmi	k6eAd1
schopný	schopný	k2eAgMnSc1d1
syn	syn	k1gMnSc1
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mimořádným	mimořádný	k2eAgInPc3d1
a	a	k8xC
zřejmě	zřejmě	k6eAd1
společným	společný	k2eAgNnSc7d1
dílem	dílo	k1gNnSc7
je	být	k5eAaImIp3nS
Gmündský	Gmündský	k2eAgInSc1d1
kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Kříže	Kříž	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalším	další	k1gNnSc7
norimberský	norimberský	k2eAgInSc1d1
kostel	kostel	k1gInSc1
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Nejdůležitější	důležitý	k2eAgFnPc1d3
stavby	stavba	k1gFnPc1
ovšem	ovšem	k9
realizoval	realizovat	k5eAaBmAgMnS
Petr	Petr	k1gMnSc1
Parléř	Parléř	k1gMnSc1
v	v	k7c6
Čechách	Čechy	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
jimi	on	k3xPp3gInPc7
především	především	k9
katedrála	katedrála	k1gFnSc1
svatého	svatý	k2eAgMnSc2d1
Víta	Vít	k1gMnSc2
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc7
mladý	mladý	k2eAgMnSc1d1
stavitel	stavitel	k1gMnSc1
předvedl	předvést	k5eAaPmAgMnS
asi	asi	k9
nejvíce	hodně	k6eAd3,k6eAd1
své	svůj	k3xOyFgFnPc4
neobyčejné	obyčejný	k2eNgFnPc4d1
invence	invence	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Parléř	Parléř	k1gInSc1
na	na	k7c6
jedné	jeden	k4xCgFnSc6
straně	strana	k1gFnSc6
zcela	zcela	k6eAd1
integrálně	integrálně	k6eAd1
pokračuje	pokračovat	k5eAaImIp3nS
ve	v	k7c6
směru	směr	k1gInSc6
určeném	určený	k2eAgInSc6d1
francouzskou	francouzský	k2eAgFnSc7d1
rayonantní	rayonantní	k2eAgFnSc7d1
gotikou	gotika	k1gFnSc7
<g/>
,	,	kIx,
přitom	přitom	k6eAd1
ji	on	k3xPp3gFnSc4
však	však	k9
nesmírně	smírně	k6eNd1
inovativně	inovativně	k6eAd1
domýšlí	domýšlet	k5eAaImIp3nS
<g/>
,	,	kIx,
obohacuje	obohacovat	k5eAaImIp3nS
a	a	k8xC
v	v	k7c6
konečném	konečný	k2eAgInSc6d1
důsledku	důsledek	k1gInSc6
vlastně	vlastně	k9
také	také	k9
porušuje	porušovat	k5eAaImIp3nS
a	a	k8xC
destruuje	destruovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takový	takový	k3xDgInSc4
charakter	charakter	k1gInSc4
mají	mít	k5eAaImIp3nP
všechny	všechen	k3xTgInPc1
jeho	jeho	k3xOp3gInPc1
zásahy	zásah	k1gInPc1
na	na	k7c6
pražské	pražský	k2eAgFnSc6d1
katedrále	katedrála	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průběžná	průběžný	k2eAgFnSc1d1
síťová	síťový	k2eAgFnSc1d1
žebrová	žebrový	k2eAgFnSc1d1
klenba	klenba	k1gFnSc1
<g/>
,	,	kIx,
zcela	zcela	k6eAd1
originální	originální	k2eAgInSc4d1
<g/>
,	,	kIx,
dynamizující	dynamizující	k2eAgInSc4d1
způsob	způsob	k1gInSc4
formování	formování	k1gNnSc4
triforia	triforium	k1gNnSc2
<g/>
,	,	kIx,
způsobem	způsob	k1gInSc7
zaklenutí	zaklenutí	k1gNnSc2
sakristie	sakristie	k1gFnSc2
pomocí	pomocí	k7c2
klenby	klenba	k1gFnSc2
osazené	osazený	k2eAgInPc1d1
visutými	visutý	k2eAgInPc7d1
svorníky	svorník	k1gInPc7
či	či	k8xC
utvářením	utváření	k1gNnSc7
a	a	k8xC
zaklenutím	zaklenutí	k1gNnSc7
klenby	klenba	k1gFnSc2
kaple	kaple	k1gFnSc2
svatého	svatý	k2eAgMnSc2d1
Václava	Václav	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobným	podobný	k2eAgInSc7d1
paradoxním	paradoxní	k2eAgInSc7d1
dojmem	dojem	k1gInSc7
působí	působit	k5eAaImIp3nS
však	však	k9
také	také	k9
takřka	takřka	k6eAd1
neuvěřitelný	uvěřitelný	k2eNgInSc4d1
způsob	způsob	k1gInSc4
<g/>
,	,	kIx,
kterým	který	k3yQgMnPc3,k3yRgMnPc3,k3yIgMnPc3
Parléř	Parléř	k1gInSc1
vyřešil	vyřešit	k5eAaPmAgInS
jižní	jižní	k2eAgInSc1d1
portál	portál	k1gInSc1
katedrály	katedrála	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zmínit	zmínit	k5eAaPmF
je	on	k3xPp3gNnSc4
nutné	nutný	k2eAgNnSc4d1
i	i	k9
bohatě	bohatě	k6eAd1
dekorovaný	dekorovaný	k2eAgInSc1d1
opěrný	opěrný	k2eAgInSc1d1
systém	systém	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
mj.	mj.	kA
zdoben	zdobit	k5eAaImNgInS
panelací	panelací	k2eAgInSc1d1
se	s	k7c7
slepými	slepý	k2eAgFnPc7d1
kružbami	kružba	k1gFnPc7
a	a	k8xC
fiály	fiála	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
jakoby	jakoby	k8xS
„	„	k?
<g/>
prorážejí	prorážet	k5eAaImIp3nP
<g/>
“	“	k?
konstrukci	konstrukce	k1gFnSc3
vlastních	vlastní	k2eAgInPc2d1
opěráků	opěrák	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dalšími	další	k2eAgFnPc7d1
významnými	významný	k2eAgFnPc7d1
stavbami	stavba	k1gFnPc7
jsou	být	k5eAaImIp3nP
kostel	kostel	k1gInSc4
svatého	svatý	k2eAgMnSc2d1
Bartoloměje	Bartoloměj	k1gMnSc2
v	v	k7c6
Kolíně	Kolín	k1gInSc6
<g/>
,	,	kIx,
Staroměstská	staroměstský	k2eAgFnSc1d1
mostecká	mostecký	k2eAgFnSc1d1
věž	věž	k1gFnSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
či	či	k8xC
žel	žel	k9
požárem	požár	k1gInSc7
prakticky	prakticky	k6eAd1
zničená	zničený	k2eAgFnSc1d1
a	a	k8xC
přestavěná	přestavěný	k2eAgFnSc1d1
Kaple	kaple	k1gFnSc1
všech	všecek	k3xTgFnPc2
svatých	svatá	k1gFnPc2
na	na	k7c6
Pražském	pražský	k2eAgInSc6d1
hradě	hrad	k1gInSc6
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
zřejmě	zřejmě	k6eAd1
výrazně	výrazně	k6eAd1
podobala	podobat	k5eAaImAgFnS
pařížské	pařížský	k2eAgFnSc2d1
Sainte	Saint	k1gMnSc5
Chapelle	Chapell	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
katedrále	katedrála	k1gFnSc6
snad	snad	k9
působili	působit	k5eAaImAgMnP
i	i	k9
Parléřovi	Parléřův	k2eAgMnPc1d1
synové	syn	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předpokládá	předpokládat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
podíleli	podílet	k5eAaImAgMnP
i	i	k9
na	na	k7c6
některých	některý	k3yIgFnPc6
stavbách	stavba	k1gFnPc6
v	v	k7c6
Ulmu	Ulmum	k1gNnSc6
(	(	kIx(
<g/>
münster	münster	k1gMnSc1
<g/>
)	)	kIx)
či	či	k8xC
Bamberku	Bamberka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
vypuknutí	vypuknutí	k1gNnSc6
husitských	husitský	k2eAgFnPc2d1
válek	válka	k1gFnPc2
se	se	k3xPyFc4
žáci	žák	k1gMnPc1
Petra	Petr	k1gMnSc2
Parléře	Parléř	k1gMnSc2
z	z	k7c2
Prahy	Praha	k1gFnSc2
přesunuli	přesunout	k5eAaPmAgMnP
do	do	k7c2
Vídně	Vídeň	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
výrazně	výrazně	k6eAd1
podíleli	podílet	k5eAaImAgMnP
na	na	k7c6
dostavbě	dostavba	k1gFnSc6
katedrály	katedrála	k1gFnSc2
svatého	svatý	k2eAgMnSc2d1
Štěpána	Štěpán	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Interiér	interiér	k1gInSc1
mariánského	mariánský	k2eAgInSc2d1
kostela	kostel	k1gInSc2
v	v	k7c6
Mnichově	Mnichov	k1gInSc6
</s>
<s>
Arnold	Arnold	k1gMnSc1
Westfálský	Westfálský	k2eAgMnSc1d1
–	–	k?
zámek	zámek	k1gInSc1
Albrechtsburg	Albrechtsburg	k1gInSc1
v	v	k7c6
Míšni	Míšeň	k1gFnSc6
</s>
<s>
Středoevropské	středoevropský	k2eAgFnPc1d1
stavby	stavba	k1gFnPc1
pozdní	pozdní	k2eAgFnSc2d1
gotiky	gotika	k1gFnSc2
</s>
<s>
V	v	k7c6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
v	v	k7c6
Itálii	Itálie	k1gFnSc6
už	už	k6eAd1
dávno	dávno	k6eAd1
probíhala	probíhat	k5eAaImAgFnS
renesance	renesance	k1gFnPc4
měla	mít	k5eAaImAgFnS
architektura	architektura	k1gFnSc1
za	za	k7c7
Alpami	Alpy	k1gFnPc7
stále	stále	k6eAd1
ještě	ještě	k9
v	v	k7c6
podstatě	podstata	k1gFnSc6
gotickou	gotický	k2eAgFnSc4d1
podobu	podoba	k1gFnSc4
<g/>
,	,	kIx,
byť	byť	k8xS
někdy	někdy	k6eAd1
s	s	k7c7
izolovaně	izolovaně	k6eAd1
použitými	použitý	k2eAgInPc7d1
prvky	prvek	k1gInPc7
renesančními	renesanční	k2eAgInPc7d1
<g/>
.	.	kIx.
</s>
<s>
Velice	velice	k6eAd1
zajímavou	zajímavý	k2eAgFnSc4d1
podobu	podoba	k1gFnSc4
má	mít	k5eAaImIp3nS
pozdněgotická	pozdněgotický	k2eAgFnSc1d1
cihlová	cihlový	k2eAgFnSc1d1
architektura	architektura	k1gFnSc1
v	v	k7c6
Německu	Německo	k1gNnSc6
(	(	kIx(
<g/>
především	především	k6eAd1
severním	severní	k2eAgMnPc3d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
postupně	postupně	k6eAd1
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
tak	tak	k9
výrazné	výrazný	k2eAgFnSc3d1
minimalizaci	minimalizace	k1gFnSc3
tvarosloví	tvarosloví	k1gNnSc2
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
téměř	téměř	k6eAd1
podobají	podobat	k5eAaImIp3nP
některým	některý	k3yIgFnPc3
puristickým	puristický	k2eAgFnPc3d1
stavbám	stavba	k1gFnPc3
z	z	k7c2
počátku	počátek	k1gInSc2
dvacátého	dvacátý	k4xOgNnSc2
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
nejmarkantněji	markantně	k6eAd3
případ	případ	k1gInSc4
kostela	kostel	k1gInSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
v	v	k7c6
Stralsundu	Stralsund	k1gInSc6
<g/>
,	,	kIx,
ovšem	ovšem	k9
velmi	velmi	k6eAd1
minimalistický	minimalistický	k2eAgInSc1d1
je	být	k5eAaImIp3nS
i	i	k9
cihlový	cihlový	k2eAgInSc4d1
mariánský	mariánský	k2eAgInSc4d1
kostel	kostel	k1gInSc4
v	v	k7c6
Mnichově	Mnichov	k1gInSc6
od	od	k7c2
Jörga	Jörg	k1gMnSc2
von	von	k1gInSc4
Halspach	Halspacha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Jiné	jiný	k2eAgFnPc1d1
cihlové	cihlový	k2eAgFnPc1d1
stavby	stavba	k1gFnPc1
sice	sice	k8xC
používají	používat	k5eAaImIp3nP
větší	veliký	k2eAgNnSc4d2
množství	množství	k1gNnSc4
dekoru	dekor	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
nakládají	nakládat	k5eAaImIp3nP
s	s	k7c7
ním	on	k3xPp3gInSc7
přesto	přesto	k8xC
velmi	velmi	k6eAd1
neortodoxně	ortodoxně	k6eNd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někdy	někdy	k6eAd1
tyto	tento	k3xDgFnPc1
stavby	stavba	k1gFnPc1
dokonce	dokonce	k9
připomenou	připomenout	k5eAaPmIp3nP
sececní	sececní	k2eAgFnSc4d1
cihlovou	cihlový	k2eAgFnSc4d1
architekturu	architektura	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Totéž	týž	k3xTgNnSc4
se	se	k3xPyFc4
však	však	k9
děje	dít	k5eAaImIp3nS
i	i	k9
v	v	k7c6
kameni	kámen	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stačí	stačit	k5eAaBmIp3nS
zmínit	zmínit	k5eAaPmF
kroucené	kroucený	k2eAgInPc4d1
sloupy	sloup	k1gInPc4
severní	severní	k2eAgFnSc2d1
boční	boční	k2eAgFnSc2d1
lodi	loď	k1gFnSc2
katedrály	katedrála	k1gFnSc2
v	v	k7c6
Braunschweigu	Braunschweig	k1gInSc6
či	či	k8xC
neuvěřitelné	uvěřitelný	k2eNgNnSc1d1
dvojité	dvojitý	k2eAgNnSc1d1
šnekové	šnekový	k2eAgNnSc1d1
schodiště	schodiště	k1gNnSc1
na	na	k7c6
hradě	hrad	k1gInSc6
ve	v	k7c6
Štýrském	štýrský	k2eAgInSc6d1
Hradci	Hradec	k1gInSc6
z	z	k7c2
doky	dok	k1gInPc1
kolem	kolem	k7c2
roku	rok	k1gInSc2
1500	#num#	k4
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
takřka	takřka	k6eAd1
připomene	připomenout	k5eAaPmIp3nS
například	například	k6eAd1
tvorbu	tvorba	k1gFnSc4
Antoni	Antoň	k1gFnSc3
Gaudího	Gaudí	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
tvorby	tvorba	k1gFnSc2
mnoha	mnoho	k4c2
velmi	velmi	k6eAd1
zajímavých	zajímavý	k2eAgMnPc2d1
stavitelů	stavitel	k1gMnPc2
zmiňme	zminout	k5eAaPmRp1nP
ještě	ještě	k9
dva	dva	k4xCgMnPc4
<g/>
:	:	kIx,
prvním	první	k4xOgMnSc6
je	být	k5eAaImIp3nS
Arnold	Arnold	k1gMnSc1
von	von	k1gInSc4
Westfalen	Westfalen	k2eAgInSc4d1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc1
zámek	zámek	k1gInSc1
v	v	k7c6
Míšni	Míšeň	k1gFnSc6
tzv.	tzv.	kA
Albrechtsburg	Albrechtsburg	k1gInSc1
(	(	kIx(
<g/>
po	po	k7c6
roce	rok	k1gInSc6
<g/>
.	.	kIx.
1470	#num#	k4
<g/>
)	)	kIx)
dodnes	dodnes	k6eAd1
udivuje	udivovat	k5eAaImIp3nS
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
kolika	kolika	k1gFnSc1
nejrůznějších	různý	k2eAgFnPc2d3
variací	variace	k1gFnPc2
je	být	k5eAaImIp3nS
schopno	schopen	k2eAgNnSc1d1
gotické	gotický	k2eAgNnSc1d1
tvarosloví	tvarosloví	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kamenné	kamenný	k2eAgInPc1d1
prvky	prvek	k1gInPc1
jsou	být	k5eAaImIp3nP
stáčeny	stáčen	k2eAgInPc1d1
a	a	k8xC
křiveny	křiven	k2eAgInPc1d1
<g/>
,	,	kIx,
místnosti	místnost	k1gFnPc1
jsou	být	k5eAaImIp3nP
zaklenuty	zaklenout	k5eAaPmNgFnP
ohromnými	ohromný	k2eAgFnPc7d1
bezžebernými	bezžeberný	k2eAgFnPc7d1
sklípkovými	sklípkový	k2eAgFnPc7d1
klenbami	klenba	k1gFnPc7
<g/>
,	,	kIx,
kamenné	kamenný	k2eAgInPc1d1
pruty	prut	k1gInPc1
ostění	ostění	k1gNnSc2
se	se	k3xPyFc4
vzájemně	vzájemně	k6eAd1
kříží	křížit	k5eAaImIp3nP
a	a	k8xC
přesekávají	přesekávat	k5eAaImIp3nP
<g/>
,	,	kIx,
vrchní	vrchní	k2eAgFnSc1d1
část	část	k1gFnSc1
oken	okno	k1gNnPc2
je	být	k5eAaImIp3nS
formována	formovat	k5eAaImNgFnS
do	do	k7c2
zvláštního	zvláštní	k2eAgNnSc2d1
„	„	k?
<g/>
záclonového	záclonový	k2eAgInSc2d1
<g/>
“	“	k?
tvaru	tvar	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
podobném	podobný	k2eAgMnSc6d1
duchu	duch	k1gMnSc6
se	se	k3xPyFc4
nese	nést	k5eAaImIp3nS
i	i	k9
tvorba	tvorba	k1gFnSc1
Benedikta	Benedikt	k1gMnSc2
Rieda	Ried	k1gMnSc2
<g/>
,	,	kIx,
jihoněmeckého	jihoněmecký	k2eAgMnSc2d1
architekta	architekt	k1gMnSc2
působícího	působící	k2eAgMnSc2d1
v	v	k7c6
Čechách	Čechy	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc7
nejvýznamnější	významný	k2eAgFnSc7d3
realizací	realizace	k1gFnSc7
je	být	k5eAaImIp3nS
tzv.	tzv.	kA
Vladislavský	vladislavský	k2eAgInSc1d1
sál	sál	k1gInSc1
(	(	kIx(
<g/>
1495	#num#	k4
<g/>
–	–	k?
<g/>
1515	#num#	k4
<g/>
)	)	kIx)
s	s	k7c7
jezdeckými	jezdecký	k2eAgInPc7d1
schody	schod	k1gInPc7
a	a	k8xC
také	také	k9
s	s	k7c7
přilehlým	přilehlý	k2eAgNnSc7d1
Ludvíkovým	Ludvíkův	k2eAgNnSc7d1
křídlem	křídlo	k1gNnSc7
na	na	k7c6
Pražském	pražský	k2eAgInSc6d1
Hradě	hrad	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základem	základ	k1gInSc7
Riedovy	Riedův	k2eAgFnSc2d1
tvorby	tvorba	k1gFnSc2
jsou	být	k5eAaImIp3nP
síťové	síťový	k2eAgFnPc4d1
klenby	klenba	k1gFnPc4
zaklenuté	zaklenutý	k2eAgFnPc4d1
pomocí	pomocí	k7c2
stáčených	stáčený	k2eAgFnPc2d1
žeber	žebro	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalšími	další	k1gNnPc7
například	například	k6eAd1
palác	palác	k1gInSc4
na	na	k7c6
hradě	hrad	k1gInSc6
v	v	k7c6
Blatné	blatný	k2eAgFnSc6d1
či	či	k8xC
dostavba	dostavba	k1gFnSc1
chrámu	chrám	k1gInSc2
svaté	svatý	k2eAgFnSc2d1
Barbory	Barbora	k1gFnSc2
v	v	k7c6
Kutné	kutný	k2eAgFnSc6d1
Hoře	hora	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Významné	významný	k2eAgFnPc1d1
gotické	gotický	k2eAgFnPc1d1
stavby	stavba	k1gFnPc1
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Gotická	gotický	k2eAgFnSc1d1
architektura	architektura	k1gFnSc1
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Katedrála	katedrála	k1gFnSc1
svatého	svatý	k2eAgMnSc2d1
Víta	Vít	k1gMnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
(	(	kIx(
<g/>
pol.	pol.	k?
14	#num#	k4
<g/>
.	.	kIx.
stol.	stol.	k?
–	–	k?
1929	#num#	k4
<g/>
)	)	kIx)
–	–	k?
Čechy	Čech	k1gMnPc4
</s>
<s>
Katedrála	katedrála	k1gFnSc1
svaté	svatý	k2eAgFnSc2d1
Barbory	Barbora	k1gFnSc2
v	v	k7c6
Kutné	kutný	k2eAgFnSc6d1
Hoře	hora	k1gFnSc6
(	(	kIx(
<g/>
konec	konec	k1gInSc1
14	#num#	k4
<g/>
.	.	kIx.
stol.	stol.	k?
<g/>
)	)	kIx)
–	–	k?
Čechy	Čech	k1gMnPc4
</s>
<s>
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Bartoloměje	Bartoloměj	k1gMnSc2
v	v	k7c6
Kolíně	Kolín	k1gInSc6
(	(	kIx(
<g/>
konec	konec	k1gInSc1
13	#num#	k4
<g/>
.	.	kIx.
stol.	stol.	k?
–	–	k?
14	#num#	k4
<g/>
.	.	kIx.
stol.	stol.	k?
<g/>
;	;	kIx,
Parléřův	Parléřův	k2eAgInSc1d1
vysoký	vysoký	k2eAgInSc1d1
chór	chór	k1gInSc1
katedrálního	katedrální	k2eAgInSc2d1
charakteru	charakter	k1gInSc2
s	s	k7c7
ochozem	ochoz	k1gInSc7
a	a	k8xC
věncem	věnec	k1gInSc7
kaplí	kaple	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
Katedrála	katedrála	k1gFnSc1
svatého	svatý	k2eAgMnSc2d1
Bartoloměje	Bartoloměj	k1gMnSc2
v	v	k7c6
Plzni	Plzeň	k1gFnSc6
(	(	kIx(
<g/>
konec	konec	k1gInSc1
14	#num#	k4
<g/>
.	.	kIx.
stol.	stol.	k?
<g/>
)	)	kIx)
–	–	k?
Čechy	Čech	k1gMnPc4
</s>
<s>
Hrad	hrad	k1gInSc1
Bezděz	Bezděz	k1gInSc1
(	(	kIx(
<g/>
1265	#num#	k4
<g/>
–	–	k?
<g/>
1278	#num#	k4
<g/>
)	)	kIx)
–	–	k?
Čechy	Čech	k1gMnPc4
</s>
<s>
Hrad	hrad	k1gInSc1
Bítov	Bítov	k1gInSc1
(	(	kIx(
<g/>
13	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
přestavěn	přestavěn	k2eAgInSc1d1
zač	zač	k6eAd1
<g/>
.	.	kIx.
14	#num#	k4
<g/>
.	.	kIx.
stol.	stol.	k?
<g/>
)	)	kIx)
–	–	k?
Morava	Morava	k1gFnSc1
</s>
<s>
Staroměstská	staroměstský	k2eAgFnSc1d1
mostecká	mostecký	k2eAgFnSc1d1
věž	věž	k1gFnSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
(	(	kIx(
<g/>
1380	#num#	k4
<g/>
)	)	kIx)
–	–	k?
Čechy	Čech	k1gMnPc4
</s>
<s>
Klášter	klášter	k1gInSc1
Porta	porta	k1gFnSc1
coeli	coele	k1gFnSc4
v	v	k7c6
Předklášteří	Předklášteří	k1gNnSc6
u	u	k7c2
Tišnova	Tišnov	k1gInSc2
(	(	kIx(
<g/>
asi	asi	k9
1230	#num#	k4
nebo	nebo	k8xC
1233	#num#	k4
<g/>
)	)	kIx)
–	–	k?
Morava	Morava	k1gFnSc1
</s>
<s>
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Jakuba	Jakub	k1gMnSc2
v	v	k7c6
Brně	Brno	k1gNnSc6
</s>
<s>
Bazilika	bazilika	k1gFnSc1
Nanebevzetí	nanebevzetí	k1gNnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
v	v	k7c6
Brně	Brno	k1gNnSc6
</s>
<s>
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Mořice	Mořic	k1gMnSc2
v	v	k7c6
Olomouci	Olomouc	k1gFnSc6
</s>
<s>
Kostel	kostel	k1gInSc1
Nanebevzetí	nanebevzetí	k1gNnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
v	v	k7c6
Mostě	most	k1gInSc6
</s>
<s>
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Mikuláše	Mikuláš	k1gMnSc2
v	v	k7c6
Lounech	Louny	k1gInPc6
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Klein	Klein	k1gMnSc1
<g/>
,	,	kIx,
B.	B.	kA
<g/>
:	:	kIx,
Počátky	počátek	k1gInPc4
a	a	k8xC
formování	formování	k1gNnSc4
gotické	gotický	k2eAgFnSc2d1
architektury	architektura	k1gFnSc2
ve	v	k7c6
Francii	Francie	k1gFnSc6
a	a	k8xC
v	v	k7c6
sousedních	sousední	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
<g/>
,	,	kIx,
in	in	k?
<g/>
:	:	kIx,
Gotika	gotika	k1gFnSc1
<g/>
,	,	kIx,
Slovart	Slovart	k1gInSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2000	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
28	#num#	k4
<g/>
–	–	k?
<g/>
30	#num#	k4
<g/>
↑	↑	k?
Borngäserová	Borngäserová	k1gFnSc1
<g/>
,	,	kIx,
B.	B.	kA
<g/>
:	:	kIx,
Gotická	gotický	k2eAgFnSc1d1
architektura	architektura	k1gFnSc1
v	v	k7c6
Itálii	Itálie	k1gFnSc6
<g />
.	.	kIx.
</s>
<s hack="1">
in	in	k?
<g/>
:	:	kIx,
Gotika	gotika	k1gFnSc1
<g/>
,	,	kIx,
Slovart	Slovart	k1gInSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2000	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
242	#num#	k4
<g/>
↑	↑	k?
Skutky	skutek	k1gInPc1
opata	opat	k1gMnSc2
Sugera	Suger	k1gMnSc2
<g/>
,	,	kIx,
texty	text	k1gInPc1
opata	opat	k1gMnSc2
Sugera	Suger	k1gMnSc2
ze	z	k7c2
Saint-Denis	Saint-Denis	k1gFnSc2
<g/>
,	,	kIx,
Bernarda	Bernard	k1gMnSc4
z	z	k7c2
Clairvaux	Clairvaux	k1gInSc4
a	a	k8xC
Viléma	Vilém	k1gMnSc4
ze	z	k7c2
Saint-Denis	Saint-Denis	k1gFnSc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2003	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Klein	Klein	k1gMnSc1
<g/>
,	,	kIx,
B.	B.	kA
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
s.	s.	k?
28	#num#	k4
<g/>
–	–	k?
<g/>
29	#num#	k4
<g/>
↑	↑	k?
Klein	Klein	k1gMnSc1
<g/>
,	,	kIx,
B.	B.	kA
<g/>
,	,	kIx,
s.	s.	k?
34	#num#	k4
<g/>
–	–	k?
<g/>
35	#num#	k4
<g/>
↑	↑	k?
Klein	Klein	k1gMnSc1
<g/>
,	,	kIx,
B.	B.	kA
<g/>
,	,	kIx,
s.	s.	k?
41	#num#	k4
<g/>
–	–	k?
<g/>
44	#num#	k4
<g/>
↑	↑	k?
Klein	Klein	k1gMnSc1
<g/>
,	,	kIx,
B.	B.	kA
<g/>
,	,	kIx,
s.	s.	k?
44	#num#	k4
<g/>
–	–	k?
<g/>
47	#num#	k4
<g/>
↑	↑	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Klein	Klein	k1gMnSc1
<g/>
,	,	kIx,
B.	B.	kA
<g/>
,	,	kIx,
s.	s.	k?
48	#num#	k4
<g/>
–	–	k?
<g/>
54	#num#	k4
<g/>
↑	↑	k?
Klein	Klein	k1gMnSc1
<g/>
,	,	kIx,
B.	B.	kA
<g/>
,	,	kIx,
s.	s.	k?
59	#num#	k4
<g/>
–	–	k?
<g/>
63	#num#	k4
<g/>
↑	↑	k?
Klein	Klein	k1gMnSc1
<g/>
,	,	kIx,
B.	B.	kA
<g/>
,	,	kIx,
s.	s.	k?
80	#num#	k4
<g/>
↑	↑	k?
Klein	Klein	k1gMnSc1
<g/>
,	,	kIx,
B.	B.	kA
<g/>
,	,	kIx,
s.	s.	k?
81	#num#	k4
<g/>
–	–	k?
<g/>
82	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
↑	↑	k?
Klein	Klein	k1gMnSc1
<g/>
,	,	kIx,
B.	B.	kA
<g/>
,	,	kIx,
s.	s.	k?
82	#num#	k4
<g/>
–	–	k?
<g/>
83	#num#	k4
<g/>
↑	↑	k?
Klein	Klein	k1gMnSc1
<g/>
,	,	kIx,
B.	B.	kA
<g/>
,	,	kIx,
s.	s.	k?
83	#num#	k4
<g/>
–	–	k?
<g/>
87	#num#	k4
<g/>
↑	↑	k?
Borngäserová	Borngäserová	k1gFnSc1
<g/>
,	,	kIx,
B.	B.	kA
<g/>
,	,	kIx,
s.	s.	k?
242	#num#	k4
<g/>
–	–	k?
<g/>
260	#num#	k4
<g/>
↑	↑	k?
Klein	Klein	k1gMnSc1
<g/>
,	,	kIx,
B.	B.	kA
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
s.	s.	k?
96	#num#	k4
<g/>
–	–	k?
<g/>
103	#num#	k4
<g/>
↑	↑	k?
Borngäserová	Borngäserová	k1gFnSc1
<g/>
,	,	kIx,
B.	B.	kA
<g/>
:	:	kIx,
Architektura	architektura	k1gFnSc1
pozdní	pozdní	k2eAgFnSc2d1
gotiky	gotika	k1gFnSc2
ve	v	k7c6
Španělsku	Španělsko	k1gNnSc6
a	a	k8xC
Portugalsku	Portugalsko	k1gNnSc6
in	in	k?
<g/>
:	:	kIx,
Gotika	gotika	k1gFnSc1
<g/>
,	,	kIx,
Slovart	Slovart	k1gInSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2000	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
266	#num#	k4
<g/>
–	–	k?
<g/>
299	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Engelová	Engelová	k1gFnSc1
<g/>
,	,	kIx,
U.	U.	kA
Gotická	gotický	k2eAgFnSc1d1
architektura	architektura	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
v	v	k7c6
Anglii	Anglie	k1gFnSc6
<g/>
,	,	kIx,
in	in	k?
<g/>
:	:	kIx,
Gotika	gotika	k1gFnSc1
<g/>
,	,	kIx,
Slovart	Slovart	k1gInSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2000	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
118	#num#	k4
<g/>
–	–	k?
<g/>
121	#num#	k4
<g/>
↑	↑	k?
Riestra	Riestr	k1gMnSc4
<g/>
,	,	kIx,
P.	P.	kA
Gotická	gotický	k2eAgFnSc1d1
architektura	architektura	k1gFnSc1
v	v	k7c6
„	„	k?
<g/>
Německých	německý	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
<g/>
“	“	k?
<g/>
,	,	kIx,
in	in	k?
<g/>
:	:	kIx,
Gotika	gotika	k1gFnSc1
<g/>
,	,	kIx,
Slovart	Slovart	k1gInSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2000	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
200	#num#	k4
<g/>
–	–	k?
<g/>
202	#num#	k4
<g/>
↑	↑	k?
Riestra	Riestr	k1gMnSc4
<g/>
,	,	kIx,
P.	P.	kA
<g/>
,	,	kIx,
s.	s.	k?
206	#num#	k4
<g/>
–	–	k?
<g/>
211	#num#	k4
<g/>
↑	↑	k?
Riestra	Riestr	k1gMnSc4
<g/>
,	,	kIx,
P.	P.	kA
<g/>
,	,	kIx,
s.	s.	k?
228	#num#	k4
<g/>
–	–	k?
<g/>
230	#num#	k4
<g/>
↑	↑	k?
Riestra	Riestr	k1gMnSc4
<g/>
,	,	kIx,
P.	P.	kA
<g/>
,	,	kIx,
s.	s.	k?
234	#num#	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Skutky	skutek	k1gInPc1
opata	opat	k1gMnSc2
Sugera	Suger	k1gMnSc2
<g/>
,	,	kIx,
texty	text	k1gInPc1
opata	opat	k1gMnSc2
Sugera	Suger	k1gMnSc2
ze	z	k7c2
Saint-Denis	Saint-Denis	k1gFnSc2
<g/>
,	,	kIx,
Bernarda	Bernard	k1gMnSc4
z	z	k7c2
Clairvaux	Clairvaux	k1gInSc4
a	a	k8xC
Viléma	Vilém	k1gMnSc4
ze	z	k7c2
Saint-Denis	Saint-Denis	k1gFnSc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Toman	Toman	k1gMnSc1
<g/>
,	,	kIx,
R.	R.	kA
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gotika	gotika	k1gFnSc1
<g/>
,	,	kIx,
Slovart	Slovart	k1gInSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2000	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80-7209-248-0	80-7209-248-0	k4
</s>
<s>
Kidson	Kidson	k1gMnSc1
<g/>
,	,	kIx,
P.	P.	kA
<g/>
:	:	kIx,
Románské	románský	k2eAgNnSc1d1
a	a	k8xC
gotické	gotický	k2eAgNnSc1d1
umění	umění	k1gNnSc1
<g/>
,	,	kIx,
Artia	Artia	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1973	#num#	k4
</s>
<s>
Huyghe	Huyghe	k1gFnSc1
<g/>
,	,	kIx,
R.	R.	kA
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Umění	umění	k1gNnSc1
středověku	středověk	k1gInSc2
<g/>
,	,	kIx,
Odeon	odeon	k1gInSc1
(	(	kIx(
<g/>
Larouusse	Larouusse	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1969	#num#	k4
</s>
<s>
Duby	dub	k1gInPc1
<g/>
,	,	kIx,
G.	G.	kA
<g/>
:	:	kIx,
Věk	věk	k1gInSc1
katedrál	katedrála	k1gFnPc2
<g/>
,	,	kIx,
Uměni	Uměn	k1gMnPc1
a	a	k8xC
společnost	společnost	k1gFnSc1
980	#num#	k4
<g/>
–	–	k?
<g/>
1420	#num#	k4
<g/>
,	,	kIx,
Argo	Argo	k1gNnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2002	#num#	k4
</s>
<s>
KOVÁČ	Kováč	k1gMnSc1
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Katedrála	katedrála	k1gFnSc1
v	v	k7c4
Chartres	Chartres	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Francouzské	francouzský	k2eAgNnSc1d1
umění	umění	k1gNnSc1
rané	raný	k2eAgFnSc2d1
a	a	k8xC
vrcholné	vrcholný	k2eAgFnSc2d1
gotiky	gotika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Ars	Ars	k1gFnSc1
Auro	aura	k1gFnSc5
prior	prior	k1gMnSc1
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
612	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
904298	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
KOVÁČ	Kováč	k1gMnSc1
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stavitelé	stavitel	k1gMnPc1
katedrál	katedrála	k1gFnPc2
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kristova	Kristův	k2eAgFnSc1d1
trnová	trnový	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Ars	Ars	k1gFnSc1
Auro	aura	k1gFnSc5
prior	prior	k1gMnSc1
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
384	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
904298	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
KOVÁČ	Kováč	k1gMnSc1
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stavitelé	stavitel	k1gMnPc1
katedrál	katedrála	k1gFnPc2
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úsvit	úsvit	k1gInSc1
renesance	renesance	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Ars	Ars	k1gFnSc1
Auro	aura	k1gFnSc5
prior	prior	k1gMnSc1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
464	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
904298	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
KOVÁČ	Kováč	k1gMnSc1
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stavitelé	stavitel	k1gMnPc1
katedrál	katedrála	k1gFnPc2
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zrození	zrození	k1gNnSc1
génia	génius	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Ars	Ars	k1gFnSc1
Auro	aura	k1gFnSc5
prior	prior	k1gMnSc1
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
.	.	kIx.
544	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
904298	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Novogotika	novogotika	k1gFnSc1
–	–	k?
architektonický	architektonický	k2eAgInSc4d1
styl	styl	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
se	se	k3xPyFc4
v	v	k7c6
době	doba	k1gFnSc6
romantismu	romantismus	k1gInSc2
gotickou	gotický	k2eAgFnSc7d1
architekturou	architektura	k1gFnSc7
inspiroval	inspirovat	k5eAaBmAgMnS
a	a	k8xC
snažil	snažit	k5eAaImAgMnS
se	se	k3xPyFc4
ji	on	k3xPp3gFnSc4
napodobovat	napodobovat	k5eAaImF
</s>
<s>
Barokní	barokní	k2eAgFnSc1d1
gotika	gotika	k1gFnSc1
–	–	k?
barokní	barokní	k2eAgFnPc4d1
stavby	stavba	k1gFnPc4
inspirované	inspirovaný	k2eAgNnSc1d1
gotickou	gotický	k2eAgFnSc7d1
architekturou	architektura	k1gFnSc7
</s>
<s>
Cihlová	cihlový	k2eAgFnSc1d1
gotika	gotika	k1gFnSc1
</s>
<s>
Lomený	lomený	k2eAgInSc1d1
oblouk	oblouk	k1gInSc1
</s>
<s>
Gotická	gotický	k2eAgFnSc1d1
klenba	klenba	k1gFnSc1
</s>
<s>
Křesťanská	křesťanský	k2eAgFnSc1d1
sakrální	sakrální	k2eAgFnSc1d1
architektura	architektura	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
gotická	gotický	k2eAgFnSc1d1
architektura	architektura	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Galerie	galerie	k1gFnSc1
gotická	gotický	k2eAgFnSc1d1
architektura	architektura	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Architektonické	architektonický	k2eAgInPc1d1
směry	směr	k1gInPc1
Starověk	starověk	k1gInSc4
</s>
<s>
Neolitická	neolitický	k2eAgFnSc1d1
architektura	architektura	k1gFnSc1
•	•	k?
Architektura	architektura	k1gFnSc1
starověkého	starověký	k2eAgInSc2d1
Egypta	Egypt	k1gInSc2
•	•	k?
Sumerská	sumerský	k2eAgFnSc1d1
architektura	architektura	k1gFnSc1
•	•	k?
Antická	antický	k2eAgFnSc1d1
architektura	architektura	k1gFnSc1
•	•	k?
Architektura	architektura	k1gFnSc1
starověkého	starověký	k2eAgNnSc2d1
Řecka	Řecko	k1gNnSc2
•	•	k?
Architektura	architektura	k1gFnSc1
starověkého	starověký	k2eAgInSc2d1
Říma	Řím	k1gInSc2
•	•	k?
Raně	raně	k6eAd1
křesťanská	křesťanský	k2eAgFnSc1d1
architektura	architektura	k1gFnSc1
•	•	k?
Byzantská	byzantský	k2eAgFnSc1d1
architektura	architektura	k1gFnSc1
•	•	k?
Incká	incký	k2eAgFnSc1d1
architektura	architektura	k1gFnSc1
Středověk	středověk	k1gInSc1
</s>
<s>
Předrománská	předrománský	k2eAgFnSc1d1
architektura	architektura	k1gFnSc1
•	•	k?
Islámská	islámský	k2eAgFnSc1d1
architektura	architektura	k1gFnSc1
•	•	k?
Románská	románský	k2eAgFnSc1d1
architektura	architektura	k1gFnSc1
•	•	k?
Gotická	gotický	k2eAgFnSc1d1
architektura	architektura	k1gFnSc1
Novověk	novověk	k1gInSc1
</s>
<s>
Renesanční	renesanční	k2eAgFnSc1d1
architektura	architektura	k1gFnSc1
•	•	k?
Manýrismus	manýrismus	k1gInSc1
•	•	k?
Barokní	barokní	k2eAgFnSc1d1
architektura	architektura	k1gFnSc1
•	•	k?
Georgiánská	Georgiánský	k2eAgFnSc1d1
architektura	architektura	k1gFnSc1
•	•	k?
Rokoko	rokoko	k1gNnSc4
•	•	k?
Empír	empír	k1gInSc1
•	•	k?
Klasicistní	klasicistní	k2eAgFnSc1d1
architektura	architektura	k1gFnSc1
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
Architektura	architektura	k1gFnSc1
romantismu	romantismus	k1gInSc2
•	•	k?
Historismus	historismus	k1gInSc1
(	(	kIx(
<g/>
Novorománský	novorománský	k2eAgInSc1d1
•	•	k?
Novobyzantský	Novobyzantský	k2eAgInSc1d1
•	•	k?
Novogotika	novogotika	k1gFnSc1
•	•	k?
Novorenesance	novorenesance	k1gFnSc2
•	•	k?
Novobaroko	Novobaroko	k1gNnSc4
•	•	k?
Neoklasicismus	neoklasicismus	k1gInSc1
•	•	k?
Eklekticismus	eklekticismus	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Monumentalismus	Monumentalismus	k1gInSc1
•	•	k?
Purismus	purismus	k1gInSc1
•	•	k?
Secesní	secesní	k2eAgFnSc1d1
architektura	architektura	k1gFnSc1
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
Kubismus	kubismus	k1gInSc1
•	•	k?
Dekorativismus	dekorativismus	k1gInSc1
•	•	k?
Konstruktivismus	konstruktivismus	k1gInSc1
•	•	k?
Funkcionalismus	funkcionalismus	k1gInSc1
•	•	k?
Rurální	rurální	k2eAgInSc1d1
funkcionalismus	funkcionalismus	k1gInSc1
•	•	k?
Socialistický	socialistický	k2eAgInSc1d1
realismus	realismus	k1gInSc1
•	•	k?
Neofunkcionalismus	Neofunkcionalismus	k1gInSc1
•	•	k?
Brutalismus	brutalismus	k1gInSc1
•	•	k?
Moderní	moderní	k2eAgFnSc1d1
architektura	architektura	k1gFnSc1
•	•	k?
Postmoderní	postmoderní	k2eAgFnSc1d1
architektura	architektura	k1gFnSc1
•	•	k?
Minimalismus	minimalismus	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Architektura	architektura	k1gFnSc1
a	a	k8xC
stavebnictví	stavebnictví	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85006792	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85006792	#num#	k4
</s>
