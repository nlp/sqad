<s>
Radio	radio	k1gNnSc1
Frequency	Frequenca	k1gFnSc2
Identification	Identification	k1gInSc1
<g/>
,	,	kIx,
identifikace	identifikace	k1gFnSc1
na	na	k7c6
rádiové	rádiový	k2eAgFnSc6d1
frekvenci	frekvence	k1gFnSc6
(	(	kIx(
<g/>
RFID	RFID	kA
<g/>
)	)	kIx)
je	on	k3xPp3gFnPc4
další	další	k2eAgFnPc4d1
generace	generace	k1gFnPc4
identifikátorů	identifikátor	k1gInPc2
navržených	navržený	k2eAgInPc2d1
(	(	kIx(
<g/>
nejen	nejen	k6eAd1
<g/>
)	)	kIx)
k	k	k7c3
identifikaci	identifikace	k1gFnSc3
zboží	zboží	k1gNnSc2
<g/>
,	,	kIx,
navazující	navazující	k2eAgFnSc2d1
na	na	k7c4
systém	systém	k1gInSc4
čárových	čárový	k2eAgInPc2d1
kódů	kód	k1gInPc2
<g/>
.	.	kIx.
</s>