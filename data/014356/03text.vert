<s>
RFID	RFID	kA
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojován	ozdrojován	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yIgFnPc4,k3yRgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
RFID	RFID	kA
čip	čip	k1gInSc1
s	s	k7c7
anténou	anténa	k1gFnSc7
</s>
<s>
Radio	radio	k1gNnSc1
Frequency	Frequenca	k1gFnSc2
Identification	Identification	k1gInSc1
<g/>
,	,	kIx,
identifikace	identifikace	k1gFnSc1
na	na	k7c6
rádiové	rádiový	k2eAgFnSc6d1
frekvenci	frekvence	k1gFnSc6
(	(	kIx(
<g/>
RFID	RFID	kA
<g/>
)	)	kIx)
je	on	k3xPp3gFnPc4
další	další	k2eAgFnPc4d1
generace	generace	k1gFnPc4
identifikátorů	identifikátor	k1gInPc2
navržených	navržený	k2eAgInPc2d1
(	(	kIx(
<g/>
nejen	nejen	k6eAd1
<g/>
)	)	kIx)
k	k	k7c3
identifikaci	identifikace	k1gFnSc3
zboží	zboží	k1gNnSc2
<g/>
,	,	kIx,
navazující	navazující	k2eAgFnSc2d1
na	na	k7c4
systém	systém	k1gInSc4
čárových	čárový	k2eAgInPc2d1
kódů	kód	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejně	stejně	k6eAd1
jako	jako	k8xC,k8xS
čárové	čárový	k2eAgInPc1d1
kódy	kód	k1gInPc1
slouží	sloužit	k5eAaImIp3nP
k	k	k7c3
bezkontaktní	bezkontaktní	k2eAgFnSc3d1
komunikaci	komunikace	k1gFnSc3
na	na	k7c4
krátkou	krátký	k2eAgFnSc4d1
vzdálenost	vzdálenost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Iniciátorem	iniciátor	k1gMnSc7
vývoje	vývoj	k1gInSc2
je	být	k5eAaImIp3nS
stejně	stejně	k6eAd1
jako	jako	k9
u	u	k7c2
čárových	čárový	k2eAgInPc2d1
kódů	kód	k1gInPc2
firma	firma	k1gFnSc1
Wal-Mart	Wal-Mart	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Patent	patent	k1gInSc1
na	na	k7c4
technologii	technologie	k1gFnSc4
RFID	RFID	kA
získal	získat	k5eAaPmAgMnS
vynálezce	vynálezce	k1gMnSc1
Charles	Charles	k1gMnSc1
Walton	Walton	k1gInSc1
(	(	kIx(
<g/>
není	být	k5eNaImIp3nS
z	z	k7c2
rodiny	rodina	k1gFnSc2
majitelů	majitel	k1gMnPc2
Wal-Martu	Wal-Mart	k1gInSc2
<g/>
)	)	kIx)
v	v	k7c6
roce	rok	k1gInSc6
1983	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Čipy	čip	k1gInPc1
jsou	být	k5eAaImIp3nP
k	k	k7c3
dispozici	dispozice	k1gFnSc3
v	v	k7c6
provedení	provedení	k1gNnSc6
pro	pro	k7c4
čtení	čtení	k1gNnSc4
nebo	nebo	k8xC
pro	pro	k7c4
čtení	čtení	k1gNnSc4
a	a	k8xC
zápis	zápis	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
komunikaci	komunikace	k1gFnSc4
využívají	využívat	k5eAaImIp3nP,k5eAaPmIp3nP
převážně	převážně	k6eAd1
nosnou	nosný	k2eAgFnSc4d1
frekvenci	frekvence	k1gFnSc4
125	#num#	k4
kHz	khz	kA
<g/>
,	,	kIx,
134	#num#	k4
kHz	khz	kA
a	a	k8xC
13,56	13,56	k4
MHz	Mhz	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
některých	některý	k3yIgInPc6
státech	stát	k1gInPc6
se	se	k3xPyFc4
dají	dát	k5eAaPmIp3nP
používat	používat	k5eAaImF
i	i	k9
další	další	k2eAgFnPc4d1
frekvence	frekvence	k1gFnPc4
jako	jako	k8xC,k8xS
868	#num#	k4
MHz	Mhz	kA
(	(	kIx(
<g/>
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
)	)	kIx)
a	a	k8xC
915	#num#	k4
MHz	Mhz	kA
(	(	kIx(
<g/>
v	v	k7c6
Americe	Amerika	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Technologii	technologie	k1gFnSc4
RFID	RFID	kA
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
a	a	k8xC
vylepšuje	vylepšovat	k5eAaImIp3nS
novější	nový	k2eAgInSc4d2
systém	systém	k1gInSc4
NFC	NFC	kA
<g/>
,	,	kIx,
rozšiřující	rozšiřující	k2eAgFnSc4d1
jejich	jejich	k3xOp3gFnSc3
možnosti	možnost	k1gFnSc3
a	a	k8xC
používání	používání	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s>
Typy	typ	k1gInPc1
RFID	RFID	kA
čipů	čip	k1gInPc2
</s>
<s>
Pasivní	pasivní	k2eAgFnSc1d1
</s>
<s>
Vysílač	vysílač	k1gInSc1
(	(	kIx(
<g/>
snímač	snímač	k1gInSc1
<g/>
,	,	kIx,
čtečka	čtečka	k1gFnSc1
<g/>
)	)	kIx)
periodicky	periodicky	k6eAd1
vysílá	vysílat	k5eAaImIp3nS
do	do	k7c2
okolí	okolí	k1gNnSc2
elektromagnetické	elektromagnetický	k2eAgInPc4d1
pulsy	puls	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
se	se	k3xPyFc4
v	v	k7c6
blízkosti	blízkost	k1gFnSc6
objeví	objevit	k5eAaPmIp3nS
pasivní	pasivní	k2eAgInSc1d1
RFID	RFID	kA
čip	čip	k1gInSc1
<g/>
,	,	kIx,
využije	využít	k5eAaPmIp3nS
přijímanou	přijímaný	k2eAgFnSc4d1
energii	energie	k1gFnSc4
k	k	k7c3
nabití	nabití	k1gNnSc3
svého	svůj	k3xOyFgInSc2
napájecího	napájecí	k2eAgInSc2d1
kondenzátoru	kondenzátor	k1gInSc2
a	a	k8xC
odešle	odeslat	k5eAaPmIp3nS
odpověď	odpověď	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pasivní	pasivní	k2eAgInPc1d1
čipy	čip	k1gInPc1
dokáží	dokázat	k5eAaPmIp3nP
vysílat	vysílat	k5eAaImF
buď	buď	k8xC
jedno	jeden	k4xCgNnSc1
číslo	číslo	k1gNnSc1
(	(	kIx(
<g/>
elektronické	elektronický	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
produktu	produkt	k1gInSc2
EPC	EPC	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
určené	určený	k2eAgInPc4d1
při	při	k7c6
jejich	jejich	k3xOp3gFnSc6
výrobě	výroba	k1gFnSc6
<g/>
,	,	kIx,
nebo	nebo	k8xC
disponují	disponovat	k5eAaBmIp3nP
navíc	navíc	k6eAd1
ještě	ještě	k9
dodatečnou	dodatečný	k2eAgFnSc7d1
pamětí	paměť	k1gFnSc7
<g/>
,	,	kIx,
do	do	k7c2
které	který	k3yRgFnSc2,k3yIgFnSc2,k3yQgFnSc2
lze	lze	k6eAd1
zapisovat	zapisovat	k5eAaImF
a	a	k8xC
číst	číst	k5eAaImF
další	další	k2eAgFnPc4d1
informace	informace	k1gFnPc4
(	(	kIx(
<g/>
například	například	k6eAd1
v	v	k7c6
případě	případ	k1gInSc6
elektronické	elektronický	k2eAgFnSc2d1
peněženky	peněženka	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Využívají	využívat	k5eAaPmIp3nP,k5eAaImIp3nP
se	se	k3xPyFc4
například	například	k6eAd1
v	v	k7c6
těchto	tento	k3xDgFnPc6
aplikacích	aplikace	k1gFnPc6
<g/>
:	:	kIx,
</s>
<s>
K	k	k7c3
identifikaci	identifikace	k1gFnSc3
předmětů	předmět	k1gInPc2
(	(	kIx(
<g/>
zboží	zboží	k1gNnSc2
<g/>
)	)	kIx)
jako	jako	k8xC,k8xS
nástupce	nástupce	k1gMnSc1
čárového	čárový	k2eAgInSc2d1
kódu	kód	k1gInSc2
<g/>
,	,	kIx,
</s>
<s>
k	k	k7c3
řízení	řízení	k1gNnSc3
přístupu	přístup	k1gInSc2
osob	osoba	k1gFnPc2
do	do	k7c2
uzavřených	uzavřený	k2eAgInPc2d1
objektů	objekt	k1gInPc2
<g/>
,	,	kIx,
</s>
<s>
k	k	k7c3
bezhotovostním	bezhotovostní	k2eAgFnPc3d1
platbám	platba	k1gFnPc3
v	v	k7c6
podobě	podoba	k1gFnSc6
elektronické	elektronický	k2eAgFnSc2d1
peněženky	peněženka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Aktivní	aktivní	k2eAgFnSc1d1
</s>
<s>
Používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
méně	málo	k6eAd2
často	často	k6eAd1
než	než	k8xS
pasivní	pasivní	k2eAgInSc1d1
systém	systém	k1gInSc1
RFID	RFID	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
totiž	totiž	k9
složitější	složitý	k2eAgMnSc1d2
a	a	k8xC
dražší	drahý	k2eAgMnSc1d2
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
obsahují	obsahovat	k5eAaImIp3nP
navíc	navíc	k6eAd1
i	i	k9
zdroj	zdroj	k1gInSc1
napájení	napájení	k1gNnSc2
a	a	k8xC
jsou	být	k5eAaImIp3nP
schopny	schopen	k2eAgFnPc1d1
samy	sám	k3xTgFnPc1
vysílat	vysílat	k5eAaImF
svou	svůj	k3xOyFgFnSc4
identifikaci	identifikace	k1gFnSc4
–	–	k?
používají	používat	k5eAaImIp3nP
se	se	k3xPyFc4
proto	proto	k8xC
pro	pro	k7c4
aktivní	aktivní	k2eAgFnSc4d1
lokalizaci	lokalizace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktivní	aktivní	k2eAgInPc1d1
RFID	RFID	kA
čipy	čip	k1gInPc1
kromě	kromě	k7c2
svého	svůj	k3xOyFgNnSc2
identifikačního	identifikační	k2eAgNnSc2d1
čísla	číslo	k1gNnSc2
většinou	většinou	k6eAd1
mají	mít	k5eAaImIp3nP
prostor	prostor	k1gInSc4
pro	pro	k7c4
další	další	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
(	(	kIx(
<g/>
na	na	k7c4
podnět	podnět	k1gInSc4
obdobný	obdobný	k2eAgInSc4d1
výzvě	výzva	k1gFnSc3
pro	pro	k7c4
identifikaci	identifikace	k1gFnSc4
<g/>
)	)	kIx)
dokáží	dokázat	k5eAaPmIp3nP
ukládat	ukládat	k5eAaImF
nebo	nebo	k8xC
odeslat	odeslat	k5eAaPmF
spolu	spolu	k6eAd1
s	s	k7c7
identifikačním	identifikační	k2eAgNnSc7d1
číslem	číslo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obdobný	obdobný	k2eAgInSc1d1
je	být	k5eAaImIp3nS
princip	princip	k1gInSc1
NFC	NFC	kA
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
je	být	k5eAaImIp3nS
patrně	patrně	k6eAd1
perspektivně	perspektivně	k6eAd1
nahradí	nahradit	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Identifikace	identifikace	k1gFnSc1
RFID	RFID	kA
čipů	čip	k1gInPc2
</s>
<s>
RFID	RFID	kA
čipy	čip	k1gInPc1
obsahují	obsahovat	k5eAaImIp3nP
96	#num#	k4
<g/>
bitové	bitový	k2eAgNnSc1d1
unikátní	unikátní	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
takzvané	takzvaný	k2eAgFnSc2d1
EPC	EPC	kA
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
(	(	kIx(
<g/>
z	z	k7c2
hlediska	hledisko	k1gNnSc2
logistiky	logistika	k1gFnSc2
a	a	k8xC
obchodu	obchod	k1gInSc2
<g/>
)	)	kIx)
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
přiděleno	přidělit	k5eAaPmNgNnS
každému	každý	k3xTgInSc3
jednotlivému	jednotlivý	k2eAgInSc3d1
konkrétnímu	konkrétní	k2eAgInSc3d1
kusu	kus	k1gInSc3
zboží	zboží	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
EPC	EPC	kA
se	se	k3xPyFc4
přiděluje	přidělovat	k5eAaImIp3nS
centrálně	centrálně	k6eAd1
výrobcům	výrobce	k1gMnPc3
v	v	k7c6
jednotlivých	jednotlivý	k2eAgFnPc6d1
řadách	řada	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
EPC	EPC	kA
o	o	k7c6
délce	délka	k1gFnSc6
96	#num#	k4
bitů	bit	k1gInPc2
má	mít	k5eAaImIp3nS
nabídnout	nabídnout	k5eAaPmF
dostatečný	dostatečný	k2eAgInSc4d1
číselný	číselný	k2eAgInSc4d1
prostor	prostor	k1gInSc4
268	#num#	k4
milionům	milion	k4xCgInPc3
výrobců	výrobce	k1gMnPc2
produkujícím	produkující	k2eAgFnPc3d1
každý	každý	k3xTgInSc1
16	#num#	k4
milionů	milion	k4xCgInPc2
druhů	druh	k1gInPc2
výrobků	výrobek	k1gInPc2
(	(	kIx(
<g/>
tříd	třída	k1gFnPc2
<g/>
)	)	kIx)
a	a	k8xC
v	v	k7c6
každé	každý	k3xTgFnSc6
třídě	třída	k1gFnSc6
je	být	k5eAaImIp3nS
prostor	prostor	k1gInSc4
pro	pro	k7c4
68	#num#	k4
miliard	miliarda	k4xCgFnPc2
sériových	sériový	k2eAgNnPc2d1
čísel	číslo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
zatím	zatím	k6eAd1
není	být	k5eNaImIp3nS
ani	ani	k8xC
teoretický	teoretický	k2eAgInSc4d1
výhled	výhled	k1gInSc4
na	na	k7c4
upotřebení	upotřebení	k1gNnSc4
takového	takový	k3xDgNnSc2
množství	množství	k1gNnSc2
čísel	číslo	k1gNnPc2
EPC	EPC	kA
<g/>
,	,	kIx,
mohou	moct	k5eAaImIp3nP
čipy	čip	k1gInPc1
používat	používat	k5eAaImF
EPC	EPC	kA
o	o	k7c6
délce	délka	k1gFnSc6
64	#num#	k4
bitů	bit	k1gInPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
sníží	snížit	k5eAaPmIp3nS
jejich	jejich	k3xOp3gFnSc4
cenu	cena	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
druhou	druhý	k4xOgFnSc4
stranu	strana	k1gFnSc4
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
i	i	k9
výhled	výhled	k1gInSc4
pro	pro	k7c4
přechod	přechod	k1gInSc4
na	na	k7c4
128	#num#	k4
bitů	bit	k1gInPc2
pro	pro	k7c4
případ	případ	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nP
číselné	číselný	k2eAgFnPc1d1
řady	řada	k1gFnPc1
přestaly	přestat	k5eAaPmAgFnP
stačit	stačit	k5eAaBmF
<g/>
.	.	kIx.
</s>
<s>
Informace	informace	k1gFnPc1
obsažené	obsažený	k2eAgFnPc1d1
v	v	k7c6
paměti	paměť	k1gFnSc6
RFID	RFID	kA
čipu	čip	k1gInSc2
</s>
<s>
K	k	k7c3
odvozování	odvozování	k1gNnSc3
informací	informace	k1gFnPc2
na	na	k7c6
základě	základ	k1gInSc6
EPC	EPC	kA
přímo	přímo	k6eAd1
slouží	sloužit	k5eAaImIp3nS
služba	služba	k1gFnSc1
zvaná	zvaný	k2eAgFnSc1d1
Object	Object	k1gInSc4
Name	Nam	k1gInSc2
Service	Service	k1gFnSc2
-	-	kIx~
ONS	ONS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
přiřazuje	přiřazovat	k5eAaImIp3nS
ke	k	k7c3
každému	každý	k3xTgNnSc3
EPC	EPC	kA
adresu	adresa	k1gFnSc4
s	s	k7c7
popisem	popis	k1gInSc7
zboží	zboží	k1gNnSc2
ve	v	k7c6
formátu	formát	k1gInSc6
XML	XML	kA
<g/>
,	,	kIx,
resp.	resp.	kA
jeho	jeho	k3xOp3gInSc6
speciálním	speciální	k2eAgInSc6d1
derivátu	derivát	k1gInSc6
PML	PML	kA
-	-	kIx~
Physical	Physical	k1gMnSc1
Markup	Markup	k1gMnSc1
Language	language	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
formátu	formát	k1gInSc6
se	se	k3xPyFc4
mohou	moct	k5eAaImIp3nP
uchovávat	uchovávat	k5eAaImF
všechna	všechen	k3xTgNnPc4
potřebná	potřebný	k2eAgNnPc4d1
data	datum	k1gNnPc4
ke	k	k7c3
zboží	zboží	k1gNnSc3
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
jeho	jeho	k3xOp3gFnSc1
záruka	záruka	k1gFnSc1
<g/>
,	,	kIx,
trvanlivost	trvanlivost	k1gFnSc1
<g/>
,	,	kIx,
způsoby	způsob	k1gInPc1
použití	použití	k1gNnSc2
a	a	k8xC
další	další	k2eAgInPc4d1
údaje	údaj	k1gInPc4
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc4
může	moct	k5eAaImIp3nS
obchodník	obchodník	k1gMnSc1
snadno	snadno	k6eAd1
importovat	importovat	k5eAaBmF
a	a	k8xC
používat	používat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
Sport	sport	k1gInSc1
</s>
<s>
RFID	RFID	kA
UHF	UHF	kA
anténa	anténa	k1gFnSc1
IronTime	IronTim	k1gInSc5
</s>
<s>
RFID	RFID	kA
čipy	čip	k1gInPc4
různých	různý	k2eAgFnPc2d1
frekvencí	frekvence	k1gFnPc2
se	se	k3xPyFc4
používají	používat	k5eAaImIp3nP
jako	jako	k8xS,k8xC
čipová	čipový	k2eAgFnSc1d1
časomíra	časomíra	k1gFnSc1
pro	pro	k7c4
změření	změření	k1gNnSc4
účastníků	účastník	k1gMnPc2
sportovních	sportovní	k2eAgInPc2d1
závodů	závod	k1gInPc2
<g/>
,	,	kIx,
především	především	k9
masových	masový	k2eAgInPc2d1
<g/>
,	,	kIx,
kde	kde	k6eAd1
není	být	k5eNaImIp3nS
možné	možný	k2eAgNnSc1d1
měřit	měřit	k5eAaImF
jednotlivce	jednotlivec	k1gMnPc4
přesně	přesně	k6eAd1
stopkami	stopka	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Závodníci	Závodník	k1gMnPc1
mají	mít	k5eAaImIp3nP
čip	čip	k1gInSc4
umístěn	umístit	k5eAaPmNgInS
obvykle	obvykle	k6eAd1
na	na	k7c6
textilním	textilní	k2eAgInSc6d1
pásku	pásek	k1gInSc6
nad	nad	k7c7
kotníkem	kotník	k1gInSc7
<g/>
,	,	kIx,
na	na	k7c6
startovním	startovní	k2eAgNnSc6d1
čísle	číslo	k1gNnSc6
nebo	nebo	k8xC
na	na	k7c6
přilbě	přilba	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čip	čip	k1gInSc1
se	se	k3xPyFc4
načte	načíst	k5eAaPmIp3nS,k5eAaBmIp3nS
při	při	k7c6
průběhu	průběh	k1gInSc6
cílovou	cílový	k2eAgFnSc4d1
linií	linie	k1gFnSc7
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
umístěna	umístěn	k2eAgFnSc1d1
anténa	anténa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Frekvence	frekvence	k1gFnSc1
RFID	RFID	kA
HF	HF	kA
(	(	kIx(
<g/>
13	#num#	k4
MHz	Mhz	kA
<g/>
)	)	kIx)
vyžadují	vyžadovat	k5eAaImIp3nP
dlouhé	dlouhý	k2eAgFnPc4d1
kovové	kovový	k2eAgFnPc4d1
smyčky	smyčka	k1gFnPc4
položené	položený	k2eAgFnPc4d1
v	v	k7c6
cílové	cílový	k2eAgFnSc6d1
linii	linie	k1gFnSc6
<g/>
,	,	kIx,
vyšší	vysoký	k2eAgFnSc1d2
frekvence	frekvence	k1gFnSc1
UHF	UHF	kA
(	(	kIx(
<g/>
860	#num#	k4
MHz	Mhz	kA
<g/>
)	)	kIx)
potom	potom	k6eAd1
malé	malý	k2eAgFnSc2d1
antény	anténa	k1gFnSc2
na	na	k7c6
straně	strana	k1gFnSc6
v	v	k7c6
cíli	cíl	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dosah	dosah	k1gInSc1
antény	anténa	k1gFnSc2
UHF	UHF	kA
je	být	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
6	#num#	k4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolehlivost	spolehlivost	k1gFnSc1
čtení	čtení	k1gNnSc2
a	a	k8xC
dosah	dosah	k1gInSc1
čtečky	čtečka	k1gFnSc2
čipů	čip	k1gInPc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
negativně	negativně	k6eAd1
ovlivněn	ovlivněn	k2eAgInSc1d1
lidským	lidský	k2eAgNnSc7d1
tělem	tělo	k1gNnSc7
<g/>
,	,	kIx,
neboť	neboť	k8xC
voda	voda	k1gFnSc1
v	v	k7c6
něm	on	k3xPp3gInSc6
pohlcuje	pohlcovat	k5eAaImIp3nS
rádiový	rádiový	k2eAgInSc4d1
signál	signál	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Near	Near	k1gMnSc1
Field	Field	k1gMnSc1
Communication	Communication	k1gInSc4
</s>
<s>
VeriChip	VeriChip	k1gMnSc1
</s>
<s>
In-karta	In-karta	k1gFnSc1
<g/>
,	,	kIx,
Opencard	Opencard	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
RFID	RFID	kA
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
RFID	RFID	kA
v	v	k7c6
České	český	k2eAgFnSc6d1
terminologické	terminologický	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
knihovnictví	knihovnictví	k1gNnSc2
a	a	k8xC
informační	informační	k2eAgFnSc2d1
vědy	věda	k1gFnSc2
(	(	kIx(
<g/>
TDKIV	TDKIV	kA
<g/>
)	)	kIx)
</s>
<s>
RFID-EPC	RFID-EPC	k?
Portal	Portal	k1gMnSc1
-	-	kIx~
český	český	k2eAgInSc1d1
nekomerční	komerční	k2eNgInSc1d1
portál	portál	k1gInSc1
věnovaný	věnovaný	k2eAgInSc1d1
problematice	problematika	k1gFnSc3
RFID	RFID	kA
a	a	k8xC
EPC	EPC	kA
<g/>
.	.	kIx.
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
RFID	RFID	kA
(	(	kIx(
<g/>
Radio	radio	k1gNnSc1
Frequency	Frequenca	k1gFnSc2
Identification	Identification	k1gInSc1
<g/>
)	)	kIx)
and	and	k?
Surveillance	Surveillance	k1gFnSc1
–	–	k?
sympozium	sympozium	k1gNnSc1
několika	několik	k4yIc2
odborníků	odborník	k1gMnPc2
na	na	k7c4
téma	téma	k1gNnSc4
RFID	RFID	kA
čipů	čip	k1gInPc2
a	a	k8xC
práva	právo	k1gNnSc2
na	na	k7c6
půdě	půda	k1gFnSc6
Santa	Santa	k1gMnSc1
Clara	Clara	k1gFnSc1
University	universita	k1gFnSc2
of	of	k?
California	Californium	k1gNnSc2
</s>
<s>
Aplikace	aplikace	k1gFnSc1
RFID	RFID	kA
v	v	k7c6
dodavatelském	dodavatelský	k2eAgInSc6d1
řetězci	řetězec	k1gInSc6
</s>
<s>
Identifikace	identifikace	k1gFnSc1
založená	založený	k2eAgFnSc1d1
na	na	k7c6
rádiové	rádiový	k2eAgFnSc6d1
frekvenci	frekvence	k1gFnSc6
–	–	k?
RFID	RFID	kA
</s>
<s>
RFID	RFID	kA
Guide	Guid	k1gInSc5
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4509863-3	4509863-3	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
13657	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
Informační	informační	k2eAgFnSc1d1
věda	věda	k1gFnSc1
a	a	k8xC
knihovnictví	knihovnictví	k1gNnSc1
</s>
