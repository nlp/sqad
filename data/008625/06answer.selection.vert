<s>
Jónský	jónský	k2eAgInSc1d1	jónský
modus	modus	k1gInSc1	modus
je	být	k5eAaImIp3nS	být
pojem	pojem	k1gInSc4	pojem
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
hudební	hudební	k2eAgFnSc2d1	hudební
nauky	nauka	k1gFnSc2	nauka
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
posloupnost	posloupnost	k1gFnSc4	posloupnost
tónů	tón	k1gInPc2	tón
diatonické	diatonický	k2eAgFnSc2d1	diatonická
durové	durový	k2eAgFnSc2d1	durová
stupnice	stupnice	k1gFnSc2	stupnice
zahrané	zahraný	k2eAgFnSc2d1	zahraná
od	od	k7c2	od
jejího	její	k3xOp3gInSc2	její
prvního	první	k4xOgInSc2	první
stupně	stupeň	k1gInSc2	stupeň
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
jónský	jónský	k2eAgInSc1d1	jónský
modus	modus	k1gInSc1	modus
zní	znět	k5eAaImIp3nS	znět
stejně	stejně	k6eAd1	stejně
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
sama	sám	k3xTgFnSc1	sám
diatonická	diatonický	k2eAgFnSc1d1	diatonická
durová	durový	k2eAgFnSc1d1	durová
stupnice	stupnice	k1gFnSc1	stupnice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
