<p>
<s>
Jónský	jónský	k2eAgInSc1d1	jónský
modus	modus	k1gInSc1	modus
je	být	k5eAaImIp3nS	být
pojem	pojem	k1gInSc4	pojem
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
hudební	hudební	k2eAgFnSc2d1	hudební
nauky	nauka	k1gFnSc2	nauka
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
posloupnost	posloupnost	k1gFnSc4	posloupnost
tónů	tón	k1gInPc2	tón
diatonické	diatonický	k2eAgFnSc2d1	diatonická
durové	durový	k2eAgFnSc2d1	durová
stupnice	stupnice	k1gFnSc2	stupnice
zahrané	zahraný	k2eAgFnSc2d1	zahraná
od	od	k7c2	od
jejího	její	k3xOp3gInSc2	její
prvního	první	k4xOgInSc2	první
stupně	stupeň	k1gInSc2	stupeň
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
jónský	jónský	k2eAgInSc1d1	jónský
modus	modus	k1gInSc1	modus
zní	znět	k5eAaImIp3nS	znět
stejně	stejně	k6eAd1	stejně
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
sama	sám	k3xTgFnSc1	sám
diatonická	diatonický	k2eAgFnSc1d1	diatonická
durová	durový	k2eAgFnSc1d1	durová
stupnice	stupnice	k1gFnSc1	stupnice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
terminologii	terminologie	k1gFnSc6	terminologie
používané	používaný	k2eAgFnSc2d1	používaná
v	v	k7c6	v
antickém	antický	k2eAgNnSc6d1	antické
Řecku	Řecko	k1gNnSc6	Řecko
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
názvy	název	k1gInPc1	název
církevních	církevní	k2eAgInPc2d1	církevní
modů	modus	k1gInPc2	modus
pocházejí	pocházet	k5eAaImIp3nP	pocházet
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
jónský	jónský	k2eAgInSc4d1	jónský
modus	modus	k1gInSc4	modus
nazýván	nazývat	k5eAaImNgInS	nazývat
lydický	lydický	k2eAgInSc1d1	lydický
–	–	k?	–
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
název	název	k1gInSc1	název
používán	používat	k5eAaImNgInS	používat
pro	pro	k7c4	pro
modus	modus	k1gInSc4	modus
od	od	k7c2	od
čtvrtého	čtvrtý	k4xOgInSc2	čtvrtý
stupně	stupeň	k1gInSc2	stupeň
durové	durový	k2eAgFnSc2d1	durová
stupnice	stupnice	k1gFnSc2	stupnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc4	vlastnost
jónského	jónský	k2eAgInSc2d1	jónský
modu	modus	k1gInSc2	modus
==	==	k?	==
</s>
</p>
<p>
<s>
Jónský	jónský	k2eAgInSc1d1	jónský
modus	modus	k1gInSc1	modus
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
z	z	k7c2	z
durové	durový	k2eAgFnSc2d1	durová
stupnice	stupnice	k1gFnSc2	stupnice
jejím	její	k3xOp3gNnPc3	její
zahráním	zahrání	k1gNnPc3	zahrání
od	od	k7c2	od
prvního	první	k4xOgInSc2	první
stupně	stupeň	k1gInSc2	stupeň
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
případě	případ	k1gInSc6	případ
C	C	kA	C
dur	dur	k1gNnSc6	dur
je	být	k5eAaImIp3nS	být
základním	základní	k2eAgInSc7d1	základní
tónem	tón	k1gInSc7	tón
jónského	jónský	k2eAgInSc2d1	jónský
modu	modus	k1gInSc2	modus
C	C	kA	C
a	a	k8xC	a
znění	znění	k1gNnSc6	znění
jónského	jónský	k2eAgInSc2d1	jónský
modu	modus	k1gInSc2	modus
<g/>
:	:	kIx,	:
c-d-e-f-g-a-	c-	k?	c-d-e-f-g-a-
<g/>
h.	h.	k?	h.
</s>
</p>
<p>
<s>
Nejbližším	blízký	k2eAgInSc7d3	Nejbližší
tvrdším	tvrdý	k2eAgInSc7d2	tvrdší
modem	modus	k1gInSc7	modus
je	být	k5eAaImIp3nS	být
lydický	lydický	k2eAgInSc1d1	lydický
modus	modus	k1gInSc1	modus
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
od	od	k7c2	od
jónského	jónský	k2eAgNnSc2d1	Jónské
liší	lišit	k5eAaImIp3nS	lišit
zvětšenou	zvětšený	k2eAgFnSc7d1	zvětšená
kvartou	kvarta	k1gFnSc7	kvarta
<g/>
.	.	kIx.	.
</s>
<s>
Nejbližším	blízký	k2eAgInSc7d3	Nejbližší
měkčím	měkký	k2eAgInSc7d2	měkčí
modem	modus	k1gInSc7	modus
je	být	k5eAaImIp3nS	být
mixolydický	mixolydický	k2eAgInSc1d1	mixolydický
modus	modus	k1gInSc1	modus
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
od	od	k7c2	od
jónského	jónský	k2eAgNnSc2d1	Jónské
liší	lišit	k5eAaImIp3nS	lišit
malou	malý	k2eAgFnSc7d1	malá
septimou	septima	k1gFnSc7	septima
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Intervalové	intervalový	k2eAgNnSc4d1	intervalové
složení	složení	k1gNnSc4	složení
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Složení	složení	k1gNnSc1	složení
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
tóninách	tónina	k1gFnPc6	tónina
==	==	k?	==
</s>
</p>
<p>
<s>
Složení	složení	k1gNnSc1	složení
jónského	jónský	k2eAgInSc2d1	jónský
modu	modus	k1gInSc2	modus
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
tóninách	tónina	k1gFnPc6	tónina
shodné	shodný	k2eAgFnPc1d1	shodná
se	s	k7c7	s
složením	složení	k1gNnSc7	složení
durové	durový	k2eAgFnSc2d1	durová
stupnice	stupnice	k1gFnSc2	stupnice
a	a	k8xC	a
lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
najít	najít	k5eAaPmF	najít
v	v	k7c6	v
seznamu	seznam	k1gInSc6	seznam
durových	durový	k2eAgFnPc2d1	durová
stupnic	stupnice	k1gFnPc2	stupnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Charakteristické	charakteristický	k2eAgInPc1d1	charakteristický
akordy	akord	k1gInPc1	akord
==	==	k?	==
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
jónský	jónský	k2eAgInSc4d1	jónský
modus	modus	k1gInSc4	modus
je	být	k5eAaImIp3nS	být
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
durový	durový	k2eAgInSc1d1	durový
kvintakord	kvintakord	k1gInSc1	kvintakord
<g/>
,	,	kIx,	,
ze	z	k7c2	z
septakordů	septakord	k1gInPc2	septakord
pak	pak	k6eAd1	pak
velký	velký	k2eAgInSc4d1	velký
septakord	septakord	k1gInSc4	septakord
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kompletní	kompletní	k2eAgInSc1d1	kompletní
tónový	tónový	k2eAgInSc1d1	tónový
materiál	materiál	k1gInSc1	materiál
modu	modus	k1gInSc2	modus
je	být	k5eAaImIp3nS	být
vyjádřen	vyjádřit	k5eAaPmNgInS	vyjádřit
sedmizvukem	sedmizvuk	k1gInSc7	sedmizvuk
(	(	kIx(	(
<g/>
tercdecimovým	tercdecimový	k2eAgInSc7d1	tercdecimový
akordem	akord	k1gInSc7	akord
<g/>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
X	X	kA	X
</s>
</p>
<p>
</p>
<p>
<s>
13	[number]	k4	13
</s>
</p>
<p>
</p>
<p>
<s>
/	/	kIx~	/
</s>
</p>
<p>
</p>
<p>
<s>
7	[number]	k4	7
</s>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
j	j	k?	j
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
X	X	kA	X
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
13	[number]	k4	13
<g/>
/	/	kIx~	/
<g/>
7	[number]	k4	7
<g/>
maj	maj	k?	maj
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
X	X	kA	X
je	být	k5eAaImIp3nS	být
základní	základní	k2eAgInSc4d1	základní
tón	tón	k1gInSc4	tón
jónského	jónský	k2eAgInSc2d1	jónský
modu	modus	k1gInSc2	modus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
charakteristické	charakteristický	k2eAgInPc4d1	charakteristický
akordy	akord	k1gInPc4	akord
jónského	jónský	k2eAgInSc2d1	jónský
modu	modus	k1gInSc2	modus
v	v	k7c6	v
tónině	tónina	k1gFnSc6	tónina
C	C	kA	C
dur	dur	k1gNnSc7	dur
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Použitelné	použitelný	k2eAgInPc1d1	použitelný
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
další	další	k2eAgInPc1d1	další
akordy	akord	k1gInPc1	akord
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
vzniknou	vzniknout	k5eAaPmIp3nP	vzniknout
vynecháním	vynechání	k1gNnSc7	vynechání
některých	některý	k3yIgInPc2	některý
intervalů	interval	k1gInPc2	interval
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
C	C	kA	C
</s>
</p>
<p>
</p>
<p>
<s>
13	[number]	k4	13
</s>
</p>
<p>
</p>
<p>
<s>
/	/	kIx~	/
</s>
</p>
<p>
</p>
<p>
<s>
7	[number]	k4	7
</s>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
j	j	k?	j
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
C	C	kA	C
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
13	[number]	k4	13
<g/>
/	/	kIx~	/
<g/>
7	[number]	k4	7
<g/>
maj	maj	k?	maj
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
C	C	kA	C
</s>
</p>
<p>
</p>
<p>
<s>
6	[number]	k4	6
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
C	C	kA	C
</s>
</p>
<p>
</p>
<p>
<s>
7	[number]	k4	7
</s>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
j	j	k?	j
</s>
</p>
<p>
</p>
<p>
<s>
/	/	kIx~	/
</s>
</p>
<p>
</p>
<p>
<s>
6	[number]	k4	6
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
C	C	kA	C
</s>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
<s>
9	[number]	k4	9
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
C	C	kA	C
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
6	[number]	k4	6
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
C	C	kA	C
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
7	[number]	k4	7
<g/>
maj	maj	k?	maj
<g/>
/	/	kIx~	/
<g/>
6	[number]	k4	6
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
C	C	kA	C
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
add	add	k?	add
<g/>
9	[number]	k4	9
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Modus	modus	k1gInSc1	modus
</s>
</p>
<p>
<s>
Tónika	tónika	k1gFnSc1	tónika
</s>
</p>
<p>
<s>
Mixolydický	Mixolydický	k2eAgInSc1d1	Mixolydický
modus	modus	k1gInSc1	modus
</s>
</p>
<p>
<s>
Lydický	Lydický	k2eAgInSc1d1	Lydický
modus	modus	k1gInSc1	modus
</s>
</p>
<p>
<s>
Durová	durový	k2eAgFnSc1d1	durová
stupnice	stupnice	k1gFnSc1	stupnice
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Kytarové	kytarový	k2eAgInPc1d1	kytarový
prstoklady	prstoklad	k1gInPc1	prstoklad
jónského	jónský	k2eAgInSc2d1	jónský
modu	modus	k1gInSc2	modus
</s>
</p>
