<s>
Usta	Usta	k6eAd1
</s>
<s>
Možná	možná	k9
hledáte	hledat	k5eAaImIp2nP
<g/>
:	:	kIx,
Ústa	ústa	k1gNnPc1
(	(	kIx(
<g/>
počátek	počátek	k1gInSc1
trávicí	trávicí	k2eAgFnSc2d1
soustavy	soustava	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
USTA	USTA	kA
(	(	kIx(
<g/>
Americkou	americký	k2eAgFnSc4d1
tenisovou	tenisový	k2eAgFnSc4d1
asociaci	asociace	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Usta	Ust	k2eAgFnSc1d1
Krajina	Krajina	k1gFnSc1
podél	podél	k7c2
středního	střední	k2eAgNnSc2d1
tokuZákladní	tokuZákladný	k2eAgMnPc1d1
informace	informace	k1gFnSc1
Délka	délka	k1gFnSc1
toku	tok	k1gInSc2
</s>
<s>
253	#num#	k4
km	km	kA
Plocha	plocha	k1gFnSc1
povodí	povodit	k5eAaPmIp3nS
</s>
<s>
6	#num#	k4
030	#num#	k4
km²	km²	k?
Průměrný	průměrný	k2eAgInSc1d1
průtok	průtok	k1gInSc1
</s>
<s>
(	(	kIx(
<g/>
47	#num#	k4
km	km	kA
od	od	k7c2
ústí	ústí	k1gNnSc2
<g/>
)	)	kIx)
28	#num#	k4
m³	m³	k?
<g/>
/	/	kIx~
<g/>
s	s	k7c7
Světadíl	světadíl	k1gInSc1
</s>
<s>
Evropa	Evropa	k1gFnSc1
Pramen	pramen	k1gInSc1
</s>
<s>
57	#num#	k4
<g/>
°	°	k?
<g/>
30	#num#	k4
<g/>
′	′	k?
<g/>
27	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
46	#num#	k4
<g/>
°	°	k?
<g/>
59	#num#	k4
<g/>
′	′	k?
<g/>
48	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Ústí	ústí	k1gNnSc1
</s>
<s>
do	do	k7c2
Vetlugy	Vetluga	k1gFnSc2
56	#num#	k4
<g/>
°	°	k?
<g/>
53	#num#	k4
<g/>
′	′	k?
<g/>
0,96	0,96	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
45	#num#	k4
<g/>
°	°	k?
<g/>
26	#num#	k4
<g/>
′	′	k?
<g/>
29,34	29,34	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Protéká	protékat	k5eAaImIp3nS
</s>
<s>
Rusko	Rusko	k1gNnSc1
Rusko	Rusko	k1gNnSc1
(	(	kIx(
<g/>
Kirovská	Kirovská	k1gFnSc1
<g/>
,	,	kIx,
Nižněnovgorodská	Nižněnovgorodský	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
<g/>
)	)	kIx)
Úmoří	úmoří	k1gNnSc1
<g/>
,	,	kIx,
povodí	povodí	k1gNnSc1
</s>
<s>
bezodtoká	bezodtoký	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
<g/>
,	,	kIx,
Kaspické	kaspický	k2eAgNnSc1d1
moře	moře	k1gNnSc1
<g/>
,	,	kIx,
Volha	Volha	k1gFnSc1
<g/>
,	,	kIx,
Vetluga	Vetluga	k1gFnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Usta	Usta	k1gFnSc1
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
У	У	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
řeka	řeka	k1gFnSc1
v	v	k7c6
Nižněnovgorodské	Nižněnovgorodský	k2eAgFnSc6d1
a	a	k8xC
v	v	k7c6
Kirovské	Kirovský	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
v	v	k7c6
Rusku	Rusko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
253	#num#	k4
km	km	kA
dlouhá	dlouhý	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Povodí	povodí	k1gNnSc1
má	mít	k5eAaImIp3nS
rozlohu	rozloha	k1gFnSc4
6	#num#	k4
030	#num#	k4
km²	km²	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Průběh	průběh	k1gInSc1
toku	tok	k1gInSc2
</s>
<s>
Ústí	ústí	k1gNnSc1
zleva	zleva	k6eAd1
do	do	k7c2
Vetlugy	Vetluga	k1gFnSc2
(	(	kIx(
<g/>
povodí	povodí	k1gNnSc2
Volhy	Volha	k1gFnSc2
<g/>
)	)	kIx)
na	na	k7c6
169	#num#	k4
říčním	říční	k2eAgInSc6d1
kilometru	kilometr	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vodní	vodní	k2eAgInSc1d1
režim	režim	k1gInSc1
</s>
<s>
Zdrojem	zdroj	k1gInSc7
vody	voda	k1gFnSc2
jsou	být	k5eAaImIp3nP
převážně	převážně	k6eAd1
sněhové	sněhový	k2eAgFnPc1d1
srážky	srážka	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průměrný	průměrný	k2eAgInSc1d1
průtok	průtok	k1gInSc1
vody	voda	k1gFnSc2
ve	v	k7c6
vzdálenosti	vzdálenost	k1gFnSc6
47	#num#	k4
km	km	kA
od	od	k7c2
ústí	ústí	k1gNnSc2
činí	činit	k5eAaImIp3nS
28	#num#	k4
m³	m³	k?
<g/>
/	/	kIx~
<g/>
s.	s.	k?
Zamrzá	zamrzat	k5eAaImIp3nS
v	v	k7c6
listopadu	listopad	k1gInSc6
a	a	k8xC
rozmrzá	rozmrzat	k5eAaImIp3nS
v	v	k7c6
dubnu	duben	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Využití	využití	k1gNnSc1
</s>
<s>
Řeka	řeka	k1gFnSc1
je	být	k5eAaImIp3nS
splavná	splavný	k2eAgFnSc1d1
pro	pro	k7c4
vodáky	vodák	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
dolním	dolní	k2eAgInSc6d1
toku	tok	k1gInSc6
je	být	k5eAaImIp3nS
možná	možný	k2eAgFnSc1d1
vodní	vodní	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
</s>
<s>
Registr	registr	k1gInSc1
vodních	vodní	k2eAgInPc2d1
toků	tok	k1gInPc2
Ruské	ruský	k2eAgFnSc2d1
federace	federace	k1gFnSc2
–	–	k?
Usta	Usta	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byly	být	k5eAaImAgFnP
použity	použit	k2eAgFnPc1d1
informace	informace	k1gFnPc1
z	z	k7c2
Velké	velký	k2eAgFnSc2d1
sovětské	sovětský	k2eAgFnSc2d1
encyklopedie	encyklopedie	k1gFnSc2
<g/>
,	,	kIx,
heslo	heslo	k1gNnSc1
„	„	k?
<g/>
У	У	k?
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Geografie	geografie	k1gFnSc1
|	|	kIx~
Rusko	Rusko	k1gNnSc1
</s>
