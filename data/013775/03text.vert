<s>
Šiprúň	Šiprúň	k1gFnSc1
</s>
<s>
Šiprúň	Šiprúň	k1gFnSc1
Šiprúň	Šiprúň	k1gFnSc1
</s>
<s>
Vrchol	vrchol	k1gInSc1
</s>
<s>
1461	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Poloha	poloha	k1gFnSc1
Světadíl	světadíl	k1gInSc1
</s>
<s>
Evropa	Evropa	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Slovensko	Slovensko	k1gNnSc1
Slovensko	Slovensko	k1gNnSc4
Pohoří	pohořet	k5eAaPmIp3nS
</s>
<s>
Velká	velký	k2eAgFnSc1d1
Fatra	Fatra	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
1	#num#	k4
<g/>
′	′	k?
<g/>
26	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
19	#num#	k4
<g/>
°	°	k?
<g/>
13	#num#	k4
<g/>
′	′	k?
<g/>
42	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Šiprúň	Šiprúň	k1gFnSc1
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Šiprúň	Šiprúň	k1gFnSc1
(	(	kIx(
<g/>
1461	#num#	k4
m	m	kA
n.	n.	kA
m.	m.	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
dvouvrcholová	dvouvrcholový	k2eAgFnSc1d1
hora	hora	k1gFnSc1
ve	v	k7c6
Velké	velký	k2eAgFnSc6d1
Fatře	Fatra	k1gFnSc6
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nižší	nízký	k2eAgInSc1d2
západní	západní	k2eAgInSc1d1
vrchol	vrchol	k1gInSc1
(	(	kIx(
<g/>
1443	#num#	k4
m	m	kA
<g/>
)	)	kIx)
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
Liptovské	liptovský	k2eAgFnSc6d1
větvi	větev	k1gFnSc6
hlavního	hlavní	k2eAgInSc2d1
hřebene	hřeben	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
v	v	k7c6
těchto	tento	k3xDgNnPc6
místech	místo	k1gNnPc6
dělí	dělit	k5eAaImIp3nS
na	na	k7c4
další	další	k2eAgNnPc4d1
dvě	dva	k4xCgNnPc4
ramena	rameno	k1gNnPc4
(	(	kIx(
<g/>
oddělena	oddělit	k5eAaPmNgNnP
dolinou	dolina	k1gFnSc7
Čutkovo	Čutkův	k2eAgNnSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyšší	vysoký	k2eAgInSc1d2
vrchol	vrchol	k1gInSc1
(	(	kIx(
<g/>
1461	#num#	k4
m	m	kA
<g/>
)	)	kIx)
leží	ležet	k5eAaImIp3nS
ve	v	k7c6
východní	východní	k2eAgFnSc6d1
rozsoše	rozsocha	k1gFnSc6
nižšího	nízký	k2eAgInSc2d2
vrcholu	vrchol	k1gInSc2
<g/>
,	,	kIx,
od	od	k7c2
něhož	jenž	k3xRgInSc2
je	být	k5eAaImIp3nS
oddělen	oddělit	k5eAaPmNgInS
Vyšným	vyšný	k2eAgInSc7d1
Šiprúnskym	Šiprúnskym	k1gInSc1
sedlem	sedlo	k1gNnSc7
(	(	kIx(
<g/>
1385	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozsocha	rozsocha	k1gFnSc1
dále	daleko	k6eAd2
pokračuje	pokračovat	k5eAaImIp3nS
k	k	k7c3
vrcholu	vrchol	k1gInSc2
Pulčíkovo	Pulčíkův	k2eAgNnSc1d1
(	(	kIx(
<g/>
1238	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
za	za	k7c7
nímž	jenž	k3xRgMnSc7
klesá	klesat	k5eAaImIp3nS
do	do	k7c2
údolí	údolí	k1gNnSc2
Revúce	Revúce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyšší	vysoký	k2eAgInSc1d2
vrchol	vrchol	k1gInSc1
poskytuje	poskytovat	k5eAaImIp3nS
dobré	dobrý	k2eAgInPc4d1
výhledy	výhled	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Přístup	přístup	k1gInSc1
</s>
<s>
po	po	k7c6
červené	červený	k2eAgFnSc6d1
značce	značka	k1gFnSc6
z	z	k7c2
Vyšného	vyšný	k2eAgMnSc2d1
Šiprúnskeho	Šiprúnske	k1gMnSc2
sedla	sedlo	k1gNnSc2
(	(	kIx(
<g/>
na	na	k7c4
vyšší	vysoký	k2eAgInSc4d2
vrchol	vrchol	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
vrcholů	vrchol	k1gInPc2
ve	v	k7c6
Velké	velký	k2eAgFnSc6d1
Fatře	Fatra	k1gFnSc6
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Nejvyšší	vysoký	k2eAgInPc4d3
vrcholy	vrchol	k1gInPc4
Velké	velký	k2eAgFnSc2d1
Fatry	Fatra	k1gFnSc2
na	na	k7c4
Treking	Treking	k1gInSc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Šiprúň	Šiprúň	k1gFnSc1
na	na	k7c6
Turistika	turistika	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Výstup	výstup	k1gInSc1
na	na	k7c4
Šiprúň	Šiprúň	k1gFnSc4
na	na	k7c4
Treking	Treking	k1gInSc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Velká	velký	k2eAgFnSc1d1
Fatra	Fatra	k1gFnSc1
Geomorfologické	geomorfologický	k2eAgFnSc2d1
podcelky	podcelka	k1gFnSc2
</s>
<s>
Bralná	Bralný	k2eAgFnSc1d1
Fatra	Fatra	k1gFnSc1
•	•	k?
Hôľná	Hôľný	k2eAgFnSc1d1
Fatra	Fatra	k1gFnSc1
•	•	k?
Lysec	lysec	k1gMnSc1
•	•	k?
Revúcké	Revúcký	k2eAgFnSc2d1
podolie	podolie	k1gFnSc2
•	•	k?
Šiprúň	Šiprúň	k1gFnSc4
•	•	k?
Šípská	Šípský	k2eAgFnSc1d1
Fatra	Fatra	k1gFnSc1
•	•	k?
Zvolen	Zvolen	k1gInSc1
Velkoplošné	velkoplošný	k2eAgInPc1d1
chráněné	chráněný	k2eAgInPc4d1
území	území	k1gNnSc6
</s>
<s>
Národní	národní	k2eAgInSc1d1
park	park	k1gInSc1
Velká	velký	k2eAgFnSc1d1
Fatra	Fatra	k1gFnSc1
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Borišov	Borišov	k1gInSc1
•	•	k?
Čierny	Čierna	k1gFnSc2
kameň	kamenit	k5eAaImRp2nS
•	•	k?
Harmanecká	Harmanecký	k2eAgFnSc1d1
tisina	tisina	k1gFnSc1
•	•	k?
Jánošíkova	Jánošíkův	k2eAgFnSc1d1
kolkáreň	kolkáreň	k1gFnSc1
•	•	k?
Kornietová	Kornietový	k2eAgFnSc1d1
•	•	k?
Kundračka	Kundračka	k1gFnSc1
•	•	k?
Lysec	lysec	k1gMnSc1
•	•	k?
Madačov	Madačov	k1gInSc1
•	•	k?
Padva	Padva	k1gFnSc1
•	•	k?
Rumbáre	Rumbár	k1gInSc5
•	•	k?
Skalná	skalný	k2eAgFnSc1d1
Alpa	alpa	k1gFnSc1
•	•	k?
Suchý	suchý	k2eAgInSc1d1
vrch	vrch	k1gInSc1
•	•	k?
Šíp	šíp	k1gInSc1
•	•	k?
Tlstá	Tlstý	k2eAgFnSc1d1
•	•	k?
Velká	velký	k2eAgFnSc1d1
Skalná	skalný	k2eAgFnSc1d1
Přírodní	přírodní	k2eAgFnSc1d1
rezervace	rezervace	k1gFnSc1
</s>
<s>
Biela	Biela	k1gFnSc1
skala	skát	k5eAaImAgFnS
•	•	k?
Katova	katův	k2eAgFnSc1d1
skala	skát	k5eAaImAgFnS
•	•	k?
Korbeľka	Korbeľka	k1gFnSc1
•	•	k?
Močiar	Močiar	k1gInSc1
•	•	k?
Rojkovská	Rojkovský	k2eAgFnSc1d1
travertínová	travertínový	k2eAgFnSc1d1
kopa	kopa	k1gFnSc1
•	•	k?
Rojkovské	Rojkovský	k2eAgNnSc4d1
rašelinisko	rašelinisko	k1gNnSc4
•	•	k?
Smrekovica	Smrekovica	k1gFnSc1
Přírodní	přírodní	k2eAgFnPc1d1
památky	památka	k1gFnPc1
</s>
<s>
Blatné	blatný	k2eAgFnPc1d1
•	•	k?
Dogerské	Dogerský	k2eAgFnPc1d1
skaly	skát	k5eAaImAgFnP
•	•	k?
Horná	Horný	k2eAgFnSc1d1
Túfna	Túfna	k1gFnSc1
a	a	k8xC
Dolná	Dolný	k2eAgFnSc1d1
Túfna	Túfna	k1gFnSc1
•	•	k?
Jazierske	Jazierske	k1gFnSc1
travertíny	travertína	k1gFnSc2
•	•	k?
Krkavá	krkavý	k2eAgFnSc1d1
skala	skát	k5eAaImAgFnS
•	•	k?
Majerova	Majerův	k2eAgFnSc1d1
skala	skát	k5eAaImAgFnS
•	•	k?
Matejkovský	Matejkovský	k2eAgMnSc1d1
kamenný	kamenný	k2eAgMnSc1d1
prúd	prúd	k1gMnSc1
•	•	k?
Mažarná	Mažarný	k2eAgFnSc1d1
•	•	k?
Prielom	Prielom	k1gInSc4
Teplého	Teplého	k2eAgInSc2d1
potoka	potok	k1gInSc2
•	•	k?
Vlčia	Vlčia	k1gFnSc1
skala	skát	k5eAaImAgFnS
Evropsky	evropsky	k6eAd1
významné	významný	k2eAgNnSc4d1
území	území	k1gNnSc4
</s>
<s>
Močiar	Močiar	k1gInSc1
Chráněný	chráněný	k2eAgInSc4d1
areál	areál	k1gInSc4
</s>
<s>
Dekretov	Dekretov	k1gInSc1
porast	porast	k1gInSc1
•	•	k?
Háj	háj	k1gInSc1
pred	pred	k6eAd1
Teplou	teplý	k2eAgFnSc7d1
dolinou	dolina	k1gFnSc7
•	•	k?
Jazernické	Jazernický	k2eAgFnSc2d1
jazierko	jazierka	k1gFnSc5
•	•	k?
Krásno	krásno	k1gNnSc4
•	•	k?
Revúca	Revúca	k1gMnSc1
•	•	k?
Žarnovica	Žarnovica	k1gMnSc1
Lokality	lokalita	k1gFnSc2
Světového	světový	k2eAgNnSc2d1
dědictví	dědictví	k1gNnSc2
UNESCO	UNESCO	kA
</s>
<s>
Vlkolínec	Vlkolínec	k1gMnSc1
Slovenské	slovenský	k2eAgFnSc2d1
kulturní	kulturní	k2eAgFnSc2d1
a	a	k8xC
historické	historický	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Blatnický	Blatnický	k2eAgInSc1d1
hrad	hrad	k1gInSc1
•	•	k?
Sklabinský	Sklabinský	k2eAgInSc1d1
hrad	hrad	k1gInSc1
•	•	k?
Sučanský	Sučanský	k2eAgInSc1d1
hrad	hrad	k1gInSc1
•	•	k?
Vlkolínec	Vlkolínec	k1gInSc1
Vybrané	vybraný	k2eAgInPc1d1
vrcholy	vrchol	k1gInPc1
Velké	velký	k2eAgFnSc2d1
Fatry	Fatra	k1gFnSc2
</s>
<s>
Ostredok	Ostredok	k1gInSc1
(	(	kIx(
<g/>
1596	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
•	•	k?
Frčkov	Frčkov	k1gInSc1
(	(	kIx(
<g/>
1585	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
•	•	k?
Krížna	Krížna	k1gFnSc1
(	(	kIx(
<g/>
1574	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
•	•	k?
Rakytov	Rakytov	k1gInSc1
(	(	kIx(
<g/>
1567	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
•	•	k?
Suchý	suchý	k2eAgInSc4d1
vrch	vrch	k1gInSc4
(	(	kIx(
<g/>
1550	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Ploská	ploský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1532	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
•	•	k?
Smrekovica	Smrekovica	k1gFnSc1
(	(	kIx(
<g/>
1560	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
•	•	k?
Borišov	Borišov	k1gInSc1
(	(	kIx(
<g/>
1510	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
•	•	k?
Malá	malý	k2eAgFnSc1d1
Smrekovica	Smrekovica	k1gFnSc1
(	(	kIx(
<g/>
1485	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
•	•	k?
Čierny	Čierna	k1gFnSc2
kameň	kamenit	k5eAaImRp2nS
(	(	kIx(
<g/>
1479	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
•	•	k?
Skalná	skalný	k2eAgFnSc1d1
Alpa	alpa	k1gFnSc1
(	(	kIx(
<g/>
1463	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
•	•	k?
Šiprúň	Šiprúň	k1gFnSc1
(	(	kIx(
<g/>
1461	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
•	•	k?
Smrekov	Smrekov	k1gInSc1
(	(	kIx(
<g/>
1441	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
•	•	k?
Lubená	Lubená	k1gFnSc1
(	(	kIx(
<g/>
1414	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
•	•	k?
Zvolen	Zvolen	k1gInSc1
(	(	kIx(
<g/>
1403	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
•	•	k?
Minčol	Minčol	k1gInSc1
(	(	kIx(
<g/>
1398	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
•	•	k?
Kračkov	Kračkov	k1gInSc1
(	(	kIx(
<g/>
1398	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
•	•	k?
Kľak	Kľak	k1gInSc1
(	(	kIx(
<g/>
1394	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
•	•	k?
Biela	Biela	k1gFnSc1
skala	skát	k5eAaImAgFnS
(	(	kIx(
<g/>
1385	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
•	•	k?
Kráľova	Kráľův	k2eAgFnSc1d1
studňa	studňa	k1gFnSc1
(	(	kIx(
<g/>
1384	#num#	k4
m	m	kA
<g />
.	.	kIx.
</s>
<s hack="1">
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
•	•	k?
Lysec	lysec	k1gMnSc1
(	(	kIx(
<g/>
1381	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
•	•	k?
Kráľova	Kráľovo	k1gNnSc2
skala	skát	k5eAaImAgFnS
(	(	kIx(
<g/>
1377	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
•	•	k?
Tlstá	Tlstý	k2eAgFnSc1d1
(	(	kIx(
<g/>
1373	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
•	•	k?
Chyžky	chyžka	k1gFnSc2
(	(	kIx(
<g/>
1342	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
•	•	k?
Čierna	Čierna	k1gFnSc1
hora	hora	k1gFnSc1
(	(	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
1335	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
•	•	k?
Javorina	Javorina	k1gFnSc1
(	(	kIx(
<g/>
1328	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
•	•	k?
Jarabiná	Jarabiná	k1gFnSc1
(	(	kIx(
<g/>
1314	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
•	•	k?
Magura	Magura	k1gFnSc1
(	(	kIx(
<g/>
1309	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
•	•	k?
Štefanová	Štefanová	k1gFnSc1
(	(	kIx(
<g/>
1300	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
•	•	k?
Majerova	Majerův	k2eAgMnSc4d1
skala	skát	k5eAaImAgFnS
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
1283	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
•	•	k?
Drienok	Drienok	k1gInSc1
(	(	kIx(
<g/>
1267	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
•	•	k?
Ostrá	ostrý	k2eAgFnSc1d1
(	(	kIx(
<g/>
1263	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
•	•	k?
Malinné	Malinný	k2eAgNnSc1d1
(	(	kIx(
<g/>
1209	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
•	•	k?
Tlstá	Tlstý	k2eAgFnSc1d1
hora	hora	k1gFnSc1
(	(	kIx(
<g/>
1208	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
•	•	k?
Šíp	šíp	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
1169	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
•	•	k?
Japeň	Japeň	k1gFnSc1
(	(	kIx(
<g/>
Velká	velký	k2eAgFnSc1d1
Fatra	Fatra	k1gFnSc1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
1154	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
•	•	k?
Kečky	kečka	k1gFnSc2
(	(	kIx(
<g/>
1138	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
•	•	k?
Sidorovo	Sidorův	k2eAgNnSc1d1
(	(	kIx(
<g/>
1099	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
•	•	k?
Šturec	Šturec	k1gInSc1
(	(	kIx(
<g/>
1075	#num#	k4
m	m	kA
<g />
.	.	kIx.
</s>
<s hack="1">
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
•	•	k?
Ostré	ostrý	k2eAgNnSc1d1
(	(	kIx(
<g/>
1066	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
•	•	k?
Zadný	Zadný	k1gMnSc1
Japeň	Japeň	k1gFnSc4
(	(	kIx(
<g/>
1065	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
•	•	k?
Magura	Magura	k1gFnSc1
(	(	kIx(
<g/>
1059	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
•	•	k?
Čebrať	Čebrať	k1gFnSc1
(	(	kIx(
<g/>
1054	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
•	•	k?
Lučenec	Lučenec	k1gInSc1
(	(	kIx(
<g/>
1041	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
Sedla	sedlo	k1gNnPc1
Velké	velký	k2eAgFnSc2d1
Fatry	Fatra	k1gFnSc2
</s>
<s>
Brestová	Brestová	k1gFnSc1
•	•	k?
Ľubochnianské	Ľubochnianský	k2eAgNnSc1d1
•	•	k?
Malý	malý	k2eAgMnSc1d1
Šturec	Šturec	k1gMnSc1
•	•	k?
Nižné	nižný	k2eAgInPc4d1
Šiprúňské	Šiprúňský	k2eAgInPc4d1
•	•	k?
Ploské	ploský	k2eAgNnSc1d1
•	•	k?
pod	pod	k7c7
Červeným	červené	k1gNnSc7
grúňom	grúňom	k1gInSc1
•	•	k?
pod	pod	k7c4
Smrekovom	Smrekovom	k1gInSc4
•	•	k?
Prašnické	Prašnický	k2eAgFnSc2d1
•	•	k?
Prašnické	Prašnický	k2eAgFnPc4d1
(	(	kIx(
<g/>
východní	východní	k2eAgFnPc4d1
<g/>
)	)	kIx)
•	•	k?
Príslop	Príslop	k1gInSc1
•	•	k?
Rakytovské	Rakytovský	k2eAgInPc1d1
(	(	kIx(
<g/>
jižní	jižní	k2eAgInPc1d1
<g/>
)	)	kIx)
•	•	k?
Rakytovské	Rakytovský	k2eAgFnPc4d1
(	(	kIx(
<g/>
severní	severní	k2eAgFnPc4d1
<g/>
)	)	kIx)
•	•	k?
Rybovské	Rybovská	k1gFnSc2
•	•	k?
Veľký	Veľký	k2eAgMnSc1d1
Šturec	Šturec	k1gMnSc1
•	•	k?
Vyšné	vyšný	k2eAgNnSc1d1
Šiprúňske	Šiprúňske	k1gNnSc1
•	•	k?
za	za	k7c4
Drieňkom	Drieňkom	k1gInSc4
Jezera	jezero	k1gNnSc2
Velké	velký	k2eAgFnSc2d1
Fatry	Fatra	k1gFnSc2
</s>
<s>
Blatné	blatný	k2eAgFnPc1d1
Horské	Horské	k2eAgFnPc1d1
chaty	chata	k1gFnPc1
Velké	velký	k2eAgFnSc2d1
Fatry	Fatra	k1gFnSc2
</s>
<s>
Chata	chata	k1gFnSc1
pod	pod	k7c7
Borišovom	Borišovom	k1gInSc1
•	•	k?
Horský	horský	k2eAgInSc1d1
hotel	hotel	k1gInSc1
Kráľova	Kráľův	k2eAgInSc2d1
Studňa	Studň	k1gInSc2
Údolí	údolí	k1gNnSc2
na	na	k7c6
Velké	velký	k2eAgFnSc6d1
Fatře	Fatra	k1gFnSc6
</s>
<s>
Belianska	Beliansko	k1gNnSc2
dolina	dolina	k1gFnSc1
•	•	k?
Blatnická	Blatnický	k2eAgFnSc1d1
dolina	dolina	k1gFnSc1
•	•	k?
Blatná	blatný	k2eAgFnSc1d1
dolina	dolina	k1gFnSc1
•	•	k?
Bystrická	bystrický	k2eAgFnSc1d1
dolina	dolina	k1gFnSc1
•	•	k?
Čutkovská	Čutkovský	k2eAgFnSc1d1
dolina	dolina	k1gFnSc1
•	•	k?
Dedošová	Dedošový	k2eAgFnSc1d1
dolina	dolina	k1gFnSc1
•	•	k?
Gaderská	Gaderský	k2eAgFnSc1d1
dolina	dolina	k1gFnSc1
•	•	k?
Harmanecká	Harmanecký	k2eAgFnSc1d1
dolina	dolina	k1gFnSc1
•	•	k?
Hlavná	Hlavný	k2eAgFnSc1d1
dolina	dolina	k1gFnSc1
•	•	k?
Hornojasenská	Hornojasenský	k2eAgFnSc1d1
dolina	dolina	k1gFnSc1
•	•	k?
Kantorská	kantorský	k2eAgFnSc1d1
dolina	dolina	k1gFnSc1
•	•	k?
Komjatnianska	Komjatniansko	k1gNnSc2
dolina	dolina	k1gFnSc1
•	•	k?
Konský	Konský	k2eAgInSc4d1
dol	dol	k1gInSc4
•	•	k?
Ľubochnianska	Ľubochniansko	k1gNnSc2
dolina	dolina	k1gFnSc1
•	•	k?
Mača	Mač	k1gInSc2
•	•	k?
Necpalská	Necpalský	k2eAgFnSc1d1
dolina	dolina	k1gFnSc1
•	•	k?
Nižné	nižný	k2eAgNnSc4d1
Matejkovo	Matejkův	k2eAgNnSc4d1
•	•	k?
Rakytovská	Rakytovský	k2eAgFnSc1d1
<g />
.	.	kIx.
</s>
<s hack="1">
dolina	dolina	k1gFnSc1
•	•	k?
Revúcka	Revúcka	k1gFnSc1
dolina	dolina	k1gFnSc1
•	•	k?
Rožková	Rožkový	k2eAgFnSc1d1
dolina	dolina	k1gFnSc1
•	•	k?
Selenec	Selenec	k1gMnSc1
•	•	k?
Skalné	skalný	k2eAgNnSc1d1
•	•	k?
Sklabinská	Sklabinský	k2eAgFnSc1d1
dolina	dolina	k1gFnSc1
•	•	k?
Suchá	suchý	k2eAgFnSc1d1
dolina	dolina	k1gFnSc1
•	•	k?
Teplá	teplý	k2eAgFnSc1d1
dolina	dolina	k1gFnSc1
•	•	k?
Trlenská	Trlenský	k2eAgFnSc1d1
dolina	dolina	k1gFnSc1
•	•	k?
dolina	dolina	k1gFnSc1
Túfna	Túfn	k1gInSc2
•	•	k?
Vápenná	vápenný	k2eAgFnSc1d1
dolina	dolina	k1gFnSc1
•	•	k?
Veľká	Veľký	k2eAgFnSc1d1
dolina	dolina	k1gFnSc1
•	•	k?
Vyšné	vyšný	k2eAgNnSc1d1
Matejkovo	Matejkův	k2eAgNnSc1d1
•	•	k?
Zalámaná	zalámaný	k2eAgFnSc1d1
dolina	dolina	k1gFnSc1
•	•	k?
Zelená	zelený	k2eAgFnSc1d1
dolina	dolina	k1gFnSc1
•	•	k?
Žarnovická	Žarnovický	k2eAgFnSc1d1
dolina	dolina	k1gFnSc1
•	•	k?
Turistické	turistický	k2eAgFnPc4d1
značené	značený	k2eAgFnPc4d1
trasy	trasa	k1gFnPc4
ve	v	k7c6
Velké	velký	k2eAgFnSc6d1
Fatře	Fatra	k1gFnSc6
</s>
<s>
0801	#num#	k4
(	(	kIx(
<g/>
Cesta	cesta	k1gFnSc1
hrdinů	hrdina	k1gMnPc2
SNP	SNP	kA
<g/>
)	)	kIx)
•	•	k?
0853	#num#	k4
•	•	k?
0856	#num#	k4
•	•	k?
0857	#num#	k4
•	•	k?
0858	#num#	k4
•	•	k?
0870	#num#	k4
(	(	kIx(
<g/>
Velkofatranská	Velkofatranský	k2eAgFnSc1d1
magistrála	magistrála	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
0871	#num#	k4
•	•	k?
5432	#num#	k4
•	•	k?
5435	#num#	k4
•	•	k?
5570	#num#	k4
•	•	k?
5600	#num#	k4
•	•	k?
5603	#num#	k4
•	•	k?
5628	#num#	k4
•	•	k?
5630	#num#	k4
•	•	k?
5634	#num#	k4
•	•	k?
5635	#num#	k4
•	•	k?
5639	#num#	k4
•	•	k?
5641	#num#	k4
•	•	k?
5649	#num#	k4
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
2628	#num#	k4
•	•	k?
2704	#num#	k4
•	•	k?
2705	#num#	k4
•	•	k?
2718	#num#	k4
•	•	k?
2719	#num#	k4
•	•	k?
2720	#num#	k4
•	•	k?
2721	#num#	k4
•	•	k?
2722	#num#	k4
•	•	k?
2724	#num#	k4
•	•	k?
2731	#num#	k4
•	•	k?
2732	#num#	k4
•	•	k?
2733	#num#	k4
•	•	k?
8435	#num#	k4
•	•	k?
8436	#num#	k4
•	•	k?
8452	#num#	k4
•	•	k?
8454	#num#	k4
•	•	k?
8601	#num#	k4
•	•	k?
8602	#num#	k4
•	•	k?
8603	#num#	k4
•	•	k?
8604	#num#	k4
•	•	k?
8605	#num#	k4
•	•	k?
8607	#num#	k4
•	•	k?
8611	#num#	k4
•	•	k?
8612	#num#	k4
•	•	k?
8613	#num#	k4
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
8614	#num#	k4
•	•	k?
8630	#num#	k4
•	•	k?
8631	#num#	k4
•	•	k?
8633	#num#	k4
•	•	k?
8636	#num#	k4
•	•	k?
8638	#num#	k4
•	•	k?
8639	#num#	k4
•	•	k?
8641	#num#	k4
•	•	k?
8643	#num#	k4
•	•	k?
8645	#num#	k4
•	•	k?
8646	#num#	k4
•	•	k?
8647	#num#	k4
•	•	k?
8648	#num#	k4
•	•	k?
8681	#num#	k4
•	•	k?
Naučná	naučný	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
Hubovský	Hubovský	k1gMnSc1
okruh	okruh	k1gInSc1
•	•	k?
Naučná	naučný	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
Gaderská	Gaderský	k2eAgFnSc1d1
dolina	dolina	k1gFnSc1
•	•	k?
Naučná	naučný	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
Ľubochňa	Ľubochňum	k1gNnSc2
–	–	k?
Ľubochnianska	Ľubochniansko	k1gNnSc2
dolina	dolina	k1gFnSc1
•	•	k?
Švošovská	Švošovský	k2eAgFnSc1d1
naučná	naučný	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
•	•	k?
Naučná	naučný	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
Vlkolínec	Vlkolínec	k1gInSc1
•	•	k?
Naučná	naučný	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
Čierny	Čierna	k1gFnSc2
kameň	kamenit	k5eAaImRp2nS
•	•	k?
Naučná	naučný	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
J.	J.	kA
D.	D.	kA
Matejovie	Matejovie	k1gFnSc1
•	•	k?
Naučná	naučný	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
Čutkovská	Čutkovský	k2eAgFnSc1d1
dolina	dolina	k1gFnSc1
•	•	k?
Naučná	naučný	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
Hrebeňom	Hrebeňom	k1gInSc4
Veľkej	Veľkej	k1gFnSc2
Fatry	Fatra	k1gFnSc2
•	•	k?
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Geografie	geografie	k1gFnSc1
|	|	kIx~
Slovensko	Slovensko	k1gNnSc1
</s>
