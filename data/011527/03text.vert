<p>
<s>
Toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
seznam	seznam	k1gInSc1	seznam
britských	britský	k2eAgMnPc2d1	britský
králů	král	k1gMnPc2	král
a	a	k8xC	a
vládnoucích	vládnoucí	k2eAgFnPc2d1	vládnoucí
královen	královna	k1gFnPc2	královna
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1707	[number]	k4	1707
se	se	k3xPyFc4	se
na	na	k7c6	na
základě	základ	k1gInSc6	základ
zákonů	zákon	k1gInPc2	zákon
o	o	k7c4	o
sjednocení	sjednocení	k1gNnSc4	sjednocení
Anglie	Anglie	k1gFnSc2	Anglie
a	a	k8xC	a
Skotsko	Skotsko	k1gNnSc1	Skotsko
spojily	spojit	k5eAaPmAgInP	spojit
do	do	k7c2	do
jednoho	jeden	k4xCgInSc2	jeden
suverénního	suverénní	k2eAgInSc2d1	suverénní
státu	stát	k1gInSc2	stát
–	–	k?	–
Království	království	k1gNnSc2	království
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Anna	Anna	k1gFnSc1	Anna
Stuartovna	Stuartovna	k1gFnSc1	Stuartovna
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
jeho	jeho	k3xOp3gFnSc7	jeho
první	první	k4xOgFnSc7	první
královnou	královna	k1gFnSc7	královna
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
zůstala	zůstat	k5eAaPmAgFnS	zůstat
královnou	královna	k1gFnSc7	královna
Irska	Irsko	k1gNnSc2	Irsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Stuartovci	Stuartovec	k1gMnPc5	Stuartovec
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Hannoverská	hannoverský	k2eAgFnSc1d1	hannoverská
dynastie	dynastie	k1gFnSc1	dynastie
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Dynastie	dynastie	k1gFnSc1	dynastie
Sachsen-Coburg-Gotha	Sachsen-Coburg-Gotha	k1gFnSc1	Sachsen-Coburg-Gotha
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
Windsorská	windsorský	k2eAgFnSc1d1	Windsorská
dynastie	dynastie	k1gFnSc1	dynastie
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Rodokmen	rodokmen	k1gInSc4	rodokmen
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
DAVIES	DAVIES	kA	DAVIES
<g/>
,	,	kIx,	,
Norman	Norman	k1gMnSc1	Norman
<g/>
.	.	kIx.	.
</s>
<s>
Ostrovy	ostrov	k1gInPc1	ostrov
:	:	kIx,	:
dějiny	dějiny	k1gFnPc1	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
BB	BB	kA	BB
art	art	k?	art
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
975	[number]	k4	975
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7341	[number]	k4	7341
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
90	[number]	k4	90
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MORGAN	morgan	k1gMnSc1	morgan
<g/>
,	,	kIx,	,
Kenneth	Kenneth	k1gMnSc1	Kenneth
O.	O.	kA	O.
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
656	[number]	k4	656
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
432	[number]	k4	432
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgFnPc4d1	související
stránky	stránka	k1gFnPc4	stránka
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
nejdéle	dlouho	k6eAd3	dlouho
vládnoucích	vládnoucí	k2eAgMnPc2d1	vládnoucí
britských	britský	k2eAgMnPc2d1	britský
panovníků	panovník	k1gMnPc2	panovník
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
vládnoucích	vládnoucí	k2eAgMnPc2d1	vládnoucí
britských	britský	k2eAgMnPc2d1	britský
panovníků	panovník	k1gMnPc2	panovník
podle	podle	k7c2	podle
délky	délka	k1gFnSc2	délka
života	život	k1gInSc2	život
</s>
</p>
<p>
<s>
Linie	linie	k1gFnSc1	linie
následnictví	následnictví	k1gNnSc2	následnictví
britského	britský	k2eAgInSc2d1	britský
trůnu	trůn	k1gInSc2	trůn
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
australských	australský	k2eAgMnPc2d1	australský
panovníků	panovník	k1gMnPc2	panovník
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
kanadských	kanadský	k2eAgMnPc2d1	kanadský
panovníků	panovník	k1gMnPc2	panovník
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
hannoverských	hannoverský	k2eAgMnPc2d1	hannoverský
panovníků	panovník	k1gMnPc2	panovník
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
novozélandských	novozélandský	k2eAgMnPc2d1	novozélandský
panovníků	panovník	k1gMnPc2	panovník
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Anglie	Anglie	k1gFnSc2	Anglie
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Skotska	Skotsko	k1gNnSc2	Skotsko
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Irska	Irsko	k1gNnSc2	Irsko
</s>
</p>
<p>
<s>
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
Irska	Irsko	k1gNnSc2	Irsko
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
</s>
</p>
<p>
<s>
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
</s>
</p>
<p>
<s>
Britské	britský	k2eAgNnSc1d1	Britské
impérium	impérium	k1gNnSc1	impérium
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
filmů	film	k1gInPc2	film
o	o	k7c6	o
britských	britský	k2eAgMnPc6d1	britský
panovnících	panovník	k1gMnPc6	panovník
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Seznam	seznam	k1gInSc1	seznam
britských	britský	k2eAgMnPc2d1	britský
králů	král	k1gMnPc2	král
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
