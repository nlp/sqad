<p>
<s>
Darkwoods	Darkwoods	k6eAd1	Darkwoods
My	my	k3xPp1nPc1	my
Betrothed	Betrothed	k1gInSc4	Betrothed
je	být	k5eAaImIp3nS	být
finská	finský	k2eAgFnSc1d1	finská
black	black	k6eAd1	black
metalová	metalový	k2eAgFnSc1d1	metalová
kapela	kapela	k1gFnSc1	kapela
založená	založený	k2eAgFnSc1d1	založená
hudebníkem	hudebník	k1gMnSc7	hudebník
s	s	k7c7	s
pseudonymem	pseudonym	k1gInSc7	pseudonym
Emperor	Emperor	k1gInSc1	Emperor
Nattasett	Nattasetta	k1gFnPc2	Nattasetta
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1993	[number]	k4	1993
původně	původně	k6eAd1	původně
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Virgin	Virgin	k1gInSc1	Virgin
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Cunt	Cunta	k1gFnPc2	Cunta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
se	se	k3xPyFc4	se
přejmenovala	přejmenovat	k5eAaPmAgFnS	přejmenovat
na	na	k7c4	na
Darkwoods	Darkwoods	k1gInSc4	Darkwoods
My	my	k3xPp1nPc1	my
Betrothed	Betrothed	k1gInSc1	Betrothed
<g/>
.	.	kIx.	.
</s>
<s>
Čerpá	čerpat	k5eAaImIp3nS	čerpat
ze	z	k7c2	z
severské	severský	k2eAgFnSc2d1	severská
mytologie	mytologie	k1gFnSc2	mytologie
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
první	první	k4xOgNnSc1	první
demo	demo	k2eAgNnSc1d1	demo
Reborn	Reborn	k1gNnSc1	Reborn
In	In	k1gMnSc1	In
The	The	k1gMnSc1	The
Promethean	Promethean	k1gMnSc1	Promethean
Flame	Flam	k1gInSc5	Flam
(	(	kIx(	(
<g/>
ještě	ještě	k6eAd1	ještě
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Virgin	Virgin	k1gInSc1	Virgin
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Cunt	Cunta	k1gFnPc2	Cunta
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
první	první	k4xOgNnSc4	první
studiové	studiový	k2eAgNnSc4d1	studiové
album	album	k1gNnSc4	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
Heirs	Heirs	k1gInSc4	Heirs
of	of	k?	of
the	the	k?	the
Northstar	Northstar	k1gInSc1	Northstar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Demo	demo	k2eAgFnPc1d1	demo
nahrávky	nahrávka	k1gFnPc1	nahrávka
===	===	k?	===
</s>
</p>
<p>
<s>
Reborn	Reborn	k1gMnSc1	Reborn
In	In	k1gMnSc1	In
The	The	k1gMnSc1	The
Promethean	Promethean	k1gMnSc1	Promethean
Flame	Flam	k1gInSc5	Flam
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
–	–	k?	–
ještě	ještě	k6eAd1	ještě
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Virgin	Virgin	k1gInSc1	Virgin
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Cunt	Cunt	k1gInSc1	Cunt
</s>
</p>
<p>
<s>
Dark	Dark	k6eAd1	Dark
Aureoles	Aureoles	k1gInSc1	Aureoles
Gathering	Gathering	k1gInSc1	Gathering
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Studiová	studiový	k2eAgNnPc4d1	studiové
alba	album	k1gNnPc4	album
===	===	k?	===
</s>
</p>
<p>
<s>
Heirs	Heirs	k1gInSc1	Heirs
of	of	k?	of
the	the	k?	the
Northstar	Northstar	k1gInSc1	Northstar
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Autumn	Autumn	k1gMnSc1	Autumn
Roars	Roarsa	k1gFnPc2	Roarsa
Thunder	Thunder	k1gMnSc1	Thunder
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Witch-Hunts	Witch-Hunts	k1gInSc1	Witch-Hunts
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Kompilace	kompilace	k1gFnSc2	kompilace
===	===	k?	===
</s>
</p>
<p>
<s>
Dark	Dark	k6eAd1	Dark
Aureoles	Aureoles	k1gInSc1	Aureoles
Gathering	Gathering	k1gInSc1	Gathering
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
The	The	k?	The
Eerie	Eerie	k1gFnSc1	Eerie
Sampler	Sampler	k1gMnSc1	Sampler
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Split	Split	k1gInSc1	Split
nahrávky	nahrávka	k1gFnSc2	nahrávka
===	===	k?	===
</s>
</p>
<p>
<s>
Sacrament	Sacrament	k1gInSc1	Sacrament
of	of	k?	of
Wilderness	Wilderness	k1gInSc1	Wilderness
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
–	–	k?	–
split	split	k1gInSc1	split
s	s	k7c7	s
kapelami	kapela	k1gFnPc7	kapela
Nightwish	Nightwisha	k1gFnPc2	Nightwisha
a	a	k8xC	a
Eternal	Eternal	k1gFnPc2	Eternal
Tears	Tearsa	k1gFnPc2	Tearsa
of	of	k?	of
Sorrow	Sorrow	k1gMnSc1	Sorrow
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
blackmetalových	blackmetalův	k2eAgFnPc2d1	blackmetalův
skupin	skupina	k1gFnPc2	skupina
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Darkwoods	Darkwoods	k6eAd1	Darkwoods
My	my	k3xPp1nPc1	my
Betrothed	Betrothed	k1gInSc4	Betrothed
v	v	k7c6	v
databázi	databáze	k1gFnSc6	databáze
Encyclopaedia	Encyclopaedium	k1gNnSc2	Encyclopaedium
Metallum	Metallum	k1gInSc4	Metallum
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Darkwoods	Darkwoods	k6eAd1	Darkwoods
My	my	k3xPp1nPc1	my
Betrothed	Betrothed	k1gInSc1	Betrothed
<g/>
,	,	kIx,	,
Discogs	Discogs	k1gInSc1	Discogs
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Darkwoods	Darkwoods	k6eAd1	Darkwoods
My	my	k3xPp1nPc1	my
Betrothed	Betrothed	k1gMnSc1	Betrothed
<g/>
,	,	kIx,	,
Last	Last	k1gMnSc1	Last
<g/>
.	.	kIx.	.
<g/>
fm	fm	k?	fm
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Darkwoods	Darkwoods	k6eAd1	Darkwoods
My	my	k3xPp1nPc1	my
Betrothed	Betrothed	k1gInSc1	Betrothed
<g/>
,	,	kIx,	,
Myspace	Myspace	k1gFnSc1	Myspace
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
