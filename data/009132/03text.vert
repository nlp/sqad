<p>
<s>
Starkočský	Starkočský	k2eAgInSc1d1	Starkočský
potok	potok	k1gInSc1	potok
je	být	k5eAaImIp3nS	být
pravostranný	pravostranný	k2eAgInSc1d1	pravostranný
přítok	přítok	k1gInSc1	přítok
řeky	řeka	k1gFnSc2	řeka
Doubravy	Doubrava	k1gFnSc2	Doubrava
protékající	protékající	k2eAgInPc4d1	protékající
okresy	okres	k1gInPc4	okres
Kutná	kutný	k2eAgFnSc1d1	Kutná
Hora	hora	k1gFnSc1	hora
a	a	k8xC	a
Chrudim	Chrudim	k1gFnSc1	Chrudim
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
jeho	jeho	k3xOp3gInSc2	jeho
toku	tok	k1gInSc2	tok
činí	činit	k5eAaImIp3nS	činit
12,8	[number]	k4	12,8
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Plocha	plocha	k1gFnSc1	plocha
povodí	povodí	k1gNnSc2	povodí
měří	měřit	k5eAaImIp3nS	měřit
12,7	[number]	k4	12,7
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Průběh	průběh	k1gInSc1	průběh
toku	tok	k1gInSc2	tok
==	==	k?	==
</s>
</p>
<p>
<s>
Potok	potok	k1gInSc1	potok
pramení	pramenit	k5eAaImIp3nS	pramenit
v	v	k7c6	v
Sečské	sečský	k2eAgFnSc6d1	Sečská
vrchovině	vrchovina	k1gFnSc6	vrchovina
při	při	k7c6	při
okraji	okraj	k1gInSc6	okraj
lesa	les	k1gInSc2	les
mezi	mezi	k7c7	mezi
Licoměřicemi	Licoměřice	k1gFnPc7	Licoměřice
<g/>
,	,	kIx,	,
Jetonicemi	Jetonice	k1gFnPc7	Jetonice
a	a	k8xC	a
Zbyslavcem	Zbyslavec	k1gMnSc7	Zbyslavec
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
okolo	okolo	k7c2	okolo
485	[number]	k4	485
m.	m.	k?	m.
Nejprve	nejprve	k6eAd1	nejprve
jeho	jeho	k3xOp3gInSc1	jeho
tok	tok	k1gInSc1	tok
směřuje	směřovat	k5eAaImIp3nS	směřovat
lesnatým	lesnatý	k2eAgNnSc7d1	lesnaté
údolím	údolí	k1gNnSc7	údolí
krátce	krátce	k6eAd1	krátce
na	na	k7c4	na
sever	sever	k1gInSc4	sever
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
obrací	obracet	k5eAaImIp3nS	obracet
na	na	k7c4	na
západ	západ	k1gInSc4	západ
a	a	k8xC	a
teče	téct	k5eAaImIp3nS	téct
při	při	k7c6	při
rozhraní	rozhraní	k1gNnSc6	rozhraní
Sečské	sečský	k2eAgFnSc2d1	Sečská
vrchoviny	vrchovina	k1gFnSc2	vrchovina
a	a	k8xC	a
Chvaletické	chvaletický	k2eAgFnSc2d1	Chvaletická
pahorkatiny	pahorkatina	k1gFnSc2	pahorkatina
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
opuštění	opuštění	k1gNnSc6	opuštění
lesa	les	k1gInSc2	les
zhruba	zhruba	k6eAd1	zhruba
na	na	k7c6	na
desátém	desátý	k4xOgInSc6	desátý
říčním	říční	k2eAgInSc6d1	říční
kilometru	kilometr	k1gInSc6	kilometr
potok	potok	k1gInSc1	potok
teče	teč	k1gFnSc2	teč
na	na	k7c6	na
rozhraní	rozhraní	k1gNnSc6	rozhraní
Chvaletické	chvaletický	k2eAgFnSc2d1	Chvaletická
pahorkatiny	pahorkatina	k1gFnSc2	pahorkatina
a	a	k8xC	a
Čáslavské	Čáslavské	k2eAgFnSc2d1	Čáslavské
kotliny	kotlina	k1gFnSc2	kotlina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
úseku	úsek	k1gInSc6	úsek
napájí	napájet	k5eAaImIp3nS	napájet
menší	malý	k2eAgInSc1d2	menší
rybník	rybník	k1gInSc1	rybník
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
vsi	ves	k1gFnSc2	ves
Bílý	bílý	k2eAgInSc1d1	bílý
Kámen	kámen	k1gInSc1	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
hrází	hráz	k1gFnSc7	hráz
rybníka	rybník	k1gInSc2	rybník
vtéká	vtékat	k5eAaImIp3nS	vtékat
Starkočský	Starkočský	k2eAgInSc4d1	Starkočský
potok	potok	k1gInSc4	potok
do	do	k7c2	do
Čáslavké	Čáslavký	k2eAgFnSc2d1	Čáslavký
kotliny	kotlina	k1gFnSc2	kotlina
a	a	k8xC	a
směřuje	směřovat	k5eAaImIp3nS	směřovat
regulovaným	regulovaný	k2eAgNnSc7d1	regulované
korytem	koryto	k1gNnSc7	koryto
převážně	převážně	k6eAd1	převážně
na	na	k7c4	na
severozápad	severozápad	k1gInSc4	severozápad
k	k	k7c3	k
obci	obec	k1gFnSc3	obec
Starkoč	Starkoč	k1gFnSc2	Starkoč
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
obtéká	obtékat	k5eAaImIp3nS	obtékat
při	při	k7c6	při
jejím	její	k3xOp3gInSc6	její
západním	západní	k2eAgInSc6d1	západní
okraji	okraj	k1gInSc6	okraj
<g/>
.	.	kIx.	.
</s>
<s>
Severozápadní	severozápadní	k2eAgInSc4d1	severozápadní
směr	směr	k1gInSc4	směr
si	se	k3xPyFc3	se
potok	potok	k1gInSc4	potok
udržuje	udržovat	k5eAaImIp3nS	udržovat
až	až	k9	až
ke	k	k7c3	k
svému	svůj	k3xOyFgNnSc3	svůj
ústí	ústí	k1gNnSc3	ústí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dolním	dolní	k2eAgInSc6d1	dolní
toku	tok	k1gInSc6	tok
u	u	k7c2	u
Zbyslavi	Zbyslaev	k1gFnSc6	Zbyslaev
a	a	k8xC	a
Zaříčan	Zaříčan	k1gMnSc1	Zaříčan
teče	téct	k5eAaImIp3nS	téct
souběžně	souběžně	k6eAd1	souběžně
s	s	k7c7	s
řekou	řeka	k1gFnSc7	řeka
Doubravou	Doubrava	k1gFnSc7	Doubrava
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yIgFnSc2	který
se	se	k3xPyFc4	se
vlévá	vlévat	k5eAaImIp3nS	vlévat
na	na	k7c6	na
jejím	její	k3xOp3gNnSc6	její
13,2	[number]	k4	13,2
říčním	říční	k2eAgInSc6d1	říční
kilometru	kilometr	k1gInSc6	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Ústí	ústí	k1gNnSc1	ústí
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
východně	východně	k6eAd1	východně
od	od	k7c2	od
Bojman	Bojman	k1gMnSc1	Bojman
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
okolo	okolo	k7c2	okolo
215	[number]	k4	215
m.	m.	k?	m.
</s>
</p>
<p>
<s>
===	===	k?	===
Větší	veliký	k2eAgInPc1d2	veliký
přítoky	přítok	k1gInPc1	přítok
===	===	k?	===
</s>
</p>
<p>
<s>
Nejdelším	dlouhý	k2eAgInSc7d3	nejdelší
přítokem	přítok	k1gInSc7	přítok
Starkočského	Starkočský	k2eAgInSc2d1	Starkočský
potoka	potok	k1gInSc2	potok
je	být	k5eAaImIp3nS	být
bezejmenný	bezejmenný	k2eAgInSc1d1	bezejmenný
levostranný	levostranný	k2eAgInSc1d1	levostranný
přítok	přítok	k1gInSc1	přítok
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
jej	on	k3xPp3gMnSc4	on
posiluje	posilovat	k5eAaImIp3nS	posilovat
na	na	k7c6	na
8,2	[number]	k4	8,2
říčním	říční	k2eAgInSc6d1	říční
kilometru	kilometr	k1gInSc6	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
jeho	on	k3xPp3gInSc2	on
toku	tok	k1gInSc2	tok
činí	činit	k5eAaImIp3nS	činit
1,6	[number]	k4	1,6
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vodní	vodní	k2eAgInSc1d1	vodní
režim	režim	k1gInSc1	režim
==	==	k?	==
</s>
</p>
<p>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
průtok	průtok	k1gInSc1	průtok
Starkočského	Starkočský	k2eAgInSc2d1	Starkočský
potoka	potok	k1gInSc2	potok
u	u	k7c2	u
ústí	ústí	k1gNnSc2	ústí
činí	činit	k5eAaImIp3nS	činit
0,1	[number]	k4	0,1
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
