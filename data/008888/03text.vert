<p>
<s>
Doktor	doktor	k1gMnSc1	doktor
práv	práv	k2eAgMnSc1d1	práv
(	(	kIx(	(
<g/>
ve	v	k7c6	v
zkratce	zkratka	k1gFnSc6	zkratka
JUDr.	JUDr.	kA	JUDr.
psané	psaný	k2eAgInPc4d1	psaný
před	před	k7c7	před
jménem	jméno	k1gNnSc7	jméno
<g/>
;	;	kIx,	;
dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
též	též	k9	též
používala	používat	k5eAaImAgFnS	používat
zkratka	zkratka	k1gFnSc1	zkratka
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
jur	jura	k1gFnPc2	jura
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
akademický	akademický	k2eAgInSc4d1	akademický
titul	titul	k1gInSc4	titul
udělovaný	udělovaný	k2eAgInSc4d1	udělovaný
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
práva	právo	k1gNnSc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Udělován	udělován	k2eAgMnSc1d1	udělován
je	být	k5eAaImIp3nS	být
těm	ten	k3xDgMnPc3	ten
právníkům	právník	k1gMnPc3	právník
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
už	už	k6eAd1	už
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
titulu	titul	k1gInSc2	titul
magistr	magistr	k1gMnSc1	magistr
(	(	kIx(	(
<g/>
Mgr.	Mgr.	kA	Mgr.
<g/>
)	)	kIx)	)
a	a	k8xC	a
kteří	který	k3yQgMnPc1	který
poté	poté	k6eAd1	poté
úspěšně	úspěšně	k6eAd1	úspěšně
absolvovali	absolvovat	k5eAaPmAgMnP	absolvovat
rigorózní	rigorózní	k2eAgFnSc4d1	rigorózní
zkoušku	zkouška	k1gFnSc4	zkouška
ze	z	k7c2	z
zvoleného	zvolený	k2eAgInSc2d1	zvolený
právního	právní	k2eAgInSc2d1	právní
oboru	obor	k1gInSc2	obor
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
také	také	k9	také
obhajoba	obhajoba	k1gFnSc1	obhajoba
rigorózní	rigorózní	k2eAgFnSc2d1	rigorózní
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
fakultativní	fakultativní	k2eAgFnSc4d1	fakultativní
zkoušku	zkouška	k1gFnSc4	zkouška
zpravidla	zpravidla	k6eAd1	zpravidla
spojenou	spojený	k2eAgFnSc4d1	spojená
s	s	k7c7	s
poplatky	poplatek	k1gInPc7	poplatek
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
<g/>
,	,	kIx,	,
středověký	středověký	k2eAgInSc1d1	středověký
význam	význam	k1gInSc1	význam
latinské	latinský	k2eAgFnSc2d1	Latinská
zkratky	zkratka	k1gFnSc2	zkratka
JUDr.	JUDr.	kA	JUDr.
je	být	k5eAaImIp3nS	být
juris	juris	k1gFnPc7	juris
utriusque	utriusque	k1gFnSc1	utriusque
doctor	doctor	k1gInSc1	doctor
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
doktor	doktor	k1gMnSc1	doktor
obojího	obojí	k4xRgMnSc2	obojí
práva	právo	k1gNnPc1	právo
(	(	kIx(	(
<g/>
římského	římský	k2eAgInSc2d1	římský
i	i	k8xC	i
kanonického	kanonický	k2eAgInSc2d1	kanonický
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
zdrojů	zdroj	k1gInPc2	zdroj
však	však	k9	však
jde	jít	k5eAaImIp3nS	jít
i	i	k9	i
o	o	k7c4	o
zkratku	zkratka	k1gFnSc4	zkratka
juris	juris	k1gFnSc4	juris
universi	universe	k1gFnSc4	universe
doctor	doctor	k1gMnSc1	doctor
<g/>
,	,	kIx,	,
doktor	doktor	k1gMnSc1	doktor
veškerých	veškerý	k3xTgNnPc2	veškerý
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
===	===	k?	===
</s>
</p>
<p>
<s>
Název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
doktor	doktor	k1gMnSc1	doktor
obojího	obojí	k4xRgMnSc2	obojí
práva	právo	k1gNnSc2	právo
<g/>
"	"	kIx"	"
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
boloňské	boloňský	k2eAgFnSc2d1	Boloňská
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
učitelé	učitel	k1gMnPc1	učitel
římského	římský	k2eAgNnSc2d1	římské
práva	právo	k1gNnSc2	právo
označovali	označovat	k5eAaImAgMnP	označovat
jako	jako	k9	jako
doctores	doctores	k1gMnSc1	doctores
legum	legum	k1gInSc1	legum
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
i	i	k9	i
na	na	k7c4	na
další	další	k2eAgFnPc4d1	další
univerzity	univerzita	k1gFnPc4	univerzita
a	a	k8xC	a
díky	díky	k7c3	díky
pozdějšímu	pozdní	k2eAgNnSc3d2	pozdější
sloučení	sloučení	k1gNnSc3	sloučení
římskoprávních	římskoprávní	k2eAgFnPc2d1	římskoprávní
a	a	k8xC	a
církevněprávních	církevněprávní	k2eAgFnPc2d1	církevněprávní
studií	studie	k1gFnPc2	studie
došlo	dojít	k5eAaPmAgNnS	dojít
i	i	k9	i
ke	k	k7c3	k
spojení	spojení	k1gNnSc3	spojení
tohoto	tento	k3xDgInSc2	tento
titulu	titul	k1gInSc2	titul
s	s	k7c7	s
titulem	titul	k1gInSc7	titul
učitelů	učitel	k1gMnPc2	učitel
kanonického	kanonický	k2eAgNnSc2d1	kanonické
práva	právo	k1gNnSc2	právo
doctores	doctores	k1gMnSc1	doctores
canonum	canonum	k1gNnSc1	canonum
et	et	k?	et
decretalium	decretalium	k1gNnSc1	decretalium
do	do	k7c2	do
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
jediného	jediný	k2eAgInSc2d1	jediný
titulu	titul	k1gInSc2	titul
doktora	doktor	k1gMnSc4	doktor
obojího	obojí	k4xRgMnSc2	obojí
práva	právo	k1gNnPc1	právo
(	(	kIx(	(
<g/>
doctor	doctor	k1gInSc1	doctor
utriusque	utriusqu	k1gFnSc2	utriusqu
juris	juris	k1gFnSc2	juris
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
První	první	k4xOgFnSc1	první
moderní	moderní	k2eAgFnSc1d1	moderní
úprava	úprava	k1gFnSc1	úprava
získání	získání	k1gNnSc4	získání
titulu	titul	k1gInSc2	titul
doktora	doktor	k1gMnSc2	doktor
práv	práv	k2eAgInSc4d1	práv
na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
byla	být	k5eAaImAgNnP	být
obsažena	obsáhnout	k5eAaPmNgNnP	obsáhnout
v	v	k7c6	v
rakouském	rakouský	k2eAgInSc6d1	rakouský
rigorózním	rigorózní	k2eAgInSc6d1	rigorózní
řádu	řád	k1gInSc6	řád
pro	pro	k7c4	pro
fakulty	fakulta	k1gFnPc4	fakulta
věd	věda	k1gFnPc2	věda
právních	právní	k2eAgFnPc2d1	právní
a	a	k8xC	a
státních	státní	k2eAgFnPc2d1	státní
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1872	[number]	k4	1872
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
v	v	k7c6	v
mírně	mírně	k6eAd1	mírně
pozměněné	pozměněný	k2eAgFnSc6d1	pozměněná
podobě	podoba	k1gFnSc6	podoba
platil	platit	k5eAaImAgMnS	platit
i	i	k9	i
po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
první	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
student	student	k1gMnSc1	student
práv	právo	k1gNnPc2	právo
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vykonal	vykonat	k5eAaPmAgInS	vykonat
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
studia	studio	k1gNnSc2	studio
jednu	jeden	k4xCgFnSc4	jeden
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
předepsaných	předepsaný	k2eAgFnPc2d1	předepsaná
státních	státní	k2eAgFnPc2d1	státní
zkoušek	zkouška	k1gFnPc2	zkouška
(	(	kIx(	(
<g/>
historickoprávní	historickoprávní	k2eAgFnSc7d1	historickoprávní
<g/>
,	,	kIx,	,
soudní	soudní	k2eAgFnSc7d1	soudní
a	a	k8xC	a
státovědeckou	státovědecký	k2eAgFnSc7d1	státovědecký
<g/>
)	)	kIx)	)
s	s	k7c7	s
prospěchem	prospěch	k1gInSc7	prospěch
alespoň	alespoň	k9	alespoň
dobrým	dobrý	k2eAgMnSc7d1	dobrý
<g/>
,	,	kIx,	,
mohl	moct	k5eAaImAgInS	moct
po	po	k7c4	po
každé	každý	k3xTgInPc4	každý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
absolvovat	absolvovat	k5eAaPmF	absolvovat
i	i	k9	i
přísnou	přísný	k2eAgFnSc4d1	přísná
zkoušku	zkouška	k1gFnSc4	zkouška
doktorskou	doktorský	k2eAgFnSc4d1	doktorská
neboli	neboli	k8xC	neboli
rigorózní	rigorózní	k2eAgFnSc4d1	rigorózní
<g/>
.	.	kIx.	.
</s>
<s>
Zkušebními	zkušební	k2eAgInPc7d1	zkušební
předměty	předmět	k1gInPc7	předmět
u	u	k7c2	u
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
rigoróz	rigorózum	k1gNnPc2	rigorózum
byly	být	k5eAaImAgFnP	být
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
historickoprávní	historickoprávní	k2eAgNnSc1d1	historickoprávní
rigorózum	rigorózum	k1gNnSc1	rigorózum
–	–	k?	–
právo	právo	k1gNnSc1	právo
římské	římský	k2eAgInPc1d1	římský
<g/>
,	,	kIx,	,
právo	právo	k1gNnSc1	právo
církevní	církevní	k2eAgNnSc1d1	církevní
<g/>
,	,	kIx,	,
dějiny	dějiny	k1gFnPc1	dějiny
veřejného	veřejný	k2eAgNnSc2d1	veřejné
a	a	k8xC	a
soukromého	soukromý	k2eAgNnSc2d1	soukromé
práva	právo	k1gNnSc2	právo
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
dějiny	dějiny	k1gFnPc4	dějiny
práva	právo	k1gNnSc2	právo
na	na	k7c6	na
území	území	k1gNnSc6	území
republiky	republika	k1gFnSc2	republika
Československé	československý	k2eAgNnSc1d1	Československé
(	(	kIx(	(
<g/>
před	před	k7c7	před
vznikem	vznik	k1gInSc7	vznik
republiky	republika	k1gFnSc2	republika
místo	místo	k7c2	místo
právních	právní	k2eAgFnPc2d1	právní
dějin	dějiny	k1gFnPc2	dějiny
"	"	kIx"	"
<g/>
německé	německý	k2eAgNnSc1d1	německé
právo	právo	k1gNnSc1	právo
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
dějin	dějiny	k1gFnPc2	dějiny
právních	právní	k2eAgInPc2d1	právní
pramenů	pramen	k1gInPc2	pramen
a	a	k8xC	a
práva	právo	k1gNnSc2	právo
veřejného	veřejný	k2eAgNnSc2d1	veřejné
a	a	k8xC	a
dějin	dějiny	k1gFnPc2	dějiny
a	a	k8xC	a
systému	systém	k1gInSc2	systém
práva	právo	k1gNnSc2	právo
soukromého	soukromý	k2eAgNnSc2d1	soukromé
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
judiciální	judiciální	k2eAgNnSc1d1	judiciální
rigorózum	rigorózum	k1gNnSc1	rigorózum
–	–	k?	–
právo	právo	k1gNnSc4	právo
občanské	občanský	k2eAgFnSc2d1	občanská
<g/>
,	,	kIx,	,
právo	právo	k1gNnSc4	právo
obchodní	obchodní	k2eAgNnSc4d1	obchodní
a	a	k8xC	a
směnečné	směnečný	k2eAgNnSc4d1	směnečné
<g/>
,	,	kIx,	,
civilní	civilní	k2eAgNnSc4d1	civilní
řízení	řízení	k1gNnSc4	řízení
soudní	soudní	k2eAgNnSc4d1	soudní
a	a	k8xC	a
právo	právo	k1gNnSc4	právo
trestní	trestní	k2eAgNnSc4d1	trestní
a	a	k8xC	a
řízení	řízení	k1gNnSc4	řízení
trestní	trestní	k2eAgNnSc4d1	trestní
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
státovědecké	státovědecký	k2eAgNnSc1d1	státovědecký
rigorózum	rigorózum	k1gNnSc1	rigorózum
–	–	k?	–
právo	právo	k1gNnSc4	právo
ústavní	ústavní	k2eAgMnPc1d1	ústavní
<g/>
,	,	kIx,	,
nauka	nauka	k1gFnSc1	nauka
správní	správní	k2eAgFnSc1d1	správní
a	a	k8xC	a
právo	právo	k1gNnSc1	právo
správní	správní	k2eAgNnSc1d1	správní
<g/>
,	,	kIx,	,
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
národním	národní	k2eAgNnSc6d1	národní
hospodářství	hospodářství	k1gNnSc6	hospodářství
a	a	k8xC	a
národohospodářská	národohospodářský	k2eAgFnSc1d1	Národohospodářská
politika	politika	k1gFnSc1	politika
<g/>
,	,	kIx,	,
finanční	finanční	k2eAgFnSc1d1	finanční
věda	věda	k1gFnSc1	věda
a	a	k8xC	a
právo	právo	k1gNnSc4	právo
finanční	finanční	k2eAgInPc4d1	finanční
a	a	k8xC	a
právo	právo	k1gNnSc4	právo
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
moderní	moderní	k2eAgFnSc2d1	moderní
doby	doba	k1gFnSc2	doba
absolvent	absolvent	k1gMnSc1	absolvent
nenabýval	nabývat	k5eNaImAgMnS	nabývat
titulu	titul	k1gInSc3	titul
automaticky	automaticky	k6eAd1	automaticky
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
až	až	k9	až
udělením	udělení	k1gNnSc7	udělení
na	na	k7c6	na
slavnostní	slavnostní	k2eAgFnSc6d1	slavnostní
promoci	promoce	k1gFnSc6	promoce
<g/>
.	.	kIx.	.
</s>
<s>
Právnickou	právnický	k2eAgFnSc4d1	právnická
fakultu	fakulta	k1gFnSc4	fakulta
šlo	jít	k5eAaImAgNnS	jít
absolvovat	absolvovat	k5eAaPmF	absolvovat
i	i	k9	i
jen	jen	k6eAd1	jen
složením	složení	k1gNnSc7	složení
státních	státní	k2eAgFnPc2d1	státní
zkoušek	zkouška	k1gFnPc2	zkouška
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
bez	bez	k7c2	bez
získání	získání	k1gNnSc2	získání
titulu	titul	k1gInSc2	titul
doktora	doktor	k1gMnSc4	doktor
práv	práv	k2eAgInSc1d1	práv
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
jiných	jiný	k2eAgFnPc2d1	jiná
právnických	právnický	k2eAgFnPc2d1	právnická
profesí	profes	k1gFnPc2	profes
<g/>
,	,	kIx,	,
doktorát	doktorát	k1gInSc1	doktorát
byl	být	k5eAaImAgInS	být
podmínkou	podmínka	k1gFnSc7	podmínka
pro	pro	k7c4	pro
výkon	výkon	k1gInSc4	výkon
advokacie	advokacie	k1gFnSc2	advokacie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
prvním	první	k4xOgInSc6	první
rigorózu	rigorózum	k1gNnSc6	rigorózum
studenti	student	k1gMnPc1	student
používali	používat	k5eAaImAgMnP	používat
neoficiální	oficiální	k2eNgInSc4d1	neoficiální
čekatelský	čekatelský	k2eAgInSc4d1	čekatelský
titul	titul	k1gInSc4	titul
JUC	JUC	kA	JUC
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
z	z	k7c2	z
lat.	lat.	k?	lat.
juris	juris	k1gFnSc2	juris
utriusque	utriusquat	k5eAaPmIp3nS	utriusquat
candidatus	candidatus	k1gInSc1	candidatus
<g/>
;	;	kIx,	;
podobně	podobně	k6eAd1	podobně
byly	být	k5eAaImAgInP	být
užívány	užívat	k5eAaImNgInP	užívat
i	i	k8xC	i
tituly	titul	k1gInPc1	titul
MUC	MUC	kA	MUC
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
PhC	PhC	k1gFnSc1	PhC
<g/>
.	.	kIx.	.
nebo	nebo	k8xC	nebo
RNC	RNC	kA	RNC
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
vyšel	vyjít	k5eAaPmAgMnS	vyjít
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
reformě	reforma	k1gFnSc3	reforma
studia	studio	k1gNnSc2	studio
z	z	k7c2	z
užívání	užívání	k1gNnSc2	užívání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
===	===	k?	===
</s>
</p>
<p>
<s>
Uvedená	uvedený	k2eAgFnSc1d1	uvedená
úprava	úprava	k1gFnSc1	úprava
platila	platit	k5eAaImAgFnS	platit
až	až	k9	až
do	do	k7c2	do
nástupu	nástup	k1gInSc2	nástup
komunistů	komunista	k1gMnPc2	komunista
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgFnP	být
akademické	akademický	k2eAgInPc4d1	akademický
tituly	titul	k1gInPc4	titul
zákonem	zákon	k1gInSc7	zákon
o	o	k7c6	o
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
č.	č.	k?	č.
58	[number]	k4	58
<g/>
/	/	kIx~	/
<g/>
1950	[number]	k4	1950
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
zrušeny	zrušit	k5eAaPmNgFnP	zrušit
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
tak	tak	k6eAd1	tak
byli	být	k5eAaImAgMnP	být
absolventi	absolvent	k1gMnPc1	absolvent
právnických	právnický	k2eAgFnPc2d1	právnická
fakult	fakulta	k1gFnPc2	fakulta
označováni	označovat	k5eAaImNgMnP	označovat
jen	jen	k9	jen
profesním	profesní	k2eAgNnSc7d1	profesní
označením	označení	k1gNnSc7	označení
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
promovaný	promovaný	k2eAgMnSc1d1	promovaný
právník	právník	k1gMnSc1	právník
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
používaná	používaný	k2eAgFnSc1d1	používaná
zkratka	zkratka	k1gFnSc1	zkratka
prom	prom	k1gInSc1	prom
<g/>
.	.	kIx.	.
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
však	však	k9	však
byly	být	k5eAaImAgInP	být
tituly	titul	k1gInPc1	titul
znovu	znovu	k6eAd1	znovu
zavedeny	zaveden	k2eAgInPc1d1	zaveden
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
tehdejší	tehdejší	k2eAgFnSc1d1	tehdejší
úprava	úprava	k1gFnSc1	úprava
byla	být	k5eAaImAgFnS	být
podobná	podobný	k2eAgFnSc1d1	podobná
té	ten	k3xDgFnSc3	ten
dnešní	dnešní	k2eAgFnSc3d1	dnešní
<g/>
.	.	kIx.	.
</s>
<s>
Absolventi	absolvent	k1gMnPc1	absolvent
práv	právo	k1gNnPc2	právo
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
vykonali	vykonat	k5eAaPmAgMnP	vykonat
státní	státní	k2eAgFnSc4d1	státní
závěrečnou	závěrečný	k2eAgFnSc4d1	závěrečná
zkoušku	zkouška	k1gFnSc4	zkouška
<g/>
,	,	kIx,	,
mohli	moct	k5eAaImAgMnP	moct
podstoupit	podstoupit	k5eAaPmF	podstoupit
státní	státní	k2eAgFnSc4d1	státní
rigorózní	rigorózní	k2eAgFnSc4d1	rigorózní
zkoušku	zkouška	k1gFnSc4	zkouška
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
skládala	skládat	k5eAaImAgFnS	skládat
z	z	k7c2	z
písemné	písemný	k2eAgFnSc2d1	písemná
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
z	z	k7c2	z
ústní	ústní	k2eAgFnSc2d1	ústní
zkoušky	zkouška	k1gFnSc2	zkouška
ze	z	k7c2	z
zvoleného	zvolený	k2eAgInSc2d1	zvolený
vědního	vědní	k2eAgInSc2d1	vědní
oboru	obor	k1gInSc2	obor
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc7	jeho
širšího	široký	k2eAgInSc2d2	širší
vědního	vědní	k2eAgInSc2d1	vědní
základu	základ	k1gInSc2	základ
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
ústní	ústní	k2eAgFnSc2d1	ústní
zkoušky	zkouška	k1gFnSc2	zkouška
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vědními	vědní	k2eAgInPc7d1	vědní
obory	obor	k1gInPc7	obor
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgNnPc6	který
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
státní	státní	k2eAgFnSc4d1	státní
rigorózní	rigorózní	k2eAgFnSc4d1	rigorózní
zkoušku	zkouška	k1gFnSc4	zkouška
vykonat	vykonat	k5eAaPmF	vykonat
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
byly	být	k5eAaImAgFnP	být
dějiny	dějiny	k1gFnPc1	dějiny
státu	stát	k1gInSc2	stát
a	a	k8xC	a
práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
teorie	teorie	k1gFnSc2	teorie
státu	stát	k1gInSc2	stát
a	a	k8xC	a
práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
státní	státní	k2eAgNnSc4d1	státní
právo	právo	k1gNnSc4	právo
<g/>
,	,	kIx,	,
správní	správní	k2eAgNnSc4d1	správní
právo	právo	k1gNnSc4	právo
a	a	k8xC	a
zemědělskodružstevní	zemědělskodružstevní	k2eAgNnSc4d1	zemědělskodružstevní
právo	právo	k1gNnSc4	právo
<g/>
,	,	kIx,	,
hospodářské	hospodářský	k2eAgNnSc4d1	hospodářské
právo	právo	k1gNnSc4	právo
a	a	k8xC	a
finanční	finanční	k2eAgNnSc4d1	finanční
právo	právo	k1gNnSc4	právo
<g/>
,	,	kIx,	,
pracovní	pracovní	k2eAgNnSc4d1	pracovní
právo	právo	k1gNnSc4	právo
<g/>
,	,	kIx,	,
občanské	občanský	k2eAgNnSc1d1	občanské
právo	právo	k1gNnSc4	právo
hmotné	hmotný	k2eAgNnSc4d1	hmotné
a	a	k8xC	a
občanské	občanský	k2eAgNnSc4d1	občanské
právo	právo	k1gNnSc4	právo
procesní	procesní	k2eAgNnSc4d1	procesní
<g/>
,	,	kIx,	,
trestní	trestní	k2eAgNnSc4d1	trestní
právo	právo	k1gNnSc4	právo
a	a	k8xC	a
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
právo	právo	k1gNnSc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
byl	být	k5eAaImAgInS	být
ale	ale	k9	ale
titul	titul	k1gInSc1	titul
doktora	doktor	k1gMnSc2	doktor
práv	právo	k1gNnPc2	právo
znehodnocen	znehodnotit	k5eAaPmNgMnS	znehodnotit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
udílen	udílet	k5eAaImNgInS	udílet
rovněž	rovněž	k9	rovněž
absolventům	absolvent	k1gMnPc3	absolvent
Vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
SNB	SNB	kA	SNB
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
neabsolvovali	absolvovat	k5eNaPmAgMnP	absolvovat
řádné	řádný	k2eAgNnSc4d1	řádné
univerzitní	univerzitní	k2eAgNnSc4d1	univerzitní
právnické	právnický	k2eAgNnSc4d1	právnické
studium	studium	k1gNnSc4	studium
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
udělené	udělený	k2eAgInPc1d1	udělený
tituly	titul	k1gInPc1	titul
nebyly	být	k5eNaImAgInP	být
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
jejich	jejich	k3xOp3gFnSc4	jejich
nositelům	nositel	k1gMnPc3	nositel
odebrány	odebrán	k2eAgMnPc4d1	odebrán
a	a	k8xC	a
tito	tento	k3xDgMnPc1	tento
je	on	k3xPp3gNnSc4	on
tedy	tedy	k9	tedy
mohou	moct	k5eAaImIp3nP	moct
i	i	k9	i
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
dále	daleko	k6eAd2	daleko
užívat	užívat	k5eAaImF	užívat
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
např.	např.	kA	např.
Česká	český	k2eAgFnSc1d1	Česká
advokátní	advokátní	k2eAgFnSc1d1	advokátní
komora	komora	k1gFnSc1	komora
je	být	k5eAaImIp3nS	být
za	za	k7c4	za
právnické	právnický	k2eAgNnSc4d1	právnické
vzdělání	vzdělání	k1gNnSc4	vzdělání
<g/>
,	,	kIx,	,
potřebné	potřebné	k1gNnSc4	potřebné
pro	pro	k7c4	pro
výkon	výkon	k1gInSc4	výkon
povolání	povolání	k1gNnSc2	povolání
advokáta	advokát	k1gMnSc4	advokát
<g/>
,	,	kIx,	,
neuznává	uznávat	k5eNaImIp3nS	uznávat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
bylo	být	k5eAaImAgNnS	být
novým	nový	k2eAgInSc7d1	nový
vysokoškolským	vysokoškolský	k2eAgInSc7d1	vysokoškolský
zákonem	zákon	k1gInSc7	zákon
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
udělování	udělování	k1gNnPc2	udělování
titulu	titul	k1gInSc2	titul
doktora	doktor	k1gMnSc2	doktor
práva	právo	k1gNnSc2	právo
(	(	kIx(	(
<g/>
JUDr.	JUDr.	kA	JUDr.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
jako	jako	k9	jako
ostatních	ostatní	k2eAgInPc2d1	ostatní
fakultativních	fakultativní	k2eAgInPc2d1	fakultativní
malých	malý	k2eAgInPc2d1	malý
doktorátů	doktorát	k1gInPc2	doktorát
<g/>
,	,	kIx,	,
zrušeno	zrušen	k2eAgNnSc1d1	zrušeno
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
absolvování	absolvování	k1gNnSc6	absolvování
školy	škola	k1gFnSc2	škola
udělován	udělován	k2eAgInSc1d1	udělován
titul	titul	k1gInSc1	titul
magistra	magistra	k1gFnSc1	magistra
(	(	kIx(	(
<g/>
Mgr.	Mgr.	kA	Mgr.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obdobně	obdobně	k6eAd1	obdobně
jako	jako	k9	jako
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
podmínky	podmínka	k1gFnPc1	podmínka
pro	pro	k7c4	pro
získání	získání	k1gNnSc4	získání
doktorátu	doktorát	k1gInSc2	doktorát
<g/>
,	,	kIx,	,
obtížnější	obtížný	k2eAgMnSc1d2	obtížnější
<g/>
.	.	kIx.	.
</s>
<s>
Boloňský	boloňský	k2eAgInSc1d1	boloňský
proces	proces	k1gInSc1	proces
pak	pak	k6eAd1	pak
sjednotil	sjednotit	k5eAaPmAgInS	sjednotit
evropské	evropský	k2eAgNnSc4d1	Evropské
vysokoškolské	vysokoškolský	k2eAgNnSc4d1	vysokoškolské
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
nesouhlas	nesouhlas	k1gInSc4	nesouhlas
s	s	k7c7	s
neudělováním	neudělování	k1gNnSc7	neudělování
titulu	titul	k1gInSc2	titul
JUDr.	JUDr.	kA	JUDr.
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
těchto	tento	k3xDgInPc6	tento
tzv.	tzv.	kA	tzv.
malých	malý	k2eAgInPc2d1	malý
doktorátů	doktorát	k1gInPc2	doktorát
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
přijetí	přijetí	k1gNnSc2	přijetí
nového	nový	k2eAgInSc2d1	nový
vysokoškolského	vysokoškolský	k2eAgInSc2d1	vysokoškolský
zákona	zákon	k1gInSc2	zákon
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
tento	tento	k3xDgInSc4	tento
titul	titul	k1gInSc1	titul
opět	opět	k6eAd1	opět
udělován	udělován	k2eAgInSc1d1	udělován
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nyní	nyní	k6eAd1	nyní
po	po	k7c6	po
dodatečné	dodatečný	k2eAgFnSc6d1	dodatečná
a	a	k8xC	a
zpoplatněné	zpoplatněný	k2eAgFnSc6d1	zpoplatněná
rigorózní	rigorózní	k2eAgFnSc6d1	rigorózní
zkoušce	zkouška	k1gFnSc6	zkouška
–	–	k?	–
jeho	jeho	k3xOp3gNnSc4	jeho
udělení	udělení	k1gNnSc4	udělení
tak	tak	k6eAd1	tak
nepředchází	předcházet	k5eNaImIp3nS	předcházet
žádné	žádný	k3yNgNnSc4	žádný
další	další	k2eAgNnSc4d1	další
formální	formální	k2eAgNnSc4d1	formální
studium	studium	k1gNnSc4	studium
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
jedna	jeden	k4xCgFnSc1	jeden
zkouška	zkouška	k1gFnSc1	zkouška
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
nebývá	bývat	k5eNaImIp3nS	bývat
(	(	kIx(	(
<g/>
kupř	kupř	kA	kupř
<g/>
.	.	kIx.	.
srov.	srov.	kA	srov.
J.D.	J.D.	k1gFnSc2	J.D.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
stav	stav	k1gInSc1	stav
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
uděluje	udělovat	k5eAaImIp3nS	udělovat
jak	jak	k6eAd1	jak
JUDr.	JUDr.	kA	JUDr.
(	(	kIx(	(
<g/>
doktor	doktor	k1gMnSc1	doktor
práv	práv	k2eAgMnSc1d1	práv
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k9	tak
Mgr.	Mgr.	kA	Mgr.
(	(	kIx(	(
<g/>
magistr	magistr	k1gMnSc1	magistr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
oba	dva	k4xCgMnPc1	dva
označují	označovat	k5eAaImIp3nP	označovat
de	de	k?	de
facto	facto	k1gNnSc4	facto
stejnou	stejný	k2eAgFnSc4d1	stejná
kvalifikaci	kvalifikace	k1gFnSc4	kvalifikace
(	(	kIx(	(
<g/>
magisterskou	magisterský	k2eAgFnSc4d1	magisterská
úroveň	úroveň	k1gFnSc4	úroveň
<g/>
,	,	kIx,	,
7	[number]	k4	7
v	v	k7c6	v
ISCED	ISCED	kA	ISCED
<g/>
,	,	kIx,	,
master	master	k1gMnSc1	master
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
degree	degree	k1gFnSc7	degree
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
magisterský	magisterský	k2eAgInSc1d1	magisterský
stupeň	stupeň	k1gInSc1	stupeň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
však	však	k9	však
bývá	bývat	k5eAaImIp3nS	bývat
předmětem	předmět	k1gInSc7	předmět
kritiky	kritika	k1gFnSc2	kritika
<g/>
.	.	kIx.	.
<g/>
Vyšší	vysoký	k2eAgFnSc4d2	vyšší
kvalifikaci	kvalifikace	k1gFnSc4	kvalifikace
(	(	kIx(	(
<g/>
8	[number]	k4	8
v	v	k7c6	v
ISCED	ISCED	kA	ISCED
<g/>
,	,	kIx,	,
doctor	doctor	k1gInSc1	doctor
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
degree	degree	k1gNnSc7	degree
<g/>
)	)	kIx)	)
vhodnou	vhodný	k2eAgFnSc4d1	vhodná
primárně	primárně	k6eAd1	primárně
pro	pro	k7c4	pro
vědeckou	vědecký	k2eAgFnSc4d1	vědecká
činnost	činnost	k1gFnSc4	činnost
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
možno	možno	k6eAd1	možno
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
dalším	další	k1gNnSc7	další
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g />
.	.	kIx.	.
</s>
<s>
<g/>
letým	letý	k2eAgNnSc7d1	leté
studiem	studio	k1gNnSc7	studio
v	v	k7c6	v
doktorském	doktorský	k2eAgInSc6d1	doktorský
studijním	studijní	k2eAgInSc6d1	studijní
programu	program	k1gInSc6	program
(	(	kIx(	(
<g/>
doktor	doktor	k1gMnSc1	doktor
–	–	k?	–
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1990-1998	[number]	k4	1990-1998
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c6	o
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
postgraduální	postgraduální	k2eAgNnSc1d1	postgraduální
studium	studium	k1gNnSc1	studium
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
doktor	doktor	k1gMnSc1	doktor
–	–	k?	–
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
předchozím	předchozí	k2eAgNnSc6d1	předchozí
období	období	k1gNnSc6	období
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c6	o
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
vědeckou	vědecký	k2eAgFnSc4d1	vědecká
aspiranturu	aspirantura	k1gFnSc4	aspirantura
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
kandidát	kandidát	k1gMnSc1	kandidát
věd	věda	k1gFnPc2	věda
–	–	k?	–
CSc.	CSc.	kA	CSc.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
==	==	k?	==
</s>
</p>
<p>
<s>
Získání	získání	k1gNnSc4	získání
titulu	titul	k1gInSc2	titul
doktora	doktor	k1gMnSc2	doktor
práv	právo	k1gNnPc2	právo
je	být	k5eAaImIp3nS	být
podmíněno	podmínit	k5eAaPmNgNnS	podmínit
dřívějším	dřívější	k2eAgNnSc7d1	dřívější
získáním	získání	k1gNnSc7	získání
titulu	titul	k1gInSc2	titul
magistr	magistr	k1gMnSc1	magistr
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
obhájením	obhájení	k1gNnSc7	obhájení
písemné	písemný	k2eAgFnSc2d1	písemná
rigorózní	rigorózní	k2eAgFnSc2d1	rigorózní
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
úspěšným	úspěšný	k2eAgNnSc7d1	úspěšné
vykonáním	vykonání	k1gNnSc7	vykonání
ústní	ústní	k2eAgFnSc2d1	ústní
zkoušky	zkouška	k1gFnSc2	zkouška
ze	z	k7c2	z
zvoleného	zvolený	k2eAgInSc2d1	zvolený
právního	právní	k2eAgInSc2d1	právní
oboru	obor	k1gInSc2	obor
<g/>
.	.	kIx.	.
</s>
<s>
Udělovat	udělovat	k5eAaImF	udělovat
jej	on	k3xPp3gNnSc4	on
na	na	k7c6	na
českých	český	k2eAgFnPc6d1	Česká
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
může	moct	k5eAaImIp3nS	moct
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Právnická	právnický	k2eAgFnSc1d1	právnická
fakulta	fakulta	k1gFnSc1	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
</s>
</p>
<p>
<s>
Právnická	právnický	k2eAgFnSc1d1	právnická
fakulta	fakulta	k1gFnSc1	fakulta
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
</s>
</p>
<p>
<s>
Právnická	právnický	k2eAgFnSc1d1	právnická
fakulta	fakulta	k1gFnSc1	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Palackého	Palacký	k1gMnSc2	Palacký
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
–	–	k?	–
uděluje	udělovat	k5eAaImIp3nS	udělovat
od	od	k7c2	od
ledna	leden	k1gInSc2	leden
2010	[number]	k4	2010
</s>
</p>
<p>
<s>
Fakulta	fakulta	k1gFnSc1	fakulta
právnická	právnický	k2eAgFnSc1d1	právnická
Západočeské	západočeský	k2eAgFnPc4d1	Západočeská
univerzity	univerzita	k1gFnPc4	univerzita
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
–	–	k?	–
do	do	k7c2	do
listopadu	listopad	k1gInSc2	listopad
2011	[number]	k4	2011
a	a	k8xC	a
opět	opět	k6eAd1	opět
od	od	k7c2	od
června	červen	k1gInSc2	červen
2015	[number]	k4	2015
<g/>
Každá	každý	k3xTgFnSc1	každý
fakulta	fakulta	k1gFnSc1	fakulta
si	se	k3xPyFc3	se
obory	obor	k1gInPc4	obor
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgFnPc6	který
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
rigorózní	rigorózní	k2eAgFnSc4d1	rigorózní
zkoušku	zkouška	k1gFnSc4	zkouška
vykonat	vykonat	k5eAaPmF	vykonat
<g/>
,	,	kIx,	,
stanoví	stanovit	k5eAaPmIp3nS	stanovit
svým	svůj	k3xOyFgInSc7	svůj
vlastním	vlastní	k2eAgInSc7d1	vlastní
rigorózním	rigorózní	k2eAgInSc7d1	rigorózní
řádem	řád	k1gInSc7	řád
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
jsou	být	k5eAaImIp3nP	být
navíc	navíc	k6eAd1	navíc
uznávány	uznáván	k2eAgInPc1d1	uznáván
za	za	k7c4	za
rovnocenné	rovnocenný	k2eAgInPc4d1	rovnocenný
akademické	akademický	k2eAgInPc4d1	akademický
tituly	titul	k1gInPc4	titul
získané	získaný	k2eAgInPc4d1	získaný
na	na	k7c6	na
slovenských	slovenský	k2eAgFnPc6d1	slovenská
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
i	i	k9	i
slovenský	slovenský	k2eAgInSc1d1	slovenský
titul	titul	k1gInSc1	titul
doktora	doktor	k1gMnSc2	doktor
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
<g/>
Titul	titul	k1gInSc1	titul
doktora	doktor	k1gMnSc2	doktor
práv	právo	k1gNnPc2	právo
však	však	k9	však
není	být	k5eNaImIp3nS	být
výsledkem	výsledek	k1gInSc7	výsledek
doktorského	doktorský	k2eAgNnSc2d1	doktorské
studia	studio	k1gNnSc2	studio
(	(	kIx(	(
<g/>
doktor	doktor	k1gMnSc1	doktor
–	–	k?	–
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
,	,	kIx,	,
8	[number]	k4	8
v	v	k7c6	v
ISCED	ISCED	kA	ISCED
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
<g />
.	.	kIx.	.
</s>
<s>
velký	velký	k2eAgInSc1d1	velký
doktorát	doktorát	k1gInSc1	doktorát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
o	o	k7c4	o
magisterskou	magisterský	k2eAgFnSc4d1	magisterská
úroveň	úroveň	k1gFnSc4	úroveň
vysokoškolského	vysokoškolský	k2eAgNnSc2d1	vysokoškolské
vzdělání	vzdělání	k1gNnSc2	vzdělání
(	(	kIx(	(
<g/>
7	[number]	k4	7
v	v	k7c6	v
ISCED	ISCED	kA	ISCED
<g/>
,	,	kIx,	,
master	master	k1gMnSc1	master
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
degree	degree	k1gFnSc7	degree
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
tzv.	tzv.	kA	tzv.
malý	malý	k2eAgInSc4d1	malý
doktorát	doktorát	k1gInSc4	doktorát
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gMnSc1	jeho
nositel	nositel	k1gMnSc1	nositel
vzděláním	vzdělání	k1gNnSc7	vzdělání
(	(	kIx(	(
<g/>
kvalifikací	kvalifikace	k1gFnSc7	kvalifikace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
možnostmi	možnost	k1gFnPc7	možnost
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
kompetencemi	kompetence	k1gFnPc7	kompetence
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
rovnocenný	rovnocenný	k2eAgMnSc1d1	rovnocenný
s	s	k7c7	s
právníkem	právník	k1gMnSc7	právník
s	s	k7c7	s
titulem	titul	k1gInSc7	titul
magistra	magistra	k1gFnSc1	magistra
<g/>
.	.	kIx.	.
</s>
<s>
Tedy	tedy	k9	tedy
právník	právník	k1gMnSc1	právník
(	(	kIx(	(
<g/>
kupříkladu	kupříkladu	k6eAd1	kupříkladu
advokát	advokát	k1gMnSc1	advokát
<g/>
,	,	kIx,	,
státní	státní	k2eAgMnSc1d1	státní
zástupce	zástupce	k1gMnSc1	zástupce
<g/>
,	,	kIx,	,
soudce	soudce	k1gMnSc1	soudce
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
s	s	k7c7	s
titulem	titul	k1gInSc7	titul
doktora	doktor	k1gMnSc2	doktor
práv	právo	k1gNnPc2	právo
(	(	kIx(	(
<g/>
JUDr.	JUDr.	kA	JUDr.
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
od	od	k7c2	od
právníka	právník	k1gMnSc2	právník
s	s	k7c7	s
titulem	titul	k1gInSc7	titul
magistr	magistr	k1gMnSc1	magistr
(	(	kIx(	(
<g/>
Mgr.	Mgr.	kA	Mgr.
<g/>
)	)	kIx)	)
de	de	k?	de
facto	facto	k1gNnSc1	facto
neliší	lišit	k5eNaImIp3nS	lišit
<g/>
,	,	kIx,	,
oba	dva	k4xCgMnPc1	dva
mají	mít	k5eAaImIp3nP	mít
stejné	stejný	k2eAgNnSc4d1	stejné
pětileté	pětiletý	k2eAgNnSc4d1	pětileté
magisterské	magisterský	k2eAgNnSc4d1	magisterské
vzdělání	vzdělání	k1gNnSc4	vzdělání
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
právník	právník	k1gMnSc1	právník
s	s	k7c7	s
Mgr.	Mgr.	kA	Mgr.
ukončil	ukončit	k5eAaPmAgMnS	ukončit
studium	studium	k1gNnSc4	studium
státní	státní	k2eAgFnSc7d1	státní
závěrečnou	závěrečný	k2eAgFnSc7d1	závěrečná
zkouškou	zkouška	k1gFnSc7	zkouška
(	(	kIx(	(
<g/>
hovorově	hovorově	k6eAd1	hovorově
označovanou	označovaný	k2eAgFnSc7d1	označovaná
jako	jako	k8xS	jako
státnice	státnice	k1gFnSc1	státnice
<g/>
)	)	kIx)	)
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
nepokračoval	pokračovat	k5eNaImAgMnS	pokračovat
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
právník	právník	k1gMnSc1	právník
s	s	k7c7	s
JUDr.	JUDr.	kA	JUDr.
se	se	k3xPyFc4	se
po	po	k7c6	po
získání	získání	k1gNnSc6	získání
titulu	titul	k1gInSc2	titul
Mgr.	Mgr.	kA	Mgr.
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
následně	následně	k6eAd1	následně
složit	složit	k5eAaPmF	složit
ještě	ještě	k9	ještě
dodatečnou	dodatečný	k2eAgFnSc4d1	dodatečná
státní	státní	k2eAgFnSc4d1	státní
rigorózní	rigorózní	k2eAgFnSc4d1	rigorózní
zkoušku	zkouška	k1gFnSc4	zkouška
(	(	kIx(	(
<g/>
rígo	rígo	k6eAd1	rígo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nositel	nositel	k1gMnSc1	nositel
titulu	titul	k1gInSc2	titul
magistr	magistr	k1gMnSc1	magistr
(	(	kIx(	(
<g/>
Mgr.	Mgr.	kA	Mgr.
<g/>
)	)	kIx)	)
může	moct	k5eAaImIp3nS	moct
též	též	k9	též
požádat	požádat	k5eAaPmF	požádat
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
pakliže	pakliže	k8xS	pakliže
to	ten	k3xDgNnSc1	ten
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
příslušný	příslušný	k2eAgInSc4d1	příslušný
rigorózní	rigorózní	k2eAgInSc4d1	rigorózní
řád	řád	k1gInSc4	řád
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
stejná	stejný	k2eAgFnSc1d1	stejná
předložená	předložený	k2eAgFnSc1d1	předložená
magisterská	magisterský	k2eAgFnSc1d1	magisterská
(	(	kIx(	(
<g/>
diplomová	diplomový	k2eAgFnSc1d1	Diplomová
<g/>
)	)	kIx)	)
práce	práce	k1gFnSc1	práce
rovněž	rovněž	k9	rovněž
uznána	uznat	k5eAaPmNgFnS	uznat
i	i	k9	i
jako	jako	k9	jako
rigorózní	rigorózní	k2eAgFnPc4d1	rigorózní
práce	práce	k1gFnPc4	práce
<g/>
,	,	kIx,	,
podrobnosti	podrobnost	k1gFnPc4	podrobnost
většinou	většina	k1gFnSc7	většina
upravuje	upravovat	k5eAaImIp3nS	upravovat
pokyn	pokyn	k1gInSc4	pokyn
rektora	rektor	k1gMnSc2	rektor
<g/>
,	,	kIx,	,
pokyn	pokyn	k1gInSc4	pokyn
děkana	děkan	k1gMnSc2	děkan
atp.	atp.	kA	atp.
Rigorózum	rigorózum	k1gNnSc1	rigorózum
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
případech	případ	k1gInPc6	případ
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
finančně	finančně	k6eAd1	finančně
podmíněno	podmínit	k5eAaPmNgNnS	podmínit
–	–	k?	–
poplatky	poplatek	k1gInPc7	poplatek
s	s	k7c7	s
tímto	tento	k3xDgNnSc7	tento
spojené	spojený	k2eAgInPc1d1	spojený
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
příjmem	příjem	k1gInSc7	příjem
dané	daný	k2eAgFnSc2d1	daná
vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Doktorát	doktorát	k1gInSc1	doktorát
práv	právo	k1gNnPc2	právo
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
další	další	k2eAgFnPc1d1	další
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
platné	platný	k2eAgInPc1d1	platný
jak	jak	k8xS	jak
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
(	(	kIx(	(
<g/>
díky	díky	k7c3	díky
mezinárodní	mezinárodní	k2eAgFnSc3d1	mezinárodní
smlouvě	smlouva	k1gFnSc3	smlouva
není	být	k5eNaImIp3nS	být
třeba	třeba	k6eAd1	třeba
podstupovat	podstupovat	k5eAaImF	podstupovat
tzv.	tzv.	kA	tzv.
nostrifikaci	nostrifikace	k1gFnSc6	nostrifikace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Platí	platit	k5eAaImIp3nS	platit
pro	pro	k7c4	pro
ně	on	k3xPp3gNnSc4	on
i	i	k8xC	i
stejný	stejný	k2eAgInSc1d1	stejný
zápis	zápis	k1gInSc1	zápis
–	–	k?	–
zkratka	zkratka	k1gFnSc1	zkratka
titulu	titul	k1gInSc2	titul
se	se	k3xPyFc4	se
píše	psát	k5eAaImIp3nS	psát
před	před	k7c7	před
jménem	jméno	k1gNnSc7	jméno
a	a	k8xC	a
od	od	k7c2	od
jména	jméno	k1gNnSc2	jméno
se	se	k3xPyFc4	se
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
mezerou	mezera	k1gFnSc7	mezera
<g/>
,	,	kIx,	,
např.	např.	kA	např.
JUDr.	JUDr.	kA	JUDr.
Jan	Jan	k1gMnSc1	Jan
Novák	Novák	k1gMnSc1	Novák
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
zkracuje	zkracovat	k5eAaImIp3nS	zkracovat
na	na	k7c4	na
"	"	kIx"	"
<g/>
dr	dr	kA	dr
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
větného	větný	k2eAgInSc2d1	větný
celku	celek	k1gInSc2	celek
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Na	na	k7c6	na
Univerzitě	univerzita	k1gFnSc6	univerzita
Karlově	Karlův	k2eAgFnSc6d1	Karlova
vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Jan	Jan	k1gMnSc1	Jan
Novák	Novák	k1gMnSc1	Novák
<g/>
.	.	kIx.	.
/	/	kIx~	/
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Jan	Jan	k1gMnSc1	Jan
Novák	Novák	k1gMnSc1	Novák
vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
na	na	k7c6	na
Univerzitě	univerzita	k1gFnSc6	univerzita
Karlově	Karlův	k2eAgFnSc6d1	Karlova
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Obdobné	obdobný	k2eAgInPc4d1	obdobný
tituly	titul	k1gInPc4	titul
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
zemích	zem	k1gFnPc6	zem
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
platí	platit	k5eAaImIp3nS	platit
prakticky	prakticky	k6eAd1	prakticky
stejná	stejný	k2eAgFnSc1d1	stejná
úprava	úprava	k1gFnSc1	úprava
jako	jako	k8xS	jako
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Absolvent	absolvent	k1gMnSc1	absolvent
magisterského	magisterský	k2eAgInSc2d1	magisterský
právnického	právnický	k2eAgInSc2d1	právnický
studijního	studijní	k2eAgInSc2d1	studijní
programu	program	k1gInSc2	program
může	moct	k5eAaImIp3nS	moct
vykonat	vykonat	k5eAaPmF	vykonat
rigorózní	rigorózní	k2eAgFnSc4d1	rigorózní
zkoušku	zkouška	k1gFnSc4	zkouška
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
i	i	k9	i
obhajoba	obhajoba	k1gFnSc1	obhajoba
rigorózní	rigorózní	k2eAgFnSc2d1	rigorózní
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
a	a	k8xC	a
získat	získat	k5eAaPmF	získat
tím	ten	k3xDgNnSc7	ten
titul	titul	k1gInSc4	titul
doktor	doktor	k1gMnSc1	doktor
práv	právo	k1gNnPc2	právo
(	(	kIx(	(
<g/>
ve	v	k7c6	v
zkratce	zkratka	k1gFnSc6	zkratka
JUDr.	JUDr.	kA	JUDr.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
podobným	podobný	k2eAgInSc7d1	podobný
titulem	titul	k1gInSc7	titul
ve	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
zkratce	zkratka	k1gFnSc3	zkratka
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
iur	iur	k?	iur
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Doktor	doktor	k1gMnSc1	doktor
iuris	iuris	k1gFnSc2	iuris
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
jur	jura	k1gFnPc2	jura
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Doktor	doktor	k1gMnSc1	doktor
juris	juris	k1gFnSc2	juris
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
akademickou	akademický	k2eAgFnSc7d1	akademická
hodností	hodnost	k1gFnSc7	hodnost
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
práva	právo	k1gNnSc2	právo
a	a	k8xC	a
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
tedy	tedy	k8xC	tedy
titulu	titul	k1gInSc2	titul
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
Ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
významu	význam	k1gInSc6	význam
je	být	k5eAaImIp3nS	být
brán	brát	k5eAaImNgInS	brát
buď	buď	k8xC	buď
jako	jako	k8xC	jako
doktor	doktor	k1gMnSc1	doktor
práv	právo	k1gNnPc2	právo
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jako	jako	k8xS	jako
doktor	doktor	k1gMnSc1	doktor
jurisprudence	jurisprudence	k1gFnSc2	jurisprudence
(	(	kIx(	(
<g/>
právní	právní	k2eAgFnSc2d1	právní
vědy	věda	k1gFnSc2	věda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Udělován	udělován	k2eAgMnSc1d1	udělován
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
základě	základ	k1gInSc6	základ
obhájení	obhájení	k1gNnSc2	obhájení
doktorských	doktorský	k2eAgFnPc2d1	doktorská
tezí	teze	k1gFnPc2	teze
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
musí	muset	k5eAaImIp3nP	muset
vyjít	vyjít	k5eAaPmF	vyjít
jako	jako	k9	jako
publikace	publikace	k1gFnPc4	publikace
nebo	nebo	k8xC	nebo
jako	jako	k8xC	jako
soubor	soubor	k1gInSc4	soubor
článků	článek	k1gInPc2	článek
a	a	k8xC	a
které	který	k3yIgInPc1	který
musí	muset	k5eAaImIp3nP	muset
obsahovat	obsahovat	k5eAaImF	obsahovat
vlastní	vlastní	k2eAgInSc4d1	vlastní
výzkum	výzkum	k1gInSc4	výzkum
<g/>
,	,	kIx,	,
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
na	na	k7c6	na
základě	základ	k1gInSc6	základ
ústní	ústní	k2eAgFnSc2d1	ústní
zkoušky	zkouška	k1gFnSc2	zkouška
(	(	kIx(	(
<g/>
disputace	disputace	k1gFnSc2	disputace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oproti	oproti	k7c3	oproti
tomu	ten	k3xDgMnSc3	ten
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
titul	titul	k1gInSc4	titul
Doctor	Doctor	k1gMnSc1	Doctor
of	of	k?	of
Laws	Laws	k1gInSc1	Laws
(	(	kIx(	(
<g/>
ve	v	k7c6	v
zkratce	zkratka	k1gFnSc6	zkratka
LL	LL	kA	LL
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
ještě	ještě	k6eAd1	ještě
vyšším	vysoký	k2eAgInSc7d2	vyšší
než	než	k8xS	než
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
a	a	k8xC	a
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
uděluje	udělovat	k5eAaImIp3nS	udělovat
za	za	k7c4	za
velmi	velmi	k6eAd1	velmi
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
výzkumnou	výzkumný	k2eAgFnSc4d1	výzkumná
činnost	činnost	k1gFnSc4	činnost
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
jako	jako	k9	jako
čestný	čestný	k2eAgInSc4d1	čestný
titul	titul	k1gInSc4	titul
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
univerzity	univerzita	k1gFnPc1	univerzita
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Oxford	Oxford	k1gInSc1	Oxford
<g/>
,	,	kIx,	,
místo	místo	k7c2	místo
něj	on	k3xPp3gInSc2	on
udělují	udělovat	k5eAaImIp3nP	udělovat
titul	titul	k1gInSc4	titul
Doctor	Doctor	k1gInSc1	Doctor
of	of	k?	of
Civil	civil	k1gMnSc1	civil
Law	Law	k1gMnSc1	Law
(	(	kIx(	(
<g/>
ve	v	k7c6	v
zkratce	zkratka	k1gFnSc6	zkratka
D.C.L.	D.C.L.	k1gFnSc2	D.C.L.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
v	v	k7c6	v
USA	USA	kA	USA
či	či	k8xC	či
Austrálii	Austrálie	k1gFnSc6	Austrálie
vystudují	vystudovat	k5eAaPmIp3nP	vystudovat
bakalářskou	bakalářský	k2eAgFnSc4d1	Bakalářská
úroveň	úroveň	k1gFnSc4	úroveň
právnické	právnický	k2eAgFnSc2d1	právnická
fakulty	fakulta	k1gFnSc2	fakulta
<g/>
,	,	kIx,	,
získají	získat	k5eAaPmIp3nP	získat
titul	titul	k1gInSc4	titul
ve	v	k7c6	v
zkratce	zkratka	k1gFnSc6	zkratka
LL	LL	kA	LL
<g/>
.	.	kIx.	.
<g/>
B.	B.	kA	B.
Mohou	moct	k5eAaImIp3nP	moct
však	však	k9	však
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
postgraduálním	postgraduální	k2eAgNnSc6d1	postgraduální
studiu	studio	k1gNnSc6	studio
a	a	k8xC	a
získat	získat	k5eAaPmF	získat
tzv.	tzv.	kA	tzv.
profesní	profesní	k2eAgInSc4d1	profesní
doktorát	doktorát	k1gInSc4	doktorát
J.D.	J.D.	k1gFnSc2	J.D.
(	(	kIx(	(
<g/>
Juris	Juris	k1gFnSc1	Juris
Doctor	Doctor	k1gMnSc1	Doctor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
zaměřit	zaměřit	k5eAaPmF	zaměřit
na	na	k7c4	na
výzkum	výzkum	k1gInSc4	výzkum
a	a	k8xC	a
získat	získat	k5eAaPmF	získat
magisterský	magisterský	k2eAgInSc4d1	magisterský
titul	titul	k1gInSc4	titul
Master	master	k1gMnSc1	master
of	of	k?	of
Laws	Laws	k1gInSc1	Laws
(	(	kIx(	(
<g/>
ve	v	k7c6	v
zkratce	zkratka	k1gFnSc6	zkratka
LL	LL	kA	LL
<g/>
.	.	kIx.	.
<g/>
M.	M.	kA	M.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
doktorský	doktorský	k2eAgInSc1d1	doktorský
titul	titul	k1gInSc1	titul
tedy	tedy	k9	tedy
neoznačuje	označovat	k5eNaImIp3nS	označovat
právního	právní	k2eAgMnSc4d1	právní
vědce	vědec	k1gMnSc4	vědec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
odborníka	odborník	k1gMnSc4	odborník
pro	pro	k7c4	pro
právní	právní	k2eAgFnSc4d1	právní
praxi	praxe	k1gFnSc4	praxe
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
odbornou	odborný	k2eAgFnSc4d1	odborná
vědeckou	vědecký	k2eAgFnSc4d1	vědecká
činnost	činnost	k1gFnSc4	činnost
se	se	k3xPyFc4	se
uděluje	udělovat	k5eAaImIp3nS	udělovat
titul	titul	k1gInSc1	titul
Doctor	Doctor	k1gInSc1	Doctor
of	of	k?	of
Juridical	Juridical	k1gFnSc1	Juridical
Science	Science	k1gFnSc1	Science
(	(	kIx(	(
<g/>
ve	v	k7c6	v
zkratce	zkratka	k1gFnSc6	zkratka
J.	J.	kA	J.
<g/>
S.	S.	kA	S.
<g/>
D.	D.	kA	D.
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
S.	S.	kA	S.
<g/>
J.D.	J.D.	k1gFnSc1	J.D.
(	(	kIx(	(
<g/>
Scientiae	Scientiae	k1gInSc1	Scientiae
Juridicae	Juridicae	k1gNnSc1	Juridicae
Doctor	Doctor	k1gInSc1	Doctor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterému	který	k3yIgMnSc3	který
většinou	většinou	k6eAd1	většinou
předchází	předcházet	k5eAaImIp3nS	předcházet
titul	titul	k1gInSc1	titul
LL	LL	kA	LL
<g/>
.	.	kIx.	.
<g/>
M.	M.	kA	M.
a	a	k8xC	a
který	který	k3yRgInSc1	který
tak	tak	k6eAd1	tak
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
titulu	titul	k1gInSc3	titul
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
Titul	titul	k1gInSc1	titul
LL	LL	kA	LL
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
bývá	bývat	k5eAaImIp3nS	bývat
udělován	udělován	k2eAgInSc1d1	udělován
jen	jen	k9	jen
jako	jako	k8xS	jako
čestný	čestný	k2eAgInSc4d1	čestný
titul	titul	k1gInSc4	titul
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
J.	J.	kA	J.
U.	U.	kA	U.
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc4	dílo
Rigorosní	rigorosní	k2eAgInSc1d1	rigorosní
řád	řád	k1gInSc1	řád
pro	pro	k7c4	pro
fakulty	fakulta	k1gFnPc4	fakulta
věd	věda	k1gFnPc2	věda
právních	právní	k2eAgInPc2d1	právní
a	a	k8xC	a
státních	státní	k2eAgInPc2d1	státní
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
