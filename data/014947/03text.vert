<s>
HMS	HMS	kA
Hotspur	Hotspur	k1gMnSc1
(	(	kIx(
<g/>
H	H	kA
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Základní	základní	k2eAgInPc1d1
údaje	údaj	k1gInPc1
</s>
<s>
Typ	typ	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
torpédoborec	torpédoborec	k1gMnSc1
</s>
<s>
Třída	třída	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
třída	třída	k1gFnSc1
G	G	kA
a	a	k8xC
H	H	kA
</s>
<s>
Číslo	číslo	k1gNnSc1
trupu	trup	k1gInSc2
<g/>
:	:	kIx,
</s>
<s>
H01	H01	k4
</s>
<s>
Zahájení	zahájení	k1gNnSc1
stavby	stavba	k1gFnSc2
<g/>
:	:	kIx,
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1935	#num#	k4
</s>
<s>
Spuštěna	spuštěn	k2eAgNnPc1d1
na	na	k7c4
vodu	voda	k1gFnSc4
<g/>
:	:	kIx,
</s>
<s>
23	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1936	#num#	k4
</s>
<s>
Uvedena	uveden	k2eAgFnSc1d1
do	do	k7c2
služby	služba	k1gFnSc2
<g/>
:	:	kIx,
</s>
<s>
29	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1936	#num#	k4
</s>
<s>
Osud	osud	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
1948	#num#	k4
prodán	prodán	k2eAgInSc1d1
Dominikánské	dominikánský	k2eAgFnSc3d1
republice	republika	k1gFnSc3
</s>
<s>
Takticko-technická	takticko-technický	k2eAgNnPc1d1
data	datum	k1gNnPc1
</s>
<s>
Výtlak	výtlak	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
1340	#num#	k4
t	t	k?
(	(	kIx(
<g/>
standardní	standardní	k2eAgFnSc7d1
<g/>
)	)	kIx)
1980	#num#	k4
t	t	k?
(	(	kIx(
<g/>
plný	plný	k2eAgInSc4d1
<g/>
)	)	kIx)
</s>
<s>
Délka	délka	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
98	#num#	k4
m	m	kA
</s>
<s>
Šířka	šířka	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
10	#num#	k4
m	m	kA
</s>
<s>
Ponor	ponor	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
4	#num#	k4
m	m	kA
</s>
<s>
Pohon	pohon	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
34	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
hp	hp	k?
</s>
<s>
Rychlost	rychlost	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
36	#num#	k4
uzlů	uzel	k1gInPc2
</s>
<s>
Posádka	posádka	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
145	#num#	k4
</s>
<s>
Výzbroj	výzbroj	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
4	#num#	k4
<g/>
×	×	k?
120	#num#	k4
<g/>
mm	mm	kA
kanón	kanón	k1gInSc4
<g/>
8	#num#	k4
<g/>
×	×	k?
12,7	12,7	k4
mm	mm	kA
Vickers	Vickersa	k1gFnPc2
(	(	kIx(
<g/>
2	#num#	k4
<g/>
×	×	k?
<g/>
IV	IV	kA
<g/>
)	)	kIx)
<g/>
8	#num#	k4
<g/>
×	×	k?
533	#num#	k4
<g/>
mm	mm	kA
torpédomet	torpédomet	k1gInSc1
</s>
<s>
HMS	HMS	kA
Hotspur	Hotspur	k1gMnSc1
byl	být	k5eAaImAgMnS
britský	britský	k2eAgMnSc1d1
torpédoborec	torpédoborec	k1gMnSc1
třídy	třída	k1gFnSc2
G	G	kA
a	a	k8xC
H	H	kA
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
sloužil	sloužit	k5eAaImAgInS
u	u	k7c2
Royal	Royal	k1gMnSc1
Navy	Navy	k?
během	během	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Stavba	stavba	k1gFnSc1
</s>
<s>
Stavba	stavba	k1gFnSc1
začala	začít	k5eAaPmAgFnS
27	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1935	#num#	k4
v	v	k7c6
loděnici	loděnice	k1gFnSc6
Scott	Scott	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Shipbuilding	Shipbuilding	k1gInSc1
and	and	k?
Engineering	Engineering	k1gInSc1
v	v	k7c4
Greenock	Greenock	k1gInSc4
ve	v	k7c6
Skotsku	Skotsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
vodu	voda	k1gFnSc4
byl	být	k5eAaImAgInS
spuštěn	spustit	k5eAaPmNgInS
23	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1936	#num#	k4
a	a	k8xC
do	do	k7c2
služby	služba	k1gFnSc2
byl	být	k5eAaImAgMnS
přijmut	přijmout	k5eAaPmNgMnS
29	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1936	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Operační	operační	k2eAgFnSc1d1
služba	služba	k1gFnSc1
</s>
<s>
Účastnil	účastnit	k5eAaImAgInS
se	se	k3xPyFc4
v	v	k7c6
dubnu	duben	k1gInSc6
1940	#num#	k4
první	první	k4xOgFnSc2
námořní	námořní	k2eAgFnSc2d1
bitvy	bitva	k1gFnSc2
u	u	k7c2
Narviku	Narvik	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Účastnil	účastnit	k5eAaImAgInS
se	se	k3xPyFc4
v	v	k7c6
březnu	březen	k1gInSc6
1941	#num#	k4
bitvy	bitva	k1gFnSc2
u	u	k7c2
Matapanu	Matapan	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Účastnil	účastnit	k5eAaImAgInS
se	se	k3xPyFc4
v	v	k7c6
dubnu	duben	k1gInSc6
1941	#num#	k4
evakuace	evakuace	k1gFnSc1
Řecka	Řecko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
23	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1941	#num#	k4
spolu	spolu	k6eAd1
s	s	k7c7
torpédoborcem	torpédoborec	k1gInSc7
HMS	HMS	kA
Hasty	Hasta	k1gFnPc1
potopily	potopit	k5eAaPmAgFnP
severně	severně	k6eAd1
od	od	k7c2
Solumu	Solum	k1gInSc2
německou	německý	k2eAgFnSc4d1
ponorku	ponorka	k1gFnSc4
U-	U-	k1gFnSc2
<g/>
79	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Dne	den	k1gInSc2
23	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1948	#num#	k4
byl	být	k5eAaImAgInS
předán	předán	k2eAgInSc1d1
Dominikánské	dominikánský	k2eAgFnSc3d1
republice	republika	k1gFnSc3
<g/>
,	,	kIx,
kde	kde	k6eAd1
byl	být	k5eAaImAgInS
přejmenován	přejmenovat	k5eAaPmNgInS
na	na	k7c4
Trujillo	Trujillo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyřazen	vyřazen	k2eAgInSc4d1
a	a	k8xC
sešrotován	sešrotován	k2eAgInSc4d1
byl	být	k5eAaImAgInS
roku	rok	k1gInSc2
1972	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
HRBEK	Hrbek	k1gMnSc1
<g/>
,	,	kIx,
Ivan	Ivan	k1gMnSc1
<g/>
;	;	kIx,
HRBEK	Hrbek	k1gMnSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Loďstva	loďstvo	k1gNnPc4
států	stát	k1gInPc2
účastnících	účastnící	k2eAgInPc2d1
se	se	k3xPyFc4
druhé	druhý	k4xOgFnPc1
světové	světový	k2eAgFnPc1d1
války	válka	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Naše	náš	k3xOp1gNnSc1
vojsko	vojsko	k1gNnSc1
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
.	.	kIx.
231	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
206	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
245	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Britské	britský	k2eAgInPc1d1
torpédoborce	torpédoborec	k1gInPc1
třídy	třída	k1gFnSc2
G	G	kA
a	a	k8xC
H	H	kA
Třída	třída	k1gFnSc1
G	G	kA
<g/>
:	:	kIx,
</s>
<s>
Gallant	Gallant	k1gMnSc1
•	•	k?
Garland	Garland	k1gInSc1
•	•	k?
Gipsy	Gipsa	k1gFnSc2
•	•	k?
Glowworm	Glowworm	k1gInSc1
•	•	k?
Grafton	Grafton	k1gInSc1
•	•	k?
Grenade	Grenad	k1gInSc5
•	•	k?
Grenville	Grenvillo	k1gNnSc6
•	•	k?
Greyhound	Greyhound	k1gInSc1
•	•	k?
Griffin	Griffin	k1gInSc1
Třída	třída	k1gFnSc1
H	H	kA
<g/>
:	:	kIx,
</s>
<s>
Hardy	Hardy	k6eAd1
•	•	k?
Hasty	Hasta	k1gFnSc2
•	•	k?
Havock	Havock	k1gMnSc1
•	•	k?
Hereward	Hereward	k1gMnSc1
•	•	k?
Hero	Hero	k1gMnSc1
•	•	k?
Hostile	Hostil	k1gMnSc5
•	•	k?
Hotspur	Hotspur	k1gMnSc1
•	•	k?
Hunter	Hunter	k1gMnSc1
•	•	k?
Hyperion	Hyperion	k1gInSc1
Třída	třída	k1gFnSc1
Havant	Havant	k1gMnSc1
<g/>
:	:	kIx,
</s>
<s>
Harvester	Harvester	k1gMnSc1
•	•	k?
Havant	Havant	k1gMnSc1
•	•	k?
Havelock	Havelock	k1gMnSc1
•	•	k?
Hesperus	Hesperus	k1gInSc1
•	•	k?
Highlander	Highlander	k1gInSc1
•	•	k?
Hurricane	Hurrican	k1gMnSc5
Argentinské	argentinský	k2eAgNnSc1d1
námořnictvo	námořnictvo	k1gNnSc1
–	–	k?
Třída	třída	k1gFnSc1
Buenos	Buenos	k1gMnSc1
Aires	Aires	k1gMnSc1
<g/>
:	:	kIx,
</s>
<s>
Buenos	Buenos	k1gMnSc1
Aires	Aires	k1gMnSc1
•	•	k?
Entre	Entr	k1gInSc5
Rios	Riosa	k1gFnPc2
•	•	k?
Corrientes	Corrientes	k1gMnSc1
•	•	k?
San	San	k1gMnSc1
Juan	Juan	k1gMnSc1
•	•	k?
San	San	k1gFnSc2
Luis	Luisa	k1gFnPc2
•	•	k?
Misiones	Misiones	k1gMnSc1
•	•	k?
Santa	Santa	k1gMnSc1
Cruz	Cruz	k1gMnSc1
Brazilské	brazilský	k2eAgNnSc4d1
námořnictvo	námořnictvo	k1gNnSc4
–	–	k?
Třída	třída	k1gFnSc1
Acre	Acre	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
Acre	Acre	k6eAd1
•	•	k?
Ajuricaba	Ajuricaba	k1gMnSc1
•	•	k?
Amazonas	Amazonas	k1gMnSc1
•	•	k?
Apa	Apa	k1gMnSc1
•	•	k?
Araguarí	Araguarí	k1gMnSc1
•	•	k?
Araguaya	Araguaya	k1gMnSc1
Řecké	řecký	k2eAgNnSc1d1
námořnictvo	námořnictvo	k1gNnSc1
–	–	k?
Třída	třída	k1gFnSc1
Vasilefs	Vasilefs	k1gInSc1
Georgios	Georgios	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
Vasilefs	Vasilefs	k6eAd1
Georgios	Georgios	k1gInSc1
•	•	k?
Vasilissa	Vasilissa	k1gFnSc1
Olga	Olga	k1gFnSc1
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
třída	třída	k1gFnSc1
E	E	kA
a	a	k8xC
F	F	kA
•	•	k?
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
třída	třída	k1gFnSc1
I	i	k8xC
Seznam	seznam	k1gInSc1
britských	britský	k2eAgMnPc2d1
torpédoborců	torpédoborec	k1gMnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
