<s>
Štěstí	štěstí	k1gNnSc1	štěstí
je	být	k5eAaImIp3nS	být
český	český	k2eAgInSc1d1	český
hraný	hraný	k2eAgInSc1d1	hraný
film	film	k1gInSc1	film
režiséra	režisér	k1gMnSc2	režisér
Bohdana	Bohdan	k1gMnSc2	Bohdan
Slámy	Sláma	k1gMnSc2	Sláma
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
s	s	k7c7	s
Tatianou	Tatianý	k2eAgFnSc7d1	Tatianý
Vilhelmovou	Vilhelmová	k1gFnSc7	Vilhelmová
a	a	k8xC	a
Pavlem	Pavel	k1gMnSc7	Pavel
Liškou	Liška	k1gMnSc7	Liška
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
psychologické	psychologický	k2eAgNnSc4d1	psychologické
drama	drama	k1gNnSc4	drama
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
na	na	k7c6	na
Mostecku	Mostecko	k1gNnSc6	Mostecko
v	v	k7c6	v
úzkém	úzký	k2eAgInSc6d1	úzký
okruhu	okruh	k1gInSc6	okruh
několika	několik	k4yIc2	několik
spřátelených	spřátelený	k2eAgFnPc2d1	spřátelená
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc4	film
získal	získat	k5eAaPmAgMnS	získat
dvě	dva	k4xCgNnPc4	dva
ocenění	ocenění	k1gNnPc4	ocenění
na	na	k7c6	na
mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
filmovém	filmový	k2eAgInSc6d1	filmový
festivalu	festival	k1gInSc6	festival
v	v	k7c6	v
San	San	k1gMnSc6	San
Sebastianu	Sebastian	k1gMnSc6	Sebastian
<g/>
,	,	kIx,	,
Zlatou	zlatý	k2eAgFnSc4d1	zlatá
mušli	mušle	k1gFnSc4	mušle
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
cizojazyčný	cizojazyčný	k2eAgInSc4d1	cizojazyčný
film	film	k1gInSc4	film
a	a	k8xC	a
stříbrnou	stříbrný	k2eAgFnSc4d1	stříbrná
mušli	mušle	k1gFnSc4	mušle
za	za	k7c4	za
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
pro	pro	k7c4	pro
Annu	Anna	k1gFnSc4	Anna
Geislerovou	Geislerová	k1gFnSc4	Geislerová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
byl	být	k5eAaImAgInS	být
vyslán	vyslat	k5eAaPmNgInS	vyslat
za	za	k7c4	za
Česko	Česko	k1gNnSc4	Česko
jako	jako	k8xC	jako
kandidát	kandidát	k1gMnSc1	kandidát
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
cizojazyčný	cizojazyčný	k2eAgInSc4d1	cizojazyčný
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
pětici	pětice	k1gFnSc4	pětice
nominovaných	nominovaný	k2eAgMnPc2d1	nominovaný
se	se	k3xPyFc4	se
však	však	k9	však
nedostal	dostat	k5eNaPmAgMnS	dostat
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgInSc2	ten
získal	získat	k5eAaPmAgInS	získat
celkem	celkem	k6eAd1	celkem
sedm	sedm	k4xCc4	sedm
ocenění	ocenění	k1gNnPc2	ocenění
Český	český	k2eAgInSc1d1	český
lev	lev	k1gInSc1	lev
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgInPc4d1	jiný
také	také	k9	také
za	za	k7c4	za
režii	režie	k1gFnSc4	režie
<g/>
,	,	kIx,	,
scénář	scénář	k1gInSc4	scénář
a	a	k8xC	a
tři	tři	k4xCgFnPc4	tři
ceny	cena	k1gFnPc4	cena
za	za	k7c4	za
herecké	herecký	k2eAgInPc4d1	herecký
výkony	výkon	k1gInPc4	výkon
pro	pro	k7c4	pro
oba	dva	k4xCgMnPc4	dva
hlavní	hlavní	k2eAgMnPc4d1	hlavní
představitele	představitel	k1gMnPc4	představitel
<g/>
.	.	kIx.	.
</s>
<s>
Štěstí	štěstí	k1gNnSc1	štěstí
na	na	k7c6	na
webu	web	k1gInSc6	web
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
Štěstí	štěstit	k5eAaImIp3nS	štěstit
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc3d1	česko-slovenská
filmové	filmový	k2eAgFnSc3d1	filmová
databázi	databáze	k1gFnSc3	databáze
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Štěstí	štěstit	k5eAaImIp3nS	štěstit
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
Štěstí	štěstit	k5eAaImIp3nP	štěstit
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
