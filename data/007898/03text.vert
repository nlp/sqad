<s>
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
(	(	kIx(	(
<g/>
ukrajinsky	ukrajinsky	k6eAd1	ukrajinsky
У	У	k?	У
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
země	země	k1gFnSc1	země
ležící	ležící	k2eAgFnSc1d1	ležící
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
;	;	kIx,	;
na	na	k7c6	na
východě	východ	k1gInSc6	východ
hraničí	hraničit	k5eAaImIp3nP	hraničit
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
s	s	k7c7	s
Moldavskem	Moldavsko	k1gNnSc7	Moldavsko
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
neuznaného	uznaný	k2eNgMnSc2d1	neuznaný
Podněstří	Podněstří	k1gMnSc2	Podněstří
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rumunskem	Rumunsko	k1gNnSc7	Rumunsko
<g/>
,	,	kIx,	,
na	na	k7c6	na
západě	západ	k1gInSc6	západ
s	s	k7c7	s
Maďarskem	Maďarsko	k1gNnSc7	Maďarsko
<g/>
,	,	kIx,	,
Slovenskem	Slovensko	k1gNnSc7	Slovensko
a	a	k8xC	a
Polskem	Polsko	k1gNnSc7	Polsko
<g/>
,	,	kIx,	,
na	na	k7c6	na
severu	sever	k1gInSc6	sever
s	s	k7c7	s
Běloruskem	Bělorusko	k1gNnSc7	Bělorusko
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
hranice	hranice	k1gFnSc1	hranice
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
Černým	černý	k2eAgNnSc7d1	černé
a	a	k8xC	a
Azovským	azovský	k2eAgNnSc7d1	Azovské
mořem	moře	k1gNnSc7	moře
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgInPc7	tento
dvěma	dva	k4xCgInPc7	dva
moři	moře	k1gNnPc7	moře
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
poloostrov	poloostrov	k1gInSc1	poloostrov
Krym	Krym	k1gInSc1	Krym
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Kyjev	Kyjev	k1gInSc1	Kyjev
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
Dněpru	Dněpr	k1gInSc6	Dněpr
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc3d3	veliký
ukrajinské	ukrajinský	k2eAgFnSc3d1	ukrajinská
řece	řeka	k1gFnSc3	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
získala	získat	k5eAaPmAgFnS	získat
nezávislost	nezávislost	k1gFnSc4	nezávislost
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
;	;	kIx,	;
zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
zakládajícím	zakládající	k2eAgMnSc7d1	zakládající
členem	člen	k1gMnSc7	člen
Společenství	společenství	k1gNnSc2	společenství
nezávislých	závislý	k2eNgInPc2d1	nezávislý
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
je	být	k5eAaImIp3nS	být
unitární	unitární	k2eAgInSc4d1	unitární
stát	stát	k1gInSc4	stát
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
24	[number]	k4	24
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
dvě	dva	k4xCgFnPc4	dva
města	město	k1gNnSc2	město
se	s	k7c7	s
zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
statusem	status	k1gInSc7	status
(	(	kIx(	(
<g/>
Kyjev	Kyjev	k1gInSc1	Kyjev
a	a	k8xC	a
Sevastopol	Sevastopol	k1gInSc1	Sevastopol
<g/>
)	)	kIx)	)
a	a	k8xC	a
autonomní	autonomní	k2eAgFnSc4d1	autonomní
republiku	republika	k1gFnSc4	republika
Krym	Krym	k1gInSc1	Krym
<g/>
.	.	kIx.	.
</s>
<s>
Poloostrov	poloostrov	k1gInSc1	poloostrov
Krym	Krym	k1gInSc1	Krym
včetně	včetně	k7c2	včetně
města	město	k1gNnSc2	město
Sevastopolu	Sevastopol	k1gInSc2	Sevastopol
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2014	[number]	k4	2014
anektován	anektovat	k5eAaBmNgMnS	anektovat
Ruskou	ruský	k2eAgFnSc7d1	ruská
federací	federace	k1gFnSc7	federace
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
je	být	k5eAaImIp3nS	být
slovanského	slovanský	k2eAgInSc2d1	slovanský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
etymologie	etymologie	k1gFnSc1	etymologie
však	však	k9	však
není	být	k5eNaImIp3nS	být
jednoznačná	jednoznačný	k2eAgFnSc1d1	jednoznačná
<g/>
:	:	kIx,	:
nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
význam	význam	k1gInSc1	význam
"	"	kIx"	"
<g/>
okrajové	okrajový	k2eAgNnSc1d1	okrajové
území	území	k1gNnSc1	území
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
přesněji	přesně	k6eAd2	přesně
jednotlivá	jednotlivý	k2eAgFnSc1d1	jednotlivá
krajní	krajní	k2eAgNnSc4d1	krajní
<g/>
,	,	kIx,	,
hraniční	hraniční	k2eAgNnSc4d1	hraniční
knížectví	knížectví	k1gNnSc4	knížectví
Kyjevské	kyjevský	k2eAgFnSc2d1	Kyjevská
Rusi	Rus	k1gFnSc2	Rus
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
výklady	výklad	k1gInPc1	výklad
hovoří	hovořit	k5eAaImIp3nP	hovořit
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
knížectvími	knížectví	k1gNnPc7	knížectví
o	o	k7c6	o
"	"	kIx"	"
<g/>
ukrojených	ukrojený	k2eAgNnPc6d1	ukrojené
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
oddělených	oddělený	k2eAgFnPc6d1	oddělená
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
doložené	doložený	k2eAgNnSc1d1	doložené
užití	užití	k1gNnSc1	užití
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1187	[number]	k4	1187
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Kyjevského	kyjevský	k2eAgInSc2d1	kyjevský
letopisu	letopis	k1gInSc2	letopis
<g/>
,	,	kIx,	,
zachovaného	zachovaný	k2eAgInSc2d1	zachovaný
v	v	k7c6	v
Pověsti	pověst	k1gFnSc6	pověst
dávných	dávný	k2eAgNnPc2d1	dávné
let	léto	k1gNnPc2	léto
v	v	k7c6	v
Ipaťjevském	Ipaťjevský	k2eAgInSc6d1	Ipaťjevský
kodexu	kodex	k1gInSc6	kodex
z	z	k7c2	z
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
smrti	smrt	k1gFnSc6	smrt
perejaslavského	perejaslavský	k2eAgMnSc2d1	perejaslavský
knížete	kníže	k1gMnSc2	kníže
<g/>
,	,	kIx,	,
jejž	jenž	k3xRgInSc4	jenž
"	"	kIx"	"
<g/>
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
bohatě	bohatě	k6eAd1	bohatě
oplakala	oplakat	k5eAaPmAgFnS	oplakat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
staletích	staletí	k1gNnPc6	staletí
bylo	být	k5eAaImAgNnS	být
slovo	slovo	k1gNnSc1	slovo
nadále	nadále	k6eAd1	nadále
používáno	používat	k5eAaImNgNnS	používat
především	především	k9	především
v	v	k7c6	v
širokém	široký	k2eAgInSc6d1	široký
významu	význam	k1gInSc6	význam
krajních	krajní	k2eAgNnPc2d1	krajní
území	území	k1gNnPc2	území
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
též	též	k9	též
oblastí	oblast	k1gFnSc7	oblast
vnějších	vnější	k2eAgInPc2d1	vnější
slovanskému	slovanský	k2eAgNnSc3d1	slovanské
osídlení	osídlení	k1gNnSc3	osídlení
<g/>
,	,	kIx,	,
sousedních	sousední	k2eAgFnPc2d1	sousední
"	"	kIx"	"
<g/>
ukrajin	ukrajina	k1gFnPc2	ukrajina
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
slovo	slovo	k1gNnSc1	slovo
objevuje	objevovat	k5eAaImIp3nS	objevovat
i	i	k9	i
mimo	mimo	k7c4	mimo
slovanské	slovanský	k2eAgFnPc4d1	Slovanská
písemné	písemný	k2eAgFnPc4d1	písemná
památky	památka	k1gFnPc4	památka
a	a	k8xC	a
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
začíná	začínat	k5eAaImIp3nS	začínat
etablovat	etablovat	k5eAaBmF	etablovat
v	v	k7c6	v
evropské	evropský	k2eAgFnSc6d1	Evropská
geografii	geografie	k1gFnSc6	geografie
<g/>
;	;	kIx,	;
postupně	postupně	k6eAd1	postupně
začíná	začínat	k5eAaImIp3nS	začínat
odkazovat	odkazovat	k5eAaImF	odkazovat
k	k	k7c3	k
"	"	kIx"	"
<g/>
území	území	k1gNnSc3	území
obývanému	obývaný	k2eAgMnSc3d1	obývaný
Ukrajinci	Ukrajinec	k1gMnSc3	Ukrajinec
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Označení	označení	k1gNnSc1	označení
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
bylo	být	k5eAaImAgNnS	být
užíváno	užívat	k5eAaImNgNnS	užívat
spíše	spíše	k9	spíše
v	v	k7c6	v
lidové	lidový	k2eAgFnSc6d1	lidová
řeči	řeč	k1gFnSc6	řeč
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
polské	polský	k2eAgNnSc1d1	polské
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
ruské	ruský	k2eAgInPc1d1	ruský
úřady	úřad	k1gInPc1	úřad
používaly	používat	k5eAaImAgFnP	používat
zpravidla	zpravidla	k6eAd1	zpravidla
označení	označení	k1gNnSc4	označení
Malá	malý	k2eAgFnSc1d1	malá
Rus	Rus	k1gFnSc1	Rus
<g/>
,	,	kIx,	,
Malorusko	Malorusko	k1gNnSc1	Malorusko
–	–	k?	–
oproti	oproti	k7c3	oproti
Velké	velká	k1gFnSc3	velká
Rusi	Rus	k1gFnSc2	Rus
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
dnešnímu	dnešní	k2eAgInSc3d1	dnešní
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
Bílé	bílý	k2eAgFnSc6d1	bílá
Rusi	Rus	k1gFnSc6	Rus
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Bělorusko	Bělorusko	k1gNnSc1	Bělorusko
<g/>
)	)	kIx)	)
a	a	k8xC	a
Červené	Červené	k2eAgFnSc6d1	Červené
Rusi	Rus	k1gFnSc6	Rus
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
bývali	bývat	k5eAaImAgMnP	bývat
označováni	označovat	k5eAaImNgMnP	označovat
zpravidla	zpravidla	k6eAd1	zpravidla
jako	jako	k8xC	jako
Malorusové	Malorus	k1gMnPc1	Malorus
či	či	k8xC	či
Rusíni	Rusín	k1gMnPc1	Rusín
<g/>
;	;	kIx,	;
etnonymum	etnonymum	k1gNnSc1	etnonymum
Ukrajinec	Ukrajinec	k1gMnSc1	Ukrajinec
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
přibližně	přibližně	k6eAd1	přibližně
od	od	k7c2	od
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
plně	plně	k6eAd1	plně
se	se	k3xPyFc4	se
však	však	k9	však
prosadilo	prosadit	k5eAaPmAgNnS	prosadit
až	až	k9	až
koncem	koncem	k7c2	koncem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc4	dějiny
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Osídlení	osídlení	k1gNnSc1	osídlení
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
je	být	k5eAaImIp3nS	být
doloženo	doložit	k5eAaPmNgNnS	doložit
od	od	k7c2	od
5	[number]	k4	5
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yRgInSc2	který
pocházejí	pocházet	k5eAaImIp3nP	pocházet
nálezy	nález	k1gInPc1	nález
artefaktů	artefakt	k1gInPc2	artefakt
tripolské	tripolský	k2eAgFnSc2d1	Tripolská
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
byl	být	k5eAaImAgInS	být
jih	jih	k1gInSc1	jih
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
periferií	periferie	k1gFnPc2	periferie
antického	antický	k2eAgInSc2d1	antický
světa	svět	k1gInSc2	svět
<g/>
:	:	kIx,	:
existovalo	existovat	k5eAaImAgNnS	existovat
zde	zde	k6eAd1	zde
království	království	k1gNnSc1	království
Skythů	Skyth	k1gMnPc2	Skyth
<g/>
,	,	kIx,	,
o	o	k7c6	o
jejichž	jejichž	k3xOyRp3gInSc6	jejichž
životě	život	k1gInSc6	život
podává	podávat	k5eAaImIp3nS	podávat
zprávu	zpráva	k1gFnSc4	zpráva
řecký	řecký	k2eAgMnSc1d1	řecký
historik	historik	k1gMnSc1	historik
Hérodotos	Hérodotos	k1gMnSc1	Hérodotos
<g/>
;	;	kIx,	;
na	na	k7c6	na
Krymu	Krym	k1gInSc6	Krym
a	a	k8xC	a
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Černého	Černého	k2eAgNnSc2d1	Černého
moře	moře	k1gNnSc2	moře
vznikaly	vznikat	k5eAaImAgInP	vznikat
řecké	řecký	k2eAgInPc1d1	řecký
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
gótské	gótský	k2eAgFnSc2d1	gótská
kolonie	kolonie	k1gFnSc2	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Střední	střední	k2eAgFnSc1d1	střední
a	a	k8xC	a
severozápadní	severozápadní	k2eAgFnSc1d1	severozápadní
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
oblastí	oblast	k1gFnSc7	oblast
etnogeneze	etnogeneze	k1gFnSc2	etnogeneze
Slovanů	Slovan	k1gMnPc2	Slovan
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
882	[number]	k4	882
Novgorodský	novgorodský	k2eAgMnSc1d1	novgorodský
kníže	kníže	k1gMnSc1	kníže
Oleg	Oleg	k1gMnSc1	Oleg
ovládl	ovládnout	k5eAaPmAgMnS	ovládnout
Kyjev	Kyjev	k1gInSc4	Kyjev
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
předtím	předtím	k6eAd1	předtím
platil	platit	k5eAaImAgInS	platit
tribut	tribut	k1gInSc1	tribut
Chazarské	chazarský	k2eAgFnSc2d1	Chazarská
říši	říš	k1gFnSc3	říš
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
,	,	kIx,	,
Ruska	Rusko	k1gNnSc2	Rusko
a	a	k8xC	a
Běloruska	Bělorusko	k1gNnSc2	Bělorusko
zformoval	zformovat	k5eAaPmAgInS	zformovat
první	první	k4xOgInSc1	první
východoslovanský	východoslovanský	k2eAgInSc1d1	východoslovanský
stát	stát	k1gInSc1	stát
<g/>
,	,	kIx,	,
Kyjevská	kyjevský	k2eAgFnSc1d1	Kyjevská
Rus	Rus	k1gFnSc1	Rus
<g/>
.	.	kIx.	.
</s>
<s>
Spojení	spojený	k2eAgMnPc1d1	spojený
Slované	Slovan	k1gMnPc1	Slovan
poté	poté	k6eAd1	poté
dobyli	dobýt	k5eAaPmAgMnP	dobýt
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Chazarů	Chazar	k1gMnPc2	Chazar
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
nejvýznamnější	významný	k2eAgMnSc1d3	nejvýznamnější
panovník	panovník	k1gMnSc1	panovník
Vladimír	Vladimír	k1gMnSc1	Vladimír
I.	I.	kA	I.
přijal	přijmout	k5eAaPmAgMnS	přijmout
roku	rok	k1gInSc2	rok
988	[number]	k4	988
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
státu	stát	k1gInSc2	stát
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
několik	několik	k4yIc1	několik
samostatných	samostatný	k2eAgNnPc2d1	samostatné
knížectví	knížectví	k1gNnPc2	knížectví
<g/>
;	;	kIx,	;
na	na	k7c6	na
západě	západ	k1gInSc6	západ
bylo	být	k5eAaImAgNnS	být
nejdůležitější	důležitý	k2eAgNnSc1d3	nejdůležitější
Haličsko-volyňské	haličskoolyňský	k2eAgNnSc1d1	haličsko-volyňský
knížectví	knížectví	k1gNnSc1	knížectví
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
jih	jih	k1gInSc1	jih
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
byl	být	k5eAaImAgInS	být
osídlen	osídlit	k5eAaPmNgInS	osídlit
kočovnými	kočovný	k2eAgInPc7d1	kočovný
kmeny	kmen	k1gInPc7	kmen
Polovců	Polovec	k1gInPc2	Polovec
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
postupnému	postupný	k2eAgNnSc3d1	postupné
obsazení	obsazení	k1gNnSc3	obsazení
části	část	k1gFnSc2	část
samostatných	samostatný	k2eAgFnPc2d1	samostatná
knížectví	knížectví	k1gNnPc2	knížectví
tatarskými	tatarský	k2eAgMnPc7d1	tatarský
(	(	kIx(	(
<g/>
mongolskými	mongolský	k2eAgMnPc7d1	mongolský
<g/>
)	)	kIx)	)
nájezdníky	nájezdník	k1gMnPc7	nájezdník
<g/>
.	.	kIx.	.
</s>
<s>
Kyjev	Kyjev	k1gInSc1	Kyjev
byl	být	k5eAaImAgInS	být
zpustošen	zpustošen	k2eAgInSc1d1	zpustošen
roku	rok	k1gInSc2	rok
1240	[number]	k4	1240
Mongoly	mongol	k1gInPc1	mongol
a	a	k8xC	a
centrum	centrum	k1gNnSc1	centrum
Rusi	Rus	k1gFnSc2	Rus
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
přesunulo	přesunout	k5eAaPmAgNnS	přesunout
na	na	k7c4	na
sever	sever	k1gInSc4	sever
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
oblast	oblast	k1gFnSc1	oblast
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
pod	pod	k7c4	pod
vliv	vliv	k1gInSc4	vliv
nově	nově	k6eAd1	nově
zformované	zformovaný	k2eAgFnSc2d1	zformovaná
tatarské	tatarský	k2eAgFnSc2d1	tatarská
Zlaté	zlatý	k2eAgFnSc2d1	zlatá
hordy	horda	k1gFnSc2	horda
<g/>
;	;	kIx,	;
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
západní	západní	k2eAgFnSc4d1	západní
část	část	k1gFnSc4	část
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
tatarská	tatarský	k2eAgFnSc1d1	tatarská
moc	moc	k1gFnSc1	moc
nepronikla	proniknout	k5eNaPmAgFnS	proniknout
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgFnSc1d1	západní
část	část	k1gFnSc1	část
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
byla	být	k5eAaImAgFnS	být
od	od	k7c2	od
konce	konec	k1gInSc2	konec
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
součástí	součást	k1gFnPc2	součást
Polska	Polsko	k1gNnSc2	Polsko
(	(	kIx(	(
<g/>
Podolí	Podolí	k1gNnSc1	Podolí
<g/>
,	,	kIx,	,
Červená	červený	k2eAgFnSc1d1	červená
Rus	Rus	k1gFnSc1	Rus
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
Volyně	Volyně	k1gFnSc2	Volyně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Valná	valný	k2eAgFnSc1d1	valná
část	část	k1gFnSc1	část
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
se	se	k3xPyFc4	se
s	s	k7c7	s
úpadkem	úpadek	k1gInSc7	úpadek
Zlaté	zlatý	k2eAgFnSc2d1	zlatá
hordy	horda	k1gFnSc2	horda
stala	stát	k5eAaPmAgFnS	stát
suverénní	suverénní	k2eAgFnSc7d1	suverénní
součástí	součást	k1gFnSc7	součást
Litevského	litevský	k2eAgNnSc2d1	litevské
velkoknížectví	velkoknížectví	k1gNnSc2	velkoknížectví
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
se	se	k3xPyFc4	se
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
Krymský	krymský	k2eAgInSc1d1	krymský
chanát	chanát	k1gInSc1	chanát
a	a	k8xC	a
východ	východ	k1gInSc1	východ
se	se	k3xPyFc4	se
dostával	dostávat	k5eAaImAgInS	dostávat
pod	pod	k7c4	pod
vliv	vliv	k1gInSc4	vliv
Moskevské	moskevský	k2eAgFnSc2d1	Moskevská
Rusi	Rus	k1gFnSc2	Rus
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1569	[number]	k4	1569
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
uzavření	uzavření	k1gNnSc3	uzavření
Lublinské	lublinský	k2eAgFnSc2d1	Lublinská
unie	unie	k1gFnSc2	unie
<g/>
;	;	kIx,	;
území	území	k1gNnSc4	území
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
spadalo	spadat	k5eAaImAgNnS	spadat
pod	pod	k7c4	pod
Litvu	Litva	k1gFnSc4	Litva
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
po	po	k7c4	po
řeku	řeka	k1gFnSc4	řeka
Dněpr	Dněpr	k1gInSc1	Dněpr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Polsko-litevského	polskoitevský	k2eAgInSc2d1	polsko-litevský
státu	stát	k1gInSc2	stát
převedeno	převeden	k2eAgNnSc1d1	převedeno
přímo	přímo	k6eAd1	přímo
pod	pod	k7c4	pod
Polské	polský	k2eAgNnSc4d1	polské
království	království	k1gNnSc4	království
<g/>
.	.	kIx.	.
</s>
<s>
Ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
šlechta	šlechta	k1gFnSc1	šlechta
získala	získat	k5eAaPmAgFnS	získat
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
stejná	stejný	k2eAgNnPc1d1	stejné
práva	právo	k1gNnPc1	právo
jaké	jaký	k3yRgFnPc4	jaký
příslušely	příslušet	k5eAaImAgInP	příslušet
polské	polský	k2eAgFnSc3d1	polská
šlechtě	šlechta	k1gFnSc3	šlechta
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgNnSc1d1	významné
bylo	být	k5eAaImAgNnS	být
osídlení	osídlení	k1gNnSc1	osídlení
záporožských	záporožský	k2eAgInPc2d1	záporožský
kozáků	kozák	k1gInPc2	kozák
s	s	k7c7	s
centrem	centrum	k1gNnSc7	centrum
na	na	k7c6	na
Siči	Sič	k1gFnSc6	Sič
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
soustřeďovali	soustřeďovat	k5eAaImAgMnP	soustřeďovat
lidé	člověk	k1gMnPc1	člověk
z	z	k7c2	z
různých	různý	k2eAgFnPc2d1	různá
sociálních	sociální	k2eAgFnPc2d1	sociální
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
však	však	k9	však
poddaných	poddaný	k1gMnPc2	poddaný
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
sem	sem	k6eAd1	sem
utekli	utéct	k5eAaPmAgMnP	utéct
před	před	k7c7	před
feudálním	feudální	k2eAgInSc7d1	feudální
útlakem	útlak	k1gInSc7	útlak
<g/>
.	.	kIx.	.
</s>
<s>
Záporožci	Záporožec	k1gMnPc1	Záporožec
vytvářeli	vytvářet	k5eAaImAgMnP	vytvářet
jakési	jakýsi	k3yIgNnSc4	jakýsi
vojenské	vojenský	k2eAgNnSc4d1	vojenské
nárazníkové	nárazníkový	k2eAgNnSc4d1	nárazníkové
pásmo	pásmo	k1gNnSc4	pásmo
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Divokých	divoký	k2eAgFnPc2d1	divoká
polí	pole	k1gFnPc2	pole
<g/>
,	,	kIx,	,
sousedících	sousedící	k2eAgInPc2d1	sousedící
s	s	k7c7	s
Krymských	krymský	k2eAgMnPc2d1	krymský
chanátem	chanát	k1gInSc7	chanát
<g/>
.	.	kIx.	.
</s>
<s>
Krymští	krymský	k2eAgMnPc1d1	krymský
Tataři	Tatar	k1gMnPc1	Tatar
a	a	k8xC	a
Nogajci	Nogajce	k1gMnPc1	Nogajce
<g/>
,	,	kIx,	,
vazalové	vazal	k1gMnPc1	vazal
osmanského	osmanský	k2eAgMnSc2d1	osmanský
sultána	sultán	k1gMnSc2	sultán
<g/>
,	,	kIx,	,
podnikali	podnikat	k5eAaImAgMnP	podnikat
téměř	téměř	k6eAd1	téměř
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
loupeživé	loupeživý	k2eAgInPc4d1	loupeživý
vpády	vpád	k1gInPc4	vpád
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
zabraňovaly	zabraňovat	k5eAaImAgInP	zabraňovat
trvalejšímu	trvalý	k2eAgNnSc3d2	trvalejší
osídlení	osídlení	k1gNnSc3	osídlení
jižní	jižní	k2eAgFnSc2d1	jižní
a	a	k8xC	a
východní	východní	k2eAgFnSc2d1	východní
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
historici	historik	k1gMnPc1	historik
odhadují	odhadovat	k5eAaImIp3nP	odhadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
nájezdech	nájezd	k1gInPc6	nájezd
obávané	obávaný	k2eAgFnSc2d1	obávaná
krymskotatarské	krymskotatarský	k2eAgFnSc2d1	krymskotatarský
jízdy	jízda	k1gFnSc2	jízda
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
15	[number]	k4	15
<g/>
.	.	kIx.	.
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byly	být	k5eAaImAgFnP	být
odvlečeny	odvléct	k5eAaPmNgFnP	odvléct
a	a	k8xC	a
prodány	prodat	k5eAaPmNgFnP	prodat
do	do	k7c2	do
otroctví	otroctví	k1gNnSc2	otroctví
v	v	k7c6	v
Osmanské	osmanský	k2eAgFnSc6d1	Osmanská
říši	říš	k1gFnSc6	říš
až	až	k9	až
3	[number]	k4	3
miliony	milion	k4xCgInPc1	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Chmelnického	Chmelnický	k2eAgNnSc2d1	Chmelnický
povstání	povstání	k1gNnSc2	povstání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1648	[number]	k4	1648
došlo	dojít	k5eAaPmAgNnS	dojít
po	po	k7c6	po
několika	několik	k4yIc6	několik
menších	malý	k2eAgFnPc6d2	menší
bouřích	bouř	k1gFnPc6	bouř
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
k	k	k7c3	k
velkému	velký	k2eAgNnSc3d1	velké
kozáckému	kozácký	k2eAgNnSc3d1	kozácké
povstání	povstání	k1gNnSc3	povstání
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vzešlo	vzejít	k5eAaPmAgNnS	vzejít
ze	z	k7c2	z
Záporožské	záporožský	k2eAgFnSc2d1	Záporožská
Siče	Sič	k1gFnSc2	Sič
a	a	k8xC	a
které	který	k3yRgNnSc1	který
vedl	vést	k5eAaImAgMnS	vést
Bohdan	Bohdan	k1gMnSc1	Bohdan
Chmelnický	Chmelnický	k2eAgMnSc1d1	Chmelnický
<g/>
.	.	kIx.	.
</s>
<s>
Tomu	ten	k3xDgMnSc3	ten
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
ve	v	k7c6	v
spojenectví	spojenectví	k1gNnSc6	spojenectví
s	s	k7c7	s
krymskými	krymský	k2eAgMnPc7d1	krymský
Tatary	Tatar	k1gMnPc7	Tatar
v	v	k7c6	v
několika	několik	k4yIc6	několik
bitvách	bitva	k1gFnPc6	bitva
porazit	porazit	k5eAaPmF	porazit
polská	polský	k2eAgNnPc4d1	polské
vojska	vojsko	k1gNnPc4	vojsko
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
Kozáckého	kozácký	k2eAgInSc2d1	kozácký
hetmanátu	hetmanát	k1gInSc2	hetmanát
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
vnímán	vnímán	k2eAgMnSc1d1	vnímán
jako	jako	k8xC	jako
předchůdce	předchůdce	k1gMnSc1	předchůdce
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Poražen	porazit	k5eAaPmNgInS	porazit
byl	být	k5eAaImAgInS	být
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1651	[number]	k4	1651
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Berestečka	Berestečko	k1gNnSc2	Berestečko
<g/>
.	.	kIx.	.
</s>
<s>
Chmelnického	Chmelnický	k2eAgNnSc2d1	Chmelnický
povstání	povstání	k1gNnSc2	povstání
bylo	být	k5eAaImAgNnS	být
provázeno	provázet	k5eAaImNgNnS	provázet
protižidovskými	protižidovský	k2eAgInPc7d1	protižidovský
pogromy	pogrom	k1gInPc7	pogrom
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
vražděním	vraždění	k1gNnSc7	vraždění
Poláků	polák	k1gInPc2	polák
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
šlechticů	šlechtic	k1gMnPc2	šlechtic
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1654	[number]	k4	1654
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
obrátil	obrátit	k5eAaPmAgMnS	obrátit
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
k	k	k7c3	k
ruskému	ruský	k2eAgMnSc3d1	ruský
caru	car	k1gMnSc3	car
Alexejovi	Alexej	k1gMnSc3	Alexej
<g/>
.	.	kIx.	.
</s>
<s>
Perejaslavská	Perejaslavský	k2eAgFnSc1d1	Perejaslavský
rada	rada	k1gFnSc1	rada
však	však	k9	však
znamenala	znamenat	k5eAaImAgFnS	znamenat
ztrátu	ztráta	k1gFnSc4	ztráta
suverenity	suverenita	k1gFnSc2	suverenita
pro	pro	k7c4	pro
ukrajinská	ukrajinský	k2eAgNnPc4d1	ukrajinské
území	území	k1gNnPc4	území
<g/>
.	.	kIx.	.
</s>
<s>
Boje	boj	k1gInPc1	boj
pak	pak	k9	pak
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
moskevské	moskevský	k2eAgFnSc2d1	Moskevská
Rusi	Rus	k1gFnSc2	Rus
proti	proti	k7c3	proti
Polsku	Polsko	k1gNnSc3	Polsko
pokračovaly	pokračovat	k5eAaImAgFnP	pokračovat
dál	daleko	k6eAd2	daleko
a	a	k8xC	a
skončily	skončit	k5eAaPmAgFnP	skončit
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1667	[number]	k4	1667
mírem	mír	k1gInSc7	mír
v	v	k7c6	v
Andrušově	Andrušův	k2eAgNnSc6d1	Andrušův
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Chmelnického	Chmelnický	k2eAgInSc2d1	Chmelnický
<g/>
,	,	kIx,	,
Polsko-Litevská	polskoitevský	k2eAgFnSc1d1	polsko-litevská
unie	unie	k1gFnSc1	unie
došla	dojít	k5eAaPmAgFnS	dojít
ke	k	k7c3	k
kompromisu	kompromis	k1gInSc3	kompromis
s	s	k7c7	s
ukrajinskými	ukrajinský	k2eAgInPc7d1	ukrajinský
Kozáky	kozák	k1gInPc7	kozák
tzv.	tzv.	kA	tzv.
Dohodou	dohoda	k1gFnSc7	dohoda
z	z	k7c2	z
Haďače	Haďač	k1gInSc2	Haďač
16	[number]	k4	16
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1658	[number]	k4	1658
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
ustavila	ustavit	k5eAaPmAgFnS	ustavit
novou	nový	k2eAgFnSc4d1	nová
Unii	unie	k1gFnSc4	unie
<g/>
,	,	kIx,	,
schválenou	schválený	k2eAgFnSc4d1	schválená
polsko-litevským	polskoitevský	k2eAgInSc7d1	polsko-litevský
sněmem	sněm	k1gInSc7	sněm
12	[number]	k4	12
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1659	[number]	k4	1659
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
trojčlennou	trojčlenný	k2eAgFnSc7d1	trojčlenná
(	(	kIx(	(
<g/>
Polské	polský	k2eAgNnSc1d1	polské
království	království	k1gNnSc1	království
<g/>
,	,	kIx,	,
Litevské	litevský	k2eAgNnSc1d1	litevské
velkoknížectví	velkoknížectví	k1gNnSc1	velkoknížectví
a	a	k8xC	a
Hetmanská	hetmanský	k2eAgFnSc1d1	hetmanský
Rus	Rus	k1gFnSc1	Rus
<g/>
)	)	kIx)	)
a	a	k8xC	a
zaručila	zaručit	k5eAaPmAgFnS	zaručit
záporožským	záporožský	k2eAgMnPc3d1	záporožský
Kozákům	Kozák	k1gMnPc3	Kozák
–	–	k?	–
tj.	tj.	kA	tj.
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
,	,	kIx,	,
tradičně	tradičně	k6eAd1	tradičně
zvané	zvaný	k2eAgInPc4d1	zvaný
Rusí	Rus	k1gFnSc7	Rus
–	–	k?	–
rovné	rovný	k2eAgNnSc4d1	rovné
postavení	postavení	k1gNnSc4	postavení
vůči	vůči	k7c3	vůči
Polskému	polský	k2eAgNnSc3d1	polské
království	království	k1gNnSc3	království
a	a	k8xC	a
Litevskému	litevský	k2eAgNnSc3d1	litevské
velkoknížectví	velkoknížectví	k1gNnSc3	velkoknížectví
<g/>
,	,	kIx,	,
s	s	k7c7	s
vlastními	vlastní	k2eAgInPc7d1	vlastní
úřady	úřad	k1gInPc7	úřad
–	–	k?	–
maršálkem	maršálek	k1gMnSc7	maršálek
(	(	kIx(	(
<g/>
premiérem	premiér	k1gMnSc7	premiér
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vojskem	vojsko	k1gNnSc7	vojsko
a	a	k8xC	a
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
soudem	soud	k1gInSc7	soud
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
dohoda	dohoda	k1gFnSc1	dohoda
také	také	k9	také
ustavovala	ustavovat	k5eAaImAgFnS	ustavovat
rovnoprávnost	rovnoprávnost	k1gFnSc4	rovnoprávnost
náboženskou	náboženský	k2eAgFnSc4d1	náboženská
pro	pro	k7c4	pro
pravoslaví	pravoslaví	k1gNnSc4	pravoslaví
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
pozdějším	pozdní	k2eAgFnPc3d2	pozdější
válečným	válečný	k2eAgFnPc3d1	válečná
událostem	událost	k1gFnPc3	událost
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
včetně	včetně	k7c2	včetně
švédské	švédský	k2eAgFnSc2d1	švédská
potopy	potopa	k1gFnSc2	potopa
a	a	k8xC	a
války	válka	k1gFnSc2	válka
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
a	a	k8xC	a
také	také	k9	také
z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
strany	strana	k1gFnSc2	strana
vzpoury	vzpoura	k1gFnSc2	vzpoura
Kozáků	kozák	k1gInPc2	kozák
proti	proti	k7c3	proti
Ivanu	Ivan	k1gMnSc3	Ivan
Vyhovskému	Vyhovský	k1gMnSc3	Vyhovský
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
dohodu	dohoda	k1gFnSc4	dohoda
z	z	k7c2	z
Hadače	hadač	k1gMnSc2	hadač
uzavíral	uzavírat	k5eAaImAgInS	uzavírat
<g/>
,	,	kIx,	,
nebyla	být	k5eNaImAgFnS	být
ale	ale	k9	ale
tato	tento	k3xDgFnSc1	tento
dohoda	dohoda	k1gFnSc1	dohoda
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
naplněna	naplnit	k5eAaPmNgFnS	naplnit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
letech	léto	k1gNnPc6	léto
nepřetržitého	přetržitý	k2eNgNnSc2d1	nepřetržité
válčení	válčení	k1gNnSc2	válčení
byla	být	k5eAaImAgFnS	být
pak	pak	k6eAd1	pak
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1667	[number]	k4	1667
dohoda	dohoda	k1gFnSc1	dohoda
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
polsko-ruskou	polskouský	k2eAgFnSc7d1	polsko-ruská
smlouvou	smlouva	k1gFnSc7	smlouva
Andrusovo	Andrusův	k2eAgNnSc1d1	Andrusův
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
rozdělila	rozdělit	k5eAaPmAgFnS	rozdělit
ukrajinské	ukrajinský	k2eAgNnSc4d1	ukrajinské
území	území	k1gNnSc4	území
mezi	mezi	k7c4	mezi
Polsko-litevskou	polskoitevský	k2eAgFnSc4d1	polsko-litevská
unii	unie	k1gFnSc4	unie
a	a	k8xC	a
Rusko	Rusko	k1gNnSc4	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
byla	být	k5eAaImAgFnS	být
tehdy	tehdy	k6eAd1	tehdy
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
podle	podle	k7c2	podle
řeky	řeka	k1gFnSc2	řeka
Dněpru	Dněpr	k1gInSc2	Dněpr
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
dělení	dělení	k1gNnSc6	dělení
Polska	Polsko	k1gNnSc2	Polsko
v	v	k7c6	v
letech	let	k1gInPc6	let
1772	[number]	k4	1772
<g/>
–	–	k?	–
<g/>
1795	[number]	k4	1795
byla	být	k5eAaImAgFnS	být
k	k	k7c3	k
Rusku	Rusko	k1gNnSc3	Rusko
připojena	připojit	k5eAaPmNgFnS	připojit
i	i	k9	i
většina	většina	k1gFnSc1	většina
pravobřežní	pravobřežní	k2eAgFnSc2d1	pravobřežní
(	(	kIx(	(
<g/>
střední	střední	k2eAgFnSc2d1	střední
a	a	k8xC	a
západní	západní	k2eAgFnSc2d1	západní
<g/>
)	)	kIx)	)
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
kromě	kromě	k7c2	kromě
Haliče	Halič	k1gFnSc2	Halič
a	a	k8xC	a
Bukoviny	Bukovina	k1gFnSc2	Bukovina
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
získala	získat	k5eAaPmAgFnS	získat
habsburská	habsburský	k2eAgFnSc1d1	habsburská
monarchie	monarchie	k1gFnSc1	monarchie
(	(	kIx(	(
<g/>
1772	[number]	k4	1772
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
tak	tak	k9	tak
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
ztratila	ztratit	k5eAaPmAgFnS	ztratit
i	i	k9	i
jen	jen	k9	jen
náznak	náznak	k1gInSc4	náznak
jakékoliv	jakýkoliv	k3yIgFnSc2	jakýkoliv
autonomie	autonomie	k1gFnSc2	autonomie
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
po	po	k7c6	po
zániku	zánik	k1gInSc6	zánik
Záporožské	záporožský	k2eAgFnSc2d1	Záporožská
Siče	Sič	k1gFnSc2	Sič
roku	rok	k1gInSc2	rok
1775	[number]	k4	1775
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
také	také	k9	také
zlikvidována	zlikvidován	k2eAgFnSc1d1	zlikvidována
tatarská	tatarský	k2eAgFnSc1d1	tatarská
a	a	k8xC	a
turecká	turecký	k2eAgFnSc1d1	turecká
moc	moc	k1gFnSc1	moc
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
.	.	kIx.	.
</s>
<s>
Carevna	carevna	k1gFnSc1	carevna
Kateřina	Kateřina	k1gFnSc1	Kateřina
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Veliká	veliký	k2eAgFnSc1d1	veliká
a	a	k8xC	a
její	její	k3xOp3gFnSc4	její
rádce	rádce	k1gMnSc1	rádce
kníže	kníže	k1gMnSc1	kníže
Potěmkin	Potěmkin	k1gMnSc1	Potěmkin
založili	založit	k5eAaPmAgMnP	založit
v	v	k7c6	v
nově	nova	k1gFnSc6	nova
dobyté	dobytý	k2eAgFnSc6d1	dobytá
tzv.	tzv.	kA	tzv.
Nové	Nové	k2eAgFnSc6d1	Nové
Rusi	Rus	k1gFnSc6	Rus
několik	několik	k4yIc4	několik
významných	významný	k2eAgNnPc2d1	významné
měst	město	k1gNnPc2	město
<g/>
:	:	kIx,	:
mj.	mj.	kA	mj.
Oděsu	Oděsa	k1gFnSc4	Oděsa
a	a	k8xC	a
Dněpropetrovsk	Dněpropetrovsk	k1gInSc4	Dněpropetrovsk
<g/>
.	.	kIx.	.
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
dobou	doba	k1gFnSc7	doba
tvrdého	tvrdý	k2eAgInSc2d1	tvrdý
národnostního	národnostní	k2eAgInSc2d1	národnostní
útlaku	útlak	k1gInSc2	útlak
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Ruského	ruský	k2eAgNnSc2d1	ruské
impéria	impérium	k1gNnSc2	impérium
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
dobou	doba	k1gFnSc7	doba
ukrajinského	ukrajinský	k2eAgNnSc2d1	ukrajinské
národního	národní	k2eAgNnSc2d1	národní
obrození	obrození	k1gNnSc2	obrození
<g/>
,	,	kIx,	,
zprvu	zprvu	k6eAd1	zprvu
kulturního	kulturní	k2eAgNnSc2d1	kulturní
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
také	také	k9	také
politického	politický	k2eAgNnSc2d1	politické
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
snahy	snaha	k1gFnPc1	snaha
se	se	k3xPyFc4	se
po	po	k7c6	po
carských	carský	k2eAgFnPc6d1	carská
represích	represe	k1gFnPc6	represe
a	a	k8xC	a
zákazu	zákaz	k1gInSc6	zákaz
užívání	užívání	k1gNnSc2	užívání
ukrajinštiny	ukrajinština	k1gFnSc2	ukrajinština
(	(	kIx(	(
<g/>
1876	[number]	k4	1876
<g/>
)	)	kIx)	)
soustředily	soustředit	k5eAaPmAgFnP	soustředit
především	především	k9	především
do	do	k7c2	do
habsburské	habsburský	k2eAgFnSc2d1	habsburská
části	část	k1gFnSc2	část
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
mnohem	mnohem	k6eAd1	mnohem
liberálnější	liberální	k2eAgInSc1d2	liberálnější
režim	režim	k1gInSc1	režim
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
se	se	k3xPyFc4	se
–	–	k?	–
byť	byť	k8xS	byť
pomalu	pomalu	k6eAd1	pomalu
–	–	k?	–
rozvíjelo	rozvíjet	k5eAaImAgNnS	rozvíjet
také	také	k9	také
hospodářství	hospodářství	k1gNnSc4	hospodářství
<g/>
,	,	kIx,	,
začalo	začít	k5eAaPmAgNnS	začít
se	se	k3xPyFc4	se
s	s	k7c7	s
výstavbou	výstavba	k1gFnSc7	výstavba
železniční	železniční	k2eAgFnSc2d1	železniční
sítě	síť	k1gFnSc2	síť
a	a	k8xC	a
masivní	masivní	k2eAgFnSc7d1	masivní
těžbou	těžba	k1gFnSc7	těžba
uhlí	uhlí	k1gNnSc2	uhlí
<g/>
;	;	kIx,	;
rychle	rychle	k6eAd1	rychle
rostla	růst	k5eAaImAgFnS	růst
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Kyjev	Kyjev	k1gInSc1	Kyjev
a	a	k8xC	a
Charkov	Charkov	k1gInSc1	Charkov
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Západoukrajinská	Západoukrajinský	k2eAgFnSc1d1	Západoukrajinská
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
Ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
Ukrajinský	ukrajinský	k2eAgInSc1d1	ukrajinský
stát	stát	k1gInSc1	stát
<g/>
,	,	kIx,	,
Ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
sovětská	sovětský	k2eAgFnSc1d1	sovětská
socialistická	socialistický	k2eAgFnSc1d1	socialistická
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
Podkarpatská	podkarpatský	k2eAgFnSc1d1	Podkarpatská
Rus	Rus	k1gFnSc1	Rus
a	a	k8xC	a
Polsko-ukrajinská	polskokrajinský	k2eAgFnSc1d1	polsko-ukrajinská
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
a	a	k8xC	a
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
ruské	ruský	k2eAgFnSc2d1	ruská
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
části	část	k1gFnSc6	část
ukrajinského	ukrajinský	k2eAgNnSc2d1	ukrajinské
území	území	k1gNnSc2	území
sestaveno	sestavit	k5eAaPmNgNnS	sestavit
několik	několik	k4yIc1	několik
ukrajinských	ukrajinský	k2eAgFnPc2d1	ukrajinská
vlád	vláda	k1gFnPc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
republika	republika	k1gFnSc1	republika
–	–	k?	–
Ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
UNR	UNR	kA	UNR
<g/>
)	)	kIx)	)
–	–	k?	–
existovala	existovat	k5eAaImAgFnS	existovat
v	v	k7c6	v
letech	let	k1gInPc6	let
1917	[number]	k4	1917
<g/>
–	–	k?	–
<g/>
1919	[number]	k4	1919
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
však	však	k9	však
prakticky	prakticky	k6eAd1	prakticky
pouze	pouze	k6eAd1	pouze
válčící	válčící	k2eAgFnSc7d1	válčící
stranou	strana	k1gFnSc7	strana
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
ruské	ruský	k2eAgFnSc2d1	ruská
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
a	a	k8xC	a
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
pod	pod	k7c7	pod
náporem	nápor	k1gInSc7	nápor
ruských	ruský	k2eAgMnPc2d1	ruský
bolševiků	bolševik	k1gMnPc2	bolševik
<g/>
.	.	kIx.	.
</s>
<s>
Západoukrajinská	Západoukrajinský	k2eAgFnSc1d1	Západoukrajinská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
ZUNR	ZUNR	kA	ZUNR
<g/>
)	)	kIx)	)
existovala	existovat	k5eAaImAgFnS	existovat
v	v	k7c6	v
letech	let	k1gInPc6	let
1918	[number]	k4	1918
<g/>
–	–	k?	–
<g/>
1919	[number]	k4	1919
a	a	k8xC	a
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
své	svůj	k3xOyFgFnSc2	svůj
krátké	krátký	k2eAgFnSc2d1	krátká
existence	existence	k1gFnSc2	existence
vedla	vést	k5eAaImAgFnS	vést
boj	boj	k1gInSc4	boj
proti	proti	k7c3	proti
polským	polský	k2eAgFnPc3d1	polská
jednotkám	jednotka	k1gFnPc3	jednotka
a	a	k8xC	a
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
pod	pod	k7c7	pod
náporem	nápor	k1gInSc7	nápor
polských	polský	k2eAgMnPc2d1	polský
a	a	k8xC	a
rumunských	rumunský	k2eAgMnPc2d1	rumunský
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
části	část	k1gFnPc1	část
se	se	k3xPyFc4	se
nakrátko	nakrátko	k6eAd1	nakrátko
spojili	spojit	k5eAaPmAgMnP	spojit
22	[number]	k4	22
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1919	[number]	k4	1919
(	(	kIx(	(
<g/>
Akt	akt	k1gInSc1	akt
Zluky	Zluka	k1gFnSc2	Zluka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
praktického	praktický	k2eAgNnSc2d1	praktické
hlediska	hledisko	k1gNnSc2	hledisko
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
symbolický	symbolický	k2eAgInSc4d1	symbolický
akt	akt	k1gInSc4	akt
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1919	[number]	k4	1919
bolševici	bolševik	k1gMnPc1	bolševik
dobyli	dobýt	k5eAaPmAgMnP	dobýt
Kyjev	Kyjev	k1gInSc4	Kyjev
a	a	k8xC	a
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
tak	tak	k6eAd1	tak
upadla	upadnout	k5eAaPmAgFnS	upadnout
do	do	k7c2	do
chaosu	chaos	k1gInSc2	chaos
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
obě	dva	k4xCgFnPc1	dva
části	část	k1gFnPc1	část
státu	stát	k1gInSc2	stát
bojovaly	bojovat	k5eAaImAgFnP	bojovat
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
sobě	se	k3xPyFc3	se
své	svůj	k3xOyFgInPc4	svůj
vlastní	vlastní	k2eAgInPc4d1	vlastní
války	válek	k1gInPc4	válek
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgFnSc1d1	západní
část	část	k1gFnSc1	část
se	se	k3xPyFc4	se
musela	muset	k5eAaImAgFnS	muset
potýkat	potýkat	k5eAaImF	potýkat
s	s	k7c7	s
invazí	invaze	k1gFnSc7	invaze
Poláků	Polák	k1gMnPc2	Polák
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
zbytek	zbytek	k1gInSc1	zbytek
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
bojoval	bojovat	k5eAaImAgInS	bojovat
na	na	k7c6	na
východě	východ	k1gInSc6	východ
s	s	k7c7	s
ruskými	ruský	k2eAgMnPc7d1	ruský
bolševiky	bolševik	k1gMnPc7	bolševik
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
ukrajinským	ukrajinský	k2eAgMnSc7d1	ukrajinský
prezidentem	prezident	k1gMnSc7	prezident
byl	být	k5eAaImAgMnS	být
historik	historik	k1gMnSc1	historik
<g/>
,	,	kIx,	,
literát	literát	k1gMnSc1	literát
a	a	k8xC	a
filosof	filosof	k1gMnSc1	filosof
Mychajlo	Mychajlo	k1gNnSc1	Mychajlo
Hruševskyj	Hruševskyj	k1gMnSc1	Hruševskyj
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
bolševiků	bolševik	k1gMnPc2	bolševik
a	a	k8xC	a
bělogvardějců	bělogvardějec	k1gMnPc2	bělogvardějec
zde	zde	k6eAd1	zde
byli	být	k5eAaImAgMnP	být
anarchisté	anarchista	k1gMnPc1	anarchista
(	(	kIx(	(
<g/>
Nestor	Nestor	k1gMnSc1	Nestor
Machno	Machno	k?	Machno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nacionalisté	nacionalista	k1gMnPc1	nacionalista
(	(	kIx(	(
<g/>
Symon	Symon	k1gMnSc1	Symon
Petljura	Petljur	k1gMnSc2	Petljur
<g/>
)	)	kIx)	)
a	a	k8xC	a
hejtman	hejtman	k1gMnSc1	hejtman
Pavlo	Pavla	k1gFnSc5	Pavla
Skoropadskyj	Skoropadskyj	k1gInSc1	Skoropadskyj
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
bojovalo	bojovat	k5eAaImAgNnS	bojovat
nejdéle	dlouho	k6eAd3	dlouho
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
boji	boj	k1gInSc6	boj
o	o	k7c4	o
národní	národní	k2eAgFnSc4d1	národní
samostatnost	samostatnost	k1gFnSc4	samostatnost
byli	být	k5eAaImAgMnP	být
Ukrajinci	Ukrajinec	k1gMnPc1	Ukrajinec
poraženi	poražen	k2eAgMnPc1d1	poražen
<g/>
:	:	kIx,	:
západní	západní	k2eAgFnSc1d1	západní
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
se	se	k3xPyFc4	se
v	v	k7c6	v
letech	let	k1gInPc6	let
1920	[number]	k4	1920
<g/>
–	–	k?	–
<g/>
1921	[number]	k4	1921
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
2	[number]	k4	2
<g/>
.	.	kIx.	.
polské	polský	k2eAgFnSc2d1	polská
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
SSR	SSR	kA	SSR
jakožto	jakožto	k8xS	jakožto
řadová	řadový	k2eAgFnSc1d1	řadová
republika	republika	k1gFnSc1	republika
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
s	s	k7c7	s
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
ve	v	k7c6	v
východoukrajinském	východoukrajinský	k2eAgInSc6d1	východoukrajinský
Charkově	Charkov	k1gInSc6	Charkov
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roku	rok	k1gInSc6	rok
1934	[number]	k4	1934
přeneseno	přenést	k5eAaPmNgNnS	přenést
do	do	k7c2	do
Kyjeva	Kyjev	k1gInSc2	Kyjev
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1926	[number]	k4	1926
<g/>
–	–	k?	–
<g/>
40	[number]	k4	40
existovala	existovat	k5eAaImAgFnS	existovat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Ukrajinské	ukrajinský	k2eAgFnSc2d1	ukrajinská
SSR	SSR	kA	SSR
autonomní	autonomní	k2eAgFnSc1d1	autonomní
republika	republika	k1gFnSc1	republika
Moldavanů	Moldavan	k1gMnPc2	Moldavan
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
se	se	k3xPyFc4	se
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
konference	konference	k1gFnSc1	konference
Ukrajinců	Ukrajinec	k1gMnPc2	Ukrajinec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
emigrovali	emigrovat	k5eAaBmAgMnP	emigrovat
z	z	k7c2	z
vlasti	vlast	k1gFnSc2	vlast
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
založili	založit	k5eAaPmAgMnP	založit
Organizaci	organizace	k1gFnSc4	organizace
ukrajinských	ukrajinský	k2eAgMnPc2d1	ukrajinský
nacionalistů	nacionalista	k1gMnPc2	nacionalista
(	(	kIx(	(
<g/>
OUN	OUN	kA	OUN
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
usilovala	usilovat	k5eAaImAgFnS	usilovat
o	o	k7c4	o
samostatný	samostatný	k2eAgInSc4d1	samostatný
ukrajinský	ukrajinský	k2eAgInSc4d1	ukrajinský
stát	stát	k1gInSc4	stát
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
násilnou	násilný	k2eAgFnSc7d1	násilná
formou	forma	k1gFnSc7	forma
<g/>
)	)	kIx)	)
a	a	k8xC	a
významně	významně	k6eAd1	významně
zasahovala	zasahovat	k5eAaImAgNnP	zasahovat
do	do	k7c2	do
dalšího	další	k2eAgInSc2d1	další
osudu	osud	k1gInSc2	osud
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
sovětská	sovětský	k2eAgFnSc1d1	sovětská
socialistická	socialistický	k2eAgFnSc1d1	socialistická
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
jakožto	jakožto	k8xS	jakožto
součást	součást	k1gFnSc1	součást
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
byla	být	k5eAaImAgFnS	být
vystavena	vystavit	k5eAaPmNgFnS	vystavit
tvrdým	tvrdý	k2eAgFnPc3d1	tvrdá
zkouškám	zkouška	k1gFnPc3	zkouška
<g/>
;	;	kIx,	;
nejtěžší	těžký	k2eAgFnPc1d3	nejtěžší
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
byl	být	k5eAaImAgMnS	být
hladomor	hladomor	k1gMnSc1	hladomor
(	(	kIx(	(
<g/>
ukrajinsky	ukrajinsky	k6eAd1	ukrajinsky
Holodomor	Holodomor	k1gInSc1	Holodomor
<g/>
)	)	kIx)	)
v	v	k7c6	v
letech	let	k1gInPc6	let
1932	[number]	k4	1932
<g/>
–	–	k?	–
<g/>
1933	[number]	k4	1933
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
vyvolán	vyvolat	k5eAaPmNgInS	vyvolat
násilnou	násilný	k2eAgFnSc7d1	násilná
kolektivizací	kolektivizace	k1gFnSc7	kolektivizace
zemědělství	zemědělství	k1gNnSc2	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
katastrofa	katastrofa	k1gFnSc1	katastrofa
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
zřejmě	zřejmě	k6eAd1	zřejmě
alespoň	alespoň	k9	alespoň
zčásti	zčásti	k6eAd1	zčásti
úmyslně	úmyslně	k6eAd1	úmyslně
vyvolána	vyvolat	k5eAaPmNgFnS	vyvolat
Stalinovou	Stalinův	k2eAgFnSc7d1	Stalinova
politikou	politika	k1gFnSc7	politika
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
obestřena	obestřít	k5eAaPmNgFnS	obestřít
nejasnostmi	nejasnost	k1gFnPc7	nejasnost
a	a	k8xC	a
dohady	dohad	k1gInPc7	dohad
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
stalinský	stalinský	k2eAgInSc1d1	stalinský
režim	režim	k1gInSc1	režim
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
přikázal	přikázat	k5eAaPmAgMnS	přikázat
mlčet	mlčet	k5eAaImF	mlčet
<g/>
.	.	kIx.	.
</s>
<s>
Odhady	odhad	k1gInPc1	odhad
počtu	počet	k1gInSc2	počet
obětí	oběť	k1gFnPc2	oběť
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
různí	různý	k2eAgMnPc1d1	různý
<g/>
;	;	kIx,	;
historici	historik	k1gMnPc1	historik
hovoří	hovořit	k5eAaImIp3nP	hovořit
nejčastěji	často	k6eAd3	často
o	o	k7c4	o
2,5	[number]	k4	2,5
až	až	k9	až
5	[number]	k4	5
milionech	milion	k4xCgInPc6	milion
mrtvých	mrtvý	k1gMnPc2	mrtvý
hladem	hlad	k1gInSc7	hlad
<g/>
.	.	kIx.	.
</s>
<s>
Nejhůře	zle	k6eAd3	zle
byly	být	k5eAaImAgFnP	být
zasaženy	zasažen	k2eAgFnPc1d1	zasažena
Ukrajinská	ukrajinský	k2eAgNnPc4d1	ukrajinské
SSR	SSR	kA	SSR
<g/>
,	,	kIx,	,
severní	severní	k2eAgInSc4d1	severní
Kazachstán	Kazachstán	k1gInSc4	Kazachstán
a	a	k8xC	a
Kubáň	Kubáň	k1gFnSc4	Kubáň
v	v	k7c6	v
nynějším	nynější	k2eAgNnSc6d1	nynější
jižním	jižní	k2eAgNnSc6d1	jižní
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgInPc1d2	menší
hladomory	hladomor	k1gInPc1	hladomor
postihly	postihnout	k5eAaPmAgInP	postihnout
Ukrajinu	Ukrajina	k1gFnSc4	Ukrajina
také	také	k9	také
v	v	k7c6	v
letech	let	k1gInPc6	let
1921	[number]	k4	1921
<g/>
–	–	k?	–
<g/>
1923	[number]	k4	1923
a	a	k8xC	a
1946	[number]	k4	1946
<g/>
–	–	k?	–
<g/>
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
v	v	k7c6	v
poválečných	poválečný	k2eAgNnPc6d1	poválečné
obdobích	období	k1gNnPc6	období
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1941	[number]	k4	1941
<g/>
–	–	k?	–
<g/>
1944	[number]	k4	1944
byla	být	k5eAaImAgFnS	být
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
okupována	okupovat	k5eAaBmNgFnS	okupovat
Německou	německý	k2eAgFnSc7d1	německá
říší	říš	k1gFnSc7	říš
<g/>
;	;	kIx,	;
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
přibližně	přibližně	k6eAd1	přibližně
7	[number]	k4	7
milionů	milion	k4xCgInPc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
německé	německý	k2eAgFnSc6d1	německá
porážce	porážka	k1gFnSc6	porážka
byla	být	k5eAaImAgFnS	být
k	k	k7c3	k
Ukrajině	Ukrajina	k1gFnSc3	Ukrajina
připojena	připojit	k5eAaPmNgFnS	připojit
její	její	k3xOp3gFnSc1	její
nynější	nynější	k2eAgFnSc1d1	nynější
západní	západní	k2eAgFnSc1d1	západní
část	část	k1gFnSc1	část
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
západní	západní	k2eAgFnSc4d1	západní
Volyň	Volyň	k1gFnSc4	Volyň
a	a	k8xC	a
východní	východní	k2eAgFnSc4d1	východní
Halič	Halič	k1gFnSc4	Halič
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
před	před	k7c7	před
válkou	válka	k1gFnSc7	válka
patřila	patřit	k5eAaImAgFnS	patřit
Polsku	Polska	k1gFnSc4	Polska
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
území	území	k1gNnPc1	území
byla	být	k5eAaImAgNnP	být
krátkodobě	krátkodobě	k6eAd1	krátkodobě
obsazena	obsadit	k5eAaPmNgNnP	obsadit
sovětskými	sovětský	k2eAgNnPc7d1	sovětské
vojsky	vojsko	k1gNnPc7	vojsko
již	již	k6eAd1	již
v	v	k7c6	v
září	září	k1gNnSc6	září
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
1939	[number]	k4	1939
<g/>
/	/	kIx~	/
<g/>
1940	[number]	k4	1940
pak	pak	k6eAd1	pak
proběhla	proběhnout	k5eAaPmAgNnP	proběhnout
zfalšovaná	zfalšovaný	k2eAgNnPc1d1	zfalšované
referenda	referendum	k1gNnPc1	referendum
o	o	k7c4	o
připojení	připojení	k1gNnPc4	připojení
těchto	tento	k3xDgNnPc2	tento
území	území	k1gNnSc2	území
k	k	k7c3	k
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
Podobným	podobný	k2eAgInSc7d1	podobný
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
zfalšovaným	zfalšovaný	k2eAgNnSc7d1	zfalšované
referendem	referendum	k1gNnSc7	referendum
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
po	po	k7c4	po
obsazení	obsazení	k1gNnSc4	obsazení
Rudou	rudý	k2eAgFnSc7d1	rudá
armádou	armáda	k1gFnSc7	armáda
k	k	k7c3	k
Ukrajině	Ukrajina	k1gFnSc3	Ukrajina
připojena	připojen	k2eAgFnSc1d1	připojena
také	také	k6eAd1	také
Podkarpatská	podkarpatský	k2eAgFnSc1d1	Podkarpatská
Rus	Rus	k1gFnSc1	Rus
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
předválečnou	předválečný	k2eAgFnSc4d1	předválečná
součástí	součást	k1gFnPc2	součást
První	první	k4xOgFnSc2	první
Československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
součástí	součást	k1gFnSc7	součást
Ukrajinské	ukrajinský	k2eAgFnSc2d1	ukrajinská
SSR	SSR	kA	SSR
i	i	k8xC	i
poloostrov	poloostrov	k1gInSc1	poloostrov
Krym	Krym	k1gInSc1	Krym
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
součástí	součást	k1gFnPc2	součást
Ruské	ruský	k2eAgFnSc2d1	ruská
SFSR	SFSR	kA	SFSR
<g/>
.	.	kIx.	.
</s>
<s>
Předání	předání	k1gNnSc1	předání
tohoto	tento	k3xDgNnSc2	tento
převážně	převážně	k6eAd1	převážně
ruskojazyčného	ruskojazyčný	k2eAgNnSc2d1	ruskojazyčné
(	(	kIx(	(
<g/>
a	a	k8xC	a
kdysi	kdysi	k6eAd1	kdysi
Ruskem	Rusko	k1gNnSc7	Rusko
na	na	k7c6	na
Tatarech	tatar	k1gInPc6	tatar
dobytého	dobytý	k2eAgNnSc2d1	dobyté
<g/>
)	)	kIx)	)
území	území	k1gNnSc2	území
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
napětí	napětí	k1gNnSc1	napětí
<g/>
,	,	kIx,	,
přetrvávající	přetrvávající	k2eAgNnPc4d1	přetrvávající
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Nikita	Nikita	k1gFnSc1	Nikita
Chruščov	Chruščov	k1gInSc1	Chruščov
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
předání	předání	k1gNnSc3	předání
Krymu	Krym	k1gInSc2	Krym
osobně	osobně	k6eAd1	osobně
prosadil	prosadit	k5eAaPmAgInS	prosadit
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
40	[number]	k4	40
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
hlavou	hlava	k1gFnSc7	hlava
Ukrajinské	ukrajinský	k2eAgFnPc4d1	ukrajinská
SSR	SSR	kA	SSR
a	a	k8xC	a
v	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c6	na
Donbasu	Donbas	k1gInSc6	Donbas
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
pocházeli	pocházet	k5eAaImAgMnP	pocházet
i	i	k8xC	i
jeho	jeho	k3xOp3gMnSc1	jeho
nástupce	nástupce	k1gMnSc1	nástupce
Leonid	Leonida	k1gFnPc2	Leonida
Brežněv	Brežněv	k1gMnSc1	Brežněv
<g/>
,	,	kIx,	,
velitel	velitel	k1gMnSc1	velitel
vojsk	vojsko	k1gNnPc2	vojsko
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
Andrej	Andrej	k1gMnSc1	Andrej
Grečko	Grečko	k6eAd1	Grečko
a	a	k8xC	a
sovětští	sovětský	k2eAgMnPc1d1	sovětský
maršálové	maršál	k1gMnPc1	maršál
Semjon	Semjona	k1gFnPc2	Semjona
Timošenko	Timošenka	k1gFnSc5	Timošenka
a	a	k8xC	a
Rodion	Rodion	k1gInSc4	Rodion
Malinovskij	Malinovskij	k1gFnSc2	Malinovskij
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
masivní	masivní	k2eAgFnSc1d1	masivní
industrializace	industrializace	k1gFnSc1	industrializace
a	a	k8xC	a
urbanizace	urbanizace	k1gFnSc1	urbanizace
především	především	k6eAd1	především
východní	východní	k2eAgFnSc2d1	východní
části	část	k1gFnSc2	část
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
zbudována	zbudován	k2eAgFnSc1d1	zbudována
kaskáda	kaskáda	k1gFnSc1	kaskáda
přehrad	přehrada	k1gFnPc2	přehrada
na	na	k7c6	na
Dněpru	Dněpr	k1gInSc6	Dněpr
<g/>
.	.	kIx.	.
</s>
<s>
Rozvoj	rozvoj	k1gInSc1	rozvoj
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
moderních	moderní	k2eAgFnPc2d1	moderní
technologií	technologie	k1gFnPc2	technologie
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
si	se	k3xPyFc3	se
vedle	vedle	k6eAd1	vedle
znečištění	znečištění	k1gNnSc4	znečištění
ovzduší	ovzduší	k1gNnSc2	ovzduší
vyžádal	vyžádat	k5eAaPmAgInS	vyžádat
krutou	krutý	k2eAgFnSc4d1	krutá
daň	daň	k1gFnSc4	daň
<g/>
:	:	kIx,	:
26	[number]	k4	26
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1986	[number]	k4	1986
došlo	dojít	k5eAaPmAgNnS	dojít
na	na	k7c6	na
severu	sever	k1gInSc6	sever
země	zem	k1gFnSc2	zem
k	k	k7c3	k
černobylské	černobylský	k2eAgFnSc3d1	Černobylská
katastrofě	katastrofa	k1gFnSc3	katastrofa
<g/>
,	,	kIx,	,
nejhorší	zlý	k2eAgFnSc3d3	nejhorší
havárii	havárie	k1gFnSc3	havárie
jaderné	jaderný	k2eAgFnSc2d1	jaderná
elektrárny	elektrárna	k1gFnSc2	elektrárna
ve	v	k7c6	v
světových	světový	k2eAgFnPc6d1	světová
dějinách	dějiny	k1gFnPc6	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Ukrajinsko-běloruské	ukrajinskoěloruský	k2eAgNnSc1d1	ukrajinsko-běloruský
pomezí	pomezí	k1gNnSc1	pomezí
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
zamořeno	zamořit	k5eAaPmNgNnS	zamořit
radioaktivním	radioaktivní	k2eAgInSc7d1	radioaktivní
spadem	spad	k1gInSc7	spad
<g/>
,	,	kIx,	,
území	území	k1gNnSc4	území
v	v	k7c6	v
okruhu	okruh	k1gInSc6	okruh
několika	několik	k4yIc2	několik
desítek	desítka	k1gFnPc2	desítka
kilometrů	kilometr	k1gInPc2	kilometr
muselo	muset	k5eAaImAgNnS	muset
být	být	k5eAaImF	být
vysídleno	vysídlit	k5eAaPmNgNnS	vysídlit
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Válka	válka	k1gFnSc1	válka
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
,	,	kIx,	,
Anexe	anexe	k1gFnSc1	anexe
Krymu	Krym	k1gInSc2	Krym
Ruskou	Ruska	k1gFnSc7	Ruska
federací	federace	k1gFnSc7	federace
<g/>
,	,	kIx,	,
Proruské	proruský	k2eAgInPc4d1	proruský
nepokoje	nepokoj	k1gInPc4	nepokoj
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
2014	[number]	k4	2014
a	a	k8xC	a
Ruská	ruský	k2eAgFnSc1d1	ruská
vojenská	vojenský	k2eAgFnSc1d1	vojenská
intervence	intervence	k1gFnSc1	intervence
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
–	–	k?	–
<g/>
současnost	současnost	k1gFnSc4	současnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
Verchovna	Verchovna	k1gFnSc1	Verchovna
rada	rada	k1gFnSc1	rada
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
(	(	kIx(	(
<g/>
parlament	parlament	k1gInSc4	parlament
<g/>
)	)	kIx)	)
suverenitu	suverenita	k1gFnSc4	suverenita
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
nezávislost	nezávislost	k1gFnSc4	nezávislost
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
oficiálně	oficiálně	k6eAd1	oficiálně
vyhlášena	vyhlášen	k2eAgFnSc1d1	vyhlášena
24	[number]	k4	24
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1991	[number]	k4	1991
současně	současně	k6eAd1	současně
s	s	k7c7	s
rozpadem	rozpad	k1gInSc7	rozpad
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Prohlášení	prohlášení	k1gNnSc1	prohlášení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
bylo	být	k5eAaImAgNnS	být
potvrzeno	potvrdit	k5eAaPmNgNnS	potvrdit
referendem	referendum	k1gNnSc7	referendum
o	o	k7c6	o
nezávislosti	nezávislost	k1gFnSc6	nezávislost
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
za	za	k7c2	za
podpory	podpora	k1gFnSc2	podpora
90,3	[number]	k4	90,3
%	%	kIx~	%
voličů	volič	k1gMnPc2	volič
<g/>
.	.	kIx.	.
</s>
<s>
Přechod	přechod	k1gInSc1	přechod
k	k	k7c3	k
tržní	tržní	k2eAgFnSc3d1	tržní
ekonomice	ekonomika	k1gFnSc3	ekonomika
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
nebyl	být	k5eNaImAgInS	být
doprovázen	doprovázet	k5eAaImNgInS	doprovázet
cílevědomou	cílevědomý	k2eAgFnSc7d1	cílevědomá
kontrolou	kontrola	k1gFnSc7	kontrola
hospodářských	hospodářský	k2eAgInPc2d1	hospodářský
subjektů	subjekt	k1gInPc2	subjekt
<g/>
,	,	kIx,	,
rozklad	rozklad	k1gInSc1	rozklad
plánovaného	plánovaný	k2eAgNnSc2d1	plánované
hospodářství	hospodářství	k1gNnSc2	hospodářství
a	a	k8xC	a
především	především	k9	především
neschopnost	neschopnost	k1gFnSc1	neschopnost
politiků	politik	k1gMnPc2	politik
provést	provést	k5eAaPmF	provést
skutečné	skutečný	k2eAgFnPc4d1	skutečná
ekonomické	ekonomický	k2eAgFnPc4d1	ekonomická
a	a	k8xC	a
politické	politický	k2eAgFnPc4d1	politická
reformy	reforma	k1gFnPc4	reforma
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
všechno	všechen	k3xTgNnSc1	všechen
vedlo	vést	k5eAaImAgNnS	vést
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
k	k	k7c3	k
propadu	propad	k1gInSc3	propad
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
agónii	agónie	k1gFnSc4	agónie
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
vzpamatovala	vzpamatovat	k5eAaPmAgFnS	vzpamatovat
až	až	k9	až
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
prezidentem	prezident	k1gMnSc7	prezident
byl	být	k5eAaImAgMnS	být
Leonid	Leonid	k1gInSc4	Leonid
Kravčuk	Kravčuk	k1gMnSc1	Kravčuk
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc1	jehož
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
vystřídal	vystřídat	k5eAaPmAgMnS	vystřídat
bývalý	bývalý	k2eAgMnSc1d1	bývalý
premiér	premiér	k1gMnSc1	premiér
Leonid	Leonida	k1gFnPc2	Leonida
Kučma	kučma	k1gFnSc1	kučma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
byla	být	k5eAaImAgFnS	být
schválena	schválit	k5eAaPmNgFnS	schválit
nová	nový	k2eAgFnSc1d1	nová
ústava	ústava	k1gFnSc1	ústava
s	s	k7c7	s
poloprezidentským	poloprezidentský	k2eAgInSc7d1	poloprezidentský
systémem	systém	k1gInSc7	systém
<g/>
.	.	kIx.	.
</s>
<s>
Kučmova	Kučmův	k2eAgFnSc1d1	Kučmova
politická	politický	k2eAgFnSc1d1	politická
orientace	orientace	k1gFnSc1	orientace
byla	být	k5eAaImAgFnS	být
převážně	převážně	k6eAd1	převážně
proruská	proruský	k2eAgFnSc1d1	proruská
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
otázce	otázka	k1gFnSc6	otázka
nebyl	být	k5eNaImAgInS	být
dlouho	dlouho	k6eAd1	dlouho
příliš	příliš	k6eAd1	příliš
zásadový	zásadový	k2eAgInSc1d1	zásadový
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
postupy	postup	k1gInPc1	postup
proti	proti	k7c3	proti
opozičnímu	opoziční	k2eAgInSc3d1	opoziční
tisku	tisk	k1gInSc3	tisk
a	a	k8xC	a
radikálnějším	radikální	k2eAgMnPc3d2	radikálnější
kritikům	kritik	k1gMnPc3	kritik
mnohdy	mnohdy	k6eAd1	mnohdy
měly	mít	k5eAaImAgInP	mít
často	často	k6eAd1	často
zřetelně	zřetelně	k6eAd1	zřetelně
autoritativní	autoritativní	k2eAgInPc4d1	autoritativní
rysy	rys	k1gInPc4	rys
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
prezidentských	prezidentský	k2eAgFnPc6d1	prezidentská
volbách	volba	k1gFnPc6	volba
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
neozbrojená	ozbrojený	k2eNgFnSc1d1	neozbrojená
Oranžová	oranžový	k2eAgFnSc1d1	oranžová
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
,	,	kIx,	,
vyvolaná	vyvolaný	k2eAgFnSc1d1	vyvolaná
nesouhlasem	nesouhlas	k1gInSc7	nesouhlas
části	část	k1gFnSc2	část
veřejnosti	veřejnost	k1gFnSc2	veřejnost
s	s	k7c7	s
vítězstvím	vítězství	k1gNnSc7	vítězství
proruského	proruský	k2eAgMnSc2d1	proruský
kandidáta	kandidát	k1gMnSc2	kandidát
<g/>
,	,	kIx,	,
premiéra	premiér	k1gMnSc2	premiér
Viktora	Viktor	k1gMnSc2	Viktor
Janukovyče	Janukovyč	k1gFnSc2	Janukovyč
<g/>
.	.	kIx.	.
</s>
<s>
Janukovyč	Janukovyč	k6eAd1	Janukovyč
byl	být	k5eAaImAgInS	být
nařčen	nařčen	k2eAgInSc1d1	nařčen
z	z	k7c2	z
volebních	volební	k2eAgFnPc2d1	volební
manipulací	manipulace	k1gFnPc2	manipulace
a	a	k8xC	a
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
nařídil	nařídit	k5eAaPmAgInS	nařídit
výsledky	výsledek	k1gInPc4	výsledek
voleb	volba	k1gFnPc2	volba
anulovat	anulovat	k5eAaBmF	anulovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
prezidentské	prezidentský	k2eAgFnSc6d1	prezidentská
volbě	volba	k1gFnSc6	volba
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
jeho	jeho	k3xOp3gMnSc1	jeho
oponent	oponent	k1gMnSc1	oponent
Viktor	Viktor	k1gMnSc1	Viktor
Juščenko	Juščenka	k1gFnSc5	Juščenka
<g/>
.	.	kIx.	.
</s>
<s>
Oranžová	oranžový	k2eAgFnSc1d1	oranžová
revoluce	revoluce	k1gFnSc1	revoluce
poukázala	poukázat	k5eAaPmAgFnS	poukázat
na	na	k7c4	na
křehké	křehký	k2eAgNnSc4d1	křehké
spojení	spojení	k1gNnSc4	spojení
"	"	kIx"	"
<g/>
proevropsky	proevropsky	k6eAd1	proevropsky
<g/>
"	"	kIx"	"
orientované	orientovaný	k2eAgFnSc2d1	orientovaná
západní	západní	k2eAgFnSc2d1	západní
a	a	k8xC	a
"	"	kIx"	"
<g/>
prorusky	prorusky	k6eAd1	prorusky
<g/>
"	"	kIx"	"
orientované	orientovaný	k2eAgFnSc3d1	orientovaná
východní	východní	k2eAgFnSc3d1	východní
části	část	k1gFnSc3	část
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
politické	politický	k2eAgFnSc6d1	politická
krizi	krize	k1gFnSc6	krize
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
prezident	prezident	k1gMnSc1	prezident
Juščenko	Juščenka	k1gFnSc5	Juščenka
rozpustil	rozpustit	k5eAaPmAgInS	rozpustit
parlament	parlament	k1gInSc1	parlament
<g/>
,	,	kIx,	,
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
předčasné	předčasný	k2eAgFnPc4d1	předčasná
volby	volba	k1gFnPc4	volba
a	a	k8xC	a
pokusil	pokusit	k5eAaPmAgMnS	pokusit
se	se	k3xPyFc4	se
převzít	převzít	k5eAaPmF	převzít
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
jednotkami	jednotka	k1gFnPc7	jednotka
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
situace	situace	k1gFnSc2	situace
opět	opět	k6eAd1	opět
uklidnila	uklidnit	k5eAaPmAgFnS	uklidnit
<g/>
.	.	kIx.	.
</s>
<s>
Předčasné	předčasný	k2eAgFnPc1d1	předčasná
parlamentní	parlamentní	k2eAgFnPc1d1	parlamentní
volby	volba	k1gFnPc1	volba
byly	být	k5eAaImAgFnP	být
uskutečněny	uskutečněn	k2eAgInPc4d1	uskutečněn
30	[number]	k4	30
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
počet	počet	k1gInSc1	počet
hlasů	hlas	k1gInPc2	hlas
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
získala	získat	k5eAaPmAgFnS	získat
Janukovyčova	Janukovyčův	k2eAgFnSc1d1	Janukovyčova
Strana	strana	k1gFnSc1	strana
regionů	region	k1gInPc2	region
(	(	kIx(	(
<g/>
34	[number]	k4	34
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
těsně	těsně	k6eAd1	těsně
za	za	k7c7	za
ní	on	k3xPp3gFnSc7	on
se	se	k3xPyFc4	se
umístil	umístit	k5eAaPmAgMnS	umístit
Blok	blok	k1gInSc4	blok
Julije	Julije	k1gFnSc2	Julije
Tymošenkové	Tymošenkový	k2eAgFnSc2d1	Tymošenková
(	(	kIx(	(
<g/>
31	[number]	k4	31
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dlouhém	dlouhý	k2eAgNnSc6d1	dlouhé
vyjednávání	vyjednávání	k1gNnSc6	vyjednávání
byla	být	k5eAaImAgFnS	být
18	[number]	k4	18
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2007	[number]	k4	2007
zvolena	zvolit	k5eAaPmNgFnS	zvolit
premiérkou	premiérka	k1gFnSc7	premiérka
Julija	Julij	k1gInSc2	Julij
Tymošenková	Tymošenkový	k2eAgFnSc1d1	Tymošenková
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gNnPc4	jejíž
blok	blok	k1gInSc1	blok
utvořil	utvořit	k5eAaPmAgInS	utvořit
koalici	koalice	k1gFnSc4	koalice
s	s	k7c7	s
Juščenkovou	Juščenkový	k2eAgFnSc7d1	Juščenkový
stranou	strana	k1gFnSc7	strana
Naše	náš	k3xOp1gNnSc1	náš
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
parlamentu	parlament	k1gInSc6	parlament
však	však	k9	však
měla	mít	k5eAaImAgFnS	mít
tato	tento	k3xDgFnSc1	tento
koalice	koalice	k1gFnSc1	koalice
jen	jen	k6eAd1	jen
těsnou	těsný	k2eAgFnSc4d1	těsná
většinu	většina	k1gFnSc4	většina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
6	[number]	k4	6
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
z	z	k7c2	z
koalice	koalice	k1gFnSc2	koalice
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
dva	dva	k4xCgMnPc1	dva
poslanci	poslanec	k1gMnPc1	poslanec
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
Julie	Julie	k1gFnSc2	Julie
Tymošenkové	Tymošenkový	k2eAgFnPc1d1	Tymošenková
pak	pak	k6eAd1	pak
definitivně	definitivně	k6eAd1	definitivně
padla	padnout	k5eAaPmAgFnS	padnout
16	[number]	k4	16
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
předseda	předseda	k1gMnSc1	předseda
ukrajinského	ukrajinský	k2eAgInSc2d1	ukrajinský
parlamentu	parlament	k1gInSc2	parlament
Arsenij	Arsenij	k1gMnSc1	Arsenij
Jaceňuk	Jaceňuk	k1gMnSc1	Jaceňuk
oznámil	oznámit	k5eAaPmAgMnS	oznámit
na	na	k7c6	na
schůzce	schůzka	k1gFnSc6	schůzka
poslanců	poslanec	k1gMnPc2	poslanec
dne	den	k1gInSc2	den
16	[number]	k4	16
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2008	[number]	k4	2008
zánik	zánik	k1gInSc1	zánik
vládní	vládní	k2eAgFnSc2d1	vládní
koalice	koalice	k1gFnSc2	koalice
<g/>
.	.	kIx.	.
</s>
<s>
Změny	změna	k1gFnPc1	změna
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
prosazeny	prosazen	k2eAgFnPc4d1	prosazena
Blokem	blok	k1gInSc7	blok
Julije	Julije	k1gFnSc2	Julije
Tymošenkové	Tymošenkový	k2eAgFnSc2d1	Tymošenková
(	(	kIx(	(
<g/>
BJuT	BJuT	k1gFnSc2	BJuT
<g/>
)	)	kIx)	)
a	a	k8xC	a
proruskou	proruský	k2eAgFnSc7d1	proruská
Stranou	strana	k1gFnSc7	strana
regionů	region	k1gInPc2	region
Viktora	Viktor	k1gMnSc4	Viktor
Janukovyče	Janukovyč	k1gFnSc2	Janukovyč
<g/>
,	,	kIx,	,
označil	označit	k5eAaPmAgMnS	označit
prezident	prezident	k1gMnSc1	prezident
Juščenko	Juščenka	k1gFnSc5	Juščenka
za	za	k7c4	za
ústavní	ústavní	k2eAgInSc4d1	ústavní
převrat	převrat	k1gInSc4	převrat
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
následujícím	následující	k2eAgInSc6d1	následující
víkendu	víkend	k1gInSc6	víkend
vypršela	vypršet	k5eAaPmAgFnS	vypršet
desetidenní	desetidenní	k2eAgFnSc1d1	desetidenní
lhůta	lhůta	k1gFnSc1	lhůta
<g/>
,	,	kIx,	,
během	během	k7c2	během
níž	jenž	k3xRgFnSc2	jenž
měli	mít	k5eAaImAgMnP	mít
prozápadní	prozápadní	k2eAgMnPc1d1	prozápadní
vládní	vládní	k2eAgMnPc1d1	vládní
partneři	partner	k1gMnPc1	partner
své	svůj	k3xOyFgFnPc4	svůj
neshody	neshoda	k1gFnPc4	neshoda
vyřešit	vyřešit	k5eAaPmF	vyřešit
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
nové	nový	k2eAgFnSc3d1	nová
politické	politický	k2eAgFnSc3d1	politická
krizi	krize	k1gFnSc3	krize
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
premiérka	premiérka	k1gFnSc1	premiérka
Tymošenková	Tymošenkový	k2eAgFnSc1d1	Tymošenková
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
za	za	k7c4	za
překonanou	překonaný	k2eAgFnSc4d1	překonaná
počátkem	počátkem	k7c2	počátkem
prosince	prosinec	k1gInSc2	prosinec
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
bloku	blok	k1gInSc2	blok
Volodymyra	Volodymyr	k1gInSc2	Volodymyr
Lytvyna	Lytvyen	k2eAgFnSc1d1	Lytvyen
obnovena	obnoven	k2eAgFnSc1d1	obnovena
koalice	koalice	k1gFnSc1	koalice
BJuT	BJuT	k1gFnSc2	BJuT
a	a	k8xC	a
Juščenkovy	Juščenkův	k2eAgFnSc2d1	Juščenkova
Naší	náš	k3xOp1gFnSc2	náš
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
část	část	k1gFnSc1	část
strany	strana	k1gFnSc2	strana
prezidenta	prezident	k1gMnSc2	prezident
Juščenka	Juščenka	k1gFnSc1	Juščenka
považovala	považovat	k5eAaImAgFnS	považovat
smlouvu	smlouva	k1gFnSc4	smlouva
za	za	k7c4	za
neplatnou	platný	k2eNgFnSc4d1	neplatná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
prezidentské	prezidentský	k2eAgFnPc4d1	prezidentská
volby	volba	k1gFnPc4	volba
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
právoplatně	právoplatně	k6eAd1	právoplatně
<g/>
,	,	kIx,	,
Viktor	Viktor	k1gMnSc1	Viktor
Janukovyč	Janukovyč	k1gMnSc1	Janukovyč
s	s	k7c7	s
48	[number]	k4	48
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Strana	strana	k1gFnSc1	strana
regionů	region	k1gInPc2	region
tak	tak	k6eAd1	tak
získala	získat	k5eAaPmAgFnS	získat
nejen	nejen	k6eAd1	nejen
křeslo	křeslo	k1gNnSc4	křeslo
premiéra	premiér	k1gMnSc2	premiér
a	a	k8xC	a
předsedy	předseda	k1gMnSc2	předseda
jednokomorového	jednokomorový	k2eAgInSc2d1	jednokomorový
parlamentu	parlament	k1gInSc2	parlament
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
post	post	k1gInSc4	post
prezidenta	prezident	k1gMnSc2	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
Janukovyč	Janukovyč	k1gMnSc1	Janukovyč
prezentoval	prezentovat	k5eAaBmAgMnS	prezentovat
jako	jako	k9	jako
silně	silně	k6eAd1	silně
proruský	proruský	k2eAgMnSc1d1	proruský
kandidát	kandidát	k1gMnSc1	kandidát
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
dokázal	dokázat	k5eAaPmAgInS	dokázat
Ukrajinu	Ukrajina	k1gFnSc4	Ukrajina
přiblížit	přiblížit	k5eAaPmF	přiblížit
i	i	k9	i
k	k	k7c3	k
Evropské	evropský	k2eAgFnSc3d1	Evropská
unii	unie	k1gFnSc3	unie
(	(	kIx(	(
<g/>
EU	EU	kA	EU
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
již	již	k6eAd1	již
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
se	se	k3xPyFc4	se
zdálo	zdát	k5eAaImAgNnS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
směřuje	směřovat	k5eAaImIp3nS	směřovat
k	k	k7c3	k
podepsání	podepsání	k1gNnSc3	podepsání
asociační	asociační	k2eAgFnSc2d1	asociační
dohody	dohoda	k1gFnSc2	dohoda
s	s	k7c7	s
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
naděje	naděje	k1gFnSc1	naděje
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
tím	ten	k3xDgNnSc7	ten
výrazně	výrazně	k6eAd1	výrazně
přiblížila	přiblížit	k5eAaPmAgFnS	přiblížit
k	k	k7c3	k
budoucímu	budoucí	k2eAgNnSc3d1	budoucí
členství	členství	k1gNnSc3	členství
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
nadstátní	nadstátní	k2eAgFnSc6d1	nadstátní
organizaci	organizace	k1gFnSc6	organizace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
tehdy	tehdy	k6eAd1	tehdy
ještě	ještě	k6eAd1	ještě
dobrými	dobrý	k2eAgInPc7d1	dobrý
vztahy	vztah	k1gInPc7	vztah
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
mohla	moct	k5eAaImAgFnS	moct
stát	stát	k1gInSc4	stát
i	i	k8xC	i
jakýmsi	jakýsi	k3yIgInSc7	jakýsi
"	"	kIx"	"
<g/>
mostem	most	k1gInSc7	most
mezi	mezi	k7c7	mezi
Východem	východ	k1gInSc7	východ
a	a	k8xC	a
Západem	západ	k1gInSc7	západ
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
ale	ale	k9	ale
velmi	velmi	k6eAd1	velmi
zkomplikovala	zkomplikovat	k5eAaPmAgFnS	zkomplikovat
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Janukovyč	Janukovyč	k1gMnSc1	Janukovyč
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
i	i	k9	i
ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
vláda	vláda	k1gFnSc1	vláda
jen	jen	k9	jen
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
před	před	k7c7	před
připravovaným	připravovaný	k2eAgInSc7d1	připravovaný
summitem	summit	k1gInSc7	summit
Východního	východní	k2eAgNnSc2d1	východní
partnerství	partnerství	k1gNnSc2	partnerství
EU	EU	kA	EU
ve	v	k7c6	v
Vilniusu	Vilnius	k1gInSc6	Vilnius
pozastavili	pozastavit	k5eAaPmAgMnP	pozastavit
21	[number]	k4	21
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2013	[number]	k4	2013
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
k	k	k7c3	k
obtížné	obtížný	k2eAgFnSc3d1	obtížná
finanční	finanční	k2eAgFnSc3d1	finanční
situaci	situace	k1gFnSc3	situace
země	zem	k1gFnSc2	zem
a	a	k8xC	a
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
nabídce	nabídka	k1gFnSc3	nabídka
pomoci	pomoc	k1gFnSc2	pomoc
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Ruska	Rusko	k1gNnSc2	Rusko
přípravy	příprava	k1gFnSc2	příprava
pro	pro	k7c4	pro
podpis	podpis	k1gInSc4	podpis
asociační	asociační	k2eAgFnSc2d1	asociační
dohody	dohoda	k1gFnSc2	dohoda
s	s	k7c7	s
Evropskou	evropský	k2eAgFnSc7d1	Evropská
unií	unie	k1gFnSc7	unie
<g/>
.	.	kIx.	.
</s>
<s>
Janukovyč	Janukovyč	k1gMnSc1	Janukovyč
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
dohoda	dohoda	k1gFnSc1	dohoda
s	s	k7c7	s
EU	EU	kA	EU
zatím	zatím	k6eAd1	zatím
pro	pro	k7c4	pro
Ukrajinu	Ukrajina	k1gFnSc4	Ukrajina
nevýhodná	výhodný	k2eNgFnSc1d1	nevýhodná
<g/>
.	.	kIx.	.
</s>
<s>
Pouhý	pouhý	k2eAgInSc4d1	pouhý
den	den	k1gInSc4	den
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
v	v	k7c4	v
pátek	pátek	k1gInSc4	pátek
22	[number]	k4	22
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
zaplavila	zaplavit	k5eAaPmAgFnS	zaplavit
Kyjev	Kyjev	k1gInSc4	Kyjev
a	a	k8xC	a
část	část	k1gFnSc1	část
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
vlna	vlna	k1gFnSc1	vlna
demonstrací	demonstrace	k1gFnPc2	demonstrace
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
Vánoci	Vánoce	k1gFnPc7	Vánoce
2013	[number]	k4	2013
se	se	k3xPyFc4	se
na	na	k7c4	na
největší	veliký	k2eAgFnSc4d3	veliký
manifestaci	manifestace	k1gFnSc4	manifestace
na	na	k7c6	na
hlavním	hlavní	k2eAgNnSc6d1	hlavní
kyjevském	kyjevský	k2eAgNnSc6d1	Kyjevské
náměstí	náměstí	k1gNnSc6	náměstí
sešlo	sejít	k5eAaPmAgNnS	sejít
na	na	k7c4	na
200	[number]	k4	200
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
některých	některý	k3yIgInPc2	některý
zdrojů	zdroj	k1gInPc2	zdroj
se	se	k3xPyFc4	se
sešlo	sejít	k5eAaPmAgNnS	sejít
v	v	k7c6	v
Kyjevě	Kyjev	k1gInSc6	Kyjev
až	až	k9	až
800	[number]	k4	800
000	[number]	k4	000
demonstrantů	demonstrant	k1gMnPc2	demonstrant
a	a	k8xC	a
série	série	k1gFnSc1	série
protestů	protest	k1gInPc2	protest
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Euromajdan	Euromajdany	k1gInPc2	Euromajdany
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
přenesla	přenést	k5eAaPmAgFnS	přenést
i	i	k9	i
do	do	k7c2	do
některých	některý	k3yIgNnPc2	některý
dalších	další	k2eAgNnPc2d1	další
ukrajinských	ukrajinský	k2eAgNnPc2d1	ukrajinské
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2013	[number]	k4	2013
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
Evropská	evropský	k2eAgFnSc1d1	Evropská
komise	komise	k1gFnSc1	komise
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
exekutivní	exekutivní	k2eAgInSc1d1	exekutivní
orgán	orgán	k1gInSc1	orgán
EU	EU	kA	EU
<g/>
,	,	kIx,	,
poskytnout	poskytnout	k5eAaPmF	poskytnout
Ukrajině	Ukrajina	k1gFnSc3	Ukrajina
několikamiliardový	několikamiliardový	k2eAgInSc4d1	několikamiliardový
balík	balík	k1gInSc4	balík
pomoci	pomoct	k5eAaPmF	pomoct
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
podpis	podpis	k1gInSc4	podpis
asociační	asociační	k2eAgFnSc2d1	asociační
dohody	dohoda	k1gFnSc2	dohoda
<g/>
.	.	kIx.	.
</s>
<s>
Situace	situace	k1gFnSc1	situace
eskalovala	eskalovat	k5eAaImAgFnS	eskalovat
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
vládních	vládní	k2eAgFnPc2d1	vládní
represí	represe	k1gFnPc2	represe
vůči	vůči	k7c3	vůči
<g/>
.	.	kIx.	.
<g/>
demonstrantům	demonstrant	k1gMnPc3	demonstrant
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
února	únor	k1gInSc2	únor
2014	[number]	k4	2014
již	již	k6eAd1	již
nepokoje	nepokoj	k1gInSc2	nepokoj
přerostly	přerůst	k5eAaPmAgFnP	přerůst
v	v	k7c4	v
ozbrojené	ozbrojený	k2eAgInPc4d1	ozbrojený
střety	střet	k1gInPc4	střet
a	a	k8xC	a
prezident	prezident	k1gMnSc1	prezident
Janukovyč	Janukovyč	k1gMnSc1	Janukovyč
uprchl	uprchnout	k5eAaPmAgMnS	uprchnout
po	po	k7c6	po
nezdařeném	zdařený	k2eNgNnSc6d1	nezdařené
vyjednávání	vyjednávání	k1gNnSc6	vyjednávání
s	s	k7c7	s
opozicí	opozice	k1gFnSc7	opozice
(	(	kIx(	(
<g/>
navzdory	navzdory	k7c3	navzdory
účasti	účast	k1gFnSc3	účast
ministrů	ministr	k1gMnPc2	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
Francie	Francie	k1gFnSc2	Francie
<g/>
)	)	kIx)	)
a	a	k8xC	a
dalších	další	k2eAgFnPc6d1	další
dramatických	dramatický	k2eAgFnPc6d1	dramatická
událostech	událost	k1gFnPc6	událost
dne	den	k1gInSc2	den
22	[number]	k4	22
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
Ruska	Rusko	k1gNnSc2	Rusko
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c6	na
události	událost	k1gFnSc6	událost
demonstrovali	demonstrovat	k5eAaBmAgMnP	demonstrovat
proruští	proruský	k2eAgMnPc1d1	proruský
aktivisté	aktivista	k1gMnPc1	aktivista
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
Janukovyče	Janukovyč	k1gFnSc2	Janukovyč
na	na	k7c6	na
východě	východ	k1gInSc6	východ
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
to	ten	k3xDgNnSc4	ten
tzv.	tzv.	kA	tzv.
Antimajdan	Antimajdan	k1gInSc1	Antimajdan
<g/>
,	,	kIx,	,
série	série	k1gFnSc1	série
provládních	provládní	k2eAgFnPc2d1	provládní
demonstrací	demonstrace	k1gFnPc2	demonstrace
<g/>
,	,	kIx,	,
probíhající	probíhající	k2eAgFnPc1d1	probíhající
paralelně	paralelně	k6eAd1	paralelně
k	k	k7c3	k
protestnímu	protestní	k2eAgNnSc3d1	protestní
hnutí	hnutí	k1gNnSc3	hnutí
Euromajdan	Euromajdana	k1gFnPc2	Euromajdana
<g/>
.	.	kIx.	.
25	[number]	k4	25
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2013	[number]	k4	2013
se	se	k3xPyFc4	se
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
v	v	k7c6	v
Kyjevě	Kyjev	k1gInSc6	Kyjev
demonstrace	demonstrace	k1gFnSc2	demonstrace
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
vlády	vláda	k1gFnSc2	vláda
premiéra	premiér	k1gMnSc2	premiér
Mykoly	Mykola	k1gFnSc2	Mykola
Azarova	Azarov	k1gInSc2	Azarov
<g/>
,	,	kIx,	,
prezidenta	prezident	k1gMnSc2	prezident
Viktora	Viktor	k1gMnSc2	Viktor
Janukovyče	Janukovyč	k1gFnSc2	Janukovyč
a	a	k8xC	a
pro	pro	k7c4	pro
blízké	blízký	k2eAgFnPc4d1	blízká
vazby	vazba	k1gFnPc4	vazba
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Této	tento	k3xDgFnSc2	tento
demonstrace	demonstrace	k1gFnSc2	demonstrace
se	se	k3xPyFc4	se
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
asi	asi	k9	asi
10	[number]	k4	10
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Doněcku	Doněck	k1gInSc6	Doněck
došlo	dojít	k5eAaPmAgNnS	dojít
koncem	koncem	k7c2	koncem
února	únor	k1gInSc2	únor
2014	[number]	k4	2014
ke	k	k7c3	k
srážkám	srážka	k1gFnPc3	srážka
mezi	mezi	k7c7	mezi
aktivisty	aktivista	k1gMnPc7	aktivista
Euromajdanu	Euromajdan	k1gInSc2	Euromajdan
a	a	k8xC	a
ozbrojenými	ozbrojený	k2eAgFnPc7d1	ozbrojená
separatistickými	separatistický	k2eAgFnPc7d1	separatistická
silami	síla	k1gFnPc7	síla
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
doposud	doposud	k6eAd1	doposud
autonomním	autonomní	k2eAgInSc6d1	autonomní
Krymu	Krym	k1gInSc6	Krym
neoznačení	označený	k2eNgMnPc1d1	neoznačený
ozbrojenci	ozbrojenec	k1gMnPc1	ozbrojenec
obsadili	obsadit	k5eAaPmAgMnP	obsadit
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
26	[number]	k4	26
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2014	[number]	k4	2014
tamní	tamní	k2eAgInSc1d1	tamní
parlament	parlament	k1gInSc1	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Posléze	posléze	k6eAd1	posléze
vyhlásili	vyhlásit	k5eAaPmAgMnP	vyhlásit
někteří	některý	k3yIgMnPc1	některý
poslanci	poslanec	k1gMnPc1	poslanec
referendum	referendum	k1gNnSc4	referendum
o	o	k7c6	o
statusu	status	k1gInSc6	status
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Ruský	ruský	k2eAgInSc1d1	ruský
parlament	parlament	k1gInSc1	parlament
de	de	k?	de
facto	facto	k1gNnSc1	facto
schválil	schválit	k5eAaPmAgInS	schválit
intervenci	intervence	k1gFnSc3	intervence
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2014	[number]	k4	2014
<g/>
..	..	k?	..
16	[number]	k4	16
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2014	[number]	k4	2014
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
referendum	referendum	k1gNnSc1	referendum
o	o	k7c6	o
samostatnosti	samostatnost	k1gFnSc6	samostatnost
Krymu	Krym	k1gInSc2	Krym
a	a	k8xC	a
následném	následný	k2eAgNnSc6d1	následné
připojení	připojení	k1gNnSc6	připojení
k	k	k7c3	k
Ruské	ruský	k2eAgFnSc3d1	ruská
federaci	federace	k1gFnSc3	federace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
referendu	referendum	k1gNnSc6	referendum
se	se	k3xPyFc4	se
dle	dle	k7c2	dle
ruských	ruský	k2eAgInPc2d1	ruský
úřadů	úřad	k1gInPc2	úřad
voliči	volič	k1gMnPc1	volič
absolutní	absolutní	k2eAgMnPc1d1	absolutní
většinou	většinou	k6eAd1	většinou
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
pro	pro	k7c4	pro
nezávislost	nezávislost	k1gFnSc4	nezávislost
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Referendum	referendum	k1gNnSc1	referendum
ovšem	ovšem	k9	ovšem
nebylo	být	k5eNaImAgNnS	být
uznáno	uznat	k5eAaPmNgNnS	uznat
převážnou	převážný	k2eAgFnSc7d1	převážná
většinou	většina	k1gFnSc7	většina
členských	členský	k2eAgFnPc2d1	členská
zemí	zem	k1gFnPc2	zem
Organizace	organizace	k1gFnSc2	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
(	(	kIx(	(
<g/>
OSN	OSN	kA	OSN
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Krymský	krymský	k2eAgInSc1d1	krymský
parlament	parlament	k1gInSc1	parlament
přesto	přesto	k8xC	přesto
17	[number]	k4	17
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2014	[number]	k4	2014
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
nezávislý	závislý	k2eNgInSc1d1	nezávislý
svrchovaný	svrchovaný	k2eAgInSc1d1	svrchovaný
stát	stát	k1gInSc1	stát
s	s	k7c7	s
názvem	název	k1gInSc7	název
Republika	republika	k1gFnSc1	republika
Krym	Krym	k1gInSc1	Krym
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
okamžitě	okamžitě	k6eAd1	okamžitě
poté	poté	k6eAd1	poté
požádal	požádat	k5eAaPmAgMnS	požádat
o	o	k7c4	o
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
Ruské	ruský	k2eAgFnSc2d1	ruská
federace	federace	k1gFnSc2	federace
jako	jako	k8xC	jako
její	její	k3xOp3gInSc1	její
nový	nový	k2eAgInSc1d1	nový
subjekt	subjekt	k1gInSc1	subjekt
<g/>
.	.	kIx.	.
</s>
<s>
Proruské	proruský	k2eAgInPc1d1	proruský
nepokoje	nepokoj	k1gInPc1	nepokoj
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vypukly	vypuknout	k5eAaPmAgFnP	vypuknout
po	po	k7c6	po
svržení	svržení	k1gNnSc6	svržení
prezidenta	prezident	k1gMnSc2	prezident
Janukovyče	Janukovyč	k1gFnSc2	Janukovyč
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vystupňovaly	vystupňovat	k5eAaPmAgInP	vystupňovat
v	v	k7c4	v
ozbrojený	ozbrojený	k2eAgInSc4d1	ozbrojený
konflikt	konflikt	k1gInSc4	konflikt
mezi	mezi	k7c7	mezi
ukrajinskou	ukrajinský	k2eAgFnSc7d1	ukrajinská
armádou	armáda	k1gFnSc7	armáda
a	a	k8xC	a
separatistickými	separatistický	k2eAgFnPc7d1	separatistická
silami	síla	k1gFnPc7	síla
v	v	k7c6	v
Doněcké	doněcký	k2eAgFnSc6d1	Doněcká
a	a	k8xC	a
Luhanské	Luhanský	k2eAgFnSc6d1	Luhanská
lidové	lidový	k2eAgFnSc6d1	lidová
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
bojovým	bojový	k2eAgFnPc3d1	bojová
operacím	operace	k1gFnPc3	operace
uprchlo	uprchnout	k5eAaPmAgNnS	uprchnout
k	k	k7c3	k
začátku	začátek	k1gInSc3	začátek
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
z	z	k7c2	z
konfliktní	konfliktní	k2eAgFnSc2d1	konfliktní
oblasti	oblast	k1gFnSc2	oblast
do	do	k7c2	do
jiných	jiný	k2eAgFnPc2d1	jiná
oblastí	oblast	k1gFnPc2	oblast
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
1	[number]	k4	1
007	[number]	k4	007
900	[number]	k4	900
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
268	[number]	k4	268
400	[number]	k4	400
jich	on	k3xPp3gMnPc2	on
vyhledalo	vyhledat	k5eAaPmAgNnS	vyhledat
azyl	azyl	k1gInSc4	azyl
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
údajů	údaj	k1gInPc2	údaj
Úřadu	úřad	k1gInSc2	úřad
vysokého	vysoký	k2eAgMnSc2d1	vysoký
komisaře	komisař	k1gMnSc2	komisař
OSN	OSN	kA	OSN
pro	pro	k7c4	pro
uprchlíky	uprchlík	k1gMnPc4	uprchlík
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
září	září	k1gNnSc2	září
2015	[number]	k4	2015
uprchlo	uprchnout	k5eAaPmAgNnS	uprchnout
do	do	k7c2	do
Ruska	Rusko	k1gNnSc2	Rusko
před	před	k7c7	před
válkou	válka	k1gFnSc7	válka
zhruba	zhruba	k6eAd1	zhruba
1,3	[number]	k4	1,3
milionu	milion	k4xCgInSc2	milion
Ukrajinců	Ukrajinec	k1gMnPc2	Ukrajinec
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
počtu	počet	k1gInSc2	počet
požádalo	požádat	k5eAaPmAgNnS	požádat
400	[number]	k4	400
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
o	o	k7c4	o
status	status	k1gInSc4	status
uprchlíka	uprchlík	k1gMnSc2	uprchlík
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
bylo	být	k5eAaImAgNnS	být
300	[number]	k4	300
000	[number]	k4	000
žadatelů	žadatel	k1gMnPc2	žadatel
o	o	k7c4	o
dočasný	dočasný	k2eAgInSc4d1	dočasný
pobyt	pobyt	k1gInSc4	pobyt
a	a	k8xC	a
600	[number]	k4	600
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
není	být	k5eNaImIp3nS	být
řádně	řádně	k6eAd1	řádně
registrováno	registrovat	k5eAaBmNgNnS	registrovat
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
listopadu	listopad	k1gInSc2	listopad
2015	[number]	k4	2015
kolem	kolem	k7c2	kolem
2,6	[number]	k4	2,6
milionu	milion	k4xCgInSc2	milion
Ukrajinců	Ukrajinec	k1gMnPc2	Ukrajinec
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2015	[number]	k4	2015
se	se	k3xPyFc4	se
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
uskutečnily	uskutečnit	k5eAaPmAgFnP	uskutečnit
prezidentské	prezidentský	k2eAgFnPc4d1	prezidentská
volby	volba	k1gFnPc4	volba
hlasováním	hlasování	k1gNnSc7	hlasování
všech	všecek	k3xTgMnPc2	všecek
občanů	občan	k1gMnPc2	občan
<g/>
.	.	kIx.	.
</s>
<s>
Vítězem	vítěz	k1gMnSc7	vítěz
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Petro	Petra	k1gFnSc5	Petra
Porošenko	Porošenka	k1gFnSc5	Porošenka
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Geografie	geografie	k1gFnSc2	geografie
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
rozlohou	rozloha	k1gFnSc7	rozloha
603	[number]	k4	603
700	[number]	k4	700
km2	km2	k4	km2
druhým	druhý	k4xOgInSc7	druhý
největším	veliký	k2eAgInSc7d3	veliký
státem	stát	k1gInSc7	stát
Evropy	Evropa	k1gFnSc2	Evropa
(	(	kIx(	(
<g/>
po	po	k7c6	po
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zpravidla	zpravidla	k6eAd1	zpravidla
bývá	bývat	k5eAaImIp3nS	bývat
přiřazována	přiřazován	k2eAgNnPc4d1	přiřazován
k	k	k7c3	k
východoevropským	východoevropský	k2eAgInPc3d1	východoevropský
státům	stát	k1gInPc3	stát
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
její	její	k3xOp3gInPc1	její
západní	západní	k2eAgInPc1d1	západní
regiony	region	k1gInPc1	region
jako	jako	k8xC	jako
Halič	Halič	k1gFnSc1	Halič
nebo	nebo	k8xC	nebo
Zakarpatí	Zakarpatí	k1gNnSc1	Zakarpatí
náleží	náležet	k5eAaImIp3nS	náležet
kulturně	kulturně	k6eAd1	kulturně
a	a	k8xC	a
historicky	historicky	k6eAd1	historicky
spíše	spíše	k9	spíše
ke	k	k7c3	k
střední	střední	k2eAgFnSc3d1	střední
Evropě	Evropa	k1gFnSc3	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Střední	střední	k2eAgFnSc1d1	střední
a	a	k8xC	a
východní	východní	k2eAgFnSc1d1	východní
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
jihozápadní	jihozápadní	k2eAgFnSc1d1	jihozápadní
část	část	k1gFnSc1	část
rozlehlé	rozlehlý	k2eAgFnSc2d1	rozlehlá
Východoevropské	východoevropský	k2eAgFnSc2d1	východoevropská
roviny	rovina	k1gFnSc2	rovina
<g/>
.	.	kIx.	.
</s>
<s>
Povrch	povrch	k1gInSc1	povrch
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
především	především	k6eAd1	především
vysočinami	vysočina	k1gFnPc7	vysočina
a	a	k8xC	a
pahorkatinami	pahorkatina	k1gFnPc7	pahorkatina
či	či	k8xC	či
náhorními	náhorní	k2eAgFnPc7d1	náhorní
plošinami	plošina	k1gFnPc7	plošina
přerušovanými	přerušovaný	k2eAgFnPc7d1	přerušovaná
údolími	údolí	k1gNnPc7	údolí
řek	řeka	k1gFnPc2	řeka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
jsou	být	k5eAaImIp3nP	být
typická	typický	k2eAgFnSc1d1	typická
především	především	k6eAd1	především
pro	pro	k7c4	pro
přítoky	přítok	k1gInPc4	přítok
Dněstru	Dněstr	k1gInSc2	Dněstr
v	v	k7c6	v
Podolí	Podolí	k1gNnSc6	Podolí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
rozkládají	rozkládat	k5eAaImIp3nP	rozkládat
rovinaté	rovinatý	k2eAgFnPc4d1	rovinatá
stepi	step	k1gFnPc4	step
<g/>
;	;	kIx,	;
druhá	druhý	k4xOgFnSc1	druhý
větší	veliký	k2eAgFnSc1d2	veliký
rovinatá	rovinatý	k2eAgFnSc1d1	rovinatá
oblast	oblast	k1gFnSc1	oblast
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
Polesí	Polesí	k1gNnSc1	Polesí
při	při	k7c6	při
hranicích	hranice	k1gFnPc6	hranice
s	s	k7c7	s
Běloruskem	Bělorusko	k1gNnSc7	Bělorusko
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgNnPc7d3	veliký
pohořími	pohoří	k1gNnPc7	pohoří
jsou	být	k5eAaImIp3nP	být
Karpaty	Karpaty	k1gInPc4	Karpaty
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
země	země	k1gFnSc1	země
(	(	kIx(	(
<g/>
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
vrchem	vrch	k1gInSc7	vrch
je	být	k5eAaImIp3nS	být
Hoverla	Hoverla	k1gFnSc1	Hoverla
<g/>
,	,	kIx,	,
2061	[number]	k4	2061
m	m	kA	m
<g/>
)	)	kIx)	)
a	a	k8xC	a
Krymské	krymský	k2eAgFnPc1d1	Krymská
hory	hora	k1gFnPc1	hora
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Krymského	krymský	k2eAgInSc2d1	krymský
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
nejvyšší	vysoký	k2eAgInPc1d3	Nejvyšší
vrcholky	vrcholek	k1gInPc1	vrcholek
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
okolo	okolo	k7c2	okolo
1500	[number]	k4	1500
m.	m.	k?	m.
Poloostrov	poloostrov	k1gInSc1	poloostrov
Krym	Krym	k1gInSc1	Krym
je	být	k5eAaImIp3nS	být
se	s	k7c7	s
zbytkem	zbytek	k1gInSc7	zbytek
pevniny	pevnina	k1gFnSc2	pevnina
spojen	spojit	k5eAaPmNgInS	spojit
Perekopskou	Perekopský	k2eAgFnSc7d1	Perekopská
šíjí	šíj	k1gFnSc7	šíj
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
říční	říční	k2eAgFnSc2d1	říční
sítě	síť	k1gFnSc2	síť
je	být	k5eAaImIp3nS	být
okolo	okolo	k7c2	okolo
170	[number]	k4	170
000	[number]	k4	000
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
řek	řeka	k1gFnPc2	řeka
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
úmoří	úmoří	k1gNnSc2	úmoří
Azovského	azovský	k2eAgNnSc2d1	Azovské
a	a	k8xC	a
Černého	Černého	k2eAgNnSc2d1	Černého
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
jen	jen	k9	jen
4	[number]	k4	4
%	%	kIx~	%
území	území	k1gNnSc6	území
náleží	náležet	k5eAaImIp3nS	náležet
k	k	k7c3	k
úmoří	úmoří	k1gNnSc3	úmoří
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
řekou	řeka	k1gFnSc7	řeka
je	být	k5eAaImIp3nS	být
Dněpr	Dněpr	k1gInSc1	Dněpr
(	(	kIx(	(
<g/>
ukrajinsky	ukrajinsky	k6eAd1	ukrajinsky
Dnipro	Dnipro	k1gNnSc1	Dnipro
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
protíná	protínat	k5eAaImIp3nS	protínat
Ukrajinu	Ukrajina	k1gFnSc4	Ukrajina
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
na	na	k7c4	na
jih	jih	k1gInSc4	jih
a	a	k8xC	a
dělí	dělit	k5eAaImIp3nS	dělit
ji	on	k3xPp3gFnSc4	on
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
levobřežní	levobřežní	k2eAgNnPc4d1	levobřežní
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
pravobřežní	pravobřežní	k2eAgFnSc1d1	pravobřežní
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc7	jeho
hlavními	hlavní	k2eAgInPc7d1	hlavní
přítoky	přítok	k1gInPc7	přítok
jsou	být	k5eAaImIp3nP	být
Pripjať	Pripjať	k1gFnSc2	Pripjať
<g/>
,	,	kIx,	,
Desna	Desno	k1gNnSc2	Desno
<g/>
,	,	kIx,	,
Pslo	Pslo	k1gMnSc1	Pslo
a	a	k8xC	a
Inhulec	Inhulec	k1gMnSc1	Inhulec
<g/>
.	.	kIx.	.
</s>
<s>
Východní	východní	k2eAgFnSc7d1	východní
Ukrajinou	Ukrajina	k1gFnSc7	Ukrajina
protéká	protékat	k5eAaImIp3nS	protékat
Severní	severní	k2eAgFnSc1d1	severní
Doněc	Doněc	k1gFnSc1	Doněc
<g/>
,	,	kIx,	,
přítok	přítok	k1gInSc1	přítok
Donu	Don	k1gInSc2	Don
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
velké	velký	k2eAgFnPc1d1	velká
řeky	řeka	k1gFnPc1	řeka
jsou	být	k5eAaImIp3nP	být
Prut	prut	k1gInSc4	prut
<g/>
,	,	kIx,	,
Dněstr	Dněstr	k1gInSc4	Dněstr
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc4d1	jižní
Buh	Buh	k1gFnSc4	Buh
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
řeky	řeka	k1gFnPc1	řeka
jsou	být	k5eAaImIp3nP	být
využívány	využíván	k2eAgFnPc1d1	využívána
jednak	jednak	k8xC	jednak
pro	pro	k7c4	pro
lodní	lodní	k2eAgFnSc4d1	lodní
dopravu	doprava	k1gFnSc4	doprava
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
jako	jako	k9	jako
zdroj	zdroj	k1gInSc1	zdroj
energie	energie	k1gFnSc2	energie
<g/>
:	:	kIx,	:
zejména	zejména	k9	zejména
na	na	k7c6	na
Dněpru	Dněpr	k1gInSc6	Dněpr
bylo	být	k5eAaImAgNnS	být
vybudováno	vybudovat	k5eAaPmNgNnS	vybudovat
několik	několik	k4yIc1	několik
velkých	velký	k2eAgFnPc2d1	velká
přehrad	přehrada	k1gFnPc2	přehrada
s	s	k7c7	s
hydroelektrárnami	hydroelektrárna	k1gFnPc7	hydroelektrárna
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
nejznámější	známý	k2eAgFnSc1d3	nejznámější
je	být	k5eAaImIp3nS	být
DněproGES	DněproGES	k1gFnSc1	DněproGES
v	v	k7c6	v
Záporoží	Záporoží	k1gNnSc6	Záporoží
<g/>
;	;	kIx,	;
největší	veliký	k2eAgFnSc7d3	veliký
přehradní	přehradní	k2eAgFnSc7d1	přehradní
nádrží	nádrž	k1gFnSc7	nádrž
je	být	k5eAaImIp3nS	být
Kremenčucká	Kremenčucký	k2eAgFnSc1d1	Kremenčucký
vodní	vodní	k2eAgFnSc1d1	vodní
nádrž	nádrž	k1gFnSc1	nádrž
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgNnPc1d3	veliký
ukrajinská	ukrajinský	k2eAgNnPc1d1	ukrajinské
jezera	jezero	k1gNnPc1	jezero
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Budžaku	Budžak	k1gInSc6	Budžak
v	v	k7c6	v
údolích	údolí	k1gNnPc6	údolí
Dunaje	Dunaj	k1gInSc2	Dunaj
(	(	kIx(	(
<g/>
Jalpuh	Jalpuh	k1gInSc1	Jalpuh
<g/>
,	,	kIx,	,
Kahul	Kahul	k1gInSc1	Kahul
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgNnPc1d2	veliký
jezera	jezero	k1gNnPc1	jezero
a	a	k8xC	a
limany	liman	k1gInPc1	liman
se	se	k3xPyFc4	se
rozkládají	rozkládat	k5eAaImIp3nP	rozkládat
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Černého	černé	k1gNnSc2	černé
a	a	k8xC	a
Azovského	azovský	k2eAgNnSc2d1	Azovské
moře	moře	k1gNnSc2	moře
<g/>
;	;	kIx,	;
největší	veliký	k2eAgFnPc1d3	veliký
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
je	být	k5eAaImIp3nS	být
Dněsterský	dněsterský	k2eAgInSc1d1	dněsterský
liman	liman	k1gInSc1	liman
(	(	kIx(	(
<g/>
360	[number]	k4	360
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc1d3	veliký
přirozená	přirozený	k2eAgFnSc1d1	přirozená
vodní	vodní	k2eAgFnSc1d1	vodní
plocha	plocha	k1gFnSc1	plocha
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Volyni	Volyně	k1gFnSc6	Volyně
a	a	k8xC	a
Polesí	Polesí	k1gNnSc6	Polesí
je	být	k5eAaImIp3nS	být
mnoho	mnoho	k4c1	mnoho
krasových	krasový	k2eAgNnPc2d1	krasové
jezer	jezero	k1gNnPc2	jezero
<g/>
;	;	kIx,	;
v	v	k7c6	v
Ukrajinských	ukrajinský	k2eAgInPc6d1	ukrajinský
Karpatech	Karpaty	k1gInPc6	Karpaty
je	být	k5eAaImIp3nS	být
nejvýznamnější	významný	k2eAgNnSc1d3	nejvýznamnější
jezero	jezero	k1gNnSc1	jezero
Synevyr	Synevyra	k1gFnPc2	Synevyra
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
celá	celý	k2eAgFnSc1d1	celá
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
náleží	náležet	k5eAaImIp3nS	náležet
do	do	k7c2	do
mírného	mírný	k2eAgInSc2d1	mírný
klimatického	klimatický	k2eAgInSc2d1	klimatický
pásu	pás	k1gInSc2	pás
<g/>
,	,	kIx,	,
převládá	převládat	k5eAaImIp3nS	převládat
kontinentální	kontinentální	k2eAgNnSc1d1	kontinentální
podnebí	podnebí	k1gNnSc1	podnebí
<g/>
;	;	kIx,	;
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
úzkém	úzký	k2eAgNnSc6d1	úzké
území	území	k1gNnSc6	území
mezi	mezi	k7c7	mezi
Krymskými	krymský	k2eAgFnPc7d1	Krymská
horami	hora	k1gFnPc7	hora
a	a	k8xC	a
Černým	černý	k2eAgNnSc7d1	černé
mořem	moře	k1gNnSc7	moře
je	být	k5eAaImIp3nS	být
podnebí	podnebí	k1gNnSc1	podnebí
subtropické	subtropický	k2eAgNnSc1d1	subtropické
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
roční	roční	k2eAgFnSc1d1	roční
teplota	teplota	k1gFnSc1	teplota
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c7	mezi
+6	+6	k4	+6
°	°	k?	°
<g/>
C	C	kA	C
na	na	k7c6	na
severu	sever	k1gInSc6	sever
a	a	k8xC	a
+12	+12	k4	+12
°	°	k?	°
<g/>
C	C	kA	C
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Průměrné	průměrný	k2eAgFnPc1d1	průměrná
roční	roční	k2eAgFnPc1d1	roční
srážky	srážka	k1gFnPc1	srážka
na	na	k7c6	na
většině	většina	k1gFnSc6	většina
území	území	k1gNnSc2	území
činí	činit	k5eAaImIp3nS	činit
okolo	okolo	k7c2	okolo
600	[number]	k4	600
mm	mm	kA	mm
<g/>
;	;	kIx,	;
největší	veliký	k2eAgInSc1d3	veliký
srážkový	srážkový	k2eAgInSc1d1	srážkový
úhrn	úhrn	k1gInSc1	úhrn
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
Karpaty	Karpaty	k1gInPc4	Karpaty
(	(	kIx(	(
<g/>
1200	[number]	k4	1200
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejsuššími	suchý	k2eAgFnPc7d3	nejsušší
oblastmi	oblast	k1gFnPc7	oblast
jsou	být	k5eAaImIp3nP	být
stepi	step	k1gFnPc4	step
Chersonské	chersonský	k2eAgFnSc2d1	Chersonská
oblasti	oblast	k1gFnSc2	oblast
a	a	k8xC	a
severního	severní	k2eAgInSc2d1	severní
Krymu	Krym	k1gInSc2	Krym
(	(	kIx(	(
<g/>
400	[number]	k4	400
mm	mm	kA	mm
ročně	ročně	k6eAd1	ročně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
měst	město	k1gNnPc2	město
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Přehled	přehled	k1gInSc1	přehled
25	[number]	k4	25
největších	veliký	k2eAgNnPc2d3	veliký
měst	město	k1gNnPc2	město
podle	podle	k7c2	podle
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
tučně	tučně	k6eAd1	tučně
jsou	být	k5eAaImIp3nP	být
vyznačena	vyznačit	k5eAaPmNgNnP	vyznačit
sídla	sídlo	k1gNnPc1	sídlo
oblastí	oblast	k1gFnPc2	oblast
a	a	k8xC	a
město	město	k1gNnSc4	město
se	s	k7c7	s
zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
statusem	status	k1gInSc7	status
Sevastopol	Sevastopol	k1gInSc1	Sevastopol
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Administrativní	administrativní	k2eAgNnSc1d1	administrativní
dělení	dělení	k1gNnSc1	dělení
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
27	[number]	k4	27
správních	správní	k2eAgInPc2d1	správní
celků	celek	k1gInPc2	celek
<g/>
:	:	kIx,	:
1	[number]	k4	1
autonomní	autonomní	k2eAgFnSc1d1	autonomní
republika	republika	k1gFnSc1	republika
<g/>
:	:	kIx,	:
Autonomní	autonomní	k2eAgFnSc1d1	autonomní
republika	republika	k1gFnSc1	republika
Krym	Krym	k1gInSc1	Krym
(	(	kIx(	(
<g/>
Simferopol	Simferopol	k1gInSc1	Simferopol
<g/>
)	)	kIx)	)
24	[number]	k4	24
oblastí	oblast	k1gFnPc2	oblast
<g/>
:	:	kIx,	:
2	[number]	k4	2
města	město	k1gNnSc2	město
se	s	k7c7	s
zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
statusem	status	k1gInSc7	status
<g/>
:	:	kIx,	:
Kyjev	Kyjev	k1gInSc1	Kyjev
–	–	k?	–
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
Sevastopol	Sevastopol	k1gInSc1	Sevastopol
–	–	k?	–
nespadá	spadat	k5eNaPmIp3nS	spadat
pod	pod	k7c4	pod
Krymskou	krymský	k2eAgFnSc4d1	Krymská
republiku	republika	k1gFnSc4	republika
Tyto	tento	k3xDgInPc1	tento
celky	celek	k1gInPc1	celek
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
rajóny	rajón	k1gInPc4	rajón
<g/>
,	,	kIx,	,
kterých	který	k3yRgInPc2	který
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
celkem	celkem	k6eAd1	celkem
490	[number]	k4	490
a	a	k8xC	a
které	který	k3yIgFnPc4	který
přibližně	přibližně	k6eAd1	přibližně
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
českým	český	k2eAgInPc3d1	český
okresům	okres	k1gInPc3	okres
<g/>
,	,	kIx,	,
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
o	o	k7c4	o
něco	něco	k3yInSc4	něco
menší	malý	k2eAgNnSc1d2	menší
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgNnPc1d2	veliký
města	město	k1gNnPc1	město
bývají	bývat	k5eAaImIp3nP	bývat
samostatnou	samostatný	k2eAgFnSc7d1	samostatná
správní	správní	k2eAgFnSc7d1	správní
jednotkou	jednotka	k1gFnSc7	jednotka
podřízenou	podřízená	k1gFnSc7	podřízená
přímo	přímo	k6eAd1	přímo
oblasti	oblast	k1gFnSc2	oblast
<g/>
,	,	kIx,	,
některá	některý	k3yIgNnPc1	některý
jsou	být	k5eAaImIp3nP	být
dále	daleko	k6eAd2	daleko
rozdělena	rozdělit	k5eAaPmNgNnP	rozdělit
na	na	k7c4	na
městské	městský	k2eAgInPc4d1	městský
rajóny	rajón	k1gInPc4	rajón
<g/>
.	.	kIx.	.
</s>
<s>
Obce	obec	k1gFnPc1	obec
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
spadají	spadat	k5eAaImIp3nP	spadat
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
kategorií	kategorie	k1gFnPc2	kategorie
<g/>
:	:	kIx,	:
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
sídla	sídlo	k1gNnSc2	sídlo
městského	městský	k2eAgInSc2d1	městský
typu	typ	k1gInSc2	typ
a	a	k8xC	a
vesnice	vesnice	k1gFnSc2	vesnice
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Politický	politický	k2eAgInSc1d1	politický
systém	systém	k1gInSc1	systém
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
je	být	k5eAaImIp3nS	být
zastupitelská	zastupitelský	k2eAgFnSc1d1	zastupitelská
demokracie	demokracie	k1gFnSc1	demokracie
a	a	k8xC	a
poloprezidentská	poloprezidentský	k2eAgFnSc1d1	poloprezidentská
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
je	být	k5eAaImIp3nS	být
volen	volit	k5eAaImNgMnS	volit
přímo	přímo	k6eAd1	přímo
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c4	na
5	[number]	k4	5
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
má	mít	k5eAaImIp3nS	mít
jednokomorový	jednokomorový	k2eAgInSc4d1	jednokomorový
parlament	parlament	k1gInSc4	parlament
(	(	kIx(	(
<g/>
Verchovna	Verchovna	k1gFnSc1	Verchovna
rada	rada	k1gFnSc1	rada
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
)	)	kIx)	)
o	o	k7c4	o
450	[number]	k4	450
zastupitelích	zastupitel	k1gMnPc6	zastupitel
(	(	kIx(	(
<g/>
od	od	k7c2	od
r.	r.	kA	r.
2006	[number]	k4	2006
též	též	k9	též
na	na	k7c4	na
pětileté	pětiletý	k2eAgNnSc4d1	pětileté
období	období	k1gNnSc4	období
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
ten	ten	k3xDgMnSc1	ten
volí	volit	k5eAaImIp3nS	volit
a	a	k8xC	a
odvolává	odvolávat	k5eAaImIp3nS	odvolávat
premiéra	premiéra	k1gFnSc1	premiéra
země	zem	k1gFnSc2	zem
(	(	kIx(	(
<g/>
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
jím	jíst	k5eAaImIp1nS	jíst
je	on	k3xPp3gFnPc4	on
Arsenij	Arsenij	k1gFnPc4	Arsenij
Jaceňuk	Jaceňuka	k1gFnPc2	Jaceňuka
ze	z	k7c2	z
Lidové	lidový	k2eAgFnSc2d1	lidová
fronty	fronta	k1gFnSc2	fronta
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
premiér	premiér	k1gMnSc1	premiér
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
oficiálně	oficiálně	k6eAd1	oficiálně
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
prezidentem	prezident	k1gMnSc7	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
byla	být	k5eAaImAgFnS	být
mj.	mj.	kA	mj.
pozorovatelem	pozorovatel	k1gMnSc7	pozorovatel
Společenství	společenství	k1gNnSc2	společenství
nezávislých	závislý	k2eNgInPc2d1	nezávislý
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
po	po	k7c6	po
ruské	ruský	k2eAgFnSc6d1	ruská
anexi	anexe	k1gFnSc6	anexe
Krymu	Krym	k1gInSc2	Krym
se	se	k3xPyFc4	se
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
pro	pro	k7c4	pro
úplný	úplný	k2eAgInSc4d1	úplný
odchod	odchod	k1gInSc4	odchod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
OSN	OSN	kA	OSN
<g/>
,	,	kIx,	,
OBSE	OBSE	kA	OBSE
<g/>
,	,	kIx,	,
GUAM	GUAM	kA	GUAM
<g/>
,	,	kIx,	,
Rady	rada	k1gFnSc2	rada
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
Organizace	organizace	k1gFnSc2	organizace
pro	pro	k7c4	pro
hospodářskou	hospodářský	k2eAgFnSc4d1	hospodářská
spolupráci	spolupráce	k1gFnSc4	spolupráce
černomořských	černomořský	k2eAgFnPc2d1	černomořská
zemí	zem	k1gFnPc2	zem
(	(	kIx(	(
<g/>
BSEC	BSEC	kA	BSEC
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2008	[number]	k4	2008
se	se	k3xPyFc4	se
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
po	po	k7c6	po
čtrnáctiletém	čtrnáctiletý	k2eAgNnSc6d1	čtrnáctileté
vyjednávání	vyjednávání	k1gNnSc6	vyjednávání
oficiálně	oficiálně	k6eAd1	oficiálně
stala	stát	k5eAaPmAgFnS	stát
členem	člen	k1gMnSc7	člen
WTO	WTO	kA	WTO
<g/>
.	.	kIx.	.
</s>
<s>
Státním	státní	k2eAgInSc7d1	státní
znakem	znak	k1gInSc7	znak
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
nabytí	nabytí	k1gNnSc6	nabytí
nezávislosti	nezávislost	k1gFnSc2	nezávislost
opět	opět	k6eAd1	opět
tzv.	tzv.	kA	tzv.
tryzub	tryzuba	k1gFnPc2	tryzuba
sv.	sv.	kA	sv.
Vladimíra	Vladimír	k1gMnSc2	Vladimír
(	(	kIx(	(
<g/>
zlatý	zlatý	k2eAgInSc1d1	zlatý
trojzubec	trojzubec	k1gInSc1	trojzubec
na	na	k7c6	na
modrém	modrý	k2eAgNnSc6d1	modré
poli	pole	k1gNnSc6	pole
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
malý	malý	k2eAgInSc1d1	malý
státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
velký	velký	k2eAgInSc1d1	velký
znak	znak	k1gInSc1	znak
zatím	zatím	k6eAd1	zatím
nebyl	být	k5eNaImAgInS	být
oficiálně	oficiálně	k6eAd1	oficiálně
přijat	přijmout	k5eAaPmNgInS	přijmout
<g/>
;	;	kIx,	;
i	i	k9	i
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
dominovat	dominovat	k5eAaImF	dominovat
tryzub	tryzub	k1gInSc4	tryzub
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejných	stejný	k2eAgFnPc6d1	stejná
barvách	barva	k1gFnPc6	barva
je	být	k5eAaImIp3nS	být
i	i	k9	i
ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
,	,	kIx,	,
sestávající	sestávající	k2eAgFnSc1d1	sestávající
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
stejně	stejně	k6eAd1	stejně
širokých	široký	k2eAgInPc2d1	široký
podélných	podélný	k2eAgInPc2d1	podélný
pruhů	pruh	k1gInPc2	pruh
<g/>
:	:	kIx,	:
zlatá	zlatý	k2eAgFnSc1d1	zlatá
barva	barva	k1gFnSc1	barva
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
úrodná	úrodný	k2eAgNnPc4d1	úrodné
pole	pole	k1gNnPc4	pole
<g/>
,	,	kIx,	,
modrá	modrat	k5eAaImIp3nS	modrat
pak	pak	k6eAd1	pak
nebe	nebe	k1gNnSc4	nebe
nad	nad	k7c7	nad
nimi	on	k3xPp3gMnPc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgFnSc7d1	státní
hymnou	hymna	k1gFnSc7	hymna
je	být	k5eAaImIp3nS	být
píseň	píseň	k1gFnSc1	píseň
Šče	Šče	k1gFnSc2	Šče
nevmerla	nevmerlo	k1gNnSc2	nevmerlo
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
,	,	kIx,	,
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
roku	rok	k1gInSc2	rok
1863	[number]	k4	1863
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
uvedeného	uvedený	k2eAgNnSc2d1	uvedené
má	mít	k5eAaImIp3nS	mít
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
ještě	ještě	k6eAd1	ještě
prezidentské	prezidentský	k2eAgInPc4d1	prezidentský
symboly	symbol	k1gInPc4	symbol
<g/>
:	:	kIx,	:
pečeť	pečeť	k1gFnSc4	pečeť
<g/>
,	,	kIx,	,
znak	znak	k1gInSc4	znak
a	a	k8xC	a
standartu	standarta	k1gFnSc4	standarta
<g/>
.	.	kIx.	.
</s>
<s>
Ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
armáda	armáda	k1gFnSc1	armáda
má	mít	k5eAaImIp3nS	mít
přibližně	přibližně	k6eAd1	přibližně
300	[number]	k4	300
000	[number]	k4	000
příslušníků	příslušník	k1gMnPc2	příslušník
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
členem	člen	k1gInSc7	člen
svazku	svazek	k1gInSc2	svazek
Partnerství	partnerství	k1gNnSc2	partnerství
pro	pro	k7c4	pro
mír	mír	k1gInSc4	mír
a	a	k8xC	a
účastnila	účastnit	k5eAaImAgFnS	účastnit
se	se	k3xPyFc4	se
misí	mise	k1gFnSc7	mise
v	v	k7c6	v
bývalé	bývalý	k2eAgFnSc6d1	bývalá
Jugoslávii	Jugoslávie	k1gFnSc6	Jugoslávie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
má	mít	k5eAaImIp3nS	mít
vojáky	voják	k1gMnPc4	voják
v	v	k7c4	v
Kosovu	Kosův	k2eAgFnSc4d1	Kosova
<g/>
,	,	kIx,	,
<g/>
)	)	kIx)	)
na	na	k7c6	na
Středním	střední	k2eAgInSc6d1	střední
východě	východ	k1gInSc6	východ
a	a	k8xC	a
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
navazuje	navazovat	k5eAaImIp3nS	navazovat
užší	úzký	k2eAgInPc4d2	užší
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
NATO	NATO	kA	NATO
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
s	s	k7c7	s
polskou	polský	k2eAgFnSc7d1	polská
armádou	armáda	k1gFnSc7	armáda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
plní	plnit	k5eAaImIp3nS	plnit
úkoly	úkol	k1gInPc4	úkol
v	v	k7c6	v
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
proruským	proruský	k2eAgMnPc3d1	proruský
separatistům	separatista	k1gMnPc3	separatista
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Válka	válka	k1gFnSc1	válka
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
boku	bok	k1gInSc6	bok
pravidelné	pravidelný	k2eAgFnSc2d1	pravidelná
armády	armáda	k1gFnSc2	armáda
bojují	bojovat	k5eAaImIp3nP	bojovat
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
dobrovolnické	dobrovolnický	k2eAgInPc4d1	dobrovolnický
prapory	prapor	k1gInPc4	prapor
ukrajinské	ukrajinský	k2eAgFnSc2d1	ukrajinská
Národní	národní	k2eAgFnSc2d1	národní
gardy	garda	k1gFnSc2	garda
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
batalion	batalion	k1gInSc1	batalion
Azov	Azov	k1gInSc1	Azov
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
spadají	spadat	k5eAaImIp3nP	spadat
pod	pod	k7c4	pod
ukrajinské	ukrajinský	k2eAgNnSc4d1	ukrajinské
ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2015	[number]	k4	2015
prováděla	provádět	k5eAaImAgFnS	provádět
po	po	k7c4	po
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
výcvik	výcvik	k1gInSc1	výcvik
ukrajinské	ukrajinský	k2eAgFnSc2d1	ukrajinská
armády	armáda	k1gFnSc2	armáda
173	[number]	k4	173
<g/>
.	.	kIx.	.
výsadková	výsadkový	k2eAgFnSc1d1	výsadková
brigáda	brigáda	k1gFnSc1	brigáda
americké	americký	k2eAgFnSc2d1	americká
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
má	mít	k5eAaImIp3nS	mít
intenzivní	intenzivní	k2eAgFnSc1d1	intenzivní
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
komplikované	komplikovaný	k2eAgInPc4d1	komplikovaný
vztahy	vztah	k1gInPc4	vztah
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
největším	veliký	k2eAgMnSc7d3	veliký
sousedem	soused	k1gMnSc7	soused
Ruskem	Rusko	k1gNnSc7	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Panovalo	panovat	k5eAaImAgNnS	panovat
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
napětí	napětí	k1gNnSc4	napětí
zejména	zejména	k9	zejména
kvůli	kvůli	k7c3	kvůli
podpoře	podpora	k1gFnSc3	podpora
tehdejšího	tehdejší	k2eAgMnSc2d1	tehdejší
prezidenta	prezident	k1gMnSc2	prezident
Viktora	Viktor	k1gMnSc2	Viktor
Juščenka	Juščenka	k1gFnSc1	Juščenka
vůči	vůči	k7c3	vůči
Gruzii	Gruzie	k1gFnSc3	Gruzie
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Osetii	Osetie	k1gFnSc6	Osetie
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc3	jeho
snahám	snaha	k1gFnPc3	snaha
o	o	k7c4	o
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
NATO	NATO	kA	NATO
<g/>
,	,	kIx,	,
opakovaně	opakovaně	k6eAd1	opakovaně
také	také	k9	také
kvůli	kvůli	k7c3	kvůli
problémům	problém	k1gInPc3	problém
vzniklým	vzniklý	k2eAgInPc3d1	vzniklý
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
dodávkami	dodávka	k1gFnPc7	dodávka
ruského	ruský	k2eAgInSc2d1	ruský
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
2008	[number]	k4	2008
<g/>
/	/	kIx~	/
<g/>
2009	[number]	k4	2009
eskaloval	eskalovat	k5eAaImAgInS	eskalovat
rusko-ukrajinský	ruskokrajinský	k2eAgInSc1d1	rusko-ukrajinský
plynový	plynový	k2eAgInSc1d1	plynový
konflikt	konflikt	k1gInSc1	konflikt
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
ruský	ruský	k2eAgInSc1d1	ruský
koncern	koncern	k1gInSc1	koncern
Gazprom	Gazprom	k1gInSc1	Gazprom
odmítl	odmítnout	k5eAaPmAgInS	odmítnout
dodávat	dodávat	k5eAaImF	dodávat
plyn	plyn	k1gInSc1	plyn
ukrajinskému	ukrajinský	k2eAgInSc3d1	ukrajinský
Naftohazu	Naftohaz	k1gInSc3	Naftohaz
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
ten	ten	k3xDgMnSc1	ten
nesplatí	splatit	k5eNaPmIp3nS	splatit
dluhy	dluh	k1gInPc4	dluh
za	za	k7c4	za
předchozí	předchozí	k2eAgFnPc4d1	předchozí
dodávky	dodávka	k1gFnPc4	dodávka
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
bylo	být	k5eAaImAgNnS	být
třítýdenní	třítýdenní	k2eAgNnSc1d1	třítýdenní
přerušení	přerušení	k1gNnSc1	přerušení
dodávek	dodávka	k1gFnPc2	dodávka
plynu	plyn	k1gInSc2	plyn
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
postihlo	postihnout	k5eAaPmAgNnS	postihnout
nejen	nejen	k6eAd1	nejen
Ukrajinu	Ukrajina	k1gFnSc4	Ukrajina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
střední	střední	k2eAgFnSc4d1	střední
a	a	k8xC	a
jihovýchodní	jihovýchodní	k2eAgFnSc4d1	jihovýchodní
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
dodávky	dodávka	k1gFnPc4	dodávka
ruského	ruský	k2eAgInSc2d1	ruský
plynu	plyn	k1gInSc2	plyn
dlužil	dlužit	k5eAaImAgInS	dlužit
Ukrajinský	ukrajinský	k2eAgInSc1d1	ukrajinský
Naftohaz	Naftohaz	k1gInSc1	Naftohaz
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2013	[number]	k4	2013
zhruba	zhruba	k6eAd1	zhruba
1,3	[number]	k4	1,3
miliardy	miliarda	k4xCgFnSc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
roku	rok	k1gInSc2	rok
proto	proto	k8xC	proto
tato	tento	k3xDgFnSc1	tento
firma	firma	k1gFnSc1	firma
sama	sám	k3xTgFnSc1	sám
zastavila	zastavit	k5eAaPmAgFnS	zastavit
odběr	odběr	k1gInSc4	odběr
a	a	k8xC	a
zásobování	zásobování	k1gNnSc4	zásobování
země	zem	k1gFnSc2	zem
zajišťovala	zajišťovat	k5eAaImAgFnS	zajišťovat
odčerpáváním	odčerpávání	k1gNnSc7	odčerpávání
zásob	zásoba	k1gFnPc2	zásoba
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
případ	případ	k1gInSc4	případ
déle	dlouho	k6eAd2	dlouho
trvajícího	trvající	k2eAgNnSc2d1	trvající
přerušení	přerušení	k1gNnSc2	přerušení
dodávek	dodávka	k1gFnPc2	dodávka
ruského	ruský	k2eAgInSc2d1	ruský
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
přes	přes	k7c4	přes
Ukrajinu	Ukrajina	k1gFnSc4	Ukrajina
byl	být	k5eAaImAgInS	být
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
severní	severní	k2eAgInSc1d1	severní
plynovod	plynovod	k1gInSc1	plynovod
Nord	Norda	k1gFnPc2	Norda
Stream	Stream	k1gInSc1	Stream
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vede	vést	k5eAaImIp3nS	vést
z	z	k7c2	z
Ruska	Rusko	k1gNnSc2	Rusko
pod	pod	k7c7	pod
Baltickým	baltický	k2eAgNnSc7d1	Baltické
mořem	moře	k1gNnSc7	moře
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Užíváním	užívání	k1gNnSc7	užívání
plynovodů	plynovod	k1gInPc2	plynovod
vedoucích	vedoucí	k1gMnPc2	vedoucí
mimo	mimo	k7c4	mimo
území	území	k1gNnSc4	území
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
přichází	přicházet	k5eAaImIp3nS	přicházet
země	zem	k1gFnPc1	zem
o	o	k7c4	o
poplatky	poplatek	k1gInPc4	poplatek
související	související	k2eAgInPc4d1	související
s	s	k7c7	s
tranzitem	tranzit	k1gInSc7	tranzit
plynu	plyn	k1gInSc2	plyn
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
území	území	k1gNnSc6	území
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ruské	ruský	k2eAgFnSc6d1	ruská
anexi	anexe	k1gFnSc6	anexe
Krymu	Krym	k1gInSc2	Krym
<g/>
,	,	kIx,	,
poskytnutí	poskytnutí	k1gNnSc4	poskytnutí
pomoci	pomoct	k5eAaPmF	pomoct
sesazenému	sesazený	k2eAgMnSc3d1	sesazený
prezidentovi	prezident	k1gMnSc3	prezident
Viktoru	Viktor	k1gMnSc3	Viktor
Janukovyčovi	Janukovyč	k1gMnSc3	Janukovyč
a	a	k8xC	a
intervenci	intervence	k1gFnSc3	intervence
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
jsou	být	k5eAaImIp3nP	být
vztahy	vztah	k1gInPc1	vztah
mezi	mezi	k7c7	mezi
Kyjevem	Kyjev	k1gInSc7	Kyjev
a	a	k8xC	a
Moskvou	Moskva	k1gFnSc7	Moskva
na	na	k7c6	na
historickém	historický	k2eAgNnSc6d1	historické
minimu	minimum	k1gNnSc6	minimum
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
zakazovány	zakazován	k2eAgFnPc1d1	zakazována
knihy	kniha	k1gFnPc1	kniha
či	či	k8xC	či
filmy	film	k1gInPc1	film
autorů	autor	k1gMnPc2	autor
nebo	nebo	k8xC	nebo
protagonistů	protagonista	k1gMnPc2	protagonista
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
podle	podle	k7c2	podle
Kyjeva	Kyjev	k1gInSc2	Kyjev
ohrožují	ohrožovat	k5eAaImIp3nP	ohrožovat
ukrajinskou	ukrajinský	k2eAgFnSc4d1	ukrajinská
národní	národní	k2eAgFnSc4d1	národní
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
černou	černý	k2eAgFnSc4d1	černá
listinu	listina	k1gFnSc4	listina
se	se	k3xPyFc4	se
dostali	dostat	k5eAaPmAgMnP	dostat
nejen	nejen	k6eAd1	nejen
Rusové	Rus	k1gMnPc1	Rus
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Ivan	Ivan	k1gMnSc1	Ivan
Ochlobystin	Ochlobystin	k1gMnSc1	Ochlobystin
a	a	k8xC	a
Michail	Michail	k1gMnSc1	Michail
Porečenkov	Porečenkov	k1gInSc1	Porečenkov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zákaz	zákaz	k1gInSc1	zákaz
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
i	i	k9	i
umělců	umělec	k1gMnPc2	umělec
či	či	k8xC	či
spisovatelů	spisovatel	k1gMnPc2	spisovatel
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
nějakým	nějaký	k3yIgInSc7	nějaký
způsobem	způsob	k1gInSc7	způsob
sympatizovali	sympatizovat	k5eAaImAgMnP	sympatizovat
se	s	k7c7	s
současnou	současný	k2eAgFnSc7d1	současná
ruskou	ruský	k2eAgFnSc7d1	ruská
politikou	politika	k1gFnSc7	politika
<g/>
,	,	kIx,	,
navštívili	navštívit	k5eAaPmAgMnP	navštívit
anektovaný	anektovaný	k2eAgInSc4d1	anektovaný
Krym	Krym	k1gInSc4	Krym
a	a	k8xC	a
pod	pod	k7c4	pod
<g/>
.	.	kIx.	.
</s>
<s>
Zakázané	zakázaný	k2eAgInPc1d1	zakázaný
tak	tak	k6eAd1	tak
byly	být	k5eAaImAgInP	být
například	například	k6eAd1	například
filmy	film	k1gInPc1	film
či	či	k8xC	či
fotky	fotka	k1gFnPc1	fotka
s	s	k7c7	s
Gérardem	Gérard	k1gInSc7	Gérard
Depardieu	Depardieus	k1gInSc2	Depardieus
<g/>
,	,	kIx,	,
Stevenem	Steven	k1gMnSc7	Steven
Seagalem	Seagal	k1gMnSc7	Seagal
<g/>
,	,	kIx,	,
Mickey	Micke	k2eAgInPc1d1	Micke
Rourkem	Rourko	k1gNnSc7	Rourko
<g/>
,	,	kIx,	,
Jean-Claude	Jean-Claud	k1gInSc5	Jean-Claud
Van	van	k1gInSc1	van
Dammem	Dammo	k1gNnSc7	Dammo
<g/>
,	,	kIx,	,
filmy	film	k1gInPc1	film
režisérů	režisér	k1gMnPc2	režisér
Emira	Emiro	k1gNnSc2	Emiro
Kusturici	Kusturice	k1gFnSc4	Kusturice
<g/>
,	,	kIx,	,
Olivera	Oliver	k1gMnSc2	Oliver
Stona	Ston	k1gMnSc2	Ston
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
přibližně	přibližně	k6eAd1	přibližně
600	[number]	k4	600
umělců	umělec	k1gMnPc2	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Zákaz	zákaz	k1gInSc1	zákaz
vstupu	vstup	k1gInSc2	vstup
na	na	k7c4	na
Ukrajinu	Ukrajina	k1gFnSc4	Ukrajina
má	mít	k5eAaImIp3nS	mít
z	z	k7c2	z
"	"	kIx"	"
<g/>
bezpečnostních	bezpečnostní	k2eAgInPc2d1	bezpečnostní
důvodů	důvod	k1gInPc2	důvod
<g/>
"	"	kIx"	"
také	také	k9	také
americký	americký	k2eAgMnSc1d1	americký
zpěvák	zpěvák	k1gMnSc1	zpěvák
a	a	k8xC	a
filmový	filmový	k2eAgMnSc1d1	filmový
režisér	režisér	k1gMnSc1	režisér
Fred	Fred	k1gMnSc1	Fred
Durst	Durst	k1gMnSc1	Durst
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgInSc2	ten
daly	dát	k5eAaPmAgInP	dát
ukrajinské	ukrajinský	k2eAgInPc1d1	ukrajinský
úřady	úřad	k1gInPc1	úřad
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
žádnému	žádný	k3yNgMnSc3	žádný
ruskému	ruský	k2eAgMnSc3d1	ruský
zpěvákovi	zpěvák	k1gMnSc3	zpěvák
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
podporuje	podporovat	k5eAaImIp3nS	podporovat
oficiální	oficiální	k2eAgFnSc4d1	oficiální
ruskou	ruský	k2eAgFnSc4d1	ruská
politiku	politika	k1gFnSc4	politika
vůči	vůči	k7c3	vůči
Ukrajině	Ukrajina	k1gFnSc3	Ukrajina
<g/>
,	,	kIx,	,
nepovolí	povolit	k5eNaPmIp3nP	povolit
účast	účast	k1gFnSc4	účast
na	na	k7c6	na
soutěži	soutěž	k1gFnSc6	soutěž
Eurovision	Eurovision	k1gInSc4	Eurovision
Song	song	k1gInSc1	song
Contest	Contest	k1gInSc1	Contest
v	v	k7c6	v
Kyjevě	Kyjev	k1gInSc6	Kyjev
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
takto	takto	k6eAd1	takto
prohibitovaných	prohibitovaný	k2eAgMnPc2d1	prohibitovaný
zpěváků	zpěvák	k1gMnPc2	zpěvák
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
jedenáct	jedenáct	k4xCc1	jedenáct
jmen	jméno	k1gNnPc2	jméno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2017	[number]	k4	2017
byly	být	k5eAaImAgFnP	být
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
zakázány	zakázat	k5eAaPmNgFnP	zakázat
ruské	ruský	k2eAgFnPc1d1	ruská
internetové	internetový	k2eAgFnPc1d1	internetová
servery	server	k1gInPc4	server
VKontakte	VKontakt	k1gInSc5	VKontakt
<g/>
,	,	kIx,	,
Odnoklassniki	Odnoklassniki	k1gNnPc1	Odnoklassniki
<g/>
,	,	kIx,	,
Yandex	Yandex	k1gInSc1	Yandex
a	a	k8xC	a
Mail	mail	k1gInSc1	mail
<g/>
.	.	kIx.	.
<g/>
ru	ru	k?	ru
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
důvod	důvod	k1gInSc4	důvod
uvedla	uvést	k5eAaPmAgFnS	uvést
vláda	vláda	k1gFnSc1	vláda
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
Ruské	ruský	k2eAgFnPc1d1	ruská
tajné	tajný	k2eAgFnPc1d1	tajná
služby	služba	k1gFnPc1	služba
vedou	vést	k5eAaImIp3nP	vést
hybridní	hybridní	k2eAgFnSc4d1	hybridní
válku	válka	k1gFnSc4	válka
proti	proti	k7c3	proti
ukrajinskému	ukrajinský	k2eAgNnSc3d1	ukrajinské
obyvatelstvu	obyvatelstvo	k1gNnSc3	obyvatelstvo
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
ke	k	k7c3	k
svým	svůj	k3xOyFgFnPc3	svůj
speciálním	speciální	k2eAgFnPc3d1	speciální
informačním	informační	k2eAgFnPc3d1	informační
operacím	operace	k1gFnPc3	operace
zneužívají	zneužívat	k5eAaImIp3nP	zneužívat
internetové	internetový	k2eAgFnSc2d1	internetová
sítě	síť	k1gFnSc2	síť
VKontakte	VKontakt	k1gInSc5	VKontakt
<g/>
,	,	kIx,	,
Odnoklassniki	Odnoklassniki	k1gNnPc1	Odnoklassniki
<g/>
,	,	kIx,	,
Mail	mail	k1gInSc1	mail
<g/>
.	.	kIx.	.
<g/>
ru	ru	k?	ru
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zákaz	zákaz	k1gInSc1	zákaz
byl	být	k5eAaImAgInS	být
kritizován	kritizovat	k5eAaImNgInS	kritizovat
jako	jako	k8xS	jako
porušování	porušování	k1gNnSc1	porušování
svobody	svoboda	k1gFnSc2	svoboda
slova	slovo	k1gNnSc2	slovo
a	a	k8xC	a
svobodného	svobodný	k2eAgNnSc2d1	svobodné
šíření	šíření	k1gNnSc2	šíření
informací	informace	k1gFnPc2	informace
milionů	milion	k4xCgInPc2	milion
ukrajinských	ukrajinský	k2eAgMnPc2d1	ukrajinský
uživatelů	uživatel	k1gMnPc2	uživatel
Mezinárodní	mezinárodní	k2eAgFnSc7d1	mezinárodní
organizací	organizace	k1gFnSc7	organizace
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Polsko	Polsko	k1gNnSc1	Polsko
iniciovalo	iniciovat	k5eAaBmAgNnS	iniciovat
projekt	projekt	k1gInSc4	projekt
Východního	východní	k2eAgNnSc2d1	východní
partnerství	partnerství	k1gNnSc2	partnerství
mezi	mezi	k7c7	mezi
Evropskou	evropský	k2eAgFnSc7d1	Evropská
unií	unie	k1gFnSc7	unie
a	a	k8xC	a
šesti	šest	k4xCc7	šest
postsovětskými	postsovětský	k2eAgFnPc7d1	postsovětská
zeměmi	zem	k1gFnPc7	zem
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
aktivně	aktivně	k6eAd1	aktivně
podporovalo	podporovat	k5eAaImAgNnS	podporovat
proevropské	proevropský	k2eAgNnSc1d1	proevropské
hnutí	hnutí	k1gNnSc1	hnutí
Euromajdan	Euromajdany	k1gInPc2	Euromajdany
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
Litva	Litva	k1gFnSc1	Litva
a	a	k8xC	a
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
plánují	plánovat	k5eAaImIp3nP	plánovat
vytvořit	vytvořit	k5eAaPmF	vytvořit
společnou	společný	k2eAgFnSc4d1	společná
armádní	armádní	k2eAgFnSc4d1	armádní
brigádu	brigáda	k1gFnSc4	brigáda
na	na	k7c4	na
obranu	obrana	k1gFnSc4	obrana
před	před	k7c7	před
Ruskem	Rusko	k1gNnSc7	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Polsko-ukrajinské	polskokrajinský	k2eAgInPc4d1	polsko-ukrajinský
vztahy	vztah	k1gInPc4	vztah
komplikuje	komplikovat	k5eAaBmIp3nS	komplikovat
odlišná	odlišný	k2eAgFnSc1d1	odlišná
interpretace	interpretace	k1gFnSc1	interpretace
některých	některý	k3yIgFnPc2	některý
historických	historický	k2eAgFnPc2d1	historická
událostí	událost	k1gFnPc2	událost
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Volyňského	volyňský	k2eAgInSc2d1	volyňský
masakru	masakr	k1gInSc2	masakr
–	–	k?	–
vyvraždění	vyvraždění	k1gNnSc2	vyvraždění
zhruba	zhruba	k6eAd1	zhruba
100	[number]	k4	100
000	[number]	k4	000
polských	polský	k2eAgMnPc2d1	polský
civilistů	civilista	k1gMnPc2	civilista
Ukrajinskou	ukrajinský	k2eAgFnSc7d1	ukrajinská
povstaleckou	povstalecký	k2eAgFnSc7d1	povstalecká
armádou	armáda	k1gFnSc7	armáda
(	(	kIx(	(
<g/>
UPA	UPA	kA	UPA
<g/>
)	)	kIx)	)
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
ve	v	k7c6	v
Volyni	Volyně	k1gFnSc6	Volyně
na	na	k7c6	na
západě	západ	k1gInSc6	západ
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
a	a	k8xC	a
následné	následný	k2eAgInPc1d1	následný
odvetné	odvetný	k2eAgInPc1d1	odvetný
masakry	masakr	k1gInPc1	masakr
ukrajinských	ukrajinský	k2eAgMnPc2d1	ukrajinský
civilistů	civilista	k1gMnPc2	civilista
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2015	[number]	k4	2015
Parlament	parlament	k1gInSc1	parlament
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
schválil	schválit	k5eAaPmAgInS	schválit
zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
udělil	udělit	k5eAaPmAgMnS	udělit
příslušníkům	příslušník	k1gMnPc3	příslušník
UPA	UPA	kA	UPA
a	a	k8xC	a
Organizace	organizace	k1gFnSc1	organizace
ukrajinských	ukrajinský	k2eAgMnPc2d1	ukrajinský
nacionalistů	nacionalista	k1gMnPc2	nacionalista
(	(	kIx(	(
<g/>
OUN	OUN	kA	OUN
<g/>
)	)	kIx)	)
status	status	k1gInSc4	status
bojovníků	bojovník	k1gMnPc2	bojovník
za	za	k7c4	za
svobodu	svoboda	k1gFnSc4	svoboda
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Petro	Petra	k1gFnSc5	Petra
Porošenko	Porošenka	k1gFnSc5	Porošenka
označil	označit	k5eAaPmAgMnS	označit
bojovníky	bojovník	k1gMnPc4	bojovník
UPA	UPA	kA	UPA
za	za	k7c4	za
"	"	kIx"	"
<g/>
příklad	příklad	k1gInSc4	příklad
hrdinství	hrdinství	k1gNnSc2	hrdinství
a	a	k8xC	a
vlastenectví	vlastenectví	k1gNnSc2	vlastenectví
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2016	[number]	k4	2016
označil	označit	k5eAaPmAgInS	označit
polský	polský	k2eAgInSc1d1	polský
Sejm	Sejm	k1gInSc1	Sejm
masakry	masakr	k1gInPc1	masakr
polského	polský	k2eAgNnSc2d1	polské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
na	na	k7c6	na
Volyni	Volyně	k1gFnSc6	Volyně
za	za	k7c4	za
genocidu	genocida	k1gFnSc4	genocida
<g/>
.	.	kIx.	.
</s>
<s>
Polský	polský	k2eAgInSc1d1	polský
historický	historický	k2eAgInSc1d1	historický
film	film	k1gInSc1	film
Volyň	Volyně	k1gFnPc2	Volyně
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
oceněn	ocenit	k5eAaPmNgInS	ocenit
jako	jako	k8xS	jako
nejlepší	dobrý	k2eAgInSc1d3	nejlepší
polský	polský	k2eAgInSc1d1	polský
film	film	k1gInSc1	film
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
přijat	přijat	k2eAgMnSc1d1	přijat
negativně	negativně	k6eAd1	negativně
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
promítání	promítání	k1gNnSc4	promítání
v	v	k7c6	v
Kyjevě	Kyjev	k1gInSc6	Kyjev
bylo	být	k5eAaImAgNnS	být
na	na	k7c4	na
žádost	žádost	k1gFnSc4	žádost
ukrajinského	ukrajinský	k2eAgNnSc2d1	ukrajinské
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
zahraničí	zahraničí	k1gNnSc2	zahraničí
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
3	[number]	k4	3
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2015	[number]	k4	2015
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Evropská	evropský	k2eAgFnSc1d1	Evropská
komise	komise	k1gFnSc1	komise
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
početný	početný	k2eAgInSc4d1	početný
tým	tým	k1gInSc4	tým
poradců	poradce	k1gMnPc2	poradce
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
pomoci	pomoct	k5eAaPmF	pomoct
Ukrajině	Ukrajina	k1gFnSc3	Ukrajina
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
politicky	politicky	k6eAd1	politicky
a	a	k8xC	a
ekonomicky	ekonomicky	k6eAd1	ekonomicky
stabilizovala	stabilizovat	k5eAaBmAgFnS	stabilizovat
<g/>
.	.	kIx.	.
</s>
<s>
Tým	tým	k1gInSc1	tým
bude	být	k5eAaImBp3nS	být
pracovat	pracovat	k5eAaImF	pracovat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
"	"	kIx"	"
<g/>
Agentury	agentura	k1gFnSc2	agentura
pro	pro	k7c4	pro
modernizaci	modernizace	k1gFnSc4	modernizace
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Budou	být	k5eAaImBp3nP	být
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
zastoupeni	zastoupen	k2eAgMnPc1d1	zastoupen
významní	významný	k2eAgMnPc1d1	významný
politikové	politik	k1gMnPc1	politik
včetně	včetně	k7c2	včetně
bývalých	bývalý	k2eAgMnPc2d1	bývalý
komisařů	komisař	k1gMnPc2	komisař
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Bývalý	bývalý	k2eAgMnSc1d1	bývalý
německý	německý	k2eAgMnSc1d1	německý
ministr	ministr	k1gMnSc1	ministr
financí	finance	k1gFnPc2	finance
Peer	peer	k1gMnSc1	peer
Steinbrück	Steinbrück	k1gMnSc1	Steinbrück
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
na	na	k7c4	na
starosti	starost	k1gFnPc4	starost
ukrajinské	ukrajinský	k2eAgFnSc2d1	ukrajinská
státní	státní	k2eAgFnSc2d1	státní
finance	finance	k1gFnSc2	finance
<g/>
.	.	kIx.	.
</s>
<s>
Francouzka	Francouzka	k1gFnSc1	Francouzka
Laurence	Laurence	k1gFnSc2	Laurence
Parisot	Parisota	k1gFnPc2	Parisota
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
starat	starat	k5eAaImF	starat
o	o	k7c4	o
hospodářské	hospodářský	k2eAgFnPc4d1	hospodářská
záležitosti	záležitost	k1gFnPc4	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
Němec	Němec	k1gMnSc1	Němec
Günter	Güntra	k1gFnPc2	Güntra
Verheugen	Verheugen	k1gInSc1	Verheugen
bude	být	k5eAaImBp3nS	být
příslušný	příslušný	k2eAgInSc1d1	příslušný
pro	pro	k7c4	pro
podniky	podnik	k1gInPc4	podnik
a	a	k8xC	a
průmysl	průmysl	k1gInSc1	průmysl
<g/>
,	,	kIx,	,
Čech	Čech	k1gMnSc1	Čech
Štefan	Štefan	k1gMnSc1	Štefan
Füle	Füle	k1gNnSc4	Füle
pro	pro	k7c4	pro
otázky	otázka	k1gFnPc4	otázka
integrace	integrace	k1gFnSc2	integrace
do	do	k7c2	do
EU	EU	kA	EU
a	a	k8xC	a
Angličan	Angličan	k1gMnSc1	Angličan
Peter	Peter	k1gMnSc1	Peter
Mandelson	Mandelson	k1gMnSc1	Mandelson
pro	pro	k7c4	pro
obchodní	obchodní	k2eAgFnPc4d1	obchodní
záležitosti	záležitost	k1gFnPc4	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
Bývalý	bývalý	k2eAgMnSc1d1	bývalý
německý	německý	k2eAgMnSc1d1	německý
ministr	ministr	k1gMnSc1	ministr
obrany	obrana	k1gFnSc2	obrana
Rupert	Rupert	k1gMnSc1	Rupert
Scholz	Scholz	k1gMnSc1	Scholz
bude	být	k5eAaImBp3nS	být
doprovázet	doprovázet	k5eAaImF	doprovázet
nutné	nutný	k2eAgFnPc4d1	nutná
změny	změna	k1gFnPc4	změna
ukrajinské	ukrajinský	k2eAgFnSc2d1	ukrajinská
ústavy	ústava	k1gFnSc2	ústava
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
decentralizovaným	decentralizovaný	k2eAgFnPc3d1	decentralizovaná
strukturám	struktura	k1gFnPc3	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Francouzský	francouzský	k2eAgMnSc1d1	francouzský
politik	politik	k1gMnSc1	politik
Bernard	Bernard	k1gMnSc1	Bernard
Kouchner	Kouchner	k1gMnSc1	Kouchner
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
ministr	ministr	k1gMnSc1	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
a	a	k8xC	a
lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
radit	radit	k5eAaImF	radit
Ukrajině	Ukrajina	k1gFnSc3	Ukrajina
v	v	k7c6	v
otázkách	otázka	k1gFnPc6	otázka
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
<g/>
.	.	kIx.	.
</s>
<s>
Ředitelem	ředitel	k1gMnSc7	ředitel
zmíněné	zmíněný	k2eAgFnSc2d1	zmíněná
agentury	agentura	k1gFnSc2	agentura
bude	být	k5eAaImBp3nS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
rakouský	rakouský	k2eAgMnSc1d1	rakouský
ministr	ministr	k1gMnSc1	ministr
financí	finance	k1gFnPc2	finance
Michael	Michael	k1gMnSc1	Michael
Spindelegger	Spindelegger	k1gMnSc1	Spindelegger
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ekonomika	ekonomika	k1gFnSc1	ekonomika
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
ekonomika	ekonomika	k1gFnSc1	ekonomika
byla	být	k5eAaImAgFnS	být
druhou	druhý	k4xOgFnSc7	druhý
největší	veliký	k2eAgFnSc7d3	veliký
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
rozpadu	rozpad	k1gInSc6	rozpad
bylo	být	k5eAaImAgNnS	být
nastoleno	nastolen	k2eAgNnSc1d1	nastoleno
tržní	tržní	k2eAgNnSc1d1	tržní
hospodářství	hospodářství	k1gNnSc1	hospodářství
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
nástup	nástup	k1gInSc1	nástup
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
"	"	kIx"	"
<g/>
šokové	šokový	k2eAgFnSc2d1	šoková
terapie	terapie	k1gFnSc2	terapie
<g/>
"	"	kIx"	"
byl	být	k5eAaImAgMnS	být
velice	velice	k6eAd1	velice
bolestný	bolestný	k2eAgMnSc1d1	bolestný
<g/>
:	:	kIx,	:
zemi	zem	k1gFnSc4	zem
postihla	postihnout	k5eAaPmAgFnS	postihnout
hyperinflace	hyperinflace	k1gFnSc1	hyperinflace
<g/>
,	,	kIx,	,
mnoho	mnoho	k4c1	mnoho
podniků	podnik	k1gInPc2	podnik
ukončilo	ukončit	k5eAaPmAgNnS	ukončit
výrobu	výroba	k1gFnSc4	výroba
<g/>
,	,	kIx,	,
privatizace	privatizace	k1gFnSc1	privatizace
probíhaly	probíhat	k5eAaImAgInP	probíhat
neprůhledným	průhledný	k2eNgInSc7d1	neprůhledný
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
částečně	částečně	k6eAd1	částečně
stabilizovala	stabilizovat	k5eAaBmAgFnS	stabilizovat
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
po	po	k7c6	po
zavedení	zavedení	k1gNnSc6	zavedení
nové	nový	k2eAgFnSc2d1	nová
měny	měna	k1gFnSc2	měna
<g/>
,	,	kIx,	,
ukrajinské	ukrajinský	k2eAgFnSc2d1	ukrajinská
hřivny	hřivna	k1gFnSc2	hřivna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvních	první	k4xOgNnPc6	první
letech	léto	k1gNnPc6	léto
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
ekonomika	ekonomika	k1gFnSc1	ekonomika
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
poměrně	poměrně	k6eAd1	poměrně
rychlého	rychlý	k2eAgInSc2d1	rychlý
a	a	k8xC	a
stabilního	stabilní	k2eAgInSc2d1	stabilní
růstu	růst	k1gInSc2	růst
HDP	HDP	kA	HDP
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
činil	činit	k5eAaImAgInS	činit
7,3	[number]	k4	7,3
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
byla	být	k5eAaImAgFnS	být
však	však	k9	však
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
těžce	těžce	k6eAd1	těžce
zasažena	zasáhnout	k5eAaPmNgFnS	zasáhnout
světovou	světový	k2eAgFnSc7d1	světová
ekonomickou	ekonomický	k2eAgFnSc7d1	ekonomická
krizí	krize	k1gFnSc7	krize
<g/>
,	,	kIx,	,
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
čehož	což	k3yQnSc2	což
se	se	k3xPyFc4	se
ekonomika	ekonomika	k1gFnSc1	ekonomika
prudce	prudko	k6eAd1	prudko
propadla	propadnout	k5eAaPmAgFnS	propadnout
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
o	o	k7c4	o
více	hodně	k6eAd2	hodně
než	než	k8xS	než
15	[number]	k4	15
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
činil	činit	k5eAaImAgInS	činit
růst	růst	k1gInSc1	růst
HDP	HDP	kA	HDP
pouze	pouze	k6eAd1	pouze
0,2	[number]	k4	0,2
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
0,36	[number]	k4	0,36
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
MMF	MMF	kA	MMF
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
předpovídal	předpovídat	k5eAaImAgInS	předpovídat
růst	růst	k1gInSc1	růst
HDP	HDP	kA	HDP
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
o	o	k7c4	o
1	[number]	k4	1
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
tohoto	tento	k3xDgInSc2	tento
cíle	cíl	k1gInSc2	cíl
se	s	k7c7	s
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
nestabilní	stabilní	k2eNgFnSc3d1	nestabilní
politické	politický	k2eAgFnSc3d1	politická
situaci	situace	k1gFnSc3	situace
<g/>
,	,	kIx,	,
odtržení	odtržení	k1gNnSc4	odtržení
Krymu	Krym	k1gInSc2	Krym
a	a	k8xC	a
především	především	k9	především
kvůli	kvůli	k7c3	kvůli
válce	válka	k1gFnSc3	válka
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
se	s	k7c7	s
všemi	všecek	k3xTgInPc7	všecek
jejími	její	k3xOp3gInPc7	její
průvodními	průvodní	k2eAgInPc7d1	průvodní
jevy	jev	k1gInPc7	jev
nepodařilo	podařit	k5eNaPmAgNnS	podařit
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
<g/>
,	,	kIx,	,
ukrajinský	ukrajinský	k2eAgMnSc1d1	ukrajinský
HDP	HDP	kA	HDP
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
značně	značně	k6eAd1	značně
poklesl	poklesnout	k5eAaPmAgInS	poklesnout
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
přesná	přesný	k2eAgFnSc1d1	přesná
míra	míra	k1gFnSc1	míra
zatím	zatím	k6eAd1	zatím
není	být	k5eNaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
Českem	Česko	k1gNnSc7	Česko
a	a	k8xC	a
také	také	k9	také
Ruskem	Rusko	k1gNnSc7	Rusko
nemá	mít	k5eNaImIp3nS	mít
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
srovnatelnou	srovnatelný	k2eAgFnSc4d1	srovnatelná
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
výkonnost	výkonnost	k1gFnSc4	výkonnost
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc4	její
HDP	HDP	kA	HDP
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
(	(	kIx(	(
<g/>
v	v	k7c6	v
paritě	parita	k1gFnSc6	parita
kupní	kupní	k2eAgFnSc2d1	kupní
síly	síla	k1gFnSc2	síla
<g/>
,	,	kIx,	,
purchasing-power	purchasingower	k1gInSc4	purchasing-power
parity	parita	k1gFnSc2	parita
<g/>
,	,	kIx,	,
PPP	PPP	kA	PPP
<g/>
)	)	kIx)	)
činil	činit	k5eAaImAgInS	činit
7	[number]	k4	7
421	[number]	k4	421
USD	USD	kA	USD
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
HDP	HDP	kA	HDP
Česka	Česko	k1gNnSc2	Česko
byl	být	k5eAaImAgInS	být
19	[number]	k4	19
000	[number]	k4	000
USD	USD	kA	USD
a	a	k8xC	a
Ruska	Rusko	k1gNnSc2	Rusko
15	[number]	k4	15
000	[number]	k4	000
USD	USD	kA	USD
na	na	k7c4	na
jednoho	jeden	k4xCgMnSc4	jeden
obyvatele	obyvatel	k1gMnSc4	obyvatel
(	(	kIx(	(
<g/>
oba	dva	k4xCgInPc4	dva
státy	stát	k1gInPc4	stát
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomika	ekonomika	k1gFnSc1	ekonomika
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
srovnatelná	srovnatelný	k2eAgFnSc1d1	srovnatelná
s	s	k7c7	s
rozvojovými	rozvojový	k2eAgInPc7d1	rozvojový
státy	stát	k1gInPc7	stát
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
Mongolsko	Mongolsko	k1gNnSc4	Mongolsko
<g/>
,	,	kIx,	,
Paraguay	Paraguay	k1gFnSc4	Paraguay
a	a	k8xC	a
Salvador	Salvador	k1gInSc4	Salvador
<g/>
.	.	kIx.	.
</s>
<s>
Ukrajinské	ukrajinský	k2eAgNnSc1d1	ukrajinské
hospodářství	hospodářství	k1gNnSc1	hospodářství
nadále	nadále	k6eAd1	nadále
nese	nést	k5eAaImIp3nS	nést
znaky	znak	k1gInPc4	znak
postsovětské	postsovětský	k2eAgFnSc2d1	postsovětská
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
,	,	kIx,	,
orientované	orientovaný	k2eAgInPc4d1	orientovaný
na	na	k7c4	na
obory	obor	k1gInPc4	obor
s	s	k7c7	s
nízkou	nízký	k2eAgFnSc7d1	nízká
přidanou	přidaný	k2eAgFnSc7d1	přidaná
hodnotou	hodnota	k1gFnSc7	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
váhu	váha	k1gFnSc4	váha
má	mít	k5eAaImIp3nS	mít
těžký	těžký	k2eAgInSc1d1	těžký
průmysl	průmysl	k1gInSc1	průmysl
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
metalurgie	metalurgie	k1gFnSc1	metalurgie
a	a	k8xC	a
těžba	těžba	k1gFnSc1	těžba
uhlí	uhlí	k1gNnSc2	uhlí
ve	v	k7c6	v
válkou	válka	k1gFnSc7	válka
postiženém	postižený	k2eAgInSc6d1	postižený
Donbasu	Donbas	k1gInSc6	Donbas
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
však	však	k9	však
technicky	technicky	k6eAd1	technicky
a	a	k8xC	a
technologicky	technologicky	k6eAd1	technologicky
zastaralý	zastaralý	k2eAgInSc1d1	zastaralý
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
finanční	finanční	k2eAgFnSc2d1	finanční
krize	krize	k1gFnSc2	krize
přijala	přijmout	k5eAaPmAgFnS	přijmout
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
–	–	k?	–
ve	v	k7c4	v
stejnou	stejný	k2eAgFnSc4d1	stejná
dobu	doba	k1gFnSc4	doba
jako	jako	k8xS	jako
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
–	–	k?	–
mnohamiliardový	mnohamiliardový	k2eAgInSc1d1	mnohamiliardový
dolarový	dolarový	k2eAgInSc1d1	dolarový
úvěr	úvěr	k1gInSc1	úvěr
od	od	k7c2	od
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
měnového	měnový	k2eAgInSc2d1	měnový
fondu	fond	k1gInSc2	fond
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgMnPc7d3	veliký
obchodními	obchodní	k2eAgMnPc7d1	obchodní
partnery	partner	k1gMnPc7	partner
jsou	být	k5eAaImIp3nP	být
země	zem	k1gFnPc1	zem
EU	EU	kA	EU
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
(	(	kIx(	(
<g/>
potažmo	potažmo	k6eAd1	potažmo
SNS	SNS	kA	SNS
<g/>
)	)	kIx)	)
a	a	k8xC	a
Turecko	Turecko	k1gNnSc1	Turecko
<g/>
.	.	kIx.	.
</s>
<s>
Podíl	podíl	k1gInSc1	podíl
na	na	k7c6	na
tvorbě	tvorba	k1gFnSc6	tvorba
HDP	HDP	kA	HDP
<g/>
:	:	kIx,	:
průmysl	průmysl	k1gInSc1	průmysl
31	[number]	k4	31
%	%	kIx~	%
<g/>
,	,	kIx,	,
stavebnictví	stavebnictví	k1gNnSc1	stavebnictví
5	[number]	k4	5
%	%	kIx~	%
<g/>
,	,	kIx,	,
zemědělství	zemědělství	k1gNnSc2	zemědělství
5	[number]	k4	5
%	%	kIx~	%
<g/>
,	,	kIx,	,
služby	služba	k1gFnSc2	služba
55	[number]	k4	55
%	%	kIx~	%
HDP	HDP	kA	HDP
(	(	kIx(	(
<g/>
parita	parita	k1gFnSc1	parita
kupní	kupní	k2eAgFnSc2d1	kupní
síly	síla	k1gFnSc2	síla
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
332	[number]	k4	332
646	[number]	k4	646
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
</s>
<s>
USD	USD	kA	USD
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
184	[number]	k4	184
566	[number]	k4	566
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
</s>
<s>
USD	USD	kA	USD
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
399	[number]	k4	399
866	[number]	k4	866
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
</s>
<s>
USD	USD	kA	USD
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
428	[number]	k4	428
973	[number]	k4	973
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
</s>
<s>
USD	USD	kA	USD
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
HDP	HDP	kA	HDP
na	na	k7c4	na
1	[number]	k4	1
obyv	obyva	k1gFnPc2	obyva
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
parita	parita	k1gFnSc1	parita
kupní	kupní	k2eAgFnSc2d1	kupní
síly	síla	k1gFnSc2	síla
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
8	[number]	k4	8
624	[number]	k4	624
USD	USD	kA	USD
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
mzda	mzda	k1gFnSc1	mzda
státních	státní	k2eAgMnPc2d1	státní
pracovníků	pracovník	k1gMnPc2	pracovník
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2008	[number]	k4	2008
činila	činit	k5eAaImAgFnS	činit
1930	[number]	k4	1930
hřiven	hřivna	k1gFnPc2	hřivna
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
přibližně	přibližně	k6eAd1	přibližně
398	[number]	k4	398
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
2008	[number]	k4	2008
hřiven	hřivna	k1gFnPc2	hřivna
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
kvůli	kvůli	k7c3	kvůli
propadu	propad	k1gInSc3	propad
hřivny	hřivna	k1gFnSc2	hřivna
kvůli	kvůli	k7c3	kvůli
ekonomické	ekonomický	k2eAgFnSc3d1	ekonomická
krizi	krize	k1gFnSc3	krize
činil	činit	k5eAaImAgInS	činit
přepočet	přepočet	k1gInSc1	přepočet
jen	jen	k9	jen
251	[number]	k4	251
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
mzda	mzda	k1gFnSc1	mzda
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2012	[number]	k4	2012
činila	činit	k5eAaImAgFnS	činit
294,6	[number]	k4	294,6
EUR	euro	k1gNnPc2	euro
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
sektorů	sektor	k1gInPc2	sektor
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
průměrná	průměrný	k2eAgFnSc1d1	průměrná
mzda	mzda	k1gFnSc1	mzda
v	v	k7c6	v
průmyslu	průmysl	k1gInSc6	průmysl
(	(	kIx(	(
<g/>
340,7	[number]	k4	340,7
EUR	euro	k1gNnPc2	euro
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
následují	následovat	k5eAaImIp3nP	následovat
stavebnictví	stavebnictví	k1gNnSc4	stavebnictví
(	(	kIx(	(
<g/>
242,5	[number]	k4	242,5
EUR	euro	k1gNnPc2	euro
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obchod	obchod	k1gInSc1	obchod
(	(	kIx(	(
<g/>
262,5	[number]	k4	262,5
EUR	euro	k1gNnPc2	euro
<g/>
)	)	kIx)	)
a	a	k8xC	a
ubytování	ubytování	k1gNnSc4	ubytování
a	a	k8xC	a
stravování	stravování	k1gNnSc4	stravování
(	(	kIx(	(
<g/>
200,2	[number]	k4	200,2
EUR	euro	k1gNnPc2	euro
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
i	i	k9	i
přes	přes	k7c4	přes
nižší	nízký	k2eAgFnPc4d2	nižší
ceny	cena	k1gFnPc4	cena
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
státy	stát	k1gInPc7	stát
poměrně	poměrně	k6eAd1	poměrně
málo	málo	k6eAd1	málo
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
početné	početný	k2eAgFnSc3d1	početná
pracovní	pracovní	k2eAgFnSc3d1	pracovní
migraci	migrace	k1gFnSc3	migrace
zejména	zejména	k9	zejména
do	do	k7c2	do
Česka	Česko	k1gNnSc2	Česko
<g/>
,	,	kIx,	,
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
především	především	k9	především
do	do	k7c2	do
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
do	do	k7c2	do
Moskvy	Moskva	k1gFnSc2	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
Míra	Míra	k1gFnSc1	Míra
nezaměstnanosti	nezaměstnanost	k1gFnSc2	nezaměstnanost
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
8	[number]	k4	8
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
podíl	podíl	k1gInSc1	podíl
ekonomicky	ekonomicky	k6eAd1	ekonomicky
aktivního	aktivní	k2eAgNnSc2d1	aktivní
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
pracuje	pracovat	k5eAaImIp3nS	pracovat
ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
(	(	kIx(	(
<g/>
60,7	[number]	k4	60,7
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
stavebnictví	stavebnictví	k1gNnSc2	stavebnictví
jich	on	k3xPp3gMnPc2	on
pracuje	pracovat	k5eAaImIp3nS	pracovat
23,4	[number]	k4	23,4
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
a	a	k8xC	a
nejmenšího	malý	k2eAgInSc2d3	nejmenší
podílu	podíl	k1gInSc2	podíl
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
zemědělství	zemědělství	k1gNnSc1	zemědělství
(	(	kIx(	(
<g/>
15,8	[number]	k4	15,8
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
hřivna	hřivna	k1gFnSc1	hřivna
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
měnám	měna	k1gFnPc3	měna
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgInSc1d1	americký
dolar	dolar	k1gInSc1	dolar
(	(	kIx(	(
<g/>
USD	USD	kA	USD
<g/>
)	)	kIx)	)
a	a	k8xC	a
euro	euro	k1gNnSc1	euro
(	(	kIx(	(
<g/>
EUR	euro	k1gNnPc2	euro
<g/>
)	)	kIx)	)
již	již	k6eAd1	již
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
značně	značně	k6eAd1	značně
propadla	propadnout	k5eAaPmAgFnS	propadnout
<g/>
,	,	kIx,	,
a	a	k8xC	a
sice	sice	k8xC	sice
o	o	k7c4	o
50	[number]	k4	50
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
rychlejším	rychlý	k2eAgNnSc7d2	rychlejší
tempem	tempo	k1gNnSc7	tempo
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
hřivna	hřivna	k1gFnSc1	hřivna
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
když	když	k8xS	když
do	do	k7c2	do
konce	konec	k1gInSc2	konec
února	únor	k1gInSc2	únor
přišla	přijít	k5eAaPmAgFnS	přijít
o	o	k7c4	o
dalších	další	k2eAgInPc2d1	další
50	[number]	k4	50
%	%	kIx~	%
své	svůj	k3xOyFgFnSc2	svůj
dosavadní	dosavadní	k2eAgFnSc2d1	dosavadní
hodnoty	hodnota	k1gFnSc2	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Hřivna	hřivna	k1gFnSc1	hřivna
má	mít	k5eAaImIp3nS	mít
tedy	tedy	k9	tedy
nyní	nyní	k6eAd1	nyní
vůči	vůči	k7c3	vůči
důležitým	důležitý	k2eAgFnPc3d1	důležitá
světovým	světový	k2eAgFnPc3d1	světová
měnám	měna	k1gFnPc3	měna
pouze	pouze	k6eAd1	pouze
čtvrtinu	čtvrtina	k1gFnSc4	čtvrtina
hodnoty	hodnota	k1gFnSc2	hodnota
z	z	k7c2	z
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc4	její
směnný	směnný	k2eAgInSc4d1	směnný
kurz	kurz	k1gInSc4	kurz
oproti	oproti	k7c3	oproti
USD	USD	kA	USD
je	být	k5eAaImIp3nS	být
aktuálně	aktuálně	k6eAd1	aktuálně
25	[number]	k4	25
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
národní	národní	k2eAgFnSc1d1	národní
banka	banka	k1gFnSc1	banka
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
tento	tento	k3xDgInSc1	tento
proces	proces	k1gInSc1	proces
alespoň	alespoň	k9	alespoň
zpomalit	zpomalit	k5eAaPmF	zpomalit
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
zvedá	zvedat	k5eAaImIp3nS	zvedat
hlavní	hlavní	k2eAgFnSc4d1	hlavní
úrokovou	úrokový	k2eAgFnSc4d1	úroková
míru	míra	k1gFnSc4	míra
<g/>
.	.	kIx.	.
</s>
<s>
Naposledy	naposledy	k6eAd1	naposledy
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stalo	stát	k5eAaPmAgNnS	stát
2	[number]	k4	2
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
z	z	k7c2	z
19,5	[number]	k4	19,5
%	%	kIx~	%
ročně	ročně	k6eAd1	ročně
na	na	k7c4	na
rekordních	rekordní	k2eAgInPc2d1	rekordní
30	[number]	k4	30
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
vláda	vláda	k1gFnSc1	vláda
činí	činit	k5eAaImIp3nS	činit
další	další	k2eAgNnPc4d1	další
opatření	opatření	k1gNnPc4	opatření
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
propad	propad	k1gInSc4	propad
ekonomiky	ekonomika	k1gFnSc2	ekonomika
zbrzdila	zbrzdit	k5eAaPmAgFnS	zbrzdit
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
se	se	k3xPyFc4	se
uchází	ucházet	k5eAaImIp3nS	ucházet
o	o	k7c4	o
další	další	k2eAgFnSc4d1	další
půjčku	půjčka	k1gFnSc4	půjčka
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
měnového	měnový	k2eAgInSc2d1	měnový
fondu	fond	k1gInSc2	fond
(	(	kIx(	(
<g/>
MMF	MMF	kA	MMF
<g/>
)	)	kIx)	)
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
17,5	[number]	k4	17,5
miliardy	miliarda	k4xCgFnSc2	miliarda
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
plánovanou	plánovaný	k2eAgFnSc7d1	plánovaná
pomocí	pomoc	k1gFnSc7	pomoc
od	od	k7c2	od
MMF	MMF	kA	MMF
jsou	být	k5eAaImIp3nP	být
nezávislí	závislý	k2eNgMnPc1d1	nezávislý
pozorovatelé	pozorovatel	k1gMnPc1	pozorovatel
ovšem	ovšem	k9	ovšem
toho	ten	k3xDgInSc2	ten
názoru	názor	k1gInSc2	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
se	se	k3xPyFc4	se
jako	jako	k9	jako
dlužník	dlužník	k1gMnSc1	dlužník
stává	stávat	k5eAaImIp3nS	stávat
"	"	kIx"	"
<g/>
bezedným	bezedný	k2eAgInSc7d1	bezedný
sudem	sud	k1gInSc7	sud
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
nemohou	moct	k5eNaImIp3nP	moct
ani	ani	k9	ani
vysoké	vysoký	k2eAgFnPc4d1	vysoká
půjčky	půjčka	k1gFnPc4	půjčka
úplně	úplně	k6eAd1	úplně
zacelit	zacelit	k5eAaPmF	zacelit
<g/>
.	.	kIx.	.
</s>
<s>
Ukrajinské	ukrajinský	k2eAgNnSc1d1	ukrajinské
nerostné	nerostný	k2eAgNnSc1d1	nerostné
bohatství	bohatství	k1gNnSc1	bohatství
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
pilířů	pilíř	k1gInPc2	pilíř
sovětské	sovětský	k2eAgFnSc2d1	sovětská
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitějším	důležitý	k2eAgInSc7d3	nejdůležitější
sektorem	sektor	k1gInSc7	sektor
je	být	k5eAaImIp3nS	být
těžba	těžba	k1gFnSc1	těžba
černého	černý	k2eAgNnSc2d1	černé
uhlí	uhlí	k1gNnSc2	uhlí
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Donbasu	Donbas	k1gInSc2	Donbas
(	(	kIx(	(
<g/>
s	s	k7c7	s
hlavními	hlavní	k2eAgFnPc7d1	hlavní
centry	centrum	k1gNnPc7	centrum
Doněck	Doněck	k1gInSc1	Doněck
a	a	k8xC	a
Luhansk	Luhansk	k1gInSc1	Luhansk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
hlavním	hlavní	k2eAgInSc7d1	hlavní
průmyslovým	průmyslový	k2eAgInSc7d1	průmyslový
regionem	region	k1gInSc7	region
země	zem	k1gFnSc2	zem
<g/>
:	:	kIx,	:
ročně	ročně	k6eAd1	ročně
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
v	v	k7c6	v
uplynulých	uplynulý	k2eAgNnPc6d1	uplynulé
letech	léto	k1gNnPc6	léto
vytěžilo	vytěžit	k5eAaPmAgNnS	vytěžit
až	až	k9	až
200	[number]	k4	200
milionů	milion	k4xCgInPc2	milion
tun	tuna	k1gFnPc2	tuna
uhlí	uhlí	k1gNnSc1	uhlí
a	a	k8xC	a
vyrobilo	vyrobit	k5eAaPmAgNnS	vyrobit
se	se	k3xPyFc4	se
50	[number]	k4	50
milionů	milion	k4xCgInPc2	milion
tun	tuna	k1gFnPc2	tuna
oceli	ocel	k1gFnSc2	ocel
<g/>
.	.	kIx.	.
</s>
<s>
Významný	významný	k2eAgMnSc1d1	významný
je	být	k5eAaImIp3nS	být
dále	daleko	k6eAd2	daleko
zejména	zejména	k9	zejména
strojírenský	strojírenský	k2eAgInSc4d1	strojírenský
<g/>
,	,	kIx,	,
chemický	chemický	k2eAgInSc4d1	chemický
a	a	k8xC	a
potravinářský	potravinářský	k2eAgInSc4d1	potravinářský
průmysl	průmysl	k1gInSc4	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
Donbasu	Donbas	k1gInSc2	Donbas
jsou	být	k5eAaImIp3nP	být
velkými	velký	k2eAgMnPc7d1	velký
centry	centr	k1gMnPc7	centr
těžkého	těžký	k2eAgInSc2d1	těžký
průmyslu	průmysl	k1gInSc2	průmysl
města	město	k1gNnSc2	město
Dněpropetrovsk	Dněpropetrovsk	k1gInSc1	Dněpropetrovsk
<g/>
,	,	kIx,	,
Záporoží	Záporoží	k1gNnSc1	Záporoží
či	či	k8xC	či
Mariupol	Mariupol	k1gInSc1	Mariupol
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
města	město	k1gNnSc2	město
Kryvyj	Kryvyj	k1gFnSc4	Kryvyj
Rih	Rih	k1gFnPc2	Rih
jsou	být	k5eAaImIp3nP	být
bohatá	bohatý	k2eAgNnPc4d1	bohaté
naleziště	naleziště	k1gNnPc4	naleziště
železné	železný	k2eAgFnSc2d1	železná
rudy	ruda	k1gFnSc2	ruda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
v	v	k7c6	v
místních	místní	k2eAgFnPc6d1	místní
slévárnách	slévárna	k1gFnPc6	slévárna
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
nerostné	nerostný	k2eAgFnPc4d1	nerostná
suroviny	surovina	k1gFnPc4	surovina
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
patří	patřit	k5eAaImIp3nP	patřit
uranové	uranový	k2eAgFnPc1d1	uranová
rudy	ruda	k1gFnPc1	ruda
<g/>
,	,	kIx,	,
magnetické	magnetický	k2eAgFnPc1d1	magnetická
rudy	ruda	k1gFnPc1	ruda
a	a	k8xC	a
titan	titan	k1gInSc1	titan
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
chrom	chrom	k1gInSc1	chrom
<g/>
,	,	kIx,	,
nikl	nikl	k1gInSc1	nikl
<g/>
,	,	kIx,	,
bauxit	bauxit	k1gInSc1	bauxit
<g/>
,	,	kIx,	,
fosfáty	fosfát	k1gInPc1	fosfát
<g/>
,	,	kIx,	,
síra	síra	k1gFnSc1	síra
<g/>
,	,	kIx,	,
rašelina	rašelina	k1gFnSc1	rašelina
<g/>
,	,	kIx,	,
ropa	ropa	k1gFnSc1	ropa
<g/>
,	,	kIx,	,
sůl	sůl	k1gFnSc1	sůl
<g/>
,	,	kIx,	,
grafit	grafit	k1gInSc1	grafit
a	a	k8xC	a
mangan	mangan	k1gInSc1	mangan
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
území	území	k1gNnSc2	území
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
disponuje	disponovat	k5eAaBmIp3nS	disponovat
kvalitní	kvalitní	k2eAgFnSc7d1	kvalitní
a	a	k8xC	a
úrodnou	úrodný	k2eAgFnSc7d1	úrodná
černozemní	černozemní	k2eAgFnSc7d1	černozemní
půdou	půda	k1gFnSc7	půda
<g/>
.	.	kIx.	.
</s>
<s>
Obděláváno	obděláván	k2eAgNnSc1d1	obděláváno
je	být	k5eAaImIp3nS	být
57	[number]	k4	57
%	%	kIx~	%
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgInPc4d1	hlavní
zemědělské	zemědělský	k2eAgInPc4d1	zemědělský
produkty	produkt	k1gInPc4	produkt
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
pšenice	pšenice	k1gFnSc1	pšenice
<g/>
,	,	kIx,	,
ječmen	ječmen	k1gInSc1	ječmen
<g/>
,	,	kIx,	,
cukrová	cukrový	k2eAgFnSc1d1	cukrová
řepa	řepa	k1gFnSc1	řepa
<g/>
,	,	kIx,	,
brambory	brambora	k1gFnPc1	brambora
<g/>
,	,	kIx,	,
kukuřice	kukuřice	k1gFnSc1	kukuřice
<g/>
,	,	kIx,	,
slunečnice	slunečnice	k1gFnSc1	slunečnice
<g/>
,	,	kIx,	,
maso	maso	k1gNnSc1	maso
a	a	k8xC	a
mléčné	mléčný	k2eAgInPc1d1	mléčný
výrobky	výrobek	k1gInPc1	výrobek
<g/>
.	.	kIx.	.
</s>
<s>
Chová	chovat	k5eAaImIp3nS	chovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
hlavně	hlavně	k6eAd1	hlavně
skot	skot	k1gInSc1	skot
a	a	k8xC	a
prasata	prase	k1gNnPc1	prase
<g/>
,	,	kIx,	,
důležitý	důležitý	k2eAgInSc1d1	důležitý
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
rybolov	rybolov	k1gInSc1	rybolov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jižních	jižní	k2eAgFnPc6d1	jižní
oblastech	oblast	k1gFnPc6	oblast
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
na	na	k7c6	na
Krymu	Krym	k1gInSc6	Krym
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rozšířeno	rozšířit	k5eAaPmNgNnS	rozšířit
také	také	k9	také
vinařství	vinařství	k1gNnSc1	vinařství
<g/>
.	.	kIx.	.
</s>
<s>
Karpaty	Karpaty	k1gInPc1	Karpaty
jsou	být	k5eAaImIp3nP	být
důležitou	důležitý	k2eAgFnSc7d1	důležitá
zásobárnou	zásobárna	k1gFnSc7	zásobárna
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
venkovského	venkovský	k2eAgNnSc2d1	venkovské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
je	být	k5eAaImIp3nS	být
drobné	drobný	k2eAgNnSc1d1	drobné
zemědělství	zemědělství	k1gNnSc1	zemědělství
vedlejším	vedlejší	k2eAgInSc7d1	vedlejší
zdrojem	zdroj	k1gInSc7	zdroj
příjmů	příjem	k1gInPc2	příjem
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Doprava	doprava	k1gFnSc1	doprava
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Rozloha	rozloha	k1gFnSc1	rozloha
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
poloha	poloha	k1gFnSc1	poloha
mezi	mezi	k7c7	mezi
západní	západní	k2eAgFnSc7d1	západní
Evropou	Evropa	k1gFnSc7	Evropa
<g/>
,	,	kIx,	,
Ruskem	Rusko	k1gNnSc7	Rusko
a	a	k8xC	a
Černým	černý	k2eAgNnSc7d1	černé
mořem	moře	k1gNnSc7	moře
klade	klást	k5eAaImIp3nS	klást
velké	velký	k2eAgInPc4d1	velký
nároky	nárok	k1gInPc4	nárok
na	na	k7c4	na
dopravní	dopravní	k2eAgFnSc4d1	dopravní
infrastrukturu	infrastruktura	k1gFnSc4	infrastruktura
<g/>
.	.	kIx.	.
</s>
<s>
Rozsáhlé	rozsáhlý	k2eAgFnPc1d1	rozsáhlá
investice	investice	k1gFnPc1	investice
do	do	k7c2	do
dopravních	dopravní	k2eAgFnPc2d1	dopravní
sítí	síť	k1gFnPc2	síť
<g/>
,	,	kIx,	,
plánované	plánovaný	k2eAgFnSc2d1	plánovaná
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
pořádáním	pořádání	k1gNnSc7	pořádání
šampionátu	šampionát	k1gInSc2	šampionát
EURO	euro	k1gNnSc4	euro
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
dostávají	dostávat	k5eAaImIp3nP	dostávat
do	do	k7c2	do
skluzu	skluz	k1gInSc2	skluz
<g/>
.	.	kIx.	.
</s>
<s>
Relativně	relativně	k6eAd1	relativně
hustá	hustý	k2eAgFnSc1d1	hustá
a	a	k8xC	a
kapacitní	kapacitní	k2eAgFnSc1d1	kapacitní
železniční	železniční	k2eAgFnSc1d1	železniční
síť	síť	k1gFnSc1	síť
(	(	kIx(	(
<g/>
o	o	k7c6	o
širokém	široký	k2eAgInSc6d1	široký
rozchodu	rozchod	k1gInSc6	rozchod
1524	[number]	k4	1524
mm	mm	kA	mm
<g/>
)	)	kIx)	)
čítá	čítat	k5eAaImIp3nS	čítat
23	[number]	k4	23
300	[number]	k4	300
km	km	kA	km
tratí	trať	k1gFnPc2	trať
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
necelá	celý	k2eNgFnSc1d1	necelá
polovina	polovina	k1gFnSc1	polovina
je	být	k5eAaImIp3nS	být
elektrifikována	elektrifikován	k2eAgFnSc1d1	elektrifikována
<g/>
.	.	kIx.	.
</s>
<s>
Nejvytíženější	vytížený	k2eAgFnSc1d3	nejvytíženější
a	a	k8xC	a
nejlépe	dobře	k6eAd3	dobře
udržované	udržovaný	k2eAgFnPc1d1	udržovaná
tratě	trať	k1gFnPc1	trať
vedou	vést	k5eAaImIp3nP	vést
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
Polsko	Polsko	k1gNnSc4	Polsko
–	–	k?	–
Lvov	Lvov	k1gInSc1	Lvov
–	–	k?	–
Šepetivka	Šepetivka	k1gFnSc1	Šepetivka
–	–	k?	–
Fastiv	Fastiv	k1gInSc1	Fastiv
<g/>
/	/	kIx~	/
<g/>
Kyjev	Kyjev	k1gInSc1	Kyjev
–	–	k?	–
Dněpropetrovsk	Dněpropetrovsk	k1gInSc1	Dněpropetrovsk
–	–	k?	–
Doněck	Doněck	k1gInSc1	Doněck
–	–	k?	–
Rusko	Rusko	k1gNnSc1	Rusko
a	a	k8xC	a
Moskva	Moskva	k1gFnSc1	Moskva
–	–	k?	–
Charkov	Charkov	k1gInSc1	Charkov
–	–	k?	–
Záporoží	Záporoží	k1gNnSc2	Záporoží
–	–	k?	–
Krym	Krym	k1gInSc1	Krym
<g/>
.	.	kIx.	.
</s>
<s>
Monopolní	monopolní	k2eAgMnSc1d1	monopolní
dopravce	dopravce	k1gMnSc1	dopravce
Ukrzaliznycja	Ukrzaliznycja	k1gMnSc1	Ukrzaliznycja
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
375	[number]	k4	375
000	[number]	k4	000
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
podniků	podnik	k1gInPc2	podnik
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
a	a	k8xC	a
únoru	únor	k1gInSc6	únor
2017	[number]	k4	2017
probíhá	probíhat	k5eAaImIp3nS	probíhat
na	na	k7c6	na
území	území	k1gNnSc6	území
kontrolovaném	kontrolovaný	k2eAgNnSc6d1	kontrolované
ukrajinskou	ukrajinský	k2eAgFnSc7d1	ukrajinská
vládou	vláda	k1gFnSc7	vláda
blokáda	blokáda	k1gFnSc1	blokáda
železničních	železniční	k2eAgInPc2d1	železniční
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
i	i	k9	i
silničních	silniční	k2eAgNnPc2d1	silniční
spojení	spojení	k1gNnPc2	spojení
se	s	k7c7	s
separatistickými	separatistický	k2eAgFnPc7d1	separatistická
republikami	republika	k1gFnPc7	republika
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
nacionalistických	nacionalistický	k2eAgMnPc2d1	nacionalistický
demonstrantů	demonstrant	k1gMnPc2	demonstrant
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
účastníků	účastník	k1gMnPc2	účastník
tzv.	tzv.	kA	tzv.
protiteroristické	protiteroristický	k2eAgFnSc2d1	protiteroristická
operace	operace	k1gFnSc2	operace
(	(	kIx(	(
<g/>
ATO	ATO	kA	ATO
<g/>
)	)	kIx)	)
a	a	k8xC	a
několika	několik	k4yIc7	několik
poslanců	poslanec	k1gMnPc2	poslanec
parlamentu	parlament	k1gInSc2	parlament
je	být	k5eAaImIp3nS	být
ochromení	ochromení	k1gNnSc2	ochromení
dodávek	dodávka	k1gFnPc2	dodávka
černého	černý	k2eAgNnSc2d1	černé
uhlí	uhlí	k1gNnSc2	uhlí
z	z	k7c2	z
Donbasu	Donbas	k1gInSc2	Donbas
do	do	k7c2	do
ukrajinských	ukrajinský	k2eAgFnPc2d1	ukrajinská
elektráren	elektrárna	k1gFnPc2	elektrárna
a	a	k8xC	a
koksáren	koksárna	k1gFnPc2	koksárna
<g/>
.	.	kIx.	.
</s>
<s>
Protestují	protestovat	k5eAaBmIp3nP	protestovat
tím	ten	k3xDgNnSc7	ten
proti	proti	k7c3	proti
vysokým	vysoký	k2eAgFnPc3d1	vysoká
cenám	cena	k1gFnPc3	cena
elektřiny	elektřina	k1gFnSc2	elektřina
a	a	k8xC	a
proti	proti	k7c3	proti
korupci	korupce	k1gFnSc3	korupce
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
však	však	k9	však
způsobili	způsobit	k5eAaPmAgMnP	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
dostala	dostat	k5eAaPmAgFnS	dostat
na	na	k7c4	na
pokraj	pokraj	k1gInSc4	pokraj
energetického	energetický	k2eAgInSc2d1	energetický
kolapsu	kolaps	k1gInSc2	kolaps
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
mnoho	mnoho	k4c1	mnoho
elektráren	elektrárna	k1gFnPc2	elektrárna
pracuje	pracovat	k5eAaImIp3nS	pracovat
výhradně	výhradně	k6eAd1	výhradně
s	s	k7c7	s
vysoce	vysoce	k6eAd1	vysoce
hodnotným	hodnotný	k2eAgInSc7d1	hodnotný
antracitem	antracit	k1gInSc7	antracit
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
netěží	těžet	k5eNaImIp3nS	těžet
nikde	nikde	k6eAd1	nikde
jinde	jinde	k6eAd1	jinde
než	než	k8xS	než
na	na	k7c6	na
území	území	k1gNnSc6	území
separatistických	separatistický	k2eAgFnPc2d1	separatistická
republik	republika	k1gFnPc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Zásoby	zásoba	k1gFnPc1	zásoba
antracitu	antracit	k1gInSc2	antracit
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgFnP	moct
stačit	stačit	k5eAaBmF	stačit
již	již	k6eAd1	již
jen	jen	k9	jen
na	na	k7c4	na
40	[number]	k4	40
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
Volodymyr	Volodymyr	k1gMnSc1	Volodymyr
Hrojsman	Hrojsman	k1gMnSc1	Hrojsman
proto	proto	k8xC	proto
požádal	požádat	k5eAaPmAgMnS	požádat
demonstranty	demonstrant	k1gMnPc4	demonstrant
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
to	ten	k3xDgNnSc4	ten
"	"	kIx"	"
<g/>
s	s	k7c7	s
blokádou	blokáda	k1gFnSc7	blokáda
nepřeháněli	přehánět	k5eNaImAgMnP	přehánět
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
jsou	být	k5eAaImIp3nP	být
pouze	pouze	k6eAd1	pouze
dvě	dva	k4xCgFnPc4	dva
standardní	standardní	k2eAgFnPc4d1	standardní
dálnice	dálnice	k1gFnPc4	dálnice
<g/>
;	;	kIx,	;
175	[number]	k4	175
km	km	kA	km
úsek	úsek	k1gInSc1	úsek
z	z	k7c2	z
Charkova	Charkov	k1gInSc2	Charkov
do	do	k7c2	do
Dněpropetrovsku	Dněpropetrovsko	k1gNnSc6	Dněpropetrovsko
a	a	k8xC	a
18	[number]	k4	18
km	km	kA	km
část	část	k1gFnSc1	část
M03	M03	k1gFnSc1	M03
z	z	k7c2	z
Kyjeva	Kyjev	k1gInSc2	Kyjev
do	do	k7c2	do
Boryspil	Boryspil	k1gFnSc2	Boryspil
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Kyjeva	Kyjev	k1gInSc2	Kyjev
do	do	k7c2	do
Lvova	Lvov	k1gInSc2	Lvov
a	a	k8xC	a
Oděsy	Oděsa	k1gFnSc2	Oděsa
vedou	vést	k5eAaImIp3nP	vést
expresní	expresní	k2eAgFnPc1d1	expresní
silnice	silnice	k1gFnPc1	silnice
<g/>
.	.	kIx.	.
</s>
<s>
Individuální	individuální	k2eAgInSc1d1	individuální
automobilismus	automobilismus	k1gInSc1	automobilismus
širokých	široký	k2eAgFnPc2d1	široká
vrstev	vrstva	k1gFnPc2	vrstva
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
se	se	k3xPyFc4	se
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
ještě	ještě	k6eAd1	ještě
nerozmohl	rozmoct	k5eNaPmAgMnS	rozmoct
tak	tak	k6eAd1	tak
silně	silně	k6eAd1	silně
jako	jako	k8xS	jako
v	v	k7c6	v
západních	západní	k2eAgFnPc6d1	západní
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
Ukrajinu	Ukrajina	k1gFnSc4	Ukrajina
vedou	vést	k5eAaImIp3nP	vést
důležité	důležitý	k2eAgInPc1d1	důležitý
převozní	převozní	k2eAgInPc1d1	převozní
ropovody	ropovod	k1gInPc1	ropovod
a	a	k8xC	a
plynovody	plynovod	k1gInPc1	plynovod
z	z	k7c2	z
Ruska	Rusko	k1gNnSc2	Rusko
do	do	k7c2	do
střední	střední	k2eAgFnSc2d1	střední
a	a	k8xC	a
západní	západní	k2eAgFnSc2d1	západní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ji	on	k3xPp3gFnSc4	on
staví	stavit	k5eAaPmIp3nS	stavit
do	do	k7c2	do
pozice	pozice	k1gFnSc2	pozice
klíčového	klíčový	k2eAgMnSc4d1	klíčový
hráče	hráč	k1gMnSc4	hráč
zejména	zejména	k9	zejména
ve	v	k7c6	v
věci	věc	k1gFnSc6	věc
tranzitu	tranzit	k1gInSc2	tranzit
ruského	ruský	k2eAgInSc2d1	ruský
plynu	plyn	k1gInSc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Hojně	hojně	k6eAd1	hojně
využívána	využíván	k2eAgFnSc1d1	využívána
je	být	k5eAaImIp3nS	být
městská	městský	k2eAgFnSc1d1	městská
hromadná	hromadný	k2eAgFnSc1d1	hromadná
doprava	doprava	k1gFnSc1	doprava
(	(	kIx(	(
<g/>
MHD	MHD	kA	MHD
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
tři	tři	k4xCgInPc1	tři
fungující	fungující	k2eAgInPc1d1	fungující
systémy	systém	k1gInPc1	systém
metra	metro	k1gNnSc2	metro
v	v	k7c6	v
Kyjevě	Kyjev	k1gInSc6	Kyjev
<g/>
,	,	kIx,	,
Charkově	Charkov	k1gInSc6	Charkov
a	a	k8xC	a
Dněpropetrovsku	Dněpropetrovsko	k1gNnSc6	Dněpropetrovsko
<g/>
.	.	kIx.	.
</s>
<s>
Desítky	desítka	k1gFnPc1	desítka
měst	město	k1gNnPc2	město
mají	mít	k5eAaImIp3nP	mít
provozy	provoz	k1gInPc1	provoz
tramvajové	tramvajový	k2eAgFnSc2d1	tramvajová
či	či	k8xC	či
trolejbusové	trolejbusový	k2eAgFnSc2d1	trolejbusová
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Krymu	Krym	k1gInSc6	Krym
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
dokonce	dokonce	k9	dokonce
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
trolejbusová	trolejbusový	k2eAgFnSc1d1	trolejbusová
trať	trať	k1gFnSc1	trať
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
několika	několik	k4yIc6	několik
ukrajinských	ukrajinský	k2eAgNnPc6d1	ukrajinské
městech	město	k1gNnPc6	město
jsou	být	k5eAaImIp3nP	být
používány	používán	k2eAgFnPc1d1	používána
tramvaje	tramvaj	k1gFnPc1	tramvaj
československé	československý	k2eAgFnSc2d1	Československá
výroby	výroba	k1gFnSc2	výroba
značky	značka	k1gFnSc2	značka
Tatra	Tatra	k1gFnSc1	Tatra
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
nedostatku	nedostatek	k1gInSc3	nedostatek
financí	finance	k1gFnPc2	finance
po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
se	se	k3xPyFc4	se
prudce	prudko	k6eAd1	prudko
zpomalil	zpomalit	k5eAaPmAgInS	zpomalit
rozvoj	rozvoj	k1gInSc1	rozvoj
klasických	klasický	k2eAgFnPc2d1	klasická
sítí	síť	k1gFnPc2	síť
MHD	MHD	kA	MHD
<g/>
;	;	kIx,	;
např.	např.	kA	např.
bylo	být	k5eAaImAgNnS	být
neustále	neustále	k6eAd1	neustále
odkládáno	odkládán	k2eAgNnSc1d1	odkládáno
zahájení	zahájení	k1gNnSc1	zahájení
provozu	provoz	k1gInSc2	provoz
metra	metro	k1gNnSc2	metro
v	v	k7c6	v
Doněcku	Doněck	k1gInSc6	Doněck
<g/>
.	.	kIx.	.
</s>
<s>
Ukrajinská	ukrajinský	k2eAgNnPc4d1	ukrajinské
města	město	k1gNnPc4	město
zaplnily	zaplnit	k5eAaPmAgFnP	zaplnit
tzv.	tzv.	kA	tzv.
maršrutky	maršrutek	k1gInPc7	maršrutek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
velkých	velký	k2eAgNnPc6d1	velké
městech	město	k1gNnPc6	město
fungují	fungovat	k5eAaImIp3nP	fungovat
mezinárodní	mezinárodní	k2eAgNnPc4d1	mezinárodní
letiště	letiště	k1gNnPc4	letiště
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgNnSc1d3	nejvýznamnější
je	být	k5eAaImIp3nS	být
kyjevské	kyjevský	k2eAgNnSc1d1	Kyjevské
Mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
letiště	letiště	k1gNnSc1	letiště
Boryspil	Boryspil	k1gFnSc2	Boryspil
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
existují	existovat	k5eAaImIp3nP	existovat
letiště	letiště	k1gNnSc4	letiště
ve	v	k7c6	v
městech	město	k1gNnPc6	město
Oděsa	Oděsa	k1gFnSc1	Oděsa
<g/>
,	,	kIx,	,
Lvov	Lvov	k1gInSc1	Lvov
<g/>
,	,	kIx,	,
Dněpropetrovsk	Dněpropetrovsk	k1gInSc1	Dněpropetrovsk
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
novým	nový	k2eAgInSc7d1	nový
názvem	název	k1gInSc7	název
Dnipro	Dnipro	k1gNnSc4	Dnipro
<g/>
)	)	kIx)	)
a	a	k8xC	a
Charkov	Charkov	k1gInSc4	Charkov
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
letiště	letiště	k1gNnSc1	letiště
Doněck	Doněck	k1gInSc1	Doněck
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
dokončeno	dokončit	k5eAaPmNgNnS	dokončit
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
mistrovstvím	mistrovství	k1gNnSc7	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Donbasu	Donbas	k1gInSc6	Donbas
zcela	zcela	k6eAd1	zcela
zničeno	zničen	k2eAgNnSc1d1	zničeno
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgFnSc7d1	národní
leteckou	letecký	k2eAgFnSc7d1	letecká
společností	společnost	k1gFnSc7	společnost
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
jsou	být	k5eAaImIp3nP	být
Ukraine	Ukrain	k1gInSc5	Ukrain
International	International	k1gMnSc4	International
Airlines	Airlines	k1gInSc4	Airlines
<g/>
.	.	kIx.	.
</s>
<s>
Důležité	důležitý	k2eAgInPc1d1	důležitý
námořní	námořní	k2eAgInPc1d1	námořní
přístavy	přístav	k1gInPc1	přístav
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
Oděse	Oděsa	k1gFnSc6	Oděsa
<g/>
,	,	kIx,	,
Sevastopolu	Sevastopol	k1gInSc6	Sevastopol
<g/>
,	,	kIx,	,
Kerči	Kerč	k1gInSc6	Kerč
a	a	k8xC	a
Mariupolu	Mariupol	k1gInSc6	Mariupol
<g/>
.	.	kIx.	.
</s>
<s>
Lodní	lodní	k2eAgFnSc1d1	lodní
doprava	doprava	k1gFnSc1	doprava
je	být	k5eAaImIp3nS	být
provozována	provozovat	k5eAaImNgFnS	provozovat
také	také	k9	také
na	na	k7c6	na
řekách	řeka	k1gFnPc6	řeka
Dněpru	Dněpr	k1gInSc2	Dněpr
a	a	k8xC	a
Dunaji	Dunaj	k1gInSc3	Dunaj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
říční	říční	k2eAgFnSc1d1	říční
lodní	lodní	k2eAgFnSc1d1	lodní
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
útlumu	útlum	k1gInSc6	útlum
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
činnosti	činnost	k1gFnSc2	činnost
značně	značně	k6eAd1	značně
omezena	omezit	k5eAaPmNgFnS	omezit
<g/>
.	.	kIx.	.
</s>
<s>
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
je	být	k5eAaImIp3nS	být
hojně	hojně	k6eAd1	hojně
navštěvovanou	navštěvovaný	k2eAgFnSc7d1	navštěvovaná
zemí	zem	k1gFnSc7	zem
<g/>
:	:	kIx,	:
ročně	ročně	k6eAd1	ročně
ji	on	k3xPp3gFnSc4	on
navštíví	navštívit	k5eAaPmIp3nS	navštívit
přes	přes	k7c4	přes
16	[number]	k4	16
miliónů	milión	k4xCgInPc2	milión
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
občanů	občan	k1gMnPc2	občan
Ruské	ruský	k2eAgFnSc2d1	ruská
federace	federace	k1gFnSc2	federace
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
se	se	k3xPyFc4	se
umístila	umístit	k5eAaPmAgFnS	umístit
na	na	k7c4	na
8	[number]	k4	8
<g/>
.	.	kIx.	.
příčce	příčka	k1gFnSc6	příčka
nejnavštěvovanějších	navštěvovaný	k2eAgFnPc2d3	nejnavštěvovanější
zemí	zem	k1gFnPc2	zem
Evropy	Evropa	k1gFnSc2	Evropa
podle	podle	k7c2	podle
Světové	světový	k2eAgFnSc2d1	světová
turistické	turistický	k2eAgFnSc2d1	turistická
organizace	organizace	k1gFnSc2	organizace
(	(	kIx(	(
<g/>
WTO	WTO	kA	WTO
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
po	po	k7c6	po
zrušení	zrušení	k1gNnSc6	zrušení
vízové	vízový	k2eAgFnSc2d1	vízová
povinnosti	povinnost	k1gFnSc2	povinnost
<g/>
)	)	kIx)	)
však	však	k9	však
stoupá	stoupat	k5eAaImIp3nS	stoupat
i	i	k9	i
zájem	zájem	k1gInSc1	zájem
u	u	k7c2	u
turistů	turist	k1gMnPc2	turist
z	z	k7c2	z
EU	EU	kA	EU
a	a	k8xC	a
USA	USA	kA	USA
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
sem	sem	k6eAd1	sem
přijíždějí	přijíždět	k5eAaImIp3nP	přijíždět
spíše	spíše	k9	spíše
na	na	k7c4	na
"	"	kIx"	"
<g/>
dobrodružnou	dobrodružný	k2eAgFnSc4d1	dobrodružná
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
aktivní	aktivní	k2eAgFnSc4d1	aktivní
dovolenou	dovolená	k1gFnSc4	dovolená
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
pro	pro	k7c4	pro
Rusy	Rus	k1gMnPc4	Rus
je	být	k5eAaImIp3nS	být
zejména	zejména	k9	zejména
Krym	Krym	k1gInSc1	Krym
dlouho	dlouho	k6eAd1	dlouho
zavedenou	zavedený	k2eAgFnSc7d1	zavedená
odpočinkovou	odpočinkový	k2eAgFnSc7d1	odpočinková
destinací	destinace	k1gFnSc7	destinace
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
jezdí	jezdit	k5eAaImIp3nP	jezdit
dlouhé	dlouhý	k2eAgInPc1d1	dlouhý
vlaky	vlak	k1gInPc1	vlak
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
koutů	kout	k1gInPc2	kout
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
je	být	k5eAaImIp3nS	být
navštěvováno	navštěvovat	k5eAaImNgNnS	navštěvovat
velmi	velmi	k6eAd1	velmi
nerovnoměrně	rovnoměrně	k6eNd1	rovnoměrně
<g/>
:	:	kIx,	:
nejoblíbenější	oblíbený	k2eAgNnSc1d3	nejoblíbenější
je	být	k5eAaImIp3nS	být
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
Krym	Krym	k1gInSc1	Krym
s	s	k7c7	s
letovisky	letovisko	k1gNnPc7	letovisko
jako	jako	k8xS	jako
Jalta	Jalta	k1gFnSc1	Jalta
<g/>
,	,	kIx,	,
Sevastopol	Sevastopol	k1gInSc1	Sevastopol
či	či	k8xC	či
Sudak	Sudak	k1gInSc1	Sudak
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
zavedenou	zavedený	k2eAgFnSc7d1	zavedená
oblastí	oblast	k1gFnSc7	oblast
jsou	být	k5eAaImIp3nP	být
Karpaty	Karpaty	k1gInPc4	Karpaty
<g/>
;	;	kIx,	;
hory	hora	k1gFnPc4	hora
Zakarpatské	zakarpatský	k2eAgFnSc2d1	Zakarpatská
oblasti	oblast	k1gFnSc2	oblast
se	se	k3xPyFc4	se
i	i	k9	i
mezi	mezi	k7c7	mezi
českými	český	k2eAgMnPc7d1	český
turisty	turist	k1gMnPc7	turist
v	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
letech	let	k1gInPc6	let
stávají	stávat	k5eAaImIp3nP	stávat
téměř	téměř	k6eAd1	téměř
módou	móda	k1gFnSc7	móda
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
dalšími	další	k2eAgInPc7d1	další
cíli	cíl	k1gInPc7	cíl
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
jmenovat	jmenovat	k5eAaBmF	jmenovat
Kyjev	Kyjev	k1gInSc4	Kyjev
(	(	kIx(	(
<g/>
s	s	k7c7	s
výletními	výletní	k2eAgFnPc7d1	výletní
plavbami	plavba	k1gFnPc7	plavba
po	po	k7c6	po
Dněpru	Dněpr	k1gInSc6	Dněpr
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgNnPc4d1	další
města	město	k1gNnPc4	město
s	s	k7c7	s
historickými	historický	k2eAgFnPc7d1	historická
památkami	památka	k1gFnPc7	památka
<g/>
:	:	kIx,	:
Lvov	Lvov	k1gInSc1	Lvov
<g/>
,	,	kIx,	,
Černovice	Černovice	k1gFnSc1	Černovice
<g/>
,	,	kIx,	,
Kamenec	Kamenec	k1gInSc1	Kamenec
Podolský	podolský	k2eAgInSc1d1	podolský
<g/>
,	,	kIx,	,
Oděsa	Oděsa	k1gFnSc1	Oděsa
<g/>
,	,	kIx,	,
Poltava	Poltava	k1gFnSc1	Poltava
<g/>
,	,	kIx,	,
Černihiv	Černihiva	k1gFnPc2	Černihiva
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
"	"	kIx"	"
<g/>
běžné	běžný	k2eAgFnPc4d1	běžná
<g/>
"	"	kIx"	"
oblasti	oblast	k1gFnPc4	oblast
především	především	k6eAd1	především
východní	východní	k2eAgFnSc2d1	východní
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
nejsou	být	k5eNaImIp3nP	být
vyhledávány	vyhledáván	k2eAgFnPc1d1	vyhledávána
prakticky	prakticky	k6eAd1	prakticky
vůbec	vůbec	k9	vůbec
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ovšem	ovšem	k9	ovšem
neznamená	znamenat	k5eNaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
nezajímavé	zajímavý	k2eNgFnPc1d1	nezajímavá
<g/>
:	:	kIx,	:
například	například	k6eAd1	například
významné	významný	k2eAgNnSc1d1	významné
poutní	poutní	k2eAgNnSc1d1	poutní
místo	místo	k1gNnSc1	místo
Svjatohirsk	Svjatohirsk	k1gInSc1	Svjatohirsk
leží	ležet	k5eAaImIp3nS	ležet
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
průmyslovém	průmyslový	k2eAgInSc6d1	průmyslový
Donbasu	Donbas	k1gInSc6	Donbas
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
červenci	červenec	k1gInSc6	červenec
2007	[number]	k4	2007
měla	mít	k5eAaImAgFnS	mít
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
46	[number]	k4	46
490	[number]	k4	490
819	[number]	k4	819
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgNnPc2	jenž
68	[number]	k4	68
%	%	kIx~	%
žilo	žít	k5eAaImAgNnS	žít
ve	v	k7c6	v
městech	město	k1gNnPc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Tradičně	tradičně	k6eAd1	tradičně
byla	být	k5eAaImAgFnS	být
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
výrazně	výrazně	k6eAd1	výrazně
venkovskou	venkovský	k2eAgFnSc7d1	venkovská
zemí	zem	k1gFnSc7	zem
<g/>
;	;	kIx,	;
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1900	[number]	k4	1900
žilo	žít	k5eAaImAgNnS	žít
ve	v	k7c6	v
městech	město	k1gNnPc6	město
jenom	jenom	k9	jenom
14,3	[number]	k4	14,3
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
však	však	k9	však
počet	počet	k1gInSc4	počet
a	a	k8xC	a
podíl	podíl	k1gInSc4	podíl
městského	městský	k2eAgNnSc2d1	Městské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
výrazně	výrazně	k6eAd1	výrazně
vzrostly	vzrůst	k5eAaPmAgFnP	vzrůst
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
východní	východní	k2eAgInPc1d1	východní
průmyslové	průmyslový	k2eAgInPc1d1	průmyslový
regiony	region	k1gInPc1	region
se	se	k3xPyFc4	se
zalidnily	zalidnit	k5eAaPmAgInP	zalidnit
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
venkovské	venkovský	k2eAgFnPc1d1	venkovská
oblasti	oblast	k1gFnPc1	oblast
především	především	k9	především
severní	severní	k2eAgFnSc2d1	severní
a	a	k8xC	a
střední	střední	k2eAgFnSc2d1	střední
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
(	(	kIx(	(
<g/>
nejznatelněji	znatelně	k6eAd3	znatelně
Černihivská	Černihivský	k2eAgFnSc1d1	Černihivský
oblast	oblast	k1gFnSc1	oblast
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
dnes	dnes	k6eAd1	dnes
méně	málo	k6eAd2	málo
obyvatel	obyvatel	k1gMnPc2	obyvatel
než	než	k8xS	než
před	před	k7c7	před
druhou	druhý	k4xOgFnSc7	druhý
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
20	[number]	k4	20
letech	let	k1gInPc6	let
počet	počet	k1gInSc4	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1	oficiální
údaj	údaj	k1gInSc1	údaj
o	o	k7c6	o
počtu	počet	k1gInSc6	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
2014	[number]	k4	2014
byl	být	k5eAaImAgInS	být
42	[number]	k4	42
977	[number]	k4	977
367	[number]	k4	367
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
bez	bez	k1gInSc4	bez
AR	ar	k1gInSc1	ar
Krym	Krym	k1gInSc1	Krym
a	a	k8xC	a
města	město	k1gNnSc2	město
Sevastopol	Sevastopol	k1gInSc1	Sevastopol
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
francouzského	francouzský	k2eAgInSc2d1	francouzský
časopisu	časopis	k1gInSc2	časopis
Le	Le	k1gFnSc2	Le
Monde	Mond	k1gMnSc5	Mond
diplomatique	diplomatiqu	k1gFnPc4	diplomatiqu
ztratila	ztratit	k5eAaPmAgFnS	ztratit
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
zhruba	zhruba	k6eAd1	zhruba
7	[number]	k4	7
milionů	milion	k4xCgInPc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
příčin	příčina	k1gFnPc2	příčina
poklesu	pokles	k1gInSc2	pokles
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
je	být	k5eAaImIp3nS	být
–	–	k?	–
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
evropských	evropský	k2eAgFnPc2d1	Evropská
zemí	zem	k1gFnPc2	zem
–	–	k?	–
nízká	nízký	k2eAgFnSc1d1	nízká
porodnost	porodnost	k1gFnSc1	porodnost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
však	však	k9	však
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
přistupuje	přistupovat	k5eAaImIp3nS	přistupovat
také	také	k9	také
vysoká	vysoký	k2eAgFnSc1d1	vysoká
úmrtnost	úmrtnost	k1gFnSc1	úmrtnost
a	a	k8xC	a
značně	značně	k6eAd1	značně
negativní	negativní	k2eAgNnSc4d1	negativní
migrační	migrační	k2eAgNnSc4d1	migrační
saldo	saldo	k1gNnSc4	saldo
<g/>
.	.	kIx.	.
</s>
<s>
Očekávaná	očekávaný	k2eAgFnSc1d1	očekávaná
délka	délka	k1gFnSc1	délka
života	život	k1gInSc2	život
je	být	k5eAaImIp3nS	být
67,9	[number]	k4	67,9
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
62,2	[number]	k4	62,2
let	léto	k1gNnPc2	léto
u	u	k7c2	u
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
74	[number]	k4	74
let	léto	k1gNnPc2	léto
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
mj.	mj.	kA	mj.
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Evropy	Evropa	k1gFnSc2	Evropa
poměrně	poměrně	k6eAd1	poměrně
vysoký	vysoký	k2eAgInSc1d1	vysoký
podíl	podíl	k1gInSc1	podíl
lidí	člověk	k1gMnPc2	člověk
nakažených	nakažený	k2eAgMnPc2d1	nakažený
virem	vir	k1gInSc7	vir
HIV	HIV	kA	HIV
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
1,4	[number]	k4	1,4
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
ukrajinské	ukrajinský	k2eAgFnSc3d1	ukrajinská
národnosti	národnost	k1gFnSc3	národnost
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nS	hlásit
77,8	[number]	k4	77,8
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
kromě	kromě	k7c2	kromě
Krymu	Krym	k1gInSc2	Krym
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žijí	žít	k5eAaImIp3nP	žít
především	především	k9	především
Rusové	Rus	k1gMnPc1	Rus
(	(	kIx(	(
<g/>
58,5	[number]	k4	58,5
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nP	tvořit
Ukrajinci	Ukrajinec	k1gMnPc1	Ukrajinec
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
oblastech	oblast	k1gFnPc6	oblast
nadpoloviční	nadpoloviční	k2eAgFnSc4d1	nadpoloviční
většinu	většina	k1gFnSc4	většina
<g/>
;	;	kIx,	;
v	v	k7c6	v
Haliči	Halič	k1gFnSc6	Halič
<g/>
,	,	kIx,	,
Volyni	Volyně	k1gFnSc6	Volyně
a	a	k8xC	a
Podolí	Podolí	k1gNnSc6	Podolí
pak	pak	k6eAd1	pak
jejich	jejich	k3xOp3gInSc4	jejich
podíl	podíl	k1gInSc4	podíl
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
90	[number]	k4	90
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgNnSc1d3	veliký
zastoupení	zastoupení	k1gNnSc1	zastoupení
Rusů	Rus	k1gMnPc2	Rus
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
oblast	oblast	k1gFnSc1	oblast
Luhanská	Luhanský	k2eAgFnSc1d1	Luhanská
(	(	kIx(	(
<g/>
39	[number]	k4	39
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Doněcká	doněcký	k2eAgFnSc1d1	Doněcká
(	(	kIx(	(
<g/>
38	[number]	k4	38
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
Charkovská	charkovský	k2eAgFnSc1d1	Charkovská
(	(	kIx(	(
<g/>
26	[number]	k4	26
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnSc4d3	nejvýznamnější
rumunskou	rumunský	k2eAgFnSc4d1	rumunská
menšinu	menšina	k1gFnSc4	menšina
má	mít	k5eAaImIp3nS	mít
Bukovina	Bukovina	k1gFnSc1	Bukovina
(	(	kIx(	(
<g/>
Černovická	Černovický	k2eAgFnSc1d1	Černovická
oblast	oblast	k1gFnSc1	oblast
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Etnicky	etnicky	k6eAd1	etnicky
nejrozrůzněnějším	rozrůzněný	k2eAgInSc7d3	rozrůzněný
krajem	kraj	k1gInSc7	kraj
je	být	k5eAaImIp3nS	být
Budžak	Budžak	k1gInSc1	Budžak
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
kromě	kromě	k7c2	kromě
Ukrajinců	Ukrajinec	k1gMnPc2	Ukrajinec
a	a	k8xC	a
Rusů	Rus	k1gMnPc2	Rus
žije	žít	k5eAaImIp3nS	žít
21	[number]	k4	21
<g/>
%	%	kIx~	%
menšina	menšina	k1gFnSc1	menšina
besarabských	besarabský	k2eAgMnPc2d1	besarabský
Bulharů	Bulhar	k1gMnPc2	Bulhar
a	a	k8xC	a
4	[number]	k4	4
<g/>
%	%	kIx~	%
menšina	menšina	k1gFnSc1	menšina
Gagauzů	Gagauz	k1gInPc2	Gagauz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Zakarpatské	zakarpatský	k2eAgFnSc6d1	Zakarpatská
oblasti	oblast	k1gFnSc6	oblast
žije	žít	k5eAaImIp3nS	žít
významná	významný	k2eAgFnSc1d1	významná
maďarská	maďarský	k2eAgFnSc1d1	maďarská
menšina	menšina	k1gFnSc1	menšina
<g/>
.	.	kIx.	.
</s>
<s>
Karpatští	karpatský	k2eAgMnPc1d1	karpatský
Rusíni	Rusín	k1gMnPc1	Rusín
(	(	kIx(	(
<g/>
popř.	popř.	kA	popř.
Lemkové	Lemek	k1gMnPc1	Lemek
<g/>
,	,	kIx,	,
Huculové	Hucul	k1gMnPc1	Hucul
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
etnické	etnický	k2eAgFnPc1d1	etnická
skupiny	skupina	k1gFnPc1	skupina
<g/>
)	)	kIx)	)
nebyli	být	k5eNaImAgMnP	být
při	při	k7c6	při
oficiálním	oficiální	k2eAgNnSc6d1	oficiální
sčítání	sčítání	k1gNnSc6	sčítání
počítáni	počítat	k5eAaImNgMnP	počítat
za	za	k7c4	za
samostatný	samostatný	k2eAgInSc4d1	samostatný
národ	národ	k1gInSc4	národ
a	a	k8xC	a
přihlásili	přihlásit	k5eAaPmAgMnP	přihlásit
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
většinou	většinou	k6eAd1	většinou
k	k	k7c3	k
ukrajinské	ukrajinský	k2eAgFnSc3d1	ukrajinská
národnosti	národnost	k1gFnSc3	národnost
<g/>
.	.	kIx.	.
</s>
<s>
Kdysi	kdysi	k6eAd1	kdysi
početná	početný	k2eAgFnSc1d1	početná
menšina	menšina	k1gFnSc1	menšina
Poláků	Polák	k1gMnPc2	Polák
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1931	[number]	k4	1931
63,5	[number]	k4	63,5
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
Lvova	Lvův	k2eAgFnSc1d1	Lvův
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
vládnoucí	vládnoucí	k2eAgFnSc1d1	vládnoucí
vrstva	vrstva	k1gFnSc1	vrstva
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
,	,	kIx,	,
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
během	během	k7c2	během
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
a	a	k8xC	a
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
volyňského	volyňský	k2eAgInSc2d1	volyňský
masakru	masakr	k1gInSc2	masakr
Poláků	Polák	k1gMnPc2	Polák
ukrajinskými	ukrajinský	k2eAgFnPc7d1	ukrajinská
nacionalisty	nacionalista	k1gMnPc4	nacionalista
z	z	k7c2	z
UPA	UPA	kA	UPA
a	a	k8xC	a
masovým	masový	k2eAgInSc7d1	masový
útěkem	útěk	k1gInSc7	útěk
Poláků	Polák	k1gMnPc2	Polák
z	z	k7c2	z
regionu	region	k1gInSc2	region
<g/>
,	,	kIx,	,
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
poválečné	poválečný	k2eAgFnSc2d1	poválečná
výměny	výměna	k1gFnSc2	výměna
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
mezi	mezi	k7c7	mezi
SSSR	SSSR	kA	SSSR
a	a	k8xC	a
Polskem	Polsko	k1gNnSc7	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
holokaustu	holokaust	k1gInSc2	holokaust
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
emigrace	emigrace	k1gFnSc1	emigrace
se	se	k3xPyFc4	se
snížila	snížit	k5eAaPmAgFnS	snížit
i	i	k9	i
původně	původně	k6eAd1	původně
početná	početný	k2eAgFnSc1d1	početná
menšina	menšina	k1gFnSc1	menšina
Židů	Žid	k1gMnPc2	Žid
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
tvořili	tvořit	k5eAaImAgMnP	tvořit
např.	např.	kA	např.
36,5	[number]	k4	36,5
<g/>
%	%	kIx~	%
obyvatel	obyvatel	k1gMnSc1	obyvatel
Oděsy	Oděsa	k1gFnSc2	Oděsa
a	a	k8xC	a
27,3	[number]	k4	27,3
<g/>
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
Kyjeva	Kyjev	k1gInSc2	Kyjev
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severozápadní	severozápadní	k2eAgFnSc6d1	severozápadní
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
dosud	dosud	k6eAd1	dosud
žije	žít	k5eAaImIp3nS	žít
několik	několik	k4yIc4	několik
set	set	k1gInSc4	set
volyňských	volyňský	k2eAgMnPc2d1	volyňský
Čechů	Čech	k1gMnPc2	Čech
<g/>
,	,	kIx,	,
několik	několik	k4yIc1	několik
vesnic	vesnice	k1gFnPc2	vesnice
s	s	k7c7	s
převážně	převážně	k6eAd1	převážně
českým	český	k2eAgNnSc7d1	české
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
je	být	k5eAaImIp3nS	být
i	i	k9	i
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
např.	např.	kA	např.
ve	v	k7c6	v
vesnicích	vesnice	k1gFnPc6	vesnice
Bohemka	Bohemka	k1gFnSc1	Bohemka
<g/>
,	,	kIx,	,
Lobanovo	Lobanův	k2eAgNnSc1d1	Lobanovo
a	a	k8xC	a
Veselynivka	Veselynivka	k1gFnSc1	Veselynivka
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
repatriací	repatriace	k1gFnSc7	repatriace
do	do	k7c2	do
Československa	Československo	k1gNnSc2	Československo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
žilo	žít	k5eAaImAgNnS	žít
na	na	k7c6	na
Volyni	Volyně	k1gFnSc6	Volyně
40	[number]	k4	40
000	[number]	k4	000
Čechů	Čech	k1gMnPc2	Čech
v	v	k7c6	v
647	[number]	k4	647
čistě	čistě	k6eAd1	čistě
českých	český	k2eAgMnPc2d1	český
i	i	k8xC	i
etnicky	etnicky	k6eAd1	etnicky
smíšených	smíšený	k2eAgFnPc6d1	smíšená
obcích	obec	k1gFnPc6	obec
<g/>
.	.	kIx.	.
10	[number]	k4	10
000	[number]	k4	000
volyňských	volyňský	k2eAgMnPc2d1	volyňský
Čechů	Čech	k1gMnPc2	Čech
sloužilo	sloužit	k5eAaImAgNnS	sloužit
během	během	k7c2	během
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
čs	čs	kA	čs
<g/>
.	.	kIx.	.
armádním	armádní	k2eAgInSc6d1	armádní
sboru	sbor	k1gInSc6	sbor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgInS	podílet
na	na	k7c4	na
osvobození	osvobození	k1gNnSc4	osvobození
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Výsledky	výsledek	k1gInPc1	výsledek
sčítání	sčítání	k1gNnPc2	sčítání
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
podává	podávat	k5eAaImIp3nS	podávat
tabulka	tabulka	k1gFnSc1	tabulka
<g/>
:	:	kIx,	:
Kromě	kromě	k7c2	kromě
nyní	nyní	k6eAd1	nyní
Ruskem	Rusko	k1gNnSc7	Rusko
anektovaného	anektovaný	k2eAgInSc2d1	anektovaný
Krymu	Krym	k1gInSc2	Krym
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
ruština	ruština	k1gFnSc1	ruština
a	a	k8xC	a
také	také	k9	také
tatarština	tatarština	k1gFnSc1	tatarština
a	a	k8xC	a
ukrajinština	ukrajinština	k1gFnSc1	ukrajinština
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
prozatím	prozatím	k6eAd1	prozatím
oficiálně	oficiálně	k6eAd1	oficiálně
úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
výhradně	výhradně	k6eAd1	výhradně
ukrajinština	ukrajinština	k1gFnSc1	ukrajinština
<g/>
;	;	kIx,	;
v	v	k7c6	v
jižních	jižní	k2eAgFnPc6d1	jižní
oblastech	oblast	k1gFnPc6	oblast
a	a	k8xC	a
zejména	zejména	k9	zejména
na	na	k7c6	na
Donbasu	Donbas	k1gInSc6	Donbas
je	být	k5eAaImIp3nS	být
však	však	k9	však
ruština	ruština	k1gFnSc1	ruština
de	de	k?	de
facto	facto	k1gNnSc1	facto
také	také	k9	také
úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
vlny	vlna	k1gFnPc1	vlna
ruských	ruský	k2eAgMnPc2d1	ruský
osadníků	osadník	k1gMnPc2	osadník
přišly	přijít	k5eAaPmAgFnP	přijít
na	na	k7c4	na
severovýchodní	severovýchodní	k2eAgFnPc4d1	severovýchodní
neobydlené	obydlený	k2eNgFnPc4d1	neobydlená
stepi	step	k1gFnPc4	step
v	v	k7c4	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
osídlili	osídlit	k5eAaPmAgMnP	osídlit
Rusové	Rus	k1gMnPc1	Rus
Krym	Krym	k1gInSc4	Krym
a	a	k8xC	a
jihovýchodní	jihovýchodní	k2eAgFnSc1d1	jihovýchodní
část	část	k1gFnSc1	část
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
;	;	kIx,	;
tato	tento	k3xDgFnSc1	tento
území	území	k1gNnSc4	území
dobyl	dobýt	k5eAaPmAgInS	dobýt
ruský	ruský	k2eAgInSc1d1	ruský
stát	stát	k1gInSc1	stát
od	od	k7c2	od
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
přišly	přijít	k5eAaPmAgInP	přijít
velké	velký	k2eAgInPc1d1	velký
počty	počet	k1gInPc1	počet
Rusů	Rus	k1gMnPc2	Rus
na	na	k7c6	na
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
hospodářsky	hospodářsky	k6eAd1	hospodářsky
rozvíjející	rozvíjející	k2eAgInSc1d1	rozvíjející
území	území	k1gNnSc4	území
Donbasu	Donbas	k1gInSc2	Donbas
za	za	k7c7	za
prací	práce	k1gFnSc7	práce
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
carského	carský	k2eAgNnSc2d1	carské
Ruska	Rusko	k1gNnSc2	Rusko
byla	být	k5eAaImAgFnS	být
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
vystavena	vystavit	k5eAaPmNgFnS	vystavit
rusifikační	rusifikační	k2eAgFnSc3d1	rusifikační
politice	politika	k1gFnSc3	politika
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
ve	v	k7c6	v
značné	značný	k2eAgFnSc6d1	značná
míře	míra	k1gFnSc6	míra
i	i	k8xC	i
v	v	k7c6	v
éře	éra	k1gFnSc6	éra
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
významnou	významný	k2eAgFnSc4d1	významná
část	část	k1gFnSc4	část
ukrajinských	ukrajinský	k2eAgMnPc2d1	ukrajinský
občanů	občan	k1gMnPc2	občan
není	být	k5eNaImIp3nS	být
ukrajinština	ukrajinština	k1gFnSc1	ukrajinština
jejich	jejich	k3xOp3gInSc7	jejich
rodným	rodný	k2eAgInSc7d1	rodný
jazykem	jazyk	k1gInSc7	jazyk
<g/>
:	:	kIx,	:
při	při	k7c6	při
oficiálním	oficiální	k2eAgNnSc6d1	oficiální
sčítání	sčítání	k1gNnSc6	sčítání
lidu	lid	k1gInSc2	lid
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
uvedlo	uvést	k5eAaPmAgNnS	uvést
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
rodný	rodný	k2eAgInSc4d1	rodný
jazyk	jazyk	k1gInSc4	jazyk
ukrajinštinu	ukrajinština	k1gFnSc4	ukrajinština
67,5	[number]	k4	67,5
%	%	kIx~	%
obyvatel	obyvatel	k1gMnSc1	obyvatel
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
85,2	[number]	k4	85,2
%	%	kIx~	%
lidí	člověk	k1gMnPc2	člověk
považujících	považující	k2eAgMnPc2d1	považující
se	se	k3xPyFc4	se
za	za	k7c2	za
Ukrajince	Ukrajinec	k1gMnSc2	Ukrajinec
–	–	k?	–
zhruba	zhruba	k6eAd1	zhruba
15	[number]	k4	15
%	%	kIx~	%
Ukrajinců	Ukrajinec	k1gMnPc2	Ukrajinec
tedy	tedy	k8xC	tedy
nehovoří	hovořit	k5eNaImIp3nS	hovořit
ukrajinsky	ukrajinsky	k6eAd1	ukrajinsky
<g/>
.	.	kIx.	.
</s>
<s>
Ruština	ruština	k1gFnSc1	ruština
je	být	k5eAaImIp3nS	být
rodným	rodný	k2eAgInSc7d1	rodný
jazykem	jazyk	k1gInSc7	jazyk
29,6	[number]	k4	29,6
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
necelé	celý	k2eNgFnPc1d1	necelá
dvě	dva	k4xCgFnPc4	dva
třetiny	třetina	k1gFnPc4	třetina
tvoří	tvořit	k5eAaImIp3nP	tvořit
etničtí	etnický	k2eAgMnPc1d1	etnický
Rusové	Rus	k1gMnPc1	Rus
<g/>
;	;	kIx,	;
rusky	rusky	k6eAd1	rusky
hovoří	hovořit	k5eAaImIp3nS	hovořit
také	také	k9	také
většina	většina	k1gFnSc1	většina
ukrajinských	ukrajinský	k2eAgMnPc2d1	ukrajinský
Bělorusů	Bělorus	k1gMnPc2	Bělorus
a	a	k8xC	a
Židů	Žid	k1gMnPc2	Žid
<g/>
.	.	kIx.	.
</s>
<s>
Své	své	k1gNnSc1	své
rodné	rodný	k2eAgFnSc2d1	rodná
řeči	řeč	k1gFnSc2	řeč
si	se	k3xPyFc3	se
také	také	k9	také
udržují	udržovat	k5eAaImIp3nP	udržovat
ukrajinští	ukrajinský	k2eAgMnPc1d1	ukrajinský
Rumuni	Rumun	k1gMnPc1	Rumun
<g/>
,	,	kIx,	,
Maďaři	Maďar	k1gMnPc1	Maďar
a	a	k8xC	a
Krymští	krymský	k2eAgMnPc1d1	krymský
Tataři	Tatar	k1gMnPc1	Tatar
<g/>
.	.	kIx.	.
</s>
<s>
Užívání	užívání	k1gNnSc1	užívání
ukrajinštiny	ukrajinština	k1gFnSc2	ukrajinština
převažuje	převažovat	k5eAaImIp3nS	převažovat
v	v	k7c6	v
západních	západní	k2eAgFnPc6d1	západní
<g/>
,	,	kIx,	,
severních	severní	k2eAgFnPc6d1	severní
a	a	k8xC	a
středních	střední	k2eAgFnPc6d1	střední
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Ruština	ruština	k1gFnSc1	ruština
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
polovinou	polovina	k1gFnSc7	polovina
obyvatel	obyvatel	k1gMnPc2	obyvatel
užívána	užívat	k5eAaImNgFnS	užívat
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
(	(	kIx(	(
<g/>
na	na	k7c6	na
Donbase	Donbas	k1gInSc6	Donbas
<g/>
,	,	kIx,	,
v	v	k7c6	v
Charkově	Charkov	k1gInSc6	Charkov
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dominantním	dominantní	k2eAgInSc7d1	dominantní
jazykem	jazyk	k1gInSc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
používaným	používaný	k2eAgInSc7d1	používaný
jazykem	jazyk	k1gInSc7	jazyk
je	být	k5eAaImIp3nS	být
suržyk	suržyk	k1gInSc1	suržyk
<g/>
,	,	kIx,	,
směs	směs	k1gFnSc1	směs
převážně	převážně	k6eAd1	převážně
ukrajinské	ukrajinský	k2eAgFnSc2d1	ukrajinská
gramatiky	gramatika	k1gFnSc2	gramatika
a	a	k8xC	a
ruské	ruský	k2eAgFnSc2d1	ruská
slovní	slovní	k2eAgFnSc2d1	slovní
zásoby	zásoba	k1gFnSc2	zásoba
<g/>
.	.	kIx.	.
</s>
<s>
Rozšířen	rozšířen	k2eAgInSc1d1	rozšířen
je	být	k5eAaImIp3nS	být
zejména	zejména	k9	zejména
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
věřící	věřící	k1gFnSc4	věřící
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
okolo	okolo	k7c2	okolo
60	[number]	k4	60
%	%	kIx~	%
obyvatel	obyvatel	k1gMnSc1	obyvatel
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
více	hodně	k6eAd2	hodně
nábožensky	nábožensky	k6eAd1	nábožensky
založen	založit	k5eAaPmNgInS	založit
je	být	k5eAaImIp3nS	být
západ	západ	k1gInSc1	západ
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
průmyslových	průmyslový	k2eAgFnPc6d1	průmyslová
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
na	na	k7c6	na
Donbasu	Donbas	k1gInSc6	Donbas
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
silnou	silný	k2eAgFnSc4d1	silná
pozici	pozice	k1gFnSc4	pozice
ateismus	ateismus	k1gInSc1	ateismus
<g/>
.	.	kIx.	.
</s>
<s>
Převládajícím	převládající	k2eAgNnSc7d1	převládající
náboženstvím	náboženství	k1gNnSc7	náboženství
je	být	k5eAaImIp3nS	být
pravoslavné	pravoslavný	k2eAgNnSc4d1	pravoslavné
křesťanství	křesťanství	k1gNnSc4	křesťanství
<g/>
;	;	kIx,	;
část	část	k1gFnSc1	část
pravoslavných	pravoslavný	k2eAgFnPc2d1	pravoslavná
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nP	hlásit
k	k	k7c3	k
Kyjevský	kyjevský	k2eAgInSc4d1	kyjevský
patriarchát	patriarchát	k1gInSc4	patriarchát
(	(	kIx(	(
<g/>
asi	asi	k9	asi
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
miliony	milion	k4xCgInPc4	milion
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
uznává	uznávat	k5eAaImIp3nS	uznávat
Moskevský	moskevský	k2eAgInSc4d1	moskevský
patriarchát	patriarchát	k1gInSc4	patriarchát
(	(	kIx(	(
<g/>
asi	asi	k9	asi
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
milionů	milion	k4xCgInPc2	milion
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
zde	zde	k6eAd1	zde
působí	působit	k5eAaImIp3nS	působit
na	na	k7c6	na
západě	západ	k1gInSc6	západ
země	zem	k1gFnSc2	zem
též	též	k9	též
Ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
autokefální	autokefální	k2eAgFnSc1d1	autokefální
pravoslavná	pravoslavný	k2eAgFnSc1d1	pravoslavná
církev	církev	k1gFnSc1	církev
(	(	kIx(	(
<g/>
asi	asi	k9	asi
1,5	[number]	k4	1,5
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
miliony	milion	k4xCgInPc1	milion
věřících	věřící	k1gMnPc2	věřící
–	–	k?	–
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ukrajinskou	ukrajinský	k2eAgFnSc7d1	ukrajinská
řeckokatolickou	řeckokatolický	k2eAgFnSc7d1	řeckokatolická
církví	církev	k1gFnSc7	církev
patří	patřit	k5eAaImIp3nS	patřit
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
historicky	historicky	k6eAd1	historicky
<g/>
,	,	kIx,	,
k	k	k7c3	k
největším	veliký	k2eAgMnPc3d3	veliký
podporovatelům	podporovatel	k1gMnPc3	podporovatel
ukrajinské	ukrajinský	k2eAgFnSc2d1	ukrajinská
svébytnosti	svébytnost	k1gFnSc2	svébytnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
západě	západ	k1gInSc6	západ
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
Haliči	Halič	k1gFnSc6	Halič
<g/>
,	,	kIx,	,
převládá	převládat	k5eAaImIp3nS	převládat
řeckokatolické	řeckokatolický	k2eAgNnSc1d1	řeckokatolické
vyznání	vyznání	k1gNnSc1	vyznání
(	(	kIx(	(
<g/>
Ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
řeckokatolická	řeckokatolický	k2eAgFnSc1d1	řeckokatolická
církev	církev	k1gFnSc1	církev
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
jsou	být	k5eAaImIp3nP	být
výraznější	výrazný	k2eAgFnPc1d2	výraznější
menšinou	menšina	k1gFnSc7	menšina
též	též	k6eAd1	též
římští	římský	k2eAgMnPc1d1	římský
katolíci	katolík	k1gMnPc1	katolík
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
celkem	celkem	k6eAd1	celkem
tvoří	tvořit	k5eAaImIp3nP	tvořit
necelé	celý	k2eNgInPc4d1	necelý
2	[number]	k4	2
%	%	kIx~	%
ukrajinských	ukrajinský	k2eAgMnPc2d1	ukrajinský
věřících	věřící	k1gMnPc2	věřící
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
protináboženského	protináboženský	k2eAgInSc2d1	protináboženský
režimu	režim	k1gInSc2	režim
SSSR	SSSR	kA	SSSR
bylo	být	k5eAaImAgNnS	být
opraveno	opraven	k2eAgNnSc1d1	opraveno
či	či	k8xC	či
znovu	znovu	k6eAd1	znovu
postaveno	postaven	k2eAgNnSc4d1	postaveno
množství	množství	k1gNnSc4	množství
kostelů	kostel	k1gInPc2	kostel
a	a	k8xC	a
poutních	poutní	k2eAgNnPc2d1	poutní
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
nejznámější	známý	k2eAgFnSc1d3	nejznámější
je	být	k5eAaImIp3nS	být
Kyjevskopečerská	Kyjevskopečerský	k2eAgFnSc1d1	Kyjevskopečerská
lávra	lávra	k1gFnSc1	lávra
<g/>
.	.	kIx.	.
</s>
<s>
Menšinovými	menšinový	k2eAgNnPc7d1	menšinové
náboženstvími	náboženství	k1gNnPc7	náboženství
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
však	však	k9	však
obě	dva	k4xCgFnPc1	dva
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
tradici	tradice	k1gFnSc4	tradice
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
judaismus	judaismus	k1gInSc1	judaismus
a	a	k8xC	a
islám	islám	k1gInSc1	islám
<g/>
.	.	kIx.	.
</s>
<s>
Židovství	židovství	k1gNnSc1	židovství
bylo	být	k5eAaImAgNnS	být
od	od	k7c2	od
středověku	středověk	k1gInSc2	středověk
rozšířeno	rozšířen	k2eAgNnSc1d1	rozšířeno
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
;	;	kIx,	;
kraj	kraj	k1gInSc1	kraj
Podolí	Podolí	k1gNnSc2	Podolí
se	se	k3xPyFc4	se
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
stal	stát	k5eAaPmAgInS	stát
ohniskem	ohnisko	k1gNnSc7	ohnisko
chasidského	chasidský	k2eAgNnSc2d1	chasidské
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
však	však	k9	však
tvoří	tvořit	k5eAaImIp3nP	tvořit
židé	žid	k1gMnPc1	žid
jen	jen	k6eAd1	jen
několik	několik	k4yIc4	několik
desetin	desetina	k1gFnPc2	desetina
procenta	procento	k1gNnSc2	procento
obyvatel	obyvatel	k1gMnSc1	obyvatel
země	zem	k1gFnSc2	zem
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
synagóg	synagóga	k1gFnPc2	synagóga
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
židovských	židovský	k2eAgFnPc2d1	židovská
památek	památka	k1gFnPc2	památka
již	již	k6eAd1	již
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Centrem	centr	k1gInSc7	centr
Islámu	islám	k1gInSc2	islám
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
je	být	k5eAaImIp3nS	být
Krymský	krymský	k2eAgInSc1d1	krymský
poloostrov	poloostrov	k1gInSc1	poloostrov
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
staletí	staletí	k1gNnSc6	staletí
osídlen	osídlen	k2eAgInSc4d1	osídlen
Tatary	tatar	k1gInPc4	tatar
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
Krymskými	krymský	k2eAgInPc7d1	krymský
Tatary	tatar	k1gInPc7	tatar
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
sem	sem	k6eAd1	sem
od	od	k7c2	od
konce	konec	k1gInSc2	konec
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vracejí	vracet	k5eAaImIp3nP	vracet
ze	z	k7c2	z
středoasijského	středoasijský	k2eAgNnSc2d1	středoasijské
vyhnanství	vyhnanství	k1gNnSc2	vyhnanství
<g/>
.	.	kIx.	.
</s>
<s>
Islám	islám	k1gInSc1	islám
vyznává	vyznávat	k5eAaImIp3nS	vyznávat
něco	něco	k6eAd1	něco
přes	přes	k7c4	přes
1	[number]	k4	1
%	%	kIx~	%
obyvatel	obyvatel	k1gMnSc1	obyvatel
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Zděděným	zděděný	k2eAgInSc7d1	zděděný
úspěchem	úspěch	k1gInSc7	úspěch
sovětské	sovětský	k2eAgFnSc2d1	sovětská
politiky	politika	k1gFnSc2	politika
všeobecného	všeobecný	k2eAgInSc2d1	všeobecný
přístupu	přístup	k1gInSc2	přístup
ke	k	k7c3	k
vzdělání	vzdělání	k1gNnSc3	vzdělání
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
stoprocentní	stoprocentní	k2eAgFnSc1d1	stoprocentní
míra	míra	k1gFnSc1	míra
gramotnosti	gramotnost	k1gFnSc2	gramotnost
v	v	k7c6	v
postsovětských	postsovětský	k2eAgInPc6d1	postsovětský
státech	stát	k1gInPc6	stát
<g/>
;	;	kIx,	;
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
dnes	dnes	k6eAd1	dnes
činí	činit	k5eAaImIp3nS	činit
99,4	[number]	k4	99,4
%	%	kIx~	%
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Povinné	povinný	k2eAgNnSc1d1	povinné
primární	primární	k2eAgNnSc1d1	primární
a	a	k8xC	a
sekundární	sekundární	k2eAgNnSc1d1	sekundární
vzdělání	vzdělání	k1gNnSc1	vzdělání
začíná	začínat	k5eAaImIp3nS	začínat
v	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
roce	rok	k1gInSc6	rok
věku	věk	k1gInSc2	věk
dítěte	dítě	k1gNnSc2	dítě
a	a	k8xC	a
trvá	trvat	k5eAaImIp3nS	trvat
12	[number]	k4	12
let	léto	k1gNnPc2	léto
<g/>
:	:	kIx,	:
základní	základní	k2eAgFnSc1d1	základní
trvá	trvat	k5eAaImIp3nS	trvat
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
střední	střední	k2eAgFnSc4d1	střední
sekundární	sekundární	k2eAgFnSc4d1	sekundární
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
a	a	k8xC	a
vyšší	vysoký	k2eAgNnSc1d2	vyšší
sekundární	sekundární	k2eAgNnSc1d1	sekundární
vzdělání	vzdělání	k1gNnSc1	vzdělání
pak	pak	k6eAd1	pak
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
zde	zde	k6eAd1	zde
tzv.	tzv.	kA	tzv.
všeobecně	všeobecně	k6eAd1	všeobecně
vzdělávací	vzdělávací	k2eAgFnPc4d1	vzdělávací
školy	škola	k1gFnPc4	škola
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
lze	lze	k6eAd1	lze
získat	získat	k5eAaPmF	získat
jak	jak	k6eAd1	jak
základní	základní	k2eAgFnPc4d1	základní
<g/>
,	,	kIx,	,
tak	tak	k9	tak
středoškolské	středoškolský	k2eAgNnSc4d1	středoškolské
vzdělání	vzdělání	k1gNnSc4	vzdělání
<g/>
.	.	kIx.	.
</s>
<s>
Známkování	známkování	k1gNnSc1	známkování
má	mít	k5eAaImIp3nS	mít
dvanáct	dvanáct	k4xCc4	dvanáct
stupňů	stupeň	k1gInPc2	stupeň
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
*	*	kIx~	*
<g/>
,	,	kIx,	,
11	[number]	k4	11
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
Británii	Británie	k1gFnSc6	Británie
a	a	k8xC	a
Francii	Francie	k1gFnSc6	Francie
má	mít	k5eAaImIp3nS	mít
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
počet	počet	k1gInSc4	počet
absolventů	absolvent	k1gMnPc2	absolvent
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
funguje	fungovat	k5eAaImIp3nS	fungovat
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
všeobecných	všeobecný	k2eAgFnPc2d1	všeobecná
<g/>
,	,	kIx,	,
technických	technický	k2eAgFnPc2d1	technická
<g/>
,	,	kIx,	,
pedagogických	pedagogický	k2eAgFnPc2d1	pedagogická
či	či	k8xC	či
lékařských	lékařský	k2eAgFnPc2d1	lékařská
univerzit	univerzita	k1gFnPc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc7d3	nejstarší
vysokou	vysoký	k2eAgFnSc7d1	vysoká
školou	škola	k1gFnSc7	škola
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Evropě	Evropa	k1gFnSc6	Evropa
byla	být	k5eAaImAgFnS	být
Ostrožská	ostrožský	k2eAgFnSc1d1	Ostrožská
akademie	akademie	k1gFnSc1	akademie
(	(	kIx(	(
<g/>
založena	založen	k2eAgFnSc1d1	založena
1576	[number]	k4	1576
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
následovala	následovat	k5eAaImAgFnS	následovat
Kyjevsko-mohyljanská	Kyjevskoohyljanský	k2eAgFnSc1d1	Kyjevsko-mohyljanský
akademie	akademie	k1gFnSc1	akademie
a	a	k8xC	a
Lvovská	lvovský	k2eAgFnSc1d1	Lvovská
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
škol	škola	k1gFnPc2	škola
je	být	k5eAaImIp3nS	být
veřejná	veřejný	k2eAgFnSc1d1	veřejná
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc1d3	veliký
soukromou	soukromý	k2eAgFnSc7d1	soukromá
vysokou	vysoký	k2eAgFnSc7d1	vysoká
školou	škola	k1gFnSc7	škola
je	být	k5eAaImIp3nS	být
Mezioblastní	Mezioblastní	k2eAgFnSc1d1	Mezioblastní
akademie	akademie	k1gFnSc1	akademie
personálního	personální	k2eAgInSc2d1	personální
managementu	management	k1gInSc2	management
<g/>
.	.	kIx.	.
</s>
<s>
Univerzitní	univerzitní	k2eAgNnSc1d1	univerzitní
vzdělání	vzdělání	k1gNnSc1	vzdělání
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
reformováno	reformován	k2eAgNnSc1d1	reformováno
a	a	k8xC	a
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
tzv.	tzv.	kA	tzv.
Boloňského	boloňský	k2eAgInSc2d1	boloňský
procesu	proces	k1gInSc2	proces
je	být	k5eAaImIp3nS	být
zaváděn	zaváděn	k2eAgInSc4d1	zaváděn
bakalářský	bakalářský	k2eAgInSc4d1	bakalářský
a	a	k8xC	a
magisterský	magisterský	k2eAgInSc4d1	magisterský
titul	titul	k1gInSc4	titul
<g/>
;	;	kIx,	;
tyto	tento	k3xDgFnPc1	tento
postupně	postupně	k6eAd1	postupně
vytěsňují	vytěsňovat	k5eAaImIp3nP	vytěsňovat
dřívější	dřívější	k2eAgInSc4d1	dřívější
titul	titul	k1gInSc4	titul
spiecialista	spiecialista	k1gMnSc1	spiecialista
<g/>
.	.	kIx.	.
</s>
<s>
Ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
kultura	kultura	k1gFnSc1	kultura
vyrůstá	vyrůstat	k5eAaImIp3nS	vyrůstat
z	z	k7c2	z
křesťanských	křesťanský	k2eAgFnPc2d1	křesťanská
tradic	tradice	k1gFnPc2	tradice
<g/>
,	,	kIx,	,
především	především	k9	především
z	z	k7c2	z
pravoslaví	pravoslaví	k1gNnSc2	pravoslaví
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
katolictví	katolictví	k1gNnSc2	katolictví
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dob	doba	k1gFnPc2	doba
Kyjevské	kyjevský	k2eAgFnSc2d1	Kyjevská
Rusi	Rus	k1gFnSc2	Rus
společné	společný	k2eAgInPc4d1	společný
kořeny	kořen	k1gInPc4	kořen
s	s	k7c7	s
běloruskou	běloruský	k2eAgFnSc7d1	Běloruská
a	a	k8xC	a
ruskou	ruský	k2eAgFnSc7d1	ruská
kulturou	kultura	k1gFnSc7	kultura
<g/>
;	;	kIx,	;
ruský	ruský	k2eAgInSc1d1	ruský
vliv	vliv	k1gInSc1	vliv
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ukrajinské	ukrajinský	k2eAgFnSc6d1	ukrajinská
kultuře	kultura	k1gFnSc6	kultura
trvale	trvale	k6eAd1	trvale
a	a	k8xC	a
silně	silně	k6eAd1	silně
přítomný	přítomný	k2eAgInSc1d1	přítomný
<g/>
.	.	kIx.	.
</s>
<s>
Významný	významný	k2eAgInSc1d1	významný
je	být	k5eAaImIp3nS	být
také	také	k9	také
přínos	přínos	k1gInSc1	přínos
někdejší	někdejší	k2eAgFnSc2d1	někdejší
početné	početný	k2eAgFnSc2d1	početná
židovské	židovský	k2eAgFnSc2d1	židovská
menšiny	menšina	k1gFnSc2	menšina
(	(	kIx(	(
<g/>
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
se	s	k7c7	s
mj.	mj.	kA	mj.
zrodilo	zrodit	k5eAaPmAgNnS	zrodit
chasidské	chasidský	k2eAgNnSc1d1	chasidské
hnutí	hnutí	k1gNnSc1	hnutí
<g/>
)	)	kIx)	)
a	a	k8xC	a
muslimů	muslim	k1gMnPc2	muslim
zejména	zejména	k9	zejména
na	na	k7c6	na
Krymu	Krym	k1gInSc6	Krym
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
polské	polský	k2eAgFnSc2d1	polská
nadvlády	nadvláda	k1gFnSc2	nadvláda
na	na	k7c6	na
pravobřežní	pravobřežní	k2eAgFnSc6d1	pravobřežní
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
se	se	k3xPyFc4	se
hojně	hojně	k6eAd1	hojně
prosazovaly	prosazovat	k5eAaImAgFnP	prosazovat
polské	polský	k2eAgInPc4d1	polský
vlivy	vliv	k1gInPc4	vliv
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
patrny	patrn	k2eAgFnPc4d1	patrna
např.	např.	kA	např.
v	v	k7c6	v
ukrajinské	ukrajinský	k2eAgFnSc6d1	ukrajinská
slovní	slovní	k2eAgFnSc6d1	slovní
zásobě	zásoba	k1gFnSc6	zásoba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
kultura	kultura	k1gFnSc1	kultura
a	a	k8xC	a
ukrajinština	ukrajinština	k1gFnSc1	ukrajinština
utlačována	utlačován	k2eAgFnSc1d1	utlačována
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
carského	carský	k2eAgNnSc2d1	carské
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
zájmem	zájem	k1gInSc7	zájem
bylo	být	k5eAaImAgNnS	být
celé	celý	k2eAgNnSc1d1	celé
území	území	k1gNnSc4	území
rusifikovat	rusifikovat	k5eAaBmF	rusifikovat
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
ukrajinskému	ukrajinský	k2eAgMnSc3d1	ukrajinský
národnímu	národní	k2eAgInSc3d1	národní
obrození	obrození	k1gNnSc2	obrození
podařilo	podařit	k5eAaPmAgNnS	podařit
etablovat	etablovat	k5eAaBmF	etablovat
spisovnou	spisovný	k2eAgFnSc4d1	spisovná
ukrajinštinu	ukrajinština	k1gFnSc4	ukrajinština
(	(	kIx(	(
<g/>
průkopnickým	průkopnický	k2eAgNnSc7d1	průkopnické
dílem	dílo	k1gNnSc7	dílo
byla	být	k5eAaImAgFnS	být
Aeneida	Aeneida	k1gFnSc1	Aeneida
Ivana	Ivana	k1gFnSc1	Ivana
Kotljarevského	Kotljarevský	k2eAgNnSc2d1	Kotljarevský
a	a	k8xC	a
o	o	k7c4	o
něco	něco	k3yInSc4	něco
později	pozdě	k6eAd2	pozdě
také	také	k9	také
básně	báseň	k1gFnPc4	báseň
Tarase	taras	k1gInSc6	taras
Ševčenka	Ševčenka	k1gFnSc1	Ševčenka
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc4	jehož
sochu	socha	k1gFnSc4	socha
dnes	dnes	k6eAd1	dnes
najdeme	najít	k5eAaPmIp1nP	najít
v	v	k7c6	v
téměř	téměř	k6eAd1	téměř
každém	každý	k3xTgNnSc6	každý
ukrajinském	ukrajinský	k2eAgNnSc6d1	ukrajinské
městě	město	k1gNnSc6	město
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
třetině	třetina	k1gFnSc6	třetina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
začala	začít	k5eAaPmAgFnS	začít
vznikat	vznikat	k5eAaImF	vznikat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
podnětů	podnět	k1gInPc2	podnět
a	a	k8xC	a
tradice	tradice	k1gFnSc1	tradice
lidového	lidový	k2eAgNnSc2d1	lidové
a	a	k8xC	a
pololidového	pololidový	k2eAgNnSc2d1	pololidový
hudebního	hudební	k2eAgNnSc2d1	hudební
divadla	divadlo	k1gNnSc2	divadlo
ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
škola	škola	k1gFnSc1	škola
opery	opera	k1gFnSc2	opera
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc7d3	nejstarší
živou	živý	k2eAgFnSc7d1	živá
součástí	součást	k1gFnSc7	součást
ukrajinského	ukrajinský	k2eAgInSc2d1	ukrajinský
repertoáru	repertoár	k1gInSc2	repertoár
je	být	k5eAaImIp3nS	být
Záporožec	Záporožec	k1gMnSc1	Záporožec
za	za	k7c7	za
Dunajem	Dunaj	k1gInSc7	Dunaj
Semena	semeno	k1gNnSc2	semeno
Hulaka-Artemovského	Hulaka-Artemovský	k1gMnSc2	Hulaka-Artemovský
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1863	[number]	k4	1863
<g/>
,	,	kIx,	,
za	za	k7c4	za
vlastního	vlastní	k2eAgMnSc4d1	vlastní
zakladatele	zakladatel	k1gMnSc4	zakladatel
ukrajinské	ukrajinský	k2eAgFnSc2d1	ukrajinská
opery	opera	k1gFnSc2	opera
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
Mykola	Mykola	k1gFnSc1	Mykola
Lysenko	lysenka	k1gFnSc5	lysenka
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
jí	on	k3xPp3gFnSc3	on
svými	svůj	k3xOyFgInPc7	svůj
díly	díl	k1gInPc7	díl
(	(	kIx(	(
<g/>
Natalka	Natalka	k1gFnSc1	Natalka
Poltavka	Poltavka	k1gFnSc1	Poltavka
<g/>
,	,	kIx,	,
Taras	taras	k1gInSc1	taras
Bulba	Bulba	k1gFnSc1	Bulba
<g/>
)	)	kIx)	)
vtiskl	vtisknout	k5eAaPmAgMnS	vtisknout
folklórní	folklórní	k2eAgInSc4d1	folklórní
ráz	ráz	k1gInSc4	ráz
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
ruské	ruský	k2eAgFnSc6d1	ruská
části	část	k1gFnSc6	část
vydán	vydat	k5eAaPmNgInS	vydat
dokonce	dokonce	k9	dokonce
zákaz	zákaz	k1gInSc1	zákaz
používání	používání	k1gNnSc2	používání
ukrajinštiny	ukrajinština	k1gFnSc2	ukrajinština
<g/>
,	,	kIx,	,
přesunuly	přesunout	k5eAaPmAgFnP	přesunout
se	se	k3xPyFc4	se
emancipační	emancipační	k2eAgFnPc1d1	emancipační
snahy	snaha	k1gFnPc1	snaha
ukrajinských	ukrajinský	k2eAgMnPc2d1	ukrajinský
spisovatelů	spisovatel	k1gMnPc2	spisovatel
a	a	k8xC	a
politiků	politik	k1gMnPc2	politik
do	do	k7c2	do
liberálnější	liberální	k2eAgFnSc2d2	liberálnější
habsburské	habsburský	k2eAgFnSc2d1	habsburská
části	část	k1gFnSc2	část
země	zem	k1gFnSc2	zem
–	–	k?	–
Haliče	Halič	k1gFnSc2	Halič
a	a	k8xC	a
Bukoviny	Bukovina	k1gFnSc2	Bukovina
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
nepřineslo	přinést	k5eNaPmAgNnS	přinést
ukrajinské	ukrajinský	k2eAgFnSc3d1	ukrajinská
kultuře	kultura	k1gFnSc3	kultura
příznivé	příznivý	k2eAgNnSc1d1	příznivé
prostředí	prostředí	k1gNnSc1	prostředí
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
rusifikace	rusifikace	k1gFnSc1	rusifikace
probíhala	probíhat	k5eAaImAgFnS	probíhat
i	i	k9	i
v	v	k7c6	v
éře	éra	k1gFnSc6	éra
SSSR	SSSR	kA	SSSR
(	(	kIx(	(
<g/>
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
jsou	být	k5eAaImIp3nP	být
národní	národní	k2eAgFnPc4d1	národní
otázky	otázka	k1gFnPc4	otázka
v	v	k7c6	v
současném	současný	k2eAgNnSc6d1	současné
ukrajinském	ukrajinský	k2eAgNnSc6d1	ukrajinské
umění	umění	k1gNnSc6	umění
stále	stále	k6eAd1	stále
živé	živý	k2eAgInPc1d1	živý
<g/>
.	.	kIx.	.
</s>
<s>
Národními	národní	k2eAgMnPc7d1	národní
básníky	básník	k1gMnPc7	básník
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
krom	krom	k7c2	krom
Ševčenka	Ševčenka	k1gFnSc1	Ševčenka
i	i	k8xC	i
Ivan	Ivan	k1gMnSc1	Ivan
Franko	franko	k6eAd1	franko
a	a	k8xC	a
Lesja	Lesj	k2eAgFnSc1d1	Lesja
Ukrajinka	Ukrajinka	k1gFnSc1	Ukrajinka
<g/>
.	.	kIx.	.
</s>
<s>
Průkopníkem	průkopník	k1gMnSc7	průkopník
moderní	moderní	k2eAgFnSc2d1	moderní
ukrajinské	ukrajinský	k2eAgFnSc2d1	ukrajinská
literatury	literatura	k1gFnSc2	literatura
byl	být	k5eAaImAgMnS	být
též	též	k9	též
Ivan	Ivan	k1gMnSc1	Ivan
Kotljarevskyj	Kotljarevskyj	k1gMnSc1	Kotljarevskyj
<g/>
.	.	kIx.	.
</s>
<s>
Králem	Král	k1gMnSc7	Král
ukrajinské	ukrajinský	k2eAgFnSc2d1	ukrajinská
literatury	literatura	k1gFnSc2	literatura
je	být	k5eAaImIp3nS	být
bezpochyby	bezpochyby	k6eAd1	bezpochyby
světově	světově	k6eAd1	světově
proslulý	proslulý	k2eAgMnSc1d1	proslulý
satirik	satirik	k1gMnSc1	satirik
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Vasiljevič	Vasiljevič	k1gMnSc1	Vasiljevič
Gogol	Gogol	k1gMnSc1	Gogol
<g/>
.	.	kIx.	.
</s>
<s>
Ukrajinský	ukrajinský	k2eAgInSc1d1	ukrajinský
původ	původ	k1gInSc1	původ
má	mít	k5eAaImIp3nS	mít
ruský	ruský	k2eAgMnSc1d1	ruský
básník	básník	k1gMnSc1	básník
Jevgenij	Jevgenij	k1gMnSc1	Jevgenij
Jevtušenko	Jevtušenka	k1gFnSc5	Jevtušenka
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
narozen	narozen	k2eAgMnSc1d1	narozen
na	na	k7c6	na
Sibiři	Sibiř	k1gFnSc6	Sibiř
<g/>
,	,	kIx,	,
v	v	k7c6	v
Kyjevě	Kyjev	k1gInSc6	Kyjev
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
dramatik	dramatik	k1gMnSc1	dramatik
Michail	Michail	k1gMnSc1	Michail
Bulgakov	Bulgakov	k1gInSc4	Bulgakov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Oděse	Oděsa	k1gFnSc6	Oděsa
se	se	k3xPyFc4	se
narodili	narodit	k5eAaPmAgMnP	narodit
Isaak	Isaak	k1gMnSc1	Isaak
Babel	Babel	k1gMnSc1	Babel
či	či	k8xC	či
Anna	Anna	k1gFnSc1	Anna
Andrejevna	Andrejevna	k1gFnSc1	Andrejevna
Achmatovová	Achmatovová	k1gFnSc1	Achmatovová
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Vinnycké	Vinnycký	k2eAgFnSc6d1	Vinnycký
oblasti	oblast	k1gFnSc6	oblast
brazilská	brazilský	k2eAgFnSc1d1	brazilská
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
Clarice	Clarika	k1gFnSc3	Clarika
Lispectorová	Lispectorový	k2eAgFnSc1d1	Lispectorový
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
území	území	k1gNnSc4	území
Haliče	Halič	k1gFnSc2	Halič
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
léta	léto	k1gNnSc2	léto
přináleželo	přináležet	k5eAaImAgNnS	přináležet
k	k	k7c3	k
Polsku	Polsko	k1gNnSc3	Polsko
a	a	k8xC	a
také	také	k9	také
Rakousko-Uhersku	Rakousko-Uherska	k1gFnSc4	Rakousko-Uherska
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
Haliče	Halič	k1gFnSc2	Halič
<g/>
,	,	kIx,	,
Lvově	lvově	k6eAd1	lvově
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
například	například	k6eAd1	například
klasik	klasik	k1gMnSc1	klasik
sci-fi	scii	k1gFnSc2	sci-fi
Stanisław	Stanisław	k1gMnSc1	Stanisław
Lem	lem	k1gInSc1	lem
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
esejista	esejista	k1gMnSc1	esejista
Zbigniew	Zbigniew	k1gMnSc1	Zbigniew
Herbert	Herbert	k1gMnSc1	Herbert
<g/>
,	,	kIx,	,
humorista	humorista	k1gMnSc1	humorista
Stanisław	Stanisław	k1gFnSc2	Stanisław
Jerzy	Jerza	k1gFnSc2	Jerza
Lec	Lec	k1gFnSc2	Lec
či	či	k8xC	či
erotický	erotický	k2eAgMnSc1d1	erotický
spisovatel	spisovatel	k1gMnSc1	spisovatel
Leopold	Leopolda	k1gFnPc2	Leopolda
von	von	k1gInSc1	von
Sacher-Masoch	Sacher-Masoch	k1gInSc4	Sacher-Masoch
<g/>
,	,	kIx,	,
po	po	k7c6	po
němž	jenž	k3xRgNnSc6	jenž
byl	být	k5eAaImAgInS	být
posléze	posléze	k6eAd1	posléze
nazván	nazván	k2eAgInSc1d1	nazván
masochismus	masochismus	k1gInSc1	masochismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
jsou	být	k5eAaImIp3nP	být
populární	populární	k2eAgFnPc1d1	populární
knihy	kniha	k1gFnPc1	kniha
Jurije	Jurije	k1gFnSc2	Jurije
Andruchovyče	Andruchovyč	k1gFnSc2	Andruchovyč
<g/>
,	,	kIx,	,
Andreje	Andrej	k1gMnSc2	Andrej
Kurkova	Kurkův	k2eAgMnSc2d1	Kurkův
<g/>
,	,	kIx,	,
či	či	k8xC	či
Ihora	Ihor	k1gMnSc4	Ihor
Pavljuka	Pavljuk	k1gMnSc4	Pavljuk
<g/>
.	.	kIx.	.
</s>
<s>
Hojně	hojně	k6eAd1	hojně
překládánijsou	překládánijsý	k2eAgFnSc4d1	překládánijsý
též	též	k9	též
Oksana	Oksana	k1gFnSc1	Oksana
Zabužko	Zabužka	k1gFnSc5	Zabužka
či	či	k8xC	či
Jurij	Jurij	k1gMnSc1	Jurij
Andruchovyč	Andruchovyč	k1gMnSc1	Andruchovyč
<g/>
.	.	kIx.	.
</s>
<s>
Nejslavnějším	slavný	k2eAgMnSc7d3	nejslavnější
skladatelem	skladatel	k1gMnSc7	skladatel
narozeným	narozený	k2eAgMnSc7d1	narozený
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
je	být	k5eAaImIp3nS	být
ruský	ruský	k2eAgMnSc1d1	ruský
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
pianista	pianista	k1gMnSc1	pianista
a	a	k8xC	a
dirigent	dirigent	k1gMnSc1	dirigent
Sergej	Sergej	k1gMnSc1	Sergej
Prokofjev	Prokofjev	k1gMnSc1	Prokofjev
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
interpretů	interpret	k1gMnPc2	interpret
pianista	pianista	k1gMnSc1	pianista
ruského	ruský	k2eAgInSc2d1	ruský
původu	původ	k1gInSc2	původ
Alexander	Alexandra	k1gFnPc2	Alexandra
Siloti	Silot	k1gMnPc1	Silot
či	či	k8xC	či
baletní	baletní	k2eAgMnSc1d1	baletní
tanečník	tanečník	k1gMnSc1	tanečník
a	a	k8xC	a
choreograf	choreograf	k1gMnSc1	choreograf
Serge	Serge	k1gFnPc2	Serge
Lifar	Lifar	k1gMnSc1	Lifar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Kyjevě	Kyjev	k1gInSc6	Kyjev
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
významný	významný	k2eAgMnSc1d1	významný
avantgardní	avantgardní	k2eAgMnSc1d1	avantgardní
sochař	sochař	k1gMnSc1	sochař
Olexandr	Olexandr	k1gInSc4	Olexandr
Porfyrovyč	Porfyrovyč	k1gInSc1	Porfyrovyč
Archypenko	Archypenka	k1gFnSc5	Archypenka
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
současným	současný	k2eAgMnPc3d1	současný
známým	známý	k2eAgMnPc3d1	známý
sochařům	sochař	k1gMnPc3	sochař
patří	patřit	k5eAaImIp3nS	patřit
Mykola	Mykola	k1gFnSc1	Mykola
Šmatko	Šmatka	k1gFnSc5	Šmatka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
se	se	k3xPyFc4	se
narodili	narodit	k5eAaPmAgMnP	narodit
i	i	k9	i
malíři	malíř	k1gMnPc1	malíř
ruského	ruský	k2eAgInSc2d1	ruský
původu	původ	k1gInSc2	původ
Ilja	Ilja	k1gMnSc1	Ilja
Repin	Repin	k1gMnSc1	Repin
<g/>
,	,	kIx,	,
Vladimir	Vladimir	k1gMnSc1	Vladimir
Tatlin	Tatlin	k1gInSc1	Tatlin
<g/>
,	,	kIx,	,
Kazimir	Kazimir	k1gMnSc1	Kazimir
Malevič	Malevič	k1gMnSc1	Malevič
<g/>
,	,	kIx,	,
krajinář	krajinář	k1gMnSc1	krajinář
Archip	Archip	k1gMnSc1	Archip
Kuindži	Kuindž	k1gFnSc6	Kuindž
či	či	k8xC	či
představitel	představitel	k1gMnSc1	představitel
socialistického	socialistický	k2eAgInSc2d1	socialistický
realismu	realismus	k1gInSc2	realismus
a	a	k8xC	a
autor	autor	k1gMnSc1	autor
známého	známý	k2eAgInSc2d1	známý
Leninova	Leninův	k2eAgInSc2d1	Leninův
obrazu	obraz	k1gInSc2	obraz
byl	být	k5eAaImAgMnS	být
Isaak	Isaak	k1gMnSc1	Isaak
Brodskij	Brodskij	k1gMnSc1	Brodskij
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc7d1	významná
malířkou	malířka	k1gFnSc7	malířka
a	a	k8xC	a
sochařkou	sochařka	k1gFnSc7	sochařka
ruského	ruský	k2eAgInSc2d1	ruský
původu	původ	k1gInSc2	původ
byla	být	k5eAaImAgFnS	být
Marija	Marij	k2eAgFnSc1d1	Marija
Baškirceva	Baškirceva	k1gFnSc1	Baškirceva
<g/>
.	.	kIx.	.
</s>
<s>
Svéráznou	svérázný	k2eAgFnSc7d1	svérázná
performerkou	performerka	k1gFnSc7	performerka
ruského	ruský	k2eAgInSc2d1	ruský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
využívající	využívající	k2eAgInSc4d1	využívající
především	především	k9	především
písek	písek	k1gInSc4	písek
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Ksenija	Ksenija	k1gFnSc1	Ksenija
Simonova	Simonův	k2eAgFnSc1d1	Simonova
<g/>
.	.	kIx.	.
</s>
<s>
Významnými	významný	k2eAgMnPc7d1	významný
filmovými	filmový	k2eAgMnPc7d1	filmový
režiséry	režisér	k1gMnPc7	režisér
z	z	k7c2	z
časů	čas	k1gInPc2	čas
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
byli	být	k5eAaImAgMnP	být
Olexandr	Olexandr	k1gInSc4	Olexandr
Dovženko	Dovženka	k1gFnSc5	Dovženka
a	a	k8xC	a
Serhij	Serhij	k1gMnSc1	Serhij
(	(	kIx(	(
<g/>
Sergej	Sergej	k1gMnSc1	Sergej
<g/>
)	)	kIx)	)
Bondarčuk	Bondarčuk	k1gMnSc1	Bondarčuk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Kyjevě	Kyjev	k1gInSc6	Kyjev
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
také	také	k9	také
režisér	režisér	k1gMnSc1	režisér
Anatole	Anatola	k1gFnSc3	Anatola
Litvak	Litvak	k1gInSc4	Litvak
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
modelingu	modeling	k1gInSc2	modeling
odstartovala	odstartovat	k5eAaPmAgFnS	odstartovat
k	k	k7c3	k
hollywoodské	hollywoodský	k2eAgFnSc3d1	hollywoodská
slávě	sláva	k1gFnSc3	sláva
Olga	Olga	k1gFnSc1	Olga
Kurylenková	Kurylenkový	k2eAgFnSc1d1	Kurylenková
i	i	k9	i
Milla	Milla	k1gFnSc1	Milla
Jovovichová	Jovovichový	k2eAgFnSc1d1	Jovovichový
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
především	především	k9	především
z	z	k7c2	z
bondovky	bondovky	k?	bondovky
Quantum	Quantum	k1gNnSc1	Quantum
of	of	k?	of
Solace	Solace	k1gFnSc1	Solace
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
živý	živý	k2eAgInSc1d1	živý
ukrajinský	ukrajinský	k2eAgInSc1d1	ukrajinský
folklór	folklór	k1gInSc1	folklór
<g/>
;	;	kIx,	;
bylo	být	k5eAaImAgNnS	být
obnoveno	obnovit	k5eAaPmNgNnS	obnovit
mnoho	mnoho	k4c1	mnoho
venkovských	venkovský	k2eAgFnPc2d1	venkovská
slavností	slavnost	k1gFnPc2	slavnost
<g/>
,	,	kIx,	,
existují	existovat	k5eAaImIp3nP	existovat
soubory	soubor	k1gInPc7	soubor
lidových	lidový	k2eAgFnPc2d1	lidová
písní	píseň	k1gFnPc2	píseň
a	a	k8xC	a
tanců	tanec	k1gInPc2	tanec
<g/>
;	;	kIx,	;
prvky	prvek	k1gInPc1	prvek
písňového	písňový	k2eAgInSc2d1	písňový
folklóru	folklór	k1gInSc2	folklór
ve	v	k7c6	v
výrazné	výrazný	k2eAgFnSc6d1	výrazná
míře	míra	k1gFnSc6	míra
pronikají	pronikat	k5eAaImIp3nP	pronikat
i	i	k9	i
do	do	k7c2	do
současné	současný	k2eAgFnSc2d1	současná
ukrajinské	ukrajinský	k2eAgFnSc2d1	ukrajinská
pop-music	popusic	k1gFnSc2	pop-music
<g/>
.	.	kIx.	.
</s>
<s>
Oblíbená	oblíbený	k2eAgFnSc1d1	oblíbená
je	být	k5eAaImIp3nS	být
také	také	k9	také
tradiční	tradiční	k2eAgFnSc1d1	tradiční
ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
kuchyně	kuchyně	k1gFnSc1	kuchyně
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
s	s	k7c7	s
oblibou	obliba	k1gFnSc7	obliba
užívá	užívat	k5eAaImIp3nS	užívat
obilné	obilný	k2eAgNnSc4d1	obilné
či	či	k8xC	či
bramborové	bramborový	k2eAgNnSc4d1	bramborové
těsto	těsto	k1gNnSc4	těsto
<g/>
,	,	kIx,	,
slunečnice	slunečnice	k1gFnPc4	slunečnice
<g/>
,	,	kIx,	,
zelí	zelí	k1gNnSc4	zelí
<g/>
,	,	kIx,	,
řepu	řepa	k1gFnSc4	řepa
<g/>
,	,	kIx,	,
mleté	mletý	k2eAgNnSc4d1	mleté
maso	maso	k1gNnSc4	maso
či	či	k8xC	či
ryby	ryba	k1gFnPc4	ryba
<g/>
;	;	kIx,	;
typickými	typický	k2eAgInPc7d1	typický
pokrmy	pokrm	k1gInPc7	pokrm
jsou	být	k5eAaImIp3nP	být
boršč	boršč	k1gInSc4	boršč
<g/>
,	,	kIx,	,
pelmeně	pelmeně	k1gFnPc1	pelmeně
<g/>
,	,	kIx,	,
varenyky	varenyka	k1gFnPc1	varenyka
či	či	k8xC	či
plněné	plněný	k2eAgInPc1d1	plněný
pirohy	piroh	k1gInPc1	piroh
<g/>
;	;	kIx,	;
k	k	k7c3	k
Ukrajině	Ukrajina	k1gFnSc3	Ukrajina
ovšem	ovšem	k9	ovšem
patří	patřit	k5eAaImIp3nS	patřit
také	také	k9	také
vodka	vodka	k1gFnSc1	vodka
(	(	kIx(	(
<g/>
horilka	horilka	k1gFnSc1	horilka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
částečně	částečně	k6eAd1	částečně
konkuruje	konkurovat	k5eAaImIp3nS	konkurovat
pivo	pivo	k1gNnSc1	pivo
<g/>
;	;	kIx,	;
oblíbeným	oblíbený	k2eAgInSc7d1	oblíbený
nápojem	nápoj	k1gInSc7	nápoj
je	být	k5eAaImIp3nS	být
také	také	k9	také
kvas	kvas	k1gInSc1	kvas
<g/>
.	.	kIx.	.
</s>
<s>
Velikonoce	Velikonoce	k1gFnPc1	Velikonoce
se	se	k3xPyFc4	se
v	v	k7c6	v
ukrajinštině	ukrajinština	k1gFnSc6	ukrajinština
nazývají	nazývat	k5eAaImIp3nP	nazývat
velykdeň	velykdeň	k1gFnSc1	velykdeň
<g/>
,	,	kIx,	,
pascha	pascha	k1gFnSc1	pascha
či	či	k8xC	či
paska	paska	k1gFnSc1	paska
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
Vánoce	Vánoce	k1gFnPc1	Vánoce
slaveny	slaven	k2eAgFnPc1d1	slavena
dle	dle	k7c2	dle
juliánského	juliánský	k2eAgInSc2d1	juliánský
kalendáře	kalendář	k1gInSc2	kalendář
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
později	pozdě	k6eAd2	pozdě
než	než	k8xS	než
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgMnSc1d1	populární
je	být	k5eAaImIp3nS	být
barvení	barvení	k1gNnSc1	barvení
kraslic	kraslice	k1gFnPc2	kraslice
(	(	kIx(	(
<g/>
pysanky	pysanka	k1gFnPc1	pysanka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterému	který	k3yIgMnSc3	který
se	se	k3xPyFc4	se
v	v	k7c6	v
západoukrajinské	západoukrajinský	k2eAgFnSc6d1	západoukrajinský
Kolomyji	Kolomyji	k1gFnSc6	Kolomyji
věnuje	věnovat	k5eAaPmIp3nS	věnovat
unikátní	unikátní	k2eAgNnSc1d1	unikátní
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Ukrajincem	Ukrajinec	k1gMnSc7	Ukrajinec
byl	být	k5eAaImAgMnS	být
matematik	matematik	k1gMnSc1	matematik
Mychajlo	Mychajlo	k1gNnSc1	Mychajlo
Ostrohradskyj	Ostrohradskyj	k1gMnSc1	Ostrohradskyj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
oceňován	oceňovat	k5eAaImNgMnS	oceňovat
matematik	matematik	k1gMnSc1	matematik
Vladimir	Vladimir	k1gMnSc1	Vladimir
Geršonovič	Geršonovič	k1gMnSc1	Geršonovič
Drinfeld	Drinfeld	k1gMnSc1	Drinfeld
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
teorie	teorie	k1gFnSc2	teorie
kvantové	kvantový	k2eAgFnSc2d1	kvantová
grupy	grupa	k1gFnSc2	grupa
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
vrtulník	vrtulník	k1gInSc1	vrtulník
či	či	k8xC	či
čtyřmotorový	čtyřmotorový	k2eAgInSc1d1	čtyřmotorový
letoun	letoun	k1gInSc1	letoun
sestavil	sestavit	k5eAaPmAgInS	sestavit
ukrajinský	ukrajinský	k2eAgMnSc1d1	ukrajinský
rodák	rodák	k1gMnSc1	rodák
ruského	ruský	k2eAgMnSc2d1	ruský
a	a	k8xC	a
polského	polský	k2eAgInSc2d1	polský
šlechtického	šlechtický	k2eAgInSc2d1	šlechtický
původu	původ	k1gInSc2	původ
Igor	Igor	k1gMnSc1	Igor
Sikorskij	Sikorskij	k1gMnSc1	Sikorskij
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgMnSc7d1	významný
mechanikem	mechanik	k1gMnSc7	mechanik
byl	být	k5eAaImAgMnS	být
Stepan	Stepan	k1gMnSc1	Stepan
Tymošenko	Tymošenka	k1gFnSc5	Tymošenka
<g/>
.	.	kIx.	.
</s>
<s>
Ač	ač	k8xS	ač
narozen	narozen	k2eAgMnSc1d1	narozen
v	v	k7c6	v
ruském	ruský	k2eAgInSc6d1	ruský
Petrohradě	Petrohrad	k1gInSc6	Petrohrad
<g/>
,	,	kIx,	,
etnickým	etnický	k2eAgMnSc7d1	etnický
Ukrajincem	Ukrajinec	k1gMnSc7	Ukrajinec
byl	být	k5eAaImAgMnS	být
zakladatel	zakladatel	k1gMnSc1	zakladatel
geochemie	geochemie	k1gFnSc2	geochemie
Volodymyr	Volodymyr	k1gMnSc1	Volodymyr
Vernadskyj	Vernadskyj	k1gMnSc1	Vernadskyj
V	v	k7c6	v
Charkově	Charkov	k1gInSc6	Charkov
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
astronom	astronom	k1gMnSc1	astronom
Otto	Otto	k1gMnSc1	Otto
Struve	Struev	k1gFnSc2	Struev
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
i	i	k9	i
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
fyziologii	fyziologie	k1gFnSc4	fyziologie
Ilja	Ilja	k1gMnSc1	Ilja
Iljič	Iljič	k1gMnSc1	Iljič
Mečnikov	Mečnikov	k1gInSc4	Mečnikov
<g/>
.	.	kIx.	.
</s>
<s>
Theodosius	Theodosius	k1gMnSc1	Theodosius
Dobzhansky	Dobzhansky	k1gMnSc1	Dobzhansky
byl	být	k5eAaImAgMnS	být
genetikem	genetik	k1gMnSc7	genetik
hrajícím	hrající	k2eAgFnPc3d1	hrající
klíčovou	klíčový	k2eAgFnSc4d1	klíčová
roli	role	k1gFnSc4	role
při	při	k7c6	při
tvorbě	tvorba	k1gFnSc6	tvorba
tzv.	tzv.	kA	tzv.
moderní	moderní	k2eAgFnSc2d1	moderní
evoluční	evoluční	k2eAgFnSc2d1	evoluční
syntézy	syntéza	k1gFnSc2	syntéza
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
spíše	spíše	k9	spíše
kuriózní	kuriózní	k2eAgFnSc7d1	kuriózní
postavou	postava	k1gFnSc7	postava
byl	být	k5eAaImAgMnS	být
biolog	biolog	k1gMnSc1	biolog
sovětský	sovětský	k2eAgMnSc1d1	sovětský
Trofim	Trofim	k1gMnSc1	Trofim
Lysenko	lysenka	k1gFnSc5	lysenka
<g/>
.	.	kIx.	.
</s>
<s>
Oděsa	Oděsa	k1gFnSc1	Oděsa
bývala	bývat	k5eAaImAgFnS	bývat
tradičním	tradiční	k2eAgNnSc7d1	tradiční
místem	místo	k1gNnSc7	místo
zrodu	zrod	k1gInSc2	zrod
geniálních	geniální	k2eAgFnPc2d1	geniální
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Patřil	patřit	k5eAaImAgMnS	patřit
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
i	i	k9	i
fyzik	fyzik	k1gMnSc1	fyzik
George	George	k1gFnPc2	George
Gamow	Gamow	k1gMnSc1	Gamow
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
teorie	teorie	k1gFnSc2	teorie
o	o	k7c6	o
Velkém	velký	k2eAgInSc6d1	velký
třesku	třesk	k1gInSc6	třesk
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
matematik	matematik	k1gMnSc1	matematik
Vladimir	Vladimir	k1gMnSc1	Vladimir
Arnold	Arnold	k1gMnSc1	Arnold
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Lvově	Lvov	k1gInSc6	Lvov
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
matematik	matematik	k1gMnSc1	matematik
Stanisław	Stanisław	k1gMnSc1	Stanisław
Ulam	ulámat	k5eAaPmRp2nS	ulámat
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
Ukrajinců	Ukrajinec	k1gMnPc2	Ukrajinec
se	se	k3xPyFc4	se
prosadilo	prosadit	k5eAaPmAgNnS	prosadit
i	i	k9	i
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
humanitních	humanitní	k2eAgFnPc2d1	humanitní
a	a	k8xC	a
sociálních	sociální	k2eAgFnPc2d1	sociální
věd	věda	k1gFnPc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
Osvícencem	osvícenec	k1gMnSc7	osvícenec
a	a	k8xC	a
otcem	otec	k1gMnSc7	otec
ukrajinské	ukrajinský	k2eAgFnSc2d1	ukrajinská
vzdělanosti	vzdělanost	k1gFnSc2	vzdělanost
byl	být	k5eAaImAgMnS	být
Hryhorij	Hryhorij	k1gMnSc1	Hryhorij
Skovoroda	Skovoroda	k1gMnSc1	Skovoroda
<g/>
.	.	kIx.	.
</s>
<s>
Ukrajinský	ukrajinský	k2eAgMnSc1d1	ukrajinský
původ	původ	k1gMnSc1	původ
měl	mít	k5eAaImAgMnS	mít
i	i	k8xC	i
etnograf	etnograf	k1gMnSc1	etnograf
Mykola	Mykola	k1gFnSc1	Mykola
Myklucho-Maklaj	Myklucho-Maklaj	k1gFnSc1	Myklucho-Maklaj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Kyjevě	Kyjev	k1gInSc6	Kyjev
se	se	k3xPyFc4	se
narodili	narodit	k5eAaPmAgMnP	narodit
ruský	ruský	k2eAgMnSc1d1	ruský
filozof	filozof	k1gMnSc1	filozof
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Berďajev	Berďajev	k1gMnSc1	Berďajev
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Lvově	Lvov	k1gInSc6	Lvov
Ludwig	Ludwig	k1gInSc1	Ludwig
von	von	k1gInSc1	von
Mises	Mises	k1gInSc1	Mises
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
proslulé	proslulý	k2eAgFnSc2d1	proslulá
rakouské	rakouský	k2eAgFnSc2d1	rakouská
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Značných	značný	k2eAgInPc2d1	značný
úspěchů	úspěch	k1gInPc2	úspěch
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
ukrajinští	ukrajinský	k2eAgMnPc1d1	ukrajinský
šachisté	šachista	k1gMnPc1	šachista
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
byli	být	k5eAaImAgMnP	být
Vasyl	Vasyl	k1gInSc4	Vasyl
Ivančuk	Ivančuka	k1gFnPc2	Ivančuka
<g/>
,	,	kIx,	,
Anna	Anna	k1gFnSc1	Anna
Muzyčuková	Muzyčukový	k2eAgFnSc1d1	Muzyčukový
<g/>
,	,	kIx,	,
Ruslan	Ruslan	k1gInSc1	Ruslan
Ponomarjov	Ponomarjov	k1gInSc1	Ponomarjov
<g/>
,	,	kIx,	,
Jefim	Jefim	k1gInSc1	Jefim
Bogoljubov	Bogoljubov	k1gInSc1	Bogoljubov
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
Bronštejn	Bronštejn	k1gMnSc1	Bronštejn
<g/>
,	,	kIx,	,
Sergej	Sergej	k1gMnSc1	Sergej
Karjakin	Karjakin	k1gMnSc1	Karjakin
<g/>
.	.	kIx.	.
</s>
<s>
Úspěchů	úspěch	k1gInPc2	úspěch
dosáhly	dosáhnout	k5eAaPmAgInP	dosáhnout
také	také	k9	také
ukrajinské	ukrajinský	k2eAgInPc1d1	ukrajinský
fotbalové	fotbalový	k2eAgInPc1d1	fotbalový
kluby	klub	k1gInPc1	klub
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Dynamo	dynamo	k1gNnSc4	dynamo
Kyjev	Kyjev	k1gInSc1	Kyjev
a	a	k8xC	a
Šachtar	Šachtar	k1gInSc1	Šachtar
Doněck	Doněck	k1gInSc1	Doněck
<g/>
.	.	kIx.	.
</s>
<s>
Třemi	tři	k4xCgMnPc7	tři
nejznámějšími	známý	k2eAgMnPc7d3	nejznámější
hráči	hráč	k1gMnPc7	hráč
byli	být	k5eAaImAgMnP	být
Oleh	Oleh	k1gMnSc1	Oleh
Blochin	Blochin	k1gMnSc1	Blochin
<g/>
,	,	kIx,	,
Ihor	Ihor	k1gMnSc1	Ihor
Belanov	Belanov	k1gInSc1	Belanov
a	a	k8xC	a
Andrej	Andrej	k1gMnSc1	Andrej
Ševčenko	Ševčenka	k1gFnSc5	Ševčenka
<g/>
.	.	kIx.	.
</s>
<s>
Fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
život	život	k1gInSc1	život
všech	všecek	k3xTgFnPc2	všecek
tří	tři	k4xCgFnPc2	tři
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
trenér	trenér	k1gMnSc1	trenér
Valerij	Valerij	k1gMnSc1	Valerij
Lobanovskyj	Lobanovskyj	k1gMnSc1	Lobanovskyj
<g/>
.	.	kIx.	.
</s>
<s>
Nejslavnějším	slavný	k2eAgMnSc7d3	nejslavnější
ukrajinským	ukrajinský	k2eAgMnSc7d1	ukrajinský
sportovcem	sportovec	k1gMnSc7	sportovec
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
je	být	k5eAaImIp3nS	být
lehký	lehký	k2eAgMnSc1d1	lehký
atlet	atlet	k1gMnSc1	atlet
<g/>
,	,	kIx,	,
skokan	skokan	k1gMnSc1	skokan
o	o	k7c6	o
tyči	tyč	k1gFnSc6	tyč
Serhij	Serhij	k1gMnSc1	Serhij
Bubka	Bubka	k1gMnSc1	Bubka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
časech	čas	k1gInPc6	čas
SSSR	SSSR	kA	SSSR
přivezli	přivézt	k5eAaPmAgMnP	přivézt
olympijské	olympijský	k2eAgFnPc4d1	olympijská
medaile	medaile	k1gFnPc4	medaile
také	také	k9	také
třeba	třeba	k6eAd1	třeba
sprinter	sprinter	k1gMnSc1	sprinter
Valerij	Valerij	k1gMnSc1	Valerij
Borzov	Borzovo	k1gNnPc2	Borzovo
či	či	k8xC	či
vytrvalostní	vytrvalostní	k2eAgMnSc1d1	vytrvalostní
běžec	běžec	k1gMnSc1	běžec
Volodymyr	Volodymyr	k1gMnSc1	Volodymyr
Kuc	kuc	k0	kuc
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
Ukrajinu	Ukrajina	k1gFnSc4	Ukrajina
vybojovala	vybojovat	k5eAaPmAgFnS	vybojovat
čtyři	čtyři	k4xCgFnPc4	čtyři
zlaté	zlatý	k2eAgFnPc4d1	zlatá
medaile	medaile	k1gFnPc4	medaile
plavkyně	plavkyně	k1gFnSc1	plavkyně
Jana	Jana	k1gFnSc1	Jana
Kločkovová	Kločkovová	k1gFnSc1	Kločkovová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
boxerském	boxerský	k2eAgInSc6d1	boxerský
ringu	ring	k1gInSc6	ring
prosluli	proslout	k5eAaPmAgMnP	proslout
mistři	mistr	k1gMnPc1	mistr
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
váhových	váhový	k2eAgFnPc6d1	váhová
kategoriích	kategorie	k1gFnPc6	kategorie
Vitalij	Vitalij	k1gFnSc2	Vitalij
Klyčko	Klyčko	k6eAd1	Klyčko
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
Volodymyr	Volodymyr	k1gMnSc1	Volodymyr
Klyčko	Klyčko	k6eAd1	Klyčko
<g/>
;	;	kIx,	;
Vitalij	Vitalij	k1gFnSc1	Vitalij
Klyčko	Klyčko	k6eAd1	Klyčko
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
politikem	politik	k1gMnSc7	politik
<g/>
,	,	kIx,	,
starostou	starosta	k1gMnSc7	starosta
Kyjeva	Kyjev	k1gInSc2	Kyjev
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
–	–	k?	–
Nový	nový	k2eAgInSc1d1	nový
rok	rok	k1gInSc1	rok
7	[number]	k4	7
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
–	–	k?	–
"	"	kIx"	"
<g/>
Rizdvo	Rizdvo	k1gNnSc1	Rizdvo
<g/>
"	"	kIx"	"
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
Vánoční	vánoční	k2eAgInPc1d1	vánoční
svátky	svátek	k1gInPc1	svátek
8	[number]	k4	8
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
–	–	k?	–
Mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
den	den	k1gInSc4	den
žen	žena	k1gFnPc2	žena
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
–	–	k?	–
Svátek	svátek	k1gInSc1	svátek
práce	práce	k1gFnSc1	práce
8	[number]	k4	8
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
–	–	k?	–
Den	den	k1gInSc1	den
vítězství	vítězství	k1gNnSc1	vítězství
28	[number]	k4	28
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
–	–	k?	–
Den	den	k1gInSc1	den
Ústavy	ústava	k1gFnSc2	ústava
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
24	[number]	k4	24
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
–	–	k?	–
Den	den	k1gInSc1	den
Nezávislosti	nezávislost	k1gFnSc2	nezávislost
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
V	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
prostředí	prostředí	k1gNnSc6	prostředí
panuje	panovat	k5eAaImIp3nS	panovat
nedostatek	nedostatek	k1gInSc1	nedostatek
kvalitních	kvalitní	k2eAgFnPc2d1	kvalitní
knih	kniha	k1gFnPc2	kniha
o	o	k7c6	o
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
dáno	dát	k5eAaPmNgNnS	dát
jak	jak	k6eAd1	jak
tendenčností	tendenčnost	k1gFnSc7	tendenčnost
literatury	literatura	k1gFnSc2	literatura
z	z	k7c2	z
období	období	k1gNnSc2	období
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
malým	malý	k2eAgInSc7d1	malý
zájmem	zájem	k1gInSc7	zájem
o	o	k7c4	o
Ukrajinu	Ukrajina	k1gFnSc4	Ukrajina
(	(	kIx(	(
<g/>
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
či	či	k8xC	či
Polskem	Polsko	k1gNnSc7	Polsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k8xC	jak
české	český	k2eAgFnPc1d1	Česká
slavistiky	slavistika	k1gFnPc1	slavistika
<g/>
,	,	kIx,	,
tak	tak	k9	tak
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Jedinou	jediný	k2eAgFnSc7d1	jediná
oblastí	oblast	k1gFnSc7	oblast
<g/>
,	,	kIx,	,
o	o	k7c6	o
níž	jenž	k3xRgFnSc6	jenž
je	být	k5eAaImIp3nS	být
informací	informace	k1gFnPc2	informace
dostatek	dostatek	k1gInSc1	dostatek
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Podkarpatská	podkarpatský	k2eAgFnSc1d1	Podkarpatská
Rus	Rus	k1gFnSc1	Rus
<g/>
;	;	kIx,	;
existují	existovat	k5eAaImIp3nP	existovat
jak	jak	k6eAd1	jak
turistické	turistický	k2eAgMnPc4d1	turistický
průvodce	průvodce	k1gMnPc4	průvodce
<g/>
,	,	kIx,	,
tak	tak	k9	tak
historické	historický	k2eAgFnSc2d1	historická
a	a	k8xC	a
folkloristické	folkloristický	k2eAgFnSc2d1	folkloristická
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Podkarpatské	podkarpatský	k2eAgFnSc2d1	Podkarpatská
Rusi	Rus	k1gFnSc2	Rus
se	se	k3xPyFc4	se
věnovalo	věnovat	k5eAaImAgNnS	věnovat
několik	několik	k4yIc1	několik
autorů	autor	k1gMnPc2	autor
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
v	v	k7c6	v
době	doba	k1gFnSc6	doba
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgFnPc7	dva
světovými	světový	k2eAgFnPc7d1	světová
válkami	válka	k1gFnPc7	válka
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Ivan	Ivan	k1gMnSc1	Ivan
Olbracht	Olbracht	k1gMnSc1	Olbracht
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
Aleksijevičová	Aleksijevičová	k1gFnSc1	Aleksijevičová
<g/>
:	:	kIx,	:
Modlitba	modlitba	k1gFnSc1	modlitba
za	za	k7c4	za
Černobyl	Černobyl	k1gInSc4	Černobyl
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Doplněk	doplněk	k1gInSc1	doplněk
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
230	[number]	k4	230
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7239	[number]	k4	7239
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
82	[number]	k4	82
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
H.	H.	kA	H.
Boczkowski	Boczkowski	k1gNnSc1	Boczkowski
<g/>
:	:	kIx,	:
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
a	a	k8xC	a
ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
otázka	otázka	k1gFnSc1	otázka
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Svaz	svaz	k1gInSc1	svaz
pro	pro	k7c4	pro
osvobození	osvobození	k1gNnSc4	osvobození
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
,	,	kIx,	,
1915	[number]	k4	1915
<g/>
.	.	kIx.	.
</s>
<s>
A.	A.	kA	A.
Evans	Evans	k1gInSc1	Evans
<g/>
:	:	kIx,	:
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
[	[	kIx(	[
<g/>
turistický	turistický	k2eAgMnSc1d1	turistický
průvodce	průvodce	k1gMnSc1	průvodce
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Přel	přít	k5eAaImAgMnS	přít
<g/>
.	.	kIx.	.
</s>
<s>
R.	R.	kA	R.
Beneš	Beneš	k1gMnSc1	Beneš
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Jota	jota	k1gNnSc1	jota
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
368	[number]	k4	368
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7217	[number]	k4	7217
<g/>
-	-	kIx~	-
<g/>
509	[number]	k4	509
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
V.	V.	kA	V.
Goněc	Goněc	k1gFnSc1	Goněc
–	–	k?	–
O.	O.	kA	O.
Bojko	Bojko	k1gNnSc4	Bojko
<g/>
:	:	kIx,	:
Nejnovější	nový	k2eAgFnPc1d3	nejnovější
dějiny	dějiny	k1gFnPc1	dějiny
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Jota	jota	k1gNnSc1	jota
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
268	[number]	k4	268
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7217	[number]	k4	7217
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
31	[number]	k4	31
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
I.	I.	kA	I.
Grekov	Grekov	k1gInSc1	Grekov
<g/>
:	:	kIx,	:
Opětné	opětný	k2eAgNnSc1d1	opětné
sjednocení	sjednocení	k1gNnSc1	sjednocení
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
roku	rok	k1gInSc2	rok
1654	[number]	k4	1654
[	[	kIx(	[
<g/>
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
oficiální	oficiální	k2eAgFnSc2d1	oficiální
sovětské	sovětský	k2eAgFnSc2d1	sovětská
historiografie	historiografie	k1gFnSc2	historiografie
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
SPNL	SPNL	kA	SPNL
<g/>
,	,	kIx,	,
1954	[number]	k4	1954
<g/>
.	.	kIx.	.
</s>
<s>
V.	V.	kA	V.
Hostička	Hostička	k1gFnSc1	Hostička
<g/>
:	:	kIx,	:
Spolupráce	spolupráce	k1gFnSc1	spolupráce
Čechů	Čech	k1gMnPc2	Čech
a	a	k8xC	a
haličských	haličský	k2eAgMnPc2d1	haličský
Ukrajinců	Ukrajinec	k1gMnPc2	Ukrajinec
v	v	k7c6	v
letech	let	k1gInPc6	let
1848	[number]	k4	1848
<g/>
–	–	k?	–
<g/>
1849	[number]	k4	1849
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Československá	československý	k2eAgFnSc1d1	Československá
akademie	akademie	k1gFnSc1	akademie
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
1965	[number]	k4	1965
<g/>
.	.	kIx.	.
</s>
<s>
M.	M.	kA	M.
Hruševskyj	Hruševskyj	k1gFnSc1	Hruševskyj
<g/>
:	:	kIx,	:
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
a	a	k8xC	a
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Kyjiv	Kyjiv	k6eAd1	Kyjiv
–	–	k?	–
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1919	[number]	k4	1919
<g/>
.	.	kIx.	.
</s>
<s>
Ján	Ján	k1gMnSc1	Ján
Mlynárik	Mlynárik	k1gMnSc1	Mlynárik
<g/>
:	:	kIx,	:
Osud	osud	k1gInSc1	osud
banderovců	banderovec	k1gMnPc2	banderovec
a	a	k8xC	a
tragédie	tragédie	k1gFnSc2	tragédie
řeckokatolické	řeckokatolický	k2eAgFnSc2d1	řeckokatolická
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
95	[number]	k4	95
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
204	[number]	k4	204
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
J.	J.	kA	J.
Nečas	Nečas	k1gMnSc1	Nečas
<g/>
:	:	kIx,	:
Východoevropská	východoevropský	k2eAgFnSc1d1	východoevropská
tragédie	tragédie	k1gFnSc1	tragédie
a	a	k8xC	a
Ukrajinci	Ukrajinec	k1gMnPc1	Ukrajinec
<g/>
.	.	kIx.	.
</s>
<s>
Životné	životný	k2eAgFnPc1d1	životná
síly	síla	k1gFnPc1	síla
nové	nový	k2eAgFnSc2d1	nová
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Kyjiv	Kyjiv	k6eAd1	Kyjiv
–	–	k?	–
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1919	[number]	k4	1919
<g/>
.	.	kIx.	.
</s>
<s>
M.	M.	kA	M.
Syruček	Syruček	k1gInSc1	Syruček
<g/>
:	:	kIx,	:
Banderovci	banderovec	k1gMnPc1	banderovec
–	–	k?	–
hrdinové	hrdina	k1gMnPc1	hrdina
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
bandité	bandita	k1gMnPc1	bandita
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Epocha	epocha	k1gFnSc1	epocha
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
168	[number]	k4	168
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
87027	[number]	k4	87027
<g/>
-	-	kIx~	-
<g/>
85	[number]	k4	85
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
V.	V.	kA	V.
Veber	Veber	k1gMnSc1	Veber
(	(	kIx(	(
<g/>
ed	ed	k?	ed
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Ruská	ruský	k2eAgFnSc1d1	ruská
a	a	k8xC	a
ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
emigrace	emigrace	k1gFnSc1	emigrace
v	v	k7c6	v
ČSR	ČSR	kA	ČSR
v	v	k7c6	v
letech	let	k1gInPc6	let
1918	[number]	k4	1918
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Sborník	sborník	k1gInSc1	sborník
studií	studio	k1gNnPc2	studio
<g/>
.	.	kIx.	.
</s>
<s>
Sv.	sv.	kA	sv.
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Univerzita	univerzita	k1gFnSc1	univerzita
Karlova	Karlův	k2eAgFnSc1d1	Karlova
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
–	–	k?	–
<g/>
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
V.	V.	kA	V.
Veber	Veber	k1gMnSc1	Veber
(	(	kIx(	(
<g/>
ed	ed	k?	ed
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
historiografii	historiografie	k1gFnSc6	historiografie
–	–	k?	–
k	k	k7c3	k
70	[number]	k4	70
<g/>
.	.	kIx.	.
narozeninám	narozeniny	k1gFnPc3	narozeniny
Vladimíra	Vladimír	k1gMnSc4	Vladimír
Hostičky	Hostička	k1gFnSc2	Hostička
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Národní	národní	k2eAgFnSc1d1	národní
knihovna	knihovna	k1gFnSc1	knihovna
ČR	ČR	kA	ČR
–	–	k?	–
Slovanská	slovanský	k2eAgFnSc1d1	Slovanská
knihovna	knihovna	k1gFnSc1	knihovna
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
168	[number]	k4	168
s.	s.	k?	s.
T.	T.	kA	T.
Zahradníček	Zahradníček	k1gMnSc1	Zahradníček
<g/>
:	:	kIx,	:
Jak	jak	k8xS	jak
vyhrát	vyhrát	k5eAaPmF	vyhrát
cizí	cizí	k2eAgFnSc4d1	cizí
válku	válka	k1gFnSc4	válka
<g/>
.	.	kIx.	.
</s>
<s>
Češi	Čech	k1gMnPc1	Čech
<g/>
,	,	kIx,	,
Poláci	Polák	k1gMnPc1	Polák
a	a	k8xC	a
Ukrajinci	Ukrajinec	k1gMnPc1	Ukrajinec
<g/>
,	,	kIx,	,
1914	[number]	k4	1914
<g/>
–	–	k?	–
<g/>
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
ISV	ISV	kA	ISV
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
250	[number]	k4	250
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85866	[number]	k4	85866
<g/>
-	-	kIx~	-
<g/>
65	[number]	k4	65
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
B.	B.	kA	B.
Zilynskyj	Zilynskyj	k1gFnSc1	Zilynskyj
<g/>
:	:	kIx,	:
Ukrajinci	Ukrajinec	k1gMnPc1	Ukrajinec
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
:	:	kIx,	:
stručný	stručný	k2eAgInSc4d1	stručný
nástin	nástin	k1gInSc4	nástin
dějin	dějiny	k1gFnPc2	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Sdružení	sdružení	k1gNnSc1	sdružení
Čechů	Čech	k1gMnPc2	Čech
z	z	k7c2	z
Volyně	Volyně	k1gFnSc2	Volyně
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc2	jejich
přátel	přítel	k1gMnPc2	přítel
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
76	[number]	k4	76
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80-903164-1-7	[number]	k4	80-903164-1-7
B.	B.	kA	B.
Zilynskyj	Zilynskyj	k1gFnSc1	Zilynskyj
<g/>
:	:	kIx,	:
Ukrajinci	Ukrajinec	k1gMnPc1	Ukrajinec
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
v	v	k7c6	v
letech	let	k1gInPc6	let
1945	[number]	k4	1945
<g/>
–	–	k?	–
<g/>
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ústav	ústav	k1gInSc1	ústav
pro	pro	k7c4	pro
soudobé	soudobý	k2eAgFnPc4d1	soudobá
dějiny	dějiny	k1gFnPc4	dějiny
AV	AV	kA	AV
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
97	[number]	k4	97
s.	s.	k?	s.
Pomsta	pomsta	k1gFnSc1	pomsta
Oleksy	Oleksa	k1gFnSc2	Oleksa
Dovbuše	Dovbuše	k1gFnSc2	Dovbuše
[	[	kIx(	[
<g/>
Ukrajinské	ukrajinský	k2eAgInPc1d1	ukrajinský
mýty	mýtus	k1gInPc1	mýtus
<g/>
,	,	kIx,	,
pohádky	pohádka	k1gFnPc1	pohádka
a	a	k8xC	a
pověsti	pověst	k1gFnPc1	pověst
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-7203-619-X	[number]	k4	80-7203-619-X
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
zpívá	zpívat	k5eAaImIp3nS	zpívat
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Svět	svět	k1gInSc1	svět
sovětů	sovět	k1gInPc2	sovět
<g/>
,	,	kIx,	,
1951	[number]	k4	1951
<g/>
.	.	kIx.	.
412	[number]	k4	412
s.	s.	k?	s.
Kozácké	kozácký	k2eAgFnSc2d1	kozácká
dumy	duma	k1gFnSc2	duma
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Svět	svět	k1gInSc1	svět
sovětů	sovět	k1gInPc2	sovět
<g/>
,	,	kIx,	,
1952	[number]	k4	1952
<g/>
.	.	kIx.	.
148	[number]	k4	148
s.	s.	k?	s.
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgFnP	být
použity	použit	k2eAgFnPc1d1	použita
informace	informace	k1gFnPc1	informace
z	z	k7c2	z
Velké	velký	k2eAgFnSc2d1	velká
sovětské	sovětský	k2eAgFnSc2d1	sovětská
encyklopedie	encyklopedie	k1gFnSc2	encyklopedie
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
"	"	kIx"	"
<g/>
У	У	k?	У
с	с	k?	с
с	с	k?	с
р	р	k?	р
<g/>
,	,	kIx,	,
В	В	k?	В
в	в	k?	в
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Kategorie	kategorie	k1gFnSc2	kategorie
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
ve	v	k7c6	v
Wikizprávách	Wikizpráva	k1gFnPc6	Wikizpráva
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
na	na	k7c4	na
OpenStreetMap	OpenStreetMap	k1gInSc4	OpenStreetMap
Přehled	přehled	k1gInSc4	přehled
ukrajinské	ukrajinský	k2eAgFnSc2d1	ukrajinská
krize	krize	k1gFnSc2	krize
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
(	(	kIx(	(
<g/>
Kyjev	Kyjev	k1gInSc1	Kyjev
<g/>
,	,	kIx,	,
Krym	Krym	k1gInSc1	Krym
<g/>
)	)	kIx)	)
na	na	k7c6	na
webu	web	k1gInSc6	web
EurActiv	EurActiva	k1gFnPc2	EurActiva
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
(	(	kIx(	(
<g/>
ukrajinsky	ukrajinsky	k6eAd1	ukrajinsky
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Stránky	stránka	k1gFnSc2	stránka
prezidenta	prezident	k1gMnSc2	prezident
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
(	(	kIx(	(
<g/>
ukrajinsky	ukrajinsky	k6eAd1	ukrajinsky
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Portál	portál	k1gInSc1	portál
ukrajinské	ukrajinský	k2eAgFnSc2d1	ukrajinská
vlády	vláda	k1gFnSc2	vláda
(	(	kIx(	(
<g/>
ukrajinsky	ukrajinsky	k6eAd1	ukrajinsky
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Verchovna	Verchovna	k1gFnSc1	Verchovna
rada	rada	k1gFnSc1	rada
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
stránky	stránka	k1gFnPc4	stránka
parlamentu	parlament	k1gInSc2	parlament
(	(	kIx(	(
<g/>
ukrajinsky	ukrajinsky	k6eAd1	ukrajinsky
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Stránky	stránka	k1gFnPc4	stránka
Ukrajinské	ukrajinský	k2eAgFnSc2d1	ukrajinská
národní	národní	k2eAgFnSc2d1	národní
banky	banka	k1gFnSc2	banka
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Anglický	anglický	k2eAgInSc1d1	anglický
text	text	k1gInSc1	text
Ústavy	ústava	k1gFnSc2	ústava
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
–	–	k?	–
satelitní	satelitní	k2eAgFnPc4d1	satelitní
mapy	mapa	k1gFnPc4	mapa
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Dnešní	dnešní	k2eAgFnSc1d1	dnešní
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Internet	Internet	k1gInSc1	Internet
Encyclopedia	Encyclopedium	k1gNnSc2	Encyclopedium
of	of	k?	of
Ukraine	Ukrain	k1gInSc5	Ukrain
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
5	[number]	k4	5
<g/>
]	]	kIx)	]
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
–	–	k?	–
Z	z	k7c2	z
dějin	dějiny	k1gFnPc2	dějiny
vzájemných	vzájemný	k2eAgInPc2d1	vzájemný
vztahů	vztah	k1gInPc2	vztah
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2012	[number]	k4	2012
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Krize	krize	k1gFnSc1	krize
březen	březen	k1gInSc1	březen
2014	[number]	k4	2014
–	–	k?	–
rozhovor	rozhovor	k1gInSc1	rozhovor
s	s	k7c7	s
prezidentem	prezident	k1gMnSc7	prezident
RF	RF	kA	RF
V.	V.	kA	V.
Putinem	Putin	k1gInSc7	Putin
k	k	k7c3	k
událostem	událost	k1gFnPc3	událost
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
7	[number]	k4	7
<g/>
]	]	kIx)	]
Ukraine	Ukrain	k1gInSc5	Ukrain
–	–	k?	–
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc1	International
Report	report	k1gInSc1	report
2011	[number]	k4	2011
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gMnPc2	International
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
22	[number]	k4	22
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Ukraine	Ukrain	k1gInSc5	Ukrain
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Freedom	Freedom	k1gInSc4	Freedom
House	house	k1gNnSc1	house
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
22	[number]	k4	22
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
–	–	k?	–
o	o	k7c6	o
českých	český	k2eAgMnPc6d1	český
krajanech	krajan	k1gMnPc6	krajan
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
25	[number]	k4	25
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
14	[number]	k4	14
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
.	.	kIx.	.
</s>
<s>
BTI	BTI	kA	BTI
2010	[number]	k4	2010
–	–	k?	–
Ukraine	Ukrain	k1gInSc5	Ukrain
Country	country	k2eAgInPc2d1	country
Report	report	k1gInSc4	report
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Gütersloh	Gütersloh	k1gMnSc1	Gütersloh
<g/>
:	:	kIx,	:
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
22	[number]	k4	22
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Bureau	Burea	k2eAgFnSc4d1	Burea
of	of	k?	of
European	Europeana	k1gFnPc2	Europeana
and	and	k?	and
Eurasian	Eurasian	k1gInSc1	Eurasian
Affairs	Affairs	k1gInSc1	Affairs
<g/>
.	.	kIx.	.
</s>
<s>
Background	Background	k1gInSc1	Background
Note	Note	k1gNnSc1	Note
<g/>
:	:	kIx,	:
Ukraine	Ukrain	k1gMnSc5	Ukrain
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Department	department	k1gInSc1	department
of	of	k?	of
State	status	k1gInSc5	status
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
25	[number]	k4	25
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
22	[number]	k4	22
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
World	World	k1gInSc1	World
Factbook	Factbook	k1gInSc1	Factbook
–	–	k?	–
Ukraine	Ukrain	k1gMnSc5	Ukrain
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
REV	REV	kA	REV
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
22	[number]	k4	22
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Zastupitelský	zastupitelský	k2eAgInSc1d1	zastupitelský
úřad	úřad	k1gInSc1	úřad
ČR	ČR	kA	ČR
v	v	k7c6	v
Kyjevě	Kyjev	k1gInSc6	Kyjev
<g/>
.	.	kIx.	.
</s>
<s>
Souhrnná	souhrnný	k2eAgFnSc1d1	souhrnná
teritoriální	teritoriální	k2eAgFnSc1d1	teritoriální
informace	informace	k1gFnSc1	informace
<g/>
:	:	kIx,	:
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Businessinfo	Businessinfo	k1gNnSc1	Businessinfo
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
22	[number]	k4	22
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
HAJDA	HAJDA	kA	HAJDA
<g/>
,	,	kIx,	,
Lubomyr	Lubomyr	k1gInSc1	Lubomyr
A.	A.	kA	A.
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Ukraine	Ukrainout	k5eAaPmIp3nS	Ukrainout
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Encyclopaedia	Encyclopaedium	k1gNnPc1	Encyclopaedium
Britannica	Britannic	k2eAgNnPc1d1	Britannic
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
22	[number]	k4	22
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
