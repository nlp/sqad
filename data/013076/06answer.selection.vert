<s>
Náleží	náležet	k5eAaImIp3nS	náležet
mezi	mezi	k7c4	mezi
nejcennější	cenný	k2eAgNnPc4d3	nejcennější
dřeva	dřevo	k1gNnPc4	dřevo
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
,	,	kIx,	,
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
sloužilo	sloužit	k5eAaImAgNnS	sloužit
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
slavných	slavný	k2eAgInPc2d1	slavný
velšských	velšský	k2eAgInPc2d1	velšský
luků	luk	k1gInPc2	luk
<g/>
.	.	kIx.	.
</s>
