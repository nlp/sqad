<p>
<s>
Tis	tis	k1gInSc1	tis
červený	červený	k2eAgInSc1d1	červený
(	(	kIx(	(
<g/>
výslovnost	výslovnost	k1gFnSc1	výslovnost
[	[	kIx(	[
<g/>
ťis	ťis	k?	ťis
<g/>
]	]	kIx)	]
<g/>
;	;	kIx,	;
Taxus	Taxus	k1gInSc1	Taxus
baccata	baccat	k1gMnSc2	baccat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
tis	tis	k1gInSc1	tis
obecný	obecný	k2eAgInSc1d1	obecný
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dvoudomá	dvoudomý	k2eAgFnSc1d1	dvoudomá
stálezelená	stálezelený	k2eAgFnSc1d1	stálezelená
jehličnatá	jehličnatý	k2eAgFnSc1d1	jehličnatá
dřevina	dřevina	k1gFnSc1	dřevina
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
tisovitých	tisovití	k1gMnPc2	tisovití
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
stínomilná	stínomilný	k2eAgFnSc1d1	stínomilná
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
pomalu	pomalu	k6eAd1	pomalu
rostoucí	rostoucí	k2eAgNnSc1d1	rostoucí
<g/>
,	,	kIx,	,
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
keře	keř	k1gInSc2	keř
či	či	k8xC	či
relativně	relativně	k6eAd1	relativně
nízkého	nízký	k2eAgInSc2d1	nízký
stromu	strom	k1gInSc2	strom
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
silně	silně	k6eAd1	silně
ohrožený	ohrožený	k2eAgInSc4d1	ohrožený
třetihorní	třetihorní	k2eAgInSc4d1	třetihorní
relikt	relikt	k1gInSc4	relikt
<g/>
,	,	kIx,	,
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
zemí	zem	k1gFnPc2	zem
náleží	náležet	k5eAaImIp3nS	náležet
mezi	mezi	k7c4	mezi
přísně	přísně	k6eAd1	přísně
chráněné	chráněný	k2eAgInPc4d1	chráněný
druhy	druh	k1gInPc4	druh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Má	mít	k5eAaImIp3nS	mít
nádherné	nádherný	k2eAgNnSc1d1	nádherné
husté	hustý	k2eAgNnSc1d1	husté
dřevo	dřevo	k1gNnSc1	dřevo
<g/>
,	,	kIx,	,
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejtěžších	těžký	k2eAgFnPc2d3	nejtěžší
českých	český	k2eAgFnPc2d1	Česká
domácích	domácí	k2eAgFnPc2d1	domácí
dřevin	dřevina	k1gFnPc2	dřevina
<g/>
,	,	kIx,	,
s	s	k7c7	s
širokým	široký	k2eAgNnSc7d1	široké
červenohnědým	červenohnědý	k2eAgNnSc7d1	červenohnědé
jádrem	jádro	k1gNnSc7	jádro
a	a	k8xC	a
úzkou	úzký	k2eAgFnSc7d1	úzká
světlejší	světlý	k2eAgFnSc7d2	světlejší
bělí	běl	k1gFnSc7	běl
<g/>
.	.	kIx.	.
</s>
<s>
Náleží	náležet	k5eAaImIp3nS	náležet
mezi	mezi	k7c4	mezi
nejcennější	cenný	k2eAgNnPc4d3	nejcennější
dřeva	dřevo	k1gNnPc4	dřevo
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
,	,	kIx,	,
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
sloužilo	sloužit	k5eAaImAgNnS	sloužit
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
slavných	slavný	k2eAgInPc2d1	slavný
velšských	velšský	k2eAgInPc2d1	velšský
luků	luk	k1gInPc2	luk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Až	až	k9	až
na	na	k7c4	na
červený	červený	k2eAgInSc4d1	červený
dužnatý	dužnatý	k2eAgInSc4d1	dužnatý
nepravý	pravý	k2eNgInSc4d1	nepravý
míšek	míšek	k1gInSc4	míšek
(	(	kIx(	(
<g/>
epimatium	epimatium	k1gNnSc4	epimatium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
celá	celý	k2eAgFnSc1d1	celá
rostlina	rostlina	k1gFnSc1	rostlina
prudce	prudko	k6eAd1	prudko
jedovatá	jedovatý	k2eAgFnSc1d1	jedovatá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Synonyma	synonymum	k1gNnSc2	synonymum
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Taxus	Taxus	k1gInSc1	Taxus
communis	communis	k1gFnSc2	communis
Senilis	Senilis	k1gFnSc2	Senilis
<g/>
,	,	kIx,	,
1866	[number]	k4	1866
</s>
</p>
<p>
<s>
Taxus	Taxus	k1gMnSc1	Taxus
baccata	baccat	k1gMnSc2	baccat
subsp	subsp	k1gMnSc1	subsp
<g/>
.	.	kIx.	.
communis	communis	k1gFnSc1	communis
(	(	kIx(	(
<g/>
Senilis	Senilis	k1gFnSc1	Senilis
<g/>
)	)	kIx)	)
A.	A.	kA	A.
&	&	k?	&
Gr	Gr	k1gMnSc1	Gr
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
1912	[number]	k4	1912
</s>
</p>
<p>
<s>
Taxus	Taxus	k1gMnSc1	Taxus
baccata	baccat	k1gMnSc2	baccat
subsp	subsp	k1gMnSc1	subsp
<g/>
.	.	kIx.	.
eubaccata	eubaccata	k1gFnSc1	eubaccata
Pilger	Pilgra	k1gFnPc2	Pilgra
<g/>
,	,	kIx,	,
1916	[number]	k4	1916
</s>
</p>
<p>
<s>
Taxus	Taxus	k1gInSc1	Taxus
lugubris	lugubris	k1gFnSc2	lugubris
Salisbury	Salisbura	k1gFnSc2	Salisbura
<g/>
,	,	kIx,	,
1796	[number]	k4	1796
(	(	kIx(	(
<g/>
nom.	nom.	k?	nom.
illeg	illeg	k1gInSc1	illeg
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Taxus	Taxus	k1gMnSc1	Taxus
vulgaris	vulgaris	k1gFnSc2	vulgaris
Borckh	Borckh	k1gMnSc1	Borckh
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
1800	[number]	k4	1800
(	(	kIx(	(
<g/>
nom.	nom.	k?	nom.
illeg	illeg	k1gInSc1	illeg
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Vzhled	vzhled	k1gInSc4	vzhled
==	==	k?	==
</s>
</p>
<p>
<s>
Tis	tis	k1gInSc1	tis
může	moct	k5eAaImIp3nS	moct
dorůst	dorůst	k5eAaPmF	dorůst
výšky	výška	k1gFnSc2	výška
až	až	k9	až
20	[number]	k4	20
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
šířky	šířka	k1gFnPc1	šířka
17	[number]	k4	17
m.	m.	k?	m.
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
rostoucí	rostoucí	k2eAgMnPc1d1	rostoucí
jedinci	jedinec	k1gMnPc1	jedinec
pocházející	pocházející	k2eAgMnPc1d1	pocházející
ze	z	k7c2	z
semene	semeno	k1gNnSc2	semeno
mají	mít	k5eAaImIp3nP	mít
vzrůst	vzrůst	k1gInSc1	vzrůst
stromovitý	stromovitý	k2eAgInSc1d1	stromovitý
<g/>
,	,	kIx,	,
při	při	k7c6	při
vegetativním	vegetativní	k2eAgNnSc6d1	vegetativní
množení	množení	k1gNnSc6	množení
nabývá	nabývat	k5eAaImIp3nS	nabývat
tis	tis	k1gInSc1	tis
charakteru	charakter	k1gInSc2	charakter
keře	keř	k1gInSc2	keř
<g/>
.	.	kIx.	.
</s>
<s>
Roste	růst	k5eAaImIp3nS	růst
velmi	velmi	k6eAd1	velmi
pomalu	pomalu	k6eAd1	pomalu
–	–	k?	–
letokruhy	letokruh	k1gInPc1	letokruh
jsou	být	k5eAaImIp3nP	být
nahloučené	nahloučený	k2eAgInPc1d1	nahloučený
a	a	k8xC	a
i	i	k9	i
velmi	velmi	k6eAd1	velmi
staré	starý	k2eAgInPc1d1	starý
stromy	strom	k1gInPc1	strom
si	se	k3xPyFc3	se
zachovávají	zachovávat	k5eAaImIp3nP	zachovávat
neobvyklou	obvyklý	k2eNgFnSc4d1	neobvyklá
štíhlost	štíhlost	k1gFnSc4	štíhlost
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
velice	velice	k6eAd1	velice
dlouhověký	dlouhověký	k2eAgInSc4d1	dlouhověký
strom	strom	k1gInSc4	strom
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
známi	znám	k2eAgMnPc1d1	znám
jedinci	jedinec	k1gMnPc1	jedinec
staří	starý	k2eAgMnPc1d1	starý
přes	přes	k7c4	přes
tisíc	tisíc	k4xCgInSc4	tisíc
let	let	k1gInSc4	let
<g/>
;	;	kIx,	;
určování	určování	k1gNnSc2	určování
věku	věk	k1gInSc2	věk
tisů	tis	k1gInPc2	tis
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
složité	složitý	k2eAgNnSc1d1	složité
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
srůstajícím	srůstající	k2eAgInPc3d1	srůstající
kmenům	kmen	k1gInPc3	kmen
a	a	k8xC	a
vyhníváním	vyhnívání	k1gNnSc7	vyhnívání
jejich	jejich	k3xOp3gInSc2	jejich
vnitřku	vnitřek	k1gInSc2	vnitřek
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
hluboko	hluboko	k6eAd1	hluboko
kořenící	kořenící	k2eAgFnSc7d1	kořenící
dřevinou	dřevina	k1gFnSc7	dřevina
se	s	k7c7	s
srdčitým	srdčitý	k2eAgInSc7d1	srdčitý
<g/>
,	,	kIx,	,
všestranně	všestranně	k6eAd1	všestranně
rozvinutým	rozvinutý	k2eAgInSc7d1	rozvinutý
kořenovým	kořenový	k2eAgInSc7d1	kořenový
systémem	systém	k1gInSc7	systém
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
tis	tis	k1gInSc1	tis
dobře	dobře	k6eAd1	dobře
ukotvuje	ukotvovat	k5eAaImIp3nS	ukotvovat
i	i	k9	i
na	na	k7c6	na
skalnatých	skalnatý	k2eAgFnPc6d1	skalnatá
a	a	k8xC	a
suťových	suťový	k2eAgNnPc6d1	suťové
stanovištích	stanoviště	k1gNnPc6	stanoviště
<g/>
.	.	kIx.	.
<g/>
Z	z	k7c2	z
našich	náš	k3xOp1gInPc2	náš
jehličnanů	jehličnan	k1gInPc2	jehličnan
má	mít	k5eAaImIp3nS	mít
nejtmavší	tmavý	k2eAgNnSc4d3	nejtmavší
zabarvení	zabarvení	k1gNnSc4	zabarvení
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
2	[number]	k4	2
mm	mm	kA	mm
široké	široký	k2eAgFnSc2d1	široká
a	a	k8xC	a
20	[number]	k4	20
<g/>
–	–	k?	–
<g/>
30	[number]	k4	30
mm	mm	kA	mm
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
špičaté	špičatý	k2eAgInPc4d1	špičatý
a	a	k8xC	a
měkké	měkký	k2eAgFnPc4d1	měkká
jehlice	jehlice	k1gFnPc4	jehlice
mají	mít	k5eAaImIp3nP	mít
leskle	leskle	k6eAd1	leskle
tmavozelenou	tmavozelený	k2eAgFnSc4d1	tmavozelená
svrchní	svrchní	k2eAgFnSc4d1	svrchní
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
spodní	spodní	k2eAgFnSc1d1	spodní
strana	strana	k1gFnSc1	strana
je	být	k5eAaImIp3nS	být
světlejší	světlý	k2eAgFnSc1d2	světlejší
<g/>
,	,	kIx,	,
matně	matně	k6eAd1	matně
žlutozelená	žlutozelený	k2eAgFnSc1d1	žlutozelená
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
svislých	svislý	k2eAgInPc6d1	svislý
výhonech	výhon	k1gInPc6	výhon
obrůstají	obrůstat	k5eAaImIp3nP	obrůstat
větvičku	větvička	k1gFnSc4	větvička
dokola	dokola	k6eAd1	dokola
<g/>
,	,	kIx,	,
na	na	k7c6	na
bočních	boční	k2eAgFnPc6d1	boční
a	a	k8xC	a
vodorovných	vodorovný	k2eAgFnPc6d1	vodorovná
jsou	být	k5eAaImIp3nP	být
dvouřadě	dvouřadě	k6eAd1	dvouřadě
uspořádané	uspořádaný	k2eAgFnPc1d1	uspořádaná
<g/>
,	,	kIx,	,
na	na	k7c6	na
stromě	strom	k1gInSc6	strom
vytrvávají	vytrvávat	k5eAaImIp3nP	vytrvávat
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
8	[number]	k4	8
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
dřevo	dřevo	k1gNnSc4	dřevo
tisu	tis	k1gInSc2	tis
postrádají	postrádat	k5eAaImIp3nP	postrádat
pryskyřičné	pryskyřičný	k2eAgFnPc1d1	pryskyřičná
kanálky	kanálka	k1gFnPc1	kanálka
<g/>
.	.	kIx.	.
</s>
<s>
Kůra	kůra	k1gFnSc1	kůra
je	být	k5eAaImIp3nS	být
zpočátku	zpočátku	k6eAd1	zpočátku
červenohnědá	červenohnědý	k2eAgFnSc1d1	červenohnědá
a	a	k8xC	a
hladká	hladký	k2eAgFnSc1d1	hladká
<g/>
,	,	kIx,	,
posléze	posléze	k6eAd1	posléze
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
na	na	k7c4	na
červenohnědošedou	červenohnědošedý	k2eAgFnSc4d1	červenohnědošedý
a	a	k8xC	a
odlupuje	odlupovat	k5eAaImIp3nS	odlupovat
se	se	k3xPyFc4	se
v	v	k7c6	v
plátech	plát	k1gInPc6	plát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kulovité	kulovitý	k2eAgFnPc1d1	kulovitá
samčí	samčí	k2eAgFnPc1d1	samčí
šištice	šištice	k1gFnPc1	šištice
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
na	na	k7c6	na
spodu	spod	k1gInSc6	spod
loňských	loňský	k2eAgFnPc2d1	loňská
větviček	větvička	k1gFnPc2	větvička
<g/>
,	,	kIx,	,
samičí	samičí	k2eAgInPc1d1	samičí
jsou	být	k5eAaImIp3nP	být
značně	značně	k6eAd1	značně
redukované	redukovaný	k2eAgInPc1d1	redukovaný
<g/>
,	,	kIx,	,
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
pouze	pouze	k6eAd1	pouze
jedno	jeden	k4xCgNnSc1	jeden
vajíčko	vajíčko	k1gNnSc1	vajíčko
podepřené	podepřený	k2eAgFnSc2d1	podepřená
třemi	tři	k4xCgInPc7	tři
páry	pár	k1gInPc7	pár
křižmostojných	křižmostojný	k2eAgInPc2d1	křižmostojný
listenů	listen	k1gInPc2	listen
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
rozšiřování	rozšiřování	k1gNnSc6	rozšiřování
semen	semeno	k1gNnPc2	semeno
se	se	k3xPyFc4	se
starají	starat	k5eAaImIp3nP	starat
ptáci	pták	k1gMnPc1	pták
a	a	k8xC	a
savci	savec	k1gMnPc1	savec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
konzumují	konzumovat	k5eAaBmIp3nP	konzumovat
dužnaté	dužnatý	k2eAgInPc4d1	dužnatý
míšky	míšek	k1gInPc4	míšek
<g/>
.	.	kIx.	.
</s>
<s>
Semeno	semeno	k1gNnSc1	semeno
před	před	k7c7	před
vyklíčením	vyklíčení	k1gNnSc7	vyklíčení
obvykle	obvykle	k6eAd1	obvykle
2-4	[number]	k4	2-4
roky	rok	k1gInPc4	rok
přeléhá	přeléhat	k5eAaImIp3nS	přeléhat
<g/>
.	.	kIx.	.
</s>
<s>
Klíčí	klíčit	k5eAaImIp3nS	klíčit
dvěma	dva	k4xCgFnPc7	dva
dělohami	děloha	k1gFnPc7	děloha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Záměny	záměna	k1gFnSc2	záměna
===	===	k?	===
</s>
</p>
<p>
<s>
Jediný	jediný	k2eAgInSc1d1	jediný
druh	druh	k1gInSc1	druh
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterého	který	k3yQgNnSc2	který
na	na	k7c4	na
území	území	k1gNnSc4	území
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
připadá	připadat	k5eAaImIp3nS	připadat
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
záměna	záměna	k1gFnSc1	záměna
s	s	k7c7	s
tisem	tis	k1gInSc7	tis
červeným	červený	k2eAgInSc7d1	červený
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
našich	náš	k3xOp1gInPc6	náš
parcích	park	k1gInPc6	park
často	často	k6eAd1	často
vysazovaný	vysazovaný	k2eAgMnSc1d1	vysazovaný
příbuzný	příbuzný	k1gMnSc1	příbuzný
tis	tis	k1gInSc4	tis
prostřední	prostřední	k2eAgInSc4d1	prostřední
(	(	kIx(	(
<g/>
Taxus	Taxus	k1gInSc4	Taxus
×	×	k?	×
media	medium	k1gNnSc2	medium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tis	tis	k1gInSc1	tis
prostřední	prostřední	k2eAgInSc1d1	prostřední
je	být	k5eAaImIp3nS	být
křížencem	kříženec	k1gMnSc7	kříženec
tisu	tis	k1gInSc2	tis
červeného	červený	k2eAgInSc2d1	červený
s	s	k7c7	s
tisem	tis	k1gInSc7	tis
japonským	japonský	k2eAgInSc7d1	japonský
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
rozlišovací	rozlišovací	k2eAgInSc1d1	rozlišovací
znak	znak	k1gInSc1	znak
nejlépe	dobře	k6eAd3	dobře
poslouží	posloužit	k5eAaPmIp3nS	posloužit
právě	právě	k9	právě
výše	vysoce	k6eAd2	vysoce
zmíněná	zmíněný	k2eAgFnSc1d1	zmíněná
kůra	kůra	k1gFnSc1	kůra
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
kůra	kůra	k1gFnSc1	kůra
tisu	tis	k1gInSc2	tis
červeného	červený	k2eAgInSc2d1	červený
postupně	postupně	k6eAd1	postupně
přechází	přecházet	k5eAaImIp3nS	přecházet
na	na	k7c4	na
červenohnědošedočernou	červenohnědošedočerný	k2eAgFnSc4d1	červenohnědošedočerný
a	a	k8xC	a
odlupuje	odlupovat	k5eAaImIp3nS	odlupovat
se	se	k3xPyFc4	se
v	v	k7c6	v
plátech	plát	k1gInPc6	plát
<g/>
,	,	kIx,	,
kůra	kůra	k1gFnSc1	kůra
tisu	tis	k1gInSc2	tis
prostředního	prostřední	k2eAgInSc2d1	prostřední
s	s	k7c7	s
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
tmavne	tmavnout	k5eAaImIp3nS	tmavnout
a	a	k8xC	a
rýhovitě	rýhovitě	k6eAd1	rýhovitě
praská	praskat	k5eAaImIp3nS	praskat
<g/>
.	.	kIx.	.
</s>
<s>
Staré	Staré	k2eAgFnPc1d1	Staré
větve	větev	k1gFnPc1	větev
pak	pak	k6eAd1	pak
mají	mít	k5eAaImIp3nP	mít
kůru	kůra	k1gFnSc4	kůra
více	hodně	k6eAd2	hodně
či	či	k8xC	či
méně	málo	k6eAd2	málo
černou	černý	k2eAgFnSc7d1	černá
(	(	kIx(	(
<g/>
projevuje	projevovat	k5eAaImIp3nS	projevovat
se	se	k3xPyFc4	se
však	však	k9	však
i	i	k9	i
u	u	k7c2	u
nich	on	k3xPp3gMnPc2	on
v	v	k7c6	v
různé	různý	k2eAgFnSc6d1	různá
míře	míra	k1gFnSc6	míra
rodičovský	rodičovský	k2eAgInSc4d1	rodičovský
vliv	vliv	k1gInSc4	vliv
tisu	tis	k1gInSc2	tis
červeného	červený	k2eAgInSc2d1	červený
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Druhy	druh	k1gInPc1	druh
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
liší	lišit	k5eAaImIp3nP	lišit
několika	několik	k4yIc7	několik
drobnostmi	drobnost	k1gFnPc7	drobnost
<g/>
,	,	kIx,	,
např.	např.	kA	např.
jiným	jiný	k2eAgNnSc7d1	jiné
zakončením	zakončení	k1gNnSc7	zakončení
jehlic	jehlice	k1gFnPc2	jehlice
(	(	kIx(	(
<g/>
pozvolně	pozvolně	k6eAd1	pozvolně
se	s	k7c7	s
zužující	zužující	k2eAgFnSc7d1	zužující
u	u	k7c2	u
tisu	tis	k1gInSc2	tis
červeného	červený	k2eAgInSc2d1	červený
<g/>
,	,	kIx,	,
náhle	náhle	k6eAd1	náhle
zašpičatělé	zašpičatělý	k2eAgInPc1d1	zašpičatělý
u	u	k7c2	u
tisu	tis	k1gInSc2	tis
prostředního	prostřední	k2eAgInSc2d1	prostřední
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
celkově	celkově	k6eAd1	celkově
je	být	k5eAaImIp3nS	být
však	však	k9	však
tis	tis	k1gInSc1	tis
prostřední	prostřednět	k5eAaImIp3nS	prostřednět
jinak	jinak	k6eAd1	jinak
dost	dost	k6eAd1	dost
podobný	podobný	k2eAgInSc1d1	podobný
svému	svůj	k3xOyFgInSc3	svůj
rodičovskému	rodičovský	k2eAgInSc3d1	rodičovský
druhu	druh	k1gInSc3	druh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
a	a	k8xC	a
ekologie	ekologie	k1gFnSc1	ekologie
==	==	k?	==
</s>
</p>
<p>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
suťových	suťový	k2eAgInPc6d1	suťový
lesích	les	k1gInPc6	les
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
od	od	k7c2	od
Středomoří	středomoří	k1gNnSc2	středomoří
až	až	k9	až
po	po	k7c4	po
jižní	jižní	k2eAgNnSc4d1	jižní
Švédsko	Švédsko	k1gNnSc4	Švédsko
a	a	k8xC	a
Norsko	Norsko	k1gNnSc4	Norsko
<g/>
,	,	kIx,	,
od	od	k7c2	od
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
až	až	k9	až
po	po	k7c4	po
Litvu	Litva	k1gFnSc4	Litva
<g/>
,	,	kIx,	,
Lotyšsko	Lotyšsko	k1gNnSc4	Lotyšsko
a	a	k8xC	a
Estonsko	Estonsko	k1gNnSc4	Estonsko
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
v	v	k7c6	v
Malé	Malé	k2eAgFnSc6d1	Malé
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
v	v	k7c6	v
Zakavkazí	Zakavkazí	k1gNnSc6	Zakavkazí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
největší	veliký	k2eAgMnPc1d3	veliký
a	a	k8xC	a
nejstarší	starý	k2eAgMnPc1d3	nejstarší
jedinci	jedinec	k1gMnPc1	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dřevinou	dřevina	k1gFnSc7	dřevina
mírného	mírný	k2eAgNnSc2d1	mírné
oceanického	oceanický	k2eAgNnSc2d1	oceanické
klimatu	klima	k1gNnSc2	klima
<g/>
,	,	kIx,	,
vyhovují	vyhovovat	k5eAaImIp3nP	vyhovovat
mu	on	k3xPp3gMnSc3	on
vlhčí	vlhký	k2eAgNnSc1d2	vlhčí
<g/>
,	,	kIx,	,
stinná	stinný	k2eAgFnSc1d1	stinná
až	až	k9	až
polostinná	polostinný	k2eAgNnPc4d1	polostinné
stanoviště	stanoviště	k1gNnPc4	stanoviště
s	s	k7c7	s
hlubší	hluboký	k2eAgFnSc7d2	hlubší
živnou	živný	k2eAgFnSc7d1	živná
půdou	půda	k1gFnSc7	půda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
dostatečně	dostatečně	k6eAd1	dostatečně
vlhká	vlhký	k2eAgFnSc1d1	vlhká
a	a	k8xC	a
provzdušněná	provzdušněný	k2eAgFnSc1d1	provzdušněná
<g/>
.	.	kIx.	.
</s>
<s>
Podloží	podloží	k1gNnSc1	podloží
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vápencové	vápencový	k2eAgNnSc1d1	vápencové
i	i	k8xC	i
silikátové	silikátový	k2eAgNnSc1d1	silikátové
<g/>
.	.	kIx.	.
</s>
<s>
Nesnáší	snášet	k5eNaImIp3nS	snášet
přílišné	přílišný	k2eAgNnSc1d1	přílišné
sucho	sucho	k1gNnSc1	sucho
ani	ani	k8xC	ani
zamokření	zamokření	k1gNnSc1	zamokření
a	a	k8xC	a
kontinentální	kontinentální	k2eAgNnSc1d1	kontinentální
klima	klima	k1gNnSc1	klima
s	s	k7c7	s
letními	letní	k2eAgNnPc7d1	letní
vedry	vedro	k1gNnPc7	vedro
a	a	k8xC	a
mrazivými	mrazivý	k2eAgFnPc7d1	mrazivá
zimami	zima	k1gFnPc7	zima
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lesích	les	k1gInPc6	les
nevytváří	vytvářit	k5eNaPmIp3nS	vytvářit
souvislé	souvislý	k2eAgInPc4d1	souvislý
porosty	porost	k1gInPc4	porost
<g/>
,	,	kIx,	,
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
jako	jako	k9	jako
příměs	příměs	k1gFnSc1	příměs
ve	v	k7c6	v
spodních	spodní	k2eAgNnPc6d1	spodní
patrech	patro	k1gNnPc6	patro
porostu	porůst	k5eAaPmIp1nS	porůst
<g/>
.	.	kIx.	.
<g/>
Ačkoli	ačkoli	k8xS	ačkoli
jde	jít	k5eAaImIp3nS	jít
již	již	k6eAd1	již
o	o	k7c4	o
druh	druh	k1gInSc4	druh
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
roztroušený	roztroušený	k2eAgInSc1d1	roztroušený
až	až	k8xS	až
vzácný	vzácný	k2eAgMnSc1d1	vzácný
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
domácí	domácí	k2eAgFnSc4d1	domácí
složku	složka	k1gFnSc4	složka
naší	náš	k3xOp1gFnSc2	náš
květeny	květena	k1gFnSc2	květena
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
však	však	k9	však
třeba	třeba	k6eAd1	třeba
dodat	dodat	k5eAaPmF	dodat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gNnSc1	jeho
recentní	recentní	k2eAgNnSc1d1	recentní
rozšíření	rozšíření	k1gNnSc1	rozšíření
úzce	úzko	k6eAd1	úzko
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
přítomností	přítomnost	k1gFnSc7	přítomnost
člověka	člověk	k1gMnSc2	člověk
–	–	k?	–
tis	tis	k1gInSc1	tis
má	mít	k5eAaImIp3nS	mít
pro	pro	k7c4	pro
evropské	evropský	k2eAgNnSc4d1	Evropské
bájesloví	bájesloví	k1gNnSc4	bájesloví
(	(	kIx(	(
<g/>
od	od	k7c2	od
antického	antický	k2eAgInSc2d1	antický
přes	přes	k7c4	přes
keltské	keltský	k2eAgInPc4d1	keltský
až	až	k9	až
po	po	k7c4	po
křesťanské	křesťanský	k2eAgFnPc4d1	křesťanská
<g/>
)	)	kIx)	)
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
význam	význam	k1gInSc4	význam
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
svých	svůj	k3xOyFgFnPc6	svůj
nejvýznamnějších	významný	k2eAgFnPc6d3	nejvýznamnější
lokalitách	lokalita	k1gFnPc6	lokalita
–	–	k?	–
na	na	k7c6	na
Křivoklátsku	Křivoklátsek	k1gInSc6	Křivoklátsek
u	u	k7c2	u
Týřova	Týřov	k1gInSc2	Týřov
<g/>
,	,	kIx,	,
v	v	k7c6	v
Povltaví	Povltaví	k1gNnSc6	Povltaví
u	u	k7c2	u
Štěchovic	Štěchovice	k1gFnPc2	Štěchovice
a	a	k8xC	a
v	v	k7c6	v
Moravském	moravský	k2eAgInSc6d1	moravský
krasu	kras	k1gInSc6	kras
–	–	k?	–
byl	být	k5eAaImAgInS	být
vysazen	vysadit	k5eAaPmNgInS	vysadit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
býval	bývat	k5eAaImAgInS	bývat
o	o	k7c6	o
poznání	poznání	k1gNnSc6	poznání
hojnější	hojný	k2eAgMnSc1d2	hojnější
<g/>
,	,	kIx,	,
s	s	k7c7	s
těžištěm	těžiště	k1gNnSc7	těžiště
v	v	k7c6	v
termofytiku	termofytikum	k1gNnSc6	termofytikum
<g/>
,	,	kIx,	,
ve	v	k7c6	v
spodním	spodní	k2eAgNnSc6d1	spodní
patru	patro	k1gNnSc6	patro
listnatých	listnatý	k2eAgInPc2d1	listnatý
a	a	k8xC	a
smíšených	smíšený	k2eAgInPc2d1	smíšený
lesů	les	k1gInPc2	les
<g/>
,	,	kIx,	,
na	na	k7c6	na
sutích	suť	k1gFnPc6	suť
a	a	k8xC	a
skalnatých	skalnatý	k2eAgInPc6d1	skalnatý
svazích	svah	k1gInPc6	svah
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
400	[number]	k4	400
<g/>
–	–	k?	–
<g/>
1000	[number]	k4	1000
m	m	kA	m
např.	např.	kA	např.
na	na	k7c6	na
Šumavě	Šumava	k1gFnSc6	Šumava
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
silně	silně	k6eAd1	silně
ohrožený	ohrožený	k2eAgInSc4d1	ohrožený
druh	druh	k1gInSc4	druh
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
přísně	přísně	k6eAd1	přísně
chráněn	chránit	k5eAaImNgInS	chránit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejbohatších	bohatý	k2eAgFnPc2d3	nejbohatší
lokalit	lokalita	k1gFnPc2	lokalita
na	na	k7c4	na
výskyt	výskyt	k1gInSc4	výskyt
tisu	tis	k1gInSc2	tis
červeného	červený	k2eAgInSc2d1	červený
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
je	být	k5eAaImIp3nS	být
přírodní	přírodní	k2eAgFnSc1d1	přírodní
rezervace	rezervace	k1gFnSc1	rezervace
V	v	k7c6	v
Horách	hora	k1gFnPc6	hora
u	u	k7c2	u
vsi	ves	k1gFnSc2	ves
Terešovská	Terešovská	k1gFnSc1	Terešovská
Huť	huť	k1gFnSc1	huť
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
sloučená	sloučený	k2eAgFnSc1d1	sloučená
s	s	k7c7	s
Terešovem	Terešovo	k1gNnSc7	Terešovo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
až	až	k9	až
3400	[number]	k4	3400
jedinců	jedinec	k1gMnPc2	jedinec
a	a	k8xC	a
majitelé	majitel	k1gMnPc1	majitel
lesa	les	k1gInSc2	les
chrání	chránit	k5eAaImIp3nP	chránit
toto	tento	k3xDgNnSc4	tento
území	území	k1gNnSc4	území
již	již	k9	již
od	od	k7c2	od
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
významnou	významný	k2eAgFnSc7d1	významná
lokalitou	lokalita	k1gFnSc7	lokalita
je	být	k5eAaImIp3nS	být
přírodní	přírodní	k2eAgFnSc1d1	přírodní
památka	památka	k1gFnSc1	památka
Jílovské	jílovský	k2eAgInPc1d1	jílovský
tisy	tis	k1gInPc4	tis
u	u	k7c2	u
Jílového	Jílové	k1gNnSc2	Jílové
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Děčín	Děčín	k1gInSc1	Děčín
<g/>
,	,	kIx,	,
národní	národní	k2eAgFnSc2d1	národní
přírodní	přírodní	k2eAgFnSc2d1	přírodní
rezervace	rezervace	k1gFnSc2	rezervace
Chlumská	chlumský	k2eAgFnSc1d1	Chlumská
stráň	stráň	k1gFnSc1	stráň
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Rokycany	Rokycany	k1gInPc1	Rokycany
<g/>
,	,	kIx,	,
přírodní	přírodní	k2eAgFnPc1d1	přírodní
rezervace	rezervace	k1gFnPc1	rezervace
Drbákov-Albertovy	Drbákov-Albertův	k2eAgFnSc2d1	Drbákov-Albertův
skály	skála	k1gFnSc2	skála
ve	v	k7c6	v
Středním	střední	k2eAgNnSc6d1	střední
Povltaví	Povltaví	k1gNnSc6	Povltaví
či	či	k8xC	či
přírodní	přírodní	k2eAgFnSc1d1	přírodní
památka	památka	k1gFnSc1	památka
Tisy	Tisa	k1gFnSc2	Tisa
u	u	k7c2	u
Chrobol	Chrobola	k1gFnPc2	Chrobola
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Prachatice	Prachatice	k1gFnPc1	Prachatice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Hřebečovském	Hřebečovský	k2eAgInSc6d1	Hřebečovský
hřbetu	hřbet	k1gInSc6	hřbet
(	(	kIx(	(
<g/>
okres	okres	k1gInSc1	okres
Svitavy	Svitava	k1gFnSc2	Svitava
<g/>
)	)	kIx)	)
na	na	k7c6	na
jihovýchodních	jihovýchodní	k2eAgInPc6d1	jihovýchodní
svazích	svah	k1gInPc6	svah
Červené	Červené	k2eAgFnSc2d1	Červené
hory	hora	k1gFnSc2	hora
(	(	kIx(	(
<g/>
606	[number]	k4	606
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
přírodní	přírodní	k2eAgFnSc1d1	přírodní
památka	památka	k1gFnSc1	památka
Pod	pod	k7c7	pod
Skálou	skála	k1gFnSc7	skála
(	(	kIx(	(
<g/>
Mladějovské	Mladějovský	k2eAgInPc1d1	Mladějovský
tisy	tis	k1gInPc1	tis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
přírodně	přírodně	k6eAd1	přírodně
rostoucích	rostoucí	k2eAgInPc2d1	rostoucí
exemplářů	exemplář	k1gInPc2	exemplář
tisu	tis	k1gInSc2	tis
červeného	červený	k2eAgInSc2d1	červený
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
pozornost	pozornost	k1gFnSc4	pozornost
stojí	stát	k5eAaImIp3nS	stát
i	i	k9	i
jeho	jeho	k3xOp3gInSc1	jeho
hojný	hojný	k2eAgInSc1d1	hojný
výskyt	výskyt	k1gInSc1	výskyt
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Velké	velký	k2eAgFnSc2d1	velká
Fatry	Fatra	k1gFnSc2	Fatra
<g/>
,	,	kIx,	,
především	především	k9	především
v	v	k7c6	v
národní	národní	k2eAgFnSc6d1	národní
přírodní	přírodní	k2eAgFnSc6d1	přírodní
rezervaci	rezervace	k1gFnSc6	rezervace
Harmanecká	Harmanecký	k2eAgFnSc1d1	Harmanecká
tisina	tisina	k1gFnSc1	tisina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
až	až	k9	až
160	[number]	k4	160
000	[number]	k4	000
tisů	tis	k1gInPc2	tis
(	(	kIx(	(
<g/>
největší	veliký	k2eAgInSc1d3	veliký
výskyt	výskyt	k1gInSc1	výskyt
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obsahové	obsahový	k2eAgFnPc1d1	obsahová
látky	látka	k1gFnPc1	látka
==	==	k?	==
</s>
</p>
<p>
<s>
Celá	celý	k2eAgFnSc1d1	celá
rostlina	rostlina	k1gFnSc1	rostlina
je	být	k5eAaImIp3nS	být
až	až	k9	až
na	na	k7c4	na
sladce	sladko	k6eAd1	sladko
chutnající	chutnající	k2eAgInSc4d1	chutnající
červený	červený	k2eAgInSc4d1	červený
dužnatý	dužnatý	k2eAgInSc4d1	dužnatý
obal	obal	k1gInSc4	obal
okolo	okolo	k7c2	okolo
semene	semeno	k1gNnSc2	semeno
(	(	kIx(	(
<g/>
nepravý	pravý	k2eNgInSc1d1	nepravý
míšek	míšek	k1gInSc1	míšek
<g/>
,	,	kIx,	,
dozrávající	dozrávající	k2eAgFnSc1d1	dozrávající
v	v	k7c6	v
září	září	k1gNnSc6	září
až	až	k9	až
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
prudce	prudko	k6eAd1	prudko
jedovatá	jedovatý	k2eAgFnSc1d1	jedovatá
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
jedem	jed	k1gInSc7	jed
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
účinná	účinný	k2eAgFnSc1d1	účinná
směs	směs	k1gFnSc1	směs
alkaloidů	alkaloid	k1gInPc2	alkaloid
<g/>
,	,	kIx,	,
souhrnně	souhrnně	k6eAd1	souhrnně
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
taxin	taxin	k1gInSc1	taxin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Alkoholymyricylalkohol	Alkoholymyricylalkohol	k1gInSc1	Alkoholymyricylalkohol
</s>
</p>
<p>
<s>
Karboxylové	karboxylový	k2eAgFnPc1d1	karboxylová
kyseliny	kyselina	k1gFnPc1	kyselina
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
solikyselina	solikyselin	k2eAgFnSc1d1	solikyselin
gallová	gallová	k1gFnSc1	gallová
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
jablečnan	jablečnan	k1gInSc1	jablečnan
vápenatý	vápenatý	k2eAgInSc1d1	vápenatý
</s>
</p>
<p>
<s>
Sacharidysacharosa	Sacharidysacharosa	k1gFnSc1	Sacharidysacharosa
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
rafinosa	rafinosa	k1gFnSc1	rafinosa
</s>
</p>
<p>
<s>
Alkaloidytaxin	Alkaloidytaxin	k1gMnSc1	Alkaloidytaxin
A	a	k9	a
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
taxin	taxin	k1gInSc1	taxin
B	B	kA	B
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
milosin	milosin	k1gMnSc1	milosin
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
lykopin	lykopin	k1gMnSc1	lykopin
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
efedrin	efedrin	k1gInSc1	efedrin
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
baccatin	baccatina	k1gFnPc2	baccatina
III	III	kA	III
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
10	[number]	k4	10
<g/>
-deacetylbaccatin	eacetylbaccatina	k1gFnPc2	-deacetylbaccatina
III	III	kA	III
</s>
</p>
<p>
<s>
Steroidysitosterin	Steroidysitosterin	k1gInSc1	Steroidysitosterin
</s>
</p>
<p>
<s>
==	==	k?	==
Otravy	otrava	k1gFnSc2	otrava
==	==	k?	==
</s>
</p>
<p>
<s>
Zralý	zralý	k2eAgInSc4d1	zralý
nepravý	pravý	k2eNgInSc4d1	nepravý
míšek	míšek	k1gInSc4	míšek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
sladký	sladký	k2eAgInSc1d1	sladký
a	a	k8xC	a
červený	červený	k2eAgInSc1d1	červený
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
jedovatý	jedovatý	k2eAgMnSc1d1	jedovatý
<g/>
.	.	kIx.	.
</s>
<s>
Jedovaté	jedovatý	k2eAgNnSc1d1	jedovaté
semeno	semeno	k1gNnSc1	semeno
se	se	k3xPyFc4	se
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
vyplivnout	vyplivnout	k5eAaPmF	vyplivnout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ani	ani	k8xC	ani
při	při	k7c6	při
jeho	jeho	k3xOp3gNnSc6	jeho
pozření	pozření	k1gNnSc6	pozření
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
nemělo	mít	k5eNaImAgNnS	mít
nic	nic	k3yNnSc1	nic
stát	stát	k5eAaImF	stát
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	on	k3xPp3gFnPc4	on
člověk	člověk	k1gMnSc1	člověk
nerozkouše	rozkousat	k5eNaPmIp3nS	rozkousat
<g/>
.	.	kIx.	.
</s>
<s>
Jed	jed	k1gInSc1	jed
se	se	k3xPyFc4	se
snadno	snadno	k6eAd1	snadno
vstřebává	vstřebávat	k5eAaImIp3nS	vstřebávat
trávicím	trávicí	k2eAgNnSc7d1	trávicí
ústrojím	ústrojí	k1gNnSc7	ústrojí
a	a	k8xC	a
působí	působit	k5eAaImIp3nP	působit
velice	velice	k6eAd1	velice
rychle	rychle	k6eAd1	rychle
(	(	kIx(	(
<g/>
výplach	výplach	k1gInSc1	výplach
žaludku	žaludek	k1gInSc2	žaludek
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
co	co	k9	co
nejrychleji	rychle	k6eAd3	rychle
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
zpomaluje	zpomalovat	k5eAaImIp3nS	zpomalovat
srdeční	srdeční	k2eAgFnSc4d1	srdeční
činnost	činnost	k1gFnSc4	činnost
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
míry	míra	k1gFnSc2	míra
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
způsobí	způsobit	k5eAaPmIp3nS	způsobit
jeho	jeho	k3xOp3gFnSc4	jeho
zástavu	zástava	k1gFnSc4	zástava
<g/>
.	.	kIx.	.
</s>
<s>
Smrt	smrt	k1gFnSc1	smrt
může	moct	k5eAaImIp3nS	moct
nastat	nastat	k5eAaPmF	nastat
v	v	k7c6	v
řádu	řád	k1gInSc6	řád
desítek	desítka	k1gFnPc2	desítka
minut	minuta	k1gFnPc2	minuta
po	po	k7c6	po
požití	požití	k1gNnSc6	požití
rostliny	rostlina	k1gFnSc2	rostlina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zaznamenány	zaznamenat	k5eAaPmNgFnP	zaznamenat
jsou	být	k5eAaImIp3nP	být
smrtelné	smrtelný	k2eAgFnPc1d1	smrtelná
otravy	otrava	k1gFnPc1	otrava
po	po	k7c6	po
čaji	čaj	k1gInSc6	čaj
z	z	k7c2	z
jehličí	jehličí	k1gNnSc2	jehličí
tisu	tis	k1gInSc2	tis
nebo	nebo	k8xC	nebo
po	po	k7c6	po
žvýkání	žvýkání	k1gNnSc6	žvýkání
jeho	jeho	k3xOp3gFnPc2	jeho
větviček	větvička	k1gFnPc2	větvička
či	či	k8xC	či
jehličí	jehličí	k1gNnSc2	jehličí
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
otrávit	otrávit	k5eAaPmF	otrávit
odvarem	odvar	k1gInSc7	odvar
ze	z	k7c2	z
semen	semeno	k1gNnPc2	semeno
<g/>
.	.	kIx.	.
<g/>
Jed	jed	k1gInSc1	jed
rostliny	rostlina	k1gFnSc2	rostlina
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
nebezpečný	bezpečný	k2eNgMnSc1d1	nebezpečný
i	i	k9	i
pro	pro	k7c4	pro
nezkušená	zkušený	k2eNgNnPc4d1	nezkušené
či	či	k8xC	či
zdomácnělá	zdomácnělý	k2eAgNnPc4d1	zdomácnělé
zvířata	zvíře	k1gNnPc4	zvíře
(	(	kIx(	(
<g/>
kůň	kůň	k1gMnSc1	kůň
<g/>
,	,	kIx,	,
skot	skot	k1gInSc1	skot
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
někdy	někdy	k6eAd1	někdy
umírají	umírat	k5eAaImIp3nP	umírat
ještě	ještě	k9	ještě
dřív	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
stihnou	stihnout	k5eAaPmIp3nP	stihnout
své	svůj	k3xOyFgNnSc4	svůj
hodování	hodování	k1gNnSc4	hodování
dokončit	dokončit	k5eAaPmF	dokončit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Dřevo	dřevo	k1gNnSc1	dřevo
===	===	k?	===
</s>
</p>
<p>
<s>
Protože	protože	k8xS	protože
velmi	velmi	k6eAd1	velmi
pomalu	pomalu	k6eAd1	pomalu
roste	růst	k5eAaImIp3nS	růst
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
husté	hustý	k2eAgNnSc1d1	husté
dřevo	dřevo	k1gNnSc1	dřevo
–	–	k?	–
nejhustší	hustý	k2eAgMnSc1d3	nejhustší
<g/>
,	,	kIx,	,
a	a	k8xC	a
tedy	tedy	k9	tedy
i	i	k9	i
nejtěžší	těžký	k2eAgMnSc1d3	nejtěžší
z	z	k7c2	z
našich	náš	k3xOp1gFnPc2	náš
domácích	domácí	k2eAgFnPc2d1	domácí
dřevin	dřevina	k1gFnPc2	dřevina
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
krásný	krásný	k2eAgInSc4d1	krásný
vzhled	vzhled	k1gInSc4	vzhled
<g/>
,	,	kIx,	,
široké	široký	k2eAgNnSc4d1	široké
červenohnědé	červenohnědý	k2eAgNnSc4d1	červenohnědé
jádro	jádro	k1gNnSc4	jádro
a	a	k8xC	a
úzkou	úzký	k2eAgFnSc4d1	úzká
světlejší	světlý	k2eAgFnSc4d2	světlejší
běl	běl	k1gFnSc4	běl
<g/>
.	.	kIx.	.
</s>
<s>
Špatně	špatně	k6eAd1	špatně
se	se	k3xPyFc4	se
štípe	štípat	k5eAaImIp3nS	štípat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
snadno	snadno	k6eAd1	snadno
leští	leštit	k5eAaImIp3nS	leštit
<g/>
,	,	kIx,	,
moří	mořit	k5eAaImIp3nS	mořit
a	a	k8xC	a
soustruží	soustružit	k5eAaImIp3nS	soustružit
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
používalo	používat	k5eAaImAgNnS	používat
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
vzácného	vzácný	k2eAgInSc2d1	vzácný
nábytku	nábytek	k1gInSc2	nábytek
a	a	k8xC	a
vyřezávaných	vyřezávaný	k2eAgInPc2d1	vyřezávaný
šperků	šperk	k1gInPc2	šperk
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
jako	jako	k9	jako
téměř	téměř	k6eAd1	téměř
celá	celý	k2eAgFnSc1d1	celá
rostlina	rostlina	k1gFnSc1	rostlina
je	být	k5eAaImIp3nS	být
i	i	k9	i
dřevo	dřevo	k1gNnSc1	dřevo
tisu	tis	k1gInSc2	tis
jedovaté	jedovatý	k2eAgNnSc1d1	jedovaté
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
trvanlivější	trvanlivý	k2eAgMnSc1d2	trvanlivější
než	než	k8xS	než
ostatní	ostatní	k2eAgMnSc1d1	ostatní
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	on	k3xPp3gMnPc4	on
jed	jed	k1gInSc1	jed
chrání	chránit	k5eAaImIp3nS	chránit
před	před	k7c7	před
parazity	parazit	k1gMnPc7	parazit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
dávné	dávný	k2eAgFnSc6d1	dávná
minulosti	minulost	k1gFnSc6	minulost
byl	být	k5eAaImAgInS	být
tis	tis	k1gInSc1	tis
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
velice	velice	k6eAd1	velice
pružnému	pružný	k2eAgNnSc3d1	pružné
dřevu	dřevo	k1gNnSc3	dřevo
používán	používat	k5eAaImNgInS	používat
především	především	k9	především
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
luků	luk	k1gInPc2	luk
a	a	k8xC	a
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
byl	být	k5eAaImAgInS	být
také	také	k9	také
téměř	téměř	k6eAd1	téměř
vyhuben	vyhuben	k2eAgMnSc1d1	vyhuben
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
jedovatý	jedovatý	k2eAgInSc4d1	jedovatý
extrakt	extrakt	k1gInSc4	extrakt
z	z	k7c2	z
tisového	tisový	k2eAgNnSc2d1	Tisové
jehličí	jehličí	k1gNnSc2	jehličí
u	u	k7c2	u
Keltů	Kelt	k1gMnPc2	Kelt
údajně	údajně	k6eAd1	údajně
používal	používat	k5eAaImAgMnS	používat
k	k	k7c3	k
otrávení	otrávení	k1gNnSc3	otrávení
hrotů	hrot	k1gInPc2	hrot
šípů	šíp	k1gInPc2	šíp
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
bodných	bodný	k2eAgFnPc2d1	bodná
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Lékařství	lékařství	k1gNnSc1	lékařství
===	===	k?	===
</s>
</p>
<p>
<s>
Derivát	derivát	k1gInSc1	derivát
paclitaxel	paclitaxel	k1gInSc1	paclitaxel
<g/>
,	,	kIx,	,
vyráběný	vyráběný	k2eAgInSc1d1	vyráběný
polosynteticky	polosynteticky	k6eAd1	polosynteticky
z	z	k7c2	z
baccatinu	baccatina	k1gFnSc4	baccatina
III	III	kA	III
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
jako	jako	k9	jako
cytostatikum	cytostatikum	k1gNnSc4	cytostatikum
u	u	k7c2	u
rakoviny	rakovina	k1gFnSc2	rakovina
vaječníků	vaječník	k1gInPc2	vaječník
a	a	k8xC	a
plic	plíce	k1gFnPc2	plíce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zahradnictví	zahradnictví	k1gNnSc2	zahradnictví
===	===	k?	===
</s>
</p>
<p>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
byl	být	k5eAaImAgInS	být
dříve	dříve	k6eAd2	dříve
hojnou	hojný	k2eAgFnSc7d1	hojná
dřevinou	dřevina	k1gFnSc7	dřevina
našich	náš	k3xOp1gInPc2	náš
smíšených	smíšený	k2eAgInPc2d1	smíšený
lesů	les	k1gInPc2	les
<g/>
,	,	kIx,	,
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
výlučně	výlučně	k6eAd1	výlučně
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
okrasná	okrasný	k2eAgFnSc1d1	okrasná
dřevina	dřevina	k1gFnSc1	dřevina
v	v	k7c6	v
sadech	sad	k1gInPc6	sad
a	a	k8xC	a
zahradách	zahrada	k1gFnPc6	zahrada
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
řadě	řada	k1gFnSc6	řada
různých	různý	k2eAgInPc2d1	různý
kultivarů	kultivar	k1gInPc2	kultivar
<g/>
.	.	kIx.	.
</s>
<s>
Významem	význam	k1gInSc7	význam
jej	on	k3xPp3gNnSc2	on
však	však	k9	však
v	v	k7c6	v
sadovnické	sadovnický	k2eAgFnSc6d1	Sadovnická
tvorbě	tvorba	k1gFnSc6	tvorba
předčil	předčít	k5eAaPmAgMnS	předčít
jeho	jeho	k3xOp3gMnSc1	jeho
kříženec	kříženec	k1gMnSc1	kříženec
Taxus	Taxus	k1gMnSc1	Taxus
×	×	k?	×
media	medium	k1gNnPc4	medium
s	s	k7c7	s
japonským	japonský	k2eAgInSc7d1	japonský
tisem	tis	k1gInSc7	tis
Taxus	Taxus	k1gMnSc1	Taxus
cuspidata	cuspidat	k1gMnSc2	cuspidat
<g/>
.	.	kIx.	.
</s>
<s>
Kříženec	kříženec	k1gMnSc1	kříženec
T.	T.	kA	T.
media	medium	k1gNnSc2	medium
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
kultivarech	kultivar	k1gInPc6	kultivar
značnou	značný	k2eAgFnSc4d1	značná
variabilitu	variabilita	k1gFnSc4	variabilita
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
oblíben	oblíbit	k5eAaPmNgMnS	oblíbit
zejména	zejména	k9	zejména
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
moderních	moderní	k2eAgFnPc6d1	moderní
městských	městský	k2eAgFnPc6d1	městská
výsadbách	výsadba	k1gFnPc6	výsadba
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
tisu	tis	k1gInSc2	tis
červeného	červené	k1gNnSc2	červené
si	se	k3xPyFc3	se
pro	pro	k7c4	pro
sadovnickou	sadovnický	k2eAgFnSc4d1	Sadovnická
tvorbu	tvorba	k1gFnSc4	tvorba
udržely	udržet	k5eAaPmAgFnP	udržet
význam	význam	k1gInSc4	význam
především	především	k6eAd1	především
jeho	jeho	k3xOp3gFnPc1	jeho
mohutnější	mohutný	k2eAgFnPc1d2	mohutnější
formy	forma	k1gFnPc1	forma
(	(	kIx(	(
<g/>
a	a	k8xC	a
i	i	k9	i
některé	některý	k3yIgInPc1	některý
speciální	speciální	k2eAgInPc1d1	speciální
–	–	k?	–
variegované	variegovaný	k2eAgNnSc1d1	variegovaný
a	a	k8xC	a
barevné	barevný	k2eAgNnSc1d1	barevné
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
použitelné	použitelný	k2eAgInPc1d1	použitelný
do	do	k7c2	do
volnějších	volný	k2eAgFnPc2d2	volnější
a	a	k8xC	a
historických	historický	k2eAgFnPc2d1	historická
parkových	parkový	k2eAgFnPc2d1	parková
výsadeb	výsadba	k1gFnPc2	výsadba
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
botanický	botanický	k2eAgInSc1d1	botanický
druh	druh	k1gInSc1	druh
tisu	tis	k1gInSc2	tis
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
nejčastěji	často	k6eAd3	často
právě	právě	k9	právě
v	v	k7c6	v
historických	historický	k2eAgFnPc6d1	historická
zámeckých	zámecký	k2eAgFnPc6d1	zámecká
zahradách	zahrada	k1gFnPc6	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
krásných	krásný	k2eAgMnPc2d1	krásný
<g/>
,	,	kIx,	,
věkovitých	věkovitý	k2eAgInPc2d1	věkovitý
exemplářů	exemplář	k1gInPc2	exemplář
roste	růst	k5eAaImIp3nS	růst
například	například	k6eAd1	například
v	v	k7c6	v
Lednickém	lednický	k2eAgInSc6d1	lednický
parku	park	k1gInSc6	park
a	a	k8xC	a
i	i	k9	i
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
Lednicko-valtickém	lednickoaltický	k2eAgInSc6d1	lednicko-valtický
areálu	areál	k1gInSc6	areál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Charakter	charakter	k1gInSc1	charakter
růstu	růst	k1gInSc2	růst
====	====	k?	====
</s>
</p>
<p>
<s>
Typ	typ	k1gInSc1	typ
růstu	růst	k1gInSc2	růst
tisu	tis	k1gInSc2	tis
je	být	k5eAaImIp3nS	být
významně	významně	k6eAd1	významně
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
byl	být	k5eAaImAgMnS	být
namnožen	namnožit	k5eAaPmNgMnS	namnožit
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
<g/>
-li	i	k?	-li
tisy	tis	k1gInPc4	tis
(	(	kIx(	(
<g/>
botanická	botanický	k2eAgFnSc1d1	botanická
forma	forma	k1gFnSc1	forma
<g/>
)	)	kIx)	)
pěstovány	pěstovat	k5eAaImNgInP	pěstovat
ze	z	k7c2	z
semene	semeno	k1gNnSc2	semeno
<g/>
,	,	kIx,	,
vyrostou	vyrůst	k5eAaPmIp3nP	vyrůst
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
relativně	relativně	k6eAd1	relativně
velké	velký	k2eAgFnPc1d1	velká
<g/>
,	,	kIx,	,
vzpřímeně	vzpřímeně	k6eAd1	vzpřímeně
rostoucí	rostoucí	k2eAgInPc1d1	rostoucí
stromy	strom	k1gInPc1	strom
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
<g/>
-li	i	k?	-li
rozmnožovány	rozmnožován	k2eAgInPc1d1	rozmnožován
vegetativně	vegetativně	k6eAd1	vegetativně
–	–	k?	–
řízkováním	řízkování	k1gNnSc7	řízkování
<g/>
,	,	kIx,	,
dceřiná	dceřiný	k2eAgFnSc1d1	dceřiná
rostlina	rostlina	k1gFnSc1	rostlina
si	se	k3xPyFc3	se
poměrně	poměrně	k6eAd1	poměrně
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
uchovává	uchovávat	k5eAaImIp3nS	uchovávat
typ	typ	k1gInSc1	typ
růstu	růst	k1gInSc2	růst
který	který	k3yIgInSc4	který
měla	mít	k5eAaImAgFnS	mít
původní	původní	k2eAgFnSc1d1	původní
větvička	větvička	k1gFnSc1	větvička
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yIgFnSc2	který
byl	být	k5eAaImAgMnS	být
odebírán	odebírán	k2eAgInSc4d1	odebírán
řízek	řízek	k1gInSc4	řízek
<g/>
.	.	kIx.	.
</s>
<s>
Záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c4	na
pozici	pozice	k1gFnSc4	pozice
v	v	k7c6	v
koruně	koruna	k1gFnSc6	koruna
mateřské	mateřský	k2eAgFnSc2d1	mateřská
rostliny	rostlina	k1gFnSc2	rostlina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
řízek	řízek	k1gInSc1	řízek
odebrán	odebrat	k5eAaPmNgInS	odebrat
<g/>
.	.	kIx.	.
</s>
<s>
Tedy	tedy	k9	tedy
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
<g/>
-li	i	k?	-li
odebrán	odebrán	k2eAgMnSc1d1	odebrán
z	z	k7c2	z
bočních	boční	k2eAgMnPc2d1	boční
<g/>
,	,	kIx,	,
do	do	k7c2	do
strany	strana	k1gFnSc2	strana
rostoucích	rostoucí	k2eAgFnPc2d1	rostoucí
větviček	větvička	k1gFnPc2	větvička
<g/>
,	,	kIx,	,
výsledná	výsledný	k2eAgFnSc1d1	výsledná
rostlina	rostlina	k1gFnSc1	rostlina
bude	být	k5eAaImBp3nS	být
postrádat	postrádat	k5eAaImF	postrádat
terminál	terminál	k1gInSc1	terminál
(	(	kIx(	(
<g/>
růstový	růstový	k2eAgInSc1d1	růstový
vrchol	vrchol	k1gInSc1	vrchol
<g/>
)	)	kIx)	)
a	a	k8xC	a
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
spíše	spíše	k9	spíše
křovitý	křovitý	k2eAgInSc4d1	křovitý
růst	růst	k1gInSc4	růst
s	s	k7c7	s
vodorovnými	vodorovný	k2eAgFnPc7d1	vodorovná
anebo	anebo	k8xC	anebo
šikmými	šikmý	k2eAgFnPc7d1	šikmá
větvemi	větev	k1gFnPc7	větev
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
<g/>
-li	i	k?	-li
řízek	řízek	k1gInSc1	řízek
odebrán	odebrat	k5eAaPmNgInS	odebrat
z	z	k7c2	z
terminálních	terminální	k2eAgMnPc2d1	terminální
–	–	k?	–
vzhůru	vzhůru	k6eAd1	vzhůru
rostoucích	rostoucí	k2eAgInPc2d1	rostoucí
výhonů	výhon	k1gInPc2	výhon
<g/>
,	,	kIx,	,
výsledný	výsledný	k2eAgInSc1d1	výsledný
habitus	habitus	k1gInSc1	habitus
bude	být	k5eAaImBp3nS	být
podobný	podobný	k2eAgInSc1d1	podobný
jako	jako	k9	jako
u	u	k7c2	u
semenáčů	semenáč	k1gInPc2	semenáč
<g/>
,	,	kIx,	,
jen	jen	k9	jen
se	s	k7c7	s
slabším	slabý	k2eAgInSc7d2	slabší
vzrůstem	vzrůst	k1gInSc7	vzrůst
<g/>
.	.	kIx.	.
</s>
<s>
Všeobecně	všeobecně	k6eAd1	všeobecně
ale	ale	k9	ale
platí	platit	k5eAaImIp3nS	platit
(	(	kIx(	(
<g/>
i	i	k8xC	i
u	u	k7c2	u
ostatních	ostatní	k2eAgFnPc2d1	ostatní
vegetativně	vegetativně	k6eAd1	vegetativně
množených	množený	k2eAgFnPc2d1	množená
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
určité	určitý	k2eAgFnSc6d1	určitá
době	doba	k1gFnSc6	doba
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
namnožená	namnožený	k2eAgFnSc1d1	namnožená
rostlina	rostlina	k1gFnSc1	rostlina
stejný	stejný	k2eAgInSc4d1	stejný
habitus	habitus	k1gInSc4	habitus
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
rostlina	rostlina	k1gFnSc1	rostlina
mateřská	mateřský	k2eAgFnSc1d1	mateřská
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
okrasných	okrasný	k2eAgFnPc2d1	okrasná
forem	forma	k1gFnPc2	forma
je	být	k5eAaImIp3nS	být
množení	množení	k1gNnSc1	množení
stonkovými	stonkový	k2eAgInPc7d1	stonkový
řízky	řízek	k1gInPc7	řízek
jediným	jediný	k2eAgMnSc7d1	jediný
možným	možný	k2eAgInSc7d1	možný
způsobem	způsob	k1gInSc7	způsob
množení	množení	k1gNnSc2	množení
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
uchovat	uchovat	k5eAaPmF	uchovat
vlastnosti	vlastnost	k1gFnPc4	vlastnost
mateřské	mateřský	k2eAgFnSc2d1	mateřská
rostliny	rostlina	k1gFnSc2	rostlina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Protože	protože	k8xS	protože
velice	velice	k6eAd1	velice
dobře	dobře	k6eAd1	dobře
regeneruje	regenerovat	k5eAaBmIp3nS	regenerovat
i	i	k9	i
na	na	k7c6	na
starém	starý	k2eAgNnSc6d1	staré
dřevě	dřevo	k1gNnSc6	dřevo
(	(	kIx(	(
<g/>
hluboký	hluboký	k2eAgInSc1d1	hluboký
zmlazovací	zmlazovací	k2eAgInSc1d1	zmlazovací
řez	řez	k1gInSc1	řez
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
se	se	k3xPyFc4	se
hodí	hodit	k5eAaPmIp3nS	hodit
na	na	k7c4	na
stříhané	stříhaný	k2eAgInPc4d1	stříhaný
živé	živý	k2eAgInPc4d1	živý
ploty	plot	k1gInPc4	plot
<g/>
,	,	kIx,	,
pěstované	pěstovaný	k2eAgInPc4d1	pěstovaný
v	v	k7c6	v
zástinu	zástin	k1gInSc6	zástin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Kultivary	kultivar	k1gInPc4	kultivar
====	====	k?	====
</s>
</p>
<p>
<s>
'	'	kIx"	'
<g/>
Fastigata	Fastigata	k1gFnSc1	Fastigata
<g/>
'	'	kIx"	'
–	–	k?	–
pouze	pouze	k6eAd1	pouze
samičí	samičí	k2eAgFnSc1d1	samičí
<g/>
,	,	kIx,	,
vzpřímeně	vzpřímeně	k6eAd1	vzpřímeně
<g/>
,	,	kIx,	,
sloupcovitě	sloupcovitě	k6eAd1	sloupcovitě
rostoucí	rostoucí	k2eAgFnPc1d1	rostoucí
<g/>
;	;	kIx,	;
jehlice	jehlice	k1gFnPc1	jehlice
vždy	vždy	k6eAd1	vždy
radiálně	radiálně	k6eAd1	radiálně
uspořádané	uspořádaný	k2eAgFnPc4d1	uspořádaná
</s>
</p>
<p>
<s>
'	'	kIx"	'
<g/>
Aurea	Aureum	k1gNnSc2	Aureum
<g/>
'	'	kIx"	'
–	–	k?	–
žlutavě	žlutavě	k6eAd1	žlutavě
zbarvené	zbarvený	k2eAgFnPc1d1	zbarvená
jehlice	jehlice	k1gFnPc1	jehlice
</s>
</p>
<p>
<s>
'	'	kIx"	'
<g/>
Adpressa	Adpressa	k1gFnSc1	Adpressa
<g/>
'	'	kIx"	'
–	–	k?	–
krátké	krátký	k2eAgFnSc2d1	krátká
široké	široký	k2eAgFnSc2d1	široká
jehlice	jehlice	k1gFnSc2	jehlice
</s>
</p>
<p>
<s>
'	'	kIx"	'
<g/>
Dovastoniana	Dovastoniana	k1gFnSc1	Dovastoniana
<g/>
'	'	kIx"	'
–	–	k?	–
smuteční	smuteční	k2eAgInSc4d1	smuteční
typ	typ	k1gInSc4	typ
</s>
</p>
<p>
<s>
'	'	kIx"	'
<g/>
Amersfoort	Amersfoort	k1gInSc4	Amersfoort
<g/>
'	'	kIx"	'
–	–	k?	–
drobné	drobný	k2eAgFnPc4d1	drobná
kulaté	kulatý	k2eAgFnPc4d1	kulatá
jehlice	jehlice	k1gFnPc4	jehlice
<g/>
,	,	kIx,	,
vysoce	vysoce	k6eAd1	vysoce
odlupčivá	odlupčivý	k2eAgFnSc1d1	odlupčivá
kůra	kůra	k1gFnSc1	kůra
</s>
</p>
<p>
<s>
==	==	k?	==
Pověsti	pověst	k1gFnSc6	pověst
==	==	k?	==
</s>
</p>
<p>
<s>
Tis	tis	k1gInSc1	tis
je	být	k5eAaImIp3nS	být
opředený	opředený	k2eAgMnSc1d1	opředený
pověstmi	pověst	k1gFnPc7	pověst
<g/>
.	.	kIx.	.
</s>
<s>
Odedávna	odedávna	k6eAd1	odedávna
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
dřevinu	dřevina	k1gFnSc4	dřevina
regenerace	regenerace	k1gFnSc2	regenerace
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
hlavní	hlavní	k2eAgInSc1d1	hlavní
kmen	kmen	k1gInSc1	kmen
zestárne	zestárnout	k5eAaPmIp3nS	zestárnout
<g/>
,	,	kIx,	,
obraší	obrašit	k5eAaPmIp3nS	obrašit
zakrátko	zakrátko	k6eAd1	zakrátko
mladými	mladý	k2eAgInPc7d1	mladý
zelenými	zelený	k2eAgInPc7d1	zelený
výhonky	výhonek	k1gInPc7	výhonek
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
byl	být	k5eAaImAgMnS	být
vždy	vždy	k6eAd1	vždy
vysazovaný	vysazovaný	k2eAgMnSc1d1	vysazovaný
na	na	k7c6	na
hřbitovech	hřbitov	k1gInPc6	hřbitov
a	a	k8xC	a
tradovalo	tradovat	k5eAaImAgNnS	tradovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
kořeny	kořen	k1gInPc1	kořen
těchto	tento	k3xDgInPc2	tento
hřbitovních	hřbitovní	k2eAgInPc2d1	hřbitovní
tisů	tis	k1gInPc2	tis
sahají	sahat	k5eAaImIp3nP	sahat
až	až	k6eAd1	až
k	k	k7c3	k
ústům	ústa	k1gNnPc3	ústa
nebožtíků	nebožtík	k1gMnPc2	nebožtík
a	a	k8xC	a
napájejí	napájet	k5eAaImIp3nP	napájet
je	on	k3xPp3gNnSc4	on
životem	život	k1gInSc7	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Irská	irský	k2eAgFnSc1d1	irská
pověst	pověst	k1gFnSc1	pověst
Noise	Noise	k1gFnSc2	Noise
a	a	k8xC	a
Deire	Deir	k1gInSc5	Deir
zase	zase	k9	zase
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
nenaplněné	naplněný	k2eNgFnSc6d1	nenaplněná
lásce	láska	k1gFnSc6	láska
milenců	milenec	k1gMnPc2	milenec
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnPc3	který
rodiny	rodina	k1gFnPc1	rodina
nepřály	přát	k5eNaImAgFnP	přát
manželské	manželský	k2eAgNnSc4d1	manželské
štěstí	štěstí	k1gNnSc4	štěstí
a	a	k8xC	a
raději	rád	k6eAd2	rád
je	on	k3xPp3gFnPc4	on
zabily	zabít	k5eAaPmAgFnP	zabít
<g/>
,	,	kIx,	,
než	než	k8xS	než
by	by	kYmCp3nP	by
něco	něco	k3yInSc4	něco
takového	takový	k3xDgNnSc2	takový
připustily	připustit	k5eAaPmAgFnP	připustit
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
milenci	milenec	k1gMnPc1	milenec
nemohli	moct	k5eNaImAgMnP	moct
setkat	setkat	k5eAaPmF	setkat
ani	ani	k8xC	ani
v	v	k7c6	v
posmrtném	posmrtný	k2eAgInSc6d1	posmrtný
životě	život	k1gInSc6	život
<g/>
,	,	kIx,	,
probodli	probodnout	k5eAaPmAgMnP	probodnout
jejich	jejich	k3xOp3gNnPc4	jejich
těla	tělo	k1gNnPc4	tělo
nepřející	přející	k2eNgNnPc4d1	nepřející
příbuzní	příbuzný	k1gMnPc1	příbuzný
tisovými	tisový	k2eAgInPc7d1	tisový
kůly	kůl	k1gInPc7	kůl
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
láska	láska	k1gFnSc1	láska
však	však	k9	však
překonala	překonat	k5eAaPmAgFnS	překonat
všechno	všechen	k3xTgNnSc1	všechen
<g/>
.	.	kIx.	.
</s>
<s>
Kůly	kůl	k1gInPc1	kůl
obrašily	obrašit	k5eAaPmAgFnP	obrašit
mladými	mladý	k2eAgFnPc7d1	mladá
větvičkami	větvička	k1gFnPc7	větvička
<g/>
,	,	kIx,	,
vyrostly	vyrůst	k5eAaPmAgFnP	vyrůst
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
krásné	krásný	k2eAgInPc4d1	krásný
a	a	k8xC	a
vysoké	vysoký	k2eAgInPc4d1	vysoký
stromy	strom	k1gInPc4	strom
a	a	k8xC	a
ve	v	k7c6	v
větvích	větev	k1gFnPc6	větev
se	se	k3xPyFc4	se
pevně	pevně	k6eAd1	pevně
propletly	proplést	k5eAaPmAgFnP	proplést
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
skončili	skončit	k5eAaPmAgMnP	skončit
Noise	Noise	k1gFnPc4	Noise
a	a	k8xC	a
Deire	Deir	k1gInSc5	Deir
ve	v	k7c6	v
věčném	věčný	k2eAgNnSc6d1	věčné
objetí	objetí	k1gNnSc6	objetí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Památné	památný	k2eAgInPc4d1	památný
a	a	k8xC	a
významné	významný	k2eAgInPc4d1	významný
tisy	tis	k1gInPc4	tis
červené	červený	k2eAgInPc4d1	červený
==	==	k?	==
</s>
</p>
<p>
<s>
Protože	protože	k8xS	protože
tis	tis	k1gInSc1	tis
v	v	k7c6	v
našich	náš	k3xOp1gFnPc6	náš
podnebných	podnebný	k2eAgFnPc6d1	podnebná
podmínkách	podmínka	k1gFnPc6	podmínka
roste	růst	k5eAaImIp3nS	růst
poněkud	poněkud	k6eAd1	poněkud
rychleji	rychle	k6eAd2	rychle
než	než	k8xS	než
například	například	k6eAd1	například
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
,	,	kIx,	,
býval	bývat	k5eAaImAgInS	bývat
některým	některý	k3yIgInPc3	některý
vzrostlým	vzrostlý	k2eAgInPc3d1	vzrostlý
tisovým	tisový	k2eAgInPc3d1	tisový
stromům	strom	k1gInPc3	strom
přisuzovaný	přisuzovaný	k2eAgInSc4d1	přisuzovaný
vyšší	vysoký	k2eAgInSc4d2	vyšší
věk	věk	k1gInSc4	věk
<g/>
,	,	kIx,	,
než	než	k8xS	než
jakému	jaký	k3yRgNnSc3	jaký
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
výsledky	výsledek	k1gInPc1	výsledek
moderních	moderní	k2eAgInPc2d1	moderní
výzkumů	výzkum	k1gInPc2	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
byl	být	k5eAaImAgInS	být
přisuzován	přisuzován	k2eAgInSc1d1	přisuzován
věk	věk	k1gInSc1	věk
zhruba	zhruba	k6eAd1	zhruba
2000	[number]	k4	2000
let	léto	k1gNnPc2	léto
následujícím	následující	k2eAgInPc3d1	následující
tuzemským	tuzemský	k2eAgInPc3d1	tuzemský
tisům	tis	k1gInPc3	tis
<g/>
:	:	kIx,	:
tis	tis	k1gInSc1	tis
u	u	k7c2	u
Macochy	Macocha	k1gFnSc2	Macocha
(	(	kIx(	(
<g/>
zaniklý	zaniklý	k2eAgMnSc1d1	zaniklý
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
let	léto	k1gNnPc2	léto
podle	podle	k7c2	podle
prof.	prof.	kA	prof.
Kolenatého	kolenatý	k2eAgNnSc2d1	kolenatý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vilémovický	Vilémovický	k2eAgInSc1d1	Vilémovický
tis	tis	k1gInSc1	tis
(	(	kIx(	(
<g/>
1500	[number]	k4	1500
<g/>
-	-	kIx~	-
<g/>
2000	[number]	k4	2000
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tis	tis	k1gInSc1	tis
v	v	k7c6	v
Krompachu	Krompach	k1gInSc6	Krompach
(	(	kIx(	(
<g/>
1800	[number]	k4	1800
let	léto	k1gNnPc2	léto
podle	podle	k7c2	podle
botanika	botanik	k1gMnSc4	botanik
Huga	Hugo	k1gMnSc2	Hugo
Wilhelma	Wilhelm	k1gMnSc2	Wilhelm
Conwentze	Conwentze	k1gFnSc2	Conwentze
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
let	léto	k1gNnPc2	léto
podle	podle	k7c2	podle
starých	starý	k2eAgFnPc2d1	stará
map	mapa	k1gFnPc2	mapa
<g/>
,	,	kIx,	,
pohlednic	pohlednice	k1gFnPc2	pohlednice
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pernštejnský	pernštejnský	k2eAgInSc1d1	pernštejnský
tis	tis	k1gInSc1	tis
(	(	kIx(	(
<g/>
1000	[number]	k4	1000
<g/>
-	-	kIx~	-
<g/>
1500	[number]	k4	1500
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přes	přes	k7c4	přes
15	[number]	k4	15
tisů	tis	k1gInPc2	tis
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
svým	svůj	k3xOyFgInSc7	svůj
obvodem	obvod	k1gInSc7	obvod
300	[number]	k4	300
centimetrů	centimetr	k1gInPc2	centimetr
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
tis	tis	k1gInSc1	tis
v	v	k7c6	v
Krompachu	Krompach	k1gInSc6	Krompach
(	(	kIx(	(
<g/>
462	[number]	k4	462
cm	cm	kA	cm
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pernštejnský	pernštejnský	k2eAgInSc1d1	pernštejnský
tis	tis	k1gInSc1	tis
(	(	kIx(	(
<g/>
461	[number]	k4	461
cm	cm	kA	cm
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tisíciletý	tisíciletý	k2eAgInSc1d1	tisíciletý
tis	tis	k1gInSc1	tis
v	v	k7c6	v
Žilině	Žilina	k1gFnSc6	Žilina
u	u	k7c2	u
Nového	Nového	k2eAgInSc2d1	Nového
Jičína	Jičín	k1gInSc2	Jičín
(	(	kIx(	(
<g/>
400	[number]	k4	400
cm	cm	kA	cm
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vilémovický	Vilémovický	k2eAgInSc1d1	Vilémovický
tis	tis	k1gInSc1	tis
(	(	kIx(	(
<g/>
363	[number]	k4	363
cm	cm	kA	cm
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Koláčkův	Koláčkův	k2eAgInSc1d1	Koláčkův
tis	tis	k1gInSc1	tis
v	v	k7c4	v
Zubří	zubří	k2eAgInPc4d1	zubří
(	(	kIx(	(
<g/>
353	[number]	k4	353
cm	cm	kA	cm
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kamenický	kamenický	k2eAgInSc1d1	kamenický
tis	tis	k1gInSc1	tis
(	(	kIx(	(
<g/>
332	[number]	k4	332
cm	cm	kA	cm
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tis	tis	k1gInSc1	tis
v	v	k7c6	v
Bílé	bílý	k2eAgFnSc6d1	bílá
Vodě	voda	k1gFnSc6	voda
(	(	kIx(	(
<g/>
336	[number]	k4	336
cm	cm	kA	cm
<g/>
,	,	kIx,	,
VZ	VZ	kA	VZ
LČR	LČR	kA	LČR
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ondrův	Ondrův	k2eAgInSc1d1	Ondrův
tis	tis	k1gInSc1	tis
v	v	k7c6	v
Jindřichově	Jindřichův	k2eAgInSc6d1	Jindřichův
(	(	kIx(	(
<g/>
335	[number]	k4	335
cm	cm	kA	cm
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Všechovický	Všechovický	k2eAgInSc1d1	Všechovický
tis	tis	k1gInSc1	tis
(	(	kIx(	(
<g/>
320	[number]	k4	320
cm	cm	kA	cm
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tis	tis	k1gInSc1	tis
v	v	k7c6	v
Bílovci	Bílovec	k1gInSc6	Bílovec
(	(	kIx(	(
<g/>
311	[number]	k4	311
cm	cm	kA	cm
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tis	tis	k1gInSc1	tis
na	na	k7c6	na
louce	louka	k1gFnSc6	louka
(	(	kIx(	(
<g/>
308	[number]	k4	308
cm	cm	kA	cm
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mikulovický	Mikulovický	k2eAgInSc1d1	Mikulovický
tis	tis	k1gInSc1	tis
(	(	kIx(	(
<g/>
305	[number]	k4	305
cm	cm	kA	cm
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tis	tis	k1gInSc1	tis
v	v	k7c6	v
Podzámecké	podzámecký	k2eAgFnSc6d1	Podzámecká
zahradě	zahrada	k1gFnSc6	zahrada
(	(	kIx(	(
<g/>
302	[number]	k4	302
cm	cm	kA	cm
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tis	tis	k1gInSc1	tis
v	v	k7c6	v
Karolince	Karolinka	k1gFnSc6	Karolinka
(	(	kIx(	(
<g/>
301	[number]	k4	301
cm	cm	kA	cm
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Uhřínovský	Uhřínovský	k2eAgInSc1d1	Uhřínovský
tis	tis	k1gInSc1	tis
(	(	kIx(	(
<g/>
300	[number]	k4	300
cm	cm	kA	cm
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
Obvod	obvod	k1gInSc1	obvod
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
200-300	[number]	k4	200-300
cm	cm	kA	cm
je	být	k5eAaImIp3nS	být
běžnější	běžný	k2eAgNnSc1d2	běžnější
<g/>
,	,	kIx,	,
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
ho	on	k3xPp3gMnSc4	on
přes	přes	k7c4	přes
25	[number]	k4	25
stromů	strom	k1gInPc2	strom
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgInPc1d1	další
památné	památný	k2eAgInPc1d1	památný
stromy	strom	k1gInPc1	strom
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
památné	památný	k2eAgInPc4d1	památný
tisy	tis	k1gInPc4	tis
červené	červený	k2eAgInPc4d1	červený
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Květena	květena	k1gFnSc1	květena
ČSR	ČSR	kA	ČSR
<g/>
,	,	kIx,	,
díl	díl	k1gInSc1	díl
1	[number]	k4	1
/	/	kIx~	/
S.	S.	kA	S.
Hejný	Hejný	k1gMnSc1	Hejný
<g/>
,	,	kIx,	,
B.	B.	kA	B.
Slavík	slavík	k1gInSc1	slavík
(	(	kIx(	(
<g/>
Eds	Eds	k1gFnSc1	Eds
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
–	–	k?	–
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
–	–	k?	–
S.	S.	kA	S.
344	[number]	k4	344
<g/>
–	–	k?	–
<g/>
346	[number]	k4	346
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Dubensko	Dubensko	k6eAd1	Dubensko
</s>
</p>
<p>
<s>
V	v	k7c6	v
Horách	hora	k1gFnPc6	hora
</s>
</p>
<p>
<s>
Tisy	Tisa	k1gFnPc1	Tisa
na	na	k7c6	na
Vsetínsku	Vsetínsko	k1gNnSc6	Vsetínsko
</s>
</p>
<p>
<s>
Památné	památný	k2eAgInPc1d1	památný
tisy	tis	k1gInPc1	tis
červené	červený	k2eAgFnSc2d1	červená
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
tis	tis	k1gInSc1	tis
červený	červený	k2eAgInSc1d1	červený
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
tis	tis	k1gInSc1	tis
červený	červený	k2eAgInSc1d1	červený
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Tis	tis	k1gInSc1	tis
–	–	k?	–
strom	strom	k1gInSc1	strom
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
Vyhlášeno	vyhlásit	k5eAaPmNgNnS	vyhlásit
LČR	LČR	kA	LČR
pro	pro	k7c4	pro
rok	rok	k1gInSc4	rok
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tis	tis	k1gInSc1	tis
červený	červený	k2eAgInSc1d1	červený
na	na	k7c4	na
botanika	botanik	k1gMnSc4	botanik
<g/>
.	.	kIx.	.
<g/>
wendys	wendys	k1gInSc1	wendys
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
www.npsumava.cz	www.npsumava.cz	k1gInSc1	www.npsumava.cz
Inventarizace	inventarizace	k1gFnSc2	inventarizace
a	a	k8xC	a
genetická	genetický	k2eAgFnSc1d1	genetická
diverzita	diverzita	k1gFnSc1	diverzita
tisu	tis	k1gInSc2	tis
červeného	červené	k1gNnSc2	červené
ve	v	k7c6	v
ZCHÚ	ZCHÚ	kA	ZCHÚ
ČR	ČR	kA	ČR
jako	jako	k8xS	jako
podklad	podklad	k1gInSc1	podklad
pro	pro	k7c4	pro
záchranná	záchranný	k2eAgNnPc4d1	záchranné
opatření	opatření	k1gNnPc4	opatření
a	a	k8xC	a
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gFnSc4	jeho
reintrodukci	reintrodukce	k1gFnSc4	reintrodukce
</s>
</p>
<p>
<s>
Tis	tis	k1gInSc1	tis
červený	červený	k2eAgInSc1d1	červený
na	na	k7c6	na
biolibu	biolib	k1gInSc6	biolib
</s>
</p>
<p>
<s>
Tis	tis	k1gInSc1	tis
v	v	k7c6	v
sadovnickém	sadovnický	k2eAgNnSc6d1	sadovnické
použití	použití	k1gNnSc6	použití
</s>
</p>
