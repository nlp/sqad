<s>
Rubikova	Rubikův	k2eAgFnSc1d1
kostka	kostka	k1gFnSc1
</s>
<s>
Rubikova	Rubikův	k2eAgFnSc1d1
kostka	kostka	k1gFnSc1
</s>
<s>
Autor	autor	k1gMnSc1
<g/>
:	:	kIx,
</s>
<s>
Ernő	Ernő	k?
Rubik	Rubik	k1gMnSc1
Datum	datum	k1gNnSc1
vydání	vydání	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
1974	#num#	k4
Počet	počet	k1gInSc4
hráčů	hráč	k1gMnPc2
<g/>
:	:	kIx,
</s>
<s>
1	#num#	k4
Doporučený	doporučený	k2eAgInSc1d1
věk	věk	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
od	od	k7c2
6	#num#	k4
let	léto	k1gNnPc2
Tento	tento	k3xDgInSc4
box	box	k1gInSc4
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Rubikova	Rubikův	k2eAgFnSc1d1
kostka	kostka	k1gFnSc1
je	být	k5eAaImIp3nS
mechanický	mechanický	k2eAgInSc1d1
hlavolam	hlavolam	k1gInSc1
<g/>
,	,	kIx,
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
původní	původní	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
tvořený	tvořený	k2eAgInSc4d1
krychlí	krychle	k1gFnSc7
složenou	složený	k2eAgFnSc7d1
z	z	k7c2
dílčích	dílčí	k2eAgFnPc2d1
barevných	barevný	k2eAgFnPc2d1
krychliček	krychlička	k1gFnPc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc7
úkolem	úkol	k1gInSc7
je	být	k5eAaImIp3nS
rotacemi	rotace	k1gFnPc7
přeuspořádat	přeuspořádat	k5eAaPmF
jednotlivé	jednotlivý	k2eAgFnPc4d1
dílčí	dílčí	k2eAgFnPc4d1
části	část	k1gFnPc4
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
každá	každý	k3xTgFnSc1
strana	strana	k1gFnSc1
celého	celý	k2eAgNnSc2d1
tělesa	těleso	k1gNnSc2
byla	být	k5eAaImAgFnS
obarvena	obarvit	k5eAaPmNgFnS
jen	jen	k6eAd1
jednou	jeden	k4xCgFnSc7
barvou	barva	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
atypických	atypický	k2eAgFnPc6d1
variantách	varianta	k1gFnPc6
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
hranoly	hranol	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yIgInPc4,k3yRgInPc4,k3yQgInPc4
nejsou	být	k5eNaImIp3nP
krychlemi	krychle	k1gFnPc7
<g/>
,	,	kIx,
jehlany	jehlan	k1gInPc7
<g/>
,	,	kIx,
mnohostěny	mnohostěn	k1gInPc1
a	a	k8xC
další	další	k2eAgNnPc1d1
tělesa	těleso	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s>
Kostka	kostka	k1gFnSc1
formátu	formát	k1gInSc2
3	#num#	k4
<g/>
×	×	k?
<g/>
3	#num#	k4
<g/>
×	×	k?
<g/>
3	#num#	k4
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
hitem	hit	k1gInSc7
na	na	k7c6
přelomu	přelom	k1gInSc6
70	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
80	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byla	být	k5eAaImAgFnS
vyráběna	vyrábět	k5eAaImNgFnS
v	v	k7c6
milionových	milionový	k2eAgFnPc6d1
sériích	série	k1gFnPc6
a	a	k8xC
stala	stát	k5eAaPmAgFnS
se	se	k3xPyFc4
nejprodávanějším	prodávaný	k2eAgInSc7d3
produktem	produkt	k1gInSc7
na	na	k7c6
Zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Vynalezl	vynaleznout	k5eAaPmAgInS
ji	on	k3xPp3gFnSc4
maďarský	maďarský	k2eAgMnSc1d1
sochař	sochař	k1gMnSc1
a	a	k8xC
architekt	architekt	k1gMnSc1
Ernő	Ernő	k1gMnSc1
Rubik	Rubik	k1gMnSc1
19	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1974	#num#	k4
<g/>
,	,	kIx,
patent	patent	k1gInSc1
podal	podat	k5eAaPmAgInS
30	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1975	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Terminologie	terminologie	k1gFnSc1
</s>
<s>
Kvádrové	kvádrový	k2eAgInPc1d1
modely	model	k1gInPc1
se	se	k3xPyFc4
označují	označovat	k5eAaImIp3nP
primárně	primárně	k6eAd1
třemi	tři	k4xCgInPc7
přirozenými	přirozený	k2eAgInPc7d1
čísly	číslo	k1gNnPc7
oddělenými	oddělený	k2eAgFnPc7d1
znakem	znak	k1gInSc7
„	„	k?
<g/>
×	×	k?
<g/>
“	“	k?
<g/>
,	,	kIx,
kde	kde	k6eAd1
každé	každý	k3xTgNnSc4
z	z	k7c2
čísel	číslo	k1gNnPc2
vyjadřuje	vyjadřovat	k5eAaImIp3nS
počet	počet	k1gInSc1
jednotlivých	jednotlivý	k2eAgFnPc2d1
krychliček	krychlička	k1gFnPc2
na	na	k7c6
jiné	jiný	k2eAgFnSc6d1
straně	strana	k1gFnSc6
kostky	kostka	k1gFnSc2
<g/>
;	;	kIx,
nejsou	být	k5eNaImIp3nP
<g/>
-li	-li	k?
čísla	číslo	k1gNnPc1
stejná	stejný	k2eAgNnPc1d1
<g/>
,	,	kIx,
obvyklejší	obvyklý	k2eAgNnSc1d2
je	být	k5eAaImIp3nS
vzestupné	vzestupný	k2eAgNnSc1d1
řazení	řazení	k1gNnSc1
(	(	kIx(
<g/>
kupř	kupř	kA
<g/>
.	.	kIx.
„	„	k?
<g/>
3	#num#	k4
<g/>
×	×	k?
<g/>
4	#num#	k4
<g/>
×	×	k?
<g/>
4	#num#	k4
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neortodoxní	ortodoxní	k2eNgFnPc4d1
varianty	varianta	k1gFnPc4
nemají	mít	k5eNaImIp3nP
snadno	snadno	k6eAd1
předvídatelné	předvídatelný	k2eAgNnSc4d1
značení	značení	k1gNnSc4
<g/>
;	;	kIx,
zpravidla	zpravidla	k6eAd1
má	mít	k5eAaImIp3nS
každá	každý	k3xTgFnSc1
svůj	svůj	k3xOyFgInSc4
obchodní	obchodní	k2eAgInSc4d1
název	název	k1gInSc4
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc1
vazba	vazba	k1gFnSc1
na	na	k7c4
tvar	tvar	k1gInSc4
kostky	kostka	k1gFnSc2
je	být	k5eAaImIp3nS
pochybná	pochybný	k2eAgFnSc1d1
<g/>
,	,	kIx,
nebo	nebo	k8xC
vůbec	vůbec	k9
žádná	žádný	k3yNgFnSc1
<g/>
.	.	kIx.
</s>
<s>
Pokud	pokud	k8xS
jde	jít	k5eAaImIp3nS
o	o	k7c4
stav	stav	k1gInSc4
kostky	kostka	k1gFnSc2
<g/>
,	,	kIx,
české	český	k2eAgNnSc1d1
názvosloví	názvosloví	k1gNnSc1
je	být	k5eAaImIp3nS
takové	takový	k3xDgNnSc1
<g/>
,	,	kIx,
že	že	k8xS
kostka	kostka	k1gFnSc1
v	v	k7c6
takovém	takový	k3xDgInSc6
uspořádání	uspořádání	k1gNnSc6
<g/>
,	,	kIx,
že	že	k8xS
na	na	k7c6
každé	každý	k3xTgFnSc6
stěně	stěna	k1gFnSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
právě	právě	k9
jedna	jeden	k4xCgFnSc1
barva	barva	k1gFnSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
složená	složený	k2eAgFnSc1d1
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
kostku	kostka	k1gFnSc4
nevyhovující	vyhovující	k2eNgFnSc4d1
této	tento	k3xDgFnSc3
podmínce	podmínka	k1gFnSc3
označujeme	označovat	k5eAaImIp1nP
přívlastkem	přívlastek	k1gInSc7
rozházená	rozházený	k2eAgNnPc5d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proces	proces	k1gInSc4
transformace	transformace	k1gFnSc2
tělesa	těleso	k1gNnSc2
z	z	k7c2
rozházeného	rozházený	k2eAgMnSc2d1
do	do	k7c2
složeného	složený	k2eAgInSc2d1
stavu	stav	k1gInSc2
pak	pak	k6eAd1
nazýváme	nazývat	k5eAaImIp1nP
skládáním	skládání	k1gNnSc7
<g/>
,	,	kIx,
či	či	k8xC
řešením	řešení	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Charakteristika	charakteristika	k1gFnSc1
základní	základní	k2eAgFnSc2d1
verze	verze	k1gFnSc2
</s>
<s>
Nejběžnějším	běžný	k2eAgInSc7d3
typem	typ	k1gInSc7
kostky	kostka	k1gFnSc2
je	být	k5eAaImIp3nS
model	model	k1gInSc1
3	#num#	k4
<g/>
×	×	k?
<g/>
3	#num#	k4
<g/>
×	×	k?
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
má	mít	k5eAaImIp3nS
tři	tři	k4xCgFnPc4
vrstvy	vrstva	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
dohromady	dohromady	k6eAd1
tvoří	tvořit	k5eAaImIp3nP
26	#num#	k4
krychliček	krychlička	k1gFnPc2
—	—	k?
8	#num#	k4
rohů	roh	k1gInPc2
(	(	kIx(
<g/>
corners	cornersit	k5eAaPmRp2nS
<g/>
)	)	kIx)
<g/>
,	,	kIx,
12	#num#	k4
hran	hrana	k1gFnPc2
(	(	kIx(
<g/>
edges	edges	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
6	#num#	k4
středů	střed	k1gInPc2
(	(	kIx(
<g/>
centres	centresa	k1gFnPc2
<g/>
)	)	kIx)
—	—	k?
a	a	k8xC
jádro	jádro	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každá	každý	k3xTgFnSc1
z	z	k7c2
krychliček	krychlička	k1gFnPc2
je	být	k5eAaImIp3nS
nositelem	nositel	k1gMnSc7
jedné	jeden	k4xCgFnSc2
<g/>
,	,	kIx,
dvou	dva	k4xCgNnPc2
nebo	nebo	k8xC
tří	tři	k4xCgFnPc2
jednobarevných	jednobarevný	k2eAgFnPc2d1
nálepek	nálepka	k1gFnPc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
dohromady	dohromady	k6eAd1
je	být	k5eAaImIp3nS
na	na	k7c6
celém	celý	k2eAgNnSc6d1
tělese	těleso	k1gNnSc6
zastoupeno	zastoupit	k5eAaPmNgNnS
tolik	tolik	k4xDc1,k4yIc1
barev	barva	k1gFnPc2
<g/>
,	,	kIx,
kolik	kolik	k4yQc4,k4yRc4,k4yIc4
stěn	stěna	k1gFnPc2
má	mít	k5eAaImIp3nS
krychle	krychle	k1gFnSc1
<g/>
,	,	kIx,
tedy	tedy	k9
šest	šest	k4xCc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počet	počet	k1gInSc1
barev	barva	k1gFnPc2
na	na	k7c6
krychličce	krychlička	k1gFnSc6
je	být	k5eAaImIp3nS
dán	dát	k5eAaPmNgMnS
jejím	její	k3xOp3gNnSc7
umístěním	umístění	k1gNnSc7
na	na	k7c6
tělese	těleso	k1gNnSc6
—	—	k?
na	na	k7c6
rohové	rohový	k2eAgFnSc6d1
krychličce	krychlička	k1gFnSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
tři	tři	k4xCgFnPc4
nálepky	nálepka	k1gFnPc4
<g/>
,	,	kIx,
na	na	k7c6
hranové	hranový	k2eAgFnSc6d1
dvě	dva	k4xCgNnPc4
a	a	k8xC
na	na	k7c6
středové	středový	k2eAgFnSc6d1
jedna	jeden	k4xCgFnSc1
<g/>
.	.	kIx.
</s>
<s>
Celá	celý	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
je	být	k5eAaImIp3nS
propojena	propojit	k5eAaPmNgFnS
pohyblivým	pohyblivý	k2eAgInSc7d1
mechanismem	mechanismus	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
umožňuje	umožňovat	k5eAaImIp3nS
kteroukoli	kterýkoli	k3yIgFnSc4
vrstvu	vrstva	k1gFnSc4
pootočit	pootočit	k5eAaPmF
o	o	k7c4
libovolný	libovolný	k2eAgInSc4d1
celočíselný	celočíselný	k2eAgInSc4d1
násobek	násobek	k1gInSc4
pravého	pravý	k2eAgInSc2d1
úhlu	úhel	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Středy	středa	k1gFnPc1
jsou	být	k5eAaImIp3nP
jako	jako	k8xS,k8xC
jediné	jediný	k2eAgFnPc1d1
z	z	k7c2
částí	část	k1gFnPc2
nepohyblivé	pohyblivý	k2eNgNnSc1d1
<g/>
,	,	kIx,
tzn.	tzn.	kA
zaujímají	zaujímat	k5eAaImIp3nP
vůči	vůči	k7c3
sobě	se	k3xPyFc3
stále	stále	k6eAd1
stejnou	stejný	k2eAgFnSc4d1
polohu	poloha	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Barva	barva	k1gFnSc1
středu	střed	k1gInSc2
tím	ten	k3xDgInSc7
pádem	pád	k1gInSc7
určuje	určovat	k5eAaImIp3nS
<g/>
,	,	kIx,
jaká	jaký	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
výsledná	výsledný	k2eAgFnSc1d1
barva	barva	k1gFnSc1
celé	celý	k2eAgFnSc2d1
stěny	stěna	k1gFnSc2
tento	tento	k3xDgInSc1
střed	střed	k1gInSc1
obsahující	obsahující	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1
počet	počet	k1gInSc1
kombinací	kombinace	k1gFnPc2
(	(	kIx(
<g/>
permutací	permutace	k1gFnPc2
<g/>
)	)	kIx)
pro	pro	k7c4
kostku	kostka	k1gFnSc4
3	#num#	k4
<g/>
×	×	k?
<g/>
3	#num#	k4
<g/>
×	×	k?
<g/>
3	#num#	k4
je	být	k5eAaImIp3nS
43	#num#	k4
252	#num#	k4
003	#num#	k4
274	#num#	k4
489	#num#	k4
856	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Složená	složený	k2eAgFnSc1d1
Rubikova	Rubikův	k2eAgFnSc1d1
kostka	kostka	k1gFnSc1
</s>
<s>
Rubikova	Rubikův	k2eAgFnSc1d1
kostka	kostka	k1gFnSc1
v	v	k7c6
rozházené	rozházený	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
</s>
<s>
Rozložená	rozložený	k2eAgFnSc1d1
Rubikova	Rubikův	k2eAgFnSc1d1
kostka	kostka	k1gFnSc1
</s>
<s>
Varianty	varianta	k1gFnPc1
hlavolamu	hlavolam	k1gInSc2
</s>
<s>
Princip	princip	k1gInSc1
verze	verze	k1gFnSc1
3	#num#	k4
<g/>
×	×	k?
<g/>
3	#num#	k4
<g/>
×	×	k?
<g/>
3	#num#	k4
byl	být	k5eAaImAgInS
aplikován	aplikovat	k5eAaBmNgInS
na	na	k7c4
větší	veliký	k2eAgInPc4d2
i	i	k8xC
menší	malý	k2eAgInPc4d2
rozměry	rozměr	k1gInPc4
<g/>
,	,	kIx,
načež	načež	k6eAd1
se	se	k3xPyFc4
na	na	k7c4
trh	trh	k1gInSc4
dostaly	dostat	k5eAaPmAgInP
typy	typ	k1gInPc1
2	#num#	k4
<g/>
×	×	k?
<g/>
2	#num#	k4
<g/>
×	×	k?
<g/>
2	#num#	k4
<g/>
,	,	kIx,
4	#num#	k4
<g/>
×	×	k?
<g/>
4	#num#	k4
<g/>
×	×	k?
<g/>
4	#num#	k4
<g/>
,	,	kIx,
5	#num#	k4
<g/>
×	×	k?
<g/>
5	#num#	k4
<g/>
×	×	k?
<g/>
5	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
×	×	k?
<g/>
6	#num#	k4
<g/>
×	×	k?
<g/>
6	#num#	k4
a	a	k8xC
7	#num#	k4
<g/>
×	×	k?
<g/>
7	#num#	k4
<g/>
×	×	k?
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
opuštění	opuštění	k1gNnSc3
krychlového	krychlový	k2eAgNnSc2d1
tělesa	těleso	k1gNnSc2
<g/>
,	,	kIx,
což	což	k3yRnSc4,k3yQnSc4
demonstrují	demonstrovat	k5eAaBmIp3nP
např.	např.	kA
hlavolamy	hlavolam	k1gInPc1
Megaminx	Megaminx	k1gInSc1
<g/>
,	,	kIx,
Pyraminx	Pyraminx	k1gInSc1
nebo	nebo	k8xC
Square-	Square-	k1gFnSc1
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konečně	konečně	k6eAd1
existují	existovat	k5eAaImIp3nP
také	také	k9
tzv.	tzv.	kA
siamské	siamský	k2eAgFnPc4d1
kostky	kostka	k1gFnPc4
<g/>
,	,	kIx,
jež	jenž	k3xRgFnPc1
vznikly	vzniknout	k5eAaPmAgFnP
spojením	spojení	k1gNnSc7
více	hodně	k6eAd2
jiných	jiný	k2eAgFnPc2d1
kostek	kostka	k1gFnPc2
—	—	k?
původních	původní	k2eAgMnPc2d1
i	i	k8xC
odvozených	odvozený	k2eAgMnPc2d1
<g/>
.	.	kIx.
</s>
<s>
Českým	český	k2eAgInSc7d1
patentem	patent	k1gInSc7
je	být	k5eAaImIp3nS
kostka	kostka	k1gFnSc1
Square	square	k1gInSc4
One	One	k1gFnSc2
<g/>
,	,	kIx,
alternativně	alternativně	k6eAd1
<g/>
,	,	kIx,
zejména	zejména	k9
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
<g/>
,	,	kIx,
označovaná	označovaný	k2eAgFnSc1d1
Square	square	k1gInSc4
21	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Rozmíchaná	rozmíchaný	k2eAgFnSc1d1
kostka	kostka	k1gFnSc1
ve	v	k7c6
variantě	varianta	k1gFnSc6
5	#num#	k4
<g/>
×	×	k?
<g/>
5	#num#	k4
<g/>
×	×	k?
<g/>
5	#num#	k4
</s>
<s>
Rozmíchaná	rozmíchaný	k2eAgFnSc1d1
kostka	kostka	k1gFnSc1
ve	v	k7c6
verzi	verze	k1gFnSc6
2	#num#	k4
<g/>
×	×	k?
<g/>
2	#num#	k4
<g/>
×	×	k?
<g/>
2	#num#	k4
</s>
<s>
Rubikovo	Rubikův	k2eAgNnSc1d1
domino	domino	k1gNnSc1
</s>
<s>
Atypický	atypický	k2eAgInSc1d1
tvar	tvar	k1gInSc1
Rubikovy	Rubikův	k2eAgFnSc2d1
kostky	kostka	k1gFnSc2
<g/>
,	,	kIx,
zvaný	zvaný	k2eAgInSc1d1
Pyraminx	Pyraminx	k1gInSc1
<g/>
,	,	kIx,
ve	v	k7c6
složené	složený	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
</s>
<s>
Megaminx	Megaminx	k1gInSc1
—	—	k?
dvanáctistěn	dvanáctistěn	k2eAgInSc1d1
</s>
<s>
Vlevo	vlevo	k6eAd1
typ	typ	k1gInSc1
Square	square	k1gInSc1
One	One	k1gFnSc1
(	(	kIx(
<g/>
Cube	Cube	k1gInSc1
21	#num#	k4
<g/>
)	)	kIx)
ve	v	k7c6
složené	složený	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
<g/>
,	,	kIx,
uprostřed	uprostřed	k6eAd1
v	v	k7c6
rozházené	rozházený	k2eAgFnSc6d1
<g/>
,	,	kIx,
vpravo	vpravo	k6eAd1
model	model	k1gInSc1
Skewb	Skewba	k1gFnPc2
</s>
<s>
Vyosená	Vyosený	k2eAgFnSc1d1
varianta	varianta	k1gFnSc1
Rubikovy	Rubikův	k2eAgFnSc2d1
kostky	kostka	k1gFnSc2
(	(	kIx(
<g/>
Skládáte	skládat	k5eAaImIp2nP
tvar	tvar	k1gInSc4
namísto	namísto	k7c2
barev	barva	k1gFnPc2
<g/>
)	)	kIx)
—	—	k?
model	model	k1gInSc1
Mirror	Mirror	k1gMnSc1
Block	Block	k1gMnSc1
</s>
<s>
Typy	typ	k1gInPc1
3	#num#	k4
<g/>
×	×	k?
<g/>
3	#num#	k4
<g/>
×	×	k?
<g/>
4	#num#	k4
(	(	kIx(
<g/>
vlevo	vlevo	k6eAd1
<g/>
)	)	kIx)
a	a	k8xC
3	#num#	k4
<g/>
×	×	k?
<g/>
3	#num#	k4
<g/>
×	×	k?
<g/>
5	#num#	k4
(	(	kIx(
<g/>
vpravo	vpravo	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Řešení	řešení	k1gNnSc1
</s>
<s>
Rubikovu	Rubikův	k2eAgFnSc4d1
kostku	kostka	k1gFnSc4
lze	lze	k6eAd1
řešit	řešit	k5eAaImF
mnoha	mnoho	k4c2
způsoby	způsob	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existuje	existovat	k5eAaImIp3nS
mnoho	mnoho	k4c4
systematických	systematický	k2eAgFnPc2d1
metod	metoda	k1gFnPc2
<g/>
,	,	kIx,
díky	díky	k7c3
kterým	který	k3yRgMnPc3,k3yQgMnPc3,k3yIgMnPc3
kostku	kostka	k1gFnSc4
vyřeší	vyřešit	k5eAaPmIp3nS
i	i	k9
laik	laik	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postupy	postup	k1gInPc7
se	se	k3xPyFc4
liší	lišit	k5eAaImIp3nS
především	především	k6eAd1
průměrným	průměrný	k2eAgInSc7d1
počtem	počet	k1gInSc7
tahů	tah	k1gInPc2
potřebných	potřebný	k2eAgInPc2d1
na	na	k7c6
složení	složení	k1gNnSc6
<g/>
,	,	kIx,
principem	princip	k1gInSc7
a	a	k8xC
množstvím	množství	k1gNnSc7
různých	různý	k2eAgInPc2d1
algoritmů	algoritmus	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc4,k3yQgInPc4,k3yIgInPc4
je	být	k5eAaImIp3nS
potřeba	potřeba	k6eAd1
si	se	k3xPyFc3
zapamatovat	zapamatovat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Nejznámější	známý	k2eAgFnSc1d3
metoda	metoda	k1gFnSc1
spočívá	spočívat	k5eAaImIp3nS
ve	v	k7c4
skládání	skládání	k1gNnSc4
jedné	jeden	k4xCgFnSc2
vrstvy	vrstva	k1gFnSc2
po	po	k7c4
druhé	druhý	k4xOgNnSc4
(	(	kIx(
<g/>
Beginner	Beginner	k1gInSc1
layer	layra	k1gFnPc2
by	by	kYmCp3nS
layer	layer	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnPc1d1
známé	známý	k2eAgFnPc1d1
a	a	k8xC
používané	používaný	k2eAgFnPc1d1
metody	metoda	k1gFnPc1
jsou	být	k5eAaImIp3nP
např.	např.	kA
tzv.	tzv.	kA
CFOP	CFOP	kA
(	(	kIx(
<g/>
Jessica	Jessica	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
method	methoda	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Lars	Lars	k1gInSc1
Petrus	Petrus	k1gInSc1
method	methoda	k1gFnPc2
<g/>
,	,	kIx,
Zborovski-Bruchem	Zborovski-Bruch	k1gInSc7
method	method	k1gInSc1
či	či	k8xC
corners	corners	k1gInSc1
first	first	k1gFnSc1
<g/>
;	;	kIx,
jejich	jejich	k3xOp3gInSc7
společným	společný	k2eAgInSc7d1
jmenovatelem	jmenovatel	k1gInSc7
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
že	že	k8xS
byly	být	k5eAaImAgFnP
vyvinuty	vyvinout	k5eAaPmNgFnP
pro	pro	k7c4
rychlé	rychlý	k2eAgNnSc4d1
skládání	skládání	k1gNnSc4
kostky	kostka	k1gFnSc2
<g/>
,	,	kIx,
tzv.	tzv.	kA
speedcubing	speedcubing	k1gInSc1
<g/>
,	,	kIx,
popř.	popř.	kA
skládání	skládání	k1gNnSc4
kostky	kostka	k1gFnSc2
na	na	k7c4
co	co	k3yQnSc4,k3yRnSc4,k3yInSc4
nejmenší	malý	k2eAgInSc1d3
počet	počet	k1gInSc1
tahů	tah	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
červenci	červenec	k1gInSc6
2010	#num#	k4
bylo	být	k5eAaImAgNnS
hrubou	hrubý	k2eAgFnSc7d1
výpočetní	výpočetní	k2eAgFnSc7d1
silou	síla	k1gFnSc7
(	(	kIx(
<g/>
s	s	k7c7
různými	různý	k2eAgFnPc7d1
optimalizacemi	optimalizace	k1gFnPc7
<g/>
)	)	kIx)
dokázáno	dokázán	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
jakoukoliv	jakýkoliv	k3yIgFnSc4
kombinaci	kombinace	k1gFnSc4
lze	lze	k6eAd1
vyřešit	vyřešit	k5eAaPmF
do	do	k7c2
20	#num#	k4
tahů	tah	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Speedcubing	Speedcubing	k1gInSc1
</s>
<s>
Speedcubing	Speedcubing	k1gInSc1
—	—	k?
skládání	skládání	k1gNnSc2
Rubikovy	Rubikův	k2eAgFnSc2d1
kostky	kostka	k1gFnSc2
co	co	k9
nejrychleji	rychle	k6eAd3
</s>
<s>
Pojem	pojem	k1gInSc1
speedcubing	speedcubing	k1gInSc1
označuje	označovat	k5eAaImIp3nS
systém	systém	k1gInSc1
soutěžení	soutěžení	k1gNnSc2
ve	v	k7c4
skládání	skládání	k1gNnSc4
Rubikovy	Rubikův	k2eAgFnSc2d1
kostky	kostka	k1gFnSc2
a	a	k8xC
příbuzných	příbuzný	k2eAgInPc2d1
hlavolamů	hlavolam	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k8xS,k8xC
název	název	k1gInSc1
napovídá	napovídat	k5eAaBmIp3nS
<g/>
,	,	kIx,
jde	jít	k5eAaImIp3nS
především	především	k9
o	o	k7c4
soutěže	soutěž	k1gFnPc4
v	v	k7c6
co	co	k9
nejrychlejším	rychlý	k2eAgNnSc6d3
složení	složení	k1gNnSc1
<g/>
,	,	kIx,
ve	v	k7c6
druhé	druhý	k4xOgFnSc6
řadě	řada	k1gFnSc6
potom	potom	k6eAd1
o	o	k7c6
složení	složení	k1gNnSc6
v	v	k7c6
co	co	k9
nejmenším	malý	k2eAgInSc6d3
počtu	počet	k1gInSc6
tahů	tah	k1gInPc2
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
populární	populární	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
též	též	k9
zápolení	zápolení	k1gNnSc4
ve	v	k7c4
skládání	skládání	k1gNnSc4
poslepu	poslepu	k6eAd1
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
závodí	závodit	k5eAaImIp3nS
i	i	k9
ve	v	k7c6
složení	složení	k1gNnSc6
co	co	k9
největšího	veliký	k2eAgInSc2d3
počtu	počet	k1gInSc2
kostek	kostka	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
zřídka	zřídka	k6eAd1
se	se	k3xPyFc4
na	na	k7c6
soutěžích	soutěž	k1gFnPc6
objevuje	objevovat	k5eAaImIp3nS
také	také	k9
skládání	skládání	k1gNnSc1
jen	jen	k6eAd1
jednou	jeden	k4xCgFnSc7
rukou	ruka	k1gFnSc7
či	či	k8xC
nohama	noha	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Co	co	k3yQnSc1,k3yRnSc1,k3yInSc1
se	se	k3xPyFc4
týče	týkat	k5eAaImIp3nS
soutěžích	soutěž	k1gFnPc6
instrumentů	instrument	k1gInPc2
<g/>
,	,	kIx,
nejčastěji	často	k6eAd3
se	se	k3xPyFc4
soutěží	soutěžit	k5eAaImIp3nS
s	s	k7c7
kostkami	kostka	k1gFnPc7
2	#num#	k4
<g/>
×	×	k?
<g/>
2	#num#	k4
<g/>
×	×	k?
<g/>
2	#num#	k4
<g/>
,	,	kIx,
3	#num#	k4
<g/>
×	×	k?
<g/>
3	#num#	k4
<g/>
×	×	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
4	#num#	k4
<g/>
×	×	k?
<g/>
4	#num#	k4
<g/>
×	×	k?
<g/>
4	#num#	k4
a	a	k8xC
5	#num#	k4
<g/>
×	×	k?
<g/>
5	#num#	k4
<g/>
×	×	k?
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Hraje	hrát	k5eAaImIp3nS
<g/>
-li	-li	k?
se	se	k3xPyFc4
na	na	k7c4
rychlost	rychlost	k1gFnSc4
<g/>
,	,	kIx,
praxe	praxe	k1gFnSc1
bývá	bývat	k5eAaImIp3nS
taková	takový	k3xDgFnSc1
<g/>
,	,	kIx,
že	že	k8xS
každý	každý	k3xTgInSc1
soutěžící	soutěžící	k2eAgInSc1d1
úkol	úkol	k1gInSc1
splní	splnit	k5eAaPmIp3nS
pětkrát	pětkrát	k6eAd1
a	a	k8xC
jako	jako	k9
jeho	jeho	k3xOp3gInSc1
výsledek	výsledek	k1gInSc1
se	se	k3xPyFc4
započítá	započítat	k5eAaPmIp3nS
aritmetický	aritmetický	k2eAgInSc1d1
průměr	průměr	k1gInSc1
tří	tři	k4xCgMnPc2
jeho	jeho	k3xOp3gInPc2
průměrných	průměrný	k2eAgInPc2d1
časů	čas	k1gInPc2
(	(	kIx(
<g/>
nejlepší	dobrý	k2eAgInSc4d3
a	a	k8xC
nejhorší	zlý	k2eAgInSc4d3
čas	čas	k1gInSc4
se	se	k3xPyFc4
ignorují	ignorovat	k5eAaImIp3nP
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Běžné	běžný	k2eAgInPc1d1
časy	čas	k1gInPc1
ve	v	k7c6
variantě	varianta	k1gFnSc6
3	#num#	k4
<g/>
×	×	k?
<g/>
3	#num#	k4
<g/>
×	×	k?
<g/>
3	#num#	k4
klesají	klesat	k5eAaImIp3nP
pod	pod	k7c7
20	#num#	k4
s	s	k7c7
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
disciplínu	disciplína	k1gFnSc4
činí	činit	k5eAaImIp3nS
divácky	divácky	k6eAd1
atraktivní	atraktivní	k2eAgMnSc1d1
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
rozšířením	rozšíření	k1gNnSc7
kostky	kostka	k1gFnSc2
na	na	k7c4
4	#num#	k4
<g/>
×	×	k?
<g/>
4	#num#	k4
<g/>
×	×	k?
<g/>
4	#num#	k4
počet	počet	k1gInSc4
nutných	nutný	k2eAgInPc2d1
tahů	tah	k1gInPc2
dramaticky	dramaticky	k6eAd1
stoupá	stoupat	k5eAaImIp3nS
<g/>
,	,	kIx,
a	a	k8xC
s	s	k7c7
ním	on	k3xPp3gMnSc7
pochopitelně	pochopitelně	k6eAd1
i	i	k9
časy	čas	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Disciplíny	disciplína	k1gFnPc1
<g/>
,	,	kIx,
v	v	k7c6
nichž	jenž	k3xRgInPc2
v	v	k7c6
průběhu	průběh	k1gInSc6
sledování	sledování	k1gNnSc2
účastník	účastník	k1gMnSc1
soutěže	soutěž	k1gFnSc2
kostku	kostka	k1gFnSc4
nemůže	moct	k5eNaImIp3nS
pozorovat	pozorovat	k5eAaImF
<g/>
,	,	kIx,
mívají	mívat	k5eAaImIp3nP
to	ten	k3xDgNnSc4
pravidlo	pravidlo	k1gNnSc4
<g/>
,	,	kIx,
že	že	k8xS
čas	čas	k1gInSc1
se	se	k3xPyFc4
spouští	spouštět	k5eAaImIp3nS
v	v	k7c6
momentě	moment	k1gInSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
si	se	k3xPyFc3
soutěžící	soutěžící	k2eAgFnSc4d1
kostku	kostka	k1gFnSc4
začne	začít	k5eAaPmIp3nS
prohlížet	prohlížet	k5eAaImF
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
normální	normální	k2eAgFnSc2d1
soutěže	soutěž	k1gFnSc2
se	se	k3xPyFc4
vyznačují	vyznačovat	k5eAaImIp3nP
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
čas	čas	k1gInSc4
přidělený	přidělený	k2eAgInSc4d1
na	na	k7c4
prohlédnutí	prohlédnutí	k1gNnSc4
kostky	kostka	k1gFnSc2
před	před	k7c7
zahájením	zahájení	k1gNnSc7
skládání	skládání	k1gNnSc2
je	být	k5eAaImIp3nS
konstantní	konstantní	k2eAgFnSc1d1
<g/>
,	,	kIx,
a	a	k8xC
sice	sice	k8xC
15	#num#	k4
s.	s.	k?
</s>
<s>
Oficiální	oficiální	k2eAgInPc4d1
světové	světový	k2eAgInPc4d1
rekordy	rekord	k1gInPc4
ve	v	k7c6
složení	složení	k1gNnSc6
jedné	jeden	k4xCgFnSc2
kostky	kostka	k1gFnSc2
ke	k	k7c3
dni	den	k1gInSc3
12	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2019	#num#	k4
</s>
<s>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
TypČas	TypČas	k1gMnSc1
(	(	kIx(
<g/>
min	min	kA
<g/>
:	:	kIx,
<g/>
s	s	k7c7
<g/>
)	)	kIx)
<g/>
Držitel	držitel	k1gMnSc1
rekordu	rekord	k1gInSc2
</s>
<s>
2	#num#	k4
<g/>
×	×	k?
<g/>
2	#num#	k4
<g/>
×	×	k?
<g/>
20	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0,49	0,49	k4
Maciej	Maciej	k1gInSc1
Czapiewski	Czapiewsk	k1gFnSc2
</s>
<s>
2	#num#	k4
<g/>
×	×	k?
<g/>
2	#num#	k4
<g/>
×	×	k?
<g/>
2	#num#	k4
—	—	k?
průměr	průměr	k1gInSc1
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1,21	1,21	k4
Martin	Martin	k1gMnSc1
Væ	Væ	k1gInSc2
Egdal	Egdal	k1gMnSc1
</s>
<s>
3	#num#	k4
<g/>
×	×	k?
<g/>
3	#num#	k4
<g/>
×	×	k?
<g/>
30	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3,47	3,47	k4
Yusheng	Yusheng	k1gMnSc1
Du	Du	k?
(	(	kIx(
<g/>
杜	杜	k?
<g/>
)	)	kIx)
</s>
<s>
3	#num#	k4
<g/>
×	×	k?
<g/>
3	#num#	k4
<g/>
×	×	k?
<g/>
3	#num#	k4
—	—	k?
průměr	průměr	k1gInSc1
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
5,53	5,53	k4
Feliks	Feliksa	k1gFnPc2
Zemdegs	Zemdegsa	k1gFnPc2
</s>
<s>
4	#num#	k4
<g/>
×	×	k?
<g/>
4	#num#	k4
<g/>
×	×	k?
<g/>
40	#num#	k4
<g/>
:	:	kIx,
<g/>
17,42	17,42	k4
Sebastian	Sebastian	k1gMnSc1
Weyer	Weyer	k1gMnSc1
</s>
<s>
4	#num#	k4
<g/>
×	×	k?
<g/>
4	#num#	k4
<g/>
×	×	k?
<g/>
4	#num#	k4
—	—	k?
průměr	průměr	k1gInSc1
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
21,13	21,13	k4
Max	max	kA
Park	park	k1gInSc1
</s>
<s>
5	#num#	k4
<g/>
×	×	k?
<g/>
5	#num#	k4
<g/>
×	×	k?
<g/>
50	#num#	k4
<g/>
:	:	kIx,
<g/>
36,06	36,06	k4
Max	max	kA
Park	park	k1gInSc1
</s>
<s>
5	#num#	k4
<g/>
×	×	k?
<g/>
5	#num#	k4
<g/>
×	×	k?
<g/>
5	#num#	k4
—	—	k?
průměr	průměr	k1gInSc1
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
39,65	39,65	k4
Max	max	kA
Park	park	k1gInSc1
</s>
<s>
6	#num#	k4
<g/>
×	×	k?
<g/>
6	#num#	k4
<g/>
×	×	k?
<g/>
61	#num#	k4
<g/>
:	:	kIx,
<g/>
13,82	13,82	k4
Max	max	kA
Park	park	k1gInSc1
</s>
<s>
6	#num#	k4
<g/>
×	×	k?
<g/>
6	#num#	k4
<g/>
×	×	k?
<g/>
6	#num#	k4
—	—	k?
průměr	průměr	k1gInSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
17,10	17,10	k4
Max	max	kA
Park	park	k1gInSc1
</s>
<s>
7	#num#	k4
<g/>
×	×	k?
<g/>
7	#num#	k4
<g/>
×	×	k?
<g/>
71	#num#	k4
<g/>
:	:	kIx,
<g/>
40,89	40,89	k4
Max	max	kA
Park	park	k1gInSc1
</s>
<s>
7	#num#	k4
<g/>
×	×	k?
<g/>
7	#num#	k4
<g/>
×	×	k?
<g/>
7	#num#	k4
—	—	k?
průměr	průměr	k1gInSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
50,10	50,10	k4
Max	max	kA
Park	park	k1gInSc1
</s>
<s>
3	#num#	k4
<g/>
×	×	k?
<g/>
3	#num#	k4
<g/>
×	×	k?
<g/>
3	#num#	k4
—	—	k?
jednou	jednou	k6eAd1
rukou	ruka	k1gFnSc7
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
6,82	6,82	k4
Max	max	kA
Park	park	k1gInSc1
</s>
<s>
3	#num#	k4
<g/>
×	×	k?
<g/>
3	#num#	k4
<g/>
×	×	k?
<g/>
3	#num#	k4
—	—	k?
jednou	jednou	k6eAd1
rukou	ruka	k1gFnSc7
—	—	k?
průměr	průměr	k1gInSc4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
9,42	9,42	k4
Max	max	kA
Park	park	k1gInSc1
</s>
<s>
3	#num#	k4
<g/>
×	×	k?
<g/>
3	#num#	k4
<g/>
×	×	k?
<g/>
3	#num#	k4
—	—	k?
nohama	noha	k1gFnPc7
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
16,96	16,96	k4
Daniel	Daniel	k1gMnSc1
Rose-Levine	Rose-Levin	k1gMnSc5
</s>
<s>
3	#num#	k4
<g/>
×	×	k?
<g/>
3	#num#	k4
<g/>
×	×	k?
<g/>
3	#num#	k4
—	—	k?
nohama	noha	k1gFnPc7
—	—	k?
průměr	průměr	k1gInSc1
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
20,58	20,58	k4
Daniel	Daniel	k1gMnSc1
Rose-Levine	Rose-Levin	k1gInSc5
</s>
<s>
3	#num#	k4
<g/>
×	×	k?
<g/>
3	#num#	k4
<g/>
×	×	k?
<g/>
3	#num#	k4
—	—	k?
poslepu	poslepu	k6eAd1
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
15,50	15,50	k4
Max	Max	k1gMnSc1
Hilliard	Hilliard	k1gMnSc1
</s>
<s>
3	#num#	k4
<g/>
×	×	k?
<g/>
3	#num#	k4
<g/>
×	×	k?
<g/>
3	#num#	k4
—	—	k?
poslepu	poslepu	k6eAd1
—	—	k?
průměr	průměr	k1gInSc1
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
18,28	18,28	k4
Max	Max	k1gMnSc1
Hilliard	Hilliard	k1gMnSc1
</s>
<s>
4	#num#	k4
<g/>
×	×	k?
<g/>
4	#num#	k4
<g/>
×	×	k?
<g/>
4	#num#	k4
—	—	k?
poslepu	poslepu	k6eAd1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
6.23	6.23	k4
Stanley	Stanlea	k1gMnSc2
Chapel	Chapel	k1gInSc4
</s>
<s>
4	#num#	k4
<g/>
×	×	k?
<g/>
4	#num#	k4
<g/>
×	×	k?
<g/>
4	#num#	k4
—	—	k?
poslepu	poslepu	k6eAd1
—	—	k?
průměr	průměr	k1gInSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
12.55	12.55	k4
Stanley	Stanlea	k1gFnSc2
Chapel	Chapela	k1gFnPc2
</s>
<s>
5	#num#	k4
<g/>
×	×	k?
<g/>
5	#num#	k4
<g/>
×	×	k?
<g/>
5	#num#	k4
—	—	k?
poslepu	poslepu	k6eAd1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
38.77	38.77	k4
Stanley	Stanlea	k1gFnSc2
Chapel	Chapela	k1gFnPc2
</s>
<s>
5	#num#	k4
<g/>
×	×	k?
<g/>
5	#num#	k4
<g/>
×	×	k?
<g/>
5	#num#	k4
—	—	k?
poslepu	poslepu	k6eAd1
—	—	k?
průměr	průměr	k1gInSc1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
23.92	23.92	k4
Stanley	Stanlea	k1gFnSc2
Chapel	Chapela	k1gFnPc2
</s>
<s>
TyprezultátDržitel	TyprezultátDržitel	k1gMnSc1
rekordu	rekord	k1gInSc2
</s>
<s>
3	#num#	k4
<g/>
×	×	k?
<g/>
3	#num#	k4
<g/>
×	×	k?
<g/>
3	#num#	k4
—	—	k?
málo	málo	k4c4
tahů	tah	k1gInPc2
<g/>
16	#num#	k4
tahy	tah	k1gInPc7
Sebastiano	Sebastiana	k1gFnSc5
Tronto	Tront	k2eAgNnSc1d1
</s>
<s>
3	#num#	k4
<g/>
×	×	k?
<g/>
3	#num#	k4
<g/>
×	×	k?
<g/>
3	#num#	k4
—	—	k?
málo	málo	k4c4
tahů	tah	k1gInPc2
—	—	k?
průměr	průměr	k1gInSc1
<g/>
22,00	22,00	k4
tahy	tah	k1gInPc7
Sebastiano	Sebastiana	k1gFnSc5
Tronto	Tront	k2eAgNnSc1d1
</s>
<s>
3	#num#	k4
<g/>
×	×	k?
<g/>
3	#num#	k4
<g/>
×	×	k?
<g/>
3	#num#	k4
—	—	k?
hromadné	hromadný	k2eAgNnSc4d1
skládání	skládání	k1gNnSc4
poslepu	poslepu	k6eAd1
<g/>
59	#num#	k4
<g/>
/	/	kIx~
<g/>
60	#num#	k4
kostek	kostka	k1gFnPc2
Graham	Graham	k1gMnSc1
Siggins	Sigginsa	k1gFnPc2
</s>
<s>
Metody	metoda	k1gFnPc1
skládání	skládání	k1gNnSc2
ve	v	k7c6
speedcubingu	speedcubing	k1gInSc6
</s>
<s>
Nejznámější	známý	k2eAgFnPc4d3
klasické	klasický	k2eAgFnPc4d1
metody	metoda	k1gFnPc4
skládání	skládání	k1gNnSc2
kostky	kostka	k1gFnSc2
nejsou	být	k5eNaImIp3nP
pro	pro	k7c4
speedcubing	speedcubing	k1gInSc4
příliš	příliš	k6eAd1
vhodné	vhodný	k2eAgNnSc4d1
<g/>
,	,	kIx,
protože	protože	k8xS
na	na	k7c6
složení	složení	k1gNnSc6
je	být	k5eAaImIp3nS
potřeba	potřeba	k6eAd1
poměrně	poměrně	k6eAd1
vysoký	vysoký	k2eAgInSc1d1
počet	počet	k1gInSc1
tahů	tah	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byly	být	k5eAaImAgInP
proto	proto	k6eAd1
vynalezeny	vynalezen	k2eAgFnPc1d1
jiné	jiný	k2eAgFnPc1d1
metody	metoda	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
počet	počet	k1gInSc1
tahů	tah	k1gInPc2
redukují	redukovat	k5eAaBmIp3nP
<g/>
,	,	kIx,
a	a	k8xC
tím	ten	k3xDgNnSc7
složení	složení	k1gNnSc4
zrychlují	zrychlovat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalším	další	k2eAgInSc7d1
důležitým	důležitý	k2eAgInSc7d1
faktorem	faktor	k1gInSc7
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
potřebné	potřebný	k2eAgInPc1d1
tahy	tah	k1gInPc1
byly	být	k5eAaImAgInP
na	na	k7c6
kostce	kostka	k1gFnSc6
rychle	rychle	k6eAd1
vidět	vidět	k5eAaImF
<g/>
,	,	kIx,
a	a	k8xC
snížil	snížit	k5eAaPmAgInS
se	se	k3xPyFc4
tak	tak	k9
čas	čas	k1gInSc1
potřebný	potřebný	k2eAgInSc1d1
na	na	k7c4
promyšlení	promyšlení	k1gNnSc4
dalšího	další	k2eAgInSc2d1
kroku	krok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Naprostá	naprostý	k2eAgFnSc1d1
většina	většina	k1gFnSc1
metod	metoda	k1gFnPc2
se	se	k3xPyFc4
snaží	snažit	k5eAaImIp3nS
určitým	určitý	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
složit	složit	k5eAaPmF
první	první	k4xOgFnPc4
dvě	dva	k4xCgFnPc4
vrstvy	vrstva	k1gFnPc4
kostky	kostka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Složením	složení	k1gNnSc7
třetí	třetí	k4xOgFnSc2
vrstvy	vrstva	k1gFnSc2
se	se	k3xPyFc4
od	od	k7c2
sebe	sebe	k3xPyFc4
metody	metoda	k1gFnPc1
už	už	k6eAd1
poté	poté	k6eAd1
neliší	lišit	k5eNaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výjimku	výjimek	k1gInSc2
v	v	k7c6
tomto	tento	k3xDgInSc6
případě	případ	k1gInSc6
tvoří	tvořit	k5eAaImIp3nS
např.	např.	kA
metoda	metoda	k1gFnSc1
corners	corners	k1gInSc1
first	first	k1gFnSc1
(	(	kIx(
<g/>
„	„	k?
<g/>
nejdříve	dříve	k6eAd3
rohy	roh	k1gInPc4
<g/>
“	“	k?
<g/>
)	)	kIx)
nebo	nebo	k8xC
tzv.	tzv.	kA
roux	roux	k1gInSc1
method	method	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Nejpoužívanější	používaný	k2eAgFnPc1d3
metody	metoda	k1gFnPc1
<g/>
:	:	kIx,
</s>
<s>
CFOP	CFOP	kA
(	(	kIx(
<g/>
Jessica	Jessica	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
method	methoda	k1gFnPc2
<g/>
)	)	kIx)
—	—	k?
spočívá	spočívat	k5eAaImIp3nS
ve	v	k7c6
vytvoření	vytvoření	k1gNnSc6
tzv.	tzv.	kA
kříže	kříž	k1gInPc1
(	(	kIx(
<g/>
čtyř	čtyři	k4xCgFnPc2
správně	správně	k6eAd1
umístěných	umístěný	k2eAgFnPc2d1
a	a	k8xC
natočených	natočený	k2eAgFnPc2d1
hran	hrana	k1gFnPc2
jedné	jeden	k4xCgFnSc2
vrstvy	vrstva	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
po	po	k7c6
jehož	jehož	k3xOyRp3gInSc6
sestavení	sestavení	k1gNnSc6
se	se	k3xPyFc4
společně	společně	k6eAd1
umísťuje	umísťovat	k5eAaImIp3nS
jak	jak	k6eAd1
roh	roh	k1gInSc4
této	tento	k3xDgFnSc2
vrstvy	vrstva	k1gFnSc2
<g/>
,	,	kIx,
tak	tak	k8xS,k8xC
jemu	on	k3xPp3gInSc3
příslušné	příslušný	k2eAgFnPc4d1
hrany	hrana	k1gFnPc4
ze	z	k7c2
střední	střední	k2eAgFnSc2d1
vrstvy	vrstva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tímto	tento	k3xDgInSc7
způsobem	způsob	k1gInSc7
se	se	k3xPyFc4
složí	složit	k5eAaPmIp3nP
první	první	k4xOgFnPc1
dvě	dva	k4xCgFnPc1
řady	řada	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgInSc4d1
patro	patro	k1gNnSc1
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
zpravidla	zpravidla	k6eAd1
dvěma	dva	k4xCgInPc7
kroky	krok	k1gInPc7
<g/>
,	,	kIx,
orientováním	orientování	k1gNnSc7
poslední	poslední	k2eAgFnSc2d1
vrstvy	vrstva	k1gFnSc2
(	(	kIx(
<g/>
na	na	k7c6
straně	strana	k1gFnSc6
je	být	k5eAaImIp3nS
jedna	jeden	k4xCgFnSc1
barva	barva	k1gFnSc1
<g/>
,	,	kIx,
kostky	kostka	k1gFnPc1
jsou	být	k5eAaImIp3nP
proházeny	proházen	k2eAgFnPc1d1
<g/>
)	)	kIx)
a	a	k8xC
následným	následný	k2eAgNnSc7d1
proházením	proházení	k1gNnSc7
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
se	se	k3xPyFc4
složí	složit	k5eAaPmIp3nS
zbytek	zbytek	k1gInSc1
kostky	kostka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
metoda	metoda	k1gFnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
mezi	mezi	k7c7
speedcubery	speedcuber	k1gMnPc7
nejpoužívanější	používaný	k2eAgNnSc1d3
<g/>
.	.	kIx.
</s>
<s>
Zborowski-Bruchem	Zborowski-Bruch	k1gInSc7
method	methoda	k1gFnPc2
(	(	kIx(
<g/>
ZB	ZB	kA
method	method	k1gInSc1
<g/>
)	)	kIx)
—	—	k?
obdoba	obdoba	k1gFnSc1
metody	metoda	k1gFnSc2
Fridrich	Fridrich	k1gMnSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
ovšem	ovšem	k9
při	při	k7c6
umístění	umístění	k1gNnSc6
posledního	poslední	k2eAgInSc2d1
rohu	roh	k1gInSc2
a	a	k8xC
hrany	hrana	k1gFnSc2
prvních	první	k4xOgFnPc2
dvou	dva	k4xCgFnPc2
vrstev	vrstva	k1gFnPc2
správně	správně	k6eAd1
natočí	natočit	k5eAaBmIp3nS
hrany	hrana	k1gFnPc4
poslední	poslední	k2eAgFnSc2d1
vrstvy	vrstva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Metoda	metoda	k1gFnSc1
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
těžká	těžký	k2eAgFnSc1d1
na	na	k7c6
učení	učení	k1gNnSc6
<g/>
;	;	kIx,
na	na	k7c6
zvládnutí	zvládnutí	k1gNnSc6
celé	celý	k2eAgFnSc2d1
metody	metoda	k1gFnSc2
je	být	k5eAaImIp3nS
totiž	totiž	k9
potřeba	potřeba	k1gFnSc1
zapamatovat	zapamatovat	k5eAaPmF
si	se	k3xPyFc3
téměř	téměř	k6eAd1
800	#num#	k4
různých	různý	k2eAgInPc2d1
algoritmů	algoritmus	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Zbigniew	Zbigniew	k?
Zborowski	Zborowski	k1gNnPc2
method	method	k1gInSc1
(	(	kIx(
<g/>
ZZ	ZZ	kA
method	method	k1gInSc1
<g/>
)	)	kIx)
—	—	k?
v	v	k7c6
první	první	k4xOgFnSc6
fázi	fáze	k1gFnSc6
se	se	k3xPyFc4
všechny	všechen	k3xTgFnPc1
hrany	hrana	k1gFnPc1
správně	správně	k6eAd1
orientují	orientovat	k5eAaBmIp3nP
a	a	k8xC
vytvoří	vytvořit	k5eAaPmIp3nP
se	se	k3xPyFc4
na	na	k7c6
spodní	spodní	k2eAgFnSc6d1
straně	strana	k1gFnSc6
linie	linie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
zbytek	zbytek	k1gInSc4
složení	složení	k1gNnSc2
stačí	stačit	k5eAaBmIp3nS
pohyby	pohyb	k1gInPc4
pravé	pravá	k1gFnSc2
<g/>
,	,	kIx,
levé	levý	k2eAgFnSc2d1
a	a	k8xC
vrchní	vrchní	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhý	druhý	k4xOgInSc1
krok	krok	k1gInSc1
je	být	k5eAaImIp3nS
stejný	stejný	k2eAgInSc1d1
jako	jako	k8xS,k8xC
u	u	k7c2
metody	metoda	k1gFnSc2
Fridrich	Fridrich	k1gMnSc1
<g/>
,	,	kIx,
ovšem	ovšem	k9
na	na	k7c6
konci	konec	k1gInSc6
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
různit	různit	k5eAaImF
podle	podle	k7c2
spousty	spousta	k1gFnSc2
alternativ	alternativa	k1gFnPc2
pro	pro	k7c4
poslední	poslední	k2eAgNnSc4d1
patro	patro	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Lars	Lars	k6eAd1
Petrus	Petrus	k1gInSc1
method	method	k1gInSc1
—	—	k?
spočívá	spočívat	k5eAaImIp3nS
ve	v	k7c6
vytvoření	vytvoření	k1gNnSc6
tzv.	tzv.	kA
pracovního	pracovní	k2eAgInSc2d1
rohu	roh	k1gInSc2
(	(	kIx(
<g/>
bloku	blok	k1gInSc2
2	#num#	k4
<g/>
×	×	k?
<g/>
2	#num#	k4
<g/>
×	×	k?
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
se	se	k3xPyFc4
v	v	k7c6
dalším	další	k2eAgInSc6d1
kroku	krok	k1gInSc6
rozšíří	rozšířit	k5eAaPmIp3nS
na	na	k7c4
2	#num#	k4
<g/>
×	×	k?
<g/>
3	#num#	k4
<g/>
×	×	k?
<g/>
2	#num#	k4
<g/>
,	,	kIx,
poté	poté	k6eAd1
se	se	k3xPyFc4
zorientují	zorientovat	k5eAaPmIp3nP
zbylé	zbylý	k2eAgInPc1d1
hrany	hrana	k1gFnPc4
a	a	k8xC
následně	následně	k6eAd1
se	se	k3xPyFc4
rozšíří	rozšířit	k5eAaPmIp3nS
na	na	k7c4
3	#num#	k4
<g/>
×	×	k?
<g/>
3	#num#	k4
<g/>
×	×	k?
<g/>
2	#num#	k4
(	(	kIx(
<g/>
tzn.	tzn.	kA
dosložení	dosložení	k1gNnSc1
do	do	k7c2
prvních	první	k4xOgFnPc2
dvou	dva	k4xCgFnPc2
vrstev	vrstva	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výhodou	výhoda	k1gFnSc7
této	tento	k3xDgFnSc2
metody	metoda	k1gFnSc2
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
ze	z	k7c2
všech	všecek	k3xTgMnPc2
zmíněných	zmíněný	k2eAgMnPc2d1
potřebuje	potřebovat	k5eAaImIp3nS
na	na	k7c4
složení	složení	k1gNnSc4
nejméně	málo	k6eAd3
tahů	tah	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
u	u	k7c2
ní	on	k3xPp3gFnSc2
ovšem	ovšem	k9
větší	veliký	k2eAgInSc1d2
problém	problém	k1gInSc1
kroky	krok	k1gInPc1
vidět	vidět	k5eAaImF
dopředu	dopředu	k6eAd1
a	a	k8xC
neztrácet	ztrácet	k5eNaImF
tolik	tolik	k6eAd1
času	čas	k1gInSc2
prohlížením	prohlížení	k1gNnSc7
kostky	kostka	k1gFnSc2
a	a	k8xC
plánováním	plánování	k1gNnSc7
dalšího	další	k2eAgInSc2d1
kroku	krok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Metoda	metoda	k1gFnSc1
je	být	k5eAaImIp3nS
vhodná	vhodný	k2eAgFnSc1d1
pro	pro	k7c4
disciplíny	disciplína	k1gFnPc4
<g/>
,	,	kIx,
v	v	k7c6
nichž	jenž	k3xRgInPc6
je	být	k5eAaImIp3nS
cílem	cíl	k1gInSc7
složit	složit	k5eAaPmF
kostku	kostka	k1gFnSc4
v	v	k7c6
co	co	k9
nejmenším	malý	k2eAgInSc6d3
počtu	počet	k1gInSc6
tahů	tah	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Corners	Corners	k1gInSc1
first	first	k1gInSc1
(	(	kIx(
<g/>
Ortega	Ortega	k1gFnSc1
<g/>
)	)	kIx)
—	—	k?
nejdříve	dříve	k6eAd3
se	se	k3xPyFc4
složí	složit	k5eAaPmIp3nS
všech	všecek	k3xTgInPc2
8	#num#	k4
rohů	roh	k1gInPc2
<g/>
,	,	kIx,
poté	poté	k6eAd1
hrany	hrana	k1gFnPc1
nejprve	nejprve	k6eAd1
v	v	k7c6
jedné	jeden	k4xCgFnSc6
vrstvě	vrstva	k1gFnSc6
<g/>
,	,	kIx,
pak	pak	k6eAd1
v	v	k7c6
protější	protější	k2eAgFnSc6d1
a	a	k8xC
nakonec	nakonec	k6eAd1
se	se	k3xPyFc4
jedním	jeden	k4xCgInSc7
algoritmem	algoritmus	k1gInSc7
umístí	umístit	k5eAaPmIp3nS
a	a	k8xC
zároveň	zároveň	k6eAd1
správně	správně	k6eAd1
otočí	otočit	k5eAaPmIp3nP
hrany	hrana	k1gFnPc4
ve	v	k7c6
vrstvě	vrstva	k1gFnSc6
střední	střední	k2eAgFnSc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
metoda	metoda	k1gFnSc1
tedy	tedy	k9
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
výše	vysoce	k6eAd2
zmíněných	zmíněný	k2eAgInPc2d1
neprochází	procházet	k5eNaImIp3nS
stavem	stav	k1gInSc7
složení	složení	k1gNnSc1
dvou	dva	k4xCgFnPc2
řad	řada	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Roux	Roux	k1gInSc1
—	—	k?
metoda	metoda	k1gFnSc1
založená	založený	k2eAgFnSc1d1
na	na	k7c6
tzv.	tzv.	kA
„	„	k?
<g/>
blockbuildingu	blockbuilding	k1gInSc2
<g/>
“	“	k?
—	—	k?
v	v	k7c6
prvním	první	k4xOgInSc6
kroku	krok	k1gInSc6
se	se	k3xPyFc4
libovolným	libovolný	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
vytvoří	vytvořit	k5eAaPmIp3nS
blok	blok	k1gInSc1
1	#num#	k4
<g/>
×	×	k?
<g/>
2	#num#	k4
<g/>
×	×	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
obvykle	obvykle	k6eAd1
na	na	k7c6
levé	levý	k2eAgFnSc6d1
straně	strana	k1gFnSc6
kostky	kostka	k1gFnSc2
<g/>
,	,	kIx,
ve	v	k7c6
druhém	druhý	k4xOgInSc6
kroku	krok	k1gInSc6
je	být	k5eAaImIp3nS
pak	pak	k6eAd1
třeba	třeba	k6eAd1
vytvořit	vytvořit	k5eAaPmF
stejný	stejný	k2eAgInSc4d1
blok	blok	k1gInSc4
na	na	k7c6
protější	protější	k2eAgFnSc6d1
straně	strana	k1gFnSc6
(	(	kIx(
<g/>
při	při	k7c6
zachování	zachování	k1gNnSc6
prvního	první	k4xOgMnSc2
bloku	blok	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následuje	následovat	k5eAaImIp3nS
orientace	orientace	k1gFnSc1
a	a	k8xC
permutace	permutace	k1gFnPc1
rohů	roh	k1gInPc2
horní	horní	k2eAgFnPc1d1
vrstvy	vrstva	k1gFnPc1
pomocí	pomocí	k7c2
naučených	naučený	k2eAgInPc2d1
algoritmů	algoritmus	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgInSc4d1
krok	krok	k1gInSc4
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yRgInSc6,k3yQgInSc6,k3yIgInSc6
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
zbylých	zbylý	k2eAgFnPc2d1
6	#num#	k4
hran	hrana	k1gFnPc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
rozdělen	rozdělit	k5eAaPmNgInS
do	do	k7c2
tří	tři	k4xCgFnPc2
částí	část	k1gFnPc2
(	(	kIx(
<g/>
orientace	orientace	k1gFnSc1
hran	hrana	k1gFnPc2
<g/>
,	,	kIx,
složení	složení	k1gNnSc2
2	#num#	k4
bočních	boční	k2eAgFnPc2d1
hran	hrana	k1gFnPc2
<g/>
,	,	kIx,
permutace	permutace	k1gFnSc2
posledních	poslední	k2eAgFnPc2d1
4	#num#	k4
hran	hrana	k1gFnPc2
<g/>
)	)	kIx)
—	—	k?
tento	tento	k3xDgInSc4
krok	krok	k1gInSc4
je	být	k5eAaImIp3nS
velice	velice	k6eAd1
těžký	těžký	k2eAgInSc1d1
na	na	k7c4
pochopení	pochopení	k1gNnSc4
<g/>
,	,	kIx,
zato	zato	k6eAd1
však	však	k9
velmi	velmi	k6eAd1
efektivní	efektivní	k2eAgMnSc1d1
(	(	kIx(
<g/>
používají	používat	k5eAaImIp3nP
se	se	k3xPyFc4
pouze	pouze	k6eAd1
tahy	tah	k1gInPc7
horní	horní	k2eAgFnSc2d1
a	a	k8xC
prostřední	prostřední	k2eAgFnSc2d1
vrstvou	vrstva	k1gFnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
metoda	metoda	k1gFnSc1
vyžaduje	vyžadovat	k5eAaImIp3nS
méně	málo	k6eAd2
tahů	tah	k1gInPc2
na	na	k7c4
jedno	jeden	k4xCgNnSc4
složení	složení	k1gNnSc4
než	než	k8xS
ostatní	ostatní	k2eAgFnPc4d1
metody	metoda	k1gFnPc4
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
vyžaduje	vyžadovat	k5eAaImIp3nS
hodně	hodně	k6eAd1
přemýšlení	přemýšlení	k1gNnSc4
dopředu	dopředu	k6eAd1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
její	její	k3xOp3gFnSc4
celkovou	celkový	k2eAgFnSc4d1
rychlost	rychlost	k1gFnSc4
snižuje	snižovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
tak	tak	k6eAd1
rychlému	rychlý	k2eAgNnSc3d1
složení	složení	k1gNnSc3
<g/>
,	,	kIx,
od	od	k7c2
jakého	jaký	k3yIgNnSc2,k3yQgNnSc2,k3yRgNnSc2
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
slibovat	slibovat	k5eAaImF
si	se	k3xPyFc3
úspěch	úspěch	k1gInSc4
v	v	k7c6
soutěžích	soutěž	k1gFnPc6
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
vedle	vedle	k7c2
velké	velký	k2eAgFnSc2d1
dávky	dávka	k1gFnSc2
motorického	motorický	k2eAgNnSc2d1
nadání	nadání	k1gNnSc2
pro	pro	k7c4
provádění	provádění	k1gNnSc4
správných	správný	k2eAgInPc2d1
kroků	krok	k1gInPc2
ve	v	k7c6
vysoké	vysoký	k2eAgFnSc6d1
rychlosti	rychlost	k1gFnSc6
nezbytné	nezbytný	k2eAgFnPc1d1,k2eNgFnPc1d1
se	se	k3xPyFc4
naučit	naučit	k5eAaPmF
velké	velký	k2eAgNnSc4d1
množství	množství	k1gNnSc4
různých	různý	k2eAgInPc2d1
algoritmů	algoritmus	k1gInPc2
<g/>
,	,	kIx,
zejména	zejména	k9
pro	pro	k7c4
složení	složení	k1gNnSc4
poslední	poslední	k2eAgFnSc2d1
vrstvy	vrstva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
případě	případ	k1gInSc6
metody	metoda	k1gFnSc2
Fridrich	Fridrich	k1gMnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
mezi	mezi	k7c4
speedcubery	speedcuber	k1gMnPc4
nejvíce	nejvíce	k6eAd1,k6eAd3
oblíbená	oblíbený	k2eAgFnSc1d1
a	a	k8xC
uznávaná	uznávaný	k2eAgFnSc1d1
<g/>
,	,	kIx,
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
78	#num#	k4
<g/>
,	,	kIx,
metoda	metoda	k1gFnSc1
Roux	Roux	k1gInSc1
v	v	k7c6
tomto	tento	k3xDgInSc6
ohledu	ohled	k1gInSc6
znamená	znamenat	k5eAaImIp3nS
počet	počet	k1gInSc1
42	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průměrný	průměrný	k2eAgInSc1d1
počet	počet	k1gInSc1
tahů	tah	k1gInPc2
napříč	napříč	k7c7
těmito	tento	k3xDgInPc7
algoritmy	algoritmus	k1gInPc7
je	být	k5eAaImIp3nS
cca	cca	kA
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Všech	všecek	k3xTgInPc2
algoritmů	algoritmus	k1gInPc2
potřebných	potřebný	k2eAgInPc2d1
pro	pro	k7c4
složení	složení	k1gNnSc4
poslední	poslední	k2eAgFnSc2d1
vrstvy	vrstva	k1gFnSc2
v	v	k7c6
jednom	jeden	k4xCgInSc6
kroku	krok	k1gInSc6
je	být	k5eAaImIp3nS
1	#num#	k4
211	#num#	k4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
vzhledem	vzhledem	k7c3
k	k	k7c3
možnostem	možnost	k1gFnPc3
lidské	lidský	k2eAgFnSc2d1
paměti	paměť	k1gFnSc2
příliš	příliš	k6eAd1
velké	velký	k2eAgNnSc4d1
číslo	číslo	k1gNnSc4
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
se	se	k3xPyFc4
na	na	k7c6
špičkové	špičkový	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
v	v	k7c6
metodách	metoda	k1gFnPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
už	už	k6eAd1
jsou	být	k5eAaImIp3nP
hrany	hrana	k1gFnPc4
horní	horní	k2eAgFnPc4d1
vrstvy	vrstva	k1gFnPc4
správně	správně	k6eAd1
natočeny	natočit	k5eAaBmNgFnP
z	z	k7c2
kroků	krok	k1gInPc2
předtím	předtím	k6eAd1
jen	jen	k9
jedním	jeden	k4xCgInSc7
krokem	krok	k1gInSc7
<g/>
,	,	kIx,
poslední	poslední	k2eAgFnSc1d1
vrstva	vrstva	k1gFnSc1
řeší	řešit	k5eAaImIp3nS
ve	v	k7c6
dvou	dva	k4xCgInPc6
krocích	krok	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
rychlé	rychlý	k2eAgNnSc4d1
skládání	skládání	k1gNnSc4
poslepu	poslepu	k6eAd1
se	se	k3xPyFc4
používají	používat	k5eAaImIp3nP
úplně	úplně	k6eAd1
jiné	jiný	k2eAgFnPc1d1
metody	metoda	k1gFnPc1
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yQgInPc6,k3yIgInPc6,k3yRgInPc6
se	se	k3xPyFc4
používají	používat	k5eAaImIp3nP
algoritmy	algoritmus	k1gInPc1
přemísťující	přemísťující	k2eAgInPc1d1
jen	jen	k9
několik	několik	k4yIc1
málo	málo	k4c1
kostek	kostka	k1gFnPc2
na	na	k7c4
správná	správný	k2eAgNnPc4d1
místa	místo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důležitým	důležitý	k2eAgInSc7d1
prvkem	prvek	k1gInSc7
pro	pro	k7c4
rychlé	rychlý	k2eAgNnSc4d1
skládání	skládání	k1gNnSc4
poslepu	poslepu	k6eAd1
je	být	k5eAaImIp3nS
především	především	k9
schopnost	schopnost	k1gFnSc1
rychle	rychle	k6eAd1
si	se	k3xPyFc3
zapamatovat	zapamatovat	k5eAaPmF
počáteční	počáteční	k2eAgNnSc4d1
rozložení	rozložení	k1gNnSc4
kostky	kostka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
populární	populární	k2eAgFnSc6d1
kultuře	kultura	k1gFnSc6
</s>
<s>
Velká	velký	k2eAgFnSc1d1
Rubikova	Rubikův	k2eAgFnSc1d1
kostka	kostka	k1gFnSc1
v	v	k7c6
kampusu	kampus	k1gInSc6
University	universita	k1gFnSc2
of	of	k?
Michigan	Michigan	k1gInSc1
</s>
<s>
Ukázka	ukázka	k1gFnSc1
Rubikova	Rubikův	k2eAgInSc2d1
kubismu	kubismus	k1gInSc2
—	—	k?
Pete	Pet	k1gMnSc4
Fecteau	Fecteaa	k1gMnSc4
<g/>
:	:	kIx,
Dream	Dream	k1gInSc1
Big	Big	k1gFnSc2
</s>
<s>
Rubikova	Rubikův	k2eAgFnSc1d1
kostka	kostka	k1gFnSc1
se	se	k3xPyFc4
objevila	objevit	k5eAaPmAgFnS
v	v	k7c6
mnoha	mnoho	k4c6
filmech	film	k1gInPc6
<g/>
:	:	kIx,
</s>
<s>
3	#num#	k4
Idiots	Idiots	k1gInSc1
</s>
<s>
Armageddon	Armageddon	k1gMnSc1
</s>
<s>
Brick	Brick	k6eAd1
</s>
<s>
Dude	Dude	k6eAd1
</s>
<s>
Holiday	Holidaa	k1gFnPc1
<g/>
:	:	kIx,
A	a	k9
Soldier	Soldier	k1gMnSc1
Is	Is	k1gMnSc1
Never	Never	k1gMnSc1
Off	Off	k1gMnSc1
Duty	Duty	k?
</s>
<s>
Chameleon	chameleon	k1gMnSc1
Street	Street	k1gMnSc1
</s>
<s>
Karthik	Karthik	k1gInSc1
Calling	Calling	k1gInSc1
Karthik	Karthik	k1gInSc1
</s>
<s>
Let	let	k1gInSc1
Me	Me	k1gMnSc2
In	In	k1gMnSc2
</s>
<s>
Let	léto	k1gNnPc2
the	the	k?
Right	Right	k1gMnSc1
One	One	k1gMnSc1
In	In	k1gMnSc1
</s>
<s>
My	my	k3xPp1nPc1
Name	Name	k1gNnPc7
is	is	k?
Khan	Khan	k1gMnSc1
</s>
<s>
Nói	Nói	k?
the	the	k?
Albino	Albino	k1gNnSc4
</s>
<s>
Snowden	Snowdna	k1gFnPc2
</s>
<s>
The	The	k?
Pursuit	Pursuit	k1gInSc1
of	of	k?
Happyness	Happyness	k1gInSc1
</s>
<s>
There	Ther	k1gMnSc5
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Something	Something	k1gInSc1
About	About	k1gMnSc1
Mary	Mary	k1gFnSc1
</s>
<s>
WALL-E	WALL-E	k?
</s>
<s>
Where	Wher	k1gMnSc5
<g/>
'	'	kIx"
<g/>
s	s	k7c7
My	my	k3xPp1nPc1
Car	car	k1gMnSc1
<g/>
?	?	kIx.
</s>
<s>
Kostka	kostka	k1gFnSc1
sehrála	sehrát	k5eAaPmAgFnS
roli	role	k1gFnSc4
rovněž	rovněž	k9
v	v	k7c6
několika	několik	k4yIc6
seriálech	seriál	k1gInPc6
<g/>
:	:	kIx,
</s>
<s>
Doctor	Doctor	k1gMnSc1
Who	Who	k1gMnSc1
</s>
<s>
Everybody	Everyboda	k1gFnPc1
Hates	Hatesa	k1gFnPc2
Chris	Chris	k1gFnSc2
</s>
<s>
Show	show	k1gFnSc1
Jerryho	Jerry	k1gMnSc2
Seinfelda	Seinfeld	k1gMnSc2
</s>
<s>
The	The	k?
Carrie	Carrie	k1gFnSc1
Diaries	Diaries	k1gInSc1
</s>
<s>
The	The	k?
Fresh	Fresh	k1gInSc1
Prince	princ	k1gMnSc2
of	of	k?
Bel-Air	Bel-Air	k1gMnSc1
</s>
<s>
The	The	k?
Simpsons	Simpsons	k1gInSc1
</s>
<s>
The	The	k?
Big	Big	k1gMnSc1
Bang	Bang	k1gMnSc1
Theory	Theora	k1gFnSc2
</s>
<s>
Ve	v	k7c6
filmech	film	k1gInPc6
a	a	k8xC
seriálech	seriál	k1gInPc6
se	se	k3xPyFc4
kostka	kostka	k1gFnSc1
typicky	typicky	k6eAd1
vyskytuje	vyskytovat	k5eAaImIp3nS
v	v	k7c6
kontextu	kontext	k1gInSc6
spojujícím	spojující	k2eAgInSc6d1
inteligenci	inteligence	k1gFnSc4
určité	určitý	k2eAgFnPc4d1
postavy	postava	k1gFnPc4
s	s	k7c7
její	její	k3xOp3gFnSc7
schopností	schopnost	k1gFnPc2
kostku	kostka	k1gFnSc4
rychle	rychle	k6eAd1
složit	složit	k5eAaPmF
<g/>
,	,	kIx,
resp.	resp.	kA
frustrací	frustrace	k1gFnPc2
z	z	k7c2
neschopnosti	neschopnost	k1gFnSc2
ji	on	k3xPp3gFnSc4
složit	složit	k5eAaPmF
(	(	kIx(
<g/>
And	Anda	k1gFnPc2
The	The	k1gFnSc2
Band	banda	k1gFnPc2
Played	Played	k1gMnSc1
On	on	k3xPp3gMnSc1
<g/>
,	,	kIx,
Being	Being	k1gMnSc1
John	John	k1gMnSc1
Malkovich	Malkovich	k1gMnSc1
<g/>
,	,	kIx,
Hellboy	Hellboy	k1gInPc1
<g/>
,	,	kIx,
The	The	k1gFnSc1
Wedding	Wedding	k1gInSc1
Singer	Singer	k1gInSc1
<g/>
,	,	kIx,
UHF	UHF	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
nás	my	k3xPp1nPc2
na	na	k7c6
stejné	stejná	k1gFnSc6
téma	téma	k1gNnSc1
-	-	kIx~
frustrace	frustrace	k1gFnSc1
z	z	k7c2
nesložení	nesložení	k1gNnSc2
kostky	kostka	k1gFnSc2
<g/>
,	,	kIx,
nazpíval	nazpívat	k5eAaBmAgMnS,k5eAaPmAgMnS
písničku	písnička	k1gFnSc4
Rubikova	Rubikův	k2eAgFnSc1d1
kostka	kostka	k1gFnSc1
interpret	interpret	k1gMnSc1
Karel	Karel	k1gMnSc1
Zich	Zich	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS
i	i	k9
tzv.	tzv.	kA
„	„	k?
<g/>
Rubikův	Rubikův	k2eAgInSc4d1
kubismus	kubismus	k1gInSc4
<g/>
“	“	k?
<g/>
,	,	kIx,
spočívající	spočívající	k2eAgFnSc7d1
ve	v	k7c4
skládání	skládání	k1gNnSc4
velkého	velký	k2eAgInSc2d1
obrazu	obraz	k1gInSc2
z	z	k7c2
rastru	rastr	k1gInSc2
Rubikových	Rubikových	k2eAgFnPc2d1
kostek	kostka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Amazing	Amazing	k1gInSc1
Facts	Facts	k1gInSc1
to	ten	k3xDgNnSc4
Blow	Blow	k1gFnSc7
Your	Your	k1gMnSc1
Mind	Mind	k1gMnSc1
Pt	Pt	k1gMnSc1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
↑	↑	k?
Hrají	hrát	k5eAaImIp3nP
si	se	k3xPyFc3
s	s	k7c7
ní	on	k3xPp3gFnSc7
děti	dítě	k1gFnPc1
i	i	k9
dospělí	dospělí	k1gMnPc1
<g/>
,	,	kIx,
Rubikova	Rubikův	k2eAgFnSc1d1
kostka	kostka	k1gFnSc1
oslavila	oslavit	k5eAaPmAgFnS
40	#num#	k4
<g/>
.	.	kIx.
narozeniny	narozeniny	k1gFnPc4
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
God	God	k1gFnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Number	Number	k1gInSc1
is	is	k?
20	#num#	k4
<g/>
:	:	kIx,
http://www.cube20.org/	http://www.cube20.org/	k4
<g/>
↑	↑	k?
TEAM	team	k1gInSc1
<g/>
,	,	kIx,
WCA	WCA	kA
Website	Websit	k1gInSc5
<g/>
.	.	kIx.
www.worldcubeassociation.org	www.worldcubeassociation.org	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
www.worldcubeassociation.org	www.worldcubeassociation.org	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Rubikova	Rubikův	k2eAgFnSc1d1
pomsta	pomsta	k1gFnSc1
</s>
<s>
Ernő	Ernő	k?
Rubik	Rubik	k1gMnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Rubikova	Rubikův	k2eAgFnSc1d1
kostka	kostka	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Téma	téma	k1gFnSc1
Rubikova	Rubikův	k2eAgFnSc1d1
kostka	kostka	k1gFnSc1
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
První	první	k4xOgNnSc1
mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
ve	v	k7c6
skládání	skládání	k1gNnSc6
Rubikovy	Rubikův	k2eAgFnSc2d1
kostky	kostka	k1gFnSc2
</s>
<s>
Metody	metoda	k1gFnPc1
rychlého	rychlý	k2eAgNnSc2d1
skládání	skládání	k1gNnSc2
Rubikovy	Rubikův	k2eAgFnSc2d1
kostky	kostka	k1gFnSc2
</s>
<s>
Božské	božský	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
Rubikovy	Rubikův	k2eAgFnSc2d1
kostky	kostka	k1gFnSc2
</s>
<s>
Historické	historický	k2eAgInPc1d1
rekordy	rekord	k1gInPc1
Rubikovy	Rubikův	k2eAgFnSc2d1
kostky	kostka	k1gFnSc2
</s>
<s>
Videa	video	k1gNnPc4
rekordů	rekord	k1gInPc2
ve	v	k7c4
skládání	skládání	k1gNnSc4
Rubikovy	Rubikův	k2eAgFnSc2d1
kostky	kostka	k1gFnSc2
</s>
<s>
Největší	veliký	k2eAgFnSc1d3
Rubikova	Rubikův	k2eAgFnSc1d1
kostka	kostka	k1gFnSc1
33	#num#	k4
<g/>
×	×	k?
<g/>
33	#num#	k4
<g/>
×	×	k?
<g/>
33	#num#	k4
</s>
<s>
Intuitivní	intuitivní	k2eAgInSc1d1
návod	návod	k1gInSc1
na	na	k7c4
složení	složení	k1gNnSc4
-	-	kIx~
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
interaktivní	interaktivní	k2eAgInPc4d1
animované	animovaný	k2eAgInPc4d1
simulátory	simulátor	k1gInPc4
Rubikovy	Rubikův	k2eAgFnSc2d1
kostky	kostka	k1gFnSc2
pro	pro	k7c4
lepší	dobrý	k2eAgFnSc4d2
názornost	názornost	k1gFnSc4
</s>
<s>
Oficiální	oficiální	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
Rubikovy	Rubikův	k2eAgFnSc2d1
kostky	kostka	k1gFnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Video-návod	Video-návod	k1gInSc1
pro	pro	k7c4
začátečníky	začátečník	k1gMnPc4
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
složit	složit	k5eAaPmF
Rubikovu	Rubikův	k2eAgFnSc4d1
kostku	kostka	k1gFnSc4
3	#num#	k4
<g/>
×	×	k?
<g/>
3	#num#	k4
<g/>
×	×	k?
<g/>
3	#num#	k4
(	(	kIx(
<g/>
22	#num#	k4
minut	minuta	k1gFnPc2
<g/>
)	)	kIx)
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Video-návody	Video-návod	k1gInPc1
na	na	k7c4
složení	složení	k1gNnSc4
rubikovy	rubikův	k2eAgFnSc2d1
kostky	kostka	k1gFnSc2
od	od	k7c2
začátečnických	začátečnický	k2eAgFnPc2d1
po	po	k7c4
pokročilé	pokročilý	k2eAgFnPc4d1
metody	metoda	k1gFnPc4
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4076864-8	4076864-8	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85115690	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85115690	#num#	k4
</s>
