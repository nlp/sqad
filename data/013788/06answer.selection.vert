<s>
Kostka	kostka	k1gFnSc1
formátu	formát	k1gInSc2
3	#num#	k4
<g/>
×	×	k?
<g/>
3	#num#	k4
<g/>
×	×	k?
<g/>
3	#num#	k4
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
hitem	hit	k1gInSc7
na	na	k7c6
přelomu	přelom	k1gInSc6
70	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
80	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byla	být	k5eAaImAgFnS
vyráběna	vyrábět	k5eAaImNgFnS
v	v	k7c6
milionových	milionový	k2eAgFnPc6d1
sériích	série	k1gFnPc6
a	a	k8xC
stala	stát	k5eAaPmAgFnS
se	se	k3xPyFc4
nejprodávanějším	prodávaný	k2eAgInSc7d3
produktem	produkt	k1gInSc7
na	na	k7c6
Zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Vynalezl	vynaleznout	k5eAaPmAgInS
ji	on	k3xPp3gFnSc4
maďarský	maďarský	k2eAgMnSc1d1
sochař	sochař	k1gMnSc1
a	a	k8xC
architekt	architekt	k1gMnSc1
Ernő	Ernő	k1gMnSc1
Rubik	Rubik	k1gMnSc1
19	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1974	#num#	k4
<g/>
,	,	kIx,
patent	patent	k1gInSc1
podal	podat	k5eAaPmAgInS
30	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1975	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>