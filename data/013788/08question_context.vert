<s>
Rubikova	Rubikův	k2eAgFnSc1d1
kostka	kostka	k1gFnSc1
je	být	k5eAaImIp3nS
mechanický	mechanický	k2eAgInSc1d1
hlavolam	hlavolam	k1gInSc1
<g/>
,	,	kIx,
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
původní	původní	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
tvořený	tvořený	k2eAgInSc4d1
krychlí	krychle	k1gFnSc7
složenou	složený	k2eAgFnSc7d1
z	z	k7c2
dílčích	dílčí	k2eAgFnPc2d1
barevných	barevný	k2eAgFnPc2d1
krychliček	krychlička	k1gFnPc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc7
úkolem	úkol	k1gInSc7
je	být	k5eAaImIp3nS
rotacemi	rotace	k1gFnPc7
přeuspořádat	přeuspořádat	k5eAaPmF
jednotlivé	jednotlivý	k2eAgFnPc4d1
dílčí	dílčí	k2eAgFnPc4d1
části	část	k1gFnPc4
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
každá	každý	k3xTgFnSc1
strana	strana	k1gFnSc1
celého	celý	k2eAgNnSc2d1
tělesa	těleso	k1gNnSc2
byla	být	k5eAaImAgFnS
obarvena	obarvit	k5eAaPmNgFnS
jen	jen	k6eAd1
jednou	jeden	k4xCgFnSc7
barvou	barva	k1gFnSc7
<g/>
.	.	kIx.
</s>