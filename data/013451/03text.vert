<p>
<s>
Mardža	Mardža	k1gFnSc1	Mardža
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
<g/>
:	:	kIx,	:
מ	מ	k?	מ
<g/>
'	'	kIx"	'
<g/>
ה	ה	k?	ה
<g/>
,	,	kIx,	,
arabsky	arabsky	k6eAd1	arabsky
<g/>
:	:	kIx,	:
ا	ا	k?	ا
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
vesnice	vesnice	k1gFnSc1	vesnice
v	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
<g/>
,	,	kIx,	,
v	v	k7c6	v
Centrálním	centrální	k2eAgInSc6d1	centrální
distriktu	distrikt	k1gInSc6	distrikt
začleněná	začleněný	k2eAgFnSc1d1	začleněná
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
do	do	k7c2	do
města	město	k1gNnSc2	město
Zemer	Zemra	k1gFnPc2	Zemra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nacházela	nacházet	k5eAaImAgFnS	nacházet
se	se	k3xPyFc4	se
nadmořské	nadmořský	k2eAgFnSc3d1	nadmořská
výšce	výška	k1gFnSc3	výška
cca	cca	kA	cca
140	[number]	k4	140
metrů	metr	k1gInPc2	metr
na	na	k7c4	na
pomezí	pomezí	k1gNnSc4	pomezí
pobřežní	pobřežní	k2eAgFnSc2d1	pobřežní
nížiny	nížina	k1gFnSc2	nížina
(	(	kIx(	(
<g/>
Šaronská	Šaronský	k2eAgFnSc1d1	Šaronský
planina	planina	k1gFnSc1	planina
<g/>
)	)	kIx)	)
a	a	k8xC	a
pahorků	pahorek	k1gInPc2	pahorek
v	v	k7c6	v
předpolí	předpolí	k1gNnSc6	předpolí
Samařska	Samařsko	k1gNnSc2	Samařsko
<g/>
,	,	kIx,	,
cca	cca	kA	cca
40	[number]	k4	40
kilometrů	kilometr	k1gInPc2	kilometr
severovýchodně	severovýchodně	k6eAd1	severovýchodně
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
Tel	tel	kA	tel
Avivu	Aviv	k1gInSc2	Aviv
a	a	k8xC	a
cca	cca	kA	cca
15	[number]	k4	15
kilometrů	kilometr	k1gInPc2	kilometr
východně	východně	k6eAd1	východně
od	od	k7c2	od
Netanje	Netanj	k1gFnSc2	Netanj
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
situována	situovat	k5eAaBmNgFnS	situovat
na	na	k7c6	na
Zelené	Zelené	k2eAgFnSc6d1	Zelené
linii	linie	k1gFnSc6	linie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
Izrael	Izrael	k1gInSc4	Izrael
v	v	k7c6	v
jeho	jeho	k3xOp3gFnPc6	jeho
mezinárodně	mezinárodně	k6eAd1	mezinárodně
uznávaných	uznávaný	k2eAgFnPc6d1	uznávaná
hranicích	hranice	k1gFnPc6	hranice
od	od	k7c2	od
území	území	k1gNnSc2	území
Západního	západní	k2eAgInSc2d1	západní
břehu	břeh	k1gInSc2	břeh
Jordánu	Jordán	k1gInSc2	Jordán
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
dopravní	dopravní	k2eAgFnSc4d1	dopravní
síť	síť	k1gFnSc4	síť
byla	být	k5eAaImAgFnS	být
napojena	napojit	k5eAaPmNgFnS	napojit
pomocí	pomocí	k7c2	pomocí
místní	místní	k2eAgFnSc2d1	místní
silnice	silnice	k1gFnSc2	silnice
číslo	číslo	k1gNnSc1	číslo
574	[number]	k4	574
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ležela	ležet	k5eAaImAgFnS	ležet
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
nazývané	nazývaný	k2eAgInPc1d1	nazývaný
Trojúhelník	trojúhelník	k1gInSc1	trojúhelník
<g/>
,	,	kIx,	,
obývané	obývaný	k2eAgNnSc1d1	obývané
izraelskými	izraelský	k2eAgMnPc7d1	izraelský
Araby	Arab	k1gMnPc7	Arab
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
po	po	k7c6	po
podepsání	podepsání	k1gNnSc6	podepsání
dohod	dohoda	k1gFnPc2	dohoda
o	o	k7c4	o
příměří	příměří	k1gNnSc4	příměří
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
ukončily	ukončit	k5eAaPmAgInP	ukončit
první	první	k4xOgFnSc4	první
arabsko-izraelskou	arabskozraelský	k2eAgFnSc4d1	arabsko-izraelská
válku	válka	k1gFnSc4	válka
<g/>
,	,	kIx,	,
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
státu	stát	k1gInSc2	stát
Izrael	Izrael	k1gInSc1	Izrael
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
ale	ale	k8xC	ale
místní	místní	k2eAgFnSc1d1	místní
arabská	arabský	k2eAgFnSc1d1	arabská
populace	populace	k1gFnSc1	populace
byla	být	k5eAaImAgFnS	být
zachována	zachovat	k5eAaPmNgFnS	zachovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
byla	být	k5eAaImAgFnS	být
Mardža	Mardža	k1gFnSc1	Mardža
(	(	kIx(	(
<g/>
společně	společně	k6eAd1	společně
s	s	k7c7	s
nedalekými	daleký	k2eNgFnPc7d1	nedaleká
vesnicemi	vesnice	k1gFnPc7	vesnice
Jama	Jamus	k1gMnSc2	Jamus
<g/>
,	,	kIx,	,
Bir	Bir	k1gMnSc2	Bir
al-Sika	al-Sik	k1gMnSc2	al-Sik
a	a	k8xC	a
Ibtan	Ibtan	k1gInSc4	Ibtan
<g/>
)	)	kIx)	)
sloučena	sloučit	k5eAaPmNgFnS	sloučit
do	do	k7c2	do
místní	místní	k2eAgFnSc2d1	místní
rady	rada	k1gFnSc2	rada
(	(	kIx(	(
<g/>
malého	malý	k2eAgNnSc2d1	malé
města	město	k1gNnSc2	město
<g/>
)	)	kIx)	)
Zemer	Zemer	k1gInSc1	Zemer
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
má	mít	k5eAaImIp3nS	mít
ovšem	ovšem	k9	ovšem
podobu	podoba	k1gFnSc4	podoba
volné	volný	k2eAgFnSc2d1	volná
aglomerace	aglomerace	k1gFnSc2	aglomerace
rozptýlené	rozptýlený	k2eAgFnSc2d1	rozptýlená
venkovské	venkovský	k2eAgFnSc2d1	venkovská
zástavby	zástavba	k1gFnSc2	zástavba
a	a	k8xC	a
bývalá	bývalý	k2eAgFnSc1d1	bývalá
vesnice	vesnice	k1gFnSc1	vesnice
Mardža	Mardžum	k1gNnSc2	Mardžum
si	se	k3xPyFc3	se
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
udržuje	udržovat	k5eAaImIp3nS	udržovat
urbanistickou	urbanistický	k2eAgFnSc4d1	urbanistická
samostatnost	samostatnost	k1gFnSc4	samostatnost
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
na	na	k7c6	na
jihovýchodním	jihovýchodní	k2eAgInSc6d1	jihovýchodní
okraji	okraj	k1gInSc6	okraj
Zemeru	Zemer	k1gInSc2	Zemer
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Zemer	Zemer	k1gMnSc1	Zemer
</s>
</p>
