<p>
<s>
Panthera	Panthera	k1gFnSc1	Panthera
blytheae	blythea	k1gInSc2	blythea
je	být	k5eAaImIp3nS	být
vyhynulý	vyhynulý	k2eAgInSc1d1	vyhynulý
druh	druh	k1gInSc1	druh
velké	velký	k2eAgFnSc2d1	velká
kočkovité	kočkovitý	k2eAgFnSc2d1	kočkovitá
šelmy	šelma	k1gFnSc2	šelma
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Panthera	Panther	k1gMnSc2	Panther
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
žil	žít	k5eAaImAgInS	žít
před	před	k7c7	před
přibližně	přibližně	k6eAd1	přibližně
5.95	[number]	k4	5.95
<g/>
–	–	k?	–
<g/>
4,1	[number]	k4	4,1
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
během	během	k7c2	během
svrchního	svrchní	k2eAgInSc2d1	svrchní
miocénu	miocén	k1gInSc2	miocén
a	a	k8xC	a
spodního	spodní	k2eAgInSc2d1	spodní
pliocénu	pliocén	k1gInSc2	pliocén
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
nejstarší	starý	k2eAgInSc1d3	nejstarší
známý	známý	k2eAgInSc1d1	známý
druh	druh	k1gInSc1	druh
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Panthera	Panthero	k1gNnSc2	Panthero
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
středně	středně	k6eAd1	středně
velká	velký	k2eAgFnSc1d1	velká
šelma	šelma	k1gFnSc1	šelma
(	(	kIx(	(
<g/>
cca	cca	kA	cca
15	[number]	k4	15
až	až	k9	až
30	[number]	k4	30
kg	kg	kA	kg
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zřejmě	zřejmě	k6eAd1	zřejmě
spřízněná	spřízněný	k2eAgFnSc1d1	spřízněná
s	s	k7c7	s
irbisem	irbis	k1gInSc7	irbis
(	(	kIx(	(
<g/>
Panthera	Panthera	k1gFnSc1	Panthera
uncia	uncia	k1gFnSc1	uncia
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
fosilie	fosilie	k1gFnSc1	fosilie
Panthera	Panthera	k1gFnSc1	Panthera
blytheae	blytheae	k1gFnSc1	blytheae
byla	být	k5eAaImAgFnS	být
objevena	objevit	k5eAaPmNgFnS	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
týmem	tým	k1gInSc7	tým
vědců	vědec	k1gMnPc2	vědec
z	z	k7c2	z
Amerického	americký	k2eAgNnSc2d1	americké
muzea	muzeum	k1gNnSc2	muzeum
přírodní	přírodní	k2eAgFnSc2d1	přírodní
historie	historie	k1gFnSc2	historie
(	(	kIx(	(
<g/>
American	American	k1gMnSc1	American
Museum	museum	k1gNnSc1	museum
of	of	k?	of
Natural	Natural	k?	Natural
History	Histor	k1gMnPc7	Histor
<g/>
)	)	kIx)	)
vedeným	vedený	k2eAgMnSc7d1	vedený
Jackem	Jacek	k1gMnSc7	Jacek
Tsengem	Tseng	k1gInSc7	Tseng
v	v	k7c6	v
regionu	region	k1gInSc6	region
Zanda	Zando	k1gNnSc2	Zando
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
Tibetské	tibetský	k2eAgFnSc2d1	tibetská
náhorní	náhorní	k2eAgFnSc2d1	náhorní
plošiny	plošina	k1gFnSc2	plošina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Charakteristika	charakteristikon	k1gNnSc2	charakteristikon
==	==	k?	==
</s>
</p>
<p>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
nálezem	nález	k1gInSc7	nález
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
dobře	dobře	k6eAd1	dobře
zachovalá	zachovalý	k2eAgFnSc1d1	zachovalá
fosílie	fosílie	k1gFnSc1	fosílie
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
jedné	jeden	k4xCgFnSc2	jeden
lebky	lebka	k1gFnSc2	lebka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
mírně	mírně	k6eAd1	mírně
zdeformovaná	zdeformovaný	k2eAgFnSc1d1	zdeformovaná
nadložím	nadloží	k1gNnSc7	nadloží
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
má	mít	k5eAaImIp3nS	mít
zploštělý	zploštělý	k2eAgInSc4d1	zploštělý
tvar	tvar	k1gInSc4	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
byly	být	k5eAaImAgFnP	být
objeveny	objevit	k5eAaPmNgInP	objevit
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
několika	několik	k4yIc2	několik
dalších	další	k2eAgMnPc2d1	další
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Zkoumání	zkoumání	k1gNnSc1	zkoumání
lebky	lebka	k1gFnSc2	lebka
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
druh	druh	k1gInSc1	druh
má	mít	k5eAaImIp3nS	mít
řadu	řada	k1gFnSc4	řada
rysů	rys	k1gInPc2	rys
běžných	běžný	k2eAgInPc2d1	běžný
u	u	k7c2	u
jiných	jiný	k2eAgInPc2d1	jiný
druhů	druh	k1gInPc2	druh
Panthera	Panthero	k1gNnSc2	Panthero
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
švu	šev	k1gInSc2	šev
nacházejícího	nacházející	k2eAgInSc2d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c6	na
postorbitalním	postorbitalní	k2eAgNnSc6d1	postorbitalní
zúžení	zúžení	k1gNnSc6	zúžení
a	a	k8xC	a
absence	absence	k1gFnSc1	absence
přední	přední	k2eAgFnSc2d1	přední
boule	boule	k1gFnSc2	boule
převislé	převislý	k2eAgFnSc2d1	převislá
přes	přes	k7c4	přes
infraorbitalní	infraorbitalný	k2eAgMnPc1d1	infraorbitalný
kanál	kanál	k1gInSc4	kanál
<g/>
.	.	kIx.	.
</s>
<s>
Druh	druh	k1gInSc4	druh
byl	být	k5eAaImAgMnS	být
nazván	nazván	k2eAgMnSc1d1	nazván
Panthera	Panther	k1gMnSc4	Panther
blytheae	blythea	k1gFnSc2	blythea
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
Blythe	Blyth	k1gFnSc2	Blyth
Haagaové	Haagaové	k2eAgFnSc2d1	Haagaové
<g/>
,	,	kIx,	,
milovnice	milovnice	k1gFnPc4	milovnice
irbisů	irbis	k1gInPc2	irbis
a	a	k8xC	a
dcery	dcera	k1gFnSc2	dcera
významných	významný	k2eAgMnPc2d1	významný
mecenášů	mecenáš	k1gMnPc2	mecenáš
Národního	národní	k2eAgNnSc2d1	národní
historického	historický	k2eAgNnSc2d1	historické
muzea	muzeum	k1gNnSc2	muzeum
(	(	kIx(	(
<g/>
Natural	Natural	k?	Natural
History	Histor	k1gInPc7	Histor
Museum	museum	k1gNnSc1	museum
<g/>
)	)	kIx)	)
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angeles	k1gInSc4	Angeles
County	Counta	k1gFnSc2	Counta
<g/>
.	.	kIx.	.
<g/>
Velikost	velikost	k1gFnSc1	velikost
této	tento	k3xDgFnSc2	tento
kočky	kočka	k1gFnSc2	kočka
byla	být	k5eAaImAgFnS	být
přibližně	přibližně	k6eAd1	přibližně
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
levharta	levhart	k1gMnSc2	levhart
obláčkového	obláčkový	k2eAgMnSc2d1	obláčkový
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
o	o	k7c4	o
něco	něco	k3yInSc4	něco
menší	malý	k2eAgFnSc1d2	menší
než	než	k8xS	než
velikost	velikost	k1gFnSc1	velikost
irbisa	irbisa	k1gFnSc1	irbisa
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
však	však	k9	však
o	o	k7c4	o
odhad	odhad	k1gInSc4	odhad
založený	založený	k2eAgInSc4d1	založený
jen	jen	k6eAd1	jen
na	na	k7c6	na
základě	základ	k1gInSc6	základ
relativní	relativní	k2eAgFnSc2d1	relativní
velikosti	velikost	k1gFnSc2	velikost
lebky	lebka	k1gFnSc2	lebka
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
mírně	mírně	k6eAd1	mírně
nepřesný	přesný	k2eNgInSc4d1	nepřesný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
ostatním	ostatní	k2eAgMnPc3d1	ostatní
druhům	druh	k1gMnPc3	druh
Panthera	Panthero	k1gNnPc4	Panthero
==	==	k?	==
</s>
</p>
<p>
<s>
Nejbližší	blízký	k2eAgMnSc1d3	nejbližší
žijící	žijící	k2eAgMnSc1d1	žijící
příbuzný	příbuzný	k1gMnSc1	příbuzný
Panthera	Panthero	k1gNnSc2	Panthero
blytheae	blytheae	k1gNnSc2	blytheae
je	být	k5eAaImIp3nS	být
irbis	irbis	k1gFnSc1	irbis
<g/>
.	.	kIx.	.
</s>
<s>
Hovoří	hovořit	k5eAaImIp3nS	hovořit
pro	pro	k7c4	pro
to	ten	k3xDgNnSc4	ten
důkazy	důkaz	k1gInPc4	důkaz
na	na	k7c6	na
základě	základ	k1gInSc6	základ
podobnosti	podobnost	k1gFnSc2	podobnost
lebeční	lebeční	k2eAgFnSc2d1	lebeční
struktury	struktura	k1gFnSc2	struktura
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
sdílená	sdílený	k2eAgFnSc1d1	sdílená
oblast	oblast	k1gFnSc1	oblast
rozšíření	rozšíření	k1gNnSc2	rozšíření
na	na	k7c6	na
Tibetské	tibetský	k2eAgFnSc6d1	tibetská
náhorní	náhorní	k2eAgFnSc6d1	náhorní
plošině	plošina	k1gFnSc6	plošina
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
stáří	stář	k1gFnPc2	stář
fosilií	fosilie	k1gFnPc2	fosilie
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Panthera	Panthera	k1gFnSc1	Panthera
blytheae	blytheae	k1gFnSc1	blytheae
měl	mít	k5eAaImAgInS	mít
velmi	velmi	k6eAd1	velmi
podobnou	podobný	k2eAgFnSc4d1	podobná
skladbu	skladba	k1gFnSc4	skladba
kořisti	kořist	k1gFnSc2	kořist
jako	jako	k8xS	jako
irbis	irbis	k1gFnSc2	irbis
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
většina	většina	k1gFnSc1	většina
druhů	druh	k1gInPc2	druh
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
loví	lovit	k5eAaImIp3nS	lovit
<g/>
,	,	kIx,	,
existovala	existovat	k5eAaImAgFnS	existovat
už	už	k6eAd1	už
před	před	k7c7	před
vyhynutím	vyhynutí	k1gNnSc7	vyhynutí
Panthera	Panther	k1gMnSc2	Panther
blytheae	blythea	k1gMnSc2	blythea
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
není	být	k5eNaImIp3nS	být
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
sesterský	sesterský	k2eAgInSc4d1	sesterský
taxon	taxon	k1gInSc4	taxon
či	či	k8xC	či
o	o	k7c4	o
předka	předek	k1gMnSc4	předek
irbisa	irbis	k1gMnSc4	irbis
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
autoři	autor	k1gMnPc1	autor
navíc	navíc	k6eAd1	navíc
zpochybňují	zpochybňovat	k5eAaImIp3nP	zpochybňovat
jednoznačné	jednoznačný	k2eAgNnSc4d1	jednoznačné
zařazení	zařazení	k1gNnSc4	zařazení
Panthera	Panther	k1gMnSc2	Panther
blytheae	blythea	k1gMnSc2	blythea
mezi	mezi	k7c4	mezi
velké	velký	k2eAgFnPc4d1	velká
kočky	kočka	k1gFnPc4	kočka
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
podobné	podobný	k2eAgInPc1d1	podobný
znaky	znak	k1gInPc1	znak
na	na	k7c6	na
lebce	lebka	k1gFnSc6	lebka
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
i	i	k9	i
někteří	některý	k3yIgMnPc1	některý
zástupci	zástupce	k1gMnPc1	zástupce
koček	kočka	k1gFnPc2	kočka
malých	malý	k2eAgFnPc2d1	malá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Fylogeneze	fylogeneze	k1gFnSc1	fylogeneze
podčeledi	podčeleď	k1gFnSc2	podčeleď
Pantherinae	Pantherina	k1gFnSc2	Pantherina
podle	podle	k7c2	podle
Tsenga	Tsenga	k1gFnSc1	Tsenga
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
===	===	k?	===
Důkazy	důkaz	k1gInPc1	důkaz
pro	pro	k7c4	pro
asijský	asijský	k2eAgInSc4d1	asijský
původ	původ	k1gInSc4	původ
rodu	rod	k1gInSc2	rod
Panthera	Panthera	k1gFnSc1	Panthera
===	===	k?	===
</s>
</p>
<p>
<s>
Divergence	divergence	k1gFnSc1	divergence
podčeledi	podčeleď	k1gFnSc2	podčeleď
Pantherinae	Pantherina	k1gFnSc2	Pantherina
od	od	k7c2	od
zbytku	zbytek	k1gInSc2	zbytek
Felidae	Felida	k1gMnSc2	Felida
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
mnohem	mnohem	k6eAd1	mnohem
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
předpokládaly	předpokládat	k5eAaImAgInP	předpokládat
nedávné	dávný	k2eNgInPc1d1	nedávný
odhady	odhad	k1gInPc1	odhad
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
současných	současný	k2eAgFnPc2d1	současná
zkoumání	zkoumání	k1gNnSc4	zkoumání
přibližně	přibližně	k6eAd1	přibližně
před	před	k7c4	před
16,4	[number]	k4	16,4
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
lety	let	k1gInPc7	let
<g/>
.	.	kIx.	.
</s>
<s>
Rod	rod	k1gInSc1	rod
Panthera	Panthero	k1gNnSc2	Panthero
pak	pak	k6eAd1	pak
derivoval	derivovat	k5eAaBmAgInS	derivovat
asi	asi	k9	asi
před	před	k7c7	před
10	[number]	k4	10
až	až	k8xS	až
11	[number]	k4	11
miliony	milion	k4xCgInPc7	milion
lety	léto	k1gNnPc7	léto
a	a	k8xC	a
k	k	k7c3	k
oddělení	oddělení	k1gNnSc3	oddělení
kladu	klást	k5eAaImIp1nS	klást
Panthera	Panthera	k1gFnSc1	Panthera
tigris	tigris	k1gFnSc1	tigris
<g/>
,	,	kIx,	,
Panthera	Panthera	k1gFnSc1	Panthera
uncia	uncia	k1gFnSc1	uncia
a	a	k8xC	a
Panthera	Panthera	k1gFnSc1	Panthera
blytheae	blytheae	k1gFnSc1	blytheae
došlo	dojít	k5eAaPmAgNnS	dojít
asi	asi	k9	asi
před	před	k7c7	před
8	[number]	k4	8
až	až	k8xS	až
9	[number]	k4	9
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
irbisem	irbis	k1gInSc7	irbis
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
oddělili	oddělit	k5eAaPmAgMnP	oddělit
od	od	k7c2	od
tygra	tygr	k1gMnSc2	tygr
asi	asi	k9	asi
před	před	k7c7	před
7	[number]	k4	7
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Analýza	analýza	k1gFnSc1	analýza
umístění	umístění	k1gNnSc2	umístění
fosilií	fosilie	k1gFnPc2	fosilie
Panthera	Panthero	k1gNnSc2	Panthero
blytheae	blytheae	k1gFnPc2	blytheae
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
dalších	další	k2eAgInPc2d1	další
již	již	k6eAd1	již
vyhynulých	vyhynulý	k2eAgInPc2d1	vyhynulý
druhů	druh	k1gInPc2	druh
Panthera	Panthero	k1gNnSc2	Panthero
<g/>
,	,	kIx,	,
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
první	první	k4xOgMnPc1	první
zástupci	zástupce	k1gMnPc1	zástupce
rodu	rod	k1gInSc2	rod
Panthera	Panthero	k1gNnSc2	Panthero
se	se	k3xPyFc4	se
vyvinuli	vyvinout	k5eAaPmAgMnP	vyvinout
z	z	k7c2	z
kočkovitých	kočkovití	k1gMnPc2	kočkovití
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
a	a	k8xC	a
severní	severní	k2eAgFnSc6d1	severní
Asii	Asie	k1gFnSc6	Asie
nebo	nebo	k8xC	nebo
holarkticé	holarkticý	k2eAgFnSc6d1	holarkticý
oblasti	oblast	k1gFnSc6	oblast
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
a	a	k8xC	a
následně	následně	k6eAd1	následně
migrovali	migrovat	k5eAaImAgMnP	migrovat
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Panthera	Panther	k1gMnSc2	Panther
blytheae	blythea	k1gMnSc2	blythea
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
