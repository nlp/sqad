<s>
Panthera	Panthera	k1gFnSc1	Panthera
blytheae	blythea	k1gInSc2	blythea
je	být	k5eAaImIp3nS	být
vyhynulý	vyhynulý	k2eAgInSc1d1	vyhynulý
druh	druh	k1gInSc1	druh
velké	velký	k2eAgFnSc2d1	velká
kočkovité	kočkovitý	k2eAgFnSc2d1	kočkovitá
šelmy	šelma	k1gFnSc2	šelma
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Panthera	Panther	k1gMnSc2	Panther
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
žil	žít	k5eAaImAgInS	žít
před	před	k7c7	před
přibližně	přibližně	k6eAd1	přibližně
5.95	[number]	k4	5.95
<g/>
–	–	k?	–
<g/>
4,1	[number]	k4	4,1
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
během	během	k7c2	během
svrchního	svrchní	k2eAgInSc2d1	svrchní
miocénu	miocén	k1gInSc2	miocén
a	a	k8xC	a
spodního	spodní	k2eAgInSc2d1	spodní
pliocénu	pliocén	k1gInSc2	pliocén
<g/>
.	.	kIx.	.
</s>
