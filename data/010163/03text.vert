<p>
<s>
Stanley	Stanley	k1gInPc1	Stanley
Kubrick	Kubricka	k1gFnPc2	Kubricka
(	(	kIx(	(
<g/>
26	[number]	k4	26
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1928	[number]	k4	1928
Bronx	Bronx	k1gInSc1	Bronx
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
USA	USA	kA	USA
–	–	k?	–
7	[number]	k4	7
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
Harpenden	Harpendno	k1gNnPc2	Harpendno
<g/>
,	,	kIx,	,
hrabství	hrabství	k1gNnSc1	hrabství
Hertford	Hertforda	k1gFnPc2	Hertforda
<g/>
,	,	kIx,	,
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
režisér	režisér	k1gMnSc1	režisér
a	a	k8xC	a
fotograf	fotograf	k1gMnSc1	fotograf
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
lékaře	lékař	k1gMnSc2	lékař
Jacka	Jacko	k1gNnSc2	Jacko
Kubricka	Kubricko	k1gNnSc2	Kubricko
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
špatné	špatný	k2eAgInPc4d1	špatný
školní	školní	k2eAgInPc4d1	školní
výsledky	výsledek	k1gInPc4	výsledek
byl	být	k5eAaImAgInS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
velice	velice	k6eAd1	velice
inteligentního	inteligentní	k2eAgMnSc4d1	inteligentní
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
ho	on	k3xPp3gMnSc4	on
otec	otec	k1gMnSc1	otec
naučil	naučit	k5eAaPmAgMnS	naučit
hrát	hrát	k5eAaImF	hrát
šachy	šach	k1gInPc4	šach
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Kubricka	Kubricko	k1gNnSc2	Kubricko
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
vynikající	vynikající	k2eAgMnSc1d1	vynikající
hráč	hráč	k1gMnSc1	hráč
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
jeho	jeho	k3xOp3gInPc1	jeho
filmy	film	k1gInPc1	film
skutečně	skutečně	k6eAd1	skutečně
připomínají	připomínat	k5eAaImIp3nP	připomínat
partii	partie	k1gFnSc4	partie
šachů	šach	k1gInPc2	šach
<g/>
.	.	kIx.	.
</s>
<s>
Vše	všechen	k3xTgNnSc1	všechen
je	být	k5eAaImIp3nS	být
důkladně	důkladně	k6eAd1	důkladně
a	a	k8xC	a
pečlivě	pečlivě	k6eAd1	pečlivě
promyšleno	promyslet	k5eAaPmNgNnS	promyslet
dopředu	dopředu	k6eAd1	dopředu
<g/>
.	.	kIx.	.
</s>
<s>
Kubrick	Kubrick	k1gMnSc1	Kubrick
dostal	dostat	k5eAaPmAgMnS	dostat
ke	k	k7c3	k
třináctým	třináctý	k4xOgFnPc3	třináctý
narozeninám	narozeniny	k1gFnPc3	narozeniny
fotoaparát	fotoaparát	k1gInSc1	fotoaparát
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
výjimečné	výjimečný	k2eAgNnSc4d1	výjimečné
nadání	nadání	k1gNnSc4	nadání
a	a	k8xC	a
v	v	k7c6	v
sedmnácti	sedmnáct	k4xCc6	sedmnáct
ho	on	k3xPp3gMnSc4	on
dokonce	dokonce	k9	dokonce
přijali	přijmout	k5eAaPmAgMnP	přijmout
do	do	k7c2	do
časopisu	časopis	k1gInSc2	časopis
Look	Looka	k1gFnPc2	Looka
jako	jako	k8xS	jako
učedníka	učedník	k1gMnSc2	učedník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
vyměnil	vyměnit	k5eAaPmAgMnS	vyměnit
fotoaparát	fotoaparát	k1gInSc4	fotoaparát
za	za	k7c4	za
filmovou	filmový	k2eAgFnSc4d1	filmová
kameru	kamera	k1gFnSc4	kamera
a	a	k8xC	a
natočil	natočit	k5eAaBmAgMnS	natočit
krátké	krátký	k2eAgInPc4d1	krátký
dokumentární	dokumentární	k2eAgInPc4d1	dokumentární
snímky	snímek	k1gInPc4	snímek
o	o	k7c6	o
životě	život	k1gInSc6	život
boxerů	boxer	k1gMnPc2	boxer
(	(	kIx(	(
<g/>
Day	Day	k1gMnPc2	Day
of	of	k?	of
the	the	k?	the
Fight	Fight	k1gInSc1	Fight
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
o	o	k7c6	o
knězi	kněz	k1gMnSc6	kněz
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
musí	muset	k5eAaImIp3nS	muset
na	na	k7c6	na
nejvzdálenější	vzdálený	k2eAgFnSc6d3	nejvzdálenější
farnosti	farnost	k1gFnSc6	farnost
létat	létat	k5eAaImF	létat
letadlem	letadlo	k1gNnSc7	letadlo
(	(	kIx(	(
<g/>
Flying	Flying	k1gInSc1	Flying
Padre	Padr	k1gMnSc5	Padr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
o	o	k7c6	o
námořnických	námořnický	k2eAgInPc6d1	námořnický
odborech	odbor	k1gInPc6	odbor
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Seafarers	Seafarersa	k1gFnPc2	Seafarersa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
prvním	první	k4xOgInSc7	první
celovečerním	celovečerní	k2eAgInSc7d1	celovečerní
dílem	díl	k1gInSc7	díl
byl	být	k5eAaImAgInS	být
Fear	Fear	k1gInSc1	Fear
and	and	k?	and
Desire	Desir	k1gInSc5	Desir
o	o	k7c6	o
fiktivní	fiktivní	k2eAgFnSc6d1	fiktivní
válce	válka	k1gFnSc6	válka
ve	v	k7c6	v
fiktivním	fiktivní	k2eAgInSc6d1	fiktivní
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stanley	Stanle	k1gMnPc4	Stanle
Kubrick	Kubricko	k1gNnPc2	Kubricko
je	být	k5eAaImIp3nS	být
znám	znát	k5eAaImIp1nS	znát
svým	svůj	k3xOyFgNnSc7	svůj
puntičkářstvím	puntičkářství	k1gNnSc7	puntičkářství
a	a	k8xC	a
kritičností	kritičnost	k1gFnSc7	kritičnost
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc4	několik
jeho	jeho	k3xOp3gInPc2	jeho
filmů	film	k1gInPc2	film
bylo	být	k5eAaImAgNnS	být
cenzurováno	cenzurován	k2eAgNnSc1d1	cenzurováno
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
<g/>
.	.	kIx.	.
</s>
<s>
Snímek	snímek	k1gInSc1	snímek
Stezky	stezka	k1gFnSc2	stezka
slávy	sláva	k1gFnSc2	sláva
o	o	k7c6	o
popravě	poprava	k1gFnSc6	poprava
tří	tři	k4xCgMnPc2	tři
francouzských	francouzský	k2eAgMnPc2d1	francouzský
vojáků	voják	k1gMnPc2	voják
v	v	k7c6	v
období	období	k1gNnSc6	období
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
léta	léto	k1gNnSc2	léto
zakázán	zakázat	k5eAaPmNgInS	zakázat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lolita	Lolita	k1gMnSc1	Lolita
podle	podle	k7c2	podle
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
románu	román	k1gInSc2	román
Vladimira	Vladimir	k1gInSc2	Vladimir
Nabokova	Nabokův	k2eAgInSc2d1	Nabokův
o	o	k7c6	o
lásce	láska	k1gFnSc6	láska
profesora	profesor	k1gMnSc2	profesor
ke	k	k7c3	k
čtrnáctileté	čtrnáctiletý	k2eAgFnSc3d1	čtrnáctiletá
nymfičce	nymfička	k1gFnSc3	nymfička
musela	muset	k5eAaImAgFnS	muset
být	být	k5eAaImF	být
upravována	upravovat	k5eAaImNgFnS	upravovat
samotným	samotný	k2eAgInSc7d1	samotný
Kubrickem	Kubrick	k1gInSc7	Kubrick
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
jakýkoliv	jakýkoliv	k3yIgInSc4	jakýkoliv
nedobrovolný	dobrovolný	k2eNgInSc4d1	nedobrovolný
zásah	zásah	k1gInSc4	zásah
do	do	k7c2	do
svého	svůj	k3xOyFgNnSc2	svůj
díla	dílo	k1gNnSc2	dílo
odmítal	odmítat	k5eAaImAgMnS	odmítat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
filmu	film	k1gInSc2	film
Mechanický	mechanický	k2eAgInSc1d1	mechanický
pomeranč	pomeranč	k1gInSc1	pomeranč
britské	britský	k2eAgInPc1d1	britský
úřady	úřad	k1gInPc1	úřad
tvrdily	tvrdit	k5eAaImAgInP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
film	film	k1gInSc1	film
rozpoutal	rozpoutat	k5eAaPmAgInS	rozpoutat
vlnu	vlna	k1gFnSc4	vlna
zločinů	zločin	k1gInPc2	zločin
páchaných	páchaný	k2eAgInPc2d1	páchaný
mladistvými	mladistvý	k1gMnPc7	mladistvý
<g/>
.	.	kIx.	.
</s>
<s>
Kubrickově	Kubrickův	k2eAgFnSc3d1	Kubrickova
rodině	rodina	k1gFnSc3	rodina
bylo	být	k5eAaImAgNnS	být
vyhrožováno	vyhrožován	k2eAgNnSc1d1	vyhrožováno
smrtí	smrt	k1gFnSc7	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Tvůrce	tvůrce	k1gMnSc1	tvůrce
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
film	film	k1gInSc1	film
se	se	k3xPyFc4	se
až	až	k9	až
do	do	k7c2	do
jeho	jeho	k3xOp3gFnSc2	jeho
smrti	smrt	k1gFnSc2	smrt
nesmí	smět	k5eNaImIp3nS	smět
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
promítat	promítat	k5eAaImF	promítat
v	v	k7c6	v
kinech	kino	k1gNnPc6	kino
a	a	k8xC	a
v	v	k7c6	v
televizi	televize	k1gFnSc6	televize
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kubrickovým	Kubrickův	k2eAgInSc7d1	Kubrickův
posledním	poslední	k2eAgInSc7d1	poslední
filmem	film	k1gInSc7	film
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
psychologický	psychologický	k2eAgInSc1d1	psychologický
snímek	snímek	k1gInSc1	snímek
Eyes	Eyesa	k1gFnPc2	Eyesa
Wide	Wid	k1gFnSc2	Wid
Shut	Shuta	k1gFnPc2	Shuta
<g/>
.	.	kIx.	.
</s>
<s>
Stanley	Stanlea	k1gMnSc2	Stanlea
Kubrick	Kubrick	k1gMnSc1	Kubrick
zemřel	zemřít	k5eAaPmAgMnS	zemřít
ve	v	k7c6	v
spánku	spánek	k1gInSc6	spánek
na	na	k7c4	na
infarkt	infarkt	k1gInSc4	infarkt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ocenění	ocenění	k1gNnSc1	ocenění
==	==	k?	==
</s>
</p>
<p>
<s>
Kubrick	Kubrick	k1gMnSc1	Kubrick
získal	získat	k5eAaPmAgMnS	získat
za	za	k7c4	za
celou	celá	k1gFnSc4	celá
svou	svůj	k3xOyFgFnSc4	svůj
kariéru	kariéra	k1gFnSc4	kariéra
jednoho	jeden	k4xCgMnSc2	jeden
Oscara	Oscar	k1gMnSc2	Oscar
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
za	za	k7c4	za
vizuální	vizuální	k2eAgInPc4d1	vizuální
efekty	efekt	k1gInPc4	efekt
k	k	k7c3	k
2001	[number]	k4	2001
<g/>
:	:	kIx,	:
Vesmírná	vesmírný	k2eAgFnSc1d1	vesmírná
odysea	odysea	k1gFnSc1	odysea
<g/>
.	.	kIx.	.
</s>
<s>
Nominován	nominovat	k5eAaBmNgInS	nominovat
byl	být	k5eAaImAgInS	být
však	však	k9	však
dvanáctkrát	dvanáctkrát	k6eAd1	dvanáctkrát
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
Vesmírná	vesmírný	k2eAgFnSc1d1	vesmírná
odysea	odysea	k1gFnSc1	odysea
zdokonalil	zdokonalit	k5eAaPmAgMnS	zdokonalit
metodu	metoda	k1gFnSc4	metoda
přední	přední	k2eAgFnSc2d1	přední
projekce	projekce	k1gFnSc2	projekce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Spalující	spalující	k2eAgFnSc1d1	spalující
touha	touha	k1gFnSc1	touha
(	(	kIx(	(
<g/>
Eyes	Eyes	k1gInSc1	Eyes
Wide	Wide	k1gFnPc2	Wide
Shut	Shutum	k1gNnPc2	Shutum
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Olověná	olověný	k2eAgFnSc1d1	olověná
vesta	vesta	k1gFnSc1	vesta
(	(	kIx(	(
<g/>
Full	Full	k1gMnSc1	Full
Metal	metat	k5eAaImAgMnS	metat
Jacket	Jacket	k1gInSc4	Jacket
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Osvícení	osvícení	k1gNnSc1	osvícení
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Shining	Shining	k1gInSc1	Shining
<g/>
,	,	kIx,	,
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Barry	Barr	k1gInPc1	Barr
Lyndon	Lyndona	k1gFnPc2	Lyndona
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mechanický	mechanický	k2eAgInSc1d1	mechanický
pomeranč	pomeranč	k1gInSc1	pomeranč
(	(	kIx(	(
<g/>
A	a	k9	a
Clockwork	Clockwork	k1gInSc1	Clockwork
Orange	Orange	k1gFnPc2	Orange
<g/>
,	,	kIx,	,
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2001	[number]	k4	2001
<g/>
:	:	kIx,	:
Vesmírná	vesmírný	k2eAgFnSc1d1	vesmírná
odysea	odysea	k1gFnSc1	odysea
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
:	:	kIx,	:
A	a	k8xC	a
Space	Space	k1gFnSc1	Space
Odyssey	Odyssea	k1gFnSc2	Odyssea
<g/>
,	,	kIx,	,
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Divnoláska	Divnoláska	k1gFnSc1	Divnoláska
aneb	aneb	k?	aneb
jak	jak	k8xC	jak
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
naučil	naučit	k5eAaPmAgMnS	naučit
nedělat	dělat	k5eNaImF	dělat
si	se	k3xPyFc3	se
starosti	starost	k1gFnPc4	starost
a	a	k8xC	a
mít	mít	k5eAaImF	mít
rád	rád	k6eAd1	rád
bombu	bomba	k1gFnSc4	bomba
(	(	kIx(	(
<g/>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Strangelove	Strangelov	k1gInSc5	Strangelov
or	or	k?	or
<g/>
:	:	kIx,	:
How	How	k1gMnSc1	How
I	i	k8xC	i
Learned	Learned	k1gMnSc1	Learned
to	ten	k3xDgNnSc4	ten
Stop	stop	k2eAgInSc1d1	stop
Worrying	Worrying	k1gInSc1	Worrying
and	and	k?	and
Love	lov	k1gInSc5	lov
the	the	k?	the
Bomb	bomba	k1gFnPc2	bomba
<g/>
,	,	kIx,	,
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Lolita	Lolita	k1gFnSc1	Lolita
(	(	kIx(	(
<g/>
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Spartakus	Spartakus	k1gMnSc1	Spartakus
(	(	kIx(	(
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Stezky	stezka	k1gFnPc1	stezka
slávy	sláva	k1gFnSc2	sláva
(	(	kIx(	(
<g/>
Paths	Paths	k1gInSc1	Paths
of	of	k?	of
Glory	Glora	k1gFnSc2	Glora
<g/>
,	,	kIx,	,
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zabíjení	zabíjení	k1gNnSc1	zabíjení
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Killing	Killing	k1gInSc1	Killing
<g/>
,	,	kIx,	,
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Killer	Killer	k1gInSc1	Killer
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Kiss	Kissa	k1gFnPc2	Kissa
(	(	kIx(	(
<g/>
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
The	The	k?	The
Seafarers	Seafarers	k1gInSc1	Seafarers
(	(	kIx(	(
<g/>
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Fear	Fear	k1gInSc1	Fear
and	and	k?	and
Desire	Desir	k1gInSc5	Desir
(	(	kIx(	(
<g/>
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Day	Day	k?	Day
of	of	k?	of
the	the	k?	the
Fight	Fight	k1gInSc1	Fight
(	(	kIx(	(
<g/>
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Flying	Flying	k1gInSc1	Flying
Padre	Padr	k1gInSc5	Padr
(	(	kIx(	(
<g/>
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
<s>
Fotografie	fotografia	k1gFnPc1	fotografia
pro	pro	k7c4	pro
Look	Look	k1gInSc4	Look
od	od	k7c2	od
Stanleyho	Stanley	k1gMnSc2	Stanley
Cubricka	Cubricko	k1gNnSc2	Cubricko
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Stanley	Stanlea	k1gFnSc2	Stanlea
Kubrick	Kubrick	k1gInSc1	Kubrick
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Stanley	Stanlea	k1gFnSc2	Stanlea
Kubrick	Kubricka	k1gFnPc2	Kubricka
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
