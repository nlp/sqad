<p>
<s>
Talmberk	Talmberk	k1gInSc1	Talmberk
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Burg	Burg	k1gMnSc1	Burg
Talenberg	Talenberg	k1gMnSc1	Talenberg
<g/>
,	,	kIx,	,
Burg	Burg	k1gMnSc1	Burg
Talmberg	Talmberg	k1gMnSc1	Talmberg
(	(	kIx(	(
<g/>
1297	[number]	k4	1297
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Burg	Burg	k1gMnSc1	Burg
Tallenberg	Tallenberg	k1gMnSc1	Tallenberg
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zřícenina	zřícenina	k1gFnSc1	zřícenina
hradu	hrad	k1gInSc2	hrad
částečně	částečně	k6eAd1	částečně
zastavěná	zastavěný	k2eAgFnSc1d1	zastavěná
rodinnými	rodinný	k2eAgInPc7d1	rodinný
domy	dům	k1gInPc7	dům
ve	v	k7c6	v
stejnojmenné	stejnojmenný	k2eAgFnSc6d1	stejnojmenná
vesnici	vesnice	k1gFnSc6	vesnice
asi	asi	k9	asi
3	[number]	k4	3
km	km	kA	km
severně	severně	k6eAd1	severně
od	od	k7c2	od
Ratají	Rataj	k1gFnPc2	Rataj
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Kutná	kutný	k2eAgFnSc1d1	Kutná
Hora	hora	k1gFnSc1	hora
<g/>
.	.	kIx.	.
</s>
<s>
Stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
ostrožně	ostrožna	k1gFnSc6	ostrožna
se	s	k7c7	s
strmými	strmý	k2eAgInPc7d1	strmý
svahy	svah	k1gInPc7	svah
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
365	[number]	k4	365
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
je	být	k5eAaImIp3nS	být
chráněn	chránit	k5eAaImNgInS	chránit
jako	jako	k8xS	jako
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Hrad	hrad	k1gInSc1	hrad
postavil	postavit	k5eAaPmAgInS	postavit
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
Arnošt	Arnošt	k1gMnSc1	Arnošt
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
rozrodu	rozrod	k1gInSc2	rozrod
Kouniců	Kouniec	k1gInPc2	Kouniec
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	on	k3xPp3gInSc2	on
rodu	rod	k1gInSc2	rod
patřil	patřit	k5eAaImAgInS	patřit
až	až	k9	až
do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1473	[number]	k4	1473
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
majitel	majitel	k1gMnSc1	majitel
uváděn	uváděn	k2eAgMnSc1d1	uváděn
Bedřich	Bedřich	k1gMnSc1	Bedřich
Ojíř	Ojíř	k1gMnSc1	Ojíř
z	z	k7c2	z
Očedělic	Očedělice	k1gFnPc2	Očedělice
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1483	[number]	k4	1483
Bedřich	Bedřich	k1gMnSc1	Bedřich
ze	z	k7c2	z
Šumburka	Šumburek	k1gMnSc2	Šumburek
a	a	k8xC	a
Talmberka	Talmberka	k1gFnSc1	Talmberka
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1533	[number]	k4	1533
se	se	k3xPyFc4	se
hrad	hrad	k1gInSc1	hrad
uvádí	uvádět	k5eAaImIp3nS	uvádět
jako	jako	k9	jako
pustý	pustý	k2eAgInSc4d1	pustý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Stavební	stavební	k2eAgFnSc1d1	stavební
podoba	podoba	k1gFnSc1	podoba
==	==	k?	==
</s>
</p>
<p>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
novodobé	novodobý	k2eAgFnSc3d1	novodobá
zástavbě	zástavba	k1gFnSc3	zástavba
neznáme	znát	k5eNaImIp1nP	znát
podobu	podoba	k1gFnSc4	podoba
předhradí	předhradí	k1gNnSc2	předhradí
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
od	od	k7c2	od
hradního	hradní	k2eAgNnSc2d1	hradní
jádra	jádro	k1gNnSc2	jádro
odděloval	oddělovat	k5eAaImAgInS	oddělovat
široký	široký	k2eAgInSc1d1	široký
šíjový	šíjový	k2eAgInSc1d1	šíjový
příkop	příkop	k1gInSc1	příkop
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
jádra	jádro	k1gNnSc2	jádro
se	se	k3xPyFc4	se
vstupovalo	vstupovat	k5eAaImAgNnS	vstupovat
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
branou	braný	k2eAgFnSc7d1	braná
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yIgFnSc2	který
se	se	k3xPyFc4	se
zachoval	zachovat	k5eAaPmAgInS	zachovat
zadní	zadní	k2eAgInSc1d1	zadní
portál	portál	k1gInSc1	portál
průjezdu	průjezd	k1gInSc2	průjezd
mladší	mladý	k2eAgFnSc2d2	mladší
budovy	budova	k1gFnSc2	budova
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vyplňovala	vyplňovat	k5eAaImAgFnS	vyplňovat
parkán	parkán	k1gInSc4	parkán
<g/>
.	.	kIx.	.
</s>
<s>
Vlevo	vlevo	k6eAd1	vlevo
od	od	k7c2	od
brány	brána	k1gFnSc2	brána
stál	stát	k5eAaImAgInS	stát
bergfrit	bergfrit	k1gInSc4	bergfrit
snížený	snížený	k2eAgInSc4d1	snížený
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
a	a	k8xC	a
vpravo	vpravo	k6eAd1	vpravo
menší	malý	k2eAgFnSc1d2	menší
věž	věž	k1gFnSc1	věž
částečně	částečně	k6eAd1	částečně
vytápěná	vytápěný	k2eAgFnSc1d1	vytápěná
kachlovými	kachlový	k2eAgNnPc7d1	kachlové
kamny	kamna	k1gNnPc7	kamna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
straně	strana	k1gFnSc6	strana
jádra	jádro	k1gNnSc2	jádro
stál	stát	k5eAaImAgInS	stát
dvouprostorový	dvouprostorový	k2eAgInSc1d1	dvouprostorový
palác	palác	k1gInSc1	palác
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yRgInSc2	který
se	se	k3xPyFc4	se
dochovala	dochovat	k5eAaPmAgFnS	dochovat
valeně	valeně	k6eAd1	valeně
zaklenutá	zaklenutý	k2eAgFnSc1d1	zaklenutá
místnost	místnost	k1gFnSc1	místnost
suterénu	suterén	k1gInSc2	suterén
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc4d1	celý
obvod	obvod	k1gInSc4	obvod
jádra	jádro	k1gNnSc2	jádro
obíhala	obíhat	k5eAaImAgFnS	obíhat
parkánová	parkánový	k2eAgFnSc1d1	Parkánová
hradba	hradba	k1gFnSc1	hradba
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
část	část	k1gFnSc1	část
se	s	k7c7	s
střílnou	střílna	k1gFnSc7	střílna
se	se	k3xPyFc4	se
zachovala	zachovat	k5eAaPmAgFnS	zachovat
před	před	k7c7	před
branou	brána	k1gFnSc7	brána
jádra	jádro	k1gNnSc2	jádro
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přístup	přístup	k1gInSc1	přístup
==	==	k?	==
</s>
</p>
<p>
<s>
Osadou	osada	k1gFnSc7	osada
prochází	procházet	k5eAaImIp3nS	procházet
červeně	červeně	k6eAd1	červeně
značená	značený	k2eAgFnSc1d1	značená
turistická	turistický	k2eAgFnSc1d1	turistická
trasa	trasa	k1gFnSc1	trasa
ze	z	k7c2	z
Sázavy	Sázava	k1gFnSc2	Sázava
do	do	k7c2	do
Ratají	Rataj	k1gFnPc2	Rataj
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
<g/>
.	.	kIx.	.
</s>
<s>
Zbytky	zbytek	k1gInPc1	zbytek
hradu	hrad	k1gInSc2	hrad
jsou	být	k5eAaImIp3nP	být
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
novodobou	novodobý	k2eAgFnSc4d1	novodobá
zástavbu	zástavba	k1gFnSc4	zástavba
částečně	částečně	k6eAd1	částečně
přístupné	přístupný	k2eAgNnSc1d1	přístupné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
SEDLÁČEK	Sedláček	k1gMnSc1	Sedláček
<g/>
,	,	kIx,	,
August	August	k1gMnSc1	August
<g/>
.	.	kIx.	.
</s>
<s>
Hrady	hrad	k1gInPc1	hrad
<g/>
,	,	kIx,	,
zámky	zámek	k1gInPc1	zámek
a	a	k8xC	a
tvrze	tvrz	k1gFnPc1	tvrz
Království	království	k1gNnSc2	království
českého	český	k2eAgNnSc2d1	české
<g/>
:	:	kIx,	:
Čáslavsko	Čáslavsko	k1gNnSc1	Čáslavsko
<g/>
.	.	kIx.	.
</s>
<s>
Svazek	svazek	k1gInSc1	svazek
XII	XII	kA	XII
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Jiří	Jiří	k1gMnSc1	Jiří
Čížek	Čížek	k1gMnSc1	Čížek
–	–	k?	–
ViGo	ViGo	k1gMnSc1	ViGo
agency	agenca	k1gFnSc2	agenca
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
331	[number]	k4	331
s.	s.	k?	s.
Kapitola	kapitola	k1gFnSc1	kapitola
Talmberk	Talmberk	k1gInSc1	Talmberk
hrad	hrad	k1gInSc1	hrad
<g/>
,	,	kIx,	,
s.	s.	k?	s.
58	[number]	k4	58
<g/>
–	–	k?	–
<g/>
62	[number]	k4	62
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Talmberk	Talmberk	k1gInSc1	Talmberk
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Talmberk	Talmberk	k1gInSc1	Talmberk
na	na	k7c4	na
Hrady	hrad	k1gInPc4	hrad
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
