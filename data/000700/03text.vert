<s>
Železník	železník	k1gInSc1	železník
(	(	kIx(	(
<g/>
29	[number]	k4	29
<g/>
.	.	kIx.	.
duben	duben	k1gInSc1	duben
1978	[number]	k4	1978
-	-	kIx~	-
22	[number]	k4	22
<g/>
.	.	kIx.	.
prosinec	prosinec	k1gInSc1	prosinec
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
dostihový	dostihový	k2eAgMnSc1d1	dostihový
kůň	kůň	k1gMnSc1	kůň
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
jako	jako	k8xS	jako
jediný	jediný	k2eAgMnSc1d1	jediný
dokázal	dokázat	k5eAaPmAgMnS	dokázat
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
a	a	k8xC	a
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
vyhrát	vyhrát	k5eAaPmF	vyhrát
Velkou	velký	k2eAgFnSc4d1	velká
pardubickou	pardubický	k2eAgFnSc4d1	pardubická
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
Želatina	želatina	k1gFnSc1	želatina
porodila	porodit	k5eAaPmAgFnS	porodit
celkem	celkem	k6eAd1	celkem
šest	šest	k4xCc4	šest
hříbat	hříbě	k1gNnPc2	hříbě
(	(	kIx(	(
<g/>
Želka	želka	k1gFnSc1	želka
<g/>
,	,	kIx,	,
Žeton	žeton	k1gInSc1	žeton
<g/>
,	,	kIx,	,
Žitomír	Žitomír	k1gMnSc1	Žitomír
<g/>
,	,	kIx,	,
Žikava	Žikava	k1gFnSc1	Žikava
<g/>
,	,	kIx,	,
Žerotín	Žerotín	k1gInSc1	Žerotín
a	a	k8xC	a
Železník	železník	k1gInSc1	železník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Želatina	želatina	k1gFnSc1	želatina
se	se	k3xPyFc4	se
na	na	k7c6	na
dráze	dráha	k1gFnSc6	dráha
příliš	příliš	k6eAd1	příliš
neprosadila	prosadit	k5eNaPmAgFnS	prosadit
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
dobrému	dobrý	k2eAgInSc3d1	dobrý
původu	původ	k1gInSc3	původ
měla	mít	k5eAaImAgFnS	mít
velice	velice	k6eAd1	velice
slabou	slabý	k2eAgFnSc4d1	slabá
výkonnost	výkonnost	k1gFnSc4	výkonnost
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gMnSc7	její
otcem	otec	k1gMnSc7	otec
byl	být	k5eAaImAgMnS	být
Le	Le	k1gMnSc1	Le
Loup	Loup	k1gMnSc1	Loup
Garou	Gará	k1gFnSc4	Gará
(	(	kIx(	(
<g/>
FR	fr	k0	fr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Železníkův	železníkův	k2eAgMnSc1d1	železníkův
otec	otec	k1gMnSc1	otec
Zigeunersohn	Zigeunersohn	k1gMnSc1	Zigeunersohn
patřil	patřit	k5eAaImAgMnS	patřit
v	v	k7c6	v
NDR	NDR	kA	NDR
ke	k	k7c3	k
špičce	špička	k1gFnSc3	špička
na	na	k7c6	na
dráze	dráha	k1gFnSc6	dráha
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
i	i	k9	i
velmi	velmi	k6eAd1	velmi
úspěšným	úspěšný	k2eAgMnSc7d1	úspěšný
plemeníkem	plemeník	k1gMnSc7	plemeník
<g/>
.	.	kIx.	.
</s>
<s>
Startoval	startovat	k5eAaBmAgMnS	startovat
v	v	k7c6	v
23	[number]	k4	23
dostizích	dostih	k1gInPc6	dostih
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
12	[number]	k4	12
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
o	o	k7c6	o
něm	on	k3xPp3gInSc6	on
mluví	mluvit	k5eAaImIp3nS	mluvit
jako	jako	k9	jako
o	o	k7c6	o
českém	český	k2eAgMnSc6d1	český
Secretariatovi	Secretariat	k1gMnSc6	Secretariat
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Šamoríně	Šamorín	k1gInSc6	Šamorín
29	[number]	k4	29
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1978	[number]	k4	1978
a	a	k8xC	a
uhynul	uhynout	k5eAaPmAgInS	uhynout
22	[number]	k4	22
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2004	[number]	k4	2004
v	v	k7c6	v
Malé	Malé	k2eAgFnSc6d1	Malé
Morávce	Morávka	k1gFnSc6	Morávka
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
ruského	ruský	k2eAgInSc2d1	ruský
chovu	chov	k1gInSc2	chov
se	se	k3xPyFc4	se
jmenovala	jmenovat	k5eAaImAgFnS	jmenovat
Želatina	želatina	k1gFnSc1	želatina
<g/>
,	,	kIx,	,
otec	otec	k1gMnSc1	otec
Zigeunersohn	Zigeunersohn	k1gMnSc1	Zigeunersohn
byl	být	k5eAaImAgMnS	být
plemeníkem	plemeník	k1gMnSc7	plemeník
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1979	[number]	k4	1979
ho	on	k3xPp3gMnSc4	on
koupil	koupit	k5eAaPmAgInS	koupit
Agrochemický	agrochemický	k2eAgInSc1d1	agrochemický
podnik	podnik	k1gInSc1	podnik
ve	v	k7c6	v
Slavkově	Slavkov	k1gInSc6	Slavkov
u	u	k7c2	u
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
běhal	běhat	k5eAaImAgMnS	běhat
rovinné	rovinný	k2eAgInPc4d1	rovinný
dostihy	dostih	k1gInPc4	dostih
a	a	k8xC	a
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
poprvé	poprvé	k6eAd1	poprvé
zkusil	zkusit	k5eAaPmAgMnS	zkusit
proutěnky	proutěnka	k1gFnSc2	proutěnka
<g/>
.	.	kIx.	.
</s>
<s>
Následovalo	následovat	k5eAaImAgNnS	následovat
zranění	zranění	k1gNnSc1	zranění
zadní	zadní	k2eAgFnSc2d1	zadní
nohy	noha	k1gFnSc2	noha
na	na	k7c6	na
pastvině	pastvina	k1gFnSc6	pastvina
a	a	k8xC	a
kůň	kůň	k1gMnSc1	kůň
byl	být	k5eAaImAgMnS	být
prodán	prodat	k5eAaPmNgMnS	prodat
do	do	k7c2	do
Státního	státní	k2eAgInSc2d1	státní
statku	statek	k1gInSc2	statek
Světlá	světlat	k5eAaImIp3nS	světlat
Hora	hora	k1gFnSc1	hora
<g/>
.	.	kIx.	.
</s>
<s>
Železník	železník	k1gMnSc1	železník
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
uzdravil	uzdravit	k5eAaPmAgInS	uzdravit
a	a	k8xC	a
mohl	moct	k5eAaImAgInS	moct
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
dostizích	dostih	k1gInPc6	dostih
<g/>
,	,	kIx,	,
zpočátku	zpočátku	k6eAd1	zpočátku
s	s	k7c7	s
Antonínem	Antonín	k1gMnSc7	Antonín
Novákem	Novák	k1gMnSc7	Novák
v	v	k7c6	v
sedle	sedlo	k1gNnSc6	sedlo
<g/>
.	.	kIx.	.
</s>
<s>
Steeplechase	steeplechase	k1gFnSc4	steeplechase
začal	začít	k5eAaPmAgMnS	začít
běhat	běhat	k5eAaImF	běhat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
začal	začít	k5eAaPmAgInS	začít
vítězit	vítězit	k5eAaImF	vítězit
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
Velkou	velká	k1gFnSc7	velká
pardubickou	pardubický	k2eAgFnSc7d1	pardubická
běžel	běžet	k5eAaImAgMnS	běžet
6	[number]	k4	6
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1985	[number]	k4	1985
s	s	k7c7	s
Antonínem	Antonín	k1gMnSc7	Antonín
Novákem	Novák	k1gMnSc7	Novák
v	v	k7c6	v
sedle	sedlo	k1gNnSc6	sedlo
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
Železník	železník	k1gInSc1	železník
bez	bez	k7c2	bez
potíží	potíž	k1gFnPc2	potíž
kvalifikoval	kvalifikovat	k5eAaBmAgMnS	kvalifikovat
a	a	k8xC	a
vítězil	vítězit	k5eAaImAgMnS	vítězit
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
dostizích	dostih	k1gInPc6	dostih
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
,	,	kIx,	,
nepovedlo	povést	k5eNaPmAgNnS	povést
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
vyhrát	vyhrát	k5eAaPmF	vyhrát
Velkou	velká	k1gFnSc4	velká
pardubickou	pardubický	k2eAgFnSc7d1	pardubická
ani	ani	k8xC	ani
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
<g/>
.	.	kIx.	.
</s>
<s>
Zvítězit	zvítězit	k5eAaPmF	zvítězit
se	se	k3xPyFc4	se
mu	on	k3xPp3gNnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
s	s	k7c7	s
žokejem	žokej	k1gMnSc7	žokej
Josefem	Josef	k1gMnSc7	Josef
Váňou	Váňa	k1gMnSc7	Váňa
v	v	k7c6	v
sedle	sedlo	k1gNnSc6	sedlo
a	a	k8xC	a
novým	nový	k2eAgInSc7d1	nový
traťovým	traťový	k2eAgInSc7d1	traťový
rekordem	rekord	k1gInSc7	rekord
9	[number]	k4	9
<g/>
:	:	kIx,	:
<g/>
56,13	[number]	k4	56,13
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
žokejem	žokej	k1gMnSc7	žokej
Váňou	Váňa	k1gMnSc7	Váňa
zvítězili	zvítězit	k5eAaPmAgMnP	zvítězit
ještě	ještě	k9	ještě
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
a	a	k8xC	a
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnPc4	první
tři	tři	k4xCgNnPc4	tři
vítězství	vítězství	k1gNnPc4	vítězství
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
Železník	železník	k1gInSc1	železník
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
uvádí	uvádět	k5eAaImIp3nS	uvádět
výrok	výrok	k1gInSc4	výrok
dostihové	dostihový	k2eAgFnSc2d1	dostihová
komise	komise	k1gFnSc2	komise
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
lehce	lehko	k6eAd1	lehko
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
však	však	k9	však
nedá	dát	k5eNaPmIp3nS	dát
říci	říct	k5eAaPmF	říct
o	o	k7c4	o
jeho	jeho	k3xOp3gNnSc4	jeho
vítězství	vítězství	k1gNnSc4	vítězství
čtvrtém	čtvrtý	k4xOgInSc6	čtvrtý
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Železník	železník	k1gMnSc1	železník
upadl	upadnout	k5eAaPmAgMnS	upadnout
na	na	k7c6	na
Poplerově	Poplerův	k2eAgInSc6d1	Poplerův
skoku	skok	k1gInSc6	skok
<g/>
.	.	kIx.	.
</s>
<s>
Žokej	žokej	k1gMnSc1	žokej
Váňa	Váňa	k1gMnSc1	Váňa
na	na	k7c4	na
ryzáka	ryzák	k1gMnSc4	ryzák
znovu	znovu	k6eAd1	znovu
nasedl	nasednout	k5eAaPmAgMnS	nasednout
a	a	k8xC	a
po	po	k7c6	po
stíhací	stíhací	k2eAgFnSc6d1	stíhací
jízdě	jízda	k1gFnSc6	jízda
zvítězili	zvítězit	k5eAaPmAgMnP	zvítězit
před	před	k7c7	před
druhým	druhý	k4xOgInSc7	druhý
Drakem	drak	k1gInSc7	drak
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
po	po	k7c6	po
kolizi	kolize	k1gFnSc6	kolize
s	s	k7c7	s
jiným	jiný	k2eAgMnSc7d1	jiný
koněm	kůň	k1gMnSc7	kůň
Železník	železník	k1gMnSc1	železník
upadl	upadnout	k5eAaPmAgMnS	upadnout
a	a	k8xC	a
dostih	dostih	k1gInSc4	dostih
nedokončil	dokončit	k5eNaPmAgMnS	dokončit
<g/>
.	.	kIx.	.
</s>
<s>
Naposledy	naposledy	k6eAd1	naposledy
startovala	startovat	k5eAaBmAgFnS	startovat
tato	tento	k3xDgFnSc1	tento
dvojice	dvojice	k1gFnSc1	dvojice
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
ročník	ročník	k1gInSc1	ročník
Velké	velký	k2eAgFnSc2d1	velká
pardubické	pardubický	k2eAgFnSc2d1	pardubická
byl	být	k5eAaImAgMnS	být
však	však	k9	však
ovlivněn	ovlivnit	k5eAaPmNgMnS	ovlivnit
demonstranty	demonstrant	k1gMnPc7	demonstrant
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
způsobili	způsobit	k5eAaPmAgMnP	způsobit
zmatek	zmatek	k1gInSc4	zmatek
mezi	mezi	k7c7	mezi
koňmi	kůň	k1gMnPc7	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Železník	železník	k1gInSc1	železník
doplatil	doplatit	k5eAaPmAgInS	doplatit
na	na	k7c4	na
kolizi	kolize	k1gFnSc4	kolize
jiných	jiný	k2eAgInPc2d1	jiný
dvou	dva	k4xCgMnPc2	dva
koní	kůň	k1gMnPc2	kůň
a	a	k8xC	a
ztratil	ztratit	k5eAaPmAgMnS	ztratit
svého	svůj	k3xOyFgMnSc4	svůj
jezdce	jezdec	k1gMnSc4	jezdec
<g/>
.	.	kIx.	.
</s>
<s>
Pravidla	pravidlo	k1gNnPc1	pravidlo
už	už	k9	už
nedovolovala	dovolovat	k5eNaImAgNnP	dovolovat
znovu	znovu	k6eAd1	znovu
nasednout	nasednout	k5eAaPmF	nasednout
a	a	k8xC	a
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
dostihu	dostih	k1gInSc6	dostih
<g/>
.	.	kIx.	.
</s>
<s>
Železník	železník	k1gInSc1	železník
byl	být	k5eAaImAgInS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
koněm	kůň	k1gMnSc7	kůň
roku	rok	k1gInSc2	rok
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1987	[number]	k4	1987
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
a	a	k8xC	a
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Kůň	kůň	k1gMnSc1	kůň
si	se	k3xPyFc3	se
později	pozdě	k6eAd2	pozdě
na	na	k7c6	na
vyjížďce	vyjížďka	k1gFnSc6	vyjížďka
zlomil	zlomit	k5eAaPmAgMnS	zlomit
korunkovou	korunkový	k2eAgFnSc4d1	korunková
kost	kost	k1gFnSc4	kost
pravé	pravý	k2eAgFnSc2d1	pravá
zadní	zadní	k2eAgFnSc2d1	zadní
nohy	noha	k1gFnSc2	noha
a	a	k8xC	a
nemohl	moct	k5eNaImAgMnS	moct
závodit	závodit	k5eAaImF	závodit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Světlé	světlý	k2eAgFnSc6d1	světlá
Hoře	hora	k1gFnSc6	hora
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
turistickou	turistický	k2eAgFnSc7d1	turistická
atrakcí	atrakce	k1gFnSc7	atrakce
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
ve	v	k7c6	v
vysokém	vysoký	k2eAgInSc6d1	vysoký
věku	věk	k1gInSc6	věk
26	[number]	k4	26
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
kariéru	kariéra	k1gFnSc4	kariéra
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
58	[number]	k4	58
dostihů	dostih	k1gInPc2	dostih
<g/>
:	:	kIx,	:
30	[number]	k4	30
<g/>
×	×	k?	×
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
<g/>
,	,	kIx,	,
9	[number]	k4	9
<g/>
×	×	k?	×
byl	být	k5eAaImAgInS	být
druhý	druhý	k4xOgMnSc1	druhý
<g/>
,	,	kIx,	,
jednou	jednou	k6eAd1	jednou
třetí	třetí	k4xOgMnSc1	třetí
<g/>
,	,	kIx,	,
jednou	jednou	k6eAd1	jednou
čtvrtý	čtvrtý	k4xOgMnSc1	čtvrtý
<g/>
,	,	kIx,	,
dvakrát	dvakrát	k6eAd1	dvakrát
pátý	pátý	k4xOgMnSc1	pátý
<g/>
,	,	kIx,	,
výhry	výhra	k1gFnSc2	výhra
činily	činit	k5eAaImAgInP	činit
1	[number]	k4	1
723	[number]	k4	723
800	[number]	k4	800
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Horses-Online	Horses-Onlin	k1gInSc5	Horses-Onlin
i-equus	iquus	k1gInSc4	i-equus
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
