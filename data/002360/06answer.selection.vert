<s>
Čeština	čeština	k1gFnSc1	čeština
také	také	k9	také
používá	používat	k5eAaImIp3nS	používat
spřežku	spřežka	k1gFnSc4	spřežka
ch	ch	k0	ch
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
samostatné	samostatný	k2eAgNnSc4d1	samostatné
písmeno	písmeno	k1gNnSc4	písmeno
<g/>
,	,	kIx,	,
stojící	stojící	k2eAgFnSc4d1	stojící
v	v	k7c6	v
abecedě	abeceda	k1gFnSc6	abeceda
mezi	mezi	k7c7	mezi
h	h	k?	h
a	a	k8xC	a
i	i	k?	i
<g/>
.	.	kIx.	.
</s>
