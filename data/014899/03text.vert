<s>
Nijmegenský	Nijmegenský	k2eAgInSc1d1
mír	mír	k1gInSc1
</s>
<s>
Malba	malba	k1gFnSc1
"	"	kIx"
<g/>
Nijmengenský	Nijmengenský	k2eAgInSc1d1
mír	mír	k1gInSc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
Henri	Henri	k1gNnSc1
Gascard	Gascarda	k1gFnPc2
(	(	kIx(
<g/>
1635	#num#	k4
<g/>
-	-	kIx~
<g/>
1701	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Nijmegenský	Nijmegenský	k2eAgInSc1d1
mír	mír	k1gInSc1
je	být	k5eAaImIp3nS
soubor	soubor	k1gInSc4
několika	několik	k4yIc2
smluv	smlouva	k1gFnPc2
uzavřených	uzavřený	k2eAgFnPc2d1
v	v	k7c6
nizozemském	nizozemský	k2eAgNnSc6d1
městě	město	k1gNnSc6
Nijmegen	Nijmegen	k1gInSc4
mezi	mezi	k7c7
srpnem	srpen	k1gInSc7
1678	#num#	k4
a	a	k8xC
prosincem	prosinec	k1gInSc7
1679	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
smlouvy	smlouva	k1gFnSc2
ukončily	ukončit	k5eAaPmAgInP
různé	různý	k2eAgInPc1d1
<g/>
,	,	kIx,
vzájemně	vzájemně	k6eAd1
propojené	propojený	k2eAgInPc1d1
konflikty	konflikt	k1gInPc1
mezi	mezi	k7c7
Francií	Francie	k1gFnSc7
<g/>
,	,	kIx,
Nizozemskem	Nizozemsko	k1gNnSc7
<g/>
,	,	kIx,
Španělskem	Španělsko	k1gNnSc7
<g/>
,	,	kIx,
Braniborskem-Pruskem	Braniborskem-Prusek	k1gInSc7
<g/>
,	,	kIx,
Švédskem	Švédsko	k1gNnSc7
<g/>
,	,	kIx,
Dánskem	Dánsko	k1gNnSc7
<g/>
,	,	kIx,
münsterským	münsterský	k2eAgNnSc7d1
biskupstvím	biskupství	k1gNnSc7
a	a	k8xC
Svatou	Svata	k1gFnSc7
říší	říš	k1gFnSc7
římskou	římska	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvýznamnější	významný	k2eAgFnSc1d3
z	z	k7c2
nich	on	k3xPp3gMnPc2
byla	být	k5eAaImAgFnS
smlouva	smlouva	k1gFnSc1
o	o	k7c4
uzavření	uzavření	k1gNnSc4
míru	mír	k1gInSc2
mezi	mezi	k7c7
Francií	Francie	k1gFnSc7
a	a	k8xC
Nizozemskem	Nizozemsko	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Předcházející	předcházející	k2eAgFnPc1d1
události	událost	k1gFnPc1
</s>
<s>
Francouzsko-nizozemská	francouzsko-nizozemský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
v	v	k7c6
letech	let	k1gInPc6
1672	#num#	k4
<g/>
–	–	k?
<g/>
1678	#num#	k4
rozpoutala	rozpoutat	k5eAaPmAgFnS
další	další	k2eAgInPc4d1
konflikty	konflikt	k1gInPc4
ukončené	ukončený	k2eAgInPc4d1
formálně	formálně	k6eAd1
až	až	k6eAd1
v	v	k7c6
Nijmegenu	Nijmegen	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Separátní	separátní	k2eAgFnPc1d1
mírové	mírový	k2eAgFnPc1d1
smlouvy	smlouva	k1gFnPc1
byly	být	k5eAaImAgFnP
připraveny	připravit	k5eAaPmNgFnP
pro	pro	k7c4
vyřešení	vyřešení	k1gNnSc4
konfliktů	konflikt	k1gInPc2
jako	jako	k9
byla	být	k5eAaImAgFnS
třetí	třetí	k4xOgFnSc1
anglo-nizozemská	anglo-nizozemský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
nebo	nebo	k8xC
skå	skå	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
byly	být	k5eAaImAgFnP
součástí	součást	k1gFnSc7
francouzsko-nizozemské	francouzsko-nizozemský	k2eAgFnPc1d1
války	válka	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Anglie	Anglie	k1gFnSc1
se	se	k3xPyFc4
z	z	k7c2
počátku	počátek	k1gInSc2
účastnila	účastnit	k5eAaImAgFnS
války	válka	k1gFnPc4
na	na	k7c6
straně	strana	k1gFnSc6
Francouzů	Francouz	k1gMnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1674	#num#	k4
od	od	k7c2
války	válka	k1gFnSc2
odstoupila	odstoupit	k5eAaPmAgFnS
uzavřením	uzavření	k1gNnSc7
mírové	mírový	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
ve	v	k7c6
Westminsteru	Westminster	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mírová	mírový	k2eAgFnSc1d1
jednání	jednání	k1gNnPc4
započala	započnout	k5eAaPmAgFnS
již	již	k6eAd1
roku	rok	k1gInSc2
1676	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
žádná	žádný	k3yNgFnSc1
ze	z	k7c2
stran	strana	k1gFnPc2
se	se	k3xPyFc4
neshodla	shodnout	k5eNaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
shodě	shoda	k1gFnSc3
došlo	dojít	k5eAaPmAgNnS
v	v	k7c6
létě	léto	k1gNnSc6
1678	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Smlouvy	smlouva	k1gFnPc1
uzavřené	uzavřený	k2eAgFnPc1d1
v	v	k7c4
Nijmegen	Nijmegen	k1gInSc4
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1678	#num#	k4
–	–	k?
Francie	Francie	k1gFnSc1
a	a	k8xC
Nizozemská	nizozemský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
podepsaly	podepsat	k5eAaPmAgFnP
mír	mír	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Švédsko	Švédsko	k1gNnSc1
nebylo	být	k5eNaImAgNnS
účastníkem	účastník	k1gMnSc7
mírového	mírový	k2eAgNnSc2d1
ujednání	ujednání	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
jeden	jeden	k4xCgInSc1
paragraf	paragraf	k1gInSc1
smlouvy	smlouva	k1gFnSc2
nutil	nutit	k5eAaImAgInS
Nizozemsko	Nizozemsko	k1gNnSc4
k	k	k7c3
neutrálnímu	neutrální	k2eAgInSc3d1
přístupu	přístup	k1gInSc3
ve	v	k7c6
vztahu	vztah	k1gInSc6
ke	k	k7c3
Švédsku	Švédsko	k1gNnSc3
<g/>
,	,	kIx,
s	s	k7c7
nímž	jenž	k3xRgNnSc7
bylo	být	k5eAaImAgNnS
ve	v	k7c6
válce	válka	k1gFnSc6
od	od	k7c2
roku	rok	k1gInSc2
1675	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1678	#num#	k4
–	–	k?
Francie	Francie	k1gFnSc2
a	a	k8xC
Španělsko	Španělsko	k1gNnSc4
podepsaly	podepsat	k5eAaPmAgFnP
mírovou	mírový	k2eAgFnSc4d1
smlouvu	smlouva	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1679	#num#	k4
–	–	k?
France	Franc	k1gMnSc4
uzavřela	uzavřít	k5eAaPmAgFnS
mír	mír	k1gInSc4
s	s	k7c7
Říší	říš	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1679	#num#	k4
–	–	k?
Švédsko	Švédsko	k1gNnSc1
uzavřelo	uzavřít	k5eAaPmAgNnS
mír	mír	k1gInSc4
s	s	k7c7
Říší	říš	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1679	#num#	k4
–	–	k?
Švédsko	Švédsko	k1gNnSc1
podepsalo	podepsat	k5eAaPmAgNnS
mírovou	mírový	k2eAgFnSc4d1
smlouvu	smlouva	k1gFnSc4
s	s	k7c7
münsterským	münsterský	k2eAgNnSc7d1
biskupstvím	biskupství	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Smlouva	smlouva	k1gFnSc1
také	také	k9
nechala	nechat	k5eAaPmAgFnS
rozpustit	rozpustit	k5eAaPmF
všechny	všechen	k3xTgMnPc4
münsterské	münsterský	k2eAgMnPc4d1
vojáky	voják	k1gMnPc4
v	v	k7c6
dánských	dánský	k2eAgFnPc6d1
službách	služba	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1679	#num#	k4
–	–	k?
Švédsko	Švédsko	k1gNnSc1
podepsalo	podepsat	k5eAaPmAgNnS
mírové	mírový	k2eAgNnSc1d1
ujednání	ujednání	k1gNnSc1
s	s	k7c7
Nizozemskem	Nizozemsko	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Mírové	mírový	k2eAgFnPc1d1
podmínky	podmínka	k1gFnPc1
</s>
<s>
Francouzsko-nizozemská	francouzsko-nizozemský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
skončila	skončit	k5eAaPmAgFnS
ujednáním	ujednání	k1gNnSc7
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
Francii	Francie	k1gFnSc4
dávalo	dávat	k5eAaImAgNnS
v	v	k7c4
držbu	držba	k1gFnSc4
Svobodné	svobodný	k2eAgNnSc1d1
hrabství	hrabství	k1gNnSc1
burgundské	burgundský	k2eAgFnSc2d1
(	(	kIx(
<g/>
Franche-Comté	Franche-Comtý	k2eAgFnSc2d1
<g/>
)	)	kIx)
a	a	k8xC
Lotrinsko	Lotrinsko	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mimo	mimo	k7c4
akvizice	akvizice	k1gFnPc4
uskutečněné	uskutečněný	k2eAgFnPc4d1
Ludvíkem	Ludvík	k1gMnSc7
XIV	XIV	kA
<g/>
.	.	kIx.
v	v	k7c6
důsledku	důsledek	k1gInSc6
pyrenejského	pyrenejský	k2eAgInSc2d1
míru	mír	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1659	#num#	k4
a	a	k8xC
cášského	cášský	k2eAgInSc2d1
míru	mír	k1gInSc2
z	z	k7c2
roku	rok	k1gInSc2
1668	#num#	k4
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc1
získala	získat	k5eAaPmAgFnS
nejen	nejen	k6eAd1
Svobodné	svobodný	k2eAgNnSc4d1
hrabství	hrabství	k1gNnSc4
burgundské	burgundský	k2eAgFnSc2d1
(	(	kIx(
<g/>
Franche-Comté	Franche-Comtý	k2eAgFnSc2d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
další	další	k2eAgNnPc1d1
teritoria	teritorium	k1gNnPc1
Španělského	španělský	k2eAgNnSc2d1
Nizozemí	Nizozemí	k1gNnSc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
města	město	k1gNnSc2
Saint-Omer	Saint-Omer	k1gInSc1
se	s	k7c7
zbývající	zbývající	k2eAgFnSc7d1
severozápadní	severozápadní	k2eAgFnSc7d1
částí	část	k1gFnSc7
bývalého	bývalý	k2eAgNnSc2d1
říššského	říššský	k2eAgNnSc2d1
hrabství	hrabství	k1gNnSc2
Artois	Artois	k1gFnSc2
<g/>
,	,	kIx,
země	zem	k1gFnSc2
Cassel	Cassela	k1gFnPc2
a	a	k8xC
Aire	Aire	k1gFnPc2
v	v	k7c6
jihozápadních	jihozápadní	k2eAgInPc6d1
Flandrech	Flandry	k1gInPc6
<g/>
,	,	kIx,
Cambraiské	Cambraiský	k2eAgNnSc4d1
biskupství	biskupství	k1gNnSc4
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
města	město	k1gNnPc4
Valenciennes	Valenciennesa	k1gFnPc2
a	a	k8xC
Maubeuge	Maubeuge	k1gFnPc2
v	v	k7c6
jižním	jižní	k2eAgNnSc6d1
Henegavsku	Henegavsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c4
druhou	druhý	k4xOgFnSc4
stranu	strana	k1gFnSc4
<g/>
,	,	kIx,
francouzský	francouzský	k2eAgMnSc1d1
král	král	k1gMnSc1
postoupil	postoupit	k5eAaPmAgMnS
Maastricht	Maastricht	k1gInSc4
<g/>
,	,	kIx,
dobytý	dobytý	k2eAgMnSc1d1
a	a	k8xC
obsazený	obsazený	k2eAgMnSc1d1
Francouzi	Francouz	k1gMnPc1
<g/>
,	,	kIx,
a	a	k8xC
Oranžské	oranžský	k2eAgNnSc1d1
knížectví	knížectví	k1gNnSc1
nizozemskému	nizozemský	k2eAgMnSc3d1
místodržiteli	místodržitel	k1gMnSc3
Vilému	Viléma	k1gFnSc4
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oranžskému	oranžský	k2eAgMnSc3d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Francouzská	francouzský	k2eAgFnSc1d1
vojska	vojsko	k1gNnPc1
opustila	opustit	k5eAaPmAgFnS
několik	několik	k4yIc4
okupovaných	okupovaný	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
v	v	k7c6
severních	severní	k2eAgInPc6d1
Flandrech	Flandry	k1gInPc6
a	a	k8xC
Henegavsku	Henegavsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Císař	Císař	k1gMnSc1
Leopold	Leopold	k1gMnSc1
I.	I.	kA
musel	muset	k5eAaImAgMnS
přijmout	přijmout	k5eAaPmF
francouzskou	francouzský	k2eAgFnSc4d1
okupaci	okupace	k1gFnSc4
měst	město	k1gNnPc2
Freiburgu	Freiburg	k1gInSc2
(	(	kIx(
<g/>
do	do	k7c2
roku	rok	k1gInSc2
1697	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Kehlu	Kehla	k1gFnSc4
(	(	kIx(
<g/>
do	do	k7c2
roku	rok	k1gInSc2
1698	#num#	k4
<g/>
)	)	kIx)
na	na	k7c6
pravém	pravý	k2eAgInSc6d1
břehu	břeh	k1gInSc6
Rýna	Rýn	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgFnPc1d1
události	událost	k1gFnPc1
</s>
<s>
Některé	některý	k3yIgFnPc1
země	zem	k1gFnPc1
se	se	k3xPyFc4
zúčastnily	zúčastnit	k5eAaPmAgFnP
podepisování	podepisování	k1gNnSc4
mírových	mírový	k2eAgFnPc2d1
smluv	smlouva	k1gFnPc2
jinde	jinde	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Švédsko	Švédsko	k1gNnSc1
uzavřelo	uzavřít	k5eAaPmAgNnS
Cellský	Cellský	k2eAgInSc4d1
mír	mír	k1gInSc4
s	s	k7c7
Brunšvickem-Lüneburskem	Brunšvickem-Lünebursek	k1gInSc7
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc1
a	a	k8xC
Švédsko	Švédsko	k1gNnSc1
uzavřely	uzavřít	k5eAaPmAgInP
Saint-Germainská	Saint-Germainský	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
s	s	k7c7
Braniborskem	Braniborsko	k1gNnSc7
a	a	k8xC
Francie	Francie	k1gFnSc1
diktovala	diktovat	k5eAaImAgFnS
podmínky	podmínka	k1gFnPc4
v	v	k7c6
mírové	mírový	k2eAgFnSc6d1
smlouvě	smlouva	k1gFnSc6
z	z	k7c2
Fontainebleau	Fontainebleaus	k1gInSc2
mezi	mezi	k7c7
Švédskem	Švédsko	k1gNnSc7
a	a	k8xC
Dánskem	Dánsko	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Marc-Antoine	Marc-Antoinout	k5eAaPmIp3nS
Charpentier	Charpentier	k1gInSc4
zkomponoval	zkomponovat	k5eAaPmAgInS
k	k	k7c3
příležitosti	příležitost	k1gFnSc3
uzavření	uzavření	k1gNnSc2
míru	mír	k1gInSc2
Te	Te	k1gMnSc1
Deum	Deum	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předehra	předehra	k1gFnSc1
tohoto	tento	k3xDgMnSc2
Te	Te	k1gMnSc2
Deum	Deum	k1gMnSc1
je	být	k5eAaImIp3nS
také	také	k9
známá	známý	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
znělka	znělka	k1gFnSc1
Euvize	Euvize	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Treaty	Treata	k1gFnSc2
of	of	k?
Nijmegen	Nijmegen	k1gInSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
NOLAN	NOLAN	kA
<g/>
,	,	kIx,
Cathal	Cathal	k1gMnSc1
J.	J.	kA
Wars	Wars	k1gInSc1
of	of	k?
the	the	k?
age	age	k?
of	of	k?
Louis	Louis	k1gMnSc1
XIV	XIV	kA
<g/>
,	,	kIx,
1650	#num#	k4
<g/>
-	-	kIx~
<g/>
1715	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
ABC-CLIO	ABC-CLIO	k1gMnSc1
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
313330468	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
128	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
HORNE	Horn	k1gMnSc5
<g/>
,	,	kIx,
Alistair	Alistaira	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
La	la	k1gNnSc1
Belle	bell	k1gInSc5
France	Franc	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
USA	USA	kA
<g/>
:	:	kIx,
Vintage	Vintag	k1gMnSc2
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9781400034871	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
164	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Novověk	novověk	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
90024431	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
184076910	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
90024431	#num#	k4
</s>
