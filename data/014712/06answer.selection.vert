<s>
Promethium	Promethium	k1gNnSc1
(	(	kIx(
<g/>
chemická	chemický	k2eAgFnSc1d1
značka	značka	k1gFnSc1
Pm	Pm	kA
<g/>
,	,	kIx,
latinsky	latinsky	k6eAd1
Promethium	Promethium	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
jediným	jediný	k2eAgInSc7d1
lanthanoidem	lanthanoid	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
nemá	mít	k5eNaImIp3nS
stabilní	stabilní	k2eAgInSc4d1
izotop	izotop	k1gInSc4
a	a	k8xC
v	v	k7c6
přírodě	příroda	k1gFnSc6
se	se	k3xPyFc4
prakticky	prakticky	k6eAd1
nevyskytuje	vyskytovat	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>