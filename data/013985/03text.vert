<s>
Letectvo	letectvo	k1gNnSc1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
aktualizaci	aktualizace	k1gFnSc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
obsahuje	obsahovat	k5eAaImIp3nS
zastaralé	zastaralý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
odrážel	odrážet	k5eAaImAgMnS
aktuální	aktuální	k2eAgInSc4d1
stav	stav	k1gInSc4
a	a	k8xC
nedávné	dávný	k2eNgFnPc4d1
události	událost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historické	historický	k2eAgFnPc4d1
informace	informace	k1gFnPc4
nemažte	mazat	k5eNaImRp2nP
<g/>
,	,	kIx,
raději	rád	k6eAd2
je	on	k3xPp3gInPc4
převeďte	převést	k5eAaPmRp2nP
do	do	k7c2
minulého	minulý	k2eAgInSc2d1
času	čas	k1gInSc2
a	a	k8xC
případně	případně	k6eAd1
přesuňte	přesunout	k5eAaPmRp2nP
do	do	k7c2
části	část	k1gFnSc2
článku	článek	k1gInSc2
věnované	věnovaný	k2eAgFnPc4d1
dějinám	dějiny	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s>
Letectvo	letectvo	k1gNnSc1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
Země	země	k1gFnSc1
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Vznik	vznik	k1gInSc4
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1947	#num#	k4
Typ	typa	k1gFnPc2
</s>
<s>
letectvo	letectvo	k1gNnSc1
Velikost	velikost	k1gFnSc1
</s>
<s>
328	#num#	k4
600	#num#	k4
aktivního	aktivní	k2eAgInSc2d1
personálu	personál	k1gInSc2
Motto	motto	k1gNnSc1
</s>
<s>
"	"	kIx"
<g/>
Aim	Aim	k1gMnSc1
High	High	k1gMnSc1
...	...	k?
Fly-Fight-Win	Fly-Fight-Win	k1gMnSc1
<g/>
"	"	kIx"
Velitelé	velitel	k1gMnPc1
</s>
<s>
generál	generál	k1gMnSc1
David	David	k1gMnSc1
L.	L.	kA
Goldfein	Goldfein	k1gMnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
náčelník	náčelník	k1gInSc1
štábu	štáb	k1gInSc2
USAF	USAF	kA
<g/>
)	)	kIx)
Nadřazené	nadřazený	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
</s>
<s>
Ministerstvo	ministerstvo	k1gNnSc1
obrany	obrana	k1gFnSc2
USA	USA	kA
Účast	účast	k1gFnSc1
Války	válka	k1gFnSc2
</s>
<s>
Korejská	korejský	k2eAgFnSc1d1
válkaVálka	válkaVálka	k1gFnSc1
ve	v	k7c6
VietnamuInvaze	VietnamuInvaz	k1gInSc6
na	na	k7c4
GrenaduVálka	GrenaduVálek	k1gMnSc4
v	v	k7c4
ZálivuOperace	ZálivuOperace	k1gFnPc4
Rozhodná	rozhodný	k2eAgFnSc1d1
sílaOperace	sílaOperace	k1gFnSc1
Spojenecká	spojenecký	k2eAgFnSc1d1
sílaOperace	sílaOperace	k1gFnSc2
Trvalá	trvalý	k2eAgFnSc1d1
svobodaVálka	svobodaVálka	k1gFnSc1
v	v	k7c4
IrákuVálka	IrákuVálek	k1gMnSc4
v	v	k7c6
severozápadním	severozápadní	k2eAgInSc6d1
PákistánuVálka	PákistánuVálek	k1gMnSc4
proti	proti	k7c3
Islámskému	islámský	k2eAgInSc3d1
státu	stát	k1gInSc3
Insignie	insignie	k1gFnPc4
Znak	znak	k1gInSc4
</s>
<s>
Letectvo	letectvo	k1gNnSc1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
United	United	k1gMnSc1
States	States	k1gMnSc1
Air	Air	k1gMnSc1
Force	force	k1gFnSc1
<g/>
;	;	kIx,
USAF	USAF	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
Ozbrojených	ozbrojený	k2eAgFnPc2d1
sil	síla	k1gFnPc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
a	a	k8xC
jednou	jeden	k4xCgFnSc7
z	z	k7c2
osmi	osm	k4xCc2
amerických	americký	k2eAgFnPc2d1
uniformovaných	uniformovaný	k2eAgFnPc2d1
složek	složka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktivně	aktivně	k6eAd1
v	v	k7c6
něm	on	k3xPp3gInSc6
slouží	sloužit	k5eAaImIp3nP
352	#num#	k4
000	#num#	k4
mužů	muž	k1gMnPc2
i	i	k8xC
žen	žena	k1gFnPc2
a	a	k8xC
má	mít	k5eAaImIp3nS
k	k	k7c3
dispozici	dispozice	k1gFnSc3
přes	přes	k7c4
9000	#num#	k4
letadel	letadlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
USAF	USAF	kA
i	i	k8xC
jeho	jeho	k3xOp3gMnPc1
předchůdci	předchůdce	k1gMnPc1
se	se	k3xPyFc4
zúčastnili	zúčastnit	k5eAaPmAgMnP
mnoha	mnoho	k4c2
válečných	válečný	k2eAgInPc2d1
konfliktů	konflikt	k1gInPc2
po	po	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Vytvořeno	vytvořen	k2eAgNnSc1d1
bylo	být	k5eAaImAgNnS
výnosem	výnos	k1gInSc7
Shromáždění	shromáždění	k1gNnSc2
národní	národní	k2eAgFnSc2d1
bezpečnosti	bezpečnost	k1gFnSc2
z	z	k7c2
roku	rok	k1gInSc2
1947	#num#	k4
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
za	za	k7c4
den	den	k1gInSc4
jeho	jeho	k3xOp3gInSc2,k3xPp3gInSc2
oficiálního	oficiální	k2eAgInSc2d1
vzniku	vznik	k1gInSc2
se	se	k3xPyFc4
považuje	považovat	k5eAaImIp3nS
18	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
1947	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velitelem	velitel	k1gMnSc7
USAF	USAF	kA
je	být	k5eAaImIp3nS
náčelník	náčelník	k1gMnSc1
štábu	štáb	k1gInSc2
letectva	letectvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Náčelník	náčelník	k1gInSc1
štábu	štáb	k1gInSc2
letectva	letectvo	k1gNnSc2
je	být	k5eAaImIp3nS
čtyřhvězdičkový	čtyřhvězdičkový	k2eAgMnSc1d1
generál	generál	k1gMnSc1
jmenovaný	jmenovaný	k1gMnSc1
prezidentem	prezident	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
je	být	k5eAaImIp3nS
zároveň	zároveň	k6eAd1
jedním	jeden	k4xCgMnSc7
z	z	k7c2
pěti	pět	k4xCc2
náčelníků	náčelník	k1gMnPc2
Spojeného	spojený	k2eAgInSc2d1
generálního	generální	k2eAgInSc2d1
štábu	štáb	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc1
zároveň	zároveň	k6eAd1
slouží	sloužit	k5eAaImIp3nS
jako	jako	k8xS,k8xC
poradce	poradce	k1gMnSc2
prezidenta	prezident	k1gMnSc2
<g/>
,	,	kIx,
Národního	národní	k2eAgNnSc2d1
bezpečnostního	bezpečnostní	k2eAgNnSc2d1
shromáždění	shromáždění	k1gNnSc2
(	(	kIx(
<g/>
NSC	NSC	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ministra	ministr	k1gMnSc4
obrany	obrana	k1gFnSc2
a	a	k8xC
ministra	ministr	k1gMnSc2
letectví	letectví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
svého	svůj	k3xOyFgInSc2
vzniku	vznik	k1gInSc2
byla	být	k5eAaImAgFnS
organizační	organizační	k2eAgFnSc1d1
struktura	struktura	k1gFnSc1
USAF	USAF	kA
neustále	neustále	k6eAd1
revidována	revidován	k2eAgFnSc1d1
<g/>
,	,	kIx,
v	v	k7c6
reakci	reakce	k1gFnSc6
na	na	k7c4
světové	světový	k2eAgFnPc4d1
události	událost	k1gFnPc4
a	a	k8xC
politický	politický	k2eAgInSc4d1
vývoj	vývoj	k1gInSc4
ve	v	k7c6
světě	svět	k1gInSc6
i	i	k8xC
v	v	k7c6
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naposledy	naposledy	k6eAd1
zaznamenala	zaznamenat	k5eAaPmAgFnS
struktura	struktura	k1gFnSc1
USAF	USAF	kA
organizační	organizační	k2eAgFnSc4d1
změnu	změna	k1gFnSc4
v	v	k7c6
srpnu	srpen	k1gInSc6
2009	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
bylo	být	k5eAaImAgNnS
ustanoveno	ustanovit	k5eAaPmNgNnS
10	#num#	k4
<g/>
.	.	kIx.
hlavní	hlavní	k2eAgNnSc1d1
velitelství	velitelství	k1gNnSc1
vzdušných	vzdušný	k2eAgFnPc2d1
sil	síla	k1gFnPc2
(	(	kIx(
<g/>
MAJCOM	MAJCOM	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každé	každý	k3xTgNnSc1
hlavní	hlavní	k2eAgNnSc1d1
velitelství	velitelství	k1gNnSc1
se	se	k3xPyFc4
zodpovídá	zodpovídat	k5eAaPmIp3nS,k5eAaImIp3nS
ministru	ministr	k1gMnSc3
obrany	obrana	k1gFnSc2
prostřednictvím	prostřednictvím	k7c2
náčelníka	náčelník	k1gMnSc2
generálního	generální	k2eAgInSc2d1
štábu	štáb	k1gInSc2
letectva	letectvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některá	některý	k3yIgNnPc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
podporují	podporovat	k5eAaImIp3nP
i	i	k9
Spojené	spojený	k2eAgNnSc1d1
bojové	bojový	k2eAgNnSc1d1
velitelství	velitelství	k1gNnSc1
nebo	nebo	k8xC
podřízené	podřízený	k2eAgNnSc1d1
velitelství	velitelství	k1gNnSc1
tím	ten	k3xDgInSc7
způsobem	způsob	k1gInSc7
<g/>
,	,	kIx,
že	že	k8xS
jim	on	k3xPp3gMnPc3
poskytují	poskytovat	k5eAaImIp3nP
detašované	detašovaný	k2eAgFnPc1d1
letecké	letecký	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
(	(	kIx(
<g/>
C-NAF	C-NAF	k1gFnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgInSc4d1
velitelství	velitelství	k1gNnSc1
se	se	k3xPyFc4
dále	daleko	k6eAd2
dělí	dělit	k5eAaImIp3nS
na	na	k7c4
kombinované	kombinovaný	k2eAgFnPc4d1
vzdušné	vzdušný	k2eAgFnPc4d1
síly	síla	k1gFnPc4
(	(	kIx(
<g/>
NAF	NAF	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
křídla	křídlo	k1gNnSc2
<g/>
,	,	kIx,
skupiny	skupina	k1gFnSc2
<g/>
,	,	kIx,
perutě	peruť	k1gFnSc2
a	a	k8xC
letky	letka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Současná	současný	k2eAgFnSc1d1
situace	situace	k1gFnSc1
</s>
<s>
F-22	F-22	k4
Raptor	Raptor	k1gInSc1
</s>
<s>
USAF	USAF	kA
je	být	k5eAaImIp3nS
jednou	jeden	k4xCgFnSc7
z	z	k7c2
největších	veliký	k2eAgFnPc2d3
vzdušných	vzdušný	k2eAgFnPc2d1
sil	síla	k1gFnPc2
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
tak	tak	k6eAd1
ale	ale	k8xC
bojuje	bojovat	k5eAaImIp3nS
se	se	k3xPyFc4
svým	svůj	k3xOyFgInSc7
poměrně	poměrně	k6eAd1
napjatým	napjatý	k2eAgInSc7d1
rozpočtem	rozpočet	k1gInSc7
při	při	k7c6
naplňování	naplňování	k1gNnSc6
operačních	operační	k2eAgFnPc2d1
snah	snaha	k1gFnPc2
i	i	k8xC
snah	snaha	k1gFnPc2
modernizačních	modernizační	k2eAgFnPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současnosti	současnost	k1gFnSc6
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
USAF	USAF	kA
zaměstnává	zaměstnávat	k5eAaImIp3nS
686	#num#	k4
944	#num#	k4
osob	osoba	k1gFnPc2
<g/>
,	,	kIx,
při	při	k7c6
čemž	což	k3yRnSc6,k3yQnSc6
331	#num#	k4
700	#num#	k4
patří	patřit	k5eAaImIp3nS
k	k	k7c3
aktivním	aktivní	k2eAgFnPc3d1
vzdušným	vzdušný	k2eAgFnPc3d1
silám	síla	k1gFnPc3
<g/>
,	,	kIx,
69	#num#	k4
500	#num#	k4
je	být	k5eAaImIp3nS
přiděleno	přidělit	k5eAaPmNgNnS
k	k	k7c3
Velitelství	velitelství	k1gNnSc3
záložních	záložní	k2eAgFnPc2d1
leteckých	letecký	k2eAgFnPc2d1
sil	síla	k1gFnPc2
(	(	kIx(
<g/>
Air	Air	k1gFnSc1
Force	force	k1gFnSc2
Reserve	Reserev	k1gFnSc2
Command	Commanda	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
106	#num#	k4
700	#num#	k4
osob	osoba	k1gFnPc2
slouží	sloužit	k5eAaImIp3nS
v	v	k7c6
Národní	národní	k2eAgFnSc6d1
letecké	letecký	k2eAgFnSc6d1
gardě	garda	k1gFnSc6
a	a	k8xC
zbytek	zbytek	k1gInSc1
tvoří	tvořit	k5eAaImIp3nS
179	#num#	k4
044	#num#	k4
civilních	civilní	k2eAgMnPc2d1
pracovníků	pracovník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letos	letos	k6eAd1
dokončí	dokončit	k5eAaPmIp3nP
svůj	svůj	k3xOyFgInSc4
desátý	desátý	k4xOgInSc4
rok	rok	k1gInSc4
v	v	k7c6
podpoře	podpora	k1gFnSc6
operace	operace	k1gFnSc1
Trvalá	trvalá	k1gFnSc1
svoboda	svoboda	k1gFnSc1
(	(	kIx(
<g/>
Enduring	Enduring	k1gInSc1
Freedom	Freedom	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
osmý	osmý	k4xOgInSc4
rok	rok	k1gInSc4
v	v	k7c6
operaci	operace	k1gFnSc6
Irácká	irácký	k2eAgFnSc1d1
svoboda	svoboda	k1gFnSc1
(	(	kIx(
<g/>
Iraqi	Iraq	k1gFnSc2
Freedom	Freedom	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
reakci	reakce	k1gFnSc6
na	na	k7c4
potřebu	potřeba	k1gFnSc4
doplňkových	doplňkový	k2eAgFnPc2d1
informací	informace	k1gFnPc2
v	v	k7c6
boji	boj	k1gInSc6
proti	proti	k7c3
povstalcům	povstalec	k1gMnPc3
v	v	k7c6
Iráku	Irák	k1gInSc6
a	a	k8xC
Afghánistánu	Afghánistán	k1gInSc6
rozšířilo	rozšířit	k5eAaPmAgNnS
USAF	USAF	kA
nasazení	nasazení	k1gNnSc1
bezpilotních	bezpilotní	k2eAgInPc2d1
letounů	letoun	k1gInPc2
(	(	kIx(
<g/>
RPA	RPA	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
zároveň	zároveň	k6eAd1
vznikla	vzniknout	k5eAaPmAgFnS
potřeba	potřeba	k1gFnSc1
nového	nový	k2eAgInSc2d1
personálu	personál	k1gInSc2
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gInSc3
výcviku	výcvik	k1gInSc3
k	k	k7c3
ovládání	ovládání	k1gNnSc3
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
údržbě	údržba	k1gFnSc3
těchto	tento	k3xDgInPc2
strojů	stroj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
přesto	přesto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
stíhačky	stíhačka	k1gFnPc1
F-22	F-22	k1gFnPc2
zatím	zatím	k6eAd1
nebyly	být	k5eNaImAgFnP
bojově	bojově	k6eAd1
nasazeny	nasadit	k5eAaPmNgFnP
v	v	k7c6
operacích	operace	k1gFnPc6
v	v	k7c6
Iráku	Irák	k1gInSc6
<g/>
,	,	kIx,
Afghánistánu	Afghánistán	k1gInSc6
ani	ani	k8xC
Libyi	Libye	k1gFnSc6
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
v	v	k7c6
současnosti	současnost	k1gFnSc6
pravidelně	pravidelně	k6eAd1
nasazovány	nasazován	k2eAgFnPc4d1
v	v	k7c6
Pacifiku	Pacifik	k1gInSc6
při	při	k7c6
podpoře	podpora	k1gFnSc6
leteckého	letecký	k2eAgInSc2d1
expedičního	expediční	k2eAgInSc2d1
sboru	sbor	k1gInSc2
a	a	k8xC
jeho	jeho	k3xOp3gFnPc2
potřeb	potřeba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
USAF	USAF	kA
zavedlo	zavést	k5eAaPmAgNnS
do	do	k7c2
výzbroje	výzbroj	k1gFnSc2
svůj	svůj	k3xOyFgMnSc1
160	#num#	k4
<g/>
.	.	kIx.
stroj	stroj	k1gInSc1
F-22	F-22	k1gFnSc2
v	v	k7c6
květnu	květen	k1gInSc6
2010	#num#	k4
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
výroba	výroba	k1gFnSc1
tohoto	tento	k3xDgInSc2
stroje	stroj	k1gInSc2
páté	pátá	k1gFnSc2
generace	generace	k1gFnSc1
je	být	k5eAaImIp3nS
ohraničena	ohraničit	k5eAaPmNgFnS
množstvím	množství	k1gNnSc7
187	#num#	k4
kusů	kus	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Přesto	přesto	k8xC
však	však	k9
USAF	USAF	kA
počítá	počítat	k5eAaImIp3nS
s	s	k7c7
nákupem	nákup	k1gInSc7
a	a	k8xC
nasazením	nasazení	k1gNnSc7
letounů	letoun	k1gInPc2
Lockheed	Lockheed	k1gMnSc1
Martin	Martin	k1gMnSc1
F-35	F-35	k1gFnSc2
Lightning	Lightning	k1gInSc1
II	II	kA
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
i	i	k9
navzdory	navzdory	k7c3
zvyšování	zvyšování	k1gNnSc3
ceny	cena	k1gFnSc2
programu	program	k1gInSc2
a	a	k8xC
pomalému	pomalý	k2eAgInSc3d1
procesu	proces	k1gInSc3
nasazování	nasazování	k1gNnSc2
tohoto	tento	k3xDgInSc2
stroje	stroj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nehledě	hledět	k5eNaImSgMnS
na	na	k7c4
tyto	tento	k3xDgInPc4
problémy	problém	k1gInPc4
začalo	začít	k5eAaPmAgNnS
USAF	USAF	kA
postupně	postupně	k6eAd1
vyřazovat	vyřazovat	k5eAaImF
z	z	k7c2
provozu	provoz	k1gInSc2
254	#num#	k4
letadel	letadlo	k1gNnPc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
součást	součást	k1gFnSc1
restrukturalizace	restrukturalizace	k1gFnSc2
letectva	letectvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
květnu	květen	k1gInSc6
2010	#num#	k4
zveřejněný	zveřejněný	k2eAgInSc4d1
rozpočet	rozpočet	k1gInSc4
na	na	k7c4
fiskální	fiskální	k2eAgInSc4d1
rok	rok	k1gInSc4
2011	#num#	k4
<g/>
,	,	kIx,
počítá	počítat	k5eAaImIp3nS
s	s	k7c7
vyřazením	vyřazení	k1gNnSc7
dalších	další	k2eAgInPc2d1
59	#num#	k4
strojů	stroj	k1gInPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
28	#num#	k4
kusů	kus	k1gInPc2
C-130E	C-130E	k1gFnPc2
a	a	k8xC
17	#num#	k4
kusů	kus	k1gInPc2
C-	C-	k1gFnSc2
<g/>
5	#num#	k4
<g/>
A.	A.	kA
Nedávno	nedávno	k6eAd1
dokončená	dokončený	k2eAgFnSc1d1
studie	studie	k1gFnSc1
o	o	k7c4
požadované	požadovaný	k2eAgFnPc4d1
schopnosti	schopnost	k1gFnPc4
mobility	mobilita	k1gFnSc2
(	(	kIx(
<g/>
MCRS	MCRS	kA
<g/>
)	)	kIx)
vyústila	vyústit	k5eAaPmAgFnS
do	do	k7c2
nového	nový	k2eAgInSc2d1
operačního	operační	k2eAgInSc2d1
plánu	plán	k1gInSc2
pro	pro	k7c4
nasazení	nasazení	k1gNnSc4
<g />
.	.	kIx.
</s>
<s hack="1">
strojů	stroj	k1gInPc2
C-17	C-17	k1gFnPc2
v	v	k7c6
počtu	počet	k1gInSc6
223	#num#	k4
kusů	kus	k1gInPc2
a	a	k8xC
C-5	C-5	k1gFnPc2
v	v	k7c6
počtu	počet	k1gInSc6
89	#num#	k4
kusů	kus	k1gInPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
52	#num#	k4
plně	plně	k6eAd1
modernizovaných	modernizovaný	k2eAgInPc2d1
strojů	stroj	k1gInPc2
C-	C-	k1gFnSc2
<g/>
5	#num#	k4
<g/>
M.	M.	kA
I	i	k9
přesto	přesto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
C-5A	C-5A	k1gFnSc1
nezíská	získat	k5eNaPmIp3nS
nové	nový	k2eAgInPc4d1
motory	motor	k1gInPc4
nebo	nebo	k8xC
jiné	jiný	k2eAgFnPc4d1
úpravy	úprava	k1gFnPc4
související	související	k2eAgFnPc4d1
s	s	k7c7
programem	program	k1gInSc7
rozšiřování	rozšiřování	k1gNnSc1
způsobilosti	způsobilost	k1gFnSc2
a	a	k8xC
instalace	instalace	k1gFnSc2
nových	nový	k2eAgInPc2d1
motorů	motor	k1gInPc2
(	(	kIx(
<g/>
RERP	RERP	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nemodernizovaných	modernizovaný	k2eNgFnPc2d1
strojům	stroj	k1gInPc3
bude	být	k5eAaImBp3nS
alespoň	alespoň	k9
vyměněna	vyměnit	k5eAaPmNgFnS
avionika	avionika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současnosti	současnost	k1gFnSc6
USAF	USAF	kA
provozuje	provozovat	k5eAaImIp3nS
112	#num#	k4
kusů	kus	k1gInPc2
strojů	stroj	k1gInPc2
C-	C-	k1gFnSc4
<g/>
5	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
rámci	rámec	k1gInSc6
programu	program	k1gInSc2
vyřazovaní	vyřazovaný	k2eAgMnPc1d1
starších	starý	k2eAgMnPc2d2
strojů	stroj	k1gInPc2
předpokládá	předpokládat	k5eAaImIp3nS
vyřazení	vyřazení	k1gNnSc1
23	#num#	k4
letadel	letadlo	k1gNnPc2
verze	verze	k1gFnSc1
C-	C-	k1gFnSc1
<g/>
5	#num#	k4
<g/>
A.	A.	kA
Mezitím	mezitím	k6eAd1
armádní	armádní	k2eAgMnPc1d1
plánovači	plánovač	k1gMnPc1
schválili	schválit	k5eAaPmAgMnP
nákup	nákup	k1gInSc4
10	#num#	k4
dodatečných	dodatečný	k2eAgInPc2d1
strojů	stroj	k1gInPc2
C-	C-	k1gFnSc2
<g/>
17	#num#	k4
<g/>
A	A	kA
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
celkové	celkový	k2eAgNnSc4d1
množství	množství	k1gNnSc4
těchto	tento	k3xDgInPc2
strojů	stroj	k1gInPc2
zvýšili	zvýšit	k5eAaPmAgMnP
na	na	k7c4
233	#num#	k4
kusů	kus	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
vyřazení	vyřazení	k1gNnSc6
strojů	stroj	k1gInPc2
verze	verze	k1gFnSc2
C-130E	C-130E	k1gFnSc1
bude	být	k5eAaImBp3nS
disponovat	disponovat	k5eAaBmF
transportní	transportní	k2eAgNnSc1d1
letectvo	letectvo	k1gNnSc1
USAF	USAF	kA
přibližně	přibližně	k6eAd1
400	#num#	k4
kusy	kus	k1gInPc4
C-	C-	k1gFnSc2
<g/>
130	#num#	k4
<g/>
H	H	kA
<g/>
/	/	kIx~
<g/>
J	J	kA
a	a	k8xC
38	#num#	k4
kusů	kus	k1gInPc2
C-	C-	k1gFnSc2
<g/>
27	#num#	k4
<g/>
J.	J.	kA
</s>
<s>
Implementace	implementace	k1gFnSc1
posledních	poslední	k2eAgNnPc2d1
rozhodnutí	rozhodnutí	k1gNnPc2
programu	program	k1gInSc2
na	na	k7c4
přesun	přesun	k1gInSc4
a	a	k8xC
uzavírání	uzavírání	k1gNnSc4
leteckých	letecký	k2eAgFnPc2d1
základen	základna	k1gFnPc2
(	(	kIx(
<g/>
BRAC	BRAC	kA
<g/>
)	)	kIx)
z	z	k7c2
roku	rok	k1gInSc2
2005	#num#	k4
byla	být	k5eAaImAgFnS
již	již	k6eAd1
téměř	téměř	k6eAd1
dokončena	dokončit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgFnPc4
zbývající	zbývající	k2eAgFnPc4d1
perutě	peruť	k1gFnPc4
USAF	USAF	kA
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
podléhají	podléhat	k5eAaImIp3nP
tomuto	tento	k3xDgNnSc3
rozhodnutí	rozhodnutí	k1gNnSc3
<g/>
,	,	kIx,
budou	být	k5eAaImBp3nP
deaktivovány	deaktivovat	k5eAaImNgInP
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
rámci	rámec	k1gInSc6
tohoto	tento	k3xDgInSc2
programu	program	k1gInSc2
budou	být	k5eAaImBp3nP
americké	americký	k2eAgMnPc4d1
námořnictvo	námořnictvo	k1gNnSc1
a	a	k8xC
americká	americký	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
vytvářet	vytvářet	k5eAaImF
společné	společný	k2eAgFnPc4d1
základny	základna	k1gFnPc4
(	(	kIx(
<g/>
joint	joint	k1gMnSc1
bases	bases	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
krok	krok	k1gInSc1
konsoliduje	konsolidovat	k5eAaBmIp3nS
operační	operační	k2eAgFnPc4d1
funkce	funkce	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
byly	být	k5eAaImAgFnP
předtím	předtím	k6eAd1
duplikované	duplikovaný	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Základny	základna	k1gFnPc1
USAF	USAF	kA
</s>
<s>
Vojenská	vojenský	k2eAgNnPc1d1
letiště	letiště	k1gNnPc1
Air	Air	k1gFnSc2
Force	force	k1gFnSc2
na	na	k7c6
americkém	americký	k2eAgNnSc6d1
výsostném	výsostný	k2eAgNnSc6d1
území	území	k1gNnSc6
mají	mít	k5eAaImIp3nP
označení	označení	k1gNnSc4
„	„	k?
<g/>
Air	Air	k1gFnSc2
Force	force	k1gFnSc2
Base	basa	k1gFnSc3
<g/>
“	“	k?
(	(	kIx(
<g/>
AFB	AFB	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
mimo	mimo	k7c4
něj	on	k3xPp3gMnSc4
„	„	k?
<g/>
Air	Air	k1gFnSc3
Base	basa	k1gFnSc3
<g/>
“	“	k?
(	(	kIx(
<g/>
AB	AB	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
menší	malý	k2eAgFnPc1d2
pak	pak	k6eAd1
„	„	k?
<g/>
Air	Air	k1gFnSc1
Station	station	k1gInSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
AS	as	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letiště	letiště	k1gNnSc1
využívaná	využívaný	k2eAgFnSc1d1
jinými	jiný	k2eAgFnPc7d1
ozbrojenými	ozbrojený	k2eAgFnPc7d1
silami	síla	k1gFnPc7
pak	pak	k6eAd1
mají	mít	k5eAaImIp3nP
odlišná	odlišný	k2eAgNnPc4d1
označení	označení	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s>
Mapka	mapka	k1gFnSc1
nejdůležitějších	důležitý	k2eAgFnPc2d3
základen	základna	k1gFnPc2
</s>
<s>
Základny	základna	k1gFnPc1
v	v	k7c6
USA	USA	kA
</s>
<s>
výběr	výběr	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
Andrews	Andrews	k1gInSc1
Air	Air	k1gFnSc2
Force	force	k1gFnSc3
Base	basa	k1gFnSc3
<g/>
,	,	kIx,
Maryland	Maryland	k1gInSc4
</s>
<s>
Barksdale	Barksdala	k1gFnSc3
Air	Air	k1gFnSc3
Force	force	k1gFnSc3
Base	basa	k1gFnSc3
<g/>
,	,	kIx,
Louisiana	Louisiana	k1gFnSc1
</s>
<s>
Beale	Beale	k1gInSc1
Air	Air	k1gFnSc2
Force	force	k1gFnSc3
Base	basa	k1gFnSc3
<g/>
,	,	kIx,
Kalifornie	Kalifornie	k1gFnSc1
</s>
<s>
Castle	Castle	k1gFnSc1
Air	Air	k1gFnSc2
Force	force	k1gFnSc2
Base	basa	k1gFnSc3
<g/>
,	,	kIx,
Kalifornie	Kalifornie	k1gFnSc1
</s>
<s>
Davis-Monthan	Davis-Monthan	k1gInSc1
Air	Air	k1gFnSc2
Force	force	k1gFnSc3
Base	basa	k1gFnSc3
<g/>
,	,	kIx,
Arizona	Arizona	k1gFnSc1
</s>
<s>
Dyess	Dyess	k1gInSc1
Air	Air	k1gFnSc2
Force	force	k1gFnSc3
Base	basa	k1gFnSc3
<g/>
,	,	kIx,
Texas	Texas	k1gInSc1
</s>
<s>
Edwards	Edwards	k1gInSc1
Air	Air	k1gFnSc2
Force	force	k1gFnSc3
Base	basa	k1gFnSc3
<g/>
,	,	kIx,
Kalifornie	Kalifornie	k1gFnSc1
</s>
<s>
Eglin	Eglin	k1gInSc1
Air	Air	k1gFnSc2
Force	force	k1gFnSc3
Base	basa	k1gFnSc3
<g/>
,	,	kIx,
Florida	Florida	k1gFnSc1
</s>
<s>
Ellsworth	Ellsworth	k1gInSc1
Air	Air	k1gFnSc2
Force	force	k1gFnSc3
Base	basa	k1gFnSc3
<g/>
,	,	kIx,
Jižní	jižní	k2eAgFnSc1d1
Dakota	Dakota	k1gFnSc1
</s>
<s>
Elmendorf	Elmendorf	k1gInSc1
Air	Air	k1gFnSc2
Force	force	k1gFnSc3
Base	basa	k1gFnSc3
<g/>
,	,	kIx,
Aljaška	Aljaška	k1gFnSc1
</s>
<s>
Hickam	Hickam	k1gInSc1
Air	Air	k1gFnSc2
Force	force	k1gFnSc3
Base	basa	k1gFnSc3
<g/>
,	,	kIx,
Havaj	Havaj	k1gFnSc4
</s>
<s>
Holloman	Holloman	k1gMnSc1
Air	Air	k1gFnSc2
Force	force	k1gFnSc2
Base	basa	k1gFnSc3
<g/>
,	,	kIx,
Nové	Nové	k2eAgNnSc1d1
Mexiko	Mexiko	k1gNnSc1
</s>
<s>
Lackland	Lackland	k1gInSc1
Air	Air	k1gFnSc2
Force	force	k1gFnSc3
Base	basa	k1gFnSc3
<g/>
,	,	kIx,
Texas	Texas	k1gInSc1
<g/>
,	,	kIx,
sídlo	sídlo	k1gNnSc1
ISR	ISR	kA
</s>
<s>
Langley	Langley	k1gInPc1
Air	Air	k1gFnSc2
Force	force	k1gFnSc3
Base	basa	k1gFnSc3
<g/>
,	,	kIx,
Virginie	Virginie	k1gFnSc5
</s>
<s>
Luke	Luke	k1gInSc1
Air	Air	k1gFnSc2
Force	force	k1gFnSc3
Base	basa	k1gFnSc3
<g/>
,	,	kIx,
Arizona	Arizona	k1gFnSc1
</s>
<s>
MacDill	MacDill	k1gInSc1
Air	Air	k1gFnSc2
Force	force	k1gFnSc3
Base	basa	k1gFnSc3
<g/>
,	,	kIx,
Florida	Florida	k1gFnSc1
</s>
<s>
McConnell	McConnell	k1gInSc1
Air	Air	k1gFnSc2
Force	force	k1gFnSc3
Base	basa	k1gFnSc3
<g/>
,	,	kIx,
Kansas	Kansas	k1gInSc4
</s>
<s>
Minot	Minot	k1gInSc1
Air	Air	k1gFnSc2
Force	force	k1gFnSc3
Base	basa	k1gFnSc3
<g/>
,	,	kIx,
Severní	severní	k2eAgFnSc1d1
Dakota	Dakota	k1gFnSc1
</s>
<s>
Mountain	Mountain	k2eAgInSc1d1
Home	Home	k1gInSc1
Air	Air	k1gFnSc2
Force	force	k1gFnSc3
Base	basa	k1gFnSc3
<g/>
,	,	kIx,
Idaho	Idaha	k1gFnSc5
</s>
<s>
Nellis	Nellis	k1gInSc1
Air	Air	k1gFnSc2
Force	force	k1gFnSc3
Base	basa	k1gFnSc3
<g/>
,	,	kIx,
Nevada	Nevada	k1gFnSc1
</s>
<s>
Offutt	Offutt	k1gInSc1
Air	Air	k1gFnSc2
Force	force	k1gFnSc3
Base	basa	k1gFnSc3
<g/>
,	,	kIx,
Nebraska	Nebraska	k1gFnSc1
</s>
<s>
Peterson	Peterson	k1gInSc1
Air	Air	k1gFnSc2
Force	force	k1gFnSc3
Base	basa	k1gFnSc3
<g/>
,	,	kIx,
Colorado	Colorado	k1gNnSc4
</s>
<s>
Pope	pop	k1gMnSc5
Air	Air	k1gMnSc5
Force	force	k1gFnPc1
Base	basa	k1gFnSc3
<g/>
,	,	kIx,
Severní	severní	k2eAgFnSc1d1
Karolína	Karolína	k1gFnSc1
</s>
<s>
Randolph	Randolph	k1gInSc1
Air	Air	k1gFnSc2
Force	force	k1gFnSc3
Base	basa	k1gFnSc3
<g/>
,	,	kIx,
Texas	Texas	k1gInSc1
</s>
<s>
Schriever	Schriever	k1gInSc1
Air	Air	k1gFnSc2
Force	force	k1gFnSc3
Base	basa	k1gFnSc3
<g/>
,	,	kIx,
Colorado	Colorado	k1gNnSc4
</s>
<s>
Scott	Scott	k1gInSc1
Air	Air	k1gFnSc2
Force	force	k1gFnSc3
Base	basa	k1gFnSc3
<g/>
,	,	kIx,
Illinois	Illinois	k1gFnSc3
</s>
<s>
Sheppard	Sheppard	k1gInSc1
Air	Air	k1gFnSc2
Force	force	k1gFnSc3
Base	basa	k1gFnSc3
<g/>
,	,	kIx,
Texas	Texas	k1gInSc1
</s>
<s>
Tinker	Tinker	k1gInSc1
Air	Air	k1gFnSc2
Force	force	k1gFnSc3
Base	basa	k1gFnSc3
<g/>
,	,	kIx,
Oklahoma	Oklahoma	k1gNnSc4
</s>
<s>
Tyndall	Tyndall	k1gInSc1
Air	Air	k1gFnSc2
Force	force	k1gFnSc3
Base	basa	k1gFnSc3
<g/>
,	,	kIx,
Florida	Florida	k1gFnSc1
</s>
<s>
Travis	Travis	k1gInSc1
Air	Air	k1gFnSc2
Force	force	k1gFnSc3
Base	basa	k1gFnSc3
<g/>
,	,	kIx,
Kalifornie	Kalifornie	k1gFnSc1
</s>
<s>
Vandenberg	Vandenberg	k1gInSc1
Air	Air	k1gFnSc2
Force	force	k1gFnSc3
Base	basa	k1gFnSc3
<g/>
,	,	kIx,
Kalifornie	Kalifornie	k1gFnSc1
</s>
<s>
Whiteman	Whiteman	k1gMnSc1
Air	Air	k1gFnSc2
Force	force	k1gFnSc2
Base	basa	k1gFnSc3
<g/>
,	,	kIx,
Missouri	Missouri	k1gNnSc3
</s>
<s>
Wright-Patterson	Wright-Patterson	k1gInSc1
Air	Air	k1gFnSc2
Force	force	k1gFnSc3
Base	basa	k1gFnSc3
<g/>
,	,	kIx,
Ohio	Ohio	k1gNnSc4
</s>
<s>
Základny	základna	k1gFnPc1
mimo	mimo	k7c4
USA	USA	kA
</s>
<s>
výběr	výběr	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
Andersen	Andersen	k1gMnSc1
Air	Air	k1gFnSc2
Force	force	k1gFnSc2
Base	basa	k1gFnSc3
<g/>
,	,	kIx,
Guam	Guam	k1gInSc4
</s>
<s>
Aviano	Aviana	k1gFnSc5
Air	Air	k1gMnSc4
Base	basa	k1gFnSc6
<g/>
,	,	kIx,
Itálie	Itálie	k1gFnSc1
</s>
<s>
Berlín	Berlín	k1gInSc1
Tempelhof	Tempelhof	k1gInSc1
<g/>
,	,	kIx,
Německo	Německo	k1gNnSc1
<g/>
,	,	kIx,
do	do	k7c2
června	červen	k1gInSc2
1993	#num#	k4
</s>
<s>
Clark	Clark	k1gInSc1
Air	Air	k1gFnSc2
Base	basa	k1gFnSc3
(	(	kIx(
<g/>
1903	#num#	k4
<g/>
-	-	kIx~
<g/>
1991	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Filipíny	Filipíny	k1gFnPc4
</s>
<s>
Incirlik	Incirlik	k1gMnSc1
Air	Air	k1gFnSc2
Base	basa	k1gFnSc3
<g/>
,	,	kIx,
Turecko	Turecko	k1gNnSc4
</s>
<s>
Joint	Joint	k1gMnSc1
Base	basa	k1gFnSc3
Balad	balada	k1gFnPc2
<g/>
,	,	kIx,
Irák	Irák	k1gInSc1
</s>
<s>
Kadena	Kaden	k1gMnSc4
Air	Air	k1gMnSc4
Base	basa	k1gFnSc6
<g/>
,	,	kIx,
Japonsko	Japonsko	k1gNnSc1
</s>
<s>
Kunsan	Kunsan	k1gInSc1
Air	Air	k1gFnSc2
Base	basa	k1gFnSc3
<g/>
,	,	kIx,
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
</s>
<s>
Misawa	Misawa	k6eAd1
Air	Air	k1gFnSc6
Base	basa	k1gFnSc6
<g/>
,	,	kIx,
Japonsko	Japonsko	k1gNnSc1
</s>
<s>
Ramstein	Ramstein	k1gInSc1
Air	Air	k1gFnSc2
Base	basa	k1gFnSc3
<g/>
,	,	kIx,
Německo	Německo	k1gNnSc4
</s>
<s>
Thule	Thule	k1gInSc1
Air	Air	k1gFnSc2
Base	basa	k1gFnSc3
<g/>
,	,	kIx,
Grónsko	Grónsko	k1gNnSc4
</s>
<s>
Osan	Osan	k1gInSc1
Air	Air	k1gFnSc2
Base	basa	k1gFnSc3
<g/>
,	,	kIx,
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
</s>
<s>
Yokota	Yokot	k1gMnSc4
Air	Air	k1gMnSc4
Base	basa	k1gFnSc6
<g/>
,	,	kIx,
Japonsko	Japonsko	k1gNnSc1
</s>
<s>
Nasazení	nasazení	k1gNnSc1
v	v	k7c6
konfliktech	konflikt	k1gInPc6
</s>
<s>
výběr	výběr	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
1947	#num#	k4
<g/>
–	–	k?
<g/>
1992	#num#	k4
operační	operační	k2eAgFnSc4d1
připravenost	připravenost	k1gFnSc4
bombardérů	bombardér	k1gInPc2
s	s	k7c7
jadernými	jaderný	k2eAgFnPc7d1
zbraněmi	zbraň	k1gFnPc7
během	během	k7c2
studené	studený	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
1950	#num#	k4
<g/>
–	–	k?
<g/>
1953	#num#	k4
Korejská	korejský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
1964	#num#	k4
Povstání	povstání	k1gNnPc1
Simba	Simba	k1gFnSc1
<g/>
,	,	kIx,
Konžská	konžský	k2eAgFnSc1d1
krize	krize	k1gFnSc1
</s>
<s>
1964	#num#	k4
<g/>
–	–	k?
<g/>
1973	#num#	k4
Vietnamská	vietnamský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
1973	#num#	k4
Operace	operace	k1gFnSc1
Nickel	Nickela	k1gFnPc2
Grass	Grassa	k1gFnPc2
<g/>
,	,	kIx,
letecký	letecký	k2eAgInSc1d1
most	most	k1gInSc1
se	s	k7c7
zbraněmi	zbraň	k1gFnPc7
do	do	k7c2
Izraele	Izrael	k1gInSc2
během	během	k7c2
Jomkipurské	Jomkipurský	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
1980	#num#	k4
Operace	operace	k1gFnSc1
Eagle	Eagle	k1gFnSc1
Claw	Claw	k1gFnSc1
<g/>
,	,	kIx,
operace	operace	k1gFnSc1
k	k	k7c3
záchraně	záchrana	k1gFnSc3
rukojmích	rukojmí	k1gMnPc2
z	z	k7c2
velvyslanectví	velvyslanectví	k1gNnSc2
USA	USA	kA
v	v	k7c6
Teheránu	Teherán	k1gInSc6
v	v	k7c6
dubnu	duben	k1gInSc6
1980	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1986	#num#	k4
Operace	operace	k1gFnSc1
El	Ela	k1gFnPc2
Dorado	Dorada	k1gFnSc5
Canyon	Canyon	k1gInSc1
<g/>
,	,	kIx,
nálet	nálet	k1gInSc1
proti	proti	k7c3
cílům	cíl	k1gInPc3
v	v	k7c6
Libyi	Libye	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
1990	#num#	k4
<g/>
–	–	k?
<g/>
1991	#num#	k4
Operace	operace	k1gFnSc2
Pouštní	pouštní	k2eAgInSc1d1
štít	štít	k1gInSc1
<g/>
,	,	kIx,
ochrana	ochrana	k1gFnSc1
Saúdské	saúdský	k2eAgFnSc2d1
Arábie	Arábie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
1991	#num#	k4
Operace	operace	k1gFnSc1
Pouštní	pouštní	k2eAgFnSc2d1
bouře	bouř	k1gFnSc2
<g/>
,	,	kIx,
Irák	Irák	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
1991	#num#	k4
<g/>
–	–	k?
<g/>
1996	#num#	k4
Operace	operace	k1gFnSc1
Provide	Provid	k1gInSc5
Comfort	Comfort	k1gInSc4
<g/>
,	,	kIx,
zajištění	zajištění	k1gNnSc4
bezletové	bezletový	k2eAgFnSc2d1
zóny	zóna	k1gFnSc2
v	v	k7c6
Iráku	Irák	k1gInSc6
na	na	k7c4
sever	sever	k1gInSc4
od	od	k7c2
36	#num#	k4
<g/>
.	.	kIx.
rovnoběžky	rovnoběžka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
1991	#num#	k4
<g/>
–	–	k?
<g/>
2003	#num#	k4
Operace	operace	k1gFnSc1
Southern	Southerna	k1gFnPc2
Watch	Watcha	k1gFnPc2
<g/>
,	,	kIx,
zajištění	zajištění	k1gNnSc4
bezletové	bezletový	k2eAgFnSc2d1
zóny	zóna	k1gFnSc2
jižně	jižně	k6eAd1
od	od	k7c2
33	#num#	k4
<g/>
.	.	kIx.
rovnoběžky	rovnoběžka	k1gFnSc2
v	v	k7c6
Iráku	Irák	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
1993	#num#	k4
<g/>
–	–	k?
<g/>
1995	#num#	k4
Operace	operace	k1gFnSc1
Deny	Dena	k1gFnSc2
Flight	Flighta	k1gFnPc2
<g/>
,	,	kIx,
bezletová	bezletový	k2eAgFnSc1d1
zóna	zóna	k1gFnSc1
nad	nad	k7c7
Bosnou	Bosna	k1gFnSc7
a	a	k8xC
Hercegovinou	Hercegovina	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
1995	#num#	k4
Operace	operace	k1gFnSc1
Rozhodná	rozhodný	k2eAgFnSc1d1
síla	síla	k1gFnSc1
<g/>
,	,	kIx,
bombardování	bombardování	k1gNnSc1
v	v	k7c6
Bosně	Bosna	k1gFnSc6
a	a	k8xC
Hercegovině	Hercegovina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
1996	#num#	k4
Operace	operace	k1gFnSc1
Desert	desert	k1gInSc1
Strike	Strike	k1gFnSc1
<g/>
,	,	kIx,
bombardování	bombardování	k1gNnSc1
v	v	k7c6
Iráku	Irák	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
1997	#num#	k4
<g/>
–	–	k?
<g/>
2003	#num#	k4
Operace	operace	k1gFnSc1
Northern	Northerna	k1gFnPc2
Watch	Watcha	k1gFnPc2
<g/>
,	,	kIx,
bezletová	bezletový	k2eAgFnSc1d1
zóna	zóna	k1gFnSc1
severně	severně	k6eAd1
od	od	k7c2
36	#num#	k4
<g/>
.	.	kIx.
rovnoběžky	rovnoběžka	k1gFnSc2
v	v	k7c6
Iráku	Irák	k1gInSc6
</s>
<s>
1998	#num#	k4
Operace	operace	k1gFnSc1
Pouštní	pouštní	k2eAgFnSc1d1
liška	liška	k1gFnSc1
<g/>
,	,	kIx,
bombardování	bombardování	k1gNnSc1
v	v	k7c6
Iráku	Irák	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
1999	#num#	k4
Operace	operace	k1gFnSc1
Spojenecká	spojenecký	k2eAgFnSc1d1
síla	síla	k1gFnSc1
<g/>
,	,	kIx,
bombardování	bombardování	k1gNnSc1
v	v	k7c4
Kosovu	Kosův	k2eAgFnSc4d1
a	a	k8xC
Srbsku	Srbsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
2001	#num#	k4
<g/>
–	–	k?
<g/>
současnost	současnost	k1gFnSc1
Operace	operace	k1gFnSc1
Enduring	Enduring	k1gInSc4
Freedom	Freedom	k1gInSc1
<g/>
,	,	kIx,
bombardování	bombardování	k1gNnSc1
v	v	k7c6
Afghánistánu	Afghánistán	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
2003	#num#	k4
<g/>
–	–	k?
<g/>
2010	#num#	k4
Operace	operace	k1gFnSc1
Iraqi	Iraq	k1gFnSc2
Freedom	Freedom	k1gInSc1
<g/>
,	,	kIx,
invaze	invaze	k1gFnSc1
a	a	k8xC
bombardování	bombardování	k1gNnSc1
v	v	k7c6
Iráku	Irák	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
2011	#num#	k4
Operace	operace	k1gFnSc1
Odyssey	Odyssea	k1gFnSc2
Dawn	Dawn	k1gNnSc1
<g/>
,	,	kIx,
bezletová	bezletový	k2eAgFnSc1d1
zóna	zóna	k1gFnSc1
nad	nad	k7c7
Libyí	Libye	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
2014	#num#	k4
<g/>
–	–	k?
<g/>
současnost	současnost	k1gFnSc1
Operace	operace	k1gFnSc1
Inherent	Inherent	k1gInSc4
Resolve	Resolev	k1gFnSc2
<g/>
,	,	kIx,
součást	součást	k1gFnSc4
války	válka	k1gFnSc2
proti	proti	k7c3
Islámskému	islámský	k2eAgInSc3d1
státu	stát	k1gInSc3
</s>
<s>
Seznam	seznam	k1gInSc1
letadel	letadlo	k1gNnPc2
</s>
<s>
Letadlo	letadlo	k1gNnSc1
</s>
<s>
Foto	foto	k1gNnSc1
</s>
<s>
Země	země	k1gFnSc1
původu	původ	k1gInSc2
</s>
<s>
Úloha	úloha	k1gFnSc1
</s>
<s>
Verze	verze	k1gFnSc1
</s>
<s>
Ve	v	k7c6
službě	služba	k1gFnSc6
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
Stíhací	stíhací	k2eAgNnPc1d1
letadla	letadlo	k1gNnPc1
</s>
<s>
McDonnell	McDonnell	k1gMnSc1
Douglas	Douglas	k1gMnSc1
F-15	F-15	k1gMnSc1
Eagle	Eagle	k1gInSc4
</s>
<s>
USA	USA	kA
</s>
<s>
stíhač	stíhač	k1gMnSc1
k	k	k7c3
vybojování	vybojování	k1gNnSc3
vzdušné	vzdušný	k2eAgFnSc2d1
nadvlády	nadvláda	k1gFnSc2
</s>
<s>
F-15C	F-15C	k4
</s>
<s>
266	#num#	k4
</s>
<s>
18	#num#	k4
strojů	stroj	k1gInPc2
F-15C	F-15C	k1gFnPc2
bylo	být	k5eAaImAgNnS
vybaven	vybavit	k5eAaPmNgInS
AESA	AESA	kA
radarem	radar	k1gInSc7
APG-	APG-	k1gFnSc7
<g/>
63	#num#	k4
<g/>
(	(	kIx(
<g/>
V	V	kA
<g/>
)	)	kIx)
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
McDonnell	McDonnell	k1gInSc1
Douglas	Douglas	k1gInSc1
F-15E	F-15E	k1gMnSc2
Strike	Strik	k1gMnSc2
Eagle	Eagl	k1gMnSc2
</s>
<s>
USA	USA	kA
</s>
<s>
víceúčelové	víceúčelový	k2eAgNnSc1d1
stíhací	stíhací	k2eAgNnSc1d1
letadlo	letadlo	k1gNnSc1
</s>
<s>
F-15E	F-15E	k4
</s>
<s>
219	#num#	k4
</s>
<s>
Do	do	k7c2
března	březen	k1gInSc2
2013	#num#	k4
bylo	být	k5eAaImAgNnS
8	#num#	k4
strojů	stroj	k1gInPc2
F-15E	F-15E	k1gFnPc2
vybavených	vybavený	k2eAgInPc2d1
AESA	AESA	kA
radarem	radar	k1gInSc7
AN	AN	kA
<g/>
/	/	kIx~
<g/>
APG-	APG-	k1gFnSc1
<g/>
82	#num#	k4
(	(	kIx(
<g/>
V	V	kA
<g/>
)	)	kIx)
1	#num#	k4
<g/>
,	,	kIx,
celkově	celkově	k6eAd1
se	se	k3xPyFc4
takto	takto	k6eAd1
plánuje	plánovat	k5eAaImIp3nS
zmodernizovat	zmodernizovat	k5eAaPmF
217	#num#	k4
stíhaček	stíhačka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Modernizace	modernizace	k1gFnSc1
by	by	kYmCp3nS
měla	mít	k5eAaImAgFnS
udržet	udržet	k5eAaPmF
stroje	stroj	k1gInPc4
F-15	F-15	k1gFnPc2
ve	v	k7c6
službě	služba	k1gFnSc6
do	do	k7c2
roku	rok	k1gInSc2
2035	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
General	Generat	k5eAaPmAgInS,k5eAaImAgInS
Dynamics	Dynamics	k1gInSc4
F-16	F-16	k1gMnPc2
Fighting	Fighting	k1gInSc1
Falcon	Falcon	k1gNnSc4
</s>
<s>
USA	USA	kA
</s>
<s>
víceúčelové	víceúčelový	k2eAgNnSc1d1
stíhací	stíhací	k2eAgNnSc1d1
letadlo	letadlo	k1gNnSc1
</s>
<s>
F-16C	F-16C	k4
</s>
<s>
1078	#num#	k4
</s>
<s>
F-16C	F-16C	k4
mají	mít	k5eAaImIp3nP
být	být	k5eAaImF
postupně	postupně	k6eAd1
nahrazeny	nahradit	k5eAaPmNgInP
stroji	stroj	k1gInPc7
F-	F-	k1gMnSc2
<g/>
35	#num#	k4
<g/>
A.	A.	kA
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Lockheed	Lockheed	k1gMnSc1
Martin	Martin	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Boeing	boeing	k1gInSc1
F-22	F-22	k1gMnSc1
Raptor	Raptor	k1gMnSc1
</s>
<s>
USA	USA	kA
</s>
<s>
stíhač	stíhač	k1gMnSc1
k	k	k7c3
vybojování	vybojování	k1gNnSc3
vzdušné	vzdušný	k2eAgFnSc2d1
nadvlády	nadvláda	k1gFnSc2
</s>
<s>
186	#num#	k4
</s>
<s>
149	#num#	k4
letadel	letadlo	k1gNnPc2
bylo	být	k5eAaImAgNnS
zmodernizováno	zmodernizovat	k5eAaPmNgNnS
na	na	k7c4
úroveň	úroveň	k1gFnSc4
Block	Block	k1gInSc1
3.1	3.1	k4
<g/>
,	,	kIx,
v	v	k7c6
jejímž	jejíž	k3xOyRp3gInSc6
rámci	rámec	k1gInSc6
byly	být	k5eAaImAgFnP
integrované	integrovaný	k2eAgFnPc1d1
pumy	puma	k1gFnPc1
typu	typ	k1gInSc2
JDAM	JDAM	kA
a	a	k8xC
SDB	SDB	kA
<g/>
.	.	kIx.
</s>
<s>
Lockheed	Lockheed	k1gMnSc1
Martin	Martin	k1gMnSc1
F-35	F-35	k1gFnSc2
Lightning	Lightning	k1gInSc1
II	II	kA
</s>
<s>
USA	USA	kA
</s>
<s>
víceúčelové	víceúčelový	k2eAgNnSc1d1
stíhací	stíhací	k2eAgNnSc1d1
letadlo	letadlo	k1gNnSc1
</s>
<s>
F-35A	F-35A	k4
</s>
<s>
229	#num#	k4
</s>
<s>
F-35A	F-35A	k4
by	by	kYmCp3nS
měly	mít	k5eAaImAgFnP
dosáhnout	dosáhnout	k5eAaPmF
počáteční	počáteční	k2eAgFnSc2d1
operační	operační	k2eAgFnSc2d1
schopnosti	schopnost	k1gFnSc2
do	do	k7c2
srpna	srpen	k1gInSc2
2016	#num#	k4
a	a	k8xC
plné	plný	k2eAgFnSc2d1
operační	operační	k2eAgFnSc2d1
schopnosti	schopnost	k1gFnSc2
do	do	k7c2
roku	rok	k1gInSc2
2022	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Dalších	další	k2eAgInPc2d1
1	#num#	k4
534	#num#	k4
objednaných	objednaný	k2eAgMnPc2d1
<g/>
.	.	kIx.
</s>
<s>
Bojová	bojový	k2eAgNnPc1d1
letadla	letadlo	k1gNnPc1
</s>
<s>
Fairchild	Fairchild	k1gMnSc1
A-10	A-10	k1gMnSc1
Thunderbolt	Thunderbolt	k1gMnSc1
II	II	kA
</s>
<s>
USA	USA	kA
</s>
<s>
bitevní	bitevní	k2eAgInSc1d1
letoun	letoun	k1gInSc1
</s>
<s>
A-10C	A-10C	k4
</s>
<s>
287	#num#	k4
</s>
<s>
A-10C	A-10C	k4
mají	mít	k5eAaImIp3nP
být	být	k5eAaImF
postupně	postupně	k6eAd1
nahrazeny	nahradit	k5eAaPmNgInP
stroji	stroj	k1gInPc7
F-	F-	k1gMnSc2
<g/>
35	#num#	k4
<g/>
A.	A.	kA
</s>
<s>
Lockheed	Lockheed	k1gMnSc1
AC-130	AC-130	k1gMnSc1
</s>
<s>
USA	USA	kA
</s>
<s>
letoun	letoun	k1gInSc1
palebné	palebný	k2eAgFnSc2d1
podpory	podpora	k1gFnSc2
</s>
<s>
AC-	AC-	k?
<g/>
130	#num#	k4
<g/>
JAC-	JAC-	k1gFnSc1
<g/>
130	#num#	k4
<g/>
U	U	kA
<g/>
/	/	kIx~
<g/>
W	W	kA
</s>
<s>
1120	#num#	k4
</s>
<s>
Bombardéry	bombardér	k1gInPc1
</s>
<s>
Rockwell	Rockwell	k1gMnSc1
B-1	B-1	k1gMnSc1
Lancer	Lancer	k1gMnSc1
</s>
<s>
USA	USA	kA
</s>
<s>
strategický	strategický	k2eAgInSc4d1
bombardér	bombardér	k1gInSc4
</s>
<s>
B-1B	B-1B	k4
</s>
<s>
59	#num#	k4
</s>
<s>
Northrop	Northrop	k1gInSc1
B-2	B-2	k1gFnSc2
Spirit	Spirita	k1gFnPc2
</s>
<s>
USA	USA	kA
</s>
<s>
strategický	strategický	k2eAgInSc4d1
bombardér	bombardér	k1gInSc4
</s>
<s>
B-2	B-2	k4
</s>
<s>
20	#num#	k4
</s>
<s>
Boeing	boeing	k1gInSc1
B-52	B-52	k1gFnSc2
Stratofortress	Stratofortressa	k1gFnPc2
</s>
<s>
USA	USA	kA
</s>
<s>
strategický	strategický	k2eAgInSc4d1
bombardér	bombardér	k1gInSc4
</s>
<s>
B-52H	B-52H	k4
</s>
<s>
74	#num#	k4
</s>
<s>
Dopravní	dopravní	k2eAgNnPc1d1
letadla	letadlo	k1gNnPc1
</s>
<s>
Lockheed	Lockheed	k1gMnSc1
C-5	C-5	k1gMnSc1
Galaxy	Galax	k1gInPc7
</s>
<s>
USA	USA	kA
</s>
<s>
strategické	strategický	k2eAgNnSc1d1
transportní	transportní	k2eAgNnSc1d1
letadlo	letadlo	k1gNnSc1
</s>
<s>
C-5M	C-5M	k4
</s>
<s>
52	#num#	k4
</s>
<s>
Boeing	boeing	k1gInSc1
C-17	C-17	k1gFnSc2
Globemaster	Globemaster	k1gMnSc1
III	III	kA
</s>
<s>
USA	USA	kA
</s>
<s>
strategické	strategický	k2eAgNnSc1d1
transportní	transportní	k2eAgNnSc1d1
letadlo	letadlo	k1gNnSc1
</s>
<s>
C-17	C-17	k4
</s>
<s>
222	#num#	k4
</s>
<s>
Lockheed	Lockheed	k1gMnSc1
C-130	C-130	k1gMnSc1
Hercules	Hercules	k1gMnSc1
</s>
<s>
USA	USA	kA
</s>
<s>
taktické	taktický	k2eAgNnSc1d1
transportní	transportní	k2eAgNnSc1d1
letadlo	letadlo	k1gNnSc1
</s>
<s>
C-130HLC-130	C-130HLC-130	k4
</s>
<s>
19210	#num#	k4
</s>
<s>
Lockheed	Lockheed	k1gMnSc1
Martin	Martin	k1gMnSc1
C-130J	C-130J	k1gMnSc1
Super	super	k2eAgInSc1d1
Hercules	Hercules	k1gInSc1
</s>
<s>
USA	USA	kA
</s>
<s>
taktické	taktický	k2eAgNnSc1d1
transportní	transportní	k2eAgNnSc1d1
letadlo	letadlo	k1gNnSc1
</s>
<s>
C-130J	C-130J	k4
</s>
<s>
132	#num#	k4
</s>
<s>
Objednaných	objednaný	k2eAgInPc2d1
dalších	další	k2eAgInPc2d1
18	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Boeing	boeing	k1gInSc4
C-32	C-32	k1gFnSc2
</s>
<s>
USA	USA	kA
</s>
<s>
doprava	doprava	k1gFnSc1
VIP	VIP	kA
</s>
<s>
C-32B	C-32B	k4
</s>
<s>
4	#num#	k4
</s>
<s>
Dornier	Dornier	k1gInSc1
328	#num#	k4
</s>
<s>
Německo	Německo	k1gNnSc1
</s>
<s>
dopravní	dopravní	k2eAgNnSc4d1
letadlo	letadlo	k1gNnSc4
(	(	kIx(
<g/>
pro	pro	k7c4
cestující	cestující	k1gMnPc4
<g/>
)	)	kIx)
</s>
<s>
C-146A	C-146A	k4
</s>
<s>
20	#num#	k4
</s>
<s>
PZL	PZL	kA
M28	M28	k1gMnSc1
Skytruck	Skytruck	k1gMnSc1
</s>
<s>
Polsko	Polsko	k1gNnSc1
Polsko	Polsko	k1gNnSc1
</s>
<s>
dopravní	dopravní	k2eAgNnSc4d1
letadlo	letadlo	k1gNnSc4
(	(	kIx(
<g/>
pro	pro	k7c4
cestující	cestující	k1gMnPc4
<g/>
)	)	kIx)
</s>
<s>
C-145A	C-145A	k4
</s>
<s>
5	#num#	k4
</s>
<s>
Beechcraft	Beechcraft	k1gInSc1
1900	#num#	k4
</s>
<s>
USA	USA	kA
</s>
<s>
dopravní	dopravní	k2eAgNnSc4d1
letadlo	letadlo	k1gNnSc4
(	(	kIx(
<g/>
pro	pro	k7c4
cestující	cestující	k1gMnPc4
<g/>
)	)	kIx)
</s>
<s>
C-12J	C-12J	k4
</s>
<s>
3	#num#	k4
</s>
<s>
Boeing	boeing	k1gInSc4
C-40	C-40	k1gFnSc2
</s>
<s>
USA	USA	kA
</s>
<s>
dopravní	dopravní	k2eAgNnSc4d1
letadlo	letadlo	k1gNnSc4
(	(	kIx(
<g/>
pro	pro	k7c4
cestující	cestující	k1gMnPc4
<g/>
)	)	kIx)
</s>
<s>
C-	C-	k?
<g/>
40	#num#	k4
<g/>
B	B	kA
<g/>
/	/	kIx~
<g/>
C	C	kA
</s>
<s>
12	#num#	k4
</s>
<s>
Cessna	Cessna	k1gFnSc1
208	#num#	k4
Caravan	Caravan	k1gMnSc1
</s>
<s>
USA	USA	kA
</s>
<s>
dopravní	dopravní	k2eAgNnSc4d1
letadlo	letadlo	k1gNnSc4
(	(	kIx(
<g/>
pro	pro	k7c4
cestující	cestující	k1gMnPc4
<g/>
)	)	kIx)
</s>
<s>
UC-27B	UC-27B	k4
</s>
<s>
2	#num#	k4
</s>
<s>
de	de	k?
Havilland	Havilland	k1gInSc1
Canada	Canada	k1gFnSc1
DHC-6	DHC-6	k1gMnSc1
Twin	Twin	k1gMnSc1
Otter	Otter	k1gMnSc1
</s>
<s>
Kanada	Kanada	k1gFnSc1
Kanada	Kanada	k1gFnSc1
</s>
<s>
letadlo	letadlo	k1gNnSc1
armádního	armádní	k2eAgInSc2d1
parašutistického	parašutistický	k2eAgInSc2d1
týmu	tým	k1gInSc2
</s>
<s>
UV-18	UV-18	k4
</s>
<s>
1	#num#	k4
</s>
<s>
Gulfstream	Gulfstream	k1gInSc1
IV	Iva	k1gFnPc2
</s>
<s>
USA	USA	kA
</s>
<s>
doprava	doprava	k1gFnSc1
VIP	VIP	kA
</s>
<s>
C-	C-	k?
<g/>
20	#num#	k4
<g/>
A	A	kA
<g/>
/	/	kIx~
<g/>
B	B	kA
<g/>
/	/	kIx~
<g/>
C	C	kA
<g/>
/	/	kIx~
<g/>
F	F	kA
</s>
<s>
1	#num#	k4
</s>
<s>
Beechcraft	Beechcraft	k2eAgMnSc1d1
C-12	C-12	k1gMnSc1
Huron	Huron	k1gMnSc1
</s>
<s>
USA	USA	kA
</s>
<s>
dopravní	dopravní	k2eAgNnSc4d1
letadlo	letadlo	k1gNnSc4
(	(	kIx(
<g/>
pro	pro	k7c4
cestující	cestující	k1gMnPc4
<g/>
)	)	kIx)
</s>
<s>
C-12	C-12	k4
</s>
<s>
23	#num#	k4
</s>
<s>
Learjet	Learjet	k1gInSc1
35	#num#	k4
</s>
<s>
USA	USA	kA
</s>
<s>
doprava	doprava	k1gFnSc1
VIP	VIP	kA
</s>
<s>
C-21A	C-21A	k4
</s>
<s>
18	#num#	k4
</s>
<s>
Fairchild	Fairchild	k6eAd1
Swearingen	Swearingen	k1gInSc1
Metroliner	Metrolinra	k1gFnPc2
</s>
<s>
USA	USA	kA
</s>
<s>
dopravní	dopravní	k2eAgNnSc4d1
letadlo	letadlo	k1gNnSc4
(	(	kIx(
<g/>
pro	pro	k7c4
cestující	cestující	k1gMnPc4
<g/>
)	)	kIx)
</s>
<s>
C-26A	C-26A	k4
</s>
<s>
1	#num#	k4
</s>
<s>
Pilatus	Pilatus	k1gMnSc1
PC-12	PC-12	k1gMnSc1
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1
Švýcarsko	Švýcarsko	k1gNnSc1
</s>
<s>
dopravní	dopravní	k2eAgNnSc4d1
letadlo	letadlo	k1gNnSc4
(	(	kIx(
<g/>
pro	pro	k7c4
cestující	cestující	k1gMnPc4
<g/>
)	)	kIx)
</s>
<s>
U-28A	U-28A	k4
</s>
<s>
12	#num#	k4
</s>
<s>
Letadla	letadlo	k1gNnPc1
speciálního	speciální	k2eAgNnSc2d1
určení	určení	k1gNnSc2
</s>
<s>
Boeing	boeing	k1gInSc4
E-3	E-3	k1gFnSc2
Sentry	Sentr	k1gMnPc7
</s>
<s>
USA	USA	kA
</s>
<s>
letadlo	letadlo	k1gNnSc1
včasné	včasný	k2eAgFnSc2d1
výstrahy	výstraha	k1gFnSc2
a	a	k8xC
řízení	řízení	k1gNnSc2
</s>
<s>
E-	E-	k?
<g/>
3	#num#	k4
<g/>
B	B	kA
<g/>
/	/	kIx~
<g/>
C	C	kA
<g/>
/	/	kIx~
<g/>
G	G	kA
</s>
<s>
31	#num#	k4
</s>
<s>
Northrop	Northrop	k1gInSc1
Grumman	Grumman	k1gMnSc1
E-8	E-8	k1gMnSc1
Joint	Joint	k1gMnSc1
STARS	STARS	kA
</s>
<s>
USA	USA	kA
</s>
<s>
letadlo	letadlo	k1gNnSc1
velení	velení	k1gNnSc2
a	a	k8xC
řízení	řízení	k1gNnSc2
<g/>
,	,	kIx,
průzkumné	průzkumný	k2eAgNnSc1d1
letadlo	letadlo	k1gNnSc1
</s>
<s>
E-8C	E-8C	k4
</s>
<s>
16	#num#	k4
</s>
<s>
Boeing	boeing	k1gInSc4
E-4	E-4	k1gFnSc2
</s>
<s>
USA	USA	kA
</s>
<s>
letadlo	letadlo	k1gNnSc1
velení	velení	k1gNnSc2
a	a	k8xC
řízení	řízení	k1gNnSc2
</s>
<s>
E-4B	E-4B	k4
</s>
<s>
4	#num#	k4
</s>
<s>
CASA	CASA	kA
<g/>
/	/	kIx~
<g/>
IPTN	IPTN	kA
CN-235	CN-235	k1gMnSc1
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
SIGINT	SIGINT	kA
</s>
<s>
CN-235	CN-235	k4
</s>
<s>
5	#num#	k4
</s>
<s>
Bombardier	Bombardier	k1gMnSc1
Dash	Dash	k1gMnSc1
8	#num#	k4
</s>
<s>
USAKanada	USAKanada	k1gFnSc1
Kanada	Kanada	k1gFnSc1
</s>
<s>
průzkumné	průzkumný	k2eAgNnSc1d1
letadlo	letadlo	k1gNnSc1
</s>
<s>
E-9A	E-9A	k4
</s>
<s>
3	#num#	k4
</s>
<s>
Lockheed	Lockheed	k1gMnSc1
EC-130	EC-130	k1gMnSc1
</s>
<s>
USA	USA	kA
</s>
<s>
letadlo	letadlo	k1gNnSc4
pro	pro	k7c4
rádioelektronický	rádioelektronický	k2eAgInSc4d1
boj	boj	k1gInSc4
</s>
<s>
EC-130HEC-130J	EC-130HEC-130J	k4
</s>
<s>
147	#num#	k4
</s>
<s>
Bombardier	Bombardier	k1gInSc4
Global	globat	k5eAaImAgInS
Express	express	k1gInSc1
</s>
<s>
USA	USA	kA
</s>
<s>
letadlo	letadlo	k1gNnSc1
pro	pro	k7c4
komunikační	komunikační	k2eAgNnSc4d1
spojení	spojení	k1gNnSc4
</s>
<s>
E-11A	E-11A	k4
</s>
<s>
4	#num#	k4
</s>
<s>
Lockheed	Lockheed	k1gMnSc1
HC-130	HC-130	k1gMnSc1
</s>
<s>
USA	USA	kA
</s>
<s>
záchranné	záchranný	k2eAgNnSc1d1
letadlo	letadlo	k1gNnSc1
</s>
<s>
HC-130JHC-130N	HC-130JHC-130N	k4
</s>
<s>
1417	#num#	k4
</s>
<s>
Boeing	boeing	k1gInSc1
KC-135	KC-135	k1gMnSc1
Stratotanker	Stratotanker	k1gMnSc1
</s>
<s>
USA	USA	kA
</s>
<s>
tankovací	tankovací	k2eAgNnSc1d1
letadlo	letadlo	k1gNnSc1
</s>
<s>
KC-135R	KC-135R	k4
</s>
<s>
1	#num#	k4
</s>
<s>
Beechcraft	Beechcraft	k2eAgMnSc1d1
C-12	C-12	k1gMnSc1
Huron	Huron	k1gMnSc1
</s>
<s>
USA	USA	kA
</s>
<s>
průzkumné	průzkumný	k2eAgNnSc1d1
letadlo	letadlo	k1gNnSc1
</s>
<s>
MC-12W	MC-12W	k4
</s>
<s>
53	#num#	k4
</s>
<s>
Fairchild	Fairchild	k6eAd1
Swearingen	Swearingen	k1gInSc1
Metroliner	Metrolinra	k1gFnPc2
</s>
<s>
USA	USA	kA
</s>
<s>
průzkumné	průzkumný	k2eAgNnSc1d1
letadlo	letadlo	k1gNnSc1
</s>
<s>
RC-26B	RC-26B	k4
</s>
<s>
11	#num#	k4
</s>
<s>
Boeing	boeing	k1gInSc1
OC-135B	OC-135B	k1gMnSc1
Open	Open	k1gMnSc1
Skies	Skies	k1gMnSc1
</s>
<s>
USA	USA	kA
</s>
<s>
průzkumné	průzkumný	k2eAgNnSc1d1
letadlo	letadlo	k1gNnSc1
</s>
<s>
OC-135	OC-135	k4
</s>
<s>
2	#num#	k4
</s>
<s>
Pilatus	Pilatus	k1gMnSc1
PC-12	PC-12	k1gMnSc1
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1
Švýcarsko	Švýcarsko	k1gNnSc1
</s>
<s>
průzkumné	průzkumný	k2eAgNnSc1d1
letadlo	letadlo	k1gNnSc1
</s>
<s>
U-28	U-28	k4
</s>
<s>
16	#num#	k4
</s>
<s>
Boeing	boeing	k1gInSc4
RC-135	RC-135	k1gFnSc2
</s>
<s>
USA	USA	kA
</s>
<s>
letadlo	letadlo	k1gNnSc1
elektronického	elektronický	k2eAgInSc2d1
průzkumu	průzkum	k1gInSc2
</s>
<s>
RC-135	RC-135	k4
</s>
<s>
21	#num#	k4
</s>
<s>
Lockheed	Lockheed	k1gMnSc1
U-2	U-2	k1gMnSc1
</s>
<s>
USA	USA	kA
</s>
<s>
průzkumné	průzkumný	k2eAgNnSc1d1
letadlo	letadlo	k1gNnSc1
</s>
<s>
U-2S	U-2S	k4
</s>
<s>
26	#num#	k4
</s>
<s>
Lockheed	Lockheed	k1gMnSc1
WC-130	WC-130	k1gMnSc1
</s>
<s>
USA	USA	kA
</s>
<s>
letadlo	letadlo	k1gNnSc1
pro	pro	k7c4
průzkum	průzkum	k1gInSc4
počasí	počasí	k1gNnSc2
</s>
<s>
WC-130J	WC-130J	k4
</s>
<s>
10	#num#	k4
</s>
<s>
Boeing	boeing	k1gInSc1
WC-135	WC-135	k1gFnSc1
Constant	Constant	k1gMnSc1
Phoenix	Phoenix	k1gInSc1
</s>
<s>
USA	USA	kA
</s>
<s>
letadlo	letadlo	k1gNnSc1
pro	pro	k7c4
průzkum	průzkum	k1gInSc4
počasí	počasí	k1gNnSc2
</s>
<s>
WC-	WC-	k?
<g/>
135	#num#	k4
<g/>
C	C	kA
<g/>
/	/	kIx~
<g/>
W	W	kA
</s>
<s>
2	#num#	k4
</s>
<s>
McDonnell	McDonnell	k1gMnSc1
Douglas	Douglas	k1gMnSc1
KC-10	KC-10	k1gMnSc1
</s>
<s>
USA	USA	kA
</s>
<s>
tankovací	tankovací	k2eAgNnSc1d1
letadlo	letadlo	k1gNnSc1
</s>
<s>
KC-10	KC-10	k4
</s>
<s>
59	#num#	k4
</s>
<s>
Boeing	boeing	k1gInSc1
KC-135	KC-135	k1gMnSc1
Stratotanker	Stratotanker	k1gMnSc1
</s>
<s>
USA	USA	kA
</s>
<s>
tankovací	tankovací	k2eAgNnSc1d1
letadlo	letadlo	k1gNnSc1
</s>
<s>
KC-	KC-	k?
<g/>
135	#num#	k4
<g/>
R	R	kA
<g/>
/	/	kIx~
<g/>
T	T	kA
</s>
<s>
396	#num#	k4
</s>
<s>
Lockheed	Lockheed	k1gMnSc1
MC-130	MC-130	k1gMnSc1
</s>
<s>
USA	USA	kA
</s>
<s>
tankovací	tankovací	k2eAgNnSc1d1
letadlo	letadlo	k1gNnSc1
</s>
<s>
MC-	MC-	k?
<g/>
130	#num#	k4
<g/>
H	H	kA
<g/>
/	/	kIx~
<g/>
PMC-	PMC-	k1gMnSc1
<g/>
130	#num#	k4
<g/>
J	J	kA
</s>
<s>
1651	#num#	k4
</s>
<s>
Objednáno	objednán	k2eAgNnSc1d1
dalších	další	k2eAgNnPc2d1
13	#num#	k4
letadel	letadlo	k1gNnPc2
verze	verze	k1gFnSc1
MC-	MC-	k1gFnSc1
<g/>
130	#num#	k4
<g/>
J.	J.	kA
</s>
<s>
Boeing	boeing	k1gInSc1
KC-46	KC-46	k1gMnSc1
Pegasus	Pegasus	k1gMnSc1
</s>
<s>
USA	USA	kA
</s>
<s>
tankovací	tankovací	k2eAgNnSc1d1
letadlo	letadlo	k1gNnSc1
</s>
<s>
23	#num#	k4
</s>
<s>
Objednaných	objednaný	k2eAgInPc2d1
dalších	další	k2eAgInPc2d1
38	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Gulfstream	Gulfstream	k1gInSc1
G550	G550	k1gFnSc2
</s>
<s>
USA	USA	kA
</s>
<s>
SIGINT	SIGINT	kA
<g/>
/	/	kIx~
<g/>
PSYOP	PSYOP	kA
</s>
<s>
EC-35B	EC-35B	k4
</s>
<s>
Objednány	objednán	k2eAgInPc1d1
další	další	k2eAgInPc1d1
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Cvičná	cvičný	k2eAgNnPc1d1
letadla	letadlo	k1gNnPc1
</s>
<s>
Northrop	Northrop	k1gInSc1
Grumman	Grumman	k1gMnSc1
E-8	E-8	k1gMnSc1
Joint	Joint	k1gMnSc1
STARS	STARS	kA
</s>
<s>
USA	USA	kA
</s>
<s>
cvičné	cvičný	k2eAgNnSc1d1
letadlo	letadlo	k1gNnSc1
</s>
<s>
E-8C	E-8C	k4
</s>
<s>
1	#num#	k4
</s>
<s>
T-1	T-1	k4
Jayhawk	Jayhawk	k1gInSc1
</s>
<s>
USA	USA	kA
</s>
<s>
cvičné	cvičný	k2eAgNnSc1d1
letadlo	letadlo	k1gNnSc1
</s>
<s>
176	#num#	k4
</s>
<s>
Boeing	boeing	k1gInSc4
C-135	C-135	k1gFnSc2
</s>
<s>
USA	USA	kA
</s>
<s>
cvičné	cvičný	k2eAgNnSc1d1
letadlo	letadlo	k1gNnSc1
</s>
<s>
3	#num#	k4
</s>
<s>
McDonnell	McDonnell	k1gMnSc1
Douglas	Douglas	k1gMnSc1
F-15	F-15	k1gMnSc1
Eagle	Eagle	k1gInSc4
</s>
<s>
USA	USA	kA
</s>
<s>
stíhač	stíhač	k1gMnSc1
k	k	k7c3
vybojování	vybojování	k1gNnSc3
vzdušné	vzdušný	k2eAgFnSc2d1
nadvlády	nadvláda	k1gFnSc2
</s>
<s>
F-15D	F-15D	k4
</s>
<s>
26	#num#	k4
</s>
<s>
General	Generat	k5eAaImAgInS,k5eAaPmAgInS
Dynamics	Dynamics	k1gInSc4
F-16	F-16	k1gMnPc2
Fighting	Fighting	k1gInSc1
Falcon	Falcona	k1gFnPc2
</s>
<s>
USA	USA	kA
</s>
<s>
víceúčelové	víceúčelový	k2eAgNnSc1d1
stíhací	stíhací	k2eAgNnSc1d1
letadlo	letadlo	k1gNnSc1
</s>
<s>
F-16D	F-16D	k4
</s>
<s>
150	#num#	k4
</s>
<s>
Lockheed	Lockheed	k1gMnSc1
Martin	Martin	k1gMnSc1
F-35	F-35	k1gFnSc2
Lightning	Lightning	k1gInSc1
II	II	kA
</s>
<s>
USA	USA	kA
</s>
<s>
víceúčelové	víceúčelový	k2eAgNnSc1d1
stíhací	stíhací	k2eAgNnSc1d1
letadlo	letadlo	k1gNnSc1
</s>
<s>
F-35A	F-35A	k4
</s>
<s>
26	#num#	k4
</s>
<s>
Cirrus	Cirrus	k1gMnSc1
SR20	SR20	k1gMnSc1
</s>
<s>
USA	USA	kA
</s>
<s>
cvičné	cvičný	k2eAgNnSc1d1
letadlo	letadlo	k1gNnSc1
</s>
<s>
T-53A	T-53A	k4
</s>
<s>
25	#num#	k4
</s>
<s>
Beechcraft	Beechcraft	k2eAgMnSc1d1
T-6	T-6	k1gMnSc1
Texan	Texan	k1gMnSc1
II	II	kA
</s>
<s>
USA	USA	kA
</s>
<s>
cvičné	cvičný	k2eAgNnSc1d1
letadlo	letadlo	k1gNnSc1
</s>
<s>
446	#num#	k4
</s>
<s>
Northrop	Northrop	k1gInSc1
T-38	T-38	k1gFnPc2
Talon	talon	k1gInSc4
</s>
<s>
USA	USA	kA
</s>
<s>
cvičné	cvičný	k2eAgNnSc1d1
letadlo	letadlo	k1gNnSc1
</s>
<s>
T-	T-	k?
<g/>
38	#num#	k4
<g/>
A	A	kA
<g/>
/	/	kIx~
<g/>
C	C	kA
</s>
<s>
505	#num#	k4
</s>
<s>
Lockheed	Lockheed	k1gMnSc1
U-2	U-2	k1gMnSc1
</s>
<s>
USA	USA	kA
</s>
<s>
cvičné	cvičný	k2eAgNnSc1d1
letadlo	letadlo	k1gNnSc1
</s>
<s>
TU-2S	TU-2S	k4
</s>
<s>
4	#num#	k4
</s>
<s>
Bell	bell	k1gInSc1
UH-1	UH-1	k1gFnSc2
Iroquois	Iroquois	k1gFnSc2
</s>
<s>
USA	USA	kA
</s>
<s>
cvičný	cvičný	k2eAgInSc1d1
vrtulník	vrtulník	k1gInSc1
</s>
<s>
TH-1H	TH-1H	k4
</s>
<s>
40	#num#	k4
</s>
<s>
Grob	Grob	k1gInSc1
G	G	kA
120	#num#	k4
TP	TP	kA
</s>
<s>
Německo	Německo	k1gNnSc1
</s>
<s>
cvičné	cvičný	k2eAgNnSc1d1
letadlo	letadlo	k1gNnSc1
</s>
<s>
5	#num#	k4
</s>
<s>
Embraer	Embraer	k1gInSc1
EMB	EMB	kA
314	#num#	k4
</s>
<s>
Brazílie	Brazílie	k1gFnSc1
</s>
<s>
cvičné	cvičný	k2eAgNnSc1d1
letadlo	letadlo	k1gNnSc1
</s>
<s>
9	#num#	k4
</s>
<s>
Objednaný	objednaný	k2eAgInSc1d1
další	další	k2eAgInSc1d1
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Vrtulníky	vrtulník	k1gInPc1
</s>
<s>
Bell	bell	k1gInSc1
Boeing	boeing	k1gInSc1
V-22	V-22	k1gMnSc2
Osprey	Osprea	k1gMnSc2
</s>
<s>
USA	USA	kA
</s>
<s>
dopravní	dopravní	k2eAgInSc1d1
konvertoplán	konvertoplán	k2eAgInSc1d1
</s>
<s>
CV-22B	CV-22B	k4
</s>
<s>
50	#num#	k4
</s>
<s>
Objednaný	objednaný	k2eAgInSc1d1
další	další	k2eAgInSc1d1
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Sikorsky	Sikorsky	k6eAd1
HH-60	HH-60	k1gMnSc1
Pave	Pav	k1gFnSc2
Hawk	Hawk	k1gMnSc1
</s>
<s>
USA	USA	kA
</s>
<s>
dopravní	dopravní	k2eAgInSc1d1
vrtulník	vrtulník	k1gInSc1
</s>
<s>
HH-	HH-	k?
<g/>
60	#num#	k4
<g/>
G	G	kA
<g/>
/	/	kIx~
<g/>
U	U	kA
<g/>
/	/	kIx~
<g/>
W	W	kA
<g/>
/	/	kIx~
<g/>
MH-	MH-	k1gMnSc1
<g/>
60	#num#	k4
<g/>
G	G	kA
</s>
<s>
84	#num#	k4
</s>
<s>
Objednaných	objednaný	k2eAgInPc2d1
dalších	další	k2eAgInPc2d1
22	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Bell	bell	k1gInSc1
UH-1N	UH-1N	k1gFnSc2
Twin	Twina	k1gFnPc2
Huey	Huea	k1gFnSc2
</s>
<s>
USA	USA	kA
</s>
<s>
užitkový	užitkový	k2eAgInSc1d1
vrtulník	vrtulník	k1gInSc1
</s>
<s>
UH-1N	UH-1N	k4
</s>
<s>
65	#num#	k4
</s>
<s>
AgustaWestland	AgustaWestland	k1gInSc1
AW139	AW139	k1gFnSc2
</s>
<s>
USA	USA	kA
</s>
<s>
užitkový	užitkový	k2eAgInSc1d1
vrtulník	vrtulník	k1gInSc1
</s>
<s>
MH-139	MH-139	k4
</s>
<s>
Objednaných	objednaný	k2eAgInPc2d1
84	#num#	k4
mají	mít	k5eAaImIp3nP
nahradit	nahradit	k5eAaPmF
Bell	bell	k1gInSc4
UH-1N	UH-1N	k1gFnSc2
Twin	Twin	k1gInSc4
Huey	Huea	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Bezpilotní	bezpilotní	k2eAgNnPc1d1
letadla	letadlo	k1gNnPc1
</s>
<s>
General	Generat	k5eAaPmAgInS,k5eAaImAgInS
Atomics	Atomics	k1gInSc1
MQ-1	MQ-1	k1gMnSc1
Predator	Predator	k1gMnSc1
</s>
<s>
USA	USA	kA
</s>
<s>
bezpilotní	bezpilotní	k2eAgNnSc1d1
letadlo	letadlo	k1gNnSc1
</s>
<s>
MQ-1B	MQ-1B	k4
</s>
<s>
150	#num#	k4
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
General	Generat	k5eAaPmAgInS,k5eAaImAgInS
Atomics	Atomics	k1gInSc1
MQ-9	MQ-9	k1gMnSc1
Reaper	Reaper	k1gMnSc1
</s>
<s>
USA	USA	kA
</s>
<s>
bezpilotní	bezpilotní	k2eAgNnSc1d1
letadlo	letadlo	k1gNnSc1
</s>
<s>
MQ-9	MQ-9	k4
</s>
<s>
163	#num#	k4
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Lockheed	Lockheed	k1gMnSc1
Martin	Martin	k1gMnSc1
RQ-170	RQ-170	k1gMnSc1
Sentinel	sentinel	k1gInSc4
</s>
<s>
USA	USA	kA
</s>
<s>
bezpilotní	bezpilotní	k2eAgNnSc1d1
letadlo	letadlo	k1gNnSc1
</s>
<s>
neznámý	známý	k2eNgInSc1d1
počet	počet	k1gInSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
United	United	k1gMnSc1
States	States	k1gMnSc1
Air	Air	k1gMnSc1
Force	force	k1gFnSc4
na	na	k7c6
slovenské	slovenský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
OREN	OREN	kA
<g/>
,	,	kIx,
Amir	Amir	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gen.	gen.	kA
David	David	k1gMnSc1
Goldfein	Goldfein	k1gMnSc1
to	ten	k3xDgNnSc4
Be	Be	k1gFnSc7
Second	Second	k1gMnSc1
Jewish	Jewish	k1gMnSc1
U.	U.	kA
<g/>
S.	S.	kA
Air	Air	k1gMnSc1
Force	force	k1gFnSc2
Chief	Chief	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Haarec	Haarec	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
World	World	k1gMnSc1
Air	Air	k1gMnSc1
Forces	Forces	k1gMnSc1
2016	#num#	k4
16	#num#	k4
<g/>
.	.	kIx.
leden	leden	k1gInSc1
2016	#num#	k4
Flightglobal	Flightglobal	k1gFnPc2
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
↑	↑	k?
Air	Air	k1gMnSc1
Force	force	k1gFnSc2
Updates	Updates	k1gMnSc1
F-15	F-15	k1gMnSc1
Fleet	Fleet	k1gMnSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
Radars	Radars	k1gInSc1
<g/>
,	,	kIx,
Sensors	Sensors	k1gInSc1
<g/>
.	.	kIx.
www.dodbuzz.com	www.dodbuzz.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
The	The	k1gMnSc1
Multi-variant	Multi-variant	k1gMnSc1
<g/>
,	,	kIx,
Multirole	Multirole	k1gFnSc1
5	#num#	k4
<g/>
th	th	k?
Generation	Generation	k1gInSc1
Fighter	fighter	k1gMnSc1
<g/>
↑	↑	k?
Lockheed	Lockheed	k1gMnSc1
secures	secures	k1gMnSc1
$	$	kIx~
<g/>
1.17	1.17	k4
<g/>
bn	bn	k?
contract	contract	k2eAgMnSc1d1
from	from	k1gMnSc1
US	US	kA
DoD	DoD	k1gFnSc1
for	forum	k1gNnPc2
F-35	F-35	k1gFnSc2
programme	programit	k5eAaPmRp1nP
<g/>
↑	↑	k?
MQ-1B	MQ-1B	k1gMnSc1
Predator	Predator	k1gMnSc1
<g/>
↑	↑	k?
MQ	MQ	kA
<g/>
–	–	k?
<g/>
1	#num#	k4
Predator	Predator	k1gInSc1
/	/	kIx~
MQ	MQ	kA
<g/>
–	–	k?
<g/>
9	#num#	k4
Reaper	Reapra	k1gFnPc2
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
United	United	k1gMnSc1
States	States	k1gMnSc1
Army	Arma	k1gFnSc2
Air	Air	k1gMnSc1
Forces	Forces	k1gMnSc1
</s>
<s>
Armádní	armádní	k2eAgInSc1d1
letecký	letecký	k2eAgInSc1d1
sbor	sbor	k1gInSc1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Letectvo	letectvo	k1gNnSc1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
USAF	USAF	kA
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Ozbrojené	ozbrojený	k2eAgFnPc1d1
síly	síla	k1gFnPc1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
</s>
<s>
Armáda	armáda	k1gFnSc1
•	•	k?
Námořnictvo	námořnictvo	k1gNnSc1
•	•	k?
Letectvo	letectvo	k1gNnSc1
•	•	k?
Námořní	námořní	k2eAgFnSc1d1
pěchota	pěchota	k1gFnSc1
•	•	k?
Vesmírné	vesmírný	k2eAgFnPc4d1
síly	síla	k1gFnPc4
•	•	k?
Pobřežní	pobřežní	k1gMnSc1
stráž	stráž	k1gFnSc4
</s>
<s>
Seznam	seznam	k1gInSc1
vzdušných	vzdušný	k2eAgFnPc2d1
sil	síla	k1gFnPc2
Nezávislé	závislý	k2eNgInPc1d1
státy	stát	k1gInPc1
</s>
<s>
Afghánistán	Afghánistán	k1gInSc1
•	•	k?
Albánie	Albánie	k1gFnSc1
•	•	k?
Alžírsko	Alžírsko	k1gNnSc1
•	•	k?
Angola	Angola	k1gFnSc1
•	•	k?
Argentina	Argentina	k1gFnSc1
•	•	k?
Arménie	Arménie	k1gFnSc2
•	•	k?
Austrálie	Austrálie	k1gFnSc2
•	•	k?
Ázerbájdžán	Ázerbájdžán	k1gInSc1
•	•	k?
Bahamy	Bahamy	k1gFnPc4
•	•	k?
Bahrajn	Bahrajn	k1gMnSc1
•	•	k?
Bangladéš	Bangladéš	k1gInSc1
•	•	k?
Belgie	Belgie	k1gFnSc2
•	•	k?
Belize	Belize	k1gFnSc2
•	•	k?
Benin	Benin	k1gMnSc1
•	•	k?
Bělorusko	Bělorusko	k1gNnSc4
•	•	k?
Bolívie	Bolívie	k1gFnSc1
•	•	k?
Bosna	Bosna	k1gFnSc1
a	a	k8xC
Hercegovina	Hercegovina	k1gFnSc1
•	•	k?
Botswana	Botswana	k1gFnSc1
•	•	k?
Brazílie	Brazílie	k1gFnSc2
•	•	k?
Brunej	Brunej	k1gFnSc2
•	•	k?
Bulharsko	Bulharsko	k1gNnSc1
•	•	k?
Burkina	Burkina	k1gMnSc1
Faso	Faso	k1gMnSc1
•	•	k?
Čad	Čad	k1gInSc1
•	•	k?
Černá	černat	k5eAaImIp3nS
Hora	hora	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Česko	Česko	k1gNnSc1
•	•	k?
Čína	Čína	k1gFnSc1
•	•	k?
Dánsko	Dánsko	k1gNnSc1
•	•	k?
DR	dr	kA
Kongo	Kongo	k1gNnSc1
•	•	k?
Dominikánská	dominikánský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
•	•	k?
Džibutsko	Džibutsko	k1gNnSc4
•	•	k?
Egypt	Egypt	k1gInSc1
•	•	k?
Ekvádor	Ekvádor	k1gInSc1
•	•	k?
Eritrea	Eritrea	k1gFnSc1
•	•	k?
Estonsko	Estonsko	k1gNnSc1
•	•	k?
Etiopie	Etiopie	k1gFnSc1
•	•	k?
Filipíny	Filipíny	k1gFnPc4
•	•	k?
Finsko	Finsko	k1gNnSc1
•	•	k?
Francie	Francie	k1gFnSc2
•	•	k?
Ghana	Ghana	k1gFnSc1
•	•	k?
Gruzie	Gruzie	k1gFnSc1
•	•	k?
Guatemala	Guatemala	k1gFnSc1
•	•	k?
Honduras	Honduras	k1gInSc1
•	•	k?
Chile	Chile	k1gNnSc2
•	•	k?
Chorvatsko	Chorvatsko	k1gNnSc1
•	•	k?
Indie	Indie	k1gFnSc2
•	•	k?
Indonésie	Indonésie	k1gFnSc2
•	•	k?
Irák	Irák	k1gInSc1
•	•	k?
Írán	Írán	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Irsko	Irsko	k1gNnSc4
•	•	k?
Island	Island	k1gInSc1
•	•	k?
Itálie	Itálie	k1gFnSc2
•	•	k?
Izrael	Izrael	k1gInSc1
•	•	k?
Jamajka	Jamajka	k1gFnSc1
•	•	k?
Japonsko	Japonsko	k1gNnSc4
•	•	k?
Jemen	Jemen	k1gInSc1
•	•	k?
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
•	•	k?
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
•	•	k?
Jižní	jižní	k2eAgInSc1d1
Súdán	Súdán	k1gInSc1
•	•	k?
Jordánsko	Jordánsko	k1gNnSc1
•	•	k?
Kambodža	Kambodža	k1gFnSc1
•	•	k?
Kamerun	Kamerun	k1gInSc1
•	•	k?
Kanada	Kanada	k1gFnSc1
•	•	k?
Katar	katar	k1gInSc1
•	•	k?
Kazachstán	Kazachstán	k1gInSc1
•	•	k?
Keňa	Keňa	k1gFnSc1
•	•	k?
Kolumbie	Kolumbie	k1gFnSc1
•	•	k?
Kongo	Kongo	k1gNnSc1
•	•	k?
Kuba	Kuba	k1gFnSc1
•	•	k?
Kuvajt	Kuvajt	k1gInSc1
•	•	k?
Kypr	Kypr	k1gInSc1
•	•	k?
Kyrgyzstán	Kyrgyzstán	k1gInSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Laos	Laos	k1gInSc1
•	•	k?
Libanon	Libanon	k1gInSc1
•	•	k?
Libye	Libye	k1gFnSc1
•	•	k?
Litva	Litva	k1gFnSc1
•	•	k?
Lotyšsko	Lotyšsko	k1gNnSc1
•	•	k?
Lucembursko	Lucembursko	k1gNnSc1
•	•	k?
Maďarsko	Maďarsko	k1gNnSc1
•	•	k?
Malajsie	Malajsie	k1gFnSc1
•	•	k?
Malawi	Malawi	k1gNnSc2
•	•	k?
Mali	Mali	k1gNnSc2
•	•	k?
Malta	Malta	k1gFnSc1
•	•	k?
Maroko	Maroko	k1gNnSc1
•	•	k?
Mauritánie	Mauritánie	k1gFnSc1
•	•	k?
Mexiko	Mexiko	k1gNnSc1
•	•	k?
Moldavsko	Moldavsko	k1gNnSc1
•	•	k?
Mongolsko	Mongolsko	k1gNnSc4
•	•	k?
Mosambik	Mosambik	k1gInSc1
•	•	k?
Myanmar	Myanmar	k1gInSc1
•	•	k?
Namibie	Namibie	k1gFnSc2
•	•	k?
Německo	Německo	k1gNnSc4
•	•	k?
Niger	Niger	k1gInSc1
•	•	k?
Nigérie	Nigérie	k1gFnSc1
•	•	k?
Nikaragua	Nikaragua	k1gFnSc1
•	•	k?
Nizozemsko	Nizozemsko	k1gNnSc1
•	•	k?
Norsko	Norsko	k1gNnSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Nový	nový	k2eAgInSc1d1
Zéland	Zéland	k1gInSc1
•	•	k?
Omán	Omán	k1gInSc1
•	•	k?
Pákistán	Pákistán	k1gInSc1
•	•	k?
Paraguay	Paraguay	k1gFnSc1
•	•	k?
Peru	prát	k5eAaImIp1nS
•	•	k?
Polsko	Polsko	k1gNnSc1
•	•	k?
Portugalsko	Portugalsko	k1gNnSc1
•	•	k?
Rakousko	Rakousko	k1gNnSc1
•	•	k?
Rumunsko	Rumunsko	k1gNnSc1
•	•	k?
Rusko	Rusko	k1gNnSc1
•	•	k?
Řecko	Řecko	k1gNnSc1
•	•	k?
Salvador	Salvador	k1gMnSc1
•	•	k?
Saúdská	saúdský	k2eAgFnSc1d1
Arábie	Arábie	k1gFnSc1
•	•	k?
Senegal	Senegal	k1gInSc1
•	•	k?
Severní	severní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
•	•	k?
Severní	severní	k2eAgFnSc1d1
Makedonie	Makedonie	k1gFnSc1
•	•	k?
Singapur	Singapur	k1gInSc1
•	•	k?
Slovensko	Slovensko	k1gNnSc1
•	•	k?
Slovinsko	Slovinsko	k1gNnSc1
•	•	k?
Spojené	spojený	k2eAgInPc4d1
arabské	arabský	k2eAgInPc4d1
emiráty	emirát	k1gInPc4
•	•	k?
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
•	•	k?
Spojené	spojený	k2eAgFnSc2d1
<g />
.	.	kIx.
</s>
<s hack="1">
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
•	•	k?
Srbsko	Srbsko	k1gNnSc4
•	•	k?
Srí	Srí	k1gMnSc1
Lanka	lanko	k1gNnSc2
•	•	k?
Středoafrická	středoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
•	•	k?
Surinam	Surinam	k1gInSc1
•	•	k?
Súdán	Súdán	k1gInSc1
•	•	k?
Sýrie	Sýrie	k1gFnSc1
•	•	k?
Španělsko	Španělsko	k1gNnSc1
•	•	k?
Švédsko	Švédsko	k1gNnSc1
•	•	k?
Švýcarsko	Švýcarsko	k1gNnSc4
•	•	k?
Tádžikistán	Tádžikistán	k1gInSc1
•	•	k?
Tanzanie	Tanzanie	k1gFnSc2
•	•	k?
Thajsko	Thajsko	k1gNnSc1
•	•	k?
Togo	Togo	k1gNnSc1
•	•	k?
Tunisko	Tunisko	k1gNnSc1
•	•	k?
Turecko	Turecko	k1gNnSc4
•	•	k?
Turkmenistán	Turkmenistán	k1gInSc1
•	•	k?
Uganda	Uganda	k1gFnSc1
•	•	k?
Ukrajina	Ukrajina	k1gFnSc1
•	•	k?
Uruguay	Uruguay	k1gFnSc1
•	•	k?
Uzbekistán	Uzbekistán	k1gInSc1
•	•	k?
Venezuela	Venezuela	k1gFnSc1
•	•	k?
Vietnam	Vietnam	k1gInSc1
•	•	k?
Zambie	Zambie	k1gFnSc2
•	•	k?
Zimbabwe	Zimbabwe	k1gFnSc1
Státy	stát	k1gInPc4
s	s	k7c7
omezeným	omezený	k2eAgNnSc7d1
uznáním	uznání	k1gNnSc7
</s>
<s>
Abcházie	Abcházie	k1gFnSc1
•	•	k?
Arcach	Arcach	k1gMnSc1
•	•	k?
Podněstří	Podněstří	k1gMnSc1
•	•	k?
Tchaj-wan	Tchaj-wan	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
kn	kn	k?
<g/>
20030912001	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
872-2	872-2	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0004	#num#	k4
0478	#num#	k4
7146	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
79126811	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
142489267	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
79126811	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Letectvo	letectvo	k1gNnSc1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
</s>
