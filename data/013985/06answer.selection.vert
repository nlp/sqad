<s>
Letectvo	letectvo	k1gNnSc1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
United	United	k1gMnSc1
States	States	k1gMnSc1
Air	Air	k1gMnSc1
Force	force	k1gFnSc1
<g/>
;	;	kIx,
USAF	USAF	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
Ozbrojených	ozbrojený	k2eAgFnPc2d1
sil	síla	k1gFnPc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
a	a	k8xC
jednou	jeden	k4xCgFnSc7
z	z	k7c2
osmi	osm	k4xCc2
amerických	americký	k2eAgFnPc2d1
uniformovaných	uniformovaný	k2eAgFnPc2d1
složek	složka	k1gFnPc2
<g/>
.	.	kIx.
</s>