<s>
Roku	rok	k1gInSc2	rok
1842	[number]	k4	1842
švédský	švédský	k2eAgMnSc1d1	švédský
chemik	chemik	k1gMnSc1	chemik
baron	baron	k1gMnSc1	baron
Jöns	Jönsa	k1gFnPc2	Jönsa
Jacob	Jacoba	k1gFnPc2	Jacoba
Berzelius	Berzelius	k1gInSc1	Berzelius
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
termín	termín	k1gInSc1	termín
halogen	halogen	k1gInSc1	halogen
–	–	k?	–
ἅ	ἅ	k?	ἅ
(	(	kIx(	(
<g/>
háls	háls	k1gInSc1	háls
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sůl	sůl	k1gFnSc1	sůl
a	a	k8xC	a
moře	moře	k1gNnSc1	moře
<g/>
,	,	kIx,	,
a	a	k8xC	a
γ	γ	k?	γ
(	(	kIx(	(
<g/>
gen-	gen-	k?	gen-
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
γ	γ	k?	γ
(	(	kIx(	(
<g/>
gígnomai	gígnoma	k1gFnSc2	gígnoma
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
stávat	stávat	k5eAaImF	stávat
se	se	k3xPyFc4	se
<g/>
"	"	kIx"	"
–	–	k?	–
pro	pro	k7c4	pro
čtyři	čtyři	k4xCgInPc4	čtyři
prvky	prvek	k1gInPc4	prvek
(	(	kIx(	(
<g/>
fluor	fluor	k1gInSc1	fluor
<g/>
,	,	kIx,	,
chlór	chlór	k1gInSc1	chlór
<g/>
,	,	kIx,	,
brom	brom	k1gInSc1	brom
<g/>
,	,	kIx,	,
jód	jód	k1gInSc1	jód
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
společně	společně	k6eAd1	společně
s	s	k7c7	s
kovy	kov	k1gInPc7	kov
mořské	mořský	k2eAgFnSc2d1	mořská
soli	sůl	k1gFnSc2	sůl
<g/>
.	.	kIx.	.
</s>
