<s>
Cat	Cat	k?
Island	Island	k1gInSc1
</s>
<s>
Cat	Cat	k?
Island	Island	k1gInSc1
</s>
<s>
Cat	Cat	k?
Island	Island	k1gInSc1
</s>
<s>
Stát	stát	k1gInSc1
</s>
<s>
Bahamy	Bahamy	k1gFnPc1
Bahamy	Bahamy	k1gFnPc1
Zeměpisné	zeměpisný	k2eAgFnPc4d1
souřadnice	souřadnice	k1gFnPc4
</s>
<s>
24	#num#	k4
<g/>
°	°	k?
<g/>
18	#num#	k4
<g/>
′	′	k?
<g/>
41	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
75	#num#	k4
<g/>
°	°	k?
<g/>
25	#num#	k4
<g/>
′	′	k?
<g/>
23	#num#	k4
<g/>
″	″	k?
z.	z.	k?
d.	d.	k?
Osídlení	osídlení	k1gNnSc2
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Cat	Cat	k?
Island	Island	k1gInSc1
je	být	k5eAaImIp3nS
ostrov	ostrov	k1gInSc4
ležící	ležící	k2eAgInSc4d1
v	v	k7c6
souostroví	souostroví	k1gNnSc6
Bahamy	Bahamy	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
roku	rok	k1gInSc3
2010	#num#	k4
zde	zde	k6eAd1
žilo	žít	k5eAaImAgNnS
1522	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
tomto	tento	k3xDgInSc6
ostrově	ostrov	k1gInSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
dvě	dva	k4xCgNnPc1
letiště	letiště	k1gNnPc1
a	a	k8xC
kopec	kopec	k1gInSc1
Mount	Mounta	k1gInPc1
Alvernia	Alvernium	k1gFnPc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1
je	být	k5eAaImIp3nS
nejvyšším	vysoký	k2eAgInSc7d3
bodem	bod	k1gInSc7
Baham	Bahamy	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
jižní	jižní	k2eAgFnSc6d1
části	část	k1gFnSc6
ostrova	ostrov	k1gInSc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
jezero	jezero	k1gNnSc1
Great	Great	k1gInSc1
Lake	Lake	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
CAT	CAT	kA
ISLAND	Island	k1gInSc1
POPULATION	POPULATION	kA
BY	by	k9
SETTLEMENT	settlement	k1gInSc4
AND	Anda	k1gFnPc2
TOTAL	totat	k5eAaImAgMnS
NUMBER	NUMBER	kA
OF	OF	kA
OCCUPIED	OCCUPIED	kA
DWELLINGS	DWELLINGS	kA
<g/>
:	:	kIx,
2010	#num#	k4
CENSUS	census	k1gInSc1
-	-	kIx~
Bahamas	Bahamas	k1gInSc1
Department	department	k1gInSc1
of	of	k?
Statistics	Statistics	k1gInSc1
<g/>
↑	↑	k?
Anson	Anson	k1gMnSc1
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
F.	F.	kA
(	(	kIx(
<g/>
Peter	Peter	k1gMnSc1
Frederick	Frederick	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Hermit	Hermita	k1gFnPc2
of	of	k?
Cat	Cat	k1gMnSc1
Island	Island	k1gInSc1
:	:	kIx,
the	the	k?
life	lifat	k5eAaPmIp3nS
of	of	k?
Fra	Fra	k1gFnSc1
Jerome	Jerom	k1gInSc5
Hawes	Hawes	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Burns	Burns	k1gInSc1
&	&	k?
Oates	Oates	k1gInSc1
<g/>
,	,	kIx,
1958	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
VIAF	VIAF	kA
<g/>
:	:	kIx,
234223210	#num#	k4
</s>
