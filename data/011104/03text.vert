<p>
<s>
Kumáón	Kumáón	k1gInSc1	Kumáón
(	(	kIx(	(
<g/>
hindsky	hindsky	k6eAd1	hindsky
क	क	k?	क
<g/>
ु	ु	k?	ु
<g/>
म	म	k?	म
<g/>
ा	ा	k?	ा
<g/>
ऊ	ऊ	k?	ऊ
<g/>
ँ	ँ	k?	ँ
<g/>
,	,	kIx,	,
Kumā	Kumā	k1gFnSc1	Kumā
<g/>
̃	̃	k?	̃
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
Kumaon	Kumaon	k1gInSc1	Kumaon
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
historický	historický	k2eAgInSc1d1	historický
region	region	k1gInSc1	region
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
správních	správní	k2eAgFnPc2d1	správní
oblastí	oblast	k1gFnPc2	oblast
indického	indický	k2eAgInSc2d1	indický
státu	stát	k1gInSc2	stát
Uttarákhand	Uttarákhand	k1gInSc1	Uttarákhand
(	(	kIx(	(
<g/>
druhou	druhý	k4xOgFnSc7	druhý
správní	správní	k2eAgFnSc7d1	správní
oblastí	oblast	k1gFnSc7	oblast
je	být	k5eAaImIp3nS	být
Garhvál	Garhvál	k1gInSc1	Garhvál
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
hraničí	hraničit	k5eAaImIp3nP	hraničit
s	s	k7c7	s
Tibetem	Tibet	k1gInSc7	Tibet
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
s	s	k7c7	s
Nepálem	Nepál	k1gInSc7	Nepál
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
s	s	k7c7	s
Uttarpradéšem	Uttarpradéš	k1gInSc7	Uttarpradéš
a	a	k8xC	a
na	na	k7c6	na
západě	západ	k1gInSc6	západ
s	s	k7c7	s
Garhválem	Garhvál	k1gInSc7	Garhvál
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
okresy	okres	k1gInPc4	okres
Almóra	Almór	k1gMnSc2	Almór
<g/>
,	,	kIx,	,
Bagéšvar	Bagéšvar	k1gInSc1	Bagéšvar
<g/>
,	,	kIx,	,
Čampávat	Čampávat	k1gMnSc1	Čampávat
<g/>
,	,	kIx,	,
Nainítál	Nainítál	k1gMnSc1	Nainítál
<g/>
,	,	kIx,	,
Pithaurágarh	Pithaurágarh	k1gMnSc1	Pithaurágarh
a	a	k8xC	a
Udhamsinhnagar	Udhamsinhnagar	k1gMnSc1	Udhamsinhnagar
<g/>
.	.	kIx.	.
</s>
<s>
Správním	správní	k2eAgNnSc7d1	správní
centrem	centrum	k1gNnSc7	centrum
Kumáónské	Kumáónský	k2eAgFnSc2d1	Kumáónský
oblasti	oblast	k1gFnSc2	oblast
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
Nainítál	Nainítála	k1gFnPc2	Nainítála
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
Kumáón	Kumáón	k1gInSc1	Kumáón
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
velký	velký	k2eAgInSc1d1	velký
díl	díl	k1gInSc1	díl
Himálaje	Himálaj	k1gFnSc2	Himálaj
a	a	k8xC	a
dva	dva	k4xCgInPc4	dva
podhorské	podhorský	k2eAgInPc4d1	podhorský
pásy	pás	k1gInPc4	pás
<g/>
,	,	kIx,	,
vyšší	vysoký	k2eAgInSc4d2	vyšší
bhabhar	bhabhar	k1gInSc4	bhabhar
a	a	k8xC	a
nižší	nízký	k2eAgFnSc4d2	nižší
terai	terae	k1gFnSc4	terae
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
pásma	pásmo	k1gNnPc1	pásmo
jsou	být	k5eAaImIp3nP	být
až	až	k9	až
do	do	k7c2	do
1850	[number]	k4	1850
m	m	kA	m
pokryta	pokrýt	k5eAaPmNgFnS	pokrýt
neprostupným	prostupný	k2eNgInSc7d1	neprostupný
lesem	les	k1gInSc7	les
ponechaným	ponechaný	k2eAgInSc7d1	ponechaný
divoké	divoký	k2eAgFnPc4d1	divoká
zvěři	zvěř	k1gFnSc3	zvěř
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc1	zbytek
Kumáónu	Kumáón	k1gInSc2	Kumáón
tvoří	tvořit	k5eAaImIp3nS	tvořit
bludiště	bludiště	k1gNnSc1	bludiště
hor	hora	k1gFnPc2	hora
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
Himálaje	Himálaj	k1gFnSc2	Himálaj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
úseku	úsek	k1gInSc6	úsek
dlouhém	dlouhý	k2eAgInSc6d1	dlouhý
225	[number]	k4	225
km	km	kA	km
a	a	k8xC	a
širokém	široký	k2eAgNnSc6d1	široké
65	[number]	k4	65
km	km	kA	km
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
přes	přes	k7c4	přes
třicet	třicet	k4xCc4	třicet
štítů	štít	k1gInPc2	štít
přesahujících	přesahující	k2eAgInPc2d1	přesahující
5500	[number]	k4	5500
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Řeky	řeka	k1gFnSc2	řeka
jako	jako	k8xC	jako
Gorí	Gor	k1gFnPc2	Gor
<g/>
,	,	kIx,	,
Dhaulí	Dhaule	k1gFnPc2	Dhaule
a	a	k8xC	a
Kálí	kálet	k5eAaImIp3nS	kálet
pramení	pramenit	k5eAaImIp3nS	pramenit
většinou	většinou	k6eAd1	většinou
na	na	k7c6	na
jižních	jižní	k2eAgInPc6d1	jižní
svazích	svah	k1gInPc6	svah
tibetského	tibetský	k2eAgNnSc2d1	tibetské
rozvodí	rozvodí	k1gNnSc2	rozvodí
<g/>
,	,	kIx,	,
severně	severně	k6eAd1	severně
od	od	k7c2	od
kumáónských	kumáónský	k2eAgInPc2d1	kumáónský
hřebenů	hřeben	k1gInPc2	hřeben
<g/>
,	,	kIx,	,
kterými	který	k3yQgFnPc7	který
si	se	k3xPyFc3	se
pak	pak	k6eAd1	pak
prorážejí	prorážet	k5eAaImIp3nP	prorážet
cestu	cesta	k1gFnSc4	cesta
hlubokými	hluboký	k2eAgInPc7d1	hluboký
a	a	k8xC	a
prudce	prudko	k6eAd1	prudko
skloněnými	skloněný	k2eAgInPc7d1	skloněný
kaňony	kaňon	k1gInPc7	kaňon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgInPc4d1	hlavní
zdejší	zdejší	k2eAgInSc4d1	zdejší
stromy	strom	k1gInPc4	strom
patří	patřit	k5eAaImIp3nS	patřit
borovice	borovice	k1gFnSc1	borovice
Roxburghova	Roxburghova	k1gFnSc1	Roxburghova
(	(	kIx(	(
<g/>
Pinus	Pinus	k1gInSc1	Pinus
roxburghii	roxburghie	k1gFnSc4	roxburghie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
cypřiš	cypřiš	k1gInSc1	cypřiš
himálajský	himálajský	k2eAgInSc1d1	himálajský
(	(	kIx(	(
<g/>
Cupressus	Cupressus	k1gInSc1	Cupressus
torulosa	torulosa	k1gFnSc1	torulosa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jedle	jedle	k1gFnSc1	jedle
pindrow	pindrow	k?	pindrow
(	(	kIx(	(
<g/>
Abies	Abies	k1gMnSc1	Abies
pindrow	pindrow	k?	pindrow
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
olše	olše	k1gFnSc1	olše
(	(	kIx(	(
<g/>
Alnus	Alnus	k1gInSc1	Alnus
<g/>
)	)	kIx)	)
a	a	k8xC	a
sal	sal	k?	sal
(	(	kIx(	(
<g/>
Shorea	Shorea	k1gMnSc1	Shorea
robusta	robusta	k1gMnSc1	robusta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
horninovém	horninový	k2eAgInSc6d1	horninový
podkladu	podklad	k1gInSc6	podklad
převažuje	převažovat	k5eAaImIp3nS	převažovat
vápenec	vápenec	k1gInSc1	vápenec
<g/>
,	,	kIx,	,
pískovec	pískovec	k1gInSc1	pískovec
<g/>
,	,	kIx,	,
břidlice	břidlice	k1gFnSc1	břidlice
<g/>
,	,	kIx,	,
rula	rula	k1gFnSc1	rula
a	a	k8xC	a
žula	žula	k1gFnSc1	žula
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
podhorských	podhorský	k2eAgNnPc2d1	podhorské
pásem	pásmo	k1gNnPc2	pásmo
a	a	k8xC	a
hlubokých	hluboký	k2eAgNnPc2d1	hluboké
údolí	údolí	k1gNnPc2	údolí
převládá	převládat	k5eAaImIp3nS	převládat
mírné	mírný	k2eAgNnSc1d1	mírné
podnebí	podnebí	k1gNnSc1	podnebí
<g/>
.	.	kIx.	.
</s>
<s>
Okrajové	okrajový	k2eAgInPc1d1	okrajový
hřebeny	hřeben	k1gInPc1	hřeben
Himálaje	Himálaj	k1gFnSc2	Himálaj
dostávají	dostávat	k5eAaImIp3nP	dostávat
první	první	k4xOgInSc4	první
zásah	zásah	k1gInSc4	zásah
monzunu	monzun	k1gInSc2	monzun
a	a	k8xC	a
srážky	srážka	k1gFnPc4	srážka
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
dvojnásobné	dvojnásobný	k2eAgFnPc1d1	dvojnásobná
oproti	oproti	k7c3	oproti
oblastem	oblast	k1gFnPc3	oblast
hlouběji	hluboko	k6eAd2	hluboko
v	v	k7c6	v
horách	hora	k1gFnPc6	hora
<g/>
,	,	kIx,	,
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
1000	[number]	k4	1000
až	až	k9	až
2000	[number]	k4	2000
mm	mm	kA	mm
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vyšších	vysoký	k2eAgInPc6d2	vyšší
hřebenech	hřeben	k1gInPc6	hřeben
chybí	chybět	k5eAaImIp3nS	chybět
zimní	zimní	k2eAgInPc4d1	zimní
průsmyky	průsmyk	k1gInPc4	průsmyk
bez	bez	k7c2	bez
sněhu	sníh	k1gInSc2	sníh
<g/>
.	.	kIx.	.
</s>
<s>
Mrazy	mráz	k1gInPc1	mráz
bývají	bývat	k5eAaImIp3nP	bývat
kruté	krutý	k2eAgInPc1d1	krutý
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
údolích	údolí	k1gNnPc6	údolí
<g/>
.	.	kIx.	.
</s>
</p>
