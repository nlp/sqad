<s>
Lidská	lidský	k2eAgFnSc1d1	lidská
lebka	lebka	k1gFnSc1	lebka
(	(	kIx(	(
<g/>
cranium	cranium	k1gNnSc1	cranium
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
u	u	k7c2	u
dítěte	dítě	k1gNnSc2	dítě
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
22	[number]	k4	22
kostí	kost	k1gFnPc2	kost
<g/>
.	.	kIx.	.
</s>
<s>
Novorozenec	novorozenec	k1gMnSc1	novorozenec
má	mít	k5eAaImIp3nS	mít
prostor	prostor	k1gInSc4	prostor
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
kostmi	kost	k1gFnPc7	kost
vyplněny	vyplnit	k5eAaPmNgInP	vyplnit
vazivovými	vazivový	k2eAgFnPc7d1	vazivová
blankami	blanka	k1gFnPc7	blanka
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
lupínky	lupínek	k1gInPc1	lupínek
<g/>
,	,	kIx,	,
fontanely	fontanela	k1gFnPc1	fontanela
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc1	ten
postupně	postupně	k6eAd1	postupně
zarůstají	zarůstat	k5eAaImIp3nP	zarůstat
a	a	k8xC	a
celá	celý	k2eAgFnSc1d1	celá
lebka	lebka	k1gFnSc1	lebka
srůstá	srůstat	k5eAaImIp3nS	srůstat
<g/>
.	.	kIx.	.
</s>
<s>
Kosti	kost	k1gFnPc1	kost
lebky	lebka	k1gFnSc2	lebka
jsou	být	k5eAaImIp3nP	být
ploché	plochý	k2eAgFnPc1d1	plochá
<g/>
,	,	kIx,	,
sestávají	sestávat	k5eAaImIp3nP	sestávat
se	se	k3xPyFc4	se
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
tenkých	tenký	k2eAgFnPc2d1	tenká
kostí	kost	k1gFnPc2	kost
a	a	k8xC	a
jedné	jeden	k4xCgFnSc2	jeden
řídké	řídký	k2eAgFnSc2d1	řídká
kosti	kost	k1gFnSc2	kost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
kostní	kostní	k2eAgFnSc1d1	kostní
dřeň	dřeň	k1gFnSc1	dřeň
<g/>
,	,	kIx,	,
jejímu	její	k3xOp3gNnSc3	její
uspořádání	uspořádání	k1gNnSc3	uspořádání
říkáme	říkat	k5eAaImIp1nP	říkat
diploe	diplo	k1gFnPc1	diplo
<g/>
.	.	kIx.	.
</s>
<s>
Diploe	Diploe	k6eAd1	Diploe
najdeme	najít	k5eAaPmIp1nP	najít
jen	jen	k9	jen
v	v	k7c6	v
lebce	lebka	k1gFnSc6	lebka
<g/>
.	.	kIx.	.
</s>
<s>
Mozková	mozkový	k2eAgFnSc1d1	mozková
část	část	k1gFnSc1	část
(	(	kIx(	(
<g/>
neurocranium	neurocranium	k1gNnSc1	neurocranium
<g/>
)	)	kIx)	)
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
pevné	pevný	k2eAgNnSc1d1	pevné
pouzdro	pouzdro	k1gNnSc1	pouzdro
pro	pro	k7c4	pro
mozek	mozek	k1gInSc4	mozek
a	a	k8xC	a
některé	některý	k3yIgInPc4	některý
smyslové	smyslový	k2eAgInPc4d1	smyslový
orgány	orgán	k1gInPc4	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
plochých	plochý	k2eAgFnPc2d1	plochá
kostí	kost	k1gFnPc2	kost
klenby	klenba	k1gFnSc2	klenba
lební	lební	k2eAgFnSc2d1	lební
-	-	kIx~	-
kalva	kalva	k1gFnSc1	kalva
(	(	kIx(	(
<g/>
kost	kost	k1gFnSc1	kost
temenní	temenní	k2eAgFnSc1d1	temenní
a	a	k8xC	a
čelní	čelní	k2eAgFnSc1d1	čelní
<g/>
)	)	kIx)	)
a	a	k8xC	a
z	z	k7c2	z
kostí	kost	k1gFnPc2	kost
spodiny	spodina	k1gFnSc2	spodina
(	(	kIx(	(
<g/>
báze	báze	k1gFnSc1	báze
<g/>
)	)	kIx)	)
lební	lební	k2eAgFnSc1d1	lební
(	(	kIx(	(
<g/>
kost	kost	k1gFnSc1	kost
týlní	týlní	k2eAgFnSc1d1	týlní
<g/>
,	,	kIx,	,
klínová	klínový	k2eAgFnSc1d1	klínová
<g/>
,	,	kIx,	,
spánková	spánkový	k2eAgFnSc1d1	spánková
a	a	k8xC	a
čichová	čichový	k2eAgFnSc1d1	čichová
<g/>
)	)	kIx)	)
1	[number]	k4	1
kost	kost	k1gFnSc4	kost
týlní	týlní	k2eAgFnSc2d1	týlní
(	(	kIx(	(
<g/>
os	osa	k1gFnPc2	osa
occipitale	occipital	k1gMnSc5	occipital
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Tvoří	tvořit	k5eAaImIp3nP	tvořit
významnou	významný	k2eAgFnSc4d1	významná
část	část	k1gFnSc4	část
spodiny	spodina	k1gFnSc2	spodina
lebeční	lebeční	k2eAgFnSc2d1	lebeční
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
velký	velký	k2eAgInSc1d1	velký
týlní	týlní	k2eAgInSc4d1	týlní
otvor	otvor	k1gInSc4	otvor
(	(	kIx(	(
<g/>
foramen	foramen	k1gInSc1	foramen
occipitale	occipital	k1gMnSc5	occipital
magnum	magnum	k1gInSc1	magnum
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
spojuje	spojovat	k5eAaImIp3nS	spojovat
lebeční	lebeční	k2eAgFnSc4d1	lebeční
dutinu	dutina	k1gFnSc4	dutina
s	s	k7c7	s
páteřním	páteřní	k2eAgInSc7d1	páteřní
kanálem	kanál	k1gInSc7	kanál
(	(	kIx(	(
<g/>
prochází	procházet	k5eAaImIp3nS	procházet
zde	zde	k6eAd1	zde
prodloužená	prodloužený	k2eAgFnSc1d1	prodloužená
mícha	mícha	k1gFnSc1	mícha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Samotné	samotný	k2eAgNnSc1d1	samotné
skloubení	skloubení	k1gNnSc1	skloubení
(	(	kIx(	(
<g/>
atlantooccipitální	atlantooccipitální	k2eAgMnSc1d1	atlantooccipitální
<g/>
)	)	kIx)	)
zajistí	zajistit	k5eAaPmIp3nP	zajistit
dva	dva	k4xCgInPc1	dva
kondyly	kondyl	k1gInPc1	kondyl
(	(	kIx(	(
<g/>
condili	condit	k5eAaImAgMnP	condit
occipitales	occipitales	k1gInSc4	occipitales
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
kterými	který	k3yQgFnPc7	který
skrz	skrz	k6eAd1	skrz
canalis	canalis	k1gInSc1	canalis
nervi	rvát	k5eNaImRp2nS	rvát
hypoglossi	hypoglosse	k1gFnSc6	hypoglosse
prochází	procházet	k5eAaImIp3nS	procházet
XII	XII	kA	XII
hlavový	hlavový	k2eAgInSc1d1	hlavový
nerv	nerv	k1gInSc1	nerv
<g/>
.	.	kIx.	.
</s>
<s>
Dobře	dobře	k6eAd1	dobře
hmatný	hmatný	k2eAgMnSc1d1	hmatný
je	být	k5eAaImIp3nS	být
tzv	tzv	kA	tzv
<g/>
:	:	kIx,	:
zevní	zevní	k2eAgInSc1d1	zevní
týlní	týlní	k2eAgInSc1d1	týlní
výběžek	výběžek	k1gInSc1	výběžek
(	(	kIx(	(
<g/>
protuberantia	protuberantia	k1gFnSc1	protuberantia
occipitalis	occipitalis	k1gFnSc1	occipitalis
externa	externa	k1gFnSc1	externa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
bývá	bývat	k5eAaImIp3nS	bývat
u	u	k7c2	u
mužských	mužský	k2eAgFnPc2d1	mužská
lebek	lebka	k1gFnPc2	lebka
výraznější	výrazný	k2eAgFnPc1d2	výraznější
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
a	a	k8xC	a
na	na	k7c6	na
přilehlé	přilehlý	k2eAgFnSc6d1	přilehlá
čáře	čára	k1gFnSc6	čára
(	(	kIx(	(
<g/>
linea	linea	k1gFnSc1	linea
nuchalis	nuchalis	k1gInSc1	nuchalis
superior	superior	k1gMnSc1	superior
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
upíná	upínat	k5eAaImIp3nS	upínat
kápový	kápový	k2eAgInSc1d1	kápový
sval	sval	k1gInSc1	sval
(	(	kIx(	(
<g/>
m.	m.	k?	m.
<g/>
trapesius	trapesius	k1gInSc1	trapesius
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
1	[number]	k4	1
kost	kost	k1gFnSc4	kost
klínová	klínový	k2eAgFnSc1d1	klínová
(	(	kIx(	(
<g/>
os	osa	k1gFnPc2	osa
sphenoidale	sphenoidale	k6eAd1	sphenoidale
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
je	být	k5eAaImIp3nS	být
nepárová	párový	k2eNgFnSc1d1	nepárová
kost	kost	k1gFnSc1	kost
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
báze	báze	k1gFnSc2	báze
lebeční	lebeční	k2eAgFnSc2d1	lebeční
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vnitřní	vnitřní	k2eAgFnSc6d1	vnitřní
lebeční	lebeční	k2eAgFnSc6d1	lebeční
bázi	báze	k1gFnSc6	báze
(	(	kIx(	(
<g/>
basis	basis	k?	basis
cranii	cranie	k1gFnSc6	cranie
interna	interna	k1gFnSc1	interna
<g/>
)	)	kIx)	)
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
její	její	k3xOp3gNnSc1	její
tělo	tělo	k1gNnSc1	tělo
tzv	tzv	kA	tzv
<g/>
:	:	kIx,	:
turecké	turecký	k2eAgNnSc1d1	turecké
sedlo	sedlo	k1gNnSc1	sedlo
(	(	kIx(	(
<g/>
sella	sella	k1gMnSc1	sella
turcica	turcica	k1gMnSc1	turcica
<g/>
)	)	kIx)	)
v	v	k7c6	v
kterém	který	k3yIgInSc6	který
je	být	k5eAaImIp3nS	být
umístěn	umístěn	k2eAgInSc1d1	umístěn
Podvěsek	podvěsek	k1gInSc1	podvěsek
mozkový	mozkový	k2eAgInSc1d1	mozkový
(	(	kIx(	(
<g/>
hypophisis	hypophisis	k1gInSc1	hypophisis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kost	kost	k1gFnSc1	kost
klínová	klínový	k2eAgFnSc1d1	klínová
má	mít	k5eAaImIp3nS	mít
navíc	navíc	k6eAd1	navíc
tzv.	tzv.	kA	tzv.
malá	malý	k2eAgNnPc4d1	malé
a	a	k8xC	a
velká	velký	k2eAgNnPc4d1	velké
křídla	křídlo	k1gNnPc4	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Malá	malý	k2eAgNnPc4d1	malé
křídla	křídlo	k1gNnPc4	křídlo
pak	pak	k6eAd1	pak
společně	společně	k6eAd1	společně
s	s	k7c7	s
tělem	tělo	k1gNnSc7	tělo
kosti	kost	k1gFnSc2	kost
klínové	klínový	k2eAgFnSc2d1	klínová
uzavírají	uzavírat	k5eAaImIp3nP	uzavírat
zrakový	zrakový	k2eAgInSc4d1	zrakový
kanál	kanál	k1gInSc4	kanál
(	(	kIx(	(
<g/>
canalis	canalis	k1gFnSc1	canalis
opticus	opticus	k1gMnSc1	opticus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
skrz	skrz	k7c4	skrz
který	který	k3yIgInSc4	který
prochází	procházet	k5eAaImIp3nS	procházet
zrakový	zrakový	k2eAgInSc1d1	zrakový
nerv	nerv	k1gInSc1	nerv
(	(	kIx(	(
<g/>
nervus	nervus	k1gMnSc1	nervus
opticus	opticus	k1gMnSc1	opticus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
1	[number]	k4	1
kost	kost	k1gFnSc4	kost
čichová	čichový	k2eAgFnSc1d1	čichová
(	(	kIx(	(
<g/>
os	osa	k1gFnPc2	osa
ethmoidale	ethmoidale	k6eAd1	ethmoidale
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Do	do	k7c2	do
jedné	jeden	k4xCgFnSc2	jeden
její	její	k3xOp3gFnSc2	její
části	část	k1gFnSc2	část
-	-	kIx~	-
lamina	lamin	k2eAgFnSc1d1	lamina
cribrosa	cribrosa	k1gFnSc1	cribrosa
-	-	kIx~	-
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
čichový	čichový	k2eAgInSc1d1	čichový
nerv	nerv	k1gInSc1	nerv
n.	n.	k?	n.
<g/>
olfactorius	olfactorius	k1gInSc1	olfactorius
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
sem	sem	k6eAd1	sem
vysílá	vysílat	k5eAaImIp3nS	vysílat
velice	velice	k6eAd1	velice
jemná	jemný	k2eAgFnSc1d1	jemná
vlákna	vlákna	k1gFnSc1	vlákna
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
při	při	k7c6	při
nárazu	náraz	k1gInSc6	náraz
na	na	k7c4	na
týlní	týlní	k2eAgFnSc4d1	týlní
krajinu	krajina	k1gFnSc4	krajina
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
většinou	většina	k1gFnSc7	většina
doprovázen	doprovázet	k5eAaImNgInS	doprovázet
prudkým	prudký	k2eAgInSc7d1	prudký
pohybem	pohyb	k1gInSc7	pohyb
mozku	mozek	k1gInSc2	mozek
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
i	i	k9	i
k	k	k7c3	k
přetrhání	přetrhání	k1gNnSc3	přetrhání
těchto	tento	k3xDgNnPc2	tento
vláken	vlákno	k1gNnPc2	vlákno
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
i	i	k9	i
k	k	k7c3	k
částečné	částečný	k2eAgFnSc3d1	částečná
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
úplné	úplný	k2eAgFnSc3d1	úplná
ztřátě	ztřáta	k1gFnSc3	ztřáta
čichu	čich	k1gInSc2	čich
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
skrz	skrz	k6eAd1	skrz
lamina	lamin	k2eAgFnSc1d1	lamina
cribrosa	cribrosa	k1gFnSc1	cribrosa
je	být	k5eAaImIp3nS	být
propojen	propojen	k2eAgInSc4d1	propojen
mozek	mozek	k1gInSc4	mozek
s	s	k7c7	s
nosní	nosní	k2eAgFnSc7d1	nosní
dutinou	dutina	k1gFnSc7	dutina
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
možná	možná	k9	možná
cesta	cesta	k1gFnSc1	cesta
pro	pro	k7c4	pro
prostup	prostup	k1gInSc4	prostup
zánětu	zánět	k1gInSc2	zánět
<g/>
.	.	kIx.	.
1	[number]	k4	1
kost	kost	k1gFnSc4	kost
čelní	čelní	k2eAgFnSc4d1	čelní
(	(	kIx(	(
<g/>
os	osa	k1gFnPc2	osa
frontale	frontal	k1gMnSc5	frontal
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
je	být	k5eAaImIp3nS	být
kostěným	kostěný	k2eAgInSc7d1	kostěný
podkladem	podklad	k1gInSc7	podklad
čela	čelo	k1gNnSc2	čelo
a	a	k8xC	a
spodní	spodní	k2eAgFnSc1d1	spodní
část	část	k1gFnSc1	část
kosti	kost	k1gFnSc2	kost
tvoří	tvořit	k5eAaImIp3nS	tvořit
strop	strop	k1gInSc1	strop
obou	dva	k4xCgFnPc2	dva
očnic	očnice	k1gFnPc2	očnice
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pneumatizovaná	pneumatizovaný	k2eAgFnSc1d1	pneumatizovaná
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
otvor	otvor	k1gInSc1	otvor
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
sinus	sinus	k1gInSc1	sinus
frontalis	frontalis	k1gFnSc2	frontalis
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
ze	z	k7c2	z
součástí	součást	k1gFnPc2	součást
vedlejších	vedlejší	k2eAgFnPc2d1	vedlejší
nosních	nosní	k2eAgFnPc2d1	nosní
dutin	dutina	k1gFnPc2	dutina
tzn.	tzn.	kA	tzn.
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
sem	sem	k6eAd1	sem
může	moct	k5eAaImIp3nS	moct
pestoupit	pestoupit	k5eAaPmF	pestoupit
zánět	zánět	k1gInSc4	zánět
z	z	k7c2	z
nosní	nosní	k2eAgFnSc2d1	nosní
dutiny	dutina	k1gFnSc2	dutina
(	(	kIx(	(
<g/>
cavitas	cavitasit	k5eAaPmRp2nS	cavitasit
nasi	nas	k1gFnSc2	nas
<g/>
)	)	kIx)	)
a	a	k8xC	a
indukovat	indukovat	k5eAaBmF	indukovat
vznik	vznik	k1gInSc4	vznik
sinusitis	sinusitis	k1gFnSc1	sinusitis
<g/>
.	.	kIx.	.
2	[number]	k4	2
kosti	kost	k1gFnSc2	kost
temenní	temenní	k2eAgFnSc2d1	temenní
(	(	kIx(	(
<g/>
os	osa	k1gFnPc2	osa
parietale	parietal	k1gMnSc5	parietal
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
párová	párový	k2eAgFnSc1d1	párová
kost	kost	k1gFnSc1	kost
tvaru	tvar	k1gInSc2	tvar
čtyřhranné	čtyřhranný	k2eAgFnSc2d1	čtyřhranná
misky	miska	k1gFnSc2	miska
<g/>
,	,	kIx,	,
opatřena	opatřit	k5eAaPmNgFnS	opatřit
liniemi	linie	k1gFnPc7	linie
úponu	úpon	k1gInSc3	úpon
pro	pro	k7c4	pro
jeden	jeden	k4xCgInSc4	jeden
ze	z	k7c2	z
žvýkacích	žvýkací	k2eAgInPc2d1	žvýkací
svalů	sval	k1gInPc2	sval
musculus	musculus	k1gInSc4	musculus
temporalis	temporalis	k1gFnSc2	temporalis
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
linea	linea	k1gFnSc1	linea
temporalis	temporalis	k1gFnSc2	temporalis
superior	superior	k1gMnSc1	superior
(	(	kIx(	(
<g/>
vrchní	vrchní	k2eAgMnSc1d1	vrchní
<g/>
)	)	kIx)	)
a	a	k8xC	a
linea	linea	k1gFnSc1	linea
temporalis	temporalis	k1gFnSc2	temporalis
inferior	inferiora	k1gFnPc2	inferiora
(	(	kIx(	(
<g/>
spodní	spodní	k2eAgFnPc1d1	spodní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Okraje	okraj	k1gInPc1	okraj
se	se	k3xPyFc4	se
spojují	spojovat	k5eAaImIp3nP	spojovat
s	s	k7c7	s
okolními	okolní	k2eAgFnPc7d1	okolní
kostmi	kost	k1gFnPc7	kost
v	v	k7c6	v
hlavních	hlavní	k2eAgInPc6d1	hlavní
lebečních	lebeční	k2eAgInPc6d1	lebeční
švech	šev	k1gInPc6	šev
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
kostí	kost	k1gFnSc7	kost
čelní	čelní	k2eAgFnPc4d1	čelní
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
šev	šev	k1gInSc1	šev
korunový	korunový	k2eAgInSc1d1	korunový
(	(	kIx(	(
<g/>
sutura	sutura	k1gFnSc1	sutura
coronalis	coronalis	k1gFnSc2	coronalis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
kostí	kost	k1gFnSc7	kost
týlní	týlní	k2eAgInSc1d1	týlní
šev	šev	k1gInSc1	šev
lambdový	lambdový	k2eAgInSc1d1	lambdový
(	(	kIx(	(
<g/>
sutura	sutura	k1gFnSc1	sutura
lambdoidea	lambdoidea	k1gMnSc1	lambdoidea
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
kosti	kost	k1gFnPc1	kost
temenní	temenní	k2eAgFnPc1d1	temenní
se	se	k3xPyFc4	se
spojují	spojovat	k5eAaImIp3nP	spojovat
švem	šev	k1gInSc7	šev
šípovým	šípový	k2eAgFnPc3d1	šípová
(	(	kIx(	(
<g/>
sutura	sutura	k1gFnSc1	sutura
sagittalis	sagittalis	k1gFnSc1	sagittalis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
2	[number]	k4	2
kosti	kost	k1gFnSc2	kost
spánkové	spánkový	k2eAgFnSc2d1	spánková
(	(	kIx(	(
<g/>
os	osa	k1gFnPc2	osa
temporale	temporale	k1gNnSc2	temporale
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
je	být	k5eAaImIp3nS	být
párová	párový	k2eAgFnSc1d1	párová
kost	kost	k1gFnSc1	kost
složitého	složitý	k2eAgInSc2d1	složitý
tvaru	tvar	k1gInSc2	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
kosti	kost	k1gFnSc2	kost
spánkové	spánkový	k2eAgFnSc2d1	spánková
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
kost	kost	k1gFnSc1	kost
skalní	skalní	k2eAgFnSc1d1	skalní
/	/	kIx~	/
pyramida	pyramida	k1gFnSc1	pyramida
(	(	kIx(	(
<g/>
os	osa	k1gFnPc2	osa
petrosum	petrosum	k1gInSc1	petrosum
<g/>
/	/	kIx~	/
<g/>
pars	pars	k1gInSc1	pars
pyramidalis	pyramidalis	k1gFnSc2	pyramidalis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výběžek	výběžek	k1gInSc1	výběžek
bradavkový	bradavkový	k2eAgInSc1d1	bradavkový
(	(	kIx(	(
<g/>
procesus	procesus	k1gInSc1	procesus
mastoideus	mastoideus	k1gInSc1	mastoideus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výběžek	výběžek	k1gInSc1	výběžek
bodcovitý	bodcovitý	k2eAgInSc1d1	bodcovitý
(	(	kIx(	(
<g/>
proc	proc	k1gMnSc1	proc
<g/>
.	.	kIx.	.
styloideus	styloideus	k1gMnSc1	styloideus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kost	kost	k1gFnSc1	kost
bubínková	bubínkový	k2eAgFnSc1d1	bubínková
(	(	kIx(	(
<g/>
pars	parsit	k5eAaPmRp2nS	parsit
tympanica	tympanica	k1gMnSc1	tympanica
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kloubní	kloubní	k2eAgFnSc1d1	kloubní
jamka	jamka	k1gFnSc1	jamka
mandibuly	mandibula	k1gFnSc2	mandibula
a	a	k8xC	a
výběžek	výběžek	k1gInSc1	výběžek
lícní	lícní	k2eAgInSc1d1	lícní
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k7c2	uvnitř
pars	parsa	k1gFnPc2	parsa
pyramidalis	pyramidalis	k1gFnSc2	pyramidalis
je	být	k5eAaImIp3nS	být
uloženo	uložit	k5eAaPmNgNnS	uložit
střední	střední	k2eAgNnSc1d1	střední
ucho	ucho	k1gNnSc1	ucho
<g/>
.	.	kIx.	.
</s>
<s>
Kosti	kost	k1gFnPc1	kost
obličejové	obličejový	k2eAgFnSc2d1	obličejová
části	část	k1gFnSc2	část
(	(	kIx(	(
<g/>
splanchnocranium	splanchnocranium	k1gNnSc1	splanchnocranium
<g/>
,	,	kIx,	,
viscerocranium	viscerocranium	k1gNnSc1	viscerocranium
<g/>
)	)	kIx)	)
obklopují	obklopovat	k5eAaImIp3nP	obklopovat
začátek	začátek	k1gInSc4	začátek
trávicího	trávicí	k2eAgNnSc2d1	trávicí
a	a	k8xC	a
dýchacího	dýchací	k2eAgNnSc2d1	dýchací
ústrojí	ústrojí	k1gNnSc2	ústrojí
<g/>
.	.	kIx.	.
</s>
<s>
Obličejová	obličejový	k2eAgFnSc1d1	obličejová
část	část	k1gFnSc1	část
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
kostí	kost	k1gFnPc2	kost
párových	párový	k2eAgFnPc2d1	párová
(	(	kIx(	(
<g/>
horní	horní	k2eAgFnSc1d1	horní
čelist	čelist	k1gFnSc1	čelist
<g/>
,	,	kIx,	,
kost	kost	k1gFnSc1	kost
patrová	patrový	k2eAgFnSc1d1	patrová
a	a	k8xC	a
lícní	lícní	k2eAgFnSc1d1	lícní
<g/>
)	)	kIx)	)
a	a	k8xC	a
z	z	k7c2	z
nepárové	párový	k2eNgFnSc2d1	nepárová
dolní	dolní	k2eAgFnSc2d1	dolní
čelisti	čelist	k1gFnSc2	čelist
a	a	k8xC	a
jazylky	jazylka	k1gFnSc2	jazylka
<g/>
.	.	kIx.	.
2	[number]	k4	2
kosti	kost	k1gFnPc4	kost
horní	horní	k2eAgFnSc2d1	horní
čelisti	čelist	k1gFnSc2	čelist
(	(	kIx(	(
<g/>
maxilla	maxilla	k6eAd1	maxilla
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Horní	horní	k2eAgFnSc1d1	horní
čelist	čelist	k1gFnSc1	čelist
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
frontálního	frontální	k2eAgInSc2d1	frontální
výběžku	výběžek	k1gInSc2	výběžek
a	a	k8xC	a
výběžku	výběžek	k1gInSc2	výběžek
lícního	lícní	k2eAgInSc2d1	lícní
(	(	kIx(	(
<g/>
proc	proc	k1gMnSc1	proc
<g/>
.	.	kIx.	.
zygomaticus	zygomaticus	k1gMnSc1	zygomaticus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
spojuje	spojovat	k5eAaImIp3nS	spojovat
s	s	k7c7	s
kostí	kost	k1gFnSc7	kost
lícní	lícní	k2eAgFnSc7d1	lícní
(	(	kIx(	(
<g/>
os	osa	k1gFnPc2	osa
zygomaticum	zygomaticum	k1gInSc1	zygomaticum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vývoje	vývoj	k1gInSc2	vývoj
se	se	k3xPyFc4	se
maxila	maxila	k1gFnSc1	maxila
zakládá	zakládat	k5eAaImIp3nS	zakládat
jako	jako	k9	jako
dvě	dva	k4xCgFnPc4	dva
kosti	kost	k1gFnPc4	kost
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
přednější	přední	k2eAgNnPc1d2	přednější
a	a	k8xC	a
menší	malý	k2eAgNnPc1d2	menší
(	(	kIx(	(
<g/>
premaxilla	premaxillo	k1gNnPc1	premaxillo
<g/>
)	)	kIx)	)
postnatálně	postnatálně	k6eAd1	postnatálně
srůstá	srůstat	k5eAaImIp3nS	srůstat
s	s	k7c7	s
ostatním	ostatní	k2eAgNnSc7d1	ostatní
tělem	tělo	k1gNnSc7	tělo
maxily	maxila	k1gFnSc2	maxila
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
maxila	maxila	k1gFnSc1	maxila
je	být	k5eAaImIp3nS	být
pneumatizovaná	pneumatizovaný	k2eAgFnSc1d1	pneumatizovaná
a	a	k8xC	a
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
dutina	dutina	k1gFnSc1	dutina
sinus	sinus	k1gInSc4	sinus
maxilaris	maxilaris	k1gInSc1	maxilaris
(	(	kIx(	(
<g/>
antrum	antrum	k1gInSc1	antrum
Highmori	Highmor	k1gFnSc2	Highmor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc1d3	veliký
z	z	k7c2	z
vedlejších	vedlejší	k2eAgFnPc2d1	vedlejší
dutin	dutina	k1gFnPc2	dutina
nosních	nosní	k2eAgFnPc2d1	nosní
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
podkladem	podklad	k1gInSc7	podklad
tvrdého	tvrdý	k2eAgNnSc2d1	tvrdé
patra	patro	k1gNnSc2	patro
je	být	k5eAaImIp3nS	být
patrový	patrový	k2eAgInSc1d1	patrový
výběžek	výběžek	k1gInSc1	výběžek
<g/>
.	.	kIx.	.
</s>
<s>
Lůžka	lůžko	k1gNnPc4	lůžko
pro	pro	k7c4	pro
zuby	zub	k1gInPc4	zub
horního	horní	k2eAgInSc2d1	horní
zubního	zubní	k2eAgInSc2d1	zubní
oblouku	oblouk	k1gInSc2	oblouk
jsou	být	k5eAaImIp3nP	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
v	v	k7c6	v
podkovitém	podkovitý	k2eAgInSc6d1	podkovitý
dásňovitém	dásňovitý	k2eAgInSc6d1	dásňovitý
výběžku	výběžek	k1gInSc6	výběžek
2	[number]	k4	2
lícní	lícní	k2eAgFnSc6d1	lícní
kosti	kost	k1gFnSc6	kost
(	(	kIx(	(
<g/>
os	osa	k1gFnPc2	osa
zygomaticum	zygomaticum	k1gInSc1	zygomaticum
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Název	název	k1gInSc1	název
z	z	k7c2	z
řeckého	řecký	k2eAgMnSc2d1	řecký
<g/>
:	:	kIx,	:
zygein	zygein	k1gInSc1	zygein
=	=	kIx~	=
spojovat	spojovat	k5eAaImF	spojovat
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
proc	proc	k1gMnSc1	proc
<g/>
.	.	kIx.	.
zygomaticus	zygomaticus	k1gMnSc1	zygomaticus
kosti	kost	k1gFnSc2	kost
spánkové	spánkový	k2eAgFnSc2d1	spánková
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
tzv.	tzv.	kA	tzv.
jařmový	jařmový	k2eAgInSc4d1	jařmový
oblouk	oblouk	k1gInSc4	oblouk
(	(	kIx(	(
<g/>
pons	ponsit	k5eAaPmRp2nS	ponsit
zygomaticus	zygomaticus	k1gInSc1	zygomaticus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jednou	k6eAd1	jednou
svojí	svůj	k3xOyFgFnSc7	svůj
plochou	plocha	k1gFnSc7	plocha
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
část	část	k1gFnSc1	část
stěny	stěna	k1gFnSc2	stěna
orbity	orbita	k1gFnSc2	orbita
(	(	kIx(	(
<g/>
facies	facies	k1gFnSc1	facies
orbitalis	orbitalis	k1gFnSc2	orbitalis
<g/>
)	)	kIx)	)
a	a	k8xC	a
jednou	jeden	k4xCgFnSc7	jeden
(	(	kIx(	(
<g/>
facies	facies	k1gFnSc4	facies
lateralis	lateralis	k1gFnSc2	lateralis
<g/>
)	)	kIx)	)
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
líc	líc	k1gFnSc1	líc
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
její	její	k3xOp3gInSc1	její
tvar	tvar	k1gInSc1	tvar
i	i	k8xC	i
velikost	velikost	k1gFnSc1	velikost
je	být	k5eAaImIp3nS	být
individuální	individuální	k2eAgFnSc1d1	individuální
a	a	k8xC	a
podílí	podílet	k5eAaImIp3nS	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
celkovém	celkový	k2eAgInSc6d1	celkový
výrazu	výraz	k1gInSc6	výraz
tváře	tvář	k1gFnSc2	tvář
<g/>
.	.	kIx.	.
2	[number]	k4	2
nosní	nosní	k2eAgFnSc2d1	nosní
kosti	kost	k1gFnSc2	kost
(	(	kIx(	(
<g/>
os	osa	k1gFnPc2	osa
nasale	nasale	k6eAd1	nasale
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
představují	představovat	k5eAaImIp3nP	představovat
dvě	dva	k4xCgFnPc4	dva
stříškovitě	stříškovitě	k6eAd1	stříškovitě
k	k	k7c3	k
sobě	se	k3xPyFc3	se
postavené	postavený	k2eAgFnPc4d1	postavená
kůstky	kůstka	k1gFnPc4	kůstka
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc4	jejichž
velikost	velikost	k1gFnSc1	velikost
je	být	k5eAaImIp3nS	být
individuální	individuální	k2eAgFnSc1d1	individuální
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
kostěný	kostěný	k2eAgInSc4d1	kostěný
podklad	podklad	k1gInSc4	podklad
nosu	nos	k1gInSc2	nos
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
mužů	muž	k1gMnPc2	muž
bývá	bývat	k5eAaImIp3nS	bývat
zahloubení	zahloubení	k1gNnSc1	zahloubení
"	"	kIx"	"
<g/>
žlábku	žlábek	k1gInSc2	žlábek
<g/>
"	"	kIx"	"
mezi	mezi	k7c7	mezi
os	osa	k1gFnPc2	osa
frontale	frontal	k1gMnSc5	frontal
a	a	k8xC	a
nasale	nasala	k1gFnSc3	nasala
větší	veliký	k2eAgFnPc1d2	veliký
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
pohlavních	pohlavní	k2eAgInPc2d1	pohlavní
rozdílů	rozdíl	k1gInPc2	rozdíl
na	na	k7c6	na
lebce	lebka	k1gFnSc6	lebka
<g/>
.	.	kIx.	.
2	[number]	k4	2
slzní	slzný	k2eAgMnPc1d1	slzný
kosti	kost	k1gFnSc6	kost
(	(	kIx(	(
<g/>
os	osa	k1gFnPc2	osa
lacrimale	lacrimale	k6eAd1	lacrimale
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Párová	párový	k2eAgFnSc1d1	párová
kost	kost	k1gFnSc4	kost
obsahující	obsahující	k2eAgInSc4d1	obsahující
otisk	otisk	k1gInSc4	otisk
slzního	slzní	k2eAgInSc2d1	slzní
váčku	váček	k1gInSc2	váček
(	(	kIx(	(
<g/>
fossa	fossa	k1gFnSc1	fossa
saci	sac	k1gFnSc2	sac
lacrimalis	lacrimalis	k1gFnSc2	lacrimalis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nosní	nosní	k2eAgFnPc1d1	nosní
skořepy	skořepa	k1gFnPc1	skořepa
<g/>
:	:	kIx,	:
2	[number]	k4	2
horní	horní	k2eAgFnPc1d1	horní
skořepy	skořepa	k1gFnPc1	skořepa
nosní	nosní	k2eAgFnPc1d1	nosní
(	(	kIx(	(
<g/>
Concha	Concha	k1gMnSc1	Concha
nasalis	nasalis	k1gFnSc2	nasalis
superior	superior	k1gMnSc1	superior
<g/>
)	)	kIx)	)
2	[number]	k4	2
střední	střední	k2eAgFnSc2d1	střední
skořepy	skořepa	k1gFnSc2	skořepa
nosní	nosní	k2eAgFnSc1d1	nosní
(	(	kIx(	(
<g/>
Concha	Concha	k1gFnSc1	Concha
nasalis	nasalis	k1gFnSc2	nasalis
media	medium	k1gNnSc2	medium
<g/>
)	)	kIx)	)
2	[number]	k4	2
dolní	dolní	k2eAgFnSc2d1	dolní
skořepy	skořepa	k1gFnSc2	skořepa
nosní	nosní	k2eAgMnSc1d1	nosní
(	(	kIx(	(
<g/>
Concha	Concha	k1gMnSc1	Concha
nasalis	nasalis	k1gFnSc2	nasalis
inferior	inferior	k1gMnSc1	inferior
<g/>
)	)	kIx)	)
-	-	kIx~	-
tenká	tenký	k2eAgFnSc1d1	tenká
plochá	plochý	k2eAgFnSc1d1	plochá
kost	kost	k1gFnSc1	kost
odstupující	odstupující	k2eAgFnSc1d1	odstupující
od	od	k7c2	od
zevní	zevní	k2eAgFnSc2d1	zevní
stěny	stěna	k1gFnSc2	stěna
dutiny	dutina	k1gFnSc2	dutina
nosní	nosní	k2eAgFnPc4d1	nosní
pod	pod	k7c7	pod
střední	střední	k2eAgFnSc7d1	střední
skořepou	skořepa	k1gFnSc7	skořepa
<g/>
.	.	kIx.	.
2	[number]	k4	2
patrové	patrový	k2eAgFnSc6d1	patrová
kosti	kost	k1gFnSc6	kost
(	(	kIx(	(
<g/>
os	osa	k1gFnPc2	osa
palatinum	palatinum	k?	palatinum
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc1	dva
lamely	lamela	k1gFnPc1	lamela
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
písmene	písmeno	k1gNnSc2	písmeno
L	L	kA	L
(	(	kIx(	(
<g/>
jsou	být	k5eAaImIp3nP	být
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
prakticky	prakticky	k6eAd1	prakticky
kolmé	kolmý	k2eAgFnPc1d1	kolmá
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
(	(	kIx(	(
<g/>
lamina	lamin	k2eAgFnSc1d1	lamina
perpendicularis	perpendicularis	k1gFnSc1	perpendicularis
<g/>
)	)	kIx)	)
tvoří	tvořit	k5eAaImIp3nS	tvořit
část	část	k1gFnSc1	část
dutiny	dutina	k1gFnSc2	dutina
nosní	nosní	k2eAgFnSc1d1	nosní
(	(	kIx(	(
<g/>
cavitas	cavitasit	k5eAaPmRp2nS	cavitasit
nasi	nas	k1gFnSc2	nas
<g/>
)	)	kIx)	)
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
(	(	kIx(	(
<g/>
lamina	lamin	k2eAgFnSc1d1	lamina
horisontalis	horisontalis	k1gFnSc1	horisontalis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
tvrdého	tvrdý	k2eAgNnSc2d1	tvrdé
patra	patro	k1gNnSc2	patro
<g/>
.	.	kIx.	.
dolní	dolní	k2eAgFnSc1d1	dolní
čelist	čelist	k1gFnSc1	čelist
(	(	kIx(	(
<g/>
Mandibula	mandibula	k1gFnSc1	mandibula
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
s	s	k7c7	s
těchto	tento	k3xDgFnPc2	tento
částí	část	k1gFnPc2	část
<g/>
:	:	kIx,	:
těla	tělo	k1gNnSc2	tělo
(	(	kIx(	(
<g/>
corpus	corpus	k1gInSc1	corpus
mandibulae	mandibula	k1gInSc2	mandibula
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ramena	rameno	k1gNnPc1	rameno
(	(	kIx(	(
<g/>
ramus	ramus	k1gInSc1	ramus
mandibulae	mandibula	k1gInSc2	mandibula
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výběžku	výběžek	k1gInSc2	výběžek
kloubního	kloubní	k2eAgInSc2d1	kloubní
a	a	k8xC	a
korunového	korunový	k2eAgInSc2d1	korunový
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
nese	nést	k5eAaImIp3nS	nést
tzv.	tzv.	kA	tzv.
zubní	zubní	k2eAgInSc1d1	zubní
výběžek	výběžek	k1gInSc1	výběžek
(	(	kIx(	(
<g/>
processus	processus	k1gInSc1	processus
alveolaris	alveolaris	k1gFnSc2	alveolaris
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
nese	nést	k5eAaImIp3nS	nést
zuby	zub	k1gInPc4	zub
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgNnPc1	dva
ramena	rameno	k1gNnPc1	rameno
patrové	patrový	k2eAgFnSc2d1	patrová
kosti	kost	k1gFnSc2	kost
jsou	být	k5eAaImIp3nP	být
zakončené	zakončený	k2eAgInPc1d1	zakončený
hlavičkou	hlavička	k1gFnSc7	hlavička
pro	pro	k7c4	pro
kloub	kloub	k1gInSc4	kloub
čelistní	čelistní	k2eAgInSc4d1	čelistní
<g/>
.	.	kIx.	.
kost	kost	k1gFnSc1	kost
radličná	radličný	k2eAgFnSc1d1	radličná
(	(	kIx(	(
<g/>
Vomer	Vomer	k1gMnSc1	Vomer
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Je	být	k5eAaImIp3nS	být
svisle	svisle	k6eAd1	svisle
postavená	postavený	k2eAgFnSc1d1	postavená
plochá	plochý	k2eAgFnSc1d1	plochá
kost	kost	k1gFnSc1	kost
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nS	tvořit
část	část	k1gFnSc1	část
nosní	nosní	k2eAgFnSc2d1	nosní
přepážky	přepážka	k1gFnSc2	přepážka
<g/>
.	.	kIx.	.
čelistní	čelistní	k2eAgInSc1d1	čelistní
kloub	kloub	k1gInSc1	kloub
(	(	kIx(	(
<g/>
articulatio	articulatio	k1gNnSc1	articulatio
temporomandibularis	temporomandibularis	k1gFnSc2	temporomandibularis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgNnSc7d1	jediné
pohyblivým	pohyblivý	k2eAgNnSc7d1	pohyblivé
spojením	spojení	k1gNnSc7	spojení
na	na	k7c6	na
lebce	lebka	k1gFnSc6	lebka
<g/>
.	.	kIx.	.
</s>
<s>
Hlavice	hlavice	k1gFnSc1	hlavice
(	(	kIx(	(
<g/>
caput	caput	k2eAgInSc1d1	caput
mandibulae	mandibulae	k1gInSc1	mandibulae
<g/>
)	)	kIx)	)
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
protáhlého	protáhlý	k2eAgInSc2d1	protáhlý
elipsoidu	elipsoid	k1gInSc2	elipsoid
zapadá	zapadat	k5eAaPmIp3nS	zapadat
do	do	k7c2	do
jamky	jamka	k1gFnSc2	jamka
na	na	k7c6	na
kosti	kost	k1gFnSc6	kost
spánkové	spánkový	k2eAgFnSc6d1	spánková
pod	pod	k7c7	pod
jařmovým	jařmový	k2eAgInSc7d1	jařmový
obloukem	oblouk	k1gInSc7	oblouk
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
obě	dva	k4xCgFnPc4	dva
kloubní	kloubní	k2eAgFnPc4d1	kloubní
plochy	plocha	k1gFnPc4	plocha
je	být	k5eAaImIp3nS	být
vsunuta	vsunout	k5eAaPmNgFnS	vsunout
vazivová	vazivový	k2eAgFnSc1d1	vazivová
destička	destička	k1gFnSc1	destička
<g/>
.	.	kIx.	.
</s>
<s>
Základními	základní	k2eAgInPc7d1	základní
pohyby	pohyb	k1gInPc7	pohyb
v	v	k7c6	v
kloubu	kloub	k1gInSc6	kloub
čelistním	čelistní	k2eAgInSc6d1	čelistní
jsou	být	k5eAaImIp3nP	být
otevírání	otevírání	k1gNnSc4	otevírání
úst	ústa	k1gNnPc2	ústa
(	(	kIx(	(
<g/>
deprese	deprese	k1gFnSc2	deprese
mandibuli	mandibule	k1gFnSc4	mandibule
<g/>
)	)	kIx)	)
a	a	k8xC	a
uzavírání	uzavírání	k1gNnSc1	uzavírání
úst	ústa	k1gNnPc2	ústa
(	(	kIx(	(
<g/>
elevace	elevace	k1gFnSc1	elevace
mandibuly	mandibula	k1gFnSc2	mandibula
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
omezeném	omezený	k2eAgInSc6d1	omezený
rozsahu	rozsah	k1gInSc6	rozsah
je	být	k5eAaImIp3nS	být
možný	možný	k2eAgInSc1d1	možný
i	i	k8xC	i
pohyb	pohyb	k1gInSc1	pohyb
mandibuly	mandibula	k1gFnSc2	mandibula
dopředu	dopředu	k6eAd1	dopředu
<g/>
,	,	kIx,	,
dozadu	dozadu	k6eAd1	dozadu
a	a	k8xC	a
do	do	k7c2	do
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
jazylka	jazylka	k1gFnSc1	jazylka
(	(	kIx(	(
<g/>
os	osa	k1gFnPc2	osa
hyoideum	hyoideum	k1gInSc1	hyoideum
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
podkovitá	podkovitý	k2eAgFnSc1d1	podkovitý
kůstka	kůstka	k1gFnSc1	kůstka
zavzatá	zavzatat	k5eAaPmIp3nS	zavzatat
do	do	k7c2	do
svalů	sval	k1gInPc2	sval
krčních	krční	k2eAgInPc2d1	krční
mezi	mezi	k7c7	mezi
dolní	dolní	k2eAgFnSc7d1	dolní
čelistí	čelist	k1gFnSc7	čelist
a	a	k8xC	a
kostrou	kostra	k1gFnSc7	kostra
hrtanu	hrtan	k1gInSc2	hrtan
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jazylce	jazylka	k1gFnSc6	jazylka
je	být	k5eAaImIp3nS	být
zavěšen	zavěšen	k2eAgInSc1d1	zavěšen
hrtan	hrtan	k1gInSc1	hrtan
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
kost	kost	k1gFnSc1	kost
má	mít	k5eAaImIp3nS	mít
tzv.	tzv.	kA	tzv.
malé	malý	k2eAgInPc1d1	malý
a	a	k8xC	a
velké	velký	k2eAgInPc1d1	velký
rohy	roh	k1gInPc1	roh
(	(	kIx(	(
<g/>
cornua	cornua	k1gFnSc1	cornua
majora	major	k1gMnSc2	major
et	et	k?	et
minora	minor	k1gMnSc2	minor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
hrtanu	hrtan	k1gInSc2	hrtan
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
upínají	upínat	k5eAaImIp3nP	upínat
dvě	dva	k4xCgFnPc1	dva
skupiny	skupina	k1gFnPc1	skupina
svalů	sval	k1gInPc2	sval
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
ji	on	k3xPp3gFnSc4	on
stabilizují	stabilizovat	k5eAaBmIp3nP	stabilizovat
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
svaly	sval	k1gInPc4	sval
nadjazylkové	nadjazylkový	k2eAgInPc4d1	nadjazylkový
(	(	kIx(	(
<g/>
suprahyoidní	suprahyoidní	k2eAgInPc4d1	suprahyoidní
<g/>
)	)	kIx)	)
a	a	k8xC	a
svaly	sval	k1gInPc4	sval
podjazylkové	podjazylkový	k2eAgInPc4d1	podjazylkový
(	(	kIx(	(
<g/>
infrahyoidní	infrahyoidní	k2eAgInPc4d1	infrahyoidní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
kost	kost	k1gFnSc1	kost
slzní	slzný	k2eAgMnPc1d1	slzný
(	(	kIx(	(
<g/>
os	osa	k1gFnPc2	osa
lacrimale	lacrimale	k6eAd1	lacrimale
<g/>
)	)	kIx)	)
tvoří	tvořit	k5eAaImIp3nP	tvořit
drobné	drobný	k2eAgFnPc4d1	drobná
párové	párový	k2eAgFnPc4d1	párová
kůstky	kůstka	k1gFnPc4	kůstka
vsazené	vsazený	k2eAgFnPc1d1	vsazená
vpředu	vpředu	k6eAd1	vpředu
do	do	k7c2	do
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
stěny	stěna	k1gFnSc2	stěna
<g />
.	.	kIx.	.
</s>
<s>
očnice	očnice	k1gFnPc1	očnice
<g/>
.	.	kIx.	.
sluchové	sluchový	k2eAgFnPc1d1	sluchová
kůstky	kůstka	k1gFnPc1	kůstka
<g/>
:	:	kIx,	:
kladívko	kladívko	k1gNnSc1	kladívko
(	(	kIx(	(
<g/>
malleus	malleus	k1gInSc1	malleus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kovadlinka	kovadlinka	k1gFnSc1	kovadlinka
(	(	kIx(	(
<g/>
incus	incus	k1gInSc1	incus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
třmínek	třmínek	k1gInSc1	třmínek
(	(	kIx(	(
<g/>
stapes	stapes	k1gInSc1	stapes
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
má	mít	k5eAaImIp3nS	mít
dokonce	dokonce	k9	dokonce
svůj	svůj	k3xOyFgInSc4	svůj
stabilizační	stabilizační	k2eAgInSc4d1	stabilizační
sval	sval	k1gInSc4	sval
-	-	kIx~	-
třmínkový	třmínkový	k2eAgInSc4d1	třmínkový
sval	sval	k1gInSc4	sval
(	(	kIx(	(
<g/>
musculus	musculus	k1gMnSc1	musculus
stapedius	stapedius	k1gMnSc1	stapedius
<g/>
)	)	kIx)	)
Dutina	dutina	k1gFnSc1	dutina
lebeční	lebeční	k2eAgFnSc1d1	lebeční
(	(	kIx(	(
<g/>
cavum	cavum	k1gInSc1	cavum
cranii	cranie	k1gFnSc4	cranie
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
určena	určit	k5eAaPmNgFnS	určit
pro	pro	k7c4	pro
mozek	mozek	k1gInSc4	mozek
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
shora	shora	k6eAd1	shora
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
lebeční	lebeční	k2eAgMnSc1d1	lebeční
jsou	být	k5eAaImIp3nP	být
patrné	patrný	k2eAgFnPc1d1	patrná
přední	přední	k2eAgFnPc1d1	přední
<g/>
,	,	kIx,	,
střední	střední	k2eAgFnSc1d1	střední
a	a	k8xC	a
zadní	zadní	k2eAgFnSc1d1	zadní
jáma	jáma	k1gFnSc1	jáma
lebeční	lebeční	k2eAgFnSc1d1	lebeční
<g/>
,	,	kIx,	,
uložené	uložený	k2eAgFnPc1d1	uložená
stupňovitě	stupňovitě	k6eAd1	stupňovitě
za	za	k7c7	za
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přední	přední	k2eAgFnSc6d1	přední
jámě	jáma	k1gFnSc6	jáma
lebeční	lebeční	k2eAgFnSc6d1	lebeční
leží	ležet	k5eAaImIp3nS	ležet
čelní	čelní	k2eAgInSc1d1	čelní
mozku	mozek	k1gInSc2	mozek
<g/>
,	,	kIx,	,
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
jámě	jáma	k1gFnSc6	jáma
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tureckém	turecký	k2eAgNnSc6d1	turecké
sedle	sedlo	k1gNnSc6	sedlo
uložen	uložen	k2eAgInSc1d1	uložen
podvěsek	podvěsek	k1gInSc1	podvěsek
mozkový	mozkový	k2eAgInSc1d1	mozkový
(	(	kIx(	(
<g/>
hypofýza	hypofýza	k1gFnSc1	hypofýza
<g/>
)	)	kIx)	)
a	a	k8xC	a
po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
spánkové	spánkový	k2eAgInPc4d1	spánkový
laloky	lalok	k1gInPc4	lalok
mozku	mozek	k1gInSc2	mozek
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k7c2	uprostřed
zadní	zadní	k2eAgFnSc2d1	zadní
jámy	jáma	k1gFnSc2	jáma
lebeční	lebeční	k2eAgFnSc2d1	lebeční
leží	ležet	k5eAaImIp3nS	ležet
prodloužená	prodloužený	k2eAgFnSc1d1	prodloužená
mícha	mícha	k1gFnSc1	mícha
a	a	k8xC	a
Varolův	Varolův	k2eAgInSc1d1	Varolův
most	most	k1gInSc1	most
<g/>
,	,	kIx,	,
po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
polokoule	polokoule	k1gFnSc2	polokoule
mozečku	mozeček	k1gInSc2	mozeček
<g/>
.	.	kIx.	.
</s>
<s>
Očnice	očnice	k1gFnSc1	očnice
(	(	kIx(	(
<g/>
orbita	orbita	k1gFnSc1	orbita
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
párový	párový	k2eAgInSc4d1	párový
prostor	prostor	k1gInSc4	prostor
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
čtyřbokého	čtyřboký	k2eAgInSc2d1	čtyřboký
jehlanu	jehlan	k1gInSc2	jehlan
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
je	být	k5eAaImIp3nS	být
uložena	uložen	k2eAgFnSc1d1	uložena
oční	oční	k2eAgFnSc1d1	oční
koule	koule	k1gFnSc1	koule
s	s	k7c7	s
přídatnými	přídatný	k2eAgInPc7d1	přídatný
orgány	orgán	k1gInPc7	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Dutina	dutina	k1gFnSc1	dutina
nosní	nosní	k2eAgFnSc1d1	nosní
(	(	kIx(	(
<g/>
cavum	cavum	k1gInSc1	cavum
nasi	nasi	k1gNnSc1	nasi
osseum	osseum	k1gInSc1	osseum
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nosní	nosní	k2eAgFnSc7d1	nosní
přepážkou	přepážka	k1gFnSc7	přepážka
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
poloviny	polovina	k1gFnPc4	polovina
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
polovina	polovina	k1gFnSc1	polovina
je	být	k5eAaImIp3nS	být
dále	daleko	k6eAd2	daleko
rozčleněna	rozčlenit	k5eAaPmNgFnS	rozčlenit
nosními	nosní	k2eAgFnPc7d1	nosní
skořepami	skořepa	k1gFnPc7	skořepa
(	(	kIx(	(
<g/>
konchami	koncha	k1gFnPc7	koncha
<g/>
)	)	kIx)	)
na	na	k7c6	na
horní	horní	k2eAgFnSc6d1	horní
<g/>
,	,	kIx,	,
střední	střední	k2eAgFnSc3d1	střední
a	a	k8xC	a
dolní	dolní	k2eAgInSc1d1	dolní
nosní	nosní	k2eAgInSc1d1	nosní
průduch	průduch	k1gInSc1	průduch
<g/>
.	.	kIx.	.
</s>
<s>
ELIŠKOVÁ	ELIŠKOVÁ	kA	ELIŠKOVÁ
<g/>
,	,	kIx,	,
Miloslava	Miloslava	k1gFnSc1	Miloslava
<g/>
;	;	kIx,	;
NAŇKA	naňka	k1gFnSc1	naňka
<g/>
,	,	kIx,	,
Ondřej	Ondřej	k1gMnSc1	Ondřej
<g/>
.	.	kIx.	.
</s>
<s>
Přehledová	přehledový	k2eAgFnSc1d1	přehledová
anatomie	anatomie	k1gFnSc1	anatomie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Karolinum	Karolinum	k1gNnSc1	Karolinum
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
309	[number]	k4	309
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
246	[number]	k4	246
<g/>
-	-	kIx~	-
<g/>
1216	[number]	k4	1216
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
</s>
