<s>
Oxid	oxid	k1gInSc1
hafničitý	hafničitý	k2eAgInSc1d1
</s>
<s>
Oxid	oxid	k1gInSc1
hafničitý	hafničitý	k2eAgInSc1d1
</s>
<s>
Obecné	obecný	k2eAgNnSc1d1
</s>
<s>
Systematický	systematický	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
Oxid	oxid	k1gInSc1
hafničitý	hafničitý	k2eAgInSc1d1
</s>
<s>
Anglický	anglický	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
Hafnium	hafnium	k1gNnSc1
<g/>
(	(	kIx(
<g/>
IV	IV	kA
<g/>
)	)	kIx)
oxide	oxid	k1gInSc5
</s>
<s>
Německý	německý	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
Hafnium	hafnium	k1gNnSc1
<g/>
(	(	kIx(
<g/>
IV	IV	kA
<g/>
)	)	kIx)
<g/>
-oxid	-oxid	k1gInSc1
</s>
<s>
Sumární	sumární	k2eAgInSc1d1
vzorec	vzorec	k1gInSc1
</s>
<s>
HfO	HfO	k?
<g/>
2	#num#	k4
</s>
<s>
Vzhled	vzhled	k1gInSc1
</s>
<s>
bílá	bílý	k2eAgFnSc1d1
práškovitá	práškovitý	k2eAgFnSc1d1
látka	látka	k1gFnSc1
</s>
<s>
Identifikace	identifikace	k1gFnSc1
</s>
<s>
Registrační	registrační	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
CAS	CAS	kA
</s>
<s>
12055-23-1	12055-23-1	k4
</s>
<s>
Vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Molární	molární	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
</s>
<s>
210,49	210,49	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
</s>
<s>
Teplota	teplota	k1gFnSc1
tání	tání	k1gNnSc2
</s>
<s>
2	#num#	k4
758	#num#	k4
°	°	k?
<g/>
C	C	kA
</s>
<s>
Teplota	teplota	k1gFnSc1
varu	var	k1gInSc2
</s>
<s>
5	#num#	k4
400	#num#	k4
°	°	k?
<g/>
C	C	kA
</s>
<s>
Hustota	hustota	k1gFnSc1
</s>
<s>
9,68	9,68	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
cm³	cm³	k?
(	(	kIx(
<g/>
20	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
9,58	9,58	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
cm	cm	kA
<g/>
3	#num#	k4
(	(	kIx(
<g/>
30	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
</s>
<s>
Rozpustnost	rozpustnost	k1gFnSc1
ve	v	k7c6
vodě	voda	k1gFnSc6
</s>
<s>
nerozpustný	rozpustný	k2eNgInSc1d1
</s>
<s>
Rozpustnost	rozpustnost	k1gFnSc1
v	v	k7c6
polárních	polární	k2eAgNnPc6d1
rozpouštědlech	rozpouštědlo	k1gNnPc6
</s>
<s>
rozpustný	rozpustný	k2eAgMnSc1d1
v	v	k7c6
roztoku	roztok	k1gInSc6
fluorovodíku	fluorovodík	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
koncentrovaném	koncentrovaný	k2eAgInSc6d1
roztoku	roztok	k1gInSc6
kyseliny	kyselina	k1gFnSc2
sírové	sírový	k2eAgFnSc2d1
a	a	k8xC
v	v	k7c6
koncentrovaných	koncentrovaný	k2eAgInPc6d1
roztocích	roztok	k1gInPc6
zásad	zásada	k1gFnPc2
nerozpustný	rozpustný	k2eNgMnSc1d1
v	v	k7c6
roztoku	roztok	k1gInSc6
kyseliny	kyselina	k1gFnSc2
<g/>
.	.	kIx.
dusičné	dusičný	k2eAgNnSc1d1
(	(	kIx(
<g/>
ne	ne	k9
<g/>
)	)	kIx)
</s>
<s>
Měrná	měrný	k2eAgFnSc1d1
magnetická	magnetický	k2eAgFnSc1d1
susceptibilita	susceptibilita	k1gFnSc1
</s>
<s>
−	−	k?
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
6	#num#	k4
cm	cm	kA
<g/>
3	#num#	k4
<g/>
g	g	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
</s>
<s>
Struktura	struktura	k1gFnSc1
</s>
<s>
Krystalová	krystalový	k2eAgFnSc1d1
struktura	struktura	k1gFnSc1
</s>
<s>
jednoklonná	jednoklonný	k2eAgFnSc1d1
(	(	kIx(
<g/>
α	α	k?
<g/>
)	)	kIx)
krychlová	krychlový	k2eAgFnSc1d1
(	(	kIx(
<g/>
β	β	k?
<g/>
)	)	kIx)
</s>
<s>
Hrana	hrana	k1gFnSc1
krystalové	krystalový	k2eAgFnSc2d1
mřížky	mřížka	k1gFnSc2
</s>
<s>
α	α	k1gFnPc1
a	a	k8xC
<g/>
=	=	kIx~
511,56	511,56	k4
pm	pm	k?
b	b	k?
<g/>
=	=	kIx~
517,22	517,22	k4
pm	pm	k?
c	c	k0
<g/>
=	=	kIx~
529,48	529,48	k4
pm	pm	k?
β	β	k?
<g/>
=	=	kIx~
99	#num#	k4
<g/>
°	°	k?
<g/>
11	#num#	k4
<g/>
´	´	k?
β	β	k1gFnSc2
a	a	k8xC
<g/>
=	=	kIx~
511,5	511,5	k4
pm	pm	k?
</s>
<s>
Termodynamické	termodynamický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Standardní	standardní	k2eAgFnSc1d1
slučovací	slučovací	k2eAgFnSc1d1
entalpie	entalpie	k1gFnSc1
Δ	Δ	k1gMnSc1
<g/>
°	°	k?
</s>
<s>
−	−	k?
145	#num#	k4
kJ	kJ	k?
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
</s>
<s>
Standardní	standardní	k2eAgFnSc1d1
molární	molární	k2eAgFnSc1d1
entropie	entropie	k1gFnSc1
S	s	k7c7
<g/>
°	°	k?
</s>
<s>
59,35	59,35	k4
JK	JK	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
<g/>
mol	mol	k1gInSc1
<g/>
−	−	k?
<g/>
1	#num#	k4
</s>
<s>
Standardní	standardní	k2eAgFnSc1d1
slučovací	slučovací	k2eAgFnSc1d1
Gibbsova	Gibbsův	k2eAgFnSc1d1
energie	energie	k1gFnSc1
Δ	Δ	k1gMnSc1
<g/>
°	°	k?
</s>
<s>
−	−	k?
089	#num#	k4
kJ	kJ	k?
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
</s>
<s>
Izobarické	izobarický	k2eAgNnSc1d1
měrné	měrný	k2eAgNnSc1d1
teplo	teplo	k1gNnSc1
cp	cp	k?
</s>
<s>
0,286	0,286	k4
JK	JK	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
<g/>
g	g	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
</s>
<s>
Není	být	k5eNaImIp3nS
<g/>
-li	-li	k?
uvedeno	uvést	k5eAaPmNgNnS
jinak	jinak	k6eAd1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
použityjednotky	použityjednotka	k1gFnPc1
SI	si	k1gNnSc2
a	a	k8xC
STP	STP	kA
(	(	kIx(
<g/>
25	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
100	#num#	k4
kPa	kPa	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Oxid	oxid	k1gInSc1
hafničitý	hafničitý	k2eAgInSc1d1
(	(	kIx(
<g/>
chemický	chemický	k2eAgInSc1d1
vzorec	vzorec	k1gInSc1
HfO	HfO	k1gFnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
oxid	oxid	k1gInSc1
hafnia	hafnium	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
v	v	k7c6
něm	on	k3xPp3gInSc6
má	mít	k5eAaImIp3nS
oxidační	oxidační	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
IV	Iva	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
bílý	bílý	k2eAgInSc1d1
prášek	prášek	k1gInSc1
nerozpustný	rozpustný	k2eNgInSc1d1
ve	v	k7c6
vodě	voda	k1gFnSc6
a	a	k8xC
kyselině	kyselina	k1gFnSc6
dusičné	dusičný	k2eAgFnSc2d1
HNO	HNO	kA
<g/>
3	#num#	k4
<g/>
,	,	kIx,
rozpustný	rozpustný	k2eAgInSc1d1
je	být	k5eAaImIp3nS
naopak	naopak	k6eAd1
v	v	k7c6
kyselině	kyselina	k1gFnSc6
fluorovodíkové	fluorovodíkový	k2eAgFnSc6d1
HF	HF	kA
<g/>
,	,	kIx,
koncentrované	koncentrovaný	k2eAgFnSc6d1
kyselině	kyselina	k1gFnSc6
sírové	sírový	k2eAgFnSc2d1
H2SO4	H2SO4	k1gFnSc2
a	a	k8xC
v	v	k7c6
koncentrovaných	koncentrovaný	k2eAgInPc6d1
roztocích	roztok	k1gInPc6
zásad	zásada	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
o	o	k7c4
jednu	jeden	k4xCgFnSc4
z	z	k7c2
nejběžnějších	běžný	k2eAgFnPc2d3
a	a	k8xC
nejstabilnějších	stabilní	k2eAgFnPc2d3
forem	forma	k1gFnPc2
hafnia	hafnium	k1gNnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
meziproduktem	meziprodukt	k1gInSc7
některých	některý	k3yIgInPc2
procesů	proces	k1gInPc2
na	na	k7c4
získání	získání	k1gNnSc4
tohoto	tento	k3xDgInSc2
kovu	kov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Vzhledem	vzhledem	k7c3
ke	k	k7c3
své	svůj	k3xOyFgFnSc3
vysoké	vysoký	k2eAgFnSc3d1
teplotě	teplota	k1gFnSc3
tání	tání	k1gNnSc2
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
na	na	k7c4
výrobu	výroba	k1gFnSc4
tavicích	tavicí	k2eAgInPc2d1
kelímků	kelímek	k1gInPc2
a	a	k8xC
termočlánků	termočlánek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
se	se	k3xPyFc4
dále	daleko	k6eAd2
objevily	objevit	k5eAaPmAgInP
první	první	k4xOgInPc1
unipolární	unipolární	k2eAgInPc1d1
tranzistory	tranzistor	k1gInPc1
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gFnSc1
řídicí	řídicí	k2eAgFnSc1d1
elektroda	elektroda	k1gFnSc1
je	být	k5eAaImIp3nS
izolována	izolovat	k5eAaBmNgFnS
pomocí	pomocí	k7c2
oxidů	oxid	k1gInPc2
hafnia	hafnium	k1gNnSc2
místo	místo	k6eAd1
typických	typický	k2eAgInPc2d1
oxidů	oxid	k1gInPc2
křemíku	křemík	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výhodou	výhoda	k1gFnSc7
je	být	k5eAaImIp3nS
4	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
×	×	k?
vyšší	vysoký	k2eAgFnSc1d2
relativní	relativní	k2eAgFnSc1d1
permitivita	permitivita	k1gFnSc1
HfO	HfO	k1gFnSc2
<g/>
2	#num#	k4
oproti	oproti	k7c3
SiO	SiO	k1gFnSc3
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Hafnium	hafnium	k1gNnSc1
dioxide	dioxid	k1gInSc5
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
VOHLÍDAL	VOHLÍDAL	kA
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
;	;	kIx,
ŠTULÍK	štulík	k1gInSc1
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
;	;	kIx,
JULÁK	JULÁK	kA
<g/>
,	,	kIx,
Alois	Alois	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chemické	chemický	k2eAgFnPc4d1
a	a	k8xC
analytické	analytický	k2eAgFnPc4d1
tabulky	tabulka	k1gFnPc4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Grada	Grada	k1gFnSc1
Publishing	Publishing	k1gInSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7169	#num#	k4
<g/>
-	-	kIx~
<g/>
855	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Oxidy	oxid	k1gInPc1
s	s	k7c7
prvkem	prvek	k1gInSc7
v	v	k7c6
oxidačním	oxidační	k2eAgNnSc6d1
čísle	číslo	k1gNnSc6
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s>
Oxid	oxid	k1gInSc1
americičitý	americičitý	k2eAgInSc1d1
(	(	kIx(
<g/>
AmO	AmO	k1gFnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
uhličitý	uhličitý	k2eAgInSc1d1
(	(	kIx(
<g/>
CO	co	k6eAd1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
ceričitý	ceričitý	k2eAgInSc1d1
(	(	kIx(
<g/>
CeO	CeO	k1gFnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
chloričitý	chloričitý	k2eAgInSc1d1
(	(	kIx(
<g/>
ClO	clo	k1gNnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
chromičitý	chromičitý	k2eAgInSc1d1
(	(	kIx(
<g/>
CrO	CrO	k1gFnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
dusičitý	dusičitý	k2eAgInSc1d1
(	(	kIx(
<g/>
NO	no	k9
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
germaničitý	germaničitý	k2eAgInSc1d1
(	(	kIx(
<g/>
GeO	GeO	k1gFnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
hafničitý	hafničitý	k2eAgInSc1d1
(	(	kIx(
<g/>
HfO	HfO	k1gFnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
neptuničitý	ptuničitý	k2eNgInSc1d1
(	(	kIx(
<g/>
NpO	NpO	k1gFnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
olovičitý	olovičitý	k2eAgInSc1d1
(	(	kIx(
<g/>
PbO	PbO	k1gFnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
manganičitý	manganičitý	k2eAgInSc1d1
(	(	kIx(
<g/>
MnO	MnO	k1gFnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
osmičitý	osmičitý	k2eAgInSc1d1
(	(	kIx(
<g/>
OsO	osa	k1gFnSc5
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
plutoničitý	plutoničitý	k2eAgInSc1d1
(	(	kIx(
<g/>
PuO	PuO	k1gFnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
protaktiničitý	protaktiničitý	k2eAgInSc1d1
(	(	kIx(
<g/>
PaO	PaO	k1gFnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
rheničitý	rheničitý	k2eAgInSc1d1
(	(	kIx(
<g/>
ReO	Rea	k1gFnSc5
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
rutheničitý	rutheničitý	k2eAgInSc1d1
(	(	kIx(
<g/>
RuO	RuO	k1gFnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
seleničitý	seleničitý	k2eAgInSc1d1
(	(	kIx(
<g/>
SeO	SeO	k1gFnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
křemičitý	křemičitý	k2eAgInSc1d1
(	(	kIx(
<g/>
SiO	SiO	k1gFnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
siřičitý	siřičitý	k2eAgInSc1d1
(	(	kIx(
<g/>
SO	So	kA
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
telluričitý	telluričitý	k2eAgInSc1d1
(	(	kIx(
<g/>
TeO	Tea	k1gFnSc5
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
thoričitý	thoričitý	k2eAgInSc1d1
(	(	kIx(
<g/>
ThO	ThO	k1gFnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
cíničitý	cíničitý	k2eAgInSc1d1
(	(	kIx(
<g/>
SnO	SnO	k1gFnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
titaničitý	titaničitý	k2eAgInSc1d1
(	(	kIx(
<g/>
TiO	TiO	k1gFnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
wolframičitý	wolframičitý	k2eAgInSc1d1
(	(	kIx(
<g/>
WO	WO	kA
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
uraničitý	uraničitý	k2eAgInSc1d1
(	(	kIx(
<g/>
UO	UO	kA
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
vanadičitý	vanadičitý	k2eAgInSc1d1
(	(	kIx(
<g/>
VO	VO	k?
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
zirkoničitý	zirkoničitý	k2eAgInSc1d1
(	(	kIx(
<g/>
ZrO	ZrO	k1gFnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
viz	vidět	k5eAaImRp2nS
též	též	k9
Trioxid	Trioxid	k1gInSc4
uhlíku	uhlík	k1gInSc2
(	(	kIx(
<g/>
CO	co	k9
<g/>
3	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
praseodymito-praseodymičitý	praseodymito-praseodymičitý	k2eAgInSc1d1
(	(	kIx(
<g/>
Pr	pr	k0
<g/>
6	#num#	k4
<g/>
O	o	k7c4
<g/>
11	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Chemie	chemie	k1gFnSc1
</s>
