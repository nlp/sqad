<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
bílý	bílý	k2eAgInSc1d1
prášek	prášek	k1gInSc1
nerozpustný	rozpustný	k2eNgInSc1d1
ve	v	k7c6
vodě	voda	k1gFnSc6
a	a	k8xC
kyselině	kyselina	k1gFnSc6
dusičné	dusičný	k2eAgFnSc2d1
HNO	HNO	kA
<g/>
3	#num#	k4
<g/>
,	,	kIx,
rozpustný	rozpustný	k2eAgInSc1d1
je	být	k5eAaImIp3nS
naopak	naopak	k6eAd1
v	v	k7c6
kyselině	kyselina	k1gFnSc6
fluorovodíkové	fluorovodíkový	k2eAgFnSc6d1
HF	HF	kA
<g/>
,	,	kIx,
koncentrované	koncentrovaný	k2eAgFnSc6d1
kyselině	kyselina	k1gFnSc6
sírové	sírový	k2eAgFnSc2d1
H2SO4	H2SO4	k1gFnSc2
a	a	k8xC
v	v	k7c6
koncentrovaných	koncentrovaný	k2eAgInPc6d1
roztocích	roztok	k1gInPc6
zásad	zásada	k1gFnPc2
<g/>
.	.	kIx.
</s>