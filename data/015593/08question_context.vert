<s>
Oxid	oxid	k1gInSc1
hafničitý	hafničitý	k2eAgInSc1d1
(	(	kIx(
<g/>
chemický	chemický	k2eAgInSc1d1
vzorec	vzorec	k1gInSc1
HfO	HfO	k1gFnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
oxid	oxid	k1gInSc1
hafnia	hafnium	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
v	v	k7c6
něm	on	k3xPp3gInSc6
má	mít	k5eAaImIp3nS
oxidační	oxidační	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
IV	Iva	k1gFnPc2
<g/>
.	.	kIx.
</s>