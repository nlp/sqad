<s>
Cukr	cukr	k1gInSc1	cukr
(	(	kIx(	(
<g/>
z	z	k7c2	z
něm.	něm.	k?	něm.
Zucker	Zuckra	k1gFnPc2	Zuckra
<g/>
,	,	kIx,	,
z	z	k7c2	z
arab	arab	k1gMnSc1	arab
<g/>
.	.	kIx.	.
sukkar	sukkar	k1gMnSc1	sukkar
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
přírodní	přírodní	k2eAgNnSc4d1	přírodní
sladidlo	sladidlo	k1gNnSc4	sladidlo
<g/>
,	,	kIx,	,
sladká	sladký	k2eAgFnSc1d1	sladká
poživatina	poživatina	k1gFnSc1	poživatina
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
označovaná	označovaný	k2eAgFnSc1d1	označovaná
odborně	odborně	k6eAd1	odborně
jako	jako	k8xS	jako
potravinářský	potravinářský	k2eAgInSc4d1	potravinářský
cukr	cukr	k1gInSc4	cukr
<g/>
.	.	kIx.	.
</s>
<s>
Chemicky	chemicky	k6eAd1	chemicky
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
sacharóza	sacharóza	k1gFnSc1	sacharóza
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
disacharid	disacharid	k1gInSc4	disacharid
složený	složený	k2eAgInSc4d1	složený
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
molekuly	molekula	k1gFnSc2	molekula
α	α	k?	α
a	a	k8xC	a
jedné	jeden	k4xCgFnSc2	jeden
molekuly	molekula	k1gFnSc2	molekula
β	β	k?	β
<g/>
,	,	kIx,	,
spojených	spojený	k2eAgInPc2d1	spojený
α	α	k?	α
<g/>
,	,	kIx,	,
<g/>
β	β	k?	β
<g/>
1,2	[number]	k4	1,2
<g/>
-glykosidovou	lykosidový	k2eAgFnSc7d1	-glykosidový
vazbou	vazba	k1gFnSc7	vazba
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
jako	jako	k8xC	jako
potravinářská	potravinářský	k2eAgFnSc1d1	potravinářská
surovina	surovina	k1gFnSc1	surovina
v	v	k7c6	v
cukrovarech	cukrovar	k1gInPc6	cukrovar
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
třtinový	třtinový	k2eAgInSc4d1	třtinový
z	z	k7c2	z
cukrové	cukrový	k2eAgFnSc2d1	cukrová
třtiny	třtina	k1gFnSc2	třtina
nebo	nebo	k8xC	nebo
řepný	řepný	k2eAgMnSc1d1	řepný
z	z	k7c2	z
cukrové	cukrový	k2eAgFnSc2d1	cukrová
řepy	řepa	k1gFnSc2	řepa
a	a	k8xC	a
dodává	dodávat	k5eAaImIp3nS	dodávat
se	se	k3xPyFc4	se
na	na	k7c4	na
trh	trh	k1gInSc4	trh
jako	jako	k8xS	jako
bílý	bílý	k2eAgInSc1d1	bílý
anebo	anebo	k8xC	anebo
surový	surový	k2eAgInSc1d1	surový
(	(	kIx(	(
<g/>
nahnědlý	nahnědlý	k2eAgInSc1d1	nahnědlý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
sypké	sypký	k2eAgFnSc6d1	sypká
podobě	podoba	k1gFnSc6	podoba
(	(	kIx(	(
<g/>
v	v	k7c6	v
krystalech	krystal	k1gInPc6	krystal
<g/>
,	,	kIx,	,
prášku	prášek	k1gInSc2	prášek
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
zpevněné	zpevněný	k2eAgInPc1d1	zpevněný
<g/>
,	,	kIx,	,
slisované	slisovaný	k2eAgInPc1d1	slisovaný
tvary	tvar	k1gInPc1	tvar
-	-	kIx~	-
homole	homole	k1gFnPc1	homole
nebo	nebo	k8xC	nebo
kostky	kostka	k1gFnPc1	kostka
<g/>
.	.	kIx.	.
</s>
<s>
Světová	světový	k2eAgFnSc1d1	světová
produkce	produkce	k1gFnSc1	produkce
cukru	cukr	k1gInSc2	cukr
činí	činit	k5eAaImIp3nS	činit
kolem	kolem	k7c2	kolem
160	[number]	k4	160
milionů	milion	k4xCgInPc2	milion
tun	tuna	k1gFnPc2	tuna
ročně	ročně	k6eAd1	ročně
a	a	k8xC	a
cukr	cukr	k1gInSc4	cukr
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
hospodářsky	hospodářsky	k6eAd1	hospodářsky
velmi	velmi	k6eAd1	velmi
významná	významný	k2eAgFnSc1d1	významná
komodita	komodita	k1gFnSc1	komodita
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
"	"	kIx"	"
<g/>
cukry	cukr	k1gInPc1	cukr
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
v	v	k7c6	v
množném	množný	k2eAgNnSc6d1	množné
čísle	číslo	k1gNnSc6	číslo
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
všechny	všechen	k3xTgInPc4	všechen
sacharidy	sacharid	k1gInPc4	sacharid
(	(	kIx(	(
<g/>
k	k	k7c3	k
nimž	jenž	k3xRgMnPc3	jenž
potravinářský	potravinářský	k2eAgInSc4d1	potravinářský
cukr	cukr	k1gInSc4	cukr
patří	patřit	k5eAaImIp3nS	patřit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sacharóza	sacharóza	k1gFnSc1	sacharóza
je	být	k5eAaImIp3nS	být
obsažena	obsáhnout	k5eAaPmNgFnS	obsáhnout
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
rostlinách	rostlina	k1gFnPc6	rostlina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
produkují	produkovat	k5eAaImIp3nP	produkovat
jako	jako	k8xS	jako
rezervu	rezerva	k1gFnSc4	rezerva
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Hospodářsky	hospodářsky	k6eAd1	hospodářsky
nejvýznamnější	významný	k2eAgFnPc1d3	nejvýznamnější
jsou	být	k5eAaImIp3nP	být
cukrová	cukrový	k2eAgFnSc1d1	cukrová
řepa	řepa	k1gFnSc1	řepa
původem	původ	k1gInSc7	původ
z	z	k7c2	z
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
tropická	tropický	k2eAgFnSc1d1	tropická
třtina	třtina	k1gFnSc1	třtina
z	z	k7c2	z
Karibiku	Karibik	k1gInSc2	Karibik
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
cukru	cukr	k1gInSc2	cukr
z	z	k7c2	z
cukrové	cukrový	k2eAgFnSc2d1	cukrová
řepy	řepa	k1gFnSc2	řepa
se	se	k3xPyFc4	se
sklizená	sklizený	k2eAgFnSc1d1	sklizená
řepa	řepa	k1gFnSc1	řepa
nejprve	nejprve	k6eAd1	nejprve
pere	prát	k5eAaImIp3nS	prát
a	a	k8xC	a
zbavuje	zbavovat	k5eAaImIp3nS	zbavovat
nečistot	nečistota	k1gFnPc2	nečistota
<g/>
,	,	kIx,	,
řeže	řezat	k5eAaImIp3nS	řezat
na	na	k7c4	na
úzké	úzký	k2eAgInPc4d1	úzký
proužky	proužek	k1gInPc4	proužek
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
řízky	řízek	k1gInPc1	řízek
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
ukládá	ukládat	k5eAaImIp3nS	ukládat
do	do	k7c2	do
difuzérů	difuzér	k1gInPc2	difuzér
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
cukr	cukr	k1gInSc4	cukr
vyluhuje	vyluhovat	k5eAaImIp3nS	vyluhovat
vodou	voda	k1gFnSc7	voda
při	při	k7c6	při
různých	různý	k2eAgFnPc6d1	různá
teplotách	teplota	k1gFnPc6	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Vyluhované	vyluhovaný	k2eAgInPc1d1	vyluhovaný
řízky	řízek	k1gInPc1	řízek
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
užívají	užívat	k5eAaImIp3nP	užívat
pro	pro	k7c4	pro
krmení	krmení	k1gNnSc4	krmení
dobytka	dobytek	k1gInSc2	dobytek
<g/>
.	.	kIx.	.
</s>
<s>
Vyluhovaná	vyluhovaný	k2eAgFnSc1d1	vyluhovaná
cukerná	cukerný	k2eAgFnSc1d1	cukerná
šťáva	šťáva	k1gFnSc1	šťáva
se	se	k3xPyFc4	se
čistí	čistit	k5eAaImIp3nS	čistit
<g/>
,	,	kIx,	,
filtruje	filtrovat	k5eAaImIp3nS	filtrovat
a	a	k8xC	a
čeří	čeřit	k5eAaImIp3nS	čeřit
přidáváním	přidávání	k1gNnSc7	přidávání
vápna	vápno	k1gNnSc2	vápno
a	a	k8xC	a
působením	působení	k1gNnSc7	působení
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
v	v	k7c6	v
saturátoru	saturátor	k1gInSc6	saturátor
a	a	k8xC	a
neutralizují	neutralizovat	k5eAaBmIp3nP	neutralizovat
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
výluhy	výluh	k1gInPc1	výluh
rostlinných	rostlinný	k2eAgFnPc2d1	rostlinná
kyselin	kyselina	k1gFnPc2	kyselina
a	a	k8xC	a
vysráží	vysrážet	k5eAaPmIp3nS	vysrážet
do	do	k7c2	do
zákalu	zákal	k1gInSc2	zákal
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
cedí	cedit	k5eAaImIp3nS	cedit
<g/>
,	,	kIx,	,
profiltruje	profiltrovat	k5eAaPmIp3nS	profiltrovat
v	v	k7c6	v
kalolisu	kalolis	k1gInSc6	kalolis
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
postup	postup	k1gInSc1	postup
se	se	k3xPyFc4	se
dvakrát	dvakrát	k6eAd1	dvakrát
až	až	k9	až
třikrát	třikrát	k6eAd1	třikrát
opakuje	opakovat	k5eAaImIp3nS	opakovat
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
šťáva	šťáva	k1gFnSc1	šťáva
vaří	vařit	k5eAaImIp3nS	vařit
(	(	kIx(	(
<g/>
zahušťuje	zahušťovat	k5eAaImIp3nS	zahušťovat
<g/>
)	)	kIx)	)
a	a	k8xC	a
odpařuje	odpařovat	k5eAaImIp3nS	odpařovat
za	za	k7c2	za
sníženého	snížený	k2eAgInSc2d1	snížený
tlaku	tlak	k1gInSc2	tlak
vzduchu	vzduch	k1gInSc2	vzduch
a	a	k8xC	a
tato	tento	k3xDgFnSc1	tento
"	"	kIx"	"
<g/>
těžká	těžký	k2eAgFnSc1d1	těžká
<g/>
"	"	kIx"	"
šťáva	šťáva	k1gFnSc1	šťáva
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
filtruje	filtrovat	k5eAaImIp3nS	filtrovat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vakuovém	vakuový	k2eAgInSc6d1	vakuový
varostroji	varostroj	k1gInSc6	varostroj
pak	pak	k6eAd1	pak
nastane	nastat	k5eAaPmIp3nS	nastat
postupná	postupný	k2eAgFnSc1d1	postupná
řetězová	řetězový	k2eAgFnSc1d1	řetězová
krystalizace	krystalizace	k1gFnSc1	krystalizace
<g/>
,	,	kIx,	,
po	po	k7c6	po
zahájení	zahájení	k1gNnSc6	zahájení
procesu	proces	k1gInSc2	proces
krystalizace	krystalizace	k1gFnPc4	krystalizace
přidáním	přidání	k1gNnSc7	přidání
malého	malý	k2eAgNnSc2d1	malé
množství	množství	k1gNnSc2	množství
krystalického	krystalický	k2eAgInSc2d1	krystalický
cukru	cukr	k1gInSc2	cukr
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vykrystalování	vykrystalování	k1gNnSc6	vykrystalování
ve	v	k7c6	v
varostroji	varostroj	k1gInSc6	varostroj
se	se	k3xPyFc4	se
zkrystalizovaná	zkrystalizovaný	k2eAgFnSc1d1	zkrystalizovaná
cukrová	cukrový	k2eAgFnSc1d1	cukrová
hmota	hmota	k1gFnSc1	hmota
odstředí	odstředit	k5eAaPmIp3nS	odstředit
a	a	k8xC	a
propláchne	propláchnout	k5eAaPmIp3nS	propláchnout
v	v	k7c6	v
odstředivkách	odstředivka	k1gFnPc6	odstředivka
od	od	k7c2	od
tekutých	tekutý	k2eAgFnPc2d1	tekutá
nečistot	nečistota	k1gFnPc2	nečistota
(	(	kIx(	(
<g/>
melasa	melasa	k1gFnSc1	melasa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zbývajících	zbývající	k2eAgInPc2d1	zbývající
z	z	k7c2	z
původní	původní	k2eAgFnSc2d1	původní
šťávy	šťáva	k1gFnSc2	šťáva
a	a	k8xC	a
výsledná	výsledný	k2eAgFnSc1d1	výsledná
vlhká	vlhký	k2eAgFnSc1d1	vlhká
vykrystalizovaná	vykrystalizovaný	k2eAgFnSc1d1	vykrystalizovaná
cukrovina	cukrovina	k1gFnSc1	cukrovina
se	se	k3xPyFc4	se
nyní	nyní	k6eAd1	nyní
(	(	kIx(	(
<g/>
v	v	k7c6	v
starších	starý	k2eAgFnPc6d2	starší
technologiích	technologie	k1gFnPc6	technologie
se	se	k3xPyFc4	se
plnila	plnit	k5eAaImAgFnS	plnit
do	do	k7c2	do
forem	forma	k1gFnPc2	forma
<g/>
)	)	kIx)	)
suší	sušit	k5eAaImIp3nS	sušit
na	na	k7c4	na
krystalický	krystalický	k2eAgInSc4d1	krystalický
cukr	cukr	k1gInSc4	cukr
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
přes	přes	k7c4	přes
síta	síto	k1gNnPc4	síto
prosévá	prosévat	k5eAaImIp3nS	prosévat
a	a	k8xC	a
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
na	na	k7c4	na
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
frakce	frakce	k1gFnPc4	frakce
zrnitosti	zrnitost	k1gFnSc2	zrnitost
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
"	"	kIx"	"
<g/>
zadní	zadní	k2eAgInSc1d1	zadní
cukr	cukr	k1gInSc1	cukr
<g/>
"	"	kIx"	"
čili	čili	k8xC	čili
melasa	melasa	k1gFnSc1	melasa
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
asi	asi	k9	asi
40	[number]	k4	40
%	%	kIx~	%
nevykrystalizovatelných	vykrystalizovatelný	k2eNgInPc2d1	vykrystalizovatelný
cukrů	cukr	k1gInPc2	cukr
a	a	k8xC	a
ošklivě	ošklivě	k6eAd1	ošklivě
páchne	páchnout	k5eAaImIp3nS	páchnout
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
lihu	líh	k1gInSc2	líh
<g/>
,	,	kIx,	,
kvasnic	kvasnice	k1gFnPc2	kvasnice
<g/>
,	,	kIx,	,
nápojů	nápoj	k1gInPc2	nápoj
(	(	kIx(	(
<g/>
např.	např.	kA	např.
kofoly	kofola	k1gFnSc2	kofola
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
krmiva	krmivo	k1gNnPc1	krmivo
pro	pro	k7c4	pro
zvířata	zvíře	k1gNnPc4	zvíře
a	a	k8xC	a
jiné	jiný	k2eAgInPc4d1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
Výroba	výroba	k1gFnSc1	výroba
cukru	cukr	k1gInSc2	cukr
z	z	k7c2	z
cukrové	cukrový	k2eAgFnSc2d1	cukrová
třtiny	třtina	k1gFnSc2	třtina
je	být	k5eAaImIp3nS	být
obdobná	obdobný	k2eAgFnSc1d1	obdobná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
snazší	snadný	k2eAgMnSc1d2	snazší
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
třtina	třtina	k1gFnSc1	třtina
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
méně	málo	k6eAd2	málo
nečistot	nečistota	k1gFnPc2	nečistota
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
cukr	cukr	k1gInSc1	cukr
v	v	k7c6	v
menším	malý	k2eAgNnSc6d2	menší
množství	množství	k1gNnSc6	množství
vyráběl	vyrábět	k5eAaImAgMnS	vyrábět
také	také	k9	také
z	z	k7c2	z
datlí	datle	k1gFnPc2	datle
<g/>
,	,	kIx,	,
z	z	k7c2	z
čiroku	čirok	k1gInSc2	čirok
<g/>
,	,	kIx,	,
z	z	k7c2	z
javoru	javor	k1gInSc2	javor
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Výroba	výroba	k1gFnSc1	výroba
cukru	cukr	k1gInSc2	cukr
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
složitá	složitý	k2eAgFnSc1d1	složitá
a	a	k8xC	a
náročná	náročný	k2eAgFnSc1d1	náročná
na	na	k7c4	na
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
cukrovarnictví	cukrovarnictví	k1gNnSc2	cukrovarnictví
tak	tak	k6eAd1	tak
výrazně	výrazně	k6eAd1	výrazně
přispěl	přispět	k5eAaPmAgMnS	přispět
rozvoj	rozvoj	k1gInSc4	rozvoj
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
strojírenství	strojírenství	k1gNnSc2	strojírenství
a	a	k8xC	a
české	český	k2eAgFnPc1d1	Česká
země	zem	k1gFnPc1	zem
zaujímaly	zaujímat	k5eAaImAgFnP	zaujímat
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
v	v	k7c6	v
obou	dva	k4xCgFnPc6	dva
oblastech	oblast	k1gFnPc6	oblast
významné	významný	k2eAgNnSc1d1	významné
postavení	postavení	k1gNnSc1	postavení
<g/>
.	.	kIx.	.
</s>
<s>
Cukr	cukr	k1gInSc1	cukr
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
stravitelná	stravitelný	k2eAgFnSc1d1	stravitelná
potravina	potravina	k1gFnSc1	potravina
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
obsahem	obsah	k1gInSc7	obsah
energie	energie	k1gFnSc2	energie
(	(	kIx(	(
<g/>
16,8	[number]	k4	16,8
kJ	kJ	k?	kJ
<g/>
/	/	kIx~	/
<g/>
g	g	kA	g
<g/>
)	)	kIx)	)
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
žádných	žádný	k3yNgFnPc2	žádný
dalších	další	k2eAgFnPc2d1	další
živin	živina	k1gFnPc2	živina
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pokojové	pokojový	k2eAgFnSc6d1	pokojová
teplotě	teplota	k1gFnSc6	teplota
se	se	k3xPyFc4	se
v	v	k7c6	v
litru	litr	k1gInSc6	litr
vody	voda	k1gFnSc2	voda
rozpustí	rozpustit	k5eAaPmIp3nS	rozpustit
přes	přes	k7c4	přes
2	[number]	k4	2
kg	kg	kA	kg
cukru	cukr	k1gInSc2	cukr
<g/>
.	.	kIx.	.
</s>
<s>
Spotřeba	spotřeba	k1gFnSc1	spotřeba
cukru	cukr	k1gInSc2	cukr
ve	v	k7c6	v
vyspělých	vyspělý	k2eAgFnPc6d1	vyspělá
zemích	zem	k1gFnPc6	zem
za	za	k7c2	za
posledních	poslední	k2eAgNnPc2d1	poslední
150	[number]	k4	150
let	léto	k1gNnPc2	léto
nesmírně	smírně	k6eNd1	smírně
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
–	–	k?	–
například	například	k6eAd1	například
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
se	s	k7c7	s
spotřebou	spotřeba	k1gFnSc7	spotřeba
40,4	[number]	k4	40,4
kg	kg	kA	kg
na	na	k7c4	na
osobu	osoba	k1gFnSc4	osoba
a	a	k8xC	a
rok	rok	k1gInSc4	rok
asi	asi	k9	asi
20	[number]	k4	20
<g/>
×	×	k?	×
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
USA	USA	kA	USA
činí	činit	k5eAaImIp3nS	činit
roční	roční	k2eAgFnSc1d1	roční
spotřeba	spotřeba	k1gFnSc1	spotřeba
cukru	cukr	k1gInSc2	cukr
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
sladidel	sladidlo	k1gNnPc2	sladidlo
asi	asi	k9	asi
70	[number]	k4	70
kg	kg	kA	kg
na	na	k7c4	na
osobu	osoba	k1gFnSc4	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
cukru	cukr	k1gInSc2	cukr
je	být	k5eAaImIp3nS	být
konzumována	konzumován	k2eAgFnSc1d1	konzumována
v	v	k7c6	v
nealkoholických	alkoholický	k2eNgInPc6d1	nealkoholický
nápojích	nápoj	k1gInPc6	nápoj
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Ještě	ještě	k9	ještě
před	před	k7c7	před
100	[number]	k4	100
lety	let	k1gInPc7	let
přitom	přitom	k6eAd1	přitom
byla	být	k5eAaImAgFnS	být
roční	roční	k2eAgFnSc1d1	roční
spotřeba	spotřeba	k1gFnSc1	spotřeba
cukru	cukr	k1gInSc2	cukr
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
nižší	nízký	k2eAgFnSc4d2	nižší
než	než	k8xS	než
2,5	[number]	k4	2,5
kg	kg	kA	kg
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
i	i	k9	i
dnes	dnes	k6eAd1	dnes
platí	platit	k5eAaImIp3nS	platit
o	o	k7c6	o
méně	málo	k6eAd2	málo
rozvinutých	rozvinutý	k2eAgFnPc6d1	rozvinutá
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
v	v	k7c6	v
Afghánistánu	Afghánistán	k1gInSc6	Afghánistán
činila	činit	k5eAaImAgFnS	činit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
roční	roční	k2eAgFnSc1d1	roční
spotřeba	spotřeba	k1gFnSc1	spotřeba
cukru	cukr	k1gInSc2	cukr
na	na	k7c4	na
osobu	osoba	k1gFnSc4	osoba
pouze	pouze	k6eAd1	pouze
1	[number]	k4	1
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Vědecké	vědecký	k2eAgInPc1d1	vědecký
závěry	závěr	k1gInPc1	závěr
ohledně	ohledně	k7c2	ohledně
vlivu	vliv	k1gInSc2	vliv
cukru	cukr	k1gInSc2	cukr
na	na	k7c6	na
zdraví	zdraví	k1gNnSc6	zdraví
neumožňují	umožňovat	k5eNaImIp3nP	umožňovat
vyvodit	vyvodit	k5eAaBmF	vyvodit
jednoznačné	jednoznačný	k2eAgInPc4d1	jednoznačný
závěry	závěr	k1gInPc4	závěr
<g/>
.	.	kIx.	.
</s>
<s>
Metaanalýza	Metaanalýza	k1gFnSc1	Metaanalýza
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
ukázala	ukázat	k5eAaPmAgFnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
běžná	běžný	k2eAgFnSc1d1	běžná
konzumace	konzumace	k1gFnSc1	konzumace
cukru	cukr	k1gInSc2	cukr
nemá	mít	k5eNaImIp3nS	mít
zjistitelný	zjistitelný	k2eAgInSc4d1	zjistitelný
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
zdraví	zdraví	k1gNnSc4	zdraví
<g/>
.	.	kIx.	.
</s>
<s>
Nadměrná	nadměrný	k2eAgFnSc1d1	nadměrná
spotřeba	spotřeba	k1gFnSc1	spotřeba
cukru	cukr	k1gInSc2	cukr
<g/>
,	,	kIx,	,
sladkých	sladký	k2eAgInPc2d1	sladký
nápojů	nápoj	k1gInPc2	nápoj
a	a	k8xC	a
cukrovinek	cukrovinka	k1gFnPc2	cukrovinka
působí	působit	k5eAaImIp3nP	působit
řadu	řada	k1gFnSc4	řada
zdravotních	zdravotní	k2eAgFnPc2d1	zdravotní
potíží	potíž	k1gFnPc2	potíž
<g/>
:	:	kIx,	:
přispívá	přispívat	k5eAaImIp3nS	přispívat
k	k	k7c3	k
růstu	růst	k1gInSc3	růst
obezity	obezita	k1gFnSc2	obezita
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
u	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
všechny	všechen	k3xTgInPc4	všechen
cukry	cukr	k1gInPc4	cukr
difundují	difundovat	k5eAaImIp3nP	difundovat
do	do	k7c2	do
zubní	zubní	k2eAgFnSc2d1	zubní
skloviny	sklovina	k1gFnSc2	sklovina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
odbourávají	odbourávat	k5eAaImIp3nP	odbourávat
na	na	k7c4	na
kyseliny	kyselina	k1gFnPc4	kyselina
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
mohou	moct	k5eAaImIp3nP	moct
snižovat	snižovat	k5eAaImF	snižovat
obsah	obsah	k1gInSc4	obsah
vápníku	vápník	k1gInSc2	vápník
<g/>
.	.	kIx.	.
</s>
<s>
Podporují	podporovat	k5eAaImIp3nP	podporovat
tak	tak	k9	tak
bakterie	bakterie	k1gFnPc1	bakterie
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
zubní	zubní	k2eAgInSc4d1	zubní
kaz	kaz	k1gInSc4	kaz
(	(	kIx(	(
<g/>
caries	caries	k1gInSc4	caries
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
cukr	cukr	k1gInSc4	cukr
zrychluje	zrychlovat	k5eAaImIp3nS	zrychlovat
stárnutí	stárnutí	k1gNnSc1	stárnutí
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Trvale	trvale	k6eAd1	trvale
zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
hladina	hladina	k1gFnSc1	hladina
glukózy	glukóza	k1gFnSc2	glukóza
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
vznik	vznik	k1gInSc1	vznik
produktů	produkt	k1gInPc2	produkt
pokročilé	pokročilý	k2eAgFnSc2d1	pokročilá
glykace	glykace	k1gFnSc2	glykace
AGE	AGE	kA	AGE
(	(	kIx(	(
<g/>
Advanced	Advanced	k1gInSc1	Advanced
glycation	glycation	k1gInSc1	glycation
end	end	k?	end
products	products	k1gInSc1	products
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
jejich	jejich	k3xOp3gNnSc2	jejich
působení	působení	k1gNnSc2	působení
se	se	k3xPyFc4	se
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
poškození	poškození	k1gNnSc1	poškození
tkání	tkáň	k1gFnPc2	tkáň
způsobené	způsobený	k2eAgFnPc4d1	způsobená
volnými	volný	k2eAgInPc7d1	volný
radikály	radikál	k1gInPc7	radikál
<g/>
.	.	kIx.	.
</s>
<s>
Cukrovka	cukrovka	k1gFnSc1	cukrovka
(	(	kIx(	(
<g/>
diabetes	diabetes	k1gInSc1	diabetes
mellitus	mellitus	k1gInSc1	mellitus
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
zvýšenou	zvýšený	k2eAgFnSc7d1	zvýšená
hladinou	hladina	k1gFnSc7	hladina
glukózy	glukóza	k1gFnSc2	glukóza
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
převažující	převažující	k2eAgFnSc1d1	převažující
forma	forma	k1gFnSc1	forma
cukrovka	cukrovka	k1gFnSc1	cukrovka
2	[number]	k4	2
<g/>
.	.	kIx.	.
typu	typ	k1gInSc2	typ
je	být	k5eAaImIp3nS	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
především	především	k6eAd1	především
obezitou	obezita	k1gFnSc7	obezita
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
způsobenou	způsobený	k2eAgFnSc4d1	způsobená
vysokým	vysoký	k2eAgInSc7d1	vysoký
příjmem	příjem	k1gInSc7	příjem
cukru	cukr	k1gInSc2	cukr
<g/>
.	.	kIx.	.
</s>
<s>
Nemocní	mocnit	k5eNaImIp3nS	mocnit
cukrovkou	cukrovka	k1gFnSc7	cukrovka
čili	čili	k8xC	čili
diabetici	diabetik	k1gMnPc1	diabetik
musí	muset	k5eAaImIp3nP	muset
užívání	užívání	k1gNnSc4	užívání
cukru	cukr	k1gInSc2	cukr
silně	silně	k6eAd1	silně
omezovat	omezovat	k5eAaImF	omezovat
a	a	k8xC	a
často	často	k6eAd1	často
jej	on	k3xPp3gMnSc4	on
nahrazují	nahrazovat	k5eAaImIp3nP	nahrazovat
náhradními	náhradní	k2eAgNnPc7d1	náhradní
sladidly	sladidlo	k1gNnPc7	sladidlo
<g/>
.	.	kIx.	.
</s>
<s>
Syntetická	syntetický	k2eAgNnPc1d1	syntetické
i	i	k8xC	i
přírodní	přírodní	k2eAgNnPc1d1	přírodní
náhradní	náhradní	k2eAgNnPc1d1	náhradní
sladidla	sladidlo	k1gNnPc1	sladidlo
mají	mít	k5eAaImIp3nP	mít
nižší	nízký	k2eAgFnSc4d2	nižší
kalorickou	kalorický	k2eAgFnSc4d1	kalorická
hodnotu	hodnota	k1gFnSc4	hodnota
než	než	k8xS	než
cukr	cukr	k1gInSc4	cukr
a	a	k8xC	a
nepřispívají	přispívat	k5eNaImIp3nP	přispívat
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
zubního	zubní	k2eAgInSc2d1	zubní
kazu	kaz	k1gInSc2	kaz
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
často	často	k6eAd1	často
užívají	užívat	k5eAaImIp3nP	užívat
i	i	k9	i
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
zdravé	zdravý	k2eAgFnSc2d1	zdravá
výživy	výživa	k1gFnSc2	výživa
a	a	k8xC	a
různých	různý	k2eAgFnPc2d1	různá
diet	dieta	k1gFnPc2	dieta
<g/>
.	.	kIx.	.
</s>
<s>
Doporučení	doporučení	k1gNnSc1	doporučení
na	na	k7c6	na
omezení	omezení	k1gNnSc6	omezení
cukru	cukr	k1gInSc2	cukr
však	však	k9	však
nejsou	být	k5eNaImIp3nP	být
dobře	dobře	k6eAd1	dobře
vědecky	vědecky	k6eAd1	vědecky
podloženy	podložit	k5eAaPmNgFnP	podložit
a	a	k8xC	a
vedou	vést	k5eAaImIp3nP	vést
ke	k	k7c3	k
konzumaci	konzumace	k1gFnSc3	konzumace
náhrad	náhrada	k1gFnPc2	náhrada
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
i	i	k9	i
více	hodně	k6eAd2	hodně
škodí	škodit	k5eAaImIp3nS	škodit
zdraví	zdraví	k1gNnSc4	zdraví
<g/>
.	.	kIx.	.
</s>
<s>
Zvýšený	zvýšený	k2eAgInSc4d1	zvýšený
cukr	cukr	k1gInSc4	cukr
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
naopak	naopak	k6eAd1	naopak
snižuje	snižovat	k5eAaImIp3nS	snižovat
riziko	riziko	k1gNnSc4	riziko
jistých	jistý	k2eAgFnPc2d1	jistá
rakovin	rakovina	k1gFnPc2	rakovina
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
pro	pro	k7c4	pro
cukr	cukr	k1gInSc4	cukr
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
evropských	evropský	k2eAgInPc6d1	evropský
jazycích	jazyk	k1gInPc6	jazyk
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
z	z	k7c2	z
arabského	arabský	k2eAgInSc2d1	arabský
sukkar	sukkar	k1gInSc1	sukkar
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ze	z	k7c2	z
sanskrtského	sanskrtský	k2eAgInSc2d1	sanskrtský
śarkarā	śarkarā	k?	śarkarā
(	(	kIx(	(
<g/>
=	=	kIx~	=
sladký	sladký	k2eAgInSc4d1	sladký
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
6000	[number]	k4	6000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
pěstování	pěstování	k1gNnSc1	pěstování
cukrové	cukrový	k2eAgFnSc2d1	cukrová
třtiny	třtina	k1gFnSc2	třtina
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
z	z	k7c2	z
Melanésie	Melanésie	k1gFnSc2	Melanésie
do	do	k7c2	do
Indie	Indie	k1gFnSc2	Indie
a	a	k8xC	a
Persie	Persie	k1gFnSc2	Persie
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Indie	Indie	k1gFnSc2	Indie
pocházejí	pocházet	k5eAaImIp3nP	pocházet
i	i	k9	i
první	první	k4xOgFnPc1	první
zmínky	zmínka	k1gFnPc1	zmínka
o	o	k7c6	o
vaření	vaření	k1gNnSc6	vaření
sladké	sladký	k2eAgFnSc2d1	sladká
šťávy	šťáva	k1gFnSc2	šťáva
z	z	k7c2	z
cukrové	cukrový	k2eAgFnSc2d1	cukrová
třtiny	třtina	k1gFnSc2	třtina
v	v	k7c6	v
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
se	se	k3xPyFc4	se
v	v	k7c6	v
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
tato	tento	k3xDgFnSc1	tento
technika	technika	k1gFnSc1	technika
přenesla	přenést	k5eAaPmAgFnS	přenést
do	do	k7c2	do
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Zahuštěná	zahuštěný	k2eAgFnSc1d1	zahuštěná
šťáva	šťáva	k1gFnSc1	šťáva
se	se	k3xPyFc4	se
nalévala	nalévat	k5eAaImAgFnS	nalévat
do	do	k7c2	do
kuželovitých	kuželovitý	k2eAgFnPc2d1	kuželovitá
dřevěných	dřevěný	k2eAgFnPc2d1	dřevěná
nebo	nebo	k8xC	nebo
hliněných	hliněný	k2eAgFnPc2d1	hliněná
nádob	nádoba	k1gFnPc2	nádoba
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
krystalizovala	krystalizovat	k5eAaImAgFnS	krystalizovat
<g/>
;	;	kIx,	;
tak	tak	k6eAd1	tak
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
homole	homole	k1gFnPc1	homole
cukru	cukr	k1gInSc2	cukr
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
se	se	k3xPyFc4	se
saccharum	saccharum	k1gNnSc1	saccharum
dováželo	dovážet	k5eAaImAgNnS	dovážet
už	už	k6eAd1	už
v	v	k7c6	v
pozdní	pozdní	k2eAgFnSc6d1	pozdní
antice	antika	k1gFnSc6	antika
jako	jako	k8xC	jako
lahůdka	lahůdka	k1gFnSc1	lahůdka
a	a	k8xC	a
luxusní	luxusní	k2eAgNnSc1d1	luxusní
zboží	zboží	k1gNnSc1	zboží
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
Říma	Řím	k1gInSc2	Řím
tento	tento	k3xDgInSc1	tento
obchod	obchod	k1gInSc1	obchod
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
a	a	k8xC	a
cukr	cukr	k1gInSc4	cukr
přivezli	přivézt	k5eAaPmAgMnP	přivézt
až	až	k9	až
účastníci	účastník	k1gMnPc1	účastník
křížových	křížový	k2eAgFnPc2d1	křížová
výprav	výprava	k1gFnPc2	výprava
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1100	[number]	k4	1100
a	a	k8xC	a
Benátčané	Benátčan	k1gMnPc1	Benátčan
začali	začít	k5eAaPmAgMnP	začít
třtinu	třtina	k1gFnSc4	třtina
pěstovat	pěstovat	k5eAaImF	pěstovat
v	v	k7c6	v
Libanonu	Libanon	k1gInSc6	Libanon
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
cukrová	cukrový	k2eAgFnSc1d1	cukrová
třtina	třtina	k1gFnSc1	třtina
pěstovala	pěstovat	k5eAaImAgFnS	pěstovat
v	v	k7c6	v
koloniích	kolonie	k1gFnPc6	kolonie
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
a	a	k8xC	a
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Karibské	karibský	k2eAgFnSc6d1	karibská
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
cukr	cukr	k1gInSc1	cukr
však	však	k9	však
zůstával	zůstávat	k5eAaImAgInS	zůstávat
luxusním	luxusní	k2eAgNnSc7d1	luxusní
zbožím	zboží	k1gNnSc7	zboží
a	a	k8xC	a
prostí	prostý	k2eAgMnPc1d1	prostý
lidé	člověk	k1gMnPc1	člověk
dál	daleko	k6eAd2	daleko
sladili	sladit	k5eAaImAgMnP	sladit
medem	med	k1gInSc7	med
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1800	[number]	k4	1800
se	se	k3xPyFc4	se
na	na	k7c6	na
světě	svět	k1gInSc6	svět
vyrábělo	vyrábět	k5eAaImAgNnS	vyrábět
asi	asi	k9	asi
180	[number]	k4	180
000	[number]	k4	000
tun	tuna	k1gFnPc2	tuna
surového	surový	k2eAgInSc2d1	surový
cukru	cukr	k1gInSc2	cukr
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1590	[number]	k4	1590
vyluhoval	vyluhovat	k5eAaPmAgMnS	vyluhovat
francouzský	francouzský	k2eAgMnSc1d1	francouzský
botanik	botanik	k1gMnSc1	botanik
Olivier	Olivier	k1gMnSc1	Olivier
de	de	k?	de
Serres	Serres	k1gMnSc1	Serres
cukr	cukr	k1gInSc4	cukr
z	z	k7c2	z
cukrové	cukrový	k2eAgFnSc2d1	cukrová
řepy	řepa	k1gFnSc2	řepa
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1747	[number]	k4	1747
vynalezl	vynaleznout	k5eAaPmAgInS	vynaleznout
lepší	dobrý	k2eAgInSc1d2	lepší
postup	postup	k1gInSc1	postup
berlínský	berlínský	k2eAgMnSc1d1	berlínský
chemik	chemik	k1gMnSc1	chemik
A.	A.	kA	A.
S.	S.	kA	S.
Marggraf	Marggraf	k1gMnSc1	Marggraf
<g/>
.	.	kIx.	.
</s>
<s>
Pěstování	pěstování	k1gNnSc1	pěstování
cukrové	cukrový	k2eAgFnSc2d1	cukrová
řepy	řepa	k1gFnSc2	řepa
se	se	k3xPyFc4	se
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
až	až	k9	až
za	za	k7c2	za
napoleonských	napoleonský	k2eAgFnPc2d1	napoleonská
válek	válka	k1gFnPc2	válka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Británie	Británie	k1gFnSc1	Británie
blokovala	blokovat	k5eAaImAgFnS	blokovat
kontinentální	kontinentální	k2eAgInPc4d1	kontinentální
přístavy	přístav	k1gInPc4	přístav
a	a	k8xC	a
dovoz	dovoz	k1gInSc4	dovoz
třtinového	třtinový	k2eAgInSc2d1	třtinový
cukru	cukr	k1gInSc2	cukr
<g/>
.	.	kIx.	.
</s>
<s>
Výrobu	výroba	k1gFnSc4	výroba
řepného	řepný	k2eAgInSc2d1	řepný
cukru	cukr	k1gInSc2	cukr
zavedl	zavést	k5eAaPmAgMnS	zavést
roku	rok	k1gInSc2	rok
1802	[number]	k4	1802
F.	F.	kA	F.
C.	C.	kA	C.
Achard	Achard	k1gMnSc1	Achard
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Cunern	Cunerna	k1gFnPc2	Cunerna
v	v	k7c6	v
Pruském	pruský	k2eAgNnSc6d1	pruské
Dolním	dolní	k2eAgNnSc6d1	dolní
Slezsku	Slezsko	k1gNnSc6	Slezsko
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Kunary	Kunara	k1gFnPc1	Kunara
<g/>
,	,	kIx,	,
obec	obec	k1gFnSc1	obec
Wińsk	Wińsk	k1gInSc1	Wińsk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
první	první	k4xOgInSc1	první
cukrovar	cukrovar	k1gInSc1	cukrovar
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
výrobu	výroba	k1gFnSc4	výroba
cukru	cukr	k1gInSc2	cukr
v	v	k7c6	v
Českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
zahájil	zahájit	k5eAaPmAgMnS	zahájit
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1787	[number]	k4	1787
Belgičan	Belgičan	k1gMnSc1	Belgičan
Sauvagne	Sauvagn	k1gInSc5	Sauvagn
v	v	k7c6	v
bývalém	bývalý	k2eAgInSc6d1	bývalý
konventu	konvent	k1gInSc6	konvent
kláštera	klášter	k1gInSc2	klášter
na	na	k7c6	na
Zbraslavi	Zbraslav	k1gFnSc6	Zbraslav
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
cukrovar	cukrovar	k1gInSc1	cukrovar
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1825	[number]	k4	1825
obnovil	obnovit	k5eAaPmAgMnS	obnovit
Antonín	Antonín	k1gMnSc1	Antonín
Richter	Richter	k1gMnSc1	Richter
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1800	[number]	k4	1800
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
cukrovar	cukrovar	k1gInSc1	cukrovar
v	v	k7c6	v
Hořovicích	Hořovice	k1gFnPc6	Hořovice
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
cukrovary	cukrovar	k1gInPc1	cukrovar
na	na	k7c4	na
cukrovou	cukrový	k2eAgFnSc4d1	cukrová
řepu	řepa	k1gFnSc4	řepa
či	či	k8xC	či
bramborový	bramborový	k2eAgInSc4d1	bramborový
škrob	škrob	k1gInSc4	škrob
vznikaly	vznikat	k5eAaImAgInP	vznikat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1810	[number]	k4	1810
(	(	kIx(	(
<g/>
Žaky	žako	k1gMnPc4	žako
u	u	k7c2	u
Čáslavi	Čáslav	k1gFnSc2	Čáslav
<g/>
,	,	kIx,	,
Liběchov	Liběchov	k1gInSc1	Liběchov
<g/>
,	,	kIx,	,
Semily	Semily	k1gInPc1	Semily
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Napoleonově	Napoleonův	k2eAgInSc6d1	Napoleonův
pádu	pád	k1gInSc6	pád
většinou	většina	k1gFnSc7	většina
zase	zase	k9	zase
zanikly	zaniknout	k5eAaPmAgFnP	zaniknout
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1829	[number]	k4	1829
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
cukrovar	cukrovar	k1gInSc1	cukrovar
v	v	k7c6	v
Kostelním	kostelní	k2eAgMnSc6d1	kostelní
Vydří	vydří	k2eAgInPc4d1	vydří
(	(	kIx(	(
<g/>
1831	[number]	k4	1831
přenesen	přenést	k5eAaPmNgInS	přenést
do	do	k7c2	do
Dačic	Dačice	k1gFnPc2	Dačice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1830	[number]	k4	1830
v	v	k7c6	v
Dobrovici	Dobrovice	k1gFnSc6	Dobrovice
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
například	například	k6eAd1	například
v	v	k7c6	v
Sadské	sadský	k2eAgFnSc6d1	Sadská
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1846	[number]	k4	1846
v	v	k7c6	v
Pečkách	Pečky	k1gFnPc6	Pečky
<g/>
.	.	kIx.	.
</s>
<s>
Nárůst	nárůst	k1gInSc1	nárůst
byl	být	k5eAaImAgInS	být
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
státní	státní	k2eAgFnSc7d1	státní
podporou	podpora	k1gFnSc7	podpora
<g/>
:	:	kIx,	:
roku	rok	k1gInSc2	rok
1830	[number]	k4	1830
Rakousko	Rakousko	k1gNnSc1	Rakousko
osvobodilo	osvobodit	k5eAaPmAgNnS	osvobodit
domácí	domácí	k2eAgFnSc4d1	domácí
výrobu	výroba	k1gFnSc4	výroba
řepného	řepný	k2eAgInSc2d1	řepný
cukru	cukr	k1gInSc2	cukr
od	od	k7c2	od
daní	daň	k1gFnPc2	daň
a	a	k8xC	a
začaly	začít	k5eAaPmAgFnP	začít
hojně	hojně	k6eAd1	hojně
vznikat	vznikat	k5eAaImF	vznikat
panské	panský	k2eAgInPc4d1	panský
cukrovary	cukrovar	k1gInPc4	cukrovar
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
cukr	cukr	k1gInSc1	cukr
stal	stát	k5eAaPmAgInS	stát
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
běžným	běžný	k2eAgNnSc7d1	běžné
zbožím	zboží	k1gNnSc7	zboží
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc7	jeho
spotřeba	spotřeba	k1gFnSc1	spotřeba
stále	stále	k6eAd1	stále
rostla	růst	k5eAaImAgFnS	růst
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
převážila	převážit	k5eAaPmAgFnS	převážit
světová	světový	k2eAgFnSc1d1	světová
produkce	produkce	k1gFnSc1	produkce
řepného	řepný	k2eAgInSc2d1	řepný
cukru	cukr	k1gInSc2	cukr
nad	nad	k7c7	nad
třtinovým	třtinový	k2eAgInSc7d1	třtinový
a	a	k8xC	a
v	v	k7c6	v
Rakousko-Uhersku	Rakousko-Uhersek	k1gInSc6	Rakousko-Uhersek
se	s	k7c7	s
88	[number]	k4	88
%	%	kIx~	%
cukru	cukr	k1gInSc2	cukr
vyrábělo	vyrábět	k5eAaImAgNnS	vyrábět
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
vývoj	vývoj	k1gInSc4	vývoj
přerušila	přerušit	k5eAaPmAgFnS	přerušit
první	první	k4xOgFnSc1	první
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
třtinový	třtinový	k2eAgInSc4d1	třtinový
cukr	cukr	k1gInSc4	cukr
opět	opět	k6eAd1	opět
převládl	převládnout	k5eAaPmAgMnS	převládnout
<g/>
.	.	kIx.	.
</s>
<s>
Cukr	cukr	k1gInSc1	cukr
se	se	k3xPyFc4	se
až	až	k6eAd1	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
dodával	dodávat	k5eAaImAgInS	dodávat
nejčastěji	často	k6eAd3	často
v	v	k7c6	v
odlévaných	odlévaný	k2eAgFnPc6d1	odlévaná
homolích	homole	k1gFnPc6	homole
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
domácnosti	domácnost	k1gFnSc6	domácnost
sekaly	sekat	k5eAaImAgFnP	sekat
na	na	k7c4	na
drobnější	drobný	k2eAgInPc4d2	drobnější
kousky	kousek	k1gInPc4	kousek
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1841	[number]	k4	1841
vyrobil	vyrobit	k5eAaPmAgMnS	vyrobit
cukrovarník	cukrovarník	k1gMnSc1	cukrovarník
Jakub	Jakub	k1gMnSc1	Jakub
Kryštof	Kryštof	k1gMnSc1	Kryštof
Rad	rad	k1gInSc4	rad
v	v	k7c6	v
Dačicích	Dačice	k1gFnPc6	Dačice
první	první	k4xOgInSc4	první
kostkový	kostkový	k2eAgInSc4d1	kostkový
cukr	cukr	k1gInSc4	cukr
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
homole	homole	k1gFnSc1	homole
postupně	postupně	k6eAd1	postupně
nahradil	nahradit	k5eAaPmAgInS	nahradit
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
cukr	cukr	k1gInSc1	cukr
běžně	běžně	k6eAd1	běžně
dodává	dodávat	k5eAaImIp3nS	dodávat
jako	jako	k9	jako
hrubší	hrubý	k2eAgInSc1d2	hrubší
a	a	k8xC	a
jemnější	jemný	k2eAgInSc1d2	jemnější
krystal	krystal	k1gInSc1	krystal
<g/>
,	,	kIx,	,
drcená	drcený	k2eAgFnSc1d1	drcená
krupice	krupice	k1gFnSc1	krupice
a	a	k8xC	a
jemně	jemně	k6eAd1	jemně
mletá	mletý	k2eAgFnSc1d1	mletá
moučka	moučka	k1gFnSc1	moučka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
se	se	k3xPyFc4	se
v	v	k7c6	v
letech	let	k1gInPc6	let
1990	[number]	k4	1990
<g/>
–	–	k?	–
<g/>
2010	[number]	k4	2010
vyrábělo	vyrábět	k5eAaImAgNnS	vyrábět
ročně	ročně	k6eAd1	ročně
400	[number]	k4	400
až	až	k9	až
500	[number]	k4	500
tisíc	tisíc	k4xCgInSc4	tisíc
tun	tuna	k1gFnPc2	tuna
bílého	bílý	k2eAgInSc2d1	bílý
cukru	cukr	k1gInSc2	cukr
<g/>
.	.	kIx.	.
</s>
