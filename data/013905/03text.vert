<s>
NGC	NGC	kA
541	#num#	k4
</s>
<s>
NGC	NGC	kA
541	#num#	k4
Snímek	snímek	k1gInSc1
z	z	k7c2
přehlídky	přehlídka	k1gFnSc2
SDSS	SDSS	kA
ukazuující	ukazuující	k2eAgInSc4d1
NGC	NGC	kA
541	#num#	k4
a	a	k8xC
její	její	k3xOp3gInSc1
útržek	útržek	k1gInSc1
zvaný	zvaný	k2eAgInSc1d1
Minkowského	Minkowského	k2eAgInSc4d1
objektPozorovací	objektPozorovací	k2eAgInSc4d1
údaje	údaj	k1gInPc4
<g/>
(	(	kIx(
<g/>
Ekvinokcium	ekvinokcium	k1gNnSc1
J	J	kA
<g/>
2000,0	2000,0	k4
<g/>
)	)	kIx)
Typ	typ	k1gInSc1
</s>
<s>
interagující	interagující	k2eAgFnSc1d1
galaxie	galaxie	k1gFnSc1
a	a	k8xC
čočková	čočkový	k2eAgFnSc1d1
galaxie	galaxie	k1gFnSc1
Třída	třída	k1gFnSc1
</s>
<s>
cD	cd	kA
<g/>
;	;	kIx,
<g/>
S	s	k7c7
<g/>
0	#num#	k4
<g/>
-	-	kIx~
Objevitel	objevitel	k1gMnSc1
</s>
<s>
Heinrich	Heinrich	k1gMnSc1
Louis	Louis	k1gMnSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
Arrest	Arrest	k1gMnSc1
Datum	datum	k1gInSc4
objevu	objev	k1gInSc2
</s>
<s>
30	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1864	#num#	k4
Rektascenze	rektascenze	k1gFnSc2
</s>
<s>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
h	h	k?
25	#num#	k4
<g/>
m	m	kA
44,3	44,3	k4
<g/>
s	s	k7c7
Deklinace	deklinace	k1gFnSc2
</s>
<s>
-0	-0	k4
<g/>
1	#num#	k4
<g/>
°	°	k?
<g/>
22	#num#	k4
<g/>
′	′	k?
<g/>
46	#num#	k4
<g/>
″	″	k?
Souhvězdí	souhvězdí	k1gNnSc2
</s>
<s>
Velryba	velryba	k1gFnSc1
(	(	kIx(
<g/>
lat.	lat.	k?
Cetus	Cetus	k1gInSc1
<g/>
)	)	kIx)
Zdánlivá	zdánlivý	k2eAgFnSc1d1
magnituda	magnituda	k1gFnSc1
(	(	kIx(
<g/>
V	V	kA
<g/>
)	)	kIx)
</s>
<s>
12,2	12,2	k4
Úhlová	úhlový	k2eAgFnSc1d1
velikost	velikost	k1gFnSc1
</s>
<s>
1,8	1,8	k4
<g/>
′	′	k?
×	×	k?
1,7	1,7	k4
<g/>
′	′	k?
Vzdálenost	vzdálenost	k1gFnSc1
</s>
<s>
245	#num#	k4
M	M	kA
ly	ly	k?
Plošná	plošný	k2eAgFnSc1d1
jasnost	jasnost	k1gFnSc1
</s>
<s>
13,5	13,5	k4
Poziční	poziční	k2eAgInSc4d1
úhel	úhel	k1gInSc4
</s>
<s>
54	#num#	k4
<g/>
°	°	k?
Rudý	rudý	k2eAgInSc1d1
posuv	posuv	k1gInSc1
</s>
<s>
0,018086	0,018086	k4
±	±	k?
0,000019	0,000019	k4
Kupa	kupa	k1gFnSc1
galaxií	galaxie	k1gFnPc2
</s>
<s>
Abell	Abell	k1gMnSc1
194	#num#	k4
Označení	označení	k1gNnSc1
v	v	k7c6
katalozích	katalog	k1gInPc6
New	New	k1gMnSc2
General	General	k1gMnSc2
Catalogue	Catalogu	k1gMnSc2
</s>
<s>
NGC	NGC	kA
541	#num#	k4
Uppsala	Uppsala	k1gFnSc1
General	General	k1gFnSc1
Catalogue	Catalogue	k1gFnSc1
</s>
<s>
UGC	UGC	kA
1004	#num#	k4
Principal	Principal	k1gFnSc2
Galaxies	Galaxiesa	k1gFnPc2
Catalogue	Catalogu	k1gFnSc2
</s>
<s>
PGC	PGC	kA
5305	#num#	k4
Atlas	Atlas	k1gInSc1
of	of	k?
Peculiar	Peculiar	k1gMnSc1
Galaxies	Galaxies	k1gMnSc1
</s>
<s>
APG	APG	kA
133	#num#	k4
Jiná	jiná	k1gFnSc1
označení	označení	k1gNnSc2
</s>
<s>
NGC	NGC	kA
541	#num#	k4
<g/>
,	,	kIx,
PGC	PGC	kA
5305	#num#	k4
<g/>
,	,	kIx,
UGC	UGC	kA
1004	#num#	k4
<g/>
,	,	kIx,
MCG	MCG	kA
+0	+0	k4
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
137	#num#	k4
<g/>
,	,	kIx,
GC	GC	kA
5178	#num#	k4
<g/>
,	,	kIx,
Arp	Arp	k1gFnSc1
133	#num#	k4
<g/>
,	,	kIx,
CGCG	CGCG	kA
385-128	385-128	k4
,	,	kIx,
2MASX	2MASX	k4
J	J	kA
<g/>
0	#num#	k4
<g/>
1254430	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
122461	#num#	k4
<g/>
,	,	kIx,
LDCE	LDCE	kA
89	#num#	k4
NED014	NED014	k1gFnSc2
</s>
<s>
(	(	kIx(
<g/>
V	V	kA
<g/>
)	)	kIx)
–	–	k?
měření	měření	k1gNnSc2
provedena	proveden	k2eAgFnSc1d1
ve	v	k7c6
viditelném	viditelný	k2eAgNnSc6d1
světle	světlo	k1gNnSc6
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
NGC	NGC	kA
541	#num#	k4
je	být	k5eAaImIp3nS
eliptická	eliptický	k2eAgFnSc1d1
galaxie	galaxie	k1gFnSc1
v	v	k7c6
souhvězdí	souhvězdí	k1gNnSc6
Velryba	velryba	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc7
zdánlivá	zdánlivý	k2eAgFnSc1d1
jasnost	jasnost	k1gFnSc1
je	být	k5eAaImIp3nS
12,2	12,2	k4
<g/>
m	m	kA
a	a	k8xC
úhlová	úhlový	k2eAgFnSc1d1
velikost	velikost	k1gFnSc1
1,8	1,8	k4
<g/>
′	′	k?
×	×	k?
1,7	1,7	k4
<g/>
′	′	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Je	být	k5eAaImIp3nS
vzdálená	vzdálený	k2eAgFnSc1d1
245	#num#	k4
milionů	milion	k4xCgInPc2
světelných	světelný	k2eAgFnPc2d1
let	léto	k1gNnPc2
<g/>
,	,	kIx,
průměr	průměr	k1gInSc1
má	mít	k5eAaImIp3nS
130	#num#	k4
000	#num#	k4
světelných	světelný	k2eAgNnPc2d1
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Galaxie	galaxie	k1gFnSc1
je	být	k5eAaImIp3nS
zařazena	zařadit	k5eAaPmNgFnS
do	do	k7c2
katalogu	katalog	k1gInSc2
pekuliárních	pekuliární	k2eAgFnPc2d1
galaxií	galaxie	k1gFnPc2
pod	pod	k7c7
označením	označení	k1gNnSc7
Arp	Arp	k1gFnSc2
133	#num#	k4
jako	jako	k8xS,k8xC
příklad	příklad	k1gInSc1
galaxie	galaxie	k1gFnSc2
s	s	k7c7
blízkými	blízký	k2eAgInPc7d1
útržky	útržek	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
NGC	NGC	kA
541	#num#	k4
je	být	k5eAaImIp3nS
rádiový	rádiový	k2eAgInSc1d1
zdroj	zdroj	k1gInSc1
3C	3C	k4
40	#num#	k4
<g/>
A	A	kA
<g/>
,	,	kIx,
podle	podle	k7c2
Fanaroffovy-Rileyho	Fanaroffovy-Riley	k1gMnSc2
klasifikace	klasifikace	k1gFnSc2
v	v	k7c6
třídě	třída	k1gFnSc6
I	I	kA
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
45	#num#	k4
<g/>
″	″	k?
severovýchodně	severovýchodně	k6eAd1
od	od	k7c2
galaxie	galaxie	k1gFnSc2
NGC	NGC	kA
541	#num#	k4
leží	ležet	k5eAaImIp3nS
nepravidelná	pravidelný	k2eNgFnSc1d1
trpasličí	trpasličí	k2eAgFnSc1d1
galaxie	galaxie	k1gFnSc1
zvaná	zvaný	k2eAgFnSc1d1
Minkowského	Minkowského	k2eAgFnSc1d1
objekt	objekt	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Galaxie	galaxie	k1gFnSc1
je	být	k5eAaImIp3nS
členem	člen	k1gMnSc7
kupy	kupa	k1gFnSc2
galaxií	galaxie	k1gFnPc2
Abell	Abell	k1gMnSc1
194	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
NGC	NGC	kA
541	#num#	k4
a	a	k8xC
párem	pár	k1gInSc7
galaxií	galaxie	k1gFnPc2
NGC	NGC	kA
545	#num#	k4
a	a	k8xC
NGC	NGC	kA
547	#num#	k4
byl	být	k5eAaImAgInS
zjištěn	zjistit	k5eAaPmNgInS
hvězdný	hvězdný	k2eAgInSc1d1
pás	pás	k1gInSc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
leží	ležet	k5eAaImIp3nS
na	na	k7c6
obloze	obloha	k1gFnSc6
v	v	k7c6
úhlové	úhlový	k2eAgFnSc6d1
vzdálenosti	vzdálenost	k1gFnSc6
4,5	4,5	k4
vteřiny	vteřina	k1gFnSc2
severovýchodně	severovýchodně	k6eAd1
od	od	k7c2
NGC	NGC	kA
541	#num#	k4
a	a	k8xC
odpovídá	odpovídat	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
325	#num#	k4
000	#num#	k4
světelných	světelný	k2eAgNnPc2d1
let	léto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Galaxii	galaxie	k1gFnSc4
objevil	objevit	k5eAaPmAgMnS
30	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1864	#num#	k4
Heinrich	Heinrich	k1gMnSc1
Louis	Louis	k1gMnSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
Arrest	Arrest	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
3C	3C	k4
40B	40B	k4
významnější	významný	k2eAgInSc4d2
a	a	k8xC
spojený	spojený	k2eAgInSc4d1
s	s	k7c7
blízkou	blízký	k2eAgFnSc7d1
galaxií	galaxie	k1gFnSc7
NGC	NGC	kA
547	#num#	k4
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
FROMMERT	FROMMERT	kA
<g/>
,	,	kIx,
Hartmut	Hartmut	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Revised	Revised	k1gInSc1
NGC	NGC	kA
Data	datum	k1gNnPc1
for	forum	k1gNnPc2
NGC	NGC	kA
541	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
SEDS	SEDS	kA
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
CROFT	CROFT	kA
<g/>
,	,	kIx,
Steve	Steve	k1gMnSc1
<g/>
;	;	kIx,
VAN	van	k1gInSc1
BREUGEL	BREUGEL	kA
<g/>
,	,	kIx,
Wil	Wil	k1gFnSc1
<g/>
;	;	kIx,
DE	DE	k?
VRIES	VRIES	kA
<g/>
,	,	kIx,
Wim	Wim	k1gFnSc1
<g/>
,	,	kIx,
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Minkowski	Minkowski	k1gNnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Object	Objecta	k1gFnPc2
<g/>
:	:	kIx,
A	a	k9
Starburst	Starburst	k1gMnSc1
Triggered	Triggered	k1gMnSc1
by	by	kYmCp3nS
a	a	k9
Radio	radio	k1gNnSc4
Jet	jet	k5eAaImF
<g/>
,	,	kIx,
Revisited	Revisited	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
1040	#num#	k4
<g/>
–	–	k?
<g/>
1055	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Astrophysical	Astrophysical	k1gFnSc1
Journal	Journal	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Srpen	srpen	k1gInSc1
2006	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
647	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
2	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1040	#num#	k4
<g/>
–	–	k?
<g/>
1055	#num#	k4
<g/>
.	.	kIx.
arXiv	arXiv	k6eAd1
<g/>
:	:	kIx,
<g/>
astro-ph	astro-ph	k1gInSc1
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
604557	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.108	10.108	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
505526	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bibcode	Bibcod	k1gInSc5
<g/>
:	:	kIx,
<g/>
2006	#num#	k4
<g/>
ApJ	ApJ	k1gFnPc2
<g/>
..	..	k?
<g/>
.647	.647	k4
<g/>
.1040	.1040	k4
<g/>
C.	C.	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
VERDOES	VERDOES	kA
KLEIJN	KLEIJN	kA
<g/>
,	,	kIx,
Gijs	Gijs	k1gInSc1
A.	A.	kA
<g/>
;	;	kIx,
BAUM	BAUM	kA
<g/>
,	,	kIx,
Stefi	Stef	k1gInPc7
A.	A.	kA
<g/>
;	;	kIx,
DE	DE	k?
ZEEUW	ZEEUW	kA
<g/>
,	,	kIx,
P.	P.	kA
Tim	Tim	k?
<g/>
,	,	kIx,
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hubble	Hubble	k1gMnSc1
Space	Space	k1gMnSc1
Telescope	Telescop	k1gInSc5
Observations	Observations	k1gInSc4
of	of	k?
Nearby	Nearba	k1gFnSc2
Radio-Loud	Radio-Loud	k1gMnSc1
Early-Type	Early-Typ	k1gInSc5
Galaxies	Galaxies	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
2592	#num#	k4
<g/>
–	–	k?
<g/>
2617	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Astronomical	Astronomical	k1gFnSc1
Journal	Journal	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prosinec	prosinec	k1gInSc1
1999	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
118	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
6	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
2592	#num#	k4
<g/>
–	–	k?
<g/>
2617	#num#	k4
<g/>
.	.	kIx.
arXiv	arXiv	k6eAd1
<g/>
:	:	kIx,
<g/>
astro-ph	astro-ph	k1gInSc1
<g/>
/	/	kIx~
<g/>
9909256	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.108	10.108	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
301135	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bibcode	Bibcod	k1gInSc5
<g/>
:	:	kIx,
<g/>
1999	#num#	k4
<g/>
AJ	aj	kA
<g/>
...	...	k?
<g/>
.118	.118	k4
<g/>
.2592	.2592	k4
<g/>
V.	V.	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
SELIGMAN	SELIGMAN	kA
<g/>
,	,	kIx,
Courtney	Courtney	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celestial	Celestial	k1gMnSc1
Atlas	Atlas	k1gMnSc1
<g/>
:	:	kIx,
NGC	NGC	kA
541	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
NGC	NGC	kA
541	#num#	k4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
NASA	NASA	kA
<g/>
/	/	kIx~
<g/>
IPAC	IPAC	kA
Extragalactic	Extragalactice	k1gFnPc2
Database	Databasa	k1gFnSc3
<g/>
:	:	kIx,
Results	Resultsa	k1gFnPc2
for	forum	k1gNnPc2
NGC	NGC	kA
541	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
FROMMERT	FROMMERT	kA
<g/>
,	,	kIx,
Hartmut	Hartmut	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Revised	Revised	k1gInSc1
NGC	NGC	kA
Data	datum	k1gNnPc1
for	forum	k1gNnPc2
NGC	NGC	kA
541	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
SEDS	SEDS	kA
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
SIMBAD	SIMBAD	kA
Astronomical	Astronomical	k1gFnSc1
Database	Databasa	k1gFnSc3
<g/>
:	:	kIx,
Results	Resultsa	k1gFnPc2
for	forum	k1gNnPc2
NGC	NGC	kA
541	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
SELIGMAN	SELIGMAN	kA
<g/>
,	,	kIx,
Courtney	Courtney	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celestial	Celestial	k1gMnSc1
Atlas	Atlas	k1gMnSc1
<g/>
:	:	kIx,
NGC	NGC	kA
541	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Katalog	katalog	k1gInSc1
NGC	NGC	kA
541	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
body	bod	k1gInPc1
<g/>
.	.	kIx.
<g/>
skin-vector	skin-vector	k1gInSc1
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
span	span	k1gInSc1
<g/>
.	.	kIx.
<g/>
souradnice-vesmir	souradnice-vesmir	k1gInSc1
<g/>
,	,	kIx,
<g/>
body	bod	k1gInPc1
<g/>
.	.	kIx.
<g/>
skin-monobook	skin-monobook	k1gInSc1
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gMnSc1
span	span	k1gMnSc1
<g/>
.	.	kIx.
<g/>
souradnice-vesmir	souradnice-vesmir	k1gMnSc1
<g/>
{	{	kIx(
<g/>
background	background	k1gMnSc1
<g/>
:	:	kIx,
<g/>
url	url	k?
<g/>
(	(	kIx(
<g/>
"	"	kIx"
<g/>
//	//	k?
<g/>
upload	upload	k1gInSc1
<g/>
.	.	kIx.
<g/>
wikimedia	wikimedium	k1gNnSc2
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
org	org	k?
<g/>
/	/	kIx~
<g/>
wikipedia	wikipedium	k1gNnSc2
<g/>
/	/	kIx~
<g/>
commons	commons	k6eAd1
<g/>
/	/	kIx~
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
70	#num#	k4
<g/>
/	/	kIx~
<g/>
Skymap	Skymap	k1gInSc1
<g/>
.	.	kIx.
<g/>
png	png	k?
<g/>
"	"	kIx"
<g/>
)	)	kIx)
<g/>
right	right	k2eAgInSc1d1
no-repeat	no-repeat	k1gInSc1
<g/>
;	;	kIx,
<g/>
padding-right	padding-right	k1gInSc1
<g/>
:	:	kIx,
<g/>
28	#num#	k4
<g/>
px	px	k?
<g/>
}	}	kIx)
<g/>
body	bod	k1gInPc4
<g/>
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
skin-vector	skin-vector	k1gInSc1
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
span	span	k1gInSc1
<g/>
.	.	kIx.
<g/>
souradnice-blok	souradnice-blok	k1gInSc1
sup	sup	k1gMnSc1
<g/>
,	,	kIx,
<g/>
body	bod	k1gInPc1
<g/>
.	.	kIx.
<g/>
skin-vector	skin-vector	k1gInSc1
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
span	span	k1gInSc1
<g/>
.	.	kIx.
<g/>
souradnice-inline	souradnice-inlin	k1gInSc5
sup	sup	k1gMnSc1
<g/>
,	,	kIx,
<g/>
body	bod	k1gInPc1
<g/>
.	.	kIx.
<g/>
skin-monobook	skin-monobook	k1gInSc1
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
span	span	k1gInSc1
<g/>
.	.	kIx.
<g/>
souradnice-blok	souradnice-blok	k1gInSc1
sup	sup	k1gMnSc1
<g/>
,	,	kIx,
<g/>
body	bod	k1gInPc1
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
skin-monobook	skin-monobook	k1gInSc1
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
span	span	k1gInSc1
<g/>
.	.	kIx.
<g/>
souradnice-inline	souradnice-inlin	k1gInSc5
sup	sup	k1gMnSc1
<g/>
{	{	kIx(
<g/>
vertical-align	vertical-align	k1gNnSc1
<g/>
:	:	kIx,
<g/>
top	topit	k5eAaImRp2nS
<g/>
}	}	kIx)
<g/>
body	bod	k1gInPc4
<g/>
.	.	kIx.
<g/>
skin-vector	skin-vector	k1gMnSc1
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gMnSc1
span	span	k1gMnSc1
<g/>
.	.	kIx.
<g/>
souradnice-blok	souradnice-blok	k1gInSc1
<g/>
,	,	kIx,
<g/>
body	bod	k1gInPc1
<g/>
.	.	kIx.
<g/>
skin-monobook	skin-monobook	k1gInSc1
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
span	span	k1gInSc1
<g/>
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
souradnice-blok	souradnice-blok	k1gInSc1
<g/>
{	{	kIx(
<g/>
font-family	font-famit	k5eAaPmAgFnP
<g/>
:	:	kIx,
<g/>
monospace	monospace	k1gFnSc1
<g/>
;	;	kIx,
<g/>
font-size	font-size	k1gFnSc1
<g/>
:	:	kIx,
<g/>
85	#num#	k4
<g/>
%	%	kIx~
<g/>
}	}	kIx)
<g/>
body	bod	k1gInPc4
<g/>
.	.	kIx.
<g/>
skin-vector	skin-vector	k1gMnSc1
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gMnSc1
span	span	k1gMnSc1
<g/>
.	.	kIx.
<g/>
souradnice-inline	souradnice-inlin	k1gInSc5
<g/>
,	,	kIx,
<g/>
body	bod	k1gInPc1
<g/>
.	.	kIx.
<g/>
skin-monobook	skin-monobook	k1gInSc1
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
span	span	k1gInSc1
<g/>
.	.	kIx.
<g/>
souradnice-inline	souradnice-inlin	k1gInSc5
<g/>
{	{	kIx(
<g/>
background	background	k1gInSc1
<g/>
:	:	kIx,
<g/>
none	none	k1gInSc1
<g/>
;	;	kIx,
<g/>
padding-right	padding-right	k1gInSc1
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
}	}	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Astronomie	astronomie	k1gFnSc1
</s>
