<s>
Kang	Kang	k1gInSc1
Pan-sok	Pan-sok	k1gInSc1
</s>
<s>
Kang	Kang	k1gInSc1
Pan-sok	Pan-sok	k1gInSc1
je	být	k5eAaImIp3nS
korejské	korejský	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
Kang	Kang	k1gMnSc1
je	být	k5eAaImIp3nS
příjmení	příjmení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Kang	Kang	k1gMnSc1
Pan-sok	Pan-sok	k1gInSc4
Narození	narození	k1gNnSc2
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1892	#num#	k4
<g/>
Chilgol	Chilgola	k1gFnPc2
<g/>
,	,	kIx,
Mangjŏ	Mangjŏ	k1gFnPc2
Korejské	korejský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
Korejské	korejský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
Úmrtí	úmrtí	k1gNnSc1
</s>
<s>
31	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1932	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
40	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Ťi-lin	Ťi-lin	k1gInSc1
Čínská	čínský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Čínská	čínský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Manžel	manžel	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ka	ka	k?
<g/>
)	)	kIx)
</s>
<s>
Kim	Kim	k?
Hjong-džik	Hjong-džik	k1gInSc1
Děti	dítě	k1gFnPc1
</s>
<s>
Kim	Kim	k?
Ir-senKim	Ir-senKim	k1gMnSc1
Čchol-džuKim	Čchol-džuKim	k1gMnSc1
Jong-džu	Jong-džu	k1gMnSc3
Rodiče	rodič	k1gMnPc4
</s>
<s>
Kang	Kang	k1gMnSc1
Dunyu	Dunyus	k1gInSc2
Wei	Wei	k1gMnSc1
Dunxin	Dunxin	k1gMnSc1
</s>
<s>
Kang	Kang	k1gMnSc1
Pan-sŏ	Pan-sŏ	k1gMnSc1
(	(	kIx(
<g/>
korejsky	korejsky	k6eAd1
강	강	k?
<g/>
,	,	kIx,
21	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1892	#num#	k4
<g/>
,	,	kIx,
Mangyongdae	Mangyongdae	k1gFnSc1
–	–	k?
31	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1932	#num#	k4
<g/>
,	,	kIx,
Ťi-lin	Ťi-lin	k1gInSc1
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
korejská	korejský	k2eAgFnSc1d1
farmářka	farmářka	k1gFnSc1
a	a	k8xC
matka	matka	k1gFnSc1
Kim	Kim	k1gFnSc2
Ir-sena	Ir-sen	k2eAgFnSc1d1
<g/>
,	,	kIx,
prvního	první	k4xOgMnSc4
severokorejského	severokorejský	k2eAgMnSc4d1
diktátora	diktátor	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
Životopis	životopis	k1gInSc1
</s>
<s>
Kang	Kang	k1gInSc1
Pan-sŏ	Pan-sŏ	k1gFnPc2
se	se	k3xPyFc4
narodila	narodit	k5eAaPmAgFnS
21	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1892	#num#	k4
ve	v	k7c6
vesnici	vesnice	k1gFnSc6
Chilgol	Chilgola	k1gFnPc2
v	v	k7c6
provincii	provincie	k1gFnSc6
Jižní	jižní	k2eAgInSc1d1
Pchjongjang	Pchjongjang	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pocházela	pocházet	k5eAaImAgFnS
z	z	k7c2
presbyteriánské	presbyteriánský	k2eAgFnSc2d1
křesťanské	křesťanský	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
<g/>
,	,	kIx,
jejím	její	k3xOp3gInSc6
otec	otec	k1gMnSc1
byl	být	k5eAaImAgMnS
učitel	učitel	k1gMnSc1
Kang	Kang	k1gMnSc1
Don-wook	Don-wook	k1gInSc1
a	a	k8xC
matkou	matka	k1gFnSc7
učitelka	učitelka	k1gFnSc1
Wi	Wi	k1gMnPc2
Don-dob	Don-doba	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Roku	rok	k1gInSc2
1908	#num#	k4
se	se	k3xPyFc4
provdala	provdat	k5eAaPmAgFnS
za	za	k7c4
Kim	Kim	k1gFnSc4
Hjŏ	Hjŏ	k1gNnSc2
<g/>
,	,	kIx,
se	s	k7c7
kterým	který	k3yRgMnSc7,k3yQgMnSc7,k3yIgMnSc7
se	se	k3xPyFc4
seznámila	seznámit	k5eAaPmAgFnS
pravděpodobně	pravděpodobně	k6eAd1
přes	přes	k7c4
amerického	americký	k2eAgMnSc4d1
misionáře	misionář	k1gMnSc4
Nelsona	Nelson	k1gMnSc4
Bella	Bella	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1912	#num#	k4
porodila	porodit	k5eAaPmAgFnS
svého	svůj	k3xOyFgMnSc2
prvního	první	k4xOgMnSc2
syna	syn	k1gMnSc2
Kim	Kim	k1gFnSc2
Ir-sena	Ir-sen	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podporovala	podporovat	k5eAaImAgFnS
svého	svůj	k3xOyFgMnSc4
manžela	manžel	k1gMnSc4
v	v	k7c6
jeho	jeho	k3xOp3gInSc6
boji	boj	k1gInSc6
za	za	k7c4
nezávislost	nezávislost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1917	#num#	k4
se	se	k3xPyFc4
přestěhovala	přestěhovat	k5eAaPmAgFnS
do	do	k7c2
Bonghwa-ri	Bonghwa-r	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
již	již	k6eAd1
působil	působit	k5eAaImAgMnS
její	její	k3xOp3gMnSc1
manžel	manžel	k1gMnSc1
a	a	k8xC
později	pozdě	k6eAd2
společně	společně	k6eAd1
uprchli	uprchnout	k5eAaPmAgMnP
do	do	k7c2
Mandžuska	Mandžusko	k1gNnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
se	se	k3xPyFc4
vyhnuli	vyhnout	k5eAaPmAgMnP
zatčení	zatčení	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zemřela	zemřít	k5eAaPmAgFnS
31	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1932	#num#	k4
v	v	k7c6
čínské	čínský	k2eAgFnSc6d1
provincii	provincie	k1gFnSc6
Ťi-lin	Ťi-lin	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Propaganda	propaganda	k1gFnSc1
</s>
<s>
V	v	k7c6
KLDR	KLDR	kA
měla	mít	k5eAaImAgFnS
již	již	k6eAd1
od	od	k7c2
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
vlastní	vlastnit	k5eAaImIp3nS
kult	kult	k1gInSc1
osobnosti	osobnost	k1gFnSc2
a	a	k8xC
byla	být	k5eAaImAgFnS
první	první	k4xOgFnSc1
z	z	k7c2
rodinných	rodinný	k2eAgMnPc2d1
příslušníků	příslušník	k1gMnPc2
Kim	Kim	k1gFnSc1
Ir-sena	Ir-sen	k2eAgFnSc1d1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
užívala	užívat	k5eAaImAgFnS
titul	titul	k1gInSc4
„	„	k?
<g/>
matka	matka	k1gFnSc1
Koreje	Korea	k1gFnSc2
<g/>
“	“	k?
nebo	nebo	k8xC
„	„	k?
<g/>
velká	velká	k1gFnSc1
matka	matka	k1gFnSc1
Koreje	Korea	k1gFnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1967	#num#	k4
ji	on	k3xPp3gFnSc4
korejské	korejský	k2eAgFnPc1d1
noviny	novina	k1gFnPc1
Rodong	Rodong	k1gMnSc1
Sinmun	Sinmun	k1gMnSc1
chválil	chválit	k5eAaImAgMnS
jako	jako	k9
„	„	k?
<g/>
matku	matka	k1gFnSc4
všech	všecek	k3xTgInPc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
titul	titul	k1gInSc4
později	pozdě	k6eAd2
užívala	užívat	k5eAaImAgFnS
i	i	k9
matka	matka	k1gFnSc1
Kim	Kim	k1gMnSc2
Čong-ila	Čong-il	k1gMnSc2
Kim	Kim	k1gMnSc1
Čong-suk	Čong-suk	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Je	být	k5eAaImIp3nS
jí	on	k3xPp3gFnSc7
zasvěcen	zasvěcen	k2eAgInSc1d1
presbyteriánský	presbyteriánský	k2eAgInSc1d1
kostel	kostel	k1gInSc1
v	v	k7c6
Chilgolu	Chilgol	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rodina	rodina	k1gFnSc1
</s>
<s>
Se	s	k7c7
svým	svůj	k3xOyFgMnSc7
manželem	manžel	k1gMnSc7
<g/>
,	,	kIx,
Kim	Kim	k1gMnSc7
Hjŏ	Hjŏ	k1gMnSc7
<g/>
,	,	kIx,
měla	mít	k5eAaImAgFnS
tři	tři	k4xCgFnPc4
děti	dítě	k1gFnPc4
<g/>
:	:	kIx,
</s>
<s>
Kim	Kim	k?
Ir-sen	Ir-sen	k1gInSc1
(	(	kIx(
<g/>
1912	#num#	k4
–	–	k?
1994	#num#	k4
<g/>
)	)	kIx)
-	-	kIx~
nejvyšší	vysoký	k2eAgMnSc1d3
představitel	představitel	k1gMnSc1
Korejské	korejský	k2eAgFnSc2d1
strany	strana	k1gFnSc2
práce	práce	k1gFnSc2
a	a	k8xC
první	první	k4xOgMnSc1
severokorejský	severokorejský	k2eAgMnSc1d1
diktátor	diktátor	k1gMnSc1
</s>
<s>
Kim	Kim	k?
Čchol-džu	Čchol-džu	k1gFnSc1
(	(	kIx(
<g/>
1916	#num#	k4
–	–	k?
1935	#num#	k4
<g/>
)	)	kIx)
-	-	kIx~
voják	voják	k1gMnSc1
a	a	k8xC
revolucionář	revolucionář	k1gMnSc1
</s>
<s>
Kim	Kim	k?
Jong-džu	Jong-džu	k1gFnSc1
(	(	kIx(
<g/>
*	*	kIx~
<g/>
1920	#num#	k4
<g/>
)	)	kIx)
-	-	kIx~
politický	politický	k2eAgMnSc1d1
funkcionář	funkcionář	k1gMnSc1
KLDR	KLDR	kA
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
PONS	PONS	kA
<g/>
,	,	kIx,
Philippe	Philipp	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Corée	Corée	k1gInSc1
du	du	k?
Nord	Nord	k1gInSc1
:	:	kIx,
un	un	k?
Etat-guérilla	Etat-guérilla	k1gMnSc1
en	en	k?
mutation	mutation	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Paris	Paris	k1gMnSc1
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Gallimard	Gallimard	k1gInSc1
1	#num#	k4
v.	v.	k?
(	(	kIx(
<g/>
707	#num#	k4
p.	p.	k?
<g/>
)	)	kIx)
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
14249	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
14249	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
OCLC	OCLC	kA
949707637	#num#	k4
↑	↑	k?
미	미	k?
선	선	k?
중	중	k?
태	태	k?
김	김	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Radio	radio	k1gNnSc1
Free	Free	k1gFnPc2
Asia	Asium	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
IM	IM	kA
<g/>
,	,	kIx,
Chae-ch	Chae-ch	k1gMnSc1
<g/>
'	'	kIx"
<g/>
ŏ	ŏ	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leader	leader	k1gMnSc1
Symbols	Symbols	k1gInSc1
and	and	k?
Personality	personalita	k1gFnSc2
Cult	Cult	k1gMnSc1
in	in	k?
North	North	k1gInSc1
Korea	Korea	k1gFnSc1
:	:	kIx,
the	the	k?
Leader	leader	k1gMnSc1
State	status	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Abingdon	Abingdon	k1gMnSc1
<g/>
,	,	kIx,
Oxon	Oxon	k1gMnSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
1	#num#	k4
online	onlinout	k5eAaPmIp3nS
resource	resourka	k1gFnSc3
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
317	#num#	k4
<g/>
-	-	kIx~
<g/>
56741	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
1	#num#	k4
<g/>
-	-	kIx~
<g/>
317	#num#	k4
<g/>
-	-	kIx~
<g/>
56741	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
OCLC	OCLC	kA
904799079	#num#	k4
↑	↑	k?
GAUSE	GAUSE	kA
<g/>
,	,	kIx,
Ken	Ken	k1gMnSc1
E.	E.	kA
North	North	k1gInSc1
Korea	Korea	k1gFnSc1
under	under	k1gMnSc1
Kim	Kim	k1gMnSc1
Chong-il	Chong-il	k1gMnSc1
:	:	kIx,
power	power	k1gInSc1
<g/>
,	,	kIx,
politics	politics	k1gInSc1
<g/>
,	,	kIx,
and	and	k?
prospects	prospects	k1gInSc1
for	forum	k1gNnPc2
change	change	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Santa	Santa	k1gFnSc1
Barbara	Barbara	k1gFnSc1
<g/>
,	,	kIx,
Calif	Calif	k1gInSc1
<g/>
.	.	kIx.
<g/>
:	:	kIx,
Praeger	Praeger	k1gInSc1
1	#num#	k4
online	onlinout	k5eAaPmIp3nS
resource	resourka	k1gFnSc3
(	(	kIx(
<g/>
xii	xii	k?
<g/>
,	,	kIx,
241	#num#	k4
pages	pagesa	k1gFnPc2
<g/>
)	)	kIx)
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
313	#num#	k4
<g/>
-	-	kIx~
<g/>
38176	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
313	#num#	k4
<g/>
-	-	kIx~
<g/>
38176	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
OCLC	OCLC	kA
746916878	#num#	k4
↑	↑	k?
North	North	k1gInSc1
Korea	Korea	k1gFnSc1
and	and	k?
Christianity	Christianit	k2eAgFnPc4d1
-	-	kIx~
uneasy	uneasa	k1gFnPc4
bedfellows	bedfellows	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
News	Newsa	k1gFnPc2
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
|	|	kIx~
Korea	Korea	k1gFnSc1
</s>
