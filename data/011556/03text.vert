<p>
<s>
Uruguay	Uruguay	k1gFnSc1	Uruguay
(	(	kIx(	(
<g/>
španělská	španělský	k2eAgFnSc1d1	španělská
výslovnost	výslovnost	k1gFnSc1	výslovnost
Uruguay	Uruguay	k1gFnSc1	Uruguay
<g/>
;	;	kIx,	;
oficiálně	oficiálně	k6eAd1	oficiálně
Uruguayská	uruguayský	k2eAgFnSc1d1	Uruguayská
východní	východní	k2eAgFnSc1d1	východní
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
španělsky	španělsky	k6eAd1	španělsky
República	República	k1gMnSc1	República
Oriental	Oriental	k1gMnSc1	Oriental
del	del	k?	del
Uruguay	Uruguay	k1gFnSc1	Uruguay
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stát	stát	k5eAaImF	stát
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc7	jeho
sousedy	soused	k1gMnPc7	soused
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
Brazílie	Brazílie	k1gFnSc2	Brazílie
a	a	k8xC	a
na	na	k7c6	na
západě	západ	k1gInSc6	západ
Argentina	Argentina	k1gFnSc1	Argentina
<g/>
,	,	kIx,	,
oddělená	oddělený	k2eAgFnSc1d1	oddělená
řekou	řeka	k1gFnSc7	řeka
Uruguay	Uruguay	k1gFnSc1	Uruguay
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
země	země	k1gFnSc1	země
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
rozlohou	rozloha	k1gFnSc7	rozloha
176	[number]	k4	176
000	[number]	k4	000
čtverečních	čtvereční	k2eAgInPc2d1	čtvereční
kilometrů	kilometr	k1gInPc2	kilometr
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
Surinamu	Surinam	k1gInSc6	Surinam
druhou	druhý	k4xOgFnSc7	druhý
nejmenší	malý	k2eAgFnSc7d3	nejmenší
jihoamerickou	jihoamerický	k2eAgFnSc7d1	jihoamerická
zemí	zem	k1gFnSc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
3,44	[number]	k4	3,44
milionu	milion	k4xCgInSc2	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
1,8	[number]	k4	1,8
milionu	milion	k4xCgInSc2	milion
v	v	k7c4	v
metropolitní	metropolitní	k2eAgFnPc4d1	metropolitní
oblasti	oblast	k1gFnPc4	oblast
hlavního	hlavní	k2eAgMnSc2d1	hlavní
a	a	k8xC	a
největší	veliký	k2eAgNnPc1d3	veliký
města	město	k1gNnPc1	město
Montevideo	Montevideo	k1gNnSc1	Montevideo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oficiálním	oficiální	k2eAgInSc7d1	oficiální
jazykem	jazyk	k1gInSc7	jazyk
je	být	k5eAaImIp3nS	být
španělština	španělština	k1gFnSc1	španělština
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
prvními	první	k4xOgMnPc7	první
kolonizátory	kolonizátor	k1gMnPc7	kolonizátor
oblasti	oblast	k1gFnSc2	oblast
byli	být	k5eAaImAgMnP	být
Portugalci	Portugalec	k1gMnPc1	Portugalec
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
evropské	evropský	k2eAgFnSc3d1	Evropská
kolonizaci	kolonizace	k1gFnSc3	kolonizace
došlo	dojít	k5eAaPmAgNnS	dojít
<g/>
,	,	kIx,	,
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
okolními	okolní	k2eAgFnPc7d1	okolní
zeměmi	zem	k1gFnPc7	zem
<g/>
,	,	kIx,	,
poměrně	poměrně	k6eAd1	poměrně
pozdě	pozdě	k6eAd1	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Evropanech	Evropan	k1gMnPc6	Evropan
získali	získat	k5eAaPmAgMnP	získat
Uruguayci	Uruguayec	k1gMnPc1	Uruguayec
nezávislost	nezávislost	k1gFnSc4	nezávislost
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1811	[number]	k4	1811
až	až	k9	až
1828	[number]	k4	1828
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
demokracie	demokracie	k1gFnSc1	demokracie
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
přerušil	přerušit	k5eAaPmAgMnS	přerušit
vojenský	vojenský	k2eAgInSc4d1	vojenský
převrat	převrat	k1gInSc4	převrat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
<g/>
.	.	kIx.	.
</s>
<s>
Armáda	armáda	k1gFnSc1	armáda
se	se	k3xPyFc4	se
moci	moct	k5eAaImF	moct
vzdala	vzdát	k5eAaPmAgFnS	vzdát
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
a	a	k8xC	a
demokratický	demokratický	k2eAgInSc1d1	demokratický
režim	režim	k1gInSc1	režim
byl	být	k5eAaImAgInS	být
obnoven	obnovit	k5eAaPmNgInS	obnovit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Uruguay	Uruguay	k1gFnSc1	Uruguay
se	se	k3xPyFc4	se
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
žebříčcích	žebříček	k1gInPc6	žebříček
a	a	k8xC	a
indexech	index	k1gInPc6	index
pravidelně	pravidelně	k6eAd1	pravidelně
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
jako	jako	k9	jako
země	země	k1gFnSc1	země
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Latinské	latinský	k2eAgFnSc6d1	Latinská
Americe	Amerika	k1gFnSc6	Amerika
nejkvalitnější	kvalitní	k2eAgFnSc4d3	nejkvalitnější
demokracii	demokracie	k1gFnSc4	demokracie
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
míru	míra	k1gFnSc4	míra
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
<g/>
,	,	kIx,	,
nejnižší	nízký	k2eAgFnSc1d3	nejnižší
úroveň	úroveň	k1gFnSc1	úroveň
korupce	korupce	k1gFnSc2	korupce
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgFnSc6d3	nejvyšší
elektronizaci	elektronizace	k1gFnSc6	elektronizace
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc4d3	veliký
svobodu	svoboda	k1gFnSc4	svoboda
tisku	tisk	k1gInSc2	tisk
a	a	k8xC	a
nejmohutnější	mohutný	k2eAgFnSc4d3	nejmohutnější
střední	střední	k2eAgFnSc4d1	střední
třídu	třída	k1gFnSc4	třída
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
zeměmi	zem	k1gFnPc7	zem
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
má	mít	k5eAaImIp3nS	mít
Uruguay	Uruguay	k1gFnSc4	Uruguay
druhý	druhý	k4xOgInSc1	druhý
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
hrubý	hrubý	k2eAgInSc1d1	hrubý
domácí	domácí	k2eAgInSc1d1	domácí
produkt	produkt	k1gInSc1	produkt
(	(	kIx(	(
<g/>
v	v	k7c6	v
paritě	parita	k1gFnSc6	parita
kupní	kupní	k2eAgFnSc2d1	kupní
síly	síla	k1gFnSc2	síla
<g/>
)	)	kIx)	)
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgInSc4	třetí
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
index	index	k1gInSc4	index
lidského	lidský	k2eAgInSc2d1	lidský
rozvoje	rozvoj	k1gInSc2	rozvoj
a	a	k8xC	a
druhou	druhý	k4xOgFnSc4	druhý
nejnižší	nízký	k2eAgFnSc4d3	nejnižší
příjmovou	příjmový	k2eAgFnSc4d1	příjmová
nerovnost	nerovnost	k1gFnSc4	nerovnost
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
bývá	bývat	k5eAaImIp3nS	bývat
Uruguay	Uruguay	k1gFnSc1	Uruguay
označována	označovat	k5eAaImNgFnS	označovat
za	za	k7c4	za
"	"	kIx"	"
<g/>
nejevropštější	evropský	k2eAgFnPc4d3	nejevropštější
<g/>
"	"	kIx"	"
z	z	k7c2	z
jihoamerických	jihoamerický	k2eAgFnPc2d1	jihoamerická
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
i	i	k9	i
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
87,7	[number]	k4	87,7
%	%	kIx~	%
Uruguayců	Uruguayec	k1gMnPc2	Uruguayec
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
evropský	evropský	k2eAgInSc4d1	evropský
původ	původ	k1gInSc4	původ
(	(	kIx(	(
<g/>
údaj	údaj	k1gInSc4	údaj
zjištěný	zjištěný	k2eAgInSc4d1	zjištěný
při	při	k7c6	při
sčítání	sčítání	k1gNnSc6	sčítání
lidu	lid	k1gInSc2	lid
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedinečnou	jedinečný	k2eAgFnSc4d1	jedinečná
v	v	k7c6	v
kontextu	kontext	k1gInSc6	kontext
jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
je	být	k5eAaImIp3nS	být
absence	absence	k1gFnSc1	absence
partyzánských	partyzánský	k2eAgNnPc2d1	partyzánské
hnutí	hnutí	k1gNnPc2	hnutí
<g/>
,	,	kIx,	,
paramilitárních	paramilitární	k2eAgFnPc2d1	paramilitární
jednotek	jednotka	k1gFnPc2	jednotka
a	a	k8xC	a
terorismu	terorismus	k1gInSc2	terorismus
<g/>
.	.	kIx.	.
</s>
<s>
Uruguay	Uruguay	k1gFnSc1	Uruguay
je	být	k5eAaImIp3nS	být
též	též	k6eAd1	též
první	první	k4xOgInSc4	první
zemí	zem	k1gFnPc2	zem
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
legalizovala	legalizovat	k5eAaBmAgFnS	legalizovat
marihuanu	marihuana	k1gFnSc4	marihuana
<g/>
,	,	kIx,	,
a	a	k8xC	a
zemí	zem	k1gFnPc2	zem
s	s	k7c7	s
nejzelenější	zelený	k2eAgFnSc7d3	nejzelenější
energetikou	energetika	k1gFnSc7	energetika
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Název	název	k1gInSc1	název
==	==	k?	==
</s>
</p>
<p>
<s>
Slovo	slovo	k1gNnSc1	slovo
Uruguay	Uruguay	k1gFnSc1	Uruguay
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
indiánského	indiánský	k2eAgInSc2d1	indiánský
jazyka	jazyk	k1gInSc2	jazyk
guaraní	guaranit	k5eAaPmIp3nS	guaranit
a	a	k8xC	a
co	co	k3yRnSc1	co
přesně	přesně	k6eAd1	přesně
označuje	označovat	k5eAaImIp3nS	označovat
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
úplně	úplně	k6eAd1	úplně
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
španělského	španělský	k2eAgMnSc2d1	španělský
přírodovědce	přírodovědec	k1gMnSc2	přírodovědec
Félixe	Félixe	k1gFnSc2	Félixe
de	de	k?	de
Azara	Azara	k1gMnSc1	Azara
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
malého	malý	k2eAgMnSc4d1	malý
ptáka	pták	k1gMnSc4	pták
nazývaného	nazývaný	k2eAgInSc2d1	nazývaný
el	ela	k1gFnPc2	ela
urú	urú	k?	urú
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
žije	žít	k5eAaImIp3nS	žít
na	na	k7c6	na
březích	břeh	k1gInPc6	břeh
řeky	řeka	k1gFnSc2	řeka
Uruguay	Uruguay	k1gFnSc1	Uruguay
<g/>
.	.	kIx.	.
</s>
<s>
Río	Río	k?	Río
del	del	k?	del
país	país	k1gInSc1	país
del	del	k?	del
urú	urú	k?	urú
by	by	kYmCp3nS	by
tedy	tedy	k9	tedy
znamenalo	znamenat	k5eAaImAgNnS	znamenat
Řeka	řeka	k1gFnSc1	řeka
země	zem	k1gFnSc2	zem
(	(	kIx(	(
<g/>
ptáka	pták	k1gMnSc2	pták
<g/>
)	)	kIx)	)
urú	urú	k?	urú
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
Azarových	Azarův	k2eAgMnPc2d1	Azarův
průvodců	průvodce	k1gMnPc2	průvodce
ale	ale	k8xC	ale
zastával	zastávat	k5eAaImAgMnS	zastávat
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
slovo	slovo	k1gNnSc1	slovo
Uruguay	Uruguay	k1gFnSc1	Uruguay
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
částí	část	k1gFnPc2	část
–	–	k?	–
uruguá	uruguat	k5eAaPmIp3nS	uruguat
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
šnek	šnek	k1gMnSc1	šnek
<g/>
,	,	kIx,	,
hlemýžď	hlemýžď	k1gMnSc1	hlemýžď
<g/>
)	)	kIx)	)
a	a	k8xC	a
ï	ï	k?	ï
(	(	kIx(	(
<g/>
řeka	řeka	k1gFnSc1	řeka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
by	by	kYmCp3nS	by
tedy	tedy	k9	tedy
označoval	označovat	k5eAaImAgInS	označovat
"	"	kIx"	"
<g/>
řeku	řeka	k1gFnSc4	řeka
hlemýžďů	hlemýžď	k1gMnPc2	hlemýžď
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Río	Río	k1gFnSc1	Río
de	de	k?	de
los	los	k1gInSc1	los
caracoles	caracoles	k1gInSc1	caracoles
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Uruguayský	uruguayský	k2eAgMnSc1d1	uruguayský
básník	básník	k1gMnSc1	básník
Juan	Juan	k1gMnSc1	Juan
Zorrilla	Zorrilla	k1gMnSc1	Zorrilla
de	de	k?	de
San	San	k1gMnSc1	San
Martín	Martín	k1gMnSc1	Martín
dokonce	dokonce	k9	dokonce
uváděl	uvádět	k5eAaImAgMnS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
spojení	spojení	k1gNnSc4	spojení
tří	tři	k4xCgNnPc2	tři
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
doslovný	doslovný	k2eAgInSc4d1	doslovný
překlad	překlad	k1gInSc4	překlad
by	by	kYmCp3nS	by
znamenal	znamenat	k5eAaImAgMnS	znamenat
Země	zem	k1gFnPc4	zem
pestrých	pestrý	k2eAgMnPc2d1	pestrý
ptáků	pták	k1gMnPc2	pták
(	(	kIx(	(
<g/>
río	río	k?	río
de	de	k?	de
los	los	k1gInSc1	los
pájaros	pájarosa	k1gFnPc2	pájarosa
pintados	pintadosa	k1gFnPc2	pintadosa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Název	název	k1gInSc1	název
Uruguayská	uruguayský	k2eAgFnSc1d1	Uruguayská
východní	východní	k2eAgFnSc1d1	východní
republika	republika	k1gFnSc1	republika
se	se	k3xPyFc4	se
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
ke	k	k7c3	k
skutečnosti	skutečnost	k1gFnSc3	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
země	země	k1gFnSc1	země
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
východním	východní	k2eAgNnSc6d1	východní
pobřeží	pobřeží	k1gNnSc6	pobřeží
řeky	řeka	k1gFnSc2	řeka
Uruguay	Uruguay	k1gFnSc1	Uruguay
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
existovala	existovat	k5eAaImAgFnS	existovat
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Cisplatina	Cisplatina	k1gFnSc1	Cisplatina
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
před	před	k7c4	před
La	la	k1gNnSc4	la
Platou	Platá	k1gFnSc4	Platá
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Původ	původ	k1gInSc1	původ
vlajky	vlajka	k1gFnSc2	vlajka
==	==	k?	==
</s>
</p>
<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
má	mít	k5eAaImIp3nS	mít
bílý	bílý	k2eAgInSc4d1	bílý
podklad	podklad	k1gInSc4	podklad
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
bílém	bílé	k1gNnSc6	bílé
podkladu	podklad	k1gInSc2	podklad
jsou	být	k5eAaImIp3nP	být
vodorovné	vodorovný	k2eAgFnPc1d1	vodorovná
<g/>
,	,	kIx,	,
úzké	úzký	k2eAgFnPc1d1	úzká
a	a	k8xC	a
tmavě	tmavě	k6eAd1	tmavě
modré	modrý	k2eAgInPc4d1	modrý
pruhy	pruh	k1gInPc4	pruh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
levém	levý	k2eAgInSc6d1	levý
vrchním	vrchní	k2eAgInSc6d1	vrchní
rohu	roh	k1gInSc6	roh
jsou	být	k5eAaImIp3nP	být
dva	dva	k4xCgInPc1	dva
modré	modrý	k2eAgInPc1d1	modrý
pruhy	pruh	k1gInPc1	pruh
přerušeny	přerušit	k5eAaPmNgInP	přerušit
protože	protože	k8xS	protože
tam	tam	k6eAd1	tam
je	být	k5eAaImIp3nS	být
umístěno	umístit	k5eAaPmNgNnS	umístit
populární	populární	k2eAgNnSc1d1	populární
žluté	žlutý	k2eAgNnSc1d1	žluté
májové	májový	k2eAgNnSc1d1	Májové
jihoamerické	jihoamerický	k2eAgNnSc1d1	jihoamerické
slunce	slunce	k1gNnSc1	slunce
svobody	svoboda	k1gFnSc2	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Slunce	slunce	k1gNnSc1	slunce
je	být	k5eAaImIp3nS	být
vyobrazováno	vyobrazován	k2eAgNnSc1d1	vyobrazován
s	s	k7c7	s
osmi	osm	k4xCc7	osm
rovnými	rovný	k2eAgFnPc7d1	rovná
a	a	k8xC	a
osmi	osm	k4xCc7	osm
zahnutými	zahnutý	k2eAgInPc7d1	zahnutý
paprsky	paprsek	k1gInPc7	paprsek
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vlajka	vlajka	k1gFnSc1	vlajka
byla	být	k5eAaImAgFnS	být
inspirovaná	inspirovaný	k2eAgFnSc1d1	inspirovaná
vlajkou	vlajka	k1gFnSc7	vlajka
Argentinskou	argentinský	k2eAgFnSc7d1	Argentinská
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Před	před	k7c7	před
příchodem	příchod	k1gInSc7	příchod
Evropanů	Evropan	k1gMnPc2	Evropan
===	===	k?	===
</s>
</p>
<p>
<s>
Úrodné	úrodný	k2eAgFnPc1d1	úrodná
planiny	planina	k1gFnPc1	planina
dnešní	dnešní	k2eAgFnPc1d1	dnešní
Uruguaye	Uruguay	k1gFnPc1	Uruguay
byly	být	k5eAaImAgFnP	být
poprvé	poprvé	k6eAd1	poprvé
osídleny	osídlit	k5eAaPmNgFnP	osídlit
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
7000	[number]	k4	7000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nomády	nomád	k1gMnPc7	nomád
žijícími	žijící	k2eAgMnPc7d1	žijící
v	v	k7c6	v
malých	malý	k2eAgFnPc6d1	malá
skupinách	skupina	k1gFnPc6	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
praobyvatelé	praobyvatel	k1gMnPc1	praobyvatel
začali	začít	k5eAaPmAgMnP	začít
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
s	s	k7c7	s
výrobou	výroba	k1gFnSc7	výroba
jednoduchých	jednoduchý	k2eAgInPc2d1	jednoduchý
kamenných	kamenný	k2eAgInPc2d1	kamenný
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
národ	národ	k1gInSc1	národ
Charrúas	Charrúasa	k1gFnPc2	Charrúasa
<g/>
,	,	kIx,	,
u	u	k7c2	u
něhož	jenž	k3xRgInSc2	jenž
byly	být	k5eAaImAgFnP	být
nalezeny	nalezen	k2eAgFnPc1d1	nalezena
známky	známka	k1gFnPc1	známka
pokročilé	pokročilý	k2eAgFnSc2d1	pokročilá
civilizace	civilizace	k1gFnSc2	civilizace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
provozovala	provozovat	k5eAaImAgFnS	provozovat
rybolov	rybolov	k1gInSc4	rybolov
a	a	k8xC	a
zemědělství	zemědělství	k1gNnSc4	zemědělství
a	a	k8xC	a
znala	znát	k5eAaImAgFnS	znát
i	i	k9	i
keramiku	keramika	k1gFnSc4	keramika
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
nejvýznamnějším	významný	k2eAgInSc7d3	nejvýznamnější
národem	národ	k1gInSc7	národ
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgInSc2d1	dnešní
Uruguaye	Uruguay	k1gFnSc2	Uruguay
byl	být	k5eAaImAgInS	být
Tupí-Guaraní	Tupí-Guaraní	k1gNnSc4	Tupí-Guaraní
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc4	jehož
příslušníci	příslušník	k1gMnPc1	příslušník
byli	být	k5eAaImAgMnP	být
také	také	k9	také
lovci	lovec	k1gMnPc1	lovec
a	a	k8xC	a
sběrači	sběrač	k1gMnPc1	sběrač
<g/>
.	.	kIx.	.
</s>
<s>
Veškeré	veškerý	k3xTgNnSc4	veškerý
původní	původní	k2eAgNnSc4d1	původní
indiánské	indiánský	k2eAgNnSc4d1	indiánské
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
nejpozději	pozdě	k6eAd3	pozdě
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
vyhubeno	vyhuben	k2eAgNnSc4d1	vyhubeno
či	či	k8xC	či
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
obětí	oběť	k1gFnPc2	oběť
některých	některý	k3yIgNnPc6	některý
z	z	k7c2	z
katastrof	katastrofa	k1gFnPc2	katastrofa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Koloniální	koloniální	k2eAgNnSc4d1	koloniální
období	období	k1gNnSc4	období
===	===	k?	===
</s>
</p>
<p>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1516	[number]	k4	1516
zemi	zem	k1gFnSc6	zem
objevili	objevit	k5eAaPmAgMnP	objevit
Španělé	Španěl	k1gMnPc1	Španěl
a	a	k8xC	a
přibližně	přibližně	k6eAd1	přibližně
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
do	do	k7c2	do
země	zem	k1gFnSc2	zem
přišli	přijít	k5eAaPmAgMnP	přijít
i	i	k9	i
Portugalci	Portugalec	k1gMnPc1	Portugalec
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
kolonizátoři	kolonizátor	k1gMnPc1	kolonizátor
pokoušeli	pokoušet	k5eAaImAgMnP	pokoušet
toto	tento	k3xDgNnSc4	tento
území	území	k1gNnSc4	území
osídlovat	osídlovat	k5eAaImF	osídlovat
<g/>
,	,	kIx,	,
naráželi	narážet	k5eAaImAgMnP	narážet
však	však	k9	však
na	na	k7c4	na
odpor	odpor	k1gInSc4	odpor
indiánů	indián	k1gMnPc2	indián
<g/>
.	.	kIx.	.
1624	[number]	k4	1624
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Sorianu	Sorian	k1gInSc6	Sorian
(	(	kIx(	(
<g/>
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Río	Río	k1gFnSc1	Río
Negro	Negro	k1gNnSc4	Negro
<g/>
)	)	kIx)	)
Španěly	Španěly	k1gInPc4	Španěly
zřízeno	zřídit	k5eAaPmNgNnS	zřídit
první	první	k4xOgNnSc4	první
trvalé	trvalý	k2eAgNnSc4d1	trvalé
sídlo	sídlo	k1gNnSc4	sídlo
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
1726	[number]	k4	1726
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
Montevideo	Montevideo	k1gNnSc1	Montevideo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
o	o	k7c6	o
území	území	k1gNnSc6	území
mezi	mezi	k7c7	mezi
Brazílií	Brazílie	k1gFnSc7	Brazílie
a	a	k8xC	a
Argentinou	Argentina	k1gFnSc7	Argentina
probíhaly	probíhat	k5eAaImAgInP	probíhat
mnohé	mnohý	k2eAgInPc1d1	mnohý
boje	boj	k1gInPc1	boj
mezi	mezi	k7c7	mezi
Brity	Brit	k1gMnPc7	Brit
<g/>
,	,	kIx,	,
Portugalci	Portugalec	k1gMnPc7	Portugalec
a	a	k8xC	a
Španěly	Španěl	k1gMnPc7	Španěl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1776	[number]	k4	1776
začlenili	začlenit	k5eAaPmAgMnP	začlenit
Španělé	Španěl	k1gMnPc1	Španěl
toto	tento	k3xDgNnSc4	tento
území	území	k1gNnSc4	území
do	do	k7c2	do
místokrálovství	místokrálovství	k1gNnSc2	místokrálovství
Río	Río	k1gMnSc2	Río
de	de	k?	de
la	la	k1gNnSc1	la
Plata	plato	k1gNnSc2	plato
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nezávislost	nezávislost	k1gFnSc4	nezávislost
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
celou	celý	k2eAgFnSc4d1	celá
Jižní	jižní	k2eAgFnSc4d1	jižní
Ameriku	Amerika	k1gFnSc4	Amerika
zachvátila	zachvátit	k5eAaPmAgFnS	zachvátit
válka	válka	k1gFnSc1	válka
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
(	(	kIx(	(
<g/>
1811	[number]	k4	1811
<g/>
–	–	k?	–
<g/>
1815	[number]	k4	1815
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnSc7d3	nejznámější
postavou	postava	k1gFnSc7	postava
celo-jihoamerického	celoihoamerický	k2eAgInSc2d1	celo-jihoamerický
boje	boj	k1gInSc2	boj
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Simón	Simón	k1gInSc1	Simón
Bolívar	Bolívar	k1gInSc4	Bolívar
<g/>
,	,	kIx,	,
v	v	k7c6	v
Uruguayi	Uruguay	k1gFnSc6	Uruguay
pak	pak	k6eAd1	pak
José	Josý	k2eAgNnSc1d1	José
Gervasio	Gervasio	k1gNnSc1	Gervasio
Artigas	Artigasa	k1gFnPc2	Artigasa
<g/>
.	.	kIx.	.
</s>
<s>
Španělé	Španěl	k1gMnPc1	Španěl
byli	být	k5eAaImAgMnP	být
z	z	k7c2	z
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
postupně	postupně	k6eAd1	postupně
vyhnáni	vyhnat	k5eAaPmNgMnP	vyhnat
a	a	k8xC	a
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
mnoho	mnoho	k4c1	mnoho
nových	nový	k2eAgInPc2d1	nový
nezávislých	závislý	k2eNgInPc2d1	nezávislý
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
Uruguaye	Uruguay	k1gFnSc2	Uruguay
bylo	být	k5eAaImAgNnS	být
zpočátku	zpočátku	k6eAd1	zpočátku
okupováno	okupovat	k5eAaBmNgNnS	okupovat
Brazílií	Brazílie	k1gFnSc7	Brazílie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1828	[number]	k4	1828
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
na	na	k7c6	na
základě	základ	k1gInSc6	základ
mírové	mírový	k2eAgFnSc2d1	mírová
dohody	dohoda	k1gFnSc2	dohoda
mezi	mezi	k7c4	mezi
Brazílii	Brazílie	k1gFnSc4	Brazílie
a	a	k8xC	a
Argentinou	Argentina	k1gFnSc7	Argentina
a	a	k8xC	a
za	za	k7c4	za
zprostředkování	zprostředkování	k1gNnSc4	zprostředkování
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
nezávislá	závislý	k2eNgFnSc1d1	nezávislá
Uruguayská	uruguayský	k2eAgFnSc1d1	Uruguayská
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Svoji	svůj	k3xOyFgFnSc4	svůj
suverenitu	suverenita	k1gFnSc4	suverenita
musela	muset	k5eAaImAgFnS	muset
Uruguay	Uruguay	k1gFnSc1	Uruguay
bránit	bránit	k5eAaImF	bránit
hned	hned	k6eAd1	hned
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
vzniku	vznik	k1gInSc6	vznik
ve	v	k7c6	v
tzv.	tzv.	kA	tzv.
Velké	velký	k2eAgFnSc6d1	velká
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
letech	let	k1gInPc6	let
1839	[number]	k4	1839
<g/>
–	–	k?	–
<g/>
1851	[number]	k4	1851
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
občanské	občanský	k2eAgFnSc3d1	občanská
válce	válka	k1gFnSc3	válka
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yRgFnSc2	který
však	však	k9	však
zasahovaly	zasahovat	k5eAaImAgFnP	zasahovat
sousední	sousední	k2eAgFnPc1d1	sousední
Argentina	Argentina	k1gFnSc1	Argentina
a	a	k8xC	a
Brazílie	Brazílie	k1gFnSc1	Brazílie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Moderní	moderní	k2eAgFnPc1d1	moderní
dějiny	dějiny	k1gFnPc1	dějiny
===	===	k?	===
</s>
</p>
<p>
<s>
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
začal	začít	k5eAaPmAgMnS	začít
z	z	k7c2	z
zemi	zem	k1gFnSc6	zem
vnitropolitický	vnitropolitický	k2eAgInSc1d1	vnitropolitický
boj	boj	k1gInSc1	boj
liberálů	liberál	k1gMnPc2	liberál
(	(	kIx(	(
<g/>
colorados	colorados	k1gMnSc1	colorados
<g/>
)	)	kIx)	)
a	a	k8xC	a
konzervativců	konzervativec	k1gMnPc2	konzervativec
(	(	kIx(	(
<g/>
blancos	blancos	k1gMnSc1	blancos
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
převrhla	převrhnout	k5eAaPmAgFnS	převrhnout
v	v	k7c4	v
násilné	násilný	k2eAgInPc4d1	násilný
nepokoje	nepokoj	k1gInPc4	nepokoj
a	a	k8xC	a
občanskou	občanský	k2eAgFnSc4d1	občanská
válku	válka	k1gFnSc4	válka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vyústila	vyústit	k5eAaPmAgFnS	vyústit
ve	v	k7c4	v
vládu	vláda	k1gFnSc4	vláda
vojenské	vojenský	k2eAgFnSc2d1	vojenská
diktatury	diktatura	k1gFnSc2	diktatura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1903	[number]	k4	1903
se	se	k3xPyFc4	se
moc	moc	k6eAd1	moc
vrátila	vrátit	k5eAaPmAgFnS	vrátit
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
civilistů	civilista	k1gMnPc2	civilista
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgMnSc1d1	nový
prezident	prezident	k1gMnSc1	prezident
a	a	k8xC	a
reformátor	reformátor	k1gMnSc1	reformátor
José	Josý	k2eAgFnSc2d1	Josý
Batlle	Batlle	k1gFnSc2	Batlle
y	y	k?	y
Ordóñ	Ordóñ	k1gMnSc1	Ordóñ
realizoval	realizovat	k5eAaBmAgMnS	realizovat
demokratické	demokratický	k2eAgFnSc2d1	demokratická
a	a	k8xC	a
sociální	sociální	k2eAgFnSc2d1	sociální
reformy	reforma	k1gFnSc2	reforma
a	a	k8xC	a
pozvedl	pozvednout	k5eAaPmAgMnS	pozvednout
místní	místní	k2eAgFnSc4d1	místní
ekonomiku	ekonomika	k1gFnSc4	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
Následovalo	následovat	k5eAaImAgNnS	následovat
období	období	k1gNnSc1	období
prosperity	prosperita	k1gFnSc2	prosperita
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
ukončeno	ukončit	k5eAaPmNgNnS	ukončit
celosvětovou	celosvětový	k2eAgFnSc7d1	celosvětová
hospodářskou	hospodářský	k2eAgFnSc7d1	hospodářská
krizí	krize	k1gFnSc7	krize
v	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Demokratický	demokratický	k2eAgInSc1d1	demokratický
režim	režim	k1gInSc1	režim
byl	být	k5eAaImAgInS	být
svržen	svrhnout	k5eAaPmNgInS	svrhnout
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
nastolena	nastolen	k2eAgFnSc1d1	nastolena
nová	nový	k2eAgFnSc1d1	nová
vojenská	vojenský	k2eAgFnSc1d1	vojenská
diktatura	diktatura	k1gFnSc1	diktatura
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
zachovávala	zachovávat	k5eAaImAgFnS	zachovávat
Uruguay	Uruguay	k1gFnSc1	Uruguay
neutrálitu	neutrálit	k1gInSc2	neutrálit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
přiklonila	přiklonit	k5eAaPmAgFnS	přiklonit
ke	k	k7c3	k
Spojencům	spojenec	k1gMnPc3	spojenec
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
začalo	začít	k5eAaPmAgNnS	začít
desetiletí	desetiletí	k1gNnSc4	desetiletí
dalšího	další	k2eAgInSc2d1	další
rozkvětu	rozkvět	k1gInSc2	rozkvět
ekonomiky	ekonomika	k1gFnSc2	ekonomika
pod	pod	k7c7	pod
vládou	vláda	k1gFnSc7	vláda
liberálů	liberál	k1gMnPc2	liberál
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
situace	situace	k1gFnSc1	situace
zhoršovala	zhoršovat	k5eAaImAgFnS	zhoršovat
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
vnitropolitických	vnitropolitický	k2eAgInPc2d1	vnitropolitický
projevů	projev	k1gInPc2	projev
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
krize	krize	k1gFnSc2	krize
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
radikalizace	radikalizace	k1gFnSc1	radikalizace
levicových	levicový	k2eAgNnPc2d1	levicové
hnutí	hnutí	k1gNnPc2	hnutí
<g/>
,	,	kIx,	,
především	především	k9	především
guerillového	guerillový	k2eAgNnSc2d1	guerillové
hnutí	hnutí	k1gNnSc2	hnutí
Tupamaros	Tupamarosa	k1gFnPc2	Tupamarosa
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
bojovala	bojovat	k5eAaImAgFnS	bojovat
proti	proti	k7c3	proti
vládě	vláda	k1gFnSc3	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
situace	situace	k1gFnSc1	situace
dospěla	dochvít	k5eAaPmAgFnS	dochvít
k	k	k7c3	k
nové	nový	k2eAgFnSc3d1	nová
vojenské	vojenský	k2eAgFnSc3d1	vojenská
diktatuře	diktatura	k1gFnSc3	diktatura
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1973	[number]	k4	1973
až	až	k9	až
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
<g/>
,	,	kIx,	,
uprostřed	uprostřed	k7c2	uprostřed
rostoucí	rostoucí	k2eAgFnSc2d1	rostoucí
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
a	a	k8xC	a
politické	politický	k2eAgFnSc2d1	politická
vřavy	vřava	k1gFnSc2	vřava
<g/>
,	,	kIx,	,
ozbrojené	ozbrojený	k2eAgFnPc1d1	ozbrojená
síly	síla	k1gFnPc1	síla
uzavřely	uzavřít	k5eAaPmAgFnP	uzavřít
kongres	kongres	k1gInSc4	kongres
a	a	k8xC	a
založily	založit	k5eAaPmAgFnP	založit
civilně-vojenský	civilněojenský	k2eAgInSc4d1	civilně-vojenský
režim	režim	k1gInSc4	režim
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
kolem	kolem	k7c2	kolem
180	[number]	k4	180
Uruguajců	Uruguajce	k1gMnPc2	Uruguajce
bylo	být	k5eAaImAgNnS	být
zabito	zabít	k5eAaPmNgNnS	zabít
během	během	k7c2	během
12	[number]	k4	12
<g/>
leté	letý	k2eAgFnSc2d1	letá
vojenské	vojenský	k2eAgFnSc2d1	vojenská
vlády	vláda	k1gFnSc2	vláda
v	v	k7c6	v
těchto	tento	k3xDgNnPc6	tento
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
byla	být	k5eAaImAgFnS	být
zabita	zabit	k2eAgFnSc1d1	zabita
v	v	k7c6	v
Argentině	Argentina	k1gFnSc6	Argentina
a	a	k8xC	a
dalších	další	k2eAgFnPc6d1	další
sousedních	sousední	k2eAgFnPc6d1	sousední
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
36	[number]	k4	36
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
Uruguayi	Uruguay	k1gFnSc6	Uruguay
<g/>
.	.	kIx.	.
</s>
<s>
Volby	volba	k1gFnPc1	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
a	a	k8xC	a
nástup	nástup	k1gInSc1	nástup
prezidenta	prezident	k1gMnSc2	prezident
Julia	Julius	k1gMnSc2	Julius
Maríi	Marí	k1gFnSc2	Marí
Sanguinettiho	Sanguinetti	k1gMnSc2	Sanguinetti
(	(	kIx(	(
<g/>
colorados	colorados	k1gInSc1	colorados
<g/>
)	)	kIx)	)
nastartovaly	nastartovat	k5eAaPmAgInP	nastartovat
další	další	k2eAgInSc4d1	další
demokratizační	demokratizační	k2eAgInSc4d1	demokratizační
proces	proces	k1gInSc4	proces
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
byl	být	k5eAaImAgInS	být
charaktterizován	charaktterizovat	k5eAaPmNgInS	charaktterizovat
hospodářskými	hospodářský	k2eAgFnPc7d1	hospodářská
těžkostmi	těžkost	k1gFnPc7	těžkost
(	(	kIx(	(
<g/>
vysoký	vysoký	k2eAgInSc1d1	vysoký
zahraniční	zahraniční	k2eAgInSc1d1	zahraniční
dluh	dluh	k1gInSc1	dluh
a	a	k8xC	a
inflace	inflace	k1gFnSc1	inflace
<g/>
)	)	kIx)	)
a	a	k8xC	a
spory	spor	k1gInPc1	spor
o	o	k7c4	o
minulost	minulost	k1gFnSc4	minulost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
dopadla	dopadnout	k5eAaPmAgFnS	dopadnout
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
a	a	k8xC	a
bankovní	bankovní	k2eAgFnSc1d1	bankovní
krize	krize	k1gFnSc1	krize
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
byla	být	k5eAaImAgFnS	být
nadvláda	nadvláda	k1gFnSc1	nadvláda
dvou	dva	k4xCgFnPc2	dva
tradičních	tradiční	k2eAgFnPc2d1	tradiční
stran	strana	k1gFnPc2	strana
přerušena	přerušit	k5eAaPmNgFnS	přerušit
vítězstvím	vítězství	k1gNnSc7	vítězství
kandidáta	kandidát	k1gMnSc4	kandidát
středolevé	středolevý	k2eAgFnSc2d1	středolevá
Široké	Široké	k2eAgFnSc2d1	Široké
fronty	fronta	k1gFnSc2	fronta
Tabaré	Tabarý	k2eAgFnSc2d1	Tabarý
Vázqueze	Vázqueze	k1gFnSc2	Vázqueze
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
pokračovatelem	pokračovatel	k1gMnSc7	pokračovatel
byl	být	k5eAaImAgMnS	být
mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
2010	[number]	k4	2010
a	a	k8xC	a
2015	[number]	k4	2015
José	Josá	k1gFnPc4	Josá
Mujica	Mujic	k1gInSc2	Mujic
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
strávil	strávit	k5eAaPmAgInS	strávit
téměř	téměř	k6eAd1	téměř
15	[number]	k4	15
let	léto	k1gNnPc2	léto
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
během	během	k7c2	během
vojenské	vojenský	k2eAgFnSc2d1	vojenská
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Vázquez	Vázquez	k1gMnSc1	Vázquez
se	se	k3xPyFc4	se
prezidentské	prezidentský	k2eAgFnSc2d1	prezidentská
funkce	funkce	k1gFnSc2	funkce
ujal	ujmout	k5eAaPmAgInS	ujmout
podruhé	podruhé	k6eAd1	podruhé
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poloha	poloha	k1gFnSc1	poloha
===	===	k?	===
</s>
</p>
<p>
<s>
Uruguay	Uruguay	k1gFnSc1	Uruguay
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
<g/>
,	,	kIx,	,
v	v	k7c6	v
regionu	region	k1gInSc6	region
Cono	Cono	k1gMnSc1	Cono
Sur	Sur	k1gMnSc1	Sur
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
druhý	druhý	k4xOgInSc4	druhý
nejmenší	malý	k2eAgInSc4d3	nejmenší
jihoamerický	jihoamerický	k2eAgInSc4d1	jihoamerický
stát	stát	k1gInSc4	stát
(	(	kIx(	(
<g/>
menší	malý	k2eAgFnSc1d2	menší
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
Surinam	Surinam	k1gInSc1	Surinam
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
sousedí	sousedit	k5eAaImIp3nS	sousedit
s	s	k7c7	s
Brazílií	Brazílie	k1gFnSc7	Brazílie
(	(	kIx(	(
<g/>
délka	délka	k1gFnSc1	délka
hranice	hranice	k1gFnSc2	hranice
je	být	k5eAaImIp3nS	být
985	[number]	k4	985
km	km	kA	km
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
západě	západ	k1gInSc6	západ
s	s	k7c7	s
Argentinou	Argentina	k1gFnSc7	Argentina
(	(	kIx(	(
<g/>
579	[number]	k4	579
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pobřeží	pobřeží	k1gNnSc1	pobřeží
je	být	k5eAaImIp3nS	být
dlouhé	dlouhý	k2eAgNnSc4d1	dlouhé
660	[number]	k4	660
km	km	kA	km
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
část	část	k1gFnSc1	část
je	být	k5eAaImIp3nS	být
omývána	omývat	k5eAaImNgFnS	omývat
vodami	voda	k1gFnPc7	voda
Río	Río	k1gFnSc2	Río
de	de	k?	de
la	la	k1gNnSc1	la
Plata	plato	k1gNnSc2	plato
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Krajina	Krajina	k1gFnSc1	Krajina
===	===	k?	===
</s>
</p>
<p>
<s>
Území	území	k1gNnSc1	území
Uruguaye	Uruguay	k1gFnSc2	Uruguay
je	být	k5eAaImIp3nS	být
geograficky	geograficky	k6eAd1	geograficky
pokračováním	pokračování	k1gNnSc7	pokračování
argentinské	argentinský	k2eAgFnSc2d1	Argentinská
pampy	pampa	k1gFnSc2	pampa
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
přechází	přecházet	k5eAaImIp3nS	přecházet
do	do	k7c2	do
pahorkatiny	pahorkatina	k1gFnSc2	pahorkatina
tvořené	tvořený	k2eAgInPc1d1	tvořený
nízkými	nízký	k2eAgInPc7d1	nízký
zaoblenými	zaoblený	k2eAgInPc7d1	zaoblený
hřbety	hřbet	k1gInPc7	hřbet
zvanými	zvaný	k2eAgInPc7d1	zvaný
cuchillas	cuchillas	k1gInSc4	cuchillas
a	a	k8xC	a
plynule	plynule	k6eAd1	plynule
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
Brazilskou	brazilský	k2eAgFnSc4d1	brazilská
vysočinu	vysočina	k1gFnSc4	vysočina
<g/>
.	.	kIx.	.
</s>
<s>
Jih	jih	k1gInSc1	jih
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
rovinatý	rovinatý	k2eAgMnSc1d1	rovinatý
<g/>
,	,	kIx,	,
jen	jen	k9	jen
u	u	k7c2	u
pobřeží	pobřeží	k1gNnSc2	pobřeží
se	se	k3xPyFc4	se
prudce	prudko	k6eAd1	prudko
svažuje	svažovat	k5eAaImIp3nS	svažovat
<g/>
.	.	kIx.	.
</s>
<s>
Podél	podél	k7c2	podél
řeky	řeka	k1gFnSc2	řeka
Uruguay	Uruguay	k1gFnSc1	Uruguay
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
močálovité	močálovitý	k2eAgFnPc4d1	močálovitá
roviny	rovina	k1gFnPc4	rovina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
bývají	bývat	k5eAaImIp3nP	bývat
často	často	k6eAd1	často
zaplavovány	zaplavován	k2eAgFnPc1d1	zaplavována
<g/>
.	.	kIx.	.
</s>
<s>
Střed	střed	k1gInSc1	střed
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
nízká	nízký	k2eAgFnSc1d1	nízká
planina	planina	k1gFnSc1	planina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
místy	místy	k6eAd1	místy
pozvedá	pozvedat	k5eAaImIp3nS	pozvedat
v	v	k7c4	v
pahorky	pahorek	k1gInPc4	pahorek
až	až	k9	až
do	do	k7c2	do
výše	výše	k1gFnSc2	výše
500	[number]	k4	500
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Terénní	terénní	k2eAgInPc4d1	terénní
zlomy	zlom	k1gInPc4	zlom
a	a	k8xC	a
svědecké	svědecký	k2eAgInPc4d1	svědecký
pahorky	pahorek	k1gInPc4	pahorek
krajině	krajina	k1gFnSc3	krajina
propůjčují	propůjčovat	k5eAaImIp3nP	propůjčovat
zřetelně	zřetelně	k6eAd1	zřetelně
pahorkovitý	pahorkovitý	k2eAgInSc4d1	pahorkovitý
charakter	charakter	k1gInSc4	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Jihovýchodní	jihovýchodní	k2eAgNnSc1d1	jihovýchodní
pobřeží	pobřeží	k1gNnSc1	pobřeží
je	být	k5eAaImIp3nS	být
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
na	na	k7c4	na
písečné	písečný	k2eAgFnPc4d1	písečná
pláže	pláž	k1gFnPc4	pláž
a	a	k8xC	a
doliny	dolina	k1gFnPc4	dolina
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ploché	plochý	k2eAgNnSc1d1	ploché
<g/>
,	,	kIx,	,
vesměs	vesměs	k6eAd1	vesměs
písčité	písčitý	k2eAgInPc1d1	písčitý
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
však	však	k9	však
znepřístupněné	znepřístupněný	k2eAgInPc4d1	znepřístupněný
skalními	skalní	k2eAgInPc7d1	skalní
ostrůvky	ostrůvek	k1gInPc7	ostrůvek
a	a	k8xC	a
útesy	útes	k1gInPc7	útes
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
komplikovalo	komplikovat	k5eAaBmAgNnS	komplikovat
stavbu	stavba	k1gFnSc4	stavba
přístavů	přístav	k1gInPc2	přístav
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
země	zem	k1gFnSc2	zem
jsou	být	k5eAaImIp3nP	být
časté	častý	k2eAgFnPc1d1	častá
vyvýšeniny	vyvýšenina	k1gFnPc1	vyvýšenina
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Cuchilla	Cuchilla	k1gMnSc1	Cuchilla
de	de	k?	de
Haedo	Haedo	k1gNnSc4	Haedo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nikde	nikde	k6eAd1	nikde
nepřevyšují	převyšovat	k5eNaImIp3nP	převyšovat
hranici	hranice	k1gFnSc4	hranice
600	[number]	k4	600
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
bodem	bod	k1gInSc7	bod
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
Cerro	Cerro	k1gNnSc1	Cerro
Catedral	Catedral	k1gFnSc2	Catedral
v	v	k7c4	v
pohoří	pohoří	k1gNnSc4	pohoří
Carapé	Carapý	k2eAgFnSc2d1	Carapý
(	(	kIx(	(
<g/>
514	[number]	k4	514
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
vyvýšeniny	vyvýšenina	k1gFnPc1	vyvýšenina
Cerro	Cerro	k1gNnSc1	Cerro
Ventana	Ventana	k1gFnSc1	Ventana
(	(	kIx(	(
<g/>
420	[number]	k4	420
m	m	kA	m
<g/>
)	)	kIx)	)
a	a	k8xC	a
Cerro	Cerro	k1gNnSc1	Cerro
Colorado	Colorado	k1gNnSc1	Colorado
(	(	kIx(	(
<g/>
299	[number]	k4	299
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejnižší	nízký	k2eAgInSc1d3	nejnižší
bod	bod	k1gInSc1	bod
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
mořské	mořský	k2eAgFnSc6d1	mořská
hladině	hladina	k1gFnSc6	hladina
<g/>
.	.	kIx.	.
</s>
<s>
Půda	půda	k1gFnSc1	půda
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Uruguayi	Uruguay	k1gFnSc6	Uruguay
úrodná	úrodný	k2eAgFnSc1d1	úrodná
a	a	k8xC	a
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
zemi	zem	k1gFnSc6	zem
zemědělsky	zemědělsky	k6eAd1	zemědělsky
využívána	využívat	k5eAaPmNgFnS	využívat
<g/>
.	.	kIx.	.
</s>
<s>
Lesy	les	k1gInPc1	les
zaujímají	zaujímat	k5eAaImIp3nP	zaujímat
jen	jen	k9	jen
kolem	kolem	k7c2	kolem
5	[number]	k4	5
%	%	kIx~	%
celkové	celkový	k2eAgFnSc2d1	celková
plochy	plocha	k1gFnSc2	plocha
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Klima	klima	k1gNnSc1	klima
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
klima	klima	k1gNnSc1	klima
subtropické	subtropický	k2eAgNnSc1d1	subtropické
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
mírné	mírný	k2eAgNnSc1d1	mírné
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
třeba	třeba	k6eAd1	třeba
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
nebo	nebo	k8xC	nebo
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
,	,	kIx,	,
s	s	k7c7	s
pevně	pevně	k6eAd1	pevně
danými	daný	k2eAgFnPc7d1	daná
ročními	roční	k2eAgFnPc7d1	roční
dobami	doba	k1gFnPc7	doba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
teplota	teplota	k1gFnSc1	teplota
je	být	k5eAaImIp3nS	být
17,5	[number]	k4	17,5
°	°	k?	°
<g/>
C.	C.	kA	C.
Nejteplejší	teplý	k2eAgInSc4d3	nejteplejší
měsíc	měsíc	k1gInSc4	měsíc
je	být	k5eAaImIp3nS	být
leden	leden	k1gInSc1	leden
(	(	kIx(	(
<g/>
32	[number]	k4	32
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejchladnějším	chladný	k2eAgNnSc7d3	nejchladnější
červen	červen	k1gInSc4	červen
(	(	kIx(	(
<g/>
6	[number]	k4	6
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Srážky	srážka	k1gFnPc1	srážka
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Uruguayi	Uruguay	k1gFnSc6	Uruguay
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
průměrně	průměrně	k6eAd1	průměrně
1000	[number]	k4	1000
mm	mm	kA	mm
<g/>
/	/	kIx~	/
<g/>
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
v	v	k7c6	v
nejvodnatějších	vodnatý	k2eAgFnPc6d3	nejvodnatější
oblastech	oblast	k1gFnPc6	oblast
na	na	k7c6	na
severu	sever	k1gInSc6	sever
země	zem	k1gFnSc2	zem
až	až	k9	až
1400	[number]	k4	1400
mm	mm	kA	mm
<g/>
/	/	kIx~	/
<g/>
rok	rok	k1gInSc1	rok
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
června	červen	k1gInSc2	červen
do	do	k7c2	do
prosince	prosinec	k1gInSc2	prosinec
padá	padat	k5eAaImIp3nS	padat
celkově	celkově	k6eAd1	celkově
méně	málo	k6eAd2	málo
srážek	srážka	k1gFnPc2	srážka
než	než	k8xS	než
od	od	k7c2	od
ledna	leden	k1gInSc2	leden
do	do	k7c2	do
května	květen	k1gInSc2	květen
<g/>
.	.	kIx.	.
</s>
<s>
Měsíc	měsíc	k1gInSc1	měsíc
s	s	k7c7	s
největším	veliký	k2eAgNnSc7d3	veliký
množstvím	množství	k1gNnSc7	množství
srážek	srážka	k1gFnPc2	srážka
je	být	k5eAaImIp3nS	být
prosinec	prosinec	k1gInSc1	prosinec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Politika	politikum	k1gNnSc2	politikum
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
Uruguayská	uruguayský	k2eAgFnSc1d1	Uruguayská
ústava	ústava	k1gFnSc1	ústava
byla	být	k5eAaImAgFnS	být
přijata	přijat	k2eAgFnSc1d1	přijata
18	[number]	k4	18
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1830	[number]	k4	1830
a	a	k8xC	a
silně	silně	k6eAd1	silně
se	se	k3xPyFc4	se
orientovala	orientovat	k5eAaBmAgFnS	orientovat
podle	podle	k7c2	podle
španělské	španělský	k2eAgFnSc2d1	španělská
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
první	první	k4xOgFnSc3	první
revizi	revize	k1gFnSc3	revize
původní	původní	k2eAgFnSc2d1	původní
ústavy	ústava	k1gFnSc2	ústava
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
Batlleho	Batlleha	k1gFnSc5	Batlleha
reformami	reforma	k1gFnPc7	reforma
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnPc1d1	následující
celkové	celkový	k2eAgFnPc1d1	celková
reformy	reforma	k1gFnPc1	reforma
ústavy	ústava	k1gFnSc2	ústava
následovaly	následovat	k5eAaImAgFnP	následovat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1934	[number]	k4	1934
<g/>
,	,	kIx,	,
1942	[number]	k4	1942
<g/>
,	,	kIx,	,
1951	[number]	k4	1951
<g/>
,	,	kIx,	,
1967	[number]	k4	1967
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejvýraznějším	výrazný	k2eAgFnPc3d3	nejvýraznější
změnám	změna	k1gFnPc3	změna
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
ústavě	ústava	k1gFnSc6	ústava
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1951	[number]	k4	1951
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
zavedena	zaveden	k2eAgFnSc1d1	zavedena
kolektivní	kolektivní	k2eAgFnSc1d1	kolektivní
forma	forma	k1gFnSc1	forma
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
systém	systém	k1gInSc1	systém
rotačního	rotační	k2eAgNnSc2d1	rotační
prezidentství	prezidentství	k1gNnSc2	prezidentství
podle	podle	k7c2	podle
švýcarského	švýcarský	k2eAgInSc2d1	švýcarský
vzoru	vzor	k1gInSc2	vzor
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
se	se	k3xPyFc4	se
země	země	k1gFnSc1	země
vrátila	vrátit	k5eAaPmAgFnS	vrátit
k	k	k7c3	k
prezidentskému	prezidentský	k2eAgInSc3d1	prezidentský
systému	systém	k1gInSc3	systém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Výkonná	výkonný	k2eAgFnSc1d1	výkonná
moc	moc	k1gFnSc1	moc
je	být	k5eAaImIp3nS	být
svěřená	svěřený	k2eAgFnSc1d1	svěřená
prezidentovi	prezident	k1gMnSc3	prezident
<g/>
,	,	kIx,	,
volenému	volený	k2eAgMnSc3d1	volený
na	na	k7c4	na
5	[number]	k4	5
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
současně	současně	k6eAd1	současně
předsedou	předseda	k1gMnSc7	předseda
vlády	vláda	k1gFnSc2	vláda
i	i	k8xC	i
hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zákonodárná	zákonodárný	k2eAgFnSc1d1	zákonodárná
moc	moc	k1gFnSc1	moc
je	být	k5eAaImIp3nS	být
vykonávána	vykonávat	k5eAaImNgFnS	vykonávat
dvoukomorovým	dvoukomorový	k2eAgInSc7d1	dvoukomorový
parlamentem	parlament	k1gInSc7	parlament
(	(	kIx(	(
<g/>
Asamblea	Asamblea	k1gFnSc1	Asamblea
General	General	k1gFnPc7	General
<g/>
)	)	kIx)	)
složeným	složený	k2eAgInPc3d1	složený
ze	z	k7c2	z
30	[number]	k4	30
<g/>
členného	členný	k2eAgInSc2d1	členný
senátu	senát	k1gInSc2	senát
(	(	kIx(	(
<g/>
Cámara	Cámara	k1gFnSc1	Cámara
de	de	k?	de
Senadores	Senadores	k1gInSc1	Senadores
<g/>
)	)	kIx)	)
a	a	k8xC	a
99	[number]	k4	99
<g/>
členné	členný	k2eAgFnSc2d1	členná
Sněmovny	sněmovna	k1gFnSc2	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
<g/>
/	/	kIx~	/
<g/>
poslanců	poslanec	k1gMnPc2	poslanec
(	(	kIx(	(
<g/>
Cámara	Cámara	k1gFnSc1	Cámara
de	de	k?	de
Representantes	Representantes	k1gInSc1	Representantes
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Volby	volba	k1gFnPc1	volba
jsou	být	k5eAaImIp3nP	být
všeobecné	všeobecný	k2eAgFnPc1d1	všeobecná
a	a	k8xC	a
přímé	přímý	k2eAgFnPc1d1	přímá
<g/>
.	.	kIx.	.
</s>
<s>
Volební	volební	k2eAgInSc1d1	volební
mandát	mandát	k1gInSc1	mandát
je	být	k5eAaImIp3nS	být
5	[number]	k4	5
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Soudní	soudní	k2eAgFnSc1d1	soudní
moc	moc	k1gFnSc1	moc
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc4	jehož
soudci	soudce	k1gMnPc1	soudce
jsou	být	k5eAaImIp3nP	být
jmenováni	jmenovat	k5eAaBmNgMnP	jmenovat
parlamentem	parlament	k1gInSc7	parlament
na	na	k7c6	na
10	[number]	k4	10
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
plní	plnit	k5eAaImIp3nS	plnit
úlohu	úloha	k1gFnSc4	úloha
ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
i	i	k9	i
poslední	poslední	k2eAgFnSc7d1	poslední
odvolací	odvolací	k2eAgFnSc7d1	odvolací
instancí	instance	k1gFnSc7	instance
místních	místní	k2eAgInPc2d1	místní
soudů	soud	k1gInPc2	soud
<g/>
.	.	kIx.	.
<g/>
Uruguayská	uruguayský	k2eAgFnSc1d1	Uruguayská
ústava	ústava	k1gFnSc1	ústava
také	také	k9	také
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
občanům	občan	k1gMnPc3	občan
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
rušili	rušit	k5eAaImAgMnP	rušit
právní	právní	k2eAgInPc4d1	právní
předpisy	předpis	k1gInPc4	předpis
nebo	nebo	k8xC	nebo
měnili	měnit	k5eAaImAgMnP	měnit
ústavu	ústava	k1gFnSc4	ústava
iniciativou	iniciativa	k1gFnSc7	iniciativa
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
může	moct	k5eAaImIp3nS	moct
vyvrcholit	vyvrcholit	k5eAaPmF	vyvrcholit
celostátním	celostátní	k2eAgNnSc7d1	celostátní
referendem	referendum	k1gNnSc7	referendum
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
posledních	poslední	k2eAgNnPc2d1	poslední
15	[number]	k4	15
let	léto	k1gNnPc2	léto
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
metoda	metoda	k1gFnSc1	metoda
použita	použít	k5eAaPmNgFnS	použít
několikrát	několikrát	k6eAd1	několikrát
<g/>
:	:	kIx,	:
k	k	k7c3	k
potvrzení	potvrzení	k1gNnSc3	potvrzení
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
vzdání	vzdání	k1gNnSc6	vzdání
se	se	k3xPyFc4	se
stíhání	stíhání	k1gNnSc1	stíhání
členů	člen	k1gMnPc2	člen
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
porušovali	porušovat	k5eAaImAgMnP	porušovat
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
během	během	k7c2	během
vojenského	vojenský	k2eAgInSc2d1	vojenský
režimu	režim	k1gInSc2	režim
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
–	–	k?	–
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
k	k	k7c3	k
zastavení	zastavení	k1gNnSc3	zastavení
privatizace	privatizace	k1gFnSc2	privatizace
podniků	podnik	k1gInPc2	podnik
veřejných	veřejný	k2eAgFnPc2d1	veřejná
služeb	služba	k1gFnPc2	služba
<g/>
;	;	kIx,	;
k	k	k7c3	k
ochraně	ochrana	k1gFnSc3	ochrana
příjmů	příjem	k1gInPc2	příjem
důchodců	důchodce	k1gMnPc2	důchodce
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
vodních	vodní	k2eAgInPc2d1	vodní
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Administrativní	administrativní	k2eAgNnSc1d1	administrativní
dělení	dělení	k1gNnSc1	dělení
===	===	k?	===
</s>
</p>
<p>
<s>
Uruguay	Uruguay	k1gFnSc1	Uruguay
je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
do	do	k7c2	do
19	[number]	k4	19
historicky	historicky	k6eAd1	historicky
vzniklých	vzniklý	k2eAgInPc2d1	vzniklý
departementů	departement	k1gInPc2	departement
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
departamentos	departamentos	k1gInSc1	departamentos
<g/>
,	,	kIx,	,
jednotné	jednotný	k2eAgNnSc1d1	jednotné
číslo	číslo	k1gNnSc1	číslo
departamento	departamento	k1gNnSc1	departamento
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
však	však	k9	však
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
centralistickému	centralistický	k2eAgInSc3d1	centralistický
státu	stát	k1gInSc3	stát
mají	mít	k5eAaImIp3nP	mít
jen	jen	k9	jen
poměrně	poměrně	k6eAd1	poměrně
málo	málo	k4c4	málo
pravomocí	pravomoc	k1gFnPc2	pravomoc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
departementů	departement	k1gInPc2	departement
stojí	stát	k5eAaImIp3nP	stát
Intendente	Intendent	k1gMnSc5	Intendent
Municipal	Municipal	k1gMnSc4	Municipal
volený	volený	k2eAgMnSc1d1	volený
z	z	k7c2	z
parlamentů	parlament	k1gInPc2	parlament
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
departementů	departement	k1gInPc2	departement
(	(	kIx(	(
<g/>
junta	junta	k1gFnSc1	junta
departamental	departamental	k1gMnSc1	departamental
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekonomika	ekonomika	k1gFnSc1	ekonomika
==	==	k?	==
</s>
</p>
<p>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejvyspělejších	vyspělý	k2eAgInPc2d3	nejvyspělejší
států	stát	k1gInPc2	stát
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
politicky	politicky	k6eAd1	politicky
vyrovnaná	vyrovnaný	k2eAgFnSc1d1	vyrovnaná
země	země	k1gFnSc1	země
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yQnSc2	což
ekonomika	ekonomika	k1gFnSc1	ekonomika
hlavně	hlavně	k9	hlavně
těží	těžet	k5eAaImIp3nS	těžet
<g/>
.	.	kIx.	.
</s>
<s>
Uruguay	Uruguay	k1gFnSc4	Uruguay
má	mít	k5eAaImIp3nS	mít
druhý	druhý	k4xOgMnSc1	druhý
nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
HDP	HDP	kA	HDP
(	(	kIx(	(
<g/>
v	v	k7c6	v
paritě	parita	k1gFnSc6	parita
kupní	kupní	k2eAgFnSc2d1	kupní
síly	síla	k1gFnSc2	síla
<g/>
)	)	kIx)	)
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgInSc4	třetí
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
index	index	k1gInSc4	index
lidského	lidský	k2eAgInSc2d1	lidský
rozvoje	rozvoj	k1gInSc2	rozvoj
a	a	k8xC	a
druhou	druhý	k4xOgFnSc4	druhý
nejnižší	nízký	k2eAgFnSc4d3	nejnižší
příjmovou	příjmový	k2eAgFnSc4d1	příjmová
nerovnost	nerovnost	k1gFnSc4	nerovnost
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
<g/>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
hospodářství	hospodářství	k1gNnSc2	hospodářství
země	země	k1gFnSc1	země
otřáslo	otřást	k5eAaPmAgNnS	otřást
v	v	k7c6	v
základech	základ	k1gInPc6	základ
<g/>
.	.	kIx.	.
</s>
<s>
Uruguay	Uruguay	k1gFnSc1	Uruguay
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1999-2002	[number]	k4	1999-2002
velkou	velký	k2eAgFnSc4d1	velká
hospodářskou	hospodářský	k2eAgFnSc4d1	hospodářská
a	a	k8xC	a
finanční	finanční	k2eAgFnSc4d1	finanční
krizi	krize	k1gFnSc4	krize
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
převážně	převážně	k6eAd1	převážně
odrazem	odraz	k1gInSc7	odraz
problémů	problém	k1gInPc2	problém
Argentiny	Argentina	k1gFnSc2	Argentina
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomika	ekonomika	k1gFnSc1	ekonomika
poklesla	poklesnout	k5eAaPmAgFnS	poklesnout
o	o	k7c4	o
11	[number]	k4	11
<g/>
%	%	kIx~	%
a	a	k8xC	a
nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
21	[number]	k4	21
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
závažnosti	závažnost	k1gFnSc3	závažnost
problémů	problém	k1gInPc2	problém
zůstaly	zůstat	k5eAaPmAgInP	zůstat
finanční	finanční	k2eAgInPc1d1	finanční
ukazatele	ukazatel	k1gInPc1	ukazatel
stabilnější	stabilní	k2eAgInPc1d2	stabilnější
než	než	k8xS	než
u	u	k7c2	u
sousedů	soused	k1gMnPc2	soused
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
díky	díky	k7c3	díky
solidní	solidní	k2eAgFnSc3d1	solidní
pověsti	pověst	k1gFnSc3	pověst
mezi	mezi	k7c7	mezi
investory	investor	k1gMnPc7	investor
a	a	k8xC	a
ratingu	rating	k1gInSc6	rating
státních	státní	k2eAgInPc2d1	státní
dluhopisů	dluhopis	k1gInPc2	dluhopis
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
je	být	k5eAaImIp3nS	být
druhý	druhý	k4xOgMnSc1	druhý
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
2004-2008	[number]	k4	2004-2008
již	již	k9	již
ekonomika	ekonomika	k1gFnSc1	ekonomika
znovu	znovu	k6eAd1	znovu
rostla	růst	k5eAaImAgFnS	růst
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
o	o	k7c4	o
6,7	[number]	k4	6,7
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Míra	Míra	k1gFnSc1	Míra
chudoby	chudoba	k1gFnSc2	chudoba
se	se	k3xPyFc4	se
snížila	snížit	k5eAaPmAgFnS	snížit
z	z	k7c2	z
33	[number]	k4	33
<g/>
%	%	kIx~	%
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
na	na	k7c4	na
21,7	[number]	k4	21,7
<g/>
%	%	kIx~	%
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
extrémní	extrémní	k2eAgFnSc1d1	extrémní
chudoba	chudoba	k1gFnSc1	chudoba
klesla	klesnout	k5eAaPmAgFnS	klesnout
z	z	k7c2	z
3,3	[number]	k4	3,3
<g/>
%	%	kIx~	%
na	na	k7c4	na
1,7	[number]	k4	1,7
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgNnSc7	jeden
poučením	poučení	k1gNnSc7	poučení
z	z	k7c2	z
krize	krize	k1gFnSc2	krize
byla	být	k5eAaImAgFnS	být
snaha	snaha	k1gFnSc1	snaha
snížit	snížit	k5eAaPmF	snížit
závislost	závislost	k1gFnSc4	závislost
uruguayské	uruguayský	k2eAgFnSc2d1	Uruguayská
ekonomiky	ekonomika	k1gFnSc2	ekonomika
na	na	k7c4	na
argentinské	argentinský	k2eAgMnPc4d1	argentinský
a	a	k8xC	a
brazilské	brazilský	k2eAgMnPc4d1	brazilský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejdůležitějším	důležitý	k2eAgInSc7d3	nejdůležitější
průmyslem	průmysl	k1gInSc7	průmysl
je	být	k5eAaImIp3nS	být
potravinářský	potravinářský	k2eAgInSc1d1	potravinářský
a	a	k8xC	a
textilní	textilní	k2eAgInSc1d1	textilní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
intenzivním	intenzivní	k2eAgNnSc6d1	intenzivní
zemědělství	zemědělství	k1gNnSc6	zemědělství
je	být	k5eAaImIp3nS	být
zastoupeno	zastoupit	k5eAaPmNgNnS	zastoupit
stále	stále	k6eAd1	stále
ještě	ještě	k9	ještě
pastevectví	pastevectví	k1gNnSc1	pastevectví
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
území	území	k1gNnSc6	území
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
tradici	tradice	k1gFnSc4	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Pěstují	pěstovat	k5eAaImIp3nP	pěstovat
se	se	k3xPyFc4	se
veškeré	veškerý	k3xTgFnPc1	veškerý
světové	světový	k2eAgFnPc1d1	světová
plodiny	plodina	k1gFnPc1	plodina
<g/>
.	.	kIx.	.
</s>
<s>
Důležitý	důležitý	k2eAgInSc1d1	důležitý
je	být	k5eAaImIp3nS	být
přístav	přístav	k1gInSc1	přístav
Montevideo	Montevideo	k1gNnSc4	Montevideo
<g/>
,	,	kIx,	,
z	z	k7c2	z
kterého	který	k3yIgMnSc2	který
plynou	plynout	k5eAaImIp3nP	plynout
do	do	k7c2	do
státní	státní	k2eAgFnSc2d1	státní
pokladny	pokladna	k1gFnSc2	pokladna
významné	významný	k2eAgInPc4d1	významný
finanční	finanční	k2eAgInPc4d1	finanční
prostředky	prostředek	k1gInPc4	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Cestovní	cestovní	k2eAgInSc1d1	cestovní
ruch	ruch	k1gInSc1	ruch
je	být	k5eAaImIp3nS	být
rozvinutý	rozvinutý	k2eAgMnSc1d1	rozvinutý
a	a	k8xC	a
velkou	velký	k2eAgFnSc7d1	velká
měrou	míra	k1gFnSc7wR	míra
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
na	na	k7c6	na
celkovém	celkový	k2eAgNnSc6d1	celkové
HDP	HDP	kA	HDP
země	zem	k1gFnPc4	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
dluh	dluh	k1gInSc1	dluh
činí	činit	k5eAaImIp3nS	činit
64,1	[number]	k4	64,1
procenta	procento	k1gNnSc2	procento
HDP	HDP	kA	HDP
(	(	kIx(	(
<g/>
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
vývozu	vývoz	k1gInSc2	vývoz
z	z	k7c2	z
Uruguaye	Uruguay	k1gFnSc2	Uruguay
míří	mířit	k5eAaImIp3nS	mířit
do	do	k7c2	do
Číny	Čína	k1gFnSc2	Čína
<g/>
,	,	kIx,	,
Brazílie	Brazílie	k1gFnSc2	Brazílie
a	a	k8xC	a
Argentiny	Argentina	k1gFnSc2	Argentina
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
tři	tři	k4xCgInPc1	tři
státy	stát	k1gInPc1	stát
dominují	dominovat	k5eAaImIp3nP	dominovat
i	i	k9	i
v	v	k7c6	v
dovozech	dovoz	k1gInPc6	dovoz
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
vývozu	vývoz	k1gInSc2	vývoz
tvoří	tvořit	k5eAaImIp3nP	tvořit
potraviny	potravina	k1gFnPc1	potravina
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
hovězí	hovězí	k2eAgNnSc4d1	hovězí
maso	maso	k1gNnSc4	maso
<g/>
,	,	kIx,	,
sojové	sojový	k2eAgInPc4d1	sojový
boby	bob	k1gInPc4	bob
a	a	k8xC	a
nápojové	nápojový	k2eAgInPc4d1	nápojový
koncentráty	koncentrát	k1gInPc4	koncentrát
<g/>
.	.	kIx.	.
</s>
<s>
Významný	významný	k2eAgInSc1d1	významný
je	být	k5eAaImIp3nS	být
také	také	k9	také
export	export	k1gInSc1	export
celulózy	celulóza	k1gFnSc2	celulóza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
11	[number]	k4	11
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2013	[number]	k4	2013
Uruguay	Uruguay	k1gFnSc1	Uruguay
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
první	první	k4xOgFnPc1	první
země	zem	k1gFnPc1	zem
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
legalizovala	legalizovat	k5eAaBmAgFnS	legalizovat
pěstování	pěstování	k1gNnSc3	pěstování
<g/>
,	,	kIx,	,
užívání	užívání	k1gNnSc3	užívání
i	i	k8xC	i
prodej	prodej	k1gInSc1	prodej
konopí	konopí	k1gNnSc2	konopí
<g/>
.	.	kIx.	.
<g/>
Téměř	téměř	k6eAd1	téměř
95	[number]	k4	95
<g/>
%	%	kIx~	%
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
v	v	k7c6	v
Uruguayi	Uruguay	k1gFnSc6	Uruguay
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
obnovitelných	obnovitelný	k2eAgInPc2d1	obnovitelný
zdrojů	zdroj	k1gInPc2	zdroj
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
z	z	k7c2	z
vodních	vodní	k2eAgFnPc2d1	vodní
elektráren	elektrárna	k1gFnPc2	elektrárna
a	a	k8xC	a
větrných	větrný	k2eAgInPc2d1	větrný
parků	park	k1gInPc2	park
<g/>
.	.	kIx.	.
<g/>
Proslulé	proslulý	k2eAgFnPc1d1	proslulá
jsou	být	k5eAaImIp3nP	být
rovněž	rovněž	k9	rovněž
výsledky	výsledek	k1gInPc1	výsledek
v	v	k7c6	v
elektronizaci	elektronizace	k1gFnSc6	elektronizace
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
a	a	k8xC	a
života	život	k1gInSc2	život
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
se	se	k3xPyFc4	se
Uruguay	Uruguay	k1gFnSc1	Uruguay
například	například	k6eAd1	například
stala	stát	k5eAaPmAgFnS	stát
první	první	k4xOgInSc4	první
zemí	zem	k1gFnPc2	zem
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
poskytla	poskytnout	k5eAaPmAgFnS	poskytnout
každému	každý	k3xTgMnSc3	každý
žáku	žák	k1gMnSc3	žák
základní	základní	k2eAgFnSc2d1	základní
školy	škola	k1gFnSc2	škola
přenosný	přenosný	k2eAgInSc4d1	přenosný
osobní	osobní	k2eAgInSc4d1	osobní
počítač	počítač	k1gInSc4	počítač
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
Uruguaye	Uruguay	k1gFnSc2	Uruguay
bydlí	bydlet	k5eAaImIp3nS	bydlet
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
byl	být	k5eAaImAgInS	být
jejich	jejich	k3xOp3gInSc1	jejich
počet	počet	k1gInSc1	počet
po	po	k7c4	po
sčítání	sčítání	k1gNnSc4	sčítání
lidu	lid	k1gInSc2	lid
odhadován	odhadovat	k5eAaImNgInS	odhadovat
na	na	k7c4	na
3,316,328	[number]	k4	3,316,328
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
2,239,000	[number]	k4	2,239,000
obyvatel	obyvatel	k1gMnPc2	obyvatel
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
0.93	[number]	k4	0.93
mužů	muž	k1gMnPc2	muž
<g/>
/	/	kIx~	/
<g/>
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Uruguay	Uruguay	k1gFnSc1	Uruguay
je	být	k5eAaImIp3nS	být
nejvíce	nejvíce	k6eAd1	nejvíce
urbanizovanou	urbanizovaný	k2eAgFnSc7d1	urbanizovaná
zemí	zem	k1gFnSc7	zem
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
91,8	[number]	k4	91,8
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
žije	žít	k5eAaImIp3nS	žít
ve	v	k7c6	v
městech	město	k1gNnPc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Skoro	skoro	k6eAd1	skoro
polovina	polovina	k1gFnSc1	polovina
obyvatel	obyvatel	k1gMnPc2	obyvatel
země	země	k1gFnSc1	země
bydlí	bydlet	k5eAaImIp3nS	bydlet
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
Montevideo	Montevideo	k1gNnSc1	Montevideo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
má	mít	k5eAaImIp3nS	mít
1,4	[number]	k4	1,4
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Předkové	předek	k1gMnPc1	předek
většiny	většina	k1gFnSc2	většina
Uruguayců	Uruguayec	k1gMnPc2	Uruguayec
(	(	kIx(	(
<g/>
okolo	okolo	k7c2	okolo
88	[number]	k4	88
%	%	kIx~	%
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
potomci	potomek	k1gMnPc1	potomek
evropských	evropský	k2eAgInPc2d1	evropský
kolonizátorů	kolonizátor	k1gMnPc2	kolonizátor
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
sem	sem	k6eAd1	sem
přišli	přijít	k5eAaPmAgMnP	přijít
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ponejvíce	ponejvíce	k6eAd1	ponejvíce
ze	z	k7c2	z
Španělska	Španělsko	k1gNnSc2	Španělsko
a	a	k8xC	a
Itálie	Itálie	k1gFnSc2	Itálie
(	(	kIx(	(
<g/>
asi	asi	k9	asi
jedna	jeden	k4xCgFnSc1	jeden
čtvrtina	čtvrtina	k1gFnSc1	čtvrtina
populace	populace	k1gFnSc2	populace
je	být	k5eAaImIp3nS	být
italském	italský	k2eAgInSc6d1	italský
původu	původ	k1gInSc6	původ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
pří	přít	k5eAaImIp3nP	přít
sčítání	sčítání	k1gNnSc4	sčítání
lidu	lid	k1gInSc3	lid
Státním	státní	k2eAgInSc7d1	státní
statistickým	statistický	k2eAgInSc7d1	statistický
úřadem	úřad	k1gInSc7	úřad
(	(	kIx(	(
<g/>
Instituto	Institut	k2eAgNnSc1d1	Instituto
Nacional	Nacional	k1gMnSc3	Nacional
de	de	k?	de
Estadística	Estadística	k1gMnSc1	Estadística
(	(	kIx(	(
<g/>
INE	INE	kA	INE
<g/>
))	))	k?	))
v	v	k7c6	v
hodnocení	hodnocení	k1gNnSc6	hodnocení
pro	pro	k7c4	pro
převládající	převládající	k2eAgInSc4d1	převládající
rodový	rodový	k2eAgInSc4d1	rodový
původ	původ	k1gInSc4	původ
(	(	kIx(	(
<g/>
byla	být	k5eAaImAgFnS	být
povolena	povolit	k5eAaPmNgFnS	povolit
jen	jen	k9	jen
jedna	jeden	k4xCgFnSc1	jeden
volba	volba	k1gFnSc1	volba
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
93,9	[number]	k4	93,9
<g/>
%	%	kIx~	%
respondentů	respondent	k1gMnPc2	respondent
uvedlo	uvést	k5eAaPmAgNnS	uvést
převažující	převažující	k2eAgInSc4d1	převažující
evropský	evropský	k2eAgInSc4d1	evropský
rodový	rodový	k2eAgInSc4d1	rodový
původ	původ	k1gInSc4	původ
<g/>
,	,	kIx,	,
4,9	[number]	k4	4,9
<g/>
%	%	kIx~	%
černý	černý	k2eAgMnSc1d1	černý
nebo	nebo	k8xC	nebo
africký	africký	k2eAgMnSc1d1	africký
<g/>
,	,	kIx,	,
1,1	[number]	k4	1,1
<g/>
%	%	kIx~	%
původní	původní	k2eAgInSc1d1	původní
domorodý	domorodý	k2eAgInSc1d1	domorodý
a	a	k8xC	a
0,1	[number]	k4	0,1
<g/>
%	%	kIx~	%
asijský	asijský	k2eAgInSc1d1	asijský
nebo	nebo	k8xC	nebo
žlutý	žlutý	k2eAgInSc1d1	žlutý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nedávné	dávný	k2eNgFnSc6d1	nedávná
době	doba	k1gFnSc6	doba
sem	sem	k6eAd1	sem
také	také	k9	také
přišlo	přijít	k5eAaPmAgNnS	přijít
množství	množství	k1gNnSc1	množství
Brazilců	Brazilec	k1gMnPc2	Brazilec
a	a	k8xC	a
Argentinců	Argentinec	k1gMnPc2	Argentinec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
vlasti	vlast	k1gFnSc2	vlast
uprchli	uprchnout	k5eAaPmAgMnP	uprchnout
buď	buď	k8xC	buď
kvůli	kvůli	k7c3	kvůli
sporům	spor	k1gInPc3	spor
se	se	k3xPyFc4	se
zákony	zákon	k1gInPc7	zákon
či	či	k8xC	či
za	za	k7c7	za
zlepšením	zlepšení	k1gNnSc7	zlepšení
životních	životní	k2eAgFnPc2d1	životní
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Migrační	migrační	k2eAgInSc1d1	migrační
zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
prošel	projít	k5eAaPmAgInS	projít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
dává	dávat	k5eAaImIp3nS	dávat
přistěhovalcům	přistěhovalec	k1gMnPc3	přistěhovalec
stejná	stejný	k2eAgNnPc4d1	stejné
práva	právo	k1gNnPc4	právo
a	a	k8xC	a
příležitosti	příležitost	k1gFnPc4	příležitost
<g/>
,	,	kIx,	,
jaké	jaký	k3yQgFnPc4	jaký
mají	mít	k5eAaImIp3nP	mít
státní	státní	k2eAgMnPc1d1	státní
příslušníci	příslušník	k1gMnPc1	příslušník
<g/>
,	,	kIx,	,
s	s	k7c7	s
předpokladem	předpoklad	k1gInSc7	předpoklad
prokazování	prokazování	k1gNnSc2	prokazování
měsíčního	měsíční	k2eAgInSc2d1	měsíční
příjmu	příjem	k1gInSc2	příjem
$	$	kIx~	$
<g/>
650	[number]	k4	650
(	(	kIx(	(
<g/>
amerických	americký	k2eAgInPc2d1	americký
dolarů	dolar	k1gInPc2	dolar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
skupinou	skupina	k1gFnSc7	skupina
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
okolo	okolo	k7c2	okolo
8	[number]	k4	8
%	%	kIx~	%
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
míšenci	míšenec	k1gMnPc1	míšenec
Evropanů	Evropan	k1gMnPc2	Evropan
a	a	k8xC	a
Indiánů	Indián	k1gMnPc2	Indián
<g/>
.	.	kIx.	.
</s>
<s>
Afrického	africký	k2eAgInSc2d1	africký
původu	původ	k1gInSc2	původ
jsou	být	k5eAaImIp3nP	být
asi	asi	k9	asi
4	[number]	k4	4
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgNnSc1d1	původní
indiánské	indiánský	k2eAgNnSc1d1	indiánské
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
dnes	dnes	k6eAd1	dnes
tvoří	tvořit	k5eAaImIp3nS	tvořit
asi	asi	k9	asi
1	[number]	k4	1
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
skupinou	skupina	k1gFnSc7	skupina
mezi	mezi	k7c4	mezi
indiány	indián	k1gMnPc4	indián
jsou	být	k5eAaImIp3nP	být
kmeny	kmen	k1gInPc7	kmen
Guaraní	Guaraní	k1gNnSc2	Guaraní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
dvou	dva	k4xCgNnPc6	dva
desetiletích	desetiletí	k1gNnPc6	desetiletí
<g/>
[	[	kIx(	[
<g/>
kdy	kdy	k6eAd1	kdy
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
odešlo	odejít	k5eAaPmAgNnS	odejít
okolo	okolo	k7c2	okolo
500	[number]	k4	500
000	[number]	k4	000
Uruguayců	Uruguayec	k1gMnPc2	Uruguayec
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
hlavně	hlavně	k9	hlavně
do	do	k7c2	do
Argentiny	Argentina	k1gFnSc2	Argentina
<g/>
,	,	kIx,	,
Španělska	Španělsko	k1gNnSc2	Španělsko
a	a	k8xC	a
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Díky	díky	k7c3	díky
poměrně	poměrně	k6eAd1	poměrně
malé	malý	k2eAgFnSc3d1	malá
míře	míra	k1gFnSc3	míra
porodnosti	porodnost	k1gFnSc2	porodnost
(	(	kIx(	(
<g/>
15,1	[number]	k4	15,1
‰	‰	k?	‰
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vysoké	vysoký	k2eAgFnSc3d1	vysoká
délce	délka	k1gFnSc3	délka
života	život	k1gInSc2	život
(	(	kIx(	(
<g/>
průměr	průměr	k1gInSc1	průměr
76,4	[number]	k4	76,4
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
a	a	k8xC	a
emigrantům	emigrant	k1gMnPc3	emigrant
(	(	kIx(	(
<g/>
0,32	[number]	k4	0,32
emigrantů	emigrant	k1gMnPc2	emigrant
<g/>
/	/	kIx~	/
<g/>
1	[number]	k4	1
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
uruguayjská	uruguayjský	k2eAgFnSc1d1	uruguayjský
společnost	společnost	k1gFnSc1	společnost
rychle	rychle	k6eAd1	rychle
stárne	stárnout	k5eAaImIp3nS	stárnout
<g/>
.	.	kIx.	.
</s>
<s>
Přírůstek	přírůstek	k1gInSc1	přírůstek
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
je	být	k5eAaImIp3nS	být
ročně	ročně	k6eAd1	ročně
0,24	[number]	k4	0,24
%	%	kIx~	%
(	(	kIx(	(
<g/>
odhad	odhad	k1gInSc1	odhad
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
čtvrtině	čtvrtina	k1gFnSc3	čtvrtina
populace	populace	k1gFnSc2	populace
je	být	k5eAaImIp3nS	být
méně	málo	k6eAd2	málo
než	než	k8xS	než
15	[number]	k4	15
let	léto	k1gNnPc2	léto
a	a	k8xC	a
asi	asi	k9	asi
sedmina	sedmina	k1gFnSc1	sedmina
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
60	[number]	k4	60
a	a	k8xC	a
více	hodně	k6eAd2	hodně
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
je	být	k5eAaImIp3nS	být
španělština	španělština	k1gFnSc1	španělština
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
jelikož	jelikož	k8xS	jelikož
byly	být	k5eAaImAgFnP	být
původními	původní	k2eAgMnPc7d1	původní
kolonizátory	kolonizátor	k1gMnPc7	kolonizátor
Portugalci	Portugalec	k1gMnPc7	Portugalec
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
velmi	velmi	k6eAd1	velmi
rozšířena	rozšířen	k2eAgFnSc1d1	rozšířena
směs	směs	k1gFnSc1	směs
portugalštiny	portugalština	k1gFnSc2	portugalština
a	a	k8xC	a
španělštiny	španělština	k1gFnSc2	španělština
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
portuñ	portuñ	k?	portuñ
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
450	[number]	k4	450
000	[number]	k4	000
Uruguayců	Uruguayec	k1gMnPc2	Uruguayec
má	mít	k5eAaImIp3nS	mít
tento	tento	k3xDgInSc4	tento
jazyk	jazyk	k1gInSc4	jazyk
za	za	k7c4	za
mateřský	mateřský	k2eAgInSc4d1	mateřský
<g/>
,	,	kIx,	,
či	či	k8xC	či
jako	jako	k9	jako
druhý	druhý	k4xOgInSc1	druhý
jazyk	jazyk	k1gInSc1	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
66	66	k4	66
%	%	kIx~	%
Uruguayců	Uruguayec	k1gMnPc2	Uruguayec
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nS	hlásit
ke	k	k7c3	k
katolické	katolický	k2eAgFnSc3d1	katolická
církvi	církev	k1gFnSc3	církev
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
nápadně	nápadně	k6eAd1	nápadně
méně	málo	k6eAd2	málo
oproti	oproti	k7c3	oproti
jiným	jiný	k2eAgFnPc3d1	jiná
latinskoamerickým	latinskoamerický	k2eAgFnPc3d1	latinskoamerická
zemím	zem	k1gFnPc3	zem
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
zřetelně	zřetelně	k6eAd1	zřetelně
vyšší	vysoký	k2eAgFnSc2d2	vyšší
oproti	oproti	k7c3	oproti
okolním	okolní	k2eAgInPc3d1	okolní
státům	stát	k1gInPc3	stát
je	být	k5eAaImIp3nS	být
podíl	podíl	k1gInSc1	podíl
ateistů	ateista	k1gMnPc2	ateista
(	(	kIx(	(
<g/>
30	[number]	k4	30
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protestantů	protestant	k1gMnPc2	protestant
(	(	kIx(	(
<g/>
2	[number]	k4	2
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
židů	žid	k1gMnPc2	žid
(	(	kIx(	(
<g/>
2	[number]	k4	2
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
protestantských	protestantský	k2eAgFnPc2d1	protestantská
skupin	skupina	k1gFnPc2	skupina
mají	mít	k5eAaImIp3nP	mít
zvláštní	zvláštní	k2eAgNnSc4d1	zvláštní
postavení	postavení	k1gNnSc4	postavení
valdenští	valdenští	k1gMnPc1	valdenští
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
si	se	k3xPyFc3	se
v	v	k7c4	v
Uruguay	Uruguay	k1gFnSc4	Uruguay
zbudovali	zbudovat	k5eAaPmAgMnP	zbudovat
svou	svůj	k3xOyFgFnSc4	svůj
jihoamerickou	jihoamerický	k2eAgFnSc4d1	jihoamerická
základnu	základna	k1gFnSc4	základna
<g/>
.	.	kIx.	.
</s>
<s>
Židovská	židovský	k2eAgFnSc1d1	židovská
imigrace	imigrace	k1gFnSc1	imigrace
měla	mít	k5eAaImAgFnS	mít
dvě	dva	k4xCgFnPc4	dva
vlny	vlna	k1gFnPc4	vlna
<g/>
,	,	kIx,	,
Sefardé	Sefardý	k2eAgFnPc4d1	Sefardý
z	z	k7c2	z
Turecka	Turecko	k1gNnSc2	Turecko
přišli	přijít	k5eAaPmAgMnP	přijít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1904	[number]	k4	1904
<g/>
,	,	kIx,	,
Aškenázové	Aškenázová	k1gFnPc1	Aškenázová
z	z	k7c2	z
Ruska	Rusko	k1gNnSc2	Rusko
roku	rok	k1gInSc2	rok
1906	[number]	k4	1906
<g/>
.	.	kIx.	.
</s>
<s>
Usazovali	usazovat	k5eAaImAgMnP	usazovat
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
v	v	k7c6	v
Montevideu	Montevideo	k1gNnSc6	Montevideo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
atmosférou	atmosféra	k1gFnSc7	atmosféra
dosti	dosti	k6eAd1	dosti
lišilo	lišit	k5eAaImAgNnS	lišit
od	od	k7c2	od
jiných	jiný	k2eAgFnPc2d1	jiná
silně	silně	k6eAd1	silně
katolických	katolický	k2eAgFnPc2d1	katolická
metropolí	metropol	k1gFnPc2	metropol
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
měla	mít	k5eAaImAgFnS	mít
Uruguay	Uruguay	k1gFnSc1	Uruguay
největší	veliký	k2eAgFnSc4d3	veliký
židovskou	židovský	k2eAgFnSc4d1	židovská
diasporu	diaspora	k1gFnSc4	diaspora
(	(	kIx(	(
<g/>
na	na	k7c4	na
počet	počet	k1gInSc4	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
země	zem	k1gFnSc2	zem
<g/>
)	)	kIx)	)
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
však	však	k9	však
počty	počet	k1gInPc1	počet
setrvale	setrvale	k6eAd1	setrvale
klesají	klesat	k5eAaImIp3nP	klesat
<g/>
,	,	kIx,	,
mnoho	mnoho	k6eAd1	mnoho
z	z	k7c2	z
uruguayských	uruguayský	k2eAgMnPc2d1	uruguayský
židů	žid	k1gMnPc2	žid
odešlo	odejít	k5eAaPmAgNnS	odejít
do	do	k7c2	do
Izraele	Izrael	k1gInSc2	Izrael
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kultura	kultura	k1gFnSc1	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Umění	umění	k1gNnSc1	umění
===	===	k?	===
</s>
</p>
<p>
<s>
Spisovatel	spisovatel	k1gMnSc1	spisovatel
Juan	Juan	k1gMnSc1	Juan
Carlos	Carlos	k1gMnSc1	Carlos
Onetti	Onetti	k1gNnPc2	Onetti
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
<g/>
,	,	kIx,	,
jakožto	jakožto	k8xS	jakožto
první	první	k4xOgMnSc1	první
Uruguayec	Uruguayec	k1gMnSc1	Uruguayec
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
,	,	kIx,	,
neprestižnější	prestižní	k2eNgNnSc4d2	prestižní
literární	literární	k2eAgNnSc4d1	literární
ocenění	ocenění	k1gNnSc4	ocenění
španělskojazyčného	španělskojazyčný	k2eAgInSc2d1	španělskojazyčný
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
Cervantesovu	Cervantesův	k2eAgFnSc4d1	Cervantesova
cenu	cena	k1gFnSc4	cena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
ho	on	k3xPp3gMnSc4	on
napodobila	napodobit	k5eAaPmAgFnS	napodobit
básnířka	básnířka	k1gFnSc1	básnířka
Ida	Ida	k1gFnSc1	Ida
Vitaleová	Vitaleová	k1gFnSc1	Vitaleová
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
představitelkou	představitelka	k1gFnSc7	představitelka
tzv.	tzv.	kA	tzv.
Generace	generace	k1gFnSc1	generace
roku	rok	k1gInSc2	rok
45	[number]	k4	45
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
třeba	třeba	k6eAd1	třeba
básník	básník	k1gMnSc1	básník
Mario	Mario	k1gMnSc1	Mario
Benedetti	Benedetti	k1gNnPc2	Benedetti
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
fantastických	fantastický	k2eAgFnPc2d1	fantastická
povídek	povídka	k1gFnPc2	povídka
Horacio	Horacio	k6eAd1	Horacio
Quiroga	Quiroga	k1gFnSc1	Quiroga
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
budoucí	budoucí	k2eAgInSc1d1	budoucí
magický	magický	k2eAgInSc1d1	magický
realismus	realismus	k1gInSc1	realismus
<g/>
.	.	kIx.	.
</s>
<s>
Klasikem	klasik	k1gMnSc7	klasik
moderní	moderní	k2eAgFnSc2d1	moderní
uruguayské	uruguayský	k2eAgFnSc2d1	Uruguayská
poezie	poezie	k1gFnSc2	poezie
je	být	k5eAaImIp3nS	být
Delmira	Delmira	k1gFnSc1	Delmira
Agustiniová	Agustiniový	k2eAgFnSc1d1	Agustiniový
či	či	k8xC	či
Juana	Juan	k1gMnSc2	Juan
de	de	k?	de
Ibarbourou	Ibarboura	k1gFnSc7	Ibarboura
<g/>
.	.	kIx.	.
</s>
<s>
Levicovou	levicový	k2eAgFnSc7d1	levicová
esejistikou	esejistika	k1gFnSc7	esejistika
se	se	k3xPyFc4	se
chtěl	chtít	k5eAaImAgMnS	chtít
prosadit	prosadit	k5eAaPmF	prosadit
Eduardo	Eduardo	k1gNnSc4	Eduardo
Galeano	Galeana	k1gFnSc5	Galeana
(	(	kIx(	(
<g/>
zejm.	zejm.	k?	zejm.
Otevřené	otevřený	k2eAgFnSc2d1	otevřená
žíly	žíla	k1gFnSc2	žíla
Latinské	latinský	k2eAgFnSc2d1	Latinská
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
Jorge	Jorge	k1gFnPc2	Jorge
Drexler	Drexler	k1gMnSc1	Drexler
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc7	první
Uruguaycem	Uruguayec	k1gMnSc7	Uruguayec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
získal	získat	k5eAaPmAgMnS	získat
Oscara	Oscar	k1gMnSc4	Oscar
<g/>
,	,	kIx,	,
za	za	k7c4	za
píseň	píseň	k1gFnSc4	píseň
k	k	k7c3	k
filmu	film	k1gInSc2	film
Motocyklové	motocyklový	k2eAgInPc1d1	motocyklový
deníky	deník	k1gInPc1	deník
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
i	i	k8xC	i
herečka	herečka	k1gFnSc1	herečka
telenovel	telenovela	k1gFnPc2	telenovela
se	se	k3xPyFc4	se
proslavila	proslavit	k5eAaPmAgFnS	proslavit
Natalia	Natalia	k1gFnSc1	Natalia
Oreiro	Oreiro	k1gNnSc4	Oreiro
<g/>
.	.	kIx.	.
</s>
<s>
Podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
i	i	k9	i
herečce	herečka	k1gFnSc3	herečka
Bárbaře	Bárbara	k1gFnSc3	Bárbara
Mori	Mor	k1gFnSc3	Mor
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
v	v	k7c6	v
mexických	mexický	k2eAgFnPc6d1	mexická
produkcích	produkce	k1gFnPc6	produkce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
světového	světový	k2eAgNnSc2d1	světové
kulturního	kulturní	k2eAgNnSc2d1	kulturní
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
byly	být	k5eAaImAgInP	být
zapsány	zapsat	k5eAaPmNgInP	zapsat
historické	historický	k2eAgNnSc4d1	historické
centrum	centrum	k1gNnSc4	centrum
města	město	k1gNnSc2	město
Colonia	Colonium	k1gNnSc2	Colonium
del	del	k?	del
Sacramento	Sacramento	k1gNnSc1	Sacramento
a	a	k8xC	a
Industriální	industriální	k2eAgInSc1d1	industriální
komplex	komplex	k1gInSc1	komplex
zpracovny	zpracovna	k1gFnSc2	zpracovna
masa	maso	k1gNnSc2	maso
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Fray	Fraa	k1gFnSc2	Fraa
Bentos	Bentosa	k1gFnPc2	Bentosa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sport	sport	k1gInSc1	sport
===	===	k?	===
</s>
</p>
<p>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
sportem	sport	k1gInSc7	sport
v	v	k7c6	v
Uruguayi	Uruguay	k1gFnSc6	Uruguay
je	být	k5eAaImIp3nS	být
kopaná	kopaná	k1gFnSc1	kopaná
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
moderní	moderní	k2eAgInSc1d1	moderní
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
stadion	stadion	k1gInSc1	stadion
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
každém	každý	k3xTgNnSc6	každý
větším	veliký	k2eAgNnSc6d2	veliký
uruguayském	uruguayský	k2eAgNnSc6d1	Uruguayské
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
Montevideu	Montevideo	k1gNnSc6	Montevideo
působí	působit	k5eAaImIp3nS	působit
tři	tři	k4xCgInPc4	tři
světoznámé	světoznámý	k2eAgInPc4d1	světoznámý
fotbalové	fotbalový	k2eAgInPc4d1	fotbalový
kluby	klub	k1gInPc4	klub
Nacional	Nacional	k1gFnPc2	Nacional
<g/>
,	,	kIx,	,
Peňarol	Peňarola	k1gFnPc2	Peňarola
a	a	k8xC	a
Cerro	Cerro	k1gNnSc4	Cerro
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
tam	tam	k6eAd1	tam
byly	být	k5eAaImAgFnP	být
velmi	velmi	k6eAd1	velmi
úspěšné	úspěšný	k2eAgFnPc1d1	úspěšná
také	také	k9	také
CA	ca	kA	ca
Bella	Bella	k1gMnSc1	Bella
Vista	vista	k2eAgMnSc1d1	vista
a	a	k8xC	a
CA	ca	kA	ca
Lito	lit	k2eAgNnSc1d1	lito
Montevideo	Montevideo	k1gNnSc1	Montevideo
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
již	již	k6eAd1	již
třikrát	třikrát	k6eAd1	třikrát
vítězem	vítěz	k1gMnSc7	vítěz
Uruguayské	uruguayský	k2eAgFnSc2d1	Uruguayská
národní	národní	k2eAgFnSc2d1	národní
fotbalové	fotbalový	k2eAgFnSc2d1	fotbalová
ligy	liga	k1gFnSc2	liga
Danubio	Danubio	k1gMnSc1	Danubio
F.	F.	kA	F.
<g/>
C.	C.	kA	C.
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
na	na	k7c6	na
stadionu	stadion	k1gInSc6	stadion
pro	pro	k7c4	pro
20	[number]	k4	20
000	[number]	k4	000
diváků	divák	k1gMnPc2	divák
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
pro	pro	k7c4	pro
dostihový	dostihový	k2eAgInSc4d1	dostihový
sport	sport	k1gInSc4	sport
v	v	k7c6	v
Montevideu	Montevideo	k1gNnSc6	Montevideo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
založení	založení	k1gNnSc2	založení
bulharskými	bulharský	k2eAgMnPc7d1	bulharský
exulanty	exulant	k1gMnPc7	exulant
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
nejmladším	mladý	k2eAgInSc7d3	nejmladší
<g/>
"	"	kIx"	"
klubem	klub	k1gInSc7	klub
v	v	k7c6	v
uruguayské	uruguayský	k2eAgFnSc6d1	Uruguayská
první	první	k4xOgFnSc6	první
lize	liga	k1gFnSc6	liga
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
novějším	nový	k2eAgInPc3d2	novější
úspěšným	úspěšný	k2eAgInPc3d1	úspěšný
fotbalovým	fotbalový	k2eAgInPc3d1	fotbalový
klubům	klub	k1gInPc3	klub
patří	patřit	k5eAaImIp3nP	patřit
též	též	k9	též
Liverpool	Liverpool	k1gInSc1	Liverpool
Montevideo	Montevideo	k1gNnSc1	Montevideo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Reprezentační	reprezentační	k2eAgNnSc1d1	reprezentační
fotbalové	fotbalový	k2eAgNnSc1d1	fotbalové
národní	národní	k2eAgNnSc1d1	národní
mužstvo	mužstvo	k1gNnSc1	mužstvo
Uruguaye	Uruguay	k1gFnSc2	Uruguay
je	být	k5eAaImIp3nS	být
dvojnásobným	dvojnásobný	k2eAgMnSc7d1	dvojnásobný
mistrem	mistr	k1gMnSc7	mistr
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
a	a	k8xC	a
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dvojnásobným	dvojnásobný	k2eAgMnSc7d1	dvojnásobný
olympijským	olympijský	k2eAgMnSc7d1	olympijský
vítězem	vítěz	k1gMnSc7	vítěz
(	(	kIx(	(
<g/>
1924	[number]	k4	1924
<g/>
,	,	kIx,	,
1928	[number]	k4	1928
<g/>
)	)	kIx)	)
a	a	k8xC	a
třikrát	třikrát	k6eAd1	třikrát
získala	získat	k5eAaPmAgFnS	získat
Uruguay	Uruguay	k1gFnSc1	Uruguay
na	na	k7c6	na
světových	světový	k2eAgInPc6d1	světový
fotbalových	fotbalový	k2eAgInPc6d1	fotbalový
šampionátech	šampionát	k1gInPc6	šampionát
"	"	kIx"	"
<g/>
bramborovou	bramborový	k2eAgFnSc4d1	bramborová
medaili	medaile	k1gFnSc4	medaile
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
čtvrté	čtvrtá	k1gFnPc4	čtvrtá
místo	místo	k7c2	místo
–	–	k?	–
účast	účast	k1gFnSc1	účast
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1954	[number]	k4	1954
<g/>
,	,	kIx,	,
1970	[number]	k4	1970
a	a	k8xC	a
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
historických	historický	k2eAgMnPc2d1	historický
hráčů	hráč	k1gMnPc2	hráč
se	se	k3xPyFc4	se
proslavili	proslavit	k5eAaPmAgMnP	proslavit
Héctor	Héctor	k1gMnSc1	Héctor
Scarone	Scaron	k1gMnSc5	Scaron
<g/>
,	,	kIx,	,
Pedro	Pedra	k1gMnSc5	Pedra
Petrone	Petron	k1gMnSc5	Petron
<g/>
,	,	kIx,	,
Emilio	Emilio	k6eAd1	Emilio
Recoba	Recoba	k1gFnSc1	Recoba
<g/>
,	,	kIx,	,
Pedro	Pedro	k1gNnSc1	Pedro
Cea	Cea	k1gFnSc2	Cea
<g/>
,	,	kIx,	,
Paolo	Paolo	k1gNnSc1	Paolo
Dorado	Dorada	k1gFnSc5	Dorada
<g/>
,	,	kIx,	,
Santos	Santos	k1gMnSc1	Santos
Iriarte	Iriart	k1gInSc5	Iriart
<g/>
,	,	kIx,	,
Héctor	Héctor	k1gInSc4	Héctor
Castro	Castro	k1gNnSc4	Castro
<g/>
,	,	kIx,	,
José	José	k1gNnSc4	José
Andrade	Andrad	k1gInSc5	Andrad
<g/>
,	,	kIx,	,
Victor	Victor	k1gMnSc1	Victor
Rodríguez	Rodríguez	k1gMnSc1	Rodríguez
Andrade	Andrad	k1gInSc5	Andrad
<g/>
,	,	kIx,	,
Obdulio	Obdulio	k6eAd1	Obdulio
Varela	Varela	k1gFnSc1	Varela
<g/>
,	,	kIx,	,
Juan	Juan	k1gMnSc1	Juan
Carlos	Carlos	k1gMnSc1	Carlos
a	a	k8xC	a
Matias	Matias	k1gMnSc1	Matias
<g />
.	.	kIx.	.
</s>
<s>
Gonzálezové	González	k1gMnPc1	González
<g/>
,	,	kIx,	,
Juan	Juan	k1gMnSc1	Juan
Alberto	Alberta	k1gFnSc5	Alberta
Schiaffino	Schiaffino	k1gNnSc1	Schiaffino
<g/>
,	,	kIx,	,
Pablo	Pablo	k1gNnSc1	Pablo
Forlán	Forlán	k1gInSc1	Forlán
<g/>
,	,	kIx,	,
Pedro	Pedro	k1gNnSc1	Pedro
Rocha	Rocha	k1gMnSc1	Rocha
<g/>
,	,	kIx,	,
brankář	brankář	k1gMnSc1	brankář
Ladislao	Ladislao	k1gMnSc1	Ladislao
Mazurkiewicz	Mazurkiewicz	k1gMnSc1	Mazurkiewicz
(	(	kIx(	(
<g/>
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zahajovací	zahajovací	k2eAgNnSc1d1	zahajovací
utkání	utkání	k1gNnSc1	utkání
ve	v	k7c4	v
Wembley	Wemblea	k1gFnPc4	Wemblea
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgMnSc6	jenž
zazvonilo	zazvonit	k5eAaPmAgNnS	zazvonit
Rochovo	Rochův	k2eAgNnSc4d1	Rochův
břevno	břevno	k1gNnSc4	břevno
střelou	střela	k1gFnSc7	střela
ze	z	k7c2	z
40	[number]	k4	40
m	m	kA	m
–	–	k?	–
Anglie	Anglie	k1gFnSc2	Anglie
–	–	k?	–
Uruguay	Uruguay	k1gFnSc1	Uruguay
<g />
.	.	kIx.	.
</s>
<s>
0	[number]	k4	0
:	:	kIx,	:
0	[number]	k4	0
,	,	kIx,	,
vítězství	vítězství	k1gNnSc1	vítězství
nad	nad	k7c7	nad
Francií	Francie	k1gFnSc7	Francie
2	[number]	k4	2
:	:	kIx,	:
1	[number]	k4	1
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Mazurkiewicz	Mazurkiewicz	k1gMnSc1	Mazurkiewicz
obdržel	obdržet	k5eAaPmAgMnS	obdržet
gól	gól	k1gInSc4	gól
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
penalty	penalta	k1gFnSc2	penalta
<g/>
,	,	kIx,	,
Uruguay	Uruguay	k1gFnSc1	Uruguay
došla	dojít	k5eAaPmAgFnS	dojít
až	až	k9	až
do	do	k7c2	do
čtvrtfinále	čtvrtfinále	k1gNnSc2	čtvrtfinále
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
vyřazena	vyřadit	k5eAaPmNgFnS	vyřadit
pozdějším	pozdní	k2eAgInSc7d2	pozdější
finalistou	finalista	k1gMnSc7	finalista
Německou	německý	k2eAgFnSc7d1	německá
spolkovou	spolkový	k2eAgFnSc7d1	spolková
republikou	republika	k1gFnSc7	republika
<g/>
,	,	kIx,	,
v	v	k7c6	v
tvrdém	tvrdý	k2eAgNnSc6d1	tvrdé
utkání	utkání	k1gNnSc6	utkání
a	a	k8xC	a
Mazurkiewicz	Mazurkiewicz	k1gInSc1	Mazurkiewicz
poprvé	poprvé	k6eAd1	poprvé
kapituloval	kapitulovat	k5eAaBmAgInS	kapitulovat
gólem	gól	k1gInSc7	gól
ze	z	k7c2	z
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
až	až	k9	až
byli	být	k5eAaImAgMnP	být
Uruguaycům	Uruguayec	k1gMnPc3	Uruguayec
dosti	dosti	k6eAd1	dosti
přísně	přísně	k6eAd1	přísně
vyloučeni	vyloučit	k5eAaPmNgMnP	vyloučit
dva	dva	k4xCgMnPc1	dva
hráči	hráč	k1gMnPc1	hráč
)	)	kIx)	)
<g/>
,	,	kIx,	,
Enzo	Enzo	k6eAd1	Enzo
Francescoli	Francescole	k1gFnSc4	Francescole
a	a	k8xC	a
Álvaro	Álvara	k1gFnSc5	Álvara
Recoba	Recoba	k1gFnSc1	Recoba
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
současných	současný	k2eAgMnPc2d1	současný
hráčů	hráč	k1gMnPc2	hráč
na	na	k7c6	na
fotbalovém	fotbalový	k2eAgNnSc6d1	fotbalové
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
Jihoafrické	jihoafrický	k2eAgFnSc6d1	Jihoafrická
republice	republika	k1gFnSc6	republika
(	(	kIx(	(
<g/>
léto	léto	k1gNnSc1	léto
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
jmenujme	jmenovat	k5eAaImRp1nP	jmenovat
Diego	Diego	k6eAd1	Diego
Forlána	Forlán	k2eAgNnPc1d1	Forlán
<g/>
,	,	kIx,	,
Diego	Diego	k6eAd1	Diego
Lugána	Lugána	k1gFnSc1	Lugána
<g/>
,	,	kIx,	,
Luise	Luisa	k1gFnSc6	Luisa
Suaréze	Suaréza	k1gFnSc6	Suaréza
a	a	k8xC	a
Edinsona	Edinsona	k1gFnSc1	Edinsona
Cavaniho	Cavani	k1gMnSc2	Cavani
Gómeze	Gómeze	k1gFnSc2	Gómeze
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
"	"	kIx"	"
<g/>
zazářili	zazářit	k5eAaPmAgMnP	zazářit
<g/>
"	"	kIx"	"
i	i	k9	i
na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
(	(	kIx(	(
<g/>
Copa	Copa	k1gMnSc1	Copa
América	América	k1gMnSc1	América
<g/>
)	)	kIx)	)
v	v	k7c6	v
Argentině	Argentina	k1gFnSc6	Argentina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
vítězného	vítězný	k2eAgNnSc2d1	vítězné
finále	finále	k1gNnSc2	finále
vyřadili	vyřadit	k5eAaPmAgMnP	vyřadit
i	i	k9	i
domácí	domácí	k2eAgNnSc4d1	domácí
mužstvo	mužstvo	k1gNnSc4	mužstvo
<g/>
.	.	kIx.	.
</s>
<s>
Uruguay	Uruguay	k1gFnSc1	Uruguay
je	být	k5eAaImIp3nS	být
též	též	k9	též
několikanásobným	několikanásobný	k2eAgMnSc7d1	několikanásobný
vítězem	vítěz	k1gMnSc7	vítěz
Jihoamerického	jihoamerický	k2eAgInSc2d1	jihoamerický
Poháru	pohár	k1gInSc2	pohár
a	a	k8xC	a
současným	současný	k2eAgMnSc7d1	současný
mistrem	mistr	k1gMnSc7	mistr
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
Copa	Copus	k1gMnSc4	Copus
América	Américus	k1gMnSc4	Américus
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1916	[number]	k4	1916
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
uruguayská	uruguayský	k2eAgFnSc1d1	Uruguayská
fotbalová	fotbalový	k2eAgFnSc1d1	fotbalová
reprezentace	reprezentace	k1gFnSc1	reprezentace
již	již	k6eAd1	již
patnáctkrát	patnáctkrát	k6eAd1	patnáctkrát
(	(	kIx(	(
<g/>
Argentina	Argentina	k1gFnSc1	Argentina
14	[number]	k4	14
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
Brazílie	Brazílie	k1gFnSc1	Brazílie
8	[number]	k4	8
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
Paraguay	Paraguay	k1gFnSc1	Paraguay
a	a	k8xC	a
Peru	Peru	k1gNnSc1	Peru
2	[number]	k4	2
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
a	a	k8xC	a
Bolívie	Bolívie	k1gFnSc1	Bolívie
jednou	jednou	k6eAd1	jednou
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stříbrné	stříbrný	k2eAgFnPc4d1	stříbrná
medaile	medaile	k1gFnPc4	medaile
získaly	získat	k5eAaPmAgFnP	získat
ještě	ještě	k6eAd1	ještě
Chile	Chile	k1gNnSc4	Chile
a	a	k8xC	a
Mexiko	Mexiko	k1gNnSc4	Mexiko
<g/>
,	,	kIx,	,
bronzovou	bronzový	k2eAgFnSc7d1	bronzová
Honduras	Honduras	k1gInSc4	Honduras
<g/>
,	,	kIx,	,
čtvrtá	čtvrtá	k1gFnSc1	čtvrtá
místa	místo	k1gNnSc2	místo
získaly	získat	k5eAaPmAgFnP	získat
ještě	ještě	k6eAd1	ještě
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Venezuela	Venezuela	k1gFnSc1	Venezuela
a	a	k8xC	a
Ekvádor	Ekvádor	k1gInSc1	Ekvádor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
šestkrát	šestkrát	k6eAd1	šestkrát
získala	získat	k5eAaPmAgFnS	získat
stříbrné	stříbrný	k2eAgFnPc4d1	stříbrná
medaile	medaile	k1gFnPc4	medaile
a	a	k8xC	a
devětkrát	devětkrát	k6eAd1	devětkrát
bronzové	bronzový	k2eAgNnSc1d1	bronzové
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Uruguayi	Uruguay	k1gFnSc6	Uruguay
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
zatím	zatím	k6eAd1	zatím
jediné	jediný	k2eAgNnSc1d1	jediné
zato	zato	k6eAd1	zato
<g/>
,	,	kIx,	,
však	však	k9	však
první	první	k4xOgNnSc4	první
fotbalové	fotbalový	k2eAgNnSc4d1	fotbalové
mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
–	–	k?	–
od	od	k7c2	od
12	[number]	k4	12
<g/>
.	.	kIx.	.
do	do	k7c2	do
30	[number]	k4	30
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
se	se	k3xPyFc4	se
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
třináct	třináct	k4xCc1	třináct
zemí	zem	k1gFnPc2	zem
ve	v	k7c6	v
čtyřech	čtyři	k4xCgFnPc6	čtyři
skupinách	skupina	k1gFnPc6	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Uruguay	Uruguay	k1gFnSc1	Uruguay
postoupila	postoupit	k5eAaPmAgFnS	postoupit
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
skupiny	skupina	k1gFnSc2	skupina
do	do	k7c2	do
semifinále	semifinále	k1gNnSc2	semifinále
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
USA	USA	kA	USA
bez	bez	k7c2	bez
jediné	jediný	k2eAgFnSc2d1	jediná
obdržené	obdržený	k2eAgFnSc2d1	obdržená
branky	branka	k1gFnSc2	branka
<g/>
,	,	kIx,	,
v	v	k7c6	v
nejtěžším	těžký	k2eAgNnSc6d3	nejtěžší
utkání	utkání	k1gNnSc6	utkání
na	na	k7c4	na
Centenario	Centenario	k1gNnSc4	Centenario
Estadio	Estadio	k6eAd1	Estadio
v	v	k7c6	v
Montevideu	Montevideo	k1gNnSc6	Montevideo
před	před	k7c7	před
85	[number]	k4	85
000	[number]	k4	000
diváků	divák	k1gMnPc2	divák
<g/>
,	,	kIx,	,
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
nad	nad	k7c7	nad
Peru	Peru	k1gNnSc7	Peru
<g/>
,	,	kIx,	,
předtím	předtím	k6eAd1	předtím
poraženým	poražený	k2eAgNnSc7d1	poražené
Rumunskem	Rumunsko	k1gNnSc7	Rumunsko
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
1	[number]	k4	1
:	:	kIx,	:
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Chuť	chuť	k1gFnSc1	chuť
si	se	k3xPyFc3	se
Uruguayci	Uruguayec	k1gMnPc1	Uruguayec
napravili	napravit	k5eAaPmAgMnP	napravit
výhrou	výhra	k1gFnSc7	výhra
nad	nad	k7c7	nad
Rumunskem	Rumunsko	k1gNnSc7	Rumunsko
4	[number]	k4	4
:	:	kIx,	:
0	[number]	k4	0
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
před	před	k7c7	před
stejnou	stejný	k2eAgFnSc7d1	stejná
návštěvou	návštěva	k1gFnSc7	návštěva
<g/>
.	.	kIx.	.
</s>
<s>
Třetím	třetí	k4xOgMnSc7	třetí
postupujícím	postupující	k2eAgMnSc7d1	postupující
byla	být	k5eAaImAgFnS	být
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
skupině	skupina	k1gFnSc6	skupina
obdržela	obdržet	k5eAaPmAgFnS	obdržet
jedinou	jediný	k2eAgFnSc4d1	jediná
branku	branka	k1gFnSc4	branka
od	od	k7c2	od
nepostupující	postupující	k2eNgFnSc2d1	nepostupující
Brazílie	Brazílie	k1gFnSc2	Brazílie
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
deklasovala	deklasovat	k5eAaBmAgFnS	deklasovat
Bolívii	Bolívie	k1gFnSc4	Bolívie
4	[number]	k4	4
:	:	kIx,	:
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtý	čtvrtý	k4xOgMnSc1	čtvrtý
semifinalista	semifinalista	k1gMnSc1	semifinalista
<g/>
,	,	kIx,	,
ambiciozní	ambiciozní	k2eAgFnSc1d1	ambiciozní
Argentina	Argentina	k1gFnSc1	Argentina
porazila	porazit	k5eAaPmAgFnS	porazit
Francii	Francie	k1gFnSc4	Francie
1	[number]	k4	1
:	:	kIx,	:
0	[number]	k4	0
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
od	od	k7c2	od
Mexika	Mexiko	k1gNnSc2	Mexiko
obdržela	obdržet	k5eAaPmAgFnS	obdržet
tři	tři	k4xCgFnPc4	tři
(	(	kIx(	(
<g/>
6	[number]	k4	6
:	:	kIx,	:
3	[number]	k4	3
<g/>
)	)	kIx)	)
a	a	k8xC	a
od	od	k7c2	od
Chile	Chile	k1gNnSc2	Chile
jednu	jeden	k4xCgFnSc4	jeden
(	(	kIx(	(
<g/>
3	[number]	k4	3
:	:	kIx,	:
1	[number]	k4	1
<g/>
)	)	kIx)	)
branku	branka	k1gFnSc4	branka
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgNnPc1	dva
semifinále	semifinále	k1gNnPc1	semifinále
hraná	hraný	k2eAgFnSc1d1	hraná
26	[number]	k4	26
<g/>
.	.	kIx.	.
a	a	k8xC	a
27	[number]	k4	27
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1930	[number]	k4	1930
skončila	skončit	k5eAaPmAgFnS	skončit
shodně	shodně	k6eAd1	shodně
deklasováním	deklasování	k1gNnSc7	deklasování
soupeřů	soupeř	k1gMnPc2	soupeř
6	[number]	k4	6
:	:	kIx,	:
1	[number]	k4	1
(	(	kIx(	(
<g/>
Argentina	Argentina	k1gFnSc1	Argentina
:	:	kIx,	:
USA	USA	kA	USA
,	,	kIx,	,
Uruguay	Uruguay	k1gFnSc1	Uruguay
:	:	kIx,	:
Jugoslávie	Jugoslávie	k1gFnSc1	Jugoslávie
<g/>
,	,	kIx,	,
Uruguayec	Uruguayec	k1gMnSc1	Uruguayec
Pedro	Pedro	k1gNnSc1	Pedro
Cea	Cea	k1gMnSc1	Cea
v	v	k7c6	v
semifinálovém	semifinálový	k2eAgInSc6d1	semifinálový
zápase	zápas	k1gInSc6	zápas
vstřelil	vstřelit	k5eAaPmAgMnS	vstřelit
3	[number]	k4	3
góly	gól	k1gInPc4	gól
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Utkání	utkání	k1gNnSc4	utkání
o	o	k7c4	o
třetí	třetí	k4xOgNnSc4	třetí
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
ještě	ještě	k6eAd1	ještě
nehrálo	hrát	k5eNaImAgNnS	hrát
a	a	k8xC	a
oběma	dva	k4xCgMnPc3	dva
poraženým	poražený	k2eAgMnPc3d1	poražený
semifinalistům	semifinalista	k1gMnPc3	semifinalista
(	(	kIx(	(
<g/>
USA	USA	kA	USA
i	i	k8xC	i
Jugoslávii	Jugoslávie	k1gFnSc6	Jugoslávie
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
přiděleny	přidělit	k5eAaPmNgFnP	přidělit
svorně	svorně	k6eAd1	svorně
bronzové	bronzový	k2eAgFnPc1d1	bronzová
medaile	medaile	k1gFnPc1	medaile
(	(	kIx(	(
<g/>
o	o	k7c4	o
bronzové	bronzový	k2eAgFnPc4d1	bronzová
medaile	medaile	k1gFnPc4	medaile
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
bojovalo	bojovat	k5eAaImAgNnS	bojovat
až	až	k9	až
na	na	k7c4	na
5	[number]	k4	5
<g/>
.	.	kIx.	.
světovém	světový	k2eAgInSc6d1	světový
šampionátu	šampionát	k1gInSc6	šampionát
v	v	k7c6	v
kopané	kopaná	k1gFnSc6	kopaná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
Uruguay	Uruguay	k1gFnSc1	Uruguay
:	:	kIx,	:
Rakousko	Rakousko	k1gNnSc1	Rakousko
1	[number]	k4	1
:	:	kIx,	:
3	[number]	k4	3
<g/>
,	,	kIx,	,
poločas	poločas	k1gInSc1	poločas
1	[number]	k4	1
:	:	kIx,	:
1	[number]	k4	1
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
Uruguay	Uruguay	k1gFnSc4	Uruguay
<g/>
,	,	kIx,	,
tehdejšího	tehdejší	k2eAgMnSc4d1	tehdejší
mistra	mistr	k1gMnSc4	mistr
světa	svět	k1gInSc2	svět
ze	z	k7c2	z
čtvrtého	čtvrtý	k4xOgInSc2	čtvrtý
"	"	kIx"	"
<g/>
brazilského	brazilský	k2eAgInSc2d1	brazilský
<g/>
"	"	kIx"	"
světového	světový	k2eAgInSc2d1	světový
šampionátu	šampionát	k1gInSc2	šampionát
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
nešťastný	šťastný	k2eNgInSc1d1	nešťastný
zápas	zápas	k1gInSc1	zápas
–	–	k?	–
prohra	prohra	k1gFnSc1	prohra
penaltou	penalta	k1gFnSc7	penalta
a	a	k8xC	a
vlastním	vlastní	k2eAgInSc7d1	vlastní
gólem	gól	k1gInSc7	gól
–	–	k?	–
jako	jako	k8xS	jako
semifinále	semifinále	k1gNnSc6	semifinále
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
po	po	k7c6	po
výsledku	výsledek	k1gInSc6	výsledek
2	[number]	k4	2
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
2	[number]	k4	2
podlehli	podlehnout	k5eAaPmAgMnP	podlehnout
tehdy	tehdy	k6eAd1	tehdy
hvězdnému	hvězdný	k2eAgNnSc3d1	Hvězdné
Maďarsku	Maďarsko	k1gNnSc3	Maďarsko
až	až	k6eAd1	až
po	po	k7c6	po
prodloužení	prodloužení	k1gNnSc6	prodloužení
–	–	k?	–
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
C	C	kA	C
Uruguay	Uruguay	k1gFnSc1	Uruguay
:	:	kIx,	:
Československo	Československo	k1gNnSc1	Československo
2	[number]	k4	2
:	:	kIx,	:
0	[number]	k4	0
<g/>
,	,	kIx,	,
Uruguay	Uruguay	k1gFnSc1	Uruguay
:	:	kIx,	:
Skotsko	Skotsko	k1gNnSc1	Skotsko
7	[number]	k4	7
:	:	kIx,	:
0	[number]	k4	0
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
:	:	kIx,	:
Československo	Československo	k1gNnSc1	Československo
5	[number]	k4	5
:	:	kIx,	:
0	[number]	k4	0
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
:	:	kIx,	:
Skotsko	Skotsko	k1gNnSc1	Skotsko
1	[number]	k4	1
:	:	kIx,	:
0	[number]	k4	0
–	–	k?	–
vítěz	vítěz	k1gMnSc1	vítěz
Uruguay	Uruguay	k1gFnSc4	Uruguay
<g/>
,	,	kIx,	,
zápas	zápas	k1gInSc4	zápas
s	s	k7c7	s
<g />
.	.	kIx.	.
</s>
<s>
Rakouskem	Rakousko	k1gNnSc7	Rakousko
se	se	k3xPyFc4	se
již	již	k6eAd1	již
nehrál	hrát	k5eNaImAgMnS	hrát
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
oba	dva	k4xCgInPc1	dva
týmy	tým	k1gInPc1	tým
již	již	k6eAd1	již
postupovaly	postupovat	k5eAaImAgInP	postupovat
<g/>
,	,	kIx,	,
prvenství	prvenství	k1gNnSc4	prvenství
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
C	C	kA	C
o	o	k7c6	o
skóre	skóre	k1gNnSc6	skóre
9	[number]	k4	9
:	:	kIx,	:
0	[number]	k4	0
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
Uruguaye	Uruguay	k1gFnSc2	Uruguay
bylo	být	k5eAaImAgNnS	být
tehdy	tehdy	k6eAd1	tehdy
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
potvrzeno	potvrdit	k5eAaPmNgNnS	potvrdit
ještě	ještě	k6eAd1	ještě
losem	los	k1gInSc7	los
<g/>
,	,	kIx,	,
čtvrtfinále	čtvrtfinále	k1gNnSc4	čtvrtfinále
Uruguay	Uruguay	k1gFnSc1	Uruguay
:	:	kIx,	:
Anglie	Anglie	k1gFnSc1	Anglie
4	[number]	k4	4
:	:	kIx,	:
2	[number]	k4	2
<g/>
,	,	kIx,	,
poločas	poločas	k1gInSc1	poločas
2	[number]	k4	2
:	:	kIx,	:
1	[number]	k4	1
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Finále	finále	k1gNnSc1	finále
1	[number]	k4	1
<g/>
.	.	kIx.	.
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
kopané	kopaná	k1gFnSc6	kopaná
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
na	na	k7c4	na
Centenario	Centenario	k1gNnSc4	Centenario
Estadio	Estadio	k6eAd1	Estadio
v	v	k7c6	v
Montevideu	Montevideo	k1gNnSc6	Montevideo
30	[number]	k4	30
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1930	[number]	k4	1930
téměř	téměř	k6eAd1	téměř
před	před	k7c7	před
100	[number]	k4	100
000	[number]	k4	000
diváky	divák	k1gMnPc7	divák
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Argentina	Argentina	k1gFnSc1	Argentina
po	po	k7c6	po
úvodním	úvodní	k2eAgInSc6d1	úvodní
uruguayském	uruguayský	k2eAgInSc6d1	uruguayský
gólu	gól	k1gInSc6	gól
(	(	kIx(	(
<g/>
Dorado	Dorada	k1gFnSc5	Dorada
12	[number]	k4	12
<g/>
́	́	k?	́
<g/>
)	)	kIx)	)
otočila	otočit	k5eAaPmAgFnS	otočit
výsledek	výsledek	k1gInSc4	výsledek
poločasu	poločas	k1gInSc2	poločas
dvěma	dva	k4xCgFnPc7	dva
brankami	branka	k1gFnPc7	branka
ve	v	k7c4	v
svůj	svůj	k3xOyFgInSc4	svůj
prospěch	prospěch	k1gInSc4	prospěch
(	(	kIx(	(
<g/>
Peucelle	Peucelle	k1gInSc1	Peucelle
20	[number]	k4	20
<g/>
́	́	k?	́
<g/>
a	a	k8xC	a
Stábile	Stábila	k1gFnSc6	Stábila
37	[number]	k4	37
<g/>
́	́	k?	́
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
poločase	poločas	k1gInSc6	poločas
však	však	k9	však
úspěšně	úspěšně	k6eAd1	úspěšně
stříleli	střílet	k5eAaImAgMnP	střílet
jen	jen	k9	jen
Uruguayci	Uruguayec	k1gMnPc1	Uruguayec
(	(	kIx(	(
<g/>
Cea	Cea	k1gFnSc1	Cea
57	[number]	k4	57
<g/>
́	́	k?	́
<g/>
,	,	kIx,	,
Iriarte	Iriart	k1gInSc5	Iriart
68	[number]	k4	68
<g/>
́	́	k?	́
<g/>
,	,	kIx,	,
Castro	Castro	k1gNnSc1	Castro
89	[number]	k4	89
<g/>
́	́	k?	́
<g/>
)	)	kIx)	)
a	a	k8xC	a
stali	stát	k5eAaPmAgMnP	stát
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
finálovým	finálový	k2eAgNnSc7d1	finálové
vítězstvím	vítězství	k1gNnSc7	vítězství
4	[number]	k4	4
:	:	kIx,	:
2	[number]	k4	2
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
tehdy	tehdy	k6eAd1	tehdy
"	"	kIx"	"
<g/>
úřadující	úřadující	k2eAgMnPc1d1	úřadující
<g/>
"	"	kIx"	"
dvojnásobní	dvojnásobný	k2eAgMnPc1d1	dvojnásobný
olympijští	olympijský	k2eAgMnPc1d1	olympijský
vítězové	vítěz	k1gMnPc1	vítěz
<g/>
,	,	kIx,	,
i	i	k9	i
prvními	první	k4xOgMnPc7	první
mistry	mistr	k1gMnPc7	mistr
světa	svět	k1gInSc2	svět
v	v	k7c6	v
kopané	kopaná	k1gFnSc6	kopaná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velké	velký	k2eAgFnSc2d1	velká
ambice	ambice	k1gFnSc2	ambice
měla	mít	k5eAaImAgFnS	mít
Uruguay	Uruguay	k1gFnSc1	Uruguay
i	i	k9	i
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
2014	[number]	k4	2014
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
po	po	k7c6	po
postupu	postup	k1gInSc6	postup
ze	z	k7c2	z
základní	základní	k2eAgFnSc2d1	základní
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vyřadila	vyřadit	k5eAaPmAgNnP	vyřadit
národní	národní	k2eAgNnPc1d1	národní
mužstva	mužstvo	k1gNnPc1	mužstvo
Anglie	Anglie	k1gFnSc2	Anglie
(	(	kIx(	(
<g/>
2	[number]	k4	2
:	:	kIx,	:
1	[number]	k4	1
<g/>
)	)	kIx)	)
a	a	k8xC	a
Itálie	Itálie	k1gFnSc1	Itálie
(	(	kIx(	(
<g/>
1	[number]	k4	1
:	:	kIx,	:
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
v	v	k7c6	v
utkání	utkání	k1gNnSc6	utkání
s	s	k7c7	s
Itálií	Itálie	k1gFnSc7	Itálie
však	však	k9	však
nejlepší	dobrý	k2eAgMnPc1d3	nejlepší
hráč	hráč	k1gMnSc1	hráč
Uruguaye	Uruguay	k1gFnSc2	Uruguay
Luis	Luisa	k1gFnPc2	Luisa
Suárez	Suárez	k1gMnSc1	Suárez
kousl	kousnout	k5eAaPmAgMnS	kousnout
v	v	k7c6	v
zápalu	zápal	k1gInSc6	zápal
boje	boj	k1gInSc2	boj
Italského	italský	k2eAgMnSc4d1	italský
obránce	obránce	k1gMnSc4	obránce
a	a	k8xC	a
ač	ač	k8xS	ač
nebyl	být	k5eNaImAgMnS	být
vyloučen	vyloučit	k5eAaPmNgMnS	vyloučit
během	během	k7c2	během
utkání	utkání	k1gNnSc2	utkání
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
zápase	zápas	k1gInSc6	zápas
FIFA	FIFA	kA	FIFA
výborem	výbor	k1gInSc7	výbor
exemplárně	exemplárně	k6eAd1	exemplárně
potrestán	potrestat	k5eAaPmNgMnS	potrestat
(	(	kIx(	(
<g/>
vyloučením	vyloučení	k1gNnSc7	vyloučení
z	z	k7c2	z
turnaje	turnaj	k1gInSc2	turnaj
<g/>
,	,	kIx,	,
vysokou	vysoký	k2eAgFnSc7d1	vysoká
pokutou	pokuta	k1gFnSc7	pokuta
<g/>
,	,	kIx,	,
zákazem	zákaz	k1gInSc7	zákaz
dalších	další	k2eAgInPc2d1	další
startů	start	k1gInPc2	start
za	za	k7c4	za
Uruguay	Uruguay	k1gFnSc4	Uruguay
<g/>
,	,	kIx,	,
FC	FC	kA	FC
Liverpool	Liverpool	k1gInSc1	Liverpool
i	i	k8xC	i
FC	FC	kA	FC
Barcelona	Barcelona	k1gFnSc1	Barcelona
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
přestupoval-	přestupoval-	k?	přestupoval-
pro	pro	k7c4	pro
hlasovali	hlasovat	k5eAaImAgMnP	hlasovat
i	i	k9	i
zástupci	zástupce	k1gMnPc1	zástupce
Brazílie	Brazílie	k1gFnSc2	Brazílie
<g/>
,	,	kIx,	,
kterým	který	k3yIgNnSc7	který
zřejmě	zřejmě	k6eAd1	zřejmě
Uruguay	Uruguay	k1gFnSc1	Uruguay
nevyhovovala	vyhovovat	k5eNaImAgFnS	vyhovovat
jako	jako	k9	jako
možný	možný	k2eAgMnSc1d1	možný
čtvrtfinálový	čtvrtfinálový	k2eAgMnSc1d1	čtvrtfinálový
soupeř	soupeř	k1gMnSc1	soupeř
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
byla	být	k5eAaImAgFnS	být
Uruguay	Uruguay	k1gFnSc1	Uruguay
bez	bez	k7c2	bez
Suareze	Suareze	k1gFnSc2	Suareze
v	v	k7c6	v
dalším	další	k2eAgNnSc6d1	další
kole	kolo	k1gNnSc6	kolo
<g/>
,	,	kIx,	,
po	po	k7c6	po
chybách	chyba	k1gFnPc6	chyba
brankáře	brankář	k1gMnSc2	brankář
Muslery	Musler	k1gInPc4	Musler
<g/>
,	,	kIx,	,
vyřazena	vyřadit	k5eAaPmNgFnS	vyřadit
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
Uruguay	Uruguay	k1gFnSc1	Uruguay
neuspěla	uspět	k5eNaPmAgFnS	uspět
při	při	k7c6	při
obhajobě	obhajoba	k1gFnSc6	obhajoba
titulu	titul	k1gInSc2	titul
na	na	k7c4	na
Copa	Cop	k2eAgNnPc4d1	Cop
America	Americum	k1gNnPc4	Americum
v	v	k7c6	v
Chile	Chile	k1gNnSc6	Chile
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
domácími	domácí	k1gFnPc7	domácí
(	(	kIx(	(
<g/>
pozdějším	pozdní	k2eAgMnSc7d2	pozdější
vítězem	vítěz	k1gMnSc7	vítěz
<g/>
)	)	kIx)	)
po	po	k7c6	po
eliminaci	eliminace	k1gFnSc6	eliminace
Suareze	Suareze	k1gFnSc2	Suareze
a	a	k8xC	a
nařízené	nařízený	k2eAgFnSc6d1	nařízená
penaltě	penalta	k1gFnSc6	penalta
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
vyřazena	vyřazen	k2eAgFnSc1d1	vyřazena
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
1	[number]	k4	1
:	:	kIx,	:
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
se	se	k3xPyFc4	se
Uruguay	Uruguay	k1gFnSc1	Uruguay
zúčastňuje	zúčastňovat	k5eAaImIp3nS	zúčastňovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
<g/>
.	.	kIx.	.
</s>
<s>
Jediné	jediný	k2eAgFnPc4d1	jediná
dvě	dva	k4xCgFnPc4	dva
zlaté	zlatá	k1gFnPc4	zlatá
vybojovali	vybojovat	k5eAaPmAgMnP	vybojovat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
uvedeno	uvést	k5eAaPmNgNnS	uvést
výše	vysoce	k6eAd2	vysoce
<g/>
,	,	kIx,	,
fotbalisté	fotbalista	k1gMnPc1	fotbalista
<g/>
.	.	kIx.	.
</s>
<s>
Krom	krom	k7c2	krom
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
získat	získat	k5eAaPmF	získat
dvě	dva	k4xCgFnPc4	dva
medaile	medaile	k1gFnPc4	medaile
stříbrné	stříbrný	k2eAgFnSc2d1	stříbrná
<g/>
,	,	kIx,	,
na	na	k7c6	na
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
kanoistovi	kanoista	k1gMnSc3	kanoista
Eduardo	Eduardo	k1gNnSc4	Eduardo
Rissovi	Riss	k1gMnSc3	Riss
a	a	k8xC	a
na	na	k7c6	na
olympiádě	olympiáda	k1gFnSc6	olympiáda
v	v	k7c6	v
Sydney	Sydney	k1gNnSc6	Sydney
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
dráhovému	dráhový	k2eAgMnSc3d1	dráhový
cyklistovi	cyklista	k1gMnSc3	cyklista
Miltonu	Milton	k1gInSc2	Milton
Wynantsovi	Wynants	k1gMnSc3	Wynants
<g/>
,	,	kIx,	,
v	v	k7c6	v
bodovacím	bodovací	k2eAgInSc6d1	bodovací
závodě	závod	k1gInSc6	závod
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc4	dva
bronzy	bronz	k1gInPc4	bronz
získal	získat	k5eAaPmAgInS	získat
uruguayský	uruguayský	k2eAgInSc1d1	uruguayský
basketbalový	basketbalový	k2eAgInSc1d1	basketbalový
tým	tým	k1gInSc1	tým
mužů	muž	k1gMnPc2	muž
(	(	kIx(	(
<g/>
1952	[number]	k4	1952
<g/>
,	,	kIx,	,
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zbývající	zbývající	k2eAgFnPc4d1	zbývající
čtyři	čtyři	k4xCgFnPc4	čtyři
bronzové	bronzový	k2eAgFnPc4d1	bronzová
medaile	medaile	k1gFnPc4	medaile
zajistili	zajistit	k5eAaPmAgMnP	zajistit
kanoisté	kanoista	k1gMnPc1	kanoista
a	a	k8xC	a
boxer	boxer	k1gInSc1	boxer
Washington	Washington	k1gInSc1	Washington
Rodríguez	Rodríguez	k1gInSc1	Rodríguez
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Hispanoamerika	Hispanoamerika	k1gFnSc1	Hispanoamerika
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
CHALUPA	Chalupa	k1gMnSc1	Chalupa
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Argentiny	Argentina	k1gFnSc2	Argentina
<g/>
,	,	kIx,	,
Uruguaye	Uruguay	k1gFnSc2	Uruguay
a	a	k8xC	a
Chile	Chile	k1gNnSc2	Chile
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7106-323-1	[number]	k4	80-7106-323-1
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Uruguay	Uruguay	k1gFnSc1	Uruguay
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Uruguay	Uruguay	k1gFnSc1	Uruguay
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Uruguay	Uruguay	k1gFnSc1	Uruguay
-	-	kIx~	-
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc1	International
Report	report	k1gInSc1	report
2011	[number]	k4	2011
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc2	International
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Uruguay	Uruguay	k1gFnSc1	Uruguay
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Freedom	Freedom	k1gInSc1	Freedom
House	house	k1gNnSc1	house
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Uruguai	Uruguai	k1gNnSc1	Uruguai
-	-	kIx~	-
o	o	k7c6	o
českých	český	k2eAgMnPc6d1	český
krajanech	krajan	k1gMnPc6	krajan
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
2002-05-16	[number]	k4	2002-05-16
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
14	[number]	k4	14
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
.	.	kIx.	.
</s>
<s>
BTI	BTI	kA	BTI
2010	[number]	k4	2010
–	–	k?	–
Uruguay	Uruguay	k1gFnSc4	Uruguay
Country	country	k2eAgInSc4d1	country
Report	report	k1gInSc4	report
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Gütersloh	Gütersloh	k1gMnSc1	Gütersloh
<g/>
:	:	kIx,	:
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bureau	Bureau	k6eAd1	Bureau
of	of	k?	of
Western	western	k1gInSc1	western
Hemisphere	Hemispher	k1gInSc5	Hemispher
Affairs	Affairsa	k1gFnPc2	Affairsa
<g/>
.	.	kIx.	.
</s>
<s>
Background	Background	k1gInSc1	Background
Note	Note	k1gInSc1	Note
<g/>
:	:	kIx,	:
Uruguay	Uruguay	k1gFnSc1	Uruguay
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Department	department	k1gInSc1	department
of	of	k?	of
State	status	k1gInSc5	status
<g/>
,	,	kIx,	,
2011-06-23	[number]	k4	2011-06-23
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
World	World	k1gInSc1	World
Factbook	Factbook	k1gInSc1	Factbook
-	-	kIx~	-
Uruguay	Uruguay	k1gFnSc1	Uruguay
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Rev	Rev	k?	Rev
<g/>
.	.	kIx.	.
2011-07-05	[number]	k4	2011-07-05
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zastupitelský	zastupitelský	k2eAgInSc1d1	zastupitelský
úřad	úřad	k1gInSc1	úřad
ČR	ČR	kA	ČR
v	v	k7c6	v
Montevideu	Montevideo	k1gNnSc6	Montevideo
<g/>
.	.	kIx.	.
</s>
<s>
Souhrnná	souhrnný	k2eAgFnSc1d1	souhrnná
teritoriální	teritoriální	k2eAgFnSc1d1	teritoriální
informace	informace	k1gFnSc1	informace
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Businessinfo	Businessinfo	k1gNnSc1	Businessinfo
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
2011-05-01	[number]	k4	2011-05-01
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2012	[number]	k4	2012
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ALISKY	ALISKY	kA	ALISKY
<g/>
,	,	kIx,	,
Marvin	Marvina	k1gFnPc2	Marvina
H.	H.	kA	H.
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Uruguay	Uruguay	k1gFnSc1	Uruguay
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Encyclopaedia	Encyclopaedium	k1gNnPc1	Encyclopaedium
Britannica	Britannic	k2eAgNnPc1d1	Britannic
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
