<s>
SMILES	SMILES	kA
</s>
<s>
Tvorba	tvorba	k1gFnSc1
zápisu	zápis	k1gInSc2
SMILES	SMILES	kA
<g/>
:	:	kIx,
Rozpojte	rozpojit	k5eAaPmRp2nP
cykly	cyklus	k1gInPc4
<g/>
,	,	kIx,
potom	potom	k6eAd1
zapište	zapsat	k5eAaPmRp2nP
jako	jako	k9
větve	větev	k1gFnPc4
vně	vně	k7c2
hlavního	hlavní	k2eAgInSc2d1
řetězce	řetězec	k1gInSc2
</s>
<s>
SMILES	SMILES	kA
je	být	k5eAaImIp3nS
zkratka	zkratka	k1gFnSc1
pro	pro	k7c4
anglický	anglický	k2eAgInSc4d1
termín	termín	k1gInSc4
„	„	k?
<g/>
simplified	simplified	k1gInSc1
molecular	molecular	k1gMnSc1
input	input	k1gMnSc1
line	linout	k5eAaImIp3nS
entry	entra	k1gMnSc2
specification	specification	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
čili	čili	k8xC
česky	česky	k6eAd1
„	„	k?
<g/>
zjednodušená	zjednodušený	k2eAgFnSc1d1
molekulární	molekulární	k2eAgFnSc1d1
specifikace	specifikace	k1gFnSc1
pro	pro	k7c4
vstupní	vstupní	k2eAgInPc4d1
řádky	řádek	k1gInPc4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgNnPc1
specifikace	specifikace	k1gFnSc2
jednoznačným	jednoznačný	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
popisuje	popisovat	k5eAaImIp3nS
strukturu	struktura	k1gFnSc4
molekul	molekula	k1gFnPc2
pomocí	pomocí	k7c2
řetězců	řetězec	k1gInPc2
znaků	znak	k1gInPc2
ASCII	ascii	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řetězce	řetězec	k1gInSc2
SMILES	SMILES	kA
lze	lze	k6eAd1
importovat	importovat	k5eAaBmF
do	do	k7c2
většiny	většina	k1gFnSc2
editorů	editor	k1gMnPc2
molekul	molekula	k1gFnPc2
a	a	k8xC
konvertovat	konvertovat	k5eAaBmF
je	být	k5eAaImIp3nS
zpět	zpět	k6eAd1
do	do	k7c2
dvojrozměrných	dvojrozměrný	k2eAgFnPc2d1
kreseb	kresba	k1gFnPc2
nebo	nebo	k8xC
trojrozměrných	trojrozměrný	k2eAgInPc2d1
modelů	model	k1gInPc2
molekul	molekula	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Původní	původní	k2eAgFnSc4d1
specifikaci	specifikace	k1gFnSc4
SMILES	SMILES	kA
vyvinuli	vyvinout	k5eAaPmAgMnP
Arthur	Arthur	k1gMnSc1
Weininger	Weininger	k1gMnSc1
a	a	k8xC
David	David	k1gMnSc1
Weininger	Weininger	k1gMnSc1
koncem	koncem	k7c2
80	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
byla	být	k5eAaImAgFnS
modifikována	modifikovat	k5eAaBmNgFnS
a	a	k8xC
rozšířena	rozšířit	k5eAaPmNgFnS
<g/>
,	,	kIx,
především	především	k9
v	v	k7c4
Daylight	Daylight	k1gInSc4
Chemical	Chemical	k1gMnPc2
Information	Information	k1gInSc1
Systems	Systemsa	k1gFnPc2
Inc	Inc	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
vznikl	vzniknout	k5eAaPmAgInS
otevřený	otevřený	k2eAgInSc1d1
standard	standard	k1gInSc1
nazvaný	nazvaný	k2eAgInSc1d1
„	„	k?
<g/>
OpenSMILES	OpenSMILES	k1gFnSc1
<g/>
“	“	k?
<g/>
;	;	kIx,
za	za	k7c7
vznikem	vznik	k1gInSc7
stojí	stát	k5eAaImIp3nS
chemická	chemický	k2eAgFnSc1d1
open-source	open-sourka	k1gFnSc3
komunita	komunita	k1gFnSc1
Blue	Blue	k1gFnSc1
Obelisk	obelisk	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
další	další	k2eAgFnSc4d1
„	„	k?
<g/>
lineární	lineární	k2eAgFnSc2d1
<g/>
“	“	k?
notace	notace	k1gFnSc2
patří	patřit	k5eAaImIp3nS
například	například	k6eAd1
Wiswesser	Wiswesser	k1gInSc4
Line	linout	k5eAaImIp3nS
Notation	Notation	k1gInSc1
(	(	kIx(
<g/>
WLN	WLN	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ROSDAL	ROSDAL	kA
nebo	nebo	k8xC
SLN	SLN	kA
(	(	kIx(
<g/>
Tripos	Tripos	k1gMnSc1
Inc	Inc	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
červenci	červenec	k1gInSc6
2006	#num#	k4
uvedla	uvést	k5eAaPmAgFnS
IUPAC	IUPAC	kA
jako	jako	k8xS,k8xC
standardní	standardní	k2eAgFnSc4d1
reprezentaci	reprezentace	k1gFnSc4
vzorců	vzorec	k1gInPc2
formát	formát	k1gInSc4
InChI	InChI	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
SMILES	SMILES	kA
se	se	k3xPyFc4
obecně	obecně	k6eAd1
tvrdí	tvrdit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
má	mít	k5eAaImIp3nS
oproti	oproti	k7c3
InChI	InChI	k1gFnSc3
výhodu	výhod	k1gInSc2
v	v	k7c4
o	o	k7c4
něco	něco	k6eAd1
lepší	dobrý	k2eAgFnPc4d2
lidské	lidský	k2eAgFnPc4d1
čitelnosti	čitelnost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
také	také	k6eAd1
podporován	podporovat	k5eAaImNgMnS
širší	široký	k2eAgFnSc7d2
základnou	základna	k1gFnSc7
softwaru	software	k1gInSc2
s	s	k7c7
rozsáhlým	rozsáhlý	k2eAgNnSc7d1
teoretickým	teoretický	k2eAgNnSc7d1
zázemím	zázemí	k1gNnSc7
(	(	kIx(
<g/>
např.	např.	kA
teorií	teorie	k1gFnPc2
grafů	graf	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
InChI	InChI	k?
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Simplified	Simplified	k1gMnSc1
molecular	molecular	k1gMnSc1
input	input	k1gMnSc1
line	linout	k5eAaImIp3nS
entry	entr	k1gMnPc4
specification	specification	k1gInSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Anderson	Anderson	k1gMnSc1
<g/>
,	,	kIx,
E.	E.	kA
<g/>
;	;	kIx,
Veith	Veith	k1gMnSc1
<g/>
,	,	kIx,
G.D	G.D	k1gMnSc1
<g/>
;	;	kIx,
Weininger	Weininger	k1gMnSc1
<g/>
,	,	kIx,
D.	D.	kA
(	(	kIx(
<g/>
1987	#num#	k4
<g/>
)	)	kIx)
SMILES	SMILES	kA
<g/>
:	:	kIx,
A	a	k9
line	linout	k5eAaImIp3nS
notation	notation	k1gInSc1
and	and	k?
computerized	computerized	k1gInSc1
interpreter	interpreter	k1gInSc1
for	forum	k1gNnPc2
chemical	chemicat	k5eAaPmAgMnS
structures	structures	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Report	report	k1gInSc4
No	no	k9
<g/>
.	.	kIx.
</s>
<s desamb="1">
EPA	EPA	kA
<g/>
/	/	kIx~
<g/>
600	#num#	k4
<g/>
/	/	kIx~
<g/>
M-	M-	k1gFnSc1
<g/>
87	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
21	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
U.	U.	kA
<g/>
S.	S.	kA
EPA	EPA	kA
<g/>
,	,	kIx,
Environmental	Environmental	k1gMnSc1
Research	Research	k1gMnSc1
Laboratory-Duluth	Laboratory-Duluth	k1gMnSc1
<g/>
,	,	kIx,
Duluth	Duluth	k1gMnSc1
<g/>
,	,	kIx,
MN	MN	kA
55804	#num#	k4
</s>
<s>
Weininger	Weininger	k1gMnSc1
<g/>
,	,	kIx,
D.	D.	kA
(	(	kIx(
<g/>
1988	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
SMILES	SMILES	kA
<g/>
,	,	kIx,
a	a	k8xC
chemical	chemicat	k5eAaPmAgMnS
language	language	k1gFnSc3
and	and	k?
information	information	k1gInSc1
system	syst	k1gMnSc7
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Introduction	Introduction	k1gInSc1
to	ten	k3xDgNnSc1
methodology	methodolog	k1gMnPc4
and	and	k?
encoding	encoding	k1gInSc1
rules	rules	k1gInSc1
<g/>
,	,	kIx,
J.	J.	kA
Chem.	Chem.	k1gMnSc1
Inf.	Inf.	k1gMnSc1
Comput	Comput	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sci	Sci	k1gFnSc1
<g/>
.	.	kIx.
28	#num#	k4
<g/>
,	,	kIx,
31	#num#	k4
<g/>
-	-	kIx~
<g/>
36	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Weininger	Weininger	k1gMnSc1
<g/>
,	,	kIx,
D.	D.	kA
<g/>
;	;	kIx,
Weininger	Weininger	k1gMnSc1
<g/>
,	,	kIx,
A.	A.	kA
<g/>
;	;	kIx,
Weininger	Weininger	k1gMnSc1
<g/>
,	,	kIx,
J.L.	J.L.	k1gMnSc1
(	(	kIx(
<g/>
1989	#num#	k4
<g/>
)	)	kIx)
SMILES	SMILES	kA
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Algorithm	Algorithm	k1gInSc1
for	forum	k1gNnPc2
generation	generation	k1gInSc1
of	of	k?
unique	uniquat	k5eAaPmIp3nS
SMILES	SMILES	kA
notation	notation	k1gInSc1
J.	J.	kA
Chem.	Chem.	k1gMnSc2
Inf.	Inf.	k1gMnSc1
Comput	Comput	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sci	Sci	k1gFnSc1
<g/>
.	.	kIx.
29	#num#	k4
<g/>
,	,	kIx,
97	#num#	k4
<g/>
-	-	kIx~
<g/>
101	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Helson	Helson	k1gMnSc1
<g/>
,	,	kIx,
H.E.	H.E.	k1gMnSc1
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
Structure	Structur	k1gMnSc5
Diagram	diagram	k1gInSc1
Generation	Generation	k1gInSc1
In	In	k1gMnSc2
Rev	Rev	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Comput	Comput	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chem.	Chem.	k1gMnSc1
edited	edited	k1gMnSc1
by	by	kYmCp3nS
Lipkowitz	Lipkowitza	k1gFnPc2
<g/>
,	,	kIx,
K.	K.	kA
B.	B.	kA
and	and	k?
Boyd	Boyd	k1gInSc1
<g/>
,	,	kIx,
D.	D.	kA
B.	B.	kA
Wiley-VCH	Wiley-VCH	k1gMnSc1
<g/>
,	,	kIx,
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
,	,	kIx,
pages	pages	k1gInSc1
313	#num#	k4
<g/>
-	-	kIx~
<g/>
398	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Specifikace	specifikace	k1gFnSc1
</s>
<s>
„	„	k?
<g/>
SMILES	SMILES	kA
–	–	k?
Zjednodušený	zjednodušený	k2eAgInSc4d1
chemický	chemický	k2eAgInSc4d1
jazyk	jazyk	k1gInSc4
<g/>
“	“	k?
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
OpenSMILES	OpenSMILES	k1gFnSc2
</s>
<s>
„	„	k?
<g/>
SMARTS	SMARTS	kA
–	–	k?
rozšíření	rozšíření	k1gNnSc1
SMILES	SMILES	kA
<g/>
“	“	k?
</s>
<s>
Daylight	Daylight	k1gMnSc1
SMILES	SMILES	kA
tutorial	tutorial	k1gMnSc1
</s>
<s>
Parsing	Parsing	k1gInSc1
SMILES	SMILES	kA
</s>
<s>
Softwarové	softwarový	k2eAgFnPc1d1
utility	utilita	k1gFnPc1
podporující	podporující	k2eAgFnSc2d1
SMILES	SMILES	kA
</s>
<s>
Online	Onlinout	k5eAaPmIp3nS
SMILES	SMILES	kA
Translator	Translator	k1gInSc1
and	and	k?
Structure	Structur	k1gMnSc5
File	Filus	k1gMnSc5
Generator	Generator	k1gMnSc1
–	–	k?
Java	Java	k1gMnSc1
online	onlinout	k5eAaPmIp3nS
molecule	molecula	k1gFnSc3
editor	editor	k1gInSc4
</s>
<s>
PubChem	PubCh	k1gInSc7
server	server	k1gInSc4
side	sid	k1gInSc2
structure	structur	k1gMnSc5
editor	editor	k1gInSc1
–	–	k?
online	onlinout	k5eAaPmIp3nS
molecule	molecule	k1gFnSc1
editor	editor	k1gInSc4
</s>
<s>
smi	smi	k?
<g/>
23	#num#	k4
<g/>
d	d	k?
–	–	k?
3D	3D	k4
Coordinate	Coordinat	k1gInSc5
Generation	Generation	k1gInSc1
</s>
<s>
Daylight	Daylight	k2eAgInSc1d1
Depict	Depict	k1gInSc1
–	–	k?
Translate	Translat	k1gInSc5
a	a	k8xC
SMILES	SMILES	kA
formula	formout	k5eAaPmAgFnS
into	into	k6eAd1
graphics	graphics	k6eAd1
</s>
<s>
–	–	k?
vytváření	vytváření	k1gNnSc1
souborů	soubor	k1gInPc2
ve	v	k7c6
formátu	formát	k1gInSc6
PNG	PNG	kA
a	a	k8xC
GIF	GIF	kA
</s>
<s>
JME	jmout	k5eAaPmIp3nS
molecule	molecule	k1gFnSc1
editor	editor	k1gInSc4
Archivováno	archivovat	k5eAaBmNgNnS
28	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
2001	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
</s>
<s>
ACD	ACD	kA
<g/>
/	/	kIx~
<g/>
ChemSketch	ChemSketch	k1gMnSc1
</s>
<s>
Marvin	Marvina	k1gFnPc2
by	by	k9
ChemAxon	ChemAxon	k1gNnSc1
–	–	k?
online	onlinout	k5eAaPmIp3nS
chemical	chemicat	k5eAaPmAgInS
editor	editor	k1gInSc4
<g/>
/	/	kIx~
<g/>
viewer	viewer	k1gMnSc1
and	and	k?
SMILlůES	SMILlůES	k1gMnSc1
generator	generator	k1gMnSc1
<g/>
/	/	kIx~
<g/>
converter	converter	k1gMnSc1
</s>
<s>
Instant	Instant	k1gInSc1
JChem	JChem	k1gInSc4
by	by	kYmCp3nS
ChemAxon	ChemAxon	k1gMnSc1
–	–	k?
desktop	desktop	k1gInSc1
application	application	k1gInSc1
for	forum	k1gNnPc2
storing	storing	k1gInSc1
<g/>
/	/	kIx~
<g/>
generating	generating	k1gInSc1
<g/>
/	/	kIx~
<g/>
converting	converting	k1gInSc1
<g/>
/	/	kIx~
<g/>
visualizing	visualizing	k1gInSc1
<g/>
/	/	kIx~
<g/>
searching	searching	k1gInSc1
SMILES	SMILES	kA
structures	structures	k1gInSc1
<g/>
,	,	kIx,
particularly	particularl	k1gInPc1
batch	batcha	k1gFnPc2
processing	processing	k1gInSc1
<g/>
;	;	kIx,
personal	personat	k5eAaPmAgInS,k5eAaImAgInS
edition	edition	k1gInSc1
free	free	k6eAd1
</s>
<s>
JChem	JChem	k6eAd1
for	forum	k1gNnPc2
Excel	Excel	kA
by	by	kYmCp3nS
ChemAxon	ChemAxon	k1gMnSc1
–	–	k?
MS	MS	kA
Excel	Excel	kA
add-in	add-in	k1gMnSc1
for	forum	k1gNnPc2
storing	storing	k1gInSc1
<g/>
/	/	kIx~
<g/>
generating	generating	k1gInSc1
<g/>
/	/	kIx~
<g/>
converting	converting	k1gInSc1
<g/>
/	/	kIx~
<g/>
visualizing	visualizing	k1gInSc1
<g/>
/	/	kIx~
<g/>
searching	searching	k1gInSc1
SMILES	SMILES	kA
structures	structures	k1gInSc1
</s>
<s>
Smormo-Ed	Smormo-Ed	k1gInSc1
–	–	k?
a	a	k8xC
molecule	molecule	k1gFnSc1
editor	editor	k1gInSc1
for	forum	k1gNnPc2
Linux	linux	k1gInSc1
which	which	k1gMnSc1
can	can	k?
read	read	k6eAd1
and	and	k?
write	write	k5eAaPmIp2nP
SMILES	SMILES	kA
</s>
<s>
InChI	InChI	k?
<g/>
.	.	kIx.
<g/>
info	info	k1gMnSc1
–	–	k?
an	an	k?
unofficial	unofficial	k1gMnSc1
InChI	InChI	k1gMnSc1
website	websit	k1gInSc5
featuring	featuring	k1gInSc1
on-line	on-lin	k1gInSc5
converter	convertrum	k1gNnPc2
from	from	k1gMnSc1
InChI	InChI	k1gFnSc2
and	and	k?
SMILES	SMILES	kA
to	ten	k3xDgNnSc4
molecular	molecular	k1gInSc1
drawings	drawings	k6eAd1
</s>
<s>
Balloon	Balloon	k1gMnSc1
–	–	k?
A	a	k9
free	freat	k5eAaPmIp3nS
program	program	k1gInSc1
for	forum	k1gNnPc2
3D	3D	k4
coordinate	coordinat	k1gInSc5
generation	generation	k1gInSc1
and	and	k?
conformational	conformationat	k5eAaPmAgInS,k5eAaImAgInS
analysis	analysis	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Dingo	dingo	k1gMnSc1
–	–	k?
an	an	k?
IUPAC-compliant	IUPAC-compliant	k1gInSc1
cross-platform	cross-platform	k1gInSc1
open-source	open-sourka	k1gFnSc3
library	librar	k1gMnPc4
for	forum	k1gNnPc2
molecule	molecule	k1gFnSc2
and	and	k?
reaction	reaction	k1gInSc1
2D	2D	k4
structural	structurat	k5eAaImAgInS,k5eAaPmAgInS
formula	formul	k1gMnSc4
rendering	rendering	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Open	Open	k1gMnSc1
Babel	Babel	k1gMnSc1
–	–	k?
an	an	k?
open-source	open-sourka	k1gFnSc3
chemical	chemicat	k5eAaPmAgInS
toolbox	toolbox	k1gInSc1
allowing	allowing	k1gInSc1
anyone	anyon	k1gInSc5
to	ten	k3xDgNnSc1
search	search	k1gMnSc1
<g/>
,	,	kIx,
convert	convert	k1gMnSc1
<g/>
,	,	kIx,
analyze	analyze	k1gFnSc1
<g/>
,	,	kIx,
or	or	k?
store	stor	k1gMnSc5
biochemical	biochemicat	k5eAaPmAgMnS
data	datum	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s>
Bioclipse	Bioclipse	k1gFnSc1
–	–	k?
a	a	k8xC
free	fre	k1gFnSc2
and	and	k?
open	open	k1gMnSc1
source	source	k1gMnSc1
workbench	workbench	k1gMnSc1
for	forum	k1gNnPc2
the	the	k?
life	lif	k1gFnSc2
sciences	sciencesa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
