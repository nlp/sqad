<s>
SMILES	SMILES	kA
je	být	k5eAaImIp3nS
zkratka	zkratka	k1gFnSc1
pro	pro	k7c4
anglický	anglický	k2eAgInSc4d1
termín	termín	k1gInSc4
„	„	k?
<g/>
simplified	simplified	k1gInSc1
molecular	molecular	k1gMnSc1
input	input	k1gMnSc1
line	linout	k5eAaImIp3nS
entry	entra	k1gMnSc2
specification	specification	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
čili	čili	k8xC
česky	česky	k6eAd1
„	„	k?
<g/>
zjednodušená	zjednodušený	k2eAgFnSc1d1
molekulární	molekulární	k2eAgFnSc1d1
specifikace	specifikace	k1gFnSc1
pro	pro	k7c4
vstupní	vstupní	k2eAgInPc4d1
řádky	řádek	k1gInPc4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>