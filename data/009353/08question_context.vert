<s>
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
(	(	kIx(	(
<g/>
*	*	kIx~	*
28.	[number]	k4	28.
září	zářit	k5eAaImIp3nS	zářit
1944	[number]	k4	1944
Kolín	Kolín	k1gInSc1	Kolín
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
ekonom	ekonom	k1gMnSc1	ekonom
<g/>
,	,	kIx,	,
prognostik	prognostik	k1gMnSc1	prognostik
a	a	k8xC	a
třetí	třetí	k4xOgMnSc1	třetí
prezident	prezident	k1gMnSc1	prezident
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Úřadu	úřad	k1gInSc2	úřad
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
ujal	ujmout	k5eAaPmAgMnS	ujmout
složením	složení	k1gNnSc7	složení
slibu	slib	k1gInSc2	slib
8.	[number]	k4	8.
března	březen	k1gInSc2	březen
2013	[number]	k4	2013
a	a	k8xC	a
druhé	druhý	k4xOgNnSc4	druhý
funkční	funkční	k2eAgNnSc4d1	funkční
období	období	k1gNnSc4	období
zahájil	zahájit	k5eAaPmAgInS	zahájit
přesně	přesně	k6eAd1	přesně
o	o	k7c4	o
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
1993	[number]	k4	1993
<g/>
–	–	k?	–
<g/>
2001	[number]	k4	2001
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
předseda	předseda	k1gMnSc1	předseda
České	český	k2eAgFnSc2d1	Česká
strany	strana	k1gFnSc2	strana
sociálně	sociálně	k6eAd1	sociálně
demokratické	demokratický	k2eAgFnPc1d1	demokratická
<g/>
.	.	kIx.	.
</s>

