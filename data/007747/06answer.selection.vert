<s>
Poslední	poslední	k2eAgInPc1d1	poslední
výsledky	výsledek	k1gInPc1	výsledek
z	z	k7c2	z
gravitačního	gravitační	k2eAgNnSc2d1	gravitační
měření	měření	k1gNnSc2	měření
sondy	sonda	k1gFnSc2	sonda
Magellan	Magellana	k1gFnPc2	Magellana
nasvědčují	nasvědčovat	k5eAaImIp3nP	nasvědčovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kůra	kůra	k1gFnSc1	kůra
Venuše	Venuše	k1gFnSc2	Venuše
je	být	k5eAaImIp3nS	být
silná	silný	k2eAgFnSc1d1	silná
přibližně	přibližně	k6eAd1	přibližně
35	[number]	k4	35
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
