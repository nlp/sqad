<p>
<s>
Hydrofon	hydrofon	k1gInSc1	hydrofon
je	být	k5eAaImIp3nS	být
podvodní	podvodní	k2eAgInSc1d1	podvodní
mikrofon	mikrofon	k1gInSc1	mikrofon
<g/>
,	,	kIx,	,
umožňující	umožňující	k2eAgInSc1d1	umožňující
pasivně	pasivně	k6eAd1	pasivně
naslouchat	naslouchat	k5eAaImF	naslouchat
zvukům	zvuk	k1gInPc3	zvuk
z	z	k7c2	z
okolní	okolní	k2eAgFnSc2d1	okolní
vody	voda	k1gFnSc2	voda
(	(	kIx(	(
<g/>
aktivním	aktivní	k2eAgNnSc7d1	aktivní
zařízením	zařízení	k1gNnSc7	zařízení
je	být	k5eAaImIp3nS	být
sonar	sonar	k1gInSc1	sonar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hydrofon	hydrofon	k1gInSc1	hydrofon
byl	být	k5eAaImAgInS	být
prvním	první	k4xOgNnSc7	první
podvodním	podvodní	k2eAgNnSc7d1	podvodní
čidlem	čidlo	k1gNnSc7	čidlo
vyvinutým	vyvinutý	k2eAgNnSc7d1	vyvinuté
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Představoval	představovat	k5eAaImAgMnS	představovat
důležitý	důležitý	k2eAgInSc4d1	důležitý
prostředek	prostředek	k1gInSc4	prostředek
protiponorkového	protiponorkový	k2eAgInSc2d1	protiponorkový
boje	boj	k1gInSc2	boj
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
umožňoval	umožňovat	k5eAaImAgInS	umožňovat
odhalit	odhalit	k5eAaPmF	odhalit
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
i	i	k9	i
lokalizovat	lokalizovat	k5eAaBmF	lokalizovat
ponořenou	ponořený	k2eAgFnSc4d1	ponořená
ponorku	ponorka	k1gFnSc4	ponorka
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
ponořená	ponořený	k2eAgFnSc1d1	ponořená
ponorka	ponorka	k1gFnSc1	ponorka
jím	jíst	k5eAaImIp1nS	jíst
mohla	moct	k5eAaImAgFnS	moct
získat	získat	k5eAaPmF	získat
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
svém	svůj	k3xOyFgNnSc6	svůj
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Hydrofon	hydrofon	k1gInSc1	hydrofon
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
protiponorkovém	protiponorkový	k2eAgInSc6d1	protiponorkový
boji	boj	k1gInSc6	boj
používán	používat	k5eAaImNgInS	používat
už	už	k6eAd1	už
za	za	k7c2	za
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
a	a	k8xC	a
přispěl	přispět	k5eAaPmAgInS	přispět
tak	tak	k6eAd1	tak
k	k	k7c3	k
ochraně	ochrana	k1gFnSc3	ochrana
spojeneckých	spojenecký	k2eAgInPc2d1	spojenecký
konvojů	konvoj	k1gInPc2	konvoj
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
také	také	k9	také
v	v	k7c6	v
seismologii	seismologie	k1gFnSc6	seismologie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Sonar	sonar	k1gInSc1	sonar
</s>
</p>
<p>
<s>
Radar	radar	k1gInSc1	radar
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
hydrofon	hydrofon	k1gInSc1	hydrofon
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Hydrophone	Hydrophon	k1gInSc5	Hydrophon
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
