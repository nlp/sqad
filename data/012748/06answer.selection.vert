<s>
Hydrofon	hydrofon	k1gInSc1	hydrofon
je	být	k5eAaImIp3nS	být
podvodní	podvodní	k2eAgInSc1d1	podvodní
mikrofon	mikrofon	k1gInSc1	mikrofon
<g/>
,	,	kIx,	,
umožňující	umožňující	k2eAgInSc1d1	umožňující
pasivně	pasivně	k6eAd1	pasivně
naslouchat	naslouchat	k5eAaImF	naslouchat
zvukům	zvuk	k1gInPc3	zvuk
z	z	k7c2	z
okolní	okolní	k2eAgFnSc2d1	okolní
vody	voda	k1gFnSc2	voda
(	(	kIx(	(
<g/>
aktivním	aktivní	k2eAgNnSc7d1	aktivní
zařízením	zařízení	k1gNnSc7	zařízení
je	být	k5eAaImIp3nS	být
sonar	sonar	k1gInSc1	sonar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
