<s>
Jak	jak	k6eAd1	jak
ze	z	k7c2	z
nazývá	nazývat	k5eAaImIp3nS	nazývat
podvodní	podvodní	k2eAgInSc1d1	podvodní
mikrofon	mikrofon	k1gInSc1	mikrofon
<g/>
,	,	kIx,	,
umožňující	umožňující	k2eAgInSc1d1	umožňující
pasivně	pasivně	k6eAd1	pasivně
naslouchat	naslouchat	k5eAaImF	naslouchat
zvukům	zvuk	k1gInPc3	zvuk
z	z	k7c2	z
okolní	okolní	k2eAgFnSc2d1	okolní
vody	voda	k1gFnSc2	voda
<g/>
?	?	kIx.	?
</s>
