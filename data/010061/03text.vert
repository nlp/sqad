<p>
<s>
Mrkev	mrkev	k1gFnSc1	mrkev
obecná	obecná	k1gFnSc1	obecná
(	(	kIx(	(
<g/>
Daucus	Daucus	k1gInSc1	Daucus
carota	carota	k1gFnSc1	carota
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rostlina	rostlina	k1gFnSc1	rostlina
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
miříkovitých	miříkovitý	k2eAgMnPc2d1	miříkovitý
<g/>
,	,	kIx,	,
pěstovaná	pěstovaný	k2eAgFnSc1d1	pěstovaná
jako	jako	k8xC	jako
kořenová	kořenový	k2eAgFnSc1d1	kořenová
zelenina	zelenina	k1gFnSc1	zelenina
<g/>
.	.	kIx.	.
</s>
<s>
Blízkým	blízký	k2eAgMnPc3d1	blízký
příbuzným	příbuzný	k1gMnPc3	příbuzný
mrkve	mrkev	k1gFnSc2	mrkev
je	být	k5eAaImIp3nS	být
pastinák	pastinák	k1gInSc1	pastinák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poddruhy	poddruh	k1gInPc4	poddruh
==	==	k?	==
</s>
</p>
<p>
<s>
Mrkev	mrkev	k1gFnSc1	mrkev
obecná	obecná	k1gFnSc1	obecná
pravá	pravá	k1gFnSc1	pravá
Daucus	Daucus	k1gInSc1	Daucus
carota	carota	k1gFnSc1	carota
subsp	subsp	k1gMnSc1	subsp
<g/>
.	.	kIx.	.
carota	carota	k1gFnSc1	carota
(	(	kIx(	(
<g/>
syn	syn	k1gMnSc1	syn
<g/>
.	.	kIx.	.
</s>
<s>
Daucus	Daucus	k1gMnSc1	Daucus
sylvestris	sylvestris	k1gFnPc2	sylvestris
Miller	Miller	k1gMnSc1	Miller
<g/>
,	,	kIx,	,
1768	[number]	k4	1768
<g/>
;	;	kIx,	;
syn	syn	k1gMnSc1	syn
<g/>
.	.	kIx.	.
</s>
<s>
Daucus	Daucus	k1gInSc1	Daucus
carota	carota	k1gFnSc1	carota
var.	var.	k?	var.
sylvestris	sylvestris	k1gFnSc1	sylvestris
(	(	kIx(	(
<g/>
Miller	Miller	k1gMnSc1	Miller
<g/>
)	)	kIx)	)
Hoffmann	Hoffmann	k1gMnSc1	Hoffmann
<g/>
,	,	kIx,	,
1791	[number]	k4	1791
<g/>
;	;	kIx,	;
syn	syn	k1gMnSc1	syn
<g/>
.	.	kIx.	.
</s>
<s>
Daucus	Daucus	k1gMnSc1	Daucus
carota	carot	k1gMnSc2	carot
subsp	subsp	k1gMnSc1	subsp
<g/>
.	.	kIx.	.
sylvestris	sylvestris	k1gFnSc1	sylvestris
(	(	kIx(	(
<g/>
Miller	Miller	k1gMnSc1	Miller
<g/>
)	)	kIx)	)
Schübler	Schübler	k1gMnSc1	Schübler
&	&	k?	&
Martens	Martens	k1gInSc1	Martens
<g/>
,	,	kIx,	,
1834	[number]	k4	1834
<g/>
)	)	kIx)	)
-	-	kIx~	-
Původní	původní	k2eAgFnSc1d1	původní
divoká	divoký	k2eAgFnSc1d1	divoká
nešlechtěná	šlechtěný	k2eNgFnSc1d1	nešlechtěná
forma	forma	k1gFnSc1	forma
mrkve	mrkev	k1gFnSc2	mrkev
<g/>
.	.	kIx.	.
</s>
<s>
Kořen	kořen	k1gInSc1	kořen
je	být	k5eAaImIp3nS	být
tenký	tenký	k2eAgInSc1d1	tenký
a	a	k8xC	a
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mrkev	mrkev	k1gFnSc1	mrkev
obecná	obecný	k2eAgFnSc1d1	obecná
setá	setý	k2eAgFnSc1d1	setá
Daucus	Daucus	k1gMnSc1	Daucus
carota	carot	k1gMnSc2	carot
subsp	subsp	k1gMnSc1	subsp
<g/>
.	.	kIx.	.
sativus	sativus	k1gMnSc1	sativus
(	(	kIx(	(
<g/>
Hoffmann	Hoffmann	k1gMnSc1	Hoffmann
<g/>
)	)	kIx)	)
Arcangeli	Arcangel	k1gInPc7	Arcangel
<g/>
,	,	kIx,	,
1882	[number]	k4	1882
ex	ex	k6eAd1	ex
Schübler	Schübler	k1gInSc1	Schübler
&	&	k?	&
Martens	Martens	k1gInSc1	Martens
<g/>
,	,	kIx,	,
1834	[number]	k4	1834
(	(	kIx(	(
<g/>
syn	syn	k1gMnSc1	syn
<g/>
.	.	kIx.	.
</s>
<s>
Daucus	Daucus	k1gInSc1	Daucus
carota	carot	k1gMnSc2	carot
var.	var.	k?	var.
sativus	sativus	k1gMnSc1	sativus
Hoffmann	Hoffmann	k1gMnSc1	Hoffmann
<g/>
,	,	kIx,	,
1791	[number]	k4	1791
<g/>
;	;	kIx,	;
syn	syn	k1gMnSc1	syn
<g/>
.	.	kIx.	.
</s>
<s>
Daucus	Daucus	k1gMnSc1	Daucus
sativus	sativus	k1gMnSc1	sativus
(	(	kIx(	(
<g/>
Hoffmann	Hoffmann	k1gMnSc1	Hoffmann
<g/>
)	)	kIx)	)
Passerini	Passerin	k1gMnPc1	Passerin
<g/>
,	,	kIx,	,
1852	[number]	k4	1852
ex	ex	k6eAd1	ex
Röhling	Röhling	k1gInSc1	Röhling
<g/>
,	,	kIx,	,
1812	[number]	k4	1812
<g/>
)	)	kIx)	)
-	-	kIx~	-
Sem	sem	k6eAd1	sem
patří	patřit	k5eAaImIp3nS	patřit
všechny	všechen	k3xTgFnPc4	všechen
pěstované	pěstovaný	k2eAgFnPc4d1	pěstovaná
odrůdy	odrůda	k1gFnPc4	odrůda
mrkve	mrkev	k1gFnSc2	mrkev
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
původní	původní	k2eAgFnSc2d1	původní
plané	planý	k2eAgFnSc2d1	planá
mrkve	mrkev	k1gFnSc2	mrkev
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
zejména	zejména	k9	zejména
mohutností	mohutnost	k1gFnSc7	mohutnost
kořene	kořen	k1gInSc2	kořen
<g/>
.	.	kIx.	.
</s>
<s>
Barva	barva	k1gFnSc1	barva
kořene	kořen	k1gInSc2	kořen
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
,	,	kIx,	,
žlutá	žlutý	k2eAgFnSc1d1	žlutá
<g/>
,	,	kIx,	,
oranžová	oranžový	k2eAgFnSc1d1	oranžová
<g/>
,	,	kIx,	,
červená	červený	k2eAgFnSc1d1	červená
<g/>
,	,	kIx,	,
nachová	nachový	k2eAgFnSc1d1	nachová
<g/>
,	,	kIx,	,
fialová	fialový	k2eAgFnSc1d1	fialová
i	i	k8xC	i
vícebarevná	vícebarevný	k2eAgFnSc1d1	vícebarevná
(	(	kIx(	(
<g/>
např.	např.	kA	např.
zvenku	zvenku	k6eAd1	zvenku
fialová	fialový	k2eAgFnSc1d1	fialová
<g/>
,	,	kIx,	,
vevnitř	vevnitř	k6eAd1	vevnitř
oranžová	oranžový	k2eAgFnSc1d1	oranžová
nebo	nebo	k8xC	nebo
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozdělení	rozdělení	k1gNnSc1	rozdělení
odrůd	odrůda	k1gFnPc2	odrůda
==	==	k?	==
</s>
</p>
<p>
<s>
Odrůdy	odrůda	k1gFnPc1	odrůda
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
do	do	k7c2	do
několika	několik	k4yIc2	několik
základních	základní	k2eAgFnPc2d1	základní
skupin	skupina	k1gFnPc2	skupina
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Typ	typ	k1gInSc1	typ
Berlikum	Berlikum	k1gNnSc1	Berlikum
má	mít	k5eAaImIp3nS	mít
větší	veliký	k2eAgNnSc1d2	veliký
<g/>
,	,	kIx,	,
mohutné	mohutný	k2eAgInPc1d1	mohutný
kořeny	kořen	k1gInPc1	kořen
<g/>
,	,	kIx,	,
válcovité	válcovitý	k2eAgInPc1d1	válcovitý
až	až	k9	až
mírně	mírně	k6eAd1	mírně
kónické	kónický	k2eAgNnSc1d1	kónické
<g/>
,	,	kIx,	,
s	s	k7c7	s
tupým	tupý	k2eAgNnSc7d1	tupé
zakončením	zakončení	k1gNnSc7	zakončení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Typ	typ	k1gInSc1	typ
Flakee	Flake	k1gFnSc2	Flake
má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
<g/>
,	,	kIx,	,
kónicky	kónicky	k6eAd1	kónicky
zúžené	zúžený	k2eAgInPc4d1	zúžený
kořeny	kořen	k1gInPc4	kořen
s	s	k7c7	s
širokou	široký	k2eAgFnSc7d1	široká
plochou	plocha	k1gFnSc7	plocha
hlavou	hlava	k1gFnSc7	hlava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Typ	typ	k1gInSc1	typ
Chantenay	Chantenaa	k1gFnSc2	Chantenaa
má	mít	k5eAaImIp3nS	mít
kratší	krátký	k2eAgInSc4d2	kratší
kořen	kořen	k1gInSc4	kořen
kónického	kónický	k2eAgInSc2d1	kónický
tvaru	tvar	k1gInSc2	tvar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Typ	typ	k1gInSc1	typ
Nantes	Nantes	k1gInSc1	Nantes
je	být	k5eAaImIp3nS	být
pěstován	pěstovat	k5eAaImNgInS	pěstovat
nejčastěji	často	k6eAd3	často
–	–	k?	–
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
typická	typický	k2eAgFnSc1d1	typická
karotka	karotka	k1gFnSc1	karotka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Typ	typ	k1gInSc1	typ
Parisian	Parisiany	k1gInPc2	Parisiany
je	být	k5eAaImIp3nS	být
krátký	krátký	k2eAgInSc4d1	krátký
kulatý	kulatý	k2eAgInSc4d1	kulatý
kořen	kořen	k1gInSc4	kořen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Synonyma	synonymum	k1gNnSc2	synonymum
==	==	k?	==
</s>
</p>
<p>
<s>
Daucus	Daucus	k1gInSc1	Daucus
gingidium	gingidium	k1gNnSc1	gingidium
Linné	Linná	k1gFnSc2	Linná
<g/>
,	,	kIx,	,
1753	[number]	k4	1753
</s>
</p>
<p>
<s>
Daucus	Daucus	k1gMnSc1	Daucus
vulgaris	vulgaris	k1gFnPc2	vulgaris
Garsault	Garsault	k1gMnSc1	Garsault
<g/>
,	,	kIx,	,
1767	[number]	k4	1767
</s>
</p>
<p>
<s>
Daucus	Daucus	k1gMnSc1	Daucus
vulgaris	vulgaris	k1gFnPc2	vulgaris
Lamarck	Lamarck	k1gMnSc1	Lamarck
<g/>
,	,	kIx,	,
1779	[number]	k4	1779
(	(	kIx(	(
<g/>
nom.	nom.	k?	nom.
illeg	illeg	k1gInSc1	illeg
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Daucus	Daucus	k1gInSc1	Daucus
communis	communis	k1gFnSc2	communis
Rouy	Roua	k1gFnSc2	Roua
&	&	k?	&
E.	E.	kA	E.
G.	G.	kA	G.
Camus	Camus	k1gMnSc1	Camus
in	in	k?	in
Rouy	Roua	k1gFnSc2	Roua
<g/>
,	,	kIx,	,
1901	[number]	k4	1901
(	(	kIx(	(
<g/>
nom.	nom.	k?	nom.
illeg	illeg	k1gInSc1	illeg
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Vzhled	vzhled	k1gInSc4	vzhled
==	==	k?	==
</s>
</p>
<p>
<s>
Mrkev	mrkev	k1gFnSc1	mrkev
je	být	k5eAaImIp3nS	být
dvouletá	dvouletý	k2eAgFnSc1d1	dvouletá
rostlina	rostlina	k1gFnSc1	rostlina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
první	první	k4xOgInSc4	první
rok	rok	k1gInSc4	rok
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
přízemní	přízemní	k2eAgFnSc4d1	přízemní
růžici	růžice	k1gFnSc4	růžice
listů	list	k1gInPc2	list
a	a	k8xC	a
v	v	k7c6	v
mohutném	mohutný	k2eAgInSc6d1	mohutný
hlavním	hlavní	k2eAgInSc6d1	hlavní
kořeni	kořen	k1gInSc6	kořen
shromažďuje	shromažďovat	k5eAaImIp3nS	shromažďovat
živiny	živina	k1gFnPc4	živina
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
rok	rok	k1gInSc1	rok
vyžene	vyhnat	k5eAaPmIp3nS	vyhnat
lodyhu	lodyha	k1gFnSc4	lodyha
s	s	k7c7	s
okoličnatým	okoličnatý	k2eAgNnSc7d1	okoličnatý
květenstvím	květenství	k1gNnSc7	květenství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
Rostlina	rostlina	k1gFnSc1	rostlina
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
jižní	jižní	k2eAgFnSc2d1	jižní
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
Afghánistánu	Afghánistán	k1gInSc2	Afghánistán
<g/>
,	,	kIx,	,
Íránu	Írán	k1gInSc2	Írán
a	a	k8xC	a
Pákistánu	Pákistán	k1gInSc2	Pákistán
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ještě	ještě	k6eAd1	ještě
přežívají	přežívat	k5eAaImIp3nP	přežívat
její	její	k3xOp3gMnPc1	její
divocí	divoký	k2eAgMnPc1d1	divoký
a	a	k8xC	a
nezkultivovaní	zkultivovaný	k2eNgMnPc1d1	zkultivovaný
zástupci	zástupce	k1gMnPc1	zástupce
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
tak	tak	k9	tak
centrum	centrum	k1gNnSc4	centrum
diverzity	diverzita	k1gFnSc2	diverzita
druhu	druh	k1gInSc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Historie	historie	k1gFnSc1	historie
jejího	její	k3xOp3gNnSc2	její
rozšíření	rozšíření	k1gNnSc2	rozšíření
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
jistá	jistý	k2eAgFnSc1d1	jistá
<g/>
;	;	kIx,	;
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
do	do	k7c2	do
celé	celý	k2eAgFnSc2d1	celá
oblasti	oblast	k1gFnSc2	oblast
od	od	k7c2	od
Indie	Indie	k1gFnSc2	Indie
po	po	k7c4	po
východní	východní	k2eAgNnPc4d1	východní
Středomoří	středomoří	k1gNnPc4	středomoří
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
až	až	k9	až
do	do	k7c2	do
západní	západní	k2eAgFnSc2d1	západní
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
do	do	k7c2	do
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Nizozemí	Nizozemí	k1gNnSc6	Nizozemí
vyšlechtěna	vyšlechtěn	k2eAgFnSc1d1	vyšlechtěna
její	její	k3xOp3gFnSc1	její
oranžová	oranžový	k2eAgFnSc1d1	oranžová
odrůda	odrůda	k1gFnSc1	odrůda
(	(	kIx(	(
<g/>
pocházející	pocházející	k2eAgFnPc1d1	pocházející
z	z	k7c2	z
dřívější	dřívější	k2eAgFnSc2d1	dřívější
černé	černý	k2eAgFnSc2d1	černá
odrůdy	odrůda	k1gFnSc2	odrůda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
dnes	dnes	k6eAd1	dnes
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Divoké	divoký	k2eAgMnPc4d1	divoký
zástupce	zástupce	k1gMnPc4	zástupce
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
můžeme	moct	k5eAaImIp1nP	moct
objevit	objevit	k5eAaPmF	objevit
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
i	i	k8xC	i
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obsahové	obsahový	k2eAgFnPc1d1	obsahová
látky	látka	k1gFnPc1	látka
==	==	k?	==
</s>
</p>
<p>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
mnoho	mnoho	k4c1	mnoho
vitamínů	vitamín	k1gInPc2	vitamín
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
užitečných	užitečný	k2eAgFnPc2d1	užitečná
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
nejvýznamnější	významný	k2eAgMnPc4d3	nejvýznamnější
jsou	být	k5eAaImIp3nP	být
β	β	k?	β
–	–	k?	–
dimery	dimera	k1gFnPc4	dimera
vitamínu	vitamín	k1gInSc2	vitamín
A	a	k9	a
zodpovědné	zodpovědný	k2eAgNnSc1d1	zodpovědné
za	za	k7c4	za
červenou	červený	k2eAgFnSc4d1	červená
barvu	barva	k1gFnSc4	barva
kořene	kořen	k1gInSc2	kořen
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
bohatá	bohatý	k2eAgFnSc1d1	bohatá
na	na	k7c4	na
vlákniny	vláknina	k1gFnPc4	vláknina
a	a	k8xC	a
antioxidanty	antioxidant	k1gInPc4	antioxidant
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
chemické	chemický	k2eAgFnSc6d1	chemická
stránce	stránka	k1gFnSc6	stránka
byla	být	k5eAaImAgFnS	být
podrobně	podrobně	k6eAd1	podrobně
prozkoumána	prozkoumat	k5eAaPmNgFnS	prozkoumat
a	a	k8xC	a
v	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
kořenu	kořen	k1gInSc6	kořen
a	a	k8xC	a
semenech	semeno	k1gNnPc6	semeno
bylo	být	k5eAaImAgNnS	být
nalezeno	naleznout	k5eAaPmNgNnS	naleznout
několik	několik	k4yIc4	několik
set	sto	k4xCgNnPc2	sto
různých	různý	k2eAgFnPc2d1	různá
chemických	chemický	k2eAgFnPc2d1	chemická
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tabulka	tabulka	k1gFnSc1	tabulka
udává	udávat	k5eAaImIp3nS	udávat
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
průměrný	průměrný	k2eAgInSc1d1	průměrný
obsah	obsah	k1gInSc1	obsah
živin	živina	k1gFnPc2	živina
<g/>
,	,	kIx,	,
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
vitamínů	vitamín	k1gInPc2	vitamín
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
nutričních	nutriční	k2eAgInPc2d1	nutriční
parametrů	parametr	k1gInPc2	parametr
zjištěných	zjištěný	k2eAgInPc2d1	zjištěný
v	v	k7c6	v
syrové	syrový	k2eAgFnSc6d1	syrová
mrkvi	mrkev	k1gFnSc6	mrkev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Použití	použití	k1gNnSc3	použití
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Použití	použití	k1gNnSc3	použití
v	v	k7c6	v
kuchyni	kuchyně	k1gFnSc6	kuchyně
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
kuchyni	kuchyně	k1gFnSc6	kuchyně
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
kořen	kořen	k1gInSc1	kořen
i	i	k8xC	i
nať	nať	k1gFnSc1	nať
(	(	kIx(	(
<g/>
převážně	převážně	k6eAd1	převážně
k	k	k7c3	k
dochucení	dochucení	k1gNnSc3	dochucení
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
např.	např.	kA	např.
nať	nať	k1gFnSc1	nať
petržele	petržel	k1gFnSc2	petržel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mrkev	mrkev	k1gFnSc1	mrkev
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
jíst	jíst	k5eAaImF	jíst
syrová	syrový	k2eAgFnSc1d1	syrová
<g/>
,	,	kIx,	,
vcelku	vcelku	k6eAd1	vcelku
jen	jen	k9	jen
tak	tak	k6eAd1	tak
po	po	k7c6	po
opláchnutí	opláchnutí	k1gNnSc6	opláchnutí
<g/>
,	,	kIx,	,
nasekaná	nasekaný	k2eAgFnSc1d1	nasekaná
či	či	k8xC	či
nastrouhaná	nastrouhaný	k2eAgFnSc1d1	nastrouhaná
do	do	k7c2	do
salátů	salát	k1gInPc2	salát
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
s	s	k7c7	s
jablky	jablko	k1gNnPc7	jablko
nebo	nebo	k8xC	nebo
cibulí	cibule	k1gFnSc7	cibule
<g/>
,	,	kIx,	,
posypané	posypaný	k2eAgNnSc1d1	posypané
petrželkou	petrželka	k1gFnSc7	petrželka
<g/>
,	,	kIx,	,
pažitkou	pažitka	k1gFnSc7	pažitka
nebo	nebo	k8xC	nebo
ořechovými	ořechový	k2eAgNnPc7d1	ořechové
jádry	jádro	k1gNnPc7	jádro
<g/>
,	,	kIx,	,
či	či	k8xC	či
tepelně	tepelně	k6eAd1	tepelně
upravená	upravený	k2eAgFnSc1d1	upravená
v	v	k7c6	v
polévce	polévka	k1gFnSc6	polévka
<g/>
,	,	kIx,	,
játrové	játrový	k2eAgFnSc6d1	játrová
omáčce	omáčka	k1gFnSc6	omáčka
atd	atd	kA	atd
<g/>
...	...	k?	...
</s>
</p>
<p>
<s>
V	v	k7c6	v
polévkách	polévka	k1gFnPc6	polévka
bývá	bývat	k5eAaImIp3nS	bývat
často	často	k6eAd1	často
kombinována	kombinovat	k5eAaImNgFnS	kombinovat
s	s	k7c7	s
celerem	celer	k1gInSc7	celer
<g/>
,	,	kIx,	,
petrželí	petržel	k1gFnSc7	petržel
a	a	k8xC	a
cibulí	cibule	k1gFnSc7	cibule
kuchyňskou	kuchyňská	k1gFnSc7	kuchyňská
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Lékařství	lékařství	k1gNnSc1	lékařství
a	a	k8xC	a
léčitelství	léčitelství	k1gNnSc1	léčitelství
===	===	k?	===
</s>
</p>
<p>
<s>
Jako	jako	k8xS	jako
droga	droga	k1gFnSc1	droga
se	se	k3xPyFc4	se
sbírá	sbírat	k5eAaImIp3nS	sbírat
její	její	k3xOp3gInSc4	její
kořen	kořen	k1gInSc4	kořen
(	(	kIx(	(
<g/>
Radix	radix	k1gInSc4	radix
dauci	dauce	k1gFnSc4	dauce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
semena	semeno	k1gNnPc1	semeno
(	(	kIx(	(
<g/>
Fructus	Fructus	k1gInSc1	Fructus
dauci	dauce	k1gFnSc4	dauce
<g/>
)	)	kIx)	)
a	a	k8xC	a
výjimečně	výjimečně	k6eAd1	výjimečně
i	i	k9	i
nať	nať	k1gFnSc1	nať
(	(	kIx(	(
<g/>
Herba	Herba	k1gMnSc1	Herba
dauci	dauce	k1gFnSc4	dauce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mrkev	mrkev	k1gFnSc1	mrkev
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
léčitelství	léčitelství	k1gNnSc6	léčitelství
jako	jako	k8xC	jako
močopudný	močopudný	k2eAgInSc1d1	močopudný
a	a	k8xC	a
projímavý	projímavý	k2eAgInSc1d1	projímavý
prostředek	prostředek	k1gInSc1	prostředek
<g/>
,	,	kIx,	,
při	při	k7c6	při
šerosleposti	šeroslepost	k1gFnSc6	šeroslepost
<g/>
,	,	kIx,	,
močových	močový	k2eAgInPc6d1	močový
kamenech	kámen	k1gInPc6	kámen
a	a	k8xC	a
revmatických	revmatický	k2eAgInPc6d1	revmatický
zánětech	zánět	k1gInPc6	zánět
kloubů	kloub	k1gInPc2	kloub
<g/>
.	.	kIx.	.
</s>
<s>
Šťáva	šťáva	k1gFnSc1	šťáva
z	z	k7c2	z
kořene	kořen	k1gInSc2	kořen
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
také	také	k9	také
při	při	k7c6	při
ischemické	ischemický	k2eAgFnSc6d1	ischemická
chorobě	choroba	k1gFnSc6	choroba
srdeční	srdeční	k2eAgFnSc6d1	srdeční
<g/>
.	.	kIx.	.
</s>
<s>
Mrkev	mrkev	k1gFnSc1	mrkev
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
důležitou	důležitý	k2eAgFnSc4d1	důležitá
vlákninu	vláknina	k1gFnSc4	vláknina
a	a	k8xC	a
podporuje	podporovat	k5eAaImIp3nS	podporovat
vylučování	vylučování	k1gNnSc1	vylučování
cholesterolu	cholesterol	k1gInSc2	cholesterol
z	z	k7c2	z
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
úhradu	úhrada	k1gFnSc4	úhrada
vitaminu	vitamin	k1gInSc2	vitamin
A	a	k9	a
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
sníst	sníst	k5eAaPmF	sníst
asi	asi	k9	asi
50	[number]	k4	50
g	g	kA	g
syrové	syrový	k2eAgNnSc4d1	syrové
denně	denně	k6eAd1	denně
<g/>
,	,	kIx,	,
především	především	k9	především
v	v	k7c6	v
zimním	zimní	k2eAgNnSc6d1	zimní
a	a	k8xC	a
předjarním	předjarní	k2eAgNnSc6d1	předjarní
období	období	k1gNnSc6	období
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2005	[number]	k4	2005
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
látka	látka	k1gFnSc1	látka
falkarinol	falkarinola	k1gFnPc2	falkarinola
(	(	kIx(	(
<g/>
falcarinol	falcarinol	k1gInSc1	falcarinol
<g/>
)	)	kIx)	)
obsažená	obsažený	k2eAgFnSc1d1	obsažená
v	v	k7c6	v
mrkvi	mrkev	k1gFnSc6	mrkev
může	moct	k5eAaImIp3nS	moct
snižovat	snižovat	k5eAaImF	snižovat
rozvoj	rozvoj	k1gInSc4	rozvoj
rakoviny	rakovina	k1gFnSc2	rakovina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Květena	květena	k1gFnSc1	květena
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
díl	díl	k1gInSc1	díl
5	[number]	k4	5
/	/	kIx~	/
B.	B.	kA	B.
Slavík	Slavík	k1gMnSc1	Slavík
(	(	kIx(	(
<g/>
Ed	Ed	k1gMnSc1	Ed
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
-	-	kIx~	-
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
-	-	kIx~	-
S.	S.	kA	S.
421	[number]	k4	421
<g/>
-	-	kIx~	-
<g/>
422	[number]	k4	422
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
mrkev	mrkev	k1gFnSc1	mrkev
obecná	obecný	k2eAgFnSc1d1	obecná
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Mrkev	mrkev	k1gFnSc1	mrkev
obecná	obecná	k1gFnSc1	obecná
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Mrkev	mrkev	k1gFnSc1	mrkev
obecná	obecná	k1gFnSc1	obecná
na	na	k7c4	na
botanika	botanik	k1gMnSc4	botanik
<g/>
.	.	kIx.	.
<g/>
wendys	wendys	k6eAd1	wendys
</s>
</p>
<p>
<s>
Mrkev	mrkev	k1gFnSc1	mrkev
obecná	obecná	k1gFnSc1	obecná
na	na	k7c6	na
biolibu	biolib	k1gInSc6	biolib
</s>
</p>
<p>
<s>
NAŘÍZENÍ	nařízení	k1gNnSc1	nařízení
KOMISE	komise	k1gFnSc2	komise
(	(	kIx(	(
<g/>
ES	ES	kA	ES
<g/>
)	)	kIx)	)
č.	č.	k?	č.
730	[number]	k4	730
<g/>
/	/	kIx~	/
<g/>
1999	[number]	k4	1999
ze	z	k7c2	z
dne	den	k1gInSc2	den
7	[number]	k4	7
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
kterým	který	k3yIgNnSc7	který
se	se	k3xPyFc4	se
stanoví	stanovit	k5eAaPmIp3nS	stanovit
obchodní	obchodní	k2eAgFnSc1d1	obchodní
norma	norma	k1gFnSc1	norma
pro	pro	k7c4	pro
mrkev	mrkev	k1gFnSc4	mrkev
</s>
</p>
<p>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
den	den	k1gInSc1	den
mrkve	mrkev	k1gFnSc2	mrkev
</s>
</p>
<p>
<s>
Mrkev	mrkev	k1gFnSc1	mrkev
bývala	bývat	k5eAaImAgFnS	bývat
fialová	fialový	k2eAgFnSc1d1	fialová
i	i	k8xC	i
žlutá	žlutý	k2eAgFnSc1d1	žlutá
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
oranžovou	oranžový	k2eAgFnSc4d1	oranžová
barvu	barva	k1gFnSc4	barva
může	moct	k5eAaImIp3nS	moct
prý	prý	k9	prý
politika	politika	k1gFnSc1	politika
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
technet	technet	k1gInSc1	technet
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
)	)	kIx)	)
</s>
</p>
