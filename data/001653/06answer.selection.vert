<s>
Lev	Lev	k1gMnSc1	Lev
Davidovič	Davidovič	k1gMnSc1	Davidovič
Landau	Landaa	k1gFnSc4	Landaa
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
:	:	kIx,	:
Л	Л	k?	Л
<g/>
́	́	k?	́
<g/>
в	в	k?	в
Д	Д	k?	Д
<g/>
́	́	k?	́
<g/>
д	д	k?	д
Л	Л	k?	Л
<g/>
́	́	k?	́
<g/>
у	у	k?	у
<g/>
;	;	kIx,	;
22	[number]	k4	22
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1908	[number]	k4	1908
<g/>
,	,	kIx,	,
Baku	Baku	k1gNnSc1	Baku
-	-	kIx~	-
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
sovětský	sovětský	k2eAgMnSc1d1	sovětský
fyzik	fyzik	k1gMnSc1	fyzik
židovského	židovský	k2eAgInSc2d1	židovský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
přispěl	přispět	k5eAaPmAgInS	přispět
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
mnoha	mnoho	k4c2	mnoho
oblastí	oblast	k1gFnPc2	oblast
teoretické	teoretický	k2eAgFnSc2d1	teoretická
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
.	.	kIx.	.
</s>
