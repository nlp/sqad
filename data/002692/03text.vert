<s>
Cerekvický	Cerekvický	k2eAgInSc1d1	Cerekvický
potok	potok	k1gInSc1	potok
je	být	k5eAaImIp3nS	být
levostranný	levostranný	k2eAgInSc1d1	levostranný
přítok	přítok	k1gInSc1	přítok
říčky	říčka	k1gFnSc2	říčka
Hejlovky	Hejlovka	k1gFnSc2	Hejlovka
protékající	protékající	k2eAgFnSc2d1	protékající
okresem	okres	k1gInSc7	okres
Pelhřimov	Pelhřimov	k1gInSc1	Pelhřimov
v	v	k7c6	v
Kraji	kraj	k1gInSc6	kraj
Vysočina	vysočina	k1gFnSc1	vysočina
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
jeho	jeho	k3xOp3gInSc2	jeho
toku	tok	k1gInSc2	tok
činí	činit	k5eAaImIp3nS	činit
16,9	[number]	k4	16,9
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Plocha	plocha	k1gFnSc1	plocha
povodí	povodí	k1gNnSc2	povodí
měří	měřit	k5eAaImIp3nS	měřit
54,2	[number]	k4	54,2
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Potok	potok	k1gInSc1	potok
pramení	pramenit	k5eAaImIp3nS	pramenit
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
Lidmaně	Lidmaň	k1gFnSc2	Lidmaň
<g/>
,	,	kIx,	,
na	na	k7c6	na
západním	západní	k2eAgNnSc6d1	západní
úbočí	úbočí	k1gNnSc6	úbočí
vrchu	vrch	k1gInSc2	vrch
Bohutín	Bohutína	k1gFnPc2	Bohutína
(	(	kIx(	(
<g/>
710	[number]	k4	710
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
okolo	okolo	k7c2	okolo
680	[number]	k4	680
m.	m.	k?	m.
Teče	téct	k5eAaImIp3nS	téct
převážně	převážně	k6eAd1	převážně
severovýchodním	severovýchodní	k2eAgInSc7d1	severovýchodní
směrem	směr	k1gInSc7	směr
<g/>
.	.	kIx.	.
</s>
<s>
Protéká	protékat	k5eAaImIp3nS	protékat
obcemi	obec	k1gFnPc7	obec
Lidmaň	Lidmaň	k1gFnSc1	Lidmaň
<g/>
,	,	kIx,	,
Moraveč	Moraveč	k1gInSc1	Moraveč
a	a	k8xC	a
Novou	nový	k2eAgFnSc7d1	nová
Cerekví	Cerekev	k1gFnSc7	Cerekev
<g/>
.	.	kIx.	.
</s>
<s>
Ústí	ústí	k1gNnSc1	ústí
zleva	zleva	k6eAd1	zleva
do	do	k7c2	do
Hejlovky	Hejlovka	k1gFnSc2	Hejlovka
(	(	kIx(	(
<g/>
Želivky	Želivka	k1gFnSc2	Želivka
<g/>
)	)	kIx)	)
na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
89,4	[number]	k4	89,4
říčním	říční	k2eAgInSc6d1	říční
kilometru	kilometr	k1gInSc6	kilometr
nedaleko	nedaleko	k7c2	nedaleko
vsi	ves	k1gFnSc2	ves
Vlásenice	Vlásenice	k1gFnSc2	Vlásenice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
zhruba	zhruba	k6eAd1	zhruba
5	[number]	k4	5
km	km	kA	km
západně	západně	k6eAd1	západně
od	od	k7c2	od
Pelhřimova	Pelhřimov	k1gInSc2	Pelhřimov
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgInSc7d3	veliký
přítokem	přítok	k1gInSc7	přítok
Cerekvického	Cerekvický	k2eAgInSc2d1	Cerekvický
potoka	potok	k1gInSc2	potok
je	být	k5eAaImIp3nS	být
potok	potok	k1gInSc1	potok
Brůdek	brůdek	k1gInSc1	brůdek
(	(	kIx(	(
<g/>
hčp	hčp	k?	hčp
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
)	)	kIx)	)
s	s	k7c7	s
plochou	plocha	k1gFnSc7	plocha
povodí	povodí	k1gNnSc2	povodí
11,4	[number]	k4	11,4
km2	km2	k4	km2
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
jej	on	k3xPp3gMnSc4	on
posiluje	posilovat	k5eAaImIp3nS	posilovat
zprava	zprava	k6eAd1	zprava
nad	nad	k7c7	nad
Novou	nový	k2eAgFnSc7d1	nová
Cerekví	Cerekev	k1gFnSc7	Cerekev
na	na	k7c6	na
6,5	[number]	k4	6,5
říčním	říční	k2eAgInSc6d1	říční
kilometru	kilometr	k1gInSc6	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
jeho	jeho	k3xOp3gInSc2	jeho
toku	tok	k1gInSc2	tok
činí	činit	k5eAaImIp3nS	činit
6,1	[number]	k4	6,1
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
průtok	průtok	k1gInSc1	průtok
Cerekvického	Cerekvický	k2eAgInSc2d1	Cerekvický
potoka	potok	k1gInSc2	potok
u	u	k7c2	u
ústí	ústí	k1gNnSc2	ústí
činí	činit	k5eAaImIp3nS	činit
0,35	[number]	k4	0,35
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
M-denní	Menní	k2eAgInPc4d1	M-denní
průtoky	průtok	k1gInPc4	průtok
u	u	k7c2	u
ústí	ústí	k1gNnSc2	ústí
<g/>
:	:	kIx,	:
</s>
