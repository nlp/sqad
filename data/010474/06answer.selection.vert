<s>
Pojem	pojem	k1gInSc1	pojem
"	"	kIx"	"
<g/>
unschooling	unschooling	k1gInSc1	unschooling
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
někdy	někdy	k6eAd1	někdy
překládaný	překládaný	k2eAgInSc4d1	překládaný
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
odškolení	odškolení	k1gNnSc1	odškolení
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
použil	použít	k5eAaPmAgMnS	použít
americký	americký	k2eAgMnSc1d1	americký
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
pedagog	pedagog	k1gMnSc1	pedagog
<g/>
,	,	kIx,	,
zastánce	zastánce	k1gMnSc1	zastánce
domácího	domácí	k2eAgNnSc2d1	domácí
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
a	a	k8xC	a
průkopník	průkopník	k1gMnSc1	průkopník
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
teorie	teorie	k1gFnSc1	teorie
práv	právo	k1gNnPc2	právo
dítěte	dítě	k1gNnSc2	dítě
John	John	k1gMnSc1	John
Holt	Holt	k?	Holt
</s>
