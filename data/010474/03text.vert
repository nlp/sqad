<p>
<s>
Unschooling	Unschooling	k1gInSc1	Unschooling
je	být	k5eAaImIp3nS	být
vzdělávací	vzdělávací	k2eAgFnSc1d1	vzdělávací
metoda	metoda	k1gFnSc1	metoda
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
na	na	k7c6	na
zásadě	zásada	k1gFnSc6	zásada
<g/>
,	,	kIx,	,
že	že	k8xS	že
sám	sám	k3xTgMnSc1	sám
vzdělávaný	vzdělávaný	k2eAgMnSc1d1	vzdělávaný
si	se	k3xPyFc3	se
určuje	určovat	k5eAaImIp3nS	určovat
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
a	a	k8xC	a
jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
naučí	naučit	k5eAaPmIp3nS	naučit
<g/>
.	.	kIx.	.
</s>
<s>
Metoda	metoda	k1gFnSc1	metoda
unschoolingu	unschooling	k1gInSc2	unschooling
se	se	k3xPyFc4	se
diametrálně	diametrálně	k6eAd1	diametrálně
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
klasického	klasický	k2eAgInSc2d1	klasický
školského	školský	k2eAgInSc2d1	školský
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
vycházejícího	vycházející	k2eAgMnSc2d1	vycházející
stále	stále	k6eAd1	stále
z	z	k7c2	z
modelu	model	k1gInSc2	model
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
"	"	kIx"	"
<g/>
produktem	produkt	k1gInSc7	produkt
<g/>
"	"	kIx"	"
vzdělání	vzdělání	k1gNnSc1	vzdělání
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
gramotný	gramotný	k2eAgMnSc1d1	gramotný
voják	voják	k1gMnSc1	voják
a	a	k8xC	a
poslušný	poslušný	k2eAgMnSc1d1	poslušný
poddaný	poddaný	k1gMnSc1	poddaný
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
běžné	běžný	k2eAgFnSc6d1	běžná
škole	škola	k1gFnSc6	škola
"	"	kIx"	"
<g/>
autorita	autorita	k1gFnSc1	autorita
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ať	ať	k8xC	ať
už	už	k6eAd1	už
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
učitele	učitel	k1gMnSc2	učitel
<g/>
,	,	kIx,	,
legislativy	legislativa	k1gFnSc2	legislativa
<g/>
,	,	kIx,	,
či	či	k8xC	či
systému	systém	k1gInSc2	systém
výuky	výuka	k1gFnSc2	výuka
<g/>
)	)	kIx)	)
povinně	povinně	k6eAd1	povinně
předepisuje	předepisovat	k5eAaImIp3nS	předepisovat
obsah	obsah	k1gInSc4	obsah
a	a	k8xC	a
rozsah	rozsah	k1gInSc4	rozsah
znalostí	znalost	k1gFnPc2	znalost
a	a	k8xC	a
dovedností	dovednost	k1gFnPc2	dovednost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
musí	muset	k5eAaImIp3nS	muset
vzdělávaný	vzdělávaný	k2eAgMnSc1d1	vzdělávaný
zvládnout	zvládnout	k5eAaPmF	zvládnout
a	a	k8xC	a
hodnotí	hodnotit	k5eAaImIp3nS	hodnotit
jej	on	k3xPp3gNnSc4	on
(	(	kIx(	(
<g/>
zpravidla	zpravidla	k6eAd1	zpravidla
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
jinými	jiný	k2eAgFnPc7d1	jiná
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
unschooling	unschooling	k1gInSc1	unschooling
je	být	k5eAaImIp3nS	být
založený	založený	k2eAgInSc1d1	založený
na	na	k7c6	na
iniciativě	iniciativa	k1gFnSc6	iniciativa
vzdělávaného	vzdělávaný	k2eAgInSc2d1	vzdělávaný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
unschoolingu	unschooling	k1gInSc2	unschooling
tedy	tedy	k9	tedy
neexistují	existovat	k5eNaImIp3nP	existovat
předem	předem	k6eAd1	předem
vymyšlené	vymyšlený	k2eAgFnPc1d1	vymyšlená
osnovy	osnova	k1gFnPc1	osnova
–	–	k?	–
učení	učení	k1gNnSc1	učení
probíhá	probíhat	k5eAaImIp3nS	probíhat
samovolně	samovolně	k6eAd1	samovolně
např.	např.	kA	např.
hraním	hranit	k5eAaImIp1nS	hranit
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
diskutováním	diskutování	k1gNnSc7	diskutování
a	a	k8xC	a
zkoumáním	zkoumání	k1gNnSc7	zkoumání
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
vzdělávací	vzdělávací	k2eAgInSc1d1	vzdělávací
směr	směr	k1gInSc1	směr
stojí	stát	k5eAaImIp3nS	stát
vůči	vůči	k7c3	vůči
klasickému	klasický	k2eAgNnSc3d1	klasické
školství	školství	k1gNnSc3	školství
v	v	k7c6	v
opozici	opozice	k1gFnSc6	opozice
a	a	k8xC	a
odmítá	odmítat	k5eAaImIp3nS	odmítat
jeho	jeho	k3xOp3gFnPc4	jeho
metody	metoda	k1gFnPc4	metoda
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
známkování	známkování	k1gNnSc1	známkování
<g/>
,	,	kIx,	,
věkové	věkový	k2eAgNnSc1d1	věkové
rozdělení	rozdělení	k1gNnSc1	rozdělení
do	do	k7c2	do
tříd	třída	k1gFnPc2	třída
a	a	k8xC	a
studijní	studijní	k2eAgInPc4d1	studijní
plány	plán	k1gInPc4	plán
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
klasickému	klasický	k2eAgInSc3d1	klasický
modelu	model	k1gInSc3	model
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
přechod	přechod	k1gInSc4	přechod
z	z	k7c2	z
prostředí	prostředí	k1gNnSc2	prostředí
vytvořeného	vytvořený	k2eAgInSc2d1	vytvořený
dospělými	dospělý	k2eAgMnPc7d1	dospělý
do	do	k7c2	do
prostředí	prostředí	k1gNnSc2	prostředí
tvořeného	tvořený	k2eAgInSc2d1	tvořený
dětmi	dítě	k1gFnPc7	dítě
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
studenty	student	k1gMnPc4	student
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Unschooling	Unschooling	k1gInSc1	Unschooling
je	být	k5eAaImIp3nS	být
založený	založený	k2eAgInSc1d1	založený
na	na	k7c6	na
poznatku	poznatek	k1gInSc6	poznatek
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejlépe	dobře	k6eAd3	dobře
a	a	k8xC	a
nejpřirozeněji	přirozeně	k6eAd3	přirozeně
se	se	k3xPyFc4	se
člověk	člověk	k1gMnSc1	člověk
učí	učit	k5eAaImIp3nS	učit
věci	věc	k1gFnPc4	věc
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
užitečné	užitečný	k2eAgInPc4d1	užitečný
a	a	k8xC	a
potřebné	potřebný	k2eAgInPc4d1	potřebný
<g/>
,	,	kIx,	,
zavrhuje	zavrhovat	k5eAaImIp3nS	zavrhovat
tresty	trest	k1gInPc4	trest
<g/>
,	,	kIx,	,
odměny	odměna	k1gFnPc4	odměna
<g/>
,	,	kIx,	,
známkování	známkování	k1gNnSc1	známkování
atd.	atd.	kA	atd.
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
[	[	kIx(	[
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
Pro	pro	k7c4	pro
unschooling	unschooling	k1gInSc4	unschooling
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
důležité	důležitý	k2eAgNnSc1d1	důležité
co	co	k8xS	co
nejpodnětnější	podnětný	k2eAgNnSc1d3	nejpodnětnější
prostředí	prostředí	k1gNnSc1	prostředí
–	–	k?	–
materiální	materiální	k2eAgMnSc1d1	materiální
i	i	k8xC	i
personální	personální	k2eAgMnSc1d1	personální
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Pojem	pojem	k1gInSc1	pojem
"	"	kIx"	"
<g/>
unschooling	unschooling	k1gInSc1	unschooling
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
někdy	někdy	k6eAd1	někdy
překládaný	překládaný	k2eAgInSc4d1	překládaný
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
odškolení	odškolení	k1gNnSc1	odškolení
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
použil	použít	k5eAaPmAgMnS	použít
americký	americký	k2eAgMnSc1d1	americký
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
pedagog	pedagog	k1gMnSc1	pedagog
<g/>
,	,	kIx,	,
zastánce	zastánce	k1gMnSc1	zastánce
domácího	domácí	k2eAgNnSc2d1	domácí
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
a	a	k8xC	a
průkopník	průkopník	k1gMnSc1	průkopník
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
teorie	teorie	k1gFnSc1	teorie
práv	právo	k1gNnPc2	právo
dítěte	dítě	k1gNnSc2	dítě
John	John	k1gMnSc1	John
Holt	Holt	k?	Holt
</s>
<s>
Holt	Holt	k?	Holt
mnoho	mnoho	k4c4	mnoho
let	léto	k1gNnPc2	léto
bojoval	bojovat	k5eAaImAgInS	bojovat
za	za	k7c4	za
reformu	reforma	k1gFnSc4	reforma
školství	školství	k1gNnSc2	školství
<g/>
.	.	kIx.	.
</s>
<s>
Doufal	doufat	k5eAaImAgMnS	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podaří	podařit	k5eAaPmIp3nS	podařit
iniciovat	iniciovat	k5eAaBmF	iniciovat
hluboké	hluboký	k2eAgNnSc1d1	hluboké
přehodnocení	přehodnocení	k1gNnSc1	přehodnocení
školského	školský	k2eAgInSc2d1	školský
systému	systém	k1gInSc2	systém
a	a	k8xC	a
chtěl	chtít	k5eAaImAgMnS	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
školy	škola	k1gFnPc1	škola
staly	stát	k5eAaPmAgFnP	stát
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
přátelštějšími	přátelský	k2eAgNnPc7d2	přátelštější
místy	místo	k1gNnPc7	místo
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
plynul	plynout	k5eAaImAgMnS	plynout
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
dospěl	dochvít	k5eAaPmAgMnS	dochvít
k	k	k7c3	k
přesvědčení	přesvědčení	k1gNnSc3	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
školy	škola	k1gFnPc1	škola
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
požadavkům	požadavek	k1gInPc3	požadavek
většinové	většinový	k2eAgFnSc2d1	většinová
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
k	k	k7c3	k
výraznější	výrazný	k2eAgFnSc3d2	výraznější
reformě	reforma	k1gFnSc3	reforma
školství	školství	k1gNnSc4	školství
za	za	k7c2	za
jeho	on	k3xPp3gInSc2	on
života	život	k1gInSc2	život
nedojde	dojít	k5eNaPmIp3nS	dojít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
Holt	Holt	k?	Holt
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nepovažuji	považovat	k5eNaImIp1nS	považovat
domácí	domácí	k2eAgFnSc4d1	domácí
výuku	výuka	k1gFnSc4	výuka
za	za	k7c4	za
odpověď	odpověď	k1gFnSc4	odpověď
na	na	k7c4	na
špatné	špatný	k2eAgFnPc4d1	špatná
školy	škola	k1gFnPc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Myslím	myslet	k5eAaImIp1nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
domov	domov	k1gInSc1	domov
je	být	k5eAaImIp3nS	být
vhodnou	vhodný	k2eAgFnSc7d1	vhodná
základnou	základna	k1gFnSc7	základna
pro	pro	k7c4	pro
zkoumání	zkoumání	k1gNnSc4	zkoumání
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
kterému	který	k3yQgNnSc3	který
říkáme	říkat	k5eAaImIp1nP	říkat
učení	učení	k1gNnSc4	učení
a	a	k8xC	a
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
<g/>
.	.	kIx.	.
</s>
<s>
Domov	domov	k1gInSc1	domov
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
nejlepším	dobrý	k2eAgNnSc7d3	nejlepší
místem	místo	k1gNnSc7	místo
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
dobré	dobrý	k2eAgInPc1d1	dobrý
by	by	kYmCp3nP	by
školy	škola	k1gFnPc1	škola
byly	být	k5eAaImAgFnP	být
<g/>
.	.	kIx.	.
<g/>
Správný	správný	k2eAgInSc4d1	správný
vztah	vztah	k1gInSc4	vztah
škol	škola	k1gFnPc2	škola
k	k	k7c3	k
domovu	domov	k1gInSc3	domov
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
vztah	vztah	k1gInSc1	vztah
mezi	mezi	k7c7	mezi
knihovnou	knihovna	k1gFnSc7	knihovna
a	a	k8xC	a
domovem	domov	k1gInSc7	domov
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
kluzištěm	kluziště	k1gNnSc7	kluziště
a	a	k8xC	a
domovem	domov	k1gInSc7	domov
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
doplňkový	doplňkový	k2eAgInSc1d1	doplňkový
zdroj	zdroj	k1gInSc1	zdroj
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Dospěl	dochvít	k5eAaPmAgMnS	dochvít
k	k	k7c3	k
přesvědčení	přesvědčení	k1gNnSc3	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
školství	školství	k1gNnSc1	školství
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
reformovat	reformovat	k5eAaBmF	reformovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
základu	základ	k1gInSc2	základ
špatné	špatný	k2eAgInPc4d1	špatný
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
propagátorem	propagátor	k1gMnSc7	propagátor
domácího	domácí	k2eAgNnSc2d1	domácí
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
by	by	kYmCp3nS	by
podle	podle	k7c2	podle
něj	on	k3xPp3gNnSc2	on
ovšem	ovšem	k9	ovšem
zbytečné	zbytečný	k2eAgNnSc1d1	zbytečné
pouze	pouze	k6eAd1	pouze
vzít	vzít	k5eAaPmF	vzít
děti	dítě	k1gFnPc4	dítě
ze	z	k7c2	z
školy	škola	k1gFnSc2	škola
a	a	k8xC	a
vytvořit	vytvořit	k5eAaPmF	vytvořit
doma	doma	k6eAd1	doma
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
škole	škola	k1gFnSc3	škola
podobné	podobný	k2eAgFnSc3d1	podobná
<g/>
.	.	kIx.	.
</s>
<s>
Věřil	věřit	k5eAaImAgMnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
děti	dítě	k1gFnPc4	dítě
obklopené	obklopený	k2eAgFnPc4d1	obklopená
různorodým	různorodý	k2eAgNnSc7d1	různorodé
a	a	k8xC	a
stimulujícím	stimulující	k2eAgNnSc7d1	stimulující
prostředím	prostředí	k1gNnSc7	prostředí
se	se	k3xPyFc4	se
naučí	naučit	k5eAaPmIp3nS	naučit
vše	všechen	k3xTgNnSc1	všechen
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
jsou	být	k5eAaImIp3nP	být
připraveny	připraven	k2eAgFnPc1d1	připravena
se	se	k3xPyFc4	se
naučit	naučit	k5eAaPmF	naučit
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jsou	být	k5eAaImIp3nP	být
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
připraveny	připravit	k5eAaPmNgInP	připravit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Holt	Holt	k?	Holt
věřil	věřit	k5eAaImAgMnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
děti	dítě	k1gFnPc4	dítě
do	do	k7c2	do
učení	učení	k1gNnSc2	učení
nutit	nutit	k5eAaImF	nutit
–	–	k?	–
samy	sám	k3xTgInPc1	sám
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
přirozeně	přirozeně	k6eAd1	přirozeně
dospějí	dochvít	k5eAaPmIp3nP	dochvít
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
jim	on	k3xPp3gInPc3	on
poskytnuta	poskytnout	k5eAaPmNgFnS	poskytnout
svoboda	svoboda	k1gFnSc1	svoboda
nechat	nechat	k5eAaPmF	nechat
se	se	k3xPyFc4	se
vést	vést	k5eAaImF	vést
vlastními	vlastní	k2eAgInPc7d1	vlastní
zájmy	zájem	k1gInPc7	zájem
<g/>
,	,	kIx,	,
a	a	k8xC	a
pokud	pokud	k8xS	pokud
mají	mít	k5eAaImIp3nP	mít
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
širokou	široký	k2eAgFnSc4d1	široká
škálu	škála	k1gFnSc4	škála
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
učit	učit	k5eAaImF	učit
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
filozofii	filozofie	k1gFnSc4	filozofie
Holt	Holt	k?	Holt
nazval	nazvat	k5eAaBmAgInS	nazvat
unschoolingem	unschooling	k1gInSc7	unschooling
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozdělení	rozdělení	k1gNnSc1	rozdělení
==	==	k?	==
</s>
</p>
<p>
<s>
Unschooling	Unschooling	k1gInSc4	Unschooling
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
dělit	dělit	k5eAaImF	dělit
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
větví	větev	k1gFnPc2	větev
<g/>
:	:	kIx,	:
domácí	domácí	k2eAgInSc4d1	domácí
a	a	k8xC	a
školní	školní	k2eAgInSc4d1	školní
nebo	nebo	k8xC	nebo
vhodněji	vhodně	k6eAd2	vhodně
neformální	formální	k2eNgFnPc1d1	neformální
a	a	k8xC	a
institucionalizované	institucionalizovaný	k2eAgFnPc1d1	institucionalizovaná
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
větve	větev	k1gFnPc1	větev
sdílejí	sdílet	k5eAaImIp3nP	sdílet
stejný	stejný	k2eAgInSc4d1	stejný
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
učení	učení	k1gNnSc3	učení
a	a	k8xC	a
rozcházejí	rozcházet	k5eAaImIp3nP	rozcházet
se	se	k3xPyFc4	se
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
unschooling	unschooling	k1gInSc4	unschooling
aplikován	aplikován	k2eAgInSc4d1	aplikován
formou	forma	k1gFnSc7	forma
domácího	domácí	k2eAgNnSc2d1	domácí
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
nebo	nebo	k8xC	nebo
zda	zda	k8xS	zda
dítě	dítě	k1gNnSc1	dítě
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
student	student	k1gMnSc1	student
navštěvuje	navštěvovat	k5eAaImIp3nS	navštěvovat
vzdělávací	vzdělávací	k2eAgNnSc4d1	vzdělávací
centrum	centrum	k1gNnSc4	centrum
či	či	k8xC	či
školu	škola	k1gFnSc4	škola
zastávající	zastávající	k2eAgFnSc2d1	zastávající
hodnoty	hodnota	k1gFnSc2	hodnota
unschoolingu	unschooling	k1gInSc2	unschooling
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
významné	významný	k2eAgFnPc1d1	významná
školy	škola	k1gFnPc1	škola
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
jsou	být	k5eAaImIp3nP	být
anglická	anglický	k2eAgFnSc1d1	anglická
Summerhill	Summerhill	k1gMnSc1	Summerhill
School	School	k1gInSc1	School
a	a	k8xC	a
americká	americký	k2eAgFnSc1d1	americká
Sudbury	Sudbura	k1gFnPc1	Sudbura
Valley	Vallea	k1gFnSc2	Vallea
School	Schoola	k1gFnPc2	Schoola
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Principy	princip	k1gInPc1	princip
==	==	k?	==
</s>
</p>
<p>
<s>
lidé	člověk	k1gMnPc1	člověk
(	(	kIx(	(
<g/>
a	a	k8xC	a
děti	dítě	k1gFnPc1	dítě
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
lidé	člověk	k1gMnPc1	člověk
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
přírody	příroda	k1gFnSc2	příroda
zvídavé	zvídavý	k2eAgFnSc2d1	zvídavá
a	a	k8xC	a
učenlivé	učenlivý	k2eAgFnSc2d1	učenlivá
bytosti	bytost	k1gFnSc2	bytost
<g/>
;	;	kIx,	;
učení	učení	k1gNnSc1	učení
probíhá	probíhat	k5eAaImIp3nS	probíhat
nepřetržitě	přetržitě	k6eNd1	přetržitě
a	a	k8xC	a
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
chvíli	chvíle	k1gFnSc6	chvíle
lidského	lidský	k2eAgInSc2d1	lidský
života	život	k1gInSc2	život
</s>
</p>
<p>
<s>
učení	učení	k1gNnSc1	učení
probíhá	probíhat	k5eAaImIp3nS	probíhat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
motivace	motivace	k1gFnSc2	motivace
člověka	člověk	k1gMnSc2	člověk
(	(	kIx(	(
<g/>
dítěte	dítě	k1gNnSc2	dítě
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
mu	on	k3xPp3gMnSc3	on
umožněno	umožněn	k2eAgNnSc1d1	umožněno
následovat	následovat	k5eAaImF	následovat
své	svůj	k3xOyFgInPc4	svůj
zájmy	zájem	k1gInPc4	zájem
a	a	k8xC	a
touhy	touha	k1gFnPc4	touha
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
<g/>
-li	i	k?	-li
omezován	omezovat	k5eAaImNgInS	omezovat
příkazy	příkaz	k1gInPc7	příkaz
a	a	k8xC	a
zákazy	zákaz	k1gInPc7	zákaz
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
<g/>
-li	i	k?	-li
nucen	nutit	k5eAaImNgMnS	nutit
učit	učit	k5eAaImF	učit
se	se	k3xPyFc4	se
znalosti	znalost	k1gFnPc4	znalost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mu	on	k3xPp3gInSc3	on
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
chvíli	chvíle	k1gFnSc6	chvíle
nedávají	dávat	k5eNaImIp3nP	dávat
smysl	smysl	k1gInSc4	smysl
</s>
</p>
<p>
<s>
učení	učení	k1gNnSc1	učení
probíhá	probíhat	k5eAaImIp3nS	probíhat
často	často	k6eAd1	často
nevědomě	vědomě	k6eNd1	vědomě
nebo	nebo	k8xC	nebo
spontánně	spontánně	k6eAd1	spontánně
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
vzniklé	vzniklý	k2eAgFnSc6d1	vzniklá
potřebě	potřeba	k1gFnSc6	potřeba
se	se	k3xPyFc4	se
něčemu	něco	k3yInSc3	něco
naučit	naučit	k5eAaPmF	naučit
<g/>
;	;	kIx,	;
nejprve	nejprve	k6eAd1	nejprve
je	být	k5eAaImIp3nS	být
život	život	k1gInSc4	život
a	a	k8xC	a
poté	poté	k6eAd1	poté
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
potřebné	potřebný	k2eAgInPc1d1	potřebný
nástroje	nástroj	k1gInPc1	nástroj
</s>
</p>
<p>
<s>
unschooling	unschooling	k1gInSc1	unschooling
po	po	k7c6	po
rodičích	rodič	k1gMnPc6	rodič
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
velkou	velká	k1gFnSc4	velká
důvěru	důvěra	k1gFnSc4	důvěra
v	v	k7c4	v
dítě	dítě	k1gNnSc4	dítě
<g/>
,	,	kIx,	,
v	v	k7c4	v
jeho	jeho	k3xOp3gFnSc4	jeho
schopnost	schopnost	k1gFnSc4	schopnost
naučit	naučit	k5eAaPmF	naučit
se	se	k3xPyFc4	se
vše	všechen	k3xTgNnSc1	všechen
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
chvíli	chvíle	k1gFnSc6	chvíle
připadá	připadat	k5eAaImIp3nS	připadat
smysluplné	smysluplný	k2eAgNnSc1d1	smysluplné
a	a	k8xC	a
vhodné	vhodný	k2eAgNnSc1d1	vhodné
<g/>
;	;	kIx,	;
naučilo	naučit	k5eAaPmAgNnS	naučit
se	se	k3xPyFc4	se
samo	sám	k3xTgNnSc1	sám
a	a	k8xC	a
bez	bez	k7c2	bez
učebnic	učebnice	k1gFnPc2	učebnice
chodit	chodit	k5eAaImF	chodit
<g/>
,	,	kIx,	,
mluvit	mluvit	k5eAaImF	mluvit
<g/>
,	,	kIx,	,
lézt	lézt	k5eAaImF	lézt
po	po	k7c6	po
prolézačkách	prolézačka	k1gFnPc6	prolézačka
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
proč	proč	k6eAd1	proč
by	by	kYmCp3nS	by
tento	tento	k3xDgInSc1	tento
"	"	kIx"	"
<g/>
učící	učící	k2eAgInSc1d1	učící
<g/>
"	"	kIx"	"
proces	proces	k1gInSc1	proces
měl	mít	k5eAaImAgInS	mít
skončit	skončit	k5eAaPmF	skončit
v	v	k7c6	v
šesti	šest	k4xCc6	šest
letech	léto	k1gNnPc6	léto
věku	věk	k1gInSc2	věk
dítěte	dítě	k1gNnSc2	dítě
<g/>
?	?	kIx.	?
</s>
</p>
<p>
<s>
==	==	k?	==
Přirozenost	přirozenost	k1gFnSc4	přirozenost
a	a	k8xC	a
rozmanitost	rozmanitost	k1gFnSc4	rozmanitost
==	==	k?	==
</s>
</p>
<p>
<s>
Unschooling	Unschooling	k1gInSc1	Unschooling
je	být	k5eAaImIp3nS	být
také	také	k9	také
znám	znám	k2eAgMnSc1d1	znám
jako	jako	k8xS	jako
proces	proces	k1gInSc1	proces
učení	učení	k1gNnSc2	učení
vedený	vedený	k2eAgInSc1d1	vedený
zájmy	zájem	k1gInPc4	zájem
dítěte	dítě	k1gNnSc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
aplikování	aplikování	k1gNnSc6	aplikování
této	tento	k3xDgFnSc2	tento
vzdělávací	vzdělávací	k2eAgFnSc2d1	vzdělávací
filozofie	filozofie	k1gFnSc2	filozofie
neinstitucionalizovanou	institucionalizovaný	k2eNgFnSc7d1	neinstitucionalizovaná
formou	forma	k1gFnSc7	forma
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
v	v	k7c6	v
režimu	režim	k1gInSc6	režim
domácího	domácí	k2eAgNnSc2d1	domácí
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
<g/>
)	)	kIx)	)
rodič	rodič	k1gMnSc1	rodič
s	s	k7c7	s
dítětem	dítě	k1gNnSc7	dítě
nepoužívají	používat	k5eNaImIp3nP	používat
žádné	žádný	k3yNgNnSc4	žádný
fixní	fixní	k2eAgNnSc4d1	fixní
kurikulum	kurikulum	k1gNnSc4	kurikulum
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Pata	pata	k1gFnSc1	pata
Farengy	Fareng	k1gInPc1	Fareng
(	(	kIx(	(
<g/>
spolupracovníka	spolupracovník	k1gMnSc4	spolupracovník
a	a	k8xC	a
nástupce	nástupce	k1gMnSc4	nástupce
Johna	John	k1gMnSc4	John
Holta	Holta	k1gMnSc1	Holta
<g/>
)	)	kIx)	)
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
unschooling	unschooling	k1gInSc1	unschooling
stručně	stručně	k6eAd1	stručně
definován	definovat	k5eAaBmNgInS	definovat
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
dopřát	dopřát	k5eAaPmF	dopřát
dětem	dítě	k1gFnPc3	dítě
tolik	tolik	k4yIc4	tolik
svobody	svoboda	k1gFnSc2	svoboda
v	v	k7c4	v
učení	učení	k1gNnSc4	učení
se	se	k3xPyFc4	se
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
poznávání	poznávání	k1gNnSc2	poznávání
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
kolik	kolik	k9	kolik
jejich	jejich	k3xOp3gMnPc1	jejich
rodiče	rodič	k1gMnPc1	rodič
mohou	moct	k5eAaImIp3nP	moct
pohodlně	pohodlně	k6eAd1	pohodlně
snést	snést	k5eAaPmF	snést
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Společný	společný	k2eAgInSc4d1	společný
život	život	k1gInSc4	život
a	a	k8xC	a
vzájemný	vzájemný	k2eAgInSc4d1	vzájemný
respekt	respekt	k1gInSc4	respekt
==	==	k?	==
</s>
</p>
<p>
<s>
Výhodou	výhoda	k1gFnSc7	výhoda
unschoolingu	unschooling	k1gInSc2	unschooling
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
od	od	k7c2	od
rodičů	rodič	k1gMnPc2	rodič
nevyžaduje	vyžadovat	k5eNaImIp3nS	vyžadovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
někým	někdo	k3yInSc7	někdo
jiným	jiný	k2eAgNnSc7d1	jiné
<g/>
,	,	kIx,	,
než	než	k8xS	než
jsou	být	k5eAaImIp3nP	být
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
například	například	k6eAd1	například
učitelem	učitel	k1gMnSc7	učitel
svého	svůj	k3xOyFgNnSc2	svůj
dítěte	dítě	k1gNnSc2	dítě
v	v	k7c6	v
tradičním	tradiční	k2eAgInSc6d1	tradiční
smyslu	smysl	k1gInSc6	smysl
toho	ten	k3xDgNnSc2	ten
slova	slovo	k1gNnSc2	slovo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Namísto	namísto	k7c2	namísto
toho	ten	k3xDgMnSc2	ten
rodiče	rodič	k1gMnSc2	rodič
a	a	k8xC	a
děti	dítě	k1gFnPc1	dítě
společně	společně	k6eAd1	společně
žijí	žít	k5eAaImIp3nP	žít
<g/>
,	,	kIx,	,
učí	učit	k5eAaImIp3nS	učit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
následují	následovat	k5eAaImIp3nP	následovat
své	svůj	k3xOyFgInPc4	svůj
zájmy	zájem	k1gInPc4	zájem
<g/>
,	,	kIx,	,
hledají	hledat	k5eAaImIp3nP	hledat
odpovědi	odpověď	k1gFnPc1	odpověď
na	na	k7c4	na
otázky	otázka	k1gFnPc4	otázka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
je	on	k3xPp3gNnPc4	on
vzrušují	vzrušovat	k5eAaImIp3nP	vzrušovat
a	a	k8xC	a
využívají	využívat	k5eAaImIp3nP	využívat
materiálů	materiál	k1gInPc2	materiál
konvenčního	konvenční	k2eAgNnSc2d1	konvenční
školství	školství	k1gNnSc2	školství
dle	dle	k7c2	dle
potřeby	potřeba	k1gFnSc2	potřeba
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
jakým	jaký	k3yIgInSc7	jaký
se	se	k3xPyFc4	se
všichni	všechen	k3xTgMnPc1	všechen
učíme	učit	k5eAaImIp1nP	učit
před	před	k7c7	před
vstupem	vstup	k1gInSc7	vstup
do	do	k7c2	do
školy	škola	k1gFnSc2	škola
a	a	k8xC	a
také	také	k9	také
způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
učíme	učit	k5eAaImIp1nP	učit
<g/>
,	,	kIx,	,
když	když	k8xS	když
školu	škola	k1gFnSc4	škola
opustíme	opustit	k5eAaPmIp1nP	opustit
a	a	k8xC	a
začneme	začít	k5eAaPmIp1nP	začít
chodit	chodit	k5eAaImF	chodit
do	do	k7c2	do
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Takže	takže	k9	takže
například	například	k6eAd1	například
zájem	zájem	k1gInSc4	zájem
malého	malý	k2eAgNnSc2d1	malé
dítěte	dítě	k1gNnSc2	dítě
o	o	k7c4	o
rychlá	rychlý	k2eAgNnPc4d1	rychlé
auta	auto	k1gNnPc4	auto
může	moct	k5eAaImIp3nS	moct
vyústit	vyústit	k5eAaPmF	vyústit
ve	v	k7c6	v
čtení	čtení	k1gNnSc6	čtení
knih	kniha	k1gFnPc2	kniha
o	o	k7c4	o
fungování	fungování	k1gNnSc4	fungování
motorů	motor	k1gInPc2	motor
(	(	kIx(	(
<g/>
fyzika	fyzika	k1gFnSc1	fyzika
<g/>
,	,	kIx,	,
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
ČJ	ČJ	kA	ČJ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c4	v
otázky	otázka	k1gFnPc4	otázka
jak	jak	k6eAd1	jak
a	a	k8xC	a
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
určité	určitý	k2eAgNnSc1d1	určité
auto	auto	k1gNnSc1	auto
vyrobeno	vyrobit	k5eAaPmNgNnS	vyrobit
a	a	k8xC	a
co	co	k3yQnSc1	co
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
udělat	udělat	k5eAaPmF	udělat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
ho	on	k3xPp3gMnSc4	on
člověk	člověk	k1gMnSc1	člověk
mohl	moct	k5eAaImAgMnS	moct
<g />
.	.	kIx.	.
</s>
<s>
koupit	koupit	k5eAaPmF	koupit
(	(	kIx(	(
<g/>
dějepis	dějepis	k1gInSc1	dějepis
<g/>
,	,	kIx,	,
finanční	finanční	k2eAgFnSc1d1	finanční
gramotnost	gramotnost	k1gFnSc1	gramotnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
navrhuje	navrhovat	k5eAaImIp3nS	navrhovat
podobu	podoba	k1gFnSc4	podoba
a	a	k8xC	a
tvar	tvar	k1gInSc1	tvar
aut	auto	k1gNnPc2	auto
(	(	kIx(	(
<g/>
životopis	životopis	k1gInSc1	životopis
<g/>
,	,	kIx,	,
kreslení	kreslení	k1gNnSc4	kreslení
<g/>
,	,	kIx,	,
různé	různý	k2eAgFnPc4d1	různá
profese	profes	k1gFnPc4	profes
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
Tyto	tento	k3xDgInPc4	tento
zájmy	zájem	k1gInPc4	zájem
samozřejmě	samozřejmě	k6eAd1	samozřejmě
mohou	moct	k5eAaImIp3nP	moct
vést	vést	k5eAaImF	vést
dítě	dítě	k1gNnSc4	dítě
k	k	k7c3	k
touze	touha	k1gFnSc3	touha
přečíst	přečíst	k5eAaPmF	přečíst
si	se	k3xPyFc3	se
sám	sám	k3xTgInSc4	sám
různé	různý	k2eAgInPc4d1	různý
texty	text	k1gInPc4	text
<g/>
,	,	kIx,	,
vidět	vidět	k5eAaImF	vidět
při	při	k7c6	při
práci	práce	k1gFnSc6	práce
třeba	třeba	k6eAd1	třeba
automechanika	automechanik	k1gMnSc4	automechanik
<g/>
,	,	kIx,	,
jít	jít	k5eAaImF	jít
do	do	k7c2	do
technického	technický	k2eAgNnSc2d1	technické
muzea	muzeum	k1gNnSc2	muzeum
<g/>
,	,	kIx,	,
zkusit	zkusit	k5eAaPmF	zkusit
vyrobit	vyrobit	k5eAaPmF	vyrobit
vlastní	vlastní	k2eAgNnSc4d1	vlastní
"	"	kIx"	"
<g/>
auto	auto	k1gNnSc4	auto
<g/>
"	"	kIx"	"
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
materiálů	materiál	k1gInPc2	materiál
apod.	apod.	kA	apod.
</s>
</p>
<p>
<s>
Důležitý	důležitý	k2eAgInSc1d1	důležitý
rozdíl	rozdíl	k1gInSc1	rozdíl
oproti	oproti	k7c3	oproti
klasickému	klasický	k2eAgNnSc3d1	klasické
školství	školství	k1gNnSc3	školství
je	být	k5eAaImIp3nS	být
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
všechny	všechen	k3xTgFnPc4	všechen
tyto	tento	k3xDgFnPc4	tento
aktivity	aktivita	k1gFnPc4	aktivita
si	se	k3xPyFc3	se
vybralo	vybrat	k5eAaPmAgNnS	vybrat
dítě	dítě	k1gNnSc1	dítě
samo	sám	k3xTgNnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Nedostalo	dostat	k5eNaPmAgNnS	dostat
je	on	k3xPp3gNnSc4	on
jako	jako	k9	jako
"	"	kIx"	"
<g/>
úkol	úkol	k1gInSc4	úkol
<g/>
"	"	kIx"	"
od	od	k7c2	od
někoho	někdo	k3yInSc2	někdo
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
nad	nad	k7c7	nad
ním	on	k3xPp3gInSc7	on
má	mít	k5eAaImIp3nS	mít
moc	moc	k6eAd1	moc
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
známek	známka	k1gFnPc2	známka
a	a	k8xC	a
hodnocení	hodnocení	k1gNnSc2	hodnocení
<g/>
,	,	kIx,	,
nemusí	muset	k5eNaImIp3nP	muset
je	on	k3xPp3gInPc4	on
splnit	splnit	k5eAaPmF	splnit
ve	v	k7c6	v
stresu	stres	k1gInSc6	stres
z	z	k7c2	z
nedodržení	nedodržení	k1gNnSc2	nedodržení
termínu	termín	k1gInSc2	termín
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
je	být	k5eAaImIp3nS	být
dělat	dělat	k5eAaImF	dělat
kdekoli	kdekoli	k6eAd1	kdekoli
chce	chtít	k5eAaImIp3nS	chtít
<g/>
,	,	kIx,	,
ne	ne	k9	ne
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
určeném	určený	k2eAgNnSc6d1	určené
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
s	s	k7c7	s
určenými	určený	k2eAgFnPc7d1	určená
pomůckami	pomůcka	k1gFnPc7	pomůcka
<g/>
.	.	kIx.	.
</s>
<s>
Zapojení	zapojený	k2eAgMnPc1d1	zapojený
rodiče	rodič	k1gMnPc1	rodič
do	do	k7c2	do
těchto	tento	k3xDgFnPc2	tento
aktivit	aktivita	k1gFnPc2	aktivita
je	být	k5eAaImIp3nS	být
plně	plně	k6eAd1	plně
na	na	k7c4	na
vůli	vůle	k1gFnSc4	vůle
dítěte	dítě	k1gNnSc2	dítě
(	(	kIx(	(
<g/>
a	a	k8xC	a
samozřejmě	samozřejmě	k6eAd1	samozřejmě
také	také	k9	také
na	na	k7c6	na
možnostech	možnost	k1gFnPc6	možnost
a	a	k8xC	a
vůli	vůle	k1gFnSc4	vůle
rodiče	rodič	k1gMnSc2	rodič
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Typický	typický	k2eAgInSc4d1	typický
den	den	k1gInSc4	den
==	==	k?	==
</s>
</p>
<p>
<s>
Typický	typický	k2eAgInSc4d1	typický
den	den	k1gInSc4	den
v	v	k7c6	v
unschoolingu	unschooling	k1gInSc6	unschooling
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Respektive	respektive	k9	respektive
může	moct	k5eAaImIp3nS	moct
existovat	existovat	k5eAaImF	existovat
v	v	k7c6	v
jakékoli	jakýkoli	k3yIgFnSc6	jakýkoli
podobě	podoba	k1gFnSc6	podoba
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
si	se	k3xPyFc3	se
to	ten	k3xDgNnSc4	ten
dítě	dítě	k1gNnSc4	dítě
i	i	k8xC	i
jeho	jeho	k3xOp3gMnPc1	jeho
rodiče	rodič	k1gMnPc1	rodič
přejí	přát	k5eAaImIp3nP	přát
<g/>
.	.	kIx.	.
</s>
<s>
Unschooling	Unschooling	k1gInSc1	Unschooling
je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
svobodě	svoboda	k1gFnSc6	svoboda
<g/>
,	,	kIx,	,
vzájemném	vzájemný	k2eAgInSc6d1	vzájemný
respektu	respekt	k1gInSc6	respekt
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
respektu	respekt	k1gInSc2	respekt
rodiče	rodič	k1gMnPc4	rodič
k	k	k7c3	k
dítěti	dítě	k1gNnSc3	dítě
i	i	k9	i
naopak	naopak	k6eAd1	naopak
<g/>
)	)	kIx)	)
a	a	k8xC	a
dohodě	dohoda	k1gFnSc3	dohoda
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
děti	dítě	k1gFnPc1	dítě
navštěvují	navštěvovat	k5eAaImIp3nP	navštěvovat
nejrůznější	různý	k2eAgFnPc1d3	nejrůznější
instituce	instituce	k1gFnPc1	instituce
nebo	nebo	k8xC	nebo
kroužky	kroužek	k1gInPc1	kroužek
podle	podle	k7c2	podle
svého	svůj	k3xOyFgInSc2	svůj
výběru	výběr	k1gInSc2	výběr
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgNnSc4d1	jiné
se	se	k3xPyFc4	se
učí	učit	k5eAaImIp3nS	učit
každodenním	každodenní	k2eAgInSc7d1	každodenní
životem	život	k1gInSc7	život
a	a	k8xC	a
hrou	hra	k1gFnSc7	hra
s	s	k7c7	s
mladšími	mladý	k2eAgFnPc7d2	mladší
či	či	k8xC	či
staršími	starý	k2eAgFnPc7d2	starší
dětmi	dítě	k1gFnPc7	dítě
<g/>
,	,	kIx,	,
rodiči	rodič	k1gMnPc7	rodič
<g/>
,	,	kIx,	,
příbuznými	příbuzný	k1gMnPc7	příbuzný
<g/>
,	,	kIx,	,
rodinnými	rodinný	k2eAgMnPc7d1	rodinný
známými	známý	k1gMnPc7	známý
a	a	k8xC	a
lidmi	člověk	k1gMnPc7	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
je	on	k3xPp3gFnPc4	on
obklopují	obklopovat	k5eAaImIp3nP	obklopovat
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
děti	dítě	k1gFnPc1	dítě
jsou	být	k5eAaImIp3nP	být
rády	rád	k2eAgFnPc1d1	ráda
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgInPc4d1	jiný
si	se	k3xPyFc3	se
raději	rád	k6eAd2	rád
někam	někam	k6eAd1	někam
zalezou	zalézt	k5eAaPmIp3nP	zalézt
s	s	k7c7	s
knížkou	knížka	k1gFnSc7	knížka
nebo	nebo	k8xC	nebo
počítačem	počítač	k1gInSc7	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
děti	dítě	k1gFnPc1	dítě
či	či	k8xC	či
rodiny	rodina	k1gFnPc1	rodina
spíše	spíše	k9	spíše
samotářské	samotářský	k2eAgFnPc1d1	samotářská
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgFnPc1d1	jiná
jsou	být	k5eAaImIp3nP	být
zase	zase	k9	zase
společenské	společenský	k2eAgFnPc1d1	společenská
a	a	k8xC	a
vyhledávají	vyhledávat	k5eAaImIp3nP	vyhledávat
nejrůznější	různý	k2eAgInPc4d3	nejrůznější
typy	typ	k1gInPc4	typ
akcí	akce	k1gFnPc2	akce
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
vzdělávacích	vzdělávací	k2eAgNnPc2d1	vzdělávací
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
rodiny	rodina	k1gFnPc1	rodina
hodně	hodně	k6eAd1	hodně
cestují	cestovat	k5eAaImIp3nP	cestovat
<g/>
,	,	kIx,	,
potkávají	potkávat	k5eAaImIp3nP	potkávat
se	se	k3xPyFc4	se
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
zajímavými	zajímavý	k2eAgMnPc7d1	zajímavý
lidmi	člověk	k1gMnPc7	člověk
a	a	k8xC	a
prožívají	prožívat	k5eAaImIp3nP	prožívat
spoustu	spousta	k1gFnSc4	spousta
dobrodružství	dobrodružství	k1gNnSc2	dobrodružství
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
děti	dítě	k1gFnPc1	dítě
rády	rád	k2eAgFnPc1d1	ráda
chodí	chodit	k5eAaImIp3nP	chodit
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
rodiči	rodič	k1gMnPc7	rodič
do	do	k7c2	do
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgFnPc4d1	jiná
rády	rád	k2eAgFnPc4d1	ráda
tráví	trávit	k5eAaImIp3nS	trávit
čas	čas	k1gInSc1	čas
s	s	k7c7	s
prarodiči	prarodič	k1gMnPc7	prarodič
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
unschoolingový	unschoolingový	k2eAgInSc1d1	unschoolingový
život	život	k1gInSc1	život
směsí	směs	k1gFnPc2	směs
všeho	všecek	k3xTgNnSc2	všecek
výše	vysoce	k6eAd2	vysoce
uvedeného	uvedený	k2eAgNnSc2d1	uvedené
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
rodina	rodina	k1gFnSc1	rodina
je	být	k5eAaImIp3nS	být
jiná	jiný	k2eAgFnSc1d1	jiná
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
samotné	samotný	k2eAgFnSc6d1	samotná
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
své	svůj	k3xOyFgInPc4	svůj
dny	den	k1gInPc4	den
prožívá	prožívat	k5eAaImIp3nS	prožívat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Situace	situace	k1gFnSc1	situace
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
==	==	k?	==
</s>
</p>
<p>
<s>
Unschooling	Unschooling	k1gInSc1	Unschooling
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc3	jeho
různé	různý	k2eAgFnPc4d1	různá
formy	forma	k1gFnPc4	forma
–	–	k?	–
od	od	k7c2	od
"	"	kIx"	"
<g/>
holtovského	holtovský	k2eAgNnSc2d1	holtovský
<g/>
"	"	kIx"	"
<g/>
[	[	kIx(	[
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
unschoolingu	unschooling	k1gInSc2	unschooling
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
rodiče	rodič	k1gMnPc1	rodič
nechávají	nechávat	k5eAaImIp3nP	nechávat
vést	vést	k5eAaImF	vést
zájmy	zájem	k1gInPc4	zájem
dítěte	dítě	k1gNnSc2	dítě
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
mu	on	k3xPp3gMnSc3	on
nabízet	nabízet	k5eAaImF	nabízet
různé	různý	k2eAgFnPc4d1	různá
formy	forma	k1gFnPc4	forma
trávení	trávení	k1gNnSc2	trávení
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
až	až	k9	až
po	po	k7c6	po
tzv.	tzv.	kA	tzv.
radikální	radikální	k2eAgInSc1d1	radikální
unschooling	unschooling	k1gInSc1	unschooling
(	(	kIx(	(
<g/>
Sandra	Sandra	k1gFnSc1	Sandra
Dodd	Dodd	k1gMnSc1	Dodd
<g/>
[	[	kIx(	[
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
nebo	nebo	k8xC	nebo
Dayna	Dayna	k1gFnSc1	Dayna
Martin	Martin	k1gMnSc1	Martin
<g/>
[	[	kIx(	[
<g/>
5	[number]	k4	5
<g/>
]	]	kIx)	]
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
maximální	maximální	k2eAgFnSc6d1	maximální
svobodě	svoboda	k1gFnSc6	svoboda
pro	pro	k7c4	pro
dítě	dítě	k1gNnSc4	dítě
a	a	k8xC	a
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
jej	on	k3xPp3gMnSc4	on
co	co	k9	co
nejméně	málo	k6eAd3	málo
ovlivňovat	ovlivňovat	k5eAaImF	ovlivňovat
–	–	k?	–
je	být	k5eAaImIp3nS	být
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
zejména	zejména	k9	zejména
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
vzdělávacích	vzdělávací	k2eAgInPc2d1	vzdělávací
systémů	systém	k1gInPc2	systém
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
–	–	k?	–
existují	existovat	k5eAaImIp3nP	existovat
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
dva	dva	k4xCgInPc1	dva
typy	typ	k1gInPc1	typ
států	stát	k1gInPc2	stát
<g/>
:	:	kIx,	:
země	země	k1gFnSc1	země
s	s	k7c7	s
"	"	kIx"	"
<g/>
povinnou	povinný	k2eAgFnSc7d1	povinná
školní	školní	k2eAgFnSc7d1	školní
docházkou	docházka	k1gFnSc7	docházka
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
postkomunistické	postkomunistický	k2eAgInPc4d1	postkomunistický
státy	stát	k1gInPc4	stát
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
a	a	k8xC	a
Slovinska	Slovinsko	k1gNnSc2	Slovinsko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
třeba	třeba	k6eAd1	třeba
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
či	či	k8xC	či
některé	některý	k3yIgInPc4	některý
státy	stát	k1gInPc4	stát
USA	USA	kA	USA
<g/>
)	)	kIx)	)
a	a	k8xC	a
země	země	k1gFnSc1	země
s	s	k7c7	s
"	"	kIx"	"
<g/>
povinným	povinný	k2eAgNnSc7d1	povinné
vzděláváním	vzdělávání	k1gNnSc7	vzdělávání
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ostatní	ostatní	k2eAgMnPc1d1	ostatní
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
,	,	kIx,	,
Irsko	Irsko	k1gNnSc1	Irsko
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
[	[	kIx(	[
<g/>
6	[number]	k4	6
<g/>
]]	]]	k?	]]
<g/>
.	.	kIx.	.
</s>
<s>
Povinná	povinný	k2eAgFnSc1d1	povinná
školní	školní	k2eAgFnSc1d1	školní
docházka	docházka	k1gFnSc1	docházka
je	být	k5eAaImIp3nS	být
zákonná	zákonný	k2eAgFnSc1d1	zákonná
povinnost	povinnost	k1gFnSc1	povinnost
dětí	dítě	k1gFnPc2	dítě
navštěvovat	navštěvovat	k5eAaImF	navštěvovat
v	v	k7c6	v
určitém	určitý	k2eAgNnSc6d1	určité
věkovém	věkový	k2eAgNnSc6d1	věkové
rozmezí	rozmezí	k1gNnSc6	rozmezí
školu	škola	k1gFnSc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
povinnost	povinnost	k1gFnSc1	povinnost
zákonných	zákonný	k2eAgMnPc2d1	zákonný
zástupců	zástupce	k1gMnPc2	zástupce
děti	dítě	k1gFnPc4	dítě
příslušného	příslušný	k2eAgInSc2d1	příslušný
věku	věk	k1gInSc2	věk
do	do	k7c2	do
školy	škola	k1gFnSc2	škola
posílat	posílat	k5eAaImF	posílat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
je	být	k5eAaImIp3nS	být
umožněno	umožnit	k5eAaPmNgNnS	umožnit
dětem	dítě	k1gFnPc3	dítě
plnit	plnit	k5eAaImF	plnit
povinnou	povinný	k2eAgFnSc4d1	povinná
školní	školní	k2eAgFnSc4d1	školní
docházku	docházka	k1gFnSc4	docházka
formou	forma	k1gFnSc7	forma
individuálního	individuální	k2eAgNnSc2d1	individuální
(	(	kIx(	(
<g/>
domácího	domácí	k2eAgNnSc2d1	domácí
<g/>
)	)	kIx)	)
vzdělávání	vzdělávání	k1gNnSc1	vzdělávání
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
děti	dítě	k1gFnPc1	dítě
musí	muset	k5eAaImIp3nP	muset
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
časových	časový	k2eAgInPc6d1	časový
intervalech	interval	k1gInPc6	interval
docházet	docházet	k5eAaImF	docházet
do	do	k7c2	do
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
jsou	být	k5eAaImIp3nP	být
zapsané	zapsaný	k2eAgFnPc1d1	zapsaná
k	k	k7c3	k
individuálnímu	individuální	k2eAgNnSc3d1	individuální
vzdělávání	vzdělávání	k1gNnSc3	vzdělávání
<g/>
,	,	kIx,	,
a	a	k8xC	a
prokazovat	prokazovat	k5eAaImF	prokazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
dodržují	dodržovat	k5eAaImIp3nP	dodržovat
vzdělávací	vzdělávací	k2eAgInSc4d1	vzdělávací
program	program	k1gInSc4	program
dané	daný	k2eAgFnSc2d1	daná
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
že	že	k8xS	že
umí	umět	k5eAaImIp3nS	umět
vše	všechen	k3xTgNnSc1	všechen
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
děti	dítě	k1gFnPc4	dítě
stejného	stejný	k2eAgInSc2d1	stejný
věku	věk	k1gInSc2	věk
učí	učit	k5eAaImIp3nS	učit
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
škole	škola	k1gFnSc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
pro	pro	k7c4	pro
zajímavost	zajímavost	k1gFnSc4	zajímavost
<g/>
:	:	kIx,	:
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
je	být	k5eAaImIp3nS	být
školní	školní	k2eAgFnSc1d1	školní
docházka	docházka	k1gFnSc1	docházka
povinná	povinný	k2eAgFnSc1d1	povinná
až	až	k9	až
do	do	k7c2	do
18	[number]	k4	18
let	léto	k1gNnPc2	léto
věku	věk	k1gInSc2	věk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
domácí	domácí	k2eAgNnSc1d1	domácí
vzdělávání	vzdělávání	k1gNnSc1	vzdělávání
bylo	být	k5eAaImAgNnS	být
legalizováno	legalizovat	k5eAaBmNgNnS	legalizovat
už	už	k9	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
např.	např.	kA	např.
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
není	být	k5eNaImIp3nS	být
domácí	domácí	k2eAgNnSc4d1	domácí
vzdělání	vzdělání	k1gNnSc4	vzdělání
legalizováno	legalizován	k2eAgNnSc4d1	legalizováno
vůbec	vůbec	k9	vůbec
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
až	až	k9	až
do	do	k7c2	do
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
tomu	ten	k3xDgMnSc3	ten
tak	tak	k6eAd1	tak
bylo	být	k5eAaImAgNnS	být
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
zemích	zem	k1gFnPc6	zem
tudíž	tudíž	k8xC	tudíž
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
unschooling	unschooling	k1gInSc4	unschooling
legálně	legálně	k6eAd1	legálně
provozovat	provozovat	k5eAaImF	provozovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
dítě	dítě	k1gNnSc1	dítě
nemá	mít	k5eNaImIp3nS	mít
svobodu	svoboda	k1gFnSc4	svoboda
volit	volit	k5eAaImF	volit
si	se	k3xPyFc3	se
co	co	k9	co
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
<g/>
,	,	kIx,	,
s	s	k7c7	s
kým	kdo	k3yQnSc7	kdo
a	a	k8xC	a
jakým	jaký	k3yIgInSc7	jaký
způsobem	způsob	k1gInSc7	způsob
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
učit	učit	k5eAaImF	učit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
musí	muset	k5eAaImIp3nS	muset
plnit	plnit	k5eAaImF	plnit
požadavky	požadavek	k1gInPc4	požadavek
zpracované	zpracovaný	k2eAgInPc4d1	zpracovaný
ministerstvem	ministerstvo	k1gNnSc7	ministerstvo
školství	školství	k1gNnSc2	školství
daného	daný	k2eAgInSc2d1	daný
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
potažmo	potažmo	k6eAd1	potažmo
konkrétní	konkrétní	k2eAgFnPc4d1	konkrétní
školy	škola	k1gFnPc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
země	zem	k1gFnPc1	zem
předepisují	předepisovat	k5eAaImIp3nP	předepisovat
doma	doma	k6eAd1	doma
vzdělávaným	vzdělávaný	k2eAgFnPc3d1	vzdělávaná
dětem	dítě	k1gFnPc3	dítě
dokonce	dokonce	k9	dokonce
komisionální	komisionální	k2eAgFnPc4d1	komisionální
zkoušky	zkouška	k1gFnPc4	zkouška
<g/>
.	.	kIx.	.
</s>
<s>
Dochází	docházet	k5eAaImIp3nS	docházet
tedy	tedy	k9	tedy
k	k	k7c3	k
paradoxní	paradoxní	k2eAgFnSc3d1	paradoxní
situaci	situace	k1gFnSc3	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
rodiče	rodič	k1gMnPc1	rodič
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jsou	být	k5eAaImIp3nP	být
primárními	primární	k2eAgMnPc7d1	primární
vzdělavateli	vzdělavatel	k1gMnPc7	vzdělavatel
svých	svůj	k3xOyFgFnPc2	svůj
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
nemají	mít	k5eNaImIp3nP	mít
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
jejich	jejich	k3xOp3gNnSc4	jejich
hodnocení	hodnocení	k1gNnSc4	hodnocení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemích	zem	k1gFnPc6	zem
s	s	k7c7	s
povinným	povinný	k2eAgNnSc7d1	povinné
vzděláváním	vzdělávání	k1gNnSc7	vzdělávání
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
prokazovat	prokazovat	k5eAaImF	prokazovat
pouze	pouze	k6eAd1	pouze
fakt	fakt	k1gInSc4	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
dítě	dítě	k1gNnSc1	dítě
vzdělává	vzdělávat	k5eAaImIp3nS	vzdělávat
a	a	k8xC	a
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
řadě	řada	k1gFnSc6	řada
zemí	zem	k1gFnPc2	zem
(	(	kIx(	(
<g/>
např.	např.	kA	např.
některé	některý	k3yIgInPc4	některý
státy	stát	k1gInPc4	stát
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Anglie	Anglie	k1gFnSc1	Anglie
<g/>
)	)	kIx)	)
nemusí	muset	k5eNaImIp3nP	muset
rodiče	rodič	k1gMnPc1	rodič
svůj	svůj	k3xOyFgInSc4	svůj
záměr	záměr	k1gInSc4	záměr
vzdělávat	vzdělávat	k5eAaImF	vzdělávat
své	svůj	k3xOyFgFnPc4	svůj
děti	dítě	k1gFnPc4	dítě
doma	doma	k6eAd1	doma
vůbec	vůbec	k9	vůbec
oznamovat	oznamovat	k5eAaImF	oznamovat
<g/>
,	,	kIx,	,
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
zemích	zem	k1gFnPc6	zem
mají	mít	k5eAaImIp3nP	mít
rodiče	rodič	k1gMnPc1	rodič
pouze	pouze	k6eAd1	pouze
povinnost	povinnost	k1gFnSc4	povinnost
tento	tento	k3xDgInSc4	tento
záměr	záměr	k1gInSc4	záměr
oznámit	oznámit	k5eAaPmF	oznámit
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
například	například	k6eAd1	například
místnímu	místní	k2eAgInSc3d1	místní
školskému	školský	k2eAgInSc3d1	školský
úřadu	úřad	k1gInSc3	úřad
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
domácí	domácí	k2eAgNnSc4d1	domácí
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
legální	legální	k2eAgNnSc4d1	legální
už	už	k6eAd1	už
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1882	[number]	k4	1882
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jen	jen	k9	jen
některé	některý	k3yIgFnPc1	některý
země	zem	k1gFnPc1	zem
požadují	požadovat	k5eAaImIp3nP	požadovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
rodiče	rodič	k1gMnPc4	rodič
o	o	k7c4	o
domácí	domácí	k2eAgNnSc4d1	domácí
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
žádali	žádat	k5eAaImAgMnP	žádat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Situace	situace	k1gFnSc1	situace
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
==	==	k?	==
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c7	mezi
státy	stát	k1gInPc7	stát
s	s	k7c7	s
povinnou	povinný	k2eAgFnSc7d1	povinná
školní	školní	k2eAgFnSc7d1	školní
docházkou	docházka	k1gFnSc7	docházka
<g/>
,	,	kIx,	,
k	k	k7c3	k
níž	jenž	k3xRgFnSc3	jenž
se	se	k3xPyFc4	se
novelou	novela	k1gFnSc7	novela
(	(	kIx(	(
<g/>
178	[number]	k4	178
<g/>
/	/	kIx~	/
<g/>
2016	[number]	k4	2016
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
školského	školský	k2eAgInSc2d1	školský
zákona	zákon	k1gInSc2	zákon
od	od	k7c2	od
září	září	k1gNnSc2	září
2017	[number]	k4	2017
přidalo	přidat	k5eAaPmAgNnS	přidat
povinné	povinný	k2eAgNnSc1d1	povinné
předškolní	předškolní	k2eAgNnSc1d1	předškolní
vzdělávání	vzdělávání	k1gNnSc1	vzdělávání
pro	pro	k7c4	pro
všechny	všechen	k3xTgFnPc4	všechen
pětileté	pětiletý	k2eAgFnPc4d1	pětiletá
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
povoleno	povolit	k5eAaPmNgNnS	povolit
plnit	plnit	k5eAaImF	plnit
povinnou	povinný	k2eAgFnSc4d1	povinná
školní	školní	k2eAgFnSc4d1	školní
docházku	docházka	k1gFnSc4	docházka
individuálním	individuální	k2eAgInSc7d1	individuální
způsobem	způsob	k1gInSc7	způsob
§	§	k?	§
41	[number]	k4	41
školského	školský	k2eAgInSc2d1	školský
zákona	zákon	k1gInSc2	zákon
<g/>
[	[	kIx(	[
<g/>
7	[number]	k4	7
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
každé	každý	k3xTgNnSc1	každý
takto	takto	k6eAd1	takto
vzdělávané	vzdělávaný	k2eAgNnSc1d1	vzdělávané
dítě	dítě	k1gNnSc1	dítě
však	však	k9	však
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
zapsáno	zapsat	k5eAaPmNgNnS	zapsat
v	v	k7c6	v
konkrétní	konkrétní	k2eAgFnSc6d1	konkrétní
ZŠ	ZŠ	kA	ZŠ
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
ŠVP	ŠVP	kA	ŠVP
musí	muset	k5eAaImIp3nS	muset
také	také	k9	také
dodržovat	dodržovat	k5eAaImF	dodržovat
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
výše	výše	k1gFnSc2	výše
uvedeného	uvedený	k2eAgNnSc2d1	uvedené
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
unschooling	unschooling	k1gInSc4	unschooling
legálně	legálně	k6eAd1	legálně
provozovat	provozovat	k5eAaImF	provozovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
dítě	dítě	k1gNnSc1	dítě
nemá	mít	k5eNaImIp3nS	mít
svobodu	svoboda	k1gFnSc4	svoboda
volit	volit	k5eAaImF	volit
si	se	k3xPyFc3	se
co	co	k9	co
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
<g/>
,	,	kIx,	,
s	s	k7c7	s
kým	kdo	k3yQnSc7	kdo
a	a	k8xC	a
jakým	jaký	k3yIgInSc7	jaký
způsobem	způsob	k1gInSc7	způsob
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
učit	učit	k5eAaImF	učit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
musí	muset	k5eAaImIp3nS	muset
plnit	plnit	k5eAaImF	plnit
požadavky	požadavek	k1gInPc4	požadavek
rámcového	rámcový	k2eAgInSc2d1	rámcový
vzdělávacího	vzdělávací	k2eAgInSc2d1	vzdělávací
programu	program	k1gInSc2	program
MŠMT	MŠMT	kA	MŠMT
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
potažmo	potažmo	k6eAd1	potažmo
školního	školní	k2eAgInSc2d1	školní
vzdělávacího	vzdělávací	k2eAgInSc2d1	vzdělávací
programu	program	k1gInSc2	program
konkrétní	konkrétní	k2eAgFnSc2d1	konkrétní
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Překlad	překlad	k1gInSc1	překlad
==	==	k?	==
</s>
</p>
<p>
<s>
Do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
tento	tento	k3xDgInSc4	tento
termín	termín	k1gInSc4	termín
přeložit	přeložit	k5eAaPmF	přeložit
jako	jako	k9	jako
učení	učení	k1gNnSc4	učení
nebo	nebo	k8xC	nebo
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
svobodné	svobodný	k2eAgNnSc4d1	svobodné
<g/>
,	,	kIx,	,
přirozené	přirozený	k2eAgNnSc4d1	přirozené
<g/>
,	,	kIx,	,
nezávislé	závislý	k2eNgNnSc4d1	nezávislé
<g/>
,	,	kIx,	,
dětmi	dítě	k1gFnPc7	dítě
řízené	řízený	k2eAgNnSc1d1	řízené
<g/>
,	,	kIx,	,
studenty	student	k1gMnPc4	student
řízené	řízený	k2eAgMnPc4d1	řízený
<g/>
,	,	kIx,	,
zájmy	zájem	k1gInPc4	zájem
řízené	řízený	k2eAgNnSc1d1	řízené
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
použít	použít	k5eAaPmF	použít
termín	termín	k1gInSc4	termín
odškolení	odškolení	k1gNnSc2	odškolení
nebo	nebo	k8xC	nebo
jednoduše	jednoduše	k6eAd1	jednoduše
žití	žití	k1gNnSc2	žití
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
HOLT	HOLT	k?	HOLT
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
děti	dítě	k1gFnPc1	dítě
učí	učit	k5eAaImIp3nP	učit
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Agentura	agentura	k1gFnSc1	agentura
Strom	strom	k1gInSc1	strom
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HOLT	HOLT	k?	HOLT
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Proč	proč	k6eAd1	proč
děti	dítě	k1gFnPc1	dítě
neprospívají	prospívat	k5eNaImIp3nP	prospívat
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Agentura	agentura	k1gFnSc1	agentura
Strom	strom	k1gInSc1	strom
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VALENTA	Valenta	k1gMnSc1	Valenta
<g/>
,	,	kIx,	,
M.	M.	kA	M.
Waldorfská	waldorfský	k2eAgFnSc1d1	Waldorfská
pedagogika	pedagogika	k1gFnSc1	pedagogika
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
alternativy	alternativa	k1gFnPc1	alternativa
<g/>
.	.	kIx.	.
</s>
<s>
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
:	:	kIx,	:
Univerzita	univerzita	k1gFnSc1	univerzita
Palackého	Palacký	k1gMnSc2	Palacký
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Alternativní	alternativní	k2eAgFnPc1d1	alternativní
školy	škola	k1gFnPc1	škola
<g/>
.	.	kIx.	.
</s>
<s>
Povídání	povídání	k1gNnSc1	povídání
o	o	k7c6	o
alternativních	alternativní	k2eAgFnPc6d1	alternativní
školách	škola	k1gFnPc6	škola
a	a	k8xC	a
školství	školství	k1gNnSc6	školství
<g/>
,	,	kIx,	,
2016	[number]	k4	2016
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
2016	[number]	k4	2016
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
RÝDL	Rýdl	k1gMnSc1	Rýdl
<g/>
,	,	kIx,	,
K.	K.	kA	K.
Alternativní	alternativní	k2eAgNnSc1d1	alternativní
pedagogické	pedagogický	k2eAgNnSc1d1	pedagogické
hnutí	hnutí	k1gNnSc1	hnutí
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
společnosti	společnost	k1gFnSc6	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Marek	Marek	k1gMnSc1	Marek
Zeman	Zeman	k1gMnSc1	Zeman
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Summerhill	Summerhill	k1gMnSc1	Summerhill
</s>
</p>
<p>
<s>
Sudbury	Sudbura	k1gFnPc1	Sudbura
</s>
</p>
<p>
<s>
Domácí	domácí	k2eAgNnSc1d1	domácí
vzdělávání	vzdělávání	k1gNnSc1	vzdělávání
</s>
</p>
<p>
<s>
ScioŠkoly	ScioŠkola	k1gFnPc1	ScioŠkola
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Svoboda	Svoboda	k1gMnSc1	Svoboda
Učení	učení	k1gNnSc2	učení
</s>
</p>
<p>
<s>
ScioŠkola	ScioŠkola	k1gFnSc1	ScioŠkola
</s>
</p>
