<s>
Zimní	zimní	k2eAgFnPc1d1
olympijské	olympijský	k2eAgFnPc1d1
hry	hra	k1gFnPc1
(	(	kIx(
<g/>
též	též	k9
zimní	zimní	k2eAgFnSc1d1
olympiáda	olympiáda	k1gFnSc1
<g/>
,	,	kIx,
zkráceně	zkráceně	k6eAd1
ZOH	ZOH	kA
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
jedna	jeden	k4xCgFnSc1
z	z	k7c2
nejdůležitějších	důležitý	k2eAgFnPc2d3
zimních	zimní	k2eAgFnPc2d1
sportovních	sportovní	k2eAgFnPc2d1
událostí	událost	k1gFnPc2
<g/>
.	.	kIx.
</s>