<s>
Devátá	devátý	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
</s>
<s>
Devátá	devátý	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
</s>
<s>
konflikt	konflikt	k1gInSc1
<g/>
:	:	kIx,
Křížové	Křížové	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
</s>
<s>
Křižákův	křižákův	k2eAgInSc1d1
návrat	návrat	k1gInSc1
<g/>
,	,	kIx,
romantický	romantický	k2eAgInSc1d1
obraz	obraz	k1gInSc1
K.	K.	kA
F.	F.	kA
Lessinga	Lessing	k1gMnSc2
představuje	představovat	k5eAaImIp3nS
prohru	prohra	k1gFnSc4
křížových	křížový	k2eAgFnPc2d1
výprav	výprava	k1gFnPc2
do	do	k7c2
Svaté	svatý	k2eAgFnSc2d1
země	zem	k1gFnSc2
</s>
<s>
trvání	trvání	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
1271	#num#	k4
<g/>
–	–	k?
<g/>
1272	#num#	k4
</s>
<s>
místo	místo	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Levanta	Levanta	k1gFnSc1
(	(	kIx(
<g/>
Outremer	Outremer	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Blízký	blízký	k2eAgInSc4d1
východ	východ	k1gInSc4
</s>
<s>
výsledek	výsledek	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
bezvýsledný	bezvýsledný	k2eAgMnSc1d1
<g/>
,	,	kIx,
nepříznivý	příznivý	k2eNgMnSc1d1
pro	pro	k7c4
křižáky	křižák	k1gInPc4
</s>
<s>
desetileté	desetiletý	k2eAgNnSc4d1
příměří	příměří	k1gNnSc4
mezi	mezi	k7c7
mamlúky	mamlúek	k1gMnPc7
a	a	k8xC
křižáky	křižák	k1gMnPc7
</s>
<s>
mamlúcká	mamlúcký	k2eAgFnSc1d1
flotila	flotila	k1gFnSc1
zničena	zničit	k5eAaPmNgFnS
</s>
<s>
postupná	postupný	k2eAgFnSc1d1
ztráta	ztráta	k1gFnSc1
posledních	poslední	k2eAgFnPc2d1
křižáckých	křižácký	k2eAgFnPc2d1
držav	država	k1gFnPc2
</s>
<s>
strany	strana	k1gFnPc1
</s>
<s>
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
prince	princ	k1gMnSc2
Eduarda	Eduard	k1gMnSc2
</s>
<s>
Anglické	anglický	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
Lucemburské	lucemburský	k2eAgNnSc1d1
hrabství	hrabství	k1gNnSc1
</s>
<s>
Bretaňské	bretaňský	k2eAgNnSc1d1
vévodství	vévodství	k1gNnSc1
</s>
<s>
samostatné	samostatný	k2eAgInPc1d1
křižácké	křižácký	k2eAgInPc1d1
státy	stát	k1gInPc1
a	a	k8xC
rytířské	rytířský	k2eAgInPc1d1
řády	řád	k1gInPc1
<g/>
:	:	kIx,
</s>
<s>
Kyperské	kyperský	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
Jeruzalémské	jeruzalémský	k2eAgNnSc1d1
království	království	k1gNnSc1
Jeruzalémské	jeruzalémský	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
Řád	řád	k1gInSc4
templářů	templář	k1gMnPc2
Řád	řád	k1gInSc1
templářů	templář	k1gMnPc2
</s>
<s>
Řád	řád	k1gInSc1
johanitů	johanita	k1gMnPc2
</s>
<s>
Řád	řád	k1gInSc1
německých	německý	k2eAgMnPc2d1
rytířů	rytíř	k1gMnPc2
Řád	řád	k1gInSc1
německých	německý	k2eAgMnPc2d1
rytířů	rytíř	k1gMnPc2
</s>
<s>
Ílchanát	Ílchanát	k1gInSc1
Ílchanát	Ílchanát	k1gInSc1
Mongolské	mongolský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
se	s	k7c7
svými	svůj	k3xOyFgMnPc7
vazaly	vazal	k1gMnPc7
</s>
<s>
Kilíkijská	Kilíkijský	k2eAgFnSc1d1
Arménie	Arménie	k1gFnSc1
</s>
<s>
Antiochie-Tripolis	Antiochie-Tripolis	k1gFnSc1
</s>
<s>
Gruzínské	gruzínský	k2eAgNnSc1d1
království	království	k1gNnSc1
Gruzínské	gruzínský	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
Rúmský	Rúmský	k2eAgInSc1d1
sultanát	sultanát	k1gInSc1
Rúmský	Rúmský	k2eAgInSc4d1
sultanát	sultanát	k1gInSc4
</s>
<s>
Mamlúcký	mamlúcký	k2eAgInSc1d1
sultanát	sultanát	k1gInSc1
Mamlúcký	mamlúcký	k2eAgInSc4d1
sultanát	sultanát	k1gInSc4
</s>
<s>
Bahriové	Bahriový	k2eAgNnSc1d1
</s>
<s>
velitelé	velitel	k1gMnPc1
</s>
<s>
Eduard	Eduard	k1gMnSc1
<g/>
,	,	kIx,
vévoda	vévoda	k1gMnSc1
gaskoňský	gaskoňský	k2eAgInSc4d1
Lev	lev	k1gInSc4
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Arménský	arménský	k2eAgMnSc1d1
Hugo	Hugo	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kyperský	kyperský	k2eAgInSc1d1
Bohemund	Bohemund	k1gInSc1
VI	VI	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jindřich	Jindřich	k1gMnSc1
V.	V.	kA
Lucemburský	lucemburský	k2eAgMnSc1d1
Jan	Jan	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bretaňský	bretaňský	k2eAgMnSc1d1
Jan	Jan	k1gMnSc1
z	z	k7c2
Montfortu	Montfort	k1gInSc2
Ota	Ota	k1gMnSc1
z	z	k7c2
Grandsonu	Grandson	k1gInSc2
Abaqa	Abaqa	k1gMnSc1
Chán	chán	k1gMnSc1
Samagar	Samagar	k1gMnSc1
</s>
<s>
Bajbars	Bajbars	k6eAd1
</s>
<s>
síla	síla	k1gFnSc1
</s>
<s>
neznámá	známý	k2eNgFnSc1d1
</s>
<s>
1	#num#	k4
000	#num#	k4
mužů	muž	k1gMnPc2
v	v	k7c6
Eduardově	Eduardův	k2eAgFnSc6d1
výpravě	výprava	k1gFnSc6
</s>
<s>
10	#num#	k4
000	#num#	k4
mužů	muž	k1gMnPc2
<g/>
,	,	kIx,
zejména	zejména	k9
jezdců	jezdec	k1gMnPc2
</s>
<s>
neznámá	známý	k2eNgFnSc1d1
</s>
<s>
ztráty	ztráta	k1gFnPc1
</s>
<s>
neznámé	známý	k2eNgNnSc1d1
</s>
<s>
celkem	celkem	k6eAd1
neznámé	známý	k2eNgNnSc1d1
</s>
<s>
nazaretská	nazaretský	k2eAgFnSc1d1
posádka	posádka	k1gFnSc1
padla	padnout	k5eAaPmAgFnS,k5eAaImAgFnS
</s>
<s>
17	#num#	k4
galér	galéra	k1gFnPc2
potopeno	potopen	k2eAgNnSc4d1
</s>
<s>
1	#num#	k4
500	#num#	k4
mrtvých	mrtvý	k2eAgMnPc2d1
vojáků	voják	k1gMnPc2
ve	v	k7c6
vesnici	vesnice	k1gFnSc6
Qaqun	Qaquna	k1gFnPc2
</s>
<s>
Devátá	devátý	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
v	v	k7c6
letech	let	k1gInPc6
1271	#num#	k4
<g/>
–	–	k?
<g/>
1272	#num#	k4
byla	být	k5eAaImAgFnS
poslední	poslední	k2eAgFnSc7d1
křížovou	křížový	k2eAgFnSc7d1
výpravou	výprava	k1gFnSc7
do	do	k7c2
Svaté	svatý	k2eAgFnSc2d1
země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skončila	skončit	k5eAaPmAgFnS
vítězstvím	vítězství	k1gNnSc7
mamlúků	mamlúek	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
postupně	postupně	k6eAd1
dobyli	dobýt	k5eAaPmAgMnP
a	a	k8xC
obsadili	obsadit	k5eAaPmAgMnP
všechna	všechen	k3xTgNnPc4
křižácká	křižácký	k2eAgNnPc4d1
území	území	k1gNnPc4
na	na	k7c6
Blízkém	blízký	k2eAgInSc6d1
východě	východ	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Prolog	prolog	k1gInSc1
</s>
<s>
Roku	rok	k1gInSc2
1268	#num#	k4
byla	být	k5eAaImAgFnS
mamlúky	mamlúky	k6eAd1
dobyta	dobýt	k5eAaPmNgFnS
a	a	k8xC
zničena	zničit	k5eAaPmNgFnS
Antiochie	Antiochie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1270	#num#	k4
končí	končit	k5eAaImIp3nS
osmá	osmý	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
se	se	k3xPyFc4
přes	přes	k7c4
původní	původní	k2eAgFnPc4d1
velké	velká	k1gFnPc4
plány	plán	k1gInPc1
francouzského	francouzský	k2eAgMnSc2d1
krále	král	k1gMnSc2
Ludvíka	Ludvík	k1gMnSc2
IX	IX	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
nepodařilo	podařit	k5eNaPmAgNnS
pomoci	pomoc	k1gFnSc2
muslimy	muslim	k1gMnPc4
ohroženým	ohrožený	k2eAgInPc3d1
křižáckým	křižácký	k2eAgInPc3d1
státům	stát	k1gInPc3
na	na	k7c6
Blízkém	blízký	k2eAgInSc6d1
východě	východ	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Anglický	anglický	k2eAgMnSc1d1
princ	princ	k1gMnSc1
a	a	k8xC
gaskoňský	gaskoňský	k2eAgMnSc1d1
vévoda	vévoda	k1gMnSc1
Eduard	Eduard	k1gMnSc1
připlouvá	připlouvat	k5eAaImIp3nS
k	k	k7c3
Tunisu	Tunis	k1gInSc3
příliš	příliš	k6eAd1
pozdě	pozdě	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Angličané	Angličan	k1gMnPc1
<g/>
,	,	kIx,
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
většiny	většina	k1gFnSc2
Francouzů	Francouz	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
odplouvali	odplouvat	k5eAaImAgMnP
domů	dům	k1gInPc2
<g/>
,	,	kIx,
pokračovali	pokračovat	k5eAaImAgMnP
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
cestě	cesta	k1gFnSc6
na	na	k7c4
pomoc	pomoc	k1gFnSc4
Jeruzalémskému	jeruzalémský	k2eAgNnSc3d1
království	království	k1gNnSc3
<g/>
,	,	kIx,
Antiochijskému	antiochijský	k2eAgNnSc3d1
knížectví	knížectví	k1gNnSc3
a	a	k8xC
Tripolskému	tripolský	k2eAgNnSc3d1
hrabství	hrabství	k1gNnSc3
<g/>
,	,	kIx,
které	který	k3yIgNnSc4,k3yRgNnSc4,k3yQgNnSc4
nyní	nyní	k6eAd1
ohrožoval	ohrožovat	k5eAaImAgMnS
mamlúcký	mamlúcký	k2eAgMnSc1d1
sultán	sultán	k1gMnSc1
Bajbars	Bajbarsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Anglickému	anglický	k2eAgMnSc3d1
princi	princ	k1gMnSc3
přislíbil	přislíbit	k5eAaPmAgMnS
podporu	podpora	k1gFnSc4
bratr	bratr	k1gMnSc1
zemřelého	zemřelý	k2eAgMnSc2d1
francouzského	francouzský	k2eAgMnSc2d1
krále	král	k1gMnSc2
Ludvíka	Ludvík	k1gMnSc2
<g/>
,	,	kIx,
sicilský	sicilský	k2eAgMnSc1d1
král	král	k1gMnSc1
Karel	Karel	k1gMnSc1
I.	I.	kA
z	z	k7c2
Anjou	Anjý	k2eAgFnSc7d1
<g/>
.	.	kIx.
</s>
<s>
Průběh	průběh	k1gInSc1
výpravy	výprava	k1gFnSc2
</s>
<s>
Postup	postup	k1gInSc1
křižáckých	křižácký	k2eAgNnPc2d1
vojsk	vojsko	k1gNnPc2
během	během	k7c2
deváté	devátý	k4xOgFnSc2
kruciáty	kruciáta	k1gFnSc2
</s>
<s>
Výprava	výprava	k1gFnSc1
přezimovala	přezimovat	k5eAaBmAgFnS
na	na	k7c6
Sicílii	Sicílie	k1gFnSc6
a	a	k8xC
v	v	k7c6
květnu	květen	k1gInSc6
roku	rok	k1gInSc2
1271	#num#	k4
přistala	přistat	k5eAaPmAgFnS
v	v	k7c6
Akkonu	Akkon	k1gInSc6
<g/>
,	,	kIx,
právě	právě	k6eAd1
v	v	k7c6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
sultán	sultán	k1gMnSc1
Bajbars	Bajbarsa	k1gFnPc2
obléhal	obléhat	k5eAaImAgMnS
město	město	k1gNnSc4
Tripolis	Tripolis	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolu	spolu	k6eAd1
s	s	k7c7
vojskem	vojsko	k1gNnSc7
kyperského	kyperský	k2eAgMnSc2d1
krále	král	k1gMnSc2
Huga	Hugo	k1gMnSc2
III	III	kA
<g/>
.	.	kIx.
se	se	k3xPyFc4
vůdci	vůdce	k1gMnPc1
rozhodli	rozhodnout	k5eAaPmAgMnP
prorazit	prorazit	k5eAaPmF
mamlúcké	mamlúcký	k2eAgNnSc4d1
obležení	obležení	k1gNnSc4
Tripolisu	Tripolis	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Eduard	Eduard	k1gMnSc1
si	se	k3xPyFc3
byl	být	k5eAaImAgMnS
vědom	vědom	k2eAgMnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
proti	proti	k7c3
přesile	přesila	k1gFnSc3
muslimů	muslim	k1gMnPc2
nemá	mít	k5eNaImIp3nS
šanci	šance	k1gFnSc4
na	na	k7c4
úspěch	úspěch	k1gInSc4
a	a	k8xC
proto	proto	k8xC
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
jednání	jednání	k1gNnSc3
s	s	k7c7
mongolským	mongolský	k2eAgMnSc7d1
chánem	chán	k1gMnSc7
Ílchanátu	Ílchanát	k1gInSc2
Abaqou	Abaqa	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chán	chán	k1gMnSc1
přijímá	přijímat	k5eAaImIp3nS
dohodu	dohoda	k1gFnSc4
o	o	k7c6
společném	společný	k2eAgInSc6d1
útoku	útok	k1gInSc6
proti	proti	k7c3
mamlúkům	mamlúek	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyslaných	vyslaný	k2eAgInPc2d1
10	#num#	k4
000	#num#	k4
jezdců	jezdec	k1gInPc2
vedených	vedený	k2eAgInPc2d1
vojevůdcem	vojevůdce	k1gMnSc7
Samagarem	Samagar	k1gMnSc7
obsazuje	obsazovat	k5eAaImIp3nS
území	území	k1gNnPc4
na	na	k7c4
jih	jih	k1gInSc4
od	od	k7c2
města	město	k1gNnSc2
Aleppa	Alepp	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mocný	mocný	k2eAgInSc1d1
protiútok	protiútok	k1gInSc1
mamlúků	mamlúek	k1gMnPc2
však	však	k9
donutí	donutit	k5eAaPmIp3nS
Mongoly	Mongol	k1gMnPc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
se	se	k3xPyFc4
stáhli	stáhnout	k5eAaPmAgMnP
až	až	k9
za	za	k7c4
řeku	řeka	k1gFnSc4
Eufrat	Eufrat	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sultán	sultána	k1gFnPc2
Bajbars	Bajbarsa	k1gFnPc2
se	se	k3xPyFc4
neúspěšně	úspěšně	k6eNd1
pokusil	pokusit	k5eAaPmAgMnS
s	s	k7c7
vojskem	vojsko	k1gNnSc7
vylodit	vylodit	k5eAaPmF
na	na	k7c6
Kypru	Kypr	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Princ	princ	k1gMnSc1
Eduard	Eduard	k1gMnSc1
vyburcoval	vyburcovat	k5eAaPmAgMnS
zbylé	zbylý	k2eAgInPc4d1
křižácké	křižácký	k2eAgInPc4d1
rytíře	rytíř	k1gMnSc4
k	k	k7c3
boji	boj	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vytáhl	vytáhnout	k5eAaPmAgMnS
v	v	k7c6
čele	čelo	k1gNnSc6
vojska	vojsko	k1gNnSc2
na	na	k7c4
jih	jih	k1gInSc4
<g/>
,	,	kIx,
rytíři	rytíř	k1gMnPc1
však	však	k9
odmítli	odmítnout	k5eAaPmAgMnP
bránit	bránit	k5eAaImF
nově	nově	k6eAd1
nabytá	nabytý	k2eAgNnPc4d1
území	území	k1gNnPc4
a	a	k8xC
Eduard	Eduard	k1gMnSc1
se	se	k3xPyFc4
proto	proto	k8xC
stáhl	stáhnout	k5eAaPmAgMnS
zpět	zpět	k6eAd1
do	do	k7c2
Akkonu	Akkon	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Princ	princ	k1gMnSc1
byl	být	k5eAaImAgMnS
v	v	k7c6
Akkonu	Akkon	k1gInSc6
téměř	téměř	k6eAd1
zabit	zabít	k5eAaPmNgMnS
vrahem	vrah	k1gMnSc7
<g/>
,	,	kIx,
kterého	který	k3yQgMnSc4,k3yRgMnSc4,k3yIgMnSc4
vyslal	vyslat	k5eAaPmAgMnS
sultán	sultán	k1gMnSc1
Bajbars	Bajbarsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
létě	léto	k1gNnSc6
<g/>
,	,	kIx,
když	když	k8xS
již	již	k6eAd1
Eduard	Eduard	k1gMnSc1
připravoval	připravovat	k5eAaImAgMnS
novou	nový	k2eAgFnSc4d1
výpravu	výprava	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
měla	mít	k5eAaImAgFnS
za	za	k7c4
úkol	úkol	k1gInSc4
dobýt	dobýt	k5eAaPmF
Jeruzalém	Jeruzalém	k1gInSc4
<g/>
,	,	kIx,
přichází	přicházet	k5eAaImIp3nS
z	z	k7c2
Anglie	Anglie	k1gFnSc2
zpráva	zpráva	k1gFnSc1
o	o	k7c6
smrti	smrt	k1gFnSc6
krále	král	k1gMnSc4
Jindřicha	Jindřich	k1gMnSc2
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Eduard	Eduard	k1gMnSc1
proto	proto	k8xC
v	v	k7c6
září	září	k1gNnSc6
roku	rok	k1gInSc2
1272	#num#	k4
opouští	opouštět	k5eAaImIp3nS
Svatou	svatý	k2eAgFnSc4d1
zemi	zem	k1gFnSc4
a	a	k8xC
odplouvá	odplouvat	k5eAaImIp3nS
domů	dům	k1gInPc2
do	do	k7c2
Anglie	Anglie	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
korunován	korunovat	k5eAaBmNgInS
králem	král	k1gMnSc7
Eduardem	Eduard	k1gMnSc7
I.	I.	kA
Sliboval	slibovat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
do	do	k7c2
Svaté	svatý	k2eAgFnSc2d1
země	zem	k1gFnSc2
ještě	ještě	k9
vrátí	vrátit	k5eAaPmIp3nS
<g/>
,	,	kIx,
ale	ale	k8xC
politické	politický	k2eAgInPc1d1
problémy	problém	k1gInPc1
v	v	k7c6
Anglii	Anglie	k1gFnSc6
mu	on	k3xPp3gInSc3
to	ten	k3xDgNnSc1
již	již	k6eAd1
nedovolily	dovolit	k5eNaPmAgFnP
<g/>
.	.	kIx.
</s>
<s>
Důsledky	důsledek	k1gInPc1
</s>
<s>
Gustave	Gustav	k1gMnSc5
Doré	Doré	k1gNnSc6
<g/>
:	:	kIx,
Princ	princ	k1gMnSc1
Eduard	Eduard	k1gMnSc1
při	při	k7c6
sebeobraně	sebeobrana	k1gFnSc6
zabijí	zabít	k5eAaPmIp3nP
nájemného	nájemný	k2eAgMnSc4d1
vraha	vrah	k1gMnSc4
</s>
<s>
Sultán	sultán	k1gMnSc1
Bajbars	Bajbars	k1gInSc4
po	po	k7c6
Eduardově	Eduardův	k2eAgNnSc6d1
odplutí	odplutí	k1gNnSc6
postupně	postupně	k6eAd1
likvidoval	likvidovat	k5eAaBmAgInS
poslední	poslední	k2eAgInPc4d1
zbytky	zbytek	k1gInPc4
křižáckých	křižácký	k2eAgFnPc2d1
držav	država	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1289	#num#	k4
byl	být	k5eAaImAgInS
egyptským	egyptský	k2eAgMnSc7d1
sultánem	sultán	k1gMnSc7
Qalawunem	Qalawun	k1gMnSc7
dobyt	dobyt	k2eAgInSc4d1
a	a	k8xC
vypleněn	vypleněn	k2eAgInSc4d1
Tripolis	Tripolis	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
šestitýdenním	šestitýdenní	k2eAgNnSc6d1
obléhání	obléhání	k1gNnSc6
padl	padnout	k5eAaImAgMnS,k5eAaPmAgMnS
18	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1291	#num#	k4
samotný	samotný	k2eAgInSc1d1
Akkon	Akkon	k1gInSc1
–	–	k?
poslední	poslední	k2eAgFnSc1d1
velká	velký	k2eAgFnSc1d1
křižácká	křižácký	k2eAgFnSc1d1
država	država	k1gFnSc1
na	na	k7c6
Blízkém	blízký	k2eAgInSc6d1
východě	východ	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křesťané	křesťan	k1gMnPc1
<g/>
,	,	kIx,
kterým	který	k3yIgMnPc3,k3yQgMnPc3,k3yRgMnPc3
se	se	k3xPyFc4
nepodařilo	podařit	k5eNaPmAgNnS
uniknout	uniknout	k5eAaPmF
<g/>
,	,	kIx,
byli	být	k5eAaImAgMnP
vyvražděni	vyvraždit	k5eAaPmNgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přístavní	přístavní	k2eAgInSc1d1
města	město	k1gNnSc2
byla	být	k5eAaImAgFnS
zbořena	zbořen	k2eAgFnSc1d1
a	a	k8xC
nová	nový	k2eAgNnPc1d1
centra	centrum	k1gNnPc1
byla	být	k5eAaImAgNnP
později	pozdě	k6eAd2
postavena	postavit	k5eAaPmNgNnP
dál	daleko	k6eAd2
od	od	k7c2
moře	moře	k1gNnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
již	již	k6eAd1
nemohla	moct	k5eNaImAgFnS
sloužit	sloužit	k5eAaImF
jako	jako	k9
základny	základna	k1gFnSc2
pro	pro	k7c4
další	další	k2eAgFnSc4d1
křesťanskou	křesťanský	k2eAgFnSc4d1
ofenzívu	ofenzíva	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgFnSc1d1
křižácká	křižácký	k2eAgFnSc1d1
pevnost	pevnost	k1gFnSc1
v	v	k7c6
Zámoří	zámoří	k1gNnSc6
na	na	k7c6
ostrově	ostrov	k1gInSc6
Ruad	Ruada	k1gFnPc2
padla	padnout	k5eAaPmAgFnS,k5eAaImAgFnS
v	v	k7c6
září	září	k1gNnSc6
1303	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Latinské	latinský	k2eAgFnPc4d1
državy	država	k1gFnPc4
přetrvaly	přetrvat	k5eAaPmAgFnP
pouze	pouze	k6eAd1
na	na	k7c6
Kypru	Kypr	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dobytím	dobytí	k1gNnSc7
Akkonu	Akkon	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1291	#num#	k4
skončilo	skončit	k5eAaPmAgNnS
období	období	k1gNnSc1
křížových	křížový	k2eAgFnPc2d1
výprav	výprava	k1gFnPc2
do	do	k7c2
Svaté	svatý	k2eAgFnSc2d1
země	zem	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
od	od	k7c2
vyhlášení	vyhlášení	k1gNnSc2
první	první	k4xOgFnSc2
křížové	křížový	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
papežem	papež	k1gMnSc7
Urbanem	Urban	k1gMnSc7
II	II	kA
<g/>
.	.	kIx.
v	v	k7c6
roce	rok	k1gInSc6
1095	#num#	k4
trvalo	trvat	k5eAaImAgNnS
bezmála	bezmála	k6eAd1
200	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
TYERMAN	TYERMAN	kA
<g/>
,	,	kIx,
Christopher	Christophra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svaté	svatý	k2eAgInPc1d1
války	válek	k1gInPc1
:	:	kIx,
dějiny	dějiny	k1gFnPc1
křížových	křížový	k2eAgFnPc2d1
výprav	výprava	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Lidové	lidový	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
.	.	kIx.
926	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7422	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
91	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
WOLFF	WOLFF	kA
<g/>
,	,	kIx,
Robert	Robert	k1gMnSc1
L.	L.	kA
<g/>
;	;	kIx,
HAZARD	hazard	k1gMnSc1
<g/>
,	,	kIx,
Harry	Harr	k1gMnPc4
W.	W.	kA
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
History	Histor	k1gInPc1
of	of	k?
the	the	k?
Crusades	Crusades	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vol	vol	k6eAd1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
,	,	kIx,
The	The	k1gMnSc1
later	later	k1gMnSc1
Crusades	Crusades	k1gMnSc1
<g/>
,	,	kIx,
1189	#num#	k4
<g/>
-	-	kIx~
<g/>
1311	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Madison	Madison	k1gInSc1
<g/>
:	:	kIx,
University	universita	k1gFnSc2
of	of	k?
Wisconsin	Wisconsin	k2eAgInSc1d1
Press	Press	k1gInSc1
<g/>
,	,	kIx,
1969	#num#	k4
<g/>
.	.	kIx.
871	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Křížové	Křížové	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
křížové	křížový	k2eAgFnSc2d1
výpravyproti	výpravyprot	k1gMnPc5
muslimům	muslim	k1gMnPc3
</s>
<s>
do	do	k7c2
Svaté	svatý	k2eAgFnSc2d1
země	zem	k1gFnSc2
<g/>
(	(	kIx(
<g/>
1095	#num#	k4
<g/>
–	–	k?
<g/>
1291	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Lidová	lidový	k2eAgFnSc1d1
(	(	kIx(
<g/>
1096	#num#	k4
<g/>
)	)	kIx)
•	•	k?
První	první	k4xOgFnSc2
(	(	kIx(
<g/>
1095	#num#	k4
<g/>
–	–	k?
<g/>
1099	#num#	k4
<g/>
)	)	kIx)
•	•	k?
výprava	výprava	k1gFnSc1
roku	rok	k1gInSc2
1101	#num#	k4
(	(	kIx(
<g/>
1101	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Norská	norský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1107	#num#	k4
<g/>
–	–	k?
<g/>
1110	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Benátská	benátský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1122	#num#	k4
<g/>
–	–	k?
<g/>
1124	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Damašská	damašský	k2eAgFnSc1d1
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
1129	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Druhá	druhý	k4xOgFnSc1
(	(	kIx(
<g/>
1147	#num#	k4
<g/>
–	–	k?
<g/>
1149	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Amauryho	Amaury	k1gMnSc2
tažení	tažení	k1gNnSc1
do	do	k7c2
Egypta	Egypt	k1gInSc2
(	(	kIx(
<g/>
1154	#num#	k4
<g/>
–	–	k?
<g/>
1169	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Třetí	třetí	k4xOgFnSc2
(	(	kIx(
<g/>
1189	#num#	k4
<g/>
–	–	k?
<g/>
1192	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Německá	německý	k2eAgFnSc1d1
(	(	kIx(
<g/>
1197	#num#	k4
<g/>
–	–	k?
<g/>
1198	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
•	•	k?
Čtvrtá	čtvrtá	k1gFnSc1
(	(	kIx(
<g/>
1202	#num#	k4
<g/>
–	–	k?
<g/>
1204	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Dětské	dětský	k2eAgNnSc1d1
(	(	kIx(
<g/>
1212	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Pátá	pátá	k1gFnSc1
(	(	kIx(
<g/>
1217	#num#	k4
<g/>
–	–	k?
<g/>
1221	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Šestá	šestý	k4xOgFnSc1
(	(	kIx(
<g/>
1228	#num#	k4
<g/>
–	–	k?
<g/>
1229	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Baronská	baronský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1239	#num#	k4
<g/>
–	–	k?
<g/>
1241	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
•	•	k?
Pastýřská	pastýřská	k1gFnSc1
(	(	kIx(
<g/>
1251	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Sedmá	sedmý	k4xOgFnSc1
(	(	kIx(
<g/>
1248	#num#	k4
<g/>
–	–	k?
<g/>
1254	#num#	k4
<g/>
)	)	kIx)
•	•	k?
výprava	výprava	k1gFnSc1
roku	rok	k1gInSc2
1267	#num#	k4
•	•	k?
Osmá	osmý	k4xOgFnSc1
(	(	kIx(
<g/>
1270	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Devátá	devátý	k4xOgFnSc1
(	(	kIx(
<g/>
1271	#num#	k4
<g/>
–	–	k?
<g/>
1272	#num#	k4
<g/>
)	)	kIx)
Reconquista	Reconquista	k1gMnSc1
<g/>
(	(	kIx(
<g/>
718	#num#	k4
<g/>
–	–	k?
<g/>
1492	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Reconquista	Reconquista	k1gMnSc1
(	(	kIx(
<g/>
718	#num#	k4
<g/>
–	–	k?
<g/>
1492	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Barbastro	Barbastro	k1gNnSc4
(	(	kIx(
<g/>
1063	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Mallorka	Mallorka	k1gFnSc1
(	(	kIx(
<g/>
1313	#num#	k4
<g/>
–	–	k?
<g/>
1315	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Las	laso	k1gNnPc2
Navas	Navas	k1gInSc1
de	de	k?
Tolosa	Tolosa	k1gFnSc1
(	(	kIx(
<g/>
1212	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Pastýřská	pastýřská	k1gFnSc1
(	(	kIx(
<g/>
1320	#num#	k4
<g/>
)	)	kIx)
po	po	k7c6
roce	rok	k1gInSc6
1291	#num#	k4
</s>
<s>
Chudinská	chudinský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1309	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Smyrenské	smyrenský	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
(	(	kIx(
<g/>
1343	#num#	k4
<g/>
–	–	k?
<g/>
1351	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Alexandrijská	alexandrijský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1365	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Mahdijská	Mahdijský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1390	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Modlící	modlící	k2eAgMnSc1d1
se	se	k3xPyFc4
křižácký	křižácký	k2eAgMnSc1d1
rytíř	rytíř	k1gMnSc1
na	na	k7c6
dobové	dobový	k2eAgFnSc6d1
miniatuře	miniatura	k1gFnSc6
(	(	kIx(
<g/>
BL	BL	kA
MS	MS	kA
Royal	Royal	k1gInSc4
2A	2A	k4
XXII	XXII	kA
f.	f.	k?
220	#num#	k4
<g/>
)	)	kIx)
severní	severní	k2eAgFnSc2d1
křížové	křížový	k2eAgFnSc2d1
výpravyproti	výpravyprot	k1gMnPc1
pohanům	pohan	k1gMnPc3
</s>
<s>
Kalmarská	Kalmarský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1123	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Venedská	Venedský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1147	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Švédské	švédský	k2eAgNnSc1d1
(	(	kIx(
<g/>
tři	tři	k4xCgFnPc1
<g/>
)	)	kIx)
•	•	k?
Livonská	Livonský	k2eAgFnSc1d1
(	(	kIx(
<g/>
13	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
)	)	kIx)
•	•	k?
Pruská	pruský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1217	#num#	k4
<g/>
–	–	k?
<g/>
1274	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Litevská	litevský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1283	#num#	k4
<g/>
–	–	k?
<g/>
1410	#num#	k4
<g/>
)	)	kIx)
křížové	křížový	k2eAgFnSc2d1
výpravyproti	výpravyprot	k1gMnPc5
kacířům	kacíř	k1gMnPc3
</s>
<s>
proti	proti	k7c3
kacířům	kacíř	k1gMnPc3
aproti	aprot	k1gMnPc5
neposlušnýmpanovníkům	neposlušnýmpanovník	k1gMnPc3
</s>
<s>
Albigenská	albigenský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1209	#num#	k4
<g/>
–	–	k?
<g/>
1229	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Drentská	Drentský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1228	#num#	k4
<g/>
–	–	k?
<g/>
1232	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Stedingerská	Stedingerský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1233	#num#	k4
<g/>
–	–	k?
<g/>
1234	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bosenská	bosenský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1235	#num#	k4
<g/>
–	–	k?
<g/>
1241	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Novgorodská	novgorodský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1241	#num#	k4
<g/>
–	–	k?
<g/>
1242	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Aragonská	aragonský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1284	#num#	k4
<g/>
–	–	k?
<g/>
1285	#num#	k4
<g/>
)	)	kIx)
•	•	k?
do	do	k7c2
Čech	Čechy	k1gFnPc2
proti	proti	k7c3
valdenským	valdenští	k1gMnPc3
(	(	kIx(
<g/>
1340	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Despenserova	Despenserův	k2eAgInSc2d1
(	(	kIx(
<g/>
1382	#num#	k4
<g/>
–	–	k?
<g/>
1383	#num#	k4
<g/>
)	)	kIx)
proti	proti	k7c3
husitům	husita	k1gMnPc3
</s>
<s>
První	první	k4xOgMnSc1
(	(	kIx(
<g/>
1420	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Druhá	druhý	k4xOgFnSc1
(	(	kIx(
<g/>
1421	#num#	k4
<g/>
–	–	k?
<g/>
1422	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Třetí	třetí	k4xOgFnSc2
(	(	kIx(
<g/>
1427	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Čtvrtá	čtvrtá	k1gFnSc1
(	(	kIx(
<g/>
1431	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Česko-uherské	česko-uherský	k2eAgFnSc2d1
války	válka	k1gFnSc2
(	(	kIx(
<g/>
1468	#num#	k4
<g/>
-	-	kIx~
<g/>
1478	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
pozdní	pozdní	k2eAgInPc1d1
křížové	křížový	k2eAgInPc1d1
výpravyproti	výpravyprot	k1gMnPc1
Osmanským	osmanský	k2eAgFnPc3d1
Turkům	turek	k1gInPc3
</s>
<s>
Savojská	savojský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1366	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Nikopolská	Nikopolský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1396	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Varnská	Varnská	k1gFnSc1
(	(	kIx(
<g/>
1444	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Projekt	projekt	k1gInSc1
Evžena	Evžen	k1gMnSc2
IV	IV	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1444	#num#	k4
<g/>
–	–	k?
<g/>
1445	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Projekt	projekt	k1gInSc1
Kalixta	Kalixta	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1455	#num#	k4
<g/>
–	–	k?
<g/>
1458	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Projekt	projekt	k1gInSc1
Pia	Pius	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1463	#num#	k4
<g/>
–	–	k?
<g/>
1464	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Portugalská	portugalský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1481	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k5eAaPmF
<g/>
:	:	kIx,
<g/>
bold	bold	k6eAd1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc7
<g/>
:	:	kIx,
Křížové	Křížové	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
|	|	kIx~
Středověk	středověk	k1gInSc1
|	|	kIx~
Válka	válka	k1gFnSc1
</s>
