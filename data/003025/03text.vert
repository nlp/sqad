<s>
C	C	kA	C
<g/>
++	++	k?	++
je	být	k5eAaImIp3nS	být
multiparadigmatický	multiparadigmatický	k2eAgInSc1d1	multiparadigmatický
programovací	programovací	k2eAgInSc1d1	programovací
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
Bjarne	Bjarn	k1gInSc5	Bjarn
Stroustrup	Stroustrup	k1gMnSc1	Stroustrup
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
v	v	k7c6	v
Bellových	Bellův	k2eAgFnPc6d1	Bellova
laboratořích	laboratoř	k1gFnPc6	laboratoř
AT	AT	kA	AT
<g/>
&	&	k?	&
<g/>
T	T	kA	T
rozšířením	rozšíření	k1gNnSc7	rozšíření
jazyka	jazyk	k1gInSc2	jazyk
C	C	kA	C
.	.	kIx.	.
</s>
<s>
C	C	kA	C
<g/>
++	++	k?	++
podporuje	podporovat	k5eAaImIp3nS	podporovat
několik	několik	k4yIc1	několik
programovacích	programovací	k2eAgInPc2d1	programovací
stylů	styl	k1gInPc2	styl
(	(	kIx(	(
<g/>
paradigmat	paradigma	k1gNnPc2	paradigma
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
procedurální	procedurální	k2eAgNnSc4d1	procedurální
programování	programování	k1gNnSc4	programování
<g/>
,	,	kIx,	,
objektově	objektově	k6eAd1	objektově
orientované	orientovaný	k2eAgNnSc4d1	orientované
programování	programování	k1gNnSc4	programování
a	a	k8xC	a
generické	generický	k2eAgNnSc4d1	generické
programování	programování	k1gNnSc4	programování
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
tedy	tedy	k9	tedy
jazykem	jazyk	k1gInSc7	jazyk
čistě	čistě	k6eAd1	čistě
objektovým	objektový	k2eAgInSc7d1	objektový
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
patří	patřit	k5eAaImIp3nS	patřit
C	C	kA	C
<g/>
++	++	k?	++
mezi	mezi	k7c4	mezi
nejrozšířenější	rozšířený	k2eAgInPc4d3	nejrozšířenější
programovací	programovací	k2eAgInPc4d1	programovací
jazyky	jazyk	k1gInPc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starý	k2eAgFnSc1d2	starší
verze	verze	k1gFnSc1	verze
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
označované	označovaný	k2eAgFnPc4d1	označovaná
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
C	C	kA	C
with	with	k1gMnSc1	with
Classes	Classes	k1gMnSc1	Classes
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
C	C	kA	C
s	s	k7c7	s
třídami	třída	k1gFnPc7	třída
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
používány	používat	k5eAaImNgInP	používat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
C	C	kA	C
<g/>
++	++	k?	++
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
Rick	Rick	k1gMnSc1	Rick
Mascitti	Mascitť	k1gFnSc2	Mascitť
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
jméno	jméno	k1gNnSc1	jméno
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS	zdůrazňovat
evoluční	evoluční	k2eAgFnSc4d1	evoluční
povahu	povaha	k1gFnSc4	povaha
změn	změna	k1gFnPc2	změna
oproti	oproti	k7c3	oproti
jazyku	jazyk	k1gInSc3	jazyk
C	C	kA	C
<g/>
;	;	kIx,	;
"	"	kIx"	"
<g/>
++	++	k?	++
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
operátor	operátor	k1gInSc4	operátor
inkrementace	inkrementace	k1gFnSc2	inkrementace
v	v	k7c4	v
C.	C.	kA	C.
Kratší	krátký	k2eAgNnSc1d2	kratší
jméno	jméno	k1gNnSc1	jméno
"	"	kIx"	"
<g/>
C	C	kA	C
<g/>
+	+	kIx~	+
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
syntaktická	syntaktický	k2eAgFnSc1d1	syntaktická
chyba	chyba	k1gFnSc1	chyba
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
též	též	k9	též
použito	použít	k5eAaPmNgNnS	použít
jako	jako	k8xS	jako
jméno	jméno	k1gNnSc1	jméno
jiného	jiný	k2eAgInSc2d1	jiný
nesouvisejícího	související	k2eNgInSc2d1	nesouvisející
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
byl	být	k5eAaImAgInS	být
jazyk	jazyk	k1gInSc1	jazyk
vyvíjen	vyvíjet	k5eAaImNgInS	vyvíjet
již	již	k6eAd1	již
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
oficiální	oficiální	k2eAgFnSc1d1	oficiální
norma	norma	k1gFnSc1	norma
C	C	kA	C
<g/>
++	++	k?	++
byla	být	k5eAaImAgFnS	být
přijata	přijmout	k5eAaPmNgFnS	přijmout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
další	další	k2eAgInSc1d1	další
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
(	(	kIx(	(
<g/>
INCITS	INCITS	kA	INCITS
<g/>
/	/	kIx~	/
<g/>
ISO	ISO	kA	ISO
<g/>
/	/	kIx~	/
<g/>
IEC	IEC	kA	IEC
14882	[number]	k4	14882
<g/>
-	-	kIx~	-
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
a	a	k8xC	a
2007	[number]	k4	2007
byly	být	k5eAaImAgFnP	být
přijaty	přijmout	k5eAaPmNgFnP	přijmout
některé	některý	k3yIgFnPc1	některý
aktualizace	aktualizace	k1gFnPc1	aktualizace
<g/>
.	.	kIx.	.
</s>
<s>
Standard	standard	k1gInSc1	standard
označovaný	označovaný	k2eAgInSc1d1	označovaný
jako	jako	k8xC	jako
C	C	kA	C
<g/>
++	++	k?	++
<g/>
11	[number]	k4	11
<g/>
,	,	kIx,	,
značně	značně	k6eAd1	značně
rozšířil	rozšířit	k5eAaPmAgMnS	rozšířit
C	C	kA	C
<g/>
++	++	k?	++
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
organizací	organizace	k1gFnSc7	organizace
ISO	ISO	kA	ISO
v	v	k7c6	v
září	září	k1gNnSc6	září
2011	[number]	k4	2011
jako	jako	k8xS	jako
ISO	ISO	kA	ISO
<g/>
/	/	kIx~	/
<g/>
IEC	IEC	kA	IEC
14882	[number]	k4	14882
<g/>
:	:	kIx,	:
<g/>
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Současný	současný	k2eAgInSc1d1	současný
standard	standard	k1gInSc1	standard
je	být	k5eAaImIp3nS	být
C	C	kA	C
<g/>
++	++	k?	++
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
</s>
<s>
Jazyk	jazyk	k1gInSc1	jazyk
C	C	kA	C
je	být	k5eAaImIp3nS	být
až	až	k9	až
na	na	k7c4	na
několik	několik	k4yIc4	několik
jasně	jasně	k6eAd1	jasně
definovaných	definovaný	k2eAgFnPc2d1	definovaná
výjimek	výjimka	k1gFnPc2	výjimka
podmnožinou	podmnožina	k1gFnSc7	podmnožina
C	C	kA	C
<g/>
++	++	k?	++
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
uvádí	uvádět	k5eAaImIp3nS	uvádět
Bjarne	Bjarn	k1gInSc5	Bjarn
Stroustrup	Stroustrup	k1gInSc1	Stroustrup
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgInPc1	všechen
programy	program	k1gInPc1	program
uvedené	uvedený	k2eAgInPc1d1	uvedený
ve	v	k7c6	v
slavné	slavný	k2eAgFnSc6d1	slavná
učebnici	učebnice	k1gFnSc6	učebnice
jazyka	jazyk	k1gInSc2	jazyk
C	C	kA	C
The	The	k1gFnSc2	The
C	C	kA	C
Programming	Programming	k1gInSc4	Programming
Language	language	k1gFnPc2	language
od	od	k7c2	od
Briana	Brian	k1gMnSc2	Brian
W.	W.	kA	W.
Kernighana	Kernighan	k1gMnSc2	Kernighan
a	a	k8xC	a
Dennise	Dennise	k1gFnSc2	Dennise
M.	M.	kA	M.
Ritchieho	Ritchie	k1gMnSc2	Ritchie
jsou	být	k5eAaImIp3nP	být
zároveň	zároveň	k6eAd1	zároveň
programy	program	k1gInPc4	program
v	v	k7c6	v
C	C	kA	C
<g/>
++	++	k?	++
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc4	první
překladače	překladač	k1gInPc4	překladač
C	C	kA	C
<g/>
++	++	k?	++
byly	být	k5eAaImAgInP	být
preprocesory	preprocesor	k1gInPc1	preprocesor
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
překládaly	překládat	k5eAaImAgInP	překládat
z	z	k7c2	z
C	C	kA	C
<g/>
++	++	k?	++
do	do	k7c2	do
čistého	čistý	k2eAgInSc2d1	čistý
C.	C.	kA	C.
Považovat	považovat	k5eAaImF	považovat
jazyk	jazyk	k1gInSc4	jazyk
C	C	kA	C
<g/>
++	++	k?	++
za	za	k7c4	za
pouhé	pouhý	k2eAgNnSc4d1	pouhé
rozšíření	rozšíření	k1gNnSc4	rozšíření
jazyka	jazyk	k1gInSc2	jazyk
C	C	kA	C
by	by	kYmCp3nS	by
ale	ale	k9	ale
bylo	být	k5eAaImAgNnS	být
chybou	chyba	k1gFnSc7	chyba
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
není	být	k5eNaImIp3nS	být
s	s	k7c7	s
jazykem	jazyk	k1gInSc7	jazyk
C	C	kA	C
zcela	zcela	k6eAd1	zcela
kompatibilní	kompatibilní	k2eAgFnSc1d1	kompatibilní
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
programy	program	k1gInPc1	program
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
C	C	kA	C
nelze	lze	k6eNd1	lze
překládat	překládat	k5eAaImF	překládat
překladači	překladač	k1gMnPc1	překladač
pro	pro	k7c4	pro
C	C	kA	C
<g/>
++	++	k?	++
<g/>
.	.	kIx.	.
</s>
<s>
C	C	kA	C
<g/>
++	++	k?	++
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
18	[number]	k4	18
primitivních	primitivní	k2eAgInPc2d1	primitivní
datových	datový	k2eAgInPc2d1	datový
typů	typ	k1gInPc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celá	k1gFnSc1	celá
čísla	číslo	k1gNnSc2	číslo
<g/>
:	:	kIx,	:
Čísla	číslo	k1gNnSc2	číslo
s	s	k7c7	s
plovoucí	plovoucí	k2eAgFnSc7d1	plovoucí
desetinnou	desetinný	k2eAgFnSc7d1	desetinná
čárkou	čárka	k1gFnSc7	čárka
<g/>
:	:	kIx,	:
Znaky	znak	k1gInPc1	znak
<g/>
:	:	kIx,	:
Logická	logický	k2eAgFnSc1d1	logická
hodnota	hodnota	k1gFnSc1	hodnota
<g/>
:	:	kIx,	:
bool	bool	k1gInSc1	bool
velikost	velikost	k1gFnSc1	velikost
<g/>
:	:	kIx,	:
min	min	kA	min
<g/>
.	.	kIx.	.
8	[number]	k4	8
bitů	bit	k1gInPc2	bit
Koncepce	koncepce	k1gFnSc1	koncepce
objektů	objekt	k1gInPc2	objekt
jazyka	jazyk	k1gInSc2	jazyk
C	C	kA	C
<g/>
++	++	k?	++
byla	být	k5eAaImAgFnS	být
převzata	převzít	k5eAaPmNgFnS	převzít
z	z	k7c2	z
jazyka	jazyk	k1gInSc2	jazyk
Simula	Simula	k1gFnSc1	Simula
67	[number]	k4	67
<g/>
.	.	kIx.	.
</s>
<s>
Objekty	objekt	k1gInPc1	objekt
(	(	kIx(	(
<g/>
třídy	třída	k1gFnPc1	třída
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
pojaty	pojat	k2eAgFnPc1d1	pojata
jako	jako	k8xS	jako
přirozené	přirozený	k2eAgNnSc1d1	přirozené
rozšíření	rozšíření	k1gNnSc1	rozšíření
datových	datový	k2eAgFnPc2d1	datová
struktur	struktura	k1gFnPc2	struktura
jazyka	jazyk	k1gInSc2	jazyk
C	C	kA	C
o	o	k7c4	o
možnost	možnost	k1gFnSc4	možnost
vkládání	vkládání	k1gNnSc2	vkládání
členských	členský	k2eAgFnPc2d1	členská
funkcí	funkce	k1gFnPc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
C	C	kA	C
<g/>
++	++	k?	++
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
řídit	řídit	k5eAaImF	řídit
viditelnost	viditelnost	k1gFnSc4	viditelnost
složek	složka	k1gFnPc2	složka
objektů	objekt	k1gInPc2	objekt
pro	pro	k7c4	pro
ostatní	ostatní	k2eAgFnPc4d1	ostatní
části	část	k1gFnPc4	část
programu	program	k1gInSc2	program
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
C	C	kA	C
<g/>
++	++	k?	++
existuje	existovat	k5eAaImIp3nS	existovat
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
jiných	jiný	k2eAgInPc2d1	jiný
jazyků	jazyk	k1gInPc2	jazyk
vícenásobná	vícenásobný	k2eAgFnSc1d1	vícenásobná
dědičnost	dědičnost	k1gFnSc1	dědičnost
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
třída	třída	k1gFnSc1	třída
C	C	kA	C
může	moct	k5eAaImIp3nS	moct
dědit	dědit	k5eAaImF	dědit
od	od	k7c2	od
třídy	třída	k1gFnSc2	třída
A	A	kA	A
i	i	k8xC	i
B.	B.	kA	B.
Pro	pro	k7c4	pro
případ	případ	k1gInSc4	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
třídy	třída	k1gFnPc1	třída
B	B	kA	B
a	a	k8xC	a
C	C	kA	C
dědily	dědit	k5eAaImAgInP	dědit
od	od	k7c2	od
A	A	kA	A
a	a	k8xC	a
třída	třída	k1gFnSc1	třída
D	D	kA	D
dědila	dědit	k5eAaImAgFnS	dědit
od	od	k7c2	od
B	B	kA	B
i	i	k8xC	i
C	C	kA	C
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
u	u	k7c2	u
tříd	třída	k1gFnPc2	třída
B	B	kA	B
a	a	k8xC	a
C	C	kA	C
použít	použít	k5eAaPmF	použít
virtuální	virtuální	k2eAgFnPc4d1	virtuální
dědění	dědění	k1gNnPc4	dědění
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
virtuální	virtuální	k2eAgFnSc2d1	virtuální
dědičnosti	dědičnost	k1gFnSc2	dědičnost
má	mít	k5eAaImIp3nS	mít
ještě	ještě	k9	ještě
C	C	kA	C
<g/>
++	++	k?	++
tři	tři	k4xCgInPc1	tři
druhy	druh	k1gInPc1	druh
klasické	klasický	k2eAgFnSc2d1	klasická
dědičnosti	dědičnost	k1gFnSc2	dědičnost
<g/>
:	:	kIx,	:
public	publicum	k1gNnPc2	publicum
(	(	kIx(	(
<g/>
veřejná	veřejný	k2eAgFnSc1d1	veřejná
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
modifikátory	modifikátor	k1gInPc1	modifikátor
oprávnění	oprávnění	k1gNnSc2	oprávnění
zděděných	zděděný	k2eAgFnPc2d1	zděděná
metod	metoda	k1gFnPc2	metoda
a	a	k8xC	a
atributů	atribut	k1gInPc2	atribut
se	se	k3xPyFc4	se
nezmění	změnit	k5eNaPmIp3nS	změnit
protected	protected	k1gInSc1	protected
(	(	kIx(	(
<g/>
chráněná	chráněný	k2eAgFnSc1d1	chráněná
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
veřejné	veřejný	k2eAgFnSc2d1	veřejná
a	a	k8xC	a
chráněné	chráněný	k2eAgFnSc2d1	chráněná
složky	složka	k1gFnSc2	složka
základní	základní	k2eAgFnSc2d1	základní
třídy	třída	k1gFnSc2	třída
budou	být	k5eAaImBp3nP	být
v	v	k7c6	v
odvozené	odvozený	k2eAgFnSc6d1	odvozená
třídě	třída	k1gFnSc6	třída
chráněné	chráněný	k2eAgFnSc2d1	chráněná
private	privat	k1gInSc5	privat
(	(	kIx(	(
<g/>
soukromá	soukromý	k2eAgFnSc1d1	soukromá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
veřejné	veřejný	k2eAgFnSc2d1	veřejná
a	a	k8xC	a
chráněné	chráněný	k2eAgFnSc2d1	chráněná
složky	složka	k1gFnSc2	složka
základní	základní	k2eAgFnSc2d1	základní
třídy	třída	k1gFnSc2	třída
budou	být	k5eAaImBp3nP	být
v	v	k7c6	v
odvozené	odvozený	k2eAgFnSc6d1	odvozená
třídě	třída	k1gFnSc6	třída
sourkomé	sourkomý	k2eAgFnSc2d1	sourkomý
C	C	kA	C
<g/>
++	++	k?	++
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
překrývat	překrývat	k5eAaImF	překrývat
metody	metoda	k1gFnPc4	metoda
v	v	k7c6	v
základních	základní	k2eAgFnPc6d1	základní
třídách	třída	k1gFnPc6	třída
metodami	metoda	k1gFnPc7	metoda
z	z	k7c2	z
rozšířených	rozšířený	k2eAgFnPc2d1	rozšířená
tříd	třída	k1gFnPc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
mechanismus	mechanismus	k1gInSc1	mechanismus
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
polymorfismus	polymorfismus	k1gInSc1	polymorfismus
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
využití	využití	k1gNnSc4	využití
polymorfismu	polymorfismus	k1gInSc2	polymorfismus
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
třídě	třída	k1gFnSc6	třída
u	u	k7c2	u
polymorfické	polymorfický	k2eAgFnSc2d1	polymorfický
metody	metoda	k1gFnSc2	metoda
uvést	uvést	k5eAaPmF	uvést
klíčové	klíčový	k2eAgNnSc4d1	klíčové
slovo	slovo	k1gNnSc4	slovo
virtual	virtuat	k5eAaBmAgMnS	virtuat
<g/>
.	.	kIx.	.
</s>
<s>
Polymorfismus	polymorfismus	k1gInSc1	polymorfismus
je	být	k5eAaImIp3nS	být
podobný	podobný	k2eAgInSc1d1	podobný
přetěžování	přetěžování	k1gNnSc3	přetěžování
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
u	u	k7c2	u
přetěžování	přetěžování	k1gNnSc2	přetěžování
probíhá	probíhat	k5eAaImIp3nS	probíhat
rozhodování	rozhodování	k1gNnSc4	rozhodování
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
metoda	metoda	k1gFnSc1	metoda
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
volat	volat	k5eAaImF	volat
při	při	k7c6	při
překladu	překlad	k1gInSc6	překlad
<g/>
,	,	kIx,	,
u	u	k7c2	u
polymorfismu	polymorfismus	k1gInSc2	polymorfismus
až	až	k6eAd1	až
za	za	k7c2	za
běhu	běh	k1gInSc2	běh
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
RTTI	RTTI	kA	RTTI
<g/>
.	.	kIx.	.
</s>
<s>
Run-Time	Run-Timat	k5eAaPmIp3nS	Run-Timat
Type	typ	k1gInSc5	typ
Information	Information	k1gInSc1	Information
resp.	resp.	kA	resp.
Run-Time	Run-Tim	k1gInSc5	Run-Tim
Type	typ	k1gInSc5	typ
Identification	Identification	k1gInSc4	Identification
je	být	k5eAaImIp3nS	být
způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
jakým	jaký	k3yRgInSc7	jaký
se	se	k3xPyFc4	se
v	v	k7c6	v
C	C	kA	C
<g/>
++	++	k?	++
získávají	získávat	k5eAaImIp3nP	získávat
informace	informace	k1gFnPc1	informace
o	o	k7c6	o
typu	typ	k1gInSc6	typ
objektu	objekt	k1gInSc2	objekt
za	za	k7c2	za
běhu	běh	k1gInSc2	běh
programu	program	k1gInSc2	program
<g/>
.	.	kIx.	.
</s>
<s>
RTTI	RTTI	kA	RTTI
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
hlavních	hlavní	k2eAgFnPc2d1	hlavní
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
operátoru	operátor	k1gInSc2	operátor
informací	informace	k1gFnPc2	informace
o	o	k7c6	o
typu	typ	k1gInSc6	typ
typeid	typeida	k1gFnPc2	typeida
a	a	k8xC	a
operátoru	operátor	k1gInSc2	operátor
přetypování	přetypování	k1gNnSc2	přetypování
dynamic_cast	dynamic_cast	k1gFnSc1	dynamic_cast
<g/>
<	<	kIx(	<
<g/>
>	>	kIx)	>
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
jiných	jiný	k2eAgInPc2d1	jiný
jazyků	jazyk	k1gInPc2	jazyk
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
C	C	kA	C
<g/>
++	++	k?	++
čtyři	čtyři	k4xCgFnPc1	čtyři
metody	metoda	k1gFnPc1	metoda
přetypování	přetypování	k1gNnSc2	přetypování
<g/>
.	.	kIx.	.
</s>
<s>
Statické	statický	k2eAgNnSc1d1	statické
přetypování	přetypování	k1gNnSc1	přetypování
-	-	kIx~	-
static_cast	static_cast	k1gInSc1	static_cast
<g/>
<typ>
(	(	kIx(	(
<g/>
výraz	výraz	k1gInSc1	výraz
<g/>
)	)	kIx)	)
-	-	kIx~	-
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
přetypování	přetypování	k1gNnSc3	přetypování
primitivních	primitivní	k2eAgInPc2d1	primitivní
typů	typ	k1gInPc2	typ
nebo	nebo	k8xC	nebo
rychlému	rychlý	k2eAgNnSc3d1	rychlé
přetypování	přetypování	k1gNnSc3	přetypování
objektů	objekt	k1gInPc2	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
neprobíhají	probíhat	k5eNaImIp3nP	probíhat
při	při	k7c6	při
přetypování	přetypování	k1gNnSc6	přetypování
kontroly	kontrola	k1gFnSc2	kontrola
je	být	k5eAaImIp3nS	být
doporučeno	doporučen	k2eAgNnSc1d1	doporučeno
takto	takto	k6eAd1	takto
přetypovat	přetypovat	k5eAaPmF	přetypovat
pouze	pouze	k6eAd1	pouze
objekty	objekt	k1gInPc4	objekt
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yRgInPc2	který
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
při	při	k7c6	při
kompilaci	kompilace	k1gFnSc6	kompilace
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
lze	lze	k6eAd1	lze
přetypovat	přetypovat	k5eAaPmF	přetypovat
na	na	k7c4	na
cílový	cílový	k2eAgInSc4d1	cílový
typ	typ	k1gInSc4	typ
<g/>
.	.	kIx.	.
</s>
<s>
Dynamické	dynamický	k2eAgNnSc1d1	dynamické
přetypování	přetypování	k1gNnSc1	přetypování
-	-	kIx~	-
dynamic_cast	dynamic_cast	k1gInSc1	dynamic_cast
<g/>
<typ>
(	(	kIx(	(
<g/>
výraz	výraz	k1gInSc1	výraz
<g/>
)	)	kIx)	)
-	-	kIx~	-
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
přetypování	přetypování	k1gNnSc3	přetypování
ukazatelů	ukazatel	k1gInPc2	ukazatel
nebo	nebo	k8xC	nebo
referencí	reference	k1gFnPc2	reference
na	na	k7c4	na
objekty	objekt	k1gInPc4	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
přetypování	přetypování	k1gNnSc6	přetypování
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
kontrolám	kontrola	k1gFnPc3	kontrola
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
chyby	chyba	k1gFnSc2	chyba
při	při	k7c6	při
přetypování	přetypování	k1gNnSc6	přetypování
ukazatelů	ukazatel	k1gInPc2	ukazatel
je	být	k5eAaImIp3nS	být
vrácena	vrácen	k2eAgFnSc1d1	vrácena
hodnota	hodnota	k1gFnSc1	hodnota
NULL	NULL	kA	NULL
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
chyby	chyba	k1gFnSc2	chyba
při	při	k7c6	při
přetypování	přetypování	k1gNnSc6	přetypování
referencí	reference	k1gFnPc2	reference
je	být	k5eAaImIp3nS	být
vyhozena	vyhozen	k2eAgFnSc1d1	vyhozena
výjimka	výjimka	k1gFnSc1	výjimka
std	std	k?	std
<g/>
::	::	k?	::
<g/>
bad_cast	bad_cast	k1gMnSc1	bad_cast
<g/>
.	.	kIx.	.
</s>
<s>
Obecné	obecný	k2eAgNnSc1d1	obecné
přetypování	přetypování	k1gNnSc1	přetypování
-	-	kIx~	-
reinterpret_cast	reinterpret_cast	k1gInSc1	reinterpret_cast
<g/>
<typ>
(	(	kIx(	(
<g/>
výraz	výraz	k1gInSc1	výraz
<g/>
)	)	kIx)	)
-	-	kIx~	-
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
přetypování	přetypování	k1gNnSc3	přetypování
nesouvisejících	související	k2eNgNnPc2d1	nesouvisející
dat	datum	k1gNnPc2	datum
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
pole	pole	k1gNnSc1	pole
znaků	znak	k1gInPc2	znak
je	být	k5eAaImIp3nS	být
takto	takto	k6eAd1	takto
možné	možný	k2eAgNnSc1d1	možné
přetypovat	přetypovat	k5eAaPmF	přetypovat
na	na	k7c4	na
strukturu	struktura	k1gFnSc4	struktura
se	s	k7c7	s
stejným	stejný	k2eAgInSc7d1	stejný
počtem	počet	k1gInSc7	počet
znaků	znak	k1gInPc2	znak
<g/>
.	.	kIx.	.
</s>
<s>
Konstantní	konstantní	k2eAgNnSc1d1	konstantní
přetypování	přetypování	k1gNnSc1	přetypování
-	-	kIx~	-
const_cast	const_cast	k1gInSc1	const_cast
<g/>
<typ>
(	(	kIx(	(
<g/>
výraz	výraz	k1gInSc1	výraz
<g/>
)	)	kIx)	)
-	-	kIx~	-
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
přidávat	přidávat	k5eAaImF	přidávat
a	a	k8xC	a
odebírat	odebírat	k5eAaImF	odebírat
modifikátory	modifikátor	k1gInPc4	modifikátor
const	const	k5eAaPmF	const
a	a	k8xC	a
volatile	volatile	k6eAd1	volatile
<g/>
.	.	kIx.	.
</s>
<s>
C	C	kA	C
<g/>
++	++	k?	++
také	také	k9	také
podporuje	podporovat	k5eAaImIp3nS	podporovat
přetypování	přetypování	k1gNnSc1	přetypování
(	(	kIx(	(
<g/>
typ	typ	k1gInSc1	typ
<g/>
)	)	kIx)	)
<g/>
výraz	výraz	k1gInSc1	výraz
převzaté	převzatý	k2eAgFnSc2d1	převzatá
z	z	k7c2	z
jazyka	jazyk	k1gInSc2	jazyk
C.	C.	kA	C.
Šablony	šablona	k1gFnPc1	šablona
rozšiřují	rozšiřovat	k5eAaImIp3nP	rozšiřovat
znovupoužitelnost	znovupoužitelnost	k1gFnSc4	znovupoužitelnost
kódu	kód	k1gInSc2	kód
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
napsat	napsat	k5eAaPmF	napsat
kód	kód	k1gInSc4	kód
se	s	k7c7	s
zcela	zcela	k6eAd1	zcela
obecným	obecný	k2eAgInSc7d1	obecný
(	(	kIx(	(
<g/>
neurčeným	určený	k2eNgInSc7d1	neurčený
<g/>
)	)	kIx)	)
datovým	datový	k2eAgInSc7d1	datový
typem	typ	k1gInSc7	typ
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgInPc1d1	jiný
jazyky	jazyk	k1gInPc1	jazyk
mohou	moct	k5eAaImIp3nP	moct
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
stejné	stejný	k2eAgFnPc4d1	stejná
funkcionality	funkcionalita	k1gFnPc4	funkcionalita
použitím	použití	k1gNnSc7	použití
kořene	kořen	k1gInSc2	kořen
objektové	objektový	k2eAgFnSc2d1	objektová
hierarchie	hierarchie	k1gFnSc2	hierarchie
nebo	nebo	k8xC	nebo
genericitou	genericita	k1gFnSc7	genericita
<g/>
.	.	kIx.	.
</s>
<s>
Jazyk	jazyk	k1gInSc1	jazyk
C	C	kA	C
<g/>
++	++	k?	++
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
deklarovat	deklarovat	k5eAaBmF	deklarovat
více	hodně	k6eAd2	hodně
funkcí	funkce	k1gFnPc2	funkce
se	s	k7c7	s
stejným	stejný	k2eAgInSc7d1	stejný
názvem	název	k1gInSc7	název
<g/>
.	.	kIx.	.
</s>
<s>
Kompilátor	kompilátor	k1gInSc1	kompilátor
určí	určit	k5eAaPmIp3nS	určit
správné	správný	k2eAgNnSc4d1	správné
volání	volání	k1gNnSc4	volání
podle	podle	k7c2	podle
počtu	počet	k1gInSc2	počet
a	a	k8xC	a
typu	typ	k1gInSc2	typ
argumentů	argument	k1gInPc2	argument
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
technika	technika	k1gFnSc1	technika
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
přetěžování	přetěžování	k1gNnSc1	přetěžování
funkcí	funkce	k1gFnPc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
silnou	silný	k2eAgFnSc7d1	silná
vlastností	vlastnost	k1gFnSc7	vlastnost
jazyka	jazyk	k1gInSc2	jazyk
je	být	k5eAaImIp3nS	být
i	i	k9	i
možnost	možnost	k1gFnSc1	možnost
přetěžovat	přetěžovat	k5eAaImF	přetěžovat
standardní	standardní	k2eAgInPc4d1	standardní
operátory	operátor	k1gInPc4	operátor
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
'	'	kIx"	'
<g/>
+	+	kIx~	+
<g/>
'	'	kIx"	'
nebo	nebo	k8xC	nebo
'	'	kIx"	'
<g/>
=	=	kIx~	=
<g/>
'	'	kIx"	'
<g/>
)	)	kIx)	)
a	a	k8xC	a
tak	tak	k9	tak
přirozeně	přirozeně	k6eAd1	přirozeně
využívat	využívat	k5eAaImF	využívat
tyto	tento	k3xDgInPc4	tento
operátory	operátor	k1gInPc4	operátor
pro	pro	k7c4	pro
nově	nově	k6eAd1	nově
vytvářené	vytvářený	k2eAgFnPc4d1	vytvářená
třídy	třída	k1gFnPc4	třída
a	a	k8xC	a
tvorbu	tvorba	k1gFnSc4	tvorba
abstraktních	abstraktní	k2eAgInPc2d1	abstraktní
datových	datový	k2eAgInPc2d1	datový
typů	typ	k1gInPc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Standard	standard	k1gInSc1	standard
jazyka	jazyk	k1gInSc2	jazyk
C	C	kA	C
<g/>
++	++	k?	++
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
částí	část	k1gFnPc2	část
<g/>
:	:	kIx,	:
popis	popis	k1gInSc1	popis
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
standardní	standardní	k2eAgFnSc2d1	standardní
knihovny	knihovna	k1gFnSc2	knihovna
<g/>
.	.	kIx.	.
</s>
<s>
Standardní	standardní	k2eAgFnSc1d1	standardní
knihovna	knihovna	k1gFnSc1	knihovna
jazyka	jazyk	k1gInSc2	jazyk
C	C	kA	C
<g/>
++	++	k?	++
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
mírně	mírně	k6eAd1	mírně
modifikovanou	modifikovaný	k2eAgFnSc4d1	modifikovaná
verzi	verze	k1gFnSc4	verze
standardní	standardní	k2eAgFnSc2d1	standardní
knihovny	knihovna	k1gFnSc2	knihovna
jazyka	jazyk	k1gInSc2	jazyk
C	C	kA	C
a	a	k8xC	a
Standard	standard	k1gInSc1	standard
Template	Templat	k1gInSc5	Templat
Library	Librar	k1gMnPc7	Librar
(	(	kIx(	(
<g/>
STL	STL	kA	STL
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
STL	STL	kA	STL
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
užitečných	užitečný	k2eAgFnPc2d1	užitečná
datových	datový	k2eAgFnPc2d1	datová
struktur	struktura	k1gFnPc2	struktura
a	a	k8xC	a
algoritmů	algoritmus	k1gInPc2	algoritmus
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
vektory	vektor	k1gInPc1	vektor
(	(	kIx(	(
<g/>
dynamické	dynamický	k2eAgNnSc4d1	dynamické
pole	pole	k1gNnSc4	pole
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spojové	spojový	k2eAgInPc1d1	spojový
seznamy	seznam	k1gInPc1	seznam
<g/>
,	,	kIx,	,
iterátory	iterátor	k1gInPc1	iterátor
<g/>
,	,	kIx,	,
zobecněné	zobecněný	k2eAgInPc1d1	zobecněný
ukazatele	ukazatel	k1gInPc1	ukazatel
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
multi	mulť	k1gFnPc1	mulť
<g/>
)	)	kIx)	)
<g/>
mapy	mapa	k1gFnPc1	mapa
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
multi	mult	k5eAaBmF	mult
<g/>
)	)	kIx)	)
<g/>
sety	set	k1gInPc4	set
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
tyto	tento	k3xDgFnPc1	tento
struktury	struktura	k1gFnPc1	struktura
mají	mít	k5eAaImIp3nP	mít
konzistentní	konzistentní	k2eAgNnSc4d1	konzistentní
rozhraní	rozhraní	k1gNnSc4	rozhraní
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
použitím	použití	k1gNnSc7	použití
šablon	šablona	k1gFnPc2	šablona
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
možné	možný	k2eAgNnSc1d1	možné
programovat	programovat	k5eAaImF	programovat
generické	generický	k2eAgInPc4d1	generický
algoritmy	algoritmus	k1gInPc4	algoritmus
schopné	schopný	k2eAgNnSc1d1	schopné
pracovat	pracovat	k5eAaImF	pracovat
s	s	k7c7	s
kterýmkoliv	kterýkoliv	k3yIgInSc7	kterýkoliv
kontejnerem	kontejner	k1gInSc7	kontejner
nebo	nebo	k8xC	nebo
sekvencí	sekvence	k1gFnSc7	sekvence
definovanou	definovaný	k2eAgFnSc7d1	definovaná
iterátory	iterátor	k1gMnPc7	iterátor
<g/>
.	.	kIx.	.
</s>
<s>
Používání	používání	k1gNnSc1	používání
standardní	standardní	k2eAgFnSc2d1	standardní
knihovny	knihovna	k1gFnSc2	knihovna
-	-	kIx~	-
například	například	k6eAd1	například
používání	používání	k1gNnSc1	používání
std	std	k?	std
<g/>
::	::	k?	::
<g/>
vector	vector	k1gMnSc1	vector
nebo	nebo	k8xC	nebo
std	std	k?	std
<g/>
::	::	k?	::
<g/>
string	string	k1gInSc1	string
místo	místo	k1gNnSc1	místo
polí	pole	k1gFnPc2	pole
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
jazyka	jazyk	k1gInSc2	jazyk
C	C	kA	C
-	-	kIx~	-
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
bezpečnějšímu	bezpečný	k2eAgMnSc3d2	bezpečnější
a	a	k8xC	a
lépe	dobře	k6eAd2	dobře
škálovatelnému	škálovatelný	k2eAgInSc3d1	škálovatelný
softwaru	software	k1gInSc3	software
<g/>
.	.	kIx.	.
</s>
<s>
STL	STL	kA	STL
byla	být	k5eAaImAgFnS	být
původně	původně	k6eAd1	původně
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
a	a	k8xC	a
používána	používat	k5eAaImNgFnS	používat
firmou	firma	k1gFnSc7	firma
Hewlett-Packard	Hewlett-Packarda	k1gFnPc2	Hewlett-Packarda
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
také	také	k9	také
Silicon	Silicon	kA	Silicon
Graphics	Graphics	k1gInSc1	Graphics
<g/>
.	.	kIx.	.
</s>
<s>
Standard	standard	k1gInSc1	standard
se	se	k3xPyFc4	se
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
neodkazuje	odkazovat	k5eNaImIp3nS	odkazovat
jako	jako	k9	jako
na	na	k7c4	na
"	"	kIx"	"
<g/>
STL	STL	kA	STL
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
jako	jako	k9	jako
na	na	k7c4	na
část	část	k1gFnSc4	část
standardní	standardní	k2eAgFnSc2d1	standardní
knihovny	knihovna	k1gFnSc2	knihovna
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
stále	stále	k6eAd1	stále
používá	používat	k5eAaImIp3nS	používat
tento	tento	k3xDgInSc1	tento
pojem	pojem	k1gInSc1	pojem
na	na	k7c4	na
odlišení	odlišení	k1gNnSc4	odlišení
od	od	k7c2	od
ostatních	ostatní	k2eAgFnPc2d1	ostatní
částí	část	k1gFnPc2	část
knihovny	knihovna	k1gFnSc2	knihovna
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
překladačů	překladač	k1gMnPc2	překladač
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
implementaci	implementace	k1gFnSc4	implementace
standardu	standard	k1gInSc2	standard
C	C	kA	C
<g/>
++	++	k?	++
včetně	včetně	k7c2	včetně
STL	STL	kA	STL
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
také	také	k9	také
implementace	implementace	k1gFnSc2	implementace
standardu	standard	k1gInSc2	standard
nezávislé	závislý	k2eNgFnPc4d1	nezávislá
na	na	k7c6	na
kompilátoru	kompilátor	k1gInSc6	kompilátor
(	(	kIx(	(
<g/>
např.	např.	kA	např.
STLPort	STLPort	k1gInSc1	STLPort
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgInPc1d1	jiný
projekty	projekt	k1gInPc1	projekt
také	také	k9	také
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
různé	různý	k2eAgFnPc4d1	různá
zákaznické	zákaznický	k2eAgFnPc4d1	zákaznická
implementace	implementace	k1gFnPc4	implementace
knihovny	knihovna	k1gFnSc2	knihovna
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
STL	STL	kA	STL
s	s	k7c7	s
různými	různý	k2eAgInPc7d1	různý
cíli	cíl	k1gInPc7	cíl
návrhu	návrh	k1gInSc2	návrh
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnSc1d1	následující
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
aplikace	aplikace	k1gFnSc1	aplikace
vypíše	vypsat	k5eAaPmIp3nS	vypsat
"	"	kIx"	"
<g/>
Hello	Hello	k1gNnSc1	Hello
<g/>
,	,	kIx,	,
world	world	k1gInSc1	world
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
na	na	k7c4	na
standardní	standardní	k2eAgInSc4d1	standardní
výstup	výstup	k1gInSc4	výstup
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
aplikace	aplikace	k1gFnSc1	aplikace
vypíše	vypsat	k5eAaPmIp3nS	vypsat
všechny	všechen	k3xTgInPc4	všechen
argumenty	argument	k1gInPc4	argument
programu	program	k1gInSc2	program
zadané	zadaná	k1gFnSc2	zadaná
při	při	k7c6	při
spuštění	spuštění	k1gNnSc6	spuštění
Pro	pro	k7c4	pro
zkrácení	zkrácení	k1gNnSc4	zkrácení
kódu	kód	k1gInSc2	kód
je	být	k5eAaImIp3nS	být
uvedena	uveden	k2eAgFnSc1d1	uvedena
deklarace	deklarace	k1gFnSc1	deklarace
a	a	k8xC	a
definice	definice	k1gFnSc1	definice
tříd	třída	k1gFnPc2	třída
najednou	najednou	k6eAd1	najednou
<g/>
.	.	kIx.	.
</s>
<s>
Správně	správně	k6eAd1	správně
by	by	kYmCp3nS	by
deklarace	deklarace	k1gFnSc1	deklarace
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
v	v	k7c6	v
hlavičkovém	hlavičkový	k2eAgInSc6d1	hlavičkový
souboru	soubor	k1gInSc6	soubor
<g/>
,	,	kIx,	,
definice	definice	k1gFnSc1	definice
v	v	k7c6	v
souboru	soubor	k1gInSc6	soubor
s	s	k7c7	s
kódem	kód	k1gInSc7	kód
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
je	být	k5eAaImIp3nS	být
doporučeno	doporučen	k2eAgNnSc1d1	doporučeno
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
každá	každý	k3xTgFnSc1	každý
třída	třída	k1gFnSc1	třída
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
samostatném	samostatný	k2eAgInSc6d1	samostatný
souboru	soubor	k1gInSc6	soubor
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
standardní	standardní	k2eAgFnSc6d1	standardní
knihovně	knihovna	k1gFnSc6	knihovna
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
algoritmy	algoritmus	k1gInPc4	algoritmus
pro	pro	k7c4	pro
práci	práce	k1gFnSc4	práce
s	s	k7c7	s
datovými	datový	k2eAgFnPc7d1	datová
strukturami	struktura	k1gFnPc7	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
příkladu	příklad	k1gInSc6	příklad
je	být	k5eAaImIp3nS	být
pomocí	pomocí	k7c2	pomocí
standardních	standardní	k2eAgInPc2d1	standardní
algoritmů	algoritmus	k1gInPc2	algoritmus
vygenerována	vygenerován	k2eAgFnSc1d1	vygenerována
a	a	k8xC	a
vypsána	vypsán	k2eAgFnSc1d1	vypsána
Fibonacciho	Fibonacci	k1gMnSc2	Fibonacci
posloupnost	posloupnost	k1gFnSc4	posloupnost
<g/>
.	.	kIx.	.
</s>
