<s>
Galileo	Galilea	k1gFnSc5	Galilea
Galilei	Galile	k1gFnSc5	Galile
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1564	[number]	k4	1564
<g/>
,	,	kIx,	,
Pisa	Pisa	k1gFnSc1	Pisa
–	–	k?	–
8	[number]	k4	8
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1642	[number]	k4	1642
<g/>
,	,	kIx,	,
Arcetri	Arcetre	k1gFnSc4	Arcetre
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
toskánský	toskánský	k2eAgMnSc1d1	toskánský
astronom	astronom	k1gMnSc1	astronom
<g/>
,	,	kIx,	,
filosof	filosof	k1gMnSc1	filosof
a	a	k8xC	a
fyzik	fyzik	k1gMnSc1	fyzik
těsně	těsně	k6eAd1	těsně
spjatý	spjatý	k2eAgMnSc1d1	spjatý
s	s	k7c7	s
vědeckou	vědecký	k2eAgFnSc7d1	vědecká
revolucí	revoluce	k1gFnSc7	revoluce
<g/>
.	.	kIx.	.
</s>
