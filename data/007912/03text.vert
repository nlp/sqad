<s>
Karel	Karel	k1gMnSc1	Karel
je	být	k5eAaImIp3nS	být
celoevropsky	celoevropsky	k6eAd1	celoevropsky
rozšířené	rozšířený	k2eAgNnSc1d1	rozšířené
velice	velice	k6eAd1	velice
oblíbené	oblíbený	k2eAgNnSc1d1	oblíbené
mužské	mužský	k2eAgNnSc1d1	mužské
jméno	jméno	k1gNnSc1	jméno
<g/>
,	,	kIx,	,
používané	používaný	k2eAgInPc4d1	používaný
i	i	k8xC	i
hlavami	hlava	k1gFnPc7	hlava
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Karel	Karla	k1gFnPc2	Karla
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
z	z	k7c2	z
germánského	germánský	k2eAgNnSc2d1	germánské
slova	slovo	k1gNnSc2	slovo
karlaz	karlaz	k1gInSc1	karlaz
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
svobodný	svobodný	k2eAgMnSc1d1	svobodný
muž	muž	k1gMnSc1	muž
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
staroangličtině	staroangličtina	k1gFnSc6	staroangličtina
přežilo	přežít	k5eAaPmAgNnS	přežít
jako	jako	k9	jako
churl	churl	k1gInSc4	churl
(	(	kIx(	(
<g/>
staroanglicky	staroanglicky	k6eAd1	staroanglicky
"	"	kIx"	"
<g/>
ċ	ċ	k?	ċ
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
slávy	sláva	k1gFnSc2	sláva
císaře	císař	k1gMnSc2	císař
Karla	Karel	k1gMnSc2	Karel
Velikého	veliký	k2eAgMnSc2d1	veliký
se	se	k3xPyFc4	se
velice	velice	k6eAd1	velice
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
a	a	k8xC	a
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
,	,	kIx,	,
polštině	polština	k1gFnSc6	polština
<g/>
,	,	kIx,	,
chorvatštině	chorvatština	k1gFnSc6	chorvatština
<g/>
,	,	kIx,	,
srbštině	srbština	k1gFnSc3	srbština
a	a	k8xC	a
maďarštině	maďarština	k1gFnSc3	maďarština
zdruhovělo	zdruhovět	k5eAaPmAgNnS	zdruhovět
<g/>
:	:	kIx,	:
označuje	označovat	k5eAaImIp3nS	označovat
krále	král	k1gMnPc4	král
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
slaví	slavit	k5eAaImIp3nP	slavit
Karlové	Karel	k1gMnPc1	Karel
svátek	svátek	k1gInSc4	svátek
4	[number]	k4	4
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
nebo	nebo	k8xC	nebo
podle	podle	k7c2	podle
staršího	starý	k2eAgInSc2d2	starší
kalendáře	kalendář	k1gInSc2	kalendář
28	[number]	k4	28
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
<g/>
.	.	kIx.	.
</s>
<s>
Ženská	ženský	k2eAgFnSc1d1	ženská
podoba	podoba	k1gFnSc1	podoba
jména	jméno	k1gNnSc2	jméno
je	být	k5eAaImIp3nS	být
Karla	Karla	k1gFnSc1	Karla
neboli	neboli	k8xC	neboli
Karlička	Karlička	k1gFnSc1	Karlička
a	a	k8xC	a
Karolína	Karolína	k1gFnSc1	Karolína
<g/>
.	.	kIx.	.
</s>
<s>
Domácká	domácký	k2eAgFnSc1d1	domácká
podoba	podoba	k1gFnSc1	podoba
jména	jméno	k1gNnPc4	jméno
také	také	k9	také
Kája	Kája	k?	Kája
<g/>
,	,	kIx,	,
Karlík	Karlík	k1gMnSc1	Karlík
nebo	nebo	k8xC	nebo
Karlíček	Karlíček	k1gMnSc1	Karlíček
<g/>
.	.	kIx.	.
</s>
<s>
Kája	Kája	k?	Kája
<g/>
,	,	kIx,	,
Karlík	Karlík	k1gMnSc1	Karlík
<g/>
,	,	kIx,	,
Karlíček	Karlíček	k1gMnSc1	Karlíček
<g/>
,	,	kIx,	,
Karlos	Karlos	k1gMnSc1	Karlos
<g/>
,	,	kIx,	,
Kódl	Kódl	k1gInSc4	Kódl
Následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
uvádí	uvádět	k5eAaImIp3nS	uvádět
četnost	četnost	k1gFnSc4	četnost
jména	jméno	k1gNnSc2	jméno
v	v	k7c6	v
ČR	ČR	kA	ČR
a	a	k8xC	a
pořadí	pořadí	k1gNnSc2	pořadí
mezi	mezi	k7c7	mezi
mužskými	mužský	k2eAgNnPc7d1	mužské
jmény	jméno	k1gNnPc7	jméno
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
roků	rok	k1gInPc2	rok
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yQgInPc4	který
jsou	být	k5eAaImIp3nP	být
dostupné	dostupný	k2eAgInPc1d1	dostupný
údaje	údaj	k1gInPc1	údaj
MV	MV	kA	MV
ČR	ČR	kA	ČR
–	–	k?	–
lze	lze	k6eAd1	lze
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
tedy	tedy	k9	tedy
vysledovat	vysledovat	k5eAaPmF	vysledovat
trend	trend	k1gInSc4	trend
v	v	k7c6	v
užívání	užívání	k1gNnSc6	užívání
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
<g/>
:	:	kIx,	:
Změna	změna	k1gFnSc1	změna
procentního	procentní	k2eAgNnSc2d1	procentní
zastoupení	zastoupení	k1gNnSc2	zastoupení
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
mezi	mezi	k7c7	mezi
žijícími	žijící	k2eAgMnPc7d1	žijící
muži	muž	k1gMnPc7	muž
v	v	k7c6	v
ČR	ČR	kA	ČR
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
procentní	procentní	k2eAgFnSc1d1	procentní
změna	změna	k1gFnSc1	změna
se	se	k3xPyFc4	se
započítáním	započítání	k1gNnSc7	započítání
celkového	celkový	k2eAgInSc2d1	celkový
úbytku	úbytek	k1gInSc2	úbytek
mužů	muž	k1gMnPc2	muž
v	v	k7c6	v
ČR	ČR	kA	ČR
za	za	k7c4	za
sledované	sledovaný	k2eAgInPc4d1	sledovaný
roky	rok	k1gInPc4	rok
1999	[number]	k4	1999
<g/>
–	–	k?	–
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
−	−	k?	−
%	%	kIx~	%
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
poměrně	poměrně	k6eAd1	poměrně
značném	značný	k2eAgInSc6d1	značný
úbytku	úbytek	k1gInSc6	úbytek
obliby	obliba	k1gFnSc2	obliba
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
údajů	údaj	k1gInPc2	údaj
ČSÚ	ČSÚ	kA	ČSÚ
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
34	[number]	k4	34
<g/>
.	.	kIx.	.
nejčastější	častý	k2eAgNnSc1d3	nejčastější
mužské	mužský	k2eAgNnSc1d1	mužské
jméno	jméno	k1gNnSc1	jméno
u	u	k7c2	u
novorozenců	novorozenec	k1gMnPc2	novorozenec
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
klesla	klesnout	k5eAaPmAgFnS	klesnout
oblíbenost	oblíbenost	k1gFnSc1	oblíbenost
na	na	k7c4	na
39	[number]	k4	39
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
nově	nově	k6eAd1	nově
narozených	narozený	k2eAgMnPc2d1	narozený
Karlů	Karel	k1gMnPc2	Karel
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
dat	datum	k1gNnPc2	datum
portálu	portál	k1gInSc2	portál
Kdejsme	Kdejsme	k1gFnSc2	Kdejsme
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
2929	[number]	k4	2929
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
pouze	pouze	k6eAd1	pouze
288	[number]	k4	288
novorozenců	novorozenec	k1gMnPc2	novorozenec
bylo	být	k5eAaImAgNnS	být
pojmenováno	pojmenovat	k5eAaPmNgNnS	pojmenovat
Karel	Karel	k1gMnSc1	Karel
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
činí	činit	k5eAaImIp3nS	činit
pokles	pokles	k1gInSc4	pokles
o	o	k7c4	o
-90,17	-90,17	k4	-90,17
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Carl	Carl	k1gMnSc1	Carl
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Carles	Carles	k1gInSc1	Carles
(	(	kIx(	(
<g/>
katalánsky	katalánsky	k6eAd1	katalánsky
<g/>
)	)	kIx)	)
Carlo	Carlo	k1gNnSc4	Carlo
(	(	kIx(	(
<g/>
italsky	italsky	k6eAd1	italsky
<g/>
)	)	kIx)	)
Carlos	Carlos	k1gMnSc1	Carlos
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
<g/>
,	,	kIx,	,
portugalsky	portugalsky	k6eAd1	portugalsky
<g/>
)	)	kIx)	)
Charles	Charles	k1gMnSc1	Charles
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
,	,	kIx,	,
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
Charlie	Charlie	k1gMnSc1	Charlie
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Karl	Karl	k1gMnSc1	Karl
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
,	,	kIx,	,
švédsky	švédsky	k6eAd1	švédsky
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
dánsky	dánsky	k6eAd1	dánsky
<g/>
)	)	kIx)	)
Karol	Karol	k1gInSc1	Karol
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
<g/>
,	,	kIx,	,
slovensky	slovensky	k6eAd1	slovensky
<g/>
)	)	kIx)	)
Karel	Karel	k1gMnSc1	Karel
(	(	kIx(	(
<g/>
nizozemsky	nizozemsky	k6eAd1	nizozemsky
<g/>
)	)	kIx)	)
Karlo	Karla	k1gFnSc5	Karla
(	(	kIx(	(
<g/>
chorvatsky	chorvatsky	k6eAd1	chorvatsky
<g/>
)	)	kIx)	)
Karel	Karel	k1gMnSc1	Karel
Veliký	veliký	k2eAgMnSc1d1	veliký
(	(	kIx(	(
<g/>
742	[number]	k4	742
<g/>
–	–	k?	–
<g/>
814	[number]	k4	814
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
král	král	k1gMnSc1	král
a	a	k8xC	a
císař	císař	k1gMnSc1	císař
Karel	Karel	k1gMnSc1	Karel
I.	I.	kA	I.
Flanderský	flanderský	k2eAgMnSc1d1	flanderský
(	(	kIx(	(
<g/>
1084	[number]	k4	1084
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
–	–	k?	–
<g/>
1127	[number]	k4	1127
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hrabě	hrabě	k1gMnSc1	hrabě
flanderský	flanderský	k2eAgMnSc1d1	flanderský
Karel	Karel	k1gMnSc1	Karel
Boromejský	Boromejský	k2eAgMnSc1d1	Boromejský
(	(	kIx(	(
<g/>
1538	[number]	k4	1538
<g/>
–	–	k?	–
<g/>
1584	[number]	k4	1584
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Evžen	Evžen	k1gMnSc1	Evžen
z	z	k7c2	z
Mazenodu	Mazenod	k1gInSc2	Mazenod
(	(	kIx(	(
<g/>
1782	[number]	k4	1782
<g/>
–	–	k?	–
<g/>
1861	[number]	k4	1861
<g/>
)	)	kIx)	)
Karl	Karl	k1gMnSc1	Karl
Leisner	Leisner	k1gMnSc1	Leisner
(	(	kIx(	(
<g/>
1915	[number]	k4	1915
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německý	německý	k2eAgInSc1d1	německý
<g />
.	.	kIx.	.
</s>
<s>
kněz	kněz	k1gMnSc1	kněz
<g/>
,	,	kIx,	,
umučený	umučený	k2eAgMnSc1d1	umučený
v	v	k7c6	v
Dachau	Dachaus	k1gInSc6	Dachaus
<g/>
;	;	kIx,	;
blahořečen	blahořečen	k2eAgInSc4d1	blahořečen
1966	[number]	k4	1966
Karel	Karla	k1gFnPc2	Karla
Lwanga	Lwanga	k1gFnSc1	Lwanga
((	((	k?	((
<g/>
1860	[number]	k4	1860
nebo	nebo	k8xC	nebo
1865	[number]	k4	1865
–	–	k?	–
1886	[number]	k4	1886
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
katolický	katolický	k2eAgMnSc1d1	katolický
křesťan	křesťan	k1gMnSc1	křesťan
<g/>
,	,	kIx,	,
upálený	upálený	k2eAgMnSc1d1	upálený
v	v	k7c6	v
Ugandě	Uganda	k1gFnSc6	Uganda
Karel	Karel	k1gMnSc1	Karel
Veliký	veliký	k2eAgMnSc1d1	veliký
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
franský	franský	k2eAgMnSc1d1	franský
(	(	kIx(	(
<g/>
768	[number]	k4	768
<g/>
–	–	k?	–
<g/>
814	[number]	k4	814
<g/>
)	)	kIx)	)
a	a	k8xC	a
římský	římský	k2eAgMnSc1d1	římský
císař	císař	k1gMnSc1	císař
(	(	kIx(	(
<g/>
800	[number]	k4	800
<g/>
–	–	k?	–
<g/>
814	[number]	k4	814
<g/>
)	)	kIx)	)
Karel	Karel	k1gMnSc1	Karel
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Holý	Holý	k1gMnSc1	Holý
<g/>
,	,	kIx,	,
římský	římský	k2eAgMnSc1d1	římský
císař	císař	k1gMnSc1	císař
(	(	kIx(	(
<g/>
875	[number]	k4	875
<g/>
–	–	k?	–
<g/>
877	[number]	k4	877
<g/>
)	)	kIx)	)
a	a	k8xC	a
první	první	k4xOgMnSc1	první
západofranský	západofranský	k2eAgMnSc1d1	západofranský
král	král	k1gMnSc1	král
Karel	Karel	k1gMnSc1	Karel
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Tlustý	tlustý	k2eAgMnSc1d1	tlustý
<g/>
,	,	kIx,	,
římský	římský	k2eAgMnSc1d1	římský
císař	císař	k1gMnSc1	císař
(	(	kIx(	(
<g/>
876	[number]	k4	876
<g/>
–	–	k?	–
<g/>
887	[number]	k4	887
<g/>
,	,	kIx,	,
†	†	k?	†
<g/>
888	[number]	k4	888
<g/>
)	)	kIx)	)
Karel	Karel	k1gMnSc1	Karel
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Francouzský	francouzský	k2eAgMnSc1d1	francouzský
<g/>
,	,	kIx,	,
západofranský	západofranský	k2eAgMnSc1d1	západofranský
král	král	k1gMnSc1	král
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
římský	římský	k2eAgMnSc1d1	římský
císař	císař	k1gMnSc1	císař
(	(	kIx(	(
<g/>
1355	[number]	k4	1355
<g/>
–	–	k?	–
<g/>
1378	[number]	k4	1378
<g/>
)	)	kIx)	)
Karel	Karel	k1gMnSc1	Karel
V.	V.	kA	V.
<g/>
,	,	kIx,	,
římský	římský	k2eAgMnSc1d1	římský
císař	císař	k1gMnSc1	císař
(	(	kIx(	(
<g/>
1519	[number]	k4	1519
<g/>
–	–	k?	–
<g/>
1556	[number]	k4	1556
<g/>
,	,	kIx,	,
†	†	k?	†
<g/>
1558	[number]	k4	1558
<g/>
)	)	kIx)	)
Karel	Karel	k1gMnSc1	Karel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
římský	římský	k2eAgMnSc1d1	římský
císař	císař	k1gMnSc1	císař
(	(	kIx(	(
<g/>
1711	[number]	k4	1711
<g/>
–	–	k?	–
<g/>
1740	[number]	k4	1740
<g/>
)	)	kIx)	)
Karel	Karel	k1gMnSc1	Karel
VII	VII	kA	VII
<g/>
.	.	kIx.	.
</s>
<s>
Albrecht	Albrecht	k1gMnSc1	Albrecht
<g/>
,	,	kIx,	,
římský	římský	k2eAgMnSc1d1	římský
císař	císař	k1gMnSc1	císař
(	(	kIx(	(
<g/>
1742	[number]	k4	1742
<g/>
–	–	k?	–
<g/>
1745	[number]	k4	1745
<g/>
)	)	kIx)	)
Karel	Karel	k1gMnSc1	Karel
I.	I.	kA	I.
(	(	kIx(	(
<g/>
1887	[number]	k4	1887
<g/>
–	–	k?	–
<g/>
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
císař	císař	k1gMnSc1	císař
rakouský	rakouský	k2eAgMnSc1d1	rakouský
a	a	k8xC	a
jako	jako	k9	jako
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
král	král	k1gMnSc1	král
uherský	uherský	k2eAgMnSc1d1	uherský
(	(	kIx(	(
<g/>
1916	[number]	k4	1916
<g/>
–	–	k?	–
<g/>
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
Karel	Karel	k1gMnSc1	Karel
z	z	k7c2	z
Provence	Provence	k1gFnSc2	Provence
(	(	kIx(	(
<g/>
asi	asi	k9	asi
845	[number]	k4	845
–	–	k?	–
863	[number]	k4	863
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
Burgundska	Burgundsko	k1gNnSc2	Burgundsko
a	a	k8xC	a
Provence	Provence	k1gFnSc2	Provence
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Španělský	španělský	k2eAgMnSc1d1	španělský
(	(	kIx(	(
<g/>
1748	[number]	k4	1748
<g/>
-	-	kIx~	-
<g/>
1819	[number]	k4	1819
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
španělský	španělský	k2eAgMnSc1d1	španělský
král	král	k1gMnSc1	král
angličtí	anglický	k2eAgMnPc1d1	anglický
Karel	Karel	k1gMnSc1	Karel
I.	I.	kA	I.
Stuart	Stuart	k1gInSc1	Stuart
(	(	kIx(	(
<g/>
1600	[number]	k4	1600
<g/>
–	–	k?	–
<g/>
1649	[number]	k4	1649
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
Irska	Irsko	k1gNnSc2	Irsko
Karel	Karla	k1gFnPc2	Karla
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Stuart	Stuart	k1gInSc1	Stuart
(	(	kIx(	(
<g/>
1630	[number]	k4	1630
<g/>
–	–	k?	–
<g/>
1685	[number]	k4	1685
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
Irska	Irsko	k1gNnSc2	Irsko
Karel	Karel	k1gMnSc1	Karel
Edvard	Edvard	k1gMnSc1	Edvard
(	(	kIx(	(
<g/>
1720	[number]	k4	1720
<g/>
–	–	k?	–
<g/>
1788	[number]	k4	1788
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pretendent	pretendent	k1gMnSc1	pretendent
anglický	anglický	k2eAgMnSc1d1	anglický
Charles	Charles	k1gMnSc1	Charles
<g/>
,	,	kIx,	,
princ	princ	k1gMnSc1	princ
z	z	k7c2	z
Walesu	Wales	k1gInSc2	Wales
(	(	kIx(	(
<g/>
*	*	kIx~	*
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
současný	současný	k2eAgMnSc1d1	současný
následník	následník	k1gMnSc1	následník
trůnu	trůn	k1gInSc2	trůn
švédští	švédský	k2eAgMnPc1d1	švédský
<g />
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
X.	X.	kA	X.
Gustav	Gustav	k1gMnSc1	Gustav
(	(	kIx(	(
<g/>
1622	[number]	k4	1622
<g/>
-	-	kIx~	-
<g/>
1660	[number]	k4	1660
<g/>
)	)	kIx)	)
Karel	Karel	k1gMnSc1	Karel
XI	XI	kA	XI
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1655	[number]	k4	1655
<g/>
-	-	kIx~	-
<g/>
1697	[number]	k4	1697
<g/>
)	)	kIx)	)
Karel	Karel	k1gMnSc1	Karel
XII	XII	kA	XII
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1682	[number]	k4	1682
<g/>
-	-	kIx~	-
<g/>
1718	[number]	k4	1718
<g/>
)	)	kIx)	)
Karel	Karel	k1gMnSc1	Karel
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1648	[number]	k4	1648
<g/>
-	-	kIx~	-
<g/>
1718	[number]	k4	1718
<g/>
)	)	kIx)	)
Karel	Karel	k1gMnSc1	Karel
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1763	[number]	k4	1763
<g/>
-	-	kIx~	-
<g/>
1844	[number]	k4	1844
<g/>
)	)	kIx)	)
Karel	Karel	k1gMnSc1	Karel
XV	XV	kA	XV
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1826	[number]	k4	1826
<g/>
-	-	kIx~	-
<g/>
1872	[number]	k4	1872
<g/>
)	)	kIx)	)
Karel	Karel	k1gMnSc1	Karel
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
</s>
<s>
Gustav	Gustav	k1gMnSc1	Gustav
(	(	kIx(	(
<g/>
*	*	kIx~	*
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
Karel	Karel	k1gMnSc1	Karel
Bedřich	Bedřich	k1gMnSc1	Bedřich
(	(	kIx(	(
<g/>
1728	[number]	k4	1728
<g/>
–	–	k?	–
<g/>
1811	[number]	k4	1811
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
velkovévoda	velkovévoda	k1gMnSc1	velkovévoda
badenský	badenský	k2eAgMnSc1d1	badenský
Karel	Karel	k1gMnSc1	Karel
Bedřich	Bedřich	k1gMnSc1	Bedřich
August	August	k1gMnSc1	August
Vilém	Vilém	k1gMnSc1	Vilém
(	(	kIx(	(
<g/>
1804	[number]	k4	1804
<g/>
–	–	k?	–
<g/>
1873	[number]	k4	1873
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vévoda	vévoda	k1gMnSc1	vévoda
brunšvický	brunšvický	k2eAgMnSc1d1	brunšvický
Karel	Karel	k1gMnSc1	Karel
I.	I.	kA	I.
Flanderský	flanderský	k2eAgMnSc1d1	flanderský
(	(	kIx(	(
<g/>
1084	[number]	k4	1084
<g/>
?	?	kIx.	?
</s>
<s>
–	–	k?	–
<g/>
1127	[number]	k4	1127
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
flanderský	flanderský	k2eAgMnSc1d1	flanderský
hrabě	hrabě	k1gMnSc1	hrabě
Karel	Karel	k1gMnSc1	Karel
Smělý	Smělý	k1gMnSc1	Smělý
(	(	kIx(	(
<g/>
1433	[number]	k4	1433
<g/>
–	–	k?	–
<g/>
1477	[number]	k4	1477
<g/>
)	)	kIx)	)
–	–	k?	–
vévoda	vévoda	k1gMnSc1	vévoda
burgundský	burgundský	k2eAgMnSc1d1	burgundský
Karel	Karel	k1gMnSc1	Karel
Vilém	Vilém	k1gMnSc1	Vilém
(	(	kIx(	(
<g/>
1679	[number]	k4	1679
<g/>
–	–	k?	–
<g/>
1738	[number]	k4	1738
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
markrabí	markrabí	k1gMnSc1	markrabí
badensko-durlašský	badenskourlašský	k2eAgMnSc1d1	badensko-durlašský
Karel	Karel	k1gMnSc1	Karel
Vilém	Vilém	k1gMnSc1	Vilém
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
(	(	kIx(	(
<g/>
1735	[number]	k4	1735
<g/>
-	-	kIx~	-
<g/>
1806	[number]	k4	1806
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
vévoda	vévoda	k1gMnSc1	vévoda
brunšvický	brunšvický	k2eAgMnSc1d1	brunšvický
Karel	Karel	k1gMnSc1	Karel
<g/>
,	,	kIx,	,
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Württembergu	Württemberg	k1gInSc2	Württemberg
(	(	kIx(	(
<g/>
*	*	kIx~	*
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
současná	současný	k2eAgFnSc1d1	současná
hlava	hlava	k1gFnSc1	hlava
dynastie	dynastie	k1gFnSc2	dynastie
Württemberků	Württemberka	k1gMnPc2	Württemberka
Charles	Charles	k1gMnSc1	Charles
Montgomery	Montgomera	k1gFnSc2	Montgomera
Burns	Burns	k1gInSc1	Burns
Carl	Carl	k1gMnSc1	Carl
Carlson	Carlson	k1gMnSc1	Carlson
Karel	Karel	k1gMnSc1	Karel
I.	I.	kA	I.
<g/>
,	,	kIx,	,
rozcestník	rozcestník	k1gInSc1	rozcestník
Karel	Karel	k1gMnSc1	Karel
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
rozcestník	rozcestník	k1gInSc1	rozcestník
Karel	Karel	k1gMnSc1	Karel
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
rozcestník	rozcestník	k1gInSc1	rozcestník
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
rozcestník	rozcestník	k1gInSc1	rozcestník
Karel	Karel	k1gMnSc1	Karel
V.	V.	kA	V.
<g/>
,	,	kIx,	,
rozcestník	rozcestník	k1gInSc1	rozcestník
Karel	Karel	k1gMnSc1	Karel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
rozcestník	rozcestník	k1gInSc1	rozcestník
Karel	Karel	k1gMnSc1	Karel
VII	VII	kA	VII
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
rozcestník	rozcestník	k1gInSc1	rozcestník
Karel	Karel	k1gMnSc1	Karel
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
rozcestník	rozcestník	k1gInSc1	rozcestník
Karel	Karel	k1gMnSc1	Karel
IX	IX	kA	IX
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
rozcestník	rozcestník	k1gInSc1	rozcestník
Karel	Karel	k1gMnSc1	Karel
X.	X.	kA	X.
<g/>
,	,	kIx,	,
rozcestník	rozcestník	k1gInSc1	rozcestník
Karel	Karel	k1gMnSc1	Karel
(	(	kIx(	(
<g/>
programovací	programovací	k2eAgInSc1d1	programovací
jazyk	jazyk	k1gInSc1	jazyk
<g/>
)	)	kIx)	)
Karlík	Karlík	k1gMnSc1	Karlík
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
rozcestník	rozcestník	k1gInSc1	rozcestník
<g/>
)	)	kIx)	)
Král	Král	k1gMnSc1	Král
(	(	kIx(	(
<g/>
rozcestník	rozcestník	k1gInSc1	rozcestník
<g/>
)	)	kIx)	)
Seznam	seznam	k1gInSc1	seznam
článků	článek	k1gInPc2	článek
začínajících	začínající	k2eAgInPc2d1	začínající
na	na	k7c4	na
"	"	kIx"	"
<g/>
Karel	Karel	k1gMnSc1	Karel
<g/>
"	"	kIx"	"
Seznam	seznam	k1gInSc1	seznam
článků	článek	k1gInPc2	článek
začínajících	začínající	k2eAgInPc2d1	začínající
na	na	k7c4	na
"	"	kIx"	"
<g/>
Karol	Karol	k1gInSc4	Karol
<g/>
"	"	kIx"	"
Seznam	seznam	k1gInSc4	seznam
článků	článek	k1gInPc2	článek
začínajících	začínající	k2eAgInPc2d1	začínající
na	na	k7c4	na
"	"	kIx"	"
<g/>
Charles	Charles	k1gMnSc1	Charles
<g/>
"	"	kIx"	"
Ray	Ray	k1gMnSc1	Ray
Charles	Charles	k1gMnSc1	Charles
–	–	k?	–
americký	americký	k2eAgMnSc1d1	americký
hudebník	hudebník	k1gMnSc1	hudebník
(	(	kIx(	(
<g/>
klavírista	klavírista	k1gMnSc1	klavírista
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
skladatel	skladatel	k1gMnSc1	skladatel
Ivan	Ivan	k1gMnSc1	Ivan
Karel	Karel	k1gMnSc1	Karel
–	–	k?	–
český	český	k2eAgMnSc1d1	český
lékař	lékař	k1gMnSc1	lékař
Ján	Ján	k1gMnSc1	Ján
Karel	Karel	k1gMnSc1	Karel
–	–	k?	–
slovenský	slovenský	k2eAgMnSc1d1	slovenský
fotbalista	fotbalista	k1gMnSc1	fotbalista
<g/>
,	,	kIx,	,
bratr	bratr	k1gMnSc1	bratr
Jozefa	Jozef	k1gMnSc2	Jozef
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Karel	Karel	k1gMnSc1	Karel
–	–	k?	–
český	český	k2eAgMnSc1d1	český
fotbalista	fotbalista	k1gMnSc1	fotbalista
Jiří	Jiří	k1gMnSc1	Jiří
Karel	Karel	k1gMnSc1	Karel
–	–	k?	–
český	český	k2eAgMnSc1d1	český
archeolog	archeolog	k1gMnSc1	archeolog
a	a	k8xC	a
historik	historik	k1gMnSc1	historik
Jozef	Jozef	k1gMnSc1	Jozef
Karel	Karel	k1gMnSc1	Karel
–	–	k?	–
slovenský	slovenský	k2eAgMnSc1d1	slovenský
fotbalista	fotbalista	k1gMnSc1	fotbalista
<g/>
,	,	kIx,	,
bratr	bratr	k1gMnSc1	bratr
Jána	Ján	k1gMnSc2	Ján
Rudolf	Rudolf	k1gMnSc1	Rudolf
Karel	Karel	k1gMnSc1	Karel
–	–	k?	–
český	český	k2eAgMnSc1d1	český
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
dirigent	dirigent	k1gMnSc1	dirigent
Miloslava	Miloslava	k1gFnSc1	Miloslava
Knappová	Knappová	k1gFnSc1	Knappová
<g/>
:	:	kIx,	:
Jak	jak	k8xC	jak
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
jmenovat	jmenovat	k5eAaImF	jmenovat
<g/>
.	.	kIx.	.
</s>
<s>
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1985	[number]	k4	1985
<g/>
,	,	kIx,	,
p.	p.	k?	p.
117	[number]	k4	117
Petr	Petr	k1gMnSc1	Petr
Čornej	Čornej	k1gMnSc1	Čornej
<g/>
:	:	kIx,	:
Panovníci	panovník	k1gMnPc1	panovník
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
<g/>
.	.	kIx.	.
</s>
<s>
Martin	Martin	k1gMnSc1	Martin
Vopěnka	Vopěnka	k1gFnSc1	Vopěnka
–	–	k?	–
Práh	práh	k1gInSc1	práh
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
62	[number]	k4	62
pp	pp	k?	pp
<g/>
.	.	kIx.	.
20	[number]	k4	20
nejčastějších	častý	k2eAgNnPc2d3	nejčastější
mužských	mužský	k2eAgNnPc2d1	mužské
jmen	jméno	k1gNnPc2	jméno
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
MV	MV	kA	MV
ČR	ČR	kA	ČR
Jména	jméno	k1gNnPc4	jméno
dětí	dítě	k1gFnPc2	dítě
narozená	narozený	k2eAgFnSc1d1	narozená
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2012	[number]	k4	2012
Slovníkové	slovníkový	k2eAgInPc4d1	slovníkový
heslo	heslo	k1gNnSc1	heslo
Karel	Karel	k1gMnSc1	Karel
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
