<s>
Oko	oko	k1gNnSc1	oko
je	být	k5eAaImIp3nS	být
smyslový	smyslový	k2eAgInSc4d1	smyslový
orgán	orgán	k1gInSc4	orgán
reagující	reagující	k2eAgInSc4d1	reagující
na	na	k7c4	na
světlo	světlo	k1gNnSc4	světlo
(	(	kIx(	(
<g/>
fotoreceptor	fotoreceptor	k1gInSc1	fotoreceptor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
zajišťující	zajišťující	k2eAgInSc4d1	zajišťující
zrak	zrak	k1gInSc4	zrak
<g/>
.	.	kIx.	.
</s>
