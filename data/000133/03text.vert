<s>
Oko	oko	k1gNnSc1	oko
je	být	k5eAaImIp3nS	být
smyslový	smyslový	k2eAgInSc4d1	smyslový
orgán	orgán	k1gInSc4	orgán
reagující	reagující	k2eAgInSc4d1	reagující
na	na	k7c4	na
světlo	světlo	k1gNnSc4	světlo
(	(	kIx(	(
<g/>
fotoreceptor	fotoreceptor	k1gInSc1	fotoreceptor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
zajišťující	zajišťující	k2eAgInSc4d1	zajišťující
zrak	zrak	k1gInSc4	zrak
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
vývoje	vývoj	k1gInSc2	vývoj
živočichů	živočich	k1gMnPc2	živočich
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
výraznému	výrazný	k2eAgInSc3d1	výrazný
rozvoji	rozvoj	k1gInSc3	rozvoj
od	od	k7c2	od
světločivných	světločivný	k2eAgInPc2d1	světločivný
orgánů	orgán	k1gInPc2	orgán
reagujících	reagující	k2eAgInPc2d1	reagující
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
existenci	existence	k1gFnSc6	existence
světla	světlo	k1gNnSc2	světlo
až	až	k9	až
po	po	k7c4	po
oko	oko	k1gNnSc4	oko
"	"	kIx"	"
<g/>
jednoduché	jednoduchý	k2eAgFnPc1d1	jednoduchá
<g/>
"	"	kIx"	"
u	u	k7c2	u
obratlovců	obratlovec	k1gMnPc2	obratlovec
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
člověka	člověk	k1gMnSc2	člověk
<g/>
)	)	kIx)	)
a	a	k8xC	a
hlavonožců	hlavonožec	k1gMnPc2	hlavonožec
a	a	k8xC	a
oko	oko	k1gNnSc1	oko
složené	složený	k2eAgFnSc2d1	složená
např.	např.	kA	např.
u	u	k7c2	u
členovců	členovec	k1gMnPc2	členovec
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
živočichů	živočich	k1gMnPc2	živočich
(	(	kIx(	(
<g/>
většina	většina	k1gFnSc1	většina
savců	savec	k1gMnPc2	savec
<g/>
,	,	kIx,	,
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
plazů	plaz	k1gMnPc2	plaz
a	a	k8xC	a
některých	některý	k3yIgFnPc2	některý
ryb	ryba	k1gFnPc2	ryba
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
oči	oko	k1gNnPc4	oko
umístěné	umístěný	k2eAgInPc1d1	umístěný
na	na	k7c6	na
přední	přední	k2eAgFnSc6d1	přední
straně	strana	k1gFnSc6	strana
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jim	on	k3xPp3gMnPc3	on
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
trojrozměrné	trojrozměrný	k2eAgNnSc4d1	trojrozměrné
binokulární	binokulární	k2eAgNnSc4d1	binokulární
vidění	vidění	k1gNnSc4	vidění
-	-	kIx~	-
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
obrázky	obrázek	k1gInPc4	obrázek
z	z	k7c2	z
obou	dva	k4xCgNnPc2	dva
očí	oko	k1gNnPc2	oko
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
do	do	k7c2	do
jednoho	jeden	k4xCgMnSc2	jeden
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
vnímáme	vnímat	k5eAaImIp1nP	vnímat
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
tomu	ten	k3xDgNnSc3	ten
existuje	existovat	k5eAaImIp3nS	existovat
monokulární	monokulární	k2eAgNnSc1d1	monokulární
vidění	vidění	k1gNnSc1	vidění
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
živočich	živočich	k1gMnSc1	živočich
vnímá	vnímat	k5eAaImIp3nS	vnímat
současně	současně	k6eAd1	současně
dva	dva	k4xCgInPc4	dva
rozdílné	rozdílný	k2eAgInPc4d1	rozdílný
obrazy	obraz	k1gInPc4	obraz
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
zajíc	zajíc	k1gMnSc1	zajíc
nebo	nebo	k8xC	nebo
chameleon	chameleon	k1gMnSc1	chameleon
<g/>
.	.	kIx.	.
</s>
<s>
Oko	oko	k1gNnSc1	oko
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
evoluční	evoluční	k2eAgFnSc2d1	evoluční
historie	historie	k1gFnSc2	historie
mnohokrát	mnohokrát	k6eAd1	mnohokrát
a	a	k8xC	a
nezávisle	závisle	k6eNd1	závisle
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
zdrojů	zdroj	k1gInPc2	zdroj
i	i	k9	i
čtyřicetkrát	čtyřicetkrát	k6eAd1	čtyřicetkrát
až	až	k9	až
šedesátkrát	šedesátkrát	k6eAd1	šedesátkrát
<g/>
.	.	kIx.	.
</s>
<s>
Komorové	komorový	k2eAgNnSc1d1	komorové
oko	oko	k1gNnSc1	oko
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
pokročilých	pokročilý	k2eAgInPc2d1	pokročilý
typů	typ	k1gInPc2	typ
očí	oko	k1gNnPc2	oko
<g/>
,	,	kIx,	,
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
zřejmě	zřejmě	k6eAd1	zřejmě
nezávisle	závisle	k6eNd1	závisle
třikrát	třikrát	k6eAd1	třikrát
<g/>
:	:	kIx,	:
u	u	k7c2	u
obratlovců	obratlovec	k1gMnPc2	obratlovec
<g/>
,	,	kIx,	,
hlavonožců	hlavonožec	k1gMnPc2	hlavonožec
a	a	k8xC	a
u	u	k7c2	u
medúz	medúza	k1gFnPc2	medúza
z	z	k7c2	z
třídy	třída	k1gFnSc2	třída
čtyřhranky	čtyřhranka	k1gFnSc2	čtyřhranka
(	(	kIx(	(
<g/>
Cubozoa	Cubozoa	k1gMnSc1	Cubozoa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oči	oko	k1gNnPc1	oko
evidentně	evidentně	k6eAd1	evidentně
zcela	zcela	k6eAd1	zcela
nepříbuzných	příbuzný	k2eNgFnPc2d1	nepříbuzná
skupin	skupina	k1gFnPc2	skupina
organismů	organismus	k1gInPc2	organismus
tak	tak	k6eAd1	tak
mají	mít	k5eAaImIp3nP	mít
někdy	někdy	k6eAd1	někdy
podobnou	podobný	k2eAgFnSc4d1	podobná
stavbu	stavba	k1gFnSc4	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Udává	udávat	k5eAaImIp3nS	udávat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
existuje	existovat	k5eAaImIp3nS	existovat
přibližně	přibližně	k6eAd1	přibližně
devět	devět	k4xCc1	devět
optických	optický	k2eAgInPc2d1	optický
mechanismů	mechanismus	k1gInPc2	mechanismus
vidění	vidění	k1gNnSc2	vidění
<g/>
,	,	kIx,	,
každý	každý	k3xTgMnSc1	každý
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
několikrát	několikrát	k6eAd1	několikrát
<g/>
.	.	kIx.	.
</s>
<s>
Evoluce	evoluce	k1gFnSc1	evoluce
oka	oko	k1gNnSc2	oko
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
stává	stávat	k5eAaImIp3nS	stávat
argumentem	argument	k1gInSc7	argument
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
zpochybňují	zpochybňovat	k5eAaImIp3nP	zpochybňovat
evoluční	evoluční	k2eAgFnSc4d1	evoluční
teorii	teorie	k1gFnSc4	teorie
a	a	k8xC	a
zastávají	zastávat	k5eAaImIp3nP	zastávat
kreacionismus	kreacionismus	k1gInSc4	kreacionismus
<g/>
.	.	kIx.	.
</s>
<s>
Tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
například	například	k6eAd1	například
<g/>
,	,	kIx,	,
že	že	k8xS	že
jakékoliv	jakýkoliv	k3yIgInPc1	jakýkoliv
mezistupně	mezistupeň	k1gInPc1	mezistupeň
při	při	k7c6	při
evoluci	evoluce	k1gFnSc6	evoluce
tak	tak	k9	tak
složitého	složitý	k2eAgInSc2d1	složitý
orgánu	orgán	k1gInSc2	orgán
by	by	kYmCp3nP	by
byly	být	k5eAaImAgFnP	být
nefunkční	funkční	k2eNgFnPc1d1	nefunkční
<g/>
.	.	kIx.	.
</s>
<s>
Darwin	Darwin	k1gMnSc1	Darwin
o	o	k7c4	o
evoluci	evoluce	k1gFnSc4	evoluce
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
výzvou	výzva	k1gFnSc7	výzva
pro	pro	k7c4	pro
naši	náš	k3xOp1gFnSc4	náš
představivost	představivost	k1gFnSc4	představivost
<g/>
,	,	kIx,	,
spíše	spíše	k9	spíše
než	než	k8xS	než
pro	pro	k7c4	pro
rozum	rozum	k1gInSc4	rozum
<g/>
,	,	kIx,	,
biolog	biolog	k1gMnSc1	biolog
Tomáš	Tomáš	k1gMnSc1	Tomáš
Grim	Grim	k1gMnSc1	Grim
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
dodává	dodávat	k5eAaImIp3nS	dodávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
neschopnost	neschopnost	k1gFnSc1	neschopnost
některých	některý	k3yIgMnPc2	některý
autorů	autor	k1gMnPc2	autor
si	se	k3xPyFc3	se
něco	něco	k3yInSc1	něco
představit	představit	k5eAaPmF	představit
není	být	k5eNaImIp3nS	být
relevantním	relevantní	k2eAgInSc7d1	relevantní
argumentem	argument	k1gInSc7	argument
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
totiž	totiž	k9	totiž
dokonce	dokonce	k9	dokonce
vytvořeny	vytvořen	k2eAgInPc1d1	vytvořen
počítačové	počítačový	k2eAgInPc1d1	počítačový
programy	program	k1gInPc1	program
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
postupnými	postupný	k2eAgFnPc7d1	postupná
proměnami	proměna	k1gFnPc7	proměna
jednoduché	jednoduchý	k2eAgFnPc1d1	jednoduchá
pigmentové	pigmentový	k2eAgFnPc1d1	pigmentová
skvrny	skvrna	k1gFnPc1	skvrna
vytvořily	vytvořit	k5eAaPmAgFnP	vytvořit
komorové	komorový	k2eAgNnSc4d1	komorové
oko	oko	k1gNnSc4	oko
méně	málo	k6eAd2	málo
než	než	k8xS	než
dvěma	dva	k4xCgInPc7	dva
tisíci	tisíc	k4xCgInPc7	tisíc
drobných	drobný	k2eAgFnPc2d1	drobná
mutací	mutace	k1gFnPc2	mutace
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
za	za	k7c4	za
neuvěřitelné	uvěřitelný	k2eNgFnPc4d1	neuvěřitelná
krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
přechodné	přechodný	k2eAgInPc1d1	přechodný
kroky	krok	k1gInPc1	krok
byly	být	k5eAaImAgInP	být
životaschopné	životaschopný	k2eAgInPc1d1	životaschopný
a	a	k8xC	a
mnohé	mnohý	k2eAgInPc1d1	mnohý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
opravdu	opravdu	k6eAd1	opravdu
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
živočišných	živočišný	k2eAgInPc2d1	živočišný
kmenů	kmen	k1gInPc2	kmen
můžeme	moct	k5eAaImIp1nP	moct
nalézt	nalézt	k5eAaPmF	nalézt
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
zrak	zrak	k1gInSc1	zrak
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
funkce	funkce	k1gFnSc1	funkce
oka	oko	k1gNnSc2	oko
je	být	k5eAaImIp3nS	být
zřejmá	zřejmý	k2eAgFnSc1d1	zřejmá
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
orgán	orgán	k1gInSc1	orgán
znám	znát	k5eAaImIp1nS	znát
jako	jako	k9	jako
sídlo	sídlo	k1gNnSc1	sídlo
zraku	zrak	k1gInSc2	zrak
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
buněčného	buněčný	k2eAgNnSc2d1	buněčné
hlediska	hledisko	k1gNnSc2	hledisko
se	se	k3xPyFc4	se
v	v	k7c6	v
očích	oko	k1gNnPc6	oko
obvykle	obvykle	k6eAd1	obvykle
uplatňují	uplatňovat	k5eAaImIp3nP	uplatňovat
specializované	specializovaný	k2eAgFnPc1d1	specializovaná
světločivné	světločivný	k2eAgFnPc1d1	světločivná
buňky	buňka	k1gFnPc1	buňka
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
tyčinky	tyčinka	k1gFnSc2	tyčinka
nebo	nebo	k8xC	nebo
čípky	čípek	k1gInPc4	čípek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
oční	oční	k2eAgInPc1d1	oční
pigmenty	pigment	k1gInPc1	pigment
(	(	kIx(	(
<g/>
např.	např.	kA	např.
u	u	k7c2	u
člověka	člověk	k1gMnSc4	člověk
rodopsin	rodopsin	k2eAgInSc1d1	rodopsin
a	a	k8xC	a
jodopsiny	jodopsin	k2eAgFnPc1d1	jodopsin
<g/>
)	)	kIx)	)
a	a	k8xC	a
složitou	složitý	k2eAgFnSc7d1	složitá
kaskádou	kaskáda	k1gFnSc7	kaskáda
biochemických	biochemický	k2eAgFnPc2d1	biochemická
drah	draha	k1gFnPc2	draha
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
vznik	vznik	k1gInSc4	vznik
nervového	nervový	k2eAgInSc2d1	nervový
impulsu	impuls	k1gInSc2	impuls
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
směřuje	směřovat	k5eAaImIp3nS	směřovat
pomocí	pomocí	k7c2	pomocí
nervových	nervový	k2eAgFnPc2d1	nervová
buněk	buňka	k1gFnPc2	buňka
do	do	k7c2	do
mozku	mozek	k1gInSc2	mozek
(	(	kIx(	(
<g/>
u	u	k7c2	u
bezobratlých	bezobratlý	k2eAgMnPc2d1	bezobratlý
do	do	k7c2	do
nějakého	nějaký	k3yIgNnSc2	nějaký
nervového	nervový	k2eAgNnSc2d1	nervové
ganglia	ganglion	k1gNnSc2	ganglion
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
lidské	lidský	k2eAgNnSc1d1	lidské
oko	oko	k1gNnSc1	oko
<g/>
.	.	kIx.	.
</s>
<s>
Oko	oko	k1gNnSc1	oko
obratlovců	obratlovec	k1gMnPc2	obratlovec
je	být	k5eAaImIp3nS	být
párový	párový	k2eAgInSc1d1	párový
orgán	orgán	k1gInSc1	orgán
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
zárodečném	zárodečný	k2eAgNnSc6d1	zárodečné
stadiu	stadion	k1gNnSc6	stadion
zakládá	zakládat	k5eAaImIp3nS	zakládat
z	z	k7c2	z
přední	přední	k2eAgFnSc2d1	přední
(	(	kIx(	(
<g/>
rostrální	rostrální	k2eAgFnSc2d1	rostrální
<g/>
)	)	kIx)	)
části	část	k1gFnSc2	část
hypothalamu	hypothalam	k1gInSc2	hypothalam
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
účinkům	účinek	k1gInPc3	účinek
jistých	jistý	k2eAgInPc2d1	jistý
signálních	signální	k2eAgInPc2d1	signální
proteinů	protein	k1gInPc2	protein
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
Shh	Shh	k1gFnSc1	Shh
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
jediná	jediný	k2eAgFnSc1d1	jediná
masa	masa	k1gFnSc1	masa
buněk	buňka	k1gFnPc2	buňka
rozdělí	rozdělit	k5eAaPmIp3nS	rozdělit
vedví	vedví	k6eAd1	vedví
a	a	k8xC	a
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
levé	levý	k2eAgNnSc4d1	levé
a	a	k8xC	a
pravé	pravý	k2eAgNnSc4d1	pravé
oko	oko	k1gNnSc4	oko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
stavbě	stavba	k1gFnSc6	stavba
oka	oko	k1gNnSc2	oko
se	se	k3xPyFc4	se
však	však	k9	však
embryonálně	embryonálně	k6eAd1	embryonálně
podílí	podílet	k5eAaImIp3nS	podílet
i	i	k9	i
jiné	jiný	k2eAgFnPc4d1	jiná
tkáně	tkáň	k1gFnPc4	tkáň
<g/>
.	.	kIx.	.
</s>
<s>
Struktura	struktura	k1gFnSc1	struktura
lidského	lidský	k2eAgNnSc2d1	lidské
oka	oko	k1gNnSc2	oko
se	se	k3xPyFc4	se
plně	plně	k6eAd1	plně
přizpůsobuje	přizpůsobovat	k5eAaImIp3nS	přizpůsobovat
potřebě	potřeba	k1gFnSc3	potřeba
zaostřit	zaostřit	k5eAaPmF	zaostřit
paprsek	paprsek	k1gInSc4	paprsek
světla	světlo	k1gNnSc2	světlo
na	na	k7c6	na
sítnici	sítnice	k1gFnSc6	sítnice
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
retina	retina	k1gFnSc1	retina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
části	část	k1gFnPc1	část
oka	oko	k1gNnSc2	oko
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
které	který	k3yQgNnSc4	který
paprsek	paprsek	k1gInSc1	paprsek
světla	světlo	k1gNnSc2	světlo
prochází	procházet	k5eAaImIp3nS	procházet
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
průhledné	průhledný	k2eAgFnPc1d1	průhledná
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
co	co	k9	co
nejvíce	nejvíce	k6eAd1	nejvíce
zabraňovaly	zabraňovat	k5eAaImAgFnP	zabraňovat
rozptylu	rozptyl	k1gInSc6	rozptyl
dopadajícího	dopadající	k2eAgNnSc2d1	dopadající
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Rohovka	rohovka	k1gFnSc1	rohovka
(	(	kIx(	(
<g/>
cornea	cornea	k1gFnSc1	cornea
<g/>
)	)	kIx)	)
a	a	k8xC	a
čočka	čočka	k1gFnSc1	čočka
(	(	kIx(	(
<g/>
lens	lens	k1gInSc1	lens
<g/>
)	)	kIx)	)
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
paprsek	paprsek	k1gInSc4	paprsek
světla	světlo	k1gNnSc2	světlo
spojit	spojit	k5eAaPmF	spojit
a	a	k8xC	a
zaostřit	zaostřit	k5eAaPmF	zaostřit
na	na	k7c4	na
zadní	zadní	k2eAgFnSc4d1	zadní
stěnu	stěna	k1gFnSc4	stěna
oka	oko	k1gNnSc2	oko
-	-	kIx~	-
sítnici	sítnice	k1gFnSc6	sítnice
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
světlo	světlo	k1gNnSc1	světlo
pak	pak	k6eAd1	pak
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
chemické	chemický	k2eAgFnPc4d1	chemická
přeměny	přeměna	k1gFnPc4	přeměna
ve	v	k7c6	v
světločivných	světločivný	k2eAgFnPc6d1	světločivná
buňkách	buňka	k1gFnPc6	buňka
(	(	kIx(	(
<g/>
tyčinky	tyčinka	k1gFnPc4	tyčinka
a	a	k8xC	a
čípky	čípek	k1gInPc4	čípek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vysílají	vysílat	k5eAaImIp3nP	vysílat
nervové	nervový	k2eAgInPc4d1	nervový
impulsy	impuls	k1gInPc4	impuls
zrakovým	zrakový	k2eAgInSc7d1	zrakový
nervem	nerv	k1gInSc7	nerv
(	(	kIx(	(
<g/>
nervus	nervus	k1gInSc1	nervus
opticus	opticus	k1gInSc1	opticus
<g/>
)	)	kIx)	)
do	do	k7c2	do
mozku	mozek	k1gInSc2	mozek
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
složené	složený	k2eAgNnSc1d1	složené
oko	oko	k1gNnSc1	oko
<g/>
.	.	kIx.	.
</s>
<s>
Členovci	členovec	k1gMnPc1	členovec
mají	mít	k5eAaImIp3nP	mít
zcela	zcela	k6eAd1	zcela
jinak	jinak	k6eAd1	jinak
vytvořené	vytvořený	k2eAgInPc4d1	vytvořený
oční	oční	k2eAgInPc4d1	oční
orgány	orgán	k1gInPc4	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
dáno	dát	k5eAaPmNgNnS	dát
především	především	k9	především
jejich	jejich	k3xOp3gFnSc7	jejich
velikostí	velikost	k1gFnSc7	velikost
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
optické	optický	k2eAgFnSc2d1	optická
vady	vada	k1gFnSc2	vada
<g/>
,	,	kIx,	,
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
ohybem	ohyb	k1gInSc7	ohyb
světla	světlo	k1gNnSc2	světlo
na	na	k7c6	na
panence	panenka	k1gFnSc6	panenka
příliš	příliš	k6eAd1	příliš
malých	malý	k2eAgNnPc2d1	malé
očí	oko	k1gNnPc2	oko
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
prakticky	prakticky	k6eAd1	prakticky
znemožňovaly	znemožňovat	k5eAaImAgFnP	znemožňovat
jejich	jejich	k3xOp3gNnSc4	jejich
používání	používání	k1gNnSc4	používání
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
drobné	drobný	k2eAgInPc4d1	drobný
druhy	druh	k1gInPc4	druh
hmyzu	hmyz	k1gInSc2	hmyz
by	by	kYmCp3nP	by
takové	takový	k3xDgNnSc1	takový
oči	oko	k1gNnPc4	oko
byly	být	k5eAaImAgFnP	být
bezcenné	bezcenný	k2eAgFnPc1d1	bezcenná
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
u	u	k7c2	u
nich	on	k3xPp3gInPc2	on
vyvinuly	vyvinout	k5eAaPmAgFnP	vyvinout
jiné	jiný	k2eAgInPc4d1	jiný
orgány	orgán	k1gInPc4	orgán
-	-	kIx~	-
složené	složený	k2eAgFnPc4d1	složená
oči	oko	k1gNnPc4	oko
<g/>
.	.	kIx.	.
</s>
