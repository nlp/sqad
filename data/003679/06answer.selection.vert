<s>
Byl	být	k5eAaImAgInS	být
objeven	objeven	k2eAgInSc1d1	objeven
roku	rok	k1gInSc2	rok
1774	[number]	k4	1774
Carlem	Carl	k1gMnSc7	Carl
Wilhelmem	Wilhelm	k1gMnSc7	Wilhelm
Scheelem	Scheel	k1gMnSc7	Scheel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dnešní	dnešní	k2eAgNnSc1d1	dnešní
pojmenování	pojmenování	k1gNnSc1	pojmenování
mu	on	k3xPp3gMnSc3	on
dal	dát	k5eAaPmAgMnS	dát
až	až	k6eAd1	až
roku	rok	k1gInSc2	rok
1810	[number]	k4	1810
anglický	anglický	k2eAgMnSc1d1	anglický
chemik	chemik	k1gMnSc1	chemik
sir	sir	k1gMnSc1	sir
Humphry	Humphra	k1gFnSc2	Humphra
Davy	Dav	k1gInPc4	Dav
<g/>
.	.	kIx.	.
</s>
