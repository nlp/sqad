<p>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Baťa	Baťa	k1gMnSc1	Baťa
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1876	[number]	k4	1876
Zlín	Zlín	k1gInSc1	Zlín
–	–	k?	–
12.	[number]	k4	12.
července	červenec	k1gInSc2	červenec
1932	[number]	k4	1932
Otrokovice	Otrokovice	k1gFnPc1	Otrokovice
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
československý	československý	k2eAgMnSc1d1	československý
podnikatel	podnikatel	k1gMnSc1	podnikatel
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
král	král	k1gMnSc1	král
obuvi	obuv	k1gFnSc2	obuv
<g/>
"	"	kIx"	"
–	–	k?	–
tvůrce	tvůrce	k1gMnSc1	tvůrce
světového	světový	k2eAgNnSc2d1	světové
obuvnického	obuvnický	k2eAgNnSc2d1	Obuvnické
impéria	impérium	k1gNnSc2	impérium
<g/>
,	,	kIx,	,
starosta	starosta	k1gMnSc1	starosta
Zlína	Zlín	k1gInSc2	Zlín
(	(	kIx(	(
<g/>
1923	[number]	k4	1923
<g/>
–	–	k?	–
<g/>
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
a	a	k8xC	a
veřejný	veřejný	k2eAgInSc4d1	veřejný
činitel	činitel	k1gInSc4	činitel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
bratrem	bratr	k1gMnSc7	bratr
Antonínem	Antonín	k1gMnSc7	Antonín
ml	ml	kA	ml
<g/>
.	.	kIx.	.
a	a	k8xC	a
sestrou	sestra	k1gFnSc7	sestra
Annou	Anna	k1gFnSc7	Anna
založil	založit	k5eAaPmAgMnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1894	[number]	k4	1894
ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
obuvnickou	obuvnický	k2eAgFnSc4d1	obuvnická
firmu	firma	k1gFnSc4	firma
Baťa	Baťa	k1gMnSc1	Baťa
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
rozsáhlý	rozsáhlý	k2eAgInSc4d1	rozsáhlý
komplex	komplex	k1gInSc4	komplex
výroby	výroba	k1gFnSc2	výroba
<g/>
,	,	kIx,	,
obchodu	obchod	k1gInSc2	obchod
<g/>
,	,	kIx,	,
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
služeb	služba	k1gFnPc2	služba
a	a	k8xC	a
financí	finance	k1gFnPc2	finance
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
podnikatelů	podnikatel	k1gMnPc2	podnikatel
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Zavedl	zavést	k5eAaPmAgInS	zavést
originální	originální	k2eAgFnPc4d1	originální
metody	metoda	k1gFnPc4	metoda
řízení	řízení	k1gNnSc2	řízení
výroby	výroba	k1gFnSc2	výroba
a	a	k8xC	a
obchodu	obchod	k1gInSc2	obchod
a	a	k8xC	a
také	také	k9	také
systém	systém	k1gInSc1	systém
motivace	motivace	k1gFnSc2	motivace
pracovníků	pracovník	k1gMnPc2	pracovník
(	(	kIx(	(
<g/>
Baťova	Baťův	k2eAgFnSc1d1	Baťova
soustava	soustava	k1gFnSc1	soustava
řízení	řízení	k1gNnSc2	řízení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dokázal	dokázat	k5eAaPmAgInS	dokázat
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
množství	množství	k1gNnPc4	množství
budoucích	budoucí	k2eAgMnPc2d1	budoucí
ekonomů	ekonom	k1gMnPc2	ekonom
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
postupy	postup	k1gInPc1	postup
byly	být	k5eAaImAgInP	být
na	na	k7c4	na
tehdejší	tehdejší	k2eAgNnSc4d1	tehdejší
podnikání	podnikání	k1gNnSc4	podnikání
revoluční	revoluční	k2eAgFnSc1d1	revoluční
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
stále	stále	k6eAd1	stále
užívány	užíván	k2eAgInPc1d1	užíván
jako	jako	k8xC	jako
příklady	příklad	k1gInPc1	příklad
top	topit	k5eAaImRp2nS	topit
managementu	management	k1gInSc2	management
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Rozsahem	rozsah	k1gInSc7	rozsah
svých	svůj	k3xOyFgFnPc2	svůj
aktivit	aktivita	k1gFnPc2	aktivita
(	(	kIx(	(
<g/>
35	[number]	k4	35
oborů	obor	k1gInPc2	obor
výroby	výroba	k1gFnSc2	výroba
<g/>
,	,	kIx,	,
obchodu	obchod	k1gInSc2	obchod
<g/>
,	,	kIx,	,
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
služeb	služba	k1gFnPc2	služba
a	a	k8xC	a
financí	finance	k1gFnPc2	finance
<g/>
)	)	kIx)	)
působil	působit	k5eAaImAgInS	působit
na	na	k7c4	na
úroveň	úroveň	k1gFnSc4	úroveň
podnikání	podnikání	k1gNnSc2	podnikání
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
,	,	kIx,	,
nízkými	nízký	k2eAgFnPc7d1	nízká
cenami	cena	k1gFnPc7	cena
svých	svůj	k3xOyFgFnPc2	svůj
bot	bota	k1gFnPc2	bota
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
profil	profil	k1gInSc1	profil
spotřebního	spotřební	k2eAgInSc2d1	spotřební
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
budováním	budování	k1gNnSc7	budování
svého	svůj	k3xOyFgInSc2	svůj
továrního	tovární	k2eAgInSc2d1	tovární
areálu	areál	k1gInSc2	areál
dokázal	dokázat	k5eAaPmAgInS	dokázat
podle	podle	k7c2	podle
svých	svůj	k3xOyFgFnPc2	svůj
představ	představa	k1gFnPc2	představa
přebudovat	přebudovat	k5eAaPmF	přebudovat
město	město	k1gNnSc4	město
Zlín	Zlín	k1gInSc1	Zlín
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
jeho	jeho	k3xOp3gMnSc1	jeho
starosta	starosta	k1gMnSc1	starosta
prosadil	prosadit	k5eAaPmAgMnS	prosadit
koncepci	koncepce	k1gFnSc4	koncepce
zahradního	zahradní	k2eAgNnSc2d1	zahradní
města	město	k1gNnSc2	město
s	s	k7c7	s
originální	originální	k2eAgFnSc7d1	originální
funkcionalistickou	funkcionalistický	k2eAgFnSc7d1	funkcionalistická
architekturou	architektura	k1gFnSc7	architektura
<g/>
;	;	kIx,	;
ze	z	k7c2	z
Zlína	Zlín	k1gInSc2	Zlín
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc4	tento
styl	styl	k1gInSc4	styl
šířil	šířit	k5eAaImAgMnS	šířit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Baťovými	Baťův	k2eAgFnPc7d1	Baťova
továrnami	továrna	k1gFnPc7	továrna
do	do	k7c2	do
dalších	další	k2eAgNnPc2d1	další
míst	místo	k1gNnPc2	místo
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
,	,	kIx,	,
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
rozvětvený	rozvětvený	k2eAgInSc1d1	rozvětvený
vzdělávací	vzdělávací	k2eAgInSc1d1	vzdělávací
systém	systém	k1gInSc1	systém
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgMnPc4	svůj
zaměstnance	zaměstnanec	k1gMnPc4	zaměstnanec
motivoval	motivovat	k5eAaBmAgMnS	motivovat
k	k	k7c3	k
využívání	využívání	k1gNnSc3	využívání
zdokonalovacích	zdokonalovací	k2eAgInPc2d1	zdokonalovací
kurzů	kurz	k1gInPc2	kurz
celoživotního	celoživotní	k2eAgNnSc2d1	celoživotní
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
<g/>
,	,	kIx,	,
zřídil	zřídit	k5eAaPmAgInS	zřídit
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
odbornou	odborný	k2eAgFnSc7d1	odborná
školu	škola	k1gFnSc4	škola
(	(	kIx(	(
<g/>
Baťova	Baťův	k2eAgFnSc1d1	Baťova
škola	škola	k1gFnSc1	škola
práce	práce	k1gFnSc2	práce
pro	pro	k7c4	pro
Mladé	mladé	k1gNnSc4	mladé
muže	muž	k1gMnSc2	muž
a	a	k8xC	a
Mladé	mladý	k2eAgFnSc2d1	mladá
ženy	žena	k1gFnSc2	žena
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
starosta	starosta	k1gMnSc1	starosta
Zlína	Zlín	k1gInSc2	Zlín
prosadil	prosadit	k5eAaPmAgMnS	prosadit
zavedení	zavedení	k1gNnSc4	zavedení
experimentálních	experimentální	k2eAgFnPc2d1	experimentální
forem	forma	k1gFnPc2	forma
veřejného	veřejný	k2eAgNnSc2d1	veřejné
školství	školství	k1gNnSc2	školství
(	(	kIx(	(
<g/>
zlínské	zlínský	k2eAgNnSc1d1	zlínské
pokusné	pokusný	k2eAgNnSc1d1	pokusné
školství	školství	k1gNnSc1	školství
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zřízením	zřízení	k1gNnSc7	zřízení
nemocnice	nemocnice	k1gFnSc2	nemocnice
ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
položil	položit	k5eAaPmAgMnS	položit
základy	základ	k1gInPc4	základ
k	k	k7c3	k
moderní	moderní	k2eAgFnSc3d1	moderní
péči	péče	k1gFnSc3	péče
ve	v	k7c6	v
městě	město	k1gNnSc6	město
a	a	k8xC	a
regionu	region	k1gInSc6	region
<g/>
.	.	kIx.	.
</s>
<s>
Prosazováním	prosazování	k1gNnSc7	prosazování
projektů	projekt	k1gInPc2	projekt
dálkové	dálkový	k2eAgFnSc2d1	dálková
železniční	železniční	k2eAgFnSc2d1	železniční
<g/>
,	,	kIx,	,
letecké	letecký	k2eAgFnSc2d1	letecká
<g/>
,	,	kIx,	,
říční	říční	k2eAgFnSc2d1	říční
a	a	k8xC	a
silniční	silniční	k2eAgFnSc2d1	silniční
dopravy	doprava	k1gFnSc2	doprava
mířil	mířit	k5eAaImAgMnS	mířit
ke	k	k7c3	k
zlepšení	zlepšení	k1gNnSc3	zlepšení
soustavy	soustava	k1gFnSc2	soustava
komunikací	komunikace	k1gFnPc2	komunikace
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
zlínského	zlínský	k2eAgInSc2d1	zlínský
regionu	region	k1gInSc2	region
i	i	k8xC	i
celého	celý	k2eAgNnSc2d1	celé
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc7d1	velká
celoživotní	celoživotní	k2eAgFnSc7d1	celoživotní
oporou	opora	k1gFnSc7	opora
Tomáše	Tomáš	k1gMnSc2	Tomáš
Bati	Baťa	k1gMnSc2	Baťa
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
v	v	k7c6	v
soukromém	soukromý	k2eAgInSc6d1	soukromý
životě	život	k1gInSc6	život
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
v	v	k7c6	v
podnikání	podnikání	k1gNnSc6	podnikání
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gMnPc3	jeho
žena	žena	k1gFnSc1	žena
Marie	Marie	k1gFnSc1	Marie
<g/>
,	,	kIx,	,
vídeňská	vídeňský	k2eAgFnSc1d1	Vídeňská
Češka	Češka	k1gFnSc1	Češka
z	z	k7c2	z
velmi	velmi	k6eAd1	velmi
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
dobré	dobrý	k2eAgFnSc2d1	dobrá
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
velmi	velmi	k6eAd1	velmi
činnou	činný	k2eAgFnSc7d1	činná
v	v	k7c6	v
sociální	sociální	k2eAgFnSc6d1	sociální
a	a	k8xC	a
zdravotní	zdravotní	k2eAgFnSc6d1	zdravotní
sféře	sféra	k1gFnSc6	sféra
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
svým	svůj	k3xOyFgFnPc3	svůj
aktivitám	aktivita	k1gFnPc3	aktivita
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
velmi	velmi	k6eAd1	velmi
uznávanou	uznávaný	k2eAgFnSc7d1	uznávaná
osobností	osobnost	k1gFnSc7	osobnost
ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Dětství	dětství	k1gNnSc4	dětství
==	==	k?	==
</s>
</p>
<p>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Baťa	Baťa	k1gMnSc1	Baťa
pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
staletí	staletí	k1gNnSc6	staletí
zabývala	zabývat	k5eAaImAgFnS	zabývat
ševcovstvím	ševcovstvím	k?	ševcovstvím
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
ševci	švec	k1gMnSc6	švec
jménem	jméno	k1gNnSc7	jméno
Lukáš	Lukáš	k1gMnSc1	Lukáš
Baťa	Baťa	k1gMnSc1	Baťa
(	(	kIx(	(
<g/>
Batiu	Batium	k1gNnSc6	Batium
<g/>
,	,	kIx,	,
Batiku	batik	k1gInSc6	batik
<g/>
,	,	kIx,	,
Batu	Bata	k1gFnSc4	Bata
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1667	[number]	k4	1667
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sousedních	sousední	k2eAgFnPc6d1	sousední
Želechovicích	Želechovice	k1gFnPc6	Želechovice
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1644	[number]	k4	1644
téhož	týž	k3xTgNnSc2	týž
příjmení	příjmení	k1gNnSc2	příjmení
(	(	kIx(	(
<g/>
snad	snad	k9	snad
významem	význam	k1gInSc7	význam
"	"	kIx"	"
<g/>
táta	táta	k1gMnSc1	táta
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
Jánoš	Jánoš	k1gMnSc1	Jánoš
Baťů	Baťa	k1gMnPc2	Baťa
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
Batík	Batík	k1gMnSc1	Batík
a	a	k8xC	a
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1580	[number]	k4	1580
Václav	Václav	k1gMnSc1	Václav
Batiů	Bati	k1gInPc2	Bati
<g/>
.	.	kIx.	.
</s>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgNnSc4	třetí
dítě	dítě	k1gNnSc4	dítě
Antonína	Antonín	k1gMnSc2	Antonín
Bati	Baťa	k1gMnSc2	Baťa
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
3.	[number]	k4	3.
dubna	duben	k1gInSc2	duben
1876	[number]	k4	1876
ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
v	v	k7c6	v
Dlouhé	Dlouhé	k2eAgFnSc6d1	Dlouhé
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
věku	věk	k1gInSc6	věk
8	[number]	k4	8
let	léto	k1gNnPc2	léto
mu	on	k3xPp3gNnSc3	on
zemřela	zemřít	k5eAaPmAgFnS	zemřít
matka	matka	k1gFnSc1	matka
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
znovu	znovu	k6eAd1	znovu
oženit	oženit	k5eAaPmF	oženit
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
přestěhovat	přestěhovat	k5eAaPmF	přestěhovat
rodinu	rodina	k1gFnSc4	rodina
a	a	k8xC	a
živnost	živnost	k1gFnSc4	živnost
do	do	k7c2	do
Uherského	uherský	k2eAgNnSc2d1	Uherské
Hradiště	Hradiště	k1gNnSc2	Hradiště
<g/>
.	.	kIx.	.
</s>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
tak	tak	k6eAd1	tak
přešel	přejít	k5eAaPmAgMnS	přejít
do	do	k7c2	do
tamní	tamní	k2eAgFnSc2d1	tamní
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
místo	místo	k7c2	místo
českého	český	k2eAgInSc2d1	český
jazyka	jazyk	k1gInSc2	jazyk
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
jazyk	jazyk	k1gInSc4	jazyk
německý	německý	k2eAgMnSc1d1	německý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
První	první	k4xOgInPc1	první
řemeslnické	řemeslnický	k2eAgInPc1d1	řemeslnický
krůčky	krůček	k1gInPc1	krůček
==	==	k?	==
</s>
</p>
<p>
<s>
Už	už	k6eAd1	už
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
12	[number]	k4	12
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
zajímat	zajímat	k5eAaImF	zajímat
o	o	k7c4	o
řemeslo	řemeslo	k1gNnSc4	řemeslo
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
to	ten	k3xDgNnSc1	ten
nejen	nejen	k6eAd1	nejen
jak	jak	k6eAd1	jak
boty	bota	k1gFnPc4	bota
vyrábět	vyrábět	k5eAaImF	vyrábět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
jak	jak	k6eAd1	jak
je	být	k5eAaImIp3nS	být
co	co	k9	co
nejlépe	dobře	k6eAd3	dobře
prodat	prodat	k5eAaPmF	prodat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
už	už	k6eAd1	už
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
ovládal	ovládat	k5eAaImAgMnS	ovládat
vše	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
mu	on	k3xPp3gMnSc3	on
k	k	k7c3	k
živnosti	živnost	k1gFnSc3	živnost
ševce	švec	k1gMnSc2	švec
postačovalo	postačovat	k5eAaImAgNnS	postačovat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
14	[number]	k4	14
letech	léto	k1gNnPc6	léto
odešel	odejít	k5eAaPmAgMnS	odejít
mladý	mladý	k2eAgMnSc1d1	mladý
Tomáš	Tomáš	k1gMnSc1	Tomáš
z	z	k7c2	z
domova	domov	k1gInSc2	domov
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
vůli	vůle	k1gFnSc3	vůle
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
odjel	odjet	k5eAaPmAgMnS	odjet
do	do	k7c2	do
Prostějova	Prostějov	k1gInSc2	Prostějov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pracoval	pracovat	k5eAaImAgMnS	pracovat
u	u	k7c2	u
firmy	firma	k1gFnSc2	firma
Fäber	Fäbra	k1gFnPc2	Fäbra
vyrábějící	vyrábějící	k2eAgFnSc2d1	vyrábějící
ševcovské	ševcovská	k1gFnSc2	ševcovská
stroje	stroj	k1gInSc2	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
zajímal	zajímat	k5eAaImAgMnS	zajímat
o	o	k7c4	o
stroje	stroj	k1gInPc4	stroj
ulehčující	ulehčující	k2eAgInPc4d1	ulehčující
a	a	k8xC	a
zrychlující	zrychlující	k2eAgFnSc4d1	zrychlující
práci	práce	k1gFnSc4	práce
ševců	švec	k1gMnPc2	švec
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
však	však	k9	však
dostal	dostat	k5eAaPmAgMnS	dostat
výpověď	výpověď	k1gFnSc4	výpověď
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnPc1	jeho
zaměstnavatelé	zaměstnavatel	k1gMnPc1	zaměstnavatel
obávali	obávat	k5eAaImAgMnP	obávat
konkurence	konkurence	k1gFnPc4	konkurence
<g/>
.	.	kIx.	.
</s>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
otcovy	otcův	k2eAgFnSc2d1	otcova
dílny	dílna	k1gFnSc2	dílna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
vzájemné	vzájemný	k2eAgFnSc6d1	vzájemná
neshodě	neshoda	k1gFnSc6	neshoda
odjel	odjet	k5eAaPmAgMnS	odjet
bez	bez	k7c2	bez
peněžních	peněžní	k2eAgInPc2d1	peněžní
prostředků	prostředek	k1gInPc2	prostředek
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
za	za	k7c7	za
svou	svůj	k3xOyFgFnSc7	svůj
sestrou	sestra	k1gFnSc7	sestra
Annou	Anna	k1gFnSc7	Anna
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
mu	on	k3xPp3gMnSc3	on
finančně	finančně	k6eAd1	finančně
vypomohla	vypomoct	k5eAaPmAgFnS	vypomoct
a	a	k8xC	a
mladý	mladý	k2eAgMnSc1d1	mladý
Tomáš	Tomáš	k1gMnSc1	Tomáš
začal	začít	k5eAaPmAgMnS	začít
podnikat	podnikat	k5eAaImF	podnikat
na	na	k7c4	na
vlastní	vlastní	k2eAgFnSc4d1	vlastní
pěst	pěst	k1gFnSc4	pěst
ve	v	k7c6	v
vlastní	vlastní	k2eAgFnSc6d1	vlastní
dílně	dílna	k1gFnSc6	dílna
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
horlivost	horlivost	k1gFnSc1	horlivost
po	po	k7c6	po
zisku	zisk	k1gInSc6	zisk
byla	být	k5eAaImAgFnS	být
ovšem	ovšem	k9	ovšem
záhy	záhy	k6eAd1	záhy
ukončena	ukončit	k5eAaPmNgFnS	ukončit
neznalostí	neznalost	k1gFnSc7	neznalost
tamějšího	tamější	k2eAgInSc2d1	tamější
trhu	trh	k1gInSc2	trh
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
neměl	mít	k5eNaImAgMnS	mít
úřední	úřední	k2eAgNnSc4d1	úřední
povolení	povolení	k1gNnSc4	povolení
vykonávat	vykonávat	k5eAaImF	vykonávat
řemeslo	řemeslo	k1gNnSc4	řemeslo
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
v	v	k7c6	v
doprovodu	doprovod	k1gInSc6	doprovod
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
vrátil	vrátit	k5eAaPmAgMnS	vrátit
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Uherského	uherský	k2eAgNnSc2d1	Uherské
Hradiště	Hradiště	k1gNnSc2	Hradiště
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
obchodníkem	obchodník	k1gMnSc7	obchodník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Podnikatelské	podnikatelský	k2eAgInPc4d1	podnikatelský
začátky	začátek	k1gInPc4	začátek
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1894	[number]	k4	1894
se	se	k3xPyFc4	se
nechali	nechat	k5eAaPmAgMnP	nechat
Tomáš	Tomáš	k1gMnSc1	Tomáš
<g/>
,	,	kIx,	,
bratr	bratr	k1gMnSc1	bratr
Antonín	Antonín	k1gMnSc1	Antonín
a	a	k8xC	a
sestra	sestra	k1gFnSc1	sestra
Anna	Anna	k1gFnSc1	Anna
vyplatit	vyplatit	k5eAaPmF	vyplatit
svým	svůj	k3xOyFgMnSc7	svůj
otcem	otec	k1gMnSc7	otec
z	z	k7c2	z
rodinného	rodinný	k2eAgInSc2d1	rodinný
podniku	podnik	k1gInSc2	podnik
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
své	svůj	k3xOyFgFnSc2	svůj
zesnulé	zesnulý	k2eAgFnSc2d1	zesnulá
matky	matka	k1gFnSc2	matka
dostali	dostat	k5eAaPmAgMnP	dostat
věno	věno	k1gNnSc4	věno
800	[number]	k4	800
zlatých	zlatá	k1gFnPc2	zlatá
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yIgNnSc7	který
ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
založili	založit	k5eAaPmAgMnP	založit
obuvnickou	obuvnický	k2eAgFnSc4d1	obuvnická
živnost	živnost	k1gFnSc4	živnost
<g/>
,	,	kIx,	,
ohlášenou	ohlášená	k1gFnSc4	ohlášená
na	na	k7c4	na
nejstaršího	starý	k2eAgMnSc4d3	nejstarší
sourozence	sourozenec	k1gMnSc4	sourozenec
Antonína	Antonín	k1gMnSc4	Antonín
Baťu	Baťa	k1gMnSc4	Baťa
ml	ml	kA	ml
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tehdejších	tehdejší	k2eAgFnPc2d1	tehdejší
norem	norma	k1gFnPc2	norma
nebyl	být	k5eNaImAgMnS	být
totiž	totiž	k9	totiž
Tomáš	Tomáš	k1gMnSc1	Tomáš
ještě	ještě	k6eAd1	ještě
plnoletý	plnoletý	k2eAgMnSc1d1	plnoletý
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
žádali	žádat	k5eAaImAgMnP	žádat
o	o	k7c4	o
povolení	povolení	k1gNnSc4	povolení
obuvnické	obuvnický	k2eAgFnSc2d1	obuvnická
živnosti	živnost	k1gFnSc2	živnost
v	v	k7c6	v
Uherském	uherský	k2eAgNnSc6d1	Uherské
Hradišti	Hradiště	k1gNnSc6	Hradiště
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jim	on	k3xPp3gMnPc3	on
město	město	k1gNnSc1	město
jejich	jejich	k3xOp3gFnPc2	jejich
záměr	záměra	k1gFnPc2	záměra
provozovat	provozovat	k5eAaImF	provozovat
živnost	živnost	k1gFnSc4	živnost
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
obuvi	obuv	k1gFnSc2	obuv
a	a	k8xC	a
vybudovat	vybudovat	k5eAaPmF	vybudovat
obuvnický	obuvnický	k2eAgInSc4d1	obuvnický
podnik	podnik	k1gInSc4	podnik
<g/>
,	,	kIx,	,
nepovolilo	povolit	k5eNaPmAgNnS	povolit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
však	však	k9	však
získali	získat	k5eAaPmAgMnP	získat
pro	pro	k7c4	pro
své	své	k1gNnSc4	své
podnikání	podnikání	k1gNnSc2	podnikání
pochopení	pochopení	k1gNnSc2	pochopení
a	a	k8xC	a
souhlas	souhlas	k1gInSc1	souhlas
k	k	k7c3	k
zahájení	zahájení	k1gNnSc3	zahájení
výroby	výroba	k1gFnSc2	výroba
obuvi	obuv	k1gFnSc2	obuv
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
město	město	k1gNnSc1	město
Zlín	Zlín	k1gInSc4	Zlín
posvětilo	posvětit	k5eAaPmAgNnS	posvětit
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
o	o	k7c6	o
tom	ten	k3xDgInSc6	ten
radní	radní	k1gMnSc1	radní
dopředu	dopředu	k6eAd1	dopředu
věděli	vědět	k5eAaImAgMnP	vědět
<g/>
,	,	kIx,	,
založení	založení	k1gNnSc6	založení
základů	základ	k1gInPc2	základ
budoucího	budoucí	k2eAgInSc2d1	budoucí
obuvnického	obuvnický	k2eAgInSc2d1	obuvnický
komplexu	komplex	k1gInSc2	komplex
<g/>
.	.	kIx.	.
</s>
<s>
Sourozenci	sourozenec	k1gMnPc1	sourozenec
Baťovi	Baťův	k2eAgMnPc1d1	Baťův
zpočátku	zpočátku	k6eAd1	zpočátku
vyráběli	vyrábět	k5eAaImAgMnP	vyrábět
valašskou	valašský	k2eAgFnSc4d1	Valašská
prošívanou	prošívaný	k2eAgFnSc4d1	prošívaná
houněnou	houněný	k2eAgFnSc4d1	houněný
obuv	obuv	k1gFnSc4	obuv
na	na	k7c6	na
symetrickém	symetrický	k2eAgNnSc6d1	symetrické
kopytě	kopyto	k1gNnSc6	kopyto
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
stejný	stejný	k2eAgInSc4d1	stejný
tvar	tvar	k1gInSc4	tvar
kopyta	kopyto	k1gNnSc2	kopyto
pro	pro	k7c4	pro
levou	levý	k2eAgFnSc4d1	levá
i	i	k8xC	i
pravou	pravý	k2eAgFnSc4d1	pravá
nohu	noha	k1gFnSc4	noha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
její	její	k3xOp3gFnSc6	její
výrobě	výroba	k1gFnSc6	výroba
využívali	využívat	k5eAaPmAgMnP	využívat
především	především	k9	především
práce	práce	k1gFnSc2	práce
domácích	domácí	k2eAgMnPc2d1	domácí
dělníků	dělník	k1gMnPc2	dělník
<g/>
.	.	kIx.	.
</s>
<s>
Zaměstnávali	zaměstnávat	k5eAaImAgMnP	zaměstnávat
okolo	okolo	k7c2	okolo
10	[number]	k4	10
dělníků	dělník	k1gMnPc2	dělník
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
museli	muset	k5eAaImAgMnP	muset
pracovat	pracovat	k5eAaImF	pracovat
fixní	fixní	k2eAgFnSc4d1	fixní
pracovní	pracovní	k2eAgFnSc4d1	pracovní
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
za	za	k7c4	za
kterou	který	k3yRgFnSc4	který
dostávali	dostávat	k5eAaImAgMnP	dostávat
pravidelnou	pravidelný	k2eAgFnSc4d1	pravidelná
týdenní	týdenní	k2eAgFnSc4d1	týdenní
mzdu	mzda	k1gFnSc4	mzda
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
řízení	řízení	k1gNnSc2	řízení
podniku	podnik	k1gInSc2	podnik
byl	být	k5eAaImAgMnS	být
na	na	k7c4	na
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
velice	velice	k6eAd1	velice
neobvyklý	obvyklý	k2eNgInSc1d1	neobvyklý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
velmi	velmi	k6eAd1	velmi
průkopnický	průkopnický	k2eAgMnSc1d1	průkopnický
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
1895	[number]	k4	1895
byl	být	k5eAaImAgInS	být
veškerý	veškerý	k3xTgInSc1	veškerý
jejich	jejich	k3xOp3gInSc1	jejich
majetek	majetek	k1gInSc1	majetek
zastaven	zastavit	k5eAaPmNgInS	zastavit
na	na	k7c4	na
splátky	splátka	k1gFnPc4	splátka
a	a	k8xC	a
směnky	směnka	k1gFnPc4	směnka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
už	už	k6eAd1	už
nebylo	být	k5eNaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
platit	platit	k5eAaImF	platit
<g/>
.	.	kIx.	.
</s>
<s>
Začaly	začít	k5eAaPmAgFnP	začít
jim	on	k3xPp3gMnPc3	on
hrozit	hrozit	k5eAaImF	hrozit
žaloby	žaloba	k1gFnPc4	žaloba
ze	z	k7c2	z
stran	strana	k1gFnPc2	strana
věřitelů	věřitel	k1gMnPc2	věřitel
<g/>
.	.	kIx.	.
</s>
<s>
Přišla	přijít	k5eAaPmAgFnS	přijít
krize	krize	k1gFnSc1	krize
a	a	k8xC	a
Baťovi	Baťův	k2eAgMnPc1d1	Baťův
zjistili	zjistit	k5eAaPmAgMnP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
dně	dno	k1gNnSc6	dno
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Antonín	Antonín	k1gMnSc1	Antonín
odešel	odejít	k5eAaPmAgMnS	odejít
"	"	kIx"	"
<g/>
na	na	k7c4	na
vojnu	vojna	k1gFnSc4	vojna
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ujal	ujmout	k5eAaPmAgMnS	ujmout
se	se	k3xPyFc4	se
vedení	vedení	k1gNnSc4	vedení
podniku	podnik	k1gInSc2	podnik
Tomáš	Tomáš	k1gMnSc1	Tomáš
<g/>
.	.	kIx.	.
</s>
<s>
Pokusil	pokusit	k5eAaPmAgMnS	pokusit
se	se	k3xPyFc4	se
zbavit	zbavit	k5eAaPmF	zbavit
dluhů	dluh	k1gInPc2	dluh
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
sám	sám	k3xTgInSc4	sám
popisuje	popisovat	k5eAaImIp3nS	popisovat
těmito	tento	k3xDgFnPc7	tento
slovy	slovo	k1gNnPc7	slovo
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Léta	léto	k1gNnSc2	léto
rozkvětu	rozkvět	k1gInSc2	rozkvět
firmy	firma	k1gFnSc2	firma
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Do	do	k7c2	do
léta	léto	k1gNnSc2	léto
1896	[number]	k4	1896
se	se	k3xPyFc4	se
Tomáš	Tomáš	k1gMnSc1	Tomáš
dluhů	dluh	k1gInPc2	dluh
zbavil	zbavit	k5eAaPmAgInS	zbavit
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
nato	nato	k6eAd1	nato
však	však	k9	však
měla	mít	k5eAaImAgFnS	mít
firmu	firma	k1gFnSc4	firma
postihnout	postihnout	k5eAaPmF	postihnout
další	další	k2eAgFnSc1d1	další
pohroma	pohroma	k1gFnSc1	pohroma
<g/>
.	.	kIx.	.
</s>
<s>
Firma	firma	k1gFnSc1	firma
Koditsch	Koditscha	k1gFnPc2	Koditscha
a	a	k8xC	a
spol.	spol.	k?	spol.
<g/>
,	,	kIx,	,
u	u	k7c2	u
níž	jenž	k3xRgFnSc2	jenž
měli	mít	k5eAaImAgMnP	mít
všichni	všechen	k3xTgMnPc1	všechen
ševci	švec	k1gMnPc1	švec
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Baťových	Baťová	k1gFnPc2	Baťová
<g/>
,	,	kIx,	,
uloženy	uložen	k2eAgFnPc4d1	uložena
směnky	směnka	k1gFnPc4	směnka
<g/>
,	,	kIx,	,
zkrachovala	zkrachovat	k5eAaPmAgFnS	zkrachovat
<g/>
.	.	kIx.	.
</s>
<s>
Firmu	firma	k1gFnSc4	firma
Tomášova	Tomášův	k2eAgMnSc2d1	Tomášův
otce	otec	k1gMnSc2	otec
to	ten	k3xDgNnSc1	ten
zruinovalo	zruinovat	k5eAaPmAgNnS	zruinovat
<g/>
.	.	kIx.	.
</s>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
nechtěl	chtít	k5eNaImAgMnS	chtít
vzdát	vzdát	k5eAaPmF	vzdát
<g/>
.	.	kIx.	.
</s>
<s>
Přišel	přijít	k5eAaPmAgMnS	přijít
s	s	k7c7	s
inovacemi	inovace	k1gFnPc7	inovace
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
tuto	tento	k3xDgFnSc4	tento
situaci	situace	k1gFnSc4	situace
vyřešit	vyřešit	k5eAaPmF	vyřešit
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
začít	začít	k5eAaPmF	začít
šít	šít	k5eAaImF	šít
boty	bota	k1gFnPc4	bota
z	z	k7c2	z
plátna	plátno	k1gNnSc2	plátno
<g/>
.	.	kIx.	.
</s>
<s>
Plátno	plátno	k1gNnSc1	plátno
bylo	být	k5eAaImAgNnS	být
mnohem	mnohem	k6eAd1	mnohem
levnější	levný	k2eAgMnSc1d2	levnější
a	a	k8xC	a
dostupnější	dostupný	k2eAgFnSc1d2	dostupnější
než	než	k8xS	než
pravá	pravý	k2eAgFnSc1d1	pravá
kůže	kůže	k1gFnSc1	kůže
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
baťovky	baťovky	k?	baťovky
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
plátěné	plátěný	k2eAgFnPc1d1	plátěná
boty	bota	k1gFnPc1	bota
s	s	k7c7	s
koženou	kožený	k2eAgFnSc7d1	kožená
podešví	podešev	k1gFnSc7	podešev
a	a	k8xC	a
elegantní	elegantní	k2eAgFnSc7d1	elegantní
špičkou	špička	k1gFnSc7	špička
z	z	k7c2	z
pravé	pravý	k2eAgFnSc2d1	pravá
kůže	kůže	k1gFnSc2	kůže
<g/>
)	)	kIx)	)
začal	začít	k5eAaPmAgInS	začít
být	být	k5eAaImF	být
díky	díky	k7c3	díky
reklamě	reklama	k1gFnSc3	reklama
obrovský	obrovský	k2eAgInSc4d1	obrovský
zájem	zájem	k1gInSc4	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Výroba	výroba	k1gFnSc1	výroba
se	se	k3xPyFc4	se
rozjížděla	rozjíždět	k5eAaImAgFnS	rozjíždět
ve	v	k7c6	v
velkém	velký	k2eAgInSc6d1	velký
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
Baťa	Baťa	k1gMnSc1	Baťa
roku	rok	k1gInSc2	rok
1899	[number]	k4	1899
zakoupil	zakoupit	k5eAaPmAgMnS	zakoupit
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
první	první	k4xOgInPc4	první
šicí	šicí	k2eAgInPc4d1	šicí
stroje	stroj	k1gInPc4	stroj
s	s	k7c7	s
ručním	ruční	k2eAgInSc7d1	ruční
pohonem	pohon	k1gInSc7	pohon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Anna	Anna	k1gFnSc1	Anna
Baťová	Baťová	k1gFnSc1	Baťová
<g/>
,	,	kIx,	,
sestra	sestra	k1gFnSc1	sestra
obou	dva	k4xCgMnPc2	dva
bratrů	bratr	k1gMnPc2	bratr
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zasloužila	zasloužit	k5eAaPmAgFnS	zasloužit
o	o	k7c4	o
prosperitu	prosperita	k1gFnSc4	prosperita
firmy	firma	k1gFnSc2	firma
Baťa	Baťa	k1gMnSc1	Baťa
<g/>
.	.	kIx.	.
</s>
<s>
Převzala	převzít	k5eAaPmAgFnS	převzít
od	od	k7c2	od
bratrů	bratr	k1gMnPc2	bratr
ekonomiku	ekonomika	k1gFnSc4	ekonomika
firmy	firma	k1gFnSc2	firma
a	a	k8xC	a
svým	svůj	k3xOyFgInSc7	svůj
přístupem	přístup	k1gInSc7	přístup
neumožnila	umožnit	k5eNaPmAgFnS	umožnit
zbytečné	zbytečný	k2eAgNnSc4d1	zbytečné
plýtvání	plýtvání	k1gNnSc4	plýtvání
či	či	k8xC	či
nekoncepční	koncepční	k2eNgNnSc4d1	nekoncepční
rozhazování	rozhazování	k1gNnSc4	rozhazování
získaných	získaný	k2eAgInPc2d1	získaný
finančních	finanční	k2eAgInPc2d1	finanční
prostředků	prostředek	k1gInPc2	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Působila	působit	k5eAaImAgFnS	působit
zde	zde	k6eAd1	zde
do	do	k7c2	do
svého	své	k1gNnSc2	své
provdání	provdání	k1gNnSc2	provdání
r	r	kA	r
<g/>
.	.	kIx.	.
1898.	[number]	k4	1898.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1897	[number]	k4	1897
byly	být	k5eAaImAgInP	být
všechny	všechen	k3xTgInPc1	všechen
dluhy	dluh	k1gInPc1	dluh
zaplaceny	zaplacen	k2eAgInPc1d1	zaplacen
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
stavby	stavba	k1gFnSc2	stavba
železniční	železniční	k2eAgFnSc2d1	železniční
trati	trať	k1gFnSc2	trať
z	z	k7c2	z
Otrokovic	Otrokovice	k1gFnPc2	Otrokovice
do	do	k7c2	do
Vizovic	Vizovice	k1gFnPc2	Vizovice
postavil	postavit	k5eAaPmAgMnS	postavit
Tomáš	Tomáš	k1gMnSc1	Tomáš
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
výrobní	výrobní	k2eAgFnSc4d1	výrobní
budovu	budova	k1gFnSc4	budova
<g/>
,	,	kIx,	,
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
zaměstnával	zaměstnávat	k5eAaImAgMnS	zaměstnávat
120	[number]	k4	120
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
odkoupil	odkoupit	k5eAaPmAgMnS	odkoupit
další	další	k2eAgFnPc4d1	další
pozemky	pozemka	k1gFnPc4	pozemka
na	na	k7c6	na
území	území	k1gNnSc6	území
města	město	k1gNnSc2	město
Zlína	Zlín	k1gInSc2	Zlín
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
začal	začít	k5eAaPmAgInS	začít
rozšiřovat	rozšiřovat	k5eAaImF	rozšiřovat
firemní	firemní	k2eAgFnSc4d1	firemní
infrastrukturu	infrastruktura	k1gFnSc4	infrastruktura
<g/>
.	.	kIx.	.
</s>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Baťa	Baťa	k1gMnSc1	Baťa
firmu	firma	k1gFnSc4	firma
přejmenoval	přejmenovat	k5eAaPmAgMnS	přejmenovat
ke	k	k7c3	k
dni	den	k1gInSc3	den
1.	[number]	k4	1.
srpna	srpen	k1gInSc2	srpen
1900	[number]	k4	1900
na	na	k7c4	na
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
společnost	společnost	k1gFnSc4	společnost
T	T	kA	T
<g/>
&	&	k?	&
<g/>
A	a	k9	a
Baťa	Baťa	k1gMnSc1	Baťa
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc4d1	hlavní
činnost	činnost	k1gFnSc4	činnost
byla	být	k5eAaImAgFnS	být
výroba	výroba	k1gFnSc1	výroba
plátěné	plátěný	k2eAgFnSc2d1	plátěná
a	a	k8xC	a
houněné	houněný	k2eAgFnSc2d1	houněný
obuvi	obuv	k1gFnSc2	obuv
<g/>
.	.	kIx.	.
</s>
<s>
Firma	firma	k1gFnSc1	firma
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
rozkvětu	rozkvět	k1gInSc6	rozkvět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
její	její	k3xOp3gMnSc1	její
spoluzakladatel	spoluzakladatel	k1gMnSc1	spoluzakladatel
a	a	k8xC	a
bratr	bratr	k1gMnSc1	bratr
Tomáše	Tomáš	k1gMnSc2	Tomáš
Antonín	Antonín	k1gMnSc1	Antonín
Baťa	Baťa	k1gMnSc1	Baťa
ml	ml	kA	ml
<g/>
.	.	kIx.	.
vážně	vážně	k6eAd1	vážně
onemocněl	onemocnět	k5eAaPmAgMnS	onemocnět
tuberkulózou	tuberkulóza	k1gFnSc7	tuberkulóza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Za	za	k7c7	za
zkušenostmi	zkušenost	k1gFnPc7	zkušenost
do	do	k7c2	do
USA	USA	kA	USA
==	==	k?	==
</s>
</p>
<p>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Baťa	Baťa	k1gMnSc1	Baťa
přenechal	přenechat	k5eAaPmAgMnS	přenechat
na	na	k7c4	na
čas	čas	k1gInSc4	čas
vedení	vedení	k1gNnSc4	vedení
podniku	podnik	k1gInSc6	podnik
svému	svůj	k3xOyFgMnSc3	svůj
spolupracovníku	spolupracovník	k1gMnSc3	spolupracovník
Štěpánkovi	Štěpánek	k1gMnSc3	Štěpánek
a	a	k8xC	a
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
se	se	k3xPyFc4	se
odjet	odjet	k5eAaPmF	odjet
do	do	k7c2	do
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
získat	získat	k5eAaPmF	získat
nové	nový	k2eAgFnPc4d1	nová
zkušenosti	zkušenost	k1gFnPc4	zkušenost
se	s	k7c7	s
způsoby	způsob	k1gInPc7	způsob
organizace	organizace	k1gFnSc2	organizace
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
výplaty	výplata	k1gFnSc2	výplata
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
a	a	k8xC	a
uskladnění	uskladnění	k1gNnSc4	uskladnění
polotovarů	polotovar	k1gInPc2	polotovar
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
obuvi	obuv	k1gFnSc2	obuv
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skoro	skoro	k6eAd1	skoro
půlročním	půlroční	k2eAgInSc6d1	půlroční
pobytu	pobyt	k1gInSc6	pobyt
v	v	k7c6	v
USA	USA	kA	USA
přijel	přijet	k5eAaPmAgMnS	přijet
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1905	[number]	k4	1905
a	a	k8xC	a
do	do	k7c2	do
Zlína	Zlín	k1gInSc2	Zlín
přivezl	přivézt	k5eAaPmAgMnS	přivézt
s	s	k7c7	s
sebou	se	k3xPyFc7	se
nové	nový	k2eAgInPc4d1	nový
plány	plán	k1gInPc4	plán
výstavby	výstavba	k1gFnSc2	výstavba
továrních	tovární	k2eAgFnPc2d1	tovární
budov	budova	k1gFnPc2	budova
i	i	k8xC	i
nadšení	nadšení	k1gNnSc2	nadšení
pro	pro	k7c4	pro
americký	americký	k2eAgInSc4d1	americký
směr	směr	k1gInSc4	směr
managementu	management	k1gInSc2	management
<g/>
.	.	kIx.	.
</s>
<s>
Objednal	objednat	k5eAaPmAgInS	objednat
též	též	k9	též
nové	nový	k2eAgInPc4d1	nový
výkonnější	výkonný	k2eAgInPc4d2	výkonnější
stroje	stroj	k1gInPc4	stroj
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
návratu	návrat	k1gInSc6	návrat
začal	začít	k5eAaPmAgMnS	začít
také	také	k9	také
Baťa	Baťa	k1gMnSc1	Baťa
stupňovat	stupňovat	k5eAaImF	stupňovat
požadavky	požadavek	k1gInPc4	požadavek
na	na	k7c4	na
dělníky	dělník	k1gMnPc4	dělník
<g/>
:	:	kIx,	:
za	za	k7c2	za
špatně	špatně	k6eAd1	špatně
provedenou	provedený	k2eAgFnSc4d1	provedená
práci	práce	k1gFnSc4	práce
jim	on	k3xPp3gMnPc3	on
udílel	udílet	k5eAaImAgMnS	udílet
pokuty	pokuta	k1gFnPc4	pokuta
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
srážek	srážka	k1gFnPc2	srážka
ze	z	k7c2	z
mzdy	mzda	k1gFnSc2	mzda
<g/>
.	.	kIx.	.
</s>
<s>
Nepřistoupil	přistoupit	k5eNaPmAgMnS	přistoupit
na	na	k7c4	na
požadavky	požadavek	k1gInPc4	požadavek
sociálně-demokraticky	sociálněemokraticky	k6eAd1	sociálně-demokraticky
orientované	orientovaný	k2eAgFnSc2d1	orientovaná
odborové	odborový	k2eAgFnSc2d1	odborová
organizace	organizace	k1gFnSc2	organizace
<g/>
,	,	kIx,	,
následkem	následkem	k7c2	následkem
čehož	což	k3yQnSc2	což
se	se	k3xPyFc4	se
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
stávka	stávka	k1gFnSc1	stávka
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
.	.	kIx.	.
</s>
<s>
Baťa	Baťa	k1gMnSc1	Baťa
nastalou	nastalý	k2eAgFnSc4d1	nastalá
situaci	situace	k1gFnSc4	situace
vyřešil	vyřešit	k5eAaPmAgMnS	vyřešit
výpověďmi	výpověď	k1gFnPc7	výpověď
pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
stávkující	stávkující	k1gMnPc4	stávkující
a	a	k8xC	a
na	na	k7c4	na
jejich	jejich	k3xOp3gNnSc4	jejich
místo	místo	k1gNnSc4	místo
přijal	přijmout	k5eAaPmAgMnS	přijmout
nové	nový	k2eAgMnPc4d1	nový
<g/>
,	,	kIx,	,
nekvalifikované	kvalifikovaný	k2eNgMnPc4d1	nekvalifikovaný
pracovníky	pracovník	k1gMnPc4	pracovník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Léta	léto	k1gNnPc4	léto
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
firmy	firma	k1gFnSc2	firma
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
zemřel	zemřít	k5eAaPmAgMnS	zemřít
těžce	těžce	k6eAd1	těžce
nemocný	mocný	k2eNgMnSc1d1	nemocný
Antonín	Antonín	k1gMnSc1	Antonín
Baťa	Baťa	k1gMnSc1	Baťa
<g/>
.	.	kIx.	.
</s>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
zůstal	zůstat	k5eAaPmAgMnS	zůstat
jediným	jediný	k2eAgMnSc7d1	jediný
vlastníkem	vlastník	k1gMnSc7	vlastník
firmy	firma	k1gFnSc2	firma
T	T	kA	T
<g/>
&	&	k?	&
<g/>
A	a	k9	a
Baťa	Baťa	k1gMnSc1	Baťa
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
hlavním	hlavní	k2eAgInSc7d1	hlavní
cílem	cíl	k1gInSc7	cíl
zůstávala	zůstávat	k5eAaImAgFnS	zůstávat
výroba	výroba	k1gFnSc1	výroba
lehkých	lehký	k2eAgFnPc2d1	lehká
baťovek	baťovek	k?	baťovek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
podniku	podnik	k1gInSc6	podnik
zaměstnáno	zaměstnat	k5eAaPmNgNnS	zaměstnat
asi	asi	k9	asi
350	[number]	k4	350
dělníků	dělník	k1gMnPc2	dělník
<g/>
,	,	kIx,	,
denně	denně	k6eAd1	denně
se	se	k3xPyFc4	se
vyrobilo	vyrobit	k5eAaPmAgNnS	vyrobit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
3000	[number]	k4	3000
párů	pár	k1gInPc2	pár
bot	bota	k1gFnPc2	bota
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rostoucím	rostoucí	k2eAgInSc7d1	rostoucí
objemem	objem	k1gInSc7	objem
produkce	produkce	k1gFnSc2	produkce
rostla	růst	k5eAaImAgFnS	růst
také	také	k9	také
imigrace	imigrace	k1gFnSc1	imigrace
nových	nový	k2eAgFnPc2d1	nová
pracovních	pracovní	k2eAgFnPc2d1	pracovní
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Nastal	nastat	k5eAaPmAgInS	nastat
problém	problém	k1gInSc1	problém
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
všechny	všechen	k3xTgMnPc4	všechen
dělníky	dělník	k1gMnPc4	dělník
ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
ubytovat	ubytovat	k5eAaPmF	ubytovat
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
Baťa	Baťa	k1gMnSc1	Baťa
začal	začít	k5eAaPmAgMnS	začít
s	s	k7c7	s
výstavbou	výstavba	k1gFnSc7	výstavba
tzv.	tzv.	kA	tzv.
Baťových	Baťových	k2eAgInPc2d1	Baťových
domků	domek	k1gInPc2	domek
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
další	další	k2eAgFnPc4d1	další
budovy	budova	k1gFnPc4	budova
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
dnes	dnes	k6eAd1	dnes
tvoří	tvořit	k5eAaImIp3nP	tvořit
charakteristickou	charakteristický	k2eAgFnSc4d1	charakteristická
architekturu	architektura	k1gFnSc4	architektura
celého	celý	k2eAgNnSc2d1	celé
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
přešly	přejít	k5eAaPmAgFnP	přejít
obuvnické	obuvnický	k2eAgFnPc1d1	obuvnická
dílny	dílna	k1gFnPc1	dílna
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
celokožené	celokožený	k2eAgFnSc2d1	celokožená
obuvi	obuv	k1gFnSc2	obuv
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
také	také	k9	také
prováděny	provádět	k5eAaImNgInP	provádět
opatření	opatření	k1gNnPc1	opatření
k	k	k7c3	k
prohloubení	prohloubení	k1gNnSc3	prohloubení
racionalizace	racionalizace	k1gFnSc2	racionalizace
a	a	k8xC	a
intenzifikace	intenzifikace	k1gFnSc2	intenzifikace
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Firma	firma	k1gFnSc1	firma
uzavírala	uzavírat	k5eAaImAgFnS	uzavírat
s	s	k7c7	s
dělníky	dělník	k1gMnPc7	dělník
pracovní	pracovní	k2eAgFnSc2d1	pracovní
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
nichž	jenž	k3xRgFnPc2	jenž
dělníkům	dělník	k1gMnPc3	dělník
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
nedosáhli	dosáhnout	k5eNaPmAgMnP	dosáhnout
předepsaného	předepsaný	k2eAgInSc2d1	předepsaný
pracovního	pracovní	k2eAgInSc2d1	pracovní
výkonu	výkon	k1gInSc2	výkon
<g/>
,	,	kIx,	,
účtovala	účtovat	k5eAaImAgFnS	účtovat
k	k	k7c3	k
náhradě	náhrada	k1gFnSc3	náhrada
tzv.	tzv.	kA	tzv.
ztráty	ztráta	k1gFnSc2	ztráta
na	na	k7c6	na
režiích	režie	k1gFnPc6	režie
<g/>
.	.	kIx.	.
</s>
<s>
Jinak	jinak	k6eAd1	jinak
byli	být	k5eAaImAgMnP	být
dělníci	dělník	k1gMnPc1	dělník
pokutováni	pokutovat	k5eAaImNgMnP	pokutovat
za	za	k7c4	za
nedostatečně	dostatečně	k6eNd1	dostatečně
a	a	k8xC	a
špatně	špatně	k6eAd1	špatně
provedenou	provedený	k2eAgFnSc4d1	provedená
práci	práce	k1gFnSc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
Tomáš	Tomáš	k1gMnSc1	Tomáš
Baťa	Baťa	k1gMnSc1	Baťa
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
dcerou	dcera	k1gFnSc7	dcera
správce	správce	k1gMnSc2	správce
vídeňské	vídeňský	k2eAgFnSc2d1	Vídeňská
Dvorské	Dvorské	k2eAgFnSc2d1	Dvorské
knihovny	knihovna	k1gFnSc2	knihovna
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
Menčíka	Menčík	k1gMnSc2	Menčík
Marií	Maria	k1gFnSc7	Maria
Menčíkovou	Menčíkův	k2eAgFnSc7d1	Menčíkův
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dvou	dva	k4xCgNnPc6	dva
letech	léto	k1gNnPc6	léto
přišel	přijít	k5eAaPmAgInS	přijít
na	na	k7c4	na
svět	svět	k1gInSc4	svět
jeho	jeho	k3xOp3gFnSc4	jeho
jediný	jediný	k2eAgMnSc1d1	jediný
syn	syn	k1gMnSc1	syn
Tomáš	Tomáš	k1gMnSc1	Tomáš
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Válečné	válečný	k2eAgNnSc4d1	válečné
období	období	k1gNnSc4	období
==	==	k?	==
</s>
</p>
<p>
<s>
Hned	hned	k6eAd1	hned
na	na	k7c6	na
začátku	začátek	k1gInSc2	začátek
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
dostala	dostat	k5eAaPmAgFnS	dostat
firma	firma	k1gFnSc1	firma
zakázku	zakázka	k1gFnSc4	zakázka
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
50 000	[number]	k4	50 000
párů	pár	k1gInPc2	pár
vojenských	vojenský	k2eAgFnPc2d1	vojenská
bot	bota	k1gFnPc2	bota
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
bagančat	baganče	k1gNnPc2	baganče
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
pracovníků	pracovník	k1gMnPc2	pracovník
i	i	k9	i
denní	denní	k2eAgInSc1d1	denní
pracovní	pracovní	k2eAgInSc1d1	pracovní
výkon	výkon	k1gInSc1	výkon
rychle	rychle	k6eAd1	rychle
rostly	růst	k5eAaImAgInP	růst
<g/>
.	.	kIx.	.
</s>
<s>
Stovkám	stovka	k1gFnPc3	stovka
mužů	muž	k1gMnPc2	muž
se	se	k3xPyFc4	se
pomocí	pomocí	k7c2	pomocí
práce	práce	k1gFnSc2	práce
v	v	k7c6	v
továrně	továrna	k1gFnSc6	továrna
podařilo	podařit	k5eAaPmAgNnS	podařit
vyhnout	vyhnout	k5eAaPmF	vyhnout
se	se	k3xPyFc4	se
povolávacím	povolávací	k2eAgInPc3d1	povolávací
rozkazům	rozkaz	k1gInPc3	rozkaz
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
firma	firma	k1gFnSc1	firma
vyráběla	vyrábět	k5eAaImAgFnS	vyrábět
pro	pro	k7c4	pro
rakousko-uherskou	rakouskoherský	k2eAgFnSc4d1	rakousko-uherská
armádu	armáda	k1gFnSc4	armáda
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
válečným	válečný	k2eAgInPc3d1	válečný
poměrům	poměr	k1gInPc3	poměr
patřilo	patřit	k5eAaImAgNnS	patřit
také	také	k9	také
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
podniku	podnik	k1gInSc6	podnik
pracovala	pracovat	k5eAaImAgFnS	pracovat
skupina	skupina	k1gFnSc1	skupina
ruských	ruský	k2eAgMnPc2d1	ruský
válečných	válečný	k2eAgMnPc2d1	válečný
zajatců	zajatec	k1gMnPc2	zajatec
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1914	[number]	k4	1914
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
Baťových	Baťových	k2eAgMnPc2d1	Baťových
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
desetinásobně	desetinásobně	k6eAd1	desetinásobně
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
války	válka	k1gFnSc2	válka
činila	činit	k5eAaImAgFnS	činit
denní	denní	k2eAgFnSc1d1	denní
výroba	výroba	k1gFnSc1	výroba
téměř	téměř	k6eAd1	téměř
6 000	[number]	k4	6 000
párů	pár	k1gInPc2	pár
obuvi	obuv	k1gFnSc2	obuv
a	a	k8xC	a
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
polovina	polovina	k1gFnSc1	polovina
armádních	armádní	k2eAgFnPc2d1	armádní
bot	bota	k1gFnPc2	bota
byla	být	k5eAaImAgFnS	být
právě	právě	k6eAd1	právě
odsud	odsud	k6eAd1	odsud
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
zřízena	zřídit	k5eAaPmNgFnS	zřídit
vlastní	vlastní	k2eAgFnSc1d1	vlastní
koželužna	koželužna	k1gFnSc1	koželužna
a	a	k8xC	a
zakoupeny	zakoupen	k2eAgInPc1d1	zakoupen
velkostatky	velkostatek	k1gInPc1	velkostatek
pro	pro	k7c4	pro
zásobování	zásobování	k1gNnSc4	zásobování
dřevem	dřevo	k1gNnSc7	dřevo
a	a	k8xC	a
potravinami	potravina	k1gFnPc7	potravina
pro	pro	k7c4	pro
zaměstnance	zaměstnanec	k1gMnPc4	zaměstnanec
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastní	k2eAgFnSc7d1	vlastní
výrobou	výroba	k1gFnSc7	výroba
surovin	surovina	k1gFnPc2	surovina
firma	firma	k1gFnSc1	firma
ušetřila	ušetřit	k5eAaPmAgFnS	ušetřit
na	na	k7c6	na
nákladech	náklad	k1gInPc6	náklad
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
stejného	stejný	k2eAgInSc2d1	stejný
důvodu	důvod	k1gInSc2	důvod
začala	začít	k5eAaPmAgFnS	začít
otevírat	otevírat	k5eAaImF	otevírat
vlastní	vlastní	k2eAgFnPc4d1	vlastní
prodejny	prodejna	k1gFnPc4	prodejna
nejen	nejen	k6eAd1	nejen
ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
státě	stát	k1gInSc6	stát
<g/>
:	:	kIx,	:
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
,	,	kIx,	,
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
a	a	k8xC	a
dalších	další	k2eAgNnPc6d1	další
městech	město	k1gNnPc6	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Poválečná	poválečný	k2eAgFnSc1d1	poválečná
krize	krize	k1gFnSc1	krize
==	==	k?	==
</s>
</p>
<p>
<s>
Bezprostředně	bezprostředně	k6eAd1	bezprostředně
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
války	válka	k1gFnSc2	válka
byla	být	k5eAaImAgFnS	být
firma	firma	k1gFnSc1	firma
postižena	postihnout	k5eAaPmNgFnS	postihnout
odbytovou	odbytový	k2eAgFnSc7d1	odbytová
<g/>
,	,	kIx,	,
výrobní	výrobní	k2eAgFnSc7d1	výrobní
a	a	k8xC	a
finanční	finanční	k2eAgFnSc7d1	finanční
krizí	krize	k1gFnSc7	krize
<g/>
,	,	kIx,	,
způsobenou	způsobený	k2eAgFnSc7d1	způsobená
ztrátou	ztráta	k1gFnSc7	ztráta
válečných	válečný	k2eAgFnPc2d1	válečná
dodávek	dodávka	k1gFnPc2	dodávka
<g/>
,	,	kIx,	,
stagnujícím	stagnující	k2eAgInSc7d1	stagnující
zahraničním	zahraniční	k2eAgInSc7d1	zahraniční
obchodem	obchod	k1gInSc7	obchod
a	a	k8xC	a
sníženou	snížený	k2eAgFnSc7d1	snížená
koupěschopností	koupěschopnost	k1gFnSc7	koupěschopnost
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
začala	začít	k5eAaPmAgFnS	začít
firma	firma	k1gFnSc1	firma
tuto	tento	k3xDgFnSc4	tento
krizi	krize	k1gFnSc4	krize
řešit	řešit	k5eAaImF	řešit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zřizovala	zřizovat	k5eAaImAgFnS	zřizovat
osobní	osobní	k2eAgInPc4d1	osobní
účty	účet	k1gInPc4	účet
svým	svůj	k3xOyFgMnPc3	svůj
zaměstnancům	zaměstnanec	k1gMnPc3	zaměstnanec
z	z	k7c2	z
jejich	jejich	k3xOp3gFnPc2	jejich
vlastních	vlastní	k2eAgFnPc2d1	vlastní
mezd	mzda	k1gFnPc2	mzda
a	a	k8xC	a
vkladů	vklad	k1gInPc2	vklad
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
konta	konto	k1gNnPc1	konto
byla	být	k5eAaImAgNnP	být
úročena	úročit	k5eAaImNgNnP	úročit
10	[number]	k4	10
<g/>
%	%	kIx~	%
úrokovou	úrokový	k2eAgFnSc7d1	úroková
mírou	míra	k1gFnSc7	míra
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
naspořené	naspořený	k2eAgInPc1d1	naspořený
peníze	peníz	k1gInPc1	peníz
byly	být	k5eAaImAgInP	být
použity	použít	k5eAaPmNgInP	použít
jako	jako	k8xS	jako
investice	investice	k1gFnPc1	investice
do	do	k7c2	do
provozního	provozní	k2eAgInSc2d1	provozní
kapitálu	kapitál	k1gInSc2	kapitál
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
zaměstnanci	zaměstnanec	k1gMnPc1	zaměstnanec
si	se	k3xPyFc3	se
po	po	k7c6	po
udání	udání	k1gNnSc6	udání
důvodu	důvod	k1gInSc2	důvod
mohli	moct	k5eAaImAgMnP	moct
tyto	tento	k3xDgInPc4	tento
účty	účet	k1gInPc4	účet
vybrat	vybrat	k5eAaPmF	vybrat
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
řešení	řešení	k1gNnSc1	řešení
krize	krize	k1gFnSc2	krize
mělo	mít	k5eAaImAgNnS	mít
jen	jen	k9	jen
dočasný	dočasný	k2eAgInSc4d1	dočasný
charakter	charakter	k1gInSc4	charakter
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
v	v	k7c6	v
Baťových	Baťových	k2eAgInPc6d1	Baťových
závodech	závod	k1gInPc6	závod
stávkovat	stávkovat	k5eAaImF	stávkovat
<g/>
.	.	kIx.	.
</s>
<s>
Baťa	Baťa	k1gMnSc1	Baťa
již	již	k6eAd1	již
nemohl	moct	k5eNaImAgMnS	moct
tuto	tento	k3xDgFnSc4	tento
stávku	stávka	k1gFnSc4	stávka
potlačit	potlačit	k5eAaPmF	potlačit
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
nechal	nechat	k5eAaPmAgMnS	nechat
založit	založit	k5eAaPmF	založit
odborové	odborový	k2eAgFnSc2d1	odborová
organizace	organizace	k1gFnSc2	organizace
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
si	se	k3xPyFc3	se
mohli	moct	k5eAaImAgMnP	moct
dělníci	dělník	k1gMnPc1	dělník
volit	volit	k5eAaImF	volit
své	svůj	k3xOyFgMnPc4	svůj
zástupce	zástupce	k1gMnPc4	zástupce
<g/>
.	.	kIx.	.
</s>
<s>
Krize	krize	k1gFnSc1	krize
vyvrcholila	vyvrcholit	k5eAaPmAgFnS	vyvrcholit
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
celonárodní	celonárodní	k2eAgFnSc3d1	celonárodní
dělnické	dělnický	k2eAgFnSc3d1	Dělnická
stávce	stávka	k1gFnSc3	stávka
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
zapříčinilo	zapříčinit	k5eAaPmAgNnS	zapříčinit
vznik	vznik	k1gInSc4	vznik
Komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Tovární	tovární	k2eAgInPc1d1	tovární
sklady	sklad	k1gInPc1	sklad
byly	být	k5eAaImAgInP	být
zaplněny	zaplnit	k5eAaPmNgInP	zaplnit
zbožím	zboží	k1gNnSc7	zboží
<g/>
,	,	kIx,	,
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
uložený	uložený	k2eAgInSc1d1	uložený
kapitál	kapitál	k1gInSc1	kapitál
se	se	k3xPyFc4	se
nehýbal	hýbat	k5eNaImAgInS	hýbat
a	a	k8xC	a
chyběl	chybět	k5eAaImAgInS	chybět
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
Tomáš	Tomáš	k1gMnSc1	Tomáš
Baťa	Baťa	k1gMnSc1	Baťa
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
k	k	k7c3	k
odvážnému	odvážný	k2eAgMnSc3d1	odvážný
a	a	k8xC	a
důmyslnému	důmyslný	k2eAgInSc3d1	důmyslný
kroku	krok	k1gInSc3	krok
<g/>
.	.	kIx.	.
</s>
<s>
Dvakrát	dvakrát	k6eAd1	dvakrát
snížil	snížit	k5eAaPmAgInS	snížit
cenu	cena	k1gFnSc4	cena
obuvi	obuv	k1gFnSc2	obuv
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
chtěl	chtít	k5eAaImAgMnS	chtít
vyprodat	vyprodat	k5eAaPmF	vyprodat
sklady	sklad	k1gInPc4	sklad
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
snížil	snížit	k5eAaPmAgMnS	snížit
ceny	cena	k1gFnPc4	cena
o	o	k7c4	o
50	[number]	k4	50
%	%	kIx~	%
a	a	k8xC	a
mzdy	mzda	k1gFnPc4	mzda
o	o	k7c4	o
40	[number]	k4	40
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Pracovníkům	pracovník	k1gMnPc3	pracovník
to	ten	k3xDgNnSc4	ten
vykompenzoval	vykompenzovat	k5eAaPmAgMnS	vykompenzovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jim	on	k3xPp3gMnPc3	on
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
určité	určitý	k2eAgFnPc4d1	určitá
slevy	sleva	k1gFnPc4	sleva
(	(	kIx(	(
<g/>
např.	např.	kA	např.
50	[number]	k4	50
<g/>
%	%	kIx~	%
zlevnění	zlevnění	k1gNnSc3	zlevnění
zboží	zboží	k1gNnSc2	zboží
ve	v	k7c6	v
firemním	firemní	k2eAgInSc6d1	firemní
konzumu	konzum	k1gInSc6	konzum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poloviční	poloviční	k2eAgFnPc1d1	poloviční
ceny	cena	k1gFnPc1	cena
působily	působit	k5eAaImAgFnP	působit
jako	jako	k9	jako
magnet	magnet	k1gInSc1	magnet
na	na	k7c4	na
zákazníky	zákazník	k1gMnPc4	zákazník
<g/>
.	.	kIx.	.
</s>
<s>
Zásoby	zásoba	k1gFnPc1	zásoba
bot	bota	k1gFnPc2	bota
se	se	k3xPyFc4	se
výborně	výborně	k6eAd1	výborně
prodávaly	prodávat	k5eAaImAgFnP	prodávat
a	a	k8xC	a
Baťa	Baťa	k1gMnSc1	Baťa
inkasoval	inkasovat	k5eAaBmAgMnS	inkasovat
deflací	deflace	k1gFnPc2	deflace
drahocenně	drahocenně	k6eAd1	drahocenně
zhodnocené	zhodnocený	k2eAgInPc1d1	zhodnocený
peníze	peníz	k1gInPc1	peníz
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgNnSc7	tento
opatřením	opatření	k1gNnSc7	opatření
prorazil	prorazit	k5eAaPmAgInS	prorazit
krizové	krizový	k2eAgNnSc4d1	krizové
sevření	sevření	k1gNnSc4	sevření
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
levné	levný	k2eAgFnPc1d1	levná
boty	bota	k1gFnPc1	bota
začaly	začít	k5eAaPmAgFnP	začít
ovládat	ovládat	k5eAaImF	ovládat
trh	trh	k1gInSc4	trh
<g/>
.	.	kIx.	.
</s>
<s>
Čtyři	čtyři	k4xCgNnPc4	čtyři
písmena	písmeno	k1gNnPc4	písmeno
BAŤA	Baťa	k1gMnSc1	Baťa
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
všudypřítomným	všudypřítomný	k2eAgInSc7d1	všudypřítomný
symbolem	symbol	k1gInSc7	symbol
odvážného	odvážný	k2eAgNnSc2d1	odvážné
a	a	k8xC	a
úspěšného	úspěšný	k2eAgNnSc2d1	úspěšné
podnikání	podnikání	k1gNnSc2	podnikání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
měla	mít	k5eAaImAgFnS	mít
síť	síť	k1gFnSc1	síť
prodejen	prodejna	k1gFnPc2	prodejna
Baťa	Baťa	k1gMnSc1	Baťa
již	již	k9	již
112	[number]	k4	112
poboček	pobočka	k1gFnPc2	pobočka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
Baťa	Baťa	k1gMnSc1	Baťa
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
sestavit	sestavit	k5eAaPmF	sestavit
svoji	svůj	k3xOyFgFnSc4	svůj
kandidátní	kandidátní	k2eAgFnSc4d1	kandidátní
listinu	listina	k1gFnSc4	listina
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
obecního	obecní	k2eAgNnSc2d1	obecní
zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
s	s	k7c7	s
heslem	heslo	k1gNnSc7	heslo
"	"	kIx"	"
<g/>
Chci	chtít	k5eAaImIp1nS	chtít
pracovat	pracovat	k5eAaImF	pracovat
pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
<g/>
.	.	kIx.	.
</s>
<s>
Bojovat	bojovat	k5eAaImF	bojovat
proti	proti	k7c3	proti
bídě	bída	k1gFnSc3	bída
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Volby	volba	k1gFnSc2	volba
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgMnS	získat
17	[number]	k4	17
mandátů	mandát	k1gInPc2	mandát
ze	z	k7c2	z
30	[number]	k4	30
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
starostou	starosta	k1gMnSc7	starosta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Plánovité	plánovitý	k2eAgNnSc1d1	plánovité
hospodářství	hospodářství	k1gNnSc1	hospodářství
==	==	k?	==
</s>
</p>
<p>
<s>
Noví	nový	k2eAgMnPc1d1	nový
dělníci	dělník	k1gMnPc1	dělník
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
do	do	k7c2	do
Zlína	Zlín	k1gInSc2	Zlín
přicházeli	přicházet	k5eAaImAgMnP	přicházet
ve	v	k7c6	v
dvacátých	dvacátý	k4xOgNnPc6	dvacátý
letech	léto	k1gNnPc6	léto
20.	[number]	k4	20.
století	století	k1gNnSc2	století
za	za	k7c2	za
prací	práce	k1gFnPc2	práce
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
okamžitě	okamžitě	k6eAd1	okamžitě
přijati	přijmout	k5eAaPmNgMnP	přijmout
na	na	k7c4	na
pracovní	pracovní	k2eAgFnSc4d1	pracovní
smlouvu	smlouva	k1gFnSc4	smlouva
do	do	k7c2	do
hlavní	hlavní	k2eAgFnSc2d1	hlavní
výroby	výroba	k1gFnSc2	výroba
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
nastoupili	nastoupit	k5eAaPmAgMnP	nastoupit
dle	dle	k7c2	dle
vlastní	vlastní	k2eAgFnSc2d1	vlastní
specializace	specializace	k1gFnSc2	specializace
–	–	k?	–
stavaři	stavař	k1gMnPc1	stavař
<g/>
,	,	kIx,	,
elektrikáři	elektrikář	k1gMnPc1	elektrikář
<g/>
,	,	kIx,	,
chemici	chemik	k1gMnPc1	chemik
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
do	do	k7c2	do
dalších	další	k2eAgNnPc2d1	další
<g/>
,	,	kIx,	,
nově	nově	k6eAd1	nově
tvořících	tvořící	k2eAgInPc2d1	tvořící
se	se	k3xPyFc4	se
provozů	provoz	k1gInPc2	provoz
firmy	firma	k1gFnSc2	firma
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
nejlepší	dobrý	k2eAgMnPc1d3	nejlepší
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
podniku	podnik	k1gInSc2	podnik
měli	mít	k5eAaImAgMnP	mít
možnost	možnost	k1gFnSc4	možnost
po	po	k7c6	po
pracovní	pracovní	k2eAgFnSc6d1	pracovní
době	doba	k1gFnSc6	doba
navštěvovat	navštěvovat	k5eAaImF	navštěvovat
Baťovu	Baťův	k2eAgFnSc4d1	Baťova
školu	škola	k1gFnSc4	škola
práce	práce	k1gFnSc2	práce
k	k	k7c3	k
získání	získání	k1gNnSc3	získání
vyšší	vysoký	k2eAgFnSc2d2	vyšší
kvalifikace	kvalifikace	k1gFnSc2	kvalifikace
<g/>
,	,	kIx,	,
vyšší	vysoký	k2eAgFnSc2d2	vyšší
odbornosti	odbornost	k1gFnSc2	odbornost
<g/>
,	,	kIx,	,
mohli	moct	k5eAaImAgMnP	moct
se	se	k3xPyFc4	se
věnovat	věnovat	k5eAaImF	věnovat
výuce	výuka	k1gFnSc3	výuka
cizích	cizí	k2eAgInPc2d1	cizí
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
škole	škola	k1gFnSc6	škola
viselo	viset	k5eAaImAgNnS	viset
Baťovo	Baťův	k2eAgNnSc1d1	Baťovo
heslo	heslo	k1gNnSc1	heslo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Neříkej	říkat	k5eNaImRp2nS	říkat
mi	já	k3xPp1nSc3	já
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
nejde	jít	k5eNaImIp3nS	jít
–	–	k?	–
řekni	říct	k5eAaPmRp2nS	říct
mi	já	k3xPp1nSc3	já
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
neumíš	umět	k5eNaImIp2nS	umět
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vše	všechen	k3xTgNnSc1	všechen
vedlo	vést	k5eAaImAgNnS	vést
postupně	postupně	k6eAd1	postupně
k	k	k7c3	k
získávání	získávání	k1gNnSc3	získávání
řídicích	řídicí	k2eAgFnPc2d1	řídicí
a	a	k8xC	a
manažerských	manažerský	k2eAgFnPc2d1	manažerská
funkcí	funkce	k1gFnPc2	funkce
jak	jak	k8xC	jak
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
v	v	k7c6	v
nově	nově	k6eAd1	nově
zakládaných	zakládaný	k2eAgFnPc6d1	zakládaná
pobočkách	pobočka	k1gFnPc6	pobočka
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
Baťa	Baťa	k1gMnSc1	Baťa
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgMnS	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
orientovat	orientovat	k5eAaBmF	orientovat
na	na	k7c4	na
zahraniční	zahraniční	k2eAgInSc4d1	zahraniční
obchod	obchod	k1gInSc4	obchod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
budoval	budovat	k5eAaImAgMnS	budovat
obchodní	obchodní	k2eAgFnSc4d1	obchodní
síť	síť	k1gFnSc4	síť
prodejen	prodejna	k1gFnPc2	prodejna
<g/>
.	.	kIx.	.
</s>
<s>
Prodával	prodávat	k5eAaImAgMnS	prodávat
za	za	k7c4	za
ceny	cena	k1gFnPc4	cena
pod	pod	k7c7	pod
cenovou	cenový	k2eAgFnSc7d1	cenová
úrovní	úroveň	k1gFnSc7	úroveň
konkurence	konkurence	k1gFnSc2	konkurence
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
ji	on	k3xPp3gFnSc4	on
dokázal	dokázat	k5eAaPmAgInS	dokázat
likvidovat	likvidovat	k5eAaBmF	likvidovat
<g/>
.	.	kIx.	.
</s>
<s>
Výroba	výroba	k1gFnSc1	výroba
vzrůstala	vzrůstat	k5eAaImAgFnS	vzrůstat
<g/>
,	,	kIx,	,
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
1925	[number]	k4	1925
pracovalo	pracovat	k5eAaImAgNnS	pracovat
v	v	k7c6	v
Baťově	Baťův	k2eAgInSc6d1	Baťův
koncernu	koncern	k1gInSc6	koncern
5 200	[number]	k4	5 200
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
sestaveném	sestavený	k2eAgInSc6d1	sestavený
desetiletém	desetiletý	k2eAgInSc6d1	desetiletý
plánu	plán	k1gInSc6	plán
předpovídal	předpovídat	k5eAaImAgMnS	předpovídat
denní	denní	k2eAgFnSc4d1	denní
výrobu	výroba	k1gFnSc4	výroba
100 000	[number]	k4	100 000
párů	pár	k1gInPc2	pár
bot	bota	k1gFnPc2	bota
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tento	tento	k3xDgInSc1	tento
plán	plán	k1gInSc1	plán
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
roce	rok	k1gInSc6	rok
překonán	překonat	k5eAaPmNgInS	překonat
téměř	téměř	k6eAd1	téměř
o	o	k7c4	o
dvojnásobek	dvojnásobek	k1gInSc4	dvojnásobek
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
tvořit	tvořit	k5eAaImF	tvořit
roční	roční	k2eAgInPc4d1	roční
plány	plán	k1gInPc4	plán
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
v	v	k7c6	v
sobě	se	k3xPyFc3	se
zahrnovaly	zahrnovat	k5eAaImAgFnP	zahrnovat
plány	plán	k1gInPc4	plán
veškerých	veškerý	k3xTgNnPc2	veškerý
oddělení	oddělení	k1gNnPc2	oddělení
výroby	výroba	k1gFnSc2	výroba
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
byly	být	k5eAaImAgFnP	být
dále	daleko	k6eAd2	daleko
rozděleny	rozdělit	k5eAaPmNgFnP	rozdělit
na	na	k7c4	na
týdenní	týdenní	k2eAgInPc4d1	týdenní
plány	plán	k1gInPc4	plán
a	a	k8xC	a
ty	ten	k3xDgMnPc4	ten
pak	pak	k6eAd1	pak
na	na	k7c4	na
denní	denní	k2eAgInPc4d1	denní
programy	program	k1gInPc4	program
<g/>
.	.	kIx.	.
</s>
<s>
Tudíž	tudíž	k8xC	tudíž
na	na	k7c4	na
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
byl	být	k5eAaImAgInS	být
stanoven	stanovit	k5eAaPmNgInS	stanovit
přesný	přesný	k2eAgInSc1d1	přesný
obrat	obrat	k1gInSc1	obrat
<g/>
,	,	kIx,	,
kterého	který	k3yIgInSc2	který
muselo	muset	k5eAaImAgNnS	muset
být	být	k5eAaImF	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Samosprávné	samosprávný	k2eAgFnSc2d1	samosprávná
dílny	dílna	k1gFnSc2	dílna
===	===	k?	===
</s>
</p>
<p>
<s>
Další	další	k2eAgFnSc7d1	další
revoluční	revoluční	k2eAgFnSc7d1	revoluční
inovací	inovace	k1gFnSc7	inovace
bylo	být	k5eAaImAgNnS	být
vytvoření	vytvoření	k1gNnSc1	vytvoření
hospodářských	hospodářský	k2eAgFnPc2d1	hospodářská
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
měly	mít	k5eAaImAgFnP	mít
vlastní	vlastní	k2eAgInSc4d1	vlastní
účet	účet	k1gInSc4	účet
zisků	zisk	k1gInPc2	zisk
a	a	k8xC	a
ztrát	ztráta	k1gFnPc2	ztráta
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
samosprávné	samosprávný	k2eAgFnPc1d1	samosprávná
dílny	dílna	k1gFnPc1	dílna
tvořily	tvořit	k5eAaImAgFnP	tvořit
základní	základní	k2eAgFnSc4d1	základní
buňku	buňka	k1gFnSc4	buňka
celého	celý	k2eAgInSc2d1	celý
podniku	podnik	k1gInSc2	podnik
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
stál	stát	k5eAaImAgMnS	stát
mistr	mistr	k1gMnSc1	mistr
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
za	za	k7c4	za
vše	všechen	k3xTgNnSc4	všechen
nesl	nést	k5eAaImAgInS	nést
zodpovědnost	zodpovědnost	k1gFnSc4	zodpovědnost
<g/>
.	.	kIx.	.
</s>
<s>
Každé	každý	k3xTgNnSc1	každý
oddělení	oddělení	k1gNnSc1	oddělení
(	(	kIx(	(
<g/>
dílna	dílna	k1gFnSc1	dílna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
pravém	pravý	k2eAgInSc6d1	pravý
smyslu	smysl	k1gInSc6	smysl
slova	slovo	k1gNnSc2	slovo
<g/>
,	,	kIx,	,
kupovalo	kupovat	k5eAaImAgNnS	kupovat
ve	v	k7c6	v
výrobním	výrobní	k2eAgInSc6d1	výrobní
procesu	proces	k1gInSc6	proces
od	od	k7c2	od
předcházejícího	předcházející	k2eAgNnSc2d1	předcházející
oddělení	oddělení	k1gNnSc2	oddělení
zboží	zboží	k1gNnSc2	zboží
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
po	po	k7c6	po
zpracování	zpracování	k1gNnSc6	zpracování
zase	zase	k9	zase
prodávalo	prodávat	k5eAaImAgNnS	prodávat
následujícímu	následující	k2eAgNnSc3d1	následující
oddělení	oddělení	k1gNnSc3	oddělení
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
si	se	k3xPyFc3	se
muselo	muset	k5eAaImAgNnS	muset
pečlivě	pečlivě	k6eAd1	pečlivě
polotovary	polotovar	k1gInPc4	polotovar
překontrolovat	překontrolovat	k5eAaPmF	překontrolovat
a	a	k8xC	a
převzít	převzít	k5eAaPmF	převzít
–	–	k?	–
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
jednou	jednou	k6eAd1	jednou
převzalo	převzít	k5eAaPmAgNnS	převzít
<g/>
,	,	kIx,	,
neodvolatelně	odvolatelně	k6eNd1	odvolatelně
odpovídalo	odpovídat	k5eAaImAgNnS	odpovídat
za	za	k7c4	za
kvalitu	kvalita	k1gFnSc4	kvalita
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
Baťa	Baťa	k1gMnSc1	Baťa
ušetřil	ušetřit	k5eAaPmAgMnS	ušetřit
na	na	k7c6	na
kontrolorech	kontrolor	k1gMnPc6	kontrolor
<g/>
,	,	kIx,	,
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
vyráběl	vyrábět	k5eAaImAgMnS	vyrábět
ve	v	k7c6	v
špičkové	špičkový	k2eAgFnSc6d1	špičková
kvalitě	kvalita	k1gFnSc6	kvalita
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
systém	systém	k1gInSc4	systém
navazoval	navazovat	k5eAaImAgInS	navazovat
systém	systém	k1gInSc1	systém
správních	správní	k2eAgFnPc2d1	správní
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
fungoval	fungovat	k5eAaImAgInS	fungovat
na	na	k7c6	na
podobném	podobný	k2eAgInSc6d1	podobný
principu	princip	k1gInSc6	princip
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Systém	systém	k1gInSc1	systém
mezd	mzda	k1gFnPc2	mzda
===	===	k?	===
</s>
</p>
<p>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Baťa	Baťa	k1gMnSc1	Baťa
užíval	užívat	k5eAaImAgMnS	užívat
čtyři	čtyři	k4xCgInPc4	čtyři
základní	základní	k2eAgInPc4d1	základní
druhy	druh	k1gInPc4	druh
mezd	mzda	k1gFnPc2	mzda
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
<s>
pevná	pevný	k2eAgFnSc1d1	pevná
mzda	mzda	k1gFnSc1	mzda
–	–	k?	–
brali	brát	k5eAaImAgMnP	brát
technicko-hospodářští	technickoospodářský	k2eAgMnPc1d1	technicko-hospodářský
a	a	k8xC	a
administrativní	administrativní	k2eAgMnPc1d1	administrativní
pracovníci	pracovník	k1gMnPc1	pracovník
</s>
</p>
<p>
<s>
individuální	individuální	k2eAgFnSc1d1	individuální
úkolová	úkolový	k2eAgFnSc1d1	úkolová
mzda	mzda	k1gFnSc1	mzda
–	–	k?	–
dostávali	dostávat	k5eAaImAgMnP	dostávat
dělníci	dělník	k1gMnPc1	dělník
na	na	k7c6	na
některých	některý	k3yIgInPc6	některý
speciálních	speciální	k2eAgInPc6d1	speciální
postech	post	k1gInPc6	post
</s>
</p>
<p>
<s>
kolektivní	kolektivní	k2eAgFnSc1d1	kolektivní
úkolová	úkolový	k2eAgFnSc1d1	úkolová
mzda	mzda	k1gFnSc1	mzda
–	–	k?	–
pro	pro	k7c4	pro
dělníky	dělník	k1gMnPc4	dělník
v	v	k7c6	v
dílnách	dílna	k1gFnPc6	dílna
</s>
</p>
<p>
<s>
mzda	mzda	k1gFnSc1	mzda
účasti	účast	k1gFnSc2	účast
na	na	k7c6	na
zisku	zisk	k1gInSc6	zisk
–	–	k?	–
pobírali	pobírat	k5eAaImAgMnP	pobírat
někteří	některý	k3yIgMnPc1	některý
vedoucí	vedoucí	k1gMnPc1	vedoucí
pracovních	pracovní	k2eAgFnPc2d1	pracovní
úsekůVždy	úsekůVžd	k1gMnPc7	úsekůVžd
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
byla	být	k5eAaImAgFnS	být
každému	každý	k3xTgMnSc3	každý
přinesena	přinesen	k2eAgFnSc1d1	přinesena
knížka	knížka	k1gFnSc1	knížka
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yQgFnSc2	který
se	se	k3xPyFc4	se
muselo	muset	k5eAaImAgNnS	muset
napsat	napsat	k5eAaPmF	napsat
<g/>
,	,	kIx,	,
kolik	kolik	k9	kolik
by	by	kYmCp3nS	by
si	se	k3xPyFc3	se
chtěl	chtít	k5eAaImAgMnS	chtít
v	v	k7c6	v
příštím	příští	k2eAgInSc6d1	příští
roce	rok	k1gInSc6	rok
vydělat	vydělat	k5eAaPmF	vydělat
<g/>
.	.	kIx.	.
</s>
<s>
Baťa	Baťa	k1gMnSc1	Baťa
prohlašoval	prohlašovat	k5eAaImAgMnS	prohlašovat
<g/>
:	:	kIx,	:
Jste	být	k5eAaImIp2nP	být
moji	můj	k3xOp1gMnPc1	můj
spolupracovníci	spolupracovník	k1gMnPc1	spolupracovník
(	(	kIx(	(
<g/>
výraz	výraz	k1gInSc1	výraz
dělník	dělník	k1gMnSc1	dělník
nepoužíval	používat	k5eNaImAgMnS	používat
<g/>
)	)	kIx)	)
a	a	k8xC	a
mojí	můj	k3xOp1gFnSc7	můj
povinností	povinnost	k1gFnSc7	povinnost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
vytvořit	vytvořit	k5eAaPmF	vytvořit
Vám	vy	k3xPp2nPc3	vy
takové	takový	k3xDgFnPc4	takový
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
,	,	kIx,	,
abyste	aby	kYmCp2nP	aby
si	se	k3xPyFc3	se
tyto	tento	k3xDgInPc4	tento
peníze	peníz	k1gInPc4	peníz
mohli	moct	k5eAaImAgMnP	moct
vydělat	vydělat	k5eAaPmF	vydělat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Baťa	Baťa	k1gMnSc1	Baťa
byl	být	k5eAaImAgMnS	být
čtvrtým	čtvrtý	k4xOgMnSc7	čtvrtý
nejbohatším	bohatý	k2eAgMnSc7d3	nejbohatší
člověkem	člověk	k1gMnSc7	člověk
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
hospodářsky	hospodářsky	k6eAd1	hospodářsky
velmi	velmi	k6eAd1	velmi
pozvedl	pozvednout	k5eAaPmAgMnS	pozvednout
chudý	chudý	k2eAgInSc4d1	chudý
kraj	kraj	k1gInSc4	kraj
a	a	k8xC	a
dal	dát	k5eAaPmAgMnS	dát
lidem	lid	k1gInSc7	lid
práci	práce	k1gFnSc4	práce
a	a	k8xC	a
vyšší	vysoký	k2eAgFnSc4d2	vyšší
životní	životní	k2eAgFnSc4d1	životní
úroveň	úroveň	k1gFnSc4	úroveň
<g/>
,	,	kIx,	,
v	v	k7c6	v
očích	oko	k1gNnPc6	oko
komunistů	komunista	k1gMnPc2	komunista
byl	být	k5eAaImAgInS	být
jen	jen	k9	jen
kapitalistický	kapitalistický	k2eAgMnSc1d1	kapitalistický
vykořisťovatel	vykořisťovatel	k1gMnSc1	vykořisťovatel
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
30.	[number]	k4	30.
letech	let	k1gInPc6	let
dělnický	dělnický	k2eAgInSc4d1	dělnický
pěvecký	pěvecký	k2eAgInSc4d1	pěvecký
soubor	soubor	k1gInSc4	soubor
"	"	kIx"	"
<g/>
Modré	modrý	k2eAgFnSc2d1	modrá
blůzy	blůza	k1gFnSc2	blůza
<g/>
"	"	kIx"	"
zpíval	zpívat	k5eAaImAgMnS	zpívat
<g/>
:	:	kIx,	:
Copak	copak	k9	copak
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
tu	ten	k3xDgFnSc4	ten
si	se	k3xPyFc3	se
Baťa	Baťa	k1gMnSc1	Baťa
fouká	foukat	k5eAaImIp3nS	foukat
<g/>
,	,	kIx,	,
však	však	k9	však
až	až	k9	až
přijde	přijít	k5eAaPmIp3nS	přijít
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
se	se	k3xPyFc4	se
ztratit	ztratit	k5eAaPmF	ztratit
kouká	koukat	k5eAaImIp3nS	koukat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
V	v	k7c6	v
revolučních	revoluční	k2eAgInPc6d1	revoluční
zvratech	zvrat	k1gInPc6	zvrat
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
ztratil	ztratit	k5eAaPmAgMnS	ztratit
až	až	k9	až
Jan	Jan	k1gMnSc1	Jan
Antonín	Antonín	k1gMnSc1	Antonín
Baťa	Baťa	k1gMnSc1	Baťa
–	–	k?	–
ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
už	už	k6eAd1	už
domů	domů	k6eAd1	domů
nevrátil	vrátit	k5eNaPmAgMnS	vrátit
<g/>
,	,	kIx,	,
a	a	k8xC	a
i	i	k9	i
tak	tak	k6eAd1	tak
dostal	dostat	k5eAaPmAgMnS	dostat
u	u	k7c2	u
soudu	soud	k1gInSc2	soud
v	v	k7c6	v
nepřítomnosti	nepřítomnost	k1gFnSc6	nepřítomnost
15	[number]	k4	15
let	léto	k1gNnPc2	léto
těžkého	těžký	k2eAgInSc2d1	těžký
žaláře	žalář	k1gInSc2	žalář
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Typická	typický	k2eAgFnSc1d1	typická
je	být	k5eAaImIp3nS	být
také	také	k9	také
Baťova	Baťův	k2eAgFnSc1d1	Baťova
cena	cena	k1gFnSc1	cena
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
skoro	skoro	k6eAd1	skoro
vždy	vždy	k6eAd1	vždy
končila	končit	k5eAaImAgFnS	končit
devítkou	devítka	k1gFnSc7	devítka
<g/>
.	.	kIx.	.
</s>
<s>
Jednoduše	jednoduše	k6eAd1	jednoduše
999	[number]	k4	999
Kč	Kč	kA	Kč
vypadá	vypadat	k5eAaPmIp3nS	vypadat
opticky	opticky	k6eAd1	opticky
lákavěji	lákavě	k6eAd2	lákavě
než	než	k8xS	než
1000	[number]	k4	1000
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
přitom	přitom	k6eAd1	přitom
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
rozdíl	rozdíl	k1gInSc4	rozdíl
pouhé	pouhý	k2eAgFnSc2d1	pouhá
jedné	jeden	k4xCgFnSc2	jeden
koruny	koruna	k1gFnSc2	koruna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Expanze	expanze	k1gFnSc2	expanze
podniku	podnik	k1gInSc2	podnik
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1926	[number]	k4	1926
<g/>
–	–	k?	–
<g/>
1928	[number]	k4	1928
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
export	export	k1gInSc1	export
obuvi	obuv	k1gFnSc2	obuv
a	a	k8xC	a
firma	firma	k1gFnSc1	firma
Baťa	Baťa	k1gMnSc1	Baťa
ovládala	ovládat	k5eAaImAgFnS	ovládat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
polovinu	polovina	k1gFnSc4	polovina
československého	československý	k2eAgInSc2d1	československý
vývozu	vývoz	k1gInSc2	vývoz
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
firmě	firma	k1gFnSc6	firma
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
zavedení	zavedení	k1gNnSc3	zavedení
pásové	pásový	k2eAgFnSc2d1	pásová
výroby	výroba	k1gFnSc2	výroba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
používána	používat	k5eAaImNgFnS	používat
v	v	k7c6	v
závodech	závod	k1gInPc6	závod
Henryho	Henry	k1gMnSc2	Henry
Forda	ford	k1gMnSc2	ford
<g/>
.	.	kIx.	.
</s>
<s>
Produktivita	produktivita	k1gFnSc1	produktivita
práce	práce	k1gFnSc2	práce
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
o	o	k7c4	o
75	[number]	k4	75
%	%	kIx~	%
a	a	k8xC	a
počet	počet	k1gInSc1	počet
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
o	o	k7c4	o
35	[number]	k4	35
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Čistý	čistý	k2eAgInSc1d1	čistý
obrat	obrat	k1gInSc1	obrat
firmy	firma	k1gFnSc2	firma
činil	činit	k5eAaImAgInS	činit
1,9	[number]	k4	1,9
miliardy	miliarda	k4xCgFnSc2	miliarda
předválečných	předválečný	k2eAgFnPc2d1	předválečná
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1928	[number]	k4	1928
tvořila	tvořit	k5eAaImAgFnS	tvořit
továrna	továrna	k1gFnSc1	továrna
komplex	komplex	k1gInSc1	komplex
30	[number]	k4	30
budov	budova	k1gFnPc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Koncern	koncern	k1gInSc1	koncern
se	se	k3xPyFc4	se
rozrůstal	rozrůstat	k5eAaImAgInS	rozrůstat
a	a	k8xC	a
Baťa	Baťa	k1gMnSc1	Baťa
také	také	k9	také
podnikal	podnikat	k5eAaImAgMnS	podnikat
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
sférách	sféra	k1gFnPc6	sféra
hospodářství	hospodářství	k1gNnSc2	hospodářství
(	(	kIx(	(
<g/>
gumárenský	gumárenský	k2eAgInSc4d1	gumárenský
<g/>
,	,	kIx,	,
chemický	chemický	k2eAgInSc4d1	chemický
<g/>
,	,	kIx,	,
textilní	textilní	k2eAgInSc4d1	textilní
<g/>
,	,	kIx,	,
dřevařský	dřevařský	k2eAgInSc4d1	dřevařský
průmysl	průmysl	k1gInSc4	průmysl
a	a	k8xC	a
mnohé	mnohý	k2eAgFnPc4d1	mnohá
další	další	k2eAgFnPc4d1	další
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Baťa	Baťa	k1gMnSc1	Baťa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1931	[number]	k4	1931
vyráběl	vyrábět	k5eAaImAgMnS	vyrábět
ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
<g/>
,	,	kIx,	,
Otrokovicích	Otrokovice	k1gFnPc6	Otrokovice
<g/>
,	,	kIx,	,
Třebíči	Třebíč	k1gFnSc6	Třebíč
<g/>
,	,	kIx,	,
Bošanech	Bošan	k1gInPc6	Bošan
<g/>
,	,	kIx,	,
Nových	Nových	k2eAgInPc6d1	Nových
Zámcích	zámek	k1gInPc6	zámek
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1931	[number]	k4	1931
se	se	k3xPyFc4	se
rodinný	rodinný	k2eAgInSc1d1	rodinný
podnik	podnik	k1gInSc1	podnik
změnil	změnit	k5eAaPmAgInS	změnit
na	na	k7c4	na
akciovou	akciový	k2eAgFnSc4d1	akciová
společnost	společnost	k1gFnSc4	společnost
se	s	k7c7	s
základním	základní	k2eAgInSc7d1	základní
kapitálem	kapitál	k1gInSc7	kapitál
135 000 000	[number]	k4	135 000 000
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
dlouho	dlouho	k6eAd1	dlouho
předtím	předtím	k6eAd1	předtím
vznikaly	vznikat	k5eAaImAgFnP	vznikat
dceřiné	dceřiný	k2eAgFnPc1d1	dceřiná
společnosti	společnost	k1gFnPc1	společnost
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
k	k	k7c3	k
tomu	ten	k3xDgMnSc3	ten
přibývaly	přibývat	k5eAaImAgFnP	přibývat
továrny	továrna	k1gFnPc1	továrna
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
,	,	kIx,	,
Nizozemsku	Nizozemsko	k1gNnSc6	Nizozemsko
<g/>
,	,	kIx,	,
Polsku	Polsko	k1gNnSc6	Polsko
a	a	k8xC	a
mnoha	mnoho	k4c6	mnoho
dalších	další	k2eAgFnPc6d1	další
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
výchovných	výchovný	k2eAgFnPc2d1	výchovná
i	i	k8xC	i
vzdělávacích	vzdělávací	k2eAgFnPc2d1	vzdělávací
organizací	organizace	k1gFnPc2	organizace
(	(	kIx(	(
<g/>
Baťova	Baťův	k2eAgFnSc1d1	Baťova
škola	škola	k1gFnSc1	škola
práce	práce	k1gFnSc2	práce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
vlastní	vlastní	k2eAgNnSc1d1	vlastní
filmové	filmový	k2eAgNnSc1d1	filmové
studio	studio	k1gNnSc1	studio
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
zabývalo	zabývat	k5eAaImAgNnS	zabývat
natáčením	natáčení	k1gNnSc7	natáčení
reklam	reklama	k1gFnPc2	reklama
na	na	k7c4	na
obuvnické	obuvnický	k2eAgInPc4d1	obuvnický
výrobky	výrobek	k1gInPc4	výrobek
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
ze	z	k7c2	z
studia	studio	k1gNnSc2	studio
staly	stát	k5eAaPmAgInP	stát
známé	známý	k2eAgInPc1d1	známý
Filmové	filmový	k2eAgInPc1d1	filmový
ateliéry	ateliér	k1gInPc1	ateliér
Kudlov	Kudlovo	k1gNnPc2	Kudlovo
a	a	k8xC	a
Filmové	filmový	k2eAgInPc1d1	filmový
ateliéry	ateliér	k1gInPc1	ateliér
Zlín	Zlín	k1gInSc1	Zlín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Stav	stav	k1gInSc1	stav
podniku	podnik	k1gInSc2	podnik
Tomáše	Tomáš	k1gMnSc2	Tomáš
Bati	Baťa	k1gMnSc2	Baťa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
==	==	k?	==
</s>
</p>
<p>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Baťa	Baťa	k1gMnSc1	Baťa
zaměstnával	zaměstnávat	k5eAaImAgMnS	zaměstnávat
31 235	[number]	k4	31 235
pracovníků	pracovník	k1gMnPc2	pracovník
(	(	kIx(	(
<g/>
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
23 906	[number]	k4	23 906
ve	v	k7c6	v
výrobě	výroba	k1gFnSc6	výroba
a	a	k8xC	a
7 329	[number]	k4	7 329
v	v	k7c6	v
prodejnách	prodejna	k1gFnPc6	prodejna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
továrny	továrna	k1gFnPc1	továrna
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
(	(	kIx(	(
<g/>
Zlín	Zlín	k1gInSc1	Zlín
<g/>
,	,	kIx,	,
Otrokovice	Otrokovice	k1gFnPc1	Otrokovice
<g/>
,	,	kIx,	,
Třebíč	Třebíč	k1gFnSc1	Třebíč
<g/>
,	,	kIx,	,
Krasice	Krasice	k1gInPc1	Krasice
<g/>
,	,	kIx,	,
Bošany	Bošan	k1gInPc1	Bošan
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgInPc1d1	Nové
Zámky	zámek	k1gInPc1	zámek
<g/>
)	)	kIx)	)
i	i	k9	i
v	v	k7c6	v
cizině	cizina	k1gFnSc6	cizina
(	(	kIx(	(
<g/>
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
Jugoslávie	Jugoslávie	k1gFnSc1	Jugoslávie
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
Britská	britský	k2eAgFnSc1d1	britská
Indie	Indie	k1gFnSc1	Indie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Baťova	Baťův	k2eAgFnSc1d1	Baťova
obuv	obuv	k1gFnSc1	obuv
se	se	k3xPyFc4	se
prodávala	prodávat	k5eAaImAgFnS	prodávat
v	v	k7c6	v
2 500	[number]	k4	2 500
prodejnách	prodejna	k1gFnPc6	prodejna
(	(	kIx(	(
<g/>
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
1 840	[number]	k4	1 840
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
a	a	k8xC	a
660	[number]	k4	660
v	v	k7c6	v
cizině	cizina	k1gFnSc6	cizina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
zahraniční	zahraniční	k2eAgFnPc1d1	zahraniční
prodejny	prodejna	k1gFnPc1	prodejna
<g/>
,	,	kIx,	,
továrny	továrna	k1gFnPc1	továrna
a	a	k8xC	a
sesterské	sesterský	k2eAgFnPc1d1	sesterská
společnosti	společnost	k1gFnPc1	společnost
působily	působit	k5eAaImAgFnP	působit
v	v	k7c6	v
54	[number]	k4	54
zemích	zem	k1gFnPc6	zem
světa	svět	k1gInSc2	svět
na	na	k7c6	na
čtyřech	čtyři	k4xCgInPc6	čtyři
kontinentech	kontinent	k1gInPc6	kontinent
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
<s>
Evropa	Evropa	k1gFnSc1	Evropa
–	–	k?	–
23	[number]	k4	23
zemí	zem	k1gFnPc2	zem
<g/>
:	:	kIx,	:
Jugoslávie	Jugoslávie	k1gFnSc1	Jugoslávie
<g/>
,	,	kIx,	,
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
<g/>
,	,	kIx,	,
Dánsko	Dánsko	k1gNnSc1	Dánsko
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
,	,	kIx,	,
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
<g/>
,	,	kIx,	,
Švédsko	Švédsko	k1gNnSc1	Švédsko
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
,	,	kIx,	,
Belgie	Belgie	k1gFnSc1	Belgie
<g/>
,	,	kIx,	,
Finsko	Finsko	k1gNnSc1	Finsko
<g/>
,	,	kIx,	,
Lucembursko	Lucembursko	k1gNnSc1	Lucembursko
<g/>
,	,	kIx,	,
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
,	,	kIx,	,
Litva	Litva	k1gFnSc1	Litva
<g/>
,	,	kIx,	,
Lotyšsko	Lotyšsko	k1gNnSc1	Lotyšsko
<g/>
,	,	kIx,	,
Estonsko	Estonsko	k1gNnSc1	Estonsko
<g/>
,	,	kIx,	,
Malta	Malta	k1gFnSc1	Malta
<g/>
,	,	kIx,	,
Kypr	Kypr	k1gInSc1	Kypr
<g/>
,	,	kIx,	,
Gibraltar	Gibraltar	k1gInSc1	Gibraltar
<g/>
,	,	kIx,	,
Kanárské	kanárský	k2eAgInPc1d1	kanárský
ostrovy	ostrov	k1gInPc1	ostrov
</s>
</p>
<p>
<s>
Asie	Asie	k1gFnSc1	Asie
–	–	k?	–
10	[number]	k4	10
zemí	zem	k1gFnPc2	zem
<g/>
:	:	kIx,	:
Britská	britský	k2eAgFnSc1d1	britská
Indie	Indie	k1gFnSc1	Indie
<g/>
,	,	kIx,	,
Holandská	holandský	k2eAgFnSc1d1	holandská
Indie	Indie	k1gFnSc1	Indie
<g/>
,	,	kIx,	,
Malajsko	Malajsko	k1gNnSc1	Malajsko
a	a	k8xC	a
Singapur	Singapur	k1gInSc1	Singapur
<g/>
,	,	kIx,	,
Palestina	Palestina	k1gFnSc1	Palestina
<g/>
,	,	kIx,	,
Sýrie	Sýrie	k1gFnSc1	Sýrie
<g/>
,	,	kIx,	,
Irák	Irák	k1gInSc1	Irák
<g/>
,	,	kIx,	,
Aden	Aden	k1gInSc1	Aden
<g/>
,	,	kIx,	,
Siam	Siam	k1gInSc1	Siam
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
<g/>
,	,	kIx,	,
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
Indočína	Indočína	k1gFnSc1	Indočína
</s>
</p>
<p>
<s>
Afrika	Afrika	k1gFnSc1	Afrika
–	–	k?	–
17	[number]	k4	17
zemí	zem	k1gFnPc2	zem
<g/>
:	:	kIx,	:
Egypt	Egypt	k1gInSc1	Egypt
<g/>
,	,	kIx,	,
Súdán	Súdán	k1gInSc1	Súdán
<g/>
,	,	kIx,	,
Belgické	belgický	k2eAgNnSc1d1	Belgické
Kongo	Kongo	k1gNnSc1	Kongo
<g/>
,	,	kIx,	,
Jihoafrická	jihoafrický	k2eAgFnSc1d1	Jihoafrická
Unie	unie	k1gFnSc1	unie
<g/>
,	,	kIx,	,
Francouzské	francouzský	k2eAgNnSc1d1	francouzské
Somálsko	Somálsko	k1gNnSc1	Somálsko
<g/>
,	,	kIx,	,
Alžírsko	Alžírsko	k1gNnSc1	Alžírsko
<g/>
,	,	kIx,	,
Tunisko	Tunisko	k1gNnSc1	Tunisko
<g/>
,	,	kIx,	,
Maroko	Maroko	k1gNnSc1	Maroko
<g/>
,	,	kIx,	,
Senegal	Senegal	k1gInSc1	Senegal
<g/>
,	,	kIx,	,
Zlatonosné	zlatonosný	k2eAgNnSc1d1	zlatonosné
pobřeží	pobřeží	k1gNnSc1	pobřeží
<g/>
,	,	kIx,	,
Nigérie	Nigérie	k1gFnSc1	Nigérie
<g/>
,	,	kIx,	,
Kamerun	Kamerun	k1gInSc1	Kamerun
<g/>
,	,	kIx,	,
Keňa	Keňa	k1gFnSc1	Keňa
<g/>
,	,	kIx,	,
Tanganyika	Tanganyika	k1gFnSc1	Tanganyika
<g/>
,	,	kIx,	,
Uganda	Uganda	k1gFnSc1	Uganda
<g/>
,	,	kIx,	,
Habeš	Habeš	k1gFnSc1	Habeš
<g/>
,	,	kIx,	,
Libye	Libye	k1gFnSc1	Libye
</s>
</p>
<p>
<s>
Amerika	Amerika	k1gFnSc1	Amerika
–	–	k?	–
4	[number]	k4	4
země	země	k1gFnSc1	země
<g/>
:	:	kIx,	:
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
,	,	kIx,	,
Trinidad	Trinidad	k1gInSc1	Trinidad
<g/>
,	,	kIx,	,
Brazílie	Brazílie	k1gFnSc1	Brazílie
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Smrt	smrt	k1gFnSc1	smrt
<g/>
,	,	kIx,	,
let	léto	k1gNnPc2	léto
do	do	k7c2	do
Möhlinu	Möhlin	k1gInSc2	Möhlin
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
12.	[number]	k4	12.
července	červenec	k1gInSc2	červenec
1932	[number]	k4	1932
Tomáš	Tomáš	k1gMnSc1	Tomáš
Baťa	Baťa	k1gMnSc1	Baťa
zahynul	zahynout	k5eAaPmAgMnS	zahynout
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
pilotem	pilot	k1gMnSc7	pilot
Jindřichem	Jindřich	k1gMnSc7	Jindřich
Broučkem	Brouček	k1gMnSc7	Brouček
při	při	k7c6	při
letecké	letecký	k2eAgFnSc6d1	letecká
nehodě	nehoda	k1gFnSc6	nehoda
<g/>
,	,	kIx,	,
když	když	k8xS	když
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
osobním	osobní	k2eAgNnSc6d1	osobní
letadle	letadlo	k1gNnSc6	letadlo
Junkers	Junkersa	k1gFnPc2	Junkersa
F	F	kA	F
13	[number]	k4	13
letěl	letět	k5eAaImAgMnS	letět
do	do	k7c2	do
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
otevření	otevření	k1gNnSc4	otevření
nové	nový	k2eAgFnSc2d1	nová
pobočky	pobočka	k1gFnSc2	pobočka
v	v	k7c6	v
městečku	městečko	k1gNnSc6	městečko
Möhlin	Möhlina	k1gFnPc2	Möhlina
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
Rýna	Rýn	k1gInSc2	Rýn
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
přezdíván	přezdívat	k5eAaImNgInS	přezdívat
"	"	kIx"	"
<g/>
Der	drát	k5eAaImRp2nS	drát
tschechische	tschechische	k1gNnSc4	tschechische
Schuhkönig	Schuhkönig	k1gInSc1	Schuhkönig
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vrcholila	vrcholit	k5eAaImAgFnS	vrcholit
velká	velký	k2eAgFnSc1d1	velká
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
krize	krize	k1gFnSc1	krize
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
firma	firma	k1gFnSc1	firma
Baťa	Baťa	k1gMnSc1	Baťa
pobočky	pobočka	k1gFnPc4	pobočka
již	již	k9	již
ve	v	k7c6	v
více	hodně	k6eAd2	hodně
než	než	k8xS	než
60	[number]	k4	60
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
poboček	pobočka	k1gFnPc2	pobočka
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
kupř.	kupř.	kA	kupř.
i	i	k8xC	i
v	v	k7c6	v
Möhlinu	Möhlin	k1gInSc6	Möhlin
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
iniciován	iniciován	k2eAgInSc1d1	iniciován
především	především	k6eAd1	především
zvyšováním	zvyšování	k1gNnSc7	zvyšování
dovozních	dovozní	k2eAgFnPc2d1	dovozní
cel	cela	k1gFnPc2	cela
a	a	k8xC	a
ochranou	ochrana	k1gFnSc7	ochrana
místních	místní	k2eAgMnPc2d1	místní
trhů	trh	k1gInPc2	trh
vládní	vládní	k2eAgFnSc7d1	vládní
politikou	politika	k1gFnSc7	politika
<g/>
.	.	kIx.	.
</s>
<s>
Kupř.	kupř.	kA	kupř.
koncern	koncern	k1gInSc1	koncern
Baťa	Baťa	k1gMnSc1	Baťa
v	v	k7c6	v
Möhlinu	Möhlin	k1gInSc6	Möhlin
získal	získat	k5eAaPmAgMnS	získat
pozemky	pozemek	k1gInPc4	pozemek
o	o	k7c6	o
výměře	výměra	k1gFnSc6	výměra
24	[number]	k4	24
hektarů	hektar	k1gInPc2	hektar
za	za	k7c4	za
velkorysou	velkorysý	k2eAgFnSc4d1	velkorysá
nabídku	nabídka	k1gFnSc4	nabídka
1	[number]	k4	1
franku	frank	k1gInSc2	frank
za	za	k7c4	za
metr	metr	k1gInSc4	metr
čtvereční	čtvereční	k2eAgInSc4d1	čtvereční
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c4	o
renomé	renomé	k1gNnSc4	renomé
českého	český	k2eAgMnSc2d1	český
průmyslníka	průmyslník	k1gMnSc2	průmyslník
a	a	k8xC	a
také	také	k9	také
o	o	k7c6	o
faktu	fakt	k1gInSc6	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
2800	[number]	k4	2800
obyvatel	obyvatel	k1gMnPc2	obyvatel
zdejší	zdejší	k2eAgFnSc2d1	zdejší
obce	obec	k1gFnSc2	obec
připadalo	připadat	k5eAaPmAgNnS	připadat
100	[number]	k4	100
nezaměstnaných	nezaměstnaný	k1gMnPc2	nezaměstnaný
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
průmyslový	průmyslový	k2eAgInSc1d1	průmyslový
areál	areál	k1gInSc1	areál
i	i	k9	i
se	s	k7c7	s
službami	služba	k1gFnPc7	služba
a	a	k8xC	a
kolonií	kolonie	k1gFnSc7	kolonie
domků	domek	k1gInPc2	domek
byl	být	k5eAaImAgInS	být
dokončen	dokončit	k5eAaPmNgInS	dokončit
v	v	k7c6	v
50.	[number]	k4	50.
letech	léto	k1gNnPc6	léto
a	a	k8xC	a
s	s	k7c7	s
výrobou	výroba	k1gFnSc7	výroba
bot	bota	k1gFnPc2	bota
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
skončilo	skončit	k5eAaPmAgNnS	skončit
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990.	[number]	k4	1990.
</s>
</p>
<p>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Baťa	Baťa	k1gMnSc1	Baťa
do	do	k7c2	do
Möhlinu	Möhlin	k1gInSc2	Möhlin
startoval	startovat	k5eAaBmAgInS	startovat
z	z	k7c2	z
firemního	firemní	k2eAgNnSc2d1	firemní
letiště	letiště	k1gNnSc2	letiště
v	v	k7c6	v
Otrokovicích	Otrokovice	k1gFnPc6	Otrokovice
<g/>
.	.	kIx.	.
</s>
<s>
Let	let	k1gInSc1	let
byl	být	k5eAaImAgInS	být
kvůli	kvůli	k7c3	kvůli
mlze	mlha	k1gFnSc3	mlha
o	o	k7c4	o
1	[number]	k4	1
hodinu	hodina	k1gFnSc4	hodina
odložen	odložit	k5eAaPmNgInS	odložit
<g/>
.	.	kIx.	.
</s>
<s>
Baťa	Baťa	k1gMnSc1	Baťa
trval	trvat	k5eAaImAgMnS	trvat
na	na	k7c6	na
odletu	odlet	k1gInSc6	odlet
<g/>
,	,	kIx,	,
a	a	k8xC	a
při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gInSc1	jeho
přílet	přílet	k1gInSc1	přílet
na	na	k7c4	na
10.	[number]	k4	10.
hodinu	hodina	k1gFnSc4	hodina
oznámen	oznámit	k5eAaPmNgMnS	oznámit
do	do	k7c2	do
Curychu	Curych	k1gInSc2	Curych
<g/>
.	.	kIx.	.
8	[number]	k4	8
minut	minuta	k1gFnPc2	minuta
před	před	k7c7	před
6.	[number]	k4	6.
hodinou	hodina	k1gFnSc7	hodina
ráno	ráno	k1gNnSc1	ráno
letadlo	letadlo	k1gNnSc4	letadlo
odstartovalo	odstartovat	k5eAaPmAgNnS	odstartovat
<g/>
.	.	kIx.	.
</s>
<s>
Letadlo	letadlo	k1gNnSc1	letadlo
spadlo	spadnout	k5eAaPmAgNnS	spadnout
z	z	k7c2	z
výšky	výška	k1gFnSc2	výška
kolem	kolem	k7c2	kolem
20	[number]	k4	20
až	až	k9	až
30	[number]	k4	30
m	m	kA	m
poblíž	poblíž	k7c2	poblíž
továrny	továrna	k1gFnSc2	továrna
na	na	k7c6	na
Bahňáku	bahňák	k1gInSc6	bahňák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
verze	verze	k1gFnSc1	verze
<g/>
:	:	kIx,	:
pilot	pilot	k1gMnSc1	pilot
pozoroval	pozorovat	k5eAaImAgMnS	pozorovat
elektrické	elektrický	k2eAgNnSc4d1	elektrické
vedení	vedení	k1gNnSc4	vedení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
probíhalo	probíhat	k5eAaImAgNnS	probíhat
nad	nad	k7c7	nad
areálem	areál	k1gInSc7	areál
Bahňák	bahňák	k1gMnSc1	bahňák
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
snažil	snažit	k5eAaImAgMnS	snažit
vyhnout	vyhnout	k5eAaPmF	vyhnout
<g/>
,	,	kIx,	,
zachytil	zachytit	k5eAaPmAgMnS	zachytit
o	o	k7c4	o
roh	roh	k1gInSc4	roh
buď	buď	k8xC	buď
papírny	papírna	k1gFnPc1	papírna
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
pily	pila	k1gFnPc1	pila
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
verze	verze	k1gFnSc1	verze
<g/>
:	:	kIx,	:
letadlo	letadlo	k1gNnSc1	letadlo
bloudilo	bloudit	k5eAaImAgNnS	bloudit
v	v	k7c6	v
mlze	mlha	k1gFnSc6	mlha
několik	několik	k4yIc4	několik
minut	minuta	k1gFnPc2	minuta
nad	nad	k7c7	nad
Napajedly	Napajedla	k1gNnPc7	Napajedla
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pilot	pilot	k1gMnSc1	pilot
se	se	k3xPyFc4	se
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
zorientovat	zorientovat	k5eAaPmF	zorientovat
<g/>
,	,	kIx,	,
snad	snad	k9	snad
se	se	k3xPyFc4	se
i	i	k9	i
pokusil	pokusit	k5eAaPmAgMnS	pokusit
nouzově	nouzově	k6eAd1	nouzově
přistát	přistát	k5eAaPmF	přistát
<g/>
.	.	kIx.	.
<g/>
Továrník	továrník	k1gMnSc1	továrník
Baťa	Baťa	k1gMnSc1	Baťa
tragicky	tragicky	k6eAd1	tragicky
zahynul	zahynout	k5eAaPmAgMnS	zahynout
cca	cca	kA	cca
15	[number]	k4	15
metrů	metr	k1gInPc2	metr
od	od	k7c2	od
ohrady	ohrada	k1gFnSc2	ohrada
továrny	továrna	k1gFnSc2	továrna
na	na	k7c6	na
Bahňáku	bahňák	k1gInSc6	bahňák
<g/>
.	.	kIx.	.
</s>
<s>
Pilot	pilot	k1gMnSc1	pilot
Brouček	Brouček	k1gMnSc1	Brouček
byl	být	k5eAaImAgMnS	být
nalezen	nalezen	k2eAgMnSc1d1	nalezen
mrtev	mrtev	k2eAgMnSc1d1	mrtev
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
ještě	ještě	k9	ještě
svíral	svírat	k5eAaImAgInS	svírat
knipl	knipl	k1gInSc1	knipl
letadla	letadlo	k1gNnSc2	letadlo
<g/>
,	,	kIx,	,
jakoby	jakoby	k8xS	jakoby
chtěl	chtít	k5eAaImAgMnS	chtít
do	do	k7c2	do
poslední	poslední	k2eAgFnSc2d1	poslední
chvíle	chvíle	k1gFnSc2	chvíle
zabránit	zabránit	k5eAaPmF	zabránit
tragédii	tragédie	k1gFnSc4	tragédie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Baťa	Baťa	k1gMnSc1	Baťa
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
osudnou	osudný	k2eAgFnSc7d1	osudná
havárií	havárie	k1gFnSc7	havárie
svého	svůj	k3xOyFgNnSc2	svůj
letadla	letadlo	k1gNnSc2	letadlo
unikl	uniknout	k5eAaPmAgMnS	uniknout
několikrát	několikrát	k6eAd1	několikrát
úspěšně	úspěšně	k6eAd1	úspěšně
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1911	[number]	k4	1911
(	(	kIx(	(
<g/>
na	na	k7c4	na
dušičky	dušička	k1gFnPc4	dušička
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
měl	mít	k5eAaImAgMnS	mít
autonehodu	autonehoda	k1gFnSc4	autonehoda
(	(	kIx(	(
<g/>
srazil	srazit	k5eAaPmAgMnS	srazit
se	se	k3xPyFc4	se
s	s	k7c7	s
povozem	povoz	k1gInSc7	povoz
<g/>
)	)	kIx)	)
u	u	k7c2	u
Malenovic	Malenovice	k1gFnPc2	Malenovice
<g/>
,	,	kIx,	,
a	a	k8xC	a
ze	z	k7c2	z
které	který	k3yQgFnSc2	který
pochází	pocházet	k5eAaImIp3nS	pocházet
jizva	jizva	k1gFnSc1	jizva
na	na	k7c6	na
pravé	pravý	k2eAgFnSc6d1	pravá
straně	strana	k1gFnSc6	strana
čela	čelo	k1gNnSc2	čelo
<g/>
.	.	kIx.	.
</s>
<s>
Operován	operován	k2eAgMnSc1d1	operován
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
zánět	zánět	k1gInSc1	zánět
žil	žíla	k1gFnPc2	žíla
dolních	dolní	k2eAgFnPc2d1	dolní
končetin	končetina	k1gFnPc2	končetina
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jej	on	k3xPp3gMnSc4	on
málem	málem	k6eAd1	málem
"	"	kIx"	"
<g/>
usmrtily	usmrtit	k5eAaPmAgFnP	usmrtit
<g/>
"	"	kIx"	"
křečové	křečový	k2eAgFnPc1d1	křečová
žíly	žíla	k1gFnPc1	žíla
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
si	se	k3xPyFc3	se
způsobil	způsobit	k5eAaPmAgInS	způsobit
dlouhým	dlouhý	k2eAgNnSc7d1	dlouhé
chozením	chození	k1gNnSc7	chození
s	s	k7c7	s
plnou	plný	k2eAgFnSc7d1	plná
nůší	nůše	k1gFnSc7	nůše
kůží	kůže	k1gFnPc2	kůže
na	na	k7c4	na
zádech	zádech	k1gInSc4	zádech
v	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
podnikání	podnikání	k1gNnSc2	podnikání
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Ohlasy	ohlas	k1gInPc1	ohlas
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
===	===	k?	===
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Čapek	Čapek	k1gMnSc1	Čapek
o	o	k7c6	o
Baťovi	Baťa	k1gMnSc6	Baťa
napsal	napsat	k5eAaBmAgMnS	napsat
v	v	k7c6	v
Lidových	lidový	k2eAgFnPc6d1	lidová
novinách	novina	k1gFnPc6	novina
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
<s>
Na	na	k7c6	na
pohřbu	pohřeb	k1gInSc6	pohřeb
Tomáše	Tomáš	k1gMnSc2	Tomáš
Bati	Baťa	k1gMnSc2	Baťa
pronesl	pronést	k5eAaPmAgMnS	pronést
smuteční	smuteční	k2eAgFnSc4d1	smuteční
řeč	řeč	k1gFnSc4	řeč
jeho	jeho	k3xOp3gMnPc4	jeho
nevlastní	vlastnit	k5eNaImIp3nS	vlastnit
bratr	bratr	k1gMnSc1	bratr
a	a	k8xC	a
nástupce	nástupce	k1gMnSc1	nástupce
ve	v	k7c6	v
firmě	firma	k1gFnSc6	firma
Jan	Jan	k1gMnSc1	Jan
Antonín	Antonín	k1gMnSc1	Antonín
Baťa	Baťa	k1gMnSc1	Baťa
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
ukázka	ukázka	k1gFnSc1	ukázka
jeho	jeho	k3xOp3gFnSc2	jeho
smuteční	smuteční	k2eAgFnSc2d1	smuteční
řeči	řeč	k1gFnSc2	řeč
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
podnicích	podnik	k1gInPc6	podnik
Tomáše	Tomáš	k1gMnSc2	Tomáš
Bati	Baťa	k1gMnSc2	Baťa
tehdy	tehdy	k6eAd1	tehdy
mělo	mít	k5eAaImAgNnS	mít
obživu	obživa	k1gFnSc4	obživa
třicet	třicet	k4xCc1	třicet
tisíc	tisíc	k4xCgInSc1	tisíc
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
,	,	kIx,	,
firemní	firemní	k2eAgFnPc1d1	firemní
továrny	továrna	k1gFnPc1	továrna
a	a	k8xC	a
prodejny	prodejna	k1gFnPc1	prodejna
se	se	k3xPyFc4	se
rozkládaly	rozkládat	k5eAaImAgFnP	rozkládat
na	na	k7c6	na
čtyřech	čtyři	k4xCgInPc6	čtyři
kontinentech	kontinent	k1gInPc6	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
naproti	naproti	k7c3	naproti
továrnímu	tovární	k2eAgInSc3d1	tovární
kolosu	kolos	k1gInSc3	kolos
stálo	stát	k5eAaImAgNnS	stát
Náměstí	náměstí	k1gNnSc1	náměstí
Práce	práce	k1gFnSc2	práce
s	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
obchodními	obchodní	k2eAgInPc7d1	obchodní
domy	dům	k1gInPc7	dům
<g/>
,	,	kIx,	,
sociálním	sociální	k2eAgInSc7d1	sociální
ústavem	ústav	k1gInSc7	ústav
<g/>
,	,	kIx,	,
hotelem	hotel	k1gInSc7	hotel
a	a	k8xC	a
kinem	kino	k1gNnSc7	kino
(	(	kIx(	(
<g/>
největší	veliký	k2eAgInSc4d3	veliký
biograf	biograf	k1gInSc4	biograf
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
skoro	skoro	k6eAd1	skoro
hotová	hotová	k1gFnSc1	hotová
byla	být	k5eAaImAgFnS	být
školní	školní	k2eAgFnSc1d1	školní
čtvrť	čtvrť	k1gFnSc1	čtvrť
<g/>
,	,	kIx,	,
přibývaly	přibývat	k5eAaImAgInP	přibývat
internáty	internát	k1gInPc1	internát
a	a	k8xC	a
pavilony	pavilon	k1gInPc1	pavilon
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
<g/>
,	,	kIx,	,
město	město	k1gNnSc4	město
lemovaly	lemovat	k5eAaImAgFnP	lemovat
zahradní	zahradní	k2eAgFnPc1d1	zahradní
čtvrti	čtvrt	k1gFnPc1	čtvrt
s	s	k7c7	s
jeden	jeden	k4xCgInSc4	jeden
a	a	k8xC	a
půl	půl	k1xP	půl
tisícem	tisíc	k4xCgInSc7	tisíc
domků	domek	k1gInPc2	domek
pro	pro	k7c4	pro
zaměstnance	zaměstnanec	k1gMnPc4	zaměstnanec
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
vedení	vedení	k1gNnSc2	vedení
firmy	firma	k1gFnSc2	firma
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
Tomášův	Tomášův	k2eAgMnSc1d1	Tomášův
nevlastní	vlastní	k2eNgMnSc1d1	nevlastní
bratr	bratr	k1gMnSc1	bratr
Jan	Jan	k1gMnSc1	Jan
Antonín	Antonín	k1gMnSc1	Antonín
Baťa	Baťa	k1gMnSc1	Baťa
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Dominikem	Dominik	k1gMnSc7	Dominik
Čiperou	Čipera	k1gMnSc7	Čipera
a	a	k8xC	a
Hugo	Hugo	k1gMnSc1	Hugo
Vavrečkou	Vavrečka	k1gFnSc7	Vavrečka
(	(	kIx(	(
<g/>
akcie	akcie	k1gFnPc1	akcie
firmy	firma	k1gFnSc2	firma
byly	být	k5eAaImAgFnP	být
převedeny	převést	k5eAaPmNgFnP	převést
na	na	k7c4	na
Jana	Jan	k1gMnSc4	Jan
A.	A.	kA	A.
Baťu	Baťa	k1gMnSc4	Baťa
a	a	k8xC	a
Marii	Maria	k1gFnSc4	Maria
Baťovou	Baťová	k1gFnSc4	Baťová
<g/>
,	,	kIx,	,
vdovu	vdova	k1gFnSc4	vdova
po	po	k7c6	po
Tomášovi	Tomáš	k1gMnSc6	Tomáš
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rozvoj	rozvoj	k1gInSc1	rozvoj
podnikání	podnikání	k1gNnSc3	podnikání
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
budování	budování	k1gNnSc4	budování
Zlína	Zlín	k1gInSc2	Zlín
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
vyrostl	vyrůst	k5eAaPmAgInS	vyrůst
památník	památník	k1gInSc1	památník
Tomáše	Tomáš	k1gMnSc2	Tomáš
Bati	Baťa	k1gMnSc2	Baťa
–	–	k?	–
později	pozdě	k6eAd2	pozdě
přestavěný	přestavěný	k2eAgMnSc1d1	přestavěný
v	v	k7c4	v
Dům	dům	k1gInSc4	dům
umění	umění	k1gNnSc4	umění
<g/>
,	,	kIx,	,
Baťův	Baťův	k2eAgInSc4d1	Baťův
kanál	kanál	k1gInSc4	kanál
–	–	k?	–
rozšíření	rozšíření	k1gNnSc2	rozšíření
a	a	k8xC	a
prodloužení	prodloužení	k1gNnSc2	prodloužení
vodní	vodní	k2eAgFnSc2d1	vodní
cesty	cesta	k1gFnSc2	cesta
až	až	k9	až
do	do	k7c2	do
Otrokovic	Otrokovice	k1gFnPc2	Otrokovice
s	s	k7c7	s
částečným	částečný	k2eAgNnSc7d1	částečné
využitím	využití	k1gNnSc7	využití
i	i	k9	i
řeky	řeka	k1gFnSc2	řeka
Moravy	Morava	k1gFnSc2	Morava
(	(	kIx(	(
<g/>
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
na	na	k7c6	na
stavbě	stavba	k1gFnSc6	stavba
pracovalo	pracovat	k5eAaImAgNnS	pracovat
na	na	k7c4	na
800	[number]	k4	800
dělníků	dělník	k1gMnPc2	dělník
<g/>
,	,	kIx,	,
práce	práce	k1gFnPc1	práce
se	se	k3xPyFc4	se
prováděly	provádět	k5eAaImAgFnP	provádět
ručně	ručně	k6eAd1	ručně
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
použití	použití	k1gNnSc2	použití
mechanizace	mechanizace	k1gFnSc2	mechanizace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výstavba	výstavba	k1gFnSc1	výstavba
rodinných	rodinný	k2eAgInPc2d1	rodinný
domků	domek	k1gInPc2	domek
na	na	k7c6	na
Lesní	lesní	k2eAgFnSc6d1	lesní
čtvrti	čtvrt	k1gFnSc6	čtvrt
atd.	atd.	kA	atd.
Neodmyslitelnou	odmyslitelný	k2eNgFnSc7d1	neodmyslitelná
součástí	součást	k1gFnSc7	součást
Baťovy	Baťův	k2eAgFnSc2d1	Baťova
tradice	tradice	k1gFnSc2	tradice
patřilo	patřit	k5eAaImAgNnS	patřit
i	i	k9	i
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
jedinečné	jedinečný	k2eAgInPc1d1	jedinečný
<g/>
,	,	kIx,	,
logicky	logicky	k6eAd1	logicky
navazující	navazující	k2eAgNnSc4d1	navazující
provádění	provádění	k1gNnSc4	provádění
číslování	číslování	k1gNnSc2	číslování
postavených	postavený	k2eAgMnPc2d1	postavený
čtvrtdomků	čtvrtdomek	k1gMnPc2	čtvrtdomek
a	a	k8xC	a
půldomků	půldomek	k1gMnPc2	půldomek
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
ulicích	ulice	k1gFnPc6	ulice
postupně	postupně	k6eAd1	postupně
domky	domek	k1gInPc1	domek
dostavovaly	dostavovat	k5eAaImAgInP	dostavovat
<g/>
.	.	kIx.	.
</s>
<s>
Založil	založit	k5eAaPmAgInS	založit
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
jedinečný	jedinečný	k2eAgInSc1d1	jedinečný
a	a	k8xC	a
přehledný	přehledný	k2eAgInSc1d1	přehledný
systém	systém	k1gInSc1	systém
logistiky	logistika	k1gFnSc2	logistika
i	i	k8xC	i
orientace	orientace	k1gFnSc2	orientace
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
významná	významný	k2eAgFnSc1d1	významná
stavba	stavba	k1gFnSc1	stavba
Baťův	Baťův	k2eAgInSc1d1	Baťův
mrakodrap	mrakodrap	k1gInSc1	mrakodrap
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
nesla	nést	k5eAaImAgFnS	nést
přívlastek	přívlastek	k1gInSc4	přívlastek
"	"	kIx"	"
<g/>
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
betonová	betonový	k2eAgFnSc1d1	betonová
stavba	stavba	k1gFnSc1	stavba
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Mrakodrap	mrakodrap	k1gInSc1	mrakodrap
byl	být	k5eAaImAgInS	být
správním	správní	k2eAgMnSc7d1	správní
a	a	k8xC	a
administrativním	administrativní	k2eAgNnSc7d1	administrativní
centrem	centrum	k1gNnSc7	centrum
podniků	podnik	k1gInPc2	podnik
Baťa	Baťa	k1gMnSc1	Baťa
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
administrativním	administrativní	k2eAgMnSc7d1	administrativní
a	a	k8xC	a
řídícím	řídící	k2eAgInSc7d1	řídící
centrem	centr	k1gInSc7	centr
podniků	podnik	k1gInPc2	podnik
Svit	svit	k2eAgInSc1d1	svit
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
<g/>
,	,	kIx,	,
<g/>
[	[	kIx(	[
<g/>
kdy	kdy	k6eAd1	kdy
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
po	po	k7c6	po
rozsáhlé	rozsáhlý	k2eAgFnSc6d1	rozsáhlá
rekonstrukci	rekonstrukce	k1gFnSc6	rekonstrukce
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
sídlí	sídlet	k5eAaImIp3nS	sídlet
krajský	krajský	k2eAgInSc1d1	krajský
úřad	úřad	k1gInSc1	úřad
Zlínského	zlínský	k2eAgInSc2d1	zlínský
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
JANDÍK	JANDÍK	kA	JANDÍK
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
<g/>
.	.	kIx.	.
</s>
<s>
Železní	železný	k2eAgMnPc1d1	železný
tovaryši	tovaryš	k1gMnPc1	tovaryš
<g/>
:	:	kIx,	:
sociologická	sociologický	k2eAgFnSc1d1	sociologická
reportáž	reportáž	k1gFnSc1	reportáž
o	o	k7c6	o
zrození	zrození	k1gNnSc6	zrození
nového	nový	k2eAgInSc2d1	nový
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Volná	volný	k2eAgFnSc1d1	volná
myšlenka	myšlenka	k1gFnSc1	myšlenka
<g/>
,	,	kIx,	,
1938.	[number]	k4	1938.
465	[number]	k4	465
s	s	k7c7	s
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
POKLUDA	POKLUDA	kA	POKLUDA
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
Baťovi	Baťův	k2eAgMnPc1d1	Baťův
muži	muž	k1gMnPc1	muž
<g/>
.	.	kIx.	.
</s>
<s>
Zlín	Zlín	k1gInSc1	Zlín
<g/>
:	:	kIx,	:
Kovárna	kovárna	k1gFnSc1	kovárna
VIVA	VIVA	kA	VIVA
a.	a.	k?	a.
s.	s.	k?	s.
<g/>
,	,	kIx,	,
2012.	[number]	k4	2012.
167	[number]	k4	167
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
978-80-260-3389-9	[number]	k4	978-80-260-3389-9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Osobnosti	osobnost	k1gFnPc1	osobnost
-	-	kIx~	-
Česko	Česko	k1gNnSc1	Česko
:	:	kIx,	:
Ottův	Ottův	k2eAgInSc1d1	Ottův
slovník	slovník	k1gInSc1	slovník
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ottovo	Ottův	k2eAgNnSc1d1	Ottovo
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
2008.	[number]	k4	2008.
823	[number]	k4	823
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
978-80-7360-796-8	[number]	k4	978-80-7360-796-8
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
35.	[number]	k4	35.
</s>
</p>
<p>
<s>
TOMEŠ	Tomeš	k1gMnSc1	Tomeš
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
biografický	biografický	k2eAgInSc1d1	biografický
slovník	slovník	k1gInSc1	slovník
XX	XX	kA	XX
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
I.	I.	kA	I.
A	A	kA	A
<g/>
–	–	k?	–
<g/>
J.	J.	kA	J.
Praha	Praha	k1gFnSc1	Praha
;	;	kIx,	;
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
1999.	[number]	k4	1999.
634	[number]	k4	634
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
80-7185-245-7	[number]	k4	80-7185-245-7
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
56	[number]	k4	56
<g/>
–	–	k?	–
<g/>
57	[number]	k4	57
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VOŠAHLÍKOVÁ	Vošahlíková	k1gFnSc1	Vošahlíková
<g/>
,	,	kIx,	,
Pavla	Pavla	k1gFnSc1	Pavla
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Biografický	biografický	k2eAgInSc1d1	biografický
slovník	slovník	k1gInSc1	slovník
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
:	:	kIx,	:
3.	[number]	k4	3.
sešit	sešit	k1gInSc1	sešit
:	:	kIx,	:
Bas	bas	k1gInSc1	bas
<g/>
–	–	k?	–
<g/>
Bend	Bend	k1gInSc1	Bend
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2005.	[number]	k4	2005.
264	[number]	k4	264
<g/>
–	–	k?	–
<g/>
375	[number]	k4	375
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
80-7277-287-2	[number]	k4	80-7277-287-2
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
273	[number]	k4	273
<g/>
–	–	k?	–
<g/>
274	[number]	k4	274
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bohumil	Bohumil	k1gMnSc1	Bohumil
Léhar	Léhar	k1gMnSc1	Léhar
<g/>
:	:	kIx,	:
Dějiny	dějiny	k1gFnPc1	dějiny
Baťova	Baťův	k2eAgInSc2d1	Baťův
koncernu	koncern	k1gInSc2	koncern
<g/>
,	,	kIx,	,
Státní	státní	k2eAgInSc1d1	státní
nakl	nakl	k1gInSc1	nakl
<g/>
.	.	kIx.	.
politické	politický	k2eAgFnSc2d1	politická
literatury	literatura	k1gFnSc2	literatura
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1960	[number]	k4	1960
</s>
</p>
<p>
<s>
Miroslav	Miroslav	k1gMnSc1	Miroslav
Ivanov	Ivanov	k1gInSc1	Ivanov
<g/>
:	:	kIx,	:
Sága	sága	k1gFnSc1	sága
o	o	k7c6	o
životě	život	k1gInSc6	život
a	a	k8xC	a
smrti	smrt	k1gFnSc6	smrt
Jana	Jan	k1gMnSc2	Jan
Bati	Baťa	k1gMnSc2	Baťa
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc2	jeho
bratra	bratr	k1gMnSc2	bratr
Tomáše	Tomáš	k1gMnSc2	Tomáš
<g/>
,	,	kIx,	,
nakl	naknout	k5eAaPmAgMnS	naknout
<g/>
.	.	kIx.	.
</s>
<s>
Lípa	lípa	k1gFnSc1	lípa
<g/>
,	,	kIx,	,
Vizovice	Vizovice	k1gFnPc1	Vizovice
1998	[number]	k4	1998
</s>
</p>
<p>
<s>
Eva	Eva	k1gFnSc1	Eva
Dvořáková	Dvořáková	k1gFnSc1	Dvořáková
<g/>
:	:	kIx,	:
Batismus	Batismus	k1gInSc1	Batismus
a	a	k8xC	a
Baťovci	baťovec	k1gMnPc1	baťovec
<g/>
,	,	kIx,	,
Krajské	krajský	k2eAgFnSc2d1	krajská
nakl	nakl	k1gInSc4	nakl
<g/>
.	.	kIx.	.
</s>
<s>
Gottwaldov	Gottwaldov	k1gInSc1	Gottwaldov
<g/>
,	,	kIx,	,
Gottwaldov	Gottwaldov	k1gInSc1	Gottwaldov
1960	[number]	k4	1960
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Vaňhara	Vaňhar	k1gMnSc2	Vaňhar
<g/>
:	:	kIx,	:
Příběh	příběh	k1gInSc1	příběh
jednoho	jeden	k4xCgMnSc2	jeden
muže	muž	k1gMnSc2	muž
a	a	k8xC	a
jednoho	jeden	k4xCgNnSc2	jeden
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
vlastní	vlastní	k2eAgNnSc1d1	vlastní
vydání	vydání	k1gNnSc1	vydání
<g/>
,	,	kIx,	,
Zlín	Zlín	k1gInSc1	Zlín
</s>
</p>
<p>
<s>
Milan	Milan	k1gMnSc1	Milan
Zelený	Zelený	k1gMnSc1	Zelený
<g/>
:	:	kIx,	:
Cesty	cesta	k1gFnPc1	cesta
k	k	k7c3	k
úspěchu	úspěch	k1gInSc3	úspěch
–	–	k?	–
trvalé	trvalý	k2eAgFnSc2d1	trvalá
hodnoty	hodnota	k1gFnSc2	hodnota
soustavy	soustava	k1gFnSc2	soustava
Baťa	Baťa	k1gMnSc1	Baťa
<g/>
,	,	kIx,	,
nakl	nakl	k1gMnSc1	nakl
<g/>
.	.	kIx.	.
</s>
<s>
Čintámani	Čintáman	k1gMnPc1	Čintáman
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-239-8233-8	[number]	k4	80-239-8233-8
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
koncernu	koncern	k1gInSc2	koncern
Baťa	Baťa	k1gMnSc1	Baťa
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Tomáš	Tomáš	k1gMnSc1	Tomáš
Baťa	Baťa	k1gMnSc1	Baťa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Tomáš	Tomáš	k1gMnSc1	Tomáš
Baťa	Baťa	k1gMnSc1	Baťa
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Autor	autor	k1gMnSc1	autor
Tomáš	Tomáš	k1gMnSc1	Tomáš
Baťa	Baťa	k1gMnSc1	Baťa
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Tomáš	Tomáš	k1gMnSc1	Tomáš
Baťa	Baťa	k1gMnSc1	Baťa
</s>
</p>
<p>
<s>
Bibliografie	bibliografie	k1gFnPc1	bibliografie
k	k	k7c3	k
baťovským	baťovský	k2eAgNnPc3d1	baťovské
tématům	téma	k1gNnPc3	téma
<g/>
,	,	kIx,	,
životopis	životopis	k1gInSc4	životopis
Tomáše	Tomáš	k1gMnSc2	Tomáš
Bati	Baťa	k1gMnSc2	Baťa
a	a	k8xC	a
dobové	dobový	k2eAgFnSc2d1	dobová
fotografie	fotografia	k1gFnSc2	fotografia
</s>
</p>
<p>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Baťa	Baťa	k1gMnSc1	Baťa
v	v	k7c6	v
Kdo	kdo	k3yQnSc1	kdo
byl	být	k5eAaImAgMnS	být
kdo	kdo	k3yInSc1	kdo
v	v	k7c6	v
našich	náš	k3xOp1gFnPc6	náš
dějinách	dějiny	k1gFnPc6	dějiny
ve	v	k7c6	v
20.	[number]	k4	20.
století	století	k1gNnSc6	století
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Bata	Batum	k1gNnSc2	Batum
History	Histor	k1gInPc4	Histor
</s>
</p>
<p>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
Antonín	Antonín	k1gMnSc1	Antonín
Baťovi	Baťa	k1gMnSc3	Baťa
–	–	k?	–
úvahy	úvaha	k1gFnPc4	úvaha
<g/>
,	,	kIx,	,
vzpomínky	vzpomínka	k1gFnPc4	vzpomínka
<g/>
,	,	kIx,	,
plány	plán	k1gInPc4	plán
<g/>
,	,	kIx,	,
projekty	projekt	k1gInPc4	projekt
z	z	k7c2	z
dobových	dobový	k2eAgInPc2d1	dobový
materiálů	materiál	k1gInPc2	materiál
převzaté	převzatý	k2eAgFnSc2d1	převzatá
</s>
</p>
<p>
<s>
Nadace	nadace	k1gFnSc1	nadace
Tomáše	Tomáš	k1gMnSc2	Tomáš
Bati	Baťa	k1gMnSc2	Baťa
</s>
</p>
<p>
<s>
Bataville	Bataville	k6eAd1	Bataville
</s>
</p>
<p>
<s>
Vzpomínky	vzpomínka	k1gFnPc1	vzpomínka
na	na	k7c4	na
Tomáše	Tomáš	k1gMnSc4	Tomáš
Baťu	Baťa	k1gMnSc4	Baťa
</s>
</p>
<p>
<s>
Univerzita	univerzita	k1gFnSc1	univerzita
Tomáše	Tomáš	k1gMnSc2	Tomáš
Bati	Baťa	k1gMnSc2	Baťa
</s>
</p>
<p>
<s>
Baťovo	Baťův	k2eAgNnSc1d1	Baťovo
muzeum	muzeum	k1gNnSc1	muzeum
v	v	k7c6	v
Torontu	Toronto	k1gNnSc6	Toronto
</s>
</p>
<p>
<s>
Lesy	les	k1gInPc1	les
a	a	k8xC	a
statky	statek	k1gInPc1	statek
Tomáše	Tomáš	k1gMnSc2	Tomáš
Bati	Baťa	k1gMnSc2	Baťa
</s>
</p>
<p>
<s>
Projev	projev	k1gInSc1	projev
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1930	[number]	k4	1930
-	-	kIx~	-
Archiv	archiv	k1gInSc1	archiv
ČRo	ČRo	k1gFnSc2	ČRo
</s>
</p>
<p>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Baťa	Baťa	k1gMnSc1	Baťa
<g/>
:	:	kIx,	:
Úvahy	úvaha	k1gFnPc1	úvaha
a	a	k8xC	a
projevy	projev	k1gInPc1	projev
audiokniha	audioknih	k1gMnSc2	audioknih
zdarma	zdarma	k6eAd1	zdarma
<g/>
,	,	kIx,	,
Knihovna	knihovna	k1gFnSc1	knihovna
Univerzity	univerzita	k1gFnSc2	univerzita
Tomáše	Tomáš	k1gMnSc2	Tomáš
Bati	Baťa	k1gMnSc2	Baťa
ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
<g/>
,	,	kIx,	,
čte	číst	k5eAaImIp3nS	číst
Alfred	Alfred	k1gMnSc1	Alfred
Strejček	Strejček	k1gMnSc1	Strejček
</s>
</p>
