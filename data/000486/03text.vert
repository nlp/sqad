<s>
Newton	Newton	k1gMnSc1	Newton
[	[	kIx(	[
<g/>
ňútn	ňútn	k1gMnSc1	ňútn
<g/>
]	]	kIx)	]
je	být	k5eAaImIp3nS	být
jednotka	jednotka	k1gFnSc1	jednotka
síly	síla	k1gFnSc2	síla
v	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
SI	si	k1gNnSc2	si
se	s	k7c7	s
značkou	značka	k1gFnSc7	značka
N	N	kA	N
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
odvozenou	odvozený	k2eAgFnSc4d1	odvozená
jednotku	jednotka	k1gFnSc4	jednotka
<g/>
;	;	kIx,	;
rozměr	rozměr	k1gInSc4	rozměr
v	v	k7c6	v
základních	základní	k2eAgFnPc6d1	základní
jednotkách	jednotka	k1gFnPc6	jednotka
soustavy	soustava	k1gFnSc2	soustava
SI	si	k1gNnSc2	si
je	být	k5eAaImIp3nS	být
kg	kg	kA	kg
<g/>
·	·	k?	·
<g/>
m	m	kA	m
<g/>
·	·	k?	·
<g/>
s	s	k7c7	s
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Newton	newton	k1gInSc1	newton
je	být	k5eAaImIp3nS	být
definován	definovat	k5eAaBmNgInS	definovat
následovně	následovně	k6eAd1	následovně
<g/>
:	:	kIx,	:
Síla	síla	k1gFnSc1	síla
1	[number]	k4	1
newton	newton	k1gInSc1	newton
je	být	k5eAaImIp3nS	být
taková	takový	k3xDgFnSc1	takový
síla	síla	k1gFnSc1	síla
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
udělí	udělit	k5eAaPmIp3nS	udělit
volnému	volný	k2eAgInSc3d1	volný
hmotnému	hmotný	k2eAgInSc3d1	hmotný
bodu	bod	k1gInSc3	bod
o	o	k7c6	o
hmotnosti	hmotnost	k1gFnSc6	hmotnost
1	[number]	k4	1
kg	kg	kA	kg
zrychlení	zrychlení	k1gNnSc1	zrychlení
1	[number]	k4	1
m	m	kA	m
<g/>
·	·	k?	·
<g/>
s	s	k7c7	s
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Jednotka	jednotka	k1gFnSc1	jednotka
je	být	k5eAaImIp3nS	být
pojmenována	pojmenovat	k5eAaPmNgFnS	pojmenovat
po	po	k7c6	po
významném	významný	k2eAgMnSc6d1	významný
fyzikovi	fyzik	k1gMnSc6	fyzik
Isaacu	Isaac	k1gMnSc6	Isaac
Newtonovi	Newton	k1gMnSc6	Newton
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
je	být	k5eAaImIp3nS	být
newton	newton	k1gInSc1	newton
jednotkou	jednotka	k1gFnSc7	jednotka
síly	síla	k1gFnPc1	síla
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
jednotkou	jednotka	k1gFnSc7	jednotka
tíhy	tíha	k1gFnSc2	tíha
(	(	kIx(	(
<g/>
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
váha	váha	k1gFnSc1	váha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
síly	síla	k1gFnSc2	síla
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
je	být	k5eAaImIp3nS	být
jedno	jeden	k4xCgNnSc1	jeden
těleso	těleso	k1gNnSc1	těleso
přitahováno	přitahován	k2eAgNnSc1d1	přitahováno
ke	k	k7c3	k
druhému	druhý	k4xOgMnSc3	druhý
<g/>
,	,	kIx,	,
typicky	typicky	k6eAd1	typicky
Zemi	zem	k1gFnSc3	zem
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
dána	dát	k5eAaPmNgFnS	dát
nejen	nejen	k6eAd1	nejen
působením	působení	k1gNnSc7	působení
gravitace	gravitace	k1gFnSc2	gravitace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
také	také	k9	také
případnou	případný	k2eAgFnSc7d1	případná
odstředivou	odstředivý	k2eAgFnSc7d1	odstředivá
silou	síla	k1gFnSc7	síla
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hmota	hmota	k1gFnSc1	hmota
1	[number]	k4	1
kg	kg	kA	kg
poblíž	poblíž	k7c2	poblíž
zemského	zemský	k2eAgInSc2d1	zemský
povrchu	povrch	k1gInSc2	povrch
má	mít	k5eAaImIp3nS	mít
tíhu	tíha	k1gFnSc4	tíha
zhruba	zhruba	k6eAd1	zhruba
9,81	[number]	k4	9,81
N	N	kA	N
(	(	kIx(	(
<g/>
±	±	k?	±
<g/>
1	[number]	k4	1
%	%	kIx~	%
zejména	zejména	k9	zejména
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
zeměpisné	zeměpisný	k2eAgFnSc6d1	zeměpisná
šířce	šířka	k1gFnSc6	šířka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tudíž	tudíž	k8xC	tudíž
těleso	těleso	k1gNnSc1	těleso
o	o	k7c6	o
hmotnosti	hmotnost	k1gFnSc6	hmotnost
102	[number]	k4	102
gramů	gram	k1gInPc2	gram
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
Zemi	zem	k1gFnSc3	zem
přitahováno	přitahován	k2eAgNnSc4d1	přitahováno
silou	síla	k1gFnSc7	síla
přibližně	přibližně	k6eAd1	přibližně
jednoho	jeden	k4xCgInSc2	jeden
newtonu	newton	k1gInSc2	newton
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
tíhy	tíha	k1gFnSc2	tíha
tělesa	těleso	k1gNnSc2	těleso
o	o	k7c6	o
hmotnosti	hmotnost	k1gFnSc6	hmotnost
1	[number]	k4	1
kg	kg	kA	kg
byla	být	k5eAaImAgFnS	být
definována	definovat	k5eAaBmNgFnS	definovat
dřívější	dřívější	k2eAgFnSc1d1	dřívější
jednotka	jednotka	k1gFnSc1	jednotka
síly	síla	k1gFnSc2	síla
kilopond	kilopond	k1gInSc1	kilopond
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
existovala	existovat	k5eAaImAgFnS	existovat
také	také	k9	také
jednotka	jednotka	k1gFnSc1	jednotka
stén	sténa	k1gFnPc2	sténa
(	(	kIx(	(
<g/>
sthéne	sthén	k1gInSc5	sthén
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
M.	M.	kA	M.
T.	T.	kA	T.
S.	S.	kA	S.
odpovídající	odpovídající	k2eAgFnSc4d1	odpovídající
1	[number]	k4	1
kN	kN	k?	kN
<g/>
.	.	kIx.	.
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
newton	newton	k1gInSc1	newton
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
