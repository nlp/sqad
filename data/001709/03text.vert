<s>
Jim	on	k3xPp3gMnPc3	on
Morrison	Morrison	k1gMnSc1	Morrison
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1943	[number]	k4	1943
Melbourne	Melbourne	k1gNnSc2	Melbourne
-	-	kIx~	-
3	[number]	k4	3
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1971	[number]	k4	1971
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
celým	celý	k2eAgNnSc7d1	celé
jménem	jméno	k1gNnSc7	jméno
James	James	k1gMnSc1	James
Douglas	Douglas	k1gMnSc1	Douglas
Morrison	Morrison	k1gMnSc1	Morrison
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
textař	textař	k1gMnSc1	textař
a	a	k8xC	a
leader	leader	k1gMnSc1	leader
rockové	rockový	k2eAgFnSc2d1	rocková
skupiny	skupina	k1gFnSc2	skupina
The	The	k1gFnSc2	The
Doors	Doorsa	k1gFnPc2	Doorsa
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
prudkém	prudký	k2eAgInSc6d1	prudký
vzestupu	vzestup	k1gInSc6	vzestup
The	The	k1gFnSc1	The
Doors	Doors	k1gInSc1	Doors
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
se	se	k3xPyFc4	se
u	u	k7c2	u
Morrisona	Morrison	k1gMnSc2	Morrison
postupně	postupně	k6eAd1	postupně
rozvíjela	rozvíjet	k5eAaImAgFnS	rozvíjet
těžká	těžký	k2eAgFnSc1d1	těžká
alkoholová	alkoholový	k2eAgFnSc1d1	alkoholová
a	a	k8xC	a
drogová	drogový	k2eAgFnSc1d1	drogová
závislost	závislost	k1gFnSc1	závislost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vyvrcholila	vyvrcholit	k5eAaPmAgFnS	vyvrcholit
jeho	jeho	k3xOp3gInPc1	jeho
smrtí	smrt	k1gFnSc7	smrt
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
pouhých	pouhý	k2eAgNnPc2d1	pouhé
27	[number]	k4	27
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
byla	být	k5eAaImAgFnS	být
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
kombinace	kombinace	k1gFnSc2	kombinace
alkoholu	alkohol	k1gInSc2	alkohol
a	a	k8xC	a
špatného	špatný	k2eAgInSc2d1	špatný
tělesného	tělesný	k2eAgInSc2d1	tělesný
stavu	stav	k1gInSc2	stav
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
události	událost	k1gFnPc1	událost
kolem	kolem	k7c2	kolem
jeho	jeho	k3xOp3gFnSc2	jeho
smrti	smrt	k1gFnSc2	smrt
jsou	být	k5eAaImIp3nP	být
stále	stále	k6eAd1	stále
předmětem	předmět	k1gInSc7	předmět
diskuse	diskuse	k1gFnSc2	diskuse
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
před	před	k7c7	před
pohřbem	pohřeb	k1gInSc7	pohřeb
nebyla	být	k5eNaImAgFnS	být
provedena	provést	k5eAaPmNgFnS	provést
řádná	řádný	k2eAgFnSc1d1	řádná
pitva	pitva	k1gFnSc1	pitva
a	a	k8xC	a
přesná	přesný	k2eAgFnSc1d1	přesná
příčina	příčina	k1gFnSc1	příčina
jeho	jeho	k3xOp3gNnPc2	jeho
úmrtí	úmrtí	k1gNnPc2	úmrtí
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
i	i	k9	i
nadále	nadále	k6eAd1	nadále
sporná	sporný	k2eAgFnSc1d1	sporná
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
dobře	dobře	k6eAd1	dobře
znám	znát	k5eAaImIp1nS	znát
svými	svůj	k3xOyFgFnPc7	svůj
improvizovanými	improvizovaný	k2eAgFnPc7d1	improvizovaná
vsuvkami	vsuvka	k1gFnPc7	vsuvka
poezie	poezie	k1gFnSc2	poezie
během	během	k7c2	během
živých	živý	k2eAgNnPc2d1	živé
vystoupení	vystoupení	k1gNnPc2	vystoupení
The	The	k1gFnPc2	The
Doors	Doorsa	k1gFnPc2	Doorsa
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
temné	temný	k2eAgFnSc3d1	temná
osobnosti	osobnost	k1gFnSc3	osobnost
a	a	k8xC	a
divokým	divoký	k2eAgNnSc7d1	divoké
koncertním	koncertní	k2eAgNnSc7d1	koncertní
představením	představení	k1gNnSc7	představení
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
mnohými	mnohý	k2eAgMnPc7d1	mnohý
lidmi	člověk	k1gMnPc7	člověk
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgMnPc2d3	nejznámější
<g/>
,	,	kIx,	,
nejcharismatičtějších	charismatický	k2eAgInPc2d3	nejcharismatičtější
a	a	k8xC	a
nejpřesvědčivějších	přesvědčivý	k2eAgInPc2d3	nejpřesvědčivější
frontmenů	frontmen	k1gInPc2	frontmen
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
rockové	rockový	k2eAgFnSc2d1	rocková
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Morrison	Morrison	k1gInSc1	Morrison
byl	být	k5eAaImAgInS	být
zařazen	zařadit	k5eAaPmNgInS	zařadit
na	na	k7c6	na
47	[number]	k4	47
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
seznamu	seznam	k1gInSc6	seznam
"	"	kIx"	"
<g/>
100	[number]	k4	100
největších	veliký	k2eAgMnPc2d3	veliký
zpěváků	zpěvák	k1gMnPc2	zpěvák
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
"	"	kIx"	"
magazínu	magazín	k1gInSc2	magazín
Rolling	Rolling	k1gInSc1	Rolling
Stone	ston	k1gInSc5	ston
a	a	k8xC	a
na	na	k7c4	na
22	[number]	k4	22
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
"	"	kIx"	"
<g/>
50	[number]	k4	50
největších	veliký	k2eAgMnPc2d3	veliký
zpěváků	zpěvák	k1gMnPc2	zpěvák
rocku	rock	k1gInSc2	rock
<g/>
"	"	kIx"	"
britského	britský	k2eAgInSc2d1	britský
časopisu	časopis	k1gInSc2	časopis
Classic	Classice	k1gFnPc2	Classice
Rock	rock	k1gInSc1	rock
Magazine	Magazin	k1gInSc5	Magazin
<g/>
.	.	kIx.	.
</s>
<s>
Pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
rodiny	rodina	k1gFnSc2	rodina
námořního	námořní	k2eAgMnSc4d1	námořní
důstojníka	důstojník	k1gMnSc4	důstojník
George	Georg	k1gMnSc4	Georg
Stephena	Stephen	k1gMnSc4	Stephen
Morrisona	Morrison	k1gMnSc4	Morrison
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamenalo	znamenat	k5eAaImAgNnS	znamenat
neustále	neustále	k6eAd1	neustále
stěhování	stěhování	k1gNnSc1	stěhování
z	z	k7c2	z
místa	místo	k1gNnSc2	místo
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Jim	on	k3xPp3gMnPc3	on
tak	tak	k6eAd1	tak
vyrůstal	vyrůstat	k5eAaImAgInS	vyrůstat
v	v	k7c6	v
Alexandrii	Alexandrie	k1gFnSc6	Alexandrie
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Virginie	Virginie	k1gFnSc2	Virginie
<g/>
,	,	kIx,	,
ve	v	k7c6	v
floridském	floridský	k2eAgInSc6d1	floridský
Clearwateru	Clearwater	k1gInSc6	Clearwater
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
D.C.	D.C.	k1gFnSc2	D.C.
<g/>
,	,	kIx,	,
v	v	k7c6	v
Albuquerque	Albuquerque	k1gFnSc6	Albuquerque
v	v	k7c6	v
Novém	nový	k2eAgNnSc6d1	nové
Mexiku	Mexiko	k1gNnSc6	Mexiko
<g/>
,	,	kIx,	,
v	v	k7c6	v
kalifornském	kalifornský	k2eAgInSc6d1	kalifornský
Claremontu	Claremont	k1gInSc6	Claremont
a	a	k8xC	a
mnoha	mnoho	k4c3	mnoho
dalších	další	k2eAgInPc6d1	další
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
otec	otec	k1gMnSc1	otec
trávíval	trávívat	k5eAaImAgMnS	trávívat
spoustu	spousta	k1gFnSc4	spousta
času	čas	k1gInSc2	čas
mimo	mimo	k7c4	mimo
domov	domov	k1gInSc4	domov
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamenalo	znamenat	k5eAaImAgNnS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
s	s	k7c7	s
otcem	otec	k1gMnSc7	otec
dosti	dosti	k6eAd1	dosti
odcizil	odcizit	k5eAaPmAgMnS	odcizit
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
výchova	výchova	k1gFnSc1	výchova
tak	tak	k6eAd1	tak
připadla	připadnout	k5eAaPmAgFnS	připadnout
převážně	převážně	k6eAd1	převážně
matce	matka	k1gFnSc3	matka
(	(	kIx(	(
<g/>
Clara	Clara	k1gFnSc1	Clara
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
jeho	jeho	k3xOp3gMnPc2	jeho
rodiče	rodič	k1gMnPc1	rodič
byli	být	k5eAaImAgMnP	být
velice	velice	k6eAd1	velice
přísní	přísný	k2eAgMnPc1d1	přísný
a	a	k8xC	a
konzervativní	konzervativní	k2eAgMnPc1d1	konzervativní
a	a	k8xC	a
Morrison	Morrison	k1gMnSc1	Morrison
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
tedy	tedy	k9	tedy
nevycházel	vycházet	k5eNaImAgInS	vycházet
příliš	příliš	k6eAd1	příliš
dobře	dobře	k6eAd1	dobře
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
interview	interview	k1gNnSc4	interview
dokonce	dokonce	k9	dokonce
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
oba	dva	k4xCgMnPc1	dva
jeho	jeho	k3xOp3gMnPc1	jeho
rodiče	rodič	k1gMnPc1	rodič
jsou	být	k5eAaImIp3nP	být
mrtví	mrtvý	k2eAgMnPc1d1	mrtvý
<g/>
.	.	kIx.	.
</s>
<s>
Morrisonovi	Morrisonův	k2eAgMnPc1d1	Morrisonův
měli	mít	k5eAaImAgMnP	mít
ještě	ještě	k6eAd1	ještě
dvě	dva	k4xCgFnPc4	dva
děti	dítě	k1gFnPc4	dítě
<g/>
:	:	kIx,	:
Ann	Ann	k1gFnSc1	Ann
(	(	kIx(	(
<g/>
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
mladší	mladý	k2eAgFnSc2d2	mladší
než	než	k8xS	než
Jim	on	k3xPp3gMnPc3	on
<g/>
)	)	kIx)	)
a	a	k8xC	a
Andrew	Andrew	k1gMnSc1	Andrew
(	(	kIx(	(
<g/>
o	o	k7c4	o
šest	šest	k4xCc4	šest
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jim	on	k3xPp3gMnPc3	on
odmaturoval	odmaturovat	k5eAaPmAgMnS	odmaturovat
roku	rok	k1gInSc2	rok
1961	[number]	k4	1961
na	na	k7c6	na
George	George	k1gFnSc6	George
Washington	Washington	k1gInSc1	Washington
High	Higha	k1gFnPc2	Higha
School	Schoola	k1gFnPc2	Schoola
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
začíná	začínat	k5eAaImIp3nS	začínat
studovat	studovat	k5eAaImF	studovat
na	na	k7c6	na
filmové	filmový	k2eAgFnSc6d1	filmová
fakultě	fakulta	k1gFnSc6	fakulta
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angeles	k1gMnSc1	Angeles
(	(	kIx(	(
<g/>
UCLA	UCLA	kA	UCLA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
obtěžoval	obtěžovat	k5eAaImAgMnS	obtěžovat
jít	jít	k5eAaImF	jít
na	na	k7c4	na
promoci	promoce	k1gFnSc4	promoce
<g/>
,	,	kIx,	,
UCLA	UCLA	kA	UCLA
s	s	k7c7	s
odřenýma	odřený	k2eAgNnPc7d1	odřené
ušima	ucho	k1gNnPc7	ucho
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
titul	titul	k1gInSc4	titul
bakaláře	bakalář	k1gMnSc2	bakalář
-	-	kIx~	-
B.A.	B.A.	k1gMnSc2	B.A.
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
samý	samý	k3xTgInSc1	samý
rok	rok	k1gInSc1	rok
potkal	potkat	k5eAaPmAgInS	potkat
Jim	on	k3xPp3gMnPc3	on
na	na	k7c6	na
Venice	Venika	k1gFnSc6	Venika
Beach	Beacha	k1gFnPc2	Beacha
v	v	k7c6	v
L.	L.	kA	L.
A.	A.	kA	A.
Raye	Ray	k1gMnSc2	Ray
Manzareka	Manzareek	k1gMnSc2	Manzareek
<g/>
,	,	kIx,	,
bývalého	bývalý	k2eAgMnSc2d1	bývalý
spolužáka	spolužák	k1gMnSc2	spolužák
z	z	k7c2	z
UCLA	UCLA	kA	UCLA
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
Rayovi	Raya	k1gMnSc3	Raya
svěřil	svěřit	k5eAaPmAgMnS	svěřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
skládá	skládat	k5eAaImIp3nS	skládat
písňové	písňový	k2eAgInPc4d1	písňový
texty	text	k1gInPc4	text
a	a	k8xC	a
ten	ten	k3xDgInSc1	ten
ho	on	k3xPp3gInSc4	on
přesvědčil	přesvědčit	k5eAaPmAgInS	přesvědčit
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
nějaký	nějaký	k3yIgInSc4	nějaký
zazpívá	zazpívat	k5eAaPmIp3nS	zazpívat
<g/>
.	.	kIx.	.
</s>
<s>
Sedli	sednout	k5eAaPmAgMnP	sednout
si	se	k3xPyFc3	se
tedy	tedy	k8xC	tedy
do	do	k7c2	do
písku	písek	k1gInSc2	písek
a	a	k8xC	a
Jim	on	k3xPp3gMnPc3	on
mu	on	k3xPp3gMnSc3	on
zarecitoval	zarecitovat	k5eAaPmAgInS	zarecitovat
svůj	svůj	k3xOyFgInSc4	svůj
text	text	k1gInSc4	text
písně	píseň	k1gFnSc2	píseň
Moonlight	Moonlight	k2eAgInSc4d1	Moonlight
Drive	drive	k1gInSc4	drive
<g/>
.	.	kIx.	.
</s>
<s>
Ray	Ray	k?	Ray
byl	být	k5eAaImAgInS	být
nadšený	nadšený	k2eAgInSc4d1	nadšený
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
okamžik	okamžik	k1gInSc1	okamžik
odstartoval	odstartovat	k5eAaPmAgInS	odstartovat
myšlenku	myšlenka	k1gFnSc4	myšlenka
založení	založení	k1gNnSc2	založení
kapely	kapela	k1gFnSc2	kapela
The	The	k1gFnPc2	The
Doors	Doors	k1gInSc1	Doors
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
název	název	k1gInSc1	název
měl	mít	k5eAaImAgInS	mít
Jim	on	k3xPp3gMnPc3	on
už	už	k6eAd1	už
dávno	dávno	k6eAd1	dávno
promyšlený	promyšlený	k2eAgInSc1d1	promyšlený
(	(	kIx(	(
<g/>
použil	použít	k5eAaPmAgInS	použít
úryvek	úryvek	k1gInSc1	úryvek
z	z	k7c2	z
psychedelických	psychedelický	k2eAgInPc2d1	psychedelický
memoárů	memoáry	k1gInPc2	memoáry
The	The	k1gFnSc1	The
Doors	Doors	k1gInSc1	Doors
of	of	k?	of
Perception	Perception	k1gInSc1	Perception
od	od	k7c2	od
Aldouse	Aldouse	k1gFnSc2	Aldouse
Huxleyho	Huxley	k1gMnSc2	Huxley
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
použil	použít	k5eAaPmAgMnS	použít
citát	citát	k1gInSc4	citát
básníka	básník	k1gMnSc2	básník
Williama	William	k1gMnSc2	William
Blakea	Blakeus	k1gMnSc2	Blakeus
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
If	If	k1gFnSc1	If
the	the	k?	the
doors	doors	k1gInSc1	doors
of	of	k?	of
perception	perception	k1gInSc1	perception
were	werat	k5eAaPmIp3nS	werat
cleansed	cleansed	k1gInSc4	cleansed
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
everything	everything	k1gInSc1	everything
would	would	k1gMnSc1	would
appear	appear	k1gMnSc1	appear
to	ten	k3xDgNnSc4	ten
man	man	k1gMnSc1	man
as	as	k1gNnSc2	as
it	it	k?	it
truly	trul	k1gInPc1	trul
is	is	k?	is
<g/>
,	,	kIx,	,
infinite	infinit	k1gInSc5	infinit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Kdyby	kdyby	kYmCp3nP	kdyby
dveře	dveře	k1gFnPc1	dveře
vnímání	vnímání	k1gNnSc2	vnímání
byly	být	k5eAaImAgFnP	být
průzračné	průzračný	k2eAgFnPc1d1	průzračná
<g/>
,	,	kIx,	,
člověk	člověk	k1gMnSc1	člověk
by	by	kYmCp3nS	by
uzřel	uzřít	k5eAaPmAgMnS	uzřít
vše	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
opravdu	opravdu	k6eAd1	opravdu
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
nekonečné	konečný	k2eNgNnSc4d1	nekonečné
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
Čtveřici	čtveřice	k1gFnSc3	čtveřice
The	The	k1gFnSc2	The
Doors	Doorsa	k1gFnPc2	Doorsa
nakonec	nakonec	k6eAd1	nakonec
tvořili	tvořit	k5eAaImAgMnP	tvořit
zpěvák	zpěvák	k1gMnSc1	zpěvák
a	a	k8xC	a
textař	textař	k1gMnSc1	textař
Jim	on	k3xPp3gMnPc3	on
Morrison	Morrison	k1gMnSc1	Morrison
<g/>
,	,	kIx,	,
klávesista	klávesista	k1gMnSc1	klávesista
Ray	Ray	k1gMnSc1	Ray
Manzarek	Manzarka	k1gFnPc2	Manzarka
<g/>
,	,	kIx,	,
kytarista	kytarista	k1gMnSc1	kytarista
Robby	Robba	k1gFnSc2	Robba
Krieger	Krieger	k1gMnSc1	Krieger
a	a	k8xC	a
bubeník	bubeník	k1gMnSc1	bubeník
John	John	k1gMnSc1	John
Densmore	Densmor	k1gInSc5	Densmor
<g/>
.	.	kIx.	.
</s>
<s>
Jim	on	k3xPp3gMnPc3	on
miloval	milovat	k5eAaImAgMnS	milovat
poezii	poezie	k1gFnSc4	poezie
<g/>
,	,	kIx,	,
hltal	hltat	k5eAaImAgMnS	hltat
Kerouaca	Kerouacus	k1gMnSc4	Kerouacus
<g/>
,	,	kIx,	,
Rimbauda	Rimbaud	k1gMnSc4	Rimbaud
<g/>
,	,	kIx,	,
Sartra	Sartr	k1gMnSc4	Sartr
<g/>
,	,	kIx,	,
Ginsberga	Ginsberg	k1gMnSc4	Ginsberg
<g/>
,	,	kIx,	,
pročítal	pročítat	k5eAaImAgInS	pročítat
filosofii	filosofie	k1gFnSc3	filosofie
Friedricha	Friedrich	k1gMnSc2	Friedrich
Nietzscheho	Nietzsche	k1gMnSc2	Nietzsche
a	a	k8xC	a
denně	denně	k6eAd1	denně
si	se	k3xPyFc3	se
zapisoval	zapisovat	k5eAaImAgMnS	zapisovat
své	svůj	k3xOyFgMnPc4	svůj
vlastní	vlastní	k2eAgMnPc4d1	vlastní
nápady	nápad	k1gInPc4	nápad
a	a	k8xC	a
sny	sen	k1gInPc4	sen
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
psal	psát	k5eAaImAgMnS	psát
poezii	poezie	k1gFnSc4	poezie
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
jeho	jeho	k3xOp3gFnSc7	jeho
nejznámější	známý	k2eAgFnPc4d3	nejznámější
sbírky	sbírka	k1gFnPc4	sbírka
patří	patřit	k5eAaImIp3nP	patřit
,	,	kIx,	,
The	The	k1gFnPc1	The
Lords	Lords	k1gInSc1	Lords
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
New	New	k1gMnSc1	New
Creatures	Creatures	k1gMnSc1	Creatures
či	či	k8xC	či
An	An	k1gMnSc1	An
American	American	k1gMnSc1	American
Prayer	Prayer	k1gMnSc1	Prayer
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
vydal	vydat	k5eAaPmAgMnS	vydat
na	na	k7c4	na
vlastní	vlastní	k2eAgInPc4d1	vlastní
náklady	náklad	k1gInPc4	náklad
a	a	k8xC	a
jen	jen	k9	jen
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
malém	malý	k2eAgNnSc6d1	malé
množství	množství	k1gNnSc6	množství
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
rozdal	rozdat	k5eAaPmAgMnS	rozdat
mezi	mezi	k7c4	mezi
své	svůj	k3xOyFgMnPc4	svůj
nejbližší	blízký	k2eAgMnPc4d3	nejbližší
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gInPc6	jeho
písňových	písňový	k2eAgInPc6d1	písňový
i	i	k8xC	i
básnických	básnický	k2eAgInPc6d1	básnický
textech	text	k1gInPc6	text
se	se	k3xPyFc4	se
objevovaly	objevovat	k5eAaImAgInP	objevovat
motivy	motiv	k1gInPc1	motiv
hrůzy	hrůza	k1gFnSc2	hrůza
<g/>
,	,	kIx,	,
násilí	násilí	k1gNnSc2	násilí
<g/>
,	,	kIx,	,
sexu	sex	k1gInSc2	sex
<g/>
,	,	kIx,	,
vzpoury	vzpoura	k1gFnSc2	vzpoura
<g/>
,	,	kIx,	,
zániku	zánik	k1gInSc2	zánik
a	a	k8xC	a
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pódiu	pódium	k1gNnSc6	pódium
se	se	k3xPyFc4	se
po	po	k7c6	po
počáteční	počáteční	k2eAgFnSc6d1	počáteční
plachosti	plachost	k1gFnSc6	plachost
stal	stát	k5eAaPmAgInS	stát
velice	velice	k6eAd1	velice
charismatickou	charismatický	k2eAgFnSc7d1	charismatická
osobností	osobnost	k1gFnSc7	osobnost
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc4	jeho
vystoupení	vystoupení	k1gNnSc4	vystoupení
byla	být	k5eAaImAgFnS	být
nezapomenutelným	zapomenutelný	k2eNgInSc7d1	nezapomenutelný
zážitkem	zážitek	k1gInSc7	zážitek
-	-	kIx~	-
šplhal	šplhat	k5eAaImAgInS	šplhat
po	po	k7c6	po
oponách	opona	k1gFnPc6	opona
<g/>
,	,	kIx,	,
chodil	chodit	k5eAaImAgMnS	chodit
po	po	k7c6	po
okrajích	okraj	k1gInPc6	okraj
jevišti	jeviště	k1gNnSc6	jeviště
stylem	styl	k1gInSc7	styl
provazochodce	provazochodec	k1gMnSc2	provazochodec
<g/>
,	,	kIx,	,
vrhal	vrhat	k5eAaImAgMnS	vrhat
se	se	k3xPyFc4	se
po	po	k7c6	po
hlavě	hlava	k1gFnSc6	hlava
do	do	k7c2	do
davu	dav	k1gInSc2	dav
<g/>
,	,	kIx,	,
věřil	věřit	k5eAaImAgMnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
rockový	rockový	k2eAgInSc1d1	rockový
koncert	koncert	k1gInSc1	koncert
je	být	k5eAaImIp3nS	být
něco	něco	k6eAd1	něco
na	na	k7c4	na
způsob	způsob	k1gInSc4	způsob
rituálu	rituál	k1gInSc2	rituál
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
si	se	k3xPyFc3	se
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
početné	početný	k2eAgInPc4d1	početný
zástupy	zástup	k1gInPc4	zástup
fanoušků	fanoušek	k1gMnPc2	fanoušek
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
chodili	chodit	k5eAaImAgMnP	chodit
dívat	dívat	k5eAaImF	dívat
<g/>
,	,	kIx,	,
s	s	k7c7	s
čím	co	k3yRnSc7	co
se	se	k3xPyFc4	se
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
Jim	on	k3xPp3gMnPc3	on
zase	zase	k9	zase
vytasí	vytasit	k5eAaPmIp3nS	vytasit
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
na	na	k7c6	na
vystoupeních	vystoupení	k1gNnPc6	vystoupení
často	často	k6eAd1	často
objevoval	objevovat	k5eAaImAgInS	objevovat
opilý	opilý	k2eAgInSc4d1	opilý
nebo	nebo	k8xC	nebo
sjetý	sjetý	k2eAgInSc4d1	sjetý
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
oboje	oboj	k1gFnPc4	oboj
a	a	k8xC	a
obecenstvo	obecenstvo	k1gNnSc4	obecenstvo
pravidelně	pravidelně	k6eAd1	pravidelně
přiváděl	přivádět	k5eAaImAgInS	přivádět
k	k	k7c3	k
šílenství	šílenství	k1gNnSc1	šílenství
neustálým	neustálý	k2eAgNnSc7d1	neustálé
zasahováním	zasahování	k1gNnSc7	zasahování
do	do	k7c2	do
rytmu	rytmus	k1gInSc2	rytmus
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
průběhu	průběh	k1gInSc2	průběh
koncertu	koncert	k1gInSc2	koncert
<g/>
,	,	kIx,	,
nespoutanou	spoutaný	k2eNgFnSc7d1	nespoutaná
akrobacií	akrobacie	k1gFnSc7	akrobacie
a	a	k8xC	a
palebnou	palebný	k2eAgFnSc7d1	palebná
ofenzivou	ofenziva	k1gFnSc7	ofenziva
slovních	slovní	k2eAgFnPc2d1	slovní
narážek	narážka	k1gFnPc2	narážka
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
životní	životní	k2eAgFnSc7d1	životní
láskou	láska	k1gFnSc7	láska
a	a	k8xC	a
múzou	múza	k1gFnSc7	múza
byla	být	k5eAaImAgFnS	být
Pamela	Pamela	k1gFnSc1	Pamela
Courson	Coursona	k1gFnPc2	Coursona
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
přezdíval	přezdívat	k5eAaImAgMnS	přezdívat
"	"	kIx"	"
<g/>
vesmírná	vesmírný	k2eAgFnSc1d1	vesmírná
družka	družka	k1gFnSc1	družka
<g/>
"	"	kIx"	"
a	a	k8xC	a
přestože	přestože	k8xS	přestože
měl	mít	k5eAaImAgMnS	mít
během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
spoustu	spousta	k1gFnSc4	spousta
milenek	milenka	k1gFnPc2	milenka
<g/>
,	,	kIx,	,
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
se	se	k3xPyFc4	se
vždycky	vždycky	k6eAd1	vždycky
vracel	vracet	k5eAaImAgMnS	vracet
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgInS	mít
údajně	údajně	k6eAd1	údajně
poměr	poměr	k1gInSc1	poměr
s	s	k7c7	s
Janis	Janis	k1gFnSc7	Janis
Joplin	Joplina	k1gFnPc2	Joplina
<g/>
,	,	kIx,	,
zpěvačkou	zpěvačka	k1gFnSc7	zpěvačka
skupiny	skupina	k1gFnSc2	skupina
Velvet	Velveta	k1gFnPc2	Velveta
Underground	underground	k1gInSc1	underground
Nico	Nico	k6eAd1	Nico
a	a	k8xC	a
dalšími	další	k2eAgMnPc7d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
měl	mít	k5eAaImAgInS	mít
čarodějnickou	čarodějnický	k2eAgFnSc4d1	čarodějnická
svatbu	svatba	k1gFnSc4	svatba
s	s	k7c7	s
šéfredaktorkou	šéfredaktorka	k1gFnSc7	šéfredaktorka
rockového	rockový	k2eAgInSc2d1	rockový
časopisu	časopis	k1gInSc2	časopis
a	a	k8xC	a
kněžkou	kněžka	k1gFnSc7	kněžka
kultu	kult	k1gInSc2	kult
Wicca	Wiccum	k1gNnSc2	Wiccum
Patricií	Patricie	k1gFnPc2	Patricie
Kennealy	Kenneala	k1gFnSc2	Kenneala
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgMnPc2d1	další
zpěváků	zpěvák	k1gMnPc2	zpěvák
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
pil	pít	k5eAaImAgMnS	pít
nadmíru	nadmíru	k6eAd1	nadmíru
alkohol	alkohol	k1gInSc4	alkohol
a	a	k8xC	a
užíval	užívat	k5eAaImAgMnS	užívat
všechny	všechen	k3xTgMnPc4	všechen
možné	možný	k2eAgMnPc4d1	možný
druhy	druh	k1gMnPc4	druh
drog	droga	k1gFnPc2	droga
-	-	kIx~	-
halucinogenní	halucinogenní	k2eAgInSc1d1	halucinogenní
(	(	kIx(	(
<g/>
mescalin	mescalin	k1gInSc1	mescalin
<g/>
,	,	kIx,	,
LSD	LSD	kA	LSD
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgFnSc6d2	veliký
míře	míra	k1gFnSc6	míra
než	než	k8xS	než
u	u	k7c2	u
ostatních	ostatní	k2eAgMnPc2d1	ostatní
členů	člen	k1gMnPc2	člen
kapely	kapela	k1gFnSc2	kapela
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kokain	kokain	k1gInSc4	kokain
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
heroin	heroin	k1gInSc1	heroin
<g/>
.	.	kIx.	.
</s>
<s>
Známá	známá	k1gFnSc1	známá
byla	být	k5eAaImAgFnS	být
taktéž	taktéž	k?	taktéž
i	i	k8xC	i
jeho	jeho	k3xOp3gFnSc1	jeho
obsese	obsese	k1gFnSc1	obsese
smrtí	smrt	k1gFnSc7	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
způsob	způsob	k1gInSc1	způsob
života	život	k1gInSc2	život
inspiroval	inspirovat	k5eAaBmAgInS	inspirovat
i	i	k9	i
několik	několik	k4yIc4	několik
dalších	další	k2eAgFnPc2d1	další
významných	významný	k2eAgFnPc2d1	významná
osobností	osobnost	k1gFnPc2	osobnost
(	(	kIx(	(
<g/>
Iggy	Igga	k1gFnSc2	Igga
Pop	pop	k1gInSc1	pop
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc7	jeho
poznávacím	poznávací	k2eAgNnSc7d1	poznávací
znamením	znamení	k1gNnSc7	znamení
byly	být	k5eAaImAgFnP	být
úzké	úzký	k2eAgFnPc1d1	úzká
kožené	kožený	k2eAgFnPc1d1	kožená
kalhoty	kalhoty	k1gFnPc1	kalhoty
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
staly	stát	k5eAaPmAgInP	stát
ikonickým	ikonický	k2eAgInSc7d1	ikonický
znakem	znak	k1gInSc7	znak
jiných	jiný	k2eAgFnPc2d1	jiná
rockových	rockový	k2eAgFnPc2d1	rocková
skupin	skupina	k1gFnPc2	skupina
a	a	k8xC	a
rockových	rockový	k2eAgMnPc2d1	rockový
příznivců	příznivec	k1gMnPc2	příznivec
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
vedena	veden	k2eAgFnSc1d1	vedena
řada	řada	k1gFnSc1	řada
soudních	soudní	k2eAgInPc2d1	soudní
sporů	spor	k1gInPc2	spor
o	o	k7c6	o
otcovství	otcovství	k1gNnSc6	otcovství
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
mnohokráte	mnohokráte	k6eAd1	mnohokráte
zadržen	zadržet	k5eAaPmNgInS	zadržet
za	za	k7c4	za
veřejné	veřejný	k2eAgNnSc4d1	veřejné
pohoršování	pohoršování	k1gNnSc4	pohoršování
<g/>
,	,	kIx,	,
opilství	opilství	k1gNnSc4	opilství
či	či	k8xC	či
obscénní	obscénní	k2eAgNnSc4d1	obscénní
vystupování	vystupování	k1gNnSc4	vystupování
(	(	kIx(	(
<g/>
Florida	Florida	k1gFnSc1	Florida
-	-	kIx~	-
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
Haven	Havna	k1gFnPc2	Havna
-	-	kIx~	-
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
Las	laso	k1gNnPc2	laso
Vegas	Vegas	k1gInSc1	Vegas
-	-	kIx~	-
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
Miami	Miami	k1gNnSc1	Miami
-	-	kIx~	-
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
Phoenix	Phoenix	k1gInSc1	Phoenix
-	-	kIx~	-
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
Los	los	k1gInSc1	los
Angeles	Angeles	k1gInSc1	Angeles
-	-	kIx~	-
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
mladé	mladý	k2eAgNnSc4d1	mladé
publikum	publikum	k1gNnSc4	publikum
se	se	k3xPyFc4	se
ale	ale	k9	ale
stal	stát	k5eAaPmAgMnS	stát
modlou	modla	k1gFnSc7	modla
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
mužem	muž	k1gMnSc7	muž
s	s	k7c7	s
největším	veliký	k2eAgInSc7d3	veliký
sex-appealem	sexppeal	k1gInSc7	sex-appeal
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
časopisu	časopis	k1gInSc2	časopis
Rock	rock	k1gInSc1	rock
'	'	kIx"	'
<g/>
n	n	k0	n
Roll	Roll	k1gInSc4	Roll
<g/>
.	.	kIx.	.
</s>
<s>
Žil	žít	k5eAaImAgMnS	žít
naplno	naplno	k6eAd1	naplno
<g/>
.	.	kIx.	.
</s>
<s>
Časté	častý	k2eAgNnSc1d1	časté
koncertování	koncertování	k1gNnSc1	koncertování
a	a	k8xC	a
divoký	divoký	k2eAgInSc1d1	divoký
život	život	k1gInSc1	život
se	se	k3xPyFc4	se
podepsaly	podepsat	k5eAaPmAgInP	podepsat
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
zdraví	zdraví	k1gNnSc4	zdraví
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
všeho	všecek	k3xTgNnSc2	všecek
dost	dost	k6eAd1	dost
<g/>
,	,	kIx,	,
nechtěl	chtít	k5eNaImAgInS	chtít
být	být	k5eAaImF	být
pouhým	pouhý	k2eAgInSc7d1	pouhý
sexuálním	sexuální	k2eAgInSc7d1	sexuální
symbolem	symbol	k1gInSc7	symbol
<g/>
.	.	kIx.	.
</s>
<s>
Přál	přát	k5eAaImAgInS	přát
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
lidé	člověk	k1gMnPc1	člověk
chodili	chodit	k5eAaImAgMnP	chodit
na	na	k7c4	na
jejich	jejich	k3xOp3gNnSc4	jejich
koncerty	koncert	k1gInPc1	koncert
poslouchat	poslouchat	k5eAaImF	poslouchat
hudbu	hudba	k1gFnSc4	hudba
a	a	k8xC	a
texty	text	k1gInPc4	text
a	a	k8xC	a
ne	ne	k9	ne
jen	jen	k9	jen
kvůli	kvůli	k7c3	kvůli
jeho	jeho	k3xOp3gFnSc3	jeho
osobě	osoba	k1gFnSc3	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Nechal	nechat	k5eAaPmAgMnS	nechat
si	se	k3xPyFc3	se
narůst	narůst	k5eAaPmF	narůst
vousy	vous	k1gInPc4	vous
<g/>
,	,	kIx,	,
přibral	přibrat	k5eAaPmAgInS	přibrat
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
se	se	k3xPyFc4	se
čím	čí	k3xOyQgNnSc7	čí
dál	daleko	k6eAd2	daleko
častěji	často	k6eAd2	často
objevovat	objevovat	k5eAaImF	objevovat
s	s	k7c7	s
lahví	lahev	k1gFnSc7	lahev
whisky	whisky	k1gFnSc2	whisky
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
vrátil	vrátit	k5eAaPmAgMnS	vrátit
k	k	k7c3	k
Pamele	Pamela	k1gFnSc3	Pamela
a	a	k8xC	a
společně	společně	k6eAd1	společně
se	se	k3xPyFc4	se
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
odjet	odjet	k5eAaPmF	odjet
na	na	k7c4	na
čas	čas	k1gInSc4	čas
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Odpočinout	odpočinout	k5eAaPmF	odpočinout
si	se	k3xPyFc3	se
po	po	k7c6	po
nahrání	nahrání	k1gNnSc6	nahrání
posledního	poslední	k2eAgNnSc2d1	poslední
společného	společný	k2eAgNnSc2d1	společné
alba	album	k1gNnSc2	album
The	The	k1gFnSc2	The
Doors	Doorsa	k1gFnPc2	Doorsa
L.A.	L.A.	k1gMnSc1	L.A.
Woman	Woman	k1gMnSc1	Woman
a	a	k8xC	a
soustředit	soustředit	k5eAaPmF	soustředit
se	se	k3xPyFc4	se
na	na	k7c4	na
psaní	psaní	k1gNnSc4	psaní
poezie	poezie	k1gFnSc2	poezie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
nakonec	nakonec	k6eAd1	nakonec
i	i	k9	i
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
nalezen	nalezen	k2eAgMnSc1d1	nalezen
mrtvý	mrtvý	k1gMnSc1	mrtvý
ve	v	k7c6	v
vaně	vana	k1gFnSc6	vana
svého	svůj	k3xOyFgInSc2	svůj
pařížského	pařížský	k2eAgInSc2d1	pařížský
bytu	byt	k1gInSc2	byt
na	na	k7c4	na
Rue	Rue	k1gFnSc4	Rue
Beautreillis	Beautreillis	k1gFnSc2	Beautreillis
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
příčina	příčina	k1gFnSc1	příčina
smrti	smrt	k1gFnSc2	smrt
byla	být	k5eAaImAgFnS	být
oficiálně	oficiálně	k6eAd1	oficiálně
uvedena	uvést	k5eAaPmNgFnS	uvést
srdeční	srdeční	k2eAgFnSc1d1	srdeční
zástava	zástava	k1gFnSc1	zástava
<g/>
.	.	kIx.	.
</s>
<s>
Hypotéza	hypotéza	k1gFnSc1	hypotéza
<g/>
,	,	kIx,	,
že	že	k8xS	že
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c6	o
předávkování	předávkování	k1gNnSc6	předávkování
drogami	droga	k1gFnPc7	droga
<g/>
,	,	kIx,	,
nebyla	být	k5eNaImAgFnS	být
jednoznačně	jednoznačně	k6eAd1	jednoznačně
potvrzena	potvrdit	k5eAaPmNgFnS	potvrdit
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
lidé	člověk	k1gMnPc1	člověk
však	však	k9	však
věří	věřit	k5eAaImIp3nP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
stále	stále	k6eAd1	stále
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
ústraní	ústraní	k1gNnSc6	ústraní
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
ženou	žena	k1gFnSc7	žena
Pamelou	Pamela	k1gFnSc7	Pamela
Courson	Courson	k1gInSc1	Courson
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tato	tento	k3xDgFnSc1	tento
verze	verze	k1gFnSc1	verze
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
nepravděpodobná	pravděpodobný	k2eNgFnSc1d1	nepravděpodobná
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Pamela	Pamela	k1gFnSc1	Pamela
zemřela	zemřít	k5eAaPmAgFnS	zemřít
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
po	po	k7c6	po
něm	on	k3xPp3gMnSc6	on
na	na	k7c6	na
předávkování	předávkování	k1gNnSc2	předávkování
drogami	droga	k1gFnPc7	droga
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
životě	život	k1gInSc6	život
a	a	k8xC	a
skupině	skupina	k1gFnSc6	skupina
The	The	k1gFnSc1	The
Doors	Doorsa	k1gFnPc2	Doorsa
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
například	například	k6eAd1	například
kniha	kniha	k1gFnSc1	kniha
Jerryho	Jerry	k1gMnSc2	Jerry
Hopkinse	Hopkins	k1gMnSc2	Hopkins
a	a	k8xC	a
Daniela	Daniel	k1gMnSc2	Daniel
Sugermana	Sugerman	k1gMnSc2	Sugerman
Nikdo	nikdo	k3yNnSc1	nikdo
to	ten	k3xDgNnSc4	ten
tu	tu	k6eAd1	tu
nepřežije	přežít	k5eNaPmIp3nS	přežít
orig	orig	k1gInSc1	orig
<g/>
.	.	kIx.	.
</s>
<s>
No	no	k9	no
One	One	k1gMnSc1	One
Here	Here	k1gFnPc2	Here
Gets	Gets	k1gInSc4	Gets
Out	Out	k1gFnSc4	Out
Alive	Aliev	k1gFnSc2	Aliev
poprvé	poprvé	k6eAd1	poprvé
vydaná	vydaný	k2eAgNnPc1d1	vydané
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
natočil	natočit	k5eAaBmAgMnS	natočit
americký	americký	k2eAgMnSc1d1	americký
režisér	režisér	k1gMnSc1	režisér
Oliver	Oliver	k1gMnSc1	Oliver
Stone	ston	k1gInSc5	ston
snímek	snímek	k1gInSc4	snímek
The	The	k1gFnSc3	The
Doors	Doors	k1gInSc1	Doors
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
líčí	líčit	k5eAaImIp3nS	líčit
zejména	zejména	k9	zejména
charismatickou	charismatický	k2eAgFnSc4d1	charismatická
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
problematickou	problematický	k2eAgFnSc4d1	problematická
postavu	postava	k1gFnSc4	postava
Jima	Jimus	k1gMnSc2	Jimus
Morrisona	Morrison	k1gMnSc2	Morrison
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
film	film	k1gInSc1	film
a	a	k8xC	a
zejména	zejména	k9	zejména
samotné	samotný	k2eAgNnSc1d1	samotné
vyobrazení	vyobrazení	k1gNnSc1	vyobrazení
Jimovy	Jimův	k2eAgFnSc2d1	Jimova
osobnosti	osobnost	k1gFnSc2	osobnost
ovšem	ovšem	k9	ovšem
nelibě	libě	k6eNd1	libě
nesou	nést	k5eAaImIp3nP	nést
zbylí	zbylý	k2eAgMnPc1d1	zbylý
členové	člen	k1gMnPc1	člen
kapely	kapela	k1gFnSc2	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Raye	Ray	k1gInSc2	Ray
Manzareka	Manzareek	k1gInSc2	Manzareek
byl	být	k5eAaImAgMnS	být
Jim	on	k3xPp3gMnPc3	on
velice	velice	k6eAd1	velice
inteligentní	inteligentní	k2eAgMnSc1d1	inteligentní
a	a	k8xC	a
talentovaný	talentovaný	k2eAgMnSc1d1	talentovaný
kluk	kluk	k1gMnSc1	kluk
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
smyslem	smysl	k1gInSc7	smysl
pro	pro	k7c4	pro
humor	humor	k1gInSc4	humor
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgNnSc4	jenž
chování	chování	k1gNnSc4	chování
bohužel	bohužel	k6eAd1	bohužel
ovlivňoval	ovlivňovat	k5eAaImAgInS	ovlivňovat
alkohol	alkohol	k1gInSc1	alkohol
a	a	k8xC	a
špatní	špatný	k2eAgMnPc1d1	špatný
"	"	kIx"	"
<g/>
kamarádi	kamarád	k1gMnPc1	kamarád
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Diskografie	diskografie	k1gFnSc2	diskografie
The	The	k1gFnSc2	The
Doors	Doorsa	k1gFnPc2	Doorsa
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Lords	Lords	k1gInSc1	Lords
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
New	New	k1gMnSc1	New
Creatures	Creatures	k1gMnSc1	Creatures
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
An	An	k1gMnSc1	An
American	American	k1gMnSc1	American
Prayer	Prayer	k1gMnSc1	Prayer
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
Wilderness	Wilderness	k1gInSc4	Wilderness
<g/>
:	:	kIx,	:
The	The	k1gMnSc2	The
Lost	Lost	k2eAgInSc1d1	Lost
Writings	Writings	k1gInSc1	Writings
Of	Of	k1gFnSc7	Of
Jim	on	k3xPp3gMnPc3	on
Morrison	Morrison	k1gInSc1	Morrison
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
American	American	k1gMnSc1	American
Night	Night	k1gMnSc1	Night
<g/>
:	:	kIx,	:
The	The	k1gMnSc2	The
Lost	Lost	k2eAgInSc1d1	Lost
Writings	Writings	k1gInSc1	Writings
Of	Of	k1gFnSc7	Of
Jim	on	k3xPp3gMnPc3	on
Morrison	Morrison	k1gInSc1	Morrison
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Jim	on	k3xPp3gFnPc3	on
Morrison	Morrison	k1gInSc1	Morrison
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Osoba	osoba	k1gFnSc1	osoba
Jim	on	k3xPp3gMnPc3	on
Morrison	Morrisona	k1gFnPc2	Morrisona
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Seznam	seznam	k1gInSc4	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Jim	on	k3xPp3gMnPc3	on
Morrison	Morrison	k1gMnSc1	Morrison
A	a	k8xC	a
Tribute	tribut	k1gInSc5	tribut
To	ten	k3xDgNnSc1	ten
Jim	on	k3xPp3gMnPc3	on
Morrison	Morrisona	k1gFnPc2	Morrisona
</s>
