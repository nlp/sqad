<p>
<s>
Barnardova	Barnardův	k2eAgFnSc1d1	Barnardova
šipka	šipka	k1gFnSc1	šipka
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
Barnardova	Barnardův	k2eAgFnSc1d1	Barnardova
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hvězda	hvězda	k1gFnSc1	hvězda
v	v	k7c6	v
souhvězdí	souhvězdí	k1gNnSc6	souhvězdí
Hadonoše	hadonoš	k1gMnSc2	hadonoš
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
hvězd	hvězda	k1gFnPc2	hvězda
nejrychlejší	rychlý	k2eAgInSc1d3	nejrychlejší
vlastní	vlastní	k2eAgInSc1d1	vlastní
pohyb	pohyb	k1gInSc1	pohyb
po	po	k7c6	po
obloze	obloha	k1gFnSc6	obloha
<g/>
:	:	kIx,	:
činí	činit	k5eAaImIp3nS	činit
10,34	[number]	k4	10,34
<g/>
"	"	kIx"	"
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Hvězda	hvězda	k1gFnSc1	hvězda
vzdálená	vzdálený	k2eAgFnSc1d1	vzdálená
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
necelých	celý	k2eNgInPc2d1	necelý
šest	šest	k4xCc4	šest
světelných	světelný	k2eAgNnPc2d1	světelné
let	léto	k1gNnPc2	léto
je	být	k5eAaImIp3nS	být
málo	málo	k6eAd1	málo
hmotným	hmotný	k2eAgMnSc7d1	hmotný
červeným	červený	k2eAgMnSc7d1	červený
trpaslíkem	trpaslík	k1gMnSc7	trpaslík
spektrální	spektrální	k2eAgFnSc2d1	spektrální
třídy	třída	k1gFnSc2	třída
M4	M4	k1gFnPc2	M4
a	a	k8xC	a
čtvrtou	čtvrtý	k4xOgFnSc7	čtvrtý
nejbližší	blízký	k2eAgFnSc7d3	nejbližší
hvězdou	hvězda	k1gFnSc7	hvězda
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
(	(	kIx(	(
<g/>
po	po	k7c6	po
třech	tři	k4xCgFnPc6	tři
hvězdách	hvězda	k1gFnPc6	hvězda
soustavy	soustava	k1gFnSc2	soustava
Alfa	alfa	k1gNnSc2	alfa
Centauri	Centaur	k1gFnSc2	Centaur
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
svou	svůj	k3xOyFgFnSc4	svůj
blízkost	blízkost	k1gFnSc4	blízkost
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
hvězdy	hvězda	k1gFnPc4	hvězda
jen	jen	k9	jen
deváté	devátý	k4xOgFnPc1	devátý
magnitudy	magnituda	k1gFnPc1	magnituda
<g/>
,	,	kIx,	,
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
pozorování	pozorování	k1gNnSc3	pozorování
tedy	tedy	k8xC	tedy
potřebujete	potřebovat	k5eAaImIp2nP	potřebovat
dalekohled	dalekohled	k1gInSc4	dalekohled
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
infračervené	infračervený	k2eAgFnSc6d1	infračervená
oblasti	oblast	k1gFnSc6	oblast
spektra	spektrum	k1gNnSc2	spektrum
září	zářit	k5eAaImIp3nS	zářit
mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
než	než	k8xS	než
ve	v	k7c6	v
viditelném	viditelný	k2eAgNnSc6d1	viditelné
světle	světlo	k1gNnSc6	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
jejího	její	k3xOp3gInSc2	její
pohybu	pohyb	k1gInSc2	pohyb
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1916	[number]	k4	1916
změřil	změřit	k5eAaPmAgMnS	změřit
astronom	astronom	k1gMnSc1	astronom
Edward	Edward	k1gMnSc1	Edward
Emerson	Emerson	k1gMnSc1	Emerson
Barnard	Barnard	k1gMnSc1	Barnard
<g/>
,	,	kIx,	,
na	na	k7c4	na
jehož	jehož	k3xOyRp3gFnSc4	jehož
počest	počest	k1gFnSc4	počest
byla	být	k5eAaImAgFnS	být
hvězda	hvězda	k1gFnSc1	hvězda
pojmenována	pojmenovat	k5eAaPmNgFnS	pojmenovat
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
nejrychlejším	rychlý	k2eAgInSc7d3	nejrychlejší
známým	známý	k2eAgInSc7d1	známý
pohybem	pohyb	k1gInSc7	pohyb
po	po	k7c6	po
obloze	obloha	k1gFnSc6	obloha
vyznačovala	vyznačovat	k5eAaImAgFnS	vyznačovat
Kapteynova	Kapteynův	k2eAgFnSc1d1	Kapteynův
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
poblíž	poblíž	k7c2	poblíž
hvězdy	hvězda	k1gFnSc2	hvězda
66	[number]	k4	66
Oph	Oph	k1gFnPc2	Oph
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Barnardova	Barnardův	k2eAgFnSc1d1	Barnardova
šipka	šipka	k1gFnSc1	šipka
se	se	k3xPyFc4	se
především	především	k9	především
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
blízkosti	blízkost	k1gFnSc3	blízkost
a	a	k8xC	a
poloze	poloha	k1gFnSc3	poloha
na	na	k7c6	na
nebeském	nebeský	k2eAgInSc6d1	nebeský
rovníku	rovník	k1gInSc6	rovník
stala	stát	k5eAaPmAgFnS	stát
předmětem	předmět	k1gInSc7	předmět
studia	studio	k1gNnSc2	studio
mnoha	mnoho	k4c2	mnoho
astronomů	astronom	k1gMnPc2	astronom
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
pozorování	pozorování	k1gNnSc2	pozorování
zkoumali	zkoumat	k5eAaImAgMnP	zkoumat
její	její	k3xOp3gFnPc4	její
vlastnosti	vlastnost	k1gFnPc4	vlastnost
a	a	k8xC	a
astrometrii	astrometrie	k1gFnSc4	astrometrie
<g/>
;	;	kIx,	;
také	také	k9	také
u	u	k7c2	u
ní	on	k3xPp3gFnSc2	on
hledali	hledat	k5eAaImAgMnP	hledat
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
velmi	velmi	k6eAd1	velmi
vysoké	vysoký	k2eAgNnSc4d1	vysoké
stáří	stáří	k1gNnSc4	stáří
Barnardovy	Barnardův	k2eAgFnSc2d1	Barnardova
šipky	šipka	k1gFnSc2	šipka
se	se	k3xPyFc4	se
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
dají	dát	k5eAaPmIp3nP	dát
pozorovat	pozorovat	k5eAaImF	pozorovat
erupce	erupce	k1gFnPc1	erupce
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
eruptivní	eruptivní	k2eAgFnSc7d1	eruptivní
proměnnou	proměnný	k2eAgFnSc7d1	proměnná
hvězdou	hvězda	k1gFnSc7	hvězda
typu	typ	k1gInSc2	typ
UV	UV	kA	UV
Ceti	Ceti	k1gNnSc7	Ceti
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
přijala	přijmout	k5eAaPmAgFnS	přijmout
řada	řada	k1gFnSc1	řada
astronomů	astronom	k1gMnPc2	astronom
domněnku	domněnka	k1gFnSc4	domněnka
Petera	Peter	k1gMnSc2	Peter
van	van	k1gInSc1	van
de	de	k?	de
Kampa	Kampa	k1gFnSc1	Kampa
<g/>
,	,	kIx,	,
že	že	k8xS	že
objevil	objevit	k5eAaPmAgMnS	objevit
odchylku	odchylka	k1gFnSc4	odchylka
v	v	k7c6	v
pohybu	pohyb	k1gInSc6	pohyb
Barnardovy	Barnardův	k2eAgFnSc2d1	Barnardova
šipky	šipka	k1gFnSc2	šipka
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yIgFnSc2	který
mělo	mít	k5eAaImAgNnS	mít
vyplývat	vyplývat	k5eAaImF	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
hvězdu	hvězda	k1gFnSc4	hvězda
obíhá	obíhat	k5eAaImIp3nS	obíhat
jedna	jeden	k4xCgFnSc1	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
planet	planeta	k1gFnPc2	planeta
o	o	k7c6	o
hmotnosti	hmotnost	k1gFnSc6	hmotnost
Jupitera	Jupiter	k1gMnSc2	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Data	datum	k1gNnPc1	datum
nezávisle	závisle	k6eNd1	závisle
získaná	získaný	k2eAgFnSc1d1	získaná
v	v	k7c6	v
sedmdesátých	sedmdesátý	k4xOgNnPc6	sedmdesátý
letech	léto	k1gNnPc6	léto
však	však	k9	však
žádné	žádný	k3yNgFnPc4	žádný
odchylky	odchylka	k1gFnPc4	odchylka
pohybu	pohyb	k1gInSc2	pohyb
nepotvrdila	potvrdit	k5eNaPmAgNnP	potvrdit
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
nevyloučila	vyloučit	k5eNaPmAgFnS	vyloučit
existenci	existence	k1gFnSc4	existence
planet	planeta	k1gFnPc2	planeta
terestrického	terestrický	k2eAgInSc2d1	terestrický
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Exoplaneta	Exoplaneta	k1gFnSc1	Exoplaneta
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
u	u	k7c2	u
Barnardovy	Barnardův	k2eAgFnSc2d1	Barnardova
šipky	šipka	k1gFnSc2	šipka
objevena	objevit	k5eAaPmNgFnS	objevit
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
projektu	projekt	k1gInSc2	projekt
Red	Red	k1gFnSc2	Red
Dots	Dotsa	k1gFnPc2	Dotsa
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
chladnou	chladnout	k5eAaImIp3nP	chladnout
superzemi	superze	k1gFnPc7	superze
o	o	k7c6	o
hmotnosti	hmotnost	k1gFnSc6	hmotnost
nejméně	málo	k6eAd3	málo
3,2	[number]	k4	3,2
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
okolo	okolo	k7c2	okolo
hvězdy	hvězda	k1gFnSc2	hvězda
obíhá	obíhat	k5eAaImIp3nS	obíhat
s	s	k7c7	s
periodou	perioda	k1gFnSc7	perioda
233	[number]	k4	233
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
největšího	veliký	k2eAgNnSc2d3	veliký
rozšíření	rozšíření	k1gNnSc2	rozšíření
domněnky	domněnka	k1gFnSc2	domněnka
o	o	k7c6	o
hypotetických	hypotetický	k2eAgFnPc6d1	hypotetická
planetách	planeta	k1gFnPc6	planeta
si	se	k3xPyFc3	se
hvězdu	hvězda	k1gFnSc4	hvězda
oblíbili	oblíbit	k5eAaPmAgMnP	oblíbit
autoři	autor	k1gMnPc1	autor
science	scienec	k1gInSc2	scienec
fiction	fiction	k1gInSc1	fiction
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
cílem	cíl	k1gInSc7	cíl
projektu	projekt	k1gInSc2	projekt
Daedalus	Daedalus	k1gInSc4	Daedalus
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byla	být	k5eAaImAgFnS	být
studie	studie	k1gFnSc1	studie
o	o	k7c6	o
možnosti	možnost	k1gFnSc6	možnost
vyslání	vyslání	k1gNnSc2	vyslání
rychlé	rychlý	k2eAgFnSc2d1	rychlá
bezpilotní	bezpilotní	k2eAgFnSc2d1	bezpilotní
sondy	sonda	k1gFnSc2	sonda
k	k	k7c3	k
blízkým	blízký	k2eAgFnPc3d1	blízká
hvězdám	hvězda	k1gFnPc3	hvězda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fyzikální	fyzikální	k2eAgFnPc4d1	fyzikální
vlastnosti	vlastnost	k1gFnPc4	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
Barnardova	Barnardův	k2eAgFnSc1d1	Barnardova
hvězda	hvězda	k1gFnSc1	hvězda
–	–	k?	–
červený	červený	k2eAgMnSc1d1	červený
trpaslík	trpaslík	k1gMnSc1	trpaslík
spektrální	spektrální	k2eAgFnSc2d1	spektrální
třídy	třída	k1gFnSc2	třída
M4	M4	k1gMnSc1	M4
–	–	k?	–
svítí	svítit	k5eAaImIp3nS	svítit
příliš	příliš	k6eAd1	příliš
slabě	slabě	k6eAd1	slabě
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ji	on	k3xPp3gFnSc4	on
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
pozorovat	pozorovat	k5eAaImF	pozorovat
bez	bez	k7c2	bez
dalekohledu	dalekohled	k1gInSc2	dalekohled
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
zdánlivou	zdánlivý	k2eAgFnSc4d1	zdánlivá
hvězdnou	hvězdný	k2eAgFnSc4d1	hvězdná
velikost	velikost	k1gFnSc4	velikost
9,54	[number]	k4	9,54
mag	mag	k?	mag
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
nejjasnější	jasný	k2eAgFnSc1d3	nejjasnější
hvězda	hvězda	k1gFnSc1	hvězda
oblohy	obloha	k1gFnSc2	obloha
<g/>
,	,	kIx,	,
Sirius	Sirius	k1gMnSc1	Sirius
<g/>
,	,	kIx,	,
-1,5	-1,5	k4	-1,5
mag	mag	k?	mag
a	a	k8xC	a
nejslabší	slabý	k2eAgFnSc1d3	nejslabší
hvězda	hvězda	k1gFnSc1	hvězda
pozorovatelná	pozorovatelný	k2eAgFnSc1d1	pozorovatelná
pouhým	pouhý	k2eAgNnSc7d1	pouhé
okem	oke	k1gNnSc7	oke
6	[number]	k4	6
mag	mag	k?	mag
<g/>
;	;	kIx,	;
proto	proto	k8xC	proto
magnituda	magnituda	k1gFnSc1	magnituda
9,54	[number]	k4	9,54
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
27	[number]	k4	27
jasnosti	jasnost	k1gFnPc4	jasnost
nejslabší	slabý	k2eAgFnSc2d3	nejslabší
hvězdy	hvězda	k1gFnSc2	hvězda
pozorovatelné	pozorovatelný	k2eAgFnSc2d1	pozorovatelná
pouhým	pouhý	k2eAgNnSc7d1	pouhé
okem	oke	k1gNnSc7	oke
za	za	k7c2	za
dobrých	dobrý	k2eAgFnPc2d1	dobrá
světelných	světelný	k2eAgFnPc2d1	světelná
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
stáří	stáří	k1gNnSc6	stáří
mezi	mezi	k7c7	mezi
7	[number]	k4	7
až	až	k9	až
12	[number]	k4	12
miliardami	miliarda	k4xCgFnPc7	miliarda
let	léto	k1gNnPc2	léto
je	být	k5eAaImIp3nS	být
výrazně	výrazně	k6eAd1	výrazně
starší	starý	k2eAgFnSc7d2	starší
hvězdou	hvězda	k1gFnSc7	hvězda
než	než	k8xS	než
Slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejstarší	starý	k2eAgFnPc4d3	nejstarší
hvězdy	hvězda	k1gFnPc4	hvězda
v	v	k7c6	v
naší	náš	k3xOp1gFnSc6	náš
galaxii	galaxie	k1gFnSc6	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Hvězda	hvězda	k1gFnSc1	hvězda
již	již	k6eAd1	již
ztratila	ztratit	k5eAaPmAgFnS	ztratit
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
rotační	rotační	k2eAgFnSc2d1	rotační
energie	energie	k1gFnSc2	energie
<g/>
:	:	kIx,	:
pravidelné	pravidelný	k2eAgFnPc1d1	pravidelná
mírné	mírný	k2eAgFnPc1d1	mírná
změny	změna	k1gFnPc1	změna
v	v	k7c6	v
jasnosti	jasnost	k1gFnSc6	jasnost
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
otočí	otočit	k5eAaPmIp3nP	otočit
kolem	kolem	k6eAd1	kolem
osy	osa	k1gFnPc1	osa
pouze	pouze	k6eAd1	pouze
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
130	[number]	k4	130
dnů	den	k1gInPc2	den
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
Slunce	slunce	k1gNnSc1	slunce
za	za	k7c4	za
25	[number]	k4	25
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
vysokému	vysoký	k2eAgInSc3d1	vysoký
věku	věk	k1gInSc3	věk
se	se	k3xPyFc4	se
u	u	k7c2	u
hvězdy	hvězda	k1gFnSc2	hvězda
předpokládala	předpokládat	k5eAaImAgFnS	předpokládat
malá	malý	k2eAgFnSc1d1	malá
aktivita	aktivita	k1gFnSc1	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
u	u	k7c2	u
ní	on	k3xPp3gFnSc6	on
astronomové	astronom	k1gMnPc1	astronom
pozorovali	pozorovat	k5eAaImAgMnP	pozorovat
intenzivní	intenzivní	k2eAgFnPc4d1	intenzivní
hvězdné	hvězdný	k2eAgFnPc4d1	hvězdná
erupce	erupce	k1gFnPc4	erupce
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
překvapivě	překvapivě	k6eAd1	překvapivě
ukázaly	ukázat	k5eAaPmAgFnP	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Barnardova	Barnardův	k2eAgFnSc1d1	Barnardova
hvězda	hvězda	k1gFnSc1	hvězda
je	být	k5eAaImIp3nS	být
eruptivní	eruptivní	k2eAgFnSc7d1	eruptivní
proměnnou	proměnný	k2eAgFnSc7d1	proměnná
hvězdou	hvězda	k1gFnSc7	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
taková	takový	k3xDgNnPc4	takový
nese	nést	k5eAaImIp3nS	nést
označení	označení	k1gNnSc1	označení
V2500	V2500	k1gFnSc2	V2500
Ophiuchi	Ophiuch	k1gFnSc2	Ophiuch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
byly	být	k5eAaImAgFnP	být
u	u	k7c2	u
Barnardovy	Barnardův	k2eAgFnSc2d1	Barnardova
hvězdy	hvězda	k1gFnSc2	hvězda
zjištěny	zjistit	k5eAaPmNgFnP	zjistit
první	první	k4xOgFnPc1	první
změny	změna	k1gFnPc1	změna
v	v	k7c6	v
radiální	radiální	k2eAgFnSc6d1	radiální
rychlosti	rychlost	k1gFnSc6	rychlost
způsobené	způsobený	k2eAgFnSc2d1	způsobená
jejím	její	k3xOp3gInSc7	její
pohybem	pohyb	k1gInSc7	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
proměnlivost	proměnlivost	k1gFnSc1	proměnlivost
této	tento	k3xDgFnSc2	tento
rychlosti	rychlost	k1gFnSc2	rychlost
byla	být	k5eAaImAgFnS	být
přičítána	přičítán	k2eAgFnSc1d1	přičítána
jejím	její	k3xOp3gFnPc3	její
hvězdným	hvězdný	k2eAgFnPc3d1	hvězdná
aktivitám	aktivita	k1gFnPc3	aktivita
<g/>
.	.	kIx.	.
<g/>
Vlastní	vlastní	k2eAgInSc1d1	vlastní
pohyb	pohyb	k1gInSc1	pohyb
Barnardovy	Barnardův	k2eAgFnSc2d1	Barnardova
hvězdy	hvězda	k1gFnSc2	hvězda
po	po	k7c6	po
obloze	obloha	k1gFnSc6	obloha
je	být	k5eAaImIp3nS	být
10,4	[number]	k4	10,4
obloukových	obloukový	k2eAgFnPc2d1	oblouková
sekund	sekunda	k1gFnPc2	sekunda
ročně	ročně	k6eAd1	ročně
a	a	k8xC	a
její	její	k3xOp3gInSc4	její
vlastní	vlastní	k2eAgInSc4d1	vlastní
boční	boční	k2eAgInSc4d1	boční
pohyb	pohyb	k1gInSc4	pohyb
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
rychlosti	rychlost	k1gFnSc2	rychlost
90	[number]	k4	90
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Za	za	k7c4	za
lidský	lidský	k2eAgInSc4d1	lidský
život	život	k1gInSc4	život
se	se	k3xPyFc4	se
Barnardova	Barnardův	k2eAgFnSc1d1	Barnardova
hvězda	hvězda	k1gFnSc1	hvězda
na	na	k7c6	na
pozemské	pozemský	k2eAgFnSc6d1	pozemská
obloze	obloha	k1gFnSc6	obloha
přemístí	přemístit	k5eAaPmIp3nS	přemístit
o	o	k7c4	o
čtvrtinu	čtvrtina	k1gFnSc4	čtvrtina
úhlového	úhlový	k2eAgInSc2d1	úhlový
průměru	průměr	k1gInSc2	průměr
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
<g/>
Radiální	radiální	k2eAgFnSc1d1	radiální
rychlost	rychlost	k1gFnSc1	rychlost
Barnardovy	Barnardův	k2eAgFnSc2d1	Barnardova
hvězdy	hvězda	k1gFnSc2	hvězda
vůči	vůči	k7c3	vůči
Slunci	slunce	k1gNnSc3	slunce
lze	lze	k6eAd1	lze
určit	určit	k5eAaPmF	určit
z	z	k7c2	z
jejího	její	k3xOp3gInSc2	její
modrého	modrý	k2eAgInSc2d1	modrý
posunu	posun	k1gInSc2	posun
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
katalogu	katalog	k1gInSc6	katalog
SIMBAD	SIMBAD	kA	SIMBAD
je	být	k5eAaImIp3nS	být
uvedena	uveden	k2eAgFnSc1d1	uvedena
rychlost	rychlost	k1gFnSc1	rychlost
106,8	[number]	k4	106,8
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vychází	vycházet	k5eAaImIp3nS	vycházet
ze	z	k7c2	z
starších	starý	k2eAgNnPc2d2	starší
měření	měření	k1gNnPc2	měření
(	(	kIx(	(
<g/>
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rychlost	rychlost	k1gFnSc1	rychlost
110,8	[number]	k4	110,8
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
v	v	k7c6	v
katalogu	katalog	k1gInSc6	katalog
ARICNS	ARICNS	kA	ARICNS
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
měření	měření	k1gNnSc4	měření
novější	nový	k2eAgNnSc4d2	novější
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
s	s	k7c7	s
vlastním	vlastní	k2eAgInSc7d1	vlastní
pohybem	pohyb	k1gInSc7	pohyb
hvězdy	hvězda	k1gFnSc2	hvězda
vychází	vycházet	k5eAaImIp3nS	vycházet
skutečná	skutečný	k2eAgFnSc1d1	skutečná
rychlost	rychlost	k1gFnSc1	rychlost
hvězdy	hvězda	k1gFnSc2	hvězda
vzhledem	vzhled	k1gInSc7	vzhled
ke	k	k7c3	k
Slunci	slunce	k1gNnSc3	slunce
139,7	[number]	k4	139,7
nebo	nebo	k8xC	nebo
142,7	[number]	k4	142,7
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Barnardova	Barnardův	k2eAgFnSc1d1	Barnardova
hvězda	hvězda	k1gFnSc1	hvězda
bude	být	k5eAaImBp3nS	být
nejblíže	blízce	k6eAd3	blízce
Slunci	slunce	k1gNnSc3	slunce
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
11	[number]	k4	11
700	[number]	k4	700
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
přiblíží	přiblížit	k5eAaPmIp3nS	přiblížit
na	na	k7c4	na
3,7	[number]	k4	3,7
světelných	světelný	k2eAgNnPc2d1	světelné
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
ani	ani	k8xC	ani
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
nebude	být	k5eNaImBp3nS	být
nejbližší	blízký	k2eAgMnSc1d3	nejbližší
hvězdou	hvězda	k1gFnSc7	hvězda
Slunci	slunce	k1gNnSc3	slunce
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
blíž	blízce	k6eAd2	blízce
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
už	už	k9	už
bude	být	k5eAaImBp3nS	být
Proxima	Proxima	k1gFnSc1	Proxima
Centauri	Centaur	k1gFnSc2	Centaur
<g/>
.	.	kIx.	.
</s>
<s>
Barnardova	Barnardův	k2eAgFnSc1d1	Barnardova
hvězda	hvězda	k1gFnSc1	hvězda
se	s	k7c7	s
zdánlivou	zdánlivý	k2eAgFnSc7d1	zdánlivá
hvězdnou	hvězdný	k2eAgFnSc7d1	hvězdná
velikostí	velikost	k1gFnSc7	velikost
8,5	[number]	k4	8,5
mag	mag	k?	mag
bude	být	k5eAaImBp3nS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
na	na	k7c4	na
pozorování	pozorování	k1gNnSc4	pozorování
pouhým	pouhý	k2eAgNnSc7d1	pouhé
okem	oke	k1gNnSc7	oke
příliš	příliš	k6eAd1	příliš
slabá	slabý	k2eAgFnSc1d1	slabá
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
začne	začít	k5eAaPmIp3nS	začít
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
vzdalovat	vzdalovat	k5eAaImF	vzdalovat
<g/>
.	.	kIx.	.
<g/>
Barnardova	Barnardův	k2eAgFnSc1d1	Barnardova
hvězda	hvězda	k1gFnSc1	hvězda
má	mít	k5eAaImIp3nS	mít
přibližně	přibližně	k6eAd1	přibližně
17	[number]	k4	17
procent	procento	k1gNnPc2	procento
hmotnosti	hmotnost	k1gFnSc2	hmotnost
Slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
její	její	k3xOp3gInSc1	její
poloměr	poloměr	k1gInSc1	poloměr
se	se	k3xPyFc4	se
rovná	rovnat	k5eAaImIp3nS	rovnat
15	[number]	k4	15
až	až	k9	až
20	[number]	k4	20
procentům	procento	k1gNnPc3	procento
jeho	jeho	k3xOp3gInSc6	jeho
poloměru	poloměr	k1gInSc6	poloměr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
se	se	k3xPyFc4	se
její	její	k3xOp3gInSc4	její
poloměr	poloměr	k1gInSc4	poloměr
odhadoval	odhadovat	k5eAaImAgMnS	odhadovat
na	na	k7c4	na
0,20	[number]	k4	0,20
<g/>
±	±	k?	±
<g/>
0,008	[number]	k4	0,008
slunečního	sluneční	k2eAgInSc2d1	sluneční
poloměru	poloměr	k1gInSc2	poloměr
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
než	než	k8xS	než
v	v	k7c6	v
minulých	minulý	k2eAgInPc6d1	minulý
odhadech	odhad	k1gInPc6	odhad
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
její	její	k3xOp3gFnSc4	její
skutečnou	skutečný	k2eAgFnSc4d1	skutečná
velikost	velikost	k1gFnSc4	velikost
podcenily	podcenit	k5eAaPmAgInP	podcenit
<g/>
.	.	kIx.	.
</s>
<s>
Barnardova	Barnardův	k2eAgFnSc1d1	Barnardova
hvězda	hvězda	k1gFnSc1	hvězda
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
180	[number]	k4	180
<g/>
krát	krát	k6eAd1	krát
hmotnější	hmotný	k2eAgInPc1d2	hmotnější
než	než	k8xS	než
Jupiter	Jupiter	k1gInSc1	Jupiter
<g/>
,	,	kIx,	,
její	její	k3xOp3gInSc1	její
poloměr	poloměr	k1gInSc1	poloměr
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
větší	veliký	k2eAgFnSc4d2	veliký
pouze	pouze	k6eAd1	pouze
1,5	[number]	k4	1,5
až	až	k9	až
2,0	[number]	k4	2,0
<g/>
krát	krát	k6eAd1	krát
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
velikosti	velikost	k1gFnSc3	velikost
hnědého	hnědý	k2eAgMnSc2d1	hnědý
trpaslíka	trpaslík	k1gMnSc2	trpaslík
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
efektivní	efektivní	k2eAgFnSc1d1	efektivní
teplota	teplota	k1gFnSc1	teplota
je	být	k5eAaImIp3nS	být
3134	[number]	k4	3134
<g/>
±	±	k?	±
<g/>
102	[number]	k4	102
Kelvinů	kelvin	k1gInPc2	kelvin
<g/>
,	,	kIx,	,
vizuální	vizuální	k2eAgFnSc4d1	vizuální
světelnost	světelnost	k1gFnSc4	světelnost
jen	jen	k9	jen
4	[number]	k4	4
<g/>
/	/	kIx~	/
<g/>
10000	[number]	k4	10000
sluneční	sluneční	k2eAgFnSc2d1	sluneční
světelnosti	světelnost	k1gFnSc2	světelnost
a	a	k8xC	a
bolometrická	bolometrický	k2eAgFnSc1d1	bolometrická
světelnost	světelnost	k1gFnSc1	světelnost
34,6	[number]	k4	34,6
<g/>
/	/	kIx~	/
<g/>
10000	[number]	k4	10000
světelnosti	světelnost	k1gFnSc2	světelnost
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Barnardova	Barnardův	k2eAgFnSc1d1	Barnardova
hvězda	hvězda	k1gFnSc1	hvězda
má	mít	k5eAaImIp3nS	mít
tak	tak	k6eAd1	tak
malou	malý	k2eAgFnSc4d1	malá
jasnost	jasnost	k1gFnSc4	jasnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
kdyby	kdyby	kYmCp3nS	kdyby
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
jako	jako	k8xC	jako
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
by	by	kYmCp3nS	by
jen	jen	k9	jen
100	[number]	k4	100
<g/>
krát	krát	k6eAd1	krát
jasnější	jasný	k2eAgFnSc1d2	jasnější
než	než	k8xS	než
úplněk	úplněk	k1gInSc1	úplněk
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
srovnatelné	srovnatelný	k2eAgNnSc1d1	srovnatelné
s	s	k7c7	s
jasností	jasnost	k1gFnSc7	jasnost
Slunce	slunce	k1gNnSc2	slunce
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
80	[number]	k4	80
astronomických	astronomický	k2eAgFnPc2d1	astronomická
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
.	.	kIx.	.
<g/>
Barnardova	Barnardův	k2eAgFnSc1d1	Barnardova
hvězda	hvězda	k1gFnSc1	hvězda
má	mít	k5eAaImIp3nS	mít
metalicitu	metalicita	k1gFnSc4	metalicita
mezi	mezi	k7c7	mezi
-0,5	-0,5	k4	-0,5
a	a	k8xC	a
-1,0	-1,0	k4	-1,0
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
10	[number]	k4	10
až	až	k9	až
32	[number]	k4	32
procent	procento	k1gNnPc2	procento
hodnoty	hodnota	k1gFnSc2	hodnota
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Metalicita	Metalicita	k1gFnSc1	Metalicita
<g/>
,	,	kIx,	,
podíl	podíl	k1gInSc1	podíl
hmoty	hmota	k1gFnSc2	hmota
hvězdy	hvězda	k1gFnSc2	hvězda
z	z	k7c2	z
chemických	chemický	k2eAgInPc2d1	chemický
prvků	prvek	k1gInPc2	prvek
těžších	těžký	k2eAgNnPc2d2	těžší
než	než	k8xS	než
helium	helium	k1gNnSc4	helium
<g/>
,	,	kIx,	,
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
klasifikovat	klasifikovat	k5eAaImF	klasifikovat
hvězdy	hvězda	k1gFnPc4	hvězda
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
celé	celý	k2eAgFnSc3d1	celá
galaktické	galaktický	k2eAgFnSc3d1	Galaktická
populaci	populace	k1gFnSc3	populace
<g/>
.	.	kIx.	.
</s>
<s>
Barnardova	Barnardův	k2eAgFnSc1d1	Barnardova
hvězda	hvězda	k1gFnSc1	hvězda
se	se	k3xPyFc4	se
zdá	zdát	k5eAaPmIp3nS	zdát
být	být	k5eAaImF	být
typickou	typický	k2eAgFnSc7d1	typická
starou	starý	k2eAgFnSc7d1	stará
trpasličí	trpasličí	k2eAgFnSc7d1	trpasličí
červenou	červený	k2eAgFnSc7d1	červená
hvězdou	hvězda	k1gFnSc7	hvězda
populace	populace	k1gFnSc2	populace
II	II	kA	II
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
většina	většina	k1gFnSc1	většina
hvězd	hvězda	k1gFnPc2	hvězda
galaktického	galaktický	k2eAgNnSc2d1	Galaktické
hala	halo	k1gNnSc2	halo
chudých	chudý	k1gMnPc2	chudý
na	na	k7c4	na
kovy	kov	k1gInPc4	kov
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
metalicita	metalicita	k1gFnSc1	metalicita
je	být	k5eAaImIp3nS	být
nižší	nízký	k2eAgFnSc1d2	nižší
než	než	k8xS	než
Slunce	slunce	k1gNnSc1	slunce
<g/>
,	,	kIx,	,
poněkud	poněkud	k6eAd1	poněkud
vyšší	vysoký	k2eAgFnSc1d2	vyšší
než	než	k8xS	než
u	u	k7c2	u
hvězd	hvězda	k1gFnPc2	hvězda
z	z	k7c2	z
hala	halo	k1gNnSc2	halo
galaxie	galaxie	k1gFnSc2	galaxie
a	a	k8xC	a
nižší	nízký	k2eAgMnSc1d2	nižší
než	než	k8xS	než
u	u	k7c2	u
hvězd	hvězda	k1gFnPc2	hvězda
z	z	k7c2	z
disku	disk	k1gInSc2	disk
galaxie	galaxie	k1gFnSc2	galaxie
<g/>
,	,	kIx,	,
na	na	k7c4	na
kovy	kov	k1gInPc4	kov
bohatých	bohatý	k2eAgFnPc2d1	bohatá
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
rychlostí	rychlost	k1gFnSc7	rychlost
pohybu	pohyb	k1gInSc2	pohyb
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
jemnějšímu	jemný	k2eAgNnSc3d2	jemnější
zařazení	zařazení	k1gNnSc3	zařazení
mezi	mezi	k7c4	mezi
hvězdy	hvězda	k1gFnPc4	hvězda
přechodné	přechodný	k2eAgFnSc2d1	přechodná
populace	populace	k1gFnSc2	populace
II	II	kA	II
vyskytující	vyskytující	k2eAgFnSc2d1	vyskytující
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
halem	halo	k1gNnSc7	halo
a	a	k8xC	a
diskem	disk	k1gInSc7	disk
galaxie	galaxie	k1gFnSc2	galaxie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hledání	hledání	k1gNnSc1	hledání
planet	planeta	k1gFnPc2	planeta
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
deseti	deset	k4xCc2	deset
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1963	[number]	k4	1963
až	až	k8xS	až
1973	[number]	k4	1973
<g/>
,	,	kIx,	,
značný	značný	k2eAgInSc1d1	značný
počet	počet	k1gInSc1	počet
astronomů	astronom	k1gMnPc2	astronom
přijal	přijmout	k5eAaPmAgInS	přijmout
domněnku	domněnka	k1gFnSc4	domněnka
nizozemského	nizozemský	k2eAgMnSc2d1	nizozemský
astronoma	astronom	k1gMnSc2	astronom
Petera	Peter	k1gMnSc2	Peter
van	van	k1gInSc1	van
de	de	k?	de
Kampa	Kampa	k1gFnSc1	Kampa
<g/>
,	,	kIx,	,
že	že	k8xS	že
pomocí	pomocí	k7c2	pomocí
astrometrie	astrometrie	k1gFnSc2	astrometrie
objevil	objevit	k5eAaPmAgMnS	objevit
odchylky	odchylka	k1gFnPc4	odchylka
v	v	k7c6	v
pohybu	pohyb	k1gInSc6	pohyb
Barnardovy	Barnardův	k2eAgFnSc2d1	Barnardova
hvězdy	hvězda	k1gFnSc2	hvězda
a	a	k8xC	a
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
obíhá	obíhat	k5eAaImIp3nS	obíhat
jedna	jeden	k4xCgFnSc1	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
planet	planeta	k1gFnPc2	planeta
o	o	k7c6	o
hmotnosti	hmotnost	k1gFnSc6	hmotnost
Jupitera	Jupiter	k1gMnSc2	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Van	van	k1gInSc1	van
de	de	k?	de
Kamp	kamp	k1gInSc4	kamp
pozoroval	pozorovat	k5eAaImAgMnS	pozorovat
hvězdu	hvězda	k1gFnSc4	hvězda
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
a	a	k8xC	a
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
společně	společně	k6eAd1	společně
s	s	k7c7	s
kolegy	kolega	k1gMnPc7	kolega
na	na	k7c6	na
observatoři	observatoř	k1gFnSc6	observatoř
v	v	k7c6	v
Swarthmore	Swarthmor	k1gInSc5	Swarthmor
College	Colleg	k1gInPc1	Colleg
najít	najít	k5eAaPmF	najít
nepatrné	nepatrný	k2eAgFnPc4d1	nepatrná
odchylky	odchylka	k1gFnPc4	odchylka
jednoho	jeden	k4xCgInSc2	jeden
mikrometru	mikrometr	k1gInSc2	mikrometr
v	v	k7c6	v
pohybu	pohyb	k1gInSc6	pohyb
hvězdy	hvězda	k1gFnSc2	hvězda
na	na	k7c6	na
fotografické	fotografický	k2eAgFnSc6d1	fotografická
desce	deska	k1gFnSc6	deska
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
by	by	kYmCp3nP	by
naznačovaly	naznačovat	k5eAaImAgFnP	naznačovat
přítomnost	přítomnost	k1gFnSc4	přítomnost
planet	planeta	k1gFnPc2	planeta
u	u	k7c2	u
Barnardovy	Barnardův	k2eAgFnSc2d1	Barnardova
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Měření	měření	k1gNnSc4	měření
provádělo	provádět	k5eAaImAgNnS	provádět
deset	deset	k4xCc1	deset
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zabránilo	zabránit	k5eAaPmAgNnS	zabránit
chybám	chyba	k1gFnPc3	chyba
<g/>
.	.	kIx.	.
</s>
<s>
Van	van	k1gInSc1	van
de	de	k?	de
Kamp	kamp	k1gInSc1	kamp
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
publikoval	publikovat	k5eAaBmAgMnS	publikovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kolem	kolem	k7c2	kolem
Barnadovy	Barnadův	k2eAgFnSc2d1	Barnadův
hvězdy	hvězda	k1gFnSc2	hvězda
obíhá	obíhat	k5eAaImIp3nS	obíhat
planeta	planeta	k1gFnSc1	planeta
o	o	k7c6	o
hmotnosti	hmotnost	k1gFnSc6	hmotnost
1,6	[number]	k4	1,6
Jupitera	Jupiter	k1gMnSc2	Jupiter
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
4,4	[number]	k4	4,4
AU	au	k0	au
od	od	k7c2	od
hvězdy	hvězda	k1gFnSc2	hvězda
s	s	k7c7	s
oběžnou	oběžný	k2eAgFnSc7d1	oběžná
dobou	doba	k1gFnSc7	doba
24	[number]	k4	24
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
tato	tento	k3xDgNnPc1	tento
měření	měření	k1gNnPc1	měření
vyšla	vyjít	k5eAaPmAgNnP	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
v	v	k7c6	v
dalším	další	k2eAgInSc6d1	další
odborném	odborný	k2eAgInSc6d1	odborný
článku	článek	k1gInSc6	článek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
dále	daleko	k6eAd2	daleko
zveřejnil	zveřejnit	k5eAaPmAgMnS	zveřejnit
<g/>
,	,	kIx,	,
že	že	k8xS	že
kolem	kolem	k7c2	kolem
hvězdy	hvězda	k1gFnSc2	hvězda
obíhají	obíhat	k5eAaImIp3nP	obíhat
dvě	dva	k4xCgFnPc1	dva
planety	planeta	k1gFnPc1	planeta
o	o	k7c6	o
hmotnosti	hmotnost	k1gFnSc6	hmotnost
1,1	[number]	k4	1,1
a	a	k8xC	a
0,8	[number]	k4	0,8
Jupitera	Jupiter	k1gMnSc2	Jupiter
<g/>
.	.	kIx.	.
<g/>
Poté	poté	k6eAd1	poté
další	další	k2eAgMnPc1d1	další
astronomové	astronom	k1gMnPc1	astronom
zopakovali	zopakovat	k5eAaPmAgMnP	zopakovat
Van	van	k1gInSc4	van
de	de	k?	de
Kampova	Kampův	k2eAgNnSc2d1	Kampův
měření	měření	k1gNnSc2	měření
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1973	[number]	k4	1973
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
článcích	článek	k1gInPc6	článek
vyvrátili	vyvrátit	k5eAaPmAgMnP	vyvrátit
existenci	existence	k1gFnSc4	existence
planet	planeta	k1gFnPc2	planeta
u	u	k7c2	u
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
George	Georg	k1gMnSc2	Georg
Gatewood	Gatewood	k1gInSc1	Gatewood
a	a	k8xC	a
Heinrich	Heinrich	k1gMnSc1	Heinrich
Eichhorn	Eichhorn	k1gMnSc1	Eichhorn
pořídili	pořídit	k5eAaPmAgMnP	pořídit
fotografické	fotografický	k2eAgFnPc4d1	fotografická
desky	deska	k1gFnPc4	deska
pomocí	pomocí	k7c2	pomocí
novější	nový	k2eAgFnSc2d2	novější
měřící	měřící	k2eAgFnSc2d1	měřící
techniky	technika	k1gFnSc2	technika
a	a	k8xC	a
nepodařilo	podařit	k5eNaPmAgNnS	podařit
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
planety	planeta	k1gFnSc2	planeta
ověřit	ověřit	k5eAaPmF	ověřit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalším	další	k2eAgInSc6d1	další
článku	článek	k1gInSc6	článek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
zveřejnil	zveřejnit	k5eAaPmAgMnS	zveřejnit
John	John	k1gMnSc1	John
L.	L.	kA	L.
Hershey	Hershea	k1gFnPc1	Hershea
z	z	k7c2	z
observatoře	observatoř	k1gFnSc2	observatoř
v	v	k7c6	v
Swarthmore	Swarthmor	k1gInSc5	Swarthmor
o	o	k7c4	o
čtyři	čtyři	k4xCgInPc4	čtyři
měsíce	měsíc	k1gInPc4	měsíc
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c7	za
objevem	objev	k1gInSc7	objev
je	být	k5eAaImIp3nS	být
chyba	chyba	k1gFnSc1	chyba
dalekohledu	dalekohled	k1gInSc2	dalekohled
<g/>
,	,	kIx,	,
změna	změna	k1gFnSc1	změna
hliníkového	hliníkový	k2eAgNnSc2d1	hliníkové
uchycení	uchycení	k1gNnSc2	uchycení
čočky	čočka	k1gFnSc2	čočka
za	za	k7c4	za
kovové	kovový	k2eAgMnPc4d1	kovový
a	a	k8xC	a
změna	změna	k1gFnSc1	změna
fotografické	fotografický	k2eAgFnSc2d1	fotografická
emulze	emulze	k1gFnSc2	emulze
<g/>
.	.	kIx.	.
</s>
<s>
Záležitost	záležitost	k1gFnSc1	záležitost
byla	být	k5eAaImAgFnS	být
předána	předat	k5eAaPmNgFnS	předat
vědecké	vědecký	k2eAgFnSc3d1	vědecká
obci	obec	k1gFnSc3	obec
k	k	k7c3	k
přezkoumání	přezkoumání	k1gNnSc3	přezkoumání
<g/>
.	.	kIx.	.
<g/>
Van	van	k1gInSc1	van
de	de	k?	de
Kamp	kamp	k1gInSc1	kamp
nikdy	nikdy	k6eAd1	nikdy
neuznal	uznat	k5eNaPmAgInS	uznat
svůj	svůj	k3xOyFgInSc4	svůj
omyl	omyl	k1gInSc4	omyl
a	a	k8xC	a
publikoval	publikovat	k5eAaBmAgMnS	publikovat
další	další	k2eAgNnSc4d1	další
potvrzení	potvrzení	k1gNnSc4	potvrzení
existence	existence	k1gFnSc2	existence
planet	planeta	k1gFnPc2	planeta
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
</s>
<s>
Van	van	k1gInSc1	van
de	de	k?	de
Kamp	kamp	k1gInSc1	kamp
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
Wulff	Wulff	k1gMnSc1	Wulff
Heintz	Heintz	k1gMnSc1	Heintz
<g/>
,	,	kIx,	,
Van	van	k1gInSc4	van
de	de	k?	de
Kampův	Kampův	k2eAgMnSc1d1	Kampův
nástupce	nástupce	k1gMnSc1	nástupce
na	na	k7c6	na
observatoři	observatoř	k1gFnSc6	observatoř
Swarthmore	Swarthmor	k1gInSc5	Swarthmor
a	a	k8xC	a
odborník	odborník	k1gMnSc1	odborník
na	na	k7c4	na
dvojhvězdy	dvojhvězda	k1gFnPc4	dvojhvězda
<g/>
,	,	kIx,	,
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
jeho	jeho	k3xOp3gFnSc4	jeho
práci	práce	k1gFnSc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
muži	muž	k1gMnPc1	muž
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
navzájem	navzájem	k6eAd1	navzájem
velmi	velmi	k6eAd1	velmi
odcizili	odcizit	k5eAaPmAgMnP	odcizit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Současný	současný	k2eAgInSc1d1	současný
pohled	pohled	k1gInSc1	pohled
===	===	k?	===
</s>
</p>
<p>
<s>
U	u	k7c2	u
hvězdy	hvězda	k1gFnSc2	hvězda
nelze	lze	k6eNd1	lze
zcela	zcela	k6eAd1	zcela
vyloučit	vyloučit	k5eAaPmF	vyloučit
planety	planeta	k1gFnPc4	planeta
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
jejich	jejich	k3xOp3gNnSc4	jejich
hledání	hledání	k1gNnSc4	hledání
v	v	k7c6	v
osmdesátých	osmdesátý	k4xOgNnPc6	osmdesátý
a	a	k8xC	a
devadesátých	devadesátý	k4xOgNnPc6	devadesátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
ani	ani	k8xC	ani
interferometrická	interferometrický	k2eAgNnPc1d1	interferometrické
měření	měření	k1gNnPc1	měření
hvězdy	hvězda	k1gFnSc2	hvězda
pomocí	pomocí	k7c2	pomocí
Hubblova	Hubblův	k2eAgInSc2d1	Hubblův
dalekohledu	dalekohled	k1gInSc2	dalekohled
nepřinesla	přinést	k5eNaPmAgFnS	přinést
žádné	žádný	k3yNgInPc4	žádný
výsledky	výsledek	k1gInPc4	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
Zpřesněná	zpřesněný	k2eAgNnPc1d1	zpřesněné
data	datum	k1gNnPc1	datum
získaná	získaný	k2eAgFnSc1d1	získaná
měřením	měření	k1gNnSc7	měření
pohybu	pohyb	k1gInSc2	pohyb
hvězdy	hvězda	k1gFnSc2	hvězda
a	a	k8xC	a
zpřesněné	zpřesněný	k2eAgInPc1d1	zpřesněný
údaje	údaj	k1gInPc1	údaj
její	její	k3xOp3gFnSc2	její
hmotnosti	hmotnost	k1gFnSc2	hmotnost
vyloučily	vyloučit	k5eAaPmAgInP	vyloučit
typy	typ	k1gInPc1	typ
planet	planeta	k1gFnPc2	planeta
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
u	u	k7c2	u
hvězdy	hvězda	k1gFnSc2	hvězda
nemohou	moct	k5eNaImIp3nP	moct
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
červených	červený	k2eAgMnPc2d1	červený
trpaslíků	trpaslík	k1gMnPc2	trpaslík
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
Barnardova	Barnardův	k2eAgFnSc1d1	Barnardova
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
poruchy	poruch	k1gInPc1	poruch
pohybu	pohyb	k1gInSc2	pohyb
snadněji	snadno	k6eAd2	snadno
studují	studovat	k5eAaImIp3nP	studovat
než	než	k8xS	než
u	u	k7c2	u
větších	veliký	k2eAgFnPc2d2	veliký
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
díky	díky	k7c3	díky
jejich	jejich	k3xOp3gNnSc3	jejich
nižší	nízký	k2eAgFnPc1d2	nižší
hmotnosti	hmotnost	k1gFnPc1	hmotnost
jsou	být	k5eAaImIp3nP	být
poruchy	porucha	k1gFnPc4	porucha
pohybu	pohyb	k1gInSc2	pohyb
hvězdy	hvězda	k1gFnSc2	hvězda
výraznější	výrazný	k2eAgFnSc1d2	výraznější
<g/>
.	.	kIx.	.
</s>
<s>
Gatewood	Gatewood	k1gInSc1	Gatewood
tak	tak	k9	tak
byl	být	k5eAaImAgInS	být
schopen	schopen	k2eAgInSc1d1	schopen
v	v	k7c6	v
článku	článek	k1gInSc6	článek
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
prokázat	prokázat	k5eAaPmF	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
u	u	k7c2	u
Barnardovy	Barnardův	k2eAgFnSc2d1	Barnardova
hvězdy	hvězda	k1gFnSc2	hvězda
nemohou	moct	k5eNaImIp3nP	moct
být	být	k5eAaImF	být
planety	planeta	k1gFnPc4	planeta
desetkrát	desetkrát	k6eAd1	desetkrát
hmotnější	hmotný	k2eAgMnSc1d2	hmotnější
než	než	k8xS	než
Jupiter	Jupiter	k1gMnSc1	Jupiter
(	(	kIx(	(
<g/>
dolní	dolní	k2eAgFnSc1d1	dolní
hranice	hranice	k1gFnSc1	hranice
hmotnosti	hmotnost	k1gFnSc2	hmotnost
hnědých	hnědý	k2eAgMnPc2d1	hnědý
trpaslíků	trpaslík	k1gMnPc2	trpaslík
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
měření	měření	k1gNnSc2	měření
Hubblova	Hubblův	k2eAgInSc2d1	Hubblův
dalekohledu	dalekohled	k1gInSc2	dalekohled
vyloučila	vyloučit	k5eAaPmAgFnS	vyloučit
planety	planeta	k1gFnPc4	planeta
0,8	[number]	k4	0,8
<g/>
krát	krát	k6eAd1	krát
hmotnější	hmotný	k2eAgNnSc1d2	hmotnější
než	než	k8xS	než
Jupiter	Jupiter	k1gMnSc1	Jupiter
s	s	k7c7	s
oběžnou	oběžný	k2eAgFnSc7d1	oběžná
dobou	doba	k1gFnSc7	doba
kratší	krátký	k2eAgFnSc7d2	kratší
než	než	k8xS	než
1000	[number]	k4	1000
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
Kuerster	Kuerster	k1gMnSc1	Kuerster
zjistil	zjistit	k5eAaPmAgMnS	zjistit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
obyvatelné	obyvatelný	k2eAgFnSc6d1	obyvatelná
zóně	zóna	k1gFnSc6	zóna
kolem	kolem	k7c2	kolem
Barnardovy	Barnardův	k2eAgFnSc2d1	Barnardova
hvězdy	hvězda	k1gFnSc2	hvězda
nejsou	být	k5eNaImIp3nP	být
možné	možný	k2eAgFnPc1d1	možná
planety	planeta	k1gFnPc1	planeta
7,5	[number]	k4	7,5
<g/>
krát	krát	k6eAd1	krát
hmotnější	hmotný	k2eAgFnSc1d2	hmotnější
než	než	k8xS	než
Země	země	k1gFnSc1	země
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
s	s	k7c7	s
hmotností	hmotnost	k1gFnSc7	hmotnost
3,1	[number]	k4	3,1
<g/>
krát	krát	k6eAd1	krát
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
Neptun	Neptun	k1gInSc1	Neptun
<g/>
.	.	kIx.	.
<g/>
I	i	k9	i
když	když	k8xS	když
tento	tento	k3xDgInSc1	tento
výzkum	výzkum	k1gInSc1	výzkum
výrazně	výrazně	k6eAd1	výrazně
omezil	omezit	k5eAaPmAgInS	omezit
možné	možný	k2eAgFnPc4d1	možná
hmotnosti	hmotnost	k1gFnPc4	hmotnost
planet	planeta	k1gFnPc2	planeta
u	u	k7c2	u
Barnardovy	Barnardův	k2eAgFnSc2d1	Barnardova
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
úplně	úplně	k6eAd1	úplně
vyloučena	vyloučen	k2eAgFnSc1d1	vyloučena
existence	existence	k1gFnSc1	existence
terestrické	terestrický	k2eAgFnSc2d1	terestrická
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bude	být	k5eAaImBp3nS	být
velmi	velmi	k6eAd1	velmi
obtížné	obtížný	k2eAgNnSc1d1	obtížné
ji	on	k3xPp3gFnSc4	on
objevit	objevit	k5eAaPmF	objevit
<g/>
.	.	kIx.	.
</s>
<s>
NASA	NASA	kA	NASA
Space	Space	k1gFnSc1	Space
Interferometry	interferometr	k1gInPc1	interferometr
Mission	Mission	k1gInSc1	Mission
měla	mít	k5eAaImAgNnP	mít
naplánováno	naplánován	k2eAgNnSc1d1	naplánováno
začít	začít	k5eAaPmF	začít
s	s	k7c7	s
hledáním	hledání	k1gNnSc7	hledání
exoplanet	exoplaneta	k1gFnPc2	exoplaneta
pomocí	pomocí	k7c2	pomocí
interferometrie	interferometrie	k1gFnSc2	interferometrie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
byl	být	k5eAaImAgInS	být
zrušen	zrušit	k5eAaPmNgInS	zrušit
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
obdobná	obdobný	k2eAgFnSc1d1	obdobná
mise	mise	k1gFnSc1	mise
ESA	eso	k1gNnSc2	eso
Darwin	Darwin	k1gMnSc1	Darwin
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Projekt	projekt	k1gInSc1	projekt
Daedalus	Daedalus	k1gInSc1	Daedalus
===	===	k?	===
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
diskuze	diskuze	k1gFnSc2	diskuze
o	o	k7c6	o
planetách	planeta	k1gFnPc6	planeta
se	se	k3xPyFc4	se
Barnardova	Barnardův	k2eAgFnSc1d1	Barnardova
hvězda	hvězda	k1gFnSc1	hvězda
stala	stát	k5eAaPmAgFnS	stát
cílem	cíl	k1gInSc7	cíl
projektu	projekt	k1gInSc2	projekt
Daedalus	Daedalus	k1gInSc1	Daedalus
<g/>
.	.	kIx.	.
</s>
<s>
Studie	studie	k1gFnSc1	studie
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1973	[number]	k4	1973
až	až	k9	až
1978	[number]	k4	1978
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
navrhnout	navrhnout	k5eAaPmF	navrhnout
rychlou	rychlý	k2eAgFnSc4d1	rychlá
bezpilotní	bezpilotní	k2eAgFnSc4d1	bezpilotní
sondu	sonda	k1gFnSc4	sonda
<g/>
,	,	kIx,	,
používající	používající	k2eAgFnPc1d1	používající
současné	současný	k2eAgFnPc1d1	současná
nebo	nebo	k8xC	nebo
v	v	k7c6	v
blízké	blízký	k2eAgFnSc6d1	blízká
budoucnosti	budoucnost	k1gFnSc6	budoucnost
očekávané	očekávaný	k2eAgFnSc2d1	očekávaná
techniky	technika	k1gFnSc2	technika
<g/>
.	.	kIx.	.
</s>
<s>
Hvězda	hvězda	k1gFnSc1	hvězda
byla	být	k5eAaImAgFnS	být
vybrána	vybrat	k5eAaPmNgFnS	vybrat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
v	v	k7c6	v
době	doba	k1gFnSc6	doba
studie	studie	k1gFnSc2	studie
věřilo	věřit	k5eAaImAgNnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
kolem	kolem	k7c2	kolem
ní	on	k3xPp3gFnSc2	on
obíhají	obíhat	k5eAaImIp3nP	obíhat
planety	planeta	k1gFnPc1	planeta
<g/>
.	.	kIx.	.
<g/>
Byla	být	k5eAaImAgFnS	být
navržena	navržen	k2eAgFnSc1d1	navržena
kosmická	kosmický	k2eAgFnSc1d1	kosmická
loď	loď	k1gFnSc1	loď
s	s	k7c7	s
pulsním	pulsní	k2eAgInSc7d1	pulsní
termojaderným	termojaderný	k2eAgInSc7d1	termojaderný
pohonem	pohon	k1gInSc7	pohon
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c4	na
stlačení	stlačení	k1gNnSc4	stlačení
a	a	k8xC	a
ohřátí	ohřátý	k2eAgMnPc1d1	ohřátý
mikrokapslí	mikrokapslý	k2eAgMnPc1d1	mikrokapslý
složených	složený	k2eAgInPc2d1	složený
ze	z	k7c2	z
směsi	směs	k1gFnSc2	směs
deuteria	deuterium	k1gNnSc2	deuterium
a	a	k8xC	a
3	[number]	k4	3
<g/>
He	he	k0	he
pomocí	pomoc	k1gFnPc2	pomoc
svazků	svazek	k1gInPc2	svazek
elektronů	elektron	k1gInPc2	elektron
<g/>
.	.	kIx.	.
</s>
<s>
Měly	mít	k5eAaImAgFnP	mít
tak	tak	k6eAd1	tak
vzniknout	vzniknout	k5eAaPmF	vzniknout
mikrojaderné	mikrojaderný	k2eAgFnPc1d1	mikrojaderný
exploze	exploze	k1gFnPc1	exploze
a	a	k8xC	a
loď	loď	k1gFnSc1	loď
měla	mít	k5eAaImAgFnS	mít
za	za	k7c4	za
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
12	[number]	k4	12
procent	procento	k1gNnPc2	procento
rychlosti	rychlost	k1gFnSc2	rychlost
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdy	hvězda	k1gFnPc4	hvězda
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
za	za	k7c4	za
50	[number]	k4	50
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
za	za	k7c4	za
dobu	doba	k1gFnSc4	doba
jednoho	jeden	k4xCgInSc2	jeden
lidského	lidský	k2eAgInSc2d1	lidský
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
detailním	detailní	k2eAgNnSc7d1	detailní
studiem	studio	k1gNnSc7	studio
hvězdy	hvězda	k1gFnSc2	hvězda
a	a	k8xC	a
planet	planeta	k1gFnPc2	planeta
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
zkoumáno	zkoumán	k2eAgNnSc4d1	zkoumáno
mezihvězdné	mezihvězdný	k2eAgNnSc4d1	mezihvězdné
prostředí	prostředí	k1gNnSc4	prostředí
a	a	k8xC	a
měla	mít	k5eAaImAgNnP	mít
být	být	k5eAaImF	být
prováděna	prováděn	k2eAgNnPc1d1	prováděno
astrometrická	astrometrický	k2eAgNnPc1d1	astrometrický
měření	měření	k1gNnPc1	měření
<g/>
.	.	kIx.	.
<g/>
Počáteční	počáteční	k2eAgFnSc1d1	počáteční
model	model	k1gInSc4	model
projektu	projekt	k1gInSc2	projekt
Daedalus	Daedalus	k1gInSc1	Daedalus
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
další	další	k2eAgInSc4d1	další
teoretický	teoretický	k2eAgInSc4d1	teoretický
výzkum	výzkum	k1gInSc4	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
Robert	Robert	k1gMnSc1	Robert
Freitas	Freitas	k1gMnSc1	Freitas
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
ambicióznější	ambiciózní	k2eAgInSc4d2	ambicióznější
plán	plán	k1gInSc4	plán
<g/>
:	:	kIx,	:
samoreplikující	samoreplikující	k2eAgFnPc1d1	samoreplikující
kosmické	kosmický	k2eAgFnPc1d1	kosmická
lodě	loď	k1gFnPc1	loď
určené	určený	k2eAgFnPc1d1	určená
pro	pro	k7c4	pro
vyhledávání	vyhledávání	k1gNnSc4	vyhledávání
a	a	k8xC	a
navázání	navázání	k1gNnSc4	navázání
kontaktu	kontakt	k1gInSc2	kontakt
s	s	k7c7	s
mimozemským	mimozemský	k2eAgInSc7d1	mimozemský
životem	život	k1gInSc7	život
<g/>
.	.	kIx.	.
</s>
<s>
Loď	loď	k1gFnSc1	loď
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
postavila	postavit	k5eAaPmAgFnS	postavit
na	na	k7c6	na
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
kolem	kolem	k7c2	kolem
Jupitera	Jupiter	k1gMnSc2	Jupiter
<g/>
,	,	kIx,	,
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
by	by	kYmCp3nP	by
Barnardovy	Barnardův	k2eAgFnPc4d1	Barnardova
hvězdy	hvězda	k1gFnPc4	hvězda
za	za	k7c4	za
47	[number]	k4	47
let	léto	k1gNnPc2	léto
podle	podle	k7c2	podle
parametrů	parametr	k1gInPc2	parametr
původního	původní	k2eAgInSc2d1	původní
projektu	projekt	k1gInSc2	projekt
Daedalus	Daedalus	k1gMnSc1	Daedalus
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
by	by	kYmCp3nS	by
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
hvězdy	hvězda	k1gFnPc4	hvězda
<g/>
,	,	kIx,	,
spustila	spustit	k5eAaPmAgFnS	spustit
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
automatická	automatický	k2eAgFnSc1d1	automatická
samoreplikace	samoreplikace	k1gFnSc1	samoreplikace
<g/>
,	,	kIx,	,
postavila	postavit	k5eAaPmAgFnS	postavit
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
továrna	továrna	k1gFnSc1	továrna
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
vyráběla	vyrábět	k5eAaImAgFnS	vyrábět
kopie	kopie	k1gFnSc1	kopie
původní	původní	k2eAgFnSc2d1	původní
kosmické	kosmický	k2eAgFnSc2d1	kosmická
lodě	loď	k1gFnSc2	loď
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Erupce	erupce	k1gFnPc1	erupce
==	==	k?	==
</s>
</p>
<p>
<s>
Pozorování	pozorování	k1gNnSc1	pozorování
erupce	erupce	k1gFnSc2	erupce
u	u	k7c2	u
Barnardovy	Barnardův	k2eAgFnSc2d1	Barnardova
hvězdy	hvězda	k1gFnSc2	hvězda
vzbudila	vzbudit	k5eAaPmAgFnS	vzbudit
další	další	k2eAgFnSc4d1	další
vlnu	vlna	k1gFnSc4	vlna
zájmu	zájem	k1gInSc2	zájem
o	o	k7c4	o
pozorování	pozorování	k1gNnPc4	pozorování
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Erupci	erupce	k1gFnSc4	erupce
zjistil	zjistit	k5eAaPmAgMnS	zjistit
při	při	k7c6	při
studiu	studio	k1gNnSc6	studio
poruch	poruch	k1gInSc4	poruch
pohybu	pohyb	k1gInSc2	pohyb
hvězdy	hvězda	k1gFnSc2	hvězda
William	William	k1gInSc1	William
Cochran	Cochrana	k1gFnPc2	Cochrana
z	z	k7c2	z
Texaské	texaský	k2eAgFnSc2d1	texaská
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Austinu	Austin	k1gInSc6	Austin
na	na	k7c6	na
základě	základ	k1gInSc6	základ
změn	změna	k1gFnPc2	změna
ve	v	k7c6	v
spektrální	spektrální	k2eAgFnSc6d1	spektrální
emisi	emise	k1gFnSc6	emise
ze	z	k7c2	z
dne	den	k1gInSc2	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
před	před	k7c7	před
úplnou	úplný	k2eAgFnSc7d1	úplná
analýzou	analýza	k1gFnSc7	analýza
erupce	erupce	k1gFnSc2	erupce
<g/>
.	.	kIx.	.
</s>
<s>
Diane	Dianout	k5eAaPmIp3nS	Dianout
Paulsonová	Paulsonový	k2eAgNnPc1d1	Paulsonový
z	z	k7c2	z
Goddardova	Goddardův	k2eAgNnSc2d1	Goddardovo
kosmického	kosmický	k2eAgNnSc2d1	kosmické
střediska	středisko	k1gNnSc2	středisko
zjistila	zjistit	k5eAaPmAgFnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
teplota	teplota	k1gFnSc1	teplota
erupce	erupce	k1gFnSc1	erupce
byla	být	k5eAaImAgFnS	být
8000	[number]	k4	8000
K	K	kA	K
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvojnásobek	dvojnásobek	k1gInSc1	dvojnásobek
normální	normální	k2eAgFnSc2d1	normální
teploty	teplota	k1gFnSc2	teplota
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
ze	z	k7c2	z
spektra	spektrum	k1gNnSc2	spektrum
nelze	lze	k6eNd1	lze
přesně	přesně	k6eAd1	přesně
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
,	,	kIx,	,
jakou	jaký	k3yRgFnSc7	jaký
měla	mít	k5eAaImAgFnS	mít
erupce	erupce	k1gFnPc4	erupce
energii	energie	k1gFnSc3	energie
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
náhodnému	náhodný	k2eAgInSc3d1	náhodný
charakteru	charakter	k1gInSc2	charakter
erupcí	erupce	k1gFnPc2	erupce
je	být	k5eAaImIp3nS	být
hvězda	hvězda	k1gFnSc1	hvězda
cílem	cíl	k1gInSc7	cíl
pozorování	pozorování	k1gNnSc2	pozorování
amatérských	amatérský	k2eAgMnPc2d1	amatérský
astronomů	astronom	k1gMnPc2	astronom
<g/>
.	.	kIx.	.
<g/>
Erupce	erupce	k1gFnSc1	erupce
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
astronomy	astronom	k1gMnPc4	astronom
překvapením	překvapení	k1gNnSc7	překvapení
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
intenzivní	intenzivní	k2eAgFnSc1d1	intenzivní
hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
aktivita	aktivita	k1gFnSc1	aktivita
u	u	k7c2	u
červeného	červený	k2eAgMnSc2d1	červený
trpaslíka	trpaslík	k1gMnSc2	trpaslík
jejího	její	k3xOp3gInSc2	její
věku	věk	k1gInSc2	věk
se	se	k3xPyFc4	se
neočekávala	očekávat	k5eNaImAgFnS	očekávat
<g/>
.	.	kIx.	.
</s>
<s>
Erupce	erupce	k1gFnPc1	erupce
nejsou	být	k5eNaImIp3nP	být
zcela	zcela	k6eAd1	zcela
objasněny	objasněn	k2eAgInPc1d1	objasněn
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
jsou	být	k5eAaImIp3nP	být
způsobeny	způsoben	k2eAgInPc1d1	způsoben
silnými	silný	k2eAgFnPc7d1	silná
magnetickými	magnetický	k2eAgNnPc7d1	magnetické
poli	pole	k1gNnPc7	pole
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
potlačují	potlačovat	k5eAaImIp3nP	potlačovat
plazmovou	plazmový	k2eAgFnSc4d1	plazmová
konvekci	konvekce	k1gFnSc4	konvekce
a	a	k8xC	a
vedou	vést	k5eAaImIp3nP	vést
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
náhlému	náhlý	k2eAgNnSc3d1	náhlé
vzplanutí	vzplanutí	k1gNnSc3	vzplanutí
<g/>
:	:	kIx,	:
silná	silný	k2eAgNnPc1d1	silné
magnetická	magnetický	k2eAgNnPc1d1	magnetické
pole	pole	k1gNnPc1	pole
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
u	u	k7c2	u
rychle	rychle	k6eAd1	rychle
rotujících	rotující	k2eAgFnPc2d1	rotující
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
staré	starý	k2eAgFnPc1d1	stará
hvězdy	hvězda	k1gFnPc1	hvězda
se	se	k3xPyFc4	se
většinou	většina	k1gFnSc7	většina
otáčejí	otáčet	k5eAaImIp3nP	otáčet
pomalu	pomalu	k6eAd1	pomalu
<g/>
.	.	kIx.	.
</s>
<s>
Událost	událost	k1gFnSc1	událost
takového	takový	k3xDgInSc2	takový
rozsahu	rozsah	k1gInSc2	rozsah
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
Barnardovy	Barnardův	k2eAgFnSc2d1	Barnardova
hvězdy	hvězda	k1gFnSc2	hvězda
výjimečná	výjimečný	k2eAgFnSc1d1	výjimečná
<g/>
.	.	kIx.	.
</s>
<s>
Výzkum	výzkum	k1gInSc1	výzkum
hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
periodicity	periodicita	k1gFnSc2	periodicita
nebo	nebo	k8xC	nebo
změny	změna	k1gFnSc2	změna
hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
aktivity	aktivita	k1gFnSc2	aktivita
v	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
časovém	časový	k2eAgInSc6d1	časový
horizontu	horizont	k1gInSc6	horizont
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Barnardova	Barnardův	k2eAgFnSc1d1	Barnardova
hvězda	hvězda	k1gFnSc1	hvězda
je	být	k5eAaImIp3nS	být
po	po	k7c4	po
většinu	většina	k1gFnSc4	většina
doby	doba	k1gFnSc2	doba
klidná	klidný	k2eAgFnSc1d1	klidná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
výzkum	výzkum	k1gInSc1	výzkum
ukázal	ukázat	k5eAaPmAgInS	ukázat
slabé	slabý	k2eAgInPc4d1	slabý
důkazy	důkaz	k1gInPc4	důkaz
periodických	periodický	k2eAgFnPc2d1	periodická
variací	variace	k1gFnPc2	variace
v	v	k7c6	v
jasu	jas	k1gInSc6	jas
Barnardovy	Barnardův	k2eAgFnSc2d1	Barnardova
hvězdy	hvězda	k1gFnSc2	hvězda
během	během	k7c2	během
jedné	jeden	k4xCgFnSc2	jeden
otočky	otočka	k1gFnSc2	otočka
hvězdy	hvězda	k1gFnSc2	hvězda
za	za	k7c4	za
130	[number]	k4	130
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
<g/>
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
činnost	činnost	k1gFnSc1	činnost
Barnardovy	Barnardův	k2eAgFnSc2d1	Barnardova
hvězdy	hvězda	k1gFnSc2	hvězda
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
pochopení	pochopení	k1gNnSc4	pochopení
podobných	podobný	k2eAgFnPc2d1	podobná
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Fotometrické	fotometrický	k2eAgFnPc1d1	fotometrická
studie	studie	k1gFnPc1	studie
z	z	k7c2	z
jejího	její	k3xOp3gNnSc2	její
rentgenového	rentgenový	k2eAgNnSc2d1	rentgenové
a	a	k8xC	a
ultrafialového	ultrafialový	k2eAgNnSc2d1	ultrafialové
záření	záření	k1gNnSc2	záření
vrhly	vrhnout	k5eAaImAgInP	vrhnout
nové	nový	k2eAgInPc1d1	nový
světlo	světlo	k1gNnSc4	světlo
na	na	k7c6	na
velké	velká	k1gFnSc6	velká
populace	populace	k1gFnSc2	populace
starých	starý	k2eAgMnPc2d1	starý
červených	červený	k2eAgMnPc2d1	červený
trpaslíků	trpaslík	k1gMnPc2	trpaslík
v	v	k7c6	v
galaxii	galaxie	k1gFnSc6	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Takový	takový	k3xDgInSc1	takový
výzkum	výzkum	k1gInSc1	výzkum
má	mít	k5eAaImIp3nS	mít
astrobiologické	astrobiologický	k2eAgInPc4d1	astrobiologický
důsledky	důsledek	k1gInPc4	důsledek
<g/>
:	:	kIx,	:
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
obyvatelné	obyvatelný	k2eAgFnSc2d1	obyvatelná
zóny	zóna	k1gFnSc2	zóna
červených	červený	k2eAgMnPc2d1	červený
trpaslíků	trpaslík	k1gMnPc2	trpaslík
jsou	být	k5eAaImIp3nP	být
blízko	blízko	k7c2	blízko
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
,	,	kIx,	,
planety	planeta	k1gFnSc2	planeta
budou	být	k5eAaImBp3nP	být
ovlivněny	ovlivnit	k5eAaPmNgInP	ovlivnit
slunečními	sluneční	k2eAgFnPc7d1	sluneční
erupcemi	erupce	k1gFnPc7	erupce
<g/>
,	,	kIx,	,
větry	vítr	k1gInPc7	vítr
a	a	k8xC	a
protuberancemi	protuberance	k1gFnPc7	protuberance
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sousedství	sousedství	k1gNnSc3	sousedství
Barnardovy	Barnardův	k2eAgFnSc2d1	Barnardova
hvězdy	hvězda	k1gFnSc2	hvězda
==	==	k?	==
</s>
</p>
<p>
<s>
Barnardovu	Barnardův	k2eAgFnSc4d1	Barnardova
hvězdu	hvězda	k1gFnSc4	hvězda
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
vidět	vidět	k5eAaImF	vidět
v	v	k7c6	v
nadhlavníku	nadhlavník	k1gInSc6	nadhlavník
severně	severně	k6eAd1	severně
od	od	k7c2	od
rovníku	rovník	k1gInSc2	rovník
<g/>
,	,	kIx,	,
na	na	k7c4	na
4	[number]	k4	4
<g/>
.	.	kIx.	.
stupni	stupeň	k1gInPc7	stupeň
severní	severní	k2eAgFnSc2d1	severní
zeměpisné	zeměpisný	k2eAgFnSc2d1	zeměpisná
šířky	šířka	k1gFnSc2	šířka
<g/>
.	.	kIx.	.
</s>
<s>
Hvězda	hvězda	k1gFnSc1	hvězda
je	být	k5eAaImIp3nS	být
viditelná	viditelný	k2eAgFnSc1d1	viditelná
na	na	k7c6	na
většině	většina	k1gFnSc6	většina
zeměpisných	zeměpisný	k2eAgFnPc2d1	zeměpisná
šířek	šířka	k1gFnPc2	šířka
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
míst	místo	k1gNnPc2	místo
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
pólů	pól	k1gInPc2	pól
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
atmosférická	atmosférický	k2eAgFnSc1d1	atmosférická
extinkce	extinkce	k1gFnSc1	extinkce
snižuje	snižovat	k5eAaImIp3nS	snižovat
viditelnost	viditelnost	k1gFnSc4	viditelnost
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Barnardova	Barnardův	k2eAgFnSc1d1	Barnardova
hvězda	hvězda	k1gFnSc1	hvězda
má	mít	k5eAaImIp3nS	mít
stejné	stejný	k2eAgMnPc4d1	stejný
sousedy	soused	k1gMnPc4	soused
jako	jako	k8xS	jako
Slunce	slunce	k1gNnSc4	slunce
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
většinou	většinou	k6eAd1	většinou
červení	červenit	k5eAaImIp3nP	červenit
trpaslíci	trpaslík	k1gMnPc1	trpaslík
<g/>
,	,	kIx,	,
nejmenší	malý	k2eAgMnSc1d3	nejmenší
a	a	k8xC	a
nejběžnější	běžný	k2eAgInSc1d3	nejběžnější
typ	typ	k1gInSc1	typ
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnSc1	její
nejbližší	blízký	k2eAgMnSc1d3	nejbližší
soused	soused	k1gMnSc1	soused
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
červený	červený	k2eAgMnSc1d1	červený
trpaslík	trpaslík	k1gMnSc1	trpaslík
Ross	Ross	k1gInSc4	Ross
154	[number]	k4	154
<g/>
,	,	kIx,	,
vzdálený	vzdálený	k2eAgInSc1d1	vzdálený
od	od	k7c2	od
ní	on	k3xPp3gFnSc2	on
5,41	[number]	k4	5,41
světelných	světelný	k2eAgNnPc2d1	světelné
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Slunce	slunce	k1gNnSc1	slunce
a	a	k8xC	a
Alfa	alfa	k1gFnSc1	alfa
Centauri	Centaur	k1gFnSc2	Centaur
jsou	být	k5eAaImIp3nP	být
její	její	k3xOp3gMnPc1	její
další	další	k2eAgMnPc1d1	další
nejbližší	blízký	k2eAgMnPc1d3	nejbližší
sousedé	soused	k1gMnPc1	soused
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Barnardovy	Barnardův	k2eAgFnSc2d1	Barnardova
hvězdy	hvězda	k1gFnSc2	hvězda
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
vidět	vidět	k5eAaImF	vidět
Slunce	slunce	k1gNnSc1	slunce
na	na	k7c6	na
opačné	opačný	k2eAgFnSc6d1	opačná
straně	strana	k1gFnSc6	strana
v	v	k7c6	v
souřadnicích	souřadnice	k1gFnPc6	souřadnice
RA	ra	k0	ra
=	=	kIx~	=
5	[number]	k4	5
<g/>
h	h	k?	h
57	[number]	k4	57
<g/>
m	m	kA	m
48,5	[number]	k4	48,5
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
Dec	Dec	k1gFnSc1	Dec
=	=	kIx~	=
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
°	°	k?	°
<g/>
41	[number]	k4	41
<g/>
'	'	kIx"	'
<g/>
36	[number]	k4	36
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
souhvězdí	souhvězdí	k1gNnSc2	souhvězdí
Jednorožce	jednorožec	k1gMnSc4	jednorožec
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
se	s	k7c7	s
zdánlivou	zdánlivý	k2eAgFnSc7d1	zdánlivá
hvězdnou	hvězdný	k2eAgFnSc7d1	hvězdná
velikostí	velikost	k1gFnSc7	velikost
1,15	[number]	k4	1,15
mag	mag	k?	mag
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Barnard	Barnarda	k1gFnPc2	Barnarda
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Star	Star	kA	Star
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
hvězd	hvězda	k1gFnPc2	hvězda
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
nejbližších	blízký	k2eAgFnPc2d3	nejbližší
hvězd	hvězda	k1gFnPc2	hvězda
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Barnardova	Barnardův	k2eAgFnSc1d1	Barnardova
šipka	šipka	k1gFnSc1	šipka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Článek	článek	k1gInSc1	článek
o	o	k7c6	o
Barnardově	Barnardův	k2eAgFnSc6d1	Barnardova
šipce	šipka	k1gFnSc6	šipka
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Tabulka	tabulka	k1gFnSc1	tabulka
o	o	k7c6	o
Barnardově	Barnardův	k2eAgFnSc6d1	Barnardova
šipce	šipka	k1gFnSc6	šipka
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Exoplanety	Exoplanet	k1gInPc1	Exoplanet
III	III	kA	III
<g/>
.	.	kIx.	.
–	–	k?	–
nesplněný	splněný	k2eNgInSc1d1	nesplněný
sen	sen	k1gInSc1	sen
Petera	Petera	k1gMnSc1	Petera
van	van	k1gInSc1	van
de	de	k?	de
Kampa	Kampa	k1gFnSc1	Kampa
</s>
</p>
