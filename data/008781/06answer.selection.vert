<s>
Barnardova	Barnardův	k2eAgFnSc1d1	Barnardova
šipka	šipka	k1gFnSc1	šipka
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
Barnardova	Barnardův	k2eAgFnSc1d1	Barnardova
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hvězda	hvězda	k1gFnSc1	hvězda
v	v	k7c6	v
souhvězdí	souhvězdí	k1gNnSc6	souhvězdí
Hadonoše	hadonoš	k1gMnSc2	hadonoš
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
hvězd	hvězda	k1gFnPc2	hvězda
nejrychlejší	rychlý	k2eAgInSc1d3	nejrychlejší
vlastní	vlastní	k2eAgInSc1d1	vlastní
pohyb	pohyb	k1gInSc1	pohyb
po	po	k7c6	po
obloze	obloha	k1gFnSc6	obloha
<g/>
:	:	kIx,	:
činí	činit	k5eAaImIp3nS	činit
10,34	[number]	k4	10,34
<g/>
"	"	kIx"	"
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
