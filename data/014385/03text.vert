<s>
Čertův	čertův	k2eAgInSc1d1
mlýn	mlýn	k1gInSc1
(	(	kIx(
<g/>
Moravskoslezské	moravskoslezský	k2eAgInPc1d1
Beskydy	Beskyd	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
Čertův	čertův	k2eAgInSc1d1
mlýn	mlýn	k1gInSc1
Čertův	čertův	k2eAgInSc1d1
mlýn	mlýn	k1gInSc4
z	z	k7c2
Pusteven	Pusteven	k2eAgInSc1d1
<g/>
,	,	kIx,
v	v	k7c6
pozadí	pozadí	k1gNnSc6
Kněhyně	kněhyně	k1gFnSc2
</s>
<s>
Vrchol	vrchol	k1gInSc1
</s>
<s>
1206	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Prominence	prominence	k1gFnSc2
</s>
<s>
68	#num#	k4
m	m	kA
↓	↓	k?
sedlo	sedlo	k1gNnSc1
s	s	k7c7
Kněhyní	kněhyně	k1gFnSc7
Izolace	izolace	k1gFnSc2
</s>
<s>
840	#num#	k4
m	m	kA
→	→	k?
Kněhyně	kněhyně	k1gFnSc1
Seznamy	seznam	k1gInPc4
</s>
<s>
Tisícovky	tisícovka	k1gFnPc1
v	v	k7c4
ČeskuHory	ČeskuHora	k1gFnPc4
Moravskoslezských	moravskoslezský	k2eAgFnPc2d1
Beskyd	Beskydy	k1gFnPc2
Poloha	poloha	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc4
Pohoří	pohořet	k5eAaPmIp3nS
</s>
<s>
Moravskoslezské	moravskoslezský	k2eAgFnPc1d1
Beskydy	Beskydy	k1gFnPc1
/	/	kIx~
Radhošťská	radhošťský	k2eAgFnSc1d1
hornatina	hornatina	k1gFnSc1
/	/	kIx~
Radhošťský	radhošťský	k2eAgInSc1d1
hřbet	hřbet	k1gInSc1
/	/	kIx~
Kněhyňský	Kněhyňský	k2eAgInSc4d1
hřbet	hřbet	k1gInSc4
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
29	#num#	k4
<g/>
′	′	k?
<g/>
22	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
18	#num#	k4
<g/>
°	°	k?
<g/>
18	#num#	k4
<g/>
′	′	k?
<g/>
6	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Čertův	čertův	k2eAgInSc1d1
mlýn	mlýn	k1gInSc1
</s>
<s>
Hornina	hornina	k1gFnSc1
</s>
<s>
jílovec	jílovec	k1gInSc1
<g/>
,	,	kIx,
pískovec	pískovec	k1gInSc1
Povodí	povodí	k1gNnSc2
</s>
<s>
Rožnovská	rožnovský	k2eAgFnSc1d1
Bečva	Bečva	k1gFnSc1
(	(	kIx(
<g/>
Bečva	Bečva	k1gFnSc1
<g/>
,	,	kIx,
Morava	Morava	k1gFnSc1
<g/>
,	,	kIx,
Dunaj	Dunaj	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Lubina	Lubina	k1gFnSc1
(	(	kIx(
<g/>
Odra	Odra	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Čeladenka	Čeladenka	k1gFnSc1
(	(	kIx(
<g/>
Ostravice	Ostravice	k1gFnSc1
<g/>
,	,	kIx,
Odra	Odra	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Čertův	čertův	k2eAgInSc1d1
mlýn	mlýn	k1gInSc1
je	být	k5eAaImIp3nS
pátá	pátá	k1gFnSc1
(	(	kIx(
<g/>
po	po	k7c6
Lysé	Lysé	k2eAgFnSc6d1
hoře	hora	k1gFnSc6
<g/>
,	,	kIx,
Smrku	smrk	k1gInSc6
<g/>
,	,	kIx,
Kněhyni	kněhyně	k1gFnSc6
a	a	k8xC
Malchoru	Malchor	k1gInSc6
<g/>
)	)	kIx)
nejvyšší	vysoký	k2eAgFnSc1d3
hora	hora	k1gFnSc1
Moravskoslezských	moravskoslezský	k2eAgFnPc2d1
Beskyd	Beskydy	k1gFnPc2
<g/>
,	,	kIx,
nacházející	nacházející	k2eAgMnSc1d1
se	s	k7c7
2,5	2,5	k4
km	km	kA
východně	východně	k6eAd1
od	od	k7c2
Pusteven	Pusteven	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vrchol	vrchol	k1gInSc1
hory	hora	k1gFnSc2
je	být	k5eAaImIp3nS
nejvyšším	vysoký	k2eAgInSc7d3
bodem	bod	k1gInSc7
okresu	okres	k1gInSc2
Vsetín	Vsetín	k1gInSc1
a	a	k8xC
celého	celý	k2eAgInSc2d1
Zlínského	zlínský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zalesněno	zalesnit	k5eAaPmNgNnS
smrkovým	smrkový	k2eAgInSc7d1
lesem	les	k1gInSc7
<g/>
,	,	kIx,
bez	bez	k7c2
výhledů	výhled	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celá	celý	k2eAgFnSc1d1
vrcholová	vrcholový	k2eAgFnSc1d1
část	část	k1gFnSc1
je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
NPR	NPR	kA
Kněhyně	kněhyně	k1gFnSc1
-	-	kIx~
Čertův	čertův	k2eAgInSc1d1
Mlýn	mlýn	k1gInSc1
<g/>
,	,	kIx,
nejrozsáhlejší	rozsáhlý	k2eAgFnPc1d3
rezervace	rezervace	k1gFnPc1
v	v	k7c6
Moravskoslezských	moravskoslezský	k2eAgInPc6d1
Beskydech	Beskyd	k1gInPc6
<g/>
,	,	kIx,
vyhlášené	vyhlášený	k2eAgFnPc1d1
roku	rok	k1gInSc2
1982	#num#	k4
na	na	k7c6
ploše	plocha	k1gFnSc6
195	#num#	k4
ha	ha	kA
kvůli	kvůli	k7c3
ochraně	ochrana	k1gFnSc3
přirozených	přirozený	k2eAgFnPc2d1
vrcholových	vrcholový	k2eAgFnPc2d1
smrčin	smrčina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Název	název	k1gInSc1
vznikl	vzniknout	k5eAaPmAgInS
podle	podle	k7c2
pověsti	pověst	k1gFnSc2
<g/>
,	,	kIx,
že	že	k8xS
ve	v	k7c6
žlabu	žlab	k1gInSc6
nedaleko	nedaleko	k7c2
vrcholu	vrchol	k1gInSc2
postavil	postavit	k5eAaPmAgMnS
čert	čert	k1gMnSc1
mlýn	mlýn	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
ukázku	ukázka	k1gFnSc4
odsedání	odsedání	k1gNnSc2
bloků	blok	k1gInPc2
tzv.	tzv.	kA
godulského	godulský	k2eAgInSc2d1
pískovce	pískovec	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Přístup	přístup	k1gInSc1
</s>
<s>
Vrchol	vrchol	k1gInSc1
je	být	k5eAaImIp3nS
přístupný	přístupný	k2eAgInSc1d1
po	po	k7c6
červené	červený	k2eAgFnSc6d1
značce	značka	k1gFnSc6
z	z	k7c2
Pusteven	Pusteven	k2eAgInSc4d1
přes	přes	k7c4
Tanečnici	tanečnice	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
kopíruje	kopírovat	k5eAaImIp3nS
i	i	k9
zimní	zimní	k2eAgFnSc1d1
lyžařská	lyžařský	k2eAgFnSc1d1
trasa	trasa	k1gFnSc1
(	(	kIx(
<g/>
celkem	celkem	k6eAd1
3	#num#	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
něco	něco	k3yInSc4
kratší	krátký	k2eAgFnSc1d2
varianta	varianta	k1gFnSc1
vede	vést	k5eAaImIp3nS
z	z	k7c2
Pusteven	Pusteven	k2eAgInSc1d1
po	po	k7c6
zelené	zelený	k2eAgFnSc6d1
značce	značka	k1gFnSc6
do	do	k7c2
rozcestí	rozcestí	k1gNnSc2
Tanečnice	tanečnice	k1gFnSc2
-	-	kIx~
sedlo	sedlo	k1gNnSc1
a	a	k8xC
dále	daleko	k6eAd2
po	po	k7c6
zmíněné	zmíněný	k2eAgFnSc6d1
červené	červená	k1gFnSc6
(	(	kIx(
<g/>
2,5	2,5	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Okolí	okolí	k1gNnSc1
</s>
<s>
Nedaleko	nedaleko	k7c2
vrcholu	vrchol	k1gInSc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
skalní	skalní	k2eAgInSc1d1
útvar	útvar	k1gInSc1
Čertův	čertův	k2eAgInSc1d1
stůl	stůl	k1gInSc4
<g/>
,	,	kIx,
leží	ležet	k5eAaImIp3nS
však	však	k9
mimo	mimo	k7c4
značené	značený	k2eAgFnPc4d1
cesty	cesta	k1gFnPc4
a	a	k8xC
z	z	k7c2
důvodu	důvod	k1gInSc2
ochrany	ochrana	k1gFnSc2
území	území	k1gNnPc2
je	být	k5eAaImIp3nS
nepřístupný	přístupný	k2eNgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
sedle	sedlo	k1gNnSc6
mezi	mezi	k7c7
Kněhyní	kněhyně	k1gFnSc7
a	a	k8xC
Čertovým	čertový	k2eAgInSc7d1
mlýnem	mlýn	k1gInSc7
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
pomník	pomník	k1gInSc1
Partyzánské	partyzánský	k2eAgFnSc2d1
brigády	brigáda	k1gFnSc2
Jana	Jan	k1gMnSc2
Žižky	Žižka	k1gMnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
zde	zde	k6eAd1
působila	působit	k5eAaImAgFnS
na	na	k7c4
podzim	podzim	k1gInSc4
roku	rok	k1gInSc2
1944	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Činnost	činnost	k1gFnSc1
brigády	brigáda	k1gFnSc2
dokládají	dokládat	k5eAaImIp3nP
zbytky	zbytek	k1gInPc4
bunkru	bunkr	k1gInSc2
nedaleko	nedaleko	k7c2
pomníku	pomník	k1gInSc2
a	a	k8xC
také	také	k9
pomníček	pomníček	k1gInSc1
partyzánky	partyzánka	k1gFnSc2
Růženy	Růžena	k1gFnSc2
Valentové	Valentová	k1gFnSc2
u	u	k7c2
studánky	studánka	k1gFnSc2
<g/>
,	,	kIx,
vzdálené	vzdálený	k2eAgFnSc6d1
500	#num#	k4
m	m	kA
východně	východně	k6eAd1
od	od	k7c2
sedla	sedlo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Čertův	čertův	k2eAgInSc4d1
mlýn	mlýn	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Čertův	čertův	k2eAgInSc1d1
mlýn	mlýn	k1gInSc1
na	na	k7c4
Tisicovky	Tisicovka	k1gFnPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Čertův	čertův	k2eAgInSc1d1
stůl	stůl	k1gInSc1
na	na	k7c4
Hrady	hrad	k1gInPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Beskydské	beskydský	k2eAgFnSc2d1
tisícovky	tisícovka	k1gFnSc2
Hlavní	hlavní	k2eAgInPc1d1
vrcholy	vrchol	k1gInPc1
(	(	kIx(
<g/>
27	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Lysá	lysý	k2eAgFnSc1d1
hora	hora	k1gFnSc1
(	(	kIx(
<g/>
1324	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Smrk	smrk	k1gInSc1
(	(	kIx(
<g/>
1277	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Kněhyně	kněhyně	k1gFnSc1
(	(	kIx(
<g/>
1258	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Čertův	čertův	k2eAgInSc1d1
mlýn	mlýn	k1gInSc1
(	(	kIx(
<g/>
1206	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Travný	travný	k2eAgMnSc1d1
(	(	kIx(
<g/>
1204	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Malý	malý	k2eAgInSc1d1
Smrk	smrk	k1gInSc1
(	(	kIx(
<g/>
1174	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Radhošť	Radhošť	k1gInSc1
(	(	kIx(
<g/>
1129	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Radegast	Radegast	k1gMnSc1
(	(	kIx(
<g/>
1106	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Radegast	Radegast	k1gMnSc1
Z1	Z1	k1gMnSc1
(	(	kIx(
<g/>
1100	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Tanečnice	tanečnice	k1gFnSc1
(	(	kIx(
<g/>
1084	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Ropice	Ropice	k1gFnSc1
(	(	kIx(
<g/>
1082	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Tanečnice	tanečnice	k1gFnSc2
Z	z	k7c2
(	(	kIx(
<g/>
1076	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Magurka	Magurka	k1gFnSc1
(	(	kIx(
<g/>
1068	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Velký	velký	k2eAgInSc1d1
Polom	polom	k1gInSc1
(	(	kIx(
<g/>
1067	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Malý	malý	k2eAgInSc1d1
Polom	polom	k1gInSc1
(	(	kIx(
<g/>
1061	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Slavíč	Slavíč	k1gInSc1
(	(	kIx(
<g/>
1056	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Nořičí	Nořičí	k1gFnSc1
hora	hora	k1gFnSc1
(	(	kIx(
<g/>
1047	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Velká	velká	k1gFnSc1
Stolová	stolový	k2eAgFnSc1d1
(	(	kIx(
<g/>
1046	#num#	k4
m	m	kA
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
•	•	k?
Ostrý	ostrý	k2eAgMnSc1d1
(	(	kIx(
<g/>
1044	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Javorový	javorový	k2eAgInSc4d1
vrch	vrch	k1gInSc4
(	(	kIx(
<g/>
1032	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Burkův	Burkův	k2eAgInSc4d1
vrch	vrch	k1gInSc4
(	(	kIx(
<g/>
1032	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Burkův	Burkův	k2eAgInSc4d1
vrch	vrch	k1gInSc4
Z	z	k7c2
(	(	kIx(
<g/>
1021	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Smrčina	smrčina	k1gFnSc1
(	(	kIx(
<g/>
1015	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Čuboňov	Čuboňov	k1gInSc1
(	(	kIx(
<g/>
1011	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Malá	malý	k2eAgFnSc1d1
Stolová	stolový	k2eAgFnSc1d1
(	(	kIx(
<g/>
1009	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Šindelná	Šindelný	k2eAgFnSc1d1
(	(	kIx(
<g/>
1003	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Nad	nad	k7c7
Kršlí	Kršle	k1gFnSc7
(	(	kIx(
<g/>
1001	#num#	k4
m	m	kA
<g/>
)	)	kIx)
Vedlejší	vedlejší	k2eAgInPc1d1
vrcholy	vrchol	k1gInPc1
(	(	kIx(
<g/>
5	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Smrk	smrk	k1gInSc1
J	J	kA
(	(	kIx(
<g/>
1248	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Malchor	Malchor	k1gInSc1
(	(	kIx(
<g/>
1220	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Radegast	Radegast	k1gMnSc1
Z2	Z2	k1gMnSc1
(	(	kIx(
<g/>
1099	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Zmrzlý	zmrzlý	k2eAgInSc4d1
vrch	vrch	k1gInSc4
(	(	kIx(
<g/>
1043	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Slavíč	Slavíč	k1gInSc1
JV	JV	kA
(	(	kIx(
<g/>
1012	#num#	k4
m	m	kA
<g/>
)	)	kIx)
</s>
