<s>
Čertův	čertův	k2eAgInSc1d1
mlýn	mlýn	k1gInSc1
je	být	k5eAaImIp3nS
pátá	pátá	k1gFnSc1
(	(	kIx(
<g/>
po	po	k7c6
Lysé	Lysé	k2eAgFnSc6d1
hoře	hora	k1gFnSc6
<g/>
,	,	kIx,
Smrku	smrk	k1gInSc6
<g/>
,	,	kIx,
Kněhyni	kněhyně	k1gFnSc6
a	a	k8xC
Malchoru	Malchor	k1gInSc6
<g/>
)	)	kIx)
nejvyšší	vysoký	k2eAgFnSc1d3
hora	hora	k1gFnSc1
Moravskoslezských	moravskoslezský	k2eAgFnPc2d1
Beskyd	Beskydy	k1gFnPc2
<g/>
,	,	kIx,
nacházející	nacházející	k2eAgMnSc1d1
se	s	k7c7
2,5	2,5	k4
km	km	kA
východně	východně	k6eAd1
od	od	k7c2
Pusteven	Pusteven	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>