<s>
M79	M79	k4
je	být	k5eAaImIp3nS
od	od	k7c2
Země	zem	k1gFnSc2
vzdálená	vzdálený	k2eAgFnSc1d1
okolo	okolo	k7c2
42	#num#	k4
100	#num#	k4
světelných	světelný	k2eAgNnPc2d1
let	rok	k1gInPc2
a	a	k8xC
od	od	k7c2
středu	střed	k1gInSc2
Galaxie	galaxie	k1gFnSc2
asi	asi	k9
60	#num#	k4
000	#num#	k4
světelných	světelný	k2eAgNnPc2d1
let	rok	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Jejímu	její	k3xOp3gInSc3
úhlovému	úhlový	k2eAgInSc3d1
rozměru	rozměr	k1gInSc3
9,6	9,6	k4
<g/>
'	'	kIx"
v	v	k7c6
této	tento	k3xDgFnSc6
vzdálenosti	vzdálenost	k1gFnSc6
odpovídá	odpovídat	k5eAaImIp3nS
skutečná	skutečný	k2eAgFnSc1d1
velikost	velikost	k1gFnSc1
přibližně	přibližně	k6eAd1
118	#num#	k4
světelných	světelný	k2eAgNnPc2d1
let	rok	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g />
.	.	kIx.
</s>