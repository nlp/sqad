<s>
Audi	Audi	k1gNnSc1	Audi
A4	A4	k1gFnSc2	A4
je	být	k5eAaImIp3nS	být
osobní	osobní	k2eAgInSc4d1	osobní
automobil	automobil	k1gInSc4	automobil
střední	střední	k2eAgFnSc2d1	střední
třídy	třída	k1gFnSc2	třída
vyráběný	vyráběný	k2eAgInSc1d1	vyráběný
německým	německý	k2eAgMnSc7d1	německý
výrobcem	výrobce	k1gMnSc7	výrobce
Audi	Audi	k1gNnSc2	Audi
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
trh	trh	k1gInSc4	trh
byl	být	k5eAaImAgInS	být
uveden	uvést	k5eAaPmNgInS	uvést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
jako	jako	k8xS	jako
nástupce	nástupce	k1gMnSc1	nástupce
modelu	model	k1gInSc2	model
Audi	Audi	k1gNnSc1	Audi
80	[number]	k4	80
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
jako	jako	k9	jako
sedan	sedan	k1gInSc1	sedan
<g/>
,	,	kIx,	,
kombi	kombi	k1gNnSc1	kombi
a	a	k8xC	a
kabriolet	kabriolet	k1gInSc1	kabriolet
<g/>
.	.	kIx.	.
</s>
<s>
Audi	Audi	k1gNnSc1	Audi
představilo	představit	k5eAaPmAgNnS	představit
model	model	k1gInSc4	model
A4	A4	k1gFnSc2	A4
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
A4	A4	k4	A4
bylo	být	k5eAaImAgNnS	být
postavené	postavený	k2eAgNnSc1d1	postavené
na	na	k7c6	na
platformě	platforma	k1gFnSc6	platforma
B	B	kA	B
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yIgMnPc4	který
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
představen	představit	k5eAaPmNgInS	představit
i	i	k8xC	i
nový	nový	k2eAgInSc1d1	nový
model	model	k1gInSc1	model
Passat	Passat	k1gFnSc2	Passat
od	od	k7c2	od
Volkswagenu	volkswagen	k1gInSc2	volkswagen
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
první	první	k4xOgNnSc1	první
auto	auto	k1gNnSc1	auto
od	od	k7c2	od
Volkswagen	volkswagen	k1gInSc1	volkswagen
Group	Group	k1gInSc1	Group
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
mělo	mít	k5eAaImAgNnS	mít
motor	motor	k1gInSc4	motor
s	s	k7c7	s
pěti	pět	k4xCc7	pět
ventily	ventil	k1gInPc7	ventil
na	na	k7c4	na
každý	každý	k3xTgInSc4	každý
válec	válec	k1gInSc4	válec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
bylo	být	k5eAaImAgNnS	být
Ferarri	Ferarri	k1gNnSc1	Ferarri
F	F	kA	F
<g/>
355	[number]	k4	355
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
A4	A4	k1gFnSc1	A4
jediné	jediný	k2eAgNnSc4d1	jediné
sériově	sériově	k6eAd1	sériově
vyráběné	vyráběný	k2eAgNnSc4d1	vyráběné
auto	auto	k1gNnSc4	auto
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
technologií	technologie	k1gFnSc7	technologie
<g/>
.	.	kIx.	.
</s>
<s>
Automobil	automobil	k1gInSc1	automobil
byl	být	k5eAaImAgInS	být
dodáván	dodávat	k5eAaImNgInS	dodávat
i	i	k8xC	i
se	s	k7c7	s
systémem	systém	k1gInSc7	systém
stálého	stálý	k2eAgInSc2d1	stálý
pohonu	pohon	k1gInSc2	pohon
všech	všecek	k3xTgFnPc2	všecek
čtyř	čtyři	k4xCgFnPc2	čtyři
kol	kola	k1gFnPc2	kola
quattro	quattro	k6eAd1	quattro
<g/>
.	.	kIx.	.
</s>
<s>
Audi	Audi	k1gNnSc1	Audi
A4	A4	k1gFnSc2	A4
dostalo	dostat	k5eAaPmAgNnS	dostat
novou	nový	k2eAgFnSc4d1	nová
tvář	tvář	k1gFnSc4	tvář
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
zahrnovalo	zahrnovat	k5eAaImAgNnS	zahrnovat
kromě	kromě	k7c2	kromě
drobných	drobný	k2eAgFnPc2d1	drobná
změn	změna	k1gFnPc2	změna
čirá	čirý	k2eAgNnPc1d1	čiré
přední	přední	k2eAgNnPc1d1	přední
světla	světlo	k1gNnPc1	světlo
a	a	k8xC	a
nové	nový	k2eAgInPc1d1	nový
klíčky	klíček	k1gInPc1	klíček
<g/>
.	.	kIx.	.
1.6	[number]	k4	1.6
74	[number]	k4	74
<g/>
kW	kW	kA	kW
1.6	[number]	k4	1.6
75	[number]	k4	75
<g/>
kW	kW	kA	kW
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
facelift	facelift	k5eAaPmF	facelift
<g/>
)	)	kIx)	)
1.8	[number]	k4	1.8
20V	[number]	k4	20V
85	[number]	k4	85
<g/>
kW	kW	kA	kW
1.8	[number]	k4	1.8
20V	[number]	k4	20V
92	[number]	k4	92
<g/>
kW	kW	kA	kW
1.8	[number]	k4	1.8
20V	[number]	k4	20V
95	[number]	k4	95
<g />
.	.	kIx.	.
</s>
<s>
<g/>
kW	kW	kA	kW
1.8	[number]	k4	1.8
20V	[number]	k4	20V
Biturbo	Biturba	k1gFnSc5	Biturba
186	[number]	k4	186
<g/>
kW	kW	kA	kW
1.8	[number]	k4	1.8
20V	[number]	k4	20V
125	[number]	k4	125
<g/>
kW	kW	kA	kW
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
facelift	facelift	k5eAaPmF	facelift
<g/>
)	)	kIx)	)
1.8	[number]	k4	1.8
20V	[number]	k4	20V
Turbo	turba	k1gFnSc5	turba
110	[number]	k4	110
<g/>
kW	kW	kA	kW
1.8	[number]	k4	1.8
20V	[number]	k4	20V
Turbo	turba	k1gFnSc5	turba
132	[number]	k4	132
<g/>
kW	kW	kA	kW
2.4	[number]	k4	2.4
V6	V6	k1gFnSc2	V6
30V	[number]	k4	30V
121	[number]	k4	121
<g/>
kW	kW	kA	kW
2.6	[number]	k4	2.6
V6	V6	k1gFnSc2	V6
12V	[number]	k4	12V
110	[number]	k4	110
<g/>
kW	kW	kA	kW
2.8	[number]	k4	2.8
V6	V6	k1gFnSc2	V6
12V	[number]	k4	12V
128	[number]	k4	128
<g/>
kW	kW	kA	kW
2.8	[number]	k4	2.8
<g />
.	.	kIx.	.
</s>
<s>
V6	V6	k4	V6
30V	[number]	k4	30V
142	[number]	k4	142
<g/>
kW	kW	kA	kW
2.7	[number]	k4	2.7
Biturbo	Biturba	k1gFnSc5	Biturba
V6	V6	k1gMnSc2	V6
186	[number]	k4	186
<g/>
kW	kW	kA	kW
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
verze	verze	k1gFnSc1	verze
S	s	k7c7	s
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
2.7	[number]	k4	2.7
Biturbo	Biturba	k1gFnSc5	Biturba
V6	V6	k1gMnSc2	V6
195	[number]	k4	195
<g/>
kW	kW	kA	kW
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
verze	verze	k1gFnSc1	verze
S	s	k7c7	s
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
2.7	[number]	k4	2.7
Biturbo	Biturba	k1gFnSc5	Biturba
V6	V6	k1gMnSc2	V6
279	[number]	k4	279
kW	kW	kA	kW
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
verze	verze	k1gFnSc1	verze
RS4	RS4	k1gMnSc1	RS4
Avant	Avant	k1gMnSc1	Avant
<g/>
)	)	kIx)	)
1.9	[number]	k4	1.9
TDI	TDI	kA	TDI
<g />
.	.	kIx.	.
</s>
<s>
55	[number]	k4	55
<g/>
kW	kW	kA	kW
1.9	[number]	k4	1.9
TDI	TDI	kA	TDI
66	[number]	k4	66
<g/>
kW	kW	kA	kW
1.9	[number]	k4	1.9
TDI	TDI	kA	TDI
81	[number]	k4	81
<g/>
kW	kW	kA	kW
1.9	[number]	k4	1.9
TDI-PD	TDI-PD	k1gFnSc1	TDI-PD
85	[number]	k4	85
<g/>
kW	kW	kA	kW
2.5	[number]	k4	2.5
V6	V6	k1gFnSc2	V6
TDI	TDI	kA	TDI
110	[number]	k4	110
<g/>
kW	kW	kA	kW
2.5	[number]	k4	2.5
V6	V6	k1gFnSc2	V6
TDI	TDI	kA	TDI
132	[number]	k4	132
<g/>
kW	kW	kA	kW
Úplně	úplně	k6eAd1	úplně
nové	nový	k2eAgFnSc6d1	nová
A4	A4	k1gFnSc3	A4
bylo	být	k5eAaImAgNnS	být
představené	představený	k2eAgNnSc1d1	představené
už	už	k6eAd1	už
na	na	k7c6	na
platformě	platforma	k1gFnSc6	platforma
B	B	kA	B
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
mělo	mít	k5eAaImAgNnS	mít
úplně	úplně	k6eAd1	úplně
novou	nový	k2eAgFnSc4d1	nová
převodovku	převodovka	k1gFnSc4	převodovka
Multitronic	Multitronice	k1gFnPc2	Multitronice
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
kabriolet	kabriolet	k1gInSc1	kabriolet
byl	být	k5eAaImAgInS	být
představený	představený	k2eAgInSc1d1	představený
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Benzinové	benzinový	k2eAgInPc1d1	benzinový
motory	motor	k1gInPc1	motor
1.6	[number]	k4	1.6
L	L	kA	L
(	(	kIx(	(
<g/>
75	[number]	k4	75
<g/>
kW	kW	kA	kW
<g/>
)	)	kIx)	)
1.8	[number]	k4	1.8
<g/>
T	T	kA	T
L	L	kA	L
(	(	kIx(	(
<g/>
110	[number]	k4	110
<g/>
-	-	kIx~	-
<g/>
120	[number]	k4	120
<g/>
-	-	kIx~	-
<g/>
132	[number]	k4	132
<g/>
-	-	kIx~	-
<g/>
140	[number]	k4	140
<g/>
kW	kW	kA	kW
<g/>
)	)	kIx)	)
2.0	[number]	k4	2.0
L	L	kA	L
(	(	kIx(	(
<g/>
96	[number]	k4	96
<g/>
kW	kW	kA	kW
<g/>
)	)	kIx)	)
2.4	[number]	k4	2.4
L	L	kA	L
V6	V6	k1gFnPc2	V6
(	(	kIx(	(
<g/>
125	[number]	k4	125
<g/>
<g />
.	.	kIx.	.
</s>
<s>
kW	kW	kA	kW
<g/>
)	)	kIx)	)
3.0	[number]	k4	3.0
L	L	kA	L
V6	V6	k1gFnPc2	V6
(	(	kIx(	(
<g/>
162	[number]	k4	162
<g/>
kW	kW	kA	kW
<g/>
)	)	kIx)	)
4.2	[number]	k4	4.2
L	L	kA	L
V8	V8	k1gFnPc2	V8
(	(	kIx(	(
<g/>
253	[number]	k4	253
<g/>
kW	kW	kA	kW
<g/>
)	)	kIx)	)
→	→	k?	→
verze	verze	k1gFnSc1	verze
S4	S4	k1gFnSc2	S4
Naftové	naftový	k2eAgInPc4d1	naftový
motory	motor	k1gInPc7	motor
1.9	[number]	k4	1.9
L	L	kA	L
TDI	TDI	kA	TDI
(	(	kIx(	(
<g/>
74	[number]	k4	74
<g/>
-	-	kIx~	-
<g/>
85	[number]	k4	85
<g/>
-	-	kIx~	-
<g/>
96	[number]	k4	96
<g/>
kW	kW	kA	kW
<g/>
)	)	kIx)	)
2.5	[number]	k4	2.5
L	L	kA	L
TDI	TDI	kA	TDI
(	(	kIx(	(
<g/>
114	[number]	k4	114
<g/>
-	-	kIx~	-
<g/>
120	[number]	k4	120
<g/>
-	-	kIx~	-
<g/>
132	[number]	k4	132
<g/>
kW	kW	kA	kW
<g/>
)	)	kIx)	)
Audi	Audi	k1gNnSc1	Audi
představilo	představit	k5eAaPmAgNnS	představit
facelift	facelift	k1gInSc4	facelift
A4	A4	k1gFnSc2	A4
postavený	postavený	k2eAgInSc4d1	postavený
na	na	k7c6	na
platformě	platforma	k1gFnSc6	platforma
B7	B7	k1gFnSc2	B7
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
kabriolet	kabriolet	k1gInSc1	kabriolet
byl	být	k5eAaImAgInS	být
představený	představený	k2eAgMnSc1d1	představený
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
1.6	[number]	k4	1.6
L	L	kA	L
1.8	[number]	k4	1.8
L	L	kA	L
20V	[number]	k4	20V
Turbo	turba	k1gFnSc5	turba
2.0	[number]	k4	2.0
L	L	kA	L
20V	[number]	k4	20V
2.0	[number]	k4	2.0
L	L	kA	L
TFSI	TFSI	kA	TFSI
3.2	[number]	k4	3.2
L	L	kA	L
V6	V6	k1gFnPc2	V6
FSI	FSI	kA	FSI
1.9	[number]	k4	1.9
L	L	kA	L
TDI	TDI	kA	TDI
2.0	[number]	k4	2.0
L	L	kA	L
TDI	TDI	kA	TDI
2.7	[number]	k4	2.7
L	L	kA	L
TDI	TDI	kA	TDI
3.0	[number]	k4	3.0
L	L	kA	L
TDI	TDI	kA	TDI
4,2	[number]	k4	4,2
L	L	kA	L
Třetí	třetí	k4xOgFnSc1	třetí
generace	generace	k1gFnSc1	generace
byla	být	k5eAaImAgFnS	být
představena	představit	k5eAaPmNgFnS	představit
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Rozdílem	rozdíl	k1gInSc7	rozdíl
oproti	oproti	k7c3	oproti
starší	starý	k2eAgFnSc3d2	starší
verzi	verze	k1gFnSc3	verze
jsou	být	k5eAaImIp3nP	být
nová	nový	k2eAgNnPc1d1	nové
světla	světlo	k1gNnPc1	světlo
s	s	k7c7	s
LED	led	k1gInSc1	led
pruhem	pruh	k1gInSc7	pruh
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
vybavenějších	vybavený	k2eAgFnPc2d2	vybavenější
verzí	verze	k1gFnPc2	verze
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
je	být	k5eAaImIp3nS	být
i	i	k9	i
kabriolet	kabriolet	k1gInSc4	kabriolet
s	s	k7c7	s
klasickou	klasický	k2eAgFnSc7d1	klasická
plátěnou	plátěný	k2eAgFnSc7d1	plátěná
střechou	střecha	k1gFnSc7	střecha
<g/>
.	.	kIx.	.
</s>
<s>
Benzín	benzín	k1gInSc1	benzín
<g/>
:	:	kIx,	:
1.8	[number]	k4	1.8
TFSI	TFSI	kA	TFSI
-	-	kIx~	-
88	[number]	k4	88
kW	kW	kA	kW
1.8	[number]	k4	1.8
TFSI	TFSI	kA	TFSI
<g/>
,	,	kIx,	,
1.8	[number]	k4	1.8
TFSI	TFSI	kA	TFSI
quattro	quattro	k6eAd1	quattro
-	-	kIx~	-
118	[number]	k4	118
kW	kW	kA	kW
1.8	[number]	k4	1.8
TFSI	TFSI	kA	TFSI
<g/>
,	,	kIx,	,
1.8	[number]	k4	1.8
TFSI	TFSI	kA	TFSI
quattro	quattro	k6eAd1	quattro
-	-	kIx~	-
125	[number]	k4	125
kW	kW	kA	kW
2.0	[number]	k4	2.0
TFSI	TFSI	kA	TFSI
<g/>
,	,	kIx,	,
2.0	[number]	k4	2.0
TFSI	TFSI	kA	TFSI
quattro	quattro	k6eAd1	quattro
-	-	kIx~	-
132	[number]	k4	132
kW	kW	kA	kW
2.0	[number]	k4	2.0
TFSI	TFSI	kA	TFSI
<g/>
,	,	kIx,	,
2.0	[number]	k4	2.0
TFSI	TFSI	kA	TFSI
quattro	quattro	k6eAd1	quattro
-	-	kIx~	-
155	[number]	k4	155
kW	kW	kA	kW
2.0	[number]	k4	2.0
TFSI	TFSI	kA	TFSI
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
2.0	[number]	k4	2.0
TFSI	TFSI	kA	TFSI
quattro	quattro	k6eAd1	quattro
-	-	kIx~	-
165	[number]	k4	165
kW	kW	kA	kW
3.0	[number]	k4	3.0
TFSI	TFSI	kA	TFSI
quattro	quattro	k6eAd1	quattro
-	-	kIx~	-
200	[number]	k4	200
kW	kW	kA	kW
S4	S4	k1gFnSc2	S4
quattro	quattro	k6eAd1	quattro
-	-	kIx~	-
245	[number]	k4	245
kW	kW	kA	kW
3.2	[number]	k4	3.2
FSI	FSI	kA	FSI
<g/>
,	,	kIx,	,
3.2	[number]	k4	3.2
FSI	FSI	kA	FSI
quattro	quattro	k6eAd1	quattro
–	–	k?	–
195	[number]	k4	195
kW	kW	kA	kW
<g/>
,	,	kIx,	,
250	[number]	k4	250
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
RS4	RS4	k1gFnPc4	RS4
quattro	quattro	k6eAd1	quattro
-	-	kIx~	-
331	[number]	k4	331
kW	kW	kA	kW
Diesel	diesel	k1gInSc1	diesel
<g/>
:	:	kIx,	:
2.0	[number]	k4	2.0
TDI	TDI	kA	TDI
-	-	kIx~	-
88	[number]	k4	88
kW	kW	kA	kW
<g />
.	.	kIx.	.
</s>
<s>
2.0	[number]	k4	2.0
TDI	TDI	kA	TDI
-	-	kIx~	-
100	[number]	k4	100
kW	kW	kA	kW
2.0	[number]	k4	2.0
TDI	TDI	kA	TDI
<g/>
,	,	kIx,	,
2.0	[number]	k4	2.0
TDI	TDI	kA	TDI
quattro	quattro	k6eAd1	quattro
-	-	kIx~	-
105	[number]	k4	105
kW	kW	kA	kW
2.0	[number]	k4	2.0
TDI	TDI	kA	TDI
<g/>
,	,	kIx,	,
2.0	[number]	k4	2.0
TDI	TDI	kA	TDI
quattro	quattro	k6eAd1	quattro
-	-	kIx~	-
125	[number]	k4	125
kW	kW	kA	kW
2,7	[number]	k4	2,7
TDI	TDI	kA	TDI
–	–	k?	–
140	[number]	k4	140
kW	kW	kA	kW
k	k	k7c3	k
mání	mání	k1gFnSc3	mání
s	s	k7c7	s
poháněnými	poháněný	k2eAgNnPc7d1	poháněné
předními	přední	k2eAgNnPc7d1	přední
koly	kolo	k1gNnPc7	kolo
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
s	s	k7c7	s
quattrem	quattr	k1gInSc7	quattr
používajícím	používající	k2eAgInSc7d1	používající
spolehlivý	spolehlivý	k2eAgInSc1d1	spolehlivý
diferenciál	diferenciál	k1gInSc4	diferenciál
Torsen	Torsen	k2eAgInSc4d1	Torsen
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
velmi	velmi	k6eAd1	velmi
ekonomický	ekonomický	k2eAgInSc1d1	ekonomický
motor	motor	k1gInSc1	motor
<g/>
,	,	kIx,	,
z	z	k7c2	z
vlastní	vlastní	k2eAgFnSc2d1	vlastní
zkušenosti	zkušenost	k1gFnSc2	zkušenost
úsporně	úsporně	k6eAd1	úsporně
založený	založený	k2eAgInSc1d1	založený
řidič	řidič	k1gInSc1	řidič
se	se	k3xPyFc4	se
dostane	dostat	k5eAaPmIp3nS	dostat
i	i	k9	i
pod	pod	k7c7	pod
výrobcem	výrobce	k1gMnSc7	výrobce
udávaných	udávaný	k2eAgInPc2d1	udávaný
6,6	[number]	k4	6,6
l	l	kA	l
<g/>
/	/	kIx~	/
<g/>
100	[number]	k4	100
<g/>
km	km	kA	km
<g/>
)	)	kIx)	)
3.0	[number]	k4	3.0
TDI	TDI	kA	TDI
quattro	quattro	k6eAd1	quattro
–	–	k?	–
177	[number]	k4	177
kW	kW	kA	kW
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
quattro	quattro	k6eAd1	quattro
<g/>
)	)	kIx)	)
Pátá	pátý	k4xOgFnSc1	pátý
generace	generace	k1gFnSc1	generace
byla	být	k5eAaImAgFnS	být
představena	představit	k5eAaPmNgFnS	představit
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Benzín	benzín	k1gInSc1	benzín
<g/>
:	:	kIx,	:
1.4	[number]	k4	1.4
TFSI	TFSI	kA	TFSI
-	-	kIx~	-
110	[number]	k4	110
<g/>
kW	kW	kA	kW
(	(	kIx(	(
<g/>
150	[number]	k4	150
PS	PS	kA	PS
<g/>
)	)	kIx)	)
2.0	[number]	k4	2.0
TFSI	TFSI	kA	TFSI
-	-	kIx~	-
140	[number]	k4	140
kW	kW	kA	kW
(	(	kIx(	(
<g/>
190	[number]	k4	190
PS	PS	kA	PS
<g/>
)	)	kIx)	)
2.0	[number]	k4	2.0
TFSI	TFSI	kA	TFSI
-	-	kIx~	-
185	[number]	k4	185
kw	kw	kA	kw
(	(	kIx(	(
252	[number]	k4	252
PS	PS	kA	PS
<g/>
)	)	kIx)	)
3.0	[number]	k4	3.0
TFSI	TFSI	kA	TFSI
(	(	kIx(	(
<g/>
S	s	k7c7	s
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
-	-	kIx~	-
260	[number]	k4	260
kW	kW	kA	kW
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
354	[number]	k4	354
PS	PS	kA	PS
<g/>
)	)	kIx)	)
Diesel	diesel	k1gInSc1	diesel
<g/>
:	:	kIx,	:
2.0	[number]	k4	2.0
TDI	TDI	kA	TDI
-	-	kIx~	-
110	[number]	k4	110
kW	kW	kA	kW
(	(	kIx(	(
<g/>
150	[number]	k4	150
PS	PS	kA	PS
<g/>
)	)	kIx)	)
2.0	[number]	k4	2.0
TDI	TDI	kA	TDI
-	-	kIx~	-
140	[number]	k4	140
kW	kW	kA	kW
(	(	kIx(	(
<g/>
190	[number]	k4	190
PS	PS	kA	PS
<g/>
)	)	kIx)	)
3.0	[number]	k4	3.0
TDI	TDI	kA	TDI
-	-	kIx~	-
200	[number]	k4	200
kW	kW	kA	kW
(	(	kIx(	(
<g/>
272	[number]	k4	272
PS	PS	kA	PS
<g/>
)	)	kIx)	)
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Audi	Audi	k1gNnSc2	Audi
A4	A4	k1gFnSc2	A4
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Stránky	stránka	k1gFnSc2	stránka
výrobce	výrobce	k1gMnSc1	výrobce
<g/>
:	:	kIx,	:
Zkušenosti	zkušenost	k1gFnPc1	zkušenost
uživatelů	uživatel	k1gMnPc2	uživatel
s	s	k7c7	s
Audi	Audi	k1gNnSc7	Audi
A4	A4	k1gFnSc2	A4
</s>
