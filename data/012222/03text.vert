<p>
<s>
Steradián	steradián	k1gInSc1	steradián
(	(	kIx(	(
<g/>
sr	sr	k?	sr
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
SI	si	k1gNnPc2	si
jednotka	jednotka	k1gFnSc1	jednotka
prostorového	prostorový	k2eAgInSc2d1	prostorový
úhlu	úhel	k1gInSc2	úhel
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
z	z	k7c2	z
řeckého	řecký	k2eAgInSc2d1	řecký
stereos	stereosa	k1gFnPc2	stereosa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
definována	definovat	k5eAaBmNgFnS	definovat
jako	jako	k8xS	jako
prostorový	prostorový	k2eAgInSc4d1	prostorový
úhel	úhel	k1gInSc4	úhel
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vymezí	vymezit	k5eAaPmIp3nS	vymezit
ze	z	k7c2	z
středu	střed	k1gInSc2	střed
na	na	k7c6	na
jednotkové	jednotkový	k2eAgFnSc6d1	jednotková
kouli	koule	k1gFnSc6	koule
jednotkovou	jednotkový	k2eAgFnSc4d1	jednotková
plochu	plocha	k1gFnSc4	plocha
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
na	na	k7c4	na
kouli	koule	k1gFnSc4	koule
o	o	k7c6	o
poloměru	poloměr	k1gInSc6	poloměr
r	r	kA	r
plochu	plocha	k1gFnSc4	plocha
r2	r2	k4	r2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Steradián	steradián	k1gInSc1	steradián
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
podobě	podoba	k1gFnSc6	podoba
SI	si	k1gNnSc2	si
bezrozměrná	bezrozměrný	k2eAgFnSc1d1	bezrozměrná
odvozená	odvozený	k2eAgFnSc1d1	odvozená
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
astronomii	astronomie	k1gFnSc6	astronomie
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
steradiánu	steradián	k1gInSc2	steradián
používá	používat	k5eAaImIp3nS	používat
také	také	k9	také
starší	starý	k2eAgFnSc1d2	starší
jednotka	jednotka	k1gFnSc1	jednotka
čtverečný	čtverečný	k2eAgInSc1d1	čtverečný
stupeň	stupeň	k1gInSc1	stupeň
(	(	kIx(	(
<g/>
1	[number]	k4	1
sr	sr	k?	sr
≈	≈	k?	≈
3282,80635	[number]	k4	3282,80635
čtverečných	čtverečný	k2eAgInPc2d1	čtverečný
stupňů	stupeň	k1gInPc2	stupeň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prostorový	prostorový	k2eAgInSc1d1	prostorový
úhel	úhel	k1gInSc1	úhel
odpovídající	odpovídající	k2eAgInSc1d1	odpovídající
celé	celý	k2eAgFnSc3d1	celá
kouli	koule	k1gFnSc3	koule
je	být	k5eAaImIp3nS	být
4	[number]	k4	4
<g/>
π	π	k?	π
≈	≈	k?	≈
12,6	[number]	k4	12,6
sr	sr	k?	sr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poznámky	poznámka	k1gFnSc2	poznámka
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
