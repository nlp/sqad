<s>
Saxofon	saxofon	k1gInSc1
</s>
<s>
Saxofon	saxofon	k1gInSc1
</s>
<s>
italsky	italsky	k6eAd1
sassofononěmecky	sassofononěmecky	k6eAd1
Saxophonanglicky	Saxophonanglicky	k1gFnPc1
saxophonefrancouzsky	saxophonefrancouzsky	k6eAd1
saxophone	saxophon	k1gMnSc5
Klasifikace	klasifikace	k1gFnSc1
</s>
<s>
Podle	podle	k7c2
orchestrální	orchestrální	k2eAgFnSc2d1
praxe	praxe	k1gFnSc2
<g/>
:	:	kIx,
</s>
<s>
Dřevěné	dřevěný	k2eAgInPc1d1
nástroje	nástroj	k1gInPc1
</s>
<s>
Podle	podle	k7c2
způsobu	způsob	k1gInSc2
vzniku	vznik	k1gInSc2
tónu	tón	k1gInSc2
<g/>
:	:	kIx,
</s>
<s>
Aerofony	Aerofon	k1gInPc1
Tónový	tónový	k2eAgInSc1d1
rozsah	rozsah	k1gInSc4
</s>
<s>
znějící	znějící	k2eAgInSc4d1
tónový	tónový	k2eAgInSc4d1
rozsah	rozsah	k1gInSc4
jednotlivých	jednotlivý	k2eAgInPc2d1
typů	typ	k1gInPc2
saxofonů	saxofon	k1gInPc2
se	se	k3xPyFc4
liší	lišit	k5eAaImIp3nS
<g/>
,	,	kIx,
viz	vidět	k5eAaImRp2nS
(	(	kIx(
<g/>
sopránový	sopránový	k2eAgInSc1d1
<g/>
,	,	kIx,
altový	altový	k2eAgInSc1d1
<g/>
,	,	kIx,
tenorový	tenorový	k2eAgInSc1d1
<g/>
,	,	kIx,
barytonový	barytonový	k2eAgMnSc1d1
<g/>
)	)	kIx)
Příbuzné	příbuzný	k2eAgInPc4d1
nástroje	nástroj	k1gInPc4
</s>
<s>
Klarinet	klarinet	k1gInSc1
<g/>
,	,	kIx,
Basetový	basetový	k2eAgInSc1d1
roh	roh	k1gInSc1
</s>
<s>
Saxofon	saxofon	k1gInSc1
je	být	k5eAaImIp3nS
jednoplátkový	jednoplátkový	k2eAgInSc1d1
dřevěný	dřevěný	k2eAgInSc1d1
dechový	dechový	k2eAgInSc1d1
nástroj	nástroj	k1gInSc1
vyráběný	vyráběný	k2eAgInSc1d1
převážně	převážně	k6eAd1
z	z	k7c2
mosazného	mosazný	k2eAgInSc2d1
plechu	plech	k1gInSc2
(	(	kIx(
<g/>
profesionální	profesionální	k2eAgInPc4d1
modely	model	k1gInPc4
také	také	k9
ze	z	k7c2
stříbra	stříbro	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
především	především	k9
v	v	k7c6
jazzu	jazz	k1gInSc6
(	(	kIx(
<g/>
jako	jako	k8xC,k8xS
sólový	sólový	k2eAgInSc4d1
nástroj	nástroj	k1gInSc4
i	i	k9
jako	jako	k8xS,k8xC
součást	součást	k1gFnSc1
jazzových	jazzový	k2eAgMnPc2d1
komorních	komorní	k1gMnPc2
souborů	soubor	k1gInPc2
či	či	k8xC
orchestrů	orchestr	k1gInPc2
<g/>
)	)	kIx)
a	a	k8xC
v	v	k7c6
stále	stále	k6eAd1
větší	veliký	k2eAgFnSc6d2
míře	míra	k1gFnSc6
i	i	k8xC
v	v	k7c6
klasické	klasický	k2eAgFnSc6d1
hudbě	hudba	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původním	původní	k2eAgInSc7d1
záměrem	záměr	k1gInSc7
však	však	k9
bylo	být	k5eAaImAgNnS
vytvořit	vytvořit	k5eAaPmF
nástroj	nástroj	k1gInSc4
vhodný	vhodný	k2eAgInSc4d1
pro	pro	k7c4
dechové	dechový	k2eAgInPc4d1
a	a	k8xC
vojenské	vojenský	k2eAgInPc4d1
orchestry	orchestr	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
rodiny	rodina	k1gFnSc2
dřevěných	dřevěný	k2eAgInPc2d1
dechových	dechový	k2eAgInPc2d1
nástrojů	nástroj	k1gInPc2
patří	patřit	k5eAaImIp3nS
kvůli	kvůli	k7c3
své	svůj	k3xOyFgFnSc3
stavbě	stavba	k1gFnSc3
a	a	k8xC
způsobu	způsob	k1gInSc3
jakým	jaký	k3yRgInSc7,k3yIgInSc7,k3yQgInSc7
se	se	k3xPyFc4
na	na	k7c4
něj	on	k3xPp3gMnSc4
hraje	hrát	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
od	od	k7c2
počátku	počátek	k1gInSc2
byly	být	k5eAaImAgInP
saxofony	saxofon	k1gInPc1
konstruovány	konstruovat	k5eAaImNgInP
výhradně	výhradně	k6eAd1
z	z	k7c2
kovu	kov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Saxofon	saxofon	k1gInSc1
vynalezl	vynaleznout	k5eAaPmAgInS
okolo	okolo	k7c2
roku	rok	k1gInSc2
1840	#num#	k4
belgický	belgický	k2eAgMnSc1d1
nástrojař	nástrojař	k1gMnSc1
<g/>
,	,	kIx,
flétnista	flétnista	k1gMnSc1
a	a	k8xC
klarinetista	klarinetista	k1gMnSc1
Adolphe	Adolphe	k1gMnSc1
Sax	sax	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoliv	ačkoliv	k8xS
saxofony	saxofon	k1gInPc1
hned	hned	k6eAd1
v	v	k7c6
několika	několik	k4yIc6
velikostech	velikost	k1gFnPc6
zkonstruoval	zkonstruovat	k5eAaPmAgInS
již	již	k6eAd1
na	na	k7c6
začátku	začátek	k1gInSc6
40	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
nástroj	nástroj	k1gInSc4
mu	on	k3xPp3gMnSc3
byl	být	k5eAaImAgMnS
patentován	patentován	k2eAgMnSc1d1
až	až	k6eAd1
28	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1846	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
na	na	k7c4
15	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nástroj	nástroj	k1gInSc1
byl	být	k5eAaImAgInS
poprvé	poprvé	k6eAd1
představen	představit	k5eAaPmNgInS
veřejnosti	veřejnost	k1gFnSc3
na	na	k7c6
výstavě	výstava	k1gFnSc6
v	v	k7c6
Bruselu	Brusel	k1gInSc6
v	v	k7c6
roce	rok	k1gInSc6
1841	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c4
basový	basový	k2eAgInSc4d1
saxofon	saxofon	k1gInSc4
laděný	laděný	k2eAgInSc4d1
v	v	k7c4
C.	C.	kA
Adolphe	Adolphe	k1gNnSc4
Sax	sax	k1gInSc1
také	také	k9
na	na	k7c6
počátku	počátek	k1gInSc6
40	#num#	k4
<g/>
.	.	kIx.
let	let	k1gInSc1
pořádal	pořádat	k5eAaImAgInS
soukromé	soukromý	k2eAgFnPc4d1
výstavy	výstava	k1gFnPc4
pro	pro	k7c4
pařížské	pařížský	k2eAgMnPc4d1
hudebníky	hudebník	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnSc7
původním	původní	k2eAgInSc7d1
záměrem	záměr	k1gInSc7
bylo	být	k5eAaImAgNnS
vytvořit	vytvořit	k5eAaPmF
14	#num#	k4
typů	typ	k1gInPc2
saxofonů	saxofon	k1gInPc2
<g/>
,	,	kIx,
realizovány	realizován	k2eAgFnPc1d1
byly	být	k5eAaImAgFnP
jen	jen	k9
některé	některý	k3yIgFnPc1
<g/>
.	.	kIx.
</s>
<s>
Není	být	k5eNaImIp3nS
známo	znám	k2eAgNnSc1d1
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
inspirovalo	inspirovat	k5eAaBmAgNnS
Saxe	sax	k1gInSc5
k	k	k7c3
vytvoření	vytvoření	k1gNnSc3
saxofonu	saxofon	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
s	s	k7c7
největší	veliký	k2eAgFnSc7d3
pravděpodobností	pravděpodobnost	k1gFnSc7
se	se	k3xPyFc4
pokusil	pokusit	k5eAaPmAgMnS
nasadit	nasadit	k5eAaPmF
klarinetovou	klarinetový	k2eAgFnSc4d1
hubičku	hubička	k1gFnSc4
na	na	k7c4
ofikleidu	ofikleida	k1gFnSc4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
vytvoří	vytvořit	k5eAaPmIp3nS
nástroj	nástroj	k1gInSc4
se	s	k7c7
saxofonovým	saxofonový	k2eAgInSc7d1
zvukem	zvuk	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Adolphe	Adolph	k1gInSc2
Sax	sax	k1gInSc1
kromě	kromě	k7c2
jiných	jiný	k2eAgInPc2d1
nástrojů	nástroj	k1gInPc2
konstruoval	konstruovat	k5eAaImAgMnS
na	na	k7c6
konci	konec	k1gInSc6
třicátých	třicátý	k4xOgNnPc2
let	léto	k1gNnPc2
i	i	k8xC
ofikleidy	ofikleida	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Saxovým	Saxův	k2eAgInSc7d1
záměrem	záměr	k1gInSc7
<g/>
,	,	kIx,
jasně	jasně	k6eAd1
stanoveným	stanovený	k2eAgInSc7d1
v	v	k7c6
jeho	jeho	k3xOp3gInPc6
spisech	spis	k1gInPc6
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
zkonstruovat	zkonstruovat	k5eAaPmF
zcela	zcela	k6eAd1
nový	nový	k2eAgInSc4d1
dechový	dechový	k2eAgInSc4d1
nástroj	nástroj	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
by	by	kYmCp3nS
zněl	znět	k5eAaImAgInS
dobře	dobře	k6eAd1
v	v	k7c6
nízkých	nízký	k2eAgFnPc6d1
polohách	poloha	k1gFnPc6
a	a	k8xC
který	který	k3yRgInSc4,k3yIgInSc4,k3yQgInSc4
by	by	kYmCp3nS
kombinoval	kombinovat	k5eAaImAgInS
dobrou	dobrý	k2eAgFnSc4d1
ovladatelnost	ovladatelnost	k1gFnSc4
dřevěných	dřevěný	k2eAgInPc2d1
nástrojů	nástroj	k1gInPc2
s	s	k7c7
masivním	masivní	k2eAgInSc7d1
a	a	k8xC
razantním	razantní	k2eAgInSc7d1
tónem	tón	k1gInSc7
žesťových	žesťový	k2eAgInPc2d1
nástrojů	nástroj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Adolphe	Adolph	k1gInSc2
Sax	sax	k1gInSc1
navrhl	navrhnout	k5eAaPmAgInS
celkově	celkově	k6eAd1
14	#num#	k4
typů	typ	k1gInPc2
saxofonů	saxofon	k1gInPc2
<g/>
,	,	kIx,
ale	ale	k8xC
některé	některý	k3yIgFnPc1
nebyly	být	k5eNaImAgFnP
dodnes	dodnes	k6eAd1
zkonstruovány	zkonstruován	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Nástroj	nástroj	k1gInSc1
byl	být	k5eAaImAgInS
původně	původně	k6eAd1
navržen	navrhnout	k5eAaPmNgInS
pro	pro	k7c4
potřeby	potřeba	k1gFnPc4
klasických	klasický	k2eAgInPc2d1
symfonických	symfonický	k2eAgInPc2d1
orchestrů	orchestr	k1gInPc2
<g/>
,	,	kIx,
skladatelé	skladatel	k1gMnPc1
ho	on	k3xPp3gMnSc4
ale	ale	k9
dlouhou	dlouhý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
prakticky	prakticky	k6eAd1
ignorovali	ignorovat	k5eAaImAgMnP
<g/>
;	;	kIx,
v	v	k7c6
menší	malý	k2eAgFnSc6d2
míře	míra	k1gFnSc6
se	se	k3xPyFc4
prosadil	prosadit	k5eAaPmAgMnS
jen	jen	k9
u	u	k7c2
vojenských	vojenský	k2eAgFnPc2d1
kapel	kapela	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Masově	masově	k6eAd1
se	se	k3xPyFc4
rozšířil	rozšířit	k5eAaPmAgInS
až	až	k9
od	od	k7c2
nástupu	nástup	k1gInSc2
jazzu	jazz	k1gInSc2
na	na	k7c6
přelomu	přelom	k1gInSc6
19	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Saxofon	saxofon	k1gInSc1
je	být	k5eAaImIp3nS
svými	svůj	k3xOyFgFnPc7
zvukovými	zvukový	k2eAgFnPc7d1
vlastnostmi	vlastnost	k1gFnPc7
pro	pro	k7c4
tento	tento	k3xDgInSc4
žánr	žánr	k1gInSc4
daleko	daleko	k6eAd1
vhodnější	vhodný	k2eAgInSc4d2
než	než	k8xS
pro	pro	k7c4
potřeby	potřeba	k1gFnPc4
vážné	vážný	k2eAgFnSc2d1
hudby	hudba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c4
dobu	doba	k1gFnSc4
platnosti	platnost	k1gFnSc2
patentu	patent	k1gInSc2
(	(	kIx(
<g/>
1846	#num#	k4
–	–	k?
1866	#num#	k4
<g/>
)	)	kIx)
nesměl	smět	k5eNaImAgInS
být	být	k5eAaImF
nástroj	nástroj	k1gInSc1
vyráběn	vyrábět	k5eAaImNgInS
ani	ani	k8xC
upravován	upravovat	k5eAaImNgInS
nikde	nikde	k6eAd1
jinde	jinde	k6eAd1
než	než	k8xS
v	v	k7c6
Saxově	Saxův	k2eAgFnSc6d1
továrně	továrna	k1gFnSc6
<g/>
,	,	kIx,
avšak	avšak	k8xC
tento	tento	k3xDgInSc4
i	i	k9
Saxovy	Saxův	k2eAgInPc1d1
četné	četný	k2eAgInPc1d1
další	další	k2eAgInPc1d1
patenty	patent	k1gInPc1
byly	být	k5eAaImAgInP
neustále	neustále	k6eAd1
porušovány	porušován	k2eAgInPc1d1
jeho	jeho	k3xOp3gMnSc7
soupeři	soupeř	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
vypršení	vypršení	k1gNnSc6
platnosti	platnost	k1gFnSc2
patentu	patent	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1866	#num#	k4
představilo	představit	k5eAaPmAgNnS
mnoho	mnoho	k4c1
různých	různý	k2eAgMnPc2d1
výrobců	výrobce	k1gMnPc2
své	svůj	k3xOyFgInPc4
modely	model	k1gInPc4
v	v	k7c6
různých	různý	k2eAgFnPc6d1
úpravách	úprava	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Stavba	stavba	k1gFnSc1
</s>
<s>
Hubička	hubička	k1gFnSc1
a	a	k8xC
plátek	plátek	k1gInSc1
tenorsaxofonu	tenorsaxofon	k1gInSc2
</s>
<s>
Schéma	schéma	k1gNnSc1
saxofonu	saxofon	k1gInSc2
</s>
<s>
Saxofon	saxofon	k1gInSc1
je	být	k5eAaImIp3nS
jednoplátkový	jednoplátkový	k2eAgInSc1d1
nástroj	nástroj	k1gInSc1
(	(	kIx(
<g/>
stejně	stejně	k6eAd1
jako	jako	k8xC,k8xS
klarinet	klarinet	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
trubici	trubice	k1gFnSc4
má	mít	k5eAaImIp3nS
ale	ale	k9
kónickou	kónický	k2eAgFnSc4d1
<g/>
,	,	kIx,
proto	proto	k8xC
se	se	k3xPyFc4
jeho	jeho	k3xOp3gFnSc2
akustické	akustický	k2eAgFnSc2d1
vlastnosti	vlastnost	k1gFnSc2
blíží	blížit	k5eAaImIp3nS
spíše	spíše	k9
hoboji	hoboj	k1gInSc3
(	(	kIx(
<g/>
klarinet	klarinet	k1gInSc1
má	mít	k5eAaImIp3nS
trubici	trubice	k1gFnSc4
válcovitého	válcovitý	k2eAgInSc2d1
tvaru	tvar	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
saxofonů	saxofon	k1gInPc2
má	mít	k5eAaImIp3nS
zahnutou	zahnutý	k2eAgFnSc4d1
trubici	trubice	k1gFnSc4
spíše	spíše	k9
z	z	k7c2
praktických	praktický	k2eAgInPc2d1
důvodů	důvod	k1gInPc2
(	(	kIx(
<g/>
sníží	snížit	k5eAaPmIp3nS
se	se	k3xPyFc4
tím	ten	k3xDgNnSc7
délka	délka	k1gFnSc1
nástroje	nástroj	k1gInPc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
na	na	k7c6
zvukové	zvukový	k2eAgFnSc6d1
vlastnosti	vlastnost	k1gFnSc6
má	mít	k5eAaImIp3nS
tento	tento	k3xDgInSc1
prvek	prvek	k1gInSc1
jen	jen	k9
malý	malý	k2eAgInSc4d1
vliv	vliv	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Saxofon	saxofon	k1gInSc1
přefukuje	přefukovat	k5eAaImIp3nS
do	do	k7c2
oktávy	oktáva	k1gFnSc2
(	(	kIx(
<g/>
stejně	stejně	k6eAd1
jako	jako	k8xC,k8xS
hoboj	hoboj	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
klarinet	klarinet	k1gInSc1
přefukuje	přefukovat	k5eAaImIp3nS
do	do	k7c2
duodecimy	duodecima	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hmatový	hmatový	k2eAgInSc1d1
systém	systém	k1gInSc1
je	být	k5eAaImIp3nS
Boehmův	Boehmův	k2eAgInSc1d1
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
u	u	k7c2
klarinetu	klarinet	k1gInSc2
nebo	nebo	k8xC
příčné	příčný	k2eAgFnSc2d1
flétny	flétna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hmaty	hmat	k1gInPc4
se	se	k3xPyFc4
však	však	k9
liší	lišit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Téměř	téměř	k6eAd1
všechny	všechen	k3xTgInPc1
saxofony	saxofon	k1gInPc1
jsou	být	k5eAaImIp3nP
vyráběny	vyrábět	k5eAaImNgInP
z	z	k7c2
mosazi	mosaz	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následně	následně	k6eAd1
se	se	k3xPyFc4
nástroj	nástroj	k1gInSc1
nalakuje	nalakovat	k5eAaPmIp3nS
nebo	nebo	k8xC
pokoví	pokovit	k5eAaPmIp3nS
(	(	kIx(
<g/>
stříbrem	stříbro	k1gNnSc7
či	či	k8xC
zlatem	zlato	k1gNnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
sníží	snížit	k5eAaPmIp3nS
výskyt	výskyt	k1gInSc1
korozi	koroze	k1gFnSc4
a	a	k8xC
zlepší	zlepšit	k5eAaPmIp3nS
se	se	k3xPyFc4
jeho	jeho	k3xOp3gFnSc2
zvukové	zvukový	k2eAgFnSc2d1
vlastnosti	vlastnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současnosti	současnost	k1gFnSc6
jsou	být	k5eAaImIp3nP
oblíbené	oblíbený	k2eAgFnPc1d1
také	také	k9
laky	laka	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
novému	nový	k2eAgMnSc3d1
saxofonu	saxofon	k1gInSc6
propůjčují	propůjčovat	k5eAaImIp3nP
starý	starý	k2eAgInSc4d1
a	a	k8xC
omšelý	omšelý	k2eAgInSc4d1
vzhled	vzhled	k1gInSc4
či	či	k8xC
různé	různý	k2eAgFnPc4d1
barevné	barevný	k2eAgFnPc4d1
kombinace	kombinace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ten	k3xDgInPc4
však	však	k9
ovlivňují	ovlivňovat	k5eAaImIp3nP
zvuk	zvuk	k1gInSc1
spíše	spíše	k9
negativně	negativně	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Stejně	stejně	k6eAd1
jako	jako	k9
v	v	k7c6
případě	případ	k1gInSc6
klarinetu	klarinet	k1gInSc2
má	mít	k5eAaImIp3nS
saxofon	saxofon	k1gInSc1
plátek	plátek	k1gInSc1
připevněný	připevněný	k2eAgInSc1d1
na	na	k7c4
hubičku	hubička	k1gFnSc4
<g/>
,	,	kIx,
každý	každý	k3xTgInSc1
typ	typ	k1gInSc1
saxofonu	saxofon	k1gInSc2
používá	používat	k5eAaImIp3nS
jinou	jiný	k2eAgFnSc4d1
velikost	velikost	k1gFnSc4
hubice	hubice	k1gFnSc2
<g/>
,	,	kIx,
ligatury	ligatura	k1gFnSc2
i	i	k8xC
plátku	plátek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
hráčů	hráč	k1gMnPc2
používá	používat	k5eAaImIp3nS
tradiční	tradiční	k2eAgInPc4d1
plátky	plátek	k1gInPc4
z	z	k7c2
trstě	trsť	k1gFnSc2
rákosovité	rákosovitý	k2eAgFnSc2d1
(	(	kIx(
<g/>
Arundo	Arundo	k6eAd1
donax	donax	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
existují	existovat	k5eAaImIp3nP
však	však	k9
i	i	k9
syntetické	syntetický	k2eAgFnPc1d1
varianty	varianta	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
se	se	k3xPyFc4
vyznačují	vyznačovat	k5eAaImIp3nP
vyšší	vysoký	k2eAgFnSc7d2
trvanlivostí	trvanlivost	k1gFnSc7
a	a	k8xC
odolností	odolnost	k1gFnSc7
<g/>
,	,	kIx,
za	za	k7c4
to	ten	k3xDgNnSc4
neposkytují	poskytovat	k5eNaImIp3nP
stejné	stejný	k2eAgFnPc4d1
zvukové	zvukový	k2eAgFnPc4d1
vlastnosti	vlastnost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Systém	systém	k1gInSc1
klapek	klapka	k1gFnPc2
</s>
<s>
Přibližný	přibližný	k2eAgInSc1d1
rozsah	rozsah	k1gInSc1
saxofonu	saxofon	k1gInSc2
je	být	k5eAaImIp3nS
asi	asi	k9
dvě	dva	k4xCgFnPc4
a	a	k8xC
půl	půl	k1xP
oktávy	oktáva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgInPc4
tóny	tón	k1gInPc4
nad	nad	k7c4
tuto	tento	k3xDgFnSc4
hranici	hranice	k1gFnSc4
jsou	být	k5eAaImIp3nP
součástí	součást	k1gFnSc7
přefukovaného	přefukovaný	k2eAgInSc2d1
rejstříku	rejstřík	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
změně	změna	k1gFnSc3
tónů	tón	k1gInPc2
dochází	docházet	k5eAaImIp3nS
díky	díky	k7c3
důmyslnému	důmyslný	k2eAgInSc3d1
systému	systém	k1gInSc3
otvorů	otvor	k1gInPc2
a	a	k8xC
klapek	klapka	k1gFnPc2
–	–	k?
saxofon	saxofon	k1gInSc1
má	mít	k5eAaImIp3nS
mezi	mezi	k7c7
21	#num#	k4
a	a	k8xC
23	#num#	k4
klapkami	klapka	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
zmáčknutí	zmáčknutí	k1gNnSc6
klapky	klapka	k1gFnSc2
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
otevření	otevření	k1gNnSc3
nebo	nebo	k8xC
uzavření	uzavření	k1gNnSc4
jednoho	jeden	k4xCgMnSc2
či	či	k8xC
více	hodně	k6eAd2
otvorů	otvor	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počet	počet	k1gInSc1
klapek	klapka	k1gFnPc2
záleží	záležet	k5eAaImIp3nS
na	na	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
zda	zda	k8xS
má	mít	k5eAaImIp3nS
nástroj	nástroj	k1gInSc4
klapku	klapka	k1gFnSc4
pro	pro	k7c4
Fis³	Fis³	k1gFnSc4
<g/>
,	,	kIx,
G³	G³	k1gFnSc1
a	a	k8xC
malé	malý	k2eAgNnSc1d1
a.	a.	k?
Fis	fis	k1gNnSc1
klapka	klapka	k1gFnSc1
začala	začít	k5eAaPmAgFnS
být	být	k5eAaImF
od	od	k7c2
roku	rok	k1gInSc2
1970	#num#	k4
běžnou	běžný	k2eAgFnSc7d1
součástí	součást	k1gFnSc7
nástrojů	nástroj	k1gInPc2
pro	pro	k7c4
pokročilé	pokročilý	k2eAgMnPc4d1
hráče	hráč	k1gMnPc4
a	a	k8xC
profesionály	profesionál	k1gMnPc4
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
G	G	kA
klapku	klapka	k1gFnSc4
můžeme	moct	k5eAaImIp1nP
nalézt	nalézt	k5eAaBmF,k5eAaPmF
pouze	pouze	k6eAd1
na	na	k7c6
několika	několik	k4yIc6
moderních	moderní	k2eAgInPc6d1
sopránsaxofonech	sopránsaxofon	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Několik	několik	k4yIc4
modelů	model	k1gInPc2
barytonsaxofonů	barytonsaxofon	k1gInPc2
má	mít	k5eAaImIp3nS
přídavný	přídavný	k2eAgInSc4d1
otvor	otvor	k1gInSc4
a	a	k8xC
klapku	klapka	k1gFnSc4
pro	pro	k7c4
malé	malý	k2eAgNnSc4d1
a.	a.	k?
</s>
<s>
Materiály	materiál	k1gInPc4
</s>
<s>
Téměř	téměř	k6eAd1
všechny	všechen	k3xTgInPc1
saxofony	saxofon	k1gInPc1
jsou	být	k5eAaImIp3nP
vyrobeny	vyrobit	k5eAaPmNgInP
z	z	k7c2
mosazi	mosaz	k1gFnSc2
–	–	k?
nejsou	být	k5eNaImIp3nP
však	však	k9
řazeny	řazen	k2eAgInPc1d1
mezi	mezi	k7c4
žesťové	žesťový	k2eAgInPc4d1
nástroje	nástroj	k1gInPc4
<g/>
,	,	kIx,
ale	ale	k8xC
mezi	mezi	k7c4
dřevěné	dřevěný	k2eAgInPc4d1
dechové	dechový	k2eAgInPc4d1
nástroje	nástroj	k1gInPc4
<g/>
,	,	kIx,
protože	protože	k8xS
zařazení	zařazení	k1gNnSc1
není	být	k5eNaImIp3nS
v	v	k7c6
tomto	tento	k3xDgInSc6
případě	případ	k1gInSc6
podmíněno	podmínit	k5eAaPmNgNnS
materiálem	materiál	k1gInSc7
<g/>
,	,	kIx,
ale	ale	k8xC
způsobem	způsob	k1gInSc7
tvorby	tvorba	k1gFnSc2
tónu	tón	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Výrobci	výrobce	k1gMnPc1
zkoušeli	zkoušet	k5eAaImAgMnP
i	i	k9
jiné	jiný	k2eAgInPc4d1
materiály	materiál	k1gInPc4
<g/>
,	,	kIx,
s	s	k7c7
různým	různý	k2eAgNnSc7d1
procentem	procento	k1gNnSc7
úspěchu	úspěch	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
padesátých	padesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
například	například	k6eAd1
společnost	společnost	k1gFnSc1
Grafton	Grafton	k1gInSc1
vyrobila	vyrobit	k5eAaPmAgFnS
saxofony	saxofon	k1gInPc4
z	z	k7c2
plastu	plast	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Několik	několik	k4yIc1
společností	společnost	k1gFnPc2
<g/>
,	,	kIx,
mezi	mezi	k7c7
nimi	on	k3xPp3gInPc7
např.	např.	kA
Yanagisawa	Yanagisawum	k1gNnPc4
<g/>
,	,	kIx,
vyrobilo	vyrobit	k5eAaPmAgNnS
několik	několik	k4yIc4
saxofonů	saxofon	k1gInPc2
z	z	k7c2
bronzu	bronz	k1gInSc2
<g/>
,	,	kIx,
díky	díky	k7c3
kterému	který	k3yIgNnSc3,k3yRgNnSc3,k3yQgNnSc3
má	mít	k5eAaImIp3nS
prý	prý	k9
nástroj	nástroj	k1gInSc1
plnější	plný	k2eAgInSc4d2
tón	tón	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
výrobci	výrobce	k1gMnPc1
dělají	dělat	k5eAaImIp3nP
esa	eso	k1gNnPc4
saxofonů	saxofon	k1gInPc2
nebo	nebo	k8xC
celé	celý	k2eAgInPc4d1
nástroje	nástroj	k1gInPc4
z	z	k7c2
mincovního	mincovní	k2eAgNnSc2d1
stříbra	stříbro	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Bývá	bývat	k5eAaImIp3nS
používáno	používán	k2eAgNnSc1d1
také	také	k9
niklové	niklový	k2eAgNnSc1d1
stříbro	stříbro	k1gNnSc1
<g/>
,	,	kIx,
z	z	k7c2
kterého	který	k3yRgInSc2,k3yIgInSc2,k3yQgInSc2
vyrábějí	vyrábět	k5eAaImIp3nP
nástroje	nástroj	k1gInPc1
např.	např.	kA
Selmer	Selmer	k1gMnSc1
<g/>
,	,	kIx,
Yanagisawa	Yanagisawa	k1gMnSc1
<g/>
,	,	kIx,
P.	P.	kA
Mauriat	Mauriat	k1gInSc1
či	či	k8xC
Keilwerth	Keilwerth	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
někoho	někdo	k3yInSc2
mají	mít	k5eAaImIp3nP
tyto	tento	k3xDgInPc1
nástroje	nástroj	k1gInPc1
jasnější	jasný	k2eAgInPc1d2
a	a	k8xC
silnější	silný	k2eAgInSc1d2
zvuk	zvuk	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Povrchová	povrchový	k2eAgFnSc1d1
úprava	úprava	k1gFnSc1
</s>
<s>
Když	když	k8xS
je	být	k5eAaImIp3nS
nástroj	nástroj	k1gInSc1
hotový	hotový	k2eAgInSc1d1
<g/>
,	,	kIx,
aplikuje	aplikovat	k5eAaBmIp3nS
výrobce	výrobce	k1gMnSc1
na	na	k7c4
holou	holý	k2eAgFnSc4d1
mosaz	mosaz	k1gFnSc4
slabý	slabý	k2eAgInSc4d1
nátěr	nátěr	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lak	laka	k1gFnPc2
nebo	nebo	k8xC
pokovení	pokovení	k1gNnSc2
(	(	kIx(
<g/>
stříbro	stříbro	k1gNnSc1
<g/>
,	,	kIx,
zlato	zlato	k1gNnSc1
<g/>
)	)	kIx)
slouží	sloužit	k5eAaImIp3nS
k	k	k7c3
ochraně	ochrana	k1gFnSc3
mědi	měď	k1gFnSc2
před	před	k7c7
korozí	koroze	k1gFnSc7
a	a	k8xC
navíc	navíc	k6eAd1
dotváří	dotvářet	k5eAaImIp3nS
atraktivní	atraktivní	k2eAgNnPc4d1
vzezření	vzezření	k1gNnPc4
nástroje	nástroj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
let	léto	k1gNnPc2
bylo	být	k5eAaImAgNnS
používáno	používat	k5eAaImNgNnS
mnoho	mnoho	k4c1
různých	různý	k2eAgInPc2d1
druhů	druh	k1gInPc2
a	a	k8xC
barev	barva	k1gFnPc2
povrchové	povrchový	k2eAgFnSc2d1
úpravy	úprava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Hubice	hubice	k1gFnSc1
</s>
<s>
Saxofonová	saxofonový	k2eAgFnSc1d1
hubička	hubička	k1gFnSc1
je	být	k5eAaImIp3nS
podobná	podobný	k2eAgFnSc1d1
hubičce	hubička	k1gFnSc3
klarinetu	klarinet	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hubičky	hubička	k1gFnSc2
se	se	k3xPyFc4
vyrábí	vyrábět	k5eAaImIp3nS
z	z	k7c2
mnoha	mnoho	k4c2
různých	různý	k2eAgInPc2d1
materiálů	materiál	k1gInPc2
<g/>
,	,	kIx,
nejčastěji	často	k6eAd3
však	však	k9
z	z	k7c2
ebonitu	ebonit	k1gInSc2
<g/>
,	,	kIx,
plastu	plast	k1gInSc2
nebo	nebo	k8xC
kovu	kov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Méně	málo	k6eAd2
běžnými	běžný	k2eAgInPc7d1
materiály	materiál	k1gInPc7
jsou	být	k5eAaImIp3nP
například	například	k6eAd1
sklo	sklo	k1gNnSc1
<g/>
,	,	kIx,
dřevo	dřevo	k1gNnSc1
<g/>
,	,	kIx,
křišťál	křišťál	k1gInSc1
nebo	nebo	k8xC
dokonce	dokonce	k9
kost	kost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kovové	kovový	k2eAgFnPc4d1
hubičky	hubička	k1gFnPc4
mají	mít	k5eAaImIp3nP
jasnější	jasný	k2eAgInSc1d2
a	a	k8xC
ostřejší	ostrý	k2eAgInSc1d2
tón	tón	k1gInSc1
než	než	k8xS
běžné	běžný	k2eAgFnPc1d1
hubičky	hubička	k1gFnPc1
vyráběné	vyráběný	k2eAgFnPc1d1
z	z	k7c2
ebonitu	ebonit	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
saxofonisté	saxofonista	k1gMnPc1
se	se	k3xPyFc4
domnívají	domnívat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
plastové	plastový	k2eAgFnPc1d1
hubičky	hubička	k1gFnPc1
netvoří	tvořit	k5eNaImIp3nP
dobrý	dobrý	k2eAgInSc4d1
tón	tón	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiní	jiný	k1gMnPc1
tvrdí	tvrdit	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
na	na	k7c6
materiálu	materiál	k1gInSc6
hubičky	hubička	k1gFnSc2
nezáleží	záležet	k5eNaImIp3nS
a	a	k8xC
na	na	k7c4
tón	tón	k1gInSc4
mají	mít	k5eAaImIp3nP
vliv	vliv	k1gInSc4
spíše	spíše	k9
fyzické	fyzický	k2eAgFnPc4d1
proporce	proporce	k1gFnPc4
jedince	jedinec	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
názory	názor	k1gInPc1
říkají	říkat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
pro	pro	k7c4
barvu	barva	k1gFnSc4
tónu	tón	k1gInSc2
není	být	k5eNaImIp3nS
rozhodující	rozhodující	k2eAgInSc4d1
materiál	materiál	k1gInSc4
hubičky	hubička	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
její	její	k3xOp3gInSc4
tvar	tvar	k1gInSc4
a	a	k8xC
provedení	provedení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Plátek	plátek	k1gInSc1
</s>
<s>
Ke	k	k7c3
hře	hra	k1gFnSc3
na	na	k7c4
saxofon	saxofon	k1gInSc4
je	být	k5eAaImIp3nS
potřeba	potřeba	k6eAd1
plátek	plátek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Saxofonový	saxofonový	k2eAgInSc1d1
plátek	plátek	k1gInSc1
je	být	k5eAaImIp3nS
podobný	podobný	k2eAgInSc1d1
jako	jako	k8xC,k8xS
plátek	plátek	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
na	na	k7c4
klarinet	klarinet	k1gInSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
ovšem	ovšem	k9
širší	široký	k2eAgFnSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každý	každý	k3xTgInSc1
typ	typ	k1gInSc1
saxofonu	saxofon	k1gInSc2
(	(	kIx(
<g/>
alt	alt	k1gInSc1
<g/>
,	,	kIx,
tenor	tenor	k1gInSc1
<g/>
,	,	kIx,
<g/>
…	…	k?
<g/>
)	)	kIx)
má	mít	k5eAaImIp3nS
plátek	plátek	k1gInSc1
jinak	jinak	k6eAd1
veliký	veliký	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plátky	plátek	k1gInPc7
jsou	být	k5eAaImIp3nP
běžně	běžně	k6eAd1
dostupné	dostupný	k2eAgFnPc1d1
v	v	k7c6
různých	různý	k2eAgInPc6d1
stylech	styl	k1gInPc6
<g/>
,	,	kIx,
tvrdostech	tvrdost	k1gFnPc6
a	a	k8xC
také	také	k9
od	od	k7c2
různých	různý	k2eAgMnPc2d1
výrobců	výrobce	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každý	každý	k3xTgMnSc1
hráč	hráč	k1gMnSc1
zkouší	zkoušet	k5eAaImIp3nS
plátky	plátek	k1gInPc4
různě	různě	k6eAd1
silné	silný	k2eAgNnSc4d1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
zjistil	zjistit	k5eAaPmAgMnS
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
tvrdost	tvrdost	k1gFnSc1
je	být	k5eAaImIp3nS
pro	pro	k7c4
jeho	jeho	k3xOp3gNnPc4
ústa	ústa	k1gNnPc4
a	a	k8xC
styl	styl	k1gInSc4
hry	hra	k1gFnSc2
nejvhodnější	vhodný	k2eAgNnSc1d3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvrdost	tvrdost	k1gFnSc1
plátků	plátek	k1gInPc2
je	být	k5eAaImIp3nS
v	v	k7c6
rozmezí	rozmezí	k1gNnSc6
od	od	k7c2
1	#num#	k4
(	(	kIx(
<g/>
měkké	měkký	k2eAgFnSc2d1
<g/>
)	)	kIx)
do	do	k7c2
5	#num#	k4
(	(	kIx(
<g/>
tvrdé	tvrdý	k2eAgFnSc2d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stupnice	stupnice	k1gFnSc1
není	být	k5eNaImIp3nS
standardizována	standardizován	k2eAgFnSc1d1
<g/>
;	;	kIx,
proto	proto	k8xC
je	být	k5eAaImIp3nS
například	například	k6eAd1
plátek	plátek	k1gInSc4
značky	značka	k1gFnSc2
Rico	Rico	k6eAd1
o	o	k7c6
tvrdosti	tvrdost	k1gFnSc6
3	#num#	k4
měkčí	měkčit	k5eAaImIp3nS
než	než	k8xS
stejně	stejně	k6eAd1
tvrdý	tvrdý	k2eAgInSc1d1
plátek	plátek	k1gInSc1
značky	značka	k1gFnSc2
Vandoren	Vandorna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Typy	typ	k1gInPc1
saxofonů	saxofon	k1gInPc2
</s>
<s>
Jay	Jay	k?
C.	C.	kA
Easton	Easton	k1gInSc1
s	s	k7c7
deseti	deset	k4xCc7
zástupci	zástupce	k1gMnPc7
saxonové	saxonový	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
největšímu	veliký	k2eAgInSc3d3
k	k	k7c3
nejmenšímu	malý	k2eAgMnSc3d3
<g/>
:	:	kIx,
kontrabasový	kontrabasový	k2eAgMnSc1d1
<g/>
,	,	kIx,
basový	basový	k2eAgMnSc1d1
<g/>
,	,	kIx,
barytonový	barytonový	k2eAgMnSc1d1
<g/>
,	,	kIx,
tenorový	tenorový	k2eAgMnSc1d1
<g/>
,	,	kIx,
tenorový	tenorový	k2eAgMnSc1d1
in	in	k?
C	C	kA
<g/>
,	,	kIx,
altový	altový	k2eAgMnSc1d1
<g/>
,	,	kIx,
mezzosopránový	mezzosopránový	k2eAgMnSc1d1
in	in	k?
F	F	kA
<g/>
,	,	kIx,
sopránový	sopránový	k2eAgMnSc1d1
<g/>
,	,	kIx,
sopránový	sopránový	k2eAgMnSc1d1
in	in	k?
C	C	kA
<g/>
,	,	kIx,
sopraninový	sopraninový	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Vynález	vynález	k1gInSc1
a	a	k8xC
typy	typ	k1gInPc1
</s>
<s>
Adolphe	Adolphe	k6eAd1
Sax	sax	k1gInSc1
patentoval	patentovat	k5eAaBmAgInS
2	#num#	k4
rodiny	rodina	k1gFnSc2
saxofonů	saxofon	k1gInPc2
<g/>
:	:	kIx,
</s>
<s>
orchestrální	orchestrální	k2eAgNnSc4d1
(	(	kIx(
<g/>
ladění	ladění	k1gNnSc4
v	v	k7c6
C	C	kA
a	a	k8xC
v	v	k7c6
F	F	kA
<g/>
)	)	kIx)
a	a	k8xC
</s>
<s>
vojenskou	vojenský	k2eAgFnSc4d1
(	(	kIx(
<g/>
ladění	ladění	k1gNnSc4
v	v	k7c6
B	B	kA
a	a	k8xC
v	v	k7c6
Es	es	k1gNnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Každá	každý	k3xTgFnSc1
rodina	rodina	k1gFnSc1
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
ze	z	k7c2
sedmi	sedm	k4xCc2
základních	základní	k2eAgInPc2d1
nástrojů	nástroj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
názevtyp	názevtyp	k1gMnSc1
</s>
<s>
ladění	ladění	k1gNnSc4
v	v	k7c6
tónině	tónina	k1gFnSc6
(	(	kIx(
<g/>
in	in	k?
<g/>
)	)	kIx)
</s>
<s>
orchestrálnívojenské	orchestrálnívojenský	k2eAgNnSc1d1
</s>
<s>
sopranissimový	sopranissimový	k2eAgMnSc1d1
<g/>
,	,	kIx,
<g/>
novinka	novinka	k1gFnSc1
</s>
<s>
C	C	kA
</s>
<s>
soprillo	soprillo	k1gNnSc1
<g/>
,	,	kIx,
B	B	kA
</s>
<s>
sopraninový	sopraninový	k2eAgInSc1d1
</s>
<s>
F	F	kA
</s>
<s>
Es	es	k1gNnSc1
<g/>
,	,	kIx,
sopraninosaxofon	sopraninosaxofon	k1gInSc1
</s>
<s>
sopránový	sopránový	k2eAgInSc1d1
</s>
<s>
sopránový	sopránový	k2eAgMnSc1d1
in	in	k?
C	C	kA
</s>
<s>
sopránsaxofonin	sopránsaxofonin	k1gInSc1
B	B	kA
<g/>
,	,	kIx,
běžný	běžný	k2eAgMnSc1d1
</s>
<s>
altový	altový	k2eAgInSc1d1
</s>
<s>
altový	altový	k2eAgMnSc1d1
in	in	k?
F	F	kA
<g/>
(	(	kIx(
<g/>
mezzosopránový	mezzosopránový	k2eAgMnSc1d1
in	in	k?
F	F	kA
<g/>
)	)	kIx)
</s>
<s>
altsaxofonEs	altsaxofonEs	k1gInSc1
<g/>
,	,	kIx,
běžný	běžný	k2eAgInSc1d1
</s>
<s>
tenorový	tenorový	k2eAgInSc1d1
</s>
<s>
tenorsaxofon	tenorsaxofon	k1gInSc1
in	in	k?
C	C	kA
<g/>
,	,	kIx,
<g/>
C-melody	C-meloda	k1gFnSc2
saxophone	saxophon	k1gMnSc5
</s>
<s>
tenorsaxofonB	tenorsaxofonB	k?
<g/>
,	,	kIx,
běžný	běžný	k2eAgInSc1d1
</s>
<s>
barytonový	barytonový	k2eAgInSc1d1
</s>
<s>
F	F	kA
</s>
<s>
barytonsaxofonEs	barytonsaxofonEs	k1gInSc1
<g/>
,	,	kIx,
běžný	běžný	k2eAgInSc1d1
</s>
<s>
basový	basový	k2eAgInSc1d1
</s>
<s>
C	C	kA
</s>
<s>
B	B	kA
</s>
<s>
kontrabasový	kontrabasový	k2eAgInSc1d1
</s>
<s>
F	F	kA
</s>
<s>
Es	es	k1gNnSc1
</s>
<s>
subkontrabasový	subkontrabasový	k2eAgMnSc1d1
<g/>
,	,	kIx,
<g/>
nikdy	nikdy	k6eAd1
nerealizován	realizován	k2eNgInSc1d1
</s>
<s>
C	C	kA
</s>
<s>
B	B	kA
</s>
<s>
Sax	sax	k1gInSc1
také	také	k9
plánoval	plánovat	k5eAaImAgInS
postavit	postavit	k5eAaPmF
subkontrabasový	subkontrabasový	k2eAgInSc1d1
(	(	kIx(
<g/>
bourdonový	bourdonový	k2eAgInSc1d1
<g/>
)	)	kIx)
saxofon	saxofon	k1gInSc1
<g/>
,	,	kIx,
ale	ale	k8xC
k	k	k7c3
jeho	jeho	k3xOp3gFnSc3
realizaci	realizace	k1gFnSc3
nikdy	nikdy	k6eAd1
nedošlo	dojít	k5eNaPmAgNnS
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
nedávné	dávný	k2eNgFnSc6d1
době	doba	k1gFnSc6
byl	být	k5eAaImAgInS
postaven	postaven	k2eAgInSc4d1
sopranissimový	sopranissimový	k2eAgInSc4d1
saxofon	saxofon	k1gInSc4
(	(	kIx(
<g/>
Sax	sax	k1gInSc4
ho	on	k3xPp3gInSc4
nenavrhoval	navrhovat	k5eNaImAgInS
z	z	k7c2
důvodů	důvod	k1gInPc2
velkých	velký	k2eAgFnPc2d1
technických	technický	k2eAgFnPc2d1
obtíží	obtíž	k1gFnPc2
při	při	k7c6
stavbě	stavba	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostal	dostat	k5eAaPmAgMnS
přezdívku	přezdívka	k1gFnSc4
Soprillo	Soprillo	k1gNnSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
laděn	laděn	k2eAgMnSc1d1
in	in	k?
B	B	kA
a	a	k8xC
zní	znět	k5eAaImIp3nS
o	o	k7c4
kvintu	kvinta	k1gFnSc4
výše	výše	k1gFnSc2,k1gFnSc2wB
než	než	k8xS
sopraninový	sopraninový	k2eAgInSc4d1
saxofon	saxofon	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Ladění	ladění	k1gNnSc1
a	a	k8xC
notace	notace	k1gFnSc1
</s>
<s>
Ladění	ladění	k1gNnSc1
se	se	k3xPyFc4
střídá	střídat	k5eAaImIp3nS
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
např.	např.	kA
v	v	k7c6
orchestrální	orchestrální	k2eAgFnSc6d1
rodině	rodina	k1gFnSc6
<g/>
:	:	kIx,
</s>
<s>
sopraninový	sopraninový	k2eAgInSc1d1
v	v	k7c6
F	F	kA
<g/>
,	,	kIx,
</s>
<s>
sopránový	sopránový	k2eAgInSc1d1
v	v	k7c6
C	C	kA
<g/>
,	,	kIx,
</s>
<s>
altový	altový	k2eAgMnSc1d1
v	v	k7c6
Es	es	k1gNnSc6
<g/>
,	,	kIx,
</s>
<s>
tenorový	tenorový	k2eAgInSc1d1
v	v	k7c6
C	C	kA
apod.	apod.	kA
</s>
<s>
Zápis	zápis	k1gInSc1
pro	pro	k7c4
všechny	všechen	k3xTgInPc4
saxofony	saxofon	k1gInPc4
je	být	k5eAaImIp3nS
v	v	k7c6
houslovém	houslový	k2eAgInSc6d1
klíči	klíč	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Standardní	standardní	k2eAgInSc1d1
rozsah	rozsah	k1gInSc1
je	být	k5eAaImIp3nS
od	od	k7c2
(	(	kIx(
<g/>
malého	malý	k2eAgNnSc2d1
<g/>
)	)	kIx)
b	b	k?
po	po	k7c6
f³	f³	k?
nebo	nebo	k8xC
fis³	fis³	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znějící	znějící	k2eAgInSc1d1
rozsah	rozsah	k1gInSc1
je	být	k5eAaImIp3nS
u	u	k7c2
každého	každý	k3xTgInSc2
saxofonu	saxofon	k1gInSc2
jiný	jiný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Používání	používání	k1gNnSc1
a	a	k8xC
rozšířenost	rozšířenost	k1gFnSc1
</s>
<s>
V	v	k7c6
současnosti	současnost	k1gFnSc6
se	se	k3xPyFc4
používají	používat	k5eAaImIp3nP
takřka	takřka	k6eAd1
výhradně	výhradně	k6eAd1
„	„	k?
<g/>
vojenské	vojenský	k2eAgFnSc2d1
<g/>
“	“	k?
typy	typa	k1gFnSc2
<g/>
,	,	kIx,
zejména	zejména	k9
sopránový	sopránový	k2eAgInSc1d1
(	(	kIx(
<g/>
in	in	k?
B	B	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
altový	altový	k2eAgInSc1d1
(	(	kIx(
<g/>
in	in	k?
Es	es	k1gNnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tenorový	tenorový	k2eAgInSc1d1
(	(	kIx(
<g/>
in	in	k?
B	B	kA
<g/>
)	)	kIx)
a	a	k8xC
barytonový	barytonový	k2eAgInSc1d1
(	(	kIx(
<g/>
in	in	k?
Es	es	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Jediný	jediný	k2eAgInSc1d1
nástroj	nástroj	k1gInSc1
z	z	k7c2
orchestrální	orchestrální	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
dosáhl	dosáhnout	k5eAaPmAgInS
větší	veliký	k2eAgFnPc4d2
popularity	popularita	k1gFnPc4
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
tenorsaxofon	tenorsaxofon	k1gInSc1
in	in	k?
C	C	kA
(	(	kIx(
<g/>
v	v	k7c6
angličtině	angličtina	k1gFnSc6
C-melody	C-meloda	k1gFnSc2
tenor	tenor	k1gInSc1
saxophone	saxophon	k1gMnSc5
<g/>
)	)	kIx)
<g/>
,	,	kIx,
používal	používat	k5eAaImAgInS
se	se	k3xPyFc4
zejména	zejména	k9
ve	v	k7c6
20	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
30	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Skladby	skladba	k1gFnPc1
pro	pro	k7c4
saxofony	saxofon	k1gInPc4
</s>
<s>
Tóny	Tóna	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
jsou	být	k5eAaImIp3nP
výš	vysoce	k6eAd2
než	než	k8xS
F³	F³	k1gFnSc1
<g/>
,	,	kIx,
se	se	k3xPyFc4
u	u	k7c2
všech	všecek	k3xTgInPc2
saxofonů	saxofon	k1gInPc2
považují	považovat	k5eAaImIp3nP
za	za	k7c4
součást	součást	k1gFnSc4
altissimového	altissimový	k2eAgInSc2d1
rejstříku	rejstřík	k1gInSc2
a	a	k8xC
jsou	být	k5eAaImIp3nP
tvořeny	tvořit	k5eAaImNgFnP
speciálním	speciální	k2eAgInSc7d1
nátiskem	nátisk	k1gInSc7
a	a	k8xC
hmaty	hmat	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sám	sám	k3xTgInSc1
Sax	sax	k1gInSc1
mistrovsky	mistrovsky	k6eAd1
ovládal	ovládat	k5eAaImAgInS
tuto	tento	k3xDgFnSc4
techniku	technika	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
polovině	polovina	k1gFnSc6
dvacátého	dvacátý	k4xOgNnSc2
století	století	k1gNnSc2
se	se	k3xPyFc4
někteří	některý	k3yIgMnPc1
hráči	hráč	k1gMnPc1
odmítali	odmítat	k5eAaImAgMnP
naučit	naučit	k5eAaPmF
hrát	hrát	k5eAaImF
v	v	k7c6
altissimovém	altissimový	k2eAgInSc6d1
rejstříku	rejstřík	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
mnoha	mnoho	k4c6
článcích	článek	k1gInPc6
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
byly	být	k5eAaImAgFnP
napsány	napsán	k2eAgInPc1d1
v	v	k7c6
tomto	tento	k3xDgInSc6
období	období	k1gNnSc6
<g/>
,	,	kIx,
se	se	k3xPyFc4
hovoří	hovořit	k5eAaImIp3nS
o	o	k7c6
používání	používání	k1gNnSc6
altissimových	altissimův	k2eAgInPc2d1
tónů	tón	k1gInPc2
jako	jako	k8xC,k8xS
o	o	k7c6
akrobatických	akrobatický	k2eAgInPc6d1
kouscích	kousek	k1gInPc6
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
falšují	falšovat	k5eAaImIp3nP
nebo	nebo	k8xC
využívají	využívat	k5eAaImIp3nP,k5eAaPmIp3nP
špatný	špatný	k2eAgInSc4d1
prstoklad	prstoklad	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
altissimovém	altissimový	k2eAgInSc6d1
rejstříku	rejstřík	k1gInSc6
ovšem	ovšem	k9
není	být	k5eNaImIp3nS
nic	nic	k3yNnSc1
falešného	falešný	k2eAgNnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
u	u	k7c2
ostatních	ostatní	k2eAgInPc2d1
dřevěných	dřevěný	k2eAgInPc2d1
dechových	dechový	k2eAgInPc2d1
nástrojů	nástroj	k1gInPc2
<g/>
,	,	kIx,
hráč	hráč	k1gMnSc1
pouze	pouze	k6eAd1
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
harmonických	harmonický	k2eAgInPc2d1
kmitů	kmit	k1gInPc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
tak	tak	k6eAd1
rozšířil	rozšířit	k5eAaPmAgInS
rozsah	rozsah	k1gInSc4
nástroje	nástroj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
saxofonu	saxofon	k1gInSc2
však	však	k9
vyžaduje	vyžadovat	k5eAaImIp3nS
zvládnutí	zvládnutí	k1gNnSc4
těchto	tento	k3xDgInPc2
kmitů	kmit	k1gInPc2
mnohem	mnohem	k6eAd1
větší	veliký	k2eAgNnSc4d2
úsilí	úsilí	k1gNnSc4
než	než	k8xS
u	u	k7c2
jiných	jiný	k2eAgInPc2d1
dřevěných	dřevěný	k2eAgInPc2d1
dechových	dechový	k2eAgInPc2d1
nástrojů	nástroj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Téměř	téměř	k6eAd1
všechny	všechen	k3xTgInPc4
saxofony	saxofon	k1gInPc4
jsou	být	k5eAaImIp3nP
transponující	transponující	k2eAgInPc4d1
nástroje	nástroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sopraninový	Sopraninový	k2eAgInSc1d1
<g/>
,	,	kIx,
altový	altový	k2eAgInSc1d1
a	a	k8xC
barytonový	barytonový	k2eAgMnSc1d1
jsou	být	k5eAaImIp3nP
laděny	laděn	k2eAgMnPc4d1
v	v	k7c6
Es	es	k1gNnPc6
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
sopránový	sopránový	k2eAgInSc4d1
<g/>
,	,	kIx,
tenorový	tenorový	k2eAgInSc4d1
a	a	k8xC
basový	basový	k2eAgInSc4d1
v	v	k7c6
Bb	Bb	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jelikož	jelikož	k8xS
všechny	všechen	k3xTgInPc1
nástroje	nástroj	k1gInPc1
používají	používat	k5eAaImIp3nP
stejné	stejný	k2eAgNnSc4d1
uspořádání	uspořádání	k1gNnSc4
klapek	klapka	k1gFnPc2
a	a	k8xC
prstoklad	prstoklad	k1gInSc1
pro	pro	k7c4
vytvoření	vytvoření	k1gNnSc4
tónů	tón	k1gInPc2
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
pro	pro	k7c4
schopného	schopný	k2eAgMnSc4d1
hráče	hráč	k1gMnSc4
velký	velký	k2eAgInSc1d1
problém	problém	k1gInSc1
přejít	přejít	k5eAaPmF
z	z	k7c2
jedné	jeden	k4xCgFnSc2
velikosti	velikost	k1gFnSc2
saxofonu	saxofon	k1gInSc2
na	na	k7c4
jinou	jiný	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hraje	hrát	k5eAaImIp3nS
<g/>
-li	-li	k?
saxofonista	saxofonista	k1gMnSc1
psané	psaný	k2eAgInPc4d1
C	C	kA
na	na	k7c4
altsaxofon	altsaxofon	k1gInSc4
<g/>
,	,	kIx,
zní	znět	k5eAaImIp3nS
Es	es	k1gNnSc1
(	(	kIx(
<g/>
sexta	sexta	k1gFnSc1
dolů	dol	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hraje	hrát	k5eAaImIp3nS
<g/>
-li	-li	k?
ovšem	ovšem	k9
hráč	hráč	k1gMnSc1
psané	psaný	k2eAgFnSc2d1
C	C	kA
na	na	k7c4
tenorsaxofon	tenorsaxofon	k1gInSc4
<g/>
,	,	kIx,
zní	znět	k5eAaImIp3nS
Bb	Bb	k1gFnSc1
(	(	kIx(
<g/>
nóna	nóna	k1gFnSc1
dolů	dol	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Barytonsaxofon	Barytonsaxofon	k1gInSc1
hraje	hrát	k5eAaImIp3nS
o	o	k7c4
oktávu	oktáva	k1gFnSc4
níž	nízce	k6eAd2
než	než	k8xS
altsaxofon	altsaxofon	k1gInSc1
a	a	k8xC
sopránový	sopránový	k2eAgInSc1d1
saxofon	saxofon	k1gInSc1
zní	znět	k5eAaImIp3nS
o	o	k7c4
oktávu	oktáva	k1gFnSc4
výš	vysoce	k6eAd2
než	než	k8xS
tenorový	tenorový	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Protože	protože	k8xS
barytonový	barytonový	k2eAgInSc1d1
a	a	k8xC
altový	altový	k2eAgInSc1d1
saxofon	saxofon	k1gInSc1
jsou	být	k5eAaImIp3nP
laděny	ladit	k5eAaImNgInP
v	v	k7c6
Es	es	k1gNnPc6
<g/>
,	,	kIx,
mohou	moct	k5eAaImIp3nP
hráči	hráč	k1gMnPc1
hrát	hrát	k5eAaImF
z	z	k7c2
partu	part	k1gInSc2
psaného	psaný	k2eAgInSc2d1
v	v	k7c6
basovém	basový	k2eAgInSc6d1
klíči	klíč	k1gInSc6
<g/>
,	,	kIx,
pokud	pokud	k8xS
ho	on	k3xPp3gMnSc4
budou	být	k5eAaImBp3nP
číst	číst	k5eAaImF
jako	jako	k9
houslový	houslový	k2eAgInSc4d1
a	a	k8xC
přidají	přidat	k5eAaPmIp3nP
si	se	k3xPyFc3
tři	tři	k4xCgInPc4
křížky	křížek	k1gInPc4
v	v	k7c6
předznamenání	předznamenání	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
metoda	metoda	k1gFnSc1
<g/>
,	,	kIx,
substituce	substituce	k1gFnSc1
klíčů	klíč	k1gInPc2
<g/>
,	,	kIx,
umožňuje	umožňovat	k5eAaImIp3nS
barytonsaxofonistovi	barytonsaxofonistův	k2eAgMnPc1d1
(	(	kIx(
<g/>
nebo	nebo	k8xC
jakémukoliv	jakýkoliv	k3yIgMnSc3
jinému	jiný	k2eAgMnSc3d1
hráči	hráč	k1gMnSc3
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
hraje	hrát	k5eAaImIp3nS
na	na	k7c4
nástroj	nástroj	k1gInSc4
laděný	laděný	k2eAgInSc4d1
v	v	k7c6
Es	es	k1gNnSc6
<g/>
)	)	kIx)
hrát	hrát	k5eAaImF
z	z	k7c2
partů	part	k1gInPc2
psaných	psaný	k2eAgInPc2d1
pro	pro	k7c4
fagot	fagot	k1gInSc4
<g/>
,	,	kIx,
tubu	tuba	k1gFnSc4
<g/>
,	,	kIx,
pozoun	pozoun	k1gInSc4
nebo	nebo	k8xC
kontrabas	kontrabas	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
užitečné	užitečný	k2eAgNnSc1d1
postrádá	postrádat	k5eAaImIp3nS
<g/>
-li	-li	k?
kapela	kapela	k1gFnSc1
nebo	nebo	k8xC
orchestr	orchestr	k1gInSc1
jeden	jeden	k4xCgInSc4
ze	z	k7c2
zmíněných	zmíněný	k2eAgInPc2d1
nástrojů	nástroj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Většina	většina	k1gFnSc1
barytonsaxofonů	barytonsaxofon	k1gInPc2
vyrobených	vyrobený	k2eAgInPc2d1
po	po	k7c6
roce	rok	k1gInSc6
1980	#num#	k4
má	mít	k5eAaImIp3nS
přídavnou	přídavný	k2eAgFnSc4d1
klapku	klapka	k1gFnSc4
pro	pro	k7c4
malé	malý	k2eAgFnPc4d1
A	a	k8xC
(	(	kIx(
<g/>
koncertní	koncertní	k2eAgMnSc1d1
C	C	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
avšak	avšak	k8xC
dříve	dříve	k6eAd2
vyrobené	vyrobený	k2eAgInPc1d1
barytonové	barytonový	k2eAgInPc1d1
saxofony	saxofon	k1gInPc1
a	a	k8xC
ostatní	ostatní	k2eAgInPc1d1
typy	typ	k1gInPc1
saxofonů	saxofon	k1gInPc2
tuto	tento	k3xDgFnSc4
klapku	klapka	k1gFnSc4
nemají	mít	k5eNaImIp3nP
(	(	kIx(
<g/>
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
některých	některý	k3yIgInPc2
bassaxofonů	bassaxofon	k1gInPc2
a	a	k8xC
několika	několik	k4yIc2
výjimečných	výjimečný	k2eAgInPc2d1
altsaxofonů	altsaxofon	k1gInPc2
vyrobených	vyrobený	k2eAgFnPc2d1
společností	společnost	k1gFnPc2
Selmer	Selmra	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skladatelé	skladatel	k1gMnPc1
píšící	píšící	k2eAgMnSc1d1
pro	pro	k7c4
barytonsaxofon	barytonsaxofon	k1gInSc4
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
vyžadují	vyžadovat	k5eAaImIp3nP
malé	malý	k2eAgNnSc4d1
a	a	k8xC
<g/>
,	,	kIx,
by	by	kYmCp3nP
si	se	k3xPyFc3
měli	mít	k5eAaImAgMnP
být	být	k5eAaImF
vědomi	vědom	k2eAgMnPc1d1
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
že	že	k8xS
mnoho	mnoho	k4c1
hráčů	hráč	k1gMnPc2
nebude	být	k5eNaImBp3nS
moci	moct	k5eAaImF
tuto	tento	k3xDgFnSc4
notu	nota	k1gFnSc4
z	z	k7c2
důvodu	důvod	k1gInSc2
absence	absence	k1gFnSc2
požadované	požadovaný	k2eAgFnSc2d1
klapky	klapka	k1gFnSc2
zahrát	zahrát	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Saxofon	saxofon	k1gInSc1
v	v	k7c6
orchestrech	orchestr	k1gInPc6
</s>
<s>
Dechové	dechový	k2eAgInPc1d1
orchestry	orchestr	k1gInPc1
</s>
<s>
Nejdéle	dlouho	k6eAd3
jsou	být	k5eAaImIp3nP
saxofony	saxofon	k1gInPc1
využívány	využíván	k2eAgInPc1d1
v	v	k7c6
dechových	dechový	k2eAgInPc6d1
souborech	soubor	k1gInPc6
<g/>
,	,	kIx,
již	již	k6eAd1
krátce	krátce	k6eAd1
po	po	k7c6
svém	svůj	k3xOyFgNnSc6
uvedení	uvedení	k1gNnSc6
je	být	k5eAaImIp3nS
začala	začít	k5eAaPmAgFnS
používat	používat	k5eAaImF
francouzská	francouzský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Saxofon	saxofon	k1gInSc1
kombinuje	kombinovat	k5eAaImIp3nS
výhody	výhoda	k1gFnPc4
dřevěných	dřevěný	k2eAgInPc2d1
nástrojů	nástroj	k1gInPc2
(	(	kIx(
<g/>
lehkou	lehký	k2eAgFnSc4d1
ovladatelnost	ovladatelnost	k1gFnSc4
jak	jak	k8xS,k8xC
při	při	k7c6
hře	hra	k1gFnSc6
<g/>
,	,	kIx,
tak	tak	k6eAd1
za	za	k7c4
chůze	chůze	k1gFnPc4
<g/>
)	)	kIx)
a	a	k8xC
průrazný	průrazný	k2eAgInSc4d1
tón	tón	k1gInSc4
žesťových	žesťový	k2eAgInPc2d1
nástrojů	nástroj	k1gInPc2
<g/>
;	;	kIx,
pro	pro	k7c4
účely	účel	k1gInPc4
zejména	zejména	k9
vojenských	vojenský	k2eAgFnPc2d1
kapel	kapela	k1gFnPc2
je	být	k5eAaImIp3nS
tedy	tedy	k9
velice	velice	k6eAd1
vhodný	vhodný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Klasická	klasický	k2eAgFnSc1d1
hudba	hudba	k1gFnSc1
</s>
<s>
Kvůli	kvůli	k7c3
svým	svůj	k3xOyFgFnPc3
zvukovým	zvukový	k2eAgFnPc3d1
vlastnostem	vlastnost	k1gFnPc3
se	se	k3xPyFc4
v	v	k7c6
klasické	klasický	k2eAgFnSc6d1
hudbě	hudba	k1gFnSc6
nikdy	nikdy	k6eAd1
ve	v	k7c6
větším	veliký	k2eAgNnSc6d2
měřítku	měřítko	k1gNnSc6
neprosadil	prosadit	k5eNaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
tyto	tento	k3xDgInPc4
účely	účel	k1gInPc4
je	být	k5eAaImIp3nS
jeho	jeho	k3xOp3gInSc1
tón	tón	k1gInSc1
příliš	příliš	k6eAd1
robustní	robustní	k2eAgInSc1d1
a	a	k8xC
ostrý	ostrý	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
existuje	existovat	k5eAaImIp3nS
několik	několik	k4yIc4
koncertů	koncert	k1gInPc2
pro	pro	k7c4
sólový	sólový	k2eAgInSc4d1
saxofon	saxofon	k1gInSc4
a	a	k8xC
orchestr	orchestr	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Jazz	jazz	k1gInSc1
</s>
<s>
Jazz	jazz	k1gInSc1
je	být	k5eAaImIp3nS
bez	bez	k7c2
saxofonu	saxofon	k1gInSc2
takřka	takřka	k6eAd1
nemyslitelný	myslitelný	k2eNgInSc1d1
–	–	k?
naprostá	naprostý	k2eAgFnSc1d1
většina	většina	k1gFnSc1
jazzových	jazzový	k2eAgMnPc2d1
souborů	soubor	k1gInPc2
obsahuje	obsahovat	k5eAaImIp3nS
tento	tento	k3xDgInSc1
nástroj	nástroj	k1gInSc1
<g/>
,	,	kIx,
buď	buď	k8xC
jako	jako	k9
sólový	sólový	k2eAgInSc1d1
nebo	nebo	k8xC
doprovodný	doprovodný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Velice	velice	k6eAd1
časté	častý	k2eAgNnSc1d1
je	být	k5eAaImIp3nS
seskupení	seskupení	k1gNnSc1
saxofonů	saxofon	k1gInPc2
do	do	k7c2
malých	malý	k2eAgInPc2d1
souborů	soubor	k1gInPc2
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gInSc1
zvuk	zvuk	k1gInSc1
velice	velice	k6eAd1
dobře	dobře	k6eAd1
splývá	splývat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oblíbený	oblíbený	k2eAgInSc1d1
je	být	k5eAaImIp3nS
saxofonový	saxofonový	k2eAgInSc1d1
kvartet	kvartet	k1gInSc1
–	–	k?
sopránový	sopránový	k2eAgInSc1d1
<g/>
,	,	kIx,
altový	altový	k2eAgInSc1d1
<g/>
,	,	kIx,
tenorový	tenorový	k2eAgInSc1d1
a	a	k8xC
barytonový	barytonový	k2eAgInSc1d1
<g/>
,	,	kIx,
místo	místo	k7c2
sopránového	sopránový	k2eAgNnSc2d1
se	se	k3xPyFc4
někdy	někdy	k6eAd1
používá	používat	k5eAaImIp3nS
altsaxofon	altsaxofon	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Větším	většit	k5eAaImIp1nS
komorním	komorní	k2eAgNnSc7d1
hudebním	hudební	k2eAgNnSc7d1
tělesem	těleso	k1gNnSc7
je	být	k5eAaImIp3nS
saxofonový	saxofonový	k2eAgInSc1d1
orchestr	orchestr	k1gInSc1
<g/>
,	,	kIx,
nejčastěji	často	k6eAd3
složen	složit	k5eAaPmNgMnS
z	z	k7c2
12	#num#	k4
nástrojů	nástroj	k1gInPc2
(	(	kIx(
<g/>
přičemž	přičemž	k6eAd1
obsazení	obsazení	k1gNnSc2
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
měnit	měnit	k5eAaImF
<g/>
)	)	kIx)
<g/>
:	:	kIx,
sopraninový	sopraninový	k2eAgInSc1d1
<g/>
,	,	kIx,
dva	dva	k4xCgInPc1
sopránové	sopránový	k2eAgInPc1d1
<g/>
,	,	kIx,
tři	tři	k4xCgInPc1
altové	altový	k2eAgInPc1d1
<g/>
,	,	kIx,
tři	tři	k4xCgInPc1
tenorové	tenorový	k2eAgInPc1d1
<g/>
,	,	kIx,
dva	dva	k4xCgMnPc1
barytonové	baryton	k1gMnPc1
a	a	k8xC
jeden	jeden	k4xCgInSc1
basový	basový	k2eAgInSc1d1
saxofon	saxofon	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Typická	typický	k2eAgFnSc1d1
saxofonová	saxofonový	k2eAgFnSc1d1
sekce	sekce	k1gFnSc1
v	v	k7c6
big	big	k?
bandu	band	k1gInSc6
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
z	z	k7c2
2-6	2-6	k4
altsaxofonů	altsaxofon	k1gInPc2
<g/>
,	,	kIx,
1-3	1-3	k4
tenorsaxofonů	tenorsaxofon	k1gInPc2
a	a	k8xC
jednoho	jeden	k4xCgInSc2
barytonsaxofonu	barytonsaxofon	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sopránsaxofon	Sopránsaxofon	k1gInSc1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
většinou	většina	k1gFnSc7
jen	jen	k9
k	k	k7c3
sólové	sólový	k2eAgFnSc3d1
hře	hra	k1gFnSc3
<g/>
,	,	kIx,
popř.	popř.	kA
v	v	k7c6
komorních	komorní	k2eAgInPc6d1
saxofonových	saxofonový	k2eAgInPc6d1
souborech	soubor	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Značky	značka	k1gFnPc1
</s>
<s>
Mezi	mezi	k7c4
nejznámější	známý	k2eAgMnPc4d3
výrobce	výrobce	k1gMnPc4
saxofonů	saxofon	k1gInPc2
patří	patřit	k5eAaImIp3nP
Buffet	Buffet	k1gMnSc1
Crampon	Crampon	k1gMnSc1
<g/>
,	,	kIx,
Cannonball	Cannonball	k1gMnSc1
<g/>
,	,	kIx,
Julius	Julius	k1gMnSc1
Keilwerth	Keilwerth	k1gMnSc1
<g/>
,	,	kIx,
Leblanc	Leblanc	k1gFnSc1
<g/>
,	,	kIx,
P.	P.	kA
Mauriat	Mauriat	k1gInSc1
<g/>
,	,	kIx,
Roland	Roland	k1gInSc1
<g/>
,	,	kIx,
Selmer	Selmer	k1gInSc1
<g/>
,	,	kIx,
Yamaha	yamaha	k1gFnSc1
a	a	k8xC
Yanagisava	Yanagisava	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
je	být	k5eAaImIp3nS
předním	přední	k2eAgMnSc7d1
výrobcem	výrobce	k1gMnSc7
Amati	Amať	k1gFnSc2
Kraslice	kraslice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Za	za	k7c4
tradiční	tradiční	k2eAgMnPc4d1
výrobce	výrobce	k1gMnPc4
jsou	být	k5eAaImIp3nP
považovány	považován	k2eAgFnPc1d1
firmy	firma	k1gFnPc1
Keilwerth	Keilwerth	k1gMnSc1
<g/>
,	,	kIx,
Selmer	Selmer	k1gMnSc1
<g/>
,	,	kIx,
Yamaha	yamaha	k1gFnSc1
a	a	k8xC
Yanagisava	Yanagisava	k1gFnSc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
někdy	někdy	k6eAd1
bývají	bývat	k5eAaImIp3nP
označováni	označovat	k5eAaImNgMnP
termínem	termín	k1gInSc7
„	„	k?
<g/>
Velká	velká	k1gFnSc1
čtyřka	čtyřka	k1gFnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Technika	technika	k1gFnSc1
hry	hra	k1gFnSc2
</s>
<s>
Mnoho	mnoho	k4c1
lidí	člověk	k1gMnPc2
se	se	k3xPyFc4
domnívá	domnívat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
stát	stát	k5eAaImF,k5eAaPmF
se	se	k3xPyFc4
schopným	schopný	k2eAgMnSc7d1
saxofonistou	saxofonista	k1gMnSc7
je	být	k5eAaImIp3nS
poměrně	poměrně	k6eAd1
jednoduché	jednoduchý	k2eAgNnSc1d1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
hlavně	hlavně	k9
v	v	k7c6
případě	případ	k1gInSc6
přecházíme	přecházet	k5eAaImIp1nP
<g/>
-li	-li	k?
z	z	k7c2
jiného	jiný	k2eAgInSc2d1
dřevěného	dřevěný	k2eAgInSc2d1
dechového	dechový	k2eAgInSc2d1
nástroje	nástroj	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
většinou	většina	k1gFnSc7
je	být	k5eAaImIp3nS
potřeba	potřeba	k6eAd1
značné	značný	k2eAgNnSc1d1
množství	množství	k1gNnSc1
práce	práce	k1gFnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
byl	být	k5eAaImAgMnS
člověk	člověk	k1gMnSc1
schopen	schopen	k2eAgMnSc1d1
vytvořit	vytvořit	k5eAaPmF
příjemný	příjemný	k2eAgInSc4d1
tón	tón	k1gInSc4
a	a	k8xC
dokonalou	dokonalý	k2eAgFnSc4d1
techniku	technika	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Technika	technika	k1gFnSc1
hry	hra	k1gFnSc2
na	na	k7c4
saxofon	saxofon	k1gInSc4
je	být	k5eAaImIp3nS
závislá	závislý	k2eAgFnSc1d1
na	na	k7c6
stylu	styl	k1gInSc6
<g/>
,	,	kIx,
kterému	který	k3yIgInSc3,k3yRgInSc3,k3yQgInSc3
se	se	k3xPyFc4
chce	chtít	k5eAaImIp3nS
hráč	hráč	k1gMnSc1
věnovat	věnovat	k5eAaPmF,k5eAaImF
(	(	kIx(
<g/>
klasika	klasika	k1gFnSc1
<g/>
,	,	kIx,
jazz	jazz	k1gInSc1
<g/>
,	,	kIx,
rock	rock	k1gInSc1
<g/>
,	,	kIx,
funky	funk	k1gInPc1
<g/>
,	,	kIx,
atd.	atd.	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
také	také	k9
na	na	k7c6
tónu	tón	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
chce	chtít	k5eAaImIp3nS
daný	daný	k2eAgMnSc1d1
hráč	hráč	k1gMnSc1
být	být	k5eAaImF
schopen	schopen	k2eAgMnSc1d1
vytvořit	vytvořit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Nátisk	nátisk	k1gInSc1
</s>
<s>
Nátisk	nátisk	k1gInSc1
se	se	k3xPyFc4
většinou	většina	k1gFnSc7
tvoří	tvořit	k5eAaImIp3nP
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
hráč	hráč	k1gMnSc1
má	mít	k5eAaImIp3nS
hubičku	hubička	k1gFnSc4
vloženou	vložený	k2eAgFnSc4d1
v	v	k7c6
ústech	ústa	k1gNnPc6
ne	ne	k9
více	hodně	k6eAd2
jak	jak	k8xS,k8xC
do	do	k7c2
půlky	půlka	k1gFnSc2
(	(	kIx(
<g/>
asi	asi	k9
1	#num#	k4
<g/>
cm	cm	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spodní	spodní	k2eAgInSc1d1
ret	ret	k1gInSc1
je	být	k5eAaImIp3nS
podepřen	podepřít	k5eAaPmNgInS
spodními	spodní	k2eAgInPc7d1
zuby	zub	k1gInPc7
a	a	k8xC
dotýká	dotýkat	k5eAaImIp3nS
se	se	k3xPyFc4
plátku	plátek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vrchní	vrchní	k2eAgInPc1d1
zuby	zub	k1gInPc1
lehce	lehko	k6eAd1
tlačí	tlačit	k5eAaImIp3nP
z	z	k7c2
vrchu	vrch	k1gInSc2
na	na	k7c4
hubičku	hubička	k1gFnSc4
a	a	k8xC
tím	ten	k3xDgNnSc7
stabilizují	stabilizovat	k5eAaBmIp3nP
herní	herní	k2eAgFnSc4d1
pozici	pozice	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Horní	horní	k2eAgInSc1d1
ret	ret	k1gInSc1
slouží	sloužit	k5eAaImIp3nS
k	k	k7c3
utěsnění	utěsnění	k1gNnSc3
a	a	k8xC
zabraňuje	zabraňovat	k5eAaImIp3nS
úniku	únik	k1gInSc3
vzduchu	vzduch	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Saxofonový	saxofonový	k2eAgInSc1d1
nátisk	nátisk	k1gInSc1
se	se	k3xPyFc4
přirovnává	přirovnávat	k5eAaImIp3nS
se	se	k3xPyFc4
k	k	k7c3
pískání	pískání	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s>
Vibrato	vibrato	k6eAd1
</s>
<s>
Saxofonové	saxofonový	k2eAgNnSc1d1
vibrato	vibrato	k1gNnSc1
je	být	k5eAaImIp3nS
podobné	podobný	k2eAgNnSc1d1
smyčcovému	smyčcový	k2eAgNnSc3d1
nebo	nebo	k8xC
vokálnímu	vokální	k2eAgNnSc3d1
vibratu	vibrato	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozdíl	rozdíl	k1gInSc1
je	být	k5eAaImIp3nS
pouze	pouze	k6eAd1
v	v	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
vibrace	vibrace	k1gFnPc1
jsou	být	k5eAaImIp3nP
tvořeny	tvořit	k5eAaImNgFnP
za	za	k7c2
pomoci	pomoc	k1gFnSc2
čelisti	čelist	k1gFnPc1
namísto	namísto	k7c2
prstů	prst	k1gInPc2
nebo	nebo	k8xC
bránice	bránice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pohyb	pohyb	k1gInSc4
čelisti	čelist	k1gFnSc2
si	se	k3xPyFc3
můžeme	moct	k5eAaImIp1nP
simulovat	simulovat	k5eAaImF
vyslovením	vyslovení	k1gNnSc7
slabik	slabika	k1gFnPc2
„	„	k?
<g/>
wa-wa-wa	wa-wa-wa	k1gMnSc1
<g/>
“	“	k?
nebo	nebo	k8xC
„	„	k?
<g/>
taj-yai-yai	taj-yai-ya	k1gFnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Opět	opět	k6eAd1
záleží	záležet	k5eAaImIp3nS
na	na	k7c6
hráči	hráč	k1gMnSc6
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
metoda	metoda	k1gFnSc1
je	být	k5eAaImIp3nS
pro	pro	k7c4
něho	on	k3xPp3gMnSc4
nejlepší	dobrý	k2eAgNnPc1d3
pro	pro	k7c4
vytvoření	vytvoření	k1gNnSc4
správného	správný	k2eAgNnSc2d1
vibrata	vibrato	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
většina	většina	k1gFnSc1
lidí	člověk	k1gMnPc2
tvrdí	tvrdit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
vibrato	vibrato	k6eAd1
není	být	k5eNaImIp3nS
pro	pro	k7c4
saxofon	saxofon	k1gInSc4
důležité	důležitý	k2eAgNnSc1d1
(	(	kIx(
<g/>
důležitější	důležitý	k2eAgMnSc1d2
je	být	k5eAaImIp3nS
podle	podle	k7c2
nich	on	k3xPp3gFnPc2
kvalitní	kvalitní	k2eAgFnSc1d1
tón	tón	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jiní	jiný	k2eAgMnPc1d1
tvrdí	tvrdit	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
nedílná	dílný	k2eNgFnSc1d1
součást	součást	k1gFnSc1
typické	typický	k2eAgFnSc2d1
saxofonové	saxofonový	k2eAgFnSc2d1
barvy	barva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klasické	klasický	k2eAgInPc1d1
vibrato	vibrato	k1gNnSc1
se	se	k3xPyFc4
u	u	k7c2
jednotlivých	jednotlivý	k2eAgMnPc2d1
hráčů	hráč	k1gMnPc2
liší	lišit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnoho	mnoho	k6eAd1
klasických	klasický	k2eAgMnPc2d1
hráčů	hráč	k1gMnPc2
si	se	k3xPyFc3
bere	brát	k5eAaImIp3nS
za	za	k7c4
vzor	vzor	k1gInSc4
vibrato	vibrato	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
tvoří	tvořit	k5eAaImIp3nP
houslisté	houslista	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ještě	ještě	k6eAd1
mnohem	mnohem	k6eAd1
více	hodně	k6eAd2
se	se	k3xPyFc4
u	u	k7c2
jednotlivých	jednotlivý	k2eAgMnPc2d1
hráčů	hráč	k1gMnPc2
liší	lišit	k5eAaImIp3nP
jazzové	jazzový	k2eAgNnSc4d1
vibrato	vibrato	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Zvláštní	zvláštní	k2eAgInPc1d1
efekty	efekt	k1gInPc1
</s>
<s>
K	k	k7c3
vytvoření	vytvoření	k1gNnSc3
nových	nový	k2eAgInPc2d1
a	a	k8xC
zajímavých	zajímavý	k2eAgInPc2d1
tónů	tón	k1gInPc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
použito	použít	k5eAaPmNgNnS
mnoho	mnoho	k4c1
různých	různý	k2eAgInPc2d1
efektů	efekt	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Growling	Growling	k1gInSc1
je	být	k5eAaImIp3nS
technika	technika	k1gFnSc1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
saxofonista	saxofonista	k1gMnSc1
chrčí	chrčet	k5eAaImIp3nS
<g/>
,	,	kIx,
bručí	bručet	k5eAaImIp3nS
nebo	nebo	k8xC
vrčí	vrčet	k5eAaImIp3nS
za	za	k7c2
pomoci	pomoc	k1gFnSc2
hlasivek	hlasivka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
je	být	k5eAaImIp3nS
způsobena	způsobit	k5eAaPmNgFnS
modulace	modulace	k1gFnSc1
zvuku	zvuk	k1gInSc2
a	a	k8xC
výsledkem	výsledek	k1gInSc7
je	být	k5eAaImIp3nS
drsnost	drsnost	k1gFnSc1
a	a	k8xC
chraplavost	chraplavost	k1gFnSc1
tónu	tón	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzácně	vzácně	k6eAd1
se	se	k3xPyFc4
s	s	k7c7
tímto	tento	k3xDgInSc7
efektem	efekt	k1gInSc7
můžeme	moct	k5eAaImIp1nP
setkat	setkat	k5eAaPmF
v	v	k7c6
klasické	klasický	k2eAgFnSc6d1
nebo	nebo	k8xC
dechové	dechový	k2eAgFnSc6d1
hudbě	hudba	k1gFnSc6
<g/>
,	,	kIx,
často	často	k6eAd1
se	se	k3xPyFc4
ovšem	ovšem	k9
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
v	v	k7c6
jazzu	jazz	k1gInSc6
<g/>
,	,	kIx,
blues	blues	k1gNnSc4
<g/>
,	,	kIx,
rock	rock	k1gInSc4
<g/>
’	’	k?
<g/>
n	n	k0
<g/>
’	’	k?
<g/>
rollu	roll	k1gInSc6
a	a	k8xC
v	v	k7c6
dalších	další	k2eAgInPc6d1
populárních	populární	k2eAgInPc6d1
žánrech	žánr	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
významné	významný	k2eAgMnPc4d1
hudebníky	hudebník	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
používali	používat	k5eAaImAgMnP
tuto	tento	k3xDgFnSc4
techniku	technika	k1gFnSc4
<g/>
,	,	kIx,
patří	patřit	k5eAaImIp3nS
Earl	earl	k1gMnSc1
Bostic	Bostice	k1gFnPc2
<g/>
,	,	kIx,
Boots	Bootsa	k1gFnPc2
Randolph	Randolpha	k1gFnPc2
<g/>
,	,	kIx,
Gato	Gato	k6eAd1
Barbieri	Barbieri	k1gNnSc1
<g/>
,	,	kIx,
Ben	Ben	k1gInSc1
Webster	Webster	k1gInSc1
<g/>
,	,	kIx,
Clarence	Clarence	k1gFnSc1
Clemons	Clemonsa	k1gFnPc2
a	a	k8xC
King	Kinga	k1gFnPc2
Curtis	Curtis	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Glissando	glissando	k6eAd1
je	být	k5eAaImIp3nS
technika	technika	k1gFnSc1
<g/>
,	,	kIx,
při	při	k7c6
které	který	k3yQgFnSc6,k3yIgFnSc6,k3yRgFnSc6
saxofonista	saxofonista	k1gMnSc1
nepatrně	nepatrně	k6eAd1,k6eNd1
sníží	snížit	k5eAaPmIp3nS
nebo	nebo	k8xC
zvýší	zvýšit	k5eAaPmIp3nS
tón	tón	k1gInSc4
a	a	k8xC
současně	současně	k6eAd1
sklouzne	sklouznout	k5eAaPmIp3nS
na	na	k7c4
vyšší	vysoký	k2eAgInSc4d2
nebo	nebo	k8xC
nižší	nízký	k2eAgInSc4d2
tón	tón	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
touto	tento	k3xDgFnSc7
technikou	technika	k1gFnSc7
se	se	k3xPyFc4
někdy	někdy	k6eAd1
můžeme	moct	k5eAaImIp1nP
setkat	setkat	k5eAaPmF
v	v	k7c6
dechových	dechový	k2eAgInPc6d1
orchestrech	orchestr	k1gInPc6
(	(	kIx(
<g/>
např.	např.	kA
v	v	k7c6
skladbě	skladba	k1gFnSc6
„	„	k?
<g/>
Sing	Singa	k1gFnPc2
<g/>
,	,	kIx,
sing	singa	k1gFnPc2
<g/>
,	,	kIx,
sing	singa	k1gFnPc2
<g/>
“	“	k?
od	od	k7c2
Bennyho	Benny	k1gMnSc2
Goodmana	Goodman	k1gMnSc2
<g/>
)	)	kIx)
a	a	k8xC
vzácně	vzácně	k6eAd1
také	také	k9
v	v	k7c6
orchestrálních	orchestrální	k2eAgFnPc6d1
skladbách	skladba	k1gFnPc6
(	(	kIx(
<g/>
např.	např.	kA
Gershwinova	Gershwinův	k2eAgFnSc1d1
skladba	skladba	k1gFnSc1
Rapsodie	rapsodie	k1gFnSc2
v	v	k7c6
modrém	modrý	k2eAgNnSc6d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Glissando	glissando	k6eAd1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
také	také	k9
tvořeno	tvořit	k5eAaImNgNnS
pomocí	pomocí	k7c2
jazyka	jazyk	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
ovládá	ovládat	k5eAaImIp3nS
proud	proud	k1gInSc4
vzduchu	vzduch	k1gInSc2
a	a	k8xC
zároveň	zároveň	k6eAd1
udržuje	udržovat	k5eAaImIp3nS
stabilní	stabilní	k2eAgInSc4d1
nátisk	nátisk	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Multiphonics	Multiphonics	k6eAd1
je	být	k5eAaImIp3nS
technika	technika	k1gFnSc1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
hráč	hráč	k1gMnSc1
hraje	hrát	k5eAaImIp3nS
více	hodně	k6eAd2
než	než	k8xS
jeden	jeden	k4xCgInSc4
tón	tón	k1gInSc4
zároveň	zároveň	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Speciální	speciální	k2eAgInSc1d1
prstoklad	prstoklad	k1gInSc1
zapříčiní	zapříčinit	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
nástroj	nástroj	k1gInSc1
vibruje	vibrovat	k5eAaImIp3nS
na	na	k7c6
dvou	dva	k4xCgFnPc6
různých	různý	k2eAgFnPc6d1
frekvencích	frekvence	k1gFnPc6
současně	současně	k6eAd1
a	a	k8xC
vytváří	vytvářet	k5eAaImIp3nS,k5eAaPmIp3nS
tak	tak	k6eAd1
trylkový	trylkový	k2eAgInSc4d1
tón	tón	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Známí	známý	k2eAgMnPc1d1
saxofonisté	saxofonista	k1gMnPc1
</s>
<s>
Bill	Bill	k1gMnSc1
Clinton	Clinton	k1gMnSc1
hrající	hrající	k2eAgMnSc1d1
na	na	k7c4
tenorsaxofon	tenorsaxofon	k1gInSc4
u	u	k7c2
Borise	Boris	k1gMnSc2
Jelcina	Jelcin	k1gMnSc2
</s>
<s>
Většina	většina	k1gFnSc1
saxofonistů	saxofonista	k1gMnPc2
nehraje	hrát	k5eNaImIp3nS
pouze	pouze	k6eAd1
na	na	k7c4
jediný	jediný	k2eAgInSc4d1
typ	typ	k1gInSc4
nástroje	nástroj	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
s	s	k7c7
oblibou	obliba	k1gFnSc7
používá	používat	k5eAaImIp3nS
více	hodně	k6eAd2
typů	typ	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
mnohdy	mnohdy	k6eAd1
střídají	střídat	k5eAaImIp3nP
i	i	k9
s	s	k7c7
ostatními	ostatní	k2eAgInPc7d1
nástroji	nástroj	k1gInPc7
<g/>
,	,	kIx,
především	především	k9
s	s	k7c7
klarinetem	klarinet	k1gInSc7
nebo	nebo	k8xC
příčnou	příčný	k2eAgFnSc7d1
flétnou	flétna	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejznámějšími	známý	k2eAgMnPc7d3
saxofonisty	saxofonista	k1gMnPc7
jsou	být	k5eAaImIp3nP
<g/>
:	:	kIx,
</s>
<s>
Charlie	Charlie	k1gMnSc1
Parker	Parker	k1gMnSc1
</s>
<s>
John	John	k1gMnSc1
Coltrane	Coltran	k1gInSc5
</s>
<s>
Sonny	Sonna	k1gFnPc1
Rollins	Rollinsa	k1gFnPc2
</s>
<s>
Stan	stan	k1gInSc1
Getz	Getza	k1gFnPc2
</s>
<s>
Coleman	Coleman	k1gMnSc1
Hawkins	Hawkinsa	k1gFnPc2
</s>
<s>
Z	z	k7c2
českých	český	k2eAgFnPc2d1
zejména	zejména	k9
Felix	Felix	k1gMnSc1
Slováček	Slováček	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
Stivín	Stivín	k1gMnSc1
<g/>
,	,	kIx,
Viktor	Viktor	k1gMnSc1
Kotrubenko	Kotrubenka	k1gFnSc5
<g/>
,	,	kIx,
František	František	k1gMnSc1
Kop	kop	k1gInSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
Hála	Hála	k1gMnSc1
<g/>
,	,	kIx,
Štěpán	Štěpán	k1gMnSc1
Markovič	Markovič	k1gMnSc1
<g/>
,	,	kIx,
Svatobor	Svatobor	k1gInSc4
Macák	Macák	k1gMnSc1
<g/>
,	,	kIx,
Radek	Radek	k1gMnSc1
Zapadlo	zapadnout	k5eAaPmAgNnS
<g/>
,	,	kIx,
Ondřej	Ondřej	k1gMnSc1
Štveráček	Štveráček	k1gMnSc1
<g/>
,	,	kIx,
Andrea	Andrea	k1gFnSc1
Kolmanová	Kolmanová	k1gFnSc1
<g/>
,	,	kIx,
Rostislav	Rostislav	k1gMnSc1
Fraš	Fraš	k1gMnSc1
<g/>
,	,	kIx,
Ivan	Ivan	k1gMnSc1
Myslikovjan	Myslikovjan	k1gMnSc1
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
Jiří	Jiří	k1gMnSc1
Šíma	Šíma	k1gMnSc1
(	(	kIx(
<g/>
hudebník	hudebník	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Vítek	Vítek	k1gMnSc1
Malinovský	Malinovský	k2eAgMnSc1d1
<g/>
,	,	kIx,
Mikoláš	Mikoláš	k1gMnSc1
Chadima	Chadima	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
Fiedler	Fiedler	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
Škrna	Škrno	k1gNnSc2
(	(	kIx(
<g/>
profesoři	profesor	k1gMnPc1
Pražské	pražský	k2eAgFnSc2d1
konzervatoře	konzervatoř	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Antonín	Antonín	k1gMnSc1
Mühlhansl	Mühlhansl	k1gInSc1
<g/>
,	,	kIx,
Petri	Petri	k1gNnSc1
Herzanen	Herzanna	k1gFnPc2
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
Kovařík	Kovařík	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
Zlámal	Zlámal	k1gMnSc1
<g/>
,	,	kIx,
Lubor	Lubor	k1gMnSc1
Pokluda	Pokluda	k1gMnSc1
<g/>
,	,	kIx,
Michal	Michal	k1gMnSc1
Žáček	Žáček	k1gMnSc1
<g/>
,	,	kIx,
Radim	Radim	k1gMnSc1
Hanousek	Hanousek	k1gMnSc1
nebo	nebo	k8xC
Jan	Jan	k1gMnSc1
Tříska	Tříska	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Zajímavosti	zajímavost	k1gFnPc1
</s>
<s>
Bill	Bill	k1gMnSc1
Clinton	Clinton	k1gMnSc1
<g/>
,	,	kIx,
exprezident	exprezident	k1gMnSc1
USA	USA	kA
<g/>
,	,	kIx,
s	s	k7c7
oblibou	obliba	k1gFnSc7
hraje	hrát	k5eAaImIp3nS
na	na	k7c4
tenorsaxofon	tenorsaxofon	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Líza	Líza	k1gFnSc1
Simpsonová	Simpsonová	k1gFnSc1
<g/>
,	,	kIx,
postava	postava	k1gFnSc1
z	z	k7c2
kresleného	kreslený	k2eAgInSc2d1
seriálu	seriál	k1gInSc2
Simpsonovi	Simpson	k1gMnSc3
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
takřka	takřka	k6eAd1
geniální	geniální	k2eAgFnSc7d1
hráčkou	hráčka	k1gFnSc7
na	na	k7c4
barytonsaxofon	barytonsaxofon	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Danny	Danna	k1gMnSc2
Smiřický	smiřický	k2eAgMnSc1d1
<g/>
,	,	kIx,
literární	literární	k2eAgNnSc1d1
alterego	alterego	k1gNnSc1
Josefa	Josef	k1gMnSc2
Škvoreckého	Škvorecký	k2eAgMnSc2d1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
neodmyslitelně	odmyslitelně	k6eNd1
spojen	spojit	k5eAaPmNgInS
s	s	k7c7
hrou	hra	k1gFnSc7
na	na	k7c4
tenorsaxofon	tenorsaxofon	k1gInSc4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Adolphe	Adolphat	k5eAaPmIp3nS
Sax	sax	k1gInSc4
<g/>
,	,	kIx,
Malou	malý	k2eAgFnSc4d1
Haine	Hain	k1gInSc5
<g/>
,	,	kIx,
Bruxelles	Bruxellesa	k1gFnPc2
<g/>
,	,	kIx,
Editions	Editionsa	k1gFnPc2
Université	Universita	k1gMnPc1
Bruxelles	Bruxelles	k1gMnSc1
<g/>
,	,	kIx,
1980	#num#	k4
</s>
<s>
Sax	sax	k1gInSc1
<g/>
,	,	kIx,
Mule	mula	k1gFnSc6
&	&	k?
Co	co	k9
<g/>
,	,	kIx,
Jean-Pierre	Jean-Pierr	k1gMnSc5
Thiollet	Thiollet	k1gInSc1
<g/>
,	,	kIx,
Paris	Paris	k1gMnSc1
<g/>
,	,	kIx,
H	H	kA
&	&	k?
D	D	kA
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
2-914266-03-0	2-914266-03-0	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
saxofon	saxofon	k1gInSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Hudba	hudba	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4179250-6	4179250-6	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85117829	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85117829	#num#	k4
</s>
