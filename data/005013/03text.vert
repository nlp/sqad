<s>
Korejština	korejština	k1gFnSc1	korejština
(	(	kIx(	(
<g/>
한	한	k?	한
/	/	kIx~	/
조	조	k?	조
<g/>
;	;	kIx,	;
Hangukmal	Hangukmal	k1gMnSc1	Hangukmal
/	/	kIx~	/
Čosŏ	Čosŏ	k1gMnSc1	Čosŏ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnSc7	který
mluví	mluvit	k5eAaImIp3nS	mluvit
kolem	kolem	k7c2	kolem
78	[number]	k4	78
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
a	a	k8xC	a
Severní	severní	k2eAgFnSc6d1	severní
Koreji	Korea	k1gFnSc6	Korea
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
komunity	komunita	k1gFnSc2	komunita
korejských	korejský	k2eAgMnPc2d1	korejský
emigrantů	emigrant	k1gMnPc2	emigrant
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
,	,	kIx,	,
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
,	,	kIx,	,
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
,	,	kIx,	,
Brazílii	Brazílie	k1gFnSc6	Brazílie
a	a	k8xC	a
na	na	k7c6	na
Filipínách	Filipíny	k1gFnPc6	Filipíny
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Korejština	korejština	k1gFnSc1	korejština
je	být	k5eAaImIp3nS	být
aglutinační	aglutinační	k2eAgInSc4d1	aglutinační
jazyk	jazyk	k1gInSc4	jazyk
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
stupni	stupeň	k1gInPc7	stupeň
vyjádření	vyjádření	k1gNnSc2	vyjádření
zdvořilosti	zdvořilost	k1gFnPc1	zdvořilost
<g/>
.	.	kIx.	.
</s>
<s>
Studijní	studijní	k2eAgInSc1d1	studijní
obor	obor	k1gInSc1	obor
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
zaobírá	zaobírat	k5eAaImIp3nS	zaobírat
studiem	studio	k1gNnSc7	studio
korejštiny	korejština	k1gFnSc2	korejština
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
koreanistika	koreanistika	k1gFnSc1	koreanistika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Koreji	Korea	k1gFnSc6	Korea
a	a	k8xC	a
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Koreji	Korea	k1gFnSc6	Korea
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
korejštinu	korejština	k1gFnSc4	korejština
používají	používat	k5eAaImIp3nP	používat
různé	různý	k2eAgInPc1d1	různý
názvy	název	k1gInPc1	název
založené	založený	k2eAgInPc1d1	založený
na	na	k7c6	na
různém	různý	k2eAgNnSc6d1	různé
pojmenování	pojmenování	k1gNnSc6	pojmenování
samotné	samotný	k2eAgFnSc2d1	samotná
Koreje	Korea	k1gFnSc2	Korea
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Koreji	Korea	k1gFnSc6	Korea
se	se	k3xPyFc4	se
jazyk	jazyk	k1gInSc1	jazyk
nejčastěji	často	k6eAd3	často
nazývá	nazývat	k5eAaImIp3nS	nazývat
Čosŏ	Čosŏ	k1gFnSc1	Čosŏ
(	(	kIx(	(
<g/>
조	조	k?	조
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
formálněji	formálně	k6eAd2	formálně
Čosŏ	Čosŏ	k1gFnSc1	Čosŏ
(	(	kIx(	(
<g/>
조	조	k?	조
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Koreji	Korea	k1gFnSc6	Korea
se	se	k3xPyFc4	se
jazyk	jazyk	k1gInSc1	jazyk
nejčastěji	často	k6eAd3	často
nazývá	nazývat	k5eAaImIp3nS	nazývat
Hangukmal	Hangukmal	k1gInSc1	Hangukmal
(	(	kIx(	(
<g/>
한	한	k?	한
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
formálněji	formálně	k6eAd2	formálně
Hangugŏ	Hangugŏ	k1gFnSc1	Hangugŏ
(	(	kIx(	(
<g/>
한	한	k?	한
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Gugŏ	Gugŏ	k1gFnSc1	Gugŏ
(	(	kIx(	(
<g/>
국	국	k?	국
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
národní	národní	k2eAgInSc4d1	národní
či	či	k8xC	či
domácí	domácí	k2eAgInSc4d1	domácí
jazyk	jazyk	k1gInSc4	jazyk
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lidově	lidově	k6eAd1	lidově
se	se	k3xPyFc4	se
také	také	k9	také
používá	používat	k5eAaImIp3nS	používat
označení	označení	k1gNnSc4	označení
Urimal	Urimal	k1gMnSc1	Urimal
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
náš	náš	k3xOp1gInSc1	náš
jazyk	jazyk	k1gInSc1	jazyk
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
우	우	k?	우
–	–	k?	–
jedno	jeden	k4xCgNnSc4	jeden
slovo	slovo	k1gNnSc4	slovo
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Koreji	Korea	k1gFnSc6	Korea
<g/>
,	,	kIx,	,
우	우	k?	우
말	말	k?	말
–	–	k?	–
dvě	dva	k4xCgNnPc4	dva
slova	slovo	k1gNnPc1	slovo
s	s	k7c7	s
mezerou	mezera	k1gFnSc7	mezera
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Koreji	Korea	k1gFnSc6	Korea
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
zápisu	zápis	k1gInSc3	zápis
korejštiny	korejština	k1gFnSc2	korejština
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
používaly	používat	k5eAaImAgInP	používat
čínské	čínský	k2eAgInPc1d1	čínský
znaky	znak	k1gInPc1	znak
(	(	kIx(	(
<g/>
hanča	hanča	k1gFnSc1	hanča
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
převážně	převážně	k6eAd1	převážně
korejská	korejský	k2eAgFnSc1d1	Korejská
abeceda	abeceda	k1gFnSc1	abeceda
hangul	hangul	k1gInSc1	hangul
a	a	k8xC	a
čínské	čínský	k2eAgInPc1d1	čínský
znaky	znak	k1gInPc1	znak
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
mohou	moct	k5eAaImIp3nP	moct
používat	používat	k5eAaImF	používat
jen	jen	k9	jen
k	k	k7c3	k
zápisu	zápis	k1gInSc2	zápis
sinokorejských	sinokorejský	k2eAgNnPc2d1	sinokorejský
slov	slovo	k1gNnPc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Koreji	Korea	k1gFnSc6	Korea
se	se	k3xPyFc4	se
děti	dítě	k1gFnPc1	dítě
učí	učit	k5eAaImIp3nP	učit
asi	asi	k9	asi
1	[number]	k4	1
800	[number]	k4	800
čínských	čínský	k2eAgInPc2d1	čínský
znaků	znak	k1gInPc2	znak
<g/>
;	;	kIx,	;
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Koreji	Korea	k1gFnSc6	Korea
bylo	být	k5eAaImAgNnS	být
používání	používání	k1gNnSc1	používání
handži	handzat	k5eAaPmIp1nS	handzat
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
už	už	k9	už
před	před	k7c7	před
desítkami	desítka	k1gFnPc7	desítka
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Jádro	jádro	k1gNnSc1	jádro
slovní	slovní	k2eAgFnSc2d1	slovní
zásoby	zásoba	k1gFnSc2	zásoba
korejštiny	korejština	k1gFnSc2	korejština
je	být	k5eAaImIp3nS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
původními	původní	k2eAgInPc7d1	původní
korejskými	korejský	k2eAgNnPc7d1	korejské
slovy	slovo	k1gNnPc7	slovo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kolem	kolem	k7c2	kolem
50	[number]	k4	50
%	%	kIx~	%
slovní	slovní	k2eAgFnSc2d1	slovní
zásoby	zásoba	k1gFnSc2	zásoba
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
odborná	odborný	k2eAgFnSc1d1	odborná
terminologie	terminologie	k1gFnSc1	terminologie
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
sinokorejskými	sinokorejský	k2eAgInPc7d1	sinokorejský
výrazy	výraz	k1gInPc7	výraz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
do	do	k7c2	do
korejštiny	korejština	k1gFnSc2	korejština
pronikly	proniknout	k5eAaPmAgInP	proniknout
výrazy	výraz	k1gInPc1	výraz
z	z	k7c2	z
mongolštiny	mongolština	k1gFnSc2	mongolština
<g/>
,	,	kIx,	,
sanskrtu	sanskrt	k1gInSc2	sanskrt
a	a	k8xC	a
západních	západní	k2eAgInPc2d1	západní
jazyků	jazyk	k1gInPc2	jazyk
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
němčina	němčina	k1gFnSc1	němčina
a	a	k8xC	a
poslední	poslední	k2eAgFnSc7d1	poslední
dobou	doba	k1gFnSc7	doba
hlavně	hlavně	k9	hlavně
angličtina	angličtina	k1gFnSc1	angličtina
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
korejštinou	korejština	k1gFnSc7	korejština
používanou	používaný	k2eAgFnSc7d1	používaná
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Koreji	Korea	k1gFnSc6	Korea
a	a	k8xC	a
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Koreji	Korea	k1gFnSc6	Korea
existují	existovat	k5eAaImIp3nP	existovat
určité	určitý	k2eAgInPc4d1	určitý
rozdíly	rozdíl	k1gInPc4	rozdíl
ve	v	k7c6	v
výslovnosti	výslovnost	k1gFnSc6	výslovnost
<g/>
,	,	kIx,	,
hláskování	hláskování	k1gNnSc6	hláskování
<g/>
,	,	kIx,	,
gramatice	gramatika	k1gFnSc6	gramatika
a	a	k8xC	a
slovní	slovní	k2eAgFnSc6d1	slovní
zásobě	zásoba	k1gFnSc6	zásoba
<g/>
.	.	kIx.	.
</s>
<s>
GLOMB	GLOMB	kA	GLOMB
<g/>
,	,	kIx,	,
VLADIMÍR	Vladimír	k1gMnSc1	Vladimír
<g/>
;	;	kIx,	;
PUCEK	pucka	k1gFnPc2	pucka
<g/>
,	,	kIx,	,
VLADIMÍR	Vladimíra	k1gFnPc2	Vladimíra
<g/>
:	:	kIx,	:
Klasická	klasický	k2eAgFnSc1d1	klasická
korejština	korejština	k1gFnSc1	korejština
<g/>
.	.	kIx.	.
</s>
<s>
Univerzita	univerzita	k1gFnSc1	univerzita
Karlova	Karlův	k2eAgFnSc1d1	Karlova
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Filozofická	filozofický	k2eAgFnSc1d1	filozofická
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978-80-7308-461-5	[number]	k4	978-80-7308-461-5
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
korejština	korejština	k1gFnSc1	korejština
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
korejština	korejština	k1gFnSc1	korejština
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Korejsko-český	korejsko-český	k2eAgInSc1d1	korejsko-český
slovník	slovník	k1gInSc1	slovník
online	onlinout	k5eAaPmIp3nS	onlinout
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
Korean	Korean	k1gInSc1	Korean
Broadcasting	Broadcasting	k1gInSc1	Broadcasting
System	Syst	k1gInSc7	Syst
–	–	k?	–
Kurzy	Kurz	k1gMnPc7	Kurz
základů	základ	k1gInPc2	základ
korejštiny	korejština	k1gFnSc2	korejština
</s>
