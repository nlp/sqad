<s>
Korejština	korejština	k1gFnSc1	korejština
(	(	kIx(	(
<g/>
한	한	k?	한
/	/	kIx~	/
조	조	k?	조
<g/>
;	;	kIx,	;
Hangukmal	Hangukmal	k1gMnSc1	Hangukmal
/	/	kIx~	/
Čosŏ	Čosŏ	k1gMnSc1	Čosŏ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnSc7	který
mluví	mluvit	k5eAaImIp3nS	mluvit
kolem	kolem	k7c2	kolem
78	[number]	k4	78
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
a	a	k8xC	a
Severní	severní	k2eAgFnSc6d1	severní
Koreji	Korea	k1gFnSc6	Korea
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
komunity	komunita	k1gFnSc2	komunita
korejských	korejský	k2eAgMnPc2d1	korejský
emigrantů	emigrant	k1gMnPc2	emigrant
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
,	,	kIx,	,
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
,	,	kIx,	,
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
,	,	kIx,	,
Brazílii	Brazílie	k1gFnSc6	Brazílie
a	a	k8xC	a
na	na	k7c6	na
Filipínách	Filipíny	k1gFnPc6	Filipíny
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
