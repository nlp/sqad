<s>
Koordinovaný	koordinovaný	k2eAgInSc1d1
světový	světový	k2eAgInSc1d1
čas	čas	k1gInSc1
</s>
<s>
Koordinovaný	koordinovaný	k2eAgInSc1d1
světový	světový	k2eAgInSc1d1
čas	čas	k1gInSc1
<g/>
,	,	kIx,
UTC	UTC	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Coordinated	Coordinated	k1gInSc1
Universal	Universal	k1gFnSc2
Time	Tim	k1gFnSc2
<g/>
,	,	kIx,
francouzsky	francouzsky	k6eAd1
Temps	Temps	k1gInSc1
Universel	Universela	k1gFnPc2
Coordonné	Coordonný	k2eAgFnPc1d1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
základem	základ	k1gInSc7
systému	systém	k1gInSc2
občanského	občanský	k2eAgInSc2d1
času	čas	k1gInSc2
<g/>
,	,	kIx,
jednotlivá	jednotlivý	k2eAgNnPc1d1
časová	časový	k2eAgNnPc1d1
pásma	pásmo	k1gNnPc1
jsou	být	k5eAaImIp3nP
definována	definován	k2eAgNnPc1d1
svými	svůj	k3xOyFgFnPc7
odchylkami	odchylka	k1gFnPc7
od	od	k7c2
UTC	UTC	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
UTC	UTC	kA
je	být	k5eAaImIp3nS
jako	jako	k9
základ	základ	k1gInSc1
systému	systém	k1gInSc2
měření	měření	k1gNnSc2
času	čas	k1gInSc2
nástupcem	nástupce	k1gMnSc7
GMT	GMT	kA
(	(	kIx(
<g/>
Greenwich	Greenwich	k1gInSc1
Mean	Mean	k1gNnSc1
Time	Tim	k1gMnSc2
–	–	k?
greenwichský	greenwichský	k2eAgInSc4d1
střední	střední	k2eAgInSc4d1
čas	čas	k1gInSc4
<g/>
)	)	kIx)
a	a	k8xC
v	v	k7c6
neformálním	formální	k2eNgNnSc6d1
vyjadřování	vyjadřování	k1gNnSc6
je	být	k5eAaImIp3nS
s	s	k7c7
ním	on	k3xPp3gInSc7
někdy	někdy	k6eAd1
zaměňován	zaměňován	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
GMT	GMT	kA
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
udává	udávat	k5eAaImIp3nS
čas	čas	k1gInSc4
platný	platný	k2eAgInSc4d1
v	v	k7c6
časovém	časový	k2eAgNnSc6d1
pásmu	pásmo	k1gNnSc6
základního	základní	k2eAgInSc2d1
poledníku	poledník	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
založen	založit	k5eAaPmNgInS
na	na	k7c6
rotaci	rotace	k1gFnSc6
Země	zem	k1gFnSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
UTC	UTC	kA
založen	založit	k5eAaPmNgInS
na	na	k7c6
atomových	atomový	k2eAgFnPc6d1
hodinách	hodina	k1gFnPc6
<g/>
,	,	kIx,
tzn.	tzn.	kA
je	být	k5eAaImIp3nS
na	na	k7c6
rotaci	rotace	k1gFnSc6
Země	zem	k1gFnSc2
teoreticky	teoreticky	k6eAd1
nezávislý	závislý	k2eNgInSc1d1
<g/>
,	,	kIx,
pominou	pominout	k5eAaPmIp3nP
<g/>
-li	-li	k?
se	se	k3xPyFc4
jednak	jednak	k8xC
vlivy	vliv	k1gInPc1
dilatace	dilatace	k1gFnSc2
času	čas	k1gInSc2
a	a	k8xC
jednak	jednak	k8xC
přestupné	přestupný	k2eAgFnPc1d1
sekundy	sekunda	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
ho	on	k3xPp3gMnSc4
s	s	k7c7
GMT	GMT	kA
prakticky	prakticky	k6eAd1
ztotožňují	ztotožňovat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
hlediska	hledisko	k1gNnSc2
relativity	relativita	k1gFnSc2
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
souřadnicový	souřadnicový	k2eAgInSc4d1
čas	čas	k1gInSc4
(	(	kIx(
<g/>
coordinate	coordinat	k1gMnSc5
time	timus	k1gMnSc5
<g/>
)	)	kIx)
<g/>
,	,	kIx,
protože	protože	k8xS
je	být	k5eAaImIp3nS
odvozen	odvodit	k5eAaPmNgInS
od	od	k7c2
Mezinárodního	mezinárodní	k2eAgInSc2d1
atomového	atomový	k2eAgInSc2d1
času	čas	k1gInSc2
TAI	TAI	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Princip	princip	k1gInSc1
</s>
<s>
Jelikož	jelikož	k8xS
se	se	k3xPyFc4
rotace	rotace	k1gFnSc1
Země	zem	k1gFnSc2
mírně	mírně	k6eAd1
zpomaluje	zpomalovat	k5eAaImIp3nS
<g/>
,	,	kIx,
GMT	GMT	kA
se	se	k3xPyFc4
oproti	oproti	k7c3
UTC	UTC	kA
postupně	postupně	k6eAd1
zpožďuje	zpožďovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aby	aby	kYmCp3nS
se	se	k3xPyFc4
UTC	UTC	kA
dal	dát	k5eAaPmAgInS
používat	používat	k5eAaImF
v	v	k7c6
praktickém	praktický	k2eAgInSc6d1
životě	život	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
s	s	k7c7
rotací	rotace	k1gFnSc7
Země	zem	k1gFnSc2
spjatý	spjatý	k2eAgInSc4d1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
udržován	udržovat	k5eAaImNgInS
v	v	k7c6
rozmezí	rozmezí	k1gNnSc6
±	±	k?
<g/>
0,9	0,9	k4
sekundy	sekunda	k1gFnSc2
od	od	k7c2
UT	UT	kA
<g/>
1	#num#	k4
<g/>
;	;	kIx,
pokud	pokud	k8xS
je	být	k5eAaImIp3nS
tato	tento	k3xDgFnSc1
odchylka	odchylka	k1gFnSc1
překročena	překročit	k5eAaPmNgFnS
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
o	o	k7c6
půlnoci	půlnoc	k1gFnSc6
nejbližšího	blízký	k2eAgMnSc2d3
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
nebo	nebo	k8xC
31	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
prosince	prosinec	k1gInSc2
přidána	přidat	k5eAaPmNgFnS
nebo	nebo	k8xC
(	(	kIx(
<g/>
teoreticky	teoreticky	k6eAd1
<g/>
)	)	kIx)
ubrána	ubrán	k2eAgFnSc1d1
tzv.	tzv.	kA
přestupná	přestupný	k2eAgFnSc1d1
sekunda	sekunda	k1gFnSc1
<g/>
,	,	kIx,
takže	takže	k8xS
tento	tento	k3xDgInSc1
den	den	k1gInSc1
končí	končit	k5eAaImIp3nS
v	v	k7c6
čase	čas	k1gInSc6
23.59	23.59	k4
<g/>
:	:	kIx,
<g/>
60	#num#	k4
<g/>
,	,	kIx,
resp.	resp.	kA
23.59	23.59	k4
<g/>
:	:	kIx,
<g/>
58	#num#	k4
(	(	kIx(
<g/>
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
běžného	běžný	k2eAgMnSc2d1
23.59	23.59	k4
<g/>
:	:	kIx,
<g/>
59	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
tomuto	tento	k3xDgInSc3
dochází	docházet	k5eAaImIp3nS
průměrně	průměrně	k6eAd1
jednou	jeden	k4xCgFnSc7
za	za	k7c4
rok	rok	k1gInSc4
až	až	k8xS
rok	rok	k1gInSc4
a	a	k8xC
půl	půl	k1xP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jelikož	jelikož	k8xS
se	se	k3xPyFc4
rotace	rotace	k1gFnSc1
Země	zem	k1gFnSc2
zpomaluje	zpomalovat	k5eAaImIp3nS
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
vždy	vždy	k6eAd1
přestupné	přestupný	k2eAgFnPc1d1
sekundy	sekunda	k1gFnPc1
přidávány	přidáván	k2eAgFnPc1d1
<g/>
,	,	kIx,
teoreticky	teoreticky	k6eAd1
se	se	k3xPyFc4
však	však	k9
počítá	počítat	k5eAaImIp3nS
i	i	k9
s	s	k7c7
možností	možnost	k1gFnSc7
ubrané	ubraný	k2eAgFnSc2d1
přestupné	přestupný	k2eAgFnSc2d1
sekundy	sekunda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
zda	zda	k8xS
bude	být	k5eAaImBp3nS
v	v	k7c6
příslušném	příslušný	k2eAgInSc6d1
termínu	termín	k1gInSc6
přestupná	přestupný	k2eAgFnSc1d1
sekunda	sekunda	k1gFnSc1
použita	použít	k5eAaPmNgFnS
<g/>
,	,	kIx,
rozhoduje	rozhodovat	k5eAaImIp3nS
International	International	k1gFnSc1
Earth	Earth	k1gInSc4
Rotation	Rotation	k1gInSc1
and	and	k?
Reference	reference	k1gFnPc1
Systems	Systemsa	k1gFnPc2
Service	Service	k1gFnSc2
podle	podle	k7c2
měření	měření	k1gNnSc2
rotace	rotace	k1gFnSc2
Země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1
standard	standard	k1gInSc1
času	čas	k1gInSc2
UTC	UTC	kA
je	být	k5eAaImIp3nS
určován	určovat	k5eAaImNgInS
z	z	k7c2
mnoha	mnoho	k4c2
národních	národní	k2eAgFnPc2d1
časových	časový	k2eAgFnPc2d1
laboratoří	laboratoř	k1gFnPc2
vybavených	vybavený	k2eAgFnPc2d1
atomovými	atomový	k2eAgFnPc7d1
hodinami	hodina	k1gFnPc7
<g/>
,	,	kIx,
z	z	k7c2
těchto	tento	k3xDgNnPc2
měření	měření	k1gNnPc2
pak	pak	k6eAd1
Bureau	Burea	k2eAgFnSc4d1
International	International	k1gFnSc4
des	des	k1gNnSc2
Poids	Poidsa	k1gFnPc2
et	et	k?
Mesures	Mesures	k1gInSc1
(	(	kIx(
<g/>
Mezinárodní	mezinárodní	k2eAgInSc1d1
úřad	úřad	k1gInSc1
pro	pro	k7c4
míry	míra	k1gFnPc4
a	a	k8xC
váhy	váha	k1gFnPc4
<g/>
)	)	kIx)
zpětně	zpětně	k6eAd1
určí	určit	k5eAaPmIp3nS
hodnotu	hodnota	k1gFnSc4
UTC	UTC	kA
<g/>
,	,	kIx,
neboť	neboť	k8xC
jednotlivé	jednotlivý	k2eAgFnPc4d1
atomové	atomový	k2eAgFnPc4d1
hodiny	hodina	k1gFnPc4
se	se	k3xPyFc4
mohou	moct	k5eAaImIp3nP
o	o	k7c4
několik	několik	k4yIc4
nanosekund	nanosekunda	k1gFnPc2
navzájem	navzájem	k6eAd1
lišit	lišit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
rychlejší	rychlý	k2eAgFnSc4d2
zpětné	zpětný	k2eAgNnSc1d1
stanovení	stanovení	k1gNnSc1
času	čas	k1gInSc2
je	být	k5eAaImIp3nS
vydávána	vydáván	k2eAgFnSc1d1
verze	verze	k1gFnSc1
"	"	kIx"
<g/>
UTC	UTC	kA
rapid	rapid	k1gInSc1
<g/>
"	"	kIx"
označovaná	označovaný	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
UTCr	UTCr	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Systém	systém	k1gInSc1
GPS	GPS	kA
však	však	k9
používá	používat	k5eAaImIp3nS
odlišné	odlišný	k2eAgNnSc4d1
stanovení	stanovení	k1gNnSc4
času	čas	k1gInSc2
podle	podle	k7c2
U.	U.	kA
<g/>
S.	S.	kA
Naval	navalit	k5eAaPmRp2nS
Observatory	Observator	k1gInPc4
<g/>
,	,	kIx,
označované	označovaný	k2eAgFnSc2d1
jako	jako	k8xS,k8xC
UTC	UTC	kA
<g/>
(	(	kIx(
<g/>
USNO	USNO	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navíc	navíc	k6eAd1
se	se	k3xPyFc4
od	od	k7c2
roku	rok	k1gInSc2
1980	#num#	k4
do	do	k7c2
času	čas	k1gInSc2
GPS	GPS	kA
nepřidávají	přidávat	k5eNaImIp3nP
přestupné	přestupný	k2eAgFnPc1d1
sekundy	sekunda	k1gFnPc1
(	(	kIx(
<g/>
rozdíl	rozdíl	k1gInSc1
od	od	k7c2
TAI	TAI	kA
zůstává	zůstávat	k5eAaImIp3nS
na	na	k7c4
19	#num#	k4
s	s	k7c7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
ale	ale	k8xC
softwarově	softwarově	k6eAd1
přijímači	přijímač	k1gMnPc7
korigováno	korigován	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Zkratka	zkratka	k1gFnSc1
</s>
<s>
Zkratka	zkratka	k1gFnSc1
UTC	UTC	kA
není	být	k5eNaImIp3nS
přímou	přímý	k2eAgFnSc7d1
zkratkou	zkratka	k1gFnSc7
plného	plný	k2eAgInSc2d1
názvu	název	k1gInSc2
další	další	k2eAgFnSc2d1
varianty	varianta	k1gFnSc2
světového	světový	k2eAgInSc2d1
<g/>
,	,	kIx,
univerzálního	univerzální	k2eAgInSc2d1
času	čas	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
vytvořena	vytvořit	k5eAaPmNgFnS
přidáním	přidání	k1gNnSc7
přípony	přípona	k1gFnSc2
C	C	kA
(	(	kIx(
<g/>
Coordinated	Coordinated	k1gMnSc1
<g/>
)	)	kIx)
ke	k	k7c3
zkratce	zkratka	k1gFnSc3
UT	UT	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
je	být	k5eAaImIp3nS
kompromisem	kompromis	k1gInSc7
mezi	mezi	k7c7
anglickou	anglický	k2eAgFnSc7d1
zkratkou	zkratka	k1gFnSc7
CUT	CUT	kA
(	(	kIx(
<g/>
Coordinated	Coordinated	k1gMnSc1
Universal	Universal	k1gFnSc2
Time	Tim	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
francouzskou	francouzský	k2eAgFnSc7d1
zkratkou	zkratka	k1gFnSc7
TUC	TUC	kA
(	(	kIx(
<g/>
temps	tempsa	k1gFnPc2
universel	universlo	k1gNnPc2
coordonné	coordonný	k2eAgFnPc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Za	za	k7c7
časovým	časový	k2eAgInSc7d1
údajem	údaj	k1gInSc7
(	(	kIx(
<g/>
např.	např.	kA
v	v	k7c6
normě	norma	k1gFnSc6
ISO	ISO	kA
8601	#num#	k4
<g/>
)	)	kIx)
se	se	k3xPyFc4
UTC	UTC	kA
často	často	k6eAd1
označuje	označovat	k5eAaImIp3nS
písmenem	písmeno	k1gNnSc7
Z	z	k7c2
<g/>
,	,	kIx,
případně	případně	k6eAd1
(	(	kIx(
<g/>
anglickým	anglický	k2eAgInSc7d1
<g/>
)	)	kIx)
hláskovacím	hláskovací	k2eAgInSc7d1
výrazem	výraz	k1gInSc7
Zulu	Zulu	k1gMnSc1
podle	podle	k7c2
hláskovací	hláskovací	k2eAgFnSc2d1
tabulky	tabulka	k1gFnSc2
Mezinárodní	mezinárodní	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
pro	pro	k7c4
civilní	civilní	k2eAgNnSc4d1
letectví	letectví	k1gNnSc4
ICAO	ICAO	kA
<g/>
.	.	kIx.
</s>
<s>
Čas	čas	k1gInSc1
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Místní	místní	k2eAgInSc1d1
čas	čas	k1gInSc1
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
je	být	k5eAaImIp3nS
dán	dát	k5eAaPmNgInS
jako	jako	k9
UTC	UTC	kA
<g/>
+	+	kIx~
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
(	(	kIx(
<g/>
v	v	k7c6
zimním	zimní	k2eAgNnSc6d1
období	období	k1gNnSc6
<g/>
)	)	kIx)
a	a	k8xC
jako	jako	k9
UTC	UTC	kA
<g/>
+	+	kIx~
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
v	v	k7c6
období	období	k1gNnSc6
letního	letní	k2eAgInSc2d1
času	čas	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
<g/>
-li	-li	k?
čas	čas	k1gInSc1
čehokoliv	cokoliv	k3yInSc2
udán	udán	k2eAgMnSc1d1
například	například	k6eAd1
na	na	k7c4
„	„	k?
<g/>
16	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
UTC	UTC	kA
<g/>
“	“	k?
<g/>
,	,	kIx,
v	v	k7c6
ČR	ČR	kA
to	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
17	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
místního	místní	k2eAgInSc2d1
času	čas	k1gInSc2
(	(	kIx(
<g/>
v	v	k7c6
zimě	zima	k1gFnSc6
<g/>
)	)	kIx)
nebo	nebo	k8xC
18	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
(	(	kIx(
<g/>
v	v	k7c6
letním	letní	k2eAgInSc6d1
čase	čas	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Universal	Universat	k5eAaImAgInS,k5eAaPmAgInS
Time	Time	k1gInSc1
</s>
<s>
Základní	základní	k2eAgInSc1d1
poledník	poledník	k1gInSc1
</s>
<s>
Časové	časový	k2eAgNnSc1d1
pásmo	pásmo	k1gNnSc1
</s>
<s>
NTP	NTP	kA
</s>
<s>
Terestrický	terestrický	k2eAgInSc1d1
čas	čas	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Koordinovaný	koordinovaný	k2eAgInSc4d1
světový	světový	k2eAgInSc4d1
čas	čas	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Časový	časový	k2eAgInSc1d1
server	server	k1gInSc1
BIPM	BIPM	kA
</s>
<s>
time	time	k1gFnSc1
<g/>
.	.	kIx.
<g/>
gov	gov	k?
–	–	k?
Oficiální	oficiální	k2eAgInSc4d1
čas	čas	k1gInSc4
USA	USA	kA
</s>
<s>
cmi	cmi	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
–	–	k?
Český	český	k2eAgInSc1d1
metrologický	metrologický	k2eAgInSc1d1
institut	institut	k1gInSc1
</s>
<s>
Informace	informace	k1gFnPc1
o	o	k7c6
aktuální	aktuální	k2eAgFnSc6d1
přestupné	přestupný	k2eAgFnSc6d1
sekundě	sekunda	k1gFnSc6
</s>
