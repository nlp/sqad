<s>
Koordinovaný	koordinovaný	k2eAgInSc1d1
světový	světový	k2eAgInSc1d1
čas	čas	k1gInSc1
<g/>
,	,	kIx,
UTC	UTC	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Coordinated	Coordinated	k1gInSc1
Universal	Universal	k1gFnSc2
Time	Tim	k1gFnSc2
<g/>
,	,	kIx,
francouzsky	francouzsky	k6eAd1
Temps	Temps	k1gInSc1
Universel	Universela	k1gFnPc2
Coordonné	Coordonný	k2eAgFnPc1d1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
základem	základ	k1gInSc7
systému	systém	k1gInSc2
občanského	občanský	k2eAgInSc2d1
času	čas	k1gInSc2
<g/>
,	,	kIx,
jednotlivá	jednotlivý	k2eAgNnPc1d1
časová	časový	k2eAgNnPc1d1
pásma	pásmo	k1gNnPc1
jsou	být	k5eAaImIp3nP
definována	definován	k2eAgNnPc1d1
svými	svůj	k3xOyFgFnPc7
odchylkami	odchylka	k1gFnPc7
od	od	k7c2
UTC	UTC	kA
<g/>
.	.	kIx.
</s>