<s>
Clive	Clivat	k5eAaPmIp3nS	Clivat
Staples	Staples	k1gInSc1	Staples
Lewis	Lewis	k1gFnSc2	Lewis
(	(	kIx(	(
<g/>
29	[number]	k4	29
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1898	[number]	k4	1898
<g/>
,	,	kIx,	,
Belfast	Belfast	k1gInSc1	Belfast
-	-	kIx~	-
22	[number]	k4	22
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
Oxford	Oxford	k1gInSc1	Oxford
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xC	jako
C.	C.	kA	C.
S.	S.	kA	S.
Lewis	Lewis	k1gFnPc2	Lewis
nebo	nebo	k8xC	nebo
pod	pod	k7c7	pod
přezdívkou	přezdívka	k1gFnSc7	přezdívka
Jack	Jack	k1gMnSc1	Jack
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
irský	irský	k2eAgMnSc1d1	irský
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejúspěšnějších	úspěšný	k2eAgMnPc2d3	nejúspěšnější
autorů	autor	k1gMnPc2	autor
moderní	moderní	k2eAgFnSc2d1	moderní
britské	britský	k2eAgFnSc2d1	britská
historie	historie	k1gFnSc2	historie
<g/>
.	.	kIx.	.
</s>
