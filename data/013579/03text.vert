<s>
Daniel	Daniel	k1gMnSc1
Ricciardo	Ricciardo	k1gNnSc4
</s>
<s>
Daniel	Daniel	k1gMnSc1
Ricciardo	Ricciardo	k1gNnSc4
</s>
<s>
Daniel	Daniel	k1gMnSc1
Ricciardo	Ricciardo	k1gNnSc1
v	v	k7c6
roce	rok	k1gInSc6
2019	#num#	k4
<g/>
Stát	stát	k1gInSc1
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
Austrálie	Austrálie	k1gFnSc2
Narození	narození	k1gNnSc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1989	#num#	k4
(	(	kIx(
<g/>
31	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Perth	Perth	k1gInSc1
<g/>
,	,	kIx,
Austrálie	Austrálie	k1gFnPc1
Současné	současný	k2eAgNnSc1d1
působení	působení	k1gNnSc1
ve	v	k7c6
Formuli	formule	k1gFnSc6
1	#num#	k4
Současný	současný	k2eAgInSc1d1
tým	tým	k1gInSc1
</s>
<s>
McLaren	McLarna	k1gFnPc2
<g/>
–	–	k?
<g/>
Mercedes	mercedes	k1gInSc1
Číslo	číslo	k1gNnSc1
vozu	vůz	k1gInSc2
</s>
<s>
3	#num#	k4
Umístění	umístění	k1gNnSc1
(	(	kIx(
<g/>
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
(	(	kIx(
<g/>
14	#num#	k4
b.	b.	k?
<g/>
)	)	kIx)
Kariéra	kariéra	k1gFnSc1
ve	v	k7c6
Formuli	formule	k1gFnSc6
1	#num#	k4
Aktivní	aktivní	k2eAgFnSc2d1
sezóny	sezóna	k1gFnSc2
</s>
<s>
2011	#num#	k4
<g/>
–	–	k?
<g/>
dosud	dosud	k6eAd1
Týmy	tým	k1gInPc7
</s>
<s>
HRT	HRT	kA
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
<g/>
Toro	Toro	k?
Rosso	Rossa	k1gFnSc5
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
–	–	k?
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
Red	Red	k1gFnSc1
Bull	bulla	k1gFnPc2
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
–	–	k?
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
Renault	renault	k1gInSc1
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
–	–	k?
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
McLaren	McLarna	k1gFnPc2
(	(	kIx(
<g/>
2021	#num#	k4
<g/>
–	–	k?
<g/>
dosud	dosud	k6eAd1
<g/>
)	)	kIx)
Závody	závod	k1gInPc1
</s>
<s>
190	#num#	k4
(	(	kIx(
<g/>
190	#num#	k4
startů	start	k1gInPc2
<g/>
)	)	kIx)
Mistr	mistr	k1gMnSc1
světa	svět	k1gInSc2
</s>
<s>
0	#num#	k4
Vyhrané	vyhraný	k2eAgInPc1d1
závody	závod	k1gInPc1
</s>
<s>
7	#num#	k4
Stupně	stupeň	k1gInSc2
vítězů	vítěz	k1gMnPc2
</s>
<s>
31	#num#	k4
Pole	pole	k1gNnSc1
positions	positionsa	k1gFnPc2
</s>
<s>
3	#num#	k4
Nejrychlejší	rychlý	k2eAgFnSc1d3
kola	kola	k1gFnSc1
</s>
<s>
15	#num#	k4
Body	bod	k1gInPc7
celkem	celkem	k6eAd1
</s>
<s>
1	#num#	k4
173	#num#	k4
Nejlepší	dobrý	k2eAgInPc1d3
umístění	umístění	k1gNnSc4
v	v	k7c6
sezóně	sezóna	k1gFnSc6
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
)	)	kIx)
Nejlepší	dobrý	k2eAgNnSc1d3
umístění	umístění	k1gNnSc1
v	v	k7c6
závodě	závod	k1gInSc6
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
První	první	k4xOgInSc1
závod	závod	k1gInSc1
</s>
<s>
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Velké	velká	k1gFnSc2
Británie	Británie	k1gFnSc2
2011	#num#	k4
První	první	k4xOgInSc4
vítězství	vítězství	k1gNnPc2
</s>
<s>
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Kanady	Kanada	k1gFnSc2
2014	#num#	k4
Poslední	poslední	k2eAgInSc4d1
vítězství	vítězství	k1gNnSc1
</s>
<s>
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Monaka	Monako	k1gNnSc2
2018	#num#	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Daniel	Daniel	k1gMnSc1
Ricciardo	Ricciardo	k1gFnSc1
(	(	kIx(
<g/>
*	*	kIx~
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1989	#num#	k4
<g/>
,	,	kIx,
Perth	Perth	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
australský	australský	k2eAgMnSc1d1
pilot	pilot	k1gMnSc1
Formule	formule	k1gFnSc2
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
sezonu	sezona	k1gFnSc4
2011	#num#	k4
nahradil	nahradit	k5eAaPmAgInS
v	v	k7c6
týmu	tým	k1gInSc6
Hispania	Hispanium	k1gNnSc2
Racing	Racing	k1gInSc4
Naraina	Naraino	k1gNnSc2
Karthikeyana	Karthikeyan	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
2012	#num#	k4
a	a	k8xC
2013	#num#	k4
závodil	závodit	k5eAaImAgInS
v	v	k7c6
týmu	tým	k1gInSc6
Toro	Toro	k1gNnSc6
Rosso	Rosso	k1gNnSc6
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
přešel	přejít	k5eAaPmAgMnS
k	k	k7c3
Red	Red	k1gInSc3
Bullu	bull	k1gInSc3
<g/>
,	,	kIx,
kde	kde	k6eAd1
třikrát	třikrát	k6eAd1
vyhrál	vyhrát	k5eAaPmAgMnS
<g/>
,	,	kIx,
poprvé	poprvé	k6eAd1
při	při	k7c6
GP	GP	kA
Kanady	Kanada	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nyní	nyní	k6eAd1
závodí	závodit	k5eAaImIp3nS
za	za	k7c4
tým	tým	k1gInSc4
McLaren	McLarna	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
nahradil	nahradit	k5eAaPmAgMnS
Carlose	Carlosa	k1gFnSc3
Sainze	Sainza	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Kariéra	kariéra	k1gFnSc1
před	před	k7c7
Formulí	formule	k1gFnSc7
1	#num#	k4
</s>
<s>
Začátky	začátek	k1gInPc1
</s>
<s>
Od	od	k7c2
devíti	devět	k4xCc2
let	léto	k1gNnPc2
startoval	startovat	k5eAaBmAgMnS
na	na	k7c6
motokárách	motokára	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
přestoupil	přestoupit	k5eAaPmAgMnS
do	do	k7c2
šampionátu	šampionát	k1gInSc2
Západní	západní	k2eAgFnSc2d1
Austrálie	Austrálie	k1gFnSc2
Formule	formule	k1gFnSc1
Ford	ford	k1gInSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
skončil	skončit	k5eAaPmAgMnS
osmý	osmý	k4xOgMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
získal	získat	k5eAaPmAgMnS
stipendium	stipendium	k1gNnSc4
u	u	k7c2
týmu	tým	k1gInSc2
Eurasia	Eurasius	k1gMnSc2
Motorsport	Motorsport	k1gInSc4
<g/>
,	,	kIx,
se	s	k7c7
kterým	který	k3yRgMnSc7,k3yIgMnSc7,k3yQgMnSc7
se	se	k3xPyFc4
zúčastnil	zúčastnit	k5eAaPmAgMnS
asijského	asijský	k2eAgNnSc2d1
mistrovství	mistrovství	k1gNnSc2
Formule	formule	k1gFnSc2
BMW	BMW	kA
<g/>
,	,	kIx,
během	během	k7c2
sezony	sezona	k1gFnSc2
si	se	k3xPyFc3
vyjel	vyjet	k5eAaPmAgMnS
pole	pole	k1gNnSc4
position	position	k1gInSc1
a	a	k8xC
dvakrát	dvakrát	k6eAd1
zvítězil	zvítězit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
poháru	pohár	k1gInSc6
jezdců	jezdec	k1gMnPc2
skončil	skončit	k5eAaPmAgMnS
třetí	třetí	k4xOgMnSc1
s	s	k7c7
231	#num#	k4
body	bod	k1gInPc7
<g/>
,	,	kIx,
59	#num#	k4
bodů	bod	k1gInPc2
za	za	k7c7
mistrem	mistr	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
srpnu	srpen	k1gInSc6
2006	#num#	k4
odjel	odjet	k5eAaPmAgMnS
jeden	jeden	k4xCgInSc4
závod	závod	k1gInSc4
v	v	k7c6
britské	britský	k2eAgFnSc6d1
sérii	série	k1gFnSc6
Formule	formule	k1gFnSc2
BMW	BMW	kA
<g/>
,	,	kIx,
dojel	dojet	k5eAaPmAgMnS
osmý	osmý	k4xOgMnSc1
a	a	k8xC
obdržel	obdržet	k5eAaPmAgMnS
3	#num#	k4
body	bod	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
závěru	závěr	k1gInSc6
roku	rok	k1gInSc2
získal	získat	k5eAaPmAgMnS
pátou	pátý	k4xOgFnSc4
příčku	příčka	k1gFnSc4
ve	v	k7c6
Světovém	světový	k2eAgNnSc6d1
finále	finále	k1gNnSc6
F	F	kA
BMW	BMW	kA
v	v	k7c6
týmu	tým	k1gInSc6
Fortec	Fortec	k1gMnSc1
Motorsport	Motorsport	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Formule	formule	k1gFnSc1
Renault	renault	k1gInSc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
se	se	k3xPyFc4
přesunul	přesunout	k5eAaPmAgMnS
do	do	k7c2
Evropy	Evropa	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
startoval	startovat	k5eAaBmAgInS
v	v	k7c6
evropském	evropský	k2eAgInSc6d1
a	a	k8xC
italském	italský	k2eAgInSc6d1
šampionátu	šampionát	k1gInSc6
Formule	formule	k1gFnSc1
Renault	renault	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
italské	italský	k2eAgFnSc6d1
verzi	verze	k1gFnSc6
se	s	k7c7
196	#num#	k4
body	bod	k1gInPc7
a	a	k8xC
jedním	jeden	k4xCgNnSc7
pódiem	pódium	k1gNnSc7
obsadil	obsadit	k5eAaPmAgMnS
sedmou	sedmý	k4xOgFnSc4
příčku	příčka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
evropské	evropský	k2eAgFnSc6d1
mutaci	mutace	k1gFnSc6
odjel	odjet	k5eAaPmAgMnS
pouze	pouze	k6eAd1
několik	několik	k4yIc4
grand	grand	k1gMnSc1
prix	prix	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
stejném	stejný	k2eAgInSc6d1
voze	vůz	k1gInSc6
zůstal	zůstat	k5eAaPmAgInS
i	i	k9
v	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
<g/>
,	,	kIx,
působil	působit	k5eAaImAgMnS
v	v	k7c6
šampionátech	šampionát	k1gInPc6
Evropy	Evropa	k1gFnSc2
a	a	k8xC
západní	západní	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
si	se	k3xPyFc3
vyjel	vyjet	k5eAaPmAgInS
titul	titul	k1gInSc4
a	a	k8xC
v	v	k7c6
Eurocupu	Eurocup	k1gInSc6
získal	získat	k5eAaPmAgMnS
druhou	druhý	k4xOgFnSc4
pozici	pozice	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Formule	formule	k1gFnSc1
3	#num#	k4
</s>
<s>
Uprostřed	uprostřed	k7c2
sezony	sezona	k1gFnSc2
2008	#num#	k4
debutoval	debutovat	k5eAaBmAgMnS
ve	v	k7c6
formuli	formule	k1gFnSc6
3	#num#	k4
na	na	k7c6
Nürburgringu	Nürburgring	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvalifikoval	kvalifikovat	k5eAaBmAgMnS
se	se	k3xPyFc4
osmý	osmý	k4xOgMnSc1
a	a	k8xC
dojel	dojet	k5eAaPmAgMnS
šestý	šestý	k4xOgMnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
si	se	k3xPyFc3
vybral	vybrat	k5eAaPmAgMnS
za	za	k7c4
své	svůj	k3xOyFgNnSc4
působiště	působiště	k1gNnSc4
tým	tým	k1gInSc4
Carlin	Carlin	k2eAgInSc4d1
Motorsport	Motorsport	k1gInSc4
v	v	k7c6
britské	britský	k2eAgFnSc6d1
formuli	formule	k1gFnSc6
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
absolvoval	absolvovat	k5eAaPmAgMnS
svou	svůj	k3xOyFgFnSc4
premiéru	premiéra	k1gFnSc4
ve	v	k7c4
World	World	k1gInSc4
Series	Series	k1gInSc1
by	by	kYmCp3nS
Renault	renault	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Britský	britský	k2eAgInSc1d1
šampionát	šampionát	k1gInSc1
F3	F3	k1gFnPc2
vyhrál	vyhrát	k5eAaPmAgInS
díky	díky	k7c3
87	#num#	k4
bodům	bod	k1gInPc3
jako	jako	k8xS,k8xC
první	první	k4xOgMnSc1
Australan	Australan	k1gMnSc1
po	po	k7c6
Davidu	David	k1gMnSc6
Brabhamovi	Brabham	k1gMnSc6
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
to	ten	k3xDgNnSc4
dokázal	dokázat	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
týmem	tým	k1gInSc7
pokračoval	pokračovat	k5eAaImAgInS
i	i	k9
ve	v	k7c6
slavné	slavný	k2eAgFnSc6d1
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Macaa	Macao	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
po	po	k7c6
šestém	šestý	k4xOgNnSc6
místě	místo	k1gNnSc6
v	v	k7c6
kvalifikaci	kvalifikace	k1gFnSc6
závod	závod	k1gInSc1
nedokončil	dokončit	k5eNaPmAgInS
<g/>
.	.	kIx.
</s>
<s>
Formule	formule	k1gFnSc1
Renault	renault	k1gInSc1
3.5	3.5	k4
</s>
<s>
30	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2009	#num#	k4
podepsal	podepsat	k5eAaPmAgMnS
smlouvu	smlouva	k1gFnSc4
se	s	k7c7
stájí	stáj	k1gFnSc7
Tech	Tech	k?
1	#num#	k4
pro	pro	k7c4
sezonu	sezona	k1gFnSc4
2010	#num#	k4
v	v	k7c6
sérii	série	k1gFnSc6
Formule	formule	k1gFnSc2
Renault	renault	k1gInSc1
3.5	3.5	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
celkovém	celkový	k2eAgNnSc6d1
pořadí	pořadí	k1gNnSc6
skončil	skončit	k5eAaPmAgInS
druhý	druhý	k4xOgInSc1
s	s	k7c7
8	#num#	k4
pole	pole	k1gNnSc2
position	position	k1gInSc1
a	a	k8xC
čtyřmi	čtyři	k4xCgFnPc7
výhrami	výhra	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
jezdil	jezdit	k5eAaImAgMnS
pro	pro	k7c4
tým	tým	k1gInSc4
ISR	ISR	kA
Racing	Racing	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Formule	formule	k1gFnSc1
1	#num#	k4
</s>
<s>
Ve	v	k7c6
formuli	formule	k1gFnSc6
jedna	jeden	k4xCgFnSc1
se	se	k3xPyFc4
poprvé	poprvé	k6eAd1
svezl	svézt	k5eAaPmAgMnS
v	v	k7c6
prosinci	prosinec	k1gInSc6
2009	#num#	k4
<g/>
,	,	kIx,
když	když	k8xS
testoval	testovat	k5eAaImAgMnS
pro	pro	k7c4
Red	Red	k1gFnSc4
Bull	bulla	k1gFnPc2
Racing	Racing	k1gInSc4
v	v	k7c6
rámci	rámec	k1gInSc6
testu	test	k1gInSc2
mladých	mladý	k2eAgMnPc2d1
jezdců	jezdec	k1gMnPc2
na	na	k7c6
okruhu	okruh	k1gInSc6
Jerez	Jerez	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
posledním	poslední	k2eAgInSc6d1
dni	den	k1gInSc6
testování	testování	k1gNnSc4
zajel	zajet	k5eAaPmAgInS
Ricciardo	Ricciardo	k1gNnSc4
nejrychlejší	rychlý	k2eAgInSc1d3
čas	čas	k1gInSc1
o	o	k7c4
více	hodně	k6eAd2
jak	jak	k8xS,k8xC
sekundu	sekunda	k1gFnSc4
lepší	dobrý	k2eAgMnSc1d2
než	než	k8xS
ostatní	ostatní	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
tomu	ten	k3xDgNnSc3
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
jedním	jeden	k4xCgMnSc7
z	z	k7c2
testovacích	testovací	k2eAgMnPc2d1
a	a	k8xC
rezervních	rezervní	k2eAgMnPc2d1
jezdců	jezdec	k1gMnPc2
jak	jak	k8xC,k8xS
pro	pro	k7c4
Red	Red	k1gFnSc4
Bull	bulla	k1gFnPc2
Racing	Racing	k1gInSc4
<g/>
,	,	kIx,
tak	tak	k6eAd1
pro	pro	k7c4
jeho	jeho	k3xOp3gFnSc4
sesterskou	sesterský	k2eAgFnSc4d1
stáj	stáj	k1gFnSc4
Scuderia	Scuderium	k1gNnSc2
Toro	Toro	k?
Rosso	Rossa	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
listopadu	listopad	k1gInSc6
2010	#num#	k4
se	se	k3xPyFc4
opět	opět	k6eAd1
zúčastnil	zúčastnit	k5eAaPmAgMnS
jako	jako	k8xC,k8xS
jediný	jediný	k2eAgMnSc1d1
jezdec	jezdec	k1gMnSc1
Red	Red	k1gFnSc2
Bull	bulla	k1gFnPc2
Racingu	Racing	k1gInSc2
testu	test	k1gInSc2
mladých	mladý	k2eAgMnPc2d1
jezdců	jezdec	k1gMnPc2
na	na	k7c6
konci	konec	k1gInSc6
sezony	sezona	k1gFnSc2
<g/>
,	,	kIx,
kterému	který	k3yQgInSc3,k3yRgInSc3,k3yIgInSc3
zcela	zcela	k6eAd1
dominoval	dominovat	k5eAaImAgMnS
<g/>
,	,	kIx,
když	když	k8xS
zajel	zajet	k5eAaPmAgMnS
o	o	k7c4
1,3	1,3	k4
sekundy	sekunda	k1gFnSc2
rychlejší	rychlý	k2eAgInSc4d2
čas	čas	k1gInSc4
než	než	k8xS
Vettelovo	Vettelův	k2eAgNnSc4d1
kvalifikační	kvalifikační	k2eAgNnSc4d1
kolo	kolo	k1gNnSc4
v	v	k7c4
sobotu	sobota	k1gFnSc4
předtím	předtím	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
sezonu	sezona	k1gFnSc4
2011	#num#	k4
byl	být	k5eAaImAgInS
26	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2010	#num#	k4
oznámen	oznámit	k5eAaPmNgInS
jako	jako	k8xC,k8xS
testovací	testovací	k2eAgInSc1d1
a	a	k8xC
náhradní	náhradní	k2eAgMnSc1d1
pilot	pilot	k1gMnSc1
Toro	Toro	k?
Rosso	Rossa	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Účastnil	účastnit	k5eAaImAgMnS
se	se	k3xPyFc4
pátečních	páteční	k2eAgInPc2d1
tréninků	trénink	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Velké	velká	k1gFnSc2
Británie	Británie	k1gFnSc2
2011	#num#	k4
debutoval	debutovat	k5eAaBmAgInS
za	za	k7c4
tým	tým	k1gInSc4
Hispania	Hispanium	k1gNnSc2
Racing	Racing	k1gInSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
nahradil	nahradit	k5eAaPmAgInS
Inda	Ind	k1gMnSc4
Naraina	Narain	k1gMnSc4
Karthikeyana	Karthikeyan	k1gMnSc4
<g/>
,	,	kIx,
v	v	k7c6
kvalifikaci	kvalifikace	k1gFnSc6
skončil	skončit	k5eAaPmAgInS
na	na	k7c6
posledním	poslední	k2eAgMnSc6d1
24	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
a	a	k8xC
v	v	k7c6
závodě	závod	k1gInSc6
s	s	k7c7
tříkolovou	tříkolový	k2eAgFnSc7d1
ztrátou	ztráta	k1gFnSc7
dojel	dojet	k5eAaPmAgMnS
na	na	k7c4
poslední	poslední	k2eAgInPc4d1
19	#num#	k4
<g/>
.	.	kIx.
pozici	pozice	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
2012	#num#	k4
<g/>
–	–	k?
<g/>
2013	#num#	k4
<g/>
:	:	kIx,
Toro	Toro	k?
Rosso	Rossa	k1gFnSc5
</s>
<s>
2012	#num#	k4
</s>
<s>
Po	po	k7c6
desátém	desátý	k4xOgNnSc6
místě	místo	k1gNnSc6
v	v	k7c6
kvalifikaci	kvalifikace	k1gFnSc6
na	na	k7c4
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Austrálie	Austrálie	k1gFnSc2
obsadil	obsadit	k5eAaPmAgMnS
devátou	devátý	k4xOgFnSc4
příčku	příčka	k1gFnSc4
a	a	k8xC
získal	získat	k5eAaPmAgMnS
své	svůj	k3xOyFgInPc4
první	první	k4xOgInPc4
dva	dva	k4xCgInPc4
body	bod	k1gInPc4
ve	v	k7c6
formuli	formule	k1gFnSc6
jedna	jeden	k4xCgFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
patnáctém	patnáctý	k4xOgNnSc6
místě	místo	k1gNnSc6
v	v	k7c6
kvalifikaci	kvalifikace	k1gFnSc6
obsadil	obsadit	k5eAaPmAgMnS
v	v	k7c6
malajsijské	malajsijský	k2eAgFnSc6d1
velké	velký	k2eAgFnSc6d1
ceně	cena	k1gFnSc6
dvanáctou	dvanáctý	k4xOgFnSc4
příčku	příčka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sedmnáctý	sedmnáctý	k4xOgMnSc1
skončil	skončit	k5eAaPmAgInS
v	v	k7c6
kvalifikaci	kvalifikace	k1gFnSc6
na	na	k7c4
Velkou	velký	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
Číny	Čína	k1gFnSc2
a	a	k8xC
v	v	k7c6
závodě	závod	k1gInSc6
obsadil	obsadit	k5eAaPmAgMnS
stejnou	stejný	k2eAgFnSc4d1
příčku	příčka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
kvalifikaci	kvalifikace	k1gFnSc6
na	na	k7c4
GP	GP	kA
Bahrajnu	Bahrajna	k1gFnSc4
si	se	k3xPyFc3
vyjel	vyjet	k5eAaPmAgInS
6	#num#	k4
<g/>
.	.	kIx.
pozici	pozice	k1gFnSc4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
bylo	být	k5eAaImAgNnS
jeho	on	k3xPp3gInSc4,k3xOp3gInSc4
nejlepší	dobrý	k2eAgInSc4d3
dosavadní	dosavadní	k2eAgInSc4d1
kvalifikační	kvalifikační	k2eAgInSc4d1
výsledek	výsledek	k1gInSc4
v	v	k7c6
kariéře	kariéra	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Závod	závod	k1gInSc1
dokončil	dokončit	k5eAaPmAgInS
patnáctý	patnáctý	k4xOgMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
patnáctého	patnáctý	k4xOgNnSc2
místa	místo	k1gNnSc2
v	v	k7c6
kvalifikaci	kvalifikace	k1gFnSc6
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc1
Španělska	Španělsko	k1gNnSc2
získal	získat	k5eAaPmAgInS
13	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
GP	GP	kA
Monaka	Monako	k1gNnSc2
po	po	k7c6
startu	start	k1gInSc6
z	z	k7c2
16	#num#	k4
<g/>
.	.	kIx.
příčky	příčka	k1gFnSc2
nedokončil	dokončit	k5eNaPmAgMnS
<g/>
,	,	kIx,
když	když	k8xS
měl	mít	k5eAaImAgMnS
v	v	k7c6
65	#num#	k4
<g/>
.	.	kIx.
kole	kolo	k1gNnSc6
poruchu	poruch	k1gInSc2
řízení	řízení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Se	s	k7c7
dvěma	dva	k4xCgInPc7
body	bod	k1gInPc7
držel	držet	k5eAaImAgInS
18	#num#	k4
<g/>
.	.	kIx.
příčku	příčka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
2014	#num#	k4
<g/>
–	–	k?
<g/>
2018	#num#	k4
<g/>
:	:	kIx,
Red	Red	k1gFnSc6
Bull	bulla	k1gFnPc2
Racing	Racing	k1gInSc1
</s>
<s>
2014	#num#	k4
</s>
<s>
Po	po	k7c6
druhém	druhý	k4xOgNnSc6
místě	místo	k1gNnSc6
v	v	k7c6
kvalifikaci	kvalifikace	k1gFnSc6
na	na	k7c4
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc1
Austrálie	Austrálie	k1gFnSc2
dokončil	dokončit	k5eAaPmAgInS
druhý	druhý	k4xOgInSc1
za	za	k7c7
Nicem	Nicem	k?
Rosbergem	Rosberg	k1gInSc7
<g/>
,	,	kIx,
po	po	k7c6
závodě	závod	k1gInSc6
byl	být	k5eAaImAgInS
však	však	k9
diskvalifikován	diskvalifikovat	k5eAaBmNgMnS
<g/>
,	,	kIx,
protože	protože	k8xS
jeho	jeho	k3xOp3gInSc1
vůz	vůz	k1gInSc1
porušoval	porušovat	k5eAaImAgInS
pravidlo	pravidlo	k1gNnSc4
omezujícího	omezující	k2eAgInSc2d1
průtoku	průtok	k1gInSc2
paliva	palivo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Malajsii	Malajsie	k1gFnSc6
Ricciardo	Ricciardo	k1gNnSc1
kvůli	kvůli	k7c3
mnoha	mnoho	k4c3
problémům	problém	k1gInPc3
ze	z	k7c2
závodu	závod	k1gInSc2
odstoupil	odstoupit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Bahrajnu	Bahrajn	k1gInSc2
a	a	k8xC
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Číny	Čína	k1gFnSc2
získal	získat	k5eAaPmAgMnS
24	#num#	k4
bodů	bod	k1gInPc2
za	za	k7c4
dvě	dva	k4xCgFnPc4
4	#num#	k4
<g/>
.	.	kIx.
místa	místo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poprvé	poprvé	k6eAd1
se	se	k3xPyFc4
na	na	k7c6
stupních	stupeň	k1gInPc6
vítězů	vítěz	k1gMnPc2
objevil	objevit	k5eAaPmAgInS
ve	v	k7c6
Španělsku	Španělsko	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
skončil	skončit	k5eAaPmAgInS
za	za	k7c7
oběma	dva	k4xCgInPc7
Mercedesy	mercedes	k1gInPc7
na	na	k7c4
3	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
<g/>
,	,	kIx,
úspěch	úspěch	k1gInSc4
na	na	k7c6
stupních	stupeň	k1gInPc6
vítězů	vítěz	k1gMnPc2
opakoval	opakovat	k5eAaImAgInS
při	při	k7c6
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Monaka	Monako	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Kanady	Kanada	k1gFnSc2
přišel	přijít	k5eAaPmAgMnS
zlom	zlom	k1gInSc4
v	v	k7c6
jeho	jeho	k3xOp3gFnSc6
kariéře	kariéra	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těsně	těsně	k6eAd1
před	před	k7c7
koncem	konec	k1gInSc7
závodu	závod	k1gInSc2
se	se	k3xPyFc4
dostal	dostat	k5eAaPmAgMnS
před	před	k7c4
Sergia	Sergius	k1gMnSc4
Péreze	Péreze	k1gFnSc2
a	a	k8xC
následně	následně	k6eAd1
před	před	k7c4
vedoucího	vedoucí	k2eAgMnSc4d1
muže	muž	k1gMnSc4
Nica	Nicus	k1gMnSc4
Rosberga	Rosberg	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Závod	závod	k1gInSc1
dokončil	dokončit	k5eAaPmAgInS
první	první	k4xOgMnSc1
za	za	k7c2
safety	safeta	k1gFnSc2
carem	car	k1gMnSc7
kvůli	kvůli	k7c3
nehodě	nehoda	k1gFnSc3
Felipeho	Felipe	k1gMnSc2
Massy	Massa	k1gFnSc2
a	a	k8xC
Sergia	Sergius	k1gMnSc2
Péreze	Péreze	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Rakousku	Rakousko	k1gNnSc6
měl	mít	k5eAaImAgMnS
Red	Red	k1gMnSc1
Bull	bulla	k1gFnPc2
smůlu	smůla	k1gFnSc4
<g/>
,	,	kIx,
Ricciardo	Ricciardo	k1gNnSc4
dojel	dojet	k5eAaPmAgMnS
až	až	k9
na	na	k7c6
osmém	osmý	k4xOgInSc6
místě	místo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Velké	velká	k1gFnSc2
Británie	Británie	k1gFnSc2
se	se	k3xPyFc4
opět	opět	k6eAd1
postavil	postavit	k5eAaPmAgMnS
na	na	k7c4
stupně	stupeň	k1gInPc4
vítězů	vítěz	k1gMnPc2
<g/>
,	,	kIx,
když	když	k8xS
dojel	dojet	k5eAaPmAgMnS
třetí	třetí	k4xOgFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Německu	Německo	k1gNnSc6
dojel	dojet	k5eAaPmAgMnS
až	až	k9
šestý	šestý	k4xOgMnSc1
za	za	k7c2
Fernandem	Fernando	k1gNnSc7
Alonsem	Alons	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Maďarska	Maďarsko	k1gNnSc2
se	se	k3xPyFc4
uskutečnila	uskutečnit	k5eAaPmAgFnS
v	v	k7c6
posledních	poslední	k2eAgFnPc6d1
zatáčkách	zatáčka	k1gFnPc6
bitva	bitva	k1gFnSc1
o	o	k7c4
vítězství	vítězství	k1gNnSc4
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
vedl	vést	k5eAaImAgMnS
Fernando	Fernanda	k1gFnSc5
Alonso	Alonsa	k1gFnSc5
na	na	k7c6
Ferrari	Ferrari	k1gMnSc1
<g/>
,	,	kIx,
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc1
na	na	k7c6
Mercedesu	mercedes	k1gInSc6
a	a	k8xC
Daniel	Daniel	k1gMnSc1
Ricciardo	Ricciardo	k1gNnSc4
na	na	k7c4
Red	Red	k1gFnSc4
Bullu	bulla	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ricciardovi	Ricciard	k1gMnSc3
se	se	k3xPyFc4
povedlo	povést	k5eAaPmAgNnS
oba	dva	k4xCgMnPc1
předjet	předjet	k5eAaPmNgInS
a	a	k8xC
s	s	k7c7
dostatečným	dostatečný	k2eAgInSc7d1
náskokem	náskok	k1gInSc7
dojel	dojet	k5eAaPmAgMnS
do	do	k7c2
cíle	cíl	k1gInSc2
na	na	k7c6
prvním	první	k4xOgNnSc6
místě	místo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
kolizi	kolize	k1gFnSc3
obou	dva	k4xCgFnPc2
Mercedesů	mercedes	k1gInPc2
při	při	k7c6
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Belgie	Belgie	k1gFnSc2
dokázal	dokázat	k5eAaPmAgMnS
získat	získat	k5eAaPmF
své	svůj	k3xOyFgNnSc4
třetí	třetí	k4xOgNnSc4
vítězství	vítězství	k1gNnSc4
v	v	k7c6
této	tento	k3xDgFnSc6
sezoně	sezona	k1gFnSc6
a	a	k8xC
také	také	k9
třetí	třetí	k4xOgMnSc1
v	v	k7c6
jeho	jeho	k3xOp3gFnSc6
kariéře	kariéra	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Daniel	Daniel	k1gMnSc1
Ricciardo	Ricciardo	k1gNnSc4
se	se	k3xPyFc4
dostal	dostat	k5eAaPmAgMnS
do	do	k7c2
boje	boj	k1gInSc2
o	o	k7c4
titul	titul	k1gInSc4
mistra	mistr	k1gMnSc2
světa	svět	k1gInSc2
roku	rok	k1gInSc2
2014	#num#	k4
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
na	na	k7c6
třetím	třetí	k4xOgNnSc6
místě	místo	k1gNnSc6
v	v	k7c6
poháru	pohár	k1gInSc6
jezdců	jezdec	k1gMnPc2
se	s	k7c7
156	#num#	k4
body	bod	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Kompletní	kompletní	k2eAgInPc1d1
výsledky	výsledek	k1gInPc1
ve	v	k7c6
Formuli	formule	k1gFnSc6
1	#num#	k4
</s>
<s>
Legenda	legenda	k1gFnSc1
k	k	k7c3
tabulce	tabulka	k1gFnSc3
</s>
<s>
Barva	barva	k1gFnSc1
</s>
<s>
Výsledek	výsledek	k1gInSc1
</s>
<s>
ZlatáVítěz	ZlatáVítěz	k1gMnSc1
</s>
<s>
Stříbrná	stříbrná	k1gFnSc1
<g/>
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Bronzová	bronzový	k2eAgFnSc1d1
<g/>
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
ZelenáBodované	ZelenáBodovaný	k2eAgNnSc1d1
umístění	umístění	k1gNnSc1
</s>
<s>
Modrá	modrý	k2eAgFnSc1d1
</s>
<s>
Nebodované	bodovaný	k2eNgNnSc1d1
umístění	umístění	k1gNnSc1
</s>
<s>
Dokončil	dokončit	k5eAaPmAgMnS
neklasifikován	klasifikován	k2eNgMnSc1d1
(	(	kIx(
<g/>
NC	NC	kA
<g/>
)	)	kIx)
</s>
<s>
FialováOdstoupil	FialováOdstoupit	k5eAaPmAgInS
(	(	kIx(
<g/>
Ret	ret	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Červená	červený	k2eAgFnSc1d1
</s>
<s>
Nekvalifikoval	kvalifikovat	k5eNaBmAgMnS
se	s	k7c7
(	(	kIx(
<g/>
DNQ	DNQ	kA
<g/>
)	)	kIx)
</s>
<s>
Nepředkvalifikoval	Nepředkvalifikovat	k5eAaImAgMnS,k5eAaBmAgMnS,k5eAaPmAgMnS
se	s	k7c7
(	(	kIx(
<g/>
DNPQ	DNPQ	kA
<g/>
)	)	kIx)
</s>
<s>
ČernáDiskvalifikován	ČernáDiskvalifikován	k2eAgMnSc1d1
(	(	kIx(
<g/>
DSQ	DSQ	kA
<g/>
)	)	kIx)
</s>
<s>
Bílá	bílý	k2eAgFnSc1d1
</s>
<s>
Nestartoval	startovat	k5eNaBmAgMnS
(	(	kIx(
<g/>
DNS	DNS	kA
<g/>
)	)	kIx)
</s>
<s>
Závod	závod	k1gInSc1
zrušen	zrušit	k5eAaPmNgInS
(	(	kIx(
<g/>
C	C	kA
<g/>
)	)	kIx)
</s>
<s>
Světle	světle	k6eAd1
modrá	modrý	k2eAgFnSc1d1
</s>
<s>
Pouze	pouze	k6eAd1
trénoval	trénovat	k5eAaImAgMnS
(	(	kIx(
<g/>
PO	po	k7c6
<g/>
)	)	kIx)
</s>
<s>
Páteční	páteční	k2eAgMnSc1d1
testovací	testovací	k2eAgMnSc1d1
jezdec	jezdec	k1gMnSc1
(	(	kIx(
<g/>
TD	TD	kA
<g/>
)	)	kIx)
</s>
<s>
Bez	bez	k7c2
barvy	barva	k1gFnSc2
</s>
<s>
Netrénoval	trénovat	k5eNaImAgMnS
(	(	kIx(
<g/>
DNP	DNP	kA
<g/>
)	)	kIx)
</s>
<s>
Vyřazen	vyřazen	k2eAgMnSc1d1
(	(	kIx(
<g/>
EX	ex	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Nepřijel	přijet	k5eNaPmAgMnS
(	(	kIx(
<g/>
DNA	dna	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Odvolal	odvolat	k5eAaPmAgMnS
účast	účast	k1gFnSc4
(	(	kIx(
<g/>
WD	WD	kA
<g/>
)	)	kIx)
</s>
<s>
Označení	označení	k1gNnSc1
</s>
<s>
Význam	význam	k1gInSc1
</s>
<s>
Tučnost	tučnost	k1gFnSc1
</s>
<s>
Pole	pole	k1gFnSc1
position	position	k1gInSc1
</s>
<s>
Kurzíva	kurzíva	k1gFnSc1
</s>
<s>
Nejrychlejší	rychlý	k2eAgNnSc1d3
kolo	kolo	k1gNnSc1
</s>
<s>
F	F	kA
</s>
<s>
FanBoost	FanBoost	k1gFnSc1
</s>
<s>
G	G	kA
</s>
<s>
Nejrychlejší	rychlý	k2eAgMnSc1d3
v	v	k7c6
kvalifikační	kvalifikační	k2eAgFnSc6d1
skupině	skupina	k1gFnSc6
</s>
<s>
†	†	k?
</s>
<s>
Jezdec	jezdec	k1gMnSc1
nedojel	dojet	k5eNaPmAgMnS
do	do	k7c2
cíle	cíl	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
byl	být	k5eAaImAgMnS
klasifikován	klasifikovat	k5eAaImNgMnS
<g/>
,	,	kIx,
protože	protože	k8xS
odjel	odjet	k5eAaPmAgMnS
více	hodně	k6eAd2
než	než	k8xS
90	#num#	k4
%	%	kIx~
délky	délka	k1gFnSc2
závodu	závod	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
‡	‡	k?
</s>
<s>
Byl	být	k5eAaImAgMnS
udělován	udělován	k2eAgInSc4d1
poloviční	poloviční	k2eAgInSc4d1
počet	počet	k1gInSc4
bodů	bod	k1gInPc2
<g/>
,	,	kIx,
protože	protože	k8xS
bylo	být	k5eAaImAgNnS
odjeto	odjet	k2eAgNnSc1d1
méně	málo	k6eAd2
než	než	k8xS
75	#num#	k4
%	%	kIx~
délky	délka	k1gFnSc2
závodu	závod	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Tým	tým	k1gInSc1
</s>
<s>
Šasi	šasi	k1gNnSc1
</s>
<s>
Motor	motor	k1gInSc1
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
17	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
19	#num#	k4
</s>
<s>
20	#num#	k4
</s>
<s>
21	#num#	k4
</s>
<s>
22	#num#	k4
</s>
<s>
23	#num#	k4
</s>
<s>
Body	bod	k1gInPc4
</s>
<s>
Umístění	umístění	k1gNnSc1
</s>
<s>
2011	#num#	k4
</s>
<s>
Scuderia	Scuderium	k1gNnPc1
Toro	Toro	k?
Rosso	Rossa	k1gFnSc5
</s>
<s>
Toro	Toro	k?
Rosso	Rossa	k1gFnSc5
STR6	STR6	k1gMnSc3
</s>
<s>
Ferrari	Ferrari	k1gMnSc1
056	#num#	k4
2,4	2,4	k4
V8	V8	k1gFnPc2
</s>
<s>
AUSTD	AUSTD	kA
</s>
<s>
MALTD	MALTD	kA
</s>
<s>
CHNTD	CHNTD	kA
</s>
<s>
TURTD	TURTD	kA
</s>
<s>
ESPTD	ESPTD	kA
</s>
<s>
MONTD	MONTD	kA
</s>
<s>
CANTD	CANTD	kA
</s>
<s>
EURTD	EURTD	kA
</s>
<s>
0	#num#	k4
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Hispania	Hispanium	k1gNnPc4
Racing	Racing	k1gInSc1
F1	F1	k1gMnSc1
Team	team	k1gInSc1
</s>
<s>
Hispania	Hispanium	k1gNnPc1
F111	F111	k1gFnSc2
</s>
<s>
Cosworth	Cosworth	k1gInSc1
CA2011	CA2011	k1gFnSc2
2,4	2,4	k4
V8	V8	k1gFnPc2
</s>
<s>
GBR19	GBR19	k4
</s>
<s>
HRT	HRT	kA
Formula	Formula	k1gFnSc1
1	#num#	k4
Team	team	k1gInSc1
</s>
<s>
GER19	GER19	k4
</s>
<s>
HUN18	HUN18	k4
</s>
<s>
BELRet	BELRet	k1gMnSc1
</s>
<s>
ITANC	ITANC	kA
</s>
<s>
SIN19	SIN19	k4
</s>
<s>
JPN22	JPN22	k4
</s>
<s>
KOR19	KOR19	k4
</s>
<s>
IND18	IND18	k4
</s>
<s>
ABURet	ABURet	k1gMnSc1
</s>
<s>
BRA20	BRA20	k4
</s>
<s>
2012	#num#	k4
</s>
<s>
Scuderia	Scuderium	k1gNnPc1
Toro	Toro	k?
Rosso	Rossa	k1gFnSc5
</s>
<s>
Toro	Toro	k?
Rosso	Rossa	k1gFnSc5
STR7	STR7	k1gFnPc4
</s>
<s>
Ferrari	Ferrari	k1gMnSc1
056	#num#	k4
2,4	2,4	k4
V8	V8	k1gFnPc2
</s>
<s>
AUS9	AUS9	k4
</s>
<s>
MAL12	MAL12	k4
</s>
<s>
CHN17	CHN17	k4
</s>
<s>
BHR15	BHR15	k4
</s>
<s>
ESP13	ESP13	k4
</s>
<s>
MONRet	MONRet	k1gMnSc1
</s>
<s>
CAN14	CAN14	k4
</s>
<s>
EUR11	EUR11	k4
</s>
<s>
GBR13	GBR13	k4
</s>
<s>
GER13	GER13	k4
</s>
<s>
HUN15	HUN15	k4
</s>
<s>
BEL9	BEL9	k4
</s>
<s>
ITA12	ITA12	k4
</s>
<s>
SIN9	SIN9	k4
</s>
<s>
JPN10	JPN10	k4
</s>
<s>
KOR9	KOR9	k4
</s>
<s>
IND13	IND13	k4
</s>
<s>
ABU10	ABU10	k4
</s>
<s>
USA12	USA12	k4
</s>
<s>
BRA13	BRA13	k4
</s>
<s>
10	#num#	k4
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
2013	#num#	k4
</s>
<s>
Scuderia	Scuderium	k1gNnPc1
Toro	Toro	k?
Rosso	Rossa	k1gFnSc5
</s>
<s>
Toro	Toro	k?
Rosso	Rossa	k1gFnSc5
STR8	STR8	k1gFnPc5
</s>
<s>
Ferrari	Ferrari	k1gMnSc1
056	#num#	k4
2,4	2,4	k4
V8	V8	k1gFnPc2
</s>
<s>
AUSRet	AUSRet	k1gMnSc1
</s>
<s>
MAL	málit	k5eAaImRp2nS
<g/>
18	#num#	k4
<g/>
†	†	k?
</s>
<s>
CHN7	CHN7	k4
</s>
<s>
BHR16	BHR16	k4
</s>
<s>
ESP10	ESP10	k4
</s>
<s>
MONRet	MONRet	k1gMnSc1
</s>
<s>
CAN15	CAN15	k4
</s>
<s>
GBR8	GBR8	k4
</s>
<s>
GER12	GER12	k4
</s>
<s>
HUN13	HUN13	k4
</s>
<s>
BEL10	BEL10	k4
</s>
<s>
ITA7	ITA7	k4
</s>
<s>
SINRet	SINRet	k1gMnSc1
</s>
<s>
KOR	KOR	kA
<g/>
19	#num#	k4
<g/>
†	†	k?
</s>
<s>
JPN13	JPN13	k4
</s>
<s>
IND10	IND10	k4
</s>
<s>
ABU16	ABU16	k4
</s>
<s>
USA11	USA11	k4
</s>
<s>
BRA10	BRA10	k4
</s>
<s>
20	#num#	k4
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
2014	#num#	k4
</s>
<s>
Infiniti	Infinit	k5eAaPmF,k5eAaImF
Red	Red	k1gFnSc4
Bull	bulla	k1gFnPc2
Racing	Racing	k1gInSc4
</s>
<s>
Red	Red	k?
Bull	bulla	k1gFnPc2
RB10	RB10	k1gMnSc1
</s>
<s>
Renault	renault	k1gInSc1
Energy	Energ	k1gInPc1
F1-2014	F1-2014	k1gFnSc1
1,6	1,6	k4
V6	V6	k1gMnSc1
t	t	k?
</s>
<s>
AUSDSQ	AUSDSQ	kA
</s>
<s>
MALRet	MALRet	k1gMnSc1
</s>
<s>
BHR4	BHR4	k4
</s>
<s>
CHN4	CHN4	k4
</s>
<s>
ESP3	ESP3	k4
</s>
<s>
MON3	MON3	k4
</s>
<s>
CAN1	CAN1	k4
</s>
<s>
AUT8	AUT8	k4
</s>
<s>
GBR3	GBR3	k4
</s>
<s>
GER6	GER6	k4
</s>
<s>
HUN1	HUN1	k4
</s>
<s>
BEL1	BEL1	k4
</s>
<s>
ITA5	ITA5	k4
</s>
<s>
SIN3	SIN3	k4
</s>
<s>
JPN4	JPN4	k4
</s>
<s>
RUS7	RUS7	k4
</s>
<s>
USA3	USA3	k4
</s>
<s>
BRARet	BRARet	k1gMnSc1
</s>
<s>
ABU4	ABU4	k4
</s>
<s>
238	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
2015	#num#	k4
</s>
<s>
Infiniti	Infinit	k5eAaPmF,k5eAaImF
Red	Red	k1gFnSc4
Bull	bulla	k1gFnPc2
Racing	Racing	k1gInSc4
</s>
<s>
Red	Red	k?
Bull	bulla	k1gFnPc2
RB11	RB11	k1gMnSc1
</s>
<s>
Renault	renault	k1gInSc1
Energy	Energ	k1gInPc1
F1-2015	F1-2015	k1gFnSc1
1,6	1,6	k4
V6	V6	k1gMnSc1
t	t	k?
</s>
<s>
AUS6	AUS6	k4
</s>
<s>
MAL10	MAL10	k4
</s>
<s>
CHN9	CHN9	k4
</s>
<s>
BHR6	BHR6	k4
</s>
<s>
ESP7	ESP7	k4
</s>
<s>
MON5	MON5	k4
</s>
<s>
CAN13	CAN13	k4
</s>
<s>
AUT10	AUT10	k4
</s>
<s>
GBRRet	GBRRet	k1gMnSc1
</s>
<s>
HUN3	HUN3	k4
</s>
<s>
BELRet	BELRet	k1gMnSc1
</s>
<s>
ITA8	ITA8	k4
</s>
<s>
SIN2	SIN2	k4
</s>
<s>
JPN15	JPN15	k4
</s>
<s>
RUS	Rus	k1gFnSc1
<g/>
15	#num#	k4
<g/>
†	†	k?
</s>
<s>
USA10	USA10	k4
</s>
<s>
MEX5	MEX5	k4
</s>
<s>
BRA11	BRA11	k4
</s>
<s>
ABU6	ABU6	k4
</s>
<s>
92	#num#	k4
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
2016	#num#	k4
</s>
<s>
Red	Red	k?
Bull	bulla	k1gFnPc2
Racing	Racing	k1gInSc1
</s>
<s>
Red	Red	k?
Bull	bulla	k1gFnPc2
RB12	RB12	k1gMnSc1
</s>
<s>
Renault	renault	k1gInSc1
TAG	tag	k1gInSc1
Heuer	Heuer	k1gInSc1
1,6	1,6	k4
V6	V6	k1gFnSc2
t	t	k?
</s>
<s>
AUS4	AUS4	k4
</s>
<s>
BHR4	BHR4	k4
</s>
<s>
CHN4	CHN4	k4
</s>
<s>
RUS11	RUS11	k4
</s>
<s>
ESP4	ESP4	k4
</s>
<s>
MON2	MON2	k4
</s>
<s>
CAN7	CAN7	k4
</s>
<s>
EUR7	EUR7	k4
</s>
<s>
AUT5	AUT5	k4
</s>
<s>
GBR4	GBR4	k4
</s>
<s>
HUN3	HUN3	k4
</s>
<s>
GER2	GER2	k4
</s>
<s>
BEL2	BEL2	k4
</s>
<s>
ITA5	ITA5	k4
</s>
<s>
SIN2	SIN2	k4
</s>
<s>
MAL1	MAL1	k4
</s>
<s>
JPN6	JPN6	k4
</s>
<s>
USA3	USA3	k4
</s>
<s>
MEX3	MEX3	k4
</s>
<s>
BRA8	BRA8	k4
</s>
<s>
ABU5	ABU5	k4
</s>
<s>
256	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
2017	#num#	k4
</s>
<s>
Red	Red	k?
Bull	bulla	k1gFnPc2
Racing	Racing	k1gInSc1
</s>
<s>
Red	Red	k?
Bull	bulla	k1gFnPc2
RB13	RB13	k1gMnSc1
</s>
<s>
Renault	renault	k1gInSc1
TAG	tag	k1gInSc1
Heuer	Heuer	k1gInSc1
1,6	1,6	k4
V6	V6	k1gFnSc2
t	t	k?
</s>
<s>
AUSRet	AUSRet	k1gMnSc1
</s>
<s>
CHN4	CHN4	k4
</s>
<s>
BHR5	BHR5	k4
</s>
<s>
RUSRet	RUSRet	k1gMnSc1
</s>
<s>
ESP3	ESP3	k4
</s>
<s>
MON3	MON3	k4
</s>
<s>
CAN3	CAN3	k4
</s>
<s>
AZE1	AZE1	k4
</s>
<s>
AUT3	AUT3	k4
</s>
<s>
GBR5	GBR5	k4
</s>
<s>
HUNRet	HUNRet	k1gMnSc1
</s>
<s>
BEL3	BEL3	k4
</s>
<s>
ITA4	ITA4	k4
</s>
<s>
SIN2	SIN2	k4
</s>
<s>
MAL3	MAL3	k4
</s>
<s>
JPN3	JPN3	k4
</s>
<s>
USARet	USARet	k1gMnSc1
</s>
<s>
MEXRet	MEXRet	k1gMnSc1
</s>
<s>
BRA6	BRA6	k4
</s>
<s>
ABURet	ABURet	k1gMnSc1
</s>
<s>
200	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
2018	#num#	k4
</s>
<s>
Aston	Aston	k1gMnSc1
Martin	Martin	k1gMnSc1
Red	Red	k1gMnSc1
Bull	bulla	k1gFnPc2
Racing	Racing	k1gInSc4
</s>
<s>
Red	Red	k?
Bull	bulla	k1gFnPc2
RB14	RB14	k1gMnSc1
</s>
<s>
Renault	renault	k1gInSc1
TAG	tag	k1gInSc1
Heuer	Heuer	k1gInSc1
1,6	1,6	k4
V6	V6	k1gFnSc2
t	t	k?
</s>
<s>
AUS4	AUS4	k4
</s>
<s>
BHRRet	BHRRet	k1gMnSc1
</s>
<s>
CHN1	CHN1	k4
</s>
<s>
AZERet	AZERet	k1gMnSc1
</s>
<s>
ESP5	ESP5	k4
</s>
<s>
MON1	MON1	k4
</s>
<s>
CAN4	CAN4	k4
</s>
<s>
FRA4	FRA4	k4
</s>
<s>
AUTRet	AUTRet	k1gMnSc1
</s>
<s>
GBR5	GBR5	k4
</s>
<s>
GERRet	GERRet	k1gMnSc1
</s>
<s>
HUN4	HUN4	k4
</s>
<s>
BELRet	BELRet	k1gMnSc1
</s>
<s>
ITARet	ITARet	k1gMnSc1
</s>
<s>
SIN6	SIN6	k4
</s>
<s>
RUS6	RUS6	k4
</s>
<s>
JPN4	JPN4	k4
</s>
<s>
USARet	USARet	k1gMnSc1
</s>
<s>
MEXRet	MEXRet	k1gMnSc1
</s>
<s>
BRA4	BRA4	k4
</s>
<s>
ABU4	ABU4	k4
</s>
<s>
170	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
2019	#num#	k4
</s>
<s>
Renault	renault	k1gInSc1
Sport	sport	k1gInSc1
F1	F1	k1gFnPc2
Team	team	k1gInSc1
</s>
<s>
Renault	renault	k1gInSc1
R.	R.	kA
<g/>
S.	S.	kA
<g/>
19	#num#	k4
</s>
<s>
Renault	renault	k1gInSc1
E-Tech	E-Tech	k1gInSc1
19	#num#	k4
1,6	1,6	k4
V6	V6	k1gMnPc2
t	t	k?
</s>
<s>
AUSRet	AUSRet	k1gMnSc1
</s>
<s>
BHR	BHR	kA
<g/>
18	#num#	k4
<g/>
†	†	k?
</s>
<s>
CHN7	CHN7	k4
</s>
<s>
AZERet	AZERet	k1gMnSc1
</s>
<s>
ESP12	ESP12	k4
</s>
<s>
MON9	MON9	k4
</s>
<s>
CAN6	CAN6	k4
</s>
<s>
FRA11	FRA11	k4
</s>
<s>
AUT12	AUT12	k4
</s>
<s>
GBR7	GBR7	k4
</s>
<s>
GERRet	GERRet	k1gMnSc1
</s>
<s>
HUN14	HUN14	k4
</s>
<s>
BEL14	BEL14	k4
</s>
<s>
ITA4	ITA4	k4
</s>
<s>
SIN14	SIN14	k4
</s>
<s>
RUSRet	RUSRet	k1gMnSc1
</s>
<s>
JPNDSQ	JPNDSQ	kA
</s>
<s>
MEX8	MEX8	k4
</s>
<s>
USA6	USA6	k4
</s>
<s>
BRA6	BRA6	k4
</s>
<s>
ABU11	ABU11	k4
</s>
<s>
54	#num#	k4
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
2020	#num#	k4
</s>
<s>
Renault	renault	k1gInSc1
DP	DP	kA
World	World	k1gInSc4
F1	F1	k1gFnSc2
Team	team	k1gInSc1
</s>
<s>
Renault	renault	k1gInSc1
R.	R.	kA
<g/>
S.	S.	kA
<g/>
20	#num#	k4
</s>
<s>
Renault	renault	k1gInSc1
E-Tech	E-Tech	k1gInSc1
20	#num#	k4
1,6	1,6	k4
V6	V6	k1gMnPc2
t	t	k?
</s>
<s>
AUTRet	AUTRet	k1gMnSc1
</s>
<s>
STY8	STY8	k4
</s>
<s>
HUN8	HUN8	k4
</s>
<s>
GBR4	GBR4	k4
</s>
<s>
70A14	70A14	k4
</s>
<s>
ESP11	ESP11	k4
</s>
<s>
BEL4	BEL4	k4
</s>
<s>
ITA6	ITA6	k4
</s>
<s>
TUS4	TUS4	k4
</s>
<s>
RUS5	RUS5	k4
</s>
<s>
EIF3	EIF3	k4
</s>
<s>
POR9	POR9	k4
</s>
<s>
EMI3	EMI3	k4
</s>
<s>
TUR10	TUR10	k4
</s>
<s>
BHR7	BHR7	k4
</s>
<s>
SKR5	SKR5	k4
</s>
<s>
ABU7	ABU7	k4
</s>
<s>
119	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
2021	#num#	k4
</s>
<s>
McLaren	McLarna	k1gFnPc2
F1	F1	k1gFnSc1
Team	team	k1gInSc1
</s>
<s>
McLaren	McLarna	k1gFnPc2
MCL35M	MCL35M	k1gFnPc2
</s>
<s>
Mercedes	mercedes	k1gInSc1
M12	M12	k1gMnPc2
E	E	kA
Performance	performance	k1gFnSc1
1,6	1,6	k4
V6	V6	k1gMnSc1
t	t	k?
</s>
<s>
BHR7	BHR7	k4
</s>
<s>
EMI6	EMI6	k4
</s>
<s>
POR	POR	kA
</s>
<s>
ESP	ESP	kA
</s>
<s>
MON	MON	kA
</s>
<s>
AZE	AZE	kA
</s>
<s>
CAN	CAN	kA
</s>
<s>
FRA	FRA	kA
</s>
<s>
AUT	aut	k1gInSc1
</s>
<s>
GBR	GBR	kA
</s>
<s>
HUN	Hun	k1gMnSc1
</s>
<s>
BEL	bel	k1gInSc1
</s>
<s>
NED	NED	kA
</s>
<s>
ITA	ITA	kA
</s>
<s>
RUS	Rus	k1gFnSc1
</s>
<s>
SIN	sin	kA
</s>
<s>
JPN	JPN	kA
</s>
<s>
USA	USA	kA
</s>
<s>
MEX	MEX	kA
</s>
<s>
SAP	sapa	k1gFnPc2
</s>
<s>
AUS	AUS	kA
</s>
<s>
SAU	SAU	kA
</s>
<s>
ABU	ABU	kA
</s>
<s>
14	#num#	k4
<g/>
*	*	kIx~
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
*	*	kIx~
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
*	*	kIx~
Sezóna	sezóna	k1gFnSc1
v	v	k7c6
průběhu	průběh	k1gInSc6
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Daniel	Daniela	k1gFnPc2
Ricciardo	Ricciardo	k1gNnSc4
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
(	(	kIx(
<g/>
zjednodušené	zjednodušený	k2eAgFnSc6d1
<g/>
)	)	kIx)
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
SKALOŠ	SKALOŠ	kA
<g/>
,	,	kIx,
David	David	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vettel	Vettel	k1gInSc1
si	se	k3xPyFc3
v	v	k7c6
Bahrajnu	Bahrajn	k1gInSc6
připsal	připsat	k5eAaPmAgMnS
první	první	k4xOgNnSc4
letošní	letošní	k2eAgNnSc4d1
pole	pole	k1gNnSc4
position	position	k1gInSc4
<g/>
!	!	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
formule	formule	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2012-04-21	2012-04-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Daniel	Daniel	k1gMnSc1
Ricciardo	Ricciardo	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Galerie	galerie	k1gFnSc1
Daniel	Daniel	k1gMnSc1
Ricciardo	Ricciardo	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Daniel	Daniel	k1gMnSc1
Ricciardo	Ricciardo	k1gNnSc1
–	–	k?
oficiální	oficiální	k2eAgFnSc2d1
stránky	stránka	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Australští	australský	k2eAgMnPc1d1
jezdci	jezdec	k1gMnPc1
Formule	formule	k1gFnSc2
1	#num#	k4
</s>
<s>
David	David	k1gMnSc1
Brabham	Brabham	k1gInSc1
•	•	k?
</s>
<s>
Garry	Garra	k1gFnPc1
Brabham	Brabham	k1gInSc1
•	•	k?
</s>
<s>
Jack	Jack	k6eAd1
Brabham	Brabham	k1gInSc1
•	•	k?
</s>
<s>
Warwick	Warwick	k1gMnSc1
Brown	Brown	k1gMnSc1
•	•	k?
</s>
<s>
Keith	Keith	k1gMnSc1
Campbell	Campbell	k1gMnSc1
•	•	k?
</s>
<s>
Paul	Paul	k1gMnSc1
England	Englanda	k1gFnPc2
•	•	k?
</s>
<s>
Frank	Frank	k1gMnSc1
Gardner	Gardner	k1gMnSc1
•	•	k?
</s>
<s>
Tony	Tony	k1gMnSc1
Gaze	Gaz	k1gFnSc2
•	•	k?
</s>
<s>
Paul	Paul	k1gMnSc1
Hawkins	Hawkinsa	k1gFnPc2
•	•	k?
</s>
<s>
Alan	Alan	k1gMnSc1
Jones	Jones	k1gMnSc1
•	•	k?
</s>
<s>
Ken	Ken	k?
Kavanagh	Kavanagh	k1gInSc1
•	•	k?
</s>
<s>
Brian	Brian	k1gMnSc1
McGuire	McGuir	k1gMnSc5
•	•	k?
</s>
<s>
Larry	Larr	k1gInPc1
Perkins	Perkinsa	k1gFnPc2
•	•	k?
</s>
<s>
Daniel	Daniel	k1gMnSc1
Ricciardo	Ricciardo	k1gNnSc1
•	•	k?
</s>
<s>
Tim	Tim	k?
Schenken	Schenken	k1gInSc1
•	•	k?
</s>
<s>
Vern	Vern	k1gMnSc1
Schuppan	Schuppan	k1gMnSc1
•	•	k?
</s>
<s>
Dave	Dav	k1gInSc5
Walker	Walker	k1gInSc4
•	•	k?
</s>
<s>
Mark	Mark	k1gMnSc1
Webber	Webber	k1gMnSc1
</s>
<s>
Legenda	legenda	k1gFnSc1
<g/>
:	:	kIx,
–	–	k?
mistr	mistr	k1gMnSc1
světa	svět	k1gInSc2
</s>
<s>
Vítězové	vítěz	k1gMnPc1
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Ázerbájdžánu	Ázerbájdžán	k1gInSc2
</s>
<s>
2017	#num#	k4
Daniel	Daniel	k1gMnSc1
Ricciardo	Ricciardo	k1gNnSc1
•	•	k?
2018	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2019	#num#	k4
Valtteri	Valtteri	k1gNnPc2
Bottas	Bottasa	k1gFnPc2
•	•	k?
2020	#num#	k4
</s>
<s>
Vítězové	vítěz	k1gMnPc1
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Belgie	Belgie	k1gFnSc2
</s>
<s>
1950	#num#	k4
Juan	Juan	k1gMnSc1
Manuel	Manuel	k1gMnSc1
Fangio	Fangio	k1gMnSc1
•	•	k?
1951	#num#	k4
Giuseppe	Giusepp	k1gInSc5
Farina	Farin	k2eAgInSc2d1
•	•	k?
1952	#num#	k4
Alberto	Alberta	k1gFnSc5
Ascari	Ascar	k1gInPc7
•	•	k?
1953	#num#	k4
Alberto	Alberta	k1gFnSc5
Ascari	Ascar	k1gInPc7
•	•	k?
1954	#num#	k4
Juan	Juan	k1gMnSc1
Manuel	Manuel	k1gMnSc1
Fangio	Fangio	k1gMnSc1
•	•	k?
1955	#num#	k4
Juan	Juan	k1gMnSc1
Manuel	Manuel	k1gMnSc1
Fangio	Fangio	k1gMnSc1
•	•	k?
1956	#num#	k4
Peter	Petra	k1gFnPc2
Collins	Collinsa	k1gFnPc2
•	•	k?
1957	#num#	k4
•	•	k?
1958	#num#	k4
Tony	Tony	k1gFnSc2
Brooks	Brooksa	k1gFnPc2
•	•	k?
1959	#num#	k4
•	•	k?
1960	#num#	k4
Jack	Jack	k1gInSc1
Brabham	Brabham	k1gInSc4
•	•	k?
1961	#num#	k4
Phil	Phil	k1gInSc1
Hill	Hill	k1gInSc4
•	•	k?
1962	#num#	k4
Jim	on	k3xPp3gFnPc3
Clark	Clark	k1gInSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
1963	#num#	k4
Jim	on	k3xPp3gFnPc3
Clark	Clark	k1gInSc1
•	•	k?
1964	#num#	k4
Jim	on	k3xPp3gFnPc3
Clark	Clark	k1gInSc1
•	•	k?
1965	#num#	k4
Jim	on	k3xPp3gFnPc3
Clark	Clark	k1gInSc1
•	•	k?
1966	#num#	k4
John	John	k1gMnSc1
Surtees	Surtees	k1gMnSc1
•	•	k?
1967	#num#	k4
Dan	Dana	k1gFnPc2
Gurney	Gurnea	k1gFnSc2
•	•	k?
1968	#num#	k4
Bruce	Bruce	k1gFnSc2
McLaren	McLarna	k1gFnPc2
•	•	k?
1969	#num#	k4
•	•	k?
1970	#num#	k4
Pedro	Pedro	k1gNnSc1
Rodríguez	Rodrígueza	k1gFnPc2
•	•	k?
1971	#num#	k4
•	•	k?
1972	#num#	k4
Emerson	Emerson	k1gNnSc1
Fittipaldi	Fittipald	k1gMnPc1
•	•	k?
1973	#num#	k4
Jackie	Jackie	k1gFnSc2
Stewart	Stewart	k1gMnSc1
•	•	k?
1974	#num#	k4
Emerson	Emerson	k1gNnSc1
Fittipaldi	Fittipald	k1gMnPc1
•	•	k?
1975	#num#	k4
Niki	Nik	k1gFnSc2
Lauda	Lauda	k1gFnSc1
•	•	k?
1976	#num#	k4
Niki	Nik	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
Lauda	Lauda	k1gFnSc1
•	•	k?
1977	#num#	k4
Gunnar	Gunnar	k1gInSc1
Nilsson	Nilsson	k1gInSc4
•	•	k?
1978	#num#	k4
Mario	Mario	k1gMnSc1
Andretti	Andretti	k1gNnSc1
•	•	k?
1979	#num#	k4
Jody	jod	k1gInPc1
Scheckter	Scheckter	k1gInSc1
•	•	k?
1980	#num#	k4
Didier	Didier	k1gInSc1
Pironi	Piroň	k1gFnSc6
•	•	k?
1981	#num#	k4
Carlos	Carlos	k1gInSc1
Reutemann	Reutemann	k1gInSc4
•	•	k?
1982	#num#	k4
John	John	k1gMnSc1
Watson	Watson	k1gMnSc1
•	•	k?
1983	#num#	k4
Alain	Alain	k2eAgInSc4d1
Prost	prost	k2eAgInSc4d1
•	•	k?
1984	#num#	k4
Michele	Michela	k1gFnSc6
Alboreto	Alboreto	k1gNnSc1
•	•	k?
1985	#num#	k4
Ayrton	Ayrton	k1gInSc1
Senna	Senn	k1gInSc2
•	•	k?
1986	#num#	k4
Nigel	Nigel	k1gInSc1
Mansell	Mansell	k1gInSc4
•	•	k?
1987	#num#	k4
Alain	Alain	k2eAgInSc4d1
Prost	prost	k2eAgInSc4d1
•	•	k?
1988	#num#	k4
Ayrton	Ayrton	k1gInSc1
Senna	Senn	k1gInSc2
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
1989	#num#	k4
Ayrton	Ayrton	k1gInSc1
Senna	Senn	k1gInSc2
•	•	k?
1990	#num#	k4
Ayrton	Ayrton	k1gInSc1
Senna	Senn	k1gInSc2
•	•	k?
1991	#num#	k4
Ayrton	Ayrton	k1gInSc1
Senna	Senn	k1gInSc2
•	•	k?
1992	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
1993	#num#	k4
Damon	Damon	k1gInSc1
Hill	Hill	k1gInSc4
•	•	k?
1994	#num#	k4
Damon	Damon	k1gInSc1
Hill	Hill	k1gInSc4
•	•	k?
1995	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
1996	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
1997	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
1998	#num#	k4
Damon	Damon	k1gInSc1
Hill	Hill	k1gInSc4
•	•	k?
1999	#num#	k4
David	David	k1gMnSc1
Coulthard	Coulthard	k1gMnSc1
•	•	k?
2000	#num#	k4
Mika	Mik	k1gMnSc2
Häkkinen	Häkkinen	k1gInSc4
•	•	k?
2001	#num#	k4
Michael	Michael	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2002	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2003	#num#	k4
•	•	k?
2004	#num#	k4
Kimi	Kimi	k1gNnPc2
Räikkönen	Räikkönna	k1gFnPc2
•	•	k?
2005	#num#	k4
Kimi	Kimi	k1gNnPc2
Räikkönen	Räikkönna	k1gFnPc2
•	•	k?
2006	#num#	k4
•	•	k?
2007	#num#	k4
Kimi	Kimi	k1gNnPc2
Räikkönen	Räikkönna	k1gFnPc2
•	•	k?
2008	#num#	k4
Felipe	Felip	k1gInSc5
Massa	Massa	k1gFnSc1
•	•	k?
2009	#num#	k4
Kimi	Kimi	k1gNnPc2
Räikkönen	Räikkönna	k1gFnPc2
•	•	k?
2010	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2011	#num#	k4
Sebastian	Sebastian	k1gMnSc1
Vettel	Vettel	k1gMnSc1
•	•	k?
2012	#num#	k4
Jenson	Jenson	k1gInSc1
Button	Button	k1gInSc4
•	•	k?
2013	#num#	k4
Sebastian	Sebastian	k1gMnSc1
Vettel	Vettel	k1gMnSc1
•	•	k?
2014	#num#	k4
Daniel	Daniela	k1gFnPc2
Ricciardo	Ricciardo	k1gNnSc1
•	•	k?
2015	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2016	#num#	k4
Nico	Nico	k6eAd1
Rosberg	Rosberg	k1gInSc1
•	•	k?
2017	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2018	#num#	k4
Sebastian	Sebastian	k1gMnSc1
Vettel	Vettel	k1gMnSc1
•	•	k?
2019	#num#	k4
Charles	Charles	k1gMnSc1
Leclerc	Leclerc	k1gInSc1
•	•	k?
2020	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
</s>
<s>
Vítězové	vítěz	k1gMnPc1
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Číny	Čína	k1gFnSc2
</s>
<s>
2004	#num#	k4
Rubens	Rubensa	k1gFnPc2
Barrichello	Barrichello	k1gNnSc1
•	•	k?
2005	#num#	k4
Fernando	Fernanda	k1gFnSc5
Alonso	Alonsa	k1gFnSc5
•	•	k?
2006	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2007	#num#	k4
Kimi	Kimi	k1gNnPc2
Räikkönen	Räikkönna	k1gFnPc2
•	•	k?
2008	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2009	#num#	k4
Sebastian	Sebastian	k1gMnSc1
Vettel	Vettel	k1gMnSc1
•	•	k?
2010	#num#	k4
Jenson	Jenson	k1gInSc1
Button	Button	k1gInSc4
•	•	k?
2011	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2012	#num#	k4
Nico	Nico	k6eAd1
Rosberg	Rosberg	k1gInSc1
•	•	k?
2013	#num#	k4
Fernando	Fernanda	k1gFnSc5
Alonso	Alonsa	k1gFnSc5
•	•	k?
2014	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2015	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2016	#num#	k4
Nico	Nico	k6eAd1
Rosberg	Rosberg	k1gInSc1
•	•	k?
2017	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2018	#num#	k4
Daniel	Daniela	k1gFnPc2
Ricciardo	Ricciardo	k1gNnSc1
•	•	k?
2019	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2020	#num#	k4
</s>
<s>
Vítězové	vítěz	k1gMnPc1
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Kanady	Kanada	k1gFnSc2
</s>
<s>
1967	#num#	k4
Jack	Jacka	k1gFnPc2
Brabham	Brabham	k1gInSc4
•	•	k?
1968	#num#	k4
Denny	Denna	k1gFnSc2
Hulme	houlit	k5eAaImRp1nP
•	•	k?
1969	#num#	k4
Jacky	Jacka	k1gMnSc2
Ickx	Ickx	k1gInSc4
•	•	k?
1970	#num#	k4
Jacky	Jacka	k1gMnSc2
Ickx	Ickx	k1gInSc4
•	•	k?
1971	#num#	k4
Jackie	Jackie	k1gFnSc2
Stewart	Stewart	k1gMnSc1
•	•	k?
1972	#num#	k4
Jackie	Jackie	k1gFnSc2
Stewart	Stewart	k1gMnSc1
•	•	k?
1973	#num#	k4
Peter	Petra	k1gFnPc2
Revson	Revsona	k1gFnPc2
•	•	k?
1974	#num#	k4
Emerson	Emersona	k1gFnPc2
Fittipaldi	Fittipald	k1gMnPc1
•	•	k?
1975	#num#	k4
•	•	k?
1976	#num#	k4
James	Jamesa	k1gFnPc2
Hunt	hunt	k1gInSc4
•	•	k?
1977	#num#	k4
Jody	jod	k1gInPc4
Scheckter	Schecktrum	k1gNnPc2
•	•	k?
1978	#num#	k4
Gilles	Gillesa	k1gFnPc2
Villeneuve	Villeneuev	k1gFnSc2
•	•	k?
1979	#num#	k4
Alan	alan	k1gInSc4
Jones	Jonesa	k1gFnPc2
•	•	k?
1980	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
Alan	Alan	k1gMnSc1
Jones	Jones	k1gMnSc1
•	•	k?
1981	#num#	k4
Jacques	Jacques	k1gMnSc1
Laffite	Laffit	k1gInSc5
•	•	k?
1982	#num#	k4
Nelson	Nelson	k1gMnSc1
Piquet	Piquet	k1gMnSc1
•	•	k?
1983	#num#	k4
René	René	k1gFnPc2
Arnoux	Arnoux	k1gInSc4
•	•	k?
1984	#num#	k4
Nelson	Nelson	k1gMnSc1
Piquet	Piquet	k1gMnSc1
•	•	k?
1985	#num#	k4
Michele	Michela	k1gFnSc6
Alboreto	Alboreto	k1gNnSc1
•	•	k?
1986	#num#	k4
Nigel	Nigel	k1gInSc1
Mansell	Mansell	k1gInSc4
•	•	k?
1987	#num#	k4
•	•	k?
1988	#num#	k4
Ayrton	Ayrton	k1gInSc1
Senna	Senn	k1gInSc2
•	•	k?
1989	#num#	k4
Thierry	Thierra	k1gFnSc2
Boutsen	Boutsna	k1gFnPc2
•	•	k?
1990	#num#	k4
Ayrton	Ayrton	k1gInSc1
Senna	Senn	k1gInSc2
•	•	k?
1991	#num#	k4
Nelson	Nelson	k1gMnSc1
Piquet	Piquet	k1gMnSc1
•	•	k?
1992	#num#	k4
Gerhard	Gerhard	k1gMnSc1
Berger	Berger	k1gMnSc1
•	•	k?
1993	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
Alain	Alain	k1gInSc1
Prost	prost	k2eAgInSc1d1
•	•	k?
1994	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
1995	#num#	k4
Jean	Jean	k1gMnSc1
Alesi	Alese	k1gFnSc4
•	•	k?
1996	#num#	k4
Damon	Damon	k1gInSc1
Hill	Hill	k1gInSc4
•	•	k?
1997	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
1998	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
1999	#num#	k4
Mika	Mik	k1gMnSc2
Häkkinen	Häkkinen	k1gInSc4
•	•	k?
2000	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2001	#num#	k4
Ralf	Ralf	k1gInSc1
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2002	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2003	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2004	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2005	#num#	k4
Kimi	Kimi	k1gNnPc2
Räikkönen	Räikkönna	k1gFnPc2
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
2006	#num#	k4
Fernando	Fernanda	k1gFnSc5
Alonso	Alonsa	k1gFnSc5
•	•	k?
2007	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2008	#num#	k4
Robert	Robert	k1gMnSc1
Kubica	Kubica	k1gMnSc1
•	•	k?
2009	#num#	k4
•	•	k?
2010	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2011	#num#	k4
Jenson	Jenson	k1gInSc1
Button	Button	k1gInSc4
•	•	k?
2012	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2013	#num#	k4
Sebastian	Sebastian	k1gMnSc1
Vettel	Vettel	k1gMnSc1
•	•	k?
2014	#num#	k4
Daniel	Daniela	k1gFnPc2
Ricciardo	Ricciardo	k1gNnSc1
•	•	k?
2015	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2016	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2017	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2018	#num#	k4
Sebastian	Sebastian	k1gMnSc1
Vettel	Vettel	k1gMnSc1
•	•	k?
2019	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2020	#num#	k4
</s>
<s>
Vítězové	vítěz	k1gMnPc1
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Maďarska	Maďarsko	k1gNnSc2
</s>
<s>
1986	#num#	k4
Nelson	Nelson	k1gMnSc1
Piquet	Piquet	k1gMnSc1
•	•	k?
1987	#num#	k4
Nelson	Nelson	k1gMnSc1
Piquet	Piquet	k1gMnSc1
•	•	k?
1988	#num#	k4
Ayrton	Ayrton	k1gInSc1
Senna	Senn	k1gInSc2
•	•	k?
1989	#num#	k4
Nigel	Nigel	k1gInSc1
Mansell	Mansell	k1gInSc4
•	•	k?
1990	#num#	k4
Thierry	Thierra	k1gFnSc2
Boutsen	Boutsna	k1gFnPc2
•	•	k?
1991	#num#	k4
Ayrton	Ayrton	k1gInSc1
Senna	Senn	k1gInSc2
•	•	k?
1992	#num#	k4
Ayrton	Ayrton	k1gInSc1
Senna	Senn	k1gInSc2
•	•	k?
1993	#num#	k4
Damon	Damon	k1gInSc1
Hill	Hill	k1gInSc4
•	•	k?
1994	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
1995	#num#	k4
Damon	Damon	k1gInSc1
Hill	Hill	k1gInSc4
•	•	k?
1996	#num#	k4
Jacques	Jacques	k1gMnSc1
Villeneuve	Villeneuev	k1gFnSc2
•	•	k?
1997	#num#	k4
Jacques	Jacques	k1gMnSc1
Villeneuve	Villeneuev	k1gFnSc2
•	•	k?
1998	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1999	#num#	k4
Mika	Mik	k1gMnSc2
Häkkinen	Häkkinen	k1gInSc4
•	•	k?
2000	#num#	k4
Mika	Mik	k1gMnSc2
Häkkinen	Häkkinen	k1gInSc4
•	•	k?
2001	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2002	#num#	k4
Rubens	Rubensa	k1gFnPc2
Barrichello	Barrichello	k1gNnSc1
•	•	k?
2003	#num#	k4
Fernando	Fernanda	k1gFnSc5
Alonso	Alonsa	k1gFnSc5
•	•	k?
2004	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2005	#num#	k4
Kimi	Kimi	k1gNnPc2
Räikkönen	Räikkönna	k1gFnPc2
•	•	k?
2006	#num#	k4
Jenson	Jenson	k1gInSc1
Button	Button	k1gInSc4
•	•	k?
2007	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2008	#num#	k4
Heikki	Heikki	k1gNnPc2
Kovalainen	Kovalainna	k1gFnPc2
•	•	k?
2009	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2010	#num#	k4
Mark	Mark	k1gMnSc1
Webber	Webber	k1gMnSc1
•	•	k?
2011	#num#	k4
Jenson	Jenson	k1gInSc1
Button	Button	k1gInSc4
•	•	k?
2012	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2013	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2014	#num#	k4
Daniel	Daniela	k1gFnPc2
Ricciardo	Ricciardo	k1gNnSc1
•	•	k?
2015	#num#	k4
Sebastian	Sebastian	k1gMnSc1
Vettel	Vettel	k1gMnSc1
•	•	k?
2016	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2017	#num#	k4
Sebastian	Sebastian	k1gMnSc1
Vettel	Vettel	k1gMnSc1
•	•	k?
2018	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2019	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2020	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
</s>
<s>
Vítězové	vítěz	k1gMnPc1
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Malajsie	Malajsie	k1gFnSc2
</s>
<s>
1999	#num#	k4
Eddie	Eddie	k1gFnSc1
Irvine	Irvin	k1gInSc5
•	•	k?
2000	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2001	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2002	#num#	k4
Ralf	Ralf	k1gInSc1
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2003	#num#	k4
Kimi	Kimi	k1gNnPc2
Räikkönen	Räikkönna	k1gFnPc2
•	•	k?
2004	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2005	#num#	k4
Fernando	Fernanda	k1gFnSc5
Alonso	Alonsa	k1gFnSc5
•	•	k?
2006	#num#	k4
Giancarlo	Giancarlo	k1gNnSc4
Fisichella	Fisichello	k1gNnSc2
•	•	k?
2007	#num#	k4
Fernando	Fernanda	k1gFnSc5
Alonso	Alonsa	k1gFnSc5
•	•	k?
2008	#num#	k4
Kimi	Kimi	k1gNnPc2
Räikkönen	Räikkönna	k1gFnPc2
•	•	k?
2009	#num#	k4
Jenson	Jenson	k1gInSc1
Button	Button	k1gInSc4
•	•	k?
2010	#num#	k4
Sebastian	Sebastian	k1gMnSc1
Vettel	Vettel	k1gMnSc1
•	•	k?
2011	#num#	k4
Sebastian	Sebastian	k1gMnSc1
Vettel	Vettel	k1gMnSc1
•	•	k?
2012	#num#	k4
Fernando	Fernanda	k1gFnSc5
Alonso	Alonsa	k1gFnSc5
•	•	k?
2013	#num#	k4
Sebastian	Sebastian	k1gMnSc1
Vettel	Vettel	k1gMnSc1
•	•	k?
2014	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2015	#num#	k4
Sebastian	Sebastian	k1gMnSc1
Vettel	Vettel	k1gMnSc1
•	•	k?
2016	#num#	k4
Daniel	Daniela	k1gFnPc2
Ricciardo	Ricciardo	k1gNnSc1
•	•	k?
2017	#num#	k4
Max	max	kA
Verstappen	Verstappen	k2eAgInSc4d1
</s>
<s>
Vítězové	vítěz	k1gMnPc1
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Monaka	Monako	k1gNnSc2
</s>
<s>
1950	#num#	k4
Juan	Juan	k1gMnSc1
Manuel	Manuel	k1gMnSc1
Fangio	Fangio	k1gMnSc1
•	•	k?
1951	#num#	k4
<g/>
–	–	k?
<g/>
1954	#num#	k4
•	•	k?
1955	#num#	k4
Maurice	Maurika	k1gFnSc6
Trintignant	Trintignant	k1gInSc1
•	•	k?
1956	#num#	k4
Stirling	Stirling	k1gInSc1
Moss	Moss	k1gInSc4
•	•	k?
1957	#num#	k4
Juan	Juan	k1gMnSc1
Manuel	Manuel	k1gMnSc1
Fangio	Fangio	k1gMnSc1
•	•	k?
1958	#num#	k4
Maurice	Maurika	k1gFnSc6
Trintignant	Trintignant	k1gInSc1
•	•	k?
1959	#num#	k4
Jack	Jack	k1gInSc1
Brabham	Brabham	k1gInSc4
•	•	k?
1960	#num#	k4
Stirling	Stirling	k1gInSc1
Moss	Moss	k1gInSc4
•	•	k?
1961	#num#	k4
Stirling	Stirling	k1gInSc1
Moss	Moss	k1gInSc4
•	•	k?
1962	#num#	k4
Bruce	Bruce	k1gFnSc2
McLaren	McLarna	k1gFnPc2
•	•	k?
1963	#num#	k4
Graham	graham	k1gInSc1
Hill	Hill	k1gInSc4
•	•	k?
1964	#num#	k4
Graham	graham	k1gInSc1
Hill	Hill	k1gInSc4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1965	#num#	k4
Graham	graham	k1gInSc4
Hill	Hilla	k1gFnPc2
•	•	k?
1966	#num#	k4
Jackie	Jackie	k1gFnSc2
Stewart	Stewart	k1gMnSc1
•	•	k?
1967	#num#	k4
Denny	Denna	k1gFnSc2
Hulme	houlit	k5eAaImRp1nP
•	•	k?
1968	#num#	k4
Graham	graham	k1gInSc4
Hill	Hilla	k1gFnPc2
•	•	k?
1969	#num#	k4
Graham	graham	k1gInSc4
Hill	Hilla	k1gFnPc2
•	•	k?
1970	#num#	k4
Jochen	Jochen	k2eAgInSc4d1
Rindt	Rindt	k1gInSc4
•	•	k?
1971	#num#	k4
Jackie	Jackie	k1gFnSc2
Stewart	Stewart	k1gMnSc1
•	•	k?
1972	#num#	k4
Jean-Pierre	Jean-Pierr	k1gInSc5
Beltoise	Beltois	k1gInSc6
•	•	k?
1973	#num#	k4
Jackie	Jackie	k1gFnSc2
Stewart	Stewart	k1gMnSc1
•	•	k?
1974	#num#	k4
Ronnie	Ronnie	k1gFnSc2
Peterson	Petersona	k1gFnPc2
•	•	k?
1975	#num#	k4
Niki	Nik	k1gFnSc2
Lauda	Laudo	k1gNnSc2
•	•	k?
1976	#num#	k4
Niki	Nik	k1gFnSc2
Lauda	Laudo	k1gNnSc2
•	•	k?
1977	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
Jody	jod	k1gInPc1
Scheckter	Scheckter	k1gInSc1
•	•	k?
1978	#num#	k4
Patrick	Patrick	k1gInSc1
Depailler	Depailler	k1gInSc4
•	•	k?
1979	#num#	k4
Jody	jod	k1gInPc1
Scheckter	Scheckter	k1gInSc1
•	•	k?
1980	#num#	k4
Carlos	Carlos	k1gInSc1
Reutemann	Reutemann	k1gInSc4
•	•	k?
1981	#num#	k4
Gilles	Gilles	k1gInSc1
Villeneuve	Villeneuev	k1gFnSc2
•	•	k?
1982	#num#	k4
Riccardo	Riccardo	k1gNnSc4
Patrese	Patrese	k1gFnSc2
•	•	k?
1983	#num#	k4
Keke	Keke	k1gInSc1
Rosberg	Rosberg	k1gInSc4
•	•	k?
1984	#num#	k4
Alain	Alain	k2eAgInSc4d1
Prost	prost	k2eAgInSc4d1
•	•	k?
1985	#num#	k4
Alain	Alain	k2eAgInSc4d1
Prost	prost	k2eAgInSc4d1
•	•	k?
1986	#num#	k4
Alain	Alain	k2eAgInSc4d1
Prost	prost	k2eAgInSc4d1
•	•	k?
1987	#num#	k4
Ayrton	Ayrton	k1gInSc1
Senna	Senn	k1gInSc2
•	•	k?
1988	#num#	k4
Alain	Alain	k2eAgInSc4d1
Prost	prost	k2eAgInSc4d1
•	•	k?
1989	#num#	k4
Ayrton	Ayrton	k1gInSc1
Senna	Senn	k1gInSc2
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1990	#num#	k4
Ayrton	Ayrton	k1gInSc1
Senna	Senn	k1gInSc2
•	•	k?
1991	#num#	k4
Ayrton	Ayrton	k1gInSc1
Senna	Senn	k1gInSc2
•	•	k?
1992	#num#	k4
Ayrton	Ayrton	k1gInSc1
Senna	Senn	k1gInSc2
•	•	k?
1993	#num#	k4
Ayrton	Ayrton	k1gInSc1
Senna	Senn	k1gInSc2
•	•	k?
1994	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
1995	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
1996	#num#	k4
Olivier	Olivier	k1gInSc1
Panis	Panis	k1gInSc4
•	•	k?
1997	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
1998	#num#	k4
Mika	Mik	k1gMnSc2
Häkkinen	Häkkinen	k1gInSc4
•	•	k?
1999	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2000	#num#	k4
David	David	k1gMnSc1
Coulthard	Coulthard	k1gMnSc1
•	•	k?
2001	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2002	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
David	David	k1gMnSc1
Coulthard	Coulthard	k1gMnSc1
•	•	k?
2003	#num#	k4
Juan	Juan	k1gMnSc1
Pablo	Pablo	k1gNnSc1
Montoya	Montoya	k1gMnSc1
•	•	k?
2004	#num#	k4
Jarno	Jarno	k6eAd1
Trulli	Trulle	k1gFnSc4
•	•	k?
2005	#num#	k4
Kimi	Kimi	k1gNnPc2
Räikkönen	Räikkönna	k1gFnPc2
•	•	k?
2006	#num#	k4
Fernando	Fernanda	k1gFnSc5
Alonso	Alonsa	k1gFnSc5
•	•	k?
2007	#num#	k4
Fernando	Fernanda	k1gFnSc5
Alonso	Alonsa	k1gFnSc5
•	•	k?
2008	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2009	#num#	k4
Jenson	Jenson	k1gInSc1
Button	Button	k1gInSc4
•	•	k?
2010	#num#	k4
Mark	Mark	k1gMnSc1
Webber	Webber	k1gMnSc1
•	•	k?
2011	#num#	k4
Sebastian	Sebastian	k1gMnSc1
Vettel	Vettel	k1gMnSc1
•	•	k?
2012	#num#	k4
Mark	Mark	k1gMnSc1
Webber	Webber	k1gMnSc1
•	•	k?
2013	#num#	k4
Nico	Nico	k6eAd1
Rosberg	Rosberg	k1gInSc1
•	•	k?
2014	#num#	k4
Nico	Nico	k6eAd1
Rosberg	Rosberg	k1gInSc1
•	•	k?
2015	#num#	k4
Nico	Nico	k6eAd1
Rosberg	Rosberg	k1gInSc1
•	•	k?
2016	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2017	#num#	k4
Sebastian	Sebastian	k1gMnSc1
Vettel	Vettel	k1gMnSc1
•	•	k?
2018	#num#	k4
Daniel	Daniela	k1gFnPc2
Ricciardo	Ricciardo	k1gNnSc1
•	•	k?
2019	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2020	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Formule	formule	k1gFnSc1
1	#num#	k4
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
VIAF	VIAF	kA
<g/>
:	:	kIx,
2017156223611105400001	#num#	k4
</s>
