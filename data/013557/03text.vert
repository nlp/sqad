<s>
Loubinec	loubinec	k1gInSc1
</s>
<s>
Loubinec	loubinec	k1gInSc1
Loubinec	loubinec	k1gInSc1
pětilistý	pětilistý	k2eAgInSc1d1
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc2
Říše	říš	k1gFnSc2
</s>
<s>
rostliny	rostlina	k1gFnPc1
(	(	kIx(
<g/>
Plantae	Plantae	k1gFnSc1
<g/>
)	)	kIx)
Podříše	podříše	k1gFnSc1
</s>
<s>
cévnaté	cévnatý	k2eAgFnPc1d1
rostliny	rostlina	k1gFnPc1
(	(	kIx(
<g/>
Tracheobionta	Tracheobionta	k1gFnSc1
<g/>
)	)	kIx)
Oddělení	oddělení	k1gNnSc1
</s>
<s>
krytosemenné	krytosemenný	k2eAgNnSc1d1
(	(	kIx(
<g/>
Magnoliophyta	Magnoliophyta	k1gFnSc1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
vyšší	vysoký	k2eAgFnPc1d2
dvouděložné	dvouděložná	k1gFnPc1
(	(	kIx(
<g/>
Rosopsida	Rosopsida	k1gFnSc1
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
révotvaré	révotvarý	k2eAgFnPc1d1
(	(	kIx(
<g/>
Vitales	Vitales	k1gInSc1
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc1
</s>
<s>
révovité	révovitý	k2eAgInPc4d1
(	(	kIx(
<g/>
Vitaceae	Vitacea	k1gInPc4
<g/>
)	)	kIx)
Rod	rod	k1gInSc4
</s>
<s>
loubinec	loubinec	k1gInSc1
(	(	kIx(
<g/>
Parthenocissus	Parthenocissus	k1gInSc1
<g/>
)	)	kIx)
<g/>
Planch	Planch	k1gInSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
1887	#num#	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Loubinec	loubinec	k1gInSc1
(	(	kIx(
<g/>
Parthenocissus	Parthenocissus	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
česky	česky	k6eAd1
též	též	k9
přísavník	přísavník	k1gMnSc1
a	a	k8xC
lidově	lidově	k6eAd1
psí	psí	k2eAgNnSc1d1
víno	víno	k1gNnSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
rod	rod	k1gInSc4
rostlin	rostlina	k1gFnPc2
z	z	k7c2
čeledi	čeleď	k1gFnSc2
révovitých	révovití	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Loubince	loubinec	k1gInSc2
jsou	být	k5eAaImIp3nP
dřevnaté	dřevnatý	k2eAgFnPc1d1
úponkaté	úponkatý	k2eAgFnPc1d1
liány	liána	k1gFnPc1
s	s	k7c7
jednoduchými	jednoduchý	k2eAgInPc7d1
nebo	nebo	k8xC
složenými	složený	k2eAgInPc7d1
střídavými	střídavý	k2eAgInPc7d1
listy	list	k1gInPc7
a	a	k8xC
drobnými	drobný	k2eAgInPc7d1
květy	květ	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úponky	úponek	k1gInPc7
bývají	bývat	k5eAaImIp3nP
zakončené	zakončený	k2eAgInPc1d1
přísavnými	přísavný	k2eAgFnPc7d1
destičkami	destička	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rod	rod	k1gInSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
13	#num#	k4
druhů	druh	k1gInPc2
a	a	k8xC
je	být	k5eAaImIp3nS
rozšířen	rozšířit	k5eAaPmNgInS
v	v	k7c6
Asii	Asie	k1gFnSc6
a	a	k8xC
Severní	severní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Loubince	loubinec	k1gInSc2
jsou	být	k5eAaImIp3nP
pěstovány	pěstován	k2eAgFnPc1d1
jako	jako	k8xC,k8xS
bujně	bujně	k6eAd1
rostoucí	rostoucí	k2eAgFnPc1d1
okrasné	okrasný	k2eAgFnPc1d1
liány	liána	k1gFnPc1
s	s	k7c7
univerzálním	univerzální	k2eAgNnSc7d1
použitím	použití	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
je	být	k5eAaImIp3nS
nejčastěji	často	k6eAd3
pěstován	pěstován	k2eAgInSc1d1
loubinec	loubinec	k1gInSc1
pětilistý	pětilistý	k2eAgInSc1d1
a	a	k8xC
loubinec	loubinec	k1gInSc1
trojlaločný	trojlaločný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Loubince	loubinec	k1gInPc1
jsou	být	k5eAaImIp3nP
dřevnaté	dřevnatý	k2eAgFnPc4d1
liány	liána	k1gFnPc4
s	s	k7c7
úponky	úponek	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Borka	borka	k1gFnSc1
je	být	k5eAaImIp3nS
celistvá	celistvý	k2eAgFnSc1d1
a	a	k8xC
neodlupuje	odlupovat	k5eNaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
pruzích	pruh	k1gInPc6
ani	ani	k8xC
šupinách	šupina	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Větévky	větévka	k1gFnPc1
jsou	být	k5eAaImIp3nP
ve	v	k7c6
střední	střední	k2eAgFnSc6d1
části	část	k1gFnSc6
vyplněné	vyplněná	k1gFnSc2
bílou	bílý	k2eAgFnSc4d1
<g/>
,	,	kIx,
v	v	k7c4
nodech	nodech	k1gInSc4
nepřerušovanou	přerušovaný	k2eNgFnSc7d1
dření	dřeň	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Listy	lista	k1gFnPc1
jsou	být	k5eAaImIp3nP
střídavé	střídavý	k2eAgFnPc1d1
<g/>
,	,	kIx,
jednoduché	jednoduchý	k2eAgNnSc1d1
s	s	k7c7
dlanitou	dlanitý	k2eAgFnSc7d1
žilnatinou	žilnatina	k1gFnSc7
<g/>
,	,	kIx,
trojčetné	trojčetný	k2eAgInPc1d1
nebo	nebo	k8xC
dlanitě	dlanitě	k6eAd1
složené	složený	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úponky	úponka	k1gFnPc1
jsou	být	k5eAaImIp3nP
hroznovitě	hroznovitě	k6eAd1
rozdělené	rozdělená	k1gFnPc1
do	do	k7c2
4	#num#	k4
až	až	k9
12	#num#	k4
větví	větev	k1gFnPc2
<g/>
,	,	kIx,
na	na	k7c6
jejichž	jejichž	k3xOyRp3gInPc6
koncích	konec	k1gInPc6
se	se	k3xPyFc4
u	u	k7c2
většiny	většina	k1gFnSc2
druhů	druh	k1gInPc2
vyvíjejí	vyvíjet	k5eAaImIp3nP
přísavné	přísavný	k2eAgInPc1d1
terčíky	terčík	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Květenstvím	květenství	k1gNnSc7
je	být	k5eAaImIp3nS
lata	lata	k1gFnSc1
nebo	nebo	k8xC
rozvolněné	rozvolněný	k2eAgFnPc1d1
<g/>
,	,	kIx,
zdánlivě	zdánlivě	k6eAd1
vrcholové	vrcholový	k2eAgNnSc4d1
chocholičnaté	chocholičnatý	k2eAgNnSc4d1
polycházium	polycházium	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Květenství	květenství	k1gNnSc1
vyrůstají	vyrůstat	k5eAaImIp3nP
v	v	k7c6
paždí	paždí	k1gNnSc6
listů	list	k1gInPc2
<g/>
,	,	kIx,
proti	proti	k7c3
listům	list	k1gInPc3
nebo	nebo	k8xC
řidčeji	řídce	k6eAd2
na	na	k7c6
koncích	konec	k1gInPc6
letorostů	letorost	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Květy	Květa	k1gFnPc1
jsou	být	k5eAaImIp3nP
pravidelné	pravidelný	k2eAgFnPc1d1
<g/>
,	,	kIx,
pětičetné	pětičetný	k2eAgFnPc1d1
<g/>
,	,	kIx,
oboupohlavné	oboupohlavný	k2eAgFnPc1d1
nebo	nebo	k8xC
samčí	samčí	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kalich	kalich	k1gInSc1
je	být	k5eAaImIp3nS
miskovitý	miskovitý	k2eAgMnSc1d1
<g/>
,	,	kIx,
lysý	lysý	k2eAgMnSc1d1
<g/>
,	,	kIx,
zakončený	zakončený	k2eAgMnSc1d1
5	#num#	k4
zuby	zub	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Korunní	korunní	k2eAgInPc1d1
lístky	lístek	k1gInPc1
jsou	být	k5eAaImIp3nP
volné	volný	k2eAgNnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyčinek	tyčinka	k1gFnPc2
je	být	k5eAaImIp3nS
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Květní	květní	k2eAgInSc1d1
terč	terč	k1gInSc1
je	být	k5eAaImIp3nS
málo	málo	k6eAd1
vyvinutý	vyvinutý	k2eAgInSc1d1
a	a	k8xC
pouze	pouze	k6eAd1
výjimečně	výjimečně	k6eAd1
nese	nést	k5eAaImIp3nS
5	#num#	k4
nektárií	nektárium	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Semeník	semeník	k1gInSc1
je	být	k5eAaImIp3nS
dvoupouzdrý	dvoupouzdrý	k2eAgInSc1d1
<g/>
,	,	kIx,
v	v	k7c6
každém	každý	k3xTgInSc6
pouzdře	pouzdro	k1gNnSc6
jsou	být	k5eAaImIp3nP
2	#num#	k4
vajíčka	vajíčko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čnělka	čnělka	k1gFnSc1
je	být	k5eAaImIp3nS
zřetelná	zřetelný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plodem	plod	k1gInSc7
je	být	k5eAaImIp3nS
kulovitá	kulovitý	k2eAgFnSc1d1
bobule	bobule	k1gFnSc1
obsahující	obsahující	k2eAgFnSc1d1
1	#num#	k4
až	až	k9
4	#num#	k4
semena	semeno	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Loubinec	loubinec	k1gInSc1
Parthenocissus	Parthenocissus	k1gInSc1
henryana	henryan	k1gMnSc2
</s>
<s>
Přísavné	přísavný	k2eAgInPc1d1
terčíky	terčík	k1gInPc1
loubince	loubinec	k1gInSc2
trojlaločného	trojlaločný	k2eAgInSc2d1
</s>
<s>
Detail	detail	k1gInSc1
květu	květ	k1gInSc2
loubince	loubinec	k1gInSc2
pětilistého	pětilistý	k2eAgInSc2d1
</s>
<s>
Plody	plod	k1gInPc1
loubince	loubinec	k1gInSc2
pětilistého	pětilistý	k2eAgInSc2d1
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1
</s>
<s>
Rod	rod	k1gInSc1
loubinec	loubinec	k1gInSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
13	#num#	k4
druhů	druh	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
rozšířen	rozšířit	k5eAaPmNgInS
ve	v	k7c6
východní	východní	k2eAgFnSc6d1
Asii	Asie	k1gFnSc6
a	a	k8xC
Severní	severní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Největší	veliký	k2eAgInSc1d3
areál	areál	k1gInSc1
<g/>
,	,	kIx,
sahající	sahající	k2eAgNnSc1d1
od	od	k7c2
Indie	Indie	k1gFnSc2
po	po	k7c4
Čínu	Čína	k1gFnSc4
<g/>
,	,	kIx,
Vietnam	Vietnam	k1gInSc4
a	a	k8xC
Indonésii	Indonésie	k1gFnSc4
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
druh	druh	k1gMnSc1
Parthenocissus	Parthenocissus	k1gMnSc1
semicordata	semicordata	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Číně	Čína	k1gFnSc6
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
8	#num#	k4
domácích	domácí	k2eAgInPc2d1
druhů	druh	k1gInPc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
6	#num#	k4
endemických	endemický	k2eAgFnPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Indii	Indie	k1gFnSc6
rostou	růst	k5eAaImIp3nP
2	#num#	k4
druhy	druh	k1gInPc4
<g/>
,	,	kIx,
v	v	k7c6
Indonésii	Indonésie	k1gFnSc6
2	#num#	k4
druhy	druh	k1gInPc4
<g/>
,	,	kIx,
v	v	k7c6
Severní	severní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
3	#num#	k4
druhy	druh	k1gInPc4
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
loubinec	loubinec	k1gInSc1
pětilistý	pětilistý	k2eAgInSc1d1
zasahuje	zasahovat	k5eAaImIp3nS
i	i	k9
do	do	k7c2
Mexika	Mexiko	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
evropské	evropský	k2eAgFnSc6d1
květeně	květena	k1gFnSc6
není	být	k5eNaImIp3nS
žádný	žádný	k3yNgInSc4
druh	druh	k1gInSc4
loubince	loubinec	k1gInPc4
původní	původní	k2eAgInPc4d1
<g/>
,	,	kIx,
některé	některý	k3yIgInPc1
pěstované	pěstovaný	k2eAgInPc1d1
druhy	druh	k1gInPc1
místy	místy	k6eAd1
zplaňují	zplaňovat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
nejčastěji	často	k6eAd3
zplaňuje	zplaňovat	k5eAaImIp3nS
loubinec	loubinec	k1gInSc1
popínavý	popínavý	k2eAgInSc1d1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
zejména	zejména	k9
v	v	k7c6
pobřežních	pobřežní	k2eAgFnPc6d1
křovinách	křovina	k1gFnPc6
a	a	k8xC
lužních	lužní	k2eAgInPc6d1
lesích	les	k1gInPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Včela	včela	k1gFnSc1
na	na	k7c6
květech	květ	k1gInPc6
loubince	loubinec	k1gInPc1
trojlaločného	trojlaločný	k2eAgInSc2d1
</s>
<s>
Ekologické	ekologický	k2eAgFnPc1d1
interakce	interakce	k1gFnPc1
</s>
<s>
Květy	květ	k1gInPc1
loubinců	loubinec	k1gInPc2
jsou	být	k5eAaImIp3nP
drobné	drobný	k2eAgFnPc1d1
a	a	k8xC
nenápadné	nápadný	k2eNgFnPc1d1
<g/>
,	,	kIx,
avšak	avšak	k8xC
obsahují	obsahovat	k5eAaImIp3nP
hojný	hojný	k2eAgInSc4d1
nektar	nektar	k1gInSc4
a	a	k8xC
jsou	být	k5eAaImIp3nP
navštěvovány	navštěvovat	k5eAaImNgFnP
různým	různý	k2eAgInSc7d1
hmyzem	hmyz	k1gInSc7
<g/>
,	,	kIx,
zejména	zejména	k9
včelami	včela	k1gFnPc7
<g/>
,	,	kIx,
vosami	vosa	k1gFnPc7
<g/>
,	,	kIx,
mouchami	moucha	k1gFnPc7
a	a	k8xC
brouky	brouk	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plody	plod	k1gInPc7
jsou	být	k5eAaImIp3nP
šířeny	šířen	k2eAgInPc1d1
zvířaty	zvíře	k1gNnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zástupci	zástupce	k1gMnPc1
</s>
<s>
loubinec	loubinec	k1gInSc1
popínavý	popínavý	k2eAgInSc1d1
(	(	kIx(
<g/>
Parthenocissus	Parthenocissus	k1gInSc1
inserta	insert	k1gMnSc2
<g/>
)	)	kIx)
</s>
<s>
loubinec	loubinec	k1gInSc1
pětilistý	pětilistý	k2eAgInSc1d1
(	(	kIx(
<g/>
Parthenocissus	Parthenocissus	k1gInSc1
quinquefolia	quinquefolium	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
loubinec	loubinec	k1gInSc1
trojlaločný	trojlaločný	k2eAgInSc1d1
(	(	kIx(
<g/>
Parthenocissus	Parthenocissus	k1gInSc1
tricuspidata	tricuspidat	k1gMnSc2
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Význam	význam	k1gInSc1
</s>
<s>
Loubince	loubinec	k1gInPc1
jsou	být	k5eAaImIp3nP
pěstovány	pěstovat	k5eAaImNgInP
v	v	k7c6
různých	různý	k2eAgInPc6d1
kultivarech	kultivar	k1gInPc6
jako	jako	k8xC,k8xS
okrasné	okrasný	k2eAgFnPc4d1
liány	liána	k1gFnPc4
<g/>
,	,	kIx,
vhodné	vhodný	k2eAgNnSc4d1
k	k	k7c3
rychlému	rychlý	k2eAgNnSc3d1
ozelenění	ozelenění	k1gNnSc3
stěn	stěna	k1gFnPc2
<g/>
,	,	kIx,
pergol	pergola	k1gFnPc2
<g/>
,	,	kIx,
budov	budova	k1gFnPc2
<g/>
,	,	kIx,
skal	skála	k1gFnPc2
a	a	k8xC
podobně	podobně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
ozelenění	ozelenění	k1gNnSc3
budov	budova	k1gFnPc2
a	a	k8xC
zdí	zeď	k1gFnPc2
se	se	k3xPyFc4
nejvíce	hodně	k6eAd3,k6eAd1
hodí	hodit	k5eAaImIp3nS,k5eAaPmIp3nS
loubinec	loubinec	k1gInSc1
trojlaločný	trojlaločný	k2eAgInSc1d1
<g/>
,	,	kIx,
neboť	neboť	k8xC
se	se	k3xPyFc4
nejlépe	dobře	k6eAd3
zachycuje	zachycovat	k5eAaImIp3nS
na	na	k7c6
kolmých	kolmý	k2eAgFnPc6d1
stěnách	stěna	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Loubinec	loubinec	k1gInSc1
pětilistý	pětilistý	k2eAgInSc1d1
je	být	k5eAaImIp3nS
vhodnější	vhodný	k2eAgInSc1d2
např.	např.	kA
na	na	k7c4
pergoly	pergola	k1gFnPc4
<g/>
,	,	kIx,
nepříliš	příliš	k6eNd1
kolmé	kolmý	k2eAgFnSc2d1
stěny	stěna	k1gFnSc2
apod.	apod.	kA
Loubinec	loubinec	k1gInSc4
popínavý	popínavý	k2eAgInSc1d1
nemá	mít	k5eNaImIp3nS
přísavné	přísavný	k2eAgFnSc2d1
destičky	destička	k1gFnSc2
a	a	k8xC
má	mít	k5eAaImIp3nS
spíše	spíše	k9
šlahounovitý	šlahounovitý	k2eAgInSc1d1
<g/>
,	,	kIx,
rozkleslý	rozkleslý	k2eAgInSc1d1
vzrůst	vzrůst	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Výjimečně	výjimečně	k6eAd1
je	být	k5eAaImIp3nS
v	v	k7c6
botanických	botanický	k2eAgFnPc6d1
zahradách	zahrada	k1gFnPc6
pěstován	pěstován	k2eAgInSc1d1
druh	druh	k1gInSc1
P.	P.	kA
semicordata	semicordata	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dům	dům	k1gInSc1
porostlý	porostlý	k2eAgInSc1d1
loubincem	loubinec	k1gInSc7
trojlaločným	trojlaločný	k2eAgNnPc3d1
</s>
<s>
Detail	detail	k1gInSc1
stěny	stěna	k1gFnSc2
porostlé	porostlý	k2eAgFnSc2d1
loubincem	loubinec	k1gInSc7
trojlaločným	trojlaločný	k2eAgMnSc7d1
</s>
<s>
Loubinec	loubinec	k1gInSc1
popínavý	popínavý	k2eAgInSc1d1
</s>
<s>
Podzimní	podzimní	k2eAgNnSc1d1
zbarvení	zbarvení	k1gNnSc1
loubince	loubinec	k1gInSc2
pětilistého	pětilistý	k2eAgInSc2d1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
SLAVÍK	Slavík	k1gMnSc1
<g/>
,	,	kIx,
Bohumil	Bohumil	k1gMnSc1
(	(	kIx(
<g/>
editor	editor	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Květena	květena	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
590	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
CHEN	CHEN	kA
<g/>
,	,	kIx,
Prof.	prof.	kA
Zhiduan	Zhiduan	k1gInSc1
<g/>
;	;	kIx,
WEN	WEN	kA
<g/>
,	,	kIx,
Jun	jun	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Flora	Flora	k1gFnSc1
of	of	k?
China	China	k1gFnSc1
<g/>
:	:	kIx,
Parthenocissus	Parthenocissus	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Tropicos	Tropicos	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Missouri	Missouri	k1gNnSc1
<g/>
:	:	kIx,
Missouri	Missouri	k1gNnSc1
Botanical	Botanical	k1gFnPc2
Garden	Gardna	k1gFnPc2
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
GRIN	GRIN	kA
taxonomy	taxonom	k1gInPc4
for	forum	k1gNnPc2
plants	plants	k6eAd1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Catalogue	Catalogue	k1gInSc1
of	of	k?
Life	Life	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Plants	Plants	k1gInSc1
of	of	k?
USA	USA	kA
and	and	k?
Canada	Canada	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Flora	Flora	k1gFnSc1
Europaea	Europaea	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Royal	Royal	k1gInSc1
Botanic	Botanice	k1gFnPc2
Garden	Gardno	k1gNnPc2
Edinburgh	Edinburgh	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
KUBÁT	Kubát	k1gMnSc1
<g/>
,	,	kIx,
K.	K.	kA
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klíč	klíč	k1gInSc1
ke	k	k7c3
květeně	květena	k1gFnSc3
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
836	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
JUDD	JUDD	kA
<g/>
,	,	kIx,
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plant	planta	k1gFnPc2
Systematics	Systematicsa	k1gFnPc2
<g/>
:	:	kIx,
A	a	k9
Phylogenetic	Phylogenetice	k1gFnPc2
Approach	Approach	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Sinauer	Sinauer	k1gMnSc1
Associates	Associates	k1gMnSc1
Inc	Inc	k1gMnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9780878934034	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
KOBLÍŽEK	koblížek	k1gInSc1
<g/>
,	,	kIx,
J.	J.	kA
Jehličnaté	jehličnatý	k2eAgFnPc4d1
a	a	k8xC
listnaté	listnatý	k2eAgFnPc4d1
dřeviny	dřevina	k1gFnPc4
našich	náš	k3xOp1gFnPc2
zahrad	zahrada	k1gFnPc2
a	a	k8xC
parků	park	k1gInPc2
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tišnov	Tišnov	k1gInSc1
<g/>
:	:	kIx,
Sursum	Sursum	k1gNnSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7323	#num#	k4
<g/>
-	-	kIx~
<g/>
117	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
HIEKE	HIEKE	kA
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praktická	praktický	k2eAgFnSc1d1
dendrologie	dendrologie	k1gFnSc1
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
SZN	SZN	kA
<g/>
,	,	kIx,
1978	#num#	k4
<g/>
.	.	kIx.
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
105	#num#	k4
<g/>
-	-	kIx~
<g/>
78	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Florius	Florius	k1gInSc1
-	-	kIx~
katalog	katalog	k1gInSc1
botanických	botanický	k2eAgFnPc2d1
zahrad	zahrada	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Koblížek	Koblížek	k1gMnSc1
J.	J.	kA
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Parthenocissus	Parthenocissus	k1gMnSc1
Planchon	Planchon	k1gMnSc1
<g/>
,	,	kIx,
In	In	k1gMnSc1
<g/>
:	:	kIx,
Slavík	Slavík	k1gMnSc1
B.	B.	kA
(	(	kIx(
<g/>
ed	ed	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Květena	květena	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
vol	vol	k6eAd1
<g/>
.	.	kIx.
5	#num#	k4
<g/>
,	,	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
</s>
<s>
Koblížek	Koblížek	k1gMnSc1
J.	J.	kA
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Parthenocissus	Parthenocissus	k1gMnSc1
Planchon	Planchon	k1gMnSc1
<g/>
,	,	kIx,
In	In	k1gMnSc1
<g/>
:	:	kIx,
Kubát	Kubát	k1gMnSc1
K.	K.	kA
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
(	(	kIx(
<g/>
eds	eds	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Klíč	klíč	k1gInSc1
ke	k	k7c3
Květeně	květena	k1gFnSc3
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
-928	-928	k4
p.	p.	k?
<g/>
,	,	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
loubinec	loubinec	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Taxon	taxon	k1gInSc1
Parthenocissus	Parthenocissus	k1gInSc1
ve	v	k7c6
Wikidruzích	Wikidruze	k1gFnPc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Rostliny	rostlina	k1gFnPc1
</s>
