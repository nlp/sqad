<s>
Hiragana	Hiragana	k1gFnSc1	Hiragana
(	(	kIx(	(
<g/>
ひ	ひ	k?	ひ
–	–	k?	–
hiragana	hiragana	k1gFnSc1	hiragana
<g/>
,	,	kIx,	,
平	平	k?	平
–	–	k?	–
zápis	zápis	k1gInSc1	zápis
v	v	k7c6	v
kandži	kandž	k1gFnSc6	kandž
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jedno	jeden	k4xCgNnSc1	jeden
ze	z	k7c2	z
dvou	dva	k4xCgNnPc2	dva
(	(	kIx(	(
<g/>
druhým	druhý	k4xOgNnSc7	druhý
je	být	k5eAaImIp3nS	být
katakana	katakana	k1gFnSc1	katakana
<g/>
)	)	kIx)	)
japonských	japonský	k2eAgNnPc2d1	Japonské
slabičných	slabičný	k2eAgNnPc2d1	slabičné
písem	písmo	k1gNnPc2	písmo
(	(	kIx(	(
<g/>
ve	v	k7c6	v
slabičném	slabičný	k2eAgNnSc6d1	slabičné
písmu	písmo	k1gNnSc6	písmo
jeden	jeden	k4xCgInSc1	jeden
znak	znak	k1gInSc1	znak
představuje	představovat	k5eAaImIp3nS	představovat
celou	celý	k2eAgFnSc4d1	celá
slabiku	slabika	k1gFnSc4	slabika
a	a	k8xC	a
ne	ne	k9	ne
pouhou	pouhý	k2eAgFnSc4d1	pouhá
hlásku	hláska	k1gFnSc4	hláska
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
znaky	znak	k1gInPc1	znak
i	i	k9	i
pro	pro	k7c4	pro
A	A	kA	A
<g/>
,	,	kIx,	,
I	I	kA	I
<g/>
,	,	kIx,	,
U	U	kA	U
<g/>
,	,	kIx,	,
E	E	kA	E
a	a	k8xC	a
O	O	kA	O
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sestávající	sestávající	k2eAgMnSc1d1	sestávající
ze	z	k7c2	z
46	[number]	k4	46
symbolů	symbol	k1gInPc2	symbol
(	(	kIx(	(
<g/>
45	[number]	k4	45
slabičných	slabičný	k2eAgInPc2d1	slabičný
symbolů	symbol	k1gInPc2	symbol
a	a	k8xC	a
písmeno	písmeno	k1gNnSc4	písmeno
n	n	k0	n
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Připojením	připojení	k1gNnSc7	připojení
dvou	dva	k4xCgFnPc2	dva
čárek	čárka	k1gFnPc2	čárka
(	(	kIx(	(
<g/>
nigori	nigori	k6eAd1	nigori
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
kroužku	kroužek	k1gInSc2	kroužek
(	(	kIx(	(
<g/>
marunigori	marunigori	k6eAd1	marunigori
<g/>
)	)	kIx)	)
nad	nad	k7c4	nad
určité	určitý	k2eAgInPc4d1	určitý
symboly	symbol	k1gInPc4	symbol
či	či	k8xC	či
jejich	jejich	k3xOp3gNnSc7	jejich
skládáním	skládání	k1gNnSc7	skládání
vznikají	vznikat	k5eAaImIp3nP	vznikat
ještě	ještě	k9	ještě
další	další	k2eAgInPc4d1	další
zvuky	zvuk	k1gInPc4	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
abecedy	abeceda	k1gFnPc1	abeceda
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
systémem	systém	k1gInSc7	systém
a	a	k8xC	a
řazením	řazení	k1gNnSc7	řazení
velice	velice	k6eAd1	velice
podobají	podobat	k5eAaImIp3nP	podobat
<g/>
,	,	kIx,	,
liší	lišit	k5eAaImIp3nP	lišit
se	se	k3xPyFc4	se
však	však	k9	však
způsobem	způsob	k1gInSc7	způsob
užití	užití	k1gNnSc2	užití
<g/>
.	.	kIx.	.
</s>
<s>
Odlišný	odlišný	k2eAgInSc1d1	odlišný
je	být	k5eAaImIp3nS	být
i	i	k9	i
původ	původ	k1gInSc1	původ
obou	dva	k4xCgFnPc2	dva
abeced	abeceda	k1gFnPc2	abeceda
<g/>
:	:	kIx,	:
hiragana	hiragana	k1gFnSc1	hiragana
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
zjednodušením	zjednodušení	k1gNnSc7	zjednodušení
celých	celý	k2eAgInPc2d1	celý
znaků	znak	k1gInPc2	znak
kandži	kandzat	k5eAaPmIp1nS	kandzat
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
katakana	katakana	k1gFnSc1	katakana
vychází	vycházet	k5eAaImIp3nS	vycházet
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
částí	část	k1gFnPc2	část
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
znaků	znak	k1gInPc2	znak
kandži	kandzat	k5eAaPmIp1nS	kandzat
<g/>
.	.	kIx.	.
</s>
<s>
Hiragana	Hiragana	k1gFnSc1	Hiragana
byla	být	k5eAaImAgFnS	být
kodifikována	kodifikován	k2eAgFnSc1d1	kodifikována
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
zjednoznačnění	zjednoznačnění	k1gNnSc3	zjednoznačnění
přiřazení	přiřazení	k1gNnSc2	přiřazení
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
symbolů	symbol	k1gInPc2	symbol
ke	k	k7c3	k
zvukům	zvuk	k1gInPc3	zvuk
a	a	k8xC	a
dva	dva	k4xCgInPc4	dva
symboly	symbol	k1gInPc4	symbol
(	(	kIx(	(
<g/>
wi	wi	k?	wi
<g/>
,	,	kIx,	,
we	we	k?	we
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
označeny	označit	k5eAaPmNgInP	označit
za	za	k7c4	za
zastaralé	zastaralý	k2eAgNnSc4d1	zastaralé
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
slabičná	slabičný	k2eAgFnSc1d1	slabičná
abeceda	abeceda	k1gFnSc1	abeceda
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
za	za	k7c4	za
přispění	přispění	k1gNnSc4	přispění
buddhistického	buddhistický	k2eAgMnSc2d1	buddhistický
kněze	kněz	k1gMnSc2	kněz
Kúkaie	Kúkaie	k1gFnSc2	Kúkaie
(	(	kIx(	(
<g/>
774	[number]	k4	774
<g/>
–	–	k?	–
<g/>
835	[number]	k4	835
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
symboly	symbol	k1gInPc1	symbol
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
zjednodušením	zjednodušení	k1gNnSc7	zjednodušení
celých	celá	k1gFnPc2	celá
(	(	kIx(	(
<g/>
hira	hira	k6eAd1	hira
znamená	znamenat	k5eAaImIp3nS	znamenat
v	v	k7c6	v
japonštině	japonština	k1gFnSc6	japonština
"	"	kIx"	"
<g/>
celkový	celkový	k2eAgInSc1d1	celkový
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
čínských	čínský	k2eAgInPc2d1	čínský
znaků	znak	k1gInPc2	znak
kandži	kandzat	k5eAaPmIp1nS	kandzat
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
katakany	katakana	k1gFnSc2	katakana
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
zjednodušením	zjednodušení	k1gNnSc7	zjednodušení
pouze	pouze	k6eAd1	pouze
částí	část	k1gFnPc2	část
čínských	čínský	k2eAgInPc2d1	čínský
znaků	znak	k1gInPc2	znak
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yRgFnPc2	který
již	již	k6eAd1	již
předtím	předtím	k6eAd1	předtím
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
desemantizaci	desemantizace	k1gFnSc3	desemantizace
a	a	k8xC	a
fonetizaci	fonetizace	k1gFnSc3	fonetizace
(	(	kIx(	(
<g/>
ztratily	ztratit	k5eAaPmAgInP	ztratit
význam	význam	k1gInSc4	význam
a	a	k8xC	a
staly	stát	k5eAaPmAgFnP	stát
se	se	k3xPyFc4	se
pouhými	pouhý	k2eAgInPc7d1	pouhý
zvuky	zvuk	k1gInPc7	zvuk
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohly	moct	k5eAaImAgFnP	moct
vyjadřovat	vyjadřovat	k5eAaImF	vyjadřovat
japonské	japonský	k2eAgFnPc1d1	japonská
koncovky	koncovka	k1gFnPc1	koncovka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Znaky	znak	k1gInPc1	znak
hiragany	hiragana	k1gFnSc2	hiragana
jsou	být	k5eAaImIp3nP	být
složitější	složitý	k2eAgInPc1d2	složitější
a	a	k8xC	a
na	na	k7c6	na
psaní	psaní	k1gNnSc6	psaní
obtížnější	obtížný	k2eAgMnSc1d2	obtížnější
než	než	k8xS	než
znaky	znak	k1gInPc1	znak
katakany	katakana	k1gFnSc2	katakana
<g/>
.	.	kIx.	.
</s>
<s>
Nedlouho	dlouho	k6eNd1	dlouho
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
vzniku	vznik	k1gInSc6	vznik
bylo	být	k5eAaImAgNnS	být
toto	tento	k3xDgNnSc1	tento
písmo	písmo	k1gNnSc1	písmo
nazýváno	nazýván	k2eAgNnSc1d1	nazýváno
také	také	k9	také
onnade	onnást	k5eAaPmIp3nS	onnást
(	(	kIx(	(
<g/>
ženská	ženský	k2eAgFnSc1d1	ženská
ruka	ruka	k1gFnSc1	ruka
<g/>
)	)	kIx)	)
-	-	kIx~	-
psaly	psát	k5eAaImAgFnP	psát
jím	on	k3xPp3gInSc7	on
totiž	totiž	k9	totiž
převážně	převážně	k6eAd1	převážně
ženy	žena	k1gFnPc4	žena
(	(	kIx(	(
<g/>
muži	muž	k1gMnPc1	muž
používali	používat	k5eAaImAgMnP	používat
katakanu	katakana	k1gFnSc4	katakana
a	a	k8xC	a
kandži	kandzat	k5eAaPmIp1nS	kandzat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgFnPc3	jenž
nebylo	být	k5eNaImAgNnS	být
umožněno	umožnit	k5eAaPmNgNnS	umožnit
učit	učit	k5eAaImF	učit
se	se	k3xPyFc4	se
znakům	znak	k1gInPc3	znak
kandži	kandzat	k5eAaPmIp1nS	kandzat
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
považovány	považován	k2eAgFnPc1d1	považována
za	za	k7c4	za
příliš	příliš	k6eAd1	příliš
složité	složitý	k2eAgFnPc4d1	složitá
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
zavedení	zavedení	k1gNnSc3	zavedení
hiragany	hiragana	k1gFnSc2	hiragana
tak	tak	k9	tak
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
spousta	spousta	k6eAd1	spousta
literárních	literární	k2eAgNnPc2d1	literární
děl	dělo	k1gNnPc2	dělo
z	z	k7c2	z
rukou	ruka	k1gFnPc2	ruka
žen	žena	k1gFnPc2	žena
z	z	k7c2	z
období	období	k1gNnSc2	období
Heian	Heiana	k1gFnPc2	Heiana
<g/>
.	.	kIx.	.
slova	slovo	k1gNnPc1	slovo
japonského	japonský	k2eAgInSc2d1	japonský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
která	který	k3yRgNnPc4	který
neexistují	existovat	k5eNaImIp3nP	existovat
znaky	znak	k1gInPc7	znak
kandži	kandž	k1gFnSc6	kandž
gramatická	gramatický	k2eAgNnPc1d1	gramatické
slova	slovo	k1gNnPc1	slovo
(	(	kIx(	(
<g/>
předpony	předpona	k1gFnSc2	předpona
<g/>
,	,	kIx,	,
přípony	přípona	k1gFnPc1	přípona
<g/>
,	,	kIx,	,
částice	částice	k1gFnPc1	částice
<g/>
,	,	kIx,	,
koncovky	koncovka	k1gFnPc1	koncovka
<g/>
)	)	kIx)	)
knížky	knížka	k1gFnPc1	knížka
pro	pro	k7c4	pro
malé	malý	k2eAgFnPc4d1	malá
děti	dítě	k1gFnPc4	dítě
(	(	kIx(	(
<g/>
japonské	japonský	k2eAgFnPc4d1	japonská
děti	dítě	k1gFnPc4	dítě
<g />
.	.	kIx.	.
</s>
<s>
se	se	k3xPyFc4	se
hiraganu	hiragan	k1gInSc3	hiragan
učí	učit	k5eAaImIp3nS	učit
jako	jako	k9	jako
první	první	k4xOgNnSc1	první
ze	z	k7c2	z
tří	tři	k4xCgNnPc2	tři
japonských	japonský	k2eAgNnPc2d1	Japonské
písem	písmo	k1gNnPc2	písmo
<g/>
:	:	kIx,	:
hiragana	hiragana	k1gFnSc1	hiragana
<g/>
,	,	kIx,	,
katakana	katakana	k1gFnSc1	katakana
<g/>
,	,	kIx,	,
kandži	kandzat	k5eAaPmIp1nS	kandzat
<g/>
)	)	kIx)	)
soukromé	soukromý	k2eAgInPc1d1	soukromý
dopisy	dopis	k1gInPc1	dopis
furigana	furigana	k1gFnSc1	furigana
(	(	kIx(	(
<g/>
malé	malý	k2eAgInPc1d1	malý
znaky	znak	k1gInPc1	znak
hiragany	hiragana	k1gFnSc2	hiragana
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
v	v	k7c6	v
knihách	kniha	k1gFnPc6	kniha
pro	pro	k7c4	pro
začínající	začínající	k2eAgMnPc4d1	začínající
čtenáře	čtenář	k1gMnPc4	čtenář
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
výslovnost	výslovnost	k1gFnSc4	výslovnost
složitých	složitý	k2eAgInPc2d1	složitý
znaků	znak	k1gInPc2	znak
kandži	kandzat	k5eAaPmIp1nS	kandzat
<g/>
)	)	kIx)	)
okurigana	okurigana	k1gFnSc1	okurigana
(	(	kIx(	(
<g/>
hiraganový	hiraganový	k2eAgInSc1d1	hiraganový
znak	znak	k1gInSc1	znak
následující	následující	k2eAgInSc1d1	následující
za	za	k7c4	za
kandži	kandž	k1gFnSc3	kandž
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tabulka	tabulka	k1gFnSc1	tabulka
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	s	k7c7	s
znaky	znak	k1gInPc7	znak
hiragany	hiragana	k1gFnSc2	hiragana
píší	psát	k5eAaImIp3nP	psát
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
uspořádaná	uspořádaný	k2eAgFnSc1d1	uspořádaná
podle	podle	k7c2	podle
tradičního	tradiční	k2eAgInSc2d1	tradiční
japonského	japonský	k2eAgInSc2d1	japonský
způsobu	způsob	k1gInSc2	způsob
čtení	čtení	k1gNnSc2	čtení
<g/>
.	.	kIx.	.
</s>
<s>
Začátek	začátek	k1gInSc1	začátek
je	být	k5eAaImIp3nS	být
vpravo	vpravo	k6eAd1	vpravo
nahoře	nahoře	k6eAd1	nahoře
a	a	k8xC	a
postupuje	postupovat	k5eAaImIp3nS	postupovat
se	se	k3xPyFc4	se
shora	shora	k6eAd1	shora
dolů	dolů	k6eAd1	dolů
<g/>
.	.	kIx.	.
</s>
<s>
Malá	malý	k2eAgNnPc4d1	malé
čísla	číslo	k1gNnPc4	číslo
určují	určovat	k5eAaImIp3nP	určovat
pořadí	pořadí	k1gNnPc1	pořadí
tahů	tah	k1gInPc2	tah
a	a	k8xC	a
šipky	šipka	k1gFnPc1	šipka
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
jejich	jejich	k3xOp3gInSc4	jejich
směr	směr	k1gInSc4	směr
<g/>
.	.	kIx.	.
</s>
<s>
Pořadí	pořadí	k1gNnSc1	pořadí
tahů	tah	k1gInPc2	tah
i	i	k8xC	i
směr	směr	k1gInSc1	směr
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
bezpodmínečně	bezpodmínečně	k6eAd1	bezpodmínečně
dodržovat	dodržovat	k5eAaImF	dodržovat
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
chybnému	chybný	k2eAgNnSc3d1	chybné
čtení	čtení	k1gNnSc3	čtení
<g/>
.	.	kIx.	.
kana	kanout	k5eAaImSgInS	kanout
katakana	katakana	k1gFnSc1	katakana
kandži	kandzat	k5eAaPmIp1nS	kandzat
Další	další	k2eAgInPc4d1	další
související	související	k2eAgInPc4d1	související
články	článek	k1gInPc4	článek
hentaigana	hentaigan	k1gMnSc2	hentaigan
man	mana	k1gFnPc2	mana
<g/>
'	'	kIx"	'
<g/>
jógana	jógana	k1gFnSc1	jógana
Japonsko	Japonsko	k1gNnSc1	Japonsko
japonština	japonština	k1gFnSc1	japonština
seznam	seznam	k1gInSc4	seznam
různých	různý	k2eAgNnPc2d1	různé
písem	písmo	k1gNnPc2	písmo
furigana	furigan	k1gMnSc2	furigan
čekošiki	čekošik	k1gFnSc2	čekošik
rómadži	rómadzat	k5eAaPmIp1nS	rómadzat
hebonšiki	hebonšiki	k6eAd1	hebonšiki
rómadži	rómadzat	k5eAaPmIp1nS	rómadzat
Janoš	Janoš	k1gMnSc1	Janoš
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
99	[number]	k4	99
zajímavostí	zajímavost	k1gFnPc2	zajímavost
z	z	k7c2	z
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Albatros	albatros	k1gMnSc1	albatros
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
hiragana	hiragan	k1gMnSc2	hiragan
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
hiragana	hiragana	k1gFnSc1	hiragana
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
www.kanachart.com	www.kanachart.com	k1gInSc1	www.kanachart.com
Free	Free	k1gNnSc2	Free
Kana	kanout	k5eAaImSgInS	kanout
Lessons	Lessons	k1gInSc4	Lessons
Jednoduché	jednoduchý	k2eAgNnSc4d1	jednoduché
zkoušení	zkoušení	k1gNnSc4	zkoušení
z	z	k7c2	z
hiragany	hiragana	k1gFnSc2	hiragana
(	(	kIx(	(
<g/>
s	s	k7c7	s
volitelnými	volitelný	k2eAgFnPc7d1	volitelná
skupinami	skupina	k1gFnPc7	skupina
<g/>
)	)	kIx)	)
</s>
