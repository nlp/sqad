<s>
Hiragana	Hiragana	k1gFnSc1	Hiragana
(	(	kIx(	(
<g/>
ひ	ひ	k?	ひ
–	–	k?	–
hiragana	hiragana	k1gFnSc1	hiragana
<g/>
,	,	kIx,	,
平	平	k?	平
–	–	k?	–
zápis	zápis	k1gInSc1	zápis
v	v	k7c6	v
kandži	kandž	k1gFnSc6	kandž
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jedno	jeden	k4xCgNnSc1	jeden
ze	z	k7c2	z
dvou	dva	k4xCgNnPc2	dva
(	(	kIx(	(
<g/>
druhým	druhý	k4xOgNnSc7	druhý
je	být	k5eAaImIp3nS	být
katakana	katakana	k1gFnSc1	katakana
<g/>
)	)	kIx)	)
japonských	japonský	k2eAgNnPc2d1	Japonské
slabičných	slabičný	k2eAgNnPc2d1	slabičné
písem	písmo	k1gNnPc2	písmo
(	(	kIx(	(
<g/>
ve	v	k7c6	v
slabičném	slabičný	k2eAgNnSc6d1	slabičné
písmu	písmo	k1gNnSc6	písmo
jeden	jeden	k4xCgInSc1	jeden
znak	znak	k1gInSc1	znak
představuje	představovat	k5eAaImIp3nS	představovat
celou	celý	k2eAgFnSc4d1	celá
slabiku	slabika	k1gFnSc4	slabika
a	a	k8xC	a
ne	ne	k9	ne
pouhou	pouhý	k2eAgFnSc4d1	pouhá
hlásku	hláska	k1gFnSc4	hláska
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
znaky	znak	k1gInPc1	znak
i	i	k9	i
pro	pro	k7c4	pro
A	A	kA	A
<g/>
,	,	kIx,	,
I	I	kA	I
<g/>
,	,	kIx,	,
U	U	kA	U
<g/>
,	,	kIx,	,
E	E	kA	E
a	a	k8xC	a
O	O	kA	O
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sestávající	sestávající	k2eAgMnSc1d1	sestávající
ze	z	k7c2	z
46	[number]	k4	46
symbolů	symbol	k1gInPc2	symbol
(	(	kIx(	(
<g/>
45	[number]	k4	45
slabičných	slabičný	k2eAgInPc2d1	slabičný
symbolů	symbol	k1gInPc2	symbol
a	a	k8xC	a
písmeno	písmeno	k1gNnSc4	písmeno
n	n	k0	n
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
