<s>
Dar	dar	k1gInSc1	dar
<g/>
'	'	kIx"	'
<g/>
á	á	k0	á
je	on	k3xPp3gNnPc4	on
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
začala	začít	k5eAaPmAgFnS	začít
syrská	syrský	k2eAgFnSc1d1	Syrská
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
-	-	kIx~	-
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
někdy	někdy	k6eAd1	někdy
říká	říkat	k5eAaImIp3nS	říkat
"	"	kIx"	"
<g/>
kolébka	kolébka	k1gFnSc1	kolébka
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Ve	v	k7c6	v
vleklých	vleklý	k2eAgInPc6d1	vleklý
pouličních	pouliční	k2eAgInPc6d1	pouliční
bojích	boj	k1gInPc6	boj
ale	ale	k9	ale
bylo	být	k5eAaImAgNnS	být
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
značně	značně	k6eAd1	značně
poničeno	poničen	k2eAgNnSc1d1	poničeno
a	a	k8xC	a
žádná	žádný	k3yNgFnSc1	žádný
ze	z	k7c2	z
stran	strana	k1gFnPc2	strana
zatím	zatím	k6eAd1	zatím
město	město	k1gNnSc4	město
úplně	úplně	k6eAd1	úplně
nedobyla	dobýt	k5eNaPmAgFnS	dobýt
<g/>
.	.	kIx.	.
</s>
