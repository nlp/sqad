<s>
Dar	dar	k1gInSc1	dar
<g/>
'	'	kIx"	'
<g/>
á	á	k0	á
(	(	kIx(	(
<g/>
arabsky	arabsky	k6eAd1	arabsky
د	د	k?	د
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
na	na	k7c6	na
jihozápadním	jihozápadní	k2eAgInSc6d1	jihozápadní
okraji	okraj	k1gInSc6	okraj
Sýrie	Sýrie	k1gFnSc2	Sýrie
blízko	blízko	k7c2	blízko
hranice	hranice	k1gFnSc2	hranice
s	s	k7c7	s
Jordánskem	Jordánsko	k1gNnSc7	Jordánsko
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
asi	asi	k9	asi
100	[number]	k4	100
kilometrů	kilometr	k1gInPc2	kilometr
na	na	k7c4	na
jih	jih	k1gInSc4	jih
od	od	k7c2	od
Damašku	Damašek	k1gInSc2	Damašek
na	na	k7c4	na
dálnici	dálnice	k1gFnSc4	dálnice
Damašek-Ammán	Damašek-Ammán	k2eAgMnSc1d1	Damašek-Ammán
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
starobylé	starobylý	k2eAgNnSc4d1	starobylé
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
je	být	k5eAaImIp3nS	být
zmíněno	zmínit	k5eAaPmNgNnS	zmínit
už	už	k9	už
v	v	k7c6	v
hieroglyfických	hieroglyfický	k2eAgInPc6d1	hieroglyfický
záznamech	záznam	k1gInPc6	záznam
egyptského	egyptský	k2eAgMnSc4d1	egyptský
faraona	faraon	k1gMnSc4	faraon
Thutmose	Thutmosa	k1gFnSc3	Thutmosa
III	III	kA	III
<g/>
.	.	kIx.	.
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1490-1436	[number]	k4	1490-1436
před	před	k7c7	před
naším	náš	k3xOp1gInSc7	náš
letopočtem	letopočet	k1gInSc7	letopočet
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
doby	doba	k1gFnSc2	doba
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
ve	v	k7c6	v
městě	město	k1gNnSc6	město
některé	některý	k3yIgFnSc2	některý
historické	historický	k2eAgFnSc2d1	historická
památky	památka	k1gFnSc2	památka
<g/>
:	:	kIx,	:
Je	být	k5eAaImIp3nS	být
tu	tu	k6eAd1	tu
římský	římský	k2eAgInSc1d1	římský
amfiteátr	amfiteátr	k1gInSc1	amfiteátr
a	a	k8xC	a
mešita	mešita	k1gFnSc1	mešita
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
Umajjovců	Umajjovec	k1gMnPc2	Umajjovec
a	a	k8xC	a
Ajjúbovců	Ajjúbovec	k1gMnPc2	Ajjúbovec
<g/>
.	.	kIx.	.
</s>
<s>
Dar	dar	k1gInSc1	dar
<g/>
'	'	kIx"	'
<g/>
á	á	k0	á
je	on	k3xPp3gNnPc4	on
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
začala	začít	k5eAaPmAgFnS	začít
syrská	syrský	k2eAgFnSc1d1	Syrská
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
-	-	kIx~	-
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
někdy	někdy	k6eAd1	někdy
říká	říkat	k5eAaImIp3nS	říkat
"	"	kIx"	"
<g/>
kolébka	kolébka	k1gFnSc1	kolébka
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Ve	v	k7c6	v
vleklých	vleklý	k2eAgInPc6d1	vleklý
pouličních	pouliční	k2eAgInPc6d1	pouliční
bojích	boj	k1gInPc6	boj
ale	ale	k9	ale
bylo	být	k5eAaImAgNnS	být
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
značně	značně	k6eAd1	značně
poničeno	poničen	k2eAgNnSc1d1	poničeno
a	a	k8xC	a
žádná	žádný	k3yNgFnSc1	žádný
ze	z	k7c2	z
stran	strana	k1gFnPc2	strana
zatím	zatím	k6eAd1	zatím
město	město	k1gNnSc4	město
úplně	úplně	k6eAd1	úplně
nedobyla	dobýt	k5eNaPmAgFnS	dobýt
<g/>
.	.	kIx.	.
</s>
