<s>
Makarska	Makarská	k1gFnSc1	Makarská
je	být	k5eAaImIp3nS	být
chorvatské	chorvatský	k2eAgNnSc1d1	Chorvatské
přístavní	přístavní	k2eAgNnSc1d1	přístavní
město	město	k1gNnSc1	město
a	a	k8xC	a
turistické	turistický	k2eAgNnSc1d1	turistické
letovisko	letovisko	k1gNnSc1	letovisko
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Jaderského	jaderský	k2eAgNnSc2d1	Jaderské
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
v	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Dalmácii	Dalmácie	k1gFnSc6	Dalmácie
<g/>
.	.	kIx.	.
</s>
<s>
Makarska	Makarská	k1gFnSc1	Makarská
leží	ležet	k5eAaImIp3nS	ležet
asi	asi	k9	asi
459	[number]	k4	459
km	km	kA	km
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Záhřebu	Záhřeb	k1gInSc2	Záhřeb
<g/>
,60	,60	k4	,60
km	km	kA	km
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
od	od	k7c2	od
Splitu	Split	k1gInSc2	Split
a	a	k8xC	a
140	[number]	k4	140
km	km	kA	km
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
Dubrovníku	Dubrovník	k1gInSc2	Dubrovník
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
14	[number]	k4	14
146	[number]	k4	146
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Makarska	Makarská	k1gFnSc1	Makarská
leží	ležet	k5eAaImIp3nS	ležet
60	[number]	k4	60
km	km	kA	km
jižně	jižně	k6eAd1	jižně
podél	podél	k7c2	podél
pobřeží	pobřeží	k1gNnSc2	pobřeží
od	od	k7c2	od
Splitu	Split	k1gInSc2	Split
a	a	k8xC	a
140	[number]	k4	140
km	km	kA	km
severně	severně	k6eAd1	severně
od	od	k7c2	od
Dubrovníku	Dubrovník	k1gInSc2	Dubrovník
<g/>
.	.	kIx.	.
</s>
<s>
Směrem	směr	k1gInSc7	směr
do	do	k7c2	do
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
při	při	k7c6	při
bosenské	bosenský	k2eAgFnSc6d1	bosenská
hranici	hranice	k1gFnSc6	hranice
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Biokovo	Biokův	k2eAgNnSc4d1	Biokovo
s	s	k7c7	s
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
vrcholem	vrchol	k1gInSc7	vrchol
sv.	sv.	kA	sv.
Jura	jura	k1gFnSc1	jura
(	(	kIx(	(
<g/>
1762	[number]	k4	1762
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
či	či	k8xC	či
horou	hora	k1gFnSc7	hora
Vošac	Vošac	k1gFnSc1	Vošac
(	(	kIx(	(
<g/>
1422	[number]	k4	1422
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
krásné	krásný	k2eAgFnSc3d1	krásná
krajině	krajina	k1gFnSc3	krajina
sem	sem	k6eAd1	sem
již	již	k6eAd1	již
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
začali	začít	k5eAaPmAgMnP	začít
jezdit	jezdit	k5eAaImF	jezdit
turisté	turist	k1gMnPc1	turist
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
československých	československý	k2eAgMnPc2d1	československý
<g/>
,	,	kIx,	,
a	a	k8xC	a
celé	celý	k2eAgFnSc3d1	celá
oblasti	oblast	k1gFnSc3	oblast
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
říkat	říkat	k5eAaImF	říkat
Makarská	Makarský	k2eAgFnSc1d1	Makarská
riviéra	riviéra	k1gFnSc1	riviéra
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
proslulé	proslulý	k2eAgNnSc1d1	proslulé
též	též	k9	též
svou	svůj	k3xOyFgFnSc7	svůj
promenádou	promenáda	k1gFnSc7	promenáda
s	s	k7c7	s
malými	malý	k2eAgInPc7d1	malý
obchůdky	obchůdek	k1gInPc7	obchůdek
a	a	k8xC	a
kavárnami	kavárna	k1gFnPc7	kavárna
<g/>
.	.	kIx.	.
</s>
<s>
Jedinými	jediný	k2eAgFnPc7d1	jediná
významnějšími	významný	k2eAgFnPc7d2	významnější
stavbami	stavba	k1gFnPc7	stavba
zde	zde	k6eAd1	zde
jsou	být	k5eAaImIp3nP	být
kostel	kostel	k1gInSc4	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Marka	Marek	k1gMnSc2	Marek
z	z	k7c2	z
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
a	a	k8xC	a
františkánský	františkánský	k2eAgInSc1d1	františkánský
klášter	klášter	k1gInSc1	klášter
s	s	k7c7	s
malakologickým	malakologický	k2eAgNnSc7d1	Malakologické
muzeem	muzeum	k1gNnSc7	muzeum
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c6	v
měste	měste	k?	měste
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
:	:	kIx,	:
banka	banka	k1gFnSc1	banka
<g/>
,	,	kIx,	,
<g/>
obchod	obchod	k1gInSc1	obchod
<g/>
,	,	kIx,	,
<g/>
nemocnice	nemocnice	k1gFnSc1	nemocnice
<g/>
,	,	kIx,	,
<g/>
základní	základní	k2eAgFnSc2d1	základní
a	a	k8xC	a
střední	střední	k2eAgFnSc2d1	střední
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
<g/>
pošta	pošta	k1gFnSc1	pošta
<g/>
,	,	kIx,	,
<g/>
<g />
.	.	kIx.	.
</s>
<s>
různé	různý	k2eAgInPc1d1	různý
sportovní	sportovní	k2eAgInPc1d1	sportovní
kluby	klub	k1gInPc1	klub
<g/>
.	.	kIx.	.
<g/>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
výchozím	výchozí	k2eAgInSc7d1	výchozí
bodem	bod	k1gInSc7	bod
pro	pro	k7c4	pro
tury	tur	k1gMnPc4	tur
do	do	k7c2	do
přírodního	přírodní	k2eAgInSc2d1	přírodní
parku	park	k1gInSc2	park
Biokovo	Biokův	k2eAgNnSc4d1	Biokovo
barokní	barokní	k2eAgInSc4d1	barokní
kostel	kostel	k1gInSc4	kostel
sv.	sv.	kA	sv.
Filipa	Filip	k1gMnSc4	Filip
Neri	Neri	k1gNnSc2	Neri
františkánský	františkánský	k2eAgInSc4d1	františkánský
klášter	klášter	k1gInSc4	klášter
z	z	k7c2	z
15	[number]	k4	15
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
je	být	k5eAaImIp3nS	být
vybudováno	vybudovat	k5eAaPmNgNnS	vybudovat
malakologické	malakologický	k2eAgNnSc1d1	Malakologické
muzeum	muzeum	k1gNnSc1	muzeum
barokní	barokní	k2eAgFnSc2d1	barokní
farní	farní	k2eAgInSc4d1	farní
kostel	kostel	k1gInSc4	kostel
zasvěcený	zasvěcený	k2eAgInSc4d1	zasvěcený
sv.	sv.	kA	sv.
Marku	marka	k1gFnSc4	marka
pomník	pomník	k1gInSc1	pomník
francouzského	francouzský	k2eAgInSc2d1	francouzský
<g />
.	.	kIx.	.
</s>
<s>
maršála	maršál	k1gMnSc4	maršál
Marmonta	Marmonta	k1gMnSc1	Marmonta
Malakološki	Malakološk	k1gFnSc2	Malakološk
muzej	muzej	k1gInSc1	muzej
(	(	kIx(	(
<g/>
malakologické	malakologický	k2eAgNnSc1d1	Malakologické
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
)	)	kIx)	)
s	s	k7c7	s
bohatou	bohatý	k2eAgFnSc7d1	bohatá
sbírkou	sbírka	k1gFnSc7	sbírka
mušlí	mušle	k1gFnPc2	mušle
Muzej	Muzej	k1gInSc1	Muzej
grada	grada	k1gMnSc1	grada
Makarska	Makarská	k1gFnSc1	Makarská
(	(	kIx(	(
<g/>
městské	městský	k2eAgNnSc1d1	Městské
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
)	)	kIx)	)
Gradska	Gradska	k1gFnSc1	Gradska
galerija	galerij	k2eAgFnSc1d1	galerij
antuna	antuna	k1gFnSc1	antuna
Gojaka	Gojak	k1gMnSc2	Gojak
(	(	kIx(	(
<g/>
městská	městský	k2eAgFnSc1d1	městská
galerie	galerie	k1gFnSc1	galerie
Antuna	Antuna	k1gFnSc1	Antuna
Gojaka	Gojak	k1gMnSc2	Gojak
<g/>
)	)	kIx)	)
Každoročně	každoročně	k6eAd1	každoročně
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
koná	konat	k5eAaImIp3nS	konat
Makarsko	Makarsko	k1gNnSc1	Makarsko
kulturno	kulturno	k1gNnSc4	kulturno
ljeto	ljeto	k1gNnSc1	ljeto
(	(	kIx(	(
<g/>
Makarské	Makarský	k2eAgNnSc1d1	Makarský
léto	léto	k1gNnSc1	léto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
soubor	soubor	k1gInSc4	soubor
různých	různý	k2eAgFnPc2d1	různá
kulturních	kulturní	k2eAgFnPc2d1	kulturní
akcí	akce	k1gFnPc2	akce
od	od	k7c2	od
koncertů	koncert	k1gInPc2	koncert
<g/>
,	,	kIx,	,
výstav	výstava	k1gFnPc2	výstava
až	až	k8xS	až
po	po	k7c4	po
rybářské	rybářský	k2eAgInPc4d1	rybářský
večery	večer	k1gInPc4	večer
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
Makarská	Makarská	k1gFnSc1	Makarská
je	být	k5eAaImIp3nS	být
pojmenovaná	pojmenovaný	k2eAgFnSc1d1	pojmenovaná
po	po	k7c6	po
nedaleké	daleký	k2eNgFnSc6d1	nedaleká
vesnici	vesnice	k1gFnSc6	vesnice
Makru	makro	k1gNnSc6	makro
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnPc1	první
obyvatelé	obyvatel	k1gMnPc1	obyvatel
oblasti	oblast	k1gFnSc2	oblast
Makarské	Makarská	k1gFnSc2	Makarská
dnes	dnes	k6eAd1	dnes
byli	být	k5eAaImAgMnP	být
Ilyrové	Ilyr	k1gMnPc1	Ilyr
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
ve	v	k7c6	v
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
založili	založit	k5eAaPmAgMnP	založit
osadu	osada	k1gFnSc4	osada
s	s	k7c7	s
názvem	název	k1gInSc7	název
Muccurum	Muccurum	k1gNnSc1	Muccurum
<g/>
.	.	kIx.	.
</s>
<s>
Bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Makarské	Makarská	k1gFnSc2	Makarská
18	[number]	k4	18
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
887	[number]	k4	887
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
ní	on	k3xPp3gFnSc6	on
mezi	mezi	k7c7	mezi
Benátskou	benátský	k2eAgFnSc7d1	Benátská
republikou	republika	k1gFnSc7	republika
a	a	k8xC	a
Neretvanským	Neretvanský	k2eAgNnSc7d1	Neretvanský
knížectvím	knížectví	k1gNnSc7	knížectví
<g/>
.	.	kIx.	.
</s>
<s>
Bitvu	bitva	k1gFnSc4	bitva
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
Valley	Valley	k1gInPc4	Valley
<g/>
.	.	kIx.	.
<g/>
Název	název	k1gInSc4	název
Makarská	Makarská	k1gFnSc1	Makarská
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
zmíněn	zmínit	k5eAaPmNgInS	zmínit
v	v	k7c6	v
dokumentu	dokument	k1gInSc6	dokument
ze	z	k7c2	z
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
tureckého	turecký	k2eAgNnSc2d1	turecké
dobývání	dobývání	k1gNnSc2	dobývání
<g/>
.	.	kIx.	.
</s>
<s>
Turci	Turek	k1gMnPc1	Turek
v	v	k7c6	v
Makarské	Makarská	k1gFnSc6	Makarská
udržovaly	udržovat	k5eAaImAgInP	udržovat
armádu	armáda	k1gFnSc4	armáda
a	a	k8xC	a
císařské	císařský	k2eAgFnPc4d1	císařská
výběrčí	výběrčí	k1gFnPc4	výběrčí
daní	daň	k1gFnPc2	daň
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Turecku	Turecko	k1gNnSc6	Turecko
<g/>
,	,	kIx,	,
Makarská	Makarská	k1gFnSc1	Makarská
klesla	klesnout	k5eAaPmAgFnS	klesnout
pod	pod	k7c4	pod
benátskou	benátský	k2eAgFnSc4d1	Benátská
nadvládu	nadvláda	k1gFnSc4	nadvláda
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
habsburské	habsburský	k2eAgFnSc2d1	habsburská
monarchie	monarchie	k1gFnSc2	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
vítězství	vítězství	k1gNnSc4	vítězství
Napoleona	Napoleon	k1gMnSc2	Napoleon
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
<g/>
Makarska	Makarská	k1gFnSc1	Makarská
spadala	spadat	k5eAaPmAgFnS	spadat
pod	pod	k7c4	pod
jeho	jeho	k3xOp3gFnSc4	jeho
říši	říše	k1gFnSc4	říše
<g/>
.	.	kIx.	.
</s>
<s>
Napoleon	Napoleon	k1gMnSc1	Napoleon
povzbudil	povzbudit	k5eAaPmAgMnS	povzbudit
kulturu	kultura	k1gFnSc4	kultura
a	a	k8xC	a
stavěl	stavět	k5eAaImAgMnS	stavět
silnice	silnice	k1gFnPc4	silnice
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
spojují	spojovat	k5eAaImIp3nP	spojovat
Makarskou	Makarská	k1gFnSc4	Makarská
s	s	k7c7	s
jinými	jiný	k2eAgNnPc7d1	jiné
pobřežními	pobřežní	k2eAgNnPc7d1	pobřežní
městy	město	k1gNnPc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
Napoleon	Napoleon	k1gMnSc1	Napoleon
podporoval	podporovat	k5eAaImAgMnS	podporovat
rozvoj	rozvoj	k1gInSc4	rozvoj
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
integraci	integrace	k1gFnSc4	integrace
Makarské	Makarská	k1gFnSc2	Makarská
do	do	k7c2	do
Rakousko-Uherska	Rakousko-Uhersk	k1gInSc2	Rakousko-Uhersk
<g/>
,	,	kIx,	,
<g/>
byl	být	k5eAaImAgInS	být
zodpovědný	zodpovědný	k2eAgInSc1d1	zodpovědný
za	za	k7c4	za
kvetoucí	kvetoucí	k2eAgInSc4d1	kvetoucí
cestovní	cestovní	k2eAgInSc4d1	cestovní
ruch	ruch	k1gInSc4	ruch
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
leží	ležet	k5eAaImIp3nS	ležet
poblíž	poblíž	k6eAd1	poblíž
dálnice	dálnice	k1gFnSc1	dálnice
A1	A1	k1gFnSc1	A1
ze	z	k7c2	z
Záhřebu	Záhřeb	k1gInSc2	Záhřeb
do	do	k7c2	do
Dubrovníku	Dubrovník	k1gInSc2	Dubrovník
<g/>
.	.	kIx.	.
<g/>
Krom	krom	k7c2	krom
toho	ten	k3xDgNnSc2	ten
Městem	město	k1gNnSc7	město
prochazí	prochazit	k5eAaPmIp3nS	prochazit
přímo	přímo	k6eAd1	přímo
silnice	silnice	k1gFnSc1	silnice
D8	D8	k1gFnSc1	D8
(	(	kIx(	(
<g/>
Jadranská	jadranský	k2eAgFnSc1d1	Jadranská
magistála	magistála	k1gFnSc1	magistála
<g/>
)	)	kIx)	)
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
pravidelné	pravidelný	k2eAgNnSc1d1	pravidelné
lodní	lodní	k2eAgNnSc1d1	lodní
spojení	spojení	k1gNnSc1	spojení
se	s	k7c7	s
Sumartinem	Sumartin	k1gInSc7	Sumartin
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Brač	Brač	k1gInSc4	Brač
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Makarska	Makarská	k1gFnSc1	Makarská
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
chorvatsky	chorvatsky	k6eAd1	chorvatsky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
města	město	k1gNnSc2	město
Makarska	Makarská	k1gFnSc1	Makarská
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Informační	informační	k2eAgFnPc4d1	informační
stránky	stránka	k1gFnPc4	stránka
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Historie	historie	k1gFnSc1	historie
města	město	k1gNnSc2	město
Makarska	Makarská	k1gFnSc1	Makarská
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Informace	informace	k1gFnPc1	informace
o	o	k7c6	o
městě	město	k1gNnSc6	město
Makarska	Makarská	k1gFnSc1	Makarská
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Informace	informace	k1gFnPc1	informace
o	o	k7c6	o
Makarské	Makarský	k2eAgFnSc6d1	Makarská
riviéře	riviéra	k1gFnSc6	riviéra
(	(	kIx(	(
<g/>
chorvatsky	chorvatsky	k6eAd1	chorvatsky
<g/>
)	)	kIx)	)
Aktuální	aktuální	k2eAgNnSc1d1	aktuální
počasí	počasí	k1gNnSc1	počasí
v	v	k7c6	v
Chorvatsku	Chorvatsko	k1gNnSc6	Chorvatsko
</s>
