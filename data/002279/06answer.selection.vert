<s>
Makarska	Makarská	k1gFnSc1	Makarská
je	být	k5eAaImIp3nS	být
chorvatské	chorvatský	k2eAgNnSc1d1	Chorvatské
přístavní	přístavní	k2eAgNnSc1d1	přístavní
město	město	k1gNnSc1	město
a	a	k8xC	a
turistické	turistický	k2eAgNnSc1d1	turistické
letovisko	letovisko	k1gNnSc1	letovisko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Jaderského	jaderský	k2eAgNnSc2d1	Jaderské
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
v	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Dalmácii	Dalmácie	k1gFnSc6	Dalmácie
<g/>
.	.	kIx.	.
</s>
