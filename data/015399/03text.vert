<s>
Hudební	hudební	k2eAgFnSc1d1
a	a	k8xC
taneční	taneční	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
Akademie	akademie	k1gFnSc2
múzických	múzický	k2eAgNnPc2d1
umění	umění	k1gNnPc2
v	v	k7c6
Praze	Praha	k1gFnSc6
</s>
<s>
Hudební	hudební	k2eAgFnSc1d1
a	a	k8xC
taneční	taneční	k2eAgFnSc1d1
fakultaAkademie	fakultaAkademie	k1gFnSc1
múzických	múzický	k2eAgNnPc2d1
umění	umění	k1gNnPc2
v	v	k7c6
Praze	Praha	k1gFnSc6
Lichtenštejnský	lichtenštejnský	k2eAgInSc1d1
palác	palác	k1gInSc1
<g/>
,	,	kIx,
hlavní	hlavní	k2eAgFnSc1d1
budova	budova	k1gFnSc1
školyVedení	školyVedení	k1gNnSc2
fakulty	fakulta	k1gFnSc2
Děkan	děkan	k1gMnSc1
</s>
<s>
prof.	prof.	kA
Ivan	Ivan	k1gMnSc1
Klánský	Klánský	k1gMnSc1
Proděkan	proděkan	k1gMnSc1
</s>
<s>
MgA.	MgA.	k?
Irvin	Irvin	k1gMnSc1
Venyš	Venyš	k1gMnSc1
<g/>
,	,	kIx,
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
Proděkan	proděkan	k1gMnSc1
</s>
<s>
doc.	doc.	kA
Mgr.	Mgr.	kA
Václav	Václav	k1gMnSc1
Janeček	Janeček	k1gMnSc1
<g/>
,	,	kIx,
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
Proděkan	proděkan	k1gMnSc1
</s>
<s>
prof.	prof.	kA
Vlastimil	Vlastimil	k1gMnSc1
Mareš	Mareš	k1gMnSc1
Tajemník	tajemník	k1gMnSc1
</s>
<s>
Ing.	ing.	kA
Jolana	Jolana	k1gFnSc1
Krotilová	Krotilová	k1gFnSc1
Statistické	statistický	k2eAgInPc4d1
údaje	údaj	k1gInPc4
Katedry	katedra	k1gFnSc2
</s>
<s>
13	#num#	k4
Ústavy	ústava	k1gFnSc2
</s>
<s>
5	#num#	k4
Základní	základní	k2eAgFnPc4d1
informace	informace	k1gFnPc4
Datum	datum	k1gInSc4
založení	založení	k1gNnSc1
</s>
<s>
1945	#num#	k4
Kontaktní	kontaktní	k2eAgInPc1d1
údaje	údaj	k1gInPc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
5	#num#	k4
<g/>
′	′	k?
<g/>
17,42	17,42	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
24	#num#	k4
<g/>
′	′	k?
<g/>
7,65	7,65	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
www.hamu.cz	www.hamu.cz	k1gInSc1
</s>
<s>
Hudební	hudební	k2eAgFnSc1d1
a	a	k8xC
taneční	taneční	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
(	(	kIx(
<g/>
HAMU	HAMU	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
jednou	jeden	k4xCgFnSc7
ze	z	k7c2
tří	tři	k4xCgFnPc2
fakult	fakulta	k1gFnPc2
Akademie	akademie	k1gFnSc2
múzických	múzický	k2eAgNnPc2d1
umění	umění	k1gNnPc2
v	v	k7c6
Praze	Praha	k1gFnSc6
(	(	kIx(
<g/>
DAMU	DAMU	kA
<g/>
,	,	kIx,
FAMU	FAMU	kA
<g/>
,	,	kIx,
HAMU	HAMU	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
dekretem	dekret	k1gInSc7
prezidenta	prezident	k1gMnSc2
republiky	republika	k1gFnSc2
dr	dr	kA
<g/>
.	.	kIx.
Eduarda	Eduard	k1gMnSc2
Beneše	Beneš	k1gMnSc2
z	z	k7c2
27	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1945	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sídlí	sídlet	k5eAaImIp3nS
v	v	k7c6
Praze	Praha	k1gFnSc6
na	na	k7c6
Malé	Malé	k2eAgFnSc6d1
Straně	strana	k1gFnSc6
v	v	k7c6
Lichtenštejnském	lichtenštejnský	k2eAgInSc6d1
paláci	palác	k1gInSc6
a	a	k8xC
Hartigovském	Hartigovský	k2eAgInSc6d1
paláci	palác	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
22	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2010	#num#	k4
nesla	nést	k5eAaImAgFnS
název	název	k1gInSc4
Hudební	hudební	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Děkanem	děkan	k1gMnSc7
byl	být	k5eAaImAgInS
od	od	k7c2
22	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2010	#num#	k4
do	do	k7c2
roku	rok	k1gInSc2
2018	#num#	k4
klarinetista	klarinetista	k1gMnSc1
Vlastimil	Vlastimil	k1gMnSc1
Mareš	Mareš	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
nového	nový	k2eAgInSc2d1
akademického	akademický	k2eAgInSc2d1
roku	rok	k1gInSc2
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
2019	#num#	k4
je	být	k5eAaImIp3nS
jím	on	k3xPp3gMnSc7
pianista	pianista	k1gMnSc1
Ivan	Ivan	k1gMnSc1
Klánský	Klánský	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Katedra	katedra	k1gFnSc1
klávesových	klávesový	k2eAgInPc2d1
nástrojů	nástroj	k1gInPc2
</s>
<s>
Hartigovský	Hartigovský	k2eAgInSc1d1
palác	palác	k1gInSc1
<g/>
,	,	kIx,
sídlo	sídlo	k1gNnSc1
vedení	vedení	k1gNnSc2
HAMU	HAMU	kA
</s>
<s>
Sál	sál	k1gInSc1
Bohuslava	Bohuslav	k1gMnSc4
Martinů	Martinů	k1gFnSc2
</s>
<s>
Katedra	katedra	k1gFnSc1
navázala	navázat	k5eAaPmAgFnS
na	na	k7c4
tradice	tradice	k1gFnPc4
Mistrovské	mistrovský	k2eAgFnSc2d1
školy	škola	k1gFnSc2
pražské	pražský	k2eAgFnSc2d1
konzervatoře	konzervatoř	k1gFnSc2
<g/>
,	,	kIx,
vedené	vedený	k2eAgNnSc1d1
legendárním	legendární	k2eAgMnSc7d1
Vilémem	Vilém	k1gMnSc7
Kurzem	Kurz	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Katedra	katedra	k1gFnSc1
vyučuje	vyučovat	k5eAaImIp3nS
tři	tři	k4xCgFnPc4
obory	obora	k1gFnPc4
–	–	k?
klavír	klavír	k1gInSc1
<g/>
,	,	kIx,
varhany	varhany	k1gInPc1
a	a	k8xC
cembalo	cembalo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
zrodu	zrod	k1gInSc2
stáli	stát	k5eAaImAgMnP
profesoři	profesor	k1gMnPc1
Ilona	Ilona	k1gFnSc1
Štěpánová-Kurzová	Štěpánová-Kurzová	k1gFnSc1
a	a	k8xC
František	František	k1gMnSc1
Maxián	Maxián	k1gMnSc1
<g/>
,	,	kIx,
záhy	záhy	k6eAd1
se	se	k3xPyFc4
připojil	připojit	k5eAaPmAgMnS
prof.	prof.	kA
František	František	k1gMnSc1
Rauch	Rauch	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc7d1
desetiletí	desetiletí	k1gNnSc4
práce	práce	k1gFnSc2
katedry	katedra	k1gFnSc2
jsou	být	k5eAaImIp3nP
spjata	spjat	k2eAgNnPc4d1
s	s	k7c7
dalšími	další	k2eAgFnPc7d1
velkými	velký	k2eAgFnPc7d1
osobnostmi	osobnost	k1gFnPc7
<g/>
:	:	kIx,
</s>
<s>
mezi	mezi	k7c4
klavíristy	klavírista	k1gMnPc4
to	ten	k3xDgNnSc1
byli	být	k5eAaImAgMnP
Josef	Josef	k1gMnSc1
Páleníček	Páleníčko	k1gNnPc2
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
Štěpán	Štěpán	k1gMnSc1
<g/>
,	,	kIx,
Valentina	Valentina	k1gFnSc1
Kameníková	Kameníková	k1gFnSc1
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
Jílek	Jílek	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
Panenka	panenka	k1gFnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
Hála	Hála	k1gMnSc1
<g/>
,	,	kIx,
Ivan	Ivan	k1gMnSc1
Moravec	Moravec	k1gMnSc1
<g/>
,	,	kIx,
Ivan	Ivan	k1gMnSc1
Klánský	Klánský	k1gMnSc1
<g/>
,	,	kIx,
Miroslav	Miroslav	k1gMnSc1
Langer	Langer	k1gMnSc1
a	a	k8xC
řada	řada	k1gFnSc1
dalších	další	k2eAgMnPc2d1
<g/>
;	;	kIx,
</s>
<s>
mezi	mezi	k7c7
varhaníky	varhaník	k1gMnPc7
pak	pak	k6eAd1
především	především	k9
Bedřich	Bedřich	k1gMnSc1
Antonín	Antonín	k1gMnSc1
Wiedermann	Wiedermann	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
Reinberger	Reinberger	k1gMnSc1
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
Šlechta	Šlechta	k1gMnSc1
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
Rabas	Rabas	k1gMnSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
Tůma	Tůma	k1gMnSc1
<g/>
;	;	kIx,
</s>
<s>
v	v	k7c6
oboru	obor	k1gInSc6
cembala	cembalo	k1gNnSc2
Zuzana	Zuzana	k1gFnSc1
Růžičková	Růžičková	k1gFnSc1
a	a	k8xC
Giedrė	Giedrė	k1gFnSc1
Lukšaitė	Lukšaitė	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Vedoucím	vedoucí	k1gMnSc7
katedry	katedra	k1gFnSc2
je	být	k5eAaImIp3nS
profesor	profesor	k1gMnSc1
František	František	k1gMnSc1
Malý	Malý	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Katedra	katedra	k1gFnSc1
strunných	strunný	k2eAgInPc2d1
nástrojů	nástroj	k1gInPc2
</s>
<s>
V	v	k7c6
průběhu	průběh	k1gInSc6
šesti	šest	k4xCc2
desetiletí	desetiletí	k1gNnPc2
trvání	trvání	k1gNnSc2
AMU	AMU	kA
působili	působit	k5eAaImAgMnP
na	na	k7c6
katedře	katedra	k1gFnSc6
nástrojů	nástroj	k1gInPc2
strunných	strunný	k2eAgInPc2d1
mj.	mj.	kA
Jindřich	Jindřich	k1gMnSc1
Feld	Feld	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
Micka	micka	k1gFnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
Daniel	Daniel	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
Peška	Peška	k1gMnSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
Pekelský	pekelský	k2eAgMnSc1d1
<g/>
,	,	kIx,
Marie	Marie	k1gFnSc1
Hlouňová	Hlouňová	k1gFnSc1
<g/>
,	,	kIx,
Nora	Nora	k1gFnSc1
Grumlíková	Grumlíková	k1gFnSc1
<g/>
,	,	kIx,
Alexandr	Alexandr	k1gMnSc1
Plocek	Plocka	k1gFnPc2
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
Novák	Novák	k1gMnSc1
<g/>
,	,	kIx,
Lubomír	Lubomír	k1gMnSc1
Kostecký	Kostecký	k2eAgMnSc1d1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
Josef	Josef	k1gMnSc1
Vlach	Vlach	k1gMnSc1
<g/>
,	,	kIx,
Ladislav	Ladislav	k1gMnSc1
Černý	Černý	k1gMnSc1
<g/>
,	,	kIx,
Ladislav	Ladislav	k1gMnSc1
Zelenka	Zelenka	k1gMnSc1
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
Pravoslav	Pravoslav	k1gMnSc1
Sádlo	sádlo	k1gNnSc4
<g/>
,	,	kIx,
Miloš	Miloš	k1gMnSc1
Sádlo	sádlo	k1gNnSc1
<g/>
,	,	kIx,
Antonín	Antonín	k1gMnSc1
Kohout	Kohout	k1gMnSc1
<g/>
,	,	kIx,
Alexandr	Alexandr	k1gMnSc1
Večtomov	Večtomov	k1gInSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
Hertl	Hertl	k1gMnSc1
<g/>
,	,	kIx,
Marie	Marie	k1gFnSc1
Zunová	Zunová	k1gFnSc1
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
Patras	Patras	k1gMnSc1
ad	ad	k7c4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
v	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
Václav	Václav	k1gMnSc1
Bernášek	Bernášek	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
Ondřej	Ondřej	k1gMnSc1
Kvita	Kvita	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
Hošek	Hošek	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
Hudec	Hudec	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
Chuchro	Chuchra	k1gFnSc5
<g/>
,	,	kIx,
Renata	Renata	k1gFnSc1
Kodadová	Kodadový	k2eAgFnSc1d1
<g/>
,	,	kIx,
Lubomír	Lubomír	k1gMnSc1
Malý	Malý	k1gMnSc1
<g/>
,	,	kIx,
Bohuslav	Bohuslav	k1gMnSc1
Matoušek	Matoušek	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
Messiereur	Messiereur	k1gMnSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
Motlík	Motlík	k1gMnSc1
<g/>
,	,	kIx,
Jindřich	Jindřich	k1gMnSc1
Pazdera	Pazdera	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
Pěruška	Pěruška	k1gMnSc1
<g/>
,	,	kIx,
Miroslav	Miroslav	k1gMnSc1
Petráš	Petráš	k1gMnSc1
<g/>
,	,	kIx,
Štěpán	Štěpán	k1gMnSc1
Rak	rak	k1gMnSc1
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
Snítil	Snítil	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
Tomášek	Tomášek	k1gMnSc1
<g/>
,	,	kIx,
Ivan	Ivan	k1gMnSc1
Štraus	Štraus	k1gMnSc1
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
Škampa	škampa	k1gFnSc1
<g/>
,	,	kIx,
Daniel	Daniel	k1gMnSc1
Veis	Veis	k1gInSc1
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
Zelenka	Zelenka	k1gMnSc1
<g/>
,	,	kIx,
Radomír	Radomír	k1gMnSc1
Žalud	Žalud	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Katedra	katedra	k1gFnSc1
dirigování	dirigování	k1gNnSc2
</s>
<s>
Jednou	jeden	k4xCgFnSc7
z	z	k7c2
osobností	osobnost	k1gFnPc2
katedry	katedra	k1gFnSc2
dirigování	dirigování	k1gNnSc2
byl	být	k5eAaImAgMnS
šéfdirigent	šéfdirigent	k1gMnSc1
České	český	k2eAgFnSc2d1
filharmonie	filharmonie	k1gFnSc2
Jiří	Jiří	k1gMnSc1
Bělohlávek	Bělohlávek	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
jeho	jeho	k3xOp3gMnPc4
žáky	žák	k1gMnPc4
se	se	k3xPyFc4
zařadili	zařadit	k5eAaPmAgMnP
například	například	k6eAd1
Tomáš	Tomáš	k1gMnSc1
Netopil	topit	k5eNaImAgMnS
či	či	k8xC
Jakub	Jakub	k1gMnSc1
Hrůša	Hrůša	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Katedru	katedra	k1gFnSc4
vede	vést	k5eAaImIp3nS
docent	docent	k1gMnSc1
Tomáš	Tomáš	k1gMnSc1
Koutník	Koutník	k1gMnSc1
<g/>
,	,	kIx,
někdejší	někdejší	k2eAgMnSc1d1
ředitel	ředitel	k1gMnSc1
a	a	k8xC
šéfdirigent	šéfdirigent	k1gMnSc1
Severočeské	severočeský	k2eAgFnSc2d1
filharmonie	filharmonie	k1gFnSc2
Teplice	teplice	k1gFnSc2
a	a	k8xC
dirigent	dirigent	k1gMnSc1
řady	řada	k1gFnSc2
dalších	další	k2eAgMnPc2d1
českých	český	k2eAgMnPc2d1
a	a	k8xC
slovenských	slovenský	k2eAgMnPc2d1
orchestrů	orchestr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
pedagogy	pedagog	k1gMnPc4
patří	patřit	k5eAaImIp3nS
také	také	k9
prof.	prof.	kA
Leoš	Leoš	k1gMnSc1
Svárovský	Svárovský	k1gMnSc1
<g/>
,	,	kIx,
český	český	k2eAgMnSc1d1
dirigent	dirigent	k1gMnSc1
<g/>
;	;	kIx,
dále	daleko	k6eAd2
také	také	k9
prof.	prof.	kA
Ivan	Ivan	k1gMnSc1
Pařík	Pařík	k1gMnSc1
<g/>
,	,	kIx,
emeritní	emeritní	k2eAgMnSc1d1
pedagog	pedagog	k1gMnSc1
Universität	Universitäta	k1gFnPc2
für	für	k?
Musik	musika	k1gFnPc2
und	und	k?
darstellende	darstellend	k1gInSc5
Kunst	Kunst	k1gFnSc4
Wien	Wien	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Pražská	pražský	k2eAgFnSc1d1
katedra	katedra	k1gFnSc1
dirigování	dirigování	k1gNnSc2
se	se	k3xPyFc4
zaměřuje	zaměřovat	k5eAaImIp3nS
jak	jak	k6eAd1
na	na	k7c4
studium	studium	k1gNnSc4
symfonického	symfonický	k2eAgInSc2d1
repertoáru	repertoár	k1gInSc2
<g/>
,	,	kIx,
tak	tak	k9
též	též	k9
na	na	k7c4
studium	studium	k1gNnSc4
sborového	sborový	k2eAgInSc2d1
i	i	k8xC
operního	operní	k2eAgInSc2d1
repertoáru	repertoár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Posluchači	posluchač	k1gMnPc1
mají	mít	k5eAaImIp3nP
od	od	k7c2
druhého	druhý	k4xOgInSc2
ročníku	ročník	k1gInSc2
vlastní	vlastnit	k5eAaImIp3nS
symfonický	symfonický	k2eAgInSc1d1
koncert	koncert	k1gInSc1
a	a	k8xC
zároveň	zároveň	k6eAd1
se	se	k3xPyFc4
několikrát	několikrát	k6eAd1
do	do	k7c2
roka	rok	k1gInSc2
účastní	účastnit	k5eAaImIp3nS
dirigentských	dirigentský	k2eAgInPc2d1
kurzů	kurz	k1gInPc2
u	u	k7c2
Severočeské	severočeský	k2eAgFnSc2d1
filharmonie	filharmonie	k1gFnSc2
Teplice	teplice	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yIgInPc4,k3yQgInPc4,k3yRgInPc4
pořádá	pořádat	k5eAaImIp3nS
docent	docent	k1gMnSc1
Tomáš	Tomáš	k1gMnSc1
Koutník	Koutník	k1gMnSc1
<g/>
,	,	kIx,
vedoucí	vedoucí	k1gMnSc1
katedry	katedra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Lichtenštejnský	lichtenštejnský	k2eAgInSc1d1
palác	palác	k1gInSc1
</s>
<s>
Pamětní	pamětní	k2eAgFnSc1d1
deska	deska	k1gFnSc1
spoluzakladatelky	spoluzakladatelka	k1gFnSc2
školy	škola	k1gFnSc2
prof.	prof.	kA
Ilony	Ilona	k1gFnSc2
Štěpánové-Kurzové	Štěpánové-Kurzová	k1gFnSc2
v	v	k7c6
budově	budova	k1gFnSc6
školy	škola	k1gFnSc2
</s>
<s>
Pamětní	pamětní	k2eAgFnSc1d1
deska	deska	k1gFnSc1
spoluzakladatele	spoluzakladatel	k1gMnSc2
školy	škola	k1gFnSc2
prof.	prof.	kA
Františka	František	k1gMnSc2
Maxiána	Maxián	k1gMnSc2
v	v	k7c6
budově	budova	k1gFnSc6
školy	škola	k1gFnSc2
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Cinepur	Cinepura	k1gFnPc2
<g/>
:	:	kIx,
Otazníky	otazník	k1gInPc7
kolem	kolem	k7c2
volby	volba	k1gFnSc2
rektora	rektor	k1gMnSc2
AMU	AMU	kA
<g/>
...	...	k?
<g/>
↑	↑	k?
Lidé	člověk	k1gMnPc1
|	|	kIx~
HAMU	HAMU	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
HAMU	HAMU	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Hudební	hudební	k2eAgFnSc1d1
a	a	k8xC
taneční	taneční	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
Akademie	akademie	k1gFnSc2
múzických	múzický	k2eAgNnPc2d1
umění	umění	k1gNnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Hudební	hudební	k2eAgFnSc1d1
a	a	k8xC
taneční	taneční	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
Akademie	akademie	k1gFnSc2
múzických	múzický	k2eAgNnPc2d1
umění	umění	k1gNnPc2
v	v	k7c6
Praze	Praha	k1gFnSc6
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Akademie	akademie	k1gFnSc1
múzických	múzický	k2eAgNnPc2d1
umění	umění	k1gNnPc2
v	v	k7c6
Praze	Praha	k1gFnSc6
Fakulty	fakulta	k1gFnSc2
</s>
<s>
Filmová	filmový	k2eAgFnSc1d1
a	a	k8xC
televizní	televizní	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
•	•	k?
Hudební	hudební	k2eAgFnSc1d1
a	a	k8xC
taneční	taneční	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
•	•	k?
Divadelní	divadelní	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
Pedagogická	pedagogický	k2eAgFnSc1d1
pracoviště	pracoviště	k1gNnSc4
</s>
<s>
Katedra	katedra	k1gFnSc1
cizích	cizí	k2eAgMnPc2d1
jazyků	jazyk	k1gMnPc2
AMU	AMU	kA
•	•	k?
Katedra	katedra	k1gFnSc1
tělesné	tělesný	k2eAgFnSc2d1
výchovy	výchova	k1gFnSc2
AMU	AMU	kA
Informační	informační	k2eAgNnSc1d1
pracoviště	pracoviště	k1gNnSc1
</s>
<s>
Knihovna	knihovna	k1gFnSc1
AMU	AMU	kA
•	•	k?
Počítačové	počítačový	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
AMU	AMU	kA
•	•	k?
Nakladatelství	nakladatelství	k1gNnSc1
AMU	AMU	kA
Jednotky	jednotka	k1gFnSc2
fakult	fakulta	k1gFnPc2
</s>
<s>
Divadlo	divadlo	k1gNnSc1
DISK	disk	k1gInSc1
(	(	kIx(
<g/>
DAMU	DAMU	kA
<g/>
)	)	kIx)
•	•	k?
Studio	studio	k1gNnSc1
FAMU	FAMU	kA
(	(	kIx(
<g/>
FAMU	FAMU	kA
<g/>
)	)	kIx)
Účelová	účelový	k2eAgNnPc1d1
zařízení	zařízení	k1gNnPc1
</s>
<s>
Kolej	kolej	k1gFnSc1
a	a	k8xC
učební	učební	k2eAgNnSc1d1
středisko	středisko	k1gNnSc1
•	•	k?
Ubytovací	ubytovací	k2eAgNnSc1d1
zařízení	zařízení	k1gNnSc1
•	•	k?
Učební	učební	k2eAgMnSc1d1
a	a	k8xC
výcvikové	výcvikový	k2eAgNnSc1d1
středisko	středisko	k1gNnSc1
Hluboká	Hluboká	k1gFnSc1
nad	nad	k7c4
Vltavou-Poněšice	Vltavou-Poněšic	k1gMnSc4
•	•	k?
Učební	učební	k2eAgNnSc1d1
a	a	k8xC
výcvikové	výcvikový	k2eAgNnSc1d1
středisko	středisko	k1gNnSc1
Beroun	Beroun	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
kn	kn	k?
<g/>
20010709002	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
98113076	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
158456541	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
98113076	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Hudba	hudba	k1gFnSc1
|	|	kIx~
Praha	Praha	k1gFnSc1
|	|	kIx~
Umění	umění	k1gNnSc1
</s>
