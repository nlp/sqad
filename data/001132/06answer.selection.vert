<s>
Ashton	Ashton	k1gInSc1	Ashton
Eaton	Eaton	k1gInSc1	Eaton
(	(	kIx(	(
<g/>
*	*	kIx~	*
21	[number]	k4	21
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1988	[number]	k4	1988
Bend	Bend	k1gMnSc1	Bend
<g/>
,	,	kIx,	,
Oregon	Oregon	k1gMnSc1	Oregon
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
atlet	atlet	k1gMnSc1	atlet
<g/>
,	,	kIx,	,
závodící	závodící	k2eAgMnSc1d1	závodící
ve	v	k7c6	v
vícebojích	víceboj	k1gInPc6	víceboj
a	a	k8xC	a
současný	současný	k2eAgMnSc1d1	současný
držitel	držitel	k1gMnSc1	držitel
světových	světový	k2eAgInPc2d1	světový
rekordů	rekord	k1gInPc2	rekord
v	v	k7c6	v
halovém	halový	k2eAgInSc6d1	halový
sedmiboji	sedmiboj	k1gInSc6	sedmiboj
i	i	k8xC	i
venkovním	venkovní	k2eAgInSc6d1	venkovní
desetiboji	desetiboj	k1gInSc6	desetiboj
<g/>
,	,	kIx,	,
dvojnásobný	dvojnásobný	k2eAgMnSc1d1	dvojnásobný
Olympijský	olympijský	k2eAgMnSc1d1	olympijský
vítěz	vítěz	k1gMnSc1	vítěz
(	(	kIx(	(
<g/>
Londýn	Londýn	k1gInSc1	Londýn
2012	[number]	k4	2012
-	-	kIx~	-
8869	[number]	k4	8869
b.	b.	k?	b.
a	a	k8xC	a
Rio	Rio	k1gFnSc1	Rio
de	de	k?	de
Janeiro	Janeiro	k1gNnSc1	Janeiro
2016	[number]	k4	2016
-	-	kIx~	-
8893	[number]	k4	8893
b.	b.	k?	b.
<g/>
-vyrovnaný	yrovnaný	k2eAgMnSc1d1	-vyrovnaný
OR	OR	kA	OR
Romana	Roman	k1gMnSc4	Roman
Šebrleho	Šebrle	k1gMnSc4	Šebrle
<g/>
)	)	kIx)	)
a	a	k8xC	a
dvonásobný	dvonásobný	k2eAgMnSc1d1	dvonásobný
mistr	mistr	k1gMnSc1	mistr
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
Moskva	Moskva	k1gFnSc1	Moskva
2013	[number]	k4	2013
a	a	k8xC	a
Peking	Peking	k1gInSc1	Peking
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
