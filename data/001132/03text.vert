<s>
Ashton	Ashton	k1gInSc1	Ashton
Eaton	Eaton	k1gInSc1	Eaton
(	(	kIx(	(
<g/>
*	*	kIx~	*
21	[number]	k4	21
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1988	[number]	k4	1988
Bend	Bend	k1gMnSc1	Bend
<g/>
,	,	kIx,	,
Oregon	Oregon	k1gMnSc1	Oregon
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
atlet	atlet	k1gMnSc1	atlet
<g/>
,	,	kIx,	,
závodící	závodící	k2eAgMnSc1d1	závodící
ve	v	k7c6	v
vícebojích	víceboj	k1gInPc6	víceboj
a	a	k8xC	a
současný	současný	k2eAgMnSc1d1	současný
držitel	držitel	k1gMnSc1	držitel
světových	světový	k2eAgInPc2d1	světový
rekordů	rekord	k1gInPc2	rekord
v	v	k7c6	v
halovém	halový	k2eAgInSc6d1	halový
sedmiboji	sedmiboj	k1gInSc6	sedmiboj
i	i	k8xC	i
venkovním	venkovní	k2eAgInSc6d1	venkovní
desetiboji	desetiboj	k1gInSc6	desetiboj
<g/>
,	,	kIx,	,
dvojnásobný	dvojnásobný	k2eAgMnSc1d1	dvojnásobný
Olympijský	olympijský	k2eAgMnSc1d1	olympijský
vítěz	vítěz	k1gMnSc1	vítěz
(	(	kIx(	(
<g/>
Londýn	Londýn	k1gInSc1	Londýn
2012	[number]	k4	2012
-	-	kIx~	-
8869	[number]	k4	8869
b.	b.	k?	b.
a	a	k8xC	a
Rio	Rio	k1gFnSc1	Rio
de	de	k?	de
Janeiro	Janeiro	k1gNnSc1	Janeiro
2016	[number]	k4	2016
-	-	kIx~	-
8893	[number]	k4	8893
b.	b.	k?	b.
<g/>
-vyrovnaný	yrovnaný	k2eAgMnSc1d1	-vyrovnaný
OR	OR	kA	OR
Romana	Roman	k1gMnSc4	Roman
Šebrleho	Šebrle	k1gMnSc4	Šebrle
<g/>
)	)	kIx)	)
a	a	k8xC	a
dvonásobný	dvonásobný	k2eAgMnSc1d1	dvonásobný
mistr	mistr	k1gMnSc1	mistr
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
Moskva	Moskva	k1gFnSc1	Moskva
2013	[number]	k4	2013
a	a	k8xC	a
Peking	Peking	k1gInSc1	Peking
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
3	[number]	k4	3
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2017	[number]	k4	2017
oznámil	oznámit	k5eAaPmAgMnS	oznámit
na	na	k7c6	na
sociální	sociální	k2eAgFnSc6d1	sociální
síti	síť	k1gFnSc6	síť
překvapivě	překvapivě	k6eAd1	překvapivě
konec	konec	k1gInSc4	konec
své	svůj	k3xOyFgFnSc2	svůj
aktivní	aktivní	k2eAgFnSc2d1	aktivní
kariéry	kariéra	k1gFnSc2	kariéra
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
manželkou	manželka	k1gFnSc7	manželka
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
úspěšnou	úspěšný	k2eAgFnSc7d1	úspěšná
atletickou	atletický	k2eAgFnSc7d1	atletická
vícebojařkou	vícebojařka	k1gFnSc7	vícebojařka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
překvapivě	překvapivě	k6eAd1	překvapivě
překonal	překonat	k5eAaPmAgMnS	překonat
na	na	k7c6	na
americkém	americký	k2eAgInSc6d1	americký
univerzitním	univerzitní	k2eAgInSc6d1	univerzitní
šampionátu	šampionát	k1gInSc6	šampionát
17	[number]	k4	17
let	léto	k1gNnPc2	léto
starý	starý	k2eAgInSc1d1	starý
světový	světový	k2eAgInSc4d1	světový
rekord	rekord	k1gInSc4	rekord
Dana	Dana	k1gFnSc1	Dana
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Briena	Brieno	k1gNnPc4	Brieno
v	v	k7c6	v
halovém	halový	k2eAgInSc6d1	halový
sedmiboji	sedmiboj	k1gInSc6	sedmiboj
výkonem	výkon	k1gInSc7	výkon
6	[number]	k4	6
499	[number]	k4	499
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
pak	pak	k6eAd1	pak
v	v	k7c6	v
estonském	estonský	k2eAgInSc6d1	estonský
Tallinnu	Tallinn	k1gInSc6	Tallinn
rekord	rekord	k1gInSc1	rekord
posunul	posunout	k5eAaPmAgInS	posunout
až	až	k9	až
na	na	k7c4	na
6568	[number]	k4	6568
bodů	bod	k1gInPc2	bod
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
prvním	první	k4xOgMnSc7	první
vícebojařem	vícebojař	k1gMnSc7	vícebojař
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
pokořil	pokořit	k5eAaPmAgInS	pokořit
hranici	hranice	k1gFnSc4	hranice
6500	[number]	k4	6500
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
Eaton	Eaton	k1gInSc1	Eaton
vybojoval	vybojovat	k5eAaPmAgInS	vybojovat
stříbrnou	stříbrný	k2eAgFnSc4d1	stříbrná
medaili	medaile	k1gFnSc4	medaile
na	na	k7c4	na
MS	MS	kA	MS
v	v	k7c6	v
jihokorejském	jihokorejský	k2eAgInSc6d1	jihokorejský
Tegu	Tegus	k1gInSc6	Tegus
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
osobní	osobní	k2eAgInSc1d1	osobní
rekord	rekord	k1gInSc1	rekord
v	v	k7c6	v
desetiboji	desetiboj	k1gInSc6	desetiboj
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
měl	mít	k5eAaImAgMnS	mít
hodnotu	hodnota	k1gFnSc4	hodnota
9039	[number]	k4	9039
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
výkonem	výkon	k1gInSc7	výkon
zároveň	zároveň	k6eAd1	zároveň
překonal	překonat	k5eAaPmAgMnS	překonat
předchozí	předchozí	k2eAgInSc4d1	předchozí
světový	světový	k2eAgInSc4d1	světový
rekord	rekord	k1gInSc4	rekord
Čecha	Čech	k1gMnSc2	Čech
Romana	Roman	k1gMnSc2	Roman
Šebrleho	Šebrle	k1gMnSc2	Šebrle
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
(	(	kIx(	(
<g/>
9026	[number]	k4	9026
bodů	bod	k1gInPc2	bod
<g/>
)	)	kIx)	)
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
druhým	druhý	k4xOgMnSc7	druhý
člověkem	člověk	k1gMnSc7	člověk
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
pokořil	pokořit	k5eAaPmAgInS	pokořit
devítitisícovou	devítitisícový	k2eAgFnSc4d1	devítitisícová
hranici	hranice	k1gFnSc4	hranice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
na	na	k7c6	na
HMS	HMS	kA	HMS
v	v	k7c6	v
Istanbulu	Istanbul	k1gInSc6	Istanbul
světový	světový	k2eAgInSc4d1	světový
rekord	rekord	k1gInSc4	rekord
v	v	k7c6	v
sedmiboji	sedmiboj	k1gInSc6	sedmiboj
překonal	překonat	k5eAaPmAgMnS	překonat
již	již	k6eAd1	již
potřetí	potřetí	k4xO	potřetí
dosud	dosud	k6eAd1	dosud
platným	platný	k2eAgInSc7d1	platný
výkonem	výkon	k1gInSc7	výkon
6645	[number]	k4	6645
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
29	[number]	k4	29
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2015	[number]	k4	2015
překonal	překonat	k5eAaPmAgMnS	překonat
v	v	k7c6	v
Pekingu	Peking	k1gInSc6	Peking
podruhé	podruhé	k6eAd1	podruhé
vlastní	vlastní	k2eAgInSc4d1	vlastní
světový	světový	k2eAgInSc4d1	světový
rekord	rekord	k1gInSc4	rekord
v	v	k7c6	v
desetiboji	desetiboj	k1gInSc6	desetiboj
výkonem	výkon	k1gInSc7	výkon
9045	[number]	k4	9045
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
prvního	první	k4xOgInSc2	první
světového	světový	k2eAgInSc2d1	světový
rekordu	rekord	k1gInSc2	rekord
začal	začít	k5eAaPmAgInS	začít
velmi	velmi	k6eAd1	velmi
rychlým	rychlý	k2eAgInSc7d1	rychlý
během	běh	k1gInSc7	běh
na	na	k7c4	na
60	[number]	k4	60
metrů	metr	k1gInPc2	metr
za	za	k7c4	za
6,71	[number]	k4	6,71
s.	s.	k?	s.
Po	po	k7c6	po
kvalitní	kvalitní	k2eAgFnSc6d1	kvalitní
dálce	dálka	k1gFnSc6	dálka
se	s	k7c7	s
773	[number]	k4	773
cm	cm	kA	cm
předvedl	předvést	k5eAaPmAgInS	předvést
špatnou	špatný	k2eAgFnSc4d1	špatná
kouli	koule	k1gFnSc4	koule
(	(	kIx(	(
<g/>
13,12	[number]	k4	13,12
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
první	první	k4xOgInSc1	první
den	den	k1gInSc1	den
však	však	k9	však
zakončil	zakončit	k5eAaPmAgInS	zakončit
velmi	velmi	k6eAd1	velmi
dobrou	dobrý	k2eAgFnSc7d1	dobrá
výškou	výška	k1gFnSc7	výška
211	[number]	k4	211
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc4	druhý
den	den	k1gInSc4	den
zaběhl	zaběhnout	k5eAaPmAgMnS	zaběhnout
rychle	rychle	k6eAd1	rychle
60	[number]	k4	60
m.	m.	k?	m.
př	př	kA	př
<g/>
.	.	kIx.	.
za	za	k7c4	za
7,77	[number]	k4	7,77
s	s	k7c7	s
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
skočil	skočit	k5eAaPmAgMnS	skočit
o	o	k7c6	o
tyči	tyč	k1gFnSc6	tyč
kvalitních	kvalitní	k2eAgInPc2d1	kvalitní
510	[number]	k4	510
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
však	však	k9	však
na	na	k7c4	na
výkon	výkon	k1gInSc4	výkon
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Briena	Brien	k1gMnSc4	Brien
silně	silně	k6eAd1	silně
ztrácel	ztrácet	k5eAaImAgMnS	ztrácet
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
světovém	světový	k2eAgInSc6d1	světový
rekordu	rekord	k1gInSc6	rekord
tak	tak	k6eAd1	tak
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
až	až	k9	až
závěrečný	závěrečný	k2eAgInSc1d1	závěrečný
běh	běh	k1gInSc1	běh
na	na	k7c4	na
1000	[number]	k4	1000
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Eaton	Eaton	k1gMnSc1	Eaton
zaběhl	zaběhnout	k5eAaPmAgMnS	zaběhnout
skvělých	skvělý	k2eAgNnPc2d1	skvělé
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
32,67	[number]	k4	32,67
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
Bývalý	bývalý	k2eAgInSc1d1	bývalý
světový	světový	k2eAgInSc1d1	světový
rekord	rekord	k1gInSc1	rekord
tak	tak	k6eAd1	tak
ve	v	k7c6	v
dnech	den	k1gInPc6	den
12	[number]	k4	12
<g/>
.	.	kIx.	.
a	a	k8xC	a
13	[number]	k4	13
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2010	[number]	k4	2010
překonal	překonat	k5eAaPmAgInS	překonat
o	o	k7c4	o
23	[number]	k4	23
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Tallinnu	Tallinno	k1gNnSc6	Tallinno
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
-	-	kIx~	-
6	[number]	k4	6
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
začal	začít	k5eAaPmAgInS	začít
šedesátkou	šedesátka	k1gFnSc7	šedesátka
v	v	k7c6	v
osobním	osobní	k2eAgInSc6d1	osobní
rekordu	rekord	k1gInSc6	rekord
za	za	k7c4	za
6,66	[number]	k4	6,66
s.	s.	k?	s.
<g/>
,	,	kIx,	,
do	do	k7c2	do
dálky	dálka	k1gFnSc2	dálka
skočil	skočit	k5eAaPmAgMnS	skočit
777	[number]	k4	777
cm	cm	kA	cm
<g/>
,	,	kIx,	,
v	v	k7c6	v
kouli	koule	k1gFnSc6	koule
přidal	přidat	k5eAaPmAgMnS	přidat
další	další	k2eAgInSc4d1	další
osobní	osobní	k2eAgInSc4d1	osobní
rekord	rekord	k1gInSc4	rekord
14,45	[number]	k4	14,45
m.	m.	k?	m.
a	a	k8xC	a
první	první	k4xOgInSc1	první
den	den	k1gInSc1	den
zakončil	zakončit	k5eAaPmAgInS	zakončit
výškou	výška	k1gFnSc7	výška
s	s	k7c7	s
201	[number]	k4	201
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc4	druhý
den	den	k1gInSc4	den
zaběhl	zaběhnout	k5eAaPmAgMnS	zaběhnout
famózních	famózní	k2eAgFnPc2d1	famózní
7,60	[number]	k4	7,60
s.	s.	k?	s.
na	na	k7c4	na
60	[number]	k4	60
m.	m.	k?	m.
překážek	překážka	k1gFnPc2	překážka
(	(	kIx(	(
<g/>
vícebojařský	vícebojařský	k2eAgMnSc1d1	vícebojařský
SR	SR	kA	SR
<g/>
)	)	kIx)	)
a	a	k8xC	a
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
skokem	skokem	k6eAd1	skokem
o	o	k7c6	o
tyči	tyč	k1gFnSc6	tyč
s	s	k7c7	s
520	[number]	k4	520
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Závěrečným	závěrečný	k2eAgInSc7d1	závěrečný
během	běh	k1gInSc7	běh
na	na	k7c4	na
1000	[number]	k4	1000
metrů	metr	k1gInPc2	metr
v	v	k7c6	v
čase	čas	k1gInSc6	čas
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
34,74	[number]	k4	34,74
minuty	minuta	k1gFnSc2	minuta
posbíral	posbírat	k5eAaPmAgMnS	posbírat
rekordních	rekordní	k2eAgNnPc2d1	rekordní
6	[number]	k4	6
568	[number]	k4	568
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Istanbulu	Istanbul	k1gInSc6	Istanbul
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
měl	mít	k5eAaImAgInS	mít
pomalejší	pomalý	k2eAgFnSc4d2	pomalejší
startovní	startovní	k2eAgFnSc4d1	startovní
reakci	reakce	k1gFnSc4	reakce
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
na	na	k7c4	na
své	svůj	k3xOyFgInPc4	svůj
poměry	poměr	k1gInPc4	poměr
slabším	slabý	k2eAgInSc7d2	slabší
časem	čas	k1gInSc7	čas
6,79	[number]	k4	6,79
s.	s.	k?	s.
Vše	všechen	k3xTgNnSc1	všechen
ale	ale	k9	ale
dohnal	dohnat	k5eAaPmAgInS	dohnat
v	v	k7c6	v
dálce	dálka	k1gFnSc6	dálka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dolétl	dolétnout	k5eAaPmAgInS	dolétnout
až	až	k9	až
na	na	k7c4	na
hodnotu	hodnota	k1gFnSc4	hodnota
vícebojařského	vícebojařský	k2eAgInSc2d1	vícebojařský
SR	SR	kA	SR
816	[number]	k4	816
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kouli	koule	k1gFnSc6	koule
si	se	k3xPyFc3	se
opět	opět	k6eAd1	opět
vylepšil	vylepšit	k5eAaPmAgMnS	vylepšit
osobní	osobní	k2eAgInSc4d1	osobní
rekord	rekord	k1gInSc4	rekord
na	na	k7c6	na
14,56	[number]	k4	14,56
metru	metro	k1gNnSc6	metro
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
skočil	skočit	k5eAaPmAgMnS	skočit
kvalitních	kvalitní	k2eAgFnPc2d1	kvalitní
203	[number]	k4	203
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc4	druhý
den	den	k1gInSc4	den
zaběhl	zaběhnout	k5eAaPmAgMnS	zaběhnout
překážky	překážka	k1gFnPc4	překážka
za	za	k7c4	za
skvělých	skvělý	k2eAgInPc2d1	skvělý
7,68	[number]	k4	7,68
s.	s.	k?	s.
a	a	k8xC	a
v	v	k7c6	v
tyči	tyč	k1gFnSc6	tyč
dorovnal	dorovnat	k5eAaPmAgMnS	dorovnat
výkon	výkon	k1gInSc4	výkon
z	z	k7c2	z
Tallinu	Tallin	k1gInSc2	Tallin
(	(	kIx(	(
<g/>
520	[number]	k4	520
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rychlým	rychlý	k2eAgInSc7d1	rychlý
časem	čas	k1gInSc7	čas
na	na	k7c4	na
1000	[number]	k4	1000
metrů	metr	k1gInPc2	metr
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
32,78	[number]	k4	32,78
minuty	minuta	k1gFnSc2	minuta
pak	pak	k6eAd1	pak
nasbíral	nasbírat	k5eAaPmAgMnS	nasbírat
součet	součet	k1gInSc4	součet
nového	nový	k2eAgNnSc2d1	nové
světového	světový	k2eAgNnSc2d1	světové
maxima	maximum	k1gNnSc2	maximum
6645	[number]	k4	6645
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
v	v	k7c6	v
polských	polský	k2eAgInPc6d1	polský
Sopotech	sopot	k1gInPc6	sopot
získal	získat	k5eAaPmAgMnS	získat
6632	[number]	k4	6632
bodů	bod	k1gInPc2	bod
a	a	k8xC	a
obhájil	obhájit	k5eAaPmAgMnS	obhájit
titul	titul	k1gInSc4	titul
druhým	druhý	k4xOgInSc7	druhý
nejlepším	dobrý	k2eAgInSc7d3	nejlepší
výkonem	výkon	k1gInSc7	výkon
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
pak	pak	k6eAd1	pak
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
Portlandu	Portland	k1gInSc6	Portland
na	na	k7c6	na
domácí	domácí	k2eAgFnSc6d1	domácí
půdě	půda	k1gFnSc6	půda
opět	opět	k6eAd1	opět
s	s	k7c7	s
přehledem	přehled	k1gInSc7	přehled
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
šestým	šestý	k4xOgInSc7	šestý
nejlepším	dobrý	k2eAgInSc7d3	nejlepší
součtem	součet	k1gInSc7	součet
historie	historie	k1gFnSc2	historie
6470	[number]	k4	6470
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
Eaton	Eaton	k1gMnSc1	Eaton
překonal	překonat	k5eAaPmAgMnS	překonat
po	po	k7c6	po
11	[number]	k4	11
letech	let	k1gInPc6	let
světový	světový	k2eAgInSc4d1	světový
rekord	rekord	k1gInSc4	rekord
Romana	Roman	k1gMnSc2	Roman
Šebrleho	Šebrle	k1gMnSc2	Šebrle
9026	[number]	k4	9026
bodů	bod	k1gInPc2	bod
o	o	k7c4	o
13	[number]	k4	13
bodů	bod	k1gInPc2	bod
při	při	k7c6	při
americké	americký	k2eAgFnSc6d1	americká
olympijské	olympijský	k2eAgFnSc6d1	olympijská
kvalifikaci	kvalifikace	k1gFnSc6	kvalifikace
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
výkon	výkon	k1gInSc1	výkon
dále	daleko	k6eAd2	daleko
vylepšil	vylepšit	k5eAaPmAgInS	vylepšit
na	na	k7c6	na
atletickém	atletický	k2eAgNnSc6d1	atletické
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
Pekingu	Peking	k1gInSc6	Peking
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
součtem	součet	k1gInSc7	součet
9045	[number]	k4	9045
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
dosud	dosud	k6eAd1	dosud
platným	platný	k2eAgInSc7d1	platný
světovým	světový	k2eAgInSc7d1	světový
rekordem	rekord	k1gInSc7	rekord
(	(	kIx(	(
<g/>
a	a	k8xC	a
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
třetím	třetí	k4xOgInSc7	třetí
desetibojem	desetiboj	k1gInSc7	desetiboj
s	s	k7c7	s
hodnotou	hodnota	k1gFnSc7	hodnota
nad	nad	k7c4	nad
9000	[number]	k4	9000
bodů	bod	k1gInPc2	bod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Eaton	Eaton	k1gInSc1	Eaton
zaujal	zaujmout	k5eAaPmAgInS	zaujmout
velmi	velmi	k6eAd1	velmi
rychlou	rychlý	k2eAgFnSc7d1	rychlá
úvodní	úvodní	k2eAgFnSc7d1	úvodní
stovkou	stovka	k1gFnSc7	stovka
(	(	kIx(	(
<g/>
10,23	[number]	k4	10,23
s.	s.	k?	s.
<g/>
)	)	kIx)	)
a	a	k8xC	a
zejména	zejména	k9	zejména
pak	pak	k6eAd1	pak
desetibojařským	desetibojařský	k2eAgInSc7d1	desetibojařský
světovým	světový	k2eAgInSc7d1	světový
rekordem	rekord	k1gInSc7	rekord
v	v	k7c6	v
běhu	běh	k1gInSc6	běh
na	na	k7c4	na
400	[number]	k4	400
metrů	metr	k1gInPc2	metr
v	v	k7c6	v
čase	čas	k1gInSc6	čas
rovných	rovný	k2eAgInPc2d1	rovný
45	[number]	k4	45
sekund	sekunda	k1gFnPc2	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Rekord	rekord	k1gInSc1	rekord
vybojoval	vybojovat	k5eAaPmAgInS	vybojovat
i	i	k9	i
díky	díky	k7c3	díky
velkému	velký	k2eAgNnSc3d1	velké
úsilí	úsilí	k1gNnSc3	úsilí
v	v	k7c6	v
závěrečné	závěrečný	k2eAgFnSc6d1	závěrečná
patnáctistovce	patnáctistovka	k1gFnSc6	patnáctistovka
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
dobrém	dobrý	k2eAgInSc6d1	dobrý
čase	čas	k1gInSc6	čas
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
17	[number]	k4	17
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
60	[number]	k4	60
m	m	kA	m
6,66	[number]	k4	6,66
s	s	k7c7	s
100	[number]	k4	100
m	m	kA	m
10,19	[number]	k4	10,19
s	s	k7c7	s
(	(	kIx(	(
<g/>
vícebojařský	vícebojařský	k2eAgMnSc1d1	vícebojařský
SR	SR	kA	SR
<g/>
,	,	kIx,	,
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
desetiboji	desetiboj	k1gInSc6	desetiboj
10,21	[number]	k4	10,21
s.	s.	k?	s.
<g/>
)	)	kIx)	)
Dálka	dálka	k1gFnSc1	dálka
v	v	k7c6	v
hale	hala	k1gFnSc6	hala
816	[number]	k4	816
<g />
.	.	kIx.	.
</s>
<s>
cm	cm	kA	cm
(	(	kIx(	(
<g/>
vícebojařský	vícebojařský	k2eAgMnSc1d1	vícebojařský
SR	SR	kA	SR
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
venku	venku	k6eAd1	venku
823	[number]	k4	823
cm	cm	kA	cm
(	(	kIx(	(
<g/>
vícebojařský	vícebojařský	k2eAgMnSc1d1	vícebojařský
SR	SR	kA	SR
<g/>
)	)	kIx)	)
Koule	koule	k1gFnSc1	koule
15,40	[number]	k4	15,40
m	m	kA	m
Výška	výška	k1gFnSc1	výška
210	[number]	k4	210
cm	cm	kA	cm
(	(	kIx(	(
<g/>
hala	hala	k1gFnSc1	hala
211	[number]	k4	211
cm	cm	kA	cm
<g/>
)	)	kIx)	)
400	[number]	k4	400
m	m	kA	m
45,00	[number]	k4	45,00
s	s	k7c7	s
(	(	kIx(	(
<g/>
vícebojařský	vícebojařský	k2eAgMnSc1d1	vícebojařský
SR	SR	kA	SR
<g/>
)	)	kIx)	)
60	[number]	k4	60
m	m	kA	m
př	př	kA	př
<g/>
.	.	kIx.	.
7,51	[number]	k4	7,51
s	s	k7c7	s
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
vícebojařský	vícebojařský	k2eAgMnSc1d1	vícebojařský
SR	SR	kA	SR
<g/>
)	)	kIx)	)
110	[number]	k4	110
m	m	kA	m
př	př	kA	př
<g/>
.	.	kIx.	.
13,34	[number]	k4	13,34
s	s	k7c7	s
Disk	disk	k1gInSc1	disk
47,36	[number]	k4	47,36
m	m	kA	m
Tyč	tyč	k1gFnSc1	tyč
540	[number]	k4	540
cm	cm	kA	cm
(	(	kIx(	(
<g/>
hala	hala	k1gFnSc1	hala
540	[number]	k4	540
cm	cm	kA	cm
<g/>
)	)	kIx)	)
Oštěp	oštěp	k1gInSc1	oštěp
66,64	[number]	k4	66,64
m	m	kA	m
1000	[number]	k4	1000
m	m	kA	m
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
32,67	[number]	k4	32,67
min	mina	k1gFnPc2	mina
1500	[number]	k4	1500
m	m	kA	m
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
14,48	[number]	k4	14,48
min	min	kA	min
Desetiboj	desetiboj	k1gInSc4	desetiboj
9	[number]	k4	9
045	[number]	k4	045
bodů	bod	k1gInPc2	bod
(	(	kIx(	(
<g/>
SR	SR	kA	SR
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
celkový	celkový	k2eAgInSc1d1	celkový
součet	součet	k1gInSc1	součet
OR	OR	kA	OR
9549	[number]	k4	9549
bodů	bod	k1gInPc2	bod
Sedmiboj	sedmiboj	k1gInSc1	sedmiboj
6	[number]	k4	6
645	[number]	k4	645
bodů	bod	k1gInPc2	bod
(	(	kIx(	(
<g/>
SR	SR	kA	SR
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
</s>
