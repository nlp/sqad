<p>
<s>
Aplikační	aplikační	k2eAgFnSc1d1	aplikační
vrstva	vrstva	k1gFnSc1	vrstva
je	být	k5eAaImIp3nS	být
7	[number]	k4	7
<g/>
.	.	kIx.	.
vrstva	vrstva	k1gFnSc1	vrstva
modelu	model	k1gInSc2	model
vrstvové	vrstvový	k2eAgFnSc2d1	vrstvová
síťové	síťový	k2eAgFnSc2d1	síťová
architektury	architektura	k1gFnSc2	architektura
(	(	kIx(	(
<g/>
OSI	OSI	kA	OSI
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
originále	originál	k1gInSc6	originál
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
application	application	k1gInSc1	application
layer	layra	k1gFnPc2	layra
<g/>
.	.	kIx.	.
</s>
<s>
Účelem	účel	k1gInSc7	účel
vrstvy	vrstva	k1gFnSc2	vrstva
je	být	k5eAaImIp3nS	být
poskytnout	poskytnout	k5eAaPmF	poskytnout
aplikacím	aplikace	k1gFnPc3	aplikace
přístup	přístup	k1gInSc4	přístup
ke	k	k7c3	k
komunikačnímu	komunikační	k2eAgInSc3d1	komunikační
systému	systém	k1gInSc3	systém
a	a	k8xC	a
umožnit	umožnit	k5eAaPmF	umožnit
tak	tak	k6eAd1	tak
jejich	jejich	k3xOp3gFnSc4	jejich
spolupráci	spolupráce	k1gFnSc4	spolupráce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příklady	příklad	k1gInPc1	příklad
==	==	k?	==
</s>
</p>
<p>
<s>
AIM	AIM	kA	AIM
(	(	kIx(	(
<g/>
AOL	AOL	kA	AOL
Instant	Instant	k1gInSc1	Instant
Messenger	Messenger	k1gInSc1	Messenger
Protocol	Protocol	k1gInSc4	Protocol
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
APPC	APPC	kA	APPC
</s>
</p>
<p>
<s>
AFP	AFP	kA	AFP
(	(	kIx(	(
<g/>
Appletalk	Appletalk	k1gInSc1	Appletalk
Filing	Filing	k1gInSc1	Filing
Protocol	Protocol	k1gInSc4	Protocol
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
BitTorrent	BitTorrent	k1gMnSc1	BitTorrent
</s>
</p>
<p>
<s>
CFDP	CFDP	kA	CFDP
(	(	kIx(	(
<g/>
Coherent	Coherent	k1gMnSc1	Coherent
File	Fil	k1gFnSc2	Fil
Distribution	Distribution	k1gInSc1	Distribution
Protocol	Protocol	k1gInSc1	Protocol
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
DHCP	DHCP	kA	DHCP
</s>
</p>
<p>
<s>
FTAM	FTAM	kA	FTAM
</s>
</p>
<p>
<s>
FTP	FTP	kA	FTP
(	(	kIx(	(
<g/>
File	Fil	k1gInSc2	Fil
Transfer	transfer	k1gInSc1	transfer
Protocol	Protocol	k1gInSc1	Protocol
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
gopher	gophra	k1gFnPc2	gophra
</s>
</p>
<p>
<s>
HTTP	HTTP	kA	HTTP
(	(	kIx(	(
<g/>
Hyper	hyper	k2eAgInSc1d1	hyper
Text	text	k1gInSc1	text
Transfer	transfer	k1gInSc1	transfer
Protocol	Protocol	k1gInSc4	Protocol
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
IMAP	IMAP	kA	IMAP
(	(	kIx(	(
<g/>
Internet	Internet	k1gInSc1	Internet
Message	Message	k1gNnSc1	Message
Access	Accessa	k1gFnPc2	Accessa
Protocol	Protocola	k1gFnPc2	Protocola
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ITMS	ITMS	kA	ITMS
(	(	kIx(	(
<g/>
iTunes	iTunes	k1gMnSc1	iTunes
Music	Music	k1gMnSc1	Music
Store	Stor	k1gInSc5	Stor
Protocol	Protocol	k1gInSc1	Protocol
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
IRC	IRC	kA	IRC
(	(	kIx(	(
<g/>
Internet	Internet	k1gInSc1	Internet
Relay	Relaa	k1gFnSc2	Relaa
Chat	chata	k1gFnPc2	chata
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
SILC	SILC	kA	SILC
(	(	kIx(	(
<g/>
Secure	Secur	k1gMnSc5	Secur
Internet	Internet	k1gInSc1	Internet
Live	Live	k1gNnSc6	Live
Conferencing	Conferencing	k1gInSc1	Conferencing
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
LDAP	LDAP	kA	LDAP
(	(	kIx(	(
<g/>
Lightweight	Lightweight	k1gMnSc1	Lightweight
Directory	Director	k1gInPc4	Director
Access	Accessa	k1gFnPc2	Accessa
Protocol	Protocola	k1gFnPc2	Protocola
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Modbus	Modbus	k1gMnSc1	Modbus
</s>
</p>
<p>
<s>
NNTP	NNTP	kA	NNTP
(	(	kIx(	(
<g/>
Network	network	k1gInSc1	network
News	News	k1gInSc1	News
Transfer	transfer	k1gInSc1	transfer
Protocol	Protocol	k1gInSc4	Protocol
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
NTLM	NTLM	kA	NTLM
(	(	kIx(	(
<g/>
NT	NT	kA	NT
LAN	lano	k1gNnPc2	lano
Manager	manager	k1gMnSc1	manager
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
POP3	POP3	k4	POP3
</s>
</p>
<p>
<s>
SSH	SSH	kA	SSH
(	(	kIx(	(
<g/>
Secure	Secur	k1gMnSc5	Secur
Shell	Shell	k1gMnSc1	Shell
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
SIP	SIP	kA	SIP
(	(	kIx(	(
<g/>
Session	Session	k1gInSc1	Session
Initiation	Initiation	k1gInSc1	Initiation
Protocol	Protocol	k1gInSc4	Protocol
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
SMB	SMB	kA	SMB
(	(	kIx(	(
<g/>
Server	server	k1gInSc1	server
Message	Message	k1gNnSc1	Message
Block	Block	k1gInSc1	Block
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
SMTP	SMTP	kA	SMTP
(	(	kIx(	(
<g/>
Simple	Simple	k1gFnSc1	Simple
Mail	mail	k1gInSc1	mail
Transfer	transfer	k1gInSc1	transfer
Protocol	Protocol	k1gInSc4	Protocol
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
SNMP	SNMP	kA	SNMP
(	(	kIx(	(
<g/>
Simple	Simple	k1gFnSc1	Simple
network	network	k1gInSc4	network
management	management	k1gInSc1	management
protocol	protocol	k1gInSc1	protocol
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
TFTP	TFTP	kA	TFTP
(	(	kIx(	(
<g/>
Trivial	Trivial	k1gMnSc1	Trivial
File	Fil	k1gFnSc2	Fil
Transfer	transfer	k1gInSc1	transfer
Protocol	Protocol	k1gInSc1	Protocol
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
TSP	TSP	kA	TSP
(	(	kIx(	(
<g/>
Time	Time	k1gFnSc1	Time
Stamp	Stamp	k1gMnSc1	Stamp
Protocol	Protocol	k1gInSc1	Protocol
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Telnet	Telnet	k1gMnSc1	Telnet
</s>
</p>
<p>
<s>
X	X	kA	X
<g/>
.400	.400	k4	.400
</s>
</p>
<p>
<s>
X	X	kA	X
<g/>
.500	.500	k4	.500
</s>
</p>
<p>
<s>
XMPP	XMPP	kA	XMPP
(	(	kIx(	(
<g/>
Extensible	Extensible	k1gMnSc2	Extensible
Messaging	Messaging	k1gInSc1	Messaging
and	and	k?	and
Presence	presence	k1gFnSc2	presence
Protocol	Protocola	k1gFnPc2	Protocola
<g/>
)	)	kIx)	)
</s>
</p>
