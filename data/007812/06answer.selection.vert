<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byly	být	k5eAaImAgFnP	být
známy	znám	k2eAgInPc1d1	znám
tři	tři	k4xCgFnPc1	tři
lokality	lokalita	k1gFnPc1	lokalita
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
kuřička	kuřička	k1gFnSc1	kuřička
hadcová	hadcový	k2eAgFnSc1d1	hadcová
vyskytovala	vyskytovat	k5eAaImAgFnS	vyskytovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
Borku	borek	k1gInSc6	borek
u	u	k7c2	u
Chotěboře	Chotěboř	k1gFnSc2	Chotěboř
vyhynula	vyhynout	k5eAaPmAgFnS	vyhynout
už	už	k6eAd1	už
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
u	u	k7c2	u
Kamberka	Kamberka	k1gFnSc1	Kamberka
na	na	k7c6	na
Hadcích	hadec	k1gInPc6	hadec
u	u	k7c2	u
obce	obec	k1gFnSc2	obec
Hrnčíře	Hrnčíř	k1gMnSc2	Hrnčíř
se	se	k3xPyFc4	se
zachovala	zachovat	k5eAaPmAgNnP	zachovat
pouze	pouze	k6eAd1	pouze
málo	málo	k6eAd1	málo
početná	početný	k2eAgFnSc1d1	početná
populace	populace	k1gFnSc1	populace
<g/>
.	.	kIx.	.
</s>
