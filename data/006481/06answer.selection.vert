<s>
Sókratés	Sókratés	k6eAd1	Sókratés
sám	sám	k3xTgMnSc1	sám
nezanechal	zanechat	k5eNaPmAgMnS	zanechat
žádné	žádný	k3yNgInPc4	žádný
filosofické	filosofický	k2eAgInPc4d1	filosofický
spisy	spis	k1gInPc4	spis
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
podle	podle	k7c2	podle
Platónova	Platónův	k2eAgInSc2d1	Platónův
dialogu	dialog	k1gInSc2	dialog
Faidón	Faidón	k1gMnSc1	Faidón
měl	mít	k5eAaImAgMnS	mít
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
smrtí	smrt	k1gFnSc7	smrt
napsat	napsat	k5eAaPmF	napsat
báseň	báseň	k1gFnSc4	báseň
a	a	k8xC	a
zveršovat	zveršovat	k5eAaPmF	zveršovat
Aisópovy	Aisópův	k2eAgFnPc4d1	Aisópův
bajky	bajka	k1gFnPc4	bajka
<g/>
.	.	kIx.	.
</s>
