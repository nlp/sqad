<s>
Sókratés	Sókratés	k1gInSc1	Sókratés
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
469	[number]	k4	469
v	v	k7c6	v
Athénách	Athéna	k1gFnPc6	Athéna
–	–	k?	–
399	[number]	k4	399
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
v	v	k7c6	v
Athénách	Athéna	k1gFnPc6	Athéna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
řecky	řecky	k6eAd1	řecky
Σ	Σ	k?	Σ
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
diakritiky	diakritika	k1gFnSc2	diakritika
Sokrates	Sokrates	k1gMnSc1	Sokrates
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
athénský	athénský	k2eAgMnSc1d1	athénský
filosof	filosof	k1gMnSc1	filosof
<g/>
,	,	kIx,	,
učitel	učitel	k1gMnSc1	učitel
Platónův	Platónův	k2eAgMnSc1d1	Platónův
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
postav	postava	k1gFnPc2	postava
evropské	evropský	k2eAgFnSc2d1	Evropská
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
svých	svůj	k3xOyFgMnPc2	svůj
předchůdců	předchůdce	k1gMnPc2	předchůdce
předsókratiků	předsókratik	k1gMnPc2	předsókratik
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
pátrali	pátrat	k5eAaImAgMnP	pátrat
po	po	k7c6	po
původu	původ	k1gInSc6	původ
a	a	k8xC	a
příčinách	příčina	k1gFnPc6	příčina
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
Sókratův	Sókratův	k2eAgInSc1d1	Sókratův
zájem	zájem	k1gInSc1	zájem
se	se	k3xPyFc4	se
soustředil	soustředit	k5eAaPmAgInS	soustředit
na	na	k7c6	na
záležitosti	záležitost	k1gFnSc6	záležitost
člověka	člověk	k1gMnSc2	člověk
a	a	k8xC	a
společnosti	společnost	k1gFnSc2	společnost
<g/>
;	;	kIx,	;
jak	jak	k8xS	jak
praví	pravit	k5eAaBmIp3nS	pravit
Cicero	cicero	k1gNnSc1	cicero
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
snesl	snést	k5eAaPmAgMnS	snést
filosofii	filosofie	k1gFnSc4	filosofie
z	z	k7c2	z
nebe	nebe	k1gNnSc2	nebe
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc7	svůj
dialektickou	dialektický	k2eAgFnSc7d1	dialektická
metodou	metoda	k1gFnSc7	metoda
položil	položit	k5eAaPmAgInS	položit
základy	základ	k1gInPc7	základ
západního	západní	k2eAgNnSc2d1	západní
kritického	kritický	k2eAgNnSc2d1	kritické
myšlení	myšlení	k1gNnSc2	myšlení
<g/>
.	.	kIx.	.
</s>
<s>
Sókratés	Sókratés	k6eAd1	Sókratés
sám	sám	k3xTgMnSc1	sám
nezanechal	zanechat	k5eNaPmAgMnS	zanechat
žádné	žádný	k3yNgInPc4	žádný
filosofické	filosofický	k2eAgInPc4d1	filosofický
spisy	spis	k1gInPc4	spis
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
podle	podle	k7c2	podle
Platónova	Platónův	k2eAgInSc2d1	Platónův
dialogu	dialog	k1gInSc2	dialog
Faidón	Faidón	k1gMnSc1	Faidón
měl	mít	k5eAaImAgMnS	mít
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
smrtí	smrt	k1gFnSc7	smrt
napsat	napsat	k5eAaPmF	napsat
báseň	báseň	k1gFnSc4	báseň
a	a	k8xC	a
zveršovat	zveršovat	k5eAaPmF	zveršovat
Aisópovy	Aisópův	k2eAgFnPc4d1	Aisópův
bajky	bajka	k1gFnPc4	bajka
<g/>
.	.	kIx.	.
</s>
<s>
Obával	obávat	k5eAaImAgInS	obávat
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
snad	snad	k9	snad
to	ten	k3xDgNnSc1	ten
po	po	k7c6	po
něm	on	k3xPp3gMnSc6	on
nechtěl	chtít	k5eNaImAgMnS	chtít
bůh	bůh	k1gMnSc1	bůh
<g/>
,	,	kIx,	,
a	a	k8xC	a
nechtěl	chtít	k5eNaImAgMnS	chtít
odejít	odejít	k5eAaPmF	odejít
ze	z	k7c2	z
života	život	k1gInSc2	život
neposlechnuv	poslechnout	k5eNaPmDgInS	poslechnout
boží	boží	k2eAgInSc4d1	boží
přikázání	přikázání	k1gNnSc4	přikázání
<g/>
.	.	kIx.	.
</s>
<s>
Aristofanés	Aristofanés	k1gInSc1	Aristofanés
si	se	k3xPyFc3	se
ze	z	k7c2	z
Sókrata	Sókrat	k1gMnSc2	Sókrat
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
tehdy	tehdy	k6eAd1	tehdy
bylo	být	k5eAaImAgNnS	být
po	po	k7c6	po
čtyřicítce	čtyřicítka	k1gFnSc6	čtyřicítka
<g/>
,	,	kIx,	,
dělá	dělat	k5eAaImIp3nS	dělat
legraci	legrace	k1gFnSc4	legrace
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
komedii	komedie	k1gFnSc6	komedie
Oblaka	oblaka	k1gNnPc4	oblaka
a	a	k8xC	a
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
hrách	hra	k1gFnPc6	hra
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Ptáci	pták	k1gMnPc1	pták
<g/>
,	,	kIx,	,
Kalliás	Kalliás	k1gInSc1	Kalliás
<g/>
,	,	kIx,	,
Eupolis	Eupolis	k1gInSc1	Eupolis
a	a	k8xC	a
Telekleidés	Telekleidés	k1gInSc1	Telekleidés
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
pramenem	pramen	k1gInSc7	pramen
informací	informace	k1gFnPc2	informace
o	o	k7c6	o
Sókratovi	Sókrat	k1gMnSc6	Sókrat
jsou	být	k5eAaImIp3nP	být
spisy	spis	k1gInPc4	spis
dvou	dva	k4xCgMnPc6	dva
jeho	jeho	k3xOp3gMnPc2	jeho
žáků	žák	k1gMnPc2	žák
–	–	k?	–
Xenofónta	Xenofónt	k1gMnSc4	Xenofónt
a	a	k8xC	a
Platóna	Platón	k1gMnSc4	Platón
(	(	kIx(	(
<g/>
Obrana	obrana	k1gFnSc1	obrana
Sókratova	Sókratův	k2eAgFnSc1d1	Sókratova
<g/>
,	,	kIx,	,
Kritón	Kritón	k1gInSc1	Kritón
<g/>
,	,	kIx,	,
Faidón	Faidón	k1gInSc1	Faidón
<g/>
,	,	kIx,	,
Symposion	symposion	k1gNnSc1	symposion
<g/>
,	,	kIx,	,
Theaitétos	Theaitétos	k1gInSc1	Theaitétos
<g/>
,	,	kIx,	,
Parmenidés	Parmenidés	k1gInSc1	Parmenidés
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
informace	informace	k1gFnPc1	informace
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaPmF	nalézt
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
Platónova	Platónův	k2eAgMnSc2d1	Platónův
žáka	žák	k1gMnSc2	žák
Aristotela	Aristoteles	k1gMnSc2	Aristoteles
<g/>
.	.	kIx.	.
</s>
<s>
Sochy	socha	k1gFnPc1	socha
a	a	k8xC	a
busty	busta	k1gFnPc1	busta
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
Sókrata	Sókrat	k1gMnSc4	Sókrat
znázorňují	znázorňovat	k5eAaImIp3nP	znázorňovat
jako	jako	k8xS	jako
šeredného	šeredný	k2eAgMnSc4d1	šeredný
muže	muž	k1gMnSc4	muž
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
inspirovaly	inspirovat	k5eAaBmAgInP	inspirovat
Platónovým	Platónův	k2eAgInSc7d1	Platónův
a	a	k8xC	a
Xenofónovým	Xenofónový	k2eAgInSc7d1	Xenofónový
popisem	popis	k1gInSc7	popis
<g/>
,	,	kIx,	,
přirovnávajícím	přirovnávající	k2eAgInSc7d1	přirovnávající
jej	on	k3xPp3gMnSc4	on
v	v	k7c6	v
žertu	žert	k1gInSc6	žert
k	k	k7c3	k
silénům	silén	k1gMnPc3	silén
a	a	k8xC	a
satyrům	satyr	k1gMnPc3	satyr
(	(	kIx(	(
<g/>
Platón	platón	k1gInSc1	platón
–	–	k?	–
Symposion	symposion	k1gNnSc1	symposion
<g/>
,	,	kIx,	,
Theaitétos	Theaitétos	k1gInSc1	Theaitétos
<g/>
;	;	kIx,	;
Xenofón	Xenofón	k1gInSc1	Xenofón
–	–	k?	–
Symposion	symposion	k1gNnSc1	symposion
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Otcem	otec	k1gMnSc7	otec
Sókratovým	Sókratův	k2eAgFnPc3d1	Sókratova
byl	být	k5eAaImAgInS	být
sochař	sochař	k1gMnSc1	sochař
Sofróniskos	Sofróniskosa	k1gFnPc2	Sofróniskosa
a	a	k8xC	a
matkou	matka	k1gFnSc7	matka
porodní	porodní	k2eAgFnSc1d1	porodní
bába	bába	k1gFnSc1	bába
Fainareté	Fainaretý	k2eAgFnSc2d1	Fainaretý
<g/>
,	,	kIx,	,
pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
dému	démos	k1gInSc2	démos
Antiochidova	Antiochidův	k2eAgMnSc2d1	Antiochidův
a	a	k8xC	a
narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
roce	rok	k1gInSc6	rok
77	[number]	k4	77
<g/>
.	.	kIx.	.
</s>
<s>
Olympiády	olympiáda	k1gFnPc1	olympiáda
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
jeho	jeho	k3xOp3gNnSc6	jeho
mládí	mládí	k1gNnSc6	mládí
nevíme	vědět	k5eNaImIp1nP	vědět
mnoho	mnoho	k6eAd1	mnoho
<g/>
,	,	kIx,	,
zřejmě	zřejmě	k6eAd1	zřejmě
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
ale	ale	k9	ale
dostalo	dostat	k5eAaPmAgNnS	dostat
vzdělání	vzdělání	k1gNnSc1	vzdělání
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
mu	on	k3xPp3gMnSc3	on
otec	otec	k1gMnSc1	otec
podle	podle	k7c2	podle
athénských	athénský	k2eAgInPc2d1	athénský
zákonů	zákon	k1gInPc2	zákon
musel	muset	k5eAaImAgMnS	muset
poskytnout	poskytnout	k5eAaPmF	poskytnout
–	–	k?	–
gymnastika	gymnastika	k1gFnSc1	gymnastika
<g/>
,	,	kIx,	,
hudba	hudba	k1gFnSc1	hudba
<g/>
,	,	kIx,	,
gramatika	gramatika	k1gFnSc1	gramatika
(	(	kIx(	(
<g/>
zahrnovala	zahrnovat	k5eAaImAgFnS	zahrnovat
četbu	četba	k1gFnSc4	četba
Homéra	Homér	k1gMnSc2	Homér
<g/>
,	,	kIx,	,
Hésioda	Hésiod	k1gMnSc2	Hésiod
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
básníků	básník	k1gMnPc2	básník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oženil	oženit	k5eAaPmAgMnS	oženit
se	se	k3xPyFc4	se
s	s	k7c7	s
Xantippou	Xantippa	k1gFnSc7	Xantippa
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
měl	mít	k5eAaImAgMnS	mít
tři	tři	k4xCgMnPc4	tři
syny	syn	k1gMnPc4	syn
<g/>
.	.	kIx.	.
</s>
<s>
Sókratés	Sókratés	k6eAd1	Sókratés
sám	sám	k3xTgMnSc1	sám
se	se	k3xPyFc4	se
zmiňoval	zmiňovat	k5eAaImAgMnS	zmiňovat
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
se	se	k3xPyFc4	se
naučil	naučit	k5eAaPmAgMnS	naučit
žít	žít	k5eAaImF	žít
s	s	k7c7	s
Xantippou	Xantippa	k1gFnSc7	Xantippa
<g/>
,	,	kIx,	,
dovedl	dovést	k5eAaPmAgMnS	dovést
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
vypořádat	vypořádat	k5eAaPmF	vypořádat
s	s	k7c7	s
kterýmkoli	kterýkoli	k3yIgMnSc7	kterýkoli
jiným	jiný	k2eAgMnSc7d1	jiný
člověkem	člověk	k1gMnSc7	člověk
právě	právě	k9	právě
tak	tak	k9	tak
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
krotitel	krotitel	k1gMnSc1	krotitel
koní	kůň	k1gMnPc2	kůň
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
zkrocení	zkrocení	k1gNnSc6	zkrocení
opravdu	opravdu	k6eAd1	opravdu
divokých	divoký	k2eAgNnPc2d1	divoké
zvířat	zvíře	k1gNnPc2	zvíře
schopen	schopen	k2eAgInSc1d1	schopen
jednoduše	jednoduše	k6eAd1	jednoduše
nakládat	nakládat	k5eAaImF	nakládat
se	s	k7c7	s
zvířaty	zvíře	k1gNnPc7	zvíře
klidnými	klidný	k2eAgNnPc7d1	klidné
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
Sókratovy	Sókratův	k2eAgInPc4d1	Sókratův
zábavy	zábav	k1gInPc4	zábav
patřily	patřit	k5eAaImAgFnP	patřit
návštěvy	návštěva	k1gFnPc1	návštěva
symposií	symposion	k1gNnPc2	symposion
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
pitek	pitka	k1gFnPc2	pitka
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc2	jenž
nezůstával	zůstávat	k5eNaImAgMnS	zůstávat
pozadu	pozadu	k6eAd1	pozadu
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
první	první	k4xOgFnSc2	první
fáze	fáze	k1gFnSc2	fáze
Peloponéské	peloponéský	k2eAgFnSc2d1	Peloponéská
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgInS	účastnit
bitvy	bitva	k1gFnSc2	bitva
u	u	k7c2	u
Potidají	Potidají	k1gFnSc2	Potidají
<g/>
,	,	kIx,	,
bitvy	bitva	k1gFnSc2	bitva
u	u	k7c2	u
Délia	Délium	k1gNnSc2	Délium
a	a	k8xC	a
bitvy	bitva	k1gFnSc2	bitva
u	u	k7c2	u
Amfipole	Amfipole	k1gFnSc2	Amfipole
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Platónova	Platónův	k2eAgNnSc2d1	Platónovo
Symposia	symposion	k1gNnSc2	symposion
se	se	k3xPyFc4	se
dovídáme	dovídat	k5eAaImIp1nP	dovídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
měl	mít	k5eAaImAgInS	mít
blízko	blízko	k6eAd1	blízko
k	k	k7c3	k
vyznamenání	vyznamenání	k1gNnSc3	vyznamenání
za	za	k7c4	za
statečnost	statečnost	k1gFnSc4	statečnost
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
nakonec	nakonec	k6eAd1	nakonec
dostal	dostat	k5eAaPmAgInS	dostat
mladý	mladý	k2eAgInSc1d1	mladý
Alkibiadés	Alkibiadés	k1gInSc1	Alkibiadés
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
tehdy	tehdy	k6eAd1	tehdy
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
poraněného	poraněný	k2eAgInSc2d1	poraněný
Sókratés	Sókratésa	k1gFnPc2	Sókratésa
nechtěl	chtít	k5eNaImAgMnS	chtít
opustit	opustit	k5eAaPmF	opustit
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
mu	on	k3xPp3gMnSc3	on
měl	mít	k5eAaImAgMnS	mít
zachránit	zachránit	k5eAaPmF	zachránit
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Platón	platón	k1gInSc1	platón
v	v	k7c6	v
Symposiu	symposion	k1gNnSc6	symposion
uvádí	uvádět	k5eAaImIp3nP	uvádět
Alkibiadovu	Alkibiadův	k2eAgFnSc4d1	Alkibiadova
řeč	řeč	k1gFnSc4	řeč
následovně	následovně	k6eAd1	následovně
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Já	já	k3xPp1nSc1	já
jsem	být	k5eAaImIp1nS	být
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
Sókrate	Sókrat	k1gMnSc5	Sókrat
<g/>
,	,	kIx,	,
vybízel	vybízet	k5eAaImAgMnS	vybízet
vůdce	vůdce	k1gMnPc4	vůdce
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
dali	dát	k5eAaPmAgMnP	dát
vyznamenání	vyznamenání	k1gNnSc4	vyznamenání
tobě	ty	k3xPp2nSc3	ty
<g/>
,	,	kIx,	,
a	a	k8xC	a
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
mne	já	k3xPp1nSc4	já
ani	ani	k8xC	ani
nepokáráš	pokárat	k5eNaPmIp2nS	pokárat
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
neřekneš	říct	k5eNaPmIp2nS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
lžu	lhát	k5eAaImIp1nS	lhát
<g/>
;	;	kIx,	;
ale	ale	k8xC	ale
když	když	k8xS	když
vůdcové	vůdce	k1gMnPc1	vůdce
<g/>
,	,	kIx,	,
přihlížejíce	přihlížet	k5eAaImSgFnP	přihlížet
k	k	k7c3	k
mému	můj	k3xOp1gNnSc3	můj
postavení	postavení	k1gNnSc3	postavení
<g/>
,	,	kIx,	,
chtěli	chtít	k5eAaImAgMnP	chtít
dát	dát	k5eAaPmF	dát
to	ten	k3xDgNnSc4	ten
vyznamenání	vyznamenání	k1gNnSc4	vyznamenání
mně	já	k3xPp1nSc6	já
<g/>
,	,	kIx,	,
sám	sám	k3xTgMnSc1	sám
jsi	být	k5eAaImIp2nS	být
usiloval	usilovat	k5eAaImAgMnS	usilovat
horlivěji	horlivě	k6eAd2	horlivě
než	než	k8xS	než
vůdcové	vůdce	k1gMnPc1	vůdce
<g/>
,	,	kIx,	,
abych	aby	kYmCp1nS	aby
já	já	k3xPp1nSc1	já
je	on	k3xPp3gInPc4	on
dostal	dostat	k5eAaPmAgMnS	dostat
spíše	spíše	k9	spíše
nežli	nežli	k8xS	nežli
ty	ty	k3xPp2nSc1	ty
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Thúkýdidovy	Thúkýdidův	k2eAgFnPc1d1	Thúkýdidův
Dějiny	dějiny	k1gFnPc1	dějiny
peloponéské	peloponéský	k2eAgFnSc2d1	Peloponéská
války	válka	k1gFnSc2	válka
zaznamenávají	zaznamenávat	k5eAaImIp3nP	zaznamenávat
jeho	jeho	k3xOp3gFnSc4	jeho
službu	služba	k1gFnSc4	služba
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
těchto	tento	k3xDgInPc2	tento
zdrojů	zdroj	k1gInPc2	zdroj
chodil	chodit	k5eAaImAgInS	chodit
Sókratés	Sókratés	k1gInSc1	Sókratés
jako	jako	k8xS	jako
voják	voják	k1gMnSc1	voják
bos	bos	k1gMnSc1	bos
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
dokonce	dokonce	k9	dokonce
i	i	k9	i
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Potidají	Potidají	k1gFnSc2	Potidají
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
athénské	athénský	k2eAgNnSc1d1	athénské
vojsko	vojsko	k1gNnSc1	vojsko
pochodovalo	pochodovat	k5eAaImAgNnS	pochodovat
přes	přes	k7c4	přes
zamrzlou	zamrzlý	k2eAgFnSc4d1	zamrzlá
řeku	řeka	k1gFnSc4	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Jindy	jindy	k6eAd1	jindy
prý	prý	k9	prý
Sókratés	Sókratés	k1gInSc1	Sókratés
zůstal	zůstat	k5eAaPmAgInS	zůstat
stát	stát	k5eAaImF	stát
ve	v	k7c6	v
vojenském	vojenský	k2eAgInSc6d1	vojenský
táboře	tábor	k1gInSc6	tábor
celý	celý	k2eAgInSc4d1	celý
den	den	k1gInSc4	den
bez	bez	k7c2	bez
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
,	,	kIx,	,
ponořen	ponořen	k2eAgMnSc1d1	ponořen
do	do	k7c2	do
svých	svůj	k3xOyFgFnPc2	svůj
myšlenek	myšlenka	k1gFnPc2	myšlenka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
422	[number]	k4	422
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
získali	získat	k5eAaPmAgMnP	získat
v	v	k7c6	v
Athénách	Athéna	k1gFnPc6	Athéna
vliv	vliv	k1gInSc4	vliv
mladí	mladý	k2eAgMnPc1d1	mladý
političtí	politický	k2eAgMnPc1d1	politický
reformátoři	reformátor	k1gMnPc1	reformátor
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
se	s	k7c7	s
Sókratovým	Sókratův	k2eAgMnSc7d1	Sókratův
žákem	žák	k1gMnSc7	žák
Alkibiadem	Alkibiad	k1gInSc7	Alkibiad
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
nim	on	k3xPp3gInPc3	on
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
další	další	k2eAgFnSc1d1	další
fáze	fáze	k1gFnSc1	fáze
Peloponéské	peloponéský	k2eAgFnSc2d1	Peloponéská
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
byly	být	k5eAaImAgFnP	být
Athény	Athéna	k1gFnPc1	Athéna
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
spojenci	spojenec	k1gMnPc7	spojenec
r.	r.	kA	r.
404	[number]	k4	404
poraženy	poražen	k2eAgInPc1d1	poražen
a	a	k8xC	a
skončil	skončit	k5eAaPmAgInS	skončit
tak	tak	k9	tak
jejich	jejich	k3xOp3gInSc1	jejich
zlatý	zlatý	k2eAgInSc1d1	zlatý
věk	věk	k1gInSc1	věk
<g/>
.	.	kIx.	.
</s>
<s>
Vlády	vláda	k1gFnPc1	vláda
v	v	k7c6	v
Athénách	Athéna	k1gFnPc6	Athéna
se	se	k3xPyFc4	se
chopila	chopit	k5eAaPmAgFnS	chopit
tzv.	tzv.	kA	tzv.
vláda	vláda	k1gFnSc1	vláda
třiceti	třicet	k4xCc2	třicet
tyranů	tyran	k1gMnPc2	tyran
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gNnSc6	jejíž
čele	čelo	k1gNnSc6	čelo
stál	stát	k5eAaImAgInS	stát
Kritiás	Kritiás	k1gInSc1	Kritiás
<g/>
,	,	kIx,	,
další	další	k2eAgMnSc1d1	další
Sókratův	Sókratův	k2eAgMnSc1d1	Sókratův
žák	žák	k1gMnSc1	žák
<g/>
.	.	kIx.	.
</s>
<s>
Platón	Platón	k1gMnSc1	Platón
účast	účast	k1gFnSc1	účast
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
této	tento	k3xDgFnSc2	tento
vlády	vláda	k1gFnSc2	vláda
byl	být	k5eAaImAgInS	být
Sókratés	Sókratés	k1gInSc1	Sókratés
obviňován	obviňován	k2eAgInSc1d1	obviňován
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
filosofie	filosofie	k1gFnSc1	filosofie
zrodila	zrodit	k5eAaPmAgFnS	zrodit
tyto	tento	k3xDgFnPc4	tento
vlády	vláda	k1gFnPc4	vláda
a	a	k8xC	a
přivedla	přivést	k5eAaPmAgFnS	přivést
Athény	Athéna	k1gFnPc4	Athéna
k	k	k7c3	k
porážce	porážka	k1gFnSc3	porážka
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Athénský	athénský	k2eAgInSc1d1	athénský
lidový	lidový	k2eAgInSc1d1	lidový
soud	soud	k1gInSc1	soud
<g/>
,	,	kIx,	,
vedený	vedený	k2eAgInSc1d1	vedený
třemi	tři	k4xCgFnPc7	tři
významnými	významný	k2eAgFnPc7d1	významná
osobami	osoba	k1gFnPc7	osoba
<g/>
,	,	kIx,	,
vyšetřoval	vyšetřovat	k5eAaImAgMnS	vyšetřovat
Sókrata	Sókrat	k1gMnSc4	Sókrat
pro	pro	k7c4	pro
podezření	podezření	k1gNnSc4	podezření
z	z	k7c2	z
bezbožnosti	bezbožnost	k1gFnSc2	bezbožnost
a	a	k8xC	a
kažení	kažení	k1gNnSc2	kažení
athénské	athénský	k2eAgFnSc2d1	Athénská
mládeže	mládež	k1gFnSc2	mládež
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
možná	možná	k9	možná
přispěla	přispět	k5eAaPmAgFnS	přispět
i	i	k9	i
Aristofanova	Aristofanův	k2eAgFnSc1d1	Aristofanova
komedie	komedie	k1gFnSc1	komedie
Oblaka	oblaka	k1gNnPc1	oblaka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
líčí	líčit	k5eAaImIp3nS	líčit
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
vlivem	vliv	k1gInSc7	vliv
Sókratova	Sókratův	k2eAgNnSc2d1	Sókratovo
učení	učení	k1gNnSc2	učení
synové	syn	k1gMnPc1	syn
obracejí	obracet	k5eAaImIp3nP	obracet
proti	proti	k7c3	proti
svým	svůj	k3xOyFgMnPc3	svůj
otcům	otec	k1gMnPc3	otec
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
hlavní	hlavní	k2eAgInSc4d1	hlavní
pramen	pramen	k1gInSc4	pramen
podávající	podávající	k2eAgFnSc4d1	podávající
zprávu	zpráva	k1gFnSc4	zpráva
o	o	k7c6	o
soudu	soud	k1gInSc6	soud
se	s	k7c7	s
Sókratem	Sókrat	k1gInSc7	Sókrat
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
Platónův	Platónův	k2eAgInSc1d1	Platónův
dialog	dialog	k1gInSc1	dialog
Obrana	obrana	k1gFnSc1	obrana
Sókrata	Sókrata	k1gFnSc1	Sókrata
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
de	de	k?	de
facto	facto	k1gNnSc4	facto
Platónova	Platónův	k2eAgFnSc1d1	Platónova
verze	verze	k1gFnSc1	verze
Sókratovy	Sókratův	k2eAgFnSc2d1	Sókratova
obhajovací	obhajovací	k2eAgFnSc2d1	obhajovací
řeči	řeč	k1gFnSc2	řeč
<g/>
.	.	kIx.	.
</s>
<s>
Dialog	dialog	k1gInSc1	dialog
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgFnPc4	tři
části	část	k1gFnPc4	část
<g/>
:	:	kIx,	:
v	v	k7c6	v
první	první	k4xOgFnSc6	první
se	se	k3xPyFc4	se
Sókratés	Sókratésa	k1gFnPc2	Sókratésa
hájí	hájit	k5eAaImIp3nS	hájit
nejen	nejen	k6eAd1	nejen
proti	proti	k7c3	proti
vzneseným	vznesený	k2eAgNnPc3d1	vznesené
obviněním	obvinění	k1gNnPc3	obvinění
Meléta	Melét	k1gMnSc2	Melét
<g/>
,	,	kIx,	,
Anyta	Anyt	k1gMnSc2	Anyt
a	a	k8xC	a
Lykóna	Lykón	k1gMnSc2	Lykón
(	(	kIx(	(
<g/>
které	který	k3yRgNnSc1	který
označuje	označovat	k5eAaImIp3nS	označovat
za	za	k7c4	za
"	"	kIx"	"
<g/>
druhé	druhý	k4xOgMnPc4	druhý
žalobce	žalobce	k1gMnPc4	žalobce
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
především	především	k9	především
proti	proti	k7c3	proti
žalobcům	žalobce	k1gMnPc3	žalobce
"	"	kIx"	"
<g/>
prvním	první	k4xOgMnSc6	první
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
proti	proti	k7c3	proti
pomluvám	pomluva	k1gFnPc3	pomluva
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
o	o	k7c6	o
něm	on	k3xPp3gInSc6	on
po	po	k7c6	po
Athénách	Athéna	k1gFnPc6	Athéna
kolují	kolovat	k5eAaImIp3nP	kolovat
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
v	v	k7c6	v
Athénách	Athéna	k1gFnPc6	Athéna
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
bůh	bůh	k1gMnSc1	bůh
pověřil	pověřit	k5eAaPmAgMnS	pověřit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgInS	věnovat
filosofii	filosofie	k1gFnSc3	filosofie
<g/>
,	,	kIx,	,
zkoumal	zkoumat	k5eAaImAgMnS	zkoumat
sebe	sebe	k3xPyFc4	sebe
sama	sám	k3xTgMnSc4	sám
i	i	k9	i
druhé	druhý	k4xOgFnPc1	druhý
a	a	k8xC	a
pečoval	pečovat	k5eAaImAgInS	pečovat
tak	tak	k6eAd1	tak
o	o	k7c6	o
duši	duše	k1gFnSc6	duše
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc4	druhý
část	část	k1gFnSc4	část
své	svůj	k3xOyFgFnSc2	svůj
řeči	řeč	k1gFnSc2	řeč
pronáší	pronášet	k5eAaImIp3nS	pronášet
Sókratés	Sókratés	k1gInSc4	Sókratés
v	v	k7c6	v
situaci	situace	k1gFnSc6	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
soudem	soud	k1gInSc7	soud
uznán	uznat	k5eAaPmNgInS	uznat
vinným	vinný	k1gMnSc7	vinný
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
si	se	k3xPyFc3	se
podle	podle	k7c2	podle
tehdejšího	tehdejší	k2eAgNnSc2d1	tehdejší
athénského	athénský	k2eAgNnSc2d1	athénské
práva	právo	k1gNnSc2	právo
navrhnout	navrhnout	k5eAaPmF	navrhnout
trest	trest	k1gInSc4	trest
jako	jako	k8xS	jako
alternativu	alternativa	k1gFnSc4	alternativa
k	k	k7c3	k
návrhu	návrh	k1gInSc3	návrh
obžaloby	obžaloba	k1gFnSc2	obžaloba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
žádá	žádat	k5eAaImIp3nS	žádat
jeho	jeho	k3xOp3gFnSc4	jeho
smrt	smrt	k1gFnSc4	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Sókratés	Sókratés	k6eAd1	Sókratés
však	však	k9	však
považuje	považovat	k5eAaImIp3nS	považovat
svou	svůj	k3xOyFgFnSc4	svůj
filosofickou	filosofický	k2eAgFnSc4d1	filosofická
činnost	činnost	k1gFnSc4	činnost
za	za	k7c4	za
prokazování	prokazování	k1gNnSc4	prokazování
největšího	veliký	k2eAgInSc2d3	veliký
dobrodiní	dobrodiní	k1gNnSc4	dobrodiní
obci	obec	k1gFnSc3	obec
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
si	se	k3xPyFc3	se
navrhuje	navrhovat	k5eAaImIp3nS	navrhovat
jako	jako	k9	jako
jediný	jediný	k2eAgInSc1d1	jediný
spravedlivý	spravedlivý	k2eAgInSc1d1	spravedlivý
"	"	kIx"	"
<g/>
trest	trest	k1gInSc4	trest
<g/>
"	"	kIx"	"
doživotní	doživotní	k2eAgNnPc4d1	doživotní
stravování	stravování	k1gNnPc4	stravování
na	na	k7c4	na
náklady	náklad	k1gInPc4	náklad
obce	obec	k1gFnSc2	obec
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
prytaneiu	prytaneius	k1gMnSc6	prytaneius
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byla	být	k5eAaImAgFnS	být
čest	čest	k1gFnSc1	čest
prokazovaná	prokazovaný	k2eAgFnSc1d1	prokazovaná
olympijským	olympijský	k2eAgMnSc7d1	olympijský
vítězům	vítěz	k1gMnPc3	vítěz
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
však	však	k9	však
souhlasí	souhlasit	k5eAaImIp3nP	souhlasit
i	i	k9	i
s	s	k7c7	s
peněžitou	peněžitý	k2eAgFnSc7d1	peněžitá
pokutou	pokuta	k1gFnSc7	pokuta
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc1	třetí
část	část	k1gFnSc1	část
dialogu	dialog	k1gInSc2	dialog
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
Sókratovou	Sókratův	k2eAgFnSc7d1	Sókratova
závěrečnou	závěrečný	k2eAgFnSc7d1	závěrečná
řečí	řeč	k1gFnSc7	řeč
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
se	se	k3xPyFc4	se
soud	soud	k1gInSc1	soud
přikloní	přiklonit	k5eAaPmIp3nS	přiklonit
k	k	k7c3	k
návrhu	návrh	k1gInSc3	návrh
obžaloby	obžaloba	k1gFnSc2	obžaloba
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
k	k	k7c3	k
trestu	trest	k1gInSc3	trest
smrti	smrt	k1gFnSc2	smrt
(	(	kIx(	(
<g/>
z	z	k7c2	z
501	[number]	k4	501
soudců	soudce	k1gMnPc2	soudce
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
hlasovalo	hlasovat	k5eAaImAgNnS	hlasovat
281	[number]	k4	281
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Platónův	Platónův	k2eAgInSc4d1	Platónův
dialog	dialog	k1gInSc4	dialog
Kritón	Kritón	k1gMnSc1	Kritón
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
situaci	situace	k1gFnSc4	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
do	do	k7c2	do
vězení	vězení	k1gNnSc2	vězení
za	za	k7c7	za
odsouzeným	odsouzený	k1gMnSc7	odsouzený
Sókratem	Sókrat	k1gMnSc7	Sókrat
přichází	přicházet	k5eAaImIp3nS	přicházet
jeho	jeho	k3xOp3gMnSc1	jeho
starý	starý	k2eAgMnSc1d1	starý
přítel	přítel	k1gMnSc1	přítel
Kritón	Kritón	k1gMnSc1	Kritón
a	a	k8xC	a
nabízí	nabízet	k5eAaImIp3nS	nabízet
mu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
pomůže	pomoct	k5eAaPmIp3nS	pomoct
uprchnout	uprchnout	k5eAaPmF	uprchnout
<g/>
.	.	kIx.	.
</s>
<s>
Sókratés	Sókratés	k6eAd1	Sókratés
nabídku	nabídka	k1gFnSc4	nabídka
nepřijímá	přijímat	k5eNaImIp3nS	přijímat
<g/>
:	:	kIx,	:
útěk	útěk	k1gInSc1	útěk
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
nespravedlností	nespravedlnost	k1gFnSc7	nespravedlnost
<g/>
,	,	kIx,	,
odmítnutím	odmítnutí	k1gNnSc7	odmítnutí
zákonnosti	zákonnost	k1gFnSc2	zákonnost
jako	jako	k8xS	jako
takové	takový	k3xDgInPc4	takový
<g/>
,	,	kIx,	,
zničením	zničení	k1gNnSc7	zničení
platnosti	platnost	k1gFnSc2	platnost
zákonů	zákon	k1gInPc2	zákon
obce	obec	k1gFnSc2	obec
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
individuální	individuální	k2eAgInSc4d1	individuální
lidský	lidský	k2eAgInSc4d1	lidský
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc4d1	poslední
den	den	k1gInSc4	den
Sókratova	Sókratův	k2eAgInSc2d1	Sókratův
života	život	k1gInSc2	život
je	být	k5eAaImIp3nS	být
líčen	líčit	k5eAaImNgInS	líčit
v	v	k7c6	v
dialogu	dialog	k1gInSc6	dialog
Faidón	Faidón	k1gInSc1	Faidón
<g/>
.	.	kIx.	.
</s>
<s>
Sókratés	Sókratés	k6eAd1	Sókratés
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
snaží	snažit	k5eAaImIp3nP	snažit
zbavit	zbavit	k5eAaPmF	zbavit
své	svůj	k3xOyFgMnPc4	svůj
přátele	přítel	k1gMnPc4	přítel
strachu	strach	k1gInSc3	strach
ze	z	k7c2	z
smrti	smrt	k1gFnSc2	smrt
a	a	k8xC	a
smutku	smutek	k1gInSc2	smutek
nad	nad	k7c7	nad
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ztratí	ztratit	k5eAaPmIp3nS	ztratit
svého	svůj	k3xOyFgMnSc4	svůj
učitele	učitel	k1gMnSc4	učitel
<g/>
.	.	kIx.	.
</s>
<s>
Přesvědčuje	přesvědčovat	k5eAaImIp3nS	přesvědčovat
je	být	k5eAaImIp3nS	být
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
dobrý	dobrý	k2eAgMnSc1d1	dobrý
a	a	k8xC	a
celý	celý	k2eAgInSc1d1	celý
život	život	k1gInSc1	život
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaImIp3nS	věnovat
filosofii	filosofie	k1gFnSc3	filosofie
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nemusí	muset	k5eNaImIp3nS	muset
smrti	smrt	k1gFnSc3	smrt
bát	bát	k5eAaImF	bát
<g/>
:	:	kIx,	:
jednak	jednak	k8xC	jednak
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
duše	duše	k1gFnSc1	duše
je	být	k5eAaImIp3nS	být
nesmrtelná	smrtelný	k2eNgFnSc1d1	nesmrtelná
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
filosofie	filosofie	k1gFnSc1	filosofie
duši	duše	k1gFnSc4	duše
očišťuje	očišťovat	k5eAaImIp3nS	očišťovat
od	od	k7c2	od
zla	zlo	k1gNnSc2	zlo
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
těla	tělo	k1gNnSc2	tělo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
i	i	k9	i
od	od	k7c2	od
zla	zlo	k1gNnSc2	zlo
největšího	veliký	k2eAgNnSc2d3	veliký
<g/>
:	:	kIx,	:
od	od	k7c2	od
přesvědčení	přesvědčení	k1gNnSc2	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejpravdivější	pravdivý	k2eAgMnSc1d3	nejpravdivější
a	a	k8xC	a
nejskutečnější	skutečný	k2eAgMnSc1d3	nejskutečnější
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k8xS	co
tělu	tělo	k1gNnSc3	tělo
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
nejsilnější	silný	k2eAgFnSc1d3	nejsilnější
slasti	slast	k1gFnPc1	slast
nebo	nebo	k8xC	nebo
bolesti	bolest	k1gFnPc1	bolest
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
dialogu	dialog	k1gInSc2	dialog
vypije	vypít	k5eAaPmIp3nS	vypít
Sókratés	Sókratés	k1gInSc1	Sókratés
číši	číše	k1gFnSc4	číše
bolehlavu	bolehlav	k1gInSc2	bolehlav
a	a	k8xC	a
umírá	umírat	k5eAaImIp3nS	umírat
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Nevzniká	vznikat	k5eNaImIp3nS	vznikat
z	z	k7c2	z
peněz	peníze	k1gInPc2	peníze
ctnost	ctnost	k1gFnSc1	ctnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
z	z	k7c2	z
ctnosti	ctnost	k1gFnSc2	ctnost
vznikají	vznikat	k5eAaImIp3nP	vznikat
peníze	peníz	k1gInPc4	peníz
i	i	k8xC	i
všechny	všechen	k3xTgFnPc4	všechen
ostatní	ostatní	k2eAgFnPc4d1	ostatní
pro	pro	k7c4	pro
lidi	člověk	k1gMnPc4	člověk
dobré	dobrý	k2eAgFnSc2d1	dobrá
věci	věc	k1gFnSc2	věc
<g/>
,	,	kIx,	,
v	v	k7c6	v
soukromí	soukromí	k1gNnSc6	soukromí
i	i	k8xC	i
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
hlásáním	hlásání	k1gNnSc7	hlásání
těchto	tento	k3xDgFnPc2	tento
zásad	zásada	k1gFnPc2	zásada
kazím	kazit	k5eAaImIp1nS	kazit
mládež	mládež	k1gFnSc1	mládež
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
by	by	kYmCp3nP	by
tyto	tento	k3xDgFnPc4	tento
zásady	zásada	k1gFnPc4	zásada
škodlivé	škodlivý	k2eAgFnPc4d1	škodlivá
<g/>
.	.	kIx.	.
</s>
<s>
Tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
-li	i	k?	-li
však	však	k9	však
někdo	někdo	k3yInSc1	někdo
<g/>
,	,	kIx,	,
že	že	k8xS	že
hlásám	hlásat	k5eAaImIp1nS	hlásat
něco	něco	k3yInSc1	něco
jiného	jiný	k2eAgNnSc2d1	jiné
<g/>
,	,	kIx,	,
mluví	mluvit	k5eAaImIp3nS	mluvit
nepravdu	nepravda	k1gFnSc4	nepravda
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k6eAd1	tak
bych	by	kYmCp1nS	by
mohl	moct	k5eAaImAgInS	moct
jenom	jenom	k9	jenom
říci	říct	k5eAaPmF	říct
<g/>
:	:	kIx,	:
Poslechněte	poslechnout	k5eAaPmRp2nP	poslechnout
<g/>
,	,	kIx,	,
Athéňané	Athéňan	k1gMnPc1	Athéňan
<g/>
,	,	kIx,	,
Anyta	Anyt	k1gInSc2	Anyt
nebo	nebo	k8xC	nebo
neposlechněte	poslechnout	k5eNaPmRp2nP	poslechnout
a	a	k8xC	a
osvoboďte	osvobodit	k5eAaPmRp2nP	osvobodit
mne	já	k3xPp1nSc2	já
nebo	nebo	k8xC	nebo
neosvoboďte	osvobodit	k5eNaPmRp2nP	osvobodit
<g/>
,	,	kIx,	,
vězte	vědět	k5eAaImRp2nP	vědět
však	však	k9	však
<g/>
,	,	kIx,	,
že	že	k8xS	že
nebudu	být	k5eNaImBp1nS	být
jednat	jednat	k5eAaImF	jednat
jinak	jinak	k6eAd1	jinak
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
kdybych	kdyby	kYmCp1nS	kdyby
měl	mít	k5eAaImAgInS	mít
stokrát	stokrát	k6eAd1	stokrát
umřít	umřít	k5eAaPmF	umřít
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
</s>
<s>
Největším	veliký	k2eAgInSc7d3	veliký
přínosem	přínos	k1gInSc7	přínos
západnímu	západní	k2eAgInSc3d1	západní
myšlení	myšlení	k1gNnSc2	myšlení
je	být	k5eAaImIp3nS	být
Sókratova	Sókratův	k2eAgFnSc1d1	Sókratova
dialogická	dialogický	k2eAgFnSc1d1	dialogická
metoda	metoda	k1gFnSc1	metoda
tázání	tázání	k1gNnSc2	tázání
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
sókratovská	sókratovský	k2eAgFnSc1d1	sókratovská
či	či	k8xC	či
maieutická	maieutický	k2eAgFnSc1d1	maieutický
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
negativní	negativní	k2eAgFnSc4d1	negativní
metodu	metoda	k1gFnSc4	metoda
vylučování	vylučování	k1gNnSc2	vylučování
hypotéz	hypotéza	k1gFnPc2	hypotéza
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
lepší	dobrý	k2eAgFnPc1d2	lepší
hypotézy	hypotéza	k1gFnPc1	hypotéza
jsou	být	k5eAaImIp3nP	být
zastávány	zastáván	k2eAgFnPc1d1	zastávána
a	a	k8xC	a
horší	zlý	k2eAgFnPc1d2	horší
vylučovány	vylučován	k2eAgFnPc1d1	vylučována
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
vedou	vést	k5eAaImIp3nP	vést
ke	k	k7c3	k
sporu	spor	k1gInSc3	spor
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
dospívá	dospívat	k5eAaImIp3nS	dospívat
k	k	k7c3	k
hypotézám	hypotéza	k1gFnPc3	hypotéza
<g/>
,	,	kIx,	,
tvrzením	tvrzení	k1gNnSc7	tvrzení
a	a	k8xC	a
axiomům	axiom	k1gInPc3	axiom
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgInPc6	jenž
tyto	tento	k3xDgFnPc4	tento
hypotézy	hypotéza	k1gFnPc1	hypotéza
stojí	stát	k5eAaImIp3nP	stát
a	a	k8xC	a
které	který	k3yRgFnPc1	který
nevědomě	vědomě	k6eNd1	vědomě
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
názor	názor	k1gInSc4	názor
člověka	člověk	k1gMnSc4	člověk
na	na	k7c4	na
danou	daný	k2eAgFnSc4d1	daná
otázku	otázka	k1gFnSc4	otázka
(	(	kIx(	(
<g/>
převážně	převážně	k6eAd1	převážně
etického	etický	k2eAgInSc2d1	etický
charakteru	charakter	k1gInSc2	charakter
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
maieutická	maieutický	k2eAgFnSc1d1	maieutický
metoda	metoda	k1gFnSc1	metoda
či	či	k8xC	či
maieutika	maieutika	k1gFnSc1	maieutika
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
samotné	samotný	k2eAgFnSc2d1	samotná
metody	metoda	k1gFnSc2	metoda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
přivádí	přivádět	k5eAaImIp3nS	přivádět
na	na	k7c4	na
svět	svět	k1gInSc4	svět
implicitní	implicitní	k2eAgInPc4d1	implicitní
názory	názor	k1gInPc4	názor
tázaného	tázaný	k2eAgNnSc2d1	tázané
<g/>
,	,	kIx,	,
kterým	který	k3yIgFnPc3	který
pak	pak	k6eAd1	pak
sám	sám	k3xTgMnSc1	sám
lépe	dobře	k6eAd2	dobře
porozumí	porozumět	k5eAaPmIp3nS	porozumět
(	(	kIx(	(
<g/>
řecké	řecký	k2eAgNnSc1d1	řecké
μ	μ	k?	μ
maieutria	maieutrium	k1gNnSc2	maieutrium
znamená	znamenat	k5eAaImIp3nS	znamenat
porodní	porodní	k2eAgFnSc1d1	porodní
bába	bába	k1gFnSc1	bába
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výrok	výrok	k1gInSc1	výrok
Sókratovi	Sókrat	k1gMnSc3	Sókrat
připisovaný	připisovaný	k2eAgMnSc1d1	připisovaný
<g/>
,	,	kIx,	,
alespoň	alespoň	k9	alespoň
mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
<g/>
,	,	kIx,	,
pocházející	pocházející	k2eAgMnPc1d1	pocházející
nejspíše	nejspíše	k9	nejspíše
z	z	k7c2	z
příliš	příliš	k6eAd1	příliš
odvážné	odvážný	k2eAgFnSc2d1	odvážná
interpretace	interpretace	k1gFnSc2	interpretace
Platónových	Platónův	k2eAgInPc2d1	Platónův
dialogů	dialog	k1gInPc2	dialog
<g/>
,	,	kIx,	,
především	především	k9	především
snad	snad	k9	snad
dialogu	dialog	k1gInSc2	dialog
Theaitétos	Theaitétosa	k1gFnPc2	Theaitétosa
(	(	kIx(	(
<g/>
150	[number]	k4	150
<g/>
c	c	k0	c
<g/>
,	,	kIx,	,
210	[number]	k4	210
<g/>
c	c	k0	c
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stručně	stručně	k6eAd1	stručně
a	a	k8xC	a
snad	snad	k9	snad
i	i	k9	i
do	do	k7c2	do
určité	určitý	k2eAgFnSc2d1	určitá
míry	míra	k1gFnSc2	míra
výstižně	výstižně	k6eAd1	výstižně
o	o	k7c6	o
věci	věc	k1gFnSc6	věc
vypovídají	vypovídat	k5eAaImIp3nP	vypovídat
dva	dva	k4xCgInPc1	dva
níže	nízce	k6eAd2	nízce
uvedené	uvedený	k2eAgInPc1d1	uvedený
úryvky	úryvek	k1gInPc1	úryvek
<g/>
.	.	kIx.	.
</s>
<s>
Zašel	zajít	k5eAaPmAgMnS	zajít
jsem	být	k5eAaImIp1nS	být
ke	k	k7c3	k
kterémusi	kterýsi	k3yIgNnSc3	kterýsi
z	z	k7c2	z
mužů	muž	k1gMnPc2	muž
podle	podle	k7c2	podle
zdání	zdání	k1gNnSc2	zdání
moudrých	moudrý	k2eAgFnPc2d1	moudrá
<g/>
,	,	kIx,	,
abych	aby	kYmCp1nS	aby
buď	buď	k8xC	buď
tam	tam	k6eAd1	tam
anebo	anebo	k8xC	anebo
nikde	nikde	k6eAd1	nikde
usvědčil	usvědčit	k5eAaPmAgMnS	usvědčit
věštírnu	věštírna	k1gFnSc4	věštírna
z	z	k7c2	z
nepravdy	nepravda	k1gFnSc2	nepravda
a	a	k8xC	a
ukázal	ukázat	k5eAaPmAgMnS	ukázat
její	její	k3xOp3gFnPc4	její
odpovědi	odpověď	k1gFnPc4	odpověď
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Tento	tento	k3xDgMnSc1	tento
muž	muž	k1gMnSc1	muž
je	být	k5eAaImIp3nS	být
nade	nad	k7c4	nad
mne	já	k3xPp1nSc4	já
moudřejší	moudrý	k2eAgMnSc1d2	moudřejší
<g/>
,	,	kIx,	,
a	a	k8xC	a
ty	ty	k3xPp2nSc1	ty
jsi	být	k5eAaImIp2nS	být
tvrdila	tvrdit	k5eAaImAgFnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
já	já	k3xPp1nSc1	já
jsem	být	k5eAaImIp1nS	být
moudřejší	moudrý	k2eAgMnSc1d2	moudřejší
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g />
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tu	tu	k6eAd1	tu
<g/>
,	,	kIx,	,
když	když	k8xS	když
jsem	být	k5eAaImIp1nS	být
ho	on	k3xPp3gInSc4	on
prozkoumával	prozkoumávat	k5eAaImAgMnS	prozkoumávat
–	–	k?	–
nepotřebuji	potřebovat	k5eNaImIp1nS	potřebovat
ho	on	k3xPp3gInSc4	on
totiž	totiž	k9	totiž
uvádět	uvádět	k5eAaImF	uvádět
jménem	jméno	k1gNnSc7	jméno
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
politiků	politik	k1gMnPc2	politik
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterého	který	k3yIgInSc2	který
se	se	k3xPyFc4	se
mi	já	k3xPp1nSc3	já
něco	něco	k3yInSc1	něco
takového	takový	k3xDgMnSc4	takový
stalo	stát	k5eAaPmAgNnS	stát
<g/>
,	,	kIx,	,
občané	občan	k1gMnPc1	občan
athénští	athénský	k2eAgMnPc1d1	athénský
<g/>
,	,	kIx,	,
když	když	k8xS	když
jsem	být	k5eAaImIp1nS	být
ho	on	k3xPp3gMnSc4	on
zkoumal	zkoumat	k5eAaImAgMnS	zkoumat
a	a	k8xC	a
rozmlouval	rozmlouvat	k5eAaImAgMnS	rozmlouvat
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
–	–	k?	–
<g/>
,	,	kIx,	,
nabyl	nabýt	k5eAaPmAgMnS	nabýt
jsem	být	k5eAaImIp1nS	být
<g />
.	.	kIx.	.
</s>
<s>
mínění	mínění	k1gNnSc1	mínění
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tento	tento	k3xDgMnSc1	tento
muž	muž	k1gMnSc1	muž
zdá	zdát	k5eAaImIp3nS	zdát
moudrým	moudrý	k2eAgMnSc7d1	moudrý
<g/>
,	,	kIx,	,
jak	jak	k9	jak
mnoha	mnoho	k4c3	mnoho
jiným	jiný	k2eAgMnPc3d1	jiný
lidem	člověk	k1gMnPc3	člověk
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
obzvláště	obzvláště	k6eAd1	obzvláště
sám	sám	k3xTgMnSc1	sám
sobě	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
že	že	k8xS	že
moudrý	moudrý	k2eAgMnSc1d1	moudrý
není	být	k5eNaImIp3nS	být
<g/>
;	;	kIx,	;
a	a	k8xC	a
potom	potom	k6eAd1	potom
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
mu	on	k3xPp3gMnSc3	on
ukazovat	ukazovat	k5eAaImF	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
domnívá	domnívat	k5eAaImIp3nS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
moudrý	moudrý	k2eAgMnSc1d1	moudrý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Tu	tu	k6eAd1	tu
jsem	být	k5eAaImIp1nS	být
si	se	k3xPyFc3	se
tím	ten	k3xDgNnSc7	ten
znepřátelil	znepřátelit	k5eAaPmAgMnS	znepřátelit
i	i	k9	i
jeho	jeho	k3xOp3gNnSc4	jeho
i	i	k8xC	i
mnohé	mnohý	k2eAgFnPc4d1	mnohá
z	z	k7c2	z
přítomných	přítomný	k2eAgFnPc2d1	přítomná
<g/>
;	;	kIx,	;
avšak	avšak	k8xC	avšak
odcházeje	odcházet	k5eAaImSgMnS	odcházet
uvažoval	uvažovat	k5eAaImAgMnS	uvažovat
jsem	být	k5eAaImIp1nS	být
sám	sám	k3xTgMnSc1	sám
u	u	k7c2	u
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
že	že	k8xS	že
proti	proti	k7c3	proti
tomuto	tento	k3xDgMnSc3	tento
člověku	člověk	k1gMnSc3	člověk
jsem	být	k5eAaImIp1nS	být
já	já	k3xPp1nSc1	já
opravdu	opravdu	k6eAd1	opravdu
moudřejší	moudrý	k2eAgMnSc1d2	moudřejší
<g/>
;	;	kIx,	;
bezpochyby	bezpochyby	k6eAd1	bezpochyby
totiž	totiž	k9	totiž
ani	ani	k9	ani
jeden	jeden	k4xCgMnSc1	jeden
ani	ani	k8xC	ani
druhý	druhý	k4xOgMnSc1	druhý
z	z	k7c2	z
nás	my	k3xPp1nPc4	my
neví	vědět	k5eNaImIp3nS	vědět
nic	nic	k3yNnSc1	nic
dokonalého	dokonalý	k2eAgNnSc2d1	dokonalé
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tento	tento	k3xDgInSc1	tento
se	se	k3xPyFc4	se
při	při	k7c6	při
svém	své	k1gNnSc6	své
<g />
.	.	kIx.	.
</s>
<s>
nevědění	nevědění	k1gNnSc1	nevědění
domnívá	domnívat	k5eAaImIp3nS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
něco	něco	k3yInSc4	něco
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
já	já	k3xPp1nSc1	já
ani	ani	k9	ani
nevím	vědět	k5eNaImIp1nS	vědět
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
se	se	k3xPyFc4	se
nedomnívám	domnívat	k5eNaImIp1nS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vím	vědět	k5eAaImIp1nS	vědět
<g/>
;	;	kIx,	;
podobá	podobat	k5eAaImIp3nS	podobat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsem	být	k5eAaImIp1nS	být
nad	nad	k7c4	nad
něho	on	k3xPp3gMnSc4	on
moudřejší	moudrý	k2eAgInSc1d2	moudřejší
aspoň	aspoň	k9	aspoň
o	o	k7c4	o
něco	něco	k3yInSc4	něco
málo	málo	k6eAd1	málo
<g/>
,	,	kIx,	,
právě	právě	k9	právě
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
co	co	k3yInSc1	co
nevím	vědět	k5eNaImIp1nS	vědět
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
se	se	k3xPyFc4	se
nedomnívám	domnívat	k5eNaImIp1nS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vím	vědět	k5eAaImIp1nS	vědět
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Obrana	obrana	k1gFnSc1	obrana
Sókrata	Sókrata	k1gFnSc1	Sókrata
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
F.	F.	kA	F.
N.	N.	kA	N.
<g/>
)	)	kIx)	)
Euth	Euth	k1gMnSc1	Euth
<g/>
.	.	kIx.	.
</s>
<s>
Nuže	nuže	k9	nuže
tedy	tedy	k8xC	tedy
mi	já	k3xPp1nSc3	já
odpověz	odpovědět	k5eAaPmRp2nS	odpovědět
<g/>
:	:	kIx,	:
jest	být	k5eAaImIp3nS	být
něco	něco	k3yInSc4	něco
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
znáš	znát	k5eAaImIp2nS	znát
<g/>
?	?	kIx.	?
</s>
<s>
Soc	soc	kA	soc
<g/>
.	.	kIx.	.
Ovšem	ovšem	k9	ovšem
<g/>
,	,	kIx,	,
děl	dít	k5eAaBmAgMnS	dít
jsem	být	k5eAaImIp1nS	být
já	já	k3xPp1nSc1	já
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
mnoho	mnoho	k6eAd1	mnoho
věcí	věc	k1gFnSc7	věc
<g/>
,	,	kIx,	,
arci	arci	k0	arci
malých	malý	k2eAgFnPc2d1	malá
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Euthydémos	Euthydémos	k1gInSc1	Euthydémos
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
F.	F.	kA	F.
N.	N.	kA	N.
<g/>
)	)	kIx)	)
Sókratovské	Sókratovský	k2eAgInPc1d1	Sókratovský
dialogy	dialog	k1gInPc1	dialog
jsou	být	k5eAaImIp3nP	být
dílo	dílo	k1gNnSc4	dílo
Platónovo	Platónův	k2eAgNnSc1d1	Platónovo
<g/>
,	,	kIx,	,
literární	literární	k2eAgFnSc1d1	literární
forma	forma	k1gFnSc1	forma
rozhovoru	rozhovor	k1gInSc2	rozhovor
mezi	mezi	k7c7	mezi
(	(	kIx(	(
<g/>
fiktivním	fiktivní	k2eAgInSc7d1	fiktivní
<g/>
)	)	kIx)	)
Sókratem	Sókrat	k1gInSc7	Sókrat
a	a	k8xC	a
dalšími	další	k2eAgFnPc7d1	další
postavami	postava	k1gFnPc7	postava
jeho	jeho	k3xOp3gFnSc2	jeho
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Ideje	idea	k1gFnPc1	idea
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
se	se	k3xPyFc4	se
Platón	platón	k1gInSc1	platón
snaží	snažit	k5eAaImIp3nS	snažit
zprostředkovat	zprostředkovat	k5eAaPmF	zprostředkovat
<g/>
,	,	kIx,	,
nevycházejí	vycházet	k5eNaImIp3nP	vycházet
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
úst	ústa	k1gNnPc2	ústa
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
postav	postava	k1gFnPc2	postava
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
sókratovské	sókratovský	k2eAgFnSc2d1	sókratovská
metody	metoda	k1gFnSc2	metoda
a	a	k8xC	a
pod	pod	k7c7	pod
jeho	jeho	k3xOp3gNnSc7	jeho
vedením	vedení	k1gNnSc7	vedení
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
vrcholů	vrchol	k1gInPc2	vrchol
této	tento	k3xDgFnSc2	tento
dialogické	dialogický	k2eAgFnSc2d1	dialogická
metody	metoda	k1gFnSc2	metoda
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
dialog	dialog	k1gInSc4	dialog
o	o	k7c6	o
zbožnosti	zbožnost	k1gFnSc6	zbožnost
<g/>
,	,	kIx,	,
Euthyfrón	Euthyfrón	k1gInSc1	Euthyfrón
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Platónově	Platónův	k2eAgInSc6d1	Platónův
filozofickém	filozofický	k2eAgInSc6d1	filozofický
systému	systém	k1gInSc6	systém
je	on	k3xPp3gNnSc4	on
učení	učení	k1gNnSc4	učení
procesem	proces	k1gInSc7	proces
vzpomínání	vzpomínání	k1gNnSc2	vzpomínání
či	či	k8xC	či
upamatování	upamatování	k1gNnSc2	upamatování
<g/>
.	.	kIx.	.
</s>
<s>
Duše	duše	k1gFnSc1	duše
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
se	se	k3xPyFc4	se
vtělí	vtělit	k5eAaPmIp3nS	vtělit
do	do	k7c2	do
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
přebývala	přebývat	k5eAaImAgFnS	přebývat
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
idejí	idea	k1gFnPc2	idea
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
viděla	vidět	k5eAaImAgFnS	vidět
věci	věc	k1gFnPc4	věc
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
jak	jak	k8xS	jak
skutečně	skutečně	k6eAd1	skutečně
jsou	být	k5eAaImIp3nP	být
<g/>
,	,	kIx,	,
a	a	k8xC	a
nikoli	nikoli	k9	nikoli
jen	jen	k9	jen
stíny	stín	k1gInPc1	stín
či	či	k8xC	či
obtisky	obtisk	k1gInPc1	obtisk
těchto	tento	k3xDgFnPc2	tento
věcí	věc	k1gFnPc2	věc
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
pozorujeme	pozorovat	k5eAaImIp1nP	pozorovat
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Procesem	proces	k1gInSc7	proces
tázání	tázání	k1gNnSc2	tázání
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
duše	duše	k1gFnSc1	duše
přivedena	přiveden	k2eAgFnSc1d1	přivedena
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
upamatovala	upamatovat	k5eAaPmAgFnS	upamatovat
na	na	k7c4	na
ideje	idea	k1gFnPc4	idea
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
čisté	čistý	k2eAgFnSc6d1	čistá
podobě	podoba	k1gFnSc6	podoba
a	a	k8xC	a
došla	dojít	k5eAaPmAgFnS	dojít
tak	tak	k9	tak
moudrosti	moudrost	k1gFnPc4	moudrost
<g/>
.	.	kIx.	.
</s>
<s>
Ebert	Ebert	k1gInSc1	Ebert
<g/>
,	,	kIx,	,
T.	T.	kA	T.
<g/>
,	,	kIx,	,
Sókratés	Sókratés	k1gInSc1	Sókratés
jako	jako	k8xS	jako
pythagorejec	pythagorejec	k1gMnSc1	pythagorejec
a	a	k8xC	a
anamnése	anamnése	k1gFnSc1	anamnése
v	v	k7c6	v
Platónově	Platónův	k2eAgInSc6d1	Platónův
dialogu	dialog	k1gInSc6	dialog
Faidón	Faidón	k1gInSc1	Faidón
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Oikoymenh	Oikoymenh	k1gMnSc1	Oikoymenh
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
110	[number]	k4	110
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80-86005-96-8	[number]	k4	80-86005-96-8
FISCHER	Fischer	k1gMnSc1	Fischer
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Ludvík	Ludvík	k1gMnSc1	Ludvík
<g/>
.	.	kIx.	.
</s>
<s>
Případ	případ	k1gInSc1	případ
Sokrates	Sokrates	k1gMnSc1	Sokrates
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
NLN	NLN	kA	NLN
<g/>
,	,	kIx,	,
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
170	[number]	k4	170
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80-7106-110-7	[number]	k4	80-7106-110-7
KESSIDI	KESSIDI	kA	KESSIDI
<g/>
,	,	kIx,	,
F.	F.	kA	F.
Sókratés	Sókratés	k1gInSc1	Sókratés
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
z	z	k7c2	z
ruského	ruský	k2eAgInSc2d1	ruský
originálu	originál	k1gInSc2	originál
<g/>
:	:	kIx,	:
Vilém	Vilém	k1gMnSc1	Vilém
Herold	herold	k1gMnSc1	herold
a	a	k8xC	a
Milan	Milan	k1gMnSc1	Milan
Mráz	Mráz	k1gMnSc1	Mráz
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
201	[number]	k4	201
s.	s.	k?	s.
Patočka	Patočka	k1gMnSc1	Patočka
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
,	,	kIx,	,
Sókratés	Sókratés	k1gInSc1	Sókratés
<g/>
:	:	kIx,	:
přednášky	přednáška	k1gFnPc1	přednáška
z	z	k7c2	z
antické	antický	k2eAgFnSc2d1	antická
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Státní	státní	k2eAgNnSc1d1	státní
pedagogické	pedagogický	k2eAgNnSc1d1	pedagogické
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
158	[number]	k4	158
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80-04-25383-0	[number]	k4	80-04-25383-0
Patočka	Patočka	k1gMnSc1	Patočka
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
,	,	kIx,	,
Platónova	Platónův	k2eAgFnSc1d1	Platónova
péče	péče	k1gFnSc1	péče
o	o	k7c4	o
duši	duše	k1gFnSc4	duše
a	a	k8xC	a
spravedlivý	spravedlivý	k2eAgInSc1d1	spravedlivý
stát	stát	k1gInSc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Oikumené	Oikumený	k2eAgNnSc1d1	Oikumený
2012	[number]	k4	2012
Xenofón	Xenofón	k1gInSc1	Xenofón
<g/>
,	,	kIx,	,
Vzpomínky	vzpomínka	k1gFnPc1	vzpomínka
na	na	k7c4	na
Sókrata	Sókre	k1gNnPc4	Sókre
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Svoboda	svoboda	k1gFnSc1	svoboda
1972	[number]	k4	1972
<g/>
.	.	kIx.	.
450	[number]	k4	450
s.	s.	k?	s.
XENOFÓN	XENOFÓN	kA	XENOFÓN
<g/>
.	.	kIx.	.
</s>
<s>
Xenofontovy	Xenofontův	k2eAgFnPc1d1	Xenofontův
Paměti	paměť	k1gFnPc1	paměť
o	o	k7c6	o
Sokratovi	Sokrates	k1gMnSc6	Sokrates
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Jednota	jednota	k1gFnSc1	jednota
českých	český	k2eAgMnPc2d1	český
filologů	filolog	k1gMnPc2	filolog
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Edvard	Edvard	k1gMnSc1	Edvard
Grégr	Grégr	k1gMnSc1	Grégr
<g/>
,	,	kIx,	,
1872	[number]	k4	1872
<g/>
.	.	kIx.	.
173	[number]	k4	173
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Platón	platón	k1gInSc1	platón
Platonismus	platonismus	k1gInSc1	platonismus
Xenofón	Xenofón	k1gInSc1	Xenofón
Aristotelés	Aristotelés	k1gInSc4	Aristotelés
Starověké	starověký	k2eAgNnSc4d1	starověké
Řecko	Řecko	k1gNnSc4	Řecko
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Sókratés	Sókratésa	k1gFnPc2	Sókratésa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Osoba	osoba	k1gFnSc1	osoba
Sókratés	Sókratés	k1gInSc1	Sókratés
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Autor	autor	k1gMnSc1	autor
Sókratés	Sókratés	k1gInSc1	Sókratés
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
O	o	k7c6	o
Sokratovi	Sokrates	k1gMnSc6	Sokrates
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc6	jeho
ženě	žena	k1gFnSc6	žena
Xantippě	Xantippa	k1gFnSc6	Xantippa
O	o	k7c6	o
Sókratovi	Sókrat	k1gMnSc6	Sókrat
na	na	k7c6	na
Portálu	portál	k1gInSc6	portál
Antika	antika	k1gFnSc1	antika
</s>
