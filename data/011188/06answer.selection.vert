<s>
Římská	římský	k2eAgFnSc1d1	římská
armáda	armáda	k1gFnSc1	armáda
vedená	vedený	k2eAgFnSc1d1	vedená
Publiem	Publium	k1gNnSc7	Publium
Corneliem	Cornelium	k1gNnSc7	Cornelium
Scipionem	Scipion	k1gInSc7	Scipion
(	(	kIx(	(
<g/>
Africanem	African	k1gInSc7	African
<g/>
)	)	kIx)	)
porazila	porazit	k5eAaPmAgFnS	porazit
kartaginské	kartaginský	k2eAgNnSc4d1	kartaginské
vojsko	vojsko	k1gNnSc4	vojsko
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
Hannibala	Hannibal	k1gMnSc2	Hannibal
<g/>
.	.	kIx.	.
</s>
