<s>
Litomyšlský	litomyšlský	k2eAgInSc1d1
úval	úval	k1gInSc1
</s>
<s>
Litomyšlský	litomyšlský	k2eAgInSc1d1
úval	úval	k1gInSc1
Řeka	řeka	k1gFnSc1
Loučná	Loučný	k2eAgFnSc1d1
protékající	protékající	k2eAgFnSc7d1
Litomyšlí	Litomyšl	k1gFnSc7
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3
bod	bod	k1gInSc1
</s>
<s>
525	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
(	(	kIx(
<g/>
vrstevnice	vrstevnice	k1gFnSc1
<g/>
)	)	kIx)
Rozloha	rozloha	k1gFnSc1
</s>
<s>
293,8	293,8	k4
km²	km²	k?
</s>
<s>
Nadřazená	nadřazený	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
</s>
<s>
Loučenská	Loučenský	k2eAgFnSc1d1
tabule	tabule	k1gFnSc1
Sousedníjednotky	Sousedníjednotka	k1gFnSc2
</s>
<s>
Vraclavský	Vraclavský	k2eAgInSc4d1
hřbet	hřbet	k1gInSc4
Novohradská	novohradský	k2eAgFnSc1d1
stupňovina	stupňovina	k1gFnSc1
Poličská	poličský	k2eAgFnSc1d1
tabule	tabula	k1gFnSc3
Kozlovský	kozlovský	k2eAgInSc4d1
hřbet	hřbet	k1gInSc4
Orlická	orlický	k2eAgFnSc1d1
tabule	tabule	k1gFnSc1
Východolabská	Východolabský	k2eAgFnSc1d1
tabule	tabule	k1gFnSc1
Podřazenéjednotky	Podřazenéjednotka	k1gFnSc2
</s>
<s>
Trstěnický	Trstěnický	k2eAgInSc1d1
úval	úval	k1gInSc1
Sloupnický	Sloupnický	k2eAgInSc4d1
úval	úval	k1gInSc4
Vysokomýtská	vysokomýtský	k2eAgFnSc1d1
kotlina	kotlina	k1gFnSc1
</s>
<s>
Světadíl	světadíl	k1gInSc1
</s>
<s>
Evropa	Evropa	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
</s>
<s>
Horniny	hornina	k1gFnPc1
</s>
<s>
slínovec	slínovec	k1gInSc1
<g/>
,	,	kIx,
prachovec	prachovec	k1gInSc1
<g/>
,	,	kIx,
jílovec	jílovec	k1gInSc1
<g/>
,	,	kIx,
pískovec	pískovec	k1gInSc1
Povodí	povodí	k1gNnSc2
</s>
<s>
Loučná	Loučný	k2eAgFnSc1d1
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
53	#num#	k4
<g/>
′	′	k?
<g/>
16	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
16	#num#	k4
<g/>
°	°	k?
<g/>
16	#num#	k4
<g/>
′	′	k?
<g/>
1	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Litomyšlský	litomyšlský	k2eAgInSc1d1
úval	úval	k1gInSc1
je	být	k5eAaImIp3nS
geomorfologický	geomorfologický	k2eAgInSc1d1
okrsek	okrsek	k1gInSc1
v	v	k7c6
severovýchodní	severovýchodní	k2eAgFnSc6d1
části	část	k1gFnSc6
Loučenské	Loučenský	k2eAgFnSc2d1
tabule	tabule	k1gFnSc2
<g/>
,	,	kIx,
ležící	ležící	k2eAgFnSc1d1
v	v	k7c6
okresech	okres	k1gInPc6
Ústí	ústit	k5eAaImIp3nP
nad	nad	k7c7
Orlicí	Orlice	k1gFnSc7
a	a	k8xC
Svitavy	Svitava	k1gFnPc1
v	v	k7c6
Pardubickém	pardubický	k2eAgInSc6d1
kraji	kraj	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Poloha	poloha	k1gFnSc1
a	a	k8xC
sídla	sídlo	k1gNnSc2
</s>
<s>
Severní	severní	k2eAgInSc1d1
svah	svah	k1gInSc1
vrchu	vrch	k1gInSc2
Vinice	vinice	k1gFnSc2
</s>
<s>
Území	území	k1gNnSc1
okrsku	okrsek	k1gInSc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
zhruba	zhruba	k6eAd1
mezi	mezi	k7c7
sídly	sídlo	k1gNnPc7
Týnišťko	Týnišťko	k1gNnSc1
(	(	kIx(
<g/>
na	na	k7c6
severozápadě	severozápad	k1gInSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Choceň	Choceň	k1gFnSc1
(	(	kIx(
<g/>
na	na	k7c6
severu	sever	k1gInSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Voděrady	Voděrada	k1gFnPc4
a	a	k8xC
Mikuleč	Mikuleč	k1gInSc4
(	(	kIx(
<g/>
na	na	k7c6
východě	východ	k1gInSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Vendolí	Vendolí	k1gNnSc1
(	(	kIx(
<g/>
na	na	k7c6
jihovýchodě	jihovýchod	k1gInSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Sebranice	Sebranice	k1gFnPc4
(	(	kIx(
<g/>
na	na	k7c6
jihu	jih	k1gInSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Vidlatá	vidlatý	k2eAgFnSc1d1
Seč	seč	k1gFnSc1
a	a	k8xC
Javorník	Javorník	k1gInSc1
(	(	kIx(
<g/>
na	na	k7c6
západě	západ	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uvnitř	uvnitř	k7c2
okrsku	okrsek	k1gInSc2
leží	ležet	k5eAaImIp3nS
titulní	titulní	k2eAgNnSc1d1
město	město	k1gNnSc1
Litomyšl	Litomyšl	k1gFnSc1
<g/>
,	,	kIx,
další	další	k2eAgNnSc1d1
město	město	k1gNnSc1
Vysoké	vysoká	k1gFnSc2
Mýto	mýto	k1gNnSc1
<g/>
,	,	kIx,
větší	veliký	k2eAgFnPc4d2
obce	obec	k1gFnPc4
Dolní	dolní	k2eAgFnSc1d1
Újezd	Újezd	k1gInSc1
<g/>
,	,	kIx,
Osík	Osík	k1gInSc1
<g/>
,	,	kIx,
Janov	Janov	k1gInSc1
a	a	k8xC
Čistá	čistý	k2eAgFnSc1d1
<g/>
,	,	kIx,
částečně	částečně	k6eAd1
obec	obec	k1gFnSc1
Sloupnice	Sloupnice	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Geomorfologické	geomorfologický	k2eAgNnSc1d1
členění	členění	k1gNnSc1
</s>
<s>
Okrsek	okrsek	k1gInSc1
Litomyšlský	litomyšlský	k2eAgInSc4d1
úval	úval	k1gInSc4
(	(	kIx(
<g/>
dle	dle	k7c2
značení	značení	k1gNnSc2
Jaromíra	Jaromír	k1gMnSc2
Demka	Demek	k1gMnSc2
VIC	VIC	kA
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
B	B	kA
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
)	)	kIx)
geomorfologicky	geomorfologicky	k6eAd1
náleží	náležet	k5eAaImIp3nS
do	do	k7c2
celku	celek	k1gInSc2
Svitavská	svitavský	k2eAgFnSc1d1
pahorkatina	pahorkatina	k1gFnSc1
a	a	k8xC
podcelku	podcelka	k1gFnSc4
Loučenská	Loučenský	k2eAgFnSc1d1
tabule	tabule	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Podle	podle	k7c2
alternativního	alternativní	k2eAgNnSc2d1
členění	členění	k1gNnSc2
Balatky	balatka	k1gFnSc2
a	a	k8xC
Kalvody	Kalvoda	k1gMnSc2
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
se	se	k3xPyFc4
Litomyšlský	litomyšlský	k2eAgInSc1d1
úval	úval	k1gInSc1
dále	daleko	k6eAd2
člení	členit	k5eAaImIp3nS
na	na	k7c4
podokrsky	podokrsky	k6eAd1
<g/>
:	:	kIx,
Trstěnický	Trstěnický	k2eAgInSc1d1
úval	úval	k1gInSc1
na	na	k7c6
západě	západ	k1gInSc6
<g/>
,	,	kIx,
Sloupnický	Sloupnický	k2eAgInSc1d1
úval	úval	k1gInSc1
na	na	k7c6
východě	východ	k1gInSc6
a	a	k8xC
Vysokomýtská	vysokomýtský	k2eAgFnSc1d1
kotlina	kotlina	k1gFnSc1
na	na	k7c6
severu	sever	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Úval	úval	k1gInSc1
sousedí	sousedit	k5eAaImIp3nS
s	s	k7c7
dalšími	další	k2eAgInPc7d1
okrsky	okrsek	k1gInPc7
Svitavské	svitavský	k2eAgFnSc2d1
pahorkatiny	pahorkatina	k1gFnSc2
<g/>
:	:	kIx,
Vraclavský	Vraclavský	k2eAgInSc4d1
hřbet	hřbet	k1gInSc4
a	a	k8xC
Novohradská	novohradský	k2eAgFnSc1d1
stupňovina	stupňovina	k1gFnSc1
na	na	k7c6
západě	západ	k1gInSc6
<g/>
,	,	kIx,
Poličská	poličský	k2eAgFnSc1d1
tabule	tabule	k1gFnSc1
na	na	k7c6
jihu	jih	k1gInSc6
<g/>
,	,	kIx,
Kozlovský	kozlovský	k2eAgInSc1d1
hřbet	hřbet	k1gInSc1
na	na	k7c6
východě	východ	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
sousedí	sousedit	k5eAaImIp3nS
s	s	k7c7
celky	celek	k1gInPc7
Orlická	orlický	k2eAgFnSc1d1
tabule	tabule	k1gFnPc1
na	na	k7c6
severu	sever	k1gInSc6
a	a	k8xC
Východolabská	Východolabský	k2eAgFnSc1d1
tabule	tabule	k1gFnSc1
na	na	k7c6
severozápadě	severozápad	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Významné	významný	k2eAgInPc1d1
vrcholy	vrchol	k1gInPc1
</s>
<s>
název	název	k1gInSc1
vrcholuvýška	vrcholuvýšek	k1gInSc2
(	(	kIx(
<g/>
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
<g/>
podřazená	podřazený	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
</s>
<s>
Zahořanský	Zahořanský	k2eAgInSc1d1
kopec	kopec	k1gInSc1
</s>
<s>
341	#num#	k4
</s>
<s>
Sloupnický	Sloupnický	k2eAgInSc1d1
úval	úval	k1gInSc1
</s>
<s>
Vinice	vinice	k1gFnSc1
</s>
<s>
318	#num#	k4
</s>
<s>
Vysokomýtská	vysokomýtský	k2eAgFnSc1d1
kotlina	kotlina	k1gFnSc1
</s>
<s>
Bučkův	Bučkův	k2eAgInSc1d1
kopec	kopec	k1gInSc1
</s>
<s>
315	#num#	k4
</s>
<s>
Vysokomýtská	vysokomýtský	k2eAgFnSc1d1
kotlina	kotlina	k1gFnSc1
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3
bodem	bod	k1gInSc7
Litomyšlského	litomyšlský	k2eAgInSc2d1
úvalu	úval	k1gInSc2
je	být	k5eAaImIp3nS
vrstevnice	vrstevnice	k1gFnSc1
(	(	kIx(
<g/>
525	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
na	na	k7c6
jižním	jižní	k2eAgNnSc6d1
pomezí	pomezí	k1gNnSc6
s	s	k7c7
Poličskou	poličský	k2eAgFnSc7d1
tabulí	tabule	k1gFnSc7
a	a	k8xC
Kozlovským	kozlovský	k2eAgInSc7d1
hřbetem	hřbet	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Litomyšlský	litomyšlský	k2eAgInSc1d1
úval	úval	k1gInSc1
na	na	k7c4
Mapy	mapa	k1gFnPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
1	#num#	k4
2	#num#	k4
DEMEK	DEMEK	kA
<g/>
,	,	kIx,
Jaromír	Jaromír	k1gMnSc1
<g/>
;	;	kIx,
MACKOVČIN	MACKOVČIN	kA
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
kolektiv	kolektiv	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zeměpisný	zeměpisný	k2eAgInSc1d1
lexikon	lexikon	k1gInSc1
ČR	ČR	kA
<g/>
:	:	kIx,
Hory	hora	k1gFnSc2
a	a	k8xC
nížiny	nížina	k1gFnSc2
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
AOPK	AOPK	kA
ČR	ČR	kA
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
582	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86064	#num#	k4
<g/>
-	-	kIx~
<g/>
99	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
BALATKA	balatka	k1gFnSc1
<g/>
,	,	kIx,
Břetislav	Břetislav	k1gMnSc1
<g/>
;	;	kIx,
KALVODA	Kalvoda	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Geomorfologické	geomorfologický	k2eAgInPc1d1
členění	členění	k1gNnSc4
reliéfu	reliéf	k1gInSc2
Čech	Čechy	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Kartografie	kartografie	k1gFnSc1
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7011	#num#	k4
<g/>
-	-	kIx~
<g/>
913	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Geomorfologická	geomorfologický	k2eAgFnSc1d1
mapa	mapa	k1gFnSc1
na	na	k7c4
Mapy	mapa	k1gFnPc4
<g/>
.	.	kIx.
<g/>
nature	natur	k1gMnSc5
<g/>
.	.	kIx.
<g/>
cz	cz	k?
(	(	kIx(
<g/>
J.	J.	kA
Demek	Demek	k1gInSc1
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
2006	#num#	k4
<g/>
)	)	kIx)
</s>
