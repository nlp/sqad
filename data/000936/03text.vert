<s>
Vila	vila	k1gFnSc1	vila
Tugendhat	Tugendhat	k1gFnSc2	Tugendhat
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
je	být	k5eAaImIp3nS	být
ojedinělým	ojedinělý	k2eAgInSc7d1	ojedinělý
funkcionalistickým	funkcionalistický	k2eAgInSc7d1	funkcionalistický
dílem	díl	k1gInSc7	díl
německého	německý	k2eAgMnSc2d1	německý
architekta	architekt	k1gMnSc2	architekt
Ludwiga	Ludwig	k1gMnSc2	Ludwig
Miese	Mies	k1gMnSc2	Mies
van	van	k1gInSc1	van
der	drát	k5eAaImRp2nS	drát
Rohe	Roh	k1gFnSc2	Roh
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
vypracoval	vypracovat	k5eAaPmAgInS	vypracovat
návrh	návrh	k1gInSc1	návrh
stavby	stavba	k1gFnSc2	stavba
na	na	k7c4	na
zakázku	zakázka	k1gFnSc4	zakázka
manželů	manžel	k1gMnPc2	manžel
Grety	Greta	k1gFnSc2	Greta
a	a	k8xC	a
Fritze	Fritze	k1gFnSc2	Fritze
Tugendhatových	Tugendhatův	k2eAgMnPc2d1	Tugendhatův
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
byla	být	k5eAaImAgFnS	být
vila	vila	k1gFnSc1	vila
Tugendhat	Tugendhat	k1gFnSc2	Tugendhat
zapsána	zapsat	k5eAaPmNgFnS	zapsat
do	do	k7c2	do
seznamu	seznam	k1gInSc2	seznam
světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Vila	vila	k1gFnSc1	vila
Tugendhat	Tugendhat	k1gFnPc2	Tugendhat
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
adrese	adresa	k1gFnSc6	adresa
Černopolní	Černopolní	k2eAgFnSc1d1	Černopolní
45	[number]	k4	45
<g/>
,	,	kIx,	,
na	na	k7c6	na
území	území	k1gNnSc6	území
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
Brno-sever	Brnoevra	k1gFnPc2	Brno-sevra
v	v	k7c6	v
katastrálním	katastrální	k2eAgNnSc6d1	katastrální
území	území	k1gNnSc6	území
Černá	černý	k2eAgFnSc1d1	černá
Pole	pole	k1gFnSc1	pole
na	na	k7c6	na
parcele	parcela	k1gFnSc6	parcela
č.	č.	k?	č.
3365	[number]	k4	3365
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
i	i	k8xC	i
přilehlá	přilehlý	k2eAgFnSc1d1	přilehlá
zahrada	zahrada	k1gFnSc1	zahrada
s	s	k7c7	s
parcelním	parcelní	k2eAgNnSc7d1	parcelní
číslem	číslo	k1gNnSc7	číslo
3366	[number]	k4	3366
<g/>
.	.	kIx.	.
</s>
<s>
Pozemek	pozemek	k1gInSc1	pozemek
v	v	k7c6	v
příkrém	příkrý	k2eAgInSc6d1	příkrý
svahu	svah	k1gInSc6	svah
byl	být	k5eAaImAgInS	být
součástí	součást	k1gFnSc7	součást
velkého	velký	k2eAgInSc2d1	velký
pozemku	pozemek	k1gInSc2	pozemek
<g/>
,	,	kIx,	,
patřícího	patřící	k2eAgInSc2d1	patřící
k	k	k7c3	k
vile	vila	k1gFnSc3	vila
otce	otec	k1gMnSc2	otec
Grety	Greta	k1gFnSc2	Greta
Tugendhatové	Tugendhatový	k2eAgFnSc2d1	Tugendhatová
<g/>
,	,	kIx,	,
známého	známý	k2eAgMnSc2d1	známý
brněnského	brněnský	k2eAgMnSc2d1	brněnský
textilního	textilní	k2eAgMnSc2d1	textilní
průmyslníka	průmyslník	k1gMnSc2	průmyslník
Alfreda	Alfred	k1gMnSc2	Alfred
Löw-Beera	Löw-Beer	k1gMnSc2	Löw-Beer
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
horní	horní	k2eAgFnSc1d1	horní
část	část	k1gFnSc1	část
svého	svůj	k3xOyFgInSc2	svůj
pozemku	pozemek	k1gInSc2	pozemek
dceři	dcera	k1gFnSc3	dcera
věnoval	věnovat	k5eAaPmAgInS	věnovat
jako	jako	k9	jako
svatební	svatební	k2eAgInSc4d1	svatební
dar	dar	k1gInSc4	dar
a	a	k8xC	a
celou	celý	k2eAgFnSc4d1	celá
stavbu	stavba	k1gFnSc4	stavba
domu	dům	k1gInSc2	dům
také	také	k9	také
financoval	financovat	k5eAaBmAgInS	financovat
<g/>
.	.	kIx.	.
</s>
<s>
Vila	vila	k1gFnSc1	vila
je	být	k5eAaImIp3nS	být
označována	označovat	k5eAaImNgFnS	označovat
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
německým	německý	k2eAgInSc7d1	německý
pavilónem	pavilón	k1gInSc7	pavilón
na	na	k7c6	na
výstavě	výstava	k1gFnSc6	výstava
EXPO	Expo	k1gNnSc1	Expo
v	v	k7c6	v
Barceloně	Barcelona	k1gFnSc6	Barcelona
(	(	kIx(	(
<g/>
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
za	za	k7c4	za
nejvýznačnější	význačný	k2eAgNnSc4d3	nejvýznačnější
předválečné	předválečný	k2eAgNnSc4d1	předválečné
dílo	dílo	k1gNnSc4	dílo
architekta	architekt	k1gMnSc2	architekt
Ludwiga	Ludwig	k1gMnSc2	Ludwig
Miese	Mies	k1gMnSc2	Mies
van	van	k1gInSc1	van
der	drát	k5eAaImRp2nS	drát
Rohe	Roh	k1gFnSc2	Roh
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
otců	otec	k1gMnPc2	otec
moderní	moderní	k2eAgFnSc2d1	moderní
architektury	architektura	k1gFnSc2	architektura
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
stavba	stavba	k1gFnSc1	stavba
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
spoluurčila	spoluurčit	k5eAaPmAgNnP	spoluurčit
nová	nový	k2eAgNnPc1d1	nové
měřítka	měřítko	k1gNnPc1	měřítko
moderního	moderní	k2eAgNnSc2d1	moderní
bydlení	bydlení	k1gNnSc2	bydlení
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
základním	základní	k2eAgInPc3d1	základní
dílům	díl	k1gInPc3	díl
světové	světový	k2eAgFnSc2d1	světová
moderní	moderní	k2eAgFnSc2d1	moderní
architektury	architektura	k1gFnSc2	architektura
-	-	kIx~	-
funkcionalismu	funkcionalismus	k1gInSc2	funkcionalismus
<g/>
.	.	kIx.	.
</s>
<s>
Výjimečné	výjimečný	k2eAgNnSc1d1	výjimečné
postavení	postavení	k1gNnSc1	postavení
vily	vila	k1gFnSc2	vila
je	být	k5eAaImIp3nS	být
dáno	dán	k2eAgNnSc1d1	dáno
řadou	řada	k1gFnSc7	řada
okolností	okolnost	k1gFnPc2	okolnost
<g/>
.	.	kIx.	.
</s>
<s>
Architekt	architekt	k1gMnSc1	architekt
van	vana	k1gFnPc2	vana
der	drát	k5eAaImRp2nS	drát
Rohe	Rohe	k1gNnSc1	Rohe
dostal	dostat	k5eAaPmAgMnS	dostat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
od	od	k7c2	od
investorů	investor	k1gMnPc2	investor
volnou	volný	k2eAgFnSc4d1	volná
ruku	ruka	k1gFnSc4	ruka
včetně	včetně	k7c2	včetně
finančního	finanční	k2eAgInSc2d1	finanční
rámce	rámec	k1gInSc2	rámec
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
přišla	přijít	k5eAaPmAgFnS	přijít
na	na	k7c4	na
pět	pět	k4xCc4	pět
milionů	milion	k4xCgInPc2	milion
prvorepublikových	prvorepublikový	k2eAgFnPc2d1	prvorepubliková
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
za	za	k7c4	za
tu	ten	k3xDgFnSc4	ten
cenu	cena	k1gFnSc4	cena
se	se	k3xPyFc4	se
dalo	dát	k5eAaPmAgNnS	dát
postavit	postavit	k5eAaPmF	postavit
30	[number]	k4	30
běžných	běžný	k2eAgInPc2d1	běžný
rodinných	rodinný	k2eAgInPc2d1	rodinný
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
dostatku	dostatek	k1gInSc3	dostatek
peněz	peníze	k1gInPc2	peníze
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
však	však	k9	však
mohly	moct	k5eAaImAgFnP	moct
použít	použít	k5eAaPmF	použít
unikátní	unikátní	k2eAgFnPc1d1	unikátní
technologie	technologie	k1gFnPc1	technologie
a	a	k8xC	a
moderní	moderní	k2eAgInPc1d1	moderní
materiály	materiál	k1gInPc1	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
architekti	architekt	k1gMnPc1	architekt
napodobují	napodobovat	k5eAaImIp3nP	napodobovat
zejména	zejména	k9	zejména
propojení	propojení	k1gNnSc4	propojení
zahrady	zahrada	k1gFnSc2	zahrada
a	a	k8xC	a
interiéru	interiér	k1gInSc2	interiér
<g/>
.	.	kIx.	.
</s>
<s>
Výjimečnost	výjimečnost	k1gFnSc1	výjimečnost
vily	vila	k1gFnSc2	vila
spočívá	spočívat	k5eAaImIp3nS	spočívat
i	i	k9	i
ve	v	k7c6	v
smutném	smutný	k2eAgInSc6d1	smutný
faktu	fakt	k1gInSc6	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
většina	většina	k1gFnSc1	většina
staveb	stavba	k1gFnPc2	stavba
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
van	van	k1gInSc1	van
der	drát	k5eAaImRp2nS	drát
Rohe	Roh	k1gFnSc2	Roh
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
zničena	zničit	k5eAaPmNgFnS	zničit
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
vila	vila	k1gFnSc1	vila
Tugendhat	Tugendhat	k1gFnSc2	Tugendhat
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
německý	německý	k2eAgInSc1d1	německý
pavilon	pavilon	k1gInSc1	pavilon
v	v	k7c6	v
Barceloně	Barcelona	k1gFnSc6	Barcelona
(	(	kIx(	(
<g/>
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
přitom	přitom	k6eAd1	přitom
pokládány	pokládán	k2eAgFnPc1d1	pokládána
za	za	k7c4	za
jeho	jeho	k3xOp3gNnPc4	jeho
nejlepší	dobrý	k2eAgNnPc4d3	nejlepší
předválečná	předválečný	k2eAgNnPc4d1	předválečné
díla	dílo	k1gNnPc4	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Známý	známý	k2eAgInSc1d1	známý
je	být	k5eAaImIp3nS	být
především	především	k9	především
prosklený	prosklený	k2eAgInSc4d1	prosklený
hlavní	hlavní	k2eAgInSc4d1	hlavní
obytný	obytný	k2eAgInSc4d1	obytný
prostor	prostor	k1gInSc4	prostor
vily	vila	k1gFnSc2	vila
Tugendhat	Tugendhat	k1gFnPc3	Tugendhat
se	s	k7c7	s
zimní	zimní	k2eAgFnSc7d1	zimní
zahradou	zahrada	k1gFnSc7	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Dům	dům	k1gInSc1	dům
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
převratný	převratný	k2eAgInSc4d1	převratný
z	z	k7c2	z
několika	několik	k4yIc2	několik
důvodů	důvod	k1gInPc2	důvod
<g/>
:	:	kIx,	:
obyvatelný	obyvatelný	k2eAgInSc1d1	obyvatelný
nepřerušovaný	přerušovaný	k2eNgInSc1d1	nepřerušovaný
prostor	prostor	k1gInSc1	prostor
okna	okno	k1gNnSc2	okno
hlavního	hlavní	k2eAgInSc2d1	hlavní
obytného	obytný	k2eAgInSc2d1	obytný
prostoru	prostor	k1gInSc2	prostor
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
zcela	zcela	k6eAd1	zcela
spustit	spustit	k5eAaPmF	spustit
do	do	k7c2	do
podlahy	podlaha	k1gFnSc2	podlaha
a	a	k8xC	a
splynutí	splynutí	k1gNnSc2	splynutí
interiéru	interiér	k1gInSc2	interiér
s	s	k7c7	s
přírodou	příroda	k1gFnSc7	příroda
v	v	k7c6	v
zahradě	zahrada	k1gFnSc6	zahrada
bylo	být	k5eAaImAgNnS	být
velmi	velmi	k6eAd1	velmi
působivé	působivý	k2eAgNnSc1d1	působivé
nosný	nosný	k2eAgInSc4d1	nosný
systém	systém	k1gInSc4	systém
budovy	budova	k1gFnSc2	budova
tvoří	tvořit	k5eAaImIp3nS	tvořit
ocelový	ocelový	k2eAgInSc1d1	ocelový
skelet	skelet	k1gInSc1	skelet
<g/>
,	,	kIx,	,
stropy	strop	k1gInPc1	strop
tudíž	tudíž	k8xC	tudíž
nenesou	nést	k5eNaImIp3nP	nést
zdi	zeď	k1gFnPc1	zeď
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
29	[number]	k4	29
ocelových	ocelový	k2eAgInPc2d1	ocelový
nýtovaných	nýtovaný	k2eAgInPc2d1	nýtovaný
sloupů	sloup	k1gInPc2	sloup
profilu	profil	k1gInSc2	profil
kříže	kříž	k1gInSc2	kříž
<g/>
,	,	kIx,	,
v	v	k7c6	v
obytných	obytný	k2eAgFnPc6d1	obytná
místnostech	místnost	k1gFnPc6	místnost
obaleny	obalen	k2eAgMnPc4d1	obalen
chromovaným	chromovaný	k2eAgInSc7d1	chromovaný
plechem	plech	k1gInSc7	plech
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
řešení	řešení	k1gNnSc1	řešení
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
rodinném	rodinný	k2eAgInSc6d1	rodinný
domě	dům	k1gInSc6	dům
použito	použít	k5eAaPmNgNnS	použít
poprvé	poprvé	k6eAd1	poprvé
<g/>
.	.	kIx.	.
</s>
<s>
Interiér	interiér	k1gInSc1	interiér
budovy	budova	k1gFnSc2	budova
byl	být	k5eAaImAgInS	být
taktéž	taktéž	k?	taktéž
vybaven	vybaven	k2eAgInSc1d1	vybaven
elegantním	elegantní	k2eAgInSc6d1	elegantní
a	a	k8xC	a
citlivě	citlivě	k6eAd1	citlivě
navrženým	navržený	k2eAgInSc7d1	navržený
nábytkem	nábytek	k1gInSc7	nábytek
od	od	k7c2	od
samotného	samotný	k2eAgMnSc2d1	samotný
architekta	architekt	k1gMnSc2	architekt
Ludwiga	Ludwig	k1gMnSc2	Ludwig
Miese	Mies	k1gMnSc2	Mies
van	van	k1gInSc1	van
der	drát	k5eAaImRp2nS	drát
Rohe	Roh	k1gInSc2	Roh
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
zde	zde	k6eAd1	zde
použil	použít	k5eAaPmAgMnS	použít
část	část	k1gFnSc4	část
nábytku	nábytek	k1gInSc2	nábytek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
pro	pro	k7c4	pro
německý	německý	k2eAgInSc4d1	německý
pavilon	pavilon	k1gInSc4	pavilon
na	na	k7c6	na
výstavě	výstava	k1gFnSc6	výstava
EXPO	Expo	k1gNnSc1	Expo
v	v	k7c6	v
Barceloně	Barcelona	k1gFnSc6	Barcelona
<g/>
.	.	kIx.	.
</s>
<s>
Soubor	soubor	k1gInSc1	soubor
čtyř	čtyři	k4xCgInPc2	čtyři
kusů	kus	k1gInPc2	kus
nábytku	nábytek	k1gInSc2	nábytek
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
umístěný	umístěný	k2eAgInSc4d1	umístěný
v	v	k7c6	v
Moravské	moravský	k2eAgFnSc6d1	Moravská
galerii	galerie	k1gFnSc6	galerie
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
k	k	k7c3	k
21	[number]	k4	21
<g/>
.	.	kIx.	.
únoru	únor	k1gInSc6	únor
2008	[number]	k4	2008
uznán	uznat	k5eAaPmNgMnS	uznat
za	za	k7c4	za
kulturní	kulturní	k2eAgFnSc4d1	kulturní
památku	památka	k1gFnSc4	památka
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
prostor	prostor	k1gInSc1	prostor
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
třemi	tři	k4xCgNnPc7	tři
patry	patro	k1gNnPc7	patro
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
že	že	k8xS	že
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
z	z	k7c2	z
ulice	ulice	k1gFnSc2	ulice
vila	vila	k1gFnSc1	vila
působí	působit	k5eAaImIp3nS	působit
jako	jako	k9	jako
přízemní	přízemní	k2eAgFnSc1d1	přízemní
jednopodlažní	jednopodlažní	k2eAgFnSc1d1	jednopodlažní
budova	budova	k1gFnSc1	budova
<g/>
.	.	kIx.	.
</s>
<s>
Horní	horní	k2eAgNnSc1d1	horní
patro	patro	k1gNnSc1	patro
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
přijímací	přijímací	k2eAgFnSc4d1	přijímací
halu	hala	k1gFnSc4	hala
<g/>
,	,	kIx,	,
oddělené	oddělený	k2eAgFnPc4d1	oddělená
ložnice	ložnice	k1gFnPc4	ložnice
manželů	manžel	k1gMnPc2	manžel
Tugendhatových	Tugendhatův	k2eAgMnPc2d1	Tugendhatův
(	(	kIx(	(
<g/>
se	s	k7c7	s
sociálním	sociální	k2eAgNnSc7d1	sociální
zařízením	zařízení	k1gNnSc7	zařízení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc4	dva
dětské	dětský	k2eAgInPc4d1	dětský
pokoje	pokoj	k1gInPc4	pokoj
a	a	k8xC	a
pokoj	pokoj	k1gInSc4	pokoj
vychovatelky	vychovatelka	k1gFnSc2	vychovatelka
(	(	kIx(	(
<g/>
druhé	druhý	k4xOgNnSc4	druhý
sociální	sociální	k2eAgNnSc4d1	sociální
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
)	)	kIx)	)
a	a	k8xC	a
rozlehlá	rozlehlý	k2eAgFnSc1d1	rozlehlá
terasa	terasa	k1gFnSc1	terasa
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
hornímu	horní	k2eAgNnSc3d1	horní
patru	patro	k1gNnSc3	patro
přiléhá	přiléhat	k5eAaImIp3nS	přiléhat
také	také	k9	také
byt	byt	k1gInSc1	byt
správce	správce	k1gMnSc2	správce
a	a	k8xC	a
garáž	garáž	k1gFnSc4	garáž
pro	pro	k7c4	pro
dvě	dva	k4xCgNnPc4	dva
auta	auto	k1gNnPc4	auto
<g/>
.	.	kIx.	.
</s>
<s>
Konstrukci	konstrukce	k1gFnSc4	konstrukce
vily	vila	k1gFnSc2	vila
tvoří	tvořit	k5eAaImIp3nS	tvořit
ocelový	ocelový	k2eAgInSc1d1	ocelový
skelet	skelet	k1gInSc1	skelet
<g/>
,	,	kIx,	,
železobetonové	železobetonový	k2eAgInPc1d1	železobetonový
stropy	strop	k1gInPc1	strop
a	a	k8xC	a
cihelné	cihelný	k2eAgNnSc1d1	cihelné
zdivo	zdivo	k1gNnSc1	zdivo
<g/>
.	.	kIx.	.
</s>
<s>
Terasy	terasa	k1gFnPc1	terasa
a	a	k8xC	a
schodiště	schodiště	k1gNnSc1	schodiště
mají	mít	k5eAaImIp3nP	mít
travertinové	travertinový	k2eAgInPc1d1	travertinový
obklady	obklad	k1gInPc1	obklad
<g/>
.	.	kIx.	.
</s>
<s>
Nosné	nosný	k2eAgInPc1d1	nosný
ocelové	ocelový	k2eAgInPc1d1	ocelový
pilíře	pilíř	k1gInPc1	pilíř
architekt	architekt	k1gMnSc1	architekt
nijak	nijak	k6eAd1	nijak
neskrýval	skrývat	k5eNaImAgMnS	skrývat
<g/>
,	,	kIx,	,
jen	jen	k9	jen
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
obytné	obytný	k2eAgFnSc6d1	obytná
části	část	k1gFnSc6	část
nechal	nechat	k5eAaPmAgMnS	nechat
obalit	obalit	k5eAaPmF	obalit
chromovanými	chromovaný	k2eAgFnPc7d1	chromovaná
krytkami	krytka	k1gFnPc7	krytka
<g/>
.	.	kIx.	.
</s>
<s>
Prostřední	prostřední	k2eAgNnSc1d1	prostřední
patro	patro	k1gNnSc1	patro
sloužilo	sloužit	k5eAaImAgNnS	sloužit
jako	jako	k8xS	jako
obytná	obytný	k2eAgFnSc1d1	obytná
a	a	k8xC	a
reprezentativní	reprezentativní	k2eAgFnSc1d1	reprezentativní
část	část	k1gFnSc1	část
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
volný	volný	k2eAgInSc4d1	volný
prostor	prostor	k1gInSc4	prostor
náznakově	náznakově	k6eAd1	náznakově
dělený	dělený	k2eAgInSc4d1	dělený
závěsy	závěs	k1gInPc4	závěs
<g/>
,	,	kIx,	,
jednou	jednou	k6eAd1	jednou
dřevěnou	dřevěný	k2eAgFnSc7d1	dřevěná
půlkruhovou	půlkruhový	k2eAgFnSc7d1	půlkruhová
stěnou	stěna	k1gFnSc7	stěna
(	(	kIx(	(
<g/>
oddělující	oddělující	k2eAgInSc1d1	oddělující
jídelní	jídelní	k2eAgInSc1d1	jídelní
prostor	prostor	k1gInSc1	prostor
<g/>
)	)	kIx)	)
a	a	k8xC	a
jednou	jeden	k4xCgFnSc7	jeden
onyxovou	onyxový	k2eAgFnSc7d1	onyxová
stěnou	stěna	k1gFnSc7	stěna
(	(	kIx(	(
<g/>
oddělující	oddělující	k2eAgInSc1d1	oddělující
pracovní	pracovní	k2eAgInSc1d1	pracovní
prostor	prostor	k1gInSc1	prostor
od	od	k7c2	od
obývacího	obývací	k2eAgInSc2d1	obývací
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Onyxová	onyxový	k2eAgFnSc1d1	onyxová
stěna	stěna	k1gFnSc1	stěna
má	mít	k5eAaImIp3nS	mít
tu	ten	k3xDgFnSc4	ten
jedinečnou	jedinečný	k2eAgFnSc4d1	jedinečná
vlastnost	vlastnost	k1gFnSc4	vlastnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
pod	pod	k7c7	pod
určitým	určitý	k2eAgInSc7d1	určitý
úhlem	úhel	k1gInSc7	úhel
propouští	propouštět	k5eAaImIp3nP	propouštět
sluneční	sluneční	k2eAgInPc1d1	sluneční
paprsky	paprsek	k1gInPc1	paprsek
a	a	k8xC	a
mění	měnit	k5eAaImIp3nP	měnit
se	se	k3xPyFc4	se
tak	tak	k9	tak
její	její	k3xOp3gFnSc1	její
barva	barva	k1gFnSc1	barva
<g/>
.	.	kIx.	.
</s>
<s>
Pořízení	pořízení	k1gNnSc1	pořízení
onyxové	onyxový	k2eAgFnSc2d1	onyxová
stěny	stěna	k1gFnSc2	stěna
bylo	být	k5eAaImAgNnS	být
však	však	k9	však
velmi	velmi	k6eAd1	velmi
nákladné	nákladný	k2eAgNnSc1d1	nákladné
-	-	kIx~	-
stěna	stěna	k1gFnSc1	stěna
stála	stát	k5eAaImAgFnS	stát
200	[number]	k4	200
000	[number]	k4	000
prvorepublikových	prvorepublikový	k2eAgFnPc2d1	prvorepubliková
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
prostředního	prostřední	k2eAgNnSc2d1	prostřední
patra	patro	k1gNnSc2	patro
je	být	k5eAaImIp3nS	být
i	i	k9	i
skleněnou	skleněný	k2eAgFnSc7d1	skleněná
stěnou	stěna	k1gFnSc7	stěna
oddělená	oddělený	k2eAgFnSc1d1	oddělená
zimní	zimní	k2eAgFnSc1d1	zimní
zahrada	zahrada	k1gFnSc1	zahrada
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
své	svůj	k3xOyFgNnSc4	svůj
vlastní	vlastní	k2eAgNnSc4d1	vlastní
mikroklima	mikroklima	k1gNnSc4	mikroklima
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
přední	přední	k2eAgFnSc1d1	přední
strana	strana	k1gFnSc1	strana
pokoje	pokoj	k1gInSc2	pokoj
je	být	k5eAaImIp3nS	být
prosklená	prosklený	k2eAgFnSc1d1	prosklená
sedmi	sedm	k4xCc7	sedm
velkými	velký	k2eAgFnPc7d1	velká
tabulemi	tabule	k1gFnPc7	tabule
velmi	velmi	k6eAd1	velmi
průzračného	průzračný	k2eAgNnSc2d1	průzračné
skla	sklo	k1gNnSc2	sklo
o	o	k7c6	o
rozměrech	rozměr	k1gInPc6	rozměr
5	[number]	k4	5
x	x	k?	x
3,5	[number]	k4	3,5
metru	metro	k1gNnSc6	metro
a	a	k8xC	a
síle	síla	k1gFnSc6	síla
9,8	[number]	k4	9,8
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
tabulí	tabule	k1gFnPc2	tabule
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
posouvat	posouvat	k5eAaImF	posouvat
elektromotory	elektromotor	k1gInPc4	elektromotor
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
je	být	k5eAaImIp3nS	být
celé	celý	k2eAgNnSc1d1	celé
zasunout	zasunout	k5eAaPmF	zasunout
pod	pod	k7c4	pod
podlahu	podlaha	k1gFnSc4	podlaha
a	a	k8xC	a
otevřít	otevřít	k5eAaPmF	otevřít
tak	tak	k9	tak
pokoj	pokoj	k1gInSc4	pokoj
do	do	k7c2	do
zahrady	zahrada	k1gFnSc2	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Spodní	spodní	k2eAgNnSc1d1	spodní
patro	patro	k1gNnSc1	patro
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
přístupné	přístupný	k2eAgFnSc6d1	přístupná
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
rozšířeného	rozšířený	k2eAgInSc2d1	rozšířený
návštěvního	návštěvní	k2eAgInSc2d1	návštěvní
okruhu	okruh	k1gInSc2	okruh
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
vyhrazeno	vyhradit	k5eAaPmNgNnS	vyhradit
služebnictvu	služebnictvo	k1gNnSc3	služebnictvo
a	a	k8xC	a
technickému	technický	k2eAgNnSc3d1	technické
zázemí	zázemí	k1gNnSc3	zázemí
(	(	kIx(	(
<g/>
strojovna	strojovna	k1gFnSc1	strojovna
vzduchotechniky	vzduchotechnika	k1gFnSc2	vzduchotechnika
<g/>
,	,	kIx,	,
kotelna	kotelna	k1gFnSc1	kotelna
<g/>
,	,	kIx,	,
strojovna	strojovna	k1gFnSc1	strojovna
pro	pro	k7c4	pro
elektrické	elektrický	k2eAgNnSc4d1	elektrické
spouštění	spouštění	k1gNnSc4	spouštění
oken	okno	k1gNnPc2	okno
<g/>
,	,	kIx,	,
prádelna	prádelna	k1gFnSc1	prádelna
<g/>
,	,	kIx,	,
fotokomora	fotokomora	k1gFnSc1	fotokomora
<g/>
,	,	kIx,	,
místnost	místnost	k1gFnSc1	místnost
pro	pro	k7c4	pro
kožichy	kožich	k1gInPc4	kožich
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vytápění	vytápění	k1gNnSc1	vytápění
prostředního	prostřední	k2eAgNnSc2d1	prostřední
patra	patro	k1gNnSc2	patro
bylo	být	k5eAaImAgNnS	být
na	na	k7c4	na
tehdejší	tehdejší	k2eAgFnSc4d1	tehdejší
dobu	doba	k1gFnSc4	doba
pokrokovým	pokrokový	k2eAgMnSc7d1	pokrokový
-	-	kIx~	-
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
horního	horní	k2eAgNnSc2d1	horní
a	a	k8xC	a
spodního	spodní	k2eAgNnSc2d1	spodní
patra	patro	k1gNnSc2	patro
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgInP	být
normální	normální	k2eAgInPc1d1	normální
radiátory	radiátor	k1gInPc1	radiátor
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
prostřední	prostřední	k2eAgNnSc1d1	prostřední
patro	patro	k1gNnSc1	patro
vytápělo	vytápět	k5eAaImAgNnS	vytápět
horkým	horký	k2eAgInSc7d1	horký
vzduchem	vzduch	k1gInSc7	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Horký	horký	k2eAgInSc1d1	horký
vzduch	vzduch	k1gInSc1	vzduch
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
pomocí	pomocí	k7c2	pomocí
technického	technický	k2eAgNnSc2d1	technické
zařízení	zařízení	k1gNnSc2	zařízení
<g/>
,	,	kIx,	,
umístěného	umístěný	k2eAgNnSc2d1	umístěné
ve	v	k7c6	v
zvlášť	zvlášť	k6eAd1	zvlášť
vyhrazené	vyhrazený	k2eAgFnSc6d1	vyhrazená
místnosti	místnost	k1gFnSc6	místnost
ve	v	k7c6	v
spodním	spodní	k2eAgNnSc6d1	spodní
patře	patro	k1gNnSc6	patro
<g/>
,	,	kIx,	,
nejdříve	dříve	k6eAd3	dříve
smísit	smísit	k5eAaPmF	smísit
s	s	k7c7	s
čerstvým	čerstvý	k2eAgInSc7d1	čerstvý
vzduchem	vzduch	k1gInSc7	vzduch
přiváděným	přiváděný	k2eAgInSc7d1	přiváděný
průduchem	průduch	k1gInSc7	průduch
z	z	k7c2	z
technické	technický	k2eAgFnSc2d1	technická
terasy	terasa	k1gFnSc2	terasa
nebo	nebo	k8xC	nebo
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
s	s	k7c7	s
chladným	chladný	k2eAgInSc7d1	chladný
vzduchem	vzduch	k1gInSc7	vzduch
z	z	k7c2	z
chladící	chladící	k2eAgFnSc2d1	chladící
místnosti	místnost	k1gFnSc2	místnost
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
ochlazován	ochlazovat	k5eAaImNgInS	ochlazovat
vodou	voda	k1gFnSc7	voda
polévanými	polévaný	k2eAgInPc7d1	polévaný
mořskými	mořský	k2eAgInPc7d1	mořský
kameny	kámen	k1gInPc7	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Smíšený	smíšený	k2eAgInSc1d1	smíšený
vzduch	vzduch	k1gInSc1	vzduch
byl	být	k5eAaImAgInS	být
poté	poté	k6eAd1	poté
zbaven	zbaven	k2eAgMnSc1d1	zbaven
nečistot	nečistota	k1gFnPc2	nečistota
v	v	k7c6	v
olejovém	olejový	k2eAgInSc6d1	olejový
filtru	filtr	k1gInSc6	filtr
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
"	"	kIx"	"
<g/>
provoněn	provoněn	k2eAgInSc1d1	provoněn
<g/>
"	"	kIx"	"
ve	v	k7c6	v
filtru	filtr	k1gInSc6	filtr
z	z	k7c2	z
cedrových	cedrový	k2eAgFnPc2d1	cedrová
hoblin	hoblina	k1gFnPc2	hoblina
a	a	k8xC	a
pak	pak	k6eAd1	pak
vháněn	vhánět	k5eAaImNgInS	vhánět
do	do	k7c2	do
obytného	obytný	k2eAgNnSc2d1	obytné
patra	patro	k1gNnSc2	patro
průduchy	průduch	k1gInPc1	průduch
v	v	k7c6	v
podlaze	podlaha	k1gFnSc6	podlaha
a	a	k8xC	a
ve	v	k7c6	v
stěnách	stěna	k1gFnPc6	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
spodním	spodní	k2eAgNnSc6d1	spodní
patře	patro	k1gNnSc6	patro
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
vystaveny	vystaven	k2eAgFnPc1d1	vystavena
fotografie	fotografia	k1gFnPc1	fotografia
Fritze	Fritze	k1gFnSc1	Fritze
Tugendhata	Tugendhata	k1gFnSc1	Tugendhata
dokumentující	dokumentující	k2eAgFnSc4d1	dokumentující
stavbu	stavba	k1gFnSc4	stavba
a	a	k8xC	a
zařízení	zařízení	k1gNnSc4	zařízení
vily	vila	k1gFnSc2	vila
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
Tugendhatova	Tugendhatův	k2eAgFnSc1d1	Tugendhatova
záliba	záliba	k1gFnSc1	záliba
ve	v	k7c6	v
fotografování	fotografování	k1gNnSc6	fotografování
byla	být	k5eAaImAgFnS	být
důležitým	důležitý	k2eAgInSc7d1	důležitý
zdrojem	zdroj	k1gInSc7	zdroj
pro	pro	k7c4	pro
původní	původní	k2eAgInSc4d1	původní
vzhled	vzhled	k1gInSc4	vzhled
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterého	který	k3yRgNnSc2	který
se	se	k3xPyFc4	se
orientovaly	orientovat	k5eAaBmAgInP	orientovat
návrh	návrh	k1gInSc4	návrh
a	a	k8xC	a
provedení	provedení	k1gNnSc4	provedení
rekonstrukce	rekonstrukce	k1gFnSc2	rekonstrukce
vily	vila	k1gFnSc2	vila
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2010	[number]	k4	2010
<g/>
-	-	kIx~	-
<g/>
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Architektonický	architektonický	k2eAgInSc1d1	architektonický
návrh	návrh	k1gInSc1	návrh
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1928	[number]	k4	1928
německý	německý	k2eAgMnSc1d1	německý
architekt	architekt	k1gMnSc1	architekt
Ludwig	Ludwig	k1gMnSc1	Ludwig
Mies	Miesa	k1gFnPc2	Miesa
van	van	k1gInSc4	van
der	drát	k5eAaImRp2nS	drát
Rohe	Roh	k1gFnSc2	Roh
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
na	na	k7c4	na
pozvání	pozvání	k1gNnSc4	pozvání
manželů	manžel	k1gMnPc2	manžel
Tugendhatových	Tugendhatův	k2eAgFnPc2d1	Tugendhatova
přijel	přijet	k5eAaPmAgMnS	přijet
do	do	k7c2	do
Brna	Brno	k1gNnSc2	Brno
v	v	k7c6	v
září	září	k1gNnSc6	září
zmíněného	zmíněný	k2eAgInSc2d1	zmíněný
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
(	(	kIx(	(
<g/>
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
generální	generální	k2eAgMnSc1d1	generální
dodavatel	dodavatel	k1gMnSc1	dodavatel
byla	být	k5eAaImAgFnS	být
zvolena	zvolit	k5eAaPmNgFnS	zvolit
brněnská	brněnský	k2eAgFnSc1d1	brněnská
firma	firma	k1gFnSc1	firma
Mořice	Mořic	k1gMnSc2	Mořic
a	a	k8xC	a
Artura	Artur	k1gMnSc2	Artur
Eislerových	Eislerových	k2eAgMnSc2d1	Eislerových
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
stavbě	stavba	k1gFnSc6	stavba
se	se	k3xPyFc4	se
podílela	podílet	k5eAaImAgFnS	podílet
řada	řada	k1gFnSc1	řada
domácích	domácí	k2eAgFnPc2d1	domácí
i	i	k8xC	i
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
firem	firma	k1gFnPc2	firma
<g/>
,	,	kIx,	,
přírodní	přírodní	k2eAgInSc1d1	přírodní
materiál	materiál	k1gInSc1	materiál
se	se	k3xPyFc4	se
dovážel	dovážet	k5eAaImAgInS	dovážet
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
koutů	kout	k1gInPc2	kout
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Stavbu	stavba	k1gFnSc4	stavba
financoval	financovat	k5eAaBmAgMnS	financovat
otec	otec	k1gMnSc1	otec
Grety	Greta	k1gFnSc2	Greta
Tugendhat	Tugendhat	k1gFnSc2	Tugendhat
a	a	k8xC	a
architektovi	architekt	k1gMnSc3	architekt
nestanovil	stanovit	k5eNaPmAgInS	stanovit
žádný	žádný	k3yNgInSc4	žádný
finanční	finanční	k2eAgInSc4d1	finanční
limit	limit	k1gInSc4	limit
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
stavba	stavba	k1gFnSc1	stavba
domu	dům	k1gInSc2	dům
včetně	včetně	k7c2	včetně
vybavení	vybavení	k1gNnSc2	vybavení
a	a	k8xC	a
zahrady	zahrada	k1gFnSc2	zahrada
přišla	přijít	k5eAaPmAgFnS	přijít
na	na	k7c4	na
pět	pět	k4xCc4	pět
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Vila	vila	k1gFnSc1	vila
byla	být	k5eAaImAgFnS	být
dostavěna	dostavět	k5eAaPmNgFnS	dostavět
v	v	k7c6	v
rekordně	rekordně	k6eAd1	rekordně
krátké	krátký	k2eAgFnSc6d1	krátká
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
stavba	stavba	k1gFnSc1	stavba
trvala	trvalo	k1gNnSc2	trvalo
pouhých	pouhý	k2eAgInPc2d1	pouhý
14	[number]	k4	14
měsíců	měsíc	k1gInPc2	měsíc
a	a	k8xC	a
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1930	[number]	k4	1930
se	se	k3xPyFc4	se
manželé	manžel	k1gMnPc1	manžel
Tugendhatovi	Tugendhatův	k2eAgMnPc1d1	Tugendhatův
již	již	k6eAd1	již
mohli	moct	k5eAaImAgMnP	moct
do	do	k7c2	do
vily	vila	k1gFnSc2	vila
nastěhovat	nastěhovat	k5eAaPmF	nastěhovat
<g/>
.	.	kIx.	.
</s>
<s>
Manželé	manžel	k1gMnPc1	manžel
Tugendhatovi	Tugendhatův	k2eAgMnPc1d1	Tugendhatův
obývali	obývat	k5eAaImAgMnP	obývat
vilu	vila	k1gFnSc4	vila
pouze	pouze	k6eAd1	pouze
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jakožto	jakožto	k8xS	jakožto
Židé	Žid	k1gMnPc1	Žid
v	v	k7c6	v
obavě	obava	k1gFnSc6	obava
o	o	k7c4	o
svůj	svůj	k3xOyFgInSc4	svůj
další	další	k2eAgInSc4d1	další
osud	osud	k1gInSc4	osud
opustili	opustit	k5eAaPmAgMnP	opustit
před	před	k7c7	před
hrozbou	hrozba	k1gFnSc7	hrozba
německé	německý	k2eAgFnSc2d1	německá
expanze	expanze	k1gFnSc2	expanze
Československou	československý	k2eAgFnSc4d1	Československá
republiku	republika	k1gFnSc4	republika
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
odjeli	odjet	k5eAaPmAgMnP	odjet
relativně	relativně	k6eAd1	relativně
brzy	brzy	k6eAd1	brzy
<g/>
,	,	kIx,	,
ještě	ještě	k9	ještě
před	před	k7c7	před
Mnichovskou	mnichovský	k2eAgFnSc7d1	Mnichovská
dohodou	dohoda	k1gFnSc7	dohoda
<g/>
,	,	kIx,	,
stačili	stačit	k5eAaBmAgMnP	stačit
si	se	k3xPyFc3	se
s	s	k7c7	s
sebou	se	k3xPyFc7	se
odvézt	odvézt	k5eAaPmF	odvézt
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
vnitřního	vnitřní	k2eAgNnSc2d1	vnitřní
vybavení	vybavení	k1gNnSc2	vybavení
svého	svůj	k3xOyFgInSc2	svůj
domu	dům	k1gInSc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Samotná	samotný	k2eAgFnSc1d1	samotná
budova	budova	k1gFnSc1	budova
se	se	k3xPyFc4	se
nevyhnula	vyhnout	k5eNaPmAgFnS	vyhnout
obsazení	obsazení	k1gNnSc1	obsazení
německými	německý	k2eAgMnPc7d1	německý
okupačními	okupační	k2eAgMnPc7d1	okupační
úřady	úřada	k1gMnPc7	úřada
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
ji	on	k3xPp3gFnSc4	on
zabavilo	zabavit	k5eAaPmAgNnS	zabavit
gestapo	gestapo	k1gNnSc1	gestapo
a	a	k8xC	a
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
dokonce	dokonce	k9	dokonce
zapsána	zapsat	k5eAaPmNgFnS	zapsat
jako	jako	k8xS	jako
majetek	majetek	k1gInSc1	majetek
Velkoněmecké	velkoněmecký	k2eAgFnSc2d1	Velkoněmecká
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1942	[number]	k4	1942
<g/>
-	-	kIx~	-
<g/>
1945	[number]	k4	1945
měl	mít	k5eAaImAgInS	mít
vilu	vila	k1gFnSc4	vila
pronajatou	pronajatý	k2eAgFnSc4d1	pronajatá
Willy	Willa	k1gFnPc1	Willa
Messerschmitt	Messerschmitt	k1gMnSc1	Messerschmitt
<g/>
,	,	kIx,	,
ředitel	ředitel	k1gMnSc1	ředitel
Klöcknerwerke	Klöcknerwerk	k1gFnSc2	Klöcknerwerk
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ji	on	k3xPp3gFnSc4	on
využíval	využívat	k5eAaPmAgInS	využívat
jako	jako	k8xS	jako
byt	byt	k1gInSc1	byt
a	a	k8xC	a
kancelář	kancelář	k1gFnSc1	kancelář
<g/>
.	.	kIx.	.
</s>
<s>
Inicioval	iniciovat	k5eAaBmAgMnS	iniciovat
první	první	k4xOgFnPc4	první
zásadní	zásadní	k2eAgFnPc4d1	zásadní
stavební	stavební	k2eAgFnPc4d1	stavební
úpravy	úprava	k1gFnPc4	úprava
(	(	kIx(	(
<g/>
zvýšení	zvýšení	k1gNnSc4	zvýšení
komína	komín	k1gInSc2	komín
<g/>
,	,	kIx,	,
zazdění	zazdění	k1gNnSc4	zazdění
mléčného	mléčný	k2eAgNnSc2d1	mléčné
skla	sklo	k1gNnSc2	sklo
ve	v	k7c6	v
vstupní	vstupní	k2eAgFnSc6d1	vstupní
hale	hala	k1gFnSc6	hala
a	a	k8xC	a
průchodu	průchod	k1gInSc6	průchod
na	na	k7c4	na
horní	horní	k2eAgFnSc4d1	horní
terasu	terasa	k1gFnSc4	terasa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
války	válka	k1gFnSc2	válka
byla	být	k5eAaImAgFnS	být
také	také	k9	také
odstraněna	odstraněn	k2eAgFnSc1d1	odstraněna
zaoblená	zaoblený	k2eAgFnSc1d1	zaoblená
stěna	stěna	k1gFnSc1	stěna
z	z	k7c2	z
makassarského	makassarský	k2eAgInSc2d1	makassarský
ebenu	eben	k1gInSc2	eben
<g/>
,	,	kIx,	,
vytvářející	vytvářející	k2eAgInSc4d1	vytvářející
jídelní	jídelní	k2eAgInSc4d1	jídelní
kout	kout	k1gInSc4	kout
vily	vila	k1gFnSc2	vila
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
zabrala	zabrat	k5eAaPmAgFnS	zabrat
vilu	vila	k1gFnSc4	vila
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
a	a	k8xC	a
použila	použít	k5eAaPmAgFnS	použít
ji	on	k3xPp3gFnSc4	on
k	k	k7c3	k
ubytování	ubytování	k1gNnSc3	ubytování
sovětských	sovětský	k2eAgMnPc2d1	sovětský
vojáků	voják	k1gMnPc2	voják
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
včetně	včetně	k7c2	včetně
jejich	jejich	k3xOp3gMnPc2	jejich
koní	kůň	k1gMnPc2	kůň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
byla	být	k5eAaImAgFnS	být
zničena	zničit	k5eAaPmNgFnS	zničit
převážná	převážný	k2eAgFnSc1d1	převážná
část	část	k1gFnSc1	část
zbylého	zbylý	k2eAgNnSc2d1	zbylé
vybavení	vybavení	k1gNnSc2	vybavení
a	a	k8xC	a
mobiliáře	mobiliář	k1gInSc2	mobiliář
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
police	police	k1gFnSc1	police
knihovny	knihovna	k1gFnSc2	knihovna
z	z	k7c2	z
exotického	exotický	k2eAgNnSc2d1	exotické
dřeva	dřevo	k1gNnSc2	dřevo
posloužily	posloužit	k5eAaPmAgInP	posloužit
za	za	k7c4	za
otop	otop	k1gInSc4	otop
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
ztráta	ztráta	k1gFnSc1	ztráta
však	však	k9	však
nastala	nastat	k5eAaPmAgFnS	nastat
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
při	při	k7c6	při
náletu	nálet	k1gInSc6	nálet
na	na	k7c4	na
Brno	Brno	k1gNnSc4	Brno
výbuchem	výbuch	k1gInSc7	výbuch
pumy	puma	k1gFnSc2	puma
zničena	zničit	k5eAaPmNgNnP	zničit
všechna	všechen	k3xTgNnPc4	všechen
okna	okno	k1gNnPc1	okno
hlavního	hlavní	k2eAgInSc2d1	hlavní
obytného	obytný	k2eAgInSc2d1	obytný
prostoru	prostor	k1gInSc2	prostor
kromě	kromě	k7c2	kromě
jednoho	jeden	k4xCgNnSc2	jeden
<g/>
,	,	kIx,	,
které	který	k3yQgFnSc3	který
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
zřejmě	zřejmě	k6eAd1	zřejmě
zasunuto	zasunut	k2eAgNnSc1d1	zasunuto
do	do	k7c2	do
podlahy	podlaha	k1gFnSc2	podlaha
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
koncepčně	koncepčně	k6eAd1	koncepčně
důležitá	důležitý	k2eAgFnSc1d1	důležitá
část	část	k1gFnSc1	část
vily	vila	k1gFnSc2	vila
byla	být	k5eAaImAgFnS	být
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
provizorním	provizorní	k2eAgNnSc7d1	provizorní
zasklením	zasklení	k1gNnSc7	zasklení
z	z	k7c2	z
malých	malý	k2eAgFnPc2d1	malá
skleněných	skleněný	k2eAgFnPc2d1	skleněná
tabulek	tabulka	k1gFnPc2	tabulka
do	do	k7c2	do
nově	nově	k6eAd1	nově
vložených	vložený	k2eAgInPc2d1	vložený
dřevěných	dřevěný	k2eAgInPc2d1	dřevěný
rámů	rám	k1gInPc2	rám
<g/>
.	.	kIx.	.
</s>
<s>
Technická	technický	k2eAgFnSc1d1	technická
struktura	struktura	k1gFnSc1	struktura
stavby	stavba	k1gFnSc2	stavba
však	však	k9	však
zůstala	zůstat	k5eAaPmAgFnS	zůstat
i	i	k9	i
přes	přes	k7c4	přes
válečné	válečný	k2eAgFnPc4d1	válečná
újmy	újma	k1gFnPc4	újma
zachována	zachován	k2eAgFnSc1d1	zachována
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
osvobození	osvobození	k1gNnSc6	osvobození
vila	vila	k1gFnSc1	vila
sloužila	sloužit	k5eAaImAgFnS	sloužit
jako	jako	k9	jako
soukromá	soukromý	k2eAgFnSc1d1	soukromá
škola	škola	k1gFnSc1	škola
rytmiky	rytmika	k1gFnSc2	rytmika
a	a	k8xC	a
taneční	taneční	k2eAgFnSc2d1	taneční
gymnastiky	gymnastika	k1gFnSc2	gymnastika
paní	paní	k1gFnSc2	paní
Karly	Karla	k1gFnSc2	Karla
Hladké	Hladká	k1gFnSc2	Hladká
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
let	léto	k1gNnPc2	léto
1945	[number]	k4	1945
<g/>
-	-	kIx~	-
<g/>
1950	[number]	k4	1950
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
vila	vila	k1gFnSc1	vila
předána	předat	k5eAaPmNgFnS	předat
Státnímu	státní	k2eAgMnSc3d1	státní
ústavu	ústav	k1gInSc6	ústav
léčebného	léčebný	k2eAgInSc2d1	léčebný
tělocviku	tělocvik	k1gInSc2	tělocvik
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zde	zde	k6eAd1	zde
prováděl	provádět	k5eAaImAgInS	provádět
rehabilitaci	rehabilitace	k1gFnSc4	rehabilitace
dětí	dítě	k1gFnPc2	dítě
s	s	k7c7	s
vadami	vada	k1gFnPc7	vada
páteře	páteř	k1gFnSc2	páteř
a	a	k8xC	a
špatným	špatný	k2eAgNnSc7d1	špatné
držením	držení	k1gNnSc7	držení
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
ozývaly	ozývat	k5eAaImAgInP	ozývat
hlasy	hlas	k1gInPc1	hlas
o	o	k7c6	o
významu	význam	k1gInSc6	význam
vily	vila	k1gFnSc2	vila
jako	jako	k8xS	jako
památky	památka	k1gFnSc2	památka
moderní	moderní	k2eAgFnSc2d1	moderní
architektury	architektura	k1gFnSc2	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
hlasy	hlas	k1gInPc1	hlas
byly	být	k5eAaImAgInP	být
vyslyšeny	vyslyšen	k2eAgInPc1d1	vyslyšen
6	[number]	k4	6
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
dům	dům	k1gInSc1	dům
zapsán	zapsat	k5eAaPmNgInS	zapsat
do	do	k7c2	do
Státního	státní	k2eAgInSc2d1	státní
seznamu	seznam	k1gInSc2	seznam
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
stavební	stavební	k2eAgFnSc1d1	stavební
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
již	již	k6eAd1	již
tenkrát	tenkrát	k6eAd1	tenkrát
byla	být	k5eAaImAgFnS	být
často	často	k6eAd1	často
kritizována	kritizovat	k5eAaImNgFnS	kritizovat
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
kritika	kritika	k1gFnSc1	kritika
se	se	k3xPyFc4	se
týkala	týkat	k5eAaImAgFnS	týkat
hlavně	hlavně	k9	hlavně
nedůslednosti	nedůslednost	k1gFnPc4	nedůslednost
při	při	k7c6	při
zasklívání	zasklívání	k1gNnSc6	zasklívání
hlavního	hlavní	k2eAgInSc2d1	hlavní
obytného	obytný	k2eAgInSc2d1	obytný
prostoru	prostor	k1gInSc2	prostor
(	(	kIx(	(
<g/>
nepodařilo	podařit	k5eNaPmAgNnS	podařit
se	se	k3xPyFc4	se
sehnat	sehnat	k5eAaPmF	sehnat
patřičně	patřičně	k6eAd1	patřičně
veliké	veliký	k2eAgFnPc4d1	veliká
tabule	tabule	k1gFnPc4	tabule
skla	sklo	k1gNnSc2	sklo
<g/>
,	,	kIx,	,
nová	nový	k2eAgNnPc1d1	nové
skla	sklo	k1gNnPc1	sklo
byla	být	k5eAaImAgNnP	být
slepena	slepit	k5eAaBmNgNnP	slepit
silikonem	silikon	k1gInSc7	silikon
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
dílů	díl	k1gInPc2	díl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Použita	použit	k2eAgFnSc1d1	použita
byla	být	k5eAaImAgFnS	být
zcela	zcela	k6eAd1	zcela
běžná	běžný	k2eAgNnPc4d1	běžné
skla	sklo	k1gNnPc4	sklo
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
původních	původní	k2eAgInPc2d1	původní
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
leštěná	leštěný	k2eAgFnSc1d1	leštěná
unikátní	unikátní	k2eAgFnSc1d1	unikátní
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
nákladnou	nákladný	k2eAgFnSc7d1	nákladná
technikou	technika	k1gFnSc7	technika
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
prakticky	prakticky	k6eAd1	prakticky
neleskla	lesknout	k5eNaImAgFnS	lesknout
<g/>
.	.	kIx.	.
</s>
<s>
Jediné	jediný	k2eAgNnSc1d1	jediné
dochované	dochovaný	k2eAgNnSc1d1	dochované
původní	původní	k2eAgNnSc1d1	původní
pohyblivé	pohyblivý	k2eAgNnSc1d1	pohyblivé
sklo	sklo	k1gNnSc1	sklo
bylo	být	k5eAaImAgNnS	být
zničeno	zničit	k5eAaPmNgNnS	zničit
<g/>
.	.	kIx.	.
</s>
<s>
Zničeno	zničen	k2eAgNnSc1d1	zničeno
bylo	být	k5eAaImAgNnS	být
také	také	k6eAd1	také
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
dochované	dochovaný	k2eAgNnSc1d1	dochované
zařízení	zařízení	k1gNnSc1	zařízení
koupelny	koupelna	k1gFnSc2	koupelna
(	(	kIx(	(
<g/>
kovové	kovový	k2eAgFnPc4d1	kovová
baterie	baterie	k1gFnPc4	baterie
<g/>
,	,	kIx,	,
umyvadla	umyvadlo	k1gNnPc4	umyvadlo
<g/>
,	,	kIx,	,
obkladačky	obkladačka	k1gFnPc4	obkladačka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Chromované	chromovaný	k2eAgInPc1d1	chromovaný
doplňky	doplněk	k1gInPc1	doplněk
a	a	k8xC	a
osvětlovací	osvětlovací	k2eAgNnPc1d1	osvětlovací
tělesa	těleso	k1gNnPc1	těleso
byly	být	k5eAaImAgFnP	být
nahrazeny	nahradit	k5eAaPmNgFnP	nahradit
běžnými	běžný	k2eAgFnPc7d1	běžná
<g/>
,	,	kIx,	,
typovými	typový	k2eAgInPc7d1	typový
výrobky	výrobek	k1gInPc7	výrobek
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
kritika	kritika	k1gFnSc1	kritika
této	tento	k3xDgFnSc2	tento
rekonstrukce	rekonstrukce	k1gFnSc2	rekonstrukce
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
dodnes	dodnes	k6eAd1	dodnes
<g/>
,	,	kIx,	,
stavebně	stavebně	k6eAd1	stavebně
historický	historický	k2eAgInSc1d1	historický
průzkum	průzkum	k1gInSc1	průzkum
provedený	provedený	k2eAgInSc1d1	provedený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
tuto	tento	k3xDgFnSc4	tento
kritiku	kritika	k1gFnSc4	kritika
vyvrací	vyvracet	k5eAaImIp3nS	vyvracet
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tehdejším	tehdejší	k2eAgNnPc3d1	tehdejší
materiálovým	materiálový	k2eAgNnPc3d1	materiálové
a	a	k8xC	a
stavebně	stavebně	k6eAd1	stavebně
technickým	technický	k2eAgFnPc3d1	technická
možnostem	možnost	k1gFnPc3	možnost
a	a	k8xC	a
společensko-politické	společenskoolitický	k2eAgFnSc6d1	společensko-politická
situaci	situace	k1gFnSc6	situace
můžeme	moct	k5eAaImIp1nP	moct
tuto	tento	k3xDgFnSc4	tento
rekonstrukci	rekonstrukce	k1gFnSc4	rekonstrukce
označit	označit	k5eAaPmF	označit
za	za	k7c4	za
profesionální	profesionální	k2eAgNnSc4d1	profesionální
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
sloužila	sloužit	k5eAaImAgFnS	sloužit
stavba	stavba	k1gFnSc1	stavba
komerčním	komerční	k2eAgInPc3d1	komerční
účelům	účel	k1gInPc3	účel
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
8	[number]	k4	8
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1992	[number]	k4	1992
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
vedeno	vést	k5eAaImNgNnS	vést
první	první	k4xOgFnPc1	první
kolo	kolo	k1gNnSc1	kolo
koaličních	koaliční	k2eAgInPc2d1	koaliční
rozhovorů	rozhovor	k1gInPc2	rozhovor
vítězů	vítěz	k1gMnPc2	vítěz
tehdejších	tehdejší	k2eAgFnPc2d1	tehdejší
voleb	volba	k1gFnPc2	volba
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
předsedou	předseda	k1gMnSc7	předseda
vlády	vláda	k1gFnSc2	vláda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
Václavem	Václav	k1gMnSc7	Václav
Klausem	Klaus	k1gMnSc7	Klaus
a	a	k8xC	a
předsedou	předseda	k1gMnSc7	předseda
vlády	vláda	k1gFnSc2	vláda
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
Vladimírem	Vladimír	k1gMnSc7	Vladimír
Mečiarem	Mečiar	k1gMnSc7	Mečiar
<g/>
.	.	kIx.	.
</s>
<s>
Dohoda	dohoda	k1gFnSc1	dohoda
o	o	k7c4	o
rozdělení	rozdělení	k1gNnSc4	rozdělení
Československa	Československo	k1gNnSc2	Československo
byla	být	k5eAaImAgFnS	být
podepsána	podepsán	k2eAgFnSc1d1	podepsána
26	[number]	k4	26
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
také	také	k9	také
na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
ještě	ještě	k6eAd1	ještě
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
dělení	dělení	k1gNnSc4	dělení
federálního	federální	k2eAgInSc2d1	federální
majetku	majetek	k1gInSc2	majetek
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
Rady	rada	k1gFnSc2	rada
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
byla	být	k5eAaImAgFnS	být
vila	vila	k1gFnSc1	vila
předána	předat	k5eAaPmNgFnS	předat
do	do	k7c2	do
užívání	užívání	k1gNnSc2	užívání
Muzeu	muzeum	k1gNnSc6	muzeum
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
ji	on	k3xPp3gFnSc4	on
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1994	[number]	k4	1994
veřejnosti	veřejnost	k1gFnSc2	veřejnost
zpřístupnilo	zpřístupnit	k5eAaPmAgNnS	zpřístupnit
jako	jako	k9	jako
památník	památník	k1gInSc1	památník
moderní	moderní	k2eAgFnSc2d1	moderní
architektury	architektura	k1gFnSc2	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Osudy	osud	k1gInPc1	osud
domu	dům	k1gInSc2	dům
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
inspirací	inspirace	k1gFnSc7	inspirace
pro	pro	k7c4	pro
román	román	k1gInSc4	román
Simona	Simon	k1gMnSc2	Simon
Mawera	Mawer	k1gMnSc2	Mawer
Skleněný	skleněný	k2eAgInSc4d1	skleněný
pokoj	pokoj	k1gInSc4	pokoj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
začaly	začít	k5eAaPmAgInP	začít
pokusy	pokus	k1gInPc1	pokus
o	o	k7c4	o
další	další	k2eAgFnSc4d1	další
rekonstrukci	rekonstrukce	k1gFnSc4	rekonstrukce
vily	vila	k1gFnSc2	vila
<g/>
.	.	kIx.	.
</s>
<s>
Soutěž	soutěž	k1gFnSc1	soutěž
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
společnost	společnost	k1gFnSc1	společnost
OMNIA	omnium	k1gNnSc2	omnium
projekt	projekt	k1gInSc1	projekt
<g/>
,	,	kIx,	,
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
,	,	kIx,	,
však	však	k9	však
byla	být	k5eAaImAgFnS	být
označena	označit	k5eAaPmNgFnS	označit
za	za	k7c7	za
zmanipulovanou	zmanipulovaný	k2eAgFnSc7d1	zmanipulovaná
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
antimonopolním	antimonopolní	k2eAgInSc7d1	antimonopolní
úřadem	úřad	k1gInSc7	úřad
na	na	k7c6	na
základě	základ	k1gInSc6	základ
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
správního	správní	k2eAgInSc2d1	správní
soudu	soud	k1gInSc2	soud
zrušena	zrušen	k2eAgNnPc1d1	zrušeno
<g/>
.	.	kIx.	.
</s>
<s>
Potomci	potomek	k1gMnPc1	potomek
původních	původní	k2eAgMnPc2d1	původní
majitelů	majitel	k1gMnPc2	majitel
žádali	žádat	k5eAaImAgMnP	žádat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2006	[number]	k4	2006
a	a	k8xC	a
2007	[number]	k4	2007
o	o	k7c4	o
navrácení	navrácení	k1gNnSc4	navrácení
vily	vila	k1gFnSc2	vila
a	a	k8xC	a
nabízeli	nabízet	k5eAaImAgMnP	nabízet
provedení	provedení	k1gNnSc4	provedení
rekonstrukce	rekonstrukce	k1gFnSc2	rekonstrukce
na	na	k7c4	na
vlastní	vlastní	k2eAgInPc4d1	vlastní
náklady	náklad	k1gInPc4	náklad
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
však	však	k9	však
město	město	k1gNnSc1	město
Brno	Brno	k1gNnSc4	Brno
odmítlo	odmítnout	k5eAaPmAgNnS	odmítnout
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
přistoupilo	přistoupit	k5eAaPmAgNnS	přistoupit
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
společnost	společnost	k1gFnSc4	společnost
OMNIA	omnium	k1gNnSc2	omnium
Projekt	projekt	k1gInSc1	projekt
realizovala	realizovat	k5eAaBmAgFnS	realizovat
svůj	svůj	k3xOyFgInSc4	svůj
původní	původní	k2eAgInSc4d1	původní
vítězný	vítězný	k2eAgInSc4d1	vítězný
projekt	projekt	k1gInSc4	projekt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2010	[number]	k4	2010
až	až	k9	až
2012	[number]	k4	2012
tak	tak	k9	tak
byla	být	k5eAaImAgFnS	být
vila	vila	k1gFnSc1	vila
kvůli	kvůli	k7c3	kvůli
rekonstrukci	rekonstrukce	k1gFnSc3	rekonstrukce
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
stála	stát	k5eAaImAgFnS	stát
174	[number]	k4	174
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
,	,	kIx,	,
uzavřena	uzavřen	k2eAgFnSc1d1	uzavřena
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
otevřena	otevřít	k5eAaPmNgFnS	otevřít
až	až	k6eAd1	až
29	[number]	k4	29
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2012	[number]	k4	2012
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
6	[number]	k4	6
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Primátor	primátor	k1gMnSc1	primátor
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
Roman	Roman	k1gMnSc1	Roman
Onderka	Onderka	k1gMnSc1	Onderka
při	při	k7c6	při
té	ten	k3xDgFnSc6	ten
příležitosti	příležitost	k1gFnSc6	příležitost
obhajoval	obhajovat	k5eAaImAgMnS	obhajovat
výjimečností	výjimečnost	k1gFnPc2	výjimečnost
stavby	stavba	k1gFnSc2	stavba
výši	výše	k1gFnSc6	výše
vstupného	vstupné	k1gNnSc2	vstupné
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
stanoveno	stanovit	k5eAaPmNgNnS	stanovit
na	na	k7c4	na
350	[number]	k4	350
Kč	Kč	kA	Kč
za	za	k7c4	za
delší	dlouhý	k2eAgFnSc4d2	delší
z	z	k7c2	z
nabízených	nabízený	k2eAgInPc2d1	nabízený
okruhů	okruh	k1gInPc2	okruh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
rekonstrukce	rekonstrukce	k1gFnSc2	rekonstrukce
se	se	k3xPyFc4	se
do	do	k7c2	do
vily	vila	k1gFnSc2	vila
vrátila	vrátit	k5eAaPmAgFnS	vrátit
např.	např.	kA	např.
část	část	k1gFnSc4	část
původní	původní	k2eAgFnSc2d1	původní
půlkruhové	půlkruhový	k2eAgFnSc2d1	půlkruhová
stěny	stěna	k1gFnSc2	stěna
z	z	k7c2	z
makassarského	makassarský	k2eAgInSc2d1	makassarský
ebenu	eben	k1gInSc2	eben
vyznačující	vyznačující	k2eAgInSc4d1	vyznačující
jídelní	jídelní	k2eAgInSc4d1	jídelní
kout	kout	k1gInSc4	kout
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
krátce	krátce	k6eAd1	krátce
předtím	předtím	k6eAd1	předtím
objevena	objevit	k5eAaPmNgNnP	objevit
historikem	historik	k1gMnSc7	historik
umění	umění	k1gNnSc2	umění
Miroslavem	Miroslav	k1gMnSc7	Miroslav
Ambrozem	Ambroz	k1gInSc7	Ambroz
v	v	k7c6	v
menze	menza	k1gFnSc6	menza
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
brněnské	brněnský	k2eAgFnSc2d1	brněnská
právnické	právnický	k2eAgFnSc2d1	právnická
fakulty	fakulta	k1gFnSc2	fakulta
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
době	doba	k1gFnSc6	doba
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
mělo	mít	k5eAaImAgNnS	mít
svůj	svůj	k3xOyFgInSc4	svůj
bar	bar	k1gInSc4	bar
gestapo	gestapo	k1gNnSc1	gestapo
<g/>
.	.	kIx.	.
</s>
