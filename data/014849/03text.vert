<s>
Matěj	Matěj	k1gMnSc1
Němeček	Němeček	k1gMnSc1
</s>
<s>
Matěj	Matěj	k1gMnSc1
Němeček	Němeček	k1gMnSc1
Matěj	Matěj	k1gMnSc1
Němeček	Němeček	k1gMnSc1
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
Narození	narození	k1gNnSc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1981	#num#	k4
<g/>
Opava	Opava	k1gFnSc1
<g/>
,	,	kIx,
Československo	Československo	k1gNnSc1
Úmrtí	úmrť	k1gFnPc2
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2009	#num#	k4
<g/>
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
28	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Planá	Planá	k1gFnSc1
<g/>
,	,	kIx,
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Příčina	příčina	k1gFnSc1
úmrtí	úmrtí	k1gNnPc2
</s>
<s>
vrozená	vrozený	k2eAgFnSc1d1
srdeční	srdeční	k2eAgFnSc1d1
vada	vada	k1gFnSc1
Místo	místo	k7c2
pohřbení	pohřbení	k1gNnSc2
</s>
<s>
Olšanské	olšanský	k2eAgInPc4d1
hřbitovy	hřbitov	k1gInPc4
Vzdělání	vzdělání	k1gNnSc1
</s>
<s>
DAMU	DAMU	kA
Znám	znát	k5eAaImIp1nS
jako	jako	k9
</s>
<s>
scénograf	scénograf	k1gMnSc1
<g/>
,	,	kIx,
loutkář	loutkář	k1gMnSc1
<g/>
,	,	kIx,
autor	autor	k1gMnSc1
komiksů	komiks	k1gInPc2
Titul	titul	k1gInSc4
</s>
<s>
MgA.	MgA.	k?
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Matěj	Matěj	k1gMnSc1
Němeček	Němeček	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1981	#num#	k4
Opava	Opava	k1gFnSc1
–	–	k?
15	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2009	#num#	k4
Planá	Planá	k1gFnSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
český	český	k2eAgMnSc1d1
scénograf	scénograf	k1gMnSc1
a	a	k8xC
komiksový	komiksový	k2eAgMnSc1d1
autor	autor	k1gMnSc1
<g/>
,	,	kIx,
mladší	mladý	k2eAgMnSc1d2
bratr	bratr	k1gMnSc1
novináře	novinář	k1gMnSc4
Tomáše	Tomáš	k1gMnSc4
Němečka	Němeček	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
a	a	k8xC
dílo	dílo	k1gNnSc1
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS
loutkovou	loutkový	k2eAgFnSc4d1
scénografii	scénografie	k1gFnSc4
na	na	k7c6
Katedře	katedra	k1gFnSc6
alternativního	alternativní	k2eAgNnSc2d1
a	a	k8xC
loutkového	loutkový	k2eAgNnSc2d1
divadla	divadlo	k1gNnSc2
pražské	pražský	k2eAgNnSc4d1
DAMU	DAMU	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
2005	#num#	k4
–	–	k?
2008	#num#	k4
byl	být	k5eAaImAgInS
v	v	k7c6
angažmá	angažmá	k1gNnSc6
Loutkohry	Loutkohra	k1gFnSc2
Jihočeského	jihočeský	k2eAgNnSc2d1
divadla	divadlo	k1gNnSc2
v	v	k7c6
Českých	český	k2eAgInPc6d1
Budějovicích	Budějovice	k1gInPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
vytvořil	vytvořit	k5eAaPmAgMnS
scénu	scéna	k1gFnSc4
a	a	k8xC
loutky	loutka	k1gFnSc2
k	k	k7c3
představením	představení	k1gNnPc3
Malá	malý	k2eAgFnSc1d1
čarodějnice	čarodějnice	k1gFnSc1
<g/>
,	,	kIx,
Kuba	Kuba	k1gFnSc1
Kubula	Kubula	k1gFnSc1
a	a	k8xC
Kuba	Kuba	k1gFnSc1
Kubikula	Kubikula	k1gFnSc1
<g/>
,	,	kIx,
Fimfárum	Fimfárum	k1gInSc1
<g/>
,	,	kIx,
Ten	ten	k3xDgInSc1
náš	náš	k3xOp1gInSc1
Betlém	Betlém	k1gInSc1
<g/>
,	,	kIx,
Královna	královna	k1gFnSc1
hadů	had	k1gMnPc2
a	a	k8xC
Dr	dr	kA
<g/>
.	.	kIx.
Jekyll	Jekyll	k1gMnSc1
a	a	k8xC
Mr	Mr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hyde	Hyd	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
rovněž	rovněž	k9
u	u	k7c2
vzniku	vznik	k1gInSc2
představení	představení	k1gNnSc2
(	(	kIx(
<g/>
dnes	dnes	k6eAd1
divadelně	divadelně	k6eAd1
hudební	hudební	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
<g/>
)	)	kIx)
Kašpárek	Kašpárek	k1gMnSc1
v	v	k7c6
rohlíku	rohlík	k1gInSc6
<g/>
,	,	kIx,
na	na	k7c6
jejímž	jejíž	k3xOyRp3gInSc6
vizuálním	vizuální	k2eAgInSc6d1
stylu	styl	k1gInSc6
se	s	k7c7
zásadním	zásadní	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
autorsky	autorsky	k6eAd1
podílel	podílet	k5eAaImAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolu	spolu	k6eAd1
s	s	k7c7
Danielou	Daniela	k1gFnSc7
Klimešovou	Klimešův	k2eAgFnSc7d1
byl	být	k5eAaImAgMnS
autorem	autor	k1gMnSc7
řady	řada	k1gFnSc2
průvodových	průvodový	k2eAgNnPc2d1
představení	představení	k1gNnPc2
pouličního	pouliční	k2eAgNnSc2d1
divadla	divadlo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Divadelní	divadelní	k2eAgInPc1d1
projekty	projekt	k1gInPc1
(	(	kIx(
<g/>
výběr	výběr	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
2000	#num#	k4
Průvod	průvod	k1gInSc1
vodnáře	vodnář	k1gMnSc2
a	a	k8xC
draka	drak	k1gMnSc2
(	(	kIx(
<g/>
pouliční	pouliční	k2eAgNnSc1d1
průvodové	průvodový	k2eAgNnSc1d1
divadlo	divadlo	k1gNnSc1
v	v	k7c6
rámci	rámec	k1gInSc6
projektu	projekt	k1gInSc2
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
evropské	evropský	k2eAgNnSc1d1
město	město	k1gNnSc1
kultury	kultura	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
2001	#num#	k4
Archa	archa	k1gFnSc1
děje	děj	k1gInSc2
(	(	kIx(
<g/>
pouliční	pouliční	k2eAgNnSc1d1
průvodové	průvodový	k2eAgNnSc1d1
divadlo	divadlo	k1gNnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
2002	#num#	k4
Nikola	Nikola	k1gMnSc1
Šuhaj	šuhaj	k1gMnSc1
(	(	kIx(
<g/>
divadlo	divadlo	k1gNnSc1
Disk	disco	k1gNnPc2
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
SKUTR	SKUTR	kA
<g/>
)	)	kIx)
</s>
<s>
2003	#num#	k4
Hudební	hudební	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
(	(	kIx(
<g/>
pouliční	pouliční	k2eAgNnSc1d1
průvodové	průvodový	k2eAgNnSc1d1
divadlo	divadlo	k1gNnSc1
<g/>
,	,	kIx,
Karlovy	Karlův	k2eAgInPc1d1
Vary	Vary	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
2005	#num#	k4
Pocítění	pocítění	k1gNnSc1
vousů	vous	k1gInPc2
(	(	kIx(
<g/>
divadlo	divadlo	k1gNnSc1
DISK	disco	k1gNnPc2
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Alžběta	Alžběta	k1gFnSc1
Tichoňová	Tichoňová	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
2005	#num#	k4
Pramenění	pramenění	k1gNnSc1
(	(	kIx(
<g/>
autorský	autorský	k2eAgInSc1d1
site	site	k1gInSc1
specific	specifice	k1gFnPc2
projekt	projekt	k1gInSc1
<g/>
,	,	kIx,
Mlýnská	mlýnský	k2eAgFnSc1d1
kolonáda	kolonáda	k1gFnSc1
Karlovy	Karlův	k2eAgInPc1d1
Vary	Vary	k1gInPc7
<g/>
)	)	kIx)
</s>
<s>
2006	#num#	k4
Akvabely	akvabela	k1gFnPc1
(	(	kIx(
<g/>
klub	klub	k1gInSc1
Divadla	divadlo	k1gNnSc2
J.	J.	kA
K.	K.	kA
Tyla	Tyl	k1gMnSc2
v	v	k7c6
Plzni	Plzeň	k1gFnSc6
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Vilém	Viléma	k1gFnPc2
Dubnička	Dubnička	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
2007	#num#	k4
Vyhnání	vyhnání	k1gNnSc3
nudy	nuda	k1gFnSc2
z	z	k7c2
Prahy	Praha	k1gFnSc2
(	(	kIx(
<g/>
pouliční	pouliční	k2eAgNnSc1d1
průvodové	průvodový	k2eAgNnSc1d1
divadlo	divadlo	k1gNnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Števo	Števo	k1gNnSc1
Capko	capka	k1gFnSc5
<g/>
)	)	kIx)
</s>
<s>
2007	#num#	k4
Benátská	benátský	k2eAgNnPc4d1
dvojčata	dvojče	k1gNnPc4
(	(	kIx(
<g/>
Městské	městský	k2eAgNnSc4d1
divadlo	divadlo	k1gNnSc4
Mladá	mladá	k1gFnSc1
Boleslav	Boleslav	k1gMnSc1
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Števo	Števo	k1gNnSc1
Capko	capka	k1gFnSc5
<g/>
)	)	kIx)
</s>
<s>
2007	#num#	k4
Plačky	plačky	k6eAd1
(	(	kIx(
<g/>
Divadlo	divadlo	k1gNnSc1
Archa	archa	k1gFnSc1
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
SKUTR	SKUTR	kA
<g/>
)	)	kIx)
</s>
<s>
2008	#num#	k4
Don	Don	k1gMnSc1
Juan	Juan	k1gMnSc1
(	(	kIx(
<g/>
Divadelní	divadelní	k2eAgNnSc1d1
léto	léto	k1gNnSc1
pod	pod	k7c7
plzeňským	plzeňské	k1gNnSc7
nebem	nebe	k1gNnSc7
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Vilém	Viléma	k1gFnPc2
Dubnička	Dubnička	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
2008	#num#	k4
Dorotka	Dorotka	k1gFnSc1
(	(	kIx(
<g/>
Švandovo	Švandův	k2eAgNnSc1d1
divadlo	divadlo	k1gNnSc1
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Štěpán	Štěpán	k1gMnSc1
Pácl	Pácl	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
2008	#num#	k4
Lazy	Lazy	k?
varieté	varieté	k1gNnSc1
(	(	kIx(
<g/>
Cirkus	cirkus	k1gInSc1
Sacra	Sacr	k1gInSc2
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Števo	Števo	k1gNnSc1
Capko	capka	k1gFnSc5
<g/>
)	)	kIx)
</s>
<s>
2009	#num#	k4
Revizor	revizor	k1gMnSc1
(	(	kIx(
<g/>
Divadelní	divadelní	k2eAgNnSc1d1
léto	léto	k1gNnSc1
pod	pod	k7c7
plzeňským	plzeňské	k1gNnSc7
nebem	nebe	k1gNnSc7
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Vilém	Viléma	k1gFnPc2
Dubnička	Dubnička	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Komiks	komiks	k1gInSc1
a	a	k8xC
ilustrace	ilustrace	k1gFnPc1
</s>
<s>
Jeho	jeho	k3xOp3gInSc7
nejznámějším	známý	k2eAgInSc7d3
komiksem	komiks	k1gInSc7
je	být	k5eAaImIp3nS
John	John	k1gMnSc1
Doe	Doe	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
vyšel	vyjít	k5eAaPmAgInS
jako	jako	k9
samostatný	samostatný	k2eAgInSc1d1
sešit	sešit	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
toho	ten	k3xDgInSc2
byl	být	k5eAaImAgInS
autorem	autor	k1gMnSc7
několika	několik	k4yIc2
menších	malý	k2eAgFnPc2d2
prací	práce	k1gFnPc2
<g/>
,	,	kIx,
zveřejněných	zveřejněný	k2eAgInPc2d1
v	v	k7c6
antologiích	antologie	k1gFnPc6
CZekomiks	CZekomiksa	k1gFnPc2
a	a	k8xC
Kontraband	kontrabanda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ilustroval	ilustrovat	k5eAaBmAgMnS
alba	album	k1gNnSc2
Kašpárek	Kašpárek	k1gMnSc1
v	v	k7c6
rohlíku	rohlík	k1gInSc6
a	a	k8xC
Kašpárek	Kašpárek	k1gMnSc1
navždy	navždy	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ilustroval	ilustrovat	k5eAaBmAgMnS
rovněž	rovněž	k9
knihu	kniha	k1gFnSc4
Zdeňka	Zdeněk	k1gMnSc2
Jecelína	Jecelín	k1gMnSc2
Blíženci	blíženec	k1gMnPc7
z	z	k7c2
ledové	ledový	k2eAgFnSc2d1
pouště	poušť	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Logo	logo	k1gNnSc1
Kašpárka	Kašpárek	k1gMnSc2
v	v	k7c6
rohlíku	rohlík	k1gInSc6
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
xx	xx	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
71976	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
0996	#num#	k4
4162	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
85769010	#num#	k4
</s>
