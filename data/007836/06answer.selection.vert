<s>
Premiéra	premiéra	k1gFnSc1	premiéra
anglické	anglický	k2eAgFnSc2d1	anglická
verze	verze	k1gFnSc2	verze
prvního	první	k4xOgInSc2	první
dvojdílu	dvojdíl	k1gInSc2	dvojdíl
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
2	[number]	k4	2
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
<g/>
:	:	kIx,	:
v	v	k7c6	v
USA	USA	kA	USA
na	na	k7c6	na
stanici	stanice	k1gFnSc6	stanice
Syfy	Syfa	k1gFnSc2	Syfa
(	(	kIx(	(
<g/>
Sci-Fi	scii	k1gFnSc1	sci-fi
Channel	Channel	k1gMnSc1	Channel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
na	na	k7c6	na
Sky	Sky	k1gFnSc6	Sky
One	One	k1gFnSc6	One
a	a	k8xC	a
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
na	na	k7c6	na
kanále	kanál	k1gInSc6	kanál
SPACE	SPACE	kA	SPACE
<g/>
.	.	kIx.	.
</s>
