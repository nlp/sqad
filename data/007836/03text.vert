<s>
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
<g/>
:	:	kIx,	:
Hluboký	hluboký	k2eAgInSc1d1	hluboký
vesmír	vesmír	k1gInSc1	vesmír
(	(	kIx(	(
<g/>
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
originále	originál	k1gInSc6	originál
Stargate	Stargat	k1gInSc5	Stargat
Universe	Universe	k1gFnPc4	Universe
<g/>
,	,	kIx,	,
zkracováno	zkracován	k2eAgNnSc4d1	zkracováno
jako	jako	k8xC	jako
SGU	SGU	kA	SGU
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
kanadsko-americký	kanadskomerický	k2eAgInSc1d1	kanadsko-americký
vojenský	vojenský	k2eAgInSc1d1	vojenský
seriál	seriál	k1gInSc1	seriál
ze	z	k7c2	z
sci-fi	scii	k1gNnSc2	sci-fi
světa	svět	k1gInSc2	svět
Hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
brány	brána	k1gFnSc2	brána
<g/>
.	.	kIx.	.
</s>
<s>
Výchozím	výchozí	k2eAgInSc7d1	výchozí
příběhem	příběh	k1gInSc7	příběh
seriálu	seriál	k1gInSc2	seriál
je	být	k5eAaImIp3nS	být
objevení	objevení	k1gNnSc1	objevení
způsobu	způsob	k1gInSc2	způsob
využití	využití	k1gNnSc3	využití
devátého	devátý	k4xOgInSc2	devátý
zámku	zámek	k1gInSc2	zámek
na	na	k7c6	na
hvězdné	hvězdný	k2eAgFnSc6d1	hvězdná
bráně	brána	k1gFnSc6	brána
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
doposud	doposud	k6eAd1	doposud
obestřen	obestřít	k5eAaPmNgInS	obestřít
tajemstvím	tajemství	k1gNnSc7	tajemství
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
brána	brána	k1gFnSc1	brána
má	mít	k5eAaImIp3nS	mít
totiž	totiž	k9	totiž
zámků	zámek	k1gInPc2	zámek
devět	devět	k4xCc4	devět
<g/>
.	.	kIx.	.
</s>
<s>
Sedm	sedm	k4xCc4	sedm
jich	on	k3xPp3gFnPc2	on
slouží	sloužit	k5eAaImIp3nP	sloužit
ke	k	k7c3	k
spojení	spojení	k1gNnSc3	spojení
s	s	k7c7	s
jinou	jiný	k2eAgFnSc7d1	jiná
bránou	brána	k1gFnSc7	brána
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
galaxii	galaxie	k1gFnSc6	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Osmý	osmý	k4xOgInSc1	osmý
symbol	symbol	k1gInSc1	symbol
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
spojení	spojení	k1gNnSc4	spojení
s	s	k7c7	s
bránou	brána	k1gFnSc7	brána
v	v	k7c6	v
jiné	jiný	k2eAgFnSc6d1	jiná
galaxii	galaxie	k1gFnSc6	galaxie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
však	však	k9	však
kvůli	kvůli	k7c3	kvůli
energetické	energetický	k2eAgFnSc3d1	energetická
náročnosti	náročnost	k1gFnSc3	náročnost
nutné	nutný	k2eAgNnSc1d1	nutné
přídavné	přídavný	k2eAgNnSc1d1	přídavné
napájení	napájení	k1gNnSc1	napájení
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
antické	antický	k2eAgFnSc6d1	antická
ZPM	ZPM	kA	ZPM
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
v	v	k7c6	v
pilotní	pilotní	k2eAgFnSc6d1	pilotní
dvojepizodě	dvojepizoda	k1gFnSc6	dvojepizoda
je	být	k5eAaImIp3nS	být
ujasněno	ujasněn	k2eAgNnSc1d1	ujasněno
<g/>
,	,	kIx,	,
že	že	k8xS	že
devátý	devátý	k4xOgInSc4	devátý
zámek	zámek	k1gInSc4	zámek
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
zadání	zadání	k1gNnSc3	zadání
adresy	adresa	k1gFnSc2	adresa
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
vyhledá	vyhledat	k5eAaPmIp3nS	vyhledat
antickou	antický	k2eAgFnSc4d1	antická
vesmírnou	vesmírný	k2eAgFnSc4d1	vesmírná
loď	loď	k1gFnSc4	loď
Destiny	Destina	k1gFnSc2	Destina
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
kdekoliv	kdekoliv	k6eAd1	kdekoliv
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
spojení	spojení	k1gNnSc1	spojení
si	se	k3xPyFc3	se
však	však	k9	však
také	také	k9	také
nevystačí	vystačit	k5eNaBmIp3nS	vystačit
s	s	k7c7	s
běžnou	běžný	k2eAgFnSc7d1	běžná
energií	energie	k1gFnSc7	energie
<g/>
,	,	kIx,	,
a	a	k8xC	a
SGC	SGC	kA	SGC
muselo	muset	k5eAaImAgNnS	muset
proto	proto	k8xC	proto
vystavět	vystavět	k5eAaPmF	vystavět
základnu	základna	k1gFnSc4	základna
na	na	k7c6	na
jiné	jiný	k2eAgFnSc6d1	jiná
planetě	planeta	k1gFnSc6	planeta
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
brána	brána	k1gFnSc1	brána
získávala	získávat	k5eAaImAgFnS	získávat
geotermální	geotermální	k2eAgFnSc4d1	geotermální
energii	energie	k1gFnSc4	energie
<g/>
.	.	kIx.	.
</s>
<s>
Premiéra	premiéra	k1gFnSc1	premiéra
anglické	anglický	k2eAgFnSc2d1	anglická
verze	verze	k1gFnSc2	verze
prvního	první	k4xOgInSc2	první
dvojdílu	dvojdíl	k1gInSc2	dvojdíl
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
2	[number]	k4	2
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
<g/>
:	:	kIx,	:
v	v	k7c6	v
USA	USA	kA	USA
na	na	k7c6	na
stanici	stanice	k1gFnSc6	stanice
Syfy	Syfa	k1gFnSc2	Syfa
(	(	kIx(	(
<g/>
Sci-Fi	scii	k1gFnSc1	sci-fi
Channel	Channel	k1gMnSc1	Channel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
na	na	k7c6	na
Sky	Sky	k1gFnSc6	Sky
One	One	k1gFnSc6	One
a	a	k8xC	a
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
na	na	k7c6	na
kanále	kanál	k1gInSc6	kanál
SPACE	SPACE	kA	SPACE
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
řada	řada	k1gFnSc1	řada
začala	začít	k5eAaPmAgFnS	začít
<g/>
,	,	kIx,	,
opět	opět	k6eAd1	opět
na	na	k7c6	na
stanici	stanice	k1gFnSc6	stanice
Syfy	Syfa	k1gFnSc2	Syfa
<g/>
,	,	kIx,	,
28	[number]	k4	28
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
a	a	k8xC	a
sice	sice	k8xC	sice
v	v	k7c6	v
9	[number]	k4	9
hodin	hodina	k1gFnPc2	hodina
večer	večer	k6eAd1	večer
místního	místní	k2eAgInSc2d1	místní
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
přesunutí	přesunutí	k1gNnSc2	přesunutí
seriálu	seriál	k1gInSc2	seriál
na	na	k7c4	na
úterní	úterní	k2eAgInPc4d1	úterní
večery	večer	k1gInPc4	večer
si	se	k3xPyFc3	se
stanice	stanice	k1gFnSc1	stanice
slibovala	slibovat	k5eAaImAgFnS	slibovat
zvýšení	zvýšení	k1gNnSc4	zvýšení
sledovanosti	sledovanost	k1gFnSc2	sledovanost
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
ale	ale	k9	ale
nenastalo	nastat	k5eNaPmAgNnS	nastat
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2010	[number]	k4	2010
tedy	tedy	k8xC	tedy
Syfy	Syfy	k1gInPc4	Syfy
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
2	[number]	k4	2
<g/>
.	.	kIx.	.
série	série	k1gFnSc1	série
bude	být	k5eAaImBp3nS	být
také	také	k9	také
tou	ten	k3xDgFnSc7	ten
poslední	poslední	k2eAgFnSc7d1	poslední
<g/>
.	.	kIx.	.
</s>
<s>
Oznámení	oznámení	k1gNnSc1	oznámení
zcela	zcela	k6eAd1	zcela
očekávaně	očekávaně	k6eAd1	očekávaně
odstartovalo	odstartovat	k5eAaPmAgNnS	odstartovat
vlnu	vlna	k1gFnSc4	vlna
internetových	internetový	k2eAgFnPc2d1	internetová
peticí	petice	k1gFnPc2	petice
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
<g/>
,	,	kIx,	,
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
40	[number]	k4	40
<g/>
.	.	kIx.	.
epizoda	epizoda	k1gFnSc1	epizoda
seriálu	seriál	k1gInSc2	seriál
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
<g/>
:	:	kIx,	:
Hluboký	hluboký	k2eAgInSc1d1	hluboký
vesmír	vesmír	k1gInSc1	vesmír
(	(	kIx(	(
<g/>
a	a	k8xC	a
současně	současně	k6eAd1	současně
také	také	k9	také
všech	všecek	k3xTgInPc2	všecek
seriálů	seriál	k1gInPc2	seriál
světa	svět	k1gInSc2	svět
Hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
brány	brána	k1gFnSc2	brána
obecně	obecně	k6eAd1	obecně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Gauntlet	Gauntlet	k1gInSc4	Gauntlet
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
odvysílána	odvysílán	k2eAgFnSc1d1	odvysílána
9	[number]	k4	9
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2011	[number]	k4	2011
televizí	televize	k1gFnSc7	televize
Syfy	Syfa	k1gFnSc2	Syfa
<g/>
.	.	kIx.	.
</s>
<s>
Vysílací	vysílací	k2eAgInSc1d1	vysílací
čas	čas	k1gInSc1	čas
byl	být	k5eAaImAgInS	být
21	[number]	k4	21
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
amerického	americký	k2eAgInSc2d1	americký
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
následujícího	následující	k2eAgInSc2d1	následující
dne	den	k1gInSc2	den
CEST	cesta	k1gFnPc2	cesta
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2011	[number]	k4	2011
česká	český	k2eAgFnSc1d1	Česká
komerční	komerční	k2eAgFnSc1d1	komerční
televize	televize	k1gFnSc1	televize
TV	TV	kA	TV
Nova	nova	k1gFnSc1	nova
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zakoupila	zakoupit	k5eAaPmAgNnP	zakoupit
vysílací	vysílací	k2eAgNnPc1d1	vysílací
práva	právo	k1gNnPc1	právo
na	na	k7c4	na
obě	dva	k4xCgFnPc4	dva
série	série	k1gFnPc4	série
seriálu	seriál	k1gInSc2	seriál
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
zamýšlela	zamýšlet	k5eAaImAgFnS	zamýšlet
vysílat	vysílat	k5eAaImF	vysílat
i	i	k9	i
s	s	k7c7	s
českým	český	k2eAgInSc7d1	český
dabingem	dabing	k1gInSc7	dabing
od	od	k7c2	od
12	[number]	k4	12
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2012	[number]	k4	2012
na	na	k7c6	na
dceřiné	dceřiný	k2eAgFnSc6d1	dceřiná
televizi	televize	k1gFnSc6	televize
Fanda	Fanda	k1gMnSc1	Fanda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
novém	nový	k2eAgInSc6d1	nový
seriálu	seriál	k1gInSc6	seriál
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
<g/>
:	:	kIx,	:
Hluboký	hluboký	k2eAgInSc4d1	hluboký
vesmír	vesmír	k1gInSc4	vesmír
dostali	dostat	k5eAaPmAgMnP	dostat
Lidé	člověk	k1gMnPc1	člověk
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
možnost	možnost	k1gFnSc4	možnost
dostat	dostat	k5eAaPmF	dostat
se	se	k3xPyFc4	se
na	na	k7c4	na
Antickou	antický	k2eAgFnSc4d1	antická
loď	loď	k1gFnSc4	loď
Destiny	Destina	k1gFnSc2	Destina
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
bájného	bájný	k2eAgInSc2d1	bájný
devátého	devátý	k4xOgInSc2	devátý
symbolu	symbol	k1gInSc2	symbol
<g/>
.	.	kIx.	.
</s>
<s>
Adresa	adresa	k1gFnSc1	adresa
Destiny	Destina	k1gFnSc2	Destina
je	být	k5eAaImIp3nS	být
jedinečná	jedinečný	k2eAgFnSc1d1	jedinečná
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
neváže	vázat	k5eNaImIp3nS	vázat
na	na	k7c4	na
výchozí	výchozí	k2eAgInSc4d1	výchozí
bod	bod	k1gInSc4	bod
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
může	moct	k5eAaImIp3nS	moct
cestovat	cestovat	k5eAaImF	cestovat
a	a	k8xC	a
používat	používat	k5eAaImF	používat
bránu	brána	k1gFnSc4	brána
zároveň	zároveň	k6eAd1	zároveň
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
loď	loď	k1gFnSc1	loď
byla	být	k5eAaImAgFnS	být
kdysi	kdysi	k6eAd1	kdysi
součástí	součást	k1gFnSc7	součást
velkého	velký	k2eAgInSc2d1	velký
experimentu	experiment	k1gInSc2	experiment
Antiků	Antik	k1gInPc2	Antik
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
účelem	účel	k1gInSc7	účel
bylo	být	k5eAaImAgNnS	být
zkoumání	zkoumání	k1gNnSc1	zkoumání
signálu	signál	k1gInSc2	signál
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
při	při	k7c6	při
velkém	velký	k2eAgInSc6d1	velký
třesku	třesk	k1gInSc6	třesk
a	a	k8xC	a
nebyl	být	k5eNaImAgInS	být
rozhodně	rozhodně	k6eAd1	rozhodně
přírodní	přírodní	k2eAgInSc1d1	přírodní
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
umělý	umělý	k2eAgInSc1d1	umělý
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
dokazovalo	dokazovat	k5eAaImAgNnS	dokazovat
existenci	existence	k1gFnSc4	existence
inteligence	inteligence	k1gFnSc2	inteligence
na	na	k7c6	na
samotném	samotný	k2eAgInSc6d1	samotný
počátku	počátek	k1gInSc6	počátek
věků	věk	k1gInPc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
lodí	loď	k1gFnSc7	loď
Destiny	Destina	k1gFnSc2	Destina
Antici	Antik	k1gMnPc1	Antik
vyslali	vyslat	k5eAaPmAgMnP	vyslat
několik	několik	k4yIc4	několik
konstrukčních	konstrukční	k2eAgFnPc2d1	konstrukční
lodí	loď	k1gFnPc2	loď
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
rozmístit	rozmístit	k5eAaPmF	rozmístit
po	po	k7c6	po
vesmíru	vesmír	k1gInSc6	vesmír
hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
brány	brána	k1gFnSc2	brána
<g/>
,	,	kIx,	,
samotná	samotný	k2eAgFnSc1d1	samotná
Destiny	Destin	k1gInPc4	Destin
pak	pak	k6eAd1	pak
měla	mít	k5eAaImAgFnS	mít
tyto	tento	k3xDgFnPc4	tento
lodě	loď	k1gFnPc4	loď
následovat	následovat	k5eAaImF	následovat
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
Antici	Antice	k1gFnSc4	Antice
přesunou	přesunout	k5eAaPmIp3nP	přesunout
bránou	brána	k1gFnSc7	brána
až	až	k8xS	až
bude	být	k5eAaImBp3nS	být
loď	loď	k1gFnSc1	loď
hlouběji	hluboko	k6eAd2	hluboko
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Antikové	Antikový	k2eAgNnSc1d1	Antikové
experiment	experiment	k1gInSc4	experiment
nikdy	nikdy	k6eAd1	nikdy
nedotáhli	dotáhnout	k5eNaPmAgMnP	dotáhnout
do	do	k7c2	do
konce	konec	k1gInSc2	konec
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
soustředili	soustředit	k5eAaPmAgMnP	soustředit
na	na	k7c4	na
povznesení	povznesení	k1gNnSc4	povznesení
<g/>
.	.	kIx.	.
</s>
<s>
Zápletka	zápletka	k1gFnSc1	zápletka
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
týkat	týkat	k5eAaImF	týkat
expedice	expedice	k1gFnSc1	expedice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
základně	základna	k1gFnSc6	základna
Icarus	Icarus	k1gInSc4	Icarus
snažila	snažit	k5eAaImAgFnS	snažit
přijít	přijít	k5eAaPmF	přijít
na	na	k7c4	na
kloub	kloub	k1gInSc4	kloub
devátému	devátý	k4xOgInSc3	devátý
symbolu	symbol	k1gInSc3	symbol
a	a	k8xC	a
když	když	k8xS	když
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
podaří	podařit	k5eAaPmIp3nS	podařit
navázat	navázat	k5eAaPmF	navázat
stabilní	stabilní	k2eAgFnSc4d1	stabilní
červí	červí	k2eAgFnSc4d1	červí
díru	díra	k1gFnSc4	díra
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
nuceni	nutit	k5eAaImNgMnP	nutit
ji	on	k3xPp3gFnSc4	on
použít	použít	k5eAaPmF	použít
k	k	k7c3	k
úniku	únik	k1gInSc3	únik
ze	z	k7c2	z
základny	základna	k1gFnSc2	základna
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
zatím	zatím	k6eAd1	zatím
stala	stát	k5eAaPmAgFnS	stát
cílem	cíl	k1gInSc7	cíl
útoku	útok	k1gInSc2	útok
<g/>
.	.	kIx.	.
</s>
<s>
Ocitnou	ocitnout	k5eAaPmIp3nP	ocitnout
se	se	k3xPyFc4	se
tak	tak	k9	tak
na	na	k7c4	na
Destiny	Destin	k2eAgMnPc4d1	Destin
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
Atlantida	Atlantida	k1gFnSc1	Atlantida
stane	stanout	k5eAaPmIp3nS	stanout
další	další	k2eAgFnSc7d1	další
lidskou	lidský	k2eAgFnSc7d1	lidská
kolonií	kolonie	k1gFnSc7	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Expedice	expedice	k1gFnSc1	expedice
však	však	k9	však
uvízne	uvíznout	k5eAaPmIp3nS	uvíznout
na	na	k7c6	na
lodi	loď	k1gFnSc6	loď
a	a	k8xC	a
jediné	jediný	k2eAgNnSc1d1	jediné
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
mohou	moct	k5eAaImIp3nP	moct
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
navázat	navázat	k5eAaPmF	navázat
spojení	spojení	k1gNnSc4	spojení
s	s	k7c7	s
nejbližšími	blízký	k2eAgFnPc7d3	nejbližší
branami	brána	k1gFnPc7	brána
nebo	nebo	k8xC	nebo
použít	použít	k5eAaPmF	použít
komunikační	komunikační	k2eAgInPc4d1	komunikační
kameny	kámen	k1gInPc4	kámen
ke	k	k7c3	k
spojení	spojení	k1gNnSc3	spojení
se	s	k7c7	s
Zemí	zem	k1gFnSc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možností	možnost	k1gFnSc7	možnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
expedice	expedice	k1gFnSc1	expedice
časem	časem	k6eAd1	časem
setká	setkat	k5eAaPmIp3nS	setkat
i	i	k9	i
s	s	k7c7	s
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
oněch	onen	k3xDgFnPc2	onen
lodí	loď	k1gFnPc2	loď
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
brány	brána	k1gFnSc2	brána
vyrábět	vyrábět	k5eAaImF	vyrábět
<g/>
.	.	kIx.	.
</s>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Nicholas	Nicholas	k1gMnSc1	Nicholas
Rush	Rush	k1gMnSc1	Rush
-	-	kIx~	-
Robert	Robert	k1gMnSc1	Robert
Carlyle	Carlyl	k1gInSc5	Carlyl
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Brilantní	brilantní	k2eAgMnSc1d1	brilantní
machiavelistický	machiavelistický	k2eAgMnSc1d1	machiavelistický
vědec	vědec	k1gMnSc1	vědec
na	na	k7c6	na
lodi	loď	k1gFnSc6	loď
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
producent	producent	k1gMnSc1	producent
Joseph	Joseph	k1gMnSc1	Joseph
Mallozzi	Mallozze	k1gFnSc4	Mallozze
poprvé	poprvé	k6eAd1	poprvé
zmínil	zmínit	k5eAaPmAgMnS	zmínit
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
blogu	blog	k1gInSc6	blog
v	v	k7c6	v
půlce	půlka	k1gFnSc6	půlka
listopadu	listopad	k1gInSc2	listopad
2008	[number]	k4	2008
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
David	David	k1gMnSc1	David
Rush	Rush	k1gMnSc1	Rush
<g/>
.	.	kIx.	.
</s>
<s>
Posádka	posádka	k1gFnSc1	posádka
lodi	loď	k1gFnSc2	loď
věří	věřit	k5eAaImIp3nS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Rush	Rush	k1gInSc1	Rush
přichází	přicházet	k5eAaImIp3nS	přicházet
o	o	k7c4	o
rozum	rozum	k1gInSc4	rozum
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Rush	Rush	k1gMnSc1	Rush
dělá	dělat	k5eAaImIp3nS	dělat
všechno	všechen	k3xTgNnSc4	všechen
z	z	k7c2	z
určitého	určitý	k2eAgInSc2d1	určitý
důvodu	důvod	k1gInSc2	důvod
<g/>
.	.	kIx.	.
</s>
<s>
Carlyle	Carlyl	k1gInSc5	Carlyl
vysvětlil	vysvětlit	k5eAaPmAgMnS	vysvětlit
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
své	svůj	k3xOyFgFnSc2	svůj
ženy	žena	k1gFnSc2	žena
je	být	k5eAaImIp3nS	být
Rush	Rush	k1gInSc4	Rush
poháněný	poháněný	k2eAgInSc4d1	poháněný
možností	možnost	k1gFnSc7	možnost
prozkoumávat	prozkoumávat	k5eAaImF	prozkoumávat
vesmír	vesmír	k1gInSc4	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Mallozi	Malloh	k1gMnPc1	Malloh
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
prosince	prosinec	k1gInSc2	prosinec
2008	[number]	k4	2008
upřesnil	upřesnit	k5eAaPmAgMnS	upřesnit
zprávy	zpráva	k1gFnPc4	zpráva
o	o	k7c6	o
postavách	postava	k1gFnPc6	postava
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Rush	Rush	k1gInSc1	Rush
"	"	kIx"	"
<g/>
není	být	k5eNaImIp3nS	být
vůdcem	vůdce	k1gMnSc7	vůdce
neplánované	plánovaný	k2eNgFnSc2d1	neplánovaná
expedice	expedice	k1gFnSc2	expedice
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
čest	čest	k1gFnSc1	čest
připadá	připadat	k5eAaImIp3nS	připadat
plukovníkovi	plukovník	k1gMnSc3	plukovník
Everettovi	Everett	k1gMnSc3	Everett
Youngovi	Young	k1gMnSc3	Young
<g/>
.	.	kIx.	.
</s>
<s>
Prozatím	prozatím	k6eAd1	prozatím
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
věci	věc	k1gFnPc1	věc
se	se	k3xPyFc4	se
můžou	můžou	k?	můžou
změnit	změnit	k5eAaPmF	změnit
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
lodi	loď	k1gFnSc2	loď
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
obsluhována	obsluhovat	k5eAaImNgFnS	obsluhovat
různorodou	různorodý	k2eAgFnSc7d1	různorodá
skupinou	skupina	k1gFnSc7	skupina
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
má	mít	k5eAaImIp3nS	mít
každý	každý	k3xTgInSc4	každý
různý	různý	k2eAgInSc4d1	různý
plán	plán	k1gInSc4	plán
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
V	v	k7c6	v
původních	původní	k2eAgInPc6d1	původní
návrzích	návrh	k1gInPc6	návrh
postav	postava	k1gFnPc2	postava
nebyl	být	k5eNaImAgInS	být
Rush	Rush	k1gInSc1	Rush
zahrnut	zahrnout	k5eAaPmNgInS	zahrnout
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
byl	být	k5eAaImAgInS	být
potvrzenou	potvrzený	k2eAgFnSc7d1	potvrzená
hlavní	hlavní	k2eAgFnSc7d1	hlavní
postavou	postava	k1gFnSc7	postava
<g/>
.	.	kIx.	.
</s>
<s>
Wright	Wright	k1gInSc4	Wright
a	a	k8xC	a
Cooper	Cooper	k1gInSc4	Cooper
měli	mít	k5eAaImAgMnP	mít
v	v	k7c6	v
úmyslu	úmysl	k1gInSc6	úmysl
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
postava	postava	k1gFnSc1	postava
Nicholase	Nicholasa	k1gFnSc3	Nicholasa
Rushe	Rushe	k1gFnSc4	Rushe
velmi	velmi	k6eAd1	velmi
odlišná	odlišný	k2eAgFnSc1d1	odlišná
od	od	k7c2	od
předchozích	předchozí	k2eAgFnPc2d1	předchozí
postav	postava	k1gFnPc2	postava
světa	svět	k1gInSc2	svět
Hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
brány	brána	k1gFnSc2	brána
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
někdo	někdo	k3yInSc1	někdo
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
není	být	k5eNaImIp3nS	být
hrdina	hrdina	k1gMnSc1	hrdina
ani	ani	k8xC	ani
padouch	padouch	k1gMnSc1	padouch
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spíše	spíše	k9	spíše
složitá	složitý	k2eAgFnSc1d1	složitá
postava	postava	k1gFnSc1	postava
se	s	k7c7	s
svými	svůj	k3xOyFgFnPc7	svůj
mnoha	mnoho	k4c7	mnoho
chybami	chyba	k1gFnPc7	chyba
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Rok	rok	k1gInSc1	rok
předtím	předtím	k6eAd1	předtím
než	než	k8xS	než
byl	být	k5eAaImAgInS	být
obsazen	obsadit	k5eAaPmNgInS	obsadit
<g/>
,	,	kIx,	,
chtěl	chtít	k5eAaImAgMnS	chtít
skotský	skotský	k2eAgMnSc1d1	skotský
herec	herec	k1gMnSc1	herec
Robert	Robert	k1gMnSc1	Robert
Carlyle	Carlyl	k1gInSc5	Carlyl
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
kariéře	kariéra	k1gFnSc6	kariéra
zkusit	zkusit	k5eAaPmF	zkusit
něco	něco	k3yInSc1	něco
nového	nový	k2eAgMnSc2d1	nový
a	a	k8xC	a
oslovil	oslovit	k5eAaPmAgMnS	oslovit
televizní	televizní	k2eAgFnPc4d1	televizní
společnosti	společnost	k1gFnPc4	společnost
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
.	.	kIx.	.
</s>
<s>
Nabídli	nabídnout	k5eAaPmAgMnP	nabídnout
mu	on	k3xPp3gMnSc3	on
několik	několik	k4yIc4	několik
rolí	role	k1gFnPc2	role
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejvíce	nejvíce	k6eAd1	nejvíce
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
zamlouval	zamlouvat	k5eAaImAgInS	zamlouvat
seriál	seriál	k1gInSc1	seriál
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
<g/>
:	:	kIx,	:
Hluboký	hluboký	k2eAgInSc1d1	hluboký
vesmír	vesmír	k1gInSc1	vesmír
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
"	"	kIx"	"
<g/>
najednou	najednou	k6eAd1	najednou
se	se	k3xPyFc4	se
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
otevíralo	otevírat	k5eAaImAgNnS	otevírat
drama	drama	k1gNnSc4	drama
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
poněkud	poněkud	k6eAd1	poněkud
žánru	žánr	k1gInSc2	žánr
chybělo	chybět	k5eAaImAgNnS	chybět
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Byl	být	k5eAaImAgMnS	být
si	se	k3xPyFc3	se
vědom	vědom	k2eAgMnSc1d1	vědom
úspěchu	úspěch	k1gInSc2	úspěch
Hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
<g />
.	.	kIx.	.
</s>
<s>
brány	brána	k1gFnPc4	brána
a	a	k8xC	a
viděl	vidět	k5eAaImAgMnS	vidět
"	"	kIx"	"
<g/>
celkem	celkem	k6eAd1	celkem
dost	dost	k6eAd1	dost
epizod	epizoda	k1gFnPc2	epizoda
seriálu	seriál	k1gInSc2	seriál
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
a	a	k8xC	a
spoustu	spousta	k1gFnSc4	spousta
epizod	epizoda	k1gFnPc2	epizoda
seriálu	seriál	k1gInSc2	seriál
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
<g/>
:	:	kIx,	:
Atlantida	Atlantida	k1gFnSc1	Atlantida
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Carlyle	Carlyl	k1gInSc5	Carlyl
přijal	přijmout	k5eAaPmAgMnS	přijmout
nabídku	nabídka	k1gFnSc4	nabídka
díky	díky	k7c3	díky
přístupu	přístup	k1gInSc3	přístup
Wrighta	Wright	k1gMnSc2	Wright
a	a	k8xC	a
Coopera	Cooper	k1gMnSc2	Cooper
k	k	k7c3	k
dramatu	drama	k1gNnSc3	drama
a	a	k8xC	a
díky	díky	k7c3	díky
směru	směr	k1gInSc3	směr
seriálu	seriál	k1gInSc2	seriál
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
více	hodně	k6eAd2	hodně
než	než	k8xS	než
připraven	připraven	k2eAgMnSc1d1	připraven
<g/>
"	"	kIx"	"
hrát	hrát	k5eAaImF	hrát
tuto	tento	k3xDgFnSc4	tento
postavu	postava	k1gFnSc4	postava
možná	možná	k9	možná
i	i	k9	i
několik	několik	k4yIc1	několik
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Carlyle	Carlyl	k1gInSc5	Carlyl
si	se	k3xPyFc3	se
pro	pro	k7c4	pro
roli	role	k1gFnSc4	role
nechává	nechávat	k5eAaImIp3nS	nechávat
svůj	svůj	k3xOyFgInSc4	svůj
skotský	skotský	k2eAgInSc4d1	skotský
přízvuk	přízvuk	k1gInSc4	přízvuk
<g/>
.	.	kIx.	.
</s>
<s>
Plukovník	plukovník	k1gMnSc1	plukovník
Everett	Everett	k1gMnSc1	Everett
Young	Young	k1gMnSc1	Young
-	-	kIx~	-
Justin	Justin	k1gMnSc1	Justin
Louis	Louis	k1gMnSc1	Louis
<g/>
:	:	kIx,	:
V	v	k7c6	v
původní	původní	k2eAgFnSc6d1	původní
návrhu	návrh	k1gInSc3	návrh
postavy	postava	k1gFnSc2	postava
popsán	popsán	k2eAgInSc1d1	popsán
jako	jako	k9	jako
"	"	kIx"	"
<g/>
pohledný	pohledný	k2eAgMnSc1d1	pohledný
<g/>
,	,	kIx,	,
schopný	schopný	k2eAgMnSc1d1	schopný
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
vůdce	vůdce	k1gMnSc1	vůdce
SG	SG	kA	SG
týmu	tým	k1gInSc2	tým
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
čtyřiceti	čtyřicet	k4xCc6	čtyřicet
letech	léto	k1gNnPc6	léto
plukovníkem	plukovník	k1gMnSc7	plukovník
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
jako	jako	k8xC	jako
Jack	Jack	k1gMnSc1	Jack
O	O	kA	O
<g/>
'	'	kIx"	'
<g/>
Neill	Neill	k1gMnSc1	Neill
před	před	k7c7	před
deseti	deset	k4xCc7	deset
lety	let	k1gInPc7	let
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
ale	ale	k8xC	ale
má	mít	k5eAaImIp3nS	mít
ostřejší	ostrý	k2eAgFnPc4d2	ostřejší
hrany	hrana	k1gFnPc4	hrana
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
Hlubokého	hluboký	k2eAgInSc2d1	hluboký
vesmíru	vesmír	k1gInSc2	vesmír
byl	být	k5eAaImAgInS	být
asi	asi	k9	asi
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
ženatý	ženatý	k2eAgInSc1d1	ženatý
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
dočasný	dočasný	k2eAgMnSc1d1	dočasný
velitel	velitel	k1gMnSc1	velitel
tajné	tajný	k2eAgFnSc2d1	tajná
základny	základna	k1gFnSc2	základna
na	na	k7c6	na
jiné	jiný	k2eAgFnSc6d1	jiná
planetě	planeta	k1gFnSc6	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Young	Young	k1gMnSc1	Young
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
lodi	loď	k1gFnSc6	loď
Rushův	Rushův	k2eAgMnSc1d1	Rushův
nepřítel	nepřítel	k1gMnSc1	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Nadporučík	nadporučík	k1gMnSc1	nadporučík
Matthew	Matthew	k1gMnSc1	Matthew
Scott	Scott	k1gMnSc1	Scott
-	-	kIx~	-
Brian	Brian	k1gMnSc1	Brian
J.	J.	kA	J.
Smith	Smith	k1gMnSc1	Smith
<g/>
:	:	kIx,	:
Zkušený	zkušený	k2eAgMnSc1d1	zkušený
a	a	k8xC	a
dobře	dobře	k6eAd1	dobře
trénovaný	trénovaný	k2eAgMnSc1d1	trénovaný
letec	letec	k1gMnSc1	letec
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
26	[number]	k4	26
let	léto	k1gNnPc2	léto
a	a	k8xC	a
mladší	mladý	k2eAgMnSc1d2	mladší
člen	člen	k1gMnSc1	člen
SGC	SGC	kA	SGC
s	s	k7c7	s
hodností	hodnost	k1gFnSc7	hodnost
nadporučíka	nadporučík	k1gMnSc2	nadporučík
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
mentálně	mentálně	k6eAd1	mentálně
nepřipravený	připravený	k2eNgInSc1d1	nepřipravený
na	na	k7c4	na
naléhavost	naléhavost	k1gFnSc4	naléhavost
situace	situace	k1gFnSc2	situace
<g/>
"	"	kIx"	"
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
lodi	loď	k1gFnSc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvotním	prvotní	k2eAgNnSc6d1	prvotní
obsazení	obsazení	k1gNnSc6	obsazení
byl	být	k5eAaImAgMnS	být
pojmenován	pojmenovat	k5eAaPmNgMnS	pojmenovat
Jared	Jared	k1gMnSc1	Jared
Nash	Nash	k1gMnSc1	Nash
<g/>
.	.	kIx.	.
</s>
<s>
Předtím	předtím	k6eAd1	předtím
než	než	k8xS	než
byl	být	k5eAaImAgMnS	být
odsazen	odsadit	k5eAaPmNgMnS	odsadit
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
role	role	k1gFnSc2	role
<g/>
,	,	kIx,	,
Brian	Brian	k1gMnSc1	Brian
J.	J.	kA	J.
Smith	Smith	k1gMnSc1	Smith
rok	rok	k1gInSc1	rok
a	a	k8xC	a
půl	půl	k1xP	půl
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k8xS	jako
divadelní	divadelní	k2eAgMnSc1d1	divadelní
herec	herec	k1gMnSc1	herec
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Svoji	svůj	k3xOyFgFnSc4	svůj
zkoušku	zkouška	k1gFnSc4	zkouška
na	na	k7c4	na
roli	role	k1gFnSc4	role
v	v	k7c6	v
Hlubokém	hluboký	k2eAgInSc6d1	hluboký
vesmíru	vesmír	k1gInSc6	vesmír
si	se	k3xPyFc3	se
natočil	natočit	k5eAaBmAgMnS	natočit
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
pozván	pozvat	k5eAaPmNgMnS	pozvat
na	na	k7c4	na
kamerovou	kamerový	k2eAgFnSc4d1	kamerová
zkoušku	zkouška	k1gFnSc4	zkouška
do	do	k7c2	do
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
.	.	kIx.	.
</s>
<s>
Pár	pár	k4xCyI	pár
dnů	den	k1gInPc2	den
po	po	k7c6	po
zkoušce	zkouška	k1gFnSc6	zkouška
obdržel	obdržet	k5eAaPmAgMnS	obdržet
zprávu	zpráva	k1gFnSc4	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
obsazen	obsadit	k5eAaPmNgInS	obsadit
do	do	k7c2	do
role	role	k1gFnSc2	role
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
roli	role	k1gFnSc4	role
se	se	k3xPyFc4	se
připravoval	připravovat	k5eAaImAgInS	připravovat
vojenským	vojenský	k2eAgInSc7d1	vojenský
výzkumem	výzkum	k1gInSc7	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
Předtím	předtím	k6eAd1	předtím
než	než	k8xS	než
byl	být	k5eAaImAgInS	být
obsazen	obsadit	k5eAaPmNgInS	obsadit
<g/>
,	,	kIx,	,
neviděl	vidět	k5eNaImAgMnS	vidět
mnoho	mnoho	k4c4	mnoho
televizních	televizní	k2eAgFnPc2d1	televizní
epizod	epizoda	k1gFnPc2	epizoda
Hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
brány	brána	k1gFnSc2	brána
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
dohonil	dohonit	k5eAaPmAgInS	dohonit
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
epizod	epizoda	k1gFnPc2	epizoda
seriálu	seriál	k1gInSc2	seriál
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
<g/>
.	.	kIx.	.
</s>
<s>
Chloe	Chloe	k1gFnSc1	Chloe
Armstrongová	Armstrongová	k1gFnSc1	Armstrongová
-	-	kIx~	-
Elyse	Elyse	k1gFnSc1	Elyse
Levesqueová	Levesqueová	k1gFnSc1	Levesqueová
<g/>
:	:	kIx,	:
Je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
úžasná	úžasný	k2eAgFnSc1d1	úžasná
a	a	k8xC	a
sexy	sex	k1gInPc1	sex
<g/>
"	"	kIx"	"
dcera	dcera	k1gFnSc1	dcera
amerického	americký	k2eAgMnSc2d1	americký
senátora	senátor	k1gMnSc2	senátor
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
23	[number]	k4	23
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
postava	postava	k1gFnSc1	postava
prochází	procházet	k5eAaImIp3nS	procházet
zkouškami	zkouška	k1gFnPc7	zkouška
po	po	k7c6	po
tragické	tragický	k2eAgFnSc6d1	tragická
smrti	smrt	k1gFnSc6	smrt
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
a	a	k8xC	a
zoufalými	zoufalý	k2eAgInPc7d1	zoufalý
poměry	poměr	k1gInPc7	poměr
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
vládnou	vládnout	k5eAaImIp3nP	vládnout
na	na	k7c6	na
vesmírné	vesmírný	k2eAgFnSc6d1	vesmírná
lodi	loď	k1gFnSc6	loď
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
posádka	posádka	k1gFnSc1	posádka
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
uvězněna	uvěznit	k5eAaPmNgFnS	uvěznit
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnSc1	její
otec	otec	k1gMnSc1	otec
(	(	kIx(	(
<g/>
ztvárněný	ztvárněný	k2eAgInSc1d1	ztvárněný
hercem	herec	k1gMnSc7	herec
Christopherem	Christopher	k1gMnSc7	Christopher
McDonaldem	McDonald	k1gMnSc7	McDonald
<g/>
)	)	kIx)	)
měl	mít	k5eAaImAgInS	mít
politický	politický	k2eAgInSc4d1	politický
dohled	dohled	k1gInSc4	dohled
nad	nad	k7c7	nad
projektem	projekt	k1gInSc7	projekt
programu	program	k1gInSc2	program
Hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
brány	brána	k1gFnSc2	brána
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
vytočit	vytočit	k5eAaPmF	vytočit
devátý	devátý	k4xOgInSc4	devátý
zámek	zámek	k1gInSc4	zámek
<g/>
.	.	kIx.	.
</s>
<s>
Předtím	předtím	k6eAd1	předtím
než	než	k8xS	než
se	se	k3xPyFc4	se
producenti	producent	k1gMnPc1	producent
dohodli	dohodnout	k5eAaPmAgMnP	dohodnout
na	na	k7c6	na
definitivním	definitivní	k2eAgNnSc6d1	definitivní
jménu	jméno	k1gNnSc6	jméno
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
postava	postava	k1gFnSc1	postava
pojmenovaná	pojmenovaný	k2eAgFnSc1d1	pojmenovaná
Chloe	Chloe	k1gNnSc4	Chloe
Carpenter	Carpenter	k1gInSc1	Carpenter
a	a	k8xC	a
Chloe	Chloe	k1gFnSc1	Chloe
Walker	Walkra	k1gFnPc2	Walkra
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
"	"	kIx"	"
<g/>
skvěle	skvěle	k6eAd1	skvěle
odlišný	odlišný	k2eAgInSc1d1	odlišný
konkurz	konkurz	k1gInSc1	konkurz
<g/>
"	"	kIx"	"
přesvědčil	přesvědčit	k5eAaPmAgInS	přesvědčit
producenty	producent	k1gMnPc4	producent
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
ji	on	k3xPp3gFnSc4	on
obsadili	obsadit	k5eAaPmAgMnP	obsadit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
předvedla	předvést	k5eAaPmAgFnS	předvést
"	"	kIx"	"
<g/>
strhující	strhující	k2eAgInSc4d1	strhující
rozsah	rozsah	k1gInSc4	rozsah
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
rozlišných	rozlišný	k2eAgFnPc6d1	rozlišná
a	a	k8xC	a
náročných	náročný	k2eAgFnPc6d1	náročná
scénách	scéna	k1gFnPc6	scéna
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Eli	Eli	k1gFnSc1	Eli
Wallace	Wallace	k1gFnSc1	Wallace
-	-	kIx~	-
David	David	k1gMnSc1	David
Blue	Blu	k1gFnSc2	Blu
<g/>
:	:	kIx,	:
V	v	k7c6	v
prvotním	prvotní	k2eAgNnSc6d1	prvotní
obsazení	obsazení	k1gNnSc6	obsazení
je	být	k5eAaImIp3nS	být
pojmenovaný	pojmenovaný	k2eAgMnSc1d1	pojmenovaný
Eli	Eli	k1gMnSc1	Eli
Hitchcock	Hitchcock	k1gMnSc1	Hitchcock
<g/>
.	.	kIx.	.
</s>
<s>
Eli	Eli	k?	Eli
Wallace	Wallace	k1gFnSc1	Wallace
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
absolutní	absolutní	k2eAgMnSc1d1	absolutní
flákač	flákač	k1gMnSc1	flákač
<g/>
"	"	kIx"	"
a	a	k8xC	a
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
20	[number]	k4	20
letech	léto	k1gNnPc6	léto
"	"	kIx"	"
<g/>
naprostý	naprostý	k2eAgMnSc1d1	naprostý
génius	génius	k1gMnSc1	génius
<g/>
"	"	kIx"	"
v	v	k7c6	v
matematice	matematika	k1gFnSc6	matematika
<g/>
,	,	kIx,	,
počítačích	počítač	k1gInPc6	počítač
a	a	k8xC	a
dalších	další	k2eAgInPc6d1	další
oborech	obor	k1gInPc6	obor
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
sociální	sociální	k2eAgMnSc1d1	sociální
vyvrženec	vyvrženec	k1gMnSc1	vyvrženec
s	s	k7c7	s
"	"	kIx"	"
<g/>
jízlivým	jízlivý	k2eAgInSc7d1	jízlivý
smyslem	smysl	k1gInSc7	smysl
pro	pro	k7c4	pro
humor	humor	k1gInSc4	humor
<g/>
"	"	kIx"	"
a	a	k8xC	a
postrádá	postrádat	k5eAaImIp3nS	postrádat
důvěru	důvěra	k1gFnSc4	důvěra
ve	v	k7c4	v
svou	svůj	k3xOyFgFnSc4	svůj
inteligenci	inteligence	k1gFnSc4	inteligence
<g/>
.	.	kIx.	.
</s>
<s>
Rozpis	rozpis	k1gInSc4	rozpis
postavy	postava	k1gFnSc2	postava
ho	on	k3xPp3gMnSc4	on
přirovnává	přirovnávat	k5eAaImIp3nS	přirovnávat
k	k	k7c3	k
postavě	postava	k1gFnSc3	postava
Matta	Matt	k1gMnSc2	Matt
Damona	Damon	k1gMnSc2	Damon
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Good	Gooda	k1gFnPc2	Gooda
Will	Will	k1gInSc4	Will
s	s	k7c7	s
trochou	trocha	k1gFnSc7	trocha
postavy	postava	k1gFnSc2	postava
Jacka	Jacka	k1gFnSc1	Jacka
Blacka	Blacka	k1gFnSc1	Blacka
<g/>
.	.	kIx.	.
</s>
<s>
Bude	být	k5eAaImBp3nS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
zdrojem	zdroj	k1gInSc7	zdroj
žertovných	žertovný	k2eAgInPc2d1	žertovný
kousků	kousek	k1gInPc2	kousek
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
<g/>
.	.	kIx.	.
</s>
<s>
David	David	k1gMnSc1	David
Blue	Blu	k1gFnSc2	Blu
<g/>
,	,	kIx,	,
prohlašující	prohlašující	k2eAgFnSc2d1	prohlašující
se	se	k3xPyFc4	se
za	za	k7c4	za
fanouška	fanoušek	k1gMnSc4	fanoušek
sci-fi	scii	k1gNnPc2	sci-fi
seriálů	seriál	k1gInPc2	seriál
<g/>
,	,	kIx,	,
viděl	vidět	k5eAaImAgMnS	vidět
všechny	všechen	k3xTgFnPc4	všechen
epizody	epizoda	k1gFnPc4	epizoda
seriálů	seriál	k1gInPc2	seriál
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
a	a	k8xC	a
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
<g/>
:	:	kIx,	:
Atlantida	Atlantida	k1gFnSc1	Atlantida
<g/>
.	.	kIx.	.
</s>
<s>
Nadporučík	nadporučík	k1gMnSc1	nadporučík
Tamara	Tamara	k1gFnSc1	Tamara
Johansenová	Johansenová	k1gFnSc1	Johansenová
-	-	kIx~	-
Alaina	Alain	k2eAgFnSc1d1	Alaina
Huffmanová	Huffmanová	k1gFnSc1	Huffmanová
<g/>
:	:	kIx,	:
V	v	k7c6	v
prvotním	prvotní	k2eAgNnSc6d1	prvotní
obsazení	obsazení	k1gNnSc6	obsazení
a	a	k8xC	a
rozpisu	rozpis	k1gInSc6	rozpis
postavy	postava	k1gFnSc2	postava
je	být	k5eAaImIp3nS	být
pojmenovaná	pojmenovaný	k2eAgFnSc1d1	pojmenovaná
Tamara	Tamara	k1gFnSc1	Tamara
Jon	Jon	k1gFnSc2	Jon
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
zdravotnice	zdravotnice	k1gFnSc1	zdravotnice
SGC	SGC	kA	SGC
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
zkušenosti	zkušenost	k1gFnPc4	zkušenost
z	z	k7c2	z
mimozemských	mimozemský	k2eAgFnPc2d1	mimozemská
misí	mise	k1gFnPc2	mise
a	a	k8xC	a
její	její	k3xOp3gFnSc4	její
hodnost	hodnost	k1gFnSc4	hodnost
je	být	k5eAaImIp3nS	být
nadporučík	nadporučík	k1gMnSc1	nadporučík
<g/>
.	.	kIx.	.
</s>
<s>
Přátelé	přítel	k1gMnPc1	přítel
jí	on	k3xPp3gFnSc3	on
říkají	říkat	k5eAaImIp3nP	říkat
"	"	kIx"	"
<g/>
TJ.	tj.	kA	tj.
<g/>
"	"	kIx"	"
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
hlavního	hlavní	k2eAgMnSc2d1	hlavní
lékaře	lékař	k1gMnSc2	lékař
základny	základna	k1gFnSc2	základna
Icarus	Icarus	k1gMnSc1	Icarus
v	v	k7c6	v
pilotní	pilotní	k2eAgFnSc6d1	pilotní
episodě	episoda	k1gFnSc6	episoda
"	"	kIx"	"
<g/>
Air	Air	k1gFnPc6	Air
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
osoba	osoba	k1gFnSc1	osoba
s	s	k7c7	s
největšími	veliký	k2eAgFnPc7d3	veliký
zdravotnickými	zdravotnický	k2eAgFnPc7d1	zdravotnická
zkušenostmi	zkušenost	k1gFnPc7	zkušenost
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
Destiny	Destina	k1gFnSc2	Destina
<g/>
.	.	kIx.	.
</s>
<s>
Pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
skromného	skromný	k2eAgNnSc2d1	skromné
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
krásná	krásný	k2eAgFnSc1d1	krásná
<g/>
,	,	kIx,	,
houževnatá	houževnatý	k2eAgFnSc1d1	houževnatá
<g/>
,	,	kIx,	,
chytrá	chytrý	k2eAgFnSc1d1	chytrá
a	a	k8xC	a
schopná	schopný	k2eAgFnSc1d1	schopná
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
ale	ale	k8xC	ale
také	také	k9	také
má	mít	k5eAaImIp3nS	mít
tajnou	tajný	k2eAgFnSc4d1	tajná
minulost	minulost	k1gFnSc4	minulost
s	s	k7c7	s
dalším	další	k2eAgMnSc7d1	další
členem	člen	k1gMnSc7	člen
nové	nový	k2eAgFnSc2d1	nová
posádky	posádka	k1gFnSc2	posádka
Destiny	Destina	k1gFnSc2	Destina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
seriálu	seriál	k1gInSc2	seriál
je	být	k5eAaImIp3nS	být
ohromena	ohromit	k5eAaPmNgFnS	ohromit
nedostatkem	nedostatek	k1gInSc7	nedostatek
zdravotnických	zdravotnický	k2eAgFnPc2d1	zdravotnická
znalostí	znalost	k1gFnPc2	znalost
<g/>
,	,	kIx,	,
zkušeností	zkušenost	k1gFnPc2	zkušenost
<g/>
,	,	kIx,	,
léků	lék	k1gInPc2	lék
a	a	k8xC	a
zásob	zásoba	k1gFnPc2	zásoba
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
lodi	loď	k1gFnSc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Mallozzi	Mallozze	k1gFnSc4	Mallozze
považoval	považovat	k5eAaImAgMnS	považovat
kamerové	kamerový	k2eAgFnSc2d1	kamerová
zkoušky	zkouška	k1gFnSc2	zkouška
Alainy	Alaina	k1gFnSc2	Alaina
Huffman	Huffman	k1gMnSc1	Huffman
v	v	k7c6	v
prosince	prosinec	k1gInSc2	prosinec
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
za	za	k7c4	za
tak	tak	k6eAd1	tak
dobré	dobrý	k2eAgNnSc4d1	dobré
<g/>
,	,	kIx,	,
že	že	k8xS	že
bychom	by	kYmCp1nP	by
museli	muset	k5eAaImAgMnP	muset
být	být	k5eAaImF	být
blázni	blázen	k1gMnPc1	blázen
<g/>
,	,	kIx,	,
abychom	aby	kYmCp1nP	aby
ji	on	k3xPp3gFnSc4	on
neobsadili	obsadit	k5eNaPmAgMnP	obsadit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Seržant	seržant	k1gMnSc1	seržant
Ronald	Ronald	k1gMnSc1	Ronald
Greer	Greer	k1gMnSc1	Greer
-	-	kIx~	-
Jamil	Jamil	k1gMnSc1	Jamil
Walker	Walker	k1gMnSc1	Walker
Smith	Smith	k1gMnSc1	Smith
<g/>
:	:	kIx,	:
V	v	k7c6	v
prvotních	prvotní	k2eAgInPc6d1	prvotní
castingových	castingový	k2eAgInPc6d1	castingový
dokumentech	dokument	k1gInPc6	dokument
je	být	k5eAaImIp3nS	být
pojmenován	pojmenován	k2eAgInSc4d1	pojmenován
Ron	ron	k1gInSc4	ron
"	"	kIx"	"
<g/>
Psycho	psycha	k1gFnSc5	psycha
<g/>
"	"	kIx"	"
Stasiak	Stasiak	k1gMnSc1	Stasiak
<g/>
.	.	kIx.	.
</s>
<s>
Ronald	Ronald	k1gMnSc1	Ronald
Greer	Greer	k1gMnSc1	Greer
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
velký	velký	k2eAgInSc1d1	velký
<g/>
,	,	kIx,	,
silný	silný	k2eAgInSc1d1	silný
<g/>
,	,	kIx,	,
tichý	tichý	k2eAgInSc1d1	tichý
<g/>
"	"	kIx"	"
příslušník	příslušník	k1gMnSc1	příslušník
námořní	námořní	k2eAgFnSc2d1	námořní
pěchoty	pěchota	k1gFnSc2	pěchota
s	s	k7c7	s
tajemnou	tajemný	k2eAgFnSc7d1	tajemná
minulostí	minulost	k1gFnSc7	minulost
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
nad	nad	k7c7	nad
sebou	se	k3xPyFc7	se
nemá	mít	k5eNaImIp3nS	mít
kontrolu	kontrola	k1gFnSc4	kontrola
v	v	k7c6	v
nebojových	bojový	k2eNgFnPc6d1	nebojová
situacích	situace	k1gFnPc6	situace
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
hodnost	hodnost	k1gFnSc4	hodnost
vrchního	vrchní	k2eAgMnSc2d1	vrchní
rotmistra	rotmistr	k1gMnSc2	rotmistr
<g/>
.	.	kIx.	.
</s>
<s>
Rozpis	rozpis	k1gInSc4	rozpis
postavy	postava	k1gFnSc2	postava
ho	on	k3xPp3gMnSc4	on
přirovnává	přirovnávat	k5eAaImIp3nS	přirovnávat
k	k	k7c3	k
postavě	postava	k1gFnSc3	postava
"	"	kIx"	"
<g/>
Hoot	Hoot	k1gInSc1	Hoot
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
kterou	který	k3yIgFnSc4	který
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Černý	černý	k2eAgMnSc1d1	černý
jestřáb	jestřáb	k1gMnSc1	jestřáb
sestřelen	sestřelen	k2eAgMnSc1d1	sestřelen
hraje	hrát	k5eAaImIp3nS	hrát
Eric	Eric	k1gInSc1	Eric
Bana	Ban	k1gInSc2	Ban
<g/>
.	.	kIx.	.
</s>
<s>
Camile	Camile	k6eAd1	Camile
Wrayová	Wrayový	k2eAgFnSc1d1	Wrayový
-	-	kIx~	-
Ming-Na	Ming-Na	k1gMnSc1	Ming-Na
<g/>
:	:	kIx,	:
Camile	Camila	k1gFnSc6	Camila
Wrayová	Wrayová	k1gFnSc1	Wrayová
je	být	k5eAaImIp3nS	být
první	první	k4xOgFnSc1	první
otevřeně	otevřeně	k6eAd1	otevřeně
homosexuální	homosexuální	k2eAgFnSc1d1	homosexuální
postava	postava	k1gFnSc1	postava
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
Hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
brány	brána	k1gFnSc2	brána
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zástupcem	zástupce	k1gMnSc7	zástupce
IOA	IOA	kA	IOA
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
Destiny	Destina	k1gFnSc2	Destina
a	a	k8xC	a
podporuje	podporovat	k5eAaImIp3nS	podporovat
civilní	civilní	k2eAgNnSc1d1	civilní
vedení	vedení	k1gNnSc1	vedení
na	na	k7c6	na
lodi	loď	k1gFnSc6	loď
<g/>
.	.	kIx.	.
</s>
<s>
Ming-Na	Ming-Na	k1gFnSc1	Ming-Na
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
prvních	první	k4xOgFnPc6	první
dvou	dva	k4xCgFnPc6	dva
epizodách	epizoda	k1gFnPc6	epizoda
připisována	připisován	k2eAgFnSc1d1	připisována
k	k	k7c3	k
pravidelným	pravidelný	k2eAgFnPc3d1	pravidelná
postavám	postava	k1gFnPc3	postava
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
poté	poté	k6eAd1	poté
byla	být	k5eAaImAgFnS	být
přesunuta	přesunout	k5eAaPmNgFnS	přesunout
k	k	k7c3	k
vedlejším	vedlejší	k2eAgFnPc3d1	vedlejší
postavám	postava	k1gFnPc3	postava
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
epizody	epizoda	k1gFnSc2	epizoda
"	"	kIx"	"
<g/>
Justice	justice	k1gFnSc1	justice
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
znovu	znovu	k6eAd1	znovu
pravidelnou	pravidelný	k2eAgFnSc7d1	pravidelná
postavou	postava	k1gFnSc7	postava
<g/>
.	.	kIx.	.
</s>
<s>
Plukovník	plukovník	k1gMnSc1	plukovník
David	David	k1gMnSc1	David
Telford	Telford	k1gMnSc1	Telford
-	-	kIx~	-
Lou	Lou	k1gMnSc1	Lou
Diamond	Diamond	k1gMnSc1	Diamond
Phillips	Phillips	k1gInSc4	Phillips
</s>
