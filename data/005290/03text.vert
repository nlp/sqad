<s>
Viktoriino	Viktoriin	k2eAgNnSc1d1	Viktoriino
jezero	jezero	k1gNnSc1	jezero
také	také	k9	také
Victoria	Victorium	k1gNnSc2	Victorium
Nyanza	Nyanz	k1gMnSc2	Nyanz
nebo	nebo	k8xC	nebo
Ukerewe	Ukerew	k1gMnSc2	Ukerew
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Lake	Lake	k1gNnSc1	Lake
Victoria	Victorium	k1gNnSc2	Victorium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jezero	jezero	k1gNnSc1	jezero
ve	v	k7c6	v
Východní	východní	k2eAgFnSc6d1	východní
Africe	Afrika	k1gFnSc6	Afrika
na	na	k7c6	na
území	území	k1gNnSc6	území
Tanzanie	Tanzanie	k1gFnSc2	Tanzanie
(	(	kIx(	(
<g/>
51	[number]	k4	51
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Keni	Keňa	k1gFnSc2	Keňa
(	(	kIx(	(
<g/>
6	[number]	k4	6
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ugandy	Uganda	k1gFnSc2	Uganda
(	(	kIx(	(
<g/>
43	[number]	k4	43
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
druhé	druhý	k4xOgNnSc4	druhý
největší	veliký	k2eAgNnSc4d3	veliký
sladkovodní	sladkovodní	k2eAgNnSc4d1	sladkovodní
jezero	jezero	k1gNnSc4	jezero
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
po	po	k7c6	po
Hořejším	horní	k2eAgNnSc6d2	hořejší
jezeru	jezero	k1gNnSc6	jezero
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
a	a	k8xC	a
největší	veliký	k2eAgNnSc1d3	veliký
jezero	jezero	k1gNnSc1	jezero
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
Velkých	velký	k2eAgNnPc2d1	velké
Afrických	africký	k2eAgNnPc2d1	africké
jezer	jezero	k1gNnPc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Jezero	jezero	k1gNnSc1	jezero
vyplňuje	vyplňovat	k5eAaImIp3nS	vyplňovat
mělkou	mělký	k2eAgFnSc4d1	mělká
tektonickou	tektonický	k2eAgFnSc4d1	tektonická
prohlubeň	prohlubeň	k1gFnSc4	prohlubeň
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
Východoafrické	východoafrický	k2eAgFnSc2d1	východoafrická
pahorkatiny	pahorkatina	k1gFnSc2	pahorkatina
a	a	k8xC	a
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
je	být	k5eAaImIp3nS	být
zahrazeno	zahradit	k5eAaPmNgNnS	zahradit
lávovým	lávový	k2eAgInSc7d1	lávový
proudem	proud	k1gInSc7	proud
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
68	[number]	k4	68
800	[number]	k4	800
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
maximálně	maximálně	k6eAd1	maximálně
320	[number]	k4	320
km	km	kA	km
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
a	a	k8xC	a
275	[number]	k4	275
km	km	kA	km
široké	široký	k2eAgNnSc1d1	široké
<g/>
.	.	kIx.	.
</s>
<s>
Průměrně	průměrně	k6eAd1	průměrně
je	být	k5eAaImIp3nS	být
hluboké	hluboký	k2eAgFnPc1d1	hluboká
40	[number]	k4	40
m	m	kA	m
a	a	k8xC	a
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
maximální	maximální	k2eAgFnSc2d1	maximální
hloubky	hloubka	k1gFnSc2	hloubka
80	[number]	k4	80
m.	m.	k?	m.
Objem	objem	k1gInSc1	objem
vody	voda	k1gFnSc2	voda
je	být	k5eAaImIp3nS	být
2700	[number]	k4	2700
km3	km3	k4	km3
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
1134	[number]	k4	1134
m.	m.	k?	m.
Severní	severní	k2eAgMnSc1d1	severní
<g/>
,	,	kIx,	,
východní	východní	k2eAgInPc1d1	východní
a	a	k8xC	a
jižní	jižní	k2eAgInPc1d1	jižní
břehy	břeh	k1gInPc1	břeh
jsou	být	k5eAaImIp3nP	být
nízké	nízký	k2eAgInPc1d1	nízký
<g/>
,	,	kIx,	,
písčité	písčitý	k2eAgInPc1d1	písčitý
s	s	k7c7	s
množstvím	množství	k1gNnSc7	množství
zálivů	záliv	k1gInPc2	záliv
a	a	k8xC	a
poloostrovů	poloostrov	k1gInPc2	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgInSc1d1	západní
břeh	břeh	k1gInSc1	břeh
je	být	k5eAaImIp3nS	být
vyšší	vysoký	k2eAgInSc1d2	vyšší
a	a	k8xC	a
přímý	přímý	k2eAgInSc1d1	přímý
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
pobřeží	pobřeží	k1gNnSc2	pobřeží
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
7000	[number]	k4	7000
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgInPc1d2	veliký
zálivy	záliv	k1gInPc1	záliv
jsou	být	k5eAaImIp3nP	být
Kavirondo	Kavirondo	k6eAd1	Kavirondo
a	a	k8xC	a
Spika	Spika	k1gFnSc1	Spika
<g/>
.	.	kIx.	.
</s>
<s>
Jezero	jezero	k1gNnSc1	jezero
je	být	k5eAaImIp3nS	být
obklopeno	obklopit	k5eAaPmNgNnS	obklopit
savanami	savana	k1gFnPc7	savana
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
k	k	k7c3	k
pobřeží	pobřeží	k1gNnSc3	pobřeží
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
vlhký	vlhký	k2eAgInSc4d1	vlhký
částečně	částečně	k6eAd1	částečně
opadavý	opadavý	k2eAgInSc4d1	opadavý
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
věčně	věčně	k6eAd1	věčně
zelený	zelený	k2eAgInSc1d1	zelený
les	les	k1gInSc1	les
(	(	kIx(	(
<g/>
převážně	převážně	k6eAd1	převážně
sekundární	sekundární	k2eAgMnSc1d1	sekundární
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poblíž	poblíž	k7c2	poblíž
východních	východní	k2eAgInPc2d1	východní
břehů	břeh	k1gInPc2	břeh
se	se	k3xPyFc4	se
těží	těžet	k5eAaImIp3nS	těžet
zlato	zlato	k1gNnSc1	zlato
a	a	k8xC	a
diamanty	diamant	k1gInPc1	diamant
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jezeře	jezero	k1gNnSc6	jezero
je	být	k5eAaImIp3nS	být
mnoho	mnoho	k4c1	mnoho
ostrovů	ostrov	k1gInPc2	ostrov
o	o	k7c6	o
celkové	celkový	k2eAgFnSc6d1	celková
ploše	plocha	k1gFnSc6	plocha
přibližně	přibližně	k6eAd1	přibližně
6	[number]	k4	6
000	[number]	k4	000
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnPc1d3	veliký
jsou	být	k5eAaImIp3nP	být
Ukerewe	Ukerew	k1gFnPc1	Ukerew
a	a	k8xC	a
souostroví	souostroví	k1gNnSc1	souostroví
Sese	Sese	k?	Sese
<g/>
.	.	kIx.	.
</s>
<s>
Zdrojem	zdroj	k1gInSc7	zdroj
vody	voda	k1gFnSc2	voda
jsou	být	k5eAaImIp3nP	být
atmosférické	atmosférický	k2eAgFnPc1d1	atmosférická
srážky	srážka	k1gFnPc1	srážka
a	a	k8xC	a
přítok	přítok	k1gInSc4	přítok
mnoha	mnoho	k4c2	mnoho
řek	řeka	k1gFnPc2	řeka
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgInPc7	jenž
je	být	k5eAaImIp3nS	být
nejvodnější	vodný	k2eAgFnSc1d3	vodný
Kagera	Kagera	k1gFnSc1	Kagera
(	(	kIx(	(
<g/>
horní	horní	k2eAgInSc1d1	horní
tok	tok	k1gInSc1	tok
Nilu	Nil	k1gInSc2	Nil
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
roční	roční	k2eAgInSc1d1	roční
příbytek	příbytek	k1gInSc1	příbytek
vody	voda	k1gFnSc2	voda
činí	činit	k5eAaImIp3nS	činit
114	[number]	k4	114
km3	km3	k4	km3
(	(	kIx(	(
<g/>
16	[number]	k4	16
km3	km3	k4	km3
přítok	přítok	k1gInSc1	přítok
řek	řeka	k1gFnPc2	řeka
a	a	k8xC	a
98	[number]	k4	98
km3	km3	k4	km3
srážky	srážka	k1gFnPc1	srážka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Úbytek	úbytek	k1gInSc4	úbytek
vody	voda	k1gFnSc2	voda
vypařováním	vypařování	k1gNnSc7	vypařování
je	být	k5eAaImIp3nS	být
93	[number]	k4	93
km3	km3	k4	km3
<g/>
.	.	kIx.	.
</s>
<s>
Odtok	odtok	k1gInSc1	odtok
po	po	k7c6	po
Viktoriině	Viktoriin	k2eAgInSc6d1	Viktoriin
Nilu	Nil	k1gInSc6	Nil
(	(	kIx(	(
<g/>
21	[number]	k4	21
km3	km3	k4	km3
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
regulován	regulovat	k5eAaImNgInS	regulovat
přehradou	přehrada	k1gFnSc7	přehrada
hydroelektrárny	hydroelektrárna	k1gFnSc2	hydroelektrárna
Owen	Owena	k1gFnPc2	Owena
Falls	Falls	k1gInSc1	Falls
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
2,5	[number]	k4	2,5
km	km	kA	km
od	od	k7c2	od
odtoku	odtok	k1gInSc2	odtok
řeky	řeka	k1gFnSc2	řeka
z	z	k7c2	z
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
roční	roční	k2eAgFnSc1d1	roční
odchylka	odchylka	k1gFnSc1	odchylka
úrovně	úroveň	k1gFnSc2	úroveň
hladiny	hladina	k1gFnSc2	hladina
je	být	k5eAaImIp3nS	být
0,3	[number]	k4	0,3
m	m	kA	m
(	(	kIx(	(
<g/>
maximální	maximální	k2eAgFnSc4d1	maximální
1,74	[number]	k4	1,74
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jezeře	jezero	k1gNnSc6	jezero
jsou	být	k5eAaImIp3nP	být
charakteristické	charakteristický	k2eAgFnPc4d1	charakteristická
bouře	bouř	k1gFnPc4	bouř
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
způsobované	způsobovaný	k2eAgInPc1d1	způsobovaný
uragány	uragán	k1gInPc1	uragán
v	v	k7c6	v
období	období	k1gNnSc6	období
tropických	tropický	k2eAgFnPc2d1	tropická
bouří	bouř	k1gFnPc2	bouř
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
podstatnému	podstatný	k2eAgInSc3d1	podstatný
poklesu	pokles	k1gInSc3	pokles
hladiny	hladina	k1gFnSc2	hladina
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
poklesu	pokles	k1gInSc2	pokles
srážek	srážka	k1gFnPc2	srážka
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
spojeného	spojený	k2eAgInSc2d1	spojený
s	s	k7c7	s
globálními	globální	k2eAgFnPc7d1	globální
změnami	změna	k1gFnPc7	změna
klimatu	klima	k1gNnSc2	klima
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
odčerpávání	odčerpávání	k1gNnSc2	odčerpávání
vody	voda	k1gFnSc2	voda
vodními	vodní	k2eAgFnPc7d1	vodní
elektrárnami	elektrárna	k1gFnPc7	elektrárna
v	v	k7c6	v
Ugandě	Uganda	k1gFnSc6	Uganda
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnPc1d1	základní
průmyslové	průmyslový	k2eAgFnPc1d1	průmyslová
ryby	ryba	k1gFnPc1	ryba
jsou	být	k5eAaImIp3nP	být
tilapia	tilapium	k1gNnSc2	tilapium
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
a	a	k8xC	a
ostrovech	ostrov	k1gInPc6	ostrov
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
moucha	moucha	k1gFnSc1	moucha
tse-tse	tsese	k1gFnSc1	tse-tse
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
byl	být	k5eAaImAgMnS	být
ve	v	k7c6	v
Viktoriině	Viktoriin	k2eAgNnSc6d1	Viktoriino
jezeře	jezero	k1gNnSc6	jezero
vysazen	vysazen	k2eAgMnSc1d1	vysazen
jako	jako	k8xS	jako
nepůvodní	původní	k2eNgMnSc1d1	nepůvodní
druh	druh	k1gMnSc1	druh
okoun	okoun	k1gMnSc1	okoun
nilský	nilský	k2eAgMnSc1d1	nilský
a	a	k8xC	a
způsobil	způsobit	k5eAaPmAgInS	způsobit
zde	zde	k6eAd1	zde
ekologickou	ekologický	k2eAgFnSc4d1	ekologická
katastrofu	katastrofa	k1gFnSc4	katastrofa
-	-	kIx~	-
vyhynutí	vyhynutí	k1gNnPc1	vyhynutí
nebo	nebo	k8xC	nebo
téměř	téměř	k6eAd1	téměř
vyhubení	vyhubení	k1gNnSc2	vyhubení
několika	několik	k4yIc2	několik
stovek	stovka	k1gFnPc2	stovka
původních	původní	k2eAgInPc2d1	původní
druhů	druh	k1gInPc2	druh
ryb	ryba	k1gFnPc2	ryba
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
cichlid	cichlida	k1gFnPc2	cichlida
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
jezeře	jezero	k1gNnSc6	jezero
žere	žrát	k5eAaImIp3nS	žrát
hlavně	hlavně	k9	hlavně
cichlidy	cichlida	k1gFnPc4	cichlida
rodu	rod	k1gInSc2	rod
Haplochromis	Haplochromis	k1gFnPc2	Haplochromis
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
tak	tak	k6eAd1	tak
decimuje	decimovat	k5eAaBmIp3nS	decimovat
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
byly	být	k5eAaImAgInP	být
výlovy	výlov	k1gInPc1	výlov
okouna	okoun	k1gMnSc2	okoun
nilského	nilský	k2eAgMnSc2d1	nilský
ve	v	k7c6	v
Viktoriině	Viktoriin	k2eAgNnSc6d1	Viktoriino
jezeře	jezero	k1gNnSc6	jezero
malé	malý	k2eAgFnPc1d1	malá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
jeho	jeho	k3xOp3gInSc1	jeho
výlov	výlov	k1gInSc1	výlov
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
až	až	k9	až
na	na	k7c4	na
90	[number]	k4	90
%	%	kIx~	%
ryb	ryba	k1gFnPc2	ryba
vylovených	vylovený	k2eAgFnPc2d1	vylovená
pomocí	pomocí	k7c2	pomocí
vlečných	vlečný	k2eAgFnPc2d1	vlečná
sítí	síť	k1gFnPc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
Okoun	okoun	k1gMnSc1	okoun
nilský	nilský	k2eAgMnSc1d1	nilský
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
tvoří	tvořit	k5eAaImIp3nP	tvořit
významnou	významný	k2eAgFnSc4d1	významná
část	část	k1gFnSc4	část
ekonomiky	ekonomika	k1gFnSc2	ekonomika
oblastí	oblast	k1gFnPc2	oblast
okolo	okolo	k7c2	okolo
jezera	jezero	k1gNnSc2	jezero
a	a	k8xC	a
výrobky	výrobek	k1gInPc7	výrobek
z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
se	se	k3xPyFc4	se
vyvážejí	vyvážet	k5eAaImIp3nP	vyvážet
i	i	k9	i
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
USA	USA	kA	USA
a	a	k8xC	a
na	na	k7c4	na
Blízký	blízký	k2eAgInSc4d1	blízký
východ	východ	k1gInSc4	východ
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jezeře	jezero	k1gNnSc6	jezero
je	být	k5eAaImIp3nS	být
rozvinutá	rozvinutý	k2eAgFnSc1d1	rozvinutá
místní	místní	k2eAgFnSc1d1	místní
lodní	lodní	k2eAgFnSc1d1	lodní
doprava	doprava	k1gFnSc1	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Síť	síť	k1gFnSc1	síť
vodních	vodní	k2eAgFnPc2d1	vodní
cest	cesta	k1gFnPc2	cesta
na	na	k7c6	na
jezeře	jezero	k1gNnSc6	jezero
spojuje	spojovat	k5eAaImIp3nS	spojovat
města	město	k1gNnPc4	město
a	a	k8xC	a
vesnice	vesnice	k1gFnPc4	vesnice
ležící	ležící	k2eAgFnPc4d1	ležící
na	na	k7c6	na
březích	břeh	k1gInPc6	břeh
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInPc1d1	hlavní
přístavy	přístav	k1gInPc1	přístav
jsou	být	k5eAaImIp3nP	být
Entebbe	Entebb	k1gInSc5	Entebb
(	(	kIx(	(
<g/>
Uganda	Uganda	k1gFnSc1	Uganda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mwanza	Mwanza	k1gFnSc1	Mwanza
a	a	k8xC	a
Bukoba	Bukoba	k1gFnSc1	Bukoba
(	(	kIx(	(
<g/>
Tanzanie	Tanzanie	k1gFnSc1	Tanzanie
<g/>
)	)	kIx)	)
a	a	k8xC	a
Kisumu	Kisum	k1gInSc2	Kisum
(	(	kIx(	(
<g/>
Keňa	Keňa	k1gFnSc1	Keňa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
Jinja	Jinja	k1gFnSc1	Jinja
<g/>
,	,	kIx,	,
Port	port	k1gInSc1	port
Bell	bell	k1gInSc1	bell
a	a	k8xC	a
Musoma	Musoma	k1gFnSc1	Musoma
<g/>
.	.	kIx.	.
</s>
<s>
Jezero	jezero	k1gNnSc1	jezero
je	být	k5eAaImIp3nS	být
spojeno	spojit	k5eAaPmNgNnS	spojit
s	s	k7c7	s
pobřežím	pobřeží	k1gNnSc7	pobřeží
Indického	indický	k2eAgInSc2d1	indický
oceánu	oceán	k1gInSc2	oceán
železnicemi	železnice	k1gFnPc7	železnice
Kisumu	Kisum	k1gInSc3	Kisum
–	–	k?	–
Mombasa	Mombas	k1gMnSc2	Mombas
(	(	kIx(	(
<g/>
Keňa	Keňa	k1gFnSc1	Keňa
<g/>
)	)	kIx)	)
a	a	k8xC	a
Mwanza	Mwanza	k1gFnSc1	Mwanza
–	–	k?	–
Tabora	Tabora	k1gFnSc1	Tabora
–	–	k?	–
Dar	dar	k1gInSc1	dar
es	es	k1gNnSc1	es
Salaam	Salaam	k1gInSc1	Salaam
(	(	kIx(	(
<g/>
Tanzanie	Tanzanie	k1gFnSc1	Tanzanie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jezero	jezero	k1gNnSc1	jezero
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
ve	v	k7c6	v
středním	střední	k2eAgInSc6d1	střední
antropogénu	antropogén	k1gInSc6	antropogén
zároveň	zároveň	k6eAd1	zároveň
se	s	k7c7	s
vznikem	vznik	k1gInSc7	vznik
jezerních	jezerní	k2eAgFnPc2d1	jezerní
propadlin	propadlina	k1gFnPc2	propadlina
Albertova	Albertův	k2eAgNnSc2d1	Albertovo
a	a	k8xC	a
Edwardova	Edwardův	k2eAgNnSc2d1	Edwardovo
jezera	jezero	k1gNnSc2	jezero
v	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
africké	africký	k2eAgFnSc6d1	africká
části	část	k1gFnSc6	část
Velké	velký	k2eAgFnSc2d1	velká
příkopové	příkopový	k2eAgFnSc2d1	příkopová
propadliny	propadlina	k1gFnSc2	propadlina
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
byl	být	k5eAaImAgMnS	být
narušen	narušen	k2eAgInSc4d1	narušen
dřívější	dřívější	k2eAgInSc4d1	dřívější
odtok	odtok	k1gInSc4	odtok
do	do	k7c2	do
Konžské	konžský	k2eAgFnSc2d1	Konžská
pánve	pánev	k1gFnSc2	pánev
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
systém	systém	k1gInSc1	systém
vyúsťoval	vyúsťovat	k5eAaImAgInS	vyúsťovat
na	na	k7c4	na
východ	východ	k1gInSc4	východ
k	k	k7c3	k
prohnutí	prohnutí	k1gNnSc3	prohnutí
pahorkatiny	pahorkatina	k1gFnSc2	pahorkatina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
obrovské	obrovský	k2eAgNnSc1d1	obrovské
jezero	jezero	k1gNnSc1	jezero
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
rozloha	rozloha	k1gFnSc1	rozloha
byla	být	k5eAaImAgFnS	být
zvlášť	zvlášť	k6eAd1	zvlášť
velká	velký	k2eAgFnSc1d1	velká
v	v	k7c6	v
období	období	k1gNnSc6	období
dešťů	dešť	k1gInPc2	dešť
<g/>
.	.	kIx.	.
</s>
<s>
Staré	Staré	k2eAgNnSc1d1	Staré
jezero	jezero	k1gNnSc1	jezero
získalo	získat	k5eAaPmAgNnS	získat
odtok	odtok	k1gInSc4	odtok
na	na	k7c4	na
západ	západ	k1gInSc4	západ
do	do	k7c2	do
Albertova	Albertův	k2eAgNnSc2d1	Albertovo
jezera	jezero	k1gNnSc2	jezero
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
do	do	k7c2	do
systému	systém	k1gInSc2	systém
Nilu	Nil	k1gInSc2	Nil
přes	přes	k7c4	přes
Viktoriin	Viktoriin	k2eAgInSc4d1	Viktoriin
Nil	Nil	k1gInSc4	Nil
vznikem	vznik	k1gInSc7	vznik
Murchisonových	Murchisonův	k2eAgInPc2d1	Murchisonův
vodopádů	vodopád	k1gInPc2	vodopád
<g/>
.	.	kIx.	.
</s>
<s>
Viktoriino	Viktoriin	k2eAgNnSc1d1	Viktoriino
jezero	jezero	k1gNnSc1	jezero
i	i	k9	i
severně	severně	k6eAd1	severně
ležící	ležící	k2eAgNnSc1d1	ležící
jezero	jezero	k1gNnSc1	jezero
Kyoga	Kyog	k1gMnSc2	Kyog
jsou	být	k5eAaImIp3nP	být
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
starého	starý	k1gMnSc2	starý
obrovského	obrovský	k2eAgNnSc2d1	obrovské
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Viktoriino	Viktoriin	k2eAgNnSc1d1	Viktoriino
jezero	jezero	k1gNnSc1	jezero
bylo	být	k5eAaImAgNnS	být
objeveno	objevit	k5eAaPmNgNnS	objevit
v	v	k7c6	v
r	r	kA	r
1858	[number]	k4	1858
anglickým	anglický	k2eAgMnSc7d1	anglický
cestovatelem	cestovatel	k1gMnSc7	cestovatel
J.	J.	kA	J.
Spekem	Speek	k1gMnSc7	Speek
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
pojmenováno	pojmenovat	k5eAaPmNgNnS	pojmenovat
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
královny	královna	k1gFnSc2	královna
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
většina	většina	k1gFnSc1	většina
jezer	jezero	k1gNnPc2	jezero
tohoto	tento	k3xDgInSc2	tento
regionu	region	k1gInSc2	region
je	být	k5eAaImIp3nS	být
i	i	k9	i
Viktoriino	Viktoriin	k2eAgNnSc1d1	Viktoriino
jezero	jezero	k1gNnSc1	jezero
zamořeno	zamořen	k2eAgNnSc1d1	zamořeno
původcem	původce	k1gMnSc7	původce
schistosomózy	schistosomóza	k1gFnPc4	schistosomóza
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
též	též	k9	též
bilharziózy	bilharzióza	k1gFnSc2	bilharzióza
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
díky	díky	k7c3	díky
této	tento	k3xDgFnSc3	tento
krevní	krevní	k2eAgFnSc3d1	krevní
motolici	motolice	k1gFnSc3	motolice
rodu	rod	k1gInSc2	rod
krevnička	krevnička	k1gFnSc1	krevnička
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
koupání	koupání	k1gNnSc2	koupání
v	v	k7c6	v
jezeře	jezero	k1gNnSc6	jezero
vysoce	vysoce	k6eAd1	vysoce
nebezpečné	bezpečný	k2eNgInPc1d1	nebezpečný
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
infekci	infekce	k1gFnSc3	infekce
totiž	totiž	k9	totiž
běžně	běžně	k6eAd1	běžně
dochází	docházet	k5eAaImIp3nS	docházet
jakýmkoli	jakýkoli	k3yIgInSc7	jakýkoli
kontaktem	kontakt	k1gInSc7	kontakt
těla	tělo	k1gNnSc2	tělo
s	s	k7c7	s
kontaminovanou	kontaminovaný	k2eAgFnSc7d1	kontaminovaná
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
až	až	k9	až
k	k	k7c3	k
cirhóze	cirhóza	k1gFnSc3	cirhóza
jater	játra	k1gNnPc2	játra
<g/>
,	,	kIx,	,
totálnímu	totální	k2eAgNnSc3d1	totální
ucpání	ucpání	k1gNnSc3	ucpání
střev	střevo	k1gNnPc2	střevo
<g/>
,	,	kIx,	,
anémii	anémie	k1gFnSc4	anémie
či	či	k8xC	či
selhání	selhání	k1gNnSc4	selhání
ledvin	ledvina	k1gFnPc2	ledvina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgFnP	být
použity	použit	k2eAgFnPc1d1	použita
informace	informace	k1gFnPc1	informace
z	z	k7c2	z
Velké	velký	k2eAgFnSc2d1	velká
sovětské	sovětský	k2eAgFnSc2d1	sovětská
encyklopedie	encyklopedie	k1gFnSc2	encyklopedie
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
"	"	kIx"	"
<g/>
В	В	k?	В
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Viktoriino	Viktoriin	k2eAgNnSc1d1	Viktoriino
jezero	jezero	k1gNnSc1	jezero
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
