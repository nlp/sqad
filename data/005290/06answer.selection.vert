<s>
Viktoriino	Viktoriin	k2eAgNnSc1d1	Viktoriino
jezero	jezero	k1gNnSc1	jezero
také	také	k9	také
Victoria	Victorium	k1gNnSc2	Victorium
Nyanza	Nyanz	k1gMnSc2	Nyanz
nebo	nebo	k8xC	nebo
Ukerewe	Ukerew	k1gMnSc2	Ukerew
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Lake	Lake	k1gNnSc1	Lake
Victoria	Victorium	k1gNnSc2	Victorium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jezero	jezero	k1gNnSc1	jezero
ve	v	k7c6	v
Východní	východní	k2eAgFnSc6d1	východní
Africe	Afrika	k1gFnSc6	Afrika
na	na	k7c6	na
území	území	k1gNnSc6	území
Tanzanie	Tanzanie	k1gFnSc2	Tanzanie
(	(	kIx(	(
<g/>
51	[number]	k4	51
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Keni	Keňa	k1gFnSc2	Keňa
(	(	kIx(	(
<g/>
6	[number]	k4	6
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ugandy	Uganda	k1gFnSc2	Uganda
(	(	kIx(	(
<g/>
43	[number]	k4	43
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
