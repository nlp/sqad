<s>
Proteosyntéza	Proteosyntéza	k1gFnSc1	Proteosyntéza
je	být	k5eAaImIp3nS	být
proces	proces	k1gInSc4	proces
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nP	tvořit
bílkoviny	bílkovina	k1gFnPc1	bílkovina
<g/>
.	.	kIx.	.
</s>
<s>
Proteosyntéza	Proteosyntéza	k1gFnSc1	Proteosyntéza
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
kroků	krok	k1gInPc2	krok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
přepisu	přepis	k1gInSc3	přepis
(	(	kIx(	(
<g/>
transkripci	transkripce	k1gFnSc4	transkripce
<g/>
)	)	kIx)	)
genetického	genetický	k2eAgInSc2d1	genetický
kódu	kód	k1gInSc2	kód
DNA	DNA	kA	DNA
do	do	k7c2	do
mRNA	mRNA	k?	mRNA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
překladu	překlad	k1gInSc3	překlad
(	(	kIx(	(
<g/>
translaci	translace	k1gFnSc4	translace
<g/>
)	)	kIx)	)
kódu	kód	k1gInSc2	kód
z	z	k7c2	z
mRNA	mRNA	k?	mRNA
a	a	k8xC	a
k	k	k7c3	k
tvorbě	tvorba	k1gFnSc3	tvorba
bílkovin	bílkovina	k1gFnPc2	bílkovina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lidských	lidský	k2eAgFnPc6d1	lidská
buňkách	buňka	k1gFnPc6	buňka
probíhá	probíhat	k5eAaImIp3nS	probíhat
transkripce	transkripce	k1gFnSc1	transkripce
v	v	k7c6	v
buněčném	buněčný	k2eAgNnSc6d1	buněčné
jádru	jádro	k1gNnSc6	jádro
a	a	k8xC	a
translace	translace	k1gFnSc1	translace
na	na	k7c6	na
ribozomech	ribozom	k1gInPc6	ribozom
<g/>
.	.	kIx.	.
</s>
<s>
Proteosyntéza	Proteosyntéza	k1gFnSc1	Proteosyntéza
je	být	k5eAaImIp3nS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
iniciační	iniciační	k2eAgFnSc3d1	iniciační
tRNA	trnout	k5eAaImSgInS	trnout
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
jest	být	k5eAaImIp3nS	být
tou	ten	k3xDgFnSc7	ten
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nese	nést	k5eAaImIp3nS	nést
methionin	methionin	k1gInSc4	methionin
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
naváže	navázat	k5eAaPmIp3nS	navázat
na	na	k7c4	na
malou	malý	k2eAgFnSc4d1	malá
ribozomální	ribozomální	k2eAgFnSc4d1	ribozomální
podjednotku	podjednotka	k1gFnSc4	podjednotka
a	a	k8xC	a
začne	začít	k5eAaPmIp3nS	začít
pomalu	pomalu	k6eAd1	pomalu
projíždět	projíždět	k5eAaImF	projíždět
molekulu	molekula	k1gFnSc4	molekula
mRNA	mRNA	k?	mRNA
od	od	k7c2	od
5	[number]	k4	5
<g/>
'	'	kIx"	'
konce	konec	k1gInSc2	konec
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
objeví	objevit	k5eAaPmIp3nS	objevit
iniciační	iniciační	k2eAgFnSc4d1	iniciační
sekvenci	sekvence	k1gFnSc4	sekvence
AUG	AUG	kA	AUG
-	-	kIx~	-
naváže	navázat	k5eAaPmIp3nS	navázat
se	se	k3xPyFc4	se
a	a	k8xC	a
translace	translace	k1gFnSc1	translace
začíná	začínat	k5eAaImIp3nS	začínat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
další	další	k2eAgFnPc4d1	další
sekvence	sekvence	k1gFnPc4	sekvence
(	(	kIx(	(
<g/>
kodóny	kodóna	k1gFnPc4	kodóna
<g/>
)	)	kIx)	)
nasedají	nasedat	k5eAaImIp3nP	nasedat
další	další	k2eAgFnPc1d1	další
tRNA	trnout	k5eAaImSgMnS	trnout
podle	podle	k7c2	podle
komplementarity	komplementarita	k1gFnSc2	komplementarita
bází	báze	k1gFnPc2	báze
(	(	kIx(	(
<g/>
systém	systém	k1gInSc1	systém
kodon	kodon	k1gNnSc1	kodon
na	na	k7c4	na
mRNA	mRNA	k?	mRNA
-	-	kIx~	-
antikodon	antikodon	k1gMnSc1	antikodon
na	na	k7c6	na
tRNA	trnout	k5eAaImSgInS	trnout
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
přinesenými	přinesený	k2eAgFnPc7d1	přinesená
aminokyselinami	aminokyselina	k1gFnPc7	aminokyselina
vznikají	vznikat	k5eAaImIp3nP	vznikat
peptidové	peptid	k1gMnPc1	peptid
vazby	vazba	k1gFnSc2	vazba
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tuto	tento	k3xDgFnSc4	tento
část	část	k1gFnSc4	část
translace	translace	k1gFnSc2	translace
-	-	kIx~	-
elongaci	elongace	k1gFnSc4	elongace
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
zodpovědná	zodpovědný	k2eAgFnSc1d1	zodpovědná
zejména	zejména	k9	zejména
velká	velký	k2eAgFnSc1d1	velká
ribozomální	ribozomální	k2eAgFnSc1d1	ribozomální
podjednotka	podjednotka	k1gFnSc1	podjednotka
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
zbývá	zbývat	k5eAaImIp3nS	zbývat
již	již	k6eAd1	již
jen	jen	k9	jen
kodon	kodon	k1gInSc1	kodon
beze	beze	k7c2	beze
smyslu	smysl	k1gInSc2	smysl
(	(	kIx(	(
<g/>
terminační	terminační	k2eAgMnSc1d1	terminační
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
proteosyntéza	proteosyntéza	k1gFnSc1	proteosyntéza
ukončena	ukončit	k5eAaPmNgFnS	ukončit
a	a	k8xC	a
vzniklé	vzniklý	k2eAgNnSc1d1	vzniklé
polypeptidové	polypeptidový	k2eAgNnSc1d1	polypeptidový
vlákno	vlákno	k1gNnSc1	vlákno
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
dále	daleko	k6eAd2	daleko
v	v	k7c6	v
buňce	buňka	k1gFnSc6	buňka
upravováno	upravován	k2eAgNnSc1d1	upravováno
na	na	k7c4	na
požadovanou	požadovaný	k2eAgFnSc4d1	požadovaná
bílkovinu	bílkovina	k1gFnSc4	bílkovina
zejména	zejména	k9	zejména
pomocí	pomocí	k7c2	pomocí
posttranslačních	posttranslační	k2eAgFnPc2d1	posttranslační
modifikací	modifikace	k1gFnPc2	modifikace
<g/>
.	.	kIx.	.
</s>
<s>
Drsné	drsný	k2eAgNnSc1d1	drsné
endoplazmatické	endoplazmatický	k2eAgNnSc1d1	endoplazmatické
retikulum	retikulum	k1gNnSc1	retikulum
(	(	kIx(	(
<g/>
s	s	k7c7	s
ribozomy	ribozom	k1gInPc7	ribozom
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
specializováno	specializován	k2eAgNnSc1d1	specializováno
pro	pro	k7c4	pro
tvorbu	tvorba	k1gFnSc4	tvorba
mimobuněčných	mimobuněčný	k2eAgInPc2d1	mimobuněčný
nebo	nebo	k8xC	nebo
transmembránových	transmembránův	k2eAgInPc2d1	transmembránův
proteinů	protein	k1gInPc2	protein
(	(	kIx(	(
<g/>
různé	různý	k2eAgInPc4d1	různý
iontové	iontový	k2eAgInPc4d1	iontový
kanály	kanál	k1gInPc4	kanál
či	či	k8xC	či
receptory	receptor	k1gInPc4	receptor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
zabudují	zabudovat	k5eAaPmIp3nP	zabudovat
do	do	k7c2	do
vnitřku	vnitřek	k1gInSc2	vnitřek
membrán	membrána	k1gFnPc2	membrána
(	(	kIx(	(
<g/>
sekreční	sekreční	k2eAgFnSc1d1	sekreční
granula	granula	k1gFnSc1	granula
<g/>
,	,	kIx,	,
lysozomy	lysozom	k1gInPc1	lysozom
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
volné	volný	k2eAgInPc1d1	volný
ribozomy	ribozom	k1gInPc1	ribozom
se	se	k3xPyFc4	se
podílejí	podílet	k5eAaImIp3nP	podílet
především	především	k9	především
na	na	k7c6	na
tvorbě	tvorba	k1gFnSc6	tvorba
cytosolických	cytosolický	k2eAgInPc2d1	cytosolický
proteinů	protein	k1gInPc2	protein
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
proteosyntéze	proteosyntéza	k1gFnSc3	proteosyntéza
však	však	k9	však
dochází	docházet	k5eAaImIp3nS	docházet
i	i	k9	i
v	v	k7c6	v
semiautonomních	semiautonomní	k2eAgFnPc6d1	semiautonomní
organelách	organela	k1gFnPc6	organela
(	(	kIx(	(
<g/>
mitochondrie	mitochondrie	k1gFnSc1	mitochondrie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Proteolýza	proteolýza	k1gFnSc1	proteolýza
Translace	translace	k1gFnSc2	translace
</s>
