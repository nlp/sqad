<s>
Palindrom	palindrom	k1gInSc1	palindrom
(	(	kIx(	(
<g/>
z	z	k7c2	z
řec.	řec.	k?	řec.
palindromos	palindromosa	k1gFnPc2	palindromosa
<g/>
,	,	kIx,	,
běžící	běžící	k2eAgFnPc1d1	běžící
pozpátku	pozpátku	k6eAd1	pozpátku
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
slovo	slovo	k1gNnSc1	slovo
<g/>
,	,	kIx,	,
věta	věta	k1gFnSc1	věta
<g/>
,	,	kIx,	,
číslo	číslo	k1gNnSc1	číslo
nebo	nebo	k8xC	nebo
melodie	melodie	k1gFnSc1	melodie
(	(	kIx(	(
<g/>
obecně	obecně	k6eAd1	obecně
jakákoliv	jakýkoliv	k3yIgFnSc1	jakýkoliv
sekvence	sekvence	k1gFnSc1	sekvence
symbolů	symbol	k1gInPc2	symbol
<g/>
,	,	kIx,	,
např.	např.	kA	např.
sekvence	sekvence	k1gFnSc1	sekvence
DNA	dna	k1gFnSc1	dna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
tu	ten	k3xDgFnSc4	ten
vlastnost	vlastnost	k1gFnSc4	vlastnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
lze	lze	k6eAd1	lze
číst	číst	k5eAaImF	číst
v	v	k7c6	v
libovolném	libovolný	k2eAgInSc6d1	libovolný
směru	směr	k1gInSc6	směr
(	(	kIx(	(
<g/>
zprava	zprava	k6eAd1	zprava
doleva	doleva	k6eAd1	doleva
nebo	nebo	k8xC	nebo
zleva	zleva	k6eAd1	zleva
doprava	doprava	k1gFnSc1	doprava
<g/>
)	)	kIx)	)
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
vždy	vždy	k6eAd1	vždy
stejný	stejný	k2eAgInSc4d1	stejný
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
posuzování	posuzování	k1gNnSc6	posuzování
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
stejný	stejný	k2eAgInSc4d1	stejný
význam	význam	k1gInSc4	význam
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
neberou	brát	k5eNaImIp3nP	brát
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
mezery	mezera	k1gFnSc2	mezera
mezi	mezi	k7c7	mezi
slovy	slovo	k1gNnPc7	slovo
a	a	k8xC	a
diakritika	diakritika	k1gFnSc1	diakritika
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
použita	použít	k5eAaPmNgFnS	použít
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Anna	Anna	k1gFnSc1	Anna
kajak	kajak	k1gInSc4	kajak
krk	krk	k1gInSc1	krk
madam	madam	k1gFnPc2	madam
oko	oko	k1gNnSc1	oko
Nejdelšími	dlouhý	k2eAgInPc7d3	nejdelší
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
českými	český	k2eAgMnPc7d1	český
palindromatickými	palindromatický	k2eAgFnPc7d1	palindromatický
slovy	slovo	k1gNnPc7	slovo
jsou	být	k5eAaImIp3nP	být
trpné	trpný	k2eAgInPc4d1	trpný
tvary	tvar	k1gInPc4	tvar
některých	některý	k3yIgNnPc2	některý
záporných	záporný	k2eAgNnPc2d1	záporné
sloves	sloveso	k1gNnPc2	sloveso
<g/>
:	:	kIx,	:
nepochopen	pochopen	k2eNgInSc1d1	nepochopen
<g/>
,	,	kIx,	,
nepotopen	potopen	k2eNgInSc1d1	potopen
<g/>
,	,	kIx,	,
nezasazen	zasazen	k2eNgInSc1d1	zasazen
<g/>
,	,	kIx,	,
nezařazen	zařazen	k2eNgInSc1d1	nezařazen
<g/>
.	.	kIx.	.
</s>
<s>
Geografickým	geografický	k2eAgInSc7d1	geografický
palindromem	palindrom	k1gInSc7	palindrom
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
grónské	grónský	k2eAgNnSc1d1	grónské
městečko	městečko	k1gNnSc1	městečko
Qaanaaq	Qaanaaq	k1gFnSc2	Qaanaaq
<g/>
.	.	kIx.	.
</s>
<s>
Jelenovi	Jelenův	k2eAgMnPc1d1	Jelenův
pivo	pivo	k1gNnSc4	pivo
nelej	lít	k5eNaImRp2nS	lít
<g/>
!	!	kIx.	!
</s>
<s>
A	a	k9	a
dál	daleko	k6eAd2	daleko
vidí	vidět	k5eAaImIp3nP	vidět
lítat	lítat	k5eAaImF	lítat
netopýry	netopýr	k1gMnPc4	netopýr
potentát	potentát	k1gMnSc1	potentát
i	i	k8xC	i
lid	lid	k1gInSc1	lid
i	i	k8xC	i
vláda	vláda	k1gFnSc1	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Fešná	fešný	k2eAgFnSc1d1	fešná
paní	paní	k1gFnSc1	paní
volá	volat	k5eAaImIp3nS	volat
<g/>
:	:	kIx,	:
má	mít	k5eAaImIp3nS	mít
málo	málo	k1gNnSc1	málo
vína	víno	k1gNnSc2	víno
pan	pan	k1gMnSc1	pan
šéf	šéf	k1gMnSc1	šéf
<g/>
?	?	kIx.	?
</s>
<s>
Nevypusť	vypustit	k5eNaPmRp2nS	vypustit
supy	sup	k1gMnPc7	sup
ven	ven	k6eAd1	ven
<g/>
.	.	kIx.	.
</s>
<s>
Signate	Signat	k1gMnSc5	Signat
<g/>
,	,	kIx,	,
signate	signat	k1gMnSc5	signat
<g/>
,	,	kIx,	,
mere	merat	k5eAaPmIp3nS	merat
me	me	k?	me
tangis	tangis	k1gInSc1	tangis
et	et	k?	et
angis	angis	k1gInSc1	angis
(	(	kIx(	(
<g/>
ochranný	ochranný	k2eAgInSc1d1	ochranný
palindrom	palindrom	k1gInSc1	palindrom
na	na	k7c6	na
Karlově	Karlův	k2eAgInSc6d1	Karlův
mostě	most	k1gInSc6	most
-	-	kIx~	-
na	na	k7c6	na
vědomí	vědomí	k1gNnSc6	vědomí
tobě	ty	k3xPp2nSc3	ty
znamení	znamení	k1gNnSc1	znamení
<g/>
,	,	kIx,	,
poskvrníš	poskvrnit	k5eAaPmIp2nS	poskvrnit
<g/>
-li	i	k?	-li
mě	já	k3xPp1nSc2	já
dotykem	dotyk	k1gInSc7	dotyk
<g/>
,	,	kIx,	,
budeš	být	k5eAaImBp2nS	být
zardoušen	zardousit	k5eAaPmNgInS	zardousit
<g/>
)	)	kIx)	)
Zeman	Zeman	k1gMnSc1	Zeman
nemá	mít	k5eNaImIp3nS	mít
kámen	kámen	k1gInSc4	kámen
na	na	k7c4	na
mez	mez	k1gFnSc4	mez
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgMnSc7d3	veliký
specialistou	specialista	k1gMnSc7	specialista
na	na	k7c4	na
palindromické	palindromický	k2eAgFnPc4d1	palindromický
věty	věta	k1gFnPc4	věta
byl	být	k5eAaImAgMnS	být
učitel	učitel	k1gMnSc1	učitel
českého	český	k2eAgInSc2d1	český
jazyka	jazyk	k1gInSc2	jazyk
Josef	Josef	k1gMnSc1	Josef
Wimmer	Wimmer	k1gMnSc1	Wimmer
z	z	k7c2	z
Benešova	Benešov	k1gInSc2	Benešov
nad	nad	k7c7	nad
Černou	Černá	k1gFnSc7	Černá
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
sbírce	sbírka	k1gFnSc6	sbírka
zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
tisíc	tisíc	k4xCgInPc2	tisíc
různých	různý	k2eAgFnPc2d1	různá
kombinací	kombinace	k1gFnPc2	kombinace
a	a	k8xC	a
kvality	kvalita	k1gFnSc2	kvalita
obousměrných	obousměrný	k2eAgFnPc2d1	obousměrná
vět	věta	k1gFnPc2	věta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
oboru	obor	k1gInSc6	obor
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
záznam	záznam	k1gInSc1	záznam
v	v	k7c6	v
Guinessově	Guinessův	k2eAgFnSc6d1	Guinessova
knize	kniha	k1gFnSc6	kniha
rekordů	rekord	k1gInPc2	rekord
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
