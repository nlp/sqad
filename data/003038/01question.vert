<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
slovo	slovo	k1gNnSc1	slovo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
lze	lze	k6eAd1	lze
číst	číst	k5eAaImF	číst
v	v	k7c6	v
libovolném	libovolný	k2eAgInSc6d1	libovolný
směru	směr	k1gInSc6	směr
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
vždy	vždy	k6eAd1	vždy
stejný	stejný	k2eAgInSc4d1	stejný
význam	význam	k1gInSc4	význam
<g/>
?	?	kIx.	?
</s>
