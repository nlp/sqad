<s>
Palindrom	palindrom	k1gInSc1	palindrom
(	(	kIx(	(
<g/>
z	z	k7c2	z
řec.	řec.	k?	řec.
palindromos	palindromosa	k1gFnPc2	palindromosa
<g/>
,	,	kIx,	,
běžící	běžící	k2eAgFnPc1d1	běžící
pozpátku	pozpátku	k6eAd1	pozpátku
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
slovo	slovo	k1gNnSc1	slovo
<g/>
,	,	kIx,	,
věta	věta	k1gFnSc1	věta
<g/>
,	,	kIx,	,
číslo	číslo	k1gNnSc1	číslo
nebo	nebo	k8xC	nebo
melodie	melodie	k1gFnSc1	melodie
(	(	kIx(	(
<g/>
obecně	obecně	k6eAd1	obecně
jakákoliv	jakýkoliv	k3yIgFnSc1	jakýkoliv
sekvence	sekvence	k1gFnSc1	sekvence
symbolů	symbol	k1gInPc2	symbol
<g/>
,	,	kIx,	,
např.	např.	kA	např.
sekvence	sekvence	k1gFnSc1	sekvence
DNA	dna	k1gFnSc1	dna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
tu	ten	k3xDgFnSc4	ten
vlastnost	vlastnost	k1gFnSc4	vlastnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
lze	lze	k6eAd1	lze
číst	číst	k5eAaImF	číst
v	v	k7c6	v
libovolném	libovolný	k2eAgInSc6d1	libovolný
směru	směr	k1gInSc6	směr
(	(	kIx(	(
<g/>
zprava	zprava	k6eAd1	zprava
doleva	doleva	k6eAd1	doleva
nebo	nebo	k8xC	nebo
zleva	zleva	k6eAd1	zleva
doprava	doprava	k1gFnSc1	doprava
<g/>
)	)	kIx)	)
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
vždy	vždy	k6eAd1	vždy
stejný	stejný	k2eAgInSc4d1	stejný
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
