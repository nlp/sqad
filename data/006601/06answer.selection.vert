<s>
Zahříváním	zahřívání	k1gNnSc7	zahřívání
škrobu	škrob	k1gInSc2	škrob
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
škrobový	škrobový	k2eAgInSc1d1	škrobový
maz	maz	k1gInSc1	maz
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc7	jeho
hydrolýzou	hydrolýza	k1gFnSc7	hydrolýza
vzniká	vznikat	k5eAaImIp3nS	vznikat
škrobový	škrobový	k2eAgInSc4d1	škrobový
sirup	sirup	k1gInSc4	sirup
<g/>
,	,	kIx,	,
škrobový	škrobový	k2eAgInSc4d1	škrobový
cukr	cukr	k1gInSc4	cukr
a	a	k8xC	a
glukóza	glukóza	k1gFnSc1	glukóza
<g/>
.	.	kIx.	.
</s>
