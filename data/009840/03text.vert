<p>
<s>
Kelvin	kelvin	k1gInSc1	kelvin
[	[	kIx(	[
<g/>
K	K	kA	K
<g/>
]	]	kIx)	]
je	být	k5eAaImIp3nS	být
jednotkou	jednotka	k1gFnSc7	jednotka
termodynamické	termodynamický	k2eAgFnSc2d1	termodynamická
teploty	teplota	k1gFnSc2	teplota
a	a	k8xC	a
také	také	k9	také
vhodnou	vhodný	k2eAgFnSc7d1	vhodná
jednotkou	jednotka	k1gFnSc7	jednotka
teplotního	teplotní	k2eAgInSc2d1	teplotní
rozdílu	rozdíl	k1gInSc2	rozdíl
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
velkou	velký	k2eAgFnSc4d1	velká
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
stupeň	stupeň	k1gInSc1	stupeň
Celsia	Celsius	k1gMnSc2	Celsius
–	–	k?	–
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
něj	on	k3xPp3gInSc2	on
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
i	i	k9	i
v	v	k7c6	v
jednotkách	jednotka	k1gFnPc6	jednotka
odvozených	odvozený	k2eAgFnPc6d1	odvozená
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
W	W	kA	W
<g/>
/	/	kIx~	/
<g/>
(	(	kIx(	(
<g/>
m	m	kA	m
<g/>
·	·	k?	·
<g/>
K	K	kA	K
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
též	též	k9	též
předponu	předpona	k1gFnSc4	předpona
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
mili	mili	k6eAd1	mili
v	v	k7c6	v
jednotce	jednotka	k1gFnSc6	jednotka
mK	mK	k?	mK
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kelvin	kelvin	k1gInSc1	kelvin
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
sedmi	sedm	k4xCc2	sedm
základních	základní	k2eAgFnPc2d1	základní
jednotek	jednotka	k1gFnPc2	jednotka
soustavy	soustava	k1gFnSc2	soustava
SI	si	k1gNnSc2	si
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zavedení	zavedení	k1gNnSc1	zavedení
pojmu	pojem	k1gInSc2	pojem
kelvin	kelvin	k1gInSc1	kelvin
==	==	k?	==
</s>
</p>
<p>
<s>
Kelvin	kelvin	k1gInSc1	kelvin
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
sedmi	sedm	k4xCc2	sedm
základních	základní	k2eAgFnPc2d1	základní
jednotek	jednotka	k1gFnPc2	jednotka
soustavy	soustava	k1gFnSc2	soustava
SI	si	k1gNnSc2	si
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
byl	být	k5eAaImAgInS	být
definován	definovat	k5eAaBmNgInS	definovat
dvěma	dva	k4xCgFnPc7	dva
hodnotami	hodnota	k1gFnPc7	hodnota
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
0	[number]	k4	0
K	k	k7c3	k
je	být	k5eAaImIp3nS	být
teplota	teplota	k1gFnSc1	teplota
absolutní	absolutní	k2eAgFnSc2d1	absolutní
nuly	nula	k1gFnSc2	nula
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
naprosto	naprosto	k6eAd1	naprosto
nejnižší	nízký	k2eAgFnSc1d3	nejnižší
teplota	teplota	k1gFnSc1	teplota
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
fyzikálně	fyzikálně	k6eAd1	fyzikálně
definována	definovat	k5eAaBmNgFnS	definovat
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
273,16	[number]	k4	273,16
K	k	k7c3	k
je	být	k5eAaImIp3nS	být
teplota	teplota	k1gFnSc1	teplota
trojného	trojný	k2eAgInSc2d1	trojný
bodu	bod	k1gInSc2	bod
vody	voda	k1gFnSc2	voda
(	(	kIx(	(
<g/>
teplota	teplota	k1gFnSc1	teplota
0,01	[number]	k4	0,01
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
tlak	tlak	k1gInSc1	tlak
613	[number]	k4	613
Pa	Pa	kA	Pa
<g/>
)	)	kIx)	)
<g/>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
změny	změna	k1gFnPc4	změna
definic	definice	k1gFnPc2	definice
základních	základní	k2eAgFnPc2d1	základní
fyzikálních	fyzikální	k2eAgFnPc2d1	fyzikální
jednotek	jednotka	k1gFnPc2	jednotka
byla	být	k5eAaImAgFnS	být
přijata	přijat	k2eAgFnSc1d1	přijata
20	[number]	k4	20
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2019	[number]	k4	2019
i	i	k8xC	i
nová	nový	k2eAgFnSc1d1	nová
definice	definice	k1gFnSc1	definice
kelvinu	kelvin	k1gInSc2	kelvin
(	(	kIx(	(
<g/>
fixací	fixace	k1gFnPc2	fixace
Boltzmannovy	Boltzmannův	k2eAgFnSc2d1	Boltzmannova
konstanty	konstanta	k1gFnSc2	konstanta
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
B	B	kA	B
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
k_	k_	k?	k_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
B	B	kA	B
<g/>
}	}	kIx)	}
}}	}}	k?	}}
</s>
</p>
<p>
<s>
a	a	k8xC	a
převodem	převod	k1gInSc7	převod
teploty	teplota	k1gFnSc2	teplota
na	na	k7c4	na
energii	energie	k1gFnSc4	energie
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
B	B	kA	B
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
T	T	kA	T
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
E	E	kA	E
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
k_	k_	k?	k_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
B	B	kA	B
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
T	T	kA	T
<g/>
=	=	kIx~	=
<g/>
E	E	kA	E
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
teplot	teplota	k1gFnPc2	teplota
jeden	jeden	k4xCgInSc4	jeden
stupeň	stupeň	k1gInSc4	stupeň
v	v	k7c6	v
Celsiově	Celsiův	k2eAgFnSc6d1	Celsiova
i	i	k8xC	i
Kelvinově	Kelvinův	k2eAgFnSc6d1	Kelvinova
stupnici	stupnice	k1gFnSc6	stupnice
je	být	k5eAaImIp3nS	být
stejný	stejný	k2eAgInSc1d1	stejný
<g/>
,	,	kIx,	,
1	[number]	k4	1
K	K	kA	K
≅	≅	k?	≅
1	[number]	k4	1
°	°	k?	°
<g/>
C.	C.	kA	C.
Stupnice	stupnice	k1gFnSc1	stupnice
však	však	k9	však
mají	mít	k5eAaImIp3nP	mít
různé	různý	k2eAgInPc1d1	různý
počátky	počátek	k1gInPc1	počátek
<g/>
:	:	kIx,	:
0	[number]	k4	0
°	°	k?	°
<g/>
C	C	kA	C
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
273,15	[number]	k4	273,15
K.	K.	kA	K.
</s>
</p>
<p>
<s>
Kelvinovu	Kelvinův	k2eAgFnSc4d1	Kelvinova
stupnici	stupnice	k1gFnSc4	stupnice
měření	měření	k1gNnSc2	měření
teplot	teplota	k1gFnPc2	teplota
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
skotský	skotský	k2eAgMnSc1d1	skotský
matematik	matematik	k1gMnSc1	matematik
a	a	k8xC	a
fyzik	fyzik	k1gMnSc1	fyzik
William	William	k1gInSc1	William
Thomson	Thomson	k1gInSc4	Thomson
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
za	za	k7c4	za
své	svůj	k3xOyFgInPc4	svůj
výrazné	výrazný	k2eAgInPc4d1	výrazný
vědecké	vědecký	k2eAgInPc4d1	vědecký
úspěchy	úspěch	k1gInPc4	úspěch
povýšen	povýšit	k5eAaPmNgMnS	povýšit
do	do	k7c2	do
šlechtického	šlechtický	k2eAgInSc2d1	šlechtický
stavu	stav	k1gInSc2	stav
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
lord	lord	k1gMnSc1	lord
Kelvin	kelvin	k1gInSc1	kelvin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přepočet	přepočet	k1gInSc1	přepočet
na	na	k7c4	na
jiné	jiný	k2eAgFnPc4d1	jiná
stupnice	stupnice	k1gFnPc4	stupnice
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Celsiova	Celsiův	k2eAgFnSc1d1	Celsiova
stupnice	stupnice	k1gFnSc1	stupnice
===	===	k?	===
</s>
</p>
<p>
<s>
Stupnice	stupnice	k1gFnSc1	stupnice
pro	pro	k7c4	pro
Celsiův	Celsiův	k2eAgInSc4d1	Celsiův
stupeň	stupeň	k1gInSc4	stupeň
má	mít	k5eAaImIp3nS	mít
stejný	stejný	k2eAgInSc4d1	stejný
rozdíl	rozdíl	k1gInSc4	rozdíl
teplot	teplota	k1gFnPc2	teplota
1	[number]	k4	1
°	°	k?	°
<g/>
C	C	kA	C
=	=	kIx~	=
1	[number]	k4	1
K	K	kA	K
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
posunutý	posunutý	k2eAgInSc1d1	posunutý
počátek	počátek	k1gInSc1	počátek
<g/>
:	:	kIx,	:
0	[number]	k4	0
°	°	k?	°
<g/>
C	C	kA	C
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
273,15	[number]	k4	273,15
K	K	kA	K
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
K	k	k7c3	k
</s>
</p>
<p>
</p>
<p>
<s>
/	/	kIx~	/
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
K	k	k7c3	k
</s>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
C	C	kA	C
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
/	/	kIx~	/
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∘	∘	k?	∘
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
C	C	kA	C
</s>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
273	[number]	k4	273
</s>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
15	[number]	k4	15
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
K	k	k7c3	k
<g/>
/	/	kIx~	/
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
K	k	k7c3	k
<g/>
}	}	kIx)	}
=	=	kIx~	=
<g/>
C	C	kA	C
<g/>
/	/	kIx~	/
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
circ	circ	k6eAd1	circ
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
C	C	kA	C
<g/>
}	}	kIx)	}
+273	+273	k4	+273
<g/>
{	{	kIx(	{
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
<g/>
15	[number]	k4	15
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
C	C	kA	C
</s>
</p>
<p>
</p>
<p>
<s>
/	/	kIx~	/
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∘	∘	k?	∘
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
C	C	kA	C
</s>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
K	k	k7c3	k
</s>
</p>
<p>
</p>
<p>
<s>
/	/	kIx~	/
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
K	k	k7c3	k
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
273	[number]	k4	273
</s>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
15	[number]	k4	15
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
C	C	kA	C
<g/>
/	/	kIx~	/
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
circ	circ	k1gInSc1	circ
}	}	kIx)	}
<g/>
C	C	kA	C
<g/>
}	}	kIx)	}
=	=	kIx~	=
<g/>
K	k	k7c3	k
<g/>
/	/	kIx~	/
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
K	k	k7c3	k
<g/>
}	}	kIx)	}
-273	-273	k4	-273
<g/>
{	{	kIx(	{
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
<g/>
15	[number]	k4	15
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
<g/>
kde	kde	k6eAd1	kde
C	C	kA	C
je	být	k5eAaImIp3nS	být
teplota	teplota	k1gFnSc1	teplota
ve	v	k7c6	v
stupních	stupeň	k1gInPc6	stupeň
Celsia	Celsius	k1gMnSc2	Celsius
<g/>
,	,	kIx,	,
K	K	kA	K
je	být	k5eAaImIp3nS	být
teplota	teplota	k1gFnSc1	teplota
v	v	k7c6	v
kelvinech	kelvin	k1gInPc6	kelvin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
pro	pro	k7c4	pro
hodnoty	hodnota	k1gFnPc4	hodnota
teploty	teplota	k1gFnSc2	teplota
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
0	[number]	k4	0
K	k	k7c3	k
=	=	kIx~	=
−	−	k?	−
°	°	k?	°
<g/>
C	C	kA	C
</s>
</p>
<p>
<s>
0	[number]	k4	0
°	°	k?	°
<g/>
C	C	kA	C
=	=	kIx~	=
+273,15	+273,15	k4	+273,15
K	k	k7c3	k
</s>
</p>
<p>
<s>
100	[number]	k4	100
°	°	k?	°
<g/>
C	C	kA	C
=	=	kIx~	=
+373,15	+373,15	k4	+373,15
K	k	k7c3	k
</s>
</p>
<p>
<s>
===	===	k?	===
Rankinova	Rankinův	k2eAgFnSc1d1	Rankinova
stupnice	stupnice	k1gFnSc1	stupnice
===	===	k?	===
</s>
</p>
<p>
<s>
Stupnice	stupnice	k1gFnSc1	stupnice
pro	pro	k7c4	pro
Rankinův	Rankinův	k2eAgInSc4d1	Rankinův
stupeň	stupeň	k1gInSc4	stupeň
je	být	k5eAaImIp3nS	být
Fahrenheitova	Fahrenheitův	k2eAgFnSc1d1	Fahrenheitova
stupnice	stupnice	k1gFnSc1	stupnice
posunutá	posunutý	k2eAgFnSc1d1	posunutá
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vycházela	vycházet	k5eAaImAgFnS	vycházet
z	z	k7c2	z
absolutní	absolutní	k2eAgFnSc2d1	absolutní
nuly	nula	k1gFnSc2	nula
<g/>
.	.	kIx.	.
</s>
<s>
Stupeň	stupeň	k1gInSc1	stupeň
Rankina	Rankino	k1gNnSc2	Rankino
je	být	k5eAaImIp3nS	být
stejně	stejně	k6eAd1	stejně
velký	velký	k2eAgInSc1d1	velký
jako	jako	k8xS	jako
stupeň	stupeň	k1gInSc1	stupeň
Fahrenheita	Fahrenheit	k1gMnSc2	Fahrenheit
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
1	[number]	k4	1
°	°	k?	°
<g/>
R	R	kA	R
=	=	kIx~	=
1	[number]	k4	1
°	°	k?	°
<g/>
F	F	kA	F
a	a	k8xC	a
platí	platit	k5eAaImIp3nS	platit
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
/	/	kIx~	/
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∘	∘	k?	∘
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
9	[number]	k4	9
</s>
</p>
<p>
<s>
5	[number]	k4	5
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
K	k	k7c3	k
</s>
</p>
<p>
</p>
<p>
<s>
/	/	kIx~	/
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
K	k	k7c3	k
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
R	R	kA	R
<g/>
/	/	kIx~	/
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
circ	circ	k6eAd1	circ
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
9	[number]	k4	9
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
5	[number]	k4	5
<g/>
}}	}}	k?	}}
<g/>
K	k	k7c3	k
<g/>
/	/	kIx~	/
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
K	k	k7c3	k
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
<g/>
kde	kde	k6eAd1	kde
R	R	kA	R
je	být	k5eAaImIp3nS	být
teplota	teplota	k1gFnSc1	teplota
ve	v	k7c6	v
stupních	stupeň	k1gInPc6	stupeň
Rankina	Rankin	k2eAgInSc2d1	Rankin
<g/>
,	,	kIx,	,
K	K	kA	K
je	být	k5eAaImIp3nS	být
teplota	teplota	k1gFnSc1	teplota
v	v	k7c6	v
kelvinech	kelvin	k1gInPc6	kelvin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
pro	pro	k7c4	pro
hodnoty	hodnota	k1gFnPc4	hodnota
teploty	teplota	k1gFnSc2	teplota
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
0	[number]	k4	0
K	k	k7c3	k
=	=	kIx~	=
0	[number]	k4	0
°	°	k?	°
<g/>
R	R	kA	R
</s>
</p>
<p>
<s>
273,15	[number]	k4	273,15
K	k	k7c3	k
=	=	kIx~	=
491,67	[number]	k4	491,67
°	°	k?	°
<g/>
R	R	kA	R
(	(	kIx(	(
=	=	kIx~	=
0	[number]	k4	0
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
373,15	[number]	k4	373,15
K	k	k7c3	k
=	=	kIx~	=
671,67	[number]	k4	671,67
°	°	k?	°
<g/>
R	R	kA	R
(	(	kIx(	(
=	=	kIx~	=
100	[number]	k4	100
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Fahrenheitova	Fahrenheitův	k2eAgFnSc1d1	Fahrenheitova
stupnice	stupnice	k1gFnSc1	stupnice
===	===	k?	===
</s>
</p>
<p>
<s>
Stupnice	stupnice	k1gFnSc1	stupnice
pro	pro	k7c4	pro
Fahrenheitův	Fahrenheitův	k2eAgInSc4d1	Fahrenheitův
stupeň	stupeň	k1gInSc4	stupeň
má	mít	k5eAaImIp3nS	mít
rozdíl	rozdíl	k1gInSc1	rozdíl
teplot	teplota	k1gFnPc2	teplota
stejný	stejný	k2eAgInSc1d1	stejný
jako	jako	k8xS	jako
Rankinova	Rankinův	k2eAgFnSc1d1	Rankinova
<g/>
,	,	kIx,	,
1	[number]	k4	1
°	°	k?	°
<g/>
F	F	kA	F
=	=	kIx~	=
1	[number]	k4	1
°	°	k?	°
<g/>
R	R	kA	R
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
posunutý	posunutý	k2eAgInSc1d1	posunutý
začátek	začátek	k1gInSc1	začátek
<g/>
:	:	kIx,	:
32	[number]	k4	32
°	°	k?	°
<g/>
F	F	kA	F
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
273,15	[number]	k4	273,15
K	K	kA	K
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
[	[	kIx(	[
</s>
</p>
<p>
</p>
<p>
<s>
∘	∘	k?	∘
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
]	]	kIx)	]
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
[	[	kIx(	[
</s>
</p>
<p>
</p>
<p>
<s>
K	k	k7c3	k
</s>
</p>
<p>
</p>
<p>
<s>
]	]	kIx)	]
</s>
</p>
<p>
<s>
×	×	k?	×
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
9	[number]	k4	9
</s>
</p>
<p>
<s>
5	[number]	k4	5
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
459	[number]	k4	459
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
67	[number]	k4	67
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
[	[	kIx(	[
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
circ	circ	k6eAd1	circ
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}	}	kIx)	}
]	]	kIx)	]
<g/>
=	=	kIx~	=
<g/>
[	[	kIx(	[
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
K	k	k7c3	k
<g/>
}	}	kIx)	}
]	]	kIx)	]
<g/>
\	\	kIx~	\
<g/>
times	times	k1gInSc1	times
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
9	[number]	k4	9
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
5	[number]	k4	5
<g/>
}}	}}	k?	}}
<g/>
-	-	kIx~	-
<g/>
459,67	[number]	k4	459,67
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
kde	kde	k6eAd1	kde
°	°	k?	°
<g/>
F	F	kA	F
je	být	k5eAaImIp3nS	být
teplota	teplota	k1gFnSc1	teplota
ve	v	k7c6	v
stupních	stupeň	k1gInPc6	stupeň
Fahrenheita	Fahrenheit	k1gMnSc2	Fahrenheit
<g/>
,	,	kIx,	,
K	K	kA	K
je	být	k5eAaImIp3nS	být
teplota	teplota	k1gFnSc1	teplota
v	v	k7c6	v
kelvinech	kelvin	k1gInPc6	kelvin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
pro	pro	k7c4	pro
hodnoty	hodnota	k1gFnPc4	hodnota
teploty	teplota	k1gFnSc2	teplota
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
0	[number]	k4	0
K	k	k7c3	k
=	=	kIx~	=
−	−	k?	−
°	°	k?	°
<g/>
F	F	kA	F
</s>
</p>
<p>
<s>
255,37	[number]	k4	255,37
K	k	k7c3	k
=	=	kIx~	=
0	[number]	k4	0
°	°	k?	°
<g/>
F	F	kA	F
</s>
</p>
<p>
<s>
273,15	[number]	k4	273,15
K	k	k7c3	k
=	=	kIx~	=
32	[number]	k4	32
°	°	k?	°
<g/>
F	F	kA	F
(	(	kIx(	(
=	=	kIx~	=
0	[number]	k4	0
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
373,15	[number]	k4	373,15
K	k	k7c3	k
=	=	kIx~	=
212	[number]	k4	212
°	°	k?	°
<g/>
F	F	kA	F
(	(	kIx(	(
=	=	kIx~	=
100	[number]	k4	100
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Réaumurova	Réaumurův	k2eAgFnSc1d1	Réaumurova
stupnice	stupnice	k1gFnSc1	stupnice
===	===	k?	===
</s>
</p>
<p>
<s>
Stupnice	stupnice	k1gFnSc1	stupnice
pro	pro	k7c4	pro
Réaumurův	Réaumurův	k2eAgInSc4d1	Réaumurův
stupeň	stupeň	k1gInSc4	stupeň
má	mít	k5eAaImIp3nS	mít
stejný	stejný	k2eAgInSc4d1	stejný
počátek	počátek	k1gInSc4	počátek
jako	jako	k8xC	jako
Celsiova	Celsiův	k2eAgFnSc1d1	Celsiova
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jiný	jiný	k2eAgInSc1d1	jiný
rozdíl	rozdíl	k1gInSc1	rozdíl
teplot	teplota	k1gFnPc2	teplota
<g/>
:	:	kIx,	:
100	[number]	k4	100
°	°	k?	°
<g/>
C	C	kA	C
=	=	kIx~	=
80	[number]	k4	80
°	°	k?	°
<g/>
Re	re	k9	re
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
/	/	kIx~	/
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∘	∘	k?	∘
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
8	[number]	k4	8
</s>
</p>
<p>
</p>
<p>
<s>
K	k	k7c3	k
</s>
</p>
<p>
</p>
<p>
<s>
/	/	kIx~	/
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
K	k	k7c3	k
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
218	[number]	k4	218
</s>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
52	[number]	k4	52
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
Re	re	k9	re
<g/>
/	/	kIx~	/
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
circ	circ	k6eAd1	circ
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
Re	re	k9	re
<g/>
}	}	kIx)	}
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
{	{	kIx(	{
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
<g/>
8	[number]	k4	8
<g/>
\	\	kIx~	\
K	k	k7c3	k
<g/>
/	/	kIx~	/
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
K	k	k7c3	k
<g/>
}	}	kIx)	}
-218	-218	k4	-218
<g/>
{	{	kIx(	{
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
<g/>
52	[number]	k4	52
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
<g/>
kde	kde	k6eAd1	kde
Re	re	k9	re
je	být	k5eAaImIp3nS	být
teplota	teplota	k1gFnSc1	teplota
ve	v	k7c6	v
stupních	stupeň	k1gInPc6	stupeň
Réaumura	Réaumur	k1gMnSc2	Réaumur
<g/>
,	,	kIx,	,
K	K	kA	K
je	být	k5eAaImIp3nS	být
teplota	teplota	k1gFnSc1	teplota
v	v	k7c6	v
kelvinech	kelvin	k1gInPc6	kelvin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
pro	pro	k7c4	pro
hodnoty	hodnota	k1gFnPc4	hodnota
teploty	teplota	k1gFnSc2	teplota
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
0	[number]	k4	0
K	k	k7c3	k
=	=	kIx~	=
−	−	k?	−
°	°	k?	°
<g/>
Re	re	k9	re
</s>
</p>
<p>
<s>
273,15	[number]	k4	273,15
K	k	k7c3	k
=	=	kIx~	=
0	[number]	k4	0
°	°	k?	°
<g/>
Re	re	k9	re
(	(	kIx(	(
=	=	kIx~	=
0	[number]	k4	0
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
373,15	[number]	k4	373,15
K	k	k7c3	k
=	=	kIx~	=
80	[number]	k4	80
°	°	k?	°
<g/>
Re	re	k9	re
(	(	kIx(	(
=	=	kIx~	=
100	[number]	k4	100
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Teplota	teplota	k1gFnSc1	teplota
a	a	k8xC	a
energie	energie	k1gFnSc1	energie
==	==	k?	==
</s>
</p>
<p>
<s>
Molekulová	molekulový	k2eAgFnSc1d1	molekulová
a	a	k8xC	a
statistická	statistický	k2eAgFnSc1d1	statistická
fyzika	fyzika	k1gFnSc1	fyzika
dokazují	dokazovat	k5eAaImIp3nP	dokazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
střední	střední	k2eAgFnSc1d1	střední
kinetická	kinetický	k2eAgFnSc1d1	kinetická
energie	energie	k1gFnSc1	energie
Ek	Ek	k1gMnPc2	Ek
částic	částice	k1gFnPc2	částice
tvořících	tvořící	k2eAgFnPc2d1	tvořící
soustavu	soustava	k1gFnSc4	soustava
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
klasické	klasický	k2eAgFnSc6d1	klasická
aproximaci	aproximace	k1gFnSc6	aproximace
(	(	kIx(	(
<g/>
ekvipartiční	ekvipartiční	k2eAgInSc1d1	ekvipartiční
teorém	teorém	k1gInSc1	teorém
<g/>
)	)	kIx)	)
vlastnost	vlastnost	k1gFnSc1	vlastnost
teploty	teplota	k1gFnSc2	teplota
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
aby	aby	k9	aby
dvě	dva	k4xCgFnPc4	dva
soustavy	soustava	k1gFnPc4	soustava
1	[number]	k4	1
<g/>
,	,	kIx,	,
2	[number]	k4	2
byly	být	k5eAaImAgInP	být
navzájem	navzájem	k6eAd1	navzájem
v	v	k7c6	v
rovnováze	rovnováha	k1gFnSc6	rovnováha
<g/>
,	,	kIx,	,
musejí	muset	k5eAaImIp3nP	muset
mít	mít	k5eAaImF	mít
částice	částice	k1gFnPc1	částice
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
je	on	k3xPp3gMnPc4	on
tvoří	tvořit	k5eAaImIp3nP	tvořit
<g/>
,	,	kIx,	,
stejné	stejný	k2eAgFnPc1d1	stejná
střední	střední	k2eAgFnPc4d1	střední
kinetické	kinetický	k2eAgFnPc4d1	kinetická
energie	energie	k1gFnPc4	energie
<g/>
:	:	kIx,	:
Ek	Ek	k1gFnSc1	Ek
<g/>
1	[number]	k4	1
=	=	kIx~	=
Ek	Ek	k1gFnSc2	Ek
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
měřit	měřit	k5eAaImF	měřit
teplotu	teplota	k1gFnSc4	teplota
pomocí	pomocí	k7c2	pomocí
energie	energie	k1gFnSc2	energie
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
E	E	kA	E
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
<s>
T	T	kA	T
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
E	E	kA	E
<g/>
=	=	kIx~	=
<g/>
kT	kT	k?	kT
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
<g/>
kde	kde	k6eAd1	kde
konstantou	konstanta	k1gFnSc7	konstanta
k	k	k7c3	k
úměrnosti	úměrnost	k1gFnSc3	úměrnost
je	být	k5eAaImIp3nS	být
Boltzmannova	Boltzmannův	k2eAgFnSc1d1	Boltzmannova
konstanta	konstanta	k1gFnSc1	konstanta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
SI	si	k1gNnPc2	si
(	(	kIx(	(
<g/>
v	v	k7c6	v
jednotkách	jednotka	k1gFnPc6	jednotka
J	J	kA	J
pro	pro	k7c4	pro
energii	energie	k1gFnSc4	energie
a	a	k8xC	a
K	K	kA	K
pro	pro	k7c4	pro
teplotu	teplota	k1gFnSc4	teplota
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
nyní	nyní	k6eAd1	nyní
hodnotu	hodnota	k1gFnSc4	hodnota
přesně	přesně	k6eAd1	přesně
(	(	kIx(	(
<g/>
definitoricky	definitoricky	k6eAd1	definitoricky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
1,380	[number]	k4	1,380
</s>
</p>
<p>
</p>
<p>
<s>
649	[number]	k4	649
</s>
</p>
<p>
<s>
×	×	k?	×
</s>
</p>
<p>
</p>
<p>
<s>
10	[number]	k4	10
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
23	[number]	k4	23
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
J	J	kA	J
</s>
</p>
<p>
</p>
<p>
<s>
/	/	kIx~	/
</s>
</p>
<p>
</p>
<p>
<s>
K	k	k7c3	k
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
k	k	k7c3	k
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
{	{	kIx(	{
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
<g/>
380	[number]	k4	380
<g/>
\	\	kIx~	\
<g/>
,649	,649	k4	,649
<g/>
\	\	kIx~	\
<g/>
times	times	k1gInSc1	times
10	[number]	k4	10
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
23	[number]	k4	23
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
J	J	kA	J
<g/>
/	/	kIx~	/
<g/>
K	k	k7c3	k
<g/>
}	}	kIx)	}
}	}	kIx)	}
</s>
</p>
<p>
<s>
V	v	k7c6	v
jaderné	jaderný	k2eAgFnSc6d1	jaderná
fyzice	fyzika	k1gFnSc6	fyzika
se	se	k3xPyFc4	se
energie	energie	k1gFnSc1	energie
často	často	k6eAd1	často
měří	měřit	k5eAaImIp3nS	měřit
elektronvolty	elektronvolt	k1gInPc1	elektronvolt
<g/>
,	,	kIx,	,
eV	eV	k?	eV
<g/>
;	;	kIx,	;
k	k	k7c3	k
přepočtu	přepočet	k1gInSc3	přepočet
se	se	k3xPyFc4	se
použije	použít	k5eAaPmIp3nS	použít
táž	týž	k3xTgFnSc1	týž
rovnice	rovnice	k1gFnSc1	rovnice
E	E	kA	E
=	=	kIx~	=
k	k	k7c3	k
T	T	kA	T
<g/>
,	,	kIx,	,
jen	jen	k9	jen
Boltzmannova	Boltzmannův	k2eAgFnSc1d1	Boltzmannova
konstanta	konstanta	k1gFnSc1	konstanta
bude	být	k5eAaImBp3nS	být
vyjádřena	vyjádřit	k5eAaPmNgFnS	vyjádřit
v	v	k7c6	v
eV	eV	k?	eV
<g/>
/	/	kIx~	/
<g/>
K.	K.	kA	K.
Platí	platit	k5eAaImIp3nP	platit
tyto	tento	k3xDgFnPc1	tento
ekvivalence	ekvivalence	k1gFnPc1	ekvivalence
(	(	kIx(	(
<g/>
zaokrouhleno	zaokrouhlen	k2eAgNnSc1d1	zaokrouhleno
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
K	k	k7c3	k
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
^	^	kIx~	^
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
8,617	[number]	k4	8,617
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
10	[number]	k4	10
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
5	[number]	k4	5
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
<s>
V	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
1	[number]	k4	1
<g/>
\	\	kIx~	\
<g/>
;	;	kIx,	;
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
K	k	k7c3	k
<g/>
}	}	kIx)	}
\	\	kIx~	\
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
hat	hat	k0	hat
{	{	kIx(	{
<g/>
=	=	kIx~	=
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
8	[number]	k4	8
<g/>
{	{	kIx(	{
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
<g/>
617	[number]	k4	617
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
10	[number]	k4	10
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
;	;	kIx,	;
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
eV	eV	k?	eV
<g/>
}	}	kIx)	}
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
<s>
V	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
^	^	kIx~	^
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1,160	[number]	k4	1,160
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
10	[number]	k4	10
</s>
</p>
<p>
</p>
<p>
<s>
4	[number]	k4	4
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
K	k	k7c3	k
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
1	[number]	k4	1
<g/>
\	\	kIx~	\
<g/>
;	;	kIx,	;
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
eV	eV	k?	eV
<g/>
}	}	kIx)	}
\	\	kIx~	\
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
hat	hat	k0	hat
{	{	kIx(	{
<g/>
=	=	kIx~	=
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
1	[number]	k4	1
<g/>
{	{	kIx(	{
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
<g/>
160	[number]	k4	160
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
10	[number]	k4	10
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
;	;	kIx,	;
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
K	k	k7c3	k
<g/>
}	}	kIx)	}
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
K	k	k7c3	k
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
^	^	kIx~	^
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1,381	[number]	k4	1,381
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
10	[number]	k4	10
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
23	[number]	k4	23
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
J	J	kA	J
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
1	[number]	k4	1
<g/>
\	\	kIx~	\
<g/>
;	;	kIx,	;
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
K	k	k7c3	k
<g/>
}	}	kIx)	}
\	\	kIx~	\
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
hat	hat	k0	hat
{	{	kIx(	{
<g/>
=	=	kIx~	=
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
1	[number]	k4	1
<g/>
{	{	kIx(	{
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
<g/>
381	[number]	k4	381
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
10	[number]	k4	10
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
23	[number]	k4	23
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
;	;	kIx,	;
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
J	J	kA	J
<g/>
}	}	kIx)	}
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
J	J	kA	J
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
^	^	kIx~	^
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
7,243	[number]	k4	7,243
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
10	[number]	k4	10
</s>
</p>
<p>
</p>
<p>
<s>
22	[number]	k4	22
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
K	k	k7c3	k
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
1	[number]	k4	1
<g/>
\	\	kIx~	\
<g/>
;	;	kIx,	;
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
J	J	kA	J
<g/>
}	}	kIx)	}
\	\	kIx~	\
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
hat	hat	k0	hat
{	{	kIx(	{
<g/>
=	=	kIx~	=
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
7	[number]	k4	7
<g/>
{	{	kIx(	{
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
<g/>
243	[number]	k4	243
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
10	[number]	k4	10
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
22	[number]	k4	22
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
;	;	kIx,	;
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
K	k	k7c3	k
<g/>
}	}	kIx)	}
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Barevná	barevný	k2eAgFnSc1d1	barevná
teplota	teplota	k1gFnSc1	teplota
světla	světlo	k1gNnSc2	světlo
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
kelvinech	kelvin	k1gInPc6	kelvin
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
udává	udávat	k5eAaImIp3nS	udávat
barevná	barevný	k2eAgFnSc1d1	barevná
teplota	teplota	k1gFnSc1	teplota
světla	světlo	k1gNnSc2	světlo
(	(	kIx(	(
<g/>
přesněji	přesně	k6eAd2	přesně
<g/>
:	:	kIx,	:
teplota	teplota	k1gFnSc1	teplota
záře	zář	k1gFnSc2	zář
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
umělých	umělý	k2eAgInPc2d1	umělý
světelných	světelný	k2eAgInPc2d1	světelný
zdrojů	zdroj	k1gInPc2	zdroj
–	–	k?	–
žárovek	žárovka	k1gFnPc2	žárovka
<g/>
,	,	kIx,	,
zářivek	zářivka	k1gFnPc2	zářivka
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
významné	významný	k2eAgNnSc1d1	významné
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
snímání	snímání	k1gNnSc4	snímání
a	a	k8xC	a
záznam	záznam	k1gInSc4	záznam
světla	světlo	k1gNnSc2	světlo
pro	pro	k7c4	pro
fotografie	fotografia	k1gFnPc4	fotografia
a	a	k8xC	a
film	film	k1gInSc4	film
či	či	k8xC	či
video	video	k1gNnSc4	video
<g/>
.	.	kIx.	.
</s>
<s>
Vnímaná	vnímaný	k2eAgFnSc1d1	vnímaná
barva	barva	k1gFnSc1	barva
světla	světlo	k1gNnSc2	světlo
černého	černý	k2eAgNnSc2d1	černé
tělesa	těleso	k1gNnSc2	těleso
rozežhaveného	rozežhavený	k2eAgNnSc2d1	rozežhavený
na	na	k7c4	na
danou	daný	k2eAgFnSc4d1	daná
teplotu	teplota	k1gFnSc4	teplota
T	T	kA	T
je	být	k5eAaImIp3nS	být
určena	určen	k2eAgFnSc1d1	určena
jednak	jednak	k8xC	jednak
spektrální	spektrální	k2eAgNnSc4d1	spektrální
září	září	k1gNnSc4	září
L	L	kA	L
<g/>
(	(	kIx(	(
<g/>
λ	λ	k?	λ
<g/>
,	,	kIx,	,
T	T	kA	T
<g/>
)	)	kIx)	)
podle	podle	k7c2	podle
Planckova	Planckův	k2eAgInSc2d1	Planckův
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
poměrnou	poměrný	k2eAgFnSc7d1	poměrná
spektrální	spektrální	k2eAgFnSc7d1	spektrální
světelnou	světelný	k2eAgFnSc7d1	světelná
účinností	účinnost	k1gFnSc7	účinnost
standardního	standardní	k2eAgMnSc2d1	standardní
fotometrického	fotometrický	k2eAgMnSc2d1	fotometrický
pozorovatele	pozorovatel	k1gMnSc2	pozorovatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Teplota	teplota	k1gFnSc1	teplota
0	[number]	k4	0
°	°	k?	°
<g/>
C	C	kA	C
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
bod	bod	k1gInSc1	bod
mrazu	mráz	k1gInSc2	mráz
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
teplota	teplota	k1gFnSc1	teplota
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
rovnováze	rovnováha	k1gFnSc6	rovnováha
led	led	k1gInSc1	led
<g/>
,	,	kIx,	,
kapalná	kapalný	k2eAgFnSc1d1	kapalná
voda	voda	k1gFnSc1	voda
a	a	k8xC	a
vzduch	vzduch	k1gInSc1	vzduch
nasycený	nasycený	k2eAgInSc1d1	nasycený
vodní	vodní	k2eAgFnSc7d1	vodní
parou	para	k1gFnSc7	para
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
tedy	tedy	k9	tedy
přesně	přesně	k6eAd1	přesně
teplota	teplota	k1gFnSc1	teplota
trojného	trojný	k2eAgInSc2d1	trojný
bodu	bod	k1gInSc2	bod
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
rovnováze	rovnováha	k1gFnSc6	rovnováha
led	led	k1gInSc1	led
<g/>
,	,	kIx,	,
voda	voda	k1gFnSc1	voda
a	a	k8xC	a
pára	pára	k1gFnSc1	pára
(	(	kIx(	(
<g/>
beze	beze	k7c2	beze
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
trojného	trojný	k2eAgInSc2d1	trojný
bodu	bod	k1gInSc2	bod
je	být	k5eAaImIp3nS	být
0,01	[number]	k4	0,01
°	°	k?	°
<g/>
C.	C.	kA	C.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
se	se	k3xPyFc4	se
používal	používat	k5eAaImAgInS	používat
termín	termín	k1gInSc1	termín
"	"	kIx"	"
<g/>
stupeň	stupeň	k1gInSc1	stupeň
Kelvina	Kelvina	k1gFnSc1	Kelvina
<g/>
"	"	kIx"	"
a	a	k8xC	a
značka	značka	k1gFnSc1	značka
°	°	k?	°
<g/>
K.	K.	kA	K.
Roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
však	však	k9	však
tuto	tento	k3xDgFnSc4	tento
značku	značka	k1gFnSc4	značka
zrušila	zrušit	k5eAaPmAgFnS	zrušit
Generální	generální	k2eAgFnSc1d1	generální
konference	konference	k1gFnSc1	konference
pro	pro	k7c4	pro
míry	míra	k1gFnPc4	míra
a	a	k8xC	a
váhy	váha	k1gFnPc4	váha
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
"	"	kIx"	"
<g/>
stupeň	stupeň	k1gInSc1	stupeň
<g/>
"	"	kIx"	"
jako	jako	k8xC	jako
část	část	k1gFnSc1	část
názvu	název	k1gInSc2	název
jednotky	jednotka	k1gFnSc2	jednotka
nadále	nadále	k6eAd1	nadále
užívá	užívat	k5eAaImIp3nS	užívat
jen	jen	k9	jen
pro	pro	k7c4	pro
stupnice	stupnice	k1gFnSc1	stupnice
původem	původ	k1gInSc7	původ
empirické	empirický	k2eAgFnSc2d1	empirická
(	(	kIx(	(
<g/>
např.	např.	kA	např.
stupnice	stupnice	k1gFnSc2	stupnice
tvrdosti	tvrdost	k1gFnSc2	tvrdost
nebo	nebo	k8xC	nebo
dřívější	dřívější	k2eAgFnSc2d1	dřívější
stupnice	stupnice	k1gFnSc2	stupnice
teploty	teplota	k1gFnSc2	teplota
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Réaumur	Réaumur	k1gMnSc1	Réaumur
zavedl	zavést	k5eAaPmAgMnS	zavést
svou	svůj	k3xOyFgFnSc4	svůj
stupnici	stupnice	k1gFnSc4	stupnice
na	na	k7c6	na
základě	základ	k1gInSc6	základ
lihového	lihový	k2eAgInSc2d1	lihový
teploměru	teploměr	k1gInSc2	teploměr
maje	mít	k5eAaImSgMnS	mít
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
líh	líh	k1gInSc1	líh
se	se	k3xPyFc4	se
roztahuje	roztahovat	k5eAaImIp3nS	roztahovat
s	s	k7c7	s
teplotou	teplota	k1gFnSc7	teplota
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poznámky	poznámka	k1gFnSc2	poznámka
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
MILLS	MILLS	kA	MILLS
<g/>
,	,	kIx,	,
Ian	Ian	k1gMnSc1	Ian
<g/>
.	.	kIx.	.
</s>
<s>
IUPAC	IUPAC	kA	IUPAC
<g/>
.	.	kIx.	.
</s>
<s>
Quantities	Quantities	k1gInSc1	Quantities
<g/>
,	,	kIx,	,
Units	Units	k1gInSc1	Units
and	and	k?	and
Symbols	Symbols	k1gInSc1	Symbols
in	in	k?	in
Physical	Physical	k1gFnPc2	Physical
Chemistry	Chemistr	k1gMnPc4	Chemistr
<g/>
.	.	kIx.	.
</s>
<s>
Oxford	Oxford	k1gInSc1	Oxford
<g/>
,	,	kIx,	,
London	London	k1gMnSc1	London
<g/>
,	,	kIx,	,
Edinburgh	Edinburgh	k1gMnSc1	Edinburgh
<g/>
,	,	kIx,	,
Cambridge	Cambridge	k1gFnSc1	Cambridge
<g/>
,	,	kIx,	,
Carlton	Carlton	k1gInSc1	Carlton
Victoria	Victorium	k1gNnSc2	Victorium
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Blackwell	Blackwell	k1gInSc1	Blackwell
Scientific	Scientifice	k1gFnPc2	Scientifice
publications	publicationsa	k1gFnPc2	publicationsa
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
167	[number]	k4	167
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
632	[number]	k4	632
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
3583	[number]	k4	3583
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
3	[number]	k4	3
<g/>
;	;	kIx,	;
7	[number]	k4	7
<g/>
,	,	kIx,	,
s.	s.	k?	s.
70	[number]	k4	70
<g/>
;	;	kIx,	;
113	[number]	k4	113
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
KVASNICA	KVASNICA	kA	KVASNICA
<g/>
,	,	kIx,	,
Jozef	Jozef	k1gMnSc1	Jozef
<g/>
.	.	kIx.	.
</s>
<s>
Termodynamika	termodynamika	k1gFnSc1	termodynamika
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
SNTL	SNTL	kA	SNTL
<g/>
/	/	kIx~	/
<g/>
SVTL	SVTL	kA	SVTL
<g/>
,	,	kIx,	,
1965	[number]	k4	1965
<g/>
.	.	kIx.	.
396	[number]	k4	396
s.	s.	k?	s.
(	(	kIx(	(
<g/>
čes.	čes.	k?	čes.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
KVASNICA	KVASNICA	kA	KVASNICA
<g/>
,	,	kIx,	,
Jozef	Jozef	k1gMnSc1	Jozef
<g/>
.	.	kIx.	.
</s>
<s>
Statistická	statistický	k2eAgFnSc1d1	statistická
fyzika	fyzika	k1gFnSc1	fyzika
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
314	[number]	k4	314
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
676	[number]	k4	676
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
čes.	čes.	k?	čes.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
SVOBODA	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
Emanuel	Emanuel	k1gMnSc1	Emanuel
<g/>
;	;	kIx,	;
BAKULE	bakule	k1gFnSc1	bakule
<g/>
,	,	kIx,	,
Roman	Roman	k1gMnSc1	Roman
<g/>
.	.	kIx.	.
</s>
<s>
Molekulová	molekulový	k2eAgFnSc1d1	molekulová
fyzika	fyzika	k1gFnSc1	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
276	[number]	k4	276
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
25	[number]	k4	25
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
čes.	čes.	k?	čes.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
OBDRŽÁLEK	Obdržálek	k1gMnSc1	Obdržálek
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Úvod	úvod	k1gInSc1	úvod
do	do	k7c2	do
termodynamiky	termodynamika	k1gFnSc2	termodynamika
<g/>
,	,	kIx,	,
statistické	statistický	k2eAgFnSc2d1	statistická
fyziky	fyzika	k1gFnSc2	fyzika
a	a	k8xC	a
molekulové	molekulový	k2eAgFnSc2d1	molekulová
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
MatFyzPress	MatFyzPress	k1gInSc1	MatFyzPress
<g/>
,	,	kIx,	,
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
MFF	MFF	kA	MFF
UK	UK	kA	UK
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7378	[number]	k4	7378
<g/>
-	-	kIx~	-
<g/>
287	[number]	k4	287
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
čes.	čes.	k?	čes.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
OBDRŽÁLEK	Obdržálek	k1gMnSc1	Obdržálek
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Řešené	řešený	k2eAgInPc1d1	řešený
příklady	příklad	k1gInPc1	příklad
z	z	k7c2	z
termodynamiky	termodynamika	k1gFnSc2	termodynamika
<g/>
,	,	kIx,	,
statistické	statistický	k2eAgFnSc2d1	statistická
fyziky	fyzika	k1gFnSc2	fyzika
a	a	k8xC	a
molekulové	molekulový	k2eAgFnSc2d1	molekulová
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
MatFyzPress	MatFyzPress	k1gInSc1	MatFyzPress
<g/>
,	,	kIx,	,
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
MFF	MFF	kA	MFF
UK	UK	kA	UK
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7378	[number]	k4	7378
<g/>
-	-	kIx~	-
<g/>
300	[number]	k4	300
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
čes.	čes.	k?	čes.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ISO	ISO	kA	ISO
80000	[number]	k4	80000
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
Quantities	Quantities	k1gInSc1	Quantities
and	and	k?	and
units	units	k1gInSc1	units
-	-	kIx~	-
Part	part	k1gInSc1	part
5	[number]	k4	5
<g/>
:	:	kIx,	:
Thermodynamics	Thermodynamics	k1gInSc1	Thermodynamics
</s>
</p>
<p>
<s>
ISO	ISO	kA	ISO
80000	[number]	k4	80000
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
Quantities	Quantities	k1gInSc1	Quantities
and	and	k?	and
units	units	k1gInSc1	units
-	-	kIx~	-
Part	part	k1gInSc1	part
9	[number]	k4	9
<g/>
:	:	kIx,	:
Physical	Physical	k1gMnSc1	Physical
chemistry	chemistr	k1gMnPc7	chemistr
and	and	k?	and
molecular	molecular	k1gMnSc1	molecular
physics	physics	k6eAd1	physics
</s>
</p>
<p>
<s>
ČSN	ČSN	kA	ČSN
EN	EN	kA	EN
ISO	ISO	kA	ISO
80000	[number]	k4	80000
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
Veličiny	veličina	k1gFnPc1	veličina
a	a	k8xC	a
jednotky	jednotka	k1gFnPc1	jednotka
-	-	kIx~	-
Část	část	k1gFnSc1	část
5	[number]	k4	5
<g/>
:	:	kIx,	:
Termodynamika	termodynamika	k1gFnSc1	termodynamika
</s>
</p>
<p>
<s>
ČSN	ČSN	kA	ČSN
EN	EN	kA	EN
ISO	ISO	kA	ISO
80000	[number]	k4	80000
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
:	:	kIx,	:
<g/>
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
Veličiny	veličina	k1gFnPc1	veličina
a	a	k8xC	a
jednotky	jednotka	k1gFnPc1	jednotka
-	-	kIx~	-
Část	část	k1gFnSc1	část
9	[number]	k4	9
<g/>
:	:	kIx,	:
Fyzikální	fyzikální	k2eAgFnSc2d1	fyzikální
chemie	chemie	k1gFnSc2	chemie
a	a	k8xC	a
molekulová	molekulový	k2eAgFnSc1d1	molekulová
fyzika	fyzika	k1gFnSc1	fyzika
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
kelvin	kelvin	k1gInSc1	kelvin
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
