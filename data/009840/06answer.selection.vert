<s>
Kelvinovu	Kelvinův	k2eAgFnSc4d1	Kelvinova
stupnici	stupnice	k1gFnSc4	stupnice
měření	měření	k1gNnSc2	měření
teplot	teplota	k1gFnPc2	teplota
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
skotský	skotský	k2eAgMnSc1d1	skotský
matematik	matematik	k1gMnSc1	matematik
a	a	k8xC	a
fyzik	fyzik	k1gMnSc1	fyzik
William	William	k1gInSc1	William
Thomson	Thomson	k1gInSc4	Thomson
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
za	za	k7c4	za
své	svůj	k3xOyFgInPc4	svůj
výrazné	výrazný	k2eAgInPc4d1	výrazný
vědecké	vědecký	k2eAgInPc4d1	vědecký
úspěchy	úspěch	k1gInPc4	úspěch
povýšen	povýšit	k5eAaPmNgMnS	povýšit
do	do	k7c2	do
šlechtického	šlechtický	k2eAgInSc2d1	šlechtický
stavu	stav	k1gInSc2	stav
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
lord	lord	k1gMnSc1	lord
Kelvin	kelvin	k1gInSc1	kelvin
<g/>
.	.	kIx.	.
</s>
