<s>
Egyptské	egyptský	k2eAgNnSc1d1	egyptské
hieroglyfické	hieroglyfický	k2eAgNnSc1d1	hieroglyfické
písmo	písmo	k1gNnSc1	písmo
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckého	řecký	k2eAgInSc2d1	řecký
ἱ	ἱ	k?	ἱ
γ	γ	k?	γ
<g/>
,	,	kIx,	,
hiera	hiera	k1gMnSc1	hiera
glyfé	glyfá	k1gFnSc2	glyfá
–	–	k?	–
"	"	kIx"	"
<g/>
posvátný	posvátný	k2eAgInSc1d1	posvátný
znak	znak	k1gInSc1	znak
<g/>
,	,	kIx,	,
symbol	symbol	k1gInSc1	symbol
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
2900	[number]	k4	2900
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
při	při	k7c6	při
sloučení	sloučení	k1gNnSc6	sloučení
Horního	horní	k2eAgInSc2d1	horní
a	a	k8xC	a
Dolního	dolní	k2eAgInSc2d1	dolní
Egypta	Egypt	k1gInSc2	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Egypťané	Egypťan	k1gMnPc1	Egypťan
sami	sám	k3xTgMnPc1	sám
jej	on	k3xPp3gMnSc4	on
nazývali	nazývat	k5eAaImAgMnP	nazývat
"	"	kIx"	"
<g/>
písmo	písmo	k1gNnSc1	písmo
bohů	bůh	k1gMnPc2	bůh
<g/>
"	"	kIx"	"
neboť	neboť	k8xC	neboť
věřili	věřit	k5eAaImAgMnP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
a	a	k8xC	a
lidem	lid	k1gInSc7	lid
předal	předat	k5eAaPmAgMnS	předat
bůh	bůh	k1gMnSc1	bůh
Thovt	Thovt	k1gMnSc1	Thovt
<g/>
.	.	kIx.	.
</s>
