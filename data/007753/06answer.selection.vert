<s>
Země	země	k1gFnSc1	země
jako	jako	k8xC	jako
domovský	domovský	k2eAgInSc1d1	domovský
svět	svět	k1gInSc1	svět
lidstva	lidstvo	k1gNnSc2	lidstvo
má	mít	k5eAaImIp3nS	mít
mnoho	mnoho	k4c1	mnoho
názvů	název	k1gInPc2	název
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
národu	národ	k1gInSc6	národ
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgInPc4d3	nejznámější
patří	patřit	k5eAaImIp3nS	patřit
název	název	k1gInSc4	název
latinského	latinský	k2eAgInSc2d1	latinský
původu	původ	k1gInSc2	původ
Terra	Terro	k1gNnSc2	Terro
<g/>
,	,	kIx,	,
či	či	k8xC	či
řecký	řecký	k2eAgInSc1d1	řecký
název	název	k1gInSc1	název
Gaia	Gai	k1gInSc2	Gai
<g/>
.	.	kIx.	.
</s>
