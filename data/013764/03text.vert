<s>
Žně	žeň	k1gFnPc4
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaImF,k5eAaPmF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Konkrétní	konkrétní	k2eAgInPc4d1
problémy	problém	k1gInPc4
<g/>
:	:	kIx,
zaměňují	zaměňovat	k5eAaImIp3nP
se	se	k3xPyFc4
výrazy	výraz	k1gInPc7
"	"	kIx"
<g/>
žně	žeň	k1gFnPc4
<g/>
"	"	kIx"
a	a	k8xC
"	"	kIx"
<g/>
sklizeň	sklizeň	k1gFnSc1
</s>
<s>
F.	F.	kA
Sychkov	Sychkov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Strada	strada	k1gFnSc1
(	(	kIx(
<g/>
žně	žeň	k1gFnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
1910	#num#	k4
</s>
<s>
Žně	žeň	k1gFnPc1
jsou	být	k5eAaImIp3nP
typ	typ	k1gInSc4
sklizně	sklizeň	k1gFnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
sklízí	sklízet	k5eAaImIp3nP
především	především	k9
obiloviny	obilovina	k1gFnPc1
(	(	kIx(
<g/>
pšenice	pšenice	k1gFnPc1
<g/>
,	,	kIx,
ječmen	ječmen	k1gInSc1
<g/>
,	,	kIx,
kukuřice	kukuřice	k1gFnSc1
<g/>
,	,	kIx,
žito	žito	k1gNnSc1
<g/>
,	,	kIx,
oves	oves	k1gInSc1
atd.	atd.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Česku	Česko	k1gNnSc6
a	a	k8xC
celém	celý	k2eAgInSc6d1
mírném	mírný	k2eAgInSc6d1
pásu	pás	k1gInSc6
žně	žeň	k1gFnSc2
probíhají	probíhat	k5eAaImIp3nP
v	v	k7c6
létě	léto	k1gNnSc6
<g/>
,	,	kIx,
v	v	k7c6
období	období	k1gNnSc6
relativního	relativní	k2eAgNnSc2d1
sucha	sucho	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejčastěji	často	k6eAd3
se	s	k7c7
sklízí	sklíze	k1gFnSc7
od	od	k7c2
června	červen	k1gInSc2
do	do	k7c2
září	září	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obiloviny	obilovina	k1gFnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
další	další	k2eAgFnPc1d1
plodiny	plodina	k1gFnPc1
jako	jako	k8xC,k8xS
luskoviny	luskovina	k1gFnPc1
(	(	kIx(
<g/>
hrách	hrách	k1gInSc1
<g/>
,	,	kIx,
fazole	fazole	k1gFnSc1
<g/>
,	,	kIx,
soja	soja	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
olejniny	olejnina	k1gFnSc2
(	(	kIx(
<g/>
řepka	řepka	k1gFnSc1
<g/>
)	)	kIx)
či	či	k8xC
brambory	brambor	k1gInPc1
<g/>
,	,	kIx,
se	se	k3xPyFc4
sklízí	sklízet	k5eAaImIp3nS
sklízecími	sklízecí	k2eAgInPc7d1
stroji	stroj	k1gInPc7
(	(	kIx(
<g/>
kombajny	kombajn	k1gInPc7
<g/>
)	)	kIx)
nebo	nebo	k8xC
ručně	ručně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zrno	zrno	k1gNnSc1
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
dostatečně	dostatečně	k6eAd1
zralé	zralý	k2eAgNnPc1d1
(	(	kIx(
<g/>
tvrdé	tvrdý	k2eAgNnPc1d1
<g/>
)	)	kIx)
a	a	k8xC
suché	suchý	k2eAgNnPc1d1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
jej	on	k3xPp3gMnSc4
mohl	moct	k5eAaImAgInS
kombajn	kombajn	k1gInSc1
vymlátit	vymlátit	k5eAaPmF
a	a	k8xC
aby	aby	kYmCp3nS
při	při	k7c6
skladování	skladování	k1gNnSc6
nezplesnivělo	zplesnivět	k5eNaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sklízí	sklíze	k1gFnPc2
se	se	k3xPyFc4
zrno	zrno	k1gNnSc1
s	s	k7c7
vlhkostí	vlhkost	k1gFnSc7
kolem	kolem	k7c2
15	#num#	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
žních	žeň	k1gFnPc6
se	se	k3xPyFc4
v	v	k7c6
minulosti	minulost	k1gFnSc6
konaly	konat	k5eAaImAgInP
dožínky	dožínek	k1gInPc1
jako	jako	k8xS,k8xC
oslava	oslava	k1gFnSc1
jejich	jejich	k3xOp3gNnSc2
úspěšného	úspěšný	k2eAgNnSc2d1
ukončení	ukončení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Scházívali	scházívat	k5eAaImAgMnP
se	se	k3xPyFc4
zde	zde	k6eAd1
všichni	všechen	k3xTgMnPc1
<g/>
,	,	kIx,
kdo	kdo	k3yRnSc1,k3yQnSc1,k3yInSc1
se	s	k7c7
žní	žeň	k1gFnSc7
zúčastnili	zúčastnit	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současnosti	současnost	k1gFnSc6
jsou	být	k5eAaImIp3nP
dožínky	dožínek	k1gInPc1
často	často	k6eAd1
jen	jen	k9
společenskou	společenský	k2eAgFnSc7d1
akcí	akce	k1gFnSc7
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
bez	bez	k7c2
přímé	přímý	k2eAgFnSc2d1
vazby	vazba	k1gFnSc2
na	na	k7c4
účastníky	účastník	k1gMnPc4
žní	žeň	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Dožínky	dožínky	k1gFnPc1
na	na	k7c6
Letné	Letná	k1gFnSc6
(	(	kIx(
<g/>
akce	akce	k1gFnSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
žně	žeň	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
žeň	žeň	k1gFnSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4253599-2	4253599-2	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85056214	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85056214	#num#	k4
</s>
