<s>
Československo	Československo	k1gNnSc1	Československo
mělo	mít	k5eAaImAgNnS	mít
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
podepsanou	podepsaný	k2eAgFnSc4d1	podepsaná
spojeneckou	spojenecký	k2eAgFnSc4d1	spojenecká
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
smluvní	smluvní	k2eAgFnPc1d1	smluvní
strany	strana	k1gFnPc1	strana
zavázaly	zavázat	k5eAaPmAgFnP	zavázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
případě	případ	k1gInSc6	případ
ohrožení	ohrožení	k1gNnPc2	ohrožení
společných	společný	k2eAgInPc2d1	společný
zájmů	zájem	k1gInPc2	zájem
se	se	k3xPyFc4	se
shodnou	shodnout	k5eAaPmIp3nP	shodnout
na	na	k7c6	na
opatřeních	opatření	k1gNnPc6	opatření
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
je	on	k3xPp3gNnSc4	on
měla	mít	k5eAaImAgFnS	mít
chránit	chránit	k5eAaImF	chránit
<g/>
.	.	kIx.	.
</s>
