<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Anglie	Anglie	k1gFnSc2	Anglie
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
bílým	bílý	k2eAgInSc7d1	bílý
listem	list	k1gInSc7	list
se	s	k7c7	s
svatojiřským	svatojiřský	k2eAgInSc7d1	svatojiřský
křížem	kříž	k1gInSc7	kříž
<g/>
.	.	kIx.	.
</s>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Jiří	Jiří	k1gMnSc1	Jiří
byl	být	k5eAaImAgMnS	být
podle	podle	k7c2	podle
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
hagiografie	hagiografie	k1gFnSc2	hagiografie
římským	římský	k2eAgMnSc7d1	římský
vojákem	voják	k1gMnSc7	voják
pocházejícím	pocházející	k2eAgFnPc3d1	pocházející
z	z	k7c2	z
Anatolie	Anatolie	k1gFnSc2	Anatolie
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgFnPc4d1	jiná
i	i	k8xC	i
<g/>
)	)	kIx)	)
patronem	patron	k1gInSc7	patron
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
<g/>
Tento	tento	k3xDgInSc1	tento
červený	červený	k2eAgInSc1d1	červený
kříž	kříž	k1gInSc1	kříž
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
jako	jako	k9	jako
znak	znak	k1gInSc1	znak
Anglie	Anglie	k1gFnSc2	Anglie
už	už	k6eAd1	už
v	v	k7c6	v
době	doba	k1gFnSc6	doba
křižáckých	křižácký	k2eAgFnPc2d1	křižácká
výprav	výprava	k1gFnPc2	výprava
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgInPc2d3	nejstarší
znaků	znak	k1gInPc2	znak
reprezentujících	reprezentující	k2eAgFnPc2d1	reprezentující
Anglii	Anglie	k1gFnSc4	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Status	status	k1gInSc1	status
národní	národní	k2eAgFnSc2d1	národní
vlajky	vlajka	k1gFnSc2	vlajka
Anglie	Anglie	k1gFnSc2	Anglie
dostala	dostat	k5eAaPmAgFnS	dostat
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
vlajce	vlajka	k1gFnSc6	vlajka
je	být	k5eAaImIp3nS	být
vyobrazený	vyobrazený	k2eAgInSc1d1	vyobrazený
červený	červený	k2eAgInSc1d1	červený
kříž	kříž	k1gInSc1	kříž
na	na	k7c6	na
bílém	bílý	k2eAgNnSc6d1	bílé
pozadí	pozadí	k1gNnSc6	pozadí
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
šířka	šířka	k1gFnSc1	šířka
kříže	kříž	k1gInSc2	kříž
je	být	k5eAaImIp3nS	být
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
5	[number]	k4	5
výšky	výška	k1gFnSc2	výška
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Poměr	poměr	k1gInSc1	poměr
stran	strana	k1gFnPc2	strana
je	být	k5eAaImIp3nS	být
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Anglie	Anglie	k1gFnSc2	Anglie
byla	být	k5eAaImAgFnS	být
zakomponována	zakomponovat	k5eAaPmNgFnS	zakomponovat
do	do	k7c2	do
britské	britský	k2eAgFnSc2d1	britská
vlajky	vlajka	k1gFnSc2	vlajka
a	a	k8xC	a
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
odvozených	odvozený	k2eAgFnPc2d1	odvozená
vlajek	vlajka	k1gFnPc2	vlajka
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
např.	např.	kA	např.
do	do	k7c2	do
gruzínské	gruzínský	k2eAgFnSc2d1	gruzínská
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
,	,	kIx,	,
či	či	k8xC	či
vlajky	vlajka	k1gFnPc1	vlajka
kanadské	kanadský	k2eAgFnSc2d1	kanadská
provincie	provincie	k1gFnSc2	provincie
Alberta	Albert	k1gMnSc2	Albert
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historické	historický	k2eAgFnPc1d1	historická
vlajky	vlajka	k1gFnPc1	vlajka
Anglického	anglický	k2eAgNnSc2d1	anglické
království	království	k1gNnSc2	království
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Alternativní	alternativní	k2eAgFnPc1d1	alternativní
vlajky	vlajka	k1gFnPc1	vlajka
Anglie	Anglie	k1gFnSc2	Anglie
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Vlajka	vlajka	k1gFnSc1	vlajka
Anglicka	Anglicko	k1gNnPc4	Anglicko
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Znak	znak	k1gInSc1	znak
Anglie	Anglie	k1gFnSc2	Anglie
</s>
</p>
<p>
<s>
Anglická	anglický	k2eAgFnSc1d1	anglická
hymna	hymna	k1gFnSc1	hymna
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Anglie	Anglie	k1gFnSc2	Anglie
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Anglická	anglický	k2eAgFnSc1d1	anglická
vlajka	vlajka	k1gFnSc1	vlajka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
