<s>
Nadační	nadační	k2eAgInSc1d1	nadační
fond	fond	k1gInSc1	fond
pro	pro	k7c4	pro
boj	boj	k1gInSc4	boj
proti	proti	k7c3	proti
korupci	korupce	k1gFnSc3	korupce
(	(	kIx(	(
<g/>
NFPK	NFPK	kA	NFPK
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
původní	původní	k2eAgFnSc2d1	původní
<g/>
,	,	kIx,	,
zamítnuté	zamítnutý	k2eAgFnSc2d1	zamítnutá
žádosti	žádost	k1gFnSc2	žádost
o	o	k7c4	o
registraci	registrace	k1gFnSc4	registrace
Nadační	nadační	k2eAgInSc1d1	nadační
fond	fond	k1gInSc1	fond
proti	proti	k7c3	proti
korupci	korupce	k1gFnSc3	korupce
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
nestátní	státní	k2eNgFnSc1d1	nestátní
nadace	nadace	k1gFnSc1	nadace
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
15	[number]	k4	15
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2011	[number]	k4	2011
zřídili	zřídit	k5eAaPmAgMnP	zřídit
Karel	Karel	k1gMnSc1	Karel
Janeček	Janeček	k1gMnSc1	Janeček
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
Bernard	Bernard	k1gMnSc1	Bernard
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
Kraus	Kraus	k1gMnSc1	Kraus
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
podpora	podpora	k1gFnSc1	podpora
boje	boj	k1gInSc2	boj
proti	proti	k7c3	proti
korupci	korupce	k1gFnSc3	korupce
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
motto	motto	k1gNnSc1	motto
zní	znět	k5eAaImIp3nS	znět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nebát	bát	k5eNaImF	bát
se	se	k3xPyFc4	se
a	a	k8xC	a
nekrást	krást	k5eNaImF	krást
<g/>
,	,	kIx,	,
a	a	k8xC	a
ne	ne	k9	ne
nakrást	nakrást	k5eAaBmF	nakrást
si	se	k3xPyFc3	se
a	a	k8xC	a
nebát	bát	k5eNaImF	bát
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
<s>
Cena	cena	k1gFnSc1	cena
za	za	k7c4	za
whistleblowing	whistleblowing	k1gInSc4	whistleblowing
-	-	kIx~	-
Libor	Libor	k1gMnSc1	Libor
Michálek	Michálek	k1gMnSc1	Michálek
-	-	kIx~	-
bývalý	bývalý	k2eAgMnSc1d1	bývalý
ředitel	ředitel	k1gMnSc1	ředitel
Státního	státní	k2eAgInSc2d1	státní
fondu	fond	k1gInSc2	fond
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
500	[number]	k4	500
000	[number]	k4	000
Kč	Kč	kA	Kč
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
23	[number]	k4	23
<g/>
<g />
.	.	kIx.	.
</s>
<s>
března	březen	k1gInSc2	březen
2011	[number]	k4	2011
Cena	cena	k1gFnSc1	cena
za	za	k7c4	za
odvahu	odvaha	k1gFnSc4	odvaha
2011	[number]	k4	2011
Ondřej	Ondřej	k1gMnSc1	Ondřej
Závodský	Závodský	k2eAgMnSc1d1	Závodský
-	-	kIx~	-
nevidomý	vidomý	k2eNgMnSc1d1	nevidomý
právník	právník	k1gMnSc1	právník
Zařízení	zařízení	k1gNnSc2	zařízení
služeb	služba	k1gFnPc2	služba
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
100	[number]	k4	100
000	[number]	k4	000
Kč	Kč	kA	Kč
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
23	[number]	k4	23
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2011	[number]	k4	2011
Renata	Renata	k1gFnSc1	Renata
Horáková	Horáková	k1gFnSc1	Horáková
-	-	kIx~	-
bývalá	bývalý	k2eAgFnSc1d1	bývalá
úřednice	úřednice	k1gFnSc1	úřednice
znojemské	znojemský	k2eAgFnSc2d1	Znojemská
radnice	radnice	k1gFnSc2	radnice
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
300	[number]	k4	300
000	[number]	k4	000
Kč	Kč	kA	Kč
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
9	[number]	k4	9
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2011	[number]	k4	2011
Cena	cena	k1gFnSc1	cena
za	za	k7c4	za
odvahu	odvaha	k1gFnSc4	odvaha
2012	[number]	k4	2012
Jakub	Jakub	k1gMnSc1	Jakub
Klouzal	klouzat	k5eAaImAgMnS	klouzat
-	-	kIx~	-
bývalý	bývalý	k2eAgMnSc1d1	bývalý
ředitel	ředitel	k1gMnSc1	ředitel
IT	IT	kA	IT
odboru	odbor	k1gInSc2	odbor
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
zahraničí	zahraničí	k1gNnSc2	zahraničí
,	,	kIx,	,
(	(	kIx(	(
<g/>
100	[number]	k4	100
000	[number]	k4	000
Kč	Kč	kA	Kč
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
18	[number]	k4	18
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2012	[number]	k4	2012
Věra	Věra	k1gFnSc1	Věra
Ježková	Ježková	k1gFnSc1	Ježková
-	-	kIx~	-
zaměstnankyně	zaměstnankyně	k1gFnSc1	zaměstnankyně
Státního	státní	k2eAgInSc2d1	státní
fondu	fond	k1gInSc2	fond
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
-	-	kIx~	-
odboru	odbor	k1gInSc2	odbor
kontroly	kontrola	k1gFnSc2	kontrola
,	,	kIx,	,
(	(	kIx(	(
<g/>
200	[number]	k4	200
<g />
.	.	kIx.	.
</s>
<s>
000	[number]	k4	000
Kč	Kč	kA	Kč
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
8	[number]	k4	8
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2012	[number]	k4	2012
Martin	Martin	k1gMnSc1	Martin
Konečný	Konečný	k1gMnSc1	Konečný
-	-	kIx~	-
bývalý	bývalý	k2eAgMnSc1d1	bývalý
primář	primář	k1gMnSc1	primář
psychiatrického	psychiatrický	k2eAgNnSc2d1	psychiatrické
oddělení	oddělení	k1gNnSc2	oddělení
Nemocnice	nemocnice	k1gFnSc2	nemocnice
v	v	k7c6	v
Ostrově	ostrov	k1gInSc6	ostrov
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
300	[number]	k4	300
000	[number]	k4	000
Kč	Kč	kA	Kč
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2012	[number]	k4	2012
Cena	cena	k1gFnSc1	cena
za	za	k7c4	za
odvahu	odvaha	k1gFnSc4	odvaha
2013	[number]	k4	2013
Vladimír	Vladimír	k1gMnSc1	Vladimír
Sitta	Sitta	k1gMnSc1	Sitta
<g/>
,	,	kIx,	,
ml.	ml.	kA	ml.
-	-	kIx~	-
společně	společně	k6eAd1	společně
se	s	k7c7	s
svým	svůj	k3xOyFgNnSc7	svůj
<g />
.	.	kIx.	.
</s>
<s>
otcem	otec	k1gMnSc7	otec
se	se	k3xPyFc4	se
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
podílet	podílet	k5eAaImF	podílet
na	na	k7c4	na
tunelování	tunelování	k1gNnSc4	tunelování
Dopravního	dopravní	k2eAgInSc2d1	dopravní
podniku	podnik	k1gInSc2	podnik
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
a	a	k8xC	a
přišli	přijít	k5eAaPmAgMnP	přijít
o	o	k7c4	o
svoji	svůj	k3xOyFgFnSc4	svůj
rodinnou	rodinný	k2eAgFnSc4d1	rodinná
firmu	firma	k1gFnSc4	firma
<g/>
,	,	kIx,	,
9	[number]	k4	9
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2013	[number]	k4	2013
Jiří	Jiří	k1gMnSc1	Jiří
Chytil	Chytil	k1gMnSc1	Chytil
-	-	kIx~	-
někdejší	někdejší	k2eAgMnSc1d1	někdejší
magistrátní	magistrátní	k2eAgMnSc1d1	magistrátní
úředník	úředník	k1gMnSc1	úředník
pomáhal	pomáhat	k5eAaImAgMnS	pomáhat
vyšetřovatelům	vyšetřovatel	k1gMnPc3	vyšetřovatel
rozplétat	rozplétat	k5eAaImF	rozplétat
nitky	nitka	k1gFnPc4	nitka
kolem	kolem	k7c2	kolem
politického	politický	k2eAgNnSc2d1	politické
pozadí	pozadí	k1gNnSc2	pozadí
problematické	problematický	k2eAgFnSc2d1	problematická
zakázky	zakázka	k1gFnSc2	zakázka
firmě	firma	k1gFnSc3	firma
Haguess	Haguess	k1gInSc1	Haguess
<g/>
,	,	kIx,	,
9	[number]	k4	9
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2013	[number]	k4	2013
Cena	cena	k1gFnSc1	cena
za	za	k7c4	za
<g />
.	.	kIx.	.
</s>
<s>
odvahu	odvaha	k1gFnSc4	odvaha
2014	[number]	k4	2014
Leo	Leo	k1gMnSc1	Leo
Steiner	Steiner	k1gMnSc1	Steiner
-	-	kIx~	-
bývalý	bývalý	k2eAgMnSc1d1	bývalý
ředitel	ředitel	k1gMnSc1	ředitel
Odboru	odbor	k1gInSc2	odbor
řízení	řízení	k1gNnSc2	řízení
programu	program	k1gInSc2	program
Úřadu	úřad	k1gInSc2	úřad
Regionálního	regionální	k2eAgInSc2d1	regionální
operačního	operační	k2eAgInSc2d1	operační
programu	program	k1gInSc2	program
Severozápad	severozápad	k1gInSc1	severozápad
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
člen	člen	k1gMnSc1	člen
Monitorovacího	monitorovací	k2eAgInSc2d1	monitorovací
výboru	výbor	k1gInSc2	výbor
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
se	s	k7c7	s
zjištěním	zjištění	k1gNnSc7	zjištění
o	o	k7c4	o
rozkrádání	rozkrádání	k1gNnSc4	rozkrádání
eurodotací	eurodotace	k1gFnPc2	eurodotace
František	František	k1gMnSc1	František
Mráček	Mráček	k1gMnSc1	Mráček
-	-	kIx~	-
bývalý	bývalý	k2eAgMnSc1d1	bývalý
vedoucí	vedoucí	k1gMnSc1	vedoucí
Oddělení	oddělení	k1gNnSc2	oddělení
veřejné	veřejný	k2eAgFnSc2d1	veřejná
dopravy	doprava	k1gFnSc2	doprava
Krajského	krajský	k2eAgInSc2d1	krajský
úřadu	úřad	k1gInSc2	úřad
Středočeského	středočeský	k2eAgInSc2d1	středočeský
kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
poukazoval	poukazovat	k5eAaImAgInS	poukazovat
mezi	mezi	k7c7	mezi
jinými	jiné	k1gNnPc7	jiné
na	na	k7c4	na
pochybnou	pochybný	k2eAgFnSc4d1	pochybná
investici	investice	k1gFnSc4	investice
do	do	k7c2	do
software	software	k1gInSc1	software
Cena	cena	k1gFnSc1	cena
<g />
.	.	kIx.	.
</s>
<s>
za	za	k7c4	za
odvahu	odvaha	k1gFnSc4	odvaha
2015	[number]	k4	2015
Jana	Jana	k1gFnSc1	Jana
Průšková	Průšková	k1gFnSc1	Průšková
-	-	kIx~	-
za	za	k7c4	za
odhalení	odhalení	k1gNnSc4	odhalení
pokřiveného	pokřivený	k2eAgNnSc2d1	pokřivené
jednání	jednání	k1gNnSc2	jednání
a	a	k8xC	a
zmanipulovaného	zmanipulovaný	k2eAgNnSc2d1	zmanipulované
<g/>
,	,	kIx,	,
pochybného	pochybný	k2eAgNnSc2d1	pochybné
hospodaření	hospodaření	k1gNnSc2	hospodaření
neziskové	ziskový	k2eNgFnSc2d1	nezisková
organizace	organizace	k1gFnSc2	organizace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
zajišťovat	zajišťovat	k5eAaImF	zajišťovat
rozvoj	rozvoj	k1gInSc4	rozvoj
dětí	dítě	k1gFnPc2	dítě
pěveckého	pěvecký	k2eAgInSc2d1	pěvecký
sboru	sbor	k1gInSc2	sbor
Severáček	Severáček	k1gMnSc1	Severáček
na	na	k7c6	na
Liberecku	Liberecko	k1gNnSc6	Liberecko
Lukáš	Lukáš	k1gMnSc1	Lukáš
Wagenknecht	Wagenknecht	k1gMnSc1	Wagenknecht
-	-	kIx~	-
za	za	k7c4	za
své	svůj	k3xOyFgInPc4	svůj
dlouhodobé	dlouhodobý	k2eAgInPc4d1	dlouhodobý
nekompromisní	kompromisní	k2eNgInPc4d1	nekompromisní
postoje	postoj	k1gInPc4	postoj
v	v	k7c6	v
roli	role	k1gFnSc6	role
auditora	auditor	k1gMnSc2	auditor
např.	např.	kA	např.
v	v	k7c6	v
Dopravním	dopravní	k2eAgInSc6d1	dopravní
podniku	podnik	k1gInSc6	podnik
hl.	hl.	k?	hl.
m.	m.	k?	m.
Prahy	Praha	k1gFnSc2	Praha
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
DPP	DPP	kA	DPP
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
na	na	k7c6	na
Ministerstvu	ministerstvo	k1gNnSc6	ministerstvo
financí	finance	k1gFnPc2	finance
Fond	fond	k1gInSc4	fond
chce	chtít	k5eAaImIp3nS	chtít
podporovat	podporovat	k5eAaImF	podporovat
projekty	projekt	k1gInPc4	projekt
odhalující	odhalující	k2eAgFnSc2d1	odhalující
korupční	korupční	k2eAgFnSc2d1	korupční
činnosti	činnost	k1gFnSc2	činnost
s	s	k7c7	s
významným	významný	k2eAgInSc7d1	významný
ekonomickým	ekonomický	k2eAgInSc7d1	ekonomický
dopadem	dopad	k1gInSc7	dopad
na	na	k7c4	na
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
správu	správa	k1gFnSc4	správa
<g/>
,	,	kIx,	,
udílet	udílet	k5eAaImF	udílet
ceny	cena	k1gFnPc4	cena
za	za	k7c4	za
prokázání	prokázání	k1gNnSc4	prokázání
korupce	korupce	k1gFnSc2	korupce
významného	významný	k2eAgInSc2d1	významný
ekonomického	ekonomický	k2eAgInSc2d1	ekonomický
dopadu	dopad	k1gInSc2	dopad
<g/>
,	,	kIx,	,
vypisovat	vypisovat	k5eAaImF	vypisovat
granty	grant	k1gInPc4	grant
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
investigativních	investigativní	k2eAgMnPc2d1	investigativní
novinářů	novinář	k1gMnPc2	novinář
<g/>
,	,	kIx,	,
usilovat	usilovat	k5eAaImF	usilovat
o	o	k7c4	o
dotahování	dotahování	k1gNnSc4	dotahování
zveřejněných	zveřejněný	k2eAgInPc2d1	zveřejněný
případů	případ	k1gInPc2	případ
korupce	korupce	k1gFnSc2	korupce
do	do	k7c2	do
konce	konec	k1gInSc2	konec
<g/>
,	,	kIx,	,
přispívat	přispívat	k5eAaImF	přispívat
k	k	k7c3	k
budování	budování	k1gNnSc3	budování
etických	etický	k2eAgFnPc2d1	etická
hodnot	hodnota	k1gFnPc2	hodnota
v	v	k7c6	v
demokratické	demokratický	k2eAgFnSc6d1	demokratická
společnosti	společnost	k1gFnSc6	společnost
a	a	k8xC	a
tlačit	tlačit	k5eAaImF	tlačit
na	na	k7c4	na
politiky	politik	k1gMnPc4	politik
k	k	k7c3	k
provedení	provedení	k1gNnSc3	provedení
nutných	nutný	k2eAgFnPc2d1	nutná
legislativních	legislativní	k2eAgFnPc2d1	legislativní
změn	změna	k1gFnPc2	změna
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálně	oficiálně	k6eAd1	oficiálně
zahájil	zahájit	k5eAaPmAgMnS	zahájit
svou	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
23	[number]	k4	23
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2011	[number]	k4	2011
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
velvyslance	velvyslanec	k1gMnSc2	velvyslanec
USA	USA	kA	USA
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
Normana	Norman	k1gMnSc2	Norman
Eisena	Eisen	k1gMnSc2	Eisen
ve	v	k7c6	v
Vratislavském	vratislavský	k2eAgInSc6d1	vratislavský
paláci	palác	k1gInSc6	palác
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
udělením	udělení	k1gNnSc7	udělení
prvních	první	k4xOgFnPc2	první
dvou	dva	k4xCgFnPc2	dva
cen	cena	k1gFnPc2	cena
<g/>
.	.	kIx.	.
</s>
<s>
Odměněni	odměněn	k2eAgMnPc1d1	odměněn
byli	být	k5eAaImAgMnP	být
Libor	Libor	k1gMnSc1	Libor
Michálek	Michálek	k1gMnSc1	Michálek
a	a	k8xC	a
Ondřej	Ondřej	k1gMnSc1	Ondřej
Závodský	Závodský	k2eAgMnSc1d1	Závodský
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
riskovali	riskovat	k5eAaBmAgMnP	riskovat
svou	svůj	k3xOyFgFnSc4	svůj
budoucnost	budoucnost	k1gFnSc4	budoucnost
a	a	k8xC	a
navzdory	navzdory	k7c3	navzdory
chybějící	chybějící	k2eAgFnSc3d1	chybějící
právní	právní	k2eAgFnSc3d1	právní
ochraně	ochrana	k1gFnSc3	ochrana
oznámili	oznámit	k5eAaPmAgMnP	oznámit
korupční	korupční	k2eAgNnSc4d1	korupční
podezření	podezření	k1gNnSc4	podezření
na	na	k7c6	na
svých	svůj	k3xOyFgInPc6	svůj
úřadech	úřad	k1gInPc6	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Členy	člen	k1gMnPc7	člen
správní	správní	k2eAgFnSc2d1	správní
rady	rada	k1gFnSc2	rada
nadace	nadace	k1gFnSc1	nadace
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Mgr.	Mgr.	kA	Mgr.
Karel	Karel	k1gMnSc1	Karel
Janeček	Janeček	k1gMnSc1	Janeček
<g/>
,	,	kIx,	,
MBA	MBA	kA	MBA
<g/>
,	,	kIx,	,
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
,	,	kIx,	,
matematik	matematik	k1gMnSc1	matematik
a	a	k8xC	a
podnikatel	podnikatel	k1gMnSc1	podnikatel
Ing.	ing.	kA	ing.
Stanislav	Stanislav	k1gMnSc1	Stanislav
Bernard	Bernard	k1gMnSc1	Bernard
<g/>
,	,	kIx,	,
podnikatel	podnikatel	k1gMnSc1	podnikatel
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
pivovarnictví	pivovarnictví	k1gNnSc2	pivovarnictví
a	a	k8xC	a
médií	médium	k1gNnPc2	médium
Jan	Jan	k1gMnSc1	Jan
Kraus	Kraus	k1gMnSc1	Kraus
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
moderátor	moderátor	k1gMnSc1	moderátor
Karel	Karel	k1gMnSc1	Karel
Randák	Randák	k1gMnSc1	Randák
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
ředitel	ředitel	k1gMnSc1	ředitel
Úřadu	úřad	k1gInSc2	úřad
<g />
.	.	kIx.	.
</s>
<s>
pro	pro	k7c4	pro
zahraniční	zahraniční	k2eAgInPc4d1	zahraniční
styky	styk	k1gInPc4	styk
a	a	k8xC	a
informace	informace	k1gFnSc2	informace
Ing.	ing.	kA	ing.
Radim	Radim	k1gMnSc1	Radim
Jančura	Jančura	k1gFnSc1	Jančura
<g/>
,	,	kIx,	,
podnikatel	podnikatel	k1gMnSc1	podnikatel
v	v	k7c6	v
cestovním	cestovní	k2eAgInSc6d1	cestovní
ruchu	ruch	k1gInSc6	ruch
Libor	Libor	k1gMnSc1	Libor
Winkler	Winkler	k1gMnSc1	Winkler
JUDr.	JUDr.	kA	JUDr.
Lenka	Lenka	k1gFnSc1	Lenka
Deverová	Deverová	k1gFnSc1	Deverová
Jannis	Jannis	k1gFnSc1	Jannis
Samaras	Samaras	k1gInSc4	Samaras
Fond	fond	k1gInSc1	fond
obvinil	obvinit	k5eAaPmAgInS	obvinit
pražského	pražský	k2eAgMnSc2d1	pražský
podnikatele	podnikatel	k1gMnSc2	podnikatel
Ivo	Ivo	k1gMnSc1	Ivo
Rittiga	Rittig	k1gMnSc2	Rittig
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
regionálních	regionální	k2eAgMnPc2d1	regionální
kmotrů	kmotr	k1gMnPc2	kmotr
ODS	ODS	kA	ODS
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
jeho	jeho	k3xOp3gInPc4	jeho
účty	účet	k1gInPc4	účet
přicházely	přicházet	k5eAaImAgFnP	přicházet
desítky	desítka	k1gFnPc1	desítka
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
z	z	k7c2	z
pražského	pražský	k2eAgInSc2d1	pražský
dopravního	dopravní	k2eAgInSc2d1	dopravní
podniku	podnik	k1gInSc2	podnik
<g/>
.	.	kIx.	.
</s>
<s>
Rittig	Rittig	k1gMnSc1	Rittig
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
tomu	ten	k3xDgNnSc3	ten
ostře	ostro	k6eAd1	ostro
ohradil	ohradit	k5eAaPmAgMnS	ohradit
a	a	k8xC	a
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
šokován	šokovat	k5eAaBmNgMnS	šokovat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
bezostyšnému	bezostyšný	k2eAgNnSc3d1	bezostyšné
osočení	osočení	k1gNnSc3	osočení
byl	být	k5eAaImAgInS	být
vystaven	vystavit	k5eAaPmNgInS	vystavit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
Neograph	Neographa	k1gFnPc2	Neographa
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
pro	pro	k7c4	pro
dopravní	dopravní	k2eAgInSc4d1	dopravní
podnik	podnik	k1gInSc4	podnik
dodávala	dodávat	k5eAaImAgFnS	dodávat
jízdenky	jízdenka	k1gFnPc1	jízdenka
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
uzavřenou	uzavřený	k2eAgFnSc4d1	uzavřená
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
firmou	firma	k1gFnSc7	firma
Cokeville	Cokevill	k1gMnSc2	Cokevill
Assets	Assetsa	k1gFnPc2	Assetsa
Inc	Inc	k1gMnSc2	Inc
<g/>
.	.	kIx.	.
sídlící	sídlící	k2eAgFnSc1d1	sídlící
na	na	k7c6	na
Britských	britský	k2eAgInPc6d1	britský
Panenských	panenský	k2eAgInPc6d1	panenský
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
zprostředkování	zprostředkování	k1gNnSc4	zprostředkování
smlouvy	smlouva	k1gFnSc2	smlouva
měla	mít	k5eAaImAgFnS	mít
tato	tento	k3xDgFnSc1	tento
společnost	společnost	k1gFnSc1	společnost
dostávat	dostávat	k5eAaImF	dostávat
z	z	k7c2	z
každého	každý	k3xTgInSc2	každý
lístku	lístek	k1gInSc2	lístek
17	[number]	k4	17
haléřů	haléř	k1gInPc2	haléř
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
Randák	Randák	k1gMnSc1	Randák
z	z	k7c2	z
protikorupčního	protikorupční	k2eAgInSc2d1	protikorupční
fondu	fond	k1gInSc2	fond
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
firma	firma	k1gFnSc1	firma
Cokeville	Cokeville	k1gFnSc2	Cokeville
má	mít	k5eAaImIp3nS	mít
smlouvu	smlouva	k1gFnSc4	smlouva
o	o	k7c6	o
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Rittigem	Rittig	k1gInSc7	Rittig
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejímž	jejíž	k3xOyRp3gInSc6	jejíž
základě	základ	k1gInSc6	základ
má	mít	k5eAaImIp3nS	mít
firma	firma	k1gFnSc1	firma
Rittigovi	Rittigův	k2eAgMnPc1d1	Rittigův
vyplácet	vyplácet	k5eAaImF	vyplácet
provize	provize	k1gFnPc4	provize
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
160	[number]	k4	160
tisíc	tisíc	k4xCgInPc2	tisíc
eur	euro	k1gNnPc2	euro
měsíčně	měsíčně	k6eAd1	měsíčně
a	a	k8xC	a
procenta	procento	k1gNnPc1	procento
ze	z	k7c2	z
zisku	zisk	k1gInSc2	zisk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2011	[number]	k4	2011
upozornil	upozornit	k5eAaPmAgMnS	upozornit
na	na	k7c4	na
firmu	firma	k1gFnSc4	firma
Oleo	Oleo	k6eAd1	Oleo
Chemical	Chemical	k1gFnSc2	Chemical
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
dodávala	dodávat	k5eAaImAgFnS	dodávat
bionaftu	bionafta	k1gFnSc4	bionafta
do	do	k7c2	do
pražského	pražský	k2eAgInSc2d1	pražský
dopravního	dopravní	k2eAgInSc2d1	dopravní
podniku	podnik	k1gInSc2	podnik
a	a	k8xC	a
také	také	k6eAd1	také
spolupracovala	spolupracovat	k5eAaImAgFnS	spolupracovat
s	s	k7c7	s
firmou	firma	k1gFnSc7	firma
Cokeville	Cokeville	k1gFnSc2	Cokeville
Assets	Assetsa	k1gFnPc2	Assetsa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2014	[number]	k4	2014
pak	pak	k9	pak
Útvar	útvar	k1gInSc1	útvar
pro	pro	k7c4	pro
odhalování	odhalování	k1gNnSc4	odhalování
organizovaného	organizovaný	k2eAgInSc2d1	organizovaný
zločinu	zločin	k1gInSc2	zločin
zadržel	zadržet	k5eAaPmAgMnS	zadržet
některé	některý	k3yIgMnPc4	některý
pracovníky	pracovník	k1gMnPc4	pracovník
firmy	firma	k1gFnSc2	firma
Oleo	Oleo	k6eAd1	Oleo
Chemival	Chemival	k1gMnPc2	Chemival
a	a	k8xC	a
advokáty	advokát	k1gMnPc7	advokát
kanceláře	kancelář	k1gFnSc2	kancelář
MSB	MSB	kA	MSB
Legal	Legal	k1gInSc1	Legal
<g/>
.	.	kIx.	.
</s>
<s>
Policie	policie	k1gFnSc1	policie
obvinila	obvinit	k5eAaPmAgFnS	obvinit
10	[number]	k4	10
lidí	člověk	k1gMnPc2	člověk
z	z	k7c2	z
několik	několik	k4yIc4	několik
trestných	trestný	k2eAgInPc2d1	trestný
činů	čin	k1gInPc2	čin
včetně	včetně	k7c2	včetně
krácení	krácení	k1gNnSc2	krácení
daně	daň	k1gFnSc2	daň
a	a	k8xC	a
porušení	porušení	k1gNnSc2	porušení
povinnosti	povinnost	k1gFnSc2	povinnost
při	při	k7c6	při
správě	správa	k1gFnSc6	správa
cizího	cizí	k2eAgInSc2d1	cizí
majetku	majetek	k1gInSc2	majetek
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
pracovali	pracovat	k5eAaImAgMnP	pracovat
jako	jako	k9	jako
organizovaná	organizovaný	k2eAgFnSc1d1	organizovaná
skupina	skupina	k1gFnSc1	skupina
a	a	k8xC	a
vyváděli	vyvádět	k5eAaImAgMnP	vyvádět
peníze	peníz	k1gInPc4	peníz
z	z	k7c2	z
firmy	firma	k1gFnSc2	firma
Oleo	Oleo	k1gMnSc1	Oleo
Chemical	Chemical	k1gMnSc1	Chemical
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
fondu	fond	k1gInSc2	fond
nechala	nechat	k5eAaPmAgFnS	nechat
Nemocnice	nemocnice	k1gFnSc1	nemocnice
Na	na	k7c6	na
Homolce	homolka	k1gFnSc6	homolka
za	za	k7c4	za
působení	působení	k1gNnSc4	působení
bývalého	bývalý	k2eAgMnSc2d1	bývalý
ředitele	ředitel	k1gMnSc2	ředitel
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Dbalého	dbalý	k2eAgNnSc2d1	dbalé
vydělat	vydělat	k5eAaPmF	vydělat
na	na	k7c6	na
pronájmu	pronájem	k1gInSc6	pronájem
vlastních	vlastní	k2eAgInPc2d1	vlastní
skladů	sklad	k1gInPc2	sklad
soukromé	soukromý	k2eAgFnPc1d1	soukromá
společnosti	společnost	k1gFnPc1	společnost
napojené	napojený	k2eAgFnPc4d1	napojená
na	na	k7c6	na
AK	AK	kA	AK
Šachta	šachta	k1gFnSc1	šachta
and	and	k?	and
Partners	Partners	k1gInSc1	Partners
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
na	na	k7c4	na
Iva	Ivo	k1gMnSc4	Ivo
Rittiga	Rittig	k1gMnSc4	Rittig
<g/>
.	.	kIx.	.
</s>
<s>
Fond	fond	k1gInSc1	fond
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemocnice	nemocnice	k1gFnSc1	nemocnice
tratila	tratit	k5eAaImAgFnS	tratit
na	na	k7c4	na
přenechání	přenechání	k1gNnSc4	přenechání
provozování	provozování	k1gNnSc2	provozování
skladů	sklad	k1gInPc2	sklad
kolem	kolem	k7c2	kolem
8	[number]	k4	8
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
měsíčně	měsíčně	k6eAd1	měsíčně
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
společnosti	společnost	k1gFnSc2	společnost
ALSEDA	ALSEDA	kA	ALSEDA
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
ročně	ročně	k6eAd1	ročně
kolem	kolem	k7c2	kolem
100	[number]	k4	100
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
NFPK	NFPK	kA	NFPK
podal	podat	k5eAaPmAgInS	podat
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2012	[number]	k4	2012
trestní	trestní	k2eAgNnSc4d1	trestní
oznámení	oznámení	k1gNnSc4	oznámení
na	na	k7c4	na
tehdejšího	tehdejší	k2eAgMnSc4d1	tehdejší
ministra	ministr	k1gMnSc4	ministr
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
sociálních	sociální	k2eAgFnPc2d1	sociální
věcí	věc	k1gFnPc2	věc
Jaromíra	Jaromír	k1gMnSc4	Jaromír
Drábka	Drábek	k1gMnSc4	Drábek
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc1	jeho
tehdejšího	tehdejší	k2eAgMnSc4d1	tehdejší
prvního	první	k4xOgMnSc4	první
náměstka	náměstek	k1gMnSc4	náměstek
Vladimíra	Vladimír	k1gMnSc4	Vladimír
Šišku	Šiška	k1gMnSc4	Šiška
a	a	k8xC	a
na	na	k7c4	na
neznámé	známý	k2eNgMnPc4d1	neznámý
pachatele	pachatel	k1gMnPc4	pachatel
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
bylo	být	k5eAaImAgNnS	být
podezření	podezření	k1gNnSc1	podezření
ze	z	k7c2	z
spáchání	spáchání	k1gNnSc2	spáchání
trestného	trestný	k2eAgInSc2d1	trestný
činu	čin	k1gInSc2	čin
porušení	porušení	k1gNnSc2	porušení
povinnosti	povinnost	k1gFnSc2	povinnost
při	při	k7c6	při
správě	správa	k1gFnSc6	správa
cizího	cizí	k2eAgInSc2d1	cizí
majetku	majetek	k1gInSc2	majetek
<g/>
,	,	kIx,	,
zneužití	zneužití	k1gNnSc4	zneužití
informace	informace	k1gFnSc2	informace
a	a	k8xC	a
postavení	postavení	k1gNnSc2	postavení
v	v	k7c6	v
obchodním	obchodní	k2eAgInSc6d1	obchodní
styku	styk	k1gInSc6	styk
<g/>
,	,	kIx,	,
přijetí	přijetí	k1gNnSc4	přijetí
úplatku	úplatek	k1gInSc2	úplatek
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Těchto	tento	k3xDgInPc2	tento
trestných	trestný	k2eAgInPc2d1	trestný
činů	čin	k1gInPc2	čin
se	se	k3xPyFc4	se
dotyční	dotyčný	k2eAgMnPc1d1	dotyčný
mohli	moct	k5eAaImAgMnP	moct
dopustit	dopustit	k5eAaPmF	dopustit
při	při	k7c6	při
zavádění	zavádění	k1gNnSc6	zavádění
nových	nový	k2eAgInPc2d1	nový
informačních	informační	k2eAgInPc2d1	informační
systémů	systém	k1gInPc2	systém
pro	pro	k7c4	pro
výplaty	výplata	k1gFnPc4	výplata
dávek	dávka	k1gFnPc2	dávka
pomoci	pomoct	k5eAaPmF	pomoct
v	v	k7c6	v
hmotné	hmotný	k2eAgFnSc6d1	hmotná
nouzi	nouze	k1gFnSc6	nouze
<g/>
,	,	kIx,	,
příspěvku	příspěvek	k1gInSc2	příspěvek
na	na	k7c4	na
péči	péče	k1gFnSc4	péče
a	a	k8xC	a
dávek	dávka	k1gFnPc2	dávka
pro	pro	k7c4	pro
tělesně	tělesně	k6eAd1	tělesně
postižené	postižený	k2eAgMnPc4d1	postižený
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2012	[number]	k4	2012
Jaromír	Jaromír	k1gMnSc1	Jaromír
Drábek	Drábek	k1gMnSc1	Drábek
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
post	post	k1gInSc4	post
a	a	k8xC	a
následně	následně	k6eAd1	následně
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2013	[number]	k4	2013
byli	být	k5eAaImAgMnP	být
obžalováni	obžalovat	k5eAaPmNgMnP	obžalovat
z	z	k7c2	z
podplácení	podplácení	k1gNnSc2	podplácení
a	a	k8xC	a
zneužití	zneužití	k1gNnSc2	zneužití
pravomoci	pravomoc	k1gFnSc2	pravomoc
úřední	úřední	k2eAgFnSc2d1	úřední
osoby	osoba	k1gFnSc2	osoba
Vladimír	Vladimír	k1gMnSc1	Vladimír
Šiška	Šiška	k1gMnSc1	Šiška
a	a	k8xC	a
bývalý	bývalý	k2eAgMnSc1d1	bývalý
ředitel	ředitel	k1gMnSc1	ředitel
odboru	odbor	k1gInSc2	odbor
IT	IT	kA	IT
tohoto	tento	k3xDgNnSc2	tento
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
Milan	Milan	k1gMnSc1	Milan
Hojer	Hojer	k1gMnSc1	Hojer
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2014	[number]	k4	2014
Úřad	úřad	k1gInSc1	úřad
pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
soutěže	soutěž	k1gFnSc2	soutěž
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
pravomocně	pravomocně	k6eAd1	pravomocně
zakázal	zakázat	k5eAaPmAgMnS	zakázat
společnosti	společnost	k1gFnSc3	společnost
Fujitsu	Fujitsa	k1gFnSc4	Fujitsa
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
plnění	plnění	k1gNnSc6	plnění
smlouvy	smlouva	k1gFnSc2	smlouva
na	na	k7c4	na
provoz	provoz	k1gInSc4	provoz
a	a	k8xC	a
správu	správa	k1gFnSc4	správa
informačních	informační	k2eAgInPc2d1	informační
systémů	systém	k1gInPc2	systém
na	na	k7c4	na
výplatu	výplata	k1gFnSc4	výplata
nepojistných	pojistný	k2eNgFnPc2d1	pojistný
sociálních	sociální	k2eAgFnPc2d1	sociální
dávek	dávka	k1gFnPc2	dávka
a	a	k8xC	a
služeb	služba	k1gFnPc2	služba
zaměstnanosti	zaměstnanost	k1gFnSc2	zaměstnanost
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
téměř	téměř	k6eAd1	téměř
dvou	dva	k4xCgNnPc6	dva
letech	léto	k1gNnPc6	léto
krajně	krajně	k6eAd1	krajně
problematického	problematický	k2eAgNnSc2d1	problematické
(	(	kIx(	(
<g/>
ne	ne	k9	ne
<g/>
)	)	kIx)	)
<g/>
fungování	fungování	k1gNnSc2	fungování
nového	nový	k2eAgInSc2d1	nový
IS	IS	kA	IS
na	na	k7c6	na
úřadech	úřad	k1gInPc6	úřad
práce	práce	k1gFnSc2	práce
se	se	k3xPyFc4	se
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
sociálních	sociální	k2eAgFnPc2d1	sociální
věcí	věc	k1gFnPc2	věc
vrací	vracet	k5eAaImIp3nS	vracet
k	k	k7c3	k
IS	IS	kA	IS
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
bezproblémově	bezproblémově	k6eAd1	bezproblémově
fungoval	fungovat	k5eAaImAgInS	fungovat
mnoho	mnoho	k4c4	mnoho
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Nadační	nadační	k2eAgInSc1d1	nadační
fond	fond	k1gInSc1	fond
proti	proti	k7c3	proti
korupci	korupce	k1gFnSc3	korupce
podal	podat	k5eAaPmAgMnS	podat
dne	den	k1gInSc2	den
21	[number]	k4	21
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
2013	[number]	k4	2013
trestní	trestní	k2eAgFnSc2d1	trestní
oznámení	oznámení	k1gNnSc4	oznámení
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
Hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
komory	komora	k1gFnSc2	komora
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
dále	daleko	k6eAd2	daleko
jen	jen	k9	jen
"	"	kIx"	"
<g/>
HK	HK	kA	HK
ČR	ČR	kA	ČR
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
Petra	Petr	k1gMnSc2	Petr
Kužela	Kužel	k1gMnSc2	Kužel
a	a	k8xC	a
na	na	k7c4	na
další	další	k2eAgMnPc4d1	další
neznámé	známý	k2eNgMnPc4d1	neznámý
pachatele	pachatel	k1gMnPc4	pachatel
<g/>
.	.	kIx.	.
</s>
<s>
Předmětem	předmět	k1gInSc7	předmět
devadesátistránkového	devadesátistránkový	k2eAgNnSc2d1	devadesátistránkový
trestního	trestní	k2eAgNnSc2d1	trestní
oznámení	oznámení	k1gNnSc2	oznámení
je	být	k5eAaImIp3nS	být
podezření	podezření	k1gNnSc4	podezření
ze	z	k7c2	z
spáchání	spáchání	k1gNnSc2	spáchání
trestných	trestný	k2eAgInPc2d1	trestný
činů	čin	k1gInPc2	čin
zpronevěry	zpronevěra	k1gFnSc2	zpronevěra
<g/>
,	,	kIx,	,
porušení	porušení	k1gNnSc1	porušení
povinností	povinnost	k1gFnPc2	povinnost
při	při	k7c6	při
správě	správa	k1gFnSc6	správa
cizího	cizí	k2eAgInSc2d1	cizí
majetku	majetek	k1gInSc2	majetek
<g/>
,	,	kIx,	,
sjednání	sjednání	k1gNnSc2	sjednání
výhody	výhoda	k1gFnSc2	výhoda
při	při	k7c6	při
zadávání	zadávání	k1gNnSc6	zadávání
veřejné	veřejný	k2eAgFnSc2d1	veřejná
zakázky	zakázka	k1gFnSc2	zakázka
<g/>
,	,	kIx,	,
pletichy	pleticha	k1gFnPc4	pleticha
při	při	k7c6	při
zadávání	zadávání	k1gNnSc6	zadávání
veřejné	veřejný	k2eAgFnSc2d1	veřejná
zakázky	zakázka	k1gFnSc2	zakázka
a	a	k8xC	a
jiných	jiný	k2eAgInPc2d1	jiný
trestných	trestný	k2eAgInPc2d1	trestný
činů	čin	k1gInPc2	čin
<g/>
.	.	kIx.	.
</s>
<s>
Městský	městský	k2eAgInSc1d1	městský
soud	soud	k1gInSc1	soud
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
jako	jako	k9	jako
příslušný	příslušný	k2eAgInSc4d1	příslušný
rejstříkový	rejstříkový	k2eAgInSc4d1	rejstříkový
soud	soud	k1gInSc4	soud
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2011	[number]	k4	2011
v	v	k7c6	v
usnesení	usnesení	k1gNnSc6	usnesení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
vydala	vydat	k5eAaPmAgFnS	vydat
vyšší	vysoký	k2eAgFnSc1d2	vyšší
soudní	soudní	k2eAgFnSc1d1	soudní
úřednice	úřednice	k1gFnSc1	úřednice
Romana	Romana	k1gFnSc1	Romana
Janečková	Janečková	k1gFnSc1	Janečková
<g/>
,	,	kIx,	,
odmítl	odmítnout	k5eAaPmAgInS	odmítnout
Nadační	nadační	k2eAgInSc1d1	nadační
fond	fond	k1gInSc1	fond
proti	proti	k7c3	proti
korupci	korupce	k1gFnSc3	korupce
zaregistrovat	zaregistrovat	k5eAaPmF	zaregistrovat
s	s	k7c7	s
odůvodněním	odůvodnění	k1gNnSc7	odůvodnění
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
předmětného	předmětný	k2eAgInSc2d1	předmětný
hlavního	hlavní	k2eAgInSc2d1	hlavní
účelu	účel	k1gInSc2	účel
nadačního	nadační	k2eAgInSc2d1	nadační
fondu	fond	k1gInSc2	fond
"	"	kIx"	"
<g/>
zcela	zcela	k6eAd1	zcela
jasně	jasně	k6eAd1	jasně
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
že	že	k8xS	že
uvedený	uvedený	k2eAgInSc1d1	uvedený
účel	účel	k1gInSc1	účel
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
účelem	účel	k1gInSc7	účel
nadačních	nadační	k2eAgInPc2d1	nadační
fondů	fond	k1gInPc2	fond
dle	dle	k7c2	dle
zákona	zákon	k1gInSc2	zákon
a	a	k8xC	a
že	že	k8xS	že
účel	účel	k1gInSc4	účel
zřízeného	zřízený	k2eAgInSc2d1	zřízený
fondu	fond	k1gInSc2	fond
svým	svůj	k3xOyFgInSc7	svůj
obsahem	obsah	k1gInSc7	obsah
nenaplňuje	naplňovat	k5eNaImIp3nS	naplňovat
podstatu	podstata	k1gFnSc4	podstata
obecně	obecně	k6eAd1	obecně
prospěšného	prospěšný	k2eAgInSc2d1	prospěšný
cíle	cíl	k1gInSc2	cíl
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
nadacích	nadace	k1gFnPc6	nadace
a	a	k8xC	a
nadačních	nadační	k2eAgInPc6d1	nadační
fondech	fond	k1gInPc6	fond
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
zakladatelé	zakladatel	k1gMnPc1	zakladatel
chtěli	chtít	k5eAaImAgMnP	chtít
odvolání	odvolání	k1gNnSc4	odvolání
vzdát	vzdát	k5eAaPmF	vzdát
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
od	od	k7c2	od
odborníka	odborník	k1gMnSc2	odborník
dozvěděli	dozvědět	k5eAaPmAgMnP	dozvědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
takové	takový	k3xDgNnSc1	takový
odvolací	odvolací	k2eAgNnSc1d1	odvolací
řízení	řízení	k1gNnSc1	řízení
trvá	trvat	k5eAaImIp3nS	trvat
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
až	až	k9	až
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
a	a	k8xC	a
chtěli	chtít	k5eAaImAgMnP	chtít
jen	jen	k6eAd1	jen
podat	podat	k5eAaPmF	podat
novou	nový	k2eAgFnSc4d1	nová
žádost	žádost	k1gFnSc4	žádost
s	s	k7c7	s
upraveným	upravený	k2eAgNnSc7d1	upravené
zněním	znění	k1gNnSc7	znění
zakládací	zakládací	k2eAgFnSc2d1	zakládací
listiny	listina	k1gFnSc2	listina
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
podali	podat	k5eAaPmAgMnP	podat
jak	jak	k8xC	jak
odvolání	odvolání	k1gNnSc4	odvolání
k	k	k7c3	k
Vrchnímu	vrchní	k2eAgInSc3d1	vrchní
soudu	soud	k1gInSc3	soud
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
stížnost	stížnost	k1gFnSc1	stížnost
proti	proti	k7c3	proti
postupu	postup	k1gInSc3	postup
úřednice	úřednice	k1gFnSc2	úřednice
k	k	k7c3	k
předsedovi	předseda	k1gMnSc3	předseda
Městského	městský	k2eAgInSc2d1	městský
soudu	soud	k1gInSc2	soud
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
Janu	Jana	k1gFnSc4	Jana
Sváčkovi	Sváček	k1gMnSc3	Sváček
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
opakovanou	opakovaný	k2eAgFnSc4d1	opakovaná
<g/>
,	,	kIx,	,
mírně	mírně	k6eAd1	mírně
pozměněnou	pozměněný	k2eAgFnSc4d1	pozměněná
žádost	žádost	k1gFnSc4	žádost
o	o	k7c6	o
registraci	registrace	k1gFnSc6	registrace
(	(	kIx(	(
<g/>
název	název	k1gInSc1	název
byl	být	k5eAaImAgInS	být
změněn	změnit	k5eAaPmNgInS	změnit
na	na	k7c4	na
Nadační	nadační	k2eAgInSc4d1	nadační
fond	fond	k1gInSc4	fond
pro	pro	k7c4	pro
boj	boj	k1gInSc4	boj
proti	proti	k7c3	proti
korupci	korupce	k1gFnSc3	korupce
<g/>
)	)	kIx)	)
o	o	k7c4	o
registraci	registrace	k1gFnSc4	registrace
u	u	k7c2	u
Městského	městský	k2eAgInSc2d1	městský
soudu	soud	k1gInSc2	soud
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Tu	tu	k6eAd1	tu
dostala	dostat	k5eAaPmAgFnS	dostat
přidělenou	přidělený	k2eAgFnSc4d1	přidělená
tatáž	týž	k3xTgFnSc1	týž
úřednice	úřednice	k1gFnSc1	úřednice
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
předchozí	předchozí	k2eAgFnSc1d1	předchozí
žádost	žádost	k1gFnSc1	žádost
zamítla	zamítnout	k5eAaPmAgFnS	zamítnout
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
podle	podle	k7c2	podle
jednacího	jednací	k2eAgNnSc2d1	jednací
čísla	číslo	k1gNnSc2	číslo
a	a	k8xC	a
rozvrhu	rozvrh	k1gInSc2	rozvrh
práce	práce	k1gFnSc2	práce
měla	mít	k5eAaImAgFnS	mít
připadnout	připadnout	k5eAaPmF	připadnout
někomu	někdo	k3yInSc3	někdo
jinému	jiný	k2eAgMnSc3d1	jiný
<g/>
,	,	kIx,	,
a	a	k8xC	a
tentokrát	tentokrát	k6eAd1	tentokrát
žádala	žádat	k5eAaImAgFnS	žádat
22	[number]	k4	22
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2011	[number]	k4	2011
po	po	k7c6	po
dvou	dva	k4xCgInPc6	dva
členech	člen	k1gInPc6	člen
správní	správní	k2eAgFnSc2d1	správní
rady	rada	k1gFnSc2	rada
fondu	fond	k1gInSc2	fond
<g/>
,	,	kIx,	,
Karlu	Karel	k1gMnSc3	Karel
Janečkovi	Janeček	k1gMnSc3	Janeček
a	a	k8xC	a
Tomáši	Tomáš	k1gMnSc3	Tomáš
Sedláčkovi	Sedláček	k1gMnSc3	Sedláček
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
do	do	k7c2	do
15	[number]	k4	15
dnů	den	k1gInPc2	den
od	od	k7c2	od
doručení	doručení	k1gNnSc2	doručení
doložili	doložit	k5eAaPmAgMnP	doložit
oprávněnost	oprávněnost	k1gFnSc4	oprávněnost
užívání	užívání	k1gNnSc2	užívání
titulů	titul	k1gInPc2	titul
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
a	a	k8xC	a
PhDr.	PhDr.	kA	PhDr.
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
oba	dva	k4xCgMnPc1	dva
pánové	pán	k1gMnPc1	pán
s	s	k7c7	s
těmito	tento	k3xDgInPc7	tento
tituly	titul	k1gInPc7	titul
figurují	figurovat	k5eAaImIp3nP	figurovat
již	již	k6eAd1	již
v	v	k7c6	v
několika	několik	k4yIc6	několik
rejstřících	rejstřík	k1gInPc6	rejstřík
včetně	včetně	k7c2	včetně
záznamů	záznam	k1gInPc2	záznam
o	o	k7c6	o
jiných	jiný	k2eAgInPc6d1	jiný
nadačních	nadační	k2eAgInPc6d1	nadační
fondech	fond	k1gInPc6	fond
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
Městský	městský	k2eAgInSc1d1	městský
soud	soud	k1gInSc1	soud
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
v	v	k7c4	v
pátek	pátek	k1gInSc4	pátek
11	[number]	k4	11
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2011	[number]	k4	2011
usnesl	usnést	k5eAaPmAgInS	usnést
<g/>
,	,	kIx,	,
že	že	k8xS	že
fond	fond	k1gInSc1	fond
zapíše	zapsat	k5eAaPmIp3nS	zapsat
<g/>
.	.	kIx.	.
</s>
<s>
Janečkova	Janečkův	k2eAgFnSc1d1	Janečkova
právní	právní	k2eAgFnSc1d1	právní
zástupkyně	zástupkyně	k1gFnSc1	zástupkyně
Lenka	Lenka	k1gFnSc1	Lenka
Deverová	Deverová	k1gFnSc1	Deverová
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
podílela	podílet	k5eAaImAgFnS	podílet
na	na	k7c6	na
přípravě	příprava	k1gFnSc6	příprava
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
nadacích	nadace	k1gFnPc6	nadace
a	a	k8xC	a
nadačních	nadační	k2eAgInPc6d1	nadační
fondech	fond	k1gInPc6	fond
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
spoluautorkou	spoluautorka	k1gFnSc7	spoluautorka
návrhu	návrh	k1gInSc2	návrh
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
statusu	status	k1gInSc6	status
veřejné	veřejný	k2eAgFnSc2d1	veřejná
prospěšnosti	prospěšnost	k1gFnSc2	prospěšnost
<g/>
,	,	kIx,	,
zastává	zastávat	k5eAaImIp3nS	zastávat
názor	názor	k1gInSc1	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c4	za
veřejně	veřejně	k6eAd1	veřejně
prospěšné	prospěšný	k2eAgFnPc4d1	prospěšná
činnosti	činnost	k1gFnPc4	činnost
se	se	k3xPyFc4	se
nepovažují	považovat	k5eNaImIp3nP	považovat
jen	jen	k9	jen
ty	ten	k3xDgFnPc1	ten
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
zákoně	zákon	k1gInSc6	zákon
výslovně	výslovně	k6eAd1	výslovně
vyjmenované	vyjmenovaný	k2eAgInPc4d1	vyjmenovaný
v	v	k7c6	v
demonstrativním	demonstrativní	k2eAgInSc6d1	demonstrativní
výčtu	výčet	k1gInSc6	výčet
<g/>
,	,	kIx,	,
a	a	k8xC	a
zamítnutí	zamítnutí	k1gNnSc1	zamítnutí
žádosti	žádost	k1gFnSc2	žádost
o	o	k7c4	o
registraci	registrace	k1gFnSc4	registrace
z	z	k7c2	z
uvedeného	uvedený	k2eAgInSc2d1	uvedený
důvodu	důvod	k1gInSc2	důvod
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
absurdní	absurdní	k2eAgNnSc4d1	absurdní
<g/>
.	.	kIx.	.
</s>
