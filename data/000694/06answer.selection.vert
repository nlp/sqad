<s>
Nadační	nadační	k2eAgInSc1d1	nadační
fond	fond	k1gInSc1	fond
pro	pro	k7c4	pro
boj	boj	k1gInSc4	boj
proti	proti	k7c3	proti
korupci	korupce	k1gFnSc3	korupce
(	(	kIx(	(
<g/>
NFPK	NFPK	kA	NFPK
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
původní	původní	k2eAgFnSc2d1	původní
<g/>
,	,	kIx,	,
zamítnuté	zamítnutý	k2eAgFnSc2d1	zamítnutá
žádosti	žádost	k1gFnSc2	žádost
o	o	k7c4	o
registraci	registrace	k1gFnSc4	registrace
Nadační	nadační	k2eAgInSc1d1	nadační
fond	fond	k1gInSc1	fond
proti	proti	k7c3	proti
korupci	korupce	k1gFnSc3	korupce
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
nestátní	státní	k2eNgFnSc1d1	nestátní
nadace	nadace	k1gFnSc1	nadace
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
15	[number]	k4	15
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2011	[number]	k4	2011
zřídili	zřídit	k5eAaPmAgMnP	zřídit
Karel	Karel	k1gMnSc1	Karel
Janeček	Janeček	k1gMnSc1	Janeček
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
Bernard	Bernard	k1gMnSc1	Bernard
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
Kraus	Kraus	k1gMnSc1	Kraus
<g/>
.	.	kIx.	.
</s>
