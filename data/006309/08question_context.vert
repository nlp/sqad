<s>
Waffen-SS	Waffen-SS	k?	Waffen-SS
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
:	:	kIx,	:
Zbraně	zbraň	k1gFnSc2	zbraň
SS	SS	kA	SS
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
Ozbrojené	ozbrojený	k2eAgInPc1d1	ozbrojený
SS	SS	kA	SS
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
jednotky	jednotka	k1gFnPc1	jednotka
SS	SS	kA	SS
<g/>
,	,	kIx,	,
určené	určený	k2eAgInPc4d1	určený
k	k	k7c3	k
běžným	běžný	k2eAgFnPc3d1	běžná
vojenským	vojenský	k2eAgFnPc3d1	vojenská
operacím	operace	k1gFnPc3	operace
<g/>
.	.	kIx.	.
</s>
<s>
Představovaly	představovat	k5eAaImAgFnP	představovat
německé	německý	k2eAgFnPc1d1	německá
zvláštní	zvláštní	k2eAgFnPc1d1	zvláštní
a	a	k8xC	a
cizinecké	cizinecký	k2eAgFnPc1d1	cizinecká
jednotky	jednotka	k1gFnPc1	jednotka
nacistické	nacistický	k2eAgFnSc2d1	nacistická
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
na	na	k7c6	na
konci	konec	k1gInSc6	konec
války	válka	k1gFnSc2	válka
zahrnovaly	zahrnovat	k5eAaImAgInP	zahrnovat
přibližně	přibližně	k6eAd1	přibližně
950	[number]	k4	950
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
dobrovolníků	dobrovolník	k1gMnPc2	dobrovolník
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
vedeny	vést	k5eAaImNgInP	vést
Heinrichem	Heinrich	k1gMnSc7	Heinrich
Himmlerem	Himmler	k1gMnSc7	Himmler
<g/>
.	.	kIx.	.
</s>
