<s>
Leden	leden	k1gInSc1	leden
podle	podle	k7c2	podle
gregoriánského	gregoriánský	k2eAgInSc2d1	gregoriánský
kalendáře	kalendář	k1gInSc2	kalendář
první	první	k4xOgInSc4	první
měsíc	měsíc	k1gInSc4	měsíc
v	v	k7c6	v
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
31	[number]	k4	31
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
název	název	k1gInSc1	název
měsíce	měsíc	k1gInSc2	měsíc
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
slova	slovo	k1gNnSc2	slovo
led	led	k1gInSc4	led
-	-	kIx~	-
měsíc	měsíc	k1gInSc4	měsíc
ledu	led	k1gInSc2	led
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc4	tento
měsíc	měsíc	k1gInSc4	měsíc
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Evropě	Evropa	k1gFnSc6	Evropa
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k9	jako
měsíc	měsíc	k1gInSc4	měsíc
vlka	vlk	k1gMnSc2	vlk
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
byl	být	k5eAaImAgInS	být
leden	leden	k1gInSc1	leden
v	v	k7c6	v
římském	římský	k2eAgInSc6d1	římský
kalendáři	kalendář	k1gInSc6	kalendář
až	až	k9	až
11	[number]	k4	11
<g/>
.	.	kIx.	.
měsíc	měsíc	k1gInSc4	měsíc
v	v	k7c6	v
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
153	[number]	k4	153
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
začátku	začátek	k1gInSc2	začátek
roku	rok	k1gInSc2	rok
z	z	k7c2	z
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
leden	leden	k1gInSc1	leden
<g/>
.	.	kIx.	.
</s>
<s>
Leden	leden	k1gInSc1	leden
začíná	začínat	k5eAaImIp3nS	začínat
vždy	vždy	k6eAd1	vždy
stejným	stejný	k2eAgInSc7d1	stejný
dnem	den	k1gInSc7	den
v	v	k7c6	v
týdnu	týden	k1gInSc6	týden
jako	jako	k8xS	jako
květen	květen	k1gInSc4	květen
předcházejícího	předcházející	k2eAgInSc2d1	předcházející
roku	rok	k1gInSc2	rok
a	a	k8xC	a
stejným	stejný	k2eAgInSc7d1	stejný
dnem	den	k1gInSc7	den
jako	jako	k8xS	jako
říjen	říjen	k1gInSc4	říjen
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
přestupného	přestupný	k2eAgInSc2d1	přestupný
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
připadá	připadat	k5eAaPmIp3nS	připadat
první	první	k4xOgInSc4	první
lednový	lednový	k2eAgInSc4d1	lednový
den	den	k1gInSc4	den
na	na	k7c4	na
stejný	stejný	k2eAgInSc4d1	stejný
den	den	k1gInSc4	den
v	v	k7c6	v
týdnu	týden	k1gInSc6	týden
jako	jako	k8xC	jako
první	první	k4xOgInSc4	první
den	den	k1gInSc4	den
dubna	duben	k1gInSc2	duben
a	a	k8xC	a
července	červenec	k1gInSc2	červenec
<g/>
.	.	kIx.	.
</s>
<s>
Astrologicky	astrologicky	k6eAd1	astrologicky
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
ledna	leden	k1gInSc2	leden
Slunce	slunce	k1gNnSc2	slunce
ve	v	k7c6	v
znamení	znamení	k1gNnSc6	znamení
Kozoroha	Kozoroh	k1gMnSc2	Kozoroh
a	a	k8xC	a
na	na	k7c6	na
konci	konec	k1gInSc6	konec
ve	v	k7c6	v
znamení	znamení	k1gNnSc6	znamení
Vodnáře	vodnář	k1gMnSc2	vodnář
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
astronomických	astronomický	k2eAgInPc6d1	astronomický
termínech	termín	k1gInPc6	termín
začíná	začínat	k5eAaImIp3nS	začínat
v	v	k7c6	v
souhvězdí	souhvězdí	k1gNnSc6	souhvězdí
Střelce	Střelec	k1gMnSc4	Střelec
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
v	v	k7c6	v
souhvězdí	souhvězdí	k1gNnSc6	souhvězdí
Kozoroha	Kozoroh	k1gMnSc2	Kozoroh
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yQnSc1	co
leden	leden	k1gInSc4	leden
sněhem	sníh	k1gInSc7	sníh
popráší	poprášit	k5eAaPmIp3nS	poprášit
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
únor	únor	k1gInSc1	únor
s	s	k7c7	s
vichrem	vichr	k1gInSc7	vichr
odnáší	odnášet	k5eAaImIp3nS	odnášet
<g/>
.	.	kIx.	.
</s>
<s>
Hojný	hojný	k2eAgInSc1d1	hojný
sníh	sníh	k1gInSc1	sníh
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
<g/>
,	,	kIx,	,
málo	málo	k6eAd1	málo
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
<g/>
.	.	kIx.	.
</s>
<s>
Holomrazy	holomráz	k1gInPc1	holomráz
-	-	kIx~	-
úrody	úroda	k1gFnPc1	úroda
vrazi	vrah	k1gMnPc1	vrah
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
vysoko	vysoko	k6eAd1	vysoko
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
leží	ležet	k5eAaImIp3nS	ležet
sníh	sníh	k1gInSc1	sníh
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
vysoko	vysoko	k6eAd1	vysoko
tráva	tráva	k1gFnSc1	tráva
poroste	růst	k5eAaImIp3nS	růst
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
leden	leden	k1gInSc1	leden
nejostřejší	ostrý	k2eAgInSc1d3	nejostřejší
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
roček	roček	k1gInSc1	roček
nejplodnější	plodný	k2eAgInSc1d3	nejplodnější
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
je	být	k5eAaImIp3nS	být
leden	leden	k1gInSc1	leden
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
sedlákovi	sedlák	k1gMnSc3	sedlák
milý	milý	k2eAgMnSc5d1	milý
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
není	být	k5eNaImIp3nS	být
konec	konec	k1gInSc1	konec
ledna	leden	k1gInSc2	leden
studený	studený	k2eAgInSc1d1	studený
<g/>
,	,	kIx,	,
únor	únor	k1gInSc1	únor
to	ten	k3xDgNnSc4	ten
dvakrát	dvakrát	k6eAd1	dvakrát
nahradí	nahradit	k5eAaPmIp3nS	nahradit
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
roste	růst	k5eAaImIp3nS	růst
tráva	tráva	k1gFnSc1	tráva
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
<g/>
,	,	kIx,	,
roste	růst	k5eAaImIp3nS	růst
špatně	špatně	k6eAd1	špatně
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
hrom	hrom	k1gInSc1	hrom
se	se	k3xPyFc4	se
ozve	ozvat	k5eAaPmIp3nS	ozvat
<g/>
,	,	kIx,	,
hojnost	hojnost	k1gFnSc1	hojnost
vína	víno	k1gNnSc2	víno
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
očekávání	očekávání	k1gNnSc6	očekávání
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
včely	včela	k1gFnSc2	včela
vyletují	vyletovat	k5eAaImIp3nP	vyletovat
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc4	ten
nedobrý	dobrý	k2eNgInSc4d1	nedobrý
rok	rok	k1gInSc4	rok
ohlašují	ohlašovat	k5eAaImIp3nP	ohlašovat
<g/>
.	.	kIx.	.
</s>
<s>
Leden	leden	k1gInSc1	leden
jasný	jasný	k2eAgInSc1d1	jasný
<g/>
,	,	kIx,	,
roček	roček	k1gInSc1	roček
krásný	krásný	k2eAgInSc1d1	krásný
<g/>
.	.	kIx.	.
</s>
<s>
Leden	leden	k1gInSc1	leden
studený	studený	k2eAgInSc1d1	studený
<g/>
,	,	kIx,	,
duben	duben	k1gInSc1	duben
zelený	zelený	k2eAgInSc1d1	zelený
<g/>
.	.	kIx.	.
</s>
<s>
Lednová	lednový	k2eAgFnSc1d1	lednová
zima	zima	k1gFnSc1	zima
i	i	k9	i
na	na	k7c6	na
peci	pec	k1gFnSc6	pec
je	být	k5eAaImIp3nS	být
znát	znát	k5eAaImF	znát
<g/>
.	.	kIx.	.
</s>
<s>
Lednový	lednový	k2eAgInSc1d1	lednový
déšť	déšť	k1gInSc1	déšť
z	z	k7c2	z
pecnu	pecen	k1gInSc2	pecen
odkrajuje	odkrajovat	k5eAaImIp3nS	odkrajovat
<g/>
.	.	kIx.	.
</s>
<s>
Lenivého	lenivý	k2eAgNnSc2d1	lenivé
tahá	tahat	k5eAaImIp3nS	tahat
mráz	mráz	k1gInSc1	mráz
za	za	k7c4	za
nos	nos	k1gInSc4	nos
<g/>
,	,	kIx,	,
před	před	k7c7	před
pilným	pilný	k2eAgInSc7d1	pilný
smeká	smekat	k5eAaImIp3nS	smekat
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
sněhu	sníh	k1gInSc2	sníh
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
<g/>
,	,	kIx,	,
mnoho	mnoho	k4c4	mnoho
hřibů	hřib	k1gInPc2	hřib
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Boží	boží	k2eAgNnSc4d1	boží
narození	narození	k1gNnSc4	narození
o	o	k7c4	o
bleší	bleší	k2eAgNnSc4d1	bleší
převalení	převalení	k1gNnSc4	převalení
<g/>
,	,	kIx,	,
na	na	k7c4	na
Nový	nový	k2eAgInSc4d1	nový
rok	rok	k1gInSc4	rok
o	o	k7c4	o
slepičí	slepičí	k2eAgInSc4d1	slepičí
krok	krok	k1gInSc4	krok
<g/>
,	,	kIx,	,
na	na	k7c4	na
Tři	tři	k4xCgMnPc4	tři
krále	král	k1gMnPc4	král
o	o	k7c4	o
tři	tři	k4xCgMnPc4	tři
dále	daleko	k6eAd2	daleko
(	(	kIx(	(
<g/>
délka	délka	k1gFnSc1	délka
dne	den	k1gInSc2	den
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Suchý	suchý	k2eAgInSc1d1	suchý
leden	leden	k1gInSc1	leden
<g/>
,	,	kIx,	,
mokrý	mokrý	k2eAgInSc1d1	mokrý
červen	červen	k1gInSc1	červen
<g/>
.	.	kIx.	.
</s>
<s>
Teplý	teplý	k2eAgInSc4d1	teplý
leden	leden	k1gInSc4	leden
<g/>
,	,	kIx,	,
k	k	k7c3	k
nouzi	nouze	k1gFnSc3	nouze
krok	krok	k1gInSc1	krok
jen	jen	k6eAd1	jen
jeden	jeden	k4xCgMnSc1	jeden
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
málo	málo	k1gNnSc4	málo
vody	voda	k1gFnSc2	voda
-	-	kIx~	-
mnoho	mnoho	k6eAd1	mnoho
vína	víno	k1gNnSc2	víno
<g/>
,	,	kIx,	,
mnoho	mnoho	k6eAd1	mnoho
vody	voda	k1gFnSc2	voda
-	-	kIx~	-
málo	málo	k1gNnSc1	málo
vína	víno	k1gNnSc2	víno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
mráz	mráz	k1gInSc1	mráz
-	-	kIx~	-
těší	těšit	k5eAaImIp3nS	těšit
nás	my	k3xPp1nPc2	my
<g/>
;	;	kIx,	;
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
voda	voda	k1gFnSc1	voda
-	-	kIx~	-
věčná	věčný	k2eAgFnSc1d1	věčná
škoda	škoda	k1gFnSc1	škoda
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
leden	leden	k1gInSc1	leden
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
leden	leden	k1gInSc1	leden
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
