<p>
<s>
Dareios	Dareios	k1gMnSc1	Dareios
I.	I.	kA	I.
(	(	kIx(	(
<g/>
staropersky	staropersky	k6eAd1	staropersky
Dárajavauš	Dárajavauš	k1gInSc1	Dárajavauš
[	[	kIx(	[
]	]	kIx)	]
<g/>
;	;	kIx,	;
kolem	kolem	k7c2	kolem
550	[number]	k4	550
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
–	–	k?	–
listopad	listopad	k1gInSc1	listopad
486	[number]	k4	486
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
perský	perský	k2eAgInSc1d1	perský
velkokrál	velkokrál	k1gInSc1	velkokrál
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Achaimenovců	Achaimenovec	k1gMnPc2	Achaimenovec
vládnoucí	vládnoucí	k2eAgFnSc4d1	vládnoucí
v	v	k7c6	v
letech	let	k1gInPc6	let
522	[number]	k4	522
<g/>
–	–	k?	–
<g/>
486	[number]	k4	486
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
</s>
<s>
Pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
mladší	mladý	k2eAgFnSc2d2	mladší
<g/>
,	,	kIx,	,
Ariaramnovy	Ariaramnův	k2eAgFnSc2d1	Ariaramnův
větve	větev	k1gFnSc2	větev
rodu	rod	k1gInSc2	rod
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
od	od	k7c2	od
hlavní	hlavní	k2eAgFnSc2d1	hlavní
linie	linie	k1gFnSc2	linie
oddělila	oddělit	k5eAaPmAgFnS	oddělit
po	po	k7c6	po
polovině	polovina	k1gFnSc6	polovina
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
otcem	otec	k1gMnSc7	otec
byl	být	k5eAaImAgInS	být
Hystaspés	Hystaspés	k1gInSc1	Hystaspés
(	(	kIx(	(
<g/>
Vištáspa	Vištáspa	k1gFnSc1	Vištáspa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dědem	děd	k1gMnSc7	děd
Arsamés	Arsamésa	k1gFnPc2	Arsamésa
(	(	kIx(	(
<g/>
Aršáma	Aršáma	k1gFnSc1	Aršáma
<g/>
)	)	kIx)	)
a	a	k8xC	a
praprapradědem	praprapraděd	k1gInSc7	praprapraděd
Achaimenés	Achaimenés	k1gInSc1	Achaimenés
(	(	kIx(	(
<g/>
Hachámaniš	Hachámaniš	k1gInSc1	Hachámaniš
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
dynastie	dynastie	k1gFnSc2	dynastie
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
dějin	dějiny	k1gFnPc2	dějiny
se	se	k3xPyFc4	se
Dareios	Dareios	k1gInSc1	Dareios
zapsal	zapsat	k5eAaPmAgInS	zapsat
hlavně	hlavně	k9	hlavně
jako	jako	k9	jako
skvělý	skvělý	k2eAgMnSc1d1	skvělý
organizátor	organizátor	k1gMnSc1	organizátor
a	a	k8xC	a
stavebník	stavebník	k1gMnSc1	stavebník
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
už	už	k6eAd1	už
jako	jako	k8xC	jako
zdatný	zdatný	k2eAgMnSc1d1	zdatný
vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
ani	ani	k8xC	ani
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
ohledu	ohled	k1gInSc6	ohled
nebyl	být	k5eNaImAgInS	být
zdaleka	zdaleka	k6eAd1	zdaleka
neúspěšný	úspěšný	k2eNgInSc1d1	neúspěšný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mládí	mládí	k1gNnSc1	mládí
a	a	k8xC	a
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Dareios	Dareios	k1gMnSc1	Dareios
obdržel	obdržet	k5eAaPmAgMnS	obdržet
jako	jako	k8xS	jako
všichni	všechen	k3xTgMnPc1	všechen
příslušníci	příslušník	k1gMnPc1	příslušník
perské	perský	k2eAgFnSc2d1	perská
nobility	nobilita	k1gFnSc2	nobilita
odpovídající	odpovídající	k2eAgNnSc4d1	odpovídající
vzdělání	vzdělání	k1gNnSc4	vzdělání
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
kariéra	kariéra	k1gFnSc1	kariéra
před	před	k7c7	před
nástupem	nástup	k1gInSc7	nástup
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
není	být	k5eNaImIp3nS	být
příliš	příliš	k6eAd1	příliš
známa	znám	k2eAgFnSc1d1	známa
<g/>
,	,	kIx,	,
ví	vědět	k5eAaImIp3nS	vědět
se	se	k3xPyFc4	se
jen	jen	k9	jen
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
král	král	k1gMnSc1	král
Kambýsés	Kambýsés	k1gInSc4	Kambýsés
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
svým	svůj	k3xOyFgMnSc7	svůj
kopiníkem	kopiník	k1gMnSc7	kopiník
a	a	k8xC	a
že	že	k8xS	že
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
funkci	funkce	k1gFnSc6	funkce
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
výpravy	výprava	k1gFnSc2	výprava
do	do	k7c2	do
Egypta	Egypt	k1gInSc2	Egypt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
525	[number]	k4	525
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
</s>
</p>
<p>
<s>
==	==	k?	==
Převzetí	převzetí	k1gNnPc4	převzetí
vlády	vláda	k1gFnSc2	vláda
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
se	se	k3xPyFc4	se
Dareios	Dareios	k1gInSc1	Dareios
patrně	patrně	k6eAd1	patrně
pohyboval	pohybovat	k5eAaImAgInS	pohybovat
stále	stále	k6eAd1	stále
v	v	k7c6	v
Kambýsově	Kambýsův	k2eAgFnSc6d1	Kambýsův
blízkosti	blízkost	k1gFnSc6	blízkost
a	a	k8xC	a
tam	tam	k6eAd1	tam
ho	on	k3xPp3gMnSc4	on
také	také	k9	také
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
ostatní	ostatní	k2eAgMnPc4d1	ostatní
dvořany	dvořan	k1gMnPc4	dvořan
zastihla	zastihnout	k5eAaPmAgFnS	zastihnout
zpráva	zpráva	k1gFnSc1	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
Persidě	Persida	k1gFnSc6	Persida
pozdvihl	pozdvihnout	k5eAaPmAgMnS	pozdvihnout
proti	proti	k7c3	proti
králi	král	k1gMnSc3	král
jeho	jeho	k3xOp3gInPc4	jeho
bratr	bratr	k1gMnSc1	bratr
Smerdis	Smerdis	k1gFnSc2	Smerdis
<g/>
,	,	kIx,	,
někdejší	někdejší	k2eAgMnSc1d1	někdejší
místodržitel	místodržitel	k1gMnSc1	místodržitel
na	na	k7c6	na
Východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
</s>
<s>
Výprava	výprava	k1gFnSc1	výprava
musela	muset	k5eAaImAgFnS	muset
být	být	k5eAaImF	být
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
522	[number]	k4	522
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
přerušena	přerušit	k5eAaPmNgFnS	přerušit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
veškerá	veškerý	k3xTgFnSc1	veškerý
vojenská	vojenský	k2eAgFnSc1d1	vojenská
moc	moc	k1gFnSc1	moc
obrátila	obrátit	k5eAaPmAgFnS	obrátit
proti	proti	k7c3	proti
uzurpátorovi	uzurpátor	k1gMnSc3	uzurpátor
<g/>
,	,	kIx,	,
o	o	k7c6	o
jehož	jehož	k3xOyRp3gFnPc6	jehož
aktivitách	aktivita	k1gFnPc6	aktivita
se	se	k3xPyFc4	se
nevědělo	vědět	k5eNaImAgNnS	vědět
nic	nic	k3yNnSc1	nic
bližšího	blízký	k2eAgNnSc2d2	bližší
<g/>
.	.	kIx.	.
</s>
<s>
Cestou	cesta	k1gFnSc7	cesta
na	na	k7c4	na
východ	východ	k1gInSc4	východ
však	však	k9	však
Kambýsés	Kambýsés	k1gInSc4	Kambýsés
nešťastnou	šťastný	k2eNgFnSc7d1	nešťastná
náhodou	náhoda	k1gFnSc7	náhoda
zahynul	zahynout	k5eAaPmAgMnS	zahynout
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
zanechal	zanechat	k5eAaPmAgMnS	zanechat
legitimního	legitimní	k2eAgMnSc4d1	legitimní
dědice	dědic	k1gMnSc4	dědic
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
Hérodotova	Hérodotův	k2eAgNnSc2d1	Hérodotovo
podání	podání	k1gNnSc2	podání
i	i	k8xC	i
perských	perský	k2eAgInPc2d1	perský
pramenů	pramen	k1gInPc2	pramen
nebyl	být	k5eNaImAgInS	být
vzbouřencem	vzbouřenec	k1gMnSc7	vzbouřenec
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
Smerdis	Smerdis	k1gFnSc2	Smerdis
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
dal	dát	k5eAaPmAgInS	dát
Kambýsés	Kambýsés	k1gInSc4	Kambýsés
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
tajně	tajně	k6eAd1	tajně
zavraždit	zavraždit	k5eAaPmF	zavraždit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jistý	jistý	k2eAgMnSc1d1	jistý
mág	mág	k1gMnSc1	mág
jménem	jméno	k1gNnSc7	jméno
Gaumáta	Gaumáta	k1gFnSc1	Gaumáta
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
byl	být	k5eAaImAgInS	být
prý	prý	k9	prý
pravému	pravý	k2eAgMnSc3d1	pravý
Smerdiovi	Smerdius	k1gMnSc3	Smerdius
mimořádně	mimořádně	k6eAd1	mimořádně
podobný	podobný	k2eAgInSc1d1	podobný
a	a	k8xC	a
nahrávaly	nahrávat	k5eAaImAgInP	nahrávat
mu	on	k3xPp3gMnSc3	on
i	i	k9	i
uvolněné	uvolněný	k2eAgInPc4d1	uvolněný
poměry	poměr	k1gInPc4	poměr
v	v	k7c6	v
jádru	jádro	k1gNnSc6	jádro
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
vyvolané	vyvolaný	k2eAgNnSc1d1	vyvolané
královou	králův	k2eAgFnSc7d1	králova
dlouholetou	dlouholetý	k2eAgFnSc7d1	dlouholetá
nepřítomností	nepřítomnost	k1gFnSc7	nepřítomnost
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Dareios	Dareios	k1gMnSc1	Dareios
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
jinými	jiný	k1gMnPc7	jiný
urychleně	urychleně	k6eAd1	urychleně
spěchal	spěchat	k5eAaImAgMnS	spěchat
do	do	k7c2	do
vlasti	vlast	k1gFnSc2	vlast
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nyní	nyní	k6eAd1	nyní
za	za	k7c2	za
nepříliš	příliš	k6eNd1	příliš
jasných	jasný	k2eAgFnPc2d1	jasná
okolností	okolnost	k1gFnPc2	okolnost
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
proti	proti	k7c3	proti
Smerdiovi	Smerdius	k1gMnSc3	Smerdius
<g/>
/	/	kIx~	/
<g/>
Gaumátovi	Gaumát	k1gMnSc3	Gaumát
vystoupí	vystoupit	k5eAaPmIp3nS	vystoupit
<g/>
.	.	kIx.	.
</s>
<s>
Pomáhalo	pomáhat	k5eAaImAgNnS	pomáhat
mu	on	k3xPp3gMnSc3	on
přitom	přitom	k6eAd1	přitom
šest	šest	k4xCc4	šest
jeho	jeho	k3xOp3gInPc2	jeho
druhů	druh	k1gInPc2	druh
–	–	k?	–
Otanés	Otanés	k1gInSc1	Otanés
(	(	kIx(	(
<g/>
Utána	Utána	k1gFnSc1	Utána
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Góbryás	Góbryás	k1gInSc1	Góbryás
(	(	kIx(	(
<g/>
Gaubaruva	Gaubaruva	k1gFnSc1	Gaubaruva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Intafernés	Intafernés	k1gInSc1	Intafernés
(	(	kIx(	(
<g/>
Vindafarná	Vindafarný	k2eAgFnSc1d1	Vindafarný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Megabyxos	Megabyxos	k1gMnSc1	Megabyxos
(	(	kIx(	(
<g/>
Bagabuchša	Bagabuchša	k1gMnSc1	Bagabuchša
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Hydarnés	Hydarnés	k1gInSc1	Hydarnés
(	(	kIx(	(
<g/>
Vidarna	Vidarna	k1gFnSc1	Vidarna
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ardumanis	Ardumanis	k1gFnSc1	Ardumanis
(	(	kIx(	(
<g/>
Ardumaniš	Ardumaniš	k1gInSc1	Ardumaniš
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
29	[number]	k4	29
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
522	[number]	k4	522
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
pronikli	proniknout	k5eAaPmAgMnP	proniknout
všichni	všechen	k3xTgMnPc1	všechen
do	do	k7c2	do
pevnosti	pevnost	k1gFnSc2	pevnost
Sikajauvatiš	Sikajauvatiš	k1gInSc1	Sikajauvatiš
v	v	k7c6	v
Médii	Médie	k1gFnSc6	Médie
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
uchýlil	uchýlit	k5eAaPmAgInS	uchýlit
dvůr	dvůr	k1gInSc1	dvůr
<g/>
,	,	kIx,	,
přemohli	přemoct	k5eAaPmAgMnP	přemoct
stráže	stráž	k1gFnSc2	stráž
a	a	k8xC	a
mága	mág	k1gMnSc4	mág
zavraždili	zavraždit	k5eAaPmAgMnP	zavraždit
<g/>
.	.	kIx.	.
</s>
<s>
Dareios	Dareios	k1gMnSc1	Dareios
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
novým	nový	k2eAgMnSc7d1	nový
velkokrálem	velkokrál	k1gMnSc7	velkokrál
a	a	k8xC	a
přeskočil	přeskočit	k5eAaPmAgInS	přeskočit
tak	tak	k6eAd1	tak
vlastně	vlastně	k9	vlastně
v	v	k7c6	v
následnickém	následnický	k2eAgNnSc6d1	následnické
pořadí	pořadí	k1gNnSc6	pořadí
svého	své	k1gNnSc2	své
otce	otec	k1gMnSc2	otec
i	i	k8xC	i
děda	děd	k1gMnSc2	děd
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
době	doba	k1gFnSc6	doba
převratu	převrat	k1gInSc2	převrat
ještě	ještě	k6eAd1	ještě
naživu	naživu	k6eAd1	naživu
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc1	svůj
popis	popis	k1gInSc1	popis
událostí	událost	k1gFnSc7	událost
vtělil	vtělit	k5eAaPmAgInS	vtělit
Dareios	Dareios	k1gInSc1	Dareios
do	do	k7c2	do
několika	několik	k4yIc2	několik
monumentálních	monumentální	k2eAgInPc2d1	monumentální
nápisů	nápis	k1gInPc2	nápis
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgNnPc2	jenž
nejznámější	známý	k2eAgMnSc1d3	nejznámější
se	se	k3xPyFc4	se
dochoval	dochovat	k5eAaPmAgMnS	dochovat
v	v	k7c6	v
Behistunu	Behistun	k1gInSc6	Behistun
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
psán	psán	k2eAgInSc1d1	psán
persky	persky	k6eAd1	persky
<g/>
,	,	kIx,	,
akkadsky	akkadsky	k6eAd1	akkadsky
a	a	k8xC	a
elamsky	elamsky	k6eAd1	elamsky
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Řeckou	řecký	k2eAgFnSc4d1	řecká
verzi	verze	k1gFnSc4	verze
podávají	podávat	k5eAaImIp3nP	podávat
především	především	k9	především
Hérodotos	Hérodotos	k1gMnSc1	Hérodotos
a	a	k8xC	a
Ktésiás	Ktésiás	k1gInSc1	Ktésiás
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potlačení	potlačení	k1gNnSc1	potlačení
odboje	odboj	k1gInSc2	odboj
==	==	k?	==
</s>
</p>
<p>
<s>
Úspěšným	úspěšný	k2eAgInSc7d1	úspěšný
převratem	převrat	k1gInSc7	převrat
neměl	mít	k5eNaImAgInS	mít
Dareios	Dareios	k1gInSc1	Dareios
I.	I.	kA	I.
ještě	ještě	k9	ještě
vyhráno	vyhrát	k5eAaPmNgNnS	vyhrát
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
nyní	nyní	k6eAd1	nyní
vzplály	vzplát	k5eAaPmAgFnP	vzplát
četné	četný	k2eAgFnPc1d1	četná
revolty	revolta	k1gFnPc1	revolta
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
odpadl	odpadnout	k5eAaPmAgInS	odpadnout
Elam	Elam	k1gInSc1	Elam
a	a	k8xC	a
Babylón	Babylón	k1gInSc1	Babylón
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
zmocnili	zmocnit	k5eAaPmAgMnP	zmocnit
vlády	vláda	k1gFnSc2	vláda
muži	muž	k1gMnSc6	muž
jménem	jméno	k1gNnSc7	jméno
Açina	Açino	k1gNnSc2	Açino
a	a	k8xC	a
Nidintubel	Nidintubela	k1gFnPc2	Nidintubela
<g/>
,	,	kIx,	,
další	další	k2eAgInPc1d1	další
nepokoje	nepokoj	k1gInPc1	nepokoj
měly	mít	k5eAaImAgInP	mít
následovat	následovat	k5eAaImF	následovat
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
nejnebezpečnější	bezpečný	k2eNgFnSc2d3	nejnebezpečnější
pokládal	pokládat	k5eAaImAgMnS	pokládat
král	král	k1gMnSc1	král
patrně	patrně	k6eAd1	patrně
odboj	odboj	k1gInSc4	odboj
v	v	k7c6	v
Babylóně	Babylón	k1gInSc6	Babylón
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
vypravil	vypravit	k5eAaPmAgInS	vypravit
osobně	osobně	k6eAd1	osobně
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
vojsko	vojsko	k1gNnSc1	vojsko
bez	bez	k7c2	bez
potíží	potíž	k1gFnPc2	potíž
překročilo	překročit	k5eAaPmAgNnS	překročit
Tigris	Tigris	k1gInSc4	Tigris
<g/>
,	,	kIx,	,
porazilo	porazit	k5eAaPmAgNnS	porazit
Nidintubela	Nidintubela	k1gFnSc1	Nidintubela
u	u	k7c2	u
Zazána	Zazán	k1gMnSc2	Zazán
při	při	k7c6	při
Eufratu	Eufrat	k1gInSc6	Eufrat
a	a	k8xC	a
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
522	[number]	k4	522
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
opanovalo	opanovat	k5eAaPmAgNnS	opanovat
Babylón	Babylón	k1gInSc4	Babylón
<g/>
.	.	kIx.	.
</s>
<s>
Açinovu	Açinův	k2eAgFnSc4d1	Açinův
vzpouru	vzpoura	k1gFnSc4	vzpoura
zatím	zatím	k6eAd1	zatím
zlikvidovali	zlikvidovat	k5eAaPmAgMnP	zlikvidovat
Dareiovi	Dareiův	k2eAgMnPc1d1	Dareiův
velitelé	velitel	k1gMnPc1	velitel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Elamu	Elam	k1gInSc6	Elam
se	se	k3xPyFc4	se
však	však	k9	však
situace	situace	k1gFnSc1	situace
ani	ani	k8xC	ani
poté	poté	k6eAd1	poté
neuklidnila	uklidnit	k5eNaPmAgFnS	uklidnit
<g/>
.	.	kIx.	.
</s>
<s>
Jakýsi	jakýsi	k3yIgMnSc1	jakýsi
Martija	Martija	k1gMnSc1	Martija
zde	zde	k6eAd1	zde
usiloval	usilovat	k5eAaImAgMnS	usilovat
o	o	k7c4	o
vládu	vláda	k1gFnSc4	vláda
a	a	k8xC	a
teprve	teprve	k6eAd1	teprve
Dareiovy	Dareiův	k2eAgFnPc4d1	Dareiova
hrozby	hrozba	k1gFnPc4	hrozba
Elamity	Elamita	k1gMnSc2	Elamita
přiměly	přimět	k5eAaPmAgFnP	přimět
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Martiju	Martiju	k1gFnSc4	Martiju
ještě	ještě	k9	ještě
před	před	k7c7	před
příchodem	příchod	k1gInSc7	příchod
královského	královský	k2eAgNnSc2d1	královské
vojska	vojsko	k1gNnSc2	vojsko
zabili	zabít	k5eAaPmAgMnP	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Mnohem	mnohem	k6eAd1	mnohem
vážněji	vážně	k6eAd2	vážně
se	se	k3xPyFc4	se
vyvíjela	vyvíjet	k5eAaImAgFnS	vyvíjet
vzpoura	vzpoura	k1gFnSc1	vzpoura
v	v	k7c6	v
Médii	Médie	k1gFnSc6	Médie
<g/>
,	,	kIx,	,
vedená	vedený	k2eAgFnSc1d1	vedená
Fravartišem	Fravartiš	k1gInSc7	Fravartiš
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
k	k	k7c3	k
ní	on	k3xPp3gFnSc6	on
připojila	připojit	k5eAaPmAgFnS	připojit
většina	většina	k1gFnSc1	většina
médských	médský	k2eAgFnPc2d1	Médská
jednotek	jednotka	k1gFnPc2	jednotka
ve	v	k7c6	v
vojsku	vojsko	k1gNnSc3	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Dareios	Dareios	k1gMnSc1	Dareios
neměl	mít	k5eNaImAgMnS	mít
na	na	k7c4	na
vybranou	vybraná	k1gFnSc4	vybraná
–	–	k?	–
musel	muset	k5eAaImAgMnS	muset
Fravartiše	Fravartiš	k1gInPc4	Fravartiš
stůj	stát	k5eAaImRp2nS	stát
co	co	k8xS	co
stůj	stát	k5eAaImRp2nS	stát
porazit	porazit	k5eAaPmF	porazit
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
by	by	kYmCp3nS	by
ztratil	ztratit	k5eAaPmAgMnS	ztratit
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
centrálními	centrální	k2eAgFnPc7d1	centrální
oblastmi	oblast	k1gFnPc7	oblast
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Naštěstí	naštěstí	k6eAd1	naštěstí
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
skončila	skončit	k5eAaPmAgFnS	skončit
bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Kunduru	Kundur	k1gInSc2	Kundur
Fravartišovým	Fravartišův	k2eAgInSc7d1	Fravartišův
útěkem	útěk	k1gInSc7	útěk
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
uhaslo	uhasnout	k5eAaPmAgNnS	uhasnout
další	další	k2eAgNnSc4d1	další
ohnisko	ohnisko	k1gNnSc4	ohnisko
odporu	odpor	k1gInSc2	odpor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
521	[number]	k4	521
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
zasahoval	zasahovat	k5eAaImAgMnS	zasahovat
Dareios	Dareios	k1gInSc4	Dareios
ještě	ještě	k9	ještě
v	v	k7c6	v
Arménii	Arménie	k1gFnSc6	Arménie
<g/>
,	,	kIx,	,
Sagartii	Sagartie	k1gFnSc6	Sagartie
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k9	i
ve	v	k7c6	v
vlastní	vlastní	k2eAgFnSc6d1	vlastní
Persidě	Persida	k1gFnSc6	Persida
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
revoltoval	revoltovat	k5eAaImAgMnS	revoltovat
jistý	jistý	k2eAgMnSc1d1	jistý
Vahjazdáta	Vahjazdát	k1gMnSc4	Vahjazdát
<g/>
,	,	kIx,	,
další	další	k2eAgInSc4d1	další
falešný	falešný	k2eAgInSc4d1	falešný
Smerdis	Smerdis	k1gInSc4	Smerdis
<g/>
.	.	kIx.	.
</s>
<s>
Všechna	všechen	k3xTgFnSc1	všechen
tato	tento	k3xDgNnPc4	tento
pozdvižení	pozdvižení	k1gNnPc4	pozdvižení
však	však	k9	však
skončila	skončit	k5eAaPmAgFnS	skončit
vítězstvím	vítězství	k1gNnSc7	vítězství
královských	královský	k2eAgFnPc2d1	královská
jednotek	jednotka	k1gFnPc2	jednotka
a	a	k8xC	a
popravami	poprava	k1gFnPc7	poprava
odbojníků	odbojník	k1gMnPc2	odbojník
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Dareios	Dareios	k1gMnSc1	Dareios
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
roku	rok	k1gInSc2	rok
potlačil	potlačit	k5eAaPmAgInS	potlačit
i	i	k9	i
novou	nový	k2eAgFnSc4d1	nová
vzpouru	vzpoura	k1gFnSc4	vzpoura
v	v	k7c6	v
Babylóně	Babylón	k1gInSc6	Babylón
zorganizovanou	zorganizovaný	k2eAgFnSc4d1	zorganizovaná
Arménem	Armén	k1gMnSc7	Armén
Arachou	Aracha	k1gMnSc7	Aracha
<g/>
,	,	kIx,	,
rozhostil	rozhostit	k5eAaPmAgMnS	rozhostit
se	se	k3xPyFc4	se
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
konečně	konečně	k6eAd1	konečně
klid	klid	k1gInSc4	klid
<g/>
.	.	kIx.	.
</s>
<s>
Dareios	Dareios	k1gMnSc1	Dareios
prosadil	prosadit	k5eAaPmAgMnS	prosadit
svou	svůj	k3xOyFgFnSc4	svůj
autoritu	autorita	k1gFnSc4	autorita
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc4	jenž
kdysi	kdysi	k6eAd1	kdysi
spravoval	spravovat	k5eAaImAgInS	spravovat
Kambýsés	Kambýsés	k1gInSc1	Kambýsés
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dobyvačná	dobyvačný	k2eAgNnPc1d1	dobyvačné
tažení	tažení	k1gNnPc1	tažení
==	==	k?	==
</s>
</p>
<p>
<s>
Achaimenovská	Achaimenovský	k2eAgFnSc1d1	Achaimenovský
monarchie	monarchie	k1gFnSc1	monarchie
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
Dareios	Dareios	k1gMnSc1	Dareios
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
521	[number]	k4	521
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
ovládl	ovládnout	k5eAaPmAgInS	ovládnout
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
rozlehlým	rozlehlý	k2eAgInSc7d1	rozlehlý
státním	státní	k2eAgInSc7d1	státní
útvarem	útvar	k1gInSc7	útvar
<g/>
,	,	kIx,	,
vůbec	vůbec	k9	vůbec
největším	veliký	k2eAgMnSc7d3	veliký
<g/>
,	,	kIx,	,
jaký	jaký	k9	jaký
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
na	na	k7c6	na
Předním	přední	k2eAgInSc6d1	přední
východě	východ	k1gInSc6	východ
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
<g/>
.	.	kIx.	.
</s>
<s>
Perské	perský	k2eAgFnPc1d1	perská
državy	država	k1gFnPc1	država
se	se	k3xPyFc4	se
táhly	táhnout	k5eAaImAgFnP	táhnout
od	od	k7c2	od
Egypta	Egypt	k1gInSc2	Egypt
a	a	k8xC	a
Malé	Malé	k2eAgFnSc2d1	Malé
Asie	Asie	k1gFnSc2	Asie
na	na	k7c6	na
Západě	západ	k1gInSc6	západ
až	až	k9	až
k	k	k7c3	k
hranicím	hranice	k1gFnPc3	hranice
dnešního	dnešní	k2eAgInSc2d1	dnešní
Afghánistánu	Afghánistán	k1gInSc2	Afghánistán
a	a	k8xC	a
od	od	k7c2	od
toku	tok	k1gInSc2	tok
Amudarji	Amudarj	k1gMnSc3	Amudarj
až	až	k9	až
po	po	k7c4	po
Perský	perský	k2eAgInSc4d1	perský
záliv	záliv	k1gInSc4	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dnešního	dnešní	k2eAgInSc2d1	dnešní
pohledu	pohled	k1gInSc2	pohled
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mohlo	moct	k5eAaImAgNnS	moct
zdát	zdát	k5eAaPmF	zdát
kontraproduktivní	kontraproduktivní	k2eAgNnSc1d1	kontraproduktivní
už	už	k6eAd1	už
tak	tak	k9	tak
těžko	těžko	k6eAd1	těžko
ovladatelný	ovladatelný	k2eAgInSc1d1	ovladatelný
stát	stát	k1gInSc1	stát
dále	daleko	k6eAd2	daleko
rozšiřovat	rozšiřovat	k5eAaImF	rozšiřovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Starověké	starověký	k2eAgFnSc2d1	starověká
říše	říš	k1gFnSc2	říš
však	však	k9	však
ke	k	k7c3	k
svému	své	k1gNnSc3	své
"	"	kIx"	"
<g/>
životu	život	k1gInSc2	život
<g/>
"	"	kIx"	"
potřebovaly	potřebovat	k5eAaImAgInP	potřebovat
expanzi	expanze	k1gFnSc4	expanze
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jen	jen	k9	jen
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
dal	dát	k5eAaPmAgMnS	dát
udržovat	udržovat	k5eAaImF	udržovat
respekt	respekt	k1gInSc4	respekt
vůči	vůči	k7c3	vůči
královské	královský	k2eAgFnSc3d1	královská
moci	moc	k1gFnSc3	moc
<g/>
,	,	kIx,	,
poslušnost	poslušnost	k1gFnSc1	poslušnost
vazalů	vazal	k1gMnPc2	vazal
a	a	k8xC	a
koneckonců	koneckonců	k9	koneckonců
i	i	k9	i
"	"	kIx"	"
<g/>
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
stabilita	stabilita	k1gFnSc1	stabilita
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Výboje	výboj	k1gInSc2	výboj
znamenaly	znamenat	k5eAaImAgFnP	znamenat
příliv	příliv	k1gInSc4	příliv
otroků	otrok	k1gMnPc2	otrok
z	z	k7c2	z
dobytých	dobytý	k2eAgNnPc2d1	dobyté
území	území	k1gNnPc2	území
<g/>
,	,	kIx,	,
rozšíření	rozšíření	k1gNnSc1	rozšíření
obchodních	obchodní	k2eAgInPc2d1	obchodní
kontaktů	kontakt	k1gInPc2	kontakt
a	a	k8xC	a
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
i	i	k8xC	i
odměny	odměna	k1gFnPc4	odměna
pro	pro	k7c4	pro
vojsko	vojsko	k1gNnSc4	vojsko
a	a	k8xC	a
dvořany	dvořan	k1gMnPc4	dvořan
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgNnPc7	jenž
si	se	k3xPyFc3	se
král	král	k1gMnSc1	král
vykupoval	vykupovat	k5eAaImAgMnS	vykupovat
jejich	jejich	k3xOp3gFnSc4	jejich
loajalitu	loajalita	k1gFnSc4	loajalita
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
tedy	tedy	k9	tedy
příliš	příliš	k6eAd1	příliš
překvapivé	překvapivý	k2eAgNnSc1d1	překvapivé
<g/>
,	,	kIx,	,
že	že	k8xS	že
jakmile	jakmile	k8xS	jakmile
to	ten	k3xDgNnSc4	ten
poměry	poměra	k1gFnPc1	poměra
dovolily	dovolit	k5eAaPmAgFnP	dovolit
<g/>
,	,	kIx,	,
přistoupil	přistoupit	k5eAaPmAgMnS	přistoupit
k	k	k7c3	k
výbojným	výbojný	k2eAgNnPc3d1	výbojné
tažením	tažení	k1gNnPc3	tažení
i	i	k8xC	i
Dareios	Dareios	k1gInSc1	Dareios
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
politika	politika	k1gFnSc1	politika
byla	být	k5eAaImAgFnS	být
relativně	relativně	k6eAd1	relativně
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
na	na	k7c6	na
Východě	východ	k1gInSc6	východ
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
"	"	kIx"	"
<g/>
milostí	milost	k1gFnSc7	milost
Ahura	Ahur	k1gInSc2	Ahur
Mazdovou	Mazdová	k1gFnSc7	Mazdová
<g/>
"	"	kIx"	"
posunul	posunout	k5eAaPmAgMnS	posunout
perské	perský	k2eAgNnSc4d1	perské
panství	panství	k1gNnSc4	panství
až	až	k9	až
k	k	k7c3	k
hranicím	hranice	k1gFnPc3	hranice
Indie	Indie	k1gFnSc2	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
na	na	k7c6	na
Západě	západ	k1gInSc6	západ
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
jen	jen	k9	jen
dočasných	dočasný	k2eAgFnPc2d1	dočasná
nebo	nebo	k8xC	nebo
vůbec	vůbec	k9	vůbec
žádných	žádný	k3yNgInPc2	žádný
zisků	zisk	k1gInPc2	zisk
<g/>
.	.	kIx.	.
</s>
<s>
Začalo	začít	k5eAaPmAgNnS	začít
to	ten	k3xDgNnSc1	ten
již	již	k6eAd1	již
tažením	tažení	k1gNnSc7	tažení
proti	proti	k7c3	proti
nomádským	nomádský	k2eAgMnPc3d1	nomádský
Skythům	Skyth	k1gMnPc3	Skyth
<g/>
,	,	kIx,	,
etniku	etnikum	k1gNnSc3	etnikum
íránského	íránský	k2eAgInSc2d1	íránský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
obývalo	obývat	k5eAaImAgNnS	obývat
kraje	kraj	k1gInPc4	kraj
mezi	mezi	k7c7	mezi
Dunajem	Dunaj	k1gInSc7	Dunaj
a	a	k8xC	a
Donem	Don	k1gMnSc7	Don
na	na	k7c6	na
dnešní	dnešní	k2eAgFnSc6d1	dnešní
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Dareios	Dareios	k1gMnSc1	Dareios
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
nim	on	k3xPp3gMnPc3	on
vypravil	vypravit	k5eAaPmAgInS	vypravit
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
513	[number]	k4	513
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
a	a	k8xC	a
třebaže	třebaže	k8xS	třebaže
pronikl	proniknout	k5eAaPmAgInS	proniknout
hluboko	hluboko	k6eAd1	hluboko
do	do	k7c2	do
jejich	jejich	k3xOp3gNnSc2	jejich
území	území	k1gNnSc2	území
<g/>
,	,	kIx,	,
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
je	být	k5eAaImIp3nS	být
přimět	přimět	k5eAaPmF	přimět
k	k	k7c3	k
rozhodující	rozhodující	k2eAgFnSc3d1	rozhodující
bitvě	bitva	k1gFnSc3	bitva
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
raději	rád	k6eAd2	rád
odtáhl	odtáhnout	k5eAaPmAgInS	odtáhnout
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
příliš	příliš	k6eAd1	příliš
nevzdálil	vzdálit	k5eNaPmAgMnS	vzdálit
od	od	k7c2	od
svých	svůj	k3xOyFgFnPc2	svůj
základen	základna	k1gFnPc2	základna
a	a	k8xC	a
zásobovacích	zásobovací	k2eAgFnPc2d1	zásobovací
tras	trasa	k1gFnPc2	trasa
<g/>
.	.	kIx.	.
<g/>
Ještě	ještě	k9	ještě
hůře	zle	k6eAd2	zle
dopadla	dopadnout	k5eAaPmAgFnS	dopadnout
konfrontace	konfrontace	k1gFnSc1	konfrontace
s	s	k7c7	s
řeckými	řecký	k2eAgInPc7d1	řecký
městskými	městský	k2eAgInPc7d1	městský
státy	stát	k1gInPc7	stát
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
začala	začít	k5eAaPmAgFnS	začít
jako	jako	k9	jako
vnitroperská	vnitroperský	k2eAgFnSc1d1	vnitroperský
záležitost	záležitost	k1gFnSc1	záležitost
a	a	k8xC	a
skončila	skončit	k5eAaPmAgFnS	skončit
jako	jako	k9	jako
regulérní	regulérní	k2eAgFnSc1d1	regulérní
válka	válka	k1gFnSc1	válka
s	s	k7c7	s
bojovným	bojovný	k2eAgMnSc7d1	bojovný
sousedem	soused	k1gMnSc7	soused
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Prapůvod	prapůvod	k1gInSc1	prapůvod
konfliktu	konflikt	k1gInSc2	konflikt
sahá	sahat	k5eAaImIp3nS	sahat
do	do	k7c2	do
časů	čas	k1gInPc2	čas
Kýra	Kýr	k1gInSc2	Kýr
Velikého	veliký	k2eAgInSc2d1	veliký
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Peršané	Peršan	k1gMnPc1	Peršan
ovládli	ovládnout	k5eAaPmAgMnP	ovládnout
mnoho	mnoho	k4c1	mnoho
řeckých	řecký	k2eAgFnPc2d1	řecká
osad	osada	k1gFnPc2	osada
v	v	k7c6	v
Malé	Malé	k2eAgFnSc6d1	Malé
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
stojících	stojící	k2eAgFnPc6d1	stojící
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
lýdské	lýdský	k2eAgFnSc2d1	lýdský
říše	říš	k1gFnSc2	říš
krále	král	k1gMnSc2	král
Kroisa	Kroisos	k1gMnSc2	Kroisos
<g/>
.	.	kIx.	.
</s>
<s>
Achaimenovská	Achaimenovský	k2eAgFnSc1d1	Achaimenovský
správa	správa	k1gFnSc1	správa
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
vyznačovala	vyznačovat	k5eAaImAgFnS	vyznačovat
mírností	mírnost	k1gFnSc7	mírnost
a	a	k8xC	a
tolerancí	tolerance	k1gFnSc7	tolerance
<g/>
,	,	kIx,	,
řada	řada	k1gFnSc1	řada
objektivních	objektivní	k2eAgFnPc2d1	objektivní
i	i	k8xC	i
subjektivních	subjektivní	k2eAgFnPc2d1	subjektivní
příčin	příčina	k1gFnPc2	příčina
však	však	k8xC	však
maloasijské	maloasijský	k2eAgFnSc2d1	maloasijská
Řeky	řeka	k1gFnSc2	řeka
stavěla	stavět	k5eAaImAgFnS	stavět
proti	proti	k7c3	proti
novým	nový	k2eAgMnPc3d1	nový
pánům	pan	k1gMnPc3	pan
–	–	k?	–
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
500	[number]	k4	500
<g/>
/	/	kIx~	/
<g/>
499	[number]	k4	499
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
tzv.	tzv.	kA	tzv.
iónskému	iónský	k2eAgInSc3d1	iónský
povstání	povstání	k1gNnSc2	povstání
obchodních	obchodní	k2eAgFnPc2d1	obchodní
osad	osada	k1gFnPc2	osada
<g/>
.	.	kIx.	.
<g/>
Peršané	Peršan	k1gMnPc1	Peršan
odboj	odboj	k1gInSc4	odboj
potlačovali	potlačovat	k5eAaImAgMnP	potlačovat
několik	několik	k4yIc1	několik
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
kvůli	kvůli	k7c3	kvůli
němu	on	k3xPp3gNnSc3	on
vystrojili	vystrojit	k5eAaPmAgMnP	vystrojit
zbrusu	zbrusu	k6eAd1	zbrusu
nové	nový	k2eAgNnSc4d1	nové
válečné	válečný	k2eAgNnSc4d1	válečné
loďstvo	loďstvo	k1gNnSc4	loďstvo
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byl	být	k5eAaImAgInS	být
konečně	konečně	k6eAd1	konečně
obnoven	obnovit	k5eAaPmNgInS	obnovit
pořádek	pořádek	k1gInSc1	pořádek
<g/>
,	,	kIx,	,
Dareios	Dareios	k1gInSc1	Dareios
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
využít	využít	k5eAaPmF	využít
příhodné	příhodný	k2eAgFnPc4d1	příhodná
chvíle	chvíle	k1gFnPc4	chvíle
a	a	k8xC	a
podmanit	podmanit	k5eAaPmF	podmanit
si	se	k3xPyFc3	se
i	i	k9	i
dosud	dosud	k6eAd1	dosud
nezávislé	závislý	k2eNgNnSc4d1	nezávislé
evropské	evropský	k2eAgNnSc4d1	Evropské
Řecko	Řecko	k1gNnSc4	Řecko
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
samostatnost	samostatnost	k1gFnSc4	samostatnost
mohla	moct	k5eAaImAgFnS	moct
podněcovat	podněcovat	k5eAaImF	podněcovat
perské	perský	k2eAgFnSc2d1	perská
poddané	poddaná	k1gFnSc2	poddaná
k	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
revoltám	revolta	k1gFnPc3	revolta
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
záminka	záminka	k1gFnSc1	záminka
mu	on	k3xPp3gMnSc3	on
posloužil	posloužit	k5eAaPmAgInS	posloužit
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
povstání	povstání	k1gNnSc1	povstání
v	v	k7c6	v
Malé	Malé	k2eAgFnSc6d1	Malé
Asii	Asie	k1gFnSc6	Asie
podporovala	podporovat	k5eAaImAgFnS	podporovat
dvě	dva	k4xCgNnPc4	dva
města	město	k1gNnPc4	město
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
:	:	kIx,	:
Eretrie	Eretrie	k1gFnSc2	Eretrie
a	a	k8xC	a
Athény	Athéna	k1gFnSc2	Athéna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Válka	válka	k1gFnSc1	válka
začala	začít	k5eAaPmAgFnS	začít
roku	rok	k1gInSc2	rok
492	[number]	k4	492
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
Mardonios	Mardonios	k1gMnSc1	Mardonios
podmanil	podmanit	k5eAaPmAgMnS	podmanit
Thrákii	Thrákie	k1gFnSc4	Thrákie
a	a	k8xC	a
Makedonii	Makedonie	k1gFnSc4	Makedonie
–	–	k?	–
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
perského	perský	k2eAgNnSc2d1	perské
loďstva	loďstvo	k1gNnSc2	loďstvo
však	však	k9	však
během	během	k7c2	během
výpravy	výprava	k1gFnSc2	výprava
ztroskotala	ztroskotat	k5eAaPmAgFnS	ztroskotat
u	u	k7c2	u
Athosu	Athos	k1gInSc2	Athos
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
490	[number]	k4	490
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
vyrazila	vyrazit	k5eAaPmAgFnS	vyrazit
do	do	k7c2	do
boje	boj	k1gInSc2	boj
nová	nový	k2eAgFnSc1d1	nová
expedice	expedice	k1gFnSc1	expedice
<g/>
,	,	kIx,	,
vedená	vedený	k2eAgFnSc1d1	vedená
významným	významný	k2eAgMnSc7d1	významný
velmožem	velmož	k1gMnSc7	velmož
Dátidem	Dátid	k1gMnSc7	Dátid
a	a	k8xC	a
královým	králův	k2eAgMnSc7d1	králův
synovcem	synovec	k1gMnSc7	synovec
Artafernem	Artafern	k1gInSc7	Artafern
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
ní	on	k3xPp3gFnSc6	on
Peršané	Peršan	k1gMnPc1	Peršan
dobyli	dobýt	k5eAaPmAgMnP	dobýt
Eretrie	Eretrie	k1gFnPc4	Eretrie
a	a	k8xC	a
její	její	k3xOp3gNnSc4	její
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
deportovali	deportovat	k5eAaBmAgMnP	deportovat
do	do	k7c2	do
Elamu	Elam	k1gInSc2	Elam
<g/>
,	,	kIx,	,
střetnutí	střetnutí	k1gNnSc4	střetnutí
s	s	k7c7	s
Athéňany	Athéňan	k1gMnPc7	Athéňan
však	však	k8xC	však
pro	pro	k7c4	pro
ně	on	k3xPp3gInPc4	on
dopadlo	dopadnout	k5eAaPmAgNnS	dopadnout
špatně	špatně	k6eAd1	špatně
–	–	k?	–
zkušený	zkušený	k2eAgMnSc1d1	zkušený
vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
Miltiadés	Miltiadésa	k1gFnPc2	Miltiadésa
porazil	porazit	k5eAaPmAgMnS	porazit
a	a	k8xC	a
zahnal	zahnat	k5eAaPmAgMnS	zahnat
na	na	k7c4	na
útěk	útěk	k1gInSc4	útěk
jejich	jejich	k3xOp3gNnSc1	jejich
vojsko	vojsko	k1gNnSc1	vojsko
u	u	k7c2	u
Marathonu	Marathon	k1gInSc2	Marathon
(	(	kIx(	(
<g/>
září	září	k1gNnSc2	září
490	[number]	k4	490
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
bitva	bitva	k1gFnSc1	bitva
neznamenala	znamenat	k5eNaImAgFnS	znamenat
z	z	k7c2	z
vojenského	vojenský	k2eAgNnSc2d1	vojenské
hlediska	hledisko	k1gNnSc2	hledisko
žádnou	žádný	k3yNgFnSc4	žádný
katastrofu	katastrofa	k1gFnSc4	katastrofa
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
posílila	posílit	k5eAaPmAgFnS	posílit
řecké	řecký	k2eAgNnSc4d1	řecké
sebevědomí	sebevědomí	k1gNnSc4	sebevědomí
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mělo	mít	k5eAaImAgNnS	mít
mít	mít	k5eAaImF	mít
v	v	k7c6	v
budoucnu	budoucno	k1gNnSc6	budoucno
vážné	vážný	k2eAgInPc4d1	vážný
následky	následek	k1gInPc4	následek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dynastická	dynastický	k2eAgNnPc1d1	dynastické
dilemata	dilema	k1gNnPc1	dilema
==	==	k?	==
</s>
</p>
<p>
<s>
Dareios	Dareios	k1gInSc1	Dareios
již	již	k6eAd1	již
nebyl	být	k5eNaImAgInS	být
schopen	schopen	k2eAgInSc1d1	schopen
v	v	k7c6	v
evropském	evropský	k2eAgNnSc6d1	Evropské
Řecku	Řecko	k1gNnSc6	Řecko
znovu	znovu	k6eAd1	znovu
zakročit	zakročit	k5eAaPmF	zakročit
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
se	se	k3xPyFc4	se
s	s	k7c7	s
přípravami	příprava	k1gFnPc7	příprava
na	na	k7c4	na
trestnou	trestný	k2eAgFnSc4d1	trestná
expedici	expedice	k1gFnSc4	expedice
začalo	začít	k5eAaPmAgNnS	začít
téměř	téměř	k6eAd1	téměř
okamžitě	okamžitě	k6eAd1	okamžitě
<g/>
.	.	kIx.	.
</s>
<s>
Zabránily	zabránit	k5eAaPmAgFnP	zabránit
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
nepokoje	nepokoj	k1gInPc1	nepokoj
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
nemoc	nemoc	k1gFnSc4	nemoc
a	a	k8xC	a
smrt	smrt	k1gFnSc4	smrt
(	(	kIx(	(
<g/>
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
486	[number]	k4	486
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Válku	válka	k1gFnSc4	válka
s	s	k7c7	s
Řeky	Řek	k1gMnPc7	Řek
musel	muset	k5eAaImAgMnS	muset
chtě	chtě	k6eAd1	chtě
nechtě	chtě	k6eNd1	chtě
odkázat	odkázat	k5eAaPmF	odkázat
svému	svůj	k3xOyFgMnSc3	svůj
nástupci	nástupce	k1gMnSc3	nástupce
<g/>
.	.	kIx.	.
</s>
<s>
Otázkou	otázka	k1gFnSc7	otázka
ale	ale	k9	ale
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
se	se	k3xPyFc4	se
tímto	tento	k3xDgMnSc7	tento
nástupcem	nástupce	k1gMnSc7	nástupce
má	mít	k5eAaImIp3nS	mít
stát	stát	k5eAaImF	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
výběr	výběr	k1gInSc4	výběr
nejstaršího	starý	k2eAgMnSc2d3	nejstarší
syna	syn	k1gMnSc2	syn
Artobarzana	Artobarzana	k1gFnSc1	Artobarzana
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
matkou	matka	k1gFnSc7	matka
byla	být	k5eAaImAgFnS	být
dcera	dcera	k1gFnSc1	dcera
jednoho	jeden	k4xCgMnSc2	jeden
z	z	k7c2	z
účastníků	účastník	k1gMnPc2	účastník
převratu	převrat	k1gInSc2	převrat
proti	proti	k7c3	proti
Gaumátovi	Gaumát	k1gMnSc3	Gaumát
<g/>
,	,	kIx,	,
hovořil	hovořit	k5eAaImAgMnS	hovořit
fakt	fakt	k1gInSc4	fakt
prvorozenectví	prvorozenectví	k1gNnSc2	prvorozenectví
<g/>
.	.	kIx.	.
</s>
<s>
Mladšího	mladý	k2eAgMnSc4d2	mladší
Xerxa	Xerxes	k1gMnSc4	Xerxes
zase	zase	k9	zase
doporučoval	doporučovat	k5eAaImAgInS	doporučovat
urozenější	urozený	k2eAgInSc4d2	urozený
původ	původ	k1gInSc4	původ
–	–	k?	–
byl	být	k5eAaImAgMnS	být
synem	syn	k1gMnSc7	syn
Atossy	Atossa	k1gFnSc2	Atossa
<g/>
,	,	kIx,	,
dcery	dcera	k1gFnSc2	dcera
Kýra	Kýr	k1gInSc2	Kýr
Velikého	veliký	k2eAgInSc2d1	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
ho	on	k3xPp3gMnSc4	on
již	již	k9	již
od	od	k7c2	od
mala	mal	k1gInSc2	mal
vychovávali	vychovávat	k5eAaImAgMnP	vychovávat
v	v	k7c6	v
královském	královský	k2eAgInSc6d1	královský
paláci	palác	k1gInSc6	palác
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Artobarzana	Artobarzana	k1gFnSc1	Artobarzana
nikoli	nikoli	k9	nikoli
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Patrně	patrně	k6eAd1	patrně
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
Atossiným	Atossin	k2eAgInSc7d1	Atossin
se	se	k3xPyFc4	se
Dareios	Dareios	k1gInSc1	Dareios
přiklonil	přiklonit	k5eAaPmAgInS	přiklonit
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
prince	princ	k1gMnSc2	princ
Xerxa	Xerxes	k1gMnSc2	Xerxes
a	a	k8xC	a
designoval	designovat	k5eAaPmAgMnS	designovat
ho	on	k3xPp3gMnSc4	on
za	za	k7c4	za
svého	svůj	k3xOyFgMnSc4	svůj
nástupce	nástupce	k1gMnSc4	nástupce
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
jedno	jeden	k4xCgNnSc4	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gNnPc2	jeho
málo	málo	k6eAd1	málo
šťastných	šťastný	k2eAgNnPc2d1	šťastné
rozhodnutí	rozhodnutí	k1gNnPc2	rozhodnutí
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
lze	lze	k6eAd1	lze
těžko	těžko	k6eAd1	těžko
soudit	soudit	k5eAaImF	soudit
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
by	by	kYmCp3nS	by
vypadala	vypadat	k5eAaImAgFnS	vypadat
vláda	vláda	k1gFnSc1	vláda
Artobarzanova	Artobarzanův	k2eAgFnSc1d1	Artobarzanův
<g/>
.	.	kIx.	.
</s>
<s>
Xerxovi	Xerxův	k2eAgMnPc1d1	Xerxův
ale	ale	k8xC	ale
podle	podle	k7c2	podle
všech	všecek	k3xTgFnPc2	všecek
známek	známka	k1gFnPc2	známka
chyběla	chybět	k5eAaImAgFnS	chybět
přinejmenším	přinejmenším	k6eAd1	přinejmenším
důslednost	důslednost	k1gFnSc1	důslednost
<g/>
,	,	kIx,	,
nezbytná	nezbytný	k2eAgFnSc1d1	nezbytná
k	k	k7c3	k
potlačení	potlačení	k1gNnSc3	potlačení
řeckých	řecký	k2eAgMnPc2d1	řecký
rebelů	rebel	k1gMnPc2	rebel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Stavební	stavební	k2eAgFnSc1d1	stavební
a	a	k8xC	a
správní	správní	k2eAgFnSc1d1	správní
činnost	činnost	k1gFnSc1	činnost
==	==	k?	==
</s>
</p>
<p>
<s>
Svému	svůj	k1gMnSc3	svůj
otci	otec	k1gMnSc3	otec
se	se	k3xPyFc4	se
Xerxés	Xerxés	k1gInSc1	Xerxés
vyrovnal	vyrovnat	k5eAaPmAgInS	vyrovnat
snad	snad	k9	snad
jen	jen	k9	jen
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
stavebních	stavební	k2eAgFnPc2d1	stavební
aktivit	aktivita	k1gFnPc2	aktivita
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
je	být	k5eAaImIp3nS	být
namístě	namístě	k?	namístě
podívat	podívat	k5eAaImF	podívat
se	se	k3xPyFc4	se
na	na	k7c4	na
Dareiovu	Dareiův	k2eAgFnSc4d1	Dareiova
vládu	vláda	k1gFnSc4	vláda
z	z	k7c2	z
jiného	jiný	k2eAgInSc2d1	jiný
úhlu	úhel	k1gInSc2	úhel
<g/>
.	.	kIx.	.
</s>
<s>
Dareios	Dareios	k1gMnSc1	Dareios
byl	být	k5eAaImAgMnS	být
nepochybně	pochybně	k6eNd1	pochybně
velký	velký	k2eAgMnSc1d1	velký
budovatel	budovatel	k1gMnSc1	budovatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Susách	Susy	k1gInPc6	Susy
<g/>
,	,	kIx,	,
někdejší	někdejší	k2eAgFnSc6d1	někdejší
metropoli	metropol	k1gFnSc6	metropol
Elamu	Elam	k1gInSc2	Elam
<g/>
,	,	kIx,	,
nechal	nechat	k5eAaPmAgMnS	nechat
na	na	k7c6	na
umělé	umělý	k2eAgFnSc6d1	umělá
terase	terasa	k1gFnSc6	terasa
zřídit	zřídit	k5eAaPmF	zřídit
palác	palác	k1gInSc4	palác
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc4	jehož
hlavní	hlavní	k2eAgInSc4d1	hlavní
sál	sál	k1gInSc4	sál
podepíralo	podepírat	k5eAaImAgNnS	podepírat
72	[number]	k4	72
sloupů	sloup	k1gInPc2	sloup
<g/>
.	.	kIx.	.
</s>
<s>
Jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
Pasargad	Pasargad	k1gInSc4	Pasargad
<g/>
,	,	kIx,	,
korunovačního	korunovační	k2eAgNnSc2d1	korunovační
města	město	k1gNnSc2	město
říše	říš	k1gFnSc2	říš
založeného	založený	k2eAgInSc2d1	založený
Kýrem	kýr	k1gInSc7	kýr
Velikým	veliký	k2eAgInSc7d1	veliký
<g/>
,	,	kIx,	,
vytyčil	vytyčit	k5eAaPmAgMnS	vytyčit
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
515	[number]	k4	515
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
obvod	obvod	k1gInSc4	obvod
nové	nový	k2eAgFnSc2d1	nová
rezidence	rezidence	k1gFnSc2	rezidence
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
zvané	zvaný	k2eAgNnSc1d1	zvané
Persepolis	Persepolis	k1gFnSc4	Persepolis
(	(	kIx(	(
<g/>
Pársa	Párs	k1gMnSc2	Párs
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
během	během	k7c2	během
krátké	krátký	k2eAgFnSc2d1	krátká
doby	doba	k1gFnSc2	doba
stala	stát	k5eAaPmAgFnS	stát
administrativním	administrativní	k2eAgNnSc7d1	administrativní
centrem	centrum	k1gNnSc7	centrum
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
místem	místo	k1gNnSc7	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgInP	být
mj.	mj.	kA	mj.
uchovávány	uchovávat	k5eAaImNgInP	uchovávat
královské	královský	k2eAgInPc1d1	královský
letopisy	letopis	k1gInPc1	letopis
(	(	kIx(	(
<g/>
archiv	archiv	k1gInSc1	archiv
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
četných	četný	k2eAgInPc6d1	četný
nápisech	nápis	k1gInPc6	nápis
oslavujících	oslavující	k2eAgInPc6d1	oslavující
krále	král	k1gMnSc2	král
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
činy	čina	k1gFnSc2	čina
již	již	k6eAd1	již
byla	být	k5eAaImAgFnS	být
řeč	řeč	k1gFnSc1	řeč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dareios	Dareios	k1gMnSc1	Dareios
také	také	k9	také
postavil	postavit	k5eAaPmAgMnS	postavit
slavnou	slavný	k2eAgFnSc4d1	slavná
královskou	královský	k2eAgFnSc4d1	královská
cestu	cesta	k1gFnSc4	cesta
spojující	spojující	k2eAgFnSc2d1	spojující
Susy	Susa	k1gFnSc2	Susa
se	s	k7c7	s
Sardami	Sardy	k1gFnPc7	Sardy
v	v	k7c6	v
Malé	Malé	k2eAgFnSc6d1	Malé
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
2700	[number]	k4	2700
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
,	,	kIx,	,
stálo	stát	k5eAaImAgNnS	stát
u	u	k7c2	u
ní	on	k3xPp3gFnSc2	on
111	[number]	k4	111
přepřahacích	přepřahací	k2eAgFnPc2d1	přepřahací
stanic	stanice	k1gFnPc2	stanice
a	a	k8xC	a
královští	královský	k2eAgMnPc1d1	královský
poslové	posel	k1gMnPc1	posel
byli	být	k5eAaImAgMnP	být
schopni	schopen	k2eAgMnPc1d1	schopen
doručit	doručit	k5eAaPmF	doručit
zprávu	zpráva	k1gFnSc4	zpráva
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
konce	konec	k1gInSc2	konec
na	na	k7c4	na
druhý	druhý	k4xOgInSc4	druhý
již	již	k6eAd1	již
za	za	k7c4	za
šest	šest	k4xCc4	šest
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tehdejších	tehdejší	k2eAgFnPc6d1	tehdejší
komunikačních	komunikační	k2eAgFnPc6d1	komunikační
možnostech	možnost	k1gFnPc6	možnost
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
mimořádnou	mimořádný	k2eAgFnSc4d1	mimořádná
rychlost	rychlost	k1gFnSc4	rychlost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bez	bez	k7c2	bez
povšimnutí	povšimnutí	k1gNnSc2	povšimnutí
nebyly	být	k5eNaImAgFnP	být
ponechány	ponechat	k5eAaPmNgFnP	ponechat
ani	ani	k8xC	ani
správní	správní	k2eAgFnPc1d1	správní
a	a	k8xC	a
hospodářské	hospodářský	k2eAgFnPc1d1	hospodářská
záležitosti	záležitost	k1gFnPc1	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
říši	říše	k1gFnSc4	říše
král	král	k1gMnSc1	král
nově	nově	k6eAd1	nově
rozčlenil	rozčlenit	k5eAaPmAgMnS	rozčlenit
do	do	k7c2	do
dvaceti	dvacet	k4xCc2	dvacet
satrapií	satrapie	k1gFnPc2	satrapie
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
místodržiteli	místodržitel	k1gMnPc7	místodržitel
neboli	neboli	k8xC	neboli
"	"	kIx"	"
<g/>
ochránci	ochránce	k1gMnPc1	ochránce
země	zem	k1gFnSc2	zem
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
staropersky	staropersky	k6eAd1	staropersky
chšaçapávan	chšaçapávan	k1gMnSc1	chšaçapávan
<g/>
,	,	kIx,	,
odtud	odtud	k6eAd1	odtud
řecké	řecký	k2eAgFnSc2d1	řecká
satrapa	satrapa	k1gMnSc1	satrapa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lepšímu	dobrý	k2eAgNnSc3d2	lepší
obchodování	obchodování	k1gNnSc3	obchodování
měla	mít	k5eAaImAgFnS	mít
napomoci	napomoct	k5eAaPmF	napomoct
ražba	ražba	k1gFnSc1	ražba
mincí	mince	k1gFnPc2	mince
<g/>
,	,	kIx,	,
k	k	k7c3	k
níž	jenž	k3xRgFnSc6	jenž
Peršané	Peršan	k1gMnPc1	Peršan
přistoupili	přistoupit	k5eAaPmAgMnP	přistoupit
podle	podle	k7c2	podle
lýdského	lýdský	k2eAgInSc2d1	lýdský
vzoru	vzor	k1gInSc2	vzor
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
to	ten	k3xDgNnSc4	ten
byla	být	k5eAaImAgFnS	být
žhavá	žhavý	k2eAgFnSc1d1	žhavá
novinka	novinka	k1gFnSc1	novinka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mince	mince	k1gFnPc1	mince
dostaly	dostat	k5eAaPmAgFnP	dostat
od	od	k7c2	od
Řeků	Řek	k1gMnPc2	Řek
název	název	k1gInSc4	název
dareiky	dareika	k1gFnPc4	dareika
(	(	kIx(	(
<g/>
z	z	k7c2	z
perského	perský	k2eAgMnSc2d1	perský
daranijam	daranijam	k6eAd1	daranijam
=	=	kIx~	=
zlato	zlato	k1gNnSc4	zlato
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vážily	vážit	k5eAaImAgFnP	vážit
něco	něco	k6eAd1	něco
přes	přes	k7c4	přes
osm	osm	k4xCc4	osm
gramů	gram	k1gInPc2	gram
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
nich	on	k3xPp3gInPc6	on
portrét	portrét	k1gInSc1	portrét
krále	král	k1gMnSc2	král
s	s	k7c7	s
lukem	luk	k1gInSc7	luk
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
<g/>
.	.	kIx.	.
<g/>
Dareios	Dareios	k1gInSc1	Dareios
pochopitelně	pochopitelně	k6eAd1	pochopitelně
zasáhl	zasáhnout	k5eAaPmAgInS	zasáhnout
i	i	k9	i
do	do	k7c2	do
dalších	další	k2eAgFnPc2d1	další
oblastí	oblast	k1gFnPc2	oblast
společenského	společenský	k2eAgInSc2d1	společenský
života	život	k1gInSc2	život
(	(	kIx(	(
<g/>
navázal	navázat	k5eAaPmAgInS	navázat
například	například	k6eAd1	například
na	na	k7c4	na
snášenlivou	snášenlivý	k2eAgFnSc4d1	snášenlivá
náboženskou	náboženský	k2eAgFnSc4d1	náboženská
politiku	politika	k1gFnSc4	politika
Kýra	Kýrum	k1gNnSc2	Kýrum
Velikého	veliký	k2eAgNnSc2d1	veliké
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
celkové	celkový	k2eAgFnSc6d1	celková
bilanci	bilance	k1gFnSc6	bilance
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
to	ten	k3xDgNnSc1	ten
však	však	k9	však
už	už	k6eAd1	už
nic	nic	k3yNnSc1	nic
nemění	měnit	k5eNaImIp3nS	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
drobné	drobný	k2eAgMnPc4d1	drobný
nezdary	nezdara	k1gMnPc4	nezdara
na	na	k7c6	na
Západě	západ	k1gInSc6	západ
po	po	k7c6	po
sobě	se	k3xPyFc3	se
zanechal	zanechat	k5eAaPmAgMnS	zanechat
silnou	silný	k2eAgFnSc4d1	silná
monarchii	monarchie	k1gFnSc4	monarchie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
prakticky	prakticky	k6eAd1	prakticky
v	v	k7c6	v
nezměněných	změněný	k2eNgFnPc6d1	nezměněná
hranicích	hranice	k1gFnPc6	hranice
přečkala	přečkat	k5eAaPmAgFnS	přečkat
ještě	ještě	k6eAd1	ještě
sto	sto	k4xCgNnSc4	sto
padesát	padesát	k4xCc4	padesát
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poznámky	poznámka	k1gFnSc2	poznámka
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
HÉRODOTOS	Hérodotos	k1gMnSc1	Hérodotos
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Šonka	Šonka	k1gMnSc1	Šonka
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
548	[number]	k4	548
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
1192	[number]	k4	1192
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
HOLLAND	HOLLAND	kA	HOLLAND
<g/>
,	,	kIx,	,
Tom	Tom	k1gMnSc1	Tom
<g/>
.	.	kIx.	.
</s>
<s>
Perský	perský	k2eAgInSc1d1	perský
oheň	oheň	k1gInSc1	oheň
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
světová	světový	k2eAgFnSc1d1	světová
velmoc	velmoc	k1gFnSc1	velmoc
a	a	k8xC	a
boj	boj	k1gInSc1	boj
o	o	k7c4	o
Západ	západ	k1gInSc4	západ
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Dokořán	dokořán	k6eAd1	dokořán
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
415	[number]	k4	415
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7363	[number]	k4	7363
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
94	[number]	k4	94
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
KLÍMA	Klíma	k1gMnSc1	Klíma
<g/>
,	,	kIx,	,
Otakar	Otakar	k1gMnSc1	Otakar
<g/>
.	.	kIx.	.
</s>
<s>
Sláva	Sláva	k1gFnSc1	Sláva
a	a	k8xC	a
pád	pád	k1gInSc1	pád
starého	starý	k2eAgInSc2d1	starý
Íránu	Írán	k1gInSc2	Írán
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Orbis	orbis	k1gInSc1	orbis
<g/>
,	,	kIx,	,
1977	[number]	k4	1977
<g/>
.	.	kIx.	.
252	[number]	k4	252
s.	s.	k?	s.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Oběti	oběť	k1gFnPc1	oběť
ohňům	oheň	k1gInPc3	oheň
<g/>
.	.	kIx.	.
</s>
<s>
Výběr	výběr	k1gInSc1	výběr
z	z	k7c2	z
památek	památka	k1gFnPc2	památka
staroíránské	staroíránský	k2eAgFnSc2d1	staroíránský
a	a	k8xC	a
středoíránské	středoíránský	k2eAgFnSc2d1	středoíránský
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Otakar	Otakar	k1gMnSc1	Otakar
Klíma	Klíma	k1gMnSc1	Klíma
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
347	[number]	k4	347
s.	s.	k?	s.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
WIESEHÖFER	WIESEHÖFER	kA	WIESEHÖFER
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Das	Das	k?	Das
antike	antikat	k5eAaPmIp3nS	antikat
Persien	Persien	k1gInSc1	Persien
<g/>
.	.	kIx.	.
</s>
<s>
Von	von	k1gInSc1	von
550	[number]	k4	550
v.	v.	k?	v.
Chr	chr	k0	chr
<g/>
.	.	kIx.	.
bis	bis	k?	bis
650	[number]	k4	650
n.	n.	k?	n.
Chr	chr	k0	chr
<g/>
..	..	k?	..
München	München	k2eAgInSc4d1	München
;	;	kIx,	;
Zürich	Zürich	k1gInSc4	Zürich
<g/>
:	:	kIx,	:
Artemis	Artemis	k1gFnSc1	Artemis
und	und	k?	und
Winkler	Winkler	k1gMnSc1	Winkler
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
426	[number]	k4	426
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
7608	[number]	k4	7608
<g/>
-	-	kIx~	-
<g/>
1080	[number]	k4	1080
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Achaimenovci	Achaimenovec	k1gMnPc1	Achaimenovec
</s>
</p>
<p>
<s>
Řecko-perské	řeckoerský	k2eAgFnPc1d1	řecko-perský
války	válka	k1gFnPc1	válka
</s>
</p>
<p>
<s>
Chronologie	chronologie	k1gFnSc1	chronologie
starověkých	starověký	k2eAgFnPc2d1	starověká
íránských	íránský	k2eAgFnPc2d1	íránská
dějin	dějiny	k1gFnPc2	dějiny
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Dareios	Dareiosa	k1gFnPc2	Dareiosa
I.	I.	kA	I.
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Dareiovo	Dareiův	k2eAgNnSc1d1	Dareiův
tažení	tažení	k1gNnSc1	tažení
proti	proti	k7c3	proti
Skythům	Skyth	k1gMnPc3	Skyth
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Článek	článek	k1gInSc1	článek
v	v	k7c4	v
Encyclopaedia	Encyclopaedium	k1gNnPc4	Encyclopaedium
Iranica	Iranic	k2eAgNnPc4d1	Iranic
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Článek	článek	k1gInSc1	článek
na	na	k7c6	na
serveru	server	k1gInSc6	server
Livius	Livius	k1gMnSc1	Livius
</s>
</p>
