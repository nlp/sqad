<s>
Volby	volba	k1gFnPc1
do	do	k7c2
Zastupitelstva	zastupitelstvo	k1gNnSc2
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
2014	#num#	k4
</s>
<s>
Volby	volba	k1gFnPc1
do	do	k7c2
zastupitelstva	zastupitelstvo	k1gNnSc2
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
proběhly	proběhnout	k5eAaPmAgInP
v	v	k7c6
rámci	rámec	k1gInSc6
obecních	obecní	k2eAgFnPc2d1
voleb	volba	k1gFnPc2
v	v	k7c4
pátek	pátek	k1gInSc4
10	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
a	a	k8xC
sobotu	sobota	k1gFnSc4
11	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pražané	Pražan	k1gMnPc1
nově	nově	k6eAd1
volili	volit	k5eAaImAgMnP
65	#num#	k4
zastupitelů	zastupitel	k1gMnPc2
místo	místo	k7c2
dřívějších	dřívější	k2eAgNnPc2d1
63	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
zastupitelstva	zastupitelstvo	k1gNnSc2
hl.	hl.	k?
m.	m.	k?
Prahy	Praha	k1gFnSc2
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
dostat	dostat	k5eAaPmF
sedmi	sedm	k4xCc3
subjektům	subjekt	k1gInPc3
<g/>
.	.	kIx.
</s>
<s>
Území	území	k1gNnSc1
města	město	k1gNnSc2
bylo	být	k5eAaImAgNnS
pouze	pouze	k6eAd1
jedním	jeden	k4xCgInSc7
volebním	volební	k2eAgInSc7d1
obvodem	obvod	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stalo	stát	k5eAaPmAgNnS
se	se	k3xPyFc4
tak	tak	k9
znovu	znovu	k6eAd1
po	po	k7c6
osmi	osm	k4xCc6
letech	léto	k1gNnPc6
<g/>
,	,	kIx,
neboť	neboť	k8xC
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
byla	být	k5eAaImAgFnS
Praha	Praha	k1gFnSc1
rozdělena	rozdělit	k5eAaPmNgFnS
do	do	k7c2
7	#num#	k4
volebních	volební	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
dělení	dělení	k1gNnSc1
mělo	mít	k5eAaImAgNnS
podle	podle	k7c2
kritiků	kritik	k1gMnPc2
zabránit	zabránit	k5eAaPmF
malým	malý	k2eAgFnPc3d1
stranám	strana	k1gFnPc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
se	se	k3xPyFc4
do	do	k7c2
zastupitelstva	zastupitelstvo	k1gNnSc2
dostaly	dostat	k5eAaPmAgInP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Zatímco	zatímco	k8xS
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
se	se	k3xPyFc4
o	o	k7c4
hlasy	hlas	k1gInPc4
voličů	volič	k1gMnPc2
ucházelo	ucházet	k5eAaImAgNnS
23	#num#	k4
stran	strana	k1gFnPc2
a	a	k8xC
hnutí	hnutí	k1gNnSc2
s	s	k7c7
1074	#num#	k4
jmény	jméno	k1gNnPc7
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
bylo	být	k5eAaImAgNnS
politických	politický	k2eAgInPc2d1
subjektů	subjekt	k1gInPc2
31	#num#	k4
a	a	k8xC
nabízely	nabízet	k5eAaImAgFnP
výběr	výběr	k1gInSc4
z	z	k7c2
1847	#num#	k4
kandidátů	kandidát	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Volební	volební	k2eAgFnSc1d1
účast	účast	k1gFnSc1
činila	činit	k5eAaImAgFnS
37,72	37,72	k4
%	%	kIx~
oprávněných	oprávněný	k2eAgMnPc2d1
voličů	volič	k1gMnPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
druhý	druhý	k4xOgInSc4
nejnižší	nízký	k2eAgInSc4d3
výsledek	výsledek	k1gInSc4
od	od	k7c2
Sametové	sametový	k2eAgFnSc2d1
revoluce	revoluce	k1gFnSc2
<g/>
,	,	kIx,
hned	hned	k6eAd1
za	za	k7c7
volební	volební	k2eAgFnSc7d1
účastí	účast	k1gFnSc7
v	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
činila	činit	k5eAaImAgFnS
35,29	35,29	k4
%	%	kIx~
oprávněných	oprávněný	k2eAgMnPc2d1
voličů	volič	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
porovnání	porovnání	k1gNnSc6
s	s	k7c7
předcházejícími	předcházející	k2eAgFnPc7d1
volbami	volba	k1gFnPc7
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
účast	účast	k1gFnSc1
poklesla	poklesnout	k5eAaPmAgFnS
o	o	k7c4
6,71	6,71	k4
procentních	procentní	k2eAgInPc2d1
bodů	bod	k1gInPc2
z	z	k7c2
tehdejších	tehdejší	k2eAgInPc2d1
44,43	44,43	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
volbách	volba	k1gFnPc6
zvítězilo	zvítězit	k5eAaPmAgNnS
při	při	k7c6
své	svůj	k3xOyFgFnSc6
premiéře	premiéra	k1gFnSc6
v	v	k7c6
komunálních	komunální	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
hnutí	hnutí	k1gNnSc2
ANO	ano	k9
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhá	druhý	k4xOgFnSc1
v	v	k7c6
těsném	těsný	k2eAgInSc6d1
závěsu	závěs	k1gInSc6
byla	být	k5eAaImAgFnS
TOP	topit	k5eAaImRp2nS
0	#num#	k4
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
třetím	třetí	k4xOgInSc6
<g/>
,	,	kIx,
čtvrtém	čtvrtý	k4xOgInSc6
a	a	k8xC
pátém	pátý	k4xOgInSc6
místě	místo	k1gNnSc6
skončili	skončit	k5eAaPmAgMnP
v	v	k7c6
malých	malý	k2eAgInPc6d1
rozestupech	rozestup	k1gInPc6
Trojkoalice	Trojkoalice	k1gFnSc2
(	(	kIx(
<g/>
SZ	SZ	kA
<g/>
,	,	kIx,
KDU-ČSL	KDU-ČSL	k1gFnSc1
a	a	k8xC
STAN	stan	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ODS	ODS	kA
a	a	k8xC
ČSSD	ČSSD	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
eliminaci	eliminace	k1gFnSc3
přirozené	přirozený	k2eAgFnPc4d1
uzavírací	uzavírací	k2eAgFnPc4d1
klauzule	klauzule	k1gFnPc4
jedním	jeden	k4xCgInSc7
volebním	volební	k2eAgInSc7d1
obvodem	obvod	k1gInSc7
a	a	k8xC
vysokým	vysoký	k2eAgInSc7d1
počtem	počet	k1gInSc7
zastupitelů	zastupitel	k1gMnPc2
se	se	k3xPyFc4
do	do	k7c2
zastupitelstva	zastupitelstvo	k1gNnSc2
dostali	dostat	k5eAaPmAgMnP
i	i	k9
KSČM	KSČM	kA
a	a	k8xC
Piráti	pirát	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
jen	jen	k9
těsně	těsně	k6eAd1
překročili	překročit	k5eAaPmAgMnP
5	#num#	k4
%	%	kIx~
uzavírací	uzavírací	k2eAgFnSc4d1
klauzuli	klauzule	k1gFnSc4
<g/>
,	,	kIx,
když	když	k8xS
získali	získat	k5eAaPmAgMnP
5,91	5,91	k4
%	%	kIx~
a	a	k8xC
5,31	5,31	k4
%	%	kIx~
hlasů	hlas	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
porovnání	porovnání	k1gNnSc4
<g/>
:	:	kIx,
ve	v	k7c6
volbách	volba	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
(	(	kIx(
<g/>
se	s	k7c7
7	#num#	k4
obvody	obvod	k1gInPc4
<g/>
)	)	kIx)
se	se	k3xPyFc4
do	do	k7c2
zastupitelstva	zastupitelstvo	k1gNnSc2
nedostal	dostat	k5eNaPmAgMnS
subjekt	subjekt	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
získal	získat	k5eAaPmAgInS
5,90	5,90	k4
%	%	kIx~
hlasů	hlas	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Výsledky	výsledek	k1gInPc1
hlasování	hlasování	k1gNnSc2
</s>
<s>
Výsledky	výsledek	k1gInPc4
voleb	volba	k1gFnPc2
do	do	k7c2
Zastupitelstva	zastupitelstvo	k1gNnSc2
hl.	hl.	k?
m.	m.	k?
Prahy	Praha	k1gFnSc2
2014	#num#	k4
</s>
<s>
Konečné	Konečné	k2eAgInPc1d1
výsledky	výsledek	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
v	v	k7c6
tabulce	tabulka	k1gFnSc6
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Strana	strana	k1gFnSc1
</s>
<s>
Počet	počet	k1gInSc1
hlasů	hlas	k1gInPc2
</s>
<s>
%	%	kIx~
</s>
<s>
Mandáty	mandát	k1gInPc1
</s>
<s>
Mandáty	mandát	k1gInPc1
z	z	k7c2
voleb	volba	k1gFnPc2
2010	#num#	k4
</s>
<s>
Bilance	bilance	k1gFnSc1
</s>
<s>
ANO	ano	k9
2011	#num#	k4
</s>
<s>
4	#num#	k4
574	#num#	k4
610	#num#	k4
</s>
<s>
22,08	22,08	k4
</s>
<s>
17	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
17	#num#	k4
▲	▲	k?
+17	+17	k4
</s>
<s>
TOP	topit	k5eAaImRp2nS
09	#num#	k4
</s>
<s>
4	#num#	k4
158	#num#	k4
226	#num#	k4
</s>
<s>
20,07	20,07	k4
</s>
<s>
16	#num#	k4
</s>
<s>
26	#num#	k4
</s>
<s>
-10	-10	k4
▼	▼	k?
-10	-10	k4
</s>
<s>
TROJKOALICE	TROJKOALICE	kA
ZELENÍ	zelenit	k5eAaImIp3nS
<g/>
,	,	kIx,
KDU-ČSL	KDU-ČSL	k1gMnPc1
A	a	k8xC
STAROSTOVÉ	Starosta	k1gMnPc1
–	–	k?
MÁTE	mít	k5eAaImIp2nP
PRÁVO	práv	k2eAgNnSc1d1
</s>
<s>
2	#num#	k4
323	#num#	k4
976	#num#	k4
</s>
<s>
11,22	11,22	k4
</s>
<s>
8	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
8	#num#	k4
▲	▲	k?
+8	+8	k4
</s>
<s>
Občanská	občanský	k2eAgFnSc1d1
demokratická	demokratický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
</s>
<s>
2	#num#	k4
273	#num#	k4
722	#num#	k4
</s>
<s>
10,97	10,97	k4
</s>
<s>
8	#num#	k4
</s>
<s>
20	#num#	k4
</s>
<s>
-12	-12	k4
▼	▼	k?
-12	-12	k4
</s>
<s>
Česká	český	k2eAgFnSc1d1
strana	strana	k1gFnSc1
sociálně	sociálně	k6eAd1
demokratická	demokratický	k2eAgFnSc1d1
</s>
<s>
2	#num#	k4
160	#num#	k4
963	#num#	k4
</s>
<s>
10,43	10,43	k4
</s>
<s>
8	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
-6	-6	k4
▼	▼	k?
-6	-6	k4
</s>
<s>
Komunistická	komunistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Čech	Čechy	k1gFnPc2
a	a	k8xC
Moravy	Morava	k1gFnSc2
</s>
<s>
1	#num#	k4
225	#num#	k4
102	#num#	k4
</s>
<s>
5,91	5,91	k4
</s>
<s>
4	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
1	#num#	k4
▲	▲	k?
+1	+1	k4
</s>
<s>
Česká	český	k2eAgFnSc1d1
pirátská	pirátský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
</s>
<s>
1	#num#	k4
101	#num#	k4
081	#num#	k4
</s>
<s>
5,31	5,31	k4
</s>
<s>
4	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
4	#num#	k4
▲	▲	k?
+4	+4	k4
</s>
<s>
Strana	strana	k1gFnSc1
svobodných	svobodný	k2eAgMnPc2d1
občanů	občan	k1gMnPc2
</s>
<s>
741	#num#	k4
503	#num#	k4
</s>
<s>
3,58	3,58	k4
</s>
<s>
0	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
Demokraté	demokrat	k1gMnPc1
Jana	Jan	k1gMnSc2
Kasla	Kasl	k1gMnSc2
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
512	#num#	k4
068	#num#	k4
</s>
<s>
2,47	2,47	k4
</s>
<s>
0	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
Pro	pro	k7c4
Prahu	Praha	k1gFnSc4
</s>
<s>
496	#num#	k4
821	#num#	k4
</s>
<s>
2,40	2,40	k4
</s>
<s>
0	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1
subjekty	subjekt	k1gInPc1
(	(	kIx(
<g/>
20	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
<	<	kIx(
<g/>
300	#num#	k4
000	#num#	k4
</s>
<s>
<	<	kIx(
<g/>
2,00	2,00	k4
</s>
<s>
0	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
celkem	celkem	k6eAd1
(	(	kIx(
<g/>
volební	volební	k2eAgFnSc1d1
účast	účast	k1gFnSc1
37,72	37,72	k4
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
20	#num#	k4
717	#num#	k4
535	#num#	k4
</s>
<s>
100	#num#	k4
</s>
<s>
65	#num#	k4
</s>
<s>
63	#num#	k4
</s>
<s>
+2	+2	k4
</s>
<s>
Zvolení	zvolený	k2eAgMnPc1d1
zastupitelé	zastupitel	k1gMnPc1
</s>
<s>
Jméno	jméno	k1gNnSc1
a	a	k8xC
příjmení	příjmení	k1gNnSc1
</s>
<s>
Strana	strana	k1gFnSc1
</s>
<s>
Hlasy	hlas	k1gInPc1
</s>
<s>
Pořadí	pořadí	k1gNnSc1
</s>
<s>
počet	počet	k1gInSc1
</s>
<s>
v	v	k7c6
%	%	kIx~
</s>
<s>
na	na	k7c6
kandidátce	kandidátka	k1gFnSc6
</s>
<s>
výsledné	výsledný	k2eAgNnSc1d1
</s>
<s>
Krnáčová	Krnáčová	k1gFnSc1
<g/>
,	,	kIx,
AdrianaAdriana	AdrianaAdriana	k1gFnSc1
Krnáčovábezpartijní	Krnáčovábezpartijní	k2eAgFnSc1d1
za	za	k7c4
ANO	ano	k9
201174	#num#	k4
7361,631	7361,631	k4
<g/>
.1	.1	k4
<g/>
.	.	kIx.
</s>
<s>
Kislingerová	Kislingerová	k1gFnSc1
<g/>
,	,	kIx,
EvaEva	EvaEva	k1gFnSc1
Kislingerovábezpartijní	Kislingerovábezpartijní	k2eAgFnSc1d1
za	za	k7c4
ANO	ano	k9
201173	#num#	k4
8061,612	8061,612	k4
<g/>
.2	.2	k4
<g/>
.	.	kIx.
</s>
<s>
Nacher	Nachra	k1gFnPc2
<g/>
,	,	kIx,
PatrikPatrik	PatrikPatrika	k1gFnPc2
Nacherbezpartijní	Nacherbezpartijní	k2eAgFnSc1d1
za	za	k7c4
ANO	ano	k9
201174	#num#	k4
3321,623	3321,623	k4
<g/>
.3	.3	k4
<g/>
.	.	kIx.
</s>
<s>
Hašek	Hašek	k1gMnSc1
<g/>
,	,	kIx,
MichalMichal	MichalMichal	k1gMnSc1
HašekANO	HašekANO	k1gFnSc2
201172	#num#	k4
5181,584	5181,584	k4
<g/>
.4	.4	k4
<g/>
.	.	kIx.
</s>
<s>
Kaplický	Kaplický	k2eAgInSc1d1
Fuchsová	Fuchsová	k1gFnSc1
<g/>
,	,	kIx,
EliškaEliška	EliškaElišek	k1gMnSc2
Kaplický	Kaplický	k2eAgInSc4d1
Fuchsovábezpartijní	Fuchsovábezpartijní	k2eAgInSc4d1
za	za	k7c7
ANO	ano	k9
201174	#num#	k4
1161,625	1161,625	k4
<g/>
.5	.5	k4
<g/>
.	.	kIx.
</s>
<s>
Lacko	Lacko	k1gNnSc1
<g/>
,	,	kIx,
RadekRadek	RadekRadek	k1gInSc1
LackoANO	LackoANO	k1gFnSc1
201172	#num#	k4
2611,576	2611,576	k4
<g/>
.6	.6	k4
<g/>
.	.	kIx.
</s>
<s>
Novotný	Novotný	k1gMnSc1
<g/>
,	,	kIx,
PetrPetr	PetrPetr	k1gMnSc1
NovotnýANO	NovotnýANO	k1gFnSc2
201172	#num#	k4
2131,577	2131,577	k4
<g/>
.7	.7	k4
<g/>
.	.	kIx.
</s>
<s>
Bendová	Bendová	k1gFnSc1
<g/>
,	,	kIx,
JarmilaJarmila	JarmilaJarmila	k1gFnSc1
BendováANO	BendováANO	k1gFnSc1
201172	#num#	k4
6641,588	6641,588	k4
<g/>
.8	.8	k4
<g/>
.	.	kIx.
</s>
<s>
Hadrava	Hadrava	k1gFnSc1
<g/>
,	,	kIx,
LiborLibor	LiborLibor	k1gInSc1
HadravaANO	HadravaANO	k1gFnSc1
201171	#num#	k4
6551,569	6551,569	k4
<g/>
.9	.9	k4
<g/>
.	.	kIx.
</s>
<s>
Štěpánek	Štěpánek	k1gMnSc1
<g/>
,	,	kIx,
JaroslavJaroslav	JaroslavJaroslav	k1gMnSc1
ŠtěpánekANO	ŠtěpánekANO	k1gFnSc2
201171	#num#	k4
3891,5610	3891,5610	k4
<g/>
.10	.10	k4
<g/>
.	.	kIx.
</s>
<s>
Nepil	pít	k5eNaImAgMnS
<g/>
,	,	kIx,
RadomírRadomír	RadomírRadomír	k1gMnSc1
NepilANO	NepilANO	k1gFnSc2
201171	#num#	k4
4871,5611	4871,5611	k4
<g/>
.11	.11	k4
<g/>
.	.	kIx.
</s>
<s>
Plesníková	Plesníková	k1gFnSc1
<g/>
,	,	kIx,
MarcelaMarcela	MarcelaMarcela	k1gFnSc1
PlesníkováANO	PlesníkováANO	k1gFnSc1
201171	#num#	k4
8221,5712	8221,5712	k4
<g/>
.12	.12	k4
<g/>
.	.	kIx.
</s>
<s>
Veselá	Veselá	k1gFnSc1
<g/>
,	,	kIx,
VladislavaVladislava	VladislavaVladislava	k1gFnSc1
VeseláANO	VeseláANO	k1gFnSc1
201171	#num#	k4
6591,5613	6591,5613	k4
<g/>
.13	.13	k4
<g/>
.	.	kIx.
</s>
<s>
Polách	Polách	k1gMnSc1
<g/>
,	,	kIx,
MartinMartin	MartinMartin	k1gMnSc1
PoláchANO	PoláchANO	k1gFnSc2
201171	#num#	k4
8541,5714	8541,5714	k4
<g/>
.14	.14	k4
<g/>
.	.	kIx.
</s>
<s>
Grabein	Grabein	k1gMnSc1
Procházka	Procházka	k1gMnSc1
<g/>
,	,	kIx,
KarelKarel	KarelKarel	k1gMnSc1
Grabein	Grabein	k1gMnSc1
ProcházkaANO	ProcházkaANO	k1gMnSc1
201171	#num#	k4
1841,5515	1841,5515	k4
<g/>
.15	.15	k4
<g/>
.	.	kIx.
</s>
<s>
Haramul	Haramul	k1gInSc1
<g/>
,	,	kIx,
JiříJiří	JiříJiří	k2eAgInSc1d1
Haramulbezpartijní	Haramulbezpartijní	k2eAgInSc4d1
za	za	k7c7
ANO	ano	k9
201171	#num#	k4
4341,5616	4341,5616	k4
<g/>
.16	.16	k4
<g/>
.	.	kIx.
</s>
<s>
Lébl	Lébl	k1gMnSc1
<g/>
,	,	kIx,
AntonínAntonín	AntonínAntonín	k1gMnSc1
LéblANO	LéblANO	k1gFnSc2
201171	#num#	k4
3231,5517	3231,5517	k4
<g/>
.17	.17	k4
<g/>
.	.	kIx.
</s>
<s>
Hudeček	Hudeček	k1gMnSc1
<g/>
,	,	kIx,
TomášTomáš	TomášTomáš	k1gMnSc1
HudečekTOP	HudečekTOP	k1gFnSc2
0972	#num#	k4
2301,731	2301,731	k4
<g/>
.1	.1	k4
<g/>
.	.	kIx.
</s>
<s>
Nouza	Nouza	k1gFnSc1
<g/>
,	,	kIx,
JiříJiří	JiříJiří	k2eAgFnSc1d1
NouzaTOP	NouzaTOP	k1gFnSc1
0967	#num#	k4
0	#num#	k4
<g/>
941,612.2	941,612.2	k4
<g/>
.	.	kIx.
</s>
<s>
Dlouhý	Dlouhý	k1gMnSc1
<g/>
,	,	kIx,
MartinMartin	MartinMartin	k1gMnSc1
DlouhýTOP	DlouhýTOP	k1gFnSc2
0967	#num#	k4
3361,613	3361,613	k4
<g/>
.3	.3	k4
<g/>
.	.	kIx.
</s>
<s>
Vorlíčková	Vorlíčková	k1gFnSc1
<g/>
,	,	kIx,
EvaEva	EvaEva	k1gFnSc1
VorlíčkováTOP	VorlíčkováTOP	k1gFnSc1
0966	#num#	k4
9451,604	9451,604	k4
<g/>
.4	.4	k4
<g/>
.	.	kIx.
</s>
<s>
Bříza	bříza	k1gFnSc1
<g/>
,	,	kIx,
PetrPetr	PetrPetr	k1gInSc1
Břízabezpartijní	Břízabezpartijní	k2eAgInSc1d1
za	za	k7c4
TOP	topit	k5eAaImRp2nS
0966	#num#	k4
9351,605	9351,605	k4
<g/>
.5	.5	k4
<g/>
.	.	kIx.
</s>
<s>
Novotný	Novotný	k1gMnSc1
<g/>
,	,	kIx,
VáclavVáclav	VáclavVáclav	k1gMnSc1
NovotnýTOP	NovotnýTOP	k1gFnSc2
0966	#num#	k4
2011,596	2011,596	k4
<g/>
.6	.6	k4
<g/>
.	.	kIx.
</s>
<s>
Kubišta	Kubišta	k1gMnSc1
<g/>
,	,	kIx,
AlbertAlbert	AlbertAlbert	k1gMnSc1
KubištaTOP	KubištaTOP	k1gFnSc2
0966	#num#	k4
2761,597	2761,597	k4
<g/>
.7	.7	k4
<g/>
.	.	kIx.
</s>
<s>
Hujová	Hujová	k1gFnSc1
<g/>
,	,	kIx,
VladislavaVladislava	VladislavaVladislava	k1gFnSc1
HujováTOP	HujováTOP	k1gFnSc1
0966	#num#	k4
0	#num#	k4
<g/>
901,588.8	901,588.8	k4
<g/>
.	.	kIx.
</s>
<s>
Jílek	Jílek	k1gMnSc1
<g/>
,	,	kIx,
TomášTomáš	TomášTomáš	k1gMnSc1
JílekTOP	JílekTOP	k1gFnSc2
0965	#num#	k4
8251,589	8251,589	k4
<g/>
.9	.9	k4
<g/>
.	.	kIx.
</s>
<s>
Manhart	Manhart	k1gInSc1
<g/>
,	,	kIx,
LukášLukáš	LukášLukáš	k1gFnSc1
ManhartTOP	ManhartTOP	k1gFnSc1
0965	#num#	k4
8691,5810	8691,5810	k4
<g/>
.10	.10	k4
<g/>
.	.	kIx.
</s>
<s>
Krobová	Krobový	k2eAgFnSc1d1
Hášová	Hášová	k1gFnSc1
<g/>
,	,	kIx,
MonikaMonika	MonikaMonika	k1gFnSc1
Krobová	Krobová	k1gFnSc1
HášováTOP	HášováTOP	k1gFnSc1
0965	#num#	k4
7731,5811	7731,5811	k4
<g/>
.11	.11	k4
<g/>
.	.	kIx.
</s>
<s>
Prchal	Prchal	k1gMnSc1
<g/>
,	,	kIx,
PetrPetr	PetrPetr	k1gMnSc1
PrchalTOP	PrchalTOP	k1gFnSc2
0965	#num#	k4
7751,5812	7751,5812	k4
<g/>
.12	.12	k4
<g/>
.	.	kIx.
</s>
<s>
Růžička	Růžička	k1gMnSc1
<g/>
,	,	kIx,
MilanMilan	MilanMilan	k1gMnSc1
RůžičkaTOP	RůžičkaTOP	k1gFnSc2
0965	#num#	k4
3291,5713	3291,5713	k4
<g/>
.13	.13	k4
<g/>
.	.	kIx.
</s>
<s>
Hrabák	Hrabák	k1gMnSc1
<g/>
,	,	kIx,
JiříJiří	JiříJiří	k2eAgMnSc1d1
HrabákTOP	HrabákTOP	k1gMnSc1
0971	#num#	k4
8541,5614	8541,5614	k4
<g/>
.14	.14	k4
<g/>
.	.	kIx.
</s>
<s>
Richter	Richter	k1gMnSc1
<g/>
,	,	kIx,
PavelPavel	PavelPavel	k1gMnSc1
RichterTOP	RichterTOP	k1gFnSc2
0965	#num#	k4
3561,5715	3561,5715	k4
<g/>
.15	.15	k4
<g/>
.	.	kIx.
</s>
<s>
Doležal	Doležal	k1gMnSc1
<g/>
,	,	kIx,
MarekMarek	MarekMarka	k1gFnPc2
DoležalTOP	DoležalTOP	k1gFnSc4
0965	#num#	k4
2431,5616	2431,5616	k4
<g/>
.16	.16	k4
<g/>
.	.	kIx.
</s>
<s>
Štěpánek	Štěpánek	k1gMnSc1
<g/>
,	,	kIx,
PetrPetr	PetrPetr	k1gMnSc1
ŠtěpánekSZ	ŠtěpánekSZ	k1gMnSc1
za	za	k7c4
Trojkoalici	Trojkoalice	k1gFnSc4
<g/>
42	#num#	k4
3361,821	3361,821	k4
<g/>
.1	.1	k4
<g/>
.	.	kIx.
</s>
<s>
Čižinský	Čižinský	k2eAgMnSc1d1
<g/>
,	,	kIx,
JanJan	JanJan	k1gMnSc1
ČižinskýKDU-ČSL	ČižinskýKDU-ČSL	k1gMnSc1
za	za	k7c4
Trojkoalici	Trojkoalice	k1gFnSc4
<g/>
41	#num#	k4
8111,795	8111,795	k4
<g/>
.2	.2	k4
<g/>
.	.	kIx.
</s>
<s>
Kolínská	kolínská	k1gFnSc1
<g/>
,	,	kIx,
PetraPetra	PetraPetra	k1gFnSc1
KolínskáSZ	KolínskáSZ	k1gFnSc2
za	za	k7c4
Trojkoalici	Trojkoalice	k1gFnSc4
<g/>
40	#num#	k4
1481,724	1481,724	k4
<g/>
.3	.3	k4
<g/>
.	.	kIx.
</s>
<s>
Stropnický	stropnický	k2eAgMnSc1d1
<g/>
,	,	kIx,
MatějMatěj	MatějMatěj	k1gMnSc1
StropnickýSZ	StropnickýSZ	k1gMnSc1
za	za	k7c4
Trojkoalici	Trojkoalice	k1gFnSc4
<g/>
39	#num#	k4
4841,6911	4841,6911	k4
<g/>
.4	.4	k4
<g/>
.	.	kIx.
</s>
<s>
Mirovský	Mirovský	k1gMnSc1
<g/>
,	,	kIx,
OndřejOndřej	OndřejOndřej	k1gMnSc1
MirovskýSZ	MirovskýSZ	k1gMnSc1
za	za	k7c4
Trojkoalici	Trojkoalice	k1gFnSc4
<g/>
39	#num#	k4
4321,697	4321,697	k4
<g/>
.5	.5	k4
<g/>
.	.	kIx.
</s>
<s>
Wolf	Wolf	k1gMnSc1
<g/>
,	,	kIx,
JanJan	JanJan	k1gMnSc1
WolfKDU-ČSL	WolfKDU-ČSL	k1gMnSc1
za	za	k7c4
Trojkoalici	Trojkoalice	k1gFnSc4
<g/>
38	#num#	k4
4081,652	4081,652	k4
<g/>
.6	.6	k4
<g/>
.	.	kIx.
</s>
<s>
Plamínková	plamínkový	k2eAgFnSc1d1
<g/>
,	,	kIx,
JanaJana	JanaJana	k1gFnSc1
PlamínkováSTAN	PlamínkováSTAN	k1gFnSc2
za	za	k7c4
Trojkoalici	Trojkoalice	k1gFnSc4
<g/>
38	#num#	k4
7141,663	7141,663	k4
<g/>
.7	.7	k4
<g/>
.	.	kIx.
</s>
<s>
Růžička	Růžička	k1gMnSc1
<g/>
,	,	kIx,
MilošMiloš	MilošMiloš	k1gMnSc1
RůžičkaSTAN	RůžičkaSTAN	k1gMnSc1
za	za	k7c4
Trojkoalici	Trojkoalice	k1gFnSc4
<g/>
37	#num#	k4
3061,606	3061,606	k4
<g/>
.8	.8	k4
<g/>
.	.	kIx.
</s>
<s>
Svoboda	Svoboda	k1gMnSc1
<g/>
,	,	kIx,
BohuslavBohuslav	BohuslavBohuslav	k1gMnSc1
SvobodaODS	SvobodaODS	k1gFnSc2
<g/>
44	#num#	k4
8301,971	8301,971	k4
<g/>
.1	.1	k4
<g/>
.	.	kIx.
</s>
<s>
Humplík	Humplík	k1gMnSc1
<g/>
,	,	kIx,
FilipFilip	FilipFilip	k1gMnSc1
HumplíkODS	HumplíkODS	k1gFnSc2
<g/>
36	#num#	k4
8941,622	8941,622	k4
<g/>
.2	.2	k4
<g/>
.	.	kIx.
</s>
<s>
Janderová	Janderová	k1gFnSc1
<g/>
,	,	kIx,
JaroslavaJaroslava	JaroslavaJaroslava	k1gFnSc1
JanderováODS	JanderováODS	k1gFnSc1
<g/>
36	#num#	k4
3761,593	3761,593	k4
<g/>
.3	.3	k4
<g/>
.	.	kIx.
</s>
<s>
Bellu	bell	k1gInSc2
<g/>
,	,	kIx,
AlexanderAlexander	AlexanderAlexander	k1gMnSc1
BelluODS	BelluODS	k1gFnSc2
<g/>
36	#num#	k4
2601,594	2601,594	k4
<g/>
.4	.4	k4
<g/>
.	.	kIx.
</s>
<s>
Udženija	Udženija	k1gFnSc1
<g/>
,	,	kIx,
AlexandraAlexandra	AlexandraAlexandra	k1gFnSc1
UdženijaODS	UdženijaODS	k1gFnSc1
<g/>
36	#num#	k4
9981,625	9981,625	k4
<g/>
.5	.5	k4
<g/>
.	.	kIx.
</s>
<s>
Švarc	Švarc	k1gMnSc1
<g/>
,	,	kIx,
FrantišekFrantišek	FrantišekFrantišek	k1gMnSc1
ŠvarcODS	ŠvarcODS	k1gFnSc2
<g/>
36	#num#	k4
1841,596	1841,596	k4
<g/>
.6	.6	k4
<g/>
.	.	kIx.
</s>
<s>
Fifka	Fifka	k1gMnSc1
<g/>
,	,	kIx,
PetrPetr	PetrPetr	k1gMnSc1
FifkaODS	FifkaODS	k1gFnSc2
<g/>
35	#num#	k4
9111,577	9111,577	k4
<g/>
.7	.7	k4
<g/>
.	.	kIx.
</s>
<s>
Martan	Martan	k1gInSc1
<g/>
,	,	kIx,
OndřejOndřej	OndřejOndřej	k1gFnSc1
MartanODS	MartanODS	k1gFnSc1
<g/>
36	#num#	k4
1211,588	1211,588	k4
<g/>
.8	.8	k4
<g/>
.	.	kIx.
</s>
<s>
Ludvík	Ludvík	k1gMnSc1
<g/>
,	,	kIx,
MiloslavMiloslav	MiloslavMiloslav	k1gMnSc1
LudvíkČSSD	LudvíkČSSD	k1gFnSc2
<g/>
36	#num#	k4
8511,701	8511,701	k4
<g/>
.1	.1	k4
<g/>
.	.	kIx.
</s>
<s>
Dolínek	dolínek	k1gInSc1
<g/>
,	,	kIx,
PetrPetr	PetrPetr	k1gInSc1
DolínekČSSD	DolínekČSSD	k1gFnSc1
<g/>
34	#num#	k4
8661,612	8661,612	k4
<g/>
.2	.2	k4
<g/>
.	.	kIx.
</s>
<s>
Ropková	Ropková	k1gFnSc1
<g/>
,	,	kIx,
IrenaIrena	IrenaIren	k2eAgFnSc1d1
RopkováČSSD	RopkováČSSD	k1gFnSc1
<g/>
34	#num#	k4
9181,613	9181,613	k4
<g/>
.3	.3	k4
<g/>
.	.	kIx.
</s>
<s>
Březina	Březina	k1gMnSc1
<g/>
,	,	kIx,
KarelKarel	KarelKarel	k1gMnSc1
BřezinaČSSD	BřezinaČSSD	k1gFnSc2
<g/>
34	#num#	k4
2341,584	2341,584	k4
<g/>
.4	.4	k4
<g/>
.	.	kIx.
</s>
<s>
Nováková	Nováková	k1gFnSc1
<g/>
,	,	kIx,
HanaHana	HanaHana	k1gFnSc1
NovákováČSSD	NovákováČSSD	k1gFnSc1
<g/>
34	#num#	k4
3051,585	3051,585	k4
<g/>
.5	.5	k4
<g/>
.	.	kIx.
</s>
<s>
Slezák	Slezák	k1gMnSc1
<g/>
,	,	kIx,
JanJan	JanJan	k1gMnSc1
SlezákČSSD	SlezákČSSD	k1gFnSc2
<g/>
34	#num#	k4
0	#num#	k4
<g/>
101,576.6	101,576.6	k4
<g/>
.	.	kIx.
</s>
<s>
Hodek	Hodek	k1gMnSc1
<g/>
,	,	kIx,
DanielDaniel	DanielDaniel	k1gMnSc1
HodekČSSD	HodekČSSD	k1gFnSc2
<g/>
34	#num#	k4
1151,577	1151,577	k4
<g/>
.7	.7	k4
<g/>
.	.	kIx.
</s>
<s>
Kaucký	Kaucký	k1gMnSc1
<g/>
,	,	kIx,
LukášLukáš	LukášLukáš	k1gMnSc1
KauckýČSSD	KauckýČSSD	k1gFnSc2
<g/>
34	#num#	k4
3871,598	3871,598	k4
<g/>
.8	.8	k4
<g/>
.	.	kIx.
</s>
<s>
Semelová	Semelová	k1gFnSc1
<g/>
,	,	kIx,
MartaMarta	MartaMarta	k1gFnSc1
SemelováKSČM	SemelováKSČM	k1gFnSc1
<g/>
20	#num#	k4
0	#num#	k4
<g/>
481,631.1	481,631.1	k4
<g/>
.	.	kIx.
</s>
<s>
Šimůnek	Šimůnek	k1gMnSc1
<g/>
,	,	kIx,
PetrPetr	PetrPetr	k1gMnSc1
ŠimůnekKSČM	ŠimůnekKSČM	k1gFnSc2
<g/>
19	#num#	k4
8551,622	8551,622	k4
<g/>
.2	.2	k4
<g/>
.	.	kIx.
</s>
<s>
Hrůza	Hrůza	k1gMnSc1
<g/>
,	,	kIx,
IvanIvan	IvanIvan	k1gMnSc1
HrůzaKSČM	HrůzaKSČM	k1gFnSc2
<g/>
19	#num#	k4
7601,613	7601,613	k4
<g/>
.3	.3	k4
<g/>
.	.	kIx.
</s>
<s>
Briardová	Briardová	k1gFnSc1
<g/>
,	,	kIx,
HelenaHelena	HelenaHelen	k2eAgFnSc1d1
BriardováKSČM	BriardováKSČM	k1gFnSc1
<g/>
19	#num#	k4
3821,584	3821,584	k4
<g/>
.4	.4	k4
<g/>
.	.	kIx.
</s>
<s>
Michálek	Michálek	k1gMnSc1
<g/>
,	,	kIx,
JakubJakub	JakubJakub	k1gMnSc1
MichálekPiráti	MichálekPirát	k1gMnPc1
<g/>
24	#num#	k4
4152,211	4152,211	k4
<g/>
.1	.1	k4
<g/>
.	.	kIx.
</s>
<s>
Profant	profant	k1gInSc1
<g/>
,	,	kIx,
OndřejOndřej	OndřejOndřej	k1gFnSc1
ProfantPiráti	ProfantPirát	k1gMnPc1
<g/>
20	#num#	k4
7651,882	7651,882	k4
<g/>
.2	.2	k4
<g/>
.	.	kIx.
</s>
<s>
Ferjenčík	Ferjenčík	k1gMnSc1
<g/>
,	,	kIx,
MikulášMikuláš	MikulášMikuláš	k1gMnSc1
FerjenčíkPiráti	FerjenčíkPirát	k1gMnPc1
<g/>
20	#num#	k4
3531,844	3531,844	k4
<g/>
.3	.3	k4
<g/>
.	.	kIx.
</s>
<s>
Zábranský	Zábranský	k1gMnSc1
<g/>
,	,	kIx,
AdamAdam	AdamAdam	k1gInSc1
ZábranskýPiráti	ZábranskýPirát	k1gMnPc1
<g/>
19	#num#	k4
5801,773	5801,773	k4
<g/>
.4	.4	k4
<g/>
.	.	kIx.
</s>
<s>
Náhradníci	náhradník	k1gMnPc1
</s>
<s>
V	v	k7c6
tabulce	tabulka	k1gFnSc6
jsou	být	k5eAaImIp3nP
uvedeni	uvést	k5eAaPmNgMnP
první	první	k4xOgMnPc1
dva	dva	k4xCgMnPc1
náhradníci	náhradník	k1gMnPc1
za	za	k7c4
každou	každý	k3xTgFnSc4
kandidátku	kandidátka	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
získala	získat	k5eAaPmAgFnS
mandáty	mandát	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Jméno	jméno	k1gNnSc1
a	a	k8xC
příjmení	příjmení	k1gNnSc1
</s>
<s>
Pořadí	pořadí	k1gNnSc1
</s>
<s>
Strana	strana	k1gFnSc1
</s>
<s>
Preferenční	preferenční	k2eAgInPc1d1
hlasy	hlas	k1gInPc1
</s>
<s>
na	na	k7c6
kandidátce	kandidátka	k1gFnSc6
</s>
<s>
výsledné	výsledný	k2eAgNnSc1d1
</s>
<s>
volební	volební	k2eAgInSc1d1
</s>
<s>
navrhující	navrhující	k2eAgNnSc4d1
</s>
<s>
příslušnost	příslušnost	k1gFnSc1
</s>
<s>
abs	abs	k?
<g/>
.	.	kIx.
</s>
<s>
v	v	k7c6
%	%	kIx~
</s>
<s>
Ondřej	Ondřej	k1gMnSc1
Prokop	Prokop	k1gMnSc1
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
ANO	ano	k9
2011	#num#	k4
</s>
<s>
ANO	ano	k9
2011	#num#	k4
</s>
<s>
ANO	ano	k9
2011	#num#	k4
</s>
<s>
70	#num#	k4
991	#num#	k4
</s>
<s>
1,55	1,55	k4
</s>
<s>
Filip	Filip	k1gMnSc1
Meluzín	Meluzín	k1gMnSc1
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
ANO	ano	k9
2011	#num#	k4
</s>
<s>
ANO	ano	k9
2011	#num#	k4
</s>
<s>
ANO	ano	k9
2011	#num#	k4
</s>
<s>
70	#num#	k4
968	#num#	k4
</s>
<s>
1,55	1,55	k4
</s>
<s>
Jiří	Jiří	k1gMnSc1
Pilař	Pilař	k1gMnSc1
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
TOP	topit	k5eAaImRp2nS
09	#num#	k4
</s>
<s>
TOP	topit	k5eAaImRp2nS
09	#num#	k4
</s>
<s>
TOP	topit	k5eAaImRp2nS
09	#num#	k4
</s>
<s>
65	#num#	k4
350	#num#	k4
</s>
<s>
1,57	1,57	k4
</s>
<s>
Radek	Radek	k1gMnSc1
Vondra	Vondra	k1gMnSc1
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
TOP	topit	k5eAaImRp2nS
09	#num#	k4
</s>
<s>
TOP	topit	k5eAaImRp2nS
09	#num#	k4
</s>
<s>
TOP	topit	k5eAaImRp2nS
09	#num#	k4
</s>
<s>
65	#num#	k4
184	#num#	k4
</s>
<s>
1,56	1,56	k4
</s>
<s>
Daniela	Daniela	k1gFnSc1
Rázková	Rázková	k1gFnSc1
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Trojkoalice	Trojkoalice	k1gFnSc1
</s>
<s>
KDU-ČSL	KDU-ČSL	k?
</s>
<s>
KDU-ČSL	KDU-ČSL	k?
</s>
<s>
37	#num#	k4
544	#num#	k4
</s>
<s>
1,61	1,61	k4
</s>
<s>
Zuzana	Zuzana	k1gFnSc1
Drhová	Drhová	k1gFnSc1
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Trojkoalice	Trojkoalice	k1gFnSc1
</s>
<s>
SZ	SZ	kA
</s>
<s>
SZ	SZ	kA
</s>
<s>
37	#num#	k4
801	#num#	k4
</s>
<s>
1,62	1,62	k4
</s>
<s>
František	František	k1gMnSc1
Ševít	Ševít	k1gMnSc1
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
ODS	ODS	kA
</s>
<s>
ODS	ODS	kA
</s>
<s>
ODS	ODS	kA
</s>
<s>
36	#num#	k4
010	#num#	k4
</s>
<s>
1,58	1,58	k4
</s>
<s>
Luděk	Luděk	k1gMnSc1
Fiala	Fiala	k1gMnSc1
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
ODS	ODS	kA
</s>
<s>
ODS	ODS	kA
</s>
<s>
ODS	ODS	kA
</s>
<s>
35	#num#	k4
869	#num#	k4
</s>
<s>
1,57	1,57	k4
</s>
<s>
Karel	Karel	k1gMnSc1
Šašek	Šašek	k1gMnSc1
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
ČSSD	ČSSD	kA
</s>
<s>
ČSSD	ČSSD	kA
</s>
<s>
ČSSD	ČSSD	kA
</s>
<s>
33	#num#	k4
994	#num#	k4
</s>
<s>
1,57	1,57	k4
</s>
<s>
Daniel	Daniel	k1gMnSc1
Štěpán	Štěpán	k1gMnSc1
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
ČSSD	ČSSD	kA
</s>
<s>
ČSSD	ČSSD	kA
</s>
<s>
ČSSD	ČSSD	kA
</s>
<s>
33	#num#	k4
916	#num#	k4
</s>
<s>
1,56	1,56	k4
</s>
<s>
Lubomír	Lubomír	k1gMnSc1
Ledl	Ledl	k1gMnSc1
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
KSČM	KSČM	kA
</s>
<s>
KSČM	KSČM	kA
</s>
<s>
SDS	SDS	kA
</s>
<s>
19	#num#	k4
478	#num#	k4
</s>
<s>
1,58	1,58	k4
</s>
<s>
Milan	Milan	k1gMnSc1
Macek	Macek	k1gMnSc1
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
KSČM	KSČM	kA
</s>
<s>
KSČM	KSČM	kA
</s>
<s>
KSČM	KSČM	kA
</s>
<s>
19	#num#	k4
683	#num#	k4
</s>
<s>
1,60	1,60	k4
</s>
<s>
Viktor	Viktor	k1gMnSc1
Mahrik	Mahrik	k1gMnSc1
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Piráti	pirát	k1gMnPc1
</s>
<s>
Piráti	pirát	k1gMnPc1
</s>
<s>
Pirátí	Pirátit	k5eAaPmIp3nS
</s>
<s>
18	#num#	k4
662	#num#	k4
</s>
<s>
1,69	1,69	k4
</s>
<s>
Ondřej	Ondřej	k1gMnSc1
Kallasch	Kallasch	k1gMnSc1
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Piráti	pirát	k1gMnPc1
</s>
<s>
Piráti	pirát	k1gMnPc1
</s>
<s>
Pirátí	Pirátit	k5eAaPmIp3nS
</s>
<s>
17	#num#	k4
948	#num#	k4
</s>
<s>
1,63	1,63	k4
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Celá	celý	k2eAgFnSc1d1
Praha	Praha	k1gFnSc1
jako	jako	k8xC,k8xS
jeden	jeden	k4xCgInSc1
volební	volební	k2eAgInSc1d1
obvod	obvod	k1gInSc1
<g/>
,	,	kIx,
rozhodli	rozhodnout	k5eAaPmAgMnP
zastupitelé	zastupitel	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Nová	Nová	k1gFnSc1
hnutí	hnutí	k1gNnSc2
<g/>
,	,	kIx,
staré	starý	k2eAgFnSc2d1
tváře	tvář	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podívejte	podívat	k5eAaPmRp2nP,k5eAaImRp2nP
se	se	k3xPyFc4
na	na	k7c4
volební	volební	k2eAgMnPc4d1
přeběhlíky	přeběhlík	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Volby	volba	k1gFnPc1
do	do	k7c2
zastupitelstev	zastupitelstvo	k1gNnPc2
obcí	obec	k1gFnPc2
10	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
–	–	k?
11	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
2014	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Navrhujícímiy	Navrhujícímia	k1gFnSc2
stranami	strana	k1gFnPc7
kandidátů	kandidát	k1gMnPc2
byli	být	k5eAaImAgMnP
SNK	SNK	kA
Evropští	evropský	k2eAgMnPc1d1
demokraté	demokrat	k1gMnPc1
a	a	k8xC
Spojení	spojený	k2eAgMnPc1d1
demokraté	demokrat	k1gMnPc1
-	-	kIx~
Sdružení	sdružení	k1gNnSc1
nezávislých	závislý	k2eNgInPc2d1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Výsledky	výsledek	k1gInPc4
voleb	volba	k1gFnPc2
dle	dle	k7c2
údajů	údaj	k1gInPc2
ČSÚ	ČSÚ	kA
na	na	k7c4
Volby	volba	k1gFnPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Výsledky	výsledek	k1gInPc1
voleb	volba	k1gFnPc2
na	na	k7c6
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Volby	volba	k1gFnPc1
do	do	k7c2
Zastupitelstva	zastupitelstvo	k1gNnSc2
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
</s>
<s>
1990	#num#	k4
•	•	k?
1994	#num#	k4
•	•	k?
1998	#num#	k4
•	•	k?
2002	#num#	k4
•	•	k?
2006	#num#	k4
•	•	k?
2010	#num#	k4
•	•	k?
2014	#num#	k4
•	•	k?
2018	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Praha	Praha	k1gFnSc1
|	|	kIx~
Politika	politika	k1gFnSc1
</s>
