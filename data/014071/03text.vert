<s>
Oganesson	Oganesson	k1gMnSc1
</s>
<s>
Oganesson	Oganesson	k1gMnSc1
</s>
<s>
předpokládaná	předpokládaný	k2eAgFnSc1d1
[	[	kIx(
<g/>
Rn	Rn	k1gFnSc1
<g/>
]	]	kIx)
5	#num#	k4
<g/>
f	f	k?
<g/>
14	#num#	k4
6	#num#	k4
<g/>
d	d	k?
<g/>
10	#num#	k4
7	#num#	k4
<g/>
s	s	k7c7
<g/>
2	#num#	k4
7	#num#	k4
<g/>
p	p	k?
<g/>
6	#num#	k4
<g/>
(	(	kIx(
<g/>
založeno	založit	k5eAaPmNgNnS
na	na	k7c6
radonu	radon	k1gInSc6
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
(	(	kIx(
<g/>
293	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Og	Og	kA
</s>
<s>
118	#num#	k4
</s>
<s>
↓	↓	k?
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
↓	↓	k?
</s>
<s>
Obecné	obecný	k2eAgNnSc1d1
</s>
<s>
Název	název	k1gInSc1
<g/>
,	,	kIx,
značka	značka	k1gFnSc1
<g/>
,	,	kIx,
číslo	číslo	k1gNnSc1
</s>
<s>
Oganesson	Oganesson	k1gMnSc1
<g/>
,	,	kIx,
Og	Og	k1gMnSc1
<g/>
,	,	kIx,
118	#num#	k4
</s>
<s>
Cizojazyčné	cizojazyčný	k2eAgInPc1d1
názvy	název	k1gInPc1
</s>
<s>
angl.	angl.	k?
Oganesson	Oganesson	k1gInSc1
</s>
<s>
Skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
7	#num#	k4
<g/>
.	.	kIx.
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
p	p	k?
</s>
<s>
Chemická	chemický	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
</s>
<s>
neznámé	známý	k2eNgNnSc1d1
</s>
<s>
Identifikace	identifikace	k1gFnSc1
</s>
<s>
Registrační	registrační	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
CAS	CAS	kA
</s>
<s>
54144-19-3	54144-19-3	k4
</s>
<s>
Atomové	atomový	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Relativní	relativní	k2eAgFnSc1d1
atomová	atomový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
</s>
<s>
vypočítaná	vypočítaný	k2eAgFnSc1d1
293,214	293,214	k4
<g/>
95	#num#	k4
<g/>
(	(	kIx(
<g/>
99	#num#	k4
<g/>
)	)	kIx)
pro	pro	k7c4
293	#num#	k4
<g/>
Og	Og	k1gFnPc2
</s>
<s>
Elektronová	elektronový	k2eAgFnSc1d1
konfigurace	konfigurace	k1gFnSc1
</s>
<s>
předpokládaná	předpokládaný	k2eAgFnSc1d1
[	[	kIx(
<g/>
Rn	Rn	k1gFnSc1
<g/>
]	]	kIx)
5	#num#	k4
<g/>
f	f	k?
<g/>
14	#num#	k4
6	#num#	k4
<g/>
d	d	k?
<g/>
10	#num#	k4
7	#num#	k4
<g/>
s	s	k7c7
<g/>
2	#num#	k4
7	#num#	k4
<g/>
p	p	k?
<g/>
6	#num#	k4
<g/>
(	(	kIx(
<g/>
založeno	založit	k5eAaPmNgNnS
na	na	k7c6
radonu	radon	k1gInSc6
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mechanické	mechanický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Skupenství	skupenství	k1gNnSc1
</s>
<s>
předpokládané	předpokládaný	k2eAgFnPc4d1
plynné	plynný	k2eAgFnPc4d1
</s>
<s>
Bezpečnost	bezpečnost	k1gFnSc1
</s>
<s>
Radioaktivní	radioaktivní	k2eAgFnSc1d1
</s>
<s>
Izotopy	izotop	k1gInPc1
</s>
<s>
I	i	k9
</s>
<s>
V	v	k7c6
(	(	kIx(
<g/>
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
S	s	k7c7
</s>
<s>
T	T	kA
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
E	E	kA
(	(	kIx(
<g/>
MeV	MeV	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
P	P	kA
</s>
<s>
294	#num#	k4
<g/>
Og	Og	k1gFnSc1
</s>
<s>
umělý	umělý	k2eAgInSc4d1
</s>
<s>
0	#num#	k4
<g/>
+	+	kIx~
</s>
<s>
1,8	1,8	k4
ms	ms	k?
</s>
<s>
α	α	k?
</s>
<s>
290	#num#	k4
<g/>
Lv	Lv	k1gFnSc1
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Není	být	k5eNaImIp3nS
<g/>
-li	-li	k?
uvedeno	uvést	k5eAaPmNgNnS
jinak	jinak	k6eAd1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
použityjednotky	použityjednotka	k1gFnPc1
SI	si	k1gNnSc2
a	a	k8xC
STP	STP	kA
(	(	kIx(
<g/>
25	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
100	#num#	k4
kPa	kPa	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Rn	Rn	k?
<g/>
⋏	⋏	k?
</s>
<s>
Tennessin	Tennessin	k1gMnSc1
≺	≺	k?
<g/>
Og	Og	k1gMnSc1
</s>
<s>
Oganesson	Oganesson	k1gNnSc1
(	(	kIx(
<g/>
chemická	chemický	k2eAgFnSc1d1
značka	značka	k1gFnSc1
Og	Og	k1gFnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
transuran	transuran	k1gInSc1
s	s	k7c7
protonovým	protonový	k2eAgNnSc7d1
číslem	číslo	k1gNnSc7
118	#num#	k4
(	(	kIx(
<g/>
18	#num#	k4
<g/>
.	.	kIx.
skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
7	#num#	k4
<g/>
.	.	kIx.
<g/>
(	(	kIx(
<g/>
Q	Q	kA
<g/>
)	)	kIx)
perioda	perioda	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c7
schválením	schválení	k1gNnSc7
tohoto	tento	k3xDgInSc2
názvu	název	k1gInSc2
se	se	k3xPyFc4
provizorně	provizorně	k6eAd1
označoval	označovat	k5eAaImAgInS
jako	jako	k9
Ununoktium	Ununoktium	k1gNnSc4
(	(	kIx(
<g/>
Uuo	Uuo	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Očekává	očekávat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
jeho	jeho	k3xOp3gFnPc1
vlastnosti	vlastnost	k1gFnPc1
budou	být	k5eAaImBp3nP
podobné	podobný	k2eAgFnPc1d1
jako	jako	k8xS,k8xC
vlastnosti	vlastnost	k1gFnPc1
lehčích	lehký	k2eAgInPc2d2
vzácných	vzácný	k2eAgInPc2d1
plynů	plyn	k1gInPc2
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
1	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
bude	být	k5eAaImBp3nS
to	ten	k3xDgNnSc1
tedy	tedy	k9
druhý	druhý	k4xOgInSc1
radioaktivní	radioaktivní	k2eAgInSc1d1
plyn	plyn	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Rozpadová	rozpadový	k2eAgFnSc1d1
řada	řada	k1gFnSc1
oganessonu	oganesson	k1gInSc2
–	–	k?
čísla	číslo	k1gNnPc1
udávají	udávat	k5eAaImIp3nP
poločas	poločas	k1gInSc4
a	a	k8xC
energii	energie	k1gFnSc4
rozpadu	rozpad	k1gInSc2
jednotlivých	jednotlivý	k2eAgMnPc2d1
členů	člen	k1gMnPc2
řady	řada	k1gFnSc2
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1999	#num#	k4
oznámili	oznámit	k5eAaPmAgMnP
vědci	vědec	k1gMnPc1
z	z	k7c2
Lawrence	Lawrenec	k1gMnSc2
Berkeley	Berkelea	k1gMnSc2
National	National	k1gMnSc2
Laboratory	Laborator	k1gInPc1
objev	objev	k1gInSc1
prvků	prvek	k1gInPc2
ununhexia	ununhexium	k1gNnSc2
(	(	kIx(
<g/>
dnes	dnes	k6eAd1
livermorium	livermorium	k1gNnSc1
<g/>
)	)	kIx)
a	a	k8xC
ununoctia	ununoctia	k1gFnSc1
(	(	kIx(
<g/>
článek	článek	k1gInSc1
o	o	k7c6
objevu	objev	k1gInSc6
byl	být	k5eAaImAgInS
publikován	publikovat	k5eAaBmNgInS
v	v	k7c6
časopise	časopis	k1gInSc6
Physical	Physical	k1gFnSc2
Review	Review	k1gFnSc2
Letters	Lettersa	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oganesson	Oganesson	k1gInSc1
byl	být	k5eAaImAgInS
vytvořen	vytvořit	k5eAaPmNgInS
v	v	k7c6
reakci	reakce	k1gFnSc6
</s>
<s>
8636	#num#	k4
Kr	Kr	k1gFnSc1
+	+	kIx~
20882	#num#	k4
Pb	Pb	k1gFnSc2
→	→	k?
293118	#num#	k4
Og	Og	k1gFnPc2
+	+	kIx~
10	#num#	k4
n	n	k0
</s>
<s>
a	a	k8xC
livermorium	livermorium	k1gNnSc1
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
rozpadem	rozpad	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
vědci	vědec	k1gMnPc1
svůj	svůj	k3xOyFgInSc4
objev	objev	k1gInSc4
na	na	k7c6
základě	základ	k1gInSc6
faktu	fakt	k1gInSc2
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
přípravu	příprava	k1gFnSc4
nepovedlo	povést	k5eNaPmAgNnS
potvrdit	potvrdit	k5eAaPmF
v	v	k7c6
jiné	jiný	k2eAgFnSc6d1
laboratoři	laboratoř	k1gFnSc6
<g/>
,	,	kIx,
stáhli	stáhnout	k5eAaPmAgMnP
zpět	zpět	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
červnu	červen	k1gInSc6
2002	#num#	k4
oznámil	oznámit	k5eAaPmAgMnS
vedoucí	vedoucí	k1gMnSc1
laboratoře	laboratoř	k1gFnSc2
<g/>
,	,	kIx,
že	že	k8xS
originální	originální	k2eAgFnPc1d1
práce	práce	k1gFnPc1
o	o	k7c6
objevu	objev	k1gInSc6
těchto	tento	k3xDgInPc2
dvou	dva	k4xCgInPc2
prvků	prvek	k1gInPc2
byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
na	na	k7c6
výzkumu	výzkum	k1gInSc6
Victora	Victor	k1gMnSc2
Ninova	Ninov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
oznámili	oznámit	k5eAaPmAgMnP
spolupracující	spolupracující	k2eAgInPc4d1
týmy	tým	k1gInPc4
vědců	vědec	k1gMnPc2
ze	z	k7c2
Spojeného	spojený	k2eAgInSc2d1
ústavu	ústav	k1gInSc2
jaderných	jaderný	k2eAgInPc2d1
výzkumů	výzkum	k1gInPc2
v	v	k7c6
Dubně	Dubna	k1gFnSc6
(	(	kIx(
<g/>
Rusko	Rusko	k1gNnSc1
<g/>
)	)	kIx)
a	a	k8xC
Lawrence	Lawrence	k1gFnSc1
Livermore	Livermor	k1gInSc5
National	National	k1gFnSc1
Laboratory	Laborator	k1gInPc1
(	(	kIx(
<g/>
Kalifornie	Kalifornie	k1gFnSc1
<g/>
,	,	kIx,
USA	USA	kA
<g/>
)	)	kIx)
objev	objev	k1gInSc1
3	#num#	k4
(	(	kIx(
<g/>
možná	možná	k9
4	#num#	k4
<g/>
)	)	kIx)
jader	jádro	k1gNnPc2
oganessonu	oganessona	k1gFnSc4
v	v	k7c4
reakci	reakce	k1gFnSc4
</s>
<s>
24998	#num#	k4
Cf	Cf	k1gFnSc1
+	+	kIx~
4820	#num#	k4
Ca	ca	kA
→	→	k?
294118	#num#	k4
Og	Og	k1gFnPc2
+	+	kIx~
3	#num#	k4
10	#num#	k4
n.	n.	k?
</s>
<s>
Oganesson	Oganesson	k1gInSc1
byl	být	k5eAaImAgInS
potvrzen	potvrdit	k5eAaPmNgInS
detekcí	detekce	k1gFnSc7
rozpadu	rozpad	k1gInSc2
alfa	alfa	k1gNnSc2
jeho	jeho	k3xOp3gNnPc2
jader	jádro	k1gNnPc2
na	na	k7c4
290116	#num#	k4
Lv	Lv	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
prosinci	prosinec	k1gInSc6
2015	#num#	k4
Mezinárodní	mezinárodní	k2eAgFnSc1d1
unie	unie	k1gFnSc1
pro	pro	k7c4
čistou	čistý	k2eAgFnSc4d1
a	a	k8xC
užitou	užitý	k2eAgFnSc4d1
chemii	chemie	k1gFnSc4
potvrdila	potvrdit	k5eAaPmAgFnS
splnění	splnění	k1gNnSc4
kritérií	kritérion	k1gNnPc2
pro	pro	k7c4
prokázání	prokázání	k1gNnSc4
objevu	objev	k1gInSc2
nového	nový	k2eAgInSc2d1
prvku	prvek	k1gInSc2
<g/>
,	,	kIx,
Uuo	Uuo	k1gFnSc1
uznala	uznat	k5eAaPmAgFnS
za	za	k7c4
objevené	objevený	k2eAgNnSc4d1
vědci	vědec	k1gMnSc6
ve	v	k7c6
spolupráci	spolupráce	k1gFnSc6
Dubna-Livermore	Dubna-Livermor	k1gInSc5
a	a	k8xC
vyzvala	vyzvat	k5eAaPmAgFnS
objevitele	objevitel	k1gMnSc4
k	k	k7c3
navržení	navržení	k1gNnSc3
konečného	konečný	k2eAgInSc2d1
názvu	název	k1gInSc2
a	a	k8xC
značky	značka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Konečným	konečný	k2eAgInSc7d1
návrhem	návrh	k1gInSc7
objevitelů	objevitel	k1gMnPc2
byl	být	k5eAaImAgInS
název	název	k1gInSc1
oganesson	oganesson	k1gInSc1
a	a	k8xC
značka	značka	k1gFnSc1
Og	Og	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvek	prvek	k1gInSc1
je	být	k5eAaImIp3nS
takto	takto	k6eAd1
pojmenován	pojmenovat	k5eAaPmNgMnS
na	na	k7c4
počest	počest	k1gFnSc4
ruského	ruský	k2eAgMnSc4d1
jaderného	jaderný	k2eAgMnSc4d1
vědce	vědec	k1gMnSc4
Jurije	Jurije	k1gFnSc2
Colakoviče	Colakovič	k1gMnSc4
Oganesjana	Oganesjan	k1gMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Název	název	k1gInSc1
je	být	k5eAaImIp3nS
v	v	k7c6
souladu	soulad	k1gInSc6
s	s	k7c7
názvoslovným	názvoslovný	k2eAgNnSc7d1
doporučením	doporučení	k1gNnSc7
IUPAC	IUPAC	kA
a	a	k8xC
ctí	ctít	k5eAaImIp3nS
tradiční	tradiční	k2eAgFnSc4d1
příponu	přípona	k1gFnSc4
vzácných	vzácný	k2eAgInPc2d1
plynů	plyn	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Tento	tento	k3xDgInSc1
návrh	návrh	k1gInSc1
konečného	konečný	k2eAgNnSc2d1
pojmenování	pojmenování	k1gNnSc2
předložila	předložit	k5eAaPmAgFnS
IUPAC	IUPAC	kA
v	v	k7c6
červnu	červen	k1gInSc6
2016	#num#	k4
k	k	k7c3
veřejné	veřejný	k2eAgFnSc3d1
diskusi	diskuse	k1gFnSc3
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
28	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2016	#num#	k4
schválila	schválit	k5eAaPmAgFnS
jako	jako	k9
konečné	konečný	k2eAgNnSc4d1
pojmenování	pojmenování	k1gNnSc4
a	a	k8xC
značku	značka	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Protože	protože	k8xS
byly	být	k5eAaImAgInP
současně	současně	k6eAd1
uznány	uznán	k2eAgInPc4d1
objevy	objev	k1gInPc4
prvků	prvek	k1gInPc2
nihonium	nihonium	k1gNnSc1
<g/>
,	,	kIx,
moscovium	moscovium	k1gNnSc1
a	a	k8xC
tennessin	tennessin	k1gInSc1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
již	již	k6eAd1
prokazatelně	prokazatelně	k6eAd1
objeveny	objevit	k5eAaPmNgInP
všechny	všechen	k3xTgInPc1
prvky	prvek	k1gInPc1
7	#num#	k4
<g/>
.	.	kIx.
periody	perioda	k1gFnSc2
periodické	periodický	k2eAgFnSc2d1
tabulky	tabulka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Předpokládané	předpokládaný	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Bez	bez	k7c2
ohledu	ohled	k1gInSc2
na	na	k7c4
nestabilitu	nestabilita	k1gFnSc4
způsobenou	způsobený	k2eAgFnSc7d1
radioaktivitou	radioaktivita	k1gFnSc7
očekávají	očekávat	k5eAaImIp3nP
vědci	vědec	k1gMnPc1
následující	následující	k2eAgFnSc2d1
vlastnosti	vlastnost	k1gFnSc2
<g/>
:	:	kIx,
</s>
<s>
Oganesson	Oganesson	k1gMnSc1
bude	být	k5eAaImBp3nS
reaktivnější	reaktivní	k2eAgMnSc1d2
než	než	k8xS
xenon	xenon	k1gInSc1
či	či	k8xC
radon	radon	k1gInSc1
a	a	k8xC
bude	být	k5eAaImBp3nS
tvořit	tvořit	k5eAaImF
stabilní	stabilní	k2eAgInPc4d1
oxidy	oxid	k1gInPc4
(	(	kIx(
<g/>
např.	např.	kA
OgO	OgO	k1gFnSc1
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
chloridy	chlorid	k1gInPc1
nebo	nebo	k8xC
fluoridy	fluorid	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
v	v	k7c6
důsledku	důsledek	k1gInSc6
své	svůj	k3xOyFgFnSc2
elektronové	elektronový	k2eAgFnSc2d1
konfigurace	konfigurace	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
<g/>
,	,	kIx,
ač	ač	k8xS
je	být	k5eAaImIp3nS
uzavřena	uzavřít	k5eAaPmNgFnS
stabilním	stabilní	k2eAgNnSc7d1
elektronovým	elektronový	k2eAgNnSc7d1
oktetem	okteto	k1gNnSc7
<g/>
,	,	kIx,
obsahuje	obsahovat	k5eAaImIp3nS
valenční	valenční	k2eAgFnSc4d1
sféru	sféra	k1gFnSc4
v	v	k7c6
nepoměrně	poměrně	k6eNd1
větší	veliký	k2eAgFnSc6d2
vzdálenosti	vzdálenost	k1gFnSc6
od	od	k7c2
jádra	jádro	k1gNnSc2
než	než	k8xS
je	být	k5eAaImIp3nS
tomu	ten	k3xDgNnSc3
u	u	k7c2
předchozího	předchozí	k2eAgInSc2d1
vzácného	vzácný	k2eAgInSc2d1
plynu	plyn	k1gInSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
zapříčiňuje	zapříčiňovat	k5eAaImIp3nS
menší	malý	k2eAgFnSc4d2
soudržnost	soudržnost	k1gFnSc4
jádra	jádro	k1gNnSc2
a	a	k8xC
obalu	obal	k1gInSc2
(	(	kIx(
<g/>
tím	ten	k3xDgInSc7
pádem	pád	k1gInSc7
i	i	k9
menší	malý	k2eAgFnSc4d2
ionizační	ionizační	k2eAgFnSc4d1
energii	energie	k1gFnSc4
pro	pro	k7c4
elektrony	elektron	k1gInPc4
ve	v	k7c6
valenční	valenční	k2eAgFnSc6d1
sféře	sféra	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vzhledem	vzhledem	k7c3
k	k	k7c3
odlišné	odlišný	k2eAgFnSc3d1
spin-orbitální	spin-orbitální	k2eAgFnSc3d1
vazbě	vazba	k1gFnSc3
a	a	k8xC
unikátní	unikátní	k2eAgFnSc3d1
struktuře	struktura	k1gFnSc3
obalu	obal	k1gInSc2
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
1	#num#	k4
<g/>
]	]	kIx)
se	se	k3xPyFc4
předpokládá	předpokládat	k5eAaImIp3nS
relativně	relativně	k6eAd1
vysoká	vysoký	k2eAgFnSc1d1
dipólová	dipólový	k2eAgFnSc1d1
polarizovatelnost	polarizovatelnost	k1gFnSc1
a	a	k8xC
pozitivní	pozitivní	k2eAgFnSc1d1
elektronová	elektronový	k2eAgFnSc1d1
afinita	afinita	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oproti	oproti	k7c3
lehčím	lehký	k2eAgMnPc3d2
vzácným	vzácný	k2eAgMnPc3d1
plynům	plyn	k1gInPc3
by	by	kYmCp3nS
měl	mít	k5eAaImAgInS
oganesson	oganesson	k1gInSc1
vykazovat	vykazovat	k5eAaImF
silnější	silný	k2eAgInSc4d2
van	van	k1gInSc4
der	drát	k5eAaImRp2nS
Waalsovy	Waalsův	k2eAgFnPc4d1
mezimolekulové	mezimolekulový	k2eAgFnPc4d1
vazby	vazba	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pokud	pokud	k8xS
by	by	kYmCp3nS
se	se	k3xPyFc4
oganesson	oganesson	k1gMnSc1
vyskytoval	vyskytovat	k5eAaImAgMnS
ve	v	k7c6
větším	veliký	k2eAgNnSc6d2
množství	množství	k1gNnSc6
v	v	k7c6
přírodě	příroda	k1gFnSc6
a	a	k8xC
pokud	pokud	k8xS
by	by	kYmCp3nS
tvořil	tvořit	k5eAaImAgInS
stabilní	stabilní	k2eAgInSc4d1
oxid	oxid	k1gInSc4
<g/>
,	,	kIx,
bude	být	k5eAaImBp3nS
se	se	k3xPyFc4
nacházet	nacházet	k5eAaImF
převážně	převážně	k6eAd1
jako	jako	k8xC,k8xS
oxidický	oxidický	k2eAgInSc1d1
minerál	minerál	k1gInSc1
a	a	k8xC
ne	ne	k9
jako	jako	k9
plyn	plyn	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Provedené	provedený	k2eAgInPc4d1
relativistické	relativistický	k2eAgInPc4d1
kvantově-mechanické	kvantově-mechanický	k2eAgInPc4d1
výpočty	výpočet	k1gInPc4
však	však	k9
ukazují	ukazovat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
oganesson	oganesson	k1gInSc1
má	mít	k5eAaImIp3nS
již	již	k6eAd1
natolik	natolik	k6eAd1
velké	velký	k2eAgNnSc4d1
protonové	protonový	k2eAgNnSc4d1
číslo	číslo	k1gNnSc4
<g/>
,	,	kIx,
že	že	k8xS
vzhledem	vzhledem	k7c3
k	k	k7c3
velikosti	velikost	k1gFnSc3
obalu	obal	k1gInSc2
začíná	začínat	k5eAaImIp3nS
ve	v	k7c6
spin-orbitální	spin-orbitální	k2eAgFnSc6d1
interakci	interakce	k1gFnSc6
převažovat	převažovat	k5eAaImF
nad	nad	k7c7
LS	LS	kA
vazbou	vazba	k1gFnSc7
vazba	vazba	k1gFnSc1
jj	jj	k?
a	a	k8xC
klasický	klasický	k2eAgInSc4d1
popis	popis	k1gInSc4
uspořádání	uspořádání	k1gNnSc2
obalu	obal	k1gInSc2
do	do	k7c2
slupek	slupka	k1gFnPc2
a	a	k8xC
orbitalů	orbital	k1gInPc2
se	se	k3xPyFc4
již	již	k6eAd1
nejeví	jevit	k5eNaImIp3nS
jako	jako	k9
korektní	korektní	k2eAgInSc4d1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
ale	ale	k8xC
že	že	k8xS
jeho	jeho	k3xOp3gFnSc1
struktura	struktura	k1gFnSc1
je	být	k5eAaImIp3nS
bližší	bližší	k1gNnSc4
Fermiho	Fermi	k1gMnSc2
elektronovému	elektronový	k2eAgInSc3d1
plynu	plyn	k1gInSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Očekávání	očekávání	k1gNnSc4
obdobných	obdobný	k2eAgFnPc2d1
vlastností	vlastnost	k1gFnPc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
u	u	k7c2
lehčích	lehký	k2eAgInPc2d2
vzácných	vzácný	k2eAgInPc2d1
plynů	plyn	k1gInPc2
<g/>
,	,	kIx,
proto	proto	k8xC
nemusí	muset	k5eNaImIp3nS
být	být	k5eAaImF
naplněno	naplnit	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
ŠTRAJBLOVÁ	ŠTRAJBLOVÁ	kA
<g/>
,	,	kIx,
Jana	Jana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktualita	aktualita	k1gFnSc1
z	z	k7c2
fyziky	fyzika	k1gFnSc2
<g/>
:	:	kIx,
Zapeklité	zapeklitý	k2eAgInPc1d1
atomy	atom	k1gInPc1
oganessonu	oganesson	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Články	článek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Matfyz	Matfyz	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Univerzita	univerzita	k1gFnSc1
Karlova	Karlův	k2eAgFnSc1d1
<g/>
,	,	kIx,
Matematicko-fyzikální	matematicko-fyzikální	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
<g/>
,	,	kIx,
23	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
BALL	BALL	kA
<g/>
,	,	kIx,
Philip	Philip	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Immense	Immense	k1gFnSc1
oganesson	oganesson	k1gMnSc1
projected	projected	k1gMnSc1
to	ten	k3xDgNnSc4
have	havat	k5eAaPmIp3nS
no	no	k9
electron	electron	k1gInSc1
shells	shellsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chemistry	Chemistr	k1gMnPc4
World	Worlda	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Royal	Royal	k1gInSc1
Society	societa	k1gFnSc2
of	of	k?
Chemistry	Chemistr	k1gMnPc7
<g/>
,	,	kIx,
9	#num#	k4
<g/>
.	.	kIx.
říjen	říjen	k1gInSc1
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
WILSON	WILSON	kA
<g/>
,	,	kIx,
Angela	Angela	k1gFnSc1
K.	K.	kA
Heaviest	Heaviest	k1gFnSc1
Element	element	k1gInSc1
Has	hasit	k5eAaImRp2nS
Unusual	Unusual	k1gMnSc1
Shell	Shell	k1gMnSc1
Structure	Structur	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Viewpoint	Viewpointa	k1gFnPc2
<g/>
,	,	kIx,
s.	s.	k?
1	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Physics	Physics	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
American	American	k1gInSc1
Physical	Physical	k1gFnSc2
Society	societa	k1gFnSc2
<g/>
,	,	kIx,
31	#num#	k4
<g/>
.	.	kIx.
leden	leden	k1gInSc4
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svazek	svazek	k1gInSc1
11	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
10	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
PDF	PDF	kA
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
JERABEK	JERABEK	kA
<g/>
,	,	kIx,
Paul	Paul	k1gMnSc1
<g/>
;	;	kIx,
SCHUETRUMPF	SCHUETRUMPF	kA
<g/>
,	,	kIx,
Bastian	Bastian	k1gMnSc1
<g/>
;	;	kIx,
SCHWERDTFEGER	SCHWERDTFEGER	kA
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
<g/>
;	;	kIx,
NAZAREWICZ	NAZAREWICZ	kA
<g/>
,	,	kIx,
Witold	Witold	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Electron	Electron	k1gInSc1
and	and	k?
Nucleon	Nucleon	k1gInSc1
Localization	Localization	k1gInSc1
Functions	Functions	k1gInSc1
of	of	k?
Oganesson	Oganesson	k1gInSc1
<g/>
:	:	kIx,
Approaching	Approaching	k1gInSc1
the	the	k?
Fermi-Gas	Fermi-Gas	k1gInSc1
Limit	limit	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
NDS	NDS	kA
ENSDF	ENSDF	kA
<g/>
.	.	kIx.
www-nds	www-nds	k1gInSc1
<g/>
.	.	kIx.
<g/>
iaea	iaea	k1gFnSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Discovery	Discovera	k1gFnSc2
and	and	k?
Assignment	Assignment	k1gInSc1
of	of	k?
Elements	Elements	k1gInSc1
with	with	k1gMnSc1
Atomic	Atomic	k1gMnSc1
Numbers	Numbers	k1gInSc4
113	#num#	k4
<g/>
,	,	kIx,
115	#num#	k4
<g/>
,	,	kIx,
117	#num#	k4
and	and	k?
118	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
IUPAC	IUPAC	kA
Latest	Latest	k1gInSc1
News	News	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
30	#num#	k4
<g/>
.	.	kIx.
prosinec	prosinec	k1gInSc1
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
PDF	PDF	kA
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
se	se	k3xPyFc4
rozrostla	rozrůst	k5eAaPmAgFnS
o	o	k7c4
čtyři	čtyři	k4xCgInPc4
nové	nový	k2eAgInPc4d1
prvky	prvek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Týden	týden	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
EMPRESA	EMPRESA	kA
MEDIA	medium	k1gNnSc2
<g/>
,	,	kIx,
4	#num#	k4
<g/>
.	.	kIx.
leden	leden	k1gInSc4
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
IUPAC	IUPAC	kA
News	Newsa	k1gFnPc2
<g/>
:	:	kIx,
IUPAC	IUPAC	kA
is	is	k?
naming	naming	k1gInSc1
the	the	k?
four	four	k1gInSc1
new	new	k?
elements	elements	k6eAd1
nihonium	nihonium	k1gNnSc1
<g/>
,	,	kIx,
moscovium	moscovium	k1gNnSc1
<g/>
,	,	kIx,
tennessine	tennessinout	k5eAaPmIp3nS
<g/>
,	,	kIx,
and	and	k?
oganesson	oganesson	k1gInSc1
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
červen	červen	k1gInSc1
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Moskva	Moskva	k1gFnSc1
<g/>
,	,	kIx,
Japonsko	Japonsko	k1gNnSc1
<g/>
,	,	kIx,
Tennessee	Tennessee	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nové	Nové	k2eAgInPc1d1
chemické	chemický	k2eAgInPc1d1
prvky	prvek	k1gInPc1
obohatí	obohatit	k5eAaPmIp3nP
tabulku	tabulka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Týden	týden	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
EMPRESA	EMPRESA	kA
MEDIA	medium	k1gNnSc2
<g/>
,	,	kIx,
9	#num#	k4
<g/>
.	.	kIx.
červen	červen	k1gInSc1
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
IUPAC	IUPAC	kA
Recommendations	Recommendations	k1gInSc1
<g/>
:	:	kIx,
How	How	k1gMnSc1
to	ten	k3xDgNnSc4
name	name	k6eAd1
new	new	k?
chemical	chemicat	k5eAaPmAgInS
elements	elements	k1gInSc1
<g/>
.	.	kIx.
29	#num#	k4
<g/>
.	.	kIx.
únor	únor	k1gInSc4
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
IUPAC	IUPAC	kA
News	News	k1gInSc1
<g/>
:	:	kIx,
IUPAC	IUPAC	kA
Announces	Announces	k1gMnSc1
the	the	k?
Names	Names	k1gMnSc1
of	of	k?
the	the	k?
Elements	Elements	k1gInSc1
113	#num#	k4
<g/>
,	,	kIx,
115	#num#	k4
<g/>
,	,	kIx,
117	#num#	k4
<g/>
,	,	kIx,
and	and	k?
118	#num#	k4
<g/>
.	.	kIx.
30	#num#	k4
<g/>
.	.	kIx.
listopad	listopad	k1gInSc1
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Periodic	Periodic	k1gMnSc1
Table	tablo	k1gNnSc6
<g/>
’	’	k?
<g/>
s	s	k7c7
7	#num#	k4
<g/>
th	th	k?
Period	perioda	k1gFnPc2
is	is	k?
Finally	Finalla	k1gFnSc2
Complete	Comple	k1gNnSc2
<g/>
,	,	kIx,
IUPAC-IUPAP	IUPAC-IUPAP	k1gMnSc1
Officials	Officialsa	k1gFnPc2
Say	Say	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sci-News	Sci-News	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
5	#num#	k4
<g/>
.	.	kIx.
leden	leden	k1gInSc4
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Vzácné	vzácný	k2eAgInPc1d1
plyny	plyn	k1gInPc1
</s>
<s>
Jaderná	jaderný	k2eAgFnSc1d1
fyzika	fyzika	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
oganesson	oganessona	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Galerie	galerie	k1gFnSc1
oganesson	oganessona	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Possible	Possible	k6eAd1
New	New	k1gFnSc1
Element	element	k1gInSc4
Could	Could	k1gInSc1
Rewrite	Rewrit	k1gInSc5
Textbooks	Textbooks	k1gInSc1
<g/>
,	,	kIx,
FOX	fox	k1gInSc1
News	Newsa	k1gFnPc2
<g/>
,	,	kIx,
(	(	kIx(
<g/>
česky	česky	k6eAd1
zde	zde	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
prvků	prvek	k1gInPc2
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
17	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
H	H	kA
</s>
<s>
He	he	k0
</s>
<s>
Li	li	k8xS
</s>
<s>
Be	Be	k?
</s>
<s>
B	B	kA
</s>
<s>
C	C	kA
</s>
<s>
N	N	kA
</s>
<s>
O	o	k7c6
</s>
<s>
F	F	kA
</s>
<s>
Ne	ne	k9
</s>
<s>
Na	na	k7c6
</s>
<s>
Mg	mg	kA
</s>
<s>
Al	ala	k1gFnPc2
</s>
<s>
Si	se	k3xPyFc3
</s>
<s>
P	P	kA
</s>
<s>
S	s	k7c7
</s>
<s>
Cl	Cl	k?
</s>
<s>
Ar	ar	k1gInSc1
</s>
<s>
K	k	k7c3
</s>
<s>
Ca	ca	kA
</s>
<s>
Sc	Sc	k?
</s>
<s>
Ti	ten	k3xDgMnPc1
</s>
<s>
V	v	k7c6
</s>
<s>
Cr	cr	k0
</s>
<s>
Mn	Mn	k?
</s>
<s>
Fe	Fe	k?
</s>
<s>
Co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
</s>
<s>
Ni	on	k3xPp3gFnSc4
</s>
<s>
Cu	Cu	k?
</s>
<s>
Zn	zn	kA
</s>
<s>
Ga	Ga	k?
</s>
<s>
Ge	Ge	k?
</s>
<s>
As	as	k9
</s>
<s>
Se	s	k7c7
</s>
<s>
Br	br	k0
</s>
<s>
Kr	Kr	k?
</s>
<s>
Rb	Rb	k?
</s>
<s>
Sr	Sr	k?
</s>
<s>
Y	Y	kA
</s>
<s>
Zr	Zr	k?
</s>
<s>
Nb	Nb	k?
</s>
<s>
Mo	Mo	k?
</s>
<s>
Tc	tc	k0
</s>
<s>
Ru	Ru	k?
</s>
<s>
Rh	Rh	k?
</s>
<s>
Pd	Pd	k?
</s>
<s>
Ag	Ag	k?
</s>
<s>
Cd	cd	kA
</s>
<s>
In	In	k?
</s>
<s>
Sn	Sn	k?
</s>
<s>
Sb	sb	kA
</s>
<s>
Te	Te	k?
</s>
<s>
I	i	k9
</s>
<s>
Xe	Xe	k?
</s>
<s>
Cs	Cs	k?
</s>
<s>
Ba	ba	k9
</s>
<s>
La	la	k1gNnSc1
</s>
<s>
Ce	Ce	k?
</s>
<s>
Pr	pr	k0
</s>
<s>
Nd	Nd	k?
</s>
<s>
Pm	Pm	k?
</s>
<s>
Sm	Sm	k?
</s>
<s>
Eu	Eu	k?
</s>
<s>
Gd	Gd	k?
</s>
<s>
Tb	Tb	k?
</s>
<s>
Dy	Dy	k?
</s>
<s>
Ho	on	k3xPp3gNnSc4
</s>
<s>
Er	Er	k?
</s>
<s>
Tm	Tm	k?
</s>
<s>
Yb	Yb	k?
</s>
<s>
Lu	Lu	k?
</s>
<s>
Hf	Hf	k?
</s>
<s>
Ta	ten	k3xDgFnSc1
</s>
<s>
W	W	kA
</s>
<s>
Re	re	k9
</s>
<s>
Os	osa	k1gFnPc2
</s>
<s>
Ir	Ir	k1gMnSc1
</s>
<s>
Pt	Pt	k?
</s>
<s>
Au	au	k0
</s>
<s>
Hg	Hg	k?
</s>
<s>
Tl	Tl	k?
</s>
<s>
Pb	Pb	k?
</s>
<s>
Bi	Bi	k?
</s>
<s>
Po	po	k7c6
</s>
<s>
At	At	k?
</s>
<s>
Rn	Rn	k?
</s>
<s>
Fr	fr	k0
</s>
<s>
Ra	ra	k0
</s>
<s>
Ac	Ac	k?
</s>
<s>
Th	Th	k?
</s>
<s>
Pa	Pa	kA
</s>
<s>
U	u	k7c2
</s>
<s>
Np	Np	k?
</s>
<s>
Pu	Pu	k?
</s>
<s>
Am	Am	k?
</s>
<s>
Cm	cm	kA
</s>
<s>
Bk	Bk	k?
</s>
<s>
Cf	Cf	k?
</s>
<s>
Es	es	k1gNnSc1
</s>
<s>
Fm	Fm	k?
</s>
<s>
Md	Md	k?
</s>
<s>
No	no	k9
</s>
<s>
Lr	Lr	k?
</s>
<s>
Rf	Rf	k?
</s>
<s>
Db	db	kA
</s>
<s>
Sg	Sg	k?
</s>
<s>
Bh	Bh	k?
</s>
<s>
Hs	Hs	k?
</s>
<s>
Mt	Mt	k?
</s>
<s>
Ds	Ds	k?
</s>
<s>
Rg	Rg	k?
</s>
<s>
Cn	Cn	k?
</s>
<s>
Nh	Nh	k?
</s>
<s>
Fl	Fl	k?
</s>
<s>
Mc	Mc	k?
</s>
<s>
Lv	Lv	k?
</s>
<s>
Ts	ts	k0
</s>
<s>
Og	Og	k?
</s>
<s>
Alkalické	alkalický	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Kovy	kov	k1gInPc1
alkalických	alkalický	k2eAgFnPc2d1
zemin	zemina	k1gFnPc2
</s>
<s>
Lanthanoidy	Lanthanoida	k1gFnPc1
</s>
<s>
Aktinoidy	Aktinoida	k1gFnPc1
</s>
<s>
Přechodné	přechodný	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Nepřechodné	přechodný	k2eNgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Polokovy	Polokův	k2eAgFnPc1d1
</s>
<s>
Nekovy	nekov	k1gInPc1
</s>
<s>
Halogeny	halogen	k1gInPc1
</s>
<s>
Vzácné	vzácný	k2eAgInPc1d1
plyny	plyn	k1gInPc1
</s>
<s>
neznámé	známý	k2eNgNnSc1d1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Chemie	chemie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
2012003545	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
2012003545	#num#	k4
</s>
