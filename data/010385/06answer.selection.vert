<s>
Bilbo	Bilba	k1gMnSc5	Bilba
Pytlík	pytlík	k1gMnSc1	pytlík
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Bilbo	Bilba	k1gFnSc5	Bilba
Baggins	Baggins	k1gInSc4	Baggins
<g/>
,	,	kIx,	,
v	v	k7c6	v
hobitštině	hobitština	k1gFnSc6	hobitština
Bilba	Bilb	k1gMnSc2	Bilb
Labingi	Labing	k1gFnSc2	Labing
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
fiktivní	fiktivní	k2eAgFnSc1d1	fiktivní
postava	postava	k1gFnSc1	postava
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgMnSc1d1	hlavní
představitel	představitel	k1gMnSc1	představitel
románu	román	k1gInSc2	román
Hobit	hobit	k1gMnSc1	hobit
od	od	k7c2	od
J.	J.	kA	J.
R.	R.	kA	R.
R.	R.	kA	R.
Tolkiena	Tolkiena	k1gFnSc1	Tolkiena
a	a	k8xC	a
vedlejší	vedlejší	k2eAgFnSc1d1	vedlejší
postava	postava	k1gFnSc1	postava
navazující	navazující	k2eAgFnSc2d1	navazující
trilogie	trilogie	k1gFnSc2	trilogie
Pán	pán	k1gMnSc1	pán
prstenů	prsten	k1gInPc2	prsten
<g/>
.	.	kIx.	.
</s>
