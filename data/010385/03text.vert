<p>
<s>
Bilbo	Bilba	k1gMnSc5	Bilba
Pytlík	pytlík	k1gMnSc1	pytlík
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Bilbo	Bilba	k1gFnSc5	Bilba
Baggins	Baggins	k1gInSc4	Baggins
<g/>
,	,	kIx,	,
v	v	k7c6	v
hobitštině	hobitština	k1gFnSc6	hobitština
Bilba	Bilb	k1gMnSc2	Bilb
Labingi	Labing	k1gFnSc2	Labing
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
fiktivní	fiktivní	k2eAgFnSc1d1	fiktivní
postava	postava	k1gFnSc1	postava
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgMnSc1d1	hlavní
představitel	představitel	k1gMnSc1	představitel
románu	román	k1gInSc2	román
Hobit	hobit	k1gMnSc1	hobit
od	od	k7c2	od
J.	J.	kA	J.
R.	R.	kA	R.
R.	R.	kA	R.
Tolkiena	Tolkiena	k1gFnSc1	Tolkiena
a	a	k8xC	a
vedlejší	vedlejší	k2eAgFnSc1d1	vedlejší
postava	postava	k1gFnSc1	postava
navazující	navazující	k2eAgFnSc2d1	navazující
trilogie	trilogie	k1gFnSc2	trilogie
Pán	pán	k1gMnSc1	pán
prstenů	prsten	k1gInPc2	prsten
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
rasy	rasa	k1gFnSc2	rasa
hobitů	hobit	k1gMnPc2	hobit
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
nositelem	nositel	k1gMnSc7	nositel
prstenu	prsten	k1gInSc2	prsten
ve	v	k7c6	v
Středozemi	Středozem	k1gFnSc6	Středozem
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
Jednoho	jeden	k4xCgInSc2	jeden
prstenu	prsten	k1gInSc2	prsten
vzdal	vzdát	k5eAaPmAgMnS	vzdát
dobrovolně	dobrovolně	k6eAd1	dobrovolně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Bilbo	Bilba	k1gMnSc5	Bilba
Pytlík	pytlík	k1gMnSc1	pytlík
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
roku	rok	k1gInSc2	rok
2890	[number]	k4	2890
Třetího	třetí	k4xOgInSc2	třetí
věku	věk	k1gInSc2	věk
jako	jako	k9	jako
jediný	jediný	k2eAgMnSc1d1	jediný
potomek	potomek	k1gMnSc1	potomek
Bungo	Bungo	k1gNnSc4	Bungo
Pytlíka	pytlík	k1gMnSc2	pytlík
a	a	k8xC	a
Belladony	Belladona	k1gFnSc2	Belladona
Bralové	Bralová	k1gFnSc2	Bralová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hobit	hobit	k1gMnSc1	hobit
aneb	aneb	k?	aneb
cesta	cesta	k1gFnSc1	cesta
tam	tam	k6eAd1	tam
a	a	k8xC	a
zase	zase	k9	zase
zpátky	zpátky	k6eAd1	zpátky
===	===	k?	===
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
píše	psát	k5eAaImIp3nS	psát
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Hobit	hobit	k1gMnSc1	hobit
<g/>
,	,	kIx,	,
aneb	aneb	k?	aneb
cesta	cesta	k1gFnSc1	cesta
tam	tam	k6eAd1	tam
a	a	k8xC	a
zase	zase	k9	zase
zpátky	zpátky	k6eAd1	zpátky
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
upřesněno	upřesnit	k5eAaPmNgNnS	upřesnit
v	v	k7c6	v
trilogii	trilogie	k1gFnSc6	trilogie
Pán	pán	k1gMnSc1	pán
Prstenů	prsten	k1gInPc2	prsten
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2941	[number]	k4	2941
Třetího	třetí	k4xOgInSc2	třetí
věku	věk	k1gInSc2	věk
Bilba	Bilb	k1gMnSc2	Bilb
společně	společně	k6eAd1	společně
navštívili	navštívit	k5eAaPmAgMnP	navštívit
Gandalf	Gandalf	k1gInSc4	Gandalf
a	a	k8xC	a
13	[number]	k4	13
trpaslíků	trpaslík	k1gMnPc2	trpaslík
<g/>
,	,	kIx,	,
vedených	vedený	k2eAgFnPc2d1	vedená
Thorinem	Thorin	k1gInSc7	Thorin
Pavézou	pavéza	k1gFnSc7	pavéza
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
Bilba	Bilb	k1gMnSc4	Bilb
přesvědčili	přesvědčit	k5eAaPmAgMnP	přesvědčit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
šel	jít	k5eAaImAgMnS	jít
na	na	k7c4	na
výpravu	výprava	k1gFnSc4	výprava
k	k	k7c3	k
Osamělé	osamělý	k2eAgFnSc3d1	osamělá
hoře	hora	k1gFnSc3	hora
<g/>
.	.	kIx.	.
</s>
<s>
Trpaslíky	trpaslík	k1gMnPc4	trpaslík
pak	pak	k6eAd1	pak
zachránil	zachránit	k5eAaPmAgInS	zachránit
hned	hned	k6eAd1	hned
z	z	k7c2	z
několika	několik	k4yIc2	několik
situací	situace	k1gFnPc2	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jim	on	k3xPp3gMnPc3	on
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
jejich	jejich	k3xOp3gFnSc2	jejich
cesty	cesta	k1gFnSc2	cesta
se	se	k3xPyFc4	se
Bilbo	Bilba	k1gFnSc5	Bilba
ztratil	ztratit	k5eAaPmAgInS	ztratit
v	v	k7c6	v
Mlžných	mlžný	k2eAgFnPc6d1	mlžná
horách	hora	k1gFnPc6	hora
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
až	až	k9	až
do	do	k7c2	do
jeskyně	jeskyně	k1gFnSc2	jeskyně
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
potmě	potmě	k6eAd1	potmě
nahmatal	nahmatat	k5eAaPmAgInS	nahmatat
Jeden	jeden	k4xCgInSc1	jeden
prsten	prsten	k1gInSc1	prsten
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
patřil	patřit	k5eAaImAgInS	patřit
stvoření	stvoření	k1gNnSc4	stvoření
zvanému	zvaný	k2eAgInSc3d1	zvaný
Glum	Glum	k1gInSc1	Glum
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
Bilbo	Bilba	k1gFnSc5	Bilba
potkal	potkat	k5eAaPmAgInS	potkat
i	i	k9	i
samotného	samotný	k2eAgMnSc4d1	samotný
Gluma	Glum	k1gMnSc4	Glum
a	a	k8xC	a
uzavřeli	uzavřít	k5eAaPmAgMnP	uzavřít
dohodu	dohoda	k1gFnSc4	dohoda
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
Bilbo	Bilba	k1gFnSc5	Bilba
vyhraje	vyhrát	k5eAaPmIp3nS	vyhrát
nad	nad	k7c7	nad
Glumem	Glum	k1gInSc7	Glum
v	v	k7c6	v
pokládání	pokládání	k1gNnSc6	pokládání
hádanek	hádanka	k1gFnPc2	hádanka
<g/>
,	,	kIx,	,
Glum	Glum	k1gInSc1	Glum
ho	on	k3xPp3gMnSc4	on
vyvede	vyvést	k5eAaPmIp3nS	vyvést
z	z	k7c2	z
hor.	hor.	k?	hor.
Pokud	pokud	k8xS	pokud
vyhraje	vyhrát	k5eAaPmIp3nS	vyhrát
Glum	Glum	k1gInSc4	Glum
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
Bilba	Bilba	k1gMnSc1	Bilba
sežrat	sežrat	k5eAaPmF	sežrat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
hádanek	hádanka	k1gFnPc2	hádanka
hobit	hobit	k1gMnSc1	hobit
také	také	k9	také
vyzradil	vyzradit	k5eAaPmAgMnS	vyzradit
své	svůj	k3xOyFgNnSc4	svůj
jméno	jméno	k1gNnSc4	jméno
a	a	k8xC	a
jméno	jméno	k1gNnSc4	jméno
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
pochází	pocházet	k5eAaImIp3nS	pocházet
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mělo	mít	k5eAaImAgNnS	mít
mnohem	mnohem	k6eAd1	mnohem
později	pozdě	k6eAd2	pozdě
dohru	dohra	k1gFnSc4	dohra
v	v	k7c6	v
trilogii	trilogie	k1gFnSc6	trilogie
Pán	pán	k1gMnSc1	pán
Prstenů	prsten	k1gInPc2	prsten
<g/>
.	.	kIx.	.
</s>
<s>
Bilbo	Bilba	k1gMnSc5	Bilba
nakonec	nakonec	k6eAd1	nakonec
Gluma	Glum	k1gMnSc4	Glum
přelstil	přelstít	k5eAaPmAgInS	přelstít
záludnou	záludný	k2eAgFnSc7d1	záludná
otázkou	otázka	k1gFnSc7	otázka
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
co	co	k3yInSc1	co
mám	mít	k5eAaImIp1nS	mít
v	v	k7c6	v
kapse	kapsa	k1gFnSc6	kapsa
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Glum	Glum	k1gMnSc1	Glum
dohodu	dohoda	k1gFnSc4	dohoda
nesplnil	splnit	k5eNaPmAgMnS	splnit
a	a	k8xC	a
odhodlal	odhodlat	k5eAaPmAgMnS	odhodlat
se	se	k3xPyFc4	se
na	na	k7c4	na
Bilba	Bilb	k1gMnSc4	Bilb
zaútočit	zaútočit	k5eAaPmF	zaútočit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ten	ten	k3xDgMnSc1	ten
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Prsten	prsten	k1gInSc1	prsten
propůjčuje	propůjčovat	k5eAaImIp3nS	propůjčovat
neviditelnost	neviditelnost	k1gFnSc4	neviditelnost
a	a	k8xC	a
sledováním	sledování	k1gNnSc7	sledování
Gluma	Glumum	k1gNnSc2	Glumum
nakonec	nakonec	k6eAd1	nakonec
cestu	cesta	k1gFnSc4	cesta
z	z	k7c2	z
bludiště	bludiště	k1gNnSc2	bludiště
pod	pod	k7c7	pod
Mlžnými	mlžný	k2eAgFnPc7d1	mlžná
horami	hora	k1gFnPc7	hora
našel	najít	k5eAaPmAgMnS	najít
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bilbo	Bilba	k1gMnSc5	Bilba
pak	pak	k6eAd1	pak
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
s	s	k7c7	s
trpaslíky	trpaslík	k1gMnPc7	trpaslík
dál	daleko	k6eAd2	daleko
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
se	se	k3xPyFc4	se
dostali	dostat	k5eAaPmAgMnP	dostat
do	do	k7c2	do
Temného	temný	k2eAgInSc2d1	temný
hvozdu	hvozd	k1gInSc2	hvozd
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
vysvobodil	vysvobodit	k5eAaPmAgMnS	vysvobodit
ze	z	k7c2	z
spárů	spár	k1gInPc2	spár
pavouků	pavouk	k1gMnPc2	pavouk
a	a	k8xC	a
lesních	lesní	k2eAgMnPc2d1	lesní
elfů	elf	k1gMnPc2	elf
a	a	k8xC	a
v	v	k7c6	v
sudech	sud	k1gInPc6	sud
se	se	k3xPyFc4	se
po	po	k7c6	po
řece	řeka	k1gFnSc6	řeka
dostali	dostat	k5eAaPmAgMnP	dostat
až	až	k9	až
do	do	k7c2	do
města	město	k1gNnSc2	město
pod	pod	k7c7	pod
Osamělou	osamělý	k2eAgFnSc7d1	osamělá
horou	hora	k1gFnSc7	hora
<g/>
.	.	kIx.	.
</s>
<s>
Drak	drak	k1gMnSc1	drak
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
v	v	k7c6	v
hoře	hora	k1gFnSc6	hora
sídlil	sídlit	k5eAaImAgInS	sídlit
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
zabit	zabít	k5eAaPmNgMnS	zabít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Bilbo	Bilba	k1gFnSc5	Bilba
byl	být	k5eAaImAgInS	být
zraněn	zranit	k5eAaPmNgMnS	zranit
v	v	k7c6	v
Bitvě	bitva	k1gFnSc6	bitva
pěti	pět	k4xCc2	pět
armád	armáda	k1gFnPc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
uzdravil	uzdravit	k5eAaPmAgInS	uzdravit
a	a	k8xC	a
v	v	k7c6	v
pořádku	pořádek	k1gInSc6	pořádek
vrátil	vrátit	k5eAaPmAgInS	vrátit
do	do	k7c2	do
Kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pán	pán	k1gMnSc1	pán
prstenů	prsten	k1gInPc2	prsten
===	===	k?	===
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
2989	[number]	k4	2989
Třetího	třetí	k4xOgInSc2	třetí
věku	věk	k1gInSc2	věk
si	se	k3xPyFc3	se
adoptoval	adoptovat	k5eAaPmAgMnS	adoptovat
Froda	Froda	k1gMnSc1	Froda
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
osiřel	osiřet	k5eAaPmAgMnS	osiřet
po	po	k7c6	po
nehodě	nehoda	k1gFnSc6	nehoda
svých	svůj	k3xOyFgMnPc2	svůj
rodičů	rodič	k1gMnPc2	rodič
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
Bilbo	Bilba	k1gFnSc5	Bilba
několikrát	několikrát	k6eAd1	několikrát
Prsten	prsten	k1gInSc1	prsten
použil	použít	k5eAaPmAgInS	použít
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
když	když	k8xS	když
se	se	k3xPyFc4	se
chtěl	chtít	k5eAaImAgMnS	chtít
skrýt	skrýt	k5eAaPmF	skrýt
před	před	k7c7	před
svými	svůj	k3xOyFgMnPc7	svůj
příbuznými	příbuzný	k1gMnPc7	příbuzný
<g/>
,	,	kIx,	,
Pytlíky	pytlík	k1gMnPc7	pytlík
ze	z	k7c2	z
Sáčkova	Sáčkov	k1gInSc2	Sáčkov
<g/>
.	.	kIx.	.
</s>
<s>
Vlastnil	vlastnit	k5eAaImAgMnS	vlastnit
jej	on	k3xPp3gMnSc4	on
po	po	k7c4	po
celých	celý	k2eAgNnPc2d1	celé
60	[number]	k4	60
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
tušil	tušit	k5eAaImAgMnS	tušit
<g/>
,	,	kIx,	,
co	co	k9	co
vlastně	vlastně	k9	vlastně
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
Prsten	prsten	k1gInSc4	prsten
je	být	k5eAaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ten	ten	k3xDgInSc1	ten
mu	on	k3xPp3gMnSc3	on
prodlužoval	prodlužovat	k5eAaImAgInS	prodlužovat
život	život	k1gInSc1	život
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Bilbo	Bilba	k1gFnSc5	Bilba
se	se	k3xPyFc4	se
na	na	k7c6	na
něm	on	k3xPp3gMnSc6	on
stával	stávat	k5eAaImAgInS	stávat
čím	co	k3yRnSc7	co
dál	daleko	k6eAd2	daleko
tím	ten	k3xDgNnSc7	ten
víc	hodně	k6eAd2	hodně
závislý	závislý	k2eAgInSc1d1	závislý
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
jej	on	k3xPp3gNnSc4	on
pořád	pořád	k6eAd1	pořád
u	u	k7c2	u
sebe	se	k3xPyFc2	se
v	v	k7c6	v
kapse	kapsa	k1gFnSc6	kapsa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
den	den	k1gInSc4	den
jeho	jeho	k3xOp3gNnSc2	jeho
111	[number]	k4	111
<g/>
.	.	kIx.	.
narozenin	narozeniny	k1gFnPc2	narozeniny
se	se	k3xPyFc4	se
však	však	k9	však
po	po	k7c6	po
Gandalfově	Gandalfův	k2eAgFnSc6d1	Gandalfova
výzvě	výzva	k1gFnSc6	výzva
Prstenu	prsten	k1gInSc2	prsten
vzdal	vzdát	k5eAaPmAgInS	vzdát
a	a	k8xC	a
nechal	nechat	k5eAaPmAgInS	nechat
ho	on	k3xPp3gMnSc4	on
Frodovi	Frodův	k2eAgMnPc1d1	Frodův
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
celé	celý	k2eAgNnSc1d1	celé
Dno	dno	k1gNnSc1	dno
Pytle	pytel	k1gInSc2	pytel
<g/>
.	.	kIx.	.
</s>
<s>
Odešel	odejít	k5eAaPmAgMnS	odejít
z	z	k7c2	z
Kraje	kraj	k1gInSc2	kraj
a	a	k8xC	a
víckrát	víckrát	k6eAd1	víckrát
ho	on	k3xPp3gMnSc4	on
zde	zde	k6eAd1	zde
nikdo	nikdo	k3yNnSc1	nikdo
nespatřil	spatřit	k5eNaPmAgMnS	spatřit
<g/>
.	.	kIx.	.
</s>
<s>
Odcestoval	odcestovat	k5eAaPmAgMnS	odcestovat
společně	společně	k6eAd1	společně
s	s	k7c7	s
třemi	tři	k4xCgMnPc7	tři
trpasličími	trpasličí	k2eAgMnPc7d1	trpasličí
přáteli	přítel	k1gMnPc7	přítel
do	do	k7c2	do
Roklinky	roklinka	k1gFnSc2	roklinka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
usadil	usadit	k5eAaPmAgInS	usadit
a	a	k8xC	a
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
o	o	k7c4	o
putování	putování	k1gNnSc4	putování
k	k	k7c3	k
Osamělé	osamělý	k2eAgFnSc3d1	osamělá
hoře	hora	k1gFnSc3	hora
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
píše	psát	k5eAaImIp3nS	psát
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
trilogie	trilogie	k1gFnSc2	trilogie
Pán	pán	k1gMnSc1	pán
Prstenů	prsten	k1gInPc2	prsten
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
3021	[number]	k4	3021
Třetího	třetí	k4xOgInSc2	třetí
věku	věk	k1gInSc2	věk
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgMnS	vydat
společně	společně	k6eAd1	společně
s	s	k7c7	s
Frodem	Frod	k1gInSc7	Frod
<g/>
,	,	kIx,	,
Gandalfem	Gandalf	k1gInSc7	Gandalf
<g/>
,	,	kIx,	,
Elrondem	Elrond	k1gInSc7	Elrond
a	a	k8xC	a
Galadriel	Galadriel	k1gInSc4	Galadriel
do	do	k7c2	do
Šedých	Šedých	k2eAgInPc2d1	Šedých
přístavů	přístav	k1gInPc2	přístav
a	a	k8xC	a
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
na	na	k7c4	na
loď	loď	k1gFnSc4	loď
do	do	k7c2	do
Valinoru	Valinor	k1gInSc2	Valinor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
poklidu	poklid	k1gInSc6	poklid
dožil	dožít	k5eAaPmAgMnS	dožít
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
také	také	k9	také
prohlášen	prohlásit	k5eAaPmNgMnS	prohlásit
druhým	druhý	k4xOgNnSc7	druhý
nejstarším	starý	k2eAgMnSc7d3	nejstarší
hobitem	hobit	k1gMnSc7	hobit
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Středozemi	Středozem	k1gFnSc6	Středozem
(	(	kIx(	(
<g/>
starší	starý	k2eAgMnSc1d2	starší
byl	být	k5eAaImAgMnS	být
pouze	pouze	k6eAd1	pouze
Sméagol-Glum	Sméagol-Glum	k1gInSc4	Sméagol-Glum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
