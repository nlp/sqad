<s>
Bratři	bratr	k1gMnPc1	bratr
Karamazovi	Karamazův	k2eAgMnPc1d1	Karamazův
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
také	také	k9	také
Bratři	bratr	k1gMnPc1	bratr
Karamazovovi	Karamazovův	k2eAgMnPc1d1	Karamazovův
<g/>
,	,	kIx,	,
rusky	rusky	k6eAd1	rusky
<g/>
:	:	kIx,	:
Б	Б	k?	Б
К	К	k?	К
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
sociálně	sociálně	k6eAd1	sociálně
filozofický	filozofický	k2eAgInSc4d1	filozofický
román	román	k1gInSc4	román
ruského	ruský	k2eAgMnSc2d1	ruský
spisovatele	spisovatel	k1gMnSc2	spisovatel
F.	F.	kA	F.
M.	M.	kA	M.
Dostojevského	Dostojevský	k2eAgMnSc4d1	Dostojevský
<g/>
,	,	kIx,	,
vydán	vydán	k2eAgInSc1d1	vydán
roku	rok	k1gInSc2	rok
1880	[number]	k4	1880
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
ruského	ruský	k2eAgInSc2d1	ruský
originálu	originál	k1gInSc2	originál
přeložili	přeložit	k5eAaPmAgMnP	přeložit
Břetislav	Břetislav	k1gMnSc1	Břetislav
Hůla	Hůla	k1gMnSc1	Hůla
a	a	k8xC	a
Prokop	Prokop	k1gMnSc1	Prokop
Voskovec	Voskovec	k1gMnSc1	Voskovec
<g/>
.	.	kIx.	.
</s>
<s>
Literárním	literární	k2eAgInSc7d1	literární
eposem	epos	k1gInSc7	epos
chtěl	chtít	k5eAaImAgInS	chtít
Dostojevský	Dostojevský	k2eAgInSc1d1	Dostojevský
přehodnotit	přehodnotit	k5eAaPmF	přehodnotit
své	svůj	k3xOyFgInPc4	svůj
dřívější	dřívější	k2eAgInPc4d1	dřívější
životní	životní	k2eAgInPc4d1	životní
postoje	postoj	k1gInPc4	postoj
a	a	k8xC	a
pohled	pohled	k1gInSc4	pohled
na	na	k7c4	na
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
tak	tak	k9	tak
jako	jako	k9	jako
na	na	k7c4	na
celou	celý	k2eAgFnSc4d1	celá
společnost	společnost	k1gFnSc4	společnost
včetně	včetně	k7c2	včetně
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
bylo	být	k5eAaImAgNnS	být
zamýšleno	zamýšlet	k5eAaImNgNnS	zamýšlet
jako	jako	k9	jako
dílo	dílo	k1gNnSc1	dílo
o	o	k7c6	o
pěti	pět	k4xCc6	pět
částech	část	k1gFnPc6	část
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc4	jenž
bylo	být	k5eAaImAgNnS	být
zprvu	zprvu	k6eAd1	zprvu
nazýváno	nazývat	k5eAaImNgNnS	nazývat
jako	jako	k8xS	jako
Atheismus	atheismus	k1gInSc1	atheismus
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Život	život	k1gInSc1	život
velikého	veliký	k2eAgMnSc2d1	veliký
hříšníka	hříšník	k1gMnSc2	hříšník
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
záměr	záměr	k1gInSc4	záměr
později	pozdě	k6eAd2	pozdě
pozměnil	pozměnit	k5eAaPmAgMnS	pozměnit
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
díly	díl	k1gInPc4	díl
<g/>
,	,	kIx,	,
oddělené	oddělený	k2eAgInPc4d1	oddělený
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
třinácti	třináct	k4xCc2	třináct
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Dokončil	dokončit	k5eAaPmAgMnS	dokončit
ale	ale	k9	ale
jen	jen	k9	jen
díl	díl	k1gInSc4	díl
první	první	k4xOgInSc4	první
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
obsahově	obsahově	k6eAd1	obsahově
i	i	k9	i
významově	významově	k6eAd1	významově
uzavřený	uzavřený	k2eAgInSc1d1	uzavřený
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
právě	právě	k6eAd1	právě
Bratry	bratr	k1gMnPc7	bratr
Karamazovy	Karamazův	k2eAgInPc1d1	Karamazův
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Bratři	bratr	k1gMnPc1	bratr
Dmitrij	Dmitrij	k1gMnSc1	Dmitrij
<g/>
,	,	kIx,	,
Aljoša	Aljoša	k1gMnSc1	Aljoša
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
tvoří	tvořit	k5eAaImIp3nS	tvořit
víceméně	víceméně	k9	víceméně
základní	základní	k2eAgInPc4d1	základní
lidské	lidský	k2eAgInPc4d1	lidský
archetypy	archetyp	k1gInPc4	archetyp
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
je	být	k5eAaImIp3nS	být
psán	psán	k2eAgInSc1d1	psán
v	v	k7c6	v
er-formě	erorma	k1gFnSc6	er-forma
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
nezaujatého	zaujatý	k2eNgMnSc2d1	nezaujatý
pozorovatele	pozorovatel	k1gMnSc2	pozorovatel
<g/>
,	,	kIx,	,
dílo	dílo	k1gNnSc1	dílo
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
filozofické	filozofický	k2eAgFnPc4d1	filozofická
úvahy	úvaha	k1gFnPc4	úvaha
<g/>
,	,	kIx,	,
detailní	detailní	k2eAgFnPc4d1	detailní
psychologické	psychologický	k2eAgFnPc4d1	psychologická
sondy	sonda	k1gFnPc4	sonda
a	a	k8xC	a
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
popisné	popisný	k2eAgFnPc4d1	popisná
pasáže	pasáž	k1gFnPc4	pasáž
<g/>
.	.	kIx.	.
</s>
<s>
Typické	typický	k2eAgNnSc1d1	typické
je	být	k5eAaImIp3nS	být
bohaté	bohatý	k2eAgNnSc1d1	bohaté
vyjadřování	vyjadřování	k1gNnSc1	vyjadřování
<g/>
.	.	kIx.	.
</s>
<s>
Jazykové	jazykový	k2eAgInPc1d1	jazykový
prostředky	prostředek	k1gInPc1	prostředek
jsou	být	k5eAaImIp3nP	být
u	u	k7c2	u
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
postav	postava	k1gFnPc2	postava
měněny	měněn	k2eAgFnPc1d1	měněna
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
přispívá	přispívat	k5eAaImIp3nS	přispívat
realističnosti	realističnost	k1gFnSc3	realističnost
<g/>
.	.	kIx.	.
</s>
<s>
Realistické	realistický	k2eAgNnSc1d1	realistické
vystižení	vystižení	k1gNnSc1	vystižení
života	život	k1gInSc2	život
v	v	k7c6	v
ruském	ruský	k2eAgNnSc6d1	ruské
maloměstě	maloměsto	k1gNnSc6	maloměsto
je	být	k5eAaImIp3nS	být
utvářeno	utvářen	k2eAgNnSc1d1	utvářen
především	především	k9	především
pomocí	pomocí	k7c2	pomocí
osob	osoba	k1gFnPc2	osoba
pocházejících	pocházející	k2eAgFnPc2d1	pocházející
z	z	k7c2	z
rozdílných	rozdílný	k2eAgFnPc2d1	rozdílná
společenských	společenský	k2eAgFnPc2d1	společenská
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Postavy	postava	k1gFnPc1	postava
jednají	jednat	k5eAaImIp3nP	jednat
většinou	většina	k1gFnSc7	většina
v	v	k7c6	v
afektu	afekt	k1gInSc6	afekt
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
unášeny	unášen	k2eAgFnPc1d1	unášena
emocemi	emoce	k1gFnPc7	emoce
<g/>
.	.	kIx.	.
</s>
<s>
Dmitrij	Dmitrít	k5eAaPmRp2nS	Dmitrít
Karamazov	Karamazov	k1gInSc1	Karamazov
(	(	kIx(	(
<g/>
Míťa	Míťa	k1gFnSc1	Míťa
<g/>
)	)	kIx)	)
–	–	k?	–
Nejstarší	starý	k2eAgInSc1d3	nejstarší
z	z	k7c2	z
bratrů	bratr	k1gMnPc2	bratr
<g/>
,	,	kIx,	,
mladý	mladý	k2eAgMnSc1d1	mladý
floutek	floutek	k1gMnSc1	floutek
<g/>
,	,	kIx,	,
důstojník	důstojník	k1gMnSc1	důstojník
<g/>
,	,	kIx,	,
rád	rád	k2eAgMnSc1d1	rád
hýří	hýřit	k5eAaImIp3nS	hýřit
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
hazard	hazard	k1gInSc1	hazard
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
velkým	velký	k2eAgMnSc7d1	velký
milovníkem	milovník	k1gMnSc7	milovník
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
mu	on	k3xPp3gMnSc3	on
už	už	k9	už
jeho	jeho	k3xOp3gInPc4	jeho
vysoké	vysoký	k2eAgInPc4d1	vysoký
náklady	náklad	k1gInPc4	náklad
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
proplácet	proplácet	k5eAaImF	proplácet
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yQnSc4	což
ho	on	k3xPp3gNnSc4	on
pokládá	pokládat	k5eAaImIp3nS	pokládat
za	za	k7c4	za
zloděje	zloděj	k1gMnPc4	zloděj
a	a	k8xC	a
vymáhá	vymáhat	k5eAaImIp3nS	vymáhat
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
doplatek	doplatek	k1gInSc1	doplatek
peněz	peníze	k1gInPc2	peníze
z	z	k7c2	z
dědictví	dědictví	k1gNnSc2	dědictví
po	po	k7c6	po
své	svůj	k3xOyFgFnSc6	svůj
matce	matka	k1gFnSc6	matka
<g/>
.	.	kIx.	.
</s>
<s>
Ivan	Ivan	k1gMnSc1	Ivan
Karamazov	Karamazovo	k1gNnPc2	Karamazovo
–	–	k?	–
Druhý	druhý	k4xOgMnSc1	druhý
bratr	bratr	k1gMnSc1	bratr
<g/>
,	,	kIx,	,
Dmitrijův	Dmitrijův	k2eAgInSc1d1	Dmitrijův
protiklad	protiklad	k1gInSc1	protiklad
<g/>
,	,	kIx,	,
cynik	cynik	k1gMnSc1	cynik
a	a	k8xC	a
intelektuál	intelektuál	k1gMnSc1	intelektuál
<g/>
,	,	kIx,	,
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
ateista	ateista	k1gMnSc1	ateista
sympatizující	sympatizující	k2eAgMnSc1d1	sympatizující
s	s	k7c7	s
nihilismem	nihilismus	k1gInSc7	nihilismus
<g/>
.	.	kIx.	.
</s>
<s>
Proslavil	proslavit	k5eAaPmAgMnS	proslavit
se	se	k3xPyFc4	se
několika	několik	k4yIc7	několik
studiemi	studie	k1gFnPc7	studie
o	o	k7c6	o
křesťanské	křesťanský	k2eAgFnSc6d1	křesťanská
víře	víra	k1gFnSc6	víra
a	a	k8xC	a
funkce	funkce	k1gFnSc2	funkce
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
nezájem	nezájem	k1gInSc4	nezájem
o	o	k7c4	o
druhé	druhý	k4xOgMnPc4	druhý
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
jen	jen	k9	jen
předstírá	předstírat	k5eAaImIp3nS	předstírat
<g/>
.	.	kIx.	.
</s>
<s>
Alexej	Alexej	k1gMnSc1	Alexej
Karamazov	Karamazov	k1gInSc1	Karamazov
(	(	kIx(	(
<g/>
Aljoša	Aljoša	k1gMnSc1	Aljoša
<g/>
)	)	kIx)	)
–	–	k?	–
Nejmladší	mladý	k2eAgMnSc1d3	nejmladší
z	z	k7c2	z
bratrů	bratr	k1gMnPc2	bratr
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
mírně	mírně	k6eAd1	mírně
naivní	naivní	k2eAgMnSc1d1	naivní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pokorný	pokorný	k2eAgInSc1d1	pokorný
dobrák	dobrák	k1gInSc1	dobrák
<g/>
,	,	kIx,	,
plný	plný	k2eAgInSc1d1	plný
lásky	láska	k1gFnSc2	láska
a	a	k8xC	a
sociálního	sociální	k2eAgNnSc2d1	sociální
pochopení	pochopení	k1gNnSc2	pochopení
<g/>
,	,	kIx,	,
v	v	k7c6	v
životě	život	k1gInSc6	život
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
slabým	slabý	k2eAgInSc7d1	slabý
a	a	k8xC	a
chudým	chudý	k2eAgInSc7d1	chudý
lidem	lid	k1gInSc7	lid
<g/>
.	.	kIx.	.
</s>
<s>
Plánuje	plánovat	k5eAaImIp3nS	plánovat
natrvalo	natrvalo	k6eAd1	natrvalo
vstoupit	vstoupit	k5eAaPmF	vstoupit
do	do	k7c2	do
kláštera	klášter	k1gInSc2	klášter
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
radu	rada	k1gFnSc4	rada
svého	svůj	k3xOyFgMnSc2	svůj
učitele	učitel	k1gMnSc2	učitel
<g/>
,	,	kIx,	,
Zosimy	Zosima	k1gFnSc2	Zosima
<g/>
,	,	kIx,	,
od	od	k7c2	od
záměru	záměr	k1gInSc2	záměr
upouští	upouštět	k5eAaImIp3nS	upouštět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
románu	román	k1gInSc6	román
je	být	k5eAaImIp3nS	být
dobrákem	dobrák	k1gInSc7	dobrák
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc4	jenž
smiřuje	smiřovat	k5eAaImIp3nS	smiřovat
a	a	k8xC	a
spojuje	spojovat	k5eAaImIp3nS	spojovat
rozhádané	rozhádaný	k2eAgMnPc4d1	rozhádaný
příbuzné	příbuzný	k1gMnPc4	příbuzný
<g/>
.	.	kIx.	.
</s>
<s>
Fjodor	Fjodor	k1gMnSc1	Fjodor
Pavlovič	Pavlovič	k1gMnSc1	Pavlovič
Karamazov	Karamazovo	k1gNnPc2	Karamazovo
–	–	k?	–
Otec	otec	k1gMnSc1	otec
představuje	představovat	k5eAaImIp3nS	představovat
nejhorší	zlý	k2eAgFnPc4d3	nejhorší
lidské	lidský	k2eAgFnPc4d1	lidská
vlastnosti	vlastnost	k1gFnPc4	vlastnost
hlavně	hlavně	k9	hlavně
vypočítavost	vypočítavost	k1gFnSc1	vypočítavost
<g/>
,	,	kIx,	,
zbabělost	zbabělost	k1gFnSc1	zbabělost
a	a	k8xC	a
šaškovitost	šaškovitost	k1gFnSc1	šaškovitost
<g/>
.	.	kIx.	.
</s>
<s>
Neuvědomuje	uvědomovat	k5eNaImIp3nS	uvědomovat
si	se	k3xPyFc3	se
příčiny	příčina	k1gFnPc4	příčina
a	a	k8xC	a
důsledky	důsledek	k1gInPc4	důsledek
svého	svůj	k3xOyFgNnSc2	svůj
jednání	jednání	k1gNnSc2	jednání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
každém	každý	k3xTgInSc6	každý
z	z	k7c2	z
jeho	jeho	k3xOp3gMnPc2	jeho
synů	syn	k1gMnPc2	syn
lze	lze	k6eAd1	lze
vidět	vidět	k5eAaImF	vidět
určité	určitý	k2eAgNnSc4d1	určité
"	"	kIx"	"
<g/>
karamazovství	karamazovství	k1gNnSc4	karamazovství
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
–	–	k?	–
ať	ať	k9	ať
už	už	k6eAd1	už
více	hodně	k6eAd2	hodně
či	či	k8xC	či
méně	málo	k6eAd2	málo
–	–	k?	–
zdědili	zdědit	k5eAaPmAgMnP	zdědit
<g/>
.	.	kIx.	.
</s>
<s>
Smerďakov	Smerďakov	k1gInSc4	Smerďakov
–	–	k?	–
Karamazovův	Karamazovův	k2eAgMnSc1d1	Karamazovův
sluha	sluha	k1gMnSc1	sluha
<g/>
,	,	kIx,	,
kuchař	kuchař	k1gMnSc1	kuchař
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
i	i	k9	i
syn	syn	k1gMnSc1	syn
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
je	být	k5eAaImIp3nS	být
plný	plný	k2eAgInSc1d1	plný
nenávisti	nenávist	k1gFnSc3	nenávist
a	a	k8xC	a
pohrdání	pohrdání	k1gNnSc6	pohrdání
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
nadšením	nadšení	k1gNnSc7	nadšení
přijímá	přijímat	k5eAaImIp3nS	přijímat
Ivanovou	Ivanův	k2eAgFnSc7d1	Ivanova
teorií	teorie	k1gFnSc7	teorie
o	o	k7c6	o
světu	svět	k1gInSc3	svět
bez	bez	k7c2	bez
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
.	.	kIx.	.
</s>
<s>
Kateřina	Kateřina	k1gFnSc1	Kateřina
Ivanovna	Ivanovna	k1gFnSc1	Ivanovna
–	–	k?	–
Dmitrijova	Dmitrijův	k2eAgFnSc1d1	Dmitrijův
krásná	krásný	k2eAgFnSc1d1	krásná
snoubenka	snoubenka	k1gFnSc1	snoubenka
<g/>
.	.	kIx.	.
</s>
<s>
Agrafena	Agrafen	k2eAgFnSc1d1	Agrafen
Alexandrovna	Alexandrovna	k1gFnSc1	Alexandrovna
Světlovová	Světlovový	k2eAgFnSc1d1	Světlovový
(	(	kIx(	(
<g/>
Grušeňka	Grušeňka	k1gFnSc1	Grušeňka
<g/>
)	)	kIx)	)
–	–	k?	–
Mladá	mladý	k2eAgFnSc1d1	mladá
žena	žena	k1gFnSc1	žena
<g/>
,	,	kIx,	,
zahrávající	zahrávající	k2eAgFnSc1d1	zahrávající
si	se	k3xPyFc3	se
se	s	k7c7	s
starým	starý	k2eAgInSc7d1	starý
Karamazovem	Karamazov	k1gInSc7	Karamazov
a	a	k8xC	a
Dmitrijem	Dmitrij	k1gMnSc7	Dmitrij
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
Dmitrijem	Dmitrij	k1gMnSc7	Dmitrij
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc7	jeho
otcem	otec	k1gMnSc7	otec
Fjodorem	Fjodor	k1gMnSc7	Fjodor
vrcholí	vrcholit	k5eAaImIp3nS	vrcholit
spor	spor	k1gInSc4	spor
o	o	k7c4	o
dědictví	dědictví	k1gNnSc4	dědictví
po	po	k7c6	po
Dmitrijově	Dmitrijův	k2eAgFnSc6d1	Dmitrijův
matce	matka	k1gFnSc6	matka
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
Fjodor	Fjodor	k1gMnSc1	Fjodor
Pavlovič	Pavlovič	k1gMnSc1	Pavlovič
navrhuje	navrhovat	k5eAaImIp3nS	navrhovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jim	on	k3xPp3gMnPc3	on
stařec	stařec	k1gMnSc1	stařec
Zosima	Zosim	k1gMnSc4	Zosim
byl	být	k5eAaImAgMnS	být
za	za	k7c4	za
soudce	soudce	k1gMnSc4	soudce
<g/>
.	.	kIx.	.
</s>
<s>
Setkání	setkání	k1gNnSc1	setkání
však	však	k9	však
vyvrcholí	vyvrcholit	k5eAaPmIp3nS	vyvrcholit
skandálem	skandál	k1gInSc7	skandál
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
původcem	původce	k1gMnSc7	původce
je	být	k5eAaImIp3nS	být
starý	starý	k2eAgInSc1d1	starý
Karamazov	Karamazov	k1gInSc1	Karamazov
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
večera	večer	k1gInSc2	večer
se	se	k3xPyFc4	se
Aljoša	Aljoša	k1gMnSc1	Aljoša
na	na	k7c4	na
příkaz	příkaz	k1gInSc4	příkaz
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
stěhuje	stěhovat	k5eAaImIp3nS	stěhovat
z	z	k7c2	z
kláštera	klášter	k1gInSc2	klášter
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
se	se	k3xPyFc4	se
chce	chtít	k5eAaImIp3nS	chtít
navrátit	navrátit	k5eAaPmF	navrátit
brzy	brzy	k6eAd1	brzy
zpět	zpět	k6eAd1	zpět
k	k	k7c3	k
Zosimovi	Zosim	k1gMnSc3	Zosim
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc4	jehož
život	život	k1gInSc4	život
se	se	k3xPyFc4	se
pomalu	pomalu	k6eAd1	pomalu
spěje	spět	k5eAaImIp3nS	spět
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
<g/>
.	.	kIx.	.
</s>
<s>
Doma	doma	k6eAd1	doma
se	se	k3xPyFc4	se
Aljoša	Aljoša	k1gMnSc1	Aljoša
dozvídá	dozvídat	k5eAaImIp3nS	dozvídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
chvíli	chvíle	k1gFnSc4	chvíle
před	před	k7c7	před
ním	on	k3xPp3gMnSc7	on
vtrhl	vtrhnout	k5eAaPmAgMnS	vtrhnout
do	do	k7c2	do
otcovského	otcovský	k2eAgInSc2d1	otcovský
domu	dům	k1gInSc2	dům
Dmitrij	Dmitrij	k1gFnSc2	Dmitrij
<g/>
,	,	kIx,	,
surově	surově	k6eAd1	surově
svého	svůj	k3xOyFgMnSc4	svůj
otce	otec	k1gMnSc4	otec
zbil	zbít	k5eAaPmAgMnS	zbít
a	a	k8xC	a
vyhrožoval	vyhrožovat	k5eAaImAgMnS	vyhrožovat
mu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
zabije	zabít	k5eAaPmIp3nS	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Aljoša	Aljoša	k1gMnSc1	Aljoša
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
bratra	bratr	k1gMnSc4	bratr
Dmitrije	Dmitrije	k1gMnSc4	Dmitrije
najít	najít	k5eAaPmF	najít
a	a	k8xC	a
promluvit	promluvit	k5eAaPmF	promluvit
si	se	k3xPyFc3	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
Dmitrij	Dmitrij	k1gMnSc3	Dmitrij
Aljošovi	Aljoša	k1gMnSc3	Aljoša
svěřuje	svěřovat	k5eAaImIp3nS	svěřovat
se	s	k7c7	s
vším	všecek	k3xTgNnSc7	všecek
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
ho	on	k3xPp3gMnSc4	on
trápí	trápit	k5eAaImIp3nS	trápit
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
o	o	k7c6	o
soupeření	soupeření	k1gNnSc6	soupeření
s	s	k7c7	s
otcem	otec	k1gMnSc7	otec
o	o	k7c6	o
Grušeňku	Grušeněk	k1gInSc6	Grušeněk
<g/>
,	,	kIx,	,
či	či	k8xC	či
o	o	k7c6	o
zasnoubení	zasnoubení	k1gNnSc6	zasnoubení
s	s	k7c7	s
Kateřinou	Kateřina	k1gFnSc7	Kateřina
Ivanovnou	Ivanovna	k1gFnSc7	Ivanovna
<g/>
.	.	kIx.	.
</s>
<s>
Den	den	k1gInSc4	den
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
Alexej	Alexej	k1gMnSc1	Alexej
zastavuje	zastavovat	k5eAaImIp3nS	zastavovat
u	u	k7c2	u
starého	starý	k2eAgInSc2d1	starý
Karamazova	Karamazův	k2eAgInSc2d1	Karamazův
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
cestou	cesta	k1gFnSc7	cesta
k	k	k7c3	k
paní	paní	k1gFnSc3	paní
Chochlakovové	Chochlakovový	k2eAgFnSc3d1	Chochlakovový
(	(	kIx(	(
<g/>
statkářce	statkářka	k1gFnSc3	statkářka
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
dcera	dcera	k1gFnSc1	dcera
je	být	k5eAaImIp3nS	být
zamilovaná	zamilovaný	k2eAgFnSc1d1	zamilovaná
do	do	k7c2	do
Aljoši	Aljoša	k1gMnSc2	Aljoša
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
shledá	shledat	k5eAaPmIp3nS	shledat
několik	několik	k4yIc4	několik
školáků	školák	k1gMnPc2	školák
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
házejí	házet	k5eAaImIp3nP	házet
kamení	kamení	k1gNnSc4	kamení
po	po	k7c6	po
malém	malý	k2eAgMnSc6d1	malý
chlapci	chlapec	k1gMnSc6	chlapec
<g/>
.	.	kIx.	.
</s>
<s>
Aljoša	Aljoša	k1gMnSc1	Aljoša
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
zastane	zastanout	k5eAaPmIp3nS	zastanout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
chlapec	chlapec	k1gMnSc1	chlapec
začne	začít	k5eAaPmIp3nS	začít
házet	házet	k5eAaImF	házet
kamení	kamení	k1gNnSc1	kamení
po	po	k7c6	po
svém	svůj	k3xOyFgMnSc6	svůj
zachránci	zachránce	k1gMnSc6	zachránce
<g/>
,	,	kIx,	,
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
jej	on	k3xPp3gMnSc4	on
kousne	kousnout	k5eAaPmIp3nS	kousnout
<g/>
.	.	kIx.	.
</s>
<s>
Aljoša	Aljoša	k1gMnSc1	Aljoša
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
dozvídá	dozvídat	k5eAaImIp3nS	dozvídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
onen	onen	k3xDgMnSc1	onen
chlapec	chlapec	k1gMnSc1	chlapec
se	se	k3xPyFc4	se
mstil	mstít	k5eAaImAgMnS	mstít
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Dmitrij	Dmitrij	k1gMnSc1	Dmitrij
zmlátil	zmlátit	k5eAaPmAgMnS	zmlátit
a	a	k8xC	a
zneuctil	zneuctít	k5eAaPmAgMnS	zneuctít
jeho	on	k3xPp3gMnSc4	on
otce	otec	k1gMnSc4	otec
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Chochlakovů	Chochlakov	k1gInPc2	Chochlakov
hovoří	hovořit	k5eAaImIp3nP	hovořit
s	s	k7c7	s
Kateřinou	Kateřina	k1gFnSc7	Kateřina
Ivanovnou	Ivanovna	k1gFnSc7	Ivanovna
o	o	k7c6	o
Dmitrijovi	Dmitrij	k1gMnSc6	Dmitrij
a	a	k8xC	a
o	o	k7c6	o
penězích	peníze	k1gInPc6	peníze
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
má	mít	k5eAaImIp3nS	mít
dát	dát	k5eAaPmF	dát
štábnímu	štábní	k2eAgMnSc3d1	štábní
generálovi	generál	k1gMnSc3	generál
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
tak	tak	k9	tak
učiní	učinit	k5eAaPmIp3nS	učinit
a	a	k8xC	a
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
chlapec	chlapec	k1gMnSc1	chlapec
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
ho	on	k3xPp3gMnSc4	on
pokousal	pokousat	k5eAaPmAgMnS	pokousat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
právě	právě	k9	právě
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
<g/>
,	,	kIx,	,
Iljuša	Iljuša	k1gMnSc1	Iljuša
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
Aljoša	Aljoša	k1gMnSc1	Aljoša
se	se	k3xPyFc4	se
zasnoubí	zasnoubit	k5eAaPmIp3nS	zasnoubit
s	s	k7c7	s
nezletilou	zletilý	k2eNgFnSc7d1	nezletilá
Lisou	Lisa	k1gFnSc7	Lisa
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
setkává	setkávat	k5eAaImIp3nS	setkávat
s	s	k7c7	s
bratrem	bratr	k1gMnSc7	bratr
Ivanem	Ivan	k1gMnSc7	Ivan
a	a	k8xC	a
společně	společně	k6eAd1	společně
jdou	jít	k5eAaImIp3nP	jít
do	do	k7c2	do
hostince	hostinec	k1gInSc2	hostinec
<g/>
.	.	kIx.	.
</s>
<s>
Alexej	Alexej	k1gMnSc1	Alexej
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
navrací	navracet	k5eAaBmIp3nS	navracet
do	do	k7c2	do
kláštera	klášter	k1gInSc2	klášter
a	a	k8xC	a
Ivan	Ivan	k1gMnSc1	Ivan
odchází	odcházet	k5eAaImIp3nS	odcházet
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
před	před	k7c7	před
svou	svůj	k3xOyFgFnSc7	svůj
cestou	cesta	k1gFnSc7	cesta
do	do	k7c2	do
Moskvy	Moskva	k1gFnSc2	Moskva
rozloučil	rozloučit	k5eAaPmAgMnS	rozloučit
s	s	k7c7	s
otcem	otec	k1gMnSc7	otec
<g/>
.	.	kIx.	.
</s>
<s>
Tentýž	týž	k3xTgInSc4	týž
večer	večer	k1gInSc4	večer
umírá	umírat	k5eAaImIp3nS	umírat
stařec	stařec	k1gMnSc1	stařec
Zosima	Zosim	k1gMnSc2	Zosim
<g/>
.	.	kIx.	.
</s>
<s>
Aljoša	Aljoša	k1gMnSc1	Aljoša
navštěvuje	navštěvovat	k5eAaImIp3nS	navštěvovat
Grušenku	Grušenka	k1gFnSc4	Grušenka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
strach	strach	k1gInSc4	strach
o	o	k7c4	o
Dmitrije	Dmitrije	k1gMnSc4	Dmitrije
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
ze	z	k7c2	z
žárlivosti	žárlivost	k1gFnSc2	žárlivost
zabít	zabít	k5eAaPmF	zabít
svého	svůj	k3xOyFgMnSc4	svůj
otce	otec	k1gMnSc4	otec
<g/>
.	.	kIx.	.
</s>
<s>
Gruša	Gruša	k1gMnSc1	Gruša
později	pozdě	k6eAd2	pozdě
odjíždí	odjíždět	k5eAaImIp3nS	odjíždět
do	do	k7c2	do
Mokrého	Mokrý	k1gMnSc2	Mokrý
za	za	k7c7	za
svým	svůj	k1gMnSc7	svůj
milencem	milenec	k1gMnSc7	milenec
<g/>
,	,	kIx,	,
Polákem	Polák	k1gMnSc7	Polák
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ji	on	k3xPp3gFnSc4	on
před	před	k7c7	před
pěti	pět	k4xCc7	pět
lety	léto	k1gNnPc7	léto
opustil	opustit	k5eAaPmAgMnS	opustit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ona	onen	k3xDgFnSc1	onen
doufá	doufat	k5eAaImIp3nS	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
i	i	k9	i
přesto	přesto	k6eAd1	přesto
stále	stále	k6eAd1	stále
miluje	milovat	k5eAaImIp3nS	milovat
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
proto	proto	k8xC	proto
si	se	k3xPyFc3	se
s	s	k7c7	s
Dmitrijem	Dmitrij	k1gMnSc7	Dmitrij
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc7	jeho
otcem	otec	k1gMnSc7	otec
pouze	pouze	k6eAd1	pouze
zahrává	zahrávat	k5eAaImIp3nS	zahrávat
<g/>
.	.	kIx.	.
</s>
<s>
Dmitrij	Dmitrít	k5eAaPmRp2nS	Dmitrít
své	svůj	k3xOyFgNnSc4	svůj
poslední	poslední	k2eAgInPc4d1	poslední
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
strávil	strávit	k5eAaPmAgMnS	strávit
ve	v	k7c6	v
zmatku	zmatek	k1gInSc6	zmatek
<g/>
,	,	kIx,	,
neúspěšně	úspěšně	k6eNd1	úspěšně
sháněl	shánět	k5eAaImAgMnS	shánět
tři	tři	k4xCgInPc4	tři
tisíce	tisíc	k4xCgInPc4	tisíc
rublů	rubl	k1gInPc2	rubl
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
dlužil	dlužit	k5eAaImAgMnS	dlužit
Kateřině	Kateřina	k1gFnSc6	Kateřina
Ivanovně	Ivanovna	k1gFnSc6	Ivanovna
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Grušeňka	Grušeňka	k1gFnSc1	Grušeňka
odjížděla	odjíždět	k5eAaImAgFnS	odjíždět
<g/>
,	,	kIx,	,
Dmitrij	Dmitrij	k1gFnSc1	Dmitrij
ji	on	k3xPp3gFnSc4	on
marně	marně	k6eAd1	marně
hledal	hledat	k5eAaImAgInS	hledat
<g/>
,	,	kIx,	,
vydal	vydat	k5eAaPmAgInS	vydat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
za	za	k7c7	za
otcem	otec	k1gMnSc7	otec
<g/>
,	,	kIx,	,
jež	jenž	k3xRgMnPc4	jenž
Grušence	Grušenec	k1gMnPc4	Grušenec
nabízel	nabízet	k5eAaImAgInS	nabízet
tři	tři	k4xCgInPc4	tři
tisíce	tisíc	k4xCgInPc4	tisíc
rublů	rubl	k1gInPc2	rubl
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
ho	on	k3xPp3gInSc4	on
navštívila	navštívit	k5eAaPmAgFnS	navštívit
<g/>
.	.	kIx.	.
</s>
<s>
Karamazův	Karamazův	k2eAgMnSc1d1	Karamazův
sluha	sluha	k1gMnSc1	sluha
Směrďakov	Směrďakov	k1gInSc1	Směrďakov
zrovna	zrovna	k6eAd1	zrovna
prodělával	prodělávat	k5eAaImAgInS	prodělávat
záchvat	záchvat	k1gInSc1	záchvat
padoucnice	padoucnice	k1gFnSc2	padoucnice
<g/>
.	.	kIx.	.
</s>
<s>
Dmitrij	Dmitrít	k5eAaPmRp2nS	Dmitrít
tedy	tedy	k9	tedy
na	na	k7c4	na
otcovo	otcův	k2eAgNnSc4d1	otcovo
okno	okno	k1gNnSc4	okno
zaklepal	zaklepat	k5eAaPmAgMnS	zaklepat
přesné	přesný	k2eAgNnSc1d1	přesné
znamení	znamení	k1gNnSc1	znamení
Grušenky	Grušenka	k1gFnSc2	Grušenka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
mu	on	k3xPp3gMnSc3	on
vyzradil	vyzradit	k5eAaPmAgInS	vyzradit
dříve	dříve	k6eAd2	dříve
Směrďakov	Směrďakov	k1gInSc1	Směrďakov
<g/>
.	.	kIx.	.
</s>
<s>
Fjodor	Fjodor	k1gInSc1	Fjodor
Karamazov	Karamazov	k1gInSc1	Karamazov
otevřel	otevřít	k5eAaPmAgInS	otevřít
okno	okno	k1gNnSc4	okno
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
tedy	tedy	k9	tedy
sám	sám	k3xTgMnSc1	sám
<g/>
,	,	kIx,	,
Dmitrij	Dmitrij	k1gMnSc1	Dmitrij
uprchl	uprchnout	k5eAaPmAgMnS	uprchnout
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zdolávání	zdolávání	k1gNnSc6	zdolávání
plotu	plot	k1gInSc2	plot
nechtěně	chtěně	k6eNd1	chtěně
poranil	poranit	k5eAaPmAgMnS	poranit
Grigorije	Grigorije	k1gMnSc1	Grigorije
<g/>
.	.	kIx.	.
</s>
<s>
Zanedlouho	zanedlouho	k6eAd1	zanedlouho
Dmitrij	Dmitrij	k1gMnSc1	Dmitrij
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
Grušeňka	Grušeňka	k1gFnSc1	Grušeňka
a	a	k8xC	a
vydal	vydat	k5eAaPmAgMnS	vydat
se	se	k3xPyFc4	se
za	za	k7c7	za
ní	on	k3xPp3gFnSc7	on
na	na	k7c4	na
flám	flám	k1gInSc4	flám
do	do	k7c2	do
Mokrého	Mokrý	k1gMnSc2	Mokrý
<g/>
.	.	kIx.	.
</s>
<s>
Sužovaly	sužovat	k5eAaImAgFnP	sužovat
ho	on	k3xPp3gMnSc4	on
myšlenky	myšlenka	k1gFnPc4	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
zabil	zabít	k5eAaPmAgMnS	zabít
Grigorije	Grigorije	k1gFnSc4	Grigorije
<g/>
,	,	kIx,	,
a	a	k8xC	a
chtěl	chtít	k5eAaImAgMnS	chtít
spáchat	spáchat	k5eAaPmF	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
získal	získat	k5eAaPmAgMnS	získat
Grušeňčino	Grušeňčin	k2eAgNnSc4d1	Grušeňčin
srdce	srdce	k1gNnSc4	srdce
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
sebevražedné	sebevražedný	k2eAgFnPc4d1	sebevražedná
myšlenky	myšlenka	k1gFnPc4	myšlenka
z	z	k7c2	z
mysli	mysl	k1gFnSc2	mysl
zapudil	zapudit	k5eAaPmAgMnS	zapudit
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
ránu	ráno	k1gNnSc3	ráno
do	do	k7c2	do
hostince	hostinec	k1gInSc2	hostinec
přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
prokurátor	prokurátor	k1gMnSc1	prokurátor
<g/>
,	,	kIx,	,
Dmitrij	Dmitrij	k1gMnSc1	Dmitrij
je	být	k5eAaImIp3nS	být
obviněn	obvinit	k5eAaPmNgMnS	obvinit
z	z	k7c2	z
vraždy	vražda	k1gFnSc2	vražda
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
Fjodora	Fjodor	k1gMnSc2	Fjodor
Pavloviče	Pavlovič	k1gMnSc2	Pavlovič
Karamazova	Karamazův	k2eAgMnSc2d1	Karamazův
a	a	k8xC	a
vzat	vzít	k5eAaPmNgMnS	vzít
do	do	k7c2	do
vazby	vazba	k1gFnSc2	vazba
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
po	po	k7c6	po
měsíci	měsíc	k1gInSc6	měsíc
se	se	k3xPyFc4	se
Ivan	Ivan	k1gMnSc1	Ivan
od	od	k7c2	od
Směrďakova	Směrďakův	k2eAgNnSc2d1	Směrďakův
dozvídá	dozvídat	k5eAaImIp3nS	dozvídat
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
přihodilo	přihodit	k5eAaPmAgNnS	přihodit
onoho	onen	k3xDgInSc2	onen
osudného	osudný	k2eAgInSc2d1	osudný
večera	večer	k1gInSc2	večer
<g/>
.	.	kIx.	.
</s>
<s>
Směrďakov	Směrďakov	k1gInSc4	Směrďakov
tedy	tedy	k8xC	tedy
záchvat	záchvat	k1gInSc4	záchvat
předstíral	předstírat	k5eAaImAgMnS	předstírat
a	a	k8xC	a
zavraždil	zavraždit	k5eAaPmAgMnS	zavraždit
starého	starý	k2eAgInSc2d1	starý
Karamazova	Karamazův	k2eAgInSc2d1	Karamazův
a	a	k8xC	a
vše	všechen	k3xTgNnSc4	všechen
přichystal	přichystat	k5eAaPmAgInS	přichystat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgMnS	být
obviněn	obvinit	k5eAaPmNgMnS	obvinit
právě	právě	k9	právě
Dmitrij	Dmitrij	k1gMnSc1	Dmitrij
<g/>
.	.	kIx.	.
</s>
<s>
Směrďakov	Směrďakov	k1gInSc1	Směrďakov
za	za	k7c4	za
svolení	svolení	k1gNnSc4	svolení
k	k	k7c3	k
vraždě	vražda	k1gFnSc3	vražda
Fjodora	Fjodor	k1gMnSc2	Fjodor
Pavloviče	Pavlovič	k1gMnSc2	Pavlovič
Karamazova	Karamazův	k2eAgMnSc2d1	Karamazův
považoval	považovat	k5eAaImAgMnS	považovat
Ivanův	Ivanův	k2eAgInSc4d1	Ivanův
odjezd	odjezd	k1gInSc4	odjezd
do	do	k7c2	do
Moskvy	Moskva	k1gFnSc2	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
toho	ten	k3xDgInSc2	ten
večera	večer	k1gInSc2	večer
se	se	k3xPyFc4	se
Smerďakov	Smerďakov	k1gInSc1	Smerďakov
oběsí	oběsit	k5eAaPmIp3nS	oběsit
a	a	k8xC	a
Ivan	Ivan	k1gMnSc1	Ivan
z	z	k7c2	z
tíhy	tíha	k1gFnSc2	tíha
událostí	událost	k1gFnPc2	událost
duševně	duševně	k6eAd1	duševně
ochoří	ochořet	k5eAaPmIp3nS	ochořet
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
soudu	soud	k1gInSc2	soud
vše	všechen	k3xTgNnSc1	všechen
svědčí	svědčit	k5eAaImIp3nS	svědčit
proti	proti	k7c3	proti
Dmitrijovi	Dmitrij	k1gMnSc3	Dmitrij
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc7	jeho
poslední	poslední	k2eAgFnSc7d1	poslední
nadějí	naděje	k1gFnSc7	naděje
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
Ivan	Ivan	k1gMnSc1	Ivan
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
jako	jako	k8xC	jako
jediný	jediný	k2eAgMnSc1d1	jediný
zná	znát	k5eAaImIp3nS	znát
pravdu	pravda	k1gFnSc4	pravda
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
jeho	jeho	k3xOp3gNnSc3	jeho
duševnímu	duševní	k2eAgNnSc3d1	duševní
onemocnění	onemocnění	k1gNnSc3	onemocnění
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gFnSc1	jeho
výpověď	výpověď	k1gFnSc1	výpověď
není	být	k5eNaImIp3nS	být
brána	brát	k5eAaImNgFnS	brát
vážně	vážně	k6eAd1	vážně
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
Dmitrij	Dmitrij	k1gMnSc3	Dmitrij
Karamazov	Karamazov	k1gInSc4	Karamazov
odsouzen	odsouzen	k2eAgInSc4d1	odsouzen
k	k	k7c3	k
dvaceti	dvacet	k4xCc3	dvacet
rokům	rok	k1gInPc3	rok
práce	práce	k1gFnSc2	práce
na	na	k7c6	na
Sibiři	Sibiř	k1gFnSc6	Sibiř
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
epilogu	epilog	k1gInSc6	epilog
se	se	k3xPyFc4	se
usmíří	usmířit	k5eAaPmIp3nS	usmířit
Kateřina	Kateřina	k1gFnSc1	Kateřina
Ivanovna	Ivanovna	k1gFnSc1	Ivanovna
s	s	k7c7	s
Grušeňkou	Grušeňka	k1gFnSc7	Grušeňka
a	a	k8xC	a
společně	společně	k6eAd1	společně
plánují	plánovat	k5eAaImIp3nP	plánovat
Dmitrijův	Dmitrijův	k2eAgInSc4d1	Dmitrijův
útěk	útěk	k1gInSc4	útěk
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
končí	končit	k5eAaImIp3nS	končit
pohřbem	pohřeb	k1gInSc7	pohřeb
Ijuši	Ijuše	k1gFnSc4	Ijuše
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yQgInSc6	který
má	mít	k5eAaImIp3nS	mít
proslov	proslov	k1gInSc4	proslov
Aljoša	Aljoša	k1gMnSc1	Aljoša
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
nabádá	nabádat	k5eAaBmIp3nS	nabádat
ostatní	ostatní	k2eAgMnPc4d1	ostatní
chlapce	chlapec	k1gMnPc4	chlapec
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
nikdy	nikdy	k6eAd1	nikdy
na	na	k7c6	na
Iljušu	Iljušu	k1gFnSc6	Iljušu
nezapomněli	zapomnět	k5eNaImAgMnP	zapomnět
<g/>
.	.	kIx.	.
</s>
<s>
Bratří	bratřit	k5eAaImIp3nP	bratřit
Karamazovi	Karamazův	k2eAgMnPc1d1	Karamazův
<g/>
:	:	kIx,	:
román	román	k1gInSc4	román
ve	v	k7c6	v
čtyřech	čtyři	k4xCgFnPc6	čtyři
částech	část	k1gFnPc6	část
s	s	k7c7	s
epilogem	epilog	k1gInSc7	epilog
<g/>
.	.	kIx.	.
</s>
<s>
Přel	přít	k5eAaImAgMnS	přít
<g/>
.	.	kIx.	.
</s>
<s>
Jaromír	Jaromír	k1gMnSc1	Jaromír
Hrubý	Hrubý	k1gMnSc1	Hrubý
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
1894	[number]	k4	1894
<g/>
.	.	kIx.	.
3	[number]	k4	3
sv.	sv.	kA	sv.
(	(	kIx(	(
<g/>
374	[number]	k4	374
<g/>
,	,	kIx,	,
306	[number]	k4	306
<g/>
,	,	kIx,	,
356	[number]	k4	356
s.	s.	k?	s.
<g/>
)	)	kIx)	)
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
1923	[number]	k4	1923
<g/>
.	.	kIx.	.
3	[number]	k4	3
sv.	sv.	kA	sv.
(	(	kIx(	(
<g/>
377	[number]	k4	377
<g/>
,	,	kIx,	,
311	[number]	k4	311
<g/>
,	,	kIx,	,
368	[number]	k4	368
s.	s.	k?	s.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Neautorizovaný	autorizovaný	k2eNgInSc1d1	neautorizovaný
přetisk	přetisk	k1gInSc1	přetisk
<g/>
:	:	kIx,	:
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Dobrovský	Dobrovský	k1gMnSc1	Dobrovský
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
904	[number]	k4	904
s.	s.	k?	s.
Bratří	bratřit	k5eAaImIp3nP	bratřit
Karamazovi	Karamazův	k2eAgMnPc1d1	Karamazův
<g/>
:	:	kIx,	:
román	román	k1gInSc4	román
o	o	k7c6	o
čtyřech	čtyři	k4xCgNnPc6	čtyři
dílech	dílo	k1gNnPc6	dílo
s	s	k7c7	s
epilogem	epilog	k1gInSc7	epilog
<g/>
.	.	kIx.	.
</s>
<s>
Přel	přít	k5eAaImAgMnS	přít
<g/>
.	.	kIx.	.
</s>
<s>
Stanislav	Stanislav	k1gMnSc1	Stanislav
Minařík	Minařík	k1gMnSc1	Minařík
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Stanislav	Stanislav	k1gMnSc1	Stanislav
Minařík	Minařík	k1gMnSc1	Minařík
<g/>
,	,	kIx,	,
1923	[number]	k4	1923
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Kvasnička	Kvasnička	k1gMnSc1	Kvasnička
a	a	k8xC	a
Hampl	Hampl	k1gMnSc1	Hampl
<g/>
,	,	kIx,	,
1926	[number]	k4	1926
<g/>
.	.	kIx.	.
4	[number]	k4	4
sv.	sv.	kA	sv.
(	(	kIx(	(
<g/>
243	[number]	k4	243
<g/>
,	,	kIx,	,
250	[number]	k4	250
<g/>
,	,	kIx,	,
281	[number]	k4	281
<g/>
,	,	kIx,	,
399	[number]	k4	399
s.	s.	k?	s.
<g/>
)	)	kIx)	)
Bratři	bratr	k1gMnPc1	bratr
Karamazovi	Karamazův	k2eAgMnPc1d1	Karamazův
<g/>
:	:	kIx,	:
román	román	k1gInSc4	román
ve	v	k7c6	v
čtyřech	čtyři	k4xCgFnPc6	čtyři
částech	část	k1gFnPc6	část
s	s	k7c7	s
epilogem	epilog	k1gInSc7	epilog
<g/>
.	.	kIx.	.
</s>
<s>
Přel	přít	k5eAaImAgMnS	přít
<g/>
.	.	kIx.	.
</s>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Ryba	Ryba	k1gMnSc1	Ryba
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Henning	Henning	k1gInSc1	Henning
Franzen	Franzna	k1gFnPc2	Franzna
<g/>
,	,	kIx,	,
1928	[number]	k4	1928
<g/>
.	.	kIx.	.
4	[number]	k4	4
sv.	sv.	kA	sv.
(	(	kIx(	(
<g/>
331	[number]	k4	331
<g/>
,	,	kIx,	,
282	[number]	k4	282
<g/>
,	,	kIx,	,
374	[number]	k4	374
<g/>
,	,	kIx,	,
174	[number]	k4	174
s.	s.	k?	s.
<g/>
)	)	kIx)	)
Bratři	bratr	k1gMnPc1	bratr
Karamazovi	Karamazův	k2eAgMnPc1d1	Karamazův
<g/>
:	:	kIx,	:
román	román	k1gInSc4	román
o	o	k7c6	o
dvanácti	dvanáct	k4xCc6	dvanáct
knihách	kniha	k1gFnPc6	kniha
s	s	k7c7	s
epilogem	epilog	k1gInSc7	epilog
<g/>
.	.	kIx.	.
</s>
<s>
Přel	přít	k5eAaImAgMnS	přít
<g/>
.	.	kIx.	.
</s>
<s>
Břetislav	Břetislav	k1gMnSc1	Břetislav
Hůla	Hůla	k1gMnSc1	Hůla
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Melantrich	Melantrich	k1gMnSc1	Melantrich
<g/>
,	,	kIx,	,
1929	[number]	k4	1929
a	a	k8xC	a
1938	[number]	k4	1938
<g/>
.	.	kIx.	.
3	[number]	k4	3
sv.	sv.	kA	sv.
(	(	kIx(	(
<g/>
454	[number]	k4	454
<g/>
,	,	kIx,	,
340	[number]	k4	340
<g/>
,	,	kIx,	,
390	[number]	k4	390
s.	s.	k?	s.
<g/>
)	)	kIx)	)
Bratři	bratr	k1gMnPc1	bratr
Karamazovi	Karamazův	k2eAgMnPc1d1	Karamazův
<g/>
:	:	kIx,	:
román	román	k1gInSc4	román
o	o	k7c6	o
čtyřech	čtyři	k4xCgNnPc6	čtyři
dílech	dílo	k1gNnPc6	dílo
s	s	k7c7	s
epilogem	epilog	k1gInSc7	epilog
<g/>
.	.	kIx.	.
</s>
<s>
Přel	přít	k5eAaImAgMnS	přít
<g/>
.	.	kIx.	.
</s>
<s>
Prokop	Prokop	k1gMnSc1	Prokop
Voskovec	Voskovec	k1gMnSc1	Voskovec
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
SNKLU	SNKLU	kA	SNKLU
<g/>
,	,	kIx,	,
1965	[number]	k4	1965
<g/>
.	.	kIx.	.
2	[number]	k4	2
sv.	sv.	kA	sv.
(	(	kIx(	(
<g/>
586	[number]	k4	586
<g/>
,	,	kIx,	,
326	[number]	k4	326
s.	s.	k?	s.
<g/>
)	)	kIx)	)
Bratři	bratr	k1gMnPc1	bratr
Karamazovovi	Karamazovův	k2eAgMnPc1d1	Karamazovův
<g/>
:	:	kIx,	:
román	román	k1gInSc4	román
o	o	k7c6	o
čtyřech	čtyři	k4xCgNnPc6	čtyři
dílech	dílo	k1gNnPc6	dílo
s	s	k7c7	s
epilogem	epilog	k1gInSc7	epilog
<g/>
.	.	kIx.	.
</s>
<s>
Přel	přít	k5eAaImAgMnS	přít
<g/>
.	.	kIx.	.
</s>
<s>
Prokop	Prokop	k1gMnSc1	Prokop
Voskovec	Voskovec	k1gMnSc1	Voskovec
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
2	[number]	k4	2
sv.	sv.	kA	sv.
(	(	kIx(	(
<g/>
546	[number]	k4	546
<g/>
,	,	kIx,	,
298	[number]	k4	298
s.	s.	k?	s.
<g/>
)	)	kIx)	)
Bratři	bratr	k1gMnPc1	bratr
Karamazovi	Karamazův	k2eAgMnPc1d1	Karamazův
<g/>
:	:	kIx,	:
román	román	k1gInSc4	román
o	o	k7c6	o
čtyřech	čtyři	k4xCgNnPc6	čtyři
dílech	dílo	k1gNnPc6	dílo
s	s	k7c7	s
epilogem	epilog	k1gInSc7	epilog
<g/>
.	.	kIx.	.
</s>
<s>
Přel	přít	k5eAaImAgMnS	přít
<g/>
.	.	kIx.	.
</s>
<s>
Prokop	Prokop	k1gMnSc1	Prokop
Voskovec	Voskovec	k1gMnSc1	Voskovec
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Bonus	bonus	k1gInSc1	bonus
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
2	[number]	k4	2
sv.	sv.	kA	sv.
(	(	kIx(	(
<g/>
359	[number]	k4	359
<g/>
,	,	kIx,	,
508	[number]	k4	508
s.	s.	k?	s.
<g/>
)	)	kIx)	)
Bratři	bratr	k1gMnPc1	bratr
Karamazovi	Karamazův	k2eAgMnPc1d1	Karamazův
<g/>
:	:	kIx,	:
román	román	k1gInSc4	román
o	o	k7c6	o
čtyřech	čtyři	k4xCgNnPc6	čtyři
dílech	dílo	k1gNnPc6	dílo
s	s	k7c7	s
epilogem	epilog	k1gInSc7	epilog
<g/>
.	.	kIx.	.
</s>
<s>
Přel	přít	k5eAaImAgMnS	přít
<g/>
.	.	kIx.	.
</s>
<s>
Prokop	Prokop	k1gMnSc1	Prokop
Voskovec	Voskovec	k1gMnSc1	Voskovec
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
850	[number]	k4	850
s.	s.	k?	s.
Bratři	bratr	k1gMnPc1	bratr
Karamazovi	Karamazův	k2eAgMnPc1d1	Karamazův
<g/>
.	.	kIx.	.
</s>
<s>
Přel	přít	k5eAaImAgMnS	přít
<g/>
.	.	kIx.	.
</s>
<s>
Prokop	Prokop	k1gMnSc1	Prokop
Voskovec	Voskovec	k1gMnSc1	Voskovec
<g/>
.	.	kIx.	.
</s>
<s>
Voznice	voznice	k1gFnSc1	voznice
<g/>
:	:	kIx,	:
Leda	leda	k8xS	leda
<g/>
;	;	kIx,	;
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Rozmluvy	rozmluva	k1gFnSc2	rozmluva
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
751	[number]	k4	751
s.	s.	k?	s.
Bratři	bratr	k1gMnPc1	bratr
Karamazovi	Karamazův	k2eAgMnPc1d1	Karamazův
<g/>
.	.	kIx.	.
</s>
<s>
Přel	přít	k5eAaImAgMnS	přít
<g/>
.	.	kIx.	.
</s>
<s>
Libor	Libor	k1gMnSc1	Libor
Dvořák	Dvořák	k1gMnSc1	Dvořák
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
981	[number]	k4	981
s.	s.	k?	s.
1958	[number]	k4	1958
Bratři	bratr	k1gMnPc1	bratr
Karamazovi	Karamazův	k2eAgMnPc1d1	Karamazův
–	–	k?	–
Americký	americký	k2eAgInSc1d1	americký
film	film	k1gInSc1	film
<g/>
.	.	kIx.	.
</s>
<s>
Režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Richard	Richard	k1gMnSc1	Richard
Brooks	Brooksa	k1gFnPc2	Brooksa
<g/>
,	,	kIx,	,
hrají	hrát	k5eAaImIp3nP	hrát
<g/>
:	:	kIx,	:
Yul	Yul	k1gMnPc1	Yul
Brynner	Brynner	k1gMnSc1	Brynner
<g/>
,	,	kIx,	,
Maria	Maria	k1gFnSc1	Maria
Schell	Schell	k1gInSc1	Schell
<g/>
,	,	kIx,	,
Claire	Clair	k1gInSc5	Clair
Bloom	Bloom	k1gInSc1	Bloom
<g/>
.	.	kIx.	.
1969	[number]	k4	1969
Bratři	bratr	k1gMnPc1	bratr
Karamazovi	Karamazův	k2eAgMnPc1d1	Karamazův
–	–	k?	–
Ruský	ruský	k2eAgInSc1d1	ruský
film	film	k1gInSc1	film
<g/>
.	.	kIx.	.
</s>
<s>
Režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Kirill	Kirill	k1gInSc1	Kirill
Lavrov	Lavrov	k1gInSc1	Lavrov
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
Pyrjev	Pyrjev	k1gMnSc1	Pyrjev
<g/>
,	,	kIx,	,
hrají	hrát	k5eAaImIp3nP	hrát
<g/>
:	:	kIx,	:
Kirill	Kirill	k1gInSc1	Kirill
Lavrov	Lavrov	k1gInSc1	Lavrov
<g/>
,	,	kIx,	,
Andrej	Andrej	k1gMnSc1	Andrej
Abrikosov	Abrikosov	k1gInSc1	Abrikosov
<g/>
.	.	kIx.	.
2008	[number]	k4	2008
Karamazovi	Karamazův	k2eAgMnPc1d1	Karamazův
–	–	k?	–
Česko-polský	českoolský	k2eAgInSc4d1	česko-polský
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
současném	současný	k2eAgNnSc6d1	současné
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
skupina	skupina	k1gFnSc1	skupina
pražských	pražský	k2eAgInPc2d1	pražský
herců	herc	k1gInPc2	herc
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
oceláren	ocelárna	k1gFnPc2	ocelárna
uvedla	uvést	k5eAaPmAgFnS	uvést
jevištní	jevištní	k2eAgFnSc4d1	jevištní
adaptaci	adaptace	k1gFnSc4	adaptace
hry	hra	k1gFnSc2	hra
Bratři	bratr	k1gMnPc1	bratr
Karamazovi	Karamazův	k2eAgMnPc1d1	Karamazův
<g/>
.	.	kIx.	.
</s>
<s>
Režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Petr	Petr	k1gMnSc1	Petr
Zelenka	Zelenka	k1gMnSc1	Zelenka
<g/>
,	,	kIx,	,
hrají	hrát	k5eAaImIp3nP	hrát
<g/>
:	:	kIx,	:
Ivan	Ivan	k1gMnSc1	Ivan
Trojan	Trojan	k1gMnSc1	Trojan
<g/>
,	,	kIx,	,
Radek	Radek	k1gMnSc1	Radek
Holub	Holub	k1gMnSc1	Holub
<g/>
,	,	kIx,	,
Igor	Igor	k1gMnSc1	Igor
Chmela	Chmel	k1gMnSc2	Chmel
<g/>
.	.	kIx.	.
</s>
<s>
Činoherní	činoherní	k2eAgFnPc1d1	činoherní
dramatizace	dramatizace	k1gFnPc1	dramatizace
nejsou	být	k5eNaImIp3nP	být
časté	častý	k2eAgFnPc1d1	častá
pro	pro	k7c4	pro
značný	značný	k2eAgInSc4d1	značný
rozsah	rozsah	k1gInSc4	rozsah
románu	román	k1gInSc2	román
<g/>
.	.	kIx.	.
</s>
<s>
Opera	opera	k1gFnSc1	opera
je	být	k5eAaImIp3nS	být
frekventovanější	frekventovaný	k2eAgFnSc1d2	frekventovanější
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
česká	český	k2eAgFnSc1d1	Česká
verze	verze	k1gFnSc1	verze
je	být	k5eAaImIp3nS	být
jediná	jediný	k2eAgFnSc1d1	jediná
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
1922	[number]	k4	1922
Stavovské	stavovský	k2eAgNnSc1d1	Stavovské
divadlo	divadlo	k1gNnSc1	divadlo
,	,	kIx,	,
československá	československý	k2eAgFnSc1d1	Československá
premiéra	premiéra	k1gFnSc1	premiéra
<g/>
,	,	kIx,	,
hráli	hrát	k5eAaImAgMnP	hrát
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Vávra	Vávra	k1gMnSc1	Vávra
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Steimar	Steimar	k1gMnSc1	Steimar
<g/>
,	,	kIx,	,
<g/>
Eduard	Eduard	k1gMnSc1	Eduard
Kohout	Kohout	k1gMnSc1	Kohout
<g/>
,	,	kIx,	,
Bedřich	Bedřich	k1gMnSc1	Bedřich
Karen	Karen	k1gInSc1	Karen
<g/>
,	,	kIx,	,
Eva	Eva	k1gFnSc1	Eva
Vrchlická	Vrchlická	k1gFnSc1	Vrchlická
a	a	k8xC	a
Růžena	Růžena	k1gFnSc1	Růžena
Nasková	Nasková	k1gFnSc1	Nasková
1981	[number]	k4	1981
Divadlo	divadlo	k1gNnSc4	divadlo
Na	na	k7c6	na
zábradlí	zábradlí	k1gNnSc6	zábradlí
<g />
.	.	kIx.	.
</s>
<s>
<g/>
;	;	kIx,	;
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Evald	Evald	k1gInSc1	Evald
Schorm	Schorm	k1gInSc1	Schorm
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Háša	Háša	k1gMnSc1	Háša
<g/>
;	;	kIx,	;
hráli	hrát	k5eAaImAgMnP	hrát
<g/>
:	:	kIx,	:
Jana	Jana	k1gFnSc1	Jana
Preissová	Preissová	k1gFnSc1	Preissová
<g/>
,	,	kIx,	,
Libuše	Libuše	k1gFnSc1	Libuše
Geprtová	Geprtová	k1gFnSc1	Geprtová
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Bartoška	Bartoška	k1gMnSc1	Bartoška
<g/>
)	)	kIx)	)
19	[number]	k4	19
<g/>
??	??	k?	??
Divadlo	divadlo	k1gNnSc1	divadlo
Husa	Hus	k1gMnSc2	Hus
na	na	k7c6	na
provázku	provázek	k1gInSc6	provázek
<g/>
;	;	kIx,	;
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Bratři	bratr	k1gMnPc1	bratr
Karamazovi	Karamazův	k2eAgMnPc1d1	Karamazův
<g/>
:	:	kIx,	:
Vzkříšení	vzkříšený	k2eAgMnPc1d1	vzkříšený
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Vladimír	Vladimír	k1gMnSc1	Vladimír
Morávek	Morávek	k1gMnSc1	Morávek
<g/>
<g />
.	.	kIx.	.
</s>
<s>
;	;	kIx,	;
s	s	k7c7	s
Jiřím	Jiří	k1gMnSc7	Jiří
Vyorálkem	Vyorálek	k1gMnSc7	Vyorálek
<g/>
,	,	kIx,	,
Janem	Jan	k1gMnSc7	Jan
Budařem	Budař	k1gMnSc7	Budař
a	a	k8xC	a
Ditou	Dita	k1gFnSc7	Dita
Kaplanovou	Kaplanův	k2eAgFnSc7d1	Kaplanova
v	v	k7c6	v
hlavních	hlavní	k2eAgFnPc6d1	hlavní
rolích	role	k1gFnPc6	role
<g/>
.	.	kIx.	.
2012	[number]	k4	2012
Divadlo	divadlo	k1gNnSc1	divadlo
Štěstí	štěstí	k1gNnSc4	štěstí
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
Baráčnické	baráčnický	k2eAgFnSc6d1	Baráčnická
rychtě	rychta	k1gFnSc6	rychta
<g/>
,	,	kIx,	,
inscenace	inscenace	k1gFnSc1	inscenace
inspirovaná	inspirovaný	k2eAgFnSc1d1	inspirovaná
parafrází	parafráze	k1gFnSc7	parafráze
Egona	Egon	k1gMnSc2	Egon
Bondyho	Bondy	k1gMnSc2	Bondy
Bratři	bratr	k1gMnPc1	bratr
Ramazovi	Ramazův	k2eAgMnPc1d1	Ramazův
vypustila	vypustit	k5eAaPmAgFnS	vypustit
všechny	všechen	k3xTgFnPc4	všechen
ženské	ženský	k2eAgFnPc4d1	ženská
postavy	postava	k1gFnPc4	postava
<g/>
.	.	kIx.	.
2013	[number]	k4	2013
Slovenské	slovenský	k2eAgFnSc6d1	slovenská
národní	národní	k2eAgInPc1d1	národní
divadlo	divadlo	k1gNnSc4	divadlo
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
,	,	kIx,	,
Bratia	Bratia	k1gFnSc1	Bratia
Karamazovci	Karamazovec	k1gInSc3	Karamazovec
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
hráli	hrát	k5eAaImAgMnP	hrát
<g/>
:	:	kIx,	:
Dušan	Dušan	k1gMnSc1	Dušan
Jamrich	Jamrich	k1gMnSc1	Jamrich
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
Ondrík	Ondrík	k1gMnSc1	Ondrík
<g/>
,	,	kIx,	,
Luboš	Luboš	k1gMnSc1	Luboš
Kostelný	Kostelný	k2eAgMnSc1d1	Kostelný
<g/>
,	,	kIx,	,
Táňa	Táňa	k1gFnSc1	Táňa
Pauhofová	Pauhofový	k2eAgFnSc1d1	Pauhofová
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Roman	Roman	k1gMnSc1	Roman
Polák	Polák	k1gMnSc1	Polák
<g/>
,	,	kIx,	,
představení	představení	k1gNnSc1	představení
trvalo	trvat	k5eAaImAgNnS	trvat
4,5	[number]	k4	4,5
hodiny	hodina	k1gFnPc1	hodina
<g/>
.	.	kIx.	.
2015	[number]	k4	2015
Fjodor	Fjodor	k1gMnSc1	Fjodor
Michajlovič	Michajlovič	k1gMnSc1	Michajlovič
Dostojevskij	Dostojevskij	k1gMnSc1	Dostojevskij
-	-	kIx~	-
Bratři	bratr	k1gMnPc1	bratr
Karamazovi	Karamazův	k2eAgMnPc1d1	Karamazův
(	(	kIx(	(
<g/>
Činoherní	činoherní	k2eAgInSc1d1	činoherní
klub	klub	k1gInSc1	klub
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
režie	režie	k1gFnSc1	režie
Martin	Martin	k1gMnSc1	Martin
Čičvák	Čičvák	k1gMnSc1	Čičvák
<g/>
,	,	kIx,	,
hrají	hrát	k5eAaImIp3nP	hrát
Juraj	Juraj	k1gFnSc1	Juraj
Kukura	Kukura	k1gFnSc1	Kukura
<g/>
,	,	kIx,	,
Matěj	Matěj	k1gMnSc1	Matěj
Dadák	Dadák	k1gMnSc1	Dadák
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
Čapka	Čapka	k1gMnSc1	Čapka
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Hájek	Hájek	k1gMnSc1	Hájek
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
Finger	Finger	k1gMnSc1	Finger
<g/>
,	,	kIx,	,
Natálie	Natálie	k1gFnSc1	Natálie
Puklušová	Puklušová	k1gFnSc1	Puklušová
<g/>
,	,	kIx,	,
Gabriela	Gabriela	k1gFnSc1	Gabriela
Míčová	míčový	k2eAgFnSc1d1	Míčová
<g/>
,	,	kIx,	,
představení	představení	k1gNnSc1	představení
trvá	trvat	k5eAaImIp3nS	trvat
3	[number]	k4	3
hodiny	hodina	k1gFnPc4	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Operu	oprat	k5eAaPmIp1nS	oprat
Bratři	bratr	k1gMnPc1	bratr
Karamazovi	Karamazův	k2eAgMnPc1d1	Karamazův
napsal	napsat	k5eAaBmAgInS	napsat
roku	rok	k1gInSc2	rok
1927	[number]	k4	1927
český	český	k2eAgMnSc1d1	český
skladatel	skladatel	k1gMnSc1	skladatel
Otakar	Otakar	k1gMnSc1	Otakar
Jeremiáš	Jeremiáš	k1gMnSc1	Jeremiáš
<g/>
.	.	kIx.	.
</s>
