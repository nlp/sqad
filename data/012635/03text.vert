<p>
<s>
Římskokatolický	římskokatolický	k2eAgInSc1d1	římskokatolický
kostel	kostel	k1gInSc1	kostel
svaté	svatý	k2eAgFnSc2d1	svatá
Kateřiny	Kateřina	k1gFnSc2	Kateřina
Alexandrijské	alexandrijský	k2eAgFnSc2d1	Alexandrijská
ve	v	k7c6	v
Volarech	Volar	k1gInPc6	Volar
postavil	postavit	k5eAaPmAgMnS	postavit
stavitel	stavitel	k1gMnSc1	stavitel
Jan	Jan	k1gMnSc1	Jan
Canevalle	Canevalle	k1gInSc4	Canevalle
v	v	k7c6	v
barokním	barokní	k2eAgInSc6d1	barokní
slohu	sloh	k1gInSc6	sloh
nedaleko	nedaleko	k7c2	nedaleko
náměstí	náměstí	k1gNnSc2	náměstí
na	na	k7c6	na
základech	základ	k1gInPc6	základ
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1688	[number]	k4	1688
zbořené	zbořený	k2eAgFnPc1d1	zbořená
starší	starý	k2eAgFnPc1d2	starší
gotické	gotický	k2eAgFnPc1d1	gotická
stavby	stavba	k1gFnPc1	stavba
<g/>
,	,	kIx,	,
o	o	k7c6	o
níž	jenž	k3xRgFnSc6	jenž
jsou	být	k5eAaImIp3nP	být
první	první	k4xOgFnPc1	první
zmínky	zmínka	k1gFnPc1	zmínka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1496	[number]	k4	1496
<g/>
.	.	kIx.	.
</s>
<s>
Svěcení	svěcení	k1gNnSc1	svěcení
kostela	kostel	k1gInSc2	kostel
se	se	k3xPyFc4	se
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
8	[number]	k4	8
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1690	[number]	k4	1690
<g/>
.	.	kIx.	.
</s>
<s>
Chrám	chrám	k1gInSc1	chrám
je	být	k5eAaImIp3nS	být
chráněn	chránit	k5eAaImNgInS	chránit
jako	jako	k8xS	jako
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
<g/>
Z	z	k7c2	z
původního	původní	k2eAgInSc2d1	původní
kostela	kostel	k1gInSc2	kostel
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
menší	malý	k2eAgMnSc1d2	menší
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zachovala	zachovat	k5eAaPmAgFnS	zachovat
spodní	spodní	k2eAgFnSc1d1	spodní
část	část	k1gFnSc1	část
(	(	kIx(	(
<g/>
přízemí	přízemí	k1gNnSc1	přízemí
<g/>
)	)	kIx)	)
věže	věž	k1gFnPc1	věž
o	o	k7c6	o
čtvercovém	čtvercový	k2eAgInSc6d1	čtvercový
půdorysu	půdorys	k1gInSc6	půdorys
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
přistavěna	přistavět	k5eAaPmNgFnS	přistavět
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
straně	strana	k1gFnSc6	strana
k	k	k7c3	k
jediné	jediný	k2eAgFnSc3d1	jediná
lodi	loď	k1gFnSc3	loď
tvaru	tvar	k1gInSc2	tvar
obdélníka	obdélník	k1gInSc2	obdélník
o	o	k7c6	o
rozměrech	rozměr	k1gInPc6	rozměr
16	[number]	k4	16
×	×	k?	×
11	[number]	k4	11
metrů	metr	k1gInPc2	metr
se	s	k7c7	s
zaoblenými	zaoblený	k2eAgInPc7d1	zaoblený
rohy	roh	k1gInPc7	roh
u	u	k7c2	u
kněžiště	kněžiště	k1gNnSc2	kněžiště
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
lodi	loď	k1gFnSc3	loď
přiléhající	přiléhající	k2eAgFnSc1d1	přiléhající
trojboce	trojboce	k6eAd1	trojboce
uzavřený	uzavřený	k2eAgInSc1d1	uzavřený
presbytář	presbytář	k1gInSc1	presbytář
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
necelých	celý	k2eNgInPc2d1	necelý
9	[number]	k4	9
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
šířce	šířka	k1gFnSc3	šířka
téměř	téměř	k6eAd1	téměř
7	[number]	k4	7
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Sakristie	sakristie	k1gFnSc1	sakristie
je	být	k5eAaImIp3nS	být
přistavěna	přistavět	k5eAaPmNgFnS	přistavět
k	k	k7c3	k
presbytáři	presbytář	k1gInSc3	presbytář
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
severní	severní	k2eAgFnSc6d1	severní
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1724	[number]	k4	1724
byla	být	k5eAaImAgFnS	být
věž	věž	k1gFnSc1	věž
kvůli	kvůli	k7c3	kvůli
lepší	dobrý	k2eAgFnSc3d2	lepší
slyšitelnosti	slyšitelnost	k1gFnSc3	slyšitelnost
zvonů	zvon	k1gInPc2	zvon
zvýšena	zvýšen	k2eAgFnSc1d1	zvýšena
o	o	k7c4	o
necelých	celý	k2eNgInPc2d1	necelý
sedm	sedm	k4xCc4	sedm
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1715	[number]	k4	1715
<g/>
,	,	kIx,	,
1754	[number]	k4	1754
a	a	k8xC	a
1863	[number]	k4	1863
kostel	kostel	k1gInSc1	kostel
vyhořel	vyhořet	k5eAaPmAgInS	vyhořet
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
však	však	k9	však
byl	být	k5eAaImAgInS	být
vzápětí	vzápětí	k6eAd1	vzápětí
obnoven	obnovit	k5eAaPmNgInS	obnovit
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
i	i	k9	i
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1756	[number]	k4	1756
byla	být	k5eAaImAgFnS	být
k	k	k7c3	k
lodi	loď	k1gFnSc3	loď
kostela	kostel	k1gInSc2	kostel
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
straně	strana	k1gFnSc6	strana
přistavěna	přistavěn	k2eAgFnSc1d1	přistavěna
Tusetská	Tusetský	k2eAgFnSc1d1	Tusetský
kaple	kaple	k1gFnSc1	kaple
stejného	stejný	k2eAgInSc2d1	stejný
tvaru	tvar	k1gInSc2	tvar
jako	jako	k8xS	jako
presbytář	presbytář	k1gInSc4	presbytář
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
o	o	k7c4	o
něco	něco	k3yInSc4	něco
menší	malý	k2eAgNnSc1d2	menší
(	(	kIx(	(
<g/>
délka	délka	k1gFnSc1	délka
6	[number]	k4	6
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
šířka	šířka	k1gFnSc1	šířka
přes	přes	k7c4	přes
5	[number]	k4	5
metrů	metr	k1gInPc2	metr
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
název	název	k1gInSc1	název
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
německého	německý	k2eAgInSc2d1	německý
názvu	název	k1gInSc2	název
nedaleké	daleký	k2eNgFnSc2d1	nedaleká
obce	obec	k1gFnSc2	obec
Stožec	Stožec	k1gMnSc1	Stožec
(	(	kIx(	(
<g/>
Tusset	Tusset	k1gMnSc1	Tusset
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
kaple	kaple	k1gFnSc2	kaple
byl	být	k5eAaImAgInS	být
přenesen	přenesen	k2eAgInSc1d1	přenesen
obraz	obraz	k1gInSc1	obraz
z	z	k7c2	z
lesní	lesní	k2eAgFnSc2d1	lesní
kaple	kaple	k1gFnSc2	kaple
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
pod	pod	k7c7	pod
Stožeckou	Stožecký	k2eAgFnSc7d1	Stožecká
skálou	skála	k1gFnSc7	skála
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1903	[number]	k4	1903
byla	být	k5eAaImAgFnS	být
zdejší	zdejší	k2eAgFnSc1d1	zdejší
farnost	farnost	k1gFnSc1	farnost
povýšena	povýšen	k2eAgFnSc1d1	povýšena
na	na	k7c6	na
děkanství	děkanství	k1gNnSc6	děkanství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
byl	být	k5eAaImAgInS	být
původní	původní	k2eAgInSc1d1	původní
hodinový	hodinový	k2eAgInSc1d1	hodinový
stroj	stroj	k1gInSc1	stroj
věžních	věžní	k2eAgFnPc2d1	věžní
hodin	hodina	k1gFnPc2	hodina
přenesen	přenést	k5eAaPmNgInS	přenést
do	do	k7c2	do
městského	městský	k2eAgNnSc2d1	Městské
muzea	muzeum	k1gNnSc2	muzeum
a	a	k8xC	a
nahrazen	nahradit	k5eAaPmNgInS	nahradit
novým	nový	k2eAgInSc7d1	nový
s	s	k7c7	s
elektrickým	elektrický	k2eAgNnSc7d1	elektrické
zvoněním	zvonění	k1gNnSc7	zvonění
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
velká	velký	k2eAgFnSc1d1	velká
oprava	oprava	k1gFnSc1	oprava
kostela	kostel	k1gInSc2	kostel
byla	být	k5eAaImAgFnS	být
provedena	provést	k5eAaPmNgFnS	provést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
v	v	k7c6	v
září	září	k1gNnSc6	září
2006	[number]	k4	2006
byla	být	k5eAaImAgFnS	být
věž	věž	k1gFnSc1	věž
poprvé	poprvé	k6eAd1	poprvé
mimořádně	mimořádně	k6eAd1	mimořádně
zpřístupněna	zpřístupněn	k2eAgFnSc1d1	zpřístupněna
veřejnosti	veřejnost	k1gFnSc3	veřejnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kostel	kostel	k1gInSc1	kostel
svaté	svatý	k2eAgFnSc2d1	svatá
Kateřiny	Kateřina	k1gFnSc2	Kateřina
Alexandrijské	alexandrijský	k2eAgFnPc4d1	Alexandrijská
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
farnost	farnost	k1gFnSc1	farnost
Volary	Volara	k1gFnSc2	Volara
</s>
</p>
