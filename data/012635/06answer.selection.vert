<s>
Římskokatolický	římskokatolický	k2eAgInSc1d1	římskokatolický
kostel	kostel	k1gInSc1	kostel
svaté	svatý	k2eAgFnSc2d1	svatá
Kateřiny	Kateřina	k1gFnSc2	Kateřina
Alexandrijské	alexandrijský	k2eAgFnSc2d1	Alexandrijská
ve	v	k7c6	v
Volarech	Volar	k1gInPc6	Volar
postavil	postavit	k5eAaPmAgMnS	postavit
stavitel	stavitel	k1gMnSc1	stavitel
Jan	Jan	k1gMnSc1	Jan
Canevalle	Canevalle	k1gInSc4	Canevalle
v	v	k7c6	v
barokním	barokní	k2eAgInSc6d1	barokní
slohu	sloh	k1gInSc6	sloh
nedaleko	nedaleko	k7c2	nedaleko
náměstí	náměstí	k1gNnSc2	náměstí
na	na	k7c6	na
základech	základ	k1gInPc6	základ
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1688	[number]	k4	1688
zbořené	zbořený	k2eAgFnPc1d1	zbořená
starší	starý	k2eAgFnPc1d2	starší
gotické	gotický	k2eAgFnPc1d1	gotická
stavby	stavba	k1gFnPc1	stavba
<g/>
,	,	kIx,	,
o	o	k7c6	o
níž	jenž	k3xRgFnSc6	jenž
jsou	být	k5eAaImIp3nP	být
první	první	k4xOgFnPc1	první
zmínky	zmínka	k1gFnPc1	zmínka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1496	[number]	k4	1496
<g/>
.	.	kIx.	.
</s>
