<s>
Ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
by	by	kYmCp3nS	by
neměl	mít	k5eNaImAgMnS	mít
dosud	dosud	k6eAd1	dosud
žádný	žádný	k3yNgMnSc1	žádný
černý	černý	k2eAgMnSc1d1	černý
trpaslík	trpaslík	k1gMnSc1	trpaslík
existovat	existovat	k5eAaImF	existovat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
čas	čas	k1gInSc4	čas
potřebný	potřebný	k2eAgInSc4d1	potřebný
k	k	k7c3	k
ochlazení	ochlazení	k1gNnSc3	ochlazení
bílého	bílý	k1gMnSc2	bílý
trpaslíka	trpaslík	k1gMnSc2	trpaslík
na	na	k7c4	na
požadovanou	požadovaný	k2eAgFnSc4d1	požadovaná
úroveň	úroveň	k1gFnSc4	úroveň
je	být	k5eAaImIp3nS	být
delší	dlouhý	k2eAgFnSc1d2	delší
než	než	k8xS	než
dosavadní	dosavadní	k2eAgFnSc1d1	dosavadní
délka	délka	k1gFnSc1	délka
existence	existence	k1gFnSc2	existence
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
