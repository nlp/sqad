<s>
Stará	starý	k2eAgFnSc1d1
radnice	radnice	k1gFnSc1
(	(	kIx(
<g/>
Kynšperk	Kynšperk	k1gInSc1
nad	nad	k7c7
Ohří	Ohře	k1gFnSc7
<g/>
)	)	kIx)
</s>
<s>
Stará	starý	k2eAgFnSc1d1
radnice	radnice	k1gFnSc1
Pohled	pohled	k1gInSc1
od	od	k7c2
jihuÚčel	jihuÚčet	k5eAaPmAgMnS,k5eAaImAgMnS
stavby	stavba	k1gFnPc4
</s>
<s>
radnice	radnice	k1gFnSc1
<g/>
,	,	kIx,
panské	panský	k2eAgNnSc1d1
sídlo	sídlo	k1gNnSc1
</s>
<s>
Základní	základní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Sloh	sloha	k1gFnPc2
</s>
<s>
renesance	renesance	k1gFnSc1
<g/>
,	,	kIx,
rané	raný	k2eAgNnSc1d1
baroko	baroko	k1gNnSc1
Výstavba	výstavba	k1gFnSc1
</s>
<s>
1586	#num#	k4
Přestavba	přestavba	k1gFnSc1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
polovina	polovina	k1gFnSc1
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
Současný	současný	k2eAgMnSc1d1
majitel	majitel	k1gMnSc1
</s>
<s>
Město	město	k1gNnSc1
Kynšperk	Kynšperk	k1gInSc1
nad	nad	k7c7
Ohří	Ohře	k1gFnSc7
Poloha	poloha	k1gFnSc1
Adresa	adresa	k1gFnSc1
</s>
<s>
Maxima	Maxima	k1gFnSc1
Gorkého	Gorkého	k2eAgFnSc1d1
16	#num#	k4
<g/>
,	,	kIx,
Kynšperk	Kynšperk	k1gInSc1
nad	nad	k7c7
Ohří	Ohře	k1gFnSc7
<g/>
,	,	kIx,
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Ulice	ulice	k1gFnSc2
</s>
<s>
Maxima	Maxima	k1gFnSc1
Gorkého	Gorkého	k2eAgFnSc2d1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
7	#num#	k4
<g/>
′	′	k?
<g/>
9,9	9,9	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
12	#num#	k4
<g/>
°	°	k?
<g/>
31	#num#	k4
<g/>
′	′	k?
<g/>
44,86	44,86	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Stará	starý	k2eAgFnSc1d1
radnice	radnice	k1gFnSc1
v	v	k7c6
Kynšperku	Kynšperk	k1gInSc6
nad	nad	k7c7
Ohří	Ohře	k1gFnSc7
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Rejstříkové	rejstříkový	k2eAgFnPc1d1
číslo	číslo	k1gNnSc4
památky	památka	k1gFnSc2
</s>
<s>
101519	#num#	k4
(	(	kIx(
<g/>
Pk	Pk	k1gFnSc1
<g/>
•	•	k?
<g/>
MIS	mísa	k1gFnPc2
<g/>
•	•	k?
<g/>
Sez	Sez	k1gMnSc1
<g/>
•	•	k?
<g/>
Obr	obr	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Stará	starý	k2eAgFnSc1d1
radnice	radnice	k1gFnSc1
je	být	k5eAaImIp3nS
budova	budova	k1gFnSc1
v	v	k7c6
historické	historický	k2eAgFnSc6d1
zástavbě	zástavba	k1gFnSc6
v	v	k7c6
Kynšperku	Kynšperk	k1gInSc6
nad	nad	k7c7
Ohří	Ohře	k1gFnSc7
v	v	k7c6
okrese	okres	k1gInSc6
Sokolov	Sokolov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
postaven	postavit	k5eAaPmNgInS
jako	jako	k8xC,k8xS
městská	městský	k2eAgFnSc1d1
radnice	radnice	k1gFnSc1
a	a	k8xC
spolu	spolu	k6eAd1
s	s	k7c7
nedalekou	daleký	k2eNgFnSc7d1
Městskou	městský	k2eAgFnSc7d1
bránou	brána	k1gFnSc7
je	být	k5eAaImIp3nS
nejstarším	starý	k2eAgInSc7d3
dochovaným	dochovaný	k2eAgInSc7d1
zděným	zděný	k2eAgInSc7d1
domem	dům	k1gInSc7
města	město	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c4
určitou	určitý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
sloužila	sloužit	k5eAaImAgFnS
jako	jako	k8xC,k8xS
panské	panský	k2eAgNnSc1d1
sídlo	sídlo	k1gNnSc1
a	a	k8xC
správní	správní	k2eAgFnSc1d1
budova	budova	k1gFnSc1
panství	panství	k1gNnSc2
majitelům	majitel	k1gMnPc3
města	město	k1gNnSc2
<g/>
,	,	kIx,
kterými	který	k3yQgFnPc7,k3yIgFnPc7,k3yRgFnPc7
byli	být	k5eAaImAgMnP
od	od	k7c2
roku	rok	k1gInSc2
1621	#num#	k4
Metternichové	Metternichová	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
se	se	k3xPyFc4
objekt	objekt	k1gInSc1
často	často	k6eAd1
uvádí	uvádět	k5eAaImIp3nS
i	i	k9
jako	jako	k9
Panský	panský	k2eAgInSc1d1
dům	dům	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Objekt	objekt	k1gInSc1
je	být	k5eAaImIp3nS
chráněn	chránit	k5eAaImNgInS
jako	jako	k8xC,k8xS
kulturní	kulturní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Budova	budova	k1gFnSc1
staré	starý	k2eAgFnSc2d1
radnice	radnice	k1gFnSc2
byla	být	k5eAaImAgFnS
postavena	postaven	k2eAgFnSc1d1
roku	rok	k1gInSc2
1586	#num#	k4
při	při	k7c6
jihozápadním	jihozápadní	k2eAgInSc6d1
okraji	okraj	k1gInSc6
náměstí	náměstí	k1gNnSc1
<g/>
,	,	kIx,
přibližně	přibližně	k6eAd1
150	#num#	k4
m	m	kA
východně	východně	k6eAd1
od	od	k7c2
nyní	nyní	k6eAd1
již	již	k6eAd1
zaniklého	zaniklý	k2eAgInSc2d1
kynšperského	kynšperský	k2eAgInSc2d1
hradu	hrad	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předpokládá	předpokládat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
byla	být	k5eAaImAgNnP
postavena	postavit	k5eAaPmNgNnP
na	na	k7c6
místě	místo	k1gNnSc6
středověkého	středověký	k2eAgInSc2d1
objektu	objekt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc7
původní	původní	k2eAgFnSc1d1
podoba	podoba	k1gFnSc1
je	být	k5eAaImIp3nS
neznámá	známý	k2eNgFnSc1d1
<g/>
,	,	kIx,
ví	vědět	k5eAaImIp3nS
se	se	k3xPyFc4
však	však	k9
<g/>
,	,	kIx,
že	že	k8xS
sloužila	sloužit	k5eAaImAgFnS
jako	jako	k9
městská	městský	k2eAgFnSc1d1
radnice	radnice	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současná	současný	k2eAgFnSc1d1
raně	raně	k6eAd1
barokní	barokní	k2eAgFnSc1d1
podoba	podoba	k1gFnSc1
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
doby	doba	k1gFnSc2
po	po	k7c6
přestavbě	přestavba	k1gFnSc6
v	v	k7c6
druhé	druhý	k4xOgFnSc6
polovině	polovina	k1gFnSc6
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
počátku	počátek	k1gInSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
přešel	přejít	k5eAaPmAgInS
objekt	objekt	k1gInSc1
opět	opět	k6eAd1
do	do	k7c2
vlastnictví	vlastnictví	k1gNnSc2
města	město	k1gNnSc2
a	a	k8xC
byl	být	k5eAaImAgInS
přestavován	přestavovat	k5eAaImNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Radnice	radnice	k1gFnSc1
zde	zde	k6eAd1
byla	být	k5eAaImAgFnS
zřízena	zřízen	k2eAgFnSc1d1
roku	rok	k1gInSc2
1836	#num#	k4
nebo	nebo	k8xC
1841	#num#	k4
a	a	k8xC
tomuto	tento	k3xDgInSc3
účelu	účel	k1gInSc3
sloužila	sloužit	k5eAaImAgFnS
až	až	k9
do	do	k7c2
poloviny	polovina	k1gFnSc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Stavební	stavební	k2eAgInSc1d1
stav	stav	k1gInSc1
byl	být	k5eAaImAgInS
zhoršený	zhoršený	k2eAgMnSc1d1
a	a	k8xC
několik	několik	k4yIc4
let	léto	k1gNnPc2
byl	být	k5eAaImAgInS
objet	objet	k2eAgInSc1d1
nevyužívaný	využívaný	k2eNgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krátce	krátce	k6eAd1
zde	zde	k6eAd1
měla	mít	k5eAaImAgFnS
kanceláře	kancelář	k1gFnPc4
lesní	lesní	k2eAgFnSc4d1
společnost	společnost	k1gFnSc4
a	a	k8xC
po	po	k7c6
roce	rok	k1gInSc6
1990	#num#	k4
i	i	k8xC
některé	některý	k3yIgFnPc4
soukromé	soukromý	k2eAgFnPc4d1
firmy	firma	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
byla	být	k5eAaImAgFnS
budova	budova	k1gFnSc1
rekonstruována	rekonstruovat	k5eAaBmNgFnS
a	a	k8xC
slavnostní	slavnostní	k2eAgNnSc1d1
předání	předání	k1gNnSc1
rekonstruovaného	rekonstruovaný	k2eAgInSc2d1
objektu	objekt	k1gInSc2
se	se	k3xPyFc4
uskutečnilo	uskutečnit	k5eAaPmAgNnS
28	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
budově	budova	k1gFnSc6
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
zasedací	zasedací	k2eAgFnPc1d1
místnosti	místnost	k1gFnPc1
městského	městský	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
a	a	k8xC
městské	městský	k2eAgNnSc1d1
infocentrum	infocentrum	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
24	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
2005	#num#	k4
zde	zde	k6eAd1
byla	být	k5eAaImAgFnS
slavnostně	slavnostně	k6eAd1
otevřena	otevřít	k5eAaPmNgFnS
výstavní	výstavní	k2eAgFnSc1d1
síň	síň	k1gFnSc1
Panský	panský	k2eAgInSc4d1
dům	dům	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Stavební	stavební	k2eAgFnSc1d1
podoba	podoba	k1gFnSc1
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1
portál	portál	k1gInSc1
</s>
<s>
Objekt	objekt	k1gInSc1
je	být	k5eAaImIp3nS
mohutná	mohutný	k2eAgFnSc1d1
obdélná	obdélný	k2eAgFnSc1d1
patrová	patrový	k2eAgFnSc1d1
budova	budova	k1gFnSc1
se	s	k7c7
sedlovou	sedlový	k2eAgFnSc7d1
střechou	střecha	k1gFnSc7
se	s	k7c7
čtyřmi	čtyři	k4xCgInPc7
novodobými	novodobý	k2eAgInPc7d1
vikýři	vikýř	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Budova	budova	k1gFnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
severozápadní	severozápadní	k2eAgFnSc6d1
části	část	k1gFnSc6
podsklepená	podsklepený	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jižní	jižní	k2eAgInSc4d1
průčelí	průčelí	k1gNnSc1
je	být	k5eAaImIp3nS
hladké	hladký	k2eAgMnPc4d1
<g/>
,	,	kIx,
fasádu	fasáda	k1gFnSc4
oživovaly	oživovat	k5eAaImAgInP
ještě	ještě	k9
před	před	k7c7
druhou	druhý	k4xOgFnSc7
světovou	světový	k2eAgFnSc7d1
válkou	válka	k1gFnSc7
jemné	jemný	k2eAgInPc1d1
pásy	pás	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čelní	čelní	k2eAgInSc4d1
průčelí	průčelí	k1gNnSc1
je	být	k5eAaImIp3nS
orientované	orientovaný	k2eAgNnSc1d1
k	k	k7c3
jihu	jih	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nacházejí	nacházet	k5eAaImIp3nP
se	se	k3xPyFc4
na	na	k7c6
něm	on	k3xPp3gInSc6
dva	dva	k4xCgInPc4
vstupy	vstup	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgInSc1d1
vstup	vstup	k1gInSc1
je	být	k5eAaImIp3nS
orámován	orámovat	k5eAaPmNgInS
půlkruhovým	půlkruhový	k2eAgInSc7d1
portálem	portál	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nad	nad	k7c7
ním	on	k3xPp3gMnSc7
byla	být	k5eAaImAgFnS
do	do	k7c2
vnější	vnější	k2eAgFnSc2d1
fasády	fasáda	k1gFnSc2
zasazena	zasazen	k2eAgFnSc1d1
kamenná	kamenný	k2eAgFnSc1d1
deska	deska	k1gFnSc1
se	s	k7c7
zaoblenými	zaoblený	k2eAgInPc7d1
rohy	roh	k1gInPc7
<g/>
,	,	kIx,
v	v	k7c6
horní	horní	k2eAgFnSc6d1
části	část	k1gFnSc6
s	s	k7c7
plastickým	plastický	k2eAgInSc7d1
erbem	erb	k1gInSc7
rodu	rod	k1gInSc2
Metternichů	Metternich	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Erb	erb	k1gInSc1
je	být	k5eAaImIp3nS
zřetelný	zřetelný	k2eAgInSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
nápis	nápis	k1gInSc1
byl	být	k5eAaImAgInS
po	po	k7c6
roce	rok	k1gInSc6
1946	#num#	k4
omítnut	omítnout	k5eAaPmNgInS
a	a	k8xC
je	být	k5eAaImIp3nS
nečitelný	čitelný	k2eNgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vztahoval	vztahovat	k5eAaImAgMnS
se	se	k3xPyFc4
pravděpodobně	pravděpodobně	k6eAd1
k	k	k7c3
přestavbě	přestavba	k1gFnSc3
v	v	k7c6
druhé	druhý	k4xOgFnSc6
polovině	polovina	k1gFnSc6
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
a	a	k8xC
nejspíše	nejspíše	k9
obsahoval	obsahovat	k5eAaImAgInS
i	i	k9
dataci	datace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c7
hlavním	hlavní	k2eAgInSc7d1
portálem	portál	k1gInSc7
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
plochostropá	plochostropý	k2eAgFnSc1d1
chodba	chodba	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Východní	východní	k2eAgFnSc1d1
štítové	štítový	k2eAgNnSc4d1
průčelí	průčelí	k1gNnSc4
směřuje	směřovat	k5eAaImIp3nS
do	do	k7c2
horní	horní	k2eAgFnSc2d1
části	část	k1gFnSc2
náměstí	náměstí	k1gNnSc1
Míru	mír	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
přízemí	přízemí	k1gNnSc6
jsou	být	k5eAaImIp3nP
čtyři	čtyři	k4xCgNnPc1
okna	okno	k1gNnPc1
<g/>
,	,	kIx,
čtyři	čtyři	k4xCgMnPc1
jsou	být	k5eAaImIp3nP
v	v	k7c6
patře	patro	k1gNnSc6
<g/>
,	,	kIx,
dvě	dva	k4xCgNnPc1
menší	malý	k2eAgNnPc1d2
okna	okno	k1gNnPc1
jsou	být	k5eAaImIp3nP
ve	v	k7c6
štítu	štít	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
Metternichové	Metternich	k1gMnPc1
získali	získat	k5eAaPmAgMnP
Kynšperk	Kynšperk	k1gInSc4
v	v	k7c6
roce	rok	k1gInSc6
1621	#num#	k4
nejprve	nejprve	k6eAd1
jako	jako	k8xS,k8xC
zástavu	zástava	k1gFnSc4
<g/>
,	,	kIx,
následně	následně	k6eAd1
jej	on	k3xPp3gInSc4
v	v	k7c6
roce	rok	k1gInSc6
1630	#num#	k4
koupili	koupit	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Patřil	patřit	k5eAaImAgMnS
jim	on	k3xPp3gMnPc3
až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1695	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
radnice	radnice	k1gFnSc1
stará	stará	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Národní	národní	k2eAgInSc1d1
památkový	památkový	k2eAgInSc1d1
ústav	ústav	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
SEDLÁČEK	Sedláček	k1gMnSc1
<g/>
,	,	kIx,
August	August	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hrady	hrad	k1gInPc1
<g/>
,	,	kIx,
zámky	zámek	k1gInPc1
a	a	k8xC
tvrze	tvrz	k1gFnPc1
království	království	k1gNnSc2
Českého	český	k2eAgNnSc2d1
:	:	kIx,
Plzeňsko	Plzeňsko	k1gNnSc1
a	a	k8xC
Loketsko	Loketsko	k1gNnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svazek	svazek	k1gInSc1
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
nakladatelství	nakladatelství	k1gNnSc1
F.	F.	kA
Šimáčka	Šimáček	k1gMnSc2
<g/>
,	,	kIx,
1905	#num#	k4
<g/>
.	.	kIx.
258	#num#	k4
s.	s.	k?
S.	S.	kA
184	#num#	k4
<g/>
-	-	kIx~
<g/>
185	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Ústřední	ústřední	k2eAgInSc4d1
seznam	seznam	k1gInSc4
kulturních	kulturní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Národní	národní	k2eAgInSc1d1
památkový	památkový	k2eAgInSc1d1
ústav	ústav	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Identifikátor	identifikátor	k1gInSc1
záznamu	záznam	k1gInSc2
101519	#num#	k4
:	:	kIx,
radnice	radnice	k1gFnSc1
stará	starý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Památkový	památkový	k2eAgInSc1d1
katalog	katalog	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hledat	hledat	k5eAaImF
dokumenty	dokument	k1gInPc4
v	v	k7c6
Metainformačním	Metainformační	k2eAgInSc6d1
systému	systém	k1gInSc6
NPÚ	NPÚ	kA
.	.	kIx.
1	#num#	k4
2	#num#	k4
PROKOP	Prokop	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
ml.	ml.	kA
<g/>
;	;	kIx,
SMOLA	Smola	k1gMnSc1
<g/>
,	,	kIx,
Lukáš	Lukáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sokolovsko	Sokolovsko	k1gNnSc1
<g/>
:	:	kIx,
umění	umění	k1gNnSc1
<g/>
,	,	kIx,
památky	památka	k1gFnPc1
a	a	k8xC
umělci	umělec	k1gMnPc1
do	do	k7c2
roku	rok	k1gInSc2
1945	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svazek	svazek	k1gInSc1
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sokolov	Sokolov	k1gInSc1
<g/>
:	:	kIx,
AZUS	AZUS	kA
Březová	březový	k2eAgFnSc1d1
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
svazky	svazek	k1gInPc4
(	(	kIx(
<g/>
878	#num#	k4
s.	s.	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
905485	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
904960	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
435	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Panský	panský	k2eAgInSc4d1
dům	dům	k1gInSc4
č.	č.	k?
<g/>
p.	p.	k?
<g/>
1	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oficiální	oficiální	k2eAgFnPc4d1
stránky	stránka	k1gFnPc4
města	město	k1gNnSc2
Kynšperku	Kynšperk	k1gInSc2
nad	nad	k7c7
Ohří	Ohře	k1gFnSc7
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Výstavní	výstavní	k2eAgFnSc4d1
síň	síň	k1gFnSc4
Panský	panský	k2eAgInSc4d1
dům	dům	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oficiální	oficiální	k2eAgFnPc4d1
stránky	stránka	k1gFnPc4
města	město	k1gNnSc2
Kynšperku	Kynšperk	k1gInSc2
nad	nad	k7c7
Ohří	Ohře	k1gFnSc7
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Stará	starý	k2eAgFnSc1d1
radnice	radnice	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Památky	památka	k1gFnPc1
a	a	k8xC
zajímavosti	zajímavost	k1gFnPc1
–	–	k?
Kynšperk	Kynšperk	k1gInSc1
nad	nad	k7c7
Ohří	Ohře	k1gFnSc7
na	na	k7c6
stránkách	stránka	k1gFnPc6
Místopisného	místopisný	k2eAgMnSc2d1
průvodce	průvodce	k1gMnSc2
po	po	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
</s>
<s>
Kynšperk	Kynšperk	k1gInSc1
nad	nad	k7c7
Ohří	Ohře	k1gFnSc7
–	–	k?
Panský	panský	k2eAgInSc1d1
dům	dům	k1gInSc1
či	či	k8xC
Radnice	radnice	k1gFnSc1
na	na	k7c6
stránkách	stránka	k1gFnPc6
Turistika	turistika	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
|	|	kIx~
Architektura	architektura	k1gFnSc1
a	a	k8xC
stavebnictví	stavebnictví	k1gNnSc1
</s>
