<s>
Kapsaicin	Kapsaicin	k2eAgInSc1d1	Kapsaicin
(	(	kIx(	(
<g/>
kapsicin	kapsicin	k2eAgInSc1d1	kapsicin
<g/>
,	,	kIx,	,
capsaicin	capsaicin	k2eAgInSc1d1	capsaicin
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rostlinný	rostlinný	k2eAgInSc4d1	rostlinný
alkaloid	alkaloid	k1gInSc4	alkaloid
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
zodpovědný	zodpovědný	k2eAgInSc1d1	zodpovědný
za	za	k7c4	za
pálivou	pálivý	k2eAgFnSc4d1	pálivá
chuť	chuť	k1gFnSc4	chuť
papriky	paprika	k1gFnSc2	paprika
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
hlavní	hlavní	k2eAgInSc4d1	hlavní
alkaloid	alkaloid	k1gInSc4	alkaloid
paprik	paprika	k1gFnPc2	paprika
Capsicum	Capsicum	k1gNnSc4	Capsicum
<g/>
.	.	kIx.	.
</s>
