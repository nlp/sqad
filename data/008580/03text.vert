<p>
<s>
Kapsaicin	Kapsaicin	k2eAgInSc1d1	Kapsaicin
(	(	kIx(	(
<g/>
kapsicin	kapsicin	k2eAgInSc1d1	kapsicin
<g/>
,	,	kIx,	,
capsaicin	capsaicin	k2eAgInSc1d1	capsaicin
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rostlinný	rostlinný	k2eAgInSc4d1	rostlinný
alkaloid	alkaloid	k1gInSc4	alkaloid
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
zodpovědný	zodpovědný	k2eAgInSc1d1	zodpovědný
za	za	k7c4	za
pálivou	pálivý	k2eAgFnSc4d1	pálivá
chuť	chuť	k1gFnSc4	chuť
papriky	paprika	k1gFnSc2	paprika
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
hlavní	hlavní	k2eAgInSc4d1	hlavní
alkaloid	alkaloid	k1gInSc4	alkaloid
paprik	paprika	k1gFnPc2	paprika
Capsicum	Capsicum	k1gNnSc4	Capsicum
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
savce	savec	k1gMnPc4	savec
včetně	včetně	k7c2	včetně
člověka	člověk	k1gMnSc2	člověk
je	být	k5eAaImIp3nS	být
dráždivý	dráždivý	k2eAgInSc1d1	dráždivý
a	a	k8xC	a
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
pocit	pocit	k1gInSc4	pocit
pálení	pálení	k1gNnSc2	pálení
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
tkáni	tkáň	k1gFnSc6	tkáň
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
kterou	který	k3yIgFnSc4	který
přijde	přijít	k5eAaPmIp3nS	přijít
do	do	k7c2	do
styku	styk	k1gInSc2	styk
<g/>
.	.	kIx.	.
</s>
<s>
Kapsaicin	Kapsaicin	k2eAgMnSc1d1	Kapsaicin
a	a	k8xC	a
několik	několik	k4yIc4	několik
příbuzných	příbuzný	k2eAgFnPc2d1	příbuzná
sloučenin	sloučenina	k1gFnPc2	sloučenina
se	se	k3xPyFc4	se
společně	společně	k6eAd1	společně
nazývají	nazývat	k5eAaImIp3nP	nazývat
kapsaicinoidy	kapsaicinoida	k1gFnPc1	kapsaicinoida
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
produkovány	produkován	k2eAgInPc1d1	produkován
jako	jako	k8xS	jako
sekundární	sekundární	k2eAgInPc1d1	sekundární
metabolity	metabolit	k1gInPc1	metabolit
chilli	chilli	k1gNnSc2	chilli
papričkami	paprička	k1gFnPc7	paprička
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
pro	pro	k7c4	pro
odpuzování	odpuzování	k1gNnSc4	odpuzování
některých	některý	k3yIgMnPc2	některý
býložravců	býložravec	k1gMnPc2	býložravec
a	a	k8xC	a
hub	houba	k1gFnPc2	houba
<g/>
.	.	kIx.	.
</s>
<s>
Kapsaicin	Kapsaicin	k2eAgMnSc1d1	Kapsaicin
je	být	k5eAaImIp3nS	být
rozpustný	rozpustný	k2eAgInSc1d1	rozpustný
v	v	k7c6	v
alkoholu	alkohol	k1gInSc6	alkohol
a	a	k8xC	a
tucích	tuk	k1gInPc6	tuk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Látku	látka	k1gFnSc4	látka
poprvé	poprvé	k6eAd1	poprvé
izoloval	izolovat	k5eAaBmAgInS	izolovat
z	z	k7c2	z
pálivých	pálivý	k2eAgFnPc2d1	pálivá
paprik	paprika	k1gFnPc2	paprika
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1816	[number]	k4	1816
P.	P.	kA	P.
A.	A.	kA	A.
Buchtholz	Buchtholz	k1gInSc4	Buchtholz
a	a	k8xC	a
o	o	k7c4	o
30	[number]	k4	30
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
ji	on	k3xPp3gFnSc4	on
připravil	připravit	k5eAaPmAgInS	připravit
v	v	k7c6	v
krystalické	krystalický	k2eAgFnSc6d1	krystalická
formě	forma	k1gFnSc6	forma
L.	L.	kA	L.
T.	T.	kA	T.
Tresh	Tresh	k1gMnSc1	Tresh
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
jí	jíst	k5eAaImIp3nS	jíst
dal	dát	k5eAaPmAgMnS	dát
jméno	jméno	k1gNnSc4	jméno
kapsaicin	kapsaicin	k2eAgMnSc1d1	kapsaicin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1878	[number]	k4	1878
izoloval	izolovat	k5eAaBmAgMnS	izolovat
kapsaicin	kapsaicin	k2eAgMnSc1d1	kapsaicin
také	také	k6eAd1	také
maďarský	maďarský	k2eAgMnSc1d1	maďarský
lékař	lékař	k1gMnSc1	lékař
Endre	Endr	k1gInSc5	Endr
Hogyes	Hogyes	k1gMnSc1	Hogyes
a	a	k8xC	a
prokázal	prokázat	k5eAaPmAgMnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
látka	látka	k1gFnSc1	látka
odpovědná	odpovědný	k2eAgFnSc1d1	odpovědná
za	za	k7c4	za
pálivý	pálivý	k2eAgInSc4d1	pálivý
pocit	pocit	k1gInSc4	pocit
při	při	k7c6	při
styku	styk	k1gInSc6	styk
se	s	k7c7	s
sliznicemi	sliznice	k1gFnPc7	sliznice
a	a	k8xC	a
že	že	k8xS	že
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
sekreci	sekrece	k1gFnSc4	sekrece
žaludečních	žaludeční	k2eAgFnPc2d1	žaludeční
šťáv	šťáva	k1gFnPc2	šťáva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Synteticky	synteticky	k6eAd1	synteticky
připravili	připravit	k5eAaPmAgMnP	připravit
kapsaicin	kapsaicin	k2eAgInSc1d1	kapsaicin
roku	rok	k1gInSc2	rok
1930	[number]	k4	1930
E.	E.	kA	E.
Spath	Spath	k1gInSc1	Spath
a	a	k8xC	a
F.	F.	kA	F.
S.	S.	kA	S.
Darling	Darling	k1gInSc1	Darling
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byly	být	k5eAaImAgFnP	být
z	z	k7c2	z
papriky	paprika	k1gFnSc2	paprika
izolovány	izolován	k2eAgFnPc4d1	izolována
ještě	ještě	k6eAd1	ještě
další	další	k2eAgFnPc4d1	další
látky	látka	k1gFnPc4	látka
podobného	podobný	k2eAgInSc2d1	podobný
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
dnes	dnes	k6eAd1	dnes
označujeme	označovat	k5eAaImIp1nP	označovat
společným	společný	k2eAgInSc7d1	společný
názvem	název	k1gInSc7	název
kapsaicinoidy	kapsaicinoida	k1gFnSc2	kapsaicinoida
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Působení	působení	k1gNnPc4	působení
na	na	k7c4	na
organismus	organismus	k1gInSc4	organismus
==	==	k?	==
</s>
</p>
<p>
<s>
Při	při	k7c6	při
styku	styk	k1gInSc6	styk
se	s	k7c7	s
sliznicemi	sliznice	k1gFnPc7	sliznice
vyvolávají	vyvolávat	k5eAaImIp3nP	vyvolávat
kapsaicinoidy	kapsaicinoid	k1gInPc1	kapsaicinoid
pocit	pocit	k1gInSc1	pocit
pálení	pálení	k1gNnSc2	pálení
spojený	spojený	k2eAgInSc1d1	spojený
s	s	k7c7	s
bolestí	bolest	k1gFnSc7	bolest
<g/>
.	.	kIx.	.
</s>
<s>
Pálí	pálit	k5eAaImIp3nS	pálit
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
ústech	ústa	k1gNnPc6	ústa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
na	na	k7c4	na
sliznici	sliznice	k1gFnSc4	sliznice
nosu	nos	k1gInSc2	nos
či	či	k8xC	či
oka	oko	k1gNnSc2	oko
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
v	v	k7c6	v
pepřových	pepřový	k2eAgInPc6d1	pepřový
sprejích	sprej	k1gInPc6	sprej
sloužících	sloužící	k1gMnPc2	sloužící
k	k	k7c3	k
osobní	osobní	k2eAgFnSc3d1	osobní
obraně	obrana	k1gFnSc3	obrana
<g/>
.	.	kIx.	.
</s>
<s>
Intenzivní	intenzivní	k2eAgNnSc1d1	intenzivní
dráždění	dráždění	k1gNnSc1	dráždění
sliznic	sliznice	k1gFnPc2	sliznice
kapsaicinoidy	kapsaicinoida	k1gFnSc2	kapsaicinoida
<g/>
,	,	kIx,	,
provázené	provázený	k2eAgFnSc6d1	provázená
pálením	pálení	k1gNnSc7	pálení
a	a	k8xC	a
prudkou	prudký	k2eAgFnSc7d1	prudká
bolestí	bolest	k1gFnSc7	bolest
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zprostředkováno	zprostředkován	k2eAgNnSc1d1	zprostředkováno
přes	přes	k7c4	přes
nociceptory	nociceptor	k1gMnPc4	nociceptor
<g/>
.	.	kIx.	.
</s>
<s>
Molekulární	molekulární	k2eAgFnSc1d1	molekulární
podstata	podstata	k1gFnSc1	podstata
účinku	účinek	k1gInSc2	účinek
kapsaicinoidů	kapsaicinoid	k1gInPc2	kapsaicinoid
a	a	k8xC	a
jim	on	k3xPp3gFnPc3	on
podobných	podobný	k2eAgFnPc2d1	podobná
substancí	substance	k1gFnPc2	substance
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
vazbě	vazba	k1gFnSc6	vazba
na	na	k7c4	na
specifické	specifický	k2eAgFnPc4d1	specifická
kapsaicinové	kapsaicinová	k1gFnPc4	kapsaicinová
nebo	nebo	k8xC	nebo
též	též	k9	též
vanilloidové	vanilloidový	k2eAgInPc4d1	vanilloidový
receptory	receptor	k1gInPc4	receptor
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
podstatou	podstata	k1gFnSc7	podstata
nociceptorů	nociceptor	k1gMnPc2	nociceptor
vnímajících	vnímající	k2eAgMnPc2d1	vnímající
pálivou	pálivý	k2eAgFnSc4d1	pálivá
chuť	chuť	k1gFnSc4	chuť
<g/>
.	.	kIx.	.
</s>
<s>
Vanilloidové	Vanilloidový	k2eAgInPc1d1	Vanilloidový
receptory	receptor	k1gInPc1	receptor
fungují	fungovat	k5eAaImIp3nP	fungovat
jako	jako	k9	jako
iontové	iontový	k2eAgInPc4d1	iontový
kanály	kanál	k1gInPc4	kanál
pro	pro	k7c4	pro
masivní	masivní	k2eAgInSc4d1	masivní
vstup	vstup	k1gInSc4	vstup
iontů	ion	k1gInPc2	ion
vápníku	vápník	k1gInSc2	vápník
do	do	k7c2	do
neuronu	neuron	k1gInSc2	neuron
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
otevírány	otevírán	k2eAgFnPc1d1	otevírán
látkami	látka	k1gFnPc7	látka
typu	typ	k1gInSc2	typ
kapsaicinu	kapsaicin	k2eAgFnSc4d1	kapsaicin
nebo	nebo	k8xC	nebo
též	též	k9	též
tepelným	tepelný	k2eAgInSc7d1	tepelný
podnětem	podnět	k1gInSc7	podnět
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
specifické	specifický	k2eAgInPc1d1	specifický
receptory	receptor	k1gInPc1	receptor
pro	pro	k7c4	pro
vnímání	vnímání	k1gNnSc4	vnímání
bolestivých	bolestivý	k2eAgInPc2d1	bolestivý
podnětů	podnět	k1gInPc2	podnět
<g/>
.	.	kIx.	.
</s>
<s>
Citlivost	citlivost	k1gFnSc1	citlivost
vanilloidových	vanilloidový	k2eAgInPc2d1	vanilloidový
receptorů	receptor	k1gInPc2	receptor
na	na	k7c4	na
kapsaicin	kapsaicin	k2eAgInSc4d1	kapsaicin
je	být	k5eAaImIp3nS	být
obrovská	obrovský	k2eAgFnSc1d1	obrovská
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
naše	náš	k3xOp1gInPc1	náš
nociceptory	nociceptor	k1gInPc1	nociceptor
jsou	být	k5eAaImIp3nP	být
schopny	schopen	k2eAgInPc1d1	schopen
rozpoznat	rozpoznat	k5eAaPmF	rozpoznat
kapsaicin	kapsaicin	k2eAgMnSc1d1	kapsaicin
a	a	k8xC	a
jemu	on	k3xPp3gInSc3	on
podobné	podobný	k2eAgFnPc4d1	podobná
látky	látka	k1gFnPc4	látka
již	již	k9	již
ve	v	k7c6	v
zcela	zcela	k6eAd1	zcela
nepatrných	patrný	k2eNgFnPc6d1	patrný
koncentracích	koncentrace	k1gFnPc6	koncentrace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dobách	doba	k1gFnPc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nebylo	být	k5eNaImAgNnS	být
možné	možný	k2eAgFnPc4d1	možná
koncentrace	koncentrace	k1gFnPc4	koncentrace
kapsaicinu	kapsaicin	k2eAgFnSc4d1	kapsaicin
přesně	přesně	k6eAd1	přesně
měřit	měřit	k5eAaImF	měřit
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
pro	pro	k7c4	pro
hodnocení	hodnocení	k1gNnSc4	hodnocení
pálivosti	pálivost	k1gFnSc2	pálivost
zaveden	zaveden	k2eAgInSc1d1	zaveden
Scovillův	Scovillův	k2eAgInSc1d1	Scovillův
test	test	k1gInSc1	test
pálivosti	pálivost	k1gFnSc2	pálivost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
iniciální	iniciální	k2eAgFnSc6d1	iniciální
fázi	fáze	k1gFnSc6	fáze
účinku	účinek	k1gInSc2	účinek
nastupuje	nastupovat	k5eAaImIp3nS	nastupovat
analgetická	analgetický	k2eAgFnSc1d1	analgetická
fáze	fáze	k1gFnSc1	fáze
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
kapsaicin	kapsaicin	k2eAgMnSc1d1	kapsaicin
tlumí	tlumit	k5eAaImIp3nS	tlumit
bolest	bolest	k1gFnSc4	bolest
<g/>
.	.	kIx.	.
</s>
<s>
Čeští	český	k2eAgMnPc1d1	český
vědci	vědec	k1gMnPc1	vědec
objevili	objevit	k5eAaPmAgMnP	objevit
<g/>
,	,	kIx,	,
že	že	k8xS	že
látka	látka	k1gFnSc1	látka
nejlépe	dobře	k6eAd3	dobře
tlumí	tlumit	k5eAaImIp3nS	tlumit
především	především	k9	především
chronickou	chronický	k2eAgFnSc4d1	chronická
bolest	bolest	k1gFnSc4	bolest
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pozitivní	pozitivní	k2eAgInPc1d1	pozitivní
účinky	účinek	k1gInPc1	účinek
==	==	k?	==
</s>
</p>
<p>
<s>
Pozitivní	pozitivní	k2eAgInPc1d1	pozitivní
účinky	účinek	k1gInPc1	účinek
způsobené	způsobený	k2eAgInPc1d1	způsobený
přítomností	přítomnost	k1gFnSc7	přítomnost
dusíkaté	dusíkatý	k2eAgFnPc1d1	dusíkatá
látky	látka	k1gFnPc1	látka
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
fenylalkylaminových	fenylalkylaminův	k2eAgInPc2d1	fenylalkylaminův
alkaloidů	alkaloid	k1gInPc2	alkaloid
přispívá	přispívat	k5eAaImIp3nS	přispívat
k	k	k7c3	k
podpoře	podpora	k1gFnSc3	podpora
krevní	krevní	k2eAgFnSc2d1	krevní
cirkulace	cirkulace	k1gFnSc2	cirkulace
<g/>
,	,	kIx,	,
zvýšení	zvýšení	k1gNnSc4	zvýšení
rychlosti	rychlost	k1gFnSc2	rychlost
metabolismu	metabolismus	k1gInSc2	metabolismus
<g/>
,	,	kIx,	,
stimulaci	stimulace	k1gFnSc6	stimulace
žaludeční	žaludeční	k2eAgFnSc2d1	žaludeční
sekrece	sekrece	k1gFnSc2	sekrece
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
k	k	k7c3	k
podpoře	podpora	k1gFnSc3	podpora
zažívání	zažívání	k1gNnSc2	zažívání
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Nemá	mít	k5eNaImIp3nS	mít
však	však	k9	však
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
snížení	snížení	k1gNnSc4	snížení
váhy	váha	k1gFnSc2	váha
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgFnPc4d2	veliký
dávky	dávka	k1gFnPc4	dávka
(	(	kIx(	(
<g/>
u	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
i	i	k8xC	i
malé	malý	k2eAgFnPc1d1	malá
<g/>
)	)	kIx)	)
vyvolávají	vyvolávat	k5eAaImIp3nP	vyvolávat
nevolnosti	nevolnost	k1gFnPc1	nevolnost
a	a	k8xC	a
průjem	průjem	k1gInSc1	průjem
<g/>
.	.	kIx.	.
<g/>
Léčebný	léčebný	k2eAgInSc1d1	léčebný
efekt	efekt	k1gInSc1	efekt
kapsaicinu	kapsaicin	k2eAgFnSc4d1	kapsaicin
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
projevit	projevit	k5eAaPmF	projevit
při	při	k7c6	při
dávkách	dávka	k1gFnPc6	dávka
400	[number]	k4	400
miligramů	miligram	k1gInPc2	miligram
třikrát	třikrát	k6eAd1	třikrát
týdně	týdně	k6eAd1	týdně
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
množství	množství	k1gNnSc4	množství
odpovídající	odpovídající	k2eAgNnSc4d1	odpovídající
asi	asi	k9	asi
třem	tři	k4xCgFnPc3	tři
až	až	k9	až
osmi	osm	k4xCc2	osm
paprikám	paprika	k1gFnPc3	paprika
habanera	habanera	k1gFnSc1	habanera
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
druh	druh	k1gInSc1	druh
papriky	paprika	k1gFnSc2	paprika
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
poloostrova	poloostrov	k1gInSc2	poloostrov
Yucatán	Yucatán	k1gMnSc1	Yucatán
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejpálivějším	pálivý	k2eAgNnPc3d3	nejpálivější
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pomoc	pomoc	k1gFnSc1	pomoc
při	při	k7c6	při
léčení	léčení	k1gNnSc6	léčení
nemocí	nemoc	k1gFnPc2	nemoc
===	===	k?	===
</s>
</p>
<p>
<s>
trombózy	trombóza	k1gFnPc1	trombóza
</s>
</p>
<p>
<s>
křečové	křečový	k2eAgFnPc4d1	křečová
žíly	žíla	k1gFnPc4	žíla
</s>
</p>
<p>
<s>
omrzliny	omrzlina	k1gFnPc1	omrzlina
</s>
</p>
<p>
<s>
podlitiny	podlitina	k1gFnPc1	podlitina
<g/>
,	,	kIx,	,
hematomy	hematom	k1gInPc1	hematom
</s>
</p>
<p>
<s>
hadí	hadí	k2eAgNnSc1d1	hadí
uštknutí	uštknutí	k1gNnSc1	uštknutí
</s>
</p>
<p>
<s>
Raynaudova	Raynaudův	k2eAgFnSc1d1	Raynaudova
nemoc	nemoc	k1gFnSc1	nemoc
</s>
</p>
<p>
<s>
otrava	otrava	k1gFnSc1	otrava
potravinami	potravina	k1gFnPc7	potravina
–	–	k?	–
ostré	ostrý	k2eAgNnSc4d1	ostré
chilli	chilli	k1gNnSc4	chilli
papričky	paprička	k1gFnSc2	paprička
ničí	ničí	k3xOyNgFnSc1	ničí
bakterie	bakterie	k1gFnSc1	bakterie
(	(	kIx(	(
<g/>
Vibrio	vibrio	k1gNnSc1	vibrio
vulnificus	vulnificus	k1gMnSc1	vulnificus
–	–	k?	–
původce	původce	k1gMnSc4	původce
otravy	otrava	k1gFnPc4	otrava
ústřicemi	ústřice	k1gFnPc7	ústřice
či	či	k8xC	či
špatným	špatný	k2eAgNnSc7d1	špatné
suši	suš	k1gFnSc6	suš
<g/>
,	,	kIx,	,
Escherichia	Escherichia	k1gFnSc1	Escherichia
coli	coli	k1gNnSc2	coli
<g/>
,	,	kIx,	,
Shigella	Shigella	k1gMnSc1	Shigella
<g/>
,	,	kIx,	,
Salmonella	Salmonella	k1gMnSc1	Salmonella
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
srdeční	srdeční	k2eAgFnSc2d1	srdeční
arytmie	arytmie	k1gFnSc2	arytmie
–	–	k?	–
kapsaicin	kapsaicin	k2eAgInSc1d1	kapsaicin
funguje	fungovat	k5eAaImIp3nS	fungovat
jako	jako	k9	jako
přírodní	přírodní	k2eAgInSc4d1	přírodní
blokátor	blokátor	k1gInSc4	blokátor
kalcia	kalcium	k1gNnSc2	kalcium
</s>
</p>
<p>
<s>
diabetes	diabetes	k1gInSc1	diabetes
mellitus	mellitus	k1gInSc1	mellitus
–	–	k?	–
kapsaicin	kapsaicin	k2eAgInSc1d1	kapsaicin
snižuje	snižovat	k5eAaImIp3nS	snižovat
hladinu	hladina	k1gFnSc4	hladina
krevního	krevní	k2eAgInSc2d1	krevní
cukru	cukr	k1gInSc2	cukr
</s>
</p>
<p>
<s>
ateroskleróza	ateroskleróza	k1gFnSc1	ateroskleróza
</s>
</p>
<p>
<s>
usazování	usazování	k1gNnSc1	usazování
hlenů	hlen	k1gInPc2	hlen
v	v	k7c6	v
plicích	plíce	k1gFnPc6	plíce
</s>
</p>
<p>
<s>
alkoholismus	alkoholismus	k1gInSc1	alkoholismus
</s>
</p>
<p>
<s>
podchlazení	podchlazení	k1gNnSc1	podchlazení
</s>
</p>
<p>
<s>
řezné	řezný	k2eAgFnPc1d1	řezná
rány	rána	k1gFnPc1	rána
–	–	k?	–
kajenský	kajenský	k2eAgInSc1d1	kajenský
pepř	pepř	k1gInSc1	pepř
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
zastavit	zastavit	k5eAaPmF	zastavit
krvácení	krvácení	k1gNnSc4	krvácení
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Capsaicin	Capsaicin	k2eAgInSc4d1	Capsaicin
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Scovilleova	Scovilleův	k2eAgFnSc1d1	Scovilleův
stupnice	stupnice	k1gFnSc1	stupnice
</s>
</p>
<p>
<s>
Paprička	paprička	k1gFnSc1	paprička
habanero	habanera	k1gFnSc5	habanera
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
kapsaicin	kapsaicin	k2eAgMnSc1d1	kapsaicin
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
kapsaicin	kapsaicin	k2eAgMnSc1d1	kapsaicin
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Kapsaicin	Kapsaicin	k2eAgMnSc1d1	Kapsaicin
v	v	k7c6	v
medicíně	medicína	k1gFnSc6	medicína
</s>
</p>
<p>
<s>
Osel	osel	k1gMnSc1	osel
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
-	-	kIx~	-
Nejpálivější	pálivý	k2eAgFnSc1d3	nejpálivější
paprička	paprička	k1gFnSc1	paprička
na	na	k7c6	na
světě	svět	k1gInSc6	svět
</s>
</p>
