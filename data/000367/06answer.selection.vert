<s>
Nobelium	nobelium	k1gNnSc1	nobelium
bylo	být	k5eAaImAgNnS	být
poprvé	poprvé	k6eAd1	poprvé
připraveno	připravit	k5eAaPmNgNnS	připravit
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
v	v	k7c6	v
laboratořích	laboratoř	k1gFnPc6	laboratoř
kalifornské	kalifornský	k2eAgFnSc2d1	kalifornská
university	universita	k1gFnSc2	universita
v	v	k7c4	v
Berkeley	Berkelea	k1gFnPc4	Berkelea
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
nového	nový	k2eAgInSc2d1	nový
lineárního	lineární	k2eAgInSc2d1	lineární
urychlovače	urychlovač	k1gInSc2	urychlovač
částic	částice	k1gFnPc2	částice
<g/>
.	.	kIx.	.
</s>
