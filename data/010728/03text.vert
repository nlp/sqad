<p>
<s>
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
Horváth	Horváth	k1gMnSc1	Horváth
(	(	kIx(	(
<g/>
*	*	kIx~	*
18	[number]	k4	18
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1977	[number]	k4	1977
Vlašim	Vlašim	k1gFnSc1	Vlašim
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	s	k7c7	s
12	[number]	k4	12
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2005	[number]	k4	2005
stal	stát	k5eAaPmAgMnS	stát
vítězem	vítěz	k1gMnSc7	vítěz
druhého	druhý	k4xOgInSc2	druhý
ročníku	ročník	k1gInSc2	ročník
soutěže	soutěž	k1gFnSc2	soutěž
Česko	Česko	k1gNnSc1	Česko
hledá	hledat	k5eAaImIp3nS	hledat
SuperStar	superstar	k1gFnSc4	superstar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biografie	biografie	k1gFnSc2	biografie
==	==	k?	==
</s>
</p>
<p>
<s>
Zpěvák	Zpěvák	k1gMnSc1	Zpěvák
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgMnSc4	jenž
jméno	jméno	k1gNnSc1	jméno
je	být	k5eAaImIp3nS	být
spojeno	spojit	k5eAaPmNgNnS	spojit
s	s	k7c7	s
vítězstvím	vítězství	k1gNnSc7	vítězství
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
řadě	řada	k1gFnSc6	řada
soutěže	soutěž	k1gFnSc2	soutěž
Česko	Česko	k1gNnSc1	Česko
hledá	hledat	k5eAaImIp3nS	hledat
Superstar	superstar	k1gFnSc4	superstar
<g/>
.	.	kIx.	.
</s>
<s>
Muzikální	muzikální	k2eAgFnSc2d1	muzikální
schopnosti	schopnost	k1gFnSc2	schopnost
však	však	k9	však
projevoval	projevovat	k5eAaImAgInS	projevovat
již	již	k6eAd1	již
od	od	k7c2	od
útlého	útlý	k2eAgInSc2d1	útlý
věku	věk	k1gInSc2	věk
a	a	k8xC	a
v	v	k7c6	v
letech	let	k1gInPc6	let
teenagerovských	teenagerovský	k2eAgInPc6d1	teenagerovský
úspěšně	úspěšně	k6eAd1	úspěšně
koncertoval	koncertovat	k5eAaImAgMnS	koncertovat
ve	v	k7c6	v
vlastní	vlastní	k2eAgFnSc6d1	vlastní
skupině	skupina	k1gFnSc6	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Vlasta	Vlasta	k1gMnSc1	Vlasta
svým	svůj	k3xOyFgNnSc7	svůj
odhodláním	odhodlání	k1gNnSc7	odhodlání
a	a	k8xC	a
přístupem	přístup	k1gInSc7	přístup
k	k	k7c3	k
hudební	hudební	k2eAgFnSc3d1	hudební
tvorbě	tvorba	k1gFnSc3	tvorba
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
osobnosti	osobnost	k1gFnSc3	osobnost
respektu	respekt	k1gInSc2	respekt
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
showbyznysu	showbyznys	k1gInSc6	showbyznys
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c4	za
debutové	debutový	k2eAgNnSc4d1	debutové
album	album	k1gNnSc4	album
"	"	kIx"	"
<g/>
Místo	místo	k7c2	místo
zázraků	zázrak	k1gInPc2	zázrak
<g/>
"	"	kIx"	"
dostal	dostat	k5eAaPmAgInS	dostat
platinovou	platinový	k2eAgFnSc4d1	platinová
desku	deska	k1gFnSc4	deska
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
byl	být	k5eAaImAgInS	být
diváky	divák	k1gMnPc7	divák
zvolen	zvolit	k5eAaPmNgMnS	zvolit
Objevem	objev	k1gInSc7	objev
roku	rok	k1gInSc2	rok
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
Český	český	k2eAgInSc1d1	český
slavík	slavík	k1gInSc1	slavík
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
úspěšný	úspěšný	k2eAgMnSc1d1	úspěšný
byl	být	k5eAaImAgInS	být
i	i	k9	i
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
Deska	deska	k1gFnSc1	deska
roku	rok	k1gInSc2	rok
Allianz	Allianza	k1gFnPc2	Allianza
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
získal	získat	k5eAaPmAgMnS	získat
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
nejprodávanější	prodávaný	k2eAgNnSc4d3	nejprodávanější
debutové	debutový	k2eAgNnSc4d1	debutové
album	album	k1gNnSc4	album
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
vydal	vydat	k5eAaPmAgInS	vydat
další	další	k2eAgInSc4d1	další
CD	CD	kA	CD
"	"	kIx"	"
<g/>
Do	do	k7c2	do
peří	peří	k1gNnSc2	peří
nefoukej	foukat	k5eNaImRp2nS	foukat
<g/>
"	"	kIx"	"
a	a	k8xC	a
nazpíval	nazpívat	k5eAaBmAgMnS	nazpívat
titulní	titulní	k2eAgFnSc4d1	titulní
píseň	píseň	k1gFnSc4	píseň
pro	pro	k7c4	pro
televizní	televizní	k2eAgInSc4d1	televizní
seriál	seriál	k1gInSc4	seriál
Horákovi	Horákovi	k1gRnPc1	Horákovi
"	"	kIx"	"
<g/>
Láska	láska	k1gFnSc1	láska
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
nám	my	k3xPp1nPc3	my
osudem	osud	k1gInSc7	osud
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
vysílal	vysílat	k5eAaImAgInS	vysílat
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
televizi	televize	k1gFnSc6	televize
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
singlem	singl	k1gInSc7	singl
"	"	kIx"	"
<g/>
Adios	Adios	k1gInSc1	Adios
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
Vlasta	Vlasta	k1gMnSc1	Vlasta
Horváth	Horváth	k1gMnSc1	Horváth
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
českého	český	k2eAgNnSc2d1	české
národního	národní	k2eAgNnSc2d1	národní
kola	kolo	k1gNnSc2	kolo
Eurosong	Eurosonga	k1gFnPc2	Eurosonga
<g/>
.	.	kIx.	.
</s>
<s>
Nazpíval	nazpívat	k5eAaPmAgMnS	nazpívat
rovněž	rovněž	k9	rovněž
píseň	píseň	k1gFnSc4	píseň
k	k	k7c3	k
pohádce	pohádka	k1gFnSc3	pohádka
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Trošky	troška	k1gFnSc2	troška
"	"	kIx"	"
<g/>
Nejkrásnější	krásný	k2eAgFnSc1d3	nejkrásnější
hádanka	hádanka	k1gFnSc1	hádanka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgInPc3d1	další
významným	významný	k2eAgInPc3d1	významný
počinům	počin	k1gInPc3	počin
patří	patřit	k5eAaImIp3nS	patřit
hostování	hostování	k1gNnSc1	hostování
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
skupiny	skupina	k1gFnSc2	skupina
Čechomor	Čechomora	k1gFnPc2	Čechomora
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
a	a	k8xC	a
zásadním	zásadní	k2eAgInSc7d1	zásadní
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
Vlastu	Vlasta	k1gFnSc4	Vlasta
Horvátha	Horvátha	k1gFnSc1	Horvátha
jeho	jeho	k3xOp3gFnSc1	jeho
kapela	kapela	k1gFnSc1	kapela
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yIgFnSc7	který
nyní	nyní	k6eAd1	nyní
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
<g/>
.	.	kIx.	.
</s>
<s>
Tvorba	tvorba	k1gFnSc1	tvorba
nových	nový	k2eAgFnPc2d1	nová
písní	píseň	k1gFnPc2	píseň
je	být	k5eAaImIp3nS	být
důkazem	důkaz	k1gInSc7	důkaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
Vlasta	Vlasta	k1gMnSc1	Vlasta
Horváth	Horváth	k1gMnSc1	Horváth
je	být	k5eAaImIp3nS	být
zpěvákem	zpěvák	k1gMnSc7	zpěvák
vyzrálým	vyzrálý	k2eAgMnSc7d1	vyzrálý
<g/>
,	,	kIx,	,
o	o	k7c6	o
čemž	což	k3yRnSc6	což
svědčí	svědčit	k5eAaImIp3nS	svědčit
jeho	jeho	k3xOp3gFnSc1	jeho
jasná	jasný	k2eAgFnSc1d1	jasná
umělecká	umělecký	k2eAgFnSc1d1	umělecká
představa	představa	k1gFnSc1	představa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
vydal	vydat	k5eAaPmAgInS	vydat
nový	nový	k2eAgInSc1d1	nový
singl	singl	k1gInSc1	singl
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Téma	téma	k1gNnSc1	téma
lásky	láska	k1gFnSc2	láska
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
hudba	hudba	k1gFnSc1	hudba
Vlasta	Vlasta	k1gMnSc1	Vlasta
Horváth	Horváth	k1gMnSc1	Horváth
<g/>
,	,	kIx,	,
text	text	k1gInSc1	text
Viktor	Viktor	k1gMnSc1	Viktor
Dyk	Dyk	k?	Dyk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
poté	poté	k6eAd1	poté
druhý	druhý	k4xOgInSc4	druhý
singl	singl	k1gInSc4	singl
"	"	kIx"	"
<g/>
To	ten	k3xDgNnSc1	ten
máme	mít	k5eAaImIp1nP	mít
znát	znát	k5eAaImF	znát
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
hudba	hudba	k1gFnSc1	hudba
Vlasta	Vlasta	k1gMnSc1	Vlasta
Horváth	Horváth	k1gMnSc1	Horváth
<g/>
,	,	kIx,	,
text	text	k1gInSc1	text
Eliška	Eliška	k1gFnSc1	Eliška
Hlavová	Hlavová	k1gFnSc1	Hlavová
<g/>
)	)	kIx)	)
a	a	k8xC	a
svoji	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
vánoční	vánoční	k2eAgFnSc4d1	vánoční
píseň	píseň	k1gFnSc4	píseň
"	"	kIx"	"
<g/>
Přihořívá	přihořívat	k5eAaImIp3nS	přihořívat
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
hudba	hudba	k1gFnSc1	hudba
Vlasta	Vlasta	k1gMnSc1	Vlasta
Horváth	Horváth	k1gMnSc1	Horváth
<g/>
,	,	kIx,	,
text	text	k1gInSc1	text
Eliška	Eliška	k1gFnSc1	Eliška
Hlavová	Hlavová	k1gFnSc1	Hlavová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
2014	[number]	k4	2014
vydal	vydat	k5eAaPmAgMnS	vydat
své	svůj	k3xOyFgNnSc4	svůj
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
třetí	třetí	k4xOgFnSc2	třetí
CD	CD	kA	CD
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
To	ten	k3xDgNnSc1	ten
máme	mít	k5eAaImIp1nP	mít
znát	znát	k5eAaImF	znát
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
hudební	hudební	k2eAgFnSc6d1	hudební
stránce	stránka	k1gFnSc6	stránka
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
výhradně	výhradně	k6eAd1	výhradně
autorské	autorský	k2eAgNnSc4d1	autorské
album	album	k1gNnSc4	album
Vlasty	Vlasta	k1gMnSc2	Vlasta
Horvátha	Horváth	k1gMnSc2	Horváth
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
songy	song	k1gInPc4	song
laděné	laděný	k2eAgInPc4d1	laděný
do	do	k7c2	do
pop-rocku	popock	k1gInSc2	pop-rock
s	s	k7c7	s
bluesovým	bluesový	k2eAgNnSc7d1	bluesové
cítěním	cítění	k1gNnSc7	cítění
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
pozornost	pozornost	k1gFnSc4	pozornost
stojí	stát	k5eAaImIp3nP	stát
i	i	k9	i
texty	text	k1gInPc1	text
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mají	mít	k5eAaImIp3nP	mít
hloubku	hloubka	k1gFnSc4	hloubka
a	a	k8xC	a
cit	cit	k1gInSc4	cit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgMnSc1d1	další
jeho	jeho	k3xOp3gFnSc7	jeho
aktivitou	aktivita	k1gFnSc7	aktivita
je	být	k5eAaImIp3nS	být
účinkování	účinkování	k1gNnSc1	účinkování
v	v	k7c6	v
RockOpeře	RockOpera	k1gFnSc6	RockOpera
Praha	Praha	k1gFnSc1	Praha
v	v	k7c6	v
rockové	rockový	k2eAgFnSc6d1	rocková
opeře	opera	k1gFnSc6	opera
"	"	kIx"	"
<g/>
Oidipus	Oidipus	k1gMnSc1	Oidipus
Tyranus	Tyranus	k1gMnSc1	Tyranus
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
alternuje	alternovat	k5eAaImIp3nS	alternovat
s	s	k7c7	s
Kamilem	Kamil	k1gMnSc7	Kamil
Střihavkou	Střihavka	k1gMnSc7	Střihavka
<g/>
,	,	kIx,	,
v	v	k7c6	v
rockové	rockový	k2eAgFnSc6d1	rocková
opeře	opera	k1gFnSc6	opera
"	"	kIx"	"
<g/>
Antigona	Antigona	k1gFnSc1	Antigona
<g/>
"	"	kIx"	"
a	a	k8xC	a
v	v	k7c6	v
metal	metal	k1gInSc1	metal
opeře	opera	k1gFnSc3	opera
"	"	kIx"	"
<g/>
7	[number]	k4	7
proti	proti	k7c3	proti
Thébám	Théby	k1gFnPc3	Théby
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
stojí	stát	k5eAaImIp3nS	stát
za	za	k7c4	za
zmínku	zmínka	k1gFnSc4	zmínka
jeho	on	k3xPp3gNnSc2	on
působení	působení	k1gNnSc2	působení
jako	jako	k8xS	jako
pedagoga	pedagog	k1gMnSc2	pedagog
na	na	k7c6	na
Mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
konzervatoři	konzervatoř	k1gFnSc6	konzervatoř
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Vyučuje	vyučovat	k5eAaImIp3nS	vyučovat
zde	zde	k6eAd1	zde
interpretaci	interpretace	k1gFnSc4	interpretace
na	na	k7c4	na
oddělení	oddělení	k1gNnSc4	oddělení
Rockové	rockový	k2eAgFnSc2d1	rocková
opery	opera	k1gFnSc2	opera
a	a	k8xC	a
Popu	pop	k1gInSc2	pop
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
1.3	[number]	k4	1.3
<g/>
.	.	kIx.	.
vychází	vycházet	k5eAaImIp3nS	vycházet
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
již	již	k6eAd1	již
čtvrté	čtvrtá	k1gFnPc4	čtvrtá
-	-	kIx~	-
po	po	k7c6	po
hudební	hudební	k2eAgFnSc6d1	hudební
i	i	k8xC	i
textové	textový	k2eAgFnSc6d1	textová
stránce	stránka	k1gFnSc6	stránka
výhradně	výhradně	k6eAd1	výhradně
autorské	autorský	k2eAgFnSc6d1	autorská
CD	CD	kA	CD
<g/>
.	.	kIx.	.
</s>
<s>
CD	CD	kA	CD
kromě	kromě	k7c2	kromě
10	[number]	k4	10
nových	nový	k2eAgFnPc2d1	nová
písní	píseň	k1gFnPc2	píseň
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
i	i	k9	i
bonusovou	bonusový	k2eAgFnSc4d1	bonusová
píseň	píseň	k1gFnSc4	píseň
-	-	kIx~	-
Dneska	Dneska	k?	Dneska
jsem	být	k5eAaImIp1nS	být
tvůj	tvůj	k3xOp2gMnSc1	tvůj
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
text	text	k1gInSc4	text
této	tento	k3xDgFnSc2	tento
písně	píseň	k1gFnSc2	píseň
se	se	k3xPyFc4	se
postaral	postarat	k5eAaPmAgMnS	postarat
český	český	k2eAgMnSc1d1	český
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
textař	textař	k1gMnSc1	textař
<g/>
,	,	kIx,	,
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
multiinstrumentalista	multiinstrumentalista	k1gMnSc1	multiinstrumentalista
<g/>
,	,	kIx,	,
muzikálový	muzikálový	k2eAgMnSc1d1	muzikálový
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
producent	producent	k1gMnSc1	producent
Viktor	Viktor	k1gMnSc1	Viktor
Dyk	Dyk	k?	Dyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
S	s	k7c7	s
kapelou	kapela	k1gFnSc7	kapela
Frenzy	Frenza	k1gFnSc2	Frenza
===	===	k?	===
</s>
</p>
<p>
<s>
Frenzy	Frenza	k1gFnSc2	Frenza
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
-	-	kIx~	-
klip	klip	k1gInSc4	klip
Stále	stále	k6eAd1	stále
to	ten	k3xDgNnSc4	ten
cítím	cítit	k5eAaImIp1nS	cítit
</s>
</p>
<p>
<s>
===	===	k?	===
Sólová	sólový	k2eAgNnPc4d1	sólové
alba	album	k1gNnPc4	album
===	===	k?	===
</s>
</p>
<p>
<s>
Místo	místo	k7c2	místo
zázraků	zázrak	k1gInPc2	zázrak
(	(	kIx(	(
<g/>
24.10	[number]	k4	24.10
<g/>
.2005	.2005	k4	.2005
<g/>
)	)	kIx)	)
-	-	kIx~	-
klip	klip	k1gInSc1	klip
Víra	víra	k1gFnSc1	víra
Nevinná	vinný	k2eNgNnPc5d1	nevinné
</s>
</p>
<p>
<s>
Do	do	k7c2	do
peří	peří	k1gNnSc2	peří
nefoukej	foukat	k5eNaImRp2nS	foukat
(	(	kIx(	(
<g/>
1.11	[number]	k4	1.11
<g/>
.2006	.2006	k4	.2006
<g/>
)	)	kIx)	)
-	-	kIx~	-
klipy	klip	k1gInPc1	klip
Adios	Adiosa	k1gFnPc2	Adiosa
<g/>
,	,	kIx,	,
Zoufale	zoufale	k6eAd1	zoufale
sám	sám	k3xTgMnSc1	sám
</s>
</p>
<p>
<s>
To	ten	k3xDgNnSc1	ten
máme	mít	k5eAaImIp1nP	mít
znát	znát	k5eAaImF	znát
(	(	kIx(	(
<g/>
31.7	[number]	k4	31.7
<g/>
.2014	.2014	k4	.2014
<g/>
)	)	kIx)	)
-	-	kIx~	-
klipy	klip	k1gInPc1	klip
Téma	téma	k1gNnSc2	téma
lásky	láska	k1gFnSc2	láska
<g/>
,	,	kIx,	,
To	ten	k3xDgNnSc1	ten
máme	mít	k5eAaImIp1nP	mít
znát	znát	k5eAaImF	znát
<g/>
,	,	kIx,	,
Kousek	kousek	k6eAd1	kousek
mýho	mýho	k?	mýho
já	já	k3xPp1nSc1	já
</s>
</p>
<p>
<s>
Stále	stále	k6eAd1	stále
nevěřím	věřit	k5eNaImIp1nS	věřit
(	(	kIx(	(
<g/>
1.3	[number]	k4	1.3
<g/>
.2019	.2019	k4	.2019
<g/>
)	)	kIx)	)
-	-	kIx~	-
klipy	klip	k1gInPc4	klip
Stále	stále	k6eAd1	stále
nevěřím	věřit	k5eNaImIp1nS	věřit
<g/>
,	,	kIx,	,
Běž	běžet	k5eAaImRp2nS	běžet
dál	daleko	k6eAd2	daleko
ke	k	k7c3	k
svým	svůj	k3xOyFgInPc3	svůj
snům	sen	k1gInPc3	sen
</s>
</p>
<p>
<s>
===	===	k?	===
Jiné	jiný	k2eAgFnPc1d1	jiná
===	===	k?	===
</s>
</p>
<p>
<s>
Česko	Česko	k1gNnSc1	Česko
hledá	hledat	k5eAaImIp3nS	hledat
SuperStar	superstar	k1gFnSc1	superstar
Top	topit	k5eAaImRp2nS	topit
12	[number]	k4	12
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
–	–	k?	–
píseň	píseň	k1gFnSc1	píseň
Mandy	Manda	k1gFnSc2	Manda
</s>
</p>
<p>
<s>
SuperVánoce	SuperVánoce	k1gFnSc1	SuperVánoce
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
–	–	k?	–
píseň	píseň	k1gFnSc4	píseň
Jsou	být	k5eAaImIp3nP	být
svátky	svátek	k1gInPc4	svátek
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
Horváth	Horváth	k1gMnSc1	Horváth
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
stránka	stránka	k1gFnSc1	stránka
Vlasty	Vlasta	k1gMnSc2	Vlasta
Horvátha	Horváth	k1gMnSc2	Horváth
</s>
</p>
<p>
<s>
Vlasta	Vlasta	k1gFnSc1	Vlasta
Horváth	Horvátha	k1gFnPc2	Horvátha
na	na	k7c4	na
Maxmuzik	Maxmuzik	k1gInSc4	Maxmuzik
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
