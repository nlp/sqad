<p>
<s>
Antonín	Antonín	k1gMnSc1	Antonín
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Horák	Horák	k1gMnSc1	Horák
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1875	[number]	k4	1875
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
12	[number]	k4	12
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1910	[number]	k4	1910
Bělehrad	Bělehrad	k1gInSc1	Bělehrad
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
učitelský	učitelský	k2eAgInSc4d1	učitelský
ústav	ústav	k1gInSc4	ústav
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
absolvování	absolvování	k1gNnSc6	absolvování
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
učil	učít	k5eAaPmAgMnS	učít
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
se	se	k3xPyFc4	se
vzdělával	vzdělávat	k5eAaImAgMnS	vzdělávat
v	v	k7c6	v
hudbě	hudba	k1gFnSc6	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Klavír	klavír	k1gInSc4	klavír
studoval	studovat	k5eAaImAgMnS	studovat
u	u	k7c2	u
Ondřeje	Ondřej	k1gMnSc2	Ondřej
Horníka	Horník	k1gMnSc2	Horník
a	a	k8xC	a
ve	v	k7c6	v
skladbě	skladba	k1gFnSc6	skladba
byl	být	k5eAaImAgMnS	být
jeho	jeho	k3xOp3gMnSc1	jeho
učitelem	učitel	k1gMnSc7	učitel
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Fibich	Fibich	k1gMnSc1	Fibich
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1901	[number]	k4	1901
byl	být	k5eAaImAgMnS	být
dirigentem	dirigent	k1gMnSc7	dirigent
divadelního	divadelní	k2eAgInSc2d1	divadelní
orchestru	orchestr	k1gInSc2	orchestr
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
a	a	k8xC	a
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
dirigentem	dirigent	k1gMnSc7	dirigent
Městského	městský	k2eAgNnSc2d1	Městské
divadla	divadlo	k1gNnSc2	divadlo
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
řídil	řídit	k5eAaImAgInS	řídit
orchestr	orchestr	k1gInSc1	orchestr
v	v	k7c6	v
Osijeku	Osijek	k1gInSc6	Osijek
(	(	kIx(	(
<g/>
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
1	[number]	k4	1
<g/>
.	.	kIx.	.
kapelníkem	kapelník	k1gMnSc7	kapelník
Smíchovské	smíchovský	k2eAgFnSc2d1	Smíchovská
operety	opereta	k1gFnSc2	opereta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
roku	rok	k1gInSc2	rok
1909	[number]	k4	1909
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
jako	jako	k9	jako
1	[number]	k4	1
<g/>
.	.	kIx.	.
dirigent	dirigent	k1gMnSc1	dirigent
opery	opera	k1gFnSc2	opera
v	v	k7c6	v
Bělehradě	Bělehrad	k1gInSc6	Bělehrad
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hned	hned	k6eAd1	hned
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
vážně	vážně	k6eAd1	vážně
onemocněl	onemocnět	k5eAaPmAgMnS	onemocnět
a	a	k8xC	a
záhy	záhy	k6eAd1	záhy
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
skladbách	skladba	k1gFnPc6	skladba
se	se	k3xPyFc4	se
zaměřil	zaměřit	k5eAaPmAgMnS	zaměřit
na	na	k7c6	na
zhudebnění	zhudebnění	k1gNnSc6	zhudebnění
vrcholných	vrcholný	k2eAgNnPc2d1	vrcholné
děl	dělo	k1gNnPc2	dělo
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Opery	opera	k1gFnSc2	opera
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c4	na
večer	večer	k1gInSc4	večer
Bílé	bílý	k2eAgFnSc2d1	bílá
soboty	sobota	k1gFnSc2	sobota
(	(	kIx(	(
<g/>
libreto	libreto	k1gNnSc1	libreto
Adolf	Adolf	k1gMnSc1	Adolf
Wenig	Wenig	k1gMnSc1	Wenig
<g/>
,	,	kIx,	,
Národní	národní	k2eAgNnSc1d1	národní
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
1898	[number]	k4	1898
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Babička	babička	k1gFnSc1	babička
(	(	kIx(	(
<g/>
libreto	libreto	k1gNnSc1	libreto
Adolf	Adolf	k1gMnSc1	Adolf
Wenig	Wenig	k1gMnSc1	Wenig
podle	podle	k7c2	podle
Boženy	Božena	k1gFnSc2	Božena
Němcové	Němcová	k1gFnSc2	Němcová
<g/>
,	,	kIx,	,
Národní	národní	k2eAgNnSc1d1	národní
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
1900	[number]	k4	1900
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Do	do	k7c2	do
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
nedokončena	dokončen	k2eNgFnSc1d1	nedokončena
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Jiná	jiný	k2eAgNnPc1d1	jiné
jevištní	jevištní	k2eAgNnPc1d1	jevištní
díla	dílo	k1gNnPc1	dílo
===	===	k?	===
</s>
</p>
<p>
<s>
Nosáček	Nosáček	k1gInSc1	Nosáček
(	(	kIx(	(
<g/>
melodram	melodram	k1gInSc1	melodram
na	na	k7c4	na
text	text	k1gInSc4	text
Adolfa	Adolf	k1gMnSc2	Adolf
Weniga	Wenig	k1gMnSc2	Wenig
podle	podle	k7c2	podle
pohádky	pohádka	k1gFnSc2	pohádka
Wilhelma	Wilhelm	k1gMnSc2	Wilhelm
Hauffa	Hauff	k1gMnSc2	Hauff
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
májová	májový	k2eAgFnSc1d1	Májová
noc	noc	k1gFnSc1	noc
(	(	kIx(	(
<g/>
kantáta	kantáta	k1gFnSc1	kantáta
na	na	k7c4	na
text	text	k1gInSc4	text
Karla	Karel	k1gMnSc2	Karel
Jaromíra	Jaromír	k1gMnSc2	Jaromír
Erbena	Erben	k1gMnSc2	Erben
</s>
</p>
<p>
<s>
Stranický	stranický	k2eAgInSc1d1	stranický
dudák	dudák	k1gInSc1	dudák
(	(	kIx(	(
<g/>
scénická	scénický	k2eAgFnSc1d1	scénická
hudba	hudba	k1gFnSc1	hudba
ke	k	k7c3	k
hře	hra	k1gFnSc3	hra
Josefa	Josef	k1gMnSc2	Josef
Kajetána	Kajetán	k1gMnSc2	Kajetán
Tyla	Tyl	k1gMnSc2	Tyl
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Lucerna	lucerna	k1gFnSc1	lucerna
(	(	kIx(	(
<g/>
scénická	scénický	k2eAgFnSc1d1	scénická
hudba	hudba	k1gFnSc1	hudba
ke	k	k7c3	k
hře	hra	k1gFnSc3	hra
Aloise	Alois	k1gMnSc2	Alois
Jiráska	Jirásek	k1gMnSc2	Jirásek
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Výrava	Výrava	k1gFnSc1	Výrava
(	(	kIx(	(
<g/>
předehra	předehra	k1gFnSc1	předehra
ke	k	k7c3	k
hře	hra	k1gFnSc3	hra
Františka	František	k1gMnSc2	František
Adolfa	Adolf	k1gMnSc2	Adolf
Šuberta	Šubert	k1gMnSc2	Šubert
</s>
</p>
<p>
<s>
===	===	k?	===
Ostatní	ostatní	k2eAgFnPc4d1	ostatní
skladby	skladba	k1gFnPc4	skladba
===	===	k?	===
</s>
</p>
<p>
<s>
Smyčcová	smyčcový	k2eAgFnSc1d1	smyčcová
serenáda	serenáda	k1gFnSc1	serenáda
</s>
</p>
<p>
<s>
Symfonie	symfonie	k1gFnSc1	symfonie
</s>
</p>
<p>
<s>
Písně	píseň	k1gFnPc1	píseň
na	na	k7c4	na
slova	slovo	k1gNnPc4	slovo
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Vrchlického	Vrchlický	k1gMnSc2	Vrchlický
a	a	k8xC	a
Josefa	Josef	k1gMnSc2	Josef
Václava	Václav	k1gMnSc2	Václav
Sládka	Sládek	k1gMnSc2	Sládek
<g/>
,	,	kIx,	,
Vítězslava	Vítězslav	k1gMnSc2	Vítězslav
Hálka	hálka	k1gFnSc1	hálka
a	a	k8xC	a
Františka	Františka	k1gFnSc1	Františka
Ladislava	Ladislav	k1gMnSc2	Ladislav
Čelakovského	Čelakovský	k2eAgInSc2d1	Čelakovský
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Antonín	Antonín	k1gMnSc1	Antonín
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Horák	Horák	k1gMnSc1	Horák
</s>
</p>
<p>
<s>
Československý	československý	k2eAgInSc1d1	československý
hudební	hudební	k2eAgInSc1d1	hudební
slovník	slovník	k1gInSc1	slovník
I	i	k9	i
(	(	kIx(	(
<g/>
A	a	k9	a
<g/>
–	–	k?	–
<g/>
L	L	kA	L
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
SHV	SHV	kA	SHV
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
Pazdírkův	Pazdírkův	k2eAgInSc1d1	Pazdírkův
hudební	hudební	k2eAgInSc1d1	hudební
slovník	slovník	k1gInSc1	slovník
naučný	naučný	k2eAgInSc1d1	naučný
:	:	kIx,	:
Část	část	k1gFnSc1	část
osobní	osobní	k2eAgFnSc1d1	osobní
<g/>
.	.	kIx.	.
</s>
<s>
II	II	kA	II
<g/>
,	,	kIx,	,
Svazek	svazek	k1gInSc1	svazek
prvý	prvý	k4xOgInSc1	prvý
<g/>
.	.	kIx.	.
</s>
<s>
A-K	A-K	k?	A-K
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
1937	[number]	k4	1937
</s>
</p>
<p>
<s>
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Nejedlý	Nejedlý	k1gMnSc1	Nejedlý
<g/>
:	:	kIx,	:
Dějiny	dějiny	k1gFnPc1	dějiny
opery	opera	k1gFnSc2	opera
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1949	[number]	k4	1949
</s>
</p>
