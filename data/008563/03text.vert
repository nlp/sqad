<p>
<s>
Eva	Eva	k1gFnSc1	Eva
Anna	Anna	k1gFnSc1	Anna
Paula	Paula	k1gFnSc1	Paula
Braunová	Braunová	k1gFnSc1	Braunová
později	pozdě	k6eAd2	pozdě
Eva	Eva	k1gFnSc1	Eva
Anna	Anna	k1gFnSc1	Anna
Paula	Paula	k1gFnSc1	Paula
Hitlerová	Hitlerová	k1gFnSc1	Hitlerová
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1912	[number]	k4	1912
Mnichov	Mnichov	k1gInSc1	Mnichov
–	–	k?	–
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1945	[number]	k4	1945
Berlín	Berlín	k1gInSc1	Berlín
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
dlouholetá	dlouholetý	k2eAgFnSc1d1	dlouholetá
milenka	milenka	k1gFnSc1	milenka
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
manželka	manželka	k1gFnSc1	manželka
Adolfa	Adolf	k1gMnSc2	Adolf
Hitlera	Hitler	k1gMnSc2	Hitler
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Eva	Eva	k1gFnSc1	Eva
Braunová	Braunová	k1gFnSc1	Braunová
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
dcerou	dcera	k1gFnSc7	dcera
učitele	učitel	k1gMnSc2	učitel
Friedricha	Friedrich	k1gMnSc2	Friedrich
Brauna	Braun	k1gMnSc2	Braun
a	a	k8xC	a
Franzisky	Franziska	k1gFnSc2	Franziska
Braunové	Braunová	k1gFnSc2	Braunová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sedmnácti	sedmnáct	k4xCc6	sedmnáct
letech	léto	k1gNnPc6	léto
vzala	vzít	k5eAaPmAgFnS	vzít
práci	práce	k1gFnSc4	práce
asistentky	asistentka	k1gFnSc2	asistentka
ve	v	k7c6	v
fotoateliéru	fotoateliér	k1gInSc6	fotoateliér
Heinricha	Heinrich	k1gMnSc2	Heinrich
Hoffmanna	Hoffmann	k1gMnSc2	Hoffmann
<g/>
,	,	kIx,	,
oficiálního	oficiální	k2eAgMnSc2d1	oficiální
fotografa	fotograf	k1gMnSc2	fotograf
NSDAP	NSDAP	kA	NSDAP
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
seznámila	seznámit	k5eAaPmAgFnS	seznámit
s	s	k7c7	s
Hitlerem	Hitler	k1gMnSc7	Hitler
<g/>
.	.	kIx.	.
</s>
<s>
Hitler	Hitler	k1gMnSc1	Hitler
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
často	často	k6eAd1	často
zval	zvát	k5eAaImAgInS	zvát
na	na	k7c4	na
jídlo	jídlo	k1gNnSc4	jídlo
nebo	nebo	k8xC	nebo
do	do	k7c2	do
kina	kino	k1gNnSc2	kino
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
se	se	k3xPyFc4	se
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
k	k	k7c3	k
Hitlerovi	Hitler	k1gMnSc3	Hitler
do	do	k7c2	do
Berghofu	Berghof	k1gInSc2	Berghof
poblíž	poblíž	k6eAd1	poblíž
Berchtesgadenu	Berchtesgaden	k2eAgFnSc4d1	Berchtesgaden
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
politický	politický	k2eAgInSc1d1	politický
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
Hitlera	Hitler	k1gMnSc4	Hitler
není	být	k5eNaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
minimální	minimální	k2eAgMnSc1d1	minimální
<g/>
.	.	kIx.	.
</s>
<s>
Adolf	Adolf	k1gMnSc1	Adolf
a	a	k8xC	a
Eva	Eva	k1gFnSc1	Eva
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
neobjevili	objevit	k5eNaPmAgMnP	objevit
na	na	k7c6	na
veřejnosti	veřejnost	k1gFnSc6	veřejnost
jako	jako	k9	jako
pár	pár	k4xCyI	pár
<g/>
,	,	kIx,	,
Hitler	Hitler	k1gMnSc1	Hitler
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
obával	obávat	k5eAaImAgMnS	obávat
ztráty	ztráta	k1gFnPc4	ztráta
popularity	popularita	k1gFnSc2	popularita
mezi	mezi	k7c7	mezi
ženami	žena	k1gFnPc7	žena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
března	březen	k1gInSc2	březen
a	a	k8xC	a
dubna	duben	k1gInSc2	duben
1945	[number]	k4	1945
odjela	odjet	k5eAaPmAgFnS	odjet
Braunová	Braunová	k1gFnSc1	Braunová
z	z	k7c2	z
Mnichova	Mnichov	k1gInSc2	Mnichov
do	do	k7c2	do
Hitlerova	Hitlerův	k2eAgInSc2d1	Hitlerův
bunkru	bunkr	k1gInSc2	bunkr
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
29	[number]	k4	29
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1945	[number]	k4	1945
uzavřeli	uzavřít	k5eAaPmAgMnP	uzavřít
manželství	manželství	k1gNnSc4	manželství
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc4d1	následující
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
po	po	k7c6	po
zjištění	zjištění	k1gNnSc6	zjištění
<g/>
,	,	kIx,	,
že	že	k8xS	že
Berlín	Berlín	k1gInSc1	Berlín
bude	být	k5eAaImBp3nS	být
brzy	brzy	k6eAd1	brzy
obsazen	obsadit	k5eAaPmNgInS	obsadit
Rudou	rudý	k2eAgFnSc7d1	rudá
armádou	armáda	k1gFnSc7	armáda
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
spáchali	spáchat	k5eAaPmAgMnP	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rodina	rodina	k1gFnSc1	rodina
==	==	k?	==
</s>
</p>
<p>
<s>
Rodiče	rodič	k1gMnPc1	rodič
Evy	Eva	k1gFnSc2	Eva
Braunové	Braunové	k2eAgFnSc2d1	Braunové
<g/>
,	,	kIx,	,
Frederick	Frederick	k1gMnSc1	Frederick
(	(	kIx(	(
<g/>
řečený	řečený	k2eAgMnSc1d1	řečený
"	"	kIx"	"
<g/>
Fritz	Fritz	k1gMnSc1	Fritz
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
Franziska	Franziska	k1gFnSc1	Franziska
Katharina	Katharina	k1gFnSc1	Katharina
Braunová	Braunová	k1gFnSc1	Braunová
<g/>
,	,	kIx,	,
rozená	rozený	k2eAgFnSc1d1	rozená
Kronbergerová	Kronbergerová	k1gFnSc1	Kronbergerová
<g/>
,	,	kIx,	,
přežili	přežít	k5eAaPmAgMnP	přežít
svou	svůj	k3xOyFgFnSc4	svůj
dceru	dcera	k1gFnSc4	dcera
a	a	k8xC	a
zemřeli	zemřít	k5eAaPmAgMnP	zemřít
22	[number]	k4	22
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
13	[number]	k4	13
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1976	[number]	k4	1976
v	v	k7c6	v
Ruhpoldingu	Ruhpolding	k1gInSc6	Ruhpolding
(	(	kIx(	(
<g/>
Bavorsko	Bavorsko	k1gNnSc1	Bavorsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
pohřbeni	pohřben	k2eAgMnPc1d1	pohřben
na	na	k7c6	na
místním	místní	k2eAgInSc6d1	místní
hřbitově	hřbitov	k1gInSc6	hřbitov
<g/>
.	.	kIx.	.
</s>
<s>
Eva	Eva	k1gFnSc1	Eva
Braunová	Braunová	k1gFnSc1	Braunová
měla	mít	k5eAaImAgFnS	mít
dvě	dva	k4xCgFnPc4	dva
sestry	sestra	k1gFnPc4	sestra
<g/>
,	,	kIx,	,
Ilse	Ils	k1gFnPc4	Ils
(	(	kIx(	(
<g/>
1908	[number]	k4	1908
<g/>
–	–	k?	–
<g/>
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
a	a	k8xC	a
Margaret	Margareta	k1gFnPc2	Margareta
(	(	kIx(	(
<g/>
1915	[number]	k4	1915
<g/>
–	–	k?	–
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přezdívanou	přezdívaný	k2eAgFnSc7d1	přezdívaná
"	"	kIx"	"
<g/>
Gretl	Gretl	k1gFnSc7	Gretl
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ilse	Ilse	k1gFnSc1	Ilse
byla	být	k5eAaImAgFnS	být
k	k	k7c3	k
německému	německý	k2eAgInSc3d1	německý
národnímu	národní	k2eAgInSc3d1	národní
socialismu	socialismus	k1gInSc3	socialismus
(	(	kIx(	(
<g/>
nacismu	nacismus	k1gInSc2	nacismus
<g/>
)	)	kIx)	)
velmi	velmi	k6eAd1	velmi
skeptická	skeptický	k2eAgFnSc1d1	skeptická
<g/>
.	.	kIx.	.
</s>
<s>
Držela	držet	k5eAaImAgFnS	držet
si	se	k3xPyFc3	se
od	od	k7c2	od
Hitlera	Hitler	k1gMnSc2	Hitler
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc2	jeho
doprovodu	doprovod	k1gInSc2	doprovod
značný	značný	k2eAgInSc4d1	značný
odstup	odstup	k1gInSc4	odstup
a	a	k8xC	a
sestru	sestra	k1gFnSc4	sestra
na	na	k7c6	na
Obersalzbergu	Obersalzberg	k1gInSc6	Obersalzberg
navštěvovala	navštěvovat	k5eAaImAgFnS	navštěvovat
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
zřídka	zřídka	k6eAd1	zřídka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
let	léto	k1gNnPc2	léto
sestře	sestra	k1gFnSc6	sestra
často	často	k6eAd1	často
vyčítala	vyčítat	k5eAaImAgFnS	vyčítat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
arogantnější	arogantní	k2eAgFnSc1d2	arogantnější
a	a	k8xC	a
povrchnější	povrchní	k2eAgFnSc1d2	povrchnější
<g/>
.	.	kIx.	.
</s>
<s>
Gretl	Gretnout	k5eAaPmAgMnS	Gretnout
byla	být	k5eAaImAgFnS	být
stálou	stálý	k2eAgFnSc7d1	stálá
součástí	součást	k1gFnSc7	součást
Evina	Evin	k2eAgInSc2d1	Evin
doprovodu	doprovod	k1gInSc2	doprovod
na	na	k7c6	na
Berghofu	Berghof	k1gInSc6	Berghof
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1944	[number]	k4	1944
se	se	k3xPyFc4	se
provdala	provdat	k5eAaPmAgFnS	provdat
za	za	k7c4	za
ctižádostivého	ctižádostivý	k2eAgMnSc4d1	ctižádostivý
důstojníka	důstojník	k1gMnSc4	důstojník
SS	SS	kA	SS
Hermanna	Hermann	k1gMnSc4	Hermann
Fegeleina	Fegelein	k1gMnSc4	Fegelein
<g/>
,	,	kIx,	,
styčného	styčný	k2eAgMnSc4d1	styčný
důstojníka	důstojník	k1gMnSc4	důstojník
Heinricha	Heinrich	k1gMnSc2	Heinrich
Himmlera	Himmler	k1gMnSc2	Himmler
<g/>
.	.	kIx.	.
</s>
<s>
Fegelein	Fegelein	k1gMnSc1	Fegelein
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1945	[number]	k4	1945
uprchnout	uprchnout	k5eAaPmF	uprchnout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byl	být	k5eAaImAgInS	být
zajat	zajmout	k5eAaPmNgInS	zajmout
<g/>
.	.	kIx.	.
</s>
<s>
Hitler	Hitler	k1gMnSc1	Hitler
jej	on	k3xPp3gMnSc4	on
obvinil	obvinit	k5eAaPmAgMnS	obvinit
ze	z	k7c2	z
spojení	spojení	k1gNnSc2	spojení
s	s	k7c7	s
Himmlerovou	Himmlerův	k2eAgFnSc7d1	Himmlerova
předchozí	předchozí	k2eAgFnSc7d1	předchozí
nabídkou	nabídka	k1gFnSc7	nabídka
kapitulace	kapitulace	k1gFnSc2	kapitulace
Spojencům	spojenec	k1gMnPc3	spojenec
a	a	k8xC	a
nechal	nechat	k5eAaPmAgMnS	nechat
jej	on	k3xPp3gNnSc4	on
29	[number]	k4	29
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1945	[number]	k4	1945
na	na	k7c6	na
nádvoří	nádvoří	k1gNnSc6	nádvoří
Nového	Nového	k2eAgNnSc2d1	Nového
říšského	říšský	k2eAgNnSc2d1	říšské
kancléřství	kancléřství	k1gNnSc2	kancléřství
zastřelit	zastřelit	k5eAaPmF	zastřelit
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
Eva	Eva	k1gFnSc1	Eva
jej	on	k3xPp3gMnSc4	on
od	od	k7c2	od
toho	ten	k3xDgNnSc2	ten
nemohla	moct	k5eNaImAgFnS	moct
odradit	odradit	k5eAaPmF	odradit
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
se	se	k3xPyFc4	se
angažovala	angažovat	k5eAaBmAgFnS	angažovat
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
svého	svůj	k3xOyFgMnSc2	svůj
švagra	švagr	k1gMnSc2	švagr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ustala	ustat	k5eAaPmAgFnS	ustat
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
snahách	snaha	k1gFnPc6	snaha
<g/>
,	,	kIx,	,
když	když	k8xS	když
zjistila	zjistit	k5eAaPmAgFnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jí	on	k3xPp3gFnSc3	on
ukradl	ukradnout	k5eAaPmAgInS	ukradnout
šperky	šperk	k1gInPc7	šperk
a	a	k8xC	a
Gretl	Gretl	k1gFnSc7	Gretl
podvedl	podvést	k5eAaPmAgMnS	podvést
<g/>
.	.	kIx.	.
</s>
<s>
Gretl	Gretnout	k5eAaPmAgMnS	Gretnout
byla	být	k5eAaImAgNnP	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
ve	v	k7c6	v
vysokém	vysoký	k2eAgInSc6d1	vysoký
stupni	stupeň	k1gInSc6	stupeň
těhotenství	těhotenství	k1gNnSc2	těhotenství
<g/>
,	,	kIx,	,
dne	den	k1gInSc2	den
5	[number]	k4	5
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
porodila	porodit	k5eAaPmAgFnS	porodit
dceru	dcera	k1gFnSc4	dcera
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
své	svůj	k3xOyFgFnSc2	svůj
sestry	sestra	k1gFnSc2	sestra
pojmenovala	pojmenovat	k5eAaPmAgFnS	pojmenovat
"	"	kIx"	"
<g/>
Eva	Eva	k1gFnSc1	Eva
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Evina	Evin	k2eAgFnSc1d1	Evina
sestřenice	sestřenice	k1gFnSc1	sestřenice
Gertraud	Gertrauda	k1gFnPc2	Gertrauda
Weiskerová	Weiskerová	k1gFnSc1	Weiskerová
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
v	v	k7c6	v
několika	několik	k4yIc2	několik
televizních	televizní	k2eAgFnPc6d1	televizní
dokumentech	dokument	k1gInPc6	dokument
a	a	k8xC	a
rozhovorech	rozhovor	k1gInPc6	rozhovor
a	a	k8xC	a
sepsala	sepsat	k5eAaPmAgFnS	sepsat
autobiografický	autobiografický	k2eAgInSc4d1	autobiografický
román	román	k1gInSc4	román
Evas	Evasa	k1gFnPc2	Evasa
Cousine	Cousine	k?	Cousine
(	(	kIx(	(
<g/>
Evina	Evin	k2eAgFnSc1d1	Evina
sestřenice	sestřenice	k1gFnSc1	sestřenice
<g/>
)	)	kIx)	)
o	o	k7c6	o
společném	společný	k2eAgInSc6d1	společný
čase	čas	k1gInSc6	čas
stráveném	strávený	k2eAgInSc6d1	strávený
s	s	k7c7	s
Evou	Eva	k1gFnSc7	Eva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Eva	Eva	k1gFnSc1	Eva
Braun	Braun	k1gMnSc1	Braun
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Eva	Eva	k1gFnSc1	Eva
Braunová	Braunová	k1gFnSc1	Braunová
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
