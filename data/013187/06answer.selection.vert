<s>
Její	její	k3xOp3gFnSc4	její
potravu	potrava	k1gFnSc4	potrava
tvoří	tvořit	k5eAaImIp3nS	tvořit
ze	z	k7c2	z
70	[number]	k4	70
až	až	k9	až
90	[number]	k4	90
%	%	kIx~	%
malí	malý	k2eAgMnPc1d1	malý
hlodavci	hlodavec	k1gMnPc1	hlodavec
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
hraboš	hraboš	k1gMnSc1	hraboš
<g/>
,	,	kIx,	,
myšice	myšice	k1gFnSc1	myšice
<g/>
,	,	kIx,	,
myš	myš	k1gFnSc1	myš
<g/>
,	,	kIx,	,
norník	norník	k1gMnSc1	norník
a	a	k8xC	a
krysa	krysa	k1gFnSc1	krysa
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
ptáci	pták	k1gMnPc1	pták
až	až	k9	až
do	do	k7c2	do
velikosti	velikost	k1gFnSc2	velikost
slepice	slepice	k1gFnPc1	slepice
a	a	k8xC	a
jiní	jiný	k2eAgMnPc1d1	jiný
drobní	drobný	k2eAgMnPc1d1	drobný
obratlovci	obratlovec	k1gMnPc1	obratlovec
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
veverka	veverka	k1gFnSc1	veverka
<g/>
,	,	kIx,	,
lasice	lasice	k1gFnSc1	lasice
<g/>
,	,	kIx,	,
rejsek	rejsek	k1gInSc1	rejsek
<g/>
,	,	kIx,	,
ještěrka	ještěrka	k1gFnSc1	ještěrka
či	či	k8xC	či
žába	žába	k1gFnSc1	žába
<g/>
.	.	kIx.	.
</s>
