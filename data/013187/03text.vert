<p>
<s>
Kočka	kočka	k1gFnSc1	kočka
divoká	divoký	k2eAgFnSc1d1	divoká
(	(	kIx(	(
<g/>
Felis	Felis	k1gFnSc1	Felis
silvestris	silvestris	k1gFnSc1	silvestris
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
savec	savec	k1gMnSc1	savec
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
kočkovitých	kočkovití	k1gMnPc2	kočkovití
<g/>
.	.	kIx.	.
</s>
<s>
Šelmu	šelma	k1gFnSc4	šelma
vědecky	vědecky	k6eAd1	vědecky
popsal	popsat	k5eAaPmAgMnS	popsat
a	a	k8xC	a
zařadil	zařadit	k5eAaPmAgMnS	zařadit
německý	německý	k2eAgMnSc1d1	německý
přírodovědec	přírodovědec	k1gMnSc1	přírodovědec
Johann	Johann	k1gMnSc1	Johann
Schreber	Schreber	k1gMnSc1	Schreber
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1777	[number]	k4	1777
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
podčeledi	podčeleď	k1gFnSc2	podčeleď
malé	malý	k2eAgFnSc2d1	malá
kočky	kočka	k1gFnSc2	kočka
a	a	k8xC	a
rodu	rod	k1gInSc2	rod
Felis	Felis	k1gFnSc2	Felis
<g/>
.	.	kIx.	.
</s>
<s>
Taxonomie	taxonomie	k1gFnSc1	taxonomie
rodu	rod	k1gInSc2	rod
Felis	Felis	k1gFnSc2	Felis
a	a	k8xC	a
druhu	druh	k1gInSc2	druh
Felis	Felis	k1gFnSc2	Felis
silvestris	silvestris	k1gFnPc2	silvestris
není	být	k5eNaImIp3nS	být
dosud	dosud	k6eAd1	dosud
vyjasněná	vyjasněný	k2eAgFnSc1d1	vyjasněná
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
udává	udávat	k5eAaImIp3nS	udávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kočka	kočka	k1gFnSc1	kočka
divoká	divoký	k2eAgFnSc1d1	divoká
se	se	k3xPyFc4	se
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
na	na	k7c4	na
dalších	další	k2eAgFnPc2d1	další
3	[number]	k4	3
až	až	k9	až
23	[number]	k4	23
poddruhů	poddruh	k1gInPc2	poddruh
(	(	kIx(	(
<g/>
subspecií	subspecie	k1gFnPc2	subspecie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poddruhy	poddruh	k1gInPc1	poddruh
<g/>
,	,	kIx,	,
kočka	kočka	k1gFnSc1	kočka
plavá	plavý	k2eAgFnSc1d1	plavá
a	a	k8xC	a
kočka	kočka	k1gFnSc1	kočka
stepní	stepní	k2eAgFnSc1d1	stepní
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
předkové	předek	k1gMnPc1	předek
dnešní	dnešní	k2eAgFnSc2d1	dnešní
domestikované	domestikovaný	k2eAgFnSc2d1	domestikovaná
kočky	kočka	k1gFnSc2	kočka
domácí	domácí	k2eAgFnSc2d1	domácí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejrozšířenějších	rozšířený	k2eAgFnPc2d3	nejrozšířenější
kočkovitých	kočkovitý	k2eAgFnPc2d1	kočkovitá
šelem	šelma	k1gFnPc2	šelma
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
na	na	k7c6	na
rozsáhlých	rozsáhlý	k2eAgNnPc6d1	rozsáhlé
územích	území	k1gNnPc6	území
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
se	se	k3xPyFc4	se
šedo-hnědo-černou	šedonědo-černý	k2eAgFnSc7d1	šedo-hnědo-černý
barvou	barva	k1gFnSc7	barva
srsti	srst	k1gFnSc2	srst
v	v	k7c6	v
rozličné	rozličný	k2eAgFnSc6d1	rozličná
barevné	barevný	k2eAgFnSc6d1	barevná
kombinaci	kombinace	k1gFnSc6	kombinace
<g/>
.	.	kIx.	.
</s>
<s>
Váží	vážit	k5eAaImIp3nS	vážit
většinou	většina	k1gFnSc7	většina
mezi	mezi	k7c7	mezi
2	[number]	k4	2
a	a	k8xC	a
8	[number]	k4	8
kilogramy	kilogram	k1gInPc7	kilogram
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
3	[number]	k4	3
až	až	k9	až
6	[number]	k4	6
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
těla	tělo	k1gNnSc2	tělo
bez	bez	k7c2	bez
ocasu	ocas	k1gInSc2	ocas
je	být	k5eAaImIp3nS	být
44	[number]	k4	44
až	až	k9	až
75	[number]	k4	75
cm	cm	kA	cm
<g/>
,	,	kIx,	,
ocas	ocas	k1gInSc1	ocas
měří	měřit	k5eAaImIp3nS	měřit
21	[number]	k4	21
až	až	k9	až
37	[number]	k4	37
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Dožívá	dožívat	k5eAaImIp3nS	dožívat
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
12	[number]	k4	12
<g/>
–	–	k?	–
<g/>
14	[number]	k4	14
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
až	až	k9	až
20	[number]	k4	20
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Živí	živit	k5eAaImIp3nS	živit
se	se	k3xPyFc4	se
především	především	k9	především
různými	různý	k2eAgInPc7d1	různý
druhy	druh	k1gInPc7	druh
hlodavců	hlodavec	k1gMnPc2	hlodavec
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
ptáky	pták	k1gMnPc4	pták
<g/>
,	,	kIx,	,
menšími	malý	k2eAgInPc7d2	menší
plazy	plaz	k1gInPc7	plaz
a	a	k8xC	a
obojživelníky	obojživelník	k1gMnPc7	obojživelník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
loví	lovit	k5eAaImIp3nP	lovit
ve	v	k7c6	v
větší	veliký	k2eAgFnSc6d2	veliký
míře	míra	k1gFnSc6	míra
zajícovce	zajícovec	k1gInSc2	zajícovec
<g/>
.	.	kIx.	.
</s>
<s>
Doplňkovou	doplňkový	k2eAgFnSc4d1	doplňková
kořist	kořist	k1gFnSc4	kořist
tvoří	tvořit	k5eAaImIp3nP	tvořit
rozliční	rozličný	k2eAgMnPc1d1	rozličný
bezobratlí	bezobratlí	k1gMnPc1	bezobratlí
a	a	k8xC	a
výjimečně	výjimečně	k6eAd1	výjimečně
i	i	k9	i
mláďata	mládě	k1gNnPc4	mládě
větších	veliký	k2eAgNnPc2d2	veliký
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
srny	srna	k1gFnPc4	srna
a	a	k8xC	a
divoká	divoký	k2eAgNnPc4d1	divoké
prasata	prase	k1gNnPc4	prase
<g/>
.	.	kIx.	.
</s>
<s>
Sama	sám	k3xTgFnSc1	sám
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
ulovena	uloven	k2eAgFnSc1d1	ulovena
jakýmikoliv	jakýkoliv	k3yIgFnPc7	jakýkoliv
většími	veliký	k2eAgFnPc7d2	veliký
šelmami	šelma	k1gFnPc7	šelma
<g/>
,	,	kIx,	,
především	především	k9	především
rysy	rys	k1gInPc7	rys
<g/>
,	,	kIx,	,
vlky	vlk	k1gMnPc7	vlk
a	a	k8xC	a
levharty	levhart	k1gMnPc7	levhart
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
často	často	k6eAd1	často
padnou	padnout	k5eAaImIp3nP	padnout
za	za	k7c4	za
oběť	oběť	k1gFnSc4	oběť
dravým	dravý	k2eAgMnPc3d1	dravý
ptákům	pták	k1gMnPc3	pták
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
širokému	široký	k2eAgInSc3d1	široký
areálu	areál	k1gInSc3	areál
rozšíření	rozšíření	k1gNnSc2	rozšíření
a	a	k8xC	a
poměrně	poměrně	k6eAd1	poměrně
početné	početný	k2eAgFnSc3d1	početná
populaci	populace	k1gFnSc3	populace
jí	jíst	k5eAaImIp3nS	jíst
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
svaz	svaz	k1gInSc1	svaz
ochrany	ochrana	k1gFnSc2	ochrana
přírody	příroda	k1gFnSc2	příroda
vede	vést	k5eAaImIp3nS	vést
jako	jako	k9	jako
druh	druh	k1gInSc1	druh
málo	málo	k6eAd1	málo
dotčený	dotčený	k2eAgInSc1d1	dotčený
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
se	se	k3xPyFc4	se
až	až	k9	až
na	na	k7c4	na
výjimky	výjimka	k1gFnPc4	výjimka
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
Národním	národní	k2eAgInSc6d1	národní
parku	park	k1gInSc6	park
Podyjí	Podyjí	k1gNnSc2	Podyjí
<g/>
,	,	kIx,	,
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
ji	on	k3xPp3gFnSc4	on
také	také	k6eAd1	také
zachytily	zachytit	k5eAaPmAgFnP	zachytit
fotopasti	fotopast	k1gFnPc1	fotopast
v	v	k7c6	v
Bílých	bílý	k2eAgInPc6d1	bílý
Karpatech	Karpaty	k1gInPc6	Karpaty
<g/>
)	)	kIx)	)
přirozeně	přirozeně	k6eAd1	přirozeně
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
k	k	k7c3	k
nám	my	k3xPp1nPc3	my
začíná	začínat	k5eAaImIp3nS	začínat
pronikat	pronikat	k5eAaImF	pronikat
z	z	k7c2	z
prosperujících	prosperující	k2eAgFnPc2d1	prosperující
populací	populace	k1gFnPc2	populace
v	v	k7c6	v
okolních	okolní	k2eAgInPc6d1	okolní
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
z	z	k7c2	z
Bavorska	Bavorsko	k1gNnSc2	Bavorsko
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
2011	[number]	k4	2011
až	až	k8xS	až
2013	[number]	k4	2013
se	se	k3xPyFc4	se
objevilo	objevit	k5eAaPmAgNnS	objevit
několik	několik	k4yIc1	několik
pozorování	pozorování	k1gNnPc2	pozorování
z	z	k7c2	z
fotopastí	fotopast	k1gFnPc2	fotopast
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
Šumavy	Šumava	k1gFnSc2	Šumava
a	a	k8xC	a
Beskyd	Beskyd	k1gInSc1	Beskyd
naznačující	naznačující	k2eAgInSc1d1	naznačující
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
do	do	k7c2	do
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
kočka	kočka	k1gFnSc1	kočka
divoká	divoký	k2eAgFnSc1d1	divoká
postupně	postupně	k6eAd1	postupně
navrací	navracet	k5eAaBmIp3nS	navracet
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
–	–	k?	–
vedle	vedle	k7c2	vedle
rysa	rys	k1gMnSc2	rys
ostrovida	ostrovid	k1gMnSc2	ostrovid
–	–	k?	–
jediným	jediný	k2eAgMnSc7d1	jediný
zdejším	zdejší	k2eAgMnSc7d1	zdejší
zástupcem	zástupce	k1gMnSc7	zástupce
kočkovitých	kočkovitý	k2eAgFnPc2d1	kočkovitá
šelem	šelma	k1gFnPc2	šelma
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žije	žít	k5eAaImIp3nS	žít
několik	několik	k4yIc4	několik
set	sto	k4xCgNnPc2	sto
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Taxonomie	taxonomie	k1gFnSc1	taxonomie
a	a	k8xC	a
nomenklatura	nomenklatura	k1gFnSc1	nomenklatura
==	==	k?	==
</s>
</p>
<p>
<s>
Johann	Johann	k1gMnSc1	Johann
Christian	Christian	k1gMnSc1	Christian
Daniel	Daniela	k1gFnPc2	Daniela
von	von	k1gInSc1	von
Schreber	Schreber	k1gMnSc1	Schreber
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1777	[number]	k4	1777
poprvé	poprvé	k6eAd1	poprvé
vyčlenil	vyčlenit	k5eAaPmAgInS	vyčlenit
kočku	kočka	k1gFnSc4	kočka
divokou	divoký	k2eAgFnSc4d1	divoká
do	do	k7c2	do
samostatného	samostatný	k2eAgInSc2d1	samostatný
druhu	druh	k1gInSc2	druh
Felis	Felis	k1gFnSc2	Felis
(	(	kIx(	(
<g/>
catus	catus	k1gInSc1	catus
<g/>
)	)	kIx)	)
silvestris	silvestris	k1gInSc1	silvestris
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
desetiletích	desetiletí	k1gNnPc6	desetiletí
přírodovědci	přírodovědec	k1gMnPc1	přírodovědec
a	a	k8xC	a
objevitelé	objevitel	k1gMnPc1	objevitel
popisovali	popisovat	k5eAaImAgMnP	popisovat
různé	různý	k2eAgFnPc4d1	různá
divoké	divoký	k2eAgFnPc4d1	divoká
kočky	kočka	k1gFnPc4	kočka
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
přestávalo	přestávat	k5eAaImAgNnS	přestávat
být	být	k5eAaImF	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
samostatné	samostatný	k2eAgInPc4d1	samostatný
druhy	druh	k1gInPc4	druh
a	a	k8xC	a
kdy	kdy	k6eAd1	kdy
o	o	k7c4	o
poddruhy	poddruh	k1gInPc4	poddruh
(	(	kIx(	(
<g/>
subspecie	subspecie	k1gFnPc4	subspecie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
uznávaný	uznávaný	k2eAgMnSc1d1	uznávaný
specialista	specialista	k1gMnSc1	specialista
na	na	k7c6	na
taxonomii	taxonomie	k1gFnSc6	taxonomie
Reginald	Reginald	k1gMnSc1	Reginald
Innes	Innes	k1gMnSc1	Innes
Pocock	Pocock	k1gMnSc1	Pocock
provedl	provést	k5eAaPmAgMnS	provést
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
revizi	revize	k1gFnSc4	revize
dosavadního	dosavadní	k2eAgNnSc2d1	dosavadní
bádání	bádání	k1gNnSc2	bádání
a	a	k8xC	a
rozčlenil	rozčlenit	k5eAaPmAgMnS	rozčlenit
populaci	populace	k1gFnSc4	populace
kočky	kočka	k1gFnSc2	kočka
divoké	divoký	k2eAgFnSc2d1	divoká
na	na	k7c6	na
35	[number]	k4	35
subspecií	subspecie	k1gFnPc2	subspecie
členěných	členěný	k2eAgFnPc2d1	členěná
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
skupin	skupina	k1gFnPc2	skupina
(	(	kIx(	(
<g/>
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
druhů	druh	k1gInPc2	druh
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
skupinu	skupina	k1gFnSc4	skupina
silvestris	silvestris	k1gFnSc4	silvestris
z	z	k7c2	z
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
Malé	Malé	k2eAgFnSc2d1	Malé
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
skupinu	skupina	k1gFnSc4	skupina
bieti	bieť	k1gFnSc2	bieť
z	z	k7c2	z
východní	východní	k2eAgFnSc2d1	východní
Asie	Asie	k1gFnSc2	Asie
a	a	k8xC	a
skupinu	skupina	k1gFnSc4	skupina
lybica	lybic	k1gInSc2	lybic
z	z	k7c2	z
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
západní	západní	k2eAgFnSc2d1	západní
a	a	k8xC	a
střední	střední	k2eAgFnSc2d1	střední
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Skupinu	skupina	k1gFnSc4	skupina
silvestris	silvestris	k1gInSc1	silvestris
rozčlenil	rozčlenit	k5eAaPmAgInS	rozčlenit
na	na	k7c4	na
7	[number]	k4	7
poddruhů	poddruh	k1gInPc2	poddruh
a	a	k8xC	a
skupiny	skupina	k1gFnSc2	skupina
bieti	bieť	k1gFnSc2	bieť
a	a	k8xC	a
lybica	lybica	k6eAd1	lybica
neuváděl	uvádět	k5eNaImAgMnS	uvádět
jako	jako	k8xS	jako
poddruhy	poddruh	k1gInPc4	poddruh
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
jako	jako	k9	jako
velmi	velmi	k6eAd1	velmi
spřízněné	spřízněný	k2eAgInPc1d1	spřízněný
sesterské	sesterský	k2eAgInPc1d1	sesterský
taxony	taxon	k1gInPc1	taxon
specie	specie	k1gFnSc2	specie
Felis	Felis	k1gFnSc2	Felis
silvestris	silvestris	k1gFnSc2	silvestris
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
zařadil	zařadit	k5eAaPmAgMnS	zařadit
do	do	k7c2	do
rodu	rod	k1gInSc2	rod
Felis	Felis	k1gInSc1	Felis
druhy	druh	k1gInPc1	druh
Felis	Felis	k1gFnSc1	Felis
catus	catus	k1gInSc1	catus
<g/>
,	,	kIx,	,
Felis	Felis	k1gFnSc1	Felis
margarita	margarita	k1gFnSc1	margarita
<g/>
,	,	kIx,	,
Felis	Felis	k1gFnSc1	Felis
nigripes	nigripesa	k1gFnPc2	nigripesa
a	a	k8xC	a
Felis	Felis	k1gFnPc2	Felis
chaus	chaus	k1gMnSc1	chaus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Poddruhy	poddruh	k1gInPc4	poddruh
===	===	k?	===
</s>
</p>
<p>
<s>
Taxonomická	taxonomický	k2eAgFnSc1d1	Taxonomická
příručka	příručka	k1gFnSc1	příručka
Mammal	Mammal	k1gInSc1	Mammal
Species	species	k1gFnSc1	species
of	of	k?	of
the	the	k?	the
World	Worlda	k1gFnPc2	Worlda
vydaná	vydaný	k2eAgFnSc1d1	vydaná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
udává	udávat	k5eAaImIp3nS	udávat
celkem	celkem	k6eAd1	celkem
22	[number]	k4	22
poddruhů	poddruh	k1gInPc2	poddruh
Felis	Felis	k1gFnSc1	Felis
silvestris	silvestris	k1gFnSc1	silvestris
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
F.	F.	kA	F.
s.	s.	k?	s.
silvestris	silvestris	k1gInSc1	silvestris
(	(	kIx(	(
<g/>
kočka	kočka	k1gFnSc1	kočka
evropská	evropský	k2eAgFnSc1d1	Evropská
<g/>
)	)	kIx)	)
Schreber	Schreber	k1gInSc1	Schreber
1777	[number]	k4	1777
<g/>
,	,	kIx,	,
nominátní	nominátní	k2eAgInSc1d1	nominátní
poddruh	poddruh	k1gInSc1	poddruh
<g/>
,	,	kIx,	,
žije	žít	k5eAaImIp3nS	žít
na	na	k7c6	na
velké	velký	k2eAgFnSc6d1	velká
části	část	k1gFnSc6	část
území	území	k1gNnSc2	území
Evropy	Evropa	k1gFnSc2	Evropa
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Skandinávie	Skandinávie	k1gFnSc2	Skandinávie
a	a	k8xC	a
ostrovů	ostrov	k1gInPc2	ostrov
Středomoří	středomoří	k1gNnSc2	středomoří
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
velkou	velký	k2eAgFnSc4d1	velká
subspecii	subspecie	k1gFnSc4	subspecie
s	s	k7c7	s
tmavým	tmavý	k2eAgNnSc7d1	tmavé
zbarvením	zbarvení	k1gNnSc7	zbarvení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
F.	F.	kA	F.
s.	s.	k?	s.
cafra	cafra	k1gFnSc1	cafra
(	(	kIx(	(
<g/>
kočka	kočka	k1gFnSc1	kočka
jihoafrická	jihoafrický	k2eAgFnSc1d1	Jihoafrická
<g/>
)	)	kIx)	)
Desmarest	Desmarest	k1gFnSc1	Desmarest
1822	[number]	k4	1822
<g/>
,	,	kIx,	,
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
a	a	k8xC	a
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc4	dva
barevné	barevný	k2eAgFnPc4d1	barevná
fáze	fáze	k1gFnPc4	fáze
-	-	kIx~	-
ocelově	ocelově	k6eAd1	ocelově
šedá	šedý	k2eAgNnPc1d1	šedé
a	a	k8xC	a
tmavě	tmavě	k6eAd1	tmavě
šedá	šedý	k2eAgNnPc1d1	šedé
<g/>
.	.	kIx.	.
</s>
<s>
Lebka	lebka	k1gFnSc1	lebka
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
velká	velký	k2eAgFnSc1d1	velká
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
F.	F.	kA	F.
s.	s.	k?	s.
caucasica	caucasica	k1gFnSc1	caucasica
(	(	kIx(	(
<g/>
kočka	kočka	k1gFnSc1	kočka
kavkazská	kavkazský	k2eAgFnSc1d1	kavkazská
<g/>
)	)	kIx)	)
Satunin	Satunin	k1gInSc1	Satunin
1905	[number]	k4	1905
<g/>
,	,	kIx,	,
žije	žít	k5eAaImIp3nS	žít
na	na	k7c6	na
Kavkaze	Kavkaz	k1gInSc6	Kavkaz
a	a	k8xC	a
v	v	k7c6	v
Malé	Malé	k2eAgFnSc6d1	Malé
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Vzrůstem	vzrůst	k1gInSc7	vzrůst
menší	malý	k2eAgFnSc2d2	menší
a	a	k8xC	a
barvou	barva	k1gFnSc7	barva
světlejší	světlý	k2eAgFnSc1d2	světlejší
než	než	k8xS	než
F.	F.	kA	F.
s.	s.	k?	s.
silvestris	silvestris	k1gInSc1	silvestris
<g/>
.	.	kIx.	.
</s>
<s>
Kresba	kresba	k1gFnSc1	kresba
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
je	být	k5eAaImIp3nS	být
dobře	dobře	k6eAd1	dobře
vyvinutá	vyvinutý	k2eAgFnSc1d1	vyvinutá
<g/>
,	,	kIx,	,
na	na	k7c6	na
ocase	ocas	k1gInSc6	ocas
přítomny	přítomen	k2eAgInPc1d1	přítomen
jen	jen	k9	jen
tři	tři	k4xCgInPc4	tři
proužky	proužek	k1gInPc4	proužek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
F.	F.	kA	F.
s.	s.	k?	s.
caudata	caudat	k1gMnSc2	caudat
Gray	Graa	k1gMnSc2	Graa
<g/>
,	,	kIx,	,
1874	[number]	k4	1874
-	-	kIx~	-
žije	žít	k5eAaImIp3nS	žít
ve	v	k7c6	v
Střední	střední	k2eAgFnSc6d1	střední
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
se	se	k3xPyFc4	se
podobá	podobat	k5eAaImIp3nS	podobat
kočce	kočka	k1gFnSc3	kočka
kavkazské	kavkazský	k2eAgFnSc3d1	kavkazská
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc1	její
ocas	ocas	k1gInSc4	ocas
je	být	k5eAaImIp3nS	být
však	však	k9	však
poněkud	poněkud	k6eAd1	poněkud
delší	dlouhý	k2eAgFnSc1d2	delší
a	a	k8xC	a
hlava	hlava	k1gFnSc1	hlava
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Zbarvení	zbarvení	k1gNnSc1	zbarvení
je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
světlejší	světlý	k2eAgInSc1d2	světlejší
<g/>
,	,	kIx,	,
skvrny	skvrna	k1gFnPc4	skvrna
menší	malý	k2eAgFnPc4d2	menší
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
poměrně	poměrně	k6eAd1	poměrně
výrazné	výrazný	k2eAgNnSc1d1	výrazné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
F.	F.	kA	F.
s.	s.	k?	s.
chutuchta	chutucht	k1gMnSc2	chutucht
Birula	Birul	k1gMnSc2	Birul
<g/>
,	,	kIx,	,
1916	[number]	k4	1916
-	-	kIx~	-
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
jižním	jižní	k2eAgNnSc6d1	jižní
Mongolsku	Mongolsko	k1gNnSc6	Mongolsko
a	a	k8xC	a
severní	severní	k2eAgFnSc6d1	severní
Číně	Čína	k1gFnSc6	Čína
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
F.	F.	kA	F.
s.	s.	k?	s.
cretensis	cretensis	k1gInSc1	cretensis
Haltenorth	Haltenortha	k1gFnPc2	Haltenortha
<g/>
,	,	kIx,	,
1953	[number]	k4	1953
-	-	kIx~	-
žije	žít	k5eAaImIp3nS	žít
na	na	k7c6	na
Krétě	Kréta	k1gFnSc6	Kréta
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
světleji	světle	k6eAd2	světle
zbarvená	zbarvený	k2eAgFnSc1d1	zbarvená
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
život	život	k1gInSc1	život
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
málo	málo	k6eAd1	málo
prozkoumaný	prozkoumaný	k2eAgInSc1d1	prozkoumaný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
F.	F.	kA	F.
s.	s.	k?	s.
foxi	foxi	k?	foxi
Pocock	Pocock	k1gMnSc1	Pocock
<g/>
,	,	kIx,	,
1944	[number]	k4	1944
-	-	kIx~	-
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
jižních	jižní	k2eAgFnPc6d1	jižní
částech	část	k1gFnPc6	část
západní	západní	k2eAgFnSc2d1	západní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
podobná	podobný	k2eAgFnSc1d1	podobná
subspecii	subspecie	k1gFnSc4	subspecie
haussa	haussa	k1gFnSc1	haussa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
má	mít	k5eAaImIp3nS	mít
barvu	barva	k1gFnSc4	barva
více	hodně	k6eAd2	hodně
do	do	k7c2	do
červena	červeno	k1gNnSc2	červeno
a	a	k8xC	a
větší	veliký	k2eAgFnSc4d2	veliký
lebku	lebka	k1gFnSc4	lebka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
F.	F.	kA	F.
s.	s.	k?	s.
gordoni	gordoň	k1gFnSc6	gordoň
(	(	kIx(	(
<g/>
kočka	kočka	k1gFnSc1	kočka
arabská	arabský	k2eAgFnSc1d1	arabská
<g/>
)	)	kIx)	)
Harrison	Harrison	k1gMnSc1	Harrison
<g/>
,	,	kIx,	,
1968	[number]	k4	1968
-	-	kIx~	-
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
na	na	k7c6	na
Arabském	arabský	k2eAgInSc6d1	arabský
poloostrově	poloostrov	k1gInSc6	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Srst	srst	k1gFnSc1	srst
má	mít	k5eAaImIp3nS	mít
krátkou	krátký	k2eAgFnSc4d1	krátká
světle	světle	k6eAd1	světle
šedohnědě	šedohnědě	k6eAd1	šedohnědě
zbarvenou	zbarvený	k2eAgFnSc4d1	zbarvená
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
F.	F.	kA	F.
s.	s.	k?	s.
grampia	grampia	k1gFnSc1	grampia
Miller	Miller	k1gMnSc1	Miller
<g/>
,	,	kIx,	,
1907	[number]	k4	1907
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
ji	on	k3xPp3gFnSc4	on
nalézt	nalézt	k5eAaPmF	nalézt
ve	v	k7c6	v
Skotsku	Skotsko	k1gNnSc6	Skotsko
<g/>
.	.	kIx.	.
</s>
<s>
Těžko	těžko	k6eAd1	těžko
rozlišitelná	rozlišitelný	k2eAgFnSc1d1	rozlišitelná
od	od	k7c2	od
kočky	kočka	k1gFnSc2	kočka
divoké	divoký	k2eAgFnPc4d1	divoká
evropské	evropský	k2eAgFnPc4d1	Evropská
<g/>
,	,	kIx,	,
status	status	k1gInSc1	status
subspecie	subspecie	k1gFnSc2	subspecie
nejistý	jistý	k2eNgInSc1d1	nejistý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
něco	něco	k3yInSc4	něco
větší	veliký	k2eAgFnSc1d2	veliký
a	a	k8xC	a
tmavší	tmavý	k2eAgFnSc1d2	tmavší
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
F.	F.	kA	F.
s.	s.	k?	s.
griselda	griselda	k1gMnSc1	griselda
Thomas	Thomas	k1gMnSc1	Thomas
<g/>
,	,	kIx,	,
1926	[number]	k4	1926
-	-	kIx~	-
obývá	obývat	k5eAaImIp3nS	obývat
Kalahari	Kalahar	k1gFnPc4	Kalahar
a	a	k8xC	a
přilehlá	přilehlý	k2eAgNnPc4d1	přilehlé
území	území	k1gNnPc4	území
<g/>
.	.	kIx.	.
</s>
<s>
Podobná	podobný	k2eAgFnSc1d1	podobná
poddruhu	poddruh	k1gInSc2	poddruh
cafra	cafro	k1gNnSc2	cafro
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
celkově	celkově	k6eAd1	celkově
světlejší	světlý	k2eAgInSc1d2	světlejší
s	s	k7c7	s
méně	málo	k6eAd2	málo
výrazným	výrazný	k2eAgNnSc7d1	výrazné
značením	značení	k1gNnSc7	značení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
F.	F.	kA	F.
s.	s.	k?	s.
haussa	haussa	k1gFnSc1	haussa
Thomas	Thomas	k1gMnSc1	Thomas
a	a	k8xC	a
Hinton	Hinton	k1gInSc1	Hinton
<g/>
,	,	kIx,	,
1921	[number]	k4	1921
-	-	kIx~	-
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Sahelu	Sahel	k1gInSc2	Sahel
<g/>
,	,	kIx,	,
především	především	k9	především
v	v	k7c6	v
lesních	lesní	k2eAgNnPc6d1	lesní
společenstvech	společenstvo	k1gNnPc6	společenstvo
<g/>
.	.	kIx.	.
</s>
<s>
Malý	malý	k2eAgInSc1d1	malý
poddruh	poddruh	k1gInSc1	poddruh
s	s	k7c7	s
šupinatým	šupinatý	k2eAgNnSc7d1	šupinaté
nebo	nebo	k8xC	nebo
světle	světle	k6eAd1	světle
šedivým	šedivý	k2eAgInSc7d1	šedivý
kožichem	kožich	k1gInSc7	kožich
a	a	k8xC	a
červeným	červený	k2eAgInSc7d1	červený
nádechem	nádech	k1gInSc7	nádech
na	na	k7c6	na
hřbetním	hřbetní	k2eAgInSc6d1	hřbetní
pásku	pásek	k1gInSc6	pásek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
F.	F.	kA	F.
s.	s.	k?	s.
iraki	iraki	k1gNnSc1	iraki
Cheesman	Cheesman	k1gMnSc1	Cheesman
<g/>
,	,	kIx,	,
1921	[number]	k4	1921
-	-	kIx~	-
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Kuvajtu	Kuvajt	k1gInSc6	Kuvajt
a	a	k8xC	a
Iráku	Irák	k1gInSc6	Irák
<g/>
.	.	kIx.	.
</s>
<s>
Odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
se	se	k3xPyFc4	se
od	od	k7c2	od
tristrami	tristra	k1gFnPc7	tristra
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
homogennější	homogenní	k2eAgInSc1d2	homogennější
odstín	odstín	k1gInSc1	odstín
na	na	k7c6	na
horních	horní	k2eAgFnPc6d1	horní
částech	část	k1gFnPc6	část
<g/>
,	,	kIx,	,
nediferencovaný	diferencovaný	k2eNgInSc1d1	nediferencovaný
dorzální	dorzální	k2eAgInSc4d1	dorzální
pás	pás	k1gInSc4	pás
a	a	k8xC	a
bělejší	bílý	k2eAgFnSc4d2	bělejší
tvář	tvář	k1gFnSc4	tvář
a	a	k8xC	a
nohy	noha	k1gFnPc4	noha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
F.	F.	kA	F.
s.	s.	k?	s.
jordansi	jordanse	k1gFnSc6	jordanse
(	(	kIx(	(
<g/>
kočka	kočka	k1gFnSc1	kočka
baleárská	baleárský	k2eAgFnSc1d1	baleárská
<g/>
)	)	kIx)	)
Schwarz	Schwarz	k1gMnSc1	Schwarz
<g/>
,	,	kIx,	,
1930	[number]	k4	1930
-	-	kIx~	-
obývá	obývat	k5eAaImIp3nS	obývat
Baleáry	Baleáry	k1gFnPc4	Baleáry
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
F.	F.	kA	F.
s.	s.	k?	s.
lybica	lybica	k1gFnSc1	lybica
(	(	kIx(	(
<g/>
kočka	kočka	k1gFnSc1	kočka
plavá	plavý	k2eAgFnSc1d1	plavá
<g/>
)	)	kIx)	)
Forster	Forster	k1gMnSc1	Forster
<g/>
,	,	kIx,	,
1780	[number]	k4	1780
-	-	kIx~	-
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
na	na	k7c6	na
Sicílii	Sicílie	k1gFnSc6	Sicílie
<g/>
,	,	kIx,	,
Sardinii	Sardinie	k1gFnSc6	Sardinie
<g/>
,	,	kIx,	,
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
a	a	k8xC	a
alžírské	alžírský	k2eAgFnSc3d1	alžírská
části	část	k1gFnSc3	část
Sahary	Sahara	k1gFnSc2	Sahara
<g/>
.	.	kIx.	.
</s>
<s>
Barva	barva	k1gFnSc1	barva
srsti	srst	k1gFnSc2	srst
je	být	k5eAaImIp3nS	být
nažloutlá	nažloutlý	k2eAgFnSc1d1	nažloutlá
<g/>
,	,	kIx,	,
s	s	k7c7	s
nezřetelnými	zřetelný	k2eNgInPc7d1	nezřetelný
pruhy	pruh	k1gInPc7	pruh
a	a	k8xC	a
skvrnami	skvrna	k1gFnPc7	skvrna
a	a	k8xC	a
bledě	bledě	k6eAd1	bledě
hnědým	hnědý	k2eAgInSc7d1	hnědý
slzným	slzný	k2eAgInSc7d1	slzný
pruhem	pruh	k1gInSc7	pruh
<g/>
.	.	kIx.	.
</s>
<s>
Uši	ucho	k1gNnPc1	ucho
jsou	být	k5eAaImIp3nP	být
červenohnědé	červenohnědý	k2eAgInPc1d1	červenohnědý
a	a	k8xC	a
ocas	ocas	k1gInSc1	ocas
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
<g/>
,	,	kIx,	,
s	s	k7c7	s
několika	několik	k4yIc7	několik
kroužky	kroužek	k1gInPc7	kroužek
a	a	k8xC	a
hnědým	hnědý	k2eAgInSc7d1	hnědý
koncem	konec	k1gInSc7	konec
<g/>
.	.	kIx.	.
</s>
<s>
Jedinci	jedinec	k1gMnPc1	jedinec
na	na	k7c6	na
Sardinii	Sardinie	k1gFnSc6	Sardinie
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
severoafrických	severoafrický	k2eAgInPc2d1	severoafrický
protějšků	protějšek	k1gInPc2	protějšek
tmavšíma	tmavý	k2eAgNnPc7d2	tmavší
ušima	ucho	k1gNnPc7	ucho
a	a	k8xC	a
obecně	obecně	k6eAd1	obecně
tmavšími	tmavý	k2eAgFnPc7d2	tmavší
horními	horní	k2eAgFnPc7d1	horní
částmi	část	k1gFnPc7	část
<g/>
,	,	kIx,	,
postrádají	postrádat	k5eAaImIp3nP	postrádat
typický	typický	k2eAgInSc1d1	typický
písečný	písečný	k2eAgInSc1d1	písečný
tón	tón	k1gInSc1	tón
přítomný	přítomný	k2eAgInSc1d1	přítomný
v	v	k7c6	v
severoafrických	severoafrický	k2eAgInPc6d1	severoafrický
exemplářích	exemplář	k1gInPc6	exemplář
<g/>
.	.	kIx.	.
</s>
<s>
Předek	předek	k1gInSc1	předek
kočky	kočka	k1gFnSc2	kočka
domácí	domácí	k2eAgFnSc2d1	domácí
</s>
</p>
<p>
<s>
F.	F.	kA	F.
s.	s.	k?	s.
mellandi	melland	k1gMnPc1	melland
Schwann	Schwann	k1gMnSc1	Schwann
<g/>
,	,	kIx,	,
1904	[number]	k4	1904
-	-	kIx~	-
obývá	obývat	k5eAaImIp3nS	obývat
severní	severní	k2eAgFnSc4d1	severní
Angolu	Angola	k1gFnSc4	Angola
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
Konžské	konžský	k2eAgFnSc2d1	Konžská
pánve	pánev	k1gFnSc2	pánev
a	a	k8xC	a
severní	severní	k2eAgFnSc2d1	severní
Zimbabwe	Zimbabw	k1gFnSc2	Zimbabw
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
F.	F.	kA	F.
s.	s.	k?	s.
nesterovi	nester	k1gMnSc6	nester
Birula	Birula	k1gFnSc1	Birula
<g/>
,	,	kIx,	,
1916	[number]	k4	1916
-	-	kIx~	-
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
<g/>
,	,	kIx,	,
jihozápadním	jihozápadní	k2eAgInSc6d1	jihozápadní
Íránu	Írán	k1gInSc6	Írán
a	a	k8xC	a
severozápadní	severozápadní	k2eAgFnPc4d1	severozápadní
části	část	k1gFnPc4	část
Arabského	arabský	k2eAgInSc2d1	arabský
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
,	,	kIx,	,
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
<g/>
,	,	kIx,	,
Izraeli	Izrael	k1gInSc6	Izrael
a	a	k8xC	a
Palestině	Palestina	k1gFnSc6	Palestina
</s>
</p>
<p>
<s>
F.	F.	kA	F.
s.	s.	k?	s.
ocreata	ocreata	k1gFnSc1	ocreata
Gmelin	Gmelin	k1gInSc1	Gmelin
<g/>
,	,	kIx,	,
1791	[number]	k4	1791
-	-	kIx~	-
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Etiopii	Etiopie	k1gFnSc6	Etiopie
<g/>
.	.	kIx.	.
</s>
<s>
Odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
se	se	k3xPyFc4	se
od	od	k7c2	od
kočky	kočka	k1gFnSc2	kočka
plavé	plavý	k2eAgNnSc1d1	plavé
větší	veliký	k2eAgFnSc7d2	veliký
lebkou	lebka	k1gFnSc7	lebka
a	a	k8xC	a
kožešinou	kožešina	k1gFnSc7	kožešina
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
má	mít	k5eAaImIp3nS	mít
šedivější	šedivý	k2eAgFnSc4d2	šedivější
podkladovou	podkladový	k2eAgFnSc4d1	podkladová
barvu	barva	k1gFnSc4	barva
a	a	k8xC	a
více	hodně	k6eAd2	hodně
tmavé	tmavý	k2eAgFnPc1d1	tmavá
černé	černý	k2eAgFnPc1d1	černá
kresby	kresba	k1gFnPc1	kresba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
F.	F.	kA	F.
s.	s.	k?	s.
ornata	ornata	k1gFnSc1	ornata
(	(	kIx(	(
<g/>
kočka	kočka	k1gFnSc1	kočka
stepní	stepní	k2eAgFnSc1d1	stepní
<g/>
)	)	kIx)	)
Gray	Gray	k1gInPc1	Gray
<g/>
,	,	kIx,	,
1832	[number]	k4	1832
-	-	kIx~	-
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
a	a	k8xC	a
severozápadní	severozápadní	k2eAgFnSc6d1	severozápadní
Indii	Indie	k1gFnSc6	Indie
a	a	k8xC	a
v	v	k7c6	v
Pákistánu	Pákistán	k1gInSc6	Pákistán
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
výrazné	výrazný	k2eAgFnPc4d1	výrazná
tmavé	tmavý	k2eAgFnPc4d1	tmavá
skvrny	skvrna	k1gFnPc4	skvrna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
F.	F.	kA	F.
s.	s.	k?	s.
reyi	reyi	k1gNnSc1	reyi
(	(	kIx(	(
<g/>
kočka	kočka	k1gFnSc1	kočka
korsická	korsický	k2eAgFnSc1d1	Korsická
<g/>
)	)	kIx)	)
Lavauden	Lavaudno	k1gNnPc2	Lavaudno
<g/>
,	,	kIx,	,
1929	[number]	k4	1929
-	-	kIx~	-
žila	žít	k5eAaImAgFnS	žít
na	na	k7c6	na
Korsice	Korsika	k1gFnSc6	Korsika
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
tmavou	tmavý	k2eAgFnSc4d1	tmavá
srst	srst	k1gFnSc4	srst
<g/>
,	,	kIx,	,
krátký	krátký	k2eAgInSc4d1	krátký
ocas	ocas	k1gInSc4	ocas
a	a	k8xC	a
za	za	k7c7	za
ušima	ucho	k1gNnPc7	ucho
jí	on	k3xPp3gFnSc3	on
chybí	chybit	k5eAaPmIp3nS	chybit
červenohnědé	červenohnědý	k2eAgNnSc1d1	červenohnědé
zbarvení	zbarvení	k1gNnSc1	zbarvení
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
poddruh	poddruh	k1gInSc4	poddruh
vyhynulý	vyhynulý	k2eAgInSc4d1	vyhynulý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
F.	F.	kA	F.
s.	s.	k?	s.
rubida	rubida	k1gFnSc1	rubida
Schwann	Schwanno	k1gNnPc2	Schwanno
<g/>
,	,	kIx,	,
1904	[number]	k4	1904
-	-	kIx~	-
žije	žít	k5eAaImIp3nS	žít
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
jižním	jižní	k2eAgInSc6d1	jižní
Súdánu	Súdán	k1gInSc6	Súdán
a	a	k8xC	a
severovýchodní	severovýchodní	k2eAgFnSc3d1	severovýchodní
části	část	k1gFnSc3	část
Konžské	konžský	k2eAgFnSc2d1	Konžská
pánve	pánev	k1gFnSc2	pánev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
F.	F.	kA	F.
s.	s.	k?	s.
tristrami	tristra	k1gFnPc7	tristra
Pocock	Pocock	k1gMnSc1	Pocock
<g/>
,	,	kIx,	,
1944	[number]	k4	1944
-	-	kIx~	-
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
Palestině	Palestina	k1gFnSc6	Palestina
a	a	k8xC	a
Izraeli	Izrael	k1gInSc6	Izrael
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tmavší	tmavý	k2eAgMnSc1d2	tmavší
a	a	k8xC	a
více	hodně	k6eAd2	hodně
do	do	k7c2	do
šeda	šedo	k1gNnSc2	šedo
zbarvená	zbarvený	k2eAgFnSc1d1	zbarvená
než	než	k8xS	než
lybica	lybic	k2eAgFnSc1d1	lybic
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
F.	F.	kA	F.
s.	s.	k?	s.
ugandae	ugandae	k1gInSc1	ugandae
Schwann	Schwanna	k1gFnPc2	Schwanna
<g/>
,	,	kIx,	,
1904	[number]	k4	1904
-	-	kIx~	-
obývá	obývat	k5eAaImIp3nS	obývat
Ugandu	Uganda	k1gFnSc4	Uganda
<g/>
.	.	kIx.	.
<g/>
tyto	tento	k3xDgInPc1	tento
poddruhy	poddruh	k1gInPc1	poddruh
jsou	být	k5eAaImIp3nP	být
děleny	dělit	k5eAaImNgInP	dělit
do	do	k7c2	do
třech	tři	k4xCgFnPc2	tři
skupin	skupina	k1gFnPc2	skupina
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
lesní	lesní	k2eAgFnSc2d1	lesní
<g/>
"	"	kIx"	"
kočky	kočka	k1gFnSc2	kočka
(	(	kIx(	(
<g/>
skupina	skupina	k1gFnSc1	skupina
silvestris	silvestris	k1gFnSc1	silvestris
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
stepní	stepní	k2eAgFnSc2d1	stepní
<g/>
"	"	kIx"	"
kočky	kočka	k1gFnSc2	kočka
(	(	kIx(	(
<g/>
skupina	skupina	k1gFnSc1	skupina
ornata	ornata	k1gFnSc1	ornata
-	-	kIx~	-
caudata	caudata	k1gFnSc1	caudata
<g/>
)	)	kIx)	)
–	–	k?	–
liší	lišit	k5eAaImIp3nS	lišit
se	se	k3xPyFc4	se
od	od	k7c2	od
lesních	lesní	k2eAgFnPc2d1	lesní
koček	kočka	k1gFnPc2	kočka
menším	menšit	k5eAaImIp1nS	menšit
vzrůstem	vzrůst	k1gInSc7	vzrůst
<g/>
,	,	kIx,	,
delšíma	dlouhý	k2eAgFnPc7d2	delší
a	a	k8xC	a
špičatějšíma	špičatý	k2eAgNnPc7d2	špičatější
ušima	ucho	k1gNnPc7	ucho
a	a	k8xC	a
světlejší	světlý	k2eAgFnSc7d2	světlejší
barvou	barva	k1gFnSc7	barva
srsti	srst	k1gFnSc2	srst
<g/>
;	;	kIx,	;
patří	patřit	k5eAaImIp3nP	patřit
sem	sem	k6eAd1	sem
poddruhy	poddruh	k1gInPc4	poddruh
ornata	ornat	k1gMnSc2	ornat
<g/>
,	,	kIx,	,
nesterovi	sterův	k2eNgMnPc1d1	sterův
a	a	k8xC	a
iraki	irak	k1gMnPc1	irak
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
křovinné	křovinný	k2eAgFnSc2d1	křovinná
<g/>
"	"	kIx"	"
kočky	kočka	k1gFnSc2	kočka
(	(	kIx(	(
<g/>
skupina	skupina	k1gFnSc1	skupina
ornata	ornata	k1gFnSc1	ornata
-	-	kIx~	-
lybica	lybica	k1gFnSc1	lybica
<g/>
)	)	kIx)	)
–	–	k?	–
liší	lišit	k5eAaImIp3nS	lišit
se	se	k3xPyFc4	se
od	od	k7c2	od
stepních	stepní	k2eAgFnPc2d1	stepní
koček	kočka	k1gFnPc2	kočka
světlou	světlý	k2eAgFnSc7d1	světlá
barvou	barva	k1gFnSc7	barva
srsti	srst	k1gFnSc2	srst
s	s	k7c7	s
jasně	jasně	k6eAd1	jasně
vyznačenými	vyznačený	k2eAgFnPc7d1	vyznačená
skvrnami	skvrna	k1gFnPc7	skvrna
a	a	k8xC	a
pruhy	pruh	k1gInPc7	pruh
<g/>
;	;	kIx,	;
patří	patřit	k5eAaImIp3nP	patřit
sem	sem	k6eAd1	sem
poddruhy	poddruh	k1gInPc4	poddruh
chutuchta	chutucht	k1gMnSc4	chutucht
<g/>
,	,	kIx,	,
lybica	lybicus	k1gMnSc4	lybicus
<g/>
,	,	kIx,	,
ocreata	ocreat	k1gMnSc4	ocreat
<g/>
,	,	kIx,	,
rubida	rubid	k1gMnSc4	rubid
<g/>
,	,	kIx,	,
cafra	cafr	k1gMnSc4	cafr
<g/>
,	,	kIx,	,
griselda	griseld	k1gMnSc4	griseld
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
mellandi	melland	k1gMnPc1	melland
<g/>
;	;	kIx,	;
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
pochází	pocházet	k5eAaImIp3nS	pocházet
dnešní	dnešní	k2eAgFnSc1d1	dnešní
kočka	kočka	k1gFnSc1	kočka
domácíITIS	domácíITIS	k?	domácíITIS
(	(	kIx(	(
<g/>
Integrated	Integrated	k1gMnSc1	Integrated
Taxonomic	Taxonomic	k1gMnSc1	Taxonomic
Information	Information	k1gInSc4	Information
System	Syst	k1gInSc7	Syst
<g/>
)	)	kIx)	)
k	k	k7c3	k
těmto	tento	k3xDgInPc3	tento
poddruhům	poddruh	k1gInPc3	poddruh
přidává	přidávat	k5eAaImIp3nS	přidávat
ještě	ještě	k6eAd1	ještě
23	[number]	k4	23
<g/>
.	.	kIx.	.
subspecii	subspecie	k1gFnSc6	subspecie
jménem	jméno	k1gNnSc7	jméno
Felis	Felis	k1gFnSc2	Felis
silvestris	silvestris	k1gFnSc1	silvestris
vellerosa	vellerosa	k1gFnSc1	vellerosa
<g/>
.	.	kIx.	.
<g/>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
svaz	svaz	k1gInSc1	svaz
ochrany	ochrana	k1gFnSc2	ochrana
přírody	příroda	k1gFnSc2	příroda
(	(	kIx(	(
<g/>
IUCN	IUCN	kA	IUCN
<g/>
)	)	kIx)	)
však	však	k9	však
na	na	k7c6	na
základě	základ	k1gInSc6	základ
posledních	poslední	k2eAgFnPc2d1	poslední
fylogeografických	fylogeografický	k2eAgFnPc2d1	fylogeografická
analýz	analýza	k1gFnPc2	analýza
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
pouze	pouze	k6eAd1	pouze
čtyři	čtyři	k4xCgInPc4	čtyři
poddruhy	poddruh	k1gInPc4	poddruh
-	-	kIx~	-
silvestris	silvestris	k1gFnSc1	silvestris
<g/>
,	,	kIx,	,
ornata	ornata	k1gFnSc1	ornata
<g/>
,	,	kIx,	,
lybica	lybica	k1gFnSc1	lybica
a	a	k8xC	a
cafra	cafra	k1gFnSc1	cafra
<g/>
,	,	kIx,	,
k	k	k7c3	k
nimž	jenž	k3xRgFnPc3	jenž
přidává	přidávat	k5eAaImIp3nS	přidávat
pátý	pátý	k4xOgInSc1	pátý
poddruh	poddruh	k1gInSc1	poddruh
bieti	bieť	k1gFnSc2	bieť
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
označovaný	označovaný	k2eAgInSc1d1	označovaný
za	za	k7c4	za
samostatný	samostatný	k2eAgInSc4d1	samostatný
druh	druh	k1gInSc4	druh
(	(	kIx(	(
<g/>
kočka	kočka	k1gFnSc1	kočka
šedá	šedý	k2eAgFnSc1d1	šedá
–	–	k?	–
Felis	Felis	k1gFnSc1	Felis
bieti	bieť	k1gFnSc2	bieť
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1	rozšíření
těchto	tento	k3xDgFnPc2	tento
pěti	pět	k4xCc2	pět
poddruhů	poddruh	k1gInPc2	poddruh
je	být	k5eAaImIp3nS	být
vyobrazeno	vyobrazit	k5eAaPmNgNnS	vyobrazit
na	na	k7c6	na
mapce	mapka	k1gFnSc6	mapka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Taxonomická	taxonomický	k2eAgFnSc1d1	Taxonomická
revize	revize	k1gFnSc1	revize
kočkovitých	kočkovitý	k2eAgFnPc2d1	kočkovitá
šelem	šelma	k1gFnPc2	šelma
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
odborná	odborný	k2eAgFnSc1d1	odborná
skupina	skupina	k1gFnSc1	skupina
IUCN	IUCN	kA	IUCN
SSC	SSC	kA	SSC
Cat	Cat	k1gMnSc1	Cat
Specialist	Specialist	k1gMnSc1	Specialist
Group	Group	k1gMnSc1	Group
<g/>
,	,	kIx,	,
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
kočku	kočka	k1gFnSc4	kočka
divokou	divoký	k2eAgFnSc4d1	divoká
(	(	kIx(	(
<g/>
Felis	Felis	k1gFnSc4	Felis
silvestris	silvestris	k1gFnSc2	silvestris
<g/>
)	)	kIx)	)
jen	jen	k9	jen
populace	populace	k1gFnSc2	populace
žijící	žijící	k2eAgFnSc2d1	žijící
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
Malé	Malé	k2eAgFnSc6d1	Malé
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgFnSc2d1	ostatní
kočky	kočka	k1gFnSc2	kočka
zahrnula	zahrnout	k5eAaPmAgFnS	zahrnout
do	do	k7c2	do
jiných	jiný	k2eAgInPc2d1	jiný
druhů	druh	k1gInPc2	druh
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
rodu	rod	k1gInSc2	rod
Felis	Felis	k1gFnSc2	Felis
<g/>
.	.	kIx.	.
</s>
<s>
Samotnou	samotný	k2eAgFnSc4d1	samotná
Felis	Felis	k1gFnSc4	Felis
silvestris	silvestris	k1gFnSc2	silvestris
pak	pak	k6eAd1	pak
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
pravděpodobné	pravděpodobný	k2eAgInPc4d1	pravděpodobný
(	(	kIx(	(
<g/>
Felis	Felis	k1gFnSc1	Felis
silvestris	silvestris	k1gFnSc1	silvestris
silvestris	silvestris	k1gFnSc1	silvestris
<g/>
,	,	kIx,	,
Felis	Felis	k1gFnSc1	Felis
silvestris	silvestris	k1gFnSc1	silvestris
caucasica	caucasica	k1gFnSc1	caucasica
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeden	jeden	k4xCgInSc4	jeden
pochybný	pochybný	k2eAgInSc4d1	pochybný
poddruh	poddruh	k1gInSc4	poddruh
(	(	kIx(	(
<g/>
Felis	Felis	k1gFnSc1	Felis
silvrestris	silvrestris	k1gFnSc1	silvrestris
grampia	grampia	k1gFnSc1	grampia
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Kočka	kočka	k1gFnSc1	kočka
divoká	divoký	k2eAgFnSc1d1	divoká
může	moct	k5eAaImIp3nS	moct
vážit	vážit	k5eAaImF	vážit
1,2	[number]	k4	1,2
<g/>
–	–	k?	–
<g/>
11	[number]	k4	11
kg	kg	kA	kg
a	a	k8xC	a
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
délky	délka	k1gFnSc2	délka
47	[number]	k4	47
<g/>
–	–	k?	–
<g/>
80	[number]	k4	80
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
hmotnost	hmotnost	k1gFnSc1	hmotnost
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c7	mezi
2	[number]	k4	2
a	a	k8xC	a
8	[number]	k4	8
kg	kg	kA	kg
<g/>
,	,	kIx,	,
údaje	údaj	k1gInPc1	údaj
pod	pod	k7c7	pod
a	a	k8xC	a
nad	nad	k7c7	nad
touto	tento	k3xDgFnSc7	tento
hranicí	hranice	k1gFnSc7	hranice
jsou	být	k5eAaImIp3nP	být
výjimečné	výjimečný	k2eAgFnPc1d1	výjimečná
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
poddruhu	poddruh	k1gInSc6	poddruh
<g/>
,	,	kIx,	,
dostupnosti	dostupnost	k1gFnSc6	dostupnost
potravy	potrava	k1gFnSc2	potrava
a	a	k8xC	a
ročním	roční	k2eAgNnSc6d1	roční
období	období	k1gNnSc6	období
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
roku	rok	k1gInSc2	rok
může	moct	k5eAaImIp3nS	moct
u	u	k7c2	u
stejného	stejné	k1gNnSc2	stejné
jedince	jedinec	k1gMnSc4	jedinec
kolísat	kolísat	k5eAaImF	kolísat
až	až	k9	až
o	o	k7c4	o
25	[number]	k4	25
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
první	první	k4xOgInSc4	první
pohled	pohled	k1gInSc4	pohled
zavalitější	zavalitý	k2eAgFnSc4d2	zavalitější
postavu	postava	k1gFnSc4	postava
než	než	k8xS	než
kočka	kočka	k1gFnSc1	kočka
domácí	domácí	k2eAgFnSc1d1	domácí
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
díky	díky	k7c3	díky
delší	dlouhý	k2eAgFnSc3d2	delší
a	a	k8xC	a
hustější	hustý	k2eAgFnSc3d2	hustší
srsti	srst	k1gFnSc3	srst
<g/>
;	;	kIx,	;
tento	tento	k3xDgInSc1	tento
rozdíl	rozdíl	k1gInSc1	rozdíl
je	být	k5eAaImIp3nS	být
nejvíce	nejvíce	k6eAd1	nejvíce
patrný	patrný	k2eAgInSc1d1	patrný
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
<g/>
.	.	kIx.	.
</s>
<s>
Kočka	kočka	k1gFnSc1	kočka
divoká	divoký	k2eAgFnSc1d1	divoká
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
větší	veliký	k2eAgFnSc4d2	veliký
hlavu	hlava	k1gFnSc4	hlava
s	s	k7c7	s
dlouhými	dlouhý	k2eAgInPc7d1	dlouhý
vousky	vousek	k1gInPc7	vousek
a	a	k8xC	a
menšíma	malý	k2eAgNnPc7d2	menší
ušima	ucho	k1gNnPc7	ucho
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zbarvena	zbarven	k2eAgFnSc1d1	zbarvena
šedohnědě	šedohnědě	k6eAd1	šedohnědě
až	až	k6eAd1	až
šedožlutě	šedožlutě	k6eAd1	šedožlutě
<g/>
,	,	kIx,	,
s	s	k7c7	s
výrazným	výrazný	k2eAgNnSc7d1	výrazné
pruhováním	pruhování	k1gNnSc7	pruhování
na	na	k7c6	na
hřbetu	hřbet	k1gInSc6	hřbet
<g/>
,	,	kIx,	,
ocase	ocas	k1gInSc6	ocas
a	a	k8xC	a
nohách	noha	k1gFnPc6	noha
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hřbetu	hřbet	k1gInSc6	hřbet
je	být	k5eAaImIp3nS	být
výrazný	výrazný	k2eAgInSc1d1	výrazný
tmavý	tmavý	k2eAgInSc1d1	tmavý
pás	pás	k1gInSc1	pás
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
břicho	břicho	k1gNnSc1	břicho
bývá	bývat	k5eAaImIp3nS	bývat
krémově	krémově	k6eAd1	krémově
žluté	žlutý	k2eAgFnPc4d1	žlutá
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
zdivočelé	zdivočelý	k2eAgFnSc2d1	Zdivočelá
kočky	kočka	k1gFnSc2	kočka
domácí	domácí	k2eAgNnSc1d1	domácí
ji	on	k3xPp3gFnSc4	on
lze	lze	k6eAd1	lze
rozlišit	rozlišit	k5eAaPmF	rozlišit
zejména	zejména	k9	zejména
dle	dle	k7c2	dle
těchto	tento	k3xDgInPc2	tento
znaků	znak	k1gInPc2	znak
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
ocas	ocas	k1gInSc1	ocas
kočky	kočka	k1gFnSc2	kočka
divoké	divoký	k2eAgFnSc2d1	divoká
je	být	k5eAaImIp3nS	být
kratší	krátký	k2eAgFnSc1d2	kratší
než	než	k8xS	než
polovina	polovina	k1gFnSc1	polovina
délky	délka	k1gFnSc2	délka
jejího	její	k3xOp3gNnSc2	její
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
délce	délka	k1gFnSc6	délka
huňatý	huňatý	k2eAgMnSc1d1	huňatý
a	a	k8xC	a
před	před	k7c7	před
špičkou	špička	k1gFnSc7	špička
má	mít	k5eAaImIp3nS	mít
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
pruhy	pruh	k1gInPc7	pruh
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
zdivočelá	zdivočelý	k2eAgFnSc1d1	Zdivočelá
kočka	kočka	k1gFnSc1	kočka
domácí	domácí	k1gMnPc4	domácí
má	mít	k5eAaImIp3nS	mít
ocas	ocas	k1gInSc1	ocas
delší	dlouhý	k2eAgFnSc4d2	delší
než	než	k8xS	než
polovinu	polovina	k1gFnSc4	polovina
délky	délka	k1gFnSc2	délka
svého	svůj	k3xOyFgNnSc2	svůj
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
zužuje	zužovat	k5eAaImIp3nS	zužovat
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
větší	veliký	k2eAgInSc1d2	veliký
počet	počet	k1gInSc1	počet
pruhů	pruh	k1gInPc2	pruh
u	u	k7c2	u
špičky	špička	k1gFnSc2	špička
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
tělo	tělo	k1gNnSc1	tělo
je	být	k5eAaImIp3nS	být
robustnější	robustní	k2eAgNnSc1d2	robustnější
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
hlava	hlava	k1gFnSc1	hlava
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
slechy	slech	k1gInPc1	slech
menší	malý	k2eAgInPc1d2	menší
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
srst	srst	k1gFnSc1	srst
a	a	k8xC	a
hmatové	hmatový	k2eAgInPc1d1	hmatový
vousy	vous	k1gInPc1	vous
jsou	být	k5eAaImIp3nP	být
delší	dlouhý	k2eAgInPc1d2	delší
<g/>
.	.	kIx.	.
<g/>
Spolehlivost	spolehlivost	k1gFnSc1	spolehlivost
však	však	k9	však
není	být	k5eNaImIp3nS	být
stoprocentní	stoprocentní	k2eAgFnSc1d1	stoprocentní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Smysly	smysl	k1gInPc4	smysl
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Zrak	zrak	k1gInSc4	zrak
====	====	k?	====
</s>
</p>
<p>
<s>
Kočka	kočka	k1gFnSc1	kočka
divoká	divoký	k2eAgFnSc1d1	divoká
má	mít	k5eAaImIp3nS	mít
zrak	zrak	k1gInSc4	zrak
přizpůsobený	přizpůsobený	k2eAgInSc4d1	přizpůsobený
k	k	k7c3	k
vidění	vidění	k1gNnSc3	vidění
za	za	k7c2	za
šera	šero	k1gNnSc2	šero
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
ani	ani	k8xC	ani
jasné	jasný	k2eAgNnSc1d1	jasné
světlo	světlo	k1gNnSc1	světlo
jí	on	k3xPp3gFnSc2	on
nijak	nijak	k6eAd1	nijak
neomezuje	omezovat	k5eNaImIp3nS	omezovat
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
6	[number]	k4	6
základních	základní	k2eAgFnPc2d1	základní
barev	barva	k1gFnPc2	barva
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
na	na	k7c4	na
krátkou	krátký	k2eAgFnSc4d1	krátká
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Dokáže	dokázat	k5eAaPmIp3nS	dokázat
vnímat	vnímat	k5eAaImF	vnímat
i	i	k9	i
ultrafialovou	ultrafialový	k2eAgFnSc4d1	ultrafialová
část	část	k1gFnSc4	část
spektra	spektrum	k1gNnSc2	spektrum
<g/>
,	,	kIx,	,
infračervenou	infračervený	k2eAgFnSc7d1	infračervená
nikoliv	nikoliv	k9	nikoliv
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
zorné	zorný	k2eAgNnSc1d1	zorné
pole	pole	k1gNnSc1	pole
270	[number]	k4	270
<g/>
-	-	kIx~	-
<g/>
290	[number]	k4	290
<g/>
°	°	k?	°
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
umožněno	umožnit	k5eAaPmNgNnS	umožnit
silně	silně	k6eAd1	silně
vyklenutou	vyklenutý	k2eAgFnSc7d1	vyklenutá
rohovkou	rohovka	k1gFnSc7	rohovka
<g/>
.	.	kIx.	.
</s>
<s>
Nejostřeji	ostro	k6eAd3	ostro
vidí	vidět	k5eAaImIp3nS	vidět
na	na	k7c4	na
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
2	[number]	k4	2
až	až	k9	až
6	[number]	k4	6
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
objekty	objekt	k1gInPc1	objekt
nacházející	nacházející	k2eAgInPc1d1	nacházející
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
blízko	blízko	k6eAd1	blízko
zrakem	zrak	k1gInSc7	zrak
téměř	téměř	k6eAd1	téměř
nerozliší	rozlišit	k5eNaPmIp3nS	rozlišit
<g/>
.	.	kIx.	.
</s>
<s>
Schopnost	schopnost	k1gFnSc1	schopnost
zachytit	zachytit	k5eAaPmF	zachytit
světlo	světlo	k1gNnSc4	světlo
má	mít	k5eAaImIp3nS	mít
asi	asi	k9	asi
6	[number]	k4	6
<g/>
x	x	k?	x
lepší	dobrý	k2eAgMnSc1d2	lepší
než	než	k8xS	než
člověk	člověk	k1gMnSc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Přísun	přísun	k1gInSc1	přísun
světla	světlo	k1gNnSc2	světlo
do	do	k7c2	do
oka	oko	k1gNnSc2	oko
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
smršťováním	smršťování	k1gNnSc7	smršťování
a	a	k8xC	a
roztahováním	roztahování	k1gNnSc7	roztahování
zornic	zornice	k1gFnPc2	zornice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Čich	čich	k1gInSc4	čich
====	====	k?	====
</s>
</p>
<p>
<s>
Čich	čich	k1gInSc1	čich
kočky	kočka	k1gFnSc2	kočka
je	být	k5eAaImIp3nS	být
podstatně	podstatně	k6eAd1	podstatně
horší	zlý	k2eAgMnSc1d2	horší
než	než	k8xS	než
u	u	k7c2	u
psovitých	psovitý	k2eAgFnPc2d1	psovitá
šelem	šelma	k1gFnPc2	šelma
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
lepší	dobrý	k2eAgMnSc1d2	lepší
než	než	k8xS	než
lidský	lidský	k2eAgMnSc1d1	lidský
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
účinný	účinný	k2eAgInSc1d1	účinný
především	především	k9	především
na	na	k7c4	na
krátkou	krátký	k2eAgFnSc4d1	krátká
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
čichání	čichání	k1gNnSc3	čichání
využívá	využívat	k5eAaPmIp3nS	využívat
čichové	čichový	k2eAgFnSc2d1	čichová
sliznice	sliznice	k1gFnSc2	sliznice
v	v	k7c6	v
nose	nos	k1gInSc6	nos
a	a	k8xC	a
Jacobsonův	Jacobsonův	k2eAgInSc1d1	Jacobsonův
orgán	orgán	k1gInSc1	orgán
v	v	k7c6	v
patře	patro	k1gNnSc6	patro
tlamy	tlama	k1gFnSc2	tlama
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
slouží	sloužit	k5eAaImIp3nS	sloužit
především	především	k9	především
k	k	k7c3	k
zachycování	zachycování	k1gNnSc3	zachycování
říjných	říjný	k2eAgInPc2d1	říjný
pachů	pach	k1gInPc2	pach
(	(	kIx(	(
<g/>
flémování	flémování	k1gNnSc2	flémování
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Potravu	potrava	k1gFnSc4	potrava
před	před	k7c7	před
prvním	první	k4xOgNnSc7	první
ochutnáním	ochutnání	k1gNnSc7	ochutnání
obvykle	obvykle	k6eAd1	obvykle
očichá	očichat	k5eAaPmIp3nS	očichat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Hmat	hmat	k1gInSc4	hmat
====	====	k?	====
</s>
</p>
<p>
<s>
K	k	k7c3	k
ohmatávání	ohmatávání	k1gNnSc3	ohmatávání
objektů	objekt	k1gInPc2	objekt
využívá	využívat	k5eAaPmIp3nS	využívat
hmatové	hmatový	k2eAgFnPc4d1	hmatová
vousky	vouska	k1gFnPc4	vouska
na	na	k7c6	na
obličeji	obličej	k1gInSc6	obličej
zvané	zvaný	k2eAgInPc4d1	zvaný
sinusové	sinusový	k2eAgInPc4d1	sinusový
chlupy	chlup	k1gInPc4	chlup
či	či	k8xC	či
vibrisy	vibris	k1gInPc4	vibris
<g/>
.	.	kIx.	.
</s>
<s>
Pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
jí	on	k3xPp3gFnSc3	on
v	v	k7c6	v
orientaci	orientace	k1gFnSc6	orientace
ve	v	k7c6	v
tmě	tma	k1gFnSc6	tma
<g/>
,	,	kIx,	,
ve	v	k7c6	v
stísněných	stísněný	k2eAgFnPc6d1	stísněná
prostorách	prostora	k1gFnPc6	prostora
a	a	k8xC	a
při	při	k7c6	při
identifikaci	identifikace	k1gFnSc6	identifikace
kořisti	kořist	k1gFnSc2	kořist
<g/>
.	.	kIx.	.
</s>
<s>
Kočka	kočka	k1gFnSc1	kočka
je	on	k3xPp3gInPc4	on
může	moct	k5eAaImIp3nS	moct
natáčet	natáčet	k5eAaImF	natáčet
různými	různý	k2eAgInPc7d1	různý
směry	směr	k1gInPc7	směr
<g/>
.	.	kIx.	.
</s>
<s>
Hmat	hmat	k1gInSc1	hmat
kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
i	i	k9	i
polštářky	polštářek	k1gInPc1	polštářek
na	na	k7c6	na
tlapkách	tlapka	k1gFnPc6	tlapka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Sluch	sluch	k1gInSc4	sluch
====	====	k?	====
</s>
</p>
<p>
<s>
Sluch	sluch	k1gInSc1	sluch
má	mít	k5eAaImIp3nS	mít
kočka	kočka	k1gFnSc1	kočka
divoká	divoký	k2eAgFnSc1d1	divoká
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
vyvinutý	vyvinutý	k2eAgInSc1d1	vyvinutý
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
značně	značně	k6eAd1	značně
pohyblivý	pohyblivý	k2eAgInSc1d1	pohyblivý
boltec	boltec	k1gInSc1	boltec
<g/>
,	,	kIx,	,
o	o	k7c4	o
což	což	k3yQnSc4	což
se	se	k3xPyFc4	se
stará	starat	k5eAaImIp3nS	starat
27	[number]	k4	27
svalů	sval	k1gInPc2	sval
<g/>
.	.	kIx.	.
</s>
<s>
Vnímá	vnímat	k5eAaImIp3nS	vnímat
frekvence	frekvence	k1gFnSc1	frekvence
od	od	k7c2	od
0,2	[number]	k4	0,2
kHz	khz	kA	khz
až	až	k6eAd1	až
do	do	k7c2	do
55	[number]	k4	55
kHz	khz	kA	khz
(	(	kIx(	(
<g/>
pes	pes	k1gMnSc1	pes
do	do	k7c2	do
40	[number]	k4	40
kHz	khz	kA	khz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
reaguje	reagovat	k5eAaBmIp3nS	reagovat
na	na	k7c4	na
vysoké	vysoký	k2eAgFnPc4d1	vysoká
frekvence	frekvence	k1gFnPc4	frekvence
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
kHz	khz	kA	khz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
vydávají	vydávat	k5eAaImIp3nP	vydávat
drobní	drobný	k2eAgMnPc1d1	drobný
hlodavci	hlodavec	k1gMnPc1	hlodavec
(	(	kIx(	(
<g/>
větší	veliký	k2eAgFnSc2d2	veliký
kočkovité	kočkovitý	k2eAgFnSc2d1	kočkovitá
šelmy	šelma	k1gFnSc2	šelma
lépe	dobře	k6eAd2	dobře
reagují	reagovat	k5eAaBmIp3nP	reagovat
na	na	k7c4	na
zvuky	zvuk	k1gInPc4	zvuk
o	o	k7c6	o
nižší	nízký	k2eAgFnSc6d2	nižší
frekvenci	frekvence	k1gFnSc6	frekvence
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zvuk	zvuk	k1gInSc1	zvuk
pohybu	pohyb	k1gInSc2	pohyb
myši	myš	k1gFnSc2	myš
zachytí	zachytit	k5eAaPmIp3nS	zachytit
nejméně	málo	k6eAd3	málo
na	na	k7c4	na
15	[number]	k4	15
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgNnSc1d1	vnitřní
ucho	ucho	k1gNnSc1	ucho
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
kočce	kočka	k1gFnSc3	kočka
dosahovat	dosahovat	k5eAaImF	dosahovat
značné	značný	k2eAgFnPc4d1	značná
schopnosti	schopnost	k1gFnPc4	schopnost
rovnováhy	rovnováha	k1gFnSc2	rovnováha
při	při	k7c6	při
pohybu	pohyb	k1gInSc6	pohyb
ve	v	k7c6	v
větvích	větev	k1gFnPc6	větev
či	či	k8xC	či
při	při	k7c6	při
kontrole	kontrola	k1gFnSc6	kontrola
pádu	pád	k1gInSc2	pád
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Životní	životní	k2eAgInSc1d1	životní
areál	areál	k1gInSc1	areál
==	==	k?	==
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
kočku	kočka	k1gFnSc4	kočka
divokou	divoký	k2eAgFnSc4d1	divoká
jsou	být	k5eAaImIp3nP	být
typické	typický	k2eAgInPc1d1	typický
smíšené	smíšený	k2eAgInPc1d1	smíšený
a	a	k8xC	a
listnaté	listnatý	k2eAgInPc1d1	listnatý
lesy	les	k1gInPc1	les
v	v	k7c6	v
podhůří	podhůří	k1gNnSc6	podhůří
nebo	nebo	k8xC	nebo
na	na	k7c6	na
vrchovinách	vrchovina	k1gFnPc6	vrchovina
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ve	v	k7c6	v
vysokých	vysoký	k2eAgFnPc6d1	vysoká
horách	hora	k1gFnPc6	hora
nebo	nebo	k8xC	nebo
v	v	k7c6	v
nížinách	nížina	k1gFnPc6	nížina
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
teritoria	teritorium	k1gNnSc2	teritorium
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
50	[number]	k4	50
ha	ha	kA	ha
až	až	k6eAd1	až
po	po	k7c4	po
několik	několik	k4yIc4	několik
set	sto	k4xCgNnPc2	sto
hektarů	hektar	k1gInPc2	hektar
<g/>
.	.	kIx.	.
</s>
<s>
Kocouři	kocour	k1gMnPc1	kocour
mívají	mívat	k5eAaImIp3nP	mívat
větší	veliký	k2eAgNnSc4d2	veliký
teritorium	teritorium	k1gNnSc4	teritorium
než	než	k8xS	než
samice	samice	k1gFnPc1	samice
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
francouzských	francouzský	k2eAgFnPc2d1	francouzská
koček	kočka	k1gFnPc2	kočka
opatřené	opatřený	k2eAgFnSc2d1	opatřená
radiolokačním	radiolokační	k2eAgNnSc7d1	radiolokační
zařízením	zařízení	k1gNnSc7	zařízení
se	se	k3xPyFc4	se
pohybovaly	pohybovat	k5eAaImAgInP	pohybovat
v	v	k7c6	v
teritoriu	teritorium	k1gNnSc6	teritorium
o	o	k7c6	o
průměrné	průměrný	k2eAgFnSc6d1	průměrná
výměře	výměra	k1gFnSc6	výměra
184	[number]	k4	184
ha	ha	kA	ha
<g/>
,	,	kIx,	,
samci	samec	k1gMnPc1	samec
měli	mít	k5eAaImAgMnP	mít
území	území	k1gNnSc4	území
od	od	k7c2	od
220	[number]	k4	220
do	do	k7c2	do
1270	[number]	k4	1270
ha	ha	kA	ha
(	(	kIx(	(
<g/>
průměr	průměr	k1gInSc1	průměr
573	[number]	k4	573
ha	ha	kA	ha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kočky	kočka	k1gFnPc1	kočka
divoké	divoký	k2eAgFnSc2d1	divoká
si	se	k3xPyFc3	se
své	svůj	k3xOyFgNnSc4	svůj
území	území	k1gNnSc4	území
značkují	značkovat	k5eAaImIp3nP	značkovat
drápáním	drápání	k1gNnSc7	drápání
kůry	kůra	k1gFnSc2	kůra
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
močením	močení	k1gNnSc7	močení
<g/>
,	,	kIx,	,
trusem	trus	k1gInSc7	trus
a	a	k8xC	a
pachovými	pachový	k2eAgInPc7d1	pachový
výměšky	výměšek	k1gInPc7	výměšek
z	z	k7c2	z
meziprstových	meziprstův	k2eAgFnPc2d1	meziprstův
žláz	žláza	k1gFnPc2	žláza
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
den	den	k1gInSc4	den
odpočívají	odpočívat	k5eAaImIp3nP	odpočívat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
skrýši	skrýš	k1gFnSc6	skrýš
(	(	kIx(	(
<g/>
např.	např.	kA	např.
dutiny	dutina	k1gFnSc2	dutina
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
vývraty	vývrat	k1gInPc1	vývrat
<g/>
,	,	kIx,	,
skalní	skalní	k2eAgFnPc1d1	skalní
dutiny	dutina	k1gFnPc1	dutina
<g/>
,	,	kIx,	,
nory	nora	k1gFnPc1	nora
jezevců	jezevec	k1gMnPc2	jezevec
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
za	za	k7c2	za
teplých	teplý	k2eAgInPc2d1	teplý
dnů	den	k1gInPc2	den
se	se	k3xPyFc4	se
však	však	k9	však
rády	rád	k2eAgFnPc1d1	ráda
vyhřívají	vyhřívat	k5eAaImIp3nP	vyhřívat
na	na	k7c6	na
slunci	slunce	k1gNnSc6	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
lov	lov	k1gInSc4	lov
vyráží	vyrážet	k5eAaImIp3nP	vyrážet
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
procestují	procestovat	k5eAaPmIp3nP	procestovat
5	[number]	k4	5
až	až	k9	až
8	[number]	k4	8
km	km	kA	km
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
==	==	k?	==
</s>
</p>
<p>
<s>
Žije	žít	k5eAaImIp3nS	žít
spíše	spíše	k9	spíše
samotářsky	samotářsky	k6eAd1	samotářsky
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
páření	páření	k1gNnSc2	páření
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
spadá	spadat	k5eAaPmIp3nS	spadat
do	do	k7c2	do
jarních	jarní	k2eAgInPc2d1	jarní
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
na	na	k7c4	na
čas	čas	k1gInSc4	čas
stává	stávat	k5eAaImIp3nS	stávat
společenskou	společenský	k2eAgFnSc4d1	společenská
a	a	k8xC	a
svého	svůj	k3xOyFgMnSc2	svůj
partnera	partner	k1gMnSc2	partner
láká	lákat	k5eAaImIp3nS	lákat
hlasitým	hlasitý	k2eAgNnSc7d1	hlasité
mňoukáním	mňoukání	k1gNnSc7	mňoukání
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
se	se	k3xPyFc4	se
říje	říje	k1gFnSc1	říje
podobá	podobat	k5eAaImIp3nS	podobat
říji	říje	k1gFnSc4	říje
koček	kočka	k1gFnPc2	kočka
domácích	domácí	k2eAgMnPc2d1	domácí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
skrytá	skrytý	k2eAgFnSc1d1	skrytá
a	a	k8xC	a
probíhá	probíhat	k5eAaImIp3nS	probíhat
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
.	.	kIx.	.
</s>
<s>
Březost	březost	k1gFnSc1	březost
trvá	trvat	k5eAaImIp3nS	trvat
cca	cca	kA	cca
65	[number]	k4	65
dnů	den	k1gInPc2	den
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
porodem	porod	k1gInSc7	porod
tří	tři	k4xCgNnPc2	tři
až	až	k9	až
pěti	pět	k4xCc2	pět
koťat	kotě	k1gNnPc2	kotě
<g/>
.	.	kIx.	.
</s>
<s>
Koťata	kotě	k1gNnPc4	kotě
obyčejně	obyčejně	k6eAd1	obyčejně
vrhá	vrhat	k5eAaImIp3nS	vrhat
od	od	k7c2	od
dubna	duben	k1gInSc2	duben
do	do	k7c2	do
června	červen	k1gInSc2	červen
a	a	k8xC	a
stará	starat	k5eAaImIp3nS	starat
se	se	k3xPyFc4	se
o	o	k7c4	o
ně	on	k3xPp3gMnPc4	on
až	až	k9	až
do	do	k7c2	do
podzimu	podzim	k1gInSc2	podzim
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
osamostatňují	osamostatňovat	k5eAaImIp3nP	osamostatňovat
<g/>
.	.	kIx.	.
</s>
<s>
Dospělosti	dospělost	k1gFnPc1	dospělost
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
zhruba	zhruba	k6eAd1	zhruba
ve	v	k7c6	v
stáří	stáří	k1gNnSc6	stáří
dvanácti	dvanáct	k4xCc2	dvanáct
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
mláďata	mládě	k1gNnPc4	mládě
brání	bránit	k5eAaImIp3nS	bránit
(	(	kIx(	(
<g/>
postaví	postavit	k5eAaPmIp3nS	postavit
se	se	k3xPyFc4	se
i	i	k9	i
člověku	člověk	k1gMnSc3	člověk
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
případě	případ	k1gInSc6	případ
ohrožení	ohrožení	k1gNnSc2	ohrožení
je	být	k5eAaImIp3nS	být
přenáší	přenášet	k5eAaImIp3nS	přenášet
do	do	k7c2	do
jiného	jiný	k2eAgInSc2d1	jiný
úkrytu	úkryt	k1gInSc2	úkryt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potrava	potrava	k1gFnSc1	potrava
==	==	k?	==
</s>
</p>
<p>
<s>
Její	její	k3xOp3gFnSc4	její
potravu	potrava	k1gFnSc4	potrava
tvoří	tvořit	k5eAaImIp3nS	tvořit
ze	z	k7c2	z
70	[number]	k4	70
až	až	k9	až
90	[number]	k4	90
%	%	kIx~	%
malí	malý	k2eAgMnPc1d1	malý
hlodavci	hlodavec	k1gMnPc1	hlodavec
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
hraboš	hraboš	k1gMnSc1	hraboš
<g/>
,	,	kIx,	,
myšice	myšice	k1gFnSc1	myšice
<g/>
,	,	kIx,	,
myš	myš	k1gFnSc1	myš
<g/>
,	,	kIx,	,
norník	norník	k1gMnSc1	norník
a	a	k8xC	a
krysa	krysa	k1gFnSc1	krysa
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
ptáci	pták	k1gMnPc1	pták
až	až	k9	až
do	do	k7c2	do
velikosti	velikost	k1gFnSc2	velikost
slepice	slepice	k1gFnPc1	slepice
a	a	k8xC	a
jiní	jiný	k2eAgMnPc1d1	jiný
drobní	drobný	k2eAgMnPc1d1	drobný
obratlovci	obratlovec	k1gMnPc1	obratlovec
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
veverka	veverka	k1gFnSc1	veverka
<g/>
,	,	kIx,	,
lasice	lasice	k1gFnSc1	lasice
<g/>
,	,	kIx,	,
rejsek	rejsek	k1gInSc1	rejsek
<g/>
,	,	kIx,	,
ještěrka	ještěrka	k1gFnSc1	ještěrka
či	či	k8xC	či
žába	žába	k1gFnSc1	žába
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
habitatech	habitat	k1gInPc6	habitat
tvoří	tvořit	k5eAaImIp3nS	tvořit
významnou	významný	k2eAgFnSc4d1	významná
(	(	kIx(	(
<g/>
místy	místo	k1gNnPc7	místo
dokonce	dokonce	k9	dokonce
dominantní	dominantní	k2eAgMnSc1d1	dominantní
<g/>
)	)	kIx)	)
součást	součást	k1gFnSc4	součást
jídelníčku	jídelníček	k1gInSc2	jídelníček
zajícovci	zajícovec	k1gMnSc3	zajícovec
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
divocí	divoký	k2eAgMnPc1d1	divoký
králíci	králík	k1gMnPc1	králík
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sušších	suchý	k2eAgFnPc6d2	sušší
oblastech	oblast	k1gFnPc6	oblast
požírají	požírat	k5eAaImIp3nP	požírat
i	i	k9	i
různé	různý	k2eAgMnPc4d1	různý
členovce	členovec	k1gMnPc4	členovec
<g/>
,	,	kIx,	,
především	především	k9	především
hmyz	hmyz	k1gInSc1	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
Výjimečně	výjimečně	k6eAd1	výjimečně
uloví	ulovit	k5eAaPmIp3nP	ulovit
i	i	k9	i
větší	veliký	k2eAgNnPc1d2	veliký
zvířata	zvíře	k1gNnPc1	zvíře
(	(	kIx(	(
<g/>
srnče	srnče	k1gNnSc1	srnče
<g/>
,	,	kIx,	,
sele	sele	k1gNnSc1	sele
divokého	divoký	k2eAgNnSc2d1	divoké
prasete	prase	k1gNnSc2	prase
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rostlinný	rostlinný	k2eAgInSc1d1	rostlinný
materiál	materiál	k1gInSc1	materiál
v	v	k7c6	v
potravě	potrava	k1gFnSc6	potrava
obaluje	obalovat	k5eAaImIp3nS	obalovat
ostré	ostrý	k2eAgInPc4d1	ostrý
úlomky	úlomek	k1gInPc4	úlomek
kostí	kost	k1gFnPc2	kost
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
dráždí	dráždit	k5eAaImIp3nP	dráždit
žaludeční	žaludeční	k2eAgFnSc4d1	žaludeční
stěnu	stěna	k1gFnSc4	stěna
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vyvrhovat	vyvrhovat	k5eAaImF	vyvrhovat
kůstky	kůstka	k1gFnPc4	kůstka
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nP	by
poranily	poranit	k5eAaPmAgFnP	poranit
jícen	jícen	k1gInSc4	jícen
<g/>
.	.	kIx.	.
</s>
<s>
Objem	objem	k1gInSc1	objem
žaludku	žaludek	k1gInSc2	žaludek
kočky	kočka	k1gFnSc2	kočka
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
450	[number]	k4	450
cm3	cm3	k4	cm3
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
sníst	sníst	k5eAaPmF	sníst
najednou	najednou	k6eAd1	najednou
až	až	k9	až
téměř	téměř	k6eAd1	téměř
0,5	[number]	k4	0,5
kg	kg	kA	kg
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Obvyklá	obvyklý	k2eAgFnSc1d1	obvyklá
denní	denní	k2eAgFnSc1d1	denní
spotřeba	spotřeba	k1gFnSc1	spotřeba
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
je	být	k5eAaImIp3nS	být
dostatek	dostatek	k1gInSc4	dostatek
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
0,4	[number]	k4	0,4
až	až	k9	až
0,7	[number]	k4	0,7
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
rok	rok	k1gInSc4	rok
zabije	zabít	k5eAaPmIp3nS	zabít
kočka	kočka	k1gFnSc1	kočka
divoká	divoký	k2eAgFnSc1d1	divoká
až	až	k9	až
2000	[number]	k4	2000
drobných	drobný	k2eAgMnPc2d1	drobný
hlodavců	hlodavec	k1gMnPc2	hlodavec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
dokáže	dokázat	k5eAaPmIp3nS	dokázat
hladovět	hladovět	k5eAaImF	hladovět
až	až	k9	až
10	[number]	k4	10
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Způsob	způsob	k1gInSc1	způsob
lovu	lov	k1gInSc2	lov
je	být	k5eAaImIp3nS	být
podobný	podobný	k2eAgInSc1d1	podobný
kočce	kočka	k1gFnSc3	kočka
domácí	domácí	k1gFnSc2	domácí
-	-	kIx~	-
kočka	kočka	k1gFnSc1	kočka
divoká	divoký	k2eAgFnSc1d1	divoká
přepadá	přepadat	k5eAaImIp3nS	přepadat
svou	svůj	k3xOyFgFnSc4	svůj
kořist	kořist	k1gFnSc4	kořist
ze	z	k7c2	z
zálohy	záloha	k1gFnSc2	záloha
jedním	jeden	k4xCgInSc7	jeden
rychlým	rychlý	k2eAgInSc7d1	rychlý
výpadem	výpad	k1gInSc7	výpad
či	či	k8xC	či
skokem	skok	k1gInSc7	skok
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
kořist	kořist	k1gFnSc1	kořist
pronásleduje	pronásledovat	k5eAaImIp3nS	pronásledovat
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
prakticky	prakticky	k6eAd1	prakticky
všechny	všechen	k3xTgFnPc4	všechen
kočky	kočka	k1gFnPc4	kočka
není	být	k5eNaImIp3nS	být
vytrvalá	vytrvalý	k2eAgFnSc1d1	vytrvalá
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
unaví	unavit	k5eAaPmIp3nP	unavit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Predace	Predace	k1gFnSc2	Predace
==	==	k?	==
</s>
</p>
<p>
<s>
Kočka	kočka	k1gFnSc1	kočka
divoká	divoký	k2eAgFnSc1d1	divoká
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
stát	stát	k5eAaImF	stát
kořistí	kořist	k1gFnSc7	kořist
mnoha	mnoho	k4c2	mnoho
druhů	druh	k1gInPc2	druh
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
napadena	napaden	k2eAgFnSc1d1	napadena
a	a	k8xC	a
sežrána	sežrán	k2eAgFnSc1d1	sežrána
rysem	rys	k1gInSc7	rys
<g/>
,	,	kIx,	,
vlkem	vlk	k1gMnSc7	vlk
<g/>
,	,	kIx,	,
kunou	kuna	k1gFnSc7	kuna
(	(	kIx(	(
<g/>
koťata	kotě	k1gNnPc1	kotě
a	a	k8xC	a
mladí	mladý	k2eAgMnPc1d1	mladý
jedinci	jedinec	k1gMnPc1	jedinec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výrem	výr	k1gMnSc7	výr
<g/>
,	,	kIx,	,
orlem	orel	k1gMnSc7	orel
<g/>
,	,	kIx,	,
liškou	liška	k1gFnSc7	liška
(	(	kIx(	(
<g/>
koťata	kotě	k1gNnPc1	kotě
a	a	k8xC	a
mladí	mladý	k2eAgMnPc1d1	mladý
jedinci	jedinec	k1gMnPc1	jedinec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jezevcem	jezevec	k1gMnSc7	jezevec
(	(	kIx(	(
<g/>
ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
obvykle	obvykle	k6eAd1	obvykle
dobře	dobře	k6eAd1	dobře
snáší	snášet	k5eAaImIp3nS	snášet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
divokými	divoký	k2eAgInPc7d1	divoký
psy	pes	k1gMnPc4	pes
a	a	k8xC	a
možná	možná	k9	možná
dokonce	dokonce	k9	dokonce
i	i	k9	i
divočákem	divočák	k1gMnSc7	divočák
<g/>
.	.	kIx.	.
</s>
<s>
Menším	malý	k2eAgMnPc3d2	menší
dravcům	dravec	k1gMnPc3	dravec
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
kuna	kuna	k1gFnSc1	kuna
a	a	k8xC	a
liška	liška	k1gFnSc1	liška
se	se	k3xPyFc4	se
však	však	k9	však
obvykle	obvykle	k6eAd1	obvykle
dokáže	dokázat	k5eAaPmIp3nS	dokázat
ubránit	ubránit	k5eAaPmF	ubránit
a	a	k8xC	a
ve	v	k7c6	v
zcela	zcela	k6eAd1	zcela
výjimečných	výjimečný	k2eAgInPc6d1	výjimečný
případech	případ	k1gInPc6	případ
je	být	k5eAaImIp3nS	být
naopak	naopak	k6eAd1	naopak
ona	onen	k3xDgFnSc1	onen
jejich	jejich	k3xOp3gMnPc7	jejich
predátorem	predátor	k1gMnSc7	predátor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
==	==	k?	==
</s>
</p>
<p>
<s>
Hlavním	hlavní	k2eAgMnSc7d1	hlavní
nepřítelem	nepřítel	k1gMnSc7	nepřítel
kočky	kočka	k1gFnSc2	kočka
divoké	divoký	k2eAgNnSc4d1	divoké
byl	být	k5eAaImAgMnS	být
a	a	k8xC	a
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
místech	místo	k1gNnPc6	místo
stále	stále	k6eAd1	stále
je	být	k5eAaImIp3nS	být
člověk	člověk	k1gMnSc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
byla	být	k5eAaImAgFnS	být
kočka	kočka	k1gFnSc1	kočka
po	po	k7c6	po
staletí	staletí	k1gNnSc6	staletí
pronásledována	pronásledovat	k5eAaImNgFnS	pronásledovat
a	a	k8xC	a
lovena	lovit	k5eAaImNgFnS	lovit
jako	jako	k9	jako
"	"	kIx"	"
<g/>
škůdce	škůdce	k1gMnSc1	škůdce
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
začala	začít	k5eAaPmAgFnS	začít
populace	populace	k1gFnSc1	populace
rychle	rychle	k6eAd1	rychle
ubývat	ubývat	k5eAaImF	ubývat
a	a	k8xC	a
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
z	z	k7c2	z
naší	náš	k3xOp1gFnSc2	náš
přírody	příroda	k1gFnSc2	příroda
prakticky	prakticky	k6eAd1	prakticky
vymizela	vymizet	k5eAaPmAgFnS	vymizet
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
extrémně	extrémně	k6eAd1	extrémně
vzácně	vzácně	k6eAd1	vzácně
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
prakticky	prakticky	k6eAd1	prakticky
jen	jen	k9	jen
o	o	k7c4	o
jedince	jedinec	k1gMnPc4	jedinec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
k	k	k7c3	k
nám	my	k3xPp1nPc3	my
přišli	přijít	k5eAaPmAgMnP	přijít
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
(	(	kIx(	(
<g/>
Šumava	Šumava	k1gFnSc1	Šumava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rakouska	Rakousko	k1gNnSc2	Rakousko
(	(	kIx(	(
<g/>
Podyjí	Podyjí	k1gNnSc2	Podyjí
<g/>
)	)	kIx)	)
či	či	k8xC	či
Slovenska	Slovensko	k1gNnSc2	Slovensko
(	(	kIx(	(
<g/>
Beskydy	Beskydy	k1gFnPc1	Beskydy
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
kusů	kus	k1gInPc2	kus
je	být	k5eAaImIp3nS	být
obtížné	obtížný	k2eAgNnSc1d1	obtížné
odhadnout	odhadnout	k5eAaPmF	odhadnout
<g/>
,	,	kIx,	,
zřejmě	zřejmě	k6eAd1	zřejmě
půjde	jít	k5eAaImIp3nS	jít
maximálně	maximálně	k6eAd1	maximálně
o	o	k7c4	o
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
možných	možný	k2eAgFnPc2d1	možná
metod	metoda	k1gFnPc2	metoda
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
zjistit	zjistit	k5eAaPmF	zjistit
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
ve	v	k7c4	v
sledované	sledovaný	k2eAgFnPc4d1	sledovaná
oblasti	oblast	k1gFnPc4	oblast
kočka	kočka	k1gFnSc1	kočka
divoká	divoký	k2eAgFnSc1d1	divoká
nachází	nacházet	k5eAaImIp3nS	nacházet
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
instalovat	instalovat	k5eAaBmF	instalovat
zde	zde	k6eAd1	zde
kolíky	kolík	k1gInPc4	kolík
napuštěné	napuštěný	k2eAgInPc4d1	napuštěný
tinkturou	tinktura	k1gFnSc7	tinktura
z	z	k7c2	z
kozlíku	kozlík	k1gInSc2	kozlík
lékařského	lékařský	k2eAgInSc2d1	lékařský
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
pach	pach	k1gInSc1	pach
kočky	kočka	k1gFnSc2	kočka
silně	silně	k6eAd1	silně
vábí	vábit	k5eAaImIp3nS	vábit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
místě	místo	k1gNnSc6	místo
je	být	k5eAaImIp3nS	být
poté	poté	k6eAd1	poté
možno	možno	k6eAd1	možno
instalovat	instalovat	k5eAaBmF	instalovat
fotopasti	fotopast	k1gFnPc4	fotopast
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
z	z	k7c2	z
kolíku	kolík	k1gInSc2	kolík
odebírat	odebírat	k5eAaImF	odebírat
kočičí	kočičí	k2eAgFnSc4d1	kočičí
srst	srst	k1gFnSc4	srst
k	k	k7c3	k
analýze	analýza	k1gFnSc3	analýza
DNA	dno	k1gNnSc2	dno
<g/>
.	.	kIx.	.
</s>
<s>
Legislativa	legislativa	k1gFnSc1	legislativa
ochrany	ochrana	k1gFnSc2	ochrana
přírody	příroda	k1gFnSc2	příroda
řadí	řadit	k5eAaImIp3nS	řadit
kočku	kočka	k1gFnSc4	kočka
divokou	divoký	k2eAgFnSc4d1	divoká
mezi	mezi	k7c4	mezi
zvláště	zvláště	k6eAd1	zvláště
chráněné	chráněný	k2eAgFnPc4d1	chráněná
<g/>
,	,	kIx,	,
kriticky	kriticky	k6eAd1	kriticky
ohrožené	ohrožený	k2eAgInPc4d1	ohrožený
druhy	druh	k1gInPc4	druh
<g/>
.	.	kIx.	.
<g/>
Mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
2011	[number]	k4	2011
až	až	k9	až
2013	[number]	k4	2013
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
několika	několik	k4yIc3	několik
pozorováním	pozorování	k1gNnSc7	pozorování
výskytu	výskyt	k1gInSc2	výskyt
kočky	kočka	k1gFnSc2	kočka
divoké	divoký	k2eAgFnSc2d1	divoká
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Šumavy	Šumava	k1gFnSc2	Šumava
a	a	k8xC	a
Beskyd	Beskydy	k1gFnPc2	Beskydy
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
pozorování	pozorování	k1gNnSc3	pozorování
výskytu	výskyt	k1gInSc2	výskyt
kočky	kočka	k1gFnSc2	kočka
divoké	divoký	k2eAgFnSc2d1	divoká
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
Bílých	bílý	k2eAgInPc2d1	bílý
Karpat	Karpaty	k1gInPc2	Karpaty
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hybridizace	hybridizace	k1gFnSc2	hybridizace
==	==	k?	==
</s>
</p>
<p>
<s>
Kočka	kočka	k1gFnSc1	kočka
divoká	divoký	k2eAgFnSc1d1	divoká
evropská	evropský	k2eAgFnSc1d1	Evropská
(	(	kIx(	(
<g/>
Felis	Felis	k1gFnSc1	Felis
silvestris	silvestris	k1gFnSc1	silvestris
silvestris	silvestris	k1gFnSc1	silvestris
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
křížit	křížit	k5eAaImF	křížit
se	s	k7c7	s
zdivočelou	zdivočelý	k2eAgFnSc7d1	Zdivočelá
kočkou	kočka	k1gFnSc7	kočka
domácí	domácí	k2eAgFnSc2d1	domácí
(	(	kIx(	(
<g/>
Felis	Felis	k1gFnSc2	Felis
s.	s.	k?	s.
catus	catus	k1gInSc1	catus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hybridizace	hybridizace	k1gFnSc1	hybridizace
byla	být	k5eAaImAgFnS	být
popsána	popsat	k5eAaPmNgFnS	popsat
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
areálu	areál	k1gInSc6	areál
výskytu	výskyt	k1gInSc2	výskyt
kočky	kočka	k1gFnSc2	kočka
divoké	divoký	k2eAgFnPc4d1	divoká
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
podílem	podíl	k1gInSc7	podíl
kříženců	kříženec	k1gMnPc2	kříženec
v	v	k7c6	v
populacích	populace	k1gFnPc6	populace
v	v	k7c6	v
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
a	a	k8xC	a
Skotsku	Skotsko	k1gNnSc6	Skotsko
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišit	rozlišit	k5eAaPmF	rozlišit
kočku	kočka	k1gFnSc4	kočka
divokou	divoký	k2eAgFnSc4d1	divoká
od	od	k7c2	od
zdivočelé	zdivočelý	k2eAgFnSc2d1	Zdivočelá
kočky	kočka	k1gFnSc2	kočka
domácí	domácí	k2eAgFnSc2d1	domácí
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc2	jejich
kříženců	kříženec	k1gMnPc2	kříženec
na	na	k7c6	na
základě	základ	k1gInSc6	základ
zbavení	zbavení	k1gNnSc2	zbavení
srsti	srst	k1gFnSc2	srst
a	a	k8xC	a
morfologických	morfologický	k2eAgInPc2d1	morfologický
znaků	znak	k1gInPc2	znak
je	být	k5eAaImIp3nS	být
problematické	problematický	k2eAgNnSc1d1	problematické
a	a	k8xC	a
pro	pro	k7c4	pro
spolehlivé	spolehlivý	k2eAgNnSc4d1	spolehlivé
rozlišení	rozlišení	k1gNnSc4	rozlišení
druhů	druh	k1gInPc2	druh
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
použít	použít	k5eAaPmF	použít
genetických	genetický	k2eAgFnPc2d1	genetická
metod	metoda	k1gFnPc2	metoda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
ANDĚRA	ANDĚRA	kA	ANDĚRA
<g/>
,	,	kIx,	,
Miloš	Miloš	k1gMnSc1	Miloš
<g/>
;	;	kIx,	;
GAISLER	GAISLER	kA	GAISLER
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Savci	savec	k1gMnPc1	savec
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
:	:	kIx,	:
popis	popis	k1gInSc1	popis
<g/>
,	,	kIx,	,
rozšíření	rozšíření	k1gNnSc1	rozšíření
<g/>
,	,	kIx,	,
ekologie	ekologie	k1gFnSc1	ekologie
<g/>
,	,	kIx,	,
ochrana	ochrana	k1gFnSc1	ochrana
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
2185	[number]	k4	2185
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ČERVENÝ	Červený	k1gMnSc1	Červený
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
,	,	kIx,	,
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
</s>
<s>
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
myslivosti	myslivost	k1gFnSc2	myslivost
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ottovo	Ottův	k2eAgNnSc1d1	Ottovo
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
v	v	k7c6	v
divizi	divize	k1gFnSc6	divize
Cesty	cesta	k1gFnSc2	cesta
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7181	[number]	k4	7181
<g/>
-	-	kIx~	-
<g/>
901	[number]	k4	901
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Kočka	kočka	k1gFnSc1	kočka
divoká	divoký	k2eAgFnSc1d1	divoká
<g/>
,	,	kIx,	,
s.	s.	k?	s.
316	[number]	k4	316
<g/>
-	-	kIx~	-
<g/>
317	[number]	k4	317
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŠKALOUD	Škaloud	k1gMnSc1	Škaloud
<g/>
,	,	kIx,	,
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
<g/>
.	.	kIx.	.
</s>
<s>
Liška	Liška	k1gMnSc1	Liška
a	a	k8xC	a
větší	veliký	k2eAgFnPc4d2	veliký
šelmy	šelma	k1gFnPc4	šelma
<g/>
:	:	kIx,	:
psík	psík	k1gMnSc1	psík
mývalovitý	mývalovitý	k2eAgMnSc1d1	mývalovitý
<g/>
,	,	kIx,	,
mýval	mýval	k1gMnSc1	mýval
<g/>
,	,	kIx,	,
liška	liška	k1gFnSc1	liška
<g/>
,	,	kIx,	,
šakal	šakal	k1gMnSc1	šakal
<g/>
,	,	kIx,	,
medvěd	medvěd	k1gMnSc1	medvěd
<g/>
,	,	kIx,	,
rys	rys	k1gMnSc1	rys
<g/>
,	,	kIx,	,
kočka	kočka	k1gFnSc1	kočka
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Brázda	Brázda	k1gMnSc1	Brázda
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
209	[number]	k4	209
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
372	[number]	k4	372
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Kočka	kočka	k1gFnSc1	kočka
<g/>
,	,	kIx,	,
s.	s.	k?	s.
193	[number]	k4	193
<g/>
–	–	k?	–
<g/>
251	[number]	k4	251
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
kočka	kočka	k1gFnSc1	kočka
divoká	divoký	k2eAgFnSc1d1	divoká
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Felis	Felis	k1gFnSc2	Felis
silvestris	silvestris	k1gFnSc2	silvestris
ve	v	k7c6	v
Wikidruzích	Wikidruh	k1gInPc6	Wikidruh
</s>
</p>
<p>
<s>
BioLib	BioLib	k1gMnSc1	BioLib
</s>
</p>
<p>
<s>
České	český	k2eAgFnPc1d1	Česká
stránky	stránka	k1gFnPc1	stránka
o	o	k7c6	o
kočce	kočka	k1gFnSc6	kočka
divoké	divoký	k2eAgFnSc2d1	divoká
</s>
</p>
<p>
<s>
Živé	živý	k2eAgNnSc1d1	živé
srdce	srdce	k1gNnSc1	srdce
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
:	:	kIx,	:
Kočka	kočka	k1gFnSc1	kočka
divoká	divoký	k2eAgFnSc1d1	divoká
(	(	kIx(	(
<g/>
krátký	krátký	k2eAgInSc4d1	krátký
film	film	k1gInSc4	film
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
iDnes	iDnes	k1gInSc1	iDnes
<g/>
:	:	kIx,	:
V	v	k7c6	v
Podyjí	Podyjí	k1gNnSc6	Podyjí
žije	žít	k5eAaImIp3nS	žít
vzácná	vzácný	k2eAgFnSc1d1	vzácná
kočka	kočka	k1gFnSc1	kočka
divoká	divoký	k2eAgFnSc1d1	divoká
<g/>
.	.	kIx.	.
</s>
<s>
Přírodovědci	přírodovědec	k1gMnPc1	přírodovědec
ji	on	k3xPp3gFnSc4	on
viděli	vidět	k5eAaImAgMnP	vidět
už	už	k6eAd1	už
několikrát	několikrát	k6eAd1	několikrát
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Naturfoto	Naturfota	k1gFnSc5	Naturfota
<g/>
:	:	kIx,	:
kočka	kočka	k1gFnSc1	kočka
divoká	divoký	k2eAgFnSc1d1	divoká
</s>
</p>
<p>
<s>
iForum	iForum	k1gInSc1	iForum
<g/>
:	:	kIx,	:
Hledá	hledat	k5eAaImIp3nS	hledat
se	se	k3xPyFc4	se
kočka	kočka	k1gFnSc1	kočka
divoká	divoký	k2eAgFnSc1d1	divoká
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
pátráním	pátrání	k1gNnSc7	pátrání
může	moct	k5eAaImIp3nS	moct
pomoci	pomoct	k5eAaPmF	pomoct
každý	každý	k3xTgMnSc1	každý
</s>
</p>
