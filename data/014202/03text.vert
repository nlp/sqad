<s>
Národní	národní	k2eAgInSc1d1
památkový	památkový	k2eAgInSc1d1
ústav	ústav	k1gInSc1
</s>
<s>
Národní	národní	k2eAgInSc4d1
památkový	památkový	k2eAgInSc4d1
ústav	ústav	k1gInSc4
Generální	generální	k2eAgNnSc1d1
ředitelství	ředitelství	k1gNnSc1
Národního	národní	k2eAgInSc2d1
památkového	památkový	k2eAgInSc2d1
ústavu	ústav	k1gInSc2
sídlí	sídlet	k5eAaImIp3nS
v	v	k7c6
Ledebourském	Ledebourský	k2eAgInSc6d1
paláci	palác	k1gInSc6
v	v	k7c6
Praze	Praha	k1gFnSc6
na	na	k7c6
Malé	Malé	k2eAgFnSc6d1
Straně	strana	k1gFnSc6
Zkratka	zkratka	k1gFnSc1
</s>
<s>
NPÚ	NPÚ	kA
Předchůdce	předchůdce	k1gMnSc1
</s>
<s>
Státní	státní	k2eAgInSc1d1
ústav	ústav	k1gInSc1
památkové	památkový	k2eAgFnSc2d1
péče	péče	k1gFnSc2
Vznik	vznik	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2003	#num#	k4
Typ	typ	k1gInSc1
</s>
<s>
odborná	odborný	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
státní	státní	k2eAgFnSc2d1
památkové	památkový	k2eAgFnSc2d1
péče	péče	k1gFnSc2
Právní	právní	k2eAgFnSc1d1
forma	forma	k1gFnSc1
</s>
<s>
státní	státní	k2eAgFnSc1d1
příspěvková	příspěvkový	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
zřizovaná	zřizovaný	k2eAgFnSc1d1
ministerstvem	ministerstvo	k1gNnSc7
kultury	kultura	k1gFnSc2
Účel	účel	k1gInSc1
</s>
<s>
odborná	odborný	k2eAgFnSc1d1
a	a	k8xC
výzkumná	výzkumný	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
státní	státní	k2eAgFnSc2d1
památkové	památkový	k2eAgFnSc2d1
péče	péče	k1gFnSc2
dle	dle	k7c2
zákona	zákon	k1gInSc2
č.	č.	k?
20	#num#	k4
<g/>
/	/	kIx~
<g/>
1987	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
státní	státní	k2eAgFnSc6d1
památkové	památkový	k2eAgFnSc6d1
péči	péče	k1gFnSc6
<g/>
;	;	kIx,
správa	správa	k1gFnSc1
zpřístupněných	zpřístupněný	k2eAgFnPc2d1
památek	památka	k1gFnPc2
ve	v	k7c6
vlastnictví	vlastnictví	k1gNnSc6
státu	stát	k1gInSc2
Sídlo	sídlo	k1gNnSc4
</s>
<s>
Valdštejnské	valdštejnský	k2eAgNnSc1d1
náměstí	náměstí	k1gNnSc1
162	#num#	k4
<g/>
/	/	kIx~
<g/>
3	#num#	k4
<g/>
Praha	Praha	k1gFnSc1
1	#num#	k4
<g/>
,	,	kIx,
Malá	malý	k2eAgFnSc1d1
Strana	strana	k1gFnSc1
<g/>
118	#num#	k4
01	#num#	k4
Praha	Praha	k1gFnSc1
011	#num#	k4
Místo	místo	k7c2
</s>
<s>
Praha	Praha	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
5	#num#	k4
<g/>
′	′	k?
<g/>
25,05	25,05	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
24	#num#	k4
<g/>
′	′	k?
<g/>
17,71	17,71	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Působnost	působnost	k1gFnSc1
</s>
<s>
celostátní	celostátní	k2eAgFnSc1d1
Generální	generální	k2eAgFnSc1d1
ředitelka	ředitelka	k1gFnSc1
</s>
<s>
ing.	ing.	kA
arch	arch	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naděžda	Naděžda	k1gFnSc1
Goryczková	Goryczkový	k2eAgFnSc1d1
Zaměstnanců	zaměstnanec	k1gMnPc2
</s>
<s>
1906	#num#	k4
(	(	kIx(
<g/>
9	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2016	#num#	k4
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
www.npu.cz/cs	www.npu.cz/cs	k6eAd1
Datová	datový	k2eAgFnSc1d1
schránka	schránka	k1gFnSc1
</s>
<s>
2	#num#	k4
<g/>
cy	cy	k?
<g/>
8	#num#	k4
<g/>
h	h	k?
<g/>
6	#num#	k4
<g/>
t	t	k?
IČO	IČO	kA
</s>
<s>
75032333	#num#	k4
(	(	kIx(
<g/>
VR	vr	k0
<g/>
)	)	kIx)
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
můžou	můžou	k?
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Národní	národní	k2eAgInSc1d1
památkový	památkový	k2eAgInSc1d1
ústav	ústav	k1gInSc1
(	(	kIx(
<g/>
NPÚ	NPÚ	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
odbornou	odborný	k2eAgFnSc7d1
a	a	k8xC
výzkumnou	výzkumný	k2eAgFnSc7d1
organizací	organizace	k1gFnSc7
státní	státní	k2eAgFnSc2d1
památkové	památkový	k2eAgFnSc2d1
péče	péče	k1gFnSc2
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
s	s	k7c7
celostátní	celostátní	k2eAgFnSc7d1
působností	působnost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
státní	státní	k2eAgFnSc7d1
příspěvkovou	příspěvkový	k2eAgFnSc7d1
organizací	organizace	k1gFnSc7
zřízenou	zřízený	k2eAgFnSc7d1
ke	k	k7c3
dni	den	k1gInSc3
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2003	#num#	k4
podle	podle	k7c2
zákona	zákon	k1gInSc2
České	český	k2eAgFnSc2d1
národní	národní	k2eAgFnSc2d1
rady	rada	k1gFnSc2
o	o	k7c6
státní	státní	k2eAgFnSc6d1
památkové	památkový	k2eAgFnSc6d1
péči	péče	k1gFnSc6
(	(	kIx(
<g/>
zák	zák	k?
<g/>
.	.	kIx.
č.	č.	k?
20	#num#	k4
<g/>
/	/	kIx~
<g/>
1987	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
ve	v	k7c6
znění	znění	k1gNnSc6
pozdějších	pozdní	k2eAgFnPc2d2
novel	novela	k1gFnPc2
<g/>
)	)	kIx)
Ministerstvem	ministerstvo	k1gNnSc7
kultury	kultura	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Počátky	počátek	k1gInPc4
státem	stát	k1gInSc7
zajišťované	zajišťovaný	k2eAgFnSc2d1
ochrany	ochrana	k1gFnSc2
památek	památka	k1gFnPc2
spadají	spadat	k5eAaImIp3nP,k5eAaPmIp3nP
do	do	k7c2
poloviny	polovina	k1gFnSc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byla	být	k5eAaImAgFnS
zřízena	zřízen	k2eAgFnSc1d1
C.	C.	kA
a	a	k8xC
k.	k.	k?
Centrální	centrální	k2eAgFnSc2d1
komise	komise	k1gFnSc2
pro	pro	k7c4
průzkum	průzkum	k1gInSc4
a	a	k8xC
uchování	uchování	k1gNnSc4
stavebních	stavební	k2eAgFnPc2d1
a	a	k8xC
uměleckých	umělecký	k2eAgFnPc2d1
památek	památka	k1gFnPc2
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
(	(	kIx(
<g/>
1850	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postupně	postupně	k6eAd1
se	se	k3xPyFc4
vytvořily	vytvořit	k5eAaPmAgInP
zemské	zemský	k2eAgInPc1d1
památkové	památkový	k2eAgInPc1d1
úřady	úřad	k1gInPc1
se	s	k7c7
sídly	sídlo	k1gNnPc7
v	v	k7c6
Praze	Praha	k1gFnSc6
a	a	k8xC
Brně	Brno	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
roce	rok	k1gInSc6
1918	#num#	k4
se	se	k3xPyFc4
k	k	k7c3
nim	on	k3xPp3gMnPc3
připojil	připojit	k5eAaPmAgInS
komisariát	komisariát	k1gInSc4
v	v	k7c6
Bratislavě	Bratislava	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
roce	rok	k1gInSc6
1945	#num#	k4
na	na	k7c4
jejich	jejich	k3xOp3gMnPc4
činnost	činnost	k1gFnSc1
navázala	navázat	k5eAaPmAgFnS
Státní	státní	k2eAgFnSc1d1
památková	památkový	k2eAgFnSc1d1
správa	správa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Předchůdci	předchůdce	k1gMnPc1
Národního	národní	k2eAgInSc2d1
památkového	památkový	k2eAgInSc2d1
ústavu	ústav	k1gInSc2
byly	být	k5eAaImAgFnP
od	od	k7c2
roku	rok	k1gInSc2
1958	#num#	k4
Státní	státní	k2eAgInSc1d1
ústav	ústav	k1gInSc1
památkové	památkový	k2eAgFnSc2d1
péče	péče	k1gFnSc2
a	a	k8xC
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
<g/>
,	,	kIx,
vzniklý	vzniklý	k2eAgInSc1d1
ze	z	k7c2
zákona	zákon	k1gInSc2
22	#num#	k4
<g/>
/	/	kIx~
<g/>
1958	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
a	a	k8xC
krajská	krajský	k2eAgNnPc1d1
střediska	středisko	k1gNnPc1
státní	státní	k2eAgFnSc2d1
památkové	památkový	k2eAgFnSc2d1
péče	péče	k1gFnSc2
a	a	k8xC
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zvláštností	zvláštnost	k1gFnPc2
bylo	být	k5eAaImAgNnS
Pražské	pražský	k2eAgNnSc1d1
středisko	středisko	k1gNnSc1
státní	státní	k2eAgFnSc2d1
památkové	památkový	k2eAgFnSc2d1
péče	péče	k1gFnSc2
a	a	k8xC
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
<g/>
,	,	kIx,
dalšími	další	k2eAgFnPc7d1
anomáliemi	anomálie	k1gFnPc7
byla	být	k5eAaImAgNnP
Okresní	okresní	k2eAgNnPc1d1
střediska	středisko	k1gNnPc1
státní	státní	k2eAgFnSc2d1
památkové	památkový	k2eAgFnSc2d1
péče	péče	k1gFnSc2
a	a	k8xC
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
nejstarší	starý	k2eAgNnSc4d3
vzniklo	vzniknout	k5eAaPmAgNnS
v	v	k7c6
roce	rok	k1gInSc6
1965	#num#	k4
v	v	k7c6
Olomouci	Olomouc	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1991	#num#	k4
byl	být	k5eAaImAgInS
obor	obor	k1gInSc1
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
převeden	převést	k5eAaPmNgInS
na	na	k7c6
nově	nova	k1gFnSc6
vzniklé	vzniklý	k2eAgNnSc4d1
ministerstvo	ministerstvo	k1gNnSc4
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
a	a	k8xC
ústav	ústav	k1gInSc1
podřízený	podřízený	k2eAgInSc1d1
ministerstvu	ministerstvo	k1gNnSc6
kultury	kultura	k1gFnSc2
byl	být	k5eAaImAgInS
přejmenován	přejmenovat	k5eAaPmNgInS
na	na	k7c4
Státní	státní	k2eAgInSc4d1
ústav	ústav	k1gInSc4
památkové	památkový	k2eAgFnSc2d1
péče	péče	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
zániku	zánik	k1gInSc6
krajských	krajský	k2eAgInPc2d1
národních	národní	k2eAgInPc2d1
výborů	výbor	k1gInPc2
přešla	přejít	k5eAaPmAgNnP
krajská	krajský	k2eAgNnPc1d1
střediska	středisko	k1gNnPc1
památkové	památkový	k2eAgFnSc2d1
péče	péče	k1gFnSc2
pod	pod	k7c4
ministerstvo	ministerstvo	k1gNnSc4
kultury	kultura	k1gFnSc2
(	(	kIx(
<g/>
pražské	pražský	k2eAgNnSc1d1
středisko	středisko	k1gNnSc1
až	až	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
2001	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
pod	pod	k7c4
názvy	název	k1gInPc4
památkový	památkový	k2eAgInSc1d1
ústav	ústav	k1gInSc1
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
2001	#num#	k4
státní	státní	k2eAgInSc1d1
památkový	památkový	k2eAgInSc1d1
ústav	ústav	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Splynutím	splynutí	k1gNnSc7
Státního	státní	k2eAgInSc2d1
ústavu	ústav	k1gInSc2
památkové	památkový	k2eAgFnSc2d1
péče	péče	k1gFnSc2
a	a	k8xC
krajských	krajský	k2eAgInPc2d1
státních	státní	k2eAgInPc2d1
památkových	památkový	k2eAgInPc2d1
ústavů	ústav	k1gInPc2
vznikl	vzniknout	k5eAaPmAgInS
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
lednu	leden	k1gInSc6
2003	#num#	k4
Národní	národní	k2eAgInSc1d1
památkový	památkový	k2eAgInSc1d1
ústav	ústav	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Dřívější	dřívější	k2eAgNnPc1d1
krajská	krajský	k2eAgNnPc1d1
střediska	středisko	k1gNnPc1
se	se	k3xPyFc4
stala	stát	k5eAaPmAgNnP
územními	územní	k2eAgNnPc7d1
pracovišti	pracoviště	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
byly	být	k5eAaImAgInP
v	v	k7c6
rámci	rámec	k1gInSc6
NPÚ	NPÚ	kA
organizačně	organizačně	k6eAd1
vyčleněny	vyčlenit	k5eAaPmNgFnP
čtyři	čtyři	k4xCgFnPc1
územní	územní	k2eAgFnPc1d1
památkové	památkový	k2eAgFnPc1d1
správy	správa	k1gFnPc1
<g/>
,	,	kIx,
zejména	zejména	k9
zajišťující	zajišťující	k2eAgInSc4d1
návštěvnický	návštěvnický	k2eAgInSc4d1
provoz	provoz	k1gInSc4
zpřístupněných	zpřístupněný	k2eAgFnPc2d1
památek	památka	k1gFnPc2
ve	v	k7c6
správě	správa	k1gFnSc6
NPÚ	NPÚ	kA
<g/>
.	.	kIx.
</s>
<s>
Zákonný	zákonný	k2eAgInSc1d1
rámec	rámec	k1gInSc1
</s>
<s>
Zákon	zákon	k1gInSc1
o	o	k7c6
státní	státní	k2eAgFnSc6d1
památkové	památkový	k2eAgFnSc6d1
péči	péče	k1gFnSc6
nestanoví	stanovit	k5eNaPmIp3nS
název	název	k1gInSc4
organizace	organizace	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
§	§	k?
32	#num#	k4
aktuálního	aktuální	k2eAgNnSc2d1
znění	znění	k1gNnSc2
o	o	k7c6
ní	on	k3xPp3gFnSc6
hovoří	hovořit	k5eAaImIp3nS
jako	jako	k9
o	o	k7c6
„	„	k?
<g/>
odborné	odborný	k2eAgFnSc3d1
organizaci	organizace	k1gFnSc3
státní	státní	k2eAgFnSc2d1
památkové	památkový	k2eAgFnSc2d1
péče	péče	k1gFnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
původním	původní	k2eAgNnSc6d1
znění	znění	k1gNnSc6
z	z	k7c2
roku	rok	k1gInSc2
1987	#num#	k4
psal	psát	k5eAaImAgInS
o	o	k7c4
„	„	k?
<g/>
ústřední	ústřední	k2eAgFnSc4d1
organizaci	organizace	k1gFnSc4
státní	státní	k2eAgFnSc2d1
památkové	památkový	k2eAgFnSc2d1
péče	péče	k1gFnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
jíž	jenž	k3xRgFnSc3
tehdy	tehdy	k6eAd1
podléhaly	podléhat	k5eAaImAgFnP
krajské	krajský	k2eAgFnPc4d1
organizace	organizace	k1gFnPc4
památkové	památkový	k2eAgFnSc2d1
péče	péče	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
odstavce	odstavec	k1gInSc2
1	#num#	k4
aktuálního	aktuální	k2eAgNnSc2d1
znění	znění	k1gNnSc2
je	být	k5eAaImIp3nS
organizací	organizace	k1gFnSc7
pro	pro	k7c4
výkon	výkon	k1gInSc4
a	a	k8xC
koordinaci	koordinace	k1gFnSc4
veškeré	veškerý	k3xTgFnSc2
odborné	odborný	k2eAgFnSc2d1
činnosti	činnost	k1gFnSc2
v	v	k7c6
oboru	obor	k1gInSc6
státní	státní	k2eAgFnSc2d1
památkové	památkový	k2eAgFnSc2d1
péče	péče	k1gFnSc2
k	k	k7c3
zabezpečení	zabezpečení	k1gNnSc3
jednoty	jednota	k1gFnSc2
kulturně	kulturně	k6eAd1
politických	politický	k2eAgInPc2d1
záměrů	záměr	k1gInPc2
a	a	k8xC
ideově	ideově	k6eAd1
metodických	metodický	k2eAgInPc2d1
<g/>
,	,	kIx,
ekonomických	ekonomický	k2eAgNnPc2d1
a	a	k8xC
technických	technický	k2eAgNnPc2d1
hledisek	hledisko	k1gNnPc2
<g/>
,	,	kIx,
jakož	jakož	k8xC
i	i	k9
perspektivního	perspektivní	k2eAgInSc2d1
rozvoje	rozvoj	k1gInSc2
státní	státní	k2eAgFnSc2d1
památkové	památkový	k2eAgFnSc2d1
péče	péče	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
§	§	k?
25	#num#	k4
je	být	k5eAaImIp3nS
podřízena	podřídit	k5eAaPmNgFnS
ministerstvu	ministerstvo	k1gNnSc3
kultury	kultura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
§	§	k?
26	#num#	k4
odst	odsta	k1gFnPc2
<g/>
.	.	kIx.
2	#num#	k4
písmeno	písmeno	k1gNnSc4
j	j	k?
<g/>
)	)	kIx)
zákon	zákon	k1gInSc1
stanoví	stanovit	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
ministerstvo	ministerstvo	k1gNnSc1
vydává	vydávat	k5eAaImIp3nS,k5eAaPmIp3nS
statut	statut	k1gInSc4
odborné	odborný	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
státní	státní	k2eAgFnSc2d1
památkové	památkový	k2eAgFnSc2d1
péče	péče	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
státní	státní	k2eAgFnSc7d1
příspěvkovou	příspěvkový	k2eAgFnSc7d1
organizací	organizace	k1gFnSc7
s	s	k7c7
celostátní	celostátní	k2eAgFnSc7d1
působností	působnost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
§	§	k?
27	#num#	k4
zákon	zákon	k1gInSc1
stanoví	stanovit	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
památková	památkový	k2eAgFnSc1d1
inspekce	inspekce	k1gFnSc1
<g/>
,	,	kIx,
zřízená	zřízený	k2eAgFnSc1d1
rovněž	rovněž	k9
ministerstvem	ministerstvo	k1gNnSc7
kultury	kultura	k1gFnSc2
<g/>
,	,	kIx,
spolupracuje	spolupracovat	k5eAaImIp3nS
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
i	i	k9
odbornou	odborný	k2eAgFnSc7d1
organizací	organizace	k1gFnSc7
státní	státní	k2eAgFnSc2d1
památkové	památkový	k2eAgFnSc2d1
péče	péče	k1gFnSc2
a	a	k8xC
opírá	opírat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
její	její	k3xOp3gFnSc4
pomoc	pomoc	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
§	§	k?
27	#num#	k4
<g/>
a	a	k8xC
s	s	k7c7
ní	on	k3xPp3gFnSc7
spolupracují	spolupracovat	k5eAaImIp3nP
i	i	k9
celní	celní	k2eAgInPc4d1
úřady	úřad	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
§	§	k?
29	#num#	k4
se	se	k3xPyFc4
o	o	k7c4
její	její	k3xOp3gFnSc4
odbornou	odborný	k2eAgFnSc4d1
pomoc	pomoc	k1gFnSc4
při	při	k7c6
plnění	plnění	k1gNnSc6
svých	svůj	k3xOyFgInPc2
úkolů	úkol	k1gInPc2
opírají	opírat	k5eAaImIp3nP
i	i	k9
obecní	obecní	k2eAgInPc4d1
úřady	úřad	k1gInPc4
obcí	obec	k1gFnPc2
s	s	k7c7
rozšířenou	rozšířený	k2eAgFnSc7d1
působností	působnost	k1gFnSc7
<g/>
;	;	kIx,
odborná	odborný	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
SPP	SPP	kA
se	se	k3xPyFc4
vyjadřuje	vyjadřovat	k5eAaImIp3nS
ke	k	k7c3
jmenování	jmenování	k1gNnSc3
konzervátora	konzervátor	k1gMnSc2
státní	státní	k2eAgFnSc2d1
památkové	památkový	k2eAgFnSc2d1
péče	péče	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
obecním	obecní	k2eAgInSc7d1
úřadem	úřad	k1gInSc7
jmenován	jmenovat	k5eAaBmNgInS,k5eAaImNgInS
jako	jako	k9
dobrovolný	dobrovolný	k2eAgMnSc1d1
pracovník	pracovník	k1gMnSc1
členem	člen	k1gMnSc7
komise	komise	k1gFnSc2
SPP	SPP	kA
zřízené	zřízený	k2eAgFnSc2d1
radou	rada	k1gFnSc7
ORP	ORP	kA
<g/>
,	,	kIx,
a	a	k8xC
poskytuje	poskytovat	k5eAaImIp3nS
konzervátorovi	konzervátor	k1gMnSc3
odbornou	odborný	k2eAgFnSc4d1
pomoc	pomoc	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obce	obec	k1gFnSc2
podle	podle	k7c2
§	§	k?
30	#num#	k4
při	při	k7c6
své	svůj	k3xOyFgFnSc6
péči	péče	k1gFnSc6
o	o	k7c4
kulturní	kulturní	k2eAgFnPc4d1
památky	památka	k1gFnPc4
a	a	k8xC
kontrole	kontrola	k1gFnSc6
vlastníků	vlastník	k1gMnPc2
kulturních	kulturní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
vycházejí	vycházet	k5eAaImIp3nP
z	z	k7c2
odborných	odborný	k2eAgFnPc2d1
vyjádření	vyjádření	k1gNnPc2
odborné	odborný	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
státní	státní	k2eAgFnSc2d1
památkové	památkový	k2eAgFnSc2d1
péče	péče	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Činnost	činnost	k1gFnSc1
</s>
<s>
Odborná	odborný	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
státní	státní	k2eAgFnSc2d1
památkové	památkový	k2eAgFnSc2d1
péče	péče	k1gFnSc2
podle	podle	k7c2
§	§	k?
32	#num#	k4
odst	odsta	k1gFnPc2
<g/>
.	.	kIx.
2	#num#	k4
zákona	zákon	k1gInSc2
o	o	k7c6
státní	státní	k2eAgFnSc6d1
památkové	památkový	k2eAgFnSc6d1
péči	péče	k1gFnSc6
</s>
<s>
zpracovává	zpracovávat	k5eAaImIp3nS
rozbory	rozbor	k1gInPc4
stavu	stav	k1gInSc2
a	a	k8xC
vývoje	vývoj	k1gInSc2
státní	státní	k2eAgFnSc2d1
památkové	památkový	k2eAgFnSc2d1
péče	péče	k1gFnSc2
<g/>
,	,	kIx,
podklady	podklad	k1gInPc4
pro	pro	k7c4
prognózy	prognóza	k1gFnPc4
<g/>
,	,	kIx,
koncepce	koncepce	k1gFnPc4
a	a	k8xC
dlouhodobé	dlouhodobý	k2eAgInPc4d1
výhledy	výhled	k1gInPc4
rozvoje	rozvoj	k1gInSc2
státní	státní	k2eAgFnSc2d1
památkové	památkový	k2eAgFnSc2d1
péče	péče	k1gFnSc2
<g/>
,	,	kIx,
</s>
<s>
organizuje	organizovat	k5eAaBmIp3nS
<g/>
,	,	kIx,
koordinuje	koordinovat	k5eAaBmIp3nS
a	a	k8xC
plní	plnit	k5eAaImIp3nS
vědeckovýzkumné	vědeckovýzkumný	k2eAgInPc4d1
úkoly	úkol	k1gInPc4
státní	státní	k2eAgFnSc2d1
památkové	památkový	k2eAgFnSc2d1
péče	péče	k1gFnSc2
<g/>
,	,	kIx,
rozpracovává	rozpracovávat	k5eAaImIp3nS
teorii	teorie	k1gFnSc4
a	a	k8xC
metodologii	metodologie	k1gFnSc4
státní	státní	k2eAgFnSc2d1
památkové	památkový	k2eAgFnSc2d1
péče	péče	k1gFnSc2
a	a	k8xC
metodiku	metodika	k1gFnSc4
společenského	společenský	k2eAgNnSc2d1
uplatnění	uplatnění	k1gNnSc2
kulturních	kulturní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
<g/>
,	,	kIx,
</s>
<s>
plní	plnit	k5eAaImIp3nS
úkoly	úkol	k1gInPc4
odborně	odborně	k6eAd1
metodického	metodický	k2eAgNnSc2d1
<g/>
,	,	kIx,
dokumentačního	dokumentační	k2eAgNnSc2d1
a	a	k8xC
informačního	informační	k2eAgNnSc2d1
pracoviště	pracoviště	k1gNnSc2
pro	pro	k7c4
úsek	úsek	k1gInSc4
státní	státní	k2eAgFnSc2d1
památkové	památkový	k2eAgFnSc2d1
péče	péče	k1gFnSc2
a	a	k8xC
zabezpečuje	zabezpečovat	k5eAaImIp3nS
průzkumy	průzkum	k1gInPc4
<g/>
,	,	kIx,
výzkumy	výzkum	k1gInPc4
a	a	k8xC
dokumentaci	dokumentace	k1gFnSc4
kulturních	kulturní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
<g/>
,	,	kIx,
památkových	památkový	k2eAgFnPc2d1
rezervací	rezervace	k1gFnPc2
a	a	k8xC
památkových	památkový	k2eAgFnPc2d1
zón	zóna	k1gFnPc2
a	a	k8xC
je	být	k5eAaImIp3nS
současně	současně	k6eAd1
poskytovatelem	poskytovatel	k1gMnSc7
údajů	údaj	k1gInPc2
podle	podle	k7c2
zvláštního	zvláštní	k2eAgInSc2d1
právního	právní	k2eAgInSc2d1
předpisu	předpis	k1gInSc2
<g/>
,	,	kIx,
</s>
<s>
vede	vést	k5eAaImIp3nS
ústřední	ústřední	k2eAgInSc1d1
seznam	seznam	k1gInSc1
kulturních	kulturní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
<g/>
,	,	kIx,
</s>
<s>
připravuje	připravovat	k5eAaImIp3nS
odborné	odborný	k2eAgInPc4d1
podklady	podklad	k1gInPc4
pro	pro	k7c4
ministerstvo	ministerstvo	k1gNnSc4
kultury	kultura	k1gFnSc2
<g/>
,	,	kIx,
zejména	zejména	k9
pro	pro	k7c4
prohlášení	prohlášení	k1gNnSc4
věcí	věc	k1gFnPc2
za	za	k7c4
kulturní	kulturní	k2eAgFnPc4d1
památky	památka	k1gFnPc4
<g/>
,	,	kIx,
</s>
<s>
zpracovává	zpracovávat	k5eAaImIp3nS
potřebné	potřebný	k2eAgInPc4d1
odborné	odborný	k2eAgInPc4d1
podklady	podklad	k1gInPc4
pro	pro	k7c4
ostatní	ostatní	k2eAgInPc4d1
orgány	orgán	k1gInPc4
státní	státní	k2eAgFnSc2d1
památkové	památkový	k2eAgFnSc2d1
péče	péče	k1gFnSc2
<g/>
,	,	kIx,
metodicky	metodicky	k6eAd1
usměrňuje	usměrňovat	k5eAaImIp3nS
činnost	činnost	k1gFnSc1
konzervátorů	konzervátor	k1gMnPc2
a	a	k8xC
zpravodajů	zpravodaj	k1gMnPc2
a	a	k8xC
poskytuje	poskytovat	k5eAaImIp3nS
bezplatnou	bezplatný	k2eAgFnSc4d1
odbornou	odborný	k2eAgFnSc4d1
pomoc	pomoc	k1gFnSc4
vlastníkům	vlastník	k1gMnPc3
kulturních	kulturní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
při	při	k7c6
zajišťování	zajišťování	k1gNnSc6
péče	péče	k1gFnSc2
o	o	k7c4
kulturní	kulturní	k2eAgFnPc4d1
památky	památka	k1gFnPc4
<g/>
,	,	kIx,
</s>
<s>
zabezpečuje	zabezpečovat	k5eAaImIp3nS
odborný	odborný	k2eAgInSc4d1
dohled	dohled	k1gInSc4
nad	nad	k7c7
prováděním	provádění	k1gNnSc7
komplexní	komplexní	k2eAgFnSc2d1
péče	péče	k1gFnSc2
o	o	k7c4
kulturní	kulturní	k2eAgFnPc4d1
památky	památka	k1gFnPc4
a	a	k8xC
nad	nad	k7c7
jejich	jejich	k3xOp3gNnSc7
soustavným	soustavný	k2eAgNnSc7d1
využíváním	využívání	k1gNnSc7
<g/>
,	,	kIx,
</s>
<s>
sleduje	sledovat	k5eAaImIp3nS
kulturně	kulturně	k6eAd1
výchovné	výchovný	k2eAgNnSc1d1
využití	využití	k1gNnSc1
kulturních	kulturní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
a	a	k8xC
jejich	jejich	k3xOp3gFnSc4
propagaci	propagace	k1gFnSc4
a	a	k8xC
zabezpečuje	zabezpečovat	k5eAaImIp3nS
kulturně	kulturně	k6eAd1
výchovné	výchovný	k2eAgNnSc1d1
využití	využití	k1gNnSc1
a	a	k8xC
zpřístupnění	zpřístupnění	k1gNnSc1
kulturních	kulturní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
<g/>
,	,	kIx,
s	s	k7c7
nimiž	jenž	k3xRgInPc7
hospodaří	hospodařit	k5eAaImIp3nS
<g/>
,	,	kIx,
</s>
<s>
zabezpečuje	zabezpečovat	k5eAaImIp3nS
další	další	k2eAgNnSc1d1
vzdělávání	vzdělávání	k1gNnSc1
pracovníků	pracovník	k1gMnPc2
v	v	k7c6
oboru	obor	k1gInSc6
státní	státní	k2eAgFnSc2d1
památkové	památkový	k2eAgFnSc2d1
péče	péče	k1gFnSc2
<g/>
,	,	kIx,
</s>
<s>
plní	plnit	k5eAaImIp3nP
další	další	k2eAgInPc4d1
úkoly	úkol	k1gInPc4
na	na	k7c6
úseku	úsek	k1gInSc6
státní	státní	k2eAgFnSc2d1
památkové	památkový	k2eAgFnSc2d1
péče	péče	k1gFnSc2
<g/>
,	,	kIx,
kterými	který	k3yQgInPc7,k3yRgInPc7,k3yIgInPc7
ji	on	k3xPp3gFnSc4
pověří	pověřit	k5eAaPmIp3nS
ministerstvo	ministerstvo	k1gNnSc1
kultury	kultura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Národní	národní	k2eAgInSc1d1
památkový	památkový	k2eAgInSc1d1
ústav	ústav	k1gInSc1
zajišťuje	zajišťovat	k5eAaImIp3nS
a	a	k8xC
vykonává	vykonávat	k5eAaImIp3nS
základní	základní	k2eAgInSc4d1
i	i	k8xC
aplikovaný	aplikovaný	k2eAgInSc4d1
vědecký	vědecký	k2eAgInSc4d1
výzkum	výzkum	k1gInSc4
v	v	k7c6
oblasti	oblast	k1gFnSc6
památkové	památkový	k2eAgFnSc2d1
péče	péče	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Provádí	provádět	k5eAaImIp3nS
též	též	k9
další	další	k2eAgFnPc4d1
odborné	odborný	k2eAgFnPc4d1
<g/>
,	,	kIx,
pedagogické	pedagogický	k2eAgFnPc4d1
<g/>
,	,	kIx,
vzdělávací	vzdělávací	k2eAgFnPc4d1
<g/>
,	,	kIx,
publikační	publikační	k2eAgFnPc4d1
a	a	k8xC
popularizační	popularizační	k2eAgFnPc4d1
činnosti	činnost	k1gFnPc4
k	k	k7c3
zajištění	zajištění	k1gNnSc3
kvality	kvalita	k1gFnSc2
a	a	k8xC
odbornosti	odbornost	k1gFnSc2
péče	péče	k1gFnSc2
o	o	k7c4
kulturní	kulturní	k2eAgFnPc4d1
památky	památka	k1gFnPc4
a	a	k8xC
památkově	památkově	k6eAd1
chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odborně	odborně	k6eAd1
a	a	k8xC
metodicky	metodicky	k6eAd1
usměrňuje	usměrňovat	k5eAaImIp3nS
a	a	k8xC
podporuje	podporovat	k5eAaImIp3nS
péči	péče	k1gFnSc4
o	o	k7c4
památky	památka	k1gFnPc4
a	a	k8xC
památkově	památkově	k6eAd1
chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
ve	v	k7c6
vlastnictví	vlastnictví	k1gNnSc6
a	a	k8xC
správě	správa	k1gFnSc6
jiných	jiný	k2eAgInPc2d1
subjektů	subjekt	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Osoby	osoba	k1gFnPc1
pověřené	pověřený	k2eAgFnPc1d1
orgány	orgán	k1gInPc4
státní	státní	k2eAgFnSc2d1
památkové	památkový	k2eAgFnSc2d1
péče	péče	k1gFnSc2
nebo	nebo	k8xC
celními	celní	k2eAgInPc7d1
úřady	úřad	k1gInPc7
vybavené	vybavený	k2eAgFnSc2d1
osvědčením	osvědčení	k1gNnPc3
od	od	k7c2
pověřujícího	pověřující	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
nebo	nebo	k8xC
organizace	organizace	k1gFnSc2
jsou	být	k5eAaImIp3nP
podle	podle	k7c2
§	§	k?
34	#num#	k4
zákona	zákon	k1gInSc2
o	o	k7c6
státní	státní	k2eAgFnSc6d1
památkové	památkový	k2eAgFnSc6d1
péči	péče	k1gFnSc6
oprávněny	oprávnit	k5eAaPmNgFnP
vstupovat	vstupovat	k5eAaImF
do	do	k7c2
podniků	podnik	k1gInPc2
<g/>
,	,	kIx,
závodů	závod	k1gInPc2
<g/>
,	,	kIx,
zařízení	zařízení	k1gNnPc2
<g/>
,	,	kIx,
objektů	objekt	k1gInPc2
a	a	k8xC
na	na	k7c4
ostatní	ostatní	k2eAgFnPc4d1
nemovitosti	nemovitost	k1gFnPc4
a	a	k8xC
vykonávat	vykonávat	k5eAaImF
v	v	k7c6
nich	on	k3xPp3gInPc6
potřebné	potřebný	k2eAgFnPc1d1
odborné	odborný	k2eAgFnPc4d1
práce	práce	k1gFnPc4
na	na	k7c4
ochranu	ochrana	k1gFnSc4
kulturních	kulturní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
nebo	nebo	k8xC
pro	pro	k7c4
vědecké	vědecký	k2eAgInPc4d1
účely	účel	k1gInPc4
<g/>
,	,	kIx,
zejména	zejména	k9
dokumentační	dokumentační	k2eAgNnSc4d1
a	a	k8xC
konzervační	konzervační	k2eAgNnSc4d1
<g/>
,	,	kIx,
jakož	jakož	k8xC
i	i	k9
odborný	odborný	k2eAgInSc4d1
dozor	dozor	k1gInSc4
<g/>
,	,	kIx,
požadovat	požadovat	k5eAaImF
za	za	k7c7
tím	ten	k3xDgInSc7
účelem	účel	k1gInSc7
potřebné	potřebný	k2eAgInPc4d1
údaje	údaj	k1gInPc4
a	a	k8xC
vysvětlení	vysvětlení	k1gNnSc4
a	a	k8xC
nahlížet	nahlížet	k5eAaImF
do	do	k7c2
příslušných	příslušný	k2eAgInPc2d1
dokladů	doklad	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Se	s	k7c7
státním	státní	k2eAgNnSc7d1
hospodářským	hospodářský	k2eAgNnSc7d1
a	a	k8xC
služebním	služební	k2eAgNnSc7d1
tajemstvím	tajemství	k1gNnSc7
se	se	k3xPyFc4
přitom	přitom	k6eAd1
však	však	k9
mohou	moct	k5eAaImIp3nP
seznamovat	seznamovat	k5eAaImF
<g/>
,	,	kIx,
jen	jen	k9
jsou	být	k5eAaImIp3nP
<g/>
-li	-li	k?
k	k	k7c3
tomu	ten	k3xDgNnSc3
určeny	určit	k5eAaPmNgFnP
podle	podle	k7c2
zvláštních	zvláštní	k2eAgInPc2d1
předpisů	předpis	k1gInPc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
musí	muset	k5eAaImIp3nS
dbát	dbát	k5eAaImF
zájmů	zájem	k1gInPc2
obrany	obrana	k1gFnSc2
státu	stát	k1gInSc2
a	a	k8xC
zachovávat	zachovávat	k5eAaImF
tato	tento	k3xDgNnPc4
tajemství	tajemství	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Své	svůj	k3xOyFgInPc4
úkoly	úkol	k1gInPc4
jsou	být	k5eAaImIp3nP
povinny	povinen	k2eAgInPc1d1
plnit	plnit	k5eAaImF
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
organizace	organizace	k1gFnSc1
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
činnosti	činnost	k1gFnSc6
nebo	nebo	k8xC
občan	občan	k1gMnSc1
ve	v	k7c6
svých	svůj	k3xOyFgNnPc6
právech	právo	k1gNnPc6
byli	být	k5eAaImAgMnP
omezeni	omezit	k5eAaPmNgMnP
jen	jen	k9
v	v	k7c6
nezbytném	nezbytný	k2eAgInSc6d1,k2eNgInSc6d1
rozsahu	rozsah	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
vstup	vstup	k1gInSc4
do	do	k7c2
objektů	objekt	k1gInPc2
a	a	k8xC
zařízení	zařízení	k1gNnSc2
ozbrojených	ozbrojený	k2eAgFnPc2d1
sil	síla	k1gFnPc2
a	a	k8xC
sborů	sbor	k1gInPc2
platí	platit	k5eAaImIp3nP
zvláštní	zvláštní	k2eAgInPc1d1
předpisy	předpis	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
§	§	k?
43	#num#	k4
<g/>
a	a	k8xC
zákona	zákon	k1gInSc2
o	o	k7c6
SPP	SPP	kA
mají	mít	k5eAaImIp3nP
orgány	orgán	k1gInPc1
státní	státní	k2eAgFnSc2d1
památkové	památkový	k2eAgFnSc2d1
péče	péče	k1gFnSc2
přístup	přístup	k1gInSc4
k	k	k7c3
vymezeným	vymezený	k2eAgInPc3d1
údajům	údaj	k1gInPc3
základního	základní	k2eAgInSc2d1
registru	registr	k1gInSc2
obyvatel	obyvatel	k1gMnPc2
<g/>
,	,	kIx,
informačního	informační	k2eAgInSc2d1
systému	systém	k1gInSc2
evidence	evidence	k1gFnSc2
obyvatel	obyvatel	k1gMnPc2
<g/>
,	,	kIx,
informačního	informační	k2eAgInSc2d1
systému	systém	k1gInSc2
cizinců	cizinec	k1gMnPc2
<g/>
,	,	kIx,
.	.	kIx.
</s>
<s>
NPÚ	NPÚ	kA
vede	vést	k5eAaImIp3nS
Ústřední	ústřední	k2eAgInSc4d1
seznam	seznam	k1gInSc4
kulturních	kulturní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
(	(	kIx(
<g/>
ÚSKP	ÚSKP	kA
ČR	ČR	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Dále	daleko	k6eAd2
má	mít	k5eAaImIp3nS
v	v	k7c6
přímé	přímý	k2eAgFnSc6d1
správě	správa	k1gFnSc6
soubor	soubor	k1gInSc1
kulturních	kulturní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
<g/>
,	,	kIx,
zejména	zejména	k9
státních	státní	k2eAgInPc2d1
hradů	hrad	k1gInPc2
a	a	k8xC
zámků	zámek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
správě	správa	k1gFnSc6
NPÚ	NPÚ	kA
je	být	k5eAaImIp3nS
asi	asi	k9
stovka	stovka	k1gFnSc1
státních	státní	k2eAgInPc2d1
památkových	památkový	k2eAgInPc2d1
objektů	objekt	k1gInPc2
<g/>
,	,	kIx,
převážně	převážně	k6eAd1
národních	národní	k2eAgFnPc2d1
kulturních	kulturní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
hradů	hrad	k1gInPc2
<g/>
,	,	kIx,
zámků	zámek	k1gInPc2
<g/>
,	,	kIx,
klášterů	klášter	k1gInPc2
<g/>
,	,	kIx,
kostelů	kostel	k1gInPc2
<g/>
,	,	kIx,
souborů	soubor	k1gInPc2
lidové	lidový	k2eAgFnSc2d1
architektury	architektura	k1gFnSc2
<g/>
,	,	kIx,
zahrad	zahrada	k1gFnPc2
<g/>
,	,	kIx,
parků	park	k1gInPc2
a	a	k8xC
technických	technický	k2eAgFnPc2d1
památek	památka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
je	být	k5eAaImIp3nS
zpřístupněná	zpřístupněný	k2eAgFnSc1d1
a	a	k8xC
nabízí	nabízet	k5eAaImIp3nS
návštěvníkům	návštěvník	k1gMnPc3
možnost	možnost	k1gFnSc4
prohlídky	prohlídka	k1gFnPc1
v	v	k7c6
rámci	rámec	k1gInSc6
jednoho	jeden	k4xCgNnSc2
a	a	k8xC
více	hodně	k6eAd2
návštěvnických	návštěvnický	k2eAgInPc2d1
okruhů	okruh	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krajská	krajský	k2eAgFnSc1d1
střediska	středisko	k1gNnSc2
státní	státní	k2eAgFnSc2d1
památkové	památkový	k2eAgFnSc2d1
péče	péče	k1gFnSc2
krátce	krátce	k6eAd1
spravovala	spravovat	k5eAaImAgFnS
zpřístupněné	zpřístupněný	k2eAgInPc4d1
státní	státní	k2eAgInPc4d1
hrady	hrad	k1gInPc4
a	a	k8xC
zámky	zámek	k1gInPc4
po	po	k7c6
roce	rok	k1gInSc6
1960	#num#	k4
<g/>
,	,	kIx,
pak	pak	k6eAd1
jich	on	k3xPp3gMnPc2
většinu	většina	k1gFnSc4
předala	předat	k5eAaPmAgFnS
do	do	k7c2
správy	správa	k1gFnSc2
okresních	okresní	k2eAgInPc2d1
národních	národní	k2eAgInPc2d1
výborů	výbor	k1gInPc2
a	a	k8xC
nakonec	nakonec	k6eAd1
je	být	k5eAaImIp3nS
po	po	k7c6
r.	r.	kA
1968	#num#	k4
postupně	postupně	k6eAd1
přejímala	přejímat	k5eAaImAgFnS
zpět	zpět	k6eAd1
do	do	k7c2
své	svůj	k3xOyFgFnSc2
správy	správa	k1gFnSc2
<g/>
,	,	kIx,
poslední	poslední	k2eAgNnSc1d1
nedlouho	nedlouho	k1gNnSc1
před	před	k7c7
r.	r.	kA
1989	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Seznam	seznam	k1gInSc4
památkových	památkový	k2eAgInPc2d1
objektů	objekt	k1gInPc2
ve	v	k7c6
správě	správa	k1gFnSc6
Národního	národní	k2eAgInSc2d1
památkového	památkový	k2eAgInSc2d1
ústavu	ústav	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Pokud	pokud	k8xS
je	být	k5eAaImIp3nS
kulturní	kulturní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
státním	státní	k2eAgInSc7d1
majetkem	majetek	k1gInSc7
<g/>
,	,	kIx,
práva	právo	k1gNnPc4
a	a	k8xC
povinnosti	povinnost	k1gFnPc4
stanovené	stanovený	k2eAgFnPc4d1
zákonem	zákon	k1gInSc7
vlastníku	vlastník	k1gMnSc6
památky	památka	k1gFnSc2
má	mít	k5eAaImIp3nS
podle	podle	k7c2
§	§	k?
43	#num#	k4
zákona	zákon	k1gInSc2
o	o	k7c6
státní	státní	k2eAgFnSc6d1
památkové	památkový	k2eAgFnSc6d1
péči	péče	k1gFnSc6
ta	ten	k3xDgFnSc1
státní	státní	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
má	mít	k5eAaImIp3nS
památku	památka	k1gFnSc4
ve	v	k7c6
správě	správa	k1gFnSc6
<g/>
,	,	kIx,
popřípadě	popřípadě	k6eAd1
nestátní	státní	k2eNgFnPc4d1
organizace	organizace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yRgFnPc4,k3yQgFnPc4,k3yIgFnPc4
byla	být	k5eAaImAgFnS
památka	památka	k1gFnSc1
svěřena	svěřit	k5eAaPmNgFnS
do	do	k7c2
trvalého	trvalý	k2eAgNnSc2d1
užívání	užívání	k1gNnSc2
<g/>
,	,	kIx,
popřípadě	popřípadě	k6eAd1
organizace	organizace	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
má	mít	k5eAaImIp3nS
užívací	užívací	k2eAgNnSc4d1
právo	právo	k1gNnSc4
v	v	k7c6
družstevním	družstevní	k2eAgMnSc6d1
nebo	nebo	k8xC
náhradním	náhradní	k2eAgNnSc6d1
užívání	užívání	k1gNnSc6
k	k	k7c3
zajištění	zajištění	k1gNnSc3
výroby	výroba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
NPÚ	NPÚ	kA
udílí	udílet	k5eAaImIp3nS
cenu	cena	k1gFnSc4
Patrimonium	patrimonium	k1gNnSc1
pro	pro	k7c4
futuro	futura	k1gFnSc5
s	s	k7c7
cílem	cíl	k1gInSc7
společensky	společensky	k6eAd1
ocenit	ocenit	k5eAaPmF
a	a	k8xC
podpořit	podpořit	k5eAaPmF
přínosy	přínos	k1gInPc4
pro	pro	k7c4
záchranu	záchrana	k1gFnSc4
<g/>
,	,	kIx,
obnovu	obnova	k1gFnSc4
<g/>
,	,	kIx,
zkoumání	zkoumání	k1gNnSc4
a	a	k8xC
prezentaci	prezentace	k1gFnSc4
hodnot	hodnota	k1gFnPc2
kulturního	kulturní	k2eAgNnSc2d1
dědictví	dědictví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Publikační	publikační	k2eAgFnSc1d1
činnost	činnost	k1gFnSc1
</s>
<s>
Aktualizovaný	aktualizovaný	k2eAgInSc1d1
přehled	přehled	k1gInSc1
publikační	publikační	k2eAgFnSc2d1
činnosti	činnost	k1gFnSc2
je	být	k5eAaImIp3nS
dostupný	dostupný	k2eAgInSc1d1
na	na	k7c6
webu	web	k1gInSc6
instituce	instituce	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
NPÚ	NPÚ	kA
vydává	vydávat	k5eAaImIp3nS,k5eAaPmIp3nS
podrobné	podrobný	k2eAgFnPc4d1
Výroční	výroční	k2eAgFnPc4d1
zprávy	zpráva	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základní	základní	k2eAgFnSc1d1
odborné	odborný	k2eAgNnSc4d1
periodikum	periodikum	k1gNnSc4
představují	představovat	k5eAaImIp3nP
Zprávy	zpráva	k1gFnPc1
památkové	památkový	k2eAgFnSc2d1
péče	péče	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
NPÚ	NPÚ	kA
vydává	vydávat	k5eAaImIp3nS,k5eAaPmIp3nS
oborové	oborový	k2eAgFnPc4d1
metodiky	metodika	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Odborná	odborný	k2eAgNnPc1d1
pracoviště	pracoviště	k1gNnPc1
vydávají	vydávat	k5eAaImIp3nP,k5eAaPmIp3nP
vlastní	vlastní	k2eAgInPc4d1
časopisy	časopis	k1gInPc4
či	či	k8xC
sborníky	sborník	k1gInPc4
a	a	k8xC
publikace	publikace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Pražské	pražský	k2eAgNnSc1d1
územní	územní	k2eAgNnSc1d1
odborné	odborný	k2eAgNnSc1d1
pracoviště	pracoviště	k1gNnSc1
vydává	vydávat	k5eAaImIp3nS,k5eAaPmIp3nS
časopis	časopis	k1gInSc1
(	(	kIx(
<g/>
do	do	k7c2
roku	rok	k1gInSc2
2008	#num#	k4
sborník	sborník	k1gInSc1
<g/>
)	)	kIx)
Staletá	staletý	k2eAgFnSc1d1
Praha	Praha	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Územní	územní	k2eAgNnSc1d1
odborné	odborný	k2eAgNnSc1d1
pracoviště	pracoviště	k1gNnSc1
středních	střední	k2eAgFnPc2d1
Čech	Čechy	k1gFnPc2
v	v	k7c6
Praze	Praha	k1gFnSc6
vydává	vydávat	k5eAaImIp3nS,k5eAaPmIp3nS
časopisy	časopis	k1gInPc4
Památky	památka	k1gFnSc2
středních	střední	k2eAgFnPc2d1
Čech	Čechy	k1gFnPc2
a	a	k8xC
Průzkumy	průzkum	k1gInPc4
památek	památka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Územní	územní	k2eAgNnSc1d1
odborné	odborný	k2eAgNnSc1d1
pracoviště	pracoviště	k1gNnSc1
v	v	k7c6
Českých	český	k2eAgInPc6d1
Budějovicích	Budějovice	k1gInPc6
vydává	vydávat	k5eAaImIp3nS,k5eAaPmIp3nS
sborník	sborník	k1gInSc1
Památky	památka	k1gFnSc2
jižních	jižní	k2eAgFnPc2d1
Čech	Čechy	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Územní	územní	k2eAgNnSc1d1
odborné	odborný	k2eAgNnSc1d1
pracoviště	pracoviště	k1gNnSc1
v	v	k7c6
Plzni	Plzeň	k1gFnSc6
vydává	vydávat	k5eAaPmIp3nS,k5eAaImIp3nS
periodikum	periodikum	k1gNnSc1
Památky	památka	k1gFnSc2
západních	západní	k2eAgFnPc2d1
Čech	Čechy	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Územní	územní	k2eAgNnSc1d1
odborné	odborný	k2eAgNnSc1d1
pracoviště	pracoviště	k1gNnSc1
v	v	k7c6
Ústí	ústí	k1gNnSc6
nad	nad	k7c7
Labem	Labe	k1gNnSc7
vydává	vydávat	k5eAaImIp3nS,k5eAaPmIp3nS
periodikum	periodikum	k1gNnSc1
Monumentorum	Monumentorum	k1gNnSc4
Custos	Custos	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Územní	územní	k2eAgNnSc1d1
odborné	odborný	k2eAgNnSc1d1
pracoviště	pracoviště	k1gNnSc1
v	v	k7c6
Liberci	Liberec	k1gInSc6
spoluvydává	spoluvydávat	k5eAaImIp3nS
časopis	časopis	k1gInSc1
Fontes	Fontesa	k1gFnPc2
Nissae	Nissae	k1gFnPc2
/	/	kIx~
Prameny	pramen	k1gInPc1
Nisy	Nisa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Územní	územní	k2eAgNnSc1d1
odborné	odborný	k2eAgNnSc1d1
pracoviště	pracoviště	k1gNnSc1
v	v	k7c6
Josefově	Josefov	k1gInSc6
vydává	vydávat	k5eAaPmIp3nS,k5eAaImIp3nS
sborník	sborník	k1gInSc1
Monumenta	Monumento	k1gNnSc2
Vivent	Vivent	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Územní	územní	k2eAgNnSc1d1
odborné	odborný	k2eAgNnSc1d1
pracoviště	pracoviště	k1gNnSc1
v	v	k7c6
Pardubicích	Pardubice	k1gInPc6
vydává	vydávat	k5eAaImIp3nS,k5eAaPmIp3nS
jednou	jednou	k6eAd1
ročně	ročně	k6eAd1
Sborník	sborník	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Územní	územní	k2eAgNnSc1d1
odborné	odborný	k2eAgNnSc1d1
pracoviště	pracoviště	k1gNnSc1
v	v	k7c6
Telči	Telč	k1gFnSc6
vydává	vydávat	k5eAaImIp3nS,k5eAaPmIp3nS
sborník	sborník	k1gInSc1
Památky	památka	k1gFnSc2
Vysočiny	vysočina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Územní	územní	k2eAgNnSc1d1
odborné	odborný	k2eAgNnSc1d1
pracoviště	pracoviště	k1gNnSc1
v	v	k7c6
Brně	Brno	k1gNnSc6
vydává	vydávat	k5eAaImIp3nS,k5eAaPmIp3nS
ročenku	ročenka	k1gFnSc4
Památková	památkový	k2eAgFnSc1d1
péče	péče	k1gFnSc1
na	na	k7c6
Moravě	Morava	k1gFnSc6
–	–	k?
Monumentorum	Monumentorum	k1gNnSc1
Moraviae	Moraviae	k1gFnSc1
tutela	tutela	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seznam	seznam	k1gInSc1
publikací	publikace	k1gFnPc2
je	být	k5eAaImIp3nS
dostupný	dostupný	k2eAgInSc1d1
on-line	on-lin	k1gInSc5
<g/>
.	.	kIx.
</s>
<s>
Územní	územní	k2eAgNnSc1d1
odborné	odborný	k2eAgNnSc1d1
pracoviště	pracoviště	k1gNnSc1
v	v	k7c6
Kroměříži	Kroměříž	k1gFnSc6
vydává	vydávat	k5eAaPmIp3nS,k5eAaImIp3nS
sborník	sborník	k1gInSc1
Ingredere	Ingreder	k1gInSc5
hospes	hospes	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Územní	územní	k2eAgNnSc1d1
odborné	odborný	k2eAgNnSc1d1
pracoviště	pracoviště	k1gNnSc1
v	v	k7c6
Olomouci	Olomouc	k1gFnSc6
vydává	vydávat	k5eAaPmIp3nS,k5eAaImIp3nS
Sborník	sborník	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Územní	územní	k2eAgNnSc1d1
odborné	odborný	k2eAgNnSc1d1
pracoviště	pracoviště	k1gNnSc1
v	v	k7c6
Ostravě	Ostrava	k1gFnSc6
vydává	vydávat	k5eAaPmIp3nS,k5eAaImIp3nS
Sborník	sborník	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Publikace	publikace	k1gFnSc1
NPÚ-ÚOP	NPÚ-ÚOP	k1gFnSc2
v	v	k7c6
Liberci	Liberec	k1gInSc6
dostupné	dostupný	k2eAgNnSc1d1
on-line	on-lin	k1gInSc5
<g/>
.	.	kIx.
</s>
<s>
Především	především	k9
pro	pro	k7c4
obecnou	obecný	k2eAgFnSc4d1
veřejnost	veřejnost	k1gFnSc4
jsou	být	k5eAaImIp3nP
určeny	určen	k2eAgMnPc4d1
četné	četný	k2eAgMnPc4d1
průvodce	průvodce	k1gMnPc4
po	po	k7c6
zpřístupněných	zpřístupněný	k2eAgFnPc6d1
památkách	památka	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
publikační	publikační	k2eAgFnSc3d1
činnosti	činnost	k1gFnSc3
lze	lze	k6eAd1
přiřadit	přiřadit	k5eAaPmF
rozsáhlé	rozsáhlý	k2eAgInPc4d1
elektronické	elektronický	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
informací	informace	k1gFnPc2
pro	pro	k7c4
odbornou	odborný	k2eAgFnSc4d1
i	i	k8xC
širokou	široký	k2eAgFnSc4d1
kulturní	kulturní	k2eAgFnSc4d1
veřejnost	veřejnost	k1gFnSc4
(	(	kIx(
<g/>
webové	webový	k2eAgFnPc4d1
stránky	stránka	k1gFnPc4
<g/>
,	,	kIx,
databáze	databáze	k1gFnPc4
<g/>
,	,	kIx,
působení	působení	k1gNnSc4
na	na	k7c6
sociálních	sociální	k2eAgFnPc6d1
sítích	síť	k1gFnPc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Pracoviště	pracoviště	k1gNnSc1
a	a	k8xC
organizační	organizační	k2eAgNnSc1d1
uspořádání	uspořádání	k1gNnSc1
</s>
<s>
Před	před	k7c7
vznikem	vznik	k1gInSc7
Národního	národní	k2eAgInSc2d1
památkového	památkový	k2eAgInSc2d1
ústavu	ústav	k1gInSc2
existovalo	existovat	k5eAaImAgNnS
kromě	kromě	k7c2
ústředního	ústřední	k2eAgInSc2d1
Státního	státní	k2eAgInSc2d1
ústavu	ústav	k1gInSc2
památkové	památkový	k2eAgFnSc2d1
péče	péče	k1gFnSc2
9	#num#	k4
krajských	krajský	k2eAgInPc2d1
státních	státní	k2eAgInPc2d1
památkových	památkový	k2eAgInPc2d1
ústavů	ústav	k1gInPc2
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Státní	státní	k2eAgInSc1d1
památkový	památkový	k2eAgInSc1d1
ústav	ústav	k1gInSc1
v	v	k7c6
hlavním	hlavní	k2eAgNnSc6d1
městě	město	k1gNnSc6
Praze	Praha	k1gFnSc6
</s>
<s>
Státní	státní	k2eAgInSc1d1
památkový	památkový	k2eAgInSc1d1
ústav	ústav	k1gInSc1
středních	střední	k2eAgFnPc2d1
Čech	Čechy	k1gFnPc2
v	v	k7c6
Praze	Praha	k1gFnSc6
</s>
<s>
Státní	státní	k2eAgInSc1d1
památkový	památkový	k2eAgInSc1d1
ústav	ústav	k1gInSc1
v	v	k7c6
Českých	český	k2eAgInPc6d1
Budějovicích	Budějovice	k1gInPc6
</s>
<s>
Státní	státní	k2eAgInSc1d1
památkový	památkový	k2eAgInSc1d1
ústav	ústav	k1gInSc1
v	v	k7c6
Plzni	Plzeň	k1gFnSc6
</s>
<s>
Státní	státní	k2eAgInSc1d1
památkový	památkový	k2eAgInSc1d1
ústav	ústav	k1gInSc1
v	v	k7c6
Ústí	ústí	k1gNnSc6
nad	nad	k7c7
Labem	Labe	k1gNnSc7
</s>
<s>
Státní	státní	k2eAgInSc1d1
památkový	památkový	k2eAgInSc1d1
ústav	ústav	k1gInSc1
v	v	k7c6
Pardubicích	Pardubice	k1gInPc6
</s>
<s>
Státní	státní	k2eAgInSc1d1
památkový	památkový	k2eAgInSc1d1
ústav	ústav	k1gInSc1
v	v	k7c6
Brně	Brno	k1gNnSc6
</s>
<s>
Státní	státní	k2eAgInSc1d1
památkový	památkový	k2eAgInSc1d1
ústav	ústav	k1gInSc1
v	v	k7c6
Ostravě	Ostrava	k1gFnSc6
</s>
<s>
Státní	státní	k2eAgInSc1d1
památkový	památkový	k2eAgInSc1d1
ústav	ústav	k1gInSc1
v	v	k7c6
Olomouci	Olomouc	k1gFnSc6
</s>
<s>
Ve	v	k7c6
Východočeském	východočeský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
tedy	tedy	k8xC
sídlil	sídlit	k5eAaImAgInS
mimo	mimo	k7c4
krajské	krajský	k2eAgNnSc4d1
město	město	k1gNnSc4
a	a	k8xC
pro	pro	k7c4
Severomoravský	severomoravský	k2eAgInSc4d1
a	a	k8xC
Jihomoravský	jihomoravský	k2eAgInSc4d1
kraj	kraj	k1gInSc4
existovaly	existovat	k5eAaImAgInP
tři	tři	k4xCgInPc1
státní	státní	k2eAgInPc1d1
památkové	památkový	k2eAgInPc1d1
ústavy	ústav	k1gInPc1
<g/>
,	,	kIx,
krom	krom	k7c2
krajských	krajský	k2eAgNnPc2d1
měst	město	k1gNnPc2
navíc	navíc	k6eAd1
ústav	ústav	k1gInSc4
v	v	k7c6
Olomouci	Olomouc	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Při	při	k7c6
vzniku	vznik	k1gInSc6
NPÚ	NPÚ	kA
v	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
byl	být	k5eAaImAgInS
Státní	státní	k2eAgInSc1d1
ústav	ústav	k1gInSc1
památkové	památkový	k2eAgFnSc2d1
péče	péče	k1gFnSc2
transformován	transformován	k2eAgInSc1d1
v	v	k7c6
ústřední	ústřední	k2eAgNnSc4d1
pracoviště	pracoviště	k1gNnSc4
NPÚ	NPÚ	kA
a	a	k8xC
dosavadní	dosavadní	k2eAgFnSc2d1
krajské	krajský	k2eAgFnSc2d1
státní	státní	k2eAgFnSc2d1
památkové	památkový	k2eAgFnSc2d1
ústavy	ústava	k1gFnSc2
v	v	k7c4
územní	územní	k2eAgNnPc4d1
odborná	odborný	k2eAgNnPc4d1
pracoviště	pracoviště	k1gNnPc4
NPÚ	NPÚ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odborná	odborný	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
státní	státní	k2eAgFnSc2d1
památkové	památkový	k2eAgFnSc2d1
péče	péče	k1gFnSc2
si	se	k3xPyFc3
podle	podle	k7c2
§	§	k?
32	#num#	k4
odst	odsta	k1gFnPc2
<g/>
.	.	kIx.
3	#num#	k4
zákona	zákon	k1gInSc2
o	o	k7c6
státní	státní	k2eAgFnSc6d1
památkové	památkový	k2eAgFnSc6d1
péči	péče	k1gFnSc6
v	v	k7c6
aktuálním	aktuální	k2eAgNnSc6d1
znění	znění	k1gNnSc6
vytváří	vytvářit	k5eAaPmIp3nS,k5eAaImIp3nS
pro	pro	k7c4
výkon	výkon	k1gInSc4
státní	státní	k2eAgFnSc2d1
památkové	památkový	k2eAgFnSc2d1
péče	péče	k1gFnSc2
se	se	k3xPyFc4
souhlasem	souhlas	k1gInSc7
ministerstva	ministerstvo	k1gNnSc2
kultury	kultura	k1gFnSc2
a	a	k8xC
po	po	k7c6
projednání	projednání	k1gNnSc6
s	s	k7c7
krajem	kraj	k1gInSc7
krajská	krajský	k2eAgFnSc1d1
<g/>
,	,	kIx,
popřípadě	popřípadě	k6eAd1
i	i	k9
další	další	k2eAgFnPc4d1
územní	územní	k2eAgFnPc4d1
odborná	odborný	k2eAgNnPc4d1
pracoviště	pracoviště	k1gNnPc4
(	(	kIx(
<g/>
střediska	středisko	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Navíc	navíc	k6eAd1
byly	být	k5eAaImAgFnP
ještě	ještě	k9
při	při	k7c6
zřízení	zřízení	k1gNnSc6
NPÚ	NPÚ	kA
v	v	k7c4
územní	územní	k2eAgFnPc4d1
odborná	odborný	k2eAgNnPc4d1
pracoviště	pracoviště	k1gNnPc4
přeměněny	přeměněn	k2eAgFnPc4d1
správy	správa	k1gFnPc4
státních	státní	k2eAgInPc2d1
zámků	zámek	k1gInPc2
Sychrov	Sychrov	k1gInSc1
a	a	k8xC
Bouzov	Bouzov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pracoviště	pracoviště	k1gNnPc1
Bouzov	Bouzov	k1gInSc4
bylo	být	k5eAaImAgNnS
v	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
sloučeno	sloučit	k5eAaPmNgNnS
do	do	k7c2
olomouckého	olomoucký	k2eAgNnSc2d1
pracoviště	pracoviště	k1gNnSc2
a	a	k8xC
pracoviště	pracoviště	k1gNnSc2
na	na	k7c6
Sychrově	Sychrův	k2eAgFnSc6d1
bylo	být	k5eAaImAgNnS
v	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
zahrnuto	zahrnout	k5eAaPmNgNnS
do	do	k7c2
nového	nový	k2eAgNnSc2d1
pracoviště	pracoviště	k1gNnSc2
v	v	k7c6
Liberci	Liberec	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
červenci	červenec	k1gInSc3
2006	#num#	k4
(	(	kIx(
<g/>
s	s	k7c7
plným	plný	k2eAgNnSc7d1
převzetím	převzetí	k1gNnSc7
činnosti	činnost	k1gFnSc2
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2007	#num#	k4
<g/>
)	)	kIx)
přibylo	přibýt	k5eAaPmAgNnS
územní	územní	k2eAgNnSc1d1
odborné	odborný	k2eAgNnSc1d1
pracoviště	pracoviště	k1gNnSc1
v	v	k7c6
Lokti	loket	k1gInSc6
pro	pro	k7c4
Karlovarský	karlovarský	k2eAgInSc4d1
kraj	kraj	k1gInSc4
<g/>
,	,	kIx,
k	k	k7c3
2	#num#	k4
<g/>
.	.	kIx.
únoru	únor	k1gInSc6
2006	#num#	k4
územní	územní	k2eAgFnSc1d1
odborné	odborný	k2eAgNnSc4d1
pracoviště	pracoviště	k1gNnSc4
v	v	k7c6
Liberci	Liberec	k1gInSc6
pro	pro	k7c4
Liberecký	liberecký	k2eAgInSc4d1
kraj	kraj	k1gInSc4
(	(	kIx(
<g/>
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
nahradilo	nahradit	k5eAaPmAgNnS
pracoviště	pracoviště	k1gNnSc4
na	na	k7c6
Sychrově	Sychrův	k2eAgMnSc6d1
<g/>
)	)	kIx)
a	a	k8xC
v	v	k7c6
Kroměříži	Kroměříž	k1gFnSc6
pro	pro	k7c4
Zlínský	zlínský	k2eAgInSc4d1
kraj	kraj	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2008	#num#	k4
působí	působit	k5eAaImIp3nS
územní	územní	k2eAgNnSc4d1
odborné	odborný	k2eAgNnSc4d1
pracoviště	pracoviště	k1gNnSc4
v	v	k7c6
Josefově	Josefov	k1gInSc6
pro	pro	k7c4
Královéhradecký	královéhradecký	k2eAgInSc4d1
kraj	kraj	k1gInSc4
a	a	k8xC
územní	územní	k2eAgNnSc4d1
odborné	odborný	k2eAgNnSc4d1
pracoviště	pracoviště	k1gNnSc4
v	v	k7c6
Telči	Telč	k1gFnSc6
pro	pro	k7c4
kraj	kraj	k1gInSc4
Vysočina	vysočina	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ke	k	k7c3
dni	den	k1gInSc3
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2013	#num#	k4
byla	být	k5eAaImAgFnS
vytvořena	vytvořit	k5eAaPmNgFnS
4	#num#	k4
nová	nový	k2eAgNnPc1d1
pracoviště	pracoviště	k1gNnPc1
<g/>
,	,	kIx,
územní	územní	k2eAgFnSc2d1
památkové	památkový	k2eAgFnSc2d1
správy	správa	k1gFnSc2
<g/>
,	,	kIx,
do	do	k7c2
nichž	jenž	k3xRgFnPc2
byla	být	k5eAaImAgFnS
vyčleněna	vyčleněn	k2eAgFnSc1d1
správa	správa	k1gFnSc1
státních	státní	k2eAgFnPc2d1
kulturních	kulturní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současně	současně	k6eAd1
bylo	být	k5eAaImAgNnS
ústřední	ústřední	k2eAgNnSc1d1
pracoviště	pracoviště	k1gNnSc1
přejmenováno	přejmenovat	k5eAaPmNgNnS
na	na	k7c4
generální	generální	k2eAgNnSc4d1
ředitelství	ředitelství	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
NPÚ	NPÚ	kA
se	se	k3xPyFc4
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2013	#num#	k4
organizačně	organizačně	k6eAd1
člení	členit	k5eAaImIp3nS
na	na	k7c4
generální	generální	k2eAgNnSc4d1
ředitelství	ředitelství	k1gNnSc4
v	v	k7c6
Praze	Praha	k1gFnSc6
(	(	kIx(
<g/>
GnŘ	GnŘ	k1gFnPc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
územní	územní	k2eAgFnPc4d1
odborná	odborný	k2eAgNnPc4d1
pracoviště	pracoviště	k1gNnPc4
(	(	kIx(
<g/>
ÚOP	ÚOP	kA
<g/>
)	)	kIx)
v	v	k7c6
jednotlivých	jednotlivý	k2eAgInPc6d1
krajích	kraj	k1gInPc6
a	a	k8xC
4	#num#	k4
územní	územní	k2eAgFnSc2d1
památkové	památkový	k2eAgFnSc2d1
správy	správa	k1gFnSc2
(	(	kIx(
<g/>
ÚPS	ÚPS	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Generální	generální	k2eAgNnSc1d1
ředitelství	ředitelství	k1gNnSc1
řídí	řídit	k5eAaImIp3nS
celkovou	celkový	k2eAgFnSc4d1
činnost	činnost	k1gFnSc4
NPÚ	NPÚ	kA
a	a	k8xC
v	v	k7c6
rozsahu	rozsah	k1gInSc6
vymezeném	vymezený	k2eAgInSc6d1
vnitřními	vnitřní	k2eAgInPc7d1
předpisy	předpis	k1gInPc7
přímo	přímo	k6eAd1
zajišťuje	zajišťovat	k5eAaImIp3nS
úkoly	úkol	k1gInPc4
státní	státní	k2eAgFnSc2d1
památkové	památkový	k2eAgFnSc2d1
péče	péče	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Územní	územní	k2eAgFnPc4d1
odborná	odborný	k2eAgNnPc4d1
pracoviště	pracoviště	k1gNnPc4
zajišťují	zajišťovat	k5eAaImIp3nP
úkoly	úkol	k1gInPc1
státní	státní	k2eAgFnSc2d1
památkové	památkový	k2eAgFnSc2d1
péče	péče	k1gFnSc2
v	v	k7c6
rozsahu	rozsah	k1gInSc6
své	svůj	k3xOyFgFnSc2
územní	územní	k2eAgFnSc2d1
působnosti	působnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Územní	územní	k2eAgFnSc2d1
památkové	památkový	k2eAgFnSc2d1
správy	správa	k1gFnSc2
spravují	spravovat	k5eAaImIp3nP
zpřístupněné	zpřístupněný	k2eAgFnPc1d1
kulturní	kulturní	k2eAgFnPc1d1
památky	památka	k1gFnPc1
v	v	k7c6
přímé	přímý	k2eAgFnSc6d1
správě	správa	k1gFnSc6
NPÚ	NPÚ	kA
(	(	kIx(
<g/>
hrady	hrad	k1gInPc1
<g/>
,	,	kIx,
zámky	zámek	k1gInPc1
<g/>
,	,	kIx,
kláštery	klášter	k1gInPc1
<g/>
,	,	kIx,
kostely	kostel	k1gInPc1
<g/>
,	,	kIx,
industriální	industriální	k2eAgFnPc1d1
památky	památka	k1gFnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
lednu	leden	k1gInSc3
2019	#num#	k4
byly	být	k5eAaImAgFnP
do	do	k7c2
nově	nově	k6eAd1
zřízeného	zřízený	k2eAgNnSc2d1
Národního	národní	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
v	v	k7c6
přírodě	příroda	k1gFnSc6
převedeny	převést	k5eAaPmNgInP
tři	tři	k4xCgInPc1
skanzeny	skanzen	k1gInPc1
(	(	kIx(
<g/>
Příkazy	příkaz	k1gInPc1
<g/>
,	,	kIx,
Vysočina	vysočina	k1gFnSc1
<g/>
,	,	kIx,
Zubrnice	Zubrnice	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yRgFnPc4,k3yQgFnPc4
dosud	dosud	k6eAd1
spravoval	spravovat	k5eAaImAgMnS
NPÚ	NPÚ	kA
<g/>
.	.	kIx.
</s>
<s>
Pracoviště	pracoviště	k1gNnSc1
a	a	k8xC
jejich	jejich	k3xOp3gNnSc4
umístění	umístění	k1gNnSc4
</s>
<s>
Petrášův	Petrášův	k2eAgInSc1d1
palác	palác	k1gInSc1
je	být	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
2001	#num#	k4
sídlem	sídlo	k1gNnSc7
územního	územní	k2eAgNnSc2d1
odborného	odborný	k2eAgNnSc2d1
pracoviště	pracoviště	k1gNnSc2
v	v	k7c6
Olomouci	Olomouc	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
NPÚ	NPÚ	kA
<g/>
,	,	kIx,
generální	generální	k2eAgNnSc1d1
ředitelství	ředitelství	k1gNnSc1
zajišťuje	zajišťovat	k5eAaImIp3nS
činnost	činnost	k1gFnSc4
NPÚ	NPÚ	kA
jako	jako	k8xC,k8xS
celku	celek	k1gInSc2
(	(	kIx(
<g/>
sídlí	sídlet	k5eAaImIp3nS
v	v	k7c6
Ledebourském	Ledebourský	k2eAgInSc6d1
paláci	palác	k1gInSc6
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
)	)	kIx)
</s>
<s>
Část	část	k1gFnSc1
odborných	odborný	k2eAgNnPc2d1
pracovišť	pracoviště	k1gNnPc2
a	a	k8xC
edukační	edukační	k2eAgInSc1d1
odbor	odbor	k1gInSc1
generálního	generální	k2eAgNnSc2d1
ředitelství	ředitelství	k1gNnSc2
se	se	k3xPyFc4
na	na	k7c4
podzim	podzim	k1gInSc4
2017	#num#	k4
přesune	přesunout	k5eAaPmIp3nS
do	do	k7c2
domu	dům	k1gInSc2
U	u	k7c2
Voříkovských	Voříkovský	k2eAgFnPc2d1
v	v	k7c6
Liliové	liliový	k2eAgFnSc6d1
5	#num#	k4
v	v	k7c6
Praze	Praha	k1gFnSc6
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Územní	územní	k2eAgNnPc1d1
odborná	odborný	k2eAgNnPc1d1
pracoviště	pracoviště	k1gNnPc1
(	(	kIx(
<g/>
+	+	kIx~
<g/>
sídla	sídlo	k1gNnSc2
<g/>
)	)	kIx)
<g/>
:	:	kIx,
</s>
<s>
NPÚ	NPÚ	kA
územní	územní	k2eAgFnSc1d1
odborné	odborný	k2eAgNnSc4d1
pracoviště	pracoviště	k1gNnSc4
v	v	k7c6
Praze	Praha	k1gFnSc6
–	–	k?
zajišťuje	zajišťovat	k5eAaImIp3nS
činnost	činnost	k1gFnSc4
NPÚ	NPÚ	kA
v	v	k7c6
hlavním	hlavní	k2eAgNnSc6d1
městě	město	k1gNnSc6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
(	(	kIx(
<g/>
bývalý	bývalý	k2eAgInSc1d1
Purkrabský	purkrabský	k2eAgInSc1d1
pivovar	pivovar	k1gInSc1
v	v	k7c6
ulici	ulice	k1gFnSc6
Na	na	k7c6
Perštýně	Perštýna	k1gFnSc6
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
)	)	kIx)
</s>
<s>
NPÚ	NPÚ	kA
územní	územní	k2eAgFnSc1d1
odborné	odborný	k2eAgNnSc4d1
pracoviště	pracoviště	k1gNnSc4
středních	střední	k2eAgFnPc2d1
Čech	Čechy	k1gFnPc2
v	v	k7c6
Praze	Praha	k1gFnSc6
–	–	k?
zajišťuje	zajišťovat	k5eAaImIp3nS
činnost	činnost	k1gFnSc4
NPÚ	NPÚ	kA
ve	v	k7c6
Středočeském	středočeský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
NPÚ	NPÚ	kA
územní	územní	k2eAgFnSc1d1
odborné	odborný	k2eAgNnSc4d1
pracoviště	pracoviště	k1gNnSc4
v	v	k7c6
Českých	český	k2eAgInPc6d1
Budějovicích	Budějovice	k1gInPc6
–	–	k?
zajišťuje	zajišťovat	k5eAaImIp3nS
činnost	činnost	k1gFnSc4
NPÚ	NPÚ	kA
v	v	k7c6
Jihočeském	jihočeský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Dům	dům	k1gInSc1
U	u	k7c2
Ferusů	Ferus	k1gMnPc2
<g/>
)	)	kIx)
</s>
<s>
NPÚ	NPÚ	kA
územní	územní	k2eAgFnSc1d1
odborné	odborný	k2eAgNnSc4d1
pracoviště	pracoviště	k1gNnSc4
v	v	k7c6
Plzni	Plzeň	k1gFnSc6
–	–	k?
zajišťuje	zajišťovat	k5eAaImIp3nS
činnost	činnost	k1gFnSc4
NPÚ	NPÚ	kA
v	v	k7c6
Plzeňském	plzeňský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Dům	dům	k1gInSc1
U	u	k7c2
Zlatého	zlatý	k2eAgNnSc2d1
slunce	slunce	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
NPÚ	NPÚ	kA
územní	územní	k2eAgFnSc1d1
odborné	odborný	k2eAgNnSc4d1
pracoviště	pracoviště	k1gNnSc4
v	v	k7c6
Ústí	ústí	k1gNnSc6
nad	nad	k7c7
Labem	Labe	k1gNnSc7
–	–	k?
zajišťuje	zajišťovat	k5eAaImIp3nS
činnost	činnost	k1gFnSc4
NPÚ	NPÚ	kA
v	v	k7c6
Ústeckém	ústecký	k2eAgInSc6d1
kraji	kraj	k1gInSc6
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Zámek	zámek	k1gInSc4
Krásné	krásný	k2eAgNnSc1d1
Březno	Březno	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
NPÚ	NPÚ	kA
územní	územní	k2eAgFnSc1d1
odborné	odborný	k2eAgNnSc4d1
pracoviště	pracoviště	k1gNnSc4
v	v	k7c6
Liberci	Liberec	k1gInSc6
–	–	k?
zajišťuje	zajišťovat	k5eAaImIp3nS
činnost	činnost	k1gFnSc4
NPÚ	NPÚ	kA
v	v	k7c6
Libereckém	liberecký	k2eAgInSc6d1
kraji	kraj	k1gInSc6
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Vila	vít	k5eAaImAgFnS
Franze	Franze	k1gFnSc1
Bognera	Bogner	k1gMnSc2
<g/>
)	)	kIx)
</s>
<s>
NPÚ	NPÚ	kA
územní	územní	k2eAgFnSc1d1
odborné	odborný	k2eAgNnSc4d1
pracoviště	pracoviště	k1gNnSc4
v	v	k7c6
Pardubicích	Pardubice	k1gInPc6
–	–	k?
zajišťuje	zajišťovat	k5eAaImIp3nS
činnost	činnost	k1gFnSc4
NPÚ	NPÚ	kA
v	v	k7c6
Pardubickém	pardubický	k2eAgInSc6d1
kraji	kraj	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
NPÚ	NPÚ	kA
územní	územní	k2eAgFnSc1d1
odborné	odborný	k2eAgNnSc4d1
pracoviště	pracoviště	k1gNnSc4
v	v	k7c6
Brně	Brno	k1gNnSc6
–	–	k?
zajišťuje	zajišťovat	k5eAaImIp3nS
činnost	činnost	k1gFnSc4
NPÚ	NPÚ	kA
v	v	k7c6
Jihomoravském	jihomoravský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
NPÚ	NPÚ	kA
územní	územní	k2eAgFnSc1d1
odborné	odborný	k2eAgNnSc4d1
pracoviště	pracoviště	k1gNnSc4
v	v	k7c6
Ostravě	Ostrava	k1gFnSc6
–	–	k?
zajišťuje	zajišťovat	k5eAaImIp3nS
činnost	činnost	k1gFnSc4
NPÚ	NPÚ	kA
v	v	k7c6
Moravskoslezském	moravskoslezský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
NPÚ	NPÚ	kA
územní	územní	k2eAgFnSc1d1
odborné	odborný	k2eAgNnSc4d1
pracoviště	pracoviště	k1gNnSc4
v	v	k7c6
Olomouci	Olomouc	k1gFnSc6
–	–	k?
zajišťuje	zajišťovat	k5eAaImIp3nS
činnost	činnost	k1gFnSc4
NPÚ	NPÚ	kA
v	v	k7c6
Olomouckém	olomoucký	k2eAgInSc6d1
kraji	kraj	k1gInSc6
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Petrášův	Petrášův	k2eAgInSc1d1
palác	palác	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
NPÚ	NPÚ	kA
územní	územní	k2eAgFnSc1d1
odborné	odborný	k2eAgNnSc4d1
pracoviště	pracoviště	k1gNnSc4
v	v	k7c6
Kroměříži	Kroměříž	k1gFnSc6
–	–	k?
zajišťuje	zajišťovat	k5eAaImIp3nS
činnost	činnost	k1gFnSc4
NPÚ	NPÚ	kA
ve	v	k7c6
Zlínském	zlínský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Arcibiskupský	arcibiskupský	k2eAgInSc1d1
zámek	zámek	k1gInSc1
Kroměříž	Kroměříž	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
NPÚ	NPÚ	kA
územní	územní	k2eAgFnSc1d1
odborné	odborný	k2eAgNnSc4d1
pracoviště	pracoviště	k1gNnSc4
v	v	k7c6
Lokti	loket	k1gInSc6
–	–	k?
zajišťuje	zajišťovat	k5eAaImIp3nS
činnost	činnost	k1gFnSc4
NPÚ	NPÚ	kA
v	v	k7c6
Karlovarském	karlovarský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
NPÚ	NPÚ	kA
územní	územní	k2eAgFnSc1d1
odborné	odborný	k2eAgNnSc4d1
pracoviště	pracoviště	k1gNnSc4
v	v	k7c6
Telči	Telč	k1gFnSc6
–	–	k?
zajišťuje	zajišťovat	k5eAaImIp3nS
činnost	činnost	k1gFnSc4
NPÚ	NPÚ	kA
v	v	k7c6
kraji	kraj	k1gInSc6
Vysočina	vysočina	k1gFnSc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Lannerův	Lannerův	k2eAgInSc1d1
dům	dům	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
NPÚ	NPÚ	kA
územní	územní	k2eAgFnSc1d1
odborné	odborný	k2eAgNnSc4d1
pracoviště	pracoviště	k1gNnSc4
v	v	k7c6
Jaroměři-Josefově	Jaroměři-Josefův	k2eAgInSc6d1
–	–	k?
zajišťuje	zajišťovat	k5eAaImIp3nS
činnost	činnost	k1gFnSc4
NPÚ	NPÚ	kA
v	v	k7c6
Královéhradeckém	královéhradecký	k2eAgInSc6d1
kraji	kraj	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Územní	územní	k2eAgFnSc2d1
památkové	památkový	k2eAgFnSc2d1
správy	správa	k1gFnSc2
<g/>
:	:	kIx,
</s>
<s>
NPÚ	NPÚ	kA
územní	územní	k2eAgFnSc1d1
památková	památkový	k2eAgFnSc1d1
správa	správa	k1gFnSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
zajišťuje	zajišťovat	k5eAaImIp3nS
správu	správa	k1gFnSc4
a	a	k8xC
zpřístupnění	zpřístupnění	k1gNnSc4
kulturních	kulturní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
v	v	k7c6
přímé	přímý	k2eAgFnSc6d1
správě	správa	k1gFnSc6
NPÚ	NPÚ	kA
v	v	k7c6
hl.	hl.	k?
m.	m.	k?
Praze	Praha	k1gFnSc3
a	a	k8xC
v	v	k7c6
krajích	kraj	k1gInPc6
Středočeském	středočeský	k2eAgMnSc6d1
<g/>
,	,	kIx,
Ústeckém	ústecký	k2eAgMnSc6d1
a	a	k8xC
Karlovarském	karlovarský	k2eAgMnSc6d1
<g/>
.	.	kIx.
</s>
<s>
NPÚ	NPÚ	kA
územní	územní	k2eAgFnSc1d1
památková	památkový	k2eAgFnSc1d1
správa	správa	k1gFnSc1
v	v	k7c6
Českých	český	k2eAgInPc6d1
Budějovicích	Budějovice	k1gInPc6
zajišťuje	zajišťovat	k5eAaImIp3nS
správu	správa	k1gFnSc4
a	a	k8xC
zpřístupnění	zpřístupnění	k1gNnSc4
kulturních	kulturní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
v	v	k7c6
přímé	přímý	k2eAgFnSc6d1
správě	správa	k1gFnSc6
NPÚ	NPÚ	kA
v	v	k7c6
krajích	kraj	k1gInPc6
Jihočeském	jihočeský	k2eAgNnSc6d1
<g/>
,	,	kIx,
Plzeňském	plzeňské	k1gNnSc6
a	a	k8xC
Vysočina	vysočina	k1gFnSc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
tzv.	tzv.	kA
Špitálek	špitálek	k1gInSc1
<g/>
,	,	kIx,
tč	tč	kA
<g/>
.	.	kIx.
v	v	k7c6
rekonstrukci	rekonstrukce	k1gFnSc6
<g/>
)	)	kIx)
</s>
<s>
NPÚ	NPÚ	kA
územní	územní	k2eAgFnSc1d1
památková	památkový	k2eAgFnSc1d1
správa	správa	k1gFnSc1
na	na	k7c6
Sychrově	Sychrův	k2eAgInSc6d1
zajišťuje	zajišťovat	k5eAaImIp3nS
správu	správa	k1gFnSc4
a	a	k8xC
zpřístupnění	zpřístupnění	k1gNnSc4
kulturních	kulturní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
v	v	k7c6
přímé	přímý	k2eAgFnSc6d1
správě	správa	k1gFnSc6
NPÚ	NPÚ	kA
v	v	k7c6
krajích	kraj	k1gInPc6
Libereckém	liberecký	k2eAgMnSc6d1
<g/>
,	,	kIx,
Královéhradeckém	královéhradecký	k2eAgMnSc6d1
a	a	k8xC
Pardubickém	pardubický	k2eAgMnSc6d1
<g/>
.	.	kIx.
</s>
<s>
NPÚ	NPÚ	kA
územní	územní	k2eAgFnSc1d1
památková	památkový	k2eAgFnSc1d1
správa	správa	k1gFnSc1
v	v	k7c6
Kroměříži	Kroměříž	k1gFnSc6
zajišťuje	zajišťovat	k5eAaImIp3nS
správu	správa	k1gFnSc4
a	a	k8xC
zpřístupnění	zpřístupnění	k1gNnSc4
kulturních	kulturní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
v	v	k7c6
přímé	přímý	k2eAgFnSc6d1
správě	správa	k1gFnSc6
NPÚ	NPÚ	kA
v	v	k7c6
krajích	kraj	k1gInPc6
Jihomoravském	jihomoravský	k2eAgNnSc6d1
<g/>
,	,	kIx,
Zlínském	zlínský	k2eAgNnSc6d1
<g/>
,	,	kIx,
Olomouckém	olomoucký	k2eAgNnSc6d1
a	a	k8xC
Moravskoslezském	moravskoslezský	k2eAgNnSc6d1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Arcibiskupský	arcibiskupský	k2eAgInSc1d1
zámek	zámek	k1gInSc1
Kroměříž	Kroměříž	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Vedení	vedení	k1gNnSc1
a	a	k8xC
spory	spor	k1gInPc1
o	o	k7c4
koncepci	koncepce	k1gFnSc4
</s>
<s>
Ředitelé	ředitel	k1gMnPc1
</s>
<s>
1958	#num#	k4
<g/>
-	-	kIx~
<g/>
(	(	kIx(
<g/>
1965	#num#	k4
<g/>
??	??	k?
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Václav	Václav	k1gMnSc1
Mencl	Mencl	k1gMnSc1
</s>
<s>
(	(	kIx(
<g/>
1965	#num#	k4
<g/>
-	-	kIx~
<g/>
1975	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Vladimír	Vladimír	k1gMnSc1
Novotný	Novotný	k1gMnSc1
</s>
<s>
(	(	kIx(
<g/>
1975	#num#	k4
<g/>
-	-	kIx~
<g/>
1983	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Otilie	Otilie	k1gFnSc1
Novotná	Novotná	k1gFnSc1
</s>
<s>
(	(	kIx(
<g/>
1983	#num#	k4
<g/>
)	)	kIx)
<g/>
-	-	kIx~
<g/>
1990	#num#	k4
<g/>
:	:	kIx,
Bedřich	Bedřich	k1gMnSc1
Tykva	Tykv	k1gMnSc2
</s>
<s>
1990	#num#	k4
<g/>
-	-	kIx~
<g/>
2002	#num#	k4
<g/>
:	:	kIx,
Josef	Josef	k1gMnSc1
Štulc	Štulc	k1gFnSc4
</s>
<s>
2003	#num#	k4
<g/>
–	–	k?
<g/>
2004	#num#	k4
<g/>
:	:	kIx,
Jiří	Jiří	k1gMnSc1
T.	T.	kA
Kotalík	Kotalík	k1gMnSc1
</s>
<s>
2005	#num#	k4
<g/>
:	:	kIx,
Zdeněk	Zdeněk	k1gMnSc1
Novák	Novák	k1gMnSc1
</s>
<s>
2005	#num#	k4
<g/>
–	–	k?
<g/>
2006	#num#	k4
<g/>
:	:	kIx,
Tomáš	Tomáš	k1gMnSc1
Hájek	Hájek	k1gMnSc1
</s>
<s>
2006	#num#	k4
<g/>
–	–	k?
<g/>
2008	#num#	k4
<g/>
:	:	kIx,
Pavel	Pavel	k1gMnSc1
Jerie	Jerie	k1gFnSc2
</s>
<s>
Generální	generální	k2eAgFnSc1d1
ředitelka	ředitelka	k1gFnSc1
od	od	k7c2
roku	rok	k1gInSc2
2008	#num#	k4
<g/>
:	:	kIx,
Naděžda	Naděžda	k1gFnSc1
Goryczková	Goryczkový	k2eAgFnSc1d1
</s>
<s>
Spory	spor	k1gInPc1
o	o	k7c4
koncepci	koncepce	k1gFnSc4
</s>
<s>
Ministr	ministr	k1gMnSc1
kultury	kultura	k1gFnSc2
Pavel	Pavel	k1gMnSc1
Dostál	Dostál	k1gMnSc1
jmenoval	jmenovat	k5eAaBmAgMnS,k5eAaImAgMnS
generálním	generální	k2eAgMnSc7d1
ředitelem	ředitel	k1gMnSc7
NPÚ	NPÚ	kA
Zdeňka	Zdeněk	k1gMnSc4
Nováka	Novák	k1gMnSc4
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
lednu	leden	k1gInSc3
2006	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ministr	ministr	k1gMnSc1
kultury	kultura	k1gFnSc2
Vítězslav	Vítězslav	k1gMnSc1
Jandák	Jandák	k1gMnSc1
nejprve	nejprve	k6eAd1
rozhodnutí	rozhodnutí	k1gNnPc4
uspíšil	uspíšit	k5eAaPmAgMnS
a	a	k8xC
jmenoval	jmenovat	k5eAaBmAgMnS,k5eAaImAgMnS
jej	on	k3xPp3gMnSc4
dopisem	dopis	k1gInSc7
z	z	k7c2
11	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
do	do	k7c2
funkce	funkce	k1gFnSc2
již	již	k6eAd1
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2005	#num#	k4
<g/>
,	,	kIx,
avšak	avšak	k8xC
dopisem	dopis	k1gInSc7
z	z	k7c2
1	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
ho	on	k3xPp3gMnSc4
ke	k	k7c3
2	#num#	k4
<g/>
.	.	kIx.
prosinci	prosinec	k1gInSc6
odvolal	odvolat	k5eAaPmAgMnS
a	a	k8xC
s	s	k7c7
ním	on	k3xPp3gMnSc7
zároveň	zároveň	k6eAd1
též	též	k9
10	#num#	k4
ředitelů	ředitel	k1gMnPc2
odborných	odborný	k2eAgNnPc2d1
územních	územní	k2eAgNnPc2d1
pracovišť	pracoviště	k1gNnPc2
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
podle	podle	k7c2
svých	svůj	k3xOyFgNnPc2
slov	slovo	k1gNnPc2
zahájil	zahájit	k5eAaPmAgInS
reformu	reforma	k1gFnSc4
památkové	památkový	k2eAgFnSc2d1
péče	péče	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Ministr	ministr	k1gMnSc1
Jandák	Jandák	k1gMnSc1
zdůvodnil	zdůvodnit	k5eAaPmAgMnS
odvolání	odvolání	k1gNnSc4
generálního	generální	k2eAgMnSc2d1
ředitele	ředitel	k1gMnSc2
zjištěním	zjištění	k1gNnPc3
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
na	na	k7c6
Strahově	Strahov	k1gInSc6
bourá	bourat	k5eAaImIp3nS
část	část	k1gFnSc1
Hladové	hladový	k2eAgFnSc2d1
zdi	zeď	k1gFnSc2
<g/>
,	,	kIx,
Jandákův	Jandákův	k2eAgMnSc1d1
náměstek	náměstek	k1gMnSc1
to	ten	k3xDgNnSc4
však	však	k9
vysvětloval	vysvětlovat	k5eAaImAgMnS
širšími	široký	k2eAgFnPc7d2
systémovými	systémový	k2eAgFnPc7d1
změnami	změna	k1gFnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
lednu	leden	k1gInSc6
2006	#num#	k4
ministr	ministr	k1gMnSc1
kultury	kultura	k1gFnSc2
Vítězslav	Vítězslav	k1gMnSc1
Jandák	Jandák	k1gMnSc1
jmenoval	jmenovat	k5eAaImAgMnS,k5eAaBmAgMnS
generálním	generální	k2eAgMnSc7d1
ředitelem	ředitel	k1gMnSc7
NPÚ	NPÚ	kA
Tomáše	Tomáš	k1gMnSc4
Hájka	Hájek	k1gMnSc4
<g/>
,	,	kIx,
dosavadního	dosavadní	k2eAgMnSc4d1
poradce	poradce	k1gMnSc4
ministra	ministr	k1gMnSc2
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
<g/>
,	,	kIx,
na	na	k7c6
základě	základ	k1gInSc6
jednomyslného	jednomyslný	k2eAgNnSc2d1
doporučení	doporučení	k1gNnSc2
komise	komise	k1gFnSc2
ministerstva	ministerstvo	k1gNnSc2
kultury	kultura	k1gFnSc2
pod	pod	k7c7
vedením	vedení	k1gNnSc7
ředitele	ředitel	k1gMnSc2
personálního	personální	k2eAgInSc2d1
odboru	odbor	k1gInSc2
ministerstva	ministerstvo	k1gNnSc2
kultury	kultura	k1gFnSc2
Petra	Petr	k1gMnSc2
Millera	Miller	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hájek	Hájek	k1gMnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
terčem	terč	k1gInSc7
kritiky	kritika	k1gFnSc2
odborné	odborný	k2eAgFnSc2d1
veřejnosti	veřejnost	k1gFnSc2
(	(	kIx(
<g/>
včetně	včetně	k7c2
některých	některý	k3yIgMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
ho	on	k3xPp3gMnSc4
předtím	předtím	k6eAd1
do	do	k7c2
funkce	funkce	k1gFnSc2
doporučili	doporučit	k5eAaPmAgMnP
<g/>
)	)	kIx)
a	a	k8xC
vzniklo	vzniknout	k5eAaPmAgNnS
několik	několik	k4yIc1
petic	petice	k1gFnPc2
za	za	k7c4
jeho	jeho	k3xOp3gNnSc4
odvolání	odvolání	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hájkovi	Hájkův	k2eAgMnPc1d1
údajně	údajně	k6eAd1
památkáři	památkář	k1gMnPc1
<g/>
,	,	kIx,
historici	historik	k1gMnPc1
umění	umění	k1gNnPc2
a	a	k8xC
další	další	k2eAgMnPc1d1
odborníci	odborník	k1gMnPc1
vyčitali	vyčitat	k5eAaPmAgMnP,k5eAaImAgMnP,k5eAaBmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
prosazoval	prosazovat	k5eAaImAgMnS
oddělení	oddělení	k1gNnSc4
správy	správa	k1gFnSc2
státních	státní	k2eAgInPc2d1
památkových	památkový	k2eAgInPc2d1
objektů	objekt	k1gInPc2
od	od	k7c2
výkonu	výkon	k1gInSc2
expertní	expertní	k2eAgFnSc2d1
činnosti	činnost	k1gFnSc2
(	(	kIx(
<g/>
k	k	k7c3
tomu	ten	k3xDgNnSc3
nakonec	nakonec	k6eAd1
došlo	dojít	k5eAaPmAgNnS
v	v	k7c6
mírnější	mírný	k2eAgFnSc6d2
podobě	podoba	k1gFnSc6
<g/>
,	,	kIx,
tedy	tedy	k9
pouhým	pouhý	k2eAgNnSc7d1
vyčleněním	vyčlenění	k1gNnSc7
v	v	k7c6
rámci	rámec	k1gInSc6
NPÚ	NPÚ	kA
<g/>
,	,	kIx,
od	od	k7c2
ledna	leden	k1gInSc2
2013	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
že	že	k8xS
chtěl	chtít	k5eAaImAgMnS
prosadit	prosadit	k5eAaPmF
nový	nový	k2eAgInSc4d1
památkový	památkový	k2eAgInSc4d1
zákon	zákon	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
by	by	kYmCp3nS
sjednotil	sjednotit	k5eAaPmAgInS
správní	správní	k2eAgNnSc4d1
řízení	řízení	k1gNnSc4
s	s	k7c7
vypracováváním	vypracovávání	k1gNnSc7
odborných	odborný	k2eAgInPc2d1
posudků	posudek	k1gInPc2
(	(	kIx(
<g/>
od	od	k7c2
toho	ten	k3xDgInSc2
NPÚ	NPÚ	kA
poté	poté	k6eAd1
zatím	zatím	k6eAd1
upustil	upustit	k5eAaPmAgMnS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
mu	on	k3xPp3gMnSc3
vyčítáno	vyčítán	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
především	především	k6eAd1
teoretikem	teoretik	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předmětem	předmět	k1gInSc7
odporu	odpor	k1gInSc2
bylo	být	k5eAaImAgNnS
jeho	jeho	k3xOp3gNnSc1
tvrzení	tvrzení	k1gNnSc1
<g/>
,	,	kIx,
že	že	k8xS
zachování	zachování	k1gNnSc1
památky	památka	k1gFnSc2
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
poměřovat	poměřovat	k5eAaImF
její	její	k3xOp3gFnSc7
životaschopností	životaschopnost	k1gFnSc7
<g/>
,	,	kIx,
úvahy	úvaha	k1gFnPc1
o	o	k7c4
redukci	redukce	k1gFnSc4
počtu	počet	k1gInSc2
památek	památka	k1gFnPc2
a	a	k8xC
malý	malý	k2eAgInSc4d1
důraz	důraz	k1gInSc4
na	na	k7c4
zachování	zachování	k1gNnSc4
autenticity	autenticita	k1gFnSc2
památky	památka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Ministr	ministr	k1gMnSc1
kultury	kultura	k1gFnSc2
Martin	Martin	k1gMnSc1
Štěpánek	Štěpánek	k1gMnSc1
v	v	k7c6
září	září	k1gNnSc6
2006	#num#	k4
Tomáše	Tomáš	k1gMnSc4
Hájka	Hájek	k1gMnSc4
z	z	k7c2
funkce	funkce	k1gFnSc2
odvolal	odvolat	k5eAaPmAgInS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Oficiálně	oficiálně	k6eAd1
byla	být	k5eAaImAgFnS
důvodem	důvod	k1gInSc7
odvolání	odvolání	k1gNnSc4
„	„	k?
<g/>
dlouhodobá	dlouhodobý	k2eAgFnSc1d1
a	a	k8xC
neřešená	řešený	k2eNgNnPc4d1
manažerská	manažerský	k2eAgNnPc4d1
a	a	k8xC
odborná	odborný	k2eAgNnPc4d1
pochybení	pochybení	k1gNnSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2006	#num#	k4
ministr	ministr	k1gMnSc1
kultury	kultura	k1gFnSc2
Martin	Martin	k1gMnSc1
Štěpánek	Štěpánek	k1gMnSc1
jmenoval	jmenovat	k5eAaImAgMnS,k5eAaBmAgMnS
generálním	generální	k2eAgMnSc7d1
ředitelem	ředitel	k1gMnSc7
NPÚ	NPÚ	kA
Pavla	Pavel	k1gMnSc2
Jerieho	Jerie	k1gMnSc2
<g/>
,	,	kIx,
dosavadního	dosavadní	k2eAgMnSc2d1
náměstka	náměstek	k1gMnSc2
ředitele	ředitel	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Některá	některý	k3yIgNnPc1
média	médium	k1gNnPc1
o	o	k7c6
tom	ten	k3xDgInSc6
psala	psát	k5eAaImAgNnP
jako	jako	k9
o	o	k7c6
dočasném	dočasný	k2eAgInSc6d1
pověření	pověření	k1gNnSc2
řízením	řízení	k1gNnSc7
do	do	k7c2
doby	doba	k1gFnSc2
řádného	řádný	k2eAgNnSc2d1
výběrového	výběrový	k2eAgNnSc2d1
řízení	řízení	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
Koncem	koncem	k7c2
listopadu	listopad	k1gInSc2
2008	#num#	k4
oznámil	oznámit	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
hodlá	hodlat	k5eAaImIp3nS
rezignovat	rezignovat	k5eAaBmF
na	na	k7c4
funkci	funkce	k1gFnSc4
<g/>
,	,	kIx,
protože	protože	k8xS
prý	prý	k9
byl	být	k5eAaImAgInS
od	od	k7c2
března	březen	k1gInSc2
pod	pod	k7c7
silným	silný	k2eAgInSc7d1
tlakem	tlak	k1gInSc7
<g/>
,	,	kIx,
aby	aby	k9
čtyři	čtyři	k4xCgInPc4
vybrané	vybraný	k2eAgInPc4d1
státní	státní	k2eAgInPc4d1
zámky	zámek	k1gInPc4
vložil	vložit	k5eAaPmAgMnS
do	do	k7c2
nově	nově	k6eAd1
založeného	založený	k2eAgNnSc2d1
zájmového	zájmový	k2eAgNnSc2d1
sdružení	sdružení	k1gNnSc2
právnických	právnický	k2eAgFnPc2d1
osob	osoba	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
by	by	kYmCp3nP
pro	pro	k7c4
rozvoj	rozvoj	k1gInSc4
těchto	tento	k3xDgFnPc2
památek	památka	k1gFnPc2
samo	sám	k3xTgNnSc1
žádalo	žádat	k5eAaImAgNnS
o	o	k7c4
evropské	evropský	k2eAgFnPc4d1
dotace	dotace	k1gFnPc4
a	a	k8xC
hospodařilo	hospodařit	k5eAaImAgNnS
s	s	k7c7
nimi	on	k3xPp3gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jerie	Jerie	k1gFnSc1
byl	být	k5eAaImAgInS
názoru	názor	k1gInSc2
<g/>
,	,	kIx,
že	že	k8xS
zcizením	zcizení	k1gNnSc7
těchto	tento	k3xDgFnPc2
památek	památka	k1gFnPc2
státu	stát	k1gInSc2
by	by	kYmCp3nS
se	se	k3xPyFc4
dopustil	dopustit	k5eAaPmAgMnS
trestného	trestný	k2eAgInSc2d1
činu	čin	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
na	na	k7c6
základě	základ	k1gInSc6
nezávislých	závislý	k2eNgFnPc2d1
analýz	analýza	k1gFnPc2
od	od	k7c2
ministerstva	ministerstvo	k1gNnSc2
financí	finance	k1gFnPc2
a	a	k8xC
ministerstva	ministerstvo	k1gNnSc2
pro	pro	k7c4
místní	místní	k2eAgInSc4d1
rozvoj	rozvoj	k1gInSc4
takový	takový	k3xDgInSc4
postup	postup	k1gInSc4
vyloučil	vyloučit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
byl	být	k5eAaImAgInS
prý	prý	k9
tlačen	tlačen	k2eAgInSc1d1
k	k	k7c3
prohlášení	prohlášení	k1gNnSc3
těchto	tento	k3xDgFnPc2
památek	památka	k1gFnPc2
za	za	k7c4
nepotřebný	potřebný	k2eNgInSc4d1
majetek	majetek	k1gInSc4
či	či	k8xC
k	k	k7c3
jejich	jejich	k3xOp3gNnSc3
zapůjčení	zapůjčení	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jerie	Jerie	k1gFnSc2
tento	tento	k3xDgInSc1
způsob	způsob	k1gInSc1
tunelování	tunelování	k1gNnSc2
nazval	nazvat	k5eAaBmAgInS,k5eAaPmAgInS
„	„	k?
<g/>
modelem	model	k1gInSc7
a	a	k8xC
la	la	k1gNnSc1
Mlynář	Mlynář	k1gMnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Projekty	projekt	k1gInPc7
prosazovala	prosazovat	k5eAaImAgFnS
zejména	zejména	k9
Jana	Jana	k1gFnSc1
Vohralíková	Vohralíkový	k2eAgFnSc1d1
<g/>
,	,	kIx,
vrchní	vrchní	k2eAgFnSc1d1
ředitelka	ředitelka	k1gFnSc1
sekce	sekce	k1gFnSc2
D	D	kA
na	na	k7c6
ministerstvu	ministerstvo	k1gNnSc6
kultury	kultura	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
Jerieho	Jerie	k1gMnSc4
argumentaci	argumentace	k1gFnSc4
o	o	k7c6
porušení	porušení	k1gNnSc6
zákona	zákon	k1gInSc2
označila	označit	k5eAaPmAgFnS
za	za	k7c4
nesmysl	nesmysl	k1gInSc4
založený	založený	k2eAgInSc4d1
na	na	k7c6
polopravdách	polopravda	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jerie	Jerie	k1gFnSc1
podle	podle	k7c2
ní	on	k3xPp3gFnSc2
není	být	k5eNaImIp3nS
ochoten	ochoten	k2eAgInSc1d1
památky	památka	k1gFnPc4
vrátit	vrátit	k5eAaPmF
do	do	k7c2
života	život	k1gInSc2
a	a	k8xC
celý	celý	k2eAgInSc4d1
život	život	k1gInSc4
hájil	hájit	k5eAaImAgMnS
jednu	jeden	k4xCgFnSc4
koncepci	koncepce	k1gFnSc4
<g/>
,	,	kIx,
a	a	k8xC
když	když	k8xS
najednou	najednou	k6eAd1
vidí	vidět	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
to	ten	k3xDgNnSc1
jde	jít	k5eAaImIp3nS
i	i	k9
jinak	jinak	k6eAd1
<g/>
,	,	kIx,
tak	tak	k6eAd1
si	se	k3xPyFc3
vymýšlí	vymýšlet	k5eAaImIp3nS
různé	různý	k2eAgInPc4d1
důvody	důvod	k1gInPc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
to	ten	k3xDgNnSc4
zkomplikoval	zkomplikovat	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2008	#num#	k4
ministr	ministr	k1gMnSc1
kultury	kultura	k1gFnSc2
Václav	Václav	k1gMnSc1
Jehlička	jehlička	k1gFnSc1
na	na	k7c6
základě	základ	k1gInSc6
doporučení	doporučení	k1gNnSc4
výběrové	výběrový	k2eAgFnSc2d1
komise	komise	k1gFnSc2
jmenoval	jmenovat	k5eAaBmAgMnS,k5eAaImAgMnS
generální	generální	k2eAgFnSc7d1
ředitelkou	ředitelka	k1gFnSc7
NPÚ	NPÚ	kA
Naděždu	Naděžda	k1gFnSc4
Goryczkovou	Goryczkový	k2eAgFnSc4d1
<g/>
,	,	kIx,
dosavadní	dosavadní	k2eAgFnSc4d1
ředitelku	ředitelka	k1gFnSc4
ostravského	ostravský	k2eAgNnSc2d1
pracoviště	pracoviště	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Když	když	k8xS
připravovala	připravovat	k5eAaImAgFnS
oddělení	oddělení	k1gNnSc4
správy	správa	k1gFnSc2
státních	státní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
od	od	k7c2
odborné	odborný	k2eAgFnSc2d1
památkové	památkový	k2eAgFnSc2d1
péče	péče	k1gFnSc2
<g/>
,	,	kIx,
odborníky	odborník	k1gMnPc4
byla	být	k5eAaImAgFnS
podezírána	podezírán	k2eAgFnSc1d1
ze	z	k7c2
snahy	snaha	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
o	o	k7c6
privatizaci	privatizace	k1gFnSc6
nejzachovalejších	zachovalý	k2eAgInPc2d3
hradů	hrad	k1gInPc2
a	a	k8xC
zámků	zámek	k1gInPc2
<g/>
,	,	kIx,
tajení	tajení	k1gNnSc1
plánů	plán	k1gInPc2
a	a	k8xC
odmítání	odmítání	k1gNnSc1
veřejné	veřejný	k2eAgFnSc2d1
diskuse	diskuse	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Parlamentní	parlamentní	k2eAgInSc1d1
podvýbor	podvýbor	k1gInSc1
pro	pro	k7c4
kulturu	kultura	k1gFnSc4
prý	prý	k9
na	na	k7c4
jednání	jednání	k1gNnSc4
dospěl	dochvít	k5eAaPmAgMnS
k	k	k7c3
závěru	závěr	k1gInSc3
<g/>
,	,	kIx,
že	že	k8xS
reorganizace	reorganizace	k1gFnSc1
nepřinesla	přinést	k5eNaPmAgFnS
vyšší	vysoký	k2eAgInSc4d2
standard	standard	k1gInSc4
služeb	služba	k1gFnPc2
a	a	k8xC
servisu	servis	k1gInSc2
pro	pro	k7c4
regiony	region	k1gInPc4
a	a	k8xC
ani	ani	k8xC
ke	k	k7c3
zlepšení	zlepšení	k1gNnSc3
financování	financování	k1gNnSc2
nedochází	docházet	k5eNaImIp3nS
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
konstatoval	konstatovat	k5eAaBmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
probíhající	probíhající	k2eAgFnSc6d1
restrukturalizaci	restrukturalizace	k1gFnSc6
provázejí	provázet	k5eAaImIp3nP
zásadní	zásadní	k2eAgInPc4d1
nedostatky	nedostatek	k1gInPc4
a	a	k8xC
vyzval	vyzvat	k5eAaPmAgMnS
ministerstvo	ministerstvo	k1gNnSc4
k	k	k7c3
vyvození	vyvození	k1gNnSc3
důsledků	důsledek	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Výhrady	výhrada	k1gFnPc1
byly	být	k5eAaImAgFnP
i	i	k9
například	například	k6eAd1
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
územní	územní	k2eAgFnSc1d1
správa	správa	k1gFnSc1
pro	pro	k7c4
Moravu	Morava	k1gFnSc4
není	být	k5eNaImIp3nS
umístěna	umístit	k5eAaPmNgFnS
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
Pro	pro	k7c4
výměnu	výměna	k1gFnSc4
ředitelky	ředitelka	k1gFnSc2
se	se	k3xPyFc4
vyjadřoval	vyjadřovat	k5eAaImAgMnS
zejména	zejména	k9
poslanec	poslanec	k1gMnSc1
a	a	k8xC
stínový	stínový	k2eAgMnSc1d1
ministr	ministr	k1gMnSc1
kultury	kultura	k1gFnSc2
Vítězslav	Vítězslav	k1gMnSc1
Jandák	Jandák	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
únoru	únor	k1gInSc6
2013	#num#	k4
skupina	skupina	k1gFnSc1
čtyř	čtyři	k4xCgMnPc2
akademiků	akademik	k1gMnPc2
(	(	kIx(
<g/>
děkan	děkan	k1gMnSc1
FF	ff	kA
UK	UK	kA
Michal	Michal	k1gMnSc1
Stehlík	Stehlík	k1gMnSc1
<g/>
,	,	kIx,
ředitel	ředitel	k1gMnSc1
Ústavu	ústav	k1gInSc2
pro	pro	k7c4
dějiny	dějiny	k1gFnPc4
umění	umění	k1gNnSc2
FF	ff	kA
UK	UK	kA
Jan	Jan	k1gMnSc1
Royt	Royt	k1gMnSc1
<g/>
,	,	kIx,
ředitel	ředitel	k1gMnSc1
Ústavu	ústav	k1gInSc2
dějin	dějiny	k1gFnPc2
<g />
.	.	kIx.
</s>
<s hack="1">
umění	umění	k1gNnSc1
AV	AV	kA
ČR	ČR	kA
Vojtěch	Vojtěch	k1gMnSc1
Lahoda	lahoda	k1gFnSc1
<g/>
,	,	kIx,
rektor	rektor	k1gMnSc1
Akademie	akademie	k1gFnSc2
výtvarných	výtvarný	k2eAgNnPc2d1
umění	umění	k1gNnPc2
v	v	k7c6
Praze	Praha	k1gFnSc6
Jiří	Jiří	k1gMnSc1
T.	T.	kA
Kotalík	Kotalík	k1gMnSc1
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
protestovala	protestovat	k5eAaBmAgFnS
u	u	k7c2
ministryně	ministryně	k1gFnSc2
kultury	kultura	k1gFnSc2
Aleny	Alena	k1gFnSc2
Hanákové	Hanáková	k1gFnSc2
otevřeným	otevřený	k2eAgInSc7d1
dopisem	dopis	k1gInSc7
proti	proti	k7c3
údajně	údajně	k6eAd1
připravovanému	připravovaný	k2eAgNnSc3d1
odvolání	odvolání	k1gNnSc3
Naděždy	Naděžda	k1gFnSc2
Goryczkové	Goryczkový	k2eAgFnSc2d1
a	a	k8xC
vyjádřila	vyjádřit	k5eAaPmAgFnS
jí	on	k3xPp3gFnSc3
podporu	podpora	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Seznam	seznam	k1gInSc1
památkových	památkový	k2eAgInPc2d1
objektů	objekt	k1gInPc2
ve	v	k7c6
správě	správa	k1gFnSc6
Národního	národní	k2eAgInSc2d1
památkového	památkový	k2eAgInSc2d1
ústavu	ústav	k1gInSc2
</s>
<s>
Podrobný	podrobný	k2eAgInSc1d1
seznam	seznam	k1gInSc1
památek	památka	k1gFnPc2
ve	v	k7c6
správě	správa	k1gFnSc6
NPÚ	NPÚ	kA
viz	vidět	k5eAaImRp2nS
Seznam	seznam	k1gInSc4
památkových	památkový	k2eAgInPc2d1
objektů	objekt	k1gInPc2
ve	v	k7c6
správě	správa	k1gFnSc6
Národního	národní	k2eAgInSc2d1
památkového	památkový	k2eAgInSc2d1
ústavu	ústav	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
Souhrnné	souhrnný	k2eAgFnPc1d1
informace	informace	k1gFnPc1
a	a	k8xC
historie	historie	k1gFnPc1
<g/>
,	,	kIx,
Národní	národní	k2eAgInSc1d1
památkový	památkový	k2eAgInSc1d1
ústav	ústav	k1gInSc1
<g/>
↑	↑	k?
Publikační	publikační	k2eAgFnSc4d1
činnost	činnost	k1gFnSc4
NPÚ	NPÚ	kA
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Zdeněk	Zdeněk	k1gMnSc1
Novák	Novák	k1gMnSc1
-	-	kIx~
jednodenní	jednodenní	k2eAgMnSc1d1
ředitel	ředitel	k1gMnSc1
Národního	národní	k2eAgInSc2d1
památkového	památkový	k2eAgInSc2d1
ústavu	ústav	k1gInSc2
<g/>
,	,	kIx,
Radiožurnál	radiožurnál	k1gInSc1
<g/>
,	,	kIx,
8	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
20051	#num#	k4
2	#num#	k4
3	#num#	k4
Štěpánek	Štěpánek	k1gMnSc1
odvolal	odvolat	k5eAaPmAgMnS
šéfa	šéf	k1gMnSc4
památkového	památkový	k2eAgInSc2d1
ústavu	ústav	k1gInSc2
<g/>
,	,	kIx,
Český	český	k2eAgInSc1d1
a	a	k8xC
slovenský	slovenský	k2eAgInSc1d1
svet	svet	k1gInSc1
<g/>
,	,	kIx,
27	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2006	#num#	k4
<g/>
,	,	kIx,
ČTK	ČTK	kA
<g/>
↑	↑	k?
Ředitel	ředitel	k1gMnSc1
Národního	národní	k2eAgInSc2d1
památkového	památkový	k2eAgInSc2d1
ústavu	ústav	k1gInSc2
odvolán	odvolat	k5eAaPmNgMnS
<g/>
,	,	kIx,
BBC-Czech	BBC-Cza	k1gMnPc6
<g/>
,	,	kIx,
2	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
20051	#num#	k4
2	#num#	k4
Ministr	ministr	k1gMnSc1
Štěpánek	Štěpánek	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
odvolal	odvolat	k5eAaPmAgMnS
ředitele	ředitel	k1gMnSc4
památkového	památkový	k2eAgInSc2d1
ústavu	ústav	k1gInSc2
Hájka	hájka	k1gFnSc1
<g/>
,	,	kIx,
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
26	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2006	#num#	k4
<g/>
↑	↑	k?
Pavel	Pavel	k1gMnSc1
Jerie	Jerie	k1gFnSc2
generálním	generální	k2eAgMnSc7d1
ředitelem	ředitel	k1gMnSc7
NPÚ	NPÚ	kA
(	(	kIx(
<g/>
tisková	tiskový	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
NPÚ	NPÚ	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
NPÚ	NPÚ	kA
<g/>
,	,	kIx,
27	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2006	#num#	k4
<g/>
↑	↑	k?
Šéfa	šéf	k1gMnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
památkářů	památkář	k1gMnPc2
prý	prý	k9
nutili	nutit	k5eAaImAgMnP
tunelovat	tunelovat	k5eAaImF
<g/>
,	,	kIx,
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
30	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
↑	↑	k?
Památkovému	památkový	k2eAgInSc3d1
ústavu	ústav	k1gInSc2
bude	být	k5eAaImBp3nS
šéfovat	šéfovat	k5eAaImF
Naděžda	Naděžda	k1gFnSc1
Goryczková	Goryczkový	k2eAgFnSc1d1
<g/>
,	,	kIx,
iDnes	iDnes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
,	,	kIx,
jba	jba	k?
<g/>
,	,	kIx,
ČTK	ČTK	kA
<g/>
↑	↑	k?
Naděžda	Naděžda	k1gFnSc1
Goryczková	Goryczkový	k2eAgFnSc1d1
<g/>
,	,	kIx,
ředitelka	ředitelka	k1gFnSc1
Národního	národní	k2eAgInSc2d1
památkového	památkový	k2eAgInSc2d1
ústavu	ústav	k1gInSc2
<g/>
,	,	kIx,
Radiožurnál	radiožurnál	k1gInSc1
<g/>
,	,	kIx,
18	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
20121	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Marie	Marie	k1gFnSc1
Reslová	Reslová	k1gFnSc1
<g/>
:	:	kIx,
Odvolá	odvolat	k5eAaPmIp3nS
ministryně	ministryně	k1gFnSc1
kultury	kultura	k1gFnSc2
šéfku	šéfka	k1gFnSc4
Národního	národní	k2eAgInSc2d1
památkového	památkový	k2eAgInSc2d1
ústavu	ústav	k1gInSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
,	,	kIx,
Česká	český	k2eAgFnSc1d1
pozice	pozice	k1gFnSc1
<g/>
,	,	kIx,
20	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
↑	↑	k?
Otevřený	otevřený	k2eAgInSc4d1
dopis	dopis	k1gInSc4
ministryni	ministryně	k1gFnSc4
kultury	kultura	k1gFnSc2
proti	proti	k7c3
zpochybnění	zpochybnění	k1gNnSc3
pozice	pozice	k1gFnSc2
generální	generální	k2eAgFnSc2d1
ředitelky	ředitelka	k1gFnSc2
Národního	národní	k2eAgInSc2d1
památkového	památkový	k2eAgInSc2d1
ústavu	ústav	k1gInSc2
Ing.	ing.	kA
Arch	arch	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naděždy	Naděžda	k1gFnSc2
Goryczkové	Goryczkový	k2eAgFnSc2d1
aneb	aneb	k?
další	další	k2eAgNnSc4d1
kolo	kolo	k1gNnSc4
personálií	personálie	k1gFnPc2
na	na	k7c4
MK	MK	kA
ČR	ČR	kA
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
,	,	kIx,
19	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Karel	Karel	k1gMnSc1
Kuča	Kuča	k?
<g/>
:	:	kIx,
Hrady	hrad	k1gInPc4
<g/>
,	,	kIx,
zámky	zámek	k1gInPc4
a	a	k8xC
další	další	k2eAgFnPc4d1
památky	památka	k1gFnPc4
ve	v	k7c6
správě	správa	k1gFnSc6
Národního	národní	k2eAgInSc2d1
památkového	památkový	k2eAgInSc2d1
ústavu	ústav	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978-80-87104-44-6	978-80-87104-44-6	k4
</s>
<s>
Milan	Milan	k1gMnSc1
Jančo	Janča	k1gMnSc5
<g/>
,	,	kIx,
Kateřina	Kateřina	k1gFnSc1
Burešová	Burešová	k1gFnSc1
<g/>
,	,	kIx,
Ondřej	Ondřej	k1gMnSc1
Šefců	Šefce	k1gMnPc2
<g/>
:	:	kIx,
Jak	jak	k8xS,k8xC
dobýt	dobýt	k5eAaPmF
hrad	hrad	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Památky	památka	k1gFnSc2
takřka	takřka	k6eAd1
bez	bez	k7c2
bariér	bariéra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978-80-87104-09-5	978-80-87104-09-5	k4
</s>
<s>
Milan	Milan	k1gMnSc1
Jančo	Janča	k1gMnSc5
<g/>
,	,	kIx,
Ondřej	Ondřej	k1gMnSc1
Šefců	Šefce	k1gMnPc2
<g/>
:	:	kIx,
Jak	jak	k8xC,k8xS
dobýt	dobýt	k5eAaPmF
hrad	hrad	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Památky	památka	k1gFnSc2
takřka	takřka	k6eAd1
bez	bez	k7c2
bariér	bariéra	k1gFnPc2
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978-80-87104-47-7	978-80-87104-47-7	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Národní	národní	k2eAgInSc4d1
památkový	památkový	k2eAgInSc4d1
ústav	ústav	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Národní	národní	k2eAgInSc1d1
památkový	památkový	k2eAgInSc1d1
ústav	ústav	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ko	ko	k?
<g/>
2006325301	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2163	#num#	k4
9469	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
2012011185	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
135847292	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
2012011185	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
</s>
