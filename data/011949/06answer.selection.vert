<s>
Velikonoční	velikonoční	k2eAgNnSc4d1	velikonoční
pondělí	pondělí	k1gNnSc4	pondělí
(	(	kIx(	(
<g/>
liturgicky	liturgicky	k6eAd1	liturgicky
Pondělí	pondělí	k1gNnSc1	pondělí
v	v	k7c6	v
oktávu	oktáv	k1gInSc6	oktáv
velikonočním	velikonoční	k2eAgInSc6d1	velikonoční
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
též	též	k9	též
Červené	Červené	k2eAgNnSc4d1	Červené
pondělí	pondělí	k1gNnSc4	pondělí
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dnem	den	k1gInSc7	den
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
následuje	následovat	k5eAaImIp3nS	následovat
po	po	k7c6	po
neděli	neděle	k1gFnSc6	neděle
Zmrtvýchvstání	zmrtvýchvstání	k1gNnSc2	zmrtvýchvstání
Páně	páně	k2eAgNnSc2d1	páně
<g/>
.	.	kIx.	.
</s>
