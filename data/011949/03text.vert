<p>
<s>
Velikonoční	velikonoční	k2eAgNnSc4d1	velikonoční
pondělí	pondělí	k1gNnSc4	pondělí
(	(	kIx(	(
<g/>
liturgicky	liturgicky	k6eAd1	liturgicky
Pondělí	pondělí	k1gNnSc1	pondělí
v	v	k7c6	v
oktávu	oktáv	k1gInSc6	oktáv
velikonočním	velikonoční	k2eAgInSc6d1	velikonoční
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
též	též	k9	též
Červené	Červené	k2eAgNnSc4d1	Červené
pondělí	pondělí	k1gNnSc4	pondělí
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dnem	den	k1gInSc7	den
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
následuje	následovat	k5eAaImIp3nS	následovat
po	po	k7c6	po
neděli	neděle	k1gFnSc6	neděle
Zmrtvýchvstání	zmrtvýchvstání	k1gNnSc2	zmrtvýchvstání
Páně	páně	k2eAgNnSc2d1	páně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
našem	náš	k3xOp1gInSc6	náš
prostředí	prostředí	k1gNnSc6	prostředí
je	být	k5eAaImIp3nS	být
spojeno	spojit	k5eAaPmNgNnS	spojit
s	s	k7c7	s
nejrůznějšími	různý	k2eAgFnPc7d3	nejrůznější
tradicemi	tradice	k1gFnPc7	tradice
a	a	k8xC	a
zvyklostmi	zvyklost	k1gFnPc7	zvyklost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
lze	lze	k6eAd1	lze
zařadit	zařadit	k5eAaPmF	zařadit
k	k	k7c3	k
přechodovým	přechodový	k2eAgInPc3d1	přechodový
rituálům	rituál	k1gInPc3	rituál
končící	končící	k2eAgFnSc2d1	končící
zimy	zima	k1gFnSc2	zima
a	a	k8xC	a
nastávajícího	nastávající	k2eAgNnSc2d1	nastávající
jara	jaro	k1gNnSc2	jaro
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
nekřesťanská	křesťanský	k2eNgFnSc1d1	nekřesťanská
praxe	praxe	k1gFnSc1	praxe
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
dějin	dějiny	k1gFnPc2	dějiny
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
kulturách	kultura	k1gFnPc6	kultura
teologizována	teologizovat	k5eAaPmNgFnS	teologizovat
a	a	k8xC	a
zasazena	zasadit	k5eAaPmNgFnS	zasadit
do	do	k7c2	do
křesťanského	křesťanský	k2eAgInSc2d1	křesťanský
rámce	rámec	k1gInSc2	rámec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Církevní	církevní	k2eAgFnSc1d1	církevní
oslava	oslava	k1gFnSc1	oslava
Velikonoc	Velikonoce	k1gFnPc2	Velikonoce
má	mít	k5eAaImIp3nS	mít
zcela	zcela	k6eAd1	zcela
jiný	jiný	k2eAgInSc4d1	jiný
rámec	rámec	k1gInSc4	rámec
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
je	být	k5eAaImIp3nS	být
Kristovo	Kristův	k2eAgNnSc1d1	Kristovo
vítězství	vítězství	k1gNnSc1	vítězství
nad	nad	k7c7	nad
smrtí	smrt	k1gFnSc7	smrt
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
vzkříšení	vzkříšení	k1gNnSc1	vzkříšení
<g/>
)	)	kIx)	)
–	–	k?	–
základní	základní	k2eAgInSc4d1	základní
bod	bod	k1gInSc4	bod
celého	celý	k2eAgNnSc2d1	celé
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Velikonoční	velikonoční	k2eAgNnSc4d1	velikonoční
pondělí	pondělí	k1gNnSc4	pondělí
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
druhým	druhý	k4xOgNnSc7	druhý
dnem	dno	k1gNnSc7	dno
velikonočního	velikonoční	k2eAgInSc2d1	velikonoční
oktávu	oktáv	k1gInSc2	oktáv
(	(	kIx(	(
<g/>
osmidení	osmidení	k1gNnSc2	osmidení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
křesťané	křesťan	k1gMnPc1	křesťan
prožívají	prožívat	k5eAaImIp3nP	prožívat
velikonoční	velikonoční	k2eAgFnSc4d1	velikonoční
radost	radost	k1gFnSc4	radost
ze	z	k7c2	z
svého	svůj	k3xOyFgNnSc2	svůj
vykoupení	vykoupení	k1gNnSc2	vykoupení
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
liturgického	liturgický	k2eAgNnSc2d1	liturgické
hlediska	hledisko	k1gNnSc2	hledisko
je	být	k5eAaImIp3nS	být
pondělí	pondělí	k1gNnSc1	pondělí
rovnocenné	rovnocenný	k2eAgNnSc1d1	rovnocenné
následujícím	následující	k2eAgInPc3d1	následující
dnům	den	k1gInPc3	den
oktávu	oktáv	k1gInSc2	oktáv
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
historicky	historicky	k6eAd1	historicky
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
pouze	pouze	k6eAd1	pouze
pondělí	pondělí	k1gNnSc1	pondělí
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
dnem	den	k1gInSc7	den
pracovního	pracovní	k2eAgInSc2d1	pracovní
klidu	klid	k1gInSc2	klid
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
krom	krom	k7c2	krom
toho	ten	k3xDgMnSc2	ten
bývalo	bývat	k5eAaImAgNnS	bývat
zvykem	zvyk	k1gInSc7	zvyk
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
významných	významný	k2eAgInPc6d1	významný
svátcích	svátek	k1gInPc6	svátek
následoval	následovat	k5eAaImAgInS	následovat
jakýsi	jakýsi	k3yIgInSc4	jakýsi
volný	volný	k2eAgInSc4d1	volný
den	den	k1gInSc4	den
(	(	kIx(	(
<g/>
odtud	odtud	k6eAd1	odtud
např.	např.	kA	např.
Pondělí	pondělí	k1gNnPc4	pondělí
svatodušní	svatodušní	k2eAgFnSc2d1	svatodušní
či	či	k8xC	či
svátek	svátek	k1gInSc1	svátek
svatého	svatý	k2eAgMnSc2d1	svatý
Štěpána	Štěpán	k1gMnSc2	Štěpán
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
volné	volný	k2eAgInPc1d1	volný
dny	den	k1gInPc1	den
po	po	k7c6	po
svátcích	svátek	k1gInPc6	svátek
doprovázely	doprovázet	k5eAaImAgFnP	doprovázet
různé	různý	k2eAgInPc4d1	různý
lidové	lidový	k2eAgInPc4d1	lidový
zvyky	zvyk	k1gInPc4	zvyk
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
součástí	součást	k1gFnSc7	součást
běžného	běžný	k2eAgInSc2d1	běžný
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
i	i	k8xC	i
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
je	být	k5eAaImIp3nS	být
zvykem	zvyk	k1gInSc7	zvyk
chodit	chodit	k5eAaImF	chodit
s	s	k7c7	s
pomlázkou	pomlázka	k1gFnSc7	pomlázka
<g/>
.	.	kIx.	.
</s>
<s>
Pomlázka	pomlázka	k1gFnSc1	pomlázka
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
několika	několik	k4yIc2	několik
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
12	[number]	k4	12
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
vrbových	vrbový	k2eAgInPc2d1	vrbový
proutků	proutek	k1gInPc2	proutek
pletený	pletený	k2eAgInSc1d1	pletený
šlehací	šlehací	k2eAgInSc1d1	šlehací
nástroj	nástroj	k1gInSc1	nástroj
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yQgNnSc7	který
chlapci	chlapec	k1gMnPc1	chlapec
chodí	chodit	k5eAaImIp3nP	chodit
na	na	k7c4	na
koledu	koleda	k1gFnSc4	koleda
a	a	k8xC	a
mrskají	mrskat	k5eAaImIp3nP	mrskat
děvčata	děvče	k1gNnPc1	děvče
z	z	k7c2	z
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Šlehání	šlehání	k1gNnSc1	šlehání
přes	přes	k7c4	přes
hýždě	hýždě	k1gFnPc4	hýždě
doprovází	doprovázet	k5eAaImIp3nP	doprovázet
odříkávání	odříkávání	k1gNnSc3	odříkávání
různých	různý	k2eAgFnPc2d1	různá
koledních	kolední	k2eAgFnPc2d1	kolední
říkanek	říkanka	k1gFnPc2	říkanka
<g/>
.	.	kIx.	.
</s>
<s>
Velikonoční	velikonoční	k2eAgNnSc1d1	velikonoční
mrskání	mrskání	k1gNnSc1	mrskání
děvčat	děvče	k1gNnPc2	děvče
jim	on	k3xPp3gMnPc3	on
má	mít	k5eAaImIp3nS	mít
předat	předat	k5eAaPmF	předat
část	část	k1gFnSc4	část
jarní	jarní	k2eAgFnSc2d1	jarní
svěžesti	svěžest	k1gFnSc2	svěžest
vrbového	vrbový	k2eAgNnSc2d1	vrbové
proutí	proutí	k1gNnSc2	proutí
(	(	kIx(	(
<g/>
Mrskut	mrskut	k1gInSc1	mrskut
a	a	k8xC	a
mrskání	mrskání	k1gNnPc1	mrskání
jsou	být	k5eAaImIp3nP	být
názvy	název	k1gInPc4	název
zažité	zažitý	k2eAgInPc4d1	zažitý
především	především	k6eAd1	především
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
má	mít	k5eAaImIp3nS	mít
zvyk	zvyk	k1gInSc1	zvyk
velmi	velmi	k6eAd1	velmi
silnou	silný	k2eAgFnSc4d1	silná
tradici	tradice	k1gFnSc4	tradice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
regionech	region	k1gInPc6	region
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
se	se	k3xPyFc4	se
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
odlišnými	odlišný	k2eAgInPc7d1	odlišný
výrazy	výraz	k1gInPc7	výraz
pro	pro	k7c4	pro
totéž	týž	k3xTgNnSc4	týž
<g/>
,	,	kIx,	,
např.	např.	kA	např.
šupání	šupání	k1gNnSc1	šupání
<g/>
,	,	kIx,	,
vyšupat	vyšupat	k5eAaPmF	vyšupat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velikonoční	velikonoční	k2eAgFnSc1d1	velikonoční
koleda	koleda	k1gFnSc1	koleda
je	být	k5eAaImIp3nS	být
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
odměnou	odměna	k1gFnSc7	odměna
pro	pro	k7c4	pro
koledníky	koledník	k1gMnPc4	koledník
<g/>
.	.	kIx.	.
</s>
<s>
Tradičně	tradičně	k6eAd1	tradičně
to	ten	k3xDgNnSc4	ten
jsou	být	k5eAaImIp3nP	být
především	především	k6eAd1	především
malovaná	malovaný	k2eAgNnPc1d1	malované
vajíčka	vajíčko	k1gNnPc1	vajíčko
neboli	neboli	k8xC	neboli
kraslice	kraslice	k1gFnPc1	kraslice
<g/>
.	.	kIx.	.
</s>
<s>
Velikonoční	velikonoční	k2eAgNnSc1d1	velikonoční
koledování	koledování	k1gNnSc1	koledování
nabírá	nabírat	k5eAaImIp3nS	nabírat
krajově	krajově	k6eAd1	krajově
rozličných	rozličný	k2eAgFnPc2d1	rozličná
podob	podoba	k1gFnPc2	podoba
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
pomlázce	pomlázka	k1gFnSc6	pomlázka
se	se	k3xPyFc4	se
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
již	již	k6eAd1	již
pražský	pražský	k2eAgMnSc1d1	pražský
kazatel	kazatel	k1gMnSc1	kazatel
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Konrád	Konrád	k1gMnSc1	Konrád
Waldhauser	Waldhauser	k1gMnSc1	Waldhauser
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Výpočet	výpočet	k1gInSc1	výpočet
data	datum	k1gNnSc2	datum
Velikonoc	Velikonoce	k1gFnPc2	Velikonoce
</s>
</p>
<p>
<s>
pomlázka	pomlázka	k1gFnSc1	pomlázka
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Velikonoční	velikonoční	k2eAgFnSc2d1	velikonoční
pondělí	pondělí	k1gNnSc4	pondělí
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
Velikonoční	velikonoční	k2eAgNnSc4d1	velikonoční
pondělí	pondělí	k1gNnSc4	pondělí
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Postup	postup	k1gInSc1	postup
pletení	pletení	k1gNnSc2	pletení
velikonoční	velikonoční	k2eAgFnSc2d1	velikonoční
pomlázky	pomlázka	k1gFnSc2	pomlázka
krok	krok	k1gInSc4	krok
za	za	k7c7	za
krokem	krok	k1gInSc7	krok
</s>
</p>
<p>
<s>
Velikonoční	velikonoční	k2eAgFnSc1d1	velikonoční
kuchyně	kuchyně	k1gFnSc1	kuchyně
našich	náš	k3xOp1gFnPc2	náš
babiček	babička	k1gFnPc2	babička
</s>
</p>
<p>
<s>
Lidové	lidový	k2eAgFnPc1d1	lidová
tradice	tradice	k1gFnPc1	tradice
Velikonoc	Velikonoce	k1gFnPc2	Velikonoce
</s>
</p>
<p>
<s>
Videonávod	Videonávod	k1gInSc1	Videonávod
<g/>
:	:	kIx,	:
jak	jak	k8xS	jak
správně	správně	k6eAd1	správně
uplést	uplést	k5eAaPmF	uplést
pomlázku	pomlázka	k1gFnSc4	pomlázka
</s>
</p>
