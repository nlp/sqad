<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Rusnok	Rusnok	k1gInSc1	Rusnok
(	(	kIx(	(
<g/>
*	*	kIx~	*
16	[number]	k4	16
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1960	[number]	k4	1960
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ekonom	ekonom	k1gMnSc1	ekonom
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
od	od	k7c2	od
června	červen	k1gInSc2	červen
2013	[number]	k4	2013
do	do	k7c2	do
ledna	leden	k1gInSc2	leden
2014	[number]	k4	2014
předseda	předseda	k1gMnSc1	předseda
tzv.	tzv.	kA	tzv.
úřednické	úřednický	k2eAgFnSc2d1	úřednická
vlády	vláda	k1gFnSc2	vláda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
člen	člen	k1gMnSc1	člen
bankovní	bankovní	k2eAgFnSc2d1	bankovní
rady	rada	k1gFnSc2	rada
České	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
banky	banka	k1gFnSc2	banka
<g/>
,	,	kIx,	,
od	od	k7c2	od
července	červenec	k1gInSc2	červenec
2016	[number]	k4	2016
její	její	k3xOp3gNnSc1	její
guvernér	guvernér	k1gMnSc1	guvernér
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rusnok	Rusnok	k1gInSc1	Rusnok
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
dubna	duben	k1gInSc2	duben
2001	[number]	k4	2001
posledním	poslední	k2eAgMnSc6d1	poslední
ze	z	k7c2	z
tří	tři	k4xCgMnPc2	tři
ministrů	ministr	k1gMnPc2	ministr
financí	finance	k1gFnPc2	finance
sociálně	sociálně	k6eAd1	sociálně
demokratické	demokratický	k2eAgFnSc2d1	demokratická
vlády	vláda	k1gFnSc2	vláda
Miloše	Miloš	k1gMnSc2	Miloš
Zemana	Zeman	k1gMnSc2	Zeman
<g/>
,	,	kIx,	,
v	v	k7c6	v
následující	následující	k2eAgFnSc6d1	následující
koaliční	koaliční	k2eAgFnSc6d1	koaliční
vládě	vláda	k1gFnSc6	vláda
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Špidly	Špidla	k1gMnSc2	Špidla
byl	být	k5eAaImAgMnS	být
do	do	k7c2	do
března	březen	k1gInSc2	březen
2003	[number]	k4	2003
ministrem	ministr	k1gMnSc7	ministr
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
obchodu	obchod	k1gInSc2	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
konfliktu	konflikt	k1gInSc6	konflikt
se	s	k7c7	s
Špidlou	Špidla	k1gMnSc7	Špidla
a	a	k8xC	a
odchodu	odchod	k1gInSc6	odchod
z	z	k7c2	z
politiky	politika	k1gFnSc2	politika
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
ING	ing	kA	ing
Penzijním	penzijní	k2eAgInSc6d1	penzijní
fondu	fond	k1gInSc6	fond
a	a	k8xC	a
různých	různý	k2eAgNnPc6d1	různé
oborových	oborový	k2eAgNnPc6d1	oborové
sdruženích	sdružení	k1gNnPc6	sdružení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
úterý	úterý	k1gNnSc6	úterý
25	[number]	k4	25
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2013	[number]	k4	2013
jej	on	k3xPp3gInSc4	on
prezident	prezident	k1gMnSc1	prezident
Zeman	Zeman	k1gMnSc1	Zeman
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
premiérem	premiér	k1gMnSc7	premiér
a	a	k8xC	a
pověřil	pověřit	k5eAaPmAgMnS	pověřit
sestavením	sestavení	k1gNnSc7	sestavení
"	"	kIx"	"
<g/>
vlády	vláda	k1gFnSc2	vláda
odborníků	odborník	k1gMnPc2	odborník
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
Jiřího	Jiří	k1gMnSc2	Jiří
Rusnoka	Rusnoek	k1gMnSc2	Rusnoek
složila	složit	k5eAaPmAgFnS	složit
slib	slib	k1gInSc4	slib
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
prezidenta	prezident	k1gMnSc2	prezident
10	[number]	k4	10
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
a	a	k8xC	a
7	[number]	k4	7
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
nezískala	získat	k5eNaPmAgFnS	získat
důvěru	důvěra	k1gFnSc4	důvěra
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
93	[number]	k4	93
<g/>
:	:	kIx,	:
<g/>
100	[number]	k4	100
hlasů	hlas	k1gInPc2	hlas
<g/>
;	;	kIx,	;
zůstala	zůstat	k5eAaPmAgFnS	zůstat
však	však	k9	však
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
rekordní	rekordní	k2eAgFnSc4d1	rekordní
dobu	doba	k1gFnSc4	doba
až	až	k9	až
do	do	k7c2	do
jmenování	jmenování	k1gNnSc2	jmenování
vlády	vláda	k1gFnSc2	vláda
Bohuslava	Bohuslav	k1gMnSc2	Bohuslav
Sobotky	Sobotka	k1gMnSc2	Sobotka
29	[number]	k4	29
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biografie	biografie	k1gFnSc2	biografie
==	==	k?	==
</s>
</p>
<p>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
Gymnázium	gymnázium	k1gNnSc1	gymnázium
v	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
Těšíně	Těšín	k1gInSc6	Těšín
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
Národohospodářskou	národohospodářský	k2eAgFnSc4d1	Národohospodářská
fakultu	fakulta	k1gFnSc4	fakulta
VŠE	všechen	k3xTgNnSc1	všechen
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
do	do	k7c2	do
Státní	státní	k2eAgFnSc2d1	státní
plánovací	plánovací	k2eAgFnSc2d1	plánovací
komise	komise	k1gFnSc2	komise
jako	jako	k8xC	jako
referent	referent	k1gMnSc1	referent
v	v	k7c6	v
odboru	odbor	k1gInSc6	odbor
dlouhodobých	dlouhodobý	k2eAgInPc2d1	dlouhodobý
výhledů	výhled	k1gInPc2	výhled
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c6	na
Federálním	federální	k2eAgNnSc6d1	federální
ministerstvu	ministerstvo	k1gNnSc6	ministerstvo
pro	pro	k7c4	pro
strategické	strategický	k2eAgNnSc4d1	strategické
plánování	plánování	k1gNnSc4	plánování
jako	jako	k8xC	jako
vedoucí	vedoucí	k2eAgNnSc4d1	vedoucí
oddělení	oddělení	k1gNnSc4	oddělení
sociální	sociální	k2eAgFnSc2d1	sociální
strategie	strategie	k1gFnSc2	strategie
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1989	[number]	k4	1989
byl	být	k5eAaImAgMnS	být
kandidátem	kandidát	k1gMnSc7	kandidát
členství	členství	k1gNnSc2	členství
v	v	k7c6	v
KSČ	KSČ	kA	KSČ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1992	[number]	k4	1992
<g/>
–	–	k?	–
<g/>
1998	[number]	k4	1998
byl	být	k5eAaImAgInS	být
poradcem	poradce	k1gMnSc7	poradce
a	a	k8xC	a
vedoucím	vedoucí	k1gMnSc7	vedoucí
sociálně-ekonomického	sociálněkonomický	k2eAgNnSc2d1	sociálně-ekonomické
oddělení	oddělení	k1gNnSc2	oddělení
Českomoravské	českomoravský	k2eAgFnSc2d1	Českomoravská
konfederace	konfederace	k1gFnSc2	konfederace
odborových	odborový	k2eAgInPc2d1	odborový
svazů	svaz	k1gInPc2	svaz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1998	[number]	k4	1998
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
ČSSD	ČSSD	kA	ČSSD
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1998	[number]	k4	1998
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
náměstkem	náměstek	k1gMnSc7	náměstek
ministra	ministr	k1gMnSc2	ministr
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
sociálních	sociální	k2eAgFnPc2d1	sociální
věcí	věc	k1gFnPc2	věc
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Špidly	Špidla	k1gMnSc2	Špidla
ve	v	k7c6	v
vládě	vláda	k1gFnSc6	vláda
Miloše	Miloš	k1gMnSc2	Miloš
Zemana	Zeman	k1gMnSc2	Zeman
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
komunálních	komunální	k2eAgFnPc6d1	komunální
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1998	[number]	k4	1998
za	za	k7c4	za
ČSSD	ČSSD	kA	ČSSD
neúspěšně	úspěšně	k6eNd1	úspěšně
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
do	do	k7c2	do
zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
Praha	Praha	k1gFnSc1	Praha
18	[number]	k4	18
<g/>
;	;	kIx,	;
jako	jako	k8xS	jako
profesi	profese	k1gFnSc4	profese
měl	mít	k5eAaImAgInS	mít
uvedenou	uvedený	k2eAgFnSc4d1	uvedená
"	"	kIx"	"
<g/>
ekonom	ekonom	k1gMnSc1	ekonom
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k8xS	co
druhý	druhý	k4xOgMnSc1	druhý
z	z	k7c2	z
ministrů	ministr	k1gMnPc2	ministr
financí	finance	k1gFnPc2	finance
Zemanovy	Zemanův	k2eAgFnSc2d1	Zemanova
vlády	vláda	k1gFnSc2	vláda
Pavel	Pavel	k1gMnSc1	Pavel
Mertlík	Mertlík	k1gInSc1	Mertlík
kvůli	kvůli	k7c3	kvůli
sporům	spor	k1gInPc3	spor
s	s	k7c7	s
premiérem	premiér	k1gMnSc7	premiér
a	a	k8xC	a
zbytkem	zbytek	k1gInSc7	zbytek
vlády	vláda	k1gFnSc2	vláda
odstoupil	odstoupit	k5eAaPmAgInS	odstoupit
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
Rusnok	Rusnok	k1gInSc1	Rusnok
13	[number]	k4	13
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2001	[number]	k4	2001
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2002	[number]	k4	2002
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgInS	zvolit
poslancem	poslanec	k1gMnSc7	poslanec
jako	jako	k8xC	jako
lídr	lídr	k1gMnSc1	lídr
kandidátky	kandidátka	k1gFnSc2	kandidátka
ČSSD	ČSSD	kA	ČSSD
v	v	k7c6	v
Královéhradeckém	královéhradecký	k2eAgInSc6d1	královéhradecký
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nové	nový	k2eAgFnSc6d1	nová
Špidlově	Špidlův	k2eAgFnSc6d1	Špidlova
vládě	vláda	k1gFnSc6	vláda
se	se	k3xPyFc4	se
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2002	[number]	k4	2002
stal	stát	k5eAaPmAgMnS	stát
ministrem	ministr	k1gMnSc7	ministr
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
obchodu	obchod	k1gInSc2	obchod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
13	[number]	k4	13
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2003	[number]	k4	2003
premiér	premiér	k1gMnSc1	premiér
Vladimír	Vladimír	k1gMnSc1	Vladimír
Špidla	Špidla	k1gMnSc1	Špidla
Rusnoka	Rusnoka	k1gMnSc1	Rusnoka
odvolal	odvolat	k5eAaPmAgMnS	odvolat
z	z	k7c2	z
vlády	vláda	k1gFnSc2	vláda
s	s	k7c7	s
odůvodněním	odůvodnění	k1gNnSc7	odůvodnění
špatné	špatný	k2eAgFnSc2d1	špatná
komunikace	komunikace	k1gFnSc2	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
sjezdu	sjezd	k1gInSc6	sjezd
ČSSD	ČSSD	kA	ČSSD
29	[number]	k4	29
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
Rusnok	Rusnok	k1gInSc1	Rusnok
kandidoval	kandidovat	k5eAaImAgInS	kandidovat
na	na	k7c4	na
předsedu	předseda	k1gMnSc4	předseda
jako	jako	k8xS	jako
představitel	představitel	k1gMnSc1	představitel
pragmatického	pragmatický	k2eAgNnSc2d1	pragmatické
křídla	křídlo	k1gNnSc2	křídlo
<g/>
;	;	kIx,	;
Špidla	Špidla	k1gMnSc1	Špidla
ale	ale	k8xC	ale
předsednictví	předsednictví	k1gNnSc4	předsednictví
obhájil	obhájit	k5eAaPmAgMnS	obhájit
299	[number]	k4	299
hlasy	hlas	k1gInPc4	hlas
proti	proti	k7c3	proti
147	[number]	k4	147
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
předtím	předtím	k6eAd1	předtím
Rusnok	Rusnok	k1gInSc1	Rusnok
otevřeně	otevřeně	k6eAd1	otevřeně
podpořil	podpořit	k5eAaPmAgInS	podpořit
v	v	k7c6	v
prezidentské	prezidentský	k2eAgFnSc6d1	prezidentská
volbě	volba	k1gFnSc6	volba
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
oficiální	oficiální	k2eAgFnSc2d1	oficiální
linie	linie	k1gFnSc2	linie
ČSSD	ČSSD	kA	ČSSD
<g/>
)	)	kIx)	)
Václava	Václav	k1gMnSc2	Václav
Klause	Klaus	k1gMnSc2	Klaus
<g/>
.	.	kIx.	.
</s>
<s>
Podporoval	podporovat	k5eAaImAgMnS	podporovat
rovněž	rovněž	k9	rovněž
velkou	velký	k2eAgFnSc4d1	velká
koalici	koalice	k1gFnSc4	koalice
ČSSD	ČSSD	kA	ČSSD
a	a	k8xC	a
ODS	ODS	kA	ODS
<g/>
.	.	kIx.	.
23	[number]	k4	23
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
Rusnok	Rusnok	k1gInSc1	Rusnok
složil	složit	k5eAaPmAgInS	složit
poslanecký	poslanecký	k2eAgInSc4d1	poslanecký
mandát	mandát	k1gInSc4	mandát
<g/>
;	;	kIx,	;
nahradil	nahradit	k5eAaPmAgMnS	nahradit
jej	on	k3xPp3gMnSc4	on
první	první	k4xOgMnSc1	první
náhradník	náhradník	k1gMnSc1	náhradník
kandidátky	kandidátka	k1gFnSc2	kandidátka
Robin	robin	k2eAgMnSc1d1	robin
Böhnisch	Böhnisch	k1gMnSc1	Böhnisch
<g/>
.	.	kIx.	.
<g/>
Kritický	kritický	k2eAgInSc1d1	kritický
postoj	postoj	k1gInSc1	postoj
vůči	vůči	k7c3	vůči
hlavnímu	hlavní	k2eAgInSc3d1	hlavní
proudu	proud	k1gInSc3	proud
sociální	sociální	k2eAgFnSc2d1	sociální
demokracie	demokracie	k1gFnSc2	demokracie
si	se	k3xPyFc3	se
Rusnok	Rusnok	k1gInSc1	Rusnok
zachoval	zachovat	k5eAaPmAgInS	zachovat
i	i	k9	i
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2006	[number]	k4	2006
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
mluvilo	mluvit	k5eAaImAgNnS	mluvit
o	o	k7c4	o
jeho	jeho	k3xOp3gNnSc4	jeho
vyloučení	vyloučení	k1gNnSc4	vyloučení
z	z	k7c2	z
ČSSD	ČSSD	kA	ČSSD
<g/>
,	,	kIx,	,
když	když	k8xS	když
přijal	přijmout	k5eAaPmAgInS	přijmout
nabídku	nabídka	k1gFnSc4	nabídka
ministra	ministr	k1gMnSc2	ministr
financí	finance	k1gFnPc2	finance
menšinové	menšinový	k2eAgFnSc2d1	menšinová
vlády	vláda	k1gFnSc2	vláda
ODS	ODS	kA	ODS
Vlastimila	Vlastimil	k1gMnSc2	Vlastimil
Tlustého	tlustý	k2eAgMnSc2d1	tlustý
a	a	k8xC	a
zasedl	zasednout	k5eAaPmAgMnS	zasednout
do	do	k7c2	do
jeho	jeho	k3xOp3gFnSc2	jeho
expertní	expertní	k2eAgFnSc2d1	expertní
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Předseda	předseda	k1gMnSc1	předseda
Jiří	Jiří	k1gMnSc1	Jiří
Paroubek	Paroubka	k1gFnPc2	Paroubka
tehdy	tehdy	k6eAd1	tehdy
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Rusnok	Rusnok	k1gInSc1	Rusnok
myšlenkově	myšlenkově	k6eAd1	myšlenkově
již	již	k6eAd1	již
dávno	dávno	k6eAd1	dávno
se	s	k7c7	s
sociální	sociální	k2eAgFnSc7d1	sociální
demokracií	demokracie	k1gFnSc7	demokracie
rozešel	rozejít	k5eAaPmAgMnS	rozejít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2008	[number]	k4	2008
se	se	k3xPyFc4	se
před	před	k7c7	před
očekávanou	očekávaný	k2eAgFnSc7d1	očekávaná
rekonstrukcí	rekonstrukce	k1gFnSc7	rekonstrukce
koaliční	koaliční	k2eAgFnSc2d1	koaliční
vlády	vláda	k1gFnSc2	vláda
Mirka	Mirek	k1gMnSc2	Mirek
Topolánka	Topolánek	k1gMnSc2	Topolánek
objevily	objevit	k5eAaPmAgFnP	objevit
zprávy	zpráva	k1gFnPc1	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
Rusnok	Rusnok	k1gInSc1	Rusnok
(	(	kIx(	(
<g/>
ač	ač	k8xS	ač
stále	stále	k6eAd1	stále
člen	člen	k1gMnSc1	člen
ČSSD	ČSSD	kA	ČSSD
<g/>
)	)	kIx)	)
mohl	moct	k5eAaImAgMnS	moct
stát	stát	k5eAaImF	stát
vicepremiérem	vicepremiér	k1gMnSc7	vicepremiér
pro	pro	k7c4	pro
ekonomiku	ekonomika	k1gFnSc4	ekonomika
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
Rusnok	Rusnok	k1gInSc1	Rusnok
opustil	opustit	k5eAaPmAgInS	opustit
politiku	politika	k1gFnSc4	politika
a	a	k8xC	a
pracoval	pracovat	k5eAaImAgMnS	pracovat
ve	v	k7c6	v
finančnictví	finančnictví	k1gNnSc6	finančnictví
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
poradce	poradce	k1gMnSc2	poradce
Výkonné	výkonný	k2eAgFnSc2d1	výkonná
rady	rada	k1gFnSc2	rada
ING	ing	kA	ing
pro	pro	k7c4	pro
Českou	český	k2eAgFnSc4d1	Česká
a	a	k8xC	a
Slovenskou	slovenský	k2eAgFnSc4d1	slovenská
republiku	republika	k1gFnSc4	republika
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
7	[number]	k4	7
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2003	[number]	k4	2003
byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
dozorčí	dozorčí	k2eAgFnSc2d1	dozorčí
rady	rada	k1gFnSc2	rada
ING	ing	kA	ing
Penzijní	penzijní	k2eAgInSc1d1	penzijní
fond	fond	k1gInSc1	fond
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
(	(	kIx(	(
<g/>
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2013	[number]	k4	2013
ING	ing	kA	ing
Penzijní	penzijní	k2eAgFnSc1d1	penzijní
společnost	společnost	k1gFnSc1	společnost
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2006	[number]	k4	2006
povýšil	povýšit	k5eAaPmAgInS	povýšit
na	na	k7c4	na
předsedu	předseda	k1gMnSc4	předseda
představenstva	představenstvo	k1gNnSc2	představenstvo
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
byl	být	k5eAaImAgMnS	být
znovuzvolen	znovuzvolit	k5eAaPmNgMnS	znovuzvolit
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2011	[number]	k4	2011
<g/>
;	;	kIx,	;
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
ke	k	k7c3	k
30	[number]	k4	30
<g/>
.	.	kIx.	.
červnu	červen	k1gInSc3	červen
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
2005	[number]	k4	2005
<g/>
–	–	k?	–
<g/>
12	[number]	k4	12
byl	být	k5eAaImAgMnS	být
prezidentem	prezident	k1gMnSc7	prezident
Asociace	asociace	k1gFnSc2	asociace
penzijních	penzijní	k2eAgInPc2d1	penzijní
fondů	fond	k1gInPc2	fond
(	(	kIx(	(
<g/>
předtím	předtím	k6eAd1	předtím
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
její	její	k3xOp3gInPc4	její
viceprezident	viceprezident	k1gMnSc1	viceprezident
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
2006	[number]	k4	2006
<g/>
–	–	k?	–
<g/>
2009	[number]	k4	2009
byl	být	k5eAaImAgMnS	být
předsedou	předseda	k1gMnSc7	předseda
Výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
finanční	finanční	k2eAgInSc4d1	finanční
trh	trh	k1gInSc4	trh
<g/>
,	,	kIx,	,
poradního	poradní	k2eAgInSc2d1	poradní
orgánu	orgán	k1gInSc2	orgán
bankovní	bankovní	k2eAgFnSc2d1	bankovní
rady	rada	k1gFnSc2	rada
ČNB	ČNB	kA	ČNB
<g/>
;	;	kIx,	;
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
prezidentem	prezident	k1gMnSc7	prezident
Rady	rada	k1gFnSc2	rada
pro	pro	k7c4	pro
veřejný	veřejný	k2eAgInSc4d1	veřejný
dohled	dohled	k1gInSc4	dohled
nad	nad	k7c7	nad
auditem	audit	k1gInSc7	audit
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
Národní	národní	k2eAgFnSc2d1	národní
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
rady	rada	k1gFnSc2	rada
vlády	vláda	k1gFnSc2	vláda
ČR	ČR	kA	ČR
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
–	–	k?	–
<g/>
13	[number]	k4	13
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2010	[number]	k4	2010
Rusnok	Rusnok	k1gInSc1	Rusnok
vystoupil	vystoupit	k5eAaPmAgInS	vystoupit
z	z	k7c2	z
ČSSD	ČSSD	kA	ČSSD
<g/>
;	;	kIx,	;
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
vedl	vést	k5eAaImAgMnS	vést
nesouhlas	nesouhlas	k1gInSc4	nesouhlas
s	s	k7c7	s
populismem	populismus	k1gInSc7	populismus
a	a	k8xC	a
rozdávačností	rozdávačnost	k1gFnSc7	rozdávačnost
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
na	na	k7c4	na
starostu	starosta	k1gMnSc4	starosta
Prahy-Vinoře	Prahy-Vinoř	k1gFnSc2	Prahy-Vinoř
za	za	k7c7	za
SPOZ	SPOZ	kA	SPOZ
jako	jako	k8xS	jako
nestraník	nestraník	k1gMnSc1	nestraník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2011	[number]	k4	2011
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
za	za	k7c4	za
tuto	tento	k3xDgFnSc4	tento
politickou	politický	k2eAgFnSc4d1	politická
stranu	strana	k1gFnSc4	strana
v	v	k7c6	v
médiích	médium	k1gNnPc6	médium
<g/>
.	.	kIx.	.
</s>
<s>
Předseda	předseda	k1gMnSc1	předseda
Zemanovců	zemanovec	k1gMnPc2	zemanovec
však	však	k8xC	však
po	po	k7c6	po
Rusnokově	Rusnokův	k2eAgNnSc6d1	Rusnokovo
designování	designování	k1gNnSc6	designování
premiérem	premiér	k1gMnSc7	premiér
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
členem	člen	k1gMnSc7	člen
"	"	kIx"	"
<g/>
ani	ani	k8xC	ani
ekonomickým	ekonomický	k2eAgMnSc7d1	ekonomický
expertem	expert	k1gMnSc7	expert
strany	strana	k1gFnSc2	strana
[	[	kIx(	[
<g/>
...	...	k?	...
<g/>
]	]	kIx)	]
v	v	k7c6	v
žádném	žádný	k3yNgInSc6	žádný
vztahu	vztah	k1gInSc6	vztah
se	s	k7c7	s
SPOZ	SPOZ	kA	SPOZ
není	být	k5eNaImIp3nS	být
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
==	==	k?	==
Premiér	premiér	k1gMnSc1	premiér
a	a	k8xC	a
působení	působení	k1gNnSc1	působení
v	v	k7c6	v
ČNB	ČNB	kA	ČNB
==	==	k?	==
</s>
</p>
<p>
<s>
Před	před	k7c7	před
prezidentskými	prezidentský	k2eAgFnPc7d1	prezidentská
volbami	volba	k1gFnPc7	volba
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2013	[number]	k4	2013
Rusnok	Rusnok	k1gInSc1	Rusnok
podpořil	podpořit	k5eAaPmAgInS	podpořit
Miloše	Miloš	k1gMnSc4	Miloš
Zemana	Zeman	k1gMnSc4	Zeman
<g/>
;	;	kIx,	;
po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
vítězství	vítězství	k1gNnSc6	vítězství
jej	on	k3xPp3gMnSc4	on
navštívil	navštívit	k5eAaPmAgMnS	navštívit
<g/>
,	,	kIx,	,
přislíbil	přislíbit	k5eAaPmAgMnS	přislíbit
stát	stát	k5eAaImF	stát
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnSc7	jeho
ekonomickým	ekonomický	k2eAgMnSc7d1	ekonomický
poradcem	poradce	k1gMnSc7	poradce
a	a	k8xC	a
naznačil	naznačit	k5eAaPmAgInS	naznačit
i	i	k9	i
ochotu	ochota	k1gFnSc4	ochota
přijmout	přijmout	k5eAaPmF	přijmout
nominaci	nominace	k1gFnSc4	nominace
do	do	k7c2	do
bankovní	bankovní	k2eAgFnSc2d1	bankovní
rady	rada	k1gFnSc2	rada
ČNB	ČNB	kA	ČNB
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
po	po	k7c6	po
červnové	červnový	k2eAgFnSc6d1	červnová
demisi	demise	k1gFnSc6	demise
premiéra	premiér	k1gMnSc2	premiér
Nečase	Nečas	k1gMnSc2	Nečas
Zeman	Zeman	k1gMnSc1	Zeman
dal	dát	k5eAaPmAgMnS	dát
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
úřednickou	úřednický	k2eAgFnSc4d1	úřednická
vládu	vláda	k1gFnSc4	vláda
<g/>
,	,	kIx,	,
Rusnok	Rusnok	k1gInSc4	Rusnok
byl	být	k5eAaImAgMnS	být
hlavní	hlavní	k2eAgMnSc1d1	hlavní
očekávaný	očekávaný	k2eAgMnSc1d1	očekávaný
kandidát	kandidát	k1gMnSc1	kandidát
<g/>
;	;	kIx,	;
v	v	k7c6	v
úterý	úterý	k1gNnSc6	úterý
25	[number]	k4	25
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
odpoledne	odpoledne	k6eAd1	odpoledne
to	ten	k3xDgNnSc4	ten
Zeman	Zeman	k1gMnSc1	Zeman
oficiálně	oficiálně	k6eAd1	oficiálně
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
a	a	k8xC	a
rovnou	rovnou	k6eAd1	rovnou
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
Rusnoka	Rusnoka	k1gMnSc1	Rusnoka
premiérem	premiér	k1gMnSc7	premiér
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
pověřil	pověřit	k5eAaPmAgInS	pověřit
sestavením	sestavení	k1gNnSc7	sestavení
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
<g/>
Další	další	k2eAgInSc4d1	další
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
Rusnok	Rusnok	k1gInSc1	Rusnok
v	v	k7c6	v
Kramářově	kramářův	k2eAgFnSc6d1	Kramářova
vile	vila	k1gFnSc6	vila
jednal	jednat	k5eAaImAgMnS	jednat
s	s	k7c7	s
kandidáty	kandidát	k1gMnPc7	kandidát
na	na	k7c4	na
ministry	ministr	k1gMnPc4	ministr
<g/>
;	;	kIx,	;
to	ten	k3xDgNnSc1	ten
šlo	jít	k5eAaImAgNnS	jít
bez	bez	k7c2	bez
obtíží	obtíž	k1gFnPc2	obtíž
a	a	k8xC	a
celou	celý	k2eAgFnSc4d1	celá
vládu	vláda	k1gFnSc4	vláda
Zeman	Zeman	k1gMnSc1	Zeman
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
10	[number]	k4	10
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středu	střed	k1gInSc6	střed
7	[number]	k4	7
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
vláda	vláda	k1gFnSc1	vláda
Jiřího	Jiří	k1gMnSc2	Jiří
Rusnoka	Rusnoek	k1gMnSc2	Rusnoek
nezískala	získat	k5eNaPmAgFnS	získat
důvěru	důvěra	k1gFnSc4	důvěra
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
důvěru	důvěra	k1gFnSc4	důvěra
bylo	být	k5eAaImAgNnS	být
93	[number]	k4	93
poslanců	poslanec	k1gMnPc2	poslanec
(	(	kIx(	(
<g/>
KSČM	KSČM	kA	KSČM
<g/>
,	,	kIx,	,
VV	VV	kA	VV
<g/>
,	,	kIx,	,
na	na	k7c6	na
poslední	poslední	k2eAgFnSc6d1	poslední
chvíli	chvíle	k1gFnSc6	chvíle
i	i	k9	i
ČSSD	ČSSD	kA	ČSSD
<g/>
)	)	kIx)	)
proti	proti	k7c3	proti
100	[number]	k4	100
(	(	kIx(	(
<g/>
bývalá	bývalý	k2eAgFnSc1d1	bývalá
koalice	koalice	k1gFnSc1	koalice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
závěr	závěr	k1gInSc4	závěr
zasedání	zasedání	k1gNnSc2	zasedání
v	v	k7c4	v
pátek	pátek	k1gInSc4	pátek
9	[number]	k4	9
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
vláda	vláda	k1gFnSc1	vláda
usnesením	usnesení	k1gNnSc7	usnesení
č.	č.	k?	č.
617	[number]	k4	617
podala	podat	k5eAaPmAgFnS	podat
demisi	demise	k1gFnSc4	demise
<g/>
;	;	kIx,	;
v	v	k7c6	v
úterý	úterý	k1gNnSc6	úterý
13	[number]	k4	13
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
ji	on	k3xPp3gFnSc4	on
Rusnok	Rusnok	k1gInSc1	Rusnok
předal	předat	k5eAaPmAgInS	předat
do	do	k7c2	do
Zemanových	Zemanových	k2eAgFnPc2d1	Zemanových
rukou	ruka	k1gFnPc2	ruka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kvůli	kvůli	k7c3	kvůli
následnému	následný	k2eAgNnSc3d1	následné
seberozpuštění	seberozpuštění	k1gNnSc3	seberozpuštění
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
a	a	k8xC	a
dlouhému	dlouhý	k2eAgNnSc3d1	dlouhé
sestavování	sestavování	k1gNnSc3	sestavování
nové	nový	k2eAgFnSc2d1	nová
vlády	vláda	k1gFnSc2	vláda
po	po	k7c6	po
předčasných	předčasný	k2eAgFnPc6d1	předčasná
volbách	volba	k1gFnPc6	volba
zůstala	zůstat	k5eAaPmAgFnS	zůstat
Rusnokova	Rusnokův	k2eAgFnSc1d1	Rusnokova
vláda	vláda	k1gFnSc1	vláda
bez	bez	k7c2	bez
důvěry	důvěra	k1gFnSc2	důvěra
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
rekordní	rekordní	k2eAgFnSc4d1	rekordní
dobu	doba	k1gFnSc4	doba
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
ledna	leden	k1gInSc2	leden
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
středu	střed	k1gInSc6	střed
5	[number]	k4	5
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2014	[number]	k4	2014
prezident	prezident	k1gMnSc1	prezident
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
Rusnoka	Rusnoek	k1gMnSc4	Rusnoek
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
členem	člen	k1gInSc7	člen
bankovní	bankovní	k2eAgFnSc2d1	bankovní
rady	rada	k1gFnSc2	rada
ČNB	ČNB	kA	ČNB
<g/>
.	.	kIx.	.
</s>
<s>
Funkce	funkce	k1gFnPc4	funkce
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgInS	ujmout
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vypršel	vypršet	k5eAaPmAgInS	vypršet
šestiletý	šestiletý	k2eAgInSc4d1	šestiletý
mandát	mandát	k1gInSc4	mandát
Evě	Eva	k1gFnSc3	Eva
Zamrazilové	Zamrazilové	k2eAgMnPc2d1	Zamrazilové
<g/>
.	.	kIx.	.
25	[number]	k4	25
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2016	[number]	k4	2016
jej	on	k3xPp3gInSc4	on
prezident	prezident	k1gMnSc1	prezident
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
guvernérem	guvernér	k1gMnSc7	guvernér
ČNB	ČNB	kA	ČNB
<g/>
.	.	kIx.	.
</s>
<s>
Funkce	funkce	k1gFnPc4	funkce
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgInS	ujmout
od	od	k7c2	od
července	červenec	k1gInSc2	červenec
a	a	k8xC	a
vystřídal	vystřídat	k5eAaPmAgMnS	vystřídat
tak	tak	k6eAd1	tak
Miroslava	Miroslav	k1gMnSc4	Miroslav
Singera	Singer	k1gMnSc4	Singer
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rodinný	rodinný	k2eAgInSc1d1	rodinný
život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Rusnok	Rusnok	k1gInSc1	Rusnok
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
rodinném	rodinný	k2eAgInSc6d1	rodinný
domku	domek	k1gInSc6	domek
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
v	v	k7c6	v
Praze-Vinoři	Praze-Vinoř	k1gFnSc6	Praze-Vinoř
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
Ivetou	Iveta	k1gFnSc7	Iveta
<g/>
,	,	kIx,	,
inženýrkou	inženýrka	k1gFnSc7	inženýrka
ekonomie	ekonomie	k1gFnSc2	ekonomie
a	a	k8xC	a
personalistkou	personalistka	k1gFnSc7	personalistka
t.	t.	k?	t.
č.	č.	k?	č.
možná	možná	k9	možná
v	v	k7c6	v
domácnosti	domácnost	k1gFnSc6	domácnost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
vyhýbá	vyhýbat	k5eAaImIp3nS	vyhýbat
médiím	médium	k1gNnPc3	médium
a	a	k8xC	a
veřejným	veřejný	k2eAgFnPc3d1	veřejná
akcím	akce	k1gFnPc3	akce
<g/>
.	.	kIx.	.
</s>
<s>
Vzali	vzít	k5eAaPmAgMnP	vzít
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
letech	let	k1gInPc6	let
1988	[number]	k4	1988
<g/>
–	–	k?	–
<g/>
9	[number]	k4	9
<g/>
;	;	kIx,	;
mají	mít	k5eAaImIp3nP	mít
tři	tři	k4xCgFnPc4	tři
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
<g/>
Rusnokův	Rusnokův	k2eAgMnSc1d1	Rusnokův
syn	syn	k1gMnSc1	syn
Michal	Michal	k1gMnSc1	Michal
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
absolvent	absolvent	k1gMnSc1	absolvent
Právnické	právnický	k2eAgFnSc2d1	právnická
fakulty	fakulta	k1gFnSc2	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Palackého	Palackého	k2eAgFnSc2d1	Palackého
žijící	žijící	k2eAgFnSc2d1	žijící
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
,	,	kIx,	,
vzbudil	vzbudit	k5eAaPmAgInS	vzbudit
koncem	koncem	k7c2	koncem
srpna	srpen	k1gInSc2	srpen
2013	[number]	k4	2013
pozornost	pozornost	k1gFnSc1	pozornost
bulvárních	bulvární	k2eAgNnPc2d1	bulvární
médií	médium	k1gNnPc2	médium
<g/>
,	,	kIx,	,
když	když	k8xS	když
na	na	k7c6	na
Facebooku	Facebook	k1gInSc6	Facebook
napsal	napsat	k5eAaBmAgMnS	napsat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
ze	z	k7c2	z
SPOZ	SPOZ	kA	SPOZ
a	a	k8xC	a
"	"	kIx"	"
<g/>
socialismus	socialismus	k1gInSc1	socialismus
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgNnSc4d3	veliký
zlo	zlo	k1gNnSc4	zlo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
straně	strana	k1gFnSc6	strana
byl	být	k5eAaImAgMnS	být
od	od	k7c2	od
ledna	leden	k1gInSc2	leden
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
člen	člen	k1gMnSc1	člen
olomoucké	olomoucký	k2eAgFnSc2d1	olomoucká
organizace	organizace	k1gFnSc2	organizace
publikoval	publikovat	k5eAaBmAgMnS	publikovat
několik	několik	k4yIc1	několik
článků	článek	k1gInPc2	článek
na	na	k7c4	na
ekonomicko-politická	ekonomickoolitický	k2eAgNnPc4d1	ekonomicko-politické
témata	téma	k1gNnPc4	téma
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
své	svůj	k3xOyFgNnSc4	svůj
vyjádření	vyjádření	k1gNnSc4	vyjádření
zmírnil	zmírnit	k5eAaPmAgMnS	zmírnit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kontroverze	kontroverze	k1gFnPc4	kontroverze
kolem	kolem	k7c2	kolem
Mandelova	Mandelův	k2eAgInSc2d1	Mandelův
pohřbu	pohřeb	k1gInSc2	pohřeb
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
úmrtí	úmrtí	k1gNnSc6	úmrtí
Nelsona	Nelson	k1gMnSc2	Nelson
Mandely	Mandela	k1gFnSc2	Mandela
počátkem	počátkem	k7c2	počátkem
prosince	prosinec	k1gInSc2	prosinec
2013	[number]	k4	2013
vedl	vést	k5eAaImAgInS	vést
Rusnok	Rusnok	k1gInSc1	Rusnok
v	v	k7c6	v
Poslanecké	poslanecký	k2eAgFnSc6d1	Poslanecká
sněmovně	sněmovna	k1gFnSc6	sněmovna
o	o	k7c6	o
přestávce	přestávka	k1gFnSc6	přestávka
jednání	jednání	k1gNnSc2	jednání
rozhovor	rozhovor	k1gInSc1	rozhovor
s	s	k7c7	s
ministrem	ministr	k1gMnSc7	ministr
obrany	obrana	k1gFnSc2	obrana
Pickem	Picek	k1gMnSc7	Picek
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vyjadřoval	vyjadřovat	k5eAaImAgMnS	vyjadřovat
nechuť	nechuť	k1gFnSc4	nechuť
letět	letět	k5eAaImF	letět
na	na	k7c4	na
pohřeb	pohřeb	k1gInSc4	pohřeb
a	a	k8xC	a
naději	naděje	k1gFnSc4	naděje
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
toho	ten	k3xDgNnSc2	ten
ujme	ujmout	k5eAaPmIp3nS	ujmout
prezident	prezident	k1gMnSc1	prezident
Zeman	Zeman	k1gMnSc1	Zeman
<g/>
,	,	kIx,	,
nekultivovanými	kultivovaný	k2eNgInPc7d1	nekultivovaný
až	až	k8xS	až
vulgárními	vulgární	k2eAgInPc7d1	vulgární
výrazy	výraz	k1gInPc7	výraz
<g/>
.	.	kIx.	.
</s>
<s>
Konverzaci	konverzace	k1gFnSc3	konverzace
zachytily	zachytit	k5eAaPmAgInP	zachytit
zapnuté	zapnutý	k2eAgInPc1d1	zapnutý
mikrofony	mikrofon	k1gInPc1	mikrofon
u	u	k7c2	u
řečnického	řečnický	k2eAgInSc2d1	řečnický
pultu	pult	k1gInSc2	pult
a	a	k8xC	a
odvysílala	odvysílat	k5eAaPmAgFnS	odvysílat
ji	on	k3xPp3gFnSc4	on
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
v	v	k7c6	v
nočním	noční	k2eAgInSc6d1	noční
záznamu	záznam	k1gInSc6	záznam
ze	z	k7c2	z
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
;	;	kIx,	;
jeden	jeden	k4xCgMnSc1	jeden
divák	divák	k1gMnSc1	divák
zveřejnil	zveřejnit	k5eAaPmAgMnS	zveřejnit
záznam	záznam	k1gInSc4	záznam
na	na	k7c4	na
YouTube	YouTub	k1gInSc5	YouTub
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
rozšířil	rozšířit	k5eAaPmAgMnS	rozšířit
a	a	k8xC	a
zpráva	zpráva	k1gFnSc1	zpráva
vzbudila	vzbudit	k5eAaPmAgFnS	vzbudit
pozornost	pozornost	k1gFnSc4	pozornost
i	i	k9	i
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Rusnok	Rusnok	k1gInSc1	Rusnok
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
za	za	k7c4	za
svá	svůj	k3xOyFgNnPc4	svůj
slova	slovo	k1gNnPc4	slovo
omluvil	omluvit	k5eAaPmAgMnS	omluvit
s	s	k7c7	s
odkazem	odkaz	k1gInSc7	odkaz
na	na	k7c4	na
časově	časově	k6eAd1	časově
náročný	náročný	k2eAgInSc4d1	náročný
pracovní	pracovní	k2eAgInSc4d1	pracovní
program	program	k1gInSc4	program
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Vláda	vláda	k1gFnSc1	vláda
Jiřího	Jiří	k1gMnSc2	Jiří
Rusnoka	Rusnoek	k1gMnSc2	Rusnoek
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Jiří	Jiří	k1gMnPc1	Jiří
Rusnok	Rusnok	k1gInSc4	Rusnok
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Rusnok	Rusnok	k1gInSc1	Rusnok
<g/>
,	,	kIx,	,
blog	blog	k1gInSc1	blog
na	na	k7c4	na
Aktuálně	aktuálně	k6eAd1	aktuálně
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Rusnok	Rusnok	k1gInSc1	Rusnok
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Rusnok	Rusnok	k1gInSc1	Rusnok
<g/>
:	:	kIx,	:
Intervence	intervence	k1gFnPc4	intervence
nepřeceňujme	přeceňovat	k5eNaImRp1nP	přeceňovat
<g/>
,	,	kIx,	,
samy	sám	k3xTgFnPc1	sám
zázrak	zázrak	k1gInSc4	zázrak
nezmůžou	zmoct	k5eNaPmIp3nPwO	zmoct
–	–	k?	–
videorozhovor	videorozhovor	k1gInSc4	videorozhovor
na	na	k7c6	na
portálu	portál	k1gInSc6	portál
Roklen	Roklna	k1gFnPc2	Roklna
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
