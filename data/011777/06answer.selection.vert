<s>
Rusnok	Rusnok	k1gInSc1	Rusnok
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
dubna	duben	k1gInSc2	duben
2001	[number]	k4	2001
posledním	poslední	k2eAgMnSc6d1	poslední
ze	z	k7c2	z
tří	tři	k4xCgMnPc2	tři
ministrů	ministr	k1gMnPc2	ministr
financí	finance	k1gFnPc2	finance
sociálně	sociálně	k6eAd1	sociálně
demokratické	demokratický	k2eAgFnSc2d1	demokratická
vlády	vláda	k1gFnSc2	vláda
Miloše	Miloš	k1gMnSc2	Miloš
Zemana	Zeman	k1gMnSc2	Zeman
<g/>
,	,	kIx,	,
v	v	k7c6	v
následující	následující	k2eAgFnSc6d1	následující
koaliční	koaliční	k2eAgFnSc6d1	koaliční
vládě	vláda	k1gFnSc6	vláda
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Špidly	Špidla	k1gMnSc2	Špidla
byl	být	k5eAaImAgMnS	být
do	do	k7c2	do
března	březen	k1gInSc2	březen
2003	[number]	k4	2003
ministrem	ministr	k1gMnSc7	ministr
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
obchodu	obchod	k1gInSc2	obchod
<g/>
.	.	kIx.	.
</s>
