<s>
Naše	náš	k3xOp1gFnSc1	náš
řeč	řeč	k1gFnSc1	řeč
je	být	k5eAaImIp3nS	být
český	český	k2eAgInSc1d1	český
jazykovědný	jazykovědný	k2eAgInSc1d1	jazykovědný
časopis	časopis	k1gInSc1	časopis
vydávaný	vydávaný	k2eAgInSc1d1	vydávaný
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1916	[number]	k4	1916
pětkrát	pětkrát	k6eAd1	pětkrát
ročně	ročně	k6eAd1	ročně
Ústavem	ústav	k1gInSc7	ústav
pro	pro	k7c4	pro
jazyk	jazyk	k1gInSc4	jazyk
český	český	k2eAgInSc4d1	český
Akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
