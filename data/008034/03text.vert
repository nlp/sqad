<s>
Naše	náš	k3xOp1gFnSc1	náš
řeč	řeč	k1gFnSc1	řeč
je	být	k5eAaImIp3nS	být
český	český	k2eAgInSc1d1	český
jazykovědný	jazykovědný	k2eAgInSc1d1	jazykovědný
časopis	časopis	k1gInSc1	časopis
vydávaný	vydávaný	k2eAgInSc1d1	vydávaný
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1916	[number]	k4	1916
pětkrát	pětkrát	k6eAd1	pětkrát
ročně	ročně	k6eAd1	ročně
Ústavem	ústav	k1gInSc7	ústav
pro	pro	k7c4	pro
jazyk	jazyk	k1gInSc4	jazyk
český	český	k2eAgInSc4d1	český
Akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
publikovány	publikovat	k5eAaBmNgInP	publikovat
články	článek	k1gInPc1	článek
o	o	k7c6	o
českém	český	k2eAgInSc6d1	český
jazyce	jazyk	k1gInSc6	jazyk
<g/>
,	,	kIx,	,
se	s	k7c7	s
zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
jazykovou	jazykový	k2eAgFnSc4d1	jazyková
kulturu	kultura	k1gFnSc4	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
je	být	k5eAaImIp3nS	být
vedoucí	vedoucí	k2eAgFnSc7d1	vedoucí
redaktorkou	redaktorka	k1gFnSc7	redaktorka
časopisu	časopis	k1gInSc2	časopis
Markéta	Markéta	k1gFnSc1	Markéta
Pravdová	Pravdová	k1gFnSc1	Pravdová
a	a	k8xC	a
výkonným	výkonný	k2eAgMnSc7d1	výkonný
redaktorem	redaktor	k1gMnSc7	redaktor
Ondřej	Ondřej	k1gMnSc1	Ondřej
Dufek	Dufek	k1gMnSc1	Dufek
<g/>
.	.	kIx.	.
</s>
