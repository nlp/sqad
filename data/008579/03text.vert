<p>
<s>
Mincecore	Mincecor	k1gMnSc5	Mincecor
je	být	k5eAaImIp3nS	být
hudební	hudební	k2eAgInSc4d1	hudební
žánr	žánr	k1gInSc4	žánr
stojící	stojící	k2eAgInSc4d1	stojící
na	na	k7c6	na
základech	základ	k1gInPc6	základ
grindcore	grindcor	k1gInSc5	grindcor
se	s	k7c7	s
sociálně-politickými	sociálněolitický	k2eAgInPc7d1	sociálně-politický
texty	text	k1gInPc7	text
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
mincecore	mincecor	k1gInSc5	mincecor
poprvé	poprvé	k6eAd1	poprvé
použila	použít	k5eAaPmAgFnS	použít
belgická	belgický	k2eAgFnSc1d1	belgická
hudební	hudební	k2eAgFnSc1d1	hudební
skupina	skupina	k1gFnSc1	skupina
Agathocles	Agathoclesa	k1gFnPc2	Agathoclesa
<g/>
.	.	kIx.	.
<g/>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
syrovou	syrový	k2eAgFnSc4d1	syrová
a	a	k8xC	a
minimalistickou	minimalistický	k2eAgFnSc4d1	minimalistická
formu	forma	k1gFnSc4	forma
grindcore	grindcor	k1gInSc5	grindcor
<g/>
.	.	kIx.	.
</s>
<s>
Texty	text	k1gInPc1	text
písní	píseň	k1gFnPc2	píseň
pojednávají	pojednávat	k5eAaImIp3nP	pojednávat
o	o	k7c6	o
sociálních	sociální	k2eAgInPc6d1	sociální
a	a	k8xC	a
politických	politický	k2eAgInPc6d1	politický
problémech	problém	k1gInPc6	problém
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
píseň	píseň	k1gFnSc1	píseň
zpravidla	zpravidla	k6eAd1	zpravidla
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgMnSc1	jeden
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
dva	dva	k4xCgInPc4	dva
akordy	akord	k1gInPc4	akord
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
se	se	k3xPyFc4	se
za	za	k7c4	za
mincecore	mincecor	k1gInSc5	mincecor
považovali	považovat	k5eAaImAgMnP	považovat
Malignant	Malignant	k1gMnSc1	Malignant
Tumour	Tumour	k1gMnSc1	Tumour
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
období	období	k1gNnSc6	období
1997	[number]	k4	1997
<g/>
–	–	k?	–
<g/>
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příklady	příklad	k1gInPc1	příklad
mincecorových	mincecorův	k2eAgFnPc2d1	mincecorův
kapel	kapela	k1gFnPc2	kapela
==	==	k?	==
</s>
</p>
<p>
<s>
Agathocles	Agathocles	k1gMnSc1	Agathocles
</s>
</p>
<p>
<s>
Malignant	Malignant	k1gMnSc1	Malignant
Tumour	Tumour	k1gMnSc1	Tumour
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Unholy	Unhola	k1gFnPc1	Unhola
Grave	grave	k6eAd1	grave
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
