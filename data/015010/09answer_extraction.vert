Byla	být	k5eAaImAgFnS
určená	určený	k2eAgFnSc1d1
pro	pro	k7c4
nezaměstnané	nezaměstnaný	k1gMnPc4
a	a	k8xC
sociálně	sociálně	k6eAd1
slabé	slabý	k2eAgMnPc4d1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
ji	on	k3xPp3gFnSc4
dostávali	dostávat	k5eAaImAgMnP
od	od	k7c2
státu	stát	k1gInSc2
místo	místo	k7c2
peněz	peníze	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc4,k3yRgInPc4,k3yIgInPc4
by	by	kYmCp3nP
mohli	moct	k5eAaImAgMnP
promrhat	promrhat	k5eAaPmF
<g/>
.	.	kIx.
