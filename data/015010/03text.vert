<s>
Žebračenka	žebračenka	k1gFnSc1
</s>
<s>
Žebračenky	žebračenka	k1gFnPc1
</s>
<s>
Žebračenka	žebračenka	k1gFnSc1
nebo	nebo	k8xC
almuženka	almuženka	k1gFnSc1
nebo	nebo	k8xC
almužní	almužní	k2eAgFnSc1d1
poukázka	poukázka	k1gFnSc1
byla	být	k5eAaImAgFnS
v	v	k7c6
Československu	Československo	k1gNnSc6
od	od	k7c2
roku	rok	k1gInSc2
1927	#num#	k4
poukázka	poukázka	k1gFnSc1
na	na	k7c4
stravu	strava	k1gFnSc4
či	či	k8xC
oděv	oděv	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
určená	určený	k2eAgFnSc1d1
pro	pro	k7c4
nezaměstnané	nezaměstnaný	k1gMnPc4
a	a	k8xC
sociálně	sociálně	k6eAd1
slabé	slabý	k2eAgMnPc4d1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
ji	on	k3xPp3gFnSc4
dostávali	dostávat	k5eAaImAgMnP
od	od	k7c2
státu	stát	k1gInSc2
místo	místo	k7c2
peněz	peníze	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc4,k3yRgInPc4,k3yIgInPc4
by	by	kYmCp3nP
mohli	moct	k5eAaImAgMnP
promrhat	promrhat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Termín	termín	k1gInSc1
žebračenka	žebračenka	k1gFnSc1
se	se	k3xPyFc4
začal	začít	k5eAaPmAgMnS
znovu	znovu	k6eAd1
používat	používat	k5eAaImF
i	i	k9
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
jsou	být	k5eAaImIp3nP
takto	takto	k6eAd1
slangově	slangově	k6eAd1
nazývány	nazývat	k5eAaImNgFnP
poukázky	poukázka	k1gFnPc1
na	na	k7c4
nakoupení	nakoupení	k1gNnSc4
základních	základní	k2eAgFnPc2d1
potravin	potravina	k1gFnPc2
a	a	k8xC
hygienických	hygienický	k2eAgFnPc2d1
potřeb	potřeba	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
dostávají	dostávat	k5eAaImIp3nP
sociálně	sociálně	k6eAd1
potřební	potřební	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Charakteristika	charakteristika	k1gFnSc1
</s>
<s>
V	v	k7c6
užším	úzký	k2eAgInSc6d2
smyslu	smysl	k1gInSc6
se	se	k3xPyFc4
jako	jako	k9
„	„	k?
<g/>
žebračenky	žebračenka	k1gFnPc1
<g/>
“	“	k?
označují	označovat	k5eAaImIp3nP
zejména	zejména	k9
potravinové	potravinový	k2eAgFnPc1d1
poukázky	poukázka	k1gFnPc1
přidělováné	přidělováný	k2eAgFnPc1d1
v	v	k7c6
Československu	Československo	k1gNnSc6
nezaměstnaným	nezaměstnaný	k1gMnSc7
v	v	k7c6
letech	let	k1gInPc6
1930	#num#	k4
<g/>
–	–	k?
<g/>
1939	#num#	k4
(	(	kIx(
<g/>
v	v	k7c6
důsledku	důsledek	k1gInSc6
světové	světový	k2eAgFnSc2d1
hospodářské	hospodářský	k2eAgFnSc2d1
krize	krize	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stát	stát	k1gInSc1
přiděloval	přidělovat	k5eAaImAgInS
poukázky	poukázka	k1gFnSc2
na	na	k7c6
podporu	podpor	k1gInSc6
pro	pro	k7c4
nezaměstnané	nezaměstnaný	k1gMnPc4
v	v	k7c6
rámci	rámec	k1gInSc6
státní	státní	k2eAgFnSc2d1
stravovací	stravovací	k2eAgFnSc2d1
akce	akce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
srpnu	srpen	k1gInSc6
1930	#num#	k4
Ministerstvo	ministerstvo	k1gNnSc1
sociální	sociální	k2eAgFnSc2d1
péče	péče	k1gFnSc2
nezaměstnaným	nezaměstnaný	k1gMnPc3
týdně	týdně	k6eAd1
přidělovalo	přidělovat	k5eAaImAgNnS
poukázky	poukázka	k1gFnSc2
v	v	k7c6
hodnotě	hodnota	k1gFnSc6
10	#num#	k4
až	až	k9
20	#num#	k4
Kč	Kč	kA
<g/>
,	,	kIx,
za	za	k7c4
které	který	k3yQgFnPc4,k3yIgFnPc4,k3yRgFnPc4
si	se	k3xPyFc3
mohli	moct	k5eAaImAgMnP
jejich	jejich	k3xOp3gMnPc1
držitelé	držitel	k1gMnPc1
v	v	k7c6
určitých	určitý	k2eAgInPc6d1
dnech	den	k1gInPc6
vyměnit	vyměnit	k5eAaPmF
potraviny	potravina	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gNnSc1
přidělování	přidělování	k1gNnSc1
se	se	k3xPyFc4
řídilo	řídit	k5eAaImAgNnS
určitými	určitý	k2eAgNnPc7d1
pravidly	pravidlo	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poukázky	poukázka	k1gFnSc2
nedostávali	dostávat	k5eNaImAgMnP
ti	ten	k3xDgMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
od	od	k7c2
roku	rok	k1gInSc2
1929	#num#	k4
nebyli	být	k5eNaImAgMnP
nejméně	málo	k6eAd3
3	#num#	k4
měsíce	měsíc	k1gInSc2
nepřetržitě	přetržitě	k6eNd1
zaměstnáni	zaměstnán	k2eAgMnPc1d1
(	(	kIx(
<g/>
sezónní	sezónní	k2eAgMnPc1d1
dělníci	dělník	k1gMnPc1
až	až	k9
6	#num#	k4
měsíců	měsíc	k1gInPc2
<g/>
)	)	kIx)
<g/>
;	;	kIx,
dále	daleko	k6eAd2
ti	ten	k3xDgMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
dostávali	dostávat	k5eAaImAgMnP
jakoukoliv	jakýkoliv	k3yIgFnSc4
jinou	jiný	k2eAgFnSc4d1
<g/>
,	,	kIx,
byť	byť	k8xS
i	i	k9
velmi	velmi	k6eAd1
malou	malý	k2eAgFnSc4d1
podporu	podpora	k1gFnSc4
<g/>
,	,	kIx,
byli	být	k5eAaImAgMnP
vlastníky	vlastník	k1gMnPc7
domku	domek	k1gInSc2
<g/>
,	,	kIx,
nepřijali	přijmout	k5eNaPmAgMnP
práci	práce	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
jim	on	k3xPp3gMnPc3
nabídla	nabídnout	k5eAaPmAgFnS
zprostředkovatelna	zprostředkovatelna	k1gFnSc1
práce	práce	k1gFnSc2
nebo	nebo	k8xC
nějaký	nějaký	k3yIgInSc1
úřad	úřad	k1gInSc1
<g/>
,	,	kIx,
a	a	k8xC
podobně	podobně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
začátku	začátek	k1gInSc2
se	se	k3xPyFc4
přidělovaly	přidělovat	k5eAaImAgFnP
poukázky	poukázka	k1gFnPc1
pouze	pouze	k6eAd1
na	na	k7c4
2	#num#	k4
měsíce	měsíc	k1gInPc4
<g/>
,	,	kIx,
postupně	postupně	k6eAd1
se	se	k3xPyFc4
však	však	k9
čas	čas	k1gInSc1
prodlužoval	prodlužovat	k5eAaImAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
přidělování	přidělování	k1gNnSc6
rozhodovaly	rozhodovat	k5eAaImAgInP
okresní	okresní	k2eAgInPc1d1
a	a	k8xC
obecní	obecní	k2eAgFnSc1d1
sociální	sociální	k2eAgFnSc1d1
komise	komise	k1gFnSc1
<g/>
,	,	kIx,
okresní	okresní	k2eAgInPc1d1
a	a	k8xC
notářské	notářský	k2eAgInPc1d1
úřady	úřad	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ministerstvo	ministerstvo	k1gNnSc1
sociální	sociální	k2eAgFnSc2d1
péče	péče	k1gFnSc2
poskytovalo	poskytovat	k5eAaImAgNnS
poukázky	poukázka	k1gFnPc4
podle	podle	k7c2
objemu	objem	k1gInSc2
financí	finance	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
mělo	mít	k5eAaImAgNnS
k	k	k7c3
dispozici	dispozice	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
roce	rok	k1gInSc6
1933	#num#	k4
odpracovávali	odpracovávat	k5eAaImAgMnP
nezaměstnaní	nezaměstnaný	k1gMnPc1
jako	jako	k9
protihodnotu	protihodnota	k1gFnSc4
za	za	k7c4
žebračenky	žebračenka	k1gFnPc4
bezplatně	bezplatně	k6eAd1
určitý	určitý	k2eAgInSc4d1
počet	počet	k1gInSc4
pracovních	pracovní	k2eAgInPc2d1
dnů	den	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Práci	práce	k1gFnSc6
zadávali	zadávat	k5eAaImAgMnP
notáři	notár	k1gMnPc1
<g/>
,	,	kIx,
starostové	starosta	k1gMnPc1
<g/>
,	,	kIx,
různé	různý	k2eAgInPc1d1
úřady	úřad	k1gInPc1
a	a	k8xC
jejím	její	k3xOp3gNnSc7
odmítnutím	odmítnutí	k1gNnSc7
skončilo	skončit	k5eAaPmAgNnS
i	i	k9
přidělování	přidělování	k1gNnSc1
přídělů	příděl	k1gInPc2
potravin	potravina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyskytovaly	vyskytovat	k5eAaImAgInP
se	se	k3xPyFc4
i	i	k8xC
případy	případ	k1gInPc1
zneužívání	zneužívání	k1gNnSc1
<g/>
,	,	kIx,
defraudace	defraudace	k1gFnPc1
a	a	k8xC
spekulace	spekulace	k1gFnPc1
s	s	k7c7
poukázkami	poukázka	k1gFnPc7
ze	z	k7c2
strany	strana	k1gFnSc2
úředníků	úředník	k1gMnPc2
pro	pro	k7c4
jejich	jejich	k3xOp3gNnSc4
vlastní	vlastní	k2eAgNnSc4d1
obohacení	obohacení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Žobračenka	Žobračenka	k1gFnSc1
na	na	k7c6
slovenské	slovenský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Potravinové	potravinový	k2eAgInPc1d1
lístky	lístek	k1gInPc1
</s>
<s>
sKarta	sKarta	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Žebračenky	žebračenka	k1gFnPc1
budou	být	k5eAaImBp3nP
pro	pro	k7c4
všechny	všechen	k3xTgInPc4
chudé	chudý	k2eAgInPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bez	bez	k7c2
rozdílu	rozdíl	k1gInSc2
</s>
