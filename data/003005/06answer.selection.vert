<s>
Barevné	barevný	k2eAgNnSc4d1	barevné
vidění	vidění	k1gNnSc4	vidění
lidského	lidský	k2eAgNnSc2d1	lidské
oka	oko	k1gNnSc2	oko
zprostředkují	zprostředkovat	k5eAaPmIp3nP	zprostředkovat
receptory	receptor	k1gInPc1	receptor
zvané	zvaný	k2eAgInPc1d1	zvaný
čípky	čípek	k1gInPc1	čípek
trojího	trojí	k4xRgMnSc2	trojí
druhu	druh	k1gInSc2	druh
-	-	kIx~	-
citlivé	citlivý	k2eAgNnSc1d1	citlivé
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
základní	základní	k2eAgFnPc4d1	základní
barvy	barva	k1gFnPc4	barva
<g/>
:	:	kIx,	:
červenou	červený	k2eAgFnSc4d1	červená
<g/>
,	,	kIx,	,
zelenou	zelený	k2eAgFnSc4d1	zelená
a	a	k8xC	a
modrou	modrý	k2eAgFnSc4d1	modrá
(	(	kIx(	(
<g/>
anglická	anglický	k2eAgFnSc1d1	anglická
zkratka	zkratka	k1gFnSc1	zkratka
RGB	RGB	kA	RGB
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
