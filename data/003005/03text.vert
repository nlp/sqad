<s>
Barva	barva	k1gFnSc1	barva
je	být	k5eAaImIp3nS	být
vlastnost	vlastnost	k1gFnSc1	vlastnost
světla	světlo	k1gNnSc2	světlo
resp.	resp.	kA	resp.
látky	látka	k1gFnSc2	látka
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yIgNnSc1	který
toto	tento	k3xDgNnSc4	tento
světlo	světlo	k1gNnSc4	světlo
vychází	vycházet	k5eAaImIp3nS	vycházet
<g/>
.	.	kIx.	.
</s>
<s>
Vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
vjem	vjem	k1gInSc4	vjem
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
vytvářen	vytvářit	k5eAaPmNgInS	vytvářit
na	na	k7c4	na
sítnici	sítnice	k1gFnSc4	sítnice
oka	oko	k1gNnSc2	oko
viditelným	viditelný	k2eAgNnSc7d1	viditelné
elektromagnetickým	elektromagnetický	k2eAgNnSc7d1	elektromagnetické
zářením	záření	k1gNnSc7	záření
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
vyzářeno	vyzářit	k5eAaPmNgNnS	vyzářit
pozorovanou	pozorovaný	k2eAgFnSc7d1	pozorovaná
látkou	látka	k1gFnSc7	látka
<g/>
,	,	kIx,	,
odrazilo	odrazit	k5eAaPmAgNnS	odrazit
se	se	k3xPyFc4	se
od	od	k7c2	od
ní	on	k3xPp3gFnSc2	on
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
jí	on	k3xPp3gFnSc7	on
prošlo	projít	k5eAaPmAgNnS	projít
(	(	kIx(	(
<g/>
v	v	k7c6	v
případě	případ	k1gInSc6	případ
průhledných	průhledný	k2eAgInPc2d1	průhledný
materiálů	materiál	k1gInPc2	materiál
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
některé	některý	k3yIgFnPc4	některý
kapaliny	kapalina	k1gFnPc4	kapalina
<g/>
,	,	kIx,	,
plyny	plyn	k1gInPc4	plyn
nebo	nebo	k8xC	nebo
sklo	sklo	k1gNnSc4	sklo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Barevný	barevný	k2eAgInSc1d1	barevný
vjem	vjem	k1gInSc1	vjem
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
spektrálním	spektrální	k2eAgNnSc6d1	spektrální
složení	složení	k1gNnSc6	složení
přicházejícího	přicházející	k2eAgNnSc2d1	přicházející
světla	světlo	k1gNnSc2	světlo
(	(	kIx(	(
<g/>
závislosti	závislost	k1gFnSc2	závislost
světelného	světelný	k2eAgInSc2d1	světelný
toku	tok	k1gInSc2	tok
na	na	k7c6	na
frekvenci	frekvence	k1gFnSc6	frekvence
či	či	k8xC	či
vlnové	vlnový	k2eAgFnSc3d1	vlnová
délce	délka	k1gFnSc3	délka
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc3	jeho
intenzitě	intenzita	k1gFnSc3	intenzita
vzhledem	vzhledem	k7c3	vzhledem
pozadí	pozadí	k1gNnSc3	pozadí
<g/>
.	.	kIx.	.
</s>
<s>
Barevné	barevný	k2eAgNnSc4d1	barevné
vidění	vidění	k1gNnSc4	vidění
lidského	lidský	k2eAgNnSc2d1	lidské
oka	oko	k1gNnSc2	oko
zprostředkují	zprostředkovat	k5eAaPmIp3nP	zprostředkovat
receptory	receptor	k1gInPc1	receptor
zvané	zvaný	k2eAgInPc1d1	zvaný
čípky	čípek	k1gInPc1	čípek
trojího	trojí	k4xRgMnSc2	trojí
druhu	druh	k1gInSc2	druh
-	-	kIx~	-
citlivé	citlivý	k2eAgNnSc1d1	citlivé
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
základní	základní	k2eAgFnPc4d1	základní
barvy	barva	k1gFnPc4	barva
<g/>
:	:	kIx,	:
červenou	červený	k2eAgFnSc4d1	červená
<g/>
,	,	kIx,	,
zelenou	zelený	k2eAgFnSc4d1	zelená
a	a	k8xC	a
modrou	modrý	k2eAgFnSc4d1	modrá
(	(	kIx(	(
<g/>
anglická	anglický	k2eAgFnSc1d1	anglická
zkratka	zkratka	k1gFnSc1	zkratka
RGB	RGB	kA	RGB
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
živočichové	živočich	k1gMnPc1	živočich
se	s	k7c7	s
čtyřmi	čtyři	k4xCgFnPc7	čtyři
nebo	nebo	k8xC	nebo
jen	jen	k6eAd1	jen
dvěma	dva	k4xCgInPc7	dva
typy	typ	k1gInPc7	typ
čípků	čípek	k1gInPc2	čípek
v	v	k7c6	v
sítnici	sítnice	k1gFnSc6	sítnice
<g/>
.	.	kIx.	.
</s>
<s>
Tabulka	tabulka	k1gFnSc1	tabulka
uvádí	uvádět	k5eAaImIp3nS	uvádět
barevné	barevný	k2eAgNnSc4d1	barevné
spektrum	spektrum	k1gNnSc4	spektrum
světla	světlo	k1gNnSc2	světlo
rozdělené	rozdělená	k1gFnSc2	rozdělená
podle	podle	k7c2	podle
barev	barva	k1gFnPc2	barva
<g/>
,	,	kIx,	,
odpovídající	odpovídající	k2eAgFnSc2d1	odpovídající
vlnové	vlnový	k2eAgFnSc2d1	vlnová
délky	délka	k1gFnSc2	délka
a	a	k8xC	a
frekvence	frekvence	k1gFnSc2	frekvence
monochromatické	monochromatický	k2eAgNnSc4d1	monochromatické
záření	záření	k1gNnSc4	záření
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
hranicemi	hranice	k1gFnPc7	hranice
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
červené	červená	k1gFnSc2	červená
resp.	resp.	kA	resp.
fialové	fialový	k2eAgFnSc2d1	fialová
barvy	barva	k1gFnSc2	barva
leží	ležet	k5eAaImIp3nP	ležet
pásma	pásmo	k1gNnPc1	pásmo
infračerveného	infračervený	k2eAgNnSc2d1	infračervené
a	a	k8xC	a
ultrafialového	ultrafialový	k2eAgNnSc2d1	ultrafialové
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
již	již	k6eAd1	již
lidské	lidský	k2eAgNnSc1d1	lidské
oko	oko	k1gNnSc1	oko
nevnímá	vnímat	k5eNaImIp3nS	vnímat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jsou	být	k5eAaImIp3nP	být
ho	on	k3xPp3gNnSc4	on
schopni	schopen	k2eAgMnPc1d1	schopen
vnímat	vnímat	k5eAaImF	vnímat
někteří	některý	k3yIgMnPc1	některý
jiní	jiný	k2eAgMnPc1d1	jiný
živočichové	živočich	k1gMnPc1	živočich
vybavení	vybavený	k2eAgMnPc1d1	vybavený
čípky	čípek	k1gInPc4	čípek
příslušného	příslušný	k2eAgInSc2d1	příslušný
rozsahu	rozsah	k1gInSc2	rozsah
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
možné	možný	k2eAgFnPc1d1	možná
barvy	barva	k1gFnPc1	barva
či	či	k8xC	či
odstíny	odstín	k1gInPc1	odstín
vznikají	vznikat	k5eAaImIp3nP	vznikat
skládáním	skládání	k1gNnPc3	skládání
základních	základní	k2eAgInPc2d1	základní
(	(	kIx(	(
<g/>
spektrálních	spektrální	k2eAgFnPc2d1	spektrální
<g/>
)	)	kIx)	)
barev	barva	k1gFnPc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k8xC	tak
např.	např.	kA	např.
pozorujeme	pozorovat	k5eAaImIp1nP	pozorovat
bílou	bílý	k2eAgFnSc4d1	bílá
barvu	barva	k1gFnSc4	barva
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
dopadající	dopadající	k2eAgNnSc4d1	dopadající
záření	záření	k1gNnSc4	záření
vnímají	vnímat	k5eAaImIp3nP	vnímat
všechny	všechen	k3xTgInPc1	všechen
tři	tři	k4xCgInPc1	tři
druhy	druh	k1gInPc1	druh
čípků	čípek	k1gInPc2	čípek
<g/>
,	,	kIx,	,
a	a	k8xC	a
černou	černá	k1gFnSc4	černá
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
záření	záření	k1gNnPc1	záření
nevnímají	vnímat	k5eNaImIp3nP	vnímat
žádné	žádný	k3yNgFnPc4	žádný
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Barevný	barevný	k2eAgInSc4d1	barevný
vjem	vjem	k1gInSc4	vjem
světla	světlo	k1gNnSc2	světlo
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
odlišný	odlišný	k2eAgInSc4d1	odlišný
vzhledem	vzhled	k1gInSc7	vzhled
ke	k	k7c3	k
světlému	světlý	k2eAgNnSc3d1	světlé
nebo	nebo	k8xC	nebo
tmavému	tmavý	k2eAgNnSc3d1	tmavé
pozadí	pozadí	k1gNnSc3	pozadí
či	či	k8xC	či
okolí	okolí	k1gNnSc3	okolí
předmětu	předmět	k1gInSc2	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
barva	barva	k1gFnSc1	barva
vnímaná	vnímaný	k2eAgFnSc1d1	vnímaná
jako	jako	k8xS	jako
žlutá	žlutý	k2eAgFnSc1d1	žlutá
či	či	k8xC	či
oranžová	oranžový	k2eAgFnSc1d1	oranžová
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
relativně	relativně	k6eAd1	relativně
tmavému	tmavý	k2eAgNnSc3d1	tmavé
pozadí	pozadí	k1gNnSc3	pozadí
<g/>
/	/	kIx~	/
<g/>
okolí	okolí	k1gNnSc4	okolí
se	s	k7c7	s
vzhledem	vzhled	k1gInSc7	vzhled
ke	k	k7c3	k
světlému	světlý	k2eAgInSc3d1	světlý
(	(	kIx(	(
<g/>
jasnějšímu	jasný	k2eAgMnSc3d2	jasnější
<g/>
)	)	kIx)	)
pozadí	pozadí	k1gNnSc3	pozadí
<g/>
/	/	kIx~	/
<g/>
okolí	okolí	k1gNnSc3	okolí
jeví	jevit	k5eAaImIp3nP	jevit
jako	jako	k8xS	jako
hnědá	hnědat	k5eAaImIp3nS	hnědat
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
daný	daný	k2eAgInSc4d1	daný
světelný	světelný	k2eAgInSc4d1	světelný
tok	tok	k1gInSc4	tok
je	být	k5eAaImIp3nS	být
stejný	stejný	k2eAgInSc1d1	stejný
<g/>
.	.	kIx.	.
</s>
<s>
Barva	barva	k1gFnSc1	barva
objektu	objekt	k1gInSc2	objekt
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
jeho	jeho	k3xOp3gFnPc6	jeho
fyzikálních	fyzikální	k2eAgFnPc6d1	fyzikální
vlastnostech	vlastnost	k1gFnPc6	vlastnost
a	a	k8xC	a
na	na	k7c6	na
vnímání	vnímání	k1gNnSc6	vnímání
pozorovatele	pozorovatel	k1gMnSc2	pozorovatel
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
fyzikálního	fyzikální	k2eAgNnSc2d1	fyzikální
můžeme	moct	k5eAaImIp1nP	moct
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
povrch	povrch	k1gInSc1	povrch
má	mít	k5eAaImIp3nS	mít
barvu	barva	k1gFnSc4	barva
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
odráží	odrážet	k5eAaImIp3nS	odrážet
nebo	nebo	k8xC	nebo
vyzařuje	vyzařovat	k5eAaImIp3nS	vyzařovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
odrazu	odraz	k1gInSc2	odraz
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c4	na
složení	složení	k1gNnSc4	složení
spektra	spektrum	k1gNnSc2	spektrum
dopadajícího	dopadající	k2eAgNnSc2d1	dopadající
světla	světlo	k1gNnSc2	světlo
a	a	k8xC	a
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
které	který	k3yIgFnSc6	který
složky	složka	k1gFnPc1	složka
spektra	spektrum	k1gNnSc2	spektrum
tohoto	tento	k3xDgNnSc2	tento
světla	světlo	k1gNnSc2	světlo
povrch	povrch	k6eAd1wR	povrch
odráží	odrážet	k5eAaImIp3nS	odrážet
a	a	k8xC	a
které	který	k3yRgNnSc1	který
pohlcuje	pohlcovat	k5eAaImIp3nS	pohlcovat
a	a	k8xC	a
s	s	k7c7	s
jakou	jaký	k3yIgFnSc7	jaký
intenzitou	intenzita	k1gFnSc7	intenzita
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
úhlu	úhel	k1gInSc6	úhel
pozorování	pozorování	k1gNnSc2	pozorování
objektu	objekt	k1gInSc2	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Vnímání	vnímání	k1gNnSc1	vnímání
barev	barva	k1gFnPc2	barva
je	být	k5eAaImIp3nS	být
subjektivní	subjektivní	k2eAgFnSc1d1	subjektivní
záležitost	záležitost	k1gFnSc1	záležitost
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
přechody	přechod	k1gInPc1	přechod
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
barvami	barva	k1gFnPc7	barva
barevného	barevný	k2eAgNnSc2d1	barevné
spektra	spektrum	k1gNnSc2	spektrum
jsou	být	k5eAaImIp3nP	být
plynulé	plynulý	k2eAgFnPc1d1	plynulá
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
studie	studie	k1gFnSc2	studie
Berlina	berlina	k1gFnSc1	berlina
a	a	k8xC	a
Kaye	Kaye	k1gFnSc1	Kaye
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
každý	každý	k3xTgInSc1	každý
jazyk	jazyk	k1gInSc1	jazyk
na	na	k7c6	na
světě	svět	k1gInSc6	svět
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
2	[number]	k4	2
-	-	kIx~	-
12	[number]	k4	12
základních	základní	k2eAgFnPc2d1	základní
barev	barva	k1gFnPc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnPc1d1	základní
barvy	barva	k1gFnPc1	barva
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
z	z	k7c2	z
jednoho	jeden	k4xCgNnSc2	jeden
slova	slovo	k1gNnSc2	slovo
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
ne	ne	k9	ne
např.	např.	kA	např.
světle	světle	k6eAd1	světle
zelená	zelený	k2eAgFnSc1d1	zelená
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
užívají	užívat	k5eAaImIp3nP	užívat
se	se	k3xPyFc4	se
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
frekvencí	frekvence	k1gFnSc7	frekvence
a	a	k8xC	a
o	o	k7c4	o
jejich	jejich	k3xOp3gNnSc4	jejich
užívání	užívání	k1gNnSc4	užívání
panuje	panovat	k5eAaImIp3nS	panovat
mezi	mezi	k7c7	mezi
mluvčími	mluvčí	k1gMnPc7	mluvčí
jazyka	jazyk	k1gInSc2	jazyk
shoda	shoda	k1gFnSc1	shoda
<g/>
.	.	kIx.	.
</s>
<s>
Jazyky	jazyk	k1gInPc1	jazyk
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
mezi	mezi	k7c7	mezi
pouze	pouze	k6eAd1	pouze
dvěma	dva	k4xCgFnPc7	dva
barvami	barva	k1gFnPc7	barva
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
nejprve	nejprve	k6eAd1	nejprve
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
mezi	mezi	k7c7	mezi
tmavou	tmavý	k2eAgFnSc7d1	tmavá
barvou	barva	k1gFnSc7	barva
a	a	k8xC	a
světlou	světlý	k2eAgFnSc7d1	světlá
barvou	barva	k1gFnSc7	barva
<g/>
.	.	kIx.	.
</s>
<s>
Posléze	posléze	k6eAd1	posléze
se	se	k3xPyFc4	se
k	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
vždy	vždy	k6eAd1	vždy
přidává	přidávat	k5eAaImIp3nS	přidávat
červená	červený	k2eAgFnSc1d1	červená
-	-	kIx~	-
tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
tři	tři	k4xCgFnPc1	tři
nejzákladnější	základní	k2eAgFnPc1d3	nejzákladnější
barvy	barva	k1gFnPc1	barva
jsou	být	k5eAaImIp3nP	být
černá	černý	k2eAgFnSc1d1	černá
<g/>
,	,	kIx,	,
bílá	bílý	k2eAgFnSc1d1	bílá
a	a	k8xC	a
červená	červený	k2eAgFnSc1d1	červená
<g/>
.	.	kIx.	.
</s>
<s>
Následuje	následovat	k5eAaImIp3nS	následovat
zelená	zelený	k2eAgFnSc1d1	zelená
nebo	nebo	k8xC	nebo
žlutá	žlutý	k2eAgFnSc1d1	žlutá
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
druhá	druhý	k4xOgFnSc1	druhý
z	z	k7c2	z
dvojice	dvojice	k1gFnSc2	dvojice
těchto	tento	k3xDgFnPc2	tento
barev	barva	k1gFnPc2	barva
<g/>
,	,	kIx,	,
a	a	k8xC	a
následně	následně	k6eAd1	následně
modrá	modrý	k2eAgFnSc1d1	modrá
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
jazyky	jazyk	k1gInPc1	jazyk
se	s	k7c7	s
šesti	šest	k4xCc7	šest
barvami	barva	k1gFnPc7	barva
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
černou	černá	k1gFnSc4	černá
<g/>
,	,	kIx,	,
bílou	bílý	k2eAgFnSc4d1	bílá
<g/>
,	,	kIx,	,
červenou	červený	k2eAgFnSc4d1	červená
<g/>
,	,	kIx,	,
zelenou	zelený	k2eAgFnSc4d1	zelená
<g/>
,	,	kIx,	,
žlutou	žlutý	k2eAgFnSc4d1	žlutá
a	a	k8xC	a
modrou	modrý	k2eAgFnSc4d1	modrá
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
vyvinou	vyvinout	k5eAaPmIp3nP	vyvinout
i	i	k9	i
názvy	název	k1gInPc1	název
pro	pro	k7c4	pro
ostatní	ostatní	k2eAgFnPc4d1	ostatní
barvy	barva	k1gFnPc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Čeština	čeština	k1gFnSc1	čeština
má	mít	k5eAaImIp3nS	mít
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
angličtina	angličtina	k1gFnSc1	angličtina
a	a	k8xC	a
němčina	němčina	k1gFnSc1	němčina
<g/>
,	,	kIx,	,
jedenáct	jedenáct	k4xCc1	jedenáct
základních	základní	k2eAgFnPc2d1	základní
barev	barva	k1gFnPc2	barva
<g/>
:	:	kIx,	:
černou	černá	k1gFnSc4	černá
<g/>
,	,	kIx,	,
bílou	bílý	k2eAgFnSc4d1	bílá
<g/>
,	,	kIx,	,
červenou	červený	k2eAgFnSc4d1	červená
<g/>
,	,	kIx,	,
zelenou	zelený	k2eAgFnSc4d1	zelená
<g/>
,	,	kIx,	,
žlutou	žlutý	k2eAgFnSc4d1	žlutá
<g/>
,	,	kIx,	,
modrou	modrý	k2eAgFnSc4d1	modrá
<g/>
,	,	kIx,	,
hnědou	hnědý	k2eAgFnSc4d1	hnědá
<g/>
,	,	kIx,	,
šedou	šedý	k2eAgFnSc4d1	šedá
<g/>
,	,	kIx,	,
oranžovou	oranžový	k2eAgFnSc4d1	oranžová
<g/>
,	,	kIx,	,
růžovou	růžový	k2eAgFnSc4d1	růžová
a	a	k8xC	a
fialovou	fialový	k2eAgFnSc4d1	fialová
<g/>
.	.	kIx.	.
</s>
<s>
Ruština	ruština	k1gFnSc1	ruština
a	a	k8xC	a
italština	italština	k1gFnSc1	italština
mají	mít	k5eAaImIp3nP	mít
dvanáct	dvanáct	k4xCc4	dvanáct
barev	barva	k1gFnPc2	barva
-	-	kIx~	-
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
mezi	mezi	k7c4	mezi
světle	světle	k6eAd1	světle
modrou	modrý	k2eAgFnSc4d1	modrá
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
goluboj	goluboj	k1gInSc1	goluboj
<g/>
)	)	kIx)	)
a	a	k8xC	a
tmavě	tmavě	k6eAd1	tmavě
modrou	modrý	k2eAgFnSc4d1	modrá
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
sinij	sinít	k5eAaPmRp2nS	sinít
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vyčlenění	vyčlenění	k1gNnSc4	vyčlenění
světle	světle	k6eAd1	světle
modré	modrý	k2eAgFnPc1d1	modrá
jakožto	jakožto	k8xS	jakožto
tyrkysové	tyrkysový	k2eAgFnPc1d1	tyrkysová
mezi	mezi	k7c4	mezi
základní	základní	k2eAgFnPc4d1	základní
barvy	barva	k1gFnPc4	barva
bylo	být	k5eAaImAgNnS	být
nedávno	nedávno	k6eAd1	nedávno
potvrzeno	potvrdit	k5eAaPmNgNnS	potvrdit
i	i	k9	i
pro	pro	k7c4	pro
angličtinu	angličtina	k1gFnSc4	angličtina
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
barvou	barva	k1gFnSc7	barva
lila	lila	k2eAgFnPc2d1	lila
(	(	kIx(	(
<g/>
světle	světle	k6eAd1	světle
fialová	fialový	k2eAgFnSc1d1	fialová
<g/>
)	)	kIx)	)
-	-	kIx~	-
podle	podle	k7c2	podle
studie	studie	k1gFnSc2	studie
z	z	k7c2	z
r.	r.	kA	r.
2015	[number]	k4	2015
tak	tak	k6eAd1	tak
má	mít	k5eAaImIp3nS	mít
současná	současný	k2eAgFnSc1d1	současná
angličtina	angličtina	k1gFnSc1	angličtina
13	[number]	k4	13
základních	základní	k2eAgFnPc2d1	základní
barev	barva	k1gFnPc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Colorterapie	Colorterapie	k1gFnPc1	Colorterapie
<g/>
,	,	kIx,	,
také	také	k9	také
jinak	jinak	k6eAd1	jinak
léčení	léčení	k1gNnSc4	léčení
pomocí	pomocí	k7c2	pomocí
barev	barva	k1gFnPc2	barva
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
technika	technika	k1gFnSc1	technika
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
využívala	využívat	k5eAaImAgFnS	využívat
již	již	k6eAd1	již
ve	v	k7c6	v
starém	starý	k2eAgInSc6d1	starý
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Barvy	barva	k1gFnPc1	barva
vyzařují	vyzařovat	k5eAaImIp3nP	vyzařovat
různé	různý	k2eAgNnSc4d1	různé
elektromagnetické	elektromagnetický	k2eAgNnSc4d1	elektromagnetické
vlnění	vlnění	k1gNnSc4	vlnění
s	s	k7c7	s
rozdílným	rozdílný	k2eAgNnSc7d1	rozdílné
působením	působení	k1gNnSc7	působení
na	na	k7c4	na
lidskou	lidský	k2eAgFnSc4d1	lidská
mysl	mysl	k1gFnSc4	mysl
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
tohoto	tento	k3xDgNnSc2	tento
působení	působení	k1gNnSc2	působení
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
při	při	k7c6	při
colorterapii	colorterapie	k1gFnSc6	colorterapie
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
této	tento	k3xDgFnSc2	tento
terapie	terapie	k1gFnSc2	terapie
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
barvě	barva	k1gFnSc6	barva
pokoje	pokoj	k1gInSc2	pokoj
<g/>
,	,	kIx,	,
oblečení	oblečení	k1gNnSc2	oblečení
či	či	k8xC	či
ložního	ložní	k2eAgNnSc2d1	ložní
povlečení	povlečení	k1gNnSc2	povlečení
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
v	v	k7c6	v
nemocnicích	nemocnice	k1gFnPc6	nemocnice
převážně	převážně	k6eAd1	převážně
zelené	zelený	k2eAgFnPc1d1	zelená
<g/>
,	,	kIx,	,
modré	modrý	k2eAgFnPc1d1	modrá
nebo	nebo	k8xC	nebo
žluté	žlutý	k2eAgFnPc1d1	žlutá
stěny	stěna	k1gFnPc1	stěna
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ty	ten	k3xDgMnPc4	ten
navozují	navozovat	k5eAaImIp3nP	navozovat
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
pocity	pocit	k1gInPc4	pocit
klidu	klid	k1gInSc2	klid
a	a	k8xC	a
míru	mír	k1gInSc2	mír
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
si	se	k3xPyFc3	se
můžeme	moct	k5eAaImIp1nP	moct
dopřát	dopřát	k5eAaPmF	dopřát
colorterapii	colorterapie	k1gFnSc4	colorterapie
v	v	k7c6	v
relaxačních	relaxační	k2eAgNnPc6d1	relaxační
centrech	centrum	k1gNnPc6	centrum
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
solné	solný	k2eAgFnPc4d1	solná
jeskyně	jeskyně	k1gFnPc4	jeskyně
<g/>
,	,	kIx,	,
sauny	sauna	k1gFnPc4	sauna
<g/>
,	,	kIx,	,
vířivky	vířivka	k1gFnPc4	vířivka
atd.	atd.	kA	atd.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
