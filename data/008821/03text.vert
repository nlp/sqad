<p>
<s>
Magelang	Magelang	k1gMnSc1	Magelang
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc4	město
na	na	k7c6	na
Jávě	Jáva	k1gFnSc6	Jáva
v	v	k7c6	v
Indonésii	Indonésie	k1gFnSc6	Indonésie
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
asi	asi	k9	asi
43	[number]	k4	43
kilometrů	kilometr	k1gInPc2	kilometr
severně	severně	k6eAd1	severně
od	od	k7c2	od
Yogyakarty	Yogyakarta	k1gFnSc2	Yogyakarta
a	a	k8xC	a
75	[number]	k4	75
kilometrů	kilometr	k1gInPc2	kilometr
od	od	k7c2	od
Semarangu	Semarang	k1gInSc2	Semarang
<g/>
,	,	kIx,	,
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
centrální	centrální	k2eAgFnSc2d1	centrální
Jávy	Jáva	k1gFnSc2	Jáva
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
největší	veliký	k2eAgNnSc1d3	veliký
město	město	k1gNnSc1	město
planiny	planina	k1gFnSc2	planina
Kedu	Kedus	k1gInSc2	Kedus
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
tradičního	tradiční	k2eAgNnSc2d1	tradiční
datování	datování	k1gNnSc2	datování
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
907	[number]	k4	907
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Magelang	Magelanga	k1gFnPc2	Magelanga
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
