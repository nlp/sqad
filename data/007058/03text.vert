<s>
Vodní	vodní	k2eAgInSc1d1	vodní
tok	tok	k1gInSc1	tok
(	(	kIx(	(
<g/>
vodoteč	vodoteč	k1gFnSc1	vodoteč
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
koryto	koryto	k1gNnSc1	koryto
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
odtéká	odtékat	k5eAaImIp3nS	odtékat
z	z	k7c2	z
povodí	povodí	k1gNnSc2	povodí
<g/>
.	.	kIx.	.
</s>
<s>
Tok	tok	k1gInSc1	tok
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
délce	délka	k1gFnSc6	délka
nebo	nebo	k8xC	nebo
v	v	k7c6	v
části	část	k1gFnSc6	část
povrchový	povrchový	k2eAgInSc4d1	povrchový
nebo	nebo	k8xC	nebo
podpovrchový	podpovrchový	k2eAgInSc4d1	podpovrchový
<g/>
,	,	kIx,	,
přirozený	přirozený	k2eAgInSc4d1	přirozený
nebo	nebo	k8xC	nebo
umělý	umělý	k2eAgInSc4d1	umělý
<g/>
.	.	kIx.	.
</s>
<s>
Tok	tok	k1gInSc1	tok
je	být	k5eAaImIp3nS	být
ohraničen	ohraničit	k5eAaPmNgInS	ohraničit
korytem	koryto	k1gNnSc7	koryto
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
dno	dno	k1gNnSc4	dno
a	a	k8xC	a
levý	levý	k2eAgInSc4d1	levý
a	a	k8xC	a
pravý	pravý	k2eAgInSc4d1	pravý
břeh	břeh	k1gInSc4	břeh
<g/>
;	;	kIx,	;
k	k	k7c3	k
rozlišení	rozlišení	k1gNnSc3	rozlišení
břehů	břeh	k1gInPc2	břeh
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
směr	směr	k1gInSc1	směr
po	po	k7c6	po
proudu	proud	k1gInSc6	proud
toku	tok	k1gInSc2	tok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
korytě	koryto	k1gNnSc6	koryto
vodního	vodní	k2eAgInSc2d1	vodní
toku	tok	k1gInSc2	tok
rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
kynetu	kyneta	k1gFnSc4	kyneta
(	(	kIx(	(
<g/>
prohloubenou	prohloubený	k2eAgFnSc4d1	prohloubená
a	a	k8xC	a
trvale	trvale	k6eAd1	trvale
zaplavovanou	zaplavovaný	k2eAgFnSc4d1	zaplavovaná
část	část	k1gFnSc4	část
koryta	koryto	k1gNnSc2	koryto
<g/>
)	)	kIx)	)
a	a	k8xC	a
bermu	berma	k1gFnSc4	berma
(	(	kIx(	(
<g/>
prostor	prostor	k1gInSc4	prostor
zaplavovaný	zaplavovaný	k2eAgInSc4d1	zaplavovaný
jen	jen	k6eAd1	jen
při	při	k7c6	při
vyšším	vysoký	k2eAgInSc6d2	vyšší
průtoku	průtok	k1gInSc6	průtok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podélná	podélný	k2eAgFnSc1d1	podélná
poloha	poloha	k1gFnSc1	poloha
na	na	k7c6	na
toku	tok	k1gInSc6	tok
se	se	k3xPyFc4	se
udává	udávat	k5eAaImIp3nS	udávat
kilometráží	kilometráž	k1gFnSc7	kilometráž
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
počítá	počítat	k5eAaImIp3nS	počítat
směrem	směr	k1gInSc7	směr
od	od	k7c2	od
ústí	ústí	k1gNnSc2	ústí
nebo	nebo	k8xC	nebo
soutoku	soutok	k1gInSc2	soutok
proti	proti	k7c3	proti
proudu	proud	k1gInSc3	proud
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jak	jak	k6eAd1	jak
k	k	k7c3	k
popisu	popis	k1gInSc3	popis
vodní	vodní	k2eAgFnSc2d1	vodní
cesty	cesta	k1gFnSc2	cesta
(	(	kIx(	(
<g/>
splavnost	splavnost	k1gFnSc1	splavnost
<g/>
,	,	kIx,	,
kotviště	kotviště	k1gNnSc1	kotviště
<g/>
,	,	kIx,	,
propustě	propustě	k6eAd1	propustě
<g/>
,	,	kIx,	,
mosty	most	k1gInPc1	most
a	a	k8xC	a
pod	pod	k7c7	pod
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
hydrologických	hydrologický	k2eAgInPc2d1	hydrologický
poměrů	poměr	k1gInPc2	poměr
(	(	kIx(	(
<g/>
průtok	průtok	k1gInSc1	průtok
<g/>
,	,	kIx,	,
orientace	orientace	k1gFnSc1	orientace
toku	tok	k1gInSc2	tok
<g/>
,	,	kIx,	,
přítoky	přítok	k1gInPc1	přítok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dílčí	dílčí	k2eAgFnSc1d1	dílčí
hydrologická	hydrologický	k2eAgFnSc1d1	hydrologická
disciplína	disciplína	k1gFnSc1	disciplína
zabývající	zabývající	k2eAgFnSc1d1	zabývající
se	se	k3xPyFc4	se
režimem	režim	k1gInSc7	režim
řek	řeka	k1gFnPc2	řeka
a	a	k8xC	a
ostatních	ostatní	k2eAgInPc2d1	ostatní
vodních	vodní	k2eAgInPc2d1	vodní
toků	tok	k1gInPc2	tok
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
potamologie	potamologie	k1gFnSc1	potamologie
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
původu	původ	k1gInSc2	původ
se	se	k3xPyFc4	se
vodní	vodní	k2eAgFnPc1d1	vodní
toky	toka	k1gFnPc1	toka
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c4	na
přirozené	přirozený	k2eAgNnSc4d1	přirozené
a	a	k8xC	a
umělé	umělý	k2eAgNnSc4d1	umělé
<g/>
.	.	kIx.	.
</s>
<s>
Umělými	umělý	k2eAgInPc7d1	umělý
vodními	vodní	k2eAgInPc7d1	vodní
toky	tok	k1gInPc7	tok
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
průtočné	průtočný	k2eAgInPc1d1	průtočný
vodní	vodní	k2eAgInPc1d1	vodní
kanály	kanál	k1gInPc1	kanál
<g/>
,	,	kIx,	,
náhony	náhon	k1gInPc1	náhon
<g/>
,	,	kIx,	,
meliorační	meliorační	k2eAgFnPc1d1	meliorační
vodoteče	vodoteč	k1gFnPc1	vodoteč
<g/>
,	,	kIx,	,
vodní	vodní	k2eAgInPc1d1	vodní
tunely	tunel	k1gInPc1	tunel
či	či	k8xC	či
akvadukty	akvadukt	k1gInPc1	akvadukt
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgFnPc1d1	mnohá
přirozené	přirozený	k2eAgFnPc1d1	přirozená
vodní	vodní	k2eAgFnPc1d1	vodní
toky	toka	k1gFnPc1	toka
jsou	být	k5eAaImIp3nP	být
regulované	regulovaný	k2eAgFnPc1d1	regulovaná
<g/>
,	,	kIx,	,
t.	t.	k?	t.
j.	j.	k?	j.
koryto	koryto	k1gNnSc1	koryto
bylo	být	k5eAaImAgNnS	být
uměle	uměle	k6eAd1	uměle
přebudováno	přebudován	k2eAgNnSc1d1	přebudováno
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
regulace	regulace	k1gFnSc2	regulace
vodních	vodní	k2eAgInPc2d1	vodní
toků	tok	k1gInPc2	tok
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
umožnění	umožnění	k1gNnSc1	umožnění
či	či	k8xC	či
zlepšení	zlepšení	k1gNnSc1	zlepšení
splavnosti	splavnost	k1gFnSc2	splavnost
<g/>
,	,	kIx,	,
protipovodňová	protipovodňový	k2eAgFnSc1d1	protipovodňová
ochrana	ochrana	k1gFnSc1	ochrana
<g/>
,	,	kIx,	,
omezení	omezení	k1gNnSc1	omezení
přirozeného	přirozený	k2eAgInSc2d1	přirozený
rozlivu	rozliv	k1gInSc2	rozliv
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
využití	využití	k1gNnSc2	využití
sousedního	sousední	k2eAgNnSc2d1	sousední
území	území	k1gNnSc2	území
nebo	nebo	k8xC	nebo
výstavba	výstavba	k1gFnSc1	výstavba
vodních	vodní	k2eAgFnPc2d1	vodní
nádrží	nádrž	k1gFnPc2	nádrž
<g/>
.	.	kIx.	.
</s>
<s>
Vodní	vodní	k2eAgFnPc1d1	vodní
toky	toka	k1gFnPc1	toka
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
velikosti	velikost	k1gFnSc2	velikost
rozdělují	rozdělovat	k5eAaImIp3nP	rozdělovat
na	na	k7c4	na
několik	několik	k4yIc4	několik
typů	typ	k1gInPc2	typ
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
hranice	hranice	k1gFnSc1	hranice
ani	ani	k8xC	ani
vztah	vztah	k1gInSc1	vztah
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
pojmy	pojem	k1gInPc7	pojem
nejsou	být	k5eNaImIp3nP	být
pevně	pevně	k6eAd1	pevně
dané	daný	k2eAgFnPc1d1	daná
a	a	k8xC	a
ani	ani	k8xC	ani
hydrologové	hydrolog	k1gMnPc1	hydrolog
je	on	k3xPp3gMnPc4	on
neužívají	užívat	k5eNaImIp3nP	užívat
jednotně	jednotně	k6eAd1	jednotně
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
toků	tok	k1gInPc2	tok
rozhodují	rozhodovat	k5eAaImIp3nP	rozhodovat
místní	místní	k2eAgFnPc4d1	místní
zvyklosti	zvyklost	k1gFnPc4	zvyklost
a	a	k8xC	a
tradice	tradice	k1gFnPc4	tradice
<g/>
.	.	kIx.	.
bystřina	bystřina	k1gFnSc1	bystřina
–	–	k?	–
malý	malý	k2eAgInSc1d1	malý
vodní	vodní	k2eAgInSc1d1	vodní
tok	tok	k1gInSc1	tok
se	s	k7c7	s
značným	značný	k2eAgInSc7d1	značný
a	a	k8xC	a
proměnlivým	proměnlivý	k2eAgInSc7d1	proměnlivý
sklonem	sklon	k1gInSc7	sklon
dna	dno	k1gNnSc2	dno
<g/>
,	,	kIx,	,
potok	potok	k1gInSc1	potok
–	–	k?	–
menší	malý	k2eAgInSc1d2	menší
vodní	vodní	k2eAgInSc1d1	vodní
tok	tok	k1gInSc1	tok
s	s	k7c7	s
vyrovnanějším	vyrovnaný	k2eAgInSc7d2	vyrovnanější
a	a	k8xC	a
mírnějším	mírný	k2eAgInSc7d2	mírnější
sklonem	sklon	k1gInSc7	sklon
<g/>
,	,	kIx,	,
říčka	říčka	k1gFnSc1	říčka
–	–	k?	–
velikostní	velikostní	k2eAgInSc4d1	velikostní
přechod	přechod	k1gInSc4	přechod
mezi	mezi	k7c7	mezi
potokem	potok	k1gInSc7	potok
a	a	k8xC	a
řekou	řeka	k1gFnSc7	řeka
<g/>
,	,	kIx,	,
řeka	řeka	k1gFnSc1	řeka
–	–	k?	–
větší	veliký	k2eAgInSc1d2	veliký
vodní	vodní	k2eAgInSc1d1	vodní
tok	tok	k1gInSc1	tok
<g/>
,	,	kIx,	,
veletok	veletok	k1gInSc1	veletok
–	–	k?	–
obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
vymezuje	vymezovat	k5eAaImIp3nS	vymezovat
jako	jako	k9	jako
řeka	řeka	k1gFnSc1	řeka
alespoň	alespoň	k9	alespoň
500	[number]	k4	500
km	km	kA	km
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
s	s	k7c7	s
plochou	plocha	k1gFnSc7	plocha
povodí	povodí	k1gNnSc2	povodí
alespoň	alespoň	k9	alespoň
100	[number]	k4	100
000	[number]	k4	000
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
průtok	průtok	k1gInSc1	průtok
–	–	k?	–
spojnice	spojnice	k1gFnSc1	spojnice
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
vodními	vodní	k2eAgInPc7d1	vodní
útvary	útvar	k1gInPc7	útvar
<g/>
.	.	kIx.	.
</s>
<s>
Bystřina	bystřina	k1gFnSc1	bystřina
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
typ	typ	k1gInSc4	typ
potoka	potok	k1gInSc2	potok
<g/>
,	,	kIx,	,
říčka	říčka	k1gFnSc1	říčka
či	či	k8xC	či
veletok	veletok	k1gInSc1	veletok
za	za	k7c4	za
typy	typ	k1gInPc4	typ
řek	řeka	k1gFnPc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
říčka	říčka	k1gFnSc1	říčka
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
ztotožňován	ztotožňován	k2eAgInSc1d1	ztotožňován
s	s	k7c7	s
pojmem	pojem	k1gInSc7	pojem
potok	potok	k1gInSc1	potok
<g/>
.	.	kIx.	.
</s>
<s>
Skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
vedle	vedle	k7c2	vedle
zdrobnělého	zdrobnělý	k2eAgNnSc2d1	zdrobnělé
označení	označení	k1gNnSc2	označení
řeky	řeka	k1gFnSc2	řeka
existuje	existovat	k5eAaImIp3nS	existovat
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
jazycích	jazyk	k1gInPc6	jazyk
pro	pro	k7c4	pro
potok	potok	k1gInSc4	potok
i	i	k8xC	i
zvláštní	zvláštní	k2eAgNnSc4d1	zvláštní
slovo	slovo	k1gNnSc4	slovo
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
doklad	doklad	k1gInSc4	doklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
chápání	chápání	k1gNnSc6	chápání
těchto	tento	k3xDgInPc2	tento
vodních	vodní	k2eAgInPc2d1	vodní
toků	tok	k1gInPc2	tok
je	být	k5eAaImIp3nS	být
rozdíl	rozdíl	k1gInSc1	rozdíl
vnímán	vnímat	k5eAaImNgInS	vnímat
nejen	nejen	k6eAd1	nejen
jako	jako	k8xC	jako
kvantitativní	kvantitativní	k2eAgNnSc4d1	kvantitativní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
jako	jako	k9	jako
kvalitativní	kvalitativní	k2eAgFnSc3d1	kvalitativní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
mají	mít	k5eAaImIp3nP	mít
obvykle	obvykle	k6eAd1	obvykle
vodní	vodní	k2eAgInPc1d1	vodní
toky	tok	k1gInPc1	tok
vnímané	vnímaný	k2eAgInPc1d1	vnímaný
historicky	historicky	k6eAd1	historicky
jako	jako	k9	jako
řeka	řeka	k1gFnSc1	řeka
nebo	nebo	k8xC	nebo
říčka	říčka	k1gFnSc1	říčka
název	název	k1gInSc1	název
ženského	ženský	k2eAgInSc2d1	ženský
rodu	rod	k1gInSc2	rod
<g/>
,	,	kIx,	,
potoky	potok	k1gInPc1	potok
název	název	k1gInSc1	název
mužského	mužský	k2eAgInSc2d1	mužský
rodu	rod	k1gInSc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
Objektivním	objektivní	k2eAgNnSc7d1	objektivní
kritériem	kritérion	k1gNnSc7	kritérion
mohutnosti	mohutnost	k1gFnSc2	mohutnost
toku	tok	k1gInSc2	tok
v	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
profilu	profil	k1gInSc6	profil
toku	tok	k1gInSc2	tok
a	a	k8xC	a
okamžiku	okamžik	k1gInSc2	okamžik
je	být	k5eAaImIp3nS	být
průtok	průtok	k1gInSc1	průtok
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
charakteristiku	charakteristika	k1gFnSc4	charakteristika
toku	tok	k1gInSc2	tok
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
profilu	profil	k1gInSc6	profil
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
průměry	průměr	k1gInPc1	průměr
průtoku	průtok	k1gInSc2	průtok
za	za	k7c4	za
určité	určitý	k2eAgNnSc4d1	určité
období	období	k1gNnSc4	období
<g/>
,	,	kIx,	,
maximální	maximální	k2eAgInSc4d1	maximální
a	a	k8xC	a
minimální	minimální	k2eAgInSc4d1	minimální
průtoky	průtok	k1gInPc4	průtok
za	za	k7c4	za
určité	určitý	k2eAgNnSc4d1	určité
období	období	k1gNnSc4	období
atd.	atd.	kA	atd.
Průtočnost	průtočnost	k1gFnSc4	průtočnost
vodního	vodní	k2eAgInSc2d1	vodní
toku	tok	k1gInSc2	tok
v	v	k7c6	v
určitém	určitý	k2eAgNnSc6d1	určité
místě	místo	k1gNnSc6	místo
udává	udávat	k5eAaImIp3nS	udávat
průtočnou	průtočný	k2eAgFnSc4d1	průtočná
kapacitu	kapacita	k1gFnSc4	kapacita
koryta	koryto	k1gNnSc2	koryto
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
průměrné	průměrný	k2eAgFnSc2d1	průměrná
četnosti	četnost	k1gFnSc2	četnost
dosažení	dosažení	k1gNnSc2	dosažení
určitých	určitý	k2eAgInPc2d1	určitý
průtoků	průtok	k1gInPc2	průtok
se	se	k3xPyFc4	se
vypočítává	vypočítávat	k5eAaImIp3nS	vypočítávat
a	a	k8xC	a
udává	udávat	k5eAaImIp3nS	udávat
N-letý	Netý	k2eAgInSc4d1	N-letý
průtok	průtok	k1gInSc4	průtok
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
stoletá	stoletý	k2eAgFnSc1d1	stoletá
voda	voda	k1gFnSc1	voda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tok	tok	k1gInSc1	tok
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zpravidla	zpravidla	k6eAd1	zpravidla
nevysychá	vysychat	k5eNaImIp3nS	vysychat
(	(	kIx(	(
<g/>
zpravidla	zpravidla	k6eAd1	zpravidla
je	být	k5eAaImIp3nS	být
napájen	napájet	k5eAaImNgInS	napájet
podzemními	podzemní	k2eAgFnPc7d1	podzemní
vodami	voda	k1gFnPc7	voda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
stálý	stálý	k2eAgInSc4d1	stálý
tok	tok	k1gInSc4	tok
<g/>
.	.	kIx.	.
</s>
<s>
Občasným	občasný	k2eAgInSc7d1	občasný
tokem	tok	k1gInSc7	tok
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
tok	tok	k1gInSc1	tok
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
přirozeném	přirozený	k2eAgInSc6d1	přirozený
režimu	režim	k1gInSc6	režim
jsou	být	k5eAaImIp3nP	být
období	období	k1gNnPc1	období
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bývá	bývat	k5eAaImIp3nS	bývat
vyschlý	vyschlý	k2eAgInSc4d1	vyschlý
<g/>
.	.	kIx.	.
pramen	pramen	k1gInSc4	pramen
ústí	ústit	k5eAaImIp3nS	ústit
soutok	soutok	k1gInSc1	soutok
povodí	povodí	k1gNnSc2	povodí
rozvodí	rozvodit	k5eAaImIp3nS	rozvodit
bifurkace	bifurkace	k1gFnSc1	bifurkace
řečiště	řečiště	k1gNnSc2	řečiště
úmoří	úmoří	k1gNnSc2	úmoří
koloběh	koloběh	k1gInSc4	koloběh
vody	voda	k1gFnSc2	voda
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
vodní	vodní	k2eAgInSc1d1	vodní
tok	tok	k1gInSc1	tok
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
vodní	vodní	k2eAgInSc4d1	vodní
tok	tok	k1gInSc4	tok
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
