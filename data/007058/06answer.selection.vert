<s>
Tok	tok	k1gInSc1	tok
je	být	k5eAaImIp3nS	být
ohraničen	ohraničit	k5eAaPmNgInS	ohraničit
korytem	koryto	k1gNnSc7	koryto
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
dno	dno	k1gNnSc4	dno
a	a	k8xC	a
levý	levý	k2eAgInSc4d1	levý
a	a	k8xC	a
pravý	pravý	k2eAgInSc4d1	pravý
břeh	břeh	k1gInSc4	břeh
<g/>
;	;	kIx,	;
k	k	k7c3	k
rozlišení	rozlišení	k1gNnSc3	rozlišení
břehů	břeh	k1gInPc2	břeh
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
směr	směr	k1gInSc1	směr
po	po	k7c6	po
proudu	proud	k1gInSc6	proud
toku	tok	k1gInSc2	tok
<g/>
.	.	kIx.	.
</s>
