<s>
Lingvista	lingvista	k1gMnSc1	lingvista
Joseph	Joseph	k1gMnSc1	Joseph
Greenberg	Greenberg	k1gMnSc1	Greenberg
řadil	řadit	k5eAaImAgMnS	řadit
nivchštinu	nivchština	k1gFnSc4	nivchština
mezi	mezi	k7c7	mezi
tzv.	tzv.	kA	tzv.
euroasijské	euroasijský	k2eAgInPc4d1	euroasijský
jazyky	jazyk	k1gInPc4	jazyk
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
široce	široko	k6eAd1	široko
pojatá	pojatý	k2eAgFnSc1d1	pojatá
rodina	rodina	k1gFnSc1	rodina
zahrnující	zahrnující	k2eAgFnSc2d1	zahrnující
evropské	evropský	k2eAgFnSc2d1	Evropská
a	a	k8xC	a
asijské	asijský	k2eAgInPc1d1	asijský
jazyky	jazyk	k1gInPc1	jazyk
od	od	k7c2	od
indoevropských	indoevropský	k2eAgFnPc2d1	indoevropská
po	po	k7c4	po
čukotsko-kamčatské	čukotskoamčatský	k2eAgFnPc4d1	čukotsko-kamčatský
<g/>
.	.	kIx.	.
</s>
