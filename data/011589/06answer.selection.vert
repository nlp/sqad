<s>
Pošta	pošta	k1gFnSc1	pošta
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
službu	služba	k1gFnSc4	služba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
logistiku	logistika	k1gFnSc4	logistika
korespondenci	korespondence	k1gFnSc4	korespondence
<g/>
,	,	kIx,	,
listovním	listovní	k2eAgFnPc3d1	listovní
službám	služba	k1gFnPc3	služba
<g/>
,	,	kIx,	,
rozesílání	rozesílání	k1gNnSc3	rozesílání
zásilek	zásilka	k1gFnPc2	zásilka
<g/>
,	,	kIx,	,
převodům	převod	k1gInPc3	převod
peněz	peníze	k1gInPc2	peníze
<g/>
,	,	kIx,	,
vydávání	vydávání	k1gNnSc1	vydávání
známek	známka	k1gFnPc2	známka
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
