<p>
<s>
Kuba	Kuba	k1gFnSc1	Kuba
Ryba	ryba	k1gFnSc1	ryba
(	(	kIx(	(
<g/>
*	*	kIx~	*
6	[number]	k4	6
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
Kutná	kutný	k2eAgFnSc1d1	Kutná
Hora	hora	k1gFnSc1	hora
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
textař	textař	k1gMnSc1	textař
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
zakládající	zakládající	k2eAgMnSc1d1	zakládající
člen	člen	k1gMnSc1	člen
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
a	a	k8xC	a
baskytarista	baskytarista	k1gMnSc1	baskytarista
hudební	hudební	k2eAgFnSc2d1	hudební
skupiny	skupina	k1gFnSc2	skupina
Rybičky	rybička	k1gFnSc2	rybička
48	[number]	k4	48
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
synem	syn	k1gMnSc7	syn
skladatele	skladatel	k1gMnSc2	skladatel
a	a	k8xC	a
baskytaristy	baskytarista	k1gMnSc2	baskytarista
Pavla	Pavel	k1gMnSc2	Pavel
Jakuba	Jakub	k1gMnSc2	Jakub
Ryby	Ryba	k1gMnSc2	Ryba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
na	na	k7c6	na
koncertě	koncert	k1gInSc6	koncert
zkolaboval	zkolabovat	k5eAaPmAgMnS	zkolabovat
<g/>
.	.	kIx.	.
</s>
<s>
Věnuje	věnovat	k5eAaImIp3nS	věnovat
se	se	k3xPyFc4	se
charitě	charita	k1gFnSc3	charita
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
převážně	převážně	k6eAd1	převážně
nemocným	nemocný	k2eAgFnPc3d1	nemocná
dětem	dítě	k1gFnPc3	dítě
a	a	k8xC	a
zvířatům	zvíře	k1gNnPc3	zvíře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
4.10	[number]	k4	4.10
<g/>
.2018	.2018	k4	.2018
požádal	požádat	k5eAaPmAgMnS	požádat
o	o	k7c4	o
ruku	ruka	k1gFnSc4	ruka
svou	svůj	k3xOyFgFnSc4	svůj
dlouholetou	dlouholetý	k2eAgFnSc4d1	dlouholetá
přítelkyni	přítelkyně	k1gFnSc4	přítelkyně
Dominiku	Dominik	k1gMnSc3	Dominik
v	v	k7c4	v
den	den	k1gInSc4	den
jejích	její	k3xOp3gMnPc2	její
25	[number]	k4	25
<g/>
.	.	kIx.	.
narozenin	narozeniny	k1gFnPc2	narozeniny
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Působení	působení	k1gNnPc4	působení
v	v	k7c6	v
hudebních	hudební	k2eAgFnPc6d1	hudební
skupinách	skupina	k1gFnPc6	skupina
==	==	k?	==
</s>
</p>
<p>
<s>
1997	[number]	k4	1997
-	-	kIx~	-
1998	[number]	k4	1998
-	-	kIx~	-
Šedý	Šedý	k1gMnSc1	Šedý
Tetřev	tetřev	k1gMnSc1	tetřev
</s>
</p>
<p>
<s>
1998	[number]	k4	1998
-	-	kIx~	-
2001-	[number]	k4	2001-
Nawzdory	Nawzdor	k1gInPc4	Nawzdor
</s>
</p>
<p>
<s>
2000	[number]	k4	2000
-	-	kIx~	-
2002	[number]	k4	2002
-	-	kIx~	-
Wootochit	Wootochit	k1gInSc1	Wootochit
</s>
</p>
<p>
<s>
2002	[number]	k4	2002
-	-	kIx~	-
současnost	současnost	k1gFnSc4	současnost
-	-	kIx~	-
Rybičky	rybička	k1gFnSc2	rybička
48	[number]	k4	48
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Profil	profil	k1gInSc1	profil
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
skupiny	skupina	k1gFnSc2	skupina
Rybičky	rybička	k1gFnSc2	rybička
48	[number]	k4	48
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
