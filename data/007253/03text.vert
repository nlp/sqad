<s>
Čáp	Čáp	k1gMnSc1	Čáp
bílý	bílý	k1gMnSc1	bílý
(	(	kIx(	(
<g/>
Ciconia	Ciconium	k1gNnSc2	Ciconium
ciconia	ciconium	k1gNnSc2	ciconium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
velký	velký	k2eAgInSc1d1	velký
druh	druh	k1gInSc1	druh
brodivého	brodivý	k2eAgMnSc2d1	brodivý
ptáka	pták	k1gMnSc2	pták
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
čápovitých	čápovitý	k2eAgFnPc2d1	čápovitý
(	(	kIx(	(
<g/>
Ciconiidae	Ciconiidae	k1gFnPc2	Ciconiidae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Čápa	Čáp	k1gMnSc4	Čáp
bílého	bílý	k1gMnSc4	bílý
popsal	popsat	k5eAaPmAgMnS	popsat
Carl	Carl	k1gMnSc1	Carl
Linnaeus	Linnaeus	k1gMnSc1	Linnaeus
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1758	[number]	k4	1758
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Ardea	Ardeum	k1gNnSc2	Ardeum
ciconia	ciconium	k1gNnSc2	ciconium
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
rodu	rod	k1gInSc2	rod
Ciconia	Ciconium	k1gNnSc2	Ciconium
druh	druh	k1gMnSc1	druh
zařadil	zařadit	k5eAaPmAgMnS	zařadit
francouzský	francouzský	k2eAgMnSc1d1	francouzský
zoolog	zoolog	k1gMnSc1	zoolog
Mathurin	Mathurin	k1gInSc4	Mathurin
Jacques	Jacques	k1gMnSc1	Jacques
Brisson	Brisson	k1gMnSc1	Brisson
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1760	[number]	k4	1760
<g/>
.	.	kIx.	.
</s>
<s>
Vědecký	vědecký	k2eAgInSc1d1	vědecký
název	název	k1gInSc1	název
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
latinského	latinský	k2eAgInSc2d1	latinský
cǐ	cǐ	k?	cǐ
a	a	k8xC	a
podle	podle	k7c2	podle
antických	antický	k2eAgMnPc2d1	antický
autorů	autor	k1gMnPc2	autor
má	mít	k5eAaImIp3nS	mít
nejspíš	nejspíš	k9	nejspíš
původ	původ	k1gInSc4	původ
ve	v	k7c6	v
slovese	sloveso	k1gNnSc6	sloveso
canere	canrat	k5eAaPmIp3nS	canrat
(	(	kIx(	(
<g/>
zpívat	zpívat	k5eAaImF	zpívat
<g/>
,	,	kIx,	,
zvučet	zvučet	k5eAaImF	zvučet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odkazujícího	odkazující	k2eAgMnSc4d1	odkazující
na	na	k7c4	na
čapí	čapí	k2eAgNnSc4d1	čapí
klapání	klapání	k1gNnSc4	klapání
zobákem	zobák	k1gInSc7	zobák
<g/>
.	.	kIx.	.
</s>
<s>
Staročeský	staročeský	k2eAgInSc1d1	staročeský
ekvivalent	ekvivalent	k1gInSc1	ekvivalent
czap	czap	k1gMnSc1	czap
(	(	kIx(	(
<g/>
čáp	čáp	k1gMnSc1	čáp
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
již	již	k9	již
v	v	k7c6	v
latinských	latinský	k2eAgInPc6d1	latinský
pramenech	pramen	k1gInPc6	pramen
ze	z	k7c2	z
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
odvozením	odvození	k1gNnSc7	odvození
od	od	k7c2	od
názvu	název	k1gInSc2	název
czapľa	czapľus	k1gMnSc2	czapľus
<g/>
,	,	kIx,	,
označujícího	označující	k2eAgMnSc2d1	označující
nohatého	nohatý	k2eAgMnSc2d1	nohatý
ptáka	pták	k1gMnSc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
se	se	k3xPyFc4	se
2	[number]	k4	2
poddruhy	poddruh	k1gInPc7	poddruh
<g/>
:	:	kIx,	:
čáp	čáp	k1gMnSc1	čáp
bílý	bílý	k1gMnSc1	bílý
evropský	evropský	k2eAgMnSc1d1	evropský
(	(	kIx(	(
<g/>
Ciconia	Ciconium	k1gNnSc2	Ciconium
ciconia	ciconium	k1gNnSc2	ciconium
ciconia	ciconium	k1gNnSc2	ciconium
<g/>
)	)	kIx)	)
-	-	kIx~	-
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
severozápadní	severozápadní	k2eAgFnSc6d1	severozápadní
Africe	Afrika	k1gFnSc6	Afrika
a	a	k8xC	a
Přední	přední	k2eAgFnSc3d1	přední
Asii	Asie	k1gFnSc3	Asie
<g/>
,	,	kIx,	,
zimuje	zimovat	k5eAaImIp3nS	zimovat
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
čáp	čáp	k1gMnSc1	čáp
bílý	bílý	k1gMnSc1	bílý
středoasijský	středoasijský	k2eAgMnSc1d1	středoasijský
(	(	kIx(	(
<g/>
Ciconia	Ciconium	k1gNnSc2	Ciconium
ciconia	ciconium	k1gNnSc2	ciconium
asiatica	asiaticum	k1gNnSc2	asiaticum
<g/>
)	)	kIx)	)
-	-	kIx~	-
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
zimuje	zimovat	k5eAaImIp3nS	zimovat
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
kontinentu	kontinent	k1gInSc2	kontinent
Dříve	dříve	k6eAd2	dříve
byl	být	k5eAaImAgMnS	být
mezi	mezi	k7c7	mezi
poddruhy	poddruh	k1gInPc7	poddruh
čápa	čáp	k1gMnSc2	čáp
bílého	bílý	k1gMnSc2	bílý
uváděn	uvádět	k5eAaImNgInS	uvádět
také	také	k9	také
čáp	čáp	k1gMnSc1	čáp
východní	východní	k2eAgMnSc1d1	východní
(	(	kIx(	(
<g/>
Ciconia	Ciconium	k1gNnSc2	Ciconium
boyciana	boyciana	k1gFnSc1	boyciana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
široce	široko	k6eAd1	široko
uznáván	uznáván	k2eAgInSc4d1	uznáván
jako	jako	k8xC	jako
samostatný	samostatný	k2eAgInSc4d1	samostatný
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgMnSc1d1	velký
<g/>
,	,	kIx,	,
s	s	k7c7	s
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
krkem	krk	k1gInSc7	krk
a	a	k8xC	a
dlouhýma	dlouhý	k2eAgFnPc7d1	dlouhá
nohama	noha	k1gFnPc7	noha
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
těla	tělo	k1gNnSc2	tělo
je	být	k5eAaImIp3nS	být
95-110	[number]	k4	95-110
cm	cm	kA	cm
a	a	k8xC	a
rozpětí	rozpětí	k1gNnSc6	rozpětí
křídel	křídlo	k1gNnPc2	křídlo
180-218	[number]	k4	180-218
cm	cm	kA	cm
<g/>
,	,	kIx,	,
samec	samec	k1gMnSc1	samec
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
něco	něco	k3yInSc4	něco
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
samice	samice	k1gFnSc1	samice
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
2,3	[number]	k4	2,3
<g/>
–	–	k?	–
<g/>
4,4	[number]	k4	4,4
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
zbarvení	zbarvení	k1gNnSc6	zbarvení
převažuje	převažovat	k5eAaImIp3nS	převažovat
bílá	bílý	k2eAgFnSc1d1	bílá
barva	barva	k1gFnSc1	barva
<g/>
,	,	kIx,	,
letky	letek	k1gInPc1	letek
jsou	být	k5eAaImIp3nP	být
černé	černý	k2eAgInPc1d1	černý
<g/>
,	,	kIx,	,
zobák	zobák	k1gInSc4	zobák
a	a	k8xC	a
nohy	noha	k1gFnPc4	noha
červené	červený	k2eAgFnPc4d1	červená
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgNnPc1	dva
pohlaví	pohlaví	k1gNnPc1	pohlaví
jsou	být	k5eAaImIp3nP	být
zbarvena	zbarvit	k5eAaPmNgNnP	zbarvit
stejně	stejně	k6eAd1	stejně
<g/>
,	,	kIx,	,
mladý	mladý	k2eAgMnSc1d1	mladý
pták	pták	k1gMnSc1	pták
má	mít	k5eAaImIp3nS	mít
matněji	matně	k6eAd2	matně
červený	červený	k2eAgInSc1d1	červený
zobák	zobák	k1gInSc1	zobák
s	s	k7c7	s
tmavou	tmavý	k2eAgFnSc7d1	tmavá
špičkou	špička	k1gFnSc7	špička
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letu	let	k1gInSc6	let
drží	držet	k5eAaImIp3nS	držet
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
volavek	volavka	k1gFnPc2	volavka
a	a	k8xC	a
pelikánů	pelikán	k1gInPc2	pelikán
krk	krk	k1gInSc1	krk
natažený	natažený	k2eAgMnSc1d1	natažený
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgInSc7d1	typický
zvukovým	zvukový	k2eAgInSc7d1	zvukový
projevem	projev	k1gInSc7	projev
je	být	k5eAaImIp3nS	být
klapání	klapání	k1gNnSc1	klapání
zobákem	zobák	k1gInSc7	zobák
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
se	se	k3xPyFc4	se
ozývá	ozývat	k5eAaImIp3nS	ozývat
hlavně	hlavně	k9	hlavně
na	na	k7c6	na
hnízdě	hnízdo	k1gNnSc6	hnízdo
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdní	hnízdní	k2eAgInSc1d1	hnízdní
areál	areál	k1gInSc1	areál
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
,	,	kIx,	,
Přední	přední	k2eAgFnSc4d1	přední
a	a	k8xC	a
střední	střední	k2eAgFnSc4d1	střední
Asii	Asie	k1gFnSc4	Asie
a	a	k8xC	a
severozápadní	severozápadní	k2eAgFnSc4d1	severozápadní
Afriku	Afrika	k1gFnSc4	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
malá	malý	k2eAgFnSc1d1	malá
populace	populace	k1gFnSc1	populace
také	také	k9	také
v	v	k7c6	v
Jihoafrické	jihoafrický	k2eAgFnSc6d1	Jihoafrická
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Čáp	Čáp	k1gMnSc1	Čáp
bílý	bílý	k1gMnSc1	bílý
je	být	k5eAaImIp3nS	být
tažný	tažný	k2eAgMnSc1d1	tažný
pták	pták	k1gMnSc1	pták
se	s	k7c7	s
zimovišti	zimoviště	k1gNnPc7	zimoviště
v	v	k7c6	v
subsaharské	subsaharský	k2eAgFnSc6d1	subsaharská
Africe	Afrika	k1gFnSc6	Afrika
a	a	k8xC	a
jižní	jižní	k2eAgFnSc6d1	jižní
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
přibývá	přibývat	k5eAaImIp3nS	přibývat
také	také	k6eAd1	také
případů	případ	k1gInPc2	případ
přezimování	přezimování	k1gNnSc2	přezimování
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
(	(	kIx(	(
<g/>
např.	např.	kA	např.
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
,	,	kIx,	,
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
Bulharsku	Bulharsko	k1gNnSc6	Bulharsko
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
Blízkém	blízký	k2eAgInSc6d1	blízký
východě	východ	k1gInSc6	východ
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bývá	bývat	k5eAaImIp3nS	bývat
připisováno	připisovat	k5eAaImNgNnS	připisovat
mírnějším	mírný	k2eAgFnPc3d2	mírnější
zimám	zima	k1gFnPc3	zima
a	a	k8xC	a
také	také	k9	také
větší	veliký	k2eAgFnSc3d2	veliký
potravní	potravní	k2eAgFnSc3d1	potravní
nabídce	nabídka	k1gFnSc3	nabídka
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
denního	denní	k2eAgMnSc4d1	denní
migranta	migrant	k1gMnSc4	migrant
táhnoucího	táhnoucí	k2eAgMnSc4d1	táhnoucí
ve	v	k7c6	v
větších	veliký	k2eAgNnPc6d2	veliký
hejnech	hejno	k1gNnPc6	hejno
čítajících	čítající	k2eAgInPc2d1	čítající
i	i	k8xC	i
několik	několik	k4yIc4	několik
tisíc	tisíc	k4xCgInSc4	tisíc
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
urazí	urazit	k5eAaPmIp3nS	urazit
pomocí	pomocí	k7c2	pomocí
plachtění	plachtění	k1gNnSc2	plachtění
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yRgInSc6	který
využívá	využívat	k5eAaImIp3nS	využívat
stoupavých	stoupavý	k2eAgInPc2d1	stoupavý
termálních	termální	k2eAgInPc2d1	termální
proudů	proud	k1gInPc2	proud
nad	nad	k7c7	nad
pevninou	pevnina	k1gFnSc7	pevnina
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
z	z	k7c2	z
evropských	evropský	k2eAgFnPc2d1	Evropská
populací	populace	k1gFnPc2	populace
se	se	k3xPyFc4	se
na	na	k7c4	na
podzimní	podzimní	k2eAgInSc4d1	podzimní
tah	tah	k1gInSc4	tah
vydávají	vydávat	k5eAaPmIp3nP	vydávat
od	od	k7c2	od
srpna	srpen	k1gInSc2	srpen
(	(	kIx(	(
<g/>
mladí	mladý	k2eAgMnPc1d1	mladý
ptáci	pták	k1gMnPc1	pták
o	o	k7c4	o
několik	několik	k4yIc4	několik
dnů	den	k1gInPc2	den
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
dospělý	dospělý	k2eAgMnSc1d1	dospělý
pár	pár	k4xCyI	pár
<g/>
)	)	kIx)	)
a	a	k8xC	a
migrují	migrovat	k5eAaImIp3nP	migrovat
dvěma	dva	k4xCgFnPc7	dva
hlavními	hlavní	k2eAgFnPc7d1	hlavní
tahovými	tahový	k2eAgFnPc7d1	tahová
cestami	cesta	k1gFnPc7	cesta
–	–	k?	–
západní	západní	k2eAgFnSc4d1	západní
<g/>
,	,	kIx,	,
vedoucí	vedoucí	k2eAgFnSc4d1	vedoucí
přes	přes	k7c4	přes
Gibraltar	Gibraltar	k1gInSc4	Gibraltar
do	do	k7c2	do
západní	západní	k2eAgFnSc2d1	západní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
a	a	k8xC	a
východní	východní	k2eAgFnSc1d1	východní
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vede	vést	k5eAaImIp3nS	vést
přes	přes	k7c4	přes
Turecko	Turecko	k1gNnSc4	Turecko
do	do	k7c2	do
východní	východní	k2eAgFnSc2d1	východní
<g/>
,	,	kIx,	,
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
a	a	k8xC	a
jižní	jižní	k2eAgFnSc2d1	jižní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Hranice	hranice	k1gFnSc1	hranice
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgFnPc7	tento
tahovými	tahový	k2eAgFnPc7d1	tahová
cestami	cesta	k1gFnPc7	cesta
leží	ležet	k5eAaImIp3nS	ležet
mezi	mezi	k7c7	mezi
Rýnem	Rýn	k1gInSc7	Rýn
a	a	k8xC	a
Labem	Labe	k1gNnSc7	Labe
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
i	i	k9	i
na	na	k7c6	na
území	území	k1gNnSc6	území
ČR	ČR	kA	ČR
<g/>
)	)	kIx)	)
a	a	k8xC	a
obě	dva	k4xCgFnPc1	dva
populace	populace	k1gFnPc1	populace
se	se	k3xPyFc4	se
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
mísí	mísit	k5eAaImIp3nP	mísit
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
z	z	k7c2	z
ČR	ČR	kA	ČR
se	se	k3xPyFc4	se
do	do	k7c2	do
Afriky	Afrika	k1gFnSc2	Afrika
vydávají	vydávat	k5eAaImIp3nP	vydávat
převážně	převážně	k6eAd1	převážně
jihovýchodním	jihovýchodní	k2eAgInSc6d1	jihovýchodní
a	a	k8xC	a
méně	málo	k6eAd2	málo
(	(	kIx(	(
<g/>
především	především	k9	především
ptáci	pták	k1gMnPc1	pták
ze	z	k7c2	z
západních	západní	k2eAgFnPc2d1	západní
a	a	k8xC	a
jižních	jižní	k2eAgFnPc2d1	jižní
Čech	Čechy	k1gFnPc2	Čechy
<g/>
)	)	kIx)	)
i	i	k8xC	i
jihozápadním	jihozápadní	k2eAgInSc7d1	jihozápadní
směrem	směr	k1gInSc7	směr
přes	přes	k7c4	přes
Německo	Německo	k1gNnSc4	Německo
<g/>
,	,	kIx,	,
Francii	Francie	k1gFnSc4	Francie
a	a	k8xC	a
Španělsko	Španělsko	k1gNnSc4	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zimovištích	zimoviště	k1gNnPc6	zimoviště
většinou	většinou	k6eAd1	většinou
nesetrvávají	setrvávat	k5eNaImIp3nP	setrvávat
dlouho	dlouho	k6eAd1	dlouho
<g/>
;	;	kIx,	;
staří	starý	k2eAgMnPc1d1	starý
ptáci	pták	k1gMnPc1	pták
se	se	k3xPyFc4	se
na	na	k7c4	na
jarní	jarní	k2eAgInSc4d1	jarní
tah	tah	k1gInSc4	tah
vydávají	vydávat	k5eAaImIp3nP	vydávat
zpravidla	zpravidla	k6eAd1	zpravidla
během	během	k7c2	během
února	únor	k1gInSc2	únor
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
mladí	mladý	k2eAgMnPc1d1	mladý
<g/>
)	)	kIx)	)
ptáci	pták	k1gMnPc1	pták
se	se	k3xPyFc4	se
však	však	k9	však
občas	občas	k6eAd1	občas
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
nevracejí	vracet	k5eNaImIp3nP	vracet
vůbec	vůbec	k9	vůbec
a	a	k8xC	a
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
setrvávají	setrvávat	k5eAaImIp3nP	setrvávat
i	i	k9	i
následující	následující	k2eAgNnPc4d1	následující
letní	letní	k2eAgNnPc4d1	letní
období	období	k1gNnPc4	období
<g/>
.	.	kIx.	.
</s>
<s>
Jarní	jarní	k2eAgInSc1d1	jarní
tah	tah	k1gInSc1	tah
je	být	k5eAaImIp3nS	být
výrazně	výrazně	k6eAd1	výrazně
rychlejší	rychlý	k2eAgMnSc1d2	rychlejší
než	než	k8xS	než
podzimní	podzimní	k2eAgInSc1d1	podzimní
a	a	k8xC	a
čápi	čáp	k1gMnPc1	čáp
při	při	k7c6	při
něm	on	k3xPp3gInSc6	on
dělají	dělat	k5eAaImIp3nP	dělat
i	i	k9	i
méně	málo	k6eAd2	málo
zastávek	zastávka	k1gFnPc2	zastávka
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
zimující	zimující	k2eAgMnPc1d1	zimující
v	v	k7c6	v
Jihoafrické	jihoafrický	k2eAgFnSc6d1	Jihoafrická
republice	republika	k1gFnSc6	republika
každý	každý	k3xTgInSc1	každý
rok	rok	k1gInSc1	rok
uletí	uletět	k5eAaPmIp3nS	uletět
cestou	cesta	k1gFnSc7	cesta
na	na	k7c4	na
zimoviště	zimoviště	k1gNnSc4	zimoviště
a	a	k8xC	a
zpět	zpět	k6eAd1	zpět
více	hodně	k6eAd2	hodně
než	než	k8xS	než
20	[number]	k4	20
000	[number]	k4	000
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
podzimního	podzimní	k2eAgInSc2d1	podzimní
tahu	tah	k1gInSc2	tah
přitom	přitom	k6eAd1	přitom
denně	denně	k6eAd1	denně
zdolají	zdolat	k5eAaPmIp3nP	zdolat
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
kolem	kolem	k7c2	kolem
200	[number]	k4	200
km	km	kA	km
<g/>
,	,	kIx,	,
na	na	k7c6	na
jarním	jarní	k2eAgInSc6d1	jarní
tahu	tah	k1gInSc6	tah
okolo	okolo	k7c2	okolo
300-400	[number]	k4	300-400
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
jednotlivě	jednotlivě	k6eAd1	jednotlivě
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
volných	volný	k2eAgFnPc6d1	volná
koloniích	kolonie	k1gFnPc6	kolonie
v	v	k7c6	v
otevřené	otevřený	k2eAgFnSc6d1	otevřená
zemědělské	zemědělský	k2eAgFnSc6d1	zemědělská
krajině	krajina	k1gFnSc6	krajina
s	s	k7c7	s
přístupem	přístup	k1gInSc7	přístup
k	k	k7c3	k
bažinatým	bažinatý	k2eAgInPc3d1	bažinatý
říčním	říční	k2eAgInPc3d1	říční
břehům	břeh	k1gInPc3	břeh
<g/>
,	,	kIx,	,
mokřadům	mokřad	k1gInPc3	mokřad
a	a	k8xC	a
zaplavovaným	zaplavovaný	k2eAgNnSc7d1	zaplavované
územím	území	k1gNnSc7	území
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
hnízdiště	hnízdiště	k1gNnPc4	hnízdiště
obvykle	obvykle	k6eAd1	obvykle
jako	jako	k8xC	jako
první	první	k4xOgFnSc1	první
přilétá	přilétat	k5eAaImIp3nS	přilétat
koncem	koncem	k7c2	koncem
března	březen	k1gInSc2	březen
nebo	nebo	k8xC	nebo
začátkem	začátkem	k7c2	začátkem
dubna	duben	k1gInSc2	duben
samec	samec	k1gMnSc1	samec
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
obsazuje	obsazovat	k5eAaImIp3nS	obsazovat
pokud	pokud	k8xS	pokud
možno	možno	k6eAd1	možno
loňské	loňský	k2eAgNnSc4d1	loňské
hnízdo	hnízdo	k1gNnSc4	hnízdo
a	a	k8xC	a
hájí	hájit	k5eAaImIp3nP	hájit
ho	on	k3xPp3gInSc4	on
před	před	k7c7	před
ostatními	ostatní	k2eAgInPc7d1	ostatní
samci	samec	k1gInPc7	samec
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
příletu	přílet	k1gInSc6	přílet
samice	samice	k1gFnSc2	samice
se	se	k3xPyFc4	se
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
oba	dva	k4xCgMnPc1	dva
ptáci	pták	k1gMnPc1	pták
zdraví	zdraví	k1gNnSc4	zdraví
hlasitým	hlasitý	k2eAgNnSc7d1	hlasité
klapáním	klapání	k1gNnSc7	klapání
zobákem	zobák	k1gInSc7	zobák
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdo	hnízdo	k1gNnSc4	hnízdo
stavějí	stavět	k5eAaImIp3nP	stavět
oba	dva	k4xCgMnPc1	dva
ptáci	pták	k1gMnPc1	pták
na	na	k7c6	na
střechách	střecha	k1gFnPc6	střecha
<g/>
,	,	kIx,	,
komínech	komín	k1gInPc6	komín
<g/>
,	,	kIx,	,
sloupech	sloup	k1gInPc6	sloup
<g/>
,	,	kIx,	,
stromech	strom	k1gInPc6	strom
<g/>
,	,	kIx,	,
speciálních	speciální	k2eAgFnPc6d1	speciální
podložkách	podložka	k1gFnPc6	podložka
a	a	k8xC	a
jiných	jiný	k2eAgNnPc6d1	jiné
vyvýšených	vyvýšený	k2eAgNnPc6d1	vyvýšené
místech	místo	k1gNnPc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdění	hnízdění	k1gNnSc1	hnízdění
na	na	k7c6	na
lidských	lidský	k2eAgNnPc6d1	lidské
obydlích	obydlí	k1gNnPc6	obydlí
je	být	k5eAaImIp3nS	být
doloženo	doložen	k2eAgNnSc1d1	doloženo
již	již	k6eAd1	již
od	od	k7c2	od
středověku	středověk	k1gInSc2	středověk
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
Fridricha	Fridrich	k1gMnSc2	Fridrich
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Štaufského	Štaufský	k2eAgMnSc4d1	Štaufský
De	De	k?	De
arte	arte	k6eAd1	arte
venandi	venand	k1gMnPc1	venand
cum	cum	k?	cum
avibusStavebním	avibusStavební	k2eAgInSc7d1	avibusStavební
materiálem	materiál	k1gInSc7	materiál
jsou	být	k5eAaImIp3nP	být
suché	suchý	k2eAgFnPc1d1	suchá
větve	větev	k1gFnPc1	větev
<g/>
,	,	kIx,	,
kotlinka	kotlinka	k1gFnSc1	kotlinka
je	být	k5eAaImIp3nS	být
vystlána	vystlat	k5eAaPmNgFnS	vystlat
drny	drn	k1gInPc7	drn
<g/>
,	,	kIx,	,
trávou	tráva	k1gFnSc7	tráva
<g/>
,	,	kIx,	,
srstí	srst	k1gFnSc7	srst
zvířat	zvíře	k1gNnPc2	zvíře
a	a	k8xC	a
méně	málo	k6eAd2	málo
často	často	k6eAd1	často
i	i	k9	i
textilie	textilie	k1gFnSc1	textilie
a	a	k8xC	a
jiné	jiný	k2eAgInPc1d1	jiný
odpadky	odpadek	k1gInPc1	odpadek
včetně	včetně	k7c2	včetně
kusů	kus	k1gInPc2	kus
igelitu	igelit	k1gInSc2	igelit
nebo	nebo	k8xC	nebo
provázků	provázek	k1gInPc2	provázek
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgNnSc1d1	Nové
hnízdo	hnízdo	k1gNnSc1	hnízdo
mívá	mívat	k5eAaImIp3nS	mívat
průměr	průměr	k1gInSc4	průměr
80	[number]	k4	80
cm	cm	kA	cm
a	a	k8xC	a
výšku	výška	k1gFnSc4	výška
15-30	[number]	k4	15-30
cm	cm	kA	cm
<g/>
,	,	kIx,	,
každý	každý	k3xTgMnSc1	každý
rok	rok	k1gInSc4	rok
však	však	k9	však
bývá	bývat	k5eAaImIp3nS	bývat
trochu	trochu	k6eAd1	trochu
přistavováno	přistavován	k2eAgNnSc1d1	přistavováno
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
může	moct	k5eAaImIp3nS	moct
časem	časem	k6eAd1	časem
dosahovat	dosahovat	k5eAaImF	dosahovat
značných	značný	k2eAgInPc2d1	značný
rozměrů	rozměr	k1gInPc2	rozměr
(	(	kIx(	(
<g/>
výšky	výška	k1gFnSc2	výška
přes	přes	k7c4	přes
3	[number]	k4	3
m	m	kA	m
a	a	k8xC	a
průměru	průměr	k1gInSc2	průměr
až	až	k9	až
2	[number]	k4	2
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
1	[number]	k4	1
<g/>
x	x	k?	x
ročně	ročně	k6eAd1	ročně
od	od	k7c2	od
dubna	duben	k1gInSc2	duben
do	do	k7c2	do
srpna	srpen	k1gInSc2	srpen
<g/>
.	.	kIx.	.
</s>
<s>
Snůška	snůška	k1gFnSc1	snůška
čítá	čítat	k5eAaImIp3nS	čítat
2-7	[number]	k4	2-7
bílých	bílý	k2eAgNnPc2d1	bílé
vajec	vejce	k1gNnPc2	vejce
o	o	k7c6	o
rozměrech	rozměr	k1gInPc6	rozměr
73	[number]	k4	73
x	x	k?	x
53	[number]	k4	53
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Inkubace	inkubace	k1gFnSc1	inkubace
začíná	začínat	k5eAaImIp3nS	začínat
od	od	k7c2	od
snesení	snesení	k1gNnSc2	snesení
prvního	první	k4xOgNnSc2	první
vejce	vejce	k1gNnSc2	vejce
a	a	k8xC	a
trvá	trvat	k5eAaImIp3nS	trvat
přibližně	přibližně	k6eAd1	přibližně
33	[number]	k4	33
dnů	den	k1gInPc2	den
<g/>
,	,	kIx,	,
sedí	sedit	k5eAaImIp3nS	sedit
oba	dva	k4xCgMnPc4	dva
rodiče	rodič	k1gMnPc4	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
se	se	k3xPyFc4	se
líhnou	líhnout	k5eAaImIp3nP	líhnout
postupně	postupně	k6eAd1	postupně
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
tak	tak	k6eAd1	tak
patrný	patrný	k2eAgInSc1d1	patrný
věkový	věkový	k2eAgInSc1d1	věkový
rozdíl	rozdíl	k1gInSc1	rozdíl
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
utlačování	utlačování	k1gNnSc3	utlačování
mladších	mladý	k2eAgNnPc2d2	mladší
mláďat	mládě	k1gNnPc2	mládě
staršími	starý	k2eAgMnPc7d2	starší
sourozenci	sourozenec	k1gMnPc7	sourozenec
na	na	k7c6	na
hnízdě	hnízdo	k1gNnSc6	hnízdo
nedochází	docházet	k5eNaImIp3nS	docházet
<g/>
,	,	kIx,	,
slabší	slabý	k2eAgNnPc4d2	slabší
mláďata	mládě	k1gNnPc4	mládě
však	však	k9	však
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
občas	občas	k6eAd1	občas
zabita	zabit	k2eAgFnSc1d1	zabita
rodiči	rodič	k1gMnPc7	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Děje	dít	k5eAaImIp3nS	dít
se	se	k3xPyFc4	se
tak	tak	k9	tak
v	v	k7c6	v
případě	případ	k1gInSc6	případ
nedostatku	nedostatek	k1gInSc2	nedostatek
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
dospělí	dospělý	k2eAgMnPc1d1	dospělý
ptáci	pták	k1gMnPc1	pták
zabitím	zabití	k1gNnSc7	zabití
nejslabších	slabý	k2eAgNnPc2d3	nejslabší
mláďat	mládě	k1gNnPc2	mládě
snaží	snažit	k5eAaImIp3nS	snažit
zajistit	zajistit	k5eAaPmF	zajistit
větší	veliký	k2eAgFnSc4d2	veliký
šanci	šance	k1gFnSc4	šance
na	na	k7c4	na
přežití	přežití	k1gNnSc4	přežití
zbylých	zbylý	k2eAgMnPc2d1	zbylý
potomků	potomek	k1gMnPc2	potomek
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
rychlý	rychlý	k2eAgInSc1d1	rychlý
a	a	k8xC	a
efektivní	efektivní	k2eAgInSc1d1	efektivní
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
praktikován	praktikovat	k5eAaImNgInS	praktikovat
jen	jen	k9	jen
vzácně	vzácně	k6eAd1	vzácně
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
se	se	k3xPyFc4	se
ve	v	k7c6	v
stáří	stáří	k1gNnSc6	stáří
14	[number]	k4	14
dnů	den	k1gInPc2	den
začínají	začínat	k5eAaImIp3nP	začínat
držet	držet	k5eAaImF	držet
na	na	k7c6	na
nohou	noha	k1gFnPc6	noha
a	a	k8xC	a
od	od	k7c2	od
25	[number]	k4	25
<g/>
.	.	kIx.	.
dne	den	k1gInSc2	den
již	již	k6eAd1	již
na	na	k7c6	na
hnízdě	hnízdo	k1gNnSc6	hnízdo
zpravidla	zpravidla	k6eAd1	zpravidla
stojí	stát	k5eAaImIp3nS	stát
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdní	hnízdní	k2eAgFnSc1d1	hnízdní
péče	péče	k1gFnSc1	péče
trvá	trvat	k5eAaImIp3nS	trvat
zhruba	zhruba	k6eAd1	zhruba
8	[number]	k4	8
týdnů	týden	k1gInPc2	týden
a	a	k8xC	a
po	po	k7c6	po
dalších	další	k2eAgInPc6d1	další
10	[number]	k4	10
dnech	den	k1gInPc6	den
jsou	být	k5eAaImIp3nP	být
mláďata	mládě	k1gNnPc4	mládě
již	již	k6eAd1	již
plně	plně	k6eAd1	plně
samostatná	samostatný	k2eAgFnSc1d1	samostatná
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vyhnízdění	vyhnízdění	k1gNnSc6	vyhnízdění
se	se	k3xPyFc4	se
rodiny	rodina	k1gFnPc1	rodina
slučují	slučovat	k5eAaImIp3nP	slučovat
do	do	k7c2	do
menších	malý	k2eAgNnPc2d2	menší
i	i	k8xC	i
větších	veliký	k2eAgNnPc2d2	veliký
hejn	hejno	k1gNnPc2	hejno
<g/>
,	,	kIx,	,
v	v	k7c6	v
kterých	který	k3yIgFnPc6	který
později	pozdě	k6eAd2	pozdě
táhnou	táhnout	k5eAaImIp3nP	táhnout
do	do	k7c2	do
zimovišť	zimoviště	k1gNnPc2	zimoviště
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
ztrátám	ztráta	k1gFnPc3	ztráta
na	na	k7c6	na
hnízdech	hnízdo	k1gNnPc6	hnízdo
dochází	docházet	k5eAaImIp3nS	docházet
nejčastěji	často	k6eAd3	často
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
nepříznivého	příznivý	k2eNgNnSc2d1	nepříznivé
počasí	počasí	k1gNnSc2	počasí
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
mohou	moct	k5eAaImIp3nP	moct
malá	malý	k2eAgNnPc4d1	malé
mláďata	mládě	k1gNnPc4	mládě
prochladnout	prochladnout	k5eAaPmF	prochladnout
nebo	nebo	k8xC	nebo
zahynout	zahynout	k5eAaPmF	zahynout
hlady	hlady	k6eAd1	hlady
<g/>
.	.	kIx.	.
</s>
<s>
Pohlavní	pohlavní	k2eAgFnPc1d1	pohlavní
dospělosti	dospělost	k1gFnPc1	dospělost
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
ve	v	k7c6	v
stáří	stáří	k1gNnSc6	stáří
3	[number]	k4	3
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
ale	ale	k9	ale
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
často	často	k6eAd1	často
o	o	k7c4	o
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
roky	rok	k1gInPc7	rok
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
dožívá	dožívat	k5eAaImIp3nS	dožívat
průměrně	průměrně	k6eAd1	průměrně
8-10	[number]	k4	8-10
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
rekord	rekord	k1gInSc1	rekord
je	být	k5eAaImIp3nS	být
34	[number]	k4	34
let	léto	k1gNnPc2	léto
a	a	k8xC	a
10	[number]	k4	10
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Čáp	Čáp	k1gMnSc1	Čáp
bílý	bílý	k1gMnSc1	bílý
je	být	k5eAaImIp3nS	být
potravní	potravní	k2eAgMnSc1d1	potravní
oportunista	oportunista	k1gMnSc1	oportunista
<g/>
.	.	kIx.	.
</s>
<s>
Živí	živit	k5eAaImIp3nS	živit
se	se	k3xPyFc4	se
hlavně	hlavně	k9	hlavně
drobnými	drobný	k2eAgMnPc7d1	drobný
savci	savec	k1gMnPc7	savec
(	(	kIx(	(
<g/>
hraboši	hraboš	k1gMnPc7	hraboš
<g/>
,	,	kIx,	,
krtky	krtek	k1gMnPc7	krtek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hmyzem	hmyz	k1gInSc7	hmyz
<g/>
,	,	kIx,	,
žábami	žába	k1gFnPc7	žába
a	a	k8xC	a
žížalami	žížala	k1gFnPc7	žížala
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
požírá	požírat	k5eAaImIp3nS	požírat
také	také	k9	také
plazy	plaz	k1gMnPc4	plaz
<g/>
,	,	kIx,	,
plže	plž	k1gMnPc4	plž
<g/>
,	,	kIx,	,
ryby	ryba	k1gFnPc4	ryba
<g/>
,	,	kIx,	,
ptačí	ptačí	k2eAgNnPc4d1	ptačí
vejce	vejce	k1gNnPc4	vejce
i	i	k8xC	i
mláďata	mládě	k1gNnPc4	mládě
a	a	k8xC	a
mršiny	mršina	k1gFnPc4	mršina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
často	často	k6eAd1	často
sleduje	sledovat	k5eAaImIp3nS	sledovat
hejna	hejno	k1gNnSc2	hejno
sarančat	saranče	k1gNnPc2	saranče
<g/>
.	.	kIx.	.
</s>
<s>
Kořist	kořist	k1gFnSc1	kořist
vyhlíží	vyhlížet	k5eAaImIp3nS	vyhlížet
za	za	k7c4	za
pomalé	pomalý	k2eAgFnPc4d1	pomalá
rozvážné	rozvážný	k2eAgFnPc4d1	rozvážná
chůze	chůze	k1gFnPc4	chůze
<g/>
,	,	kIx,	,
na	na	k7c4	na
hlodavce	hlodavec	k1gMnSc4	hlodavec
někdy	někdy	k6eAd1	někdy
nehybně	hybně	k6eNd1	hybně
čeká	čekat	k5eAaImIp3nS	čekat
u	u	k7c2	u
nor	nora	k1gFnPc2	nora
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
volavky	volavka	k1gFnSc2	volavka
<g/>
.	.	kIx.	.
</s>
<s>
Početnost	početnost	k1gFnSc1	početnost
čápa	čáp	k1gMnSc2	čáp
bílého	bílý	k1gMnSc2	bílý
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
výrazně	výrazně	k6eAd1	výrazně
klesala	klesat	k5eAaImAgFnS	klesat
a	a	k8xC	a
z	z	k7c2	z
některých	některý	k3yIgFnPc2	některý
zemí	zem	k1gFnPc2	zem
zcela	zcela	k6eAd1	zcela
vymizel	vymizet	k5eAaPmAgMnS	vymizet
<g/>
;	;	kIx,	;
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1895	[number]	k4	1895
byl	být	k5eAaImAgInS	být
naposledy	naposledy	k6eAd1	naposledy
pozorován	pozorovat	k5eAaImNgInS	pozorovat
v	v	k7c6	v
Belgii	Belgie	k1gFnSc6	Belgie
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
a	a	k8xC	a
roku	rok	k1gInSc6	rok
1950	[number]	k4	1950
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgFnPc7d1	hlavní
příčinami	příčina	k1gFnPc7	příčina
úbytku	úbytek	k1gInSc2	úbytek
byly	být	k5eAaImAgFnP	být
industrializace	industrializace	k1gFnPc1	industrializace
zemědělství	zemědělství	k1gNnSc2	zemědělství
a	a	k8xC	a
nepříznivé	příznivý	k2eNgFnSc2d1	nepříznivá
změny	změna	k1gFnSc2	změna
v	v	k7c6	v
krajině	krajina	k1gFnSc6	krajina
<g/>
,	,	kIx,	,
zahrnující	zahrnující	k2eAgNnPc4d1	zahrnující
vysoušení	vysoušení	k1gNnPc4	vysoušení
mokřin	mokřina	k1gFnPc2	mokřina
a	a	k8xC	a
přeměny	přeměna	k1gFnSc2	přeměna
luk	louka	k1gFnPc2	louka
na	na	k7c4	na
pole	pole	k1gNnSc4	pole
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgInP	začít
stavy	stav	k1gInPc1	stav
na	na	k7c6	na
většině	většina	k1gFnSc6	většina
území	území	k1gNnSc2	území
opět	opět	k6eAd1	opět
zvyšovat	zvyšovat	k5eAaImF	zvyšovat
a	a	k8xC	a
čáp	čáp	k1gMnSc1	čáp
bílý	bílý	k1gMnSc1	bílý
jako	jako	k8xC	jako
hnízdič	hnízdič	k1gMnSc1	hnízdič
postupně	postupně	k6eAd1	postupně
osídlil	osídlit	k5eAaPmAgMnS	osídlit
řadu	řada	k1gFnSc4	řada
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
vymizel	vymizet	k5eAaPmAgMnS	vymizet
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1984	[number]	k4	1984
byla	být	k5eAaImAgFnS	být
velikost	velikost	k1gFnSc1	velikost
evropské	evropský	k2eAgFnSc2d1	Evropská
populace	populace	k1gFnSc2	populace
odhadována	odhadovat	k5eAaImNgFnS	odhadovat
na	na	k7c4	na
135	[number]	k4	135
000	[number]	k4	000
párů	pár	k1gInPc2	pár
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
již	již	k6eAd1	již
233	[number]	k4	233
000	[number]	k4	000
párů	pár	k1gInPc2	pár
<g/>
.	.	kIx.	.
</s>
<s>
Dřívějších	dřívější	k2eAgInPc2d1	dřívější
počtů	počet	k1gInPc2	počet
však	však	k9	však
evropská	evropský	k2eAgFnSc1d1	Evropská
populace	populace	k1gFnSc1	populace
stále	stále	k6eAd1	stále
nedosahuje	dosahovat	k5eNaImIp3nS	dosahovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
hodnocena	hodnocen	k2eAgFnSc1d1	hodnocena
jako	jako	k8xC	jako
zmenšená	zmenšený	k2eAgFnSc1d1	zmenšená
<g/>
.	.	kIx.	.
</s>
<s>
Čáp	Čáp	k1gMnSc1	Čáp
bílý	bílý	k1gMnSc1	bílý
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgInPc4	který
se	se	k3xPyFc4	se
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
Dohoda	dohoda	k1gFnSc1	dohoda
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
africko-euroasijských	africkouroasijský	k2eAgMnPc2d1	africko-euroasijský
stěhovavých	stěhovavý	k2eAgMnPc2d1	stěhovavý
vodních	vodní	k2eAgMnPc2d1	vodní
ptáků	pták	k1gMnPc2	pták
(	(	kIx(	(
<g/>
AEWA	AEWA	kA	AEWA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
zmiňovaných	zmiňovaný	k2eAgFnPc2d1	zmiňovaná
změn	změna	k1gFnPc2	změna
v	v	k7c6	v
krajině	krajina	k1gFnSc6	krajina
je	být	k5eAaImIp3nS	být
ohrožován	ohrožovat	k5eAaImNgInS	ohrožovat
hlavně	hlavně	k6eAd1	hlavně
kolizemi	kolize	k1gFnPc7	kolize
s	s	k7c7	s
dráty	drát	k1gInPc7	drát
<g/>
,	,	kIx,	,
zasažením	zasažení	k1gNnSc7	zasažení
elektrickým	elektrický	k2eAgInSc7d1	elektrický
proudem	proud	k1gInSc7	proud
na	na	k7c6	na
sloupech	sloup	k1gInPc6	sloup
VN	VN	kA	VN
<g/>
,	,	kIx,	,
pesticidy	pesticid	k1gInPc1	pesticid
používanými	používaný	k2eAgFnPc7d1	používaná
při	pře	k1gFnSc3	pře
hubení	hubení	k1gNnSc6	hubení
sarančí	saranče	k1gFnPc2	saranče
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
a	a	k8xC	a
ilegálním	ilegální	k2eAgInSc7d1	ilegální
lovem	lov	k1gInSc7	lov
na	na	k7c6	na
tahu	tah	k1gInSc6	tah
a	a	k8xC	a
zimovištích	zimoviště	k1gNnPc6	zimoviště
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
567	[number]	k4	567
čápů	čáp	k1gMnPc2	čáp
bílých	bílý	k2eAgFnPc2d1	bílá
okroužkovaných	okroužkovaný	k2eAgFnPc2d1	okroužkovaná
na	na	k7c6	na
území	území	k1gNnSc6	území
ČR	ČR	kA	ČR
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1934-2012	[number]	k4	1934-2012
byly	být	k5eAaImAgFnP	být
nejčastějšími	častý	k2eAgFnPc7d3	nejčastější
příčinami	příčina	k1gFnPc7	příčina
úhynu	úhyn	k1gInSc2	úhyn
náraz	náraz	k1gInSc4	náraz
na	na	k7c4	na
dráty	drát	k1gInPc4	drát
(	(	kIx(	(
<g/>
50	[number]	k4	50
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zasažení	zasažení	k1gNnSc1	zasažení
elektrickým	elektrický	k2eAgInSc7d1	elektrický
proudem	proud	k1gInSc7	proud
(	(	kIx(	(
<g/>
23	[number]	k4	23
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zástřel	zástřel	k1gInSc1	zástřel
(	(	kIx(	(
<g/>
10	[number]	k4	10
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
antropogenní	antropogenní	k2eAgFnPc1d1	antropogenní
příčiny	příčina	k1gFnPc1	příčina
(	(	kIx(	(
<g/>
14	[number]	k4	14
%	%	kIx~	%
<g/>
;	;	kIx,	;
nejčastěji	často	k6eAd3	často
nárazy	náraz	k1gInPc1	náraz
a	a	k8xC	a
pády	pád	k1gInPc1	pád
do	do	k7c2	do
lidských	lidský	k2eAgFnPc2d1	lidská
staveb	stavba	k1gFnPc2	stavba
<g/>
,	,	kIx,	,
srážky	srážka	k1gFnPc1	srážka
s	s	k7c7	s
dopravními	dopravní	k2eAgInPc7d1	dopravní
prostředky	prostředek	k1gInPc7	prostředek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přirozené	přirozený	k2eAgFnPc1d1	přirozená
příčiny	příčina	k1gFnPc1	příčina
představovaly	představovat	k5eAaImAgFnP	představovat
pouze	pouze	k6eAd1	pouze
3	[number]	k4	3
%	%	kIx~	%
případů	případ	k1gInPc2	případ
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
evropské	evropský	k2eAgFnSc2d1	Evropská
populace	populace	k1gFnSc2	populace
je	být	k5eAaImIp3nS	být
soustředěna	soustředit	k5eAaPmNgFnS	soustředit
do	do	k7c2	do
východní	východní	k2eAgFnSc2d1	východní
části	část	k1gFnSc2	část
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
výsledků	výsledek	k1gInPc2	výsledek
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
sčítání	sčítání	k1gNnSc2	sčítání
z	z	k7c2	z
let	léto	k1gNnPc2	léto
2004	[number]	k4	2004
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejvíce	nejvíce	k6eAd1	nejvíce
párů	pár	k1gInPc2	pár
hnízdí	hnízdit	k5eAaImIp3nP	hnízdit
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
(	(	kIx(	(
<g/>
52	[number]	k4	52
500	[number]	k4	500
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Španělsku	Španělsko	k1gNnSc6	Španělsko
(	(	kIx(	(
<g/>
33	[number]	k4	33
217	[number]	k4	217
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
(	(	kIx(	(
<g/>
30	[number]	k4	30
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
Bělorusku	Bělorusko	k1gNnSc6	Bělorusko
(	(	kIx(	(
<g/>
20	[number]	k4	20
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Litvě	Litva	k1gFnSc6	Litva
(	(	kIx(	(
<g/>
13	[number]	k4	13
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Lotyšsku	Lotyšsko	k1gNnSc6	Lotyšsko
(	(	kIx(	(
<g/>
10	[number]	k4	10
700	[number]	k4	700
<g/>
)	)	kIx)	)
a	a	k8xC	a
Rusku	Rusko	k1gNnSc6	Rusko
(	(	kIx(	(
<g/>
10	[number]	k4	10
200	[number]	k4	200
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stavy	stav	k1gInPc1	stav
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
i	i	k9	i
přes	přes	k7c4	přes
intenzivní	intenzivní	k2eAgNnPc4d1	intenzivní
ochranná	ochranný	k2eAgNnPc4d1	ochranné
opatření	opatření	k1gNnPc4	opatření
nízké	nízký	k2eAgFnPc4d1	nízká
<g/>
;	;	kIx,	;
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
hnízdilo	hnízdit	k5eAaImAgNnS	hnízdit
973	[number]	k4	973
párů	pár	k1gInPc2	pár
<g/>
,	,	kIx,	,
v	v	k7c6	v
Nizozemsku	Nizozemsko	k1gNnSc6	Nizozemsko
528	[number]	k4	528
párů	pár	k1gInPc2	pár
<g/>
,	,	kIx,	,
v	v	k7c6	v
Dánsku	Dánsko	k1gNnSc6	Dánsko
jen	jen	k9	jen
3	[number]	k4	3
páry	pár	k1gInPc4	pár
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
ČR	ČR	kA	ČR
hnízdil	hnízdit	k5eAaImAgMnS	hnízdit
čáp	čáp	k1gMnSc1	čáp
bílý	bílý	k1gMnSc1	bílý
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jen	jen	k9	jen
vzácně	vzácně	k6eAd1	vzácně
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
prakticky	prakticky	k6eAd1	prakticky
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
Třeboňsku	Třeboňsko	k1gNnSc6	Třeboňsko
<g/>
,	,	kIx,	,
Písecku	Písecko	k1gNnSc6	Písecko
a	a	k8xC	a
jižní	jižní	k2eAgFnSc6d1	jižní
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Šířit	šířit	k5eAaImF	šířit
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
až	až	k9	až
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
;	;	kIx,	;
při	při	k7c6	při
prvním	první	k4xOgInSc6	první
sčítání	sčítání	k1gNnSc6	sčítání
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1934	[number]	k4	1934
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
celém	celý	k2eAgNnSc6d1	celé
území	území	k1gNnSc6	území
194	[number]	k4	194
hnízd	hnízdo	k1gNnPc2	hnízdo
s	s	k7c7	s
mláďaty	mládě	k1gNnPc7	mládě
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
byla	být	k5eAaImAgFnS	být
velikost	velikost	k1gFnSc1	velikost
celkové	celkový	k2eAgFnSc2d1	celková
populace	populace	k1gFnSc2	populace
odhadnuta	odhadnut	k2eAgFnSc1d1	odhadnuta
na	na	k7c4	na
459	[number]	k4	459
párů	pár	k1gInPc2	pár
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
začalo	začít	k5eAaPmAgNnS	začít
pravidelné	pravidelný	k2eAgNnSc4d1	pravidelné
každoroční	každoroční	k2eAgNnSc4d1	každoroční
sčítání	sčítání	k1gNnSc4	sčítání
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
na	na	k7c4	na
648	[number]	k4	648
párů	pár	k1gInPc2	pár
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
bylo	být	k5eAaImAgNnS	být
zaznamenáno	zaznamenat	k5eAaPmNgNnS	zaznamenat
1282	[number]	k4	1282
hnízd	hnízdo	k1gNnPc2	hnízdo
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yQnSc2	což
bylo	být	k5eAaImAgNnS	být
obsazeno	obsadit	k5eAaPmNgNnS	obsadit
celým	celý	k2eAgInSc7d1	celý
párem	pár	k1gInSc7	pár
901	[number]	k4	901
hnízd	hnízdo	k1gNnPc2	hnízdo
a	a	k8xC	a
alespoň	alespoň	k9	alespoň
jedno	jeden	k4xCgNnSc1	jeden
mládě	mládě	k1gNnSc1	mládě
bylo	být	k5eAaImAgNnS	být
vyvedeno	vyvést	k5eAaPmNgNnS	vyvést
z	z	k7c2	z
634	[number]	k4	634
hnízd	hnízdo	k1gNnPc2	hnízdo
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
vyhlášky	vyhláška	k1gFnSc2	vyhláška
č.	č.	k?	č.
395	[number]	k4	395
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ČR	ČR	kA	ČR
zvláště	zvláště	k6eAd1	zvláště
chráněn	chránit	k5eAaImNgInS	chránit
jako	jako	k8xC	jako
ohrožený	ohrožený	k2eAgInSc1d1	ohrožený
druh	druh	k1gInSc1	druh
<g/>
.	.	kIx.	.
</s>
<s>
Čáp	Čáp	k1gMnSc1	Čáp
bílý	bílý	k1gMnSc1	bílý
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
životu	život	k1gInSc3	život
v	v	k7c6	v
těsné	těsný	k2eAgFnSc6d1	těsná
blízkosti	blízkost	k1gFnSc6	blízkost
lidí	člověk	k1gMnPc2	člověk
stal	stát	k5eAaPmAgMnS	stát
významnou	významný	k2eAgFnSc7d1	významná
součástí	součást	k1gFnSc7	součást
lidové	lidový	k2eAgFnSc2d1	lidová
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
folklóru	folklór	k1gInSc2	folklór
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
starověkém	starověký	k2eAgInSc6d1	starověký
Egyptě	Egypt	k1gInSc6	Egypt
symbolizoval	symbolizovat	k5eAaImAgMnS	symbolizovat
zbožnost	zbožnost	k1gFnSc4	zbožnost
a	a	k8xC	a
spravedlnost	spravedlnost	k1gFnSc4	spravedlnost
<g/>
,	,	kIx,	,
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
nesmrtelnost	nesmrtelnost	k1gFnSc4	nesmrtelnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
muslimských	muslimský	k2eAgFnPc6d1	muslimská
zemích	zem	k1gFnPc6	zem
čápy	čáp	k1gMnPc4	čáp
nepronásledovali	pronásledovat	k5eNaImAgMnP	pronásledovat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
věřili	věřit	k5eAaImAgMnP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
ukrývá	ukrývat	k5eAaImIp3nS	ukrývat
duše	duše	k1gFnSc1	duše
muslima	muslim	k1gMnSc2	muslim
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
nepodnikl	podniknout	k5eNaPmAgMnS	podniknout
pouť	pouť	k1gFnSc4	pouť
do	do	k7c2	do
Mekky	Mekka	k1gFnSc2	Mekka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středověké	středověký	k2eAgFnSc6d1	středověká
Evropě	Evropa	k1gFnSc6	Evropa
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gFnSc1	jeho
užitečnost	užitečnost	k1gFnSc1	užitečnost
shledávána	shledávat	k5eAaImNgFnS	shledávat
především	především	k9	především
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
loví	lovit	k5eAaImIp3nP	lovit
obojživelníky	obojživelník	k1gMnPc4	obojživelník
a	a	k8xC	a
plazy	plaz	k1gMnPc4	plaz
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
byli	být	k5eAaImAgMnP	být
považováni	považován	k2eAgMnPc1d1	považován
za	za	k7c4	za
nežádoucí	žádoucí	k2eNgFnSc4d1	nežádoucí
havěť	havěť	k1gFnSc4	havěť
spřízněnou	spřízněný	k2eAgFnSc4d1	spřízněná
s	s	k7c7	s
peklem	peklo	k1gNnSc7	peklo
a	a	k8xC	a
černou	černý	k2eAgFnSc7d1	černá
magií	magie	k1gFnSc7	magie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
řadě	řada	k1gFnSc6	řada
zemí	zem	k1gFnPc2	zem
byl	být	k5eAaImAgInS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
symbol	symbol	k1gInSc4	symbol
štěstí	štěstí	k1gNnSc2	štěstí
<g/>
,	,	kIx,	,
zdraví	zdraví	k1gNnSc2	zdraví
a	a	k8xC	a
Božího	boží	k2eAgNnSc2d1	boží
požehnání	požehnání	k1gNnSc2	požehnání
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
ztělesněním	ztělesnění	k1gNnSc7	ztělesnění
věrnosti	věrnost	k1gFnSc2	věrnost
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
páru	pár	k1gInSc2	pár
a	a	k8xC	a
věrnosti	věrnost	k1gFnSc2	věrnost
hospodářství	hospodářství	k1gNnSc2	hospodářství
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gFnSc6	jehož
střeše	střecha	k1gFnSc6	střecha
hnízdil	hnízdit	k5eAaImAgMnS	hnízdit
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
byl	být	k5eAaImAgMnS	být
podle	podle	k7c2	podle
údajů	údaj	k1gInPc2	údaj
z	z	k7c2	z
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
na	na	k7c6	na
tahu	tah	k1gInSc6	tah
běžně	běžně	k6eAd1	běžně
ve	v	k7c6	v
velkém	velký	k2eAgInSc6d1	velký
odstřelován	odstřelovat	k5eAaImNgMnS	odstřelovat
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
již	již	k9	již
pro	pro	k7c4	pro
zábavu	zábava	k1gFnSc4	zábava
<g/>
,	,	kIx,	,
či	či	k8xC	či
pro	pro	k7c4	pro
maso	maso	k1gNnSc4	maso
<g/>
.	.	kIx.	.
</s>
<s>
Zřejmě	zřejmě	k6eAd1	zřejmě
nejznámější	známý	k2eAgFnSc7d3	nejznámější
evropskou	evropský	k2eAgFnSc7d1	Evropská
pověrou	pověra	k1gFnSc7	pověra
spojenou	spojený	k2eAgFnSc7d1	spojená
s	s	k7c7	s
čápem	čáp	k1gMnSc7	čáp
je	být	k5eAaImIp3nS	být
ta	ten	k3xDgFnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nosí	nosit	k5eAaImIp3nP	nosit
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
germánské	germánský	k2eAgFnSc6d1	germánská
mytologii	mytologie	k1gFnSc6	mytologie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
čáp	čáp	k1gMnSc1	čáp
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
posla	posel	k1gMnSc4	posel
bohyně	bohyně	k1gFnSc2	bohyně
Holdy	hold	k1gInPc4	hold
<g/>
,	,	kIx,	,
na	na	k7c4	na
jejíž	jejíž	k3xOyRp3gInSc4	jejíž
pokyn	pokyn	k1gInSc4	pokyn
lovil	lovit	k5eAaImAgMnS	lovit
duše	duše	k1gFnPc4	duše
nenarozených	narozený	k2eNgFnPc2d1	nenarozená
z	z	k7c2	z
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
vkládal	vkládat	k5eAaImAgMnS	vkládat
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
těla	tělo	k1gNnSc2	tělo
matky	matka	k1gFnSc2	matka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lidové	lidový	k2eAgFnSc6d1	lidová
tradici	tradice	k1gFnSc6	tradice
je	být	k5eAaImIp3nS	být
dobře	dobře	k6eAd1	dobře
znám	znám	k2eAgInSc1d1	znám
též	též	k9	též
jako	jako	k8xC	jako
posel	posel	k1gMnSc1	posel
jara	jaro	k1gNnSc2	jaro
<g/>
,	,	kIx,	,
o	o	k7c6	o
čemž	což	k3yQnSc6	což
svědčí	svědčit	k5eAaImIp3nS	svědčit
i	i	k9	i
česká	český	k2eAgFnSc1d1	Česká
pranostika	pranostika	k1gFnSc1	pranostika
"	"	kIx"	"
<g/>
na	na	k7c4	na
svatého	svatý	k2eAgMnSc4d1	svatý
Řehoře	Řehoř	k1gMnSc4	Řehoř
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
čáp	čáp	k1gMnSc1	čáp
letí	letět	k5eAaImIp3nS	letět
za	za	k7c4	za
moře	moře	k1gNnSc4	moře
<g/>
,	,	kIx,	,
žába	žába	k1gFnSc1	žába
hubu	huba	k1gFnSc4	huba
otevře	otevřít	k5eAaPmIp3nS	otevřít
<g/>
,	,	kIx,	,
líný	líný	k2eAgMnSc1d1	líný
sedlák	sedlák	k1gMnSc1	sedlák
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
neoře	orat	k5eNaImIp3nS	orat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Čáp	Čáp	k1gMnSc1	Čáp
bílý	bílý	k1gMnSc1	bílý
je	být	k5eAaImIp3nS	být
národním	národní	k2eAgMnSc7d1	národní
ptákem	pták	k1gMnSc7	pták
Litvy	Litva	k1gFnSc2	Litva
<g/>
,	,	kIx,	,
Polska	Polsko	k1gNnSc2	Polsko
a	a	k8xC	a
Běloruska	Bělorusko	k1gNnSc2	Bělorusko
a	a	k8xC	a
objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
ve	v	k7c6	v
znaku	znak	k1gInSc6	znak
Haagu	Haag	k1gInSc2	Haag
a	a	k8xC	a
řady	řada	k1gFnSc2	řada
jiných	jiný	k2eAgNnPc2d1	jiné
evropských	evropský	k2eAgNnPc2d1	Evropské
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Čáp	Čáp	k1gMnSc1	Čáp
bílý	bílý	k1gMnSc1	bílý
je	být	k5eAaImIp3nS	být
chován	chovat	k5eAaImNgInS	chovat
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
českých	český	k2eAgFnPc6d1	Česká
zoologických	zoologický	k2eAgFnPc6d1	zoologická
zahradách	zahrada	k1gFnPc6	zahrada
(	(	kIx(	(
<g/>
např.	např.	kA	např.
ZOO	zoo	k1gNnSc1	zoo
Děčín	Děčín	k1gInSc1	Děčín
<g/>
,	,	kIx,	,
ZOO	zoo	k1gFnSc1	zoo
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
společnost	společnost	k1gFnSc1	společnost
ornitologická	ornitologický	k2eAgFnSc1d1	ornitologická
–	–	k?	–
Pták	pták	k1gMnSc1	pták
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
Pták	pták	k1gMnSc1	pták
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
(	(	kIx(	(
<g/>
společně	společně	k6eAd1	společně
s	s	k7c7	s
čápem	čáp	k1gMnSc7	čáp
černým	černý	k1gMnSc7	černý
<g/>
)	)	kIx)	)
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
ornitologická	ornitologický	k2eAgFnSc1d1	ornitologická
spoločnosť	spoločnostit	k5eAaPmRp2nS	spoločnostit
–	–	k?	–
Pták	pták	k1gMnSc1	pták
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
Naturschutzbund	Naturschutzbund	k1gInSc1	Naturschutzbund
Deutschland	Deutschland	k1gInSc4	Deutschland
(	(	kIx(	(
<g/>
NABU	NABU	kA	NABU
<g/>
)	)	kIx)	)
–	–	k?	–
Pták	pták	k1gMnSc1	pták
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
a	a	k8xC	a
1994	[number]	k4	1994
Magyar	Magyar	k1gMnSc1	Magyar
Madártani	Madártan	k1gMnPc1	Madártan
és	és	k?	és
Természetvédelmi	Természetvédel	k1gFnPc7	Természetvédel
Egyesület	Egyesület	k1gInSc1	Egyesület
(	(	kIx(	(
<g/>
MME	MME	kA	MME
<g/>
)	)	kIx)	)
–	–	k?	–
Pták	pták	k1gMnSc1	pták
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
<g/>
,	,	kIx,	,
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
a	a	k8xC	a
1999	[number]	k4	1999
Estonsko	Estonsko	k1gNnSc4	Estonsko
<g/>
,	,	kIx,	,
Lotyšsko	Lotyšsko	k1gNnSc1	Lotyšsko
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
–	–	k?	–
Pták	pták	k1gMnSc1	pták
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
Dungel	Dungel	k1gInSc1	Dungel
J.	J.	kA	J.
<g/>
,	,	kIx,	,
Hudec	Hudec	k1gMnSc1	Hudec
K.	K.	kA	K.
<g/>
:	:	kIx,	:
Atlas	Atlas	k1gInSc1	Atlas
ptáků	pták	k1gMnPc2	pták
České	český	k2eAgFnSc2d1	Česká
a	a	k8xC	a
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Šťastný	Šťastný	k1gMnSc1	Šťastný
K.	K.	kA	K.
<g/>
,	,	kIx,	,
Bejček	Bejček	k1gMnSc1	Bejček
V.	V.	kA	V.
<g/>
,	,	kIx,	,
Hudec	Hudec	k1gMnSc1	Hudec
K.	K.	kA	K.
<g/>
:	:	kIx,	:
Svět	svět	k1gInSc1	svět
zvířat	zvíře	k1gNnPc2	zvíře
IV	Iva	k1gFnPc2	Iva
-	-	kIx~	-
Ptáci	pták	k1gMnPc1	pták
<g/>
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Albatros	albatros	k1gMnSc1	albatros
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Čáp	Čáp	k1gMnSc1	Čáp
bílý	bílý	k2eAgMnSc1d1	bílý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Galerie	galerie	k1gFnSc1	galerie
Čáp	Čáp	k1gMnSc1	Čáp
bílý	bílý	k2eAgMnSc1d1	bílý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
čáp	čáp	k1gMnSc1	čáp
bílý	bílý	k1gMnSc1	bílý
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Taxon	taxon	k1gInSc1	taxon
Ciconia	Ciconium	k1gNnSc2	Ciconium
ciconia	ciconium	k1gNnSc2	ciconium
ve	v	k7c6	v
Wikidruzích	Wikidruh	k1gInPc6	Wikidruh
Biolib	Bioliba	k1gFnPc2	Bioliba
hlasová	hlasový	k2eAgFnSc1d1	hlasová
ukázka	ukázka	k1gFnSc1	ukázka
z	z	k7c2	z
projektu	projekt	k1gInSc2	projekt
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
Hlas	hlas	k1gInSc1	hlas
na	na	k7c4	na
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
Interaktivní	interaktivní	k2eAgFnSc1d1	interaktivní
mapa	mapa	k1gFnSc1	mapa
hnízd	hnízdo	k1gNnPc2	hnízdo
v	v	k7c6	v
ČR	ČR	kA	ČR
Webkamera	Webkamera	k1gFnSc1	Webkamera
-	-	kIx~	-
hnízdo	hnízdo	k1gNnSc1	hnízdo
TANAPu	TANAPus	k1gInSc2	TANAPus
Webkamera	Webkamera	k1gFnSc1	Webkamera
-	-	kIx~	-
na	na	k7c6	na
komíně	komín	k1gInSc6	komín
v	v	k7c6	v
Teplé	Teplá	k1gFnSc6	Teplá
v	v	k7c6	v
Karlovarském	karlovarský	k2eAgInSc6d1	karlovarský
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Webkamera	Webkamera	k1gFnSc1	Webkamera
-	-	kIx~	-
hnízdo	hnízdo	k1gNnSc1	hnízdo
v	v	k7c6	v
Holandsku	Holandsko	k1gNnSc6	Holandsko
Webkamera	Webkamero	k1gNnSc2	Webkamero
-	-	kIx~	-
hnízdo	hnízdo	k1gNnSc1	hnízdo
Čápů	Čáp	k1gMnPc2	Čáp
Bílých	bílý	k2eAgMnPc2d1	bílý
na	na	k7c6	na
komíně	komín	k1gInSc6	komín
Základní	základní	k2eAgFnSc2d1	základní
školy	škola	k1gFnSc2	škola
v	v	k7c6	v
Dlouhé	Dlouhé	k2eAgFnSc6d1	Dlouhé
Loučce	loučka	k1gFnSc6	loučka
Webkamera	Webkamera	k1gFnSc1	Webkamera
-	-	kIx~	-
ekologická	ekologický	k2eAgFnSc1d1	ekologická
výuka	výuka	k1gFnSc1	výuka
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
monitorováním	monitorování	k1gNnSc7	monitorování
Čápa	Čáp	k1gMnSc2	Čáp
bílého	bílý	k1gMnSc2	bílý
v	v	k7c6	v
Międzyrzeczu	Międzyrzecz	k1gInSc6	Międzyrzecz
Górnym	Górnym	k1gInSc1	Górnym
i	i	k8xC	i
Dolnym	Dolnym	k1gInSc1	Dolnym
Webkamera	Webkamer	k1gMnSc2	Webkamer
-	-	kIx~	-
tři	tři	k4xCgFnPc1	tři
čapí	čapí	k2eAgFnPc1d1	čapí
hnízda	hnízdo	k1gNnPc1	hnízdo
on-line	onin	k1gInSc5	on-lin
s	s	k7c7	s
infra	infra	k1gMnSc1	infra
přenosem	přenos	k1gInSc7	přenos
jednou	jednou	k6eAd1	jednou
kamerou	kamera	k1gFnSc7	kamera
ze	z	k7c2	z
Supíkovic	Supíkovice	k1gFnPc2	Supíkovice
Webkamera	Webkamero	k1gNnSc2	Webkamero
-	-	kIx~	-
hnízdo	hnízdo	k1gNnSc1	hnízdo
Čápů	Čáp	k1gMnPc2	Čáp
bílých	bílý	k2eAgMnPc2d1	bílý
na	na	k7c6	na
komíně	komín	k1gInSc6	komín
v	v	k7c6	v
Kralicích	Kralice	k1gFnPc6	Kralice
Webkamera	Webkamero	k1gNnSc2	Webkamero
-	-	kIx~	-
Hnízdění	hnízdění	k1gNnPc2	hnízdění
handicapovaných	handicapovaný	k2eAgMnPc2d1	handicapovaný
čápů	čáp	k1gMnPc2	čáp
bílých	bílý	k2eAgMnPc2d1	bílý
-	-	kIx~	-
Záchranná	záchranný	k2eAgFnSc1d1	záchranná
stanice	stanice	k1gFnSc1	stanice
živočichů	živočich	k1gMnPc2	živočich
MAKOV	MAKOV	kA	MAKOV
</s>
