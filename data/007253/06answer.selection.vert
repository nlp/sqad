<s>
Rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
se	se	k3xPyFc4	se
2	[number]	k4	2
poddruhy	poddruh	k1gInPc7	poddruh
<g/>
:	:	kIx,	:
čáp	čáp	k1gMnSc1	čáp
bílý	bílý	k1gMnSc1	bílý
evropský	evropský	k2eAgMnSc1d1	evropský
(	(	kIx(	(
<g/>
Ciconia	Ciconium	k1gNnSc2	Ciconium
ciconia	ciconium	k1gNnSc2	ciconium
ciconia	ciconium	k1gNnSc2	ciconium
<g/>
)	)	kIx)	)
-	-	kIx~	-
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
severozápadní	severozápadní	k2eAgFnSc6d1	severozápadní
Africe	Afrika	k1gFnSc6	Afrika
a	a	k8xC	a
Přední	přední	k2eAgFnSc3d1	přední
Asii	Asie	k1gFnSc3	Asie
<g/>
,	,	kIx,	,
zimuje	zimovat	k5eAaImIp3nS	zimovat
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
čáp	čáp	k1gMnSc1	čáp
bílý	bílý	k1gMnSc1	bílý
středoasijský	středoasijský	k2eAgMnSc1d1	středoasijský
(	(	kIx(	(
<g/>
Ciconia	Ciconium	k1gNnSc2	Ciconium
ciconia	ciconium	k1gNnSc2	ciconium
asiatica	asiaticum	k1gNnSc2	asiaticum
<g/>
)	)	kIx)	)
-	-	kIx~	-
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
zimuje	zimovat	k5eAaImIp3nS	zimovat
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
kontinentu	kontinent	k1gInSc2	kontinent
Dříve	dříve	k6eAd2	dříve
byl	být	k5eAaImAgMnS	být
mezi	mezi	k7c7	mezi
poddruhy	poddruh	k1gInPc7	poddruh
čápa	čáp	k1gMnSc2	čáp
bílého	bílý	k1gMnSc2	bílý
uváděn	uvádět	k5eAaImNgInS	uvádět
také	také	k9	také
čáp	čáp	k1gMnSc1	čáp
východní	východní	k2eAgMnSc1d1	východní
(	(	kIx(	(
<g/>
Ciconia	Ciconium	k1gNnSc2	Ciconium
boyciana	boyciana	k1gFnSc1	boyciana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
široce	široko	k6eAd1	široko
uznáván	uznáván	k2eAgInSc4d1	uznáván
jako	jako	k8xS	jako
samostatný	samostatný	k2eAgInSc4d1	samostatný
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
