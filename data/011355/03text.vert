<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
různobarevný	různobarevný	k2eAgInSc1d1	různobarevný
kus	kus	k1gInSc1	kus
látky	látka	k1gFnSc2	látka
<g/>
,	,	kIx,	,
obdélníkového	obdélníkový	k2eAgInSc2d1	obdélníkový
<g/>
,	,	kIx,	,
čtvercového	čtvercový	k2eAgInSc2d1	čtvercový
či	či	k8xC	či
jinak	jinak	k6eAd1	jinak
pevně	pevně	k6eAd1	pevně
stanoveného	stanovený	k2eAgInSc2d1	stanovený
tvaru	tvar	k1gInSc2	tvar
a	a	k8xC	a
poměru	poměr	k1gInSc2	poměr
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
organizaci	organizace	k1gFnSc4	organizace
(	(	kIx(	(
<g/>
typicky	typicky	k6eAd1	typicky
zejména	zejména	k9	zejména
stát	stát	k5eAaPmF	stát
či	či	k8xC	či
jinou	jiný	k2eAgFnSc4d1	jiná
územní	územní	k2eAgFnSc4d1	územní
správní	správní	k2eAgFnSc4d1	správní
korporaci	korporace	k1gFnSc4	korporace
jako	jako	k8xS	jako
zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
kraj	kraj	k1gInSc1	kraj
<g/>
,	,	kIx,	,
obec	obec	k1gFnSc1	obec
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
signalizaci	signalizace	k1gFnSc3	signalizace
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgFnPc1d1	státní
vlajky	vlajka	k1gFnPc1	vlajka
se	se	k3xPyFc4	se
často	často	k6eAd1	často
označují	označovat	k5eAaImIp3nP	označovat
též	též	k9	též
jako	jako	k9	jako
národní	národní	k2eAgFnSc1d1	národní
<g/>
.	.	kIx.	.
</s>
<s>
Nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
vlajkách	vlajka	k1gFnPc6	vlajka
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
vexilologie	vexilologie	k1gFnSc1	vexilologie
<g/>
.	.	kIx.	.
</s>
<s>
Vlajek	vlajka	k1gFnPc2	vlajka
se	se	k3xPyFc4	se
tradičně	tradičně	k6eAd1	tradičně
užívá	užívat	k5eAaImIp3nS	užívat
i	i	k9	i
jako	jako	k9	jako
praporů	prapor	k1gInPc2	prapor
<g/>
,	,	kIx,	,
prapor	prapor	k1gInSc1	prapor
se	se	k3xPyFc4	se
od	od	k7c2	od
vlajky	vlajka	k1gFnSc2	vlajka
liší	lišit	k5eAaImIp3nP	lišit
zejména	zejména	k9	zejména
způsobem	způsob	k1gInSc7	způsob
vyvěšení	vyvěšení	k1gNnSc2	vyvěšení
<g/>
.	.	kIx.	.
</s>
<s>
Grafická	grafický	k2eAgFnSc1d1	grafická
podoba	podoba	k1gFnSc1	podoba
vlajky	vlajka	k1gFnSc2	vlajka
se	se	k3xPyFc4	se
jako	jako	k9	jako
symbol	symbol	k1gInSc1	symbol
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
podobách	podoba	k1gFnPc6	podoba
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
tištěné	tištěný	k2eAgFnPc1d1	tištěná
na	na	k7c6	na
papíře	papír	k1gInSc6	papír
či	či	k8xC	či
zobrazené	zobrazený	k2eAgFnPc4d1	zobrazená
na	na	k7c6	na
obrazovce	obrazovka	k1gFnSc6	obrazovka
(	(	kIx(	(
<g/>
monitoru	monitor	k1gInSc6	monitor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
k	k	k7c3	k
označení	označení	k1gNnSc3	označení
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
národa	národ	k1gInSc2	národ
<g/>
,	,	kIx,	,
jazyku	jazyk	k1gMnSc3	jazyk
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
vlajkou	vlajka	k1gFnSc7	vlajka
a	a	k8xC	a
praporem	prapor	k1gInSc7	prapor
==	==	k?	==
</s>
</p>
<p>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
laická	laický	k2eAgFnSc1d1	laická
veřejnost	veřejnost	k1gFnSc1	veřejnost
mezi	mezi	k7c7	mezi
pojmy	pojem	k1gInPc1	pojem
prapor	prapor	k1gInSc1	prapor
a	a	k8xC	a
vlajka	vlajka	k1gFnSc1	vlajka
nijak	nijak	k6eAd1	nijak
nerozlišuje	rozlišovat	k5eNaImIp3nS	rozlišovat
<g/>
,	,	kIx,	,
u	u	k7c2	u
symbolů	symbol	k1gInPc2	symbol
státních	státní	k2eAgInPc2d1	státní
a	a	k8xC	a
jiných	jiný	k2eAgInPc2d1	jiný
podobně	podobně	k6eAd1	podobně
významných	významný	k2eAgInPc2d1	významný
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
prapor	prapor	k1gInSc4	prapor
a	a	k8xC	a
vlajku	vlajka	k1gFnSc4	vlajka
rozlišovat	rozlišovat	k5eAaImF	rozlišovat
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
vlajku	vlajka	k1gFnSc4	vlajka
je	být	k5eAaImIp3nS	být
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
vodorovná	vodorovný	k2eAgFnSc1d1	vodorovná
poloha	poloha	k1gFnSc1	poloha
a	a	k8xC	a
upevnění	upevnění	k1gNnSc1	upevnění
k	k	k7c3	k
vlajkové	vlajkový	k2eAgFnSc3d1	vlajková
tyči	tyč	k1gFnSc3	tyč
či	či	k8xC	či
stožáru	stožár	k1gInSc2	stožár
pomocí	pomocí	k7c2	pomocí
lanka	lanko	k1gNnSc2	lanko
<g/>
,	,	kIx,	,
prapor	prapor	k1gInSc1	prapor
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
žerdi	žerď	k1gFnSc3	žerď
přichycen	přichycen	k2eAgInSc1d1	přichycen
napevno	napevno	k6eAd1	napevno
<g/>
.	.	kIx.	.
</s>
<s>
Prapor	prapor	k1gInSc1	prapor
nemá	mít	k5eNaImIp3nS	mít
oproti	oproti	k7c3	oproti
vlajce	vlajka	k1gFnSc3	vlajka
předepsaný	předepsaný	k2eAgInSc4d1	předepsaný
poměr	poměr	k1gInSc4	poměr
stran	strana	k1gFnPc2	strana
a	a	k8xC	a
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
ve	v	k7c6	v
vodorovné	vodorovný	k2eAgFnSc6d1	vodorovná
<g/>
,	,	kIx,	,
šikmé	šikmý	k2eAgFnSc3d1	šikmá
i	i	k8xC	i
svislé	svislý	k2eAgFnSc3d1	svislá
poloze	poloha	k1gFnSc3	poloha
<g/>
.	.	kIx.	.
</s>
<s>
Prapory	prapor	k1gInPc1	prapor
(	(	kIx(	(
<g/>
řidčeji	řídce	k6eAd2	řídce
vlajky	vlajka	k1gFnPc4	vlajka
<g/>
)	)	kIx)	)
existující	existující	k2eAgFnSc4d1	existující
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
jediném	jediný	k2eAgInSc6d1	jediný
exempláři	exemplář	k1gInSc6	exemplář
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
jako	jako	k8xC	jako
symbol	symbol	k1gInSc1	symbol
konkrétní	konkrétní	k2eAgFnSc2d1	konkrétní
vojenské	vojenský	k2eAgFnSc2d1	vojenská
jednotky	jednotka	k1gFnSc2	jednotka
či	či	k8xC	či
cechu	cech	k1gInSc2	cech
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
označují	označovat	k5eAaImIp3nP	označovat
slovem	slovem	k6eAd1	slovem
zástava	zástava	k1gFnSc1	zástava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Laická	laický	k2eAgFnSc1d1	laická
veřejnost	veřejnost	k1gFnSc1	veřejnost
mezi	mezi	k7c7	mezi
praporem	prapor	k1gInSc7	prapor
a	a	k8xC	a
vlajkou	vlajka	k1gFnSc7	vlajka
obyčejně	obyčejně	k6eAd1	obyčejně
nerozlišuje	rozlišovat	k5eNaImIp3nS	rozlišovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
literární	literární	k2eAgFnSc6d1	literární
a	a	k8xC	a
filmové	filmový	k2eAgFnSc3d1	filmová
tvorbě	tvorba	k1gFnSc3	tvorba
dokonce	dokonce	k9	dokonce
někdy	někdy	k6eAd1	někdy
převládá	převládat	k5eAaImIp3nS	převládat
užívání	užívání	k1gNnSc2	užívání
slova	slovo	k1gNnSc2	slovo
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
románový	románový	k2eAgInSc4d1	románový
klub	klub	k1gInSc4	klub
Rychlé	Rychlé	k2eAgInPc4d1	Rychlé
šípy	šíp	k1gInPc4	šíp
má	mít	k5eAaImIp3nS	mít
svou	svůj	k3xOyFgFnSc4	svůj
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
formálně	formálně	k6eAd1	formálně
vzato	vzít	k5eAaPmNgNnS	vzít
spíše	spíše	k9	spíše
praporem	prapor	k1gInSc7	prapor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Jestliže	jestliže	k8xS	jestliže
se	se	k3xPyFc4	se
vlajky	vlajka	k1gFnPc1	vlajka
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
vytahované	vytahovaný	k2eAgFnPc1d1	vytahovaná
na	na	k7c4	na
stožár	stožár	k1gInSc4	stožár
lankem	lanko	k1gNnSc7	lanko
<g/>
)	)	kIx)	)
vyvinuly	vyvinout	k5eAaPmAgInP	vyvinout
až	až	k6eAd1	až
počátkem	počátkem	k7c2	počátkem
novověku	novověk	k1gInSc2	novověk
na	na	k7c6	na
námořních	námořní	k2eAgFnPc6d1	námořní
lodích	loď	k1gFnPc6	loď
<g/>
,	,	kIx,	,
původ	původ	k1gInSc1	původ
jejich	jejich	k3xOp3gMnPc2	jejich
předchůdců	předchůdce	k1gMnPc2	předchůdce
sahá	sahat	k5eAaImIp3nS	sahat
až	až	k9	až
do	do	k7c2	do
starověku	starověk	k1gInSc2	starověk
<g/>
.	.	kIx.	.
</s>
<s>
Existence	existence	k1gFnSc1	existence
na	na	k7c6	na
žerdi	žerď	k1gFnSc6	žerď
upevněných	upevněný	k2eAgInPc2d1	upevněný
předmětů	předmět	k1gInPc2	předmět
či	či	k8xC	či
vyobrazení	vyobrazení	k1gNnPc2	vyobrazení
<g/>
,	,	kIx,	,
označovaných	označovaný	k2eAgInPc2d1	označovaný
odborným	odborný	k2eAgInSc7d1	odborný
výrazem	výraz	k1gInSc7	výraz
vexilloid	vexilloid	k1gInSc1	vexilloid
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
doložena	doložit	k5eAaPmNgFnS	doložit
už	už	k6eAd1	už
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
3	[number]	k4	3
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
a	a	k8xC	a
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
stoletích	století	k1gNnPc6	století
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
Blízkém	blízký	k2eAgInSc6d1	blízký
východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
zprávy	zpráva	k1gFnPc1	zpráva
o	o	k7c6	o
látkovém	látkový	k2eAgInSc6d1	látkový
praporu	prapor	k1gInSc6	prapor
máme	mít	k5eAaImIp1nP	mít
z	z	k7c2	z
počátků	počátek	k1gInPc2	počátek
dynastie	dynastie	k1gFnSc2	dynastie
Čou	Čou	k1gFnSc2	Čou
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
čínském	čínský	k2eAgInSc6d1	čínský
Ma-wang-tuej	Maanguej	k1gInSc1	Ma-wang-tuej
byl	být	k5eAaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
i	i	k8xC	i
nejstarší	starý	k2eAgInSc1d3	nejstarší
dochovaný	dochovaný	k2eAgInSc1d1	dochovaný
prapor	prapor	k1gInSc1	prapor
–	–	k?	–
rozměrný	rozměrný	k2eAgInSc1d1	rozměrný
hedvábný	hedvábný	k2eAgInSc1d1	hedvábný
praporec	praporec	k1gInSc1	praporec
byl	být	k5eAaImAgInS	být
uložen	uložit	k5eAaPmNgInS	uložit
do	do	k7c2	do
výbavy	výbava	k1gFnSc2	výbava
šlechtického	šlechtický	k2eAgInSc2d1	šlechtický
hrobu	hrob	k1gInSc2	hrob
za	za	k7c2	za
dynastie	dynastie	k1gFnSc2	dynastie
Chan	Chan	k1gInSc1	Chan
ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
starém	starý	k2eAgInSc6d1	starý
Římě	Řím	k1gInSc6	Řím
zavedl	zavést	k5eAaPmAgInS	zavést
roku	rok	k1gInSc2	rok
105	[number]	k4	105
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Gaius	Gaius	k1gMnSc1	Gaius
Marius	Marius	k1gMnSc1	Marius
symboly	symbol	k1gInPc7	symbol
legií	legie	k1gFnSc7	legie
zvaná	zvaný	k2eAgNnPc1d1	zvané
vexilla	vexillo	k1gNnPc1	vexillo
<g/>
.	.	kIx.	.
</s>
<s>
Vexillum	Vexillum	k1gNnSc1	Vexillum
(	(	kIx(	(
<g/>
zdrobnělina	zdrobnělina	k1gFnSc1	zdrobnělina
z	z	k7c2	z
latinského	latinský	k2eAgInSc2d1	latinský
velum	velum	k1gNnSc1	velum
=	=	kIx~	=
plachta	plachta	k1gFnSc1	plachta
<g/>
)	)	kIx)	)
mělo	mít	k5eAaImAgNnS	mít
podobu	podoba	k1gFnSc4	podoba
čtvercového	čtvercový	k2eAgInSc2d1	čtvercový
kusu	kus	k1gInSc2	kus
látky	látka	k1gFnSc2	látka
<g/>
,	,	kIx,	,
uchyceného	uchycený	k2eAgInSc2d1	uchycený
na	na	k7c6	na
tyči	tyč	k1gFnSc6	tyč
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
písmene	písmeno	k1gNnSc2	písmeno
T.	T.	kA	T.
S	s	k7c7	s
nástupem	nástup	k1gInSc7	nástup
křesťanství	křesťanství	k1gNnSc2	křesťanství
za	za	k7c4	za
Konstantina	Konstantin	k1gMnSc4	Konstantin
I.	I.	kA	I.
počátkem	počátek	k1gInSc7	počátek
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
vexillum	vexillum	k1gNnSc1	vexillum
proměnilo	proměnit	k5eAaPmAgNnS	proměnit
v	v	k7c4	v
labarum	labarum	k1gInSc4	labarum
–	–	k?	–
dosavadní	dosavadní	k2eAgInPc1d1	dosavadní
"	"	kIx"	"
<g/>
pohanské	pohanský	k2eAgInPc1d1	pohanský
<g/>
"	"	kIx"	"
symboly	symbol	k1gInPc1	symbol
byly	být	k5eAaImAgInP	být
vystřídány	vystřídat	k5eAaPmNgInP	vystřídat
křesťanskými	křesťanský	k2eAgFnPc7d1	křesťanská
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Kristovým	Kristův	k2eAgInSc7d1	Kristův
monogramem	monogram	k1gInSc7	monogram
XP	XP	kA	XP
[	[	kIx(	[
<g/>
chí	chí	k1gNnSc1	chí
ró	ró	k1gNnSc2	ró
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Prapory	prapor	k1gInPc1	prapor
dnešního	dnešní	k2eAgInSc2d1	dnešní
vzhledu	vzhled	k1gInSc2	vzhled
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
jest	být	k5eAaImIp3nS	být
upevněné	upevněný	k2eAgNnSc1d1	upevněné
k	k	k7c3	k
žerdi	žerď	k1gFnSc3	žerď
ne	ne	k9	ne
horním	horní	k2eAgInSc7d1	horní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bočním	boční	k2eAgInSc7d1	boční
okrajem	okraj	k1gInSc7	okraj
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
dostaly	dostat	k5eAaPmAgInP	dostat
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
z	z	k7c2	z
východu	východ	k1gInSc2	východ
od	od	k7c2	od
Hunů	Hun	k1gMnPc2	Hun
<g/>
,	,	kIx,	,
Peršanů	Peršan	k1gMnPc2	Peršan
a	a	k8xC	a
Arabů	Arab	k1gMnPc2	Arab
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
raném	raný	k2eAgInSc6d1	raný
středověku	středověk	k1gInSc6	středověk
byl	být	k5eAaImAgInS	být
obzvlášť	obzvlášť	k6eAd1	obzvlášť
rozšířeným	rozšířený	k2eAgInSc7d1	rozšířený
typem	typ	k1gInSc7	typ
praporů	prapor	k1gInPc2	prapor
gonfanon	gonfanon	k1gNnSc1	gonfanon
<g/>
,	,	kIx,	,
obdélník	obdélník	k1gInSc1	obdélník
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
vlající	vlající	k2eAgFnSc1d1	vlající
část	část	k1gFnSc1	část
je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
do	do	k7c2	do
několika	několik	k4yIc2	několik
protáhlých	protáhlý	k2eAgInPc2d1	protáhlý
pruhů	pruh	k1gInPc2	pruh
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
heraldiky	heraldika	k1gFnSc2	heraldika
ve	v	k7c6	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
symbolika	symbolika	k1gFnSc1	symbolika
šlechtických	šlechtický	k2eAgInPc2d1	šlechtický
znaků	znak	k1gInPc2	znak
přenášela	přenášet	k5eAaImAgFnS	přenášet
i	i	k9	i
na	na	k7c4	na
prapory	prapor	k1gInPc4	prapor
<g/>
,	,	kIx,	,
takovéto	takovýto	k3xDgNnSc1	takovýto
vlající	vlající	k2eAgNnSc1d1	vlající
zpodobnění	zpodobnění	k1gNnSc1	zpodobnění
znaku	znak	k1gInSc2	znak
<g/>
,	,	kIx,	,
typické	typický	k2eAgNnSc1d1	typické
na	na	k7c4	na
výšku	výška	k1gFnSc4	výška
protáhlým	protáhlý	k2eAgInSc7d1	protáhlý
obdélným	obdélný	k2eAgInSc7d1	obdélný
tvarem	tvar	k1gInSc7	tvar
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
banderium	banderium	k1gNnSc1	banderium
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
a	a	k8xC	a
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byly	být	k5eAaImAgFnP	být
velmi	velmi	k6eAd1	velmi
běžné	běžný	k2eAgFnPc4d1	běžná
korouhve	korouhev	k1gFnPc4	korouhev
<g/>
,	,	kIx,	,
prapory	prapor	k1gInPc4	prapor
<g/>
,	,	kIx,	,
uchycené	uchycený	k2eAgInPc4d1	uchycený
k	k	k7c3	k
žerdi	žerď	k1gFnSc3	žerď
bokem	bokem	k6eAd1	bokem
a	a	k8xC	a
ještě	ještě	k6eAd1	ještě
napnuté	napnutý	k2eAgFnPc1d1	napnutá
tyčkou	tyčka	k1gFnSc7	tyčka
u	u	k7c2	u
horního	horní	k2eAgInSc2d1	horní
okraje	okraj	k1gInSc2	okraj
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
s	s	k7c7	s
protaženým	protažený	k2eAgInSc7d1	protažený
horním	horní	k2eAgInSc7d1	horní
vlajícím	vlající	k2eAgInSc7d1	vlající
cípem	cíp	k1gInSc7	cíp
<g/>
.	.	kIx.	.
</s>
<s>
Prapory	prapor	k1gInPc1	prapor
hrály	hrát	k5eAaImAgInP	hrát
důležitou	důležitý	k2eAgFnSc4d1	důležitá
úlohu	úloha	k1gFnSc4	úloha
při	při	k7c6	při
rozlišení	rozlišení	k1gNnSc6	rozlišení
bojujících	bojující	k2eAgFnPc2d1	bojující
stran	strana	k1gFnPc2	strana
ve	v	k7c6	v
válkách	válka	k1gFnPc6	válka
(	(	kIx(	(
<g/>
ve	v	k7c6	v
feudálním	feudální	k2eAgNnSc6d1	feudální
Japonsku	Japonsko	k1gNnSc6	Japonsko
tak	tak	k6eAd1	tak
za	za	k7c2	za
bojů	boj	k1gInPc2	boj
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
nosili	nosit	k5eAaImAgMnP	nosit
jednotliví	jednotlivý	k2eAgMnPc1d1	jednotlivý
vojáci	voják	k1gMnPc1	voják
prapor	prapor	k1gInSc4	prapor
se	s	k7c7	s
symbolem	symbol	k1gInSc7	symbol
svého	svůj	k3xOyFgMnSc2	svůj
pána	pán	k1gMnSc2	pán
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sloužily	sloužit	k5eAaImAgFnP	sloužit
k	k	k7c3	k
orientaci	orientace	k1gFnSc3	orientace
v	v	k7c6	v
bitevní	bitevní	k2eAgFnSc6d1	bitevní
vřavě	vřava	k1gFnSc6	vřava
<g/>
.	.	kIx.	.
</s>
<s>
Plukovní	plukovní	k2eAgFnPc1d1	plukovní
zástavy	zástava	k1gFnPc1	zástava
byly	být	k5eAaImAgFnP	být
ceněnou	ceněný	k2eAgFnSc7d1	ceněná
trofejí	trofej	k1gFnSc7	trofej
a	a	k8xC	a
životnost	životnost	k1gFnSc1	životnost
praporečníků	praporečník	k1gMnPc2	praporečník
v	v	k7c6	v
boji	boj	k1gInSc6	boj
zpravidla	zpravidla	k6eAd1	zpravidla
mizivá	mizivý	k2eAgFnSc1d1	mizivá
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
funkce	funkce	k1gFnSc1	funkce
však	však	k9	však
pozbyla	pozbýt	k5eAaPmAgFnS	pozbýt
s	s	k7c7	s
přelomem	přelom	k1gInSc7	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
významu	význam	k1gInSc2	význam
<g/>
,	,	kIx,	,
armády	armáda	k1gFnSc2	armáda
již	již	k6eAd1	již
nebojovaly	bojovat	k5eNaImAgFnP	bojovat
v	v	k7c6	v
sevřených	sevřený	k2eAgFnPc6d1	sevřená
formacích	formace	k1gFnPc6	formace
pod	pod	k7c7	pod
prapory	prapor	k1gInPc7	prapor
a	a	k8xC	a
k	k	k7c3	k
předávání	předávání	k1gNnSc3	předávání
rozkazů	rozkaz	k1gInPc2	rozkaz
posloužil	posloužit	k5eAaPmAgMnS	posloužit
v	v	k7c6	v
zákopech	zákop	k1gInPc6	zákop
polní	polní	k2eAgInSc4d1	polní
telefon	telefon	k1gInSc4	telefon
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
jsou	být	k5eAaImIp3nP	být
tak	tak	k9	tak
prapory	prapor	k1gInPc1	prapor
pozemních	pozemní	k2eAgNnPc2d1	pozemní
vojsk	vojsko	k1gNnPc2	vojsko
už	už	k9	už
jen	jen	k9	jen
symbolickými	symbolický	k2eAgFnPc7d1	symbolická
zástavami	zástava	k1gFnPc7	zástava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
vývoj	vývoj	k1gInSc4	vývoj
státních	státní	k2eAgFnPc2d1	státní
vlajek	vlajka	k1gFnPc2	vlajka
měla	mít	k5eAaImAgFnS	mít
významný	významný	k2eAgInSc4d1	významný
vliv	vliv	k1gInSc4	vliv
i	i	k8xC	i
francouzská	francouzský	k2eAgFnSc1d1	francouzská
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Trikolora	trikolora	k1gFnSc1	trikolora
se	s	k7c7	s
svislými	svislý	k2eAgInPc7d1	svislý
pruhy	pruh	k1gInPc7	pruh
v	v	k7c6	v
barvě	barva	k1gFnSc6	barva
modré	modrý	k2eAgFnSc2d1	modrá
<g/>
,	,	kIx,	,
červené	červený	k2eAgFnSc2d1	červená
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
zrodila	zrodit	k5eAaPmAgFnS	zrodit
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
symbolem	symbol	k1gInSc7	symbol
svobody	svoboda	k1gFnSc2	svoboda
a	a	k8xC	a
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
podobu	podoba	k1gFnSc4	podoba
státních	státní	k2eAgFnPc2d1	státní
vlajek	vlajka	k1gFnPc2	vlajka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlajky	vlajka	k1gFnSc2	vlajka
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
moři	moře	k1gNnSc6	moře
bylo	být	k5eAaImAgNnS	být
označování	označování	k1gNnSc1	označování
státní	státní	k2eAgFnSc2d1	státní
příslušnosti	příslušnost	k1gFnSc2	příslušnost
lodi	loď	k1gFnSc2	loď
vlajkou	vlajka	k1gFnSc7	vlajka
běžné	běžný	k2eAgInPc1d1	běžný
již	již	k6eAd1	již
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc4	tento
zvyk	zvyk	k1gInSc4	zvyk
rozšířili	rozšířit	k5eAaPmAgMnP	rozšířit
zejména	zejména	k9	zejména
Holanďané	Holanďan	k1gMnPc1	Holanďan
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
vyvinuly	vyvinout	k5eAaPmAgFnP	vyvinout
zvláštní	zvláštní	k2eAgFnPc1d1	zvláštní
civilní	civilní	k2eAgFnPc1d1	civilní
a	a	k8xC	a
válečné	válečný	k2eAgFnPc1d1	válečná
námořní	námořní	k2eAgFnPc1d1	námořní
varianty	varianta	k1gFnPc1	varianta
státních	státní	k2eAgFnPc2d1	státní
vlajek	vlajka	k1gFnPc2	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Vlajky	vlajka	k1gFnPc1	vlajka
mají	mít	k5eAaImIp3nP	mít
i	i	k9	i
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
zásadní	zásadní	k2eAgInSc4d1	zásadní
význam	význam	k1gInSc4	význam
pro	pro	k7c4	pro
určení	určení	k1gNnSc4	určení
státní	státní	k2eAgFnSc2d1	státní
příslušnosti	příslušnost	k1gFnSc2	příslušnost
lodi	loď	k1gFnSc2	loď
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Častá	častý	k2eAgFnSc1d1	častá
praxe	praxe	k1gFnSc1	praxe
však	však	k9	však
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
asi	asi	k9	asi
od	od	k7c2	od
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nákladní	nákladní	k2eAgFnPc1d1	nákladní
lodě	loď	k1gFnPc1	loď
registrují	registrovat	k5eAaBmIp3nP	registrovat
pod	pod	k7c7	pod
vlajkou	vlajka	k1gFnSc7	vlajka
cizího	cizí	k2eAgInSc2d1	cizí
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
flag	flag	k1gInSc1	flag
of	of	k?	of
convenience	convenience	k1gFnSc1	convenience
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Primitivní	primitivní	k2eAgFnSc1d1	primitivní
námořní	námořní	k2eAgFnSc1d1	námořní
vlajková	vlajkový	k2eAgFnSc1d1	vlajková
signalizace	signalizace	k1gFnSc1	signalizace
se	se	k3xPyFc4	se
používala	používat	k5eAaImAgFnS	používat
už	už	k6eAd1	už
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
(	(	kIx(	(
<g/>
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Salamíny	Salamína	k1gFnSc2	Salamína
roku	rok	k1gInSc2	rok
480	[number]	k4	480
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
prý	prý	k9	prý
Themistoklés	Themistoklés	k1gInSc4	Themistoklés
dal	dát	k5eAaPmAgMnS	dát
signál	signál	k1gInSc4	signál
k	k	k7c3	k
útoku	útok	k1gInSc3	útok
rudým	rudý	k2eAgInSc7d1	rudý
kusem	kus	k1gInSc7	kus
látky	látka	k1gFnSc2	látka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
existuje	existovat	k5eAaImIp3nS	existovat
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
námořní	námořní	k2eAgFnSc1d1	námořní
vlajková	vlajkový	k2eAgFnSc1d1	vlajková
abeceda	abeceda	k1gFnSc1	abeceda
<g/>
,	,	kIx,	,
užívající	užívající	k2eAgFnSc4d1	užívající
kombinaci	kombinace	k1gFnSc4	kombinace
40	[number]	k4	40
signálních	signální	k2eAgFnPc2d1	signální
vlajek	vlajka	k1gFnPc2	vlajka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vzhled	vzhled	k1gInSc4	vzhled
==	==	k?	==
</s>
</p>
<p>
<s>
Dnešní	dnešní	k2eAgFnPc1d1	dnešní
vlajky	vlajka	k1gFnPc1	vlajka
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
pak	pak	k6eAd1	pak
státní	státní	k2eAgFnPc1d1	státní
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
naprosté	naprostý	k2eAgFnSc6d1	naprostá
většině	většina	k1gFnSc6	většina
pravoúhlé	pravoúhlý	k2eAgFnSc6d1	pravoúhlá
<g/>
,	,	kIx,	,
obdélníkové	obdélníkový	k2eAgFnPc4d1	obdélníková
<g/>
.	.	kIx.	.
</s>
<s>
Čtvercový	čtvercový	k2eAgInSc4d1	čtvercový
tvar	tvar	k1gInSc4	tvar
mají	mít	k5eAaImIp3nP	mít
pouze	pouze	k6eAd1	pouze
vlajky	vlajka	k1gFnPc4	vlajka
Vatikánu	Vatikán	k1gInSc2	Vatikán
a	a	k8xC	a
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
<g/>
,	,	kIx,	,
nepravoúhlými	pravoúhlý	k2eNgFnPc7d1	pravoúhlý
výjimkami	výjimka	k1gFnPc7	výjimka
jsou	být	k5eAaImIp3nP	být
cípaté	cípatý	k2eAgFnPc4d1	cípatá
vlajky	vlajka	k1gFnPc4	vlajka
Nepálu	Nepál	k1gInSc2	Nepál
<g/>
,	,	kIx,	,
státu	stát	k1gInSc2	stát
Ohio	Ohio	k1gNnSc1	Ohio
v	v	k7c4	v
USA	USA	kA	USA
nebo	nebo	k8xC	nebo
různé	různý	k2eAgFnPc4d1	různá
lodní	lodní	k2eAgFnPc4d1	lodní
<g/>
,	,	kIx,	,
vojenské	vojenský	k2eAgFnPc4d1	vojenská
či	či	k8xC	či
lokální	lokální	k2eAgFnPc4d1	lokální
vlajky	vlajka	k1gFnPc4	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastější	častý	k2eAgInSc1d3	nejčastější
poměr	poměr	k1gInSc1	poměr
šířky	šířka	k1gFnSc2	šířka
a	a	k8xC	a
délky	délka	k1gFnSc2	délka
stran	strana	k1gFnPc2	strana
je	být	k5eAaImIp3nS	být
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
(	(	kIx(	(
<g/>
česká	český	k2eAgFnSc1d1	Česká
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
)	)	kIx)	)
či	či	k8xC	či
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mnohé	mnohý	k2eAgFnPc1d1	mnohá
vlajky	vlajka	k1gFnPc1	vlajka
mají	mít	k5eAaImIp3nP	mít
poměr	poměr	k1gInSc4	poměr
šířky	šířka	k1gFnSc2	šířka
a	a	k8xC	a
délky	délka	k1gFnSc2	délka
osobitý	osobitý	k2eAgMnSc1d1	osobitý
<g/>
,	,	kIx,	,
např.	např.	kA	např.
polská	polský	k2eAgFnSc1d1	polská
vlajka	vlajka	k1gFnSc1	vlajka
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
8	[number]	k4	8
<g/>
,	,	kIx,	,
vlajka	vlajka	k1gFnSc1	vlajka
USA	USA	kA	USA
10	[number]	k4	10
<g/>
:	:	kIx,	:
<g/>
19	[number]	k4	19
aj.	aj.	kA	aj.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlajky	vlajka	k1gFnPc4	vlajka
národní	národní	k2eAgFnSc2d1	národní
a	a	k8xC	a
státní	státní	k2eAgFnSc2d1	státní
==	==	k?	==
</s>
</p>
<p>
<s>
Přísně	přísně	k6eAd1	přísně
vzato	vzat	k2eAgNnSc1d1	vzato
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
za	za	k7c4	za
státní	státní	k2eAgFnSc4d1	státní
vlajku	vlajka	k1gFnSc4	vlajka
měla	mít	k5eAaImAgFnS	mít
označovat	označovat	k5eAaImF	označovat
pouze	pouze	k6eAd1	pouze
taková	takový	k3xDgFnSc1	takový
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
vyhrazena	vyhradit	k5eAaPmNgFnS	vyhradit
pro	pro	k7c4	pro
použití	použití	k1gNnSc1	použití
státními	státní	k2eAgInPc7d1	státní
orgány	orgán	k1gInPc7	orgán
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
vlajky	vlajka	k1gFnPc1	vlajka
<g/>
,	,	kIx,	,
vyvěšované	vyvěšovaný	k2eAgInPc1d1	vyvěšovaný
soukromými	soukromý	k2eAgFnPc7d1	soukromá
osobami	osoba	k1gFnPc7	osoba
a	a	k8xC	a
institucemi	instituce	k1gFnPc7	instituce
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
pouze	pouze	k6eAd1	pouze
vlajky	vlajka	k1gFnPc1	vlajka
národní	národní	k2eAgFnPc1d1	národní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
běžném	běžný	k2eAgInSc6d1	běžný
projevu	projev	k1gInSc6	projev
se	se	k3xPyFc4	se
však	však	k9	však
oba	dva	k4xCgInPc1	dva
pojmy	pojem	k1gInPc1	pojem
zaměňují	zaměňovat	k5eAaImIp3nP	zaměňovat
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zemích	zem	k1gFnPc6	zem
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
ČR	ČR	kA	ČR
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
státní	státní	k2eAgInSc1d1	státní
a	a	k8xC	a
národní	národní	k2eAgFnSc1d1	národní
vlajka	vlajka	k1gFnSc1	vlajka
vzhledově	vzhledově	k6eAd1	vzhledově
nijak	nijak	k6eAd1	nijak
neliší	lišit	k5eNaImIp3nP	lišit
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
země	zem	k1gFnPc1	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mají	mít	k5eAaImIp3nP	mít
oba	dva	k4xCgInPc1	dva
druhy	druh	k1gInPc1	druh
vlajek	vlajka	k1gFnPc2	vlajka
rozdílnou	rozdílný	k2eAgFnSc4d1	rozdílná
podobu	podoba	k1gFnSc4	podoba
–	–	k?	–
na	na	k7c6	na
státní	státní	k2eAgFnSc6d1	státní
vlajce	vlajka	k1gFnSc6	vlajka
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
znak	znak	k1gInSc1	znak
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
na	na	k7c6	na
národní	národní	k2eAgFnSc6d1	národní
vlajce	vlajka	k1gFnSc6	vlajka
chybí	chybit	k5eAaPmIp3nS	chybit
(	(	kIx(	(
<g/>
rakouská	rakouský	k2eAgFnSc1d1	rakouská
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
,	,	kIx,	,
finská	finský	k2eAgFnSc1d1	finská
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
dnes	dnes	k6eAd1	dnes
užívané	užívaný	k2eAgFnPc4d1	užívaná
státní	státní	k2eAgFnPc4d1	státní
vlajky	vlajka	k1gFnPc4	vlajka
sahají	sahat	k5eAaImIp3nP	sahat
svými	svůj	k3xOyFgInPc7	svůj
kořeny	kořen	k1gInPc7	kořen
až	až	k6eAd1	až
do	do	k7c2	do
středověku	středověk	k1gInSc2	středověk
<g/>
.	.	kIx.	.
</s>
<s>
Rakouská	rakouský	k2eAgFnSc1d1	rakouská
<g/>
,	,	kIx,	,
anglická	anglický	k2eAgFnSc1d1	anglická
a	a	k8xC	a
dánská	dánský	k2eAgFnSc1d1	dánská
vlajka	vlajka	k1gFnSc1	vlajka
pocházejí	pocházet	k5eAaImIp3nP	pocházet
již	již	k6eAd1	již
ze	z	k7c2	z
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Dánská	dánský	k2eAgFnSc1d1	dánská
vlajka	vlajka	k1gFnSc1	vlajka
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
téměř	téměř	k6eAd1	téměř
800	[number]	k4	800
<g/>
letou	letý	k2eAgFnSc7d1	letá
tradicí	tradice	k1gFnSc7	tradice
zřejmě	zřejmě	k6eAd1	zřejmě
nejstarší	starý	k2eAgMnSc1d3	nejstarší
setrvale	setrvale	k6eAd1	setrvale
užívanou	užívaný	k2eAgFnSc7d1	užívaná
státní	státní	k2eAgFnSc7d1	státní
vlajkou	vlajka	k1gFnSc7	vlajka
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
o	o	k7c4	o
málo	málo	k1gNnSc4	málo
mladší	mladý	k2eAgMnSc1d2	mladší
je	být	k5eAaImIp3nS	být
švýcarská	švýcarský	k2eAgFnSc1d1	švýcarská
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
,	,	kIx,	,
ze	z	k7c2	z
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc3	století
pocházejí	pocházet	k5eAaImIp3nP	pocházet
švédská	švédský	k2eAgFnSc1d1	švédská
a	a	k8xC	a
nizozemská	nizozemský	k2eAgFnSc1d1	nizozemská
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Vlajky	vlajka	k1gFnPc1	vlajka
některých	některý	k3yIgFnPc2	některý
zemí	zem	k1gFnPc2	zem
měly	mít	k5eAaImAgFnP	mít
na	na	k7c4	na
utváření	utváření	k1gNnSc4	utváření
nových	nový	k2eAgFnPc2d1	nová
vlajek	vlajka	k1gFnPc2	vlajka
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
po	po	k7c6	po
stránce	stránka	k1gFnSc6	stránka
barevné	barevný	k2eAgFnSc2d1	barevná
symboliky	symbolika	k1gFnSc2	symbolika
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
tvarového	tvarový	k2eAgNnSc2d1	tvarové
uspořádání	uspořádání	k1gNnSc2	uspořádání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Skandinávský	skandinávský	k2eAgInSc1d1	skandinávský
kříž	kříž	k1gInSc1	kříž
na	na	k7c6	na
dánské	dánský	k2eAgFnSc6d1	dánská
vlajce	vlajka	k1gFnSc6	vlajka
zvané	zvaný	k2eAgNnSc1d1	zvané
Dannebrog	Dannebrog	k1gInSc4	Dannebrog
tak	tak	k6eAd1	tak
postupně	postupně	k6eAd1	postupně
převzaly	převzít	k5eAaPmAgInP	převzít
všechny	všechen	k3xTgFnPc4	všechen
severské	severský	k2eAgFnPc4d1	severská
země	zem	k1gFnPc4	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Švýcarská	švýcarský	k2eAgFnSc1d1	švýcarská
vlajka	vlajka	k1gFnSc1	vlajka
s	s	k7c7	s
obrácenými	obrácený	k2eAgFnPc7d1	obrácená
barvami	barva	k1gFnPc7	barva
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
symbolem	symbol	k1gInSc7	symbol
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
červeného	červený	k2eAgInSc2d1	červený
kříže	kříž	k1gInSc2	kříž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
(	(	kIx(	(
<g/>
zvaná	zvaný	k2eAgFnSc1d1	zvaná
Union	union	k1gInSc1	union
Jack	Jack	k1gMnSc1	Jack
<g/>
)	)	kIx)	)
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
překrytím	překrytí	k1gNnSc7	překrytí
starších	starý	k2eAgFnPc2d2	starší
vlajek	vlajka	k1gFnPc2	vlajka
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
Skotska	Skotsko	k1gNnSc2	Skotsko
a	a	k8xC	a
Irska	Irsko	k1gNnSc2	Irsko
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
jako	jako	k8xS	jako
kanton	kanton	k1gInSc1	kanton
součástí	součást	k1gFnPc2	součást
vlajek	vlajka	k1gFnPc2	vlajka
mnoha	mnoho	k4c2	mnoho
bývalých	bývalý	k2eAgMnPc2d1	bývalý
i	i	k8xC	i
stávajících	stávající	k2eAgFnPc2d1	stávající
britských	britský	k2eAgFnPc2d1	britská
kolonií	kolonie	k1gFnPc2	kolonie
(	(	kIx(	(
<g/>
Australská	australský	k2eAgFnSc1d1	australská
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
,	,	kIx,	,
Novozélandská	novozélandský	k2eAgFnSc1d1	novozélandská
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
revoluční	revoluční	k2eAgFnSc1d1	revoluční
trikolóra	trikolóra	k1gFnSc1	trikolóra
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1794	[number]	k4	1794
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
pak	pak	k6eAd1	pak
její	její	k3xOp3gNnSc1	její
svislé	svislý	k2eAgNnSc1d1	svislé
uspořádání	uspořádání	k1gNnSc1	uspořádání
<g/>
,	,	kIx,	,
inspirovala	inspirovat	k5eAaBmAgFnS	inspirovat
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
italskou	italský	k2eAgFnSc4d1	italská
<g/>
,	,	kIx,	,
belgickou	belgický	k2eAgFnSc4d1	belgická
<g/>
,	,	kIx,	,
mexickou	mexický	k2eAgFnSc4d1	mexická
či	či	k8xC	či
irskou	irský	k2eAgFnSc4d1	irská
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ruská	ruský	k2eAgFnSc1d1	ruská
vodorovná	vodorovný	k2eAgFnSc1d1	vodorovná
trikolóra	trikolóra	k1gFnSc1	trikolóra
<g/>
,	,	kIx,	,
vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
po	po	k7c6	po
nizozemském	nizozemský	k2eAgInSc6d1	nizozemský
vzoru	vzor	k1gInSc6	vzor
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
velký	velký	k2eAgInSc4d1	velký
ohlas	ohlas	k1gInSc4	ohlas
u	u	k7c2	u
slovanských	slovanský	k2eAgInPc2d1	slovanský
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
,	,	kIx,	,
modrá	modrý	k2eAgFnSc1d1	modrá
a	a	k8xC	a
červená	červený	k2eAgFnSc1d1	červená
barva	barva	k1gFnSc1	barva
ruské	ruský	k2eAgFnSc2d1	ruská
vlajky	vlajka	k1gFnSc2	vlajka
se	se	k3xPyFc4	se
jako	jako	k9	jako
tzv.	tzv.	kA	tzv.
panslovanské	panslovanský	k2eAgFnPc4d1	panslovanská
barvy	barva	k1gFnPc4	barva
odrážejí	odrážet	k5eAaImIp3nP	odrážet
například	například	k6eAd1	například
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
či	či	k8xC	či
srbské	srbský	k2eAgFnSc6d1	Srbská
vlajce	vlajka	k1gFnSc6	vlajka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vliv	vliv	k1gInSc1	vliv
vlajky	vlajka	k1gFnSc2	vlajka
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
s	s	k7c7	s
hvězdami	hvězda	k1gFnPc7	hvězda
a	a	k8xC	a
pruhy	pruh	k1gInPc7	pruh
se	se	k3xPyFc4	se
odráží	odrážet	k5eAaImIp3nS	odrážet
na	na	k7c6	na
kubánské	kubánský	k2eAgFnSc6d1	kubánská
<g/>
,	,	kIx,	,
chilské	chilský	k2eAgFnSc3d1	chilská
<g/>
,	,	kIx,	,
liberijské	liberijský	k2eAgFnSc3d1	liberijská
<g/>
,	,	kIx,	,
malajsijské	malajsijský	k2eAgFnSc3d1	malajsijská
či	či	k8xC	či
portorické	portorický	k2eAgFnSc3d1	portorická
vlajce	vlajka	k1gFnSc3	vlajka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Žlutá	žlutý	k2eAgFnSc1d1	žlutá
<g/>
,	,	kIx,	,
modrá	modrý	k2eAgFnSc1d1	modrá
a	a	k8xC	a
červená	červený	k2eAgFnSc1d1	červená
barva	barva	k1gFnSc1	barva
někdejší	někdejší	k2eAgFnSc2d1	někdejší
Velké	velký	k2eAgFnSc2d1	velká
Kolumbie	Kolumbie	k1gFnSc2	Kolumbie
stále	stále	k6eAd1	stále
spojuje	spojovat	k5eAaImIp3nS	spojovat
kolumbijskou	kolumbijský	k2eAgFnSc4d1	kolumbijská
<g/>
,	,	kIx,	,
venezuelskou	venezuelský	k2eAgFnSc4d1	venezuelská
a	a	k8xC	a
ekvádorskou	ekvádorský	k2eAgFnSc4d1	ekvádorská
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vliv	vliv	k1gInSc1	vliv
íránské	íránský	k2eAgFnSc2d1	íránská
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
,	,	kIx,	,
s	s	k7c7	s
červenou	červený	k2eAgFnSc7d1	červená
<g/>
,	,	kIx,	,
bílou	bílý	k2eAgFnSc7d1	bílá
a	a	k8xC	a
zelenou	zelený	k2eAgFnSc7d1	zelená
barvou	barva	k1gFnSc7	barva
se	se	k3xPyFc4	se
odráží	odrážet	k5eAaImIp3nS	odrážet
na	na	k7c6	na
tádžické	tádžický	k2eAgFnSc6d1	tádžická
či	či	k8xC	či
kurdské	kurdský	k2eAgFnSc6d1	kurdská
vlajce	vlajka	k1gFnSc6	vlajka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Barva	barva	k1gFnSc1	barva
černá	černý	k2eAgFnSc1d1	černá
<g/>
,	,	kIx,	,
bílá	bílý	k2eAgFnSc1d1	bílá
a	a	k8xC	a
červená	červený	k2eAgFnSc1d1	červená
(	(	kIx(	(
<g/>
a	a	k8xC	a
často	často	k6eAd1	často
zelená	zelený	k2eAgFnSc1d1	zelená
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
rozšířily	rozšířit	k5eAaPmAgFnP	rozšířit
jako	jako	k9	jako
tzv.	tzv.	kA	tzv.
panarabské	panarabský	k2eAgFnPc4d1	panarabská
barvy	barva	k1gFnPc4	barva
a	a	k8xC	a
odráží	odrážet	k5eAaImIp3nS	odrážet
se	se	k3xPyFc4	se
na	na	k7c6	na
vlajkách	vlajka	k1gFnPc6	vlajka
arabských	arabský	k2eAgInPc2d1	arabský
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nezávislá	závislý	k2eNgFnSc1d1	nezávislá
Etiopie	Etiopie	k1gFnSc1	Etiopie
<g/>
,	,	kIx,	,
k	k	k7c3	k
níž	jenž	k3xRgFnSc3	jenž
vzhlížely	vzhlížet	k5eAaImAgFnP	vzhlížet
kolonizované	kolonizovaný	k2eAgFnPc1d1	kolonizovaná
země	zem	k1gFnPc1	zem
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
vzorem	vzor	k1gInSc7	vzor
i	i	k8xC	i
svou	svůj	k3xOyFgFnSc7	svůj
vlajkou	vlajka	k1gFnSc7	vlajka
–	–	k?	–
její	její	k3xOp3gMnSc1	její
zelená	zelenat	k5eAaImIp3nS	zelenat
<g/>
,	,	kIx,	,
žlutá	žlutý	k2eAgFnSc1d1	žlutá
a	a	k8xC	a
červená	červená	k1gFnSc1	červená
se	se	k3xPyFc4	se
rozšířily	rozšířit	k5eAaPmAgFnP	rozšířit
jako	jako	k9	jako
tzv.	tzv.	kA	tzv.
panafrické	panafrický	k2eAgFnPc4d1	panafrická
barvy	barva	k1gFnPc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Vlajky	vlajka	k1gFnPc1	vlajka
a	a	k8xC	a
znaky	znak	k1gInPc1	znak
afrických	africký	k2eAgMnPc2d1	africký
států	stát	k1gInPc2	stát
často	často	k6eAd1	často
nerespektují	respektovat	k5eNaImIp3nP	respektovat
vexilologická	vexilologický	k2eAgNnPc1d1	vexilologický
a	a	k8xC	a
heraldická	heraldický	k2eAgNnPc1d1	heraldické
pravidla	pravidlo	k1gNnPc1	pravidlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Prapory	prapor	k1gInPc1	prapor
a	a	k8xC	a
vlajky	vlajka	k1gFnPc1	vlajka
územních	územní	k2eAgInPc2d1	územní
samosprávných	samosprávný	k2eAgInPc2d1	samosprávný
celků	celek	k1gInPc2	celek
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
==	==	k?	==
</s>
</p>
<p>
<s>
Používání	používání	k1gNnSc1	používání
vlajek	vlajka	k1gFnPc2	vlajka
a	a	k8xC	a
praporů	prapor	k1gInPc2	prapor
obcí	obec	k1gFnPc2	obec
a	a	k8xC	a
zemí	zem	k1gFnPc2	zem
má	mít	k5eAaImIp3nS	mít
tradici	tradice	k1gFnSc4	tradice
sahající	sahající	k2eAgFnSc4d1	sahající
do	do	k7c2	do
historie	historie	k1gFnSc2	historie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
obcích	obec	k1gFnPc6	obec
367	[number]	k4	367
<g/>
/	/	kIx~	/
<g/>
1990	[number]	k4	1990
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
v	v	k7c6	v
§	§	k?	§
5	[number]	k4	5
umožnil	umožnit	k5eAaPmAgInS	umožnit
obcím	obec	k1gFnPc3	obec
užívat	užívat	k5eAaImF	užívat
znak	znak	k1gInSc4	znak
a	a	k8xC	a
prapor	prapor	k1gInSc4	prapor
obce	obec	k1gFnSc2	obec
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
pokud	pokud	k8xS	pokud
nemají	mít	k5eNaImIp3nP	mít
historický	historický	k2eAgInSc4d1	historický
znak	znak	k1gInSc4	znak
a	a	k8xC	a
prapor	prapor	k1gInSc4	prapor
<g/>
,	,	kIx,	,
mohly	moct	k5eAaImAgInP	moct
jim	on	k3xPp3gMnPc3	on
být	být	k5eAaImF	být
na	na	k7c4	na
jejich	jejich	k3xOp3gInSc4	jejich
návrh	návrh	k1gInSc4	návrh
předsednictvem	předsednictvo	k1gNnSc7	předsednictvo
České	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
uděleny	udělit	k5eAaPmNgFnP	udělit
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
obcích	obec	k1gFnPc6	obec
128	[number]	k4	128
<g/>
/	/	kIx~	/
<g/>
2000	[number]	k4	2000
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
původně	původně	k6eAd1	původně
zmiňoval	zmiňovat	k5eAaImAgMnS	zmiňovat
prapory	prapor	k1gInPc4	prapor
obcí	obec	k1gFnPc2	obec
<g/>
,	,	kIx,	,
městských	městský	k2eAgInPc2d1	městský
obvodů	obvod	k1gInPc2	obvod
a	a	k8xC	a
městských	městský	k2eAgFnPc2d1	městská
částí	část	k1gFnPc2	část
v	v	k7c6	v
§	§	k?	§
5	[number]	k4	5
(	(	kIx(	(
<g/>
další	další	k2eAgFnSc1d1	další
novelizace	novelizace	k1gFnSc1	novelizace
přesunula	přesunout	k5eAaPmAgFnS	přesunout
ustanovení	ustanovení	k1gNnSc4	ustanovení
do	do	k7c2	do
§	§	k?	§
34	[number]	k4	34
<g/>
a	a	k8xC	a
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
418	[number]	k4	418
<g/>
/	/	kIx~	/
<g/>
1990	[number]	k4	1990
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
v	v	k7c6	v
§	§	k?	§
5	[number]	k4	5
předpokládal	předpokládat	k5eAaImAgMnS	předpokládat
existenci	existence	k1gFnSc4	existence
praporu	prapor	k1gInSc2	prapor
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
a	a	k8xC	a
nezmiňoval	zmiňovat	k5eNaImAgMnS	zmiňovat
způsob	způsob	k1gInSc4	způsob
jeho	jeho	k3xOp3gNnSc2	jeho
udělení	udělení	k1gNnSc2	udělení
ani	ani	k8xC	ani
případné	případný	k2eAgFnSc2d1	případná
změny	změna	k1gFnSc2	změna
jeho	jeho	k3xOp3gFnSc2	jeho
podoby	podoba	k1gFnSc2	podoba
<g/>
,	,	kIx,	,
upravoval	upravovat	k5eAaImAgInS	upravovat
však	však	k9	však
udělení	udělení	k1gNnSc4	udělení
praporu	prapor	k1gInSc2	prapor
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
předsednictvem	předsednictvo	k1gNnSc7	předsednictvo
České	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
po	po	k7c4	po
vyjádření	vyjádření	k1gNnSc4	vyjádření
zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
městská	městský	k2eAgFnSc1d1	městská
část	část	k1gFnSc1	část
nemá	mít	k5eNaImIp3nS	mít
historický	historický	k2eAgInSc4d1	historický
prapor	prapor	k1gInSc4	prapor
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
Praze	Praha	k1gFnSc6	Praha
131	[number]	k4	131
<g/>
/	/	kIx~	/
<g/>
2000	[number]	k4	2000
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
v	v	k7c6	v
§	§	k?	§
15	[number]	k4	15
rovněž	rovněž	k9	rovněž
předpokládal	předpokládat	k5eAaImAgMnS	předpokládat
existenci	existence	k1gFnSc4	existence
praporu	prapor	k1gInSc2	prapor
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
a	a	k8xC	a
vůbec	vůbec	k9	vůbec
nezmiňoval	zmiňovat	k5eNaImAgMnS	zmiňovat
způsob	způsob	k1gInSc4	způsob
jeho	on	k3xPp3gNnSc2	on
udělení	udělení	k1gNnSc2	udělení
ani	ani	k8xC	ani
případné	případný	k2eAgFnSc2d1	případná
změny	změna	k1gFnSc2	změna
jeho	jeho	k3xOp3gFnSc2	jeho
podoby	podoba	k1gFnSc2	podoba
<g/>
,	,	kIx,	,
prapory	prapor	k1gInPc4	prapor
městských	městský	k2eAgFnPc2d1	městská
částí	část	k1gFnPc2	část
však	však	k9	však
podle	podle	k7c2	podle
tohoto	tento	k3xDgInSc2	tento
zákona	zákon	k1gInSc2	zákon
uděloval	udělovat	k5eAaImAgMnS	udělovat
předseda	předseda	k1gMnSc1	předseda
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
podaný	podaný	k2eAgInSc4d1	podaný
na	na	k7c6	na
základě	základ	k1gInSc6	základ
žádosti	žádost	k1gFnSc2	žádost
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obdobná	obdobný	k2eAgNnPc1d1	obdobné
ustanovení	ustanovení	k1gNnPc1	ustanovení
<g/>
,	,	kIx,	,
o	o	k7c6	o
praporu	prapor	k1gInSc6	prapor
kraje	kraj	k1gInSc2	kraj
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc3	jeho
ochraně	ochrana	k1gFnSc3	ochrana
<g/>
,	,	kIx,	,
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
krajském	krajský	k2eAgNnSc6d1	krajské
zřízení	zřízení	k1gNnSc6	zřízení
<g/>
.	.	kIx.	.
</s>
<s>
Novelou	novela	k1gFnSc7	novela
účinnou	účinný	k2eAgFnSc7d1	účinná
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2001	[number]	k4	2001
však	však	k9	však
tato	tento	k3xDgNnPc1	tento
ustanovení	ustanovení	k1gNnPc1	ustanovení
<g/>
,	,	kIx,	,
zřejmě	zřejmě	k6eAd1	zřejmě
omylem	omyl	k1gInSc7	omyl
<g/>
,	,	kIx,	,
ze	z	k7c2	z
zákona	zákon	k1gInSc2	zákon
vypadla	vypadnout	k5eAaPmAgFnS	vypadnout
<g/>
.	.	kIx.	.
<g/>
Historii	historie	k1gFnSc4	historie
používání	používání	k1gNnSc1	používání
slova	slovo	k1gNnSc2	slovo
prapor	prapor	k1gInSc4	prapor
shrnula	shrnout	k5eAaPmAgNnP	shrnout
</s>
</p>
<p>
<s>
poslankyně	poslankyně	k1gFnSc1	poslankyně
Ivana	Ivana	k1gFnSc1	Ivana
Levá	levá	k1gFnSc1	levá
(	(	kIx(	(
<g/>
členka	členka	k1gFnSc1	členka
Podvýboru	podvýbor	k1gInSc2	podvýbor
pro	pro	k7c4	pro
heraldiku	heraldika	k1gFnSc4	heraldika
a	a	k8xC	a
vexilologii	vexilologie	k1gFnSc4	vexilologie
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
)	)	kIx)	)
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
vystoupení	vystoupení	k1gNnSc6	vystoupení
k	k	k7c3	k
návrhu	návrh	k1gInSc3	návrh
nahrazení	nahrazení	k1gNnSc2	nahrazení
nevhodného	vhodný	k2eNgInSc2d1	nevhodný
výrazu	výraz	k1gInSc2	výraz
"	"	kIx"	"
<g/>
prapor	prapor	k1gInSc1	prapor
<g/>
"	"	kIx"	"
správným	správný	k2eAgInSc7d1	správný
pojmem	pojem	k1gInSc7	pojem
"	"	kIx"	"
<g/>
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
to	ten	k3xDgNnSc1	ten
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
odborné	odborný	k2eAgFnSc3d1	odborná
vexilologické	vexilologický	k2eAgFnSc3d1	vexilologická
terminologii	terminologie	k1gFnSc3	terminologie
<g/>
,	,	kIx,	,
následovně	následovně	k6eAd1	následovně
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
vlajkou	vlajka	k1gFnSc7	vlajka
a	a	k8xC	a
praporem	prapor	k1gInSc7	prapor
byl	být	k5eAaImAgInS	být
vymezen	vymezit	k5eAaPmNgInS	vymezit
již	již	k6eAd1	již
v	v	k7c6	v
zákoně	zákon	k1gInSc6	zákon
č.	č.	k?	č.
121	[number]	k4	121
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
v	v	k7c6	v
zákoně	zákon	k1gInSc6	zákon
č.	č.	k?	č.
269	[number]	k4	269
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1936	[number]	k4	1936
<g/>
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
vlajka	vlajka	k1gFnSc1	vlajka
se	se	k3xPyFc4	se
v	v	k7c6	v
první	první	k4xOgFnSc6	první
republice	republika	k1gFnSc6	republika
nepoužíval	používat	k5eNaImAgMnS	používat
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
státní	státní	k2eAgFnSc1d1	státní
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
též	též	k9	též
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
symbolu	symbol	k1gInSc2	symbol
územní	územní	k2eAgFnSc2d1	územní
korporace	korporace	k1gFnSc2	korporace
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
pojem	pojem	k1gInSc1	pojem
prapor	prapor	k1gInSc1	prapor
byl	být	k5eAaImAgInS	být
užívaný	užívaný	k2eAgInSc1d1	užívaný
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
státní	státní	k2eAgInSc4d1	státní
prapor	prapor	k1gInSc4	prapor
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
symbol	symbol	k1gInSc1	symbol
obce	obec	k1gFnSc2	obec
či	či	k8xC	či
jiné	jiný	k2eAgFnSc2d1	jiná
korporace	korporace	k1gFnSc2	korporace
<g/>
.	.	kIx.	.
</s>
<s>
Poúnorová	poúnorový	k2eAgFnSc1d1	poúnorová
zákonná	zákonný	k2eAgFnSc1d1	zákonná
úprava	úprava	k1gFnSc1	úprava
omezila	omezit	k5eAaPmAgFnS	omezit
princip	princip	k1gInSc4	princip
územní	územní	k2eAgFnSc2d1	územní
samosprávy	samospráva	k1gFnSc2	samospráva
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
užívání	užívání	k1gNnSc1	užívání
vlajek	vlajka	k1gFnPc2	vlajka
či	či	k8xC	či
praporů	prapor	k1gInPc2	prapor
bylo	být	k5eAaImAgNnS	být
rovněž	rovněž	k6eAd1	rovněž
upraveno	upravit	k5eAaPmNgNnS	upravit
zákonem	zákon	k1gInSc7	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
367	[number]	k4	367
<g/>
/	/	kIx~	/
<g/>
1990	[number]	k4	1990
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
však	však	k9	však
nastolil	nastolit	k5eAaPmAgInS	nastolit
stav	stav	k1gInSc1	stav
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vlajka	vlajka	k1gFnSc1	vlajka
byla	být	k5eAaImAgFnS	být
vnímána	vnímat	k5eAaImNgFnS	vnímat
výhradně	výhradně	k6eAd1	výhradně
jako	jako	k8xS	jako
symbol	symbol	k1gInSc1	symbol
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
prapor	prapor	k1gInSc1	prapor
je	být	k5eAaImIp3nS	být
něco	něco	k6eAd1	něco
vlajce	vlajka	k1gFnSc3	vlajka
podobného	podobný	k2eAgInSc2d1	podobný
<g/>
,	,	kIx,	,
co	co	k9	co
užívají	užívat	k5eAaImIp3nP	užívat
města	město	k1gNnPc4	město
či	či	k8xC	či
jiné	jiný	k2eAgFnPc4d1	jiná
korporace	korporace	k1gFnPc4	korporace
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
úpravu	úprava	k1gFnSc4	úprava
převzal	převzít	k5eAaPmAgInS	převzít
i	i	k9	i
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
1993	[number]	k4	1993
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vymezuje	vymezovat	k5eAaImIp3nS	vymezovat
pouze	pouze	k6eAd1	pouze
pojem	pojem	k1gInSc1	pojem
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
352	[number]	k4	352
<g/>
/	/	kIx~	/
<g/>
2001	[number]	k4	2001
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
Také	také	k9	také
nová	nový	k2eAgFnSc1d1	nová
zákonná	zákonný	k2eAgFnSc1d1	zákonná
úprava	úprava	k1gFnSc1	úprava
obecního	obecní	k2eAgNnSc2d1	obecní
zřízení	zřízení	k1gNnSc2	zřízení
<g/>
,	,	kIx,	,
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
128	[number]	k4	128
<g/>
/	/	kIx~	/
<g/>
2000	[number]	k4	2000
<g />
.	.	kIx.	.
</s>
<s>
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
obcích	obec	k1gFnPc6	obec
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
používá	používat	k5eAaImIp3nS	používat
pouze	pouze	k6eAd1	pouze
termínu	termín	k1gInSc2	termín
prapor	prapor	k1gInSc1	prapor
obce	obec	k1gFnSc2	obec
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Zákon	zákon	k1gInSc1	zákon
216	[number]	k4	216
<g/>
/	/	kIx~	/
<g/>
2004	[number]	k4	2004
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
účinný	účinný	k2eAgInSc1d1	účinný
ode	ode	k7c2	ode
dne	den	k1gInSc2	den
vyhlášení	vyhlášení	k1gNnSc4	vyhlášení
28	[number]	k4	28
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
nahradil	nahradit	k5eAaPmAgInS	nahradit
v	v	k7c6	v
ustanoveních	ustanovení	k1gNnPc6	ustanovení
o	o	k7c6	o
praporech	prapor	k1gInPc6	prapor
územních	územní	k2eAgInPc2d1	územní
samosprávných	samosprávný	k2eAgInPc2d1	samosprávný
celků	celek	k1gInPc2	celek
(	(	kIx(	(
<g/>
krajů	kraj	k1gInPc2	kraj
a	a	k8xC	a
obcí	obec	k1gFnPc2	obec
včetně	včetně	k7c2	včetně
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
městských	městský	k2eAgInPc2d1	městský
obvodů	obvod	k1gInPc2	obvod
a	a	k8xC	a
městských	městský	k2eAgFnPc2d1	městská
částí	část	k1gFnPc2	část
<g/>
)	)	kIx)	)
slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
prapor	prapor	k1gInSc1	prapor
<g/>
"	"	kIx"	"
slovem	slovem	k6eAd1	slovem
"	"	kIx"	"
<g/>
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
však	však	k9	však
žádná	žádný	k3yNgNnPc1	žádný
přechodná	přechodný	k2eAgNnPc1d1	přechodné
ustanovení	ustanovení	k1gNnPc1	ustanovení
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
upravovala	upravovat	k5eAaImAgFnS	upravovat
nahrazení	nahrazení	k1gNnSc4	nahrazení
obecních	obecní	k2eAgInPc2d1	obecní
praporů	prapor	k1gInPc2	prapor
obecními	obecní	k2eAgFnPc7d1	obecní
vlajkami	vlajka	k1gFnPc7	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Navrhovatelem	navrhovatel	k1gMnSc7	navrhovatel
zákona	zákon	k1gInSc2	zákon
bylo	být	k5eAaImAgNnS	být
zastupitelstvo	zastupitelstvo	k1gNnSc1	zastupitelstvo
Ústeckého	ústecký	k2eAgInSc2d1	ústecký
kraje	kraj	k1gInSc2	kraj
a	a	k8xC	a
původní	původní	k2eAgInSc1d1	původní
návrh	návrh	k1gInSc1	návrh
se	se	k3xPyFc4	se
vztahoval	vztahovat	k5eAaImAgInS	vztahovat
jen	jen	k9	jen
na	na	k7c4	na
kraje	kraj	k1gInPc4	kraj
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
původně	původně	k6eAd1	původně
pouze	pouze	k6eAd1	pouze
krajům	kraj	k1gInPc3	kraj
pouze	pouze	k6eAd1	pouze
vrátit	vrátit	k5eAaPmF	vrátit
právo	právo	k1gNnSc4	právo
užívat	užívat	k5eAaImF	užívat
prapor	prapor	k1gInSc4	prapor
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2001	[number]	k4	2001
nedopatřením	nedopatření	k1gNnSc7	nedopatření
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
rozšíření	rozšíření	k1gNnSc3	rozšíření
působnosti	působnost	k1gFnSc2	působnost
zákona	zákon	k1gInSc2	zákon
na	na	k7c4	na
všechny	všechen	k3xTgFnPc4	všechen
úrovně	úroveň	k1gFnPc4	úroveň
územně	územně	k6eAd1	územně
samosprávných	samosprávný	k2eAgInPc2d1	samosprávný
celků	celek	k1gInPc2	celek
a	a	k8xC	a
nahrazení	nahrazení	k1gNnSc1	nahrazení
slova	slovo	k1gNnSc2	slovo
prapor	prapor	k1gInSc4	prapor
slovem	slovem	k6eAd1	slovem
vlajka	vlajka	k1gFnSc1	vlajka
došlo	dojít	k5eAaPmAgNnS	dojít
schválením	schválení	k1gNnSc7	schválení
pozměňovacího	pozměňovací	k2eAgInSc2d1	pozměňovací
návrhu	návrh	k1gInSc2	návrh
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
podala	podat	k5eAaPmAgFnS	podat
poslankyně	poslankyně	k1gFnSc1	poslankyně
za	za	k7c2	za
KSČM	KSČM	kA	KSČM
Ivana	Ivana	k1gFnSc1	Ivana
Levá	levá	k1gFnSc1	levá
na	na	k7c4	na
podnět	podnět	k1gInSc4	podnět
expertní	expertní	k2eAgFnSc2d1	expertní
skupiny	skupina	k1gFnSc2	skupina
podvýboru	podvýbor	k1gInSc2	podvýbor
pro	pro	k7c4	pro
heraldiku	heraldika	k1gFnSc4	heraldika
a	a	k8xC	a
vexilologii	vexilologie	k1gFnSc4	vexilologie
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
,	,	kIx,	,
zástupce	zástupce	k1gMnSc2	zástupce
předkladatele	předkladatel	k1gMnSc2	předkladatel
novely	novela	k1gFnSc2	novela
hejtman	hejtman	k1gMnSc1	hejtman
Jiří	Jiří	k1gMnSc1	Jiří
Šulc	Šulc	k1gMnSc1	Šulc
(	(	kIx(	(
<g/>
ODS	ODS	kA	ODS
<g/>
)	)	kIx)	)
připomínky	připomínka	k1gFnSc2	připomínka
pochválil	pochválit	k5eAaPmAgMnS	pochválit
a	a	k8xC	a
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
jim	on	k3xPp3gMnPc3	on
plnou	plný	k2eAgFnSc4d1	plná
podporu	podpora	k1gFnSc4	podpora
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
164	[number]	k4	164
hlasujících	hlasující	k1gMnPc2	hlasující
bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
pozměňovací	pozměňovací	k2eAgInSc4d1	pozměňovací
návrh	návrh	k1gInSc4	návrh
158	[number]	k4	158
<g/>
,	,	kIx,	,
nikdo	nikdo	k3yNnSc1	nikdo
nebyl	být	k5eNaImAgInS	být
proti	proti	k7c3	proti
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
(	(	kIx(	(
<g/>
160	[number]	k4	160
hlasy	hlas	k1gInPc7	hlas
ze	z	k7c2	z
164	[number]	k4	164
<g/>
,	,	kIx,	,
nikdo	nikdo	k3yNnSc1	nikdo
proti	proti	k7c3	proti
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
schválen	schválit	k5eAaPmNgInS	schválit
i	i	k8xC	i
celý	celý	k2eAgInSc1d1	celý
novelizační	novelizační	k2eAgInSc1d1	novelizační
zákon	zákon	k1gInSc1	zákon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vyvěšování	vyvěšování	k1gNnPc1	vyvěšování
vlajek	vlajka	k1gFnPc2	vlajka
a	a	k8xC	a
praporů	prapor	k1gInPc2	prapor
==	==	k?	==
</s>
</p>
<p>
<s>
Základní	základní	k2eAgNnPc1d1	základní
pravidla	pravidlo	k1gNnPc1	pravidlo
o	o	k7c6	o
používání	používání	k1gNnSc6	používání
státních	státní	k2eAgInPc2d1	státní
symbolů	symbol	k1gInPc2	symbol
a	a	k8xC	a
vyvěšování	vyvěšování	k1gNnSc2	vyvěšování
státní	státní	k2eAgFnSc2d1	státní
vlajky	vlajka	k1gFnSc2	vlajka
stanoví	stanovit	k5eAaPmIp3nS	stanovit
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
352	[number]	k4	352
<g/>
/	/	kIx~	/
<g/>
2001	[number]	k4	2001
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
vyvěšování	vyvěšování	k1gNnSc1	vyvěšování
státních	státní	k2eAgFnPc2d1	státní
vlajek	vlajka	k1gFnPc2	vlajka
se	se	k3xPyFc4	se
věnují	věnovat	k5eAaPmIp3nP	věnovat
paragrafy	paragraf	k1gInPc1	paragraf
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Principy	princip	k1gInPc1	princip
zákonem	zákon	k1gInSc7	zákon
stanovené	stanovený	k2eAgInPc1d1	stanovený
jsou	být	k5eAaImIp3nP	být
následující	následující	k2eAgInPc1d1	následující
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Při	při	k7c6	při
vyvěšování	vyvěšování	k1gNnSc6	vyvěšování
státních	státní	k2eAgFnPc2d1	státní
vlajek	vlajka	k1gFnPc2	vlajka
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
každá	každý	k3xTgFnSc1	každý
vlajka	vlajka	k1gFnSc1	vlajka
má	mít	k5eAaImIp3nS	mít
vyhrazen	vyhrazen	k2eAgInSc4d1	vyhrazen
svůj	svůj	k3xOyFgInSc4	svůj
vlajkový	vlajkový	k2eAgInSc4d1	vlajkový
stožár	stožár	k1gInSc4	stožár
<g/>
,	,	kIx,	,
žerď	žerď	k1gFnSc4	žerď
apod.	apod.	kA	apod.
Umisťovat	umisťovat	k5eAaImF	umisťovat
více	hodně	k6eAd2	hodně
státních	státní	k2eAgFnPc2d1	státní
vlajek	vlajka	k1gFnPc2	vlajka
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
stožár	stožár	k1gInSc4	stožár
je	být	k5eAaImIp3nS	být
nepřípustné	přípustný	k2eNgNnSc1d1	nepřípustné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
nejčestnějším	čestný	k2eAgNnSc6d3	nejčestnější
místě	místo	k1gNnSc6	místo
–	–	k?	–
uprostřed	uprostřed	k7c2	uprostřed
–	–	k?	–
je	být	k5eAaImIp3nS	být
domácí	domácí	k2eAgFnSc1d1	domácí
(	(	kIx(	(
<g/>
na	na	k7c6	na
obrázku	obrázek	k1gInSc6	obrázek
česká	český	k2eAgFnSc1d1	Česká
<g/>
)	)	kIx)	)
státní	státní	k2eAgFnSc1d1	státní
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
vlajek	vlajka	k1gFnPc2	vlajka
sudý	sudý	k2eAgInSc4d1	sudý
počet	počet	k1gInSc4	počet
<g/>
,	,	kIx,	,
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
nejčestnější	čestný	k2eAgNnSc1d3	nejčestnější
místo	místo	k1gNnSc1	místo
vpravo	vpravo	k6eAd1	vpravo
od	od	k7c2	od
středu	střed	k1gInSc2	střed
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
vyvěšovatele	vyvěšovatel	k1gMnSc2	vyvěšovatel
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
vlevo	vlevo	k6eAd1	vlevo
od	od	k7c2	od
středu	střed	k1gInSc2	střed
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
pozorovatele	pozorovatel	k1gMnSc2	pozorovatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
znamení	znamení	k1gNnSc6	znamení
smutku	smutek	k1gInSc2	smutek
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
při	při	k7c6	při
úmrtí	úmrtí	k1gNnSc6	úmrtí
hlavy	hlava	k1gFnSc2	hlava
státu	stát	k1gInSc2	stát
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
státní	státní	k2eAgFnSc1d1	státní
vlajka	vlajka	k1gFnSc1	vlajka
vztyčuje	vztyčovat	k5eAaImIp3nS	vztyčovat
pouze	pouze	k6eAd1	pouze
"	"	kIx"	"
<g/>
na	na	k7c4	na
půl	půl	k1xP	půl
žerdi	žerď	k1gFnSc2	žerď
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
svislé	svislý	k2eAgFnSc6d1	svislá
orientaci	orientace	k1gFnSc6	orientace
vlajky	vlajka	k1gFnSc2	vlajka
se	se	k3xPyFc4	se
horní	horní	k2eAgFnSc1d1	horní
část	část	k1gFnSc1	část
praporu	prapor	k1gInSc2	prapor
nachází	nacházet	k5eAaImIp3nS	nacházet
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
vyvěšovatele	vyvěšovatel	k1gMnSc2	vyvěšovatel
vpravo	vpravo	k6eAd1	vpravo
<g/>
,	,	kIx,	,
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
pozorovatele	pozorovatel	k1gMnSc2	pozorovatel
vlevo	vlevo	k6eAd1	vlevo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Vexilologické	Vexilologický	k2eAgNnSc1d1	Vexilologický
názvosloví	názvosloví	k1gNnSc1	názvosloví
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
vlajek	vlajka	k1gFnPc2	vlajka
států	stát	k1gInPc2	stát
světa	svět	k1gInSc2	svět
</s>
</p>
<p>
<s>
Námořní	námořní	k2eAgFnSc1d1	námořní
vlajková	vlajkový	k2eAgFnSc1d1	vlajková
abeceda	abeceda	k1gFnSc1	abeceda
</s>
</p>
<p>
<s>
Olympijská	olympijský	k2eAgFnSc1d1	olympijská
vlajka	vlajka	k1gFnSc1	vlajka
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
vlajka	vlajka	k1gFnSc1	vlajka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
vlajka	vlajka	k1gFnSc1	vlajka
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Vexilolognet	Vexilolognet	k1gInSc1	Vexilolognet
-	-	kIx~	-
server	server	k1gInSc1	server
o	o	k7c6	o
vlajkách	vlajka	k1gFnPc6	vlajka
</s>
</p>
