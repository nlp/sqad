<s>
Projektil	projektil	k1gInSc1	projektil
neboli	neboli	k8xC	neboli
střela	střela	k1gFnSc1	střela
je	být	k5eAaImIp3nS	být
předmět	předmět	k1gInSc4	předmět
vystřelený	vystřelený	k2eAgInSc4d1	vystřelený
ze	z	k7c2	z
střelné	střelný	k2eAgFnSc2d1	střelná
zbraně	zbraň	k1gFnSc2	zbraň
určený	určený	k2eAgInSc4d1	určený
k	k	k7c3	k
zasažení	zasažení	k1gNnSc3	zasažení
cíle	cíl	k1gInSc2	cíl
nebo	nebo	k8xC	nebo
vyvolání	vyvolání	k1gNnSc4	vyvolání
jiného	jiný	k2eAgInSc2d1	jiný
efektu	efekt	k1gInSc2	efekt
<g/>
.	.	kIx.	.
</s>
