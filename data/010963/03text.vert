<p>
<s>
Projektil	projektil	k1gInSc1	projektil
neboli	neboli	k8xC	neboli
střela	střela	k1gFnSc1	střela
je	být	k5eAaImIp3nS	být
předmět	předmět	k1gInSc4	předmět
vystřelený	vystřelený	k2eAgInSc4d1	vystřelený
ze	z	k7c2	z
střelné	střelný	k2eAgFnSc2d1	střelná
zbraně	zbraň	k1gFnSc2	zbraň
určený	určený	k2eAgInSc4d1	určený
k	k	k7c3	k
zasažení	zasažení	k1gNnSc3	zasažení
cíle	cíl	k1gInSc2	cíl
nebo	nebo	k8xC	nebo
vyvolání	vyvolání	k1gNnSc4	vyvolání
jiného	jiný	k2eAgInSc2d1	jiný
efektu	efekt	k1gInSc2	efekt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dříve	dříve	k6eAd2	dříve
byla	být	k5eAaImAgFnS	být
střela	střela	k1gFnSc1	střela
samostatný	samostatný	k2eAgInSc4d1	samostatný
prvek	prvek	k1gInSc1	prvek
který	který	k3yQgInSc4	který
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ucpávkou	ucpávka	k1gFnSc7	ucpávka
utěsňoval	utěsňovat	k5eAaImAgMnS	utěsňovat
prachovou	prachový	k2eAgFnSc4d1	prachová
náplň	náplň	k1gFnSc4	náplň
v	v	k7c6	v
hlavni	hlaveň	k1gFnSc6	hlaveň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
moderních	moderní	k2eAgFnPc2d1	moderní
palných	palný	k2eAgFnPc2d1	palná
zbraní	zbraň	k1gFnPc2	zbraň
je	být	k5eAaImIp3nS	být
střela	střela	k1gFnSc1	střela
částí	část	k1gFnPc2	část
náboje	náboj	k1gInSc2	náboj
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zasazena	zasadit	k5eAaPmNgFnS	zasadit
do	do	k7c2	do
hrdla	hrdlo	k1gNnSc2	hrdlo
nábojnice	nábojnice	k1gFnSc2	nábojnice
a	a	k8xC	a
působí	působit	k5eAaImIp3nS	působit
jako	jako	k9	jako
zátka	zátka	k1gFnSc1	zátka
při	při	k7c6	při
hoření	hoření	k1gNnSc6	hoření
střelného	střelný	k2eAgInSc2d1	střelný
prachu	prach	k1gInSc2	prach
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dělení	dělení	k1gNnSc1	dělení
střel	střela	k1gFnPc2	střela
a	a	k8xC	a
střeliva	střelivo	k1gNnSc2	střelivo
==	==	k?	==
</s>
</p>
<p>
<s>
Z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
určení	určení	k1gNnSc2	určení
střely	střela	k1gFnSc2	střela
a	a	k8xC	a
požadavků	požadavek	k1gInPc2	požadavek
na	na	k7c4	na
její	její	k3xOp3gInSc4	její
účinek	účinek	k1gInSc4	účinek
jsou	být	k5eAaImIp3nP	být
vyvinuty	vyvinut	k2eAgFnPc1d1	vyvinuta
různé	různý	k2eAgFnPc1d1	různá
konstrukce	konstrukce	k1gFnPc1	konstrukce
střel	střela	k1gFnPc2	střela
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgNnSc1d1	základní
rozdělení	rozdělení	k1gNnSc1	rozdělení
střeliva	střelivo	k1gNnSc2	střelivo
lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
udělat	udělat	k5eAaPmF	udělat
podle	podle	k7c2	podle
oblasti	oblast	k1gFnSc2	oblast
použití	použití	k1gNnSc2	použití
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sportovní	sportovní	k2eAgFnPc1d1	sportovní
</s>
</p>
<p>
<s>
Lovecké	lovecký	k2eAgNnSc1d1	lovecké
</s>
</p>
<p>
<s>
VojenskéDalší	VojenskéDalší	k2eAgNnSc1d1	VojenskéDalší
možné	možný	k2eAgNnSc1d1	možné
dělení	dělení	k1gNnSc1	dělení
střeliva	střelivo	k1gNnSc2	střelivo
vycházející	vycházející	k2eAgNnSc4d1	vycházející
ze	z	k7c2	z
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
zbraních	zbraň	k1gFnPc6	zbraň
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
střele	střela	k1gFnSc6	střela
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Jednotná	jednotný	k2eAgFnSc1d1	jednotná
střela	střela	k1gFnSc1	střela
</s>
</p>
<p>
<s>
Těleso	těleso	k1gNnSc1	těleso
<g/>
,	,	kIx,	,
určené	určený	k2eAgNnSc1d1	určené
k	k	k7c3	k
zasažení	zasažení	k1gNnSc3	zasažení
cíle	cíl	k1gInSc2	cíl
nebo	nebo	k8xC	nebo
vyvolání	vyvolání	k1gNnSc4	vyvolání
jiného	jiný	k2eAgInSc2d1	jiný
efektu	efekt	k1gInSc2	efekt
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
opuštění	opuštění	k1gNnSc6	opuštění
hlavně	hlavně	k9	hlavně
nerozdělí	rozdělit	k5eNaPmIp3nS	rozdělit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hromadná	hromadný	k2eAgFnSc1d1	hromadná
střela	střela	k1gFnSc1	střela
</s>
</p>
<p>
<s>
Těleso	těleso	k1gNnSc1	těleso
nebo	nebo	k8xC	nebo
látka	látka	k1gFnSc1	látka
ve	v	k7c6	v
skupenství	skupenství	k1gNnSc6	skupenství
tuhém	tuhý	k2eAgNnSc6d1	tuhé
<g/>
,	,	kIx,	,
kapalném	kapalný	k2eAgNnSc6d1	kapalné
nebo	nebo	k8xC	nebo
plynném	plynný	k2eAgMnSc6d1	plynný
<g/>
,	,	kIx,	,
určená	určený	k2eAgFnSc1d1	určená
k	k	k7c3	k
zasažení	zasažení	k1gNnSc3	zasažení
cíle	cíl	k1gInSc2	cíl
nebo	nebo	k8xC	nebo
vyvolání	vyvolání	k1gNnSc4	vyvolání
jiného	jiný	k2eAgInSc2d1	jiný
efektu	efekt	k1gInSc2	efekt
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
opuštění	opuštění	k1gNnSc6	opuštění
hlavně	hlavně	k9	hlavně
rozdělí	rozdělit	k5eAaPmIp3nS	rozdělit
<g/>
.	.	kIx.	.
<g/>
Do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
jednotných	jednotný	k2eAgFnPc2d1	jednotná
střel	střela	k1gFnPc2	střela
lze	lze	k6eAd1	lze
přiřadit	přiřadit	k5eAaPmF	přiřadit
další	další	k2eAgFnPc4d1	další
podskupiny	podskupina	k1gFnPc4	podskupina
</s>
</p>
<p>
<s>
Speciální	speciální	k2eAgInPc1d1	speciální
střely	střel	k1gInPc1	střel
–	–	k?	–
například	například	k6eAd1	například
tzv.	tzv.	kA	tzv.
Short	Shorta	k1gFnPc2	Shorta
stop	stop	k2eAgFnSc1d1	stop
střela	střela	k1gFnSc1	střela
obsahující	obsahující	k2eAgInSc1d1	obsahující
váček	váček	k1gInSc4	váček
z	z	k7c2	z
tkaniny	tkanina	k1gFnSc2	tkanina
naplněný	naplněný	k2eAgInSc4d1	naplněný
broky	brok	k1gInPc4	brok
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
dopadá	dopadat	k5eAaImIp3nS	dopadat
na	na	k7c4	na
cíl	cíl	k1gInSc4	cíl
plochou	plochý	k2eAgFnSc4d1	plochá
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
zastavit	zastavit	k5eAaPmF	zastavit
a	a	k8xC	a
nepůsobit	působit	k5eNaImF	působit
smrtící	smrtící	k2eAgInPc4d1	smrtící
účinky	účinek	k1gInPc4	účinek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Paintballová	paintballový	k2eAgFnSc1d1	paintballová
střela	střela	k1gFnSc1	střela
–	–	k?	–
střela	střela	k1gFnSc1	střela
kulového	kulový	k2eAgInSc2d1	kulový
tvaru	tvar	k1gInSc2	tvar
pro	pro	k7c4	pro
speciální	speciální	k2eAgFnPc4d1	speciální
zbraně	zbraň	k1gFnPc4	zbraň
obsahující	obsahující	k2eAgFnSc4d1	obsahující
značkovací	značkovací	k2eAgFnSc4d1	značkovací
látku	látka	k1gFnSc4	látka
<g/>
.	.	kIx.	.
<g/>
K	k	k7c3	k
hromadným	hromadný	k2eAgFnPc3d1	hromadná
střelám	střela	k1gFnPc3	střela
lze	lze	k6eAd1	lze
přiřadit	přiřadit	k5eAaPmF	přiřadit
i	i	k9	i
plynové	plynový	k2eAgFnPc4d1	plynová
střely	střela	k1gFnPc4	střela
pro	pro	k7c4	pro
expanzní	expanzní	k2eAgFnPc4d1	expanzní
zbraně	zbraň	k1gFnPc4	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Střela	střela	k1gFnSc1	střela
je	být	k5eAaImIp3nS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
například	například	k6eAd1	například
z	z	k7c2	z
krystalické	krystalický	k2eAgFnSc2d1	krystalická
chemikálie	chemikálie	k1gFnSc2	chemikálie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
při	při	k7c6	při
výstřelku	výstřelek	k1gInSc6	výstřelek
mění	měnit	k5eAaImIp3nS	měnit
na	na	k7c4	na
plyn	plyn	k1gInSc4	plyn
emitovaný	emitovaný	k2eAgInSc4d1	emitovaný
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
cíle	cíl	k1gInSc2	cíl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Jednotné	jednotný	k2eAgFnSc2d1	jednotná
a	a	k8xC	a
hromadné	hromadný	k2eAgFnSc2d1	hromadná
střely	střela	k1gFnSc2	střela
podle	podle	k7c2	podle
zbraní	zbraň	k1gFnPc2	zbraň
==	==	k?	==
</s>
</p>
<p>
<s>
U	u	k7c2	u
historických	historický	k2eAgFnPc2d1	historická
zbraní	zbraň	k1gFnPc2	zbraň
se	se	k3xPyFc4	se
používaly	používat	k5eAaImAgFnP	používat
jednotné	jednotný	k2eAgFnPc1d1	jednotná
střely	střela	k1gFnPc1	střela
i	i	k9	i
do	do	k7c2	do
hladkých	hladký	k2eAgFnPc2d1	hladká
hlavní	hlavní	k2eAgFnSc7d1	hlavní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
</s>
</p>
<p>
<s>
jednotná	jednotný	k2eAgFnSc1d1	jednotná
střela	střela	k1gFnSc1	střela
používána	používat	k5eAaImNgFnS	používat
obvykle	obvykle	k6eAd1	obvykle
do	do	k7c2	do
zbraní	zbraň	k1gFnPc2	zbraň
s	s	k7c7	s
drážkovaným	drážkovaný	k2eAgInSc7d1	drážkovaný
vývrtem	vývrt	k1gInSc7	vývrt
hlavně	hlavně	k6eAd1	hlavně
<g/>
.	.	kIx.	.
</s>
<s>
Hromadná	hromadný	k2eAgFnSc1d1	hromadná
střela	střela	k1gFnSc1	střela
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
brokové	brokový	k2eAgNnSc1d1	brokové
střelivo	střelivo	k1gNnSc1	střelivo
<g/>
)	)	kIx)	)
do	do	k7c2	do
zbraní	zbraň	k1gFnPc2	zbraň
s	s	k7c7	s
hladkým	hladký	k2eAgInSc7d1	hladký
vývrtem	vývrt	k1gInSc7	vývrt
hlavně	hlavně	k6eAd1	hlavně
<g/>
.	.	kIx.	.
</s>
<s>
Méně	málo	k6eAd2	málo
často	často	k6eAd1	často
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
a	a	k8xC	a
používají	používat	k5eAaImIp3nP	používat
i	i	k9	i
brokové	brokový	k2eAgInPc1d1	brokový
náboje	náboj	k1gInPc1	náboj
pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
ve	v	k7c6	v
zbraních	zbraň	k1gFnPc6	zbraň
s	s	k7c7	s
drážkovaným	drážkovaný	k2eAgInSc7d1	drážkovaný
vývrtem	vývrt	k1gInSc7	vývrt
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
některé	některý	k3yIgInPc1	některý
speciální	speciální	k2eAgInPc1d1	speciální
vojenské	vojenský	k2eAgInPc1d1	vojenský
náboje	náboj	k1gInPc1	náboj
(	(	kIx(	(
<g/>
střely	střel	k1gInPc1	střel
pro	pro	k7c4	pro
strážní	strážní	k2eAgFnSc4d1	strážní
službu	služba	k1gFnSc4	služba
a	a	k8xC	a
střelivo	střelivo	k1gNnSc4	střelivo
duplex	duplex	k1gInSc1	duplex
<g/>
)	)	kIx)	)
používají	používat	k5eAaImIp3nP	používat
střelu	střela	k1gFnSc4	střela
vyhovující	vyhovující	k2eAgFnSc4d1	vyhovující
definici	definice	k1gFnSc4	definice
hromadné	hromadný	k2eAgFnSc2d1	hromadná
střely	střela	k1gFnSc2	střela
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
pro	pro	k7c4	pro
brokovnice	brokovnice	k1gFnPc4	brokovnice
jsou	být	k5eAaImIp3nP	být
vyráběny	vyráběn	k2eAgInPc1d1	vyráběn
náboje	náboj	k1gInPc1	náboj
s	s	k7c7	s
jednotnými	jednotný	k2eAgFnPc7d1	jednotná
střelami	střela	k1gFnPc7	střela
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ráž	Ráž	k1gMnSc1	Ráž
(	(	kIx(	(
<g/>
Ráže	ráže	k1gFnSc1	ráže
<g/>
)	)	kIx)	)
střel	střel	k1gInSc1	střel
==	==	k?	==
</s>
</p>
<p>
<s>
U	u	k7c2	u
jednotných	jednotný	k2eAgFnPc2d1	jednotná
střel	střela	k1gFnPc2	střela
je	být	k5eAaImIp3nS	být
ráže	ráže	k1gFnSc1	ráže
smluvní	smluvní	k2eAgNnSc1d1	smluvní
číslo	číslo	k1gNnSc1	číslo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
přibližně	přibližně	k6eAd1	přibližně
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
průměr	průměr	k1gInSc4	průměr
střely	střela	k1gFnSc2	střela
<g/>
.	.	kIx.	.
</s>
<s>
Střely	střít	k5eAaImAgFnP	střít
stejné	stejný	k2eAgFnPc1d1	stejná
ráže	ráže	k1gFnPc1	ráže
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
částí	část	k1gFnSc7	část
různých	různý	k2eAgInPc2d1	různý
nábojů	náboj	k1gInPc2	náboj
<g/>
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
ráže	ráže	k1gFnSc2	ráže
u	u	k7c2	u
nábojů	náboj	k1gInPc2	náboj
a	a	k8xC	a
zbraní	zbraň	k1gFnPc2	zbraň
má	mít	k5eAaImIp3nS	mít
širší	široký	k2eAgInSc4d2	širší
význam	význam	k1gInSc4	význam
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
kromě	kromě	k7c2	kromě
průměru	průměr	k1gInSc2	průměr
zahrnovat	zahrnovat	k5eAaImF	zahrnovat
i	i	k9	i
jiné	jiný	k2eAgInPc4d1	jiný
údaje	údaj	k1gInPc4	údaj
popisující	popisující	k2eAgFnSc2d1	popisující
další	další	k2eAgFnSc2d1	další
vlastnosti	vlastnost	k1gFnSc2	vlastnost
střeliva	střelivo	k1gNnSc2	střelivo
nebo	nebo	k8xC	nebo
zbraně	zbraň	k1gFnSc2	zbraň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Střely	střel	k1gInPc7	střel
pro	pro	k7c4	pro
palné	palný	k2eAgFnPc4d1	palná
hlavňové	hlavňový	k2eAgFnPc4d1	hlavňová
zbraně	zbraň	k1gFnPc4	zbraň
s	s	k7c7	s
drážkovým	drážkový	k2eAgInSc7d1	drážkový
vývrtem	vývrt	k1gInSc7	vývrt
==	==	k?	==
</s>
</p>
<p>
<s>
Zavedení	zavedení	k1gNnSc1	zavedení
drážkovaných	drážkovaný	k2eAgFnPc2d1	drážkovaná
hlavní	hlavní	k2eAgFnSc2d1	hlavní
a	a	k8xC	a
bezdýmných	bezdýmný	k2eAgInPc2d1	bezdýmný
prachů	prach	k1gInPc2	prach
znamenaly	znamenat	k5eAaImAgFnP	znamenat
významný	významný	k2eAgInSc4d1	významný
pokrok	pokrok	k1gInSc4	pokrok
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
nárůstu	nárůst	k1gInSc3	nárůst
rychlosti	rychlost	k1gFnSc2	rychlost
střel	střela	k1gFnPc2	střela
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vývoj	vývoj	k1gInSc1	vývoj
tvaru	tvar	k1gInSc2	tvar
střel	střela	k1gFnPc2	střela
===	===	k?	===
</s>
</p>
<p>
<s>
Střela	střela	k1gFnSc1	střela
<g/>
,	,	kIx,	,
zastaralý	zastaralý	k2eAgInSc1d1	zastaralý
název	název	k1gInSc1	název
kulka	kulka	k1gFnSc1	kulka
<g/>
,	,	kIx,	,
odkazoval	odkazovat	k5eAaImAgMnS	odkazovat
na	na	k7c4	na
její	její	k3xOp3gFnSc4	její
původně	původně	k6eAd1	původně
většinou	většinou	k6eAd1	většinou
kulový	kulový	k2eAgInSc4d1	kulový
tvar	tvar	k1gInSc4	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Tvar	tvar	k1gInSc1	tvar
koule	koule	k1gFnSc2	koule
bývá	bývat	k5eAaImIp3nS	bývat
dnes	dnes	k6eAd1	dnes
naprosto	naprosto	k6eAd1	naprosto
výjimečný	výjimečný	k2eAgInSc1d1	výjimečný
i	i	k9	i
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
setkat	setkat	k5eAaPmF	setkat
např.	např.	kA	např.
u	u	k7c2	u
vzduchovek	vzduchovka	k1gFnPc2	vzduchovka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Střely	střela	k1gFnPc1	střela
získávaly	získávat	k5eAaImAgFnP	získávat
postupně	postupně	k6eAd1	postupně
podlouhlý	podlouhlý	k2eAgInSc4d1	podlouhlý
tvar	tvar	k1gInSc4	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Výhoda	výhoda	k1gFnSc1	výhoda
tohoto	tento	k3xDgInSc2	tento
tvaru	tvar	k1gInSc2	tvar
jsou	být	k5eAaImIp3nP	být
lepší	dobrý	k2eAgFnPc4d2	lepší
aerodynamické	aerodynamický	k2eAgFnPc4d1	aerodynamická
a	a	k8xC	a
balistické	balistický	k2eAgFnPc4d1	balistická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastějšími	častý	k2eAgInPc7d3	nejčastější
tvary	tvar	k1gInPc7	tvar
střel	střela	k1gFnPc2	střela
jsou	být	k5eAaImIp3nP	být
protáhlé	protáhlý	k2eAgInPc1d1	protáhlý
válce	válec	k1gInPc1	válec
s	s	k7c7	s
tupou	tupý	k2eAgFnSc7d1	tupá
<g/>
,	,	kIx,	,
půlkulovitou	půlkulovitý	k2eAgFnSc7d1	půlkulovitá
<g/>
,	,	kIx,	,
kuželovitou	kuželovitý	k2eAgFnSc7d1	kuželovitá
nebo	nebo	k8xC	nebo
ogivální	ogivální	k2eAgFnSc7d1	ogivální
špičkou	špička	k1gFnSc7	špička
<g/>
.	.	kIx.	.
</s>
<s>
Růst	růst	k1gInSc1	růst
rychlosti	rychlost	k1gFnSc2	rychlost
střel	střela	k1gFnPc2	střela
a	a	k8xC	a
požadavek	požadavek	k1gInSc1	požadavek
na	na	k7c4	na
velký	velký	k2eAgInSc4d1	velký
dostřel	dostřel	k1gInSc4	dostřel
(	(	kIx(	(
<g/>
a	a	k8xC	a
tedy	tedy	k9	tedy
menší	malý	k2eAgInSc4d2	menší
aerodynamický	aerodynamický	k2eAgInSc4d1	aerodynamický
odpor	odpor	k1gInSc4	odpor
<g/>
)	)	kIx)	)
vedl	vést	k5eAaImAgMnS	vést
k	k	k7c3	k
zašpičatění	zašpičatění	k1gNnSc3	zašpičatění
střel	střela	k1gFnPc2	střela
v	v	k7c6	v
přední	přední	k2eAgFnSc6d1	přední
části	část	k1gFnSc6	část
a	a	k8xC	a
také	také	k9	také
přidání	přidání	k1gNnSc4	přidání
aerodynamického	aerodynamický	k2eAgInSc2d1	aerodynamický
kužele	kužel	k1gInSc2	kužel
v	v	k7c6	v
zadní	zadní	k2eAgFnSc6d1	zadní
části	část	k1gFnSc6	část
<g/>
.	.	kIx.	.
</s>
<s>
Střela	střela	k1gFnSc1	střela
s	s	k7c7	s
aerodynamickou	aerodynamický	k2eAgFnSc7d1	aerodynamická
úpravou	úprava	k1gFnSc7	úprava
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
biogivální	biogivální	k2eAgFnSc1d1	biogivální
<g/>
.	.	kIx.	.
</s>
<s>
Tvary	tvar	k1gInPc1	tvar
střel	střela	k1gFnPc2	střela
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
podle	podle	k7c2	podle
účelu	účel	k1gInSc2	účel
a	a	k8xC	a
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
v	v	k7c6	v
jaké	jaký	k3yRgFnSc6	jaký
zbrani	zbraň	k1gFnSc6	zbraň
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
zbraní	zbraň	k1gFnPc2	zbraň
určených	určený	k2eAgFnPc2d1	určená
pro	pro	k7c4	pro
střelbu	střelba	k1gFnSc4	střelba
na	na	k7c6	na
kratší	krátký	k2eAgFnSc6d2	kratší
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
není	být	k5eNaImIp3nS	být
třeba	třeba	k6eAd1	třeba
dbát	dbát	k5eAaImF	dbát
tolik	tolik	k6eAd1	tolik
na	na	k7c4	na
nízký	nízký	k2eAgInSc4d1	nízký
odpor	odpor	k1gInSc4	odpor
vzduchu	vzduch	k1gInSc2	vzduch
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
u	u	k7c2	u
zbraní	zbraň	k1gFnPc2	zbraň
pro	pro	k7c4	pro
střelbu	střelba	k1gFnSc4	střelba
na	na	k7c6	na
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vývoj	vývoj	k1gInSc1	vývoj
materiálů	materiál	k1gInPc2	materiál
střel	střela	k1gFnPc2	střela
===	===	k?	===
</s>
</p>
<p>
<s>
Zprvu	zprvu	k6eAd1	zprvu
kamenné	kamenný	k2eAgInPc1d1	kamenný
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
litinové	litinový	k2eAgFnSc2d1	litinová
koule	koule	k1gFnSc2	koule
pro	pro	k7c4	pro
kanóny	kanón	k1gInPc4	kanón
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
ruční	ruční	k2eAgFnPc4d1	ruční
zbraně	zbraň	k1gFnPc4	zbraň
olověné	olověný	k2eAgFnPc4d1	olověná
<g/>
.	.	kIx.	.
</s>
<s>
Střely	střel	k1gInPc4	střel
vyráběné	vyráběný	k2eAgInPc4d1	vyráběný
z	z	k7c2	z
olova	olovo	k1gNnSc2	olovo
zanáší	zanášet	k5eAaImIp3nS	zanášet
vnitřní	vnitřní	k2eAgInSc4d1	vnitřní
povrch	povrch	k1gInSc4	povrch
hlavní	hlavní	k2eAgInSc4d1	hlavní
<g/>
.	.	kIx.	.
</s>
<s>
Olovo	olovo	k1gNnSc1	olovo
začalo	začít	k5eAaPmAgNnS	začít
být	být	k5eAaImF	být
legováno	legovat	k5eAaImNgNnS	legovat
tvrdšími	tvrdý	k2eAgInPc7d2	tvrdší
kovy	kov	k1gInPc7	kov
–	–	k?	–
např.	např.	kA	např.
zinkem	zinek	k1gInSc7	zinek
a	a	k8xC	a
antimonem	antimon	k1gInSc7	antimon
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
i	i	k9	i
ke	k	k7c3	k
konstrukční	konstrukční	k2eAgFnSc3d1	konstrukční
úpravě	úprava	k1gFnSc3	úprava
střel	střela	k1gFnPc2	střela
<g/>
.	.	kIx.	.
</s>
<s>
Střela	střela	k1gFnSc1	střela
má	mít	k5eAaImIp3nS	mít
vnější	vnější	k2eAgFnSc1d1	vnější
část	část	k1gFnSc1	část
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
plášť	plášť	k1gInSc1	plášť
<g/>
)	)	kIx)	)
a	a	k8xC	a
jádro	jádro	k1gNnSc1	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
dvě	dva	k4xCgFnPc1	dva
části	část	k1gFnPc1	část
střely	střela	k1gFnSc2	střela
jsou	být	k5eAaImIp3nP	být
vyráběny	vyrábět	k5eAaImNgFnP	vyrábět
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
materiálů	materiál	k1gInPc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
povrch	povrch	k1gInSc4	povrch
střely	střela	k1gFnSc2	střela
(	(	kIx(	(
<g/>
plášť	plášť	k1gInSc1	plášť
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
používat	používat	k5eAaImF	používat
měď	měď	k1gFnSc4	měď
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
slitina	slitina	k1gFnSc1	slitina
zvaná	zvaný	k2eAgFnSc1d1	zvaná
tombak	tombak	k1gInSc1	tombak
(	(	kIx(	(
<g/>
90	[number]	k4	90
%	%	kIx~	%
Cu	Cu	k1gMnPc2	Cu
a	a	k8xC	a
10	[number]	k4	10
%	%	kIx~	%
Zn	zn	kA	zn
<g/>
)	)	kIx)	)
a	a	k8xC	a
mědinikl	mědinikl	k1gInSc1	mědinikl
(	(	kIx(	(
<g/>
80	[number]	k4	80
%	%	kIx~	%
Cu	Cu	k1gMnPc2	Cu
a	a	k8xC	a
20	[number]	k4	20
%	%	kIx~	%
Ni	on	k3xPp3gFnSc4	on
<g/>
)	)	kIx)	)
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
s	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
technologií	technologie	k1gFnPc2	technologie
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
pláště	plášť	k1gInPc4	plášť
střel	střela	k1gFnPc2	střela
používána	používán	k2eAgFnSc1d1	používána
i	i	k8xC	i
ocel	ocel	k1gFnSc1	ocel
<g/>
.	.	kIx.	.
</s>
<s>
Ocelové	ocelový	k2eAgInPc1d1	ocelový
pláště	plášť	k1gInPc1	plášť
střel	střela	k1gFnPc2	střela
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
povrchově	povrchově	k6eAd1	povrchově
upravují	upravovat	k5eAaImIp3nP	upravovat
niklováním	niklování	k1gNnSc7	niklování
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
ocel	ocel	k1gFnSc1	ocel
plátovaná	plátovaný	k2eAgFnSc1d1	plátovaná
tombakem	tombak	k1gInSc7	tombak
nebo	nebo	k8xC	nebo
mědiniklem	mědinikl	k1gInSc7	mědinikl
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
vnitřní	vnitřní	k2eAgNnSc4d1	vnitřní
jádro	jádro	k1gNnSc4	jádro
střel	střela	k1gFnPc2	střela
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
používá	používat	k5eAaImIp3nS	používat
olovo	olovo	k1gNnSc1	olovo
<g/>
.	.	kIx.	.
</s>
<s>
Olovo	olovo	k1gNnSc1	olovo
bývá	bývat	k5eAaImIp3nS	bývat
legované	legovaný	k2eAgNnSc1d1	legované
antimonem	antimon	k1gInSc7	antimon
(	(	kIx(	(
<g/>
Sb	sb	kA	sb
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Konstrukce	konstrukce	k1gFnSc1	konstrukce
střel	střela	k1gFnPc2	střela
===	===	k?	===
</s>
</p>
<p>
<s>
Celoplášťové	Celoplášťový	k2eAgNnSc1d1	Celoplášťový
(	(	kIx(	(
<g/>
charakteristika	charakteristika	k1gFnSc1	charakteristika
–	–	k?	–
velká	velký	k2eAgFnSc1d1	velká
průbojnost	průbojnost	k1gFnSc1	průbojnost
<g/>
,	,	kIx,	,
malá	malý	k2eAgFnSc1d1	malá
deformovatelnost	deformovatelnost	k1gFnSc1	deformovatelnost
<g/>
.	.	kIx.	.
</s>
<s>
Nepředává	předávat	k5eNaImIp3nS	předávat
cíli	cíl	k1gInSc3	cíl
obvykle	obvykle	k6eAd1	obvykle
všechnu	všechen	k3xTgFnSc4	všechen
energii	energie	k1gFnSc4	energie
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
průstřelu	průstřel	k1gInSc3	průstřel
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Poloplášťové	Poloplášťový	k2eAgFnPc4d1	Poloplášťový
střely	střela	k1gFnPc4	střela
<g/>
.	.	kIx.	.
</s>
<s>
Olověné	olověný	k2eAgNnSc1d1	olověné
jádro	jádro	k1gNnSc1	jádro
střely	střela	k1gFnSc2	střela
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
přední	přední	k2eAgFnSc6d1	přední
části	část	k1gFnSc6	část
střely	střela	k1gFnSc2	střela
obnaženo	obnažen	k2eAgNnSc1d1	obnaženo
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
střela	střela	k1gFnSc1	střela
se	se	k3xPyFc4	se
po	po	k7c6	po
zásahu	zásah	k1gInSc6	zásah
deformuje	deformovat	k5eAaImIp3nS	deformovat
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
menší	malý	k2eAgFnSc1d2	menší
průbojnost	průbojnost	k1gFnSc1	průbojnost
<g/>
,	,	kIx,	,
vyšší	vysoký	k2eAgInSc1d2	vyšší
ranivý	ranivý	k2eAgInSc1d1	ranivý
účinek	účinek	k1gInSc1	účinek
<g/>
)	)	kIx)	)
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
dělá	dělat	k5eAaImIp3nS	dělat
prosekávací	prosekávací	k2eAgFnSc1d1	prosekávací
hrana	hrana	k1gFnSc1	hrana
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
výstupek	výstupek	k1gInSc4	výstupek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zabezpečí	zabezpečit	k5eAaPmIp3nS	zabezpečit
ostře	ostro	k6eAd1	ostro
ohraničený	ohraničený	k2eAgInSc1d1	ohraničený
vstřel	vstřel	k1gInSc1	vstřel
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
podpoří	podpořit	k5eAaPmIp3nS	podpořit
krvácení	krvácení	k1gNnSc4	krvácení
zasaženého	zasažený	k2eAgNnSc2d1	zasažené
zvířete	zvíře	k1gNnSc2	zvíře
nebo	nebo	k8xC	nebo
přesněji	přesně	k6eAd2	přesně
ohraničený	ohraničený	k2eAgInSc1d1	ohraničený
vstřel	vstřel	k1gInSc1	vstřel
na	na	k7c6	na
papírovém	papírový	k2eAgInSc6d1	papírový
terči	terč	k1gInSc6	terč
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jádro	jádro	k1gNnSc1	jádro
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
provádí	provádět	k5eAaImIp3nS	provádět
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
různě	různě	k6eAd1	různě
tvrdých	tvrdý	k2eAgFnPc2d1	tvrdá
slitin	slitina	k1gFnPc2	slitina
olova	olovo	k1gNnSc2	olovo
<g/>
.	.	kIx.	.
</s>
<s>
Měkčí	měkký	k2eAgFnSc1d2	měkčí
část	část	k1gFnSc1	část
jádra	jádro	k1gNnSc2	jádro
se	se	k3xPyFc4	se
deformuje	deformovat	k5eAaImIp3nS	deformovat
a	a	k8xC	a
tvrdší	tvrdý	k2eAgMnSc1d2	tvrdší
si	se	k3xPyFc3	se
zachovává	zachovávat	k5eAaImIp3nS	zachovávat
tvar	tvar	k1gInSc4	tvar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Celoolověné	Celoolověný	k2eAgFnPc4d1	Celoolověný
střely	střela	k1gFnPc4	střela
</s>
</p>
<p>
<s>
Expanzní	expanzní	k2eAgFnPc1d1	expanzní
střely	střela	k1gFnPc1	střela
–	–	k?	–
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
druh	druh	k1gInSc4	druh
poloplášťové	poloplášťový	k2eAgFnSc2d1	poloplášťová
střely	střela	k1gFnSc2	střela
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
dutinu	dutina	k1gFnSc4	dutina
v	v	k7c6	v
přední	přední	k2eAgFnSc6d1	přední
části	část	k1gFnSc6	část
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
dutina	dutina	k1gFnSc1	dutina
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
hřibovité	hřibovitý	k2eAgFnSc3d1	hřibovitá
deformaci	deformace	k1gFnSc3	deformace
po	po	k7c6	po
zásahu	zásah	k1gInSc6	zásah
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
dutina	dutina	k1gFnSc1	dutina
zhoršuje	zhoršovat	k5eAaImIp3nS	zhoršovat
aerodynamiku	aerodynamika	k1gFnSc4	aerodynamika
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
někdy	někdy	k6eAd1	někdy
překryta	překrýt	k5eAaPmNgFnS	překrýt
tenkostěnnou	tenkostěnný	k2eAgFnSc7d1	tenkostěnná
kuklou	kukla	k1gFnSc7	kukla
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
vyplněna	vyplnit	k5eAaPmNgFnS	vyplnit
měkkým	měkký	k2eAgInSc7d1	měkký
materiálem	materiál	k1gInSc7	materiál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Homogenní	homogenní	k2eAgFnPc4d1	homogenní
střely	střela	k1gFnPc4	střela
<g/>
.	.	kIx.	.
</s>
<s>
Střela	střela	k1gFnSc1	střela
je	být	k5eAaImIp3nS	být
vyrobena	vyrobit	k5eAaPmNgFnS	vyrobit
v	v	k7c6	v
základu	základ	k1gInSc2	základ
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
z	z	k7c2	z
tombaku	tombak	k1gInSc2	tombak
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
vkládá	vkládat	k5eAaImIp3nS	vkládat
olovo	olovo	k1gNnSc1	olovo
například	například	k6eAd1	například
formou	forma	k1gFnSc7	forma
vyvrtání	vyvrtání	k1gNnSc2	vyvrtání
<g/>
.	.	kIx.	.
</s>
<s>
Střela	střela	k1gFnSc1	střela
má	mít	k5eAaImIp3nS	mít
lepší	dobrý	k2eAgFnSc1d2	lepší
soudržnost	soudržnost	k1gFnSc1	soudržnost
–	–	k?	–
například	například	k6eAd1	například
při	při	k7c6	při
zásahu	zásah	k1gInSc6	zásah
kosti	kost	k1gFnSc2	kost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Speciální	speciální	k2eAgInPc1d1	speciální
střely	střel	k1gInPc1	střel
s	s	k7c7	s
řízenou	řízený	k2eAgFnSc7d1	řízená
deformací	deformace	k1gFnSc7	deformace
<g/>
.	.	kIx.	.
</s>
<s>
Různé	různý	k2eAgNnSc1d1	různé
konstrukční	konstrukční	k2eAgNnSc1d1	konstrukční
uspořádání	uspořádání	k1gNnSc1	uspořádání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Střely	střel	k1gInPc1	střel
pro	pro	k7c4	pro
vojenské	vojenský	k2eAgInPc4d1	vojenský
náboje	náboj	k1gInPc4	náboj
===	===	k?	===
</s>
</p>
<p>
<s>
Některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
uvedených	uvedený	k2eAgFnPc2d1	uvedená
střel	střela	k1gFnPc2	střela
se	se	k3xPyFc4	se
používaly	používat	k5eAaImAgFnP	používat
jen	jen	k9	jen
v	v	k7c6	v
určitém	určitý	k2eAgNnSc6d1	určité
období	období	k1gNnSc6	období
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
již	již	k6eAd1	již
nevyrábí	vyrábět	k5eNaImIp3nS	vyrábět
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
tyto	tento	k3xDgFnPc1	tento
střely	střela	k1gFnPc1	střela
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
mezinárodními	mezinárodní	k2eAgFnPc7d1	mezinárodní
smlouvami	smlouva	k1gFnPc7	smlouva
o	o	k7c6	o
střelivu	střelivo	k1gNnSc6	střelivo
používaném	používaný	k2eAgNnSc6d1	používané
ve	v	k7c6	v
válečném	válečný	k2eAgInSc6d1	válečný
konfliktu	konflikt	k1gInSc6	konflikt
a	a	k8xC	a
proto	proto	k8xC	proto
jejich	jejich	k3xOp3gNnSc1	jejich
použití	použití	k1gNnSc1	použití
bylo	být	k5eAaImAgNnS	být
omezeno	omezit	k5eAaPmNgNnS	omezit
na	na	k7c4	na
mírové	mírový	k2eAgNnSc4d1	Mírové
období	období	k1gNnSc4	období
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
střely	střela	k1gFnPc4	střela
pro	pro	k7c4	pro
strážní	strážní	k2eAgFnSc4d1	strážní
službu	služba	k1gFnSc4	služba
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
byla	být	k5eAaImAgFnS	být
jejich	jejich	k3xOp3gFnSc1	jejich
výroba	výroba	k1gFnSc1	výroba
ukončena	ukončit	k5eAaPmNgFnS	ukončit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Střela	střela	k1gFnSc1	střela
s	s	k7c7	s
plnoolověným	plnoolověný	k2eAgNnSc7d1	plnoolověný
jádrem	jádro	k1gNnSc7	jádro
</s>
</p>
<p>
<s>
střely	střela	k1gFnPc4	střela
s	s	k7c7	s
měkkým	měkký	k2eAgNnSc7d1	měkké
ocelovým	ocelový	k2eAgNnSc7d1	ocelové
jádrem	jádro	k1gNnSc7	jádro
</s>
</p>
<p>
<s>
střely	střela	k1gFnPc1	střela
průbojné	průbojný	k2eAgFnPc1d1	průbojná
(	(	kIx(	(
<g/>
ocelový	ocelový	k2eAgInSc1d1	ocelový
plášť	plášť	k1gInSc1	plášť
<g/>
,	,	kIx,	,
olověná	olověný	k2eAgFnSc1d1	olověná
košilka	košilka	k1gFnSc1	košilka
a	a	k8xC	a
ocelové	ocelový	k2eAgNnSc1d1	ocelové
jádro	jádro	k1gNnSc1	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
jádro	jádro	k1gNnSc4	jádro
se	se	k3xPyFc4	se
používaly	používat	k5eAaImAgFnP	používat
i	i	k9	i
jiné	jiný	k2eAgFnPc1d1	jiná
těžké	těžký	k2eAgFnPc1d1	těžká
a	a	k8xC	a
vysoce	vysoce	k6eAd1	vysoce
pevné	pevný	k2eAgInPc4d1	pevný
kovy	kov	k1gInPc4	kov
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
střely	střela	k1gFnPc4	střela
svítící	svítící	k2eAgFnPc4d1	svítící
(	(	kIx(	(
<g/>
značkovací	značkovací	k2eAgFnPc4d1	značkovací
–	–	k?	–
se	s	k7c7	s
stopovkou	stopovka	k1gFnSc7	stopovka
<g/>
,	,	kIx,	,
plněné	plněný	k2eAgInPc1d1	plněný
látkou	látka	k1gFnSc7	látka
pro	pro	k7c4	pro
zanechání	zanechání	k1gNnSc4	zanechání
světelné	světelný	k2eAgFnSc2d1	světelná
stopy	stopa	k1gFnSc2	stopa
k	k	k7c3	k
vizuální	vizuální	k2eAgFnSc3d1	vizuální
kontrole	kontrola	k1gFnSc3	kontrola
dráhy	dráha	k1gFnSc2	dráha
letu	let	k1gInSc2	let
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
střely	střela	k1gFnPc4	střela
zápalné	zápalný	k2eAgFnSc2d1	zápalná
</s>
</p>
<p>
<s>
střely	střela	k1gFnPc4	střela
průbojně	průbojně	k6eAd1	průbojně
svítící	svítící	k2eAgFnPc4d1	svítící
</s>
</p>
<p>
<s>
střely	střela	k1gFnPc4	střela
průbojně	průbojně	k6eAd1	průbojně
zápalné	zápalný	k2eAgFnPc4d1	zápalná
</s>
</p>
<p>
<s>
střely	střela	k1gFnPc4	střela
průbojně	průbojně	k6eAd1	průbojně
zápalné	zápalný	k2eAgFnPc4d1	zápalná
svítící	svítící	k2eAgFnPc4d1	svítící
</s>
</p>
<p>
<s>
střely	střela	k1gFnPc4	střela
pro	pro	k7c4	pro
strážní	strážní	k2eAgFnSc4d1	strážní
službu	služba	k1gFnSc4	služba
</s>
</p>
<p>
<s>
střely	střela	k1gFnPc1	střela
duplex	duplex	k1gInSc1	duplex
</s>
</p>
<p>
<s>
střely	střela	k1gFnPc4	střela
šípové	šípový	k2eAgFnSc2d1	šípová
</s>
</p>
<p>
<s>
střely	střela	k1gFnPc4	střela
otrávené	otrávený	k2eAgFnPc4d1	otrávená
(	(	kIx(	(
<g/>
za	za	k7c2	za
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
Německo	Německo	k1gNnSc1	Německo
i	i	k8xC	i
Rusko	Rusko	k1gNnSc1	Rusko
používalo	používat	k5eAaImAgNnS	používat
projektily	projektil	k1gInPc4	projektil
s	s	k7c7	s
kyanidovou	kyanidový	k2eAgFnSc7d1	kyanidová
ampulí	ampule	k1gFnSc7	ampule
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
střely	střela	k1gFnPc4	střela
zástřelné	zástřelný	k2eAgFnSc2d1	zástřelný
</s>
</p>
<p>
<s>
střely	střel	k1gInPc4	střel
výbušné	výbušný	k2eAgInPc4d1	výbušný
–	–	k?	–
při	při	k7c6	při
nárazu	náraz	k1gInSc6	náraz
či	či	k8xC	či
průniku	průnik	k1gInSc6	průnik
cílem	cíl	k1gInSc7	cíl
explodují	explodovat	k5eAaBmIp3nP	explodovat
</s>
</p>
<p>
<s>
střely	střel	k1gInPc1	střel
redukované	redukovaný	k2eAgInPc1d1	redukovaný
(	(	kIx(	(
<g/>
terčové	terčový	k2eAgInPc1d1	terčový
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
střely	střela	k1gFnPc4	střela
pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
výcviku	výcvik	k1gInSc2	výcvik
</s>
</p>
<p>
<s>
střely	střela	k1gFnPc4	střela
školní	školní	k2eAgFnSc2d1	školní
</s>
</p>
<p>
<s>
==	==	k?	==
Střely	střel	k1gInPc7	střel
pro	pro	k7c4	pro
palné	palný	k2eAgFnPc4d1	palná
hlavňové	hlavňový	k2eAgFnPc4d1	hlavňová
zbraně	zbraň	k1gFnPc4	zbraň
s	s	k7c7	s
hladkým	hladký	k2eAgInSc7d1	hladký
vývrtem	vývrt	k1gInSc7	vývrt
(	(	kIx(	(
<g/>
brokové	brokový	k2eAgInPc1d1	brokový
náboje	náboj	k1gInPc1	náboj
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Hromadná	hromadný	k2eAgFnSc1d1	hromadná
střela	střela	k1gFnSc1	střela
===	===	k?	===
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
brokovnice	brokovnice	k1gFnPc4	brokovnice
a	a	k8xC	a
brokové	brokový	k2eAgInPc4d1	brokový
náboje	náboj	k1gInPc4	náboj
je	být	k5eAaImIp3nS	být
hromadná	hromadný	k2eAgFnSc1d1	hromadná
střela	střela	k1gFnSc1	střela
typická	typický	k2eAgFnSc1d1	typická
<g/>
.	.	kIx.	.
</s>
<s>
Střela	střela	k1gFnSc1	střela
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
broky	brok	k1gInPc7	brok
stejného	stejný	k2eAgInSc2d1	stejný
průměru	průměr	k1gInSc2	průměr
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
broků	brok	k1gInPc2	brok
v	v	k7c6	v
náboji	náboj	k1gInSc6	náboj
deklarovaná	deklarovaný	k2eAgFnSc1d1	deklarovaná
výrobcem	výrobce	k1gMnSc7	výrobce
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
orientační	orientační	k2eAgFnSc1d1	orientační
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
brokovém	brokový	k2eAgInSc6d1	brokový
náboji	náboj	k1gInSc6	náboj
s	s	k7c7	s
deklarovanou	deklarovaný	k2eAgFnSc7d1	deklarovaná
velikostí	velikost	k1gFnSc7	velikost
broků	brok	k1gInPc2	brok
3,5	[number]	k4	3,5
mm	mm	kA	mm
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
často	často	k6eAd1	často
broky	brok	k1gInPc1	brok
od	od	k7c2	od
velikosti	velikost	k1gFnSc2	velikost
3,25	[number]	k4	3,25
do	do	k7c2	do
3,75	[number]	k4	3,75
mm	mm	kA	mm
<g/>
.	.	kIx.	.
.	.	kIx.	.
</s>
<s>
Broky	brok	k1gInPc1	brok
bývají	bývat	k5eAaImIp3nP	bývat
v	v	k7c6	v
náboji	náboj	k1gInSc6	náboj
uloženy	uložit	k5eAaPmNgInP	uložit
v	v	k7c6	v
plastovém	plastový	k2eAgInSc6d1	plastový
kontejneru	kontejner	k1gInSc6	kontejner
<g/>
.	.	kIx.	.
</s>
<s>
Broky	brok	k1gInPc1	brok
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
olověné	olověný	k2eAgFnPc1d1	olověná
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
kulový	kulový	k2eAgInSc4d1	kulový
tvar	tvar	k1gInSc4	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
i	i	k9	i
broky	brok	k1gInPc1	brok
které	který	k3yQgMnPc4	který
nemají	mít	k5eNaImIp3nP	mít
kulový	kulový	k2eAgInSc4d1	kulový
tvar	tvar	k1gInSc4	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
i	i	k9	i
broky	brok	k1gInPc1	brok
z	z	k7c2	z
jiných	jiný	k2eAgInPc2d1	jiný
materiálů	materiál	k1gInPc2	materiál
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
olovo	olovo	k1gNnSc1	olovo
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
myslivosti	myslivost	k1gFnSc6	myslivost
č.	č.	k?	č.
449	[number]	k4	449
<g/>
/	/	kIx~	/
<g/>
2001	[number]	k4	2001
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
v	v	k7c6	v
platném	platný	k2eAgNnSc6d1	platné
znění	znění	k1gNnSc6	znění
<g/>
,	,	kIx,	,
s	s	k7c7	s
účinností	účinnost	k1gFnSc7	účinnost
od	od	k7c2	od
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2010	[number]	k4	2010
zakazuje	zakazovat	k5eAaImIp3nS	zakazovat
používat	používat	k5eAaImF	používat
olověné	olověný	k2eAgInPc4d1	olověný
brokové	brokový	k2eAgInPc4d1	brokový
náboje	náboj	k1gInPc4	náboj
k	k	k7c3	k
lovu	lov	k1gInSc3	lov
vodního	vodní	k2eAgNnSc2d1	vodní
ptactva	ptactvo	k1gNnSc2	ptactvo
na	na	k7c6	na
mokřadech	mokřad	k1gInPc6	mokřad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Broky	brok	k1gInPc1	brok
které	který	k3yRgMnPc4	který
nejsou	být	k5eNaImIp3nP	být
z	z	k7c2	z
olova	olovo	k1gNnSc2	olovo
se	se	k3xPyFc4	se
prodávají	prodávat	k5eAaImIp3nP	prodávat
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
ocelové	ocelový	k2eAgInPc4d1	ocelový
broky	brok	k1gInPc4	brok
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
určitou	určitý	k2eAgFnSc4d1	určitá
nepřesnost	nepřesnost	k1gFnSc4	nepřesnost
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
materiálem	materiál	k1gInSc7	materiál
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
těchto	tento	k3xDgInPc2	tento
broků	brok	k1gInPc2	brok
je	být	k5eAaImIp3nS	být
železo	železo	k1gNnSc1	železo
s	s	k7c7	s
tvrdostí	tvrdost	k1gFnSc7	tvrdost
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
maximálně	maximálně	k6eAd1	maximálně
110	[number]	k4	110
HV1	HV1	k1gFnPc2	HV1
a	a	k8xC	a
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
100	[number]	k4	100
HV	HV	kA	HV
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Broky	brok	k1gInPc1	brok
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
škále	škála	k1gFnSc6	škála
rozměrů	rozměr	k1gInPc2	rozměr
od	od	k7c2	od
cca	cca	kA	cca
2,4	[number]	k4	2,4
mm	mm	kA	mm
až	až	k8xS	až
cca	cca	kA	cca
9	[number]	k4	9
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
broků	brok	k1gInPc2	brok
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
náboji	náboj	k1gInSc6	náboj
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
jejich	jejich	k3xOp3gFnSc2	jejich
velikosti	velikost	k1gFnSc2	velikost
velmi	velmi	k6eAd1	velmi
liší	lišit	k5eAaImIp3nP	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
náboj	náboj	k1gInSc1	náboj
12	[number]	k4	12
×	×	k?	×
70	[number]	k4	70
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
cca	cca	kA	cca
8	[number]	k4	8
kusů	kus	k1gInPc2	kus
broků	brok	k1gInPc2	brok
velikosti	velikost	k1gFnSc2	velikost
9,1	[number]	k4	9,1
mm	mm	kA	mm
anebo	anebo	k8xC	anebo
cca	cca	kA	cca
370	[number]	k4	370
kusů	kus	k1gInPc2	kus
broků	brok	k1gInPc2	brok
2,5	[number]	k4	2,5
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Výroba	výroba	k1gFnSc1	výroba
olověných	olověný	k2eAgInPc2d1	olověný
broků	brok	k1gInPc2	brok
====	====	k?	====
</s>
</p>
<p>
<s>
Olověné	olověný	k2eAgInPc1d1	olověný
broky	brok	k1gInPc1	brok
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vyráběly	vyrábět	k5eAaImAgFnP	vyrábět
litím	lití	k1gNnSc7	lití
roztaveného	roztavený	k2eAgNnSc2d1	roztavené
olova	olovo	k1gNnSc2	olovo
přes	přes	k7c4	přes
síto	síto	k1gNnSc4	síto
z	z	k7c2	z
vysoko	vysoko	k6eAd1	vysoko
umístěné	umístěný	k2eAgFnSc2d1	umístěná
pozice	pozice	k1gFnSc2	pozice
–	–	k?	–
licí	licí	k2eAgFnPc1d1	licí
věže	věž	k1gFnPc1	věž
<g/>
.	.	kIx.	.
</s>
<s>
Pádem	Pád	k1gInSc7	Pád
z	z	k7c2	z
výšky	výška	k1gFnSc2	výška
se	se	k3xPyFc4	se
kapky	kapka	k1gFnSc2	kapka
roztaveného	roztavený	k2eAgNnSc2d1	roztavené
olova	olovo	k1gNnSc2	olovo
při	při	k7c6	při
průchodu	průchod	k1gInSc6	průchod
vzduchem	vzduch	k1gInSc7	vzduch
vytvarovaly	vytvarovat	k5eAaPmAgFnP	vytvarovat
do	do	k7c2	do
koule	koule	k1gFnSc2	koule
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
dopadaly	dopadat	k5eAaImAgFnP	dopadat
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ztuhly	ztuhnout	k5eAaPmAgFnP	ztuhnout
při	při	k7c6	při
zachování	zachování	k1gNnSc6	zachování
kulového	kulový	k2eAgInSc2d1	kulový
tvaru	tvar	k1gInSc2	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zvýšení	zvýšení	k1gNnSc4	zvýšení
povrchového	povrchový	k2eAgNnSc2d1	povrchové
napětí	napětí	k1gNnSc2	napětí
se	se	k3xPyFc4	se
do	do	k7c2	do
roztaveného	roztavený	k2eAgInSc2d1	roztavený
kovu	kov	k1gInSc2	kov
přidává	přidávat	k5eAaImIp3nS	přidávat
arzen	arzen	k1gInSc1	arzen
(	(	kIx(	(
<g/>
jinak	jinak	k6eAd1	jinak
by	by	kYmCp3nP	by
takto	takto	k6eAd1	takto
vyráběné	vyráběný	k2eAgInPc1d1	vyráběný
broky	brok	k1gInPc1	brok
neměly	mít	k5eNaImAgInP	mít
kulovitý	kulovitý	k2eAgInSc1d1	kulovitý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kapkovitý	kapkovitý	k2eAgInSc1d1	kapkovitý
tvar	tvar	k1gInSc1	tvar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Jednotná	jednotný	k2eAgFnSc1d1	jednotná
střela	střela	k1gFnSc1	střela
===	===	k?	===
</s>
</p>
<p>
<s>
Konstrukce	konstrukce	k1gFnSc1	konstrukce
jednotné	jednotný	k2eAgFnSc2d1	jednotná
střely	střela	k1gFnSc2	střela
brokovnice	brokovnice	k1gFnSc1	brokovnice
je	být	k5eAaImIp3nS	být
upravena	upravit	k5eAaPmNgFnS	upravit
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
používána	používat	k5eAaImNgFnS	používat
v	v	k7c6	v
hladké	hladký	k2eAgFnSc6d1	hladká
hlavni	hlaveň	k1gFnSc6	hlaveň
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
způsobu	způsob	k1gInSc2	způsob
stabilizace	stabilizace	k1gFnSc2	stabilizace
je	být	k5eAaImIp3nS	být
lze	lze	k6eAd1	lze
dělit	dělit	k5eAaImF	dělit
na	na	k7c4	na
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
kulovité	kulovitý	k2eAgInPc4d1	kulovitý
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
stabilizace	stabilizace	k1gFnSc2	stabilizace
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
šípové	šípový	k2eAgInPc4d1	šípový
(	(	kIx(	(
<g/>
podlouhlé	podlouhlý	k2eAgInPc4d1	podlouhlý
s	s	k7c7	s
posunem	posun	k1gInSc7	posun
těžiště	těžiště	k1gNnSc2	těžiště
do	do	k7c2	do
přední	přední	k2eAgFnSc2d1	přední
části	část	k1gFnSc2	část
střely	střela	k1gFnSc2	střela
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
turbínové	turbínový	k2eAgFnPc4d1	turbínová
(	(	kIx(	(
<g/>
stabilizované	stabilizovaný	k2eAgFnPc4d1	stabilizovaná
rotací	rotace	k1gFnSc7	rotace
<g/>
;	;	kIx,	;
střela	střela	k1gFnSc1	střela
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
podélné	podélný	k2eAgFnSc6d1	podélná
ose	osa	k1gFnSc6	osa
otvor	otvor	k1gInSc1	otvor
opatřený	opatřený	k2eAgInSc1d1	opatřený
šroubovicí	šroubovice	k1gFnSc7	šroubovice
–	–	k?	–
např.	např.	kA	např.
střela	střela	k1gFnSc1	střela
Ideal	Ideal	k1gMnSc1	Ideal
<g/>
/	/	kIx~	/
<g/>
Stendebach	Stendebach	k1gMnSc1	Stendebach
<g/>
/	/	kIx~	/
<g/>
;	;	kIx,	;
šroubovice	šroubovice	k1gFnSc1	šroubovice
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
i	i	k9	i
na	na	k7c6	na
vnějším	vnější	k2eAgInSc6d1	vnější
povrchu	povrch	k1gInSc6	povrch
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
šípovo-turbínové	šípovourbínový	k2eAgFnPc4d1	šípovo-turbínový
(	(	kIx(	(
<g/>
stabilizované	stabilizovaný	k2eAgFnPc4d1	stabilizovaná
rotací	rotace	k1gFnSc7	rotace
<g/>
;	;	kIx,	;
kombinuje	kombinovat	k5eAaImIp3nS	kombinovat
oba	dva	k4xCgInPc4	dva
principy	princip	k1gInPc4	princip
<g/>
;	;	kIx,	;
například	například	k6eAd1	například
střela	střela	k1gFnSc1	střela
Brenneke	Brenneke	k1gFnSc1	Brenneke
střela	střela	k1gFnSc1	střela
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
podélné	podélný	k2eAgFnSc6d1	podélná
otvor	otvor	k1gInSc1	otvor
opatřený	opatřený	k2eAgInSc1d1	opatřený
šroubovicí	šroubovice	k1gFnSc7	šroubovice
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Paradox	paradox	k1gInSc1	paradox
(	(	kIx(	(
<g/>
stabilizované	stabilizovaný	k2eAgFnSc2d1	stabilizovaná
rotací	rotace	k1gFnSc7	rotace
<g/>
;	;	kIx,	;
pro	pro	k7c4	pro
brokovnice	brokovnice	k1gFnPc4	brokovnice
s	s	k7c7	s
vývrtem	vývrt	k1gInSc7	vývrt
Paradox	paradox	k1gInSc1	paradox
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
ústí	ústí	k1gNnSc6	ústí
hlavně	hlavně	k9	hlavně
drážkovanou	drážkovaný	k2eAgFnSc4d1	drážkovaná
část	část	k1gFnSc4	část
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Značení	značení	k1gNnPc1	značení
střel	střela	k1gFnPc2	střela
==	==	k?	==
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
nejčastěji	často	k6eAd3	často
používané	používaný	k2eAgInPc4d1	používaný
typy	typ	k1gInPc4	typ
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
díky	díky	k7c3	díky
ceně	cena	k1gFnSc3	cena
a	a	k8xC	a
legislativě	legislativa	k1gFnSc6	legislativa
<g/>
)	)	kIx)	)
na	na	k7c6	na
českém	český	k2eAgInSc6d1	český
trhu	trh	k1gInSc6	trh
pro	pro	k7c4	pro
krátké	krátký	k2eAgFnPc4d1	krátká
palné	palný	k2eAgFnPc4d1	palná
zbraně	zbraň	k1gFnPc4	zbraň
patří	patřit	k5eAaImIp3nP	patřit
střely	střela	k1gFnPc1	střela
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
celoplášťová	celoplášťový	k2eAgFnSc1d1	celoplášťová
(	(	kIx(	(
<g/>
FMJ	FMJ	kA	FMJ
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
poloplášťová	poloplášťový	k2eAgFnSc1d1	poloplášťová
s	s	k7c7	s
měkkou	měkký	k2eAgFnSc7d1	měkká
špičkou	špička	k1gFnSc7	špička
Soft	Soft	k?	Soft
Point	pointa	k1gFnPc2	pointa
(	(	kIx(	(
<g/>
SP	SP	kA	SP
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
olověná	olověný	k2eAgFnSc1d1	olověná
s	s	k7c7	s
oblou	oblý	k2eAgFnSc7d1	oblá
špičkou	špička	k1gFnSc7	špička
(	(	kIx(	(
<g/>
LRN	LRN	kA	LRN
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
poloplášťová	poloplášťový	k2eAgFnSc1d1	poloplášťová
s	s	k7c7	s
dutinou	dutina	k1gFnSc7	dutina
ve	v	k7c6	v
špičce	špička	k1gFnSc6	špička
(	(	kIx(	(
<g/>
HP	HP	kA	HP
<g/>
)	)	kIx)	)
–	–	k?	–
s	s	k7c7	s
řízenou	řízený	k2eAgFnSc7d1	řízená
deformací	deformace	k1gFnSc7	deformace
(	(	kIx(	(
<g/>
v	v	k7c6	v
ČR	ČR	kA	ČR
pro	pro	k7c4	pro
krátké	krátký	k2eAgFnPc4d1	krátká
zbraně	zbraň	k1gFnPc4	zbraň
spadá	spadat	k5eAaImIp3nS	spadat
do	do	k7c2	do
kategorie	kategorie	k1gFnSc2	kategorie
A	A	kA	A
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
zakázané	zakázaný	k2eAgFnSc2d1	zakázaná
<g/>
)	)	kIx)	)
<g/>
Zkratky	zkratka	k1gFnPc1	zkratka
různých	různý	k2eAgFnPc2d1	různá
používaných	používaný	k2eAgFnPc2d1	používaná
střel	střela	k1gFnPc2	střela
a	a	k8xC	a
stručné	stručný	k2eAgInPc1d1	stručný
popisy	popis	k1gInPc1	popis
(	(	kIx(	(
<g/>
vedle	vedle	k7c2	vedle
standardně	standardně	k6eAd1	standardně
používaných	používaný	k2eAgInPc2d1	používaný
obecně	obecně	k6eAd1	obecně
platných	platný	k2eAgFnPc2d1	platná
zkratek	zkratka	k1gFnPc2	zkratka
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
přehledu	přehled	k1gInSc6	přehled
zahrnuty	zahrnut	k2eAgFnPc1d1	zahrnuta
i	i	k8xC	i
zkratky	zkratka	k1gFnPc1	zkratka
konkrétních	konkrétní	k2eAgInPc2d1	konkrétní
druhů	druh	k1gInPc2	druh
střeliva	střelivo	k1gNnSc2	střelivo
některých	některý	k3yIgMnPc2	některý
výrobců	výrobce	k1gMnPc2	výrobce
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
AP	ap	kA	ap
–	–	k?	–
Průbojné	průbojný	k2eAgNnSc1d1	průbojné
střelivo	střelivo	k1gNnSc1	střelivo
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
prostřelení	prostřelení	k1gNnSc3	prostřelení
obrněných	obrněný	k2eAgInPc2d1	obrněný
cílů	cíl	k1gInPc2	cíl
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
neprůstřelné	průstřelný	k2eNgFnPc4d1	neprůstřelná
vesty	vesta	k1gFnPc4	vesta
<g/>
,	,	kIx,	,
pancíř	pancíř	k1gInSc4	pancíř
vozidel	vozidlo	k1gNnPc2	vozidlo
<g/>
,	,	kIx,	,
tanky	tank	k1gInPc1	tank
a	a	k8xC	a
jiné	jiný	k2eAgInPc1d1	jiný
způsoby	způsob	k1gInPc1	způsob
ochrany	ochrana	k1gFnSc2	ochrana
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
jaký	jaký	k3yQgInSc1	jaký
materiál	materiál	k1gInSc1	materiál
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
střela	střela	k1gFnSc1	střela
schopná	schopný	k2eAgFnSc1d1	schopná
prostřelit	prostřelit	k5eAaPmF	prostřelit
záleží	záležet	k5eAaImIp3nP	záležet
na	na	k7c6	na
její	její	k3xOp3gFnSc6	její
ráži	ráže	k1gFnSc6	ráže
<g/>
,	,	kIx,	,
konstrukci	konstrukce	k1gFnSc6	konstrukce
a	a	k8xC	a
zbrani	zbraň	k1gFnSc6	zbraň
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yIgFnSc2	který
je	být	k5eAaImIp3nS	být
vystřelena	vystřelen	k2eAgFnSc1d1	vystřelena
<g/>
.	.	kIx.	.
</s>
<s>
Průbojná	průbojný	k2eAgFnSc1d1	průbojná
střela	střela	k1gFnSc1	střela
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
jádra	jádro	k1gNnSc2	jádro
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
tvrzená	tvrzený	k2eAgFnSc1d1	tvrzená
ocel	ocel	k1gFnSc1	ocel
<g/>
,	,	kIx,	,
wolfram	wolfram	k1gInSc1	wolfram
nebo	nebo	k8xC	nebo
ochuzený	ochuzený	k2eAgInSc1d1	ochuzený
uran	uran	k1gInSc1	uran
<g/>
)	)	kIx)	)
uzavřeného	uzavřený	k2eAgNnSc2d1	uzavřené
v	v	k7c6	v
plášti	plášť	k1gInSc6	plášť
z	z	k7c2	z
měkčího	měkký	k2eAgInSc2d2	měkčí
materiálu	materiál	k1gInSc2	materiál
(	(	kIx(	(
<g/>
aby	aby	kYmCp3nP	aby
snáze	snadno	k6eAd2	snadno
proletěl	proletět	k5eAaPmAgInS	proletět
drážkovanou	drážkovaný	k2eAgFnSc4d1	drážkovaná
hlavní	hlavní	k2eAgFnSc4d1	hlavní
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
slitina	slitina	k1gFnSc1	slitina
mědi	měď	k1gFnSc2	měď
nebo	nebo	k8xC	nebo
hliníku	hliník	k1gInSc2	hliník
<g/>
.	.	kIx.	.
</s>
<s>
Náboje	náboj	k1gInPc4	náboj
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
střelou	střela	k1gFnSc7	střela
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
vystřelit	vystřelit	k5eAaPmF	vystřelit
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
typů	typ	k1gInPc2	typ
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
,	,	kIx,	,
od	od	k7c2	od
pistolí	pistol	k1gFnPc2	pistol
a	a	k8xC	a
revolverů	revolver	k1gInPc2	revolver
až	až	k9	až
po	po	k7c4	po
tankové	tankový	k2eAgInPc4d1	tankový
kanóny	kanón	k1gInPc4	kanón
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ACC	ACC	kA	ACC
–	–	k?	–
Accelerator	Accelerator	k1gInSc1	Accelerator
</s>
</p>
<p>
<s>
BBWC	BBWC	kA	BBWC
–	–	k?	–
Bevel	Bevel	k1gMnSc1	Bevel
Base	basa	k1gFnSc3	basa
Wadcutter	Wadcutter	k1gInSc4	Wadcutter
</s>
</p>
<p>
<s>
BEB	BEB	kA	BEB
–	–	k?	–
Brass	Brass	k1gInSc1	Brass
Enclosed	Enclosed	k1gInSc1	Enclosed
Base	bas	k1gInSc6	bas
</s>
</p>
<p>
<s>
BT	BT	kA	BT
–	–	k?	–
Boat-Tail	Boat-Tail	k1gInSc1	Boat-Tail
–	–	k?	–
tvar	tvar	k1gInSc1	tvar
střely	střela	k1gFnSc2	střela
se	se	k3xPyFc4	se
inspiruje	inspirovat	k5eAaBmIp3nS	inspirovat
kapkou	kapka	k1gFnSc7	kapka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
snížil	snížit	k5eAaPmAgInS	snížit
odpor	odpor	k1gInSc1	odpor
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
dna	dno	k1gNnSc2	dno
střely	střela	k1gFnSc2	střela
<g/>
,	,	kIx,	,
špička	špička	k1gFnSc1	špička
má	mít	k5eAaImIp3nS	mít
standardní	standardní	k2eAgInSc4d1	standardní
ogivální	ogivální	k2eAgInSc4d1	ogivální
tvar	tvar	k1gInSc4	tvar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BTHP	BTHP	kA	BTHP
–	–	k?	–
Boat	Boat	k1gMnSc1	Boat
Tail	Tail	k1gMnSc1	Tail
Hollow	Hollow	k1gMnSc1	Hollow
Point	pointa	k1gFnPc2	pointa
</s>
</p>
<p>
<s>
CB	CB	kA	CB
–	–	k?	–
Cast	Cast	k2eAgInSc4d1	Cast
Bullet	Bullet	k1gInSc4	Bullet
–	–	k?	–
litá	litý	k2eAgFnSc1d1	litá
střela	střela	k1gFnSc1	střela
<g/>
.	.	kIx.	.
</s>
<s>
Střelci	Střelec	k1gMnPc1	Střelec
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
odlévají	odlévat	k5eAaImIp3nP	odlévat
z	z	k7c2	z
roztaveného	roztavený	k2eAgInSc2d1	roztavený
kovu	kov	k1gInSc2	kov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
CL	CL	kA	CL
–	–	k?	–
Core-Lokt	Core-Lokt	k1gInSc1	Core-Lokt
–	–	k?	–
lovecká	lovecký	k2eAgFnSc1d1	lovecká
střela	střela	k1gFnSc1	střela
vyvinutá	vyvinutý	k2eAgFnSc1d1	vyvinutá
firmou	firma	k1gFnSc7	firma
Remington	remington	k1gInSc4	remington
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
CN	CN	kA	CN
–	–	k?	–
Conical	Conical	k1gMnSc1	Conical
Nose	nos	k1gInSc6	nos
–	–	k?	–
rotační	rotační	k2eAgInSc1d1	rotační
komolý	komolý	k2eAgInSc1d1	komolý
kužel	kužel	k1gInSc1	kužel
</s>
</p>
<p>
<s>
DEWC	DEWC	kA	DEWC
–	–	k?	–
Double	double	k2eAgMnSc1d1	double
Ended	Ended	k1gMnSc1	Ended
Wadcutter	Wadcutter	k1gMnSc1	Wadcutter
</s>
</p>
<p>
<s>
FMJ	FMJ	kA	FMJ
–	–	k?	–
Full	Full	k1gInSc1	Full
Metal	metal	k1gInSc1	metal
Jacket	Jacket	k1gInSc1	Jacket
(	(	kIx(	(
<g/>
celokovový	celokovový	k2eAgInSc1d1	celokovový
plášť	plášť	k1gInSc1	plášť
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
FMJ-SWC	FMJ-SWC	k?	FMJ-SWC
–	–	k?	–
Full	Full	k1gInSc4	Full
Metal	metat	k5eAaImAgMnS	metat
Jacket	Jacket	k1gMnSc1	Jacket
Semi	Sem	k1gFnSc2	Sem
Wad	Wad	k?	Wad
Cutter	Cutter	k1gMnSc1	Cutter
</s>
</p>
<p>
<s>
FN	FN	kA	FN
–	–	k?	–
Flat	Flat	k1gInSc1	Flat
Nose	nos	k1gInSc5	nos
</s>
</p>
<p>
<s>
FP	FP	kA	FP
–	–	k?	–
Flat	Flat	k1gInSc1	Flat
Point	pointa	k1gFnPc2	pointa
</s>
</p>
<p>
<s>
FST	FST	kA	FST
–	–	k?	–
Fail	Fail	k1gInSc1	Fail
Safe	safe	k1gInSc1	safe
Talon	talon	k1gInSc1	talon
–	–	k?	–
střela	střela	k1gFnSc1	střela
zkonstruovaná	zkonstruovaný	k2eAgFnSc1d1	zkonstruovaná
firmou	firma	k1gFnSc7	firma
Winchester	Winchester	k1gInSc4	Winchester
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
GameKing	GameKing	k1gInSc4	GameKing
–	–	k?	–
Poloplášťové	Poloplášťový	k2eAgFnSc2d1	Poloplášťový
střely	střela	k1gFnSc2	střela
GameKing	GameKing	k1gInSc1	GameKing
<g/>
®	®	k?	®
se	s	k7c7	s
zúženou	zúžený	k2eAgFnSc7d1	zúžená
zádí	záď	k1gFnSc7	záď
jsou	být	k5eAaImIp3nP	být
konstruovány	konstruovat	k5eAaImNgInP	konstruovat
pro	pro	k7c4	pro
lov	lov	k1gInSc4	lov
na	na	k7c6	na
větší	veliký	k2eAgFnSc6d2	veliký
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jejich	jejich	k3xOp3gFnSc4	jejich
přesnost	přesnost	k1gFnSc4	přesnost
a	a	k8xC	a
účinek	účinek	k1gInSc4	účinek
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
rozhodující	rozhodující	k2eAgInSc4d1	rozhodující
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
výsledek	výsledek	k1gInSc4	výsledek
střelby	střelba	k1gFnSc2	střelba
<g/>
.	.	kIx.	.
</s>
<s>
Aerodynamický	aerodynamický	k2eAgInSc1d1	aerodynamický
tvar	tvar	k1gInSc1	tvar
střely	střela	k1gFnSc2	střela
významně	významně	k6eAd1	významně
snižuje	snižovat	k5eAaImIp3nS	snižovat
odpor	odpor	k1gInSc4	odpor
vzduchu	vzduch	k1gInSc2	vzduch
–	–	k?	–
výsledkem	výsledek	k1gInSc7	výsledek
je	být	k5eAaImIp3nS	být
nižší	nízký	k2eAgInSc1d2	nižší
úbytek	úbytek	k1gInSc1	úbytek
rychlosti	rychlost	k1gFnSc2	rychlost
<g/>
,	,	kIx,	,
vyšší	vysoký	k2eAgFnSc1d2	vyšší
dopadová	dopadový	k2eAgFnSc1d1	dopadová
energie	energie	k1gFnSc1	energie
<g/>
,	,	kIx,	,
plošší	plochý	k2eAgFnSc1d2	plošší
dráha	dráha	k1gFnSc1	dráha
letu	let	k1gInSc2	let
a	a	k8xC	a
menší	malý	k2eAgFnSc1d2	menší
odchylka	odchylka	k1gFnSc1	odchylka
vlivem	vlivem	k7c2	vlivem
bočního	boční	k2eAgInSc2d1	boční
větru	vítr	k1gInSc2	vítr
než	než	k8xS	než
u	u	k7c2	u
srovnatelných	srovnatelný	k2eAgFnPc2d1	srovnatelná
střel	střela	k1gFnPc2	střela
s	s	k7c7	s
válcovou	válcový	k2eAgFnSc7d1	válcová
zádí	záď	k1gFnSc7	záď
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
GD	GD	kA	GD
–	–	k?	–
Gold	Gold	k1gInSc1	Gold
Dot	Dot	k1gFnSc1	Dot
–	–	k?	–
tato	tento	k3xDgFnSc1	tento
střela	střela	k1gFnSc1	střela
byla	být	k5eAaImAgFnS	být
vyvinuta	vyvinout	k5eAaPmNgFnS	vyvinout
firmou	firma	k1gFnSc7	firma
CCI-Speer	CCI-Speer	k1gInSc4	CCI-Speer
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
speciální	speciální	k2eAgInSc1d1	speciální
elektrochemický	elektrochemický	k2eAgInSc1d1	elektrochemický
postup	postup	k1gInSc1	postup
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
dokonale	dokonale	k6eAd1	dokonale
spojí	spojit	k5eAaPmIp3nS	spojit
jádro	jádro	k1gNnSc1	jádro
s	s	k7c7	s
pláštěm	plášť	k1gInSc7	plášť
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
takřka	takřka	k6eAd1	takřka
eliminuje	eliminovat	k5eAaBmIp3nS	eliminovat
selhání	selhání	k1gNnSc4	selhání
střely	střela	k1gFnSc2	střela
oddělením	oddělení	k1gNnSc7	oddělení
pláště	plášť	k1gInSc2	plášť
od	od	k7c2	od
jádra	jádro	k1gNnSc2	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
díky	díky	k7c3	díky
inovativní	inovativní	k2eAgFnSc3d1	inovativní
konstrukci	konstrukce	k1gFnSc3	konstrukce
expanzivní	expanzivní	k2eAgFnSc2d1	expanzivní
dutiny	dutina	k1gFnSc2	dutina
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
minimálním	minimální	k2eAgFnPc3d1	minimální
ztrátám	ztráta	k1gFnPc3	ztráta
materiálu	materiál	k1gInSc2	materiál
střely	střel	k1gInPc4	střel
po	po	k7c6	po
dopadu	dopad	k1gInSc6	dopad
do	do	k7c2	do
cíle	cíl	k1gInSc2	cíl
a	a	k8xC	a
následné	následný	k2eAgFnSc3d1	následná
expanzi	expanze	k1gFnSc3	expanze
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
GG-JHP	GG-JHP	k?	GG-JHP
–	–	k?	–
Guardian	Guardian	k1gMnSc1	Guardian
Gold	Gold	k1gMnSc1	Gold
Jacketed	Jacketed	k1gMnSc1	Jacketed
Hollow	Hollow	k1gMnSc1	Hollow
Point	pointa	k1gFnPc2	pointa
–	–	k?	–
střela	střela	k1gFnSc1	střela
s	s	k7c7	s
řízenou	řízený	k2eAgFnSc7d1	řízená
deformací	deformace	k1gFnSc7	deformace
<g/>
.	.	kIx.	.
</s>
<s>
Vyráběná	vyráběný	k2eAgFnSc1d1	vyráběná
například	například	k6eAd1	například
muničkou	munička	k1gFnSc7	munička
Magtech	Magt	k1gInPc6	Magt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
GS	GS	kA	GS
–	–	k?	–
Golden	Goldna	k1gFnPc2	Goldna
Saber	Saber	k1gMnSc1	Saber
–	–	k?	–
střela	střela	k1gFnSc1	střela
je	být	k5eAaImIp3nS	být
konstruována	konstruovat	k5eAaImNgFnS	konstruovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
svým	svůj	k3xOyFgInSc7	svůj
průměrem	průměr	k1gInSc7	průměr
dobře	dobře	k6eAd1	dobře
prošla	projít	k5eAaPmAgFnS	projít
při	pře	k1gFnSc4	pře
výstřelu	výstřel	k1gInSc2	výstřel
drážkami	drážka	k1gFnPc7	drážka
hlavně	hlavně	k6eAd1	hlavně
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
je	být	k5eAaImIp3nS	být
dosažena	dosažen	k2eAgFnSc1d1	dosažena
vyšší	vysoký	k2eAgFnSc1d2	vyšší
přesnost	přesnost	k1gFnSc1	přesnost
<g/>
.	.	kIx.	.
</s>
<s>
Správně	správně	k6eAd1	správně
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mělo	mít	k5eAaImAgNnS	mít
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
střelu	střela	k1gFnSc4	střela
typu	typ	k1gInSc2	typ
JHP	JHP	kA	JHP
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
firma	firma	k1gFnSc1	firma
Winchester	Winchester	k1gInSc1	Winchester
jí	jíst	k5eAaImIp3nS	jíst
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
nazývá	nazývat	k5eAaImIp3nS	nazývat
HPJ	HPJ	kA	HPJ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HBWC	HBWC	kA	HBWC
–	–	k?	–
Hollow	Hollow	k1gMnSc1	Hollow
Base	basa	k1gFnSc3	basa
Wadcutter	Wadcuttra	k1gFnPc2	Wadcuttra
</s>
</p>
<p>
<s>
HC	HC	kA	HC
–	–	k?	–
Hard	Hard	k1gMnSc1	Hard
Cast	Cast	k1gMnSc1	Cast
</s>
</p>
<p>
<s>
Hollow	Hollow	k?	Hollow
Cavity	Cavit	k1gInPc1	Cavit
–	–	k?	–
viz	vidět	k5eAaImRp2nS	vidět
HP	HP	kA	HP
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dutinka	dutinka	k1gFnSc1	dutinka
je	být	k5eAaImIp3nS	být
extrémně	extrémně	k6eAd1	extrémně
velká	velký	k2eAgFnSc1d1	velká
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vede	vést	k5eAaImIp3nS	vést
buď	buď	k8xC	buď
k	k	k7c3	k
extrémně	extrémně	k6eAd1	extrémně
velké	velký	k2eAgFnSc3d1	velká
expanzi	expanze	k1gFnSc3	expanze
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
k	k	k7c3	k
fragmentaci	fragmentace	k1gFnSc3	fragmentace
střely	střela	k1gFnSc2	střela
po	po	k7c6	po
nárazu	náraz	k1gInSc6	náraz
(	(	kIx(	(
<g/>
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
nechtěná	chtěný	k2eNgFnSc1d1	nechtěná
skutečnost	skutečnost	k1gFnSc1	skutečnost
při	při	k7c6	při
loveckém	lovecký	k2eAgNnSc6d1	lovecké
použití	použití	k1gNnSc6	použití
a	a	k8xC	a
zakázaná	zakázaný	k2eAgFnSc1d1	zakázaná
při	při	k7c6	při
použití	použití	k1gNnSc6	použití
proti	proti	k7c3	proti
lidským	lidský	k2eAgInPc3d1	lidský
cílům	cíl	k1gInPc3	cíl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HP	HP	kA	HP
–	–	k?	–
Hollow	Hollow	k1gFnSc2	Hollow
Point	pointa	k1gFnPc2	pointa
<g/>
.	.	kIx.	.
</s>
<s>
Střela	střela	k1gFnSc1	střela
Hollow	Hollow	k1gFnSc2	Hollow
Point	pointa	k1gFnPc2	pointa
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
nazývána	nazýván	k2eAgFnSc1d1	nazývána
jako	jako	k8xS	jako
Hollow	Hollow	k1gFnSc1	Hollow
Tip	tip	k1gInSc1	tip
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
střela	střela	k1gFnSc1	střela
s	s	k7c7	s
dutinkou	dutinka	k1gFnSc7	dutinka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
DUM-DUM	DUM-DUM	k1gFnSc1	DUM-DUM
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
střela	střela	k1gFnSc1	střela
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
přední	přední	k2eAgFnSc6d1	přední
části	část	k1gFnSc6	část
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
svého	svůj	k3xOyFgInSc2	svůj
tvaru	tvar	k1gInSc2	tvar
<g/>
,	,	kIx,	,
dutinku	dutinka	k1gFnSc4	dutinka
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
určena	určit	k5eAaPmNgFnS	určit
zejména	zejména	k9	zejména
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
po	po	k7c6	po
vstupu	vstup	k1gInSc6	vstup
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
cíle	cíl	k1gInSc2	cíl
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
její	její	k3xOp3gFnSc3	její
expanzi	expanze	k1gFnSc3	expanze
(	(	kIx(	(
<g/>
při	při	k7c6	při
průletu	průlet	k1gInSc6	průlet
měkkými	měkký	k2eAgInPc7d1	měkký
tkáněmi	tkáň	k1gFnPc7	tkáň
tyto	tento	k3xDgFnPc4	tento
měkké	měkký	k2eAgFnPc4d1	měkká
tkáně	tkáň	k1gFnPc4	tkáň
tlačí	tlačit	k5eAaImIp3nP	tlačit
zevnitř	zevnitř	k6eAd1	zevnitř
dutiny	dutina	k1gFnPc1	dutina
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
roztáhne	roztáhnout	k5eAaPmIp3nS	roztáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
je	být	k5eAaImIp3nS	být
typický	typický	k2eAgInSc1d1	typický
hřibovitý	hřibovitý	k2eAgInSc1d1	hřibovitý
tvar	tvar	k1gInSc1	tvar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
jednak	jednak	k8xC	jednak
snížení	snížení	k1gNnSc4	snížení
penetrace	penetrace	k1gFnSc2	penetrace
(	(	kIx(	(
<g/>
nedojde	dojít	k5eNaPmIp3nS	dojít
k	k	k7c3	k
nechtěnému	chtěný	k2eNgNnSc3d1	nechtěné
prostřelení	prostřelení	k1gNnSc3	prostřelení
cíle	cíl	k1gInSc2	cíl
a	a	k8xC	a
tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
možnému	možný	k2eAgNnSc3d1	možné
zranění	zranění	k1gNnSc3	zranění
osob	osoba	k1gFnPc2	osoba
za	za	k7c7	za
ním	on	k3xPp3gNnSc7	on
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
větší	veliký	k2eAgNnSc4d2	veliký
poškození	poškození	k1gNnSc4	poškození
tkání	tkáň	k1gFnPc2	tkáň
(	(	kIx(	(
<g/>
jednak	jednak	k8xC	jednak
projde	projít	k5eAaPmIp3nS	projít
větším	veliký	k2eAgNnSc7d2	veliký
množstvím	množství	k1gNnSc7	množství
tkáně	tkáň	k1gFnSc2	tkáň
a	a	k8xC	a
pak	pak	k6eAd1	pak
také	také	k9	také
může	moct	k5eAaImIp3nS	moct
předat	předat	k5eAaPmF	předat
více	hodně	k6eAd2	hodně
kinetické	kinetický	k2eAgFnPc4d1	kinetická
energie	energie	k1gFnPc4	energie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vedlejším	vedlejší	k2eAgInSc7d1	vedlejší
efektem	efekt	k1gInSc7	efekt
těchto	tento	k3xDgFnPc2	tento
střel	střela	k1gFnPc2	střela
je	být	k5eAaImIp3nS	být
zvýšení	zvýšení	k1gNnSc1	zvýšení
jejich	jejich	k3xOp3gFnSc2	jejich
přesnosti	přesnost	k1gFnSc2	přesnost
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
těžiště	těžiště	k1gNnSc1	těžiště
posouvá	posouvat	k5eAaImIp3nS	posouvat
více	hodně	k6eAd2	hodně
dozadu	dozadu	k6eAd1	dozadu
<g/>
.	.	kIx.	.
</s>
<s>
Míra	Míra	k1gFnSc1	Míra
deformace	deformace	k1gFnSc2	deformace
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
rychlosti	rychlost	k1gFnSc6	rychlost
střely	střela	k1gFnSc2	střela
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
má	mít	k5eAaImIp3nS	mít
při	při	k7c6	při
nárazu	náraz	k1gInSc6	náraz
do	do	k7c2	do
tkáně	tkáň	k1gFnSc2	tkáň
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
do	do	k7c2	do
čeho	co	k3yQnSc2	co
narazí	narazit	k5eAaPmIp3nP	narazit
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
nárazu	náraz	k1gInSc6	náraz
do	do	k7c2	do
něčeho	něco	k3yInSc2	něco
tvrdého	tvrdý	k2eAgNnSc2d1	tvrdé
(	(	kIx(	(
<g/>
např.	např.	kA	např.
kost	kost	k1gFnSc1	kost
<g/>
)	)	kIx)	)
nemusí	muset	k5eNaImIp3nS	muset
k	k	k7c3	k
expanzi	expanze	k1gFnSc3	expanze
vůbec	vůbec	k9	vůbec
dojít	dojít	k5eAaPmF	dojít
(	(	kIx(	(
<g/>
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
lepší	dobrý	k2eAgMnSc1d2	lepší
při	při	k7c6	při
výběru	výběr	k1gInSc6	výběr
vhodné	vhodný	k2eAgFnPc4d1	vhodná
ráže	ráže	k1gFnPc4	ráže
na	na	k7c4	na
sebeobranu	sebeobrana	k1gFnSc4	sebeobrana
nebrat	brat	k5eNaPmF	brat
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
případnou	případný	k2eAgFnSc4d1	případná
výhodu	výhoda	k1gFnSc4	výhoda
expanzivního	expanzivní	k2eAgInSc2d1	expanzivní
efektu	efekt	k1gInSc2	efekt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
pro	pro	k7c4	pro
sebeobranu	sebeobrana	k1gFnSc4	sebeobrana
toto	tento	k3xDgNnSc1	tento
střelivo	střelivo	k1gNnSc1	střelivo
spadá	spadat	k5eAaImIp3nS	spadat
do	do	k7c2	do
kat	kat	k1gMnSc1	kat
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
zakázané	zakázaný	k2eAgNnSc1d1	zakázané
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HPJ	HPJ	kA	HPJ
–	–	k?	–
High	High	k1gMnSc1	High
Performance	performance	k1gFnSc2	performance
Jacketed	Jacketed	k1gMnSc1	Jacketed
</s>
</p>
<p>
<s>
HS	HS	kA	HS
–	–	k?	–
Hydra	hydra	k1gFnSc1	hydra
Shok	Shok	k1gInSc1	Shok
–	–	k?	–
střela	střela	k1gFnSc1	střela
vyvinutá	vyvinutý	k2eAgFnSc1d1	vyvinutá
firmou	firma	k1gFnSc7	firma
Federal	Federal	k1gFnSc4	Federal
Cartridge	Cartridg	k1gFnSc2	Cartridg
Co	co	k3yQnSc1	co
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
její	její	k3xOp3gFnSc3	její
konstrukci	konstrukce	k1gFnSc3	konstrukce
dochází	docházet	k5eAaImIp3nS	docházet
při	při	k7c6	při
průchodu	průchod	k1gInSc6	průchod
měkkými	měkký	k2eAgFnPc7d1	měkká
tkáněmi	tkáň	k1gFnPc7	tkáň
k	k	k7c3	k
maximální	maximální	k2eAgFnSc3d1	maximální
expanzi	expanze	k1gFnSc3	expanze
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
zvětšení	zvětšení	k1gNnSc4	zvětšení
způsobeného	způsobený	k2eAgNnSc2d1	způsobené
poranění	poranění	k1gNnSc2	poranění
a	a	k8xC	a
zastavovacího	zastavovací	k2eAgInSc2d1	zastavovací
efektu	efekt	k1gInSc2	efekt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hollow	Hollow	k?	Hollow
Tip	tip	k1gInSc1	tip
–	–	k?	–
viz	vidět	k5eAaImRp2nS	vidět
HP	HP	kA	HP
(	(	kIx(	(
<g/>
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
synonyma	synonymum	k1gNnPc4	synonymum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
J	J	kA	J
–	–	k?	–
Jacketed	Jacketed	k1gInSc1	Jacketed
–	–	k?	–
obecně	obecně	k6eAd1	obecně
oplášťovaná	oplášťovaný	k2eAgFnSc1d1	oplášťovaná
střela	střela	k1gFnSc1	střela
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
JFP	JFP	kA	JFP
–	–	k?	–
Jacketed	Jacketed	k1gInSc1	Jacketed
Flat	Flat	k1gMnSc1	Flat
Point	pointa	k1gFnPc2	pointa
–	–	k?	–
varianta	varianta	k1gFnSc1	varianta
střely	střela	k1gFnSc2	střela
SP	SP	kA	SP
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
její	její	k3xOp3gFnSc1	její
špička	špička	k1gFnSc1	špička
má	mít	k5eAaImIp3nS	mít
plochý	plochý	k2eAgInSc4d1	plochý
tvar	tvar	k1gInSc4	tvar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
JHC	JHC	kA	JHC
–	–	k?	–
Jacketed	Jacketed	k1gInSc1	Jacketed
Hollow	Hollow	k1gMnSc2	Hollow
Cavity	Cavita	k1gMnSc2	Cavita
–	–	k?	–
viz	vidět	k5eAaImRp2nS	vidět
Hollow	Hollow	k1gFnPc6	Hollow
Cavity	Cavita	k1gMnSc2	Cavita
(	(	kIx(	(
<g/>
synonyma	synonymum	k1gNnSc2	synonymum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
JHP	JHP	kA	JHP
–	–	k?	–
Jacketed	Jacketed	k1gInSc1	Jacketed
Hollow	Hollow	k1gMnSc1	Hollow
Point	pointa	k1gFnPc2	pointa
–	–	k?	–
poloplášťová	poloplášťový	k2eAgFnSc1d1	poloplášťová
střela	střela	k1gFnSc1	střela
s	s	k7c7	s
expanzní	expanzní	k2eAgFnSc7d1	expanzní
dutinou	dutina	k1gFnSc7	dutina
v	v	k7c6	v
přední	přední	k2eAgFnSc6d1	přední
části	část	k1gFnSc6	část
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
do	do	k7c2	do
olověného	olověný	k2eAgNnSc2d1	olověné
jádra	jádro	k1gNnSc2	jádro
zcela	zcela	k6eAd1	zcela
zakrytého	zakrytý	k2eAgMnSc2d1	zakrytý
tombakovým	tombakový	k2eAgFnPc3d1	tombakový
(	(	kIx(	(
<g/>
slitina	slitina	k1gFnSc1	slitina
mědi	měď	k1gFnSc2	měď
a	a	k8xC	a
zinku	zinek	k1gInSc2	zinek
<g/>
)	)	kIx)	)
pláštěm	plášť	k1gInSc7	plášť
<g/>
.	.	kIx.	.
</s>
<s>
Střela	střela	k1gFnSc1	střela
je	být	k5eAaImIp3nS	být
částečně	částečně	k6eAd1	částečně
krytá	krytý	k2eAgFnSc1d1	krytá
pláštěm	plášť	k1gInSc7	plášť
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nedocházelo	docházet	k5eNaImAgNnS	docházet
k	k	k7c3	k
otěru	otěr	k1gInSc3	otěr
olova	olovo	k1gNnSc2	olovo
v	v	k7c6	v
hlavni	hlaveň	k1gFnSc6	hlaveň
při	při	k7c6	při
výstřelu	výstřel	k1gInSc6	výstřel
<g/>
.	.	kIx.	.
</s>
<s>
Plášť	plášť	k1gInSc1	plášť
střely	střela	k1gFnSc2	střela
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
dutiny	dutina	k1gFnSc2	dutina
na	na	k7c6	na
několika	několik	k4yIc6	několik
místech	místo	k1gNnPc6	místo
podélně	podélně	k6eAd1	podélně
rýhován	rýhován	k2eAgInSc4d1	rýhován
<g/>
.	.	kIx.	.
</s>
<s>
Střela	střela	k1gFnSc1	střela
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
řízenou	řízený	k2eAgFnSc7d1	řízená
deformací	deformace	k1gFnSc7	deformace
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
dopadové	dopadový	k2eAgFnSc6d1	dopadová
energii	energie	k1gFnSc6	energie
a	a	k8xC	a
odporu	odpor	k1gInSc6	odpor
cíle	cíl	k1gInSc2	cíl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
JSP	JSP	kA	JSP
–	–	k?	–
Jacketed	Jacketed	k1gInSc1	Jacketed
Soft	Soft	k?	Soft
Point	pointa	k1gFnPc2	pointa
–	–	k?	–
varianta	varianta	k1gFnSc1	varianta
střely	střela	k1gFnSc2	střela
SP	SP	kA	SP
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
L	L	kA	L
–	–	k?	–
Lead	Lead	k1gInSc1	Lead
–	–	k?	–
olověná	olověný	k2eAgFnSc1d1	olověná
střela	střela	k1gFnSc1	střela
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
LFN	LFN	kA	LFN
–	–	k?	–
Long	Long	k1gMnSc1	Long
Flat	Flat	k1gMnSc1	Flat
Nose	nos	k1gInSc6	nos
</s>
</p>
<p>
<s>
LFP	LFP	kA	LFP
–	–	k?	–
Lead	Lead	k1gInSc1	Lead
Flat	Flat	k1gMnSc1	Flat
Point	pointa	k1gFnPc2	pointa
–	–	k?	–
olověná	olověný	k2eAgFnSc1d1	olověná
střela	střela	k1gFnSc1	střela
s	s	k7c7	s
plochou	plocha	k1gFnSc7	plocha
špičkou	špička	k1gFnSc7	špička
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
LHP	LHP	kA	LHP
–	–	k?	–
Lead	Lead	k1gInSc1	Lead
Hollow	Hollow	k1gMnSc1	Hollow
Point	pointa	k1gFnPc2	pointa
–	–	k?	–
olověná	olověný	k2eAgFnSc1d1	olověná
střela	střela	k1gFnSc1	střela
s	s	k7c7	s
dutinkou	dutinka	k1gFnSc7	dutinka
<g/>
.	.	kIx.	.
</s>
<s>
Viz	vidět	k5eAaImRp2nS	vidět
HP	HP	kA	HP
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
LRN	LRN	kA	LRN
–	–	k?	–
Lead	Lead	k1gInSc1	Lead
Round	round	k1gInSc1	round
Nose	nos	k1gInSc6	nos
–	–	k?	–
olověná	olověný	k2eAgFnSc1d1	olověná
střela	střela	k1gFnSc1	střela
s	s	k7c7	s
oblou	oblý	k2eAgFnSc7d1	oblá
špičkou	špička	k1gFnSc7	špička
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
jsou	být	k5eAaImIp3nP	být
pokryty	pokryt	k2eAgFnPc1d1	pokryta
vrstvou	vrstva	k1gFnSc7	vrstva
plastu	plast	k1gInSc2	plast
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
snížit	snížit	k5eAaPmF	snížit
při	při	k7c6	při
střelbě	střelba	k1gFnSc6	střelba
otěr	otěr	k1gInSc4	otěr
olova	olovo	k1gNnSc2	olovo
v	v	k7c6	v
hlavni	hlaveň	k1gFnSc6	hlaveň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
LSWC	LSWC	kA	LSWC
–	–	k?	–
Lead	Lead	k1gMnSc1	Lead
Semi-Wadcutter	Semi-Wadcutter	k1gMnSc1	Semi-Wadcutter
–	–	k?	–
olověná	olověný	k2eAgFnSc1d1	olověná
střela	střela	k1gFnSc1	střela
typu	typ	k1gInSc2	typ
Wadcutter	Wadcuttrum	k1gNnPc2	Wadcuttrum
s	s	k7c7	s
tupou	tupý	k2eAgFnSc7d1	tupá
špičkou	špička	k1gFnSc7	špička
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
LSWC-GC	LSWC-GC	k?	LSWC-GC
–	–	k?	–
Lead	Lead	k1gMnSc1	Lead
Semi-Wadcutter	Semi-Wadcutter	k1gMnSc1	Semi-Wadcutter
Gas	Gas	k1gMnSc1	Gas
Checked	Checked	k1gMnSc1	Checked
</s>
</p>
<p>
<s>
LWC	LWC	kA	LWC
–	–	k?	–
Lead	Lead	k1gMnSc1	Lead
Wadcutter	Wadcutter	k1gMnSc1	Wadcutter
</s>
</p>
<p>
<s>
LTC	LTC	kA	LTC
–	–	k?	–
Lead	Lead	k1gMnSc1	Lead
Truncated	Truncated	k1gMnSc1	Truncated
Cone	Con	k1gInSc2	Con
–	–	k?	–
olověná	olověný	k2eAgFnSc1d1	olověná
střela	střela	k1gFnSc1	střela
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Truncated	Truncated	k1gInSc1	Truncated
Cone	Con	k1gInSc2	Con
<g/>
"	"	kIx"	"
označuje	označovat	k5eAaImIp3nS	označovat
její	její	k3xOp3gInSc4	její
tvar	tvar	k1gInSc4	tvar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MC	MC	kA	MC
–	–	k?	–
Metal	metal	k1gInSc1	metal
Cased	Cased	k1gInSc1	Cased
–	–	k?	–
obecně	obecně	k6eAd1	obecně
střela	střít	k5eAaImAgFnS	střít
oplášťována	oplášťován	k2eAgFnSc1d1	oplášťován
nějakým	nějaký	k3yIgInSc7	nějaký
kovem	kov	k1gInSc7	kov
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Kromě	kromě	k7c2	kromě
kovu	kov	k1gInSc2	kov
existují	existovat	k5eAaImIp3nP	existovat
také	také	k9	také
střely	střela	k1gFnPc4	střela
pokryté	pokrytý	k2eAgFnPc4d1	pokrytá
plastem	plast	k1gInSc7	plast
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
LRN	LRN	kA	LRN
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
MRWC	MRWC	kA	MRWC
–	–	k?	–
Mid-Range	Mid-Range	k1gInSc1	Mid-Range
Wadcutter	Wadcutter	k1gInSc1	Wadcutter
</s>
</p>
<p>
<s>
PB	PB	kA	PB
–	–	k?	–
Lead	Lead	k1gInSc1	Lead
Bullet	Bullet	k1gInSc1	Bullet
–	–	k?	–
celoolověná	celoolověný	k2eAgFnSc1d1	celoolověný
střela	střela	k1gFnSc1	střela
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PL	PL	kA	PL
–	–	k?	–
Power-Lokt	Power-Lokt	k1gInSc1	Power-Lokt
</s>
</p>
<p>
<s>
PSP	PSP	kA	PSP
–	–	k?	–
Plated	Plated	k1gInSc1	Plated
Soft	Soft	k?	Soft
Point	pointa	k1gFnPc2	pointa
</s>
</p>
<p>
<s>
PSP	PSP	kA	PSP
–	–	k?	–
Pointed	Pointed	k1gInSc1	Pointed
Soft	Soft	k?	Soft
Point	pointa	k1gFnPc2	pointa
–	–	k?	–
varianta	varianta	k1gFnSc1	varianta
k	k	k7c3	k
SP	SP	kA	SP
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vrchol	vrchol	k1gInSc1	vrchol
střely	střela	k1gFnSc2	střela
je	být	k5eAaImIp3nS	být
špičatý	špičatý	k2eAgInSc1d1	špičatý
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
typ	typ	k1gInSc1	typ
se	se	k3xPyFc4	se
nesmí	smět	k5eNaImIp3nS	smět
používat	používat	k5eAaImF	používat
v	v	k7c6	v
případě	případ	k1gInSc6	případ
nábojů	náboj	k1gInPc2	náboj
se	s	k7c7	s
středovým	středový	k2eAgInSc7d1	středový
zápalem	zápal	k1gInSc7	zápal
a	a	k8xC	a
zásobnících	zásobník	k1gInPc6	zásobník
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
náboje	náboj	k1gInPc1	náboj
jsou	být	k5eAaImIp3nP	být
řazeny	řadit	k5eAaImNgInP	řadit
za	za	k7c4	za
sebou	se	k3xPyFc7	se
–	–	k?	–
mohlo	moct	k5eAaImAgNnS	moct
by	by	kYmCp3nP	by
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
odpálení	odpálení	k1gNnSc3	odpálení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PTS	PTS	kA	PTS
–	–	k?	–
střela	střela	k1gFnSc1	střela
PTS	PTS	kA	PTS
je	být	k5eAaImIp3nS	být
výsledkem	výsledek	k1gInSc7	výsledek
snah	snaha	k1gFnPc2	snaha
konstruktérů	konstruktér	k1gMnPc2	konstruktér
loveckých	lovecký	k2eAgInPc2d1	lovecký
kulových	kulový	k2eAgInPc2d1	kulový
nábojů	náboj	k1gInPc2	náboj
o	o	k7c4	o
střely	střela	k1gFnPc4	střela
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
ranivým	ranivý	k2eAgInSc7d1	ranivý
účinkem	účinek	k1gInSc7	účinek
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
by	by	kYmCp3nS	by
si	se	k3xPyFc3	se
současně	současně	k6eAd1	současně
zachovaly	zachovat	k5eAaPmAgFnP	zachovat
vynikající	vynikající	k2eAgFnPc4d1	vynikající
balistické	balistický	k2eAgFnPc4d1	balistická
hodnoty	hodnota	k1gFnPc4	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Střela	střela	k1gFnSc1	střela
s	s	k7c7	s
olověným	olověný	k2eAgNnSc7d1	olověné
jádrem	jádro	k1gNnSc7	jádro
je	být	k5eAaImIp3nS	být
překryta	překrýt	k5eAaPmNgFnS	překrýt
zpevňujícím	zpevňující	k2eAgInSc7d1	zpevňující
pláštěm	plášť	k1gInSc7	plášť
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zakončena	zakončit	k5eAaPmNgFnS	zakončit
polymerovým	polymerový	k2eAgInSc7d1	polymerový
hrotem	hrot	k1gInSc7	hrot
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
lepší	dobrý	k2eAgFnSc4d2	lepší
přesnost	přesnost	k1gFnSc4	přesnost
je	být	k5eAaImIp3nS	být
žádoucí	žádoucí	k2eAgMnSc1d1	žádoucí
mít	mít	k5eAaImF	mít
v	v	k7c6	v
přední	přední	k2eAgFnSc6d1	přední
části	část	k1gFnSc6	část
dutinku	dutinka	k1gFnSc4	dutinka
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
lepšího	dobrý	k2eAgNnSc2d2	lepší
umístění	umístění	k1gNnSc2	umístění
těžiště	těžiště	k1gNnSc2	těžiště
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
ale	ale	k9	ale
mění	měnit	k5eAaImIp3nS	měnit
aerodynamické	aerodynamický	k2eAgFnPc4d1	aerodynamická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Takže	takže	k9	takže
se	se	k3xPyFc4	se
použije	použít	k5eAaPmIp3nS	použít
plastová	plastový	k2eAgFnSc1d1	plastová
špička	špička	k1gFnSc1	špička
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
takřka	takřka	k6eAd1	takřka
nic	nic	k3yNnSc1	nic
neváží	vážit	k5eNaImIp3nS	vážit
[	[	kIx(	[
<g/>
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
nemá	mít	k5eNaImIp3nS	mít
téměř	téměř	k6eAd1	téměř
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
polohu	poloha	k1gFnSc4	poloha
těžiště	těžiště	k1gNnSc2	těžiště
střely	střela	k1gFnSc2	střela
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Optimální	optimální	k2eAgInSc1d1	optimální
tvar	tvar	k1gInSc1	tvar
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
dosahování	dosahování	k1gNnSc4	dosahování
vyšších	vysoký	k2eAgFnPc2d2	vyšší
rychlostí	rychlost	k1gFnPc2	rychlost
a	a	k8xC	a
stability	stabilita	k1gFnSc2	stabilita
střely	střela	k1gFnSc2	střela
na	na	k7c6	na
dráze	dráha	k1gFnSc6	dráha
letu	let	k1gInSc2	let
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
vlastnosti	vlastnost	k1gFnPc1	vlastnost
zaručují	zaručovat	k5eAaImIp3nP	zaručovat
vynikající	vynikající	k2eAgFnSc4d1	vynikající
přesnost	přesnost	k1gFnSc4	přesnost
a	a	k8xC	a
ranivost	ranivost	k1gFnSc4	ranivost
střely	střela	k1gFnSc2	střela
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
celistvá	celistvý	k2eAgFnSc1d1	celistvá
při	při	k7c6	při
nárazu	náraz	k1gInSc6	náraz
<g/>
,	,	kIx,	,
netříští	tříštit	k5eNaImIp3nS	tříštit
se	se	k3xPyFc4	se
a	a	k8xC	a
neznehodnocuje	znehodnocovat	k5eNaImIp3nS	znehodnocovat
zvěřinu	zvěřina	k1gFnSc4	zvěřina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
RN	RN	kA	RN
–	–	k?	–
Round	round	k1gInSc1	round
Nose	nos	k1gInSc6	nos
–	–	k?	–
ogivální	ogivální	k2eAgFnSc1d1	ogivální
špička	špička	k1gFnSc1	špička
střely	střela	k1gFnSc2	střela
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SCHP	SCHP	kA	SCHP
–	–	k?	–
střela	střela	k1gFnSc1	střela
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
jedním	jeden	k4xCgInSc7	jeden
kusem	kus	k1gInSc7	kus
mědi	měď	k1gFnSc2	měď
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
není	být	k5eNaImIp3nS	být
zde	zde	k6eAd1	zde
dělení	dělení	k1gNnSc4	dělení
na	na	k7c4	na
jádro	jádro	k1gNnSc4	jádro
a	a	k8xC	a
plášť	plášť	k1gInSc4	plášť
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přední	přední	k2eAgFnSc2d1	přední
části	část	k1gFnSc2	část
je	být	k5eAaImIp3nS	být
přítomná	přítomný	k2eAgFnSc1d1	přítomná
dutinka	dutinka	k1gFnSc1	dutinka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SCP1	SCP1	k4	SCP1
–	–	k?	–
soft	soft	k?	soft
core	core	k1gInSc1	core
(	(	kIx(	(
<g/>
lead	lead	k1gMnSc1	lead
<g/>
)	)	kIx)	)
with	with	k1gMnSc1	with
steel	steel	k1gMnSc1	steel
penetrator	penetrator	k1gMnSc1	penetrator
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SJSP	SJSP	kA	SJSP
–	–	k?	–
flat	flat	k1gInSc1	flat
–	–	k?	–
semijacketed	semijacketed	k1gInSc1	semijacketed
soft	soft	k?	soft
point	pointa	k1gFnPc2	pointa
–	–	k?	–
flat	flat	k1gMnSc1	flat
–	–	k?	–
poloplášťová	poloplášťový	k2eAgFnSc1d1	poloplášťová
střela	střela	k1gFnSc1	střela
s	s	k7c7	s
plochou	plocha	k1gFnSc7	plocha
špičkou	špička	k1gFnSc7	špička
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SP	SP	kA	SP
–	–	k?	–
Soft	Soft	k?	Soft
Point	pointa	k1gFnPc2	pointa
–	–	k?	–
ta	ten	k3xDgNnPc4	ten
část	část	k1gFnSc1	část
střely	střela	k1gFnSc2	střela
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
přichází	přicházet	k5eAaImIp3nS	přicházet
do	do	k7c2	do
styku	styk	k1gInSc2	styk
s	s	k7c7	s
hlavní	hlavní	k2eAgFnSc7d1	hlavní
je	být	k5eAaImIp3nS	být
zakryta	zakrýt	k5eAaPmNgFnS	zakrýt
pláštěm	plášť	k1gInSc7	plášť
(	(	kIx(	(
<g/>
důvody	důvod	k1gInPc1	důvod
pro	pro	k7c4	pro
toto	tento	k3xDgNnSc4	tento
a	a	k8xC	a
materiály	materiál	k1gInPc4	materiál
pláště	plášť	k1gInSc2	plášť
viz	vidět	k5eAaImRp2nS	vidět
FMJ	FMJ	kA	FMJ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přední	přední	k2eAgFnSc6d1	přední
části	část	k1gFnSc6	část
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
ale	ale	k9	ale
obnažené	obnažený	k2eAgNnSc1d1	obnažené
olověné	olověný	k2eAgNnSc1d1	olověné
jádro	jádro	k1gNnSc1	jádro
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
větší	veliký	k2eAgFnSc1d2	veliký
měkkost	měkkost	k1gFnSc1	měkkost
této	tento	k3xDgFnSc3	tento
přední	přední	k2eAgFnSc1d1	přední
část	část	k1gFnSc1	část
než	než	k8xS	než
u	u	k7c2	u
FMJ	FMJ	kA	FMJ
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
konstrukce	konstrukce	k1gFnSc1	konstrukce
má	mít	k5eAaImIp3nS	mít
několik	několik	k4yIc4	několik
důsledků	důsledek	k1gInPc2	důsledek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
HP	HP	kA	HP
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
větší	veliký	k2eAgFnSc3d2	veliký
expanzi	expanze	k1gFnSc3	expanze
a	a	k8xC	a
menší	malý	k2eAgFnSc3d2	menší
penetraci	penetrace	k1gFnSc3	penetrace
(	(	kIx(	(
<g/>
výhody	výhoda	k1gFnPc1	výhoda
a	a	k8xC	a
nevýhody	nevýhoda	k1gFnPc1	nevýhoda
této	tento	k3xDgFnSc2	tento
skutečnosti	skutečnost	k1gFnSc2	skutečnost
viz	vidět	k5eAaImRp2nS	vidět
HP	HP	kA	HP
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
chtěné	chtěný	k2eAgNnSc1d1	chtěné
i	i	k8xC	i
nechtěné	chtěný	k2eNgNnSc1d1	nechtěné
<g/>
.	.	kIx.	.
</s>
<s>
Záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
konkrétním	konkrétní	k2eAgNnSc6d1	konkrétní
použití	použití	k1gNnSc6	použití
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yInSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
zastavovacího	zastavovací	k2eAgInSc2d1	zastavovací
efektu	efekt	k1gInSc2	efekt
této	tento	k3xDgFnSc2	tento
střely	střela	k1gFnSc2	střela
oproti	oproti	k7c3	oproti
FMJ	FMJ	kA	FMJ
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
rozdíl	rozdíl	k1gInSc4	rozdíl
u	u	k7c2	u
běžně	běžně	k6eAd1	běžně
používaných	používaný	k2eAgInPc2d1	používaný
nábojů	náboj	k1gInPc2	náboj
do	do	k7c2	do
ručních	ruční	k2eAgFnPc2d1	ruční
zbraní	zbraň	k1gFnPc2	zbraň
se	s	k7c7	s
střední	střední	k2eAgFnSc7d1	střední
a	a	k8xC	a
nižší	nízký	k2eAgFnSc7d2	nižší
úsťovou	úsťový	k2eAgFnSc7d1	úsťová
rychlostí	rychlost	k1gFnSc7	rychlost
je	být	k5eAaImIp3nS	být
rozdíl	rozdíl	k1gInSc1	rozdíl
minimální	minimální	k2eAgInSc1d1	minimální
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc7d1	velká
výhodou	výhoda	k1gFnSc7	výhoda
oproti	oproti	k7c3	oproti
HP	HP	kA	HP
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
pistolí	pistol	k1gFnPc2	pistol
je	být	k5eAaImIp3nS	být
konstruováno	konstruovat	k5eAaImNgNnS	konstruovat
na	na	k7c6	na
střelu	střel	k1gInSc6	střel
typu	typ	k1gInSc2	typ
FMJ	FMJ	kA	FMJ
a	a	k8xC	a
u	u	k7c2	u
HP	HP	kA	HP
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
nabíjením	nabíjení	k1gNnSc7	nabíjení
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
SP	SP	kA	SP
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
více	hodně	k6eAd2	hodně
variant	variant	k1gInSc4	variant
zejména	zejména	k9	zejména
co	co	k3yInSc1	co
do	do	k7c2	do
tvaru	tvar	k1gInSc2	tvar
přední	přední	k2eAgFnSc2d1	přední
části	část	k1gFnSc2	část
–	–	k?	–
např.	např.	kA	např.
PSP	PSP	kA	PSP
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SP	SP	kA	SP
–	–	k?	–
Spire	Spir	k1gInSc5	Spir
Point	pointa	k1gFnPc2	pointa
–	–	k?	–
Lovecká	lovecký	k2eAgFnSc1d1	lovecká
střela	střela	k1gFnSc1	střela
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
nárazu	náraz	k1gInSc6	náraz
deformuje	deformovat	k5eAaImIp3nS	deformovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SPTZ	SPTZ	kA	SPTZ
–	–	k?	–
Spitzer	Spitzer	k1gInSc1	Spitzer
–	–	k?	–
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c6	o
střelu	střel	k1gInSc6	střel
s	s	k7c7	s
vysoce	vysoce	k6eAd1	vysoce
aerodynamickým	aerodynamický	k2eAgInSc7d1	aerodynamický
tvarem	tvar	k1gInSc7	tvar
používanou	používaný	k2eAgFnSc4d1	používaná
ve	v	k7c6	v
středně	středně	k6eAd1	středně
a	a	k8xC	a
vysoce	vysoce	k6eAd1	vysoce
výkonných	výkonný	k2eAgInPc6d1	výkonný
nábojích	náboj	k1gInPc6	náboj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ST	St	kA	St
–	–	k?	–
Silver	Silver	k1gInSc1	Silver
Tip	tip	k1gInSc1	tip
</s>
</p>
<p>
<s>
STHP	STHP	kA	STHP
–	–	k?	–
Silver	Silver	k1gInSc1	Silver
Tip	tip	k1gInSc1	tip
Hollow	Hollow	k1gFnPc2	Hollow
Point	pointa	k1gFnPc2	pointa
–	–	k?	–
varianta	varianta	k1gFnSc1	varianta
HP	HP	kA	HP
<g/>
;	;	kIx,	;
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
ohledech	ohled	k1gInPc6	ohled
lepší	dobrý	k2eAgFnPc1d2	lepší
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vyšší	vysoký	k2eAgFnSc1d2	vyšší
cena	cena	k1gFnSc1	cena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TC	tc	k0	tc
–	–	k?	–
Truncated	Truncated	k1gInSc1	Truncated
Cone	Cone	k1gFnSc1	Cone
–	–	k?	–
označuje	označovat	k5eAaImIp3nS	označovat
tvar	tvar	k1gInSc4	tvar
střely	střela	k1gFnSc2	střela
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TFMJ	TFMJ	kA	TFMJ
–	–	k?	–
Total	totat	k5eAaImAgInS	totat
Full	Full	k1gInSc1	Full
Metal	metal	k1gInSc1	metal
Jacket	Jacket	k1gInSc1	Jacket
–	–	k?	–
celoplášť	celopláštit	k5eAaPmRp2nS	celopláštit
se	se	k3xPyFc4	se
zapláštěným	zapláštěný	k2eAgNnSc7d1	zapláštěný
dnem	dno	k1gNnSc7	dno
<g/>
,	,	kIx,	,
olověné	olověný	k2eAgNnSc1d1	olověné
jádro	jádro	k1gNnSc1	jádro
<g/>
,	,	kIx,	,
varianta	varianta	k1gFnSc1	varianta
FMJ	FMJ	kA	FMJ
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
u	u	k7c2	u
netoxických	toxický	k2eNgInPc2d1	netoxický
nábojů	náboj	k1gInPc2	náboj
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zabraňuje	zabraňovat	k5eAaImIp3nS	zabraňovat
odpařování	odpařování	k1gNnSc1	odpařování
olova	olovo	k1gNnSc2	olovo
při	při	k7c6	při
výstřelu	výstřel	k1gInSc6	výstřel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VLD	VLD	kA	VLD
–	–	k?	–
Very	Vera	k1gFnSc2	Vera
Low	Low	k1gMnSc1	Low
Drag	Drag	k1gMnSc1	Drag
–	–	k?	–
střela	střela	k1gFnSc1	střela
s	s	k7c7	s
vylepšenou	vylepšený	k2eAgFnSc7d1	vylepšená
aerodynamikou	aerodynamika	k1gFnSc7	aerodynamika
ke	k	k7c3	k
snížení	snížení	k1gNnSc3	snížení
odporu	odpor	k1gInSc2	odpor
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
střelbu	střelba	k1gFnSc4	střelba
na	na	k7c6	na
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
WC	WC	kA	WC
–	–	k?	–
Wadcutter	Wadcutter	k1gInSc1	Wadcutter
–	–	k?	–
speciální	speciální	k2eAgFnPc4d1	speciální
střely	střela	k1gFnPc4	střela
určené	určený	k2eAgFnPc4d1	určená
pro	pro	k7c4	pro
střelbu	střelba	k1gFnSc4	střelba
do	do	k7c2	do
papírových	papírový	k2eAgInPc2d1	papírový
terčů	terč	k1gInPc2	terč
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
zřetelné	zřetelný	k2eAgInPc4d1	zřetelný
průstřely	průstřel	k1gInPc4	průstřel
<g/>
.	.	kIx.	.
</s>
<s>
Náboje	náboj	k1gInPc1	náboj
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgInPc6	který
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
většinou	většinou	k6eAd1	většinou
podzvukovou	podzvukový	k2eAgFnSc4d1	podzvuková
úsťovou	úsťový	k2eAgFnSc4d1	úsťová
rychlost	rychlost	k1gFnSc4	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
ploché	plochý	k2eAgFnSc3d1	plochá
přední	přední	k2eAgFnSc3d1	přední
části	část	k1gFnSc3	část
střely	střela	k1gFnSc2	střela
může	moct	k5eAaImIp3nS	moct
docházet	docházet	k5eAaImF	docházet
k	k	k7c3	k
problémům	problém	k1gInPc3	problém
při	při	k7c6	při
nabíjení	nabíjení	k1gNnSc6	nabíjení
u	u	k7c2	u
automatických	automatický	k2eAgFnPc2d1	automatická
a	a	k8xC	a
poloautomatických	poloautomatický	k2eAgFnPc2d1	poloautomatická
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
jsou	být	k5eAaImIp3nP	být
používány	používat	k5eAaImNgFnP	používat
především	především	k9	především
v	v	k7c6	v
revolverech	revolver	k1gInPc6	revolver
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
WLN	WLN	kA	WLN
–	–	k?	–
Wide	Wide	k1gInSc1	Wide
Long	Long	k1gMnSc1	Long
Nose	nos	k1gInSc5	nos
</s>
</p>
<p>
<s>
XTP	XTP	kA	XTP
–	–	k?	–
Extreme	Extrem	k1gInSc5	Extrem
Terminal	Terminal	k1gMnSc1	Terminal
Performance	performance	k1gFnSc2	performance
–	–	k?	–
střely	střela	k1gFnSc2	střela
s	s	k7c7	s
řízenou	řízený	k2eAgFnSc7d1	řízená
deformací	deformace	k1gFnSc7	deformace
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
HP	HP	kA	HP
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
použít	použít	k5eAaPmF	použít
při	při	k7c6	při
velkém	velký	k2eAgInSc6d1	velký
rozsahu	rozsah	k1gInSc6	rozsah
úsťových	úsťový	k2eAgFnPc2d1	úsťová
rychlostí	rychlost	k1gFnPc2	rychlost
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
většiny	většina	k1gFnSc2	většina
HP	HP	kA	HP
střel	střel	k1gInSc1	střel
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
k	k	k7c3	k
požadované	požadovaný	k2eAgFnSc3d1	požadovaná
deformaci	deformace	k1gFnSc3	deformace
dojde	dojít	k5eAaPmIp3nS	dojít
pouze	pouze	k6eAd1	pouze
při	při	k7c6	při
určité	určitý	k2eAgFnSc6d1	určitá
konkrétní	konkrétní	k2eAgFnSc6d1	konkrétní
úsťové	úsťový	k2eAgFnSc6d1	úsťová
rychlosti	rychlost	k1gFnSc6	rychlost
nebo	nebo	k8xC	nebo
pouze	pouze	k6eAd1	pouze
malých	malý	k2eAgFnPc6d1	malá
odchylkách	odchylka	k1gFnPc6	odchylka
od	od	k7c2	od
ní	on	k3xPp3gFnSc2	on
<g/>
.	.	kIx.	.
<g/>
Dále	daleko	k6eAd2	daleko
existuje	existovat	k5eAaImIp3nS	existovat
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
různých	různý	k2eAgFnPc2d1	různá
patentovaných	patentovaný	k2eAgFnPc2d1	patentovaná
střel	střela	k1gFnPc2	střela
s	s	k7c7	s
důmyslnými	důmyslný	k2eAgFnPc7d1	důmyslná
konstrukcemi	konstrukce	k1gFnPc7	konstrukce
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
zlepšit	zlepšit	k5eAaPmF	zlepšit
všechny	všechen	k3xTgFnPc4	všechen
jejich	jejich	k3xOp3gFnPc4	jejich
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnPc1	jejich
ceny	cena	k1gFnPc1	cena
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
mnohem	mnohem	k6eAd1	mnohem
vyšší	vysoký	k2eAgFnPc1d2	vyšší
než	než	k8xS	než
u	u	k7c2	u
nejběžněji	běžně	k6eAd3	běžně
používaných	používaný	k2eAgFnPc2d1	používaná
střel	střela	k1gFnPc2	střela
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Střely	střel	k1gInPc7	střel
pro	pro	k7c4	pro
lafetované	lafetovaný	k2eAgFnPc4d1	lafetovaná
zbraně	zbraň	k1gFnPc4	zbraň
==	==	k?	==
</s>
</p>
<p>
<s>
Dělostřelecké	dělostřelecký	k2eAgInPc1d1	dělostřelecký
projektily	projektil	k1gInPc1	projektil
jsou	být	k5eAaImIp3nP	být
plněny	plnit	k5eAaImNgInP	plnit
trhavinou	trhavina	k1gFnSc7	trhavina
<g/>
,	,	kIx,	,
vybuchují	vybuchovat	k5eAaImIp3nP	vybuchovat
ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
nebo	nebo	k8xC	nebo
po	po	k7c6	po
dopadu	dopad	k1gInSc6	dopad
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
plněny	plnit	k5eAaImNgInP	plnit
radioaktivním	radioaktivní	k2eAgInSc7d1	radioaktivní
materiálem	materiál	k1gInSc7	materiál
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
i	i	k9	i
chemikáliemi	chemikálie	k1gFnPc7	chemikálie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rychlost	rychlost	k1gFnSc1	rychlost
projektilů	projektil	k1gInPc2	projektil
některých	některý	k3yIgFnPc2	některý
ráží	ráže	k1gFnPc2	ráže
zbraní	zbraň	k1gFnPc2	zbraň
po	po	k7c6	po
výstřelu	výstřel	k1gInSc6	výstřel
==	==	k?	==
</s>
</p>
<p>
<s>
Níže	nízce	k6eAd2	nízce
uvedená	uvedený	k2eAgFnSc1d1	uvedená
tabulka	tabulka	k1gFnSc1	tabulka
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
orientační	orientační	k2eAgFnSc1d1	orientační
<g/>
.	.	kIx.	.
</s>
<s>
Počáteční	počáteční	k2eAgFnSc1d1	počáteční
rychlost	rychlost	k1gFnSc1	rychlost
střely	střela	k1gFnSc2	střela
je	být	k5eAaImIp3nS	být
přímo	přímo	k6eAd1	přímo
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c4	na
více	hodně	k6eAd2	hodně
faktorech	faktor	k1gInPc6	faktor
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgMnPc7d1	hlavní
jsou	být	k5eAaImIp3nP	být
váha	váha	k1gFnSc1	váha
střely	střela	k1gFnSc2	střela
<g/>
,	,	kIx,	,
množství	množství	k1gNnSc1	množství
a	a	k8xC	a
druh	druh	k1gInSc1	druh
výmetné	výmetný	k2eAgFnSc2d1	výmetná
slože	slož	k1gFnSc2	slož
(	(	kIx(	(
<g/>
prachu	prach	k1gInSc2	prach
<g/>
)	)	kIx)	)
a	a	k8xC	a
délka	délka	k1gFnSc1	délka
hlavně	hlavně	k6eAd1	hlavně
<g/>
.	.	kIx.	.
</s>
<s>
Vlivů	vliv	k1gInPc2	vliv
na	na	k7c4	na
počáteční	počáteční	k2eAgFnSc4d1	počáteční
rychlost	rychlost	k1gFnSc4	rychlost
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pro	pro	k7c4	pro
praxi	praxe	k1gFnSc4	praxe
jsou	být	k5eAaImIp3nP	být
zanedbatelné	zanedbatelný	k2eAgFnPc1d1	zanedbatelná
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
náboj	náboj	k1gInSc4	náboj
6,5	[number]	k4	6,5
×	×	k?	×
65	[number]	k4	65
RWS	RWS	kA	RWS
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
1	[number]	k4	1
000	[number]	k4	000
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
5,6	[number]	k4	5,6
Ultra	ultra	k2eAgInSc1d1	ultra
Rapid	rapid	k1gInSc1	rapid
1	[number]	k4	1
030	[number]	k4	030
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
9	[number]	k4	9
mm	mm	kA	mm
Luger	Luger	k1gInSc4	Luger
subsonic	subsonice	k1gFnPc2	subsonice
290	[number]	k4	290
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
Část	část	k1gFnSc1	část
seznamu	seznam	k1gInSc2	seznam
zkratek	zkratka	k1gFnPc2	zkratka
označení	označení	k1gNnPc2	označení
byla	být	k5eAaImAgFnS	být
převzata	převzít	k5eAaPmNgFnS	převzít
z	z	k7c2	z
článku	článek	k1gInSc2	článek
"	"	kIx"	"
<g/>
Bullet	Bullet	k1gInSc1	Bullet
<g/>
"	"	kIx"	"
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
wikipedii	wikipedie	k1gFnSc6	wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
nábojů	náboj	k1gInPc2	náboj
do	do	k7c2	do
ručních	ruční	k2eAgFnPc2d1	ruční
zbraní	zbraň	k1gFnPc2	zbraň
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
nábojů	náboj	k1gInPc2	náboj
do	do	k7c2	do
pušek	puška	k1gFnPc2	puška
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
pistolí	pistol	k1gFnPc2	pistol
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
revolverů	revolver	k1gInPc2	revolver
</s>
</p>
<p>
<s>
Balistický	balistický	k2eAgInSc1d1	balistický
koeficient	koeficient	k1gInSc1	koeficient
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
Kontstrukce	Kontstrukce	k1gFnSc1	Kontstrukce
střely	střela	k1gFnSc2	střela
Snail	Snaila	k1gFnPc2	Snaila
</s>
</p>
