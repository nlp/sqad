<s>
New	New	k?	New
Horizons	Horizons	k1gInSc1	Horizons
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Nové	Nové	k2eAgInPc1d1	Nové
obzory	obzor	k1gInPc1	obzor
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americká	americký	k2eAgFnSc1d1	americká
planetární	planetární	k2eAgFnSc1d1	planetární
sonda	sonda	k1gFnSc1	sonda
<g/>
,	,	kIx,	,
určená	určený	k2eAgFnSc1d1	určená
k	k	k7c3	k
průzkumu	průzkum	k1gInSc3	průzkum
trpasličí	trpasličí	k2eAgFnSc2d1	trpasličí
planety	planeta	k1gFnSc2	planeta
Pluto	Pluto	k1gMnSc1	Pluto
<g/>
,	,	kIx,	,
jejích	její	k3xOp3gInPc2	její
měsíců	měsíc	k1gInPc2	měsíc
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
transneptunických	transneptunický	k2eAgNnPc2d1	transneptunické
těles	těleso	k1gNnPc2	těleso
(	(	kIx(	(
<g/>
TNO	TNO	kA	TNO
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
první	první	k4xOgFnSc1	první
expedice	expedice	k1gFnSc1	expedice
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
nového	nový	k2eAgInSc2d1	nový
rámcového	rámcový	k2eAgInSc2d1	rámcový
programu	program	k1gInSc2	program
NASA	NASA	kA	NASA
New	New	k1gMnPc2	New
Frontiers	Frontiersa	k1gFnPc2	Frontiersa
<g/>
.	.	kIx.	.
</s>
<s>
Sondu	sonda	k1gFnSc4	sonda
postavila	postavit	k5eAaPmAgFnS	postavit
a	a	k8xC	a
provozuje	provozovat	k5eAaImIp3nS	provozovat
Laboratoř	laboratoř	k1gFnSc1	laboratoř
aplikované	aplikovaný	k2eAgFnSc2d1	aplikovaná
fyziky	fyzika	k1gFnSc2	fyzika
(	(	kIx(	(
<g/>
APL	APL	kA	APL
<g/>
)	)	kIx)	)
při	pře	k1gFnSc4	pře
Johns	Johnsa	k1gFnPc2	Johnsa
Hopkins	Hopkinsa	k1gFnPc2	Hopkinsa
University	universita	k1gFnSc2	universita
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
programu	program	k1gInSc2	program
New	New	k1gFnSc2	New
Frontiers	Frontiersa	k1gFnPc2	Frontiersa
financuje	financovat	k5eAaBmIp3nS	financovat
ředitelství	ředitelství	k1gNnSc1	ředitelství
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
misí	mise	k1gFnPc2	mise
(	(	kIx(	(
<g/>
Science	Science	k1gFnSc1	Science
Mission	Mission	k1gInSc1	Mission
Directorate	Directorat	k1gInSc5	Directorat
<g/>
)	)	kIx)	)
při	při	k7c6	při
ústředí	ústředí	k1gNnSc6	ústředí
NASA	NASA	kA	NASA
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
vědeckého	vědecký	k2eAgNnSc2d1	vědecké
vybavení	vybavení	k1gNnSc2	vybavení
koordinuje	koordinovat	k5eAaBmIp3nS	koordinovat
Southwest	Southwest	k1gMnSc1	Southwest
Research	Research	k1gMnSc1	Research
Institute	institut	k1gInSc5	institut
(	(	kIx(	(
<g/>
SwRI	SwRI	k1gMnSc6	SwRI
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sonda	sonda	k1gFnSc1	sonda
od	od	k7c2	od
svého	svůj	k3xOyFgNnSc2	svůj
vypuštění	vypuštění	k1gNnSc2	vypuštění
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
do	do	k7c2	do
průletu	průlet	k1gInSc2	průlet
okolo	okolo	k7c2	okolo
Pluta	Pluto	k1gNnSc2	Pluto
14	[number]	k4	14
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2015	[number]	k4	2015
urazila	urazit	k5eAaPmAgFnS	urazit
téměř	téměř	k6eAd1	téměř
4,9	[number]	k4	4,9
miliardy	miliarda	k4xCgFnSc2	miliarda
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
průletu	průlet	k1gInSc6	průlet
pořídila	pořídit	k5eAaPmAgFnS	pořídit
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
vědeckých	vědecký	k2eAgNnPc2d1	vědecké
dat	datum	k1gNnPc2	datum
a	a	k8xC	a
fotografií	fotografia	k1gFnPc2	fotografia
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
budou	být	k5eAaImBp3nP	být
v	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
16	[number]	k4	16
měsících	měsíc	k1gInPc6	měsíc
po	po	k7c6	po
průletu	průlet	k1gInSc6	průlet
postupně	postupně	k6eAd1	postupně
odvysílány	odvysílán	k2eAgInPc1d1	odvysílán
na	na	k7c4	na
síť	síť	k1gFnSc4	síť
pozemních	pozemní	k2eAgFnPc2d1	pozemní
přijímacích	přijímací	k2eAgFnPc2d1	přijímací
stanic	stanice	k1gFnPc2	stanice
systému	systém	k1gInSc2	systém
DSN	DSN	kA	DSN
(	(	kIx(	(
<g/>
Deep	Deep	k1gMnSc1	Deep
Space	Space	k1gFnSc2	Space
Network	network	k1gInSc1	network
<g/>
)	)	kIx)	)
k	k	k7c3	k
dalšímu	další	k2eAgNnSc3d1	další
zpracování	zpracování	k1gNnSc3	zpracování
<g/>
.	.	kIx.	.
</s>
<s>
Pořídila	pořídit	k5eAaPmAgFnS	pořídit
tak	tak	k9	tak
jedinečné	jedinečný	k2eAgInPc4d1	jedinečný
materiály	materiál	k1gInPc4	materiál
o	o	k7c6	o
jednom	jeden	k4xCgInSc6	jeden
z	z	k7c2	z
posledních	poslední	k2eAgInPc2d1	poslední
neprobádaných	probádaný	k2eNgInPc2d1	neprobádaný
světů	svět	k1gInPc2	svět
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgNnSc7d1	další
tělesem	těleso	k1gNnSc7	těleso
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yQgMnSc3	který
sonda	sonda	k1gFnSc1	sonda
zamíří	zamířit	k5eAaPmIp3nS	zamířit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
2014	[number]	k4	2014
MU	MU	kA	MU
<g/>
69	[number]	k4	69
<g/>
.	.	kIx.	.
</s>
<s>
Sonda	sonda	k1gFnSc1	sonda
by	by	kYmCp3nS	by
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
měla	mít	k5eAaImAgFnS	mít
dorazit	dorazit	k5eAaPmF	dorazit
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2019	[number]	k4	2019
<g/>
.	.	kIx.	.
</s>
<s>
Náklady	náklad	k1gInPc1	náklad
na	na	k7c4	na
misi	mise	k1gFnSc4	mise
byly	být	k5eAaImAgFnP	být
přibližně	přibližně	k6eAd1	přibližně
vyčísleny	vyčíslen	k2eAgFnPc1d1	vyčíslena
na	na	k7c4	na
700	[number]	k4	700
miliónů	milión	k4xCgInPc2	milión
amerických	americký	k2eAgInPc2d1	americký
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Sonda	sonda	k1gFnSc1	sonda
je	být	k5eAaImIp3nS	být
tříose	tříose	k6eAd1	tříose
případně	případně	k6eAd1	případně
rotací	rotace	k1gFnSc7	rotace
stabilizovaná	stabilizovaný	k2eAgFnSc1d1	stabilizovaná
<g/>
,	,	kIx,	,
tvaru	tvar	k1gInSc2	tvar
nízkého	nízký	k2eAgInSc2d1	nízký
nepravidelného	pravidelný	k2eNgInSc2d1	nepravidelný
šestibokého	šestiboký	k2eAgInSc2d1	šestiboký
hranolu	hranol	k1gInSc2	hranol
o	o	k7c6	o
rozměrech	rozměr	k1gInPc6	rozměr
základny	základna	k1gFnSc2	základna
přibližně	přibližně	k6eAd1	přibližně
2,1	[number]	k4	2,1
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
2,7	[number]	k4	2,7
m	m	kA	m
včetně	včetně	k7c2	včetně
RTG	RTG	kA	RTG
<g/>
)	)	kIx)	)
×	×	k?	×
1,8	[number]	k4	1,8
m	m	kA	m
a	a	k8xC	a
výšce	výška	k1gFnSc6	výška
0,7	[number]	k4	0,7
m	m	kA	m
(	(	kIx(	(
<g/>
2,2	[number]	k4	2,2
m	m	kA	m
včetně	včetně	k7c2	včetně
anténního	anténní	k2eAgInSc2d1	anténní
systému	systém	k1gInSc2	systém
a	a	k8xC	a
adaptéru	adaptér	k1gInSc2	adaptér
pro	pro	k7c4	pro
připojení	připojení	k1gNnSc4	připojení
k	k	k7c3	k
nosné	nosný	k2eAgFnSc3d1	nosná
raketě	raketa	k1gFnSc3	raketa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vybavena	vybavit	k5eAaPmNgFnS	vybavit
radioizotopovým	radioizotopový	k2eAgInSc7d1	radioizotopový
termoelektrickým	termoelektrický	k2eAgInSc7d1	termoelektrický
generátorem	generátor	k1gInSc7	generátor
RTG	RTG	kA	RTG
(	(	kIx(	(
<g/>
Radioisotope	radioisotop	k1gInSc5	radioisotop
Thermolectric	Thermolectric	k1gMnSc1	Thermolectric
Generator	Generator	k1gInSc1	Generator
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dodávajícím	dodávající	k2eAgInSc6d1	dodávající
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
mise	mise	k1gFnSc2	mise
240	[number]	k4	240
W	W	kA	W
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
minimálně	minimálně	k6eAd1	minimálně
200	[number]	k4	200
W	W	kA	W
<g/>
)	)	kIx)	)
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
zdroj	zdroj	k1gInSc1	zdroj
slouží	sloužit	k5eAaImIp3nS	sloužit
11	[number]	k4	11
kg	kg	kA	kg
oxidu	oxid	k1gInSc2	oxid
plutoničitého	plutoničitý	k2eAgInSc2d1	plutoničitý
238	[number]	k4	238
<g/>
PuO	PuO	k1gFnPc7	PuO
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Sonda	sonda	k1gFnSc1	sonda
nese	nést	k5eAaImIp3nS	nést
sedm	sedm	k4xCc4	sedm
vědeckých	vědecký	k2eAgInPc2d1	vědecký
experimentů	experiment	k1gInPc2	experiment
o	o	k7c6	o
celkové	celkový	k2eAgFnSc6d1	celková
hmotnosti	hmotnost	k1gFnSc6	hmotnost
30	[number]	k4	30
kg	kg	kA	kg
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
<g/>
:	:	kIx,	:
vysokorozlišující	vysokorozlišující	k2eAgInSc4d1	vysokorozlišující
spektrometr	spektrometr	k1gInSc4	spektrometr
Alice	Alice	k1gFnSc2	Alice
pro	pro	k7c4	pro
extrémní	extrémní	k2eAgFnSc4d1	extrémní
a	a	k8xC	a
vzdálenou	vzdálený	k2eAgFnSc4d1	vzdálená
ultrafialovou	ultrafialový	k2eAgFnSc4d1	ultrafialová
oblast	oblast	k1gFnSc4	oblast
(	(	kIx(	(
<g/>
spektrální	spektrální	k2eAgInSc1d1	spektrální
rozsah	rozsah	k1gInSc1	rozsah
50	[number]	k4	50
–	–	k?	–
180	[number]	k4	180
nm	nm	k?	nm
<g/>
,	,	kIx,	,
1024	[number]	k4	1024
kanálů	kanál	k1gInPc2	kanál
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
souprava	souprava	k1gFnSc1	souprava
kamer	kamera	k1gFnPc2	kamera
Ralph	Ralpha	k1gFnPc2	Ralpha
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
kamery	kamera	k1gFnPc1	kamera
MVIC	MVIC	kA	MVIC
(	(	kIx(	(
<g/>
Multispectra	Multispectr	k1gMnSc2	Multispectr
Visible	Visible	k1gFnSc1	Visible
Imaging	Imaging	k1gInSc1	Imaging
Camera	Camera	k1gFnSc1	Camera
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
viditelnou	viditelný	k2eAgFnSc4d1	viditelná
oblast	oblast	k1gFnSc4	oblast
(	(	kIx(	(
<g/>
prostorové	prostorový	k2eAgNnSc4d1	prostorové
rozlišení	rozlišení	k1gNnSc4	rozlišení
250	[number]	k4	250
m	m	kA	m
na	na	k7c4	na
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
10	[number]	k4	10
000	[number]	k4	000
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
<g/>
:	:	kIx,	:
tři	tři	k4xCgFnPc1	tři
panchromatické	panchromatický	k2eAgFnPc1d1	panchromatická
kamery	kamera	k1gFnPc1	kamera
s	s	k7c7	s
detekčními	detekční	k2eAgInPc7d1	detekční
prvky	prvek	k1gInPc7	prvek
CCD	CCD	kA	CCD
<g/>
;	;	kIx,	;
čtyři	čtyři	k4xCgFnPc1	čtyři
barevné	barevný	k2eAgFnPc1d1	barevná
kamery	kamera	k1gFnPc1	kamera
s	s	k7c7	s
detekčními	detekční	k2eAgInPc7d1	detekční
prvky	prvek	k1gInPc7	prvek
CCD	CCD	kA	CCD
<g/>
;	;	kIx,	;
<g />
.	.	kIx.	.
</s>
<s>
infračervený	infračervený	k2eAgInSc1d1	infračervený
zobrazující	zobrazující	k2eAgInSc1d1	zobrazující
spektrometr	spektrometr	k1gInSc1	spektrometr
LEISA	LEISA	kA	LEISA
(	(	kIx(	(
<g/>
Linear	Linear	k1gInSc1	Linear
Etalon	etalon	k1gInSc1	etalon
Imaging	Imaging	k1gInSc1	Imaging
Spectral	Spectral	k1gMnSc2	Spectral
Array	Arraa	k1gMnSc2	Arraa
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
mapování	mapování	k1gNnSc4	mapování
rozložení	rozložení	k1gNnSc2	rozložení
N	N	kA	N
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
CH	Ch	kA	Ch
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
CO	co	k6eAd1	co
<g/>
,	,	kIx,	,
H2O	H2O	k1gFnPc2	H2O
a	a	k8xC	a
organických	organický	k2eAgFnPc2d1	organická
látek	látka	k1gFnPc2	látka
<g/>
;	;	kIx,	;
rádiový	rádiový	k2eAgInSc1d1	rádiový
experiment	experiment	k1gInSc1	experiment
REX	REX	kA	REX
(	(	kIx(	(
<g/>
Radio	radio	k1gNnSc1	radio
Science	Scienec	k1gInSc2	Scienec
Experiment	experiment	k1gInSc1	experiment
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
<g/>
:	:	kIx,	:
studium	studium	k1gNnSc4	studium
vlastností	vlastnost	k1gFnPc2	vlastnost
<g />
.	.	kIx.	.
</s>
<s>
atmosféry	atmosféra	k1gFnPc1	atmosféra
Pluta	Pluto	k1gNnSc2	Pluto
ze	z	k7c2	z
změn	změna	k1gFnPc2	změna
rádiových	rádiový	k2eAgFnPc2d1	rádiová
vln	vlna	k1gFnPc2	vlna
během	během	k7c2	během
rádiového	rádiový	k2eAgInSc2d1	rádiový
zákrytu	zákryt	k1gInSc2	zákryt
sondy	sonda	k1gFnSc2	sonda
za	za	k7c7	za
planetou	planeta	k1gFnSc7	planeta
<g/>
;	;	kIx,	;
měření	měření	k1gNnSc1	měření
rádiového	rádiový	k2eAgNnSc2d1	rádiové
záření	záření	k1gNnSc2	záření
Pluta	Pluto	k1gNnSc2	Pluto
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
těles	těleso	k1gNnPc2	těleso
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
;	;	kIx,	;
dlouhofokální	dlouhofokální	k2eAgFnSc1d1	dlouhofokální
kamera	kamera	k1gFnSc1	kamera
LORRI	LORRI	kA	LORRI
(	(	kIx(	(
<g/>
Long	Long	k1gMnSc1	Long
Range	Rang	k1gFnSc2	Rang
Reconnaisance	Reconnaisance	k1gFnSc1	Reconnaisance
Imager	Imager	k1gMnSc1	Imager
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
snímkování	snímkování	k1gNnSc4	snímkování
Pluta	Pluto	k1gNnSc2	Pluto
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
těles	těleso	k1gNnPc2	těleso
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
(	(	kIx(	(
<g/>
prostorové	prostorový	k2eAgNnSc4d1	prostorové
rozlišení	rozlišení	k1gNnSc4	rozlišení
50	[number]	k4	50
m	m	kA	m
<g />
.	.	kIx.	.
</s>
<s>
na	na	k7c4	na
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
10	[number]	k4	10
000	[number]	k4	000
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
přístroj	přístroj	k1gInSc1	přístroj
SWAP	SWAP	kA	SWAP
(	(	kIx(	(
<g/>
Solar	Solar	k1gMnSc1	Solar
Wind	Wind	k1gMnSc1	Wind
at	at	k?	at
Pluto	Pluto	k1gMnSc1	Pluto
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
studium	studium	k1gNnSc4	studium
interakce	interakce	k1gFnSc2	interakce
planety	planeta	k1gFnSc2	planeta
se	s	k7c7	s
slunečním	sluneční	k2eAgInSc7d1	sluneční
větrem	vítr	k1gInSc7	vítr
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
<g/>
:	:	kIx,	:
analyzátor	analyzátor	k1gInSc1	analyzátor
plazmatu	plazma	k1gNnSc2	plazma
RPA	RPA	kA	RPA
(	(	kIx(	(
<g/>
Retarding	Retarding	k1gInSc1	Retarding
Potential	Potential	k1gMnSc1	Potential
Analyzer	Analyzer	k1gMnSc1	Analyzer
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
elektrostatický	elektrostatický	k2eAgInSc1d1	elektrostatický
analyzátor	analyzátor	k1gInSc1	analyzátor
nabitých	nabitý	k2eAgFnPc2d1	nabitá
částice	částice	k1gFnSc1	částice
ESA	eso	k1gNnSc2	eso
(	(	kIx(	(
<g/>
Electrostatic	Electrostatice	k1gFnPc2	Electrostatice
Analyzer	Analyzer	k1gInSc1	Analyzer
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
analyzátor	analyzátor	k1gInSc1	analyzátor
energetických	energetický	k2eAgFnPc2d1	energetická
částic	částice	k1gFnPc2	částice
PEPSSI	PEPSSI	kA	PEPSSI
(	(	kIx(	(
<g/>
Pluto	Pluto	k1gNnSc1	Pluto
Energetic	Energetice	k1gFnPc2	Energetice
Particle	Particle	k1gNnPc2	Particle
Spectrometer	Spectrometer	k1gMnSc1	Spectrometer
Science	Science	k1gFnSc2	Science
Investigation	Investigation	k1gInSc1	Investigation
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
detektor	detektor	k1gInSc1	detektor
kosmických	kosmický	k2eAgFnPc2d1	kosmická
prachových	prachový	k2eAgFnPc2d1	prachová
částic	částice	k1gFnPc2	částice
SDC	SDC	kA	SDC
(	(	kIx(	(
<g/>
Student	student	k1gMnSc1	student
Dust	Dust	k1gMnSc1	Dust
Counter	Counter	k1gMnSc1	Counter
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Řízení	řízení	k1gNnSc1	řízení
sondy	sonda	k1gFnSc2	sonda
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
procesor	procesor	k1gInSc1	procesor
typu	typ	k1gInSc2	typ
Mongoose	Mongoosa	k1gFnSc3	Mongoosa
V	V	kA	V
(	(	kIx(	(
<g/>
taktová	taktový	k2eAgFnSc1d1	taktová
frekvence	frekvence	k1gFnSc1	frekvence
12	[number]	k4	12
MHz	Mhz	kA	Mhz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vědecká	vědecký	k2eAgNnPc1d1	vědecké
a	a	k8xC	a
technická	technický	k2eAgNnPc1d1	technické
data	datum	k1gNnPc1	datum
jsou	být	k5eAaImIp3nP	být
zaznamenávána	zaznamenávat	k5eAaImNgNnP	zaznamenávat
do	do	k7c2	do
dvou	dva	k4xCgInPc2	dva
polovodičových	polovodičový	k2eAgFnPc2d1	polovodičová
velkokapacitních	velkokapacitní	k2eAgFnPc2d1	velkokapacitní
pamětí	paměť	k1gFnPc2	paměť
s	s	k7c7	s
kapacitou	kapacita	k1gFnSc7	kapacita
2	[number]	k4	2
×	×	k?	×
64	[number]	k4	64
Gbit	Gbita	k1gFnPc2	Gbita
<g/>
.	.	kIx.	.
</s>
<s>
Komunikační	komunikační	k2eAgInSc1d1	komunikační
systém	systém	k1gInSc1	systém
sond	sonda	k1gFnPc2	sonda
pracuje	pracovat	k5eAaImIp3nS	pracovat
v	v	k7c6	v
pásmu	pásmo	k1gNnSc6	pásmo
X	X	kA	X
(	(	kIx(	(
<g/>
8	[number]	k4	8
GHz	GHz	k1gFnPc2	GHz
<g/>
,	,	kIx,	,
přenosová	přenosový	k2eAgFnSc1d1	přenosová
rychlost	rychlost	k1gFnSc1	rychlost
od	od	k7c2	od
Pluta	Pluto	k1gNnSc2	Pluto
800	[number]	k4	800
bit	bit	k1gInSc1	bit
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
přenos	přenos	k1gInSc1	přenos
veškerých	veškerý	k3xTgNnPc2	veškerý
dat	datum	k1gNnPc2	datum
z	z	k7c2	z
průletu	průlet	k1gInSc2	průlet
kolem	kolem	k7c2	kolem
Pluta	Pluto	k1gNnSc2	Pluto
bude	být	k5eAaImBp3nS	být
trvat	trvat	k5eAaImF	trvat
9	[number]	k4	9
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Sonda	sonda	k1gFnSc1	sonda
je	být	k5eAaImIp3nS	být
dále	daleko	k6eAd2	daleko
vybavena	vybavit	k5eAaPmNgFnS	vybavit
12	[number]	k4	12
motorky	motorka	k1gFnPc4	motorka
na	na	k7c4	na
jednosložkové	jednosložkový	k2eAgFnPc4d1	jednosložková
kapalné	kapalný	k2eAgFnPc4d1	kapalná
pohonné	pohonný	k2eAgFnPc4d1	pohonná
látky	látka	k1gFnPc4	látka
(	(	kIx(	(
<g/>
hydrazin	hydrazin	k1gInSc1	hydrazin
<g/>
)	)	kIx)	)
o	o	k7c6	o
tahu	tah	k1gInSc6	tah
12	[number]	k4	12
×	×	k?	×
0,8	[number]	k4	0,8
N	N	kA	N
a	a	k8xC	a
4	[number]	k4	4
silnějšími	silný	k2eAgInPc7d2	silnější
motorky	motorek	k1gInPc7	motorek
na	na	k7c4	na
stejné	stejný	k2eAgFnPc4d1	stejná
pohonné	pohonný	k2eAgFnPc4d1	pohonná
látky	látka	k1gFnPc4	látka
o	o	k7c6	o
tahu	tah	k1gInSc6	tah
4	[number]	k4	4
×	×	k?	×
4,4	[number]	k4	4,4
N	N	kA	N
<g/>
;	;	kIx,	;
motorky	motorek	k1gInPc1	motorek
jsou	být	k5eAaImIp3nP	být
rozděleny	rozdělit	k5eAaPmNgInP	rozdělit
do	do	k7c2	do
dvou	dva	k4xCgInPc2	dva
nezávislých	závislý	k2eNgInPc2d1	nezávislý
okruhů	okruh	k1gInPc2	okruh
po	po	k7c6	po
8	[number]	k4	8
motorcích	motorek	k1gInPc6	motorek
<g/>
.	.	kIx.	.
</s>
<s>
Motorky	motorka	k1gFnPc1	motorka
slouží	sloužit	k5eAaImIp3nP	sloužit
k	k	k7c3	k
udržování	udržování	k1gNnSc3	udržování
orientace	orientace	k1gFnSc2	orientace
během	během	k7c2	během
zkoumání	zkoumání	k1gNnSc2	zkoumání
planet	planeta	k1gFnPc2	planeta
a	a	k8xC	a
během	během	k7c2	během
korekčních	korekční	k2eAgInPc2d1	korekční
manévrů	manévr	k1gInPc2	manévr
<g/>
;	;	kIx,	;
většinu	většina	k1gFnSc4	většina
doby	doba	k1gFnSc2	doba
přeletu	přelet	k1gInSc2	přelet
meziplanetárním	meziplanetární	k2eAgInSc7d1	meziplanetární
prostorem	prostor	k1gInSc7	prostor
je	být	k5eAaImIp3nS	být
orientace	orientace	k1gFnSc1	orientace
udržována	udržovat	k5eAaImNgFnS	udržovat
rotací	rotace	k1gFnSc7	rotace
sondy	sonda	k1gFnSc2	sonda
rychlostí	rychlost	k1gFnPc2	rychlost
přibližně	přibližně	k6eAd1	přibližně
5	[number]	k4	5
obr	obr	k1gMnSc1	obr
<g/>
/	/	kIx~	/
<g/>
min	min	kA	min
<g/>
.	.	kIx.	.
Pro	pro	k7c4	pro
zjišťování	zjišťování	k1gNnSc4	zjišťování
orientace	orientace	k1gFnSc2	orientace
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
slouží	sloužit	k5eAaImIp3nS	sloužit
inerciální	inerciální	k2eAgFnSc1d1	inerciální
plošina	plošina	k1gFnSc1	plošina
IMU	IMU	kA	IMU
(	(	kIx(	(
<g/>
Inertial	Inertial	k1gMnSc1	Inertial
Measurement	Measurement	k1gMnSc1	Measurement
Unit	Unit	k1gMnSc1	Unit
<g/>
)	)	kIx)	)
a	a	k8xC	a
systém	systém	k1gInSc1	systém
sledovačů	sledovač	k1gInPc2	sledovač
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Let	let	k1gInSc1	let
sondy	sonda	k1gFnSc2	sonda
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
technické	technický	k2eAgFnSc6d1	technická
stránce	stránka	k1gFnSc6	stránka
řízen	řídit	k5eAaImNgInS	řídit
ze	z	k7c2	z
střediska	středisko	k1gNnSc2	středisko
Mission	Mission	k1gInSc1	Mission
Operations	Operations	k1gInSc1	Operations
Center	centrum	k1gNnPc2	centrum
(	(	kIx(	(
<g/>
MOC	moc	k1gFnSc1	moc
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
umístěného	umístěný	k2eAgInSc2d1	umístěný
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
Johns	Johnsa	k1gFnPc2	Johnsa
Hopkins	Hopkinsa	k1gFnPc2	Hopkinsa
University	universita	k1gFnSc2	universita
<g/>
.	.	kIx.	.
</s>
<s>
Práci	práce	k1gFnSc4	práce
vědeckých	vědecký	k2eAgInPc2d1	vědecký
přístrojů	přístroj	k1gInPc2	přístroj
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
středisko	středisko	k1gNnSc1	středisko
SOC	soc	kA	soc
(	(	kIx(	(
<g/>
Science	Science	k1gFnSc1	Science
Operations	Operations	k1gInSc1	Operations
Center	centrum	k1gNnPc2	centrum
<g/>
)	)	kIx)	)
na	na	k7c4	na
Southwest	Southwest	k1gInSc4	Southwest
Research	Researcha	k1gFnPc2	Researcha
Institute	institut	k1gInSc5	institut
<g/>
.	.	kIx.	.
</s>
<s>
Spojení	spojení	k1gNnSc1	spojení
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
celosvětová	celosvětový	k2eAgFnSc1d1	celosvětová
síť	síť	k1gFnSc1	síť
sledovacích	sledovací	k2eAgFnPc2d1	sledovací
stanic	stanice	k1gFnPc2	stanice
dálkového	dálkový	k2eAgNnSc2d1	dálkové
kosmického	kosmický	k2eAgNnSc2d1	kosmické
spojení	spojení	k1gNnSc2	spojení
DSN	DSN	kA	DSN
(	(	kIx(	(
<g/>
Deep	Deep	k1gMnSc1	Deep
Space	Space	k1gFnSc2	Space
Network	network	k1gInSc1	network
<g/>
)	)	kIx)	)
organizace	organizace	k1gFnSc2	organizace
NASA	NASA	kA	NASA
<g/>
.	.	kIx.	.
</s>
<s>
Nosná	nosný	k2eAgFnSc1d1	nosná
raketa	raketa	k1gFnSc1	raketa
typu	typ	k1gInSc2	typ
Atlas	Atlas	k1gInSc1	Atlas
V	v	k7c4	v
model	model	k1gInSc4	model
551	[number]	k4	551
(	(	kIx(	(
<g/>
výr	výr	k1gMnSc1	výr
<g/>
.	.	kIx.	.
č.	č.	k?	č.
AV-	AV-	k1gFnSc7	AV-
<g/>
0	[number]	k4	0
<g/>
10	[number]	k4	10
<g/>
)	)	kIx)	)
s	s	k7c7	s
urychlovacím	urychlovací	k2eAgMnSc7d1	urychlovací
třetím	třetí	k4xOgNnSc7	třetí
stupněm	stupeň	k1gInSc7	stupeň
Star	Star	kA	Star
48	[number]	k4	48
vzlétla	vzlétnout	k5eAaPmAgFnS	vzlétnout
po	po	k7c6	po
několika	několik	k4yIc6	několik
odkladech	odklad	k1gInPc6	odklad
<g/>
,	,	kIx,	,
způsobených	způsobený	k2eAgFnPc6d1	způsobená
převážně	převážně	k6eAd1	převážně
špatným	špatný	k2eAgNnSc7d1	špatné
počasím	počasí	k1gNnSc7	počasí
<g/>
,	,	kIx,	,
z	z	k7c2	z
rampy	rampa	k1gFnSc2	rampa
SLC-41	SLC-41	k1gFnSc2	SLC-41
kosmodromu	kosmodrom	k1gInSc2	kosmodrom
Eastern	Eastern	k1gInSc1	Eastern
Test	test	k1gInSc1	test
Range	Range	k1gInSc1	Range
na	na	k7c6	na
Floridě	Florida	k1gFnSc6	Florida
19	[number]	k4	19
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2006	[number]	k4	2006
v	v	k7c4	v
19	[number]	k4	19
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
světového	světový	k2eAgInSc2d1	světový
času	čas	k1gInSc2	čas
(	(	kIx(	(
<g/>
UT	UT	kA	UT
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
:	:	kIx,	:
<g/>
41	[number]	k4	41
<g/>
:	:	kIx,	:
<g/>
41	[number]	k4	41
UT	UT	kA	UT
třetí	třetí	k4xOgInSc4	třetí
stupeň	stupeň	k1gInSc4	stupeň
navedl	navést	k5eAaPmAgMnS	navést
užitečné	užitečný	k2eAgNnSc4d1	užitečné
zatížení	zatížení	k1gNnSc4	zatížení
na	na	k7c4	na
hyperbolickou	hyperbolický	k2eAgFnSc4d1	hyperbolická
únikovou	únikový	k2eAgFnSc4d1	úniková
dráhu	dráha	k1gFnSc4	dráha
ze	z	k7c2	z
zemského	zemský	k2eAgNnSc2d1	zemské
gravitačního	gravitační	k2eAgNnSc2d1	gravitační
pole	pole	k1gNnSc2	pole
a	a	k8xC	a
v	v	k7c6	v
19	[number]	k4	19
<g/>
:	:	kIx,	:
<g/>
44	[number]	k4	44
<g/>
:	:	kIx,	:
<g/>
55	[number]	k4	55
UT	UT	kA	UT
se	se	k3xPyFc4	se
od	od	k7c2	od
tohoto	tento	k3xDgInSc2	tento
stupně	stupeň	k1gInSc2	stupeň
sonda	sonda	k1gFnSc1	sonda
oddělila	oddělit	k5eAaPmAgFnS	oddělit
a	a	k8xC	a
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
pouť	pouť	k1gFnSc4	pouť
k	k	k7c3	k
Jupiteru	Jupiter	k1gInSc3	Jupiter
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
k	k	k7c3	k
Plutu	plut	k1gInSc3	plut
<g/>
.	.	kIx.	.
</s>
<s>
Stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
zatím	zatím	k6eAd1	zatím
nejrychleji	rychle	k6eAd3	rychle
se	se	k3xPyFc4	se
pohybujícím	pohybující	k2eAgNnSc7d1	pohybující
umělým	umělý	k2eAgNnSc7d1	umělé
kosmickým	kosmický	k2eAgNnSc7d1	kosmické
tělesem	těleso	k1gNnSc7	těleso
v	v	k7c6	v
zemském	zemský	k2eAgNnSc6d1	zemské
gravitačním	gravitační	k2eAgNnSc6d1	gravitační
poli	pole	k1gNnSc6	pole
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
UT	UT	kA	UT
dne	den	k1gInSc2	den
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
sonda	sonda	k1gFnSc1	sonda
překročila	překročit	k5eAaPmAgFnS	překročit
dráhu	dráha	k1gFnSc4	dráha
Měsíce	měsíc	k1gInSc2	měsíc
a	a	k8xC	a
78	[number]	k4	78
dní	den	k1gInPc2	den
po	po	k7c6	po
startu	start	k1gInSc6	start
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
<g/>
)	)	kIx)	)
překročila	překročit	k5eAaPmAgFnS	překročit
dráhu	dráha	k1gFnSc4	dráha
Marsu	Mars	k1gInSc2	Mars
<g/>
.	.	kIx.	.
</s>
<s>
Průzkum	průzkum	k1gInSc1	průzkum
objektů	objekt	k1gInPc2	objekt
v	v	k7c6	v
pásu	pás	k1gInSc6	pás
planetek	planetka	k1gFnPc2	planetka
mezi	mezi	k7c7	mezi
drahou	draha	k1gFnSc7	draha
Marsu	Mars	k1gInSc2	Mars
a	a	k8xC	a
Jupiteru	Jupiter	k1gInSc2	Jupiter
nebyl	být	k5eNaImAgInS	být
plánován	plánovat	k5eAaImNgInS	plánovat
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
úspory	úspora	k1gFnSc2	úspora
pohonných	pohonný	k2eAgFnPc2d1	pohonná
hmot	hmota	k1gFnPc2	hmota
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
daly	dát	k5eAaPmAgInP	dát
později	pozdě	k6eAd2	pozdě
lépe	dobře	k6eAd2	dobře
využít	využít	k5eAaPmF	využít
pro	pro	k7c4	pro
průzkum	průzkum	k1gInSc4	průzkum
těles	těleso	k1gNnPc2	těleso
Kuiperova	Kuiperův	k2eAgInSc2d1	Kuiperův
pásu	pás	k1gInSc2	pás
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
po	po	k7c6	po
startu	start	k1gInSc6	start
tým	tým	k1gInSc1	tým
sondy	sonda	k1gFnSc2	sonda
zkoumal	zkoumat	k5eAaImAgInS	zkoumat
dráhu	dráha	k1gFnSc4	dráha
letu	let	k1gInSc2	let
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
některý	některý	k3yIgInSc1	některý
asteroid	asteroid	k1gInSc1	asteroid
nebude	být	k5eNaImBp3nS	být
nacházet	nacházet	k5eAaImF	nacházet
v	v	k7c6	v
dosahu	dosah	k1gInSc6	dosah
přístrojů	přístroj	k1gInPc2	přístroj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2006	[number]	k4	2006
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
sonda	sonda	k1gFnSc1	sonda
13	[number]	k4	13
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
přiblíží	přiblížit	k5eAaPmIp3nS	přiblížit
k	k	k7c3	k
malé	malý	k2eAgFnSc3d1	malá
planetce	planetka	k1gFnSc3	planetka
(	(	kIx(	(
<g/>
132524	[number]	k4	132524
<g/>
)	)	kIx)	)
APL	APL	kA	APL
(	(	kIx(	(
<g/>
provizorní	provizorní	k2eAgNnSc1d1	provizorní
označení	označení	k1gNnSc1	označení
2002	[number]	k4	2002
JF	JF	kA	JF
<g/>
56	[number]	k4	56
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgNnSc1d3	veliký
přiblížení	přiblížení	k1gNnSc1	přiblížení
nastalo	nastat	k5eAaPmAgNnS	nastat
ve	v	k7c6	v
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
UT	UT	kA	UT
na	na	k7c4	na
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
101	[number]	k4	101
867	[number]	k4	867
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Asteroid	asteroid	k1gInSc1	asteroid
byl	být	k5eAaImAgInS	být
snímkován	snímkovat	k5eAaImNgInS	snímkovat
kamerou	kamera	k1gFnSc7	kamera
Ralph	Ralpha	k1gFnPc2	Ralpha
(	(	kIx(	(
<g/>
použití	použití	k1gNnSc1	použití
LORRI	LORRI	kA	LORRI
nebylo	být	k5eNaImAgNnS	být
v	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
chvíli	chvíle	k1gFnSc4	chvíle
možné	možný	k2eAgNnSc1d1	možné
kvůli	kvůli	k7c3	kvůli
blízkosti	blízkost	k1gFnSc3	blízkost
Slunce	slunce	k1gNnSc2	slunce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
řídící	řídící	k2eAgInSc1d1	řídící
tým	tým	k1gInSc1	tým
ověřil	ověřit	k5eAaPmAgInS	ověřit
funkčnost	funkčnost	k1gFnSc4	funkčnost
kamery	kamera	k1gFnSc2	kamera
a	a	k8xC	a
schopnost	schopnost	k1gFnSc4	schopnost
sondy	sonda	k1gFnSc2	sonda
sledovat	sledovat	k5eAaImF	sledovat
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
pohybující	pohybující	k2eAgInSc4d1	pohybující
cíl	cíl	k1gInSc4	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Průměr	průměr	k1gInSc1	průměr
asteroidu	asteroid	k1gInSc2	asteroid
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
2,5	[number]	k4	2,5
km	km	kA	km
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2007	[number]	k4	2007
sonda	sonda	k1gFnSc1	sonda
prolétla	prolétnout	k5eAaPmAgFnS	prolétnout
kolem	kolem	k7c2	kolem
Jupitera	Jupiter	k1gMnSc2	Jupiter
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
2,3	[number]	k4	2,3
miliónu	milión	k4xCgInSc2	milión
km	km	kA	km
rychlostí	rychlost	k1gFnSc7	rychlost
21	[number]	k4	21
km	km	kA	km
<g/>
.	.	kIx.	.
<g/>
s-	s-	k?	s-
<g/>
1	[number]	k4	1
(	(	kIx(	(
<g/>
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
Jupiteru	Jupiter	k1gInSc3	Jupiter
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
průletu	průlet	k1gInSc6	průlet
pořídila	pořídit	k5eAaPmAgFnS	pořídit
pomocí	pomocí	k7c2	pomocí
kamery	kamera	k1gFnSc2	kamera
(	(	kIx(	(
<g/>
LORRI	LORRI	kA	LORRI
<g/>
)	)	kIx)	)
snímky	snímek	k1gInPc4	snímek
jeho	on	k3xPp3gInSc2	on
povrchu	povrch	k1gInSc2	povrch
a	a	k8xC	a
snímky	snímek	k1gInPc1	snímek
některých	některý	k3yIgInPc2	některý
jeho	jeho	k3xOp3gInPc2	jeho
měsíců	měsíc	k1gInPc2	měsíc
–	–	k?	–
Io	Io	k1gFnPc2	Io
<g/>
,	,	kIx,	,
Europy	Europa	k1gFnSc2	Europa
a	a	k8xC	a
Ganymedu	Ganymed	k1gMnSc3	Ganymed
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
snímcích	snímek	k1gInPc6	snímek
měsíce	měsíc	k1gInSc2	měsíc
Io	Io	k1gMnSc2	Io
<g/>
,	,	kIx,	,
pořízených	pořízený	k2eAgNnPc2d1	pořízené
26	[number]	k4	26
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
zachytila	zachytit	k5eAaPmAgFnS	zachytit
sonda	sonda	k1gFnSc1	sonda
erupci	erupce	k1gFnSc3	erupce
vulkánu	vulkán	k1gInSc2	vulkán
Tvashtar	Tvashtar	k1gInSc1	Tvashtar
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
prachový	prachový	k2eAgInSc1d1	prachový
sloupec	sloupec	k1gInSc1	sloupec
dosahoval	dosahovat	k5eAaImAgInS	dosahovat
přibližně	přibližně	k6eAd1	přibližně
240	[number]	k4	240
km	km	kA	km
nad	nad	k7c4	nad
povrch	povrch	k1gInSc4	povrch
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
průletu	průlet	k1gInSc6	průlet
okolo	okolo	k7c2	okolo
Jupitera	Jupiter	k1gMnSc2	Jupiter
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
efektu	efekt	k1gInSc2	efekt
gravitačního	gravitační	k2eAgInSc2d1	gravitační
praku	prak	k1gInSc2	prak
k	k	k7c3	k
urychlení	urychlení	k1gNnSc3	urychlení
sondy	sonda	k1gFnSc2	sonda
o	o	k7c4	o
další	další	k2eAgFnSc4d1	další
4	[number]	k4	4
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
přeletu	přelet	k1gInSc2	přelet
mezi	mezi	k7c7	mezi
Jupiterem	Jupiter	k1gMnSc7	Jupiter
a	a	k8xC	a
Plutem	Pluto	k1gMnSc7	Pluto
byla	být	k5eAaImAgFnS	být
většina	většina	k1gFnSc1	většina
systémů	systém	k1gInPc2	systém
sondy	sonda	k1gFnSc2	sonda
a	a	k8xC	a
veškeré	veškerý	k3xTgInPc1	veškerý
vědecké	vědecký	k2eAgInPc1d1	vědecký
přístroje	přístroj	k1gInPc1	přístroj
hibernovány	hibernován	k2eAgInPc1d1	hibernován
a	a	k8xC	a
byly	být	k5eAaImAgInP	být
zapojovány	zapojovat	k5eAaImNgInP	zapojovat
přibližně	přibližně	k6eAd1	přibližně
jednou	jeden	k4xCgFnSc7	jeden
ročně	ročně	k6eAd1	ročně
ke	k	k7c3	k
komplexnímu	komplexní	k2eAgNnSc3d1	komplexní
zjištění	zjištění	k1gNnSc3	zjištění
jejich	jejich	k3xOp3gInSc2	jejich
technického	technický	k2eAgInSc2d1	technický
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2014	[number]	k4	2014
byla	být	k5eAaImAgFnS	být
sonda	sonda	k1gFnSc1	sonda
probuzena	probudit	k5eAaPmNgFnS	probudit
z	z	k7c2	z
hibernace	hibernace	k1gFnSc2	hibernace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
začala	začít	k5eAaPmAgFnS	začít
29	[number]	k4	29
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
se	se	k3xPyFc4	se
připravovat	připravovat	k5eAaImF	připravovat
na	na	k7c4	na
plánovaný	plánovaný	k2eAgInSc4d1	plánovaný
průlet	průlet	k1gInSc4	průlet
kolem	kolem	k7c2	kolem
Pluta	Pluto	k1gNnSc2	Pluto
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k6eAd1	okolo
Pluta	plut	k2eAgFnSc1d1	Pluta
sonda	sonda	k1gFnSc1	sonda
proletěla	proletět	k5eAaPmAgFnS	proletět
14	[number]	k4	14
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Sledování	sledování	k1gNnSc1	sledování
Pluta	Pluto	k1gNnSc2	Pluto
začalo	začít	k5eAaPmAgNnS	začít
pět	pět	k4xCc1	pět
měsíců	měsíc	k1gInPc2	měsíc
před	před	k7c7	před
největším	veliký	k2eAgNnSc7d3	veliký
přiblížením	přiblížení	k1gNnSc7	přiblížení
<g/>
.	.	kIx.	.
</s>
<s>
Dálkové	dálkový	k2eAgNnSc1d1	dálkové
snímkování	snímkování	k1gNnSc1	snímkování
pořídilo	pořídit	k5eAaPmAgNnS	pořídit
globální	globální	k2eAgFnSc4d1	globální
mapu	mapa	k1gFnSc4	mapa
Pluta	Pluto	k1gMnSc2	Pluto
a	a	k8xC	a
Charona	Charon	k1gMnSc2	Charon
s	s	k7c7	s
rozlišením	rozlišení	k1gNnSc7	rozlišení
40	[number]	k4	40
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
trpasličí	trpasličí	k2eAgFnSc2d1	trpasličí
planety	planeta	k1gFnSc2	planeta
proletěla	proletět	k5eAaPmAgFnS	proletět
v	v	k7c6	v
minimální	minimální	k2eAgFnSc6d1	minimální
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
12	[number]	k4	12
500	[number]	k4	500
km	km	kA	km
14	[number]	k4	14
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2015	[number]	k4	2015
v	v	k7c4	v
11	[number]	k4	11
<g/>
:	:	kIx,	:
<g/>
49	[number]	k4	49
<g/>
:	:	kIx,	:
<g/>
57	[number]	k4	57
greenwichského	greenwichský	k2eAgInSc2d1	greenwichský
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
Charonu	Charon	k1gMnSc3	Charon
proletěla	proletět	k5eAaPmAgFnS	proletět
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
cca	cca	kA	cca
27	[number]	k4	27
000	[number]	k4	000
km	km	kA	km
rychlostí	rychlost	k1gFnPc2	rychlost
přibližně	přibližně	k6eAd1	přibližně
14	[number]	k4	14
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
o	o	k7c4	o
zhruba	zhruba	k6eAd1	zhruba
4	[number]	k4	4
minuty	minuta	k1gFnSc2	minuta
později	pozdě	k6eAd2	pozdě
než	než	k8xS	než
okolo	okolo	k7c2	okolo
Pluta	Pluto	k1gNnSc2	Pluto
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
největšího	veliký	k2eAgNnSc2d3	veliký
přiblížení	přiblížení	k1gNnSc2	přiblížení
přístroje	přístroj	k1gInSc2	přístroj
pořídily	pořídit	k5eAaPmAgInP	pořídit
snímky	snímek	k1gInPc1	snímek
s	s	k7c7	s
rozlišením	rozlišení	k1gNnSc7	rozlišení
až	až	k9	až
25	[number]	k4	25
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
pixel	pixel	k1gInSc4	pixel
<g/>
,	,	kIx,	,
čtyřbarevnou	čtyřbarevný	k2eAgFnSc4d1	čtyřbarevná
celkovou	celkový	k2eAgFnSc4d1	celková
mapu	mapa	k1gFnSc4	mapa
viditelné	viditelný	k2eAgFnSc2d1	viditelná
strany	strana	k1gFnSc2	strana
s	s	k7c7	s
rozlišením	rozlišení	k1gNnSc7	rozlišení
1,6	[number]	k4	1,6
km	km	kA	km
<g/>
,	,	kIx,	,
celkovou	celkový	k2eAgFnSc4d1	celková
infračervenou	infračervený	k2eAgFnSc4d1	infračervená
spektrální	spektrální	k2eAgFnSc4d1	spektrální
mapu	mapa	k1gFnSc4	mapa
s	s	k7c7	s
rozlišením	rozlišení	k1gNnSc7	rozlišení
7	[number]	k4	7
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
pixel	pixel	k1gInSc1	pixel
a	a	k8xC	a
pro	pro	k7c4	pro
vybrané	vybraný	k2eAgFnPc4d1	vybraná
oblasti	oblast	k1gFnPc4	oblast
až	až	k9	až
na	na	k7c4	na
méně	málo	k6eAd2	málo
než	než	k8xS	než
100	[number]	k4	100
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
pixel	pixel	k1gInSc1	pixel
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
vzdálenosti	vzdálenost	k1gFnSc3	vzdálenost
sondy	sonda	k1gFnSc2	sonda
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
a	a	k8xC	a
prakticky	prakticky	k6eAd1	prakticky
nemožnému	možný	k2eNgNnSc3d1	nemožné
operativnímu	operativní	k2eAgNnSc3d1	operativní
ovládání	ovládání	k1gNnSc3	ovládání
vlastní	vlastní	k2eAgFnSc2d1	vlastní
sondy	sonda	k1gFnSc2	sonda
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
celá	celý	k2eAgFnSc1d1	celá
průletová	průletový	k2eAgFnSc1d1	průletová
sekvence	sekvence	k1gFnSc1	sekvence
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
činností	činnost	k1gFnPc2	činnost
všech	všecek	k3xTgMnPc2	všecek
pozorovacích	pozorovací	k2eAgMnPc2d1	pozorovací
přístrojů	přístroj	k1gInPc2	přístroj
<g/>
,	,	kIx,	,
dopředu	dopředu	k6eAd1	dopředu
naprogramována	naprogramován	k2eAgFnSc1d1	naprogramována
a	a	k8xC	a
zcela	zcela	k6eAd1	zcela
autonomní	autonomní	k2eAgFnSc1d1	autonomní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
časovém	časový	k2eAgNnSc6d1	časové
okně	okno	k1gNnSc6	okno
+	+	kIx~	+
<g/>
/	/	kIx~	/
<g/>
-	-	kIx~	-
12	[number]	k4	12
h	h	k?	h
okolo	okolo	k7c2	okolo
nejtěsnějšího	těsný	k2eAgInSc2d3	nejtěsnější
průletu	průlet	k1gInSc2	průlet
navíc	navíc	k6eAd1	navíc
sonda	sonda	k1gFnSc1	sonda
nekomunikovala	komunikovat	k5eNaImAgFnS	komunikovat
se	s	k7c7	s
Zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
aktivně	aktivně	k6eAd1	aktivně
nastavovala	nastavovat	k5eAaImAgFnS	nastavovat
svou	svůj	k3xOyFgFnSc4	svůj
pozici	pozice	k1gFnSc4	pozice
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
co	co	k3yQnSc4	co
nejvhodněji	vhodně	k6eAd3	vhodně
směřovala	směřovat	k5eAaImAgFnS	směřovat
pozorovací	pozorovací	k2eAgInPc4d1	pozorovací
přístroje	přístroj	k1gInPc4	přístroj
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
objekty	objekt	k1gInPc4	objekt
systému	systém	k1gInSc2	systém
Pluta	Pluto	k1gNnSc2	Pluto
<g/>
.	.	kIx.	.
</s>
<s>
Data	datum	k1gNnPc1	datum
pořízená	pořízený	k2eAgNnPc1d1	pořízené
sondou	sonda	k1gFnSc7	sonda
jsou	být	k5eAaImIp3nP	být
vysílána	vysílat	k5eAaImNgNnP	vysílat
zpět	zpět	k6eAd1	zpět
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
rychlostí	rychlost	k1gFnPc2	rychlost
zhruba	zhruba	k6eAd1	zhruba
1	[number]	k4	1
kilobit	kilobit	k5eAaPmF	kilobit
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Aby	aby	k9	aby
signál	signál	k1gInSc1	signál
astronomové	astronom	k1gMnPc1	astronom
vůbec	vůbec	k9	vůbec
zachytili	zachytit	k5eAaPmAgMnP	zachytit
<g/>
,	,	kIx,	,
musejí	muset	k5eAaImIp3nP	muset
využívat	využívat	k5eAaPmF	využívat
přesně	přesně	k6eAd1	přesně
namířené	namířený	k2eAgInPc4d1	namířený
radioteleskopy	radioteleskop	k1gInPc4	radioteleskop
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
60	[number]	k4	60
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Kompletní	kompletní	k2eAgInSc1d1	kompletní
přenos	přenos	k1gInSc1	přenos
naměřených	naměřený	k2eAgNnPc2d1	naměřené
dat	datum	k1gNnPc2	datum
bude	být	k5eAaImBp3nS	být
díky	díky	k7c3	díky
nízké	nízký	k2eAgFnSc3d1	nízká
přenosové	přenosový	k2eAgFnSc3d1	přenosová
rychlosti	rychlost	k1gFnSc3	rychlost
trvat	trvat	k5eAaImF	trvat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
16	[number]	k4	16
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Vědecky	vědecky	k6eAd1	vědecky
nejcennější	cenný	k2eAgNnSc1d3	nejcennější
data	datum	k1gNnPc1	datum
jsou	být	k5eAaImIp3nP	být
navíc	navíc	k6eAd1	navíc
vysílána	vysílat	k5eAaImNgNnP	vysílat
prioritně	prioritně	k6eAd1	prioritně
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
snížila	snížit	k5eAaPmAgFnS	snížit
případná	případný	k2eAgFnSc1d1	případná
ztráta	ztráta	k1gFnSc1	ztráta
vědeckého	vědecký	k2eAgInSc2d1	vědecký
materiálu	materiál	k1gInSc2	materiál
v	v	k7c6	v
případě	případ	k1gInSc6	případ
neočekávané	očekávaný	k2eNgFnSc2d1	neočekávaná
poruchy	porucha	k1gFnSc2	porucha
sondy	sonda	k1gFnSc2	sonda
<g/>
.	.	kIx.	.
</s>
<s>
Expedice	expedice	k1gFnSc1	expedice
pak	pak	k6eAd1	pak
má	mít	k5eAaImIp3nS	mít
pokračovat	pokračovat	k5eAaImF	pokračovat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2020	[number]	k4	2020
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
bude	být	k5eAaImBp3nS	být
prodlouženo	prodloužen	k2eAgNnSc1d1	prodlouženo
financování	financování	k1gNnSc1	financování
mise	mise	k1gFnSc2	mise
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
pohybovat	pohybovat	k5eAaImF	pohybovat
oblastí	oblast	k1gFnSc7	oblast
Kuiperova	Kuiperův	k2eAgInSc2d1	Kuiperův
pásu	pás	k1gInSc2	pás
k	k	k7c3	k
objektům	objekt	k1gInPc3	objekt
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
září	září	k1gNnSc6	září
2014	[number]	k4	2014
vybrány	vybrán	k2eAgInPc1d1	vybrán
jako	jako	k8xS	jako
další	další	k2eAgInPc1d1	další
potenciální	potenciální	k2eAgInPc1d1	potenciální
cíle	cíl	k1gInPc1	cíl
mise	mise	k1gFnSc2	mise
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tělesům	těleso	k1gNnPc3	těleso
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
km	km	kA	km
by	by	kYmCp3nP	by
sonda	sonda	k1gFnSc1	sonda
měla	mít	k5eAaImAgFnS	mít
doletět	doletět	k5eAaPmF	doletět
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2019	[number]	k4	2019
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2015	[number]	k4	2015
byl	být	k5eAaImAgInS	být
z	z	k7c2	z
kandidátů	kandidát	k1gMnPc2	kandidát
vybrán	vybrán	k2eAgInSc4d1	vybrán
objekt	objekt	k1gInSc4	objekt
2014	[number]	k4	2014
MU	MU	kA	MU
<g/>
69	[number]	k4	69
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yQgInSc3	který
by	by	kYmCp3nP	by
sonda	sonda	k1gFnSc1	sonda
měla	mít	k5eAaImAgFnS	mít
doletět	doletět	k5eAaPmF	doletět
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2019	[number]	k4	2019
<g/>
.	.	kIx.	.
</s>
<s>
Plán	plán	k1gInSc1	plán
ještě	ještě	k9	ještě
musí	muset	k5eAaImIp3nS	muset
definitivně	definitivně	k6eAd1	definitivně
schválit	schválit	k5eAaPmF	schválit
NASA	NASA	kA	NASA
<g/>
.	.	kIx.	.
</s>
<s>
Cestou	cesta	k1gFnSc7	cesta
k	k	k7c3	k
2014	[number]	k4	2014
MU69	MU69	k1gFnSc1	MU69
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
sonda	sonda	k1gFnSc1	sonda
z	z	k7c2	z
dálky	dálka	k1gFnSc2	dálka
pozorovat	pozorovat	k5eAaImF	pozorovat
asi	asi	k9	asi
tucet	tucet	k1gInSc4	tucet
dalších	další	k2eAgInPc2d1	další
objektů	objekt	k1gInPc2	objekt
Kuiperova	Kuiperův	k2eAgInSc2d1	Kuiperův
pásu	pás	k1gInSc2	pás
a	a	k8xC	a
získat	získat	k5eAaPmF	získat
základní	základní	k2eAgFnPc4d1	základní
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
jejich	jejich	k3xOp3gInSc6	jejich
vzhledu	vzhled	k1gInSc6	vzhled
a	a	k8xC	a
povrchu	povrch	k1gInSc6	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Sonda	sonda	k1gFnSc1	sonda
nese	nést	k5eAaImIp3nS	nést
kompaktní	kompaktní	k2eAgInSc4d1	kompaktní
disk	disk	k1gInSc4	disk
se	s	k7c7	s
430	[number]	k4	430
000	[number]	k4	000
jmény	jméno	k1gNnPc7	jméno
zájemců	zájemce	k1gMnPc2	zájemce
<g/>
,	,	kIx,	,
kousek	kousek	k1gInSc4	kousek
lodi	loď	k1gFnSc2	loď
SpaceShipOne	SpaceShipOn	k1gInSc5	SpaceShipOn
<g/>
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
a	a	k8xC	a
vedle	vedle	k7c2	vedle
dalších	další	k2eAgInPc2d1	další
předmětů	předmět	k1gInPc2	předmět
i	i	k8xC	i
vlajku	vlajka	k1gFnSc4	vlajka
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Vedoucí	vedoucí	k1gMnSc1	vedoucí
vědců	vědec	k1gMnPc2	vědec
Alan	Alan	k1gMnSc1	Alan
Stern	sternum	k1gNnPc2	sternum
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
sonda	sonda	k1gFnSc1	sonda
nese	nést	k5eAaImIp3nS	nést
i	i	k9	i
ampuli	ampule	k1gFnSc4	ampule
s	s	k7c7	s
popelem	popel	k1gInSc7	popel
objevitele	objevitel	k1gMnSc2	objevitel
Pluta	Pluto	k1gMnSc2	Pluto
-	-	kIx~	-
Clyda	Clyda	k1gMnSc1	Clyda
Tombaugha	Tombaugha	k1gMnSc1	Tombaugha
<g/>
.	.	kIx.	.
</s>
<s>
Snímkováním	snímkování	k1gNnSc7	snímkování
a	a	k8xC	a
průzkumem	průzkum	k1gInSc7	průzkum
Pluta	Pluto	k1gNnSc2	Pluto
<g/>
,	,	kIx,	,
sonda	sonda	k1gFnSc1	sonda
provedla	provést	k5eAaPmAgFnS	provést
historicky	historicky	k6eAd1	historicky
nejvzdálenější	vzdálený	k2eAgInSc4d3	nejvzdálenější
průzkum	průzkum	k1gInSc4	průzkum
hmotného	hmotný	k2eAgNnSc2d1	hmotné
tělesa	těleso	k1gNnSc2	těleso
-	-	kIx~	-
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
cca	cca	kA	cca
4,9	[number]	k4	4,9
miliardy	miliarda	k4xCgFnSc2	miliarda
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
vzdálenosti	vzdálenost	k1gFnSc3	vzdálenost
sondy	sonda	k1gFnSc2	sonda
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
bylo	být	k5eAaImAgNnS	být
jednosměrné	jednosměrný	k2eAgNnSc1d1	jednosměrné
zpoždění	zpoždění	k1gNnSc1	zpoždění
radiové	radiový	k2eAgFnSc2d1	radiová
komunikace	komunikace	k1gFnSc2	komunikace
cca	cca	kA	cca
4,5	[number]	k4	4,5
hodiny	hodina	k1gFnSc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
velkou	velký	k2eAgFnSc4d1	velká
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
od	od	k7c2	od
slunce	slunce	k1gNnSc2	slunce
není	být	k5eNaImIp3nS	být
sonda	sonda	k1gFnSc1	sonda
napájena	napájet	k5eAaImNgFnS	napájet
solárními	solární	k2eAgInPc7d1	solární
panely	panel	k1gInPc7	panel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
radioizotopovým	radioizotopový	k2eAgInSc7d1	radioizotopový
generátorem	generátor	k1gInSc7	generátor
<g/>
,	,	kIx,	,
záložním	záložní	k2eAgInSc7d1	záložní
kusem	kus	k1gInSc7	kus
z	z	k7c2	z
mise	mise	k1gFnSc2	mise
Cassini	Cassin	k2eAgMnPc1d1	Cassin
<g/>
.	.	kIx.	.
</s>
<s>
Sonda	sonda	k1gFnSc1	sonda
je	být	k5eAaImIp3nS	být
tělesem	těleso	k1gNnSc7	těleso
<g/>
,	,	kIx,	,
kterému	který	k3yIgMnSc3	který
byla	být	k5eAaImAgFnS	být
při	pře	k1gFnSc4	pře
vypuštění	vypuštění	k1gNnSc2	vypuštění
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
udělena	udělen	k2eAgFnSc1d1	udělena
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
úniková	únikový	k2eAgFnSc1d1	úniková
rychlost	rychlost	k1gFnSc1	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
Těsně	těsně	k6eAd1	těsně
po	po	k7c6	po
vypuštění	vypuštění	k1gNnSc6	vypuštění
byla	být	k5eAaImAgFnS	být
její	její	k3xOp3gFnSc4	její
relativní	relativní	k2eAgFnSc4d1	relativní
rychlost	rychlost	k1gFnSc4	rychlost
vůči	vůči	k7c3	vůči
Zemi	zem	k1gFnSc3	zem
16,21	[number]	k4	16,21
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
tedy	tedy	k9	tedy
58	[number]	k4	58
350	[number]	k4	350
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
Sonda	sonda	k1gFnSc1	sonda
New	New	k1gFnSc2	New
Horizons	Horizonsa	k1gFnPc2	Horizonsa
byla	být	k5eAaImAgFnS	být
vypuštěna	vypustit	k5eAaPmNgFnS	vypustit
jen	jen	k9	jen
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
než	než	k8xS	než
astronomové	astronom	k1gMnPc1	astronom
na	na	k7c6	na
kongresu	kongres	k1gInSc6	kongres
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
reklasifikovali	reklasifikovat	k5eAaBmAgMnP	reklasifikovat
Pluto	Pluto	k1gNnSc4	Pluto
jako	jako	k8xC	jako
trpasličí	trpasličí	k2eAgFnSc4d1	trpasličí
planetu	planeta	k1gFnSc4	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
členové	člen	k1gMnPc1	člen
týmu	tým	k1gInSc2	tým
New	New	k1gFnSc2	New
Horizons	Horizonsa	k1gFnPc2	Horizonsa
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
Alan	alan	k1gInSc1	alan
Stern	sternum	k1gNnPc2	sternum
<g/>
,	,	kIx,	,
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
nesouhlasí	souhlasit	k5eNaImIp3nP	souhlasit
a	a	k8xC	a
považují	považovat	k5eAaImIp3nP	považovat
Pluto	Pluto	k1gNnSc4	Pluto
dál	daleko	k6eAd2	daleko
za	za	k7c4	za
planetu	planeta	k1gFnSc4	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Nově	nově	k6eAd1	nově
objevené	objevený	k2eAgInPc1d1	objevený
měsíce	měsíc	k1gInPc1	měsíc
Pluta	Pluto	k1gNnSc2	Pluto
<g/>
,	,	kIx,	,
Nix	Nix	k1gFnSc1	Nix
a	a	k8xC	a
Hydra	hydra	k1gFnSc1	hydra
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
iniciály	iniciála	k1gFnPc1	iniciála
N	N	kA	N
a	a	k8xC	a
H	H	kA	H
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
mise	mise	k1gFnSc2	mise
New	New	k1gFnSc2	New
Horizons	Horizonsa	k1gFnPc2	Horizonsa
<g/>
.	.	kIx.	.
</s>
