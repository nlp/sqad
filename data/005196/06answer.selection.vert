<s>
New	New	k?	New
Horizons	Horizons	k1gInSc1	Horizons
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Nové	Nové	k2eAgInPc1d1	Nové
obzory	obzor	k1gInPc1	obzor
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americká	americký	k2eAgFnSc1d1	americká
planetární	planetární	k2eAgFnSc1d1	planetární
sonda	sonda	k1gFnSc1	sonda
<g/>
,	,	kIx,	,
určená	určený	k2eAgFnSc1d1	určená
k	k	k7c3	k
průzkumu	průzkum	k1gInSc3	průzkum
trpasličí	trpasličí	k2eAgFnSc2d1	trpasličí
planety	planeta	k1gFnSc2	planeta
Pluto	Pluto	k1gMnSc1	Pluto
<g/>
,	,	kIx,	,
jejích	její	k3xOp3gInPc2	její
měsíců	měsíc	k1gInPc2	měsíc
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
transneptunických	transneptunický	k2eAgNnPc2d1	transneptunické
těles	těleso	k1gNnPc2	těleso
(	(	kIx(	(
<g/>
TNO	TNO	kA	TNO
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
