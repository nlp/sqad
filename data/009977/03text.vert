<p>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Hyginus	Hyginus	k1gMnSc1	Hyginus
byl	být	k5eAaImAgMnS	být
devátým	devátý	k4xOgMnSc7	devátý
papežem	papež	k1gMnSc7	papež
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
pontifikát	pontifikát	k1gInSc1	pontifikát
se	se	k3xPyFc4	se
datuje	datovat	k5eAaImIp3nS	datovat
do	do	k7c2	do
let	léto	k1gNnPc2	léto
136	[number]	k4	136
<g/>
/	/	kIx~	/
<g/>
138	[number]	k4	138
–	–	k?	–
140	[number]	k4	140
<g/>
/	/	kIx~	/
<g/>
142	[number]	k4	142
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Hyginus	Hyginus	k1gMnSc1	Hyginus
byl	být	k5eAaImAgMnS	být
papežem	papež	k1gMnSc7	papež
pouhé	pouhý	k2eAgFnSc2d1	pouhá
dva	dva	k4xCgInPc4	dva
až	až	k9	až
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
nejpravděpodobněji	pravděpodobně	k6eAd3	pravděpodobně
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
138	[number]	k4	138
až	až	k9	až
140	[number]	k4	140
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
císaře	císař	k1gMnSc2	císař
Antonina	Antonin	k2eAgMnSc2d1	Antonin
Pia	Pius	k1gMnSc2	Pius
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
úřad	úřad	k1gInSc1	úřad
římského	římský	k2eAgInSc2d1	římský
biskupa	biskup	k1gInSc2	biskup
byl	být	k5eAaImAgMnS	být
před	před	k7c7	před
jeho	jeho	k3xOp3gNnSc7	jeho
zvolením	zvolení	k1gNnSc7	zvolení
zhruba	zhruba	k6eAd1	zhruba
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
(	(	kIx(	(
<g/>
136	[number]	k4	136
–	–	k?	–
138	[number]	k4	138
<g/>
)	)	kIx)	)
neobsazen	obsazen	k2eNgInSc1d1	neobsazen
<g/>
,	,	kIx,	,
doklady	doklad	k1gInPc1	doklad
jsou	být	k5eAaImIp3nP	být
ovšem	ovšem	k9	ovšem
nejisté	jistý	k2eNgInPc1d1	nejistý
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Athénách	Athéna	k1gFnPc6	Athéna
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
předchozím	předchozí	k2eAgInSc6d1	předchozí
životě	život	k1gInSc6	život
není	být	k5eNaImIp3nS	být
nic	nic	k3yNnSc1	nic
známo	znám	k2eAgNnSc1d1	známo
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
prameny	pramen	k1gInPc1	pramen
uvádějí	uvádět	k5eAaImIp3nP	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
filosofem	filosof	k1gMnSc7	filosof
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jde	jít	k5eAaImIp3nS	jít
patrně	patrně	k6eAd1	patrně
o	o	k7c4	o
záměnu	záměna	k1gFnSc4	záměna
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
tradice	tradice	k1gFnSc2	tradice
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
zpřesnil	zpřesnit	k5eAaPmAgInS	zpřesnit
hierarchii	hierarchie	k1gFnSc4	hierarchie
duchovenstva	duchovenstvo	k1gNnSc2	duchovenstvo
a	a	k8xC	a
organizace	organizace	k1gFnSc2	organizace
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
obce	obec	k1gFnSc2	obec
</s>
</p>
<p>
<s>
ustanovil	ustanovit	k5eAaPmAgMnS	ustanovit
při	při	k7c6	při
křtu	křest	k1gInSc6	křest
přítomnost	přítomnost	k1gFnSc1	přítomnost
kmotra	kmotra	k1gFnSc1	kmotra
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
dbát	dbát	k5eAaImF	dbát
o	o	k7c4	o
řádnou	řádný	k2eAgFnSc4d1	řádná
křesťanskou	křesťanský	k2eAgFnSc4d1	křesťanská
výchovu	výchova	k1gFnSc4	výchova
dítěte	dítě	k1gNnSc2	dítě
</s>
</p>
<p>
<s>
nařídil	nařídit	k5eAaPmAgMnS	nařídit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
všechny	všechen	k3xTgInPc1	všechen
kostely	kostel	k1gInPc1	kostel
byly	být	k5eAaImAgInP	být
řádně	řádně	k6eAd1	řádně
vysvěcenyÚdajně	vysvěcenyÚdajně	k6eAd1	vysvěcenyÚdajně
zemřel	zemřít	k5eAaPmAgMnS	zemřít
mučednickou	mučednický	k2eAgFnSc7d1	mučednická
smrtí	smrt	k1gFnSc7	smrt
za	za	k7c4	za
pronásledování	pronásledování	k1gNnSc4	pronásledování
křesťanů	křesťan	k1gMnPc2	křesťan
po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
Marka	Marek	k1gMnSc2	Marek
Aurelia	Aurelius	k1gMnSc2	Aurelius
<g/>
.	.	kIx.	.
</s>
<s>
Žádné	žádný	k3yNgInPc1	žádný
záznamy	záznam	k1gInPc1	záznam
to	ten	k3xDgNnSc4	ten
však	však	k9	však
nepotvrzují	potvrzovat	k5eNaImIp3nP	potvrzovat
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
Marcus	Marcus	k1gMnSc1	Marcus
Aurelius	Aurelius	k1gMnSc1	Aurelius
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
římským	římský	k2eAgMnSc7d1	římský
císařem	císař	k1gMnSc7	císař
až	až	k8xS	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
161	[number]	k4	161
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
památka	památka	k1gFnSc1	památka
se	se	k3xPyFc4	se
uctívá	uctívat	k5eAaImIp3nS	uctívat
11	[number]	k4	11
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
GELMI	GELMI	kA	GELMI
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Papežové	Papež	k1gMnPc1	Papež
:	:	kIx,	:
Od	od	k7c2	od
svatého	svatý	k2eAgMnSc2d1	svatý
Petra	Petr	k1gMnSc2	Petr
po	po	k7c4	po
Jana	Jan	k1gMnSc4	Jan
Pavla	Pavel	k1gMnSc2	Pavel
II	II	kA	II
<g/>
..	..	k?	..
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
328	[number]	k4	328
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
204	[number]	k4	204
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
457	[number]	k4	457
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MAXWELL-STUART	MAXWELL-STUART	k?	MAXWELL-STUART
<g/>
,	,	kIx,	,
P.	P.	kA	P.
<g/>
G.	G.	kA	G.
Papežové	Papež	k1gMnPc1	Papež
<g/>
,	,	kIx,	,
život	život	k1gInSc1	život
a	a	k8xC	a
vláda	vláda	k1gFnSc1	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
sv.	sv.	kA	sv.
Petra	Petr	k1gMnSc2	Petr
k	k	k7c3	k
Janu	Jan	k1gMnSc3	Jan
Pavlu	Pavel	k1gMnSc3	Pavel
II	II	kA	II
<g/>
..	..	k?	..
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Svoboda	Svoboda	k1gMnSc1	Svoboda
(	(	kIx(	(
<g/>
servis	servis	k1gInSc1	servis
<g/>
)	)	kIx)	)
240	[number]	k4	240
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
902300	[number]	k4	902300
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
RENDINA	RENDINA	kA	RENDINA
<g/>
,	,	kIx,	,
Claudio	Claudio	k6eAd1	Claudio
<g/>
.	.	kIx.	.
</s>
<s>
Příběhy	příběh	k1gInPc1	příběh
papežů	papež	k1gMnPc2	papež
:	:	kIx,	:
dějiny	dějiny	k1gFnPc1	dějiny
a	a	k8xC	a
tajemství	tajemství	k1gNnSc1	tajemství
:	:	kIx,	:
životopisy	životopis	k1gInPc4	životopis
265	[number]	k4	265
římských	římský	k2eAgMnPc2d1	římský
papežů	papež	k1gMnPc2	papež
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Volvox	Volvox	k1gInSc1	Volvox
Globator	Globator	k1gInSc1	Globator
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
714	[number]	k4	714
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7207	[number]	k4	7207
<g/>
-	-	kIx~	-
<g/>
574	[number]	k4	574
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VONDRUŠKA	Vondruška	k1gMnSc1	Vondruška
<g/>
,	,	kIx,	,
Isidor	Isidor	k1gMnSc1	Isidor
<g/>
.	.	kIx.	.
</s>
<s>
Životopisy	životopis	k1gInPc1	životopis
svatých	svatá	k1gFnPc2	svatá
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
dějin	dějiny	k1gFnPc2	dějiny
církevních	církevní	k2eAgFnPc2d1	církevní
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ladislav	Ladislav	k1gMnSc1	Ladislav
Kuncíř	Kuncíř	k1gMnSc1	Kuncíř
<g/>
,	,	kIx,	,
1930	[number]	k4	1930
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Hyginus	Hyginus	k1gInSc1	Hyginus
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Křesťanská	křesťanský	k2eAgFnSc1d1	křesťanská
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
</s>
</p>
