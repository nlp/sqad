<s>
Skřivánci	Skřivánek	k1gMnPc1	Skřivánek
na	na	k7c6	na
niti	nit	k1gFnSc6	nit
je	být	k5eAaImIp3nS	být
český	český	k2eAgInSc1d1	český
hořce	hořko	k6eAd1	hořko
poetický	poetický	k2eAgInSc1d1	poetický
film	film	k1gInSc1	film
režiséra	režisér	k1gMnSc2	režisér
Jiřího	Jiří	k1gMnSc2	Jiří
Menzela	Menzela	k1gMnSc2	Menzela
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
natočený	natočený	k2eAgInSc1d1	natočený
podle	podle	k7c2	podle
knihy	kniha	k1gFnSc2	kniha
Bohumila	Bohumila	k1gFnSc1	Bohumila
Hrabala	hrabat	k5eAaImAgFnS	hrabat
Inzerát	inzerát	k1gInSc4	inzerát
na	na	k7c4	na
dům	dům	k1gInSc4	dům
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
už	už	k6eAd1	už
nechci	chtít	k5eNaImIp1nS	chtít
bydlet	bydlet	k5eAaImF	bydlet
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
partě	parta	k1gFnSc6	parta
dělníků	dělník	k1gMnPc2	dělník
a	a	k8xC	a
skupině	skupina	k1gFnSc3	skupina
vězeňkyň	vězeňkyně	k1gFnPc2	vězeňkyně
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
kopečkářek	kopečkářka	k1gFnPc2	kopečkářka
<g/>
)	)	kIx)	)
pracujících	pracující	k1gMnPc2	pracující
v	v	k7c6	v
dusné	dusný	k2eAgFnSc6d1	dusná
atmosféře	atmosféra	k1gFnSc6	atmosféra
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
na	na	k7c6	na
kladenském	kladenský	k2eAgNnSc6d1	kladenské
šrotišti	šrotiště	k1gNnSc6	šrotiště
u	u	k7c2	u
zdejších	zdejší	k2eAgFnPc2d1	zdejší
železáren	železárna	k1gFnPc2	železárna
a	a	k8xC	a
oceláren	ocelárna	k1gFnPc2	ocelárna
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
má	mít	k5eAaImIp3nS	mít
nějaký	nějaký	k3yIgInSc4	nějaký
svůj	svůj	k3xOyFgInSc4	svůj
životní	životní	k2eAgInSc4d1	životní
příběh	příběh	k1gInSc4	příběh
<g/>
,	,	kIx,	,
během	během	k7c2	během
něhož	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
nedobrovolně	dobrovolně	k6eNd1	dobrovolně
dostal	dostat	k5eAaPmAgMnS	dostat
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
práci	práce	k1gFnSc3	práce
(	(	kIx(	(
<g/>
bývalý	bývalý	k2eAgMnSc1d1	bývalý
živnostník	živnostník	k1gMnSc1	živnostník
"	"	kIx"	"
<g/>
Mlíkař	Mlíkař	k?	Mlíkař
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
prokurátor	prokurátor	k1gMnSc1	prokurátor
<g/>
,	,	kIx,	,
saxofonista	saxofonista	k1gMnSc1	saxofonista
<g/>
,	,	kIx,	,
vězeňkyně	vězeňkyně	k1gFnSc1	vězeňkyně
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
<g/>
,	,	kIx,	,
kuchař	kuchař	k1gMnSc1	kuchař
ap.	ap.	kA	ap.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc4d1	hlavní
dějovou	dějový	k2eAgFnSc4d1	dějová
osu	osa	k1gFnSc4	osa
tvoří	tvořit	k5eAaImIp3nS	tvořit
milostný	milostný	k2eAgInSc1d1	milostný
příběh	příběh	k1gInSc1	příběh
mladé	mladý	k2eAgFnSc2d1	mladá
vězeňkyně	vězeňkyně	k1gFnSc2	vězeňkyně
a	a	k8xC	a
mladého	mladý	k2eAgMnSc2d1	mladý
dělníka	dělník	k1gMnSc2	dělník
(	(	kIx(	(
<g/>
Václav	Václav	k1gMnSc1	Václav
Neckář	Neckář	k1gMnSc1	Neckář
a	a	k8xC	a
Jitka	Jitka	k1gFnSc1	Jitka
Zelenohorská	zelenohorský	k2eAgFnSc1d1	Zelenohorská
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
byl	být	k5eAaImAgInS	být
vyroben	vyrobit	k5eAaPmNgInS	vyrobit
v	v	k7c6	v
ČSSR	ČSSR	kA	ČSSR
<g/>
,	,	kIx,	,
Filmové	filmový	k2eAgNnSc1d1	filmové
studio	studio	k1gNnSc1	studio
Barrandov	Barrandov	k1gInSc1	Barrandov
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
a	a	k8xC	a
ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
neveřejné	veřejný	k2eNgFnSc6d1	neveřejná
premiéře	premiéra	k1gFnSc6	premiéra
byl	být	k5eAaImAgMnS	být
pro	pro	k7c4	pro
svoji	svůj	k3xOyFgFnSc4	svůj
otevřenou	otevřený	k2eAgFnSc4d1	otevřená
kritiku	kritika	k1gFnSc4	kritika
tehdejšího	tehdejší	k2eAgInSc2d1	tehdejší
vládnoucího	vládnoucí	k2eAgInSc2d1	vládnoucí
totalitního	totalitní	k2eAgInSc2d1	totalitní
režimu	režim	k1gInSc2	režim
na	na	k7c4	na
více	hodně	k6eAd2	hodně
než	než	k8xS	než
20	[number]	k4	20
let	léto	k1gNnPc2	léto
zakázán	zakázat	k5eAaPmNgInS	zakázat
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
faktická	faktický	k2eAgFnSc1d1	faktická
veřejná	veřejný	k2eAgFnSc1d1	veřejná
premiéra	premiéra	k1gFnSc1	premiéra
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
teprve	teprve	k6eAd1	teprve
až	až	k9	až
po	po	k7c6	po
společenských	společenský	k2eAgFnPc6d1	společenská
změnách	změna	k1gFnPc6	změna
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
nastaly	nastat	k5eAaPmAgFnP	nastat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
získal	získat	k5eAaPmAgInS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
významné	významný	k2eAgNnSc4d1	významné
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
ocenění	ocenění	k1gNnSc4	ocenění
Zlatý	zlatý	k2eAgMnSc1d1	zlatý
medvěd	medvěd	k1gMnSc1	medvěd
na	na	k7c6	na
Mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
filmovém	filmový	k2eAgInSc6d1	filmový
festivalu	festival	k1gInSc6	festival
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
Skřivánci	Skřivánek	k1gMnPc1	Skřivánek
na	na	k7c6	na
niti	nit	k1gFnSc6	nit
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Skřivánci	Skřivánek	k1gMnPc1	Skřivánek
na	na	k7c6	na
niti	nit	k1gFnSc6	nit
na	na	k7c6	na
Českém	český	k2eAgInSc6d1	český
filmovém	filmový	k2eAgInSc6d1	filmový
nebi	nebe	k1gNnSc6	nebe
</s>
