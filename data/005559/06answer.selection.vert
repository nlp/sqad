<s>
Konzul	konzul	k1gMnSc1	konzul
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
cos	cos	kA	cos
<g/>
.	.	kIx.	.
<g/>
;	;	kIx,	;
latinsky	latinsky	k6eAd1	latinsky
consul	consul	k1gInSc1	consul
<g/>
,	,	kIx,	,
pl.	pl.	k?	pl.
consules	consules	k1gInSc1	consules
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
z	z	k7c2	z
consulere	consuler	k1gInSc5	consuler
<g/>
;	;	kIx,	;
česky	česky	k6eAd1	česky
radit	radit	k5eAaImF	radit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
rozvažovat	rozvažovat	k5eAaImF	rozvažovat
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
civilní	civilní	k2eAgMnSc1d1	civilní
a	a	k8xC	a
vojenský	vojenský	k2eAgMnSc1d1	vojenský
úředník	úředník	k1gMnSc1	úředník
(	(	kIx(	(
<g/>
magistratus	magistratus	k1gMnSc1	magistratus
<g/>
)	)	kIx)	)
ve	v	k7c6	v
starověkém	starověký	k2eAgInSc6d1	starověký
Římě	Řím	k1gInSc6	Řím
<g/>
.	.	kIx.	.
</s>
