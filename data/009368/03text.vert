<p>
<s>
Hrad	hrad	k1gInSc1	hrad
Frankštát	Frankštát	k1gInSc1	Frankštát
stával	stávat	k5eAaImAgInS	stávat
na	na	k7c6	na
kopci	kopec	k1gInSc6	kopec
nad	nad	k7c7	nad
obcemi	obec	k1gFnPc7	obec
Třemešek	Třemeška	k1gFnPc2	Třemeška
a	a	k8xC	a
Nový	nový	k2eAgInSc1d1	nový
Malín	Malín	k1gInSc1	Malín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
O	o	k7c6	o
hradu	hrad	k1gInSc6	hrad
existuje	existovat	k5eAaImIp3nS	existovat
velmi	velmi	k6eAd1	velmi
málo	málo	k4c4	málo
zpráv	zpráva	k1gFnPc2	zpráva
<g/>
.	.	kIx.	.
</s>
<s>
Založen	založen	k2eAgMnSc1d1	založen
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
na	na	k7c6	na
úsovském	úsovský	k2eAgNnSc6d1	úsovský
panství	panství	k1gNnSc6	panství
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
měl	mít	k5eAaImAgMnS	mít
v	v	k7c6	v
zástavě	zástava	k1gFnSc6	zástava
Jiljí	Jiljí	k1gMnSc2	Jiljí
ze	z	k7c2	z
Švábenic	Švábenice	k1gFnPc2	Švábenice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
13	[number]	k4	13
<g/>
.	.	kIx.	.
a	a	k8xC	a
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
dostavění	dostavění	k1gNnSc3	dostavění
hradu	hrad	k1gInSc2	hrad
Rabštejn	Rabštejn	k1gInSc1	Rabštejn
a	a	k8xC	a
Frankštát	Frankštát	k1gInSc1	Frankštát
byl	být	k5eAaImAgInS	být
opuštěn	opustit	k5eAaPmNgInS	opustit
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
hradě	hrad	k1gInSc6	hrad
neexistuje	existovat	k5eNaImIp3nS	existovat
žádná	žádný	k3yNgFnSc1	žádný
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
a	a	k8xC	a
nezmiňuje	zmiňovat	k5eNaImIp3nS	zmiňovat
se	se	k3xPyFc4	se
o	o	k7c6	o
něm	on	k3xPp3gMnSc6	on
téměř	téměř	k6eAd1	téměř
žádná	žádný	k3yNgFnSc1	žádný
odborná	odborný	k2eAgFnSc1d1	odborná
literatura	literatura	k1gFnSc1	literatura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Místo	místo	k7c2	místo
hradu	hrad	k1gInSc2	hrad
připomíná	připomínat	k5eAaImIp3nS	připomínat
pouze	pouze	k6eAd1	pouze
plošina	plošina	k1gFnSc1	plošina
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
stával	stávat	k5eAaImAgInS	stávat
<g/>
.	.	kIx.	.
</s>
<s>
Přístupová	přístupový	k2eAgFnSc1d1	přístupová
cesta	cesta	k1gFnSc1	cesta
vedla	vést	k5eAaImAgFnS	vést
od	od	k7c2	od
obce	obec	k1gFnSc2	obec
Třemešek	Třemeška	k1gFnPc2	Třemeška
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Frankštát	Frankštát	k1gInSc1	Frankštát
na	na	k7c6	na
webu	web	k1gInSc6	web
Hrady	hrad	k1gInPc1	hrad
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
