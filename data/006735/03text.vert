<s>
Májovci	májovec	k1gMnPc7	májovec
byla	být	k5eAaImAgFnS	být
výrazná	výrazný	k2eAgFnSc1d1	výrazná
literární	literární	k2eAgFnSc1d1	literární
skupina	skupina	k1gFnSc1	skupina
českých	český	k2eAgMnPc2d1	český
básníků	básník	k1gMnPc2	básník
a	a	k8xC	a
prozaiků	prozaik	k1gMnPc2	prozaik
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
navazovala	navazovat	k5eAaImAgFnS	navazovat
na	na	k7c4	na
odkaz	odkaz	k1gInSc4	odkaz
Karla	Karel	k1gMnSc2	Karel
Hynka	Hynek	k1gMnSc2	Hynek
Máchy	Mácha	k1gMnSc2	Mácha
<g/>
,	,	kIx,	,
Karla	Karel	k1gMnSc2	Karel
Havlíčka	Havlíček	k1gMnSc4	Havlíček
Borovského	Borovský	k1gMnSc4	Borovský
a	a	k8xC	a
Karla	Karel	k1gMnSc4	Karel
Jaromíra	Jaromír	k1gMnSc4	Jaromír
Erbena	Erben	k1gMnSc4	Erben
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
Metternichova	Metternichův	k2eAgInSc2d1	Metternichův
absolutismu	absolutismus	k1gInSc2	absolutismus
(	(	kIx(	(
<g/>
revoluční	revoluční	k2eAgInSc1d1	revoluční
rok	rok	k1gInSc1	rok
1848	[number]	k4	1848
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
na	na	k7c6	na
scéně	scéna	k1gFnSc6	scéna
objevila	objevit	k5eAaPmAgFnS	objevit
mladá	mladý	k2eAgFnSc1d1	mladá
generace	generace	k1gFnSc1	generace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
zaobírala	zaobírat	k5eAaImAgFnS	zaobírat
problematikou	problematika	k1gFnSc7	problematika
městského	městský	k2eAgInSc2d1	městský
života	život	k1gInSc2	život
a	a	k8xC	a
současných	současný	k2eAgInPc2d1	současný
sociálních	sociální	k2eAgInPc2d1	sociální
problémů	problém	k1gInPc2	problém
<g/>
.	.	kIx.	.
</s>
<s>
Nastává	nastávat	k5eAaImIp3nS	nastávat
kulturní	kulturní	k2eAgFnSc1d1	kulturní
a	a	k8xC	a
společenská	společenský	k2eAgFnSc1d1	společenská
obroda	obroda	k1gFnSc1	obroda
<g/>
.	.	kIx.	.
</s>
<s>
Zažívají	zažívat	k5eAaImIp3nP	zažívat
dobu	doba	k1gFnSc4	doba
porevolučních	porevoluční	k2eAgNnPc2d1	porevoluční
let	léto	k1gNnPc2	léto
Bachova	Bachův	k2eAgInSc2d1	Bachův
absolutismu	absolutismus	k1gInSc2	absolutismus
a	a	k8xC	a
dočasné	dočasný	k2eAgNnSc4d1	dočasné
potlačení	potlačení	k1gNnSc4	potlačení
snah	snaha	k1gFnPc2	snaha
obrozenců	obrozenec	k1gMnPc2	obrozenec
o	o	k7c4	o
lepší	dobrý	k2eAgNnSc4d2	lepší
postavení	postavení	k1gNnSc4	postavení
českého	český	k2eAgInSc2d1	český
národa	národ	k1gInSc2	národ
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Rakousko-Uherska	Rakousko-Uhersk	k1gInSc2	Rakousko-Uhersk
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1858	[number]	k4	1858
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
almanach	almanach	k1gInSc1	almanach
Máj	máj	k1gInSc1	máj
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
ústředním	ústřední	k2eAgInSc7d1	ústřední
spojujícím	spojující	k2eAgInSc7d1	spojující
dílem	díl	k1gInSc7	díl
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Májovci	májovec	k1gMnPc1	májovec
se	se	k3xPyFc4	se
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
tvorbě	tvorba	k1gFnSc6	tvorba
primárně	primárně	k6eAd1	primárně
snažili	snažit	k5eAaImAgMnP	snažit
zachytit	zachytit	k5eAaPmF	zachytit
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
jak	jak	k8xS	jak
jí	jíst	k5eAaImIp3nS	jíst
v	v	k7c6	v
okolním	okolní	k2eAgInSc6d1	okolní
světě	svět	k1gInSc6	svět
viděli	vidět	k5eAaImAgMnP	vidět
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
s	s	k7c7	s
jejími	její	k3xOp3gInPc7	její
negativními	negativní	k2eAgInPc7d1	negativní
rysy	rys	k1gInPc7	rys
<g/>
.	.	kIx.	.
</s>
<s>
Odkazovali	odkazovat	k5eAaImAgMnP	odkazovat
se	se	k3xPyFc4	se
nejenom	nejenom	k6eAd1	nejenom
na	na	k7c4	na
českou	český	k2eAgFnSc4d1	Česká
kulturu	kultura	k1gFnSc4	kultura
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
inspirovali	inspirovat	k5eAaBmAgMnP	inspirovat
se	se	k3xPyFc4	se
i	i	k8xC	i
směry	směr	k1gInPc1	směr
evropské	evropský	k2eAgFnSc2d1	Evropská
tvorby	tvorba	k1gFnSc2	tvorba
(	(	kIx(	(
<g/>
kosmopolité	kosmopolita	k1gMnPc1	kosmopolita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Neobraceli	obracet	k5eNaImAgMnP	obracet
se	se	k3xPyFc4	se
jen	jen	k9	jen
do	do	k7c2	do
české	český	k2eAgFnSc2d1	Česká
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kladli	klást	k5eAaImAgMnP	klást
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
současný	současný	k2eAgInSc4d1	současný
okolní	okolní	k2eAgInSc4d1	okolní
svět	svět	k1gInSc4	svět
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
projevilo	projevit	k5eAaPmAgNnS	projevit
i	i	k9	i
v	v	k7c6	v
časté	častý	k2eAgFnSc6d1	častá
publicistické	publicistický	k2eAgFnSc6d1	publicistická
tvorbě	tvorba	k1gFnSc6	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
jejich	jejich	k3xOp3gFnSc3	jejich
práci	práce	k1gFnSc3	práce
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
povznesení	povznesení	k1gNnSc3	povznesení
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
na	na	k7c4	na
evropskou	evropský	k2eAgFnSc4d1	Evropská
úroveň	úroveň	k1gFnSc4	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
založeny	založit	k5eAaPmNgInP	založit
spolky	spolek	k1gInPc1	spolek
Hlahol	hlahol	k1gInSc1	hlahol
<g/>
,	,	kIx,	,
Sokol	Sokol	k1gMnSc1	Sokol
<g/>
,	,	kIx,	,
Umělecká	umělecký	k2eAgFnSc1d1	umělecká
beseda	beseda	k1gFnSc1	beseda
<g/>
,	,	kIx,	,
vycházejí	vycházet	k5eAaImIp3nP	vycházet
noviny	novina	k1gFnPc4	novina
a	a	k8xC	a
časopisy	časopis	k1gInPc4	časopis
<g/>
:	:	kIx,	:
Národní	národní	k2eAgInPc4d1	národní
listy	list	k1gInPc4	list
<g/>
,	,	kIx,	,
Čas	čas	k1gInSc1	čas
<g/>
,	,	kIx,	,
Lumír	Lumír	k1gInSc1	Lumír
<g/>
,	,	kIx,	,
Květy	květ	k1gInPc1	květ
aj.	aj.	kA	aj.
Vychází	vycházet	k5eAaImIp3nS	vycházet
Ottův	Ottův	k2eAgInSc4d1	Ottův
slovník	slovník	k1gInSc4	slovník
naučný	naučný	k2eAgInSc4d1	naučný
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
realizována	realizován	k2eAgFnSc1d1	realizována
Tylova	Tylův	k2eAgFnSc1d1	Tylova
myšlenka	myšlenka	k1gFnSc1	myšlenka
–	–	k?	–
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Neruda	Neruda	k1gMnSc1	Neruda
Vítězslav	Vítězslav	k1gMnSc1	Vítězslav
Hálek	hálka	k1gFnPc2	hálka
Karolína	Karolína	k1gFnSc1	Karolína
Světlá	světlat	k5eAaImIp3nS	světlat
Adolf	Adolf	k1gMnSc1	Adolf
Heyduk	Heyduk	k1gMnSc1	Heyduk
Rudolf	Rudolf	k1gMnSc1	Rudolf
Mayer	Mayer	k1gMnSc1	Mayer
Václav	Václav	k1gMnSc1	Václav
Šolc	Šolc	k1gMnSc1	Šolc
Jakub	Jakub	k1gMnSc1	Jakub
Arbes	Arbes	k1gMnSc1	Arbes
(	(	kIx(	(
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
:	:	kIx,	:
Literatura	literatura	k1gFnSc1	literatura
pro	pro	k7c4	pro
2	[number]	k4	2
<g/>
.	.	kIx.	.
ročník	ročník	k1gInSc1	ročník
SŠ	SŠ	kA	SŠ
DIDAKTIS	DIDAKTIS	kA	DIDAKTIS
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
s	s	k7c7	s
r.	r.	kA	r.
o.	o.	k?	o.
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
Lumírovci	lumírovec	k1gMnSc3	lumírovec
Ruchovci	ruchovec	k1gMnSc3	ruchovec
</s>
