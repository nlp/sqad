<p>
<s>
Síla	síla	k1gFnSc1	síla
jin	jin	k?	jin
a	a	k8xC	a
síla	síla	k1gFnSc1	síla
jang	janga	k1gFnPc2	janga
jsou	být	k5eAaImIp3nP	být
dvě	dva	k4xCgFnPc4	dva
spojené	spojený	k2eAgFnPc4d1	spojená
části	část	k1gFnPc4	část
jednoho	jeden	k4xCgInSc2	jeden
celku	celek	k1gInSc2	celek
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
etymologického	etymologický	k2eAgNnSc2d1	etymologické
hlediska	hledisko	k1gNnSc2	hledisko
znamenají	znamenat	k5eAaImIp3nP	znamenat
znaky	znak	k1gInPc1	znak
jin	jin	k?	jin
a	a	k8xC	a
jang	jang	k1gInSc1	jang
temnotu	temnota	k1gFnSc4	temnota
a	a	k8xC	a
světlo	světlo	k1gNnSc4	světlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Koncept	koncept	k1gInSc1	koncept
jin	jin	k?	jin
–	–	k?	–
jang	jang	k1gInSc1	jang
má	mít	k5eAaImIp3nS	mít
původ	původ	k1gInSc4	původ
v	v	k7c6	v
dávné	dávný	k2eAgFnSc6d1	dávná
čínské	čínský	k2eAgFnSc6d1	čínská
filosofii	filosofie	k1gFnSc6	filosofie
a	a	k8xC	a
popisuje	popisovat	k5eAaImIp3nS	popisovat
dvě	dva	k4xCgFnPc4	dva
navzájem	navzájem	k6eAd1	navzájem
opačné	opačný	k2eAgFnPc1d1	opačná
a	a	k8xC	a
doplňující	doplňující	k2eAgFnPc1d1	doplňující
se	se	k3xPyFc4	se
síly	síla	k1gFnPc1	síla
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
živé	živý	k2eAgFnSc6d1	živá
i	i	k8xC	i
neživé	živý	k2eNgFnSc6d1	neživá
části	část	k1gFnSc6	část
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jin	Jin	k?	Jin
(	(	kIx(	(
<g/>
čínsky	čínsky	k6eAd1	čínsky
pchin-jinem	pchinin	k1gInSc7	pchin-jin
yī	yī	k?	yī
<g/>
,	,	kIx,	,
znaky	znak	k1gInPc4	znak
zjednodušené	zjednodušený	k2eAgFnSc2d1	zjednodušená
阴	阴	k?	阴
<g/>
,	,	kIx,	,
tradiční	tradiční	k2eAgFnSc6d1	tradiční
陰	陰	k?	陰
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
tmavé	tmavý	k2eAgNnSc4d1	tmavé
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
sever	sever	k1gInSc4	sever
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
tmavší	tmavý	k2eAgInSc1d2	tmavší
element	element	k1gInSc1	element
<g/>
;	;	kIx,	;
působí	působit	k5eAaImIp3nS	působit
temně	temně	k6eAd1	temně
energicky	energicky	k6eAd1	energicky
<g/>
,	,	kIx,	,
žensky	žensky	k6eAd1	žensky
a	a	k8xC	a
koresponduje	korespondovat	k5eAaImIp3nS	korespondovat
s	s	k7c7	s
nocí	noc	k1gFnSc7	noc
<g/>
.	.	kIx.	.
</s>
<s>
Jin	Jin	k?	Jin
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
symbolizovaný	symbolizovaný	k2eAgInSc1d1	symbolizovaný
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
zemí	zem	k1gFnSc7	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jang	Jang	k1gMnSc1	Jang
(	(	kIx(	(
<g/>
čínsky	čínsky	k6eAd1	čínsky
pchin-jinem	pchinino	k1gNnSc7	pchin-jino
yáng	yánga	k1gFnPc2	yánga
<g/>
,	,	kIx,	,
znaky	znak	k1gInPc4	znak
zjednodušené	zjednodušený	k2eAgFnSc2d1	zjednodušená
阳	阳	k?	阳
<g/>
,	,	kIx,	,
tradiční	tradiční	k2eAgFnSc6d1	tradiční
陽	陽	k?	陽
<g/>
)	)	kIx)	)
jih	jih	k1gInSc1	jih
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
světlejší	světlý	k2eAgInSc1d2	světlejší
element	element	k1gInSc1	element
<g/>
;	;	kIx,	;
působí	působit	k5eAaImIp3nS	působit
vesele	vesele	k6eAd1	vesele
<g/>
,	,	kIx,	,
aktivně	aktivně	k6eAd1	aktivně
<g/>
,	,	kIx,	,
světle	světlo	k1gNnSc6	světlo
<g/>
,	,	kIx,	,
mužsky	mužsky	k6eAd1	mužsky
a	a	k8xC	a
koresponduje	korespondovat	k5eAaImIp3nS	korespondovat
se	s	k7c7	s
dnem	den	k1gInSc7	den
<g/>
.	.	kIx.	.
</s>
<s>
Jang	Jang	k1gMnSc1	Jang
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
symbolizovaný	symbolizovaný	k2eAgInSc4d1	symbolizovaný
větrem	vítr	k1gInSc7	vítr
a	a	k8xC	a
ohněm	oheň	k1gInSc7	oheň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jin	Jin	k?	Jin
(	(	kIx(	(
<g/>
ženskost	ženskost	k1gFnSc1	ženskost
<g/>
,	,	kIx,	,
temna	temno	k1gNnSc2	temno
energie	energie	k1gFnSc2	energie
síly	síla	k1gFnSc2	síla
<g/>
)	)	kIx)	)
a	a	k8xC	a
Jang	Jang	k1gMnSc1	Jang
(	(	kIx(	(
<g/>
mužská	mužský	k2eAgFnSc1d1	mužská
<g/>
,	,	kIx,	,
světlá	světlý	k2eAgFnSc1d1	světlá
<g/>
,	,	kIx,	,
tvořivá	tvořivý	k2eAgFnSc1d1	tvořivá
síla	síla	k1gFnSc1	síla
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
popisem	popis	k1gInSc7	popis
doplňujících	doplňující	k2eAgInPc2d1	doplňující
se	se	k3xPyFc4	se
opaků	opak	k1gInPc2	opak
a	a	k8xC	a
nejsou	být	k5eNaImIp3nP	být
absolutní	absolutní	k2eAgInSc4d1	absolutní
–	–	k?	–
proto	proto	k8xC	proto
jakákoliv	jakýkoliv	k3yIgFnSc1	jakýkoliv
dichotomie	dichotomie	k1gFnSc1	dichotomie
jin	jin	k?	jin
a	a	k8xC	a
jang	jang	k1gInSc1	jang
bude	být	k5eAaImBp3nS	být
z	z	k7c2	z
jiné	jiný	k2eAgFnSc2d1	jiná
perspektivy	perspektiva	k1gFnSc2	perspektiva
vypadat	vypadat	k5eAaImF	vypadat
opačně	opačně	k6eAd1	opačně
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
výše	vysoce	k6eAd2	vysoce
uvedené	uvedený	k2eAgInPc1d1	uvedený
pojmy	pojem	k1gInPc1	pojem
k	k	k7c3	k
Jin	Jin	k1gMnPc3	Jin
a	a	k8xC	a
Jang	Jang	k1gMnSc1	Jang
neberou	brát	k5eNaImIp3nP	brát
doslovně	doslovně	k6eAd1	doslovně
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
síly	síla	k1gFnPc1	síla
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
mají	mít	k5eAaImIp3nP	mít
oba	dva	k4xCgInPc4	dva
dva	dva	k4xCgInPc4	dva
stavy	stav	k1gInPc4	stav
a	a	k8xC	a
tyto	tento	k3xDgInPc1	tento
stavy	stav	k1gInPc1	stav
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
neustálém	neustálý	k2eAgInSc6d1	neustálý
pohybu	pohyb	k1gInSc6	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
západě	západ	k1gInSc6	západ
se	se	k3xPyFc4	se
často	často	k6eAd1	často
Jin	Jin	k1gMnSc1	Jin
a	a	k8xC	a
Jang	Jang	k1gMnSc1	Jang
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
nepřesně	přesně	k6eNd1	přesně
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
zlo	zlo	k1gNnSc1	zlo
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
dobro	dobro	k1gNnSc1	dobro
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nesprávná	správný	k2eNgFnSc1d1	nesprávná
je	být	k5eAaImIp3nS	být
i	i	k9	i
interpretace	interpretace	k1gFnSc1	interpretace
symbolu	symbol	k1gInSc2	symbol
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
barev	barva	k1gFnPc2	barva
–	–	k?	–
tchaj-ťi	tchaj-ťi	k1gNnSc1	tchaj-ťi
tchu	tchus	k1gInSc2	tchus
nesymbolizuje	symbolizovat	k5eNaImIp3nS	symbolizovat
černou	černý	k2eAgFnSc7d1	černá
a	a	k8xC	a
bílou	bílý	k2eAgFnSc7d1	bílá
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
červenou	červený	k2eAgFnSc4d1	červená
<g/>
)	)	kIx)	)
barvu	barva	k1gFnSc4	barva
jako	jako	k8xS	jako
opak	opak	k1gInSc4	opak
(	(	kIx(	(
<g/>
černá	černý	k2eAgFnSc1d1	černá
není	být	k5eNaImIp3nS	být
neexistence	neexistence	k1gFnSc1	neexistence
bílé	bílé	k1gNnSc4	bílé
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
v	v	k7c6	v
analogii	analogie	k1gFnSc6	analogie
tmu	tma	k1gFnSc4	tma
a	a	k8xC	a
světlo	světlo	k1gNnSc4	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Tma	tma	k6eAd1	tma
odráží	odrážet	k5eAaImIp3nS	odrážet
světlo	světlo	k1gNnSc1	světlo
a	a	k8xC	a
světlo	světlo	k1gNnSc1	světlo
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
tmu	tma	k1gFnSc4	tma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Koncept	koncept	k1gInSc1	koncept
jin	jin	k?	jin
-	-	kIx~	-
jang	jang	k1gMnSc1	jang
==	==	k?	==
</s>
</p>
<p>
<s>
Každá	každý	k3xTgFnSc1	každý
věc	věc	k1gFnSc1	věc
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
popsat	popsat	k5eAaPmF	popsat
jako	jako	k8xC	jako
zároveň	zároveň	k6eAd1	zároveň
jin	jin	k?	jin
–	–	k?	–
jang	jang	k1gInSc1	jang
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jin	Jin	k?	Jin
a	a	k8xC	a
jang	janga	k1gFnPc2	janga
se	se	k3xPyFc4	se
nevylučují	vylučovat	k5eNaImIp3nP	vylučovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Všechno	všechen	k3xTgNnSc1	všechen
má	mít	k5eAaImIp3nS	mít
svůj	svůj	k3xOyFgInSc4	svůj
opak	opak	k1gInSc4	opak
–	–	k?	–
ne	ne	k9	ne
však	však	k9	však
absolutní	absolutní	k2eAgFnSc1d1	absolutní
<g/>
.	.	kIx.	.
</s>
<s>
Žádná	žádný	k3yNgFnSc1	žádný
věc	věc	k1gFnSc1	věc
totiž	totiž	k9	totiž
není	být	k5eNaImIp3nS	být
čistě	čistě	k6eAd1	čistě
jin	jin	k?	jin
nebo	nebo	k8xC	nebo
čistě	čistě	k6eAd1	čistě
jang	janga	k1gFnPc2	janga
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
ze	z	k7c2	z
sil	síla	k1gFnPc2	síla
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
základ	základ	k1gInSc1	základ
té	ten	k3xDgFnSc6	ten
druhé	druhý	k4xOgFnSc3	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
zima	zima	k1gFnSc1	zima
se	se	k3xPyFc4	se
nemůže	moct	k5eNaImIp3nS	moct
změnit	změnit	k5eAaPmF	změnit
na	na	k7c4	na
léto	léto	k1gNnSc4	léto
<g/>
.	.	kIx.	.
</s>
<s>
Jin	Jin	k1gMnSc1	Jin
a	a	k8xC	a
jang	jang	k1gMnSc1	jang
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
sobě	sebe	k3xPyFc6	sebe
závislé	závislý	k2eAgInPc1d1	závislý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jedno	jeden	k4xCgNnSc1	jeden
nemůže	moct	k5eNaImIp3nS	moct
existovat	existovat	k5eAaImF	existovat
bez	bez	k7c2	bez
druhého	druhý	k4xOgNnSc2	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Den	den	k1gInSc1	den
nemůže	moct	k5eNaImIp3nS	moct
existovat	existovat	k5eAaImF	existovat
bez	bez	k7c2	bez
noci	noc	k1gFnSc2	noc
<g/>
.	.	kIx.	.
<g/>
Jin	Jin	k1gMnSc1	Jin
a	a	k8xC	a
jang	jang	k1gMnSc1	jang
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
dále	daleko	k6eAd2	daleko
dělit	dělit	k5eAaImF	dělit
na	na	k7c4	na
jin	jin	k?	jin
a	a	k8xC	a
jang	jang	k1gInSc1	jang
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jestliže	jestliže	k8xS	jestliže
uvažujeme	uvažovat	k5eAaImIp1nP	uvažovat
teplotu	teplota	k1gFnSc4	teplota
<g/>
,	,	kIx,	,
jin	jin	k?	jin
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
chlad	chlad	k1gInSc4	chlad
a	a	k8xC	a
jang	jang	k1gInSc4	jang
teplo	teplo	k6eAd1	teplo
<g/>
.	.	kIx.	.
</s>
<s>
Jang	Jang	k1gMnSc1	Jang
však	však	k9	však
můžeme	moct	k5eAaImIp1nP	moct
znovu	znovu	k6eAd1	znovu
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
jin	jin	k?	jin
–	–	k?	–
teplo	teplo	k1gNnSc4	teplo
a	a	k8xC	a
jang	jang	k1gInSc4	jang
–	–	k?	–
žár	žár	k1gInSc1	žár
<g/>
.	.	kIx.	.
</s>
<s>
Jin	Jin	k1gMnSc1	Jin
a	a	k8xC	a
jang	jang	k1gMnSc1	jang
se	se	k3xPyFc4	se
navzájem	navzájem	k6eAd1	navzájem
podporují	podporovat	k5eAaImIp3nP	podporovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jin	Jin	k?	Jin
a	a	k8xC	a
jang	jang	k1gMnSc1	jang
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
ve	v	k7c6	v
stavu	stav	k1gInSc6	stav
rovnováhy	rovnováha	k1gFnSc2	rovnováha
<g/>
:	:	kIx,	:
pokud	pokud	k8xS	pokud
jeden	jeden	k4xCgMnSc1	jeden
naroste	narůst	k5eAaPmIp3nS	narůst
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgMnSc1	druhý
ustoupí	ustoupit	k5eAaPmIp3nS	ustoupit
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
nerovnováze	nerovnováha	k1gFnSc3	nerovnováha
<g/>
:	:	kIx,	:
přebytek	přebytek	k1gInSc1	přebytek
jin	jin	k?	jin
<g/>
,	,	kIx,	,
přebytek	přebytek	k1gInSc1	přebytek
jang	jang	k1gInSc1	jang
<g/>
,	,	kIx,	,
nedostatek	nedostatek	k1gInSc1	nedostatek
jin	jin	k?	jin
<g/>
,	,	kIx,	,
nedostatek	nedostatek	k1gInSc4	nedostatek
jang	janga	k1gFnPc2	janga
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgFnPc1	takový
nerovnováhy	nerovnováha	k1gFnPc1	nerovnováha
znovu	znovu	k6eAd1	znovu
tvoří	tvořit	k5eAaImIp3nP	tvořit
pár	pár	k4xCyI	pár
<g/>
:	:	kIx,	:
přebytek	přebytek	k1gInSc1	přebytek
jin	jin	k?	jin
způsobí	způsobit	k5eAaPmIp3nS	způsobit
nedostatek	nedostatek	k1gInSc4	nedostatek
jang	janga	k1gFnPc2	janga
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
<s>
Jin	Jin	k1gMnSc1	Jin
a	a	k8xC	a
jang	jang	k1gMnSc1	jang
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
navzájem	navzájem	k6eAd1	navzájem
měnit	měnit	k5eAaImF	měnit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Například	například	k6eAd1	například
noc	noc	k1gFnSc1	noc
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
na	na	k7c4	na
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
změna	změna	k1gFnSc1	změna
je	být	k5eAaImIp3nS	být
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
jin	jin	k?	jin
a	a	k8xC	a
jang	jang	k1gInSc1	jang
také	také	k9	také
relativní	relativní	k2eAgInSc1d1	relativní
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
vnějšího	vnější	k2eAgMnSc2d1	vnější
pozorovatele	pozorovatel	k1gMnSc2	pozorovatel
existuje	existovat	k5eAaImIp3nS	existovat
noc	noc	k1gFnSc4	noc
a	a	k8xC	a
den	den	k1gInSc4	den
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
zároveň	zároveň	k6eAd1	zároveň
a	a	k8xC	a
doplňují	doplňovat	k5eAaImIp3nP	doplňovat
se	s	k7c7	s
<g/>
.	.	kIx.	.
<g/>
Část	část	k1gFnSc1	část
z	z	k7c2	z
jin	jin	k?	jin
je	být	k5eAaImIp3nS	být
jang	jang	k1gInSc1	jang
a	a	k8xC	a
část	část	k1gFnSc1	část
z	z	k7c2	z
jang	janga	k1gFnPc2	janga
je	být	k5eAaImIp3nS	být
jin	jin	k?	jin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Malé	Malé	k2eAgFnPc1d1	Malé
tečky	tečka	k1gFnPc1	tečka
v	v	k7c6	v
symbolu	symbol	k1gInSc6	symbol
znamenají	znamenat	k5eAaImIp3nP	znamenat
stopy	stopa	k1gFnPc1	stopa
jedné	jeden	k4xCgFnSc2	jeden
síly	síla	k1gFnSc2	síla
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
i	i	k9	i
ve	v	k7c6	v
tmě	tma	k1gFnSc6	tma
je	být	k5eAaImIp3nS	být
světlo	světlo	k1gNnSc1	světlo
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Znamená	znamenat	k5eAaImIp3nS	znamenat
to	ten	k3xDgNnSc1	ten
také	také	k9	také
<g/>
,	,	kIx,	,
že	že	k8xS	že
absolutní	absolutní	k2eAgInSc4d1	absolutní
extrém	extrém	k1gInSc4	extrém
jedné	jeden	k4xCgFnSc2	jeden
síly	síla	k1gFnSc2	síla
se	se	k3xPyFc4	se
transformuje	transformovat	k5eAaBmIp3nS	transformovat
na	na	k7c4	na
opačný	opačný	k2eAgInSc4d1	opačný
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
pohledu	pohled	k1gInSc6	pohled
pozorovatele	pozorovatel	k1gMnSc2	pozorovatel
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
daná	daný	k2eAgFnSc1d1	daná
síla	síla	k1gFnSc1	síla
jin	jin	k?	jin
nebo	nebo	k8xC	nebo
jang	jang	k1gInSc1	jang
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
nejtěžší	těžký	k2eAgInSc1d3	nejtěžší
kámen	kámen	k1gInSc1	kámen
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
nejsnadněji	snadno	k6eAd3	snadno
rozbít	rozbít	k5eAaPmF	rozbít
<g/>
.	.	kIx.	.
<g/>
Jin	Jin	k1gFnSc4	Jin
a	a	k8xC	a
jang	jang	k1gInSc4	jang
do	do	k7c2	do
sebe	sebe	k3xPyFc4	sebe
organicky	organicky	k6eAd1	organicky
zapadají	zapadat	k5eAaImIp3nP	zapadat
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
základem	základ	k1gInSc7	základ
veškerých	veškerý	k3xTgInPc2	veškerý
jevů	jev	k1gInPc2	jev
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
obraz	obraz	k1gInSc1	obraz
Hory	hora	k1gFnSc2	hora
v	v	k7c6	v
mlze	mlha	k1gFnSc6	mlha
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
harmonii	harmonie	k1gFnSc4	harmonie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
spojením	spojení	k1gNnSc7	spojení
jin	jin	k?	jin
(	(	kIx(	(
<g/>
mlhy	mlha	k1gFnSc2	mlha
<g/>
)	)	kIx)	)
a	a	k8xC	a
jang	jang	k1gMnSc1	jang
(	(	kIx(	(
<g/>
hor	hora	k1gFnPc2	hora
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Učení	učení	k1gNnSc1	učení
o	o	k7c6	o
jin	jin	k?	jin
a	a	k8xC	a
jang	jang	k1gMnSc1	jang
je	být	k5eAaImIp3nS	být
obsažené	obsažený	k2eAgInPc1d1	obsažený
už	už	k6eAd1	už
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
I-ťing	I-ťing	k1gInSc1	I-ťing
(	(	kIx(	(
<g/>
Kniha	kniha	k1gFnSc1	kniha
proměn	proměna	k1gFnPc2	proměna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pomocí	pomocí	k7c2	pomocí
trigramů	trigram	k1gInPc2	trigram
(	(	kIx(	(
<g/>
elementů	element	k1gInPc2	element
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
z	z	k7c2	z
tří	tři	k4xCgInPc2	tři
jin	jin	k?	jin
nebo	nebo	k8xC	nebo
jang	jang	k1gInSc1	jang
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vyslovená	vyslovený	k2eAgFnSc1d1	vyslovená
myšlenka	myšlenka	k1gFnSc1	myšlenka
přeměn	přeměna	k1gFnPc2	přeměna
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
koncept	koncept	k1gInSc1	koncept
si	se	k3xPyFc3	se
osvojily	osvojit	k5eAaPmAgFnP	osvojit
obě	dva	k4xCgFnPc1	dva
čínské	čínský	k2eAgFnPc1d1	čínská
filosofie	filosofie	k1gFnPc1	filosofie
<g/>
,	,	kIx,	,
taoismus	taoismus	k1gInSc1	taoismus
a	a	k8xC	a
konfucianismus	konfucianismus	k1gInSc1	konfucianismus
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
raně	raně	k6eAd1	raně
středověký	středověký	k2eAgMnSc1d1	středověký
konfuciánský	konfuciánský	k2eAgMnSc1d1	konfuciánský
filosof	filosof	k1gMnSc1	filosof
Tung	Tung	k1gMnSc1	Tung
Čung-šu	Čung-šu	k1gMnSc1	Čung-šu
učil	učit	k5eAaImAgMnS	učit
<g/>
:	:	kIx,	:
Doplňkem	doplněk	k1gInSc7	doplněk
všech	všecek	k3xTgFnPc2	všecek
věcí	věc	k1gFnPc2	věc
je	být	k5eAaImIp3nS	být
jin	jin	k?	jin
a	a	k8xC	a
jang	jang	k1gInSc1	jang
<g/>
...	...	k?	...
Principy	princip	k1gInPc1	princip
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
základem	základ	k1gInSc7	základ
vladaře	vladař	k1gMnSc2	vladař
i	i	k8xC	i
služebníka	služebník	k1gMnSc2	služebník
<g/>
,	,	kIx,	,
otce	otec	k1gMnSc2	otec
i	i	k8xC	i
syna	syn	k1gMnSc2	syn
<g/>
,	,	kIx,	,
muže	muž	k1gMnSc2	muž
i	i	k8xC	i
ženy	žena	k1gFnPc1	žena
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
všechny	všechen	k3xTgInPc4	všechen
zrozené	zrozený	k2eAgInPc4d1	zrozený
z	z	k7c2	z
jin	jin	k?	jin
a	a	k8xC	a
jang	jang	k1gInSc1	jang
<g/>
.	.	kIx.	.
</s>
<s>
Vladař	vladař	k1gMnSc1	vladař
je	být	k5eAaImIp3nS	být
jang	jang	k1gMnSc1	jang
a	a	k8xC	a
služebník	služebník	k1gMnSc1	služebník
je	být	k5eAaImIp3nS	být
jin	jin	k?	jin
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
je	být	k5eAaImIp3nS	být
jang	jang	k1gMnSc1	jang
a	a	k8xC	a
syn	syn	k1gMnSc1	syn
je	být	k5eAaImIp3nS	být
jin	jin	k?	jin
<g/>
.	.	kIx.	.
</s>
<s>
Manžel	manžel	k1gMnSc1	manžel
je	být	k5eAaImIp3nS	být
jang	jang	k1gMnSc1	jang
a	a	k8xC	a
manželka	manželka	k1gFnSc1	manželka
je	být	k5eAaImIp3nS	být
jin	jin	k?	jin
<g/>
...	...	k?	...
</s>
</p>
<p>
<s>
Učení	učení	k1gNnSc1	učení
o	o	k7c6	o
jin	jin	k?	jin
a	a	k8xC	a
jang	janga	k1gFnPc2	janga
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
společnou	společný	k2eAgFnSc7d1	společná
půdou	půda	k1gFnSc7	půda
<g/>
,	,	kIx,	,
na	na	k7c6	na
které	který	k3yQgFnSc6	který
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
značnému	značný	k2eAgNnSc3d1	značné
sbližování	sbližování	k1gNnSc3	sbližování
a	a	k8xC	a
vzájemnému	vzájemný	k2eAgNnSc3d1	vzájemné
doplňování	doplňování	k1gNnSc3	doplňování
a	a	k8xC	a
ovlivňování	ovlivňování	k1gNnSc4	ovlivňování
protikladných	protikladný	k2eAgFnPc2d1	protikladná
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Jin	Jin	k1gFnPc2	Jin
a	a	k8xC	a
jang	janga	k1gFnPc2	janga
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Starověká	starověký	k2eAgFnSc1d1	starověká
čínská	čínský	k2eAgFnSc1d1	čínská
filosofie	filosofie	k1gFnSc1	filosofie
</s>
</p>
<p>
<s>
I-ťing	I-ťing	k1gInSc1	I-ťing
</s>
</p>
<p>
<s>
Taoismus	taoismus	k1gInSc1	taoismus
</s>
</p>
<p>
<s>
Konfucianismus	konfucianismus	k1gInSc1	konfucianismus
</s>
</p>
<p>
<s>
Druhé	druhý	k4xOgNnSc1	druhý
pohlaví	pohlaví	k1gNnSc1	pohlaví
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Jin	Jin	k1gFnPc2	Jin
a	a	k8xC	a
jang	janga	k1gFnPc2	janga
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc7	jejichž
tématem	téma	k1gNnSc7	téma
jsou	být	k5eAaImIp3nP	být
jin	jin	k?	jin
a	a	k8xC	a
jang	jang	k1gMnSc1	jang
</s>
</p>
