<p>
<s>
Himálaj	Himálaj	k1gFnSc1	Himálaj
(	(	kIx(	(
<g/>
sanskrtem	sanskrt	k1gInSc7	sanskrt
<g/>
,	,	kIx,	,
ह	ह	k?	ह
<g/>
ि	ि	k?	ि
<g/>
म	म	k?	म
<g/>
ा	ा	k?	ा
<g/>
ल	ल	k?	ल
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
název	název	k1gInSc4	název
pro	pro	k7c4	pro
pohoří	pohoří	k1gNnSc4	pohoří
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
Indický	indický	k2eAgInSc1d1	indický
subkontinent	subkontinent	k1gInSc1	subkontinent
od	od	k7c2	od
Tibetské	tibetský	k2eAgFnSc2d1	tibetská
náhorní	náhorní	k2eAgFnSc2d1	náhorní
roviny	rovina	k1gFnSc2	rovina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
pohoří	pohoří	k1gNnSc6	pohoří
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
deset	deset	k4xCc1	deset
ze	z	k7c2	z
čtrnácti	čtrnáct	k4xCc2	čtrnáct
nejvyšších	vysoký	k2eAgInPc2d3	Nejvyšší
vrcholů	vrchol	k1gInPc2	vrchol
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
takzvaných	takzvaný	k2eAgFnPc2d1	takzvaná
osmitisícovek	osmitisícovka	k1gFnPc2	osmitisícovka
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
Himálaje	Himálaj	k1gFnSc2	Himálaj
i	i	k8xC	i
světa	svět	k1gInSc2	svět
je	být	k5eAaImIp3nS	být
Mount	Mount	k1gInSc1	Mount
Everest	Everest	k1gInSc1	Everest
(	(	kIx(	(
<g/>
8	[number]	k4	8
850	[number]	k4	850
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pohoří	pohoří	k1gNnSc1	pohoří
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
na	na	k7c6	na
území	území	k1gNnSc6	území
Pákistánu	Pákistán	k1gInSc2	Pákistán
<g/>
,	,	kIx,	,
Číny	Čína	k1gFnSc2	Čína
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc2	Indie
<g/>
,	,	kIx,	,
Nepálu	Nepál	k1gInSc2	Nepál
a	a	k8xC	a
Bhútánu	Bhútán	k1gInSc2	Bhútán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Název	název	k1gInSc1	název
==	==	k?	==
</s>
</p>
<p>
<s>
Název	název	k1gInSc1	název
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
sanskrtských	sanskrtský	k2eAgNnPc2d1	sanskrtské
slov	slovo	k1gNnPc2	slovo
hima	hima	k1gFnSc1	hima
(	(	kIx(	(
<g/>
sníh	sníh	k1gInSc1	sníh
<g/>
)	)	kIx)	)
a	a	k8xC	a
álaja	álaj	k2eAgFnSc1d1	álaj
(	(	kIx(	(
<g/>
obydlí	obydlí	k1gNnPc2	obydlí
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
vykládá	vykládat	k5eAaImIp3nS	vykládat
se	se	k3xPyFc4	se
jako	jako	k9	jako
Domov	domov	k1gInSc1	domov
sněhu	sníh	k1gInSc2	sníh
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
starší	starý	k2eAgFnSc6d2	starší
literatuře	literatura	k1gFnSc6	literatura
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
i	i	k9	i
s	s	k7c7	s
názvem	název	k1gInSc7	název
Himalaje	Himalaje	k1gFnSc2	Himalaje
nebo	nebo	k8xC	nebo
Himaláje	Himaláje	k1gFnPc4	Himaláje
(	(	kIx(	(
<g/>
pomnožné	pomnožný	k2eAgFnPc4d1	pomnožná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
jazycích	jazyk	k1gInPc6	jazyk
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
pohoří	pohoří	k1gNnSc2	pohoří
zapisuje	zapisovat	k5eAaImIp3nS	zapisovat
</s>
</p>
<p>
<s>
sanskrtem	sanskrt	k1gInSc7	sanskrt
<g/>
,	,	kIx,	,
hindsky	hindsky	k6eAd1	hindsky
i	i	k9	i
nepálsky	nepálsky	k6eAd1	nepálsky
ह	ह	k?	ह
<g/>
ि	ि	k?	ि
<g/>
म	म	k?	म
<g/>
ा	ा	k?	ा
<g/>
ल	ल	k?	ल
<g/>
,	,	kIx,	,
Himā	Himā	k1gFnSc1	Himā
</s>
</p>
<p>
<s>
urdsky	urdsky	k6eAd1	urdsky
س	س	k?	س
ک	ک	k?	ک
ہ	ہ	k?	ہ
<g/>
,	,	kIx,	,
Selseleh	Selseleh	k1gMnSc1	Selseleh
Kū	Kū	k1gMnSc1	Kū
Himā	Himā	k1gMnSc1	Himā
</s>
</p>
<p>
<s>
paňdžábsky	paňdžábsky	k6eAd1	paňdžábsky
ھ	ھ	k?	ھ
<g/>
,	,	kIx,	,
Himā	Himā	k1gFnSc1	Himā
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
bengálsky	bengálsky	k6eAd1	bengálsky
হ	হ	k?	হ
<g/>
ি	ি	k?	ি
<g/>
ম	ম	k?	ম
<g/>
া	া	k?	া
<g/>
ল	ল	k?	ল
<g/>
়	়	k?	়
প	প	k?	প
<g/>
্	্	k?	্
<g/>
ব	ব	k?	ব
<g/>
া	া	k?	া
<g/>
ল	ল	k?	ল
<g/>
া	া	k?	া
<g/>
,	,	kIx,	,
Himálaĵ	Himálaĵ	k1gFnSc1	Himálaĵ
parbatamálá	parbatamálat	k5eAaPmIp3nS	parbatamálat
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
tibetsky	tibetsky	k6eAd1	tibetsky
ཧ	ཧ	k?	ཧ
<g/>
ི	ི	k?	ི
<g/>
་	་	k?	་
<g/>
མ	མ	k?	མ
<g/>
་	་	k?	་
<g/>
ལ	ལ	k?	ལ
<g/>
་	་	k?	་
<g/>
ཡ	ཡ	k?	ཡ
<g/>
,	,	kIx,	,
wylie	wylie	k1gFnSc1	wylie
Himalaya	Himalaya	k1gFnSc1	Himalaya
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
čínsky	čínsky	k6eAd1	čínsky
喜	喜	k?	喜
<g/>
,	,	kIx,	,
pinyin	pinyin	k2eAgMnSc1d1	pinyin
Xǐ	Xǐ	k1gMnSc1	Xǐ
Shā	Shā	k1gMnSc1	Shā
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
český	český	k2eAgInSc1d1	český
přepis	přepis	k1gInSc1	přepis
Si-ma-la-ja	Siaaa	k1gMnSc1	Si-ma-la-ja
šan-maj	šanaj	k1gMnSc1	šan-maj
</s>
</p>
<p>
<s>
==	==	k?	==
Členění	členění	k1gNnSc1	členění
==	==	k?	==
</s>
</p>
<p>
<s>
Podélně	podélně	k6eAd1	podélně
se	se	k3xPyFc4	se
Himálaj	Himálaj	k1gFnSc1	Himálaj
člení	členit	k5eAaImIp3nS	členit
na	na	k7c4	na
různě	různě	k6eAd1	různě
vysoká	vysoký	k2eAgFnSc1d1	vysoká
a	a	k8xC	a
různě	různě	k6eAd1	různě
stará	starat	k5eAaImIp3nS	starat
souběžná	souběžný	k2eAgNnPc4d1	souběžné
horská	horský	k2eAgNnPc4d1	horské
pásma	pásmo	k1gNnPc4	pásmo
<g/>
.	.	kIx.	.
</s>
<s>
Příčně	příčně	k6eAd1	příčně
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
vymezují	vymezovat	k5eAaImIp3nP	vymezovat
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
úseky	úsek	k1gInPc4	úsek
podle	podle	k7c2	podle
různých	různý	k2eAgNnPc2d1	různé
kritérií	kritérion	k1gNnPc2	kritérion
(	(	kIx(	(
<g/>
např.	např.	kA	např.
úseky	úsek	k1gInPc4	úsek
vymezené	vymezený	k2eAgInPc4d1	vymezený
velkými	velký	k2eAgFnPc7d1	velká
řekami	řeka	k1gFnPc7	řeka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
prorážejí	prorážet	k5eAaImIp3nP	prorážet
Himálaj	Himálaj	k1gFnSc4	Himálaj
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
na	na	k7c4	na
jih	jih	k1gInSc4	jih
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Oblasti	oblast	k1gFnPc4	oblast
===	===	k?	===
</s>
</p>
<p>
<s>
Ze	z	k7c2	z
západu	západ	k1gInSc2	západ
na	na	k7c4	na
východ	východ	k1gInSc4	východ
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Kašmírský	kašmírský	k2eAgInSc1d1	kašmírský
Himálaj	Himálaj	k1gFnSc4	Himálaj
mezi	mezi	k7c7	mezi
řekami	řeka	k1gFnPc7	řeka
Indus	Indus	k1gInSc1	Indus
a	a	k8xC	a
Drás	Drás	k1gInSc1	Drás
(	(	kIx(	(
<g/>
Nanga	Nanga	k1gFnSc1	Nanga
Parbat	Parbat	k1gFnSc1	Parbat
<g/>
,	,	kIx,	,
8125	[number]	k4	8125
m	m	kA	m
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Paňdžábský	paňdžábský	k2eAgInSc1d1	paňdžábský
Himálaj	Himálaj	k1gFnSc4	Himálaj
mezi	mezi	k7c7	mezi
řekami	řeka	k1gFnPc7	řeka
Drás	Drása	k1gFnPc2	Drása
a	a	k8xC	a
Satledž	Satledž	k1gFnSc1	Satledž
(	(	kIx(	(
<g/>
Nunkun	Nunkun	k1gNnSc1	Nunkun
<g/>
,	,	kIx,	,
7135	[number]	k4	7135
m	m	kA	m
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Garhválský	Garhválský	k2eAgInSc1d1	Garhválský
Himálaj	Himálaj	k1gFnSc1	Himálaj
mezi	mezi	k7c7	mezi
řekou	řeka	k1gFnSc7	řeka
Satledž	Satledž	k1gFnSc1	Satledž
a	a	k8xC	a
Nepálem	Nepál	k1gInSc7	Nepál
(	(	kIx(	(
<g/>
Nandádéví	Nandádéví	k1gNnSc1	Nandádéví
<g/>
,	,	kIx,	,
7816	[number]	k4	7816
m	m	kA	m
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nepálský	nepálský	k2eAgInSc1d1	nepálský
Himálaj	Himálaj	k1gFnSc1	Himálaj
(	(	kIx(	(
<g/>
Sagarmatha	Sagarmatha	k1gMnSc1	Sagarmatha
<g/>
,	,	kIx,	,
8848	[number]	k4	8848
m	m	kA	m
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sikkimský	Sikkimský	k2eAgInSc1d1	Sikkimský
Himálaj	Himálaj	k1gFnSc1	Himálaj
včetně	včetně	k7c2	včetně
nepálského	nepálský	k2eAgNnSc2d1	nepálské
pohraničí	pohraničí	k1gNnSc2	pohraničí
(	(	kIx(	(
<g/>
Kančendženga	Kančendženga	k1gFnSc1	Kančendženga
<g/>
,	,	kIx,	,
8586	[number]	k4	8586
m	m	kA	m
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bhútánský	bhútánský	k2eAgInSc1d1	bhútánský
Himálaj	Himálaj	k1gFnSc1	Himálaj
(	(	kIx(	(
<g/>
Kula	kula	k1gFnSc1	kula
Kangri	Kangr	k1gFnSc2	Kangr
<g/>
,	,	kIx,	,
7554	[number]	k4	7554
m	m	kA	m
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ásámský	Ásámský	k2eAgInSc1d1	Ásámský
Himálaj	Himálaj	k1gFnSc1	Himálaj
(	(	kIx(	(
<g/>
Namčhe	Namčhe	k1gFnSc1	Namčhe
Barwa	Barwa	k1gFnSc1	Barwa
<g/>
,	,	kIx,	,
7755	[number]	k4	7755
m	m	kA	m
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Pásma	pásmo	k1gNnSc2	pásmo
===	===	k?	===
</s>
</p>
<p>
<s>
Himálaje	Himálaj	k1gFnPc1	Himálaj
jsou	být	k5eAaImIp3nP	být
složeny	složit	k5eAaPmNgFnP	složit
ze	z	k7c2	z
tří	tři	k4xCgNnPc2	tři
hlavních	hlavní	k2eAgNnPc2d1	hlavní
pásem	pásmo	k1gNnPc2	pásmo
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
se	se	k3xPyFc4	se
stupňovitě	stupňovitě	k6eAd1	stupňovitě
zvedají	zvedat	k5eAaImIp3nP	zvedat
od	od	k7c2	od
jihu	jih	k1gInSc2	jih
na	na	k7c4	na
sever	sever	k1gInSc4	sever
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Siválik	Siválik	k1gMnSc1	Siválik
(	(	kIx(	(
<g/>
900	[number]	k4	900
–	–	k?	–
1	[number]	k4	1
200	[number]	k4	200
m	m	kA	m
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Malý	Malý	k1gMnSc1	Malý
Himálaj	Himálaj	k1gFnSc1	Himálaj
neboli	neboli	k8xC	neboli
Mahábhárat	Mahábhárata	k1gFnPc2	Mahábhárata
(	(	kIx(	(
<g/>
3	[number]	k4	3
000	[number]	k4	000
–	–	k?	–
4	[number]	k4	4
000	[number]	k4	000
m	m	kA	m
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Velký	velký	k2eAgInSc1d1	velký
Himálaj	Himálaj	k1gFnSc4	Himálaj
(	(	kIx(	(
<g/>
6	[number]	k4	6
000	[number]	k4	000
–	–	k?	–
8	[number]	k4	8
848	[number]	k4	848
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
Himálajská	himálajský	k2eAgFnSc1d1	himálajská
flóra	flóra	k1gFnSc1	flóra
a	a	k8xC	a
fauna	fauna	k1gFnSc1	fauna
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
podle	podle	k7c2	podle
podnebí	podnebí	k1gNnSc2	podnebí
<g/>
,	,	kIx,	,
dešťů	dešť	k1gInPc2	dešť
<g/>
,	,	kIx,	,
výšky	výška	k1gFnSc2	výška
a	a	k8xC	a
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
Podnebí	podnebí	k1gNnSc1	podnebí
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
od	od	k7c2	od
tropického	tropický	k2eAgNnSc2d1	tropické
na	na	k7c6	na
úpatí	úpatí	k1gNnSc6	úpatí
hor	hora	k1gFnPc2	hora
až	až	k9	až
po	po	k7c4	po
věčný	věčný	k2eAgInSc4d1	věčný
sníh	sníh	k1gInSc4	sníh
a	a	k8xC	a
led	led	k1gInSc4	led
v	v	k7c6	v
nejvyšších	vysoký	k2eAgFnPc6d3	nejvyšší
polohách	poloha	k1gFnPc6	poloha
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
každoročních	každoroční	k2eAgFnPc2d1	každoroční
srážek	srážka	k1gFnPc2	srážka
se	se	k3xPyFc4	se
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
podél	podél	k7c2	podél
pohoří	pohoří	k1gNnSc2	pohoří
od	od	k7c2	od
západu	západ	k1gInSc2	západ
na	na	k7c4	na
východ	východ	k1gInSc4	východ
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
různorodosti	různorodost	k1gFnSc3	různorodost
podnebí	podnebí	k1gNnSc2	podnebí
<g/>
,	,	kIx,	,
výšky	výška	k1gFnSc2	výška
<g/>
,	,	kIx,	,
množství	množství	k1gNnSc1	množství
dešťů	dešť	k1gInPc2	dešť
a	a	k8xC	a
druhů	druh	k1gInPc2	druh
půdy	půda	k1gFnSc2	půda
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Himálaji	Himálaj	k1gFnSc6	Himálaj
mnoho	mnoho	k6eAd1	mnoho
různých	různý	k2eAgFnPc2d1	různá
rostlin	rostlina	k1gFnPc2	rostlina
a	a	k8xC	a
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
Indoganžské	Indoganžský	k2eAgFnSc6d1	Indoganžská
nížině	nížina	k1gFnSc6	nížina
na	na	k7c6	na
úpatí	úpatí	k1gNnSc6	úpatí
hor	hora	k1gFnPc2	hora
se	se	k3xPyFc4	se
vegetace	vegetace	k1gFnSc1	vegetace
liší	lišit	k5eAaImIp3nS	lišit
od	od	k7c2	od
západu	západ	k1gInSc2	západ
k	k	k7c3	k
východu	východ	k1gInSc3	východ
podle	podle	k7c2	podle
množství	množství	k1gNnSc2	množství
dešťů	dešť	k1gInPc2	dešť
<g/>
.	.	kIx.	.
</s>
<s>
Roviny	rovina	k1gFnPc1	rovina
v	v	k7c6	v
Pákistánu	Pákistán	k1gInSc6	Pákistán
a	a	k8xC	a
indickém	indický	k2eAgInSc6d1	indický
Pandžábu	Pandžáb	k1gInSc6	Pandžáb
jsou	být	k5eAaImIp3nP	být
porostlé	porostlý	k2eAgInPc1d1	porostlý
suchomilnými	suchomilný	k2eAgInPc7d1	suchomilný
trnovými	trnový	k2eAgInPc7d1	trnový
křovinovými	křovinův	k2eAgInPc7d1	křovinův
lesy	les	k1gInPc7	les
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
na	na	k7c4	na
východ	východ	k1gInSc4	východ
rostou	růst	k5eAaImIp3nP	růst
vlhké	vlhký	k2eAgInPc1d1	vlhký
opadavé	opadavý	k2eAgInPc1d1	opadavý
lesy	les	k1gInPc1	les
v	v	k7c6	v
Uttarakhandu	Uttarakhando	k1gNnSc6	Uttarakhando
a	a	k8xC	a
Uttarpradéši	Uttarpradéše	k1gFnSc6	Uttarpradéše
na	na	k7c6	na
horní	horní	k2eAgFnSc6d1	horní
Ganze	Ganga	k1gFnSc6	Ganga
a	a	k8xC	a
v	v	k7c6	v
Biháru	Bihár	k1gInSc6	Bihár
a	a	k8xC	a
Západním	západní	k2eAgNnSc6d1	západní
Bengálsku	Bengálsko	k1gNnSc6	Bengálsko
na	na	k7c6	na
dolní	dolní	k2eAgFnSc6d1	dolní
Ganze	Ganga	k1gFnSc6	Ganga
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
monzunové	monzunový	k2eAgInPc1d1	monzunový
lesy	les	k1gInPc1	les
<g/>
,	,	kIx,	,
z	z	k7c2	z
jejichž	jejichž	k3xOyRp3gInPc2	jejichž
stromů	strom	k1gInPc2	strom
opadá	opadat	k5eAaBmIp3nS	opadat
listí	listí	k1gNnSc1	listí
během	během	k7c2	během
období	období	k1gNnSc2	období
sucha	sucho	k1gNnSc2	sucho
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ásámských	ásámský	k2eAgFnPc6d1	ásámský
rovinách	rovina	k1gFnPc6	rovina
rostou	růst	k5eAaImIp3nP	růst
tropické	tropický	k2eAgInPc1d1	tropický
vlhké	vlhký	k2eAgInPc1d1	vlhký
lesy	les	k1gInPc1	les
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nad	nad	k7c7	nad
nížinou	nížina	k1gFnSc7	nížina
leží	ležet	k5eAaImIp3nS	ležet
pás	pás	k1gInSc1	pás
terají	teraj	k1gFnPc2	teraj
<g/>
,	,	kIx,	,
oblast	oblast	k1gFnSc1	oblast
písčité	písčitý	k2eAgFnSc2d1	písčitá
a	a	k8xC	a
hlinité	hlinitý	k2eAgFnSc2d1	hlinitá
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
Terai	Terai	k6eAd1	Terai
má	mít	k5eAaImIp3nS	mít
více	hodně	k6eAd2	hodně
srážek	srážka	k1gFnPc2	srážka
než	než	k8xS	než
nížina	nížina	k1gFnSc1	nížina
a	a	k8xC	a
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
rovinaté	rovinatý	k2eAgFnSc6d1	rovinatá
oblasti	oblast	k1gFnSc6	oblast
se	se	k3xPyFc4	se
zpomalují	zpomalovat	k5eAaImIp3nP	zpomalovat
řeky	řeka	k1gFnPc1	řeka
tekoucí	tekoucí	k2eAgFnPc1d1	tekoucí
z	z	k7c2	z
Himálají	Himálaj	k1gFnPc2	Himálaj
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
monzunovém	monzunový	k2eAgNnSc6d1	monzunové
období	období	k1gNnSc6	období
rozvodňují	rozvodňovat	k5eAaImIp3nP	rozvodňovat
a	a	k8xC	a
přinášejí	přinášet	k5eAaImIp3nP	přinášet
úrodný	úrodný	k2eAgInSc4d1	úrodný
nános	nános	k1gInSc4	nános
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
období	období	k1gNnSc6	období
sucha	sucho	k1gNnSc2	sucho
ustupují	ustupovat	k5eAaImIp3nP	ustupovat
<g/>
.	.	kIx.	.
</s>
<s>
Střední	střední	k2eAgFnSc1d1	střední
část	část	k1gFnSc1	část
pásu	pás	k1gInSc2	pás
Terai	Tera	k1gFnSc2	Tera
tvoří	tvořit	k5eAaImIp3nS	tvořit
směs	směs	k1gFnSc4	směs
luk	louka	k1gFnPc2	louka
<g/>
,	,	kIx,	,
savan	savana	k1gFnPc2	savana
<g/>
,	,	kIx,	,
opadavých	opadavý	k2eAgFnPc2d1	opadavá
i	i	k9	i
stále	stále	k6eAd1	stále
zelených	zelený	k2eAgInPc2d1	zelený
lesů	les	k1gInPc2	les
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgFnPc7	jenž
jsou	být	k5eAaImIp3nP	být
nejvýše	vysoce	k6eAd3	vysoce
položené	položený	k2eAgFnPc1d1	položená
louky	louka	k1gFnPc1	louka
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Teraiské	Teraiský	k2eAgFnPc1d1	Teraiský
louky	louka	k1gFnPc1	louka
jsou	být	k5eAaImIp3nP	být
domovem	domov	k1gInSc7	domov
nosorožce	nosorožec	k1gMnSc2	nosorožec
indického	indický	k2eAgMnSc2d1	indický
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nad	nad	k7c7	nad
teraiským	teraiský	k2eAgInSc7d1	teraiský
pásem	pás	k1gInSc7	pás
leží	ležet	k5eAaImIp3nS	ležet
náhorní	náhorní	k2eAgFnSc1d1	náhorní
oblast	oblast	k1gFnSc1	oblast
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xS	jako
Bhabhar	Bhabhar	k1gInSc1	Bhabhar
<g/>
,	,	kIx,	,
oblast	oblast	k1gFnSc1	oblast
porézní	porézní	k2eAgFnSc2d1	porézní
a	a	k8xC	a
kamenité	kamenitý	k2eAgFnSc2d1	kamenitá
půdy	půda	k1gFnSc2	půda
tvořené	tvořený	k2eAgFnSc2d1	tvořená
naplaveninami	naplavenina	k1gFnPc7	naplavenina
z	z	k7c2	z
vyšších	vysoký	k2eAgFnPc2d2	vyšší
poloh	poloha	k1gFnPc2	poloha
<g/>
.	.	kIx.	.
</s>
<s>
Bhabhar	Bhabhar	k1gInSc1	Bhabhar
a	a	k8xC	a
níže	nízce	k6eAd2	nízce
položená	položený	k2eAgNnPc4d1	položené
místa	místo	k1gNnPc4	místo
v	v	k7c4	v
Sivaliku	Sivalika	k1gFnSc4	Sivalika
mají	mít	k5eAaImIp3nP	mít
subtropické	subtropický	k2eAgNnSc4d1	subtropické
klima	klima	k1gNnSc4	klima
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
tohoto	tento	k3xDgNnSc2	tento
subtropického	subtropický	k2eAgNnSc2d1	subtropické
pásma	pásmo	k1gNnSc2	pásmo
rostou	růst	k5eAaImIp3nP	růst
subtropické	subtropický	k2eAgInPc1d1	subtropický
borovicové	borovicový	k2eAgInPc1d1	borovicový
lesy	les	k1gInPc1	les
<g/>
.	.	kIx.	.
</s>
<s>
Střední	střední	k2eAgFnSc1d1	střední
část	část	k1gFnSc1	část
pásma	pásmo	k1gNnSc2	pásmo
je	být	k5eAaImIp3nS	být
domovem	domov	k1gInSc7	domov
himálajských	himálajský	k2eAgInPc2d1	himálajský
subtropických	subtropický	k2eAgInPc2d1	subtropický
listnatých	listnatý	k2eAgInPc2d1	listnatý
lesů	les	k1gInPc2	les
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sivalické	Sivalický	k2eAgFnPc1d1	Sivalický
hory	hora	k1gFnPc1	hora
<g/>
,	,	kIx,	,
nazývané	nazývaný	k2eAgFnPc1d1	nazývaná
také	také	k6eAd1	také
Churijské	Churijský	k2eAgFnPc1d1	Churijský
nebo	nebo	k8xC	nebo
Margallské	Margallský	k2eAgFnPc1d1	Margallský
hory	hora	k1gFnPc1	hora
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nP	tvořit
vnější	vnější	k2eAgNnSc4d1	vnější
pásmo	pásmo	k1gNnSc4	pásmo
kopců	kopec	k1gInPc2	kopec
rozšířených	rozšířený	k2eAgInPc2d1	rozšířený
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
oblasti	oblast	k1gFnSc6	oblast
Himálají	Himálaj	k1gFnPc2	Himálaj
skrz	skrz	k7c4	skrz
Pákistán	Pákistán	k1gInSc4	Pákistán
<g/>
,	,	kIx,	,
Indii	Indie	k1gFnSc4	Indie
<g/>
,	,	kIx,	,
Nepál	Nepál	k1gInSc4	Nepál
a	a	k8xC	a
Bhútán	Bhútán	k1gInSc4	Bhútán
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
subpásem	subpás	k1gInSc7	subpás
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
jeho	jeho	k3xOp3gInPc2	jeho
vrcholů	vrchol	k1gInPc2	vrchol
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
výšky	výška	k1gFnPc4	výška
od	od	k7c2	od
600	[number]	k4	600
do	do	k7c2	do
1	[number]	k4	1
200	[number]	k4	200
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgInPc1d1	jižní
svahy	svah	k1gInPc1	svah
jsou	být	k5eAaImIp3nP	být
strmější	strmý	k2eAgInPc1d2	strmější
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nS	tvořit
zlom	zlom	k1gInSc1	zlom
<g/>
,	,	kIx,	,
severní	severní	k2eAgInPc1d1	severní
svahy	svah	k1gInPc1	svah
jsou	být	k5eAaImIp3nP	být
mírnější	mírný	k2eAgInPc1d2	mírnější
<g/>
.	.	kIx.	.
</s>
<s>
Propustné	propustný	k2eAgFnPc1d1	propustná
naplaveniny	naplavenina	k1gFnPc1	naplavenina
a	a	k8xC	a
jiné	jiný	k2eAgInPc1d1	jiný
kameny	kámen	k1gInPc1	kámen
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
dešťové	dešťový	k2eAgFnSc3d1	dešťová
vodě	voda	k1gFnSc3	voda
vsakovat	vsakovat	k5eAaImF	vsakovat
se	se	k3xPyFc4	se
a	a	k8xC	a
odtékat	odtékat	k5eAaImF	odtékat
dolů	dolů	k6eAd1	dolů
do	do	k7c2	do
Bhabharu	Bhabhar	k1gInSc2	Bhabhar
a	a	k8xC	a
Terai	Tera	k1gFnSc2	Tera
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
nahoře	nahoře	k6eAd1	nahoře
rostou	růst	k5eAaImIp3nP	růst
jen	jen	k9	jen
křovinové	křovinový	k2eAgInPc1d1	křovinový
lesy	les	k1gInPc1	les
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
sever	sever	k1gInSc4	sever
od	od	k7c2	od
Sivalických	Sivalický	k2eAgFnPc2d1	Sivalický
hor	hora	k1gFnPc2	hora
a	a	k8xC	a
mezi	mezi	k7c7	mezi
sivalickými	sivalický	k2eAgNnPc7d1	sivalický
pohořími	pohoří	k1gNnPc7	pohoří
leží	ležet	k5eAaImIp3nP	ležet
otevřená	otevřený	k2eAgNnPc1d1	otevřené
údolí	údolí	k1gNnPc1	údolí
<g/>
,	,	kIx,	,
zvaná	zvaný	k2eAgNnPc1d1	zvané
Vnitřní	vnitřní	k2eAgNnPc1d1	vnitřní
Terai	Terai	k1gNnPc1	Terai
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Dehra	Dehra	k1gFnSc1	Dehra
Dun	duna	k1gFnPc2	duna
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
nebo	nebo	k8xC	nebo
Chitwan	Chitwan	k1gInSc4	Chitwan
v	v	k7c6	v
Nepálu	Nepál	k1gInSc6	Nepál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Severně	severně	k6eAd1	severně
od	od	k7c2	od
Mahabharátského	Mahabharátský	k2eAgInSc2d1	Mahabharátský
hřebenu	hřeben	k1gInSc2	hřeben
leží	ležet	k5eAaImIp3nS	ležet
hornatý	hornatý	k2eAgInSc1d1	hornatý
region	region	k1gInSc1	region
dosahující	dosahující	k2eAgInSc1d1	dosahující
do	do	k7c2	do
výšek	výška	k1gFnPc2	výška
4	[number]	k4	4
000	[number]	k4	000
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
začíná	začínat	k5eAaImIp3nS	začínat
Velký	velký	k2eAgInSc1d1	velký
Himálaj	Himálaj	k1gFnSc4	Himálaj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gFnPc6	jeho
středních	střední	k2eAgFnPc6d1	střední
výškách	výška	k1gFnPc6	výška
subtropické	subtropický	k2eAgInPc1d1	subtropický
lesy	les	k1gInPc1	les
přecházejí	přecházet	k5eAaImIp3nP	přecházet
v	v	k7c4	v
pás	pás	k1gInSc4	pás
mírných	mírný	k2eAgInPc2d1	mírný
listnatých	listnatý	k2eAgInPc2d1	listnatý
a	a	k8xC	a
smíšených	smíšený	k2eAgInPc2d1	smíšený
lesů	les	k1gInPc2	les
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
listnatými	listnatý	k2eAgInPc7d1	listnatý
lesy	les	k1gInPc7	les
leží	ležet	k5eAaImIp3nP	ležet
subalpínské	subalpínský	k2eAgInPc1d1	subalpínský
jehličnaté	jehličnatý	k2eAgInPc1d1	jehličnatý
lesy	les	k1gInPc1	les
a	a	k8xC	a
nad	nad	k7c7	nad
nimi	on	k3xPp3gInPc7	on
alpínské	alpínský	k2eAgInPc1d1	alpínský
keře	keř	k1gInPc1	keř
a	a	k8xC	a
louky	louka	k1gFnPc1	louka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
polohách	poloha	k1gFnPc6	poloha
přecházejí	přecházet	k5eAaImIp3nP	přecházet
v	v	k7c4	v
tundru	tundra	k1gFnSc4	tundra
<g/>
.	.	kIx.	.
</s>
<s>
Alpínské	alpínský	k2eAgFnPc1d1	alpínská
louky	louka	k1gFnPc1	louka
jsou	být	k5eAaImIp3nP	být
letním	letní	k2eAgInSc7d1	letní
domovem	domov	k1gInSc7	domov
ohroženého	ohrožený	k2eAgInSc2d1	ohrožený
irbise	irbise	k1gFnSc2	irbise
(	(	kIx(	(
<g/>
levharta	levhart	k1gMnSc2	levhart
sněžného	sněžný	k2eAgMnSc2d1	sněžný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
prvohorního	prvohorní	k2eAgInSc2d1	prvohorní
kontinentu	kontinent	k1gInSc2	kontinent
Pangea	Pangea	k1gFnSc1	Pangea
na	na	k7c4	na
několik	několik	k4yIc4	několik
litosférických	litosférický	k2eAgFnPc2d1	litosférická
desek	deska	k1gFnPc2	deska
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
dva	dva	k4xCgInPc1	dva
kontinenty	kontinent	k1gInPc1	kontinent
-	-	kIx~	-
Laurasie	Laurasie	k1gFnSc1	Laurasie
a	a	k8xC	a
Gondwana	Gondwana	k1gFnSc1	Gondwana
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
rozpadaly	rozpadat	k5eAaImAgFnP	rozpadat
<g/>
.	.	kIx.	.
</s>
<s>
Indická	indický	k2eAgFnSc1d1	indická
deska	deska	k1gFnSc1	deska
se	se	k3xPyFc4	se
oddělila	oddělit	k5eAaPmAgFnS	oddělit
od	od	k7c2	od
Africké	africký	k2eAgFnSc2d1	africká
a	a	k8xC	a
postupovala	postupovat	k5eAaImAgFnS	postupovat
na	na	k7c4	na
sever	sever	k1gInSc4	sever
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
kolize	kolize	k1gFnSc2	kolize
s	s	k7c7	s
Euroasijskou	euroasijský	k2eAgFnSc7d1	euroasijská
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
kterou	který	k3yQgFnSc7	který
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgNnP	začít
zasouvat	zasouvat	k5eAaImF	zasouvat
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
samotnou	samotný	k2eAgFnSc7d1	samotná
srážkou	srážka	k1gFnSc7	srážka
se	se	k3xPyFc4	se
ve	v	k7c6	v
změlčujícím	změlčující	k2eAgMnSc6d1	změlčující
se	se	k3xPyFc4	se
moři	moře	k1gNnSc6	moře
utvářely	utvářet	k5eAaImAgInP	utvářet
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
zvětšovaly	zvětšovat	k5eAaImAgFnP	zvětšovat
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
vlivem	vliv	k1gInSc7	vliv
zasouvání	zasouvání	k1gNnSc2	zasouvání
indické	indický	k2eAgFnSc2d1	indická
desky	deska	k1gFnSc2	deska
ta	ten	k3xDgFnSc1	ten
euroasijská	euroasijský	k2eAgFnSc1d1	euroasijská
vyzdvihovala	vyzdvihovat	k5eAaImAgFnS	vyzdvihovat
vzhůru	vzhůru	k6eAd1	vzhůru
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgMnS	začít
utvářet	utvářet	k5eAaImF	utvářet
horský	horský	k2eAgInSc4d1	horský
masiv	masiv	k1gInSc4	masiv
Himálaje	Himálaj	k1gFnSc2	Himálaj
a	a	k8xC	a
Karákoramu	Karákoram	k1gInSc2	Karákoram
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
proces	proces	k1gInSc1	proces
trval	trvat	k5eAaImAgInS	trvat
milióny	milión	k4xCgInPc7	milión
let	léto	k1gNnPc2	léto
a	a	k8xC	a
stále	stále	k6eAd1	stále
ještě	ještě	k6eAd1	ještě
trvá	trvat	k5eAaImIp3nS	trvat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
kdysi	kdysi	k6eAd1	kdysi
bylo	být	k5eAaImAgNnS	být
i	i	k9	i
na	na	k7c6	na
území	území	k1gNnSc6	území
Himálají	Himálaj	k1gFnPc2	Himálaj
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
dost	dost	k6eAd1	dost
dobře	dobře	k6eAd1	dobře
dokázat	dokázat	k5eAaPmF	dokázat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
Himálají	Himálaj	k1gFnPc2	Himálaj
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
zkamenělých	zkamenělý	k2eAgMnPc2d1	zkamenělý
mořských	mořský	k2eAgMnPc2d1	mořský
živočichů	živočich	k1gMnPc2	živočich
amonitů	amonit	k1gInPc2	amonit
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
přes	přes	k7c4	přes
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejbližší	blízký	k2eAgNnSc4d3	nejbližší
moře	moře	k1gNnSc4	moře
je	být	k5eAaImIp3nS	být
Bengálský	bengálský	k2eAgInSc1d1	bengálský
záliv	záliv	k1gInSc1	záliv
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c4	o
existenci	existence	k1gFnSc4	existence
moře	moře	k1gNnSc2	moře
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
dnešních	dnešní	k2eAgFnPc2d1	dnešní
velehor	velehora	k1gFnPc2	velehora
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
další	další	k2eAgInPc4d1	další
důkazy	důkaz	k1gInPc4	důkaz
o	o	k7c4	o
existenci	existence	k1gFnSc4	existence
dávného	dávný	k2eAgNnSc2d1	dávné
moře	moře	k1gNnSc2	moře
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
výškách	výška	k1gFnPc6	výška
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odškrábnutí	odškrábnutí	k1gNnSc6	odškrábnutí
vrstvy	vrstva	k1gFnSc2	vrstva
naneseného	nanesený	k2eAgNnSc2d1	nanesené
zkamenělého	zkamenělý	k2eAgNnSc2d1	zkamenělé
bahna	bahno	k1gNnSc2	bahno
lze	lze	k6eAd1	lze
jasně	jasně	k6eAd1	jasně
a	a	k8xC	a
zřetelně	zřetelně	k6eAd1	zřetelně
pozorovat	pozorovat	k5eAaImF	pozorovat
profil	profil	k1gInSc4	profil
mořských	mořský	k2eAgFnPc2d1	mořská
vlnek	vlnka	k1gFnPc2	vlnka
otištěný	otištěný	k2eAgMnSc1d1	otištěný
ve	v	k7c6	v
zkamenělém	zkamenělý	k2eAgInSc6d1	zkamenělý
písku	písek	k1gInSc6	písek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tropických	tropický	k2eAgNnPc6d1	tropické
mořích	moře	k1gNnPc6	moře
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
také	také	k9	také
takzvané	takzvaný	k2eAgInPc4d1	takzvaný
stromatolity	stromatolit	k1gInPc4	stromatolit
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
stromatolity	stromatolit	k1gInPc1	stromatolit
se	se	k3xPyFc4	se
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
z	z	k7c2	z
organismů	organismus	k1gInPc2	organismus
zanesených	zanesený	k2eAgFnPc2d1	zanesená
vápnitými	vápnitý	k2eAgInPc7d1	vápnitý
kaly	kal	k1gInPc7	kal
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
tak	tak	k9	tak
usazenina	usazenina	k1gFnSc1	usazenina
typická	typický	k2eAgFnSc1d1	typická
hlavně	hlavně	k9	hlavně
pro	pro	k7c4	pro
mělká	mělký	k2eAgNnPc4d1	mělké
prvohorní	prvohorní	k2eAgNnPc4d1	prvohorní
moře	moře	k1gNnSc4	moře
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
se	se	k3xPyFc4	se
však	však	k9	však
stromatolity	stromatolit	k1gInPc1	stromatolit
tvoří	tvořit	k5eAaImIp3nP	tvořit
i	i	k9	i
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
v	v	k7c6	v
Himálaji	Himálaj	k1gFnSc6	Himálaj
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgInPc1	tento
stromatolity	stromatolit	k1gInPc1	stromatolit
různě	různě	k6eAd1	různě
přetočeny	přetočen	k2eAgInPc1d1	přetočen
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
svědectví	svědectví	k1gNnSc4	svědectví
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
velkému	velký	k2eAgInSc3d1	velký
tlaku	tlak	k1gInSc3	tlak
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgFnPc1	tento
horniny	hornina	k1gFnPc1	hornina
vystaveny	vystavit	k5eAaPmNgFnP	vystavit
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
takto	takto	k6eAd1	takto
deformuje	deformovat	k5eAaImIp3nS	deformovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vodstvo	vodstvo	k1gNnSc1	vodstvo
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Řeky	Řek	k1gMnPc4	Řek
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Himálaji	Himálaj	k1gFnSc6	Himálaj
pramení	pramenit	k5eAaImIp3nS	pramenit
tři	tři	k4xCgInPc1	tři
ze	z	k7c2	z
světových	světový	k2eAgInPc2d1	světový
hlavních	hlavní	k2eAgInPc2d1	hlavní
říčních	říční	k2eAgInPc2d1	říční
systémů	systém	k1gInPc2	systém
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Indus	Indus	k1gInSc1	Indus
–	–	k?	–
Řeky	řeka	k1gFnSc2	řeka
na	na	k7c6	na
západě	západ	k1gInSc6	západ
Himálají	Himálaj	k1gFnPc2	Himálaj
tvoří	tvořit	k5eAaImIp3nS	tvořit
povodí	povodí	k1gNnSc1	povodí
řeky	řeka	k1gFnSc2	řeka
Indus	Indus	k1gInSc1	Indus
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
největší	veliký	k2eAgFnSc7d3	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
v	v	k7c6	v
Tibetu	Tibet	k1gInSc6	Tibet
soutokem	soutok	k1gInSc7	soutok
zdrojnic	zdrojnice	k1gFnPc2	zdrojnice
Sengge	Sengge	k1gFnPc2	Sengge
a	a	k8xC	a
Gar	Gar	k1gFnPc2	Gar
<g/>
,	,	kIx,	,
teče	téct	k5eAaImIp3nS	téct
na	na	k7c4	na
jihozápad	jihozápad	k1gInSc4	jihozápad
přes	přes	k7c4	přes
Pákistán	Pákistán	k1gInSc4	Pákistán
a	a	k8xC	a
vlévá	vlévat	k5eAaImIp3nS	vlévat
se	se	k3xPyFc4	se
do	do	k7c2	do
Arabského	arabský	k2eAgNnSc2d1	arabské
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Vlévají	vlévat	k5eAaImIp3nP	vlévat
se	se	k3xPyFc4	se
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
mimo	mimo	k6eAd1	mimo
jiné	jiný	k2eAgFnSc2d1	jiná
řeky	řeka	k1gFnSc2	řeka
Dželam	Dželam	k1gInSc1	Dželam
<g/>
,	,	kIx,	,
Čanáb	Čanáb	k1gInSc1	Čanáb
<g/>
,	,	kIx,	,
Ráví	Ráví	k1gFnSc1	Ráví
<g/>
,	,	kIx,	,
Beas	Beas	k1gInSc1	Beas
a	a	k8xC	a
Satledž	Satledž	k1gFnSc1	Satledž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ganga-Brahmaputra	Ganga-Brahmaputra	k1gFnSc1	Ganga-Brahmaputra
–	–	k?	–
Do	do	k7c2	do
tohoto	tento	k3xDgNnSc2	tento
povodí	povodí	k1gNnSc2	povodí
patří	patřit	k5eAaImIp3nS	patřit
většina	většina	k1gFnSc1	většina
himálajských	himálajský	k2eAgFnPc2d1	himálajská
řek	řeka	k1gFnPc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc7	jeho
hlavními	hlavní	k2eAgFnPc7d1	hlavní
řekami	řeka	k1gFnPc7	řeka
jsou	být	k5eAaImIp3nP	být
Ganga	Ganga	k1gFnSc1	Ganga
<g/>
,	,	kIx,	,
Brahmaputra	Brahmaputra	k1gFnSc1	Brahmaputra
a	a	k8xC	a
Jamuna	Jamuna	k1gFnSc1	Jamuna
<g/>
.	.	kIx.	.
</s>
<s>
Brahmaputra	Brahmaputra	k1gFnSc1	Brahmaputra
začíná	začínat	k5eAaImIp3nS	začínat
v	v	k7c6	v
západním	západní	k2eAgInSc6d1	západní
Tibetu	Tibet	k1gInSc6	Tibet
jako	jako	k8xS	jako
řeka	řeka	k1gFnSc1	řeka
Jarlung	Jarlunga	k1gFnPc2	Jarlunga
Cangpo	Cangpa	k1gFnSc5	Cangpa
<g/>
,	,	kIx,	,
protéká	protékat	k5eAaImIp3nS	protékat
Tibetem	Tibet	k1gInSc7	Tibet
na	na	k7c4	na
východ	východ	k1gInSc4	východ
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
západ	západ	k1gInSc4	západ
Ásámskými	Ásámský	k2eAgFnPc7d1	Ásámský
pláněmi	pláň	k1gFnPc7	pláň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bangladéši	Bangladéš	k1gInSc6	Bangladéš
se	se	k3xPyFc4	se
Ganga	Ganga	k1gFnSc1	Ganga
a	a	k8xC	a
Brahmaputra	Brahmaputrum	k1gNnPc1	Brahmaputrum
setkávají	setkávat	k5eAaImIp3nP	setkávat
a	a	k8xC	a
vlévají	vlévat	k5eAaImIp3nP	vlévat
se	se	k3xPyFc4	se
do	do	k7c2	do
Bengálského	bengálský	k2eAgInSc2d1	bengálský
zálivu	záliv	k1gInSc2	záliv
v	v	k7c6	v
největší	veliký	k2eAgFnSc6d3	veliký
říční	říční	k2eAgFnSc6d1	říční
deltě	delta	k1gFnSc6	delta
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jang-c	Jang	k1gFnSc1	Jang-c
<g/>
'	'	kIx"	'
<g/>
-ťiang	-ťiang	k1gInSc1	-ťiang
–	–	k?	–
Řeky	řeka	k1gFnSc2	řeka
Jang-c	Jang	k1gFnSc1	Jang-c
<g/>
'	'	kIx"	'
<g/>
-ťiang	-ťiang	k1gInSc1	-ťiang
<g/>
,	,	kIx,	,
Mekong	Mekong	k1gInSc1	Mekong
<g/>
,	,	kIx,	,
Salwin	Salwin	k1gInSc1	Salwin
a	a	k8xC	a
Žlutá	žlutý	k2eAgFnSc1d1	žlutá
řeka	řeka	k1gFnSc1	řeka
pramení	pramenit	k5eAaImIp3nS	pramenit
v	v	k7c6	v
Tibetské	tibetský	k2eAgFnSc6d1	tibetská
náhorní	náhorní	k2eAgFnSc6d1	náhorní
plošině	plošina	k1gFnSc6	plošina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
geologicky	geologicky	k6eAd1	geologicky
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
Himálaje	Himálaj	k1gFnSc2	Himálaj
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
nejsou	být	k5eNaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
skutečné	skutečný	k2eAgFnPc4d1	skutečná
himálajské	himálajský	k2eAgFnPc4d1	himálajská
řeky	řeka	k1gFnPc4	řeka
<g/>
.	.	kIx.	.
<g/>
Řeky	Řek	k1gMnPc4	Řek
v	v	k7c6	v
nejvýchodnějších	východní	k2eAgFnPc6d3	nejvýchodnější
částech	část	k1gFnPc6	část
Himálaje	Himálaj	k1gFnSc2	Himálaj
se	se	k3xPyFc4	se
vlévají	vlévat	k5eAaImIp3nP	vlévat
do	do	k7c2	do
řeky	řeka	k1gFnSc2	řeka
Iravádí	Iravádí	k1gNnSc2	Iravádí
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
pramení	pramenit	k5eAaImIp3nS	pramenit
ve	v	k7c6	v
východním	východní	k2eAgInSc6d1	východní
Tibetu	Tibet	k1gInSc6	Tibet
<g/>
,	,	kIx,	,
teče	téct	k5eAaImIp3nS	téct
na	na	k7c4	na
jih	jih	k1gInSc4	jih
skrz	skrz	k7c4	skrz
Myanmar	Myanmar	k1gInSc4	Myanmar
a	a	k8xC	a
vlévá	vlévat	k5eAaImIp3nS	vlévat
se	se	k3xPyFc4	se
do	do	k7c2	do
Andamanského	Andamanský	k2eAgNnSc2d1	Andamanské
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
odhadu	odhad	k1gInSc2	odhad
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
himálajských	himálajský	k2eAgFnPc2d1	himálajská
řek	řeka	k1gFnPc2	řeka
750	[number]	k4	750
miliónů	milión	k4xCgInPc2	milión
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jiného	jiný	k2eAgInSc2d1	jiný
zdroje	zdroj	k1gInSc2	zdroj
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
až	až	k9	až
o	o	k7c4	o
1,3	[number]	k4	1,3
miliardy	miliarda	k4xCgFnSc2	miliarda
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Jezera	jezero	k1gNnSc2	jezero
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Himálaji	Himálaj	k1gFnSc6	Himálaj
se	se	k3xPyFc4	se
nalézají	nalézat	k5eAaImIp3nP	nalézat
stovky	stovka	k1gFnPc1	stovka
jezer	jezero	k1gNnPc2	jezero
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
5	[number]	k4	5
000	[number]	k4	000
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgNnSc1d3	veliký
jezero	jezero	k1gNnSc1	jezero
je	být	k5eAaImIp3nS	být
Pangong	Pangong	k1gInSc4	Pangong
Tso	Tso	k1gFnPc2	Tso
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
leží	ležet	k5eAaImIp3nP	ležet
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
mezi	mezi	k7c7	mezi
Indií	Indie	k1gFnSc7	Indie
a	a	k8xC	a
Tibetem	Tibet	k1gInSc7	Tibet
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
situováno	situován	k2eAgNnSc1d1	situováno
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
4	[number]	k4	4
600	[number]	k4	600
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
8	[number]	k4	8
km	km	kA	km
široké	široký	k2eAgNnSc1d1	široké
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
134	[number]	k4	134
km	km	kA	km
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
<g/>
.	.	kIx.	.
</s>
<s>
Jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejvyšších	vysoký	k2eAgNnPc2d3	nejvyšší
jezer	jezero	k1gNnPc2	jezero
je	být	k5eAaImIp3nS	být
Gurudogmar	Gurudogmar	k1gInSc1	Gurudogmar
<g/>
,	,	kIx,	,
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
5	[number]	k4	5
148	[number]	k4	148
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1	další
velkými	velký	k2eAgMnPc7d1	velký
jezery	jezero	k1gNnPc7	jezero
jsou	být	k5eAaImIp3nP	být
jezero	jezero	k1gNnSc1	jezero
Tsongmo	Tsongma	k1gFnSc5	Tsongma
poblíž	poblíž	k6eAd1	poblíž
indicko-tibetské	indickoibetský	k2eAgFnPc4d1	indicko-tibetský
hranice	hranice	k1gFnPc4	hranice
v	v	k7c6	v
Sikkimu	Sikkim	k1gInSc6	Sikkim
a	a	k8xC	a
jezero	jezero	k1gNnSc1	jezero
Tilicho	Tilicha	k1gFnSc5	Tilicha
v	v	k7c6	v
Nepálu	Nepál	k1gInSc6	Nepál
v	v	k7c6	v
masívu	masív	k1gInSc6	masív
Annapurna	Annapurno	k1gNnSc2	Annapurno
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
až	až	k9	až
donedávna	donedávna	k6eAd1	donedávna
pro	pro	k7c4	pro
turisty	turist	k1gMnPc4	turist
uzavřena	uzavřen	k2eAgFnSc1d1	uzavřena
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
výše	vysoce	k6eAd2	vysoce
položených	položený	k2eAgFnPc6d1	položená
oblastech	oblast	k1gFnPc6	oblast
(	(	kIx(	(
<g/>
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
nad	nad	k7c7	nad
5	[number]	k4	5
500	[number]	k4	500
m	m	kA	m
<g/>
)	)	kIx)	)
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaBmF	nalézt
horská	horský	k2eAgNnPc4d1	horské
plesa	pleso	k1gNnPc4	pleso
<g/>
,	,	kIx,	,
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
činností	činnost	k1gFnSc7	činnost
ledovců	ledovec	k1gInPc2	ledovec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ledovce	ledovec	k1gInPc1	ledovec
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Himálaji	Himálaj	k1gFnSc6	Himálaj
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
přibližně	přibližně	k6eAd1	přibližně
15	[number]	k4	15
000	[number]	k4	000
ledovců	ledovec	k1gInPc2	ledovec
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
přibližně	přibližně	k6eAd1	přibližně
12	[number]	k4	12
000	[number]	k4	000
km3	km3	k4	km3
čisté	čistý	k2eAgFnSc2d1	čistá
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Ledovec	ledovec	k1gInSc4	ledovec
Siačen	Siačen	k2eAgInSc4d1	Siačen
<g/>
,	,	kIx,	,
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
70	[number]	k4	70
km	km	kA	km
a	a	k8xC	a
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
Indie	Indie	k1gFnSc2	Indie
a	a	k8xC	a
Pákistánu	Pákistán	k1gInSc2	Pákistán
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
druhý	druhý	k4xOgInSc1	druhý
nejdelší	dlouhý	k2eAgInSc1d3	nejdelší
ledovec	ledovec	k1gInSc1	ledovec
na	na	k7c6	na
světě	svět	k1gInSc6	svět
mimo	mimo	k7c4	mimo
polární	polární	k2eAgFnPc4d1	polární
oblasti	oblast	k1gFnPc4	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgInPc4d1	další
známější	známý	k2eAgInPc4d2	známější
ledovce	ledovec	k1gInPc4	ledovec
patří	patřit	k5eAaImIp3nP	patřit
Gangotri	Gangotr	k1gMnPc1	Gangotr
a	a	k8xC	a
Yamunotri	Yamunotr	k1gMnPc1	Yamunotr
(	(	kIx(	(
<g/>
Uttarakhand	Uttarakhand	k1gInSc1	Uttarakhand
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nubra	Nubra	k1gMnSc1	Nubra
<g/>
,	,	kIx,	,
Biafo	Biafo	k1gMnSc1	Biafo
a	a	k8xC	a
Baltoro	Baltora	k1gFnSc5	Baltora
(	(	kIx(	(
<g/>
oblast	oblast	k1gFnSc1	oblast
Karakoram	Karakoram	k1gInSc1	Karakoram
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Zemu	Zema	k1gFnSc4	Zema
(	(	kIx(	(
<g/>
Sikkim	Sikkim	k1gInSc4	Sikkim
<g/>
)	)	kIx)	)
a	a	k8xC	a
Khumbu	Khumba	k1gFnSc4	Khumba
(	(	kIx(	(
<g/>
oblast	oblast	k1gFnSc1	oblast
Mount	Mount	k1gInSc1	Mount
Everestu	Everest	k1gInSc2	Everest
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
vědci	vědec	k1gMnPc1	vědec
zaznamenali	zaznamenat	k5eAaPmAgMnP	zaznamenat
zvýšenou	zvýšený	k2eAgFnSc4d1	zvýšená
rychlost	rychlost	k1gFnSc4	rychlost
ústupu	ústup	k1gInSc2	ústup
ledovců	ledovec	k1gInPc2	ledovec
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
důsledkem	důsledek	k1gInSc7	důsledek
globálních	globální	k2eAgFnPc2d1	globální
klimatických	klimatický	k2eAgFnPc2d1	klimatická
změn	změna	k1gFnPc2	změna
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
důsledky	důsledek	k1gInPc1	důsledek
tohoto	tento	k3xDgInSc2	tento
jevu	jev	k1gInSc2	jev
se	se	k3xPyFc4	se
nemusejí	muset	k5eNaImIp3nP	muset
projevit	projevit	k5eAaPmF	projevit
mnoho	mnoho	k4c4	mnoho
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
znamenat	znamenat	k5eAaImF	znamenat
katastrofu	katastrofa	k1gFnSc4	katastrofa
pro	pro	k7c4	pro
stovky	stovka	k1gFnPc4	stovka
miliónů	milión	k4xCgInPc2	milión
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Indii	Indie	k1gFnSc6	Indie
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
období	období	k1gNnSc6	období
sucha	sucho	k1gNnSc2	sucho
závislí	závislý	k2eAgMnPc1d1	závislý
na	na	k7c6	na
vodě	voda	k1gFnSc6	voda
v	v	k7c6	v
řekách	řeka	k1gFnPc6	řeka
pocházející	pocházející	k2eAgInSc1d1	pocházející
právě	právě	k9	právě
z	z	k7c2	z
ledovců	ledovec	k1gInPc2	ledovec
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
klimatické	klimatický	k2eAgFnSc2d1	klimatická
zprávy	zpráva	k1gFnSc2	zpráva
OSN	OSN	kA	OSN
mohou	moct	k5eAaImIp3nP	moct
himálajské	himálajský	k2eAgInPc1d1	himálajský
ledovce	ledovec	k1gInPc1	ledovec
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
zdrojem	zdroj	k1gInSc7	zdroj
vody	voda	k1gFnSc2	voda
největších	veliký	k2eAgFnPc2d3	veliký
asijských	asijský	k2eAgFnPc2d1	asijská
řek	řeka	k1gFnPc2	řeka
<g/>
,	,	kIx,	,
zmizet	zmizet	k5eAaPmF	zmizet
vlivem	vliv	k1gInSc7	vliv
růstu	růst	k1gInSc2	růst
teplot	teplota	k1gFnPc2	teplota
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2350	[number]	k4	2350
a	a	k8xC	a
Indie	Indie	k1gFnSc1	Indie
<g/>
,	,	kIx,	,
Tibet	Tibet	k1gInSc1	Tibet
<g/>
,	,	kIx,	,
Pákistán	Pákistán	k1gInSc1	Pákistán
<g/>
,	,	kIx,	,
Bangladéš	Bangladéš	k1gInSc1	Bangladéš
<g/>
,	,	kIx,	,
Nepál	Nepál	k1gInSc1	Nepál
a	a	k8xC	a
Myanmar	Myanmar	k1gInSc1	Myanmar
tak	tak	k9	tak
mohou	moct	k5eAaImIp3nP	moct
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
desetiletích	desetiletí	k1gNnPc6	desetiletí
čelit	čelit	k5eAaImF	čelit
povodním	povodeň	k1gFnPc3	povodeň
a	a	k8xC	a
rozsáhlým	rozsáhlý	k2eAgNnPc3d1	rozsáhlé
suchům	sucho	k1gNnPc3	sucho
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vliv	vliv	k1gInSc1	vliv
Himálaje	Himálaj	k1gFnSc2	Himálaj
na	na	k7c6	na
podnebí	podnebí	k1gNnSc6	podnebí
==	==	k?	==
</s>
</p>
<p>
<s>
Himálaj	Himálaj	k1gFnSc1	Himálaj
má	mít	k5eAaImIp3nS	mít
významný	významný	k2eAgInSc4d1	významný
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
podnebí	podnebí	k1gNnSc4	podnebí
na	na	k7c6	na
Indickém	indický	k2eAgInSc6d1	indický
subkontinentu	subkontinent	k1gInSc6	subkontinent
a	a	k8xC	a
Tibetské	tibetský	k2eAgFnSc3d1	tibetská
náhorní	náhorní	k2eAgFnSc3d1	náhorní
plošině	plošina	k1gFnSc3	plošina
<g/>
.	.	kIx.	.
</s>
<s>
Znemožňuje	znemožňovat	k5eAaImIp3nS	znemožňovat
mrazivým	mrazivý	k2eAgInPc3d1	mrazivý
a	a	k8xC	a
suchým	suchý	k2eAgInPc3d1	suchý
arktickým	arktický	k2eAgInPc3d1	arktický
větrům	vítr	k1gInPc3	vítr
vanout	vanout	k5eAaImF	vanout
na	na	k7c4	na
jih	jih	k1gInSc4	jih
tohoto	tento	k3xDgInSc2	tento
subkontinentu	subkontinent	k1gInSc2	subkontinent
<g/>
,	,	kIx,	,
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
čehož	což	k3yQnSc2	což
je	být	k5eAaImIp3nS	být
klima	klima	k1gNnSc1	klima
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Asii	Asie	k1gFnSc6	Asie
mnohem	mnohem	k6eAd1	mnohem
teplejší	teplý	k2eAgFnSc6d2	teplejší
než	než	k8xS	než
odpovídající	odpovídající	k2eAgFnSc6d1	odpovídající
oblasti	oblast	k1gFnSc6	oblast
v	v	k7c6	v
mírném	mírný	k2eAgNnSc6d1	mírné
pásmu	pásmo	k1gNnSc6	pásmo
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
částech	část	k1gFnPc6	část
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
brání	bránit	k5eAaImIp3nS	bránit
monzunovým	monzunový	k2eAgInPc3d1	monzunový
větrům	vítr	k1gInPc3	vítr
vanout	vanout	k5eAaImF	vanout
dál	daleko	k6eAd2	daleko
na	na	k7c4	na
sever	sever	k1gInSc4	sever
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
silné	silný	k2eAgInPc4d1	silný
deště	dešť	k1gInPc4	dešť
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Terai	Tera	k1gFnSc2	Tera
<g/>
.	.	kIx.	.
</s>
<s>
Vědci	vědec	k1gMnPc1	vědec
se	se	k3xPyFc4	se
také	také	k9	také
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Himálaj	Himálaj	k1gFnSc1	Himálaj
hraje	hrát	k5eAaImIp3nS	hrát
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
ve	v	k7c6	v
vytvoření	vytvoření	k1gNnSc6	vytvoření
středoasijských	středoasijský	k2eAgFnPc2d1	středoasijská
pouští	poušť	k1gFnPc2	poušť
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Taklamakan	Taklamakan	k1gInSc1	Taklamakan
nebo	nebo	k8xC	nebo
Gobi	Gobi	k1gFnSc1	Gobi
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pohoří	pohoří	k1gNnSc1	pohoří
také	také	k9	také
brání	bránit	k5eAaImIp3nS	bránit
západním	západní	k2eAgFnPc3d1	západní
zimním	zimní	k2eAgFnPc3d1	zimní
poruchám	porucha	k1gFnPc3	porucha
z	z	k7c2	z
Íránu	Írán	k1gInSc2	Írán
proniknout	proniknout	k5eAaPmF	proniknout
dál	daleko	k6eAd2	daleko
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
sníh	sníh	k1gInSc4	sníh
v	v	k7c6	v
Kašmíru	Kašmír	k1gInSc6	Kašmír
a	a	k8xC	a
deště	dešť	k1gInSc2	dešť
v	v	k7c6	v
částech	část	k1gFnPc6	část
Pandžábu	Pandžáb	k1gInSc2	Pandžáb
a	a	k8xC	a
severní	severní	k2eAgFnSc6d1	severní
Indii	Indie	k1gFnSc6	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
údolí	údolí	k1gNnPc1	údolí
Brahmaputry	Brahmaputr	k1gInPc4	Brahmaputr
je	být	k5eAaImIp3nS	být
bariérou	bariéra	k1gFnSc7	bariéra
pro	pro	k7c4	pro
studené	studený	k2eAgInPc4d1	studený
severní	severní	k2eAgInPc4d1	severní
větry	vítr	k1gInPc4	vítr
<g/>
,	,	kIx,	,
část	část	k1gFnSc4	část
mrazivých	mrazivý	k2eAgInPc2d1	mrazivý
větrů	vítr	k1gInPc2	vítr
se	se	k3xPyFc4	se
do	do	k7c2	do
něj	on	k3xPp3gNnSc2	on
dostane	dostat	k5eAaPmIp3nS	dostat
a	a	k8xC	a
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
snížení	snížení	k1gNnSc1	snížení
teploty	teplota	k1gFnSc2	teplota
v	v	k7c6	v
severovýchodních	severovýchodní	k2eAgInPc6d1	severovýchodní
indických	indický	k2eAgInPc6d1	indický
státech	stát	k1gInPc6	stát
a	a	k8xC	a
v	v	k7c6	v
Bangladéši	Bangladéš	k1gInSc6	Bangladéš
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
větry	vítr	k1gInPc1	vítr
také	také	k9	také
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
vyvolávají	vyvolávat	k5eAaImIp3nP	vyvolávat
v	v	k7c6	v
těchto	tento	k3xDgNnPc6	tento
místech	místo	k1gNnPc6	místo
severovýchodní	severovýchodní	k2eAgFnSc1d1	severovýchodní
monzuny	monzun	k1gInPc7	monzun
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tryskové	tryskový	k2eAgNnSc1d1	tryskové
proudění	proudění	k1gNnSc1	proudění
má	mít	k5eAaImIp3nS	mít
vliv	vliv	k1gInSc4	vliv
i	i	k9	i
na	na	k7c4	na
vzhled	vzhled	k1gInSc4	vzhled
himálajských	himálajský	k2eAgInPc2d1	himálajský
vrcholů	vrchol	k1gInPc2	vrchol
<g/>
.	.	kIx.	.
</s>
<s>
Silný	silný	k2eAgInSc1d1	silný
proud	proud	k1gInSc1	proud
větrů	vítr	k1gInPc2	vítr
ze	z	k7c2	z
západu	západ	k1gInSc2	západ
vane	vanout	k5eAaImIp3nS	vanout
kolem	kolem	k7c2	kolem
Everestu	Everest	k1gInSc2	Everest
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
známé	známý	k2eAgInPc4d1	známý
chuchvalce	chuchvalec	k1gInPc4	chuchvalec
sněhu	sníh	k1gInSc2	sníh
vanoucí	vanoucí	k2eAgInSc4d1	vanoucí
z	z	k7c2	z
vrcholu	vrchol	k1gInSc2	vrchol
<g/>
,	,	kIx,	,
viditelné	viditelný	k2eAgFnSc2d1	viditelná
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nejvyšší	vysoký	k2eAgInPc4d3	Nejvyšší
vrcholy	vrchol	k1gInPc4	vrchol
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Himalayas	Himalayasa	k1gFnPc2	Himalayasa
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
hor	hora	k1gFnPc2	hora
</s>
</p>
<p>
<s>
Osmitisícovka	Osmitisícovka	k1gFnSc1	Osmitisícovka
</s>
</p>
<p>
<s>
Karákoram	Karákoram	k1gInSc1	Karákoram
</s>
</p>
<p>
<s>
Yeti	yeti	k1gMnSc1	yeti
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Himálaj	Himálaj	k1gFnSc1	Himálaj
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Lokální	lokální	k2eAgFnSc1d1	lokální
šablona	šablona	k1gFnSc1	šablona
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
jinou	jiný	k2eAgFnSc4d1	jiná
kategorii	kategorie	k1gFnSc4	kategorie
Commons	Commonsa	k1gFnPc2	Commonsa
než	než	k8xS	než
přiřazená	přiřazený	k2eAgFnSc1d1	přiřazená
položka	položka	k1gFnSc1	položka
Wikidat	Wikidat	k1gFnSc2	Wikidat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Lokální	lokální	k2eAgInSc1d1	lokální
odkaz	odkaz	k1gInSc1	odkaz
<g/>
:	:	kIx,	:
Himalaya	Himalaya	k1gFnSc1	Himalaya
</s>
</p>
<p>
<s>
Wikidata	Wikidata	k1gFnSc1	Wikidata
<g/>
:	:	kIx,	:
Himalayas	Himalayas	k1gInSc1	Himalayas
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Himálaj	Himálaj	k1gFnSc1	Himálaj
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Himálaj	Himálaj	k1gFnSc1	Himálaj
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
