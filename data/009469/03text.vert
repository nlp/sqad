<p>
<s>
Pilotní	pilotní	k2eAgFnSc1d1	pilotní
studie	studie	k1gFnSc1	studie
je	být	k5eAaImIp3nS	být
předběžná	předběžný	k2eAgFnSc1d1	předběžná
studie	studie	k1gFnSc1	studie
v	v	k7c6	v
malém	malý	k2eAgNnSc6d1	malé
měřítku	měřítko	k1gNnSc6	měřítko
před	před	k7c7	před
hlavním	hlavní	k2eAgInSc7d1	hlavní
výzkumem	výzkum	k1gInSc7	výzkum
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
ověření	ověření	k1gNnSc1	ověření
proveditelnosti	proveditelnost	k1gFnSc2	proveditelnost
nebo	nebo	k8xC	nebo
vylepšení	vylepšení	k1gNnSc4	vylepšení
plánu	plán	k1gInSc2	plán
výzkumu	výzkum	k1gInSc2	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
Pilotní	pilotní	k2eAgFnSc1d1	pilotní
studie	studie	k1gFnSc1	studie
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
nemusí	muset	k5eNaImIp3nS	muset
hodit	hodit	k5eAaImF	hodit
pro	pro	k7c4	pro
případové	případový	k2eAgFnPc4d1	Případová
studie	studie	k1gFnPc4	studie
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
studie	studie	k1gFnSc1	studie
provádí	provádět	k5eAaImIp3nS	provádět
před	před	k7c7	před
rozsáhlým	rozsáhlý	k2eAgInSc7d1	rozsáhlý
kvantitativním	kvantitativní	k2eAgInSc7d1	kvantitativní
výzkumem	výzkum	k1gInSc7	výzkum
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
ušetřit	ušetřit	k5eAaPmF	ušetřit
čas	čas	k1gInSc4	čas
a	a	k8xC	a
peníze	peníz	k1gInPc4	peníz
za	za	k7c4	za
nesprávně	správně	k6eNd1	správně
navržený	navržený	k2eAgInSc4d1	navržený
projekt	projekt	k1gInSc4	projekt
<g/>
.	.	kIx.	.
</s>
<s>
Pilotní	pilotní	k2eAgFnPc1d1	pilotní
studie	studie	k1gFnPc1	studie
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
provádějí	provádět	k5eAaImIp3nP	provádět
na	na	k7c6	na
relevantním	relevantní	k2eAgInSc6d1	relevantní
vzorku	vzorek	k1gInSc6	vzorek
populace	populace	k1gFnSc2	populace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
na	na	k7c6	na
těch	ten	k3xDgMnPc6	ten
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
budou	být	k5eAaImBp3nP	být
tvořit	tvořit	k5eAaImF	tvořit
část	část	k1gFnSc4	část
konečného	konečný	k2eAgInSc2d1	konečný
vzorku	vzorek	k1gInSc2	vzorek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pilot	pilot	k1gMnSc1	pilot
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
používán	používat	k5eAaImNgInS	používat
k	k	k7c3	k
otestování	otestování	k1gNnSc3	otestování
návrhu	návrh	k1gInSc2	návrh
úplného	úplný	k2eAgInSc2d1	úplný
experimentu	experiment	k1gInSc2	experiment
<g/>
.	.	kIx.	.
</s>
<s>
Konstrukce	konstrukce	k1gFnSc1	konstrukce
experimentu	experiment	k1gInSc2	experiment
pak	pak	k6eAd1	pak
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
upravena	upravit	k5eAaPmNgFnS	upravit
v	v	k7c6	v
čase	čas	k1gInSc6	čas
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
ukázat	ukázat	k5eAaPmF	ukázat
jako	jako	k9	jako
cenné	cenný	k2eAgNnSc1d1	cenné
<g/>
:	:	kIx,	:
co	co	k3yRnSc1	co
chybí	chybět	k5eAaImIp3nS	chybět
v	v	k7c6	v
pilotu	pilot	k1gInSc6	pilot
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
přidáno	přidat	k5eAaPmNgNnS	přidat
do	do	k7c2	do
experimentu	experiment	k1gInSc2	experiment
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
velký	velký	k2eAgInSc1d1	velký
(	(	kIx(	(
<g/>
a	a	k8xC	a
dražší	drahý	k2eAgMnSc1d2	dražší
<g/>
)	)	kIx)	)
experiment	experiment	k1gInSc1	experiment
nebude	být	k5eNaImBp3nS	být
muset	muset	k5eAaImF	muset
být	být	k5eAaImF	být
opakován	opakovat	k5eAaImNgMnS	opakovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Často	často	k6eAd1	často
v	v	k7c6	v
technických	technický	k2eAgFnPc6d1	technická
aplikacích	aplikace	k1gFnPc6	aplikace
pilotní	pilotní	k2eAgInPc1d1	pilotní
experimenty	experiment	k1gInPc1	experiment
slouží	sloužit	k5eAaImIp3nP	sloužit
k	k	k7c3	k
prodeji	prodej	k1gInSc3	prodej
výrobků	výrobek	k1gInPc2	výrobek
<g/>
,	,	kIx,	,
a	a	k8xC	a
poskytnou	poskytnout	k5eAaPmIp3nP	poskytnout
kvantitativní	kvantitativní	k2eAgInSc4d1	kvantitativní
důkaz	důkaz	k1gInSc4	důkaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
systém	systém	k1gInSc1	systém
má	mít	k5eAaImIp3nS	mít
potenciál	potenciál	k1gInSc4	potenciál
uspět	uspět	k5eAaPmF	uspět
ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
měřítku	měřítko	k1gNnSc6	měřítko
<g/>
.	.	kIx.	.
</s>
<s>
Pilotní	pilotní	k2eAgInPc1d1	pilotní
experimenty	experiment	k1gInPc1	experiment
se	se	k3xPyFc4	se
také	také	k9	také
používají	používat	k5eAaImIp3nP	používat
ke	k	k7c3	k
snížení	snížení	k1gNnSc3	snížení
nákladů	náklad	k1gInPc2	náklad
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jsou	být	k5eAaImIp3nP	být
mnohem	mnohem	k6eAd1	mnohem
levnější	levný	k2eAgFnPc1d2	levnější
než	než	k8xS	než
řádný	řádný	k2eAgInSc1d1	řádný
experiment	experiment	k1gInSc1	experiment
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
není	být	k5eNaImIp3nS	být
dostatečný	dostatečný	k2eAgInSc4d1	dostatečný
důvod	důvod	k1gInSc4	důvod
k	k	k7c3	k
poskytování	poskytování	k1gNnSc3	poskytování
aplikací	aplikace	k1gFnPc2	aplikace
v	v	k7c6	v
plném	plný	k2eAgInSc6d1	plný
rozsahu	rozsah	k1gInSc6	rozsah
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
piloti	pilot	k1gMnPc1	pilot
tento	tento	k3xDgInSc4	tento
důkaz	důkaz	k1gInSc4	důkaz
poskytnout	poskytnout	k5eAaPmF	poskytnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
sociologii	sociologie	k1gFnSc6	sociologie
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
na	na	k7c4	na
pilotní	pilotní	k2eAgFnPc4d1	pilotní
studie	studie	k1gFnPc4	studie
odkazováno	odkazován	k2eAgNnSc1d1	odkazováno
jako	jako	k9	jako
na	na	k7c4	na
výzkum	výzkum	k1gInSc4	výzkum
v	v	k7c6	v
malém	malý	k2eAgNnSc6d1	malé
měřítku	měřítko	k1gNnSc6	měřítko
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
není	být	k5eNaImIp3nS	být
proveden	proveden	k2eAgInSc1d1	proveden
řádný	řádný	k2eAgInSc1d1	řádný
výzkum	výzkum	k1gInSc1	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
zabrání	zabránit	k5eAaPmIp3nS	zabránit
chybám	chyba	k1gFnPc3	chyba
v	v	k7c6	v
hlavním	hlavní	k2eAgInSc6d1	hlavní
výzkumu	výzkum	k1gInSc6	výzkum
<g/>
.	.	kIx.	.
</s>
</p>
