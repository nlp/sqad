<p>
<s>
Skalní	skalní	k2eAgInSc1d1	skalní
hrad	hrad	k1gInSc1	hrad
Svojkov	Svojkov	k1gInSc1	Svojkov
stával	stávat	k5eAaImAgInS	stávat
na	na	k7c6	na
severním	severní	k2eAgInSc6d1	severní
okraji	okraj	k1gInSc6	okraj
obce	obec	k1gFnSc2	obec
Svojkov	Svojkov	k1gInSc1	Svojkov
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
romantická	romantický	k2eAgFnSc1d1	romantická
zřícenina	zřícenina	k1gFnSc1	zřícenina
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Česká	český	k2eAgFnSc1d1	Česká
Lípa	lípa	k1gFnSc1	lípa
u	u	k7c2	u
obce	obec	k1gFnSc2	obec
Svojkov	Svojkov	k1gInSc4	Svojkov
mezi	mezi	k7c7	mezi
Zákupy	zákup	k1gInPc7	zákup
a	a	k8xC	a
Novým	nový	k2eAgInSc7d1	nový
Borem	bor	k1gInSc7	bor
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
chráněn	chránit	k5eAaImNgInS	chránit
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
jako	jako	k8xS	jako
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
pod	pod	k7c7	pod
číslem	číslo	k1gNnSc7	číslo
35114	[number]	k4	35114
<g/>
/	/	kIx~	/
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
3325	[number]	k4	3325
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Existence	existence	k1gFnSc1	existence
hradu	hrad	k1gInSc2	hrad
byla	být	k5eAaImAgFnS	být
doložena	doložit	k5eAaPmNgFnS	doložit
poprvé	poprvé	k6eAd1	poprvé
roku	rok	k1gInSc2	rok
1355	[number]	k4	1355
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarším	starý	k2eAgMnSc7d3	nejstarší
známým	známý	k2eAgMnSc7d1	známý
držitelem	držitel	k1gMnSc7	držitel
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1370	[number]	k4	1370
Ješek	Ješko	k1gNnPc2	Ješko
<g/>
,	,	kIx,	,
zřejmě	zřejmě	k6eAd1	zřejmě
man	mana	k1gFnPc2	mana
Berků	Berka	k1gMnPc2	Berka
v	v	k7c6	v
Dubé	Dubá	k1gFnSc6	Dubá
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
vlastníkem	vlastník	k1gMnSc7	vlastník
také	také	k9	také
Jan	Jan	k1gMnSc1	Jan
z	z	k7c2	z
Chlumu	chlum	k1gInSc2	chlum
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xS	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
průvodců	průvodce	k1gMnPc2	průvodce
Jana	Jan	k1gMnSc2	Jan
Husa	Hus	k1gMnSc2	Hus
do	do	k7c2	do
Kostnice	Kostnice	k1gFnSc2	Kostnice
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1437	[number]	k4	1437
se	s	k7c7	s
držitelem	držitel	k1gMnSc7	držitel
hradu	hrad	k1gInSc2	hrad
stal	stát	k5eAaPmAgMnS	stát
Ctibor	Ctibor	k1gMnSc1	Ctibor
z	z	k7c2	z
Kačice	kačice	k1gFnSc2	kačice
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
vladycký	vladycký	k2eAgInSc1d1	vladycký
rod	rod	k1gInSc1	rod
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
začal	začít	k5eAaPmAgInS	začít
užívat	užívat	k5eAaImF	užívat
predikát	predikát	k1gInSc1	predikát
ze	z	k7c2	z
Svojkova	Svojkov	k1gInSc2	Svojkov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1444	[number]	k4	1444
-	-	kIx~	-
1445	[number]	k4	1445
se	se	k3xPyFc4	se
sídla	sídlo	k1gNnSc2	sídlo
zmocnil	zmocnit	k5eAaPmAgMnS	zmocnit
loupeživý	loupeživý	k2eAgMnSc1d1	loupeživý
rytíř	rytíř	k1gMnSc1	rytíř
Mikeš	Mikeš	k1gMnSc1	Mikeš
Pancíř	pancíř	k1gInSc4	pancíř
ze	z	k7c2	z
Smojna	Smojn	k1gInSc2	Smojn
a	a	k8xC	a
při	při	k7c6	při
odvetném	odvetný	k2eAgNnSc6d1	odvetné
tažení	tažení	k1gNnSc6	tažení
vojsk	vojsko	k1gNnPc2	vojsko
lužických	lužický	k2eAgNnPc2d1	Lužické
měst	město	k1gNnPc2	město
proti	proti	k7c3	proti
němu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgInS	být
hrad	hrad	k1gInSc1	hrad
dobyt	dobýt	k5eAaPmNgInS	dobýt
a	a	k8xC	a
zapálen	zapálit	k5eAaPmNgInS	zapálit
<g/>
.	.	kIx.	.
</s>
<s>
Stejný	stejný	k2eAgInSc1d1	stejný
osud	osud	k1gInSc1	osud
potkal	potkat	k5eAaPmAgInS	potkat
další	další	k2eAgInSc1d1	další
z	z	k7c2	z
Pancířových	pancířový	k2eAgInPc2d1	pancířový
hradů	hrad	k1gInPc2	hrad
nedaleký	daleký	k2eNgInSc4d1	nedaleký
Sloup	sloup	k1gInSc4	sloup
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1530	[number]	k4	1530
byl	být	k5eAaImAgInS	být
hrad	hrad	k1gInSc1	hrad
opuštěn	opustit	k5eAaPmNgInS	opustit
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
200	[number]	k4	200
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
pozemky	pozemka	k1gFnSc2	pozemka
koupil	koupit	k5eAaPmAgMnS	koupit
hrabě	hrabě	k1gMnSc1	hrabě
Josef	Josef	k1gMnSc1	Josef
Jan	Jan	k1gMnSc1	Jan
Maxmilián	Maxmilián	k1gMnSc1	Maxmilián
Kinský	Kinský	k1gMnSc1	Kinský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1780	[number]	k4	1780
byl	být	k5eAaImAgInS	být
skalní	skalní	k2eAgInSc1d1	skalní
hrad	hrad	k1gInSc4	hrad
opraven	opraven	k2eAgInSc4d1	opraven
v	v	k7c6	v
romantickém	romantický	k2eAgMnSc6d1	romantický
duchu	duch	k1gMnSc6	duch
<g/>
,	,	kIx,	,
doplněn	doplněn	k2eAgInSc1d1	doplněn
dřevěným	dřevěný	k2eAgNnSc7d1	dřevěné
schodištěm	schodiště	k1gNnSc7	schodiště
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
výletním	výletní	k2eAgInSc7d1	výletní
letohrádkem	letohrádek	k1gInSc7	letohrádek
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
časem	časem	k6eAd1	časem
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc1	popis
hradu	hrad	k1gInSc2	hrad
==	==	k?	==
</s>
</p>
<p>
<s>
Hrad	hrad	k1gInSc1	hrad
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
vysekáním	vysekání	k1gNnSc7	vysekání
místností	místnost	k1gFnPc2	místnost
do	do	k7c2	do
pískovcového	pískovcový	k2eAgInSc2d1	pískovcový
13	[number]	k4	13
metrů	metr	k1gInPc2	metr
vysokého	vysoký	k2eAgInSc2d1	vysoký
sloupu	sloup	k1gInSc2	sloup
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c4	na
20	[number]	k4	20
metrů	metr	k1gInPc2	metr
vysokém	vysoký	k2eAgInSc6d1	vysoký
zalesněném	zalesněný	k2eAgInSc6d1	zalesněný
pahorku	pahorek	k1gInSc6	pahorek
<g/>
.	.	kIx.	.
</s>
<s>
Místnosti	místnost	k1gFnPc1	místnost
vytvořily	vytvořit	k5eAaPmAgFnP	vytvořit
celkem	celkem	k6eAd1	celkem
tři	tři	k4xCgNnPc4	tři
patra	patro	k1gNnPc4	patro
<g/>
..	..	k?	..
Na	na	k7c6	na
skále	skála	k1gFnSc6	skála
stával	stávat	k5eAaImAgInS	stávat
hradní	hradní	k2eAgInSc4d1	hradní
palác	palác	k1gInSc4	palác
o	o	k7c6	o
dvou	dva	k4xCgNnPc6	dva
křídlech	křídlo	k1gNnPc6	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
jsou	být	k5eAaImIp3nP	být
patrné	patrný	k2eAgInPc1d1	patrný
zbytky	zbytek	k1gInPc1	zbytek
zdiva	zdivo	k1gNnSc2	zdivo
zmíněného	zmíněný	k2eAgInSc2d1	zmíněný
paláce	palác	k1gInSc2	palác
a	a	k8xC	a
řada	řada	k1gFnSc1	řada
vytesaných	vytesaný	k2eAgFnPc2d1	vytesaná
místností	místnost	k1gFnPc2	místnost
a	a	k8xC	a
průchodů	průchod	k1gInPc2	průchod
<g/>
.	.	kIx.	.
</s>
<s>
Schody	schod	k1gInPc1	schod
byly	být	k5eAaImAgInP	být
vedeny	vést	k5eAaImNgInP	vést
po	po	k7c6	po
stěnách	stěna	k1gFnPc6	stěna
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
zde	zde	k6eAd1	zde
několik	několik	k4yIc4	několik
dřevěných	dřevěný	k2eAgFnPc2d1	dřevěná
pavlačí	pavlač	k1gFnPc2	pavlač
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
časem	časem	k6eAd1	časem
odstraněny	odstranit	k5eAaPmNgFnP	odstranit
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
hradu	hrad	k1gInSc2	hrad
sloužila	sloužit	k5eAaImAgFnS	sloužit
jako	jako	k9	jako
skladiště	skladiště	k1gNnSc4	skladiště
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
,	,	kIx,	,
oblečení	oblečení	k1gNnSc2	oblečení
a	a	k8xC	a
potravin	potravina	k1gFnPc2	potravina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Cestovní	cestovní	k2eAgInSc1d1	cestovní
ruch	ruch	k1gInSc1	ruch
==	==	k?	==
</s>
</p>
<p>
<s>
Obcí	obec	k1gFnSc7	obec
vede	vést	k5eAaImIp3nS	vést
cyklotrasa	cyklotrasa	k1gFnSc1	cyklotrasa
3062	[number]	k4	3062
po	po	k7c6	po
silnici	silnice	k1gFnSc6	silnice
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
268	[number]	k4	268
od	od	k7c2	od
Zákup	zákup	k1gInSc1	zákup
do	do	k7c2	do
Sloupu	sloup	k1gInSc2	sloup
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
dvě	dva	k4xCgFnPc4	dva
trasy	trasa	k1gFnPc4	trasa
pro	pro	k7c4	pro
pěší	pěší	k2eAgMnPc4d1	pěší
turisty	turist	k1gMnPc4	turist
<g/>
,	,	kIx,	,
červeně	červeně	k6eAd1	červeně
značená	značený	k2eAgFnSc1d1	značená
z	z	k7c2	z
České	český	k2eAgFnSc2d1	Česká
Lípy	lípa	k1gFnSc2	lípa
a	a	k8xC	a
zelená	zelená	k1gFnSc1	zelená
č.	č.	k?	č.
<g/>
3953	[number]	k4	3953
od	od	k7c2	od
Zákup	zákup	k1gInSc1	zákup
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
malého	malý	k2eAgNnSc2d1	malé
parkoviště	parkoviště	k1gNnSc2	parkoviště
a	a	k8xC	a
restaurace	restaurace	k1gFnSc2	restaurace
Zámeček	zámeček	k1gInSc1	zámeček
je	být	k5eAaImIp3nS	být
upravená	upravený	k2eAgFnSc1d1	upravená
cesta	cesta	k1gFnSc1	cesta
až	až	k9	až
k	k	k7c3	k
hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Posledních	poslední	k2eAgNnPc2d1	poslední
100	[number]	k4	100
metrů	metr	k1gInPc2	metr
je	být	k5eAaImIp3nS	být
příkré	příkrý	k2eAgNnSc1d1	příkré
stoupání	stoupání	k1gNnSc1	stoupání
lesní	lesní	k2eAgFnSc7d1	lesní
pěšinou	pěšina	k1gFnSc7	pěšina
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
stejného	stejný	k2eAgNnSc2d1	stejné
parkoviště	parkoviště	k1gNnSc2	parkoviště
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
je	být	k5eAaImIp3nS	být
cesta	cesta	k1gFnSc1	cesta
k	k	k7c3	k
skalní	skalní	k2eAgFnSc3d1	skalní
kapli	kaple	k1gFnSc3	kaple
v	v	k7c6	v
Modlivému	modlivý	k2eAgInSc3d1	modlivý
dolu	dol	k1gInSc3	dol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Skalní	skalní	k2eAgInSc4d1	skalní
hrad	hrad	k1gInSc4	hrad
Svojkov	Svojkov	k1gInSc4	Svojkov
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Svojkov	Svojkov	k1gInSc1	Svojkov
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Hrady	hrad	k1gInPc4	hrad
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Stránky	stránka	k1gFnPc1	stránka
obce	obec	k1gFnSc2	obec
o	o	k7c6	o
hradu	hrad	k1gInSc6	hrad
</s>
</p>
