<s>
Konrád	Konrád	k1gMnSc1	Konrád
II	II	kA	II
<g/>
.	.	kIx.	.
Ota	Ota	k1gMnSc1	Ota
(	(	kIx(	(
<g/>
1136	[number]	k4	1136
<g/>
/	/	kIx~	/
<g/>
1141	[number]	k4	1141
-	-	kIx~	-
9	[number]	k4	9
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1191	[number]	k4	1191
u	u	k7c2	u
Neapole	Neapol	k1gFnSc2	Neapol
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
kníže	kníže	k1gMnSc1	kníže
(	(	kIx(	(
<g/>
1182	[number]	k4	1182
a	a	k8xC	a
1189	[number]	k4	1189
<g/>
-	-	kIx~	-
<g/>
1191	[number]	k4	1191
<g/>
)	)	kIx)	)
a	a	k8xC	a
markrabě	markrabě	k1gMnSc1	markrabě
moravský	moravský	k2eAgMnSc1d1	moravský
(	(	kIx(	(
<g/>
1182	[number]	k4	1182
<g/>
-	-	kIx~	-
<g/>
1189	[number]	k4	1189
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Konrád	Konrád	k1gMnSc1	Konrád
pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
dynastie	dynastie	k1gFnSc2	dynastie
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc7	jeho
otcem	otec	k1gMnSc7	otec
byl	být	k5eAaImAgMnS	být
kníže	kníže	k1gMnSc1	kníže
znojemského	znojemský	k2eAgInSc2d1	znojemský
údělu	úděl	k1gInSc2	úděl
Konrád	Konrád	k1gMnSc1	Konrád
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
matkou	matka	k1gFnSc7	matka
Marie	Marie	k1gFnSc1	Marie
Srbská	srbský	k2eAgFnSc1d1	Srbská
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
srbského	srbský	k2eAgMnSc2d1	srbský
župana	župan	k1gMnSc2	župan
Uroše	Uroš	k1gInSc2	Uroš
Bílého	bílý	k2eAgInSc2d1	bílý
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
sestrou	sestra	k1gFnSc7	sestra
byla	být	k5eAaImAgFnS	být
polská	polský	k2eAgFnSc1d1	polská
kněžna	kněžna	k1gFnSc1	kněžna
Helena	Helena	k1gFnSc1	Helena
Znojemská	znojemský	k2eAgFnSc1d1	Znojemská
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
pouze	pouze	k6eAd1	pouze
Konrád	Konrád	k1gMnSc1	Konrád
a	a	k8xC	a
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
vždy	vždy	k6eAd1	vždy
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
pouze	pouze	k6eAd1	pouze
pod	pod	k7c7	pod
tímto	tento	k3xDgNnSc7	tento
jménem	jméno	k1gNnSc7	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgNnSc1	druhý
jméno	jméno	k1gNnSc1	jméno
získal	získat	k5eAaPmAgInS	získat
během	během	k7c2	během
pobytu	pobyt	k1gInSc2	pobyt
v	v	k7c6	v
cizině	cizina	k1gFnSc6	cizina
a	a	k8xC	a
používal	používat	k5eAaImAgMnS	používat
je	být	k5eAaImIp3nS	být
mimo	mimo	k7c4	mimo
knížectví	knížectví	k1gNnSc4	knížectví
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
vzdělaný	vzdělaný	k2eAgMnSc1d1	vzdělaný
a	a	k8xC	a
zkušený	zkušený	k2eAgMnSc1d1	zkušený
v	v	k7c6	v
diplomacii	diplomacie	k1gFnSc6	diplomacie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
k	k	k7c3	k
dosažení	dosažení	k1gNnSc3	dosažení
cíle	cíl	k1gInSc2	cíl
neváhal	váhat	k5eNaImAgMnS	váhat
používat	používat	k5eAaImF	používat
i	i	k9	i
tvrdé	tvrdý	k2eAgInPc1d1	tvrdý
prostředky	prostředek	k1gInPc1	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
otce	otka	k1gFnSc6	otka
roku	rok	k1gInSc2	rok
1162	[number]	k4	1162
převzal	převzít	k5eAaPmAgMnS	převzít
Konrád	Konrád	k1gMnSc1	Konrád
Ota	Ota	k1gMnSc1	Ota
vládu	vláda	k1gFnSc4	vláda
ve	v	k7c6	v
znojemském	znojemský	k2eAgInSc6d1	znojemský
údělu	úděl	k1gInSc6	úděl
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
moc	moc	k1gFnSc1	moc
začala	začít	k5eAaPmAgFnS	začít
rychle	rychle	k6eAd1	rychle
vzrůstat	vzrůstat	k5eAaImF	vzrůstat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1173	[number]	k4	1173
mu	on	k3xPp3gMnSc3	on
připadl	připadnout	k5eAaPmAgInS	připadnout
také	také	k9	také
brněnský	brněnský	k2eAgInSc1d1	brněnský
úděl	úděl	k1gInSc1	úděl
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnPc1	jeho
državy	država	k1gFnPc1	država
zabíraly	zabírat	k5eAaImAgFnP	zabírat
již	již	k6eAd1	již
polovinu	polovina	k1gFnSc4	polovina
Moravy	Morava	k1gFnSc2	Morava
(	(	kIx(	(
<g/>
zatím	zatím	k6eAd1	zatím
mu	on	k3xPp3gMnSc3	on
nepatřilo	patřit	k5eNaImAgNnS	patřit
Olomoucko	Olomoucko	k1gNnSc4	Olomoucko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
bylo	být	k5eAaImAgNnS	být
stále	stále	k6eAd1	stále
složitější	složitý	k2eAgNnSc1d2	složitější
kontrolovat	kontrolovat	k5eAaImF	kontrolovat
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Dobře	dobře	k6eAd1	dobře
se	se	k3xPyFc4	se
zorientoval	zorientovat	k5eAaPmAgInS	zorientovat
v	v	k7c6	v
komplikovaných	komplikovaný	k2eAgInPc6d1	komplikovaný
vztazích	vztah	k1gInPc6	vztah
mezi	mezi	k7c7	mezi
početnými	početný	k2eAgMnPc7d1	početný
Přemyslovci	Přemyslovec	k1gMnPc7	Přemyslovec
<g/>
,	,	kIx,	,
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
důležitým	důležitý	k2eAgInSc7d1	důležitý
činitelem	činitel	k1gInSc7	činitel
v	v	k7c6	v
jejich	jejich	k3xOp3gInPc6	jejich
bojích	boj	k1gInPc6	boj
o	o	k7c4	o
pražský	pražský	k2eAgInSc4d1	pražský
trůn	trůn	k1gInSc4	trůn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1178	[number]	k4	1178
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
rakouským	rakouský	k2eAgMnSc7d1	rakouský
vévodou	vévoda	k1gMnSc7	vévoda
Leopoldem	Leopold	k1gMnSc7	Leopold
vpadl	vpadnout	k5eAaPmAgMnS	vpadnout
na	na	k7c4	na
Olomoucko	Olomoucko	k1gNnSc4	Olomoucko
a	a	k8xC	a
pokusil	pokusit	k5eAaPmAgMnS	pokusit
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
dobýt	dobýt	k5eAaPmF	dobýt
i	i	k9	i
Olomoucký	olomoucký	k2eAgInSc4d1	olomoucký
hrad	hrad	k1gInSc4	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
pomohl	pomoct	k5eAaPmAgMnS	pomoct
pražskému	pražský	k2eAgMnSc3d1	pražský
knížeti	kníže	k1gMnSc3	kníže
Bedřichovi	Bedřich	k1gMnSc3	Bedřich
udržet	udržet	k5eAaPmF	udržet
si	se	k3xPyFc3	se
trůn	trůn	k1gInSc4	trůn
proti	proti	k7c3	proti
dalším	další	k2eAgMnPc3d1	další
kandidátům	kandidát	k1gMnPc3	kandidát
<g/>
,	,	kIx,	,
očekával	očekávat	k5eAaImAgInS	očekávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
za	za	k7c4	za
odměnu	odměna	k1gFnSc4	odměna
mohl	moct	k5eAaImAgMnS	moct
získat	získat	k5eAaPmF	získat
Olomoucko	Olomoucko	k1gNnSc4	Olomoucko
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
nestalo	stát	k5eNaPmAgNnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
(	(	kIx(	(
<g/>
Bedřich	Bedřich	k1gMnSc1	Bedřich
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
spravovat	spravovat	k5eAaImF	spravovat
tento	tento	k3xDgInSc4	tento
úděl	úděl	k1gInSc4	úděl
sám	sám	k3xTgMnSc1	sám
<g/>
)	)	kIx)	)
a	a	k8xC	a
Konrád	Konrád	k1gMnSc1	Konrád
Ota	Ota	k1gMnSc1	Ota
začal	začít	k5eAaPmAgMnS	začít
pomýšlet	pomýšlet	k5eAaImF	pomýšlet
na	na	k7c4	na
odplatu	odplata	k1gFnSc4	odplata
<g/>
.	.	kIx.	.
</s>
<s>
Příležitost	příležitost	k1gFnSc1	příležitost
se	se	k3xPyFc4	se
naskytla	naskytnout	k5eAaPmAgFnS	naskytnout
roku	rok	k1gInSc2	rok
1182	[number]	k4	1182
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
velmoži	velmož	k1gMnPc1	velmož
povstali	povstat	k5eAaPmAgMnP	povstat
proti	proti	k7c3	proti
neoblíbenému	oblíbený	k2eNgMnSc3d1	neoblíbený
Bedřichovi	Bedřich	k1gMnSc3	Bedřich
a	a	k8xC	a
na	na	k7c4	na
pražský	pražský	k2eAgInSc4d1	pražský
trůn	trůn	k1gInSc4	trůn
povolali	povolat	k5eAaPmAgMnP	povolat
energického	energický	k2eAgMnSc4d1	energický
Konráda	Konrád	k1gMnSc4	Konrád
Otu	Ota	k1gMnSc4	Ota
<g/>
.	.	kIx.	.
</s>
<s>
Bedřich	Bedřich	k1gMnSc1	Bedřich
uprchl	uprchnout	k5eAaPmAgMnS	uprchnout
do	do	k7c2	do
říše	říš	k1gFnSc2	říš
k	k	k7c3	k
císaři	císař	k1gMnSc3	císař
Fridrichu	Fridrich	k1gMnSc3	Fridrich
I.	I.	kA	I.
Barbarossovi	Barbarossa	k1gMnSc3	Barbarossa
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
jako	jako	k8xS	jako
lenní	lenní	k2eAgMnSc1d1	lenní
pán	pán	k1gMnSc1	pán
pozval	pozvat	k5eAaPmAgMnS	pozvat
znesvářené	znesvářený	k2eAgFnPc4d1	znesvářená
strany	strana	k1gFnPc4	strana
na	na	k7c4	na
říšský	říšský	k2eAgInSc4d1	říšský
sněm	sněm	k1gInSc4	sněm
do	do	k7c2	do
Řezna	Řezno	k1gNnSc2	Řezno
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
kronikář	kronikář	k1gMnSc1	kronikář
Jarloch	Jarloch	k1gMnSc1	Jarloch
<g/>
,	,	kIx,	,
Čechové	Čech	k1gMnPc1	Čech
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
odmítali	odmítat	k5eAaImAgMnP	odmítat
dostavit	dostavit	k5eAaPmF	dostavit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
potom	potom	k6eAd1	potom
dali	dát	k5eAaPmAgMnP	dát
na	na	k7c4	na
čísi	čísi	k3xOyIgFnSc4	čísi
radu	rada	k1gFnSc4	rada
a	a	k8xC	a
před	před	k7c4	před
císaře	císař	k1gMnSc4	císař
předstoupili	předstoupit	k5eAaPmAgMnP	předstoupit
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
tvrdě	tvrdě	k6eAd1	tvrdě
zasáhl	zasáhnout	k5eAaPmAgMnS	zasáhnout
a	a	k8xC	a
zastrašil	zastrašit	k5eAaPmAgMnS	zastrašit
přítomné	přítomný	k2eAgMnPc4d1	přítomný
Čechy	Čech	k1gMnPc4	Čech
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
rozdělení	rozdělení	k1gNnSc1	rozdělení
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
Moravy	Morava	k1gFnSc2	Morava
mezi	mezi	k7c4	mezi
Bedřicha	Bedřich	k1gMnSc4	Bedřich
a	a	k8xC	a
Konráda	Konrád	k1gMnSc4	Konrád
Otu	Ota	k1gMnSc4	Ota
rozhodně	rozhodně	k6eAd1	rozhodně
nelze	lze	k6eNd1	lze
chápat	chápat	k5eAaImF	chápat
jako	jako	k9	jako
osamostatnění	osamostatnění	k1gNnSc4	osamostatnění
Moravy	Morava	k1gFnSc2	Morava
a	a	k8xC	a
ani	ani	k8xC	ani
jako	jako	k9	jako
její	její	k3xOp3gNnSc4	její
přímé	přímý	k2eAgNnSc4d1	přímé
podřízení	podřízení	k1gNnSc4	podřízení
Říši	říše	k1gFnSc4	říše
<g/>
.	.	kIx.	.
</s>
<s>
Konrád	Konrád	k1gMnSc1	Konrád
Ota	Ota	k1gMnSc1	Ota
se	se	k3xPyFc4	se
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
tituloval	titulovat	k5eAaImAgMnS	titulovat
jako	jako	k9	jako
moravský	moravský	k2eAgMnSc1d1	moravský
markrabí	markrabí	k1gMnSc1	markrabí
a	a	k8xC	a
usiloval	usilovat	k5eAaImAgMnS	usilovat
o	o	k7c4	o
nezávislost	nezávislost	k1gFnSc4	nezávislost
na	na	k7c6	na
pražském	pražský	k2eAgNnSc6d1	Pražské
knížeti	kníže	k1gNnSc6wR	kníže
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
historici	historik	k1gMnPc1	historik
často	často	k6eAd1	často
vykládali	vykládat	k5eAaImAgMnP	vykládat
události	událost	k1gFnSc3	událost
řezenského	řezenský	k2eAgInSc2d1	řezenský
sněmu	sněm	k1gInSc2	sněm
jako	jako	k8xC	jako
státoprávní	státoprávní	k2eAgInSc4d1	státoprávní
akt	akt	k1gInSc4	akt
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
Fridrich	Fridrich	k1gMnSc1	Fridrich
Barbarossa	Barbarossa	k1gMnSc1	Barbarossa
zřídil	zřídit	k5eAaPmAgMnS	zřídit
moravské	moravský	k2eAgNnSc4d1	Moravské
markrabství	markrabství	k1gNnSc4	markrabství
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
vyjmul	vyjmout	k5eAaPmAgMnS	vyjmout
z	z	k7c2	z
pravomoci	pravomoc	k1gFnSc2	pravomoc
českých	český	k2eAgMnPc2d1	český
knížat	kníže	k1gMnPc2wR	kníže
a	a	k8xC	a
podřídil	podřídit	k5eAaPmAgMnS	podřídit
je	být	k5eAaImIp3nS	být
říši	říše	k1gFnSc4	říše
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
však	však	k9	však
nebylo	být	k5eNaImAgNnS	být
přímo	přímo	k6eAd1	přímo
řečeno	říct	k5eAaPmNgNnS	říct
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
každém	každý	k3xTgInSc6	každý
případě	případ	k1gInSc6	případ
císař	císař	k1gMnSc1	císař
jen	jen	k6eAd1	jen
přilil	přilít	k5eAaPmAgMnS	přilít
v	v	k7c6	v
situaci	situace	k1gFnSc6	situace
závažných	závažný	k2eAgInPc2d1	závažný
rozporů	rozpor	k1gInPc2	rozpor
mezi	mezi	k7c7	mezi
Přemyslovci	Přemyslovec	k1gMnPc7	Přemyslovec
pověstný	pověstný	k2eAgInSc4d1	pověstný
olej	olej	k1gInSc4	olej
do	do	k7c2	do
ohně	oheň	k1gInSc2	oheň
<g/>
.	.	kIx.	.
</s>
<s>
Nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jednotný	jednotný	k2eAgInSc1d1	jednotný
český	český	k2eAgInSc1d1	český
stát	stát	k1gInSc1	stát
rozpadne	rozpadnout	k5eAaPmIp3nS	rozpadnout
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
se	se	k3xPyFc4	se
tomu	ten	k3xDgMnSc3	ten
stalo	stát	k5eAaPmAgNnS	stát
například	například	k6eAd1	například
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
nebo	nebo	k8xC	nebo
na	na	k7c6	na
Rusi	Rus	k1gFnSc6	Rus
<g/>
,	,	kIx,	,
sílilo	sílit	k5eAaImAgNnS	sílit
<g/>
.	.	kIx.	.
</s>
<s>
Bedřich	Bedřich	k1gMnSc1	Bedřich
společně	společně	k6eAd1	společně
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
nevlastním	vlastní	k2eNgMnSc7d1	nevlastní
bratrem	bratr	k1gMnSc7	bratr
Přemyslem	Přemysl	k1gMnSc7	Přemysl
(	(	kIx(	(
<g/>
pozdějším	pozdní	k2eAgMnSc7d2	pozdější
Přemyslem	Přemysl	k1gMnSc7	Přemysl
Otakarem	Otakar	k1gMnSc7	Otakar
I.	I.	kA	I.
<g/>
)	)	kIx)	)
hodlali	hodlat	k5eAaImAgMnP	hodlat
zasáhnout	zasáhnout	k5eAaPmF	zasáhnout
proti	proti	k7c3	proti
přílišné	přílišný	k2eAgFnSc3d1	přílišná
samostatnosti	samostatnost	k1gFnSc3	samostatnost
moravského	moravský	k2eAgMnSc2d1	moravský
markraběte	markrabě	k1gMnSc2	markrabě
Konráda	Konrád	k1gMnSc2	Konrád
Oty	Ota	k1gMnSc2	Ota
a	a	k8xC	a
vytáhli	vytáhnout	k5eAaPmAgMnP	vytáhnout
na	na	k7c4	na
Moravu	Morava	k1gFnSc4	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Tvrdě	tvrdě	k6eAd1	tvrdě
poplenili	poplenit	k5eAaPmAgMnP	poplenit
Brněnsko	Brněnsko	k1gNnSc4	Brněnsko
i	i	k8xC	i
Znojemsko	Znojemsko	k1gNnSc4	Znojemsko
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1185	[number]	k4	1185
došlo	dojít	k5eAaPmAgNnS	dojít
u	u	k7c2	u
Loděnice	loděnice	k1gFnSc2	loděnice
snad	snad	k9	snad
k	k	k7c3	k
nejhorší	zlý	k2eAgFnSc3d3	nejhorší
bitvě	bitva	k1gFnSc3	bitva
mezi	mezi	k7c7	mezi
Čechy	Čech	k1gMnPc7	Čech
a	a	k8xC	a
Moravany	Moravan	k1gMnPc7	Moravan
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Vítězem	vítěz	k1gMnSc7	vítěz
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Přemysl	Přemysl	k1gMnSc1	Přemysl
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gNnSc1	jeho
vojsko	vojsko	k1gNnSc1	vojsko
utrpělo	utrpět	k5eAaPmAgNnS	utrpět
takové	takový	k3xDgFnPc4	takový
ztráty	ztráta	k1gFnPc4	ztráta
<g/>
,	,	kIx,	,
že	že	k8xS	že
místo	místo	k7c2	místo
aby	aby	kYmCp3nS	aby
pronásledoval	pronásledovat	k5eAaImAgInS	pronásledovat
poražené	poražený	k1gMnPc4	poražený
<g/>
,	,	kIx,	,
vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
nazpět	nazpět	k6eAd1	nazpět
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
Konrád	Konrád	k1gMnSc1	Konrád
Ota	Ota	k1gMnSc1	Ota
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
raději	rád	k6eAd2	rád
s	s	k7c7	s
knížetem	kníže	k1gMnSc7	kníže
Bedřichem	Bedřich	k1gMnSc7	Bedřich
vyjednávat	vyjednávat	k5eAaImF	vyjednávat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jednáních	jednání	k1gNnPc6	jednání
v	v	k7c6	v
Kníně	Kníno	k1gNnSc6	Kníno
zřejmě	zřejmě	k6eAd1	zřejmě
uznal	uznat	k5eAaPmAgInS	uznat
svrchovanost	svrchovanost	k1gFnSc4	svrchovanost
pražského	pražský	k2eAgMnSc2d1	pražský
knížete	kníže	k1gMnSc2	kníže
nad	nad	k7c7	nad
Moravou	Morava	k1gFnSc7	Morava
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Bedřich	Bedřich	k1gMnSc1	Bedřich
mu	on	k3xPp3gMnSc3	on
přiznal	přiznat	k5eAaPmAgMnS	přiznat
její	její	k3xOp3gNnPc4	její
držení	držení	k1gNnPc4	držení
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
dohodami	dohoda	k1gFnPc7	dohoda
v	v	k7c6	v
Kníně	Knín	k1gInSc6	Knín
byl	být	k5eAaImAgInS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
spojen	spojit	k5eAaPmNgInS	spojit
i	i	k8xC	i
Konrádův	Konrádův	k2eAgInSc1d1	Konrádův
titul	titul	k1gInSc1	titul
markraběte	markrabě	k1gMnSc2	markrabě
(	(	kIx(	(
<g/>
císařská	císařský	k2eAgFnSc1d1	císařská
kancelář	kancelář	k1gFnSc1	kancelář
tituluje	titulovat	k5eAaImIp3nS	titulovat
Konráda	Konrád	k1gMnSc4	Konrád
jako	jako	k8xS	jako
markraběte	markrabě	k1gMnSc4	markrabě
mezi	mezi	k7c7	mezi
léty	léto	k1gNnPc7	léto
1186	[number]	k4	1186
<g/>
-	-	kIx~	-
<g/>
1189	[number]	k4	1189
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
přijetím	přijetí	k1gNnSc7	přijetí
titulu	titul	k1gInSc2	titul
tak	tak	k6eAd1	tak
bylo	být	k5eAaImAgNnS	být
spojeno	spojit	k5eAaPmNgNnS	spojit
i	i	k9	i
uznání	uznání	k1gNnSc1	uznání
mocenského	mocenský	k2eAgInSc2d1	mocenský
primátu	primát	k1gInSc2	primát
pražského	pražský	k2eAgMnSc2d1	pražský
knížete	kníže	k1gMnSc2	kníže
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
oplátku	oplátka	k1gFnSc4	oplátka
Konrád	Konrád	k1gMnSc1	Konrád
obdržel	obdržet	k5eAaPmAgMnS	obdržet
záruky	záruka	k1gFnPc4	záruka
nástupnictví	nástupnictví	k1gNnSc2	nástupnictví
po	po	k7c6	po
Bedřichově	Bedřichův	k2eAgFnSc6d1	Bedřichova
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
knížete	kníže	k1gMnSc2	kníže
Bedřicha	Bedřich	k1gMnSc2	Bedřich
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1189	[number]	k4	1189
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vlády	vláda	k1gFnPc1	vláda
(	(	kIx(	(
<g/>
patrně	patrně	k6eAd1	patrně
na	na	k7c6	na
základu	základ	k1gInSc6	základ
dohod	dohoda	k1gFnPc2	dohoda
z	z	k7c2	z
Knína	Knín	k1gInSc2	Knín
<g/>
)	)	kIx)	)
a	a	k8xC	a
se	se	k3xPyFc4	se
souhlasem	souhlas	k1gInSc7	souhlas
českých	český	k2eAgMnPc2d1	český
předáků	předák	k1gMnPc2	předák
ujal	ujmout	k5eAaPmAgMnS	ujmout
Konrád	Konrád	k1gMnSc1	Konrád
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Ota	Ota	k1gMnSc1	Ota
<g/>
.	.	kIx.	.
</s>
<s>
Čechy	Čechy	k1gFnPc1	Čechy
a	a	k8xC	a
Morava	Morava	k1gFnSc1	Morava
se	se	k3xPyFc4	se
tak	tak	k9	tak
spojily	spojit	k5eAaPmAgFnP	spojit
do	do	k7c2	do
jednoho	jeden	k4xCgInSc2	jeden
státního	státní	k2eAgInSc2d1	státní
celku	celek	k1gInSc2	celek
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
novopečený	novopečený	k2eAgMnSc1d1	novopečený
kníže	kníže	k1gMnSc1	kníže
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
na	na	k7c4	na
titul	titul	k1gInSc4	titul
markraběte	markrabě	k1gMnSc2	markrabě
moravského	moravský	k2eAgMnSc2d1	moravský
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vše	všechen	k3xTgNnSc1	všechen
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
se	s	k7c7	s
souhlasem	souhlas	k1gInSc7	souhlas
císaře	císař	k1gMnSc2	císař
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
kníže	kníže	k1gMnSc1	kníže
choval	chovat	k5eAaImAgMnS	chovat
smířlivě	smířlivě	k6eAd1	smířlivě
k	k	k7c3	k
ostatním	ostatní	k2eAgMnPc3d1	ostatní
členům	člen	k1gMnPc3	člen
dynastie	dynastie	k1gFnSc2	dynastie
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
uprchli	uprchnout	k5eAaPmAgMnP	uprchnout
během	během	k7c2	během
rozbrojů	rozbroj	k1gInPc2	rozbroj
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nyní	nyní	k6eAd1	nyní
mohli	moct	k5eAaImAgMnP	moct
bez	bez	k7c2	bez
obav	obava	k1gFnPc2	obava
vrátit	vrátit	k5eAaPmF	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Chladné	chladný	k2eAgInPc1d1	chladný
vztahy	vztah	k1gInPc1	vztah
přetrvávaly	přetrvávat	k5eAaImAgInP	přetrvávat
pouze	pouze	k6eAd1	pouze
mezi	mezi	k7c7	mezi
Konrádem	Konrád	k1gMnSc7	Konrád
Otou	Ota	k1gMnSc7	Ota
a	a	k8xC	a
Přemyslem	Přemysl	k1gMnSc7	Přemysl
<g/>
.	.	kIx.	.
</s>
<s>
Konrád	Konrád	k1gMnSc1	Konrád
Ota	Ota	k1gMnSc1	Ota
neusiloval	usilovat	k5eNaImAgMnS	usilovat
o	o	k7c6	o
předání	předání	k1gNnSc6	předání
trůnu	trůn	k1gInSc2	trůn
dědicům	dědic	k1gMnPc3	dědic
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jeho	jeho	k3xOp3gNnSc1	jeho
manželství	manželství	k1gNnSc1	manželství
s	s	k7c7	s
Hellichou	Hellicha	k1gFnSc7	Hellicha
z	z	k7c2	z
Wittelsbachu	Wittelsbach	k1gInSc2	Wittelsbach
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
bezdětné	bezdětný	k2eAgNnSc1d1	bezdětné
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1190	[number]	k4	1190
společně	společně	k6eAd1	společně
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
matkou	matka	k1gFnSc7	matka
Marií	Maria	k1gFnPc2	Maria
založil	založit	k5eAaPmAgInS	založit
premonstrátský	premonstrátský	k2eAgInSc1d1	premonstrátský
klášter	klášter	k1gInSc1	klášter
v	v	k7c6	v
Louce	louka	k1gFnSc6	louka
u	u	k7c2	u
Znojma	Znojmo	k1gNnSc2	Znojmo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
samého	samý	k3xTgInSc2	samý
počátku	počátek	k1gInSc2	počátek
své	svůj	k3xOyFgFnSc2	svůj
vlády	vláda	k1gFnSc2	vláda
v	v	k7c6	v
knížectví	knížectví	k1gNnSc6	knížectví
se	se	k3xPyFc4	se
Konrád	Konrád	k1gMnSc1	Konrád
Ota	Ota	k1gMnSc1	Ota
aktivně	aktivně	k6eAd1	aktivně
zapojil	zapojit	k5eAaPmAgMnS	zapojit
do	do	k7c2	do
politiky	politika	k1gFnSc2	politika
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
roku	rok	k1gInSc2	rok
1189	[number]	k4	1189
vyrazil	vyrazit	k5eAaPmAgMnS	vyrazit
na	na	k7c4	na
popud	popud	k1gInSc4	popud
císařova	císařův	k2eAgMnSc2d1	císařův
syna	syn	k1gMnSc2	syn
Jindřicha	Jindřich	k1gMnSc2	Jindřich
do	do	k7c2	do
Míšeňska	Míšeňsk	k1gInSc2	Míšeňsk
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
sporům	spor	k1gInPc3	spor
mezi	mezi	k7c7	mezi
zde	zde	k6eAd1	zde
panujícími	panující	k2eAgInPc7d1	panující
Wettiny	Wettin	k1gInPc7	Wettin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
roku	rok	k1gInSc2	rok
1190	[number]	k4	1190
se	se	k3xPyFc4	se
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
početným	početný	k2eAgInSc7d1	početný
českým	český	k2eAgInSc7d1	český
oddílem	oddíl	k1gInSc7	oddíl
zapojil	zapojit	k5eAaPmAgMnS	zapojit
do	do	k7c2	do
císařské	císařský	k2eAgFnSc2d1	císařská
korunovační	korunovační	k2eAgFnSc2d1	korunovační
jízdy	jízda	k1gFnSc2	jízda
Jindřicha	Jindřich	k1gMnSc2	Jindřich
VI	VI	kA	VI
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
nástupce	nástupce	k1gMnSc1	nástupce
Fridricha	Fridrich	k1gMnSc2	Fridrich
I.	I.	kA	I.
Barbarossy	Barbarossa	k1gMnSc2	Barbarossa
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
tažení	tažení	k1gNnSc1	tažení
se	se	k3xPyFc4	se
mu	on	k3xPp3gNnSc3	on
stalo	stát	k5eAaPmAgNnS	stát
osudným	osudný	k2eAgMnSc7d1	osudný
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
obětí	oběť	k1gFnPc2	oběť
moru	mor	k1gInSc2	mor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vypukl	vypuknout	k5eAaPmAgInS	vypuknout
v	v	k7c6	v
Jindřichově	Jindřichův	k2eAgInSc6d1	Jindřichův
táboře	tábor	k1gInSc6	tábor
nedaleko	nedaleko	k7c2	nedaleko
Neapole	Neapol	k1gFnSc2	Neapol
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc7	který
se	se	k3xPyFc4	se
císařští	císařský	k2eAgMnPc1d1	císařský
marně	marně	k6eAd1	marně
pokoušeli	pokoušet	k5eAaImAgMnP	pokoušet
dobýt	dobýt	k5eAaPmF	dobýt
<g/>
.	.	kIx.	.
</s>
<s>
Měkké	měkký	k2eAgFnPc1d1	měkká
části	část	k1gFnPc1	část
Konrádova	Konrádův	k2eAgNnSc2d1	Konrádovo
těla	tělo	k1gNnSc2	tělo
byly	být	k5eAaImAgFnP	být
pochovány	pochovat	k5eAaPmNgFnP	pochovat
v	v	k7c6	v
benediktinském	benediktinský	k2eAgInSc6d1	benediktinský
klášteře	klášter	k1gInSc6	klášter
v	v	k7c6	v
Monte	Mont	k1gInSc5	Mont
Cassino	Cassin	k2eAgNnSc1d1	Cassino
<g/>
,	,	kIx,	,
kosti	kost	k1gFnPc1	kost
byly	být	k5eAaImAgFnP	být
později	pozdě	k6eAd2	pozdě
převezeny	převézt	k5eAaPmNgFnP	převézt
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
svým	svůj	k3xOyFgInSc7	svůj
odjezdem	odjezd	k1gInSc7	odjezd
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
uspořádal	uspořádat	k5eAaPmAgMnS	uspořádat
Konrád	Konrád	k1gMnSc1	Konrád
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Ota	Ota	k1gMnSc1	Ota
nástupnické	nástupnický	k2eAgFnSc2d1	nástupnická
poměry	poměra	k1gFnSc2	poměra
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
státě	stát	k1gInSc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
dorazila	dorazit	k5eAaPmAgFnS	dorazit
zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
skonu	skon	k1gInSc6	skon
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
bez	bez	k7c2	bez
problémů	problém	k1gInPc2	problém
nastolen	nastolen	k2eAgMnSc1d1	nastolen
jím	jíst	k5eAaImIp1nS	jíst
určený	určený	k2eAgMnSc1d1	určený
kandidát	kandidát	k1gMnSc1	kandidát
<g/>
,	,	kIx,	,
nejmladší	mladý	k2eAgMnSc1d3	nejmladší
syn	syn	k1gMnSc1	syn
Soběslava	Soběslav	k1gMnSc2	Soběslav
I.	I.	kA	I.
<g/>
,	,	kIx,	,
kníže	kníže	k1gMnSc1	kníže
Václav	Václav	k1gMnSc1	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
Konrád	Konrád	k1gMnSc1	Konrád
Ota	Ota	k1gMnSc1	Ota
zastával	zastávat	k5eAaImAgMnS	zastávat
knížecí	knížecí	k2eAgFnSc4d1	knížecí
hodnost	hodnost	k1gFnSc4	hodnost
pouhé	pouhý	k2eAgInPc4d1	pouhý
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejvýznamnějším	významný	k2eAgMnPc3d3	nejvýznamnější
vládcům	vládce	k1gMnPc3	vládce
z	z	k7c2	z
přemyslovské	přemyslovský	k2eAgFnSc2d1	Přemyslovská
dynastie	dynastie	k1gFnSc2	dynastie
ve	v	k7c6	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
historici	historik	k1gMnPc1	historik
mu	on	k3xPp3gMnSc3	on
přiznávají	přiznávat	k5eAaImIp3nP	přiznávat
zásluhy	zásluha	k1gFnPc4	zásluha
za	za	k7c4	za
sjednocení	sjednocení	k1gNnSc4	sjednocení
státu	stát	k1gInSc2	stát
a	a	k8xC	a
upevnění	upevnění	k1gNnSc2	upevnění
jeho	jeho	k3xOp3gInPc2	jeho
právních	právní	k2eAgInPc2d1	právní
základů	základ	k1gInPc2	základ
<g/>
,	,	kIx,	,
navzdory	navzdory	k7c3	navzdory
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
vyčítají	vyčítat	k5eAaImIp3nP	vyčítat
přílišnou	přílišný	k2eAgFnSc4d1	přílišná
ctižádostivost	ctižádostivost	k1gFnSc4	ctižádostivost
a	a	k8xC	a
občasné	občasný	k2eAgNnSc4d1	občasné
nečestné	čestný	k2eNgNnSc4d1	nečestné
jednání	jednání	k1gNnSc4	jednání
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějším	významný	k2eAgInSc7d3	nejvýznamnější
panovnickým	panovnický	k2eAgInSc7d1	panovnický
činem	čin	k1gInSc7	čin
knížete	kníže	k1gMnSc2	kníže
bylo	být	k5eAaImAgNnS	být
vydání	vydání	k1gNnSc1	vydání
Statutu	statut	k1gInSc2	statut
Konráda	Konrád	k1gMnSc2	Konrád
Oty	Ota	k1gMnSc2	Ota
<g/>
,	,	kIx,	,
nejstaršího	starý	k2eAgInSc2d3	nejstarší
českého	český	k2eAgInSc2d1	český
zákoníku	zákoník	k1gInSc2	zákoník
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
vyhlášen	vyhlášen	k2eAgInSc1d1	vyhlášen
roku	rok	k1gInSc2	rok
1189	[number]	k4	1189
na	na	k7c6	na
sněmu	sněm	k1gInSc6	sněm
velmožů	velmož	k1gMnPc2	velmož
v	v	k7c6	v
Sadské	sadský	k2eAgFnSc6d1	Sadská
u	u	k7c2	u
Nymburka	Nymburk	k1gInSc2	Nymburk
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
cílem	cíl	k1gInSc7	cíl
tohoto	tento	k3xDgInSc2	tento
kroku	krok	k1gInSc2	krok
byla	být	k5eAaImAgFnS	být
kodifikace	kodifikace	k1gFnSc1	kodifikace
zvykového	zvykový	k2eAgNnSc2d1	zvykové
(	(	kIx(	(
<g/>
nepsaného	psaný	k2eNgNnSc2d1	nepsané
<g/>
)	)	kIx)	)
práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
především	především	k9	především
z	z	k7c2	z
trestní	trestní	k2eAgFnSc2d1	trestní
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Důležitým	důležitý	k2eAgNnSc7d1	důležité
ustanovením	ustanovení	k1gNnSc7	ustanovení
bylo	být	k5eAaImAgNnS	být
také	také	k9	také
uzákonění	uzákonění	k1gNnSc1	uzákonění
dědičnosti	dědičnost	k1gFnSc2	dědičnost
lén	léno	k1gNnPc2	léno
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
nedochovaly	dochovat	k5eNaPmAgInP	dochovat
originály	originál	k1gInPc1	originál
těchto	tento	k3xDgInPc2	tento
dokumentů	dokument	k1gInPc2	dokument
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
opisy	opis	k1gInPc1	opis
(	(	kIx(	(
<g/>
nejstarší	starý	k2eAgMnSc1d3	nejstarší
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1222	[number]	k4	1222
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nemají	mít	k5eNaImIp3nP	mít
historici	historik	k1gMnPc1	historik
pochyby	pochyba	k1gFnSc2	pochyba
o	o	k7c6	o
jejich	jejich	k3xOp3gFnSc6	jejich
pravosti	pravost	k1gFnSc6	pravost
<g/>
.	.	kIx.	.
</s>
<s>
Diskuse	diskuse	k1gFnSc1	diskuse
se	se	k3xPyFc4	se
ale	ale	k9	ale
vede	vést	k5eAaImIp3nS	vést
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
byly	být	k5eAaImAgInP	být
vydány	vydat	k5eAaPmNgInP	vydat
výhradně	výhradně	k6eAd1	výhradně
pro	pro	k7c4	pro
Moravu	Morava	k1gFnSc4	Morava
(	(	kIx(	(
<g/>
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
týkají	týkat	k5eAaImIp3nP	týkat
dochované	dochovaný	k2eAgInPc1d1	dochovaný
exempláře	exemplář	k1gInPc1	exemplář
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
pro	pro	k7c4	pro
celý	celý	k2eAgInSc4d1	celý
český	český	k2eAgInSc4d1	český
stát	stát	k1gInSc4	stát
<g/>
.	.	kIx.	.
</s>
