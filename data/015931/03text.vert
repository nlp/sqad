<s>
Stupeň	stupeň	k1gInSc1
Fahrenheita	Fahrenheit	k1gMnSc2
</s>
<s>
Znak	znak	k1gInSc1
<g/>
℉	℉	k?
<g/>
Název	název	k1gInSc1
v	v	k7c6
UnicoduDegree	UnicoduDegree	k1gFnSc6
FahrenheitKódovánídechexUnicode	FahrenheitKódovánídechexUnicod	k1gInSc5
<g/>
8457	#num#	k4
<g/>
U	U	kA
<g/>
+	+	kIx~
<g/>
2109	#num#	k4
<g/>
UTF-	UTF-	k1gFnSc1
<g/>
8226	#num#	k4
132	#num#	k4
137	#num#	k4
<g/>
e	e	k0
<g/>
2	#num#	k4
84	#num#	k4
89	#num#	k4
<g/>
Číselná	číselný	k2eAgFnSc1d1
entita	entita	k1gFnSc1
<g/>
℉	℉	k?
</s>
<s>
Stupeň	stupeň	k1gInSc1
Fahrenheita	Fahrenheit	k1gMnSc2
(	(	kIx(
<g/>
značka	značka	k1gFnSc1
°	°	k?
<g/>
F	F	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
jednotka	jednotka	k1gFnSc1
teploty	teplota	k1gFnSc2
pojmenovaná	pojmenovaný	k2eAgFnSc1d1
po	po	k7c6
německém	německý	k2eAgInSc6d1
fyzikovi	fyzik	k1gMnSc3
Gabrielu	Gabriel	k1gMnSc3
Fahrenheitovi	Fahrenheit	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnes	dnes	k6eAd1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
převážně	převážně	k6eAd1
v	v	k7c6
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s>
Vychází	vycházet	k5eAaImIp3nS
ze	z	k7c2
dvou	dva	k4xCgInPc2
základních	základní	k2eAgInPc2d1
referenčních	referenční	k2eAgInPc2d1
bodů	bod	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teplota	teplota	k1gFnSc1
0	#num#	k4
°	°	k?
<g/>
F	F	kA
je	být	k5eAaImIp3nS
nejnižší	nízký	k2eAgFnSc1d3
teplota	teplota	k1gFnSc1
<g/>
,	,	kIx,
jaké	jaký	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
Fahrenheitovi	Fahrenheitův	k2eAgMnPc1d1
dosáhnout	dosáhnout	k5eAaPmF
(	(	kIx(
<g/>
roku	rok	k1gInSc3
1724	#num#	k4
<g/>
)	)	kIx)
smícháním	smíchání	k1gNnSc7
chloridu	chlorid	k1gInSc2
amonného	amonný	k2eAgInSc2d1
<g/>
,	,	kIx,
vody	voda	k1gFnSc2
a	a	k8xC
ledu	led	k1gInSc2
(	(	kIx(
<g/>
přibližně	přibližně	k6eAd1
-17,78	-17,78	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
a	a	k8xC
teplota	teplota	k1gFnSc1
98	#num#	k4
°	°	k?
<g/>
F	F	kA
(	(	kIx(
<g/>
přibližně	přibližně	k6eAd1
36,67	36,67	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
normální	normální	k2eAgFnSc1d1
tělesná	tělesný	k2eAgFnSc1d1
teplota	teplota	k1gFnSc1
člověka	člověk	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
byly	být	k5eAaImAgInP
referenční	referenční	k2eAgInPc1d1
body	bod	k1gInPc1
upraveny	upravit	k5eAaPmNgInP
na	na	k7c4
32	#num#	k4
°	°	k?
<g/>
F	F	kA
pro	pro	k7c4
bod	bod	k1gInSc4
mrazu	mráz	k1gInSc2
vody	voda	k1gFnSc2
a	a	k8xC
212	#num#	k4
°	°	k?
<g/>
F	F	kA
bod	bod	k1gInSc4
varu	var	k1gInSc2
vody	voda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
referenční	referenční	k2eAgFnPc1d1
hodnoty	hodnota	k1gFnPc1
dělí	dělit	k5eAaImIp3nP
na	na	k7c6
Fahrenheitově	Fahrenheitův	k2eAgFnSc6d1
teplotní	teplotní	k2eAgFnSc6d1
stupnici	stupnice	k1gFnSc6
180	#num#	k4
bodů	bod	k1gInPc2
<g/>
,	,	kIx,
v	v	k7c6
případě	případ	k1gInSc6
stupnice	stupnice	k1gFnSc2
Kelvinovy	Kelvinův	k2eAgFnSc2d1
a	a	k8xC
Celsiovy	Celsiův	k2eAgFnSc2d1
100	#num#	k4
bodů	bod	k1gInPc2
<g/>
,	,	kIx,
tudíž	tudíž	k8xC
jeden	jeden	k4xCgInSc1
stupeň	stupeň	k1gInSc1
na	na	k7c6
Fahrenheitově	Fahrenheitův	k2eAgFnSc6d1
teplotní	teplotní	k2eAgFnSc6d1
škále	škála	k1gFnSc6
lze	lze	k6eAd1
odečíst	odečíst	k5eAaPmF
jako	jako	k9
5	#num#	k4
<g/>
/	/	kIx~
<g/>
9	#num#	k4
kelvinu	kelvin	k1gInSc2
nebo	nebo	k8xC
stupně	stupeň	k1gInSc2
Celsia	Celsius	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1
</s>
<s>
Země	zem	k1gFnPc1
používající	používající	k2eAgFnPc1d1
stupeň	stupeň	k1gInSc4
Fahrenheita	Fahrenheit	k1gMnSc4
</s>
<s>
Země	zem	k1gFnPc1
používající	používající	k2eAgFnPc1d1
jak	jak	k8xC,k8xS
stupeň	stupeň	k1gInSc1
Fahrenheita	Fahrenheit	k1gMnSc2
<g/>
,	,	kIx,
tak	tak	k9
stupeň	stupeň	k1gInSc1
Celsia	Celsius	k1gMnSc2
</s>
<s>
Země	zem	k1gFnPc1
používající	používající	k2eAgFnPc1d1
stupeň	stupeň	k1gInSc4
Celsia	Celsius	k1gMnSc2
</s>
<s>
Dnes	dnes	k6eAd1
se	se	k3xPyFc4
Fahrenheitova	Fahrenheitův	k2eAgFnSc1d1
stupnice	stupnice	k1gFnSc1
oficiálně	oficiálně	k6eAd1
používá	používat	k5eAaImIp3nS
v	v	k7c6
USA	USA	kA
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gNnPc6
závislých	závislý	k2eAgNnPc6d1
územích	území	k1gNnPc6
(	(	kIx(
<g/>
např.	např.	kA
Portoriko	Portoriko	k1gNnSc1
<g/>
,	,	kIx,
Guam	Guam	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Libérii	Libérie	k1gFnSc4
<g/>
,	,	kIx,
Belize	Beliza	k1gFnSc6
a	a	k8xC
Kajmanských	Kajmanský	k2eAgInPc6d1
ostrovech	ostrov	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
se	se	k3xPyFc4
v	v	k7c6
omezené	omezený	k2eAgFnSc6d1
míře	míra	k1gFnSc6
používá	používat	k5eAaImIp3nS
v	v	k7c6
Kanadě	Kanada	k1gFnSc6
a	a	k8xC
ve	v	k7c6
velmi	velmi	k6eAd1
omezené	omezený	k2eAgFnSc3d1
míře	míra	k1gFnSc3
na	na	k7c6
Britských	britský	k2eAgInPc6d1
ostrovech	ostrov	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Přepočet	přepočet	k1gInSc1
na	na	k7c4
jiné	jiný	k2eAgInPc4d1
stupně	stupeň	k1gInPc4
</s>
<s>
Fahrenheitova	Fahrenheitův	k2eAgFnSc1d1
a	a	k8xC
Celsiova	Celsiův	k2eAgFnSc1d1
stupnice	stupnice	k1gFnSc1
</s>
<s>
Kelvinova	Kelvinův	k2eAgFnSc1d1
stupnice	stupnice	k1gFnSc1
</s>
<s>
K	k	k7c3
</s>
<s>
=	=	kIx~
</s>
<s>
5	#num#	k4
</s>
<s>
(	(	kIx(
</s>
<s>
F	F	kA
</s>
<s>
+	+	kIx~
</s>
<s>
459	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
67	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
9	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
K	k	k7c3
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
5	#num#	k4
<g/>
(	(	kIx(
<g/>
F	F	kA
<g/>
+	+	kIx~
<g/>
459	#num#	k4
<g/>
{	{	kIx(
<g/>
,	,	kIx,
<g/>
}	}	kIx)
<g/>
67	#num#	k4
<g/>
)	)	kIx)
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
9	#num#	k4
<g/>
}}}	}}}	k?
</s>
<s>
,	,	kIx,
</s>
<s>
F	F	kA
</s>
<s>
=	=	kIx~
</s>
<s>
9	#num#	k4
</s>
<s>
K	k	k7c3
</s>
<s>
5	#num#	k4
</s>
<s>
−	−	k?
</s>
<s>
459	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
67	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
F	F	kA
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
9	#num#	k4
<g/>
K	k	k7c3
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
5	#num#	k4
<g/>
}}	}}	k?
<g/>
-	-	kIx~
<g/>
459	#num#	k4
<g/>
{	{	kIx(
<g/>
,	,	kIx,
<g/>
}	}	kIx)
<g/>
67	#num#	k4
<g/>
}	}	kIx)
</s>
<s>
,	,	kIx,
</s>
<s>
kde	kde	k6eAd1
K	K	kA
je	být	k5eAaImIp3nS
teplota	teplota	k1gFnSc1
v	v	k7c6
kelvinech	kelvin	k1gInPc6
<g/>
,	,	kIx,
F	F	kA
je	být	k5eAaImIp3nS
teplota	teplota	k1gFnSc1
ve	v	k7c6
stupních	stupeň	k1gInPc6
Fahrenheita	Fahrenheit	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Celsiova	Celsiův	k2eAgFnSc1d1
stupnice	stupnice	k1gFnSc1
</s>
<s>
C	C	kA
</s>
<s>
=	=	kIx~
</s>
<s>
5	#num#	k4
</s>
<s>
(	(	kIx(
</s>
<s>
F	F	kA
</s>
<s>
−	−	k?
</s>
<s>
32	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
9	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
C	C	kA
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
5	#num#	k4
<g/>
(	(	kIx(
<g/>
F-	F-	k1gFnSc1
<g/>
32	#num#	k4
<g/>
)	)	kIx)
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
9	#num#	k4
<g/>
}}}	}}}	k?
</s>
<s>
,	,	kIx,
</s>
<s>
F	F	kA
</s>
<s>
=	=	kIx~
</s>
<s>
9	#num#	k4
</s>
<s>
C	C	kA
</s>
<s>
5	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
32	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
F	F	kA
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
9	#num#	k4
<g/>
C	C	kA
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
5	#num#	k4
<g/>
}}	}}	k?
<g/>
+	+	kIx~
<g/>
32	#num#	k4
<g/>
}	}	kIx)
</s>
<s>
,	,	kIx,
</s>
<s>
kde	kde	k6eAd1
C	C	kA
je	být	k5eAaImIp3nS
teplota	teplota	k1gFnSc1
ve	v	k7c6
stupních	stupeň	k1gInPc6
Celsia	Celsius	k1gMnSc2
<g/>
,	,	kIx,
F	F	kA
je	být	k5eAaImIp3nS
teplota	teplota	k1gFnSc1
ve	v	k7c6
stupních	stupeň	k1gInPc6
Fahrenheita	Fahrenheit	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Réaumurova	Réaumurův	k2eAgFnSc1d1
stupnice	stupnice	k1gFnSc1
</s>
<s>
R	R	kA
</s>
<s>
=	=	kIx~
</s>
<s>
4	#num#	k4
</s>
<s>
(	(	kIx(
</s>
<s>
F	F	kA
</s>
<s>
−	−	k?
</s>
<s>
32	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
9	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
R	R	kA
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
4	#num#	k4
<g/>
(	(	kIx(
<g/>
F-	F-	k1gFnSc1
<g/>
32	#num#	k4
<g/>
)	)	kIx)
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
9	#num#	k4
<g/>
}}}	}}}	k?
</s>
<s>
,	,	kIx,
</s>
<s>
F	F	kA
</s>
<s>
=	=	kIx~
</s>
<s>
9	#num#	k4
</s>
<s>
R	R	kA
</s>
<s>
4	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
32	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
F	F	kA
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
9	#num#	k4
<g/>
R	R	kA
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
4	#num#	k4
<g/>
}}	}}	k?
<g/>
+	+	kIx~
<g/>
32	#num#	k4
<g/>
}	}	kIx)
</s>
<s>
,	,	kIx,
</s>
<s>
kde	kde	k6eAd1
R	R	kA
je	být	k5eAaImIp3nS
teplota	teplota	k1gFnSc1
ve	v	k7c6
stupních	stupeň	k1gInPc6
Réaumura	Réaumur	k1gMnSc2
<g/>
,	,	kIx,
F	F	kA
je	být	k5eAaImIp3nS
teplota	teplota	k1gFnSc1
ve	v	k7c6
stupních	stupeň	k1gInPc6
Fahrenheita	Fahrenheit	k1gMnSc2
<g/>
.	.	kIx.
</s>
