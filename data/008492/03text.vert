<p>
<s>
Směšné	směšné	k1gNnSc1	směšné
lásky	láska	k1gFnSc2	láska
je	být	k5eAaImIp3nS	být
sbírka	sbírka	k1gFnSc1	sbírka
povídek	povídka	k1gFnPc2	povídka
od	od	k7c2	od
spisovatele	spisovatel	k1gMnSc2	spisovatel
Milana	Milan	k1gMnSc2	Milan
Kundery	Kundera	k1gFnSc2	Kundera
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Toto	tento	k3xDgNnSc1	tento
dílo	dílo	k1gNnSc1	dílo
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
nejdříve	dříve	k6eAd3	dříve
jako	jako	k9	jako
soubor	soubor	k1gInSc1	soubor
tří	tři	k4xCgFnPc2	tři
knih	kniha	k1gFnPc2	kniha
<g/>
:	:	kIx,	:
Směšné	směšný	k2eAgFnSc2d1	směšná
lásky	láska	k1gFnSc2	láska
(	(	kIx(	(
<g/>
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Druhý	druhý	k4xOgInSc1	druhý
sešit	sešit	k1gInSc1	sešit
směšných	směšný	k2eAgFnPc2d1	směšná
lásek	láska	k1gFnPc2	láska
(	(	kIx(	(
<g/>
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
a	a	k8xC	a
Třetí	třetí	k4xOgInSc1	třetí
sešit	sešit	k1gInSc1	sešit
směšných	směšný	k2eAgFnPc2d1	směšná
lásek	láska	k1gFnPc2	láska
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
bylo	být	k5eAaImAgNnS	být
z	z	k7c2	z
těchto	tento	k3xDgMnPc2	tento
celkem	celkem	k6eAd1	celkem
deseti	deset	k4xCc2	deset
povídek	povídka	k1gFnPc2	povídka
vybráno	vybrat	k5eAaPmNgNnS	vybrat
sedm	sedm	k4xCc4	sedm
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
potom	potom	k6eAd1	potom
Kundera	Kundero	k1gNnPc4	Kundero
vydal	vydat	k5eAaPmAgMnS	vydat
jako	jako	k8xC	jako
Směšné	směšné	k1gNnSc4	směšné
lásky	láska	k1gFnSc2	láska
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
kniha	kniha	k1gFnSc1	kniha
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
povídky	povídka	k1gFnPc4	povídka
"	"	kIx"	"
<g/>
Nikdo	nikdo	k3yNnSc1	nikdo
se	se	k3xPyFc4	se
nebude	být	k5eNaImBp3nS	být
smát	smát	k5eAaImF	smát
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Zlaté	zlatý	k2eAgNnSc4d1	Zlaté
jablko	jablko	k1gNnSc4	jablko
věčné	věčný	k2eAgFnSc2d1	věčná
touhy	touha	k1gFnSc2	touha
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Falešný	falešný	k2eAgInSc1d1	falešný
autostop	autostop	k1gInSc1	autostop
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Symposion	symposion	k1gNnSc4	symposion
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Ať	ať	k8xS	ať
ustoupí	ustoupit	k5eAaPmIp3nP	ustoupit
staří	starý	k2eAgMnPc1d1	starý
mrtví	mrtvý	k1gMnPc1	mrtvý
mladým	mladý	k1gMnPc3	mladý
mrtvým	mrtvý	k2eAgMnPc3d1	mrtvý
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Doktor	doktor	k1gMnSc1	doktor
Havel	Havel	k1gMnSc1	Havel
po	po	k7c6	po
dvaceti	dvacet	k4xCc6	dvacet
letech	léto	k1gNnPc6	léto
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Eduard	Eduard	k1gMnSc1	Eduard
a	a	k8xC	a
Bůh	bůh	k1gMnSc1	bůh
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Povídky	povídka	k1gFnPc1	povídka
==	==	k?	==
</s>
</p>
<p>
<s>
Nikdo	nikdo	k3yNnSc1	nikdo
se	se	k3xPyFc4	se
nebude	být	k5eNaImBp3nS	být
smát	smát	k5eAaImF	smát
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
mladém	mladý	k2eAgMnSc6d1	mladý
vysokoškolském	vysokoškolský	k2eAgMnSc6d1	vysokoškolský
asistentovi	asistent	k1gMnSc6	asistent
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
má	mít	k5eAaImIp3nS	mít
napsat	napsat	k5eAaPmF	napsat
recenzi	recenze	k1gFnSc4	recenze
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
článek	článek	k1gInSc4	článek
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
článek	článek	k1gInSc1	článek
mu	on	k3xPp3gMnSc3	on
připadá	připadat	k5eAaImIp3nS	připadat
naprosto	naprosto	k6eAd1	naprosto
hrozný	hrozný	k2eAgInSc1d1	hrozný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
když	když	k8xS	když
se	se	k3xPyFc4	se
setká	setkat	k5eAaPmIp3nS	setkat
s	s	k7c7	s
autorem	autor	k1gMnSc7	autor
(	(	kIx(	(
<g/>
Pan	Pan	k1gMnSc1	Pan
Záturecký	Záturecký	k2eAgMnSc1d1	Záturecký
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nemá	mít	k5eNaImIp3nS	mít
to	ten	k3xDgNnSc1	ten
srdce	srdce	k1gNnSc1	srdce
mu	on	k3xPp3gInSc3	on
to	ten	k3xDgNnSc1	ten
naplno	naplno	k6eAd1	naplno
říct	říct	k5eAaPmF	říct
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
mu	on	k3xPp3gMnSc3	on
pořád	pořád	k6eAd1	pořád
slibuje	slibovat	k5eAaImIp3nS	slibovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
recenzi	recenze	k1gFnSc4	recenze
co	co	k9	co
nejdříve	dříve	k6eAd3	dříve
napíše	napsat	k5eAaBmIp3nS	napsat
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
ale	ale	k9	ale
začne	začít	k5eAaPmIp3nS	začít
před	před	k7c7	před
panem	pan	k1gMnSc7	pan
Zátureckým	Záturecký	k2eAgMnSc7d1	Záturecký
schovávat	schovávat	k5eAaImF	schovávat
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
si	se	k3xPyFc3	se
vymyslí	vymyslet	k5eAaPmIp3nS	vymyslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
odjíždí	odjíždět	k5eAaImIp3nS	odjíždět
do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
vrátit	vrátit	k5eAaPmF	vrátit
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
najednou	najednou	k6eAd1	najednou
nečekaně	nečekaně	k6eAd1	nečekaně
onemocní	onemocnět	k5eAaPmIp3nP	onemocnět
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
si	se	k3xPyFc3	se
pak	pak	k6eAd1	pak
všechny	všechen	k3xTgFnPc1	všechen
své	svůj	k3xOyFgFnPc1	svůj
přednášky	přednáška	k1gFnPc1	přednáška
přeloží	přeložit	k5eAaPmIp3nP	přeložit
na	na	k7c4	na
jiné	jiný	k2eAgInPc4d1	jiný
dny	den	k1gInPc4	den
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pan	Pan	k1gMnSc1	Pan
Záturecký	Záturecký	k2eAgMnSc1d1	Záturecký
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
nenechá	nechat	k5eNaPmIp3nS	nechat
odradit	odradit	k5eAaPmF	odradit
<g/>
.	.	kIx.	.
</s>
<s>
Zjistí	zjistit	k5eAaPmIp3nS	zjistit
si	se	k3xPyFc3	se
asistentovu	asistentův	k2eAgFnSc4d1	asistentův
adresu	adresa	k1gFnSc4	adresa
a	a	k8xC	a
jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
vydá	vydat	k5eAaPmIp3nS	vydat
<g/>
.	.	kIx.	.
</s>
<s>
Nalezne	naleznout	k5eAaPmIp3nS	naleznout
tam	tam	k6eAd1	tam
bohužel	bohužel	k9	bohužel
jen	jen	k9	jen
jeho	jeho	k3xOp3gFnSc4	jeho
polonahou	polonahý	k2eAgFnSc4d1	polonahá
přítelkyni	přítelkyně	k1gFnSc4	přítelkyně
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
asistentovi	asistent	k1gMnSc3	asistent
hodí	hodit	k5eAaPmIp3nS	hodit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
obviní	obvinit	k5eAaPmIp3nP	obvinit
pana	pan	k1gMnSc4	pan
Zátureckého	Záturecký	k2eAgMnSc4d1	Záturecký
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc4	jeho
dívku	dívka	k1gFnSc4	dívka
obtěžoval	obtěžovat	k5eAaImAgMnS	obtěžovat
<g/>
.	.	kIx.	.
</s>
<s>
Manželka	manželka	k1gFnSc1	manželka
pana	pan	k1gMnSc2	pan
Zátureckého	Záturecký	k2eAgInSc2d1	Záturecký
si	se	k3xPyFc3	se
to	ten	k3xDgNnSc1	ten
nechce	chtít	k5eNaImIp3nS	chtít
nechat	nechat	k5eAaPmF	nechat
líbit	líbit	k5eAaImF	líbit
<g/>
.	.	kIx.	.
</s>
<s>
Udá	udat	k5eAaPmIp3nS	udat
asistenta	asistent	k1gMnSc4	asistent
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
musí	muset	k5eAaImIp3nS	muset
před	před	k7c7	před
komisí	komise	k1gFnSc7	komise
prozradit	prozradit	k5eAaPmF	prozradit
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
to	ten	k3xDgNnSc1	ten
všechno	všechen	k3xTgNnSc1	všechen
bylo	být	k5eAaImAgNnS	být
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
paní	paní	k1gFnSc1	paní
Záturecká	Záturecký	k2eAgFnSc1d1	Záturecký
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
manželem	manžel	k1gMnSc7	manžel
vydávají	vydávat	k5eAaPmIp3nP	vydávat
do	do	k7c2	do
zdejší	zdejší	k2eAgFnSc2d1	zdejší
firmy	firma	k1gFnSc2	firma
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
má	mít	k5eAaImIp3nS	mít
pak	pak	k6eAd1	pak
Záturecký	Záturecký	k2eAgMnSc1d1	Záturecký
najít	najít	k5eAaPmF	najít
asistentovu	asistentův	k2eAgFnSc4d1	asistentův
přítelkyni	přítelkyně	k1gFnSc4	přítelkyně
<g/>
.	.	kIx.	.
</s>
<s>
Bohužel	bohužel	k9	bohužel
si	se	k3xPyFc3	se
už	už	k6eAd1	už
nepamatuje	pamatovat	k5eNaImIp3nS	pamatovat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
vypadá	vypadat	k5eAaImIp3nS	vypadat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nakonec	nakonec	k6eAd1	nakonec
vše	všechen	k3xTgNnSc4	všechen
skončí	skončit	k5eAaPmIp3nS	skončit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
asistent	asistent	k1gMnSc1	asistent
všechno	všechen	k3xTgNnSc4	všechen
paní	paní	k1gFnSc1	paní
Záturecké	Záturecký	k2eAgNnSc1d1	Záturecký
řekne	říct	k5eAaPmIp3nS	říct
a	a	k8xC	a
myslí	myslet	k5eAaImIp3nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
konečně	konečně	k6eAd1	konečně
klid	klid	k1gInSc4	klid
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
si	se	k3xPyFc3	se
jeho	jeho	k3xOp3gFnSc1	jeho
přítelkyně	přítelkyně	k1gFnSc1	přítelkyně
našla	najít	k5eAaPmAgFnS	najít
novou	nový	k2eAgFnSc4d1	nová
známost	známost	k1gFnSc4	známost
a	a	k8xC	a
s	s	k7c7	s
asistentem	asistent	k1gMnSc7	asistent
se	se	k3xPyFc4	se
rozchází	rozcházet	k5eAaImIp3nS	rozcházet
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
to	ten	k3xDgNnSc1	ten
ale	ale	k9	ale
vůbec	vůbec	k9	vůbec
nemrzí	mrzet	k5eNaImIp3nS	mrzet
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
stejně	stejně	k6eAd1	stejně
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
byla	být	k5eAaImAgFnS	být
jen	jen	k6eAd1	jen
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
udělal	udělat	k5eAaPmAgMnS	udělat
krásnou	krásný	k2eAgFnSc4d1	krásná
modelku	modelka	k1gFnSc4	modelka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zlaté	zlatý	k2eAgNnSc1d1	Zlaté
jablko	jablko	k1gNnSc1	jablko
věčné	věčný	k2eAgFnSc2d1	věčná
touhy	touha	k1gFnSc2	touha
</s>
</p>
<p>
<s>
Dva	dva	k4xCgMnPc1	dva
hlavní	hlavní	k2eAgMnPc1d1	hlavní
hrdinové	hrdina	k1gMnPc1	hrdina
<g/>
,	,	kIx,	,
muži	muž	k1gMnPc1	muž
středního	střední	k2eAgInSc2d1	střední
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
Ich	Ich	k?	Ich
forma	forma	k1gFnSc1	forma
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgMnSc1d1	hlavní
hrdina	hrdina	k1gMnSc1	hrdina
obdivuje	obdivovat	k5eAaImIp3nS	obdivovat
svého	svůj	k3xOyFgMnSc4	svůj
kamaráda	kamarád	k1gMnSc4	kamarád
Martina	Martin	k1gMnSc4	Martin
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
provádí	provádět	k5eAaImIp3nS	provádět
registráž	registráž	k1gFnSc4	registráž
a	a	k8xC	a
kontaktáž	kontaktáž	k1gFnSc4	kontaktáž
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
je	on	k3xPp3gMnPc4	on
následně	následně	k6eAd1	následně
mohl	moct	k5eAaImAgMnS	moct
sbalit	sbalit	k5eAaPmF	sbalit
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
má	mít	k5eAaImIp3nS	mít
doma	doma	k6eAd1	doma
manželku	manželka	k1gFnSc4	manželka
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
miluje	milovat	k5eAaImIp3nS	milovat
<g/>
.	.	kIx.	.
</s>
<s>
To	to	k9	to
nejlepší	dobrý	k2eAgMnPc1d3	nejlepší
již	již	k6eAd1	již
mají	mít	k5eAaImIp3nP	mít
za	za	k7c7	za
sebou	se	k3xPyFc7	se
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stále	stále	k6eAd1	stále
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
příběhu	příběh	k1gInSc6	příběh
je	být	k5eAaImIp3nS	být
vylíčena	vylíčen	k2eAgFnSc1d1	vylíčena
cesta	cesta	k1gFnSc1	cesta
za	za	k7c7	za
dobrodružstvím	dobrodružství	k1gNnSc7	dobrodružství
a	a	k8xC	a
ženami	žena	k1gFnPc7	žena
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
výsledku	výsledek	k1gInSc6	výsledek
úplně	úplně	k6eAd1	úplně
neúspěšná	úspěšný	k2eNgFnSc1d1	neúspěšná
a	a	k8xC	a
možná	možný	k2eAgFnSc1d1	možná
ani	ani	k8xC	ani
nikdo	nikdo	k3yNnSc1	nikdo
nechtěl	chtít	k5eNaImAgMnS	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
<g/>
.	.	kIx.	.
</s>
<s>
Závěr	závěr	k1gInSc1	závěr
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
hlavní	hlavní	k2eAgFnSc1d1	hlavní
je	být	k5eAaImIp3nS	být
naděje	naděje	k1gFnSc1	naděje
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
jim	on	k3xPp3gMnPc3	on
tohle	tenhle	k3xDgNnSc1	tenhle
"	"	kIx"	"
<g/>
dovádění	dovádění	k1gNnSc1	dovádění
<g/>
"	"	kIx"	"
dává	dávat	k5eAaImIp3nS	dávat
<g/>
.	.	kIx.	.
</s>
<s>
Něco	něco	k3yInSc1	něco
si	se	k3xPyFc3	se
tím	ten	k3xDgNnSc7	ten
dokazují	dokazovat	k5eAaImIp3nP	dokazovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Falešný	falešný	k2eAgInSc1d1	falešný
autostop	autostop	k1gInSc1	autostop
</s>
</p>
<p>
<s>
Nepojmenovaní	pojmenovaný	k2eNgMnPc1d1	nepojmenovaný
hrdinové	hrdina	k1gMnPc1	hrdina
<g/>
.	.	kIx.	.
</s>
<s>
Stydlivá	stydlivý	k2eAgFnSc1d1	stydlivá
dívka	dívka	k1gFnSc1	dívka
a	a	k8xC	a
její	její	k3xOp3gMnSc1	její
zkušený	zkušený	k2eAgMnSc1d1	zkušený
starší	starší	k1gMnSc1	starší
přítel	přítel	k1gMnSc1	přítel
jedou	jet	k5eAaImIp3nP	jet
na	na	k7c6	na
dovolenou	dovolená	k1gFnSc4	dovolená
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
cesty	cesta	k1gFnSc2	cesta
začnu	začít	k5eAaPmIp1nS	začít
hrát	hrát	k5eAaImF	hrát
hru	hra	k1gFnSc4	hra
na	na	k7c4	na
stopařku	stopařka	k1gFnSc4	stopařka
a	a	k8xC	a
řidiče	řidič	k1gMnPc4	řidič
<g/>
.	.	kIx.	.
</s>
<s>
Dívka	dívka	k1gFnSc1	dívka
se	se	k3xPyFc4	se
do	do	k7c2	do
role	role	k1gFnSc2	role
vžije	vžít	k5eAaPmIp3nS	vžít
a	a	k8xC	a
začne	začít	k5eAaPmIp3nS	začít
se	se	k3xPyFc4	se
chovat	chovat	k5eAaImF	chovat
prostopášnĕ	prostopášnĕ	k?	prostopášnĕ
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
příteli	přítel	k1gMnSc3	přítel
zhnusí	zhnusit	k5eAaPmIp3nS	zhnusit
<g/>
.	.	kIx.	.
</s>
<s>
Zastaví	zastavit	k5eAaPmIp3nS	zastavit
v	v	k7c6	v
hotelu	hotel	k1gInSc6	hotel
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pijí	pít	k5eAaImIp3nP	pít
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
ji	on	k3xPp3gFnSc4	on
muž	muž	k1gMnSc1	muž
systematicky	systematicky	k6eAd1	systematicky
ponižuje	ponižovat	k5eAaImIp3nS	ponižovat
<g/>
,	,	kIx,	,
právě	právě	k9	právě
kvůli	kvůli	k7c3	kvůli
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
chovat	chovat	k5eAaImF	chovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
ženy	žena	k1gFnPc1	žena
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
míval	mívat	k5eAaImAgInS	mívat
<g/>
.	.	kIx.	.
</s>
<s>
Miloval	milovat	k5eAaImAgMnS	milovat
ji	on	k3xPp3gFnSc4	on
právě	právě	k6eAd1	právě
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
byla	být	k5eAaImAgFnS	být
jiná	jiný	k2eAgFnSc1d1	jiná
<g/>
.	.	kIx.	.
</s>
<s>
Dívka	dívka	k1gFnSc1	dívka
je	být	k5eAaImIp3nS	být
zhrzena	zhrzen	k2eAgFnSc1d1	zhrzena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Symposion	symposion	k1gNnSc1	symposion
trochu	trochu	k6eAd1	trochu
připomíná	připomínat	k5eAaImIp3nS	připomínat
divadelní	divadelní	k2eAgFnSc4d1	divadelní
hru	hra	k1gFnSc4	hra
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
název	název	k1gInSc1	název
povídky	povídka	k1gFnSc2	povídka
navozuje	navozovat	k5eAaImIp3nS	navozovat
starořeckou	starořecký	k2eAgFnSc4d1	starořecká
pitku	pitka	k1gFnSc4	pitka
<g/>
.	.	kIx.	.
</s>
<s>
Pět	pět	k4xCc4	pět
postav	postava	k1gFnPc2	postava
–	–	k?	–
primář	primář	k1gMnSc1	primář
<g/>
,	,	kIx,	,
doktorka	doktorka	k1gFnSc1	doktorka
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yIgFnSc7	který
tajně	tajně	k6eAd1	tajně
chodí	chodit	k5eAaImIp3nS	chodit
<g/>
,	,	kIx,	,
Flajšman	Flajšman	k1gMnSc1	Flajšman
<g/>
,	,	kIx,	,
Havel	Havel	k1gMnSc1	Havel
a	a	k8xC	a
Alžběta	Alžběta	k1gFnSc1	Alžběta
–	–	k?	–
popíjí	popíjet	k5eAaImIp3nS	popíjet
víno	víno	k1gNnSc4	víno
v	v	k7c6	v
inspekčním	inspekční	k2eAgInSc6d1	inspekční
pokoji	pokoj	k1gInSc6	pokoj
během	během	k7c2	během
noční	noční	k2eAgFnSc2d1	noční
služby	služba	k1gFnSc2	služba
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
<g/>
.	.	kIx.	.
</s>
<s>
Alžběta	Alžběta	k1gFnSc1	Alžběta
má	mít	k5eAaImIp3nS	mít
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
doktora	doktor	k1gMnSc4	doktor
Havla	Havel	k1gMnSc4	Havel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
on	on	k3xPp3gMnSc1	on
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
je	být	k5eAaImIp3nS	být
velkým	velký	k2eAgMnSc7d1	velký
sukničkářem	sukničkář	k1gMnSc7	sukničkář
<g/>
,	,	kIx,	,
ji	on	k3xPp3gFnSc4	on
neustále	neustále	k6eAd1	neustále
odmítá	odmítat	k5eAaImIp3nS	odmítat
<g/>
.	.	kIx.	.
</s>
<s>
Večírek	večírek	k1gInSc1	večírek
málem	málem	k6eAd1	málem
skončí	skončit	k5eAaPmIp3nS	skončit
neštěstím	neštěstí	k1gNnSc7	neštěstí
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
k	k	k7c3	k
neštěstí	neštěstí	k1gNnSc3	neštěstí
vedlo	vést	k5eAaImAgNnS	vést
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
každá	každý	k3xTgFnSc1	každý
z	z	k7c2	z
postav	postava	k1gFnPc2	postava
jinou	jiný	k2eAgFnSc4d1	jiná
teorii	teorie	k1gFnSc4	teorie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ať	ať	k8xC	ať
ustoupí	ustoupit	k5eAaPmIp3nP	ustoupit
staří	starý	k2eAgMnPc1d1	starý
mrtví	mrtvý	k1gMnPc1	mrtvý
mladým	mladý	k2eAgMnPc3d1	mladý
mrtvým	mrtvý	k2eAgInSc7d1	mrtvý
</s>
</p>
<p>
<s>
Žena	žena	k1gFnSc1	žena
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
umře	umřít	k5eAaPmIp3nS	umřít
manžel	manžel	k1gMnSc1	manžel
přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
do	do	k7c2	do
města	město	k1gNnSc2	město
na	na	k7c4	na
dušičky	dušička	k1gFnPc4	dušička
<g/>
.	.	kIx.	.
</s>
<s>
Zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
díky	díky	k7c3	díky
její	její	k3xOp3gFnSc3	její
chybě	chyba	k1gFnSc3	chyba
zrušili	zrušit	k5eAaPmAgMnP	zrušit
jeho	on	k3xPp3gInSc4	on
hrob	hrob	k1gInSc4	hrob
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
návštěvě	návštěva	k1gFnSc6	návštěva
potká	potkat	k5eAaPmIp3nS	potkat
starého	starý	k1gMnSc4	starý
známého	známý	k1gMnSc4	známý
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
15	[number]	k4	15
let	léto	k1gNnPc2	léto
neviděla	vidět	k5eNaImAgFnS	vidět
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
o	o	k7c4	o
dost	dost	k6eAd1	dost
starší	starý	k2eAgFnSc4d2	starší
než	než	k8xS	než
on	on	k3xPp3gMnSc1	on
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
spolu	spolu	k6eAd1	spolu
spali	spát	k5eAaImAgMnP	spát
<g/>
.	.	kIx.	.
</s>
<s>
Jdou	jít	k5eAaImIp3nP	jít
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
na	na	k7c6	na
kávu	káva	k1gFnSc4	káva
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
oba	dva	k4xCgMnPc4	dva
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
příjemné	příjemný	k2eAgNnSc4d1	příjemné
posezení	posezení	k1gNnSc4	posezení
<g/>
,	,	kIx,	,
během	během	k7c2	během
času	čas	k1gInSc2	čas
se	se	k3xPyFc4	se
konverzace	konverzace	k1gFnSc1	konverzace
stačí	stačit	k5eAaBmIp3nS	stačit
k	k	k7c3	k
jednomu	jeden	k4xCgInSc3	jeden
cíli	cíl	k1gInSc3	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Žena	žena	k1gFnSc1	žena
parafrázuje	parafrázovat	k5eAaBmIp3nS	parafrázovat
název	název	k1gInSc4	název
povídky	povídka	k1gFnSc2	povídka
<g/>
.	.	kIx.	.
</s>
<s>
Myslí	myslet	k5eAaImIp3nS	myslet
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
vzhledem	vzhledem	k7c3	vzhledem
jejímu	její	k3xOp3gInSc3	její
věku	věk	k1gInSc3	věk
je	být	k5eAaImIp3nS	být
sex	sex	k1gInSc4	sex
nepatřičný	patřičný	k2eNgInSc4d1	nepatřičný
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
kvůli	kvůli	k7c3	kvůli
jejímu	její	k3xOp3gMnSc3	její
synovi	syn	k1gMnSc3	syn
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc4	jehož
obličej	obličej	k1gInSc4	obličej
se	se	k3xPyFc4	se
ji	on	k3xPp3gFnSc4	on
stále	stále	k6eAd1	stále
zjevuje	zjevovat	k5eAaImIp3nS	zjevovat
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
jsou	být	k5eAaImIp3nP	být
již	již	k6eAd1	již
zestarlejší	zestarlý	k2eAgFnPc1d2	zestarlý
<g/>
,	,	kIx,	,
žena	žena	k1gFnSc1	žena
nechce	chtít	k5eNaImIp3nS	chtít
sex	sex	k1gInSc4	sex
i	i	k9	i
kvůli	kvůli	k7c3	kvůli
zachování	zachování	k1gNnSc3	zachování
památky	památka	k1gFnSc2	památka
v	v	k7c6	v
mužovĕ	mužovĕ	k?	mužovĕ
hlavě	hlava	k1gFnSc6	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Muž	muž	k1gMnSc1	muž
chce	chtít	k5eAaImIp3nS	chtít
<g/>
,	,	kIx,	,
bere	brát	k5eAaImIp3nS	brát
to	ten	k3xDgNnSc1	ten
jako	jako	k9	jako
svůj	svůj	k3xOyFgInSc4	svůj
úspěch	úspěch	k1gInSc4	úspěch
<g/>
,	,	kIx,	,
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
minulost	minulost	k1gFnSc4	minulost
i	i	k8xC	i
přítomnost	přítomnost	k1gFnSc4	přítomnost
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
povolí	povolit	k5eAaPmIp3nS	povolit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sociální	sociální	k2eAgFnSc1d1	sociální
role	role	k1gFnSc1	role
ženy	žena	k1gFnSc2	žena
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
primárně	primárně	k6eAd1	primárně
matka	matka	k1gFnSc1	matka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Doktor	doktor	k1gMnSc1	doktor
Havel	Havel	k1gMnSc1	Havel
po	po	k7c6	po
dvaceti	dvacet	k4xCc6	dvacet
letech	léto	k1gNnPc6	léto
</s>
</p>
<p>
<s>
Doktor	doktor	k1gMnSc1	doktor
Havel	Havel	k1gMnSc1	Havel
je	být	k5eAaImIp3nS	být
nemocný	mocný	k2eNgMnSc1d1	nemocný
<g/>
,	,	kIx,	,
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
odjet	odjet	k5eAaPmF	odjet
do	do	k7c2	do
lázní	lázeň	k1gFnPc2	lázeň
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
mladší	mladý	k2eAgFnSc1d2	mladší
krásná	krásný	k2eAgFnSc1d1	krásná
žena	žena	k1gFnSc1	žena
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
žárlí	žárlit	k5eAaImIp3nS	žárlit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lázních	lázeň	k1gFnPc6	lázeň
potkává	potkávat	k5eAaImIp3nS	potkávat
redaktora	redaktor	k1gMnSc2	redaktor
lázeňského	lázeňský	k1gMnSc4	lázeňský
zpravodaje	zpravodaj	k1gMnSc4	zpravodaj
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
na	na	k7c4	na
něj	on	k3xPp3gNnSc4	on
pohlíží	pohlížet	k5eAaImIp3nP	pohlížet
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
obdivem	obdiv	k1gInSc7	obdiv
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
Havlovi	Havel	k1gMnSc3	Havel
líbí	líbit	k5eAaImIp3nP	líbit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
skleslý	skleslý	k2eAgMnSc1d1	skleslý
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
pohlíží	pohlížet	k5eAaImIp3nS	pohlížet
již	již	k6eAd1	již
méně	málo	k6eAd2	málo
žen	žena	k1gFnPc2	žena
se	s	k7c7	s
zájmem	zájem	k1gInSc7	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
jako	jako	k9	jako
Casanova	Casanův	k2eAgInSc2d1	Casanův
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
redaktorovi	redaktor	k1gMnSc3	redaktor
kolaudovat	kolaudovat	k5eAaBmF	kolaudovat
dívku	dívka	k1gFnSc4	dívka
<g/>
,	,	kIx,	,
výživa	výživa	k1gFnSc1	výživa
se	se	k3xPyFc4	se
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
.	.	kIx.	.
</s>
<s>
Využívá	využívat	k5eAaImIp3nS	využívat
příjezdu	příjezd	k1gInSc2	příjezd
své	svůj	k3xOyFgFnSc2	svůj
ženy	žena	k1gFnSc2	žena
<g/>
,	,	kIx,	,
po	po	k7c6	po
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
nepříjemném	příjemný	k2eNgNnSc6d1	nepříjemné
prostředí	prostředí	k1gNnSc6	prostředí
začne	začít	k5eAaPmIp3nS	začít
stýskat	stýskat	k5eAaImF	stýskat
<g/>
.	.	kIx.	.
</s>
<s>
Žena	žena	k1gFnSc1	žena
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
herečka	herečka	k1gFnSc1	herečka
<g/>
,	,	kIx,	,
najednou	najednou	k6eAd1	najednou
se	se	k3xPyFc4	se
doktorovi	doktor	k1gMnSc3	doktor
díky	díky	k7c3	díky
ní	on	k3xPp3gFnSc3	on
dostává	dostávat	k5eAaImIp3nS	dostávat
kýžené	kýžený	k2eAgFnSc3d1	kýžená
pozornosti	pozornost	k1gFnSc3	pozornost
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
znovu	znovu	k6eAd1	znovu
spokojen	spokojen	k2eAgMnSc1d1	spokojen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Eduard	Eduard	k1gMnSc1	Eduard
a	a	k8xC	a
Bůh	bůh	k1gMnSc1	bůh
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
povídka	povídka	k1gFnSc1	povídka
o	o	k7c6	o
mladém	mladý	k2eAgMnSc6d1	mladý
Eduardovi	Eduard	k1gMnSc6	Eduard
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
právě	právě	k6eAd1	právě
dostudoval	dostudovat	k5eAaPmAgMnS	dostudovat
vysokou	vysoký	k2eAgFnSc4d1	vysoká
školu	škola	k1gFnSc4	škola
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
učitelem	učitel	k1gMnSc7	učitel
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
malém	malý	k2eAgNnSc6d1	malé
českém	český	k2eAgNnSc6d1	české
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
si	se	k3xPyFc3	se
najde	najít	k5eAaPmIp3nS	najít
známost	známost	k1gFnSc4	známost
<g/>
,	,	kIx,	,
Alici	Alice	k1gFnSc4	Alice
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
zarputilou	zarputilý	k2eAgFnSc7d1	zarputilá
křesťankou	křesťanka	k1gFnSc7	křesťanka
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jaksi	jaksi	k6eAd1	jaksi
na	na	k7c4	na
protest	protest	k1gInSc4	protest
vůči	vůči	k7c3	vůči
komunismu	komunismus	k1gInSc3	komunismus
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
víra	víra	k1gFnSc1	víra
však	však	k9	však
vlastně	vlastně	k9	vlastně
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
jediné	jediný	k2eAgFnSc6d1	jediná
ideji	idea	k1gFnSc6	idea
<g/>
:	:	kIx,	:
chce	chtít	k5eAaImIp3nS	chtít
za	za	k7c4	za
každou	každý	k3xTgFnSc4	každý
cenu	cena	k1gFnSc4	cena
dodržet	dodržet	k5eAaPmF	dodržet
šesté	šestý	k4xOgFnSc2	šestý
(	(	kIx(	(
<g/>
respektive	respektive	k9	respektive
sedmé	sedmý	k4xOgNnSc1	sedmý
<g/>
)	)	kIx)	)
přikázání	přikázání	k1gNnPc2	přikázání
a	a	k8xC	a
vyhnout	vyhnout	k5eAaPmF	vyhnout
se	se	k3xPyFc4	se
mimomanželskému	mimomanželský	k2eAgInSc3d1	mimomanželský
sexu	sex	k1gInSc3	sex
<g/>
.	.	kIx.	.
</s>
<s>
Eduard	Eduard	k1gMnSc1	Eduard
by	by	kYmCp3nS	by
rád	rád	k6eAd1	rád
svůj	svůj	k3xOyFgInSc4	svůj
vztah	vztah	k1gInSc4	vztah
s	s	k7c7	s
Alicí	Alice	k1gFnSc7	Alice
posunul	posunout	k5eAaPmAgInS	posunout
na	na	k7c4	na
fyzickou	fyzický	k2eAgFnSc4d1	fyzická
úroveň	úroveň	k1gFnSc4	úroveň
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Alice	Alice	k1gFnPc1	Alice
veškerým	veškerý	k3xTgInSc7	veškerý
jeho	jeho	k3xOp3gInPc3	jeho
pokusům	pokus	k1gInPc3	pokus
odolává	odolávat	k5eAaImIp3nS	odolávat
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
ho	on	k3xPp3gInSc2	on
pak	pak	k6eAd1	pak
zeptá	zeptat	k5eAaPmIp3nS	zeptat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
věří	věřit	k5eAaImIp3nS	věřit
v	v	k7c4	v
Boha	bůh	k1gMnSc4	bůh
<g/>
,	,	kIx,	,
Eduard	Eduard	k1gMnSc1	Eduard
nechtěje	chtít	k5eNaImSgInS	chtít
Alici	Alice	k1gFnSc4	Alice
odradit	odradit	k5eAaPmF	odradit
zalže	zalhat	k5eAaPmIp3nS	zalhat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ano	ano	k9	ano
<g/>
,	,	kIx,	,
a	a	k8xC	a
začne	začít	k5eAaPmIp3nS	začít
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
chodit	chodit	k5eAaImF	chodit
do	do	k7c2	do
kostela	kostel	k1gInSc2	kostel
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
však	však	k9	však
donese	donést	k5eAaPmIp3nS	donést
do	do	k7c2	do
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
Eduard	Eduard	k1gMnSc1	Eduard
zaměstnán	zaměstnat	k5eAaPmNgMnS	zaměstnat
<g/>
,	,	kIx,	,
a	a	k8xC	a
způsobí	způsobit	k5eAaPmIp3nS	způsobit
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc1	ten
velké	velký	k2eAgInPc4d1	velký
problémy	problém	k1gInPc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Stane	stanout	k5eAaPmIp3nS	stanout
se	se	k3xPyFc4	se
jakýmsi	jakýsi	k3yIgMnSc7	jakýsi
mučedníkem	mučedník	k1gMnSc7	mučedník
a	a	k8xC	a
Alice	Alice	k1gFnSc1	Alice
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
poleví	polevit	k5eAaPmIp3nS	polevit
<g/>
.	.	kIx.	.
</s>
<s>
Eduard	Eduard	k1gMnSc1	Eduard
si	se	k3xPyFc3	se
však	však	k9	však
později	pozdě	k6eAd2	pozdě
uvědomí	uvědomit	k5eAaPmIp3nS	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
měl	mít	k5eAaImAgMnS	mít
Alici	Alice	k1gFnSc4	Alice
raději	rád	k6eAd2	rád
<g/>
,	,	kIx,	,
když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
neústupná	ústupný	k2eNgFnSc1d1	neústupná
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
mysli	mysl	k1gFnSc6	mysl
ztratila	ztratit	k5eAaPmAgFnS	ztratit
tvář	tvář	k1gFnSc1	tvář
<g/>
.	.	kIx.	.
</s>
</p>
