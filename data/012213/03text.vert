<p>
<s>
Krasové	krasový	k2eAgNnSc1d1	krasové
jezero	jezero	k1gNnSc1	jezero
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
rozpouštěním	rozpouštění	k1gNnPc3	rozpouštění
podloží	podloží	k1gNnSc2	podloží
<g/>
.	.	kIx.	.
</s>
<s>
Krasová	krasový	k2eAgFnSc1d1	krasová
sníženina	sníženina	k1gFnSc1	sníženina
je	být	k5eAaImIp3nS	být
poté	poté	k6eAd1	poté
vyplněna	vyplnit	k5eAaPmNgFnS	vyplnit
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
tento	tento	k3xDgInSc4	tento
druh	druh	k1gInSc4	druh
jezer	jezero	k1gNnPc2	jezero
patří	patřit	k5eAaImIp3nP	patřit
např.	např.	kA	např.
jezera	jezero	k1gNnSc2	jezero
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
závrtová	závrtový	k2eAgFnSc1d1	závrtová
(	(	kIx(	(
<g/>
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
ucpáním	ucpání	k1gNnSc7	ucpání
dna	dno	k1gNnSc2	dno
závrtu	závrt	k1gInSc6	závrt
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
poljová	poljový	k2eAgFnSc1d1	poljový
(	(	kIx(	(
<g/>
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
nedostatečným	dostatečný	k2eNgNnSc7d1	nedostatečné
odvodněním	odvodnění	k1gNnSc7	odvodnění
polje	polj	k1gFnSc2	polj
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
solná	solný	k2eAgFnSc1d1	solná
(	(	kIx(	(
<g/>
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
rozpuštěním	rozpuštění	k1gNnSc7	rozpuštění
solného	solný	k2eAgInSc2d1	solný
pně	peň	k1gInSc2	peň
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Příklady	příklad	k1gInPc1	příklad
==	==	k?	==
</s>
</p>
<p>
<s>
Plitvická	Plitvická	k1gFnSc1	Plitvická
jezera	jezero	k1gNnSc2	jezero
</s>
</p>
<p>
<s>
Skadarské	skadarský	k2eAgNnSc1d1	Skadarské
jezero	jezero	k1gNnSc1	jezero
</s>
</p>
