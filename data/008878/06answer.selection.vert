<s>
Tyrkys	tyrkys	k1gInSc1	tyrkys
je	být	k5eAaImIp3nS	být
matný	matný	k2eAgInSc1d1	matný
modro-zelený	modroelený	k2eAgInSc1d1	modro-zelený
minerál	minerál	k1gInSc1	minerál
<g/>
,	,	kIx,	,
chemicky	chemicky	k6eAd1	chemicky
tetrahydrát	tetrahydrát	k1gInSc1	tetrahydrát
tetrakis	tetrakis	k1gFnSc2	tetrakis
<g/>
(	(	kIx(	(
<g/>
fosforečnanu	fosforečnan	k1gInSc2	fosforečnan
<g/>
)	)	kIx)	)
oktakis	oktakis	k1gFnSc1	oktakis
<g/>
(	(	kIx(	(
<g/>
hydroxidu	hydroxid	k1gInSc2	hydroxid
<g/>
)	)	kIx)	)
hexahlinito-měďnatého	hexahlinitoěďnatý	k2eAgMnSc2d1	hexahlinito-měďnatý
<g/>
,	,	kIx,	,
či	či	k8xC	či
CuAl	CuAl	k1gInSc1	CuAl
<g/>
6	[number]	k4	6
<g/>
(	(	kIx(	(
<g/>
PO	Po	kA	Po
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
4	[number]	k4	4
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
8.4	[number]	k4	8.4
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
.	.	kIx.	.
</s>
