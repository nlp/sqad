<p>
<s>
Tyrkys	tyrkys	k1gInSc1	tyrkys
je	být	k5eAaImIp3nS	být
matný	matný	k2eAgInSc1d1	matný
modro-zelený	modroelený	k2eAgInSc1d1	modro-zelený
minerál	minerál	k1gInSc1	minerál
<g/>
,	,	kIx,	,
chemicky	chemicky	k6eAd1	chemicky
tetrahydrát	tetrahydrát	k1gInSc1	tetrahydrát
tetrakis	tetrakis	k1gFnSc2	tetrakis
<g/>
(	(	kIx(	(
<g/>
fosforečnanu	fosforečnan	k1gInSc2	fosforečnan
<g/>
)	)	kIx)	)
oktakis	oktakis	k1gFnSc1	oktakis
<g/>
(	(	kIx(	(
<g/>
hydroxidu	hydroxid	k1gInSc2	hydroxid
<g/>
)	)	kIx)	)
hexahlinito-měďnatého	hexahlinitoěďnatý	k2eAgMnSc2d1	hexahlinito-měďnatý
<g/>
,	,	kIx,	,
či	či	k8xC	či
CuAl	CuAl	k1gInSc1	CuAl
<g/>
6	[number]	k4	6
<g/>
(	(	kIx(	(
<g/>
PO	Po	kA	Po
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
4	[number]	k4	4
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
8.4	[number]	k4	8.4
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
znám	znám	k2eAgMnSc1d1	znám
pod	pod	k7c7	pod
mnoha	mnoho	k4c2	mnoho
jmény	jméno	k1gNnPc7	jméno
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
uchytilo	uchytit	k5eAaPmAgNnS	uchytit
se	se	k3xPyFc4	se
až	až	k6eAd1	až
jméno	jméno	k1gNnSc1	jméno
tyrkys	tyrkys	k1gInSc1	tyrkys
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
francouzského	francouzský	k2eAgMnSc2d1	francouzský
turques	turques	k1gMnSc1	turques
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
bylo	být	k5eAaImAgNnS	být
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
Turky	Turek	k1gMnPc4	Turek
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
kámen	kámen	k1gInSc1	kámen
prvotně	prvotně	k6eAd1	prvotně
přivážel	přivážet	k5eAaImAgInS	přivážet
z	z	k7c2	z
Turecka	Turecko	k1gNnSc2	Turecko
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
z	z	k7c2	z
historických	historický	k2eAgNnPc2d1	historické
perských	perský	k2eAgNnPc2d1	perské
nalezišť	naleziště	k1gNnPc2	naleziště
<g/>
.	.	kIx.	.
</s>
<s>
Plinius	Plinius	k1gMnSc1	Plinius
starší	starší	k1gMnSc1	starší
tento	tento	k3xDgInSc4	tento
minerál	minerál	k1gInSc4	minerál
označoval	označovat	k5eAaImAgMnS	označovat
jako	jako	k9	jako
Callait	Callait	k1gMnSc1	Callait
a	a	k8xC	a
Aztékové	Azték	k1gMnPc1	Azték
jej	on	k3xPp3gMnSc4	on
znali	znát	k5eAaImAgMnP	znát
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Chalchihuitl	Chalchihuitl	k1gFnSc2	Chalchihuitl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
==	==	k?	==
</s>
</p>
<p>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
sekundární	sekundární	k2eAgInSc4d1	sekundární
minerál	minerál	k1gInSc4	minerál
vznikající	vznikající	k2eAgInSc4d1	vznikající
v	v	k7c6	v
přípovrchových	přípovrchův	k2eAgFnPc6d1	přípovrchův
částech	část	k1gFnPc6	část
hornin	hornina	k1gFnPc2	hornina
bohatých	bohatý	k2eAgInPc2d1	bohatý
na	na	k7c4	na
vodu	voda	k1gFnSc4	voda
a	a	k8xC	a
se	s	k7c7	s
zvýšeným	zvýšený	k2eAgInSc7d1	zvýšený
obsahem	obsah	k1gInSc7	obsah
mědi	měď	k1gFnSc2	měď
a	a	k8xC	a
fosforu	fosfor	k1gInSc2	fosfor
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
v	v	k7c6	v
sedimentech	sediment	k1gInPc6	sediment
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
v	v	k7c6	v
pískovcích	pískovec	k1gInPc6	pískovec
<g/>
.	.	kIx.	.
</s>
<s>
Magmatického	magmatický	k2eAgInSc2d1	magmatický
původu	původ	k1gInSc2	původ
bývá	bývat	k5eAaImIp3nS	bývat
ve	v	k7c6	v
vulkanitech	vulkanit	k1gInPc6	vulkanit
bohatých	bohatý	k2eAgInPc6d1	bohatý
na	na	k7c4	na
dutiny	dutina	k1gFnPc4	dutina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
rád	rád	k6eAd1	rád
vyplňuje	vyplňovat	k5eAaImIp3nS	vyplňovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
Fyzikální	fyzikální	k2eAgFnPc1d1	fyzikální
vlastnosti	vlastnost	k1gFnPc1	vlastnost
<g/>
:	:	kIx,	:
Tvrdost	tvrdost	k1gFnSc1	tvrdost
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
matný	matný	k2eAgInSc1d1	matný
<g/>
,	,	kIx,	,
hustota	hustota	k1gFnSc1	hustota
2,6	[number]	k4	2,6
<g/>
–	–	k?	–
<g/>
2,9	[number]	k4	2,9
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
cm3	cm3	k4	cm3
<g/>
,	,	kIx,	,
štěpnost	štěpnost	k1gFnSc1	štěpnost
dokonalá	dokonalý	k2eAgFnSc1d1	dokonalá
podle	podle	k7c2	podle
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
a	a	k8xC	a
dobrá	dobrá	k9	dobrá
dle	dle	k7c2	dle
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
10	[number]	k4	10
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
lom	lom	k1gInSc1	lom
lasturnatý	lasturnatý	k2eAgInSc1d1	lasturnatý
až	až	k6eAd1	až
nerovný	rovný	k2eNgInSc1d1	nerovný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Optické	optický	k2eAgFnPc1d1	optická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
<g/>
:	:	kIx,	:
Barva	barva	k1gFnSc1	barva
<g/>
:	:	kIx,	:
tyrkysově	tyrkysově	k6eAd1	tyrkysově
modrá	modrý	k2eAgFnSc1d1	modrá
až	až	k8xS	až
modrozelená	modrozelený	k2eAgFnSc1d1	modrozelená
<g/>
.	.	kIx.	.
</s>
<s>
Lesk	lesk	k1gInSc1	lesk
voskový	voskový	k2eAgInSc1d1	voskový
<g/>
,	,	kIx,	,
průhlednost	průhlednost	k1gFnSc1	průhlednost
<g/>
:	:	kIx,	:
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
i	i	k9	i
průhledný	průhledný	k2eAgInSc1d1	průhledný
nebo	nebo	k8xC	nebo
průsvitný	průsvitný	k2eAgInSc1d1	průsvitný
<g/>
,	,	kIx,	,
vryp	vryp	k1gInSc1	vryp
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
,	,	kIx,	,
s	s	k7c7	s
nádechem	nádech	k1gInSc7	nádech
do	do	k7c2	do
modra	modro	k1gNnSc2	modro
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chemické	chemický	k2eAgFnPc1d1	chemická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
<g/>
:	:	kIx,	:
Složení	složení	k1gNnSc1	složení
<g/>
:	:	kIx,	:
Al	ala	k1gFnPc2	ala
19,90	[number]	k4	19,90
%	%	kIx~	%
<g/>
,	,	kIx,	,
Cu	Cu	k1gFnSc1	Cu
7,81	[number]	k4	7,81
%	%	kIx~	%
<g/>
,	,	kIx,	,
P	P	kA	P
15,23	[number]	k4	15,23
%	%	kIx~	%
<g/>
,	,	kIx,	,
H	H	kA	H
1,98	[number]	k4	1,98
%	%	kIx~	%
<g/>
,	,	kIx,	,
O	o	k7c6	o
55,07	[number]	k4	55,07
%	%	kIx~	%
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
rozpustný	rozpustný	k2eAgMnSc1d1	rozpustný
v	v	k7c6	v
HCl	HCl	k1gFnSc6	HCl
<g/>
.	.	kIx.	.
</s>
<s>
Zahřívá	zahřívat	k5eAaImIp3nS	zahřívat
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
tyrkys	tyrkys	k1gInSc1	tyrkys
v	v	k7c6	v
uzavřené	uzavřený	k2eAgFnSc6d1	uzavřená
nádobě	nádoba	k1gFnSc6	nádoba
<g/>
,	,	kIx,	,
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
vodu	voda	k1gFnSc4	voda
a	a	k8xC	a
barví	barvit	k5eAaImIp3nS	barvit
se	se	k3xPyFc4	se
dočerna	dočerna	k6eAd1	dočerna
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
dmuchavkou	dmuchavka	k1gFnSc7	dmuchavka
se	se	k3xPyFc4	se
netaví	tavit	k5eNaImIp3nS	tavit
a	a	k8xC	a
nebarví	barvit	k5eNaImIp3nS	barvit
plynový	plynový	k2eAgInSc1d1	plynový
plamen	plamen	k1gInSc1	plamen
zeleně	zeleň	k1gFnSc2	zeleň
-	-	kIx~	-
díky	díky	k7c3	díky
této	tento	k3xDgFnSc3	tento
reakci	reakce	k1gFnSc3	reakce
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
snadno	snadno	k6eAd1	snadno
odlišit	odlišit	k5eAaPmF	odlišit
napodobeniny	napodobenina	k1gFnPc4	napodobenina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odrůdy	odrůda	k1gFnSc2	odrůda
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Agaphit	Agaphit	k2eAgMnSc1d1	Agaphit
===	===	k?	===
</s>
</p>
<p>
<s>
Agaphit	Agaphit	k1gInSc1	Agaphit
je	být	k5eAaImIp3nS	být
odrůda	odrůda	k1gFnSc1	odrůda
tyrkysu	tyrkys	k1gInSc2	tyrkys
s	s	k7c7	s
výrazným	výrazný	k2eAgInSc7d1	výrazný
skelným	skelný	k2eAgInSc7d1	skelný
leskem	lesk	k1gInSc7	lesk
namísto	namísto	k7c2	namísto
klasického	klasický	k2eAgInSc2d1	klasický
voskového	voskový	k2eAgInSc2d1	voskový
<g/>
.	.	kIx.	.
</s>
<s>
Jediné	jediný	k2eAgNnSc1d1	jediné
naleziště	naleziště	k1gNnSc1	naleziště
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Íránu	Írán	k1gInSc6	Írán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rashleighit	Rashleighit	k2eAgMnSc1d1	Rashleighit
===	===	k?	===
</s>
</p>
<p>
<s>
Rashleighit	Rashleighit	k1gInSc1	Rashleighit
je	být	k5eAaImIp3nS	být
železem	železo	k1gNnSc7	železo
bohatá	bohatý	k2eAgFnSc1d1	bohatá
odrůda	odrůda	k1gFnSc1	odrůda
tyrkysu	tyrkys	k1gInSc2	tyrkys
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
mezičlen	mezičlen	k1gInSc4	mezičlen
mezi	mezi	k7c7	mezi
tyrkysem	tyrkys	k1gInSc7	tyrkys
a	a	k8xC	a
chalkosideritem	chalkosiderit	k1gInSc7	chalkosiderit
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
popsán	popsat	k5eAaPmNgInS	popsat
z	z	k7c2	z
lokality	lokalita	k1gFnSc2	lokalita
jeho	on	k3xPp3gInSc2	on
nálezu	nález	k1gInSc2	nález
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
Bunny	Bunn	k1gInPc7	Bunn
Mine	minout	k5eAaImIp3nS	minout
<g/>
,	,	kIx,	,
Cornwall	Cornwall	k1gInSc1	Cornwall
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
</s>
</p>
<p>
<s>
==	==	k?	==
Parageneze	parageneze	k1gFnSc2	parageneze
==	==	k?	==
</s>
</p>
<p>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
společně	společně	k6eAd1	společně
s	s	k7c7	s
Alofánem	Alofán	k1gInSc7	Alofán
<g/>
,	,	kIx,	,
Chalcedonem	chalcedon	k1gInSc7	chalcedon
<g/>
,	,	kIx,	,
Kaolinitem	kaolinit	k1gInSc7	kaolinit
<g/>
,	,	kIx,	,
Limonitem	limonit	k1gInSc7	limonit
<g/>
,	,	kIx,	,
Montmorillonitem	Montmorillonit	k1gInSc7	Montmorillonit
<g/>
,	,	kIx,	,
Pyritem	pyrit	k1gInSc7	pyrit
a	a	k8xC	a
Wavellitem	wavellit	k1gInSc7	wavellit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
U	u	k7c2	u
města	město	k1gNnSc2	město
Vielsalm	Vielsalma	k1gFnPc2	Vielsalma
<g/>
,	,	kIx,	,
v	v	k7c6	v
masívu	masív	k1gInSc6	masív
Stavelot	Stavelota	k1gFnPc2	Stavelota
v	v	k7c6	v
belgických	belgický	k2eAgFnPc6d1	belgická
Ardénách	Ardéna	k1gFnPc6	Ardéna
-	-	kIx~	-
naleziště	naleziště	k1gNnSc2	naleziště
až	až	k9	až
5	[number]	k4	5
<g/>
mm	mm	kA	mm
velikých	veliký	k2eAgInPc2d1	veliký
ledvinitých	ledvinitý	k2eAgInPc2d1	ledvinitý
agregátů	agregát	k1gInPc2	agregát
</s>
</p>
<p>
<s>
Apache	Apache	k6eAd1	Apache
Canyon	Canyon	k1gMnSc1	Canyon
Mines	Mines	k1gMnSc1	Mines
v	v	k7c6	v
Tyrkysových	tyrkysový	k2eAgFnPc6d1	tyrkysová
Horách	hora	k1gFnPc6	hora
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
<g/>
,	,	kIx,	,
USA	USA	kA	USA
-	-	kIx~	-
vzácné	vzácný	k2eAgInPc1d1	vzácný
bledě	bledě	k6eAd1	bledě
modrozelené	modrozelený	k2eAgInPc1d1	modrozelený
veliké	veliký	k2eAgInPc1d1	veliký
kusové	kusový	k2eAgInPc1d1	kusový
krystaly	krystal	k1gInPc1	krystal
<g/>
,	,	kIx,	,
pseudomorfní	pseudomorfní	k2eAgInPc1d1	pseudomorfní
po	po	k7c6	po
krystalech	krystal	k1gInPc6	krystal
berylu	beryl	k1gInSc2	beryl
</s>
</p>
<p>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Nishapur	Nishapura	k1gFnPc2	Nishapura
<g/>
,	,	kIx,	,
na	na	k7c4	na
2012	[number]	k4	2012
metrů	metr	k1gInPc2	metr
vysokém	vysoký	k2eAgInSc6d1	vysoký
vyrcholku	vyrcholek	k1gInSc6	vyrcholek
Ali-mersai	Aliersa	k1gFnSc2	Ali-mersa
<g/>
,	,	kIx,	,
vzdálený	vzdálený	k2eAgInSc1d1	vzdálený
deset	deset	k4xCc4	deset
kilometrů	kilometr	k1gInPc2	kilometr
od	od	k7c2	od
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Mashhad	Mashhad	k1gInSc1	Mashhad
provincie	provincie	k1gFnSc2	provincie
Khorasan	Khorasana	k1gFnPc2	Khorasana
v	v	k7c6	v
Íránu	Írán	k1gInSc6	Írán
-	-	kIx~	-
toto	tento	k3xDgNnSc1	tento
prastaré	prastarý	k2eAgNnSc1d1	prastaré
naleziště	naleziště	k1gNnSc1	naleziště
vydává	vydávat	k5eAaImIp3nS	vydávat
vzorky	vzorek	k1gInPc4	vzorek
nebesky	nebesky	k6eAd1	nebesky
modré	modrý	k2eAgFnPc4d1	modrá
barvy	barva	k1gFnPc4	barva
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
po	po	k7c6	po
následné	následný	k2eAgFnSc6d1	následná
dehydrataci	dehydratace	k1gFnSc6	dehydratace
zahřátím	zahřátí	k1gNnSc7	zahřátí
mění	měnit	k5eAaImIp3nS	měnit
barvu	barva	k1gFnSc4	barva
na	na	k7c4	na
nádhernou	nádherný	k2eAgFnSc4d1	nádherná
sytě	sytě	k6eAd1	sytě
modrozelenou	modrozelený	k2eAgFnSc4d1	modrozelená
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
velmi	velmi	k6eAd1	velmi
vzácný	vzácný	k2eAgInSc4d1	vzácný
a	a	k8xC	a
cenný	cenný	k2eAgInSc4d1	cenný
nerost	nerost	k1gInSc4	nerost
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
byl	být	k5eAaImAgInS	být
používán	používat	k5eAaImNgInS	používat
jako	jako	k9	jako
klenot	klenot	k1gInSc4	klenot
a	a	k8xC	a
dekorační	dekorační	k2eAgInSc4d1	dekorační
kámen	kámen	k1gInSc4	kámen
po	po	k7c6	po
tisíce	tisíc	k4xCgInSc2	tisíc
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgFnPc4	svůj
oblíbenosti	oblíbenost	k1gFnPc4	oblíbenost
vděčí	vděčit	k5eAaImIp3nS	vděčit
své	svůj	k3xOyFgFnSc3	svůj
neobyčejné	obyčejný	k2eNgFnSc3d1	neobyčejná
barvě	barva	k1gFnSc3	barva
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
způsobená	způsobený	k2eAgFnSc1d1	způsobená
příměsí	příměs	k1gFnSc7	příměs
mědi	měď	k1gFnSc2	měď
<g/>
.	.	kIx.	.
</s>
<s>
Aztékové	Azték	k1gMnPc1	Azték
vkládali	vkládat	k5eAaImAgMnP	vkládat
tyrkys	tyrkys	k1gInSc4	tyrkys
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
zlatem	zlato	k1gNnSc7	zlato
<g/>
,	,	kIx,	,
křemenem	křemen	k1gInSc7	křemen
<g/>
,	,	kIx,	,
malachitem	malachit	k1gInSc7	malachit
<g/>
,	,	kIx,	,
jadeitem	jadeit	k1gInSc7	jadeit
<g/>
,	,	kIx,	,
korály	korál	k1gMnPc7	korál
a	a	k8xC	a
lasturami	lastura	k1gFnPc7	lastura
<g/>
,	,	kIx,	,
do	do	k7c2	do
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
ceremoniálních	ceremoniální	k2eAgInPc2d1	ceremoniální
mozaikových	mozaikový	k2eAgInPc2d1	mozaikový
objektů	objekt	k1gInPc2	objekt
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
masky	maska	k1gFnPc1	maska
(	(	kIx(	(
<g/>
některé	některý	k3yIgNnSc4	některý
s	s	k7c7	s
lidskou	lidský	k2eAgFnSc7d1	lidská
lebkou	lebka	k1gFnSc7	lebka
jako	jako	k8xC	jako
základna	základna	k1gFnSc1	základna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nožů	nůž	k1gInPc2	nůž
a	a	k8xC	a
štítů	štít	k1gInPc2	štít
<g/>
.	.	kIx.	.
</s>
<s>
Přírodní	přírodní	k2eAgFnSc1d1	přírodní
pryskyřice	pryskyřice	k1gFnSc1	pryskyřice
<g/>
,	,	kIx,	,
asfalt	asfalt	k1gInSc1	asfalt
a	a	k8xC	a
vosk	vosk	k1gInSc1	vosk
byly	být	k5eAaImAgFnP	být
použity	použít	k5eAaPmNgInP	použít
pro	pro	k7c4	pro
spojení	spojení	k1gNnSc4	spojení
tyrkysu	tyrkys	k1gInSc2	tyrkys
na	na	k7c4	na
základní	základní	k2eAgInSc4d1	základní
materiál	materiál	k1gInSc4	materiál
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
obvykle	obvykle	k6eAd1	obvykle
dřevo	dřevo	k1gNnSc1	dřevo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
používali	používat	k5eAaImAgMnP	používat
kosti	kost	k1gFnPc4	kost
nebo	nebo	k8xC	nebo
mušle	mušle	k1gFnPc4	mušle
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Persii	Persie	k1gFnSc6	Persie
byl	být	k5eAaImAgInS	být
tyrkys	tyrkys	k1gInSc4	tyrkys
národním	národní	k2eAgInSc7d1	národní
kamenem	kámen	k1gInSc7	kámen
po	po	k7c6	po
tisíciletí	tisíciletí	k1gNnSc6	tisíciletí
<g/>
.	.	kIx.	.
</s>
<s>
Používal	používat	k5eAaImAgMnS	používat
se	se	k3xPyFc4	se
převážně	převážně	k6eAd1	převážně
na	na	k7c4	na
dekoraci	dekorace	k1gFnSc4	dekorace
objektů	objekt	k1gInPc2	objekt
(	(	kIx(	(
<g/>
např	např	kA	např
<g/>
:	:	kIx,	:
do	do	k7c2	do
uzdy	uzda	k1gFnSc2	uzda
od	od	k7c2	od
turbanů	turban	k1gInPc2	turban
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mešit	mešita	k1gFnPc2	mešita
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
významných	významný	k2eAgFnPc2d1	významná
staveb	stavba	k1gFnPc2	stavba
uvnitř	uvnitř	k6eAd1	uvnitř
i	i	k9	i
venku	venku	k6eAd1	venku
<g/>
.	.	kIx.	.
</s>
<s>
Perský	perský	k2eAgInSc4d1	perský
styl	styl	k1gInSc4	styl
a	a	k8xC	a
použití	použití	k1gNnSc4	použití
tyrkysu	tyrkys	k1gInSc2	tyrkys
byl	být	k5eAaImAgInS	být
později	pozdě	k6eAd2	pozdě
zanesen	zanést	k5eAaPmNgInS	zanést
do	do	k7c2	do
Indie	Indie	k1gFnSc2	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
vliv	vliv	k1gInSc1	vliv
lze	lze	k6eAd1	lze
vidět	vidět	k5eAaImF	vidět
na	na	k7c6	na
zlatých	zlatý	k2eAgInPc6d1	zlatý
špercích	šperk	k1gInPc6	šperk
(	(	kIx(	(
<g/>
společně	společně	k6eAd1	společně
s	s	k7c7	s
rubíny	rubín	k1gInPc7	rubín
a	a	k8xC	a
diamanty	diamant	k1gInPc7	diamant
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
budovách	budova	k1gFnPc6	budova
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
Tádž	Tádž	k1gFnSc1	Tádž
Mahal	Mahal	k1gInSc1	Mahal
<g/>
.	.	kIx.	.
</s>
<s>
Perský	perský	k2eAgInSc1d1	perský
tyrkys	tyrkys	k1gInSc1	tyrkys
měl	mít	k5eAaImAgInS	mít
často	často	k6eAd1	často
rytiny	rytina	k1gFnSc2	rytina
se	s	k7c7	s
zbožnými	zbožný	k2eAgNnPc7d1	zbožné
slovy	slovo	k1gNnPc7	slovo
v	v	k7c6	v
arabštině	arabština	k1gFnSc6	arabština
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgInP	být
poté	poté	k6eAd1	poté
vykládané	vykládaný	k2eAgNnSc1d1	vykládané
zlatem	zlato	k1gNnSc7	zlato
<g/>
.	.	kIx.	.
</s>
<s>
Egyptské	egyptský	k2eAgNnSc1d1	egyptské
použití	použití	k1gNnSc1	použití
tyrkysu	tyrkys	k1gInSc2	tyrkys
sahá	sahat	k5eAaImIp3nS	sahat
hluboko	hluboko	k6eAd1	hluboko
do	do	k7c2	do
minulosti	minulost	k1gFnSc2	minulost
prvních	první	k4xOgFnPc2	první
civilizací	civilizace	k1gFnPc2	civilizace
<g/>
,	,	kIx,	,
ne	ne	k9	ne
<g/>
-li	i	k?	-li
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nejvíce	hodně	k6eAd3	hodně
známé	známý	k2eAgInPc1d1	známý
kusy	kus	k1gInPc1	kus
tyrkysu	tyrkys	k1gInSc2	tyrkys
jsou	být	k5eAaImIp3nP	být
ty	ten	k3xDgFnPc1	ten
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
objeveny	objevit	k5eAaPmNgFnP	objevit
z	z	k7c2	z
Tutanchamonova	Tutanchamonův	k2eAgInSc2d1	Tutanchamonův
hrobu	hrob	k1gInSc2	hrob
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
zdobí	zdobit	k5eAaImIp3nP	zdobit
prsteny	prsten	k1gInPc4	prsten
a	a	k8xC	a
náhrdelníky	náhrdelník	k1gInPc4	náhrdelník
<g/>
,	,	kIx,	,
vše	všechen	k3xTgNnSc1	všechen
společně	společně	k6eAd1	společně
se	s	k7c7	s
zlatem	zlato	k1gNnSc7	zlato
<g/>
.	.	kIx.	.
</s>
<s>
Klenot	klenot	k1gInSc1	klenot
byl	být	k5eAaImAgInS	být
zpracováván	zpracovávat	k5eAaImNgInS	zpracovávat
do	do	k7c2	do
kuliček	kulička	k1gFnPc2	kulička
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
používaly	používat	k5eAaImAgFnP	používat
jako	jako	k8xS	jako
výplň	výplň	k1gFnSc1	výplň
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
ho	on	k3xPp3gMnSc4	on
vyřezávali	vyřezávat	k5eAaImAgMnP	vyřezávat
do	do	k7c2	do
motivu	motiv	k1gInSc2	motiv
skaraba	skarab	k1gMnSc2	skarab
<g/>
,	,	kIx,	,
doprovázený	doprovázený	k2eAgInSc1d1	doprovázený
karneolem	karneol	k1gInSc7	karneol
<g/>
,	,	kIx,	,
lapisem	lapis	k1gInSc7	lapis
lazuli	lazule	k1gFnSc4	lazule
a	a	k8xC	a
barevným	barevný	k2eAgNnSc7d1	barevné
sklem	sklo	k1gNnSc7	sklo
<g/>
.	.	kIx.	.
</s>
<s>
Tyrkys	tyrkys	k1gInSc1	tyrkys
byl	být	k5eAaImAgInS	být
spojený	spojený	k2eAgInSc1d1	spojený
s	s	k7c7	s
bohyní	bohyně	k1gFnSc7	bohyně
Hathor	Hathora	k1gFnPc2	Hathora
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
natolik	natolik	k6eAd1	natolik
oblíbený	oblíbený	k2eAgMnSc1d1	oblíbený
u	u	k7c2	u
starých	starý	k2eAgMnPc2d1	starý
Egypťanů	Egypťan	k1gMnPc2	Egypťan
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgInSc7	první
drahokamem	drahokam	k1gInSc7	drahokam
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
napodobovali	napodobovat	k5eAaImAgMnP	napodobovat
glazovanou	glazovaný	k2eAgFnSc7d1	glazovaná
keramikou	keramika	k1gFnSc7	keramika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
seznam	seznam	k1gInSc1	seznam
minerálů	minerál	k1gInPc2	minerál
</s>
</p>
<p>
<s>
seznam	seznam	k1gInSc1	seznam
drahokamů	drahokam	k1gInPc2	drahokam
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
tyrkys	tyrkys	k1gInSc1	tyrkys
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Tyrkys	tyrkys	k1gInSc1	tyrkys
na	na	k7c6	na
webu	web	k1gInSc6	web
mindat	mindat	k5eAaImF	mindat
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Tyrkys	tyrkys	k1gInSc1	tyrkys
na	na	k7c6	na
portálu	portál	k1gInSc6	portál
webmineral	webminerat	k5eAaImAgMnS	webminerat
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
Tyrkys	tyrkys	k1gInSc1	tyrkys
v	v	k7c6	v
atlasu	atlas	k1gInSc6	atlas
minerálů	minerál	k1gInPc2	minerál
</s>
</p>
