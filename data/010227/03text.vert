<p>
<s>
Zía	Zía	k?	Zía
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
<g/>
:	:	kIx,	:
Ζ	Ζ	k?	Ζ
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
horská	horský	k2eAgFnSc1d1	horská
vesnice	vesnice	k1gFnSc1	vesnice
na	na	k7c6	na
řeckém	řecký	k2eAgInSc6d1	řecký
ostrově	ostrov	k1gInSc6	ostrov
Kós	Kós	k1gInSc1	Kós
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	s	k7c7	s
16	[number]	k4	16
kilometrů	kilometr	k1gInPc2	kilometr
od	od	k7c2	od
Kósu	Kós	k1gInSc2	Kós
<g/>
,	,	kIx,	,
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
ostrova	ostrov	k1gInSc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
se	se	k3xPyFc4	se
dochoval	dochovat	k5eAaPmAgMnS	dochovat
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
dříve	dříve	k6eAd2	dříve
vybudovaných	vybudovaný	k2eAgInPc2d1	vybudovaný
vodních	vodní	k2eAgInPc2d1	vodní
mlýnů	mlýn	k1gInPc2	mlýn
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
památkou	památka	k1gFnSc7	památka
obce	obec	k1gFnSc2	obec
je	být	k5eAaImIp3nS	být
vesnický	vesnický	k2eAgInSc1d1	vesnický
kostelík	kostelík	k1gInSc1	kostelík
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
současné	současný	k2eAgFnSc2d1	současná
polohy	poloha	k1gFnSc2	poloha
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
vesnice	vesnice	k1gFnSc2	vesnice
je	být	k5eAaImIp3nS	být
možný	možný	k2eAgInSc1d1	možný
pěší	pěší	k2eAgInSc1d1	pěší
výstup	výstup	k1gInSc1	výstup
na	na	k7c4	na
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
horu	hora	k1gFnSc4	hora
ostrova	ostrov	k1gInSc2	ostrov
<g/>
,	,	kIx,	,
Díkeos	Díkeosa	k1gFnPc2	Díkeosa
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měří	měřit	k5eAaImIp3nS	měřit
846	[number]	k4	846
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
sousední	sousední	k2eAgFnSc7d1	sousední
vesnicí	vesnice	k1gFnSc7	vesnice
Lagoudi	Lagoud	k1gMnPc1	Lagoud
je	on	k3xPp3gFnPc4	on
pro	pro	k7c4	pro
statistické	statistický	k2eAgInPc4d1	statistický
účely	účel	k1gInPc4	účel
brána	brána	k1gFnSc1	brána
jako	jako	k8xC	jako
jedna	jeden	k4xCgFnSc1	jeden
sídelní	sídelní	k2eAgFnSc1d1	sídelní
jednotka	jednotka	k1gFnSc1	jednotka
a	a	k8xC	a
tato	tento	k3xDgFnSc1	tento
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
měla	mít	k5eAaImAgFnS	mít
151	[number]	k4	151
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Zía	Zía	k1gFnSc2	Zía
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
http://wikimapia.org/#lang=cs&	[url]	k?	http://wikimapia.org/#lang=cs&
</s>
</p>
<p>
<s>
http://www.greeka.com/dodecanese/kos/kos-villages/kos-zia.htm	[url]	k6eAd1	http://www.greeka.com/dodecanese/kos/kos-villages/kos-zia.htm
</s>
</p>
