<p>
<s>
Noční	noční	k2eAgFnSc1d1	noční
směna	směna	k1gFnSc1	směna
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
<g/>
:	:	kIx,	:
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
pracovní	pracovní	k2eAgFnSc4d1	pracovní
směnu	směna	k1gFnSc4	směna
<g/>
,	,	kIx,	,
trvající	trvající	k2eAgInSc4d1	trvající
přes	přes	k7c4	přes
noc	noc	k1gFnSc4	noc
</s>
</p>
<p>
<s>
Noční	noční	k2eAgFnSc1d1	noční
směna	směna	k1gFnSc1	směna
(	(	kIx(	(
<g/>
kniha	kniha	k1gFnSc1	kniha
<g/>
)	)	kIx)	)
–	–	k?	–
soubor	soubor	k1gInSc1	soubor
povídek	povídka	k1gFnPc2	povídka
spisovatele	spisovatel	k1gMnSc2	spisovatel
Stephena	Stephen	k1gMnSc2	Stephen
Kinga	King	k1gMnSc2	King
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Noční	noční	k2eAgFnSc1d1	noční
směna	směna	k1gFnSc1	směna
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
–	–	k?	–
americký	americký	k2eAgInSc4d1	americký
hororový	hororový	k2eAgInSc4d1	hororový
film	film	k1gInSc4	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Noční	noční	k2eAgFnSc1d1	noční
směna	směna	k1gFnSc1	směna
(	(	kIx(	(
<g/>
seriál	seriál	k1gInSc1	seriál
<g/>
)	)	kIx)	)
–	–	k?	–
americký	americký	k2eAgInSc4d1	americký
dramatický	dramatický	k2eAgInSc4d1	dramatický
seriál	seriál	k1gInSc4	seriál
</s>
</p>
<p>
<s>
Noční	noční	k2eAgFnSc1d1	noční
směna	směna	k1gFnSc1	směna
(	(	kIx(	(
<g/>
Spongebob	Spongeboba	k1gFnPc2	Spongeboba
<g/>
)	)	kIx)	)
–	–	k?	–
díl	díl	k1gInSc1	díl
seriálu	seriál	k1gInSc2	seriál
Spongebob	Spongeboba	k1gFnPc2	Spongeboba
v	v	k7c6	v
kalhotách	kalhoty	k1gFnPc6	kalhoty
</s>
</p>
