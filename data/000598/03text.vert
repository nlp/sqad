<s>
Karlův	Karlův	k2eAgInSc1d1	Karlův
most	most	k1gInSc1	most
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgInSc1d3	nejstarší
stojící	stojící	k2eAgInSc1d1	stojící
most	most	k1gInSc1	most
přes	přes	k7c4	přes
řeku	řeka	k1gFnSc4	řeka
Vltavu	Vltava	k1gFnSc4	Vltava
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
druhý	druhý	k4xOgInSc1	druhý
nejstarší	starý	k2eAgInSc1d3	nejstarší
dochovaný	dochovaný	k2eAgInSc1d1	dochovaný
most	most	k1gInSc1	most
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
čtvrtá	čtvrtý	k4xOgFnSc1	čtvrtý
kamenná	kamenný	k2eAgFnSc1d1	kamenná
mostní	mostní	k2eAgFnSc1d1	mostní
stavba	stavba	k1gFnSc1	stavba
<g/>
,	,	kIx,	,
po	po	k7c6	po
mostu	most	k1gInSc2	most
Juditině	Juditin	k2eAgMnSc6d1	Juditin
<g/>
,	,	kIx,	,
píseckém	písecký	k2eAgMnSc6d1	písecký
a	a	k8xC	a
roudnickém	roudnický	k2eAgMnSc6d1	roudnický
<g/>
.	.	kIx.	.
</s>
<s>
Karlův	Karlův	k2eAgInSc1d1	Karlův
most	most	k1gInSc1	most
nahradil	nahradit	k5eAaPmAgInS	nahradit
předchozí	předchozí	k2eAgInSc1d1	předchozí
Juditin	Juditin	k2eAgInSc1d1	Juditin
most	most	k1gInSc1	most
<g/>
,	,	kIx,	,
stržený	stržený	k2eAgInSc1d1	stržený
roku	rok	k1gInSc2	rok
1342	[number]	k4	1342
při	při	k7c6	při
jarním	jarní	k2eAgNnSc6d1	jarní
tání	tání	k1gNnSc6	tání
ledů	led	k1gInPc2	led
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
nového	nový	k2eAgInSc2d1	nový
mostu	most	k1gInSc2	most
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1357	[number]	k4	1357
pod	pod	k7c7	pod
záštitou	záštita	k1gFnSc7	záštita
krále	král	k1gMnSc2	král
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1402	[number]	k4	1402
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
i	i	k9	i
díky	díky	k7c3	díky
kamennému	kamenný	k2eAgInSc3d1	kamenný
mostu	most	k1gInSc2	most
významnou	významný	k2eAgFnSc7d1	významná
zastávkou	zastávka	k1gFnSc7	zastávka
na	na	k7c6	na
evropských	evropský	k2eAgFnPc6d1	Evropská
obchodních	obchodní	k2eAgFnPc6d1	obchodní
stezkách	stezka	k1gFnPc6	stezka
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
postupně	postupně	k6eAd1	postupně
na	na	k7c4	na
most	most	k1gInSc4	most
umístěno	umístit	k5eAaPmNgNnS	umístit
30	[number]	k4	30
převážně	převážně	k6eAd1	převážně
barokních	barokní	k2eAgFnPc2d1	barokní
soch	socha	k1gFnPc2	socha
a	a	k8xC	a
sousoší	sousoší	k1gNnSc4	sousoší
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
říkalo	říkat	k5eAaImAgNnS	říkat
jen	jen	k9	jen
"	"	kIx"	"
<g/>
kamenný	kamenný	k2eAgInSc4d1	kamenný
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
pražský	pražský	k2eAgMnSc1d1	pražský
<g/>
"	"	kIx"	"
most	most	k1gInSc1	most
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
Karlův	Karlův	k2eAgInSc1d1	Karlův
most	most	k1gInSc1	most
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
vžil	vžít	k5eAaPmAgInS	vžít
až	až	k9	až
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1870	[number]	k4	1870
<g/>
,	,	kIx,	,
prvním	první	k4xOgMnSc6	první
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
toto	tento	k3xDgNnSc4	tento
označení	označení	k1gNnSc4	označení
použil	použít	k5eAaPmAgMnS	použít
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
pražský	pražský	k2eAgMnSc1d1	pražský
nakladatel	nakladatel	k1gMnSc1	nakladatel
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
mědirytec	mědirytec	k1gMnSc1	mědirytec
Joseph	Joseph	k1gMnSc1	Joseph
Rudl	rudnout	k5eAaImAgMnS	rudnout
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
monografii	monografie	k1gFnSc6	monografie
s	s	k7c7	s
názvem	název	k1gInSc7	název
Die	Die	k1gMnSc2	Die
Berühmte	Berühmte	k1gMnSc2	Berühmte
Karls-Brücke	Karls-Brück	k1gMnSc2	Karls-Brück
und	und	k?	und
ihre	ihrat	k5eAaPmIp3nS	ihrat
Statuen	Statuen	k1gInSc1	Statuen
<g/>
,	,	kIx,	,
mit	mit	k?	mit
einem	einem	k6eAd1	einem
kurzen	kurzen	k2eAgInSc4d1	kurzen
Anhange	Anhange	k1gInSc4	Anhange
<g/>
:	:	kIx,	:
Die	Die	k1gFnSc4	Die
Franzens-Ketten-Brücke	Franzens-Ketten-Brück	k1gInSc2	Franzens-Ketten-Brück
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
mostě	most	k1gInSc6	most
vede	vést	k5eAaImIp3nS	vést
historická	historický	k2eAgFnSc1d1	historická
královská	královský	k2eAgFnSc1d1	královská
cesta	cesta	k1gFnSc1	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Karlův	Karlův	k2eAgInSc4d1	Karlův
most	most	k1gInSc4	most
spojuje	spojovat	k5eAaImIp3nS	spojovat
Staré	Staré	k2eAgNnSc1d1	Staré
Město	město	k1gNnSc1	město
s	s	k7c7	s
Malou	malý	k2eAgFnSc7d1	malá
Stranou	strana	k1gFnSc7	strana
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
515,76	[number]	k4	515,76
metrů	metr	k1gInPc2	metr
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
šířka	šířka	k1gFnSc1	šířka
je	být	k5eAaImIp3nS	být
9,40	[number]	k4	9,40
až	až	k9	až
9,50	[number]	k4	9,50
m	m	kA	m
<g/>
;	;	kIx,	;
výška	výška	k1gFnSc1	výška
vozovky	vozovka	k1gFnSc2	vozovka
je	být	k5eAaImIp3nS	být
13	[number]	k4	13
m	m	kA	m
nad	nad	k7c7	nad
normální	normální	k2eAgFnSc7d1	normální
hladinou	hladina	k1gFnSc7	hladina
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
šestnácti	šestnáct	k4xCc7	šestnáct
oblouky	oblouk	k1gInPc7	oblouk
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
mají	mít	k5eAaImIp3nP	mít
rozpon	rozpon	k1gInSc4	rozpon
mezi	mezi	k7c4	mezi
16,62	[number]	k4	16,62
m	m	kA	m
(	(	kIx(	(
<g/>
staroměstský	staroměstský	k2eAgInSc4d1	staroměstský
břeh	břeh	k1gInSc4	břeh
<g/>
)	)	kIx)	)
až	až	k9	až
23,38	[number]	k4	23,38
m.	m.	k?	m.
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
délce	délka	k1gFnSc6	délka
je	být	k5eAaImIp3nS	být
třikrát	třikrát	k6eAd1	třikrát
zalomen	zalomen	k2eAgInSc1d1	zalomen
a	a	k8xC	a
proti	proti	k7c3	proti
proudu	proud	k1gInSc3	proud
je	být	k5eAaImIp3nS	být
nepatrně	patrně	k6eNd1	patrně
vypouklý	vypouklý	k2eAgInSc1d1	vypouklý
<g/>
.	.	kIx.	.
</s>
<s>
Most	most	k1gInSc1	most
je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
mlýnských	mlýnský	k2eAgInPc6d1	mlýnský
kamenech	kámen	k1gInPc6	kámen
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
možná	možná	k9	možná
podloženy	podložen	k2eAgInPc4d1	podložen
rošty	rošt	k1gInPc4	rošt
z	z	k7c2	z
dubových	dubový	k2eAgFnPc2d1	dubová
pilot	pilota	k1gFnPc2	pilota
<g/>
.	.	kIx.	.
</s>
<s>
Most	most	k1gInSc1	most
je	být	k5eAaImIp3nS	být
doplněn	doplnit	k5eAaPmNgInS	doplnit
třemi	tři	k4xCgFnPc7	tři
věžemi	věž	k1gFnPc7	věž
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Malé	Malé	k2eAgFnSc6d1	Malé
Straně	strana	k1gFnSc6	strana
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
větší	veliký	k2eAgFnSc1d2	veliký
a	a	k8xC	a
menší	malý	k2eAgFnSc1d2	menší
Malostranská	malostranský	k2eAgFnSc1d1	Malostranská
mostecká	mostecký	k2eAgFnSc1d1	Mostecká
věž	věž	k1gFnSc1	věž
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
straně	strana	k1gFnSc6	strana
Starého	Starého	k2eAgNnSc2d1	Starého
Města	město	k1gNnSc2	město
pak	pak	k6eAd1	pak
Staroměstská	staroměstský	k2eAgFnSc1d1	Staroměstská
mostecká	mostecký	k2eAgFnSc1d1	Mostecká
věž	věž	k1gFnSc1	věž
<g/>
.	.	kIx.	.
</s>
<s>
Staroměstská	staroměstský	k2eAgFnSc1d1	Staroměstská
věž	věž	k1gFnSc1	věž
však	však	k9	však
nestojí	stát	k5eNaImIp3nS	stát
na	na	k7c6	na
krajní	krajní	k2eAgFnSc6d1	krajní
opěře	opěra	k1gFnSc6	opěra
mostu	most	k1gInSc2	most
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
mostních	mostní	k2eAgFnPc2d1	mostní
věží	věž	k1gFnPc2	věž
obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
vnitřním	vnitřní	k2eAgInSc6d1	vnitřní
pilíři	pilíř	k1gInSc6	pilíř
<g/>
.	.	kIx.	.
</s>
<s>
Staroměstská	staroměstský	k2eAgFnSc1d1	Staroměstská
opěra	opěra	k1gFnSc1	opěra
mostu	most	k1gInSc2	most
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
zabudována	zabudován	k2eAgFnSc1d1	zabudována
ve	v	k7c6	v
sklepích	sklep	k1gInPc6	sklep
domu	dům	k1gInSc2	dům
čp.	čp.	k?	čp.
193	[number]	k4	193
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Sochy	socha	k1gFnSc2	socha
na	na	k7c6	na
Karlově	Karlův	k2eAgInSc6d1	Karlův
mostě	most	k1gInSc6	most
<g/>
.	.	kIx.	.
</s>
<s>
Harmonické	harmonický	k2eAgNnSc1d1	harmonické
spojení	spojení	k1gNnSc1	spojení
monumentální	monumentální	k2eAgFnSc2d1	monumentální
středověké	středověký	k2eAgFnSc2d1	středověká
architektury	architektura	k1gFnSc2	architektura
s	s	k7c7	s
výzdobou	výzdoba	k1gFnSc7	výzdoba
třiceti	třicet	k4xCc2	třicet
převážně	převážně	k6eAd1	převážně
vrcholně	vrcholně	k6eAd1	vrcholně
barokních	barokní	k2eAgFnPc2d1	barokní
soch	socha	k1gFnPc2	socha
a	a	k8xC	a
sousoší	sousoší	k1gNnSc1	sousoší
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
působivý	působivý	k2eAgInSc4d1	působivý
celek	celek	k1gInSc4	celek
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
dvě	dva	k4xCgNnPc4	dva
slavná	slavný	k2eAgNnPc4d1	slavné
období	období	k1gNnPc4	období
českého	český	k2eAgNnSc2d1	české
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
mostě	most	k1gInSc6	most
<g/>
,	,	kIx,	,
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Ukřižování	ukřižování	k1gNnSc2	ukřižování
<g/>
,	,	kIx,	,
stával	stávat	k5eAaImAgInS	stávat
kříž	kříž	k1gInSc4	kříž
již	již	k9	již
od	od	k7c2	od
dob	doba	k1gFnPc2	doba
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
kolem	kolem	k7c2	kolem
r.	r.	kA	r.
1500	[number]	k4	1500
proti	proti	k7c3	proti
němu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
boží	boží	k2eAgFnSc1d1	boží
muka	muka	k1gFnSc1	muka
a	a	k8xC	a
na	na	k7c6	na
zhlaví	zhlaví	k1gNnSc6	zhlaví
pilíře	pilíř	k1gInSc2	pilíř
na	na	k7c6	na
Kampě	Kampa	k1gFnSc6	Kampa
socha	socha	k1gFnSc1	socha
Bruncvíka	Bruncvík	k1gMnSc2	Bruncvík
označující	označující	k2eAgFnSc1d1	označující
oblast	oblast	k1gFnSc1	oblast
staroměstského	staroměstský	k2eAgNnSc2d1	Staroměstské
práva	právo	k1gNnSc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
sochařská	sochařský	k2eAgFnSc1d1	sochařská
výzdoba	výzdoba	k1gFnSc1	výzdoba
však	však	k9	však
byla	být	k5eAaImAgFnS	být
původně	původně	k6eAd1	původně
soustředěna	soustředit	k5eAaPmNgFnS	soustředit
na	na	k7c4	na
věže	věž	k1gFnPc4	věž
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
staroměstskou	staroměstský	k2eAgFnSc7d1	Staroměstská
<g/>
.	.	kIx.	.
</s>
<s>
Socha	socha	k1gFnSc1	socha
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
v	v	k7c4	v
upomínku	upomínka	k1gFnSc4	upomínka
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
svržení	svržení	k1gNnSc4	svržení
z	z	k7c2	z
mostu	most	k1gInSc2	most
z	z	k7c2	z
r.	r.	kA	r.
1683	[number]	k4	1683
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
teprve	teprve	k6eAd1	teprve
se	se	k3xPyFc4	se
rodícího	rodící	k2eAgInSc2d1	rodící
kultu	kult	k1gInSc2	kult
světce	světec	k1gMnSc2	světec
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
vzorem	vzor	k1gInSc7	vzor
pro	pro	k7c4	pro
většinu	většina	k1gFnSc4	většina
z	z	k7c2	z
bezpočtu	bezpočet	k1gInSc2	bezpočet
jeho	jeho	k3xOp3gNnSc2	jeho
vyobrazení	vyobrazení	k1gNnSc2	vyobrazení
a	a	k8xC	a
také	také	k9	také
první	první	k4xOgFnSc4	první
ze	z	k7c2	z
slavné	slavný	k2eAgFnSc2d1	slavná
barokní	barokní	k2eAgFnSc2d1	barokní
sochařské	sochařský	k2eAgFnSc2d1	sochařská
výzdoby	výzdoba	k1gFnSc2	výzdoba
mostu	most	k1gInSc2	most
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
přibyly	přibýt	k5eAaPmAgFnP	přibýt
další	další	k2eAgFnPc1d1	další
sochy	socha	k1gFnPc1	socha
a	a	k8xC	a
sousoší	sousoší	k1gNnSc1	sousoší
financované	financovaný	k2eAgFnSc2d1	financovaná
různými	různý	k2eAgMnPc7d1	různý
donátory	donátor	k1gMnPc7	donátor
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
úzkém	úzký	k2eAgNnSc6d1	úzké
rozmezí	rozmezí	k1gNnSc6	rozmezí
let	léto	k1gNnPc2	léto
1707	[number]	k4	1707
až	až	k6eAd1	až
1714	[number]	k4	1714
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
most	most	k1gInSc4	most
osazena	osazen	k2eAgFnSc1d1	osazena
většina	většina	k1gFnSc1	většina
soch	socha	k1gFnPc2	socha
a	a	k8xC	a
sousoší	sousoší	k1gNnSc1	sousoší
a	a	k8xC	a
zaplněny	zaplněn	k2eAgInPc1d1	zaplněn
jimi	on	k3xPp3gMnPc7	on
zbývající	zbývající	k2eAgInPc4d1	zbývající
volné	volný	k2eAgInPc4d1	volný
pilíře	pilíř	k1gInPc4	pilíř
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
reprezentativní	reprezentativní	k2eAgNnPc4d1	reprezentativní
díla	dílo	k1gNnPc4	dílo
řady	řada	k1gFnSc2	řada
známých	známý	k2eAgMnPc2d1	známý
českých	český	k2eAgMnPc2d1	český
vrcholně	vrcholně	k6eAd1	vrcholně
barokních	barokní	k2eAgMnPc2d1	barokní
sochařů	sochař	k1gMnPc2	sochař
<g/>
,	,	kIx,	,
k	k	k7c3	k
nejhodnotnějším	hodnotný	k2eAgFnPc3d3	nejhodnotnější
patří	patřit	k5eAaImIp3nS	patřit
sv.	sv.	kA	sv.
Luitgarda	Luitgard	k1gMnSc4	Luitgard
Matyáše	Matyáš	k1gMnSc4	Matyáš
Bernarda	Bernard	k1gMnSc4	Bernard
Brauna	Braun	k1gMnSc4	Braun
a	a	k8xC	a
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
děl	dělo	k1gNnPc2	dělo
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
Maxmiliána	Maxmilián	k1gMnSc2	Maxmilián
Brokoffa	Brokoff	k1gMnSc2	Brokoff
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
další	další	k2eAgFnSc3d1	další
výraznější	výrazný	k2eAgFnSc3d2	výraznější
proměně	proměna	k1gFnSc3	proměna
výzdoby	výzdoba	k1gFnSc2	výzdoba
mostu	most	k1gInSc2	most
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
době	doba	k1gFnSc6	doba
kolem	kolem	k7c2	kolem
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
menší	malý	k2eAgFnSc1d2	menší
část	část	k1gFnSc1	část
barokních	barokní	k2eAgFnPc2d1	barokní
soch	socha	k1gFnPc2	socha
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
kvůli	kvůli	k7c3	kvůli
jejich	jejich	k3xOp3gInSc3	jejich
technickému	technický	k2eAgInSc3d1	technický
stavu	stav	k1gInSc3	stav
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
dobovému	dobový	k2eAgInSc3d1	dobový
vkusu	vkus	k1gInSc3	vkus
bližšími	blízký	k2eAgInPc7d2	bližší
<g/>
,	,	kIx,	,
klasicistně	klasicistně	k6eAd1	klasicistně
statičtějšími	statický	k2eAgInPc7d2	statičtější
díly	díl	k1gInPc7	díl
Josefa	Josef	k1gMnSc2	Josef
a	a	k8xC	a
Emanuela	Emanuel	k1gMnSc2	Emanuel
Maxe	Max	k1gMnSc2	Max
<g/>
.	.	kIx.	.
</s>
<s>
Škody	škoda	k1gFnPc4	škoda
způsobené	způsobený	k2eAgFnPc4d1	způsobená
povodní	povodeň	k1gFnSc7	povodeň
v	v	k7c4	v
r.	r.	kA	r.
1890	[number]	k4	1890
a	a	k8xC	a
horšící	horšící	k2eAgMnSc1d1	horšící
se	se	k3xPyFc4	se
stav	stav	k1gInSc1	stav
barokních	barokní	k2eAgNnPc2d1	barokní
děl	dělo	k1gNnPc2	dělo
z	z	k7c2	z
pískovce	pískovec	k1gInSc2	pískovec
s	s	k7c7	s
omezenou	omezený	k2eAgFnSc7d1	omezená
životností	životnost	k1gFnSc7	životnost
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
pozvolna	pozvolna	k6eAd1	pozvolna
na	na	k7c4	na
most	most	k1gInSc4	most
osazují	osazovat	k5eAaImIp3nP	osazovat
kopie	kopie	k1gFnPc1	kopie
<g/>
.	.	kIx.	.
</s>
<s>
Snesené	snesený	k2eAgInPc1d1	snesený
originály	originál	k1gInPc1	originál
soch	socha	k1gFnPc2	socha
a	a	k8xC	a
sousoší	sousoší	k1gNnSc1	sousoší
z	z	k7c2	z
Karlova	Karlův	k2eAgInSc2d1	Karlův
mostu	most	k1gInSc2	most
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
uloženy	uložit	k5eAaPmNgInP	uložit
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
Lapidáriu	lapidárium	k1gNnSc6	lapidárium
na	na	k7c6	na
pražském	pražský	k2eAgNnSc6d1	Pražské
Výstavišti	výstaviště	k1gNnSc6	výstaviště
a	a	k8xC	a
v	v	k7c6	v
sálu	sál	k1gInSc3	sál
Gorlice	Gorlice	k1gFnSc1	Gorlice
na	na	k7c6	na
Vyšehradě	Vyšehrad	k1gInSc6	Vyšehrad
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
pravěku	pravěk	k1gInSc2	pravěk
lidé	člověk	k1gMnPc1	člověk
přecházeli	přecházet	k5eAaImAgMnP	přecházet
řeku	řeka	k1gFnSc4	řeka
Vltavu	Vltava	k1gFnSc4	Vltava
v	v	k7c6	v
dnešním	dnešní	k2eAgNnSc6d1	dnešní
centru	centrum	k1gNnSc6	centrum
Prahy	Praha	k1gFnSc2	Praha
přes	přes	k7c4	přes
několik	několik	k4yIc4	několik
brodů	brod	k1gInPc2	brod
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
se	se	k3xPyFc4	se
táhl	táhnout	k5eAaImAgMnS	táhnout
od	od	k7c2	od
dnešní	dnešní	k2eAgFnSc2d1	dnešní
paty	pata	k1gFnSc2	pata
Mánesova	Mánesův	k2eAgInSc2d1	Mánesův
mostu	most	k1gInSc2	most
na	na	k7c6	na
Starém	starý	k2eAgNnSc6d1	staré
Městě	město	k1gNnSc6	město
(	(	kIx(	(
<g/>
Náměstí	náměstí	k1gNnSc1	náměstí
Jana	Jan	k1gMnSc2	Jan
Palacha	Palach	k1gMnSc2	Palach
<g/>
)	)	kIx)	)
šikmo	šikmo	k6eAd1	šikmo
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
k	k	k7c3	k
dnešní	dnešní	k2eAgFnSc3d1	dnešní
Hergetově	Hergetův	k2eAgFnSc3d1	Hergetova
cihelně	cihelna	k1gFnSc3	cihelna
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
dnešního	dnešní	k2eAgInSc2d1	dnešní
Karlova	Karlův	k2eAgInSc2d1	Karlův
mostu	most	k1gInSc2	most
postaven	postavit	k5eAaPmNgInS	postavit
most	most	k1gInSc1	most
dřevěný	dřevěný	k2eAgInSc1d1	dřevěný
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
ale	ale	k9	ale
roku	rok	k1gInSc2	rok
1157	[number]	k4	1157
stržen	stržen	k2eAgInSc1d1	stržen
povodní	povodní	k2eAgInSc1d1	povodní
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
dal	dát	k5eAaPmAgMnS	dát
král	král	k1gMnSc1	král
Vladislav	Vladislav	k1gMnSc1	Vladislav
II	II	kA	II
<g/>
.	.	kIx.	.
vybudovat	vybudovat	k5eAaPmF	vybudovat
první	první	k4xOgInSc4	první
pražský	pražský	k2eAgInSc4d1	pražský
kamenný	kamenný	k2eAgInSc4d1	kamenný
most	most	k1gInSc4	most
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
dokončen	dokončit	k5eAaPmNgInS	dokončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1172	[number]	k4	1172
a	a	k8xC	a
dostal	dostat	k5eAaPmAgMnS	dostat
jméno	jméno	k1gNnSc4	jméno
po	po	k7c6	po
králově	králův	k2eAgFnSc6d1	králova
manželce	manželka	k1gFnSc6	manželka
Juditě	Judita	k1gFnSc6	Judita
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
dílo	dílo	k1gNnSc4	dílo
zničila	zničit	k5eAaPmAgFnS	zničit
povodeň	povodeň	k1gFnSc1	povodeň
3	[number]	k4	3
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1342	[number]	k4	1342
<g/>
.	.	kIx.	.
</s>
<s>
Kronikář	kronikář	k1gMnSc1	kronikář
Beneš	Beneš	k1gMnSc1	Beneš
Krabice	krabice	k1gFnSc1	krabice
z	z	k7c2	z
Weitmile	Weitmila	k1gFnSc3	Weitmila
k	k	k7c3	k
roku	rok	k1gInSc3	rok
1357	[number]	k4	1357
píše	psát	k5eAaImIp3nS	psát
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
císař	císař	k1gMnSc1	císař
položil	položit	k5eAaPmAgMnS	položit
základní	základní	k2eAgInSc4d1	základní
neboli	neboli	k8xC	neboli
první	první	k4xOgInSc4	první
kámen	kámen	k1gInSc4	kámen
v	v	k7c6	v
základu	základ	k1gInSc6	základ
nového	nový	k2eAgInSc2d1	nový
mostu	most	k1gInSc2	most
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
blízko	blízko	k7c2	blízko
kláštera	klášter	k1gInSc2	klášter
svatého	svatý	k2eAgMnSc2d1	svatý
Klimenta	Kliment	k1gMnSc2	Kliment
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Přesnější	přesný	k2eAgInSc4d2	přesnější
datum	datum	k1gInSc4	datum
Beneš	Beneš	k1gMnSc1	Beneš
Krabice	krabice	k1gFnSc2	krabice
neuvádí	uvádět	k5eNaImIp3nS	uvádět
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
červenec	červenec	k1gInSc4	červenec
nalezneme	nalézt	k5eAaBmIp1nP	nalézt
u	u	k7c2	u
kronikáře	kronikář	k1gMnSc2	kronikář
Prokopa	Prokop	k1gMnSc2	Prokop
Lupáče	Lupáč	k1gMnSc2	Lupáč
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
ovšem	ovšem	k9	ovšem
o	o	k7c4	o
tvrzení	tvrzení	k1gNnPc4	tvrzení
mnohem	mnohem	k6eAd1	mnohem
pozdější	pozdní	k2eAgFnSc2d2	pozdější
(	(	kIx(	(
<g/>
o	o	k7c4	o
3	[number]	k4	3
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
a	a	k8xC	a
bez	bez	k7c2	bez
zdůvodnění	zdůvodnění	k1gNnSc2	zdůvodnění
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
podle	podle	k7c2	podle
záznamů	záznam	k1gInPc2	záznam
v	v	k7c4	v
tuto	tento	k3xDgFnSc4	tento
dobu	doba	k1gFnSc4	doba
císař	císař	k1gMnSc1	císař
pobýval	pobývat	k5eAaImAgMnS	pobývat
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
a	a	k8xC	a
mohl	moct	k5eAaImAgInS	moct
tedy	tedy	k9	tedy
most	most	k1gInSc1	most
založit	založit	k5eAaPmF	založit
<g/>
.	.	kIx.	.
</s>
<s>
Historiky	historik	k1gMnPc4	historik
je	být	k5eAaImIp3nS	být
nejčastěji	často	k6eAd3	často
předpokládáno	předpokládán	k2eAgNnSc4d1	předpokládáno
datum	datum	k1gNnSc4	datum
založení	založení	k1gNnSc2	založení
na	na	k7c4	na
svátek	svátek	k1gInSc4	svátek
zemského	zemský	k2eAgMnSc2d1	zemský
patrona	patron	k1gMnSc2	patron
sv.	sv.	kA	sv.
Víta	Vít	k1gMnSc2	Vít
15	[number]	k4	15
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
.	.	kIx.	.
</s>
<s>
Astronom	astronom	k1gMnSc1	astronom
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Horský	Horský	k1gMnSc1	Horský
(	(	kIx(	(
<g/>
1929	[number]	k4	1929
<g/>
-	-	kIx~	-
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
poněkud	poněkud	k6eAd1	poněkud
spekulativní	spekulativní	k2eAgFnSc4d1	spekulativní
hypotézu	hypotéza	k1gFnSc4	hypotéza
o	o	k7c6	o
datu	datum	k1gNnSc6	datum
9	[number]	k4	9
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1357	[number]	k4	1357
<g/>
,	,	kIx,	,
v	v	k7c6	v
5	[number]	k4	5
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
31	[number]	k4	31
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
prý	prý	k9	prý
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
<g/>
,	,	kIx,	,
že	že	k8xS	že
tomuto	tento	k3xDgNnSc3	tento
datu	datum	k1gNnSc3	datum
odpovídala	odpovídat	k5eAaImAgFnS	odpovídat
příznivá	příznivý	k2eAgFnSc1d1	příznivá
astrologická	astrologický	k2eAgFnSc1d1	astrologická
konstelace	konstelace	k1gFnSc1	konstelace
(	(	kIx(	(
<g/>
konjunkce	konjunkce	k1gFnSc1	konjunkce
Slunce	slunce	k1gNnSc2	slunce
se	s	k7c7	s
Saturnem	Saturn	k1gInSc7	Saturn
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
datum	datum	k1gNnSc1	datum
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
sledu	sled	k1gInSc2	sled
lichých	lichý	k2eAgNnPc2d1	liché
čísel	číslo	k1gNnPc2	číslo
od	od	k7c2	od
jedné	jeden	k4xCgFnSc2	jeden
do	do	k7c2	do
devíti	devět	k4xCc2	devět
a	a	k8xC	a
zpět	zpět	k6eAd1	zpět
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
stržení	stržení	k1gNnSc6	stržení
Juditina	Juditin	k2eAgInSc2d1	Juditin
mostu	most	k1gInSc2	most
sloužil	sloužit	k5eAaImAgInS	sloužit
Pražanům	Pražan	k1gMnPc3	Pražan
most	most	k1gInSc4	most
dřevěný	dřevěný	k2eAgInSc4d1	dřevěný
<g/>
.	.	kIx.	.
</s>
<s>
Doprava	doprava	k1gFnSc1	doprava
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
témže	týž	k3xTgNnSc6	týž
místě	místo	k1gNnSc6	místo
také	také	k9	také
prováděna	provádět	k5eAaImNgNnP	provádět
loďkami	loďka	k1gFnPc7	loďka
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
byla	být	k5eAaImAgFnS	být
velice	velice	k6eAd1	velice
nákladná	nákladný	k2eAgFnSc1d1	nákladná
a	a	k8xC	a
finančně	finančně	k6eAd1	finančně
české	český	k2eAgNnSc4d1	české
království	království	k1gNnSc4	království
vyčerpávala	vyčerpávat	k5eAaImAgFnS	vyčerpávat
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
její	její	k3xOp3gNnSc4	její
financování	financování	k1gNnSc4	financování
byly	být	k5eAaImAgFnP	být
dokonce	dokonce	k9	dokonce
v	v	k7c6	v
kostelích	kostel	k1gInPc6	kostel
pořádány	pořádán	k2eAgFnPc1d1	pořádána
sbírky	sbírka	k1gFnPc1	sbírka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
otevření	otevření	k1gNnSc6	otevření
mostu	most	k1gInSc2	most
se	se	k3xPyFc4	se
za	za	k7c4	za
průjezd	průjezd	k1gInSc4	průjezd
<g/>
/	/	kIx~	/
<g/>
přechod	přechod	k1gInSc4	přechod
platilo	platit	k5eAaImAgNnS	platit
mýtné	mýtné	k1gNnSc1	mýtné
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
let	léto	k1gNnPc2	léto
neustále	neustále	k6eAd1	neustále
měnilo	měnit	k5eAaImAgNnS	měnit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1816	[number]	k4	1816
bylo	být	k5eAaImAgNnS	být
"	"	kIx"	"
<g/>
mostní	mostní	k2eAgNnSc4d1	mostní
clo	clo	k1gNnSc4	clo
<g/>
"	"	kIx"	"
zcela	zcela	k6eAd1	zcela
odstraněno	odstranit	k5eAaPmNgNnS	odstranit
<g/>
.	.	kIx.	.
</s>
<s>
Dokončena	dokončen	k2eAgFnSc1d1	dokončena
byla	být	k5eAaImAgFnS	být
až	až	k9	až
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1406	[number]	k4	1406
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
setkat	setkat	k5eAaPmF	setkat
již	již	k9	již
se	s	k7c7	s
záznamy	záznam	k1gInPc7	záznam
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
zmiňují	zmiňovat	k5eAaImIp3nP	zmiňovat
"	"	kIx"	"
<g/>
nový	nový	k2eAgInSc4d1	nový
kamenný	kamenný	k2eAgInSc4d1	kamenný
most	most	k1gInSc4	most
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
stavbě	stavba	k1gFnSc6	stavba
Karlova	Karlův	k2eAgInSc2d1	Karlův
mostu	most	k1gInSc2	most
(	(	kIx(	(
<g/>
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
nesl	nést	k5eAaImAgInS	nést
název	název	k1gInSc4	název
Kamenný	kamenný	k2eAgInSc1d1	kamenný
most	most	k1gInSc1	most
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
stavitelé	stavitel	k1gMnPc1	stavitel
pokoušeli	pokoušet	k5eAaImAgMnP	pokoušet
vyvarovat	vyvarovat	k5eAaPmF	vyvarovat
chyb	chyba	k1gFnPc2	chyba
<g/>
,	,	kIx,	,
kterými	který	k3yIgFnPc7	který
trpěl	trpět	k5eAaImAgMnS	trpět
starší	starý	k2eAgInSc4d2	starší
most	most	k1gInSc4	most
Juditin	Juditin	k2eAgInSc4d1	Juditin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Karlův	Karlův	k2eAgInSc1d1	Karlův
most	most	k1gInSc1	most
měl	mít	k5eAaImAgInS	mít
proto	proto	k8xC	proto
méně	málo	k6eAd2	málo
oblouků	oblouk	k1gInPc2	oblouk
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
vyšší	vysoký	k2eAgMnSc1d2	vyšší
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc1	jeho
pilíře	pilíř	k1gInPc1	pilíř
byly	být	k5eAaImAgInP	být
založeny	založit	k5eAaPmNgInP	založit
hluboko	hluboko	k6eAd1	hluboko
<g/>
,	,	kIx,	,
2,4	[number]	k4	2,4
m	m	kA	m
pod	pod	k7c7	pod
úrovní	úroveň	k1gFnSc7	úroveň
říčního	říční	k2eAgNnSc2d1	říční
dna	dno	k1gNnSc2	dno
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
to	ten	k3xDgNnSc1	ten
však	však	k9	však
nebylo	být	k5eNaImAgNnS	být
dostačující	dostačující	k2eAgNnSc1d1	dostačující
<g/>
;	;	kIx,	;
založení	založení	k1gNnSc1	založení
pilířů	pilíř	k1gInPc2	pilíř
na	na	k7c4	na
skalní	skalní	k2eAgNnSc4d1	skalní
podloží	podloží	k1gNnSc4	podloží
by	by	kYmCp3nP	by
jistě	jistě	k9	jistě
zvýšilo	zvýšit	k5eAaPmAgNnS	zvýšit
stabilitu	stabilita	k1gFnSc4	stabilita
mostu	most	k1gInSc2	most
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
až	až	k9	až
9	[number]	k4	9
m	m	kA	m
pod	pod	k7c7	pod
úrovní	úroveň	k1gFnSc7	úroveň
říčního	říční	k2eAgNnSc2d1	říční
dna	dno	k1gNnSc2	dno
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
zcela	zcela	k6eAd1	zcela
mimo	mimo	k7c4	mimo
technické	technický	k2eAgFnPc4d1	technická
možnosti	možnost	k1gFnPc4	možnost
středověkých	středověký	k2eAgMnPc2d1	středověký
stavitelů	stavitel	k1gMnPc2	stavitel
mostu	most	k1gInSc2	most
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
stavitele	stavitel	k1gMnSc2	stavitel
byl	být	k5eAaImAgMnS	být
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
považován	považován	k2eAgMnSc1d1	považován
Petr	Petr	k1gMnSc1	Petr
Parléř	Parléř	k1gMnSc1	Parléř
ze	z	k7c2	z
Švábského	švábský	k2eAgInSc2d1	švábský
Gmündu	Gmünd	k1gInSc2	Gmünd
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
byla	být	k5eAaImAgFnS	být
však	však	k9	však
publikována	publikován	k2eAgFnSc1d1	publikována
hypotéza	hypotéza	k1gFnSc1	hypotéza
<g/>
,	,	kIx,	,
že	že	k8xS	že
projektantem	projektant	k1gMnSc7	projektant
a	a	k8xC	a
prvním	první	k4xOgMnSc6	první
stavitelem	stavitel	k1gMnSc7	stavitel
mostu	most	k1gInSc2	most
byl	být	k5eAaImAgMnS	být
kameník	kameník	k1gMnSc1	kameník
a	a	k8xC	a
pražský	pražský	k2eAgMnSc1d1	pražský
měšťan	měšťan	k1gMnSc1	měšťan
Oto	Oto	k1gMnSc1	Oto
<g/>
,	,	kIx,	,
zvaný	zvaný	k2eAgMnSc1d1	zvaný
též	též	k9	též
Otlin	Otlin	k1gMnSc1	Otlin
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
legendy	legenda	k1gFnSc2	legenda
byla	být	k5eAaImAgFnS	být
při	při	k7c6	při
stavbě	stavba	k1gFnSc6	stavba
do	do	k7c2	do
malty	malta	k1gFnSc2	malta
přidávána	přidáván	k2eAgNnPc1d1	přidáváno
vajíčka	vajíčko	k1gNnPc1	vajíčko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
se	se	k3xPyFc4	se
při	při	k7c6	při
rozboru	rozbor	k1gInSc6	rozbor
původní	původní	k2eAgFnSc2d1	původní
malty	malta	k1gFnSc2	malta
mělo	mít	k5eAaImAgNnS	mít
prokázat	prokázat	k5eAaPmF	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
legenda	legenda	k1gFnSc1	legenda
je	být	k5eAaImIp3nS	být
pravdivá	pravdivý	k2eAgFnSc1d1	pravdivá
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
naopak	naopak	k6eAd1	naopak
vědci	vědec	k1gMnPc1	vědec
z	z	k7c2	z
Vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
chemicko-technologické	chemickoechnologický	k2eAgMnPc4d1	chemicko-technologický
oznámili	oznámit	k5eAaPmAgMnP	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vejce	vejce	k1gNnPc4	vejce
na	na	k7c4	na
stavbu	stavba	k1gFnSc4	stavba
použita	použit	k2eAgFnSc1d1	použita
nebyla	být	k5eNaImAgFnS	být
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
také	také	k9	také
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
malta	malta	k1gFnSc1	malta
zalévala	zalévat	k5eAaImAgFnS	zalévat
mlékem	mléko	k1gNnSc7	mléko
a	a	k8xC	a
vínem	víno	k1gNnSc7	víno
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
se	se	k3xPyFc4	se
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
(	(	kIx(	(
<g/>
sice	sice	k8xC	sice
nechtěně	chtěně	k6eNd1	chtěně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přece	přece	k9	přece
<g/>
)	)	kIx)	)
přidalo	přidat	k5eAaPmAgNnS	přidat
pár	pár	k4xCyI	pár
tvarůžků	tvarůžek	k1gInPc2	tvarůžek
a	a	k8xC	a
syrečků	syreček	k1gInPc2	syreček
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1393	[number]	k4	1393
dal	dát	k5eAaPmAgMnS	dát
Václav	Václav	k1gMnSc1	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
do	do	k7c2	do
řeky	řeka	k1gFnSc2	řeka
svrhnout	svrhnout	k5eAaPmF	svrhnout
tělo	tělo	k1gNnSc4	tělo
umučeného	umučený	k2eAgMnSc2d1	umučený
generálního	generální	k2eAgMnSc2d1	generální
vikáře	vikář	k1gMnSc2	vikář
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
údajném	údajný	k2eAgNnSc6d1	údajné
místě	místo	k1gNnSc6	místo
svržení	svržení	k1gNnSc2	svržení
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
z	z	k7c2	z
mostu	most	k1gInSc2	most
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
kamenném	kamenný	k2eAgNnSc6d1	kamenné
zábradlí	zábradlí	k1gNnSc6	zábradlí
malý	malý	k2eAgInSc4d1	malý
kovový	kovový	k2eAgInSc4d1	kovový
křížek	křížek	k1gInSc4	křížek
<g/>
.	.	kIx.	.
</s>
<s>
Most	most	k1gInSc1	most
sloužil	sloužit	k5eAaImAgInS	sloužit
městu	město	k1gNnSc3	město
jako	jako	k9	jako
významná	významný	k2eAgFnSc1d1	významná
dopravní	dopravní	k2eAgFnSc1d1	dopravní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
jako	jako	k9	jako
obchodní	obchodní	k2eAgFnSc1d1	obchodní
tepna	tepna	k1gFnSc1	tepna
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
obou	dva	k4xCgFnPc2	dva
stran	strana	k1gFnPc2	strana
mostu	most	k1gInSc2	most
(	(	kIx(	(
<g/>
jak	jak	k8xS	jak
staroměstské	staroměstský	k2eAgNnSc1d1	Staroměstské
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
i	i	k9	i
malostranské	malostranský	k2eAgNnSc1d1	Malostranské
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
proto	proto	k8xC	proto
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
délky	délka	k1gFnSc2	délka
zastavěn	zastavěn	k2eAgInSc4d1	zastavěn
obchůdky	obchůdek	k1gInPc4	obchůdek
a	a	k8xC	a
krámy	krám	k1gInPc4	krám
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgFnPc4	svůj
živnosti	živnost	k1gFnPc4	živnost
měli	mít	k5eAaImAgMnP	mít
na	na	k7c6	na
mostě	most	k1gInSc6	most
např.	např.	kA	např.
uzenáři	uzenář	k1gMnPc1	uzenář
<g/>
,	,	kIx,	,
hřebenáři	hřebenář	k1gMnPc1	hřebenář
<g/>
,	,	kIx,	,
či	či	k8xC	či
koláčníci	koláčník	k1gMnPc1	koláčník
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInPc1d1	poslední
krámky	krámek	k1gInPc1	krámek
byly	být	k5eAaImAgInP	být
z	z	k7c2	z
mostu	most	k1gInSc2	most
odstraněny	odstranit	k5eAaPmNgFnP	odstranit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1828	[number]	k4	1828
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
si	se	k3xPyFc3	se
své	svůj	k3xOyFgNnSc4	svůj
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
mostě	most	k1gInSc6	most
vydobyli	vydobýt	k5eAaPmAgMnP	vydobýt
výtvarníci	výtvarník	k1gMnPc1	výtvarník
<g/>
,	,	kIx,	,
fotografové	fotograf	k1gMnPc1	fotograf
a	a	k8xC	a
hudebníci	hudebník	k1gMnPc1	hudebník
vybíraní	vybíraný	k2eAgMnPc1d1	vybíraný
Sdružením	sdružení	k1gNnSc7	sdružení
výtvarníků	výtvarník	k1gMnPc2	výtvarník
Karlova	Karlův	k2eAgInSc2d1	Karlův
mostu	most	k1gInSc2	most
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
nechvalně	chvalně	k6eNd1	chvalně
proslavilo	proslavit	k5eAaPmAgNnS	proslavit
omezováním	omezování	k1gNnSc7	omezování
nekomerční	komerční	k2eNgFnSc2d1	nekomerční
umělecké	umělecký	k2eAgFnSc2d1	umělecká
činnosti	činnost	k1gFnSc2	činnost
na	na	k7c6	na
mostě	most	k1gInSc6	most
<g/>
.	.	kIx.	.
</s>
<s>
Mnohokrát	mnohokrát	k6eAd1	mnohokrát
byl	být	k5eAaImAgInS	být
most	most	k1gInSc1	most
ohrožován	ohrožován	k2eAgInSc1d1	ohrožován
povodněmi	povodeň	k1gFnPc7	povodeň
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
již	již	k6eAd1	již
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
stavby	stavba	k1gFnSc2	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1359	[number]	k4	1359
<g/>
,	,	kIx,	,
1367	[number]	k4	1367
<g/>
,	,	kIx,	,
1370	[number]	k4	1370
<g/>
,	,	kIx,	,
1373	[number]	k4	1373
a	a	k8xC	a
1374	[number]	k4	1374
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1432	[number]	k4	1432
veliká	veliký	k2eAgFnSc1d1	veliká
povodeň	povodeň	k1gFnSc1	povodeň
zaplavila	zaplavit	k5eAaPmAgFnS	zaplavit
větší	veliký	k2eAgFnSc4d2	veliký
část	část	k1gFnSc4	část
Starého	Starého	k2eAgNnSc2d1	Starého
Města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
zbořeno	zbořit	k5eAaPmNgNnS	zbořit
5	[number]	k4	5
pilířů	pilíř	k1gInPc2	pilíř
mostu	most	k1gInSc2	most
<g/>
.	.	kIx.	.
</s>
<s>
Most	most	k1gInSc1	most
byl	být	k5eAaImAgInS	být
poté	poté	k6eAd1	poté
opravován	opravovat	k5eAaImNgInS	opravovat
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1503	[number]	k4	1503
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
celých	celý	k2eAgNnPc2d1	celé
71	[number]	k4	71
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
povodně	povodeň	k1gFnPc1	povodeň
ohrožovaly	ohrožovat	k5eAaImAgFnP	ohrožovat
most	most	k1gInSc4	most
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1655	[number]	k4	1655
<g/>
,	,	kIx,	,
1784	[number]	k4	1784
<g/>
,	,	kIx,	,
1845	[number]	k4	1845
<g/>
,	,	kIx,	,
1872	[number]	k4	1872
a	a	k8xC	a
1890	[number]	k4	1890
<g/>
.	.	kIx.	.
</s>
<s>
Války	válka	k1gFnPc1	válka
tělesu	těleso	k1gNnSc3	těleso
mostu	most	k1gInSc2	most
příliš	příliš	k6eAd1	příliš
neublížily	ublížit	k5eNaPmAgFnP	ublížit
<g/>
,	,	kIx,	,
při	při	k7c6	při
obléhání	obléhání	k1gNnSc6	obléhání
švédskými	švédský	k2eAgMnPc7d1	švédský
vojsky	vojsky	k6eAd1	vojsky
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
ale	ale	k8xC	ale
byly	být	k5eAaImAgFnP	být
poničeny	poničen	k2eAgFnPc1d1	poničena
sochy	socha	k1gFnPc1	socha
a	a	k8xC	a
Staroměstská	staroměstský	k2eAgFnSc1d1	Staroměstská
mostecká	mostecký	k2eAgFnSc1d1	Mostecká
věž	věž	k1gFnSc1	věž
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
povodni	povodeň	k1gFnSc6	povodeň
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1784	[number]	k4	1784
stržené	stržený	k2eAgInPc4d1	stržený
vory	vor	k1gInPc4	vor
a	a	k8xC	a
ledy	led	k1gInPc4	led
ucpaly	ucpat	k5eAaPmAgInP	ucpat
oblouky	oblouk	k1gInPc1	oblouk
mostu	most	k1gInSc2	most
a	a	k8xC	a
voda	voda	k1gFnSc1	voda
podemlela	podemlít	k5eAaPmAgFnS	podemlít
pilíře	pilíř	k1gInPc4	pilíř
<g/>
.	.	kIx.	.
</s>
<s>
Zřítilo	zřítit	k5eAaPmAgNnS	zřítit
se	se	k3xPyFc4	se
přední	přední	k2eAgNnSc1d1	přední
zhlaví	zhlaví	k1gNnSc1	zhlaví
pilíře	pilíř	k1gInSc2	pilíř
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
stála	stát	k5eAaImAgFnS	stát
vojenská	vojenský	k2eAgFnSc1d1	vojenská
strážnice	strážnice	k1gFnSc1	strážnice
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
šest	šest	k4xCc1	šest
mostních	mostní	k2eAgInPc2d1	mostní
pilířů	pilíř	k1gInPc2	pilíř
bylo	být	k5eAaImAgNnS	být
silně	silně	k6eAd1	silně
poškozeno	poškodit	k5eAaPmNgNnS	poškodit
<g/>
.	.	kIx.	.
</s>
<s>
Rekonstrukční	rekonstrukční	k2eAgFnPc4d1	rekonstrukční
práce	práce	k1gFnPc4	práce
vedl	vést	k5eAaImAgMnS	vést
inženýr	inženýr	k1gMnSc1	inženýr
Franz	Franz	k1gMnSc1	Franz
Herget	Herget	k1gMnSc1	Herget
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
navigačním	navigační	k2eAgMnSc7d1	navigační
stavebním	stavební	k2eAgMnSc7d1	stavební
ředitelem	ředitel	k1gMnSc7	ředitel
Františkem	František	k1gMnSc7	František
Traxalem	Traxal	k1gMnSc7	Traxal
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odstranění	odstranění	k1gNnSc6	odstranění
porušeného	porušený	k2eAgNnSc2d1	porušené
zdiva	zdivo	k1gNnSc2	zdivo
byly	být	k5eAaImAgFnP	být
základy	základ	k1gInPc4	základ
zpevněny	zpevnit	k5eAaPmNgInP	zpevnit
novými	nový	k2eAgFnPc7d1	nová
pilotami	pilota	k1gFnPc7	pilota
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
do	do	k7c2	do
dna	dno	k1gNnSc2	dno
zatloukaly	zatloukat	k5eAaImAgFnP	zatloukat
ručními	ruční	k2eAgNnPc7d1	ruční
beranidly	beranidlo	k1gNnPc7	beranidlo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
piloty	pilot	k1gInPc4	pilot
byl	být	k5eAaImAgInS	být
nasazen	nasazen	k2eAgInSc1d1	nasazen
dřevěný	dřevěný	k2eAgInSc1d1	dřevěný
rošt	rošt	k1gInSc1	rošt
a	a	k8xC	a
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
pokládány	pokládán	k2eAgInPc1d1	pokládán
mlýnské	mlýnský	k2eAgInPc1d1	mlýnský
kameny	kámen	k1gInPc1	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
povodni	povodeň	k1gFnSc6	povodeň
v	v	k7c6	v
září	září	k1gNnSc6	září
1890	[number]	k4	1890
protrhly	protrhnout	k5eAaPmAgFnP	protrhnout
most	most	k1gInSc4	most
klády	kláda	k1gFnSc2	kláda
z	z	k7c2	z
utržených	utržený	k2eAgInPc2d1	utržený
vorů	vor	k1gInPc2	vor
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
o	o	k7c4	o
Karlův	Karlův	k2eAgInSc4d1	Karlův
most	most	k1gInSc4	most
zarazily	zarazit	k5eAaPmAgFnP	zarazit
a	a	k8xC	a
bily	bít	k5eAaImAgFnP	bít
do	do	k7c2	do
jeho	jeho	k3xOp3gInPc2	jeho
pilířů	pilíř	k1gInPc2	pilíř
<g/>
.	.	kIx.	.
</s>
<s>
Hladina	hladina	k1gFnSc1	hladina
řeky	řeka	k1gFnSc2	řeka
stoupla	stoupnout	k5eAaPmAgFnS	stoupnout
o	o	k7c4	o
2,5	[number]	k4	2,5
až	až	k9	až
3	[number]	k4	3
metry	metr	k1gInPc4	metr
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
o	o	k7c4	o
půl	půl	k1xP	půl
šesté	šestý	k4xOgFnSc2	šestý
ráno	ráno	k6eAd1	ráno
spadly	spadnout	k5eAaPmAgInP	spadnout
dva	dva	k4xCgInPc1	dva
oblouky	oblouk	k1gInPc1	oblouk
mostu	most	k1gInSc2	most
<g/>
;	;	kIx,	;
poškozeny	poškozen	k2eAgFnPc1d1	poškozena
byly	být	k5eAaImAgFnP	být
tři	tři	k4xCgInPc4	tři
pilíře	pilíř	k1gInPc4	pilíř
(	(	kIx(	(
<g/>
podemleté	podemletý	k2eAgFnPc4d1	podemletá
vodou	voda	k1gFnSc7	voda
<g/>
)	)	kIx)	)
a	a	k8xC	a
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
spadly	spadnout	k5eAaPmAgInP	spadnout
dvě	dva	k4xCgFnPc4	dva
sochy	socha	k1gFnPc4	socha
zdobící	zdobící	k2eAgInSc1d1	zdobící
most	most	k1gInSc1	most
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
Ignáce	Ignác	k1gMnSc2	Ignác
z	z	k7c2	z
Loyoly	Loyola	k1gFnSc2	Loyola
a	a	k8xC	a
sv.	sv.	kA	sv.
Františka	František	k1gMnSc2	František
Xaverského	xaverský	k2eAgMnSc2d1	xaverský
od	od	k7c2	od
F.	F.	kA	F.
M.	M.	kA	M.
Brokoffa	Brokoff	k1gMnSc2	Brokoff
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nebývale	nebývale	k6eAd1	nebývale
velké	velký	k2eAgNnSc4d1	velké
poškození	poškození	k1gNnSc4	poškození
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
představovalo	představovat	k5eAaImAgNnS	představovat
problém	problém	k1gInSc4	problém
pro	pro	k7c4	pro
život	život	k1gInSc4	život
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
most	most	k1gInSc1	most
byl	být	k5eAaImAgInS	být
důležitou	důležitý	k2eAgFnSc7d1	důležitá
dopravní	dopravní	k2eAgFnSc7d1	dopravní
spojnicí	spojnice	k1gFnSc7	spojnice
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
opadnutí	opadnutí	k1gNnSc6	opadnutí
vody	voda	k1gFnSc2	voda
byl	být	k5eAaImAgInS	být
o	o	k7c4	o
posouzení	posouzení	k1gNnSc4	posouzení
rozsahu	rozsah	k1gInSc2	rozsah
škod	škoda	k1gFnPc2	škoda
a	a	k8xC	a
návrh	návrh	k1gInSc1	návrh
řešení	řešení	k1gNnSc2	řešení
požádán	požádán	k2eAgMnSc1d1	požádán
architekt	architekt	k1gMnSc1	architekt
Josef	Josef	k1gMnSc1	Josef
Hlávka	Hlávka	k1gMnSc1	Hlávka
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
společně	společně	k6eAd1	společně
s	s	k7c7	s
vídeňským	vídeňský	k2eAgMnSc7d1	vídeňský
profesorem	profesor	k1gMnSc7	profesor
Franzem	Franz	k1gMnSc7	Franz
von	von	k1gInSc4	von
Ržihou	Ržiha	k1gMnSc7	Ržiha
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
opravu	oprava	k1gFnSc4	oprava
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
provedena	provést	k5eAaPmNgFnS	provést
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
následující	následující	k2eAgInPc4d1	následující
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
nahrazovala	nahrazovat	k5eAaImAgFnS	nahrazovat
zbořené	zbořený	k2eAgFnPc4d1	zbořená
části	část	k1gFnPc4	část
dřevěná	dřevěný	k2eAgFnSc1d1	dřevěná
konstrukce	konstrukce	k1gFnSc1	konstrukce
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ze	z	k7c2	z
severní	severní	k2eAgFnSc2d1	severní
strany	strana	k1gFnSc2	strana
mostu	most	k1gInSc2	most
obcházela	obcházet	k5eAaImAgFnS	obcházet
poškozené	poškozený	k1gMnPc4	poškozený
místo	místo	k6eAd1	místo
<g/>
;	;	kIx,	;
nepoškozené	poškozený	k2eNgInPc1d1	nepoškozený
konce	konec	k1gInPc1	konec
mostu	most	k1gInSc2	most
nadále	nadále	k6eAd1	nadále
sloužily	sloužit	k5eAaImAgInP	sloužit
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
opravě	oprava	k1gFnSc6	oprava
již	již	k6eAd1	již
byla	být	k5eAaImAgFnS	být
použita	použít	k5eAaPmNgFnS	použít
nová	nový	k2eAgFnSc1d1	nová
metoda	metoda	k1gFnSc1	metoda
-	-	kIx~	-
pilíře	pilíř	k1gInPc1	pilíř
se	se	k3xPyFc4	se
nově	nově	k6eAd1	nově
zakládaly	zakládat	k5eAaImAgFnP	zakládat
na	na	k7c6	na
železných	železný	k2eAgInPc6d1	železný
kesonech	keson	k1gInPc6	keson
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
opravě	oprava	k1gFnSc6	oprava
se	se	k3xPyFc4	se
uvažovalo	uvažovat	k5eAaImAgNnS	uvažovat
i	i	k9	i
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zřícené	zřícený	k2eAgFnPc1d1	zřícená
tři	tři	k4xCgFnPc1	tři
klenby	klenba	k1gFnPc1	klenba
budou	být	k5eAaImBp3nP	být
nahrazeny	nahradit	k5eAaPmNgFnP	nahradit
dvěma	dva	k4xCgFnPc7	dva
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
usnadnil	usnadnit	k5eAaPmAgInS	usnadnit
průtok	průtok	k1gInSc1	průtok
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
však	však	k9	však
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
zásada	zásada	k1gFnSc1	zásada
zachování	zachování	k1gNnSc2	zachování
původní	původní	k2eAgFnSc2d1	původní
podoby	podoba	k1gFnSc2	podoba
mostu	most	k1gInSc2	most
<g/>
,	,	kIx,	,
nové	nový	k2eAgInPc1d1	nový
pilíře	pilíř	k1gInPc1	pilíř
byly	být	k5eAaImAgInP	být
pouze	pouze	k6eAd1	pouze
vybudovány	vybudovat	k5eAaPmNgInP	vybudovat
nepatrně	nepatrně	k6eAd1	nepatrně
užší	úzký	k2eAgInPc1d2	užší
<g/>
.	.	kIx.	.
</s>
<s>
Obnova	obnova	k1gFnSc1	obnova
zřícené	zřícený	k2eAgFnSc2d1	zřícená
části	část	k1gFnSc2	část
mostu	most	k1gInSc2	most
byla	být	k5eAaImAgFnS	být
dokončena	dokončen	k2eAgFnSc1d1	dokončena
19	[number]	k4	19
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1892	[number]	k4	1892
<g/>
.	.	kIx.	.
</s>
<s>
Utopené	utopený	k2eAgFnPc1d1	utopená
sochy	socha	k1gFnPc1	socha
však	však	k9	však
byly	být	k5eAaImAgFnP	být
z	z	k7c2	z
Vltavy	Vltava	k1gFnSc2	Vltava
vyzvednuty	vyzvednout	k5eAaPmNgFnP	vyzvednout
přičiněním	přičinění	k1gNnSc7	přičinění
J.	J.	kA	J.
Hlávky	hlávka	k1gFnSc2	hlávka
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1901	[number]	k4	1901
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
letech	léto	k1gNnPc6	léto
(	(	kIx(	(
<g/>
1902	[number]	k4	1902
<g/>
-	-	kIx~	-
<g/>
1904	[number]	k4	1904
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
původní	původní	k2eAgInPc1d1	původní
pilíře	pilíř	k1gInPc1	pilíř
zpevňovaly	zpevňovat	k5eAaImAgInP	zpevňovat
menšími	malý	k2eAgInPc7d2	menší
kesonovými	kesonový	k2eAgInPc7d1	kesonový
věnci	věnec	k1gInPc7	věnec
<g/>
,	,	kIx,	,
spuštěnými	spuštěný	k2eAgInPc7d1	spuštěný
kolem	kolem	k7c2	kolem
základů	základ	k1gInPc2	základ
<g/>
.	.	kIx.	.
</s>
<s>
Stavební	stavební	k2eAgInSc1d1	stavební
průzkum	průzkum	k1gInSc1	průzkum
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1966	[number]	k4	1966
<g/>
-	-	kIx~	-
<g/>
1967	[number]	k4	1967
ukázal	ukázat	k5eAaPmAgInS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
most	most	k1gInSc1	most
ohrožují	ohrožovat	k5eAaImIp3nP	ohrožovat
zejména	zejména	k9	zejména
trhlinky	trhlinka	k1gFnPc1	trhlinka
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgFnPc7	jenž
vnikala	vnikat	k5eAaImAgFnS	vnikat
do	do	k7c2	do
konstrukcí	konstrukce	k1gFnPc2	konstrukce
dešťová	dešťový	k2eAgFnSc1d1	dešťová
voda	voda	k1gFnSc1	voda
se	se	k3xPyFc4	se
solí	solit	k5eAaImIp3nS	solit
ze	z	k7c2	z
zimních	zimní	k2eAgInPc2d1	zimní
posypů	posyp	k1gInPc2	posyp
vozovky	vozovka	k1gFnSc2	vozovka
<g/>
.	.	kIx.	.
</s>
<s>
Rozevírání	rozevírání	k1gNnSc1	rozevírání
mostu	most	k1gInSc2	most
mělo	mít	k5eAaImAgNnS	mít
proto	proto	k8xC	proto
být	být	k5eAaImF	být
zastaveno	zastaven	k2eAgNnSc1d1	zastaveno
soustavou	soustava	k1gFnSc7	soustava
kotev	kotva	k1gFnPc2	kotva
<g/>
,	,	kIx,	,
po	po	k7c6	po
délce	délka	k1gFnSc6	délka
byla	být	k5eAaImAgFnS	být
vložena	vložit	k5eAaPmNgFnS	vložit
železobetonová	železobetonový	k2eAgFnSc1d1	železobetonová
deska	deska	k1gFnSc1	deska
se	s	k7c7	s
sítí	síť	k1gFnSc7	síť
táhel	táhlo	k1gNnPc2	táhlo
<g/>
.	.	kIx.	.
</s>
<s>
Teplotní	teplotní	k2eAgFnPc4d1	teplotní
změny	změna	k1gFnPc4	změna
a	a	k8xC	a
pronikání	pronikání	k1gNnSc4	pronikání
vody	voda	k1gFnSc2	voda
měly	mít	k5eAaImAgInP	mít
být	být	k5eAaImF	být
omezeny	omezit	k5eAaPmNgInP	omezit
vrstvami	vrstva	k1gFnPc7	vrstva
izolací	izolace	k1gFnPc2	izolace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
plášti	plášť	k1gInSc6	plášť
byly	být	k5eAaImAgInP	být
vyměněny	vyměněn	k2eAgInPc1d1	vyměněn
poškozené	poškozený	k2eAgInPc1d1	poškozený
pískovcové	pískovcový	k2eAgInPc1d1	pískovcový
kvádry	kvádr	k1gInPc1	kvádr
<g/>
.	.	kIx.	.
</s>
<s>
Předchozí	předchozí	k2eAgInSc1d1	předchozí
asfaltový	asfaltový	k2eAgInSc1d1	asfaltový
povrch	povrch	k1gInSc1	povrch
<g/>
,	,	kIx,	,
zřízený	zřízený	k2eAgInSc1d1	zřízený
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
nahradily	nahradit	k5eAaPmAgInP	nahradit
štípané	štípaný	k2eAgInPc1d1	štípaný
pásky	pásek	k1gInPc1	pásek
žuly	žout	k5eAaImAgInP	žout
<g/>
.	.	kIx.	.
</s>
<s>
Most	most	k1gInSc1	most
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
dokončení	dokončení	k1gNnSc2	dokončení
rekonstrukce	rekonstrukce	k1gFnSc2	rekonstrukce
vyhrazen	vyhradit	k5eAaPmNgInS	vyhradit
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
pěší	pěší	k2eAgMnPc4d1	pěší
<g/>
.	.	kIx.	.
</s>
<s>
Náklad	náklad	k1gInSc1	náklad
na	na	k7c4	na
celkovou	celkový	k2eAgFnSc4d1	celková
opravu	oprava	k1gFnSc4	oprava
činil	činit	k5eAaImAgInS	činit
asi	asi	k9	asi
50	[number]	k4	50
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
pískovcových	pískovcový	k2eAgInPc2d1	pískovcový
kvádrů	kvádr	k1gInPc2	kvádr
již	již	k6eAd1	již
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
není	být	k5eNaImIp3nS	být
původních	původní	k2eAgNnPc2d1	původní
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
ze	z	k7c2	z
14	[number]	k4	14
<g/>
.	.	kIx.	.
a	a	k8xC	a
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Nově	nově	k6eAd1	nově
vyměněné	vyměněný	k2eAgInPc1d1	vyměněný
kvádříky	kvádřík	k1gInPc1	kvádřík
jsou	být	k5eAaImIp3nP	být
šedé	šedý	k2eAgInPc1d1	šedý
nebo	nebo	k8xC	nebo
jasně	jasně	k6eAd1	jasně
žluté	žlutý	k2eAgFnPc1d1	žlutá
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInPc1d1	původní
kameny	kámen	k1gInPc1	kámen
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
silně	silně	k6eAd1	silně
erodované	erodovaný	k2eAgInPc1d1	erodovaný
a	a	k8xC	a
některé	některý	k3yIgInPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
nesou	nést	k5eAaImIp3nP	nést
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
povrchu	povrch	k1gInSc6	povrch
kamenické	kamenický	k2eAgFnSc2d1	kamenická
značky	značka	k1gFnSc2	značka
(	(	kIx(	(
<g/>
např.	např.	kA	např.
pilíř	pilíř	k1gInSc4	pilíř
v	v	k7c6	v
korytu	koryto	k1gNnSc6	koryto
Čertovky	Čertovka	k1gFnSc2	Čertovka
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgInSc4	třetí
pilíř	pilíř	k1gInSc4	pilíř
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
Starého	Starého	k2eAgNnSc2d1	Starého
Města	město	k1gNnSc2	město
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
od	od	k7c2	od
r.	r.	kA	r.
2001	[number]	k4	2001
se	se	k3xPyFc4	se
v	v	k7c6	v
odborných	odborný	k2eAgInPc6d1	odborný
kruzích	kruh	k1gInPc6	kruh
i	i	k9	i
veřejně	veřejně	k6eAd1	veřejně
vedla	vést	k5eAaImAgFnS	vést
debata	debata	k1gFnSc1	debata
o	o	k7c6	o
dalších	další	k2eAgFnPc6d1	další
opravách	oprava	k1gFnPc6	oprava
mostu	most	k1gInSc2	most
<g/>
.	.	kIx.	.
</s>
<s>
Předchozí	předchozí	k2eAgFnSc1d1	předchozí
generální	generální	k2eAgFnSc1d1	generální
oprava	oprava	k1gFnSc1	oprava
mostu	most	k1gInSc2	most
měla	mít	k5eAaImAgFnS	mít
své	svůj	k3xOyFgInPc4	svůj
problémy	problém	k1gInPc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Hydroizolace	hydroizolace	k1gFnSc1	hydroizolace
pod	pod	k7c7	pod
dlažbou	dlažba	k1gFnSc7	dlažba
neplnila	plnit	k5eNaImAgFnS	plnit
místy	místy	k6eAd1	místy
svou	svůj	k3xOyFgFnSc4	svůj
úlohu	úloha	k1gFnSc4	úloha
a	a	k8xC	a
plně	plně	k6eAd1	plně
nezabránila	zabránit	k5eNaPmAgFnS	zabránit
průsaku	průsak	k1gInSc3	průsak
srážkové	srážkový	k2eAgFnSc2d1	srážková
vody	voda	k1gFnSc2	voda
do	do	k7c2	do
konstrukce	konstrukce	k1gFnSc2	konstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
diskutovanou	diskutovaný	k2eAgFnSc7d1	diskutovaná
otázkou	otázka	k1gFnSc7	otázka
byla	být	k5eAaImAgFnS	být
funkce	funkce	k1gFnSc1	funkce
železobetonové	železobetonový	k2eAgFnSc2d1	železobetonová
desky	deska	k1gFnSc2	deska
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
mít	mít	k5eAaImF	mít
stabilizační	stabilizační	k2eAgFnSc4d1	stabilizační
funkci	funkce	k1gFnSc4	funkce
pro	pro	k7c4	pro
konstrukci	konstrukce	k1gFnSc4	konstrukce
mostu	most	k1gInSc2	most
<g/>
,	,	kIx,	,
omezovat	omezovat	k5eAaImF	omezovat
možnost	možnost	k1gFnSc4	možnost
protržení	protržení	k1gNnSc2	protržení
oblouků	oblouk	k1gInPc2	oblouk
při	při	k7c6	při
nebezpečných	bezpečný	k2eNgFnPc6d1	nebezpečná
povodních	povodeň	k1gFnPc6	povodeň
<g/>
,	,	kIx,	,
spínat	spínat	k5eAaImF	spínat
poprsní	poprsní	k2eAgFnPc4d1	poprsní
zdi	zeď	k1gFnPc4	zeď
a	a	k8xC	a
sloužit	sloužit	k5eAaImF	sloužit
jako	jako	k9	jako
podklad	podklad	k1gInSc4	podklad
pro	pro	k7c4	pro
izolační	izolační	k2eAgFnPc4d1	izolační
vrstvy	vrstva	k1gFnPc4	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Ostré	ostrý	k2eAgInPc1d1	ostrý
spory	spor	k1gInPc1	spor
o	o	k7c4	o
budoucnost	budoucnost	k1gFnSc4	budoucnost
této	tento	k3xDgFnSc2	tento
desky	deska	k1gFnSc2	deska
(	(	kIx(	(
<g/>
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
přicházely	přicházet	k5eAaImAgFnP	přicházet
varianty	varianta	k1gFnPc1	varianta
<g/>
:	:	kIx,	:
ponechat	ponechat	k5eAaPmF	ponechat
<g/>
,	,	kIx,	,
rozřezat	rozřezat	k5eAaPmF	rozřezat
na	na	k7c6	na
části	část	k1gFnSc6	část
či	či	k8xC	či
zcela	zcela	k6eAd1	zcela
odstranit	odstranit	k5eAaPmF	odstranit
<g/>
)	)	kIx)	)
utlumila	utlumit	k5eAaPmAgFnS	utlumit
až	až	k9	až
stoletá	stoletý	k2eAgFnSc1d1	stoletá
voda	voda	k1gFnSc1	voda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
most	most	k1gInSc1	most
přestál	přestát	k5eAaPmAgInS	přestát
bez	bez	k7c2	bez
viditelného	viditelný	k2eAgNnSc2d1	viditelné
poškození	poškození	k1gNnSc2	poškození
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2004	[number]	k4	2004
<g/>
-	-	kIx~	-
<g/>
2005	[number]	k4	2005
byl	být	k5eAaImAgInS	být
spor	spor	k1gInSc1	spor
konečně	konečně	k6eAd1	konečně
uzavřen	uzavřít	k5eAaPmNgInS	uzavřít
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
stanoven	stanovit	k5eAaPmNgInS	stanovit
časový	časový	k2eAgInSc1d1	časový
program	program	k1gInSc1	program
realizace	realizace	k1gFnSc2	realizace
stavby	stavba	k1gFnSc2	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Roky	rok	k1gInPc1	rok
2005	[number]	k4	2005
a	a	k8xC	a
2006	[number]	k4	2006
měly	mít	k5eAaImAgInP	mít
být	být	k5eAaImF	být
věnovány	věnovat	k5eAaImNgInP	věnovat
dopracování	dopracování	k1gNnSc6	dopracování
průzkumů	průzkum	k1gInPc2	průzkum
a	a	k8xC	a
shromáždění	shromáždění	k1gNnSc2	shromáždění
kompletní	kompletní	k2eAgFnSc2d1	kompletní
dokumentace	dokumentace	k1gFnSc2	dokumentace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
byly	být	k5eAaImAgInP	být
také	také	k6eAd1	také
první	první	k4xOgInPc4	první
dva	dva	k4xCgInPc4	dva
pilíře	pilíř	k1gInPc4	pilíř
(	(	kIx(	(
<g/>
osmý	osmý	k4xOgMnSc1	osmý
a	a	k8xC	a
devátý	devátý	k4xOgMnSc1	devátý
z	z	k7c2	z
malostranské	malostranský	k2eAgFnSc2d1	Malostranská
strany	strana	k1gFnSc2	strana
<g/>
)	)	kIx)	)
ukotveny	ukotven	k2eAgInPc1d1	ukotven
do	do	k7c2	do
betonových	betonový	k2eAgInPc2d1	betonový
sarkofágů	sarkofág	k1gInPc2	sarkofág
<g/>
.	.	kIx.	.
</s>
<s>
Oprava	oprava	k1gFnSc1	oprava
horní	horní	k2eAgFnSc2d1	horní
stavby	stavba	k1gFnSc2	stavba
začala	začít	k5eAaPmAgFnS	začít
20	[number]	k4	20
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2007	[number]	k4	2007
a	a	k8xC	a
12	[number]	k4	12
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2010	[number]	k4	2010
byla	být	k5eAaImAgFnS	být
stavba	stavba	k1gFnSc1	stavba
kolaudována	kolaudovat	k5eAaBmNgFnS	kolaudovat
<g/>
.	.	kIx.	.
</s>
<s>
Most	most	k1gInSc1	most
se	se	k3xPyFc4	se
opravoval	opravovat	k5eAaImAgInS	opravovat
po	po	k7c6	po
částech	část	k1gFnPc6	část
a	a	k8xC	a
za	za	k7c2	za
provozu	provoz	k1gInSc2	provoz
<g/>
,	,	kIx,	,
zůstal	zůstat	k5eAaPmAgInS	zůstat
zachován	zachovat	k5eAaPmNgInS	zachovat
čtyřmetrový	čtyřmetrový	k2eAgInSc1d1	čtyřmetrový
koridor	koridor	k1gInSc1	koridor
přístupný	přístupný	k2eAgInSc1d1	přístupný
veřejnosti	veřejnost	k1gFnSc3	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
oprav	oprava	k1gFnPc2	oprava
byla	být	k5eAaImAgFnS	být
zjištěna	zjistit	k5eAaPmNgFnS	zjistit
některá	některý	k3yIgNnPc1	některý
dosud	dosud	k6eAd1	dosud
neznámá	známý	k2eNgNnPc1d1	neznámé
fakta	faktum	k1gNnPc1	faktum
o	o	k7c6	o
historii	historie	k1gFnSc6	historie
mostu	most	k1gInSc2	most
<g/>
.	.	kIx.	.
</s>
<s>
Památková	památkový	k2eAgFnSc1d1	památková
inspekce	inspekce	k1gFnSc1	inspekce
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2008	[number]	k4	2008
upozornila	upozornit	k5eAaPmAgFnS	upozornit
na	na	k7c4	na
výrazná	výrazný	k2eAgNnPc4d1	výrazné
pochybení	pochybení	k1gNnPc4	pochybení
<g/>
:	:	kIx,	:
při	při	k7c6	při
opravě	oprava	k1gFnSc6	oprava
mostu	most	k1gInSc2	most
byla	být	k5eAaImAgFnS	být
zásadně	zásadně	k6eAd1	zásadně
dotčena	dotčen	k2eAgFnSc1d1	dotčena
nenahraditelná	nahraditelný	k2eNgFnSc1d1	nenahraditelná
estetická	estetický	k2eAgFnSc1d1	estetická
a	a	k8xC	a
výtvarná	výtvarný	k2eAgFnSc1d1	výtvarná
hodnota	hodnota	k1gFnSc1	hodnota
mostu	most	k1gInSc2	most
<g/>
.	.	kIx.	.
</s>
<s>
Inspekce	inspekce	k1gFnSc1	inspekce
kritizovala	kritizovat	k5eAaImAgFnS	kritizovat
hlavně	hlavně	k6eAd1	hlavně
nekoncepční	koncepční	k2eNgFnSc4d1	nekoncepční
a	a	k8xC	a
nedbalou	dbalý	k2eNgFnSc4d1	nedbalá
opravu	oprava	k1gFnSc4	oprava
zábradlí	zábradlí	k1gNnSc2	zábradlí
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Výběr	výběr	k1gInSc1	výběr
kamenů	kámen	k1gInPc2	kámen
k	k	k7c3	k
vyřazení	vyřazení	k1gNnSc3	vyřazení
byl	být	k5eAaImAgInS	být
prováděn	provádět	k5eAaImNgInS	provádět
nekoncepčně	koncepčně	k6eNd1	koncepčně
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
podmínek	podmínka	k1gFnPc2	podmínka
stanovených	stanovený	k2eAgFnPc2d1	stanovená
orgánem	orgán	k1gInSc7	orgán
státní	státní	k2eAgFnSc2d1	státní
památkové	památkový	k2eAgFnSc2d1	památková
péče	péče	k1gFnSc2	péče
a	a	k8xC	a
ve	v	k7c6	v
značném	značný	k2eAgInSc6d1	značný
rozsahu	rozsah	k1gInSc6	rozsah
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
vyřazených	vyřazený	k2eAgInPc2d1	vyřazený
kamenných	kamenný	k2eAgInPc2d1	kamenný
kvádrů	kvádr	k1gInPc2	kvádr
výrazně	výrazně	k6eAd1	výrazně
přesáhl	přesáhnout	k5eAaPmAgInS	přesáhnout
množství	množství	k1gNnSc4	množství
zjištěné	zjištěný	k2eAgMnPc4d1	zjištěný
diagnostickým	diagnostický	k2eAgInSc7d1	diagnostický
průzkumem	průzkum	k1gInSc7	průzkum
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
etapě	etapa	k1gFnSc6	etapa
se	se	k3xPyFc4	se
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
pěti	pět	k4xCc2	pět
až	až	k9	až
deseti	deset	k4xCc2	deset
let	léto	k1gNnPc2	léto
mají	mít	k5eAaImIp3nP	mít
opravovat	opravovat	k5eAaImF	opravovat
mostní	mostní	k2eAgInPc1d1	mostní
oblouky	oblouk	k1gInPc1	oblouk
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgInPc2d3	nejdůležitější
bodů	bod	k1gInPc2	bod
před	před	k7c7	před
samotným	samotný	k2eAgNnSc7d1	samotné
započetím	započetí	k1gNnSc7	započetí
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
je	být	k5eAaImIp3nS	být
výběr	výběr	k1gInSc4	výběr
vhodné	vhodný	k2eAgFnSc2d1	vhodná
lokality	lokalita	k1gFnSc2	lokalita
pro	pro	k7c4	pro
těžbu	těžba	k1gFnSc4	těžba
pískovce	pískovec	k1gInSc2	pískovec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1759	[number]	k4	1759
byly	být	k5eAaImAgInP	být
sepsány	sepsán	k2eAgInPc1d1	sepsán
komplexní	komplexní	k2eAgInPc1d1	komplexní
předpisy	předpis	k1gInPc1	předpis
pro	pro	k7c4	pro
provoz	provoz	k1gInSc4	provoz
na	na	k7c6	na
mostě	most	k1gInSc6	most
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
nařízeno	nařízen	k2eAgNnSc1d1	nařízeno
chodit	chodit	k5eAaImF	chodit
<g/>
/	/	kIx~	/
<g/>
jezdit	jezdit	k5eAaImF	jezdit
pouze	pouze	k6eAd1	pouze
po	po	k7c6	po
pravé	pravý	k2eAgFnSc6d1	pravá
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1883	[number]	k4	1883
jezdila	jezdit	k5eAaImAgFnS	jezdit
na	na	k7c6	na
mostě	most	k1gInSc6	most
koňka	koňka	k1gFnSc1	koňka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
ji	on	k3xPp3gFnSc4	on
vystřídala	vystřídat	k5eAaPmAgFnS	vystřídat
elektrická	elektrický	k2eAgFnSc1d1	elektrická
tramvaj	tramvaj	k1gFnSc1	tramvaj
<g/>
.	.	kIx.	.
</s>
<s>
Elektrický	elektrický	k2eAgInSc1d1	elektrický
proud	proud	k1gInSc1	proud
byl	být	k5eAaImAgInS	být
přes	přes	k7c4	přes
most	most	k1gInSc4	most
veden	veden	k2eAgInSc4d1	veden
mostovkou	mostovka	k1gFnSc7	mostovka
po	po	k7c6	po
speciálních	speciální	k2eAgFnPc6d1	speciální
kolejnicích	kolejnice	k1gFnPc6	kolejnice
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
zkonstruoval	zkonstruovat	k5eAaPmAgMnS	zkonstruovat
František	František	k1gMnSc1	František
Křižík	Křižík	k1gMnSc1	Křižík
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nebyl	být	k5eNaImAgInS	být
narušen	narušen	k2eAgInSc4d1	narušen
umělecký	umělecký	k2eAgInSc4d1	umělecký
vzhled	vzhled	k1gInSc4	vzhled
mostu	most	k1gInSc2	most
trolejemi	trolej	k1gFnPc7	trolej
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
třech	tři	k4xCgNnPc6	tři
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
těžké	těžký	k2eAgFnSc2d1	těžká
tramvaje	tramvaj	k1gFnSc2	tramvaj
most	most	k1gInSc4	most
dosti	dosti	k6eAd1	dosti
porušují	porušovat	k5eAaImIp3nP	porušovat
a	a	k8xC	a
tak	tak	k6eAd1	tak
byly	být	k5eAaImAgInP	být
nahrazeny	nahradit	k5eAaPmNgInP	nahradit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
autobusy	autobus	k1gInPc1	autobus
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
se	se	k3xPyFc4	se
však	však	k9	však
také	také	k9	také
neosvědčily	osvědčit	k5eNaPmAgFnP	osvědčit
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc4d1	následující
rok	rok	k1gInSc4	rok
byl	být	k5eAaImAgInS	být
autobusový	autobusový	k2eAgInSc1d1	autobusový
provoz	provoz	k1gInSc1	provoz
zrušen	zrušit	k5eAaPmNgInS	zrušit
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
obnoven	obnovit	k5eAaPmNgInS	obnovit
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
autobusy	autobus	k1gInPc7	autobus
na	na	k7c6	na
pneumatikách	pneumatika	k1gFnPc6	pneumatika
<g/>
.	.	kIx.	.
</s>
<s>
Veřejná	veřejný	k2eAgFnSc1d1	veřejná
doprava	doprava	k1gFnSc1	doprava
zde	zde	k6eAd1	zde
fungovala	fungovat	k5eAaImAgFnS	fungovat
až	až	k9	až
do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Automobilový	automobilový	k2eAgInSc1d1	automobilový
provoz	provoz	k1gInSc1	provoz
skončil	skončit	k5eAaPmAgInS	skončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
slouží	sloužit	k5eAaImIp3nS	sloužit
most	most	k1gInSc4	most
jen	jen	k9	jen
pro	pro	k7c4	pro
pěší	pěší	k2eAgMnPc4d1	pěší
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k7c2	uprostřed
mostu	most	k1gInSc2	most
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
domek	domek	k1gInSc4	domek
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
mj.	mj.	kA	mj.
po	po	k7c6	po
podepsání	podepsání	k1gNnSc6	podepsání
vestfálského	vestfálský	k2eAgInSc2d1	vestfálský
míru	mír	k1gInSc2	mír
konala	konat	k5eAaImAgFnS	konat
jednání	jednání	k1gNnSc4	jednání
o	o	k7c4	o
ukončení	ukončení	k1gNnSc4	ukončení
švédského	švédský	k2eAgNnSc2d1	švédské
obléhání	obléhání	k1gNnSc2	obléhání
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
29	[number]	k4	29
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1648	[number]	k4	1648
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
bylo	být	k5eAaImAgNnS	být
podepsáno	podepsat	k5eAaPmNgNnS	podepsat
příměří	příměří	k1gNnSc4	příměří
a	a	k8xC	a
7	[number]	k4	7
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1649	[number]	k4	1649
konečná	konečný	k2eAgFnSc1d1	konečná
mírová	mírový	k2eAgFnSc1d1	mírová
smlouva	smlouva	k1gFnSc1	smlouva
<g/>
.	.	kIx.	.
</s>
