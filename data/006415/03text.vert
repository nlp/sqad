<s>
Jáchyme	Jáchym	k1gMnSc5	Jáchym
<g/>
,	,	kIx,	,
hoď	hodit	k5eAaImRp2nS	hodit
ho	on	k3xPp3gMnSc4	on
do	do	k7c2	do
stroje	stroj	k1gInSc2	stroj
<g/>
!	!	kIx.	!
</s>
<s>
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
filmová	filmový	k2eAgFnSc1d1	filmová
komedie	komedie	k1gFnSc1	komedie
natočená	natočený	k2eAgFnSc1d1	natočená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
režisérem	režisér	k1gMnSc7	režisér
Oldřichem	Oldřich	k1gMnSc7	Oldřich
Lipským	lipský	k2eAgMnSc7d1	lipský
<g/>
.	.	kIx.	.
</s>
<s>
Hrdinou	Hrdina	k1gMnSc7	Hrdina
filmu	film	k1gInSc2	film
je	být	k5eAaImIp3nS	být
roztržitý	roztržitý	k2eAgMnSc1d1	roztržitý
mladý	mladý	k2eAgMnSc1d1	mladý
automechanik	automechanik	k1gMnSc1	automechanik
František	František	k1gMnSc1	František
Koudelka	Koudelka	k1gMnSc1	Koudelka
(	(	kIx(	(
<g/>
hraje	hrát	k5eAaImIp3nS	hrát
Luděk	Luděk	k1gMnSc1	Luděk
Sobota	Sobota	k1gMnSc1	Sobota
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
si	se	k3xPyFc3	se
začne	začít	k5eAaPmIp3nS	začít
řídit	řídit	k5eAaImF	řídit
život	život	k1gInSc4	život
podle	podle	k7c2	podle
počítačem	počítač	k1gInSc7	počítač
sestaveného	sestavený	k2eAgInSc2d1	sestavený
kondiciogramu	kondiciogram	k1gInSc2	kondiciogram
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
období	období	k1gNnSc1	období
nejistoty	nejistota	k1gFnSc2	nejistota
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
životě	život	k1gInSc6	život
střídají	střídat	k5eAaImIp3nP	střídat
lepší	dobrý	k2eAgInPc4d2	lepší
dny	den	k1gInPc4	den
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
filmu	film	k1gInSc2	film
pochází	pocházet	k5eAaImIp3nS	pocházet
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
hlášek	hláška	k1gFnPc2	hláška
a	a	k8xC	a
scén	scéna	k1gFnPc2	scéna
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
obecně	obecně	k6eAd1	obecně
známými	známá	k1gFnPc7	známá
<g/>
.	.	kIx.	.
</s>
<s>
Symbolickými	symbolický	k2eAgFnPc7d1	symbolická
postavami	postava	k1gFnPc7	postava
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
například	například	k6eAd1	například
psychiatr	psychiatr	k1gMnSc1	psychiatr
docent	docent	k1gMnSc1	docent
Chocholoušek	chocholoušek	k1gMnSc1	chocholoušek
(	(	kIx(	(
<g/>
hraje	hrát	k5eAaImIp3nS	hrát
Václav	Václav	k1gMnSc1	Václav
Lohniský	Lohniský	k1gMnSc1	Lohniský
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
japonský	japonský	k2eAgMnSc1d1	japonský
malíř	malíř	k1gMnSc1	malíř
miniatur	miniatura	k1gFnPc2	miniatura
Uko	Uko	k1gMnSc1	Uko
Ješita	ješita	k1gMnSc1	ješita
(	(	kIx(	(
<g/>
hraje	hrát	k5eAaImIp3nS	hrát
Tetsuchi	Tetsuche	k1gFnSc4	Tetsuche
Sassagawa	Sassagaw	k1gInSc2	Sassagaw
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výraznou	výrazný	k2eAgFnSc7d1	výrazná
dvojicí	dvojice	k1gFnSc7	dvojice
byli	být	k5eAaImAgMnP	být
i	i	k9	i
vedoucí	vedoucí	k1gMnPc1	vedoucí
autoservisu	autoservis	k1gInSc2	autoservis
Karfík	Karfík	k1gInSc1	Karfík
(	(	kIx(	(
<g/>
hraje	hrát	k5eAaImIp3nS	hrát
Ladislav	Ladislav	k1gMnSc1	Ladislav
Smoljak	Smoljak	k1gMnSc1	Smoljak
<g/>
)	)	kIx)	)
a	a	k8xC	a
psycholog	psycholog	k1gMnSc1	psycholog
Klásek	klásek	k1gInSc1	klásek
(	(	kIx(	(
<g/>
hraje	hrát	k5eAaImIp3nS	hrát
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Svěrák	Svěrák	k1gMnSc1	Svěrák
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mladý	mladý	k2eAgMnSc1d1	mladý
automechanik	automechanik	k1gMnSc1	automechanik
František	František	k1gMnSc1	František
Koudelka	Koudelka	k1gMnSc1	Koudelka
se	se	k3xPyFc4	se
loučí	loučit	k5eAaImIp3nS	loučit
na	na	k7c6	na
vesnici	vesnice	k1gFnSc6	vesnice
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
pracovišti	pracoviště	k1gNnSc6	pracoviště
v	v	k7c6	v
STS	STS	kA	STS
Chvojkovice-Brod	Chvojkovice-Brod	k1gInSc4	Chvojkovice-Brod
a	a	k8xC	a
jede	jet	k5eAaImIp3nS	jet
za	za	k7c7	za
svou	svůj	k3xOyFgFnSc7	svůj
tetou	teta	k1gFnSc7	teta
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
je	být	k5eAaImIp3nS	být
plachý	plachý	k2eAgMnSc1d1	plachý
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
roztržitý	roztržitý	k2eAgMnSc1d1	roztržitý
člověk	člověk	k1gMnSc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
posledního	poslední	k2eAgInSc2d1	poslední
dne	den	k1gInSc2	den
v	v	k7c6	v
STS	STS	kA	STS
zapomene	zapomnět	k5eAaImIp3nS	zapomnět
zašpalkovat	zašpalkovat	k5eAaPmF	zašpalkovat
kola	kolo	k1gNnSc2	kolo
traktoru	traktor	k1gInSc2	traktor
pojmenovaného	pojmenovaný	k2eAgInSc2d1	pojmenovaný
"	"	kIx"	"
<g/>
Máňa	Máňa	k1gFnSc1	Máňa
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
sjede	sjet	k5eAaPmIp3nS	sjet
do	do	k7c2	do
rybníka	rybník	k1gInSc2	rybník
a	a	k8xC	a
potopí	potopit	k5eAaPmIp3nP	potopit
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Vedoucí	vedoucí	k1gMnSc1	vedoucí
ho	on	k3xPp3gMnSc4	on
raději	rád	k6eAd2	rád
posílá	posílat	k5eAaImIp3nS	posílat
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
už	už	k9	už
nic	nic	k3yNnSc1	nic
nezkazil	zkazit	k5eNaPmAgMnS	zkazit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vsi	ves	k1gFnSc6	ves
si	se	k3xPyFc3	se
nechá	nechat	k5eAaPmIp3nS	nechat
před	před	k7c7	před
odjezdem	odjezd	k1gInSc7	odjezd
za	za	k7c4	za
15	[number]	k4	15
Kčs	Kčs	kA	Kčs
vypracovat	vypracovat	k5eAaPmF	vypracovat
kondiciogram	kondiciogram	k1gInSc4	kondiciogram
<g/>
,	,	kIx,	,
počítačem	počítač	k1gInSc7	počítač
vytvořenou	vytvořený	k2eAgFnSc4d1	vytvořená
předpověď	předpověď	k1gFnSc4	předpověď
dobrých	dobrý	k2eAgInPc2d1	dobrý
<g/>
,	,	kIx,	,
neutrálních	neutrální	k2eAgInPc2d1	neutrální
<g/>
,	,	kIx,	,
horších	zlý	k2eAgInPc2d2	horší
a	a	k8xC	a
kritických	kritický	k2eAgInPc2d1	kritický
dnů	den	k1gInPc2	den
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
jej	on	k3xPp3gNnSc4	on
přivítá	přivítat	k5eAaPmIp3nS	přivítat
tetička	tetička	k1gFnSc1	tetička
Marie	Marie	k1gFnSc1	Marie
Sýkorová	Sýkorová	k1gFnSc1	Sýkorová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
psa	pes	k1gMnSc4	pes
Gregora	Gregor	k1gMnSc4	Gregor
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
bude	být	k5eAaImBp3nS	být
spát	spát	k5eAaImF	spát
v	v	k7c6	v
pokoji	pokoj	k1gInSc6	pokoj
s	s	k7c7	s
Gregorem	Gregor	k1gMnSc7	Gregor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zatímco	zatímco	k8xS	zatímco
Gregor	Gregor	k1gMnSc1	Gregor
má	mít	k5eAaImIp3nS	mít
kvalitní	kvalitní	k2eAgNnSc4d1	kvalitní
lůžko	lůžko	k1gNnSc4	lůžko
<g/>
,	,	kIx,	,
on	on	k3xPp3gMnSc1	on
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
spokojit	spokojit	k5eAaPmF	spokojit
s	s	k7c7	s
rozvrzanou	rozvrzaný	k2eAgFnSc7d1	rozvrzaná
pohovkou	pohovka	k1gFnSc7	pohovka
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
pomocí	pomocí	k7c2	pomocí
úplatku	úplatek	k1gInSc2	úplatek
topinkami	topinka	k1gFnPc7	topinka
Gregora	Gregor	k1gMnSc4	Gregor
nalákat	nalákat	k5eAaPmF	nalákat
na	na	k7c4	na
pohovku	pohovka	k1gFnSc4	pohovka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
lépe	dobře	k6eAd2	dobře
vyspal	vyspat	k5eAaPmAgMnS	vyspat
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
posteli	postel	k1gFnSc6	postel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tetička	tetička	k1gFnSc1	tetička
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
dojednala	dojednat	k5eAaPmAgFnS	dojednat
práci	práce	k1gFnSc4	práce
v	v	k7c6	v
autoservisu	autoservis	k1gInSc6	autoservis
<g/>
.	.	kIx.	.
</s>
<s>
Večer	večer	k6eAd1	večer
však	však	k9	však
musí	muset	k5eAaImIp3nS	muset
pravidelně	pravidelně	k6eAd1	pravidelně
venčit	venčit	k5eAaImF	venčit
psa	pes	k1gMnSc4	pes
<g/>
.	.	kIx.	.
</s>
<s>
Ráno	ráno	k6eAd1	ráno
se	se	k3xPyFc4	se
František	František	k1gMnSc1	František
hlásí	hlásit	k5eAaImIp3nS	hlásit
do	do	k7c2	do
zaměstnání	zaměstnání	k1gNnSc2	zaměstnání
u	u	k7c2	u
vedoucího	vedoucí	k2eAgInSc2d1	vedoucí
autoservisu	autoservis	k1gInSc2	autoservis
Karfíka	Karfík	k1gMnSc2	Karfík
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
mu	on	k3xPp3gMnSc3	on
společně	společně	k6eAd1	společně
s	s	k7c7	s
podnikovým	podnikový	k2eAgMnSc7d1	podnikový
psychologem	psycholog	k1gMnSc7	psycholog
Kláskem	klásek	k1gInSc7	klásek
dá	dát	k5eAaPmIp3nS	dát
školení	školení	k1gNnSc4	školení
a	a	k8xC	a
varuje	varovat	k5eAaImIp3nS	varovat
jej	on	k3xPp3gMnSc4	on
především	především	k9	především
před	před	k7c7	před
přijímáním	přijímání	k1gNnSc7	přijímání
úplatků	úplatek	k1gInPc2	úplatek
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
poznamená	poznamenat	k5eAaPmIp3nS	poznamenat
<g/>
,	,	kIx,	,
každého	každý	k3xTgMnSc4	každý
čtvrt	čtvrt	k1gFnSc1	čtvrt
roku	rok	k1gInSc2	rok
opouští	opouštět	k5eAaImIp3nS	opouštět
bránu	brána	k1gFnSc4	brána
servisu	servis	k1gInSc2	servis
jeden	jeden	k4xCgMnSc1	jeden
zaměstnanec	zaměstnanec	k1gMnSc1	zaměstnanec
v	v	k7c6	v
sanitním	sanitní	k2eAgInSc6d1	sanitní
voze	vůz	k1gInSc6	vůz
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
odvezen	odvézt	k5eAaPmNgMnS	odvézt
na	na	k7c4	na
psychiatrickou	psychiatrický	k2eAgFnSc4d1	psychiatrická
kliniku	klinika	k1gFnSc4	klinika
docenta	docent	k1gMnSc2	docent
Chocholouška	chocholoušek	k1gMnSc2	chocholoušek
(	(	kIx(	(
<g/>
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
zbláznil	zbláznit	k5eAaPmAgMnS	zbláznit
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
Karfíkova	Karfíkův	k2eAgInSc2d1	Karfíkův
názoru	názor	k1gInSc2	názor
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
úplatků	úplatek	k1gInPc2	úplatek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Koudelka	Koudelka	k1gMnSc1	Koudelka
je	být	k5eAaImIp3nS	být
přidělen	přidělit	k5eAaPmNgInS	přidělit
na	na	k7c4	na
zaučení	zaučení	k1gNnSc4	zaučení
k	k	k7c3	k
Bédovi	Béda	k1gMnSc3	Béda
Hudečkovi	Hudeček	k1gMnSc3	Hudeček
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
zrovna	zrovna	k6eAd1	zrovna
pracuje	pracovat	k5eAaImIp3nS	pracovat
na	na	k7c6	na
zeleném	zelený	k2eAgInSc6d1	zelený
Renaultu	renault	k1gInSc6	renault
16	[number]	k4	16
koketující	koketující	k2eAgFnSc1d1	koketující
paní	paní	k1gFnSc1	paní
Nevyjelové	Nevyjelová	k1gFnSc2	Nevyjelová
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yQgFnSc4	který
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
udělat	udělat	k5eAaPmF	udělat
dojem	dojem	k1gInSc4	dojem
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
se	se	k3xPyFc4	se
řídí	řídit	k5eAaImIp3nS	řídit
svým	svůj	k3xOyFgInSc7	svůj
kondiciogramem	kondiciogram	k1gInSc7	kondiciogram
a	a	k8xC	a
když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
opět	opět	k6eAd1	opět
ujede	ujet	k5eAaPmIp3nS	ujet
automobil	automobil	k1gInSc4	automobil
a	a	k8xC	a
spadne	spadnout	k5eAaPmIp3nS	spadnout
z	z	k7c2	z
mostu	most	k1gInSc2	most
Legií	legie	k1gFnPc2	legie
do	do	k7c2	do
Vltavy	Vltava	k1gFnSc2	Vltava
<g/>
,	,	kIx,	,
zoufá	zoufat	k5eAaBmIp3nS	zoufat
si	se	k3xPyFc3	se
z	z	k7c2	z
kritických	kritický	k2eAgInPc2d1	kritický
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Kondiciogram	kondiciogram	k1gInSc1	kondiciogram
si	se	k3xPyFc3	se
pořídí	pořídit	k5eAaPmIp3nS	pořídit
i	i	k9	i
jeho	jeho	k3xOp3gMnSc1	jeho
spolupracovník	spolupracovník	k1gMnSc1	spolupracovník
Béda	Béda	k1gMnSc1	Béda
Hudeček	Hudeček	k1gMnSc1	Hudeček
<g/>
.	.	kIx.	.
</s>
<s>
Františka	František	k1gMnSc2	František
zaujme	zaujmout	k5eAaPmIp3nS	zaujmout
prodavačka	prodavačka	k1gFnSc1	prodavačka
v	v	k7c6	v
bufetu	bufet	k1gInSc6	bufet
<g/>
.	.	kIx.	.
</s>
<s>
Neví	vědět	k5eNaImIp3nS	vědět
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
ji	on	k3xPp3gFnSc4	on
má	mít	k5eAaImIp3nS	mít
oslovit	oslovit	k5eAaPmF	oslovit
a	a	k8xC	a
tak	tak	k8xC	tak
si	se	k3xPyFc3	se
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
chodí	chodit	k5eAaImIp3nP	chodit
pouze	pouze	k6eAd1	pouze
rozměnit	rozměnit	k5eAaPmF	rozměnit
peníze	peníz	k1gInPc4	peníz
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
odhodlá	odhodlat	k5eAaPmIp3nS	odhodlat
a	a	k8xC	a
seznámí	seznámit	k5eAaPmIp3nS	seznámit
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
nelíbí	líbit	k5eNaImIp3nS	líbit
jejímu	její	k3xOp3gMnSc3	její
kolegovi	kolega	k1gMnSc3	kolega
u	u	k7c2	u
výčepu	výčep	k1gInSc2	výčep
Karlovi	Karel	k1gMnSc3	Karel
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
pozve	pozvat	k5eAaPmIp3nS	pozvat
Blanku	Blanka	k1gFnSc4	Blanka
(	(	kIx(	(
<g/>
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
prodavačka	prodavačka	k1gFnSc1	prodavačka
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
<g/>
)	)	kIx)	)
k	k	k7c3	k
tetě	teta	k1gFnSc3	teta
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
zrovna	zrovna	k6eAd1	zrovna
s	s	k7c7	s
Gregorem	Gregor	k1gMnSc7	Gregor
na	na	k7c6	na
výstavě	výstava	k1gFnSc6	výstava
psů	pes	k1gMnPc2	pes
<g/>
.	.	kIx.	.
</s>
<s>
Teta	Teta	k1gFnSc1	Teta
se	se	k3xPyFc4	se
ale	ale	k9	ale
nečekaně	nečekaně	k6eAd1	nečekaně
vrátí	vrátit	k5eAaPmIp3nS	vrátit
a	a	k8xC	a
najde	najít	k5eAaPmIp3nS	najít
doma	doma	k6eAd1	doma
Františka	Františka	k1gFnSc1	Františka
s	s	k7c7	s
Blankou	Blanka	k1gFnSc7	Blanka
<g/>
.	.	kIx.	.
</s>
<s>
Rozčílí	rozčílit	k5eAaPmIp3nS	rozčílit
se	se	k3xPyFc4	se
a	a	k8xC	a
zakáže	zakázat	k5eAaPmIp3nS	zakázat
mu	on	k3xPp3gMnSc3	on
ji	on	k3xPp3gFnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
navíc	navíc	k6eAd1	navíc
dostane	dostat	k5eAaPmIp3nS	dostat
přes	přes	k7c4	přes
hubu	huba	k1gFnSc4	huba
od	od	k7c2	od
Karla	Karel	k1gMnSc2	Karel
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yQgInSc7	který
se	se	k3xPyFc4	se
potká	potkat	k5eAaPmIp3nS	potkat
<g/>
,	,	kIx,	,
když	když	k8xS	když
doprovází	doprovázet	k5eAaImIp3nS	doprovázet
Blanku	Blanka	k1gFnSc4	Blanka
k	k	k7c3	k
tramvaji	tramvaj	k1gFnSc3	tramvaj
<g/>
.	.	kIx.	.
</s>
<s>
Blanka	Blanka	k1gFnSc1	Blanka
přemluví	přemluvit	k5eAaPmIp3nS	přemluvit
Františka	František	k1gMnSc4	František
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
přihlásil	přihlásit	k5eAaPmAgMnS	přihlásit
do	do	k7c2	do
kurzu	kurz	k1gInSc2	kurz
juda	judo	k1gNnSc2	judo
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
sama	sám	k3xTgMnSc4	sám
chodí	chodit	k5eAaImIp3nP	chodit
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
udělá	udělat	k5eAaPmIp3nS	udělat
dobrý	dobrý	k2eAgInSc4d1	dobrý
dojem	dojem	k1gInSc4	dojem
na	na	k7c4	na
trenéra	trenér	k1gMnSc4	trenér
Tumpacha	Tumpach	k1gMnSc4	Tumpach
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
nemůže	moct	k5eNaImIp3nS	moct
ani	ani	k8xC	ani
hnout	hnout	k5eAaPmF	hnout
<g/>
,	,	kIx,	,
když	když	k8xS	když
si	se	k3xPyFc3	se
jej	on	k3xPp3gMnSc4	on
chce	chtít	k5eAaImIp3nS	chtít
vyzkoušet	vyzkoušet	k5eAaPmF	vyzkoušet
(	(	kIx(	(
<g/>
Františkovi	František	k1gMnSc3	František
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
skřípla	skřípnout	k5eAaPmAgFnS	skřípnout
část	část	k1gFnSc1	část
saka	sako	k1gNnSc2	sako
do	do	k7c2	do
dveří	dveře	k1gFnPc2	dveře
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Začne	začít	k5eAaPmIp3nS	začít
také	také	k9	také
navštěvovat	navštěvovat	k5eAaImF	navštěvovat
biograf	biograf	k1gInSc4	biograf
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
promítají	promítat	k5eAaImIp3nP	promítat
snímek	snímek	k1gInSc4	snímek
Šerif	šerif	k1gMnSc1	šerif
nemá	mít	k5eNaImIp3nS	mít
odpoledne	odpoledne	k6eAd1	odpoledne
čas	čas	k1gInSc4	čas
<g/>
.	.	kIx.	.
</s>
<s>
Líbí	líbit	k5eAaImIp3nS	líbit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
tam	tam	k6eAd1	tam
jedna	jeden	k4xCgFnSc1	jeden
scéna	scéna	k1gFnSc1	scéna
(	(	kIx(	(
<g/>
kdy	kdy	k6eAd1	kdy
šerif	šerif	k1gMnSc1	šerif
pořádně	pořádně	k6eAd1	pořádně
natáhne	natáhnout	k5eAaPmIp3nS	natáhnout
jednomu	jeden	k4xCgNnSc3	jeden
padouchovi	padouchův	k2eAgMnPc1d1	padouchův
<g/>
)	)	kIx)	)
a	a	k8xC	a
ještě	ještě	k9	ještě
v	v	k7c6	v
biografu	biograf	k1gInSc6	biograf
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
nacvičuje	nacvičovat	k5eAaImIp3nS	nacvičovat
<g/>
.	.	kIx.	.
</s>
<s>
Hodlá	hodlat	k5eAaImIp3nS	hodlat
si	se	k3xPyFc3	se
vyřídit	vyřídit	k5eAaPmF	vyřídit
účty	účet	k1gInPc4	účet
s	s	k7c7	s
Karlem	Karel	k1gMnSc7	Karel
<g/>
.	.	kIx.	.
</s>
<s>
Trenér	trenér	k1gMnSc1	trenér
Tumpach	Tumpach	k?	Tumpach
shání	shánět	k5eAaImIp3nS	shánět
na	na	k7c4	na
záskok	záskok	k1gInSc4	záskok
pro	pro	k7c4	pro
turnaj	turnaj	k1gInSc4	turnaj
juda	judo	k1gNnSc2	judo
některého	některý	k3yIgMnSc2	některý
schopného	schopný	k2eAgMnSc2d1	schopný
nováčka	nováček	k1gMnSc2	nováček
(	(	kIx(	(
<g/>
protože	protože	k8xS	protože
Hemele	Hemel	k1gInSc2	Hemel
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
vojně	vojna	k1gFnSc6	vojna
a	a	k8xC	a
Smola	Smola	k1gMnSc1	Smola
se	se	k3xPyFc4	se
rozstonal	rozstonat	k5eAaPmAgMnS	rozstonat
<g/>
)	)	kIx)	)
a	a	k8xC	a
vzpomene	vzpomenout	k5eAaPmIp3nS	vzpomenout
si	se	k3xPyFc3	se
na	na	k7c4	na
Koudelku	koudelka	k1gFnSc4	koudelka
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
moc	moc	k6eAd1	moc
nemá	mít	k5eNaImIp3nS	mít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Blanka	Blanka	k1gFnSc1	Blanka
mu	on	k3xPp3gMnSc3	on
šeptne	šeptnout	k5eAaPmIp3nS	šeptnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
době	doba	k1gFnSc6	doba
turnaje	turnaj	k1gInSc2	turnaj
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
dobré	dobrý	k2eAgInPc4d1	dobrý
dny	den	k1gInPc4	den
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
je	být	k5eAaImIp3nS	být
nominován	nominovat	k5eAaBmNgMnS	nominovat
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
zajde	zajít	k5eAaPmIp3nS	zajít
do	do	k7c2	do
bufetu	bufet	k1gInSc2	bufet
a	a	k8xC	a
informuje	informovat	k5eAaBmIp3nS	informovat
výčepního	výčepní	k2eAgMnSc4d1	výčepní
Karla	Karel	k1gMnSc4	Karel
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
sedmadvacátého	sedmadvacátý	k4xOgInSc2	sedmadvacátý
neopouštěl	opouštět	k5eNaImAgMnS	opouštět
Prahu	Praha	k1gFnSc4	Praha
<g/>
,	,	kIx,	,
vyzývá	vyzývat	k5eAaImIp3nS	vyzývat
jej	on	k3xPp3gMnSc4	on
na	na	k7c4	na
souboj	souboj	k1gInSc4	souboj
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
středu	středa	k1gFnSc4	středa
sedmadvacátého	sedmadvacátý	k4xOgInSc2	sedmadvacátý
František	Františka	k1gFnPc2	Františka
vstane	vstát	k5eAaPmIp3nS	vstát
<g/>
,	,	kIx,	,
sebevědomě	sebevědomě	k6eAd1	sebevědomě
vysvětlí	vysvětlit	k5eAaPmIp3nS	vysvětlit
tetě	teta	k1gFnSc3	teta
Marii	Maria	k1gFnSc4	Maria
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
nepletla	plést	k5eNaImAgFnS	plést
do	do	k7c2	do
osobních	osobní	k2eAgFnPc2d1	osobní
záležitostí	záležitost	k1gFnPc2	záležitost
<g/>
,	,	kIx,	,
a	a	k8xC	a
jde	jít	k5eAaImIp3nS	jít
do	do	k7c2	do
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
mu	on	k3xPp3gNnSc3	on
Béda	Béda	k1gMnSc1	Béda
Hudeček	Hudeček	k1gMnSc1	Hudeček
předá	předat	k5eAaPmIp3nS	předat
do	do	k7c2	do
úschovy	úschova	k1gFnSc2	úschova
své	svůj	k3xOyFgInPc4	svůj
úplatky	úplatek	k1gInPc4	úplatek
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
je	on	k3xPp3gFnPc4	on
neztratil	ztratit	k5eNaPmAgMnS	ztratit
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
totiž	totiž	k9	totiž
kritický	kritický	k2eAgInSc4d1	kritický
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Vedoucí	vedoucí	k1gMnSc1	vedoucí
Karfík	Karfík	k1gMnSc1	Karfík
pomocí	pomocí	k7c2	pomocí
kamery	kamera	k1gFnSc2	kamera
přistihne	přistihnout	k5eAaPmIp3nS	přistihnout
Františka	František	k1gMnSc4	František
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
si	se	k3xPyFc3	se
vytahuje	vytahovat	k5eAaImIp3nS	vytahovat
z	z	k7c2	z
kapse	kapsa	k1gFnSc6	kapsa
štosy	štosy	k?	štosy
bankovek	bankovka	k1gFnPc2	bankovka
a	a	k8xC	a
mumlá	mumlat	k5eAaImIp3nS	mumlat
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
blázen	blázen	k1gMnSc1	blázen
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
přesně	přesně	k6eAd1	přesně
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
chce	chtít	k5eAaImIp3nS	chtít
slyšet	slyšet	k5eAaImF	slyšet
a	a	k8xC	a
okamžitě	okamžitě	k6eAd1	okamžitě
telefonuje	telefonovat	k5eAaImIp3nS	telefonovat
docentu	docent	k1gMnSc3	docent
Chocholouškovi	chocholoušek	k1gMnSc3	chocholoušek
<g/>
.	.	kIx.	.
</s>
<s>
Saniťáci	Saniťáci	k?	Saniťáci
však	však	k9	však
nedopadnou	dopadnout	k5eNaPmIp3nP	dopadnout
Koudelku	koudelka	k1gFnSc4	koudelka
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
Bédu	Béda	k1gFnSc4	Béda
Hudečka	Hudeček	k1gMnSc2	Hudeček
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
poprosil	poprosit	k5eAaPmAgMnS	poprosit
Františka	František	k1gMnSc4	František
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zajel	zajet	k5eAaPmAgMnS	zajet
za	za	k7c4	za
paní	paní	k1gFnSc4	paní
Nevyjelovou	Nevyjelová	k1gFnSc4	Nevyjelová
a	a	k8xC	a
předal	předat	k5eAaPmAgMnS	předat
jí	jíst	k5eAaImIp3nS	jíst
vzkaz	vzkaz	k1gInSc4	vzkaz
-	-	kIx~	-
básničku	básnička	k1gFnSc4	básnička
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Růže	růže	k1gFnSc1	růže
k	k	k7c3	k
lásce	láska	k1gFnSc3	láska
schůdeček	schůdeček	k1gInSc1	schůdeček
<g/>
,	,	kIx,	,
s	s	k7c7	s
úctou	úcta	k1gFnSc7	úcta
Béda	Béda	k1gFnSc1	Béda
Hudeček	Hudeček	k1gMnSc1	Hudeček
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
František	František	k1gMnSc1	František
pak	pak	k6eAd1	pak
odjede	odjet	k5eAaPmIp3nS	odjet
ze	z	k7c2	z
zaměstnání	zaměstnání	k1gNnSc2	zaměstnání
vypůjčeným	vypůjčený	k2eAgInSc7d1	vypůjčený
vozem	vůz	k1gInSc7	vůz
Ford	ford	k1gInSc1	ford
Mustang	mustang	k1gMnSc1	mustang
1966	[number]	k4	1966
Convertible	Convertible	k1gFnSc2	Convertible
dánského	dánský	k2eAgMnSc4d1	dánský
hudebníka	hudebník	k1gMnSc4	hudebník
<g/>
.	.	kIx.	.
</s>
<s>
Předtím	předtím	k6eAd1	předtím
stihne	stihnout	k5eAaPmIp3nS	stihnout
absolvovat	absolvovat	k5eAaPmF	absolvovat
své	svůj	k3xOyFgInPc4	svůj
zápasy	zápas	k1gInPc4	zápas
v	v	k7c6	v
judu	judo	k1gNnSc6	judo
a	a	k8xC	a
soupeře	soupeř	k1gMnPc4	soupeř
k	k	k7c3	k
překvapení	překvapení	k1gNnSc3	překvapení
trenéra	trenér	k1gMnSc2	trenér
Tumpacha	Tumpach	k1gMnSc2	Tumpach
dokonce	dokonce	k9	dokonce
porazit	porazit	k5eAaPmF	porazit
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
chvátá	chvátat	k5eAaImIp3nS	chvátat
do	do	k7c2	do
bufetu	bufet	k1gInSc2	bufet
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
ale	ale	k8xC	ale
Karel	Karel	k1gMnSc1	Karel
není	být	k5eNaImIp3nS	být
<g/>
,	,	kIx,	,
odjel	odjet	k5eAaPmAgMnS	odjet
čepovat	čepovat	k5eAaImF	čepovat
pivo	pivo	k1gNnSc4	pivo
do	do	k7c2	do
Jesenice	Jesenice	k1gFnSc2	Jesenice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bydlišti	bydliště	k1gNnSc6	bydliště
paní	paní	k1gFnSc2	paní
Nevyjelové	Nevyjelová	k1gFnSc2	Nevyjelová
mu	on	k3xPp3gMnSc3	on
sousedka	sousedka	k1gFnSc1	sousedka
(	(	kIx(	(
<g/>
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
domnívá	domnívat	k5eAaImIp3nS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
Bédu	Béda	k1gFnSc4	Béda
Hudečka	Hudeček	k1gMnSc2	Hudeček
<g/>
)	)	kIx)	)
předá	předat	k5eAaPmIp3nS	předat
vzkaz	vzkaz	k1gInSc4	vzkaz
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
za	za	k7c2	za
ní	on	k3xPp3gFnSc2	on
přijel	přijet	k5eAaPmAgMnS	přijet
na	na	k7c4	na
zámek	zámek	k1gInSc4	zámek
Konopiště	Konopiště	k1gNnSc2	Konopiště
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
výstava	výstava	k1gFnSc1	výstava
japonského	japonský	k2eAgMnSc2d1	japonský
malíře	malíř	k1gMnSc2	malíř
Uko	Uko	k1gMnSc2	Uko
Ješity	ješita	k1gMnSc2	ješita
<g/>
.	.	kIx.	.
</s>
<s>
Koudelka	koudelka	k1gFnSc1	koudelka
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
kvapně	kvapně	k6eAd1	kvapně
vydává	vydávat	k5eAaImIp3nS	vydávat
<g/>
,	,	kIx,	,
cestou	cesta	k1gFnSc7	cesta
se	se	k3xPyFc4	se
zúčastní	zúčastnit	k5eAaPmIp3nS	zúčastnit
12	[number]	k4	12
<g/>
.	.	kIx.	.
ročníku	ročník	k1gInSc2	ročník
motoristického	motoristický	k2eAgInSc2d1	motoristický
závodu	závod	k1gInSc2	závod
"	"	kIx"	"
<g/>
Konopišťské	konopišťský	k2eAgNnSc1d1	konopišťské
kolo	kolo	k1gNnSc1	kolo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
tento	tento	k3xDgInSc1	tento
závod	závod	k1gInSc1	závod
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
konkurenci	konkurence	k1gFnSc6	konkurence
automobilů	automobil	k1gInPc2	automobil
Alpine	Alpin	k1gInSc5	Alpin
podaří	podařit	k5eAaPmIp3nS	podařit
vyhrát	vyhrát	k5eAaPmF	vyhrát
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
čemuž	což	k3yQnSc3	což
vehementně	vehementně	k6eAd1	vehementně
protestuje	protestovat	k5eAaBmIp3nS	protestovat
cholerický	cholerický	k2eAgMnSc1d1	cholerický
závodník	závodník	k1gMnSc1	závodník
Stanislav	Stanislav	k1gMnSc1	Stanislav
Volejník	Volejník	k1gMnSc1	Volejník
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
protest	protest	k1gInSc1	protest
je	být	k5eAaImIp3nS	být
uznán	uznat	k5eAaPmNgInS	uznat
za	za	k7c4	za
oprávněný	oprávněný	k2eAgInSc4d1	oprávněný
a	a	k8xC	a
Volejník	Volejník	k1gMnSc1	Volejník
trvá	trvat	k5eAaImIp3nS	trvat
na	na	k7c6	na
opakování	opakování	k1gNnSc6	opakování
slavnostního	slavnostní	k2eAgInSc2d1	slavnostní
ceremoniálu	ceremoniál	k1gInSc2	ceremoniál
předávání	předávání	k1gNnSc2	předávání
cen	cena	k1gFnPc2	cena
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
něho	on	k3xPp3gNnSc2	on
jej	on	k3xPp3gInSc4	on
lapí	lapět	k5eAaImIp3nS	lapět
saniťáci	saniťáci	k?	saniťáci
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
stále	stále	k6eAd1	stále
neúspěšně	úspěšně	k6eNd1	úspěšně
pátrají	pátrat	k5eAaImIp3nP	pátrat
po	po	k7c6	po
Františku	František	k1gMnSc6	František
Koudelkovi	Koudelka	k1gMnSc6	Koudelka
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
konečně	konečně	k6eAd1	konečně
přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
na	na	k7c4	na
výstavu	výstava	k1gFnSc4	výstava
malíře	malíř	k1gMnSc2	malíř
Uko	Uko	k1gMnSc2	Uko
Ješity	ješita	k1gMnSc2	ješita
<g/>
.	.	kIx.	.
</s>
<s>
Pořadatelé	pořadatel	k1gMnPc1	pořadatel
si	se	k3xPyFc3	se
jej	on	k3xPp3gNnSc4	on
s	s	k7c7	s
japonským	japonský	k2eAgMnSc7d1	japonský
malířem	malíř	k1gMnSc7	malíř
miniatur	miniatura	k1gFnPc2	miniatura
spletou	splést	k5eAaPmIp3nP	splést
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
oděn	odět	k5eAaPmNgMnS	odět
v	v	k7c6	v
tradičním	tradiční	k2eAgNnSc6d1	tradiční
japonském	japonský	k2eAgNnSc6d1	Japonské
kimonu	kimono	k1gNnSc6	kimono
(	(	kIx(	(
<g/>
ještě	ještě	k9	ještě
ze	z	k7c2	z
zápasů	zápas	k1gInPc2	zápas
z	z	k7c2	z
juda	judo	k1gNnSc2	judo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tady	tady	k6eAd1	tady
předá	předat	k5eAaPmIp3nS	předat
vzkaz	vzkaz	k1gInSc1	vzkaz
paní	paní	k1gFnSc2	paní
Nevyjelové	Nevyjelová	k1gFnSc2	Nevyjelová
a	a	k8xC	a
poté	poté	k6eAd1	poté
zmizí	zmizet	k5eAaPmIp3nP	zmizet
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
na	na	k7c4	na
Konopiště	Konopiště	k1gNnSc4	Konopiště
přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
skutečný	skutečný	k2eAgMnSc1d1	skutečný
Uko	Uko	k1gMnSc1	Uko
Ješita	ješita	k1gMnSc1	ješita
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zadržen	zadržet	k5eAaPmNgMnS	zadržet
a	a	k8xC	a
odvezen	odvezen	k2eAgMnSc1d1	odvezen
saniťáky	saniťáky	k?	saniťáky
docenta	docent	k1gMnSc4	docent
Chocholouška	chocholoušek	k1gMnSc4	chocholoušek
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
už	už	k6eAd1	už
má	mít	k5eAaImIp3nS	mít
jejich	jejich	k3xOp3gInPc2	jejich
přešlapů	přešlap	k1gInPc2	přešlap
po	po	k7c4	po
krk	krk	k1gInSc4	krk
a	a	k8xC	a
na	na	k7c4	na
další	další	k2eAgInSc4d1	další
výjezd	výjezd	k1gInSc4	výjezd
vyráží	vyrážet	k5eAaImIp3nS	vyrážet
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
on	on	k3xPp3gMnSc1	on
se	se	k3xPyFc4	se
splete	splést	k5eAaPmIp3nS	splést
<g/>
,	,	kIx,	,
když	když	k8xS	když
odchytí	odchytit	k5eAaPmIp3nP	odchytit
uklízeče	uklízeč	k1gMnSc4	uklízeč
v	v	k7c6	v
tělocvičně	tělocvična	k1gFnSc6	tělocvična
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
probíhal	probíhat	k5eAaImAgInS	probíhat
turnaj	turnaj	k1gInSc1	turnaj
juda	judo	k1gNnSc2	judo
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
už	už	k6eAd1	už
je	být	k5eAaImIp3nS	být
mezitím	mezitím	k6eAd1	mezitím
v	v	k7c6	v
Jesenici	Jesenice	k1gFnSc6	Jesenice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vyzve	vyzvat	k5eAaPmIp3nS	vyzvat
na	na	k7c4	na
souboj	souboj	k1gInSc4	souboj
výčepního	výčepní	k2eAgMnSc2d1	výčepní
Karla	Karel	k1gMnSc2	Karel
<g/>
,	,	kIx,	,
svého	svůj	k3xOyFgMnSc4	svůj
rivala	rival	k1gMnSc4	rival
o	o	k7c4	o
Blančinu	Blančin	k2eAgFnSc4d1	Blančina
náklonnost	náklonnost	k1gFnSc4	náklonnost
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
tady	tady	k6eAd1	tady
se	se	k3xPyFc4	se
docentu	docent	k1gMnSc3	docent
Chocholouškovi	chocholoušek	k1gMnSc3	chocholoušek
s	s	k7c7	s
jeho	jeho	k3xOp3gInSc7	jeho
týmem	tým	k1gInSc7	tým
nepodaří	podařit	k5eNaPmIp3nS	podařit
Františka	František	k1gMnSc2	František
Koudelku	Koudelka	k1gMnSc4	Koudelka
polapit	polapit	k5eAaPmF	polapit
<g/>
,	,	kIx,	,
místo	místo	k7c2	místo
něj	on	k3xPp3gNnSc2	on
si	se	k3xPyFc3	se
odváží	odvážet	k5eAaImIp3nS	odvážet
Karla	Karla	k1gFnSc1	Karla
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
už	už	k9	už
je	být	k5eAaImIp3nS	být
moc	moc	k6eAd1	moc
i	i	k9	i
na	na	k7c4	na
docenta	docent	k1gMnSc4	docent
Chocholouška	chocholoušek	k1gMnSc4	chocholoušek
a	a	k8xC	a
protitankovou	protitankový	k2eAgFnSc7d1	protitanková
tarasnicí	tarasnice	k1gFnSc7	tarasnice
(	(	kIx(	(
<g/>
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
zabavena	zabavit	k5eAaPmNgFnS	zabavit
jednomu	jeden	k4xCgMnSc3	jeden
uprchlému	uprchlý	k1gMnSc3	uprchlý
pacientovi	pacient	k1gMnSc3	pacient
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
s	s	k7c7	s
její	její	k3xOp3gFnSc7	její
pomocí	pomoc	k1gFnSc7	pomoc
zničil	zničit	k5eAaPmAgMnS	zničit
tři	tři	k4xCgInPc4	tři
sanitní	sanitní	k2eAgInPc4d1	sanitní
vozy	vůz	k1gInPc4	vůz
<g/>
,	,	kIx,	,
než	než	k8xS	než
byl	být	k5eAaImAgInS	být
dopaden	dopaden	k2eAgMnSc1d1	dopaden
<g/>
)	)	kIx)	)
pálí	pálit	k5eAaImIp3nP	pálit
po	po	k7c6	po
svých	svůj	k3xOyFgMnPc6	svůj
asistentech	asistent	k1gMnPc6	asistent
<g/>
.	.	kIx.	.
</s>
<s>
Dobíjí	dobíjet	k5eAaImIp3nS	dobíjet
mu	on	k3xPp3gMnSc3	on
vedoucí	vedoucí	k1gMnSc1	vedoucí
autoservisu	autoservis	k1gInSc2	autoservis
Karfík	Karfík	k1gMnSc1	Karfík
a	a	k8xC	a
psycholog	psycholog	k1gMnSc1	psycholog
Klásek	klásek	k1gInSc1	klásek
<g/>
.	.	kIx.	.
</s>
<s>
Blanka	Blanka	k1gFnSc1	Blanka
navrhne	navrhnout	k5eAaPmIp3nS	navrhnout
Františkovi	František	k1gMnSc3	František
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
vezmou	vzít	k5eAaPmIp3nP	vzít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závěrečné	závěrečný	k2eAgFnSc6d1	závěrečná
scéně	scéna	k1gFnSc6	scéna
si	se	k3xPyFc3	se
František	František	k1gMnSc1	František
Koudelka	Koudelka	k1gMnSc1	Koudelka
pročítá	pročítat	k5eAaImIp3nS	pročítat
kondiciogram	kondiciogram	k1gInSc4	kondiciogram
a	a	k8xC	a
plánuje	plánovat	k5eAaImIp3nS	plánovat
si	se	k3xPyFc3	se
podle	podle	k7c2	podle
něj	on	k3xPp3gInSc2	on
stavbu	stavba	k1gFnSc4	stavba
stodoly	stodola	k1gFnPc1	stodola
u	u	k7c2	u
malebného	malebný	k2eAgInSc2d1	malebný
venkovského	venkovský	k2eAgInSc2d1	venkovský
domku	domek	k1gInSc2	domek
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
s	s	k7c7	s
Blankou	Blanka	k1gFnSc7	Blanka
nastěhoval	nastěhovat	k5eAaPmAgMnS	nastěhovat
<g/>
.	.	kIx.	.
</s>
<s>
Těhotná	těhotný	k2eAgFnSc1d1	těhotná
Blanka	Blanka	k1gFnSc1	Blanka
přináší	přinášet	k5eAaImIp3nS	přinášet
na	na	k7c4	na
stůl	stůl	k1gInSc4	stůl
talíř	talíř	k1gInSc1	talíř
plný	plný	k2eAgInSc1d1	plný
topinek	topinka	k1gFnPc2	topinka
a	a	k8xC	a
František	František	k1gMnSc1	František
si	se	k3xPyFc3	se
jednu	jeden	k4xCgFnSc4	jeden
vezme	vzít	k5eAaPmIp3nS	vzít
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k6eAd1	kolem
si	se	k3xPyFc3	se
hrají	hrát	k5eAaImIp3nP	hrát
tři	tři	k4xCgFnPc1	tři
jejich	jejich	k3xOp3gFnPc1	jejich
děti	dítě	k1gFnPc1	dítě
v	v	k7c6	v
námořnickém	námořnický	k2eAgInSc6d1	námořnický
stejnokroji	stejnokroj	k1gInSc6	stejnokroj
<g/>
.	.	kIx.	.
</s>
<s>
Přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
k	k	k7c3	k
nim	on	k3xPp3gNnPc3	on
trojice	trojice	k1gFnSc1	trojice
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
delegace	delegace	k1gFnPc1	delegace
v	v	k7c6	v
černém	černý	k2eAgInSc6d1	černý
automobilu	automobil	k1gInSc6	automobil
Tatra	Tatra	k1gFnSc1	Tatra
603	[number]	k4	603
<g/>
.	.	kIx.	.
</s>
<s>
Mluvčí	mluvčí	k1gMnSc1	mluvčí
výpočetního	výpočetní	k2eAgNnSc2d1	výpočetní
střediska	středisko	k1gNnSc2	středisko
Jih	jih	k1gInSc1	jih
se	se	k3xPyFc4	se
omlouvá	omlouvat	k5eAaImIp3nS	omlouvat
<g/>
,	,	kIx,	,
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Františkovi	Františkův	k2eAgMnPc1d1	Františkův
po	po	k7c4	po
celá	celý	k2eAgNnPc4d1	celé
léta	léto	k1gNnPc4	léto
zasílali	zasílat	k5eAaImAgMnP	zasílat
špatné	špatný	k2eAgInPc4d1	špatný
kondiciogramy	kondiciogram	k1gInPc4	kondiciogram
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
patřily	patřit	k5eAaImAgInP	patřit
jeho	jeho	k3xOp3gNnPc3	jeho
jmenovci	jmenovec	k1gMnSc3	jmenovec
-	-	kIx~	-
důchodci	důchodce	k1gMnSc3	důchodce
Františku	František	k1gMnSc3	František
Koudelkovi	Koudelka	k1gMnSc3	Koudelka
z	z	k7c2	z
Příbrami	Příbram	k1gFnSc2	Příbram
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
omluvě	omluva	k1gFnSc3	omluva
se	se	k3xPyFc4	se
připojuje	připojovat	k5eAaImIp3nS	připojovat
i	i	k9	i
zástupce	zástupce	k1gMnSc1	zástupce
firmy	firma	k1gFnSc2	firma
Katoda	katoda	k1gFnSc1	katoda
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
Muži	muž	k1gMnPc1	muž
odcházejí	odcházet	k5eAaImIp3nP	odcházet
a	a	k8xC	a
jako	jako	k9	jako
kompenzaci	kompenzace	k1gFnSc4	kompenzace
za	za	k7c4	za
škodu	škoda	k1gFnSc4	škoda
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
způsobily	způsobit	k5eAaPmAgInP	způsobit
jejich	jejich	k3xOp3gInPc1	jejich
špatné	špatný	k2eAgInPc1d1	špatný
kondiciogramy	kondiciogram	k1gInPc1	kondiciogram
zanechávají	zanechávat	k5eAaImIp3nP	zanechávat
rodině	rodina	k1gFnSc3	rodina
vůz	vůz	k1gInSc4	vůz
Tatra	Tatra	k1gFnSc1	Tatra
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
s	s	k7c7	s
Blankou	Blanka	k1gFnSc7	Blanka
jim	on	k3xPp3gMnPc3	on
mávají	mávat	k5eAaImIp3nP	mávat
na	na	k7c4	na
pozdrav	pozdrav	k1gInSc4	pozdrav
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
tatrovka	tatrovka	k1gFnSc1	tatrovka
nenápadně	nápadně	k6eNd1	nápadně
sjede	sjet	k5eAaPmIp3nS	sjet
do	do	k7c2	do
rybníka	rybník	k1gInSc2	rybník
a	a	k8xC	a
potopí	potopit	k5eAaPmIp3nP	potopit
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Tvárnice	tvárnice	k1gFnPc4	tvárnice
nebrat	brat	k5eNaImF	brat
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
vedoucí	vedoucí	k1gMnSc1	vedoucí
Karfík	Karfík	k1gMnSc1	Karfík
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Třeba	třeba	k6eAd1	třeba
je	být	k5eAaImIp3nS	být
<g/>
...	...	k?	...
<g/>
třeba	třeba	k6eAd1	třeba
se	se	k3xPyFc4	se
vás	vy	k3xPp2nPc2	vy
<g/>
...	...	k?	...
<g/>
plachej	plachej	k?	plachej
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
František	František	k1gMnSc1	František
Koudelka	Koudelka	k1gMnSc1	Koudelka
se	se	k3xPyFc4	se
dvoří	dvořit	k5eAaImIp3nS	dvořit
prodavačce	prodavačka	k1gFnSc3	prodavačka
Blance	Blanka	k1gFnSc3	Blanka
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Nebudem	Nebudem	k?	Nebudem
se	se	k3xPyFc4	se
pouštět	pouštět	k5eAaImF	pouštět
do	do	k7c2	do
žádných	žádný	k3yNgFnPc2	žádný
větších	veliký	k2eAgFnPc2d2	veliký
akcí	akce	k1gFnPc2	akce
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
trenér	trenér	k1gMnSc1	trenér
juda	judo	k1gNnSc2	judo
Tumpach	Tumpach	k?	Tumpach
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k8xS	co
jej	on	k3xPp3gMnSc4	on
František	František	k1gMnSc1	František
Koudelka	Koudelka	k1gMnSc1	Koudelka
povalí	povalit	k5eAaPmIp3nS	povalit
na	na	k7c4	na
žíněnku	žíněnka	k1gFnSc4	žíněnka
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Prosíme	prosit	k5eAaImIp1nP	prosit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
dostavila	dostavit	k5eAaPmAgFnS	dostavit
do	do	k7c2	do
kanceláře	kancelář	k1gFnSc2	kancelář
maminka	maminka	k1gFnSc1	maminka
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
kočárek	kočárek	k1gInSc4	kočárek
s	s	k7c7	s
dítětem	dítě	k1gNnSc7	dítě
před	před	k7c7	před
toaletami	toaleta	k1gFnPc7	toaleta
pláče	plakat	k5eAaImIp3nS	plakat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
hlášení	hlášení	k1gNnSc1	hlášení
v	v	k7c6	v
rozhlase	rozhlas	k1gInSc6	rozhlas
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Já	já	k3xPp1nSc1	já
vám	vy	k3xPp2nPc3	vy
dám	dát	k5eAaPmIp1nS	dát
Koudelku	koudelka	k1gFnSc4	koudelka
<g/>
!	!	kIx.	!
</s>
<s>
Já	já	k3xPp1nSc1	já
vám	vy	k3xPp2nPc3	vy
ukážu	ukázat	k5eAaPmIp1nS	ukázat
Koudelku	koudelka	k1gFnSc4	koudelka
<g/>
!	!	kIx.	!
</s>
<s>
Já	já	k3xPp1nSc1	já
vám	vy	k3xPp2nPc3	vy
zapálím	zapálit	k5eAaPmIp1nS	zapálit
koudelku	koudelka	k1gFnSc4	koudelka
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
docent	docent	k1gMnSc1	docent
Chocholoušek	chocholoušek	k1gMnSc1	chocholoušek
ostřeluje	ostřelovat	k5eAaImIp3nS	ostřelovat
z	z	k7c2	z
protitankové	protitankový	k2eAgFnSc2d1	protitanková
tarasnice	tarasnice	k1gFnSc2	tarasnice
své	svůj	k3xOyFgMnPc4	svůj
asistenty	asistent	k1gMnPc4	asistent
<g/>
)	)	kIx)	)
Ladislav	Ladislav	k1gMnSc1	Ladislav
Smoljak	Smoljak	k1gMnSc1	Smoljak
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
na	na	k7c4	na
DVD	DVD	kA	DVD
edici	edice	k1gFnSc4	edice
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
společně	společně	k6eAd1	společně
se	s	k7c7	s
Zdenkem	Zdenkem	k?	Zdenkem
Svěrákem	svěrák	k1gInSc7	svěrák
měli	mít	k5eAaImAgMnP	mít
napsaný	napsaný	k2eAgInSc4d1	napsaný
scénář	scénář	k1gInSc4	scénář
k	k	k7c3	k
filmu	film	k1gInSc2	film
Deset	deset	k4xCc1	deset
zásad	zásada	k1gFnPc2	zásada
inspektora	inspektor	k1gMnSc2	inspektor
Trachty	Trachty	k?	Trachty
(	(	kIx(	(
<g/>
cimrmanovský	cimrmanovský	k2eAgInSc1d1	cimrmanovský
námět	námět	k1gInSc1	námět
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgInSc7	jenž
byly	být	k5eAaImAgInP	být
potíže	potíž	k1gFnSc2	potíž
(	(	kIx(	(
<g/>
bylo	být	k5eAaImAgNnS	být
jim	on	k3xPp3gMnPc3	on
řečeno	řečen	k2eAgNnSc1d1	řečeno
<g/>
,	,	kIx,	,
že	že	k8xS	že
Cimrman	Cimrman	k1gMnSc1	Cimrman
nikoho	nikdo	k3yNnSc4	nikdo
nezajímá	zajímat	k5eNaImIp3nS	zajímat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Chtěl	chtít	k5eAaImAgMnS	chtít
jej	on	k3xPp3gMnSc4	on
točit	točit	k5eAaImF	točit
Jiří	Jiří	k1gMnSc1	Jiří
Menzel	Menzel	k1gMnSc1	Menzel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ten	ten	k3xDgInSc1	ten
nebyl	být	k5eNaImAgInS	být
na	na	k7c6	na
Barrandově	Barrandov	k1gInSc6	Barrandov
příliš	příliš	k6eAd1	příliš
dobře	dobře	k6eAd1	dobře
zapsán	zapsat	k5eAaPmNgMnS	zapsat
a	a	k8xC	a
tak	tak	k9	tak
jej	on	k3xPp3gMnSc4	on
osobně	osobně	k6eAd1	osobně
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
Oldřichu	Oldřich	k1gMnSc3	Oldřich
Lipskému	lipský	k2eAgMnSc3d1	lipský
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
ani	ani	k8xC	ani
on	on	k3xPp3gMnSc1	on
jej	on	k3xPp3gInSc4	on
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
přes	přes	k7c4	přes
ústředního	ústřední	k2eAgMnSc4d1	ústřední
dramaturga	dramaturg	k1gMnSc4	dramaturg
Ludvíka	Ludvík	k1gMnSc4	Ludvík
Tomana	Toman	k1gMnSc4	Toman
prosadit	prosadit	k5eAaPmF	prosadit
(	(	kIx(	(
<g/>
Toman	Toman	k1gMnSc1	Toman
hrál	hrát	k5eAaImAgMnS	hrát
později	pozdě	k6eAd2	pozdě
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Nejistá	jistý	k2eNgFnSc1d1	nejistá
sezóna	sezóna	k1gFnSc1	sezóna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Režisér	režisér	k1gMnSc1	režisér
Lipský	lipský	k2eAgMnSc1d1	lipský
dostal	dostat	k5eAaPmAgInS	dostat
nápad	nápad	k1gInSc1	nápad
nechat	nechat	k5eAaPmF	nechat
scénář	scénář	k1gInSc4	scénář
stranou	stranou	k6eAd1	stranou
a	a	k8xC	a
přijít	přijít	k5eAaPmF	přijít
s	s	k7c7	s
novým	nový	k2eAgInSc7d1	nový
námětem	námět	k1gInSc7	námět
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
bude	být	k5eAaImBp3nS	být
konzultován	konzultován	k2eAgMnSc1d1	konzultován
s	s	k7c7	s
dramaturgem	dramaturg	k1gMnSc7	dramaturg
Tomanem	Toman	k1gMnSc7	Toman
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
cítil	cítit	k5eAaImAgMnS	cítit
zapojený	zapojený	k2eAgMnSc1d1	zapojený
do	do	k7c2	do
tvorby	tvorba	k1gFnSc2	tvorba
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
námětem	námět	k1gInSc7	námět
byl	být	k5eAaImAgInS	být
kondiciogram	kondiciogram	k1gInSc1	kondiciogram
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
byl	být	k5eAaImAgInS	být
tehdy	tehdy	k6eAd1	tehdy
v	v	k7c6	v
módě	móda	k1gFnSc6	móda
<g/>
.	.	kIx.	.
</s>
<s>
Kondiciogram	kondiciogram	k1gInSc1	kondiciogram
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
skutečně	skutečně	k6eAd1	skutečně
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
čím	co	k3yQnSc7	co
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
filmu	film	k1gInSc6	film
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
předpovědí	předpověď	k1gFnSc7	předpověď
dobrých	dobrý	k2eAgInPc2d1	dobrý
<g/>
,	,	kIx,	,
špatných	špatný	k2eAgInPc2d1	špatný
a	a	k8xC	a
neutrálních	neutrální	k2eAgInPc2d1	neutrální
dní	den	k1gInPc2	den
v	v	k7c6	v
životě	život	k1gInSc6	život
založenou	založený	k2eAgFnSc4d1	založená
na	na	k7c6	na
vědeckém	vědecký	k2eAgInSc6d1	vědecký
podkladě	podklad	k1gInSc6	podklad
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
toho	ten	k3xDgInSc2	ten
řídili	řídit	k5eAaImAgMnP	řídit
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
námět	námět	k1gInSc4	námět
dramaturg	dramaturg	k1gMnSc1	dramaturg
Toman	Toman	k1gMnSc1	Toman
přijal	přijmout	k5eAaPmAgMnS	přijmout
<g/>
,	,	kIx,	,
považoval	považovat	k5eAaImAgMnS	považovat
ho	on	k3xPp3gNnSc4	on
za	za	k7c4	za
vhodný	vhodný	k2eAgInSc4d1	vhodný
i	i	k8xC	i
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
boje	boj	k1gInSc2	boj
proti	proti	k7c3	proti
pověrám	pověra	k1gFnPc3	pověra
<g/>
.	.	kIx.	.
</s>
<s>
L.	L.	kA	L.
Smoljak	Smoljak	k1gMnSc1	Smoljak
podotýká	podotýkat	k5eAaImIp3nS	podotýkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
muselo	muset	k5eAaImAgNnS	muset
přijít	přijít	k5eAaPmF	přijít
s	s	k7c7	s
nějakou	nějaký	k3yIgFnSc7	nějaký
"	"	kIx"	"
<g/>
kouřovou	kouřový	k2eAgFnSc7d1	kouřová
clonou	clona	k1gFnSc7	clona
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vůbec	vůbec	k9	vůbec
film	film	k1gInSc1	film
mohl	moct	k5eAaImAgInS	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filmech	film	k1gInPc6	film
se	se	k3xPyFc4	se
nesměly	smět	k5eNaImAgInP	smět
objevovat	objevovat	k5eAaImF	objevovat
křesťanské	křesťanský	k2eAgInPc1d1	křesťanský
náboženské	náboženský	k2eAgInPc1d1	náboženský
symboly	symbol	k1gInPc1	symbol
(	(	kIx(	(
<g/>
kříže	kříž	k1gInPc1	kříž
<g/>
)	)	kIx)	)
ani	ani	k8xC	ani
církevní	církevní	k2eAgMnPc1d1	církevní
představitelé	představitel	k1gMnPc1	představitel
(	(	kIx(	(
<g/>
faráři	farář	k1gMnPc1	farář
např.	např.	kA	např.
během	během	k7c2	během
pohřbu	pohřeb	k1gInSc2	pohřeb
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
scenáristé	scenárista	k1gMnPc1	scenárista
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
počítali	počítat	k5eAaImAgMnP	počítat
s	s	k7c7	s
hlavní	hlavní	k2eAgInSc1d1	hlavní
roli	role	k1gFnSc4	role
pro	pro	k7c4	pro
tehdy	tehdy	k6eAd1	tehdy
málo	málo	k6eAd1	málo
známého	známý	k2eAgMnSc4d1	známý
herce	herec	k1gMnSc4	herec
Luďka	Luděk	k1gMnSc4	Luděk
Sobotu	Sobota	k1gMnSc4	Sobota
(	(	kIx(	(
<g/>
znal	znát	k5eAaImAgMnS	znát
jej	on	k3xPp3gInSc4	on
pouze	pouze	k6eAd1	pouze
omezený	omezený	k2eAgInSc4d1	omezený
okruh	okruh	k1gInSc4	okruh
lidí	člověk	k1gMnPc2	člověk
z	z	k7c2	z
prostředí	prostředí	k1gNnSc2	prostředí
libereckého	liberecký	k2eAgNnSc2d1	liberecké
divadla	divadlo	k1gNnSc2	divadlo
Ypsilon	ypsilon	k1gNnSc2	ypsilon
<g/>
)	)	kIx)	)
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
psali	psát	k5eAaImAgMnP	psát
dialogy	dialog	k1gInPc4	dialog
přímo	přímo	k6eAd1	přímo
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gInSc2	jeho
charakteru	charakter	k1gInSc2	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Zaujal	zaujmout	k5eAaPmAgMnS	zaujmout
je	být	k5eAaImIp3nS	být
svým	svůj	k3xOyFgInSc7	svůj
originálním	originální	k2eAgMnSc7d1	originální
a	a	k8xC	a
neotřelým	otřelý	k2eNgInSc7d1	neotřelý
projevem	projev	k1gInSc7	projev
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
jej	on	k3xPp3gMnSc4	on
doporučili	doporučit	k5eAaPmAgMnP	doporučit
režisérovi	režisér	k1gMnSc3	režisér
Oldřichu	Oldřich	k1gMnSc3	Oldřich
Lipskému	lipský	k2eAgMnSc3d1	lipský
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
Sobotou	sobota	k1gFnSc7	sobota
natočil	natočit	k5eAaBmAgMnS	natočit
kamerové	kamerový	k2eAgFnPc4d1	kamerová
zkoušky	zkouška	k1gFnPc4	zkouška
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgFnPc6	jenž
liberecký	liberecký	k2eAgMnSc1d1	liberecký
herec	herec	k1gMnSc1	herec
uspěl	uspět	k5eAaPmAgMnS	uspět
a	a	k8xC	a
Jáchymem	Jáchym	k1gMnSc7	Jáchym
odstartoval	odstartovat	k5eAaPmAgInS	odstartovat
svou	svůj	k3xOyFgFnSc4	svůj
kariéru	kariéra	k1gFnSc4	kariéra
známého	známý	k2eAgMnSc2d1	známý
herce	herec	k1gMnSc2	herec
<g/>
.	.	kIx.	.
</s>
<s>
Ladislav	Ladislav	k1gMnSc1	Ladislav
Smoljak	Smoljak	k1gMnSc1	Smoljak
se	se	k3xPyFc4	se
domnívá	domnívat	k5eAaImIp3nS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejzajímavější	zajímavý	k2eAgFnSc7d3	nejzajímavější
scénou	scéna	k1gFnSc7	scéna
filmu	film	k1gInSc2	film
je	být	k5eAaImIp3nS	být
ta	ten	k3xDgFnSc1	ten
s	s	k7c7	s
protiúplatkovým	protiúplatkový	k2eAgInSc7d1	protiúplatkový
kursem	kurs	k1gInSc7	kurs
<g/>
.	.	kIx.	.
</s>
<s>
Běžně	běžně	k6eAd1	běžně
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
poté	poté	k6eAd1	poté
v	v	k7c6	v
životě	život	k1gInSc6	život
stávalo	stávat	k5eAaImAgNnS	stávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
při	při	k7c6	při
vyřizování	vyřizování	k1gNnSc6	vyřizování
některých	některý	k3yIgFnPc2	některý
záležitostí	záležitost	k1gFnPc2	záležitost
oslovován	oslovován	k2eAgInSc4d1	oslovován
lidmi	člověk	k1gMnPc7	člověk
populární	populární	k2eAgInSc1d1	populární
hláškou	hláška	k1gFnSc7	hláška
"	"	kIx"	"
<g/>
Neber	brát	k5eNaImRp2nS	brát
úplatky	úplatek	k1gInPc7	úplatek
<g/>
,	,	kIx,	,
neber	brát	k5eNaImRp2nS	brát
úplatky	úplatek	k1gInPc7	úplatek
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
zblázníš	zbláznit	k5eAaPmIp2nS	zbláznit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Úsloví	úsloví	k1gNnSc1	úsloví
"	"	kIx"	"
<g/>
kolik	kolik	k4yQc1	kolik
třešní	třešeň	k1gFnPc2	třešeň
<g/>
,	,	kIx,	,
tolik	tolik	k4yIc4	tolik
višní	višeň	k1gFnPc2	višeň
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
dalším	další	k2eAgInSc7d1	další
případem	případ	k1gInSc7	případ
zpopularizované	zpopularizovaný	k2eAgFnSc2d1	zpopularizovaná
fráze	fráze	k1gFnSc2	fráze
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
"	"	kIx"	"
<g/>
překladatelský	překladatelský	k2eAgInSc1d1	překladatelský
oříšek	oříšek	k1gInSc1	oříšek
<g/>
"	"	kIx"	"
zavání	zavánět	k5eAaImIp3nS	zavánět
absurdním	absurdní	k2eAgInSc7d1	absurdní
humorem	humor	k1gInSc7	humor
<g/>
.	.	kIx.	.
</s>
<s>
Snímek	snímek	k1gInSc1	snímek
Jáchyme	Jáchym	k1gMnSc5	Jáchym
<g/>
,	,	kIx,	,
hoď	hodit	k5eAaPmRp2nS	hodit
ho	on	k3xPp3gMnSc4	on
do	do	k7c2	do
stroje	stroj	k1gInSc2	stroj
<g/>
!	!	kIx.	!
</s>
<s>
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Svěráka	Svěrák	k1gMnSc2	Svěrák
hodně	hodně	k6eAd1	hodně
prošpikován	prošpikovat	k5eAaPmNgInS	prošpikovat
humorem	humor	k1gInSc7	humor
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
začínající	začínající	k2eAgMnPc1d1	začínající
filmoví	filmový	k2eAgMnPc1d1	filmový
scenáristé	scenárista	k1gMnPc1	scenárista
Smoljak	Smoljak	k1gMnSc1	Smoljak
<g/>
/	/	kIx~	/
<g/>
Svěrák	Svěrák	k1gMnSc1	Svěrák
obávali	obávat	k5eAaImAgMnP	obávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
stále	stále	k6eAd1	stále
není	být	k5eNaImIp3nS	být
dostatečně	dostatečně	k6eAd1	dostatečně
vtipný	vtipný	k2eAgInSc1d1	vtipný
a	a	k8xC	a
snažili	snažit	k5eAaImAgMnP	snažit
se	se	k3xPyFc4	se
do	do	k7c2	do
něj	on	k3xPp3gMnSc2	on
vpravit	vpravit	k5eAaPmF	vpravit
co	co	k9	co
nejvíce	hodně	k6eAd3	hodně
humorných	humorný	k2eAgFnPc2d1	humorná
scén	scéna	k1gFnPc2	scéna
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
odstupem	odstup	k1gInSc7	odstup
času	čas	k1gInSc2	čas
Zdeněk	Zdeňka	k1gFnPc2	Zdeňka
Svěrák	Svěrák	k1gMnSc1	Svěrák
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
porce	porce	k1gFnSc1	porce
humoru	humor	k1gInSc2	humor
dala	dát	k5eAaPmAgFnS	dát
rozdělit	rozdělit	k5eAaPmF	rozdělit
do	do	k7c2	do
dvou	dva	k4xCgInPc2	dva
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Vzpomíná	vzpomínat	k5eAaImIp3nS	vzpomínat
na	na	k7c4	na
příhodu	příhoda	k1gFnSc4	příhoda
s	s	k7c7	s
hercem	herec	k1gMnSc7	herec
Václavem	Václav	k1gMnSc7	Václav
Lohniským	Lohniský	k1gMnSc7	Lohniský
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
ve	v	k7c6	v
scénáři	scénář	k1gInSc6	scénář
text	text	k1gInSc1	text
"	"	kIx"	"
<g/>
sanitka	sanitka	k1gFnSc1	sanitka
se	se	k3xPyFc4	se
řítí	řítit	k5eAaImIp3nS	řítit
ulicemi	ulice	k1gFnPc7	ulice
a	a	k8xC	a
dravčí	dravčí	k2eAgFnPc4d1	dravčí
oči	oko	k1gNnPc4	oko
docenta	docent	k1gMnSc2	docent
Chocholouška	chocholoušek	k1gMnSc2	chocholoušek
pátrají	pátrat	k5eAaImIp3nP	pátrat
přes	přes	k7c4	přes
sklo	sklo	k1gNnSc4	sklo
<g/>
"	"	kIx"	"
a	a	k8xC	a
po	po	k7c6	po
natočení	natočení	k1gNnSc6	natočení
scény	scéna	k1gFnSc2	scéna
se	se	k3xPyFc4	se
ptal	ptat	k5eAaImAgMnS	ptat
<g/>
,	,	kIx,	,
zdali	zdali	k8xS	zdali
měl	mít	k5eAaImAgMnS	mít
dostatečně	dostatečně	k6eAd1	dostatečně
"	"	kIx"	"
<g/>
dravčí	dravčí	k2eAgNnPc4d1	dravčí
<g/>
"	"	kIx"	"
oči	oko	k1gNnPc4	oko
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
měl	mít	k5eAaImAgInS	mít
obrovský	obrovský	k2eAgInSc1d1	obrovský
divácký	divácký	k2eAgInSc1d1	divácký
úspěch	úspěch	k1gInSc1	úspěch
i	i	k9	i
díky	díky	k7c3	díky
hereckému	herecký	k2eAgInSc3d1	herecký
objevu	objev	k1gInSc3	objev
Luďka	Luděk	k1gMnSc2	Luděk
Soboty	Sobota	k1gMnSc2	Sobota
(	(	kIx(	(
<g/>
jeho	jeho	k3xOp3gNnSc2	jeho
samotného	samotný	k2eAgNnSc2d1	samotné
takový	takový	k3xDgInSc1	takový
úspěch	úspěch	k1gInSc4	úspěch
překvapil	překvapit	k5eAaPmAgInS	překvapit
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jednu	jeden	k4xCgFnSc4	jeden
dobu	doba	k1gFnSc4	doba
se	se	k3xPyFc4	se
vysílal	vysílat	k5eAaImAgInS	vysílat
v	v	k7c6	v
upravené	upravený	k2eAgFnSc6d1	upravená
verzi	verze	k1gFnSc6	verze
s	s	k7c7	s
vystřihlou	vystřihlý	k2eAgFnSc7d1	vystřihlý
scénou	scéna	k1gFnSc7	scéna
s	s	k7c7	s
protiúplatkovým	protiúplatkový	k2eAgInSc7d1	protiúplatkový
kursem	kurs	k1gInSc7	kurs
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
byla	být	k5eAaImAgFnS	být
"	"	kIx"	"
<g/>
nemilost	nemilost	k1gFnSc4	nemilost
<g/>
"	"	kIx"	"
herce	herec	k1gMnSc2	herec
Jana	Jan	k1gMnSc2	Jan
Valy	Vala	k1gMnSc2	Vala
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
nesměl	smět	k5eNaImAgMnS	smět
objevit	objevit	k5eAaPmF	objevit
na	na	k7c6	na
televizních	televizní	k2eAgFnPc6d1	televizní
obrazovkách	obrazovka	k1gFnPc6	obrazovka
<g/>
.	.	kIx.	.
</s>
<s>
Luděk	Luděk	k1gMnSc1	Luděk
Sobota	Sobota	k1gMnSc1	Sobota
přiznává	přiznávat	k5eAaImIp3nS	přiznávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
nesmělý	smělý	k2eNgMnSc1d1	nesmělý
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
postava	postava	k1gFnSc1	postava
Františka	František	k1gMnSc2	František
Koudelky	Koudelka	k1gMnSc2	Koudelka
<g/>
.	.	kIx.	.
</s>
<s>
Uznává	uznávat	k5eAaImIp3nS	uznávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgInS	být
scénář	scénář	k1gInSc1	scénář
připraven	připravit	k5eAaPmNgInS	připravit
"	"	kIx"	"
<g/>
na	na	k7c6	na
míru	mír	k1gInSc6	mír
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k8xC	ale
našel	najít	k5eAaPmAgMnS	najít
si	se	k3xPyFc3	se
i	i	k9	i
prostor	prostor	k1gInSc4	prostor
k	k	k7c3	k
improvizaci	improvizace	k1gFnSc3	improvizace
<g/>
,	,	kIx,	,
např.	např.	kA	např.
ve	v	k7c6	v
scéně	scéna	k1gFnSc6	scéna
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
František	František	k1gMnSc1	František
povalí	povalit	k5eAaPmIp3nS	povalit
trenéra	trenér	k1gMnSc4	trenér
juda	judo	k1gNnSc2	judo
(	(	kIx(	(
<g/>
František	František	k1gMnSc1	František
Peterka	Peterka	k1gMnSc1	Peterka
<g/>
)	)	kIx)	)
na	na	k7c4	na
žíněnku	žíněnka	k1gFnSc4	žíněnka
a	a	k8xC	a
pak	pak	k6eAd1	pak
mu	on	k3xPp3gMnSc3	on
podává	podávat	k5eAaImIp3nS	podávat
ruku	ruka	k1gFnSc4	ruka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gMnSc3	on
pomohl	pomoct	k5eAaPmAgMnS	pomoct
vstát	vstát	k5eAaPmF	vstát
<g/>
.	.	kIx.	.
</s>
<s>
Scéna	scéna	k1gFnSc1	scéna
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
František	František	k1gMnSc1	František
Koudelka	Koudelka	k1gMnSc1	Koudelka
říká	říkat	k5eAaImIp3nS	říkat
legendární	legendární	k2eAgFnSc4d1	legendární
větu	věta	k1gFnSc4	věta
"	"	kIx"	"
<g/>
tak	tak	k6eAd1	tak
kdepak	kdepak	k9	kdepak
je	být	k5eAaImIp3nS	být
ten	ten	k3xDgMnSc1	ten
prďola	prďola	k1gMnSc1	prďola
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
tu	tu	k6eAd1	tu
točí	točit	k5eAaImIp3nS	točit
to	ten	k3xDgNnSc1	ten
pivo	pivo	k1gNnSc1	pivo
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
natáčela	natáčet	k5eAaImAgFnS	natáčet
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
v	v	k7c6	v
bufetu	bufet	k1gInSc6	bufet
Koruna	koruna	k1gFnSc1	koruna
na	na	k7c6	na
Václavském	václavský	k2eAgNnSc6d1	Václavské
náměstí	náměstí	k1gNnSc6	náměstí
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
