<s>
Ivan	Ivan	k1gMnSc1	Ivan
je	být	k5eAaImIp3nS	být
mužské	mužský	k2eAgNnSc4d1	mužské
křestní	křestní	k2eAgNnSc4d1	křestní
jméno	jméno	k1gNnSc4	jméno
hebrejského	hebrejský	k2eAgInSc2d1	hebrejský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
českého	český	k2eAgInSc2d1	český
kalendáře	kalendář	k1gInSc2	kalendář
má	mít	k5eAaImIp3nS	mít
svátek	svátek	k1gInSc1	svátek
25	[number]	k4	25
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
.	.	kIx.	.
</s>
<s>
Ivan	Ivan	k1gMnSc1	Ivan
je	být	k5eAaImIp3nS	být
ruskou	ruský	k2eAgFnSc7d1	ruská
variantou	varianta	k1gFnSc7	varianta
jména	jméno	k1gNnSc2	jméno
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Jména	jméno	k1gNnSc2	jméno
Jan	Jan	k1gMnSc1	Jan
i	i	k8xC	i
Ivan	Ivan	k1gMnSc1	Ivan
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
hebrejského	hebrejský	k2eAgInSc2d1	hebrejský
Jochánán	Jochánán	k2eAgInSc4d1	Jochánán
(	(	kIx(	(
<g/>
י	י	k?	י
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
Hospodin	Hospodin	k1gMnSc1	Hospodin
je	být	k5eAaImIp3nS	být
milostivý	milostivý	k2eAgMnSc1d1	milostivý
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
jméno	jméno	k1gNnSc1	jméno
se	se	k3xPyFc4	se
v	v	k7c6	v
době	doba	k1gFnSc6	doba
raného	raný	k2eAgNnSc2d1	rané
křesťanství	křesťanství	k1gNnSc2	křesťanství
šířilo	šířit	k5eAaImAgNnS	šířit
z	z	k7c2	z
latiny	latina	k1gFnSc2	latina
(	(	kIx(	(
<g/>
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
abeceda	abeceda	k1gFnSc1	abeceda
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
o	o	k7c6	o
J	J	kA	J
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
Joannés	Joannés	k1gInSc4	Joannés
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
se	se	k3xPyFc4	se
v	v	k7c6	v
poněmčené	poněmčený	k2eAgFnSc6d1	poněmčená
podobě	podoba	k1gFnSc6	podoba
Johan	Johana	k1gFnPc2	Johana
dostalo	dostat	k5eAaPmAgNnS	dostat
k	k	k7c3	k
západním	západní	k2eAgMnPc3d1	západní
slovanům	slovan	k1gMnPc3	slovan
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
východním	východní	k2eAgMnPc3d1	východní
slovanům	slovan	k1gMnPc3	slovan
se	se	k3xPyFc4	se
však	však	k9	však
toto	tento	k3xDgNnSc1	tento
jméno	jméno	k1gNnSc1	jméno
propracovalo	propracovat	k5eAaPmAgNnS	propracovat
<g/>
,	,	kIx,	,
s	s	k7c7	s
šířením	šíření	k1gNnSc7	šíření
pravoslaví	pravoslaví	k1gNnSc2	pravoslaví
<g/>
,	,	kIx,	,
z	z	k7c2	z
řecké	řecký	k2eAgFnSc2d1	řecká
varianty	varianta	k1gFnSc2	varianta
Ioannés	Ioannésa	k1gFnPc2	Ioannésa
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Ivan	Ivan	k1gMnSc1	Ivan
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
k	k	k7c3	k
nám	my	k3xPp1nPc3	my
později	pozdě	k6eAd2	pozdě
dorazil	dorazit	k5eAaPmAgMnS	dorazit
z	z	k7c2	z
východu	východ	k1gInSc2	východ
jako	jako	k8xS	jako
samostatné	samostatný	k2eAgNnSc4d1	samostatné
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Ivánek	Ivánek	k1gMnSc1	Ivánek
<g/>
,	,	kIx,	,
Váňa	Váňa	k1gMnSc1	Váňa
<g/>
,	,	kIx,	,
Váňuška	Váňuška	k1gFnSc1	Váňuška
Následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
uvádí	uvádět	k5eAaImIp3nS	uvádět
četnost	četnost	k1gFnSc4	četnost
jména	jméno	k1gNnSc2	jméno
v	v	k7c6	v
ČR	ČR	kA	ČR
a	a	k8xC	a
pmezi	pmeh	k1gMnPc1	pmeh
mužskými	mužský	k2eAgNnPc7d1	mužské
jmény	jméno	k1gNnPc7	jméno
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
dvou	dva	k4xCgInPc2	dva
roků	rok	k1gInPc2	rok
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yIgInPc4	který
jsou	být	k5eAaImIp3nP	být
dostupné	dostupný	k2eAgInPc1d1	dostupný
údaje	údaj	k1gInPc1	údaj
MV	MV	kA	MV
ČR	ČR	kA	ČR
-	-	kIx~	-
lze	lze	k6eAd1	lze
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
tedy	tedy	k9	tedy
vysledovat	vysledovat	k5eAaPmF	vysledovat
trend	trend	k1gInSc4	trend
v	v	k7c6	v
užívání	užívání	k1gNnSc6	užívání
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
<g/>
:	:	kIx,	:
Změna	změna	k1gFnSc1	změna
procentního	procentní	k2eAgNnSc2d1	procentní
zastoupení	zastoupení	k1gNnSc2	zastoupení
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
mezi	mezi	k7c7	mezi
žijícími	žijící	k2eAgMnPc7d1	žijící
muži	muž	k1gMnPc7	muž
v	v	k7c6	v
ČR	ČR	kA	ČR
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
procentní	procentní	k2eAgFnSc1d1	procentní
změna	změna	k1gFnSc1	změna
se	se	k3xPyFc4	se
započítáním	započítání	k1gNnSc7	započítání
celkového	celkový	k2eAgInSc2d1	celkový
úbytku	úbytek	k1gInSc2	úbytek
mužů	muž	k1gMnPc2	muž
v	v	k7c6	v
ČR	ČR	kA	ČR
za	za	k7c4	za
sledované	sledovaný	k2eAgInPc4d1	sledovaný
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
1999	[number]	k4	1999
<g/>
-	-	kIx~	-
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
-0,2	-0,2	k4	-0,2
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Slovensky	slovensky	k6eAd1	slovensky
<g/>
,	,	kIx,	,
rusky	rusky	k6eAd1	rusky
<g/>
,	,	kIx,	,
chorvatsky	chorvatsky	k6eAd1	chorvatsky
<g/>
,	,	kIx,	,
maďarsky	maďarsky	k6eAd1	maďarsky
<g/>
,	,	kIx,	,
bulharsky	bulharsky	k6eAd1	bulharsky
<g/>
,	,	kIx,	,
italsky	italsky	k6eAd1	italsky
<g/>
,	,	kIx,	,
dánsky	dánsky	k6eAd1	dánsky
<g/>
:	:	kIx,	:
Ivan	Ivan	k1gMnSc1	Ivan
Polsky	Polska	k1gFnSc2	Polska
<g/>
,	,	kIx,	,
holandsky	holandsky	k6eAd1	holandsky
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
<g/>
:	:	kIx,	:
Iwan	Iwan	k1gNnSc4	Iwan
Španělsky	španělsky	k6eAd1	španělsky
<g/>
:	:	kIx,	:
Iván	Iván	k1gNnSc4	Iván
Francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
:	:	kIx,	:
Ivain	Ivain	k1gInSc1	Ivain
nebo	nebo	k8xC	nebo
Yve	Yve	k1gFnSc1	Yve
Srbsky	Srbsko	k1gNnPc7	Srbsko
<g/>
:	:	kIx,	:
Jovan	Jovan	k1gInSc1	Jovan
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
kalendáři	kalendář	k1gInSc6	kalendář
<g/>
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
25	[number]	k4	25
<g/>
.	.	kIx.	.
červen	červen	k1gInSc1	červen
v	v	k7c6	v
slovenském	slovenský	k2eAgInSc6d1	slovenský
kalendáři	kalendář	k1gInSc6	kalendář
<g/>
:	:	kIx,	:
8	[number]	k4	8
<g/>
.	.	kIx.	.
červenec	červenec	k1gInSc1	červenec
Ivan	Ivan	k1gMnSc1	Ivan
Veliký	veliký	k2eAgMnSc1d1	veliký
-	-	kIx~	-
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1462	[number]	k4	1462
velikým	veliký	k2eAgNnSc7d1	veliké
knížetem	kníže	k1gNnSc7wR	kníže
moskevským	moskevský	k2eAgNnSc7d1	moskevské
Ivan	Ivan	k1gMnSc1	Ivan
IV	IV	kA	IV
<g/>
.	.	kIx.	.
-	-	kIx~	-
ruský	ruský	k2eAgMnSc1d1	ruský
car	car	k1gMnSc1	car
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
spíše	spíše	k9	spíše
pod	pod	k7c7	pod
přívlastkem	přívlastek	k1gInSc7	přívlastek
"	"	kIx"	"
<g/>
Hrozný	hrozný	k2eAgMnSc1d1	hrozný
<g/>
"	"	kIx"	"
svatý	svatý	k2eAgMnSc1d1	svatý
Ivan	Ivan	k1gMnSc1	Ivan
-	-	kIx~	-
legendární	legendární	k2eAgMnSc1d1	legendární
český	český	k2eAgMnSc1d1	český
poustevník	poustevník	k1gMnSc1	poustevník
svatý	svatý	k1gMnSc1	svatý
Ivan	Ivan	k1gMnSc1	Ivan
Rilský	rilský	k2eAgMnSc1d1	rilský
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
bulharský	bulharský	k2eAgMnSc1d1	bulharský
poustevník	poustevník	k1gMnSc1	poustevník
Ivan	Ivan	k1gMnSc1	Ivan
Christoforovič	Christoforovič	k1gMnSc1	Christoforovič
Bagramjan	Bagramjan	k1gMnSc1	Bagramjan
-	-	kIx~	-
sovětský	sovětský	k2eAgMnSc1d1	sovětský
maršál	maršál	k1gMnSc1	maršál
Ivan	Ivan	k1gMnSc1	Ivan
Basso	Bassa	k1gFnSc5	Bassa
-	-	kIx~	-
italský	italský	k2eAgMnSc1d1	italský
cyklista	cyklista	k1gMnSc1	cyklista
Ivan	Ivan	k1gMnSc1	Ivan
Blatný	blatný	k2eAgMnSc1d1	blatný
-	-	kIx~	-
český	český	k2eAgMnSc1d1	český
básník	básník	k1gMnSc1	básník
Ivan	Ivan	k1gMnSc1	Ivan
Cankar	Cankar	k1gMnSc1	Cankar
-	-	kIx~	-
slovinský	slovinský	k2eAgMnSc1d1	slovinský
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
dramatik	dramatik	k1gMnSc1	dramatik
a	a	k8xC	a
básník	básník	k1gMnSc1	básník
Ivan	Ivan	k1gMnSc1	Ivan
Dejmal	Dejmal	k1gMnSc1	Dejmal
-	-	kIx~	-
český	český	k2eAgMnSc1d1	český
ekolog	ekolog	k1gMnSc1	ekolog
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
Ivan	Ivan	k1gMnSc1	Ivan
Diviš	Diviš	k1gMnSc1	Diviš
-	-	kIx~	-
český	český	k2eAgMnSc1d1	český
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
esejista	esejista	k1gMnSc1	esejista
Jovan	Jovan	k1gMnSc1	Jovan
Dučić	Dučić	k1gMnSc1	Dučić
-	-	kIx~	-
srbský	srbský	k2eAgMnSc1d1	srbský
básník	básník	k1gMnSc1	básník
Ivan	Ivan	k1gMnSc1	Ivan
Dylevský	Dylevský	k2eAgMnSc1d1	Dylevský
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
český	český	k2eAgMnSc1d1	český
anatom	anatom	k1gMnSc1	anatom
Ivan	Ivan	k1gMnSc1	Ivan
Alexandrovič	Alexandrovič	k1gMnSc1	Alexandrovič
Gončarov	Gončarov	k1gInSc1	Gončarov
-	-	kIx~	-
ruský	ruský	k2eAgMnSc1d1	ruský
spisovatel	spisovatel	k1gMnSc1	spisovatel
Iván	Iván	k1gMnSc1	Iván
Gutiérrez	Gutiérrez	k1gMnSc1	Gutiérrez
-	-	kIx~	-
kolumbijský	kolumbijský	k2eAgMnSc1d1	kolumbijský
písničkář	písničkář	k1gMnSc1	písničkář
Ivan	Ivan	k1gMnSc1	Ivan
Hálek	hálka	k1gFnPc2	hálka
-	-	kIx~	-
český	český	k2eAgMnSc1d1	český
lékař	lékař	k1gMnSc1	lékař
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
Ivan	Ivan	k1gMnSc1	Ivan
Hašek	Hašek	k1gMnSc1	Hašek
-	-	kIx~	-
český	český	k2eAgMnSc1d1	český
fotbalista	fotbalista	k1gMnSc1	fotbalista
Ivan	Ivan	k1gMnSc1	Ivan
Havel	Havel	k1gMnSc1	Havel
-	-	kIx~	-
český	český	k2eAgMnSc1d1	český
vědec	vědec	k1gMnSc1	vědec
Ivan	Ivan	k1gMnSc1	Ivan
Hlas	hlas	k1gInSc1	hlas
-	-	kIx~	-
český	český	k2eAgMnSc1d1	český
zpěvák	zpěvák	k1gMnSc1	zpěvák
a	a	k8xC	a
skladatel	skladatel	k1gMnSc1	skladatel
Ivan	Ivan	k1gMnSc1	Ivan
Hrbek	Hrbek	k1gMnSc1	Hrbek
-	-	kIx~	-
český	český	k2eAgMnSc1d1	český
orientalista	orientalista	k1gMnSc1	orientalista
Ivan	Ivan	k1gMnSc1	Ivan
Hoffman	Hoffman	k1gMnSc1	Hoffman
-	-	kIx~	-
slovenský	slovenský	k2eAgMnSc1d1	slovenský
písničkář	písničkář	k1gMnSc1	písničkář
a	a	k8xC	a
český	český	k2eAgMnSc1d1	český
<g />
.	.	kIx.	.
</s>
<s>
novinář	novinář	k1gMnSc1	novinář
Ivan	Ivan	k1gMnSc1	Ivan
Ilić	Ilić	k1gMnSc1	Ilić
-	-	kIx~	-
americký	americký	k2eAgMnSc1d1	americký
klavírní	klavírní	k2eAgMnSc1d1	klavírní
virtuóz	virtuóza	k1gFnPc2	virtuóza
Ivan	Ivan	k1gMnSc1	Ivan
Jandl	Jandl	k1gMnSc1	Jandl
-	-	kIx~	-
český	český	k2eAgMnSc1d1	český
dětský	dětský	k2eAgMnSc1d1	dětský
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
ocenění	ocenění	k1gNnSc2	ocenění
Oscar	Oscar	k1gMnSc1	Oscar
Ivan	Ivan	k1gMnSc1	Ivan
Martin	Martin	k1gMnSc1	Martin
Jirous	Jirous	k1gMnSc1	Jirous
-	-	kIx~	-
český	český	k2eAgMnSc1d1	český
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
publicista	publicista	k1gMnSc1	publicista
Ivan	Ivan	k1gMnSc1	Ivan
Kaljajev	Kaljajev	k1gMnSc1	Kaljajev
-	-	kIx~	-
ruský	ruský	k2eAgMnSc1d1	ruský
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
revolucionář	revolucionář	k1gMnSc1	revolucionář
Ivan	Ivan	k1gMnSc1	Ivan
Klíma	Klíma	k1gMnSc1	Klíma
-	-	kIx~	-
český	český	k2eAgMnSc1d1	český
spisovatel	spisovatel	k1gMnSc1	spisovatel
Ivan	Ivan	k1gMnSc1	Ivan
Krajíček	Krajíček	k1gMnSc1	Krajíček
-	-	kIx~	-
slovenský	slovenský	k2eAgMnSc1d1	slovenský
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
komik	komik	k1gMnSc1	komik
a	a	k8xC	a
moderátor	moderátor	k1gMnSc1	moderátor
Ivan	Ivan	k1gMnSc1	Ivan
Král	Král	k1gMnSc1	Král
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
český	český	k2eAgMnSc1d1	český
kytarista	kytarista	k1gMnSc1	kytarista
a	a	k8xC	a
baskytarista	baskytarista	k1gMnSc1	baskytarista
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
skupiny	skupina	k1gFnSc2	skupina
The	The	k1gMnSc1	The
Patti	Patť	k1gFnSc2	Patť
Smith	Smith	k1gMnSc1	Smith
Group	Group	k1gMnSc1	Group
Ivan	Ivan	k1gMnSc1	Ivan
Stěpanovič	Stěpanovič	k1gMnSc1	Stěpanovič
Koněv	Koněv	k1gMnSc1	Koněv
-	-	kIx~	-
ruský	ruský	k2eAgMnSc1d1	ruský
voják	voják	k1gMnSc1	voják
<g/>
,	,	kIx,	,
maršál	maršál	k1gMnSc1	maršál
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
Ivan	Ivan	k1gMnSc1	Ivan
Lendl	Lendl	k1gMnSc1	Lendl
-	-	kIx~	-
česko-americký	českomerický	k2eAgMnSc1d1	česko-americký
tenista	tenista	k1gMnSc1	tenista
Ivan	Ivan	k1gMnSc1	Ivan
Mauger	Mauger	k1gMnSc1	Mauger
-	-	kIx~	-
novozélandský	novozélandský	k2eAgMnSc1d1	novozélandský
motocyklový	motocyklový	k2eAgMnSc1d1	motocyklový
závodník	závodník	k1gMnSc1	závodník
Ivan	Ivan	k1gMnSc1	Ivan
Medek	Medek	k1gMnSc1	Medek
-	-	kIx~	-
český	český	k2eAgMnSc1d1	český
novinář	novinář	k1gMnSc1	novinář
Ivan	Ivan	k1gMnSc1	Ivan
Mládek	Mládek	k1gMnSc1	Mládek
-	-	kIx~	-
český	český	k2eAgMnSc1d1	český
hudebník	hudebník	k1gMnSc1	hudebník
a	a	k8xC	a
bavič	bavič	k1gMnSc1	bavič
Ivan	Ivan	k1gMnSc1	Ivan
Olbracht	Olbracht	k1gMnSc1	Olbracht
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
český	český	k2eAgMnSc1d1	český
spisovatel	spisovatel	k1gMnSc1	spisovatel
Ivan	Ivan	k1gMnSc1	Ivan
Petrovič	Petrovič	k1gMnSc1	Petrovič
Pavlov	Pavlov	k1gInSc1	Pavlov
-	-	kIx~	-
ruský	ruský	k2eAgMnSc1d1	ruský
vědec	vědec	k1gMnSc1	vědec
Iván	Iván	k1gMnSc1	Iván
Pedroso	Pedrosa	k1gFnSc5	Pedrosa
-	-	kIx~	-
kubánský	kubánský	k2eAgMnSc1d1	kubánský
atlet	atlet	k1gMnSc1	atlet
Ivan	Ivan	k1gMnSc1	Ivan
Poldauf	Poldauf	k1gMnSc1	Poldauf
-	-	kIx~	-
český	český	k2eAgMnSc1d1	český
jazykovědec	jazykovědec	k1gMnSc1	jazykovědec
Ivan	Ivan	k1gMnSc1	Ivan
Rajmont	Rajmont	k1gMnSc1	Rajmont
-	-	kIx~	-
český	český	k2eAgMnSc1d1	český
divadelník	divadelník	k1gMnSc1	divadelník
a	a	k8xC	a
režisér	režisér	k1gMnSc1	režisér
Ivan	Ivan	k1gMnSc1	Ivan
Reitman	Reitman	k1gMnSc1	Reitman
-	-	kIx~	-
americký	americký	k2eAgMnSc1d1	americký
režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
producent	producent	k1gMnSc1	producent
a	a	k8xC	a
scenárista	scenárista	k1gMnSc1	scenárista
Ivan	Ivan	k1gMnSc1	Ivan
Roubal	Roubal	k1gMnSc1	Roubal
-	-	kIx~	-
český	český	k2eAgMnSc1d1	český
sériový	sériový	k2eAgMnSc1d1	sériový
vrah	vrah	k1gMnSc1	vrah
Ivan	Ivan	k1gMnSc1	Ivan
Skála	Skála	k1gMnSc1	Skála
-	-	kIx~	-
český	český	k2eAgMnSc1d1	český
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
Ivan	Ivan	k1gMnSc1	Ivan
Sviták	Sviták	k1gMnSc1	Sviták
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
český	český	k2eAgMnSc1d1	český
filosof	filosof	k1gMnSc1	filosof
<g/>
,	,	kIx,	,
kritik	kritik	k1gMnSc1	kritik
a	a	k8xC	a
básník	básník	k1gMnSc1	básník
Ivan	Ivan	k1gMnSc1	Ivan
Štampach	Štampach	k1gMnSc1	Štampach
-	-	kIx~	-
český	český	k2eAgMnSc1d1	český
religionista	religionista	k1gMnSc1	religionista
<g/>
,	,	kIx,	,
teolog	teolog	k1gMnSc1	teolog
a	a	k8xC	a
filosof	filosof	k1gMnSc1	filosof
Ivan	Ivan	k1gMnSc1	Ivan
Štraus	Štraus	k1gMnSc1	Štraus
-	-	kIx~	-
dvě	dva	k4xCgFnPc1	dva
osoby	osoba	k1gFnPc1	osoba
téhož	týž	k3xTgNnSc2	týž
jména	jméno	k1gNnSc2	jméno
a	a	k8xC	a
příjmení	příjmení	k1gNnSc2	příjmení
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
houslista	houslista	k1gMnSc1	houslista
a	a	k8xC	a
bosenskohercegovinský	bosenskohercegovinský	k2eAgMnSc1d1	bosenskohercegovinský
architekt	architekt	k1gMnSc1	architekt
Ivan	Ivan	k1gMnSc1	Ivan
Uchov	Uchov	k1gInSc1	Uchov
-	-	kIx~	-
ruský	ruský	k2eAgMnSc1d1	ruský
atlet	atlet	k1gMnSc1	atlet
Ivan	Ivan	k1gMnSc1	Ivan
Vyskočil	Vyskočil	k1gMnSc1	Vyskočil
-	-	kIx~	-
český	český	k2eAgMnSc1d1	český
dramatik	dramatik	k1gMnSc1	dramatik
a	a	k8xC	a
herec	herec	k1gMnSc1	herec
Ivan	Ivan	k1gMnSc1	Ivan
Vojnár	Vojnár	k1gMnSc1	Vojnár
-	-	kIx~	-
slovenský	slovenský	k2eAgMnSc1d1	slovenský
<g />
.	.	kIx.	.
</s>
<s>
režisér	režisér	k1gMnSc1	režisér
a	a	k8xC	a
fotograf	fotograf	k1gMnSc1	fotograf
Ivan	Ivan	k1gMnSc1	Ivan
Wernisch	Wernisch	k1gMnSc1	Wernisch
-	-	kIx~	-
český	český	k2eAgMnSc1d1	český
básník	básník	k1gMnSc1	básník
Jovan	Jovan	k1gMnSc1	Jovan
Jovanović	Jovanović	k1gMnSc1	Jovanović
Zmaj	zmaj	k1gMnSc1	zmaj
-	-	kIx~	-
srbský	srbský	k2eAgMnSc1d1	srbský
spisovatel	spisovatel	k1gMnSc1	spisovatel
Ivan	Ivan	k1gMnSc1	Ivan
Ženatý	ženatý	k2eAgMnSc1d1	ženatý
-	-	kIx~	-
český	český	k2eAgMnSc1d1	český
houslista	houslista	k1gMnSc1	houslista
a	a	k8xC	a
pedagog	pedagog	k1gMnSc1	pedagog
Dávid	Dávida	k1gFnPc2	Dávida
Ivan	Ivan	k1gMnSc1	Ivan
(	(	kIx(	(
<g/>
*	*	kIx~	*
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
-	-	kIx~	-
slovenský	slovenský	k2eAgMnSc1d1	slovenský
fotbalista	fotbalista	k1gMnSc1	fotbalista
Ivan	Ivan	k1gMnSc1	Ivan
(	(	kIx(	(
<g/>
kráter	kráter	k1gInSc1	kráter
<g/>
)	)	kIx)	)
-	-	kIx~	-
kráter	kráter	k1gInSc1	kráter
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
Svatý	svatý	k2eAgMnSc1d1	svatý
Jan	Jan	k1gMnSc1	Jan
pod	pod	k7c7	pod
Skalou	Skala	k1gMnSc7	Skala
Seznam	seznam	k1gInSc4	seznam
článků	článek	k1gInPc2	článek
začínajících	začínající	k2eAgInPc2d1	začínající
na	na	k7c4	na
"	"	kIx"	"
<g/>
Ivan	Ivan	k1gMnSc1	Ivan
<g/>
"	"	kIx"	"
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Ivan	Ivana	k1gFnPc2	Ivana
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Ivan	Ivan	k1gMnSc1	Ivan
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
