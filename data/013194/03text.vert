<p>
<s>
King	King	k1gMnSc1	King
Size	Siz	k1gInSc2	Siz
je	být	k5eAaImIp3nS	být
francouzský	francouzský	k2eAgInSc1d1	francouzský
filmový	filmový	k2eAgInSc1d1	filmový
muzikál	muzikál	k1gInSc1	muzikál
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
režíroval	režírovat	k5eAaImAgMnS	režírovat
Patrick	Patrick	k1gMnSc1	Patrick
Maurin	Maurin	k1gInSc4	Maurin
podle	podle	k7c2	podle
vlastního	vlastní	k2eAgInSc2d1	vlastní
scénáře	scénář	k1gInSc2	scénář
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
byl	být	k5eAaImAgInS	být
natáčen	natáčet	k5eAaImNgInS	natáčet
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
téměř	téměř	k6eAd1	téměř
kompletně	kompletně	k6eAd1	kompletně
na	na	k7c6	na
pravém	pravý	k2eAgInSc6d1	pravý
břehu	břeh	k1gInSc6	břeh
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
9	[number]	k4	9
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
11	[number]	k4	11
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
.	.	kIx.	.
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
obvod	obvod	k1gInSc1	obvod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
taneční	taneční	k2eAgFnSc1d1	taneční
scéna	scéna	k1gFnSc1	scéna
na	na	k7c4	na
nábřeží	nábřeží	k1gNnSc4	nábřeží
Quai	quai	k1gNnSc2	quai
de	de	k?	de
la	la	k1gNnSc2	la
Tournelle	Tournelle	k1gFnSc1	Tournelle
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
5	[number]	k4	5
<g/>
.	.	kIx.	.
obvodě	obvod	k1gInSc6	obvod
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
na	na	k7c6	na
levém	levý	k2eAgInSc6d1	levý
břehu	břeh	k1gInSc6	břeh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Děj	děj	k1gInSc1	děj
==	==	k?	==
</s>
</p>
<p>
<s>
Vincent	Vincent	k1gMnSc1	Vincent
a	a	k8xC	a
Nicolas	Nicolas	k1gMnSc1	Nicolas
žijí	žít	k5eAaImIp3nP	žít
společně	společně	k6eAd1	společně
na	na	k7c6	na
Montmartru	Montmartr	k1gInSc6	Montmartr
a	a	k8xC	a
jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
v	v	k7c6	v
gay	gay	k1gMnSc1	gay
baru	bar	k1gInSc2	bar
potkají	potkat	k5eAaPmIp3nP	potkat
Gabriela	Gabriela	k1gFnSc1	Gabriela
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yQgInSc7	který
stráví	strávit	k5eAaPmIp3nP	strávit
dohromady	dohromady	k6eAd1	dohromady
noc	noc	k1gFnSc4	noc
<g/>
.	.	kIx.	.
</s>
<s>
Gabriel	Gabriel	k1gMnSc1	Gabriel
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Lyonu	Lyon	k1gInSc2	Lyon
a	a	k8xC	a
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
studuje	studovat	k5eAaImIp3nS	studovat
politologii	politologie	k1gFnSc4	politologie
<g/>
.	.	kIx.	.
</s>
<s>
Druhého	druhý	k4xOgInSc2	druhý
dne	den	k1gInSc2	den
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
sejdou	sejít	k5eAaPmIp3nP	sejít
a	a	k8xC	a
Vincent	Vincent	k1gMnSc1	Vincent
s	s	k7c7	s
Nicolasem	Nicolas	k1gInSc7	Nicolas
Gabrielovi	Gabrielův	k2eAgMnPc1d1	Gabrielův
nabídnou	nabídnout	k5eAaPmIp3nP	nabídnout
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
bydlel	bydlet	k5eAaImAgMnS	bydlet
pohromadě	pohromadě	k6eAd1	pohromadě
<g/>
.	.	kIx.	.
</s>
<s>
Musejí	muset	k5eAaImIp3nP	muset
ale	ale	k9	ale
mít	mít	k5eAaImF	mít
větší	veliký	k2eAgFnSc4d2	veliký
postel	postel	k1gFnSc4	postel
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
všichni	všechen	k3xTgMnPc1	všechen
vešli	vejít	k5eAaPmAgMnP	vejít
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
si	se	k3xPyFc3	se
koupí	koupit	k5eAaPmIp3nP	koupit
americkou	americký	k2eAgFnSc4d1	americká
s	s	k7c7	s
velikostí	velikost	k1gFnSc7	velikost
King	King	k1gMnSc1	King
Size	Siz	k1gFnSc2	Siz
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
jejich	jejich	k3xOp3gMnPc4	jejich
příbuzné	příbuzný	k1gMnPc4	příbuzný
je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
dané	daný	k2eAgNnSc1d1	dané
<g/>
,	,	kIx,	,
že	že	k8xS	že
oba	dva	k4xCgMnPc1	dva
muži	muž	k1gMnPc1	muž
bydlí	bydlet	k5eAaImIp3nP	bydlet
v	v	k7c6	v
homosexuálním	homosexuální	k2eAgInSc6d1	homosexuální
svazku	svazek	k1gInSc6	svazek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
třech	tři	k4xCgFnPc6	tři
s	s	k7c7	s
mladým	mladý	k2eAgMnSc7d1	mladý
studentem	student	k1gMnSc7	student
je	být	k5eAaImIp3nS	být
už	už	k6eAd1	už
přece	přece	k9	přece
jen	jen	k9	jen
příliš	příliš	k6eAd1	příliš
<g/>
.	.	kIx.	.
</s>
<s>
Vincentova	Vincentův	k2eAgFnSc1d1	Vincentova
sestra	sestra	k1gFnSc1	sestra
Sophie	Sophie	k1gFnSc2	Sophie
nechce	chtít	k5eNaImIp3nS	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
všichni	všechen	k3xTgMnPc1	všechen
tři	tři	k4xCgMnPc1	tři
přijeli	přijet	k5eAaPmAgMnP	přijet
na	na	k7c4	na
Vánoce	Vánoce	k1gFnPc4	Vánoce
k	k	k7c3	k
rodičům	rodič	k1gMnPc3	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Nicolasova	Nicolasův	k2eAgFnSc1d1	Nicolasova
matka	matka	k1gFnSc1	matka
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
jejich	jejich	k3xOp3gNnSc4	jejich
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
pochopit	pochopit	k5eAaPmF	pochopit
<g/>
.	.	kIx.	.
</s>
<s>
Gabriel	Gabriel	k1gMnSc1	Gabriel
se	se	k3xPyFc4	se
setká	setkat	k5eAaPmIp3nS	setkat
se	s	k7c7	s
Sophií	Sophie	k1gFnSc7	Sophie
a	a	k8xC	a
řekne	říct	k5eAaPmIp3nS	říct
jí	on	k3xPp3gFnSc3	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
Vánoce	Vánoce	k1gFnPc4	Vánoce
raději	rád	k6eAd2	rád
odjede	odjet	k5eAaPmIp3nS	odjet
za	za	k7c7	za
rodiči	rodič	k1gMnPc7	rodič
do	do	k7c2	do
Lyonu	Lyon	k1gInSc2	Lyon
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Vincent	Vincent	k1gMnSc1	Vincent
mohl	moct	k5eAaImAgMnS	moct
být	být	k5eAaImF	být
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
rodinou	rodina	k1gFnSc7	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Sophie	Sophie	k1gFnSc1	Sophie
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
s	s	k7c7	s
jejich	jejich	k3xOp3gInSc7	jejich
vztahem	vztah	k1gInSc7	vztah
smíří	smířit	k5eAaPmIp3nS	smířit
<g/>
,	,	kIx,	,
a	a	k8xC	a
protože	protože	k8xS	protože
sama	sám	k3xTgFnSc1	sám
opouští	opouštět	k5eAaImIp3nS	opouštět
manžela	manžel	k1gMnSc4	manžel
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
milence	milenec	k1gMnSc4	milenec
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yRgInSc7	který
čeká	čekat	k5eAaImIp3nS	čekat
dítě	dítě	k1gNnSc1	dítě
<g/>
,	,	kIx,	,
za	za	k7c7	za
rodiči	rodič	k1gMnPc7	rodič
nepojede	jet	k5eNaImIp3nS	jet
a	a	k8xC	a
Vánoce	Vánoce	k1gFnPc1	Vánoce
stráví	strávit	k5eAaPmIp3nP	strávit
svátky	svátek	k1gInPc4	svátek
s	s	k7c7	s
nimi	on	k3xPp3gFnPc7	on
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gMnPc7	jejich
přáteli	přítel	k1gMnPc7	přítel
a	a	k8xC	a
Nicolasovou	Nicolasův	k2eAgFnSc7d1	Nicolasova
matkou	matka	k1gFnSc7	matka
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc1	svůj
dítě	dítě	k1gNnSc1	dítě
chce	chtít	k5eAaImIp3nS	chtít
pojmenovat	pojmenovat	k5eAaPmF	pojmenovat
Gabriel	Gabriel	k1gMnSc1	Gabriel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obsazení	obsazení	k1gNnSc1	obsazení
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
King	King	k1gInSc1	King
Size	Siz	k1gInSc2	Siz
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
v	v	k7c6	v
databázi	databáze	k1gFnSc6	databáze
AlloCiné	AlloCiná	k1gFnSc2	AlloCiná
</s>
</p>
