<p>
<s>
Bismoklit	Bismoklit	k1gInSc1	Bismoklit
je	být	k5eAaImIp3nS	být
minerál	minerál	k1gInSc4	minerál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
Mountain	Mountain	k1gInSc1	Mountain
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
BiOCl	BiOCl	k1gInSc1	BiOCl
<g/>
,	,	kIx,	,
čtverečný	čtverečný	k2eAgInSc1d1	čtverečný
minerál	minerál	k1gInSc1	minerál
<g/>
.	.	kIx.	.
</s>
<s>
Čistý	čistý	k2eAgInSc1d1	čistý
BiOCl	BiOCl	k1gInSc1	BiOCl
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
a	a	k8xC	a
vždy	vždy	k6eAd1	vždy
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
něco	něco	k3yInSc1	něco
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
izomorfní	izomorfní	k2eAgFnSc4d1	izomorfní
řadu	řada	k1gFnSc4	řada
s	s	k7c7	s
daubréeitem	daubréeit	k1gInSc7	daubréeit
a	a	k8xC	a
prakticky	prakticky	k6eAd1	prakticky
je	být	k5eAaImIp3nS	být
bismoklit	bismoklit	k5eAaPmF	bismoklit
chápán	chápat	k5eAaImNgInS	chápat
do	do	k7c2	do
OH	OH	kA	OH
<	<	kIx(	<
Cl	Cl	k1gMnSc1	Cl
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
celistvé	celistvý	k2eAgInPc4d1	celistvý
agregáty	agregát	k1gInPc4	agregát
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
také	také	k9	také
zemitý	zemitý	k2eAgInSc1d1	zemitý
<g/>
,	,	kIx,	,
sloupcovitý	sloupcovitý	k2eAgInSc1d1	sloupcovitý
až	až	k6eAd1	až
vláknitý	vláknitý	k2eAgInSc1d1	vláknitý
a	a	k8xC	a
v	v	k7c6	v
drobných	drobný	k2eAgInPc6d1	drobný
šupinkatých	šupinkatý	k2eAgInPc6d1	šupinkatý
krystalech	krystal	k1gInPc6	krystal
<g/>
.	.	kIx.	.
</s>
<s>
Nacházeny	nacházen	k2eAgInPc1d1	nacházen
sférolity	sférolit	k1gInPc1	sférolit
<g/>
,	,	kIx,	,
pseudomorfózy	pseudomorfóza	k1gFnPc1	pseudomorfóza
po	po	k7c6	po
primárních	primární	k2eAgInPc6d1	primární
minerálech	minerál	k1gInPc6	minerál
Bi	Bi	k1gFnSc2	Bi
<g/>
.	.	kIx.	.
</s>
<s>
Umělé	umělý	k2eAgInPc1d1	umělý
krystaly	krystal	k1gInPc1	krystal
bismoklitu	bismoklit	k1gInSc2	bismoklit
tvoří	tvořit	k5eAaImIp3nP	tvořit
čtverečné	čtverečný	k2eAgFnPc1d1	čtverečná
tabulky	tabulka	k1gFnPc1	tabulka
podle	podle	k7c2	podle
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
nízkou	nízký	k2eAgFnSc7d1	nízká
vicinální	vicinální	k2eAgFnSc7d1	vicinální
pyramidou	pyramida	k1gFnSc7	pyramida
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
==	==	k?	==
</s>
</p>
<p>
<s>
Druhotný	druhotný	k2eAgInSc1d1	druhotný
minerál	minerál	k1gInSc1	minerál
<g/>
,	,	kIx,	,
vzniká	vznikat	k5eAaImIp3nS	vznikat
alterací	alterace	k1gFnSc7	alterace
vizmutu	vizmut	k1gInSc2	vizmut
a	a	k8xC	a
bismutinu	bismutin	k1gInSc2	bismutin
<g/>
.	.	kIx.	.
</s>
<s>
Uměle	uměle	k6eAd1	uměle
vzniká	vznikat	k5eAaImIp3nS	vznikat
v	v	k7c6	v
drobných	drobný	k2eAgInPc6d1	drobný
krystalech	krystal	k1gInPc6	krystal
při	při	k7c6	při
hydrolýze	hydrolýza	k1gFnSc6	hydrolýza
BiCl	BiCl	k1gInSc1	BiCl
<g/>
3	[number]	k4	3
v	v	k7c6	v
HCl	HCl	k1gFnSc6	HCl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
Fyzikální	fyzikální	k2eAgMnPc4d1	fyzikální
–	–	k?	–
Barvu	barva	k1gFnSc4	barva
má	mít	k5eAaImIp3nS	mít
bělavou	bělavý	k2eAgFnSc4d1	bělavá
<g/>
,	,	kIx,	,
krémově	krémově	k6eAd1	krémově
bílou	bílý	k2eAgFnSc4d1	bílá
<g/>
,	,	kIx,	,
světle	světle	k6eAd1	světle
zelenou	zelený	k2eAgFnSc4d1	zelená
<g/>
,	,	kIx,	,
šedavou	šedavý	k2eAgFnSc4d1	šedavá
a	a	k8xC	a
žlutavě	žlutavě	k6eAd1	žlutavě
hnědou	hnědý	k2eAgFnSc4d1	hnědá
<g/>
.	.	kIx.	.
</s>
<s>
Šedavé	šedavý	k2eAgInPc1d1	šedavý
vzorky	vzorek	k1gInPc1	vzorek
mají	mít	k5eAaImIp3nP	mít
často	často	k6eAd1	často
na	na	k7c6	na
čerstvém	čerstvý	k2eAgInSc6d1	čerstvý
lomu	lom	k1gInSc6	lom
téměř	téměř	k6eAd1	téměř
bílou	bílý	k2eAgFnSc4d1	bílá
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
bývá	bývat	k5eAaImIp3nS	bývat
nerovnoměrná	rovnoměrný	k2eNgFnSc1d1	nerovnoměrná
v	v	k7c6	v
mase	masa	k1gFnSc6	masa
celého	celý	k2eAgInSc2d1	celý
vzorku	vzorek	k1gInSc2	vzorek
<g/>
.	.	kIx.	.
</s>
<s>
Lesk	lesk	k1gInSc1	lesk
má	mít	k5eAaImIp3nS	mít
mastný	mastný	k2eAgInSc1d1	mastný
<g/>
,	,	kIx,	,
hedvábný	hedvábný	k2eAgInSc1d1	hedvábný
<g/>
,	,	kIx,	,
perleťový	perleťový	k2eAgInSc1d1	perleťový
na	na	k7c6	na
štěpných	štěpný	k2eAgFnPc6d1	štěpná
plochách	plocha	k1gFnPc6	plocha
<g/>
,	,	kIx,	,
v	v	k7c6	v
celistvých	celistvý	k2eAgInPc6d1	celistvý
agregátech	agregát	k1gInPc6	agregát
jen	jen	k6eAd1	jen
mdlý	mdlý	k2eAgMnSc1d1	mdlý
až	až	k8xS	až
zemitý	zemitý	k2eAgMnSc1d1	zemitý
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
dokonalou	dokonalý	k2eAgFnSc4d1	dokonalá
štěpnost	štěpnost	k1gFnSc4	štěpnost
podle	podle	k7c2	podle
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
plastický	plastický	k2eAgInSc1d1	plastický
a	a	k8xC	a
ohebný	ohebný	k2eAgInSc1d1	ohebný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chemické	chemický	k2eAgNnSc1d1	chemické
-	-	kIx~	-
</s>
</p>
<p>
<s>
Optické	optický	k2eAgNnSc1d1	optické
-	-	kIx~	-
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
sběratelskou	sběratelský	k2eAgFnSc4d1	sběratelská
raritu	rarita	k1gFnSc4	rarita
<g/>
,	,	kIx,	,
vyhledávanou	vyhledávaný	k2eAgFnSc4d1	vyhledávaná
převážně	převážně	k6eAd1	převážně
do	do	k7c2	do
mineralogických	mineralogický	k2eAgFnPc2d1	mineralogická
sbírek	sbírka	k1gFnPc2	sbírka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Naleziště	naleziště	k1gNnSc2	naleziště
==	==	k?	==
</s>
</p>
<p>
<s>
Původně	původně	k6eAd1	původně
nalezen	naleznout	k5eAaPmNgInS	naleznout
na	na	k7c4	na
Jackals	Jackals	k1gInSc4	Jackals
Water	Water	k1gInSc1	Water
(	(	kIx(	(
<g/>
29	[number]	k4	29
km	km	kA	km
ssv	ssv	k?	ssv
<g/>
.	.	kIx.	.
od	od	k7c2	od
Steinfordu	Steinford	k1gInSc2	Steinford
<g/>
,	,	kIx,	,
Namaqualand	Namaqualand	k1gInSc1	Namaqualand
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc1d1	jižní
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
)	)	kIx)	)
v	v	k7c6	v
drobně	drobně	k6eAd1	drobně
krystalických	krystalický	k2eAgInPc6d1	krystalický
agregátech	agregát	k1gInPc6	agregát
promíšených	promíšený	k2eAgFnPc2d1	promíšená
drobnými	drobný	k2eAgFnPc7d1	drobná
šupinkami	šupinka	k1gFnPc7	šupinka
muskovitu	muskovit	k1gInSc2	muskovit
s	s	k7c7	s
bismutinem	bismutin	k1gInSc7	bismutin
v	v	k7c6	v
pegmatitu	pegmatit	k1gInSc6	pegmatit
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
ložiska	ložisko	k1gNnSc2	ložisko
Karaoba	Karaoba	k1gFnSc1	Karaoba
v	v	k7c6	v
Kazachstánu	Kazachstán	k1gInSc6	Kazachstán
známy	znám	k2eAgInPc4d1	znám
kryptokrystalické	kryptokrystalický	k2eAgInPc4d1	kryptokrystalický
agregáty	agregát	k1gInPc4	agregát
o	o	k7c6	o
rozměrech	rozměr	k1gInPc6	rozměr
5	[number]	k4	5
×	×	k?	×
3	[number]	k4	3
×	×	k?	×
2	[number]	k4	2
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Bismoklit	Bismoklit	k1gInSc1	Bismoklit
na	na	k7c6	na
webu	web	k1gInSc6	web
mindat	mindat	k5eAaImF	mindat
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
