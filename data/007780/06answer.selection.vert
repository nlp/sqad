<s>
Želva	želva	k1gFnSc1	želva
bahenní	bahenní	k2eAgFnSc1d1	bahenní
(	(	kIx(	(
<g/>
Emys	Emys	k1gInSc1	Emys
orbicularis	orbicularis	k1gInSc1	orbicularis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jediná	jediný	k2eAgFnSc1d1	jediná
želva	želva	k1gFnSc1	želva
volně	volně	k6eAd1	volně
se	se	k3xPyFc4	se
vyskytující	vyskytující	k2eAgFnSc1d1	vyskytující
(	(	kIx(	(
<g/>
přirozeně	přirozeně	k6eAd1	přirozeně
<g/>
)	)	kIx)	)
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
tedy	tedy	k9	tedy
i	i	k9	i
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
