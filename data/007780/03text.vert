<s>
Želva	želva	k1gFnSc1	želva
bahenní	bahenní	k2eAgFnSc1d1	bahenní
(	(	kIx(	(
<g/>
Emys	Emys	k1gInSc1	Emys
orbicularis	orbicularis	k1gInSc1	orbicularis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jediná	jediný	k2eAgFnSc1d1	jediná
želva	želva	k1gFnSc1	želva
volně	volně	k6eAd1	volně
se	se	k3xPyFc4	se
vyskytující	vyskytující	k2eAgFnSc1d1	vyskytující
(	(	kIx(	(
<g/>
přirozeně	přirozeně	k6eAd1	přirozeně
<g/>
)	)	kIx)	)
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
tedy	tedy	k9	tedy
i	i	k9	i
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Želva	želva	k1gFnSc1	želva
bahenní	bahenní	k2eAgFnSc1d1	bahenní
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
želvy	želva	k1gFnPc4	želva
přechodného	přechodný	k2eAgInSc2d1	přechodný
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
jsou	být	k5eAaImIp3nP	být
bahenní	bahenní	k2eAgFnPc1d1	bahenní
želvy	želva	k1gFnPc1	želva
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
podobné	podobný	k2eAgFnPc1d1	podobná
suchozemským	suchozemský	k2eAgFnPc3d1	suchozemská
želvám	želva	k1gFnPc3	želva
končetinami	končetina	k1gFnPc7	končetina
a	a	k8xC	a
vodním	vodní	k2eAgFnPc3d1	vodní
želvám	želva	k1gFnPc3	želva
tvarem	tvar	k1gInSc7	tvar
karapaxu	karapax	k1gInSc2	karapax
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
želvy	želva	k1gFnSc2	želva
bahenní	bahenní	k2eAgNnSc1d1	bahenní
může	moct	k5eAaImIp3nS	moct
dorůstat	dorůstat	k5eAaImF	dorůstat
až	až	k9	až
25	[number]	k4	25
cm	cm	kA	cm
<g/>
,	,	kIx,	,
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
drobný	drobný	k2eAgMnSc1d1	drobný
sameček	sameček	k1gMnSc1	sameček
měří	měřit	k5eAaImIp3nS	měřit
jen	jen	k9	jen
okolo	okolo	k7c2	okolo
15	[number]	k4	15
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gMnSc1	samec
má	mít	k5eAaImIp3nS	mít
oči	oko	k1gNnPc4	oko
hnědé	hnědý	k2eAgFnPc1d1	hnědá
<g/>
,	,	kIx,	,
samice	samice	k1gFnPc1	samice
nažloutlé	nažloutlý	k2eAgFnPc1d1	nažloutlá
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
prsty	prst	k1gInPc4	prst
má	mít	k5eAaImIp3nS	mít
blány	blána	k1gFnPc4	blána
<g/>
.	.	kIx.	.
</s>
<s>
Krunýř	krunýř	k1gInSc1	krunýř
má	mít	k5eAaImIp3nS	mít
tmavý	tmavý	k2eAgInSc1d1	tmavý
se	s	k7c7	s
žlutými	žlutý	k2eAgFnPc7d1	žlutá
skvrnkami	skvrnka	k1gFnPc7	skvrnka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
želva	želva	k1gFnSc1	želva
dravá	dravý	k2eAgFnSc1d1	dravá
<g/>
,	,	kIx,	,
živí	živit	k5eAaImIp3nS	živit
se	s	k7c7	s
převážně	převážně	k6eAd1	převážně
malými	malý	k2eAgFnPc7d1	malá
rybami	ryba	k1gFnPc7	ryba
<g/>
,	,	kIx,	,
obojživelníky	obojživelník	k1gMnPc7	obojživelník
<g/>
,	,	kIx,	,
plži	plž	k1gMnPc7	plž
<g/>
,	,	kIx,	,
mlži	mlž	k1gMnPc7	mlž
<g/>
,	,	kIx,	,
hmyzem	hmyz	k1gInSc7	hmyz
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc7	jeho
larvami	larva	k1gFnPc7	larva
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
chovu	chov	k1gInSc6	chov
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
jí	jíst	k5eAaImIp3nS	jíst
lze	lze	k6eAd1	lze
živočišnou	živočišný	k2eAgFnSc4d1	živočišná
potravu	potrava	k1gFnSc4	potrava
občas	občas	k6eAd1	občas
kombinovat	kombinovat	k5eAaImF	kombinovat
s	s	k7c7	s
vodními	vodní	k2eAgFnPc7d1	vodní
rostlinami	rostlina	k1gFnPc7	rostlina
<g/>
,	,	kIx,	,
salátem	salát	k1gInSc7	salát
<g/>
,	,	kIx,	,
trávou	tráva	k1gFnSc7	tráva
nebo	nebo	k8xC	nebo
květy	květ	k1gInPc7	květ
a	a	k8xC	a
listy	list	k1gInPc1	list
smetanky	smetanka	k1gFnSc2	smetanka
lékařské	lékařský	k2eAgInPc1d1	lékařský
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stravě	strava	k1gFnSc6	strava
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
dostatek	dostatek	k1gInSc4	dostatek
vitaminů	vitamin	k1gInPc2	vitamin
(	(	kIx(	(
<g/>
především	především	k9	především
A	A	kA	A
<g/>
,	,	kIx,	,
C	C	kA	C
a	a	k8xC	a
D	D	kA	D
<g/>
)	)	kIx)	)
a	a	k8xC	a
minerálních	minerální	k2eAgFnPc2d1	minerální
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Želva	želva	k1gFnSc1	želva
bahenní	bahenní	k2eAgNnSc1d1	bahenní
přezimuje	přezimovat	k5eAaBmIp3nS	přezimovat
u	u	k7c2	u
dna	dno	k1gNnSc2	dno
zahrabána	zahrabat	k5eAaPmNgFnS	zahrabat
do	do	k7c2	do
bahna	bahno	k1gNnSc2	bahno
(	(	kIx(	(
<g/>
to	ten	k3xDgNnSc1	ten
jí	jíst	k5eAaImIp3nS	jíst
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
stálou	stálý	k2eAgFnSc4d1	stálá
teplotu	teplota	k1gFnSc4	teplota
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hibernuje	Hibernovat	k5eAaImIp3nS	Hibernovat
podle	podle	k7c2	podle
momentálních	momentální	k2eAgFnPc2d1	momentální
podmínek	podmínka	k1gFnPc2	podmínka
5-7	[number]	k4	5-7
měsíců	měsíc	k1gInPc2	měsíc
(	(	kIx(	(
<g/>
asi	asi	k9	asi
od	od	k7c2	od
října	říjen	k1gInSc2	říjen
do	do	k7c2	do
dubna	duben	k1gInSc2	duben
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Aktivní	aktivní	k2eAgFnSc1d1	aktivní
je	být	k5eAaImIp3nS	být
především	především	k9	především
ráno	ráno	k6eAd1	ráno
a	a	k8xC	a
večer	večer	k6eAd1	večer
<g/>
.	.	kIx.	.
</s>
<s>
Žít	žít	k5eAaImF	žít
může	moct	k5eAaImIp3nS	moct
přes	přes	k7c4	přes
sto	sto	k4xCgNnSc4	sto
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
zdroje	zdroj	k1gInPc1	zdroj
dokonce	dokonce	k9	dokonce
uvádějí	uvádět	k5eAaImIp3nP	uvádět
120	[number]	k4	120
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Pohlavní	pohlavní	k2eAgFnSc2d1	pohlavní
dospělosti	dospělost	k1gFnSc2	dospělost
želva	želva	k1gFnSc1	želva
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
asi	asi	k9	asi
okolo	okolo	k7c2	okolo
10	[number]	k4	10
let	léto	k1gNnPc2	léto
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
Páří	pářit	k5eAaImIp3nS	pářit
se	se	k3xPyFc4	se
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
a	a	k8xC	a
během	během	k7c2	během
června	červen	k1gInSc2	červen
až	až	k6eAd1	až
začátkem	začátkem	k7c2	začátkem
července	červenec	k1gInSc2	červenec
samice	samice	k1gFnSc1	samice
klade	klást	k5eAaImIp3nS	klást
3-16	[number]	k4	3-16
vajec	vejce	k1gNnPc2	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
je	on	k3xPp3gInPc4	on
může	moct	k5eAaImIp3nS	moct
zahrabat	zahrabat	k5eAaPmF	zahrabat
i	i	k9	i
poměrně	poměrně	k6eAd1	poměrně
daleko	daleko	k6eAd1	daleko
od	od	k7c2	od
vody	voda	k1gFnSc2	voda
na	na	k7c6	na
slunném	slunný	k2eAgInSc6d1	slunný
a	a	k8xC	a
nejlépe	dobře	k6eAd3	dobře
písčitém	písčitý	k2eAgInSc6d1	písčitý
břehu	břeh	k1gInSc6	břeh
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
se	se	k3xPyFc4	se
líhnou	líhnout	k5eAaImIp3nP	líhnout
asi	asi	k9	asi
po	po	k7c6	po
100	[number]	k4	100
dnech	den	k1gInPc6	den
<g/>
,	,	kIx,	,
při	při	k7c6	při
nepřízni	nepřízeň	k1gFnSc6	nepřízeň
počasí	počasí	k1gNnSc2	počasí
ale	ale	k8xC	ale
vylézají	vylézat	k5eAaImIp3nP	vylézat
až	až	k6eAd1	až
dalšího	další	k2eAgNnSc2d1	další
jara	jaro	k1gNnSc2	jaro
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
mají	mít	k5eAaImIp3nP	mít
žlutou	žlutý	k2eAgFnSc4d1	žlutá
rohovku	rohovka	k1gFnSc4	rohovka
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
pohlaví	pohlaví	k1gNnSc6	pohlaví
mláďat	mládě	k1gNnPc2	mládě
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
v	v	k7c6	v
určité	určitý	k2eAgFnSc6d1	určitá
fázi	fáze	k1gFnSc6	fáze
jejich	jejich	k3xOp3gInSc2	jejich
vývinu	vývin	k1gInSc2	vývin
okolní	okolní	k2eAgFnSc1d1	okolní
teplota	teplota	k1gFnSc1	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
kolem	kolem	k7c2	kolem
24	[number]	k4	24
–	–	k?	–
28	[number]	k4	28
°	°	k?	°
<g/>
C	C	kA	C
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
fázi	fáze	k1gFnSc6	fáze
jejich	jejich	k3xOp3gInSc2	jejich
vývinu	vývin	k1gInSc2	vývin
se	se	k3xPyFc4	se
líhnou	líhnout	k5eAaImIp3nP	líhnout
samečkové	sameček	k1gMnPc1	sameček
<g/>
,	,	kIx,	,
při	při	k7c6	při
vyšší	vysoký	k2eAgFnSc6d2	vyšší
teplotě	teplota	k1gFnSc6	teplota
samičky	samička	k1gFnSc2	samička
<g/>
.	.	kIx.	.
</s>
<s>
Želva	želva	k1gFnSc1	želva
bahenní	bahenní	k2eAgFnSc1d1	bahenní
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
téměř	téměř	k6eAd1	téměř
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
kromě	kromě	k7c2	kromě
Skandinávie	Skandinávie	k1gFnSc2	Skandinávie
<g/>
,	,	kIx,	,
na	na	k7c6	na
Blízkém	blízký	k2eAgInSc6d1	blízký
východě	východ	k1gInSc6	východ
a	a	k8xC	a
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
ji	on	k3xPp3gFnSc4	on
najdeme	najít	k5eAaPmIp1nP	najít
ve	v	k7c6	v
stojatých	stojatý	k2eAgNnPc6d1	stojaté
slepých	slepý	k2eAgNnPc6d1	slepé
ramenech	rameno	k1gNnPc6	rameno
řek	řeka	k1gFnPc2	řeka
a	a	k8xC	a
rybnících	rybník	k1gInPc6	rybník
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
její	její	k3xOp3gFnSc1	její
aktivní	aktivní	k2eAgFnSc1d1	aktivní
teplota	teplota	k1gFnSc1	teplota
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
nízká	nízký	k2eAgFnSc1d1	nízká
<g/>
,	,	kIx,	,
dává	dávat	k5eAaImIp3nS	dávat
přednost	přednost	k1gFnSc4	přednost
prohřátým	prohřátý	k2eAgFnPc3d1	prohřátá
mělčinám	mělčina	k1gFnPc3	mělčina
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
u	u	k7c2	u
stojatých	stojatý	k2eAgFnPc2d1	stojatá
vod	voda	k1gFnPc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
je	být	k5eAaImIp3nS	být
chráněna	chráněn	k2eAgFnSc1d1	chráněna
jako	jako	k8xC	jako
kriticky	kriticky	k6eAd1	kriticky
ohrožený	ohrožený	k2eAgInSc4d1	ohrožený
druh	druh	k1gInSc4	druh
a	a	k8xC	a
její	její	k3xOp3gInSc1	její
výskyt	výskyt	k1gInSc1	výskyt
je	být	k5eAaImIp3nS	být
zaznamenáván	zaznamenávat	k5eAaImNgInS	zaznamenávat
pouze	pouze	k6eAd1	pouze
náhodně	náhodně	k6eAd1	náhodně
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgFnSc1d2	veliký
šance	šance	k1gFnSc1	šance
na	na	k7c4	na
její	její	k3xOp3gNnSc4	její
spatření	spatření	k1gNnSc4	spatření
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
,	,	kIx,	,
i	i	k8xC	i
tam	tam	k6eAd1	tam
je	být	k5eAaImIp3nS	být
však	však	k9	však
přísně	přísně	k6eAd1	přísně
chráněna	chránit	k5eAaImNgFnS	chránit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
byla	být	k5eAaImAgFnS	být
několikrát	několikrát	k6eAd1	několikrát
uměle	uměle	k6eAd1	uměle
vysazena	vysadit	k5eAaPmNgFnS	vysadit
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
jejích	její	k3xOp3gInPc2	její
stavů	stav	k1gInPc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
chovu	chov	k1gInSc6	chov
ji	on	k3xPp3gFnSc4	on
má	mít	k5eAaImIp3nS	mít
např.	např.	kA	např.
ZOO	zoo	k1gFnSc1	zoo
Ohrada	ohrada	k1gFnSc1	ohrada
<g/>
.	.	kIx.	.
</s>
