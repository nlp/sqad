<s>
Moravské	moravský	k2eAgNnSc1d1	Moravské
náměstí	náměstí	k1gNnSc1	náměstí
je	být	k5eAaImIp3nS	být
veřejné	veřejný	k2eAgNnSc1d1	veřejné
prostranství	prostranství	k1gNnSc1	prostranství
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
brněnské	brněnský	k2eAgFnSc2d1	brněnská
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
Brno-střed	Brnotřed	k1gMnSc1	Brno-střed
<g/>
,	,	kIx,	,
rozkládající	rozkládající	k2eAgMnSc1d1	rozkládající
se	se	k3xPyFc4	se
na	na	k7c6	na
severovýchodním	severovýchodní	k2eAgInSc6d1	severovýchodní
okraji	okraj	k1gInSc6	okraj
katastrálního	katastrální	k2eAgNnSc2d1	katastrální
území	území	k1gNnSc2	území
Město	město	k1gNnSc1	město
Brno	Brno	k1gNnSc1	Brno
<g/>
.	.	kIx.	.
</s>
