<p>
<s>
Moravské	moravský	k2eAgNnSc1d1	Moravské
náměstí	náměstí	k1gNnSc1	náměstí
je	být	k5eAaImIp3nS	být
veřejné	veřejný	k2eAgNnSc1d1	veřejné
prostranství	prostranství	k1gNnSc1	prostranství
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
brněnské	brněnský	k2eAgFnSc2d1	brněnská
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
Brno-střed	Brnotřed	k1gMnSc1	Brno-střed
<g/>
,	,	kIx,	,
rozkládající	rozkládající	k2eAgMnSc1d1	rozkládající
se	se	k3xPyFc4	se
na	na	k7c6	na
severovýchodním	severovýchodní	k2eAgInSc6d1	severovýchodní
okraji	okraj	k1gInSc6	okraj
katastrálního	katastrální	k2eAgNnSc2d1	katastrální
území	území	k1gNnSc2	území
Město	město	k1gNnSc1	město
Brno	Brno	k1gNnSc1	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc4	jeho
severní	severní	k2eAgFnSc4d1	severní
část	část	k1gFnSc4	část
zabírají	zabírat	k5eAaImIp3nP	zabírat
dva	dva	k4xCgInPc1	dva
parky	park	k1gInPc1	park
<g/>
,	,	kIx,	,
oddělené	oddělený	k2eAgNnSc1d1	oddělené
tramvajovou	tramvajový	k2eAgFnSc7d1	tramvajová
a	a	k8xC	a
silniční	silniční	k2eAgFnSc7d1	silniční
komunikací	komunikace	k1gFnSc7	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
východnějším	východní	k2eAgMnSc6d2	východnější
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
monumentální	monumentální	k2eAgInSc1d1	monumentální
pomník	pomník	k1gInSc1	pomník
"	"	kIx"	"
<g/>
Vítězství	vítězství	k1gNnSc1	vítězství
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
nad	nad	k7c7	nad
fašismem	fašismus	k1gInSc7	fašismus
<g/>
"	"	kIx"	"
od	od	k7c2	od
sochaře	sochař	k1gMnSc2	sochař
Vincence	Vincenc	k1gMnSc2	Vincenc
Makovského	Makovský	k1gMnSc2	Makovský
<g/>
.	.	kIx.	.
</s>
<s>
Cesty	cesta	k1gFnPc4	cesta
západnějšího	západní	k2eAgInSc2d2	západnější
parku	park	k1gInSc2	park
se	se	k3xPyFc4	se
paprskovitě	paprskovitě	k6eAd1	paprskovitě
sbíhají	sbíhat	k5eAaImIp3nP	sbíhat
k	k	k7c3	k
centrální	centrální	k2eAgFnSc3d1	centrální
fontáně	fontána	k1gFnSc3	fontána
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
Moravského	moravský	k2eAgNnSc2d1	Moravské
náměstí	náměstí	k1gNnSc2	náměstí
stojí	stát	k5eAaImIp3nS	stát
kostel	kostel	k1gInSc1	kostel
Zvěstování	zvěstování	k1gNnSc2	zvěstování
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
a	a	k8xC	a
svatého	svatý	k2eAgMnSc2d1	svatý
Tomáše	Tomáš	k1gMnSc2	Tomáš
apoštola	apoštol	k1gMnSc2	apoštol
s	s	k7c7	s
přilehlou	přilehlý	k2eAgFnSc7d1	přilehlá
budovou	budova	k1gFnSc7	budova
někdejšího	někdejší	k2eAgInSc2d1	někdejší
Místodržitelského	místodržitelský	k2eAgInSc2d1	místodržitelský
paláce	palác	k1gInSc2	palác
<g/>
.	.	kIx.	.
</s>
<s>
Dlážděné	dlážděný	k2eAgNnSc1d1	dlážděné
náměstí	náměstí	k1gNnSc1	náměstí
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
uzavírá	uzavírat	k5eAaPmIp3nS	uzavírat
z	z	k7c2	z
jihu	jih	k1gInSc2	jih
blok	blok	k1gInSc1	blok
budovy	budova	k1gFnSc2	budova
DOPZ	DOPZ	kA	DOPZ
s	s	k7c7	s
kinem	kino	k1gNnSc7	kino
Scala	scát	k5eAaImAgFnS	scát
a	a	k8xC	a
od	od	k7c2	od
západu	západ	k1gInSc2	západ
sídlo	sídlo	k1gNnSc4	sídlo
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
správního	správní	k2eAgInSc2d1	správní
soudu	soud	k1gInSc2	soud
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Do	do	k7c2	do
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
===	===	k?	===
</s>
</p>
<p>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nacházelo	nacházet	k5eAaImAgNnS	nacházet
opevnění	opevnění	k1gNnSc1	opevnění
středověkého	středověký	k2eAgNnSc2d1	středověké
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejmohutnějších	mohutný	k2eAgNnPc2d3	nejmohutnější
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
drželo	držet	k5eAaImAgNnS	držet
dnešní	dnešní	k2eAgFnSc4d1	dnešní
jižní	jižní	k2eAgFnSc4d1	jižní
část	část	k1gFnSc4	část
náměstí	náměstí	k1gNnSc2	náměstí
s	s	k7c7	s
kostelem	kostel	k1gInSc7	kostel
sv.	sv.	kA	sv.
Tomáše	Tomáš	k1gMnSc4	Tomáš
a	a	k8xC	a
místodržitelským	místodržitelský	k2eAgInSc7d1	místodržitelský
palácem	palác	k1gInSc7	palác
uvnitř	uvnitř	k7c2	uvnitř
hradeb	hradba	k1gFnPc2	hradba
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
rozsáhlé	rozsáhlý	k2eAgFnPc1d1	rozsáhlá
severní	severní	k2eAgFnPc1d1	severní
plochy	plocha	k1gFnPc1	plocha
parku	park	k1gInSc2	park
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
až	až	k9	až
jeho	jeho	k3xOp3gNnSc7	jeho
zbouráním	zbourání	k1gNnSc7	zbourání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Tomáše	Tomáš	k1gMnSc2	Tomáš
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jej	on	k3xPp3gMnSc4	on
založil	založit	k5eAaPmAgMnS	založit
Jan	Jan	k1gMnSc1	Jan
Jindřich	Jindřich	k1gMnSc1	Jindřich
společně	společně	k6eAd1	společně
s	s	k7c7	s
augustiniánským	augustiniánský	k2eAgInSc7d1	augustiniánský
klášterem	klášter	k1gInSc7	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Dokončené	dokončený	k2eAgNnSc1d1	dokončené
kněžiště	kněžiště	k1gNnSc1	kněžiště
bylo	být	k5eAaImAgNnS	být
posvěceno	posvětit	k5eAaPmNgNnS	posvětit
13	[number]	k4	13
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1356	[number]	k4	1356
<g/>
,	,	kIx,	,
současná	současný	k2eAgFnSc1d1	současná
barokní	barokní	k2eAgFnSc1d1	barokní
podoba	podoba	k1gFnSc1	podoba
kostela	kostel	k1gInSc2	kostel
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Přilehlou	přilehlý	k2eAgFnSc4d1	přilehlá
budovu	budova	k1gFnSc4	budova
Místodržitelského	místodržitelský	k2eAgInSc2d1	místodržitelský
paláce	palác	k1gInSc2	palác
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
barokní	barokní	k2eAgFnSc2d1	barokní
podoby	podoba	k1gFnSc2	podoba
přestavěna	přestavět	k5eAaPmNgFnS	přestavět
ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
využívaly	využívat	k5eAaPmAgFnP	využívat
po	po	k7c6	po
vystěhování	vystěhování	k1gNnSc6	vystěhování
kláštera	klášter	k1gInSc2	klášter
Josefem	Josef	k1gMnSc7	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1783	[number]	k4	1783
zemské	zemský	k2eAgInPc1d1	zemský
úřady	úřad	k1gInPc1	úřad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Do	do	k7c2	do
první	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
===	===	k?	===
</s>
</p>
<p>
<s>
Rozebráním	rozebrání	k1gNnSc7	rozebrání
městského	městský	k2eAgNnSc2d1	Městské
opevnění	opevnění	k1gNnSc2	opevnění
a	a	k8xC	a
rozšířením	rozšíření	k1gNnSc7	rozšíření
náměstí	náměstí	k1gNnSc2	náměstí
o	o	k7c4	o
parkové	parkový	k2eAgFnPc4d1	parková
plochy	plocha	k1gFnPc4	plocha
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
největší	veliký	k2eAgNnSc4d3	veliký
náměstí	náměstí	k1gNnSc4	náměstí
v	v	k7c6	v
celém	celý	k2eAgNnSc6d1	celé
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Vnější	vnější	k2eAgInSc1d1	vnější
bastionový	bastionový	k2eAgInSc1d1	bastionový
pás	pás	k1gInSc1	pás
města	město	k1gNnSc2	město
nechal	nechat	k5eAaPmAgInS	nechat
rozebrat	rozebrat	k5eAaPmF	rozebrat
už	už	k6eAd1	už
francouzský	francouzský	k2eAgMnSc1d1	francouzský
císař	císař	k1gMnSc1	císař
Napoleon	Napoleon	k1gMnSc1	Napoleon
I.	I.	kA	I.
roku	rok	k1gInSc2	rok
1809	[number]	k4	1809
<g/>
,	,	kIx,	,
na	na	k7c6	na
základě	základ	k1gInSc6	základ
nařízení	nařízení	k1gNnSc2	nařízení
rakouského	rakouský	k2eAgMnSc2d1	rakouský
císaře	císař	k1gMnSc2	císař
Františka	František	k1gMnSc4	František
Josefa	Josef	k1gMnSc2	Josef
I.	I.	kA	I.
ze	z	k7c2	z
dne	den	k1gInSc2	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1852	[number]	k4	1852
pak	pak	k6eAd1	pak
přestalo	přestat	k5eAaPmAgNnS	přestat
být	být	k5eAaImF	být
Brno	Brno	k1gNnSc1	Brno
vojenským	vojenský	k2eAgNnSc7d1	vojenské
městem	město	k1gNnSc7	město
a	a	k8xC	a
během	běh	k1gInSc7	běh
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
zrušen	zrušit	k5eAaPmNgInS	zrušit
i	i	k8xC	i
vnitřní	vnitřní	k2eAgInSc1d1	vnitřní
bastionový	bastionový	k2eAgInSc1d1	bastionový
pás	pás	k1gInSc1	pás
hradeb	hradba	k1gFnPc2	hradba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
rohu	roh	k1gInSc6	roh
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Lidické	lidický	k2eAgFnSc2d1	Lidická
ulice	ulice	k1gFnSc2	ulice
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1860	[number]	k4	1860
<g/>
–	–	k?	–
<g/>
1863	[number]	k4	1863
postaven	postaven	k2eAgInSc4d1	postaven
Berglův	Berglův	k2eAgInSc4d1	Berglův
palác	palác	k1gInSc4	palác
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
<g/>
,	,	kIx,	,
parkové	parkový	k2eAgFnSc6d1	parková
části	část	k1gFnSc6	část
náměstí	náměstí	k1gNnSc2	náměstí
byl	být	k5eAaImAgInS	být
17	[number]	k4	17
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1891	[number]	k4	1891
slavnostně	slavnostně	k6eAd1	slavnostně
otevřen	otevřít	k5eAaPmNgInS	otevřít
Německý	německý	k2eAgInSc1d1	německý
dům	dům	k1gInSc1	dům
jako	jako	k8xC	jako
ústřední	ústřední	k2eAgNnSc1d1	ústřední
místo	místo	k1gNnSc1	místo
spolkové	spolkový	k2eAgFnSc2d1	spolková
činnosti	činnost	k1gFnSc2	činnost
německých	německý	k2eAgMnPc2d1	německý
obyvatel	obyvatel	k1gMnPc2	obyvatel
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Historizující	historizující	k2eAgFnSc1d1	historizující
budova	budova	k1gFnSc1	budova
stála	stát	k5eAaImAgFnS	stát
v	v	k7c6	v
ose	osa	k1gFnSc6	osa
nově	nově	k6eAd1	nově
proražené	proražený	k2eAgFnPc4d1	proražená
ulice	ulice	k1gFnPc4	ulice
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgInPc4d1	dnešní
Rašínovy	Rašínův	k2eAgInPc4d1	Rašínův
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
propojila	propojit	k5eAaPmAgFnS	propojit
náměstí	náměstí	k1gNnSc4	náměstí
Svobody	svoboda	k1gFnSc2	svoboda
s	s	k7c7	s
Moravským	moravský	k2eAgInSc7d1	moravský
<g/>
,	,	kIx,	,
a	a	k8xC	a
tvořila	tvořit	k5eAaImAgFnS	tvořit
tak	tak	k6eAd1	tak
novou	nový	k2eAgFnSc4d1	nová
pohledovou	pohledový	k2eAgFnSc4d1	pohledová
dominantu	dominanta	k1gFnSc4	dominanta
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
parku	park	k1gInSc6	park
před	před	k7c7	před
ní	on	k3xPp3gFnSc7	on
vztyčen	vztyčen	k2eAgInSc1d1	vztyčen
monumentální	monumentální	k2eAgInSc1d1	monumentální
pomník	pomník	k1gInSc1	pomník
Josefa	Josef	k1gMnSc2	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
od	od	k7c2	od
sochaře	sochař	k1gMnSc2	sochař
Antona	Anton	k1gMnSc2	Anton
Breneka	Breneek	k1gMnSc2	Breneek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výrazně	výrazně	k6eAd1	výrazně
do	do	k7c2	do
náměstí	náměstí	k1gNnSc2	náměstí
zasáhlo	zasáhnout	k5eAaPmAgNnS	zasáhnout
zavedení	zavedení	k1gNnSc4	zavedení
tramvajové	tramvajový	k2eAgFnSc2d1	tramvajová
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
linka	linka	k1gFnSc1	linka
koněspřežky	koněspřežka	k1gFnSc2	koněspřežka
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
dráha	dráha	k1gFnSc1	dráha
od	od	k7c2	od
18	[number]	k4	18
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1869	[number]	k4	1869
vedla	vést	k5eAaImAgFnS	vést
od	od	k7c2	od
Kiosku	kiosek	k1gInSc2	kiosek
(	(	kIx(	(
<g/>
dnešního	dnešní	k2eAgInSc2d1	dnešní
předělu	předěl	k1gInSc2	předěl
západního	západní	k2eAgInSc2d1	západní
a	a	k8xC	a
východního	východní	k2eAgInSc2d1	východní
parku	park	k1gInSc2	park
Moravského	moravský	k2eAgNnSc2d1	Moravské
náměstí	náměstí	k1gNnSc2	náměstí
<g/>
)	)	kIx)	)
přes	přes	k7c4	přes
Žerotínovo	Žerotínův	k2eAgNnSc4d1	Žerotínovo
náměstí	náměstí	k1gNnSc4	náměstí
a	a	k8xC	a
po	po	k7c6	po
Husově	Husův	k2eAgFnSc6d1	Husova
ulici	ulice	k1gFnSc6	ulice
na	na	k7c4	na
Šilingrovo	Šilingrův	k2eAgNnSc4d1	Šilingrovo
náměstí	náměstí	k1gNnSc4	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
však	však	k9	však
byla	být	k5eAaImAgFnS	být
již	již	k6eAd1	již
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1872	[number]	k4	1872
jako	jako	k8xC	jako
nevýdělečná	výdělečný	k2eNgFnSc1d1	nevýdělečná
uzavřena	uzavřen	k2eAgFnSc1d1	uzavřena
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
2	[number]	k4	2
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1901	[number]	k4	1901
pak	pak	k9	pak
byla	být	k5eAaImAgFnS	být
zprovozněna	zprovoznit	k5eAaPmNgFnS	zprovoznit
první	první	k4xOgFnSc1	první
část	část	k1gFnSc1	část
dvoukolejné	dvoukolejný	k2eAgFnSc2d1	dvoukolejná
elektrické	elektrický	k2eAgFnSc2d1	elektrická
tramvajové	tramvajový	k2eAgFnSc2d1	tramvajová
tratě	trať	k1gFnSc2	trať
přes	přes	k7c4	přes
Moravské	moravský	k2eAgNnSc4d1	Moravské
náměstí	náměstí	k1gNnSc4	náměstí
<g/>
,	,	kIx,	,
od	od	k7c2	od
Rašínovy	Rašínův	k2eAgFnSc2d1	Rašínova
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
Kostelní	kostelní	k2eAgInSc1d1	kostelní
<g/>
)	)	kIx)	)
přes	přes	k7c4	přes
Komenského	Komenského	k2eAgNnSc4d1	Komenského
(	(	kIx(	(
<g/>
Eliščino	Eliščin	k2eAgNnSc4d1	Eliščino
<g/>
)	)	kIx)	)
náměstí	náměstí	k1gNnSc4	náměstí
na	na	k7c4	na
Obilní	obilní	k2eAgInSc4d1	obilní
trh	trh	k1gInSc4	trh
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
12	[number]	k4	12
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1903	[number]	k4	1903
byla	být	k5eAaImAgFnS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
i	i	k8xC	i
trať	trať	k1gFnSc4	trať
od	od	k7c2	od
Rašínovy	Rašínův	k2eAgFnSc2d1	Rašínova
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
již	již	k6eAd1	již
Liechtensteinovy	Liechtensteinův	k2eAgFnPc1d1	Liechtensteinova
<g/>
)	)	kIx)	)
směrem	směr	k1gInSc7	směr
ke	k	k7c3	k
Kiosku	kiosek	k1gInSc3	kiosek
a	a	k8xC	a
do	do	k7c2	do
Lidické	lidický	k2eAgFnSc2d1	Lidická
(	(	kIx(	(
<g/>
Nové	Nové	k2eAgFnSc2d1	Nové
<g/>
)	)	kIx)	)
ulice	ulice	k1gFnSc2	ulice
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
v	v	k7c6	v
časech	čas	k1gInPc6	čas
okupace	okupace	k1gFnSc2	okupace
nacistickým	nacistický	k2eAgNnSc7d1	nacistické
Německem	Německo	k1gNnSc7	Německo
<g/>
,	,	kIx,	,
od	od	k7c2	od
2	[number]	k4	2
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1939	[number]	k4	1939
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
provoz	provoz	k1gInSc1	provoz
i	i	k8xC	i
na	na	k7c6	na
nové	nový	k2eAgFnSc6d1	nová
trati	trať	k1gFnSc6	trať
směrem	směr	k1gInSc7	směr
do	do	k7c2	do
ulice	ulice	k1gFnSc2	ulice
Milady	Milada	k1gFnSc2	Milada
Horákové	Horáková	k1gFnSc2	Horáková
(	(	kIx(	(
<g/>
Francouzské	francouzský	k2eAgNnSc1d1	francouzské
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Od	od	k7c2	od
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
neslo	nést	k5eAaImAgNnS	nést
náměstí	náměstí	k1gNnSc1	náměstí
jméno	jméno	k1gNnSc1	jméno
Adolfa	Adolf	k1gMnSc2	Adolf
Hitlera	Hitler	k1gMnSc2	Hitler
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
po	po	k7c6	po
něm	on	k3xPp3gMnSc6	on
původně	původně	k6eAd1	původně
mělo	mít	k5eAaImAgNnS	mít
jmenovat	jmenovat	k5eAaImF	jmenovat
náměstí	náměstí	k1gNnSc1	náměstí
Svobody	svoboda	k1gFnSc2	svoboda
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
zvoleno	zvolen	k2eAgNnSc4d1	zvoleno
bylo	být	k5eAaImAgNnS	být
větší	veliký	k2eAgNnSc1d2	veliký
náměstí	náměstí	k1gNnSc1	náměstí
Moravské	moravský	k2eAgNnSc1d1	Moravské
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Lažanského	Lažanský	k2eAgInSc2d1	Lažanský
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1945	[number]	k4	1945
byl	být	k5eAaImAgInS	být
zbourán	zbourat	k5eAaPmNgInS	zbourat
značně	značně	k6eAd1	značně
poškozený	poškozený	k2eAgInSc1d1	poškozený
Německý	německý	k2eAgInSc1d1	německý
dům	dům	k1gInSc1	dům
<g/>
.	.	kIx.	.
</s>
<s>
Pomník	pomník	k1gInSc1	pomník
Josefa	Josef	k1gMnSc2	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
vzal	vzít	k5eAaPmAgMnS	vzít
za	za	k7c4	za
své	svůj	k3xOyFgNnSc4	svůj
už	už	k6eAd1	už
dříve	dříve	k6eAd2	dříve
–	–	k?	–
při	při	k7c6	při
prvním	první	k4xOgInSc6	první
výročí	výročí	k1gNnSc6	výročí
vzniku	vznik	k1gInSc2	vznik
Československa	Československo	k1gNnSc2	Československo
roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
byl	být	k5eAaImAgInS	být
stržen	strhnout	k5eAaPmNgInS	strhnout
československými	československý	k2eAgMnPc7d1	československý
legionáři	legionář	k1gMnPc7	legionář
<g/>
,	,	kIx,	,
úřady	úřad	k1gInPc7	úřad
jej	on	k3xPp3gMnSc4	on
již	již	k6eAd1	již
neobnovily	obnovit	k5eNaPmAgFnP	obnovit
a	a	k8xC	a
kamenný	kamenný	k2eAgInSc1d1	kamenný
podstavec	podstavec	k1gInSc1	podstavec
byl	být	k5eAaImAgInS	být
rozebrán	rozebrat	k5eAaPmNgInS	rozebrat
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
něj	on	k3xPp3gInSc2	on
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
později	pozdě	k6eAd2	pozdě
postaven	postavit	k5eAaPmNgInS	postavit
pomník	pomník	k1gInSc1	pomník
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc4	Masaryk
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
architekta	architekt	k1gMnSc2	architekt
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Fragnera	Fragner	k1gMnSc2	Fragner
a	a	k8xC	a
sochaře	sochař	k1gMnSc2	sochař
Vincence	Vincenc	k1gMnSc2	Vincenc
Makovského	Makovský	k1gMnSc2	Makovský
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgInSc1d1	základní
kámen	kámen	k1gInSc1	kámen
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
položen	položit	k5eAaPmNgInS	položit
původně	původně	k6eAd1	původně
k	k	k7c3	k
20	[number]	k4	20
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc2	výročí
vzniku	vznik	k1gInSc2	vznik
republiky	republika	k1gFnSc2	republika
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znemožnila	znemožnit	k5eAaPmAgFnS	znemožnit
okupace	okupace	k1gFnSc1	okupace
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
tedy	tedy	k9	tedy
slavnostně	slavnostně	k6eAd1	slavnostně
položen	položit	k5eAaPmNgInS	položit
27	[number]	k4	27
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
k	k	k7c3	k
realizaci	realizace	k1gFnSc3	realizace
pomníku	pomník	k1gInSc2	pomník
stejně	stejně	k6eAd1	stejně
nedošlo	dojít	k5eNaPmAgNnS	dojít
a	a	k8xC	a
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
byl	být	k5eAaImAgInS	být
odstraněn	odstranit	k5eAaPmNgInS	odstranit
i	i	k9	i
jeho	on	k3xPp3gInSc4	on
základní	základní	k2eAgInSc4d1	základní
kámen	kámen	k1gInSc4	kámen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
25	[number]	k4	25
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1946	[number]	k4	1946
bylo	být	k5eAaImAgNnS	být
náměstí	náměstí	k1gNnSc1	náměstí
pojmenováno	pojmenovat	k5eAaPmNgNnS	pojmenovat
po	po	k7c6	po
Rudé	rudý	k2eAgFnSc6d1	rudá
armádě	armáda	k1gFnSc6	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
místodržitelským	místodržitelský	k2eAgInSc7d1	místodržitelský
palácem	palác	k1gInSc7	palác
byl	být	k5eAaImAgInS	být
5	[number]	k4	5
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1950	[number]	k4	1950
odhalen	odhalen	k2eAgInSc1d1	odhalen
pomník	pomník	k1gInSc1	pomník
s	s	k7c7	s
portrétní	portrétní	k2eAgFnSc7d1	portrétní
bustou	busta	k1gFnSc7	busta
maršála	maršál	k1gMnSc2	maršál
Malinovského	Malinovský	k2eAgMnSc2d1	Malinovský
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
přenesený	přenesený	k2eAgInSc4d1	přenesený
na	na	k7c4	na
Malinovského	Malinovský	k2eAgNnSc2d1	Malinovské
náměstí	náměstí	k1gNnSc2	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
<g/>
,	,	kIx,	,
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
10	[number]	k4	10
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc4	výročí
osvobození	osvobození	k1gNnSc2	osvobození
Brna	Brno	k1gNnSc2	Brno
byl	být	k5eAaImAgInS	být
26	[number]	k4	26
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1955	[number]	k4	1955
slavnostně	slavnostně	k6eAd1	slavnostně
odhalen	odhalit	k5eAaPmNgInS	odhalit
monumentální	monumentální	k2eAgInSc1d1	monumentální
památník	památník	k1gInSc1	památník
osvobození	osvobození	k1gNnSc2	osvobození
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
parku	park	k1gInSc2	park
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
tvorbě	tvorba	k1gFnSc6	tvorba
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
architekt	architekt	k1gMnSc1	architekt
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Fuchs	Fuchs	k1gMnSc1	Fuchs
se	s	k7c7	s
sochařem	sochař	k1gMnSc7	sochař
Vincencem	Vincenc	k1gMnSc7	Vincenc
Makovským	Makovský	k1gMnSc7	Makovský
a	a	k8xC	a
sokl	sokl	k1gInSc4	sokl
pro	pro	k7c4	pro
sochu	socha	k1gFnSc4	socha
rudoarmějce	rudoarmějec	k1gMnSc2	rudoarmějec
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
Antonín	Antonín	k1gMnSc1	Antonín
Kurial	Kurial	k1gMnSc1	Kurial
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
byla	být	k5eAaImAgFnS	být
kompletně	kompletně	k6eAd1	kompletně
změněna	změnit	k5eAaPmNgFnS	změnit
úprava	úprava	k1gFnSc1	úprava
zbývající	zbývající	k2eAgFnSc2d1	zbývající
parkové	parkový	k2eAgFnSc2d1	parková
části	část	k1gFnSc2	část
náměstí	náměstí	k1gNnSc2	náměstí
a	a	k8xC	a
na	na	k7c6	na
uprázdněném	uprázdněný	k2eAgNnSc6d1	uprázdněné
místě	místo	k1gNnSc6	místo
po	po	k7c6	po
nerealizovaném	realizovaný	k2eNgInSc6d1	nerealizovaný
Masarykově	Masarykův	k2eAgInSc6d1	Masarykův
pomníku	pomník	k1gInSc6	pomník
přibyla	přibýt	k5eAaPmAgFnS	přibýt
kašna	kašna	k1gFnSc1	kašna
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
pětiúhelníku	pětiúhelník	k1gInSc2	pětiúhelník
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
po	po	k7c6	po
zemi	zem	k1gFnSc6	zem
vybíhaly	vybíhat	k5eAaImAgInP	vybíhat
cípy	cíp	k1gInPc1	cíp
původně	původně	k6eAd1	původně
červeného	červený	k2eAgInSc2d1	červený
asfaltu	asfalt	k1gInSc2	asfalt
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
celkově	celkově	k6eAd1	celkově
vytvářelo	vytvářet	k5eAaImAgNnS	vytvářet
tvar	tvar	k1gInSc4	tvar
rudé	rudý	k2eAgFnSc2d1	rudá
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dnešní	dnešní	k2eAgInSc1d1	dnešní
název	název	k1gInSc1	název
má	mít	k5eAaImIp3nS	mít
Moravské	moravský	k2eAgNnSc4d1	Moravské
náměstí	náměstí	k1gNnSc4	náměstí
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
2009	[number]	k4	2009
<g/>
–	–	k?	–
<g/>
2010	[number]	k4	2010
probíhala	probíhat	k5eAaImAgFnS	probíhat
celková	celkový	k2eAgFnSc1d1	celková
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
jižní	jižní	k2eAgFnSc2d1	jižní
části	část	k1gFnSc2	část
náměstí	náměstí	k1gNnSc2	náměstí
před	před	k7c7	před
místodržitelským	místodržitelský	k2eAgInSc7d1	místodržitelský
palácem	palác	k1gInSc7	palác
a	a	k8xC	a
kostelem	kostel	k1gInSc7	kostel
sv.	sv.	kA	sv.
Tomáše	Tomáš	k1gMnSc2	Tomáš
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
projektu	projekt	k1gInSc2	projekt
byl	být	k5eAaImAgMnS	být
Petr	Petr	k1gMnSc1	Petr
Hrůša	Hrůša	k1gMnSc1	Hrůša
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Lukášem	Lukáš	k1gMnSc7	Lukáš
Peckou	Pecka	k1gMnSc7	Pecka
a	a	k8xC	a
Vítem	Vít	k1gMnSc7	Vít
Zenklem	Zenkl	k1gInSc7	Zenkl
z	z	k7c2	z
Ateliéru	ateliér	k1gInSc2	ateliér
Brno	Brno	k1gNnSc1	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
opravě	oprava	k1gFnSc6	oprava
bylo	být	k5eAaImAgNnS	být
předáno	předat	k5eAaPmNgNnS	předat
veřejnosti	veřejnost	k1gFnPc4	veřejnost
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
zde	zde	k6eAd1	zde
zrušeno	zrušen	k2eAgNnSc4d1	zrušeno
dosavadní	dosavadní	k2eAgNnSc4d1	dosavadní
parkoviště	parkoviště	k1gNnSc4	parkoviště
<g/>
,	,	kIx,	,
dlažba	dlažba	k1gFnSc1	dlažba
byla	být	k5eAaImAgFnS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
do	do	k7c2	do
geometrického	geometrický	k2eAgInSc2d1	geometrický
rastru	rastr	k1gInSc2	rastr
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
byl	být	k5eAaImAgInS	být
rozmístěn	rozmístit	k5eAaPmNgInS	rozmístit
i	i	k9	i
mobiliář	mobiliář	k1gInSc1	mobiliář
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
prostoru	prostor	k1gInSc2	prostor
pak	pak	k6eAd1	pak
byly	být	k5eAaImAgInP	být
rozmístěny	rozmístěn	k2eAgInPc1d1	rozmístěn
prvky	prvek	k1gInPc1	prvek
zpodobňující	zpodobňující	k2eAgInPc1d1	zpodobňující
čtyři	čtyři	k4xCgFnPc1	čtyři
Platónovy	Platónův	k2eAgFnPc1d1	Platónova
ctnosti	ctnost	k1gFnPc1	ctnost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
by	by	kYmCp3nP	by
město	město	k1gNnSc1	město
mělo	mít	k5eAaImAgNnS	mít
mít	mít	k5eAaImF	mít
<g/>
:	:	kIx,	:
umírněnost	umírněnost	k1gFnSc1	umírněnost
<g/>
,	,	kIx,	,
moudrost	moudrost	k1gFnSc1	moudrost
<g/>
,	,	kIx,	,
spravedlnost	spravedlnost	k1gFnSc1	spravedlnost
a	a	k8xC	a
statečnost	statečnost	k1gFnSc1	statečnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Spravedlnost	spravedlnost	k1gFnSc1	spravedlnost
ztvárnila	ztvárnit	k5eAaPmAgFnS	ztvárnit
měděná	měděný	k2eAgFnSc1d1	měděná
socha	socha	k1gFnSc1	socha
muže	muž	k1gMnSc2	muž
zvedajícího	zvedající	k2eAgInSc2d1	zvedající
kvádr	kvádr	k1gInSc1	kvádr
(	(	kIx(	(
<g/>
lidově	lidově	k6eAd1	lidově
zvaná	zvaný	k2eAgFnSc1d1	zvaná
"	"	kIx"	"
<g/>
exekutor	exekutor	k1gMnSc1	exekutor
odnášející	odnášející	k2eAgFnSc4d1	odnášející
lednici	lednice	k1gFnSc4	lednice
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
od	od	k7c2	od
Maria	Mario	k1gMnSc2	Mario
Kotrby	kotrba	k1gFnSc2	kotrba
<g/>
,	,	kIx,	,
instalovaná	instalovaný	k2eAgFnSc1d1	instalovaná
31	[number]	k4	31
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2010	[number]	k4	2010
a	a	k8xC	a
slavnostně	slavnostně	k6eAd1	slavnostně
odhalená	odhalený	k2eAgFnSc1d1	odhalená
9	[number]	k4	9
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
na	na	k7c6	na
piazzetě	piazzeta	k1gFnSc6	piazzeta
před	před	k7c7	před
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
správním	správní	k2eAgInSc7d1	správní
soudem	soud	k1gInSc7	soud
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Umírněnost	umírněnost	k1gFnSc1	umírněnost
je	být	k5eAaImIp3nS	být
představována	představován	k2eAgNnPc4d1	představováno
sochařsky	sochařsky	k6eAd1	sochařsky
lapidární	lapidární	k2eAgFnSc7d1	lapidární
hranolovou	hranolový	k2eAgFnSc7d1	hranolová
kašnou	kašna	k1gFnSc7	kašna
<g/>
,	,	kIx,	,
situovanou	situovaný	k2eAgFnSc4d1	situovaná
osově	osově	k6eAd1	osově
naproti	naproti	k7c3	naproti
Kotrbově	Kotrbův	k2eAgFnSc3d1	Kotrbova
soše	socha	k1gFnSc3	socha
<g/>
,	,	kIx,	,
před	před	k7c7	před
místodržitelským	místodržitelský	k2eAgInSc7d1	místodržitelský
palácem	palác	k1gInSc7	palác
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Moudrost	moudrost	k1gFnSc1	moudrost
či	či	k8xC	či
prozíravost	prozíravost	k1gFnSc1	prozíravost
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
bronzový	bronzový	k2eAgInSc4d1	bronzový
model	model	k1gInSc4	model
města	město	k1gNnSc2	město
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1645	[number]	k4	1645
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
úspěšně	úspěšně	k6eAd1	úspěšně
čelilo	čelit	k5eAaImAgNnS	čelit
obléhání	obléhání	k1gNnSc1	obléhání
švédského	švédský	k2eAgNnSc2d1	švédské
vojska	vojsko	k1gNnSc2	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Plastický	plastický	k2eAgInSc1d1	plastický
model	model	k1gInSc1	model
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
předlohou	předloha	k1gFnSc7	předloha
byla	být	k5eAaImAgFnS	být
veduta	veduta	k1gFnSc1	veduta
Hanse	Hans	k1gMnSc2	Hans
Benno	Benno	k6eAd1	Benno
Bayera	Bayer	k1gMnSc4	Bayer
a	a	k8xC	a
Hanse	Hans	k1gMnSc4	Hans
Jörga	Jörg	k1gMnSc4	Jörg
Zeissera	Zeisser	k1gMnSc4	Zeisser
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
umístěn	umístit	k5eAaPmNgMnS	umístit
na	na	k7c6	na
kvádru	kvádr	k1gInSc6	kvádr
při	při	k7c6	při
jižní	jižní	k2eAgFnSc6d1	jižní
straně	strana	k1gFnSc6	strana
náměstí	náměstí	k1gNnSc2	náměstí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Původně	původně	k6eAd1	původně
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
vypsána	vypsán	k2eAgFnSc1d1	vypsána
soutěž	soutěž	k1gFnSc1	soutěž
na	na	k7c6	na
ztvárnění	ztvárnění	k1gNnSc6	ztvárnění
čtvrté	čtvrtá	k1gFnSc2	čtvrtá
ctnosti	ctnost	k1gFnSc2	ctnost
formou	forma	k1gFnSc7	forma
jezdecké	jezdecký	k2eAgFnSc2d1	jezdecká
sochy	socha	k1gFnSc2	socha
Jošta	Jošt	k1gMnSc2	Jošt
Lucemburského	lucemburský	k2eAgMnSc2d1	lucemburský
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
hrobka	hrobka	k1gFnSc1	hrobka
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
právě	právě	k9	právě
ve	v	k7c6	v
zdejším	zdejší	k2eAgInSc6d1	zdejší
kostele	kostel	k1gInSc6	kostel
sv.	sv.	kA	sv.
Tomáše	Tomáš	k1gMnSc2	Tomáš
(	(	kIx(	(
<g/>
a	a	k8xC	a
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1999	[number]	k4	1999
byla	být	k5eAaImAgFnS	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hodnotící	hodnotící	k2eAgFnSc1d1	hodnotící
komise	komise	k1gFnSc1	komise
vybrala	vybrat	k5eAaPmAgFnS	vybrat
z	z	k7c2	z
téměř	téměř	k6eAd1	téměř
50	[number]	k4	50
návrhů	návrh	k1gInPc2	návrh
ten	ten	k3xDgMnSc1	ten
od	od	k7c2	od
Maria	Mario	k1gMnSc2	Mario
Kotrby	kotrba	k1gFnSc2	kotrba
<g/>
,	,	kIx,	,
autora	autor	k1gMnSc2	autor
alegorie	alegorie	k1gFnSc2	alegorie
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
Rada	rada	k1gFnSc1	rada
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
se	se	k3xPyFc4	se
s	s	k7c7	s
výběrem	výběr	k1gInSc7	výběr
neztotožnila	ztotožnit	k5eNaPmAgFnS	ztotožnit
<g/>
,	,	kIx,	,
realizaci	realizace	k1gFnSc4	realizace
odložila	odložit	k5eAaPmAgFnS	odložit
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
soutěž	soutěž	k1gFnSc4	soutěž
novou	nový	k2eAgFnSc4d1	nová
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
21	[number]	k4	21
návrhů	návrh	k1gInPc2	návrh
pak	pak	k6eAd1	pak
byly	být	k5eAaImAgFnP	být
komisí	komise	k1gFnSc7	komise
vybrány	vybrán	k2eAgFnPc1d1	vybrána
tři	tři	k4xCgFnPc1	tři
nejlepší	dobrý	k2eAgFnPc1d3	nejlepší
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
rada	rada	k1gFnSc1	rada
odsouhlasila	odsouhlasit	k5eAaPmAgFnS	odsouhlasit
model	model	k1gInSc4	model
od	od	k7c2	od
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Róny	Róna	k1gMnSc2	Róna
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
pak	pak	k6eAd1	pak
na	na	k7c6	na
soše	socha	k1gFnSc6	socha
pracoval	pracovat	k5eAaImAgMnS	pracovat
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
vlastního	vlastní	k2eAgNnSc2d1	vlastní
vyjádření	vyjádření	k1gNnSc2	vyjádření
však	však	k9	však
netvořil	tvořit	k5eNaImAgMnS	tvořit
portrétní	portrétní	k2eAgFnSc4d1	portrétní
sochu	socha	k1gFnSc4	socha
Jošta	Jošt	k1gMnSc2	Jošt
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
především	především	k6eAd1	především
stylizovaného	stylizovaný	k2eAgMnSc4d1	stylizovaný
středověkého	středověký	k2eAgMnSc4d1	středověký
rytíře	rytíř	k1gMnSc4	rytíř
jako	jako	k8xS	jako
alegorii	alegorie	k1gFnSc4	alegorie
statečnosti	statečnost	k1gFnSc2	statečnost
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
instalaci	instalace	k1gFnSc6	instalace
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
byla	být	k5eAaImAgFnS	být
slavnostně	slavnostně	k6eAd1	slavnostně
odhalena	odhalen	k2eAgFnSc1d1	odhalena
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
letech	let	k1gInPc6	let
2012	[number]	k4	2012
<g/>
–	–	k?	–
<g/>
2013	[number]	k4	2013
došlo	dojít	k5eAaPmAgNnS	dojít
i	i	k9	i
na	na	k7c4	na
opravu	oprava	k1gFnSc4	oprava
parkové	parkový	k2eAgFnSc2d1	parková
části	část	k1gFnSc2	část
náměstí	náměstí	k1gNnSc2	náměstí
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pomníku	pomník	k1gInSc2	pomník
osvobození	osvobození	k1gNnSc2	osvobození
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
okolí	okolí	k1gNnSc4	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Socha	socha	k1gFnSc1	socha
rudoarmějce	rudoarmějec	k1gMnSc2	rudoarmějec
byla	být	k5eAaImAgFnS	být
11	[number]	k4	11
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2012	[number]	k4	2012
demontována	demontován	k2eAgFnSc1d1	demontována
a	a	k8xC	a
odvezena	odvezen	k2eAgFnSc1d1	odvezena
do	do	k7c2	do
Buštěhradu	Buštěhrad	k1gInSc2	Buštěhrad
k	k	k7c3	k
restaurátorskému	restaurátorský	k2eAgNnSc3d1	restaurátorské
ošetření	ošetření	k1gNnSc3	ošetření
<g/>
.	.	kIx.	.
</s>
<s>
Obnoveny	obnoven	k2eAgInPc1d1	obnoven
byly	být	k5eAaImAgInP	být
trávníky	trávník	k1gInPc1	trávník
i	i	k8xC	i
mobiliář	mobiliář	k1gInSc1	mobiliář
<g/>
,	,	kIx,	,
k	k	k7c3	k
zásadním	zásadní	k2eAgFnPc3d1	zásadní
změnám	změna	k1gFnPc3	změna
v	v	k7c6	v
prostorovém	prostorový	k2eAgNnSc6d1	prostorové
řešení	řešení	k1gNnSc6	řešení
však	však	k9	však
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2016	[number]	k4	2016
městská	městský	k2eAgFnSc1d1	městská
část	část	k1gFnSc1	část
Brno-střed	Brnotřed	k1gInSc4	Brno-střed
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
architektonicko-urbanistickou	architektonickorbanistický	k2eAgFnSc4d1	architektonicko-urbanistická
a	a	k8xC	a
krajinářskou	krajinářský	k2eAgFnSc4d1	krajinářská
soutěž	soutěž	k1gFnSc4	soutěž
o	o	k7c4	o
návrh	návrh	k1gInSc4	návrh
parku	park	k1gInSc2	park
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
,	,	kIx,	,
v	v	k7c6	v
části	část	k1gFnSc6	část
kolem	kolem	k7c2	kolem
pěticípé	pěticípý	k2eAgFnSc2d1	pěticípá
fontány	fontána	k1gFnSc2	fontána
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
prosince	prosinec	k1gInSc2	prosinec
byly	být	k5eAaImAgInP	být
vyhlášeny	vyhlášen	k2eAgInPc1d1	vyhlášen
výsledky	výsledek	k1gInPc1	výsledek
<g/>
,	,	kIx,	,
porota	porota	k1gFnSc1	porota
z	z	k7c2	z
20	[number]	k4	20
návrhů	návrh	k1gInPc2	návrh
vybrala	vybrat	k5eAaPmAgFnS	vybrat
záměr	záměr	k1gInSc4	záměr
kanceláře	kancelář	k1gFnSc2	kancelář
Consequence	Consequence	k1gFnSc1	Consequence
forma	forma	k1gFnSc1	forma
<g/>
,	,	kIx,	,
se	s	k7c7	s
záměrem	záměr	k1gInSc7	záměr
jeho	jeho	k3xOp3gFnSc2	jeho
realizace	realizace	k1gFnSc2	realizace
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vývoj	vývoj	k1gInSc1	vývoj
názvu	název	k1gInSc2	název
==	==	k?	==
</s>
</p>
<p>
<s>
počátkem	počátkem	k7c2	počátkem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
–	–	k?	–
Dikasterialplatz	Dikasterialplatz	k1gMnSc1	Dikasterialplatz
</s>
</p>
<p>
<s>
19	[number]	k4	19
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1860	[number]	k4	1860
–	–	k?	–
Lažanskýplatz	Lažanskýplatz	k1gMnSc1	Lažanskýplatz
</s>
</p>
<p>
<s>
15	[number]	k4	15
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1915	[number]	k4	1915
–	–	k?	–
Kaiser-Josef-Platz	Kaiser-Josef-Platz	k1gMnSc1	Kaiser-Josef-Platz
+	+	kIx~	+
Am	Am	k1gFnSc1	Am
Kiosk	kiosk	k1gInSc1	kiosk
</s>
</p>
<p>
<s>
30	[number]	k4	30
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1918	[number]	k4	1918
–	–	k?	–
Lažanského	Lažanský	k2eAgNnSc2d1	Lažanský
náměstí	náměstí	k1gNnSc2	náměstí
+	+	kIx~	+
Kiosk	kiosk	k1gInSc1	kiosk
</s>
</p>
<p>
<s>
6	[number]	k4	6
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1935	[number]	k4	1935
–	–	k?	–
Lažanského	Lažanský	k2eAgNnSc2d1	Lažanský
náměstí	náměstí	k1gNnSc2	náměstí
+	+	kIx~	+
Švehlova	Švehlův	k2eAgFnSc1d1	Švehlova
</s>
</p>
<p>
<s>
17	[number]	k4	17
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1939	[number]	k4	1939
–	–	k?	–
Lažanskýplatz	Lažanskýplatz	k1gMnSc1	Lažanskýplatz
+	+	kIx~	+
Švehlagasse	Švehlagasse	k1gFnSc1	Švehlagasse
</s>
</p>
<p>
<s>
19	[number]	k4	19
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1939	[number]	k4	1939
–	–	k?	–
Adolf-Hitler-Platz	Adolf-Hitler-Platz	k1gMnSc1	Adolf-Hitler-Platz
+	+	kIx~	+
Am	Am	k1gFnSc1	Am
Kiosk	kiosk	k1gInSc1	kiosk
</s>
</p>
<p>
<s>
10	[number]	k4	10
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
–	–	k?	–
Lažanského	Lažanský	k2eAgNnSc2d1	Lažanský
náměstí	náměstí	k1gNnSc2	náměstí
+	+	kIx~	+
Kiosk	kiosk	k1gInSc1	kiosk
</s>
</p>
<p>
<s>
25	[number]	k4	25
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1946	[number]	k4	1946
–	–	k?	–
náměstí	náměstí	k1gNnSc4	náměstí
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
</s>
</p>
<p>
<s>
25	[number]	k4	25
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1990	[number]	k4	1990
–	–	k?	–
Moravské	moravský	k2eAgNnSc1d1	Moravské
náměstí	náměstí	k1gNnSc1	náměstí
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Leopold	Leopold	k1gMnSc1	Leopold
Lažanský	Lažanský	k2eAgMnSc1d1	Lažanský
z	z	k7c2	z
Bukové	bukový	k2eAgFnSc2d1	Buková
</s>
</p>
<p>
<s>
Moravské	moravský	k2eAgNnSc1d1	Moravské
náměstí	náměstí	k1gNnSc1	náměstí
(	(	kIx(	(
<g/>
dopravní	dopravní	k2eAgInSc1d1	dopravní
uzel	uzel	k1gInSc1	uzel
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Moravské	moravský	k2eAgFnSc2d1	Moravská
náměstí	náměstí	k1gNnSc4	náměstí
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Moravské	moravský	k2eAgNnSc1d1	Moravské
náměstí	náměstí	k1gNnSc1	náměstí
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Encyklopedie	encyklopedie	k1gFnSc2	encyklopedie
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
</s>
</p>
