<s>
Alpské	alpský	k2eAgNnSc1d1	alpské
lyžování	lyžování	k1gNnSc1	lyžování
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
též	též	k9	též
zvané	zvaný	k2eAgNnSc1d1	zvané
sjezdové	sjezdový	k2eAgNnSc1d1	sjezdové
lyžování	lyžování	k1gNnSc1	lyžování
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
odvětví	odvětví	k1gNnSc1	odvětví
závodního	závodní	k2eAgNnSc2d1	závodní
lyžování	lyžování	k1gNnSc2	lyžování
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
závodník	závodník	k1gMnSc1	závodník
snaží	snažit	k5eAaImIp3nS	snažit
o	o	k7c6	o
projetí	projetí	k1gNnSc6	projetí
dané	daný	k2eAgFnSc2d1	daná
trati	trať	k1gFnSc2	trať
ze	z	k7c2	z
svahu	svah	k1gInSc2	svah
dolů	dolů	k6eAd1	dolů
v	v	k7c6	v
co	co	k9	co
nejkratším	krátký	k2eAgInSc6d3	nejkratší
čase	čas	k1gInSc6	čas
<g/>
.	.	kIx.	.
</s>
