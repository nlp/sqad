<s>
Alpské	alpský	k2eAgNnSc1d1	alpské
lyžování	lyžování	k1gNnSc1	lyžování
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
též	též	k9	též
zvané	zvaný	k2eAgNnSc1d1	zvané
sjezdové	sjezdový	k2eAgNnSc1d1	sjezdové
lyžování	lyžování	k1gNnSc1	lyžování
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
odvětví	odvětví	k1gNnSc1	odvětví
závodního	závodní	k2eAgNnSc2d1	závodní
lyžování	lyžování	k1gNnSc2	lyžování
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
závodník	závodník	k1gMnSc1	závodník
snaží	snažit	k5eAaImIp3nS	snažit
o	o	k7c6	o
projetí	projetí	k1gNnSc6	projetí
dané	daný	k2eAgFnSc2d1	daná
trati	trať	k1gFnSc2	trať
ze	z	k7c2	z
svahu	svah	k1gInSc2	svah
dolů	dolů	k6eAd1	dolů
v	v	k7c6	v
co	co	k9	co
nejkratším	krátký	k2eAgInSc6d3	nejkratší
čase	čas	k1gInSc6	čas
<g/>
.	.	kIx.	.
</s>
<s>
Trať	trať	k1gFnSc1	trať
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
svahu	svah	k1gInSc6	svah
vyznačena	vyznačen	k2eAgFnSc1d1	vyznačena
bránami	brána	k1gFnPc7	brána
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
závodníci	závodník	k1gMnPc1	závodník
musí	muset	k5eAaImIp3nP	muset
projet	projet	k5eAaPmF	projet
způsobem	způsob	k1gInSc7	způsob
odpovídajícím	odpovídající	k2eAgInSc7d1	odpovídající
pravidlům	pravidlo	k1gNnPc3	pravidlo
konkrétní	konkrétní	k2eAgFnSc2d1	konkrétní
disciplíny	disciplína	k1gFnPc1	disciplína
<g/>
.	.	kIx.	.
</s>
<s>
Slalom	slalom	k1gInSc1	slalom
se	se	k3xPyFc4	se
jezdí	jezdit	k5eAaImIp3nS	jezdit
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
nejkratší	krátký	k2eAgFnSc1d3	nejkratší
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
disciplín	disciplína	k1gFnPc2	disciplína
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
brány	brána	k1gFnPc4	brána
nejblíže	blízce	k6eAd3	blízce
u	u	k7c2	u
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Dosahované	dosahovaný	k2eAgFnPc1d1	dosahovaná
rychlosti	rychlost	k1gFnPc1	rychlost
jsou	být	k5eAaImIp3nP	být
nejnižší	nízký	k2eAgFnPc1d3	nejnižší
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
disciplín	disciplína	k1gFnPc2	disciplína
a	a	k8xC	a
závodníci	závodník	k1gMnPc1	závodník
trať	trať	k1gFnSc4	trať
projíždějí	projíždět	k5eAaImIp3nP	projíždět
krátkými	krátký	k2eAgInPc7d1	krátký
oblouky	oblouk	k1gInPc7	oblouk
v	v	k7c6	v
rychlém	rychlý	k2eAgInSc6d1	rychlý
sledu	sled	k1gInSc6	sled
<g/>
.	.	kIx.	.
</s>
<s>
Závod	závod	k1gInSc1	závod
se	se	k3xPyFc4	se
jezdí	jezdit	k5eAaImIp3nS	jezdit
dvoukolovým	dvoukolový	k2eAgInSc7d1	dvoukolový
systémem	systém	k1gInSc7	systém
na	na	k7c6	na
stejném	stejný	k2eAgInSc6d1	stejný
svahu	svah	k1gInSc6	svah
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
dni	den	k1gInSc6	den
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
každé	každý	k3xTgNnSc4	každý
kolo	kolo	k1gNnSc4	kolo
je	být	k5eAaImIp3nS	být
postavena	postaven	k2eAgFnSc1d1	postavena
jiná	jiný	k2eAgFnSc1d1	jiná
trať	trať	k1gFnSc1	trať
<g/>
.	.	kIx.	.
</s>
<s>
Pořadí	pořadí	k1gNnSc1	pořadí
je	být	k5eAaImIp3nS	být
určeno	určit	k5eAaPmNgNnS	určit
součtem	součet	k1gInSc7	součet
časů	čas	k1gInPc2	čas
z	z	k7c2	z
obou	dva	k4xCgNnPc2	dva
kol	kolo	k1gNnPc2	kolo
<g/>
,	,	kIx,	,
vítězem	vítěz	k1gMnSc7	vítěz
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
závodník	závodník	k1gMnSc1	závodník
s	s	k7c7	s
nejrychlejším	rychlý	k2eAgInSc7d3	nejrychlejší
celkovým	celkový	k2eAgInSc7d1	celkový
časem	čas	k1gInSc7	čas
<g/>
.	.	kIx.	.
</s>
<s>
Doporučená	doporučený	k2eAgFnSc1d1	Doporučená
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
tyčí	tyčit	k5eAaImIp3nP	tyčit
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
13	[number]	k4	13
metrů	metr	k1gInPc2	metr
(	(	kIx(	(
<g/>
v	v	k7c6	v
případě	případ	k1gInSc6	případ
tradičních	tradiční	k2eAgInPc2d1	tradiční
oblouků	oblouk	k1gInPc2	oblouk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sjezd	sjezd	k1gInSc1	sjezd
je	být	k5eAaImIp3nS	být
nejrychlejší	rychlý	k2eAgFnSc7d3	nejrychlejší
disciplínou	disciplína	k1gFnSc7	disciplína
alpského	alpský	k2eAgNnSc2d1	alpské
lyžování	lyžování	k1gNnSc2	lyžování
<g/>
.	.	kIx.	.
</s>
<s>
Trať	trať	k1gFnSc1	trať
je	být	k5eAaImIp3nS	být
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
disciplín	disciplína	k1gFnPc2	disciplína
a	a	k8xC	a
brány	brána	k1gFnSc2	brána
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
jen	jen	k9	jen
ohraničují	ohraničovat	k5eAaImIp3nP	ohraničovat
trať	trať	k1gFnSc4	trať
<g/>
.	.	kIx.	.
</s>
<s>
Sjezd	sjezd	k1gInSc1	sjezd
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
náročné	náročný	k2eAgInPc4d1	náročný
oblouky	oblouk	k1gInPc4	oblouk
ve	v	k7c6	v
vysokých	vysoký	k2eAgFnPc6d1	vysoká
rychlostech	rychlost	k1gFnPc6	rychlost
<g/>
,	,	kIx,	,
skoky	skok	k1gInPc4	skok
a	a	k8xC	a
rovné	rovný	k2eAgFnPc4d1	rovná
pasáže	pasáž	k1gFnPc4	pasáž
<g/>
.	.	kIx.	.
</s>
<s>
Závod	závod	k1gInSc1	závod
se	se	k3xPyFc4	se
jezdí	jezdit	k5eAaImIp3nS	jezdit
jednokolovým	jednokolový	k2eAgInSc7d1	jednokolový
systémem	systém	k1gInSc7	systém
<g/>
.	.	kIx.	.
</s>
<s>
Vítězem	vítěz	k1gMnSc7	vítěz
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
závodník	závodník	k1gMnSc1	závodník
s	s	k7c7	s
nejrychlejším	rychlý	k2eAgInSc7d3	nejrychlejší
časem	čas	k1gInSc7	čas
<g/>
.	.	kIx.	.
</s>
<s>
Alpská	alpský	k2eAgFnSc1d1	alpská
kombinace	kombinace	k1gFnSc1	kombinace
(	(	kIx(	(
<g/>
kombinace	kombinace	k1gFnSc1	kombinace
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
jednoho	jeden	k4xCgNnSc2	jeden
kola	kolo	k1gNnSc2	kolo
sjezdu	sjezd	k1gInSc2	sjezd
a	a	k8xC	a
jednoho	jeden	k4xCgNnSc2	jeden
kola	kolo	k1gNnSc2	kolo
slalomu	slalom	k1gInSc2	slalom
(	(	kIx(	(
<g/>
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
bývala	bývat	k5eAaImAgNnP	bývat
slalomová	slalomový	k2eAgNnPc1d1	slalomové
kola	kolo	k1gNnPc1	kolo
dvě	dva	k4xCgFnPc4	dva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
časy	čas	k1gInPc1	čas
jsou	být	k5eAaImIp3nP	být
sečteny	sečten	k2eAgInPc1d1	sečten
a	a	k8xC	a
vítězem	vítěz	k1gMnSc7	vítěz
je	být	k5eAaImIp3nS	být
závodník	závodník	k1gMnSc1	závodník
s	s	k7c7	s
nejkratším	krátký	k2eAgInSc7d3	nejkratší
celkovým	celkový	k2eAgInSc7d1	celkový
časem	čas	k1gInSc7	čas
<g/>
.	.	kIx.	.
</s>
<s>
Sjezd	sjezd	k1gInSc1	sjezd
a	a	k8xC	a
slalom	slalom	k1gInSc1	slalom
do	do	k7c2	do
kombinace	kombinace	k1gFnSc2	kombinace
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
odděleně	odděleně	k6eAd1	odděleně
od	od	k7c2	od
samostatných	samostatný	k2eAgFnPc2d1	samostatná
disciplín	disciplína	k1gFnPc2	disciplína
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
kratší	krátký	k2eAgInPc1d2	kratší
<g/>
.	.	kIx.	.
</s>
<s>
Kombinaci	kombinace	k1gFnSc4	kombinace
dělíme	dělit	k5eAaImIp1nP	dělit
na	na	k7c4	na
tyto	tento	k3xDgFnPc4	tento
tři	tři	k4xCgFnPc4	tři
podkategorie	podkategorie	k1gFnPc4	podkategorie
<g/>
:	:	kIx,	:
Super	super	k2eAgFnPc4d1	super
kombinace	kombinace	k1gFnPc4	kombinace
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
výsledek	výsledek	k1gInSc1	výsledek
jednoho	jeden	k4xCgNnSc2	jeden
kola	kolo	k1gNnSc2	kolo
slalomu	slalom	k1gInSc2	slalom
provedeného	provedený	k2eAgInSc2d1	provedený
podle	podle	k7c2	podle
pravidel	pravidlo	k1gNnPc2	pravidlo
této	tento	k3xDgFnSc2	tento
discipliny	disciplina	k1gFnSc2	disciplina
a	a	k8xC	a
sjezdu	sjezd	k1gInSc2	sjezd
nebo	nebo	k8xC	nebo
super	super	k1gInSc2	super
G.	G.	kA	G.
Závod	závod	k1gInSc1	závod
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
dvou	dva	k4xCgNnPc2	dva
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Sjezd	sjezd	k1gInSc1	sjezd
nebo	nebo	k8xC	nebo
super	super	k1gInSc1	super
G	G	kA	G
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
uskutečnit	uskutečnit	k5eAaPmF	uskutečnit
na	na	k7c4	na
trati	trať	k1gFnPc4	trať
homologované	homologovaný	k2eAgFnPc4d1	homologovaná
specificky	specificky	k6eAd1	specificky
pro	pro	k7c4	pro
tyto	tento	k3xDgFnPc4	tento
discipliny	disciplina	k1gFnPc4	disciplina
<g/>
.	.	kIx.	.
</s>
<s>
Slalom	slalom	k1gInSc1	slalom
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
konat	konat	k5eAaImF	konat
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
trati	trať	k1gFnSc6	trať
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgNnPc1	dva
kola	kolo	k1gNnPc1	kolo
se	se	k3xPyFc4	se
mají	mít	k5eAaImIp3nP	mít
uskutečnit	uskutečnit	k5eAaPmF	uskutečnit
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
dni	den	k1gInSc6	den
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Výjimky	výjimka	k1gFnPc1	výjimka
jsou	být	k5eAaImIp3nP	být
možné	možný	k2eAgFnPc1d1	možná
na	na	k7c6	na
základě	základ	k1gInSc6	základ
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
jury	jura	k1gFnSc2	jura
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Klasická	klasický	k2eAgFnSc1d1	klasická
kombinace	kombinace	k1gFnSc1	kombinace
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
výsledek	výsledek	k1gInSc1	výsledek
závodu	závod	k1gInSc2	závod
ve	v	k7c6	v
sjezdu	sjezd	k1gInSc6	sjezd
a	a	k8xC	a
ve	v	k7c6	v
slalomu	slalom	k1gInSc6	slalom
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
závod	závod	k1gInSc1	závod
je	být	k5eAaImIp3nS	být
hodnocen	hodnotit	k5eAaImNgInS	hodnotit
samostatně	samostatně	k6eAd1	samostatně
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
slalom	slalom	k1gInSc1	slalom
pořádán	pořádán	k2eAgInSc1d1	pořádán
jako	jako	k8xC	jako
druhá	druhý	k4xOgFnSc1	druhý
disciplina	disciplina	k1gFnSc1	disciplina
<g/>
,	,	kIx,	,
startují	startovat	k5eAaBmIp3nP	startovat
ve	v	k7c6	v
II	II	kA	II
<g/>
.	.	kIx.	.
kole	kolo	k1gNnSc6	kolo
závodníci	závodník	k1gMnPc1	závodník
označení	označení	k1gNnSc2	označení
jako	jako	k8xS	jako
kombinační	kombinační	k2eAgMnPc1d1	kombinační
jezdci	jezdec	k1gMnPc1	jezdec
písmeny	písmeno	k1gNnPc7	písmeno
"	"	kIx"	"
<g/>
K	K	kA	K
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
ZK	ZK	kA	ZK
<g/>
"	"	kIx"	"
na	na	k7c6	na
konci	konec	k1gInSc6	konec
startovního	startovní	k2eAgNnSc2d1	startovní
pole	pole	k1gNnSc2	pole
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
neumístili	umístit	k5eNaPmAgMnP	umístit
v	v	k7c6	v
první	první	k4xOgFnSc6	první
třicítce	třicítka	k1gFnSc6	třicítka
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgFnPc1d1	zvláštní
formy	forma	k1gFnPc1	forma
kombinace	kombinace	k1gFnSc2	kombinace
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
povoleny	povolen	k2eAgFnPc1d1	povolena
kombinace	kombinace	k1gFnPc1	kombinace
složené	složený	k2eAgFnPc1d1	složená
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
nebo	nebo	k8xC	nebo
čtyř	čtyři	k4xCgFnPc2	čtyři
disciplin	disciplina	k1gFnPc2	disciplina
<g/>
.	.	kIx.	.
</s>
