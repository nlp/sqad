<p>
<s>
Simo	sima	k1gFnSc5	sima
Häyhä	Häyhä	k1gFnSc5	Häyhä
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1905	[number]	k4	1905
Rautjärvi	Rautjärev	k1gFnSc6	Rautjärev
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2002	[number]	k4	2002
Hamina	Hamino	k1gNnSc2	Hamino
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přezdívaný	přezdívaný	k2eAgMnSc1d1	přezdívaný
též	též	k9	též
Bílá	bílý	k2eAgFnSc1d1	bílá
smrt	smrt	k1gFnSc1	smrt
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
finský	finský	k2eAgMnSc1d1	finský
voják	voják	k1gMnSc1	voják
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
během	během	k7c2	během
zimní	zimní	k2eAgFnSc2d1	zimní
války	válka	k1gFnSc2	válka
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
odstřelovač	odstřelovač	k1gMnSc1	odstřelovač
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
všeobecně	všeobecně	k6eAd1	všeobecně
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
nejúspěšnějšího	úspěšný	k2eAgMnSc4d3	nejúspěšnější
finského	finský	k2eAgMnSc4d1	finský
i	i	k8xC	i
celosvětového	celosvětový	k2eAgMnSc4d1	celosvětový
odstřelovače	odstřelovač	k1gMnSc4	odstřelovač
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
potvrzené	potvrzený	k2eAgNnSc1d1	potvrzené
zabití	zabití	k1gNnSc1	zabití
505	[number]	k4	505
rudoarmějců	rudoarmějec	k1gMnPc2	rudoarmějec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
a	a	k8xC	a
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Simo	sima	k1gFnSc5	sima
Häyhä	Häyhä	k1gFnSc5	Häyhä
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
malém	malý	k2eAgNnSc6d1	malé
městečku	městečko	k1gNnSc6	městečko
Rautjärvi	Rautjärev	k1gFnSc6	Rautjärev
<g/>
,	,	kIx,	,
ležícím	ležící	k2eAgNnSc6d1	ležící
na	na	k7c6	na
Karelské	karelský	k2eAgFnSc6d1	Karelská
šíji	šíj	k1gFnSc6	šíj
poblíž	poblíž	k6eAd1	poblíž
hranic	hranice	k1gFnPc2	hranice
se	s	k7c7	s
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
(	(	kIx(	(
<g/>
stav	stav	k1gInSc1	stav
1920	[number]	k4	1920
<g/>
–	–	k?	–
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vojenskou	vojenský	k2eAgFnSc4d1	vojenská
službu	služba	k1gFnSc4	služba
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
<g/>
;	;	kIx,	;
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gFnPc4	jeho
vynikající	vynikající	k2eAgFnPc4d1	vynikající
střelecké	střelecký	k2eAgFnPc4d1	střelecká
schopnosti	schopnost	k1gFnPc4	schopnost
(	(	kIx(	(
<g/>
byl	být	k5eAaImAgMnS	být
vášnivým	vášnivý	k2eAgMnSc7d1	vášnivý
lovcem	lovec	k1gMnSc7	lovec
<g/>
)	)	kIx)	)
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
svěřena	svěřen	k2eAgFnSc1d1	svěřena
úloha	úloha	k1gFnSc1	úloha
odstřelovače	odstřelovač	k1gMnSc2	odstřelovač
<g/>
,	,	kIx,	,
ve	v	k7c6	v
finských	finský	k2eAgFnPc6d1	finská
ozbrojených	ozbrojený	k2eAgFnPc6d1	ozbrojená
silách	síla	k1gFnPc6	síla
považovaná	považovaný	k2eAgFnSc1d1	považovaná
za	za	k7c4	za
přísně	přísně	k6eAd1	přísně
výběrový	výběrový	k2eAgInSc4d1	výběrový
post	post	k1gInSc4	post
<g/>
.	.	kIx.	.
</s>
<s>
Finští	finský	k2eAgMnPc1d1	finský
odstřelovači	odstřelovač	k1gMnPc1	odstřelovač
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
celosvětovém	celosvětový	k2eAgNnSc6d1	celosvětové
měřítku	měřítko	k1gNnSc6	měřítko
elitou	elita	k1gFnSc7	elita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
mobilizaci	mobilizace	k1gFnSc6	mobilizace
po	po	k7c4	po
přepadení	přepadení	k1gNnSc4	přepadení
Finska	Finsko	k1gNnSc2	Finsko
30	[number]	k4	30
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1939	[number]	k4	1939
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
byl	být	k5eAaImAgInS	být
jakožto	jakožto	k8xS	jakožto
samostatně	samostatně	k6eAd1	samostatně
působící	působící	k2eAgMnSc1d1	působící
odstřelovač	odstřelovač	k1gMnSc1	odstřelovač
přidělen	přidělit	k5eAaPmNgMnS	přidělit
k	k	k7c3	k
12	[number]	k4	12
<g/>
.	.	kIx.	.
pěší	pěší	k2eAgFnSc3d1	pěší
divizi	divize	k1gFnSc3	divize
plukovníka	plukovník	k1gMnSc2	plukovník
Svenssona	Svensson	k1gMnSc2	Svensson
<g/>
,	,	kIx,	,
dislokované	dislokovaný	k2eAgNnSc1d1	dislokované
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
okolo	okolo	k7c2	okolo
řeky	řeka	k1gFnSc2	řeka
Kollaa	Kolla	k1gInSc2	Kolla
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
bojoval	bojovat	k5eAaImAgMnS	bojovat
od	od	k7c2	od
samotného	samotný	k2eAgInSc2d1	samotný
počátku	počátek	k1gInSc2	počátek
bojů	boj	k1gInPc2	boj
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
až	až	k9	až
po	po	k7c4	po
6	[number]	k4	6
<g/>
.	.	kIx.	.
březen	březen	k1gInSc1	březen
1940	[number]	k4	1940
<g/>
,	,	kIx,	,
na	na	k7c6	na
úseku	úsek	k1gInSc6	úsek
fronty	fronta	k1gFnSc2	fronta
kde	kde	k6eAd1	kde
operoval	operovat	k5eAaImAgMnS	operovat
Häyhä	Häyhä	k1gMnSc1	Häyhä
se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
jednotkou	jednotka	k1gFnSc7	jednotka
<g/>
,	,	kIx,	,
začali	začít	k5eAaPmAgMnP	začít
tlačit	tlačit	k5eAaImF	tlačit
Sověti	Sovět	k1gMnPc1	Sovět
frontálním	frontální	k2eAgInSc7d1	frontální
útokem	útok	k1gInSc7	útok
<g/>
.	.	kIx.	.
</s>
<s>
Simův	Simův	k2eAgMnSc1d1	Simův
34	[number]	k4	34
<g/>
.	.	kIx.	.
pěší	pěší	k2eAgInSc1d1	pěší
regiment	regiment	k1gInSc1	regiment
utrpěl	utrpět	k5eAaPmAgInS	utrpět
během	během	k7c2	během
útoku	útok	k1gInSc2	útok
těžké	těžký	k2eAgFnSc2d1	těžká
ztráty	ztráta	k1gFnSc2	ztráta
<g/>
,	,	kIx,	,
sám	sám	k3xTgMnSc1	sám
Simo	sima	k1gFnSc5	sima
Häyhä	Häyhä	k1gMnSc7	Häyhä
byl	být	k5eAaImAgMnS	být
zasažen	zasáhnout	k5eAaPmNgMnS	zasáhnout
do	do	k7c2	do
obličeje	obličej	k1gInSc2	obličej
tříštivou	tříštivý	k2eAgFnSc7d1	tříštivá
střelou	střela	k1gFnSc7	střela
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mu	on	k3xPp3gMnSc3	on
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
frakturu	fraktura	k1gFnSc4	fraktura
čelisti	čelist	k1gFnSc2	čelist
<g/>
,	,	kIx,	,
potom	potom	k6eAd1	potom
upadl	upadnout	k5eAaPmAgMnS	upadnout
do	do	k7c2	do
kómatu	kóma	k1gNnSc2	kóma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
tří	tři	k4xCgInPc2	tři
měsíců	měsíc	k1gInPc2	měsíc
svého	své	k1gNnSc2	své
působení	působení	k1gNnSc2	působení
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
odstřelovačské	odstřelovačský	k2eAgFnSc2d1	odstřelovačská
pušky	puška	k1gFnSc2	puška
zlikvidoval	zlikvidovat	k5eAaPmAgInS	zlikvidovat
259	[number]	k4	259
rudoarmějců	rudoarmějec	k1gMnPc2	rudoarmějec
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
neoficiálních	oficiální	k2eNgFnPc2d1	neoficiální
finských	finský	k2eAgFnPc2d1	finská
statistik	statistika	k1gFnPc2	statistika
542	[number]	k4	542
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
však	však	k9	však
považoval	považovat	k5eAaImAgMnS	považovat
tyto	tento	k3xDgInPc4	tento
údaje	údaj	k1gInPc4	údaj
za	za	k7c4	za
přehnané	přehnaný	k2eAgFnPc4d1	přehnaná
<g/>
.	.	kIx.	.
</s>
<s>
Stejný	stejný	k2eAgInSc1d1	stejný
počet	počet	k1gInSc1	počet
zabil	zabít	k5eAaPmAgInS	zabít
pomocí	pomocí	k7c2	pomocí
samopalu	samopal	k1gInSc2	samopal
Suomi	Suo	k1gFnPc7	Suo
a	a	k8xC	a
ti	ten	k3xDgMnPc1	ten
se	se	k3xPyFc4	se
do	do	k7c2	do
odstřelovaných	odstřelovaný	k2eAgFnPc2d1	odstřelovaný
statistik	statistika	k1gFnPc2	statistika
nezapočítávají	započítávat	k5eNaImIp3nP	započítávat
<g/>
,	,	kIx,	,
celkem	celkem	k6eAd1	celkem
tedy	tedy	k9	tedy
kolem	kolem	k7c2	kolem
500	[number]	k4	500
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Metody	metoda	k1gFnPc1	metoda
==	==	k?	==
</s>
</p>
<p>
<s>
Simo	sima	k1gFnSc5	sima
Häyhä	Häyhä	k1gFnSc5	Häyhä
používal	používat	k5eAaImAgMnS	používat
obvykle	obvykle	k6eAd1	obvykle
pušku	puška	k1gFnSc4	puška
Mosin-Nagant	Mosin-Naganta	k1gFnPc2	Mosin-Naganta
M28	M28	k1gFnSc2	M28
s	s	k7c7	s
mechanickými	mechanický	k2eAgNnPc7d1	mechanické
mířidly	mířidlo	k1gNnPc7	mířidlo
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
ani	ani	k8xC	ani
jinou	jiná	k1gFnSc4	jiná
neměl	mít	k5eNaImAgMnS	mít
<g/>
,	,	kIx,	,
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1940	[number]	k4	1940
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
Švédy	švéda	k1gFnSc2	švéda
darována	darovat	k5eAaPmNgFnS	darovat
klasická	klasický	k2eAgFnSc1d1	klasická
odstřelovačská	odstřelovačský	k2eAgFnSc1d1	odstřelovačská
puška	puška	k1gFnSc1	puška
Mauser	Mausra	k1gFnPc2	Mausra
s	s	k7c7	s
puškohledem	puškohled	k1gInSc7	puškohled
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
i	i	k9	i
přesto	přesto	k8xC	přesto
dále	daleko	k6eAd2	daleko
upřednostňoval	upřednostňovat	k5eAaImAgInS	upřednostňovat
M	M	kA	M
<g/>
28	[number]	k4	28
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yQgFnSc7	který
dokázal	dokázat	k5eAaPmAgMnS	dokázat
spolehlivě	spolehlivě	k6eAd1	spolehlivě
zabíjet	zabíjet	k5eAaImF	zabíjet
nepřátele	nepřítel	k1gMnPc4	nepřítel
na	na	k7c4	na
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
400	[number]	k4	400
metrů	metr	k1gInPc2	metr
(	(	kIx(	(
<g/>
za	za	k7c2	za
vhodných	vhodný	k2eAgFnPc2d1	vhodná
podmínek	podmínka	k1gFnPc2	podmínka
až	až	k9	až
600	[number]	k4	600
metrů	metr	k1gInPc2	metr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mluvily	mluvit	k5eAaImAgInP	mluvit
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
dvě	dva	k4xCgFnPc4	dva
okolnosti	okolnost	k1gFnPc4	okolnost
<g/>
:	:	kIx,	:
jednak	jednak	k8xC	jednak
její	její	k3xOp3gNnPc4	její
mechanická	mechanický	k2eAgNnPc4d1	mechanické
mířidla	mířidlo	k1gNnPc4	mířidlo
neodrážela	odrážet	k5eNaImAgFnS	odrážet
světlo	světlo	k1gNnSc4	světlo
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
střelec	střelec	k1gMnSc1	střelec
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
měl	mít	k5eAaImAgInS	mít
menší	malý	k2eAgMnSc1d2	menší
(	(	kIx(	(
<g/>
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
méně	málo	k6eAd2	málo
nápadnou	nápadný	k2eAgFnSc4d1	nápadná
<g/>
)	)	kIx)	)
siluetu	silueta	k1gFnSc4	silueta
než	než	k8xS	než
střelec	střelec	k1gMnSc1	střelec
používající	používající	k2eAgFnSc4d1	používající
pušku	puška	k1gFnSc4	puška
s	s	k7c7	s
puškohledem	puškohled	k1gInSc7	puškohled
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
musí	muset	k5eAaImIp3nS	muset
trochu	trochu	k6eAd1	trochu
více	hodně	k6eAd2	hodně
zvednout	zvednout	k5eAaPmF	zvednout
hlavu	hlava	k1gFnSc4	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Optická	optický	k2eAgNnPc1d1	optické
mířidla	mířidlo	k1gNnPc1	mířidlo
navíc	navíc	k6eAd1	navíc
v	v	k7c6	v
tuhém	tuhý	k2eAgInSc6d1	tuhý
mrazu	mráz	k1gInSc6	mráz
namrzala	namrzat	k5eAaImAgFnS	namrzat
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
drobné	drobný	k2eAgFnSc3d1	drobná
postavě	postava	k1gFnSc3	postava
si	se	k3xPyFc3	se
Häyhä	Häyhä	k1gMnSc4	Häyhä
většinou	většinou	k6eAd1	většinou
nelehal	lehat	k5eNaImAgMnS	lehat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spíše	spíše	k9	spíše
seděl	sedět	k5eAaImAgMnS	sedět
v	v	k7c6	v
nějaké	nějaký	k3yIgFnSc6	nějaký
malé	malý	k2eAgFnSc6d1	malá
díře	díra	k1gFnSc6	díra
<g/>
.	.	kIx.	.
</s>
<s>
Oblečen	oblečen	k2eAgMnSc1d1	oblečen
v	v	k7c6	v
bílém	bílý	k2eAgInSc6d1	bílý
maskovacím	maskovací	k2eAgInSc6d1	maskovací
oděvu	oděv	k1gInSc6	oděv
operoval	operovat	k5eAaImAgMnS	operovat
v	v	k7c6	v
zasněžené	zasněžený	k2eAgFnSc6d1	zasněžená
krajině	krajina	k1gFnSc6	krajina
při	při	k7c6	při
teplotách	teplota	k1gFnPc6	teplota
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
kolísaly	kolísat	k5eAaImAgInP	kolísat
od	od	k7c2	od
−	−	k?	−
do	do	k7c2	do
−	−	k?	−
°	°	k?	°
<g/>
C.	C.	kA	C.
Když	když	k8xS	když
vyrážel	vyrážet	k5eAaImAgInS	vyrážet
na	na	k7c4	na
své	svůj	k3xOyFgFnPc4	svůj
výpravy	výprava	k1gFnPc4	výprava
do	do	k7c2	do
země	zem	k1gFnSc2	zem
nikoho	nikdo	k3yNnSc2	nikdo
a	a	k8xC	a
do	do	k7c2	do
nepřátelského	přátelský	k2eNgInSc2d1	nepřátelský
týlu	týl	k1gInSc2	týl
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
říkával	říkávat	k5eAaImAgMnS	říkávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
odchází	odcházet	k5eAaImIp3nS	odcházet
"	"	kIx"	"
<g/>
lovit	lovit	k5eAaImF	lovit
Rusy	Rus	k1gMnPc4	Rus
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
později	pozdě	k6eAd2	pozdě
ruští	ruský	k2eAgMnPc1d1	ruský
odstřelovači	odstřelovač	k1gMnPc1	odstřelovač
s	s	k7c7	s
oblibou	obliba	k1gFnSc7	obliba
tvrdili	tvrdit	k5eAaImAgMnP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jdou	jít	k5eAaImIp3nP	jít
"	"	kIx"	"
<g/>
lovit	lovit	k5eAaImF	lovit
Němce	Němec	k1gMnSc4	Němec
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
==	==	k?	==
</s>
</p>
<p>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
konci	konec	k1gInSc6	konec
zimní	zimní	k2eAgFnSc2d1	zimní
války	válka	k1gFnSc2	válka
jej	on	k3xPp3gMnSc4	on
maršál	maršál	k1gMnSc1	maršál
Mannerheim	Mannerheim	k1gMnSc1	Mannerheim
povýšil	povýšit	k5eAaPmAgMnS	povýšit
z	z	k7c2	z
desátníka	desátník	k1gMnSc2	desátník
(	(	kIx(	(
<g/>
fin.	fin.	k?	fin.
"	"	kIx"	"
<g/>
alikersantti	alikersantť	k1gFnSc2	alikersantť
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
na	na	k7c4	na
nadporučíka	nadporučík	k1gMnSc4	nadporučík
(	(	kIx(	(
<g/>
fin.	fin.	k?	fin.
"	"	kIx"	"
<g/>
luutnantti	luutnantť	k1gFnSc2	luutnantť
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
představuje	představovat	k5eAaImIp3nS	představovat
největší	veliký	k2eAgInSc1d3	veliký
hodnostní	hodnostní	k2eAgInSc1d1	hodnostní
skok	skok	k1gInSc1	skok
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
finské	finský	k2eAgFnSc2d1	finská
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Žádného	žádný	k1gMnSc4	žádný
dalšího	další	k2eAgInSc2d1	další
konfliktu	konflikt	k1gInSc2	konflikt
se	se	k3xPyFc4	se
už	už	k6eAd1	už
nezúčastnil	zúčastnit	k5eNaPmAgMnS	zúčastnit
(	(	kIx(	(
<g/>
zranění	zranění	k1gNnSc3	zranění
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
zanechalo	zanechat	k5eAaPmAgNnS	zanechat
trvalé	trvalý	k2eAgInPc4d1	trvalý
následky	následek	k1gInPc4	následek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Uplynulo	uplynout	k5eAaPmAgNnS	uplynout
několik	několik	k4yIc1	několik
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
mu	on	k3xPp3gNnSc3	on
zranění	zranění	k1gNnSc3	zranění
zahojila	zahojit	k5eAaPmAgFnS	zahojit
<g/>
.	.	kIx.	.
</s>
<s>
Sovětská	sovětský	k2eAgFnSc1d1	sovětská
tříštivá	tříštivý	k2eAgFnSc1d1	tříštivá
střela	střela	k1gFnSc1	střela
mu	on	k3xPp3gMnSc3	on
zničila	zničit	k5eAaPmAgFnS	zničit
čelist	čelist	k1gFnSc4	čelist
a	a	k8xC	a
potrhala	potrhat	k5eAaPmAgFnS	potrhat
tvář	tvář	k1gFnSc4	tvář
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgInS	zabývat
chovem	chov	k1gInSc7	chov
psů	pes	k1gMnPc2	pes
a	a	k8xC	a
lovem	lov	k1gInSc7	lov
<g/>
.	.	kIx.	.
</s>
<s>
Svoje	svůj	k3xOyFgNnPc4	svůj
poslední	poslední	k2eAgNnPc4d1	poslední
léta	léto	k1gNnPc4	léto
strávil	strávit	k5eAaPmAgMnS	strávit
ve	v	k7c6	v
vesničce	vesnička	k1gFnSc6	vesnička
Ruokolahti	Ruokolahť	k1gFnSc2	Ruokolahť
v	v	k7c6	v
jihovýchodním	jihovýchodní	k2eAgNnSc6d1	jihovýchodní
Finsku	Finsko	k1gNnSc6	Finsko
<g/>
,	,	kIx,	,
15	[number]	k4	15
km	km	kA	km
od	od	k7c2	od
ruské	ruský	k2eAgFnSc2d1	ruská
hranice	hranice	k1gFnSc2	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
zeptali	zeptat	k5eAaPmAgMnP	zeptat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
na	na	k7c4	na
tajemství	tajemství	k1gNnSc4	tajemství
jeho	jeho	k3xOp3gFnSc2	jeho
přesnosti	přesnost	k1gFnSc2	přesnost
<g/>
,	,	kIx,	,
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
<g/>
:	:	kIx,	:
Praxe	praxe	k1gFnSc1	praxe
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k8xC	a
svůj	svůj	k3xOyFgInSc4	svůj
rekord	rekord	k1gInSc4	rekord
přes	přes	k7c4	přes
500	[number]	k4	500
zabitých	zabitý	k2eAgMnPc2d1	zabitý
nepřátel	nepřítel	k1gMnPc2	nepřítel
okomentoval	okomentovat	k5eAaPmAgMnS	okomentovat
slovy	slovo	k1gNnPc7	slovo
<g/>
:	:	kIx,	:
Dělal	dělat	k5eAaImAgMnS	dělat
jsem	být	k5eAaImIp1nS	být
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
mi	já	k3xPp1nSc3	já
přikazovali	přikazovat	k5eAaImAgMnP	přikazovat
<g/>
;	;	kIx,	;
nejlépe	dobře	k6eAd3	dobře
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
jsem	být	k5eAaImIp1nS	být
mohl	moct	k5eAaImAgMnS	moct
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
zařadila	zařadit	k5eAaPmAgFnS	zařadit
švédská	švédský	k2eAgFnSc1d1	švédská
powermetalová	powermetalový	k2eAgFnSc1d1	powermetalová
skupina	skupina	k1gFnSc1	skupina
Sabaton	Sabaton	k1gInSc4	Sabaton
na	na	k7c4	na
desku	deska	k1gFnSc4	deska
Coat	Coat	k2eAgInSc1d1	Coat
of	of	k?	of
Arms	Arms	k1gInSc1	Arms
píseň	píseň	k1gFnSc1	píseň
White	Whit	k1gInSc5	Whit
Death	Death	k1gInSc4	Death
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Bílá	bílý	k2eAgFnSc1d1	bílá
smrt	smrt	k1gFnSc1	smrt
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
věnovanou	věnovaný	k2eAgFnSc4d1	věnovaná
tomuto	tento	k3xDgMnSc3	tento
odstřelovači	odstřelovač	k1gMnSc3	odstřelovač
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Simo	sima	k1gFnSc5	sima
Häyhä	Häyhä	k1gMnPc3	Häyhä
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
