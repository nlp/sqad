<s>
Městys	městys	k1gInSc1	městys
nebo	nebo	k8xC	nebo
také	také	k9	také
městečko	městečko	k1gNnSc1	městečko
je	být	k5eAaImIp3nS	být
typ	typ	k1gInSc4	typ
obcí	obec	k1gFnPc2	obec
velikostně	velikostně	k6eAd1	velikostně
a	a	k8xC	a
významově	významově	k6eAd1	významově
stojící	stojící	k2eAgFnSc1d1	stojící
mezi	mezi	k7c7	mezi
městem	město	k1gNnSc7	město
a	a	k8xC	a
vsí	ves	k1gFnSc7	ves
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
sídla	sídlo	k1gNnPc4	sídlo
<g/>
,	,	kIx,	,
kterým	který	k3yRgNnSc7	který
bylo	být	k5eAaImAgNnS	být
uděleno	udělit	k5eAaPmNgNnS	udělit
právo	právo	k1gNnSc1	právo
pořádat	pořádat	k5eAaImF	pořádat
týdenní	týdenní	k2eAgInPc4d1	týdenní
a	a	k8xC	a
dobytčí	dobytčí	k2eAgInPc4d1	dobytčí
trhy	trh	k1gInPc4	trh
(	(	kIx(	(
<g/>
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
městyse	městys	k1gInPc1	městys
lišily	lišit	k5eAaImAgInP	lišit
od	od	k7c2	od
vsí	ves	k1gFnPc2	ves
<g/>
)	)	kIx)	)
a	a	k8xC	a
zpočátku	zpočátku	k6eAd1	zpočátku
výjimečně	výjimečně	k6eAd1	výjimečně
i	i	k9	i
výroční	výroční	k2eAgInPc4d1	výroční
trhy	trh	k1gInPc4	trh
<g/>
.	.	kIx.	.
</s>
<s>
Městyse	městys	k1gInPc1	městys
musely	muset	k5eAaImAgInP	muset
plnit	plnit	k5eAaImF	plnit
roli	role	k1gFnSc4	role
spádového	spádový	k2eAgNnSc2d1	spádové
městečka	městečko	k1gNnSc2	městečko
pro	pro	k7c4	pro
okolní	okolní	k2eAgFnPc4d1	okolní
vesnice	vesnice	k1gFnPc4	vesnice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
města	město	k1gNnSc2	město
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
mnohem	mnohem	k6eAd1	mnohem
silněji	silně	k6eAd2	silně
zastoupeno	zastoupen	k2eAgNnSc1d1	zastoupeno
zemědělství	zemědělství	k1gNnSc1	zemědělství
a	a	k8xC	a
sociální	sociální	k2eAgNnSc1d1	sociální
a	a	k8xC	a
profesní	profesní	k2eAgNnSc1d1	profesní
rozvrstvení	rozvrstvení	k1gNnSc1	rozvrstvení
nebylo	být	k5eNaImAgNnS	být
tak	tak	k6eAd1	tak
výrazné	výrazný	k2eAgNnSc1d1	výrazné
<g/>
.	.	kIx.	.
</s>
<s>
Městys	městys	k1gInSc1	městys
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
současné	současný	k2eAgFnSc2d1	současná
české	český	k2eAgFnSc2d1	Česká
legislativy	legislativa	k1gFnSc2	legislativa
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
status	status	k1gInSc4	status
některých	některý	k3yIgFnPc2	některý
obcí	obec	k1gFnPc2	obec
a	a	k8xC	a
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
se	se	k3xPyFc4	se
na	na	k7c4	na
celou	celá	k1gFnSc4	celá
takovou	takový	k3xDgFnSc4	takový
obec	obec	k1gFnSc4	obec
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
jejích	její	k3xOp3gFnPc2	její
případných	případný	k2eAgFnPc2d1	případná
venkovských	venkovský	k2eAgFnPc2d1	venkovská
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Rakousko-Uhersku	Rakousko-Uhersko	k1gNnSc6	Rakousko-Uhersko
a	a	k8xC	a
Československu	Československo	k1gNnSc6	Československo
se	se	k3xPyFc4	se
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
status	status	k1gInSc1	status
města	město	k1gNnSc2	město
nebo	nebo	k8xC	nebo
městyse	městys	k1gInSc2	městys
vztahoval	vztahovat	k5eAaImAgInS	vztahovat
v	v	k7c6	v
případě	případ	k1gInSc6	případ
obcí	obec	k1gFnPc2	obec
skládajících	skládající	k2eAgFnPc6d1	skládající
se	se	k3xPyFc4	se
z	z	k7c2	z
více	hodně	k6eAd2	hodně
osad	osada	k1gFnPc2	osada
(	(	kIx(	(
<g/>
sídelních	sídelní	k2eAgInPc2d1	sídelní
útvarů	útvar	k1gInPc2	útvar
<g/>
)	)	kIx)	)
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
příslušnou	příslušný	k2eAgFnSc4d1	příslušná
osadu	osada	k1gFnSc4	osada
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
na	na	k7c4	na
celou	celý	k2eAgFnSc4d1	celá
obec	obec	k1gFnSc4	obec
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
obec	obec	k1gFnSc4	obec
Valašské	valašský	k2eAgNnSc1d1	Valašské
Meziříčí	Meziříčí	k1gNnSc1	Meziříčí
tvořily	tvořit	k5eAaImAgInP	tvořit
dvě	dva	k4xCgFnPc4	dva
osady	osada	k1gFnPc4	osada
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
město	město	k1gNnSc1	město
Valašské	valašský	k2eAgNnSc1d1	Valašské
Meziříčí	Meziříčí	k1gNnSc1	Meziříčí
a	a	k8xC	a
městys	městys	k1gInSc4	městys
Krásno	krásno	k1gNnSc4	krásno
nad	nad	k7c7	nad
Bečvou	Bečva	k1gFnSc7	Bečva
<g/>
.	.	kIx.	.
</s>
<s>
Městečka	městečko	k1gNnPc1	městečko
začala	začít	k5eAaPmAgNnP	začít
vznikat	vznikat	k5eAaImF	vznikat
od	od	k7c2	od
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
především	především	k9	především
na	na	k7c6	na
majetcích	majetek	k1gInPc6	majetek
šlechty	šlechta	k1gFnSc2	šlechta
a	a	k8xC	a
církevních	církevní	k2eAgFnPc2d1	církevní
vrchností	vrchnost	k1gFnPc2	vrchnost
<g/>
.	.	kIx.	.
</s>
<s>
Status	status	k1gInSc1	status
městečka	městečko	k1gNnSc2	městečko
uděloval	udělovat	k5eAaImAgInS	udělovat
od	od	k7c2	od
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
panovník	panovník	k1gMnSc1	panovník
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
tyto	tento	k3xDgFnPc1	tento
sídelní	sídelní	k2eAgFnPc1d1	sídelní
jednotky	jednotka	k1gFnPc1	jednotka
v	v	k7c6	v
pramenech	pramen	k1gInPc6	pramen
označovaly	označovat	k5eAaImAgInP	označovat
i	i	k9	i
jako	jako	k9	jako
městyse	městys	k1gInPc1	městys
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tato	tento	k3xDgNnPc1	tento
sídliště	sídliště	k1gNnPc1	sídliště
nazývala	nazývat	k5eAaImAgNnP	nazývat
i	i	k9	i
"	"	kIx"	"
<g/>
městce	městec	k1gInPc4	městec
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgInP	moct
dokládat	dokládat	k5eAaImF	dokládat
názvy	název	k1gInPc1	název
jako	jako	k8xC	jako
Městec	Městec	k1gInSc1	Městec
Králové	Králová	k1gFnSc2	Králová
nebo	nebo	k8xC	nebo
Heřmanův	Heřmanův	k2eAgInSc4d1	Heřmanův
Městec	Městec	k1gInSc4	Městec
<g/>
.	.	kIx.	.
</s>
<s>
Městečka	městečko	k1gNnPc1	městečko
byla	být	k5eAaImAgNnP	být
rozlohou	rozloha	k1gFnSc7	rozloha
často	často	k6eAd1	často
velmi	velmi	k6eAd1	velmi
malá	malý	k2eAgFnSc1d1	malá
a	a	k8xC	a
na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
jich	on	k3xPp3gMnPc2	on
během	během	k7c2	během
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
<g/>
.	.	kIx.	.
</s>
<s>
Eduard	Eduard	k1gMnSc1	Eduard
Maur	Maur	k1gMnSc1	Maur
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
udává	udávat	k5eAaImIp3nS	udávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
průměrná	průměrný	k2eAgFnSc1d1	průměrná
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
městečky	městečko	k1gNnPc7	městečko
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
činila	činit	k5eAaImAgFnS	činit
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
ČSR	ČSR	kA	ČSR
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
udělovala	udělovat	k5eAaImAgFnS	udělovat
status	status	k1gInSc4	status
ministerská	ministerský	k2eAgFnSc1d1	ministerská
rada	rada	k1gFnSc1	rada
na	na	k7c6	na
základě	základ	k1gInSc6	základ
detailní	detailní	k2eAgFnSc2d1	detailní
žádosti	žádost	k1gFnSc2	žádost
obce	obec	k1gFnSc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
"	"	kIx"	"
<g/>
Statistického	statistický	k2eAgInSc2d1	statistický
lexikonu	lexikon	k1gInSc2	lexikon
v	v	k7c6	v
Republice	republika	k1gFnSc6	republika
československé	československý	k2eAgNnSc1d1	Československé
<g/>
"	"	kIx"	"
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
na	na	k7c6	na
území	území	k1gNnSc6	území
ČSR	ČSR	kA	ČSR
503	[number]	k4	503
městysů	městys	k1gInPc2	městys
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2006	[number]	k4	2006
uděluje	udělovat	k5eAaImIp3nS	udělovat
status	status	k1gInSc4	status
městyse	městys	k1gInSc2	městys
předseda	předseda	k1gMnSc1	předseda
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
Parlamentu	parlament	k1gInSc2	parlament
ČR	ČR	kA	ČR
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
obce	obec	k1gFnSc2	obec
po	po	k7c4	po
vyjádření	vyjádření	k1gNnSc4	vyjádření
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Status	status	k1gInSc1	status
městyse	městys	k1gInSc2	městys
přestal	přestat	k5eAaPmAgInS	přestat
být	být	k5eAaImF	být
používán	používán	k2eAgInSc1d1	používán
komunisty	komunista	k1gMnPc7	komunista
po	po	k7c6	po
únorovém	únorový	k2eAgInSc6d1	únorový
vládním	vládní	k2eAgInSc6d1	vládní
převratu	převrat	k1gInSc6	převrat
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
sametové	sametový	k2eAgFnSc2d1	sametová
revoluce	revoluce	k1gFnSc2	revoluce
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
představitelé	představitel	k1gMnPc1	představitel
obcí	obec	k1gFnPc2	obec
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
držely	držet	k5eAaImAgFnP	držet
status	status	k1gInSc4	status
městyse	městys	k1gInSc2	městys
<g/>
,	,	kIx,	,
snažili	snažit	k5eAaImAgMnP	snažit
o	o	k7c6	o
jeho	jeho	k3xOp3gNnSc6	jeho
obnovení	obnovení	k1gNnSc6	obnovení
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
několikaleté	několikaletý	k2eAgNnSc1d1	několikaleté
lobbování	lobbování	k1gNnSc1	lobbování
bylo	být	k5eAaImAgNnS	být
úspěšné	úspěšný	k2eAgNnSc1d1	úspěšné
až	až	k9	až
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
27	[number]	k4	27
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2006	[number]	k4	2006
byla	být	k5eAaImAgFnS	být
Poslaneckou	poslanecký	k2eAgFnSc7d1	Poslanecká
sněmovnou	sněmovna	k1gFnSc7	sněmovna
Parlamentu	parlament	k1gInSc2	parlament
ČR	ČR	kA	ČR
schválena	schválen	k2eAgFnSc1d1	schválena
novela	novela	k1gFnSc1	novela
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
obcích	obec	k1gFnPc6	obec
(	(	kIx(	(
<g/>
obecní	obecní	k2eAgNnSc1d1	obecní
zřízení	zřízení	k1gNnSc1	zřízení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgInPc4d1	jiný
obnovuje	obnovovat	k5eAaImIp3nS	obnovovat
status	status	k1gInSc4	status
městyse	městys	k1gInSc2	městys
<g/>
.	.	kIx.	.
</s>
<s>
Senát	senát	k1gInSc1	senát
Parlamentu	parlament	k1gInSc2	parlament
ČR	ČR	kA	ČR
pak	pak	k6eAd1	pak
toto	tento	k3xDgNnSc4	tento
ustanovení	ustanovení	k1gNnSc4	ustanovení
schválil	schválit	k5eAaPmAgMnS	schválit
na	na	k7c4	na
své	své	k1gNnSc4	své
10	[number]	k4	10
<g/>
.	.	kIx.	.
schůzi	schůze	k1gFnSc6	schůze
29	[number]	k4	29
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
prezident	prezident	k1gMnSc1	prezident
zákon	zákon	k1gInSc4	zákon
podepsal	podepsat	k5eAaPmAgMnS	podepsat
11	[number]	k4	11
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Novela	novela	k1gFnSc1	novela
zákona	zákon	k1gInSc2	zákon
nabyla	nabýt	k5eAaPmAgFnS	nabýt
účinnosti	účinnost	k1gFnPc4	účinnost
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Obce	obec	k1gFnPc1	obec
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
měly	mít	k5eAaImAgInP	mít
oprávnění	oprávnění	k1gNnSc4	oprávnění
užívat	užívat	k5eAaImF	užívat
označení	označení	k1gNnSc4	označení
městys	městys	k1gInSc1	městys
před	před	k7c7	před
17	[number]	k4	17
<g/>
.	.	kIx.	.
květnem	květen	k1gInSc7	květen
1954	[number]	k4	1954
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
městysem	městys	k1gInSc7	městys
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
požádají	požádat	k5eAaPmIp3nP	požádat
předsedu	předseda	k1gMnSc4	předseda
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
Parlamentu	parlament	k1gInSc2	parlament
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1	ten
tak	tak	k9	tak
na	na	k7c4	na
žádost	žádost	k1gFnSc4	žádost
obce	obec	k1gFnSc2	obec
stanoví	stanovit	k5eAaPmIp3nS	stanovit
a	a	k8xC	a
určí	určit	k5eAaPmIp3nS	určit
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
od	od	k7c2	od
kterého	který	k3yRgInSc2	který
se	se	k3xPyFc4	se
obec	obec	k1gFnSc1	obec
opět	opět	k6eAd1	opět
stává	stávat	k5eAaImIp3nS	stávat
městysem	městys	k1gInSc7	městys
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
10	[number]	k4	10
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2006	[number]	k4	2006
vydal	vydat	k5eAaPmAgMnS	vydat
předseda	předseda	k1gMnSc1	předseda
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
prvním	první	k4xOgMnSc6	první
108	[number]	k4	108
obcím	obec	k1gFnPc3	obec
stanovil	stanovit	k5eAaPmAgInS	stanovit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nadále	nadále	k6eAd1	nadále
mohou	moct	k5eAaImIp3nP	moct
používat	používat	k5eAaImF	používat
titulu	titul	k1gInSc2	titul
městys	městys	k1gInSc1	městys
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Uhersku	Uhersko	k1gNnSc6	Uhersko
-	-	kIx~	-
včetně	včetně	k7c2	včetně
Slovenska	Slovensko	k1gNnSc2	Slovensko
-	-	kIx~	-
se	se	k3xPyFc4	se
takovéto	takovýto	k3xDgNnSc1	takovýto
sídlo	sídlo	k1gNnSc1	sídlo
oficiálně	oficiálně	k6eAd1	oficiálně
označovalo	označovat	k5eAaImAgNnS	označovat
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
<g/>
)	)	kIx)	)
oppidum	oppidum	k1gNnSc1	oppidum
(	(	kIx(	(
<g/>
od	od	k7c2	od
14	[number]	k4	14
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1871	[number]	k4	1871
<g/>
;	;	kIx,	;
maďarsky	maďarsky	k6eAd1	maďarsky
mezőváros	mezővárosa	k1gFnPc2	mezővárosa
-	-	kIx~	-
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
polní	polní	k2eAgNnSc1d1	polní
město	město	k1gNnSc1	město
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Marktgemeinde	Marktgemeind	k1gInSc5	Marktgemeind
(	(	kIx(	(
<g/>
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
trhová	trhový	k2eAgFnSc1d1	trhová
obec	obec	k1gFnSc1	obec
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
německojazyčných	německojazyčný	k2eAgFnPc6d1	německojazyčná
oblastech	oblast	k1gFnPc6	oblast
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Flecken	Flecken	k1gInSc1	Flecken
<g/>
,	,	kIx,	,
Marktflecken	Marktflecken	k1gInSc1	Marktflecken
<g/>
,	,	kIx,	,
Markt	Markt	k1gInSc1	Markt
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Dánsku	Dánsko	k1gNnSc6	Dánsko
se	se	k3xPyFc4	se
nazývalo	nazývat	k5eAaImAgNnS	nazývat
flæ	flæ	k?	flæ
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
köping	köping	k1gInSc1	köping
<g/>
.	.	kIx.	.
</s>
<s>
Francouzský	francouzský	k2eAgInSc1d1	francouzský
historický	historický	k2eAgInSc1d1	historický
ekvivalent	ekvivalent	k1gInSc1	ekvivalent
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
bourg	bourg	k1gInSc1	bourg
<g/>
,	,	kIx,	,
anglický	anglický	k2eAgInSc1d1	anglický
borough	borough	k1gInSc1	borough
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Estonsku	Estonsko	k1gNnSc6	Estonsko
se	se	k3xPyFc4	se
odpovídající	odpovídající	k2eAgInSc1d1	odpovídající
typ	typ	k1gInSc1	typ
sídla	sídlo	k1gNnSc2	sídlo
nazývá	nazývat	k5eAaImIp3nS	nazývat
alev	alev	k1gInSc1	alev
<g/>
.	.	kIx.	.
</s>
<s>
Administrativně	administrativně	k6eAd1	administrativně
se	se	k3xPyFc4	se
však	však	k9	však
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
ještě	ještě	k6eAd1	ještě
sídelní	sídelní	k2eAgInSc1d1	sídelní
mezistupeň	mezistupeň	k1gInSc1	mezistupeň
mezi	mezi	k7c7	mezi
vesnicí	vesnice	k1gFnSc7	vesnice
a	a	k8xC	a
městysem	městys	k1gInSc7	městys
<g/>
,	,	kIx,	,
alevik	alevik	k1gInSc1	alevik
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
překládáno	překládán	k2eAgNnSc1d1	překládáno
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
městečko	městečko	k1gNnSc1	městečko
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
malý	malý	k2eAgInSc1d1	malý
městys	městys	k1gInSc1	městys
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Městyse	městys	k1gInPc1	městys
existovaly	existovat	k5eAaImAgInP	existovat
na	na	k7c6	na
území	území	k1gNnSc6	území
historického	historický	k2eAgInSc2d1	historický
polsko-litevského	polskoitevský	k2eAgInSc2d1	polsko-litevský
státu	stát	k1gInSc2	stát
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
nynější	nynější	k2eAgNnSc1d1	nynější
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
Bělorusko	Bělorusko	k1gNnSc1	Bělorusko
<g/>
,	,	kIx,	,
Litva	Litva	k1gFnSc1	Litva
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
Lotyšska	Lotyšsko	k1gNnSc2	Lotyšsko
a	a	k8xC	a
západ	západ	k1gInSc1	západ
nynější	nynější	k2eAgFnSc2d1	nynější
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
)	)	kIx)	)
i	i	k9	i
před	před	k7c7	před
socialismem	socialismus	k1gInSc7	socialismus
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
používal	používat	k5eAaImAgInS	používat
polský	polský	k2eAgInSc1d1	polský
pojem	pojem	k1gInSc1	pojem
miasteczko	miasteczka	k1gFnSc5	miasteczka
(	(	kIx(	(
<g/>
ruský	ruský	k2eAgInSc1d1	ruský
výraz	výraz	k1gInSc1	výraz
м	м	k?	м
je	být	k5eAaImIp3nS	být
přibližným	přibližný	k2eAgInSc7d1	přibližný
přepisem	přepis	k1gInSc7	přepis
polského	polský	k2eAgInSc2d1	polský
výrazu	výraz	k1gInSc2	výraz
do	do	k7c2	do
cyrilice	cyrilice	k1gFnSc2	cyrilice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
litevský	litevský	k2eAgInSc1d1	litevský
miestelis	miestelis	k1gInSc1	miestelis
<g/>
,	,	kIx,	,
lotyšsky	lotyšsky	k6eAd1	lotyšsky
miests	miests	k6eAd1	miests
<g/>
,	,	kIx,	,
ukrajinsky	ukrajinsky	k6eAd1	ukrajinsky
м	м	k?	м
<g/>
,	,	kIx,	,
v	v	k7c6	v
jidiši	jidiše	k1gFnSc6	jidiše
ש	ש	k?	ש
(	(	kIx(	(
<g/>
štetl	štetnout	k5eAaPmAgInS	štetnout
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
přibližným	přibližný	k2eAgInSc7d1	přibližný
ekvivalentem	ekvivalent	k1gInSc7	ekvivalent
v	v	k7c6	v
bývalém	bývalý	k2eAgInSc6d1	bývalý
SSSR	SSSR	kA	SSSR
byl	být	k5eAaImAgMnS	být
(	(	kIx(	(
<g/>
a	a	k8xC	a
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
postsovětských	postsovětský	k2eAgFnPc6d1	postsovětská
zemích	zem	k1gFnPc6	zem
dodnes	dodnes	k6eAd1	dodnes
je	být	k5eAaImIp3nS	být
<g/>
)	)	kIx)	)
pojem	pojem	k1gInSc4	pojem
sídlo	sídlo	k1gNnSc4	sídlo
městského	městský	k2eAgInSc2d1	městský
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
rusky	rusky	k6eAd1	rusky
п	п	k?	п
г	г	k?	г
т	т	k?	т
<g/>
,	,	kIx,	,
ukrajinsky	ukrajinsky	k6eAd1	ukrajinsky
с	с	k?	с
м	м	k?	м
т	т	k?	т
<g/>
,	,	kIx,	,
bělorusky	bělorusky	k6eAd1	bělorusky
п	п	k?	п
г	г	k?	г
т	т	k?	т
<g/>
.	.	kIx.	.
</s>
<s>
Kritéria	kritérion	k1gNnPc1	kritérion
pro	pro	k7c4	pro
přiznání	přiznání	k1gNnSc4	přiznání
tohoto	tento	k3xDgInSc2	tento
statutu	statut	k1gInSc2	statut
se	se	k3xPyFc4	se
značně	značně	k6eAd1	značně
lišila	lišit	k5eAaImAgFnS	lišit
(	(	kIx(	(
<g/>
a	a	k8xC	a
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
dodnes	dodnes	k6eAd1	dodnes
liší	lišit	k5eAaImIp3nP	lišit
<g/>
)	)	kIx)	)
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
subjektech	subjekt	k1gInPc6	subjekt
federace	federace	k1gFnSc2	federace
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgNnSc1d3	veliký
co	co	k9	co
do	do	k7c2	do
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
je	být	k5eAaImIp3nS	být
Paškovskij	Paškovskij	k1gFnSc4	Paškovskij
v	v	k7c6	v
Krasnodarském	krasnodarský	k2eAgInSc6d1	krasnodarský
kraji	kraj	k1gInSc6	kraj
se	se	k3xPyFc4	se
46	[number]	k4	46
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
nejmenší	malý	k2eAgFnPc1d3	nejmenší
nedosahují	dosahovat	k5eNaImIp3nP	dosahovat
ani	ani	k9	ani
stovky	stovka	k1gFnPc1	stovka
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
kategorie	kategorie	k1gFnSc1	kategorie
sídel	sídlo	k1gNnPc2	sídlo
(	(	kIx(	(
<g/>
Osiedle	Osiedlo	k1gNnSc6	Osiedlo
typu	typ	k1gInSc2	typ
miejskiego	miejskiego	k6eAd1	miejskiego
<g/>
)	)	kIx)	)
existovala	existovat	k5eAaImAgFnS	existovat
podle	podle	k7c2	podle
sovětského	sovětský	k2eAgInSc2d1	sovětský
vzoru	vzor	k1gInSc2	vzor
i	i	k9	i
v	v	k7c6	v
socialistickém	socialistický	k2eAgNnSc6d1	socialistické
Polsku	Polsko	k1gNnSc6	Polsko
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1954	[number]	k4	1954
až	až	k9	až
1972	[number]	k4	1972
<g/>
.	.	kIx.	.
</s>
