<p>
<s>
Pampeliška	pampeliška	k1gFnSc1	pampeliška
lékařská	lékařský	k2eAgFnSc1d1	lékařská
(	(	kIx(	(
<g/>
Taraxacum	Taraxacum	k1gInSc1	Taraxacum
officinale	officinale	k6eAd1	officinale
auct	auct	k2eAgInSc1d1	auct
<g/>
.	.	kIx.	.
non	non	k?	non
Wigg	Wigg	k1gInSc1	Wigg
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
uváděná	uváděný	k2eAgFnSc1d1	uváděná
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
smetánka	smetánka	k1gFnSc1	smetánka
lékařská	lékařský	k2eAgFnSc1d1	lékařská
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
běžná	běžný	k2eAgFnSc1d1	běžná
rostlina	rostlina	k1gFnSc1	rostlina
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
hvězdnicovitých	hvězdnicovitý	k2eAgMnPc2d1	hvězdnicovitý
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
nápadné	nápadný	k2eAgNnSc1d1	nápadné
žluté	žlutý	k2eAgNnSc1d1	žluté
květenství	květenství	k1gNnSc1	květenství
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
úbor	úbor	k1gInSc1	úbor
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
posléze	posléze	k6eAd1	posléze
mění	měnit	k5eAaImIp3nS	měnit
v	v	k7c6	v
plodenství	plodenství	k1gNnSc6	plodenství
ochmýřených	ochmýřený	k2eAgFnPc2d1	ochmýřená
nažek	nažka	k1gFnPc2	nažka
<g/>
.	.	kIx.	.
</s>
<s>
Pampeliška	pampeliška	k1gFnSc1	pampeliška
roste	růst	k5eAaImIp3nS	růst
obecně	obecně	k6eAd1	obecně
na	na	k7c6	na
loukách	louka	k1gFnPc6	louka
<g/>
,	,	kIx,	,
u	u	k7c2	u
cest	cesta	k1gFnPc2	cesta
a	a	k8xC	a
na	na	k7c6	na
trávnících	trávník	k1gInPc6	trávník
<g/>
,	,	kIx,	,
kvete	kvést	k5eAaImIp3nS	kvést
zpravidla	zpravidla	k6eAd1	zpravidla
od	od	k7c2	od
dubna	duben	k1gInSc2	duben
do	do	k7c2	do
června	červen	k1gInSc2	červen
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
znovu	znovu	k6eAd1	znovu
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Systematika	systematika	k1gFnSc1	systematika
==	==	k?	==
</s>
</p>
<p>
<s>
Z	z	k7c2	z
botanického	botanický	k2eAgNnSc2d1	botanické
hlediska	hledisko	k1gNnSc2	hledisko
není	být	k5eNaImIp3nS	být
pampeliška	pampeliška	k1gFnSc1	pampeliška
lékařská	lékařský	k2eAgFnSc1d1	lékařská
druh	druh	k1gInSc1	druh
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rozsáhlý	rozsáhlý	k2eAgInSc1d1	rozsáhlý
komplex	komplex	k1gInSc1	komplex
mnoha	mnoho	k4c2	mnoho
převážně	převážně	k6eAd1	převážně
apomikticky	apomikticky	k6eAd1	apomikticky
(	(	kIx(	(
<g/>
tvorba	tvorba	k1gFnSc1	tvorba
semen	semeno	k1gNnPc2	semeno
bez	bez	k7c2	bez
účasti	účast	k1gFnSc2	účast
opylení	opylení	k1gNnSc2	opylení
a	a	k8xC	a
pohlavního	pohlavní	k2eAgInSc2d1	pohlavní
procesu	proces	k1gInSc2	proces
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
rozmnožujících	rozmnožující	k2eAgInPc2d1	rozmnožující
drobných	drobný	k2eAgInPc2d1	drobný
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
mikrospecií	mikrospecie	k1gFnSc7	mikrospecie
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
komplex	komplex	k1gInSc1	komplex
druhů	druh	k1gInPc2	druh
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
Taraxacum	Taraxacum	k1gInSc1	Taraxacum
officinale	officinale	k6eAd1	officinale
agg	agg	k?	agg
<g/>
.	.	kIx.	.
a	a	k8xC	a
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
morfologicky	morfologicky	k6eAd1	morfologicky
velice	velice	k6eAd1	velice
blízké	blízký	k2eAgInPc1d1	blízký
druhy	druh	k1gInPc1	druh
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
přesto	přesto	k8xC	přesto
díky	díky	k7c3	díky
apomixii	apomixie	k1gFnSc3	apomixie
geneticky	geneticky	k6eAd1	geneticky
izolované	izolovaný	k2eAgFnPc4d1	izolovaná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
počtem	počet	k1gInSc7	počet
druhů	druh	k1gInPc2	druh
je	být	k5eAaImIp3nS	být
skupina	skupina	k1gFnSc1	skupina
pampelišky	pampeliška	k1gFnSc2	pampeliška
lékařské	lékařský	k2eAgFnSc2d1	lékařská
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
taxonomicky	taxonomicky	k6eAd1	taxonomicky
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejobtížnějších	obtížný	k2eAgFnPc2d3	nejobtížnější
skupin	skupina	k1gFnPc2	skupina
flóry	flóra	k1gFnSc2	flóra
střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
až	až	k9	až
250	[number]	k4	250
těchto	tento	k3xDgFnPc2	tento
mikrospecií	mikrospecie	k1gFnPc2	mikrospecie
<g/>
,	,	kIx,	,
popsáno	popsat	k5eAaPmNgNnS	popsat
jich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
zatím	zatím	k6eAd1	zatím
asi	asi	k9	asi
100	[number]	k4	100
<g/>
.	.	kIx.	.
</s>
<s>
Druh	druh	k1gInSc1	druh
Taraxacum	Taraxacum	k1gNnSc1	Taraxacum
officinale	officinale	k6eAd1	officinale
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
úzkém	úzký	k2eAgNnSc6d1	úzké
vymezení	vymezení	k1gNnSc6	vymezení
(	(	kIx(	(
<g/>
postaveném	postavený	k2eAgInSc6d1	postavený
na	na	k7c6	na
typu	typ	k1gInSc6	typ
Leontodon	Leontodon	k1gNnSc1	Leontodon
taraxacum	taraxacum	k1gInSc1	taraxacum
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
popsal	popsat	k5eAaPmAgMnS	popsat
Carl	Carl	k1gMnSc1	Carl
von	von	k1gInSc4	von
Linne	Linn	k1gInSc5	Linn
<g/>
)	)	kIx)	)
na	na	k7c6	na
území	území	k1gNnSc6	území
ČR	ČR	kA	ČR
neroste	růst	k5eNaImIp3nS	růst
a	a	k8xC	a
proto	proto	k8xC	proto
označení	označení	k1gNnSc1	označení
tohoto	tento	k3xDgInSc2	tento
taxonomického	taxonomický	k2eAgInSc2d1	taxonomický
komplexu	komplex	k1gInSc2	komplex
jménem	jméno	k1gNnSc7	jméno
Taraxacum	Taraxacum	k1gInSc1	Taraxacum
officinale	officinale	k6eAd1	officinale
je	být	k5eAaImIp3nS	být
vědecky	vědecky	k6eAd1	vědecky
(	(	kIx(	(
<g/>
nomenklatoricky	nomenklatoricky	k6eAd1	nomenklatoricky
<g/>
)	)	kIx)	)
nesprávné	správný	k2eNgNnSc1d1	nesprávné
<g/>
.	.	kIx.	.
</s>
<s>
Správné	správný	k2eAgNnSc1d1	správné
označení	označení	k1gNnSc1	označení
skupiny	skupina	k1gFnSc2	skupina
zní	znět	k5eAaImIp3nS	znět
Taraxacum	Taraxacum	k1gInSc1	Taraxacum
sect	sect	k2eAgInSc1d1	sect
<g/>
.	.	kIx.	.
</s>
<s>
Ruderalia	Ruderalia	k1gFnSc1	Ruderalia
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejhojnějšími	hojný	k2eAgMnPc7d3	nejhojnější
zástupci	zástupce	k1gMnPc7	zástupce
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
pampelišek	pampeliška	k1gFnPc2	pampeliška
na	na	k7c6	na
našem	náš	k3xOp1gNnSc6	náš
území	území	k1gNnSc6	území
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
pampeliška	pampeliška	k1gFnSc1	pampeliška
křídlatá	křídlatý	k2eAgFnSc1d1	křídlatá
(	(	kIx(	(
<g/>
Taraxacum	Taraxacum	k1gInSc1	Taraxacum
alatum	alatum	k1gNnSc1	alatum
Lindb	Lindb	k1gInSc1	Lindb
<g/>
.	.	kIx.	.
fil.	fil.	k?	fil.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pampeliška	pampeliška	k1gFnSc1	pampeliška
bezzubá	bezzubá	k1gFnSc1	bezzubá
(	(	kIx(	(
<g/>
Taraxacum	Taraxacum	k1gInSc1	Taraxacum
hepaticum	hepaticum	k1gNnSc1	hepaticum
Railons	Railons	k1gInSc1	Railons
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pampeliška	pampeliška	k1gFnSc1	pampeliška
záhadná	záhadný	k2eAgFnSc1d1	záhadná
(	(	kIx(	(
<g/>
Taraxacum	Taraxacum	k1gInSc1	Taraxacum
glossodon	glossodon	k1gNnSc1	glossodon
Sonck	Sonck	k1gInSc1	Sonck
et	et	k?	et
H.	H.	kA	H.
Ø	Ø	k?	Ø
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
pampeliška	pampeliška	k1gFnSc1	pampeliška
upravená	upravený	k2eAgFnSc1d1	upravená
(	(	kIx(	(
<g/>
Taraxacum	Taraxacum	k1gInSc1	Taraxacum
interveniens	interveniens	k1gInSc1	interveniens
Hagl	Hagl	k1gInSc4	Hagl
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nejteplejších	teplý	k2eAgFnPc6d3	nejteplejší
oblastech	oblast	k1gFnPc6	oblast
jižní	jižní	k2eAgFnSc2d1	jižní
Moravy	Morava	k1gFnSc2	Morava
roste	růst	k5eAaImIp3nS	růst
také	také	k9	také
sexuální	sexuální	k2eAgInSc1d1	sexuální
druh	druh	k1gInSc1	druh
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
pampeliška	pampeliška	k1gFnSc1	pampeliška
mnohoúborná	mnohoúborný	k2eAgFnSc1d1	mnohoúborný
(	(	kIx(	(
<g/>
Taraxacum	Taraxacum	k1gInSc1	Taraxacum
linearisquamum	linearisquamum	k1gNnSc1	linearisquamum
van	van	k1gInSc1	van
Soest	Soest	k1gMnSc1	Soest
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
morfologicky	morfologicky	k6eAd1	morfologicky
velmi	velmi	k6eAd1	velmi
variabilní	variabilní	k2eAgInSc1d1	variabilní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Pampeliška	pampeliška	k1gFnSc1	pampeliška
je	být	k5eAaImIp3nS	být
vytrvalá	vytrvalý	k2eAgFnSc1d1	vytrvalá
plevelná	plevelný	k2eAgFnSc1d1	plevelná
bylina	bylina	k1gFnSc1	bylina
vysoká	vysoký	k2eAgFnSc1d1	vysoká
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
40	[number]	k4	40
cm	cm	kA	cm
<g/>
,	,	kIx,	,
s	s	k7c7	s
houževnatým	houževnatý	k2eAgInSc7d1	houževnatý
zásobním	zásobní	k2eAgInSc7d1	zásobní
kořenem	kořen	k1gInSc7	kořen
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
tvoří	tvořit	k5eAaImIp3nP	tvořit
přízemní	přízemní	k2eAgFnSc4d1	přízemní
růžici	růžice	k1gFnSc4	růžice
<g/>
.	.	kIx.	.
</s>
<s>
Květenství	květenství	k1gNnPc1	květenství
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
z	z	k7c2	z
listové	listový	k2eAgFnSc2d1	listová
růžice	růžice	k1gFnSc2	růžice
na	na	k7c6	na
dutých	dutý	k2eAgInPc6d1	dutý
stvolech	stvol	k1gInPc6	stvol
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
je	on	k3xPp3gInPc4	on
zářivě	zářivě	k6eAd1	zářivě
žluté	žlutý	k2eAgInPc4d1	žlutý
květní	květní	k2eAgInPc4d1	květní
úbory	úbor	k1gInPc4	úbor
<g/>
.	.	kIx.	.
</s>
<s>
Květní	květní	k2eAgInPc1d1	květní
úbory	úbor	k1gInPc1	úbor
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgInP	tvořit
až	až	k6eAd1	až
z	z	k7c2	z
200	[number]	k4	200
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
jazykových	jazykový	k2eAgInPc2d1	jazykový
květů	květ	k1gInPc2	květ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kvete	kvést	k5eAaImIp3nS	kvést
od	od	k7c2	od
dubna	duben	k1gInSc2	duben
do	do	k7c2	do
srpna	srpen	k1gInSc2	srpen
<g/>
,	,	kIx,	,
plodem	plod	k1gInSc7	plod
jsou	být	k5eAaImIp3nP	být
nažky	nažka	k1gFnPc4	nažka
s	s	k7c7	s
bílým	bílý	k2eAgNnSc7d1	bílé
padáčkovitým	padáčkovitý	k2eAgNnSc7d1	padáčkovitý
chmýřím	chmýří	k1gNnSc7	chmýří
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
kterému	který	k3yQgInSc3	který
se	se	k3xPyFc4	se
semena	semeno	k1gNnSc2	semeno
velmi	velmi	k6eAd1	velmi
snadno	snadno	k6eAd1	snadno
šíří	šířit	k5eAaImIp3nP	šířit
větrem	vítr	k1gInSc7	vítr
na	na	k7c4	na
velké	velký	k2eAgFnPc4d1	velká
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
rostlina	rostlina	k1gFnSc1	rostlina
je	být	k5eAaImIp3nS	být
prostoupená	prostoupený	k2eAgFnSc1d1	prostoupená
mléčnicemi	mléčnice	k1gFnPc7	mléčnice
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
při	při	k7c6	při
utržení	utržení	k1gNnSc6	utržení
roní	ronit	k5eAaImIp3nP	ronit
bílou	bílý	k2eAgFnSc4d1	bílá
hořkou	hořký	k2eAgFnSc4d1	hořká
šťávu	šťáva	k1gFnSc4	šťáva
–	–	k?	–
latex	latex	k1gInSc1	latex
<g/>
,	,	kIx,	,
zanechávající	zanechávající	k2eAgFnSc1d1	zanechávající
po	po	k7c6	po
zaschnutí	zaschnutí	k1gNnSc6	zaschnutí
na	na	k7c4	na
kůži	kůže	k1gFnSc4	kůže
tmavé	tmavý	k2eAgFnSc2d1	tmavá
skvrny	skvrna	k1gFnSc2	skvrna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
ve	v	k7c6	v
společenstvech	společenstvo	k1gNnPc6	společenstvo
mezofilních	mezofilní	k2eAgFnPc2d1	mezofilní
luk	louka	k1gFnPc2	louka
<g/>
,	,	kIx,	,
na	na	k7c6	na
světlých	světlý	k2eAgFnPc6d1	světlá
sušších	suchý	k2eAgFnPc6d2	sušší
loukách	louka	k1gFnPc6	louka
<g/>
,	,	kIx,	,
mezích	mez	k1gFnPc6	mez
<g/>
,	,	kIx,	,
zahradách	zahrada	k1gFnPc6	zahrada
a	a	k8xC	a
také	také	k9	také
jako	jako	k9	jako
konkurenční	konkurenční	k2eAgInSc1d1	konkurenční
plevel	plevel	k1gInSc1	plevel
na	na	k7c6	na
polích	pole	k1gNnPc6	pole
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nitrofilní	nitrofilní	k2eAgInSc4d1	nitrofilní
druh	druh	k1gInSc4	druh
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
<g/>
,	,	kIx,	,
že	že	k8xS	že
preferuje	preferovat	k5eAaImIp3nS	preferovat
půdy	půda	k1gFnSc2	půda
bohaté	bohatý	k2eAgFnSc2d1	bohatá
na	na	k7c4	na
dusík	dusík	k1gInSc4	dusík
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
dvou	dva	k4xCgNnPc6	dva
desetiletích	desetiletí	k1gNnPc6	desetiletí
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
masivnímu	masivní	k2eAgNnSc3d1	masivní
rozšíření	rozšíření	k1gNnSc3	rozšíření
četnosti	četnost	k1gFnSc2	četnost
pampelišky	pampeliška	k1gFnSc2	pampeliška
lékařské	lékařský	k2eAgFnSc2d1	lékařská
na	na	k7c6	na
polích	pole	k1gNnPc6	pole
a	a	k8xC	a
loukách	louka	k1gFnPc6	louka
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
zapříčiněno	zapříčinit	k5eAaPmNgNnS	zapříčinit
změnou	změna	k1gFnSc7	změna
využití	využití	k1gNnSc2	využití
krajiny	krajina	k1gFnSc2	krajina
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
pastva	pastva	k1gFnSc1	pastva
ustoupila	ustoupit	k5eAaPmAgFnS	ustoupit
či	či	k8xC	či
zcela	zcela	k6eAd1	zcela
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
a	a	k8xC	a
taktéž	taktéž	k?	taktéž
sečení	sečení	k1gNnSc2	sečení
luk	louka	k1gFnPc2	louka
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
mnohem	mnohem	k6eAd1	mnohem
menší	malý	k2eAgFnSc3d2	menší
míře	míra	k1gFnSc3	míra
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Pampeliška	pampeliška	k1gFnSc1	pampeliška
má	mít	k5eAaImIp3nS	mít
tak	tak	k6eAd1	tak
čas	čas	k1gInSc4	čas
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
vysemenit	vysemenit	k5eAaPmF	vysemenit
a	a	k8xC	a
široce	široko	k6eAd1	široko
se	se	k3xPyFc4	se
rozšířit	rozšířit	k5eAaPmF	rozšířit
do	do	k7c2	do
svého	svůj	k3xOyFgNnSc2	svůj
neobhospodařovaného	obhospodařovaný	k2eNgNnSc2d1	obhospodařovaný
okolí	okolí	k1gNnSc2	okolí
<g/>
,	,	kIx,	,
na	na	k7c4	na
louky	louka	k1gFnPc4	louka
ležící	ležící	k2eAgFnPc4d1	ležící
ladem	lado	k1gNnSc7	lado
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
úspěšnému	úspěšný	k2eAgNnSc3d1	úspěšné
šíření	šíření	k1gNnSc3	šíření
přispívá	přispívat	k5eAaImIp3nS	přispívat
ještě	ještě	k9	ještě
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
louky	louka	k1gFnPc1	louka
a	a	k8xC	a
pole	pole	k1gNnPc1	pole
ležící	ležící	k2eAgNnPc1d1	ležící
ladem	ladem	k6eAd1	ladem
byly	být	k5eAaImAgInP	být
dříve	dříve	k6eAd2	dříve
hojně	hojně	k6eAd1	hojně
hnojeny	hnojen	k2eAgFnPc1d1	hnojena
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
pro	pro	k7c4	pro
pampelišku	pampeliška	k1gFnSc4	pampeliška
nacházejí	nacházet	k5eAaImIp3nP	nacházet
příhodné	příhodný	k2eAgFnPc1d1	příhodná
podmínky	podmínka	k1gFnPc1	podmínka
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
bohatých	bohatý	k2eAgFnPc2d1	bohatá
zásob	zásoba	k1gFnPc2	zásoba
dusíku	dusík	k1gInSc2	dusík
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přirozeně	přirozeně	k6eAd1	přirozeně
roste	růst	k5eAaImIp3nS	růst
v	v	k7c6	v
mírném	mírný	k2eAgNnSc6d1	mírné
pásmu	pásmo	k1gNnSc6	pásmo
severní	severní	k2eAgFnSc2d1	severní
polokoule	polokoule	k1gFnSc2	polokoule
(	(	kIx(	(
<g/>
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
,	,	kIx,	,
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
,	,	kIx,	,
Asie	Asie	k1gFnSc1	Asie
<g/>
)	)	kIx)	)
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
vegetačních	vegetační	k2eAgInPc6d1	vegetační
stupních	stupeň	k1gInPc6	stupeň
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
však	však	k9	však
v	v	k7c6	v
mezických	mezický	k2eAgInPc6d1	mezický
biotopech	biotop	k1gInPc6	biotop
<g/>
.	.	kIx.	.
</s>
<s>
Různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
skupiny	skupina	k1gFnSc2	skupina
pampelišky	pampeliška	k1gFnSc2	pampeliška
lékařské	lékařský	k2eAgFnSc2d1	lékařská
byly	být	k5eAaImAgInP	být
zavlečeny	zavleknout	k5eAaPmNgInP	zavleknout
více	hodně	k6eAd2	hodně
méně	málo	k6eAd2	málo
do	do	k7c2	do
všech	všecek	k3xTgFnPc2	všecek
oblastí	oblast	k1gFnPc2	oblast
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
tropů	trop	k1gInPc2	trop
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Brazílie	Brazílie	k1gFnSc2	Brazílie
<g/>
,	,	kIx,	,
Indonésie	Indonésie	k1gFnSc2	Indonésie
<g/>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc2	Austrálie
etc	etc	k?	etc
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnPc1	využití
a	a	k8xC	a
účinky	účinek	k1gInPc1	účinek
==	==	k?	==
</s>
</p>
<p>
<s>
Pampeliška	pampeliška	k1gFnSc1	pampeliška
je	být	k5eAaImIp3nS	být
významná	významný	k2eAgFnSc1d1	významná
léčivka	léčivka	k1gFnSc1	léčivka
<g/>
.	.	kIx.	.
</s>
<s>
Sbíráme	sbírat	k5eAaImIp1nP	sbírat
kořen	kořen	k1gInSc4	kořen
před	před	k7c7	před
rozkvětem	rozkvět	k1gInSc7	rozkvět
rostliny	rostlina	k1gFnSc2	rostlina
(	(	kIx(	(
<g/>
březen	březen	k1gInSc1	březen
–	–	k?	–
duben	duben	k1gInSc1	duben
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
listy	list	k1gInPc1	list
(	(	kIx(	(
<g/>
květen	květen	k1gInSc1	květen
–	–	k?	–
září	září	k1gNnSc2	září
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nať	nať	k1gFnSc1	nať
s	s	k7c7	s
kořenem	kořen	k1gInSc7	kořen
(	(	kIx(	(
<g/>
březen	březen	k1gInSc1	březen
–	–	k?	–
duben	duben	k1gInSc1	duben
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Droga	droga	k1gFnSc1	droga
je	být	k5eAaImIp3nS	být
bez	bez	k7c2	bez
zápachu	zápach	k1gInSc2	zápach
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
mírně	mírně	k6eAd1	mírně
nahořklou	nahořklý	k2eAgFnSc4d1	nahořklá
chuť	chuť	k1gFnSc4	chuť
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
kořene	kořen	k1gInSc2	kořen
lze	lze	k6eAd1	lze
po	po	k7c6	po
usušení	usušení	k1gNnSc6	usušení
<g/>
,	,	kIx,	,
upražení	upražení	k1gNnSc1	upražení
a	a	k8xC	a
rozemletí	rozemletí	k1gNnSc1	rozemletí
připravit	připravit	k5eAaPmF	připravit
hořkou	hořký	k2eAgFnSc4d1	hořká
"	"	kIx"	"
<g/>
pampeliškovou	pampeliškový	k2eAgFnSc4d1	Pampelišková
mouku	mouka	k1gFnSc4	mouka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příznivě	příznivě	k6eAd1	příznivě
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
při	při	k7c6	při
zánětech	zánět	k1gInPc6	zánět
močových	močový	k2eAgFnPc2d1	močová
cest	cesta	k1gFnPc2	cesta
a	a	k8xC	a
při	při	k7c6	při
ledvinových	ledvinový	k2eAgInPc6d1	ledvinový
kamenech	kámen	k1gInPc6	kámen
<g/>
,	,	kIx,	,
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
tvorbu	tvorba	k1gFnSc4	tvorba
a	a	k8xC	a
vylučování	vylučování	k1gNnSc4	vylučování
žluči	žluč	k1gFnSc2	žluč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
také	také	k9	také
významná	významný	k2eAgFnSc1d1	významná
včelařská	včelařský	k2eAgFnSc1d1	včelařská
rostlina	rostlina	k1gFnSc1	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
přípravě	příprava	k1gFnSc3	příprava
bylinkových	bylinkový	k2eAgInPc2d1	bylinkový
salátů	salát	k1gInPc2	salát
nebo	nebo	k8xC	nebo
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
vína	víno	k1gNnSc2	víno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
je	být	k5eAaImIp3nS	být
pampeliška	pampeliška	k1gFnSc1	pampeliška
lékařská	lékařský	k2eAgFnSc1d1	lékařská
(	(	kIx(	(
<g/>
též	též	k9	též
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xC	jako
smetánka	smetánka	k1gFnSc1	smetánka
lékařská	lékařský	k2eAgFnSc1d1	lékařská
<g/>
)	)	kIx)	)
brána	brána	k1gFnSc1	brána
spíše	spíše	k9	spíše
jako	jako	k9	jako
plevel	plevel	k1gFnSc1	plevel
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
samotný	samotný	k2eAgInSc1d1	samotný
její	její	k3xOp3gInSc1	její
druhový	druhový	k2eAgInSc1d1	druhový
název	název	k1gInSc1	název
napovídá	napovídat	k5eAaBmIp3nS	napovídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
významnou	významný	k2eAgFnSc4d1	významná
léčivku	léčivka	k1gFnSc4	léčivka
<g/>
.	.	kIx.	.
</s>
<s>
Doporučována	doporučován	k2eAgFnSc1d1	doporučována
je	být	k5eAaImIp3nS	být
zejména	zejména	k9	zejména
při	při	k7c6	při
zánětech	zánět	k1gInPc6	zánět
močových	močový	k2eAgFnPc2d1	močová
cest	cesta	k1gFnPc2	cesta
<g/>
,	,	kIx,	,
ledvinových	ledvinový	k2eAgInPc6d1	ledvinový
kamenech	kámen	k1gInPc6	kámen
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
při	při	k7c6	při
zánětech	zánět	k1gInPc6	zánět
ledvin	ledvina	k1gFnPc2	ledvina
a	a	k8xC	a
ledvinových	ledvinový	k2eAgInPc6d1	ledvinový
kaméncích	kamének	k1gInPc6	kamének
<g/>
.	.	kIx.	.
</s>
<s>
Pozitivní	pozitivní	k2eAgInPc1d1	pozitivní
účinky	účinek	k1gInPc1	účinek
má	mít	k5eAaImIp3nS	mít
ovšem	ovšem	k9	ovšem
i	i	k9	i
na	na	k7c4	na
trávící	trávící	k2eAgFnSc4d1	trávící
soustavu	soustava	k1gFnSc4	soustava
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
ji	on	k3xPp3gFnSc4	on
lze	lze	k6eAd1	lze
užívat	užívat	k5eAaImF	užívat
při	při	k7c6	při
žaludečních	žaludeční	k2eAgFnPc6d1	žaludeční
poruchách	porucha	k1gFnPc6	porucha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
při	při	k7c6	při
prudkém	prudký	k2eAgInSc6d1	prudký
střevních	střevní	k2eAgFnPc2d1	střevní
kataru	katar	k1gInSc6	katar
<g/>
,	,	kIx,	,
nechutenství	nechutenství	k1gNnSc6	nechutenství
anebo	anebo	k8xC	anebo
zácpě	zácpa	k1gFnSc6	zácpa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Obsahové	obsahový	k2eAgFnPc1d1	obsahová
látky	látka	k1gFnPc1	látka
===	===	k?	===
</s>
</p>
<p>
<s>
Zásobním	zásobní	k2eAgInSc7d1	zásobní
cukrem	cukr	k1gInSc7	cukr
je	být	k5eAaImIp3nS	být
inulin	inulin	k1gInSc1	inulin
<g/>
,	,	kIx,	,
namísto	namísto	k7c2	namísto
u	u	k7c2	u
rostlin	rostlina	k1gFnPc2	rostlina
obvyklého	obvyklý	k2eAgInSc2d1	obvyklý
škrobu	škrob	k1gInSc2	škrob
<g/>
,	,	kIx,	,
inulin	inulin	k1gInSc1	inulin
je	být	k5eAaImIp3nS	být
vhodnější	vhodný	k2eAgInSc1d2	vhodnější
pro	pro	k7c4	pro
diabetiky	diabetik	k1gMnPc4	diabetik
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgFnPc4d1	jiná
hořčiny	hořčina	k1gFnPc4	hořčina
taraxacin	taraxacina	k1gFnPc2	taraxacina
a	a	k8xC	a
taraxasterin	taraxasterin	k1gInSc1	taraxasterin
<g/>
,	,	kIx,	,
třísloviny	tříslovina	k1gFnPc1	tříslovina
a	a	k8xC	a
fytoncidy	fytoncida	k1gFnPc1	fytoncida
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
pampeliška	pampeliška	k1gFnSc1	pampeliška
lékařská	lékařský	k2eAgFnSc1d1	lékařská
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Výukový	výukový	k2eAgInSc1d1	výukový
kurs	kurs	k1gInSc1	kurs
PKR	PKR	kA	PKR
<g/>
/	/	kIx~	/
<g/>
Taraxacum	Taraxacum	k1gInSc1	Taraxacum
sect	sect	k2eAgInSc1d1	sect
<g/>
.	.	kIx.	.
</s>
<s>
Ruderalia	Ruderalia	k1gFnSc1	Ruderalia
ve	v	k7c6	v
Wikiverzitě	Wikiverzita	k1gFnSc6	Wikiverzita
</s>
</p>
<p>
<s>
Pampeliška	pampeliška	k1gFnSc1	pampeliška
lékařská	lékařský	k2eAgFnSc1d1	lékařská
na	na	k7c6	na
Biolibu	Biolib	k1gInSc6	Biolib
</s>
</p>
<p>
<s>
Léčivé	léčivý	k2eAgFnPc1d1	léčivá
rostliny	rostlina	k1gFnPc1	rostlina
</s>
</p>
<p>
<s>
Rostliny	rostlina	k1gFnPc1	rostlina
-	-	kIx~	-
přírodou	příroda	k1gFnSc7	příroda
</s>
</p>
<p>
<s>
Taraxacum	Taraxacum	k1gInSc1	Taraxacum
Fotogalerie	Fotogalerie	k1gFnSc2	Fotogalerie
–	–	k?	–
fotogalerie	fotogalerie	k1gFnSc2	fotogalerie
několika	několik	k4yIc2	několik
evropských	evropský	k2eAgFnPc2d1	Evropská
apomiktických	apomiktický	k2eAgFnPc2d1	apomiktický
mikrospecií	mikrospecie	k1gFnPc2	mikrospecie
pampelišek	pampeliška	k1gFnPc2	pampeliška
</s>
</p>
<p>
<s>
Smetánka	smetánka	k1gFnSc1	smetánka
lékařská	lékařský	k2eAgFnSc1d1	lékařská
</s>
</p>
