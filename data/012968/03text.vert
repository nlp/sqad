<p>
<s>
Blauburger	Blauburger	k1gInSc1	Blauburger
je	být	k5eAaImIp3nS	být
středně	středně	k6eAd1	středně
pozdní	pozdní	k2eAgFnSc1d1	pozdní
moštová	moštový	k2eAgFnSc1d1	moštová
odrůda	odrůda	k1gFnSc1	odrůda
révy	réva	k1gFnSc2	réva
vinné	vinný	k2eAgFnSc2d1	vinná
(	(	kIx(	(
<g/>
Vitis	Vitis	k1gFnSc2	Vitis
vinifera	vinifer	k1gMnSc2	vinifer
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
používaná	používaný	k2eAgNnPc4d1	používané
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
červených	červený	k2eAgNnPc2d1	červené
vín	víno	k1gNnPc2	víno
<g/>
.	.	kIx.	.
</s>
<s>
Odrůda	odrůda	k1gFnSc1	odrůda
byla	být	k5eAaImAgFnS	být
vyšlechtěna	vyšlechtit	k5eAaPmNgFnS	vyšlechtit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
ve	v	k7c6	v
Spolkovém	spolkový	k2eAgInSc6d1	spolkový
ústavu	ústav	k1gInSc6	ústav
pro	pro	k7c4	pro
vinařství	vinařství	k1gNnSc4	vinařství
a	a	k8xC	a
ovocnářství	ovocnářství	k1gNnSc4	ovocnářství
v	v	k7c6	v
rakouském	rakouský	k2eAgInSc6d1	rakouský
Klosterneuburgu	Klosterneuburg	k1gInSc6	Klosterneuburg
<g/>
.	.	kIx.	.
</s>
<s>
Odrůdu	odrůda	k1gFnSc4	odrůda
vyšlechtil	vyšlechtit	k5eAaPmAgMnS	vyšlechtit
Fritz	Fritz	k1gMnSc1	Fritz
Zweigelt	Zweigelt	k1gInSc4	Zweigelt
křížením	křížení	k1gNnSc7	křížení
odrůd	odrůda	k1gFnPc2	odrůda
Modrý	modrý	k2eAgInSc1d1	modrý
Portugal	portugal	k1gInSc1	portugal
(	(	kIx(	(
<g/>
Portugieser	Portugieser	k1gInSc1	Portugieser
blau	blau	k5eAaPmIp1nS	blau
<g/>
)	)	kIx)	)
a	a	k8xC	a
Frankovka	frankovka	k1gFnSc1	frankovka
(	(	kIx(	(
<g/>
Blaufränkisch	Blaufränkisch	k1gInSc1	Blaufränkisch
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Réva	réva	k1gFnSc1	réva
vinná	vinný	k2eAgFnSc1d1	vinná
(	(	kIx(	(
<g/>
Vitis	Vitis	k1gFnSc1	Vitis
vinifera	vinifera	k1gFnSc1	vinifera
<g/>
)	)	kIx)	)
odrůda	odrůda	k1gFnSc1	odrůda
Blauburger	Blauburgra	k1gFnPc2	Blauburgra
je	být	k5eAaImIp3nS	být
jednodomá	jednodomý	k2eAgFnSc1d1	jednodomá
dřevitá	dřevitý	k2eAgFnSc1d1	dřevitá
pnoucí	pnoucí	k2eAgFnSc1d1	pnoucí
liána	liána	k1gFnSc1	liána
<g/>
,	,	kIx,	,
dorůstající	dorůstající	k2eAgFnSc1d1	dorůstající
v	v	k7c6	v
kultuře	kultura	k1gFnSc6	kultura
až	až	k9	až
několika	několik	k4yIc2	několik
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Kmen	kmen	k1gInSc1	kmen
tloušťky	tloušťka	k1gFnSc2	tloušťka
až	až	k9	až
několik	několik	k4yIc1	několik
centimetrů	centimetr	k1gInPc2	centimetr
je	být	k5eAaImIp3nS	být
pokryt	pokrýt	k5eAaPmNgInS	pokrýt
světlou	světlý	k2eAgFnSc7d1	světlá
borkou	borka	k1gFnSc7	borka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
loupe	loupat	k5eAaImIp3nS	loupat
v	v	k7c6	v
pruzích	pruh	k1gInPc6	pruh
<g/>
.	.	kIx.	.
</s>
<s>
Úponky	úponek	k1gInPc1	úponek
révy	réva	k1gFnSc2	réva
jsou	být	k5eAaImIp3nP	být
středně	středně	k6eAd1	středně
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
<g/>
,	,	kIx,	,
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
této	tento	k3xDgFnSc3	tento
rostlině	rostlina	k1gFnSc3	rostlina
pnout	pnout	k5eAaImF	pnout
se	se	k3xPyFc4	se
po	po	k7c6	po
pevných	pevný	k2eAgInPc6d1	pevný
předmětech	předmět	k1gInPc6	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Růst	růst	k1gInSc1	růst
je	být	k5eAaImIp3nS	být
středně	středně	k6eAd1	středně
bujný	bujný	k2eAgInSc1d1	bujný
se	s	k7c7	s
vzpřímenými	vzpřímený	k2eAgInPc7d1	vzpřímený
až	až	k8xS	až
polovzpřímenými	polovzpřímený	k2eAgInPc7d1	polovzpřímený
letorosty	letorost	k1gInPc7	letorost
<g/>
.	.	kIx.	.
</s>
<s>
Vrcholky	vrcholek	k1gInPc1	vrcholek
letorostů	letorost	k1gInPc2	letorost
jsou	být	k5eAaImIp3nP	být
otevřené	otevřený	k2eAgFnPc1d1	otevřená
<g/>
,	,	kIx,	,
středně	středně	k6eAd1	středně
silně	silně	k6eAd1	silně
pigmentované	pigmentovaný	k2eAgFnPc1d1	pigmentovaná
antokyaniny	antokyanina	k1gFnPc1	antokyanina
<g/>
,	,	kIx,	,
zelené	zelený	k2eAgInPc1d1	zelený
s	s	k7c7	s
jemným	jemný	k2eAgInSc7d1	jemný
bronzovým	bronzový	k2eAgInSc7d1	bronzový
nádechem	nádech	k1gInSc7	nádech
<g/>
,	,	kIx,	,
slabě	slabě	k6eAd1	slabě
pavučinovitě	pavučinovitě	k6eAd1	pavučinovitě
ochmýřené	ochmýřený	k2eAgInPc1d1	ochmýřený
až	až	k9	až
takřka	takřka	k6eAd1	takřka
lysé	lysý	k2eAgNnSc1d1	lysé
<g/>
.	.	kIx.	.
</s>
<s>
Internodia	internodium	k1gNnPc1	internodium
a	a	k8xC	a
nodia	nodium	k1gNnPc1	nodium
jsou	být	k5eAaImIp3nP	být
zelená	zelené	k1gNnPc4	zelené
s	s	k7c7	s
bronzově	bronzově	k6eAd1	bronzově
červenými	červený	k2eAgInPc7d1	červený
pruhy	pruh	k1gInPc7	pruh
až	až	k9	až
bronzovočerveně	bronzovočerveně	k6eAd1	bronzovočerveně
zbarvená	zbarvený	k2eAgFnSc1d1	zbarvená
<g/>
,	,	kIx,	,
takřka	takřka	k6eAd1	takřka
bez	bez	k7c2	bez
ochmýření	ochmýření	k1gNnSc2	ochmýření
<g/>
,	,	kIx,	,
pupeny	pupen	k1gInPc1	pupen
jsou	být	k5eAaImIp3nP	být
středně	středně	k6eAd1	středně
silně	silně	k6eAd1	silně
pigmentované	pigmentovaný	k2eAgFnPc4d1	pigmentovaná
antokyaniny	antokyanina	k1gFnPc4	antokyanina
<g/>
.	.	kIx.	.
</s>
<s>
Mladé	mladý	k2eAgInPc1d1	mladý
lístky	lístek	k1gInPc1	lístek
jsou	být	k5eAaImIp3nP	být
zelené	zelený	k2eAgInPc1d1	zelený
s	s	k7c7	s
bronzovými	bronzový	k2eAgFnPc7d1	bronzová
skvrnami	skvrna	k1gFnPc7	skvrna
<g/>
,	,	kIx,	,
slabě	slabě	k6eAd1	slabě
pigmentované	pigmentovaný	k2eAgFnSc2d1	pigmentovaná
antokyaniny	antokyanina	k1gFnSc2	antokyanina
<g/>
,	,	kIx,	,
takřka	takřka	k6eAd1	takřka
lysé	lysý	k2eAgFnPc1d1	Lysá
<g/>
.	.	kIx.	.
</s>
<s>
Jednoleté	jednoletý	k2eAgNnSc1d1	jednoleté
réví	réví	k1gNnSc1	réví
je	být	k5eAaImIp3nS	být
eliptického	eliptický	k2eAgInSc2d1	eliptický
průřezu	průřez	k1gInSc2	průřez
<g/>
,	,	kIx,	,
rýhované	rýhovaný	k2eAgInPc1d1	rýhovaný
<g/>
,	,	kIx,	,
žlutavě	žlutavě	k6eAd1	žlutavě
hnědé	hnědý	k2eAgFnPc1d1	hnědá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
List	list	k1gInSc1	list
je	být	k5eAaImIp3nS	být
středně	středně	k6eAd1	středně
velký	velký	k2eAgInSc4d1	velký
<g/>
,	,	kIx,	,
okrouhlý	okrouhlý	k2eAgInSc4d1	okrouhlý
až	až	k8xS	až
pentagonální	pentagonální	k2eAgInSc4d1	pentagonální
<g/>
,	,	kIx,	,
tří-	tří-	k?	tří-
až	až	k8xS	až
pětilaločnatý	pětilaločnatý	k2eAgInSc4d1	pětilaločnatý
s	s	k7c7	s
mělkými	mělký	k2eAgInPc7d1	mělký
výkroji	výkroj	k1gInPc7	výkroj
<g/>
,	,	kIx,	,
na	na	k7c6	na
líci	líc	k1gInSc6	líc
sytě	sytě	k6eAd1	sytě
zelený	zelený	k2eAgInSc1d1	zelený
<g/>
,	,	kIx,	,
jemně	jemně	k6eAd1	jemně
síťovitě	síťovitě	k6eAd1	síťovitě
vrásčitý	vrásčitý	k2eAgInSc1d1	vrásčitý
<g/>
,	,	kIx,	,
slabě	slabě	k6eAd1	slabě
zvlněný	zvlněný	k2eAgMnSc1d1	zvlněný
<g/>
,	,	kIx,	,
středně	středně	k6eAd1	středně
silně	silně	k6eAd1	silně
puchýřnatý	puchýřnatý	k2eAgInSc1d1	puchýřnatý
<g/>
,	,	kIx,	,
na	na	k7c6	na
rubu	rub	k1gInSc6	rub
jemně	jemně	k6eAd1	jemně
plstnatý	plstnatý	k2eAgInSc4d1	plstnatý
až	až	k8xS	až
ochlupený	ochlupený	k2eAgInSc4d1	ochlupený
<g/>
,	,	kIx,	,
okraj	okraj	k1gInSc4	okraj
listu	list	k1gInSc2	list
je	být	k5eAaImIp3nS	být
tupě	tupě	k6eAd1	tupě
zoubkovaný	zoubkovaný	k2eAgInSc1d1	zoubkovaný
<g/>
.	.	kIx.	.
</s>
<s>
Řapíkový	řapíkový	k2eAgInSc1d1	řapíkový
výkroj	výkroj	k1gInSc1	výkroj
je	být	k5eAaImIp3nS	být
lyrovitý	lyrovitý	k2eAgInSc1d1	lyrovitý
<g/>
,	,	kIx,	,
otevřený	otevřený	k2eAgInSc1d1	otevřený
až	až	k9	až
mírně	mírně	k6eAd1	mírně
překrytý	překrytý	k2eAgInSc1d1	překrytý
s	s	k7c7	s
průsvitem	průsvit	k1gInSc7	průsvit
<g/>
,	,	kIx,	,
většinou	většina	k1gFnSc7	většina
s	s	k7c7	s
ostrým	ostrý	k2eAgInSc7d1	ostrý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
někdy	někdy	k6eAd1	někdy
se	s	k7c7	s
zaobleným	zaoblený	k2eAgInSc7d1	zaoblený
dnem	den	k1gInSc7	den
<g/>
,	,	kIx,	,
řapík	řapík	k1gInSc1	řapík
je	být	k5eAaImIp3nS	být
kratší	krátký	k2eAgInSc1d2	kratší
než	než	k8xS	než
medián	medián	k1gInSc1	medián
listu	list	k1gInSc2	list
<g/>
,	,	kIx,	,
načervenalý	načervenalý	k2eAgMnSc1d1	načervenalý
<g/>
,	,	kIx,	,
žilnatina	žilnatina	k1gFnSc1	žilnatina
listu	list	k1gInSc2	list
je	být	k5eAaImIp3nS	být
obrvená	obrvený	k2eAgFnSc1d1	obrvená
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
napojení	napojení	k1gNnSc2	napojení
řapíku	řapík	k1gInSc2	řapík
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
rubu	rub	k1gInSc6	rub
slabě	slabě	k6eAd1	slabě
pigmentovaná	pigmentovaný	k2eAgFnSc1d1	pigmentovaná
antokyaniny	antokyanina	k1gFnPc1	antokyanina
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
se	se	k3xPyFc4	se
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
zbarvují	zbarvovat	k5eAaImIp3nP	zbarvovat
červenooranžově	červenooranžově	k6eAd1	červenooranžově
až	až	k9	až
tmavočerveně	tmavočerveně	k6eAd1	tmavočerveně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oboupohlavní	oboupohlavní	k2eAgInPc4d1	oboupohlavní
pětičetné	pětičetný	k2eAgInPc4d1	pětičetný
květy	květ	k1gInPc4	květ
v	v	k7c6	v
hroznovitých	hroznovitý	k2eAgNnPc6d1	hroznovité
květenstvích	květenství	k1gNnPc6	květenství
jsou	být	k5eAaImIp3nP	být
žlutozelené	žlutozelený	k2eAgInPc1d1	žlutozelený
<g/>
,	,	kIx,	,
samosprašné	samosprašný	k2eAgInPc1d1	samosprašný
<g/>
.	.	kIx.	.
</s>
<s>
Hrozny	hrozen	k1gInPc1	hrozen
jsou	být	k5eAaImIp3nP	být
středně	středně	k6eAd1	středně
velké	velký	k2eAgInPc4d1	velký
až	až	k8xS	až
velké	velký	k2eAgInPc4d1	velký
(	(	kIx(	(
<g/>
146	[number]	k4	146
g	g	kA	g
<g/>
,	,	kIx,	,
14-18	[number]	k4	14-18
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
válcovitě-kuželovité	válcovitěuželovitý	k2eAgFnSc3d1	válcovitě-kuželovitý
a	a	k8xC	a
husté	hustý	k2eAgFnSc3d1	hustá
<g/>
,	,	kIx,	,
občas	občas	k6eAd1	občas
s	s	k7c7	s
křidélky	křidélko	k1gNnPc7	křidélko
<g/>
,	,	kIx,	,
s	s	k7c7	s
delší	dlouhý	k2eAgFnSc7d2	delší
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
průměrně	průměrně	k6eAd1	průměrně
lignifikovanou	lignifikovaný	k2eAgFnSc7d1	lignifikovaný
stopkou	stopka	k1gFnSc7	stopka
<g/>
,	,	kIx,	,
bobule	bobule	k1gFnPc1	bobule
jsou	být	k5eAaImIp3nP	být
středně	středně	k6eAd1	středně
<g />
.	.	kIx.	.
</s>
<s>
velké	velký	k2eAgNnSc1d1	velké
až	až	k9	až
velké	velký	k2eAgNnSc1d1	velké
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
mm	mm	kA	mm
<g/>
,	,	kIx,	,
2	[number]	k4	2
g	g	kA	g
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
okrouhlé	okrouhlý	k2eAgFnPc1d1	okrouhlá
<g/>
,	,	kIx,	,
černomodré	černomodrý	k2eAgFnPc1d1	černomodrá
<g/>
,	,	kIx,	,
silně	silně	k6eAd1	silně
ojíněné	ojíněný	k2eAgFnPc1d1	ojíněná
<g/>
,	,	kIx,	,
se	s	k7c7	s
silnou	silný	k2eAgFnSc7d1	silná
a	a	k8xC	a
pevnou	pevný	k2eAgFnSc7d1	pevná
slupkou	slupka	k1gFnSc7	slupka
<g/>
,	,	kIx,	,
dužina	dužina	k1gFnSc1	dužina
je	být	k5eAaImIp3nS	být
šťavnatá	šťavnatý	k2eAgFnSc1d1	šťavnatá
a	a	k8xC	a
rozplývavá	rozplývavý	k2eAgFnSc1d1	rozplývavá
<g/>
,	,	kIx,	,
nezbarvená	zbarvený	k2eNgFnSc1d1	nezbarvená
<g/>
,	,	kIx,	,
plné	plný	k2eAgFnPc1d1	plná
<g/>
,	,	kIx,	,
neutrální	neutrální	k2eAgFnPc1d1	neutrální
chuti	chuť	k1gFnPc1	chuť
<g/>
.	.	kIx.	.
</s>
<s>
Stopečky	stopečka	k1gFnPc1	stopečka
bobulí	bobule	k1gFnPc2	bobule
jsou	být	k5eAaImIp3nP	být
krátké	krátký	k2eAgFnPc1d1	krátká
<g/>
,	,	kIx,	,
obtížně	obtížně	k6eAd1	obtížně
oddělitelné	oddělitelný	k2eAgFnPc1d1	oddělitelná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Původ	původ	k1gInSc1	původ
a	a	k8xC	a
rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Blauburger	Blauburger	k1gInSc1	Blauburger
je	být	k5eAaImIp3nS	být
středně	středně	k6eAd1	středně
pozdní	pozdní	k2eAgFnSc1d1	pozdní
<g/>
,	,	kIx,	,
moštová	moštový	k2eAgFnSc1d1	moštová
odrůda	odrůda	k1gFnSc1	odrůda
révy	réva	k1gFnSc2	réva
vinné	vinný	k2eAgFnSc2d1	vinná
(	(	kIx(	(
<g/>
Vitis	Vitis	k1gFnSc2	Vitis
vinifera	vinifer	k1gMnSc2	vinifer
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
vyšlechtěna	vyšlechtěn	k2eAgFnSc1d1	vyšlechtěna
roku	rok	k1gInSc2	rok
1923	[number]	k4	1923
bývalým	bývalý	k2eAgMnSc7d1	bývalý
ředitelem	ředitel	k1gMnSc7	ředitel
vinařské	vinařský	k2eAgFnSc2d1	vinařská
školy	škola	k1gFnSc2	škola
a	a	k8xC	a
vinařského	vinařský	k2eAgInSc2d1	vinařský
ústavu	ústav	k1gInSc2	ústav
(	(	kIx(	(
<g/>
HBLAuBA	HBLAuBA	k1gFnSc1	HBLAuBA
<g/>
)	)	kIx)	)
v	v	k7c6	v
rakouském	rakouský	k2eAgInSc6d1	rakouský
Klosterneuburgu	Klosterneuburg	k1gInSc6	Klosterneuburg
<g/>
,	,	kIx,	,
Fritzem	Fritz	k1gInSc7	Fritz
Zweigeltem	Zweigelt	k1gInSc7	Zweigelt
<g/>
,	,	kIx,	,
křížením	křížení	k1gNnSc7	křížení
odrůd	odrůda	k1gFnPc2	odrůda
Modrý	modrý	k2eAgInSc1d1	modrý
Portugal	portugal	k1gInSc1	portugal
(	(	kIx(	(
<g/>
Portugieser	Portugieser	k1gInSc1	Portugieser
blau	blau	k5eAaPmIp1nS	blau
<g/>
)	)	kIx)	)
a	a	k8xC	a
Frankovka	frankovka	k1gFnSc1	frankovka
(	(	kIx(	(
<g/>
Blaufränkisch	Blaufränkisch	k1gInSc1	Blaufränkisch
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
byla	být	k5eAaImAgFnS	být
odrůda	odrůda	k1gFnSc1	odrůda
pěstována	pěstován	k2eAgFnSc1d1	pěstována
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
na	na	k7c6	na
903	[number]	k4	903
hektarech	hektar	k1gInPc6	hektar
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
činí	činit	k5eAaImIp3nS	činit
2	[number]	k4	2
%	%	kIx~	%
rozlohy	rozloha	k1gFnPc1	rozloha
rakouských	rakouský	k2eAgFnPc2d1	rakouská
vinic	vinice	k1gFnPc2	vinice
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
polovina	polovina	k1gFnSc1	polovina
osazených	osazený	k2eAgFnPc2d1	osazená
ploch	plocha	k1gFnPc2	plocha
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
regionu	region	k1gInSc6	region
Niederösterreich	Niederösterreicha	k1gFnPc2	Niederösterreicha
(	(	kIx(	(
<g/>
Dolní	dolní	k2eAgNnSc1d1	dolní
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čtvrtina	čtvrtina	k1gFnSc1	čtvrtina
je	být	k5eAaImIp3nS	být
potom	potom	k6eAd1	potom
v	v	k7c6	v
regionu	region	k1gInSc6	region
Burgenland	Burgenland	k1gInSc1	Burgenland
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgFnPc1d2	menší
plochy	plocha	k1gFnPc1	plocha
vinic	vinice	k1gFnPc2	vinice
jsou	být	k5eAaImIp3nP	být
též	též	k9	též
v	v	k7c6	v
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
<g/>
,	,	kIx,	,
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
cca	cca	kA	cca
20	[number]	k4	20
ha	ha	kA	ha
<g/>
,	,	kIx,	,
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
bylo	být	k5eAaImAgNnS	být
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
necelých	celý	k2eNgInPc2d1	necelý
5	[number]	k4	5
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
Státní	státní	k2eAgFnSc6d1	státní
odrůdové	odrůdový	k2eAgFnSc6d1	odrůdová
knize	kniha	k1gFnSc6	kniha
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
není	být	k5eNaImIp3nS	být
odrůda	odrůda	k1gFnSc1	odrůda
zapsána	zapsat	k5eAaPmNgFnS	zapsat
<g/>
,	,	kIx,	,
probíhá	probíhat	k5eAaImIp3nS	probíhat
registrační	registrační	k2eAgNnSc1d1	registrační
řízení	řízení	k1gNnSc1	řízení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
ukončeno	ukončit	k5eAaPmNgNnS	ukončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Vinařský	vinařský	k2eAgInSc1d1	vinařský
zákon	zákon	k1gInSc1	zákon
povoloval	povolovat	k5eAaImAgInS	povolovat
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
odrůdy	odrůda	k1gFnSc2	odrůda
vyrábět	vyrábět	k5eAaImF	vyrábět
víno	víno	k1gNnSc4	víno
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
zemských	zemský	k2eAgFnPc2d1	zemská
vín	vína	k1gFnPc2	vína
(	(	kIx(	(
<g/>
byla	být	k5eAaImAgFnS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
ve	v	k7c6	v
vyhlášce	vyhláška	k1gFnSc6	vyhláška
č.	č.	k?	č.
437	[number]	k4	437
<g/>
/	/	kIx~	/
<g/>
2005	[number]	k4	2005
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
ve	v	k7c6	v
vyhlášce	vyhláška	k1gFnSc6	vyhláška
č.	č.	k?	č.
323	[number]	k4	323
<g/>
/	/	kIx~	/
<g/>
2004	[number]	k4	2004
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
po	po	k7c6	po
novele	novela	k1gFnSc6	novela
vyhláškou	vyhláška	k1gFnSc7	vyhláška
č.	č.	k?	č.
28	[number]	k4	28
<g/>
/	/	kIx~	/
<g/>
2010	[number]	k4	2010
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
již	již	k6eAd1	již
mezi	mezi	k7c7	mezi
odrůdami	odrůda	k1gFnPc7	odrůda
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yIgFnPc2	který
je	být	k5eAaImIp3nS	být
dovoleno	dovolen	k2eAgNnSc1d1	dovoleno
vyrábět	vyrábět	k5eAaImF	vyrábět
zemská	zemský	k2eAgNnPc4d1	zemské
vína	víno	k1gNnPc4	víno
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
vína	vína	k1gFnSc1	vína
z	z	k7c2	z
odrůdy	odrůda	k1gFnSc2	odrůda
Blauburger	Blauburger	k1gInSc1	Blauburger
objevují	objevovat	k5eAaImIp3nP	objevovat
i	i	k9	i
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
na	na	k7c6	na
oblastních	oblastní	k2eAgFnPc6d1	oblastní
výstavách	výstava	k1gFnPc6	výstava
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Pokusná	pokusný	k2eAgFnSc1d1	pokusná
výsadba	výsadba	k1gFnSc1	výsadba
byla	být	k5eAaImAgFnS	být
nedávno	nedávno	k6eAd1	nedávno
provedena	provést	k5eAaPmNgFnS	provést
ve	v	k7c6	v
vinohradech	vinohrad	k1gInPc6	vinohrad
vinařství	vinařství	k1gNnSc2	vinařství
Mikrosvín	Mikrosvín	k1gMnSc1	Mikrosvín
Mikulov	Mikulov	k1gInSc1	Mikulov
v	v	k7c6	v
Dolních	dolní	k2eAgFnPc6d1	dolní
Dunajovicích	Dunajovice	k1gFnPc6	Dunajovice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Název	název	k1gInSc1	název
==	==	k?	==
</s>
</p>
<p>
<s>
Název	název	k1gInSc1	název
odrůdy	odrůda	k1gFnSc2	odrůda
je	být	k5eAaImIp3nS	být
složený	složený	k2eAgMnSc1d1	složený
ze	z	k7c2	z
slov	slovo	k1gNnPc2	slovo
"	"	kIx"	"
<g/>
blau	blau	k6eAd1	blau
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
modrý	modrý	k2eAgInSc1d1	modrý
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
burger	burger	k1gInSc1	burger
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ve	v	k7c6	v
významu	význam	k1gInSc6	význam
"	"	kIx"	"
<g/>
pocházející	pocházející	k2eAgFnPc1d1	pocházející
z	z	k7c2	z
Klosterneuburgu	Klosterneuburg	k1gInSc2	Klosterneuburg
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pracovní	pracovní	k2eAgInPc1d1	pracovní
názvy	název	k1gInPc1	název
odrůdy	odrůda	k1gFnSc2	odrůda
jsou	být	k5eAaImIp3nP	být
Klosterneuburg	Klosterneuburg	k1gInSc4	Klosterneuburg
181-2	[number]	k4	181-2
či	či	k8xC	či
Zweigeltrebe	Zweigeltreb	k1gInSc5	Zweigeltreb
181	[number]	k4	181
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pěstování	pěstování	k1gNnSc1	pěstování
==	==	k?	==
</s>
</p>
<p>
<s>
Mrazuodolnost	Mrazuodolnost	k1gFnSc1	Mrazuodolnost
je	být	k5eAaImIp3nS	být
střední	střední	k2eAgFnSc1d1	střední
až	až	k6eAd1	až
nižší	nízký	k2eAgFnSc1d2	nižší
<g/>
,	,	kIx,	,
odrůda	odrůda	k1gFnSc1	odrůda
někdy	někdy	k6eAd1	někdy
trpí	trpět	k5eAaImIp3nS	trpět
zasycháním	zasychání	k1gNnSc7	zasychání
třapiny	třapina	k1gFnSc2	třapina
<g/>
.	.	kIx.	.
</s>
<s>
Hodí	hodit	k5eAaImIp3nS	hodit
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
většinu	většina	k1gFnSc4	většina
vedení	vedení	k1gNnSc2	vedení
<g/>
,	,	kIx,	,
snáší	snášet	k5eAaImIp3nS	snášet
dobře	dobře	k6eAd1	dobře
i	i	k9	i
krátký	krátký	k2eAgInSc4d1	krátký
řez	řez	k1gInSc4	řez
<g/>
,	,	kIx,	,
vhodné	vhodný	k2eAgFnPc4d1	vhodná
jsou	být	k5eAaImIp3nP	být
podnože	podnož	k1gFnPc1	podnož
SO-	SO-	k1gFnSc1	SO-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
T	T	kA	T
5	[number]	k4	5
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
125	[number]	k4	125
AA	AA	kA	AA
i	i	k8xC	i
K	K	kA	K
5	[number]	k4	5
<g/>
BB	BB	kA	BB
<g/>
.	.	kIx.	.
</s>
<s>
Výnosy	výnos	k1gInPc1	výnos
jsou	být	k5eAaImIp3nP	být
vyšší	vysoký	k2eAgInPc1d2	vyšší
<g/>
,	,	kIx,	,
9-14	[number]	k4	9-14
t	t	k?	t
<g/>
/	/	kIx~	/
<g/>
ha	ha	kA	ha
při	při	k7c6	při
cukernatosti	cukernatost	k1gFnSc6	cukernatost
moštu	mošt	k1gInSc2	mošt
17-19	[number]	k4	17-19
°	°	k?	°
<g/>
NM	NM	kA	NM
(	(	kIx(	(
<g/>
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
75-90	[number]	k4	75-90
°	°	k?	°
<g/>
Oe	Oe	k1gFnSc2	Oe
<g/>
)	)	kIx)	)
a	a	k8xC	a
aciditě	acidita	k1gFnSc6	acidita
8-10	[number]	k4	8-10
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
l.	l.	k?	l.
</s>
</p>
<p>
<s>
===	===	k?	===
Fenologie	fenologie	k1gFnSc2	fenologie
===	===	k?	===
</s>
</p>
<p>
<s>
Rašení	rašení	k1gNnSc1	rašení
je	být	k5eAaImIp3nS	být
středně	středně	k6eAd1	středně
pozdní	pozdní	k2eAgInSc1d1	pozdní
až	až	k8xS	až
pozdní	pozdní	k2eAgInSc1d1	pozdní
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
doba	doba	k1gFnSc1	doba
květu	květ	k1gInSc2	květ
<g/>
,	,	kIx,	,
sklizňové	sklizňový	k2eAgFnSc2d1	sklizňová
zralosti	zralost	k1gFnSc2	zralost
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
tato	tento	k3xDgFnSc1	tento
středně	středně	k6eAd1	středně
pozdní	pozdní	k2eAgFnSc1d1	pozdní
odrůda	odrůda	k1gFnSc1	odrůda
koncem	koncem	k7c2	koncem
září	září	k1gNnSc2	září
až	až	k9	až
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
října	říjen	k1gInSc2	říjen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Choroby	choroba	k1gFnPc1	choroba
a	a	k8xC	a
škůdci	škůdce	k1gMnPc1	škůdce
===	===	k?	===
</s>
</p>
<p>
<s>
Odolnost	odolnost	k1gFnSc1	odolnost
k	k	k7c3	k
houbovým	houbový	k2eAgFnPc3d1	houbová
chorobám	choroba	k1gFnPc3	choroba
je	být	k5eAaImIp3nS	být
obecně	obecně	k6eAd1	obecně
průměrná	průměrný	k2eAgFnSc1d1	průměrná
<g/>
,	,	kIx,	,
zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
náchylnost	náchylnost	k1gFnSc1	náchylnost
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
padlí	padlí	k1gNnSc3	padlí
révovému	révový	k2eAgInSc3d1	révový
(	(	kIx(	(
<g/>
Uncinula	Uncinula	k1gMnSc1	Uncinula
necator	necator	k1gMnSc1	necator
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
plísní	plísnit	k5eAaImIp3nP	plísnit
révovou	révový	k2eAgFnSc4d1	révová
(	(	kIx(	(
<g/>
Plasmopara	Plasmopara	k1gFnSc1	Plasmopara
viticola	viticola	k1gFnSc1	viticola
<g/>
)	)	kIx)	)
odrůda	odrůda	k1gFnSc1	odrůda
příliš	příliš	k6eAd1	příliš
netrpí	trpět	k5eNaImIp3nS	trpět
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
odolává	odolávat	k5eAaImIp3nS	odolávat
plísni	plíseň	k1gFnSc3	plíseň
šedé	šedý	k2eAgFnSc3d1	šedá
(	(	kIx(	(
<g/>
Botrytis	Botrytis	k1gFnSc3	Botrytis
cinerea	cinereum	k1gNnSc2	cinereum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Poloha	poloha	k1gFnSc1	poloha
a	a	k8xC	a
půdy	půda	k1gFnSc2	půda
===	===	k?	===
</s>
</p>
<p>
<s>
Odrůda	odrůda	k1gFnSc1	odrůda
není	být	k5eNaImIp3nS	být
příliš	příliš	k6eAd1	příliš
náročná	náročný	k2eAgFnSc1d1	náročná
na	na	k7c4	na
půdy	půda	k1gFnPc4	půda
<g/>
,	,	kIx,	,
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
střední	střední	k2eAgFnPc4d1	střední
až	až	k8xS	až
lepší	dobrý	k2eAgFnPc4d2	lepší
polohy	poloha	k1gFnPc4	poloha
<g/>
,	,	kIx,	,
snáší	snášet	k5eAaImIp3nS	snášet
i	i	k9	i
půdy	půda	k1gFnPc4	půda
s	s	k7c7	s
vyšším	vysoký	k2eAgInSc7d2	vyšší
obsahem	obsah	k1gInSc7	obsah
vápníku	vápník	k1gInSc2	vápník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Víno	víno	k1gNnSc1	víno
==	==	k?	==
</s>
</p>
<p>
<s>
Odrůda	odrůda	k1gFnSc1	odrůda
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
vína	vína	k1gFnSc1	vína
neutrálního	neutrální	k2eAgInSc2d1	neutrální
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
hluboké	hluboký	k2eAgFnPc4d1	hluboká
tmavočervené	tmavočervený	k2eAgFnPc4d1	tmavočervená
barvy	barva	k1gFnPc4	barva
s	s	k7c7	s
černým	černý	k2eAgInSc7d1	černý
odstínem	odstín	k1gInSc7	odstín
<g/>
,	,	kIx,	,
sametová	sametový	k2eAgFnSc1d1	sametová
<g/>
,	,	kIx,	,
bohatá	bohatý	k2eAgFnSc1d1	bohatá
na	na	k7c4	na
extrakt	extrakt	k1gInSc4	extrakt
a	a	k8xC	a
obsah	obsah	k1gInSc4	obsah
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
nižší	nízký	k2eAgFnSc7d2	nižší
aciditou	acidita	k1gFnSc7	acidita
a	a	k8xC	a
nižším	nízký	k2eAgInSc7d2	nižší
obsahem	obsah	k1gInSc7	obsah
taninů	tanin	k1gInPc2	tanin
<g/>
,	,	kIx,	,
s	s	k7c7	s
příjemným	příjemný	k2eAgInSc7d1	příjemný
<g/>
,	,	kIx,	,
nevtíravým	vtíravý	k2eNgInSc7d1	nevtíravý
buketem	buket	k1gInSc7	buket
<g/>
,	,	kIx,	,
připomínajícím	připomínající	k2eAgInSc7d1	připomínající
vína	víno	k1gNnPc4	víno
odrůdy	odrůda	k1gFnSc2	odrůda
Frankovka	frankovka	k1gFnSc1	frankovka
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
vhodná	vhodný	k2eAgFnSc1d1	vhodná
pro	pro	k7c4	pro
zrání	zrání	k1gNnSc4	zrání
v	v	k7c6	v
nových	nový	k2eAgInPc6d1	nový
dubových	dubový	k2eAgInPc6d1	dubový
sudech	sud	k1gInPc6	sud
typu	typ	k1gInSc2	typ
barrique	barriquat	k5eAaPmIp3nS	barriquat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rychle	rychle	k6eAd1	rychle
vyzrávají	vyzrávat	k5eAaImIp3nP	vyzrávat
a	a	k8xC	a
nejsou	být	k5eNaImIp3nP	být
příliš	příliš	k6eAd1	příliš
vhodná	vhodný	k2eAgNnPc1d1	vhodné
k	k	k7c3	k
delší	dlouhý	k2eAgFnSc3d2	delší
archivaci	archivace	k1gFnSc3	archivace
<g/>
.	.	kIx.	.
</s>
<s>
Typická	typický	k2eAgFnSc1d1	typická
odrůdová	odrůdový	k2eAgFnSc1d1	odrůdová
vína	vína	k1gFnSc1	vína
mají	mít	k5eAaImIp3nP	mít
v	v	k7c4	v
aroma	aroma	k1gNnSc4	aroma
červené	červený	k2eAgNnSc4d1	červené
bobulové	bobulový	k2eAgNnSc4d1	bobulové
ovoce	ovoce	k1gNnSc4	ovoce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vína	vína	k1gFnSc1	vína
z	z	k7c2	z
hroznů	hrozen	k1gInPc2	hrozen
<g/>
,	,	kIx,	,
pěstovaných	pěstovaný	k2eAgInPc2d1	pěstovaný
v	v	k7c6	v
nepříliš	příliš	k6eNd1	příliš
výhodných	výhodný	k2eAgFnPc6d1	výhodná
polohách	poloha	k1gFnPc6	poloha
či	či	k8xC	či
z	z	k7c2	z
neredukovaných	redukovaný	k2eNgInPc2d1	neredukovaný
výnosů	výnos	k1gInPc2	výnos
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
nevýrazná	výrazný	k2eNgFnSc1d1	nevýrazná
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
Blauburger	Blauburger	k1gInSc1	Blauburger
nejčastěji	často	k6eAd3	často
používán	používat	k5eAaImNgInS	používat
jako	jako	k8xC	jako
barvířka	barvířka	k1gFnSc1	barvířka
pro	pro	k7c4	pro
povýšení	povýšení	k1gNnSc4	povýšení
barvy	barva	k1gFnSc2	barva
různých	různý	k2eAgFnPc2d1	různá
cuvée	cuvée	k1gFnPc2	cuvée
červených	červený	k2eAgNnPc2d1	červené
vín	víno	k1gNnPc2	víno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Pierre	Pierr	k1gMnSc5	Pierr
Galet	Galet	k?	Galet
:	:	kIx,	:
Dictionnaire	Dictionnair	k1gMnSc5	Dictionnair
encyclopédique	encyclopédiquus	k1gMnSc5	encyclopédiquus
des	des	k1gNnSc3	des
cépages	cépages	k1gInSc1	cépages
<g/>
.	.	kIx.	.
</s>
<s>
Hachette	Hachette	k5eAaPmIp2nP	Hachette
<g/>
,	,	kIx,	,
Paris	Paris	k1gMnSc1	Paris
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
236331	[number]	k4	236331
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jancis	Jancis	k1gFnSc1	Jancis
Robinson	Robinson	k1gMnSc1	Robinson
:	:	kIx,	:
Das	Das	k1gFnSc1	Das
Oxford	Oxford	k1gInSc1	Oxford
Weinlexikon	Weinlexikon	k1gInSc1	Weinlexikon
<g/>
,	,	kIx,	,
Hallwag	Hallwag	k1gInSc1	Hallwag
<g/>
,	,	kIx,	,
Gräfe	Gräf	k1gInSc5	Gräf
und	und	k?	und
Unzer	Unzer	k1gInSc1	Unzer
<g/>
,	,	kIx,	,
München	München	k1gInSc1	München
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
8338	[number]	k4	8338
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
691	[number]	k4	691
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Přehled	přehled	k1gInSc1	přehled
odrůd	odrůda	k1gFnPc2	odrůda
révy	réva	k1gFnSc2	réva
2011	[number]	k4	2011
</s>
</p>
<p>
<s>
O	o	k7c6	o
víně	víno	k1gNnSc6	víno
<g/>
,	,	kIx,	,
nezávislý	závislý	k2eNgInSc1d1	nezávislý
portál	portál	k1gInSc1	portál
</s>
</p>
<p>
<s>
Austrian	Austrian	k1gInSc1	Austrian
wine	winat	k5eAaPmIp3nS	winat
</s>
</p>
<p>
<s>
Weltweite	Weltweit	k1gMnSc5	Weltweit
Rebsortenbeschreibung	Rebsortenbeschreibung	k1gMnSc1	Rebsortenbeschreibung
</s>
</p>
<p>
<s>
Vitis	Vitis	k1gInSc1	Vitis
International	International	k1gFnSc2	International
Variety	varieta	k1gFnSc2	varieta
Catalogue	Catalogu	k1gFnSc2	Catalogu
VIVC	VIVC	kA	VIVC
Geilweilerhof	Geilweilerhof	k1gMnSc1	Geilweilerhof
</s>
</p>
<p>
<s>
===	===	k?	===
Multimédia	multimédium	k1gNnPc1	multimédium
===	===	k?	===
</s>
</p>
<p>
<s>
Ing.	ing.	kA	ing.
Radek	Radek	k1gMnSc1	Radek
Sotolář	Sotolář	k1gMnSc1	Sotolář
:	:	kIx,	:
Multimediální	multimediální	k2eAgInSc1d1	multimediální
atlas	atlas	k1gInSc1	atlas
podnožových	podnožový	k2eAgFnPc2d1	podnožová
<g/>
,	,	kIx,	,
moštových	moštový	k2eAgFnPc2d1	moštová
a	a	k8xC	a
stolních	stolní	k2eAgFnPc2d1	stolní
odrůd	odrůda	k1gFnPc2	odrůda
révy	réva	k1gFnSc2	réva
<g/>
,	,	kIx,	,
Mendelova	Mendelův	k2eAgFnSc1d1	Mendelova
zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
a	a	k8xC	a
lesnická	lesnický	k2eAgFnSc1d1	lesnická
universita	universita	k1gFnSc1	universita
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
zahradnická	zahradnický	k2eAgFnSc1d1	zahradnická
fakulta	fakulta	k1gFnSc1	fakulta
v	v	k7c6	v
Lednici	Lednice	k1gFnSc6	Lednice
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
Martin	Martin	k1gMnSc1	Martin
Šimek	Šimek	k1gMnSc1	Šimek
:	:	kIx,	:
Encyklopédie	Encyklopédie	k1gFnSc1	Encyklopédie
všemožnejch	všemožnejch	k?	všemožnejch
odrůd	odrůda	k1gFnPc2	odrůda
révy	réva	k1gFnSc2	réva
vinné	vinný	k2eAgFnSc2d1	vinná
z	z	k7c2	z
celýho	celýho	k?	celýho
světa	svět	k1gInSc2	svět
s	s	k7c7	s
přihlédnutím	přihlédnutí	k1gNnSc7	přihlédnutí
k	k	k7c3	k
těm	ten	k3xDgMnPc3	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
již	již	k6eAd1	již
ouplně	ouplně	k6eAd1	ouplně
vymizely	vymizet	k5eAaPmAgFnP	vymizet
<g/>
,	,	kIx,	,
2008-2012	[number]	k4	2008-2012
</s>
</p>
