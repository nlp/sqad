<p>
<s>
Ohýbání	ohýbání	k1gNnSc1	ohýbání
(	(	kIx(	(
<g/>
flexe	flexe	k1gFnSc1	flexe
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
modifikace	modifikace	k1gFnSc1	modifikace
tvarů	tvar	k1gInPc2	tvar
slova	slovo	k1gNnSc2	slovo
(	(	kIx(	(
<g/>
přesněji	přesně	k6eAd2	přesně
lexému	lexém	k1gInSc3	lexém
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterými	který	k3yIgInPc7	který
se	se	k3xPyFc4	se
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
mluvnické	mluvnický	k2eAgFnSc2d1	mluvnická
kategorie	kategorie	k1gFnSc2	kategorie
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
pád	pád	k1gInSc1	pád
<g/>
,	,	kIx,	,
číslo	číslo	k1gNnSc1	číslo
<g/>
,	,	kIx,	,
rod	rod	k1gInSc1	rod
<g/>
,	,	kIx,	,
čas	čas	k1gInSc1	čas
<g/>
,	,	kIx,	,
osoba	osoba	k1gFnSc1	osoba
atd.	atd.	kA	atd.
U	u	k7c2	u
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
se	se	k3xPyFc4	se
ohýbají	ohýbat	k5eAaImIp3nP	ohýbat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
určitý	určitý	k2eAgInSc1d1	určitý
tvar	tvar	k1gInSc1	tvar
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
základní	základní	k2eAgNnSc4d1	základní
(	(	kIx(	(
<g/>
lemma	lemma	k1gNnSc4	lemma
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jednotlivá	jednotlivý	k2eAgNnPc4d1	jednotlivé
slova	slovo	k1gNnPc4	slovo
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
slovní	slovní	k2eAgInPc1d1	slovní
druhy	druh	k1gInPc1	druh
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
na	na	k7c4	na
ohebná	ohebný	k2eAgNnPc4d1	ohebné
a	a	k8xC	a
neohebná	ohebný	k2eNgNnPc4d1	neohebné
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
mluvnické	mluvnický	k2eAgFnPc4d1	mluvnická
kategorie	kategorie	k1gFnPc4	kategorie
pomocí	pomocí	k7c2	pomocí
flexe	flexe	k1gFnSc2	flexe
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
ohebných	ohebný	k2eAgNnPc2d1	ohebné
slov	slovo	k1gNnPc2	slovo
existují	existovat	k5eAaImIp3nP	existovat
dva	dva	k4xCgInPc4	dva
typy	typ	k1gInPc4	typ
flexe	flexe	k1gFnSc1	flexe
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
skloňování	skloňování	k1gNnSc1	skloňování
(	(	kIx(	(
<g/>
deklinace	deklinace	k1gFnSc1	deklinace
<g/>
)	)	kIx)	)
–	–	k?	–
obvykle	obvykle	k6eAd1	obvykle
u	u	k7c2	u
podstatných	podstatný	k2eAgFnPc2d1	podstatná
jmen	jméno	k1gNnPc2	jméno
<g/>
,	,	kIx,	,
přídavných	přídavný	k2eAgNnPc2d1	přídavné
jmen	jméno	k1gNnPc2	jméno
<g/>
,	,	kIx,	,
zájmen	zájmeno	k1gNnPc2	zájmeno
a	a	k8xC	a
číslovek	číslovka	k1gFnPc2	číslovka
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
časování	časování	k1gNnSc1	časování
(	(	kIx(	(
<g/>
konjugace	konjugace	k1gFnSc1	konjugace
<g/>
)	)	kIx)	)
–	–	k?	–
u	u	k7c2	u
sloves	sloveso	k1gNnPc2	sloveso
<g/>
.	.	kIx.	.
</s>
<s>
Flexe	flexe	k1gFnSc1	flexe
se	se	k3xPyFc4	se
hojně	hojně	k6eAd1	hojně
využívá	využívat	k5eAaImIp3nS	využívat
ve	v	k7c6	v
flektivních	flektivní	k2eAgInPc6d1	flektivní
<g/>
,	,	kIx,	,
aglutinačních	aglutinační	k2eAgInPc6d1	aglutinační
a	a	k8xC	a
introflektivních	introflektivní	k2eAgInPc6d1	introflektivní
jazycích	jazyk	k1gInPc6	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Flektivní	flektivní	k2eAgInSc1d1	flektivní
jazyk	jazyk	k1gInSc1	jazyk
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Flexe	flexe	k1gFnSc2	flexe
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
ohýbání	ohýbání	k1gNnSc2	ohýbání
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
