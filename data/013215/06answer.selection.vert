<s>
Velmi	velmi	k6eAd1
dobře	dobře	k6eAd1
plave	plavat	k5eAaImIp3nS
a	a	k8xC
obratně	obratně	k6eAd1
se	se	k3xPyFc4
i	i	k9
potápí	potápět	k5eAaImIp3nS
pod	pod	k7c4
vodní	vodní	k2eAgFnSc4d1
hladinu	hladina	k1gFnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
pátrá	pátrat	k5eAaImIp3nS
po	po	k7c6
potravě	potrava	k1gFnSc6
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4
nejčastěji	často	k6eAd3
tvoří	tvořit	k5eAaImIp3nP
malé	malý	k2eAgFnPc1d1
ryby	ryba	k1gFnPc1
<g/>
,	,	kIx,
vodní	vodní	k2eAgFnPc1d1
rostliny	rostlina	k1gFnPc1
nebo	nebo	k8xC
měkkýši	měkkýš	k1gMnPc1
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
i	i	k9
mrkev	mrkev	k1gFnSc1
<g/>
,	,	kIx,
jablka	jablko	k1gNnPc1
a	a	k8xC
ořechy	ořech	k1gInPc1
<g/>
.	.	kIx.
</s>