<p>
<s>
Ondatra	ondatra	k1gFnSc1	ondatra
pižmová	pižmový	k2eAgFnSc1d1	pižmová
(	(	kIx(	(
<g/>
Ondatra	ondatra	k1gFnSc1	ondatra
zibethicus	zibethicus	k1gMnSc1	zibethicus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
savec	savec	k1gMnSc1	savec
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
křečkovitých	křečkovitý	k2eAgMnPc2d1	křečkovitý
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
byl	být	k5eAaImAgInS	být
zavlečen	zavleknout	k5eAaPmNgInS	zavleknout
ze	z	k7c2	z
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
vodního	vodní	k2eAgMnSc4d1	vodní
savce	savec	k1gMnSc4	savec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
obývá	obývat	k5eAaImIp3nS	obývat
řeky	řeka	k1gFnSc2	řeka
<g/>
,	,	kIx,	,
jezera	jezero	k1gNnSc2	jezero
i	i	k8xC	i
rybníky	rybník	k1gInPc1	rybník
na	na	k7c6	na
celém	celý	k2eAgNnSc6d1	celé
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
nejvyšších	vysoký	k2eAgNnPc2d3	nejvyšší
pohoří	pohoří	k1gNnPc2	pohoří
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Způsob	způsob	k1gInSc1	způsob
života	život	k1gInSc2	život
==	==	k?	==
</s>
</p>
<p>
<s>
Staví	stavit	k5eAaPmIp3nP	stavit
si	se	k3xPyFc3	se
nory	nora	k1gFnPc4	nora
ve	v	k7c6	v
březích	břeh	k1gInPc6	břeh
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vyvádí	vyvádět	k5eAaImIp3nS	vyvádět
až	až	k9	až
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
ročně	ročně	k6eAd1	ročně
kolem	kolem	k7c2	kolem
5	[number]	k4	5
až	až	k9	až
14	[number]	k4	14
mláďat	mládě	k1gNnPc2	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
plave	plavat	k5eAaImIp3nS	plavat
a	a	k8xC	a
obratně	obratně	k6eAd1	obratně
se	se	k3xPyFc4	se
i	i	k9	i
potápí	potápět	k5eAaImIp3nS	potápět
pod	pod	k7c4	pod
vodní	vodní	k2eAgFnSc4d1	vodní
hladinu	hladina	k1gFnSc4	hladina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pátrá	pátrat	k5eAaImIp3nS	pátrat
po	po	k7c6	po
potravě	potrava	k1gFnSc6	potrava
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
nejčastěji	často	k6eAd3	často
tvoří	tvořit	k5eAaImIp3nP	tvořit
malé	malý	k2eAgFnPc1d1	malá
ryby	ryba	k1gFnPc1	ryba
<g/>
,	,	kIx,	,
vodní	vodní	k2eAgFnPc1d1	vodní
rostliny	rostlina	k1gFnPc1	rostlina
nebo	nebo	k8xC	nebo
měkkýši	měkkýš	k1gMnPc1	měkkýš
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k9	i
mrkev	mrkev	k1gFnSc1	mrkev
<g/>
,	,	kIx,	,
jablka	jablko	k1gNnPc1	jablko
a	a	k8xC	a
ořechy	ořech	k1gInPc1	ořech
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Stavba	stavba	k1gFnSc1	stavba
těla	tělo	k1gNnSc2	tělo
==	==	k?	==
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
velká	velký	k2eAgFnSc1d1	velká
přibližně	přibližně	k6eAd1	přibližně
30	[number]	k4	30
cm	cm	kA	cm
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
ale	ale	k9	ale
dorůstat	dorůstat	k5eAaImF	dorůstat
mnohem	mnohem	k6eAd1	mnohem
větších	veliký	k2eAgInPc2d2	veliký
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hřbetu	hřbet	k1gInSc6	hřbet
je	být	k5eAaImIp3nS	být
hnědá	hnědý	k2eAgFnSc1d1	hnědá
<g/>
,	,	kIx,	,
zespoda	zespoda	k7c2	zespoda
šedá	šedý	k2eAgNnPc1d1	šedé
<g/>
.	.	kIx.	.
</s>
<s>
Ocas	ocas	k1gInSc1	ocas
je	být	k5eAaImIp3nS	být
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
a	a	k8xC	a
zploštělý	zploštělý	k2eAgInSc4d1	zploštělý
<g/>
,	,	kIx,	,
používaný	používaný	k2eAgInSc4d1	používaný
při	při	k7c6	při
plování	plování	k1gNnSc6	plování
jako	jako	k8xS	jako
kormidlo	kormidlo	k1gNnSc4	kormidlo
<g/>
.	.	kIx.	.
</s>
<s>
Poblíž	poblíž	k7c2	poblíž
pohlavních	pohlavní	k2eAgInPc2d1	pohlavní
orgánů	orgán	k1gInPc2	orgán
má	mít	k5eAaImIp3nS	mít
žlázu	žláza	k1gFnSc4	žláza
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yQgFnSc2	který
vylučuje	vylučovat	k5eAaImIp3nS	vylučovat
pižmo	pižmo	k1gNnSc1	pižmo
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
přezdívá	přezdívat	k5eAaImIp3nS	přezdívat
i	i	k9	i
pižmovka	pižmovka	k1gFnSc1	pižmovka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zavlečení	zavlečení	k1gNnSc1	zavlečení
a	a	k8xC	a
rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Ondatra	ondatra	k1gFnSc1	ondatra
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
vysazena	vysadit	k5eAaPmNgFnS	vysadit
počátkem	počátkem	k7c2	počátkem
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
bylo	být	k5eAaImAgNnS	být
několik	několik	k4yIc4	několik
párů	pár	k1gInPc2	pár
vypuštěno	vypuštěn	k2eAgNnSc4d1	vypuštěno
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Dobříše	Dobříš	k1gFnSc2	Dobříš
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
ondatra	ondatra	k1gFnSc1	ondatra
pižmová	pižmový	k2eAgFnSc1d1	pižmová
invazním	invazní	k2eAgInSc7d1	invazní
druhem	druh	k1gInSc7	druh
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
množící	množící	k2eAgMnSc1d1	množící
savec	savec	k1gMnSc1	savec
rozšířil	rozšířit	k5eAaPmAgMnS	rozšířit
téměř	téměř	k6eAd1	téměř
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Ondatry	ondatra	k1gFnPc1	ondatra
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
často	často	k6eAd1	často
loveny	loven	k2eAgInPc1d1	loven
jako	jako	k8xC	jako
kožešinová	kožešinový	k2eAgNnPc4d1	kožešinové
zvířata	zvíře	k1gNnPc4	zvíře
a	a	k8xC	a
pro	pro	k7c4	pro
maso	maso	k1gNnSc4	maso
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
od	od	k7c2	od
těchto	tento	k3xDgNnPc2	tento
praktik	praktika	k1gFnPc2	praktika
upuštěno	upustit	k5eAaPmNgNnS	upustit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potíže	potíž	k1gFnPc4	potíž
==	==	k?	==
</s>
</p>
<p>
<s>
Při	při	k7c6	při
jejím	její	k3xOp3gNnSc6	její
zavlečení	zavlečení	k1gNnSc6	zavlečení
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
se	s	k7c7	s
(	(	kIx(	(
<g/>
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
u	u	k7c2	u
nutrie	nutrie	k1gFnSc2	nutrie
říční	říční	k2eAgMnSc1d1	říční
(	(	kIx(	(
<g/>
Myocastor	Myocastor	k1gMnSc1	Myocastor
coypus	coypus	k1gMnSc1	coypus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
zavlečené	zavlečený	k2eAgNnSc4d1	zavlečené
z	z	k7c2	z
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
ale	ale	k8xC	ale
z	z	k7c2	z
Jižní	jižní	k2eAgFnSc2d1	jižní
<g/>
)	)	kIx)	)
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nenadálé	nenadálý	k2eAgNnSc4d1	nenadálé
rozšíření	rozšíření	k1gNnSc4	rozšíření
do	do	k7c2	do
volné	volný	k2eAgFnSc2d1	volná
přírody	příroda	k1gFnSc2	příroda
způsobí	způsobit	k5eAaPmIp3nS	způsobit
značné	značný	k2eAgInPc4d1	značný
problémy	problém	k1gInPc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
obava	obava	k1gFnSc1	obava
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
nepotvrdila	potvrdit	k5eNaPmAgFnS	potvrdit
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
ondatra	ondatra	k1gFnSc1	ondatra
pižmová	pižmový	k2eAgFnSc1d1	pižmová
na	na	k7c6	na
většině	většina	k1gFnSc6	většina
míst	místo	k1gNnPc2	místo
Evropy	Evropa	k1gFnSc2	Evropa
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
Skandinávského	skandinávský	k2eAgInSc2d1	skandinávský
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
)	)	kIx)	)
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
zdomácněla	zdomácnět	k5eAaPmAgFnS	zdomácnět
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
běžnou	běžný	k2eAgFnSc7d1	běžná
součástí	součást	k1gFnSc7	součást
našich	náš	k3xOp1gInPc2	náš
mokřadních	mokřadní	k2eAgInPc2d1	mokřadní
ekosystémů	ekosystém	k1gInPc2	ekosystém
<g/>
..	..	k?	..
Při	při	k7c6	při
přemnožení	přemnožení	k1gNnSc6	přemnožení
mohou	moct	k5eAaImIp3nP	moct
však	však	k9	však
ondatry	ondatra	k1gFnSc2	ondatra
způsobit	způsobit	k5eAaPmF	způsobit
škody	škoda	k1gFnPc4	škoda
na	na	k7c6	na
hrázích	hráz	k1gFnPc6	hráz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
ondatra	ondatra	k1gFnSc1	ondatra
pižmová	pižmový	k2eAgFnSc1d1	pižmová
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
ondatra	ondatra	k1gFnSc1	ondatra
pižmová	pižmový	k2eAgFnSc1d1	pižmová
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
ondatra	ondatra	k1gFnSc1	ondatra
pižmová	pižmový	k2eAgFnSc1d1	pižmová
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Ondatra	ondatra	k1gFnSc1	ondatra
v	v	k7c6	v
databázi	databáze	k1gFnSc6	databáze
BioLib	BioLiba	k1gFnPc2	BioLiba
</s>
</p>
