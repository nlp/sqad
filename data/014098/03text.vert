<s>
Australský	australský	k2eAgInSc1d1
dolar	dolar	k1gInSc1
</s>
<s>
Australský	australský	k2eAgInSc1d1
dolaraustralian	dolaraustralian	k1gInSc1
dollar	dollar	k1gInSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
Země	země	k1gFnSc1
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
AustrálieNauru	AustrálieNaur	k1gInSc2
NauruKiribati	NauruKiribati	k1gFnSc2
KiribatiTuvalu	KiribatiTuval	k1gInSc2
Tuvalu	Tuval	k1gInSc2
ISO	ISO	kA
4217	#num#	k4
</s>
<s>
AUD	AUD	kA
Inflace	inflace	k1gFnSc1
</s>
<s>
1,5	1,5	k4
<g/>
%	%	kIx~
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
2015	#num#	k4
odhad	odhad	k1gInSc1
<g/>
)	)	kIx)
Symbol	symbol	k1gInSc1
</s>
<s>
$	$	kIx~
<g/>
;	;	kIx,
A	a	k9
<g/>
$	$	kIx~
<g/>
;	;	kIx,
AU	au	k0
<g/>
$	$	kIx~
Dílčí	dílčí	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
</s>
<s>
cent	cent	k1gInSc1
Mince	mince	k1gFnSc2
</s>
<s>
5	#num#	k4
<g/>
,	,	kIx,
10	#num#	k4
<g/>
,	,	kIx,
20	#num#	k4
<g/>
,	,	kIx,
50	#num#	k4
centů	cent	k1gInPc2
<g/>
;	;	kIx,
1	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
dolary	dolar	k1gInPc7
Bankovky	bankovka	k1gFnPc1
</s>
<s>
5	#num#	k4
<g/>
,	,	kIx,
10	#num#	k4
<g/>
,	,	kIx,
20	#num#	k4
<g/>
,	,	kIx,
50	#num#	k4
<g/>
,	,	kIx,
100	#num#	k4
dolarů	dolar	k1gInPc2
</s>
<s>
Australský	australský	k2eAgInSc1d1
dolar	dolar	k1gInSc1
je	být	k5eAaImIp3nS
oficiální	oficiální	k2eAgFnSc7d1
měnou	měna	k1gFnSc7
Australského	australský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
(	(	kIx(
<g/>
je	být	k5eAaImIp3nS
používán	používat	k5eAaImNgInS
i	i	k9
na	na	k7c6
australských	australský	k2eAgNnPc6d1
obydlených	obydlený	k2eAgNnPc6d1
zámořských	zámořský	k2eAgNnPc6d1
teritoriích	teritorium	k1gNnPc6
<g/>
:	:	kIx,
Kokosové	kokosový	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
<g/>
,	,	kIx,
Vánoční	vánoční	k2eAgInSc1d1
ostrov	ostrov	k1gInSc1
a	a	k8xC
Norfolk	Norfolk	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
tří	tři	k4xCgInPc2
ostrovních	ostrovní	k2eAgInPc2d1
států	stát	k1gInPc2
v	v	k7c6
Pacifiku	Pacifik	k1gInSc6
<g/>
:	:	kIx,
Nauru	Nauro	k1gNnSc6
<g/>
,	,	kIx,
Kiribati	Kiribati	k1gFnSc6
a	a	k8xC
Tuvalu	Tuval	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dva	dva	k4xCgInPc1
z	z	k7c2
těchto	tento	k3xDgInPc2
států	stát	k1gInPc2
(	(	kIx(
<g/>
Kiribati	Kiribati	k1gFnSc4
a	a	k8xC
Tuvalu	Tuvala	k1gFnSc4
<g/>
)	)	kIx)
vydávají	vydávat	k5eAaImIp3nP,k5eAaPmIp3nP
vlastní	vlastní	k2eAgFnPc4d1
mince	mince	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
na	na	k7c6
jejich	jejich	k3xOp3gInSc6
území	území	k1gNnSc6
kolují	kolovat	k5eAaImIp3nP
společně	společně	k6eAd1
s	s	k7c7
australskými	australský	k2eAgFnPc7d1
mincemi	mince	k1gFnPc7
a	a	k8xC
bankovkami	bankovka	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
ISO	ISO	kA
4217	#num#	k4
kód	kód	k1gInSc4
této	tento	k3xDgFnSc2
měny	měna	k1gFnSc2
je	být	k5eAaImIp3nS
AUD	AUD	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
označení	označení	k1gNnSc4
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
symbol	symbol	k1gInSc1
$	$	kIx~
<g/>
,	,	kIx,
často	často	k6eAd1
se	se	k3xPyFc4
však	však	k9
doplňuje	doplňovat	k5eAaImIp3nS
písmenem	písmeno	k1gNnSc7
(	(	kIx(
<g/>
písmeny	písmeno	k1gNnPc7
<g/>
)	)	kIx)
na	na	k7c4
tvar	tvar	k1gInSc4
A	a	k9
<g/>
$	$	kIx~
nebo	nebo	k8xC
AU	au	k0
<g/>
$	$	kIx~
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
předešlo	předejít	k5eAaPmAgNnS
záměně	záměna	k1gFnSc3
s	s	k7c7
jinou	jiný	k2eAgFnSc7d1
měnou	měna	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
také	také	k6eAd1
jmenuje	jmenovat	k5eAaImIp3nS,k5eAaBmIp3nS
„	„	k?
<g/>
dolar	dolar	k1gInSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedna	jeden	k4xCgFnSc1
setina	setina	k1gFnSc1
dolaru	dolar	k1gInSc2
se	se	k3xPyFc4
jmenuje	jmenovat	k5eAaImIp3nS,k5eAaBmIp3nS
cent	cent	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někdy	někdy	k6eAd1
je	být	k5eAaImIp3nS
dolar	dolar	k1gInSc1
nazýván	nazývat	k5eAaImNgInS
též	též	k9
„	„	k?
<g/>
Aussie	Aussie	k1gFnSc1
battler	battler	k1gInSc1
<g/>
“	“	k?
<g/>
;	;	kIx,
v	v	k7c6
nepříznivém	příznivý	k2eNgNnSc6d1
období	období	k1gNnSc6
roků	rok	k1gInPc2
2001	#num#	k4
až	až	k8xS
2002	#num#	k4
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
někdy	někdy	k6eAd1
přezdívalo	přezdívat	k5eAaImAgNnS
„	„	k?
<g/>
pacifické	pacifický	k2eAgNnSc4d1
peso	peso	k1gNnSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Australský	australský	k2eAgInSc1d1
dolar	dolar	k1gInSc1
patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
celosvětově	celosvětově	k6eAd1
nejvýznamnější	významný	k2eAgFnPc4d3
měny	měna	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
šestá	šestý	k4xOgFnSc1
nejčastěji	často	k6eAd3
obchodovaná	obchodovaný	k2eAgFnSc1d1
měna	měna	k1gFnSc1
na	na	k7c6
světě	svět	k1gInSc6
(	(	kIx(
<g/>
po	po	k7c6
americkém	americký	k2eAgInSc6d1
dolaru	dolar	k1gInSc6
<g/>
,	,	kIx,
jenu	jen	k1gInSc6
<g/>
,	,	kIx,
euru	euro	k1gNnSc6
<g/>
,	,	kIx,
britské	britský	k2eAgFnSc3d1
libře	libra	k1gFnSc3
a	a	k8xC
kanadskému	kanadský	k2eAgInSc3d1
dolaru	dolar	k1gInSc3
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přibližně	přibližně	k6eAd1
4	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
%	%	kIx~
celosvětových	celosvětový	k2eAgFnPc2d1
transakcí	transakce	k1gFnPc2
je	být	k5eAaImIp3nS
prováděno	provádět	k5eAaImNgNnS
v	v	k7c6
australské	australský	k2eAgFnSc6d1
měně	měna	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Australský	australský	k2eAgInSc1d1
dolar	dolar	k1gInSc1
je	být	k5eAaImIp3nS
oblíbený	oblíbený	k2eAgInSc1d1
díky	díky	k7c3
minimálním	minimální	k2eAgInPc3d1
zásahům	zásah	k1gInPc3
australské	australský	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
na	na	k7c6
zahraničních	zahraniční	k2eAgInPc6d1
trzích	trh	k1gInPc6
a	a	k8xC
stabilnímu	stabilní	k2eAgNnSc3d1
ekonomickému	ekonomický	k2eAgNnSc3d1
a	a	k8xC
politickému	politický	k2eAgNnSc3d1
prostředí	prostředí	k1gNnSc3
v	v	k7c6
Austrálii	Austrálie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
V	v	k7c6
koloniálním	koloniální	k2eAgNnSc6d1
období	období	k1gNnSc6
používala	používat	k5eAaImAgFnS
Austrálie	Austrálie	k1gFnSc1
měnu	měna	k1gFnSc4
svého	svůj	k3xOyFgMnSc2
kolonizátora	kolonizátor	k1gMnSc2
–	–	k?
britskou	britský	k2eAgFnSc4d1
libru	libra	k1gFnSc4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1909	#num#	k4
byla	být	k5eAaImAgFnS
ustanovena	ustanoven	k2eAgFnSc1d1
australská	australský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
měla	mít	k5eAaImAgFnS
paritní	paritní	k2eAgFnSc4d1
hodnotu	hodnota	k1gFnSc4
k	k	k7c3
britské	britský	k2eAgFnSc3d1
libře	libra	k1gFnSc3
a	a	k8xC
dělila	dělit	k5eAaImAgFnS
se	se	k3xPyFc4
na	na	k7c4
20	#num#	k4
šilinků	šilink	k1gInPc2
nebo	nebo	k8xC
240	#num#	k4
pencí	pence	k1gFnPc2
<g/>
.14	.14	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1966	#num#	k4
vznikl	vzniknout	k5eAaPmAgInS
australský	australský	k2eAgInSc1d1
dolar	dolar	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
vyšel	vyjít	k5eAaPmAgInS
z	z	k7c2
libry	libra	k1gFnSc2
v	v	k7c6
poměru	poměr	k1gInSc6
„	„	k?
<g/>
1	#num#	k4
libra	libra	k1gFnSc1
=	=	kIx~
2	#num#	k4
dolary	dolar	k1gInPc7
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nově	nově	k6eAd1
vzniklý	vzniklý	k2eAgInSc4d1
dolar	dolar	k1gInSc4
už	už	k6eAd1
používal	používat	k5eAaImAgMnS
desítkovou	desítkový	k2eAgFnSc4d1
soustavu	soustava	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Mince	mince	k1gFnPc1
a	a	k8xC
bankovky	bankovka	k1gFnPc1
</s>
<s>
Nominální	nominální	k2eAgFnPc1d1
hodnoty	hodnota	k1gFnPc1
současných	současný	k2eAgFnPc2d1
mincí	mince	k1gFnPc2
jsou	být	k5eAaImIp3nP
5	#num#	k4
<g/>
,	,	kIx,
10	#num#	k4
<g/>
,	,	kIx,
20	#num#	k4
a	a	k8xC
50	#num#	k4
centů	cent	k1gInPc2
a	a	k8xC
dále	daleko	k6eAd2
1	#num#	k4
a	a	k8xC
2	#num#	k4
dolary	dolar	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
období	období	k1gNnSc6
mezi	mezi	k7c7
roky	rok	k1gInPc7
1966	#num#	k4
a	a	k8xC
1991	#num#	k4
byly	být	k5eAaImAgFnP
v	v	k7c6
oběhu	oběh	k1gInSc6
i	i	k9
mince	mince	k1gFnSc1
o	o	k7c6
hodnotách	hodnota	k1gFnPc6
1	#num#	k4
a	a	k8xC
2	#num#	k4
centy	cent	k1gInPc7
<g/>
,	,	kIx,
pro	pro	k7c4
svou	svůj	k3xOyFgFnSc4
malou	malý	k2eAgFnSc4d1
kupní	kupní	k2eAgFnSc4d1
hodnotu	hodnota	k1gFnSc4
však	však	k9
byly	být	k5eAaImAgFnP
staženy	stáhnout	k5eAaPmNgInP
z	z	k7c2
oběhu	oběh	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
rubové	rubový	k2eAgFnSc6d1
straně	strana	k1gFnSc6
všech	všecek	k3xTgFnPc2
mincí	mince	k1gFnPc2
je	být	k5eAaImIp3nS
vyobrazená	vyobrazený	k2eAgFnSc1d1
australská	australský	k2eAgFnSc1d1
královna	královna	k1gFnSc1
Alžběta	Alžběta	k1gFnSc1
II	II	kA
<g/>
..	..	k?
</s>
<s>
Bankovky	bankovka	k1gFnPc1
jsou	být	k5eAaImIp3nP
tištěny	tisknout	k5eAaImNgFnP
v	v	k7c6
hodnotách	hodnota	k1gFnPc6
5	#num#	k4
<g/>
,	,	kIx,
10	#num#	k4
<g/>
,	,	kIx,
20	#num#	k4
<g/>
,	,	kIx,
50	#num#	k4
a	a	k8xC
100	#num#	k4
dolarů	dolar	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1988	#num#	k4
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
Austrálie	Austrálie	k1gFnPc4
první	první	k4xOgFnSc7
zemí	zem	k1gFnSc7
na	na	k7c6
světě	svět	k1gInSc6
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
zavedla	zavést	k5eAaPmAgFnS
polymerové	polymerový	k2eAgFnPc4d1
bankovky	bankovka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgInPc4
dnes	dnes	k6eAd1
platné	platný	k2eAgFnPc1d1
australské	australský	k2eAgFnPc1d1
bankovky	bankovka	k1gFnPc1
jsou	být	k5eAaImIp3nP
z	z	k7c2
polymeru	polymer	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
měn	měna	k1gFnPc2
Austrálie	Austrálie	k1gFnSc2
a	a	k8xC
Oceánie	Oceánie	k1gFnSc2
</s>
<s>
Kiribatský	Kiribatský	k2eAgInSc1d1
dolar	dolar	k1gInSc1
</s>
<s>
Tuvalský	Tuvalský	k2eAgInSc1d1
dolar	dolar	k1gInSc1
</s>
<s>
Aktuální	aktuální	k2eAgInSc1d1
kurz	kurz	k1gInSc1
měny	měna	k1gFnSc2
Australský	australský	k2eAgInSc4d1
dolar	dolar	k1gInSc4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Aktuální	aktuální	k2eAgInSc1d1
kurz	kurz	k1gInSc1
měny	měna	k1gFnSc2
Australský	australský	k2eAgInSc4d1
dolar	dolar	k1gInSc4
Podle	podle	k7c2
ČNB	ČNB	kA
<g/>
:	:	kIx,
</s>
<s>
CZK	CZK	kA
Podle	podle	k7c2
Google	Google	k1gFnSc2
Finance	finance	k1gFnSc2
<g/>
:	:	kIx,
</s>
<s>
CZK	CZK	kA
EUR	euro	k1gNnPc2
USD	USD	kA
Podle	podle	k7c2
Kurzy	kurz	k1gInPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
:	:	kIx,
</s>
<s>
CZK	CZK	kA
(	(	kIx(
<g/>
Graf	graf	k1gInSc1
Banky	banka	k1gFnSc2
<g/>
)	)	kIx)
EUR	euro	k1gNnPc2
USD	USD	kA
Podle	podle	k7c2
Yahoo	Yahoo	k6eAd1
<g/>
!	!	kIx.
</s>
<s desamb="1">
Finance	finance	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
CZK	CZK	kA
EUR	euro	k1gNnPc2
USD	USD	kA
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
INFLATION	INFLATION	kA
RATE	RATE	kA
(	(	kIx(
<g/>
CONSUMER	CONSUMER	kA
PRICES	PRICES	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
World	World	k1gMnSc1
Factbook	Factbook	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
CIA	CIA	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1553	#num#	k4
<g/>
-	-	kIx~
<g/>
8133	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Australský	australský	k2eAgInSc4d1
dolar	dolar	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Australské	australský	k2eAgFnPc1d1
bankovky	bankovka	k1gFnPc1
<g/>
:	:	kIx,
BANKNOTE	BANKNOTE	kA
FEATURES	FEATURES	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reserve	Reserev	k1gFnSc2
Bank	banka	k1gFnPc2
of	of	k?
Australia	Australius	k1gMnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Australské	australský	k2eAgFnSc2d1
mince	mince	k1gFnSc2
<g/>
:	:	kIx,
about	about	k1gInSc1
reverse	reverse	k1gFnSc2
designs	designsa	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Royal	Royal	k1gMnSc1
Australian	Australian	k1gMnSc1
Mint	Mint	k1gMnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
Bankovky	bankovka	k1gFnPc1
Austrálie	Austrálie	k1gFnSc2
</s>
