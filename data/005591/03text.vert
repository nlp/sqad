<s>
Kvarky	kvark	k1gInPc1	kvark
jsou	být	k5eAaImIp3nP	být
podle	podle	k7c2	podle
standardního	standardní	k2eAgInSc2d1	standardní
modelu	model	k1gInSc2	model
částicové	částicový	k2eAgFnSc2d1	částicová
fyziky	fyzika	k1gFnSc2	fyzika
elementární	elementární	k2eAgFnSc2d1	elementární
částice	částice	k1gFnSc2	částice
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yRgFnPc2	který
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
hadrony	hadron	k1gInPc1	hadron
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
například	například	k6eAd1	například
protony	proton	k1gInPc1	proton
a	a	k8xC	a
neutrony	neutron	k1gInPc1	neutron
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
částice	částice	k1gFnPc1	částice
mají	mít	k5eAaImIp3nP	mít
spin	spin	k1gInSc4	spin
1⁄	1⁄	k?	1⁄
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
fermiony	fermion	k1gInPc4	fermion
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
standardního	standardní	k2eAgInSc2d1	standardní
modelu	model	k1gInSc2	model
částicové	částicový	k2eAgFnSc2d1	částicová
fyziky	fyzika	k1gFnSc2	fyzika
nemají	mít	k5eNaImIp3nP	mít
kvarky	kvark	k1gInPc1	kvark
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
strukturu	struktura	k1gFnSc4	struktura
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
leptony	lepton	k1gInPc7	lepton
a	a	k8xC	a
kalibračními	kalibrační	k2eAgInPc7d1	kalibrační
bosony	boson	k1gInPc7	boson
"	"	kIx"	"
<g/>
nejmenší	malý	k2eAgFnPc4d3	nejmenší
<g/>
"	"	kIx"	"
známé	známý	k2eAgFnPc4d1	známá
částice	částice	k1gFnPc4	částice
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yIgInPc2	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
hmota	hmota	k1gFnSc1	hmota
<g/>
.	.	kIx.	.
</s>
<s>
Baryony	Baryona	k1gFnPc1	Baryona
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
proton	proton	k1gInSc1	proton
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
kvarků	kvark	k1gInPc2	kvark
<g/>
,	,	kIx,	,
mezony	mezon	k1gInPc1	mezon
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
pion	pion	k1gInSc1	pion
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
kvarku	kvark	k1gInSc2	kvark
a	a	k8xC	a
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
antikvarku	antikvark	k1gInSc2	antikvark
<g/>
.	.	kIx.	.
</s>
<s>
Teoreticky	teoreticky	k6eAd1	teoreticky
byly	být	k5eAaImAgInP	být
předpovězené	předpovězená	k1gFnSc3	předpovězená
roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
Murray	Murraa	k1gFnSc2	Murraa
Gell-Mannem	Gell-Mann	k1gMnSc7	Gell-Mann
(	(	kIx(	(
<g/>
a	a	k8xC	a
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
něm	on	k3xPp3gMnSc6	on
i	i	k8xC	i
Georgem	Georg	k1gMnSc7	Georg
Zweigem	Zweig	k1gMnSc7	Zweig
<g/>
)	)	kIx)	)
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
vlastnosti	vlastnost	k1gFnPc4	vlastnost
tehdy	tehdy	k6eAd1	tehdy
známých	známý	k2eAgFnPc2d1	známá
částic	částice	k1gFnPc2	částice
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yQnSc4	což
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
dostal	dostat	k5eAaPmAgMnS	dostat
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Pozdější	pozdní	k2eAgInPc1d2	pozdější
objevy	objev	k1gInPc1	objev
dalších	další	k2eAgFnPc2d1	další
částic	částice	k1gFnPc2	částice
si	se	k3xPyFc3	se
vyžádaly	vyžádat	k5eAaPmAgFnP	vyžádat
zavedení	zavedení	k1gNnSc4	zavedení
dodatečných	dodatečný	k2eAgInPc2d1	dodatečný
tří	tři	k4xCgInPc2	tři
kvarků	kvark	k1gInPc2	kvark
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
tedy	tedy	k9	tedy
známe	znát	k5eAaImIp1nP	znát
šest	šest	k4xCc4	šest
druhů	druh	k1gInPc2	druh
kvarků	kvark	k1gInPc2	kvark
<g/>
.	.	kIx.	.
</s>
<s>
Kvarková	Kvarkový	k2eAgFnSc1d1	Kvarkový
struktura	struktura	k1gFnSc1	struktura
byla	být	k5eAaImAgFnS	být
teoreticky	teoreticky	k6eAd1	teoreticky
předpovězena	předpovězet	k5eAaImNgFnS	předpovězet
nezávisle	závisle	k6eNd1	závisle
na	na	k7c4	na
sobě	se	k3xPyFc3	se
M.	M.	kA	M.
Gell-Mannem	Gell-Mann	k1gMnSc7	Gell-Mann
a	a	k8xC	a
G.	G.	kA	G.
Zweigem	Zweig	k1gMnSc7	Zweig
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
potvrzení	potvrzení	k1gNnSc3	potvrzení
na	na	k7c6	na
lineárním	lineární	k2eAgInSc6d1	lineární
urychlovači	urychlovač	k1gInSc6	urychlovač
SLAC	SLAC	kA	SLAC
ve	v	k7c6	v
Stanfordu	Stanford	k1gInSc6	Stanford
při	při	k7c6	při
rozptylových	rozptylový	k2eAgInPc6d1	rozptylový
experimentech	experiment	k1gInPc6	experiment
s	s	k7c7	s
protony	proton	k1gInPc7	proton
<g/>
.	.	kIx.	.
</s>
<s>
Kvarky	kvark	k1gInPc1	kvark
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
nejsou	být	k5eNaImIp3nP	být
pozorovány	pozorovat	k5eAaImNgInP	pozorovat
přímo	přímo	k6eAd1	přímo
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
dlouho	dlouho	k6eAd1	dlouho
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
teoretický	teoretický	k2eAgInSc4d1	teoretický
nástroj	nástroj	k1gInSc4	nástroj
umožňující	umožňující	k2eAgInSc4d1	umožňující
objasnit	objasnit	k5eAaPmF	objasnit
chování	chování	k1gNnSc4	chování
hadronů	hadron	k1gInPc2	hadron
a	a	k8xC	a
mezonů	mezon	k1gInPc2	mezon
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
již	již	k6eAd1	již
chápány	chápán	k2eAgFnPc1d1	chápána
jako	jako	k8xS	jako
reálné	reálný	k2eAgFnPc1d1	reálná
částice	částice	k1gFnPc1	částice
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
lze	lze	k6eAd1	lze
teoreticky	teoreticky	k6eAd1	teoreticky
popsat	popsat	k5eAaPmF	popsat
i	i	k8xC	i
jako	jako	k9	jako
kvazičástice	kvazičástika	k1gFnSc6	kvazičástika
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
kvark	kvark	k1gInSc1	kvark
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
knihy	kniha	k1gFnSc2	kniha
Finnegans	Finnegans	k1gInSc1	Finnegans
Wake	Wake	k1gFnSc4	Wake
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Plačky	plačky	k6eAd1	plačky
nad	nad	k7c7	nad
Finneganem	Finnegan	k1gInSc7	Finnegan
<g/>
)	)	kIx)	)
od	od	k7c2	od
autora	autor	k1gMnSc2	autor
Jamese	Jamese	k1gFnSc2	Jamese
Joyce	Joyce	k1gMnSc2	Joyce
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc4	slovo
kvark	kvark	k1gInSc1	kvark
i	i	k8xC	i
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
názvy	název	k1gInPc1	název
pochází	pocházet	k5eAaImIp3nP	pocházet
od	od	k7c2	od
Gell-Manna	Gell-Mann	k1gInSc2	Gell-Mann
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
nakreslil	nakreslit	k5eAaPmAgMnS	nakreslit
i	i	k9	i
ilustrační	ilustrační	k2eAgInSc4d1	ilustrační
obrázky	obrázek	k1gInPc4	obrázek
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgInP	objevit
první	první	k4xOgInPc1	první
modely	model	k1gInPc1	model
počítající	počítající	k2eAgMnSc1d1	počítající
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
kvarky	kvark	k1gInPc1	kvark
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
z	z	k7c2	z
dalších	další	k2eAgFnPc2d1	další
menších	malý	k2eAgFnPc2d2	menší
částic	částice	k1gFnPc2	částice
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
preonů	preon	k1gInPc2	preon
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
však	však	k9	však
pro	pro	k7c4	pro
tyto	tento	k3xDgFnPc4	tento
teorie	teorie	k1gFnPc4	teorie
neexistuje	existovat	k5eNaImIp3nS	existovat
jediná	jediný	k2eAgFnSc1d1	jediná
experimentální	experimentální	k2eAgFnSc1d1	experimentální
indicie	indicie	k1gFnSc1	indicie
<g/>
,	,	kIx,	,
kvarky	kvark	k1gInPc1	kvark
se	se	k3xPyFc4	se
chovají	chovat	k5eAaImIp3nP	chovat
jako	jako	k9	jako
bodové	bodový	k2eAgFnPc1d1	bodová
až	až	k9	až
do	do	k7c2	do
rozměrů	rozměr	k1gInPc2	rozměr
řádově	řádově	k6eAd1	řádově
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
18	[number]	k4	18
metru	metro	k1gNnSc6	metro
a	a	k8xC	a
tam	tam	k6eAd1	tam
je	být	k5eAaImIp3nS	být
i	i	k9	i
hranice	hranice	k1gFnSc1	hranice
současných	současný	k2eAgFnPc2d1	současná
experimentálních	experimentální	k2eAgFnPc2d1	experimentální
možností	možnost	k1gFnPc2	možnost
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
teoretické	teoretický	k2eAgInPc1d1	teoretický
koncepty	koncept	k1gInPc1	koncept
rozpracovávané	rozpracovávaný	k2eAgInPc1d1	rozpracovávaný
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
od	od	k7c2	od
preonových	preonův	k2eAgFnPc2d1	preonův
teorií	teorie	k1gFnPc2	teorie
liší	lišit	k5eAaImIp3nS	lišit
a	a	k8xC	a
potenciální	potenciální	k2eAgFnSc4d1	potenciální
nebodovost	nebodovost	k1gFnSc4	nebodovost
kvarků	kvark	k1gInPc2	kvark
řeší	řešit	k5eAaImIp3nP	řešit
nejčastěji	často	k6eAd3	často
pomocí	pomocí	k7c2	pomocí
strun	struna	k1gFnPc2	struna
<g/>
.	.	kIx.	.
</s>
<s>
Kvarky	kvark	k1gInPc1	kvark
jsou	být	k5eAaImIp3nP	být
jediné	jediný	k2eAgFnPc1d1	jediná
elementární	elementární	k2eAgFnPc1d1	elementární
částice	částice	k1gFnPc1	částice
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
podléhají	podléhat	k5eAaImIp3nP	podléhat
všem	všecek	k3xTgFnPc3	všecek
známým	známý	k2eAgFnPc3d1	známá
základním	základní	k2eAgFnPc3d1	základní
interakcím	interakce	k1gFnPc3	interakce
<g/>
.	.	kIx.	.
</s>
<s>
Kvarky	kvark	k1gInPc1	kvark
nelze	lze	k6eNd1	lze
při	při	k7c6	při
nízkých	nízký	k2eAgFnPc6d1	nízká
energiích	energie	k1gFnPc6	energie
pozorovat	pozorovat	k5eAaImF	pozorovat
jako	jako	k9	jako
volné	volný	k2eAgFnPc1d1	volná
částice	částice	k1gFnPc1	částice
(	(	kIx(	(
<g/>
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
důsledek	důsledek	k1gInSc1	důsledek
tzv.	tzv.	kA	tzv.
asymptotické	asymptotický	k2eAgFnSc2d1	asymptotická
volnosti	volnost	k1gFnSc2	volnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
rozptylových	rozptylový	k2eAgInPc2d1	rozptylový
experimentů	experiment	k1gInPc2	experiment
a	a	k8xC	a
na	na	k7c6	na
základě	základ	k1gInSc6	základ
symetrií	symetrie	k1gFnPc2	symetrie
ve	v	k7c6	v
vlastnostech	vlastnost	k1gFnPc6	vlastnost
pozorovaných	pozorovaný	k2eAgInPc2d1	pozorovaný
hadronů	hadron	k1gInPc2	hadron
<g/>
.	.	kIx.	.
</s>
<s>
Vysoce	vysoce	k6eAd1	vysoce
energetická	energetický	k2eAgFnSc1d1	energetická
srážka	srážka	k1gFnSc1	srážka
hadronů	hadron	k1gInPc2	hadron
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
jejich	jejich	k3xOp3gNnSc4	jejich
"	"	kIx"	"
<g/>
roztavení	roztavení	k1gNnSc4	roztavení
<g/>
"	"	kIx"	"
a	a	k8xC	a
vznik	vznik	k1gInSc4	vznik
tzv.	tzv.	kA	tzv.
kvark-gluonového	kvarkluonový	k2eAgNnSc2d1	kvark-gluonový
plazmatu	plazma	k1gNnSc2	plazma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gInSc6	on
se	se	k3xPyFc4	se
kvarky	kvark	k1gInPc1	kvark
mohou	moct	k5eAaImIp3nP	moct
pohybovat	pohybovat	k5eAaImF	pohybovat
volně	volně	k6eAd1	volně
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
stav	stav	k1gInSc1	stav
hmoty	hmota	k1gFnSc2	hmota
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
20	[number]	k4	20
<g/>
–	–	k?	–
<g/>
30	[number]	k4	30
mikrosekund	mikrosekunda	k1gFnPc2	mikrosekunda
po	po	k7c6	po
Velkém	velký	k2eAgInSc6d1	velký
třesku	třesk	k1gInSc6	třesk
a	a	k8xC	a
lze	lze	k6eAd1	lze
jej	on	k3xPp3gMnSc4	on
na	na	k7c4	na
extrémně	extrémně	k6eAd1	extrémně
krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
vytvořit	vytvořit	k5eAaPmF	vytvořit
v	v	k7c6	v
částicovém	částicový	k2eAgInSc6d1	částicový
urychlovači	urychlovač	k1gInSc6	urychlovač
<g/>
.	.	kIx.	.
</s>
<s>
Kvarky	kvark	k1gInPc1	kvark
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nP	řadit
k	k	k7c3	k
fermionům	fermion	k1gInPc3	fermion
se	s	k7c7	s
spinem	spin	k1gInSc7	spin
1⁄	1⁄	k?	1⁄
a	a	k8xC	a
baryonovým	baryonův	k2eAgNnSc7d1	baryonův
číslem	číslo	k1gNnSc7	číslo
1⁄	1⁄	k?	1⁄
Ke	k	k7c3	k
každému	každý	k3xTgInSc3	každý
kvarku	kvark	k1gInSc3	kvark
existuje	existovat	k5eAaImIp3nS	existovat
příslušná	příslušný	k2eAgFnSc1d1	příslušná
antičástice	antičástice	k1gFnSc1	antičástice
–	–	k?	–
antikvark	antikvark	k1gInSc1	antikvark
<g/>
.	.	kIx.	.
</s>
<s>
Kvarky	kvark	k1gInPc1	kvark
se	se	k3xPyFc4	se
rozdělují	rozdělovat	k5eAaImIp3nP	rozdělovat
na	na	k7c4	na
šest	šest	k4xCc4	šest
tzv.	tzv.	kA	tzv.
vůní	vůně	k1gFnPc2	vůně
<g/>
.	.	kIx.	.
</s>
<s>
Elektrický	elektrický	k2eAgInSc1d1	elektrický
náboj	náboj	k1gInSc1	náboj
<g/>
,	,	kIx,	,
izospin	izospin	k1gInSc1	izospin
<g/>
,	,	kIx,	,
podivnost	podivnost	k1gFnSc1	podivnost
<g/>
,	,	kIx,	,
půvab	půvab	k1gInSc1	půvab
<g/>
,	,	kIx,	,
krása	krása	k1gFnSc1	krása
a	a	k8xC	a
pravda	pravda	k1gFnSc1	pravda
představují	představovat	k5eAaImIp3nP	představovat
kvantová	kvantový	k2eAgNnPc4d1	kvantové
čísla	číslo	k1gNnPc4	číslo
kvarku	kvark	k1gInSc2	kvark
<g/>
.	.	kIx.	.
</s>
<s>
Kvarky	kvark	k1gInPc4	kvark
řadíme	řadit	k5eAaImIp1nP	řadit
do	do	k7c2	do
generací	generace	k1gFnPc2	generace
neboli	neboli	k8xC	neboli
rodin	rodina	k1gFnPc2	rodina
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
leptony	lepton	k1gInPc4	lepton
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
generaci	generace	k1gFnSc4	generace
tvoří	tvořit	k5eAaImIp3nP	tvořit
kvarky	kvark	k1gInPc1	kvark
u	u	k7c2	u
a	a	k8xC	a
d	d	k?	d
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
nahoru	nahoru	k6eAd1	nahoru
a	a	k8xC	a
dolů	dolů	k6eAd1	dolů
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
druhou	druhý	k4xOgFnSc4	druhý
generaci	generace	k1gFnSc4	generace
tvoří	tvořit	k5eAaImIp3nP	tvořit
kvarky	kvark	k1gInPc1	kvark
s	s	k7c7	s
a	a	k8xC	a
c	c	k0	c
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
podivný	podivný	k2eAgMnSc1d1	podivný
a	a	k8xC	a
půvabný	půvabný	k2eAgMnSc1d1	půvabný
<g/>
)	)	kIx)	)
a	a	k8xC	a
třetí	třetí	k4xOgFnSc4	třetí
generaci	generace	k1gFnSc4	generace
tvoří	tvořit	k5eAaImIp3nP	tvořit
kvarky	kvark	k1gInPc1	kvark
b	b	k?	b
a	a	k8xC	a
t	t	k?	t
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
spodní	spodní	k2eAgFnPc1d1	spodní
a	a	k8xC	a
svrchní	svrchní	k2eAgFnPc1d1	svrchní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
z	z	k7c2	z
kvarků	kvark	k1gInPc2	kvark
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
může	moct	k5eAaImIp3nS	moct
vyskytnou	vyskytnout	k5eAaPmIp3nP	vyskytnout
ve	v	k7c6	v
třech	tři	k4xCgFnPc6	tři
barvách	barva	k1gFnPc6	barva
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgFnPc3	jenž
přiřazujeme	přiřazovat	k5eAaImIp1nP	přiřazovat
hodnoty	hodnota	k1gFnPc1	hodnota
červená	červený	k2eAgFnSc1d1	červená
<g/>
,	,	kIx,	,
zelená	zelený	k2eAgFnSc1d1	zelená
nebo	nebo	k8xC	nebo
modrá	modrý	k2eAgFnSc1d1	modrá
<g/>
.	.	kIx.	.
</s>
<s>
Barvy	barva	k1gFnPc1	barva
slouží	sloužit	k5eAaImIp3nP	sloužit
pouze	pouze	k6eAd1	pouze
k	k	k7c3	k
určitému	určitý	k2eAgNnSc3d1	určité
označení	označení	k1gNnSc3	označení
a	a	k8xC	a
představují	představovat	k5eAaImIp3nP	představovat
jistý	jistý	k2eAgInSc4d1	jistý
druh	druh	k1gInSc4	druh
náboje	náboj	k1gInSc2	náboj
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
barevný	barevný	k2eAgInSc1d1	barevný
náboj	náboj	k1gInSc1	náboj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kvarky	kvark	k1gInPc1	kvark
však	však	k9	však
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
v	v	k7c6	v
žádném	žádný	k3yNgInSc6	žádný
případě	případ	k1gInSc6	případ
nemají	mít	k5eNaImIp3nP	mít
žádnou	žádný	k3yNgFnSc4	žádný
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jsou	být	k5eAaImIp3nP	být
mnohem	mnohem	k6eAd1	mnohem
menší	malý	k2eAgFnPc1d2	menší
než	než	k8xS	než
vlnová	vlnový	k2eAgFnSc1d1	vlnová
délka	délka	k1gFnSc1	délka
viditelného	viditelný	k2eAgNnSc2d1	viditelné
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Barva	barva	k1gFnSc1	barva
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
podobný	podobný	k2eAgInSc4d1	podobný
význam	význam	k1gInSc4	význam
jako	jako	k8xC	jako
např.	např.	kA	např.
elektrický	elektrický	k2eAgInSc4d1	elektrický
náboj	náboj	k1gInSc4	náboj
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Hadron	Hadron	k1gInSc1	Hadron
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
valenčních	valenční	k2eAgInPc2d1	valenční
kvarků	kvark	k1gInPc2	kvark
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
baryon	baryon	k1gInSc1	baryon
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
každý	každý	k3xTgInSc1	každý
kvark	kvark	k1gInSc1	kvark
má	mít	k5eAaImIp3nS	mít
jinou	jiný	k2eAgFnSc4d1	jiná
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
výsledný	výsledný	k2eAgInSc1d1	výsledný
baryon	baryon	k1gInSc1	baryon
je	být	k5eAaImIp3nS	být
bezbarvý	bezbarvý	k2eAgInSc1d1	bezbarvý
<g/>
.	.	kIx.	.
</s>
<s>
Baryon	Baryon	k1gInSc1	Baryon
tedy	tedy	k9	tedy
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
hodnotu	hodnota	k1gFnSc4	hodnota
podivnosti	podivnost	k1gFnSc2	podivnost
až	až	k9	až
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
kvarku	kvark	k1gInSc2	kvark
a	a	k8xC	a
antikvarku	antikvark	k1gInSc2	antikvark
stejné	stejný	k2eAgFnSc2d1	stejná
barvy	barva	k1gFnSc2	barva
vznikají	vznikat	k5eAaImIp3nP	vznikat
mezony	mezon	k1gInPc1	mezon
<g/>
.	.	kIx.	.
</s>
<s>
Barvy	barva	k1gFnPc1	barva
kvarků	kvark	k1gInPc2	kvark
v	v	k7c6	v
mezonu	mezon	k1gInSc6	mezon
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
pravděpodobnost	pravděpodobnost	k1gFnSc4	pravděpodobnost
zachycení	zachycení	k1gNnSc3	zachycení
kterékoli	kterýkoli	k3yIgFnSc2	kterýkoli
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
barev	barva	k1gFnPc2	barva
je	být	k5eAaImIp3nS	být
stejná	stejný	k2eAgFnSc1d1	stejná
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
při	při	k7c6	při
sledování	sledování	k1gNnSc6	sledování
mezonu	mezon	k1gInSc2	mezon
v	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
časovém	časový	k2eAgInSc6d1	časový
intervalu	interval	k1gInSc6	interval
se	se	k3xPyFc4	se
mezon	mezon	k1gInSc1	mezon
také	také	k9	také
jeví	jevit	k5eAaImIp3nS	jevit
jako	jako	k9	jako
bezbarvý	bezbarvý	k2eAgMnSc1d1	bezbarvý
<g/>
.	.	kIx.	.
</s>
<s>
Kvarky	kvark	k1gInPc1	kvark
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
hadronech	hadron	k1gInPc6	hadron
vzájemně	vzájemně	k6eAd1	vzájemně
vázány	vázat	k5eAaImNgFnP	vázat
prostřednictvím	prostřednictví	k1gNnSc7	prostřednictví
gluonů	gluon	k1gInPc2	gluon
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
vlastnosti	vlastnost	k1gFnPc4	vlastnost
částic	částice	k1gFnPc2	částice
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
.	.	kIx.	.
</s>
<s>
Mezon	mezon	k1gInSc1	mezon
K	k	k7c3	k
<g/>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
složen	složit	k5eAaPmNgInS	složit
z	z	k7c2	z
kvarku	kvark	k1gInSc2	kvark
d	d	k?	d
(	(	kIx(	(
<g/>
down	down	k1gMnSc1	down
<g/>
)	)	kIx)	)
a	a	k8xC	a
antikvarku	antikvark	k1gInSc2	antikvark
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
s	s	k7c7	s
̄	̄	k?	̄
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
overline	overlin	k1gInSc5	overlin
{	{	kIx(	{
<g/>
s	s	k7c7	s
<g/>
}}}	}}}	k?	}}}
(	(	kIx(	(
<g/>
má	mít	k5eAaImIp3nS	mít
podivnost	podivnost	k1gFnSc1	podivnost
−	−	k?	−
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
)	)	kIx)	)
Mezon	mezon	k1gInSc4	mezon
K	k	k7c3	k
<g/>
(	(	kIx(	(
<g/>
+	+	kIx~	+
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
složen	složit	k5eAaPmNgInS	složit
z	z	k7c2	z
kvarku	kvark	k1gInSc2	kvark
u	u	k7c2	u
(	(	kIx(	(
<g/>
up	up	k?	up
<g/>
)	)	kIx)	)
a	a	k8xC	a
antikvarku	antikvark	k1gInSc2	antikvark
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
s	s	k7c7	s
̄	̄	k?	̄
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
overline	overlin	k1gInSc5	overlin
{	{	kIx(	{
<g/>
s	s	k7c7	s
<g/>
}}}	}}}	k?	}}}
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
podivnost	podivnost	k1gFnSc1	podivnost
−	−	k?	−
<g/>
)	)	kIx)	)
Mezon	mezon	k1gInSc1	mezon
K	k	k7c3	k
<g/>
(	(	kIx(	(
<g/>
−	−	k?	−
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
složen	složit	k5eAaPmNgInS	složit
z	z	k7c2	z
kvarku	kvark	k1gInSc2	kvark
s	s	k7c7	s
(	(	kIx(	(
<g/>
strange	strange	k1gNnSc7	strange
<g/>
)	)	kIx)	)
a	a	k8xC	a
antikvarku	antikvark	k1gInSc2	antikvark
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
u	u	k7c2	u
̄	̄	k?	̄
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
overline	overlin	k1gInSc5	overlin
{	{	kIx(	{
<g/>
u	u	k7c2	u
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
}}}	}}}	k?	}}}
(	(	kIx(	(
<g/>
má	mít	k5eAaImIp3nS	mít
podivnost	podivnost	k1gFnSc1	podivnost
+1	+1	k4	+1
<g/>
)	)	kIx)	)
Mezonové	mezonový	k2eAgFnSc2d1	mezonový
rezonance	rezonance	k1gFnSc2	rezonance
K	K	kA	K
<g/>
*	*	kIx~	*
mají	mít	k5eAaImIp3nP	mít
stejné	stejný	k2eAgNnSc1d1	stejné
složení	složení	k1gNnSc1	složení
jako	jako	k8xS	jako
mezony	mezon	k1gInPc1	mezon
K.	K.	kA	K.
Mezon	mezon	k1gInSc1	mezon
π	π	k?	π
<g/>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
složení	složení	k1gNnSc1	složení
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
u	u	k7c2	u
:	:	kIx,	:
:	:	kIx,	:
u	u	k7c2	u
̄	̄	k?	̄
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
̄	̄	k?	̄
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
/	/	kIx~	/
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
u	u	k7c2	u
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
overline	overlin	k1gInSc5	overlin
{	{	kIx(	{
<g/>
u	u	k7c2	u
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
d	d	k?	d
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
overline	overlin	k1gInSc5	overlin
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
:	:	kIx,	:
Mezon	mezon	k1gInSc1	mezon
<g />
.	.	kIx.	.
</s>
<s hack="1">
π	π	k?	π
<g/>
(	(	kIx(	(
<g/>
+	+	kIx~	+
<g/>
)	)	kIx)	)
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
u	u	k7c2	u
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
̄	̄	k?	̄
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
overline	overlin	k1gInSc5	overlin
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}}}	}}}	k?	}}}
:	:	kIx,	:
Mezon	mezon	k1gInSc1	mezon
π	π	k?	π
<g/>
(	(	kIx(	(
<g/>
−	−	k?	−
<g/>
)	)	kIx)	)
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
d	d	k?	d
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
u	u	k7c2	u
̄	̄	k?	̄
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
overline	overlin	k1gInSc5	overlin
{	{	kIx(	{
<g/>
u	u	k7c2	u
<g/>
}}}	}}}	k?	}}}
:	:	kIx,	:
Mezonové	mezonový	k2eAgFnPc1d1	mezonový
rezonance	rezonance	k1gFnPc1	rezonance
ρ	ρ	k?	ρ
mají	mít	k5eAaImIp3nP	mít
stejné	stejný	k2eAgNnSc1d1	stejné
složení	složení	k1gNnSc4	složení
jako	jako	k8xS	jako
mezony	mezon	k1gInPc4	mezon
π	π	k?	π
Proton	proton	k1gInSc1	proton
p	p	k?	p
<g/>
(	(	kIx(	(
<g/>
+	+	kIx~	+
<g/>
)	)	kIx)	)
a	a	k8xC	a
baryonové	baryonové	k2eAgFnSc1d1	baryonové
rezonance	rezonance	k1gFnSc1	rezonance
N	N	kA	N
<g/>
(	(	kIx(	(
<g/>
+	+	kIx~	+
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
složeny	složit	k5eAaPmNgInP	složit
z	z	k7c2	z
kvarků	kvark	k1gInPc2	kvark
u	u	k7c2	u
<g/>
,	,	kIx,	,
u	u	k7c2	u
<g/>
,	,	kIx,	,
d	d	k?	d
Neutron	neutron	k1gInSc1	neutron
n	n	k0	n
<g/>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
a	a	k8xC	a
baryonové	baryonové	k2eAgFnSc1d1	baryonové
rezonance	rezonance	k1gFnSc1	rezonance
N	N	kA	N
<g />
.	.	kIx.	.
</s>
<s>
<g/>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
složeny	složit	k5eAaPmNgInP	složit
z	z	k7c2	z
kvarků	kvark	k1gInPc2	kvark
d	d	k?	d
<g/>
,	,	kIx,	,
d	d	k?	d
<g/>
,	,	kIx,	,
u	u	k7c2	u
Baryon	Baryona	k1gFnPc2	Baryona
Δ	Δ	k?	Δ
<g/>
(	(	kIx(	(
<g/>
++	++	k?	++
<g/>
)	)	kIx)	)
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
u	u	k7c2	u
<g/>
,	,	kIx,	,
u	u	k7c2	u
<g/>
,	,	kIx,	,
u	u	k7c2	u
Baryon	Baryona	k1gFnPc2	Baryona
Δ	Δ	k?	Δ
<g/>
(	(	kIx(	(
<g/>
+	+	kIx~	+
<g/>
)	)	kIx)	)
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
u	u	k7c2	u
<g/>
,	,	kIx,	,
u	u	k7c2	u
<g/>
,	,	kIx,	,
d	d	k?	d
Baryon	Baryon	k1gInSc1	Baryon
<g />
.	.	kIx.	.
</s>
<s>
Δ	Δ	k?	Δ
<g/>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
u	u	k7c2	u
<g/>
,	,	kIx,	,
d	d	k?	d
<g/>
,	,	kIx,	,
d	d	k?	d
Baryon	Baryon	k1gInSc1	Baryon
Δ	Δ	k?	Δ
<g/>
(	(	kIx(	(
<g/>
−	−	k?	−
<g/>
)	)	kIx)	)
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
d	d	k?	d
<g/>
,	,	kIx,	,
d	d	k?	d
<g/>
,	,	kIx,	,
d	d	k?	d
Baryon	Baryon	k1gInSc1	Baryon
Σ	Σ	k?	Σ
<g/>
(	(	kIx(	(
<g/>
+	+	kIx~	+
<g/>
)	)	kIx)	)
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
u	u	k7c2	u
<g/>
,	,	kIx,	,
u	u	k7c2	u
<g/>
,	,	kIx,	,
s	s	k7c7	s
Baryon	Baryon	k1gInSc1	Baryon
Σ	Σ	k?	Σ
<g/>
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
a	a	k8xC	a
baryon	baryon	k1gInSc1	baryon
Λ	Λ	k?	Λ
<g/>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
u	u	k7c2	u
<g/>
,	,	kIx,	,
d	d	k?	d
<g/>
,	,	kIx,	,
s	s	k7c7	s
Baryon	Baryon	k1gInSc1	Baryon
Σ	Σ	k?	Σ
<g/>
(	(	kIx(	(
<g/>
−	−	k?	−
<g/>
)	)	kIx)	)
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
d	d	k?	d
<g/>
,	,	kIx,	,
d	d	k?	d
<g/>
,	,	kIx,	,
s	s	k7c7	s
Baryon	Baryon	k1gInSc1	Baryon
Ξ	Ξ	k?	Ξ
<g/>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
d	d	k?	d
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
s	s	k7c7	s
<g/>
,	,	kIx,	,
s	s	k7c7	s
Baryon	Baryon	k1gInSc1	Baryon
Ξ	Ξ	k?	Ξ
<g/>
(	(	kIx(	(
<g/>
−	−	k?	−
<g/>
)	)	kIx)	)
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
u	u	k7c2	u
<g/>
,	,	kIx,	,
s	s	k7c7	s
<g/>
,	,	kIx,	,
s	s	k7c7	s
Baryon	Baryon	k1gInSc1	Baryon
Ω	Ω	k?	Ω
<g/>
(	(	kIx(	(
<g/>
−	−	k?	−
<g/>
)	)	kIx)	)
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
s	s	k7c7	s
<g/>
,	,	kIx,	,
s	s	k7c7	s
<g/>
,	,	kIx,	,
s	s	k7c7	s
Jako	jako	k9	jako
exotické	exotický	k2eAgInPc1d1	exotický
hadrony	hadron	k1gInPc1	hadron
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
nově	nově	k6eAd1	nově
objevené	objevený	k2eAgFnPc1d1	objevená
složené	složený	k2eAgFnPc1d1	složená
částice	částice	k1gFnPc1	částice
<g/>
:	:	kIx,	:
tetrakvarky	tetrakvarka	k1gFnPc1	tetrakvarka
–	–	k?	–
s	s	k7c7	s
celočíselným	celočíselný	k2eAgInSc7d1	celočíselný
spinem	spin	k1gInSc7	spin
<g/>
,	,	kIx,	,
složené	složený	k2eAgNnSc1d1	složené
ze	z	k7c2	z
2	[number]	k4	2
kvarků	kvark	k1gInPc2	kvark
a	a	k8xC	a
2	[number]	k4	2
antikvarků	antikvark	k1gInPc2	antikvark
<g/>
;	;	kIx,	;
pentakvarky	pentakvark	k1gInPc1	pentakvark
–	–	k?	–
s	s	k7c7	s
poločíselným	poločíselný	k2eAgInSc7d1	poločíselný
spinem	spin	k1gInSc7	spin
<g/>
,	,	kIx,	,
složené	složený	k2eAgNnSc1d1	složené
ze	z	k7c2	z
4	[number]	k4	4
kvarků	kvark	k1gInPc2	kvark
a	a	k8xC	a
1	[number]	k4	1
antikvarku	antikvark	k1gInSc2	antikvark
<g/>
;	;	kIx,	;
dibaryony	dibaryona	k1gFnPc1	dibaryona
–	–	k?	–
s	s	k7c7	s
celočíselným	celočíselný	k2eAgInSc7d1	celočíselný
spinem	spin	k1gInSc7	spin
<g/>
,	,	kIx,	,
složené	složený	k2eAgNnSc1d1	složené
ze	z	k7c2	z
6	[number]	k4	6
kvarků	kvark	k1gInPc2	kvark
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
exotické	exotický	k2eAgInPc4d1	exotický
hadrony	hadron	k1gInPc4	hadron
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
silně	silně	k6eAd1	silně
interagující	interagující	k2eAgFnSc2d1	interagující
složené	složený	k2eAgFnSc2d1	složená
částice	částice	k1gFnSc2	částice
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
řadí	řadit	k5eAaImIp3nP	řadit
i	i	k9	i
hypotetické	hypotetický	k2eAgFnPc1d1	hypotetická
částice	částice	k1gFnPc1	částice
obsahující	obsahující	k2eAgInPc4d1	obsahující
kvarky	kvark	k1gInPc4	kvark
nebo	nebo	k8xC	nebo
vázané	vázaný	k2eAgInPc4d1	vázaný
gluonové	gluonový	k2eAgInPc4d1	gluonový
komplexy	komplex	k1gInPc4	komplex
<g/>
:	:	kIx,	:
glueballs	glueballs	k1gInSc4	glueballs
<g/>
/	/	kIx~	/
<g/>
gluebally	glueballa	k1gMnSc2	glueballa
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
zvané	zvaný	k2eAgFnPc1d1	zvaná
též	též	k9	též
gluonia	gluonium	k1gNnSc2	gluonium
<g/>
)	)	kIx)	)
–	–	k?	–
exotické	exotický	k2eAgInPc4d1	exotický
hadrony	hadron	k1gInPc4	hadron
složené	složený	k2eAgInPc4d1	složený
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
gluonů	gluon	k1gInPc2	gluon
<g/>
;	;	kIx,	;
"	"	kIx"	"
<g/>
hybridní	hybridní	k2eAgMnSc1d1	hybridní
<g/>
"	"	kIx"	"
hadrony	hadron	k1gInPc1	hadron
<g/>
,	,	kIx,	,
vázané	vázaný	k2eAgInPc1d1	vázaný
stavy	stav	k1gInPc1	stav
kvarků	kvark	k1gInPc2	kvark
<g/>
/	/	kIx~	/
<g/>
antikvarků	antikvark	k1gInPc2	antikvark
a	a	k8xC	a
(	(	kIx(	(
<g/>
ne	ne	k9	ne
virtuálních	virtuální	k2eAgInPc2d1	virtuální
<g/>
)	)	kIx)	)
gluonů	gluon	k1gInPc2	gluon
<g/>
.	.	kIx.	.
</s>
<s>
Neúspěšné	úspěšný	k2eNgNnSc1d1	neúspěšné
bylo	být	k5eAaImAgNnS	být
dosud	dosud	k6eAd1	dosud
také	také	k9	také
hledání	hledání	k1gNnSc1	hledání
částic	částice	k1gFnPc2	částice
složených	složený	k2eAgFnPc2d1	složená
z	z	k7c2	z
leptonů	lepton	k1gInPc2	lepton
a	a	k8xC	a
kvarků	kvark	k1gInPc2	kvark
jako	jako	k9	jako
jedné	jeden	k4xCgFnSc2	jeden
třídy	třída	k1gFnSc2	třída
leptokvarků	leptokvarek	k1gInPc2	leptokvarek
<g/>
,	,	kIx,	,
hypotetických	hypotetický	k2eAgFnPc2d1	hypotetická
částic	částice	k1gFnPc2	částice
s	s	k7c7	s
nenulovým	nulový	k2eNgNnSc7d1	nenulové
baryonovým	baryonův	k2eAgNnSc7d1	baryonův
i	i	k8xC	i
leptonovým	leptonový	k2eAgNnSc7d1	leptonové
číslem	číslo	k1gNnSc7	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Elementární	elementární	k2eAgFnPc1d1	elementární
částice	částice	k1gFnPc1	částice
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
kvark	kvark	k1gInSc1	kvark
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
kvark	kvark	k1gInSc1	kvark
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Kvarky	kvark	k1gInPc4	kvark
na	na	k7c4	na
aldebaran	aldebaran	k1gInSc4	aldebaran
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Quarks	Quarks	k1gInSc1	Quarks
(	(	kIx(	(
<g/>
HyperPhysics	HyperPhysics	k1gInSc1	HyperPhysics
<g/>
)	)	kIx)	)
</s>
