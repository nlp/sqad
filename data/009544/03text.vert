<p>
<s>
Erijské	Erijský	k2eAgNnSc1d1	Erijské
jezero	jezero	k1gNnSc1	jezero
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Lake	Lake	k1gFnSc1	Lake
Erie	Eri	k1gFnSc2	Eri
<g/>
,	,	kIx,	,
francouzsky	francouzsky	k6eAd1	francouzsky
Lac	Lac	k1gFnSc1	Lac
Érié	Ériá	k1gFnSc2	Ériá
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejjižnější	jižní	k2eAgNnSc1d3	nejjižnější
jezero	jezero	k1gNnSc1	jezero
v	v	k7c6	v
systému	systém	k1gInSc6	systém
pěti	pět	k4xCc2	pět
Velkých	velká	k1gFnPc2	velká
jezer	jezero	k1gNnPc2	jezero
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Jezerem	jezero	k1gNnSc7	jezero
prochází	procházet	k5eAaImIp3nS	procházet
státní	státní	k2eAgFnSc1d1	státní
hranice	hranice	k1gFnSc1	hranice
mezi	mezi	k7c7	mezi
Kanadou	Kanada	k1gFnSc7	Kanada
na	na	k7c6	na
severu	sever	k1gInSc6	sever
(	(	kIx(	(
<g/>
provincie	provincie	k1gFnSc1	provincie
Ontario	Ontario	k1gNnSc4	Ontario
<g/>
)	)	kIx)	)
a	a	k8xC	a
USA	USA	kA	USA
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
(	(	kIx(	(
<g/>
státy	stát	k1gInPc4	stát
Michigan	Michigan	k1gInSc1	Michigan
<g/>
,	,	kIx,	,
Ohio	Ohio	k1gNnSc1	Ohio
<g/>
,	,	kIx,	,
Pennsylvania	Pennsylvanium	k1gNnPc1	Pennsylvanium
a	a	k8xC	a
New	New	k1gFnPc1	New
York	York	k1gInSc1	York
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
25	[number]	k4	25
667	[number]	k4	667
km2	km2	k4	km2
(	(	kIx(	(
<g/>
kanadská	kanadský	k2eAgFnSc1d1	kanadská
část	část	k1gFnSc1	část
12	[number]	k4	12
722	[number]	k4	722
km2	km2	k4	km2
a	a	k8xC	a
americká	americký	k2eAgFnSc1d1	americká
část	část	k1gFnSc1	část
12	[number]	k4	12
945	[number]	k4	945
m2	m2	k4	m2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
388	[number]	k4	388
km	km	kA	km
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
a	a	k8xC	a
maximálně	maximálně	k6eAd1	maximálně
92	[number]	k4	92
km	km	kA	km
široké	široký	k2eAgNnSc1d1	široké
<g/>
.	.	kIx.	.
</s>
<s>
Průměrně	průměrně	k6eAd1	průměrně
je	být	k5eAaImIp3nS	být
hluboké	hluboký	k2eAgFnPc1d1	hluboká
19	[number]	k4	19
m	m	kA	m
a	a	k8xC	a
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
maximální	maximální	k2eAgFnSc2d1	maximální
hloubky	hloubka	k1gFnSc2	hloubka
64	[number]	k4	64
m.	m.	k?	m.
Objem	objem	k1gInSc1	objem
vody	voda	k1gFnSc2	voda
je	být	k5eAaImIp3nS	být
484	[number]	k4	484
km3	km3	k4	km3
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
173	[number]	k4	173
m.	m.	k?	m.
</s>
</p>
<p>
<s>
==	==	k?	==
Pobřeží	pobřeží	k1gNnSc2	pobřeží
==	==	k?	==
</s>
</p>
<p>
<s>
Břehy	břeh	k1gInPc1	břeh
jsou	být	k5eAaImIp3nP	být
vysoké	vysoký	k2eAgInPc1d1	vysoký
a	a	k8xC	a
mírně	mírně	k6eAd1	mírně
členité	členitý	k2eAgNnSc1d1	členité
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ostrovy	ostrov	k1gInPc1	ostrov
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
Erijském	Erijský	k2eAgNnSc6d1	Erijské
jezeře	jezero	k1gNnSc6	jezero
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
americkém	americký	k2eAgNnSc6d1	americké
území	území	k1gNnSc6	území
například	například	k6eAd1	například
ostrovy	ostrov	k1gInPc1	ostrov
Kelleys	Kelleysa	k1gFnPc2	Kelleysa
Island	Island	k1gInSc1	Island
<g/>
,	,	kIx,	,
Middle	Middle	k1gFnSc1	Middle
Bass	Bass	k1gMnSc1	Bass
Island	Island	k1gInSc1	Island
<g/>
,	,	kIx,	,
North	North	k1gInSc1	North
Bass	Bass	k1gMnSc1	Bass
Island	Island	k1gInSc1	Island
<g/>
,	,	kIx,	,
North	North	k1gInSc1	North
Harbour	Harboura	k1gFnPc2	Harboura
Island	Island	k1gInSc1	Island
<g/>
,	,	kIx,	,
Rattlesnake	Rattlesnake	k1gInSc1	Rattlesnake
Island	Island	k1gInSc1	Island
<g/>
,	,	kIx,	,
South	South	k1gMnSc1	South
Bass	Bass	k1gMnSc1	Bass
Island	Island	k1gInSc4	Island
s	s	k7c7	s
městečkem	městečko	k1gNnSc7	městečko
Put-in-Bay	Putn-Baa	k1gFnSc2	Put-in-Baa
<g/>
,	,	kIx,	,
na	na	k7c6	na
kanadském	kanadský	k2eAgNnSc6d1	kanadské
území	území	k1gNnSc6	území
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
především	především	k9	především
Pelee	Pelee	k1gInSc1	Pelee
Island	Island	k1gInSc1	Island
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
ostrovů	ostrov	k1gInPc2	ostrov
Erijského	Erijský	k2eAgNnSc2d1	Erijské
jezera	jezero	k1gNnSc2	jezero
</s>
</p>
<p>
<s>
==	==	k?	==
Vodní	vodní	k2eAgInSc1d1	vodní
režim	režim	k1gInSc1	režim
==	==	k?	==
</s>
</p>
<p>
<s>
Erijské	Erijský	k2eAgNnSc1d1	Erijské
jezero	jezero	k1gNnSc1	jezero
tvoří	tvořit	k5eAaImIp3nS	tvořit
spojnici	spojnice	k1gFnSc4	spojnice
mezi	mezi	k7c7	mezi
Velkými	velký	k2eAgNnPc7d1	velké
jezery	jezero	k1gNnPc7	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Přítok	přítok	k1gInSc1	přítok
přichází	přicházet	k5eAaImIp3nS	přicházet
z	z	k7c2	z
Huronského	Huronský	k2eAgNnSc2d1	Huronské
jezera	jezero	k1gNnSc2	jezero
přes	přes	k7c4	přes
řeku	řeka	k1gFnSc4	řeka
svaté	svatý	k2eAgFnSc2d1	svatá
Kláry	Klára	k1gFnSc2	Klára
(	(	kIx(	(
<g/>
43	[number]	k4	43
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jezero	jezero	k1gNnSc1	jezero
svaté	svatý	k2eAgFnSc2d1	svatá
Kláry	Klára	k1gFnSc2	Klára
a	a	k8xC	a
řeku	řeka	k1gFnSc4	řeka
Detroit	Detroit	k1gInSc1	Detroit
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ústí	ústit	k5eAaImIp3nS	ústit
Erijského	Erijský	k2eAgNnSc2d1	Erijské
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Ontarijského	ontarijský	k2eAgNnSc2d1	Ontarijské
jezera	jezero	k1gNnSc2	jezero
odtéká	odtékat	k5eAaImIp3nS	odtékat
řeka	řeka	k1gFnSc1	řeka
Niagara	Niagara	k1gFnSc1	Niagara
(	(	kIx(	(
<g/>
54	[number]	k4	54
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Lodní	lodní	k2eAgFnSc1d1	lodní
doprava	doprava	k1gFnSc1	doprava
==	==	k?	==
</s>
</p>
<p>
<s>
Jezero	jezero	k1gNnSc1	jezero
spojuje	spojovat	k5eAaImIp3nS	spojovat
lodní	lodní	k2eAgFnSc1d1	lodní
doprava	doprava	k1gFnSc1	doprava
s	s	k7c7	s
ostatními	ostatní	k2eAgNnPc7d1	ostatní
Velkými	velký	k2eAgNnPc7d1	velké
jezery	jezero	k1gNnPc7	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Welland	Welland	k1gInSc1	Welland
Canal	Canal	k1gInSc1	Canal
s	s	k7c7	s
plavebními	plavební	k2eAgFnPc7d1	plavební
komorami	komora	k1gFnPc7	komora
(	(	kIx(	(
<g/>
obchází	obcházet	k5eAaImIp3nP	obcházet
Niagarské	niagarský	k2eAgInPc4d1	niagarský
vodopády	vodopád	k1gInPc4	vodopád
<g/>
)	)	kIx)	)
spojuje	spojovat	k5eAaImIp3nS	spojovat
Erijské	Erijský	k2eAgNnSc1d1	Erijské
a	a	k8xC	a
Ontarijské	ontarijský	k2eAgNnSc1d1	Ontarijské
jezero	jezero	k1gNnSc1	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	řeka	k1gFnSc1	řeka
Niagara	Niagara	k1gFnSc1	Niagara
a	a	k8xC	a
Erijský	Erijský	k2eAgInSc1d1	Erijský
kanál	kanál	k1gInSc1	kanál
spojují	spojovat	k5eAaImIp3nP	spojovat
Erijské	Erijský	k2eAgNnSc1d1	Erijské
jezero	jezero	k1gNnSc1	jezero
s	s	k7c7	s
řekou	řeka	k1gFnSc7	řeka
Hudson	Hudsona	k1gFnPc2	Hudsona
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osídlení	osídlení	k1gNnSc3	osídlení
pobřeží	pobřeží	k1gNnSc3	pobřeží
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
leží	ležet	k5eAaImIp3nP	ležet
města	město	k1gNnPc1	město
a	a	k8xC	a
přístavy	přístav	k1gInPc1	přístav
Buffalo	Buffalo	k1gFnSc2	Buffalo
<g/>
,	,	kIx,	,
Erie	Erie	k1gNnSc1	Erie
<g/>
,	,	kIx,	,
Toledo	Toledo	k1gNnSc1	Toledo
<g/>
,	,	kIx,	,
Monroe	Monroe	k1gFnSc1	Monroe
a	a	k8xC	a
Cleveland	Cleveland	k1gInSc1	Cleveland
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
a	a	k8xC	a
Port	port	k1gInSc1	port
Colborne	Colborn	k1gInSc5	Colborn
(	(	kIx(	(
<g/>
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jezero	jezero	k1gNnSc1	jezero
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
znečištěno	znečištěn	k2eAgNnSc1d1	znečištěno
jak	jak	k8xS	jak
průmyslem	průmysl	k1gInSc7	průmysl
tak	tak	k8xC	tak
i	i	k9	i
domácnostmi	domácnost	k1gFnPc7	domácnost
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
1⁄	1⁄	k?	1⁄
jezera	jezero	k1gNnSc2	jezero
tvoří	tvořit	k5eAaImIp3nP	tvořit
mrtvou	mrtvý	k2eAgFnSc4d1	mrtvá
zónu	zóna	k1gFnSc4	zóna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
ostrovů	ostrov	k1gInPc2	ostrov
Erijského	Erijský	k2eAgNnSc2d1	Erijské
jezera	jezero	k1gNnSc2	jezero
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgFnP	být
použity	použit	k2eAgFnPc1d1	použita
informace	informace	k1gFnPc1	informace
z	z	k7c2	z
Velké	velký	k2eAgFnSc2d1	velká
sovětské	sovětský	k2eAgFnSc2d1	sovětská
encyklopedie	encyklopedie	k1gFnSc2	encyklopedie
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
"	"	kIx"	"
<g/>
Э	Э	k?	Э
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Erijské	Erijský	k2eAgFnSc2d1	Erijský
jezero	jezero	k1gNnSc4	jezero
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
