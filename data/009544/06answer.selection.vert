<s>
Erijské	Erijský	k2eAgNnSc1d1	Erijské
jezero	jezero	k1gNnSc1	jezero
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Lake	Lake	k1gFnSc1	Lake
Erie	Eri	k1gFnSc2	Eri
<g/>
,	,	kIx,	,
francouzsky	francouzsky	k6eAd1	francouzsky
Lac	Lac	k1gFnSc1	Lac
Érié	Ériá	k1gFnSc2	Ériá
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejjižnější	jižní	k2eAgNnSc1d3	nejjižnější
jezero	jezero	k1gNnSc1	jezero
v	v	k7c6	v
systému	systém	k1gInSc6	systém
pěti	pět	k4xCc2	pět
Velkých	velká	k1gFnPc2	velká
jezer	jezero	k1gNnPc2	jezero
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
