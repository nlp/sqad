<s>
Lev	Lev	k1gMnSc1	Lev
Davidovič	Davidovič	k1gMnSc1	Davidovič
Landau	Landaa	k1gFnSc4	Landaa
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
:	:	kIx,	:
Л	Л	k?	Л
<g/>
́	́	k?	́
<g/>
в	в	k?	в
Д	Д	k?	Д
<g/>
́	́	k?	́
<g/>
д	д	k?	д
Л	Л	k?	Л
<g/>
́	́	k?	́
<g/>
у	у	k?	у
<g/>
;	;	kIx,	;
22	[number]	k4	22
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1908	[number]	k4	1908
<g/>
,	,	kIx,	,
Baku	Baku	k1gNnSc1	Baku
-	-	kIx~	-
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
sovětský	sovětský	k2eAgMnSc1d1	sovětský
fyzik	fyzik	k1gMnSc1	fyzik
židovského	židovský	k2eAgInSc2d1	židovský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
přispěl	přispět	k5eAaPmAgInS	přispět
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
mnoha	mnoho	k4c2	mnoho
oblastí	oblast	k1gFnPc2	oblast
teoretické	teoretický	k2eAgFnSc2d1	teoretická
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
jeho	jeho	k3xOp3gInPc7	jeho
úspěchy	úspěch	k1gInPc7	úspěch
patří	patřit	k5eAaImIp3nS	patřit
podíl	podíl	k1gInSc4	podíl
na	na	k7c6	na
formulování	formulování	k1gNnSc6	formulování
matice	matice	k1gFnSc2	matice
hustoty	hustota	k1gFnSc2	hustota
-	-	kIx~	-
metodě	metoda	k1gFnSc3	metoda
kvantové	kvantový	k2eAgFnSc2d1	kvantová
mechaniky	mechanika	k1gFnSc2	mechanika
<g/>
,	,	kIx,	,
kvantově	kvantově	k6eAd1	kvantově
mechanické	mechanický	k2eAgFnPc4d1	mechanická
teorie	teorie	k1gFnPc4	teorie
diamagnetismu	diamagnetismus	k1gInSc2	diamagnetismus
<g/>
,	,	kIx,	,
teorie	teorie	k1gFnSc2	teorie
supratekutosti	supratekutost	k1gFnSc2	supratekutost
<g/>
,	,	kIx,	,
teorie	teorie	k1gFnSc2	teorie
fázových	fázový	k2eAgInPc2d1	fázový
přechodů	přechod	k1gInPc2	přechod
druhého	druhý	k4xOgInSc2	druhý
druhu	druh	k1gInSc2	druh
<g/>
,	,	kIx,	,
Ginzburgovy-Landauovy	Ginzburgovy-Landauův	k2eAgFnSc2d1	Ginzburgovy-Landauův
teorie	teorie	k1gFnSc2	teorie
supravodivosti	supravodivost	k1gFnSc2	supravodivost
<g/>
,	,	kIx,	,
vysvětlení	vysvětlení	k1gNnSc1	vysvětlení
Landauova	Landauův	k2eAgNnSc2d1	Landauův
tlumení	tlumení	k1gNnSc2	tlumení
ve	v	k7c6	v
fyzice	fyzika	k1gFnSc6	fyzika
plazmatu	plazma	k1gNnSc2	plazma
<g/>
,	,	kIx,	,
Landauova	Landauův	k2eAgInSc2d1	Landauův
pólu	pól	k1gInSc2	pól
v	v	k7c6	v
kvantové	kvantový	k2eAgFnSc6d1	kvantová
elektrodynamice	elektrodynamika	k1gFnSc6	elektrodynamika
<g/>
,	,	kIx,	,
a	a	k8xC	a
dvousložkové	dvousložkový	k2eAgFnSc2d1	dvousložková
teorie	teorie	k1gFnSc2	teorie
neutrin	neutrino	k1gNnPc2	neutrino
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
obdržel	obdržet	k5eAaPmAgMnS	obdržet
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
práci	práce	k1gFnSc4	práce
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
supratekutosti	supratekutost	k1gFnSc2	supratekutost
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1908	[number]	k4	1908
v	v	k7c6	v
Baku	Baku	k1gNnSc6	Baku
v	v	k7c6	v
židovské	židovský	k2eAgFnSc6d1	židovská
rodině	rodina	k1gFnSc6	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Landau	Landau	k6eAd1	Landau
byl	být	k5eAaImAgInS	být
velice	velice	k6eAd1	velice
matematicky	matematicky	k6eAd1	matematicky
nadán	nadán	k2eAgInSc1d1	nadán
a	a	k8xC	a
sám	sám	k3xTgMnSc1	sám
o	o	k7c6	o
sobě	se	k3xPyFc3	se
říkal	říkat	k5eAaImAgMnS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
integrovat	integrovat	k5eAaBmF	integrovat
se	se	k3xPyFc4	se
naučil	naučit	k5eAaPmAgInS	naučit
ve	v	k7c6	v
13	[number]	k4	13
letech	let	k1gInPc6	let
a	a	k8xC	a
derivovat	derivovat	k5eAaBmF	derivovat
uměl	umět	k5eAaImAgMnS	umět
od	od	k7c2	od
nepaměti	nepaměť	k1gFnSc2	nepaměť
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
14	[number]	k4	14
letech	léto	k1gNnPc6	léto
začal	začít	k5eAaPmAgMnS	začít
studovat	studovat	k5eAaImF	studovat
v	v	k7c4	v
Techniku	technika	k1gFnSc4	technika
v	v	k7c6	v
Baku	Baku	k1gNnSc6	Baku
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
přešel	přejít	k5eAaPmAgInS	přejít
na	na	k7c4	na
Univerzitu	univerzita	k1gFnSc4	univerzita
v	v	k7c6	v
Baku	Baku	k1gNnSc6	Baku
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
studoval	studovat	k5eAaImAgMnS	studovat
dva	dva	k4xCgInPc4	dva
obory	obor	k1gInPc4	obor
současně	současně	k6eAd1	současně
-	-	kIx~	-
fyzikálně	fyzikálně	k6eAd1	fyzikálně
matematický	matematický	k2eAgInSc1d1	matematický
a	a	k8xC	a
chemický	chemický	k2eAgInSc1d1	chemický
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
přešel	přejít	k5eAaPmAgInS	přejít
na	na	k7c4	na
fyzikální	fyzikální	k2eAgNnSc4d1	fyzikální
oddělení	oddělení	k1gNnSc4	oddělení
Leningradské	leningradský	k2eAgFnSc2d1	Leningradská
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1927	[number]	k4	1927
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1926	[number]	k4	1926
<g/>
-	-	kIx~	-
<g/>
1927	[number]	k4	1927
publikoval	publikovat	k5eAaBmAgMnS	publikovat
své	své	k1gNnSc4	své
první	první	k4xOgFnSc2	první
práce	práce	k1gFnSc2	práce
z	z	k7c2	z
teoretické	teoretický	k2eAgFnSc2d1	teoretická
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
strávil	strávit	k5eAaPmAgMnS	strávit
půldruhého	půldruhý	k2eAgInSc2d1	půldruhý
roku	rok	k1gInSc2	rok
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
,	,	kIx,	,
v	v	k7c6	v
Dánsku	Dánsko	k1gNnSc6	Dánsko
<g/>
,	,	kIx,	,
Anglii	Anglie	k1gFnSc6	Anglie
a	a	k8xC	a
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pracoval	pracovat	k5eAaImAgInS	pracovat
s	s	k7c7	s
předními	přední	k2eAgMnPc7d1	přední
teoretickými	teoretický	k2eAgMnPc7d1	teoretický
fyziky	fyzik	k1gMnPc7	fyzik
<g/>
,	,	kIx,	,
především	především	k9	především
s	s	k7c7	s
Nielsem	Niels	k1gMnSc7	Niels
Bohrem	Bohr	k1gMnSc7	Bohr
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
pokládal	pokládat	k5eAaImAgMnS	pokládat
za	za	k7c4	za
svého	svůj	k3xOyFgMnSc4	svůj
jediného	jediný	k2eAgMnSc4d1	jediný
učitele	učitel	k1gMnSc4	učitel
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1932	[number]	k4	1932
působil	působit	k5eAaImAgMnS	působit
Landau	Landaa	k1gFnSc4	Landaa
v	v	k7c6	v
Charkově	Charkov	k1gInSc6	Charkov
<g/>
,	,	kIx,	,
na	na	k7c6	na
Ukrajinském	ukrajinský	k2eAgInSc6d1	ukrajinský
fyzikálně-technickém	fyzikálněechnický	k2eAgInSc6d1	fyzikálně-technický
institutu	institut	k1gInSc6	institut
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
stalo	stát	k5eAaPmAgNnS	stát
centrem	centr	k1gInSc7	centr
teoretické	teoretický	k2eAgFnSc2d1	teoretická
fyziky	fyzika	k1gFnSc2	fyzika
v	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
také	také	k9	také
začal	začít	k5eAaPmAgInS	začít
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Lifšicem	Lifšic	k1gMnSc7	Lifšic
i	i	k8xC	i
dalšími	další	k2eAgMnPc7d1	další
spolupracovníky	spolupracovník	k1gMnPc7	spolupracovník
psát	psát	k5eAaImF	psát
svůj	svůj	k3xOyFgInSc4	svůj
kurs	kurs	k1gInSc4	kurs
teoretické	teoretický	k2eAgFnSc2d1	teoretická
fyziky	fyzika	k1gFnSc2	fyzika
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
dále	daleko	k6eAd2	daleko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pobývalo	pobývat	k5eAaImAgNnS	pobývat
zde	zde	k6eAd1	zde
na	na	k7c6	na
stážích	stáž	k1gFnPc6	stáž
též	též	k9	též
několik	několik	k4yIc4	několik
mladých	mladý	k1gMnPc2	mladý
a	a	k8xC	a
významných	významný	k2eAgMnPc2d1	významný
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
fyziků	fyzik	k1gMnPc2	fyzik
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
maďarský	maďarský	k2eAgMnSc1d1	maďarský
fyzik	fyzik	k1gMnSc1	fyzik
Laszlo	Laszlo	k1gFnSc2	Laszlo
Tisza	Tisza	k1gFnSc1	Tisza
a	a	k8xC	a
československý	československý	k2eAgMnSc1d1	československý
fyzik	fyzik	k1gMnSc1	fyzik
Georg	Georg	k1gMnSc1	Georg
Placzek	Placzek	k1gInSc1	Placzek
<g/>
,	,	kIx,	,
spoluautor	spoluautor	k1gMnSc1	spoluautor
některých	některý	k3yIgFnPc2	některý
Landauových	Landauův	k2eAgFnPc2d1	Landauův
prací	práce	k1gFnPc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
pod	pod	k7c7	pod
tlakem	tlak	k1gInSc7	tlak
konfliktu	konflikt	k1gInSc2	konflikt
s	s	k7c7	s
rektorem	rektor	k1gMnSc7	rektor
<g/>
,	,	kIx,	,
politického	politický	k2eAgNnSc2d1	politické
ohrožení	ohrožení	k1gNnSc2	ohrožení
i	i	k8xC	i
profesních	profesní	k2eAgInPc2d1	profesní
střetů	střet	k1gInPc2	střet
požádal	požádat	k5eAaPmAgMnS	požádat
svého	svůj	k3xOyFgMnSc4	svůj
přítele	přítel	k1gMnSc4	přítel
Petra	Petr	k1gMnSc4	Petr
Kapicu	Kapica	k1gMnSc4	Kapica
o	o	k7c4	o
přijetí	přijetí	k1gNnSc4	přijetí
do	do	k7c2	do
jeho	on	k3xPp3gInSc2	on
nově	nově	k6eAd1	nově
vznikajícího	vznikající	k2eAgInSc2d1	vznikající
Institutu	institut	k1gInSc2	institut
fyzikálních	fyzikální	k2eAgInPc2d1	fyzikální
problémů	problém	k1gInPc2	problém
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
vedoucím	vedoucí	k1gMnSc7	vedoucí
teoretického	teoretický	k2eAgNnSc2d1	teoretické
oddělení	oddělení	k1gNnSc2	oddělení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
se	se	k3xPyFc4	se
Landau	Landaa	k1gFnSc4	Landaa
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Korou	Korý	k2eAgFnSc7d1	Korý
Drobancevovou	Drobancevová	k1gFnSc7	Drobancevová
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
narodil	narodit	k5eAaPmAgMnS	narodit
syn	syn	k1gMnSc1	syn
Igor	Igor	k1gMnSc1	Igor
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
experimentálním	experimentální	k2eAgMnSc7d1	experimentální
fyzikem	fyzik	k1gMnSc7	fyzik
<g/>
.	.	kIx.	.
</s>
<s>
Naivně	naivně	k6eAd1	naivně
"	"	kIx"	"
<g/>
antiburžoazní	antiburžoazní	k2eAgInPc1d1	antiburžoazní
<g/>
"	"	kIx"	"
názory	názor	k1gInPc1	názor
Landaua	Landauum	k1gNnSc2	Landauum
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
raném	raný	k2eAgNnSc6d1	rané
období	období	k1gNnSc6	období
se	se	k3xPyFc4	se
radikálně	radikálně	k6eAd1	radikálně
změnily	změnit	k5eAaPmAgInP	změnit
v	v	k7c6	v
období	období	k1gNnSc6	období
nastupujícího	nastupující	k2eAgInSc2d1	nastupující
stalinského	stalinský	k2eAgInSc2d1	stalinský
"	"	kIx"	"
<g/>
velkého	velký	k2eAgInSc2d1	velký
teroru	teror	k1gInSc2	teror
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
přítelem	přítel	k1gMnSc7	přítel
Korecem	Korece	k1gMnSc7	Korece
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c4	na
sepsání	sepsání	k1gNnSc4	sepsání
protistalinského	protistalinský	k2eAgInSc2d1	protistalinský
letáku	leták	k1gInSc2	leták
<g/>
,	,	kIx,	,
varujícího	varující	k2eAgNnSc2d1	varující
před	před	k7c7	před
stalinským	stalinský	k2eAgInSc7d1	stalinský
fašismem	fašismus	k1gInSc7	fašismus
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1938	[number]	k4	1938
byl	být	k5eAaImAgMnS	být
zatčen	zatknout	k5eAaPmNgMnS	zatknout
a	a	k8xC	a
strávil	strávit	k5eAaPmAgMnS	strávit
rok	rok	k1gInSc4	rok
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
<g/>
.	.	kIx.	.
</s>
<s>
Velice	velice	k6eAd1	velice
statečně	statečně	k6eAd1	statečně
se	se	k3xPyFc4	se
za	za	k7c4	za
něj	on	k3xPp3gMnSc4	on
postavil	postavit	k5eAaPmAgMnS	postavit
Petr	Petr	k1gMnSc1	Petr
Kapica	Kapica	k1gMnSc1	Kapica
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
hned	hned	k6eAd1	hned
v	v	k7c4	v
den	den	k1gInSc4	den
zatčení	zatčení	k1gNnSc2	zatčení
napsal	napsat	k5eAaBmAgMnS	napsat
Stalinovi	Stalin	k1gMnSc3	Stalin
odvážný	odvážný	k2eAgInSc4d1	odvážný
dopis	dopis	k1gInSc4	dopis
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
intervencí	intervence	k1gFnSc7	intervence
řady	řada	k1gFnSc2	řada
vlivných	vlivný	k2eAgMnPc2d1	vlivný
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
vědců	vědec	k1gMnPc2	vědec
(	(	kIx(	(
<g/>
zejm.	zejm.	k?	zejm.
Nielse	Niels	k1gMnSc2	Niels
Bohra	Bohr	k1gMnSc2	Bohr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
alespoň	alespoň	k9	alespoň
mírných	mírný	k2eAgFnPc2d1	mírná
podmínek	podmínka	k1gFnPc2	podmínka
Landauova	Landauův	k2eAgNnSc2d1	Landauův
věznění	věznění	k1gNnSc2	věznění
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
napsal	napsat	k5eAaBmAgMnS	napsat
Kapica	Kapica	k1gMnSc1	Kapica
další	další	k2eAgInSc4d1	další
dopis	dopis	k1gInSc4	dopis
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
Molotovovi	Molotovův	k2eAgMnPc1d1	Molotovův
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
odpovědnost	odpovědnost	k1gFnSc4	odpovědnost
byl	být	k5eAaImAgInS	být
Landau	Landaa	k1gFnSc4	Landaa
propuštěn	propuštěn	k2eAgInSc1d1	propuštěn
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
jiným	jiný	k2eAgMnSc7d1	jiný
člověkem	člověk	k1gMnSc7	člověk
<g/>
,	,	kIx,	,
tichým	tichý	k2eAgMnSc7d1	tichý
a	a	k8xC	a
opatrným	opatrný	k2eAgNnSc7d1	opatrné
<g/>
.	.	kIx.	.
</s>
<s>
Pracovníkem	pracovník	k1gMnSc7	pracovník
Institutu	institut	k1gInSc2	institut
fyzikálních	fyzikální	k2eAgInPc2d1	fyzikální
problémů	problém	k1gInPc2	problém
zůstal	zůstat	k5eAaPmAgInS	zůstat
po	po	k7c4	po
zbytek	zbytek	k1gInSc4	zbytek
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
jeho	jeho	k3xOp3gMnPc7	jeho
žáky	žák	k1gMnPc7	žák
patřil	patřit	k5eAaImAgMnS	patřit
i	i	k9	i
významný	významný	k2eAgMnSc1d1	významný
československý	československý	k2eAgMnSc1d1	československý
fyzik	fyzik	k1gMnSc1	fyzik
Jozef	Jozef	k1gMnSc1	Jozef
Kvasnica	Kvasnica	k1gMnSc1	Kvasnica
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
měl	mít	k5eAaImAgInS	mít
Landau	Landa	k2eAgFnSc4d1	Landa
těžkou	těžký	k2eAgFnSc4d1	těžká
automobilovou	automobilový	k2eAgFnSc4d1	automobilová
nehodu	nehoda	k1gFnSc4	nehoda
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yQgFnSc2	který
se	se	k3xPyFc4	se
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
předčasné	předčasný	k2eAgFnSc2d1	předčasná
smrti	smrt	k1gFnSc2	smrt
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
nikdy	nikdy	k6eAd1	nikdy
zcela	zcela	k6eAd1	zcela
nevzpamatoval	vzpamatovat	k5eNaPmAgMnS	vzpamatovat
<g/>
.	.	kIx.	.
</s>
<s>
Nehodu	nehoda	k1gFnSc4	nehoda
přežil	přežít	k5eAaPmAgMnS	přežít
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k9	i
díky	díky	k7c3	díky
solidaritě	solidarita	k1gFnSc3	solidarita
vědců	vědec	k1gMnPc2	vědec
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
zorganizovali	zorganizovat	k5eAaPmAgMnP	zorganizovat
pomoc	pomoc	k1gFnSc4	pomoc
při	při	k7c6	při
jeho	jeho	k3xOp3gNnSc6	jeho
léčení	léčení	k1gNnSc6	léčení
<g/>
.	.	kIx.	.
</s>
<s>
Landau	Landau	k6eAd1	Landau
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
slavnou	slavný	k2eAgFnSc4d1	slavná
školu	škola	k1gFnSc4	škola
teoretické	teoretický	k2eAgFnSc2d1	teoretická
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
patřili	patřit	k5eAaImAgMnP	patřit
např.	např.	kA	např.
Pomerančuk	Pomerančuk	k1gInSc1	Pomerančuk
<g/>
,	,	kIx,	,
I.	I.	kA	I.
M.	M.	kA	M.
Lifšic	Lifšic	k1gMnSc1	Lifšic
<g/>
,	,	kIx,	,
J.	J.	kA	J.
M.	M.	kA	M.
Lifšic	Lifšic	k1gMnSc1	Lifšic
<g/>
,	,	kIx,	,
A.	A.	kA	A.
A.	A.	kA	A.
Abrikosov	Abrikosov	k1gInSc1	Abrikosov
<g/>
,	,	kIx,	,
A.	A.	kA	A.
B.	B.	kA	B.
Migdal	Migdal	k1gMnSc1	Migdal
<g/>
,	,	kIx,	,
L.	L.	kA	L.
L.	L.	kA	L.
Pitajevskij	Pitajevskij	k1gMnSc1	Pitajevskij
<g/>
,	,	kIx,	,
I.	I.	kA	I.
M.	M.	kA	M.
Chalatnikov	Chalatnikov	k1gInSc1	Chalatnikov
<g/>
.	.	kIx.	.
</s>
<s>
Vědecký	vědecký	k2eAgInSc1d1	vědecký
seminář	seminář	k1gInSc1	seminář
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
Landau	Landaa	k1gFnSc4	Landaa
řídil	řídit	k5eAaImAgInS	řídit
<g/>
,	,	kIx,	,
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
do	do	k7c2	do
dějin	dějiny	k1gFnPc2	dějiny
teoretické	teoretický	k2eAgFnSc2d1	teoretická
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Jevgenijem	Jevgenij	k1gMnSc7	Jevgenij
Lifšicem	Lifšic	k1gMnSc7	Lifšic
je	být	k5eAaImIp3nS	být
Landau	Landaa	k1gMnSc4	Landaa
uveden	uveden	k2eAgInSc1d1	uveden
jako	jako	k8xS	jako
hlavní	hlavní	k2eAgMnSc1d1	hlavní
autor	autor	k1gMnSc1	autor
u	u	k7c2	u
většiny	většina	k1gFnSc2	většina
dílů	díl	k1gInPc2	díl
Kurzu	kurz	k1gInSc2	kurz
teoretické	teoretický	k2eAgFnSc2d1	teoretická
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
přeložen	přeložit	k5eAaPmNgInS	přeložit
do	do	k7c2	do
mnoha	mnoho	k4c2	mnoho
jazyků	jazyk	k1gInPc2	jazyk
a	a	k8xC	a
doposud	doposud	k6eAd1	doposud
je	být	k5eAaImIp3nS	být
využíván	využíván	k2eAgInSc1d1	využíván
studenty	student	k1gMnPc7	student
a	a	k8xC	a
fyziky	fyzik	k1gMnPc7	fyzik
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Díl	díl	k1gInSc1	díl
1	[number]	k4	1
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Mechanika	mechanika	k1gFnSc1	mechanika
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
L.	L.	kA	L.
D.	D.	kA	D.
Landau	Landaa	k1gFnSc4	Landaa
<g/>
,	,	kIx,	,
J.	J.	kA	J.
M.	M.	kA	M.
Lifšic	Lifšic	k1gMnSc1	Lifšic
Díl	díl	k1gInSc4	díl
2	[number]	k4	2
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Klasická	klasický	k2eAgFnSc1d1	klasická
teorie	teorie	k1gFnSc1	teorie
pole	pole	k1gNnSc2	pole
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
L.	L.	kA	L.
D.	D.	kA	D.
Landau	Landaa	k1gFnSc4	Landaa
<g/>
,	,	kIx,	,
J.	J.	kA	J.
M.	M.	kA	M.
Lifšic	Lifšic	k1gMnSc1	Lifšic
Díl	díl	k1gInSc4	díl
3	[number]	k4	3
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Kvantová	kvantový	k2eAgFnSc1d1	kvantová
mechanika	mechanika	k1gFnSc1	mechanika
<g/>
:	:	kIx,	:
Nerelativistická	relativistický	k2eNgFnSc1d1	nerelativistická
teorie	teorie	k1gFnSc1	teorie
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
L.	L.	kA	L.
D.	D.	kA	D.
Landau	Landaa	k1gFnSc4	Landaa
<g/>
,	,	kIx,	,
J.	J.	kA	J.
M.	M.	kA	M.
Lifšic	Lifšic	k1gMnSc1	Lifšic
Díl	díl	k1gInSc4	díl
4	[number]	k4	4
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Kvantová	kvantový	k2eAgFnSc1d1	kvantová
elektrodynamika	elektrodynamika	k1gFnSc1	elektrodynamika
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V.	V.	kA	V.
B.	B.	kA	B.
Beresteckij	Beresteckij	k1gMnSc1	Beresteckij
<g/>
,	,	kIx,	,
J.	J.	kA	J.
M.	M.	kA	M.
Lifšic	Lifšic	k1gMnSc1	Lifšic
a	a	k8xC	a
L.	L.	kA	L.
P.	P.	kA	P.
Pitajevskij	Pitajevskij	k1gFnSc1	Pitajevskij
Díl	díl	k1gInSc1	díl
5	[number]	k4	5
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Statistická	statistický	k2eAgFnSc1d1	statistická
fyzika	fyzika	k1gFnSc1	fyzika
1	[number]	k4	1
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
L.	L.	kA	L.
D.	D.	kA	D.
Landau	Landaa	k1gFnSc4	Landaa
<g/>
,	,	kIx,	,
J.	J.	kA	J.
M.	M.	kA	M.
Lifšic	Lifšic	k1gMnSc1	Lifšic
Díl	díl	k1gInSc4	díl
6	[number]	k4	6
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Mechanika	mechanika	k1gFnSc1	mechanika
kontinua	kontinuum	k1gNnSc2	kontinuum
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
L.	L.	kA	L.
D.	D.	kA	D.
Landau	Landaa	k1gFnSc4	Landaa
<g/>
,	,	kIx,	,
J.	J.	kA	J.
M.	M.	kA	M.
Lifšic	Lifšic	k1gMnSc1	Lifšic
Díl	díl	k1gInSc4	díl
7	[number]	k4	7
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Teorie	teorie	k1gFnSc1	teorie
pružnosti	pružnost	k1gFnSc2	pružnost
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
L.	L.	kA	L.
D.	D.	kA	D.
Landau	Landaa	k1gFnSc4	Landaa
<g/>
,	,	kIx,	,
J.	J.	kA	J.
M.	M.	kA	M.
Lifšic	Lifšic	k1gMnSc1	Lifšic
Díl	díl	k1gInSc4	díl
8	[number]	k4	8
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Elektrodynamika	elektrodynamika	k1gFnSc1	elektrodynamika
kontinua	kontinuum	k1gNnSc2	kontinuum
<g/>
"	"	kIx"	"
L.	L.	kA	L.
D.	D.	kA	D.
Landau	Landaa	k1gFnSc4	Landaa
<g/>
,	,	kIx,	,
J.	J.	kA	J.
M.	M.	kA	M.
Lifšic	Lifšic	k1gMnSc1	Lifšic
a	a	k8xC	a
L.	L.	kA	L.
P.	P.	kA	P.
Pitajevskij	Pitajevskij	k1gFnSc1	Pitajevskij
Díl	díl	k1gInSc1	díl
9	[number]	k4	9
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Statistická	statistický	k2eAgFnSc1d1	statistická
fyzika	fyzika	k1gFnSc1	fyzika
2	[number]	k4	2
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
J.	J.	kA	J.
M.	M.	kA	M.
Lifšic	Lifšic	k1gMnSc1	Lifšic
<g/>
,	,	kIx,	,
L.	L.	kA	L.
P.	P.	kA	P.
Pitajevskij	Pitajevskij	k1gFnSc1	Pitajevskij
Díl	díl	k1gInSc1	díl
10	[number]	k4	10
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Fyzikální	fyzikální	k2eAgFnSc1d1	fyzikální
kinetika	kinetika	k1gFnSc1	kinetika
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
J.	J.	kA	J.
M.	M.	kA	M.
Lifšic	Lifšic	k1gMnSc1	Lifšic
<g/>
,	,	kIx,	,
L.	L.	kA	L.
P.	P.	kA	P.
Pitajevskij	Pitajevskij	k1gMnSc1	Pitajevskij
Ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
Kurzu	kurz	k1gInSc2	kurz
<g/>
:	:	kIx,	:
O	o	k7c6	o
Landauovi	Landau	k1gMnSc6	Landau
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
četných	četný	k2eAgNnPc2d1	četné
svědectví	svědectví	k1gNnPc2	svědectví
jeho	jeho	k3xOp3gMnPc2	jeho
spolupracovníků	spolupracovník	k1gMnPc2	spolupracovník
dobře	dobře	k6eAd1	dobře
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
nebyl	být	k5eNaImAgInS	být
prakticky	prakticky	k6eAd1	prakticky
schopen	schopen	k2eAgInSc1d1	schopen
písemně	písemně	k6eAd1	písemně
stylizovat	stylizovat	k5eAaImF	stylizovat
své	svůj	k3xOyFgFnPc4	svůj
fyzikální	fyzikální	k2eAgFnPc4d1	fyzikální
práce	práce	k1gFnPc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
ty	ten	k3xDgFnPc1	ten
časopisecké	časopisecký	k2eAgFnPc1d1	časopisecká
práce	práce	k1gFnPc1	práce
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgMnPc2	jenž
je	být	k5eAaImIp3nS	být
napsán	napsán	k2eAgMnSc1d1	napsán
jako	jako	k8xS	jako
jediný	jediný	k2eAgMnSc1d1	jediný
autor	autor	k1gMnSc1	autor
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
Landaua	Landauus	k1gMnSc4	Landauus
většinou	většinou	k6eAd1	většinou
"	"	kIx"	"
<g/>
pod	pod	k7c7	pod
jeho	jeho	k3xOp3gNnSc7	jeho
vedením	vedení	k1gNnSc7	vedení
<g/>
"	"	kIx"	"
sepsal	sepsat	k5eAaPmAgMnS	sepsat
J.	J.	kA	J.
M.	M.	kA	M.
Lifšic	Lifšic	k1gMnSc1	Lifšic
<g/>
.	.	kIx.	.
</s>
<s>
Vždy	vždy	k6eAd1	vždy
proto	proto	k8xC	proto
existovala	existovat	k5eAaImAgFnS	existovat
otázka	otázka	k1gFnSc1	otázka
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
vlastně	vlastně	k9	vlastně
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
tento	tento	k3xDgInSc1	tento
monumentální	monumentální	k2eAgInSc1d1	monumentální
Kurz	kurz	k1gInSc1	kurz
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
fyziky	fyzik	k1gMnPc7	fyzik
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
"	"	kIx"	"
<g/>
Lifšic	Lifšic	k1gMnSc1	Lifšic
nevymyslel	vymyslet	k5eNaPmAgMnS	vymyslet
a	a	k8xC	a
Landau	Landaa	k1gFnSc4	Landaa
nenapsal	napsat	k5eNaPmAgMnS	napsat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zájmu	zájem	k1gInSc6	zájem
historické	historický	k2eAgFnSc2d1	historická
objektivity	objektivita	k1gFnSc2	objektivita
je	být	k5eAaImIp3nS	být
zapotřebí	zapotřebí	k6eAd1	zapotřebí
doplnit	doplnit	k5eAaPmF	doplnit
málo	málo	k6eAd1	málo
známou	známý	k2eAgFnSc4d1	známá
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
Kurzu	kurz	k1gInSc2	kurz
významně	významně	k6eAd1	významně
přispělo	přispět	k5eAaPmAgNnS	přispět
i	i	k9	i
několik	několik	k4yIc1	několik
jiných	jiný	k2eAgFnPc2d1	jiná
(	(	kIx(	(
<g/>
a	a	k8xC	a
mezi	mezi	k7c7	mezi
spoluautory	spoluautor	k1gMnPc7	spoluautor
neuvedených	uvedený	k2eNgMnPc2d1	neuvedený
<g/>
)	)	kIx)	)
fyziků	fyzik	k1gMnPc2	fyzik
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
Landau	Landaa	k1gFnSc4	Landaa
s	s	k7c7	s
Lifšicem	Lifšic	k1gMnSc7	Lifšic
nejsou	být	k5eNaImIp3nP	být
ani	ani	k8xC	ani
zdaleka	zdaleka	k6eAd1	zdaleka
jeho	jeho	k3xOp3gFnPc7	jeho
jedinymi	jediny	k1gFnPc7	jediny
hlavními	hlavní	k2eAgMnPc7d1	hlavní
autory	autor	k1gMnPc7	autor
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
prvá	prvý	k4xOgFnSc1	prvý
verze	verze	k1gFnSc1	verze
Statistické	statistický	k2eAgFnSc2d1	statistická
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
dokonce	dokonce	k9	dokonce
vyšla	vyjít	k5eAaPmAgFnS	vyjít
historicky	historicky	k6eAd1	historicky
jako	jako	k8xS	jako
vůbec	vůbec	k9	vůbec
první	první	k4xOgInSc1	první
svazek	svazek	k1gInSc1	svazek
Kurzu	kurz	k1gInSc2	kurz
v	v	k7c6	v
r.	r.	kA	r.
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
též	též	k9	též
dílem	díl	k1gInSc7	díl
geniálního	geniální	k2eAgMnSc4d1	geniální
teoretického	teoretický	k2eAgMnSc4d1	teoretický
fyzika	fyzik	k1gMnSc4	fyzik
Matvěje	Matvěje	k1gFnSc2	Matvěje
Petroviče	Petrovič	k1gMnSc4	Petrovič
Bronštejna	Bronštejn	k1gMnSc4	Bronštejn
<g/>
.	.	kIx.	.
</s>
<s>
Bronštejn	Bronštejn	k1gInSc1	Bronštejn
byl	být	k5eAaImAgInS	být
krátce	krátce	k6eAd1	krátce
předtím	předtím	k6eAd1	předtím
zatčen	zatknout	k5eAaPmNgMnS	zatknout
stalinskou	stalinský	k2eAgFnSc7d1	stalinská
NKVD	NKVD	kA	NKVD
a	a	k8xC	a
po	po	k7c6	po
půlroce	půlrok	k1gInSc6	půlrok
coby	coby	k?	coby
oběť	oběť	k1gFnSc4	oběť
"	"	kIx"	"
<g/>
velkého	velký	k2eAgInSc2d1	velký
teroru	teror	k1gInSc2	teror
<g/>
"	"	kIx"	"
popraven	popraven	k2eAgMnSc1d1	popraven
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
jakákoliv	jakýkoliv	k3yIgFnSc1	jakýkoliv
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
jeho	jeho	k3xOp3gNnSc6	jeho
autorství	autorství	k1gNnSc6	autorství
nemohla	moct	k5eNaImAgFnS	moct
být	být	k5eAaImF	být
tehdy	tehdy	k6eAd1	tehdy
uvedena	uvést	k5eAaPmNgFnS	uvést
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
Kurz	kurz	k1gInSc1	kurz
dále	daleko	k6eAd2	daleko
upravován	upravovat	k5eAaImNgInS	upravovat
a	a	k8xC	a
rozšiřován	rozšiřován	k2eAgInSc1d1	rozšiřován
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
tato	tento	k3xDgFnSc1	tento
okolnost	okolnost	k1gFnSc1	okolnost
nebyla	být	k5eNaImAgFnS	být
nikdy	nikdy	k6eAd1	nikdy
napravena	napraven	k2eAgFnSc1d1	napravena
<g/>
.	.	kIx.	.
</s>
<s>
Otázky	otázka	k1gFnPc1	otázka
autorství	autorství	k1gNnSc2	autorství
a	a	k8xC	a
citace	citace	k1gFnSc2	citace
primárních	primární	k2eAgInPc2d1	primární
zdrojů	zdroj	k1gInPc2	zdroj
nejsou	být	k5eNaImIp3nP	být
právě	právě	k9	právě
silnou	silný	k2eAgFnSc7d1	silná
stránkou	stránka	k1gFnSc7	stránka
tohoto	tento	k3xDgInSc2	tento
jinak	jinak	k6eAd1	jinak
výjimečného	výjimečný	k2eAgInSc2d1	výjimečný
a	a	k8xC	a
stále	stále	k6eAd1	stále
ceněného	ceněný	k2eAgNnSc2d1	ceněné
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Helium	helium	k1gNnSc1	helium
Supravodivost	supravodivost	k1gFnSc1	supravodivost
Supratekutost	supratekutost	k1gFnSc1	supratekutost
Pjotr	Pjotr	k1gInSc1	Pjotr
Leonidovič	Leonidovič	k1gMnSc1	Leonidovič
Kapica	Kapica	k1gMnSc1	Kapica
Elefter	Elefter	k1gMnSc1	Elefter
Luarsabovič	Luarsabovič	k1gMnSc1	Luarsabovič
Andronikašvili	Andronikašvili	k1gMnSc1	Andronikašvili
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Lev	Lev	k1gMnSc1	Lev
Davidovič	Davidovič	k1gInSc4	Davidovič
Landau	Landaus	k1gInSc2	Landaus
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Lev	Lev	k1gMnSc1	Lev
Davidovič	Davidovič	k1gMnSc1	Davidovič
Landau	Landaa	k1gFnSc4	Landaa
-	-	kIx~	-
anglicky	anglicky	k6eAd1	anglicky
Lev	lev	k1gInSc1	lev
Landau	Landaus	k1gInSc2	Landaus
-	-	kIx~	-
rusky	rusky	k6eAd1	rusky
Landau	Landaa	k1gFnSc4	Landaa
<g/>
,	,	kIx,	,
Kapica	Kapica	k1gMnSc1	Kapica
a	a	k8xC	a
Stalin	Stalin	k1gMnSc1	Stalin
</s>
