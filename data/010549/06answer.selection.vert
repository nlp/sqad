<s>
Saturn	Saturn	k1gInSc1	Saturn
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
velké	velký	k2eAgMnPc4d1	velký
plynné	plynný	k2eAgMnPc4d1	plynný
obry	obr	k1gMnPc4	obr
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yQgFnPc4	který
je	být	k5eAaImIp3nS	být
typické	typický	k2eAgNnSc1d1	typické
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemají	mít	k5eNaImIp3nP	mít
pevný	pevný	k2eAgInSc4d1	pevný
povrch	povrch	k1gInSc4	povrch
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
hustou	hustý	k2eAgFnSc4d1	hustá
atmosféru	atmosféra	k1gFnSc4	atmosféra
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
postupně	postupně	k6eAd1	postupně
přechází	přecházet	k5eAaImIp3nS	přecházet
do	do	k7c2	do
pláště	plášť	k1gInSc2	plášť
<g/>
.	.	kIx.	.
</s>
