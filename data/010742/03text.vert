<p>
<s>
Michael	Michael	k1gMnSc1	Michael
Foret	Foret	k1gMnSc1	Foret
(	(	kIx(	(
<g/>
*	*	kIx~	*
20	[number]	k4	20
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1986	[number]	k4	1986
Prostějov	Prostějov	k1gInSc1	Prostějov
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
textař	textař	k1gMnSc1	textař
a	a	k8xC	a
herec	herec	k1gMnSc1	herec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
se	se	k3xPyFc4	se
v	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
ročníku	ročník	k1gInSc2	ročník
hudební	hudební	k2eAgFnSc2d1	hudební
soutěže	soutěž	k1gFnSc2	soutěž
Česko	Česko	k1gNnSc1	Česko
hledá	hledat	k5eAaImIp3nS	hledat
SuperStar	superstar	k1gFnSc4	superstar
probojoval	probojovat	k5eAaPmAgMnS	probojovat
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
na	na	k7c4	na
6	[number]	k4	6
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
toho	ten	k3xDgNnSc2	ten
vydal	vydat	k5eAaPmAgInS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
s	s	k7c7	s
Ondřejem	Ondřej	k1gMnSc7	Ondřej
Soukupem	Soukup	k1gMnSc7	Soukup
produkované	produkovaný	k2eAgNnSc4d1	produkované
debutové	debutový	k2eAgNnSc4d1	debutové
album	album	k1gNnSc4	album
Forte	forte	k1gNnSc2	forte
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Objevil	objevit	k5eAaPmAgInS	objevit
se	se	k3xPyFc4	se
v	v	k7c6	v
hlavních	hlavní	k2eAgFnPc6d1	hlavní
rolích	role	k1gFnPc6	role
předních	přední	k2eAgInPc2d1	přední
českých	český	k2eAgInPc2d1	český
muzikálů	muzikál	k1gInPc2	muzikál
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
nejúspěšnějším	úspěšný	k2eAgInSc6d3	nejúspěšnější
hit-muzikálu	hituzikál	k1gInSc6	hit-muzikál
Děti	dítě	k1gFnPc1	dítě
ráje	ráje	k1gFnPc1	ráje
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
-	-	kIx~	-
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
v	v	k7c6	v
muzikálu	muzikál	k1gInSc6	muzikál
Les	les	k1gInSc4	les
Miserables-	Miserables-	k1gFnPc2	Miserables-
Bídníci	bídník	k1gMnPc1	bídník
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
-	-	kIx~	-
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
2014	[number]	k4	2014
bude	být	k5eAaImBp3nS	být
účinkovat	účinkovat	k5eAaImF	účinkovat
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
"	"	kIx"	"
<g/>
Sky	Sky	k1gFnPc6	Sky
<g/>
"	"	kIx"	"
v	v	k7c6	v
nové	nový	k2eAgFnSc6d1	nová
české	český	k2eAgFnSc6d1	Česká
inscenaci	inscenace	k1gFnSc6	inscenace
světového	světový	k2eAgInSc2d1	světový
muzikálu	muzikál	k1gInSc2	muzikál
Mamma	Mamma	k1gFnSc1	Mamma
Mia	Mia	k1gFnSc1	Mia
<g/>
!	!	kIx.	!
</s>
</p>
<p>
<s>
Má	mít	k5eAaImIp3nS	mít
vydaná	vydaný	k2eAgFnSc1d1	vydaná
dvě	dva	k4xCgNnPc1	dva
sólová	sólový	k2eAgNnPc1d1	sólové
alba	album	k1gNnPc1	album
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
<g/>
[	[	kIx(	[
<g/>
kdy	kdy	k6eAd1	kdy
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
pracuje	pracovat	k5eAaImIp3nS	pracovat
na	na	k7c6	na
třetím	třetí	k4xOgInSc6	třetí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
Vysokou	vysoký	k2eAgFnSc4d1	vysoká
školu	škola	k1gFnSc4	škola
ekonomie	ekonomie	k1gFnSc2	ekonomie
a	a	k8xC	a
managementu	management	k1gInSc2	management
obor	obora	k1gFnPc2	obora
Komunikace	komunikace	k1gFnSc1	komunikace
a	a	k8xC	a
lidské	lidský	k2eAgInPc1d1	lidský
zdroje	zdroj	k1gInPc1	zdroj
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
3	[number]	k4	3
roky	rok	k1gInPc4	rok
Konzervatoř	konzervatoř	k1gFnSc1	konzervatoř
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Ježka	Ježek	k1gMnSc2	Ježek
a	a	k8xC	a
Anglo-německou	Angloěmecká	k1gFnSc4	Anglo-německá
střední	střední	k2eAgFnSc4d1	střední
obchodní	obchodní	k2eAgFnSc4d1	obchodní
akademii	akademie	k1gFnSc4	akademie
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Životopis	životopis	k1gInSc4	životopis
==	==	k?	==
</s>
</p>
<p>
<s>
Michael	Michael	k1gMnSc1	Michael
Foret	Foret	k1gMnSc1	Foret
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
20	[number]	k4	20
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1986	[number]	k4	1986
v	v	k7c6	v
Prostějově	Prostějov	k1gInSc6	Prostějov
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
15	[number]	k4	15
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
aktivně	aktivně	k6eAd1	aktivně
věnoval	věnovat	k5eAaImAgMnS	věnovat
sportu	sport	k1gInSc2	sport
na	na	k7c6	na
základní	základní	k2eAgFnSc6d1	základní
škole	škola	k1gFnSc6	škola
závodně	závodně	k6eAd1	závodně
provozoval	provozovat	k5eAaImAgMnS	provozovat
lehkou	lehký	k2eAgFnSc4d1	lehká
atletiku	atletika	k1gFnSc4	atletika
<g/>
,	,	kIx,	,
hrál	hrát	k5eAaImAgMnS	hrát
tenis	tenis	k1gInSc4	tenis
a	a	k8xC	a
volejbal	volejbal	k1gInSc4	volejbal
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
13	[number]	k4	13
letech	léto	k1gNnPc6	léto
navštívil	navštívit	k5eAaPmAgMnS	navštívit
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
koncert	koncert	k1gInSc4	koncert
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
-	-	kIx~	-
The	The	k1gFnSc1	The
Cranberries	Cranberriesa	k1gFnPc2	Cranberriesa
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
hraje	hrát	k5eAaImIp3nS	hrát
na	na	k7c4	na
kytaru	kytara	k1gFnSc4	kytara
<g/>
,	,	kIx,	,
píše	psát	k5eAaImIp3nS	psát
vlastní	vlastní	k2eAgFnPc4d1	vlastní
písně	píseň	k1gFnPc4	píseň
a	a	k8xC	a
zpívá	zpívat	k5eAaImIp3nS	zpívat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
střední	střední	k2eAgFnSc6d1	střední
škole	škola	k1gFnSc6	škola
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
na	na	k7c6	na
různých	různý	k2eAgFnPc6d1	různá
kulturních	kulturní	k2eAgFnPc6d1	kulturní
akcích	akce	k1gFnPc6	akce
a	a	k8xC	a
soutěžích	soutěž	k1gFnPc6	soutěž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
18	[number]	k4	18
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
druhého	druhý	k4xOgInSc2	druhý
ročníku	ročník	k1gInSc2	ročník
televizní	televizní	k2eAgFnSc2d1	televizní
hudební	hudební	k2eAgFnSc2d1	hudební
soutěže	soutěž	k1gFnSc2	soutěž
Česko	Česko	k1gNnSc1	Česko
hledá	hledat	k5eAaImIp3nS	hledat
SuperStar	superstar	k1gFnSc4	superstar
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
z	z	k7c2	z
téměř	téměř	k6eAd1	téměř
9	[number]	k4	9
000	[number]	k4	000
přihlášených	přihlášený	k2eAgMnPc2d1	přihlášený
probojoval	probojovat	k5eAaPmAgMnS	probojovat
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
na	na	k7c4	na
6	[number]	k4	6
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Získal	získat	k5eAaPmAgInS	získat
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
hudebním	hudební	k2eAgNnSc7d1	hudební
vydavatelstvím	vydavatelství	k1gNnSc7	vydavatelství
Sony	Sony	kA	Sony
BMG	BMG	kA	BMG
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2006	[number]	k4	2006
vydává	vydávat	k5eAaPmIp3nS	vydávat
Ondřejem	Ondřej	k1gMnSc7	Ondřej
Soukupem	Soukup	k1gMnSc7	Soukup
produkovanou	produkovaný	k2eAgFnSc4d1	produkovaná
debutovou	debutový	k2eAgFnSc4d1	debutová
desku	deska	k1gFnSc4	deska
Forte	forte	k1gNnSc2	forte
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
mediální	mediální	k2eAgFnSc6d1	mediální
bublině	bublina	k1gFnSc6	bublina
kolem	kolem	k7c2	kolem
SuperStar	superstar	k1gFnSc2	superstar
se	se	k3xPyFc4	se
Michael	Michael	k1gMnSc1	Michael
odebral	odebrat	k5eAaPmAgMnS	odebrat
do	do	k7c2	do
ústraní	ústraní	k1gNnSc2	ústraní
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgInS	začít
studovat	studovat	k5eAaImF	studovat
Konzervatoř	konzervatoř	k1gFnSc4	konzervatoř
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Ježka	Ježek	k1gMnSc2	Ježek
a	a	k8xC	a
založil	založit	k5eAaPmAgMnS	založit
kapelu	kapela	k1gFnSc4	kapela
ForetGump	ForetGump	k1gMnSc1	ForetGump
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dvou	dva	k4xCgNnPc6	dva
letech	léto	k1gNnPc6	léto
koncertování	koncertování	k1gNnSc2	koncertování
se	se	k3xPyFc4	se
kapela	kapela	k1gFnSc1	kapela
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
a	a	k8xC	a
Michael	Michael	k1gMnSc1	Michael
posléze	posléze	k6eAd1	posléze
začal	začít	k5eAaPmAgMnS	začít
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
druhé	druhý	k4xOgFnSc6	druhý
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
zcela	zcela	k6eAd1	zcela
autorské	autorský	k2eAgNnSc4d1	autorské
<g/>
,	,	kIx,	,
desce	deska	k1gFnSc3	deska
Uvnitř	uvnitř	k6eAd1	uvnitř
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
album	album	k1gNnSc1	album
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2009	[number]	k4	2009
a	a	k8xC	a
dočkalo	dočkat	k5eAaPmAgNnS	dočkat
se	se	k3xPyFc4	se
přívětivého	přívětivý	k2eAgNnSc2d1	přívětivé
uznání	uznání	k1gNnSc2	uznání
kritiků	kritik	k1gMnPc2	kritik
a	a	k8xC	a
fanoušků	fanoušek	k1gMnPc2	fanoušek
i	i	k9	i
méně	málo	k6eAd2	málo
středoproudého	středoproudý	k2eAgNnSc2d1	středoproudý
vnímání	vnímání	k1gNnSc2	vnímání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Michael	Michael	k1gMnSc1	Michael
Foret	Foret	k1gMnSc1	Foret
s	s	k7c7	s
úspěchem	úspěch	k1gInSc7	úspěch
účinkuje	účinkovat	k5eAaImIp3nS	účinkovat
i	i	k9	i
v	v	k7c6	v
muzikálech	muzikál	k1gInPc6	muzikál
<g/>
.	.	kIx.	.
</s>
<s>
Představoval	představovat	k5eAaImAgMnS	představovat
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
rolí	role	k1gFnPc2	role
"	"	kIx"	"
<g/>
Mickeyeho	Mickeye	k1gMnSc2	Mickeye
<g/>
"	"	kIx"	"
v	v	k7c6	v
nejúspěšnějším	úspěšný	k2eAgInSc6d3	nejúspěšnější
českém	český	k2eAgInSc6d1	český
hit-muzikálu	hituzikál	k1gInSc6	hit-muzikál
Děti	dítě	k1gFnPc1	dítě
ráje	ráje	k1gFnPc1	ráje
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
premiéra	premiéra	k1gFnSc1	premiéra
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
14	[number]	k4	14
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2009	[number]	k4	2009
a	a	k8xC	a
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
muzikál	muzikál	k1gInSc1	muzikál
stihl	stihnout	k5eAaPmAgInS	stihnout
odehrát	odehrát	k5eAaPmF	odehrát
necelých	celý	k2eNgFnPc2d1	necelá
500	[number]	k4	500
repríz	repríza	k1gFnPc2	repríza
<g/>
.	.	kIx.	.
</s>
<s>
Uvedení	uvedení	k1gNnSc1	uvedení
muzikálu	muzikál	k1gInSc2	muzikál
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
i	i	k8xC	i
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
jsme	být	k5eAaImIp1nP	být
ho	on	k3xPp3gInSc4	on
mohli	moct	k5eAaImAgMnP	moct
od	od	k7c2	od
jara	jaro	k1gNnSc2	jaro
2012	[number]	k4	2012
také	také	k9	také
vídat	vídat	k5eAaImF	vídat
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
Goja	Goja	k1gMnSc1	Goja
Music	Music	k1gMnSc1	Music
Hall	Hall	k1gMnSc1	Hall
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Enjolrase	Enjolras	k1gInSc6	Enjolras
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
hrdinského	hrdinský	k2eAgInSc2d1	hrdinský
vůdce	vůdce	k1gMnPc4	vůdce
revolucionářů	revolucionář	k1gMnPc2	revolucionář
<g/>
,	,	kIx,	,
v	v	k7c6	v
nové	nový	k2eAgFnSc6d1	nová
české	český	k2eAgFnSc6d1	Česká
inscenaci	inscenace	k1gFnSc6	inscenace
jednoho	jeden	k4xCgMnSc2	jeden
z	z	k7c2	z
nejslavnějších	slavný	k2eAgInPc2d3	nejslavnější
světových	světový	k2eAgInPc2d1	světový
muzikálů	muzikál	k1gInPc2	muzikál
Les	les	k1gInSc1	les
Miserables-Bídníci	Miserables-Bídník	k1gMnPc1	Miserables-Bídník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nyní	nyní	k6eAd1	nyní
se	se	k3xPyFc4	se
Michael	Michael	k1gMnSc1	Michael
připravuje	připravovat	k5eAaImIp3nS	připravovat
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
novou	nový	k2eAgFnSc4d1	nová
roli	role	k1gFnSc4	role
v	v	k7c6	v
dalším	další	k2eAgInSc6d1	další
světovém	světový	k2eAgInSc6d1	světový
muzikálu	muzikál	k1gInSc6	muzikál
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
premiéra	premiéra	k1gFnSc1	premiéra
proběhne	proběhnout	k5eAaPmIp3nS	proběhnout
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2014	[number]	k4	2014
v	v	k7c6	v
pražském	pražský	k2eAgNnSc6d1	Pražské
Kongresovém	kongresový	k2eAgNnSc6d1	Kongresové
centru	centrum	k1gNnSc6	centrum
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
živým	živý	k2eAgInSc7d1	živý
orchestrem	orchestr	k1gInSc7	orchestr
<g/>
,	,	kIx,	,
velkolepou	velkolepý	k2eAgFnSc7d1	velkolepá
scénou	scéna	k1gFnSc7	scéna
a	a	k8xC	a
těmi	ten	k3xDgInPc7	ten
největšími	veliký	k2eAgInPc7d3	veliký
hity	hit	k1gInPc7	hit
legendární	legendární	k2eAgFnSc2d1	legendární
skupiny	skupina	k1gFnSc2	skupina
ABBA	ABBA	kA	ABBA
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
jako	jako	k9	jako
hlavní	hlavní	k2eAgMnSc1d1	hlavní
představitel	představitel	k1gMnSc1	představitel
"	"	kIx"	"
<g/>
Sky	Sky	k1gMnSc1	Sky
<g/>
"	"	kIx"	"
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
verzi	verze	k1gFnSc6	verze
nejúspěšnějšího	úspěšný	k2eAgInSc2d3	nejúspěšnější
hit-muzikálu	hituzikál	k1gInSc2	hit-muzikál
na	na	k7c4	na
světě-	světě-	k?	světě-
Mamma	Mamma	k1gNnSc4	Mamma
Mia	Mia	k1gFnSc2	Mia
<g/>
!	!	kIx.	!
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
hudebních	hudební	k2eAgFnPc2d1	hudební
zkušeností	zkušenost	k1gFnPc2	zkušenost
se	se	k3xPyFc4	se
Michael	Michael	k1gMnSc1	Michael
věnuje	věnovat	k5eAaPmIp3nS	věnovat
i	i	k9	i
herectví	herectví	k1gNnSc1	herectví
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
v	v	k7c6	v
teenagerovském	teenagerovský	k2eAgInSc6d1	teenagerovský
filmu	film	k1gInSc6	film
Experti	expert	k1gMnPc1	expert
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nazpíval	nazpívat	k5eAaBmAgInS	nazpívat
i	i	k9	i
titulní	titulní	k2eAgFnSc4d1	titulní
píseň	píseň	k1gFnSc4	píseň
Expert	expert	k1gMnSc1	expert
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
premiéra	premiéra	k1gFnSc1	premiéra
úspěšného	úspěšný	k2eAgNnSc2d1	úspěšné
retro	retro	k1gNnSc2	retro
filmu	film	k1gInSc2	film
Probudím	probudit	k5eAaPmIp1nS	probudit
se	se	k3xPyFc4	se
včera	včera	k6eAd1	včera
režiséra	režisér	k1gMnSc2	režisér
Miloslava	Miloslav	k1gMnSc2	Miloslav
Šmídmajera	Šmídmajer	k1gMnSc2	Šmídmajer
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
<g/>
[	[	kIx(	[
<g/>
kdy	kdy	k6eAd1	kdy
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
připravuje	připravovat	k5eAaImIp3nS	připravovat
své	svůj	k3xOyFgInPc4	svůj
třetí	třetí	k4xOgInPc4	třetí
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
autorské	autorský	k2eAgNnSc4d1	autorské
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
pilotním	pilotní	k2eAgInSc7d1	pilotní
singlem	singl	k1gInSc7	singl
je	být	k5eAaImIp3nS	být
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
HRA	hra	k1gFnSc1	hra
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Studiová	studiový	k2eAgNnPc4d1	studiové
alba	album	k1gNnPc4	album
===	===	k?	===
</s>
</p>
<p>
<s>
2006	[number]	k4	2006
–	–	k?	–
Forte	forte	k6eAd1	forte
</s>
</p>
<p>
<s>
2009	[number]	k4	2009
–	–	k?	–
Uvnitř	uvnitř	k7c2	uvnitř
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Michael	Michael	k1gMnSc1	Michael
Foret	Foret	k1gMnSc1	Foret
</s>
</p>
