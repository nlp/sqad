<s>
Ruchovci	ruchovec	k1gMnPc1	ruchovec
(	(	kIx(	(
<g/>
takzvaná	takzvaný	k2eAgFnSc1d1	takzvaná
škola	škola	k1gFnSc1	škola
národní	národní	k2eAgFnSc1d1	národní
<g/>
)	)	kIx)	)
byli	být	k5eAaImAgMnP	být
čeští	český	k2eAgMnPc1d1	český
básníci	básník	k1gMnPc1	básník
a	a	k8xC	a
spisovatelé	spisovatel	k1gMnPc1	spisovatel
narozeni	narozen	k2eAgMnPc1d1	narozen
mezi	mezi	k7c4	mezi
lety	let	k1gInPc4	let
1845	[number]	k4	1845
<g/>
–	–	k?	–
<g/>
1855	[number]	k4	1855
<g/>
.	.	kIx.	.
</s>
<s>
Pocházejí	pocházet	k5eAaImIp3nP	pocházet
vesměs	vesměs	k6eAd1	vesměs
ze	z	k7c2	z
zámožnějších	zámožný	k2eAgFnPc2d2	zámožnější
středních	střední	k2eAgFnPc2d1	střední
vrstev	vrstva	k1gFnPc2	vrstva
nebo	nebo	k8xC	nebo
inteligence	inteligence	k1gFnSc2	inteligence
<g/>
.	.	kIx.	.
</s>
<s>
Almanach	almanach	k1gInSc1	almanach
Ruch	ruch	k1gInSc1	ruch
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
poprvé	poprvé	k6eAd1	poprvé
vyšel	vyjít	k5eAaPmAgMnS	vyjít
roku	rok	k1gInSc2	rok
1868	[number]	k4	1868
při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
položení	položení	k1gNnSc2	položení
základního	základní	k2eAgInSc2d1	základní
kamene	kámen	k1gInSc2	kámen
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nápadně	nápadně	k6eAd1	nápadně
odlišoval	odlišovat	k5eAaImAgInS	odlišovat
od	od	k7c2	od
almanachu	almanach	k1gInSc2	almanach
Máje	máj	k1gInSc2	máj
<g/>
.	.	kIx.	.
</s>
<s>
Vanulo	vanout	k5eAaImAgNnS	vanout
z	z	k7c2	z
něho	on	k3xPp3gNnSc2	on
nadšení	nadšení	k1gNnSc2	nadšení
<g/>
,	,	kIx,	,
odhodlané	odhodlaný	k2eAgNnSc1d1	odhodlané
vlastenectví	vlastenectví	k1gNnSc1	vlastenectví
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
ruchovců	ruchovec	k1gMnPc2	ruchovec
znovu	znovu	k6eAd1	znovu
ožívá	ožívat	k5eAaImIp3nS	ožívat
myšlenka	myšlenka	k1gFnSc1	myšlenka
slovanské	slovanský	k2eAgFnSc2d1	Slovanská
jednoty	jednota	k1gFnSc2	jednota
a	a	k8xC	a
do	do	k7c2	do
její	její	k3xOp3gFnSc2	její
utopické	utopický	k2eAgFnSc2d1	utopická
vize	vize	k1gFnSc2	vize
proniká	pronikat	k5eAaImIp3nS	pronikat
ohlas	ohlas	k1gInSc1	ohlas
soudobého	soudobý	k2eAgInSc2d1	soudobý
sociálního	sociální	k2eAgInSc2d1	sociální
anarchismu	anarchismus	k1gInSc2	anarchismus
(	(	kIx(	(
<g/>
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Čech	Čech	k1gMnSc1	Čech
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přispívali	přispívat	k5eAaImAgMnP	přispívat
do	do	k7c2	do
časopisu	časopis	k1gInSc2	časopis
Osvěta	osvěta	k1gFnSc1	osvěta
<g/>
.	.	kIx.	.
</s>
<s>
Požadovali	požadovat	k5eAaImAgMnP	požadovat
účinné	účinný	k2eAgNnSc4d1	účinné
vlastenectví	vlastenectví	k1gNnSc4	vlastenectví
<g/>
,	,	kIx,	,
obraceli	obracet	k5eAaImAgMnP	obracet
se	se	k3xPyFc4	se
k	k	k7c3	k
slovanskému	slovanský	k2eAgInSc3d1	slovanský
východu	východ	k1gInSc3	východ
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
k	k	k7c3	k
Rusku	Rusko	k1gNnSc3	Rusko
a	a	k8xC	a
sledovali	sledovat	k5eAaImAgMnP	sledovat
se	s	k7c7	s
sympatiemi	sympatie	k1gFnPc7	sympatie
osvobozovací	osvobozovací	k2eAgInSc4d1	osvobozovací
boje	boj	k1gInPc4	boj
balkánských	balkánský	k2eAgMnPc2d1	balkánský
Slovanů	Slovan	k1gMnPc2	Slovan
(	(	kIx(	(
<g/>
Eliška	Eliška	k1gFnSc1	Eliška
Krásnohorská	krásnohorský	k2eAgFnSc1d1	Krásnohorská
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Holeček	Holeček	k1gMnSc1	Holeček
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zdůraznili	zdůraznit	k5eAaPmAgMnP	zdůraznit
i	i	k9	i
potřebu	potřeba	k1gFnSc4	potřeba
užšího	úzký	k2eAgInSc2d2	užší
svazku	svazek	k1gInSc2	svazek
se	s	k7c7	s
slovenským	slovenský	k2eAgInSc7d1	slovenský
národem	národ	k1gInSc7	národ
(	(	kIx(	(
<g/>
Adolf	Adolf	k1gMnSc1	Adolf
Heyduk	Heyduk	k1gMnSc1	Heyduk
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
Pokorný	Pokorný	k1gMnSc1	Pokorný
<g/>
,	,	kIx,	,
Frantíšek	Frantíšek	k1gMnSc1	Frantíšek
Táborský	Táborský	k1gMnSc1	Táborský
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Horlivě	horlivě	k6eAd1	horlivě
překládali	překládat	k5eAaImAgMnP	překládat
ze	z	k7c2	z
slovenských	slovenský	k2eAgFnPc2d1	slovenská
literatur	literatura	k1gFnPc2	literatura
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
převažuje	převažovat	k5eAaImIp3nS	převažovat
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
romantickou	romantický	k2eAgFnSc4d1	romantická
poezii	poezie	k1gFnSc4	poezie
<g/>
.	.	kIx.	.
</s>
<s>
Obnovují	obnovovat	k5eAaImIp3nP	obnovovat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
tvorbě	tvorba	k1gFnSc6	tvorba
historickou	historický	k2eAgFnSc4d1	historická
tematiku	tematika	k1gFnSc4	tematika
<g/>
.	.	kIx.	.
</s>
<s>
Silný	silný	k2eAgInSc1d1	silný
je	být	k5eAaImIp3nS	být
zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
venkov	venkov	k1gInSc4	venkov
a	a	k8xC	a
selství	selství	k1gNnSc4	selství
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
shledává	shledávat	k5eAaImIp3nS	shledávat
základ	základ	k1gInSc1	základ
národa	národ	k1gInSc2	národ
a	a	k8xC	a
ochrana	ochrana	k1gFnSc1	ochrana
před	před	k7c7	před
rostoucí	rostoucí	k2eAgFnSc7d1	rostoucí
diferenciací	diferenciace	k1gFnSc7	diferenciace
národní	národní	k2eAgFnSc2d1	národní
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Vlastenecká	vlastenecký	k2eAgFnSc1d1	vlastenecká
poezie	poezie	k1gFnSc1	poezie
této	tento	k3xDgFnSc2	tento
generace	generace	k1gFnSc2	generace
mívá	mívat	k5eAaImIp3nS	mívat
podobu	podoba	k1gFnSc4	podoba
reflexivní	reflexivní	k2eAgFnSc2d1	reflexivní
lyriky	lyrika	k1gFnSc2	lyrika
nebo	nebo	k8xC	nebo
epiky	epika	k1gFnSc2	epika
s	s	k7c7	s
historickými	historický	k2eAgFnPc7d1	historická
či	či	k8xC	či
alegorickými	alegorický	k2eAgFnPc7d1	alegorická
látkami	látka	k1gFnPc7	látka
<g/>
,	,	kIx,	,
horlí	horlit	k5eAaImIp3nP	horlit
pro	pro	k7c4	pro
národní	národní	k2eAgFnSc4d1	národní
jednotu	jednota	k1gFnSc4	jednota
a	a	k8xC	a
vyzývá	vyzývat	k5eAaImIp3nS	vyzývat
k	k	k7c3	k
politické	politický	k2eAgFnSc3d1	politická
aktivitě	aktivita	k1gFnSc3	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Formu	forma	k1gFnSc4	forma
i	i	k8xC	i
umělecké	umělecký	k2eAgInPc4d1	umělecký
prostředky	prostředek	k1gInPc4	prostředek
čerpají	čerpat	k5eAaImIp3nP	čerpat
básníci	básník	k1gMnPc1	básník
částečně	částečně	k6eAd1	částečně
z	z	k7c2	z
lidové	lidový	k2eAgFnSc2d1	lidová
písňové	písňový	k2eAgFnSc2d1	písňová
formy	forma	k1gFnSc2	forma
<g/>
,	,	kIx,	,
v	v	k7c6	v
epických	epický	k2eAgInPc6d1	epický
útvarech	útvar	k1gInPc6	útvar
napodobují	napodobovat	k5eAaImIp3nP	napodobovat
baladu	balada	k1gFnSc4	balada
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
toho	ten	k3xDgNnSc2	ten
však	však	k9	však
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
i	i	k9	i
vzrušený	vzrušený	k2eAgInSc4d1	vzrušený
patetický	patetický	k2eAgInSc4d1	patetický
sloh	sloh	k1gInSc4	sloh
<g/>
,	,	kIx,	,
zatížený	zatížený	k2eAgInSc1d1	zatížený
rétorikou	rétorika	k1gFnSc7	rétorika
(	(	kIx(	(
<g/>
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
Krásnohorská	krásnohorský	k2eAgFnSc1d1	Krásnohorská
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
májovci	májovec	k1gMnPc7	májovec
jsou	být	k5eAaImIp3nP	být
básníci	básník	k1gMnPc1	básník
sdružení	sdružený	k2eAgMnPc1d1	sdružený
kolem	kolem	k7c2	kolem
almanachu	almanach	k1gInSc2	almanach
Ruch	ruch	k1gInSc1	ruch
tradičnější	tradiční	k2eAgInSc1d2	tradičnější
v	v	k7c6	v
látce	látka	k1gFnSc6	látka
i	i	k8xC	i
formě	forma	k1gFnSc6	forma
<g/>
.	.	kIx.	.
</s>
<s>
Redaktorem	redaktor	k1gMnSc7	redaktor
almanachu	almanach	k1gInSc2	almanach
byl	být	k5eAaImAgMnS	být
J.V.	J.V.	k1gMnSc1	J.V.
<g/>
Sládek	Sládek	k1gMnSc1	Sládek
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
později	pozdě	k6eAd2	pozdě
z	z	k7c2	z
Ruchu	ruch	k1gInSc2	ruch
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
a	a	k8xC	a
přidal	přidat	k5eAaPmAgMnS	přidat
se	se	k3xPyFc4	se
k	k	k7c3	k
lumírovcům	lumírovec	k1gMnPc3	lumírovec
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
nejhodnotnější	hodnotný	k2eAgFnSc1d3	nejhodnotnější
báseň	báseň	k1gFnSc1	báseň
<g/>
,	,	kIx,	,
Husita	husita	k1gMnSc1	husita
na	na	k7c6	na
Baltu	Balt	k1gInSc6	Balt
<g/>
,	,	kIx,	,
tam	tam	k6eAd1	tam
vložil	vložit	k5eAaPmAgMnS	vložit
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Čech	Čech	k1gMnSc1	Čech
<g/>
.	.	kIx.	.
</s>
<s>
Jedinou	jediný	k2eAgFnSc7d1	jediná
kritickou	kritický	k2eAgFnSc7d1	kritická
osobností	osobnost	k1gFnSc7	osobnost
byla	být	k5eAaImAgFnS	být
Eliška	Eliška	k1gFnSc1	Eliška
Krásnohorská	krásnohorský	k2eAgFnSc1d1	Krásnohorská
<g/>
.	.	kIx.	.
</s>
<s>
Almanach	almanach	k1gInSc1	almanach
měl	mít	k5eAaImAgInS	mít
asi	asi	k9	asi
dvacet	dvacet	k4xCc4	dvacet
přispěvatelů	přispěvatel	k1gMnPc2	přispěvatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
literatuře	literatura	k1gFnSc3	literatura
věrných	věrný	k2eAgInPc2d1	věrný
jen	jen	k6eAd1	jen
několik	několik	k4yIc1	několik
<g/>
.	.	kIx.	.
</s>
<s>
Alois	Alois	k1gMnSc1	Alois
Jirásek	Jirásek	k1gMnSc1	Jirásek
Eliška	Eliška	k1gFnSc1	Eliška
Krásnohorská	krásnohorský	k2eAgFnSc1d1	Krásnohorská
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Čech	Čechy	k1gFnPc2	Čechy
Ladislav	Ladislav	k1gMnSc1	Ladislav
Quis	Quisa	k1gFnPc2	Quisa
Josef	Josef	k1gMnSc1	Josef
Václav	Václav	k1gMnSc1	Václav
Sládek	Sládek	k1gMnSc1	Sládek
(	(	kIx(	(
<g/>
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
<g/>
)	)	kIx)	)
</s>
