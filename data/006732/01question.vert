<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
čeští	český	k2eAgMnPc1d1	český
básníci	básník	k1gMnPc1	básník
a	a	k8xC	a
spisovatelé	spisovatel	k1gMnPc1	spisovatel
narození	narození	k1gNnSc2	narození
mezi	mezi	k7c4	mezi
lety	let	k1gInPc4	let
1845	[number]	k4	1845
-	-	kIx~	-
1855	[number]	k4	1855
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
škola	škola	k1gFnSc1	škola
národní	národní	k2eAgFnSc1d1	národní
<g/>
)	)	kIx)	)
<g/>
?	?	kIx.	?
</s>
