<s>
Zahrada	zahrada	k1gFnSc1	zahrada
je	být	k5eAaImIp3nS	být
slovenský	slovenský	k2eAgInSc4d1	slovenský
poetický	poetický	k2eAgInSc4d1	poetický
film	film	k1gInSc4	film
natočený	natočený	k2eAgInSc4d1	natočený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
v	v	k7c6	v
režii	režie	k1gFnSc6	režie
Martina	Martin	k1gMnSc2	Martin
Šulíka	Šulík	k1gMnSc2	Šulík
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
získal	získat	k5eAaPmAgInS	získat
cenu	cena	k1gFnSc4	cena
Český	český	k2eAgInSc1d1	český
lev	lev	k1gInSc1	lev
za	za	k7c4	za
rok	rok	k1gInSc4	rok
1995	[number]	k4	1995
v	v	k7c6	v
pěti	pět	k4xCc6	pět
kategoriích	kategorie	k1gFnPc6	kategorie
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
cen	cena	k1gFnPc2	cena
pro	pro	k7c4	pro
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
režii	režie	k1gFnSc4	režie
a	a	k8xC	a
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
scénář	scénář	k1gInSc4	scénář
<g/>
.	.	kIx.	.
</s>
<s>
Zahrada	zahrada	k1gFnSc1	zahrada
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Zahrada	zahrada	k1gFnSc1	zahrada
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
