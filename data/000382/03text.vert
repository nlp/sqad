<s>
Adolf	Adolf	k1gMnSc1	Adolf
Hitler	Hitler	k1gMnSc1	Hitler
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1889	[number]	k4	1889
Braunau	Brauna	k2eAgFnSc4d1	Brauna
am	am	k?	am
Inn	Inn	k1gFnSc4	Inn
-	-	kIx~	-
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1945	[number]	k4	1945
Berlín	Berlín	k1gInSc1	Berlín
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
německý	německý	k2eAgMnSc1d1	německý
nacistický	nacistický	k2eAgMnSc1d1	nacistický
politik	politik	k1gMnSc1	politik
rakouského	rakouský	k2eAgInSc2d1	rakouský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1933	[number]	k4	1933
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
kancléř	kancléř	k1gMnSc1	kancléř
a	a	k8xC	a
diktátor	diktátor	k1gMnSc1	diktátor
nacistického	nacistický	k2eAgNnSc2d1	nacistické
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
takzvaný	takzvaný	k2eAgMnSc1d1	takzvaný
Vůdce	vůdce	k1gMnSc1	vůdce
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Führer	Führer	k1gMnSc1	Führer
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
odpovědný	odpovědný	k2eAgMnSc1d1	odpovědný
za	za	k7c4	za
zločiny	zločin	k1gInPc4	zločin
nacistického	nacistický	k2eAgInSc2d1	nacistický
režimu	režim	k1gInSc2	režim
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
za	za	k7c4	za
vyvražďování	vyvražďování	k1gNnPc4	vyvražďování
Židů	Žid	k1gMnPc2	Žid
<g/>
,	,	kIx,	,
Romů	Rom	k1gMnPc2	Rom
a	a	k8xC	a
postižených	postižený	k1gMnPc2	postižený
<g/>
.	.	kIx.	.
</s>
<s>
Agresivní	agresivní	k2eAgFnSc7d1	agresivní
politikou	politika	k1gFnSc7	politika
zprvu	zprvu	k6eAd1	zprvu
dosahoval	dosahovat	k5eAaImAgInS	dosahovat
územní	územní	k2eAgInPc4d1	územní
zisky	zisk	k1gInPc4	zisk
a	a	k8xC	a
ústupky	ústupek	k1gInPc4	ústupek
jiných	jiný	k2eAgInPc2d1	jiný
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
však	však	k9	však
napadením	napadení	k1gNnSc7	napadení
Polska	Polsko	k1gNnSc2	Polsko
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
druhou	druhý	k4xOgFnSc4	druhý
světovou	světový	k2eAgFnSc4d1	světová
válku	válka	k1gFnSc4	válka
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
Německo	Německo	k1gNnSc1	Německo
nakonec	nakonec	k6eAd1	nakonec
utrpělo	utrpět	k5eAaPmAgNnS	utrpět
drtivou	drtivý	k2eAgFnSc4d1	drtivá
porážku	porážka	k1gFnSc4	porážka
<g/>
.	.	kIx.	.
</s>
<s>
Hitler	Hitler	k1gMnSc1	Hitler
pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
rodiny	rodina	k1gFnSc2	rodina
rakouského	rakouský	k2eAgMnSc2d1	rakouský
celního	celní	k2eAgMnSc2d1	celní
úředníka	úředník	k1gMnSc2	úředník
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
nadaný	nadaný	k2eAgMnSc1d1	nadaný
<g/>
,	,	kIx,	,
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
však	však	k9	však
systematicky	systematicky	k6eAd1	systematicky
pracovat	pracovat	k5eAaImF	pracovat
a	a	k8xC	a
dodržovat	dodržovat	k5eAaImF	dodržovat
kázeň	kázeň	k1gFnSc4	kázeň
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
nedosáhl	dosáhnout	k5eNaPmAgInS	dosáhnout
maturity	maturita	k1gFnPc4	maturita
<g/>
.	.	kIx.	.
</s>
<s>
Toužil	toužit	k5eAaImAgMnS	toužit
stát	stát	k5eAaImF	stát
se	se	k3xPyFc4	se
umělcem	umělec	k1gMnSc7	umělec
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
nedostatek	nedostatek	k1gInSc4	nedostatek
výtvarného	výtvarný	k2eAgInSc2d1	výtvarný
talentu	talent	k1gInSc2	talent
však	však	k9	však
nebyl	být	k5eNaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
na	na	k7c4	na
uměleckou	umělecký	k2eAgFnSc4d1	umělecká
školu	škola	k1gFnSc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
rodičů	rodič	k1gMnPc2	rodič
se	se	k3xPyFc4	se
protloukal	protloukat	k5eAaImAgInS	protloukat
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
jako	jako	k9	jako
nezaměstnaný	nezaměstnaný	k1gMnSc1	nezaměstnaný
bez	bez	k7c2	bez
domova	domov	k1gInSc2	domov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
utvrdil	utvrdit	k5eAaPmAgMnS	utvrdit
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
nacionalistických	nacionalistický	k2eAgInPc6d1	nacionalistický
<g/>
,	,	kIx,	,
rasistických	rasistický	k2eAgInPc6d1	rasistický
a	a	k8xC	a
antisemitských	antisemitský	k2eAgInPc6d1	antisemitský
názorech	názor	k1gInPc6	názor
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
se	se	k3xPyFc4	se
zajímat	zajímat	k5eAaImF	zajímat
o	o	k7c4	o
politiku	politika	k1gFnSc4	politika
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1913	[number]	k4	1913
se	se	k3xPyFc4	se
přesunul	přesunout	k5eAaPmAgMnS	přesunout
do	do	k7c2	do
Mnichova	Mnichov	k1gInSc2	Mnichov
a	a	k8xC	a
po	po	k7c6	po
vypuknutí	vypuknutí	k1gNnSc6	vypuknutí
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
dobrovolně	dobrovolně	k6eAd1	dobrovolně
přihlásil	přihlásit	k5eAaPmAgMnS	přihlásit
do	do	k7c2	do
německé	německý	k2eAgFnSc2d1	německá
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
bojích	boj	k1gInPc6	boj
raněn	ranit	k5eAaPmNgMnS	ranit
a	a	k8xC	a
vyznamenán	vyznamenat	k5eAaPmNgInS	vyznamenat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
tehdy	tehdy	k6eAd1	tehdy
nepatrné	patrný	k2eNgFnSc2d1	patrný
nacistické	nacistický	k2eAgFnSc2d1	nacistická
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
strhl	strhnout	k5eAaPmAgMnS	strhnout
její	její	k3xOp3gNnPc4	její
vedení	vedení	k1gNnPc4	vedení
<g/>
,	,	kIx,	,
když	když	k8xS	když
dokázal	dokázat	k5eAaPmAgMnS	dokázat
svým	svůj	k3xOyFgNnSc7	svůj
charismatickým	charismatický	k2eAgNnSc7d1	charismatické
a	a	k8xC	a
populistickým	populistický	k2eAgNnSc7d1	populistické
řečnictvím	řečnictví	k1gNnSc7	řečnictví
zmnohonásobit	zmnohonásobit	k5eAaPmF	zmnohonásobit
členstvo	členstvo	k1gNnSc4	členstvo
<g/>
.	.	kIx.	.
</s>
<s>
Vedl	vést	k5eAaImAgInS	vést
nezdařený	zdařený	k2eNgInSc1d1	nezdařený
mnichovský	mnichovský	k2eAgInSc1d1	mnichovský
puč	puč	k1gInSc1	puč
8	[number]	k4	8
<g/>
.	.	kIx.	.
a	a	k8xC	a
9	[number]	k4	9
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1923	[number]	k4	1923
<g/>
,	,	kIx,	,
po	po	k7c6	po
němž	jenž	k3xRgInSc6	jenž
byl	být	k5eAaImAgInS	být
sice	sice	k8xC	sice
odsouzen	odsoudit	k5eAaPmNgInS	odsoudit
do	do	k7c2	do
vězení	vězení	k1gNnSc2	vězení
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
setrval	setrvat	k5eAaPmAgMnS	setrvat
přes	přes	k7c4	přes
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
využil	využít	k5eAaPmAgInS	využít
však	však	k9	však
soudní	soudní	k2eAgInSc1d1	soudní
proces	proces	k1gInSc1	proces
ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
své	svůj	k3xOyFgFnSc2	svůj
popularity	popularita	k1gFnSc2	popularita
a	a	k8xC	a
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
napsal	napsat	k5eAaBmAgInS	napsat
své	svůj	k3xOyFgNnSc4	svůj
hlavní	hlavní	k2eAgNnSc4d1	hlavní
programové	programový	k2eAgNnSc4d1	programové
dílo	dílo	k1gNnSc4	dílo
Mein	Mein	k1gMnSc1	Mein
Kampf	Kampf	k1gMnSc1	Kampf
(	(	kIx(	(
<g/>
Můj	můj	k3xOp1gInSc1	můj
boj	boj	k1gInSc1	boj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vypuknutí	vypuknutí	k1gNnSc6	vypuknutí
velké	velký	k2eAgFnSc2d1	velká
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
krize	krize	k1gFnSc2	krize
koncem	koncem	k7c2	koncem
dvacátých	dvacátý	k4xOgNnPc2	dvacátý
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
v	v	k7c6	v
těžce	těžce	k6eAd1	těžce
postiženém	postižený	k2eAgNnSc6d1	postižené
Německu	Německo	k1gNnSc6	Německo
začalo	začít	k5eAaPmAgNnS	začít
dařit	dařit	k5eAaImF	dařit
extremistům	extremista	k1gMnPc3	extremista
a	a	k8xC	a
Hitlerova	Hitlerův	k2eAgNnSc2d1	Hitlerovo
NSDAP	NSDAP	kA	NSDAP
se	se	k3xPyFc4	se
vypracovala	vypracovat	k5eAaPmAgFnS	vypracovat
na	na	k7c4	na
nejsilnější	silný	k2eAgFnSc4d3	nejsilnější
politickou	politický	k2eAgFnSc4d1	politická
stranu	strana	k1gFnSc4	strana
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
přitom	přitom	k6eAd1	přitom
získala	získat	k5eAaPmAgFnS	získat
i	i	k9	i
podporu	podpora	k1gFnSc4	podpora
části	část	k1gFnSc2	část
konzervativních	konzervativní	k2eAgFnPc2d1	konzervativní
elit	elita	k1gFnPc2	elita
a	a	k8xC	a
průmyslníků	průmyslník	k1gMnPc2	průmyslník
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
obávali	obávat	k5eAaImAgMnP	obávat
rostoucího	rostoucí	k2eAgInSc2d1	rostoucí
vlivu	vliv	k1gInSc2	vliv
komunistů	komunista	k1gMnPc2	komunista
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1933	[number]	k4	1933
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
Hitler	Hitler	k1gMnSc1	Hitler
jmenování	jmenování	k1gNnSc2	jmenování
kancléřem	kancléř	k1gMnSc7	kancléř
<g/>
,	,	kIx,	,
zprvu	zprvu	k6eAd1	zprvu
ve	v	k7c6	v
spojenectví	spojenectví	k1gNnSc6	spojenectví
s	s	k7c7	s
částí	část	k1gFnSc7	část
konzervativců	konzervativec	k1gMnPc2	konzervativec
<g/>
,	,	kIx,	,
brzy	brzy	k6eAd1	brzy
však	však	k9	však
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
nacisté	nacista	k1gMnPc1	nacista
nevybíravými	vybíravý	k2eNgFnPc7d1	nevybíravá
metodami	metoda	k1gFnPc7	metoda
strhli	strhnout	k5eAaPmAgMnP	strhnout
absolutní	absolutní	k2eAgFnSc4d1	absolutní
moc	moc	k1gFnSc4	moc
<g/>
.	.	kIx.	.
</s>
<s>
Hitler	Hitler	k1gMnSc1	Hitler
se	se	k3xPyFc4	se
tvrdě	tvrdě	k6eAd1	tvrdě
vypořádal	vypořádat	k5eAaPmAgMnS	vypořádat
s	s	k7c7	s
opozicí	opozice	k1gFnSc7	opozice
ve	v	k7c6	v
vlastní	vlastní	k2eAgFnSc6d1	vlastní
straně	strana	k1gFnSc6	strana
i	i	k9	i
mimo	mimo	k7c4	mimo
ni	on	k3xPp3gFnSc4	on
<g/>
,	,	kIx,	,
k	k	k7c3	k
obětem	oběť	k1gFnPc3	oběť
takzvané	takzvaný	k2eAgFnSc2d1	takzvaná
noci	noc	k1gFnSc2	noc
dlouhých	dlouhý	k2eAgInPc2d1	dlouhý
nožů	nůž	k1gInPc2	nůž
z	z	k7c2	z
29	[number]	k4	29
<g/>
.	.	kIx.	.
na	na	k7c4	na
30	[number]	k4	30
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1934	[number]	k4	1934
patřili	patřit	k5eAaImAgMnP	patřit
i	i	k9	i
velitelé	velitel	k1gMnPc1	velitel
stranické	stranický	k2eAgFnSc2d1	stranická
milice	milice	k1gFnSc2	milice
SA	SA	kA	SA
nebo	nebo	k8xC	nebo
předchozí	předchozí	k2eAgMnSc1d1	předchozí
kancléř	kancléř	k1gMnSc1	kancléř
Kurt	Kurt	k1gMnSc1	Kurt
von	von	k1gInSc4	von
Schleicher	Schleichra	k1gFnPc2	Schleichra
<g/>
.	.	kIx.	.
</s>
<s>
Německo	Německo	k1gNnSc1	Německo
začalo	začít	k5eAaPmAgNnS	začít
zbrojit	zbrojit	k5eAaImF	zbrojit
a	a	k8xC	a
Hitler	Hitler	k1gMnSc1	Hitler
metodou	metoda	k1gFnSc7	metoda
stupňování	stupňování	k1gNnSc2	stupňování
požadavků	požadavek	k1gInPc2	požadavek
a	a	k8xC	a
hrozbou	hrozba	k1gFnSc7	hrozba
agrese	agrese	k1gFnSc2	agrese
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
významných	významný	k2eAgInPc2d1	významný
ústupků	ústupek	k1gInPc2	ústupek
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
západních	západní	k2eAgInPc2d1	západní
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
tak	tak	k6eAd1	tak
k	k	k7c3	k
Německé	německý	k2eAgFnSc3d1	německá
říši	říš	k1gFnSc3	říš
mohl	moct	k5eAaImAgInS	moct
připojit	připojit	k5eAaPmF	připojit
Rakousko	Rakousko	k1gNnSc4	Rakousko
a	a	k8xC	a
na	na	k7c6	na
základě	základ	k1gInSc6	základ
Mnichovské	mnichovský	k2eAgFnSc2d1	Mnichovská
dohody	dohoda	k1gFnSc2	dohoda
část	část	k1gFnSc1	část
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
obsadila	obsadit	k5eAaPmAgFnS	obsadit
německá	německý	k2eAgFnSc1d1	německá
vojska	vojsko	k1gNnPc1	vojsko
zbytek	zbytek	k1gInSc4	zbytek
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
a	a	k8xC	a
po	po	k7c6	po
dohodě	dohoda	k1gFnSc6	dohoda
se	s	k7c7	s
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
větší	veliký	k2eAgFnSc4d2	veliký
část	část	k1gFnSc4	část
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Napadení	napadení	k1gNnSc1	napadení
Polska	Polsko	k1gNnSc2	Polsko
však	však	k9	však
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
globální	globální	k2eAgFnSc4d1	globální
válku	válka	k1gFnSc4	válka
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
bylo	být	k5eAaImAgNnS	být
Německo	Německo	k1gNnSc1	Německo
zprvu	zprvu	k6eAd1	zprvu
úspěšné	úspěšný	k2eAgNnSc1d1	úspěšné
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
německá	německý	k2eAgFnSc1d1	německá
armáda	armáda	k1gFnSc1	armáda
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
nad	nad	k7c7	nad
Francií	Francie	k1gFnSc7	Francie
<g/>
,	,	kIx,	,
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
přepadla	přepadnout	k5eAaPmAgFnS	přepadnout
Sovětský	sovětský	k2eAgInSc4d1	sovětský
svaz	svaz	k1gInSc4	svaz
a	a	k8xC	a
postoupila	postoupit	k5eAaPmAgFnS	postoupit
hluboko	hluboko	k6eAd1	hluboko
do	do	k7c2	do
jeho	on	k3xPp3gNnSc2	on
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1943	[number]	k4	1943
však	však	k9	však
začala	začít	k5eAaPmAgFnS	začít
nabývat	nabývat	k5eAaImF	nabývat
vrchu	vrch	k1gInSc2	vrch
koalice	koalice	k1gFnSc2	koalice
Spojenců	spojenec	k1gMnPc2	spojenec
a	a	k8xC	a
Hitler	Hitler	k1gMnSc1	Hitler
svými	svůj	k3xOyFgInPc7	svůj
nevhodnými	vhodný	k2eNgInPc7d1	nevhodný
zásahy	zásah	k1gInPc7	zásah
do	do	k7c2	do
velení	velení	k1gNnSc2	velení
armády	armáda	k1gFnSc2	armáda
situaci	situace	k1gFnSc4	situace
ještě	ještě	k6eAd1	ještě
zhoršoval	zhoršovat	k5eAaImAgMnS	zhoršovat
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
vyvázl	vyváznout	k5eAaPmAgMnS	vyváznout
z	z	k7c2	z
atentátu	atentát	k1gInSc2	atentát
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
zosnovala	zosnovat	k5eAaPmAgFnS	zosnovat
skupina	skupina	k1gFnSc1	skupina
důstojníků	důstojník	k1gMnPc2	důstojník
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
plukovníkem	plukovník	k1gMnSc7	plukovník
Stauffenbergem	Stauffenberg	k1gMnSc7	Stauffenberg
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
chtěla	chtít	k5eAaImAgFnS	chtít
předejít	předejít	k5eAaPmF	předejít
drtivé	drtivý	k2eAgFnSc3d1	drtivá
porážce	porážka	k1gFnSc3	porážka
<g/>
,	,	kIx,	,
k	k	k7c3	k
níž	jenž	k3xRgFnSc3	jenž
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
Hitlerovým	Hitlerův	k2eAgNnSc7d1	Hitlerovo
neschopným	schopný	k2eNgNnSc7d1	neschopné
a	a	k8xC	a
fanatickým	fanatický	k2eAgNnSc7d1	fanatické
velením	velení	k1gNnSc7	velení
schylovalo	schylovat	k5eAaImAgNnS	schylovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1945	[number]	k4	1945
Hitler	Hitler	k1gMnSc1	Hitler
uvízl	uvíznout	k5eAaPmAgMnS	uvíznout
v	v	k7c6	v
obleženém	obležený	k2eAgNnSc6d1	obležené
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
a	a	k8xC	a
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
spáchal	spáchat	k5eAaPmAgMnS	spáchat
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
bunkru	bunkr	k1gInSc6	bunkr
sebevraždu	sebevražda	k1gFnSc4	sebevražda
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
družkou	družka	k1gFnSc7	družka
Evou	Eva	k1gFnSc7	Eva
Braunovou	Braunová	k1gFnSc7	Braunová
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
si	se	k3xPyFc3	se
těsně	těsně	k6eAd1	těsně
předtím	předtím	k6eAd1	předtím
vzal	vzít	k5eAaPmAgMnS	vzít
za	za	k7c4	za
manželku	manželka	k1gFnSc4	manželka
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	s	k7c7	s
20	[number]	k4	20
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1889	[number]	k4	1889
v	v	k7c6	v
městečku	městečko	k1gNnSc6	městečko
Braunau	Braunaus	k1gInSc2	Braunaus
am	am	k?	am
Inn	Inn	k1gMnSc1	Inn
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
nedaleko	nedaleko	k7c2	nedaleko
bavorských	bavorský	k2eAgFnPc2d1	bavorská
hranic	hranice	k1gFnPc2	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
Adolfa	Adolf	k1gMnSc2	Adolf
Hitlera	Hitler	k1gMnSc2	Hitler
se	se	k3xPyFc4	se
jmenovala	jmenovat	k5eAaImAgFnS	jmenovat
Klára	Klára	k1gFnSc1	Klára
Hitlerová	Hitlerová	k1gFnSc1	Hitlerová
<g/>
,	,	kIx,	,
za	za	k7c2	za
svobodna	svoboden	k2eAgFnSc1d1	svobodna
Pölzlová	Pölzlová	k1gFnSc1	Pölzlová
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
Alois	Alois	k1gMnSc1	Alois
Hitler	Hitler	k1gMnSc1	Hitler
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Schicklgruber	Schicklgrubra	k1gFnPc2	Schicklgrubra
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
jako	jako	k9	jako
nemanželské	manželský	k2eNgNnSc4d1	nemanželské
dítě	dítě	k1gNnSc4	dítě
Marie	Maria	k1gFnSc2	Maria
A.	A.	kA	A.
Schickelgruberové	Schickelgruberová	k1gFnSc2	Schickelgruberová
a	a	k8xC	a
Johanna	Johann	k1gInSc2	Johann
G.	G.	kA	G.
Hiedlera	Hiedler	k1gMnSc2	Hiedler
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1876	[number]	k4	1876
si	se	k3xPyFc3	se
ale	ale	k8xC	ale
změnil	změnit	k5eAaPmAgMnS	změnit
své	svůj	k3xOyFgNnSc4	svůj
příjmení	příjmení	k1gNnSc4	příjmení
na	na	k7c6	na
Hitler	Hitler	k1gMnSc1	Hitler
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byla	být	k5eAaImAgFnS	být
zkomolenina	zkomolenina	k1gFnSc1	zkomolenina
jména	jméno	k1gNnSc2	jméno
Hiedler	Hiedler	k1gInSc1	Hiedler
<g/>
.	.	kIx.	.
</s>
<s>
Pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
celní	celní	k2eAgMnSc1d1	celní
inspektor	inspektor	k1gMnSc1	inspektor
<g/>
,	,	kIx,	,
Klára	Klára	k1gFnSc1	Klára
Pölzlová	Pölzlová	k1gFnSc1	Pölzlová
pracovala	pracovat	k5eAaImAgFnS	pracovat
původně	původně	k6eAd1	původně
jako	jako	k8xS	jako
služka	služka	k1gFnSc1	služka
Aloisovy	Aloisův	k2eAgFnPc4d1	Aloisova
první	první	k4xOgFnPc4	první
ženy	žena	k1gFnPc4	žena
<g/>
.	.	kIx.	.
</s>
<s>
Klára	Klára	k1gFnSc1	Klára
byla	být	k5eAaImAgFnS	být
třetí	třetí	k4xOgFnSc7	třetí
ženou	žena	k1gFnSc7	žena
Aloise	Alois	k1gMnSc2	Alois
Hitlera	Hitler	k1gMnSc2	Hitler
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gFnSc7	jeho
vzdálenou	vzdálený	k2eAgFnSc7d1	vzdálená
sestřenicí	sestřenice	k1gFnSc7	sestřenice
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
si	se	k3xPyFc3	se
Hitlerovi	Hitlerův	k2eAgMnPc1d1	Hitlerův
rodiče	rodič	k1gMnPc1	rodič
museli	muset	k5eAaImAgMnP	muset
vyžádat	vyžádat	k5eAaPmF	vyžádat
pro	pro	k7c4	pro
povolení	povolení	k1gNnSc4	povolení
k	k	k7c3	k
sňatku	sňatek	k1gInSc3	sňatek
církevní	církevní	k2eAgFnSc1d1	církevní
dispens	dispens	k1gFnSc1	dispens
<g/>
.	.	kIx.	.
</s>
<s>
Hitlerův	Hitlerův	k2eAgMnSc1d1	Hitlerův
pradědeček	pradědeček	k1gMnSc1	pradědeček
z	z	k7c2	z
matčiny	matčin	k2eAgFnSc2d1	matčina
strany	strana	k1gFnSc2	strana
se	se	k3xPyFc4	se
jmenoval	jmenovat	k5eAaImAgInS	jmenovat
Johann	Johann	k1gInSc1	Johann
von	von	k1gInSc1	von
Nepomuk	Nepomuk	k1gInSc1	Nepomuk
Hüttler	Hüttler	k1gInSc1	Hüttler
<g/>
.	.	kIx.	.
</s>
<s>
Hitler	Hitler	k1gMnSc1	Hitler
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
jako	jako	k9	jako
čtvrté	čtvrtý	k4xOgNnSc4	čtvrtý
dítě	dítě	k1gNnSc4	dítě
ze	z	k7c2	z
šesti	šest	k4xCc2	šest
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
jeho	jeho	k3xOp3gMnPc1	jeho
sourozenci	sourozenec	k1gMnPc1	sourozenec
zemřeli	zemřít	k5eAaPmAgMnP	zemřít
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
dětském	dětský	k2eAgInSc6d1	dětský
věku	věk	k1gInSc6	věk
<g/>
,	,	kIx,	,
až	až	k9	až
na	na	k7c4	na
nejmladší	mladý	k2eAgFnSc4d3	nejmladší
sestru	sestra	k1gFnSc4	sestra
Paulu	Paula	k1gFnSc4	Paula
(	(	kIx(	(
<g/>
1896	[number]	k4	1896
<g/>
-	-	kIx~	-
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
Hitlera	Hitler	k1gMnSc2	Hitler
přežila	přežít	k5eAaPmAgFnS	přežít
o	o	k7c4	o
mnoho	mnoho	k4c4	mnoho
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Rodina	rodina	k1gFnSc1	rodina
penzionovaného	penzionovaný	k2eAgMnSc2d1	penzionovaný
státního	státní	k2eAgMnSc2d1	státní
úředníka	úředník	k1gMnSc2	úředník
Aloise	Alois	k1gMnSc2	Alois
Hitlera	Hitler	k1gMnSc2	Hitler
se	se	k3xPyFc4	se
často	často	k6eAd1	často
stěhovala	stěhovat	k5eAaImAgFnS	stěhovat
z	z	k7c2	z
místa	místo	k1gNnSc2	místo
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
usadila	usadit	k5eAaPmAgFnS	usadit
ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
Leonding	Leonding	k1gInSc4	Leonding
nedaleko	nedaleko	k7c2	nedaleko
Lince	Linec	k1gInSc2	Linec
<g/>
.	.	kIx.	.
</s>
<s>
Hitler	Hitler	k1gMnSc1	Hitler
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
nejméně	málo	k6eAd3	málo
pět	pět	k4xCc4	pět
základních	základní	k2eAgFnPc2d1	základní
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
školu	škola	k1gFnSc4	škola
v	v	k7c6	v
benediktýnském	benediktýnský	k2eAgInSc6d1	benediktýnský
klášteře	klášter	k1gInSc6	klášter
v	v	k7c6	v
Lanbachu	Lanbach	k1gInSc6	Lanbach
<g/>
,	,	kIx,	,
zpíval	zpívat	k5eAaImAgMnS	zpívat
ve	v	k7c6	v
sboru	sbor	k1gInSc6	sbor
a	a	k8xC	a
chtěl	chtít	k5eAaImAgMnS	chtít
se	se	k3xPyFc4	se
stát	stát	k5eAaImF	stát
knězem	kněz	k1gMnSc7	kněz
<g/>
.	.	kIx.	.
</s>
<s>
Prospěch	prospěch	k1gInSc1	prospěch
na	na	k7c6	na
základní	základní	k2eAgFnSc6d1	základní
škole	škola	k1gFnSc6	škola
měl	mít	k5eAaImAgInS	mít
vcelku	vcelku	k6eAd1	vcelku
dobrý	dobrý	k2eAgInSc1d1	dobrý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
střední	střední	k2eAgFnSc6d1	střední
škole	škola	k1gFnSc6	škola
v	v	k7c6	v
Linci	Linec	k1gInSc6	Linec
se	se	k3xPyFc4	se
značně	značně	k6eAd1	značně
zhoršil	zhoršit	k5eAaPmAgInS	zhoršit
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
střední	střední	k2eAgFnSc4d1	střední
školu	škola	k1gFnSc4	škola
nedokončil	dokončit	k5eNaPmAgMnS	dokončit
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
dějepise	dějepis	k1gInSc6	dějepis
a	a	k8xC	a
výtvarné	výtvarný	k2eAgFnSc3d1	výtvarná
výchově	výchova	k1gFnSc3	výchova
měl	mít	k5eAaImAgInS	mít
výborný	výborný	k2eAgInSc1d1	výborný
prospěch	prospěch	k1gInSc1	prospěch
<g/>
.	.	kIx.	.
</s>
<s>
Veliký	veliký	k2eAgInSc4d1	veliký
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
formování	formování	k1gNnSc4	formování
prvních	první	k4xOgFnPc2	první
Hitlerových	Hitlerová	k1gFnPc2	Hitlerová
nacionalistických	nacionalistický	k2eAgInPc2d1	nacionalistický
názorů	názor	k1gInPc2	názor
měl	mít	k5eAaImAgMnS	mít
jeho	jeho	k3xOp3gFnSc3	jeho
učitel	učitel	k1gMnSc1	učitel
dějepisu	dějepis	k1gInSc2	dějepis
Leopold	Leopold	k1gMnSc1	Leopold
Pötsch	Pötsch	k1gMnSc1	Pötsch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
upnuly	upnout	k5eAaPmAgFnP	upnout
ambice	ambice	k1gFnPc1	ambice
Adolfa	Adolf	k1gMnSc2	Adolf
Hitlera	Hitler	k1gMnSc2	Hitler
k	k	k7c3	k
výtvarnému	výtvarný	k2eAgNnSc3d1	výtvarné
umění	umění	k1gNnSc3	umění
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gMnSc1	jeho
despotický	despotický	k2eAgMnSc1d1	despotický
otec	otec	k1gMnSc1	otec
to	ten	k3xDgNnSc4	ten
nechtěl	chtít	k5eNaImAgMnS	chtít
připustit	připustit	k5eAaPmF	připustit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
plánoval	plánovat	k5eAaImAgMnS	plánovat
pro	pro	k7c4	pro
syna	syn	k1gMnSc4	syn
kariéru	kariéra	k1gFnSc4	kariéra
státního	státní	k2eAgMnSc4d1	státní
úředníka	úředník	k1gMnSc4	úředník
<g/>
.	.	kIx.	.
</s>
<s>
Mladý	mladý	k2eAgMnSc1d1	mladý
Adolf	Adolf	k1gMnSc1	Adolf
svého	své	k1gNnSc2	své
otce	otec	k1gMnSc2	otec
opravdu	opravdu	k6eAd1	opravdu
nenáviděl	návidět	k5eNaImAgMnS	návidět
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
opilec	opilec	k1gMnSc1	opilec
a	a	k8xC	a
Adolfa	Adolf	k1gMnSc2	Adolf
(	(	kIx(	(
<g/>
i	i	k9	i
jeho	jeho	k3xOp3gFnSc4	jeho
matku	matka	k1gFnSc4	matka
<g/>
)	)	kIx)	)
občas	občas	k6eAd1	občas
surově	surově	k6eAd1	surově
bil	bít	k5eAaImAgMnS	bít
a	a	k8xC	a
nadával	nadávat	k5eAaImAgMnS	nadávat
jim	on	k3xPp3gMnPc3	on
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
němu	on	k3xPp3gNnSc3	on
Hitler	Hitler	k1gMnSc1	Hitler
měl	mít	k5eAaImAgInS	mít
po	po	k7c4	po
zbytek	zbytek	k1gInSc4	zbytek
života	život	k1gInSc2	život
odpor	odpor	k1gInSc4	odpor
k	k	k7c3	k
alkoholu	alkohol	k1gInSc3	alkohol
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Alois	Alois	k1gMnSc1	Alois
Hitler	Hitler	k1gMnSc1	Hitler
však	však	k9	však
zemřel	zemřít	k5eAaPmAgMnS	zemřít
3	[number]	k4	3
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1903	[number]	k4	1903
na	na	k7c4	na
srdeční	srdeční	k2eAgInSc4d1	srdeční
infarkt	infarkt	k1gInSc4	infarkt
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
mozkovou	mozkový	k2eAgFnSc4d1	mozková
mrtvici	mrtvice	k1gFnSc4	mrtvice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
hospodě	hospodě	k?	hospodě
nad	nad	k7c7	nad
lahví	lahev	k1gFnSc7	lahev
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
matku	matka	k1gFnSc4	matka
Adolf	Adolf	k1gMnSc1	Adolf
Hitler	Hitler	k1gMnSc1	Hitler
miloval	milovat	k5eAaImAgMnS	milovat
<g/>
.	.	kIx.	.
</s>
<s>
Rozuměla	rozumět	k5eAaImAgFnS	rozumět
jeho	jeho	k3xOp3gNnSc7	jeho
uměleckým	umělecký	k2eAgNnSc7d1	umělecké
tužbám	tužba	k1gFnPc3	tužba
a	a	k8xC	a
plně	plně	k6eAd1	plně
ho	on	k3xPp3gInSc4	on
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
podporovala	podporovat	k5eAaImAgFnS	podporovat
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Hitlerův	Hitlerův	k2eAgMnSc1d1	Hitlerův
otec	otec	k1gMnSc1	otec
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
nestálo	stát	k5eNaImAgNnS	stát
již	již	k6eAd1	již
snu	sen	k1gInSc2	sen
o	o	k7c6	o
kariéře	kariéra	k1gFnSc6	kariéra
umělce	umělec	k1gMnSc2	umělec
nic	nic	k6eAd1	nic
v	v	k7c6	v
cestě	cesta	k1gFnSc6	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Finančně	finančně	k6eAd1	finančně
podporován	podporovat	k5eAaImNgInS	podporovat
matkou	matka	k1gFnSc7	matka
(	(	kIx(	(
<g/>
Hitler	Hitler	k1gMnSc1	Hitler
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nevyučil	vyučit	k5eNaPmAgMnS	vyučit
žádnému	žádný	k3yNgNnSc3	žádný
řemeslu	řemeslo	k1gNnSc3	řemeslo
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
dospívající	dospívající	k2eAgMnSc1d1	dospívající
Hitler	Hitler	k1gMnSc1	Hitler
bezcílně	bezcílně	k6eAd1	bezcílně
toulal	toulat	k5eAaImAgMnS	toulat
po	po	k7c6	po
Linci	Linec	k1gInSc6	Linec
a	a	k8xC	a
snil	snít	k5eAaImAgMnS	snít
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k8xC	jak
sám	sám	k3xTgMnSc1	sám
později	pozdě	k6eAd2	pozdě
přiznal	přiznat	k5eAaPmAgMnS	přiznat
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
období	období	k1gNnSc1	období
šťastného	šťastný	k2eAgNnSc2d1	šťastné
nicnedělání	nicnedělání	k1gNnSc2	nicnedělání
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
jeho	on	k3xPp3gInSc4	on
prospěch	prospěch	k1gInSc4	prospěch
na	na	k7c6	na
střední	střední	k2eAgFnSc6d1	střední
škole	škola	k1gFnSc6	škola
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
špatný	špatný	k2eAgInSc1d1	špatný
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
v	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
vášnivým	vášnivý	k2eAgMnSc7d1	vášnivý
čtenářem	čtenář	k1gMnSc7	čtenář
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
o	o	k7c4	o
historii	historie	k1gFnSc4	historie
a	a	k8xC	a
mytologii	mytologie	k1gFnSc4	mytologie
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
<g/>
,	,	kIx,	,
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
17	[number]	k4	17
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgMnS	vydat
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
,	,	kIx,	,
zřejmě	zřejmě	k6eAd1	zřejmě
na	na	k7c4	na
popud	popud	k1gInSc4	popud
své	svůj	k3xOyFgFnSc2	svůj
matky	matka	k1gFnSc2	matka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
také	také	k9	také
do	do	k7c2	do
tohoto	tento	k3xDgNnSc2	tento
města	město	k1gNnSc2	město
v	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
odešla	odejít	k5eAaPmAgFnS	odejít
<g/>
.	.	kIx.	.
</s>
<s>
Bydlel	bydlet	k5eAaImAgMnS	bydlet
zde	zde	k6eAd1	zde
v	v	k7c6	v
chlapecké	chlapecký	k2eAgFnSc6d1	chlapecká
ubytovně	ubytovna	k1gFnSc6	ubytovna
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc7	jeho
jediným	jediný	k2eAgMnSc7d1	jediný
přítelem	přítel	k1gMnSc7	přítel
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgMnS	být
jeho	jeho	k3xOp3gMnSc1	jeho
spolubydlící	spolubydlící	k2eAgMnSc1d1	spolubydlící
August	August	k1gMnSc1	August
Kubizek	Kubizka	k1gFnPc2	Kubizka
<g/>
,	,	kIx,	,
student	student	k1gMnSc1	student
hudby	hudba	k1gFnSc2	hudba
českého	český	k2eAgInSc2d1	český
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yRgInSc7	který
byl	být	k5eAaImAgMnS	být
Hitler	Hitler	k1gMnSc1	Hitler
v	v	k7c6	v
kontaktu	kontakt	k1gInSc6	kontakt
i	i	k8xC	i
v	v	k7c6	v
pozdějších	pozdní	k2eAgNnPc6d2	pozdější
letech	léto	k1gNnPc6	léto
a	a	k8xC	a
kterého	který	k3yIgMnSc4	který
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
"	"	kIx"	"
<g/>
jediného	jediný	k2eAgMnSc4d1	jediný
Čecha	Čech	k1gMnSc4	Čech
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
má	mít	k5eAaImIp3nS	mít
právo	právo	k1gNnSc4	právo
být	být	k5eAaImF	být
přítelem	přítel	k1gMnSc7	přítel
Vůdce	vůdce	k1gMnSc1	vůdce
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1907	[number]	k4	1907
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
pokusil	pokusit	k5eAaPmAgMnS	pokusit
dostat	dostat	k5eAaPmF	dostat
se	se	k3xPyFc4	se
na	na	k7c4	na
zdejší	zdejší	k2eAgFnSc4d1	zdejší
Akademii	akademie	k1gFnSc4	akademie
výtvarných	výtvarný	k2eAgNnPc2d1	výtvarné
umění	umění	k1gNnPc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
si	se	k3xPyFc3	se
téměř	téměř	k6eAd1	téměř
jist	jist	k2eAgMnSc1d1	jist
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
přijat	přijmout	k5eAaPmNgMnS	přijmout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
když	když	k8xS	když
mu	on	k3xPp3gInSc3	on
přijímací	přijímací	k2eAgFnPc4d1	přijímací
komise	komise	k1gFnPc4	komise
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
bez	bez	k7c2	bez
talentu	talent	k1gInSc2	talent
<g/>
,	,	kIx,	,
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
nepřijat	přijat	k2eNgMnSc1d1	nepřijat
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
se	se	k3xPyFc4	se
nervově	nervově	k6eAd1	nervově
zhroutil	zhroutit	k5eAaPmAgInS	zhroutit
<g/>
.	.	kIx.	.
</s>
<s>
Mladému	mladý	k1gMnSc3	mladý
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
ambicióznímu	ambiciózní	k2eAgNnSc3d1	ambiciózní
uchazeči	uchazeč	k1gMnPc7	uchazeč
doporučil	doporučit	k5eAaPmAgMnS	doporučit
rektor	rektor	k1gMnSc1	rektor
Sigmund	Sigmund	k1gMnSc1	Sigmund
le	le	k?	le
Allemand	allemanda	k1gFnPc2	allemanda
studium	studium	k1gNnSc4	studium
architektury	architektura	k1gFnSc2	architektura
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
Hitlerovi	Hitler	k1gMnSc3	Hitler
také	také	k9	také
zamlouvalo	zamlouvat	k5eAaImAgNnS	zamlouvat
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
chyběla	chybět	k5eAaImAgFnS	chybět
mu	on	k3xPp3gMnSc3	on
potřebná	potřebný	k2eAgFnSc1d1	potřebná
maturita	maturita	k1gFnSc1	maturita
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
také	také	k9	také
sníval	snívat	k5eAaImAgMnS	snívat
o	o	k7c6	o
kariéře	kariéra	k1gFnSc6	kariéra
hudebního	hudební	k2eAgMnSc2d1	hudební
skladatele	skladatel	k1gMnSc2	skladatel
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
byl	být	k5eAaImAgMnS	být
sám	sám	k3xTgMnSc1	sám
velkým	velký	k2eAgMnSc7d1	velký
obdivovatelem	obdivovatel	k1gMnSc7	obdivovatel
Richarda	Richard	k1gMnSc2	Richard
Wagnera	Wagner	k1gMnSc2	Wagner
<g/>
.	.	kIx.	.
</s>
<s>
Stejný	stejný	k2eAgInSc1d1	stejný
rok	rok	k1gInSc1	rok
<g/>
,	,	kIx,	,
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
zemřela	zemřít	k5eAaPmAgFnS	zemřít
Hitlerova	Hitlerův	k2eAgFnSc1d1	Hitlerova
matka	matka	k1gFnSc1	matka
Klára	Klára	k1gFnSc1	Klára
na	na	k7c4	na
rakovinu	rakovina	k1gFnSc4	rakovina
prsu	prs	k1gInSc2	prs
<g/>
.	.	kIx.	.
</s>
<s>
Hitler	Hitler	k1gMnSc1	Hitler
byl	být	k5eAaImAgMnS	být
smrtí	smrt	k1gFnSc7	smrt
své	svůj	k3xOyFgFnPc4	svůj
milované	milovaný	k2eAgFnPc4d1	milovaná
matky	matka	k1gFnPc4	matka
velmi	velmi	k6eAd1	velmi
zdrcen	zdrcen	k2eAgMnSc1d1	zdrcen
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
této	tento	k3xDgFnSc6	tento
tragické	tragický	k2eAgFnSc6d1	tragická
události	událost	k1gFnSc6	událost
se	se	k3xPyFc4	se
nadobro	nadobro	k6eAd1	nadobro
odstěhoval	odstěhovat	k5eAaPmAgMnS	odstěhovat
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
se	se	k3xPyFc4	se
podruhé	podruhé	k6eAd1	podruhé
pokusil	pokusit	k5eAaPmAgMnS	pokusit
dostat	dostat	k5eAaPmF	dostat
se	se	k3xPyFc4	se
na	na	k7c6	na
Akademii	akademie	k1gFnSc6	akademie
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
ho	on	k3xPp3gNnSc4	on
ale	ale	k8xC	ale
ani	ani	k8xC	ani
nepustili	pustit	k5eNaPmAgMnP	pustit
k	k	k7c3	k
přijímacím	přijímací	k2eAgFnPc3d1	přijímací
zkouškám	zkouška	k1gFnPc3	zkouška
<g/>
.	.	kIx.	.
</s>
<s>
Hitler	Hitler	k1gMnSc1	Hitler
byl	být	k5eAaImAgMnS	být
tím	ten	k3xDgNnSc7	ten
natolik	natolik	k6eAd1	natolik
zklamán	zklamat	k5eAaPmNgMnS	zklamat
<g/>
,	,	kIx,	,
že	že	k8xS	že
opustil	opustit	k5eAaPmAgMnS	opustit
i	i	k9	i
těch	ten	k3xDgMnPc2	ten
několik	několik	k4yIc1	několik
přátel	přítel	k1gMnPc2	přítel
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
měl	mít	k5eAaImAgMnS	mít
<g/>
,	,	kIx,	,
a	a	k8xC	a
odstěhoval	odstěhovat	k5eAaPmAgMnS	odstěhovat
se	se	k3xPyFc4	se
z	z	k7c2	z
ubytovny	ubytovna	k1gFnSc2	ubytovna
<g/>
.	.	kIx.	.
</s>
<s>
Nějaký	nějaký	k3yIgInSc4	nějaký
čas	čas	k1gInSc4	čas
bydlel	bydlet	k5eAaImAgMnS	bydlet
v	v	k7c6	v
pronajatém	pronajatý	k2eAgInSc6d1	pronajatý
bytě	byt	k1gInSc6	byt
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
příštího	příští	k2eAgInSc2d1	příští
roku	rok	k1gInSc2	rok
mu	on	k3xPp3gInSc3	on
došly	dojít	k5eAaPmAgInP	dojít
peníze	peníz	k1gInPc4	peníz
a	a	k8xC	a
skončil	skončit	k5eAaPmAgMnS	skončit
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
mezi	mezi	k7c7	mezi
bezdomovci	bezdomovec	k1gMnPc7	bezdomovec
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
se	se	k3xPyFc4	se
živil	živit	k5eAaImAgMnS	živit
Hitler	Hitler	k1gMnSc1	Hitler
pouze	pouze	k6eAd1	pouze
příležitostnými	příležitostný	k2eAgFnPc7d1	příležitostná
pracemi	práce	k1gFnPc7	práce
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
maloval	malovat	k5eAaImAgMnS	malovat
reklamní	reklamní	k2eAgInPc4d1	reklamní
plakáty	plakát	k1gInPc4	plakát
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ulicích	ulice	k1gFnPc6	ulice
prodával	prodávat	k5eAaImAgMnS	prodávat
obkreslené	obkreslený	k2eAgFnPc4d1	obkreslená
kýčovité	kýčovitý	k2eAgFnPc4d1	kýčovitá
pohlednice	pohlednice	k1gFnPc4	pohlednice
<g/>
.	.	kIx.	.
</s>
<s>
Bydlel	bydlet	k5eAaImAgMnS	bydlet
v	v	k7c6	v
ubytovnách	ubytovna	k1gFnPc6	ubytovna
pro	pro	k7c4	pro
chudé	chudý	k2eAgFnPc4d1	chudá
nebo	nebo	k8xC	nebo
spal	spát	k5eAaImAgMnS	spát
s	s	k7c7	s
bezdomovci	bezdomovec	k1gMnPc7	bezdomovec
na	na	k7c6	na
nádražích	nádraží	k1gNnPc6	nádraží
<g/>
,	,	kIx,	,
na	na	k7c6	na
lavičkách	lavička	k1gFnPc6	lavička
a	a	k8xC	a
v	v	k7c6	v
průjezdech	průjezd	k1gInPc6	průjezd
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
živořil	živořit	k5eAaImAgInS	živořit
skoro	skoro	k6eAd1	skoro
5	[number]	k4	5
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
v	v	k7c4	v
tuto	tento	k3xDgFnSc4	tento
dobu	doba	k1gFnSc4	doba
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
začal	začít	k5eAaPmAgInS	začít
zrát	zrát	k5eAaImF	zrát
antisemitismus	antisemitismus	k1gInSc1	antisemitismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1913	[number]	k4	1913
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
opustit	opustit	k5eAaPmF	opustit
Vídeň	Vídeň	k1gFnSc4	Vídeň
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gNnPc2	jeho
slov	slovo	k1gNnPc2	slovo
zkažené	zkažený	k2eAgNnSc1d1	zkažené
město	město	k1gNnSc1	město
plné	plný	k2eAgFnSc2d1	plná
marxistů	marxista	k1gMnPc2	marxista
a	a	k8xC	a
židů	žid	k1gMnPc2	žid
<g/>
,	,	kIx,	,
a	a	k8xC	a
přestěhovat	přestěhovat	k5eAaPmF	přestěhovat
se	se	k3xPyFc4	se
do	do	k7c2	do
bavorského	bavorský	k2eAgInSc2d1	bavorský
Mnichova	Mnichov	k1gInSc2	Mnichov
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
už	už	k6eAd1	už
se	se	k3xPyFc4	se
nechtěl	chtít	k5eNaImAgMnS	chtít
nikdy	nikdy	k6eAd1	nikdy
v	v	k7c6	v
životě	život	k1gInSc6	život
vrátit	vrátit	k5eAaPmF	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
svůj	svůj	k3xOyFgInSc4	svůj
odchod	odchod	k1gInSc4	odchod
argumentoval	argumentovat	k5eAaImAgMnS	argumentovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
chce	chtít	k5eAaImIp3nS	chtít
vyhnout	vyhnout	k5eAaPmF	vyhnout
službě	služba	k1gFnSc3	služba
v	v	k7c6	v
rakouské	rakouský	k2eAgFnSc6d1	rakouská
armádě	armáda	k1gFnSc6	armáda
a	a	k8xC	a
zkusit	zkusit	k5eAaPmF	zkusit
znovu	znovu	k6eAd1	znovu
se	se	k3xPyFc4	se
dostat	dostat	k5eAaPmF	dostat
na	na	k7c4	na
nějakou	nějaký	k3yIgFnSc4	nějaký
školu	škola	k1gFnSc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
první	první	k4xOgFnSc2	první
chvíle	chvíle	k1gFnSc2	chvíle
byl	být	k5eAaImAgInS	být
Mnichovem	Mnichov	k1gInSc7	Mnichov
nadšen	nadchnout	k5eAaPmNgInS	nadchnout
<g/>
.	.	kIx.	.
</s>
<s>
Studovat	studovat	k5eAaImF	studovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
ale	ale	k9	ale
ani	ani	k9	ani
nepokusil	pokusit	k5eNaPmAgMnS	pokusit
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k8xC	a
ani	ani	k8xC	ani
povolávacímu	povolávací	k2eAgInSc3d1	povolávací
rozkazu	rozkaz	k1gInSc3	rozkaz
se	se	k3xPyFc4	se
nevyhnul	vyhnout	k5eNaPmAgMnS	vyhnout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
u	u	k7c2	u
komise	komise	k1gFnSc2	komise
byl	být	k5eAaImAgMnS	být
shledán	shledat	k5eAaPmNgMnS	shledat
služby	služba	k1gFnPc4	služba
neschopným	schopný	k2eNgMnSc7d1	neschopný
ze	z	k7c2	z
zdravotních	zdravotní	k2eAgMnPc2d1	zdravotní
důvodů	důvod	k1gInPc2	důvod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
chudinská	chudinský	k2eAgFnSc1d1	chudinská
anabáze	anabáze	k1gFnSc1	anabáze
opakovala	opakovat	k5eAaImAgFnS	opakovat
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1914	[number]	k4	1914
nevypukla	vypuknout	k5eNaPmAgFnS	vypuknout
první	první	k4xOgFnSc1	první
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
uznán	uznat	k5eAaPmNgInS	uznat
neschopným	schopný	k2eNgFnPc3d1	neschopná
pro	pro	k7c4	pro
službu	služba	k1gFnSc4	služba
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
<g/>
,	,	kIx,	,
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
se	se	k3xPyFc4	se
pln	pln	k2eAgInSc1d1	pln
"	"	kIx"	"
<g/>
vlasteneckého	vlastenecký	k2eAgNnSc2d1	vlastenecké
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
německého	německý	k2eAgMnSc2d1	německý
<g/>
)	)	kIx)	)
nadšení	nadšení	k1gNnSc2	nadšení
přihlásil	přihlásit	k5eAaPmAgMnS	přihlásit
jako	jako	k9	jako
dobrovolník	dobrovolník	k1gMnSc1	dobrovolník
do	do	k7c2	do
16	[number]	k4	16
<g/>
.	.	kIx.	.
pěšího	pěší	k2eAgInSc2d1	pěší
záložního	záložní	k2eAgInSc2d1	záložní
pluku	pluk	k1gInSc2	pluk
u	u	k7c2	u
bavorské	bavorský	k2eAgFnSc2d1	bavorská
armády	armáda	k1gFnSc2	armáda
(	(	kIx(	(
<g/>
Bayerische	Bayerische	k1gFnSc1	Bayerische
Reserve-Infanterie-Regiment	Reserve-Infanterie-Regiment	k1gMnSc1	Reserve-Infanterie-Regiment
Nr	Nr	k1gMnSc1	Nr
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
dostal	dostat	k5eAaPmAgMnS	dostat
důležitý	důležitý	k2eAgInSc4d1	důležitý
úkol	úkol	k1gInSc4	úkol
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgInS	stát
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gMnSc4	on
polní	polní	k2eAgMnSc1d1	polní
kurýr	kurýr	k1gMnSc1	kurýr
-	-	kIx~	-
doručoval	doručovat	k5eAaImAgMnS	doručovat
po	po	k7c6	po
zákopech	zákop	k1gInPc6	zákop
rozkazy	rozkaz	k1gInPc4	rozkaz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
obdržel	obdržet	k5eAaPmAgInS	obdržet
Železný	železný	k2eAgInSc1d1	železný
kříž	kříž	k1gInSc1	kříž
II	II	kA	II
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
a	a	k8xC	a
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
Železný	železný	k2eAgInSc1d1	železný
kříž	kříž	k1gInSc1	kříž
I.	I.	kA	I.
třídy	třída	k1gFnSc2	třída
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
si	se	k3xPyFc3	se
po	po	k7c4	po
zbytek	zbytek	k1gInSc4	zbytek
života	život	k1gInSc2	život
velice	velice	k6eAd1	velice
cenil	cenit	k5eAaImAgMnS	cenit
<g/>
,	,	kIx,	,
nosil	nosit	k5eAaImAgMnS	nosit
ho	on	k3xPp3gInSc4	on
stále	stále	k6eAd1	stále
u	u	k7c2	u
sebe	sebe	k3xPyFc4	sebe
a	a	k8xC	a
dával	dávat	k5eAaImAgMnS	dávat
ho	on	k3xPp3gMnSc4	on
na	na	k7c4	na
odiv	odiv	k1gInSc4	odiv
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
kariéru	kariéra	k1gFnSc4	kariéra
ale	ale	k8xC	ale
neudělal	udělat	k5eNaPmAgMnS	udělat
<g/>
,	,	kIx,	,
na	na	k7c6	na
konci	konec	k1gInSc6	konec
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgMnS	být
pouhý	pouhý	k2eAgMnSc1d1	pouhý
desátník	desátník	k1gMnSc1	desátník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1916	[number]	k4	1916
zasáhl	zasáhnout	k5eAaPmAgInS	zasáhnout
zákop	zákop	k1gInSc1	zákop
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
Hitler	Hitler	k1gMnSc1	Hitler
nalézal	nalézat	k5eAaImAgMnS	nalézat
<g/>
,	,	kIx,	,
granát	granát	k1gInSc4	granát
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
všichni	všechen	k3xTgMnPc1	všechen
muži	muž	k1gMnPc1	muž
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
zahynuli	zahynout	k5eAaPmAgMnP	zahynout
<g/>
.	.	kIx.	.
</s>
<s>
Hitler	Hitler	k1gMnSc1	Hitler
ale	ale	k9	ale
přežil	přežít	k5eAaPmAgMnS	přežít
se	s	k7c7	s
zraněním	zranění	k1gNnSc7	zranění
lebky	lebka	k1gFnSc2	lebka
<g/>
.	.	kIx.	.
</s>
<s>
Následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
byl	být	k5eAaImAgMnS	být
opět	opět	k6eAd1	opět
poslán	poslat	k5eAaPmNgMnS	poslat
na	na	k7c4	na
frontu	fronta	k1gFnSc4	fronta
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
popisoval	popisovat	k5eAaImAgMnS	popisovat
období	období	k1gNnSc4	období
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
trávil	trávit	k5eAaImAgMnS	trávit
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
nejkrásnější	krásný	k2eAgNnSc1d3	nejkrásnější
období	období	k1gNnSc1	období
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Líbilo	líbit	k5eAaImAgNnS	líbit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
hrdě	hrdě	k6eAd1	hrdě
bojovat	bojovat	k5eAaImF	bojovat
za	za	k7c4	za
velký	velký	k2eAgInSc4d1	velký
německý	německý	k2eAgInSc4d1	německý
národ	národ	k1gInSc4	národ
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jeho	jeho	k3xOp3gMnPc2	jeho
kolegů	kolega	k1gMnPc2	kolega
z	z	k7c2	z
vojny	vojna	k1gFnSc2	vojna
prý	prý	k9	prý
i	i	k9	i
odmítal	odmítat	k5eAaImAgMnS	odmítat
dovolenou	dovolená	k1gFnSc4	dovolená
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
byl	být	k5eAaImAgInS	být
odvezen	odvézt	k5eAaPmNgMnS	odvézt
po	po	k7c6	po
otravě	otrava	k1gFnSc6	otrava
plynem	plyn	k1gInSc7	plyn
(	(	kIx(	(
<g/>
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
belgického	belgický	k2eAgNnSc2d1	Belgické
města	město	k1gNnSc2	město
Ypry	Ypry	k1gInPc1	Ypry
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
po	po	k7c6	po
které	který	k3yIgFnSc6	který
dočasně	dočasně	k6eAd1	dočasně
oslepl	oslepnout	k5eAaPmAgMnS	oslepnout
<g/>
,	,	kIx,	,
do	do	k7c2	do
lazaretu	lazaret	k1gInSc2	lazaret
v	v	k7c6	v
Pomořanech	Pomořany	k1gInPc6	Pomořany
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jej	on	k3xPp3gMnSc4	on
ošetřoval	ošetřovat	k5eAaImAgMnS	ošetřovat
mj.	mj.	kA	mj.
i	i	k8xC	i
psychiatr	psychiatr	k1gMnSc1	psychiatr
Edmund	Edmund	k1gMnSc1	Edmund
Forster	Forster	k1gMnSc1	Forster
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
o	o	k7c4	o
revoluci	revoluce	k1gFnSc4	revoluce
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
9	[number]	k4	9
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
abdikaci	abdikace	k1gFnSc3	abdikace
císaře	císař	k1gMnSc2	císař
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tomto	tento	k3xDgNnSc6	tento
zjištění	zjištění	k1gNnSc6	zjištění
se	se	k3xPyFc4	se
prý	prý	k9	prý
naprosto	naprosto	k6eAd1	naprosto
zhroutil	zhroutit	k5eAaPmAgInS	zhroutit
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
pocit	pocit	k1gInSc4	pocit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pád	pád	k1gInSc1	pád
Německa	Německo	k1gNnSc2	Německo
nelze	lze	k6eNd1	lze
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
obvyklými	obvyklý	k2eAgFnPc7d1	obvyklá
metodami	metoda	k1gFnPc7	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
když	když	k8xS	když
býval	bývat	k5eAaImAgInS	bývat
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
,	,	kIx,	,
seznámil	seznámit	k5eAaPmAgMnS	seznámit
se	se	k3xPyFc4	se
s	s	k7c7	s
myšlenkami	myšlenka	k1gFnPc7	myšlenka
německých	německý	k2eAgMnPc2d1	německý
nacionalistických	nacionalistický	k2eAgMnPc2d1	nacionalistický
extremistů	extremista	k1gMnPc2	extremista
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
obraceli	obracet	k5eAaImAgMnP	obracet
k	k	k7c3	k
tradiční	tradiční	k2eAgFnSc3d1	tradiční
myšlence	myšlenka	k1gFnSc3	myšlenka
o	o	k7c6	o
vykořisťovatelích	vykořisťovatel	k1gMnPc6	vykořisťovatel
a	a	k8xC	a
ničitelích	ničitel	k1gMnPc6	ničitel
nordického	nordický	k2eAgInSc2d1	nordický
světa	svět	k1gInSc2	svět
-	-	kIx~	-
Židech	Žid	k1gMnPc6	Žid
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
chvíli	chvíle	k1gFnSc6	chvíle
si	se	k3xPyFc3	se
prý	prý	k9	prý
umínil	umínit	k5eAaPmAgInS	umínit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
dostane	dostat	k5eAaPmIp3nS	dostat
do	do	k7c2	do
politiky	politika	k1gFnSc2	politika
a	a	k8xC	a
skoncuje	skoncovat	k5eAaPmIp3nS	skoncovat
s	s	k7c7	s
těmito	tento	k3xDgFnPc7	tento
"	"	kIx"	"
<g/>
zrádci	zrádce	k1gMnPc1	zrádce
národa	národ	k1gInSc2	národ
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
18	[number]	k4	18
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1919	[number]	k4	1919
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
Pařížská	pařížský	k2eAgFnSc1d1	Pařížská
mírová	mírový	k2eAgFnSc1d1	mírová
konference	konference	k1gFnSc1	konference
ve	v	k7c6	v
Versailles	Versailles	k1gFnSc6	Versailles
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
podepsat	podepsat	k5eAaPmF	podepsat
mírové	mírový	k2eAgFnPc4d1	mírová
smlouvy	smlouva	k1gFnPc4	smlouva
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
podmínky	podmínka	k1gFnPc1	podmínka
diktovaly	diktovat	k5eAaImAgFnP	diktovat
především	především	k6eAd1	především
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
Itálie	Itálie	k1gFnSc1	Itálie
a	a	k8xC	a
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k6eAd1	mimo
jiné	jiný	k2eAgNnSc1d1	jiné
muselo	muset	k5eAaImAgNnS	muset
Německo	Německo	k1gNnSc1	Německo
odstoupit	odstoupit	k5eAaPmF	odstoupit
přibližně	přibližně	k6eAd1	přibližně
13	[number]	k4	13
%	%	kIx~	%
říšského	říšský	k2eAgNnSc2d1	říšské
území	území	k1gNnSc2	území
<g/>
,	,	kIx,	,
omezit	omezit	k5eAaPmF	omezit
válečnou	válečný	k2eAgFnSc4d1	válečná
flotilu	flotila	k1gFnSc4	flotila
<g/>
,	,	kIx,	,
omezit	omezit	k5eAaPmF	omezit
ozbrojené	ozbrojený	k2eAgFnPc4d1	ozbrojená
síly	síla	k1gFnPc4	síla
na	na	k7c4	na
100	[number]	k4	100
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
platit	platit	k5eAaImF	platit
velmi	velmi	k6eAd1	velmi
vysoké	vysoký	k2eAgFnPc4d1	vysoká
finanční	finanční	k2eAgFnPc4d1	finanční
reparace	reparace	k1gFnPc4	reparace
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
činily	činit	k5eAaImAgFnP	činit
33	[number]	k4	33
000	[number]	k4	000
000	[number]	k4	000
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
$	$	kIx~	$
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
informátor	informátor	k1gMnSc1	informátor
vyšetřovací	vyšetřovací	k2eAgFnSc2d1	vyšetřovací
komise	komise	k1gFnSc2	komise
druhého	druhý	k4xOgInSc2	druhý
pěšího	pěší	k2eAgInSc2d1	pěší
pluku	pluk	k1gInSc2	pluk
<g/>
,	,	kIx,	,
za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
píli	píle	k1gFnSc4	píle
byl	být	k5eAaImAgInS	být
odměněn	odměnit	k5eAaPmNgInS	odměnit
jmenováním	jmenování	k1gNnSc7	jmenování
do	do	k7c2	do
tiskové	tiskový	k2eAgFnSc2d1	tisková
a	a	k8xC	a
informační	informační	k2eAgFnSc2d1	informační
kanceláře	kancelář	k1gFnSc2	kancelář
politického	politický	k2eAgNnSc2d1	politické
oddělení	oddělení	k1gNnSc2	oddělení
oblastního	oblastní	k2eAgNnSc2d1	oblastní
velitelství	velitelství	k1gNnSc2	velitelství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
byl	být	k5eAaImAgMnS	být
Hitler	Hitler	k1gMnSc1	Hitler
důvěrníkem	důvěrník	k1gMnSc7	důvěrník
roty	rota	k1gFnSc2	rota
<g/>
.	.	kIx.	.
</s>
<s>
Důvěrníci	důvěrník	k1gMnPc1	důvěrník
měli	mít	k5eAaImAgMnP	mít
na	na	k7c4	na
starost	starost	k1gFnSc4	starost
spolupráci	spolupráce	k1gFnSc3	spolupráce
s	s	k7c7	s
propagandistickým	propagandistický	k2eAgNnSc7d1	propagandistické
oddělením	oddělení	k1gNnSc7	oddělení
socialistické	socialistický	k2eAgFnSc2d1	socialistická
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
zajištěn	zajistit	k5eAaPmNgInS	zajistit
osvětový	osvětový	k2eAgInSc1d1	osvětový
materiál	materiál	k1gInSc1	materiál
pro	pro	k7c4	pro
vojáky	voják	k1gMnPc4	voják
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Hitler	Hitler	k1gMnSc1	Hitler
své	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgInPc4	první
politické	politický	k2eAgInPc4d1	politický
úkoly	úkol	k1gInPc4	úkol
tedy	tedy	k9	tedy
plnil	plnit	k5eAaImAgInS	plnit
ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
revolučnímu	revoluční	k2eAgInSc3d1	revoluční
režimu	režim	k1gInSc3	režim
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
SPD	SPD	kA	SPD
(	(	kIx(	(
<g/>
Sociálnědemokratická	sociálnědemokratický	k2eAgFnSc1d1	sociálnědemokratická
strana	strana	k1gFnSc1	strana
Německa	Německo	k1gNnSc2	Německo
<g/>
)	)	kIx)	)
a	a	k8xC	a
USPD	USPD	kA	USPD
(	(	kIx(	(
<g/>
Nezávislá	závislý	k2eNgFnSc1d1	nezávislá
sociálně	sociálně	k6eAd1	sociálně
demokratická	demokratický	k2eAgFnSc1d1	demokratická
strana	strana	k1gFnSc1	strana
Německa	Německo	k1gNnSc2	Německo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1919	[number]	k4	1919
byl	být	k5eAaImAgMnS	být
Hitler	Hitler	k1gMnSc1	Hitler
zvolen	zvolit	k5eAaPmNgMnS	zvolit
do	do	k7c2	do
rady	rada	k1gFnSc2	rada
záložního	záložní	k2eAgInSc2d1	záložní
praporu	prapor	k1gInSc2	prapor
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
A	a	k9	a
tak	tak	k9	tak
Hitler	Hitler	k1gMnSc1	Hitler
nejen	nejen	k6eAd1	nejen
nijak	nijak	k6eAd1	nijak
nepřispěl	přispět	k5eNaPmAgMnS	přispět
k	k	k7c3	k
rozdrcení	rozdrcení	k1gNnSc3	rozdrcení
mnichovské	mnichovský	k2eAgFnSc2d1	Mnichovská
"	"	kIx"	"
<g/>
rudé	rudý	k2eAgFnSc2d1	rudá
republiky	republika	k1gFnSc2	republika
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
byl	být	k5eAaImAgInS	být
dokonce	dokonce	k9	dokonce
voleným	volený	k2eAgMnSc7d1	volený
představitelem	představitel	k1gMnSc7	představitel
praporu	prapor	k1gInSc2	prapor
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
jejího	její	k3xOp3gNnSc2	její
trvání	trvání	k1gNnSc2	trvání
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jednom	jeden	k4xCgNnSc6	jeden
politickém	politický	k2eAgNnSc6d1	politické
školení	školení	k1gNnSc6	školení
Hitler	Hitler	k1gMnSc1	Hitler
zasáhl	zasáhnout	k5eAaPmAgMnS	zasáhnout
do	do	k7c2	do
diskuze	diskuze	k1gFnSc2	diskuze
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
někdo	někdo	k3yInSc1	někdo
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
pozitivně	pozitivně	k6eAd1	pozitivně
o	o	k7c6	o
Židech	Žid	k1gMnPc6	Žid
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
antisemitský	antisemitský	k2eAgInSc1d1	antisemitský
výstup	výstup	k1gInSc1	výstup
zaujal	zaujmout	k5eAaPmAgInS	zaujmout
nadřízené	nadřízený	k1gMnPc4	nadřízený
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
osvětovým	osvětový	k2eAgMnSc7d1	osvětový
důstojníkem	důstojník	k1gMnSc7	důstojník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
funkci	funkce	k1gFnSc6	funkce
mohl	moct	k5eAaImAgInS	moct
poprvé	poprvé	k6eAd1	poprvé
vyzkoušet	vyzkoušet	k5eAaPmF	vyzkoušet
své	svůj	k3xOyFgFnPc4	svůj
řečnické	řečnický	k2eAgFnPc4d1	řečnická
schopnosti	schopnost	k1gFnPc4	schopnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1919	[number]	k4	1919
dostal	dostat	k5eAaPmAgInS	dostat
rozkaz	rozkaz	k1gInSc1	rozkaz
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
prošetřil	prošetřit	k5eAaPmAgMnS	prošetřit
činnost	činnost	k1gFnSc1	činnost
malé	malý	k2eAgFnPc4d1	malá
levicové	levicový	k2eAgFnPc4d1	levicová
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc4	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Německé	německý	k2eAgFnSc2d1	německá
dělnické	dělnický	k2eAgFnSc2d1	Dělnická
strany	strana	k1gFnSc2	strana
(	(	kIx(	(
<g/>
Deutsche	Deutsche	k1gInSc1	Deutsche
Arbeiterpartei	Arbeiterparte	k1gFnSc2	Arbeiterparte
<g/>
,	,	kIx,	,
DAP	DAP	kA	DAP
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
neměla	mít	k5eNaImAgFnS	mít
strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
založil	založit	k5eAaPmAgMnS	založit
Anton	Anton	k1gMnSc1	Anton
Drexler	Drexler	k1gMnSc1	Drexler
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
než	než	k8xS	než
50	[number]	k4	50
členů	člen	k1gMnPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
Hitler	Hitler	k1gMnSc1	Hitler
se	se	k3xPyFc4	se
večer	večer	k6eAd1	večer
12	[number]	k4	12
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
při	při	k7c6	při
proslovu	proslov	k1gInSc6	proslov
jednoho	jeden	k4xCgMnSc2	jeden
řečníka	řečník	k1gMnSc2	řečník
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
odpojení	odpojení	k1gNnSc4	odpojení
Bavorska	Bavorsko	k1gNnSc2	Bavorsko
od	od	k7c2	od
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
vymrštil	vymrštit	k5eAaPmAgInS	vymrštit
a	a	k8xC	a
pronesl	pronést	k5eAaPmAgInS	pronést
tak	tak	k6eAd1	tak
ohnivý	ohnivý	k2eAgInSc1d1	ohnivý
proslov	proslov	k1gInSc1	proslov
<g/>
,	,	kIx,	,
že	že	k8xS	že
okamžitě	okamžitě	k6eAd1	okamžitě
zaujal	zaujmout	k5eAaPmAgMnS	zaujmout
všechny	všechen	k3xTgInPc4	všechen
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k8xS	tak
mu	on	k3xPp3gMnSc3	on
Drexler	Drexler	k1gInSc4	Drexler
na	na	k7c6	na
odchodu	odchod	k1gInSc6	odchod
nabídl	nabídnout	k5eAaPmAgInS	nabídnout
členství	členství	k1gNnSc4	členství
ve	v	k7c6	v
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
Hitler	Hitler	k1gMnSc1	Hitler
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
změnil	změnit	k5eAaPmAgInS	změnit
její	její	k3xOp3gInSc4	její
klubový	klubový	k2eAgInSc4d1	klubový
charakter	charakter	k1gInSc4	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
organizovat	organizovat	k5eAaBmF	organizovat
shromáždění	shromáždění	k1gNnSc4	shromáždění
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgInPc2	jenž
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
jako	jako	k9	jako
řečník	řečník	k1gMnSc1	řečník
a	a	k8xC	a
sám	sám	k3xTgMnSc1	sám
v	v	k7c6	v
ulicích	ulice	k1gFnPc6	ulice
rozdával	rozdávat	k5eAaImAgMnS	rozdávat
letáky	leták	k1gInPc4	leták
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dalším	další	k2eAgNnSc6d1	další
shromáždění	shromáždění	k1gNnSc6	shromáždění
DAP	DAP	kA	DAP
29	[number]	k4	29
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1919	[number]	k4	1919
Hitler	Hitler	k1gMnSc1	Hitler
vysvětlil	vysvětlit	k5eAaPmAgMnS	vysvětlit
<g/>
,	,	kIx,	,
že	že	k8xS	že
řešení	řešení	k1gNnSc4	řešení
pro	pro	k7c4	pro
Německo	Německo	k1gNnSc4	Německo
nepřijde	přijít	k5eNaPmIp3nS	přijít
z	z	k7c2	z
parlamentu	parlament	k1gInSc2	parlament
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
z	z	k7c2	z
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Německo	Německo	k1gNnSc1	Německo
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
obnoveno	obnovit	k5eAaPmNgNnS	obnovit
pouze	pouze	k6eAd1	pouze
radikálními	radikální	k2eAgFnPc7d1	radikální
změnami	změna	k1gFnPc7	změna
<g/>
,	,	kIx,	,
ho	on	k3xPp3gInSc4	on
podporovala	podporovat	k5eAaImAgFnS	podporovat
i	i	k8xC	i
bavorská	bavorský	k2eAgFnSc1d1	bavorská
zemská	zemský	k2eAgFnSc1d1	zemská
politika	politika	k1gFnSc1	politika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
si	se	k3xPyFc3	se
vzal	vzít	k5eAaPmAgMnS	vzít
na	na	k7c4	na
starost	starost	k1gFnSc4	starost
stranickou	stranický	k2eAgFnSc4d1	stranická
propagandu	propaganda	k1gFnSc4	propaganda
a	a	k8xC	a
24	[number]	k4	24
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1920	[number]	k4	1920
ve	v	k7c6	v
Festsaalu	Festsaal	k1gInSc6	Festsaal
slavného	slavný	k2eAgInSc2d1	slavný
Hofbräuhausu	Hofbräuhaus	k1gInSc2	Hofbräuhaus
pronesl	pronést	k5eAaPmAgMnS	pronést
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
proslovu	proslov	k1gInSc6	proslov
"	"	kIx"	"
<g/>
neměnných	měnný	k2eNgMnPc2d1	měnný
<g/>
"	"	kIx"	"
25	[number]	k4	25
bodů	bod	k1gInPc2	bod
programu	program	k1gInSc2	program
DAP	DAP	kA	DAP
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgNnSc6	který
formuloval	formulovat	k5eAaImAgMnS	formulovat
poprvé	poprvé	k6eAd1	poprvé
veřejně	veřejně	k6eAd1	veřejně
antisemitská	antisemitský	k2eAgNnPc1d1	antisemitské
opatření	opatření	k1gNnPc1	opatření
<g/>
,	,	kIx,	,
vytvoření	vytvoření	k1gNnSc1	vytvoření
Velkého	velký	k2eAgNnSc2d1	velké
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
silnou	silný	k2eAgFnSc4d1	silná
centrální	centrální	k2eAgFnSc4d1	centrální
vládu	vláda	k1gFnSc4	vláda
<g/>
,	,	kIx,	,
odmítnutí	odmítnutí	k1gNnSc4	odmítnutí
Versailleské	versailleský	k2eAgFnSc2d1	Versailleská
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
,	,	kIx,	,
zestátnění	zestátnění	k1gNnSc4	zestátnění
velkých	velký	k2eAgInPc2d1	velký
kartelů	kartel	k1gInPc2	kartel
a	a	k8xC	a
trest	trest	k1gInSc4	trest
smrti	smrt	k1gFnSc2	smrt
pro	pro	k7c4	pro
lichváře	lichvář	k1gMnPc4	lichvář
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
salzburském	salzburský	k2eAgNnSc6d1	Salzburské
zasedání	zasedání	k1gNnSc6	zasedání
oficiálně	oficiálně	k6eAd1	oficiálně
přijato	přijmout	k5eAaPmNgNnS	přijmout
jako	jako	k8xS	jako
znak	znak	k1gInSc1	znak
NSDAP	NSDAP	kA	NSDAP
prastaré	prastarý	k2eAgNnSc1d1	prastaré
kultovní	kultovní	k2eAgNnSc1d1	kultovní
znamení	znamení	k1gNnSc1	znamení
<g/>
,	,	kIx,	,
symbol	symbol	k1gInSc1	symbol
hákového	hákový	k2eAgInSc2d1	hákový
kříže	kříž	k1gInSc2	kříž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
se	se	k3xPyFc4	se
pokusilo	pokusit	k5eAaPmAgNnS	pokusit
vedení	vedení	k1gNnSc1	vedení
strany	strana	k1gFnSc2	strana
Hitlera	Hitler	k1gMnSc2	Hitler
zbavit	zbavit	k5eAaPmF	zbavit
zveřejněním	zveřejnění	k1gNnSc7	zveřejnění
pamfletu	pamflet	k1gInSc2	pamflet
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
mluvilo	mluvit	k5eAaImAgNnS	mluvit
o	o	k7c6	o
Hitlerově	Hitlerův	k2eAgFnSc6d1	Hitlerova
touze	touha	k1gFnSc6	touha
po	po	k7c6	po
moci	moc	k1gFnSc6	moc
a	a	k8xC	a
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
Žid	Žid	k1gMnSc1	Žid
<g/>
.	.	kIx.	.
</s>
<s>
Jenomže	jenomže	k8xC	jenomže
to	ten	k3xDgNnSc4	ten
už	už	k6eAd1	už
byl	být	k5eAaImAgMnS	být
Hitler	Hitler	k1gMnSc1	Hitler
pro	pro	k7c4	pro
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
již	již	k6eAd1	již
jmenovala	jmenovat	k5eAaBmAgFnS	jmenovat
NSDAP	NSDAP	kA	NSDAP
<g/>
,	,	kIx,	,
příliš	příliš	k6eAd1	příliš
cenným	cenný	k2eAgNnSc7d1	cenné
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
tento	tento	k3xDgInSc1	tento
"	"	kIx"	"
<g/>
palácový	palácový	k2eAgInSc1d1	palácový
převrat	převrat	k1gInSc1	převrat
<g/>
"	"	kIx"	"
skončil	skončit	k5eAaPmAgInS	skončit
nezdarem	nezdar	k1gInSc7	nezdar
<g/>
.	.	kIx.	.
</s>
<s>
Hitler	Hitler	k1gMnSc1	Hitler
požadoval	požadovat	k5eAaImAgMnS	požadovat
vedení	vedení	k1gNnSc4	vedení
strany	strana	k1gFnSc2	strana
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Strana	strana	k1gFnSc1	strana
si	se	k3xPyFc3	se
uvědomila	uvědomit	k5eAaPmAgFnS	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
bez	bez	k7c2	bez
Hitlera	Hitler	k1gMnSc2	Hitler
jí	on	k3xPp3gFnSc3	on
hrozí	hrozit	k5eAaImIp3nS	hrozit
zánik	zánik	k1gInSc1	zánik
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
bylo	být	k5eAaImAgNnS	být
vedení	vedení	k1gNnSc1	vedení
strany	strana	k1gFnSc2	strana
rozpuštěno	rozpustit	k5eAaPmNgNnS	rozpustit
a	a	k8xC	a
Hitler	Hitler	k1gMnSc1	Hitler
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jediným	jediný	k2eAgMnSc7d1	jediný
šéfem	šéf	k1gMnSc7	šéf
strany	strana	k1gFnSc2	strana
na	na	k7c6	na
základě	základ	k1gInSc6	základ
tzv.	tzv.	kA	tzv.
vůdcovského	vůdcovský	k2eAgInSc2d1	vůdcovský
principu	princip	k1gInSc2	princip
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1923	[number]	k4	1923
se	se	k3xPyFc4	se
Hitler	Hitler	k1gMnSc1	Hitler
cítil	cítit	k5eAaImAgMnS	cítit
natolik	natolik	k6eAd1	natolik
silný	silný	k2eAgInSc4d1	silný
<g/>
,	,	kIx,	,
že	že	k8xS	že
chtěl	chtít	k5eAaImAgMnS	chtít
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
podnikat	podnikat	k5eAaImF	podnikat
nějaké	nějaký	k3yIgInPc4	nějaký
kroky	krok	k1gInPc4	krok
k	k	k7c3	k
prosazení	prosazení	k1gNnSc3	prosazení
svých	svůj	k3xOyFgInPc2	svůj
cílů	cíl	k1gInPc2	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Velice	velice	k6eAd1	velice
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
Mussoliniho	Mussolini	k1gMnSc2	Mussolini
pochodem	pochod	k1gInSc7	pochod
na	na	k7c4	na
Řím	Řím	k1gInSc4	Řím
se	se	k3xPyFc4	se
odhodlal	odhodlat	k5eAaPmAgMnS	odhodlat
převzít	převzít	k5eAaPmF	převzít
moc	moc	k6eAd1	moc
v	v	k7c6	v
Bavorsku	Bavorsko	k1gNnSc6	Bavorsko
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
pivnímu	pivní	k2eAgInSc3d1	pivní
puči	puč	k1gInSc3	puč
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
sporům	spor	k1gInPc3	spor
mezi	mezi	k7c7	mezi
vládou	vláda	k1gFnSc7	vláda
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
a	a	k8xC	a
bavorskou	bavorský	k2eAgFnSc7d1	bavorská
vládou	vláda	k1gFnSc7	vláda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
usilovala	usilovat	k5eAaImAgFnS	usilovat
o	o	k7c4	o
faktickou	faktický	k2eAgFnSc4d1	faktická
samostatnost	samostatnost	k1gFnSc4	samostatnost
s	s	k7c7	s
pravicovou	pravicový	k2eAgFnSc7d1	pravicová
diktaturou	diktatura	k1gFnSc7	diktatura
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
těchto	tento	k3xDgInPc6	tento
sporech	spor	k1gInPc6	spor
byla	být	k5eAaImAgFnS	být
Berlínem	Berlín	k1gInSc7	Berlín
NSDAP	NSDAP	kA	NSDAP
zakázána	zakázán	k2eAgFnSc1d1	zakázána
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Bavorsko	Bavorsko	k1gNnSc1	Bavorsko
tuto	tento	k3xDgFnSc4	tento
skutečnost	skutečnost	k1gFnSc4	skutečnost
odmítlo	odmítnout	k5eAaPmAgNnS	odmítnout
přijmout	přijmout	k5eAaPmF	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
tohoto	tento	k3xDgInSc2	tento
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
kolem	kolem	k7c2	kolem
Hitlera	Hitler	k1gMnSc2	Hitler
shromáždila	shromáždit	k5eAaPmAgFnS	shromáždit
skupina	skupina	k1gFnSc1	skupina
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
Hermann	Hermann	k1gMnSc1	Hermann
Göring	Göring	k1gInSc1	Göring
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
Hess	Hess	k1gInSc1	Hess
<g/>
,	,	kIx,	,
Ernst	Ernst	k1gMnSc1	Ernst
Röhm	Röhm	k1gMnSc1	Röhm
<g/>
,	,	kIx,	,
Alfred	Alfred	k1gMnSc1	Alfred
Rosenberg	Rosenberg	k1gMnSc1	Rosenberg
a	a	k8xC	a
Max	max	kA	max
von	von	k1gInSc1	von
Scheubner-Richter	Scheubner-Richter	k1gInSc1	Scheubner-Richter
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
provést	provést	k5eAaPmF	provést
ozbrojený	ozbrojený	k2eAgInSc4d1	ozbrojený
puč	puč	k1gInSc4	puč
<g/>
.	.	kIx.	.
</s>
<s>
Hitler	Hitler	k1gMnSc1	Hitler
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
převzít	převzít	k5eAaPmF	převzít
moc	moc	k6eAd1	moc
(	(	kIx(	(
<g/>
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
z	z	k7c2	z
8	[number]	k4	8
<g/>
.	.	kIx.	.
na	na	k7c4	na
9	[number]	k4	9
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Večer	večer	k6eAd1	večer
8	[number]	k4	8
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
roku	rok	k1gInSc2	rok
1923	[number]	k4	1923
nechal	nechat	k5eAaPmAgMnS	nechat
jednotkami	jednotka	k1gFnPc7	jednotka
SA	SA	kA	SA
a	a	k8xC	a
Jungssturmu	Jungssturm	k1gInSc2	Jungssturm
obklíčit	obklíčit	k5eAaPmF	obklíčit
Měšťanský	měšťanský	k2eAgInSc1d1	měšťanský
pivovar	pivovar	k1gInSc1	pivovar
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
veřejná	veřejný	k2eAgFnSc1d1	veřejná
schůze	schůze	k1gFnSc1	schůze
separatisticky	separatisticky	k6eAd1	separatisticky
orientovaných	orientovaný	k2eAgMnPc2d1	orientovaný
politiků	politik	k1gMnPc2	politik
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
podle	podle	k7c2	podle
plánu	plán	k1gInSc2	plán
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
obsazeny	obsadit	k5eAaPmNgFnP	obsadit
městské	městský	k2eAgFnPc1d1	městská
kasárny	kasárny	k1gFnPc1	kasárny
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
nepovedlo	povést	k5eNaPmAgNnS	povést
<g/>
.	.	kIx.	.
</s>
<s>
Následný	následný	k2eAgInSc1d1	následný
nacistický	nacistický	k2eAgInSc1d1	nacistický
pochod	pochod	k1gInSc1	pochod
Mnichovem	Mnichov	k1gInSc7	Mnichov
narazil	narazit	k5eAaPmAgMnS	narazit
na	na	k7c4	na
kordon	kordon	k1gInSc4	kordon
bavorské	bavorský	k2eAgFnSc2d1	bavorská
policie	policie	k1gFnSc2	policie
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
zahájila	zahájit	k5eAaPmAgFnS	zahájit
do	do	k7c2	do
"	"	kIx"	"
<g/>
revolucionářů	revolucionář	k1gMnPc2	revolucionář
<g/>
"	"	kIx"	"
střelbu	střelba	k1gFnSc4	střelba
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
jejich	jejich	k3xOp3gInSc1	jejich
pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
převrat	převrat	k1gInSc4	převrat
zmařen	zmařen	k2eAgInSc4d1	zmařen
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
kroku	krok	k1gInSc6	krok
byl	být	k5eAaImAgMnS	být
odsouzen	odsoudit	k5eAaPmNgMnS	odsoudit
za	za	k7c4	za
velezradu	velezrada	k1gFnSc4	velezrada
k	k	k7c3	k
pěti	pět	k4xCc3	pět
letům	let	k1gInPc3	let
ve	v	k7c4	v
vězení	vězení	k1gNnSc4	vězení
v	v	k7c6	v
Landsbergu	Landsberg	k1gInSc6	Landsberg
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
20	[number]	k4	20
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1924	[number]	k4	1924
byl	být	k5eAaImAgInS	být
však	však	k9	však
podmínečně	podmínečně	k6eAd1	podmínečně
propuštěn	propuštěn	k2eAgMnSc1d1	propuštěn
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
nadiktoval	nadiktovat	k5eAaPmAgMnS	nadiktovat
svoji	svůj	k3xOyFgFnSc4	svůj
knihu	kniha	k1gFnSc4	kniha
Mein	Mein	k1gNnSc4	Mein
Kampf	Kampf	k1gInSc1	Kampf
(	(	kIx(	(
<g/>
Můj	můj	k3xOp1gInSc1	můj
boj	boj	k1gInSc1	boj
<g/>
)	)	kIx)	)
Rudolfu	Rudolf	k1gMnSc3	Rudolf
Hessovi	Hess	k1gMnSc3	Hess
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
dal	dát	k5eAaPmAgMnS	dát
s	s	k7c7	s
Hitlerem	Hitler	k1gMnSc7	Hitler
dobrovolně	dobrovolně	k6eAd1	dobrovolně
zavřít	zavřít	k5eAaPmF	zavřít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
knize	kniha	k1gFnSc6	kniha
Hitler	Hitler	k1gMnSc1	Hitler
popsal	popsat	k5eAaPmAgMnS	popsat
své	svůj	k3xOyFgInPc4	svůj
cíle	cíl	k1gInPc4	cíl
<g/>
,	,	kIx,	,
projevil	projevit	k5eAaPmAgMnS	projevit
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
jeho	jeho	k3xOp3gInSc1	jeho
vyhraněný	vyhraněný	k2eAgInSc1d1	vyhraněný
antisemitismus	antisemitismus	k1gInSc1	antisemitismus
<g/>
,	,	kIx,	,
rasismus	rasismus	k1gInSc1	rasismus
a	a	k8xC	a
ukázal	ukázat	k5eAaPmAgInS	ukázat
zde	zde	k6eAd1	zde
svoji	svůj	k3xOyFgFnSc4	svůj
představu	představa	k1gFnSc4	představa
o	o	k7c4	o
vůdčí	vůdčí	k2eAgFnSc4d1	vůdčí
roli	role	k1gFnSc4	role
nordické	nordický	k2eAgFnSc2d1	nordická
rasy	rasa	k1gFnSc2	rasa
<g/>
.	.	kIx.	.
</s>
<s>
NSDAP	NSDAP	kA	NSDAP
byla	být	k5eAaImAgFnS	být
v	v	k7c4	v
tuto	tento	k3xDgFnSc4	tento
dobu	doba	k1gFnSc4	doba
ve	v	k7c6	v
značném	značný	k2eAgInSc6d1	značný
rozkladu	rozklad	k1gInSc6	rozklad
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
díky	díky	k7c3	díky
Hitlerovým	Hitlerův	k2eAgFnPc3d1	Hitlerova
řečnickým	řečnický	k2eAgFnPc3d1	řečnická
schopnostem	schopnost	k1gFnPc3	schopnost
<g/>
,	,	kIx,	,
silnému	silný	k2eAgInSc3d1	silný
antisemitismu	antisemitismus	k1gInSc3	antisemitismus
a	a	k8xC	a
hospodářské	hospodářský	k2eAgFnSc3d1	hospodářská
krizi	krize	k1gFnSc3	krize
(	(	kIx(	(
<g/>
vzniklé	vzniklý	k2eAgFnSc3d1	vzniklá
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
strana	strana	k1gFnSc1	strana
nakonec	nakonec	k6eAd1	nakonec
velmi	velmi	k6eAd1	velmi
posílila	posílit	k5eAaPmAgFnS	posílit
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejsilnějších	silný	k2eAgFnPc2d3	nejsilnější
stran	strana	k1gFnPc2	strana
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
získali	získat	k5eAaPmAgMnP	získat
nacisté	nacista	k1gMnPc1	nacista
okolo	okolo	k7c2	okolo
17	[number]	k4	17
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
zatím	zatím	k6eAd1	zatím
nejlepší	dobrý	k2eAgInSc1d3	nejlepší
volební	volební	k2eAgInSc1d1	volební
výsledek	výsledek	k1gInSc1	výsledek
(	(	kIx(	(
<g/>
v	v	k7c6	v
předcházejících	předcházející	k2eAgFnPc6d1	předcházející
volbách	volba	k1gFnPc6	volba
se	se	k3xPyFc4	se
nedostali	dostat	k5eNaPmAgMnP	dostat
ani	ani	k9	ani
na	na	k7c4	na
5	[number]	k4	5
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
skončilo	skončit	k5eAaPmAgNnS	skončit
Hindenburgovo	Hindenburgův	k2eAgNnSc1d1	Hindenburgův
úřední	úřední	k2eAgNnSc1d1	úřední
období	období	k1gNnSc1	období
a	a	k8xC	a
Adolf	Adolf	k1gMnSc1	Adolf
Hitler	Hitler	k1gMnSc1	Hitler
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
kandidovat	kandidovat	k5eAaImF	kandidovat
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
byl	být	k5eAaImAgMnS	být
Hitler	Hitler	k1gMnSc1	Hitler
i	i	k9	i
nadále	nadále	k6eAd1	nadále
bohatě	bohatě	k6eAd1	bohatě
podporován	podporovat	k5eAaImNgInS	podporovat
německými	německý	k2eAgInPc7d1	německý
průmyslovými	průmyslový	k2eAgInPc7d1	průmyslový
a	a	k8xC	a
obchodními	obchodní	k2eAgMnPc7d1	obchodní
magnáty	magnát	k1gMnPc7	magnát
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
mu	on	k3xPp3gMnSc3	on
umožňovalo	umožňovat	k5eAaImAgNnS	umožňovat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Goebelsem	Goebels	k1gInSc7	Goebels
uskutečnit	uskutečnit	k5eAaPmF	uskutečnit
bombastickou	bombastický	k2eAgFnSc4d1	bombastická
volební	volební	k2eAgFnSc4d1	volební
kampaň	kampaň	k1gFnSc4	kampaň
<g/>
,	,	kIx,	,
během	během	k7c2	během
které	který	k3yRgFnSc2	který
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
samotný	samotný	k2eAgMnSc1d1	samotný
Hitler	Hitler	k1gMnSc1	Hitler
obrovské	obrovský	k2eAgNnSc4d1	obrovské
úsilí	úsilí	k1gNnSc4	úsilí
(	(	kIx(	(
<g/>
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
politik	politik	k1gMnSc1	politik
používal	používat	k5eAaImAgMnS	používat
k	k	k7c3	k
přepravě	přeprava	k1gFnSc3	přeprava
po	po	k7c6	po
Německu	Německo	k1gNnSc6	Německo
letadlo	letadlo	k1gNnSc4	letadlo
a	a	k8xC	a
stihl	stihnout	k5eAaPmAgMnS	stihnout
tak	tak	k6eAd1	tak
promluvit	promluvit	k5eAaPmF	promluvit
i	i	k9	i
na	na	k7c6	na
čtyřech	čtyři	k4xCgNnPc6	čtyři
shromážděních	shromáždění	k1gNnPc6	shromáždění
během	během	k7c2	během
jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
nepodařilo	podařit	k5eNaPmAgNnS	podařit
zvítězit	zvítězit	k5eAaPmF	zvítězit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
získal	získat	k5eAaPmAgMnS	získat
30,1	[number]	k4	30,1
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Hindenburg	Hindenburg	k1gInSc1	Hindenburg
49,6	[number]	k4	49,6
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgMnPc1d1	ostatní
dva	dva	k4xCgMnPc1	dva
kandidáti	kandidát	k1gMnPc1	kandidát
zůstali	zůstat	k5eAaPmAgMnP	zůstat
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
odstupem	odstup	k1gInSc7	odstup
vzadu	vzadu	k6eAd1	vzadu
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
ani	ani	k8xC	ani
jeden	jeden	k4xCgMnSc1	jeden
kandidát	kandidát	k1gMnSc1	kandidát
nezískal	získat	k5eNaPmAgMnS	získat
nadpoloviční	nadpoloviční	k2eAgFnSc4d1	nadpoloviční
většinu	většina	k1gFnSc4	většina
<g/>
,	,	kIx,	,
rozhodovalo	rozhodovat	k5eAaImAgNnS	rozhodovat
se	se	k3xPyFc4	se
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
kole	kolo	k1gNnSc6	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
kole	kolo	k1gNnSc6	kolo
získal	získat	k5eAaPmAgMnS	získat
36	[number]	k4	36
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
a	a	k8xC	a
Hindenburg	Hindenburg	k1gInSc1	Hindenburg
přes	přes	k7c4	přes
53	[number]	k4	53
%	%	kIx~	%
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
větší	veliký	k2eAgFnSc1d2	veliký
část	část	k1gFnSc1	část
národa	národ	k1gInSc2	národ
Hitlera	Hitler	k1gMnSc2	Hitler
jako	jako	k8xC	jako
prezidenta	prezident	k1gMnSc2	prezident
nepřála	přát	k5eNaImAgFnS	přát
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
neúspěšných	úspěšný	k2eNgInPc6d1	neúspěšný
pokusech	pokus	k1gInPc6	pokus
ve	v	k7c6	v
vedení	vedení	k1gNnSc6	vedení
vlády	vláda	k1gFnSc2	vláda
kancléři	kancléř	k1gMnSc3	kancléř
Brüningem	Brüning	k1gInSc7	Brüning
<g/>
,	,	kIx,	,
von	von	k1gInSc1	von
Papenem	Papen	k1gInSc7	Papen
a	a	k8xC	a
Schleicherem	Schleicher	k1gInSc7	Schleicher
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
se	se	k3xPyFc4	se
Hindenburg	Hindenburg	k1gMnSc1	Hindenburg
pod	pod	k7c7	pod
tlakem	tlak	k1gInSc7	tlak
těchto	tento	k3xDgFnPc2	tento
okolností	okolnost	k1gFnPc2	okolnost
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
jmenovat	jmenovat	k5eAaBmF	jmenovat
říšským	říšský	k2eAgMnSc7d1	říšský
kancléřem	kancléř	k1gMnSc7	kancléř
Adolfa	Adolf	k1gMnSc2	Adolf
Hitlera	Hitler	k1gMnSc2	Hitler
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
30	[number]	k4	30
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1933	[number]	k4	1933
ho	on	k3xPp3gInSc4	on
říšský	říšský	k2eAgMnSc1d1	říšský
prezident	prezident	k1gMnSc1	prezident
Hindenburg	Hindenburg	k1gMnSc1	Hindenburg
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
říšským	říšský	k2eAgMnSc7d1	říšský
kancléřem	kancléř	k1gMnSc7	kancléř
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
Hitler	Hitler	k1gMnSc1	Hitler
dostal	dostat	k5eAaPmAgMnS	dostat
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgMnS	začít
uskutečňovat	uskutečňovat	k5eAaImF	uskutečňovat
svoji	svůj	k3xOyFgFnSc4	svůj
politiku	politika	k1gFnSc4	politika
a	a	k8xC	a
pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
se	se	k3xPyFc4	se
zlikvidovat	zlikvidovat	k5eAaPmF	zlikvidovat
své	svůj	k3xOyFgMnPc4	svůj
odpůrce	odpůrce	k1gMnPc4	odpůrce
<g/>
.	.	kIx.	.
</s>
<s>
Požár	požár	k1gInSc1	požár
Říšského	říšský	k2eAgInSc2d1	říšský
sněmu	sněm	k1gInSc2	sněm
z	z	k7c2	z
27	[number]	k4	27
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
mu	on	k3xPp3gMnSc3	on
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
příležitost	příležitost	k1gFnSc4	příležitost
začít	začít	k5eAaPmF	začít
s	s	k7c7	s
útokem	útok	k1gInSc7	útok
na	na	k7c4	na
komunisty	komunista	k1gMnPc4	komunista
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
Hitlerově	Hitlerův	k2eAgFnSc6d1	Hitlerova
ekonomické	ekonomický	k2eAgFnSc6d1	ekonomická
politice	politika	k1gFnSc6	politika
se	se	k3xPyFc4	se
nedá	dát	k5eNaPmIp3nS	dát
vůbec	vůbec	k9	vůbec
hovořit	hovořit	k5eAaImF	hovořit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
ekonomickým	ekonomický	k2eAgNnPc3d1	ekonomické
problémům	problém	k1gInPc3	problém
nerozuměl	rozumět	k5eNaImAgMnS	rozumět
a	a	k8xC	a
ani	ani	k8xC	ani
o	o	k7c4	o
ně	on	k3xPp3gMnPc4	on
nejevil	jevit	k5eNaImAgInS	jevit
sebemenší	sebemenší	k2eAgInSc1d1	sebemenší
zájem	zájem	k1gInSc1	zájem
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Stát	stát	k1gInSc1	stát
se	se	k3xPyFc4	se
nemůže	moct	k5eNaImIp3nS	moct
ztotožňovat	ztotožňovat	k5eAaImF	ztotožňovat
s	s	k7c7	s
žádnou	žádný	k3yNgFnSc7	žádný
konkrétní	konkrétní	k2eAgFnSc7d1	konkrétní
ekonomickou	ekonomický	k2eAgFnSc7d1	ekonomická
koncepcí	koncepce	k1gFnSc7	koncepce
či	či	k8xC	či
ekonomickým	ekonomický	k2eAgInSc7d1	ekonomický
vývojem	vývoj	k1gInSc7	vývoj
<g/>
...	...	k?	...
Stát	stát	k1gInSc1	stát
je	být	k5eAaImIp3nS	být
rasový	rasový	k2eAgInSc1d1	rasový
organismus	organismus	k1gInSc1	organismus
a	a	k8xC	a
ne	ne	k9	ne
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
organizace	organizace	k1gFnSc1	organizace
<g/>
...	...	k?	...
Ještě	ještě	k9	ještě
nikdy	nikdy	k6eAd1	nikdy
se	se	k3xPyFc4	se
nestalo	stát	k5eNaPmAgNnS	stát
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
stát	stát	k1gInSc1	stát
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
mírovými	mírový	k2eAgInPc7d1	mírový
ekonomickými	ekonomický	k2eAgInPc7d1	ekonomický
prostředky	prostředek	k1gInPc7	prostředek
<g/>
...	...	k?	...
(	(	kIx(	(
<g/>
Mein	Mein	k1gMnSc1	Mein
Kampf	Kampf	k1gMnSc1	Kampf
<g/>
)	)	kIx)	)
Díky	díky	k7c3	díky
tajnému	tajný	k2eAgNnSc3d1	tajné
rozšiřování	rozšiřování	k1gNnSc3	rozšiřování
zbrojní	zbrojní	k2eAgFnSc2d1	zbrojní
výroby	výroba	k1gFnSc2	výroba
<g/>
,	,	kIx,	,
budování	budování	k1gNnSc1	budování
silnic	silnice	k1gFnPc2	silnice
a	a	k8xC	a
nových	nový	k2eAgFnPc2d1	nová
továren	továrna	k1gFnPc2	továrna
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
podařilo	podařit	k5eAaPmAgNnS	podařit
výrazně	výrazně	k6eAd1	výrazně
snížit	snížit	k5eAaPmF	snížit
nezaměstnanost	nezaměstnanost	k1gFnSc4	nezaměstnanost
<g/>
,	,	kIx,	,
inflaci	inflace	k1gFnSc4	inflace
a	a	k8xC	a
zvýšit	zvýšit	k5eAaPmF	zvýšit
životní	životní	k2eAgFnSc4d1	životní
úroveň	úroveň	k1gFnSc4	úroveň
průměrné	průměrný	k2eAgFnSc2d1	průměrná
německé	německý	k2eAgFnSc2d1	německá
domácnosti	domácnost	k1gFnSc2	domácnost
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Stále	stále	k6eAd1	stále
silnější	silný	k2eAgNnSc4d2	silnější
směřování	směřování	k1gNnSc4	směřování
německé	německý	k2eAgFnSc2d1	německá
ekonomiky	ekonomika	k1gFnSc2	ekonomika
na	na	k7c4	na
zbrojní	zbrojní	k2eAgFnSc4d1	zbrojní
výrobu	výroba	k1gFnSc4	výroba
však	však	k9	však
nevyhnutelně	vyhnutelně	k6eNd1	vyhnutelně
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
vypuknutí	vypuknutí	k1gNnSc3	vypuknutí
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Neboť	neboť	k8xC	neboť
i	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
ministr	ministr	k1gMnSc1	ministr
hospodářství	hospodářství	k1gNnSc2	hospodářství
Hjalmar	Hjalmar	k1gMnSc1	Hjalmar
Schacht	Schacht	k1gMnSc1	Schacht
snažil	snažit	k5eAaImAgMnS	snažit
zvyšováním	zvyšování	k1gNnSc7	zvyšování
emise	emise	k1gFnSc2	emise
bankovek	bankovka	k1gFnPc2	bankovka
a	a	k8xC	a
vydáváním	vydávání	k1gNnSc7	vydávání
státních	státní	k2eAgFnPc2d1	státní
obligací	obligace	k1gFnPc2	obligace
získávat	získávat	k5eAaImF	získávat
pro	pro	k7c4	pro
Hitlera	Hitler	k1gMnSc4	Hitler
více	hodně	k6eAd2	hodně
peněz	peníze	k1gInPc2	peníze
<g/>
,	,	kIx,	,
nedostatek	nedostatek	k1gInSc1	nedostatek
devizových	devizový	k2eAgFnPc2d1	devizová
a	a	k8xC	a
zlatých	zlatý	k2eAgFnPc2d1	zlatá
rezerv	rezerva	k1gFnPc2	rezerva
by	by	kYmCp3nS	by
musel	muset	k5eAaImAgInS	muset
nakonec	nakonec	k6eAd1	nakonec
vést	vést	k5eAaImF	vést
ke	k	k7c3	k
zhroucení	zhroucení	k1gNnSc3	zhroucení
německé	německý	k2eAgFnSc2d1	německá
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zahraniční	zahraniční	k2eAgFnSc6d1	zahraniční
politice	politika	k1gFnSc6	politika
se	se	k3xPyFc4	se
Hitler	Hitler	k1gMnSc1	Hitler
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
vystoupit	vystoupit	k5eAaPmF	vystoupit
ze	z	k7c2	z
Společnosti	společnost	k1gFnSc2	společnost
národů	národ	k1gInPc2	národ
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
krok	krok	k1gInSc1	krok
přinesl	přinést	k5eAaPmAgInS	přinést
Hitlerově	Hitlerův	k2eAgFnSc3d1	Hitlerova
politice	politika	k1gFnSc3	politika
mnoho	mnoho	k6eAd1	mnoho
výhod	výhod	k1gInSc4	výhod
<g/>
,	,	kIx,	,
a	a	k8xC	a
jelikož	jelikož	k8xS	jelikož
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
ostatní	ostatní	k1gNnSc4	ostatní
státy	stát	k1gInPc4	stát
prakticky	prakticky	k6eAd1	prakticky
nereagovaly	reagovat	k5eNaBmAgFnP	reagovat
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
nevýhody	nevýhoda	k1gFnPc1	nevýhoda
takřka	takřka	k6eAd1	takřka
nulové	nulový	k2eAgFnPc1d1	nulová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1934	[number]	k4	1934
se	se	k3xPyFc4	se
Hitler	Hitler	k1gMnSc1	Hitler
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
zbavit	zbavit	k5eAaPmF	zbavit
těch	ten	k3xDgMnPc2	ten
členů	člen	k1gMnPc2	člen
SA	SA	kA	SA
a	a	k8xC	a
NSDAP	NSDAP	kA	NSDAP
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
stále	stále	k6eAd1	stále
ještě	ještě	k6eAd1	ještě
věřili	věřit	k5eAaImAgMnP	věřit
v	v	k7c4	v
socialistickou	socialistický	k2eAgFnSc4d1	socialistická
revoluci	revoluce	k1gFnSc4	revoluce
a	a	k8xC	a
svým	svůj	k3xOyFgNnSc7	svůj
násilím	násilí	k1gNnSc7	násilí
začali	začít	k5eAaPmAgMnP	začít
znervózňovat	znervózňovat	k5eAaImF	znervózňovat
voliče	volič	k1gMnPc4	volič
i	i	k8xC	i
konzervativní	konzervativní	k2eAgInPc4d1	konzervativní
obchodní	obchodní	k2eAgInPc4d1	obchodní
kruhy	kruh	k1gInPc4	kruh
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
tajně	tajně	k6eAd1	tajně
podporovaly	podporovat	k5eAaImAgInP	podporovat
Hitlera	Hitler	k1gMnSc2	Hitler
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
byly	být	k5eAaImAgFnP	být
SA	SA	kA	SA
velkou	velký	k2eAgFnSc7d1	velká
překážkou	překážka	k1gFnSc7	překážka
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
sbližování	sbližování	k1gNnSc1	sbližování
Hitlera	Hitler	k1gMnSc2	Hitler
a	a	k8xC	a
tradičně	tradičně	k6eAd1	tradičně
konzervativního	konzervativní	k2eAgInSc2d1	konzervativní
Reichswehru	Reichswehra	k1gFnSc4	Reichswehra
(	(	kIx(	(
<g/>
říšské	říšský	k2eAgFnPc1d1	říšská
armády	armáda	k1gFnPc1	armáda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hitler	Hitler	k1gMnSc1	Hitler
si	se	k3xPyFc3	se
uvědomoval	uvědomovat	k5eAaImAgMnS	uvědomovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
bez	bez	k7c2	bez
podpory	podpora	k1gFnSc2	podpora
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
jejích	její	k3xOp3gMnPc2	její
důstojníků	důstojník	k1gMnPc2	důstojník
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
nemůže	moct	k5eNaImIp3nS	moct
podařit	podařit	k5eAaPmF	podařit
skutečně	skutečně	k6eAd1	skutečně
se	se	k3xPyFc4	se
dostat	dostat	k5eAaPmF	dostat
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
<g/>
.	.	kIx.	.
</s>
<s>
Zavraždění	zavraždění	k1gNnSc1	zavraždění
mnoha	mnoho	k4c2	mnoho
politických	politický	k2eAgMnPc2d1	politický
odpůrců	odpůrce	k1gMnPc2	odpůrce
<g/>
,	,	kIx,	,
konkurentů	konkurent	k1gMnPc2	konkurent
ve	v	k7c6	v
vlastní	vlastní	k2eAgFnSc6d1	vlastní
straně	strana	k1gFnSc6	strana
a	a	k8xC	a
především	především	k6eAd1	především
Röhma	Röhma	k1gNnSc1	Röhma
vešlo	vejít	k5eAaPmAgNnS	vejít
do	do	k7c2	do
dějin	dějiny	k1gFnPc2	dějiny
jako	jako	k8xS	jako
Noc	noc	k1gFnSc4	noc
dlouhých	dlouhý	k2eAgInPc2d1	dlouhý
nožů	nůž	k1gInPc2	nůž
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1934	[number]	k4	1934
se	se	k3xPyFc4	se
Hitlerovi	Hitler	k1gMnSc3	Hitler
podařilo	podařit	k5eAaPmAgNnS	podařit
prosadit	prosadit	k5eAaPmF	prosadit
spojení	spojení	k1gNnSc2	spojení
úřadu	úřad	k1gInSc2	úřad
říšského	říšský	k2eAgMnSc2d1	říšský
prezidenta	prezident	k1gMnSc2	prezident
a	a	k8xC	a
říšského	říšský	k2eAgMnSc2d1	říšský
kancléře	kancléř	k1gMnSc2	kancléř
<g/>
,	,	kIx,	,
den	den	k1gInSc4	den
nato	nato	k6eAd1	nato
zemřel	zemřít	k5eAaPmAgMnS	zemřít
prezident	prezident	k1gMnSc1	prezident
Hindenburg	Hindenburg	k1gMnSc1	Hindenburg
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgNnSc7	tento
se	se	k3xPyFc4	se
Hitler	Hitler	k1gMnSc1	Hitler
stal	stát	k5eAaPmAgMnS	stát
prakticky	prakticky	k6eAd1	prakticky
neomezeným	omezený	k2eNgMnSc7d1	neomezený
vládcem	vládce	k1gMnSc7	vládce
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
t.	t.	k?	t.
r.	r.	kA	r.
měl	mít	k5eAaImAgMnS	mít
Hitler	Hitler	k1gMnSc1	Hitler
projev	projev	k1gInSc4	projev
v	v	k7c6	v
Říšském	říšský	k2eAgInSc6d1	říšský
sněmu	sněm	k1gInSc6	sněm
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
odvolal	odvolat	k5eAaPmAgMnS	odvolat
na	na	k7c4	na
životní	životní	k2eAgNnPc4d1	životní
práva	právo	k1gNnPc4	právo
národů	národ	k1gInPc2	národ
a	a	k8xC	a
přihlásil	přihlásit	k5eAaPmAgMnS	přihlásit
se	se	k3xPyFc4	se
k	k	k7c3	k
mírové	mírový	k2eAgFnSc3d1	mírová
smlouvě	smlouva	k1gFnSc3	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
po	po	k7c6	po
těchto	tento	k3xDgFnPc6	tento
volbách	volba	k1gFnPc6	volba
byly	být	k5eAaImAgFnP	být
zahraniční	zahraniční	k2eAgFnPc1d1	zahraniční
vlády	vláda	k1gFnPc1	vláda
informovány	informovat	k5eAaBmNgFnP	informovat
o	o	k7c4	o
budování	budování	k1gNnSc4	budování
německé	německý	k2eAgFnSc2d1	německá
Luftwaffe	Luftwaff	k1gInSc5	Luftwaff
a	a	k8xC	a
16	[number]	k4	16
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1935	[number]	k4	1935
byla	být	k5eAaImAgFnS	být
znovu	znovu	k6eAd1	znovu
zavedena	zavést	k5eAaPmNgFnS	zavést
všeobecná	všeobecný	k2eAgFnSc1d1	všeobecná
branná	branný	k2eAgFnSc1d1	Branná
povinnost	povinnost	k1gFnSc1	povinnost
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
spojenectví	spojenectví	k1gNnSc4	spojenectví
s	s	k7c7	s
fašistickou	fašistický	k2eAgFnSc7d1	fašistická
Itálií	Itálie	k1gFnSc7	Itálie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
napadení	napadení	k1gNnSc6	napadení
Habeše	Habeš	k1gFnSc2	Habeš
izolována	izolovat	k5eAaBmNgFnS	izolovat
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
Hitler	Hitler	k1gMnSc1	Hitler
musel	muset	k5eAaImAgMnS	muset
dávat	dávat	k5eAaImF	dávat
pozor	pozor	k1gInSc4	pozor
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
kvůli	kvůli	k7c3	kvůli
Itálii	Itálie	k1gFnSc3	Itálie
nedostal	dostat	k5eNaPmAgInS	dostat
do	do	k7c2	do
rozporů	rozpor	k1gInPc2	rozpor
s	s	k7c7	s
Velkou	velký	k2eAgFnSc7d1	velká
Británií	Británie	k1gFnSc7	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
7	[number]	k4	7
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1936	[number]	k4	1936
Německo	Německo	k1gNnSc1	Německo
vypovědělo	vypovědět	k5eAaPmAgNnS	vypovědět
Locarnskou	Locarnský	k2eAgFnSc4d1	Locarnská
smlouvu	smlouva	k1gFnSc4	smlouva
a	a	k8xC	a
obsadilo	obsadit	k5eAaPmAgNnS	obsadit
demilitarizované	demilitarizovaný	k2eAgNnSc1d1	demilitarizované
území	území	k1gNnSc1	území
v	v	k7c6	v
Porýní	Porýní	k1gNnSc6	Porýní
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1936	[number]	k4	1936
a	a	k8xC	a
1937	[number]	k4	1937
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
vypracování	vypracování	k1gNnSc3	vypracování
plánů	plán	k1gInPc2	plán
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
12	[number]	k4	12
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1938	[number]	k4	1938
si	se	k3xPyFc3	se
Hitler	Hitler	k1gMnSc1	Hitler
vynutil	vynutit	k5eAaPmAgMnS	vynutit
souhlas	souhlas	k1gInSc4	souhlas
rakouského	rakouský	k2eAgMnSc2d1	rakouský
kancléře	kancléř	k1gMnSc2	kancléř
k	k	k7c3	k
anšlusu	anšlus	k1gInSc3	anšlus
Rakouska	Rakousko	k1gNnSc2	Rakousko
a	a	k8xC	a
německá	německý	k2eAgFnSc1d1	německá
vojska	vojsko	k1gNnSc2	vojsko
okupovala	okupovat	k5eAaBmAgFnS	okupovat
Rakousko	Rakousko	k1gNnSc4	Rakousko
<g/>
.	.	kIx.	.
29	[number]	k4	29
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
Mnichovské	mnichovský	k2eAgFnSc3d1	Mnichovská
konferenci	konference	k1gFnSc3	konference
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgMnPc4	který
<g />
.	.	kIx.	.
</s>
<s>
se	s	k7c7	s
s	s	k7c7	s
Velkou	velký	k2eAgFnSc7d1	velká
Británií	Británie	k1gFnSc7	Británie
<g/>
,	,	kIx,	,
Francií	Francie	k1gFnSc7	Francie
a	a	k8xC	a
Itálií	Itálie	k1gFnSc7	Itálie
dohodl	dohodnout	k5eAaPmAgMnS	dohodnout
na	na	k7c4	na
anexi	anexe	k1gFnSc4	anexe
pohraničního	pohraniční	k2eAgNnSc2d1	pohraniční
území	území	k1gNnSc2	území
nezávislého	závislý	k2eNgNnSc2d1	nezávislé
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Tato	tento	k3xDgFnSc1	tento
konference	konference	k1gFnSc1	konference
de	de	k?	de
facto	facto	k1gNnSc1	facto
ukázala	ukázat	k5eAaPmAgFnS	ukázat
<g/>
,	,	kIx,	,
v	v	k7c6	v
jaké	jaký	k3yIgFnSc6	jaký
politické	politický	k2eAgFnSc6d1	politická
krizi	krize	k1gFnSc6	krize
se	se	k3xPyFc4	se
Evropa	Evropa	k1gFnSc1	Evropa
ocitla	ocitnout	k5eAaPmAgFnS	ocitnout
<g/>
,	,	kIx,	,
a	a	k8xC	a
nastínila	nastínit	k5eAaPmAgFnS	nastínit
skutečný	skutečný	k2eAgInSc4d1	skutečný
zájem	zájem	k1gInSc4	zájem
Adolfa	Adolf	k1gMnSc2	Adolf
Hitlera	Hitler	k1gMnSc2	Hitler
ohledně	ohledně	k7c2	ohledně
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
politiky	politika	k1gFnSc2	politika
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
hlavním	hlavní	k2eAgMnSc7d1	hlavní
kritikem	kritik	k1gMnSc7	kritik
Mnichovské	mnichovský	k2eAgFnSc2d1	Mnichovská
dohody	dohoda	k1gFnSc2	dohoda
byl	být	k5eAaImAgMnS	být
toho	ten	k3xDgInSc2	ten
času	čas	k1gInSc2	čas
opoziční	opoziční	k2eAgInSc1d1	opoziční
Winston	Winston	k1gInSc1	Winston
Churchill	Churchilla	k1gFnPc2	Churchilla
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
vážné	vážný	k2eAgFnSc3d1	vážná
krizi	krize	k1gFnSc3	krize
ve	v	k7c6	v
vedení	vedení	k1gNnSc6	vedení
Wehrmachtu	wehrmacht	k1gInSc2	wehrmacht
<g/>
.	.	kIx.	.
</s>
<s>
Polní	polní	k2eAgMnSc1d1	polní
maršál	maršál	k1gMnSc1	maršál
von	von	k1gInSc4	von
Blomberg	Blomberg	k1gMnSc1	Blomberg
požádal	požádat	k5eAaPmAgMnS	požádat
Hitlera	Hitler	k1gMnSc4	Hitler
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
směl	smět	k5eAaImAgMnS	smět
oženit	oženit	k5eAaPmF	oženit
se	s	k7c7	s
ženou	žena	k1gFnSc7	žena
z	z	k7c2	z
lidu	lid	k1gInSc2	lid
<g/>
.	.	kIx.	.
</s>
<s>
Hitler	Hitler	k1gMnSc1	Hitler
svolil	svolit	k5eAaPmAgMnS	svolit
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
dokonce	dokonce	k9	dokonce
za	za	k7c4	za
svědka	svědek	k1gMnSc4	svědek
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
ale	ale	k9	ale
zjistilo	zjistit	k5eAaPmAgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
žena	žena	k1gFnSc1	žena
pracovala	pracovat	k5eAaImAgFnS	pracovat
jako	jako	k9	jako
pornoherečka	pornoherečka	k1gFnSc1	pornoherečka
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
si	se	k3xPyFc3	se
občas	občas	k6eAd1	občas
přivydělávala	přivydělávat	k5eAaImAgFnS	přivydělávat
prostitucí	prostituce	k1gFnSc7	prostituce
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgNnSc1	takový
manželství	manželství	k1gNnSc1	manželství
bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
ministra	ministr	k1gMnSc4	ministr
války	válka	k1gFnSc2	válka
nepřípustné	přípustný	k2eNgNnSc1d1	nepřípustné
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
byl	být	k5eAaImAgMnS	být
generál	generál	k1gMnSc1	generál
von	von	k1gInSc4	von
Fritsche	Fritsch	k1gInSc2	Fritsch
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
falešných	falešný	k2eAgInPc2d1	falešný
důkazů	důkaz	k1gInPc2	důkaz
sehnaných	sehnaný	k2eAgInPc2d1	sehnaný
gestapem	gestapo	k1gNnSc7	gestapo
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
výpovědi	výpověď	k1gFnPc1	výpověď
už	už	k6eAd1	už
několikrát	několikrát	k6eAd1	několikrát
trestaného	trestaný	k2eAgMnSc4d1	trestaný
vyděrače	vyděrač	k1gMnSc4	vyděrač
<g/>
,	,	kIx,	,
křivě	křivě	k6eAd1	křivě
obviněn	obvinit	k5eAaPmNgMnS	obvinit
z	z	k7c2	z
homosexuality	homosexualita	k1gFnSc2	homosexualita
<g/>
,	,	kIx,	,
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
trestné	trestný	k2eAgInPc1d1	trestný
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
pozdější	pozdní	k2eAgNnSc1d2	pozdější
vyšetřování	vyšetřování	k1gNnSc1	vyšetřování
neodhalilo	odhalit	k5eNaPmAgNnS	odhalit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
von	von	k1gInSc1	von
Fritsch	Fritscha	k1gFnPc2	Fritscha
dopustil	dopustit	k5eAaPmAgInS	dopustit
činu	čin	k1gInSc3	čin
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgMnSc2	jenž
byl	být	k5eAaImAgInS	být
obviňován	obviňován	k2eAgInSc1d1	obviňován
<g/>
,	,	kIx,	,
Hitler	Hitler	k1gMnSc1	Hitler
byl	být	k5eAaImAgMnS	být
touto	tento	k3xDgFnSc7	tento
zradou	zrada	k1gFnSc7	zrada
údajně	údajně	k6eAd1	údajně
zklamán	zklamat	k5eAaPmNgMnS	zklamat
a	a	k8xC	a
oba	dva	k4xCgMnPc4	dva
muže	muž	k1gMnPc4	muž
propustil	propustit	k5eAaPmAgInS	propustit
<g/>
.	.	kIx.	.
</s>
<s>
Von	von	k1gInSc1	von
Fritsch	Fritsch	k1gInSc1	Fritsch
byl	být	k5eAaImAgInS	být
propuštěn	propustit	k5eAaPmNgInS	propustit
4	[number]	k4	4
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1938	[number]	k4	1938
a	a	k8xC	a
Hitler	Hitler	k1gMnSc1	Hitler
se	se	k3xPyFc4	se
sám	sám	k3xTgMnSc1	sám
stal	stát	k5eAaPmAgMnS	stát
vrchním	vrchní	k2eAgMnSc7d1	vrchní
velitelem	velitel	k1gMnSc7	velitel
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Přímo	přímo	k6eAd1	přímo
ovládal	ovládat	k5eAaImAgInS	ovládat
nejen	nejen	k6eAd1	nejen
politiku	politika	k1gFnSc4	politika
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
celou	celý	k2eAgFnSc4d1	celá
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
mašinérii	mašinérie	k1gFnSc4	mašinérie
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
března	březen	k1gInSc2	březen
1939	[number]	k4	1939
dal	dát	k5eAaPmAgMnS	dát
Hitler	Hitler	k1gMnSc1	Hitler
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
Ribbentropa	Ribbentrop	k1gMnSc2	Ribbentrop
rozšířit	rozšířit	k5eAaPmF	rozšířit
návrh	návrh	k1gInSc4	návrh
k	k	k7c3	k
úpravě	úprava	k1gFnSc3	úprava
sporných	sporný	k2eAgFnPc2d1	sporná
německo-polských	německoolský	k2eAgFnPc2d1	německo-polská
otázek	otázka	k1gFnPc2	otázka
<g/>
.	.	kIx.	.
</s>
<s>
Záminkou	záminka	k1gFnSc7	záminka
pro	pro	k7c4	pro
zvyšování	zvyšování	k1gNnSc4	zvyšování
tlaku	tlak	k1gInSc2	tlak
na	na	k7c4	na
Polsko	Polsko	k1gNnSc4	Polsko
byly	být	k5eAaImAgInP	být
požadavky	požadavek	k1gInPc1	požadavek
týkající	týkající	k2eAgFnPc4d1	týkající
se	se	k3xPyFc4	se
svobodného	svobodný	k2eAgNnSc2d1	svobodné
města	město	k1gNnSc2	město
Gdaňsk	Gdaňsk	k1gInSc1	Gdaňsk
a	a	k8xC	a
požadavek	požadavek	k1gInSc1	požadavek
na	na	k7c4	na
koridor	koridor	k1gInSc4	koridor
s	s	k7c7	s
exteritoriálními	exteritoriální	k2eAgNnPc7d1	exteritoriální
právy	právo	k1gNnPc7	právo
pro	pro	k7c4	pro
železnici	železnice	k1gFnSc4	železnice
a	a	k8xC	a
silnici	silnice	k1gFnSc4	silnice
spojující	spojující	k2eAgFnSc4d1	spojující
východní	východní	k2eAgNnSc4d1	východní
Prusko	Prusko	k1gNnSc4	Prusko
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Hitler	Hitler	k1gMnSc1	Hitler
předpokládal	předpokládat	k5eAaImAgMnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podaří	podařit	k5eAaPmIp3nS	podařit
zastrašováním	zastrašování	k1gNnSc7	zastrašování
Polska	Polsko	k1gNnSc2	Polsko
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
dalších	další	k2eAgInPc2d1	další
územních	územní	k2eAgInPc2d1	územní
zisků	zisk	k1gInPc2	zisk
bez	bez	k7c2	bez
boje	boj	k1gInSc2	boj
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
předcházejících	předcházející	k2eAgFnPc6d1	předcházející
zkušenostech	zkušenost	k1gFnPc6	zkušenost
s	s	k7c7	s
neschopností	neschopnost	k1gFnSc7	neschopnost
Daladiera	Daladiero	k1gNnSc2	Daladiero
a	a	k8xC	a
Chamberlaina	Chamberlaino	k1gNnSc2	Chamberlaino
postavit	postavit	k5eAaPmF	postavit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
na	na	k7c4	na
odpor	odpor	k1gInSc4	odpor
předpokládal	předpokládat	k5eAaImAgInS	předpokládat
kolaps	kolaps	k1gInSc1	kolaps
Polska	Polsko	k1gNnSc2	Polsko
a	a	k8xC	a
nezájem	nezájem	k1gInSc1	nezájem
západních	západní	k2eAgMnPc2d1	západní
spojenců	spojenec	k1gMnPc2	spojenec
<g/>
.	.	kIx.	.
</s>
<s>
Polské	polský	k2eAgNnSc1d1	polské
vedení	vedení	k1gNnSc1	vedení
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
plukovníkem	plukovník	k1gMnSc7	plukovník
Beckem	Becek	k1gMnSc7	Becek
ovšem	ovšem	k9	ovšem
bylo	být	k5eAaImAgNnS	být
neústupné	ústupný	k2eNgNnSc1d1	neústupné
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
Hitler	Hitler	k1gMnSc1	Hitler
poprvé	poprvé	k6eAd1	poprvé
použil	použít	k5eAaPmAgMnS	použít
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
sílu	síla	k1gFnSc4	síla
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
ospravedlnění	ospravedlnění	k1gNnSc4	ospravedlnění
vpádu	vpád	k1gInSc2	vpád
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
byl	být	k5eAaImAgInS	být
v	v	k7c4	v
předvečer	předvečer	k1gInSc4	předvečer
války	válka	k1gFnSc2	válka
zinscenován	zinscenován	k2eAgInSc4d1	zinscenován
incident	incident	k1gInSc4	incident
u	u	k7c2	u
vysílače	vysílač	k1gInSc2	vysílač
v	v	k7c6	v
Gliwicích	Gliwice	k1gFnPc6	Gliwice
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
23	[number]	k4	23
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
podepsal	podepsat	k5eAaPmAgMnS	podepsat
se	s	k7c7	s
stalinským	stalinský	k2eAgInSc7d1	stalinský
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
německo-sovětský	německoovětský	k2eAgInSc1d1	německo-sovětský
pakt	pakt	k1gInSc1	pakt
o	o	k7c6	o
neútočení	neútočení	k1gNnSc6	neútočení
a	a	k8xC	a
hospodářské	hospodářský	k2eAgFnSc3d1	hospodářská
spolupráci	spolupráce	k1gFnSc3	spolupráce
z	z	k7c2	z
23	[number]	k4	23
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodující	rozhodující	k2eAgFnSc1d1	rozhodující
pasáž	pasáž	k1gFnSc1	pasáž
zněla	znět	k5eAaImAgFnS	znět
<g/>
:	:	kIx,	:
V	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
stran	strana	k1gFnPc2	strana
uzavírajících	uzavírající	k2eAgFnPc2d1	uzavírající
smlouvu	smlouva	k1gFnSc4	smlouva
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
stát	stát	k5eAaImF	stát
předmětem	předmět	k1gInSc7	předmět
válečných	válečný	k2eAgNnPc2d1	válečné
jednání	jednání	k1gNnPc2	jednání
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
třetí	třetí	k4xOgFnSc2	třetí
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
nebude	být	k5eNaImBp3nS	být
druhá	druhý	k4xOgFnSc1	druhý
ze	z	k7c2	z
stran	strana	k1gFnPc2	strana
uzavírajících	uzavírající	k2eAgFnPc2d1	uzavírající
smlouvu	smlouva	k1gFnSc4	smlouva
podporovat	podporovat	k5eAaImF	podporovat
tuto	tento	k3xDgFnSc4	tento
třetí	třetí	k4xOgFnSc4	třetí
moc	moc	k1gFnSc4	moc
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
podepsali	podepsat	k5eAaPmAgMnP	podepsat
sovětský	sovětský	k2eAgMnSc1d1	sovětský
ministr	ministr	k1gMnSc1	ministr
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
Vjačeslav	Vjačeslav	k1gMnSc1	Vjačeslav
Michajlovič	Michajlovič	k1gMnSc1	Michajlovič
Molotov	Molotovo	k1gNnPc2	Molotovo
a	a	k8xC	a
nacistický	nacistický	k2eAgMnSc1d1	nacistický
ministr	ministr	k1gMnSc1	ministr
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
Joachim	Joachima	k1gFnPc2	Joachima
von	von	k1gInSc1	von
Ribbentrop	Ribbentrop	k1gInSc4	Ribbentrop
společné	společný	k2eAgNnSc4d1	společné
německo-sovětské	německoovětský	k2eAgNnSc4d1	německo-sovětský
prohlášení	prohlášení	k1gNnSc4	prohlášení
a	a	k8xC	a
tajný	tajný	k2eAgInSc4d1	tajný
dodatečný	dodatečný	k2eAgInSc4d1	dodatečný
protokol	protokol	k1gInSc4	protokol
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
rozdělení	rozdělení	k1gNnSc1	rozdělení
sfér	sféra	k1gFnPc2	sféra
zájmů	zájem	k1gInPc2	zájem
dojednané	dojednaná	k1gFnSc2	dojednaná
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgInPc7	dva
totalitními	totalitní	k2eAgInPc7d1	totalitní
státy	stát	k1gInPc7	stát
(	(	kIx(	(
<g/>
zmiňování	zmiňování	k1gNnSc3	zmiňování
této	tento	k3xDgFnSc2	tento
historické	historický	k2eAgFnSc2d1	historická
skutečnosti	skutečnost	k1gFnSc2	skutečnost
bylo	být	k5eAaImAgNnS	být
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
SSSR	SSSR	kA	SSSR
trestným	trestný	k2eAgInSc7d1	trestný
činem	čin	k1gInSc7	čin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hitlerovo	Hitlerův	k2eAgNnSc1d1	Hitlerovo
vojenské	vojenský	k2eAgNnSc1d1	vojenské
řízení	řízení	k1gNnSc1	řízení
válečných	válečný	k2eAgFnPc2d1	válečná
operací	operace	k1gFnPc2	operace
bylo	být	k5eAaImAgNnS	být
méně	málo	k6eAd2	málo
úspěšné	úspěšný	k2eAgInPc4d1	úspěšný
než	než	k8xS	než
jeho	jeho	k3xOp3gFnSc1	jeho
politická	politický	k2eAgFnSc1d1	politická
aktivita	aktivita	k1gFnSc1	aktivita
a	a	k8xC	a
některé	některý	k3yIgFnPc1	některý
jeho	jeho	k3xOp3gFnPc1	jeho
chyby	chyba	k1gFnPc1	chyba
se	se	k3xPyFc4	se
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
projevily	projevit	k5eAaPmAgInP	projevit
zásadním	zásadní	k2eAgInSc7d1	zásadní
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
20	[number]	k4	20
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1944	[number]	k4	1944
jen	jen	k9	jen
těsně	těsně	k6eAd1	těsně
unikl	uniknout	k5eAaPmAgMnS	uniknout
atentátu	atentát	k1gInSc2	atentát
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Atentát	atentát	k1gInSc4	atentát
na	na	k7c6	na
Hitlera	Hitler	k1gMnSc2	Hitler
z	z	k7c2	z
20	[number]	k4	20
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
20	[number]	k4	20
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1945	[number]	k4	1945
měl	mít	k5eAaImAgInS	mít
56	[number]	k4	56
<g/>
.	.	kIx.	.
narozeniny	narozeniny	k1gFnPc4	narozeniny
a	a	k8xC	a
o	o	k7c4	o
deset	deset	k4xCc4	deset
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
spáchal	spáchat	k5eAaPmAgInS	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
rozkousnutím	rozkousnutí	k1gNnSc7	rozkousnutí
ampulky	ampulka	k1gFnSc2	ampulka
kyanidu	kyanid	k1gInSc2	kyanid
při	při	k7c6	při
současném	současný	k2eAgNnSc6d1	současné
stisknutí	stisknutí	k1gNnSc6	stisknutí
spouště	spoušť	k1gFnSc2	spoušť
pistole	pistol	k1gFnSc2	pistol
(	(	kIx(	(
<g/>
Walther	Walthra	k1gFnPc2	Walthra
PPK	PPK	kA	PPK
<g/>
)	)	kIx)	)
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
bunkru	bunkr	k1gInSc6	bunkr
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
Těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
smrtí	smrt	k1gFnSc7	smrt
měl	mít	k5eAaImAgMnS	mít
zakřičet	zakřičet	k5eAaPmF	zakřičet
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Bez	bez	k7c2	bez
nacionálního	nacionální	k2eAgInSc2d1	nacionální
socialismu	socialismus	k1gInSc2	socialismus
nemůže	moct	k5eNaImIp3nS	moct
Německo	Německo	k1gNnSc1	Německo
existovat	existovat	k5eAaImF	existovat
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
Jeho	jeho	k3xOp3gFnSc1	jeho
novomanželka	novomanželka	k1gFnSc1	novomanželka
Eva	Eva	k1gFnSc1	Eva
Braunová	Braunová	k1gFnSc1	Braunová
spáchala	spáchat	k5eAaPmAgFnS	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
před	před	k7c7	před
ním	on	k3xPp3gInSc7	on
kyanidovou	kyanidový	k2eAgFnSc7d1	kyanidová
kapslí	kapsle	k1gFnSc7	kapsle
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnPc1	jejich
těla	tělo	k1gNnPc1	tělo
byla	být	k5eAaImAgNnP	být
potom	potom	k6eAd1	potom
vynesena	vynést	k5eAaPmNgNnP	vynést
do	do	k7c2	do
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
kráterů	kráter	k1gInPc2	kráter
před	před	k7c7	před
bunkrem	bunkr	k1gInSc7	bunkr
<g/>
,	,	kIx,	,
polita	polit	k2eAgFnSc1d1	polita
benzínem	benzín	k1gInSc7	benzín
a	a	k8xC	a
spálena	spálen	k2eAgNnPc1d1	spáleno
<g/>
.	.	kIx.	.
</s>
<s>
Německé	německý	k2eAgFnSc3d1	německá
armádě	armáda	k1gFnSc3	armáda
na	na	k7c6	na
konci	konec	k1gInSc6	konec
války	válka	k1gFnSc2	válka
docházelo	docházet	k5eAaImAgNnS	docházet
palivo	palivo	k1gNnSc1	palivo
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
musel	muset	k5eAaImAgMnS	muset
dát	dát	k5eAaPmF	dát
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
Hitlerových	Hitlerových	k2eAgMnPc2d1	Hitlerových
generálů	generál	k1gMnPc2	generál
rozkaz	rozkaz	k1gInSc1	rozkaz
k	k	k7c3	k
vyčerpání	vyčerpání	k1gNnSc3	vyčerpání
paliva	palivo	k1gNnSc2	palivo
z	z	k7c2	z
aut	auto	k1gNnPc2	auto
na	na	k7c6	na
parkovištích	parkoviště	k1gNnPc6	parkoviště
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
těla	tělo	k1gNnSc2	tělo
Adolfa	Adolf	k1gMnSc2	Adolf
a	a	k8xC	a
Evy	Eva	k1gFnSc2	Eva
Hitlerových	Hitlerová	k1gFnPc2	Hitlerová
spálena	spálit	k5eAaPmNgFnS	spálit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
ze	z	k7c2	z
7	[number]	k4	7
<g/>
.	.	kIx.	.
na	na	k7c4	na
8	[number]	k4	8
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
dobyly	dobýt	k5eAaPmAgFnP	dobýt
Berlín	Berlín	k1gInSc4	Berlín
jednotky	jednotka	k1gFnSc2	jednotka
sovětské	sovětský	k2eAgFnSc2d1	sovětská
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
7	[number]	k4	7
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
byla	být	k5eAaImAgFnS	být
podepsána	podepsán	k2eAgFnSc1d1	podepsána
kapitulace	kapitulace	k1gFnSc1	kapitulace
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vešla	vejít	k5eAaPmAgFnS	vejít
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
druhého	druhý	k4xOgInSc2	druhý
dne	den	k1gInSc2	den
ve	v	k7c6	v
23	[number]	k4	23
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhá	Dlouhá	k1gFnSc1	Dlouhá
léta	léto	k1gNnSc2	léto
se	se	k3xPyFc4	se
vedly	vést	k5eAaImAgInP	vést
spory	spor	k1gInPc1	spor
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
Hitler	Hitler	k1gMnSc1	Hitler
skutečně	skutečně	k6eAd1	skutečně
spáchal	spáchat	k5eAaPmAgMnS	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
,	,	kIx,	,
či	či	k8xC	či
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
přece	přece	k9	přece
jen	jen	k6eAd1	jen
nepodařilo	podařit	k5eNaPmAgNnS	podařit
z	z	k7c2	z
obklíčeného	obklíčený	k2eAgInSc2d1	obklíčený
Berlína	Berlín	k1gInSc2	Berlín
uprchnout	uprchnout	k5eAaPmF	uprchnout
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgInPc1d1	dnešní
moderní	moderní	k2eAgInPc1d1	moderní
poznatky	poznatek	k1gInPc1	poznatek
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Hitler	Hitler	k1gMnSc1	Hitler
skutečně	skutečně	k6eAd1	skutečně
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
zemřel	zemřít	k5eAaPmAgMnS	zemřít
a	a	k8xC	a
že	že	k8xS	že
byly	být	k5eAaImAgInP	být
jeho	jeho	k3xOp3gInPc1	jeho
ostatky	ostatek	k1gInPc1	ostatek
částečně	částečně	k6eAd1	částečně
spáleny	spálen	k2eAgInPc1d1	spálen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
archivech	archiv	k1gInPc6	archiv
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
byly	být	k5eAaImAgFnP	být
zachovány	zachován	k2eAgInPc1d1	zachován
pouze	pouze	k6eAd1	pouze
části	část	k1gFnPc1	část
lebeční	lebeční	k2eAgFnSc2d1	lebeční
kosti	kost	k1gFnSc2	kost
a	a	k8xC	a
čelisti	čelist	k1gFnSc2	čelist
<g/>
.	.	kIx.	.
</s>
<s>
Výzkum	výzkum	k1gInSc1	výzkum
lebeční	lebeční	k2eAgFnSc2d1	lebeční
kosti	kost	k1gFnSc2	kost
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
odhalil	odhalit	k5eAaPmAgInS	odhalit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nepatřila	patřit	k5eNaImAgFnS	patřit
Hitlerovi	Hitler	k1gMnSc3	Hitler
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ženě	žena	k1gFnSc3	žena
mezi	mezi	k7c4	mezi
20	[number]	k4	20
a	a	k8xC	a
40	[number]	k4	40
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Zuby	zub	k1gInPc1	zub
v	v	k7c6	v
čelistní	čelistní	k2eAgFnSc6d1	čelistní
kosti	kost	k1gFnSc6	kost
však	však	k9	však
odpovídaly	odpovídat	k5eAaImAgFnP	odpovídat
Hitlerovým	Hitlerův	k2eAgInPc3d1	Hitlerův
stomatologickým	stomatologický	k2eAgInPc3d1	stomatologický
záznamům	záznam	k1gInPc3	záznam
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
politické	politický	k2eAgInPc1d1	politický
názory	názor	k1gInPc1	názor
byly	být	k5eAaImAgInP	být
ovlivněny	ovlivnit	k5eAaPmNgInP	ovlivnit
starší	starý	k2eAgInPc1d2	starší
německou	německý	k2eAgFnSc7d1	německá
literaturou	literatura	k1gFnSc7	literatura
a	a	k8xC	a
filozofií	filozofie	k1gFnSc7	filozofie
(	(	kIx(	(
<g/>
Herder	Herder	k1gMnSc1	Herder
<g/>
,	,	kIx,	,
Arndt	Arndt	k1gMnSc1	Arndt
<g/>
)	)	kIx)	)
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
sociálním	sociální	k2eAgInSc7d1	sociální
darwinismem	darwinismus	k1gInSc7	darwinismus
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
pokoušel	pokoušet	k5eAaImAgInS	pokoušet
o	o	k7c4	o
aplikaci	aplikace	k1gFnSc4	aplikace
Darwinových	Darwinových	k2eAgInPc2d1	Darwinových
poznatků	poznatek	k1gInPc2	poznatek
o	o	k7c6	o
přirozeném	přirozený	k2eAgInSc6d1	přirozený
vývoji	vývoj	k1gInSc6	vývoj
druhů	druh	k1gInPc2	druh
postupnou	postupný	k2eAgFnSc7d1	postupná
eliminací	eliminace	k1gFnSc7	eliminace
nejslabších	slabý	k2eAgMnPc2d3	nejslabší
i	i	k8xC	i
na	na	k7c4	na
národnostní	národnostní	k2eAgFnSc4d1	národnostní
otázku	otázka	k1gFnSc4	otázka
<g/>
.	.	kIx.	.
</s>
<s>
Hitler	Hitler	k1gMnSc1	Hitler
opovrhoval	opovrhovat	k5eAaImAgInS	opovrhovat
liberálním	liberální	k2eAgNnSc7d1	liberální
uspořádáním	uspořádání	k1gNnSc7	uspořádání
státu	stát	k1gInSc2	stát
a	a	k8xC	a
zdůrazňoval	zdůrazňovat	k5eAaImAgMnS	zdůrazňovat
moc	moc	k6eAd1	moc
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
typický	typický	k2eAgMnSc1d1	typický
demagog	demagog	k1gMnSc1	demagog
a	a	k8xC	a
populista	populista	k1gMnSc1	populista
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
projevech	projev	k1gInPc6	projev
sliboval	slibovat	k5eAaImAgMnS	slibovat
všem	všecek	k3xTgNnPc3	všecek
vrstvám	vrstva	k1gFnPc3	vrstva
společnosti	společnost	k1gFnSc6	společnost
cokoliv	cokoliv	k3yInSc4	cokoliv
<g/>
,	,	kIx,	,
jen	jen	k9	jen
aby	aby	kYmCp3nS	aby
získal	získat	k5eAaPmAgInS	získat
jejich	jejich	k3xOp3gInPc4	jejich
hlasy	hlas	k1gInPc4	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
sice	sice	k8xC	sice
vůdcem	vůdce	k1gMnSc7	vůdce
nacionálně-socialistické	nacionálněocialistický	k2eAgFnSc2d1	nacionálně-socialistická
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
k	k	k7c3	k
socialismu	socialismus	k1gInSc3	socialismus
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
tradičně	tradičně	k6eAd1	tradičně
chápán	chápat	k5eAaImNgInS	chápat
a	a	k8xC	a
jak	jak	k6eAd1	jak
ho	on	k3xPp3gMnSc4	on
vymezili	vymezit	k5eAaPmAgMnP	vymezit
radikální	radikální	k2eAgMnPc1d1	radikální
myslitelé	myslitel	k1gMnPc1	myslitel
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nehlásil	hlásit	k5eNaImAgMnS	hlásit
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Kdokoliv	kdokoliv	k3yInSc1	kdokoliv
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
je	být	k5eAaImIp3nS	být
připraven	připraven	k2eAgMnSc1d1	připraven
učinit	učinit	k5eAaPmF	učinit
z	z	k7c2	z
národní	národní	k2eAgFnSc2d1	národní
otázky	otázka	k1gFnSc2	otázka
otázku	otázka	k1gFnSc4	otázka
svou	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastnit	k5eAaImIp3nP	vlastnit
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
<g />
.	.	kIx.	.
</s>
<s>
míry	míra	k1gFnSc2	míra
<g/>
,	,	kIx,	,
že	že	k8xS	že
nezná	neznat	k5eAaImIp3nS	neznat
žádný	žádný	k3yNgInSc4	žádný
vyšší	vysoký	k2eAgInSc4d2	vyšší
ideál	ideál	k1gInSc4	ideál
než	než	k8xS	než
blaho	blaho	k1gNnSc4	blaho
svého	svůj	k3xOyFgInSc2	svůj
národa	národ	k1gInSc2	národ
<g/>
;	;	kIx,	;
kdokoliv	kdokoliv	k3yInSc1	kdokoliv
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
porozuměl	porozumět	k5eAaPmAgInS	porozumět
naší	náš	k3xOp1gFnSc6	náš
skvělé	skvělý	k2eAgFnSc6d1	skvělá
národní	národní	k2eAgFnSc6d1	národní
hymně	hymna	k1gFnSc6	hymna
"	"	kIx"	"
<g/>
Deutschland	Deutschland	k1gInSc1	Deutschland
über	über	k1gMnSc1	über
Alles	Alles	k1gMnSc1	Alles
<g/>
"	"	kIx"	"
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
smyslu	smysl	k1gInSc6	smysl
<g/>
,	,	kIx,	,
že	že	k8xS	že
nic	nic	k3yNnSc1	nic
na	na	k7c6	na
světě	svět	k1gInSc6	svět
nezastíní	zastínit	k5eNaPmIp3nS	zastínit
v	v	k7c6	v
jeho	jeho	k3xOp3gNnPc6	jeho
očích	oko	k1gNnPc6	oko
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc1	jeho
lid	lid	k1gInSc4	lid
a	a	k8xC	a
zemi	zem	k1gFnSc4	zem
-	-	kIx~	-
ten	ten	k3xDgMnSc1	ten
je	být	k5eAaImIp3nS	být
socialistou	socialista	k1gMnSc7	socialista
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Hitlerův	Hitlerův	k2eAgInSc1d1	Hitlerův
projev	projev	k1gInSc1	projev
z	z	k7c2	z
28	[number]	k4	28
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
Röhmovy	Röhmův	k2eAgFnSc2d1	Röhmův
snahy	snaha	k1gFnSc2	snaha
o	o	k7c4	o
socialistickou	socialistický	k2eAgFnSc4d1	socialistická
revoluci	revoluce	k1gFnSc4	revoluce
rázně	rázně	k6eAd1	rázně
ukončil	ukončit	k5eAaPmAgInS	ukončit
masakrem	masakr	k1gInSc7	masakr
ve	v	k7c4	v
vedení	vedení	k1gNnSc4	vedení
NSDAP	NSDAP	kA	NSDAP
a	a	k8xC	a
SA	SA	kA	SA
za	za	k7c4	za
Noci	noc	k1gFnPc4	noc
dlouhých	dlouhý	k2eAgInPc2d1	dlouhý
nožů	nůž	k1gInPc2	nůž
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1934	[number]	k4	1934
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
věcí	věc	k1gFnPc2	věc
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
nacisté	nacista	k1gMnPc1	nacista
udělali	udělat	k5eAaPmAgMnP	udělat
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
dostali	dostat	k5eAaPmAgMnP	dostat
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
zrušení	zrušení	k1gNnSc4	zrušení
odborů	odbor	k1gInPc2	odbor
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
po	po	k7c4	po
léta	léto	k1gNnPc4	léto
přijímal	přijímat	k5eAaImAgInS	přijímat
velikou	veliký	k2eAgFnSc4d1	veliká
finanční	finanční	k2eAgFnSc4d1	finanční
podporu	podpora	k1gFnSc4	podpora
velkoprůmyslníků	velkoprůmyslník	k1gMnPc2	velkoprůmyslník
<g/>
,	,	kIx,	,
bankéřů	bankéř	k1gMnPc2	bankéř
a	a	k8xC	a
junkerů	junker	k1gMnPc2	junker
a	a	k8xC	a
sliboval	slibovat	k5eAaImAgInS	slibovat
ochranu	ochrana	k1gFnSc4	ochrana
jejich	jejich	k3xOp3gInSc2	jejich
majetku	majetek	k1gInSc2	majetek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
myšlení	myšlení	k1gNnSc6	myšlení
převládají	převládat	k5eAaImIp3nP	převládat
dva	dva	k4xCgInPc4	dva
zásadní	zásadní	k2eAgInPc4d1	zásadní
prvky	prvek	k1gInPc4	prvek
<g/>
,	,	kIx,	,
silný	silný	k2eAgInSc4d1	silný
antisemitismus	antisemitismus	k1gInSc4	antisemitismus
a	a	k8xC	a
nacionalismus	nacionalismus	k1gInSc4	nacionalismus
<g/>
.	.	kIx.	.
</s>
<s>
Primitivní	primitivní	k2eAgInSc4d1	primitivní
antisemitismus	antisemitismus	k1gInSc4	antisemitismus
mu	on	k3xPp3gMnSc3	on
pomáhal	pomáhat	k5eAaImAgMnS	pomáhat
vytvářet	vytvářet	k5eAaImF	vytvářet
obraz	obraz	k1gInSc4	obraz
veřejného	veřejný	k2eAgMnSc2d1	veřejný
nepřítele	nepřítel	k1gMnSc2	nepřítel
a	a	k8xC	a
vyvrcholil	vyvrcholit	k5eAaPmAgInS	vyvrcholit
holokaustem	holokaust	k1gInSc7	holokaust
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
vyhlazením	vyhlazení	k1gNnSc7	vyhlazení
mj.	mj.	kA	mj.
6	[number]	k4	6
milionů	milion	k4xCgInPc2	milion
evropských	evropský	k2eAgMnPc2d1	evropský
Židů	Žid	k1gMnPc2	Žid
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
extatický	extatický	k2eAgInSc1d1	extatický
nacionalismus	nacionalismus	k1gInSc1	nacionalismus
přitahoval	přitahovat	k5eAaImAgInS	přitahovat
pravicově	pravicově	k6eAd1	pravicově
smýšlející	smýšlející	k2eAgFnSc4d1	smýšlející
část	část	k1gFnSc4	část
německé	německý	k2eAgFnSc2d1	německá
společnosti	společnost	k1gFnSc2	společnost
odchované	odchovaný	k2eAgFnSc2d1	odchovaná
"	"	kIx"	"
<g/>
pruským	pruský	k2eAgNnSc7d1	pruské
myšlením	myšlení	k1gNnSc7	myšlení
<g/>
"	"	kIx"	"
a	a	k8xC	a
vyvrcholil	vyvrcholit	k5eAaPmAgInS	vyvrcholit
druhou	druhý	k4xOgFnSc7	druhý
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
<g/>
.	.	kIx.	.
</s>
<s>
Hitler	Hitler	k1gMnSc1	Hitler
považoval	považovat	k5eAaImAgMnS	považovat
Židy	Žid	k1gMnPc4	Žid
a	a	k8xC	a
slovanské	slovanský	k2eAgInPc4d1	slovanský
národy	národ	k1gInPc4	národ
za	za	k7c4	za
podlidi	podčlověk	k1gMnPc4	podčlověk
a	a	k8xC	a
nacistický	nacistický	k2eAgInSc1d1	nacistický
plán	plán	k1gInSc1	plán
genocidy	genocida	k1gFnSc2	genocida
Generalplan	Generalplan	k1gMnSc1	Generalplan
Ost	Ost	k1gMnSc1	Ost
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	s	k7c7	s
souhlasem	souhlas	k1gInSc7	souhlas
Hitlera	Hitler	k1gMnSc2	Hitler
vypracoval	vypracovat	k5eAaPmAgMnS	vypracovat
říšský	říšský	k2eAgMnSc1d1	říšský
vedoucí	vedoucí	k1gMnSc1	vedoucí
SS	SS	kA	SS
Heinrich	Heinrich	k1gMnSc1	Heinrich
Himmler	Himmler	k1gMnSc1	Himmler
<g/>
,	,	kIx,	,
počítal	počítat	k5eAaImAgInS	počítat
po	po	k7c6	po
vítězné	vítězný	k2eAgFnSc6d1	vítězná
válce	válka	k1gFnSc6	válka
proti	proti	k7c3	proti
SSSR	SSSR	kA	SSSR
s	s	k7c7	s
vystěhováním	vystěhování	k1gNnSc7	vystěhování
na	na	k7c4	na
východ	východ	k1gInSc4	východ
za	za	k7c4	za
Ural	Ural	k1gInSc4	Ural
nebo	nebo	k8xC	nebo
s	s	k7c7	s
fyzickou	fyzický	k2eAgFnSc7d1	fyzická
likvidací	likvidace	k1gFnSc7	likvidace
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85	[number]	k4	85
%	%	kIx~	%
Poláků	Polák	k1gMnPc2	Polák
<g/>
,	,	kIx,	,
50	[number]	k4	50
<g/>
-	-	kIx~	-
<g/>
60	[number]	k4	60
%	%	kIx~	%
Rusů	Rus	k1gMnPc2	Rus
z	z	k7c2	z
evropské	evropský	k2eAgFnSc2d1	Evropská
části	část	k1gFnSc2	část
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
,	,	kIx,	,
50	[number]	k4	50
%	%	kIx~	%
Čechů	Čech	k1gMnPc2	Čech
<g/>
,	,	kIx,	,
65	[number]	k4	65
%	%	kIx~	%
Ukrajinců	Ukrajinec	k1gMnPc2	Ukrajinec
a	a	k8xC	a
75	[number]	k4	75
%	%	kIx~	%
Bělorusů	Bělorus	k1gMnPc2	Bělorus
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
zajistil	zajistit	k5eAaPmAgMnS	zajistit
životní	životní	k2eAgInSc4d1	životní
prostor	prostor	k1gInSc4	prostor
pro	pro	k7c4	pro
Němce	Němec	k1gMnPc4	Němec
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
Jiřího	Jiří	k1gMnSc2	Jiří
Hájka	Hájek	k1gMnSc2	Hájek
Hitler	Hitler	k1gMnSc1	Hitler
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
...	...	k?	...
sociální	sociální	k2eAgFnSc4d1	sociální
složku	složka	k1gFnSc4	složka
svého	svůj	k3xOyFgNnSc2	svůj
učení	učení	k1gNnSc2	učení
(	(	kIx(	(
<g/>
vytvoření	vytvoření	k1gNnSc1	vytvoření
zdravých	zdravý	k2eAgInPc2d1	zdravý
sociálních	sociální	k2eAgInPc2d1	sociální
poměrů	poměr	k1gInPc2	poměr
<g/>
)	)	kIx)	)
převzal	převzít	k5eAaPmAgInS	převzít
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
stojí	stát	k5eAaImIp3nS	stát
a	a	k8xC	a
leží	ležet	k5eAaImIp3nS	ležet
od	od	k7c2	od
zakladatele	zakladatel	k1gMnSc2	zakladatel
křesťansko-sociálního	křesťanskoociální	k2eAgNnSc2d1	křesťansko-sociální
hnutí	hnutí	k1gNnSc2	hnutí
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Karla	Karel	k1gMnSc2	Karel
Luegera	Lueger	k1gMnSc2	Lueger
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
však	však	k9	však
Luegera	Luegera	k1gFnSc1	Luegera
'	'	kIx"	'
<g/>
zdokonalil	zdokonalit	k5eAaPmAgInS	zdokonalit
<g/>
'	'	kIx"	'
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
koncepce	koncepce	k1gFnSc2	koncepce
sociální	sociální	k2eAgFnSc2d1	sociální
otázky	otázka	k1gFnSc2	otázka
důsledně	důsledně	k6eAd1	důsledně
vymýtil	vymýtit	k5eAaPmAgInS	vymýtit
všechny	všechen	k3xTgFnPc4	všechen
křesťanské	křesťanský	k2eAgFnPc4d1	křesťanská
motivace	motivace	k1gFnPc4	motivace
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
nahradil	nahradit	k5eAaPmAgInS	nahradit
nacionalisticko-antisemitským	nacionalistickontisemitský	k2eAgInSc7d1	nacionalisticko-antisemitský
programem	program	k1gInSc7	program
další	další	k2eAgNnSc4d1	další
jím	jíst	k5eAaImIp1nS	jíst
uznávané	uznávaný	k2eAgFnPc4d1	uznávaná
autority	autorita	k1gFnPc4	autorita
<g/>
,	,	kIx,	,
George	George	k1gInSc1	George
von	von	k1gInSc1	von
Schönerera	Schönerer	k1gMnSc2	Schönerer
<g/>
,	,	kIx,	,
zakladatele	zakladatel	k1gMnSc2	zakladatel
'	'	kIx"	'
<g/>
Všeněmeckého	všeněmecký	k2eAgNnSc2d1	všeněmecký
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
'	'	kIx"	'
a	a	k8xC	a
hnutí	hnutí	k1gNnSc2	hnutí
'	'	kIx"	'
<g/>
Pryč	pryč	k6eAd1	pryč
od	od	k7c2	od
Říma	Řím	k1gInSc2	Řím
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Program	program	k1gInSc1	program
národních	národní	k2eAgMnPc2d1	národní
socialistů	socialist	k1gMnPc2	socialist
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
byl	být	k5eAaImAgInS	být
i	i	k9	i
hodně	hodně	k6eAd1	hodně
socialistický	socialistický	k2eAgMnSc1d1	socialistický
a	a	k8xC	a
obsahoval	obsahovat	k5eAaImAgMnS	obsahovat
zrušení	zrušení	k1gNnSc4	zrušení
bezpracných	bezpracný	k2eAgInPc2d1	bezpracný
příjmů	příjem	k1gInPc2	příjem
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
konfiskaci	konfiskace	k1gFnSc3	konfiskace
válečných	válečný	k2eAgInPc2d1	válečný
zisků	zisk	k1gInPc2	zisk
<g/>
,	,	kIx,	,
znárodnění	znárodnění	k1gNnSc1	znárodnění
monopolů	monopol	k1gInPc2	monopol
<g/>
,	,	kIx,	,
rozdělení	rozdělení	k1gNnSc1	rozdělení
zisků	zisk	k1gInPc2	zisk
z	z	k7c2	z
monopolů	monopol	k1gInPc2	monopol
<g/>
,	,	kIx,	,
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
podporu	podpora	k1gFnSc4	podpora
ve	v	k7c6	v
stáří	stáří	k1gNnSc6	stáří
<g/>
,	,	kIx,	,
znárodnění	znárodnění	k1gNnSc6	znárodnění
obchodních	obchodní	k2eAgInPc2d1	obchodní
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
konfiskaci	konfiskace	k1gFnSc4	konfiskace
půdy	půda	k1gFnSc2	půda
pro	pro	k7c4	pro
veřejné	veřejný	k2eAgInPc4d1	veřejný
účely	účel	k1gInPc4	účel
bez	bez	k7c2	bez
náhrady	náhrada	k1gFnSc2	náhrada
<g/>
,	,	kIx,	,
zrušení	zrušení	k1gNnSc1	zrušení
úroků	úrok	k1gInPc2	úrok
za	za	k7c4	za
pozemkové	pozemkový	k2eAgFnPc4d1	pozemková
půjčky	půjčka	k1gFnPc4	půjčka
<g/>
,	,	kIx,	,
podporu	podpora	k1gFnSc4	podpora
nadaných	nadaný	k2eAgFnPc2d1	nadaná
dětí	dítě	k1gFnPc2	dítě
chudých	chudý	k2eAgMnPc2d1	chudý
rodičů	rodič	k1gMnPc2	rodič
<g/>
,	,	kIx,	,
povinné	povinný	k2eAgNnSc4d1	povinné
cvičení	cvičení	k1gNnSc4	cvičení
a	a	k8xC	a
sport	sport	k1gInSc4	sport
apod.	apod.	kA	apod.
Od	od	k7c2	od
tradičních	tradiční	k2eAgFnPc2d1	tradiční
forem	forma	k1gFnPc2	forma
socialismu	socialismus	k1gInSc2	socialismus
odlišoval	odlišovat	k5eAaImAgInS	odlišovat
národní	národní	k2eAgInSc1d1	národní
socialismus	socialismus	k1gInSc1	socialismus
vypjatý	vypjatý	k2eAgInSc1d1	vypjatý
nacionalismus	nacionalismus	k1gInSc1	nacionalismus
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
ne	ne	k9	ne
nenávist	nenávist	k1gFnSc4	nenávist
k	k	k7c3	k
některým	některý	k3yIgInPc3	některý
národům	národ	k1gInPc3	národ
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc7	který
projevoval	projevovat	k5eAaImAgMnS	projevovat
třeba	třeba	k6eAd1	třeba
i	i	k9	i
Karl	Karl	k1gMnSc1	Karl
Marx	Marx	k1gMnSc1	Marx
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
Hitlerova	Hitlerův	k2eAgMnSc2d1	Hitlerův
Mein	Mein	k1gInSc1	Mein
Kampfu	Kampf	k1gInSc2	Kampf
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
V	v	k7c4	v
rudé	rudý	k1gMnPc4	rudý
vidíme	vidět	k5eAaImIp1nP	vidět
socialismus	socialismus	k1gInSc4	socialismus
našeho	náš	k3xOp1gNnSc2	náš
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
,	,	kIx,	,
v	v	k7c4	v
bílé	bílý	k2eAgFnPc4d1	bílá
myšlenku	myšlenka	k1gFnSc4	myšlenka
nacionalismu	nacionalismus	k1gInSc2	nacionalismus
a	a	k8xC	a
ve	v	k7c6	v
svastice	svastika	k1gFnSc6	svastika
poslání	poslání	k1gNnSc2	poslání
boje	boj	k1gInSc2	boj
za	za	k7c4	za
vítězství	vítězství	k1gNnSc4	vítězství
árijského	árijský	k2eAgMnSc2d1	árijský
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Hitler	Hitler	k1gMnSc1	Hitler
chtěl	chtít	k5eAaImAgMnS	chtít
však	však	k9	však
zničit	zničit	k5eAaPmF	zničit
marxismus	marxismus	k1gInSc4	marxismus
a	a	k8xC	a
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mezi	mezi	k7c7	mezi
marxismem	marxismus	k1gInSc7	marxismus
a	a	k8xC	a
socialismem	socialismus	k1gInSc7	socialismus
existují	existovat	k5eAaImIp3nP	existovat
rozdíly	rozdíl	k1gInPc4	rozdíl
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1923	[number]	k4	1923
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
Hitler	Hitler	k1gMnSc1	Hitler
v	v	k7c6	v
interview	interview	k1gNnSc6	interview
s	s	k7c7	s
Georgem	Georg	k1gMnSc7	Georg
Sylvesterem	Sylvester	k1gMnSc7	Sylvester
Viereckem	Viereck	k1gInSc7	Viereck
<g/>
,	,	kIx,	,
že	že	k8xS	že
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Komunismus	komunismus	k1gInSc1	komunismus
není	být	k5eNaImIp3nS	být
socialismus	socialismus	k1gInSc1	socialismus
<g/>
.	.	kIx.	.
</s>
<s>
Marxismus	marxismus	k1gInSc1	marxismus
není	být	k5eNaImIp3nS	být
socialismus	socialismus	k1gInSc1	socialismus
<g/>
.	.	kIx.	.
...	...	k?	...
Socialismus	socialismus	k1gInSc1	socialismus
je	být	k5eAaImIp3nS	být
antický	antický	k2eAgInSc1d1	antický
Aryan	Aryan	k1gInSc1	Aryan
<g/>
,	,	kIx,	,
germánská	germánský	k2eAgFnSc1d1	germánská
instituce	instituce	k1gFnSc1	instituce
<g/>
.	.	kIx.	.
</s>
<s>
Naši	náš	k3xOp1gMnPc1	náš
germánští	germánský	k2eAgMnPc1d1	germánský
předci	předek	k1gMnPc1	předek
drželi	držet	k5eAaImAgMnP	držet
jistou	jistý	k2eAgFnSc4d1	jistá
půdu	půda	k1gFnSc4	půda
dohromady	dohromady	k6eAd1	dohromady
<g/>
.	.	kIx.	.
</s>
<s>
Oni	onen	k3xDgMnPc1	onen
kultivovali	kultivovat	k5eAaImAgMnP	kultivovat
ideu	idea	k1gFnSc4	idea
veřejného	veřejný	k2eAgNnSc2d1	veřejné
blaha	blaho	k1gNnSc2	blaho
<g/>
.	.	kIx.	.
...	...	k?	...
Socialismus	socialismus	k1gInSc1	socialismus
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
marxismu	marxismus	k1gInSc2	marxismus
nezavrhuje	zavrhovat	k5eNaImIp3nS	zavrhovat
soukromé	soukromý	k2eAgNnSc4d1	soukromé
vlastnictví	vlastnictví	k1gNnSc4	vlastnictví
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
marxismu	marxismus	k1gInSc2	marxismus
nezahrnuje	zahrnovat	k5eNaImIp3nS	zahrnovat
popření	popření	k1gNnSc4	popření
osobnosti	osobnost	k1gFnSc2	osobnost
a	a	k8xC	a
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
marxismu	marxismus	k1gInSc2	marxismus
je	být	k5eAaImIp3nS	být
patriotický	patriotický	k2eAgInSc1d1	patriotický
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Marxismus	marxismus	k1gInSc1	marxismus
však	však	k9	však
nepožaduje	požadovat	k5eNaImIp3nS	požadovat
zrušení	zrušení	k1gNnSc4	zrušení
soukromého	soukromý	k2eAgNnSc2d1	soukromé
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
spotřebních	spotřební	k2eAgInPc2d1	spotřební
statků	statek	k1gInPc2	statek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zrušení	zrušení	k1gNnSc1	zrušení
soukromého	soukromý	k2eAgNnSc2d1	soukromé
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
výrobních	výrobní	k2eAgInPc2d1	výrobní
statků	statek	k1gInPc2	statek
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1934	[number]	k4	1934
Hitler	Hitler	k1gMnSc1	Hitler
připustil	připustit	k5eAaPmAgMnS	připustit
<g/>
,	,	kIx,	,
že	že	k8xS	že
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Národní	národní	k2eAgInSc1d1	národní
socialismus	socialismus	k1gInSc1	socialismus
převzal	převzít	k5eAaPmAgInS	převzít
od	od	k7c2	od
každého	každý	k3xTgMnSc2	každý
z	z	k7c2	z
obou	dva	k4xCgInPc2	dva
táborů	tábor	k1gInPc2	tábor
samotnou	samotný	k2eAgFnSc4d1	samotná
myšlenku	myšlenka	k1gFnSc4	myšlenka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ho	on	k3xPp3gMnSc4	on
charakterizuje	charakterizovat	k5eAaBmIp3nS	charakterizovat
-	-	kIx~	-
národní	národní	k2eAgNnSc4d1	národní
odhodlání	odhodlání	k1gNnSc4	odhodlání
z	z	k7c2	z
buržoazní	buržoazní	k2eAgFnSc2d1	buržoazní
tradice	tradice	k1gFnSc2	tradice
a	a	k8xC	a
vitální	vitální	k2eAgInSc1d1	vitální
<g/>
,	,	kIx,	,
tvůrčí	tvůrčí	k2eAgInSc1d1	tvůrčí
socialismus	socialismus	k1gInSc1	socialismus
z	z	k7c2	z
marxistického	marxistický	k2eAgNnSc2d1	marxistické
učení	učení	k1gNnSc2	učení
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Po	po	k7c6	po
uchopení	uchopení	k1gNnSc6	uchopení
moci	moc	k1gFnSc2	moc
zůstala	zůstat	k5eAaPmAgFnS	zůstat
část	část	k1gFnSc1	část
hospodářství	hospodářství	k1gNnSc2	hospodářství
v	v	k7c6	v
soukromých	soukromý	k2eAgFnPc6d1	soukromá
rukou	ruka	k1gFnPc6	ruka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pod	pod	k7c7	pod
státní	státní	k2eAgFnSc7d1	státní
kontrolou	kontrola	k1gFnSc7	kontrola
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
čtyřletých	čtyřletý	k2eAgInPc2d1	čtyřletý
plánů	plán	k1gInPc2	plán
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
zavedena	zaveden	k2eAgFnSc1d1	zavedena
sociální	sociální	k2eAgNnSc4d1	sociální
opatření	opatření	k1gNnSc4	opatření
a	a	k8xC	a
podporován	podporován	k2eAgInSc4d1	podporován
socialistický	socialistický	k2eAgInSc4d1	socialistický
a	a	k8xC	a
rovnostářský	rovnostářský	k2eAgInSc4d1	rovnostářský
národní	národní	k2eAgInSc4d1	národní
étos	étos	k1gInSc4	étos
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
byli	být	k5eAaImAgMnP	být
neárijské	árijský	k2eNgFnPc4d1	neárijská
krve	krev	k1gFnPc4	krev
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgMnSc1d1	samotný
Hitler	Hitler	k1gMnSc1	Hitler
razil	razit	k5eAaImAgMnS	razit
heslo	heslo	k1gNnSc4	heslo
<g/>
:	:	kIx,	:
Rovnost	rovnost	k1gFnSc1	rovnost
všech	všecek	k3xTgMnPc2	všecek
rasových	rasový	k2eAgMnPc2d1	rasový
Němců	Němec	k1gMnPc2	Němec
<g/>
.	.	kIx.	.
</s>
<s>
Legální	legální	k2eAgInSc1d1	legální
rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
dělníkem	dělník	k1gMnSc7	dělník
a	a	k8xC	a
úředním	úřední	k2eAgInSc7d1	úřední
stavem	stav	k1gInSc7	stav
byl	být	k5eAaImAgInS	být
zrušen	zrušit	k5eAaPmNgInS	zrušit
a	a	k8xC	a
docházelo	docházet	k5eAaImAgNnS	docházet
ke	k	k7c3	k
glorifikaci	glorifikace	k1gFnSc3	glorifikace
dělníka	dělník	k1gMnSc2	dělník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
"	"	kIx"	"
<g/>
...	...	k?	...
politice	politika	k1gFnSc3	politika
plné	plný	k2eAgFnSc2d1	plná
zaměstnanosti	zaměstnanost	k1gFnSc2	zaměstnanost
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1933	[number]	k4	1933
a	a	k8xC	a
k	k	k7c3	k
hospodářské	hospodářský	k2eAgFnSc3d1	hospodářská
politice	politika	k1gFnSc3	politika
centrálního	centrální	k2eAgNnSc2d1	centrální
řízení	řízení	k1gNnSc2	řízení
hospodářského	hospodářský	k2eAgInSc2d1	hospodářský
procesu	proces	k1gInSc2	proces
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Přičemž	přičemž	k6eAd1	přičemž
rozdíl	rozdíl	k1gInSc1	rozdíl
například	například	k6eAd1	například
oproti	oproti	k7c3	oproti
Sovětskému	sovětský	k2eAgInSc3d1	sovětský
svazu	svaz	k1gInSc3	svaz
byl	být	k5eAaImAgInS	být
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Centrálně	centrálně	k6eAd1	centrálně
administrativní	administrativní	k2eAgNnSc4d1	administrativní
řízení	řízení	k1gNnSc4	řízení
hospodářského	hospodářský	k2eAgInSc2d1	hospodářský
procesu	proces	k1gInSc2	proces
může	moct	k5eAaImIp3nS	moct
sice	sice	k8xC	sice
být	být	k5eAaImF	být
spojeno	spojen	k2eAgNnSc1d1	spojeno
s	s	k7c7	s
kolektivním	kolektivní	k2eAgNnSc7d1	kolektivní
vlastnictvím	vlastnictví	k1gNnSc7	vlastnictví
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc1	tento
spojení	spojení	k1gNnSc1	spojení
však	však	k9	však
není	být	k5eNaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
...	...	k?	...
převládalo	převládat	k5eAaImAgNnS	převládat
dále	daleko	k6eAd2	daleko
soukromé	soukromý	k2eAgNnSc1d1	soukromé
vlastnictví	vlastnictví	k1gNnSc1	vlastnictví
výrobních	výrobní	k2eAgInPc2d1	výrobní
prostředků	prostředek	k1gInPc2	prostředek
<g/>
,	,	kIx,	,
zemědělské	zemědělský	k2eAgInPc4d1	zemědělský
a	a	k8xC	a
průmyslové	průmyslový	k2eAgInPc4d1	průmyslový
podniky	podnik	k1gInPc4	podnik
nadále	nadále	k6eAd1	nadále
patřily	patřit	k5eAaImAgInP	patřit
převážně	převážně	k6eAd1	převážně
soukromým	soukromý	k2eAgFnPc3d1	soukromá
osobám	osoba	k1gFnPc3	osoba
a	a	k8xC	a
společnostem	společnost	k1gFnPc3	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Soukromí	soukromý	k2eAgMnPc1d1	soukromý
vlastníci	vlastník	k1gMnPc1	vlastník
ale	ale	k8xC	ale
mohli	moct	k5eAaImAgMnP	moct
s	s	k7c7	s
výrobními	výrobní	k2eAgInPc7d1	výrobní
prostředky	prostředek	k1gInPc7	prostředek
nakládat	nakládat	k5eAaImF	nakládat
jen	jen	k9	jen
v	v	k7c6	v
omezeném	omezený	k2eAgInSc6d1	omezený
rozsahu	rozsah	k1gInSc6	rozsah
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
"	"	kIx"	"
<g/>
se	se	k3xPyFc4	se
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
mocenské	mocenský	k2eAgFnSc2d1	mocenská
organizace	organizace	k1gFnSc2	organizace
<g/>
,	,	kIx,	,
soukromé	soukromý	k2eAgFnSc2d1	soukromá
i	i	k8xC	i
veřejnoprávní	veřejnoprávní	k2eAgFnSc2d1	veřejnoprávní
<g/>
,	,	kIx,	,
navzájem	navzájem	k6eAd1	navzájem
prolnuly	prolnout	k5eAaPmAgInP	prolnout
<g/>
.	.	kIx.	.
...	...	k?	...
Německý	německý	k2eAgInSc1d1	německý
a	a	k8xC	a
ruský	ruský	k2eAgInSc1d1	ruský
hospodářský	hospodářský	k2eAgInSc1d1	hospodářský
řád	řád	k1gInSc1	řád
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1942	[number]	k4	1942
by	by	kYmCp3nP	by
potom	potom	k6eAd1	potom
byl	být	k5eAaImAgInS	být
řádem	řád	k1gInSc7	řád
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
dominovaly	dominovat	k5eAaImAgInP	dominovat
monopoly	monopol	k1gInPc1	monopol
ve	v	k7c6	v
všech	všecek	k3xTgNnPc6	všecek
výrobních	výrobní	k2eAgNnPc6d1	výrobní
odvětvích	odvětví	k1gNnPc6	odvětví
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
