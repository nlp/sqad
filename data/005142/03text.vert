<p>
<s>
Sluneční	sluneční	k2eAgFnSc1d1	sluneční
soustava	soustava	k1gFnSc1	soustava
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
Pravidel	pravidlo	k1gNnPc2	pravidlo
českého	český	k2eAgInSc2d1	český
pravopisu	pravopis	k1gInSc2	pravopis
psáno	psát	k5eAaImNgNnS	psát
s	s	k7c7	s
malým	malý	k2eAgInSc7d1	malý
s	s	k7c7	s
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
sluneční	sluneční	k2eAgFnSc1d1	sluneční
soustava	soustava	k1gFnSc1	soustava
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
Česká	český	k2eAgFnSc1d1	Česká
astronomická	astronomický	k2eAgFnSc1d1	astronomická
společnost	společnost	k1gFnSc1	společnost
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
psaní	psaní	k1gNnSc4	psaní
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
S	s	k7c7	s
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
Sluneční	sluneční	k2eAgFnSc1d1	sluneční
soustava	soustava	k1gFnSc1	soustava
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
planetární	planetární	k2eAgInSc1d1	planetární
systém	systém	k1gInSc1	systém
hvězdy	hvězda	k1gFnSc2	hvězda
známé	známý	k2eAgFnPc4d1	známá
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
planeta	planeta	k1gFnSc1	planeta
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Systém	systém	k1gInSc1	systém
tvoří	tvořit	k5eAaImIp3nS	tvořit
především	především	k9	především
8	[number]	k4	8
planet	planeta	k1gFnPc2	planeta
<g/>
,	,	kIx,	,
5	[number]	k4	5
trpasličích	trpasličí	k2eAgFnPc2d1	trpasličí
planet	planeta	k1gFnPc2	planeta
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
150	[number]	k4	150
měsíců	měsíc	k1gInPc2	měsíc
planet	planeta	k1gFnPc2	planeta
(	(	kIx(	(
<g/>
především	především	k9	především
u	u	k7c2	u
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
,	,	kIx,	,
Saturnu	Saturn	k1gInSc2	Saturn
<g/>
,	,	kIx,	,
Uranu	Uran	k1gInSc2	Uran
a	a	k8xC	a
Neptuna	Neptun	k1gMnSc2	Neptun
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgNnPc1d1	další
menší	malý	k2eAgNnPc1d2	menší
tělesa	těleso	k1gNnPc1	těleso
jako	jako	k8xS	jako
planetky	planetka	k1gFnPc1	planetka
<g/>
,	,	kIx,	,
komety	kometa	k1gFnPc1	kometa
<g/>
,	,	kIx,	,
meteoroidy	meteoroida	k1gFnPc1	meteoroida
apod.	apod.	kA	apod.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Planety	planeta	k1gFnPc1	planeta
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Planety	planeta	k1gFnPc1	planeta
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
obíhají	obíhat	k5eAaImIp3nP	obíhat
po	po	k7c6	po
eliptických	eliptický	k2eAgFnPc6d1	eliptická
drahách	draha	k1gFnPc6	draha
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
společném	společný	k2eAgNnSc6d1	společné
ohnisku	ohnisko	k1gNnSc6	ohnisko
oběžných	oběžný	k2eAgFnPc2d1	oběžná
elips	elipsa	k1gFnPc2	elipsa
<g/>
.	.	kIx.	.
</s>
<s>
Přesněji	přesně	k6eAd2	přesně
řečeno	říct	k5eAaPmNgNnS	říct
<g/>
,	,	kIx,	,
obíhají	obíhat	k5eAaImIp3nP	obíhat
kolem	kolem	k7c2	kolem
barycentra	barycentrum	k1gNnSc2	barycentrum
(	(	kIx(	(
<g/>
těžiště	těžiště	k1gNnSc1	těžiště
<g/>
)	)	kIx)	)
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tělese	těleso	k1gNnSc6	těleso
Slunce	slunce	k1gNnSc2	slunce
nebo	nebo	k8xC	nebo
jeho	jeho	k3xOp3gFnSc2	jeho
blízkosti	blízkost	k1gFnSc2	blízkost
<g/>
.	.	kIx.	.
</s>
<s>
Měsíce	měsíc	k1gInPc1	měsíc
obíhají	obíhat	k5eAaImIp3nP	obíhat
kolem	kolem	k7c2	kolem
planet	planeta	k1gFnPc2	planeta
také	také	k9	také
po	po	k7c6	po
eliptických	eliptický	k2eAgFnPc6d1	eliptická
drahách	draha	k1gFnPc6	draha
<g/>
.	.	kIx.	.
</s>
<s>
Dráhy	dráha	k1gFnPc1	dráha
nejsou	být	k5eNaImIp3nP	být
dokonale	dokonale	k6eAd1	dokonale
eliptické	eliptický	k2eAgFnPc1d1	eliptická
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
tělesa	těleso	k1gNnPc1	těleso
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
se	se	k3xPyFc4	se
vzájemně	vzájemně	k6eAd1	vzájemně
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
počítat	počítat	k5eAaImF	počítat
s	s	k7c7	s
relativistickými	relativistický	k2eAgInPc7d1	relativistický
efekty	efekt	k1gInPc7	efekt
<g/>
,	,	kIx,	,
především	především	k9	především
blízko	blízko	k7c2	blízko
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sluneční	sluneční	k2eAgFnSc1d1	sluneční
soustava	soustava	k1gFnSc1	soustava
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
Galaxie	galaxie	k1gFnSc2	galaxie
nepřesně	přesně	k6eNd1	přesně
nazývané	nazývaný	k2eAgInPc1d1	nazývaný
Mléčná	mléčný	k2eAgFnSc1d1	mléčná
dráha	dráha	k1gFnSc1	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
dále	daleko	k6eAd2	daleko
součástí	součást	k1gFnSc7	součást
tzv.	tzv.	kA	tzv.
Místní	místní	k2eAgFnSc2d1	místní
skupiny	skupina	k1gFnSc2	skupina
galaxií	galaxie	k1gFnPc2	galaxie
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
patří	patřit	k5eAaImIp3nP	patřit
galaxie	galaxie	k1gFnPc1	galaxie
M	M	kA	M
31	[number]	k4	31
v	v	k7c6	v
Andromedě	Andromed	k1gInSc6	Andromed
a	a	k8xC	a
přes	přes	k7c4	přes
30	[number]	k4	30
dalších	další	k2eAgInPc2d1	další
menších	malý	k2eAgInPc2d2	menší
vesmírných	vesmírný	k2eAgInPc2d1	vesmírný
objektů	objekt	k1gInPc2	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgFnSc1d1	místní
skupina	skupina	k1gFnSc1	skupina
galaxií	galaxie	k1gFnPc2	galaxie
pak	pak	k6eAd1	pak
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
Místní	místní	k2eAgFnSc2d1	místní
nadkupy	nadkupa	k1gFnSc2	nadkupa
galaxií	galaxie	k1gFnPc2	galaxie
(	(	kIx(	(
<g/>
Supergalaxie	supergalaxie	k1gFnSc1	supergalaxie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zhruba	zhruba	k6eAd1	zhruba
99,866	[number]	k4	99,866
%	%	kIx~	%
celkové	celkový	k2eAgFnSc2d1	celková
hmotnosti	hmotnost	k1gFnSc2	hmotnost
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
tvoří	tvořit	k5eAaImIp3nS	tvořit
samo	sám	k3xTgNnSc1	sám
Slunce	slunce	k1gNnSc1	slunce
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
svou	svůj	k3xOyFgFnSc7	svůj
gravitační	gravitační	k2eAgFnSc7d1	gravitační
silou	síla	k1gFnSc7	síla
udržuje	udržovat	k5eAaImIp3nS	udržovat
soustavu	soustava	k1gFnSc4	soustava
pohromadě	pohromadě	k6eAd1	pohromadě
<g/>
.	.	kIx.	.
</s>
<s>
Zbylých	zbylý	k2eAgNnPc2d1	zbylé
0,133	[number]	k4	0,133
%	%	kIx~	%
připadá	připadat	k5eAaPmIp3nS	připadat
na	na	k7c4	na
planety	planeta	k1gFnPc4	planeta
a	a	k8xC	a
jiná	jiný	k2eAgNnPc4d1	jiné
tělesa	těleso	k1gNnPc4	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Soustava	soustava	k1gFnSc1	soustava
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
do	do	k7c2	do
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
přibližně	přibližně	k6eAd1	přibližně
2	[number]	k4	2
světelných	světelný	k2eAgNnPc2d1	světelné
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
pásmo	pásmo	k1gNnSc1	pásmo
komet	kometa	k1gFnPc2	kometa
do	do	k7c2	do
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
přibližně	přibližně	k6eAd1	přibližně
1 000	[number]	k4	1 000
astronomických	astronomický	k2eAgFnPc2d1	astronomická
jednotek	jednotka	k1gFnPc2	jednotka
AU	au	k0	au
<g/>
,	,	kIx,	,
planetární	planetární	k2eAgFnSc1d1	planetární
soustava	soustava	k1gFnSc1	soustava
50	[number]	k4	50
AU	au	k0	au
<g/>
.	.	kIx.	.
</s>
<s>
Soustava	soustava	k1gFnSc1	soustava
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
asi	asi	k9	asi
před	před	k7c7	před
5	[number]	k4	5
miliardami	miliarda	k4xCgFnPc7	miliarda
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
různé	různý	k2eAgInPc1d1	různý
zdroje	zdroj	k1gInPc1	zdroj
uvádějí	uvádět	k5eAaImIp3nP	uvádět
rozmezí	rozmezí	k1gNnSc4	rozmezí
4,55	[number]	k4	4,55
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
miliard	miliarda	k4xCgFnPc2	miliarda
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Planety	planeta	k1gFnPc1	planeta
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
Merkur	Merkur	k1gInSc1	Merkur
(	(	kIx(	(
<g/>
☿	☿	k?	☿
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Venuše	Venuše	k1gFnSc1	Venuše
(	(	kIx(	(
<g/>
♀	♀	k?	♀
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Země	země	k1gFnSc1	země
(	(	kIx(	(
<g/>
♁	♁	k?	♁
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mars	Mars	k1gInSc1	Mars
(	(	kIx(	(
<g/>
♂	♂	k?	♂
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jupiter	Jupiter	k1gMnSc1	Jupiter
(	(	kIx(	(
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Saturn	Saturn	k1gMnSc1	Saturn
(	(	kIx(	(
<g/>
♄	♄	k?	♄
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Uran	Uran	k1gInSc1	Uran
(	(	kIx(	(
<g/>
♅	♅	k?	♅
<g/>
/	/	kIx~	/
<g/>
)	)	kIx)	)
a	a	k8xC	a
Neptun	Neptun	k1gInSc1	Neptun
(	(	kIx(	(
<g/>
♆	♆	k?	♆
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prvních	první	k4xOgFnPc2	první
pět	pět	k4xCc1	pět
planet	planeta	k1gFnPc2	planeta
bylo	být	k5eAaImAgNnS	být
rozlišeno	rozlišit	k5eAaPmNgNnS	rozlišit
už	už	k9	už
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
objevení	objevení	k1gNnSc6	objevení
byly	být	k5eAaImAgFnP	být
mezi	mezi	k7c4	mezi
planety	planeta	k1gFnPc4	planeta
na	na	k7c4	na
čas	čas	k1gInSc4	čas
zařazeny	zařadit	k5eAaPmNgInP	zařadit
i	i	k9	i
Ceres	ceres	k1gInSc1	ceres
(	(	kIx(	(
<g/>
do	do	k7c2	do
1850	[number]	k4	1850
<g/>
)	)	kIx)	)
a	a	k8xC	a
Pluto	Pluto	k1gMnSc1	Pluto
(	(	kIx(	(
<g/>
do	do	k7c2	do
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
však	však	k9	však
nejsou	být	k5eNaImIp3nP	být
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
zónách	zóna	k1gFnPc6	zóna
dominantními	dominantní	k2eAgInPc7d1	dominantní
objekty	objekt	k1gInPc7	objekt
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
označovány	označovat	k5eAaImNgInP	označovat
jako	jako	k9	jako
trpasličí	trpasličí	k2eAgFnPc1d1	trpasličí
planety	planeta	k1gFnPc1	planeta
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
se	se	k3xPyFc4	se
přidal	přidat	k5eAaPmAgMnS	přidat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
objekt	objekt	k1gInSc1	objekt
s	s	k7c7	s
provizorním	provizorní	k2eAgInSc7d1	provizorní
názvem	název	k1gInSc7	název
2003	[number]	k4	2003
UB313	UB313	k1gMnPc2	UB313
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
Eris	Eris	k1gFnSc1	Eris
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
nepatrně	patrně	k6eNd1	patrně
menší	malý	k2eAgFnSc1d2	menší
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hmotnější	hmotný	k2eAgNnSc1d2	hmotnější
než	než	k8xS	než
Pluto	plut	k2eAgNnSc1d1	Pluto
<g/>
.	.	kIx.	.
</s>
<s>
Čas	čas	k1gInSc1	čas
od	od	k7c2	od
času	čas	k1gInSc2	čas
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
úvahy	úvaha	k1gFnPc1	úvaha
o	o	k7c6	o
existenci	existence	k1gFnSc6	existence
dalších	další	k2eAgFnPc2d1	další
planet	planeta	k1gFnPc2	planeta
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
byla	být	k5eAaImAgFnS	být
předpovězena	předpovědit	k5eAaPmNgFnS	předpovědit
Devátá	devátý	k4xOgFnSc1	devátý
planeta	planeta	k1gFnSc1	planeta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Důležitými	důležitý	k2eAgFnPc7d1	důležitá
složkami	složka	k1gFnPc7	složka
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
planetky	planetka	k1gFnPc4	planetka
tzv.	tzv.	kA	tzv.
hlavního	hlavní	k2eAgInSc2d1	hlavní
pásu	pás	k1gInSc2	pás
na	na	k7c6	na
drahách	draha	k1gFnPc6	draha
mezi	mezi	k7c7	mezi
Marsem	Mars	k1gInSc7	Mars
a	a	k8xC	a
Jupiterem	Jupiter	k1gMnSc7	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hlavním	hlavní	k2eAgInSc6d1	hlavní
pásu	pás	k1gInSc6	pás
planetek	planetka	k1gFnPc2	planetka
se	se	k3xPyFc4	se
také	také	k9	také
nachází	nacházet	k5eAaImIp3nS	nacházet
trpasličí	trpasličí	k2eAgFnSc1d1	trpasličí
planeta	planeta	k1gFnSc1	planeta
Ceres	ceres	k1gInSc1	ceres
<g/>
.	.	kIx.	.
</s>
<s>
Překvapivě	překvapivě	k6eAd1	překvapivě
mnoho	mnoho	k6eAd1	mnoho
poměrně	poměrně	k6eAd1	poměrně
velkých	velký	k2eAgNnPc2d1	velké
těles	těleso	k1gNnPc2	těleso
je	být	k5eAaImIp3nS	být
především	především	k9	především
v	v	k7c6	v
posledním	poslední	k2eAgNnSc6d1	poslední
desetiletí	desetiletí	k1gNnSc6	desetiletí
nacházeno	nacházet	k5eAaImNgNnS	nacházet
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
tzv.	tzv.	kA	tzv.
Kuiperova	Kuiperův	k2eAgInSc2d1	Kuiperův
pásu	pás	k1gInSc2	pás
za	za	k7c7	za
drahou	draha	k1gFnSc7	draha
Neptunu	Neptun	k1gInSc2	Neptun
(	(	kIx(	(
<g/>
Quaoar	Quaoar	k1gMnSc1	Quaoar
<g/>
,	,	kIx,	,
Orcus	Orcus	k1gMnSc1	Orcus
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
i	i	k9	i
dále	daleko	k6eAd2	daleko
(	(	kIx(	(
<g/>
Sedna	Sedna	k1gFnSc1	Sedna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Úplný	úplný	k2eAgInSc1d1	úplný
okraj	okraj	k1gInSc1	okraj
naší	náš	k3xOp1gFnSc2	náš
soustavy	soustava	k1gFnSc2	soustava
pak	pak	k6eAd1	pak
tvoří	tvořit	k5eAaImIp3nS	tvořit
obrovská	obrovský	k2eAgFnSc1d1	obrovská
zásobárna	zásobárna	k1gFnSc1	zásobárna
kometárních	kometární	k2eAgNnPc2d1	kometární
jader	jádro	k1gNnPc2	jádro
–	–	k?	–
tzv.	tzv.	kA	tzv.
Oortův	Oortův	k2eAgInSc4d1	Oortův
oblak	oblak	k1gInSc4	oblak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Vědecká	vědecký	k2eAgFnSc1d1	vědecká
teorie	teorie	k1gFnSc1	teorie
jejího	její	k3xOp3gInSc2	její
vzniku	vznik	k1gInSc2	vznik
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
před	před	k7c7	před
více	hodně	k6eAd2	hodně
než	než	k8xS	než
4,6	[number]	k4	4,6
miliardami	miliarda	k4xCgFnPc7	miliarda
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
v	v	k7c6	v
Galaxii	galaxie	k1gFnSc6	galaxie
začaly	začít	k5eAaPmAgFnP	začít
shlukovat	shlukovat	k5eAaImF	shlukovat
částečky	částečka	k1gFnPc1	částečka
prachu	prach	k1gInSc2	prach
a	a	k8xC	a
plynu	plyn	k1gInSc2	plyn
–	–	k?	–
vznikal	vznikat	k5eAaImAgInS	vznikat
jakýsi	jakýsi	k3yIgInSc1	jakýsi
obrovský	obrovský	k2eAgInSc1d1	obrovský
prachoplynný	prachoplynný	k2eAgInSc1d1	prachoplynný
mrak	mrak	k1gInSc1	mrak
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
přeměna	přeměna	k1gFnSc1	přeměna
nedaleké	daleký	k2eNgFnSc2d1	nedaleká
hvězdy	hvězda	k1gFnSc2	hvězda
v	v	k7c4	v
supernovu	supernova	k1gFnSc4	supernova
<g/>
,	,	kIx,	,
kterýžto	kterýžto	k?	kterýžto
děj	děj	k1gInSc4	děj
doprovázely	doprovázet	k5eAaImAgFnP	doprovázet
tlakové	tlakový	k2eAgFnPc1d1	tlaková
vlny	vlna	k1gFnPc1	vlna
<g/>
,	,	kIx,	,
přiměla	přimět	k5eAaPmAgFnS	přimět
mračno	mračno	k1gNnSc4	mračno
k	k	k7c3	k
pohybu	pohyb	k1gInSc3	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Částečky	částečka	k1gFnPc1	částečka
prachu	prach	k1gInSc2	prach
a	a	k8xC	a
plynu	plyn	k1gInSc2	plyn
se	se	k3xPyFc4	se
zformovaly	zformovat	k5eAaPmAgFnP	zformovat
do	do	k7c2	do
prstenců	prstenec	k1gInPc2	prstenec
rotujících	rotující	k2eAgInPc2d1	rotující
kolem	kolem	k7c2	kolem
hustého	hustý	k2eAgInSc2d1	hustý
a	a	k8xC	a
hmotného	hmotný	k2eAgInSc2d1	hmotný
středu	střed	k1gInSc2	střed
mraku	mrak	k1gInSc2	mrak
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
mračno	mračno	k1gNnSc1	mračno
hroutilo	hroutit	k5eAaImAgNnS	hroutit
<g/>
,	,	kIx,	,
prach	prach	k1gInSc1	prach
a	a	k8xC	a
plyny	plyn	k1gInPc1	plyn
byly	být	k5eAaImAgFnP	být
gravitační	gravitační	k2eAgFnSc7d1	gravitační
silou	síla	k1gFnSc7	síla
přitahovány	přitahován	k2eAgFnPc1d1	přitahována
do	do	k7c2	do
jeho	jeho	k3xOp3gInSc2	jeho
středu	střed	k1gInSc2	střed
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
zvyšovala	zvyšovat	k5eAaImAgFnS	zvyšovat
teplota	teplota	k1gFnSc1	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Jádro	jádro	k1gNnSc1	jádro
mračna	mračno	k1gNnSc2	mračno
se	se	k3xPyFc4	se
ohřálo	ohřát	k5eAaPmAgNnS	ohřát
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
začala	začít	k5eAaPmAgFnS	začít
probíhat	probíhat	k5eAaImF	probíhat
termonukleární	termonukleární	k2eAgFnPc4d1	termonukleární
reakce	reakce	k1gFnPc4	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
Slunce	slunce	k1gNnSc1	slunce
a	a	k8xC	a
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
sluneční	sluneční	k2eAgInSc1d1	sluneční
vítr	vítr	k1gInSc1	vítr
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
"	"	kIx"	"
<g/>
rozfoukal	rozfoukat	k5eAaPmAgMnS	rozfoukat
<g/>
"	"	kIx"	"
zbylý	zbylý	k2eAgInSc4d1	zbylý
prach	prach	k1gInSc4	prach
a	a	k8xC	a
plyn	plyn	k1gInSc1	plyn
směrem	směr	k1gInSc7	směr
ke	k	k7c3	k
vznikajícím	vznikající	k2eAgFnPc3d1	vznikající
planetám	planeta	k1gFnPc3	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Malé	Malé	k2eAgFnPc1d1	Malé
částečky	částečka	k1gFnPc1	částečka
v	v	k7c6	v
protoplanetárním	protoplanetární	k2eAgNnSc6d1	protoplanetární
mračnu	mračno	k1gNnSc6	mračno
do	do	k7c2	do
sebe	sebe	k3xPyFc4	sebe
začaly	začít	k5eAaPmAgFnP	začít
narážet	narážet	k5eAaPmF	narážet
a	a	k8xC	a
spojovat	spojovat	k5eAaImF	spojovat
se	se	k3xPyFc4	se
do	do	k7c2	do
stále	stále	k6eAd1	stále
větších	veliký	k2eAgInPc2d2	veliký
a	a	k8xC	a
větších	veliký	k2eAgInPc2d2	veliký
kusů	kus	k1gInPc2	kus
hmoty	hmota	k1gFnSc2	hmota
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgMnSc1d3	veliký
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
planetesimálami	planetesimála	k1gFnPc7	planetesimála
–	–	k?	–
základními	základní	k2eAgInPc7d1	základní
kameny	kámen	k1gInPc7	kámen
budoucích	budoucí	k2eAgFnPc2d1	budoucí
planet	planeta	k1gFnPc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
působení	působení	k1gNnSc3	působení
gravitace	gravitace	k1gFnSc2	gravitace
vznikaly	vznikat	k5eAaImAgInP	vznikat
stále	stále	k6eAd1	stále
větší	veliký	k2eAgInPc4d2	veliký
objekty	objekt	k1gInPc4	objekt
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
celé	celý	k2eAgFnSc2d1	celá
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
mnoho	mnoho	k4c4	mnoho
planetek	planetka	k1gFnPc2	planetka
a	a	k8xC	a
ještě	ještě	k6eAd1	ještě
více	hodně	k6eAd2	hodně
komet	kometa	k1gFnPc2	kometa
<g/>
.	.	kIx.	.
</s>
<s>
Dál	daleko	k6eAd2	daleko
od	od	k7c2	od
středu	střed	k1gInSc2	střed
byly	být	k5eAaImAgFnP	být
teploty	teplota	k1gFnPc1	teplota
nižší	nízký	k2eAgFnPc1d2	nižší
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yRnSc3	což
vznikli	vzniknout	k5eAaPmAgMnP	vzniknout
čtyři	čtyři	k4xCgMnPc4	čtyři
plynoví	plynový	k2eAgMnPc1d1	plynový
obři	obr	k1gMnPc1	obr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Zánik	zánik	k1gInSc1	zánik
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Budoucí	budoucí	k2eAgInSc1d1	budoucí
vývoj	vývoj	k1gInSc1	vývoj
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
závisí	záviset	k5eAaImIp3nS	záviset
od	od	k7c2	od
vývoje	vývoj	k1gInSc2	vývoj
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Slunce	slunce	k1gNnSc1	slunce
má	mít	k5eAaImIp3nS	mít
jaderné	jaderný	k2eAgNnSc4d1	jaderné
palivo	palivo	k1gNnSc4	palivo
dostačující	dostačující	k2eAgNnSc4d1	dostačující
ještě	ještě	k6eAd1	ještě
na	na	k7c4	na
5	[number]	k4	5
<g/>
,	,	kIx,	,
maximálně	maximálně	k6eAd1	maximálně
na	na	k7c4	na
7	[number]	k4	7
miliard	miliarda	k4xCgFnPc2	miliarda
let	léto	k1gNnPc2	léto
postupného	postupný	k2eAgNnSc2d1	postupné
hoření	hoření	k1gNnSc2	hoření
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
spotřebování	spotřebování	k1gNnSc6	spotřebování
se	se	k3xPyFc4	se
začnou	začít	k5eAaPmIp3nP	začít
vnější	vnější	k2eAgFnPc1d1	vnější
vrstvy	vrstva	k1gFnPc1	vrstva
Slunce	slunce	k1gNnSc2	slunce
pomalu	pomalu	k6eAd1	pomalu
nafukovat	nafukovat	k5eAaImF	nafukovat
a	a	k8xC	a
pohlcovat	pohlcovat	k5eAaImF	pohlcovat
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
planety	planeta	k1gFnSc2	planeta
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Slunce	slunce	k1gNnSc1	slunce
přejde	přejít	k5eAaPmIp3nS	přejít
do	do	k7c2	do
dalšího	další	k2eAgNnSc2d1	další
stadia	stadion	k1gNnSc2	stadion
svého	svůj	k3xOyFgInSc2	svůj
vývoje	vývoj	k1gInSc2	vývoj
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
nazýváme	nazývat	k5eAaImIp1nP	nazývat
červený	červený	k2eAgMnSc1d1	červený
obr	obr	k1gMnSc1	obr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gInSc6	on
zůstane	zůstat	k5eAaPmIp3nS	zůstat
Slunce	slunce	k1gNnSc1	slunce
přibližně	přibližně	k6eAd1	přibližně
na	na	k7c4	na
35	[number]	k4	35
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
stabilní	stabilní	k2eAgInPc1d1	stabilní
a	a	k8xC	a
zatím	zatím	k6eAd1	zatím
bude	být	k5eAaImBp3nS	být
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
jádru	jádro	k1gNnSc6	jádro
spalovat	spalovat	k5eAaImF	spalovat
helium	helium	k1gNnSc4	helium
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
odpad	odpad	k1gInSc4	odpad
<g/>
"	"	kIx"	"
původních	původní	k2eAgFnPc2d1	původní
fúzních	fúzní	k2eAgFnPc2d1	fúzní
reakcí	reakce	k1gFnPc2	reakce
<g/>
)	)	kIx)	)
na	na	k7c4	na
uhlík	uhlík	k1gInSc4	uhlík
a	a	k8xC	a
kyslík	kyslík	k1gInSc4	kyslík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
vyčerpání	vyčerpání	k1gNnSc6	vyčerpání
zásob	zásoba	k1gFnPc2	zásoba
hélia	hélium	k1gNnSc2	hélium
však	však	k9	však
bude	být	k5eAaImBp3nS	být
rozpínání	rozpínání	k1gNnSc1	rozpínání
slunečního	sluneční	k2eAgInSc2d1	sluneční
povrchu	povrch	k1gInSc2	povrch
pokračovat	pokračovat	k5eAaImF	pokračovat
až	až	k9	až
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
změní	změnit	k5eAaPmIp3nS	změnit
na	na	k7c4	na
mladou	mladý	k2eAgFnSc4d1	mladá
planetární	planetární	k2eAgFnSc4d1	planetární
mlhovinu	mlhovina	k1gFnSc4	mlhovina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
pohltí	pohltit	k5eAaPmIp3nS	pohltit
i	i	k9	i
ty	ten	k3xDgFnPc4	ten
nejvzdálenější	vzdálený	k2eAgFnPc4d3	nejvzdálenější
části	část	k1gFnPc4	část
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jádra	jádro	k1gNnSc2	jádro
Slunce	slunce	k1gNnSc2	slunce
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
malá	malý	k2eAgFnSc1d1	malá
horká	horký	k2eAgFnSc1d1	horká
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
hustá	hustý	k2eAgFnSc1d1	hustá
hvězda	hvězda	k1gFnSc1	hvězda
–	–	k?	–
bílý	bílý	k1gMnSc1	bílý
trpaslík	trpaslík	k1gMnSc1	trpaslík
<g/>
.	.	kIx.	.
</s>
<s>
Vnější	vnější	k2eAgFnPc1d1	vnější
obálky	obálka	k1gFnPc1	obálka
se	se	k3xPyFc4	se
budou	být	k5eAaImBp3nP	být
jako	jako	k8xS	jako
planetární	planetární	k2eAgFnSc1d1	planetární
mlhovina	mlhovina	k1gFnSc1	mlhovina
nadále	nadále	k6eAd1	nadále
rozpínat	rozpínat	k5eAaImF	rozpínat
<g/>
,	,	kIx,	,
až	až	k8xS	až
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
smíchají	smíchat	k5eAaPmIp3nP	smíchat
s	s	k7c7	s
mezihvězdným	mezihvězdný	k2eAgInSc7d1	mezihvězdný
prostředí	prostředí	k1gNnSc3	prostředí
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
posloužit	posloužit	k5eAaPmF	posloužit
při	při	k7c6	při
vzniku	vznik	k1gInSc6	vznik
dalších	další	k2eAgFnPc2d1	další
nových	nový	k2eAgFnPc2d1	nová
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Složení	složení	k1gNnSc1	složení
soustavy	soustava	k1gFnSc2	soustava
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Slunce	slunce	k1gNnSc2	slunce
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Slunce	slunce	k1gNnSc1	slunce
je	být	k5eAaImIp3nS	být
pomyslným	pomyslný	k2eAgInSc7d1	pomyslný
centrálním	centrální	k2eAgInSc7d1	centrální
bodem	bod	k1gInSc7	bod
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
výrazně	výrazně	k6eAd1	výrazně
nejhmotnějším	hmotný	k2eAgNnSc7d3	nejhmotnější
tělesem	těleso	k1gNnSc7	těleso
celé	celý	k2eAgFnSc2d1	celá
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
což	což	k9	což
mj.	mj.	kA	mj.
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
obíhá	obíhat	k5eAaImIp3nS	obíhat
v	v	k7c6	v
těsné	těsný	k2eAgFnSc6d1	těsná
blízkosti	blízkost	k1gFnSc6	blízkost
jejího	její	k3xOp3gNnSc2	její
těžiště	těžiště	k1gNnSc2	těžiště
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
tak	tak	k9	tak
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
povětšinou	povětšinou	k6eAd1	povětšinou
nachází	nacházet	k5eAaImIp3nS	nacházet
mimo	mimo	k7c4	mimo
samotné	samotný	k2eAgNnSc4d1	samotné
Slunce	slunce	k1gNnSc4	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Vlivem	vliv	k1gInSc7	vliv
gravitačních	gravitační	k2eAgFnPc2d1	gravitační
sil	síla	k1gFnPc2	síla
úměrných	úměrná	k1gFnPc2	úměrná
sluneční	sluneční	k2eAgFnSc2d1	sluneční
hmotnosti	hmotnost	k1gFnSc2	hmotnost
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
celá	celý	k2eAgFnSc1d1	celá
soustava	soustava	k1gFnSc1	soustava
vázána	vázat	k5eAaImNgFnS	vázat
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
hvězda	hvězda	k1gFnSc1	hvězda
září	zářit	k5eAaImIp3nS	zářit
přibližně	přibližně	k6eAd1	přibližně
4,5	[number]	k4	4,5
miliardy	miliarda	k4xCgFnSc2	miliarda
let	léto	k1gNnPc2	léto
a	a	k8xC	a
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
zářit	zářit	k5eAaImF	zářit
cca	cca	kA	cca
dalších	další	k2eAgFnPc2d1	další
7	[number]	k4	7
miliard	miliarda	k4xCgFnPc2	miliarda
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vyčerpání	vyčerpání	k1gNnSc6	vyčerpání
většiny	většina	k1gFnSc2	většina
vodíku	vodík	k1gInSc2	vodík
se	se	k3xPyFc4	se
jádro	jádro	k1gNnSc1	jádro
gravitací	gravitace	k1gFnPc2	gravitace
smrští	smršť	k1gFnPc2	smršť
a	a	k8xC	a
z	z	k7c2	z
"	"	kIx"	"
<g/>
popela	popel	k1gInSc2	popel
<g/>
"	"	kIx"	"
předcházející	předcházející	k2eAgFnSc1d1	předcházející
reakce	reakce	k1gFnSc1	reakce
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
"	"	kIx"	"
<g/>
palivo	palivo	k1gNnSc4	palivo
<g/>
"	"	kIx"	"
pro	pro	k7c4	pro
následující	následující	k2eAgInSc4d1	následující
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
s	s	k7c7	s
prudkým	prudký	k2eAgInSc7d1	prudký
vzrůstem	vzrůst	k1gInSc7	vzrůst
tlaku	tlak	k1gInSc2	tlak
a	a	k8xC	a
teploty	teplota	k1gFnSc2	teplota
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
budou	být	k5eAaImBp3nP	být
"	"	kIx"	"
<g/>
zapalovat	zapalovat	k5eAaImF	zapalovat
<g/>
"	"	kIx"	"
další	další	k2eAgFnPc4d1	další
reakce	reakce	k1gFnPc4	reakce
doprovázené	doprovázený	k2eAgNnSc1d1	doprovázené
vznikem	vznik	k1gInSc7	vznik
těžších	těžký	k2eAgInPc2d2	těžší
prvků	prvek	k1gInPc2	prvek
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
uhlíku	uhlík	k1gInSc2	uhlík
<g/>
,	,	kIx,	,
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
,	,	kIx,	,
neonu	neon	k1gInSc2	neon
a	a	k8xC	a
hořčíku	hořčík	k1gInSc2	hořčík
<g/>
.	.	kIx.	.
<g/>
Samotná	samotný	k2eAgFnSc1d1	samotná
existence	existence	k1gFnSc1	existence
soustavy	soustava	k1gFnSc2	soustava
nicméně	nicméně	k8xC	nicméně
není	být	k5eNaImIp3nS	být
bezprostředně	bezprostředně	k6eAd1	bezprostředně
vázána	vázat	k5eAaImNgFnS	vázat
na	na	k7c4	na
tyto	tento	k3xDgFnPc4	tento
přeměny	přeměna	k1gFnPc4	přeměna
a	a	k8xC	a
tak	tak	k6eAd1	tak
bude	být	k5eAaImBp3nS	být
velmi	velmi	k6eAd1	velmi
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
existovat	existovat	k5eAaImF	existovat
i	i	k9	i
po	po	k7c6	po
útlumu	útlum	k1gInSc6	útlum
slunečních	sluneční	k2eAgFnPc2d1	sluneční
termonukleárních	termonukleární	k2eAgFnPc2d1	termonukleární
reakcí	reakce	k1gFnPc2	reakce
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc3	jeho
proměně	proměna	k1gFnSc3	proměna
v	v	k7c4	v
rudého	rudý	k2eAgMnSc4d1	rudý
obra	obr	k1gMnSc4	obr
a	a	k8xC	a
následné	následný	k2eAgNnSc1d1	následné
smrštění	smrštění	k1gNnSc1	smrštění
se	se	k3xPyFc4	se
v	v	k7c4	v
"	"	kIx"	"
<g/>
bílého	bílý	k2eAgMnSc4d1	bílý
trpaslíka	trpaslík	k1gMnSc4	trpaslík
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Vnitřní	vnitřní	k2eAgFnPc1d1	vnitřní
planety	planeta	k1gFnPc1	planeta
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Merkur	Merkur	k1gInSc1	Merkur
====	====	k?	====
</s>
</p>
<p>
</p>
<p>
<s>
Merkur	Merkur	k1gMnSc1	Merkur
je	být	k5eAaImIp3nS	být
Slunci	slunce	k1gNnSc3	slunce
nejbližší	blízký	k2eAgFnSc1d3	nejbližší
a	a	k8xC	a
současně	současně	k6eAd1	současně
i	i	k9	i
nejmenší	malý	k2eAgFnSc7d3	nejmenší
planetou	planeta	k1gFnSc7	planeta
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
40	[number]	k4	40
%	%	kIx~	%
větší	veliký	k2eAgFnSc2d2	veliký
velikosti	velikost	k1gFnSc2	velikost
než	než	k8xS	než
pozemský	pozemský	k2eAgInSc4d1	pozemský
Měsíc	měsíc	k1gInSc4	měsíc
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
menší	malý	k2eAgInSc4d2	menší
než	než	k8xS	než
Jupiterův	Jupiterův	k2eAgInSc4d1	Jupiterův
měsíc	měsíc	k1gInSc4	měsíc
Ganymed	Ganymed	k1gMnSc1	Ganymed
a	a	k8xC	a
Saturnův	Saturnův	k2eAgInSc1d1	Saturnův
Titan	titan	k1gInSc1	titan
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
oběžná	oběžný	k2eAgFnSc1d1	oběžná
dráha	dráha	k1gFnSc1	dráha
je	být	k5eAaImIp3nS	být
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
planet	planeta	k1gFnPc2	planeta
nejblíže	blízce	k6eAd3	blízce
ke	k	k7c3	k
Slunci	slunce	k1gNnSc3	slunce
a	a	k8xC	a
jeden	jeden	k4xCgInSc4	jeden
oběh	oběh	k1gInSc4	oběh
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
trvá	trvat	k5eAaImIp3nS	trvat
pouze	pouze	k6eAd1	pouze
87,969	[number]	k4	87,969
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Dráha	dráha	k1gFnSc1	dráha
Merkuru	Merkur	k1gInSc2	Merkur
má	mít	k5eAaImIp3nS	mít
největší	veliký	k2eAgFnSc1d3	veliký
výstřednost	výstřednost	k1gFnSc1	výstřednost
dráhy	dráha	k1gFnSc2	dráha
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
planet	planeta	k1gFnPc2	planeta
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
a	a	k8xC	a
nejmenší	malý	k2eAgInSc4d3	nejmenší
sklon	sklon	k1gInSc4	sklon
rotační	rotační	k2eAgFnSc2d1	rotační
osy	osa	k1gFnSc2	osa
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
dvou	dva	k4xCgInPc2	dva
oběhů	oběh	k1gInPc2	oběh
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
dojde	dojít	k5eAaPmIp3nS	dojít
ke	k	k7c3	k
třem	tři	k4xCgInPc3	tři
otočením	otočení	k1gNnSc7	otočení
kolem	kolem	k7c2	kolem
rotační	rotační	k2eAgFnSc2d1	rotační
osy	osa	k1gFnSc2	osa
<g/>
.	.	kIx.	.
</s>
<s>
Perihélium	perihélium	k1gNnSc1	perihélium
jeho	jeho	k3xOp3gFnSc2	jeho
dráhy	dráha	k1gFnSc2	dráha
se	se	k3xPyFc4	se
stáčí	stáčet	k5eAaImIp3nS	stáčet
ke	k	k7c3	k
Slunci	slunce	k1gNnSc3	slunce
o	o	k7c4	o
43	[number]	k4	43
vteřin	vteřina	k1gFnPc2	vteřina
za	za	k7c4	za
století	století	k1gNnSc4	století
<g/>
;	;	kIx,	;
fenomén	fenomén	k1gInSc1	fenomén
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
ve	v	k7c6	v
20.	[number]	k4	20.
století	století	k1gNnSc2	století
vysvětlil	vysvětlit	k5eAaPmAgMnS	vysvětlit
Albert	Albert	k1gMnSc1	Albert
Einstein	Einstein	k1gMnSc1	Einstein
obecnou	obecná	k1gFnSc4	obecná
teorií	teorie	k1gFnPc2	teorie
relativity	relativita	k1gFnSc2	relativita
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
Merkur	Merkur	k1gInSc1	Merkur
jasnosti	jasnost	k1gFnSc2	jasnost
mezi	mezi	k7c7	mezi
−	−	k?	−
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
<g/>
0	[number]	k4	0
až	až	k8xS	až
5	[number]	k4	5
<g/>
,	,	kIx,	,
<g/>
5m	[number]	k4	5m
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
je	být	k5eAaImIp3nS	být
viditelný	viditelný	k2eAgInSc1d1	viditelný
i	i	k9	i
pouhým	pouhý	k2eAgNnSc7d1	pouhé
okem	oke	k1gNnSc7	oke
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
nevzdaluje	vzdalovat	k5eNaImIp3nS	vzdalovat
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
nikdy	nikdy	k6eAd1	nikdy
dále	daleko	k6eAd2	daleko
než	než	k8xS	než
na	na	k7c4	na
28,3	[number]	k4	28,3
<g/>
°	°	k?	°
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
těžko	těžko	k6eAd1	těžko
pozorovatelný	pozorovatelný	k2eAgInSc1d1	pozorovatelný
<g/>
.	.	kIx.	.
</s>
<s>
Nejlepší	dobrý	k2eAgFnPc1d3	nejlepší
podmínky	podmínka	k1gFnPc1	podmínka
tak	tak	k6eAd1	tak
nastávají	nastávat	k5eAaImIp3nP	nastávat
při	při	k7c6	při
soumraku	soumrak	k1gInSc6	soumrak
či	či	k8xC	či
úsvitu	úsvit	k1gInSc6	úsvit
než	než	k8xS	než
vyjde	vyjít	k5eAaPmIp3nS	vyjít
Slunce	slunce	k1gNnSc1	slunce
nad	nad	k7c4	nad
horizont	horizont	k1gInSc4	horizont
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pozorování	pozorování	k1gNnSc1	pozorování
planety	planeta	k1gFnSc2	planeta
pozemskými	pozemský	k2eAgInPc7d1	pozemský
teleskopy	teleskop	k1gInPc7	teleskop
je	být	k5eAaImIp3nS	být
složité	složitý	k2eAgNnSc1d1	složité
kvůli	kvůli	k7c3	kvůli
blízkosti	blízkost	k1gFnSc3	blízkost
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Detailnější	detailní	k2eAgFnPc4d2	detailnější
znalosti	znalost	k1gFnPc4	znalost
přinesla	přinést	k5eAaPmAgFnS	přinést
až	až	k9	až
dvojice	dvojice	k1gFnSc1	dvojice
sond	sonda	k1gFnPc2	sonda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
kolem	kolem	k7c2	kolem
planety	planeta	k1gFnSc2	planeta
prolétla	prolétnout	k5eAaPmAgFnS	prolétnout
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
sondou	sonda	k1gFnSc7	sonda
u	u	k7c2	u
Merkuru	Merkur	k1gInSc2	Merkur
byla	být	k5eAaImAgFnS	být
americká	americký	k2eAgFnSc1d1	americká
sonda	sonda	k1gFnSc1	sonda
Mariner	Mariner	k1gInSc1	Mariner
10	[number]	k4	10
v	v	k7c6	v
70.	[number]	k4	70.
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
nasnímala	nasnímat	k5eAaPmAgNnP	nasnímat
přibližně	přibližně	k6eAd1	přibližně
45	[number]	k4	45
%	%	kIx~	%
povrchu	povrch	k1gInSc3	povrch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
dorazila	dorazit	k5eAaPmAgFnS	dorazit
k	k	k7c3	k
planetě	planeta	k1gFnSc3	planeta
další	další	k2eAgFnSc1d1	další
sonda	sonda	k1gFnSc1	sonda
MESSENGER	MESSENGER	kA	MESSENGER
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
provedla	provést	k5eAaPmAgFnS	provést
tři	tři	k4xCgInPc4	tři
průlety	průlet	k1gInPc4	průlet
kolem	kolem	k7c2	kolem
Merkuru	Merkur	k1gInSc2	Merkur
a	a	k8xC	a
v	v	k7c6	v
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
byla	být	k5eAaImAgFnS	být
navedena	navést	k5eAaPmNgFnS	navést
na	na	k7c4	na
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
kolem	kolem	k7c2	kolem
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vyčerpání	vyčerpání	k1gNnSc6	vyčerpání
veškerého	veškerý	k3xTgNnSc2	veškerý
paliva	palivo	k1gNnSc2	palivo
ukončila	ukončit	k5eAaPmAgFnS	ukončit
svou	svůj	k3xOyFgFnSc4	svůj
misi	mise	k1gFnSc4	mise
plánovaným	plánovaný	k2eAgInSc7d1	plánovaný
pádem	pád	k1gInSc7	pád
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
Merkuru	Merkur	k1gInSc2	Merkur
dne	den	k1gInSc2	den
30.	[number]	k4	30.
dubna	duben	k1gInSc2	duben
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Snímky	snímek	k1gInPc1	snímek
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
dvou	dva	k4xCgFnPc2	dva
sond	sonda	k1gFnPc2	sonda
umožnily	umožnit	k5eAaPmAgInP	umožnit
prozkoumat	prozkoumat	k5eAaPmF	prozkoumat
povrch	povrch	k1gInSc4	povrch
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
silně	silně	k6eAd1	silně
připomíná	připomínat	k5eAaImIp3nS	připomínat
měsíční	měsíční	k2eAgFnSc4d1	měsíční
krajinu	krajina	k1gFnSc4	krajina
plnou	plný	k2eAgFnSc4d1	plná
impaktních	impaktní	k2eAgInPc2d1	impaktní
kráterů	kráter	k1gInPc2	kráter
<g/>
,	,	kIx,	,
nízkých	nízký	k2eAgNnPc2d1	nízké
pohoří	pohoří	k1gNnPc2	pohoří
a	a	k8xC	a
lávových	lávový	k2eAgFnPc2d1	lávová
planin	planina	k1gFnPc2	planina
<g/>
.	.	kIx.	.
</s>
<s>
Vlivem	vlivem	k7c2	vlivem
neustálých	neustálý	k2eAgInPc2d1	neustálý
dopadů	dopad	k1gInPc2	dopad
těles	těleso	k1gNnPc2	těleso
všech	všecek	k3xTgFnPc2	všecek
velikostí	velikost	k1gFnPc2	velikost
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
Merkuru	Merkur	k1gInSc2	Merkur
je	být	k5eAaImIp3nS	být
většina	většina	k1gFnSc1	většina
povrchu	povrch	k1gInSc2	povrch
erodována	erodovat	k5eAaImNgFnS	erodovat
drobnými	drobný	k2eAgInPc7d1	drobný
krátery	kráter	k1gInPc7	kráter
<g/>
.	.	kIx.	.
</s>
<s>
Povrch	povrch	k1gInSc1	povrch
je	být	k5eAaImIp3nS	být
nejspíše	nejspíše	k9	nejspíše
vlivem	vliv	k1gInSc7	vliv
smršťování	smršťování	k1gNnSc2	smršťování
planety	planeta	k1gFnSc2	planeta
rozpraskán	rozpraskán	k2eAgInSc4d1	rozpraskán
množstvím	množství	k1gNnSc7	množství
útesových	útesový	k2eAgInPc2d1	útesový
zlomů	zlom	k1gInPc2	zlom
dosahujících	dosahující	k2eAgFnPc2d1	dosahující
výšky	výška	k1gFnPc1	výška
několika	několik	k4yIc2	několik
kilometrů	kilometr	k1gInPc2	kilometr
a	a	k8xC	a
délky	délka	k1gFnSc2	délka
stovek	stovka	k1gFnPc2	stovka
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
je	být	k5eAaImIp3nS	být
povrch	povrch	k1gInSc4	povrch
neustále	neustále	k6eAd1	neustále
bombardován	bombardován	k2eAgInSc4d1	bombardován
fotony	foton	k1gInPc4	foton
i	i	k8xC	i
slunečním	sluneční	k2eAgInSc7d1	sluneční
větrem	vítr	k1gInSc7	vítr
–	–	k?	–
proudem	proud	k1gInSc7	proud
nabitých	nabitý	k2eAgFnPc2d1	nabitá
částic	částice	k1gFnPc2	částice
směřujících	směřující	k2eAgFnPc2d1	směřující
vysokou	vysoký	k2eAgFnSc7d1	vysoká
rychlostí	rychlost	k1gFnSc7	rychlost
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Nepřítomnost	nepřítomnost	k1gFnSc1	nepřítomnost
atmosféry	atmosféra	k1gFnSc2	atmosféra
je	být	k5eAaImIp3nS	být
příčinou	příčina	k1gFnSc7	příčina
velkých	velký	k2eAgInPc2d1	velký
rozdílů	rozdíl	k1gInPc2	rozdíl
teplot	teplota	k1gFnPc2	teplota
mezi	mezi	k7c7	mezi
osvětlenou	osvětlený	k2eAgFnSc7d1	osvětlená
a	a	k8xC	a
neosvětlenou	osvětlený	k2eNgFnSc7d1	neosvětlená
polokoulí	polokoule	k1gFnSc7	polokoule
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíly	rozdíl	k1gInPc1	rozdíl
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
hodnot	hodnota	k1gFnPc2	hodnota
téměř	téměř	k6eAd1	téměř
700	[number]	k4	700
°	°	k?	°
<g/>
C.	C.	kA	C.
Na	na	k7c6	na
polokouli	polokoule	k1gFnSc6	polokoule
přivrácené	přivrácený	k2eAgFnSc6d1	přivrácená
ke	k	k7c3	k
Slunci	slunce	k1gNnSc3	slunce
může	moct	k5eAaImIp3nS	moct
teplota	teplota	k1gFnSc1	teplota
vystoupit	vystoupit	k5eAaPmF	vystoupit
na	na	k7c4	na
téměř	téměř	k6eAd1	téměř
430	[number]	k4	430
°	°	k?	°
<g/>
C.	C.	kA	C.
Na	na	k7c6	na
polokouli	polokoule	k1gFnSc6	polokoule
odvrácené	odvrácený	k2eAgFnSc6d1	odvrácená
panuje	panovat	k5eAaImIp3nS	panovat
mráz	mráz	k1gInSc1	mráz
až	až	k8xS	až
−	−	k?	−
<g/>
180	[number]	k4	180
°	°	k?	°
<g/>
C.	C.	kA	C.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Venuše	Venuše	k1gFnSc2	Venuše
====	====	k?	====
</s>
</p>
<p>
</p>
<p>
<s>
Venuše	Venuše	k1gFnSc1	Venuše
je	být	k5eAaImIp3nS	být
druhá	druhý	k4xOgFnSc1	druhý
planeta	planeta	k1gFnSc1	planeta
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pojmenovaná	pojmenovaný	k2eAgFnSc1d1	pojmenovaná
po	po	k7c6	po
římské	římský	k2eAgFnSc6d1	římská
bohyni	bohyně	k1gFnSc6	bohyně
lásky	láska	k1gFnSc2	láska
a	a	k8xC	a
krásy	krása	k1gFnSc2	krása
Venuši	Venuše	k1gFnSc4	Venuše
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jedinou	jediný	k2eAgFnSc4d1	jediná
planetu	planeta	k1gFnSc4	planeta
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
pojmenována	pojmenovat	k5eAaPmNgFnS	pojmenovat
po	po	k7c6	po
ženě	žena	k1gFnSc6	žena
<g/>
.	.	kIx.	.
</s>
<s>
Venuše	Venuše	k1gFnSc1	Venuše
je	být	k5eAaImIp3nS	být
terestrická	terestrický	k2eAgFnSc1d1	terestrická
planeta	planeta	k1gFnSc1	planeta
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
do	do	k7c2	do
velikosti	velikost	k1gFnSc2	velikost
a	a	k8xC	a
hrubé	hrubý	k2eAgFnSc2d1	hrubá
skladby	skladba	k1gFnSc2	skladba
velmi	velmi	k6eAd1	velmi
podobná	podobný	k2eAgFnSc1d1	podobná
Zemi	zem	k1gFnSc3	zem
<g/>
;	;	kIx,	;
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
nazývá	nazývat	k5eAaImIp3nS	nazývat
"	"	kIx"	"
<g/>
sesterskou	sesterský	k2eAgFnSc7d1	sesterská
planetou	planeta	k1gFnSc7	planeta
<g/>
"	"	kIx"	"
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
orbity	orbita	k1gFnPc1	orbita
všech	všecek	k3xTgFnPc2	všecek
ostatních	ostatní	k2eAgFnPc2d1	ostatní
planet	planeta	k1gFnPc2	planeta
jsou	být	k5eAaImIp3nP	být
elipsami	elipsa	k1gFnPc7	elipsa
<g/>
,	,	kIx,	,
orbita	orbita	k1gFnSc1	orbita
Venuše	Venuše	k1gFnSc2	Venuše
je	být	k5eAaImIp3nS	být
jediná	jediný	k2eAgFnSc1d1	jediná
téměř	téměř	k6eAd1	téměř
kružnicí	kružnice	k1gFnSc7	kružnice
<g/>
,	,	kIx,	,
se	s	k7c7	s
Sluncem	slunce	k1gNnSc7	slunce
pouze	pouze	k6eAd1	pouze
o	o	k7c6	o
0,7	[number]	k4	0,7
%	%	kIx~	%
mimo	mimo	k7c4	mimo
skutečný	skutečný	k2eAgInSc4d1	skutečný
střed	střed	k1gInSc4	střed
Venušiny	Venušin	k2eAgFnSc2d1	Venušina
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
Slunce	slunce	k1gNnSc2	slunce
oběhne	oběhnout	k5eAaPmIp3nS	oběhnout
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
224,7	[number]	k4	224,7
pozemského	pozemský	k2eAgInSc2d1	pozemský
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
Venuše	Venuše	k1gFnSc1	Venuše
ke	k	k7c3	k
Slunci	slunce	k1gNnSc3	slunce
blíže	blíž	k1gFnSc2	blíž
než	než	k8xS	než
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
vždy	vždy	k6eAd1	vždy
zhruba	zhruba	k6eAd1	zhruba
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
(	(	kIx(	(
<g/>
největší	veliký	k2eAgFnSc1d3	veliký
elongace	elongace	k1gFnSc1	elongace
je	být	k5eAaImIp3nS	být
47,8	[number]	k4	47,8
<g/>
°	°	k?	°
<g/>
)	)	kIx)	)
a	a	k8xC	a
lze	lze	k6eAd1	lze
ji	on	k3xPp3gFnSc4	on
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
vidět	vidět	k5eAaImF	vidět
jen	jen	k9	jen
před	před	k7c7	před
svítáním	svítání	k1gNnSc7	svítání
nebo	nebo	k8xC	nebo
po	po	k7c6	po
soumraku	soumrak	k1gInSc6	soumrak
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
i	i	k9	i
nejjasnější	jasný	k2eAgNnSc1d3	nejjasnější
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
Venuše	Venuše	k1gFnSc1	Venuše
někdy	někdy	k6eAd1	někdy
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
jitřenka	jitřenka	k1gFnSc1	jitřenka
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
večernice	večernice	k1gFnSc1	večernice
<g/>
"	"	kIx"	"
a	a	k8xC	a
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
zdaleka	zdaleka	k6eAd1	zdaleka
nejsilnější	silný	k2eAgInSc4d3	nejsilnější
bodový	bodový	k2eAgInSc4d1	bodový
zdroj	zdroj	k1gInSc4	zdroj
světla	světlo	k1gNnSc2	světlo
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
po	po	k7c6	po
Slunci	slunce	k1gNnSc6	slunce
a	a	k8xC	a
Měsíci	měsíc	k1gInSc6	měsíc
o	o	k7c4	o
magnitudě	magnitudě	k6eAd1	magnitudě
−	−	k?	−
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Výjimečně	výjimečně	k6eAd1	výjimečně
lze	lze	k6eAd1	lze
Venuši	Venuše	k1gFnSc4	Venuše
pouhým	pouhý	k2eAgNnSc7d1	pouhé
okem	oke	k1gNnSc7	oke
spatřit	spatřit	k5eAaPmF	spatřit
i	i	k8xC	i
ve	v	k7c6	v
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Venuše	Venuše	k1gFnSc1	Venuše
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
zakryta	zakrýt	k5eAaPmNgFnS	zakrýt
vrstvou	vrstva	k1gFnSc7	vrstva
husté	hustý	k2eAgFnSc2d1	hustá
oblačnosti	oblačnost	k1gFnSc2	oblačnost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nedovoluje	dovolovat	k5eNaImIp3nS	dovolovat
spatřit	spatřit	k5eAaPmF	spatřit
její	její	k3xOp3gInSc4	její
povrch	povrch	k1gInSc4	povrch
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
viditelného	viditelný	k2eAgNnSc2d1	viditelné
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
zapříčinilo	zapříčinit	k5eAaPmAgNnS	zapříčinit
velkou	velký	k2eAgFnSc4d1	velká
řadu	řada	k1gFnSc4	řada
spekulací	spekulace	k1gFnPc2	spekulace
o	o	k7c6	o
jejím	její	k3xOp3gInSc6	její
povrchu	povrch	k1gInSc6	povrch
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
přetrvávaly	přetrvávat	k5eAaImAgFnP	přetrvávat
až	až	k9	až
do	do	k7c2	do
20.	[number]	k4	20.
století	století	k1gNnPc2	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
její	její	k3xOp3gInSc1	její
povrch	povrch	k1gInSc1	povrch
prozkoumán	prozkoumat	k5eAaPmNgInS	prozkoumat
pomocí	pomocí	k7c2	pomocí
přistávacích	přistávací	k2eAgInPc2d1	přistávací
modulů	modul	k1gInPc2	modul
a	a	k8xC	a
radarového	radarový	k2eAgNnSc2d1	radarové
mapování	mapování	k1gNnSc2	mapování
povrchu	povrch	k1gInSc2	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Venuše	Venuše	k1gFnSc1	Venuše
má	mít	k5eAaImIp3nS	mít
nejhustější	hustý	k2eAgFnSc4d3	nejhustší
atmosféru	atmosféra	k1gFnSc4	atmosféra
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
terestrických	terestrický	k2eAgFnPc2d1	terestrická
planet	planeta	k1gFnPc2	planeta
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
absenci	absence	k1gFnSc4	absence
uhlíkového	uhlíkový	k2eAgInSc2d1	uhlíkový
cyklu	cyklus	k1gInSc2	cyklus
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
navázání	navázání	k1gNnSc2	navázání
do	do	k7c2	do
hornin	hornina	k1gFnPc2	hornina
či	či	k8xC	či
na	na	k7c4	na
biomasu	biomasa	k1gFnSc4	biomasa
z	z	k7c2	z
atmosféry	atmosféra	k1gFnSc2	atmosféra
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
jeho	jeho	k3xOp3gInSc3	jeho
enormnímu	enormní	k2eAgInSc3d1	enormní
nárůstu	nárůst	k1gInSc3	nárůst
až	až	k6eAd1	až
do	do	k7c2	do
současné	současný	k2eAgFnSc2d1	současná
podoby	podoba	k1gFnSc2	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Vznikl	vzniknout	k5eAaPmAgInS	vzniknout
tak	tak	k6eAd1	tak
silný	silný	k2eAgInSc1d1	silný
skleníkový	skleníkový	k2eAgInSc1d1	skleníkový
jev	jev	k1gInSc1	jev
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ohřál	ohřát	k5eAaPmAgInS	ohřát
planetu	planeta	k1gFnSc4	planeta
na	na	k7c4	na
teploty	teplota	k1gFnPc4	teplota
znemožňující	znemožňující	k2eAgInSc1d1	znemožňující
výskyt	výskyt	k1gInSc1	výskyt
kapalné	kapalný	k2eAgFnSc2d1	kapalná
vody	voda	k1gFnSc2	voda
na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
povrchu	povrch	k1gInSc6	povrch
a	a	k8xC	a
učinil	učinit	k5eAaImAgMnS	učinit
z	z	k7c2	z
Venuše	Venuše	k1gFnSc2	Venuše
suchý	suchý	k2eAgInSc4d1	suchý
a	a	k8xC	a
prašný	prašný	k2eAgInSc4d1	prašný
svět	svět	k1gInSc4	svět
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
teorie	teorie	k1gFnPc1	teorie
<g/>
,	,	kIx,	,
že	že	k8xS	že
Venuše	Venuše	k1gFnSc1	Venuše
měla	mít	k5eAaImAgFnS	mít
dříve	dříve	k6eAd2	dříve
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
Země	zem	k1gFnPc4	zem
oceány	oceán	k1gInPc7	oceán
kapalné	kapalný	k2eAgFnSc2d1	kapalná
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Voda	Voda	k1gMnSc1	Voda
se	se	k3xPyFc4	se
vlivem	vliv	k1gInSc7	vliv
narůstající	narůstající	k2eAgFnSc2d1	narůstající
teploty	teplota	k1gFnSc2	teplota
vypařila	vypařit	k5eAaPmAgFnS	vypařit
a	a	k8xC	a
následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
absenci	absence	k1gFnSc4	absence
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
vodní	vodní	k2eAgFnPc1d1	vodní
molekuly	molekula	k1gFnPc1	molekula
střetly	střetnout	k5eAaPmAgFnP	střetnout
s	s	k7c7	s
částicemi	částice	k1gFnPc7	částice
slunečního	sluneční	k2eAgInSc2d1	sluneční
větru	vítr	k1gInSc2	vítr
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
jejich	jejich	k3xOp3gInSc3	jejich
rozpadu	rozpad	k1gInSc3	rozpad
na	na	k7c4	na
kyslík	kyslík	k1gInSc4	kyslík
a	a	k8xC	a
vodík	vodík	k1gInSc4	vodík
a	a	k8xC	a
úniku	únik	k1gInSc3	únik
volných	volný	k2eAgFnPc2d1	volná
částic	částice	k1gFnPc2	částice
z	z	k7c2	z
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
tlak	tlak	k1gInSc4	tlak
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Venuše	Venuše	k1gFnSc2	Venuše
přibližně	přibližně	k6eAd1	přibližně
92násobku	92násobek	k1gInSc2	92násobek
tlaku	tlak	k1gInSc2	tlak
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Venuše	Venuše	k1gFnSc1	Venuše
byla	být	k5eAaImAgFnS	být
známa	znám	k2eAgFnSc1d1	známa
již	již	k6eAd1	již
starým	starý	k2eAgMnPc3d1	starý
Babyloňanům	Babyloňan	k1gMnPc3	Babyloňan
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1600	[number]	k4	1600
př.	př.	kA	př.
n.	n.	k?	n.
l.	l.	k?	l.
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
byla	být	k5eAaImAgFnS	být
pozorována	pozorovat	k5eAaImNgFnS	pozorovat
dlouho	dlouho	k6eAd1	dlouho
předtím	předtím	k6eAd1	předtím
v	v	k7c6	v
prehistorických	prehistorický	k2eAgFnPc6d1	prehistorická
dobách	doba	k1gFnPc6	doba
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
jasné	jasný	k2eAgFnSc3d1	jasná
viditelnosti	viditelnost	k1gFnSc3	viditelnost
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
symbolem	symbol	k1gInSc7	symbol
je	být	k5eAaImIp3nS	být
stylizované	stylizovaný	k2eAgNnSc4d1	stylizované
znázornění	znázornění	k1gNnSc4	znázornění
bohyně	bohyně	k1gFnSc2	bohyně
Venuše	Venuše	k1gFnSc2	Venuše
držící	držící	k2eAgNnSc1d1	držící
zrcadlo	zrcadlo	k1gNnSc1	zrcadlo
<g/>
:	:	kIx,	:
kruh	kruh	k1gInSc1	kruh
s	s	k7c7	s
malým	malý	k2eAgInSc7d1	malý
křížem	kříž	k1gInSc7	kříž
pod	pod	k7c7	pod
ním	on	k3xPp3gNnSc7	on
(	(	kIx(	(
<g/>
v	v	k7c6	v
Unicode	Unicod	k1gInSc5	Unicod
<g/>
:	:	kIx,	:
♀	♀	k?	♀
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
sovětského	sovětský	k2eAgInSc2d1	sovětský
programu	program	k1gInSc2	program
Veněra	Veněra	k1gFnSc1	Veněra
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
probíhal	probíhat	k5eAaImAgInS	probíhat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1961	[number]	k4	1961
<g/>
–	–	k?	–
<g/>
1983	[number]	k4	1983
bylo	být	k5eAaImAgNnS	být
k	k	k7c3	k
Venuši	Venuše	k1gFnSc3	Venuše
vypuštěno	vypustit	k5eAaPmNgNnS	vypustit
16	[number]	k4	16
sond	sonda	k1gFnPc2	sonda
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
mapa	mapa	k1gFnSc1	mapa
povrchu	povrch	k1gInSc2	povrch
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
zhotovena	zhotovit	k5eAaPmNgFnS	zhotovit
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
90.	[number]	k4	90.
letech	léto	k1gNnPc6	léto
20.	[number]	k4	20.
století	století	k1gNnSc2	století
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
projektu	projekt	k1gInSc2	projekt
Magellan	Magellan	k1gMnSc1	Magellan
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc4	tento
snímky	snímek	k1gInPc4	snímek
přinesly	přinést	k5eAaPmAgInP	přinést
poznatky	poznatek	k1gInPc1	poznatek
o	o	k7c6	o
silné	silný	k2eAgFnSc6d1	silná
sopečné	sopečný	k2eAgFnSc6d1	sopečná
aktivitě	aktivita	k1gFnSc6	aktivita
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Venuše	Venuše	k1gFnSc2	Venuše
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
přítomností	přítomnost	k1gFnSc7	přítomnost
síry	síra	k1gFnSc2	síra
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
domněnkám	domněnka	k1gFnPc3	domněnka
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
na	na	k7c4	na
Venuši	Venuše	k1gFnSc4	Venuše
nachází	nacházet	k5eAaImIp3nS	nacházet
aktivní	aktivní	k2eAgInSc1d1	aktivní
vulkanismus	vulkanismus	k1gInSc1	vulkanismus
i	i	k9	i
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
průzkumu	průzkum	k1gInSc6	průzkum
snímků	snímek	k1gInPc2	snímek
ale	ale	k9	ale
nebyly	být	k5eNaImAgInP	být
nalezeny	naleznout	k5eAaPmNgInP	naleznout
žádné	žádný	k3yNgInPc1	žádný
doklady	doklad	k1gInPc1	doklad
lávových	lávový	k2eAgInPc2d1	lávový
proudů	proud	k1gInPc2	proud
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
by	by	kYmCp3nP	by
pocházely	pocházet	k5eAaImAgInP	pocházet
z	z	k7c2	z
nedávné	dávný	k2eNgFnSc2d1	nedávná
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
bylo	být	k5eAaImAgNnS	být
překvapivě	překvapivě	k6eAd1	překvapivě
pozorováno	pozorovat	k5eAaImNgNnS	pozorovat
jen	jen	k6eAd1	jen
malé	malý	k2eAgNnSc1d1	malé
množství	množství	k1gNnSc1	množství
impaktních	impaktní	k2eAgInPc2d1	impaktní
kráterů	kráter	k1gInPc2	kráter
naznačující	naznačující	k2eAgNnSc1d1	naznačující
<g/>
,	,	kIx,	,
že	že	k8xS	že
celý	celý	k2eAgInSc4d1	celý
povrch	povrch	k1gInSc4	povrch
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
mladý	mladý	k2eAgMnSc1d1	mladý
o	o	k7c4	o
stáří	stáří	k1gNnSc4	stáří
přibližně	přibližně	k6eAd1	přibližně
půl	půl	k1xP	půl
miliardy	miliarda	k4xCgFnSc2	miliarda
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Země	zem	k1gFnSc2	zem
====	====	k?	====
</s>
</p>
<p>
</p>
<p>
<s>
Země	země	k1gFnSc1	země
je	být	k5eAaImIp3nS	být
třetí	třetí	k4xOgFnSc1	třetí
planeta	planeta	k1gFnSc1	planeta
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
největší	veliký	k2eAgFnSc1d3	veliký
terestrická	terestrický	k2eAgFnSc1d1	terestrická
planeta	planeta	k1gFnSc1	planeta
v	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
a	a	k8xC	a
jediné	jediný	k2eAgNnSc1d1	jediné
planetární	planetární	k2eAgNnSc1d1	planetární
těleso	těleso	k1gNnSc1	těleso
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
je	být	k5eAaImIp3nS	být
dle	dle	k7c2	dle
současných	současný	k2eAgInPc2d1	současný
vědeckých	vědecký	k2eAgInPc2d1	vědecký
poznatků	poznatek	k1gInPc2	poznatek
potvrzen	potvrdit	k5eAaPmNgInS	potvrdit
život	život	k1gInSc1	život
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
nejspíše	nejspíše	k9	nejspíše
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
před	před	k7c7	před
4,6	[number]	k4	4,6
miliardami	miliarda	k4xCgFnPc7	miliarda
let	léto	k1gNnPc2	léto
a	a	k8xC	a
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
vzniku	vznik	k1gInSc6	vznik
získala	získat	k5eAaPmAgFnS	získat
svůj	svůj	k3xOyFgInSc4	svůj
jediný	jediný	k2eAgInSc4d1	jediný
přirozený	přirozený	k2eAgInSc4d1	přirozený
satelit	satelit	k1gInSc4	satelit
–	–	k?	–
Měsíc	měsíc	k1gInSc1	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
obíhá	obíhat	k5eAaImIp3nS	obíhat
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
po	po	k7c6	po
téměř	téměř	k6eAd1	téměř
kružnicové	kružnicový	k2eAgFnSc6d1	kružnicová
dráze	dráha	k1gFnSc6	dráha
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
malou	malý	k2eAgFnSc7d1	malá
excentricitou	excentricita	k1gFnSc7	excentricita
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
jako	jako	k8xC	jako
domovský	domovský	k2eAgInSc1d1	domovský
svět	svět	k1gInSc1	svět
lidstva	lidstvo	k1gNnSc2	lidstvo
má	mít	k5eAaImIp3nS	mít
mnoho	mnoho	k4c1	mnoho
názvů	název	k1gInPc2	název
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
národu	národ	k1gInSc6	národ
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgInPc4d3	nejznámější
patří	patřit	k5eAaImIp3nS	patřit
název	název	k1gInSc4	název
latinského	latinský	k2eAgInSc2d1	latinský
původu	původ	k1gInSc2	původ
Terra	Terra	k1gFnSc1	Terra
<g/>
,	,	kIx,	,
Tellus	Tellus	k1gInSc1	Tellus
či	či	k8xC	či
řecký	řecký	k2eAgInSc1d1	řecký
název	název	k1gInSc1	název
Gaia	Gai	k1gInSc2	Gai
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Země	země	k1gFnSc1	země
je	být	k5eAaImIp3nS	být
dynamickou	dynamický	k2eAgFnSc7d1	dynamická
planetou	planeta	k1gFnSc7	planeta
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
zemských	zemský	k2eAgFnPc2d1	zemská
sfér	sféra	k1gFnPc2	sféra
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nedokonalou	dokonalý	k2eNgFnSc4d1	nedokonalá
kouli	koule	k1gFnSc4	koule
s	s	k7c7	s
poloměrem	poloměr	k1gInSc7	poloměr
6378	[number]	k4	6378
km	km	kA	km
<g/>
,	,	kIx,	,
uprostřed	uprostřed	k6eAd1	uprostřed
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
malé	malý	k2eAgNnSc1d1	malé
pevné	pevný	k2eAgNnSc1d1	pevné
jadérko	jadérko	k1gNnSc1	jadérko
obklopené	obklopený	k2eAgNnSc1d1	obklopené
polotekutým	polotekutý	k2eAgNnSc7d1	polotekuté
vnějším	vnější	k2eAgNnSc7d1	vnější
jádrem	jádro	k1gNnSc7	jádro
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
pláštěm	plášť	k1gInSc7	plášť
a	a	k8xC	a
zemskou	zemský	k2eAgFnSc7d1	zemská
kůrou	kůra	k1gFnSc7	kůra
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
oceánskou	oceánský	k2eAgFnSc4d1	oceánská
a	a	k8xC	a
kontinentální	kontinentální	k2eAgFnSc4d1	kontinentální
<g/>
.	.	kIx.	.
</s>
<s>
Zemská	zemský	k2eAgFnSc1d1	zemská
kůra	kůra	k1gFnSc1	kůra
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
litosférickými	litosférický	k2eAgFnPc7d1	litosférická
deskami	deska	k1gFnPc7	deska
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
neustálém	neustálý	k2eAgInSc6d1	neustálý
pohybu	pohyb	k1gInSc6	pohyb
vlivem	vliv	k1gInSc7	vliv
procesu	proces	k1gInSc2	proces
nazývaného	nazývaný	k2eAgInSc2d1	nazývaný
desková	deskový	k2eAgFnSc1d1	desková
tektonika	tektonika	k1gFnSc1	tektonika
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
hydrosféra	hydrosféra	k1gFnSc1	hydrosféra
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
souvislého	souvislý	k2eAgInSc2d1	souvislý
oceánu	oceán	k1gInSc2	oceán
kapalné	kapalný	k2eAgFnSc2d1	kapalná
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zabírá	zabírat	k5eAaImIp3nS	zabírat
přibližně	přibližně	k6eAd1	přibližně
71	[number]	k4	71
%	%	kIx~	%
zemského	zemský	k2eAgInSc2d1	zemský
povrchu	povrch	k1gInSc2	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
velmi	velmi	k6eAd1	velmi
úzkém	úzký	k2eAgInSc6d1	úzký
pásu	pás	k1gInSc6	pás
rozhraní	rozhraní	k1gNnSc2	rozhraní
mezi	mezi	k7c7	mezi
litosférou	litosféra	k1gFnSc7	litosféra
a	a	k8xC	a
atmosférou	atmosféra	k1gFnSc7	atmosféra
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
biosféra	biosféra	k1gFnSc1	biosféra
<g/>
,	,	kIx,	,
živý	živý	k2eAgInSc4d1	živý
obal	obal	k1gInSc4	obal
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
živými	živý	k2eAgInPc7d1	živý
organismy	organismus	k1gInPc7	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
činností	činnost	k1gFnSc7	činnost
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
přeměně	přeměna	k1gFnSc3	přeměna
části	část	k1gFnSc2	část
litosféry	litosféra	k1gFnSc2	litosféra
na	na	k7c4	na
půdní	půdní	k2eAgInSc4d1	půdní
obal	obal	k1gInSc4	obal
Země	zem	k1gFnSc2	zem
tzv.	tzv.	kA	tzv.
pedosféru	pedosféra	k1gFnSc4	pedosféra
<g/>
.	.	kIx.	.
</s>
<s>
Celou	celý	k2eAgFnSc4d1	celá
planetu	planeta	k1gFnSc4	planeta
obklopuje	obklopovat	k5eAaImIp3nS	obklopovat
hustá	hustý	k2eAgFnSc1d1	hustá
atmosféra	atmosféra	k1gFnSc1	atmosféra
tvořená	tvořený	k2eAgFnSc1d1	tvořená
převážně	převážně	k6eAd1	převážně
dusíkem	dusík	k1gInSc7	dusík
a	a	k8xC	a
kyslíkem	kyslík	k1gInSc7	kyslík
vytvářející	vytvářející	k2eAgFnSc4d1	vytvářející
směs	směs	k1gFnSc4	směs
obvykle	obvykle	k6eAd1	obvykle
nazývanou	nazývaný	k2eAgFnSc4d1	nazývaná
jako	jako	k8xC	jako
vzduch	vzduch	k1gInSc4	vzduch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Její	její	k3xOp3gInSc1	její
astronomický	astronomický	k2eAgInSc1d1	astronomický
symbol	symbol	k1gInSc1	symbol
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
kříže	kříž	k1gInSc2	kříž
v	v	k7c6	v
kruhu	kruh	k1gInSc6	kruh
<g/>
,	,	kIx,	,
reprezentujícího	reprezentující	k2eAgMnSc2d1	reprezentující
poledník	poledník	k1gInSc1	poledník
a	a	k8xC	a
rovník	rovník	k1gInSc1	rovník
<g/>
;	;	kIx,	;
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
variantách	varianta	k1gFnPc6	varianta
je	být	k5eAaImIp3nS	být
kříž	kříž	k1gInSc1	kříž
vysunut	vysunout	k5eAaPmNgInS	vysunout
nad	nad	k7c4	nad
kruh	kruh	k1gInSc4	kruh
(	(	kIx(	(
<g/>
Unicode	Unicod	k1gMnSc5	Unicod
<g/>
:	:	kIx,	:
⊕	⊕	k?	⊕
nebo	nebo	k8xC	nebo
♁	♁	k?	♁
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
slov	slovo	k1gNnPc2	slovo
odvozených	odvozený	k2eAgInPc2d1	odvozený
od	od	k7c2	od
Terra	Terro	k1gNnSc2	Terro
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
terestrický	terestrický	k2eAgMnSc1d1	terestrický
<g/>
,	,	kIx,	,
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
pojmy	pojem	k1gInPc4	pojem
vztahující	vztahující	k2eAgInPc4d1	vztahující
se	se	k3xPyFc4	se
k	k	k7c3	k
Zemi	zem	k1gFnSc3	zem
také	také	k9	také
prefix	prefix	k1gInSc1	prefix
telur	telur	k1gInSc1	telur
<g/>
-	-	kIx~	-
nebo	nebo	k8xC	nebo
tellur	tellur	k1gInSc4	tellur
<g/>
-	-	kIx~	-
(	(	kIx(	(
<g/>
např.	např.	kA	např.
telurický	telurický	k2eAgMnSc1d1	telurický
<g/>
,	,	kIx,	,
tellurit	tellurit	k1gInSc1	tellurit
podle	podle	k7c2	podle
bohyně	bohyně	k1gFnSc2	bohyně
Tellū	Tellū	k1gFnSc2	Tellū
<g/>
)	)	kIx)	)
a	a	k8xC	a
geo	geo	k?	geo
<g/>
-	-	kIx~	-
(	(	kIx(	(
<g/>
např.	např.	kA	např.
geocentrický	geocentrický	k2eAgInSc1d1	geocentrický
model	model	k1gInSc1	model
<g/>
,	,	kIx,	,
geologie	geologie	k1gFnSc1	geologie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
je	být	k5eAaImIp3nS	být
domovským	domovský	k2eAgInSc7d1	domovský
světem	svět	k1gInSc7	svět
lidstva	lidstvo	k1gNnSc2	lidstvo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
na	na	k7c6	na
přibližně	přibližně	k6eAd1	přibližně
200	[number]	k4	200
nezávislých	závislý	k2eNgInPc2d1	nezávislý
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
spolu	spolu	k6eAd1	spolu
ve	v	k7c6	v
vzájemném	vzájemný	k2eAgNnSc6d1	vzájemné
působení	působení	k1gNnSc6	působení
skrze	skrze	k?	skrze
diplomacii	diplomacie	k1gFnSc4	diplomacie
<g/>
,	,	kIx,	,
cestování	cestování	k1gNnSc4	cestování
a	a	k8xC	a
obchod	obchod	k1gInSc4	obchod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Mars	Mars	k1gInSc1	Mars
====	====	k?	====
</s>
</p>
<p>
</p>
<p>
<s>
Mars	Mars	k1gInSc1	Mars
je	být	k5eAaImIp3nS	být
čtvrtá	čtvrtý	k4xOgFnSc1	čtvrtý
planeta	planeta	k1gFnSc1	planeta
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
nejmenší	malý	k2eAgFnSc1d3	nejmenší
planeta	planeta	k1gFnSc1	planeta
soustavy	soustava	k1gFnSc2	soustava
po	po	k7c6	po
Merkuru	Merkur	k1gInSc6	Merkur
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pojmenována	pojmenovat	k5eAaPmNgFnS	pojmenovat
po	po	k7c6	po
římském	římský	k2eAgMnSc6d1	římský
bohu	bůh	k1gMnSc6	bůh
války	válka	k1gFnSc2	válka
Martovi	Mars	k1gMnSc3	Mars
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
planetu	planeta	k1gFnSc4	planeta
terestrického	terestrický	k2eAgInSc2d1	terestrický
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
má	mít	k5eAaImIp3nS	mít
pevný	pevný	k2eAgInSc1d1	pevný
horninový	horninový	k2eAgInSc1d1	horninový
povrch	povrch	k1gInSc1	povrch
pokrytý	pokrytý	k2eAgInSc1d1	pokrytý
impaktními	impaktní	k2eAgInPc7d1	impaktní
krátery	kráter	k1gInPc7	kráter
<g/>
,	,	kIx,	,
vysokými	vysoký	k2eAgFnPc7d1	vysoká
sopkami	sopka	k1gFnPc7	sopka
<g/>
,	,	kIx,	,
hlubokými	hluboký	k2eAgInPc7d1	hluboký
kaňony	kaňon	k1gInPc7	kaňon
a	a	k8xC	a
dalšími	další	k2eAgInPc7d1	další
útvary	útvar	k1gInPc7	útvar
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgInPc4	dva
malé	malý	k2eAgInPc4d1	malý
měsíce	měsíc	k1gInPc4	měsíc
nepravidelného	pravidelný	k2eNgInSc2d1	nepravidelný
tvaru	tvar	k1gInSc2	tvar
pojmenované	pojmenovaný	k2eAgNnSc1d1	pojmenované
Phobos	Phobos	k1gInSc4	Phobos
a	a	k8xC	a
Deimos	Deimos	k1gInSc4	Deimos
<g/>
.	.	kIx.	.
</s>
<s>
Deimos	Deimos	k1gInSc1	Deimos
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
Δ	Δ	k?	Δ
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Hrůza	hrůza	k6eAd1	hrůza
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vnějším	vnější	k2eAgMnSc7d1	vnější
a	a	k8xC	a
menším	malý	k2eAgMnPc3d2	menší
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
známých	známý	k2eAgInPc2d1	známý
měsíců	měsíc	k1gInPc2	měsíc
planety	planeta	k1gFnSc2	planeta
Marsu	Mars	k1gInSc2	Mars
<g/>
.	.	kIx.	.
</s>
<s>
Phobos	Phobos	k1gInSc1	Phobos
<g/>
,	,	kIx,	,
též	též	k9	též
psáno	psán	k2eAgNnSc1d1	psáno
Fobos	Fobos	k1gInSc4	Fobos
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
Φ	Φ	k?	Φ
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Děs	děs	k1gInSc1	děs
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vnitřním	vnitřní	k2eAgInSc7d1	vnitřní
a	a	k8xC	a
větším	veliký	k2eAgInSc7d2	veliký
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
známých	známý	k2eAgInPc2d1	známý
měsíců	měsíc	k1gInPc2	měsíc
planety	planeta	k1gFnSc2	planeta
Marsu	Mars	k1gInSc2	Mars
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
Mars	Mars	k1gInSc1	Mars
v	v	k7c6	v
opozici	opozice	k1gFnSc6	opozice
ke	k	k7c3	k
Slunci	slunce	k1gNnSc3	slunce
a	a	k8xC	a
Země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
nachází	nacházet	k5eAaImIp3nS	nacházet
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgInPc7	tento
dvěma	dva	k4xCgInPc7	dva
tělesy	těleso	k1gNnPc7	těleso
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Mars	Mars	k1gInSc1	Mars
pozorovatelný	pozorovatelný	k2eAgInSc1d1	pozorovatelný
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
noc	noc	k1gFnSc4	noc
<g/>
.	.	kIx.	.
</s>
<s>
Spolehlivé	spolehlivý	k2eAgFnPc1d1	spolehlivá
informace	informace	k1gFnPc1	informace
o	o	k7c6	o
prvních	první	k4xOgNnPc6	první
pozorováních	pozorování	k1gNnPc6	pozorování
Marsu	Mars	k1gInSc2	Mars
jako	jako	k9	jako
planety	planeta	k1gFnPc1	planeta
neexistují	existovat	k5eNaImIp3nP	existovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
došlo	dojít	k5eAaPmAgNnS	dojít
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
3000	[number]	k4	3000
až	až	k9	až
4000	[number]	k4	4000
př.	př.	kA	př.
n.	n.	k?	n.
l.	l.	k?	l.
Všechny	všechen	k3xTgFnPc1	všechen
starověké	starověký	k2eAgFnPc1d1	starověká
civilizace	civilizace	k1gFnPc1	civilizace
<g/>
,	,	kIx,	,
Egypťané	Egypťan	k1gMnPc1	Egypťan
<g/>
,	,	kIx,	,
Babylóňané	Babylóňan	k1gMnPc1	Babylóňan
a	a	k8xC	a
Řekové	Řek	k1gMnPc1	Řek
<g/>
,	,	kIx,	,
znaly	znát	k5eAaImAgFnP	znát
tuto	tento	k3xDgFnSc4	tento
"	"	kIx"	"
<g/>
putující	putující	k2eAgFnSc4d1	putující
hvězdu	hvězda	k1gFnSc4	hvězda
<g/>
"	"	kIx"	"
a	a	k8xC	a
měly	mít	k5eAaImAgFnP	mít
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
svá	svůj	k3xOyFgNnPc4	svůj
pojmenování	pojmenování	k1gNnSc4	pojmenování
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
jejímu	její	k3xOp3gInSc3	její
načervenalému	načervenalý	k2eAgInSc3d1	načervenalý
nádechu	nádech	k1gInSc3	nádech
<g/>
,	,	kIx,	,
způsobenému	způsobený	k2eAgMnSc3d1	způsobený
červenou	červený	k2eAgFnSc7d1	červená
barvou	barva	k1gFnSc7	barva
zoxidované	zoxidovaný	k2eAgFnSc2d1	zoxidovaná
půdy	půda	k1gFnSc2	půda
na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
povrchu	povrch	k1gInSc6	povrch
<g/>
,	,	kIx,	,
považovaly	považovat	k5eAaImAgInP	považovat
staré	starý	k2eAgInPc1d1	starý
národy	národ	k1gInPc1	národ
Mars	Mars	k1gInSc4	Mars
většinou	většina	k1gFnSc7	většina
za	za	k7c4	za
symbol	symbol	k1gInSc4	symbol
ohně	oheň	k1gInSc2	oheň
<g/>
,	,	kIx,	,
krve	krev	k1gFnSc2	krev
a	a	k8xC	a
zániku	zánik	k1gInSc2	zánik
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Detailní	detailní	k2eAgNnSc1d1	detailní
zkoumání	zkoumání	k1gNnSc1	zkoumání
planety	planeta	k1gFnSc2	planeta
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
od	od	k7c2	od
60.	[number]	k4	60.
let	léto	k1gNnPc2	léto
20.	[number]	k4	20.
století	století	k1gNnSc2	století
takřka	takřka	k6eAd1	takřka
20	[number]	k4	20
úspěšných	úspěšný	k2eAgFnPc2d1	úspěšná
automatických	automatický	k2eAgFnPc2d1	automatická
sond	sonda	k1gFnPc2	sonda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
kolem	kolem	k7c2	kolem
Marsu	Mars	k1gInSc2	Mars
tři	tři	k4xCgFnPc4	tři
funkční	funkční	k2eAgFnPc4d1	funkční
sondy	sonda	k1gFnPc4	sonda
(	(	kIx(	(
<g/>
Mars	Mars	k1gInSc1	Mars
Odyssey	Odyssea	k1gFnSc2	Odyssea
<g/>
,	,	kIx,	,
Mars	Mars	k1gInSc1	Mars
Express	express	k1gInSc1	express
a	a	k8xC	a
Mars	Mars	k1gInSc1	Mars
Reconnaissance	Reconnaissance	k1gFnSc2	Reconnaissance
Orbiter	Orbitra	k1gFnPc2	Orbitra
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
planety	planeta	k1gFnSc2	planeta
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
dvě	dva	k4xCgNnPc1	dva
vozítka	vozítko	k1gNnPc1	vozítko
(	(	kIx(	(
<g/>
Opportunity	Opportunita	k1gFnSc2	Opportunita
a	a	k8xC	a
Curiosity	Curiosita	k1gFnSc2	Curiosita
<g/>
)	)	kIx)	)
která	který	k3yQgFnSc1	který
poskytla	poskytnout	k5eAaPmAgFnS	poskytnout
data	datum	k1gNnSc2	datum
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
umožnila	umožnit	k5eAaPmAgFnS	umožnit
zmapovat	zmapovat	k5eAaPmF	zmapovat
větší	veliký	k2eAgFnSc4d2	veliký
část	část	k1gFnSc4	část
povrchu	povrch	k1gInSc2	povrch
<g/>
,	,	kIx,	,
definovat	definovat	k5eAaBmF	definovat
základní	základní	k2eAgNnPc4d1	základní
historická	historický	k2eAgNnPc4d1	historické
období	období	k1gNnPc4	období
či	či	k8xC	či
porozumět	porozumět	k5eAaPmF	porozumět
základním	základní	k2eAgInPc3d1	základní
jevům	jev	k1gInPc3	jev
odehrávajícím	odehrávající	k2eAgInPc3d1	odehrávající
se	se	k3xPyFc4	se
na	na	k7c6	na
planetě	planeta	k1gFnSc6	planeta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Hlavní	hlavní	k2eAgInSc1d1	hlavní
pás	pás	k1gInSc1	pás
asteroidů	asteroid	k1gInPc2	asteroid
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
pás	pás	k1gInSc1	pás
asteroidů	asteroid	k1gInPc2	asteroid
je	být	k5eAaImIp3nS	být
soustava	soustava	k1gFnSc1	soustava
planetek	planetka	k1gFnPc2	planetka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
obíhají	obíhat	k5eAaImIp3nP	obíhat
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
mezi	mezi	k7c7	mezi
drahami	draha	k1gFnPc7	draha
Marsu	Mars	k1gInSc2	Mars
a	a	k8xC	a
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
ve	v	k7c6	v
vzdálenostech	vzdálenost	k1gFnPc6	vzdálenost
od	od	k7c2	od
2	[number]	k4	2
AU	au	k0	au
do	do	k7c2	do
4	[number]	k4	4
AU	au	k0	au
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
se	se	k3xPyFc4	se
vytvořily	vytvořit	k5eAaPmAgFnP	vytvořit
z	z	k7c2	z
protoplanetárního	protoplanetární	k2eAgInSc2d1	protoplanetární
disku	disk	k1gInSc2	disk
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
gravitačního	gravitační	k2eAgInSc2d1	gravitační
vlivu	vliv	k1gInSc2	vliv
Jupiteru	Jupiter	k1gInSc2	Jupiter
nemohlo	moct	k5eNaImAgNnS	moct
vytvořit	vytvořit	k5eAaPmF	vytvořit
jediné	jediný	k2eAgNnSc1d1	jediné
velké	velký	k2eAgNnSc1d1	velké
těleso	těleso	k1gNnSc1	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgFnPc1d1	mnohá
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
dodatečně	dodatečně	k6eAd1	dodatečně
rozpadem	rozpad	k1gInSc7	rozpad
původně	původně	k6eAd1	původně
vniklých	vniklý	k2eAgNnPc2d1	vniklé
těles	těleso	k1gNnPc2	těleso
při	při	k7c6	při
jejich	jejich	k3xOp3gFnPc6	jejich
vzájemných	vzájemný	k2eAgFnPc6d1	vzájemná
srážkách	srážka	k1gFnPc6	srážka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
bylo	být	k5eAaImAgNnS	být
známo	znám	k2eAgNnSc1d1	známo
přes	přes	k7c4	přes
300 000	[number]	k4	300 000
těles	těleso	k1gNnPc2	těleso
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Vnější	vnější	k2eAgFnPc1d1	vnější
planety	planeta	k1gFnPc1	planeta
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Jupiter	Jupiter	k1gMnSc1	Jupiter
====	====	k?	====
</s>
</p>
<p>
</p>
<p>
<s>
Jupiter	Jupiter	k1gMnSc1	Jupiter
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc1d3	veliký
planeta	planeta	k1gFnSc1	planeta
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
v	v	k7c4	v
pořadí	pořadí	k1gNnSc4	pořadí
pátá	pátá	k1gFnSc1	pátá
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Sluneční	sluneční	k2eAgFnSc1d1	sluneční
soustava	soustava	k1gFnSc1	soustava
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
popisována	popisován	k2eAgFnSc1d1	popisována
jako	jako	k8xC	jako
dvojsystém	dvojsystý	k2eAgInSc6d1	dvojsystý
skládající	skládající	k2eAgInPc4d1	skládající
se	se	k3xPyFc4	se
ze	z	k7c2	z
Slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
Jupiteru	Jupiter	k1gInSc2	Jupiter
jako	jako	k8xC	jako
hlavních	hlavní	k2eAgInPc2d1	hlavní
dvou	dva	k4xCgMnPc2	dva
členů	člen	k1gMnPc2	člen
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
menších	malý	k2eAgNnPc2d2	menší
těles	těleso	k1gNnPc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Jupiter	Jupiter	k1gMnSc1	Jupiter
<g/>
,	,	kIx,	,
Saturn	Saturn	k1gMnSc1	Saturn
<g/>
,	,	kIx,	,
Uran	Uran	k1gMnSc1	Uran
<g/>
,	,	kIx,	,
a	a	k8xC	a
Neptun	Neptun	k1gInSc1	Neptun
jsou	být	k5eAaImIp3nP	být
označovány	označovat	k5eAaImNgInP	označovat
jako	jako	k9	jako
plynní	plynný	k2eAgMnPc1d1	plynný
obři	obr	k1gMnPc1	obr
<g/>
,	,	kIx,	,
či	či	k8xC	či
planety	planeta	k1gFnPc1	planeta
jupiterského	jupiterský	k2eAgInSc2d1	jupiterský
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Jupiter	Jupiter	k1gMnSc1	Jupiter
má	mít	k5eAaImIp3nS	mít
hmotnost	hmotnost	k1gFnSc4	hmotnost
přibližně	přibližně	k6eAd1	přibližně
jedné	jeden	k4xCgFnSc2	jeden
tisíciny	tisícina	k1gFnSc2	tisícina
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
okolo	okolo	k6eAd1	okolo
dva	dva	k4xCgInPc4	dva
a	a	k8xC	a
půl	půl	k1xP	půl
krát	krát	k6eAd1	krát
více	hodně	k6eAd2	hodně
než	než	k8xS	než
všechny	všechen	k3xTgFnPc1	všechen
ostatní	ostatní	k2eAgFnPc1d1	ostatní
planety	planeta	k1gFnPc1	planeta
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
dohromady	dohromady	k6eAd1	dohromady
<g/>
.	.	kIx.	.
</s>
<s>
Planeta	planeta	k1gFnSc1	planeta
je	být	k5eAaImIp3nS	být
pojmenována	pojmenovat	k5eAaPmNgFnS	pojmenovat
po	po	k7c6	po
římském	římský	k2eAgMnSc6d1	římský
bohu	bůh	k1gMnSc6	bůh
Jovovi	Jova	k1gMnSc6	Jova
(	(	kIx(	(
<g/>
v	v	k7c6	v
1.	[number]	k4	1.
pádě	pád	k1gInSc6	pád
Jupiter	Jupiter	k1gMnSc1	Jupiter
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Symbolem	symbol	k1gInSc7	symbol
planety	planeta	k1gFnSc2	planeta
je	být	k5eAaImIp3nS	být
stylizované	stylizovaný	k2eAgNnSc4d1	stylizované
znázornění	znázornění	k1gNnSc4	znázornění
božského	božský	k2eAgInSc2d1	božský
blesku	blesk	k1gInSc2	blesk
(	(	kIx(	(
<g/>
v	v	k7c6	v
Unicode	Unicod	k1gInSc5	Unicod
<g/>
:	:	kIx,	:
♃	♃	k?	♃
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jupiter	Jupiter	k1gInSc1	Jupiter
byl	být	k5eAaImAgInS	být
pozorován	pozorovat	k5eAaImNgInS	pozorovat
již	již	k6eAd1	již
od	od	k7c2	od
pradávna	pradávno	k1gNnSc2	pradávno
<g/>
,	,	kIx,	,
při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
má	mít	k5eAaImIp3nS	mít
Jupiter	Jupiter	k1gMnSc1	Jupiter
magnitudu	magnitud	k1gInSc2	magnitud
−	−	k?	−
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
<g/>
8	[number]	k4	8
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
činí	činit	k5eAaImIp3nS	činit
třetí	třetí	k4xOgInSc1	třetí
nejjasnější	jasný	k2eAgInSc1d3	nejjasnější
objekt	objekt	k1gInSc1	objekt
na	na	k7c6	na
noční	noční	k2eAgFnSc6d1	noční
obloze	obloha	k1gFnSc6	obloha
po	po	k7c6	po
Měsíci	měsíc	k1gInSc6	měsíc
a	a	k8xC	a
Venuši	Venuše	k1gFnSc6	Venuše
(	(	kIx(	(
<g/>
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
se	se	k3xPyFc4	se
před	před	k7c4	před
Jupiter	Jupiter	k1gInSc4	Jupiter
v	v	k7c6	v
jasnosti	jasnost	k1gFnSc6	jasnost
dostane	dostat	k5eAaPmIp3nS	dostat
Mars	Mars	k1gInSc1	Mars
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ideální	ideální	k2eAgFnSc6d1	ideální
pozici	pozice	k1gFnSc6	pozice
během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
oběhu	oběh	k1gInSc2	oběh
vůči	vůči	k7c3	vůči
Zemi	zem	k1gFnSc3	zem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Okolo	okolo	k7c2	okolo
planety	planeta	k1gFnSc2	planeta
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
slabé	slabý	k2eAgInPc1d1	slabý
prstence	prstenec	k1gInPc1	prstenec
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
špatně	špatně	k6eAd1	špatně
viditelné	viditelný	k2eAgFnPc1d1	viditelná
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
ho	on	k3xPp3gNnSc4	on
obklopuje	obklopovat	k5eAaImIp3nS	obklopovat
silné	silný	k2eAgNnSc4d1	silné
radiační	radiační	k2eAgNnSc4d1	radiační
pole	pole	k1gNnSc4	pole
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
z	z	k7c2	z
okolního	okolní	k2eAgInSc2d1	okolní
vesmíru	vesmír	k1gInSc2	vesmír
jsou	být	k5eAaImIp3nP	být
viditelné	viditelný	k2eAgFnPc1d1	viditelná
horní	horní	k2eAgFnPc1d1	horní
vrstvy	vrstva	k1gFnPc1	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
rozčleněny	rozčleněn	k2eAgInPc4d1	rozčleněn
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
planetární	planetární	k2eAgFnSc6d1	planetární
šířce	šířka	k1gFnSc6	šířka
do	do	k7c2	do
různě	různě	k6eAd1	různě
barevných	barevný	k2eAgInPc2d1	barevný
pruhů	pruh	k1gInPc2	pruh
a	a	k8xC	a
skvrn	skvrna	k1gFnPc2	skvrna
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
jsou	být	k5eAaImIp3nP	být
atmosférickými	atmosférický	k2eAgFnPc7d1	atmosférická
bouřemi	bouř	k1gFnPc7	bouř
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgMnSc1d3	nejznámější
takovouto	takovýto	k3xDgFnSc7	takovýto
bouří	bouř	k1gFnSc7	bouř
je	být	k5eAaImIp3nS	být
Velká	velký	k2eAgFnSc1d1	velká
rudá	rudý	k2eAgFnSc1d1	rudá
skvrna	skvrna	k1gFnSc1	skvrna
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgNnPc4d1	známé
minimálně	minimálně	k6eAd1	minimálně
od	od	k7c2	od
17.	[number]	k4	17.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Dosud	dosud	k6eAd1	dosud
není	být	k5eNaImIp3nS	být
přesně	přesně	k6eAd1	přesně
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
jaké	jaký	k3yRgFnSc2	jaký
vrstvy	vrstva	k1gFnSc2	vrstva
planetu	planeta	k1gFnSc4	planeta
tvoří	tvořit	k5eAaImIp3nP	tvořit
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
současné	současný	k2eAgInPc4d1	současný
technické	technický	k2eAgInPc4d1	technický
prostředky	prostředek	k1gInPc4	prostředek
neumožňují	umožňovat	k5eNaImIp3nP	umožňovat
její	její	k3xOp3gInSc4	její
průzkum	průzkum	k1gInSc4	průzkum
do	do	k7c2	do
větší	veliký	k2eAgFnSc2d2	veliký
hloubky	hloubka	k1gFnSc2	hloubka
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
Jupiter	Jupiter	k1gInSc1	Jupiter
je	být	k5eAaImIp3nS	být
složen	složit	k5eAaPmNgInS	složit
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
vodíku	vodík	k1gInSc2	vodík
<g/>
,	,	kIx,	,
hélia	hélium	k1gNnSc2	hélium
a	a	k8xC	a
organických	organický	k2eAgFnPc2d1	organická
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
planeta	planeta	k1gFnSc1	planeta
má	mít	k5eAaImIp3nS	mít
tvrdé	tvrdý	k2eAgNnSc1d1	tvrdé
kamenné	kamenný	k2eAgNnSc1d1	kamenné
jádro	jádro	k1gNnSc1	jádro
tvořené	tvořený	k2eAgNnSc1d1	tvořené
těžšími	těžký	k2eAgInPc7d2	těžší
prvky	prvek	k1gInPc7	prvek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jupiter	Jupiter	k1gInSc1	Jupiter
byl	být	k5eAaImAgInS	být
prozkoumán	prozkoumat	k5eAaPmNgInS	prozkoumat
několika	několik	k4yIc7	několik
automatickými	automatický	k2eAgFnPc7d1	automatická
sondami	sonda	k1gFnPc7	sonda
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
programu	program	k1gInSc2	program
Pioneer	Pioneer	kA	Pioneer
a	a	k8xC	a
programu	program	k1gInSc3	program
Voyager	Voyagra	k1gFnPc2	Voyagra
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
všechny	všechen	k3xTgFnPc1	všechen
tyto	tento	k3xDgFnPc1	tento
sondy	sonda	k1gFnPc1	sonda
kolem	kolem	k7c2	kolem
planety	planeta	k1gFnSc2	planeta
proletěly	proletět	k5eAaPmAgInP	proletět
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
k	k	k7c3	k
Jupiteru	Jupiter	k1gInSc2	Jupiter
zamířila	zamířit	k5eAaPmAgFnS	zamířit
sonda	sonda	k1gFnSc1	sonda
Galileo	Galilea	k1gFnSc5	Galilea
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
kolem	kolem	k7c2	kolem
planety	planeta	k1gFnSc2	planeta
po	po	k7c4	po
necelých	celý	k2eNgNnPc2d1	necelé
osm	osm	k4xCc4	osm
let	léto	k1gNnPc2	léto
obíhala	obíhat	k5eAaImAgFnS	obíhat
<g/>
.	.	kIx.	.
</s>
<s>
Nejnovější	nový	k2eAgNnPc1d3	nejnovější
data	datum	k1gNnPc1	datum
pocházejí	pocházet	k5eAaImIp3nP	pocházet
ze	z	k7c2	z
sondy	sonda	k1gFnSc2	sonda
New	New	k1gFnSc2	New
Horizons	Horizonsa	k1gFnPc2	Horizonsa
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2007	[number]	k4	2007
použila	použít	k5eAaPmAgFnS	použít
planetu	planeta	k1gFnSc4	planeta
pro	pro	k7c4	pro
zvýšení	zvýšení	k1gNnSc4	zvýšení
rychlosti	rychlost	k1gFnSc2	rychlost
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
cestě	cesta	k1gFnSc6	cesta
k	k	k7c3	k
Plutu	plut	k1gInSc3	plut
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
plánují	plánovat	k5eAaImIp3nP	plánovat
další	další	k2eAgFnPc4d1	další
mise	mise	k1gFnPc4	mise
do	do	k7c2	do
soustavy	soustava	k1gFnSc2	soustava
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgFnP	mít
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
prozkoumat	prozkoumat	k5eAaPmF	prozkoumat
převážně	převážně	k6eAd1	převážně
hypotetické	hypotetický	k2eAgInPc1d1	hypotetický
oceány	oceán	k1gInPc1	oceán
pod	pod	k7c7	pod
ledovou	ledový	k2eAgFnSc7d1	ledová
kůrou	kůra	k1gFnSc7	kůra
jeho	on	k3xPp3gInSc2	on
měsíce	měsíc	k1gInSc2	měsíc
Europy	Europa	k1gFnSc2	Europa
<g/>
.	.	kIx.	.
</s>
<s>
Jupiter	Jupiter	k1gMnSc1	Jupiter
má	mít	k5eAaImIp3nS	mít
nejméně	málo	k6eAd3	málo
63	[number]	k4	63
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
objevil	objevit	k5eAaPmAgInS	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1610	[number]	k4	1610
Galileo	Galilea	k1gFnSc5	Galilea
Galilei	Galilei	k1gNnPc2	Galilei
a	a	k8xC	a
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
i	i	k9	i
Simon	Simon	k1gMnSc1	Simon
Marius	Marius	k1gMnSc1	Marius
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
čtyři	čtyři	k4xCgInPc4	čtyři
velké	velký	k2eAgInPc4d1	velký
měsíce	měsíc	k1gInPc4	měsíc
Io	Io	k1gFnSc2	Io
<g/>
,	,	kIx,	,
Europu	Europ	k1gInSc2	Europ
<g/>
,	,	kIx,	,
Ganymed	Ganymed	k1gMnSc1	Ganymed
a	a	k8xC	a
Callisto	Callista	k1gMnSc5	Callista
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
známé	známý	k2eAgInPc4d1	známý
jako	jako	k8xS	jako
Galileovy	Galileův	k2eAgInPc4d1	Galileův
měsíce	měsíc	k1gInPc4	měsíc
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
u	u	k7c2	u
jejichž	jejichž	k3xOyRp3gInSc2	jejichž
nebeského	nebeský	k2eAgInSc2d1	nebeský
pohybu	pohyb	k1gInSc2	pohyb
bylo	být	k5eAaImAgNnS	být
zřetelné	zřetelný	k2eAgNnSc1d1	zřetelné
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gNnSc7	jeho
centrem	centrum	k1gNnSc7	centrum
není	být	k5eNaImIp3nS	být
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skutečnost	skutečnost	k1gFnSc1	skutečnost
byla	být	k5eAaImAgFnS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
bodem	bod	k1gInSc7	bod
obhajoby	obhajoba	k1gFnSc2	obhajoba
Koperníkovy	Koperníkův	k2eAgFnSc2d1	Koperníkova
heliocentrické	heliocentrický	k2eAgFnSc2d1	heliocentrická
teorie	teorie	k1gFnSc2	teorie
o	o	k7c6	o
pohybu	pohyb	k1gInSc6	pohyb
planet	planeta	k1gFnPc2	planeta
<g/>
;	;	kIx,	;
Galileiho	Galilei	k1gMnSc2	Galilei
vyhlášení	vyhlášení	k1gNnSc4	vyhlášení
podpory	podpora	k1gFnPc4	podpora
Koperníkově	Koperníkův	k2eAgFnSc3d1	Koperníkova
teorii	teorie	k1gFnSc3	teorie
jej	on	k3xPp3gMnSc4	on
dostalo	dostat	k5eAaPmAgNnS	dostat
do	do	k7c2	do
problémů	problém	k1gInPc2	problém
s	s	k7c7	s
inkvizicí	inkvizice	k1gFnSc7	inkvizice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Saturn	Saturn	k1gInSc4	Saturn
====	====	k?	====
</s>
</p>
<p>
</p>
<p>
<s>
Saturn	Saturn	k1gInSc1	Saturn
je	být	k5eAaImIp3nS	být
šestá	šestý	k4xOgFnSc1	šestý
<g/>
,	,	kIx,	,
po	po	k7c6	po
Jupiteru	Jupiter	k1gInSc6	Jupiter
druhá	druhý	k4xOgFnSc1	druhý
největší	veliký	k2eAgFnSc1d3	veliký
planeta	planeta	k1gFnSc1	planeta
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Planeta	planeta	k1gFnSc1	planeta
byla	být	k5eAaImAgFnS	být
pozorována	pozorovat	k5eAaImNgFnS	pozorovat
již	již	k6eAd1	již
starověkými	starověký	k2eAgMnPc7d1	starověký
astronomy	astronom	k1gMnPc7	astronom
a	a	k8xC	a
byla	být	k5eAaImAgNnP	být
pojmenována	pojmenovat	k5eAaPmNgNnP	pojmenovat
po	po	k7c6	po
římském	římský	k2eAgMnSc6d1	římský
bohu	bůh	k1gMnSc6	bůh
Saturnovi	Saturn	k1gMnSc6	Saturn
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgInS	být
obdobou	obdoba	k1gFnSc7	obdoba
řeckého	řecký	k2eAgMnSc2d1	řecký
boha	bůh	k1gMnSc2	bůh
Krona	Kron	k1gMnSc2	Kron
<g/>
.	.	kIx.	.
</s>
<s>
Astronomický	astronomický	k2eAgInSc1d1	astronomický
symbol	symbol	k1gInSc1	symbol
pro	pro	k7c4	pro
Saturn	Saturn	k1gInSc4	Saturn
je	být	k5eAaImIp3nS	být
♄	♄	k?	♄
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Saturn	Saturn	k1gInSc1	Saturn
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
velké	velký	k2eAgMnPc4d1	velký
plynné	plynný	k2eAgMnPc4d1	plynný
obry	obr	k1gMnPc4	obr
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yQgFnPc4	který
je	být	k5eAaImIp3nS	být
typické	typický	k2eAgNnSc1d1	typické
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemají	mít	k5eNaImIp3nP	mít
pevný	pevný	k2eAgInSc4d1	pevný
povrch	povrch	k1gInSc4	povrch
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
hustou	hustý	k2eAgFnSc4d1	hustá
atmosféru	atmosféra	k1gFnSc4	atmosféra
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
postupně	postupně	k6eAd1	postupně
přechází	přecházet	k5eAaImIp3nS	přecházet
do	do	k7c2	do
pláště	plášť	k1gInSc2	plášť
<g/>
.	.	kIx.	.
</s>
<s>
Atmosféra	atmosféra	k1gFnSc1	atmosféra
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
převážně	převážně	k6eAd1	převážně
lehkými	lehký	k2eAgInPc7d1	lehký
plyny	plyn	k1gInPc7	plyn
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
hlavně	hlavně	k6eAd1	hlavně
vodíkem	vodík	k1gInSc7	vodík
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
96,3	[number]	k4	96,3
%	%	kIx~	%
jejího	její	k3xOp3gInSc2	její
objemu	objem	k1gInSc2	objem
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pozorování	pozorování	k1gNnSc6	pozorování
Saturnu	Saturn	k1gInSc2	Saturn
z	z	k7c2	z
dálky	dálka	k1gFnSc2	dálka
je	být	k5eAaImIp3nS	být
planeta	planeta	k1gFnSc1	planeta
světle	světle	k6eAd1	světle
žlutá	žlutý	k2eAgFnSc1d1	žlutá
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
vrstva	vrstva	k1gFnSc1	vrstva
mraků	mrak	k1gInPc2	mrak
s	s	k7c7	s
nejasnými	jasný	k2eNgInPc7d1	nejasný
pásy	pás	k1gInPc7	pás
různých	různý	k2eAgInPc2d1	různý
barevných	barevný	k2eAgInPc2d1	barevný
odstínů	odstín	k1gInPc2	odstín
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
přibližně	přibližně	k6eAd1	přibližně
rovnoběžné	rovnoběžný	k2eAgInPc1d1	rovnoběžný
s	s	k7c7	s
rovníkem	rovník	k1gInSc7	rovník
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
oblačné	oblačný	k2eAgFnSc6d1	oblačná
vrstvě	vrstva	k1gFnSc6	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
−	−	k?	−
<g/>
140	[number]	k4	140
°	°	k?	°
<g/>
C.	C.	kA	C.
Objem	objem	k1gInSc4	objem
planety	planeta	k1gFnSc2	planeta
je	být	k5eAaImIp3nS	být
764krát	764krát	k6eAd1	764krát
větší	veliký	k2eAgInSc1d2	veliký
než	než	k8xS	než
objem	objem	k1gInSc1	objem
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
planet	planeta	k1gFnPc2	planeta
nejmenší	malý	k2eAgFnSc4d3	nejmenší
hustotu	hustota	k1gFnSc4	hustota
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
pouze	pouze	k6eAd1	pouze
0,6873	[number]	k4	0,6873
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
cm3	cm3	k4	cm3
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jedinou	jediný	k2eAgFnSc4d1	jediná
planetu	planeta	k1gFnSc4	planeta
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
menší	malý	k2eAgFnSc4d2	menší
střední	střední	k2eAgFnSc4d1	střední
hustotu	hustota	k1gFnSc4	hustota
než	než	k8xS	než
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
Saturn	Saturn	k1gMnSc1	Saturn
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
svou	svůj	k3xOyFgFnSc7	svůj
mohutnou	mohutný	k2eAgFnSc7d1	mohutná
soustavou	soustava	k1gFnSc7	soustava
planetárních	planetární	k2eAgInPc2d1	planetární
prstenců	prstenec	k1gInPc2	prstenec
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
viditelné	viditelný	k2eAgInPc1d1	viditelný
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
i	i	k8xC	i
malým	malý	k2eAgInSc7d1	malý
dalekohledem	dalekohled	k1gInSc7	dalekohled
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
prstenců	prstenec	k1gInPc2	prstenec
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
značí	značit	k5eAaImIp3nP	značit
velkými	velký	k2eAgNnPc7d1	velké
písmeny	písmeno	k1gNnPc7	písmeno
latinské	latinský	k2eAgNnSc4d1	latinské
abecedy	abeceda	k1gFnPc4	abeceda
<g/>
,	,	kIx,	,
obíhá	obíhat	k5eAaImIp3nS	obíhat
kolem	kolem	k7c2	kolem
planety	planeta	k1gFnSc2	planeta
také	také	k9	také
početná	početný	k2eAgFnSc1d1	početná
rodina	rodina	k1gFnSc1	rodina
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
jichž	jenž	k3xRgInPc2	jenž
je	být	k5eAaImIp3nS	být
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
známo	znám	k2eAgNnSc1d1	známo
60	[number]	k4	60
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgMnSc1d3	veliký
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
Titan	titan	k1gInSc1	titan
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
jako	jako	k9	jako
jediný	jediný	k2eAgInSc4d1	jediný
měsíc	měsíc	k1gInSc4	měsíc
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
hustou	hustý	k2eAgFnSc4d1	hustá
atmosféru	atmosféra	k1gFnSc4	atmosféra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeden	jeden	k4xCgInSc1	jeden
oběh	oběh	k1gInSc1	oběh
okolo	okolo	k7c2	okolo
Slunce	slunce	k1gNnSc2	slunce
vykoná	vykonat	k5eAaPmIp3nS	vykonat
Saturn	Saturn	k1gInSc1	Saturn
za	za	k7c4	za
29,46	[number]	k4	29,46
pozemského	pozemský	k2eAgInSc2d1	pozemský
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
noční	noční	k2eAgFnSc6d1	noční
obloze	obloha	k1gFnSc6	obloha
je	být	k5eAaImIp3nS	být
snadno	snadno	k6eAd1	snadno
pozorovatelný	pozorovatelný	k2eAgInSc1d1	pozorovatelný
pouhým	pouhý	k2eAgNnSc7d1	pouhé
okem	oke	k1gNnSc7	oke
jako	jako	k9	jako
nažloutlý	nažloutlý	k2eAgInSc1d1	nažloutlý
neblikavý	blikavý	k2eNgInSc1d1	blikavý
objekt	objekt	k1gInSc1	objekt
<g/>
,	,	kIx,	,
jasností	jasnost	k1gFnSc7	jasnost
srovnatelný	srovnatelný	k2eAgInSc1d1	srovnatelný
s	s	k7c7	s
nejjasnějšími	jasný	k2eAgFnPc7d3	nejjasnější
hvězdami	hvězda	k1gFnPc7	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
ekliptiky	ekliptika	k1gFnSc2	ekliptika
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nevzdálí	vzdálit	k5eNaPmIp3nS	vzdálit
na	na	k7c4	na
větší	veliký	k2eAgFnSc4d2	veliký
úhlovou	úhlový	k2eAgFnSc4d1	úhlová
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
než	než	k8xS	než
2,5	[number]	k4	2,5
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
Přechod	přechod	k1gInSc1	přechod
jedním	jeden	k4xCgNnSc7	jeden
znamením	znamení	k1gNnSc7	znamení
zvěrokruhu	zvěrokruh	k1gInSc2	zvěrokruh
trvá	trvat	k5eAaImIp3nS	trvat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
2	[number]	k4	2
roky	rok	k1gInPc7	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Uran	Uran	k1gInSc1	Uran
====	====	k?	====
</s>
</p>
<p>
</p>
<p>
<s>
Uran	Uran	k1gInSc1	Uran
je	být	k5eAaImIp3nS	být
sedmá	sedmý	k4xOgFnSc1	sedmý
planeta	planeta	k1gFnSc1	planeta
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgFnSc1	třetí
největší	veliký	k2eAgFnSc1d3	veliký
a	a	k8xC	a
čtvrtá	čtvrtý	k4xOgFnSc1	čtvrtý
nejhmotnější	hmotný	k2eAgFnSc1d3	nejhmotnější
planeta	planeta	k1gFnSc1	planeta
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Řadí	řadit	k5eAaImIp3nS	řadit
se	se	k3xPyFc4	se
mezi	mezi	k7c4	mezi
plynné	plynný	k2eAgMnPc4d1	plynný
obry	obr	k1gMnPc4	obr
a	a	k8xC	a
společně	společně	k6eAd1	společně
s	s	k7c7	s
Neptunem	Neptun	k1gInSc7	Neptun
i	i	k8xC	i
mezi	mezi	k7c7	mezi
tzv.	tzv.	kA	tzv.
ledové	ledový	k2eAgFnPc4d1	ledová
obry	obr	k1gMnPc4	obr
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
má	mít	k5eAaImIp3nS	mít
po	po	k7c6	po
řeckém	řecký	k2eAgMnSc6d1	řecký
bohu	bůh	k1gMnSc6	bůh
Úranovi	Úran	k1gMnSc6	Úran
<g/>
,	,	kIx,	,
bohu	bůh	k1gMnSc6	bůh
nebes	nebesa	k1gNnPc2	nebesa
<g/>
.	.	kIx.	.
</s>
<s>
Symboly	symbol	k1gInPc1	symbol
planety	planeta	k1gFnSc2	planeta
Uran	Uran	k1gInSc1	Uran
jsou	být	k5eAaImIp3nP	být
znak	znak	k1gInSc4	znak
♅	♅	k?	♅
(	(	kIx(	(
<g/>
užívaný	užívaný	k2eAgInSc1d1	užívaný
v	v	k7c6	v
astrologii	astrologie	k1gFnSc6	astrologie
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
(	(	kIx(	(
<g/>
užívaný	užívaný	k2eAgInSc1d1	užívaný
v	v	k7c6	v
astronomii	astronomie	k1gFnSc6	astronomie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
Uran	Uran	k1gInSc4	Uran
za	za	k7c2	za
příznivých	příznivý	k2eAgFnPc2d1	příznivá
podmínek	podmínka	k1gFnPc2	podmínka
pozorovat	pozorovat	k5eAaImF	pozorovat
pouhým	pouhý	k2eAgNnSc7d1	pouhé
okem	oke	k1gNnSc7	oke
na	na	k7c6	na
noční	noční	k2eAgFnSc6d1	noční
obloze	obloha	k1gFnSc6	obloha
<g/>
,	,	kIx,	,
nebyl	být	k5eNaImAgInS	být
antickými	antický	k2eAgFnPc7d1	antická
astronomy	astronom	k1gMnPc4	astronom
rozpoznán	rozpoznán	k2eAgInSc4d1	rozpoznán
jako	jako	k8xS	jako
planeta	planeta	k1gFnSc1	planeta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byl	být	k5eAaImAgInS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
hvězdu	hvězda	k1gFnSc4	hvězda
kvůli	kvůli	k7c3	kvůli
pomalé	pomalý	k2eAgFnSc3d1	pomalá
rychlosti	rychlost	k1gFnSc3	rychlost
a	a	k8xC	a
slabé	slabý	k2eAgFnSc3d1	slabá
záři	zář	k1gFnSc3	zář
<g/>
.	.	kIx.	.
</s>
<s>
Objev	objev	k1gInSc1	objev
Uranu	Uran	k1gInSc2	Uran
ohlásil	ohlásit	k5eAaPmAgInS	ohlásit
William	William	k1gInSc1	William
Herschel	Herschel	k1gInSc1	Herschel
13.	[number]	k4	13.
března	březen	k1gInSc2	březen
1781	[number]	k4	1781
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
době	doba	k1gFnSc6	doba
posunul	posunout	k5eAaPmAgInS	posunout
známé	známý	k2eAgFnPc4d1	známá
hranice	hranice	k1gFnPc4	hranice
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chemickým	chemický	k2eAgNnSc7d1	chemické
složením	složení	k1gNnSc7	složení
se	se	k3xPyFc4	se
Uran	Uran	k1gInSc1	Uran
podobá	podobat	k5eAaImIp3nS	podobat
Neptunu	Neptun	k1gMnSc3	Neptun
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
planety	planeta	k1gFnPc1	planeta
mají	mít	k5eAaImIp3nP	mít
rozdílné	rozdílný	k2eAgNnSc1d1	rozdílné
zastoupení	zastoupení	k1gNnSc1	zastoupení
plynů	plyn	k1gInPc2	plyn
oproti	oproti	k7c3	oproti
Jupiteru	Jupiter	k1gInSc3	Jupiter
či	či	k8xC	či
Saturnu	Saturn	k1gInSc3	Saturn
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
atmosféra	atmosféra	k1gFnSc1	atmosféra
Uranu	Uran	k1gInSc2	Uran
složením	složení	k1gNnSc7	složení
podobná	podobný	k2eAgNnPc4d1	podobné
atmosféře	atmosféra	k1gFnSc6	atmosféra
Jupiteru	Jupiter	k1gInSc2	Jupiter
či	či	k8xC	či
Saturnu	Saturn	k1gInSc2	Saturn
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
ji	on	k3xPp3gFnSc4	on
převážně	převážně	k6eAd1	převážně
plynné	plynný	k2eAgFnPc4d1	plynná
formy	forma	k1gFnPc4	forma
vodíku	vodík	k1gInSc2	vodík
a	a	k8xC	a
helia	helium	k1gNnSc2	helium
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
i	i	k9	i
výrazný	výrazný	k2eAgInSc1d1	výrazný
podíl	podíl	k1gInSc1	podíl
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
čpavku	čpavek	k1gInSc2	čpavek
či	či	k8xC	či
metanu	metan	k1gInSc2	metan
se	s	k7c7	s
stopami	stopa	k1gFnPc7	stopa
uhlovodíků	uhlovodík	k1gInPc2	uhlovodík
<g/>
.	.	kIx.	.
</s>
<s>
Atmosféra	atmosféra	k1gFnSc1	atmosféra
Uranu	Uran	k1gInSc2	Uran
je	být	k5eAaImIp3nS	být
nejchladnější	chladný	k2eAgFnSc7d3	nejchladnější
atmosférou	atmosféra	k1gFnSc7	atmosféra
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
<g/>
,	,	kIx,	,
minimální	minimální	k2eAgFnPc1d1	minimální
teploty	teplota	k1gFnPc1	teplota
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
okolo	okolo	k7c2	okolo
49	[number]	k4	49
K.	K.	kA	K.
Její	její	k3xOp3gInSc4	její
struktura	struktura	k1gFnSc1	struktura
je	být	k5eAaImIp3nS	být
vrstevnatá	vrstevnatý	k2eAgFnSc1d1	vrstevnatá
<g/>
:	:	kIx,	:
v	v	k7c6	v
nejnižších	nízký	k2eAgNnPc6d3	nejnižší
patrech	patro	k1gNnPc6	patro
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
mraky	mrak	k1gInPc1	mrak
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
ve	v	k7c6	v
svrchních	svrchní	k2eAgNnPc6d1	svrchní
patrech	patro	k1gNnPc6	patro
mraky	mrak	k1gInPc1	mrak
tvořené	tvořený	k2eAgInPc1d1	tvořený
především	především	k6eAd1	především
metanem	metan	k1gInSc7	metan
<g/>
.	.	kIx.	.
</s>
<s>
Sama	sám	k3xTgFnSc1	sám
planeta	planeta	k1gFnSc1	planeta
je	být	k5eAaImIp3nS	být
nejspíše	nejspíše	k9	nejspíše
složena	složit	k5eAaPmNgFnS	složit
především	především	k9	především
z	z	k7c2	z
ledu	led	k1gInSc2	led
a	a	k8xC	a
kamene	kámen	k1gInSc2	kámen
<g/>
.	.	kIx.	.
<g/>
Podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
další	další	k2eAgFnPc4d1	další
plynné	plynný	k2eAgFnPc4d1	plynná
planety	planeta	k1gFnPc4	planeta
má	mít	k5eAaImIp3nS	mít
i	i	k8xC	i
Uran	Uran	k1gMnSc1	Uran
planetární	planetární	k2eAgInPc4d1	planetární
prstence	prstenec	k1gInPc4	prstenec
<g/>
,	,	kIx,	,
magnetosféru	magnetosféra	k1gFnSc4	magnetosféra
a	a	k8xC	a
obíhá	obíhat	k5eAaImIp3nS	obíhat
ho	on	k3xPp3gMnSc4	on
řada	řada	k1gFnSc1	řada
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštností	zvláštnost	k1gFnSc7	zvláštnost
Uranu	Uran	k1gInSc2	Uran
je	být	k5eAaImIp3nS	být
sklon	sklon	k1gInSc1	sklon
jeho	jeho	k3xOp3gFnSc2	jeho
rotační	rotační	k2eAgFnSc2d1	rotační
osy	osa	k1gFnSc2	osa
<g/>
:	:	kIx,	:
osa	osa	k1gFnSc1	osa
leží	ležet	k5eAaImIp3nS	ležet
téměř	téměř	k6eAd1	téměř
v	v	k7c6	v
rovině	rovina	k1gFnSc6	rovina
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yRgMnPc4	který
planeta	planeta	k1gFnSc1	planeta
obíhá	obíhat	k5eAaImIp3nS	obíhat
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgFnSc1d1	severní
a	a	k8xC	a
jižní	jižní	k2eAgInSc1d1	jižní
pól	pól	k1gInSc1	pól
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
jsou	být	k5eAaImIp3nP	být
u	u	k7c2	u
jiných	jiný	k2eAgFnPc2d1	jiná
planet	planeta	k1gFnPc2	planeta
charakteristické	charakteristický	k2eAgFnSc2d1	charakteristická
pro	pro	k7c4	pro
rovník	rovník	k1gInSc4	rovník
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
občas	občas	k6eAd1	občas
stane	stanout	k5eAaPmIp3nS	stanout
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
prstence	prstenec	k1gInPc1	prstenec
Uranu	Uran	k1gInSc2	Uran
jeví	jevit	k5eAaImIp3nP	jevit
jako	jako	k9	jako
terč	terč	k1gInSc4	terč
s	s	k7c7	s
Uranem	Uran	k1gInSc7	Uran
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
kolem	kolem	k7c2	kolem
Uranu	Uran	k1gInSc2	Uran
proletěla	proletět	k5eAaPmAgFnS	proletět
sonda	sonda	k1gFnSc1	sonda
Voyager	Voyager	k1gInSc1	Voyager
2	[number]	k4	2
<g/>
,	,	kIx,	,
nepozorovala	pozorovat	k5eNaImAgFnS	pozorovat
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
planety	planeta	k1gFnSc2	planeta
žádné	žádný	k3yNgNnSc4	žádný
větší	veliký	k2eAgNnSc4d2	veliký
množství	množství	k1gNnSc4	množství
mračen	mračno	k1gNnPc2	mračno
a	a	k8xC	a
bouřkových	bouřkový	k2eAgInPc2d1	bouřkový
systémů	systém	k1gInPc2	systém
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
typické	typický	k2eAgNnSc1d1	typické
pro	pro	k7c4	pro
jiné	jiný	k2eAgMnPc4d1	jiný
plynné	plynný	k2eAgMnPc4d1	plynný
obry	obr	k1gMnPc4	obr
<g/>
.	.	kIx.	.
</s>
<s>
Pozemská	pozemský	k2eAgNnPc1d1	pozemské
pozorování	pozorování	k1gNnPc1	pozorování
však	však	k9	však
přinesla	přinést	k5eAaPmAgNnP	přinést
náznaky	náznak	k1gInPc1	náznak
sezónních	sezónní	k2eAgFnPc2d1	sezónní
změn	změna	k1gFnPc2	změna
počasí	počasí	k1gNnSc2	počasí
<g/>
,	,	kIx,	,
s	s	k7c7	s
čímž	což	k3yRnSc7	což
souvisí	souviset	k5eAaImIp3nP	souviset
i	i	k9	i
větry	vítr	k1gInPc1	vítr
vanoucí	vanoucí	k2eAgInPc1d1	vanoucí
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
mohou	moct	k5eAaImIp3nP	moct
dosahovat	dosahovat	k5eAaImF	dosahovat
rychlosti	rychlost	k1gFnSc3	rychlost
až	až	k9	až
900	[number]	k4	900
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Neptun	Neptun	k1gInSc4	Neptun
====	====	k?	====
</s>
</p>
<p>
</p>
<p>
<s>
Neptun	Neptun	k1gInSc1	Neptun
je	být	k5eAaImIp3nS	být
osmá	osmý	k4xOgFnSc1	osmý
a	a	k8xC	a
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
nejvzdálenější	vzdálený	k2eAgFnSc1d3	nejvzdálenější
planeta	planeta	k1gFnSc1	planeta
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
a	a	k8xC	a
řadí	řadit	k5eAaImIp3nS	řadit
se	se	k3xPyFc4	se
mezi	mezi	k7c4	mezi
představitele	představitel	k1gMnPc4	představitel
plynných	plynný	k2eAgInPc2d1	plynný
obrů	obr	k1gMnPc2	obr
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rovníkovým	rovníkový	k2eAgInSc7d1	rovníkový
průměrem	průměr	k1gInSc7	průměr
okolo	okolo	k7c2	okolo
50 000	[number]	k4	50 000
km	km	kA	km
spadá	spadat	k5eAaImIp3nS	spadat
mezi	mezi	k7c4	mezi
menší	malý	k2eAgFnPc4d2	menší
plynné	plynný	k2eAgFnPc4d1	plynná
obry	obr	k1gMnPc7	obr
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
u	u	k7c2	u
ostatních	ostatní	k2eAgMnPc2d1	ostatní
plynných	plynný	k2eAgMnPc2d1	plynný
obrů	obr	k1gMnPc2	obr
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
přímo	přímo	k6eAd1	přímo
pozorovat	pozorovat	k5eAaImF	pozorovat
pouze	pouze	k6eAd1	pouze
svrchní	svrchní	k2eAgFnPc4d1	svrchní
vrstvy	vrstva	k1gFnPc4	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgInPc6	který
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
několik	několik	k4yIc4	několik
velkých	velký	k2eAgFnPc2d1	velká
temných	temný	k2eAgFnPc2d1	temná
skvrn	skvrna	k1gFnPc2	skvrna
připomínajících	připomínající	k2eAgFnPc2d1	připomínající
skvrny	skvrna	k1gFnPc4	skvrna
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Neptun	Neptun	k1gInSc1	Neptun
má	mít	k5eAaImIp3nS	mít
charakteristicky	charakteristicky	k6eAd1	charakteristicky
modrou	modrý	k2eAgFnSc4d1	modrá
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
zapříčiněna	zapříčinit	k5eAaPmNgFnS	zapříčinit
množstvím	množství	k1gNnSc7	množství
metanu	metan	k1gInSc2	metan
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
.	.	kIx.	.
<g/>
Planeta	planeta	k1gFnSc1	planeta
Neptun	Neptun	k1gInSc1	Neptun
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
podobná	podobný	k2eAgFnSc1d1	podobná
Uranu	Uran	k1gMnSc3	Uran
<g/>
,	,	kIx,	,
obě	dva	k4xCgFnPc1	dva
planety	planeta	k1gFnPc1	planeta
mají	mít	k5eAaImIp3nP	mít
rozdílné	rozdílný	k2eAgNnSc4d1	rozdílné
složení	složení	k1gNnSc4	složení
než	než	k8xS	než
další	další	k2eAgMnPc1d1	další
plynní	plynný	k2eAgMnPc1d1	plynný
obři	obr	k1gMnPc1	obr
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
Jupiter	Jupiter	k1gMnSc1	Jupiter
a	a	k8xC	a
Saturn	Saturn	k1gMnSc1	Saturn
<g/>
.	.	kIx.	.
</s>
<s>
Uran	Uran	k1gInSc1	Uran
a	a	k8xC	a
Neptun	Neptun	k1gInSc1	Neptun
jsou	být	k5eAaImIp3nP	být
proto	proto	k8xC	proto
někdy	někdy	k6eAd1	někdy
vyčleňováni	vyčleňovat	k5eAaImNgMnP	vyčleňovat
do	do	k7c2	do
zvláštní	zvláštní	k2eAgFnSc2d1	zvláštní
kategorie	kategorie	k1gFnSc2	kategorie
jako	jako	k8xS	jako
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
ledoví	ledový	k2eAgMnPc1d1	ledový
obři	obr	k1gMnPc1	obr
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Atmosféra	atmosféra	k1gFnSc1	atmosféra
Neptunu	Neptun	k1gInSc2	Neptun
je	být	k5eAaImIp3nS	být
složena	složit	k5eAaPmNgFnS	složit
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
vodíku	vodík	k1gInSc2	vodík
a	a	k8xC	a
helia	helium	k1gNnSc2	helium
s	s	k7c7	s
větším	veliký	k2eAgInSc7d2	veliký
podílem	podíl	k1gInSc7	podíl
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
čpavku	čpavek	k1gInSc2	čpavek
a	a	k8xC	a
metanu	metan	k1gInSc2	metan
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
stavba	stavba	k1gFnSc1	stavba
planety	planeta	k1gFnSc2	planeta
je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
kamenitá	kamenitý	k2eAgFnSc1d1	kamenitá
a	a	k8xC	a
obohacená	obohacený	k2eAgFnSc1d1	obohacená
navíc	navíc	k6eAd1	navíc
vodním	vodní	k2eAgInSc7d1	vodní
ledem	led	k1gInSc7	led
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Planeta	planeta	k1gFnSc1	planeta
byla	být	k5eAaImAgFnS	být
objevena	objevit	k5eAaPmNgFnS	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1846	[number]	k4	1846
Johannem	Johann	k1gMnSc7	Johann
Gallem	Gall	k1gMnSc7	Gall
a	a	k8xC	a
studentem	student	k1gMnSc7	student
astronomie	astronomie	k1gFnSc2	astronomie
Louisem	Louis	k1gMnSc7	Louis
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Arrestem	Arrest	k1gMnSc7	Arrest
jako	jako	k8xC	jako
vůbec	vůbec	k9	vůbec
jediná	jediný	k2eAgFnSc1d1	jediná
na	na	k7c6	na
základě	základ	k1gInSc6	základ
matematických	matematický	k2eAgInPc2d1	matematický
výpočtů	výpočet	k1gInPc2	výpočet
gravitačních	gravitační	k2eAgFnPc2d1	gravitační
odchylek	odchylka	k1gFnPc2	odchylka
okolních	okolní	k2eAgFnPc2d1	okolní
těles	těleso	k1gNnPc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
planeta	planeta	k1gFnSc1	planeta
dostala	dostat	k5eAaPmAgFnS	dostat
své	svůj	k3xOyFgNnSc4	svůj
jméno	jméno	k1gNnSc4	jméno
podle	podle	k7c2	podle
starořímského	starořímský	k2eAgMnSc2d1	starořímský
boha	bůh	k1gMnSc2	bůh
moří	mořit	k5eAaImIp3nS	mořit
Neptuna	Neptun	k1gMnSc4	Neptun
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Trpasličí	trpasličí	k2eAgFnSc2d1	trpasličí
planety	planeta	k1gFnSc2	planeta
====	====	k?	====
</s>
</p>
<p>
</p>
<p>
<s>
Ceres	ceres	k1gInSc1	ceres
</s>
</p>
<p>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ceres	ceres	k1gInSc1	ceres
(	(	kIx(	(
<g/>
trpasličí	trpasličí	k2eAgFnSc1d1	trpasličí
planeta	planeta	k1gFnSc1	planeta
<g/>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
<s>
Ceres	ceres	k1gInSc1	ceres
je	být	k5eAaImIp3nS	být
nejmenší	malý	k2eAgMnSc1d3	nejmenší
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
Slunci	slunce	k1gNnPc7	slunce
nejbližší	blízký	k2eAgFnSc1d3	nejbližší
trpasličí	trpasličí	k2eAgFnSc1d1	trpasličí
planeta	planeta	k1gFnSc1	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Ceres	ceres	k1gInSc1	ceres
obíhá	obíhat	k5eAaImIp3nS	obíhat
Slunce	slunce	k1gNnSc1	slunce
v	v	k7c6	v
hlavním	hlavní	k2eAgInSc6d1	hlavní
pásu	pás	k1gInSc6	pás
planetek	planetka	k1gFnPc2	planetka
mezi	mezi	k7c7	mezi
Marsem	Mars	k1gInSc7	Mars
a	a	k8xC	a
Jupiterem	Jupiter	k1gMnSc7	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Ceres	ceres	k1gInSc1	ceres
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgInSc4d3	veliký
objekt	objekt	k1gInSc4	objekt
hlavního	hlavní	k2eAgInSc2d1	hlavní
pásu	pás	k1gInSc2	pás
planetek	planetka	k1gFnPc2	planetka
<g/>
.	.	kIx.	.
</s>
<s>
Rovníkový	rovníkový	k2eAgInSc1d1	rovníkový
poloměr	poloměr	k1gInSc1	poloměr
činí	činit	k5eAaImIp3nS	činit
975	[number]	k4	975
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Svojí	svůj	k3xOyFgFnSc7	svůj
hmotností	hmotnost	k1gFnSc7	hmotnost
představuje	představovat	k5eAaImIp3nS	představovat
skoro	skoro	k6eAd1	skoro
30	[number]	k4	30
<g/>
%	%	kIx~	%
pásu	pás	k1gInSc2	pás
asteroidů	asteroid	k1gInPc2	asteroid
mezi	mezi	k7c7	mezi
Marsem	Mars	k1gInSc7	Mars
a	a	k8xC	a
Jupiterem	Jupiter	k1gMnSc7	Jupiter
.	.	kIx.	.
</s>
<s>
Ceres	ceres	k1gInSc1	ceres
byl	být	k5eAaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
italským	italský	k2eAgMnSc7d1	italský
matematikem	matematik	k1gMnSc7	matematik
Giuseppem	Giusepp	k1gInSc7	Giusepp
Piazzim	Piazzim	k1gInSc4	Piazzim
1.	[number]	k4	1.
ledna	leden	k1gInSc2	leden
1801.	[number]	k4	1801.
</s>
</p>
<p>
<s>
Pluto	Pluto	k1gMnSc1	Pluto
</s>
</p>
<p>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Pluto	Pluto	k1gNnSc1	Pluto
(	(	kIx(	(
<g/>
trpasličí	trpasličí	k2eAgFnSc1d1	trpasličí
planeta	planeta	k1gFnSc1	planeta
<g/>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
<s>
Pluto	Pluto	k1gNnSc1	Pluto
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc1d3	veliký
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
nejhmotnější	hmotný	k2eAgFnSc1d3	nejhmotnější
trpasličí	trpasličí	k2eAgFnSc1d1	trpasličí
planeta	planeta	k1gFnSc1	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
objevena	objeven	k2eAgFnSc1d1	objevena
roku	rok	k1gInSc2	rok
1930	[number]	k4	1930
americkým	americký	k2eAgMnSc7d1	americký
astronomem	astronom	k1gMnSc7	astronom
Clydem	Clyd	k1gMnSc7	Clyd
Tombaughem	Tombaugh	k1gInSc7	Tombaugh
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
bylo	být	k5eAaImAgNnS	být
Pluto	Pluto	k1gNnSc1	Pluto
zařazeno	zařadit	k5eAaPmNgNnS	zařadit
mezi	mezi	k7c4	mezi
planety	planeta	k1gFnPc4	planeta
<g/>
,	,	kIx,	,
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
co	co	k9	co
se	se	k3xPyFc4	se
změnila	změnit	k5eAaPmAgFnS	změnit
definice	definice	k1gFnSc1	definice
planety	planeta	k1gFnSc2	planeta
je	být	k5eAaImIp3nS	být
Pluto	Pluto	k1gNnSc1	Pluto
zařazeno	zařadit	k5eAaPmNgNnS	zařadit
mezi	mezi	k7c4	mezi
trpasličí	trpasličí	k2eAgFnPc4d1	trpasličí
planety	planeta	k1gFnPc4	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Pluto	Pluto	k1gMnSc1	Pluto
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
i	i	k9	i
další	další	k2eAgInPc4d1	další
objekty	objekt	k1gInPc4	objekt
Kuiperova	Kuiperův	k2eAgInSc2d1	Kuiperův
pásu	pás	k1gInSc2	pás
<g/>
,	,	kIx,	,
skládá	skládat	k5eAaImIp3nS	skládat
především	především	k9	především
z	z	k7c2	z
kamenných	kamenný	k2eAgInPc2d1	kamenný
materiálů	materiál	k1gInPc2	materiál
a	a	k8xC	a
ledu	led	k1gInSc2	led
<g/>
.	.	kIx.	.
</s>
<s>
Pluto	Pluto	k1gMnSc1	Pluto
má	mít	k5eAaImIp3nS	mít
pět	pět	k4xCc4	pět
známých	známý	k2eAgInPc2d1	známý
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
<g/>
,	,	kIx,	,
Charon	Charon	k1gMnSc1	Charon
<g/>
,	,	kIx,	,
Nix	Nix	k1gMnSc1	Nix
<g/>
,	,	kIx,	,
Hydra	hydra	k1gFnSc1	hydra
<g/>
,	,	kIx,	,
Kerberos	Kerberos	k1gMnSc1	Kerberos
a	a	k8xC	a
Styx	Styx	k1gInSc1	Styx
.	.	kIx.	.
</s>
<s>
Rovníkový	rovníkový	k2eAgInSc1d1	rovníkový
poloměr	poloměr	k1gInSc1	poloměr
Pluta	Pluto	k1gNnSc2	Pluto
činí	činit	k5eAaImIp3nS	činit
2 370	[number]	k4	2 370
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Haumea	Haumea	k6eAd1	Haumea
</s>
</p>
<p>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Haumea	Haumea	k1gFnSc1	Haumea
(	(	kIx(	(
<g/>
trpasličí	trpasličí	k2eAgFnSc1d1	trpasličí
planeta	planeta	k1gFnSc1	planeta
<g/>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
<s>
Haumea	Haumea	k1gFnSc1	Haumea
je	být	k5eAaImIp3nS	být
trpasličí	trpasličí	k2eAgFnSc1d1	trpasličí
planeta	planeta	k1gFnSc1	planeta
nacházející	nacházející	k2eAgFnSc1d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
Kuiperově	Kuiperův	k2eAgInSc6d1	Kuiperův
pásu	pás	k1gInSc6	pás
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
Haumey	Haumea	k1gFnSc2	Haumea
obíhají	obíhat	k5eAaImIp3nP	obíhat
dva	dva	k4xCgMnPc1	dva
její	její	k3xOp3gMnPc1	její
měsíce	měsíc	k1gInPc4	měsíc
Hi	hi	k0	hi
<g/>
'	'	kIx"	'
<g/>
iaka	iakus	k1gMnSc4	iakus
a	a	k8xC	a
Namaka	Namak	k1gMnSc4	Namak
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
objevena	objeven	k2eAgFnSc1d1	objevena
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
týmem	tým	k1gInSc7	tým
vedeným	vedený	k2eAgMnSc7d1	vedený
Michaelem	Michael	k1gMnSc7	Michael
Brownema	Brownemum	k1gNnSc2	Brownemum
a	a	k8xC	a
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
týmem	tým	k1gInSc7	tým
vedeným	vedený	k2eAgInSc7d1	vedený
José	José	k1gNnSc4	José
Ortizem	Ortiz	k1gInSc7	Ortiz
<g/>
.	.	kIx.	.
</s>
<s>
Objev	objev	k1gInSc4	objev
provázely	provázet	k5eAaImAgInP	provázet
spory	spor	k1gInPc1	spor
o	o	k7c6	o
prvenství	prvenství	k1gNnSc6	prvenství
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgInPc7	dva
týmy	tým	k1gInPc7	tým
<g/>
.	.	kIx.	.
</s>
<s>
Haumea	Haumea	k1gFnSc1	Haumea
má	mít	k5eAaImIp3nS	mít
extrémně	extrémně	k6eAd1	extrémně
protáhlý	protáhlý	k2eAgInSc4d1	protáhlý
tvar	tvar	k1gInSc4	tvar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Makemake	Makemake	k6eAd1	Makemake
</s>
</p>
<p>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Makemake	Makemake	k1gFnSc1	Makemake
(	(	kIx(	(
<g/>
trpasličí	trpasličí	k2eAgFnSc1d1	trpasličí
planeta	planeta	k1gFnSc1	planeta
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Makemake	Makemake	k1gFnSc1	Makemake
je	být	k5eAaImIp3nS	být
trpasličí	trpasličí	k2eAgFnSc1d1	trpasličí
planeta	planeta	k1gFnSc1	planeta
nacházející	nacházející	k2eAgFnSc1d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
Kuiperově	Kuiperův	k2eAgInSc6d1	Kuiperův
pásu	pás	k1gInSc6	pás
<g/>
.	.	kIx.	.
</s>
<s>
Makemake	Makemake	k6eAd1	Makemake
má	mít	k5eAaImIp3nS	mít
jeden	jeden	k4xCgInSc4	jeden
měsíc	měsíc	k1gInSc4	měsíc
pracovně	pracovně	k6eAd1	pracovně
nazvaný	nazvaný	k2eAgMnSc1d1	nazvaný
jako	jako	k9	jako
S	s	k7c7	s
<g/>
/	/	kIx~	/
<g/>
2015	[number]	k4	2015
(	(	kIx(	(
<g/>
136472	[number]	k4	136472
<g/>
)	)	kIx)	)
1.	[number]	k4	1.
<g/>
Makemake	Makemake	k1gFnPc2	Makemake
byla	být	k5eAaImAgFnS	být
objevena	objevit	k5eAaPmNgFnS	objevit
31.	[number]	k4	31.
března	březen	k1gInSc2	březen
2005	[number]	k4	2005
a	a	k8xC	a
formálně	formálně	k6eAd1	formálně
klasifikována	klasifikován	k2eAgFnSc1d1	klasifikována
jako	jako	k8xC	jako
plutoid	plutoid	k1gInSc1	plutoid
dne	den	k1gInSc2	den
11.	[number]	k4	11.
července	červenec	k1gInSc2	červenec
2008.	[number]	k4	2008.
</s>
</p>
<p>
</p>
<p>
<s>
Toto	tento	k3xDgNnSc1	tento
těleso	těleso	k1gNnSc1	těleso
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
po	po	k7c6	po
Plutu	plut	k1gInSc6	plut
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
nejjasnější	jasný	k2eAgNnSc4d3	nejjasnější
transneptunické	transneptunický	k2eAgNnSc4d1	transneptunické
těleso	těleso	k1gNnSc4	těleso
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Eris	Eris	k1gFnSc1	Eris
</s>
</p>
<p>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Eris	Eris	k1gFnSc1	Eris
(	(	kIx(	(
<g/>
trpasličí	trpasličí	k2eAgFnSc1d1	trpasličí
planeta	planeta	k1gFnSc1	planeta
<g/>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
<s>
Eris	Eris	k1gFnSc1	Eris
je	být	k5eAaImIp3nS	být
druhá	druhý	k4xOgFnSc1	druhý
největší	veliký	k2eAgFnSc1d3	veliký
trpasličí	trpasličí	k2eAgFnSc1d1	trpasličí
planeta	planeta	k1gFnSc1	planeta
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
nejhmotnější	hmotný	k2eAgFnSc1d3	nejhmotnější
trpasličí	trpasličí	k2eAgFnSc1d1	trpasličí
planeta	planeta	k1gFnSc1	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
Rozptýleném	rozptýlený	k2eAgInSc6d1	rozptýlený
disku	disk	k1gInSc6	disk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
2005	[number]	k4	2005
bylo	být	k5eAaImAgNnS	být
toto	tento	k3xDgNnSc1	tento
těleso	těleso	k1gNnSc1	těleso
objeviteli	objevitel	k1gMnSc6	objevitel
provizorně	provizorně	k6eAd1	provizorně
pojmenováno	pojmenovat	k5eAaPmNgNnS	pojmenovat
Xena	Xen	k2eAgFnSc1d1	Xena
<g/>
.	.	kIx.	.
</s>
<s>
Eris	Eris	k1gFnSc1	Eris
má	mít	k5eAaImIp3nS	mít
měsíc	měsíc	k1gInSc4	měsíc
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
objevený	objevený	k2eAgInSc1d1	objevený
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
provizorně	provizorně	k6eAd1	provizorně
nazván	nazván	k2eAgInSc1d1	nazván
Gabrielle	Gabrielle	k1gInSc1	Gabrielle
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
další	další	k2eAgFnSc2d1	další
postavy	postava	k1gFnSc2	postava
televizního	televizní	k2eAgInSc2d1	televizní
seriálu	seriál	k1gInSc2	seriál
<g/>
)	)	kIx)	)
a	a	k8xC	a
který	který	k3yQgMnSc1	který
dostal	dostat	k5eAaPmAgMnS	dostat
předběžné	předběžný	k2eAgNnSc4d1	předběžné
označení	označení	k1gNnSc4	označení
S	s	k7c7	s
<g/>
/	/	kIx~	/
<g/>
2005	[number]	k4	2005
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
UB313	UB313	k1gFnPc2	UB313
<g/>
)	)	kIx)	)
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
měsíc	měsíc	k1gInSc4	měsíc
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
Dysnomia	Dysnomia	k1gFnSc1	Dysnomia
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Komety	kometa	k1gFnSc2	kometa
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Kometa	kometa	k1gFnSc1	kometa
je	být	k5eAaImIp3nS	být
malé	malý	k2eAgNnSc4d1	malé
těleso	těleso	k1gNnSc4	těleso
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
podobné	podobný	k2eAgFnSc2d1	podobná
planetce	planetka	k1gFnSc3	planetka
<g/>
,	,	kIx,	,
složené	složený	k2eAgInPc1d1	složený
především	především	k9	především
z	z	k7c2	z
ledu	led	k1gInSc2	led
a	a	k8xC	a
prachu	prach	k1gInSc2	prach
a	a	k8xC	a
obíhající	obíhající	k2eAgFnSc7d1	obíhající
většinou	většina	k1gFnSc7	většina
po	po	k7c6	po
velice	velice	k6eAd1	velice
výstředné	výstředný	k2eAgFnSc6d1	výstředná
(	(	kIx(	(
<g/>
excentrické	excentrický	k2eAgFnSc6d1	excentrická
<g/>
)	)	kIx)	)
dráze	dráha	k1gFnSc3	dráha
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Komety	kometa	k1gFnPc1	kometa
jsou	být	k5eAaImIp3nP	být
známé	známý	k2eAgFnPc1d1	známá
pro	pro	k7c4	pro
své	svůj	k3xOyFgInPc4	svůj
nápadné	nápadný	k2eAgInPc4d1	nápadný
ohony	ohon	k1gInPc4	ohon
<g/>
.	.	kIx.	.
</s>
<s>
Naprostá	naprostý	k2eAgFnSc1d1	naprostá
většina	většina	k1gFnSc1	většina
komet	kometa	k1gFnPc2	kometa
se	se	k3xPyFc4	se
po	po	k7c4	po
většinu	většina	k1gFnSc4	většina
času	čas	k1gInSc2	čas
zdržuje	zdržovat	k5eAaImIp3nS	zdržovat
daleko	daleko	k6eAd1	daleko
za	za	k7c7	za
oběžnou	oběžný	k2eAgFnSc7d1	oběžná
dráhou	dráha	k1gFnSc7	dráha
Pluta	Pluto	k1gNnSc2	Pluto
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
občas	občas	k6eAd1	občas
přilétne	přilétnout	k5eAaPmIp3nS	přilétnout
do	do	k7c2	do
vnitřních	vnitřní	k2eAgFnPc2d1	vnitřní
částí	část	k1gFnPc2	část
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
jsou	být	k5eAaImIp3nP	být
popisované	popisovaný	k2eAgInPc1d1	popisovaný
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
špinavé	špinavý	k2eAgFnPc1d1	špinavá
sněhové	sněhový	k2eAgFnPc1d1	sněhová
koule	koule	k1gFnPc1	koule
<g/>
"	"	kIx"	"
–	–	k?	–
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
je	on	k3xPp3gFnPc4	on
tvoří	tvořit	k5eAaImIp3nS	tvořit
zmrzlý	zmrzlý	k2eAgInSc4d1	zmrzlý
oxid	oxid	k1gInSc4	oxid
uhličitý	uhličitý	k2eAgInSc4d1	uhličitý
<g/>
,	,	kIx,	,
metan	metan	k1gInSc1	metan
a	a	k8xC	a
voda	voda	k1gFnSc1	voda
smíchaná	smíchaný	k2eAgFnSc1d1	smíchaná
s	s	k7c7	s
prachem	prach	k1gInSc7	prach
a	a	k8xC	a
různými	různý	k2eAgFnPc7d1	různá
nerostnými	nerostný	k2eAgFnPc7d1	nerostná
látkami	látka	k1gFnPc7	látka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
gravitační	gravitační	k2eAgFnSc6d1	gravitační
interakci	interakce	k1gFnSc6	interakce
komety	kometa	k1gFnSc2	kometa
s	s	k7c7	s
planetami	planeta	k1gFnPc7	planeta
se	se	k3xPyFc4	se
dráha	dráha	k1gFnSc1	dráha
komet	kometa	k1gFnPc2	kometa
může	moct	k5eAaImIp3nS	moct
změnit	změnit	k5eAaPmF	změnit
z	z	k7c2	z
eliptické	eliptický	k2eAgFnSc2d1	eliptická
na	na	k7c4	na
hyperbolickou	hyperbolický	k2eAgFnSc4d1	hyperbolická
(	(	kIx(	(
<g/>
a	a	k8xC	a
definitivně	definitivně	k6eAd1	definitivně
opustit	opustit	k5eAaPmF	opustit
sluneční	sluneční	k2eAgFnSc4d1	sluneční
soustavu	soustava	k1gFnSc4	soustava
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
na	na	k7c4	na
méně	málo	k6eAd2	málo
výstřednou	výstředný	k2eAgFnSc4d1	výstředná
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
Jupiter	Jupiter	k1gMnSc1	Jupiter
je	být	k5eAaImIp3nS	být
známý	známý	k1gMnSc1	známý
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mění	měnit	k5eAaImIp3nP	měnit
dráhy	dráha	k1gFnPc1	dráha
komet	kometa	k1gFnPc2	kometa
a	a	k8xC	a
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
krátkých	krátký	k2eAgFnPc6d1	krátká
oběžných	oběžný	k2eAgFnPc6d1	oběžná
dráhách	dráha	k1gFnPc6	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
komety	kometa	k1gFnPc1	kometa
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
ke	k	k7c3	k
Slunci	slunce	k1gNnSc3	slunce
vrací	vracet	k5eAaImIp3nS	vracet
pravidelně	pravidelně	k6eAd1	pravidelně
a	a	k8xC	a
často	často	k6eAd1	často
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
ně	on	k3xPp3gInPc4	on
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
Halleyova	Halleyův	k2eAgFnSc1d1	Halleyova
<g/>
,	,	kIx,	,
Hale-Boppova	Hale-Boppův	k2eAgFnSc1d1	Hale-Boppova
nebo	nebo	k8xC	nebo
Kohoutkova	Kohoutkův	k2eAgFnSc1d1	Kohoutkova
kometa	kometa	k1gFnSc1	kometa
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
smyslu	smysl	k1gInSc6	smysl
znamená	znamenat	k5eAaImIp3nS	znamenat
jednou	jednou	k6eAd1	jednou
za	za	k7c4	za
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
až	až	k8xS	až
staletí	staletí	k1gNnPc2	staletí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Kuiperův	Kuiperův	k2eAgInSc1d1	Kuiperův
pás	pás	k1gInSc1	pás
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Kuiperův	Kuiperův	k2eAgInSc1d1	Kuiperův
pás	pás	k1gInSc1	pás
je	být	k5eAaImIp3nS	být
oblast	oblast	k1gFnSc4	oblast
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
za	za	k7c7	za
dráhou	dráha	k1gFnSc7	dráha
Neptuna	Neptun	k1gMnSc2	Neptun
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
30	[number]	k4	30
až	až	k9	až
50	[number]	k4	50
AU	au	k0	au
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
složen	složit	k5eAaPmNgInS	složit
z	z	k7c2	z
několika	několik	k4yIc2	několik
desítek	desítka	k1gFnPc2	desítka
tisíc	tisíc	k4xCgInSc1	tisíc
těles	těleso	k1gNnPc2	těleso
větších	veliký	k2eAgInPc6d2	veliký
než	než	k8xS	než
100	[number]	k4	100
km	km	kA	km
a	a	k8xC	a
řádově	řádově	k6eAd1	řádově
miliardy	miliarda	k4xCgFnPc4	miliarda
objektů	objekt	k1gInPc2	objekt
větších	veliký	k2eAgNnPc2d2	veliký
než	než	k8xS	než
1	[number]	k4	1
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
tak	tak	k9	tak
absolutně	absolutně	k6eAd1	absolutně
nejvíce	hodně	k6eAd3	hodně
všech	všecek	k3xTgInPc2	všecek
těles	těleso	k1gNnPc2	těleso
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Pojmenován	pojmenován	k2eAgMnSc1d1	pojmenován
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
Gerardu	Gerard	k1gMnSc6	Gerard
Kuiperovi	Kuiper	k1gMnSc6	Kuiper
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
teorii	teorie	k1gFnSc4	teorie
o	o	k7c6	o
původu	původ	k1gInSc6	původ
některých	některý	k3yIgFnPc2	některý
komet	kometa	k1gFnPc2	kometa
v	v	k7c6	v
bližší	blízký	k2eAgFnSc6d2	bližší
oblasti	oblast	k1gFnSc6	oblast
než	než	k8xS	než
Oortův	Oortův	k2eAgInSc4d1	Oortův
oblak	oblak	k1gInSc4	oblak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Rozptýlený	rozptýlený	k2eAgInSc4d1	rozptýlený
a	a	k8xC	a
oddělený	oddělený	k2eAgInSc4d1	oddělený
disk	disk	k1gInSc4	disk
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Rozptýlený	rozptýlený	k2eAgInSc1d1	rozptýlený
disk	disk	k1gInSc1	disk
je	být	k5eAaImIp3nS	být
vzdálená	vzdálený	k2eAgFnSc1d1	vzdálená
oblast	oblast	k1gFnSc1	oblast
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
řídce	řídce	k6eAd1	řídce
osídlena	osídlen	k2eAgFnSc1d1	osídlena
ledovými	ledový	k2eAgFnPc7d1	ledová
planetkami	planetka	k1gFnPc7	planetka
<g/>
,	,	kIx,	,
označovanými	označovaný	k2eAgFnPc7d1	označovaná
jako	jako	k8xC	jako
objekty	objekt	k1gInPc4	objekt
rozptýleného	rozptýlený	k2eAgInSc2d1	rozptýlený
disku	disk	k1gInSc2	disk
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
objekty	objekt	k1gInPc1	objekt
jsou	být	k5eAaImIp3nP	být
podskupinou	podskupina	k1gFnSc7	podskupina
širší	široký	k2eAgFnSc2d2	širší
skupiny	skupina	k1gFnSc2	skupina
transneptunických	transneptunický	k2eAgNnPc2d1	transneptunické
těles	těleso	k1gNnPc2	těleso
(	(	kIx(	(
<g/>
TNO	TNO	kA	TNO
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výstřednost	výstřednost	k1gFnSc1	výstřednost
oběžných	oběžný	k2eAgFnPc2d1	oběžná
drah	draha	k1gFnPc2	draha
těles	těleso	k1gNnPc2	těleso
rozptýleného	rozptýlený	k2eAgInSc2d1	rozptýlený
disku	disk	k1gInSc2	disk
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
až	až	k6eAd1	až
hodnoty	hodnota	k1gFnPc1	hodnota
0,8	[number]	k4	0,8
a	a	k8xC	a
sklon	sklon	k1gInSc1	sklon
k	k	k7c3	k
rovině	rovina	k1gFnSc3	rovina
ekliptiky	ekliptika	k1gFnSc2	ekliptika
až	až	k6eAd1	až
40	[number]	k4	40
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
perihélium	perihélium	k1gNnSc1	perihélium
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgFnSc4d2	veliký
než	než	k8xS	než
30	[number]	k4	30
astronomických	astronomický	k2eAgFnPc2d1	astronomická
jednotek	jednotka	k1gFnPc2	jednotka
(	(	kIx(	(
<g/>
AU	au	k0	au
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Extrémní	extrémní	k2eAgFnPc1d1	extrémní
oběžné	oběžný	k2eAgFnPc1d1	oběžná
dráhy	dráha	k1gFnPc1	dráha
jsou	být	k5eAaImIp3nP	být
zřejmě	zřejmě	k6eAd1	zřejmě
výsledkem	výsledek	k1gInSc7	výsledek
rozptýlení	rozptýlení	k1gNnSc2	rozptýlení
těchto	tento	k3xDgNnPc2	tento
těles	těleso	k1gNnPc2	těleso
způsobeném	způsobený	k2eAgInSc6d1	způsobený
gravitačním	gravitační	k2eAgNnSc6d1	gravitační
vlivem	vliv	k1gInSc7	vliv
plynných	plynný	k2eAgMnPc2d1	plynný
obrů	obr	k1gMnPc2	obr
a	a	k8xC	a
stále	stále	k6eAd1	stále
je	být	k5eAaImIp3nS	být
svou	svůj	k3xOyFgFnSc7	svůj
gravitací	gravitace	k1gFnSc7	gravitace
narušuje	narušovat	k5eAaImIp3nS	narušovat
planeta	planeta	k1gFnSc1	planeta
Neptun	Neptun	k1gInSc1	Neptun
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Některá	některý	k3yIgNnPc1	některý
transneptunická	transneptunický	k2eAgNnPc1d1	transneptunické
tělesa	těleso	k1gNnPc1	těleso
se	se	k3xPyFc4	se
však	však	k9	však
ani	ani	k9	ani
při	při	k7c6	při
svém	svůj	k3xOyFgNnSc6	svůj
největším	veliký	k2eAgNnSc6d3	veliký
přiblížení	přiblížení	k1gNnSc6	přiblížení
Slunci	slunce	k1gNnSc6	slunce
nedostávají	dostávat	k5eNaImIp3nP	dostávat
do	do	k7c2	do
gravitačního	gravitační	k2eAgInSc2d1	gravitační
vlivu	vliv	k1gInSc2	vliv
vnějších	vnější	k2eAgFnPc2d1	vnější
planet	planeta	k1gFnPc2	planeta
<g/>
,	,	kIx,	,
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc1	jejich
dráhy	dráha	k1gFnPc1	dráha
tak	tak	k6eAd1	tak
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
nerušeny	rušen	k2eNgFnPc1d1	nerušena
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
takové	takový	k3xDgNnSc1	takový
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
jeví	jevit	k5eAaImIp3nS	jevit
být	být	k5eAaImF	být
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
"	"	kIx"	"
<g/>
oddělené	oddělený	k2eAgNnSc1d1	oddělené
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skupina	skupina	k1gFnSc1	skupina
objektů	objekt	k1gInPc2	objekt
tedy	tedy	k9	tedy
bývá	bývat	k5eAaImIp3nS	bývat
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k8xC	jako
oddělený	oddělený	k2eAgInSc4d1	oddělený
disk	disk	k1gInSc4	disk
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
populacemi	populace	k1gFnPc7	populace
rozptýleného	rozptýlený	k2eAgInSc2d1	rozptýlený
a	a	k8xC	a
odděleného	oddělený	k2eAgInSc2d1	oddělený
disku	disk	k1gInSc2	disk
však	však	k9	však
nejsou	být	k5eNaImIp3nP	být
žádné	žádný	k3yNgFnPc4	žádný
pevné	pevný	k2eAgFnPc4d1	pevná
hranice	hranice	k1gFnPc4	hranice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Heliopauza	Heliopauza	k1gFnSc1	Heliopauza
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Heliopauza	Heliopauza	k1gFnSc1	Heliopauza
je	být	k5eAaImIp3nS	být
oblast	oblast	k1gFnSc1	oblast
(	(	kIx(	(
<g/>
rozhraní	rozhraní	k1gNnSc1	rozhraní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
přestává	přestávat	k5eAaImIp3nS	přestávat
působit	působit	k5eAaImF	působit
sluneční	sluneční	k2eAgInSc1d1	sluneční
vítr	vítr	k1gInSc1	vítr
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
současných	současný	k2eAgInPc2d1	současný
poznatků	poznatek	k1gInPc2	poznatek
vane	vanout	k5eAaImIp3nS	vanout
sluneční	sluneční	k2eAgInSc4d1	sluneční
vítr	vítr	k1gInSc4	vítr
neztenčenou	ztenčený	k2eNgFnSc7d1	neztenčená
intenzitou	intenzita	k1gFnSc7	intenzita
asi	asi	k9	asi
do	do	k7c2	do
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
95	[number]	k4	95
AU	au	k0	au
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
ve	v	k7c6	v
větší	veliký	k2eAgFnSc6d2	veliký
míře	míra	k1gFnSc6	míra
střetává	střetávat	k5eAaImIp3nS	střetávat
s	s	k7c7	s
mezihvězdným	mezihvězdný	k2eAgNnSc7d1	mezihvězdné
médiem	médium	k1gNnSc7	médium
<g/>
,	,	kIx,	,
zpomaluje	zpomalovat	k5eAaImIp3nS	zpomalovat
se	se	k3xPyFc4	se
a	a	k8xC	a
mění	měnit	k5eAaImIp3nS	měnit
se	se	k3xPyFc4	se
v	v	k7c4	v
chuchvalce	chuchvalec	k1gInPc4	chuchvalec
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
vypadají	vypadat	k5eAaImIp3nP	vypadat
a	a	k8xC	a
chovají	chovat	k5eAaImIp3nP	chovat
se	se	k3xPyFc4	se
spíše	spíše	k9	spíše
jako	jako	k9	jako
ohony	ohon	k1gInPc1	ohon
komet	kometa	k1gFnPc2	kometa
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
chuchvalce	chuchvalec	k1gInPc1	chuchvalec
mohou	moct	k5eAaImIp3nP	moct
zasahovat	zasahovat	k5eAaImF	zasahovat
do	do	k7c2	do
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
dalších	další	k2eAgInPc2d1	další
přibližně	přibližně	k6eAd1	přibližně
40	[number]	k4	40
AU	au	k0	au
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
sluneční	sluneční	k2eAgInSc1d1	sluneční
vítr	vítr	k1gInSc1	vítr
vane	vanout	k5eAaImIp3nS	vanout
proti	proti	k7c3	proti
směru	směr	k1gInSc2	směr
proudění	proudění	k1gNnSc2	proudění
mezihvězdného	mezihvězdný	k2eAgNnSc2d1	mezihvězdné
média	médium	k1gNnSc2	médium
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
opačném	opačný	k2eAgInSc6d1	opačný
směru	směr	k1gInSc6	směr
to	ten	k3xDgNnSc1	ten
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
několikrát	několikrát	k6eAd1	několikrát
více	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Oortův	Oortův	k2eAgInSc1d1	Oortův
oblak	oblak	k1gInSc1	oblak
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Oortův	Oortův	k2eAgInSc1d1	Oortův
oblak	oblak	k1gInSc1	oblak
je	být	k5eAaImIp3nS	být
řídká	řídký	k2eAgFnSc1d1	řídká
kulovitá	kulovitý	k2eAgFnSc1d1	kulovitá
obálka	obálka	k1gFnSc1	obálka
kolem	kolem	k7c2	kolem
naší	náš	k3xOp1gFnSc2	náš
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
daleko	daleko	k6eAd1	daleko
za	za	k7c7	za
Kuiperovým	Kuiperův	k2eAgInSc7d1	Kuiperův
pásem	pás	k1gInSc7	pás
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
50 000	[number]	k4	50 000
až	až	k9	až
100 000	[number]	k4	100 000
AU	au	k0	au
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
pozůstatek	pozůstatek	k1gInSc4	pozůstatek
prapůvodní	prapůvodní	k2eAgFnSc2d1	prapůvodní
planetární	planetární	k2eAgFnSc2d1	planetární
mlhoviny	mlhovina	k1gFnSc2	mlhovina
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yQgFnPc4	který
naše	náš	k3xOp1gFnSc1	náš
sluneční	sluneční	k2eAgFnSc1d1	sluneční
soustava	soustava	k1gFnSc1	soustava
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
bilionů	bilion	k4xCgInPc2	bilion
komet	kometa	k1gFnPc2	kometa
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
některé	některý	k3yIgInPc4	některý
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
vlivem	vlivem	k7c2	vlivem
gravitace	gravitace	k1gFnSc2	gravitace
jiných	jiný	k2eAgNnPc2d1	jiné
těles	těleso	k1gNnPc2	těleso
změnily	změnit	k5eAaPmAgFnP	změnit
během	běh	k1gInSc7	běh
minulých	minulý	k2eAgFnPc2d1	minulá
miliard	miliarda	k4xCgFnPc2	miliarda
let	let	k1gInSc4	let
svou	svůj	k3xOyFgFnSc4	svůj
dráhu	dráha	k1gFnSc4	dráha
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
Slunci	slunce	k1gNnSc3	slunce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oortův	Oortův	k2eAgInSc1d1	Oortův
oblak	oblak	k1gInSc1	oblak
nese	nést	k5eAaImIp3nS	nést
svůj	svůj	k3xOyFgInSc4	svůj
název	název	k1gInSc4	název
po	po	k7c6	po
holandském	holandský	k2eAgMnSc6d1	holandský
astronomovi	astronom	k1gMnSc6	astronom
Janu	Jan	k1gMnSc6	Jan
Oortovi	Oort	k1gMnSc6	Oort
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
hypotézu	hypotéza	k1gFnSc4	hypotéza
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
existenci	existence	k1gFnSc6	existence
poprvé	poprvé	k6eAd1	poprvé
zveřejnil	zveřejnit	k5eAaPmAgInS	zveřejnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
existence	existence	k1gFnSc1	existence
nebyla	být	k5eNaImAgFnS	být
dosud	dosud	k6eAd1	dosud
prokázána	prokázat	k5eAaPmNgFnS	prokázat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
většina	většina	k1gFnSc1	většina
astronomů	astronom	k1gMnPc2	astronom
jej	on	k3xPp3gMnSc4	on
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
reálný	reálný	k2eAgInSc4d1	reálný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Hranice	Hranice	k1gFnPc4	Hranice
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
===	===	k?	===
</s>
</p>
<p>
<s>
Není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
za	za	k7c7	za
Oortovým	Oortův	k2eAgInSc7d1	Oortův
oblakem	oblak	k1gInSc7	oblak
nacházela	nacházet	k5eAaImAgNnP	nacházet
další	další	k2eAgNnPc1d1	další
tělesa	těleso	k1gNnPc1	těleso
patřící	patřící	k2eAgInPc4d1	patřící
do	do	k7c2	do
naší	náš	k3xOp1gFnSc2	náš
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
však	však	k9	však
neznamená	znamenat	k5eNaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zde	zde	k6eAd1	zde
nemohou	moct	k5eNaImIp3nP	moct
být	být	k5eAaImF	být
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
gravitační	gravitační	k2eAgNnSc1d1	gravitační
působení	působení	k1gNnSc1	působení
Slunce	slunce	k1gNnSc2	slunce
sahá	sahat	k5eAaImIp3nS	sahat
až	až	k9	až
do	do	k7c2	do
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
asi	asi	k9	asi
2	[number]	k4	2
světelných	světelný	k2eAgNnPc2d1	světelné
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
125	[number]	k4	125
000	[number]	k4	000
AU	au	k0	au
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
než	než	k8xS	než
odhadovaný	odhadovaný	k2eAgInSc1d1	odhadovaný
průměr	průměr	k1gInSc1	průměr
Oortova	Oortův	k2eAgInSc2d1	Oortův
oblaku	oblak	k1gInSc2	oblak
<g/>
.	.	kIx.	.
</s>
<s>
Lidstvo	lidstvo	k1gNnSc1	lidstvo
však	však	k9	však
zatím	zatím	k6eAd1	zatím
nemá	mít	k5eNaImIp3nS	mít
nástroje	nástroj	k1gInPc4	nástroj
pro	pro	k7c4	pro
podrobnější	podrobný	k2eAgInSc4d2	podrobnější
průzkum	průzkum	k1gInSc4	průzkum
této	tento	k3xDgFnSc2	tento
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
průzkumu	průzkum	k1gInSc2	průzkum
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
</s>
</p>
<p>
<s>
ORBITER	ORBITER	kA	ORBITER
Space	Space	k1gMnSc1	Space
Flight	Flight	k1gMnSc1	Flight
Simulator	Simulator	k1gMnSc1	Simulator
–	–	k?	–
simulátor	simulátor	k1gInSc1	simulátor
letů	let	k1gInPc2	let
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
pro	pro	k7c4	pro
PC	PC	kA	PC
<g/>
/	/	kIx~	/
<g/>
Win	Win	k1gMnSc1	Win
<g/>
,	,	kIx,	,
freeware	freeware	k1gInSc1	freeware
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
sluneční	sluneční	k2eAgFnSc1d1	sluneční
soustava	soustava	k1gFnSc1	soustava
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc7	jejichž
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Sluneční	sluneční	k2eAgFnSc1d1	sluneční
soustava	soustava	k1gFnSc1	soustava
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
sluneční	sluneční	k2eAgFnSc1d1	sluneční
soustava	soustava	k1gFnSc1	soustava
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Kniha	kniha	k1gFnSc1	kniha
Sluneční	sluneční	k2eAgFnSc1d1	sluneční
soustava	soustava	k1gFnSc1	soustava
ve	v	k7c6	v
Wikiknihách	Wikikniha	k1gFnPc6	Wikikniha
</s>
</p>
<p>
<s>
Články	článek	k1gInPc1	článek
České	český	k2eAgFnSc2d1	Česká
astronomické	astronomický	k2eAgFnSc2d1	astronomická
společnosti	společnost	k1gFnSc2	společnost
</s>
</p>
<p>
<s>
Vznik	vznik	k1gInSc1	vznik
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Galaxie	galaxie	k1gFnSc2	galaxie
</s>
</p>
<p>
<s>
Vznik	vznik	k1gInSc1	vznik
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
na	na	k7c6	na
Matematicko-fyzikálním	matematickoyzikální	k2eAgInSc6d1	matematicko-fyzikální
webu	web	k1gInSc6	web
Magdy	Magda	k1gFnSc2	Magda
Vlachové	Vlachová	k1gFnSc2	Vlachová
</s>
</p>
<p>
<s>
Web	web	k1gInSc1	web
o	o	k7c6	o
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
ZČU	ZČU	kA	ZČU
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Porovnání	porovnání	k1gNnSc1	porovnání
planet	planeta	k1gFnPc2	planeta
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
<g/>
,	,	kIx,	,
planet	planeta	k1gFnPc2	planeta
se	s	k7c7	s
Sluncem	slunce	k1gNnSc7	slunce
<g/>
,	,	kIx,	,
Jupiter	Jupiter	k1gInSc1	Jupiter
a	a	k8xC	a
Slunce	slunce	k1gNnSc1	slunce
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
hvězdami	hvězda	k1gFnPc7	hvězda
</s>
</p>
<p>
<s>
National	Nationat	k5eAaPmAgMnS	Nationat
Geographic	Geographic	k1gMnSc1	Geographic
</s>
</p>
<p>
<s>
Sluneční	sluneční	k2eAgFnSc1d1	sluneční
soustava	soustava	k1gFnSc1	soustava
nebo	nebo	k8xC	nebo
sluneční	sluneční	k2eAgFnSc1d1	sluneční
soustava	soustava	k1gFnSc1	soustava
–	–	k?	–
diskuse	diskuse	k1gFnSc2	diskuse
o	o	k7c6	o
pravopisu	pravopis	k1gInSc6	pravopis
</s>
</p>
<p>
<s>
Sluneční	sluneční	k2eAgFnSc1d1	sluneční
soustava	soustava	k1gFnSc1	soustava
-	-	kIx~	-
Jména	jméno	k1gNnPc1	jméno
planet	planeta	k1gFnPc2	planeta
<g/>
,	,	kIx,	,
boskowan.com	boskowan.com	k1gInSc1	boskowan.com
<g/>
;	;	kIx,	;
Jiří	Jiří	k1gMnSc1	Jiří
Wagner	Wagner	k1gMnSc1	Wagner
<g/>
,	,	kIx,	,
Jr	Jr	k1gMnSc1	Jr
<g/>
.	.	kIx.	.
</s>
<s>
Navštíveno	navštíven	k2eAgNnSc1d1	navštíveno
2018-07-01	[number]	k4	2018-07-01
<g/>
.	.	kIx.	.
</s>
</p>
