<s>
Planety	planeta	k1gFnPc1	planeta
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
obíhají	obíhat	k5eAaImIp3nP	obíhat
po	po	k7c6	po
eliptických	eliptický	k2eAgFnPc6d1	eliptická
drahách	draha	k1gFnPc6	draha
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
společném	společný	k2eAgNnSc6d1	společné
ohnisku	ohnisko	k1gNnSc6	ohnisko
oběžných	oběžný	k2eAgFnPc2d1	oběžná
elips	elipsa	k1gFnPc2	elipsa
<g/>
.	.	kIx.	.
</s>
