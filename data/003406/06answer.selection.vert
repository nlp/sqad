<s>
Nervová	nervový	k2eAgFnSc1d1	nervová
soustava	soustava	k1gFnSc1	soustava
ptáků	pták	k1gMnPc2	pták
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
vyšším	vysoký	k2eAgInSc6d2	vyšší
stupni	stupeň	k1gInSc6	stupeň
vývoje	vývoj	k1gInSc2	vývoj
než	než	k8xS	než
u	u	k7c2	u
plazů	plaz	k1gMnPc2	plaz
a	a	k8xC	a
proto	proto	k8xC	proto
jejich	jejich	k3xOp3gInPc1	jejich
životní	životní	k2eAgInPc1d1	životní
projevy	projev	k1gInPc1	projev
jsou	být	k5eAaImIp3nP	být
dokonalejší	dokonalý	k2eAgInPc1d2	dokonalejší
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc4	jejich
schopnost	schopnost	k1gFnSc4	schopnost
přizpůsobovat	přizpůsobovat	k5eAaImF	přizpůsobovat
se	se	k3xPyFc4	se
podmínkám	podmínka	k1gFnPc3	podmínka
prostředí	prostředí	k1gNnSc2	prostředí
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
.	.	kIx.	.
</s>
