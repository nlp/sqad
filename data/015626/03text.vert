<s>
Palanga	Palanga	k1gFnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaImF,k5eAaPmF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Palanga	Palanga	k1gFnSc1
Palanga	Palanga	k1gFnSc1
Pláž	pláž	k1gFnSc1
v	v	k7c6
Palanze	Palanza	k1gFnSc6
na	na	k7c6
břehu	břeh	k1gInSc6
Baltského	baltský	k2eAgNnSc2d1
moře	moře	k1gNnSc2
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
Poloha	poloha	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
55	#num#	k4
<g/>
°	°	k?
<g/>
55	#num#	k4
<g/>
′	′	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
21	#num#	k4
<g/>
°	°	k?
<g/>
3	#num#	k4
<g/>
′	′	k?
<g/>
50	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
0	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Stát	stát	k1gInSc1
</s>
<s>
Litva	Litva	k1gFnSc1
Litva	Litva	k1gFnSc1
kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
sritis	sritis	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Klaipė	Klaipė	k2eAgInSc1d1
(	(	kIx(
<g/>
Klaipė	Klaipė	k1gInSc1
<g/>
)	)	kIx)
městský	městský	k2eAgInSc1d1
okres	okres	k1gInSc1
</s>
<s>
Palanga	Palanga	k1gFnSc1
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
79	#num#	k4
km²	km²	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
17	#num#	k4
574	#num#	k4
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
222,5	222,5	k4
obyv	obyv	k1gInSc1
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Správa	správa	k1gFnSc1
Starosta	Starosta	k1gMnSc1
</s>
<s>
Šarū	Šarū	k1gMnSc1
Vaitkus	Vaitkus	k1gMnSc1
Oficiální	oficiální	k2eAgInSc4d1
web	web	k1gInSc4
</s>
<s>
www.palanga.lt	www.palanga.lt	k1gMnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Palanga	Palanga	k1gFnSc1
je	být	k5eAaImIp3nS
lázeňské	lázeňský	k2eAgNnSc1d1
město	město	k1gNnSc1
v	v	k7c6
západní	západní	k2eAgFnSc6d1
části	část	k1gFnSc6
Litvy	Litva	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c6
břehu	břeh	k1gInSc6
Baltského	baltský	k2eAgNnSc2d1
moře	moře	k1gNnSc2
<g/>
,	,	kIx,
25	#num#	k4
km	km	kA
na	na	k7c4
sever	sever	k1gInSc4
od	od	k7c2
přístavního	přístavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Klaipė	Klaipė	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klimatické	klimatický	k2eAgFnPc4d1
a	a	k8xC
balneologické	balneologický	k2eAgFnPc4d1
lázně	lázeň	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pod	pod	k7c4
město	město	k1gNnSc4
Palanga	Palang	k1gMnSc4
spadá	spadat	k5eAaPmIp3nS,k5eAaImIp3nS
<g/>
:	:	kIx,
vlastní	vlastní	k2eAgNnSc1d1
město	město	k1gNnSc1
Palanga	Palanga	k1gFnSc1
<g/>
,	,	kIx,
lázeňské	lázeňský	k2eAgNnSc1d1
město	město	k1gNnSc1
Šventoji	Šventoj	k1gInSc3
<g/>
,	,	kIx,
obec	obec	k1gFnSc1
Bū	Bū	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
městě	město	k1gNnSc6
jsou	být	k5eAaImIp3nP
tři	tři	k4xCgFnPc4
pošty	pošta	k1gFnPc4
<g/>
,	,	kIx,
(	(	kIx(
<g/>
PSČ	PSČ	kA
hlavní	hlavní	k2eAgFnSc2d1
pošty	pošta	k1gFnSc2
je	být	k5eAaImIp3nS
LT-	LT-	k1gFnSc1
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Přírodopisné	přírodopisný	k2eAgInPc1d1
a	a	k8xC
zeměpisné	zeměpisný	k2eAgInPc1d1
údaje	údaj	k1gInPc1
</s>
<s>
Městem	město	k1gNnSc7
protéká	protékat	k5eAaImIp3nS
řeka	řeka	k1gFnSc1
Rąžė	Rąžė	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
ve	v	k7c6
městě	město	k1gNnSc6
překlenuta	překlenut	k2eAgFnSc1d1
deseti	deset	k4xCc7
mosty	most	k1gInPc7
<g/>
,	,	kIx,
můstky	můstek	k1gInPc7
nebo	nebo	k8xC
lávkami	lávka	k1gFnPc7
<g/>
,	,	kIx,
její	její	k3xOp3gInPc1
přítoky	přítok	k1gInPc1
a	a	k8xC
v	v	k7c6
severní	severní	k2eAgFnSc6d1
části	část	k1gFnSc6
také	také	k9
potok	potok	k1gInSc1
Vanagupė	Vanagupė	k1gFnPc2
(	(	kIx(
<g/>
se	s	k7c7
svým	svůj	k3xOyFgInSc7
přítokem	přítok	k1gInSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
podle	podle	k7c2
kterého	který	k3yRgInSc2,k3yQgInSc2,k3yIgInSc2
dostala	dostat	k5eAaPmAgFnS
název	název	k1gInSc4
městská	městský	k2eAgFnSc1d1
čtvrť	čtvrť	k1gFnSc1
Vanagupė	Vanagupė	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Západní	západní	k2eAgInSc1d1
okraj	okraj	k1gInSc1
města	město	k1gNnSc2
omývá	omývat	k5eAaImIp3nS
Baltské	baltský	k2eAgNnSc4d1
moře	moře	k1gNnSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
tu	tu	k6eAd1
mnoho	mnoho	k4c4
pláží	pláž	k1gFnPc2
(	(	kIx(
<g/>
vlastně	vlastně	k9
nepřetržitá	přetržitý	k2eNgFnSc1d1
pláž	pláž	k1gFnSc1
od	od	k7c2
Klaipė	Klaipė	k1gFnSc2
až	až	k9
do	do	k7c2
hranice	hranice	k1gFnSc2
s	s	k7c7
Lotyšskem	Lotyšsko	k1gNnSc7
v	v	k7c6
Bū	Bū	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
východním	východní	k2eAgInSc6d1
okraji	okraj	k1gInSc6
Palangy	Palang	k1gInPc4
je	být	k5eAaImIp3nS
zahrádkářská	zahrádkářský	k2eAgFnSc1d1
kolonie	kolonie	k1gFnSc1
"	"	kIx"
<g/>
Pavė	Pavė	k1gNnSc1
<g/>
"	"	kIx"
kolektyviniai	kolektyviniai	k1gNnSc1
sodai	soda	k1gFnSc2
(	(	kIx(
<g/>
Pavė	Pavė	k1gInSc1
litevsky	litevsky	k6eAd1
znamená	znamenat	k5eAaImIp3nS
chládek	chládek	k1gInSc1
<g/>
/	/	kIx~
<g/>
stín	stín	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jižním	jižní	k2eAgInSc6d1
okraji	okraj	k1gInSc6
je	být	k5eAaImIp3nS
Nemirseta	Nemirseta	k1gFnSc1
a	a	k8xC
etnografický	etnografický	k2eAgInSc1d1
hřbitov	hřbitov	k1gInSc1
Anaičių	Anaičių	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
severním	severní	k2eAgInSc6d1
okraji	okraj	k1gInSc6
u	u	k7c2
čtvrti	čtvrt	k1gFnSc2
Kunigiškiai	Kunigiškiai	k1gNnSc2
je	být	k5eAaImIp3nS
letiště	letiště	k1gNnSc4
Palanga	Palang	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Východní	východní	k2eAgInSc4d1
okraj	okraj	k1gInSc4
Palangy	Palang	k1gInPc7
zatěžuje	zatěžovat	k5eAaImIp3nS
prozatímní	prozatímní	k2eAgInSc1d1
provoz	provoz	k1gInSc1
nedokončené	dokončený	k2eNgFnSc2d1
dálnice	dálnice	k1gFnSc2
A13	A13	k1gMnSc1
Klaipė	Klaipė	k1gMnSc1
-	-	kIx~
Bū	Bū	k1gMnSc1
-	-	kIx~
Liepā	Liepā	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Palanze	Palanza	k1gFnSc6
je	být	k5eAaImIp3nS
také	také	k9
hippodrom	hippodrom	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Směrem	směr	k1gInSc7
ke	k	k7c3
Klaipė	Klaipė	k1gFnSc3
jsou	být	k5eAaImIp3nP
také	také	k9
zřízeny	zřízen	k2eAgFnPc1d1
stezky	stezka	k1gFnPc1
pro	pro	k7c4
cyklisty	cyklista	k1gMnPc4
a	a	k8xC
pro	pro	k7c4
pěší	pěší	k2eAgFnPc4d1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
dále	daleko	k6eAd2
za	za	k7c7
Klaipė	Klaipė	k1gFnSc7
pokračují	pokračovat	k5eAaImIp3nP
různými	různý	k2eAgInPc7d1
směry	směr	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Směrem	směr	k1gInSc7
k	k	k7c3
Lotyšsku	Lotyšsko	k1gNnSc3
tyto	tento	k3xDgFnPc4
stezky	stezka	k1gFnPc1
vedou	vést	k5eAaImIp3nP
až	až	k9
ke	k	k7c3
státní	státní	k2eAgFnSc3d1
hranici	hranice	k1gFnSc3
směrem	směr	k1gInSc7
přes	přes	k7c4
Šventoji	Šventoj	k1gInSc3
<g/>
.	.	kIx.
</s>
<s>
Sport	sport	k1gInSc1
</s>
<s>
FK	FK	kA
Palanga	Palang	k1gMnSc4
fotbalový	fotbalový	k2eAgInSc1d1
klub	klub	k1gInSc1
<g/>
;	;	kIx,
</s>
<s>
KK	KK	kA
Palanga	Palang	k1gMnSc4
basketbalový	basketbalový	k2eAgInSc1d1
klub	klub	k1gInSc1
<g/>
;	;	kIx,
</s>
<s>
Městské	městský	k2eAgFnSc3d1
čtvrti	čtvrt	k1gFnSc3
</s>
<s>
Anaičiai	Anaičiai	k6eAd1
</s>
<s>
Bū	Bū	k?
</s>
<s>
Dobilas	Dobilas	k1gMnSc1
</s>
<s>
Kalgraužiai	Kalgraužiai	k6eAd1
</s>
<s>
Karvelynas	Karvelynas	k1gMnSc1
</s>
<s>
Kunigiškiai	Kunigiškiai	k6eAd1
</s>
<s>
Kontininkai	Kontininkai	k6eAd1
</s>
<s>
Manciškė	Manciškė	k?
</s>
<s>
Nemirseta	Nemirseta	k1gFnSc1
</s>
<s>
Paliepgiriai	Paliepgiriai	k6eAd1
</s>
<s>
Plytinė	Plytinė	k?
</s>
<s>
Šventoji	Šventoj	k1gInSc3
</s>
<s>
Užkanavė	Užkanavė	k?
</s>
<s>
Vanagupė	Vanagupė	k?
</s>
<s>
Vilimiškė	Vilimiškė	k?
</s>
<s>
Virbališkė	Virbališkė	k?
</s>
<s>
Partnerská	partnerský	k2eAgNnPc1d1
města	město	k1gNnPc1
</s>
<s>
Město	město	k1gNnSc1
</s>
<s>
Znak	znak	k1gInSc1
</s>
<s>
Stát	stát	k1gInSc1
</s>
<s>
Frederiksberg	Frederiksberg	k1gMnSc1
</s>
<s>
Dánsko	Dánsko	k1gNnSc1
Dánsko	Dánsko	k1gNnSc1
</s>
<s>
Jū	Jū	k5eAaPmAgFnS,k5eAaImAgFnS
</s>
<s>
Lotyšsko	Lotyšsko	k1gNnSc1
Lotyšsko	Lotyšsko	k1gNnSc1
</s>
<s>
Liepā	Liepā	k6eAd1
</s>
<s>
Lotyšsko	Lotyšsko	k1gNnSc1
Lotyšsko	Lotyšsko	k1gNnSc1
</s>
<s>
Łódź	Łódź	k?
</s>
<s>
Polsko	Polsko	k1gNnSc1
Polsko	Polsko	k1gNnSc1
</s>
<s>
Ustka	Ustka	k6eAd1
</s>
<s>
Polsko	Polsko	k1gNnSc1
Polsko	Polsko	k1gNnSc1
</s>
<s>
Pärnu	Pärnout	k5eAaImIp1nS,k5eAaPmIp1nS
</s>
<s>
Estonsko	Estonsko	k1gNnSc1
Estonsko	Estonsko	k1gNnSc1
</s>
<s>
Simrishamn	Simrishamn	k1gMnSc1
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Palanga	Palang	k1gMnSc2
na	na	k7c6
litevské	litevský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Palanga	Palang	k1gMnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Stránky	stránka	k1gFnPc1
Palangos	Palangosa	k1gFnPc2
savivaldybė	savivaldybė	k6eAd1
(	(	kIx(
<g/>
"	"	kIx"
<g/>
okresu	okres	k1gInSc2
<g/>
"	"	kIx"
statutární	statutární	k2eAgNnSc1d1
město	město	k1gNnSc1
Palanga	Palanga	k1gFnSc1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
v	v	k7c6
litevštině	litevština	k1gFnSc6
<g/>
)	)	kIx)
</s>
<s>
Turistické	turistický	k2eAgNnSc1d1
informační	informační	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
Palangy	Palang	k1gInPc7
(	(	kIx(
<g/>
v	v	k7c6
litevštině	litevština	k1gFnSc6
<g/>
)	)	kIx)
</s>
<s>
Palanga	Palanga	k1gFnSc1
v	v	k7c6
Kuršské	Kuršský	k2eAgFnSc6d1
gubernii	gubernie	k1gFnSc6
(	(	kIx(
<g/>
v	v	k7c6
litevštině	litevština	k1gFnSc6
<g/>
)	)	kIx)
</s>
<s>
Mapa	mapa	k1gFnSc1
města	město	k1gNnSc2
</s>
<s>
Vrácení	vrácení	k1gNnSc1
Palangy	Palang	k1gInPc1
Litvě	Litva	k1gFnSc6
(	(	kIx(
<g/>
v	v	k7c6
litevštině	litevština	k1gFnSc6
<g/>
)	)	kIx)
</s>
<s>
Palanga	Palanga	k1gFnSc1
<g/>
:	:	kIx,
události	událost	k1gFnPc1
<g/>
,	,	kIx,
čísla	číslo	k1gNnPc1
<g/>
,	,	kIx,
fakta	faktum	k1gNnPc1
(	(	kIx(
<g/>
v	v	k7c6
litevštině	litevština	k1gFnSc6
<g/>
)	)	kIx)
</s>
<s>
Palác	palác	k1gInSc1
Tiškevičiů	Tiškeviči	k1gMnPc2
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
Muzeum	muzeum	k1gNnSc1
jantaru	jantar	k1gInSc2
v	v	k7c6
Palanze	Palanza	k1gFnSc6
(	(	kIx(
<g/>
v	v	k7c6
litevštině	litevština	k1gFnSc6
<g/>
)	)	kIx)
</s>
<s>
Radio	radio	k1gNnSc1
"	"	kIx"
<g/>
FM	FM	kA
Palanga	Palanga	k1gFnSc1
<g/>
"	"	kIx"
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Litva	Litva	k1gFnSc1
–	–	k?
Lietuva	Lietuva	k1gFnSc1
–	–	k?
(	(	kIx(
<g/>
LT	LT	kA
<g/>
)	)	kIx)
Kraje	kraj	k1gInPc1
(	(	kIx(
<g/>
apskritis	apskritis	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
jejich	jejich	k3xOp3gNnPc4
hlavní	hlavní	k2eAgNnPc4d1
města	město	k1gNnPc4
</s>
<s>
Alytuský	Alytuský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
Alytus	Alytus	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Kaunaský	Kaunaský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
Kaunas	Kaunas	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Klaipė	Klaipė	k2eAgInSc1d1
kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
Klaipė	Klaipė	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Marijampolský	Marijampolský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
Marijampolė	Marijampolė	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Panevė	Panevė	k2eAgInSc1d1
kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
Panevė	Panevė	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Šiauliaiský	Šiauliaiský	k2eAgInSc4d1
kraj	kraj	k1gInSc4
(	(	kIx(
<g/>
Šiauliai	Šiauliae	k1gFnSc4
<g/>
)	)	kIx)
•	•	k?
Tauragė	Tauragė	k2eAgInSc1d1
kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
Tauragė	Tauragė	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Telšiajský	Telšiajský	k2eAgInSc4d1
kraj	kraj	k1gInSc4
(	(	kIx(
<g/>
Telšiai	Telšiae	k1gFnSc4
<g/>
)	)	kIx)
•	•	k?
Utenský	Utenský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
Utena	Utena	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Vilniuský	vilniuský	k2eAgInSc4d1
kraj	kraj	k1gInSc4
(	(	kIx(
<g/>
Hlavní	hlavní	k2eAgFnSc1d1
město	město	k1gNnSc4
Vilnius	Vilnius	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Města	město	k1gNnPc1
v	v	k7c6
Litvě	Litva	k1gFnSc6
Krajská	krajský	k2eAgFnSc1d1
města	město	k1gNnSc2
</s>
<s>
Alytus	Alytus	k1gMnSc1
•	•	k?
Kaunas	Kaunas	k1gMnSc1
•	•	k?
Klaipė	Klaipė	k1gMnSc1
•	•	k?
Marijampolė	Marijampolė	k1gMnSc1
•	•	k?
Panevė	Panevė	k1gInSc1
•	•	k?
Šiauliai	Šiaulia	k1gFnSc2
•	•	k?
Tauragė	Tauragė	k1gFnSc2
•	•	k?
Telšiai	Telšia	k1gFnSc2
•	•	k?
Utena	Uten	k1gInSc2
•	•	k?
Vilnius	Vilnius	k1gMnSc1
Okresní	okresní	k2eAgMnSc1d1
města	město	k1gNnSc2
</s>
<s>
Anykščiai	Anykščiai	k6eAd1
•	•	k?
Birštonas	Birštonas	k1gInSc1
•	•	k?
Biržai	Birža	k1gFnSc2
•	•	k?
Druskininkai	Druskininka	k1gFnSc2
•	•	k?
Elektrė	Elektrė	k1gFnSc2
•	•	k?
Gargždai	Gargžda	k1gFnSc2
•	•	k?
Ignalina	Ignalin	k2eAgFnSc1d1
•	•	k?
Jonava	Jonava	k1gFnSc1
•	•	k?
Joniškis	Joniškis	k1gInSc1
•	•	k?
Jurbarkas	Jurbarkas	k1gInSc1
•	•	k?
Kaišiadorys	Kaišiadorys	k1gInSc1
•	•	k?
Kalvarija	Kalvarijus	k1gMnSc2
•	•	k?
Kazlų	Kazlų	k1gMnSc2
Rū	Rū	k1gMnSc2
•	•	k?
Kė	Kė	k1gFnSc2
•	•	k?
Kelmė	Kelmė	k1gMnSc2
•	•	k?
Kretinga	Kreting	k1gMnSc2
•	•	k?
Kupiškis	Kupiškis	k1gFnSc2
•	•	k?
Lazdijai	Lazdija	k1gFnSc2
•	•	k?
Mažeikiai	Mažeikiae	k1gFnSc4
•	•	k?
Molė	Molė	k1gFnSc3
•	•	k?
Naujoji	Naujoj	k1gInSc6
Akmenė	Akmenė	k1gFnSc2
•	•	k?
Neringa	Neringa	k1gFnSc1
•	•	k?
Pagė	Pagė	k1gFnSc2
•	•	k?
Pakruojis	Pakruojis	k1gFnSc2
•	•	k?
Palanga	Palanga	k1gFnSc1
•	•	k?
Pasvalys	Pasvalysa	k1gFnPc2
•	•	k?
Plungė	Plungė	k1gMnSc1
•	•	k?
Prienai	Priena	k1gFnSc2
•	•	k?
Radviliškis	Radviliškis	k1gFnSc2
•	•	k?
Raseiniai	Raseinia	k1gFnSc2
•	•	k?
Rietavas	Rietavas	k1gInSc1
•	•	k?
Rokiškis	Rokiškis	k1gInSc1
•	•	k?
Skuodas	Skuodas	k1gInSc1
•	•	k?
Šakiai	Šakia	k1gFnSc2
•	•	k?
Šalčininkai	Šalčininka	k1gFnSc2
•	•	k?
Šilalė	Šilalė	k1gMnSc1
•	•	k?
Šilutė	Šilutė	k1gMnSc1
•	•	k?
Širvintos	Širvintos	k1gMnSc1
•	•	k?
Švenčionys	Švenčionys	k1gInSc1
•	•	k?
Trakai	Traka	k1gFnSc2
•	•	k?
Ukmergė	Ukmergė	k1gMnSc2
•	•	k?
Varė	Varė	k1gMnSc2
•	•	k?
Vilkaviškis	Vilkaviškis	k1gFnSc2
•	•	k?
Visaginas	Visaginas	k1gMnSc1
•	•	k?
Zarasai	Zarasa	k1gFnSc2
Další	další	k2eAgNnPc1d1
města	město	k1gNnPc1
</s>
<s>
Akmenė	Akmenė	k?
•	•	k?
Ariogala	Ariogala	k1gFnSc1
•	•	k?
Baltoji	Baltoj	k1gInSc6
Vokė	Vokė	k1gFnSc2
-	-	kIx~
Daugai	Daugai	k1gNnSc1
•	•	k?
Dū	Dū	k1gMnSc1
•	•	k?
Dusetos	Dusetos	k1gMnSc1
•	•	k?
Eišiškė	Eišiškė	k1gInSc1
•	•	k?
Ežerė	Ežerė	k1gInSc1
•	•	k?
Garliava	Garliava	k1gFnSc1
•	•	k?
Gelgaudiškis	Gelgaudiškis	k1gInSc1
•	•	k?
Grigiškė	Grigiškė	k1gInSc1
•	•	k?
Jieznas	Jieznas	k1gInSc1
•	•	k?
Joniškė	Joniškė	k1gInSc1
•	•	k?
Kavarskas	Kavarskas	k1gInSc1
•	•	k?
Kybartai	Kybarta	k1gFnSc2
•	•	k?
Kudirkos	Kudirkos	k1gMnSc1
Naumiestis	Naumiestis	k1gFnSc2
•	•	k?
Kuršė	Kuršė	k1gFnSc2
•	•	k?
Lentvaris	Lentvaris	k1gFnSc2
•	•	k?
Linkuva	Linkuv	k1gMnSc2
•	•	k?
Nemenčinė	Nemenčinė	k1gMnSc2
-	-	kIx~
Obeliai	Obeliai	k1gNnSc1
•	•	k?
Pabradė	Pabradė	k1gFnPc2
•	•	k?
Pandė	Pandė	k1gFnPc2
•	•	k?
Panemunė	Panemunė	k1gMnSc2
•	•	k?
Priekulė	Priekulė	k1gMnSc2
•	•	k?
Ramygala	Ramygal	k1gMnSc2
•	•	k?
Rū	Rū	k1gInSc1
•	•	k?
Salantai	Salanta	k1gFnSc2
•	•	k?
Seda	Seda	k1gMnSc1
•	•	k?
Simnas	Simnas	k1gMnSc1
•	•	k?
Skaudvilė	Skaudvilė	k1gMnSc1
•	•	k?
Smalininkai	Smalininka	k1gFnSc2
•	•	k?
Subačius	Subačius	k1gMnSc1
•	•	k?
Šeduva	Šeduva	k1gFnSc1
•	•	k?
Švenčionė	Švenčionė	k1gFnSc2
•	•	k?
Tytuvė	Tytuvė	k1gFnSc2
•	•	k?
Troškū	Troškū	k1gFnSc2
•	•	k?
Užventis	Užventis	k1gFnSc2
•	•	k?
Vabalninkas	Vabalninkas	k1gMnSc1
•	•	k?
Varniai	Varnia	k1gFnSc2
•	•	k?
Veisiejai	Veisieja	k1gFnSc2
•	•	k?
Venta	Venta	k1gMnSc1
•	•	k?
Viekšniai	Viekšnia	k1gFnSc2
•	•	k?
Vievis	Vievis	k1gFnSc2
•	•	k?
Vilkija	Vilkija	k1gMnSc1
•	•	k?
Virbalis	Virbalis	k1gFnSc2
•	•	k?
Žagarė	Žagarė	k1gFnSc2
•	•	k?
Žiežmariai	Žiežmaria	k1gFnSc2
</s>
<s>
Klaipė	Klaipė	k2eAgInSc1d1
kraj	kraj	k1gInSc1
Okresy	okres	k1gInPc1
</s>
<s>
Klaipė	Klaipė	k1gFnSc1
(	(	kIx(
<g/>
město	město	k1gNnSc1
<g/>
/	/	kIx~
<g/>
městský	městský	k2eAgInSc1d1
okres	okres	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Klaipė	Klaipė	k2eAgInSc1d1
okres	okres	k1gInSc1
(	(	kIx(
<g/>
sídlo	sídlo	k1gNnSc1
v	v	k7c6
Gargždai	Gargžda	k1gFnSc6
<g/>
)	)	kIx)
•	•	k?
Kretinga	Kreting	k1gMnSc2
•	•	k?
Neringa	Nering	k1gMnSc2
•	•	k?
Palanga	Palang	k1gMnSc2
(	(	kIx(
<g/>
lázeňský	lázeňský	k2eAgInSc1d1
okres	okres	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Skuodas	Skuodas	k1gInSc1
•	•	k?
Šilutė	Šilutė	k1gFnSc2
Města	město	k1gNnSc2
</s>
<s>
Klaipė	Klaipė	k1gFnSc1
•	•	k?
Gargždai	Gargžda	k1gFnSc2
•	•	k?
Kretinga	Kreting	k1gMnSc2
•	•	k?
Neringa	Nering	k1gMnSc2
•	•	k?
Palanga	Palang	k1gMnSc2
•	•	k?
Priekulė	Priekulė	k1gMnSc2
•	•	k?
Salantai	Salanta	k1gFnSc2
•	•	k?
Skuodas	Skuodas	k1gMnSc1
•	•	k?
Šilutė	Šilutė	k1gFnSc4
•	•	k?
Šventoji	Šventoj	k1gInSc3
(	(	kIx(
<g/>
součást	součást	k1gFnSc1
Palangy	Palang	k1gInPc1
<g/>
)	)	kIx)
Městečka	městečko	k1gNnPc1
</s>
<s>
Barstyčiai	Barstyčia	k1gMnPc1
•	•	k?
Darbė	Darbė	k1gNnSc2
•	•	k?
Dovilai	Dovila	k1gMnSc3
•	•	k?
Endriejavas	Endriejavas	k1gMnSc1
•	•	k?
Gardamas	Gardamas	k1gMnSc1
•	•	k?
Judrė	Judrė	k1gFnSc2
•	•	k?
Kartena	Karten	k2eAgFnSc1d1
•	•	k?
Katyčiai	Katyčia	k1gFnSc3
•	•	k?
Kintai	Kinta	k1gInSc6
•	•	k?
Kretingalė	Kretingalė	k1gFnSc2
•	•	k?
Lenkimai	Lenkima	k1gFnSc2
•	•	k?
Mosė	Mosė	k1gFnSc2
•	•	k?
Plikiai	Plikia	k1gFnSc2
•	•	k?
Rusnė	Rusnė	k1gMnSc1
•	•	k?
Saugos	Saugos	k1gMnSc1
•	•	k?
Švė	Švė	k1gFnSc1
•	•	k?
Vainutas	Vainutas	k1gInSc1
•	•	k?
Veiviržė	Veiviržė	k1gFnSc2
•	•	k?
Vė	Vė	k1gFnSc2
•	•	k?
Ylakiai	Ylakia	k1gFnSc2
•	•	k?
Žemaičių	Žemaičių	k1gFnSc1
Naumiestis	Naumiestis	k1gFnSc1
Vesnice	vesnice	k1gFnSc1
</s>
<s>
Ablinga	Ablinga	k1gFnSc1
•	•	k?
Apuolė	Apuolė	k1gFnSc2
•	•	k?
Agluonė	Agluonė	k1gFnSc2
•	•	k?
Bū	Bū	k1gFnSc1
(	(	kIx(
<g/>
součást	součást	k1gFnSc1
Palangy	Palang	k1gInPc1
<g/>
)	)	kIx)
•	•	k?
Dreverna	Dreverna	k1gFnSc1
•	•	k?
Jakai	Jaka	k1gFnSc2
•	•	k?
Jokū	Jokū	k1gMnSc1
•	•	k?
Kū	Kū	k1gFnSc2
•	•	k?
Lankupiai	Lankupia	k1gFnSc2
•	•	k?
Mažučiai	Mažučia	k1gFnSc2
•	•	k?
Minija	Minij	k2eAgFnSc1d1
•	•	k?
Nemirseta	Nemirseta	k1gFnSc1
(	(	kIx(
<g/>
součást	součást	k1gFnSc1
Palangy	Palang	k1gInPc1
<g/>
)	)	kIx)
•	•	k?
Radailiai	Radailiai	k1gNnPc2
•	•	k?
Senoji	Senoj	k1gInSc6
Į	Į	k1gFnSc2
•	•	k?
Šaukliai	Šauklia	k1gFnSc2
•	•	k?
Uostadvaris	Uostadvaris	k1gFnSc2
•	•	k?
Vydmantai	Vydmanta	k1gFnSc2
•	•	k?
Žibininkai	Žibininka	k1gFnSc2
Významné	významný	k2eAgFnSc2d1
samoty	samota	k1gFnSc2
</s>
<s>
Gargždelė	Gargždelė	k?
</s>
<s>
Dálnice	dálnice	k1gFnSc1
v	v	k7c6
Litvě	Litva	k1gFnSc6
</s>
<s>
A1	A1	k4
•	•	k?
A2	A2	k1gMnSc1
•	•	k?
A3	A3	k1gMnSc1
•	•	k?
A4	A4	k1gMnSc1
•	•	k?
A5	A5	k1gMnSc1
•	•	k?
A6	A6	k1gMnSc1
•	•	k?
A7	A7	k1gMnSc1
•	•	k?
A8	A8	k1gMnSc1
•	•	k?
A9	A9	k1gMnSc1
•	•	k?
A10	A10	k1gMnSc1
•	•	k?
A11	A11	k1gMnSc1
•	•	k?
A12	A12	k1gMnSc1
•	•	k?
A13	A13	k1gMnSc1
•	•	k?
A14	A14	k1gMnSc1
•	•	k?
A15	A15	k1gMnSc1
•	•	k?
A16	A16	k1gMnSc1
•	•	k?
A17	A17	k1gMnSc1
•	•	k?
A18	A18	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Litva	Litva	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
477562	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4046476-3	4046476-3	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
245638210	#num#	k4
</s>
