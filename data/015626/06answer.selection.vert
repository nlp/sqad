<s>
Palanga	Palanga	k1gFnSc1
je	být	k5eAaImIp3nS
lázeňské	lázeňský	k2eAgNnSc1d1
město	město	k1gNnSc1
v	v	k7c6
západní	západní	k2eAgFnSc6d1
části	část	k1gFnSc6
Litvy	Litva	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c6
břehu	břeh	k1gInSc6
Baltského	baltský	k2eAgNnSc2d1
moře	moře	k1gNnSc2
<g/>
,	,	kIx,
25	#num#	k4
km	km	kA
na	na	k7c4
sever	sever	k1gInSc4
od	od	k7c2
přístavního	přístavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Klaipė	Klaipė	k1gFnSc2
<g/>
.	.	kIx.
</s>