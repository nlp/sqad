<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Castiglione	Castiglion	k1gInSc5
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojován	ozdrojován	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yRgFnPc4,k3yIgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Castiglione	Castiglion	k1gInSc5
</s>
<s>
konflikt	konflikt	k1gInSc1
<g/>
:	:	kIx,
Francouzské	francouzský	k2eAgFnSc2d1
revoluční	revoluční	k2eAgFnSc2d1
války	válka	k1gFnSc2
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Castiglione	Castiglion	k1gInSc5
(	(	kIx(
<g/>
Victor	Victor	k1gMnSc1
Adam	Adam	k1gMnSc1
<g/>
,	,	kIx,
1836	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
trvání	trvání	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1796	#num#	k4
</s>
<s>
místo	místo	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Castiglione	Castiglion	k1gInSc5
delle	dell	k1gMnSc5
Stiviere	Stivier	k1gMnSc5
(	(	kIx(
<g/>
Itálie	Itálie	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
výsledek	výsledek	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
francouzské	francouzský	k2eAgNnSc4d1
vítězství	vítězství	k1gNnSc4
</s>
<s>
strany	strana	k1gFnPc1
</s>
<s>
První	první	k4xOgFnSc1
Francouzská	francouzský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
Habsburská	habsburský	k2eAgFnSc1d1
monarchie	monarchie	k1gFnSc1
Habsburská	habsburský	k2eAgFnSc1d1
monarchie	monarchie	k1gFnSc1
</s>
<s>
velitelé	velitel	k1gMnPc1
</s>
<s>
Napoleon	Napoleon	k1gMnSc1
Bonaparte	bonapart	k1gInSc5
</s>
<s>
Dagobert	Dagobert	k1gMnSc1
Sigmund	Sigmund	k1gMnSc1
von	von	k1gInSc4
Wurmser	Wurmser	k1gInSc1
</s>
<s>
síla	síla	k1gFnSc1
</s>
<s>
30	#num#	k4
000	#num#	k4
</s>
<s>
37	#num#	k4
000	#num#	k4
</s>
<s>
ztráty	ztráta	k1gFnPc1
</s>
<s>
1	#num#	k4
100	#num#	k4
-	-	kIx~
1	#num#	k4
500	#num#	k4
</s>
<s>
3	#num#	k4
000	#num#	k4
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Castiliogne	Castiliogne	k1gInSc2
proběhla	proběhnout	k5eAaPmAgNnP
v	v	k7c6
srpnu	srpen	k1gInSc6
roku	rok	k1gInSc2
1796	#num#	k4
poblíž	poblíž	k7c2
italského	italský	k2eAgNnSc2d1
města	město	k1gNnSc2
Castiglione	Castiglione	k1gInSc1
delle	dell	k1gInSc1
Stiviere	Stivier	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vojska	vojsko	k1gNnPc1
Napoleona	Napoleon	k1gMnSc2
Bonaparte	Bonaparte	k1gInSc5
se	se	k1gMnSc2
zde	zde	k6eAd1
střetla	střetnout	k5eAaPmAgNnP
s	s	k7c7
armádou	armáda	k1gFnSc7
vedenou	vedený	k2eAgFnSc4d1
rakouským	rakouský	k2eAgMnSc7d1
maršálem	maršál	k1gMnSc7
Wurmserem	Wurmser	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
údajně	údajně	k6eAd1
před	před	k7c7
bitvou	bitva	k1gFnSc7
slíbil	slíbit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
Bonaparta	Bonaparte	k1gMnSc2
ztrestá	ztrestat	k5eAaPmIp3nS
odvahou	odvaha	k1gFnSc7
a	a	k8xC
pošle	poslat	k5eAaPmIp3nS
ho	on	k3xPp3gMnSc4
zpátky	zpátky	k6eAd1
do	do	k7c2
školy	škola	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Napoleon	napoleon	k1gInSc4
však	však	k8xC
bitvu	bitva	k1gFnSc4
vyhrál	vyhrát	k5eAaPmAgMnS
a	a	k8xC
dále	daleko	k6eAd2
neuvěřitelnou	uvěřitelný	k2eNgFnSc7d1
rychlostí	rychlost	k1gFnSc7
postupoval	postupovat	k5eAaImAgInS
severní	severní	k2eAgFnSc7d1
Itálií	Itálie	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Průběh	průběh	k1gInSc1
</s>
<s>
Wurmser	Wurmser	k1gMnSc1
rozdělil	rozdělit	k5eAaPmAgMnS
armádu	armáda	k1gFnSc4
na	na	k7c4
3	#num#	k4
části	část	k1gFnSc2
<g/>
,	,	kIx,
chtěl	chtít	k5eAaImAgMnS
tak	tak	k6eAd1
snáze	snadno	k6eAd2
stisknout	stisknout	k5eAaPmF
Francouze	Francouz	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
západě	západ	k1gInSc6
měl	mít	k5eAaImAgInS
8000	#num#	k4
mužů	muž	k1gMnPc2
pod	pod	k7c7
velením	velení	k1gNnSc7
von	von	k1gInSc1
Davidoviche	Davidoviche	k1gInSc1
<g/>
,	,	kIx,
na	na	k7c6
východě	východ	k1gInSc6
útočilo	útočit	k5eAaImAgNnS
na	na	k7c4
Augereaua	Augereauum	k1gNnPc4
5000	#num#	k4
Rakušanů	Rakušan	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
střed	střed	k1gInSc4
se	se	k3xPyFc4
staral	starat	k5eAaImAgMnS
sám	sám	k3xTgMnSc1
Wurmser	Wurmser	k1gMnSc1
s	s	k7c7
24	#num#	k4
000	#num#	k4
vojáky	voják	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Napoleon	napoleon	k1gInSc1
musel	muset	k5eAaImAgInS
rychle	rychle	k6eAd1
jednat	jednat	k5eAaImF
<g/>
,	,	kIx,
rozdělil	rozdělit	k5eAaPmAgMnS
armádu	armáda	k1gFnSc4
na	na	k7c4
menší	malý	k2eAgInPc4d2
oddíly	oddíl	k1gInPc4
a	a	k8xC
porážel	porážet	k5eAaImAgMnS
jednu	jeden	k4xCgFnSc4
rakouskou	rakouský	k2eAgFnSc4d1
kolonu	kolona	k1gFnSc4
po	po	k7c6
druhé	druhý	k4xOgFnSc6
<g/>
,	,	kIx,
tento	tento	k3xDgInSc1
tah	tah	k1gInSc1
se	se	k3xPyFc4
zapsal	zapsat	k5eAaPmAgInS
do	do	k7c2
historie	historie	k1gFnSc2
jako	jako	k8xC,k8xS
„	„	k?
<g/>
manévr	manévr	k1gInSc1
od	od	k7c2
Castiliogne	Castiliogn	k1gInSc5
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Francouzská	francouzský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
pochodovala	pochodovat	k5eAaImAgFnS
40	#num#	k4
km	km	kA
a	a	k8xC
3	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
porazila	porazit	k5eAaPmAgFnS
Davidoviche	Davidoviche	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
neměl	mít	k5eNaImAgInS
zprávy	zpráva	k1gFnPc4
o	o	k7c6
pohybu	pohyb	k1gInSc6
Wurmserových	Wurmserův	k2eAgMnPc2d1
vojáků	voják	k1gMnPc2
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
raději	rád	k6eAd2
ustoupil	ustoupit	k5eAaPmAgMnS
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
Napoleon	napoleon	k1gInSc1
zvítězil	zvítězit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rakouské	rakouský	k2eAgInPc1d1
vojsko	vojsko	k1gNnSc1
se	se	k3xPyFc4
dalo	dát	k5eAaPmAgNnS
na	na	k7c4
ústup	ústup	k1gInSc4
a	a	k8xC
Francouzi	Francouz	k1gMnPc1
jej	on	k3xPp3gMnSc4
hnali	hnát	k5eAaImAgMnP
přes	přes	k7c4
Itálii	Itálie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
ŠŤOVÍČEK	Šťovíček	k1gMnSc1
<g/>
,	,	kIx,
Michal	Michal	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Francie	Francie	k1gFnSc1
proti	proti	k7c3
Evropě	Evropa	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Epocha	epocha	k1gFnSc1
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
.	.	kIx.
499	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7557	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
41	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
