<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Castiliogne	Castiliogne	k1gInSc2
proběhla	proběhnout	k5eAaPmAgNnP
v	v	k7c6
srpnu	srpen	k1gInSc6
roku	rok	k1gInSc2
1796	#num#	k4
poblíž	poblíž	k7c2
italského	italský	k2eAgNnSc2d1
města	město	k1gNnSc2
Castiglione	Castiglione	k1gInSc1
delle	dell	k1gInSc1
Stiviere	Stivier	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vojska	vojsko	k1gNnPc1
Napoleona	Napoleon	k1gMnSc2
Bonaparte	Bonaparte	k1gInSc5
se	se	k1gMnSc2
zde	zde	k6eAd1
střetla	střetnout	k5eAaPmAgNnP
s	s	k7c7
armádou	armáda	k1gFnSc7
vedenou	vedený	k2eAgFnSc4d1
rakouským	rakouský	k2eAgMnSc7d1
maršálem	maršál	k1gMnSc7
Wurmserem	Wurmser	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
údajně	údajně	k6eAd1
před	před	k7c7
bitvou	bitva	k1gFnSc7
slíbil	slíbit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
Bonaparta	Bonaparte	k1gMnSc2
ztrestá	ztrestat	k5eAaPmIp3nS
odvahou	odvaha	k1gFnSc7
a	a	k8xC
pošle	poslat	k5eAaPmIp3nS
ho	on	k3xPp3gMnSc4
zpátky	zpátky	k6eAd1
do	do	k7c2
školy	škola	k1gFnSc2
<g/>
.	.	kIx.
</s>