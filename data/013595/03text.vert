<s>
Serbia	Serbia	k1gFnSc1
Open	Open	k1gNnSc1
2009	#num#	k4
</s>
<s>
Serbia	Serbia	k1gFnSc1
Open	Open	k1gNnSc1
2009	#num#	k4
Datum	datum	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
-	-	kIx~
10	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2009	#num#	k4
Ročník	ročník	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Místo	místo	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Bělehrad	Bělehrad	k1gInSc1
Dotace	dotace	k1gFnSc2
<g/>
:	:	kIx,
</s>
<s>
450,000	450,000	k4
€	€	k?
Povrch	povrch	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
Antuka	antuka	k1gFnSc1
Web	web	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
Vítězové	vítěz	k1gMnPc1
</s>
<s>
mužská	mužský	k2eAgFnSc1d1
dvouhra	dvouhra	k1gFnSc1
</s>
<s>
Novak	Novak	k1gMnSc1
Đoković	Đoković	k1gMnSc1
</s>
<s>
mužská	mužský	k2eAgFnSc1d1
čtyřhra	čtyřhra	k1gFnSc1
</s>
<s>
Łukasz	Łukasz	k1gMnSc1
Kubot	Kubot	k1gMnSc1
/	/	kIx~
Oliver	Oliver	k1gMnSc1
Marach	Marach	k1gMnSc1
</s>
<s>
Serbia	Serbia	k1gFnSc1
Open	Opena	k1gFnPc2
</s>
<s>
2010	#num#	k4
>	>	kIx)
</s>
<s>
Tenisový	tenisový	k2eAgInSc1d1
turnaj	turnaj	k1gInSc1
ATP	atp	kA
Serbia	Serbia	k1gFnSc1
Open	Open	k1gInSc1
2009	#num#	k4
se	se	k3xPyFc4
konal	konat	k5eAaImAgInS
ve	v	k7c6
dnech	den	k1gInPc6
4	#num#	k4
<g/>
.	.	kIx.
–	–	k?
10	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
v	v	k7c6
srbském	srbský	k2eAgNnSc6d1
hlavním	hlavní	k2eAgNnSc6d1
městě	město	k1gNnSc6
Bělehradě	Bělehrad	k1gInSc6
venku	venku	k6eAd1
na	na	k7c6
antukových	antukový	k2eAgInPc6d1
dvorcích	dvorec	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odměny	odměna	k1gFnSc2
činily	činit	k5eAaImAgInP
450,000	450,000	k4
EUR	euro	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Dvouhra	dvouhra	k1gFnSc1
</s>
<s>
Nasazení	nasazení	k1gNnSc1
hráčů	hráč	k1gMnPc2
</s>
<s>
První	první	k4xOgFnSc1
4	#num#	k4
hráči	hráč	k1gMnPc1
měli	mít	k5eAaImAgMnP
v	v	k7c6
1	#num#	k4
<g/>
.	.	kIx.
kole	kolo	k1gNnSc6
volný	volný	k2eAgInSc1d1
los	los	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Č.	Č.	kA
</s>
<s>
Tenista	tenista	k1gMnSc1
</s>
<s>
Žebříček	žebříček	k1gInSc1
ATP	atp	kA
</s>
<s>
1	#num#	k4
</s>
<s>
Novak	Novak	k1gMnSc1
Đoković	Đoković	k1gMnSc1
</s>
<s>
3	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
Ivo	Ivo	k1gMnSc1
Karlović	Karlović	k1gMnSc1
</s>
<s>
24	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
Igor	Igor	k1gMnSc1
Andrejev	Andrejev	k1gMnSc1
</s>
<s>
25	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
Andreas	Andreas	k1gInSc1
Seppi	Sepp	k1gFnSc2
</s>
<s>
37	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
Viktor	Viktor	k1gMnSc1
Troicki	Troick	k1gFnSc2
</s>
<s>
40	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
Ivan	Ivan	k1gMnSc1
Ljubičić	Ljubičić	k1gMnSc1
</s>
<s>
52	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
Arnaud	Arnaud	k1gMnSc1
Clément	Clément	k1gMnSc1
</s>
<s>
54	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
Christophe	Christophe	k6eAd1
Rochus	Rochus	k1gInSc1
</s>
<s>
55	#num#	k4
</s>
<s>
Ceny	cena	k1gFnPc1
a	a	k8xC
body	bod	k1gInPc1
do	do	k7c2
žebříčku	žebříček	k1gInSc2
</s>
<s>
KoloBody	KoloBod	k1gInPc1
ATPČástka	ATPČástka	k1gFnSc1
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
250	#num#	k4
</s>
<s>
75,500	75,500	k4
EUR	euro	k1gNnPc2
</s>
<s>
Finalista	finalista	k1gMnSc1
</s>
<s>
150	#num#	k4
</s>
<s>
39,700	39,700	k4
EUR	euro	k1gNnPc2
</s>
<s>
Semifinále	semifinále	k1gNnSc1
</s>
<s>
90	#num#	k4
</s>
<s>
21,060	21,060	k4
EUR	euro	k1gNnPc2
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
</s>
<s>
45	#num#	k4
</s>
<s>
11,425	11,425	k4
EUR	euro	k1gNnPc2
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
</s>
<s>
20	#num#	k4
</s>
<s>
6,740	6,740	k4
EUR	euro	k1gNnPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
</s>
<s>
0	#num#	k4
</s>
<s>
4,055	4,055	k4
EUR	euro	k1gNnPc2
</s>
<s>
Pavouk	pavouk	k1gMnSc1
</s>
<s>
Finálová	finálový	k2eAgFnSc1d1
fáze	fáze	k1gFnSc1
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
</s>
<s>
Semifinále	semifinále	k1gNnSc1
</s>
<s>
Finále	finále	k1gNnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
Novak	Novak	k1gMnSc1
Đoković	Đoković	k1gMnSc1
</s>
<s>
6	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
Viktor	Viktor	k1gMnSc1
Troicki	Troick	k1gFnSc2
</s>
<s>
3	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
Novak	Novak	k1gMnSc1
Đoković	Đoković	k1gMnSc1
</s>
<s>
4	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
Andreas	Andreas	k1gInSc1
Seppi	Sepp	k1gFnSc2
</s>
<s>
6	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
Andreas	Andreas	k1gInSc1
Seppi	Sepp	k1gFnSc2
</s>
<s>
6	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
Marcos	Marcos	k1gMnSc1
Daniel	Daniel	k1gMnSc1
</s>
<s>
4	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
Novak	Novak	k1gMnSc1
Đoković	Đoković	k1gMnSc1
</s>
<s>
6	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
LL	LL	kA
</s>
<s>
Łukasz	Łukasz	k1gMnSc1
Kubot	Kubot	k1gMnSc1
</s>
<s>
3	#num#	k4
</s>
<s>
60	#num#	k4
</s>
<s>
Kristof	Kristof	k1gInSc1
Vliegen	Vliegen	k1gInSc1
</s>
<s>
66	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
LL	LL	kA
</s>
<s>
Łukasz	Łukasz	k1gMnSc1
Kubot	Kubot	k1gMnSc1
</s>
<s>
7	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
LL	LL	kA
</s>
<s>
Łukasz	Łukasz	k1gMnSc1
Kubot	Kubot	k1gMnSc1
</s>
<s>
7	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
Ivo	Ivo	k1gMnSc1
Karlović	Karlović	k1gMnSc1
</s>
<s>
60	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
Q	Q	kA
</s>
<s>
Flavio	Flavio	k1gMnSc1
Cipolla	Cipolla	k1gMnSc1
</s>
<s>
3	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
Ivo	Ivo	k1gMnSc1
Karlović	Karlović	k1gMnSc1
</s>
<s>
6	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
Horní	horní	k2eAgFnSc1d1
polovina	polovina	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolo	kolo	k1gNnSc1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolo	kolo	k1gNnSc1
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
Novak	Novak	k1gMnSc1
Đoković	Đoković	k1gMnSc1
</s>
<s>
Volný	volný	k2eAgInSc1d1
los	los	k1gInSc1
</s>
<s>
1	#num#	k4
</s>
<s>
Novak	Novak	k1gMnSc1
Đoković	Đoković	k1gMnSc1
</s>
<s>
6	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
Janko	Janko	k1gMnSc1
Tipsarević	Tipsarević	k1gMnSc1
</s>
<s>
2	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
Janko	Janko	k1gMnSc1
Tipsarević	Tipsarević	k1gMnSc1
</s>
<s>
7	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
Sergio	Sergio	k1gMnSc1
Roitman	Roitman	k1gMnSc1
</s>
<s>
5	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
Novak	Novak	k1gMnSc1
Đoković	Đoković	k1gMnSc1
</s>
<s>
6	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
Viktor	Viktor	k1gMnSc1
Troicki	Troick	k1gFnSc2
</s>
<s>
3	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
WC	WC	kA
</s>
<s>
Filip	Filip	k1gMnSc1
Krajinović	Krajinović	k1gMnSc1
</s>
<s>
0	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
Marcel	Marcel	k1gMnSc1
Granollers	Granollersa	k1gFnPc2
</s>
<s>
6	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
Marcel	Marcel	k1gMnSc1
Granollers	Granollersa	k1gFnPc2
</s>
<s>
3	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
Viktor	Viktor	k1gMnSc1
Troicki	Troick	k1gFnSc2
</s>
<s>
6	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
Sam	Sam	k1gMnSc1
Querrey	Querrea	k1gFnSc2
</s>
<s>
6	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
Viktor	Viktor	k1gMnSc1
Troicki	Troick	k1gFnSc2
</s>
<s>
4	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolo	kolo	k1gNnSc1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolo	kolo	k1gNnSc1
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
</s>
<s>
4	#num#	k4
</s>
<s>
Andreas	Andreas	k1gInSc1
Seppi	Sepp	k1gFnSc2
</s>
<s>
Volný	volný	k2eAgInSc1d1
los	los	k1gInSc1
</s>
<s>
4	#num#	k4
</s>
<s>
Andreas	Andreas	k1gInSc1
Seppi	Sepp	k1gFnSc2
</s>
<s>
6	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
Leonardo	Leonardo	k1gMnSc1
Mayer	Mayer	k1gMnSc1
</s>
<s>
4	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
Wayne	Waynout	k5eAaPmIp3nS,k5eAaImIp3nS
Odesnik	Odesnik	k1gInSc1
</s>
<s>
3	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
Leonardo	Leonardo	k1gMnSc1
Mayer	Mayer	k1gMnSc1
</s>
<s>
6	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
Andreas	Andreas	k1gInSc1
Seppi	Sepp	k1gFnSc2
</s>
<s>
6	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
Marcos	Marcos	k1gMnSc1
Daniel	Daniel	k1gMnSc1
</s>
<s>
4	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
Marcos	Marcos	k1gMnSc1
Daniel	Daniel	k1gMnSc1
</s>
<s>
3	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
Guillermo	Guillermo	k6eAd1
Cañ	Cañ	k1gInSc1
</s>
<s>
6	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
Marcos	Marcos	k1gMnSc1
Daniel	Daniel	k1gMnSc1
</s>
<s>
6	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
Ivan	Ivan	k1gMnSc1
Ljubičić	Ljubičić	k1gMnSc1
</s>
<s>
4	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
Q	Q	kA
</s>
<s>
Victor	Victor	k1gMnSc1
Crivoi	Crivo	k1gFnSc2
</s>
<s>
62	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
Ivan	Ivan	k1gMnSc1
Ljubičić	Ljubičić	k1gMnSc1
</s>
<s>
7	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
Dolní	dolní	k2eAgFnSc1d1
polovina	polovina	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolo	kolo	k1gNnSc1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolo	kolo	k1gNnSc1
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
</s>
<s>
8	#num#	k4
</s>
<s>
Christophe	Christophe	k6eAd1
Rochus	Rochus	k1gInSc1
</s>
<s>
6	#num#	k4
</s>
<s>
67	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
Nicolas	Nicolas	k1gMnSc1
Devilder	Devilder	k1gMnSc1
</s>
<s>
3	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
Christophe	Christophe	k6eAd1
Rochus	Rochus	k1gInSc1
</s>
<s>
6	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
Kristof	Kristof	k1gInSc1
Vliegen	Vliegen	k1gInSc1
</s>
<s>
3	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
Kristof	Kristof	k1gInSc1
Vliegen	Vliegen	k1gInSc1
</s>
<s>
2	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
Q	Q	kA
</s>
<s>
Dominik	Dominik	k1gMnSc1
Hrbatý	hrbatý	k2eAgMnSc1d1
</s>
<s>
6	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
Kristof	Kristof	k1gInSc1
Vliegen	Vliegen	k1gInSc1
</s>
<s>
66	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
LL	LL	kA
</s>
<s>
Łukasz	Łukasz	k1gMnSc1
Kubot	Kubot	k1gMnSc1
</s>
<s>
7	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
LL	LL	kA
</s>
<s>
Łukasz	Łukasz	k1gMnSc1
Kubot	Kubot	k1gMnSc1
</s>
<s>
6	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
WC	WC	kA
</s>
<s>
Arsenije	Arsenít	k5eAaPmIp3nS
Zlatanović	Zlatanović	k1gFnSc1
</s>
<s>
3	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
LL	LL	kA
</s>
<s>
Łukasz	Łukasz	k1gMnSc1
Kubot	Kubot	k1gMnSc1
</s>
<s>
3	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
Igor	Igor	k1gMnSc1
Andrejev	Andrejev	k1gMnSc1
</s>
<s>
2	#num#	k4
</s>
<s>
r	r	kA
</s>
<s>
Volný	volný	k2eAgInSc1d1
los	los	k1gInSc1
</s>
<s>
3	#num#	k4
</s>
<s>
Igor	Igor	k1gMnSc1
Andrejev	Andrejev	k1gMnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolo	kolo	k1gNnSc1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolo	kolo	k1gNnSc1
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
</s>
<s>
7	#num#	k4
</s>
<s>
Arnaud	Arnaud	k1gMnSc1
Clément	Clément	k1gMnSc1
</s>
<s>
6	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
Q	Q	kA
</s>
<s>
Santiago	Santiago	k1gNnSc1
Ventura	Ventura	kA
</s>
<s>
1	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
Arnaud	Arnaud	k1gMnSc1
Clément	Clément	k1gMnSc1
</s>
<s>
6	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
Q	Q	kA
</s>
<s>
Flavio	Flavio	k1gMnSc1
Cipolla	Cipolla	k1gMnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
Q	Q	kA
</s>
<s>
Flavio	Flavio	k1gMnSc1
Cipolla	Cipolla	k1gMnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
WC	WC	kA
</s>
<s>
Marcos	Marcos	k1gInSc1
Baghdatis	Baghdatis	k1gFnSc2
</s>
<s>
6	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
Q	Q	kA
</s>
<s>
Flavio	Flavio	k1gMnSc1
Cipolla	Cipolla	k1gMnSc1
</s>
<s>
3	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
Ivo	Ivo	k1gMnSc1
Karlović	Karlović	k1gMnSc1
</s>
<s>
6	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
Brian	Brian	k1gMnSc1
Dabul	Dabula	k1gFnPc2
</s>
<s>
6	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
Nicolás	Nicolás	k1gInSc1
Massú	Massú	k1gFnSc2
</s>
<s>
2	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
Brian	Brian	k1gMnSc1
Dabul	Dabula	k1gFnPc2
</s>
<s>
4	#num#	k4
</s>
<s>
65	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
Ivo	Ivo	k1gMnSc1
Karlović	Karlović	k1gMnSc1
</s>
<s>
6	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
Volný	volný	k2eAgInSc1d1
los	los	k1gInSc1
</s>
<s>
2	#num#	k4
</s>
<s>
Ivo	Ivo	k1gMnSc1
Karlović	Karlović	k1gMnSc1
</s>
<s>
Čtyřhra	čtyřhra	k1gFnSc1
</s>
<s>
Nasazení	nasazení	k1gNnSc1
hráčů	hráč	k1gMnPc2
</s>
<s>
Č.	Č.	kA
</s>
<s>
Tenisté	tenista	k1gMnPc1
</s>
<s>
Žebříček	žebříček	k1gInSc1
ATP	atp	kA
</s>
<s>
1	#num#	k4
</s>
<s>
Daniel	Daniel	k1gMnSc1
Nestor	Nestor	k1gMnSc1
Nenad	Nenad	k1gInSc4
Zimonjić	Zimonjić	k1gFnSc2
</s>
<s>
6	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
Łukasz	Łukasz	k1gMnSc1
Kubot	Kubot	k1gMnSc1
Oliver	Oliver	k1gMnSc1
Marach	Marach	k1gMnSc1
</s>
<s>
52	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
Stephen	Stephen	k2eAgInSc1d1
Huss	Huss	k1gInSc1
Ross	Rossa	k1gFnPc2
Hutchins	Hutchinsa	k1gFnPc2
</s>
<s>
64	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
Simon	Simon	k1gMnSc1
Aspelin	Aspelina	k1gFnPc2
Paul	Paul	k1gMnSc1
Hanley	Hanlea	k1gFnSc2
</s>
<s>
93	#num#	k4
</s>
<s>
Ceny	cena	k1gFnPc1
a	a	k8xC
body	bod	k1gInPc1
do	do	k7c2
žebříčku	žebříček	k1gInSc2
</s>
<s>
KoloBody	KoloBod	k1gInPc1
ATPČástka	ATPČástka	k1gFnSc1
</s>
<s>
Vítězové	vítěz	k1gMnPc1
</s>
<s>
250	#num#	k4
</s>
<s>
22,750	22,750	k4
EUR	euro	k1gNnPc2
</s>
<s>
Finalisté	finalista	k1gMnPc1
</s>
<s>
150	#num#	k4
</s>
<s>
11,450	11,450	k4
EUR	euro	k1gNnPc2
</s>
<s>
Semifinále	semifinále	k1gNnSc1
</s>
<s>
90	#num#	k4
</s>
<s>
6,200	6,200	k4
EUR	euro	k1gNnPc2
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
</s>
<s>
45	#num#	k4
</s>
<s>
3,440	3,440	k4
EUR	euro	k1gNnPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
</s>
<s>
0	#num#	k4
</s>
<s>
940	#num#	k4
EUR	euro	k1gNnPc2
</s>
<s>
Pavouk	pavouk	k1gMnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolo	kolo	k1gNnSc1
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
</s>
<s>
Semifinále	semifinále	k1gNnSc1
</s>
<s>
Finále	finále	k1gNnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
Daniel	Daniel	k1gMnSc1
Nestor	Nestor	k1gMnSc1
Nenad	Nenad	k1gInSc4
Zimonjić	Zimonjić	k1gMnSc3
</s>
<s>
62	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
WC	WC	kA
</s>
<s>
Marko	Marko	k1gMnSc5
Đoković	Đoković	k1gMnSc5
Darko	Darka	k1gMnSc5
Madjarovski	Madjarovsk	k1gMnSc5
</s>
<s>
7	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
WC	WC	kA
</s>
<s>
Marko	Marko	k1gMnSc5
Đoković	Đoković	k1gMnSc5
Darko	Darka	k1gMnSc5
Madjarovski	Madjarovsk	k1gMnSc5
</s>
<s>
2	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
Johan	Johan	k1gMnSc1
Brunström	Brunström	k1gMnSc1
Jean-Julien	Jean-Julien	k2eAgInSc4d1
Rojer	Rojer	k1gInSc4
</s>
<s>
6	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
Johan	Johan	k1gMnSc1
Brunström	Brunström	k1gMnSc1
Jean-Julien	Jean-Julien	k2eAgInSc4d1
Rojer	Rojer	k1gInSc4
</s>
<s>
6	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
Christophe	Christophat	k5eAaPmIp3nS
Rochus	Rochus	k1gMnSc1
Lovro	Lovro	k1gNnSc4
Zovko	Zovko	k1gNnSc5
</s>
<s>
2	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
Johan	Johan	k1gMnSc1
Brunström	Brunström	k1gMnSc1
Jean-Julien	Jean-Julien	k2eAgInSc4d1
Rojer	Rojer	k1gInSc4
</s>
<s>
6	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
Stephen	Stephen	k2eAgInSc1d1
Huss	Huss	k1gInSc1
Ross	Rossa	k1gFnPc2
Hutchins	Hutchinsa	k1gFnPc2
</s>
<s>
7	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
Marcel	Marcel	k1gMnSc1
Granollers	Granollers	k1gInSc1
Santiago	Santiago	k1gNnSc4
Ventura	Ventura	kA
</s>
<s>
2	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1
Levinský	Levinský	k2eAgMnSc1d1
Jean-Claude	Jean-Claud	k1gMnSc5
Scherrer	Scherrra	k1gFnPc2
</s>
<s>
66	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
Stephen	Stephen	k2eAgInSc1d1
Huss	Huss	k1gInSc1
Ross	Rossa	k1gFnPc2
Hutchins	Hutchinsa	k1gFnPc2
</s>
<s>
64	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
Marcel	Marcel	k1gMnSc1
Granollers	Granollers	k1gInSc1
Santiago	Santiago	k1gNnSc4
Ventura	Ventura	kA
</s>
<s>
6	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
Marcel	Marcel	k1gMnSc1
Granollers	Granollers	k1gInSc1
Santiago	Santiago	k1gNnSc4
Ventura	Ventura	kA
</s>
<s>
7	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
Sam	Sam	k1gMnSc1
Querrey	Querrea	k1gFnSc2
Sergio	Sergio	k1gMnSc1
Roitman	Roitman	k1gMnSc1
</s>
<s>
4	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
Johan	Johan	k1gMnSc1
Brunström	Brunström	k1gMnSc1
Jean-Julien	Jean-Julien	k2eAgInSc4d1
Rojer	Rojer	k1gInSc4
</s>
<s>
2	#num#	k4
</s>
<s>
63	#num#	k4
</s>
<s>
Rogier	Rogier	k1gMnSc1
Wassen	Wassna	k1gFnPc2
Igor	Igor	k1gMnSc1
Zelenay	Zelenaa	k1gFnSc2
</s>
<s>
7	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
Łukasz	Łukasz	k1gMnSc1
Kubot	Kubot	k1gMnSc1
Oliver	Oliver	k1gMnSc1
Marach	Marach	k1gMnSc1
</s>
<s>
6	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
Roko	Roko	k1gMnSc1
Karanušić	Karanušić	k1gMnSc1
Ivo	Ivo	k1gMnSc1
Karlović	Karlović	k1gMnSc1
</s>
<s>
64	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
Rogier	Rogier	k1gMnSc1
Wassen	Wassna	k1gFnPc2
Igor	Igor	k1gMnSc1
Zelenay	Zelenaa	k1gFnSc2
</s>
<s>
6	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
Arnaud	Arnaud	k1gMnSc1
Clément	Clément	k1gMnSc1
Nicolas	Nicolas	k1gMnSc1
Devilder	Devilder	k1gMnSc1
</s>
<s>
3	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
Simon	Simon	k1gMnSc1
Aspelin	Aspelina	k1gFnPc2
Paul	Paul	k1gMnSc1
Hanley	Hanlea	k1gFnSc2
</s>
<s>
4	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
Simon	Simon	k1gMnSc1
Aspelin	Aspelina	k1gFnPc2
Paul	Paul	k1gMnSc1
Hanley	Hanlea	k1gFnSc2
</s>
<s>
6	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
Rogier	Rogier	k1gMnSc1
Wassen	Wassna	k1gFnPc2
Igor	Igor	k1gMnSc1
Zelenay	Zelenaa	k1gFnSc2
</s>
<s>
5	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
Andrei	Andrea	k1gFnSc3
Pavel	Pavel	k1gMnSc1
Horia	Horius	k1gMnSc4
Tecău	Tecăa	k1gMnSc4
</s>
<s>
4	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
Łukasz	Łukasz	k1gMnSc1
Kubot	Kubot	k1gMnSc1
Oliver	Oliver	k1gMnSc1
Marach	Marach	k1gMnSc1
</s>
<s>
7	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
WC	WC	kA
</s>
<s>
Nikola	Nikola	k1gMnSc1
Ćirić	Ćirić	k1gMnSc1
David	David	k1gMnSc1
Savić	Savić	k1gMnSc1
</s>
<s>
6	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
WC	WC	kA
</s>
<s>
Nikola	Nikola	k1gMnSc1
Ćirić	Ćirić	k1gMnSc1
David	David	k1gMnSc1
Savić	Savić	k1gMnSc1
</s>
<s>
4	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
Janko	Janko	k1gMnSc1
Tipsarević	Tipsarević	k1gMnSc1
Viktor	Viktor	k1gMnSc1
Troicki	Troicke	k1gFnSc4
</s>
<s>
4	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
Łukasz	Łukasz	k1gMnSc1
Kubot	Kubot	k1gMnSc1
Oliver	Oliver	k1gMnSc1
Marach	Marach	k1gMnSc1
</s>
<s>
6	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
Łukasz	Łukasz	k1gMnSc1
Kubot	Kubot	k1gMnSc1
Oliver	Oliver	k1gMnSc1
Marach	Marach	k1gMnSc1
</s>
<s>
6	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
Pavouk	pavouk	k1gMnSc1
dvouhry	dvouhra	k1gFnSc2
</s>
<s>
Pavouk	pavouk	k1gMnSc1
čtyřhry	čtyřhra	k1gFnSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Oficiální	oficiální	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
turnaje	turnaj	k1gInSc2
</s>
