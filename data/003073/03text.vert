<s>
Sedm	sedm	k4xCc1	sedm
je	být	k5eAaImIp3nS	být
přirozené	přirozený	k2eAgNnSc4d1	přirozené
číslo	číslo	k1gNnSc4	číslo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
následuje	následovat	k5eAaImIp3nS	následovat
po	po	k7c6	po
číslu	číslo	k1gNnSc3	číslo
šest	šest	k4xCc1	šest
a	a	k8xC	a
předchází	předcházet	k5eAaImIp3nS	předcházet
číslu	číslo	k1gNnSc3	číslo
osm	osm	k4xCc1	osm
<g/>
.	.	kIx.	.
</s>
<s>
Římské	římský	k2eAgFnPc4d1	římská
číslice	číslice	k1gFnPc4	číslice
je	být	k5eAaImIp3nS	být
VII	VII	kA	VII
<g/>
.	.	kIx.	.
</s>
<s>
Sedmička	sedmička	k1gFnSc1	sedmička
je	být	k5eAaImIp3nS	být
čtvrté	čtvrtý	k4xOgNnSc1	čtvrtý
prvočíslo	prvočíslo	k1gNnSc1	prvočíslo
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
Mersennovo	Mersennův	k2eAgNnSc1d1	Mersennovo
prvočíslo	prvočíslo	k1gNnSc1	prvočíslo
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
7	[number]	k4	7
=	=	kIx~	=
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
7	[number]	k4	7
<g/>
=	=	kIx~	=
<g/>
2	[number]	k4	2
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
i	i	k9	i
příznivé	příznivý	k2eAgNnSc1d1	příznivé
číslo	číslo	k1gNnSc1	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
jediná	jediný	k2eAgFnSc1d1	jediná
grupa	grupa	k1gFnSc1	grupa
řádu	řád	k1gInSc2	řád
7	[number]	k4	7
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
7	[number]	k4	7
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbb	mathbba	k1gFnPc2	mathbba
{	{	kIx(	{
<g/>
Z	z	k7c2	z
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
7	[number]	k4	7
<g/>
}}	}}	k?	}}
)	)	kIx)	)
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
existuje	existovat	k5eAaImIp3nS	existovat
právě	právě	k9	právě
jedno	jeden	k4xCgNnSc4	jeden
konečné	konečný	k2eAgNnSc4d1	konečné
těleso	těleso	k1gNnSc4	těleso
řádu	řád	k1gInSc2	řád
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
nejmenší	malý	k2eAgNnSc4d3	nejmenší
přirozené	přirozený	k2eAgNnSc4d1	přirozené
číslo	číslo	k1gNnSc4	číslo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
nelze	lze	k6eNd1	lze
napsat	napsat	k5eAaBmF	napsat
jako	jako	k9	jako
součet	součet	k1gInSc1	součet
tří	tři	k4xCgInPc2	tři
čtverců	čtverec	k1gInPc2	čtverec
přirozených	přirozený	k2eAgNnPc2d1	přirozené
čísel	číslo	k1gNnPc2	číslo
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Lagrangeovu	Lagrangeův	k2eAgFnSc4d1	Lagrangeova
větu	věta	k1gFnSc4	věta
o	o	k7c6	o
čtyřech	čtyři	k4xCgInPc6	čtyři
čtvercích	čtverec	k1gInPc6	čtverec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ač	ač	k8xS	ač
tato	tento	k3xDgFnSc1	tento
skutečnost	skutečnost	k1gFnSc1	skutečnost
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
známá	známý	k2eAgFnSc1d1	známá
<g/>
,	,	kIx,	,
existuje	existovat	k5eAaImIp3nS	existovat
poměrně	poměrně	k6eAd1	poměrně
jednoduchý	jednoduchý	k2eAgInSc1d1	jednoduchý
způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
ověřit	ověřit	k5eAaPmF	ověřit
dělitelnost	dělitelnost	k1gFnSc4	dělitelnost
sedmi	sedm	k4xCc2	sedm
<g/>
.	.	kIx.	.
</s>
<s>
Algoritmus	algoritmus	k1gInSc1	algoritmus
je	být	k5eAaImIp3nS	být
následující	následující	k2eAgInSc1d1	následující
<g/>
:	:	kIx,	:
Odstraňte	odstranit	k5eAaPmRp2nP	odstranit
poslední	poslední	k2eAgFnSc4d1	poslední
číslici	číslice	k1gFnSc4	číslice
<g/>
,	,	kIx,	,
Vynásobte	vynásobit	k5eAaPmRp2nP	vynásobit
ji	on	k3xPp3gFnSc4	on
dvěma	dva	k4xCgFnPc7	dva
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
násobek	násobek	k1gInSc4	násobek
odečtěte	odečíst	k5eAaPmRp2nP	odečíst
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
výsledkem	výsledek	k1gInSc7	výsledek
záporné	záporný	k2eAgNnSc4d1	záporné
číslo	číslo	k1gNnSc4	číslo
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
dvojmístné	dvojmístný	k2eAgNnSc1d1	dvojmístné
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
odstraňte	odstranit	k5eAaPmRp2nP	odstranit
znaménko	znaménko	k1gNnSc4	znaménko
mínus	mínus	k1gInSc1	mínus
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc4	tento
kroky	krok	k1gInPc4	krok
opakujte	opakovat	k5eAaImRp2nP	opakovat
dokud	dokud	k8xS	dokud
neskončíte	skončit	k5eNaPmIp2nP	skončit
u	u	k7c2	u
jednociferného	jednociferný	k2eAgNnSc2d1	jednociferné
čísla	číslo	k1gNnSc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
toto	tento	k3xDgNnSc1	tento
číslo	číslo	k1gNnSc1	číslo
dělitelné	dělitelný	k2eAgNnSc1d1	dělitelné
sedmi	sedm	k4xCc6	sedm
(	(	kIx(	(
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
0	[number]	k4	0
nebo	nebo	k8xC	nebo
7	[number]	k4	7
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
původní	původní	k2eAgNnSc4d1	původní
číslo	číslo	k1gNnSc4	číslo
také	také	k9	také
dělitelné	dělitelný	k2eAgNnSc1d1	dělitelné
sedmi	sedm	k4xCc2	sedm
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
číslo	číslo	k1gNnSc4	číslo
1358	[number]	k4	1358
je	být	k5eAaImIp3nS	být
dělitelné	dělitelný	k2eAgNnSc1d1	dělitelné
sedmi	sedm	k4xCc2	sedm
<g/>
:	:	kIx,	:
135	[number]	k4	135
-	-	kIx~	-
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
×	×	k?	×
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
=	=	kIx~	=
119	[number]	k4	119
11	[number]	k4	11
-	-	kIx~	-
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
×	×	k?	×
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
=	=	kIx~	=
-	-	kIx~	-
<g/>
7	[number]	k4	7
Za	za	k7c4	za
použití	použití	k1gNnSc4	použití
Teorie	teorie	k1gFnSc2	teorie
čísel	číslo	k1gNnPc2	číslo
je	být	k5eAaImIp3nS	být
důkaz	důkaz	k1gInSc1	důkaz
snadný	snadný	k2eAgInSc1d1	snadný
<g/>
.	.	kIx.	.
</s>
<s>
Testované	testovaný	k2eAgNnSc1d1	testované
číslo	číslo	k1gNnSc1	číslo
n	n	k0	n
přepíšeme	přepsat	k5eAaPmIp1nP	přepsat
do	do	k7c2	do
podoby	podoba	k1gFnSc2	podoba
<g/>
:	:	kIx,	:
n	n	k0	n
=	=	kIx~	=
10	[number]	k4	10
<g/>
a	a	k8xC	a
+	+	kIx~	+
b	b	k?	b
Kde	kde	k6eAd1	kde
<g/>
:	:	kIx,	:
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
zbývající	zbývající	k2eAgFnPc4d1	zbývající
číslice	číslice	k1gFnPc4	číslice
a	a	k8xC	a
b	b	k?	b
je	být	k5eAaImIp3nS	být
poslední	poslední	k2eAgFnPc4d1	poslední
číslice	číslice	k1gFnPc4	číslice
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
<g/>
:	:	kIx,	:
10	[number]	k4	10
<g/>
a	a	k8xC	a
+	+	kIx~	+
b	b	k?	b
≡	≡	k?	≡
0	[number]	k4	0
(	(	kIx(	(
<g/>
mod	mod	k?	mod
7	[number]	k4	7
<g/>
)	)	kIx)	)
5	[number]	k4	5
×	×	k?	×
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
a	a	k8xC	a
+	+	kIx~	+
b	b	k?	b
<g/>
)	)	kIx)	)
≡	≡	k?	≡
0	[number]	k4	0
(	(	kIx(	(
<g/>
mod	mod	k?	mod
7	[number]	k4	7
<g/>
)	)	kIx)	)
49	[number]	k4	49
<g/>
a	a	k8xC	a
+	+	kIx~	+
a	a	k8xC	a
+	+	kIx~	+
5	[number]	k4	5
<g/>
b	b	k?	b
≡	≡	k?	≡
0	[number]	k4	0
(	(	kIx(	(
<g/>
mod	mod	k?	mod
7	[number]	k4	7
<g/>
)	)	kIx)	)
a	a	k8xC	a
+	+	kIx~	+
5	[number]	k4	5
<g/>
b	b	k?	b
-	-	kIx~	-
7	[number]	k4	7
<g/>
b	b	k?	b
≡	≡	k?	≡
0	[number]	k4	0
(	(	kIx(	(
<g/>
mod	mod	k?	mod
7	[number]	k4	7
<g/>
)	)	kIx)	)
a	a	k8xC	a
-	-	kIx~	-
2	[number]	k4	2
<g/>
b	b	k?	b
≡	≡	k?	≡
0	[number]	k4	0
(	(	kIx(	(
<g/>
mod	mod	k?	mod
7	[number]	k4	7
<g/>
)	)	kIx)	)
Protonové	protonový	k2eAgNnSc1d1	protonové
číslo	číslo	k1gNnSc1	číslo
dusíku	dusík	k1gInSc2	dusík
Počet	počet	k1gInSc1	počet
tónů	tón	k1gInPc2	tón
základní	základní	k2eAgFnSc2d1	základní
stupnice	stupnice	k1gFnSc2	stupnice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
katolické	katolický	k2eAgFnSc6d1	katolická
tradici	tradice	k1gFnSc6	tradice
existuje	existovat	k5eAaImIp3nS	existovat
sedm	sedm	k4xCc1	sedm
smrtelných	smrtelný	k2eAgInPc2d1	smrtelný
hříchů	hřích	k1gInPc2	hřích
<g/>
.	.	kIx.	.
</s>
<s>
Číslo	číslo	k1gNnSc1	číslo
7	[number]	k4	7
označuje	označovat	k5eAaImIp3nS	označovat
plnost	plnost	k1gFnSc1	plnost
a	a	k8xC	a
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
judaismu	judaismus	k1gInSc2	judaismus
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
řádu	řád	k1gInSc3	řád
přírody	příroda	k1gFnSc2	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
je	být	k5eAaImIp3nS	být
číslo	číslo	k1gNnSc1	číslo
7	[number]	k4	7
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
šťastné	šťastný	k2eAgNnSc4d1	šťastné
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Číslo	číslo	k1gNnSc4	číslo
sedm	sedm	k4xCc1	sedm
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
magické	magický	k2eAgMnPc4d1	magický
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Pohádky	pohádka	k1gFnSc2	pohádka
za	za	k7c4	za
sedmero	sedmero	k1gNnSc4	sedmero
řekami	řeka	k1gFnPc7	řeka
a	a	k8xC	a
sedmero	sedmero	k1gNnSc1	sedmero
horami	hora	k1gFnPc7	hora
</s>
