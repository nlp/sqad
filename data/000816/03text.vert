<s>
Sir	sir	k1gMnSc1	sir
Winston	Winston	k1gInSc4	Winston
Leonard	Leonard	k1gMnSc1	Leonard
Spencer-Churchill	Spencer-Churchill	k1gMnSc1	Spencer-Churchill
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1874	[number]	k4	1874
zámek	zámek	k1gInSc1	zámek
Blenheim	Blenheim	k1gInSc4	Blenheim
<g/>
,	,	kIx,	,
Woodstock	Woodstock	k1gInSc1	Woodstock
-	-	kIx~	-
24	[number]	k4	24
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1965	[number]	k4	1965
Londýn	Londýn	k1gInSc1	Londýn
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
britský	britský	k2eAgMnSc1d1	britský
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
premiér	premiér	k1gMnSc1	premiér
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1940	[number]	k4	1940
<g/>
-	-	kIx~	-
<g/>
1945	[number]	k4	1945
a	a	k8xC	a
1951	[number]	k4	1951
<g/>
-	-	kIx~	-
<g/>
1955	[number]	k4	1955
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
spisovatel	spisovatel	k1gMnSc1	spisovatel
(	(	kIx(	(
<g/>
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
<g/>
,	,	kIx,	,
žurnalista	žurnalista	k1gMnSc1	žurnalista
<g/>
,	,	kIx,	,
voják	voják	k1gMnSc1	voják
a	a	k8xC	a
zákonodárce	zákonodárce	k1gMnSc1	zákonodárce
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
státníků	státník	k1gMnPc2	státník
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Churchillovo	Churchillův	k2eAgNnSc1d1	Churchillovo
celé	celý	k2eAgNnSc1d1	celé
jméno	jméno	k1gNnSc1	jméno
znělo	znět	k5eAaImAgNnS	znět
Winston	Winston	k1gInSc4	Winston
Leonardo	Leonardo	k1gMnSc1	Leonardo
Spencer-Churchill	Spencer-Churchill	k1gMnSc1	Spencer-Churchill
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
lorda	lord	k1gMnSc2	lord
Randolph	Randolph	k1gMnSc1	Randolph
Churchilla	Churchilla	k1gMnSc1	Churchilla
ve	v	k7c6	v
veřejném	veřejný	k2eAgInSc6d1	veřejný
životě	život	k1gInSc6	život
užíval	užívat	k5eAaImAgInS	užívat
jen	jen	k9	jen
příjmení	příjmení	k1gNnSc4	příjmení
Churchill	Churchilla	k1gFnPc2	Churchilla
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
knihy	kniha	k1gFnPc1	kniha
vycházejí	vycházet	k5eAaImIp3nP	vycházet
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Winston	Winston	k1gInSc1	Winston
S.	S.	kA	S.
Churchill	Churchill	k1gInSc1	Churchill
nebo	nebo	k8xC	nebo
Winston	Winston	k1gInSc1	Winston
Spencer	Spencer	k1gMnSc1	Spencer
Churchill	Churchill	k1gMnSc1	Churchill
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Winston	Winston	k1gInSc1	Winston
Churchill	Churchill	k1gInSc1	Churchill
publikoval	publikovat	k5eAaBmAgMnS	publikovat
již	již	k9	již
jiný	jiný	k2eAgMnSc1d1	jiný
autor	autor	k1gMnSc1	autor
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
Blenheim	Blenheima	k1gFnPc2	Blenheima
ve	v	k7c6	v
Woodstocku	Woodstocko	k1gNnSc6	Woodstocko
v	v	k7c6	v
hrabství	hrabství	k1gNnSc6	hrabství
Oxfordshire	Oxfordshir	k1gInSc5	Oxfordshir
jako	jako	k8xC	jako
vnuk	vnuk	k1gMnSc1	vnuk
7	[number]	k4	7
<g/>
.	.	kIx.	.
vévody	vévoda	k1gMnPc4	vévoda
z	z	k7c2	z
Marlborough	Marlborougha	k1gFnPc2	Marlborougha
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
lord	lord	k1gMnSc1	lord
Randolph	Randolph	k1gMnSc1	Randolph
Churchill	Churchill	k1gMnSc1	Churchill
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
významným	významný	k2eAgMnSc7d1	významný
politikem	politik	k1gMnSc7	politik
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vyvedl	vyvést	k5eAaPmAgMnS	vyvést
britské	britský	k2eAgMnPc4d1	britský
konzervativce	konzervativec	k1gMnPc4	konzervativec
z	z	k7c2	z
krize	krize	k1gFnSc2	krize
v	v	k7c6	v
osmdesátých	osmdesátý	k4xOgNnPc6	osmdesátý
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
<g/>
,	,	kIx,	,
lady	lady	k1gFnSc1	lady
Jeanette	Jeanett	k1gInSc5	Jeanett
Churchill	Churchillum	k1gNnPc2	Churchillum
(	(	kIx(	(
<g/>
rozená	rozený	k2eAgFnSc1d1	rozená
Jeanette	Jeanett	k1gInSc5	Jeanett
Jerome	Jerom	k1gInSc5	Jerom
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
dcerou	dcera	k1gFnSc7	dcera
amerického	americký	k2eAgMnSc2d1	americký
obchodníka	obchodník	k1gMnSc2	obchodník
Leonarda	Leonardo	k1gMnSc2	Leonardo
Jerome	Jerom	k1gInSc5	Jerom
<g/>
.	.	kIx.	.
</s>
<s>
Žádný	žádný	k3yNgMnSc1	žádný
z	z	k7c2	z
rodičů	rodič	k1gMnPc2	rodič
neprokazoval	prokazovat	k5eNaImAgMnS	prokazovat
Winstonovi	Winston	k1gMnSc3	Winston
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
náklonnost	náklonnost	k1gFnSc4	náklonnost
nebo	nebo	k8xC	nebo
lásku	láska	k1gFnSc4	láska
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
Winston	Winston	k1gInSc1	Winston
Churchill	Churchilla	k1gFnPc2	Churchilla
jako	jako	k8xS	jako
dítě	dítě	k1gNnSc1	dítě
svou	svůj	k3xOyFgFnSc4	svůj
matku	matka	k1gFnSc4	matka
zbožňoval	zbožňovat	k5eAaImAgMnS	zbožňovat
<g/>
.	.	kIx.	.
</s>
<s>
Mladý	mladý	k2eAgInSc1d1	mladý
Winston	Winston	k1gInSc1	Winston
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
v	v	k7c6	v
sedmi	sedm	k4xCc6	sedm
letech	let	k1gInPc6	let
na	na	k7c4	na
elitní	elitní	k2eAgFnSc4d1	elitní
školu	škola	k1gFnSc4	škola
St.	st.	kA	st.
James	James	k1gMnSc1	James
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
však	však	k9	však
vykazoval	vykazovat	k5eAaImAgInS	vykazovat
velice	velice	k6eAd1	velice
špatné	špatný	k2eAgInPc4d1	špatný
výsledky	výsledek	k1gInPc4	výsledek
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
byl	být	k5eAaImAgMnS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
hloupého	hloupý	k2eAgMnSc4d1	hloupý
<g/>
.	.	kIx.	.
</s>
<s>
Snad	snad	k9	snad
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
i	i	k9	i
psychickým	psychický	k2eAgNnSc7d1	psychické
zhroucením	zhroucení	k1gNnSc7	zhroucení
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
po	po	k7c6	po
čase	čas	k1gInSc6	čas
sklátila	sklátit	k5eAaPmAgFnS	sklátit
velice	velice	k6eAd1	velice
silná	silný	k2eAgFnSc1d1	silná
horečka	horečka	k1gFnSc1	horečka
a	a	k8xC	a
zápal	zápal	k1gInSc1	zápal
plic	plíce	k1gFnPc2	plíce
-	-	kIx~	-
nemoc	nemoc	k1gFnSc1	nemoc
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ho	on	k3xPp3gMnSc4	on
poté	poté	k6eAd1	poté
provázela	provázet	k5eAaImAgFnS	provázet
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Zápas	zápas	k1gInSc1	zápas
s	s	k7c7	s
nemocí	nemoc	k1gFnSc7	nemoc
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
jeho	jeho	k3xOp3gInSc3	jeho
zdravotnímu	zdravotní	k2eAgInSc3d1	zdravotní
stavu	stav	k1gInSc3	stav
mu	on	k3xPp3gMnSc3	on
lékař	lékař	k1gMnSc1	lékař
doporučil	doporučit	k5eAaPmAgMnS	doporučit
přestup	přestup	k1gInSc4	přestup
do	do	k7c2	do
školy	škola	k1gFnSc2	škola
v	v	k7c6	v
Brightonu	Brighton	k1gInSc6	Brighton
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
nacházela	nacházet	k5eAaImAgFnS	nacházet
u	u	k7c2	u
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
a	a	k8xC	a
umožnila	umožnit	k5eAaPmAgFnS	umožnit
Winstonovi	Winston	k1gMnSc3	Winston
zotavit	zotavit	k5eAaPmF	zotavit
se	se	k3xPyFc4	se
z	z	k7c2	z
prodělané	prodělaný	k2eAgFnSc2d1	prodělaná
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
studia	studio	k1gNnSc2	studio
v	v	k7c6	v
Brightonu	Brighton	k1gInSc6	Brighton
složil	složit	k5eAaPmAgMnS	složit
přijímací	přijímací	k2eAgFnPc4d1	přijímací
zkoušky	zkouška	k1gFnPc4	zkouška
na	na	k7c4	na
školu	škola	k1gFnSc4	škola
v	v	k7c6	v
Harrow	Harrow	k1gFnSc6	Harrow
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
ještě	ještě	k9	ještě
mimo	mimo	k7c4	mimo
Londýn	Londýn	k1gInSc4	Londýn
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
snem	sen	k1gInSc7	sen
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
dostat	dostat	k5eAaPmF	dostat
se	se	k3xPyFc4	se
na	na	k7c4	na
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
akademii	akademie	k1gFnSc4	akademie
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1893	[number]	k4	1893
a	a	k8xC	a
na	na	k7c4	na
třetí	třetí	k4xOgInSc4	třetí
pokus	pokus	k1gInSc4	pokus
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
vojenským	vojenský	k2eAgMnSc7d1	vojenský
kadetem	kadet	k1gMnSc7	kadet
na	na	k7c6	na
Královské	královský	k2eAgFnSc6d1	královská
vojenské	vojenský	k2eAgFnSc6d1	vojenská
akademii	akademie	k1gFnSc6	akademie
v	v	k7c6	v
Sandhurstu	Sandhurst	k1gInSc6	Sandhurst
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
si	se	k3xPyFc3	se
vedl	vést	k5eAaImAgMnS	vést
úspěšně	úspěšně	k6eAd1	úspěšně
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dvou	dva	k4xCgInPc6	dva
letech	let	k1gInPc6	let
skončil	skončit	k5eAaPmAgMnS	skončit
osmý	osmý	k4xOgMnSc1	osmý
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
mezi	mezi	k7c7	mezi
zhruba	zhruba	k6eAd1	zhruba
sto	sto	k4xCgNnSc4	sto
chlapci	chlapec	k1gMnPc1	chlapec
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
ročníku	ročník	k1gInSc6	ročník
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
mladý	mladý	k2eAgMnSc1d1	mladý
podporučík	podporučík	k1gMnSc1	podporučík
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1895	[number]	k4	1895
odcestoval	odcestovat	k5eAaPmAgMnS	odcestovat
na	na	k7c4	na
Kubu	Kuba	k1gFnSc4	Kuba
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
působil	působit	k5eAaImAgMnS	působit
jako	jako	k8xS	jako
válečný	válečný	k2eAgMnSc1d1	válečný
dopisovatel	dopisovatel	k1gMnSc1	dopisovatel
a	a	k8xC	a
vojenský	vojenský	k2eAgMnSc1d1	vojenský
pozorovatel	pozorovatel	k1gMnSc1	pozorovatel
u	u	k7c2	u
štábu	štáb	k1gInSc2	štáb
generála	generál	k1gMnSc2	generál
Valdeze	Valdeze	k1gFnSc2	Valdeze
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
udržení	udržení	k1gNnSc4	udržení
dobrých	dobrý	k2eAgInPc2d1	dobrý
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
vztahů	vztah	k1gInPc2	vztah
však	však	k9	však
jeho	jeho	k3xOp3gInPc1	jeho
články	článek	k1gInPc1	článek
často	často	k6eAd1	často
nebyly	být	k5eNaImAgInP	být
zveřejňovány	zveřejňován	k2eAgInPc1d1	zveřejňován
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
velice	velice	k6eAd1	velice
sympatizoval	sympatizovat	k5eAaImAgInS	sympatizovat
s	s	k7c7	s
povstalci	povstalec	k1gMnPc7	povstalec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
bojovali	bojovat	k5eAaImAgMnP	bojovat
proti	proti	k7c3	proti
španělské	španělský	k2eAgFnSc3d1	španělská
nadvládě	nadvláda	k1gFnSc3	nadvláda
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
svého	svůj	k3xOyFgNnSc2	svůj
dalšího	další	k2eAgNnSc2d1	další
působení	působení	k1gNnSc2	působení
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
nejdříve	dříve	k6eAd3	dříve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1897	[number]	k4	1897
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
generála	generál	k1gMnSc2	generál
Bindona	Bindon	k1gMnSc2	Bindon
Blooda	Blooda	k1gMnSc1	Blooda
bojů	boj	k1gInPc2	boj
v	v	k7c6	v
severozápadním	severozápadní	k2eAgNnSc6d1	severozápadní
pohraničí	pohraničí	k1gNnSc6	pohraničí
Britské	britský	k2eAgFnSc2d1	britská
Indie	Indie	k1gFnSc2	Indie
proti	proti	k7c3	proti
paštunským	paštunský	k2eAgInPc3d1	paštunský
kmenům	kmen	k1gInPc3	kmen
Áfaridů	Áfarid	k1gInPc2	Áfarid
a	a	k8xC	a
Orakzáíů	Orakzáí	k1gInPc2	Orakzáí
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
několika	několik	k4yIc2	několik
bitev	bitva	k1gFnPc2	bitva
vyšel	vyjít	k5eAaPmAgInS	vyjít
nezraněn	zraněn	k2eNgInSc1d1	nezraněn
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
podle	podle	k7c2	podle
svých	svůj	k3xOyFgMnPc2	svůj
nadřízených	nadřízený	k1gMnPc2	nadřízený
prováděl	provádět	k5eAaImAgInS	provádět
šílené	šílený	k2eAgFnSc3d1	šílená
věci	věc	k1gFnSc3	věc
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
jízdu	jízda	k1gFnSc4	jízda
velice	velice	k6eAd1	velice
blízko	blízko	k7c2	blízko
rojnice	rojnice	k1gFnSc2	rojnice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1898	[number]	k4	1898
se	se	k3xPyFc4	se
jako	jako	k9	jako
člen	člen	k1gMnSc1	člen
Kitchenerovy	Kitchenerův	k2eAgFnSc2d1	Kitchenerův
expedice	expedice	k1gFnSc2	expedice
účastnil	účastnit	k5eAaImAgInS	účastnit
bojů	boj	k1gInPc2	boj
v	v	k7c6	v
Súdánu	Súdán	k1gInSc6	Súdán
u	u	k7c2	u
Omdurmánu	Omdurmán	k2eAgFnSc4d1	Omdurmán
(	(	kIx(	(
<g/>
Ummdurmánu	Ummdurmán	k2eAgFnSc4d1	Ummdurmán
<g/>
)	)	kIx)	)
proti	proti	k7c3	proti
Mahdího	Mahdí	k2eAgNnSc2d1	Mahdí
povstání	povstání	k1gNnSc2	povstání
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
zde	zde	k6eAd1	zde
bojoval	bojovat	k5eAaImAgInS	bojovat
na	na	k7c6	na
exponovaných	exponovaný	k2eAgInPc6d1	exponovaný
úsecích	úsek	k1gInPc6	úsek
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
padlo	padnout	k5eAaPmAgNnS	padnout
několik	několik	k4yIc1	několik
jeho	jeho	k3xOp3gMnPc2	jeho
blízkých	blízký	k2eAgMnPc2d1	blízký
přátel	přítel	k1gMnPc2	přítel
<g/>
,	,	kIx,	,
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgInSc4	sám
však	však	k9	však
z	z	k7c2	z
boje	boj	k1gInSc2	boj
s	s	k7c7	s
derviši	derviš	k1gMnPc7	derviš
vyšel	vyjít	k5eAaPmAgMnS	vyjít
nezraněn	zraněn	k2eNgMnSc1d1	nezraněn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
válečných	válečný	k2eAgFnPc6d1	válečná
událostech	událost	k1gFnPc6	událost
si	se	k3xPyFc3	se
přivydělával	přivydělávat	k5eAaImAgMnS	přivydělávat
jako	jako	k9	jako
válečný	válečný	k2eAgMnSc1d1	válečný
dopisovatel	dopisovatel	k1gMnSc1	dopisovatel
a	a	k8xC	a
právě	právě	k6eAd1	právě
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
u	u	k7c2	u
něho	on	k3xPp3gMnSc2	on
poprvé	poprvé	k6eAd1	poprvé
objevil	objevit	k5eAaPmAgMnS	objevit
určitý	určitý	k2eAgInSc4d1	určitý
literární	literární	k2eAgInSc4d1	literární
talent	talent	k1gInSc4	talent
<g/>
.	.	kIx.	.
</s>
<s>
Povedlo	povést	k5eAaPmAgNnS	povést
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
totiž	totiž	k9	totiž
napsat	napsat	k5eAaBmF	napsat
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
knihu	kniha	k1gFnSc4	kniha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Říční	říční	k2eAgFnSc1d1	říční
válka	válka	k1gFnSc1	válka
pojednávající	pojednávající	k2eAgFnSc1d1	pojednávající
o	o	k7c6	o
bojích	boj	k1gInPc6	boj
v	v	k7c6	v
Súdánu	Súdán	k1gInSc6	Súdán
se	se	k3xPyFc4	se
vyjadřoval	vyjadřovat	k5eAaImAgMnS	vyjadřovat
velmi	velmi	k6eAd1	velmi
negativně	negativně	k6eAd1	negativně
o	o	k7c6	o
islámu	islám	k1gInSc6	islám
a	a	k8xC	a
o	o	k7c6	o
muslimech	muslim	k1gMnPc6	muslim
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
syna	syn	k1gMnSc4	syn
prominentního	prominentní	k2eAgMnSc4d1	prominentní
politika	politik	k1gMnSc4	politik
nebylo	být	k5eNaImAgNnS	být
překvapivé	překvapivý	k2eAgNnSc1d1	překvapivé
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
prosadit	prosadit	k5eAaPmF	prosadit
v	v	k7c6	v
politice	politika	k1gFnSc6	politika
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
přednesl	přednést	k5eAaPmAgInS	přednést
několik	několik	k4yIc4	několik
veřejných	veřejný	k2eAgInPc2d1	veřejný
projevů	projev	k1gInPc2	projev
na	na	k7c6	na
schůzích	schůze	k1gFnPc6	schůze
konzervativců	konzervativec	k1gMnPc2	konzervativec
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1899	[number]	k4	1899
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
poprvé	poprvé	k6eAd1	poprvé
vstoupit	vstoupit	k5eAaPmF	vstoupit
do	do	k7c2	do
politiky	politika	k1gFnSc2	politika
<g/>
,	,	kIx,	,
když	když	k8xS	když
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
do	do	k7c2	do
Dolní	dolní	k2eAgFnSc2d1	dolní
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Oldham	Oldham	k1gInSc4	Oldham
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
za	za	k7c4	za
konzervativce	konzervativec	k1gMnSc4	konzervativec
a	a	k8xC	a
prohrál	prohrát	k5eAaPmAgMnS	prohrát
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
věku	věk	k1gInSc6	věk
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
nedělalo	dělat	k5eNaImAgNnS	dělat
problémy	problém	k1gInPc4	problém
řečnit	řečnit	k5eAaImF	řečnit
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc1	jeho
projevy	projev	k1gInPc1	projev
byly	být	k5eAaImAgInP	být
uznávané	uznávaný	k2eAgInPc4d1	uznávaný
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
nebyl	být	k5eNaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1899	[number]	k4	1899
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Africe	Afrika	k1gFnSc6	Afrika
búrská	búrský	k2eAgFnSc1d1	búrská
válka	válka	k1gFnSc1	válka
a	a	k8xC	a
Winston	Winston	k1gInSc1	Winston
Churchill	Churchilla	k1gFnPc2	Churchilla
zde	zde	k6eAd1	zde
začal	začít	k5eAaPmAgInS	začít
působit	působit	k5eAaImF	působit
jako	jako	k9	jako
dopisovatel	dopisovatel	k1gMnSc1	dopisovatel
listu	list	k1gInSc2	list
Morning	Morning	k1gInSc1	Morning
Post	post	k1gInSc1	post
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
búrského	búrský	k2eAgInSc2d1	búrský
přepadu	přepad	k1gInSc2	přepad
britského	britský	k2eAgInSc2d1	britský
obrněného	obrněný	k2eAgInSc2d1	obrněný
vlaku	vlak	k1gInSc2	vlak
byl	být	k5eAaImAgInS	být
zajat	zajat	k2eAgInSc1d1	zajat
a	a	k8xC	a
uvězněn	uvězněn	k2eAgInSc1d1	uvězněn
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
vězení	vězení	k1gNnSc2	vězení
v	v	k7c6	v
Pretorii	Pretorie	k1gFnSc6	Pretorie
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
uprchnout	uprchnout	k5eAaPmF	uprchnout
a	a	k8xC	a
tajně	tajně	k6eAd1	tajně
přejet	přejet	k5eAaPmF	přejet
vlakem	vlak	k1gInSc7	vlak
500	[number]	k4	500
km	km	kA	km
až	až	k6eAd1	až
do	do	k7c2	do
portugalského	portugalský	k2eAgInSc2d1	portugalský
Mosambiku	Mosambik	k1gInSc2	Mosambik
a	a	k8xC	a
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
pak	pak	k6eAd1	pak
na	na	k7c4	na
území	území	k1gNnSc4	území
kontrolované	kontrolovaný	k2eAgMnPc4d1	kontrolovaný
Brity	Brit	k1gMnPc4	Brit
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
dobrodružství	dobrodružství	k1gNnSc1	dobrodružství
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
udělalo	udělat	k5eAaPmAgNnS	udělat
na	na	k7c4	na
nějaký	nějaký	k3yIgInSc4	nějaký
čas	čas	k1gInSc4	čas
národního	národní	k2eAgNnSc2d1	národní
hrdinu	hrdina	k1gMnSc4	hrdina
<g/>
,	,	kIx,	,
<g/>
dokonce	dokonce	k9	dokonce
byl	být	k5eAaImAgInS	být
navržen	navrhnout	k5eAaPmNgInS	navrhnout
na	na	k7c6	na
vyznamenání	vyznamenání	k1gNnSc6	vyznamenání
Viktoriiným	Viktoriin	k2eAgInSc7d1	Viktoriin
křížem	kříž	k1gInSc7	kříž
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Horatio	Horatio	k1gMnSc1	Horatio
Kitchener	Kitchener	k1gMnSc1	Kitchener
tento	tento	k3xDgInSc4	tento
návrh	návrh	k1gInSc4	návrh
vetoval	vetovat	k5eAaBmAgMnS	vetovat
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
výhodou	výhoda	k1gFnSc7	výhoda
byla	být	k5eAaImAgFnS	být
reaktivace	reaktivace	k1gFnSc1	reaktivace
jako	jako	k8xC	jako
vojáka	voják	k1gMnSc4	voják
a	a	k8xC	a
nastoupení	nastoupení	k1gNnSc4	nastoupení
do	do	k7c2	do
činné	činný	k2eAgFnSc2d1	činná
služby	služba	k1gFnSc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
Británie	Británie	k1gFnSc1	Británie
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
jistých	jistý	k2eAgMnPc2d1	jistý
úspěchů	úspěch	k1gInPc2	úspěch
<g/>
,	,	kIx,	,
konaly	konat	k5eAaImAgInP	konat
se	se	k3xPyFc4	se
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
nové	nový	k2eAgFnSc2d1	nová
volby	volba	k1gFnSc2	volba
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgNnPc6	jenž
Churchill	Churchill	k1gMnSc1	Churchill
znovu	znovu	k6eAd1	znovu
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
a	a	k8xC	a
opět	opět	k6eAd1	opět
ve	v	k7c6	v
volebním	volební	k2eAgInSc6d1	volební
obvodu	obvod	k1gInSc6	obvod
Oldham	Oldham	k1gInSc1	Oldham
<g/>
.	.	kIx.	.
</s>
<s>
Neváhal	váhat	k5eNaImAgMnS	váhat
využít	využít	k5eAaPmF	využít
své	své	k1gNnSc4	své
popularity	popularita	k1gFnSc2	popularita
válečného	válečný	k2eAgMnSc2d1	válečný
hrdiny	hrdina	k1gMnSc2	hrdina
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
zasedl	zasednout	k5eAaPmAgMnS	zasednout
v	v	k7c6	v
Dolní	dolní	k2eAgFnSc6d1	dolní
sněmovně	sněmovna	k1gFnSc6	sněmovna
do	do	k7c2	do
lavice	lavice	k1gFnSc2	lavice
za	za	k7c4	za
konzervativce	konzervativec	k1gMnPc4	konzervativec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
přednesl	přednést	k5eAaPmAgMnS	přednést
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
projev	projev	k1gInSc4	projev
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgNnSc7	tento
začala	začít	k5eAaPmAgFnS	začít
jeho	jeho	k3xOp3gFnSc1	jeho
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
politická	politický	k2eAgFnSc1d1	politická
kariéra	kariéra	k1gFnSc1	kariéra
<g/>
,	,	kIx,	,
během	během	k7c2	během
níž	jenž	k3xRgFnSc2	jenž
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1900	[number]	k4	1900
<g/>
-	-	kIx~	-
<g/>
1922	[number]	k4	1922
a	a	k8xC	a
1924	[number]	k4	1924
<g/>
-	-	kIx~	-
<g/>
1964	[number]	k4	1964
členem	člen	k1gInSc7	člen
Dolní	dolní	k2eAgFnPc1d1	dolní
sněmovny	sněmovna	k1gFnPc1	sněmovna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1904	[number]	k4	1904
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
z	z	k7c2	z
konzervativní	konzervativní	k2eAgFnSc2d1	konzervativní
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
přešel	přejít	k5eAaPmAgMnS	přejít
k	k	k7c3	k
liberálům	liberál	k1gMnPc3	liberál
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nesouhlasil	souhlasit	k5eNaImAgMnS	souhlasit
s	s	k7c7	s
konzervativní	konzervativní	k2eAgFnSc7d1	konzervativní
politikou	politika	k1gFnSc7	politika
týkající	týkající	k2eAgNnSc4d1	týkající
se	se	k3xPyFc4	se
ochranářských	ochranářský	k2eAgInPc2d1	ochranářský
tarifů	tarif	k1gInPc2	tarif
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
liberálové	liberál	k1gMnPc1	liberál
všeobecné	všeobecný	k2eAgFnSc2d1	všeobecná
volby	volba	k1gFnSc2	volba
a	a	k8xC	a
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
Churchill	Churchill	k1gMnSc1	Churchill
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
zástupce	zástupce	k1gMnSc4	zástupce
ministra	ministr	k1gMnSc4	ministr
kolonií	kolonie	k1gFnPc2	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
také	také	k9	také
Churchill	Churchill	k1gMnSc1	Churchill
sblížil	sblížit	k5eAaPmAgMnS	sblížit
s	s	k7c7	s
advokátem	advokát	k1gMnSc7	advokát
z	z	k7c2	z
Walesu	Wales	k1gInSc2	Wales
Davidem	David	k1gMnSc7	David
Lloydem	Lloyd	k1gMnSc7	Lloyd
Georgem	Georg	k1gMnSc7	Georg
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgMnS	mít
v	v	k7c6	v
budoucnosti	budoucnost	k1gFnSc6	budoucnost
stát	stát	k5eAaImF	stát
jeho	jeho	k3xOp3gMnSc7	jeho
politickým	politický	k2eAgMnSc7d1	politický
partnerem	partner	k1gMnSc7	partner
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
ministrem	ministr	k1gMnSc7	ministr
obchodu	obchod	k1gInSc2	obchod
<g/>
,	,	kIx,	,
v	v	k7c6	v
kteréžto	kteréžto	k?	kteréžto
pozici	pozice	k1gFnSc6	pozice
úzce	úzko	k6eAd1	úzko
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
Lloyd	Lloyd	k1gMnSc1	Lloyd
Georgem	Georg	k1gMnSc7	Georg
<g/>
,	,	kIx,	,
novým	nový	k2eAgMnSc7d1	nový
státním	státní	k2eAgMnSc7d1	státní
pokladníkem	pokladník	k1gMnSc7	pokladník
(	(	kIx(	(
<g/>
Chancellor	Chancellor	k1gMnSc1	Chancellor
of	of	k?	of
the	the	k?	the
Exchequer	Exchequer	k1gInSc1	Exchequer
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
ministrem	ministr	k1gMnSc7	ministr
vnitra	vnitro	k1gNnSc2	vnitro
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jeho	jeho	k3xOp3gMnSc3	jeho
působení	působení	k1gNnSc1	působení
bylo	být	k5eAaImAgNnS	být
hodnoceno	hodnotit	k5eAaImNgNnS	hodnotit
jako	jako	k8xS	jako
poněkud	poněkud	k6eAd1	poněkud
kontroverzní	kontroverzní	k2eAgMnSc1d1	kontroverzní
<g/>
.	.	kIx.	.
</s>
<s>
Slavná	slavný	k2eAgFnSc1d1	slavná
fotografie	fotografie	k1gFnSc1	fotografie
z	z	k7c2	z
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
Churchilla	Churchilla	k1gFnSc1	Churchilla
osobně	osobně	k6eAd1	osobně
se	se	k3xPyFc4	se
účastnícího	účastnící	k2eAgInSc2d1	účastnící
tzv.	tzv.	kA	tzv.
obléhání	obléhání	k1gNnSc4	obléhání
Sidney	Sidnea	k1gFnSc2	Sidnea
Street	Streeta	k1gFnPc2	Streeta
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
bylo	být	k5eAaImAgNnS	být
kritizováno	kritizovat	k5eAaImNgNnS	kritizovat
jeho	jeho	k3xOp3gNnSc1	jeho
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
nevolat	volat	k5eNaImF	volat
hasiče	hasič	k1gMnPc4	hasič
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
když	když	k8xS	když
obléhaná	obléhaný	k2eAgFnSc1d1	obléhaná
budova	budova	k1gFnSc1	budova
začala	začít	k5eAaPmAgFnS	začít
hořet	hořet	k5eAaImF	hořet
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c4	před
anarchisty	anarchista	k1gMnPc4	anarchista
tak	tak	k6eAd1	tak
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
možnost	možnost	k1gFnSc1	možnost
buď	buď	k8xC	buď
se	se	k3xPyFc4	se
vzdát	vzdát	k5eAaPmF	vzdát
nebo	nebo	k8xC	nebo
zemřít	zemřít	k5eAaPmF	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1911	[number]	k4	1911
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc7	první
lordem	lord	k1gMnSc7	lord
admirality	admiralita	k1gFnSc2	admiralita
-	-	kIx~	-
ministrem	ministr	k1gMnSc7	ministr
námořnictví	námořnictví	k1gNnSc2	námořnictví
<g/>
.	.	kIx.	.
</s>
<s>
Podílel	podílet	k5eAaImAgInS	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
snahách	snaha	k1gFnPc6	snaha
zreformovat	zreformovat	k5eAaPmF	zreformovat
britské	britský	k2eAgNnSc4d1	Britské
loďstvo	loďstvo	k1gNnSc4	loďstvo
<g/>
.	.	kIx.	.
</s>
<s>
Zřídil	zřídit	k5eAaPmAgInS	zřídit
štáb	štáb	k1gInSc1	štáb
admirality	admiralita	k1gFnSc2	admiralita
<g/>
,	,	kIx,	,
prosadil	prosadit	k5eAaPmAgMnS	prosadit
změnu	změna	k1gFnSc4	změna
pohonu	pohon	k1gInSc2	pohon
lodí	loď	k1gFnPc2	loď
z	z	k7c2	z
uhlí	uhlí	k1gNnSc2	uhlí
na	na	k7c4	na
naftu	nafta	k1gFnSc4	nafta
atd.	atd.	kA	atd.
Churchill	Churchill	k1gMnSc1	Churchill
tušil	tušit	k5eAaImAgMnS	tušit
možnost	možnost	k1gFnSc4	možnost
konfliktu	konflikt	k1gInSc2	konflikt
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
již	již	k6eAd1	již
od	od	k7c2	od
agadirské	agadirský	k2eAgFnSc2d1	agadirský
krize	krize	k1gFnSc2	krize
a	a	k8xC	a
tak	tak	k6eAd1	tak
předložil	předložit	k5eAaPmAgMnS	předložit
vládě	vláda	k1gFnSc6	vláda
memorandum	memorandum	k1gNnSc4	memorandum
"	"	kIx"	"
<g/>
Problémy	problém	k1gInPc1	problém
kontinentálního	kontinentální	k2eAgNnSc2d1	kontinentální
vedení	vedení	k1gNnSc2	vedení
války	válka	k1gFnSc2	válka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgMnS	účastnit
vojenských	vojenský	k2eAgFnPc2d1	vojenská
akcí	akce	k1gFnPc2	akce
v	v	k7c6	v
Antverpách	Antverpy	k1gFnPc6	Antverpy
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
první	první	k4xOgFnSc7	první
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
důrazně	důrazně	k6eAd1	důrazně
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Británie	Británie	k1gFnSc1	Británie
měla	mít	k5eAaImAgFnS	mít
převahu	převaha	k1gFnSc4	převaha
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Patřil	patřit	k5eAaImAgMnS	patřit
k	k	k7c3	k
hlavním	hlavní	k2eAgMnPc3d1	hlavní
tvůrcům	tvůrce	k1gMnPc3	tvůrce
koncepce	koncepce	k1gFnSc2	koncepce
vylodění	vyloděný	k2eAgMnPc1d1	vyloděný
v	v	k7c6	v
Gallipoli	Gallipoli	k1gNnSc6	Gallipoli
na	na	k7c6	na
Dardanelách	Dardanely	k1gFnPc6	Dardanely
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
nakonec	nakonec	k6eAd1	nakonec
znamenala	znamenat	k5eAaImAgNnP	znamenat
v	v	k7c6	v
období	období	k1gNnSc6	období
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
jeho	on	k3xPp3gInSc4	on
pád	pád	k1gInSc4	pád
<g/>
.	.	kIx.	.
</s>
<s>
Turecko	Turecko	k1gNnSc1	Turecko
<g/>
,	,	kIx,	,
spojenec	spojenec	k1gMnSc1	spojenec
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
Rakousko-Uherska	Rakousko-Uhersk	k1gInSc2	Rakousko-Uhersk
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
relativně	relativně	k6eAd1	relativně
slabé	slabý	k2eAgNnSc1d1	slabé
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Cařihrad	Cařihrad	k1gInSc1	Cařihrad
(	(	kIx(	(
<g/>
Konstantinopol	Konstantinopol	k1gInSc1	Konstantinopol
<g/>
)	)	kIx)	)
leželo	ležet	k5eAaImAgNnS	ležet
u	u	k7c2	u
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
odkryté	odkrytý	k2eAgFnPc1d1	odkrytá
pro	pro	k7c4	pro
zásah	zásah	k1gInSc4	zásah
námořní	námořní	k2eAgFnSc2d1	námořní
velmoci	velmoc	k1gFnSc2	velmoc
mající	mající	k2eAgFnSc4d1	mající
převahu	převaha	k1gFnSc4	převaha
<g/>
.	.	kIx.	.
</s>
<s>
Kdyby	kdyby	kYmCp3nS	kdyby
padlo	padnout	k5eAaPmAgNnS	padnout
<g/>
,	,	kIx,	,
dalo	dát	k5eAaPmAgNnS	dát
se	se	k3xPyFc4	se
počítat	počítat	k5eAaImF	počítat
s	s	k7c7	s
kapitulací	kapitulace	k1gFnSc7	kapitulace
celého	celý	k2eAgNnSc2d1	celé
Turecka	Turecko	k1gNnSc2	Turecko
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
by	by	kYmCp3nS	by
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
zřízení	zřízení	k1gNnSc3	zřízení
bezpečného	bezpečný	k2eAgNnSc2d1	bezpečné
spojení	spojení	k1gNnSc2	spojení
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
daly	dát	k5eAaPmAgInP	dát
posílat	posílat	k5eAaImF	posílat
transporty	transport	k1gInPc1	transport
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Srbsko	Srbsko	k1gNnSc1	Srbsko
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
ještě	ještě	k6eAd1	ještě
drželo	držet	k5eAaImAgNnS	držet
<g/>
,	,	kIx,	,
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
nebylo	být	k5eNaImAgNnS	být
spojencem	spojenec	k1gMnSc7	spojenec
Trojspolku	trojspolek	k1gInSc2	trojspolek
a	a	k8xC	a
pád	pád	k1gInSc1	pád
Cařihradu	Cařihrad	k1gInSc2	Cařihrad
by	by	kYmCp3nS	by
jim	on	k3xPp3gFnPc3	on
dal	dát	k5eAaPmAgMnS	dát
potřebný	potřebný	k2eAgInSc4d1	potřebný
impuls	impuls	k1gInSc4	impuls
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
něco	něco	k3yInSc1	něco
takového	takový	k3xDgMnSc4	takový
vyžadovalo	vyžadovat	k5eAaImAgNnS	vyžadovat
koordinaci	koordinace	k1gFnSc4	koordinace
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
a	a	k8xC	a
pozemních	pozemní	k2eAgFnPc2d1	pozemní
sil	síla	k1gFnPc2	síla
-	-	kIx~	-
komplexní	komplexní	k2eAgNnSc1d1	komplexní
vedení	vedení	k1gNnSc1	vedení
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
ministr	ministr	k1gMnSc1	ministr
války	válka	k1gFnSc2	válka
Kitchener	Kitchener	k1gMnSc1	Kitchener
(	(	kIx(	(
<g/>
vedl	vést	k5eAaImAgMnS	vést
už	už	k6eAd1	už
zmiňovanou	zmiňovaný	k2eAgFnSc4d1	zmiňovaná
expedici	expedice	k1gFnSc4	expedice
do	do	k7c2	do
Súdánu	Súdán	k1gInSc2	Súdán
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
Churchill	Churchill	k1gMnSc1	Churchill
též	též	k9	též
účastnil	účastnit	k5eAaImAgMnS	účastnit
<g/>
)	)	kIx)	)
však	však	k9	však
plánu	plán	k1gInSc2	plán
příliš	příliš	k6eAd1	příliš
nedůvěřoval	důvěřovat	k5eNaImAgMnS	důvěřovat
<g/>
.	.	kIx.	.
18	[number]	k4	18
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1915	[number]	k4	1915
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
loďstvu	loďstvo	k1gNnSc3	loďstvo
ochromit	ochromit	k5eAaPmF	ochromit
pevnost	pevnost	k1gFnSc4	pevnost
v	v	k7c6	v
Dardanelách	Dardanely	k1gFnPc6	Dardanely
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
nutné	nutný	k2eAgNnSc1d1	nutné
okamžitě	okamžitě	k6eAd1	okamžitě
začít	začít	k5eAaPmF	začít
s	s	k7c7	s
invazí	invaze	k1gFnSc7	invaze
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přišel	přijít	k5eAaPmAgInS	přijít
rozkaz	rozkaz	k1gInSc1	rozkaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
čekat	čekat	k5eAaImF	čekat
na	na	k7c4	na
armádu	armáda	k1gFnSc4	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
sice	sice	k8xC	sice
byla	být	k5eAaImAgFnS	být
25	[number]	k4	25
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
vysazena	vysadit	k5eAaPmNgFnS	vysadit
a	a	k8xC	a
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
předmostí	předmostí	k1gNnSc4	předmostí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dál	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
již	již	k6eAd1	již
nedostala	dostat	k5eNaPmAgFnS	dostat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Turci	Turek	k1gMnPc1	Turek
se	se	k3xPyFc4	se
zatím	zatím	k6eAd1	zatím
zkonsolidovali	zkonsolidovat	k5eAaPmAgMnP	zkonsolidovat
<g/>
.	.	kIx.	.
</s>
<s>
Dorazily	dorazit	k5eAaPmAgInP	dorazit
jejich	jejich	k3xOp3gFnPc4	jejich
posily	posila	k1gFnPc4	posila
a	a	k8xC	a
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
května	květno	k1gNnSc2	květno
bylo	být	k5eAaImAgNnS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
Churchillův	Churchillův	k2eAgInSc4d1	Churchillův
plán	plán	k1gInSc4	plán
padl	padnout	k5eAaPmAgMnS	padnout
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
též	též	k9	též
bitva	bitva	k1gFnSc1	bitva
o	o	k7c6	o
Gallipoli	Gallipoli	k1gNnSc6	Gallipoli
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zbývalo	zbývat	k5eAaImAgNnS	zbývat
pouze	pouze	k6eAd1	pouze
vést	vést	k5eAaImF	vést
zákopovou	zákopový	k2eAgFnSc4d1	zákopová
válku	válka	k1gFnSc4	válka
v	v	k7c6	v
dalekém	daleký	k2eAgNnSc6d1	daleké
Turecku	Turecko	k1gNnSc6	Turecko
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
stáhnout	stáhnout	k5eAaPmF	stáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Armáda	armáda	k1gFnSc1	armáda
i	i	k8xC	i
loďstvo	loďstvo	k1gNnSc1	loďstvo
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
stáhly	stáhnout	k5eAaPmAgFnP	stáhnout
<g/>
,	,	kIx,	,
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
Churchillův	Churchillův	k2eAgMnSc1d1	Churchillův
vrchní	vrchní	k2eAgMnSc1d1	vrchní
admirál	admirál	k1gMnSc1	admirál
Fischer	Fischer	k1gMnSc1	Fischer
a	a	k8xC	a
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
se	se	k3xPyFc4	se
schylovalo	schylovat	k5eAaImAgNnS	schylovat
k	k	k7c3	k
vládní	vládní	k2eAgFnSc3d1	vládní
krizi	krize	k1gFnSc3	krize
<g/>
.	.	kIx.	.
</s>
<s>
Vše	všechen	k3xTgNnSc1	všechen
se	se	k3xPyFc4	se
však	však	k9	však
stalo	stát	k5eAaPmAgNnS	stát
kvůli	kvůli	k7c3	kvůli
nerozhodnosti	nerozhodnost	k1gFnSc3	nerozhodnost
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nedala	dát	k5eNaPmAgFnS	dát
Churchillovi	Churchill	k1gMnSc3	Churchill
volnou	volnou	k6eAd1	volnou
ruku	ruka	k1gFnSc4	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Vládní	vládní	k2eAgFnSc1d1	vládní
krize	krize	k1gFnSc1	krize
vyvrcholila	vyvrcholit	k5eAaPmAgFnS	vyvrcholit
<g/>
.	.	kIx.	.
</s>
<s>
Konzervativci	konzervativec	k1gMnPc1	konzervativec
dali	dát	k5eAaPmAgMnP	dát
vládě	vláda	k1gFnSc3	vláda
premiéra	premiéra	k1gFnSc1	premiéra
Asquitha	Asquitha	k1gFnSc1	Asquitha
ultimátum	ultimátum	k1gNnSc4	ultimátum
-	-	kIx~	-
buď	buď	k8xC	buď
koalice	koalice	k1gFnSc1	koalice
nebo	nebo	k8xC	nebo
hlasování	hlasování	k1gNnSc1	hlasování
o	o	k7c6	o
důvěře	důvěra	k1gFnSc6	důvěra
<g/>
.	.	kIx.	.
</s>
<s>
Premiér	premiér	k1gMnSc1	premiér
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
s	s	k7c7	s
koalicí	koalice	k1gFnSc7	koalice
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamenalo	znamenat	k5eAaImAgNnS	znamenat
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
odstoupení	odstoupení	k1gNnSc4	odstoupení
Churchilla	Churchill	k1gMnSc2	Churchill
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
pro	pro	k7c4	pro
konzervativce	konzervativec	k1gMnSc4	konzervativec
neúnosný	únosný	k2eNgInSc1d1	neúnosný
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
jako	jako	k8xS	jako
zrádce	zrádce	k1gMnSc1	zrádce
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
jednak	jednak	k8xC	jednak
kvůli	kvůli	k7c3	kvůli
nevydařené	vydařený	k2eNgFnSc3d1	nevydařená
operaci	operace	k1gFnSc3	operace
v	v	k7c6	v
Dardanelách	Dardanely	k1gFnPc6	Dardanely
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
odvolán	odvolat	k5eAaPmNgInS	odvolat
18	[number]	k4	18
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1915	[number]	k4	1915
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1915	[number]	k4	1915
odešel	odejít	k5eAaPmAgMnS	odejít
také	také	k9	také
z	z	k7c2	z
funkce	funkce	k1gFnSc2	funkce
kancléře	kancléř	k1gMnSc2	kancléř
lancasterského	lancasterský	k2eAgNnSc2d1	lancasterský
vévodství	vévodství	k1gNnSc2	vévodství
a	a	k8xC	a
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
jako	jako	k9	jako
reaktivovaný	reaktivovaný	k2eAgMnSc1d1	reaktivovaný
důstojník	důstojník	k1gMnSc1	důstojník
v	v	k7c6	v
hodnosti	hodnost	k1gFnSc6	hodnost
major	major	k1gMnSc1	major
do	do	k7c2	do
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Odcestoval	odcestovat	k5eAaPmAgMnS	odcestovat
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
později	pozdě	k6eAd2	pozdě
povýšen	povýšit	k5eAaPmNgMnS	povýšit
do	do	k7c2	do
hodnosti	hodnost	k1gFnSc2	hodnost
podplukovníka	podplukovník	k1gMnSc2	podplukovník
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
roku	rok	k1gInSc2	rok
1916	[number]	k4	1916
se	se	k3xPyFc4	se
ale	ale	k9	ale
Churchill	Churchill	k1gMnSc1	Churchill
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Dolní	dolní	k2eAgFnSc2d1	dolní
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
,	,	kIx,	,
když	když	k8xS	když
padla	padnout	k5eAaPmAgFnS	padnout
vláda	vláda	k1gFnSc1	vláda
premiéra	premiér	k1gMnSc2	premiér
Asquitha	Asquith	k1gMnSc2	Asquith
a	a	k8xC	a
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
David	David	k1gMnSc1	David
Lloyd	Lloyd	k1gMnSc1	Lloyd
George	Georg	k1gMnSc4	Georg
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
ani	ani	k8xC	ani
ten	ten	k3xDgInSc4	ten
nemohl	moct	k5eNaImAgMnS	moct
Churchilla	Churchilla	k1gMnSc1	Churchilla
do	do	k7c2	do
vlády	vláda	k1gFnSc2	vláda
prosadit	prosadit	k5eAaPmF	prosadit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
konzervativci	konzervativec	k1gMnPc1	konzervativec
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Bonarem	Bonar	k1gMnSc7	Bonar
Lawem	Law	k1gMnSc7	Law
se	se	k3xPyFc4	se
tomu	ten	k3xDgNnSc3	ten
bránili	bránit	k5eAaImAgMnP	bránit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
vstoupily	vstoupit	k5eAaPmAgInP	vstoupit
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
do	do	k7c2	do
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
zuřila	zuřit	k5eAaImAgFnS	zuřit
nejtěžší	těžký	k2eAgFnSc1d3	nejtěžší
ponorková	ponorkový	k2eAgFnSc1d1	ponorková
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
probíhaly	probíhat	k5eAaImAgInP	probíhat
boje	boj	k1gInPc1	boj
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ničeho	nic	k3yNnSc2	nic
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
Churchill	Churchill	k1gInSc1	Churchill
neúčastnil	účastnit	k5eNaImAgInS	účastnit
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
16	[number]	k4	16
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1917	[number]	k4	1917
nový	nový	k2eAgMnSc1d1	nový
premiér	premiér	k1gMnSc1	premiér
povolal	povolat	k5eAaPmAgMnS	povolat
Churchilla	Churchill	k1gMnSc4	Churchill
do	do	k7c2	do
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
nabídl	nabídnout	k5eAaPmAgInS	nabídnout
mu	on	k3xPp3gMnSc3	on
buď	buď	k8xC	buď
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
válečného	válečný	k2eAgInSc2d1	válečný
materiálu	materiál	k1gInSc2	materiál
nebo	nebo	k8xC	nebo
nově	nově	k6eAd1	nově
vytvořené	vytvořený	k2eAgNnSc1d1	vytvořené
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
letectví	letectví	k1gNnSc2	letectví
<g/>
.	.	kIx.	.
</s>
<s>
Churchill	Churchill	k1gMnSc1	Churchill
si	se	k3xPyFc3	se
vybral	vybrat	k5eAaPmAgMnS	vybrat
ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
válečného	válečný	k2eAgInSc2d1	válečný
materiálu	materiál	k1gInSc2	materiál
a	a	k8xC	a
následujícího	následující	k2eAgInSc2d1	následující
dne	den	k1gInSc2	den
bylo	být	k5eAaImAgNnS	být
zveřejněno	zveřejnit	k5eAaPmNgNnS	zveřejnit
jeho	jeho	k3xOp3gNnSc1	jeho
jmenování	jmenování	k1gNnSc1	jmenování
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1917	[number]	k4	1917
<g/>
-	-	kIx~	-
<g/>
1922	[number]	k4	1922
Churchill	Churchilla	k1gFnPc2	Churchilla
opět	opět	k6eAd1	opět
zastával	zastávat	k5eAaImAgMnS	zastávat
významné	významný	k2eAgInPc4d1	významný
úřady	úřad	k1gInPc4	úřad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
po	po	k7c6	po
říjnové	říjnový	k2eAgFnSc6d1	říjnová
revoluci	revoluce	k1gFnSc6	revoluce
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
co	co	k9	co
největší	veliký	k2eAgInSc4d3	veliký
zásah	zásah	k1gInSc4	zásah
intervenčních	intervenční	k2eAgNnPc2d1	intervenční
vojsk	vojsko	k1gNnPc2	vojsko
(	(	kIx(	(
<g/>
tvrdě	tvrdě	k6eAd1	tvrdě
<g/>
,	,	kIx,	,
že	že	k8xS	že
bolševismus	bolševismus	k1gInSc1	bolševismus
"	"	kIx"	"
<g/>
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
zadušen	zadusit	k5eAaPmNgInS	zadusit
ještě	ještě	k9	ještě
v	v	k7c6	v
kolébce	kolébka	k1gFnSc6	kolébka
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
uzavření	uzavření	k1gNnSc1	uzavření
separátního	separátní	k2eAgInSc2d1	separátní
míru	mír	k1gInSc2	mír
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
nové	nový	k2eAgFnSc2d1	nová
volby	volba	k1gFnSc2	volba
a	a	k8xC	a
Lloyd	Lloyd	k1gInSc4	Lloyd
George	Georg	k1gMnSc2	Georg
opět	opět	k6eAd1	opět
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
<g/>
.	.	kIx.	.
</s>
<s>
Tentokrát	tentokrát	k6eAd1	tentokrát
se	se	k3xPyFc4	se
Churchill	Churchill	k1gMnSc1	Churchill
stal	stát	k5eAaPmAgMnS	stát
ministrem	ministr	k1gMnSc7	ministr
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g/>
tento	tento	k3xDgInSc4	tento
úřad	úřad	k1gInSc4	úřad
zastával	zastávat	k5eAaImAgMnS	zastávat
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
ledna	leden	k1gInSc2	leden
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Irska	Irsko	k1gNnSc2	Irsko
vyslal	vyslat	k5eAaPmAgMnS	vyslat
nechvalně	chvalně	k6eNd1	chvalně
proslulé	proslulý	k2eAgFnPc4d1	proslulá
jednotky	jednotka	k1gFnPc4	jednotka
Black	Blacka	k1gFnPc2	Blacka
and	and	k?	and
Tans	Tansa	k1gFnPc2	Tansa
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
potlačil	potlačit	k5eAaPmAgInS	potlačit
Irský	irský	k2eAgInSc1d1	irský
boj	boj	k1gInSc1	boj
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
ministrem	ministr	k1gMnSc7	ministr
kolonií	kolonie	k1gFnPc2	kolonie
a	a	k8xC	a
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
funkci	funkce	k1gFnSc6	funkce
byl	být	k5eAaImAgMnS	být
také	také	k9	také
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
signatářů	signatář	k1gMnPc2	signatář
Anglo-irské	Anglorský	k2eAgFnSc2d1	Anglo-irská
smlouvy	smlouva	k1gFnSc2	smlouva
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
dala	dát	k5eAaPmAgFnS	dát
vzniknout	vzniknout	k5eAaPmF	vzniknout
Irské	irský	k2eAgFnSc3d1	irská
republice	republika	k1gFnSc3	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
začala	začít	k5eAaPmAgFnS	začít
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
krize	krize	k1gFnSc1	krize
a	a	k8xC	a
v	v	k7c6	v
krizi	krize	k1gFnSc6	krize
se	se	k3xPyFc4	se
ocitla	ocitnout	k5eAaPmAgFnS	ocitnout
vlivem	vlivem	k7c2	vlivem
vnitřních	vnitřní	k2eAgInPc2d1	vnitřní
sporů	spor	k1gInPc2	spor
i	i	k9	i
liberální	liberální	k2eAgFnSc1d1	liberální
strana	strana	k1gFnSc1	strana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
atmosféře	atmosféra	k1gFnSc6	atmosféra
padla	padnout	k5eAaImAgFnS	padnout
Lloyd	Lloyd	k1gInSc4	Lloyd
Georgova	Georgův	k2eAgFnSc1d1	Georgova
vláda	vláda	k1gFnSc1	vláda
a	a	k8xC	a
v	v	k7c6	v
nových	nový	k2eAgFnPc6d1	nová
volbách	volba	k1gFnPc6	volba
prohrála	prohrát	k5eAaPmAgFnS	prohrát
<g/>
.	.	kIx.	.
</s>
<s>
Churchill	Churchill	k1gMnSc1	Churchill
tak	tak	k6eAd1	tak
přišel	přijít	k5eAaPmAgMnS	přijít
nejen	nejen	k6eAd1	nejen
o	o	k7c4	o
místo	místo	k1gNnSc4	místo
ministra	ministr	k1gMnSc2	ministr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
prohrál	prohrát	k5eAaPmAgInS	prohrát
i	i	k9	i
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
okrsku	okrsek	k1gInSc6	okrsek
v	v	k7c6	v
Dundee	Dundee	k1gFnSc6	Dundee
<g/>
,	,	kIx,	,
i	i	k9	i
o	o	k7c4	o
místo	místo	k1gNnSc4	místo
poslance	poslanec	k1gMnSc2	poslanec
v	v	k7c6	v
parlamentu	parlament	k1gInSc6	parlament
(	(	kIx(	(
<g/>
a	a	k8xC	a
shodou	shoda	k1gFnSc7	shoda
okolností	okolnost	k1gFnPc2	okolnost
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
i	i	k9	i
o	o	k7c4	o
apendix	apendix	k1gInSc4	apendix
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
ruským	ruský	k2eAgMnSc7d1	ruský
revolucionářem	revolucionář	k1gMnSc7	revolucionář
Borisem	Boris	k1gMnSc7	Boris
Savinkovem	Savinkov	k1gInSc7	Savinkov
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
Velcí	velký	k2eAgMnPc1d1	velký
současníci	současník	k1gMnPc1	současník
o	o	k7c6	o
Savinkovovi	Savinkova	k1gMnSc6	Savinkova
psal	psát	k5eAaImAgInS	psát
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Ten	ten	k3xDgMnSc1	ten
muž	muž	k1gMnSc1	muž
má	mít	k5eAaImIp3nS	mít
vědomosti	vědomost	k1gFnPc4	vědomost
státníka	státník	k1gMnSc2	státník
<g/>
,	,	kIx,	,
vlastnosti	vlastnost	k1gFnPc1	vlastnost
velitele	velitel	k1gMnSc2	velitel
<g/>
,	,	kIx,	,
odvahu	odvaha	k1gFnSc4	odvaha
hrdiny	hrdina	k1gMnSc2	hrdina
a	a	k8xC	a
trpělivost	trpělivost	k1gFnSc4	trpělivost
mučedníka	mučedník	k1gMnSc2	mučedník
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc4d1	celý
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
strávil	strávit	k5eAaPmAgMnS	strávit
spiklenectvím	spiklenectví	k1gNnSc7	spiklenectví
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
době	doba	k1gFnSc6	doba
počátku	počátek	k1gInSc2	počátek
vlády	vláda	k1gFnSc2	vláda
Mussoliniho	Mussolini	k1gMnSc2	Mussolini
měl	mít	k5eAaImAgMnS	mít
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
Churchill	Churchill	k1gInSc1	Churchill
mírný	mírný	k2eAgInSc1d1	mírný
obdiv	obdiv	k1gInSc1	obdiv
kvůli	kvůli	k7c3	kvůli
jeho	jeho	k3xOp3gFnPc3	jeho
čistkám	čistka	k1gFnPc3	čistka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
vymýtily	vymýtit	k5eAaPmAgInP	vymýtit
komunismus	komunismus	k1gInSc4	komunismus
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Churchill	Churchill	k1gMnSc1	Churchill
byl	být	k5eAaImAgMnS	být
proslulý	proslulý	k2eAgMnSc1d1	proslulý
nenávistí	nenávist	k1gFnSc7	nenávist
ke	k	k7c3	k
komunismu	komunismus	k1gInSc3	komunismus
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
Říjnové	říjnový	k2eAgFnSc6d1	říjnová
revoluci	revoluce	k1gFnSc6	revoluce
navrhoval	navrhovat	k5eAaImAgMnS	navrhovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
provedla	provést	k5eAaPmAgFnS	provést
invazi	invaze	k1gFnSc4	invaze
do	do	k7c2	do
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Řekl	říct	k5eAaPmAgMnS	říct
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
bolševismus	bolševismus	k1gInSc1	bolševismus
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
zardoušen	zardousit	k5eAaPmNgMnS	zardousit
už	už	k6eAd1	už
v	v	k7c6	v
kolébce	kolébka	k1gFnSc6	kolébka
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Až	až	k6eAd1	až
několik	několik	k4yIc1	několik
let	léto	k1gNnPc2	léto
před	před	k7c7	před
válkou	válka	k1gFnSc7	válka
prohlásil	prohlásit	k5eAaPmAgInS	prohlásit
Itálii	Itálie	k1gFnSc3	Itálie
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc4	Německo
a	a	k8xC	a
Japonsko	Japonsko	k1gNnSc4	Japonsko
za	za	k7c4	za
Osu	osa	k1gFnSc4	osa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
znovu	znovu	k6eAd1	znovu
neúspěšně	úspěšně	k6eNd1	úspěšně
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
za	za	k7c4	za
liberály	liberál	k1gMnPc4	liberál
v	v	k7c6	v
Leicesteru	Leicester	k1gInSc6	Leicester
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
v	v	k7c6	v
Eppingu	Epping	k1gInSc6	Epping
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
konstitualista	konstitualista	k1gMnSc1	konstitualista
<g/>
"	"	kIx"	"
již	již	k9	již
za	za	k7c4	za
podpory	podpora	k1gFnPc4	podpora
konzervativní	konzervativní	k2eAgFnSc2d1	konzervativní
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
usedl	usednout	k5eAaPmAgMnS	usednout
do	do	k7c2	do
křesla	křeslo	k1gNnSc2	křeslo
ministra	ministr	k1gMnSc2	ministr
financí	finance	k1gFnPc2	finance
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
setrval	setrvat	k5eAaPmAgMnS	setrvat
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
znovu	znovu	k6eAd1	znovu
členem	člen	k1gMnSc7	člen
konzervativní	konzervativní	k2eAgFnSc2d1	konzervativní
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c4	v
Británii	Británie	k1gFnSc4	Británie
ke	k	k7c3	k
dvěma	dva	k4xCgFnPc3	dva
významným	významný	k2eAgFnPc3d1	významná
událostem	událost	k1gFnPc3	událost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
vrátila	vrátit	k5eAaPmAgFnS	vrátit
ke	k	k7c3	k
zlatému	zlatý	k2eAgInSc3d1	zlatý
standardu	standard	k1gInSc3	standard
a	a	k8xC	a
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
generální	generální	k2eAgFnSc1d1	generální
stávka	stávka	k1gFnSc1	stávka
<g/>
.	.	kIx.	.
</s>
<s>
Souvislosti	souvislost	k1gFnPc1	souvislost
jsou	být	k5eAaImIp3nP	být
zřejmé	zřejmý	k2eAgFnPc1d1	zřejmá
<g/>
.	.	kIx.	.
</s>
<s>
Návrat	návrat	k1gInSc1	návrat
ke	k	k7c3	k
zlatému	zlatý	k2eAgInSc3d1	zlatý
standardu	standard	k1gInSc3	standard
znamenal	znamenat	k5eAaImAgInS	znamenat
zhodnocení	zhodnocení	k1gNnSc4	zhodnocení
libry	libra	k1gFnSc2	libra
šterlinku	šterlink	k1gInSc2	šterlink
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mělo	mít	k5eAaImAgNnS	mít
katastrofální	katastrofální	k2eAgInSc4d1	katastrofální
následky	následek	k1gInPc4	následek
pro	pro	k7c4	pro
britskou	britský	k2eAgFnSc4d1	britská
exportní	exportní	k2eAgFnSc4d1	exportní
politiku	politika	k1gFnSc4	politika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgNnSc2	ten
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
prudkému	prudký	k2eAgInSc3d1	prudký
poklesu	pokles	k1gInSc3	pokles
mezd	mzda	k1gFnPc2	mzda
a	a	k8xC	a
generální	generální	k2eAgFnSc6d1	generální
stávce	stávka	k1gFnSc6	stávka
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
nešťastný	šťastný	k2eNgInSc1d1	nešťastný
krok	krok	k1gInSc1	krok
(	(	kIx(	(
<g/>
který	který	k3yQgInSc1	který
později	pozdě	k6eAd2	pozdě
Churchill	Churchill	k1gInSc4	Churchill
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
nejhorší	zlý	k2eAgNnSc4d3	nejhorší
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
své	svůj	k3xOyFgFnSc2	svůj
kariéry	kariéra	k1gFnSc2	kariéra
<g/>
)	)	kIx)	)
dokonce	dokonce	k9	dokonce
okomentoval	okomentovat	k5eAaPmAgMnS	okomentovat
John	John	k1gMnSc1	John
Maynard	Maynard	k1gMnSc1	Maynard
Keynes	Keynes	k1gMnSc1	Keynes
ve	v	k7c6	v
studii	studie	k1gFnSc6	studie
The	The	k1gMnSc1	The
Economic	Economic	k1gMnSc1	Economic
Consequences	Consequences	k1gMnSc1	Consequences
of	of	k?	of
Mr	Mr	k1gMnSc1	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Churchill	Churchill	k1gMnSc1	Churchill
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
padla	padnout	k5eAaImAgFnS	padnout
konzervativní	konzervativní	k2eAgFnSc1d1	konzervativní
vláda	vláda	k1gFnSc1	vláda
a	a	k8xC	a
Churchill	Churchill	k1gInSc1	Churchill
přešel	přejít	k5eAaPmAgInS	přejít
do	do	k7c2	do
stínového	stínový	k2eAgInSc2d1	stínový
kabinetu	kabinet	k1gInSc2	kabinet
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
Churchill	Churchill	k1gMnSc1	Churchill
odešel	odejít	k5eAaPmAgMnS	odejít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nesouhlasil	souhlasit	k5eNaImAgMnS	souhlasit
s	s	k7c7	s
ústupkovou	ústupkový	k2eAgFnSc7d1	ústupkový
politikou	politika	k1gFnSc7	politika
vůči	vůči	k7c3	vůči
Indii	Indie	k1gFnSc3	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
pak	pak	k6eAd1	pak
konzervativci	konzervativec	k1gMnPc7	konzervativec
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1931	[number]	k4	1931
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
volby	volba	k1gFnPc4	volba
<g/>
,	,	kIx,	,
nenabídli	nabídnout	k5eNaPmAgMnP	nabídnout
mu	on	k3xPp3gMnSc3	on
ve	v	k7c6	v
vládě	vláda	k1gFnSc6	vláda
žádné	žádný	k3yNgNnSc1	žádný
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
zůstal	zůstat	k5eAaPmAgInS	zůstat
řadovým	řadový	k2eAgMnSc7d1	řadový
poslancem	poslanec	k1gMnSc7	poslanec
<g/>
.	.	kIx.	.
</s>
<s>
Žil	žít	k5eAaImAgMnS	žít
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
venkovském	venkovský	k2eAgNnSc6d1	venkovské
sídle	sídlo	k1gNnSc6	sídlo
v	v	k7c6	v
Chartwellu	Chartwell	k1gInSc6	Chartwell
v	v	k7c6	v
Kentu	Kent	k1gInSc6	Kent
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
známým	známý	k2eAgInSc7d1	známý
hlavně	hlavně	k9	hlavně
kvůli	kvůli	k7c3	kvůli
svým	svůj	k3xOyFgInPc3	svůj
projevům	projev	k1gInPc3	projev
v	v	k7c6	v
parlamentu	parlament	k1gInSc6	parlament
<g/>
,	,	kIx,	,
oponujícím	oponující	k2eAgInSc6d1	oponující
vstřícné	vstřícný	k2eAgFnSc3d1	vstřícná
politice	politika	k1gFnSc3	politika
vlády	vláda	k1gFnSc2	vláda
vůči	vůči	k7c3	vůči
Indii	Indie	k1gFnSc3	Indie
a	a	k8xC	a
její	její	k3xOp3gFnSc3	její
snaze	snaha	k1gFnSc3	snaha
získat	získat	k5eAaPmF	získat
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
Nebyl	být	k5eNaImAgInS	být
osloven	oslovit	k5eAaPmNgInS	oslovit
ani	ani	k8xC	ani
když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
zřízeno	zřídit	k5eAaPmNgNnS	zřídit
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
pro	pro	k7c4	pro
koordinaci	koordinace	k1gFnSc4	koordinace
obrany	obrana	k1gFnSc2	obrana
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
<g/>
.	.	kIx.	.
</s>
<s>
Nedostalo	dostat	k5eNaPmAgNnS	dostat
se	se	k3xPyFc4	se
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
ani	ani	k8xC	ani
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
odešel	odejít	k5eAaPmAgMnS	odejít
premiér	premiér	k1gMnSc1	premiér
Baldwin	Baldwin	k1gMnSc1	Baldwin
a	a	k8xC	a
ministerským	ministerský	k2eAgMnSc7d1	ministerský
předsedou	předseda	k1gMnSc7	předseda
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Neville	Neville	k1gInSc1	Neville
Chamberlain	Chamberlain	k2eAgInSc1d1	Chamberlain
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
výměny	výměna	k1gFnSc2	výměna
na	na	k7c6	na
postu	post	k1gInSc6	post
premiéra	premiéra	k1gFnSc1	premiéra
byla	být	k5eAaImAgFnS	být
patrna	patrn	k2eAgFnSc1d1	patrna
v	v	k7c6	v
britské	britský	k2eAgFnSc6d1	britská
zahraniční	zahraniční	k2eAgFnSc6d1	zahraniční
politice	politika	k1gFnSc6	politika
jistá	jistý	k2eAgFnSc1d1	jistá
změna	změna	k1gFnSc1	změna
<g/>
.	.	kIx.	.
</s>
<s>
Baldwin	Baldwina	k1gFnPc2	Baldwina
se	se	k3xPyFc4	se
setkání	setkání	k1gNnSc1	setkání
s	s	k7c7	s
Hitlerem	Hitler	k1gMnSc7	Hitler
vyhýbal	vyhýbat	k5eAaImAgMnS	vyhýbat
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Neville	Neville	k1gFnSc1	Neville
Chamberlain	Chamberlain	k1gInSc1	Chamberlain
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
poslal	poslat	k5eAaPmAgMnS	poslat
již	již	k6eAd1	již
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
1937	[number]	k4	1937
svého	svůj	k3xOyFgMnSc2	svůj
pozdějšího	pozdní	k2eAgMnSc2d2	pozdější
ministra	ministr	k1gMnSc2	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
lorda	lord	k1gMnSc2	lord
Halifaxe	Halifaxe	k1gFnSc2	Halifaxe
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
více	hodně	k6eAd2	hodně
o	o	k7c6	o
Hitlerových	Hitlerových	k2eAgInPc6d1	Hitlerových
cílech	cíl	k1gInPc6	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Churchill	Churchill	k1gMnSc1	Churchill
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
vůdcem	vůdce	k1gMnSc7	vůdce
opozice	opozice	k1gFnSc2	opozice
ve	v	k7c6	v
vládní	vládní	k2eAgFnSc6d1	vládní
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
Úměrně	úměrně	k6eAd1	úměrně
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
rostla	růst	k5eAaImAgFnS	růst
moc	moc	k1gFnSc4	moc
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
rostl	růst	k5eAaImAgInS	růst
i	i	k9	i
Churchillův	Churchillův	k2eAgInSc1d1	Churchillův
odpor	odpor	k1gInSc1	odpor
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1939	[number]	k4	1939
bylo	být	k5eAaImAgNnS	být
Polsko	Polsko	k1gNnSc1	Polsko
napadeno	napaden	k2eAgNnSc1d1	napadeno
Německem	Německo	k1gNnSc7	Německo
a	a	k8xC	a
3	[number]	k4	3
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
Británie	Británie	k1gFnSc1	Británie
do	do	k7c2	do
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Následujícího	následující	k2eAgInSc2d1	následující
dne	den	k1gInSc2	den
se	se	k3xPyFc4	se
Winston	Winston	k1gInSc1	Winston
Churchill	Churchilla	k1gFnPc2	Churchilla
stal	stát	k5eAaPmAgInS	stát
opět	opět	k6eAd1	opět
prvním	první	k4xOgMnSc7	první
lordem	lord	k1gMnSc7	lord
admirality	admiralita	k1gFnSc2	admiralita
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
tzv.	tzv.	kA	tzv.
podivné	podivný	k2eAgFnSc2d1	podivná
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
ukázal	ukázat	k5eAaPmAgInS	ukázat
být	být	k5eAaImF	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejaktivnějších	aktivní	k2eAgMnPc2d3	nejaktivnější
ministrů	ministr	k1gMnPc2	ministr
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jediná	jediný	k2eAgFnSc1d1	jediná
výrazná	výrazný	k2eAgFnSc1d1	výrazná
aktivita	aktivita	k1gFnSc1	aktivita
probíhala	probíhat	k5eAaImAgFnS	probíhat
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Churchill	Churchill	k1gMnSc1	Churchill
obhajoval	obhajovat	k5eAaImAgMnS	obhajovat
preventivní	preventivní	k2eAgNnSc4d1	preventivní
obsazení	obsazení	k1gNnSc4	obsazení
Narviku	Narvik	k1gInSc2	Narvik
<g/>
,	,	kIx,	,
norského	norský	k2eAgInSc2d1	norský
neutrálního	neutrální	k2eAgInSc2d1	neutrální
přístavu	přístav	k1gInSc2	přístav
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
byla	být	k5eAaImAgFnS	být
vyvážena	vyvážit	k5eAaPmNgFnS	vyvážit
železná	železný	k2eAgFnSc1d1	železná
ruda	ruda	k1gFnSc1	ruda
(	(	kIx(	(
<g/>
surovina	surovina	k1gFnSc1	surovina
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
Německo	Německo	k1gNnSc1	Německo
nutně	nutně	k6eAd1	nutně
potřebovalo	potřebovat	k5eAaImAgNnS	potřebovat
a	a	k8xC	a
nedostávalo	dostávat	k5eNaImAgNnS	dostávat
se	se	k3xPyFc4	se
mu	on	k3xPp3gNnSc3	on
jí	jíst	k5eAaImIp3nS	jíst
<g/>
)	)	kIx)	)
a	a	k8xC	a
švédské	švédský	k2eAgFnSc2d1	švédská
Kiruny	Kiruna	k1gFnSc2	Kiruna
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejíž	jejíž	k3xOyRp3gFnSc6	jejíž
blízkosti	blízkost	k1gFnSc6	blízkost
ležely	ležet	k5eAaImAgInP	ležet
doly	dol	k1gInPc1	dol
na	na	k7c4	na
železnou	železný	k2eAgFnSc4d1	železná
rudu	ruda	k1gFnSc4	ruda
<g/>
.	.	kIx.	.
</s>
<s>
Chamberlain	Chamberlain	k1gInSc1	Chamberlain
a	a	k8xC	a
zbytek	zbytek	k1gInSc1	zbytek
kabinetu	kabinet	k1gInSc2	kabinet
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
akcí	akce	k1gFnSc7	akce
nesouhlasili	souhlasit	k5eNaImAgMnP	souhlasit
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
byla	být	k5eAaImAgFnS	být
odložena	odložit	k5eAaPmNgFnS	odložit
až	až	k6eAd1	až
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
německé	německý	k2eAgFnSc2d1	německá
invaze	invaze	k1gFnSc2	invaze
do	do	k7c2	do
Norska	Norsko	k1gNnSc2	Norsko
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
i	i	k9	i
přes	přes	k7c4	přes
britské	britský	k2eAgFnPc4d1	britská
snahy	snaha	k1gFnPc4	snaha
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1940	[number]	k4	1940
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Winston	Winston	k1gInSc1	Winston
Churchill	Churchill	k1gMnSc1	Churchill
ministerským	ministerský	k2eAgMnSc7d1	ministerský
předsedou	předseda	k1gMnSc7	předseda
(	(	kIx(	(
<g/>
rozhodovalo	rozhodovat	k5eAaImAgNnS	rozhodovat
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
ním	on	k3xPp3gMnSc7	on
a	a	k8xC	a
lordem	lord	k1gMnSc7	lord
Halifaxem	Halifax	k1gInSc7	Halifax
<g/>
)	)	kIx)	)
a	a	k8xC	a
13	[number]	k4	13
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
pronesl	pronést	k5eAaPmAgMnS	pronést
ve	v	k7c6	v
sněmovně	sněmovna	k1gFnSc6	sněmovna
známý	známý	k2eAgInSc4d1	známý
projev	projev	k1gInSc4	projev
"	"	kIx"	"
<g/>
o	o	k7c6	o
krvi	krev	k1gFnSc6	krev
<g/>
,	,	kIx,	,
dřině	dřina	k1gFnSc6	dřina
<g/>
,	,	kIx,	,
potu	pot	k1gInSc6	pot
a	a	k8xC	a
slzách	slza	k1gFnPc6	slza
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yInSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
Churchillovy	Churchillův	k2eAgFnSc2d1	Churchillova
politické	politický	k2eAgFnSc2d1	politická
činnosti	činnost	k1gFnSc2	činnost
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
druhou	druhý	k4xOgFnSc4	druhý
světovou	světový	k2eAgFnSc4d1	světová
válku	válka	k1gFnSc4	válka
rozdělit	rozdělit	k5eAaPmF	rozdělit
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
etap	etapa	k1gFnPc2	etapa
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
trvala	trvat	k5eAaImAgFnS	trvat
od	od	k7c2	od
května	květen	k1gInSc2	květen
1940	[number]	k4	1940
do	do	k7c2	do
prosince	prosinec	k1gInSc2	prosinec
1941	[number]	k4	1941
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Británie	Británie	k1gFnSc1	Británie
stála	stát	k5eAaImAgFnS	stát
osamocena	osamotit	k5eAaPmNgFnS	osamotit
proti	proti	k7c3	proti
Německu	Německo	k1gNnSc3	Německo
a	a	k8xC	a
Itálii	Itálie	k1gFnSc3	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
ještě	ještě	k9	ještě
do	do	k7c2	do
války	válka	k1gFnSc2	válka
nevstoupily	vstoupit	k5eNaPmAgFnP	vstoupit
<g/>
,	,	kIx,	,
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
Pakt	pakt	k1gInSc4	pakt
o	o	k7c6	o
neútočení	neútočení	k1gNnSc6	neútočení
a	a	k8xC	a
ostatní	ostatní	k2eAgInPc1d1	ostatní
státy	stát	k1gInPc1	stát
již	již	k6eAd1	již
stály	stát	k5eAaImAgInP	stát
mimo	mimo	k7c4	mimo
hru	hra	k1gFnSc4	hra
<g/>
.	.	kIx.	.
</s>
<s>
Churchill	Churchill	k1gMnSc1	Churchill
se	se	k3xPyFc4	se
zpočátku	zpočátku	k6eAd1	zpočátku
soustředil	soustředit	k5eAaPmAgMnS	soustředit
na	na	k7c4	na
čtyři	čtyři	k4xCgInPc4	čtyři
body	bod	k1gInPc4	bod
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
prvé	prvý	k4xOgFnPc4	prvý
odsunul	odsunout	k5eAaPmAgInS	odsunout
do	do	k7c2	do
pozadí	pozadí	k1gNnSc2	pozadí
prominentní	prominentní	k2eAgFnSc2d1	prominentní
politiky	politika	k1gFnSc2	politika
appeasementu	appeasement	k1gInSc2	appeasement
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
druhé	druhý	k4xOgNnSc4	druhý
na	na	k7c6	na
sebe	sebe	k3xPyFc4	sebe
soustředil	soustředit	k5eAaPmAgInS	soustředit
všechny	všechen	k3xTgFnPc4	všechen
významné	významný	k2eAgFnPc4d1	významná
funkce	funkce	k1gFnPc4	funkce
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
vést	vést	k5eAaImF	vést
válku	válka	k1gFnSc4	válka
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
rozhodovat	rozhodovat	k5eAaImF	rozhodovat
o	o	k7c6	o
všem	všecek	k3xTgNnSc6	všecek
důležitém	důležitý	k2eAgNnSc6d1	důležité
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
třetí	třetí	k4xOgNnSc4	třetí
provedl	provést	k5eAaPmAgInS	provést
industriální	industriální	k2eAgFnSc3d1	industriální
mobilizaci	mobilizace	k1gFnSc3	mobilizace
Británie	Británie	k1gFnSc2	Británie
(	(	kIx(	(
<g/>
okamžitě	okamžitě	k6eAd1	okamžitě
dal	dát	k5eAaPmAgInS	dát
svému	svůj	k3xOyFgMnSc3	svůj
příteli	přítel	k1gMnSc3	přítel
baronu	baron	k1gMnSc3	baron
Beaverbrookovi	Beaverbrook	k1gMnSc3	Beaverbrook
na	na	k7c6	na
starosti	starost	k1gFnSc6	starost
leteckou	letecký	k2eAgFnSc4d1	letecká
produkci	produkce	k1gFnSc4	produkce
<g/>
;	;	kIx,	;
byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
Beaverbrookova	Beaverbrookův	k2eAgFnSc1d1	Beaverbrookův
zásluha	zásluha	k1gFnSc1	zásluha
<g/>
,	,	kIx,	,
že	že	k8xS	že
během	během	k7c2	během
letecké	letecký	k2eAgFnSc2d1	letecká
bitvy	bitva	k1gFnSc2	bitva
byla	být	k5eAaImAgFnS	být
Británie	Británie	k1gFnSc1	Británie
schopna	schopen	k2eAgFnSc1d1	schopna
vzdorovat	vzdorovat	k5eAaImF	vzdorovat
německým	německý	k2eAgInPc3d1	německý
náletům	nálet	k1gInPc3	nálet
<g/>
)	)	kIx)	)
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
za	za	k7c4	za
čtvrté	čtvrtá	k1gFnPc4	čtvrtá
uvedl	uvést	k5eAaPmAgMnS	uvést
v	v	k7c4	v
život	život	k1gInSc4	život
anglo-americkou	anglomerický	k2eAgFnSc4d1	anglo-americká
alianci	aliance	k1gFnSc4	aliance
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
fázi	fáze	k1gFnSc6	fáze
měla	mít	k5eAaImAgFnS	mít
podobu	podoba	k1gFnSc4	podoba
vysílání	vysílání	k1gNnSc2	vysílání
amerických	americký	k2eAgFnPc2d1	americká
lodí	loď	k1gFnPc2	loď
přes	přes	k7c4	přes
Atlantik	Atlantik	k1gInSc4	Atlantik
a	a	k8xC	a
zásobování	zásobování	k1gNnSc4	zásobování
Británie	Británie	k1gFnSc2	Británie
životně	životně	k6eAd1	životně
důležitými	důležitý	k2eAgInPc7d1	důležitý
materiály	materiál	k1gInPc7	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
etapa	etapa	k1gFnSc1	etapa
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
období	období	k1gNnSc4	období
prosinec	prosinec	k1gInSc1	prosinec
1941	[number]	k4	1941
až	až	k9	až
listopad	listopad	k1gInSc1	listopad
1942	[number]	k4	1942
a	a	k8xC	a
nebyla	být	k5eNaImAgFnS	být
příliš	příliš	k6eAd1	příliš
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
<g/>
.	.	kIx.	.
</s>
<s>
Japonci	Japonec	k1gMnPc1	Japonec
dobyli	dobýt	k5eAaPmAgMnP	dobýt
rychle	rychle	k6eAd1	rychle
Malajsko	Malajsko	k1gNnSc4	Malajsko
i	i	k8xC	i
Barmu	Barma	k1gFnSc4	Barma
<g/>
,	,	kIx,	,
Rommel	Rommel	k1gInSc1	Rommel
porazil	porazit	k5eAaPmAgInS	porazit
nilskou	nilský	k2eAgFnSc4d1	nilská
armádu	armáda	k1gFnSc4	armáda
<g/>
,	,	kIx,	,
padl	padnout	k5eAaImAgInS	padnout
Tobrúk	Tobrúk	k1gInSc1	Tobrúk
<g/>
,	,	kIx,	,
přetěžované	přetěžovaný	k2eAgNnSc1d1	přetěžované
loďstvo	loďstvo	k1gNnSc1	loďstvo
bylo	být	k5eAaImAgNnS	být
decimováno	decimovat	k5eAaBmNgNnS	decimovat
v	v	k7c6	v
Pacifiku	Pacifik	k1gInSc6	Pacifik
a	a	k8xC	a
Indie	Indie	k1gFnSc1	Indie
vypověděla	vypovědět	k5eAaPmAgFnS	vypovědět
Británii	Británie	k1gFnSc4	Británie
poslušnost	poslušnost	k1gFnSc4	poslušnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
bylo	být	k5eAaImAgNnS	být
navrženo	navrhnout	k5eAaPmNgNnS	navrhnout
hlasování	hlasování	k1gNnSc1	hlasování
o	o	k7c6	o
důvěře	důvěra	k1gFnSc6	důvěra
vládě	vláda	k1gFnSc6	vláda
a	a	k8xC	a
v	v	k7c6	v
září	září	k1gNnSc6	září
byl	být	k5eAaImAgInS	být
navržen	navrhnout	k5eAaPmNgInS	navrhnout
dokonce	dokonce	k9	dokonce
Churchillův	Churchillův	k2eAgMnSc1d1	Churchillův
protikandidát	protikandidát	k1gMnSc1	protikandidát
(	(	kIx(	(
<g/>
sir	sir	k1gMnSc1	sir
Stafford	Stafford	k1gMnSc1	Stafford
Cripps	Crippsa	k1gFnPc2	Crippsa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Churchill	Churchill	k1gInSc1	Churchill
svou	svůj	k3xOyFgFnSc4	svůj
pozici	pozice	k1gFnSc4	pozice
nakonec	nakonec	k6eAd1	nakonec
dokázal	dokázat	k5eAaPmAgMnS	dokázat
obhájit	obhájit	k5eAaPmF	obhájit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
aliance	aliance	k1gFnSc1	aliance
spojenců	spojenec	k1gMnPc2	spojenec
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
,	,	kIx,	,
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
Británie	Británie	k1gFnSc1	Británie
představovala	představovat	k5eAaImAgFnS	představovat
nejslabší	slabý	k2eAgInSc4d3	nejslabší
článek	článek	k1gInSc4	článek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Churchill	Churchill	k1gMnSc1	Churchill
stále	stále	k6eAd1	stále
doufal	doufat	k5eAaImAgMnS	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
podaří	podařit	k5eAaPmIp3nS	podařit
udržet	udržet	k5eAaPmF	udržet
SSSR	SSSR	kA	SSSR
mimo	mimo	k7c4	mimo
střední	střední	k2eAgFnSc4d1	střední
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
veden	veden	k2eAgInSc1d1	veden
úder	úder	k1gInSc1	úder
ze	z	k7c2	z
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
Terst-Vídeň-Praha	Terst-Vídeň-Praha	k1gFnSc1	Terst-Vídeň-Praha
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
však	však	k9	však
nenalezlo	nalézt	k5eNaBmAgNnS	nalézt
podporu	podpora	k1gFnSc4	podpora
ani	ani	k8xC	ani
u	u	k7c2	u
Stalina	Stalin	k1gMnSc2	Stalin
ani	ani	k8xC	ani
u	u	k7c2	u
Roosevelta	Roosevelt	k1gMnSc2	Roosevelt
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třetí	třetí	k4xOgFnSc6	třetí
etapě	etapa	k1gFnSc6	etapa
(	(	kIx(	(
<g/>
prosinec	prosinec	k1gInSc1	prosinec
1942	[number]	k4	1942
-	-	kIx~	-
červen	červen	k1gInSc1	červen
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
nucen	nucen	k2eAgInSc4d1	nucen
tento	tento	k3xDgInSc4	tento
plán	plán	k1gInSc4	plán
opustit	opustit	k5eAaPmF	opustit
<g/>
,	,	kIx,	,
a	a	k8xC	a
zaprodat	zaprodat	k5eAaPmF	zaprodat
východní	východní	k2eAgFnSc4d1	východní
Evropu	Evropa	k1gFnSc4	Evropa
stalinskému	stalinský	k2eAgNnSc3d1	stalinské
Rusku	Rusko	k1gNnSc3	Rusko
na	na	k7c6	na
konferencích	konference	k1gFnPc6	konference
v	v	k7c6	v
Teheránu	Teherán	k1gInSc6	Teherán
a	a	k8xC	a
v	v	k7c6	v
Jaltě	Jalta	k1gFnSc6	Jalta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Britské	britský	k2eAgFnSc6d1	britská
Indii	Indie	k1gFnSc6	Indie
již	již	k6eAd1	již
od	od	k7c2	od
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
působilo	působit	k5eAaImAgNnS	působit
intenzivní	intenzivní	k2eAgNnSc1d1	intenzivní
hnutí	hnutí	k1gNnSc1	hnutí
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
kterému	který	k3yQgNnSc3	který
Churchill	Churchill	k1gMnSc1	Churchill
velmi	velmi	k6eAd1	velmi
důrazně	důrazně	k6eAd1	důrazně
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
Britské	britský	k2eAgFnSc2d1	britská
Indie	Indie	k1gFnSc2	Indie
vládou	vláda	k1gFnSc7	vláda
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
vyslána	vyslán	k2eAgFnSc1d1	vyslána
takzvaná	takzvaný	k2eAgFnSc1d1	takzvaná
Crippsova	Crippsův	k2eAgFnSc1d1	Crippsův
mise	mise	k1gFnSc1	mise
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
získat	získat	k5eAaPmF	získat
podporu	podpora	k1gFnSc4	podpora
indických	indický	k2eAgMnPc2d1	indický
nacionalistů	nacionalista	k1gMnPc2	nacionalista
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
příslib	příslib	k1gInSc4	příslib
indické	indický	k2eAgFnSc2d1	indická
nezávislosti	nezávislost	k1gFnSc2	nezávislost
nejpozději	pozdě	k6eAd3	pozdě
těsně	těsně	k6eAd1	těsně
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
mise	mise	k1gFnSc1	mise
ale	ale	k9	ale
neměla	mít	k5eNaImAgFnS	mít
podporu	podpora	k1gFnSc4	podpora
většiny	většina	k1gFnSc2	většina
britských	britský	k2eAgMnPc2d1	britský
představitelů	představitel	k1gMnPc2	představitel
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
satmoného	satmoné	k1gNnSc2	satmoné
ministerského	ministerský	k2eAgMnSc2d1	ministerský
předsedy	předseda	k1gMnSc2	předseda
Churchilla	Churchill	k1gMnSc2	Churchill
<g/>
.	.	kIx.	.
</s>
<s>
Indický	indický	k2eAgInSc1d1	indický
národní	národní	k2eAgInSc1d1	národní
kongres	kongres	k1gInSc1	kongres
zklamaný	zklamaný	k2eAgInSc1d1	zklamaný
výsledky	výsledek	k1gInPc4	výsledek
vyjednávání	vyjednávání	k1gNnSc2	vyjednávání
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1942	[number]	k4	1942
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
požadavek	požadavek	k1gInSc1	požadavek
na	na	k7c4	na
okamžité	okamžitý	k2eAgNnSc4d1	okamžité
stažení	stažení	k1gNnSc4	stažení
Britů	Brit	k1gMnPc2	Brit
z	z	k7c2	z
Indie	Indie	k1gFnSc2	Indie
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
budou	být	k5eAaImBp3nP	být
čelit	čelit	k5eAaImF	čelit
masové	masový	k2eAgFnSc2d1	masová
občanské	občanský	k2eAgFnSc2d1	občanská
neposlušnosti	neposlušnost	k1gFnSc2	neposlušnost
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
policejní	policejní	k2eAgInPc1d1	policejní
orgány	orgán	k1gInPc1	orgán
Britské	britský	k2eAgFnSc2d1	britská
Indie	Indie	k1gFnSc2	Indie
zatkly	zatknout	k5eAaPmAgInP	zatknout
desetitisíce	desetitisíce	k1gInPc4	desetitisíce
představitelů	představitel	k1gMnPc2	představitel
Indického	indický	k2eAgInSc2d1	indický
národního	národní	k2eAgInSc2d1	národní
kongresu	kongres	k1gInSc2	kongres
a	a	k8xC	a
držely	držet	k5eAaImAgFnP	držet
je	on	k3xPp3gFnPc4	on
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Britské	britský	k2eAgFnSc6d1	britská
Indii	Indie	k1gFnSc6	Indie
vypukly	vypuknout	k5eAaPmAgFnP	vypuknout
masové	masový	k2eAgFnPc1d1	masová
demonstrace	demonstrace	k1gFnPc1	demonstrace
<g/>
,	,	kIx,	,
vedené	vedený	k2eAgNnSc1d1	vedené
nejprve	nejprve	k6eAd1	nejprve
studenty	student	k1gMnPc4	student
a	a	k8xC	a
následně	následně	k6eAd1	následně
hlavně	hlavně	k9	hlavně
venkovskými	venkovský	k2eAgMnPc7d1	venkovský
rolníky	rolník	k1gMnPc7	rolník
<g/>
,	,	kIx,	,
tyto	tento	k3xDgFnPc1	tento
byly	být	k5eAaImAgFnP	být
násilně	násilně	k6eAd1	násilně
potlačeny	potlačit	k5eAaPmNgFnP	potlačit
britskou	britský	k2eAgFnSc7d1	britská
armádou	armáda	k1gFnSc7	armáda
během	během	k7c2	během
následujících	následující	k2eAgInPc2d1	následující
šesti	šest	k4xCc2	šest
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Winston	Winston	k1gInSc1	Winston
Churchill	Churchill	k1gInSc1	Churchill
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
událostmi	událost	k1gFnPc7	událost
ještě	ještě	k9	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nenávidím	návidět	k5eNaImIp1nS	návidět
Indy	Indus	k1gInPc1	Indus
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
bestiální	bestiální	k2eAgMnPc1d1	bestiální
lidé	člověk	k1gMnPc1	člověk
s	s	k7c7	s
bestiálním	bestiální	k2eAgNnSc7d1	bestiální
náboženstvím	náboženství	k1gNnSc7	náboženství
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
Brity	Brit	k1gMnPc4	Brit
ovládané	ovládaný	k2eAgNnSc1d1	ovládané
Bengálsko	Bengálsko	k1gNnSc1	Bengálsko
postihl	postihnout	k5eAaPmAgInS	postihnout
hladomor	hladomor	k1gInSc4	hladomor
způsobený	způsobený	k2eAgInSc4d1	způsobený
japonským	japonský	k2eAgInSc7d1	japonský
postupem	postup	k1gInSc7	postup
v	v	k7c6	v
Barmě	Barma	k1gFnSc6	Barma
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zahubil	zahubit	k5eAaPmAgInS	zahubit
dva	dva	k4xCgInPc1	dva
až	až	k9	až
tři	tři	k4xCgInPc1	tři
miliony	milion	k4xCgInPc1	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
Britské	britský	k2eAgFnSc2d1	britská
Indie	Indie	k1gFnSc2	Indie
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
i	i	k9	i
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
britská	britský	k2eAgFnSc1d1	britská
správa	správa	k1gFnSc1	správa
přednostně	přednostně	k6eAd1	přednostně
zásobovala	zásobovat	k5eAaImAgFnS	zásobovat
jídlem	jídlo	k1gNnSc7	jídlo
z	z	k7c2	z
Indie	Indie	k1gFnSc2	Indie
vojáky	voják	k1gMnPc4	voják
bojující	bojující	k2eAgMnPc4d1	bojující
ve	v	k7c4	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
Churchill	Churchill	k1gMnSc1	Churchill
informován	informovat	k5eAaBmNgMnS	informovat
o	o	k7c6	o
hladomoru	hladomor	k1gInSc6	hladomor
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
jen	jen	k6eAd1	jen
poznamenal	poznamenat	k5eAaPmAgMnS	poznamenat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
jestli	jestli	k8xS	jestli
je	být	k5eAaImIp3nS	být
tam	tam	k6eAd1	tam
hladomor	hladomor	k1gInSc4	hladomor
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
proč	proč	k6eAd1	proč
Gándhí	Gándhí	k1gMnSc1	Gándhí
ještě	ještě	k6eAd1	ještě
neumřel	umřít	k5eNaPmAgMnS	umřít
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
Churchill	Churchill	k1gMnSc1	Churchill
se	se	k3xPyFc4	se
také	také	k9	také
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
konferencí	konference	k1gFnSc7	konference
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
rozhodovalo	rozhodovat	k5eAaImAgNnS	rozhodovat
o	o	k7c6	o
budoucích	budoucí	k2eAgFnPc6d1	budoucí
hranicích	hranice	k1gFnPc6	hranice
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Týkalo	týkat	k5eAaImAgNnS	týkat
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
především	především	k9	především
konferencí	konference	k1gFnSc7	konference
v	v	k7c6	v
Teheránu	Teherán	k1gInSc6	Teherán
<g/>
,	,	kIx,	,
Jaltě	Jalta	k1gFnSc6	Jalta
a	a	k8xC	a
Postupimi	Postupim	k1gFnSc6	Postupim
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
Churchill	Churchill	k1gMnSc1	Churchill
snažil	snažit	k5eAaImAgMnS	snažit
"	"	kIx"	"
<g/>
hrát	hrát	k5eAaImF	hrát
partii	partie	k1gFnSc4	partie
<g/>
"	"	kIx"	"
se	s	k7c7	s
Stalinem	Stalin	k1gMnSc7	Stalin
za	za	k7c4	za
slabé	slabý	k2eAgFnPc4d1	slabá
podpory	podpora	k1gFnPc4	podpora
Roosevelta	Roosevelt	k1gMnSc2	Roosevelt
<g/>
.	.	kIx.	.
</s>
<s>
Obzvláště	obzvláště	k6eAd1	obzvláště
spornou	sporný	k2eAgFnSc4d1	sporná
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
hranice	hranice	k1gFnSc1	hranice
mezi	mezi	k7c7	mezi
Polskem	Polsko	k1gNnSc7	Polsko
a	a	k8xC	a
Sovětským	sovětský	k2eAgInSc7d1	sovětský
Svazem	svaz	k1gInSc7	svaz
a	a	k8xC	a
Polskem	Polsko	k1gNnSc7	Polsko
a	a	k8xC	a
Německem	Německo	k1gNnSc7	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
dohoda	dohoda	k1gFnSc1	dohoda
o	o	k7c6	o
posunutí	posunutí	k1gNnSc6	posunutí
hranic	hranice	k1gFnPc2	hranice
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
vnímána	vnímán	k2eAgFnSc1d1	vnímána
jako	jako	k8xC	jako
zrada	zrada	k1gFnSc1	zrada
na	na	k7c6	na
polské	polský	k2eAgFnSc6d1	polská
exilové	exilový	k2eAgFnSc6d1	exilová
vládě	vláda	k1gFnSc6	vláda
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
s	s	k7c7	s
ní	on	k3xPp3gFnSc3	on
nikdy	nikdy	k6eAd1	nikdy
nesouhlasila	souhlasit	k5eNaImAgNnP	souhlasit
<g/>
.	.	kIx.	.
</s>
<s>
Churchill	Churchill	k1gMnSc1	Churchill
byl	být	k5eAaImAgMnS	být
přesvědčen	přesvědčit	k5eAaPmNgMnS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jediný	jediný	k2eAgInSc1d1	jediný
způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
zmírnit	zmírnit	k5eAaPmF	zmírnit
napětí	napětí	k1gNnSc4	napětí
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgInPc7	tento
národy	národ	k1gInPc7	národ
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
vytvoření	vytvoření	k1gNnSc1	vytvoření
hranic	hranice	k1gFnPc2	hranice
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
by	by	kYmCp3nP	by
respektovaly	respektovat	k5eAaImAgFnP	respektovat
hranici	hranice	k1gFnSc4	hranice
národnostní	národnostní	k2eAgInSc1d1	národnostní
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
i	i	k9	i
odsunuto	odsunut	k2eAgNnSc4d1	odsunuto
německé	německý	k2eAgNnSc4d1	německé
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
západ	západ	k1gInSc4	západ
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nejen	nejen	k6eAd1	nejen
z	z	k7c2	z
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Churchill	Churchill	k1gMnSc1	Churchill
byl	být	k5eAaImAgMnS	být
zastáncem	zastánce	k1gMnSc7	zastánce
tohoto	tento	k3xDgNnSc2	tento
radikálního	radikální	k2eAgNnSc2d1	radikální
řešení	řešení	k1gNnSc2	řešení
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
debatě	debata	k1gFnSc6	debata
15	[number]	k4	15
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1944	[number]	k4	1944
v	v	k7c6	v
dolní	dolní	k2eAgFnSc6d1	dolní
sněmovně	sněmovna	k1gFnSc6	sněmovna
Parlamentu	parlament	k1gInSc2	parlament
vysvětloval	vysvětlovat	k5eAaImAgInS	vysvětlovat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Vyhoštění	vyhoštění	k1gNnSc1	vyhoštění
je	být	k5eAaImIp3nS	být
metoda	metoda	k1gFnSc1	metoda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
jsme	být	k5eAaImIp1nP	být
až	až	k9	až
doposud	doposud	k6eAd1	doposud
mohli	moct	k5eAaImAgMnP	moct
vidět	vidět	k5eAaImF	vidět
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
nejvíce	hodně	k6eAd3	hodně
uspokojivá	uspokojivý	k2eAgFnSc1d1	uspokojivá
a	a	k8xC	a
trvalá	trvalý	k2eAgFnSc1d1	trvalá
<g/>
.	.	kIx.	.
</s>
<s>
Odstraní	odstranit	k5eAaPmIp3nP	odstranit
promíchání	promíchání	k1gNnSc4	promíchání
etnik	etnikum	k1gNnPc2	etnikum
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
nekonečné	konečný	k2eNgInPc4d1	nekonečný
problémy	problém	k1gInPc4	problém
<g/>
...	...	k?	...
Bude	být	k5eAaImBp3nS	být
proveden	provést	k5eAaPmNgInS	provést
čistý	čistý	k2eAgInSc1d1	čistý
řez	řez	k1gInSc1	řez
<g/>
.	.	kIx.	.
</s>
<s>
Nejsem	být	k5eNaImIp1nS	být
znepokojen	znepokojen	k2eAgMnSc1d1	znepokojen
těmito	tento	k3xDgInPc7	tento
přesuny	přesun	k1gInPc7	přesun
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
snadněji	snadno	k6eAd2	snadno
realizovatelné	realizovatelný	k2eAgFnPc1d1	realizovatelná
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
podmínkách	podmínka	k1gFnPc6	podmínka
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Některé	některý	k3yIgNnSc1	některý
z	z	k7c2	z
jeho	jeho	k3xOp3gNnPc2	jeho
rozhodnutí	rozhodnutí	k1gNnPc2	rozhodnutí
a	a	k8xC	a
činů	čin	k1gInPc2	čin
jsou	být	k5eAaImIp3nP	být
dodnes	dodnes	k6eAd1	dodnes
považované	považovaný	k2eAgInPc1d1	považovaný
za	za	k7c4	za
kontroverzní	kontroverzní	k2eAgNnSc4d1	kontroverzní
<g/>
.	.	kIx.	.
</s>
<s>
Týká	týkat	k5eAaImIp3nS	týkat
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
jeho	on	k3xPp3gInSc2	on
indiferentního	indiferentní	k2eAgInSc2d1	indiferentní
postoje	postoj	k1gInSc2	postoj
vůči	vůči	k7c3	vůči
bengálskému	bengálský	k2eAgInSc3d1	bengálský
hladomoru	hladomor	k1gInSc3	hladomor
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
si	se	k3xPyFc3	se
vyžádal	vyžádat	k5eAaPmAgMnS	vyžádat
životy	život	k1gInPc4	život
nejméně	málo	k6eAd3	málo
2,5	[number]	k4	2,5
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Churchill	Churchill	k1gMnSc1	Churchill
také	také	k9	také
podporoval	podporovat	k5eAaImAgMnS	podporovat
kontroverzní	kontroverzní	k2eAgNnSc4d1	kontroverzní
bombardování	bombardování	k1gNnSc4	bombardování
Drážďan	Drážďany	k1gInPc2	Drážďany
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
koncem	konec	k1gInSc7	konec
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
svou	svůj	k3xOyFgFnSc4	svůj
důležitou	důležitý	k2eAgFnSc4d1	důležitá
úlohu	úloha	k1gFnSc4	úloha
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
Churchill	Churchill	k1gMnSc1	Churchill
na	na	k7c6	na
britské	britský	k2eAgFnSc6d1	britská
politické	politický	k2eAgFnSc6d1	politická
scéně	scéna	k1gFnSc6	scéna
mnoho	mnoho	k6eAd1	mnoho
nepřátel	nepřítel	k1gMnPc2	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc4	jeho
popularitu	popularita	k1gFnSc4	popularita
nezvýšily	zvýšit	k5eNaPmAgInP	zvýšit
ani	ani	k9	ani
výroky	výrok	k1gInPc1	výrok
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
znevažovaly	znevažovat	k5eAaImAgFnP	znevažovat
myšlenku	myšlenka	k1gFnSc4	myšlenka
veřejné	veřejný	k2eAgFnSc2d1	veřejná
zdravotní	zdravotní	k2eAgFnSc2d1	zdravotní
péče	péče	k1gFnSc2	péče
či	či	k8xC	či
zlepšení	zlepšení	k1gNnSc6	zlepšení
standardů	standard	k1gInPc2	standard
všeobecného	všeobecný	k2eAgNnSc2d1	všeobecné
vzdělání	vzdělání	k1gNnSc2	vzdělání
mas	masa	k1gFnPc2	masa
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
mezi	mezi	k7c7	mezi
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
pořád	pořád	k6eAd1	pořád
existovaly	existovat	k5eAaImAgFnP	existovat
vzpomínky	vzpomínka	k1gFnPc1	vzpomínka
na	na	k7c4	na
chyby	chyba	k1gFnPc4	chyba
konzervativních	konzervativní	k2eAgFnPc2d1	konzervativní
vlád	vláda	k1gFnPc2	vláda
ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
byly	být	k5eAaImAgInP	být
důvody	důvod	k1gInPc1	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
těsně	těsně	k6eAd1	těsně
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
války	válka	k1gFnSc2	válka
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1945	[number]	k4	1945
konzervativní	konzervativní	k2eAgFnSc1d1	konzervativní
strana	strana	k1gFnSc1	strana
a	a	k8xC	a
Churchill	Churchill	k1gMnSc1	Churchill
jako	jako	k8xS	jako
její	její	k3xOp3gMnPc1	její
vůdce	vůdce	k1gMnSc4	vůdce
prohráli	prohrát	k5eAaPmAgMnP	prohrát
všeobecné	všeobecný	k2eAgFnPc4d1	všeobecná
volby	volba	k1gFnPc4	volba
<g/>
.	.	kIx.	.
</s>
<s>
Zdálo	zdát	k5eAaImAgNnS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
sedmdesátiletý	sedmdesátiletý	k2eAgMnSc1d1	sedmdesátiletý
Churchill	Churchill	k1gMnSc1	Churchill
ve	v	k7c6	v
vysoké	vysoký	k2eAgFnSc6d1	vysoká
politice	politika	k1gFnSc6	politika
skončil	skončit	k5eAaPmAgMnS	skončit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
on	on	k3xPp3gMnSc1	on
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
návratu	návrat	k1gInSc6	návrat
<g/>
.	.	kIx.	.
</s>
<s>
Odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
nabízený	nabízený	k2eAgInSc4d1	nabízený
titul	titul	k1gInSc4	titul
vévody	vévoda	k1gMnSc2	vévoda
(	(	kIx(	(
<g/>
čímž	což	k3yRnSc7	což
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgInS	dostat
do	do	k7c2	do
Horní	horní	k2eAgFnSc2d1	horní
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
)	)	kIx)	)
a	a	k8xC	a
zůstal	zůstat	k5eAaPmAgMnS	zůstat
vůdcem	vůdce	k1gMnSc7	vůdce
opozice	opozice	k1gFnSc2	opozice
v	v	k7c6	v
Dolní	dolní	k2eAgFnSc6d1	dolní
sněmovně	sněmovna	k1gFnSc6	sněmovna
<g/>
.	.	kIx.	.
</s>
<s>
Přednášel	přednášet	k5eAaImAgMnS	přednášet
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
známý	známý	k2eAgInSc1d1	známý
Fultonský	Fultonský	k2eAgInSc1d1	Fultonský
projev	projev	k1gInSc1	projev
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
uvedl	uvést	k5eAaPmAgMnS	uvést
do	do	k7c2	do
veřejného	veřejný	k2eAgNnSc2d1	veřejné
povědomí	povědomí	k1gNnSc2	povědomí
původně	původně	k6eAd1	původně
Goebbelsův	Goebbelsův	k2eAgInSc1d1	Goebbelsův
pojem	pojem	k1gInSc1	pojem
železná	železný	k2eAgFnSc1d1	železná
opona	opona	k1gFnSc1	opona
<g/>
)	)	kIx)	)
a	a	k8xC	a
psal	psát	k5eAaImAgMnS	psát
(	(	kIx(	(
<g/>
např.	např.	kA	např.
šestidílnou	šestidílný	k2eAgFnSc4d1	šestidílná
historii	historie	k1gFnSc4	historie
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
za	za	k7c4	za
kterou	který	k3yIgFnSc4	který
dostal	dostat	k5eAaPmAgMnS	dostat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
také	také	k9	také
vřelým	vřelý	k2eAgMnSc7d1	vřelý
stoupencem	stoupenec	k1gMnSc7	stoupenec
evropské	evropský	k2eAgFnSc2d1	Evropská
integrace	integrace	k1gFnSc2	integrace
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
hlavních	hlavní	k2eAgFnPc2d1	hlavní
budov	budova	k1gFnPc2	budova
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
v	v	k7c6	v
Bruselu	Brusel	k1gInSc6	Brusel
nese	nést	k5eAaImIp3nS	nést
jeho	jeho	k3xOp3gNnSc4	jeho
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
Churchillovou	Churchillův	k2eAgFnSc7d1	Churchillova
nemalou	malý	k2eNgFnSc7d1	nemalá
zásluhou	zásluha	k1gFnSc7	zásluha
<g/>
,	,	kIx,	,
že	že	k8xS	že
Francie	Francie	k1gFnSc1	Francie
získala	získat	k5eAaPmAgFnS	získat
stálé	stálý	k2eAgNnSc4d1	stálé
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
Radě	rada	k1gFnSc6	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
OSN	OSN	kA	OSN
(	(	kIx(	(
<g/>
tak	tak	k6eAd1	tak
mohla	moct	k5eAaImAgFnS	moct
další	další	k2eAgFnSc1d1	další
evropská	evropský	k2eAgFnSc1d1	Evropská
mocnost	mocnost	k1gFnSc1	mocnost
vyvažovat	vyvažovat	k5eAaImF	vyvažovat
stálé	stálý	k2eAgNnSc4d1	stálé
místo	místo	k1gNnSc4	místo
SSSR	SSSR	kA	SSSR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
velké	velký	k2eAgFnSc2d1	velká
politiky	politika	k1gFnSc2	politika
se	se	k3xPyFc4	se
Winston	Winston	k1gInSc1	Winston
Churchill	Churchilla	k1gFnPc2	Churchilla
vrátil	vrátit	k5eAaPmAgInS	vrátit
ještě	ještě	k6eAd1	ještě
jednou	jeden	k4xCgFnSc7	jeden
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
jako	jako	k8xS	jako
předseda	předseda	k1gMnSc1	předseda
konzervativní	konzervativní	k2eAgFnSc2d1	konzervativní
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
získala	získat	k5eAaPmAgFnS	získat
těsnou	těsný	k2eAgFnSc4d1	těsná
většinu	většina	k1gFnSc4	většina
v	v	k7c6	v
parlamentu	parlament	k1gInSc6	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Churchill	Churchill	k1gMnSc1	Churchill
i	i	k9	i
přes	přes	k7c4	přes
doléhající	doléhající	k2eAgNnSc4d1	doléhající
stáří	stáří	k1gNnSc4	stáří
a	a	k8xC	a
zdravotní	zdravotní	k2eAgInPc4d1	zdravotní
problémy	problém	k1gInPc4	problém
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
prodělal	prodělat	k5eAaPmAgMnS	prodělat
záchvat	záchvat	k1gInSc4	záchvat
mrtvice	mrtvice	k1gFnSc2	mrtvice
<g/>
)	)	kIx)	)
během	během	k7c2	během
tohoto	tento	k3xDgNnSc2	tento
volebního	volební	k2eAgNnSc2d1	volební
období	období	k1gNnSc2	období
obnovil	obnovit	k5eAaPmAgInS	obnovit
přátelské	přátelský	k2eAgInPc4d1	přátelský
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
USA	USA	kA	USA
a	a	k8xC	a
zapojil	zapojit	k5eAaPmAgInS	zapojit
se	se	k3xPyFc4	se
do	do	k7c2	do
vytváření	vytváření	k1gNnSc2	vytváření
"	"	kIx"	"
<g/>
nové	nový	k2eAgFnSc2d1	nová
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
hlavním	hlavní	k2eAgInSc7d1	hlavní
zájmem	zájem	k1gInSc7	zájem
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
(	(	kIx(	(
<g/>
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
vývoji	vývoj	k1gInSc3	vývoj
událostí	událost	k1gFnPc2	událost
a	a	k8xC	a
množství	množství	k1gNnSc2	množství
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
krizí	krize	k1gFnPc2	krize
i	i	k9	i
musela	muset	k5eAaImAgFnS	muset
stát	stát	k1gInSc1	stát
<g/>
)	)	kIx)	)
zahraniční	zahraniční	k2eAgFnSc1d1	zahraniční
politika	politika	k1gFnSc1	politika
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
musel	muset	k5eAaImAgInS	muset
řešit	řešit	k5eAaImF	řešit
íránskou	íránský	k2eAgFnSc4d1	íránská
krizi	krize	k1gFnSc4	krize
vzniklou	vzniklý	k2eAgFnSc4d1	vzniklá
znárodněním	znárodnění	k1gNnSc7	znárodnění
Anglo-íránské	Anglo-íránský	k2eAgFnSc2d1	Anglo-íránský
ropné	ropný	k2eAgFnSc2d1	ropná
společnosti	společnost	k1gFnSc2	společnost
vládou	vláda	k1gFnSc7	vláda
Muhammada	Muhammada	k1gFnSc1	Muhammada
Mosaddeka	Mosaddeko	k1gNnPc4	Mosaddeko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
došlo	dojít	k5eAaPmAgNnS	dojít
kvůli	kvůli	k7c3	kvůli
pozemkové	pozemkový	k2eAgFnSc3d1	pozemková
reformě	reforma	k1gFnSc3	reforma
v	v	k7c6	v
Keni	Keňa	k1gFnSc6	Keňa
k	k	k7c3	k
povstání	povstání	k1gNnSc3	povstání
(	(	kIx(	(
<g/>
Povstání	povstání	k1gNnSc3	povstání
Mau	Mau	k1gFnSc2	Mau
Mau	Mau	k1gFnSc2	Mau
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
přerostlo	přerůst	k5eAaPmAgNnS	přerůst
v	v	k7c4	v
občanskou	občanský	k2eAgFnSc4d1	občanská
válku	válka	k1gFnSc4	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Malajsii	Malajsie	k1gFnSc6	Malajsie
se	se	k3xPyFc4	se
vystupňovalo	vystupňovat	k5eAaPmAgNnS	vystupňovat
povstání	povstání	k1gNnSc1	povstání
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
zde	zde	k6eAd1	zde
proti	proti	k7c3	proti
britské	britský	k2eAgFnSc3d1	britská
nadvládě	nadvláda	k1gFnSc3	nadvláda
vypuklo	vypuknout	k5eAaPmAgNnS	vypuknout
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
v	v	k7c6	v
Keni	Keňa	k1gFnSc6	Keňa
Churchill	Churchilla	k1gFnPc2	Churchilla
preferoval	preferovat	k5eAaImAgInS	preferovat
vojenský	vojenský	k2eAgInSc1d1	vojenský
zásah	zásah	k1gInSc1	zásah
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
tak	tak	k6eAd1	tak
bylo	být	k5eAaImAgNnS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
britské	britský	k2eAgNnSc1d1	Britské
koloniální	koloniální	k2eAgNnSc1d1	koloniální
panství	panství	k1gNnSc1	panství
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
se	se	k3xPyFc4	se
blíží	blížit	k5eAaImIp3nS	blížit
svému	svůj	k3xOyFgInSc3	svůj
konci	konec	k1gInSc3	konec
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
se	se	k3xPyFc4	se
Malajsie	Malajsie	k1gFnSc1	Malajsie
stala	stát	k5eAaPmAgFnS	stát
nezávislým	závislý	k2eNgInSc7d1	nezávislý
státem	stát	k1gInSc7	stát
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1953	[number]	k4	1953
Churchill	Churchill	k1gMnSc1	Churchill
prodělal	prodělat	k5eAaPmAgMnS	prodělat
další	další	k2eAgInSc4d1	další
záchvat	záchvat	k1gInSc4	záchvat
mrtvice	mrtvice	k1gFnSc2	mrtvice
<g/>
,	,	kIx,	,
27	[number]	k4	27
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
odešel	odejít	k5eAaPmAgMnS	odejít
na	na	k7c4	na
dovolenou	dovolená	k1gFnSc4	dovolená
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
Churchill	Churchill	k1gInSc1	Churchill
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
úřad	úřad	k1gInSc4	úřad
fyzicky	fyzicky	k6eAd1	fyzicky
nestačí	stačit	k5eNaBmIp3nS	stačit
a	a	k8xC	a
proto	proto	k8xC	proto
5	[number]	k4	5
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1955	[number]	k4	1955
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
z	z	k7c2	z
funkce	funkce	k1gFnSc2	funkce
premiéra	premiér	k1gMnSc2	premiér
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
místo	místo	k1gNnSc4	místo
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
Anthony	Anthona	k1gFnSc2	Anthona
Eden	Eden	k1gInSc1	Eden
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
jakožto	jakožto	k8xS	jakožto
o	o	k7c6	o
zvláštním	zvláštní	k2eAgNnSc6d1	zvláštní
protégé	protégé	k1gNnSc6	protégé
Churchilla	Churchillo	k1gNnSc2	Churchillo
dlouho	dlouho	k6eAd1	dlouho
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
.	.	kIx.	.
</s>
<s>
Churchill	Churchill	k1gMnSc1	Churchill
trávil	trávit	k5eAaImAgMnS	trávit
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
svého	svůj	k3xOyFgInSc2	svůj
času	čas	k1gInSc2	čas
v	v	k7c6	v
Chartwellu	Chartwell	k1gInSc6	Chartwell
(	(	kIx(	(
<g/>
jeho	jeho	k3xOp3gInSc1	jeho
venkovský	venkovský	k2eAgInSc1d1	venkovský
dům	dům	k1gInSc1	dům
a	a	k8xC	a
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
uchyloval	uchylovat	k5eAaImAgInS	uchylovat
k	k	k7c3	k
odpočinku	odpočinek	k1gInSc3	odpočinek
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
utrpěl	utrpět	k5eAaPmAgMnS	utrpět
zlomeninu	zlomenina	k1gFnSc4	zlomenina
stehenní	stehenní	k2eAgFnSc2d1	stehenní
kosti	kost	k1gFnSc2	kost
<g/>
,	,	kIx,	,
po	po	k7c6	po
které	který	k3yIgFnSc6	který
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
již	již	k9	již
88	[number]	k4	88
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
zotavil	zotavit	k5eAaPmAgMnS	zotavit
a	a	k8xC	a
vrátil	vrátit	k5eAaPmAgMnS	vrátit
domů	domů	k6eAd1	domů
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1965	[number]	k4	1965
měl	mít	k5eAaImAgInS	mít
další	další	k2eAgInSc1d1	další
záchvat	záchvat	k1gInSc1	záchvat
mrtvice	mrtvice	k1gFnSc2	mrtvice
<g/>
,	,	kIx,	,
tentokráte	tentokráte	k?	tentokráte
již	již	k6eAd1	již
smrtelné	smrtelný	k2eAgFnSc3d1	smrtelná
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
o	o	k7c4	o
devět	devět	k4xCc4	devět
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
24	[number]	k4	24
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1965	[number]	k4	1965
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
tělo	tělo	k1gNnSc1	tělo
bylo	být	k5eAaImAgNnS	být
po	po	k7c4	po
tři	tři	k4xCgInPc4	tři
dny	den	k1gInPc4	den
vystaveno	vystaven	k2eAgNnSc1d1	vystaveno
ve	v	k7c6	v
Westminsteru	Westminster	k1gInSc6	Westminster
<g/>
,	,	kIx,	,
obřad	obřad	k1gInSc1	obřad
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
katedrále	katedrála	k1gFnSc6	katedrála
sv.	sv.	kA	sv.
Pavla	Pavel	k1gMnSc2	Pavel
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
pohřeb	pohřeb	k1gInSc1	pohřeb
byl	být	k5eAaImAgInS	být
prvním	první	k4xOgInSc7	první
státním	státní	k2eAgInSc7d1	státní
pohřbem	pohřeb	k1gInSc7	pohřeb
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnPc3	který
byl	být	k5eAaImAgInS	být
vyznamenán	vyznamenán	k2eAgMnSc1d1	vyznamenán
nečlen	nečlen	k1gMnSc1	nečlen
královské	královský	k2eAgFnSc2d1	královská
rodiny	rodina	k1gFnSc2	rodina
od	od	k7c2	od
smrti	smrt	k1gFnSc2	smrt
polního	polní	k2eAgMnSc4d1	polní
maršála	maršál	k1gMnSc4	maršál
lorda	lord	k1gMnSc4	lord
Robertse	Roberts	k1gMnSc4	Roberts
z	z	k7c2	z
Kandaharu	Kandahar	k1gInSc2	Kandahar
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1914	[number]	k4	1914
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
pohřeb	pohřeb	k1gInSc1	pohřeb
30	[number]	k4	30
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
se	se	k3xPyFc4	se
zároveň	zároveň	k6eAd1	zároveň
stal	stát	k5eAaPmAgInS	stát
přehlídkou	přehlídka	k1gFnSc7	přehlídka
světových	světový	k2eAgFnPc2d1	světová
politických	politický	k2eAgFnPc2d1	politická
osobností	osobnost	k1gFnPc2	osobnost
<g/>
,	,	kIx,	,
překonanou	překonaný	k2eAgFnSc4d1	překonaná
až	až	k8xS	až
pohřbem	pohřeb	k1gInSc7	pohřeb
papeže	papež	k1gMnSc2	papež
Jana	Jan	k1gMnSc2	Jan
Pavla	Pavel	k1gMnSc2	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
Churchillovým	Churchillův	k2eAgNnPc3d1	Churchillovo
přáním	přání	k1gNnPc3	přání
být	být	k5eAaImF	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
v	v	k7c6	v
Bladonu	Bladon	k1gInSc6	Bladon
<g/>
,	,	kIx,	,
blízko	blízko	k7c2	blízko
Woodstocku	Woodstock	k1gInSc2	Woodstock
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
nebyl	být	k5eNaImAgInS	být
příliš	příliš	k6eAd1	příliš
vzdálen	vzdálit	k5eAaPmNgInS	vzdálit
od	od	k7c2	od
místa	místo	k1gNnSc2	místo
jeho	jeho	k3xOp3gNnSc2	jeho
narození	narození	k1gNnSc2	narození
Blenheimu	Blenheima	k1gFnSc4	Blenheima
<g/>
.	.	kIx.	.
</s>
<s>
Winston	Winston	k1gInSc1	Winston
Churchill	Churchill	k1gInSc1	Churchill
byl	být	k5eAaImAgInS	být
již	již	k6eAd1	již
od	od	k7c2	od
mládí	mládí	k1gNnSc4	mládí
plodným	plodný	k2eAgMnSc7d1	plodný
spisovatelem	spisovatel	k1gMnSc7	spisovatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obdobích	období	k1gNnPc6	období
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nezastával	zastávat	k5eNaImAgInS	zastávat
žádný	žádný	k3yNgInSc4	žádný
politický	politický	k2eAgInSc4d1	politický
post	post	k1gInSc4	post
<g/>
,	,	kIx,	,
považoval	považovat	k5eAaImAgMnS	považovat
sám	sám	k3xTgMnSc1	sám
sebe	sebe	k3xPyFc4	sebe
za	za	k7c2	za
profesionálního	profesionální	k2eAgMnSc2d1	profesionální
spisovatele	spisovatel	k1gMnSc2	spisovatel
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
také	také	k9	také
zabývá	zabývat	k5eAaImIp3nS	zabývat
politikou	politika	k1gFnSc7	politika
<g/>
.	.	kIx.	.
</s>
<s>
Psaní	psaní	k1gNnSc1	psaní
mu	on	k3xPp3gMnSc3	on
také	také	k9	také
poskytovalo	poskytovat	k5eAaImAgNnS	poskytovat
nutné	nutný	k2eAgNnSc1d1	nutné
finanční	finanční	k2eAgInPc1d1	finanční
prostředky	prostředek	k1gInPc1	prostředek
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
i	i	k9	i
přes	přes	k7c4	přes
svůj	svůj	k3xOyFgInSc4	svůj
aristokratický	aristokratický	k2eAgInSc4d1	aristokratický
původ	původ	k1gInSc4	původ
nebyl	být	k5eNaImAgInS	být
bohatý	bohatý	k2eAgMnSc1d1	bohatý
a	a	k8xC	a
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
byl	být	k5eAaImAgInS	být
zvyklý	zvyklý	k2eAgInSc1d1	zvyklý
na	na	k7c4	na
určitý	určitý	k2eAgInSc4d1	určitý
životní	životní	k2eAgInSc4d1	životní
standard	standard	k1gInSc4	standard
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc4	jeho
finanční	finanční	k2eAgFnSc4d1	finanční
situaci	situace	k1gFnSc4	situace
také	také	k9	také
nepomohly	pomoct	k5eNaPmAgFnP	pomoct
příliš	příliš	k6eAd1	příliš
časté	častý	k2eAgFnPc1d1	častá
neúspěšné	úspěšný	k2eNgFnPc1d1	neúspěšná
investice	investice	k1gFnPc1	investice
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgFnSc1	některý
jeho	jeho	k3xOp3gNnPc4	jeho
díla	dílo	k1gNnPc4	dílo
tak	tak	k9	tak
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
i	i	k9	i
kvůli	kvůli	k7c3	kvůli
tíživé	tíživý	k2eAgFnSc3d1	tíživá
finanční	finanční	k2eAgFnSc3d1	finanční
situaci	situace	k1gFnSc3	situace
<g/>
.	.	kIx.	.
</s>
<s>
Psal	psát	k5eAaImAgMnS	psát
i	i	k9	i
historická	historický	k2eAgNnPc1d1	historické
díla	dílo	k1gNnPc1	dílo
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yIgInPc6	který
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
vidět	vidět	k5eAaImF	vidět
<g/>
,	,	kIx,	,
že	že	k8xS	že
nebyl	být	k5eNaImAgMnS	být
profesionálním	profesionální	k2eAgMnSc7d1	profesionální
historikem	historik	k1gMnSc7	historik
(	(	kIx(	(
<g/>
např.	např.	kA	např.
ho	on	k3xPp3gInSc2	on
nezajímaly	zajímat	k5eNaImAgFnP	zajímat
sociální	sociální	k2eAgFnPc1d1	sociální
nebo	nebo	k8xC	nebo
ekonomické	ekonomický	k2eAgFnPc1d1	ekonomická
dějiny	dějiny	k1gFnPc1	dějiny
<g/>
,	,	kIx,	,
vlastně	vlastně	k9	vlastně
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
hlavně	hlavně	k9	hlavně
o	o	k7c4	o
politické	politický	k2eAgFnPc4d1	politická
a	a	k8xC	a
vojenské	vojenský	k2eAgFnPc4d1	vojenská
dějiny	dějiny	k1gFnPc4	dějiny
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Angličané	Angličan	k1gMnPc1	Angličan
hráli	hrát	k5eAaImAgMnP	hrát
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
svého	svůj	k3xOyFgNnSc2	svůj
stěžejního	stěžejní	k2eAgNnSc2d1	stěžejní
šestisvazkového	šestisvazkový	k2eAgNnSc2d1	šestisvazkové
historického	historický	k2eAgNnSc2d1	historické
díla	dílo	k1gNnSc2	dílo
Druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
udělena	udělen	k2eAgFnSc1d1	udělena
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
"	"	kIx"	"
<g/>
za	za	k7c2	za
jeho	on	k3xPp3gNnSc2	on
mistrovství	mistrovství	k1gNnSc2	mistrovství
v	v	k7c6	v
historickém	historický	k2eAgNnSc6d1	historické
a	a	k8xC	a
biografickém	biografický	k2eAgNnSc6d1	biografické
líčení	líčení	k1gNnSc6	líčení
i	i	k9	i
za	za	k7c4	za
jiskřivé	jiskřivý	k2eAgNnSc4d1	jiskřivé
umění	umění	k1gNnSc4	umění
řečnické	řečnický	k2eAgNnSc1d1	řečnické
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgNnSc7	jenž
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
jako	jako	k9	jako
obránce	obránce	k1gMnSc1	obránce
vznešených	vznešený	k2eAgFnPc2d1	vznešená
lidských	lidský	k2eAgFnPc2d1	lidská
hodnot	hodnota	k1gFnPc2	hodnota
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
citace	citace	k1gFnPc1	citace
z	z	k7c2	z
odůvodnění	odůvodnění	k1gNnSc2	odůvodnění
Švédské	švédský	k2eAgFnSc2d1	švédská
akademie	akademie	k1gFnSc2	akademie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Churchill	Churchill	k1gMnSc1	Churchill
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stal	stát	k5eAaPmAgMnS	stát
po	po	k7c6	po
Theodoru	Theodor	k1gMnSc6	Theodor
Mommsenovi	Mommsen	k1gMnSc6	Mommsen
<g/>
,	,	kIx,	,
Rudolfu	Rudolf	k1gMnSc6	Rudolf
Euckenovi	Eucken	k1gMnSc6	Eucken
<g/>
,	,	kIx,	,
Henri	Henr	k1gMnSc6	Henr
Bergsonovi	Bergson	k1gMnSc6	Bergson
a	a	k8xC	a
Bertrandu	Bertrand	k1gInSc3	Bertrand
Russellovi	Russellův	k2eAgMnPc1d1	Russellův
pátým	pátý	k4xOgNnSc7	pátý
(	(	kIx(	(
<g/>
a	a	k8xC	a
zatím	zatím	k6eAd1	zatím
posledním	poslední	k2eAgMnSc7d1	poslední
<g/>
)	)	kIx)	)
nositelem	nositel	k1gMnSc7	nositel
této	tento	k3xDgFnSc2	tento
ceny	cena	k1gFnSc2	cena
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
nebyl	být	k5eNaImAgMnS	být
spisovatelem	spisovatel	k1gMnSc7	spisovatel
krásné	krásný	k2eAgFnSc2d1	krásná
literatury	literatura	k1gFnSc2	literatura
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jiných	jiný	k2eAgInPc2d1	jiný
spisů	spis	k1gInPc2	spis
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
svou	svůj	k3xOyFgFnSc7	svůj
formou	forma	k1gFnSc7	forma
a	a	k8xC	a
pojetím	pojetí	k1gNnSc7	pojetí
mají	mít	k5eAaImIp3nP	mít
literární	literární	k2eAgFnSc4d1	literární
hodnotu	hodnota	k1gFnSc4	hodnota
(	(	kIx(	(
<g/>
jak	jak	k8xS	jak
to	ten	k3xDgNnSc1	ten
stojí	stát	k5eAaImIp3nS	stát
v	v	k7c6	v
příslušných	příslušný	k2eAgFnPc6d1	příslušná
stanovách	stanova	k1gFnPc6	stanova
k	k	k7c3	k
Nobelově	Nobelův	k2eAgFnSc3d1	Nobelova
ceně	cena	k1gFnSc3	cena
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
literární	literární	k2eAgFnPc1d1	literární
práce	práce	k1gFnPc1	práce
lze	lze	k6eAd1	lze
rozdělit	rozdělit	k5eAaPmF	rozdělit
do	do	k7c2	do
pěti	pět	k4xCc2	pět
skupin	skupina	k1gFnPc2	skupina
<g/>
:	:	kIx,	:
Práce	práce	k1gFnSc1	práce
týkající	týkající	k2eAgFnSc1d1	týkající
se	se	k3xPyFc4	se
dějin	dějiny	k1gFnPc2	dějiny
jeho	jeho	k3xOp3gFnSc2	jeho
rodiny	rodina	k1gFnSc2	rodina
(	(	kIx(	(
<g/>
ne	ne	k9	ne
vždy	vždy	k6eAd1	vždy
zcela	zcela	k6eAd1	zcela
objektivní	objektivní	k2eAgMnSc1d1	objektivní
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Life	Life	k1gNnSc1	Life
of	of	k?	of
Lord	lord	k1gMnSc1	lord
Randolph	Randolph	k1gMnSc1	Randolph
Churchill	Churchill	k1gMnSc1	Churchill
(	(	kIx(	(
<g/>
1906	[number]	k4	1906
<g/>
,	,	kIx,	,
Život	život	k1gInSc4	život
lorda	lord	k1gMnSc2	lord
Randolpha	Randolph	k1gMnSc2	Randolph
Churchilla	Churchill	k1gMnSc2	Churchill
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Marlborough	Marlborough	k1gInSc1	Marlborough
<g/>
:	:	kIx,	:
His	his	k1gNnSc1	his
Life	Lif	k1gInSc2	Lif
and	and	k?	and
Times	Times	k1gMnSc1	Times
(	(	kIx(	(
<g/>
1933	[number]	k4	1933
<g/>
-	-	kIx~	-
<g/>
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
Marlborough	Marlborough	k1gInSc1	Marlborough
<g/>
:	:	kIx,	:
život	život	k1gInSc1	život
a	a	k8xC	a
doba	doba	k1gFnSc1	doba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čtyři	čtyři	k4xCgInPc4	čtyři
svazky	svazek	k1gInPc4	svazek
<g/>
.	.	kIx.	.
</s>
<s>
Autobiografické	autobiografický	k2eAgFnPc1d1	autobiografická
práce	práce	k1gFnPc1	práce
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Story	story	k1gFnSc2	story
of	of	k?	of
the	the	k?	the
Malakand	Malakand	k1gInSc1	Malakand
Field	Field	k1gInSc1	Field
Force	force	k1gFnSc1	force
(	(	kIx(	(
<g/>
1898	[number]	k4	1898
<g/>
,	,	kIx,	,
Příběh	příběh	k1gInSc1	příběh
malakandského	malakandský	k2eAgInSc2d1	malakandský
sboru	sbor	k1gInSc2	sbor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vzrušující	vzrušující	k2eAgFnSc1d1	vzrušující
zpráva	zpráva	k1gFnSc1	zpráva
tehdy	tehdy	k6eAd1	tehdy
začínajícího	začínající	k2eAgMnSc2d1	začínající
dopisovatele	dopisovatel	k1gMnSc2	dopisovatel
listu	list	k1gInSc2	list
Daily	Daila	k1gFnSc2	Daila
Telegraph	Telegrapha	k1gFnPc2	Telegrapha
o	o	k7c6	o
boji	boj	k1gInSc6	boj
britské	britský	k2eAgFnSc2d1	britská
posádky	posádka	k1gFnSc2	posádka
proti	proti	k7c3	proti
indickým	indický	k2eAgMnPc3d1	indický
vzbouřencům	vzbouřenec	k1gMnPc3	vzbouřenec
z	z	k7c2	z
kmene	kmen	k1gInSc2	kmen
Čitralů	Čitral	k1gMnPc2	Čitral
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1895	[number]	k4	1895
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
River	River	k1gMnSc1	River
War	War	k1gMnSc1	War
(	(	kIx(	(
<g/>
1899	[number]	k4	1899
<g/>
,	,	kIx,	,
Říční	říční	k2eAgFnSc1d1	říční
válka	válka	k1gFnSc1	válka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
popis	popis	k1gInSc4	popis
dobytí	dobytí	k1gNnSc4	dobytí
Súdánu	Súdán	k1gInSc2	Súdán
spojenými	spojený	k2eAgInPc7d1	spojený
britsko-egyptskými	britskogyptský	k2eAgInPc7d1	britsko-egyptský
vojsky	vojsko	k1gNnPc7	vojsko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1898	[number]	k4	1898
<g/>
,	,	kIx,	,
kterého	který	k3yIgNnSc2	který
se	se	k3xPyFc4	se
jako	jako	k9	jako
voják	voják	k1gMnSc1	voják
aktivně	aktivně	k6eAd1	aktivně
účastnil	účastnit	k5eAaImAgMnS	účastnit
(	(	kIx(	(
<g/>
popis	popis	k1gInSc1	popis
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
válečných	válečný	k2eAgFnPc2d1	válečná
operací	operace	k1gFnPc2	operace
na	na	k7c6	na
Horním	horní	k2eAgInSc6d1	horní
Nilu	Nil	k1gInSc6	Nil
-	-	kIx~	-
odtud	odtud	k6eAd1	odtud
název	název	k1gInSc1	název
knihy	kniha	k1gFnSc2	kniha
<g/>
)	)	kIx)	)
My	my	k3xPp1nPc1	my
Early	earl	k1gMnPc4	earl
Life	Life	k1gFnPc2	Life
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
Mé	můj	k3xOp1gInPc1	můj
životní	životní	k2eAgInPc1d1	životní
začátky	začátek	k1gInPc1	začátek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kniha	kniha	k1gFnSc1	kniha
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
jako	jako	k9	jako
kompilace	kompilace	k1gFnPc4	kompilace
jeho	jeho	k3xOp3gFnPc2	jeho
dvou	dva	k4xCgFnPc2	dva
dřívějších	dřívější	k2eAgFnPc2d1	dřívější
autobiografických	autobiografický	k2eAgFnPc2d1	autobiografická
prací	práce	k1gFnPc2	práce
London	London	k1gMnSc1	London
to	ten	k3xDgNnSc1	ten
Ladysmith	Ladysmith	k1gInSc1	Ladysmith
via	via	k7c4	via
Pretoria	Pretorium	k1gNnPc4	Pretorium
a	a	k8xC	a
Ian	Ian	k1gFnSc1	Ian
Hamilton	Hamilton	k1gInSc1	Hamilton
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
March	March	k1gInSc1	March
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
<g/>
.	.	kIx.	.
</s>
<s>
Válečné	válečný	k2eAgInPc1d1	válečný
projevy	projev	k1gInPc1	projev
<g/>
:	:	kIx,	:
Into	Into	k1gNnSc1	Into
Battle	Battle	k1gFnSc2	Battle
(	(	kIx(	(
<g/>
1941	[number]	k4	1941
<g/>
,	,	kIx,	,
Do	do	k7c2	do
boje	boj	k1gInSc2	boj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Unrelenting	Unrelenting	k1gInSc1	Unrelenting
Struggle	Struggle	k1gFnSc1	Struggle
(	(	kIx(	(
<g/>
1942	[number]	k4	1942
<g/>
,	,	kIx,	,
Nelítostný	lítostný	k2eNgInSc1d1	nelítostný
zápas	zápas	k1gInSc1	zápas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
End	End	k1gFnSc2	End
of	of	k?	of
the	the	k?	the
Beginning	Beginning	k1gInSc1	Beginning
(	(	kIx(	(
<g/>
1943	[number]	k4	1943
<g/>
,	,	kIx,	,
Konec	konec	k1gInSc1	konec
začátku	začátek	k1gInSc2	začátek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Onwards	Onwards	k1gInSc1	Onwards
to	ten	k3xDgNnSc1	ten
Victory	Victor	k1gMnPc7	Victor
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
1943	[number]	k4	1943
<g/>
,	,	kIx,	,
Vzhůru	vzhůru	k6eAd1	vzhůru
k	k	k7c3	k
vítězství	vítězství	k1gNnSc3	vítězství
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Dawn	Dawn	k1gMnSc1	Dawn
of	of	k?	of
Liberation	Liberation	k1gInSc1	Liberation
(	(	kIx(	(
<g/>
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
Červánky	červánek	k1gInPc1	červánek
osvobození	osvobození	k1gNnSc2	osvobození
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Victory	Victor	k1gMnPc4	Victor
<g/>
,	,	kIx,	,
Secret	Secret	k1gInSc1	Secret
session	session	k1gInSc1	session
speeches	speeches	k1gInSc1	speeches
(	(	kIx(	(
<g/>
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
Vítězství	vítězství	k1gNnSc1	vítězství
<g/>
,	,	kIx,	,
Projevy	projev	k1gInPc1	projev
v	v	k7c6	v
tajných	tajný	k2eAgFnPc6d1	tajná
schůzích	schůze	k1gFnPc6	schůze
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Historická	historický	k2eAgNnPc1d1	historické
díla	dílo	k1gNnPc1	dílo
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
World	World	k1gInSc1	World
Crisis	Crisis	k1gFnSc1	Crisis
1911	[number]	k4	1911
<g/>
-	-	kIx~	-
<g/>
1918	[number]	k4	1918
(	(	kIx(	(
<g/>
1923	[number]	k4	1923
<g/>
-	-	kIx~	-
<g/>
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
šest	šest	k4xCc4	šest
svazků	svazek	k1gInPc2	svazek
(	(	kIx(	(
<g/>
první	první	k4xOgFnSc4	první
dva	dva	k4xCgInPc4	dva
díly	díl	k1gInPc1	díl
vyšly	vyjít	k5eAaPmAgInP	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
<g/>
,	,	kIx,	,
další	další	k2eAgInPc4d1	další
dva	dva	k4xCgInPc4	dva
1927	[number]	k4	1927
<g/>
,	,	kIx,	,
pátý	pátý	k4xOgMnSc1	pátý
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
a	a	k8xC	a
šestý	šestý	k4xOgMnSc1	šestý
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dílo	dílo	k1gNnSc4	dílo
zabývající	zabývající	k2eAgNnPc4d1	zabývající
se	s	k7c7	s
první	první	k4xOgFnSc7	první
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Second	Second	k1gMnSc1	Second
World	World	k1gMnSc1	World
War	War	k1gMnSc1	War
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
<g/>
-	-	kIx~	-
<g/>
1953	[number]	k4	1953
<g/>
,	,	kIx,	,
Druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
šest	šest	k4xCc4	šest
svazků	svazek	k1gInPc2	svazek
<g/>
,	,	kIx,	,
právě	právě	k9	právě
za	za	k7c4	za
toto	tento	k3xDgNnSc4	tento
dílo	dílo	k1gNnSc4	dílo
obdržel	obdržet	k5eAaPmAgMnS	obdržet
Churchil	Churchil	k1gMnSc1	Churchil
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
<g/>
.	.	kIx.	.
</s>
<s>
History	Histor	k1gInPc1	Histor
of	of	k?	of
the	the	k?	the
English-Speaking	English-Speaking	k1gInSc1	English-Speaking
Peoples	Peoples	k1gInSc1	Peoples
(	(	kIx(	(
<g/>
1956	[number]	k4	1956
<g/>
-	-	kIx~	-
<g/>
1958	[number]	k4	1958
<g/>
,	,	kIx,	,
Dějiny	dějiny	k1gFnPc1	dějiny
anglicky	anglicky	k6eAd1	anglicky
mluvících	mluvící	k2eAgInPc2d1	mluvící
národů	národ	k1gInPc2	národ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čtyři	čtyři	k4xCgInPc4	čtyři
svazky	svazek	k1gInPc4	svazek
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Great	Great	k2eAgMnSc1d1	Great
Battles	Battles	k1gMnSc1	Battles
and	and	k?	and
Leaders	Leaders	k1gInSc1	Leaders
of	of	k?	of
the	the	k?	the
Second	Second	k1gMnSc1	Second
World	World	k1gMnSc1	World
War	War	k1gMnSc1	War
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
Velké	velký	k2eAgFnSc2d1	velká
bitvy	bitva	k1gFnSc2	bitva
a	a	k8xC	a
osobnosti	osobnost	k1gFnSc2	osobnost
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
posmrtně	posmrtně	k6eAd1	posmrtně
<g/>
.	.	kIx.	.
</s>
<s>
Eseje	esej	k1gInPc1	esej
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Collected	Collected	k1gMnSc1	Collected
Essays	Essaysa	k1gFnPc2	Essaysa
of	of	k?	of
Sir	sir	k1gMnSc1	sir
Winston	Winston	k1gInSc1	Winston
Churchill	Churchill	k1gMnSc1	Churchill
(	(	kIx(	(
<g/>
1976	[number]	k4	1976
<g/>
,	,	kIx,	,
Sebrané	sebraný	k2eAgFnSc2d1	sebraná
eseje	esej	k1gFnSc2	esej
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
posmrtně	posmrtně	k6eAd1	posmrtně
<g/>
.	.	kIx.	.
</s>
<s>
Světová	světový	k2eAgFnSc1d1	světová
krize	krize	k1gFnSc1	krize
1911	[number]	k4	1911
<g/>
-	-	kIx~	-
<g/>
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
Melantrich	Melantrich	k1gInSc1	Melantrich
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1932	[number]	k4	1932
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
V.J.	V.J.	k1gMnSc1	V.J.
Hauner	Hauner	k1gMnSc1	Hauner
<g/>
,	,	kIx,	,
tři	tři	k4xCgInPc4	tři
díly	díl	k1gInPc4	díl
<g/>
:	:	kIx,	:
1911	[number]	k4	1911
<g/>
-	-	kIx~	-
<g/>
14	[number]	k4	14
<g/>
,	,	kIx,	,
1915	[number]	k4	1915
a	a	k8xC	a
1916	[number]	k4	1916
<g/>
-	-	kIx~	-
<g/>
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
boje	boj	k1gInSc2	boj
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Borový	borový	k2eAgMnSc1d1	borový
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Leo	Leo	k1gMnSc1	Leo
Braun	Braun	k1gMnSc1	Braun
<g/>
,	,	kIx,	,
první	první	k4xOgInSc4	první
svazek	svazek	k1gInSc4	svazek
válečných	válečný	k2eAgInPc2d1	válečný
projevů	projev	k1gInPc2	projev
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
1947	[number]	k4	1947
<g/>
.	.	kIx.	.
</s>
<s>
Nelítostný	lítostný	k2eNgInSc1d1	nelítostný
zápas	zápas	k1gInSc1	zápas
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Borový	borový	k2eAgMnSc1d1	borový
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Kolářík	Kolářík	k1gMnSc1	Kolářík
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc4	druhý
svazek	svazek	k1gInSc4	svazek
válečných	válečný	k2eAgInPc2d1	válečný
projevů	projev	k1gInPc2	projev
<g/>
.	.	kIx.	.
</s>
<s>
Konec	konec	k1gInSc1	konec
začátku	začátek	k1gInSc2	začátek
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Borový	borový	k2eAgMnSc1d1	borový
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Kolářík	Kolářík	k1gMnSc1	Kolářík
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgInSc4	třetí
svazek	svazek	k1gInSc4	svazek
válečných	válečný	k2eAgInPc2d1	válečný
projevů	projev	k1gInPc2	projev
<g/>
.	.	kIx.	.
</s>
<s>
Vzhůru	vzhůru	k6eAd1	vzhůru
k	k	k7c3	k
vítězství	vítězství	k1gNnSc3	vítězství
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Borový	borový	k2eAgMnSc1d1	borový
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Kolářík	Kolářík	k1gMnSc1	Kolářík
<g/>
,	,	kIx,	,
čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
svazek	svazek	k1gInSc4	svazek
válečných	válečný	k2eAgInPc2d1	válečný
projevů	projev	k1gInPc2	projev
<g/>
.	.	kIx.	.
</s>
<s>
Červánky	červánek	k1gInPc1	červánek
osvobození	osvobození	k1gNnSc2	osvobození
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Borový	borový	k2eAgMnSc1d1	borový
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Kolářík	Kolářík	k1gMnSc1	Kolářík
<g/>
,	,	kIx,	,
pátý	pátý	k4xOgInSc4	pátý
svazek	svazek	k1gInSc4	svazek
válečných	válečný	k2eAgInPc2d1	válečný
projevů	projev	k1gInPc2	projev
<g/>
.	.	kIx.	.
</s>
<s>
Vítězství	vítězství	k1gNnSc1	vítězství
<g/>
,	,	kIx,	,
Projevy	projev	k1gInPc1	projev
v	v	k7c6	v
tajných	tajný	k2eAgFnPc6d1	tajná
schůzích	schůze	k1gFnPc6	schůze
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Borový	borový	k2eAgMnSc1d1	borový
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Kolářík	Kolářík	k1gMnSc1	Kolářík
<g/>
,	,	kIx,	,
šestý	šestý	k4xOgInSc4	šestý
svazek	svazek	k1gInSc4	svazek
válečných	válečný	k2eAgInPc2d1	válečný
projevů	projev	k1gInPc2	projev
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
I.	I.	kA	I.
-	-	kIx~	-
Blížící	blížící	k2eAgFnSc2d1	blížící
se	se	k3xPyFc4	se
bouře	bouř	k1gFnSc2	bouř
<g/>
,	,	kIx,	,
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Hron	Hron	k1gMnSc1	Hron
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
II	II	kA	II
<g/>
.	.	kIx.	.
-	-	kIx~	-
Jejich	jejich	k3xOp3gFnSc1	jejich
nejskvělejší	skvělý	k2eAgFnSc1d3	nejskvělejší
hodina	hodina	k1gFnSc1	hodina
<g/>
,	,	kIx,	,
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Hron	Hron	k1gMnSc1	Hron
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
III	III	kA	III
<g/>
.	.	kIx.	.
-	-	kIx~	-
Velká	velký	k2eAgFnSc1d1	velká
aliance	aliance	k1gFnSc1	aliance
<g/>
,	,	kIx,	,
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Hron	Hron	k1gMnSc1	Hron
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
IV	IV	kA	IV
<g/>
.	.	kIx.	.
-	-	kIx~	-
Karta	karta	k1gFnSc1	karta
se	se	k3xPyFc4	se
obrací	obracet	k5eAaImIp3nS	obracet
<g/>
,	,	kIx,	,
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Hron	Hron	k1gMnSc1	Hron
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
V.	V.	kA	V.
-	-	kIx~	-
Kruh	kruh	k1gInSc1	kruh
se	se	k3xPyFc4	se
uzavírá	uzavírat	k5eAaPmIp3nS	uzavírat
<g/>
,	,	kIx,	,
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Hron	Hron	k1gMnSc1	Hron
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
VI	VI	kA	VI
<g/>
.	.	kIx.	.
-	-	kIx~	-
Triumf	triumf	k1gInSc1	triumf
a	a	k8xC	a
tragédie	tragédie	k1gFnSc1	tragédie
<g/>
,	,	kIx,	,
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Hron	Hron	k1gMnSc1	Hron
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Mé	můj	k3xOp1gInPc1	můj
životní	životní	k2eAgInPc1d1	životní
začátky	začátek	k1gInPc1	začátek
<g/>
,	,	kIx,	,
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Hron	Hron	k1gMnSc1	Hron
<g/>
,	,	kIx,	,
Dějiny	dějiny	k1gFnPc1	dějiny
anglicky	anglicky	k6eAd1	anglicky
mluvících	mluvící	k2eAgInPc2d1	mluvící
národů	národ	k1gInPc2	národ
I.	I.	kA	I.
-	-	kIx~	-
Zrození	zrození	k1gNnSc1	zrození
Británie	Británie	k1gFnSc2	Británie
<g/>
,	,	kIx,	,
Český	český	k2eAgMnSc1d1	český
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
přeložily	přeložit	k5eAaPmAgFnP	přeložit
Radka	Radka	k1gFnSc1	Radka
Edererová	Edererová	k1gFnSc1	Edererová
a	a	k8xC	a
Eva	Eva	k1gFnSc1	Eva
Křístková	Křístková	k1gFnSc1	Křístková
<g/>
,	,	kIx,	,
Příběh	příběh	k1gInSc1	příběh
malakandského	malakandský	k2eAgInSc2d1	malakandský
sboru	sbor	k1gInSc2	sbor
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Jota	jota	k1gNnSc1	jota
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Josef	Josef	k1gMnSc1	Josef
Zummer	Zummer	k1gMnSc1	Zummer
<g/>
,	,	kIx,	,
Dějiny	dějiny	k1gFnPc1	dějiny
anglicky	anglicky	k6eAd1	anglicky
mluvících	mluvící	k2eAgInPc2d1	mluvící
národů	národ	k1gInPc2	národ
II	II	kA	II
<g/>
.	.	kIx.	.
-	-	kIx~	-
Nový	nový	k2eAgInSc1d1	nový
svět	svět	k1gInSc1	svět
<g/>
,	,	kIx,	,
Argo	Argo	k1gMnSc1	Argo
a	a	k8xC	a
Český	český	k2eAgMnSc1d1	český
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
přeložily	přeložit	k5eAaPmAgFnP	přeložit
Radka	Radka	k1gFnSc1	Radka
Edererová	Edererová	k1gFnSc1	Edererová
a	a	k8xC	a
Eva	Eva	k1gFnSc1	Eva
Křístková	Křístková	k1gFnSc1	Křístková
<g/>
,	,	kIx,	,
Dějiny	dějiny	k1gFnPc1	dějiny
anglicky	anglicky	k6eAd1	anglicky
mluvících	mluvící	k2eAgInPc2d1	mluvící
národů	národ	k1gInPc2	národ
III	III	kA	III
<g/>
.	.	kIx.	.
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
Věk	věk	k1gInSc1	věk
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
,	,	kIx,	,
Argo	Argo	k1gMnSc1	Argo
a	a	k8xC	a
Český	český	k2eAgMnSc1d1	český
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
přeložily	přeložit	k5eAaPmAgFnP	přeložit
Radka	Radka	k1gFnSc1	Radka
Edererová	Edererová	k1gFnSc1	Edererová
a	a	k8xC	a
Eva	Eva	k1gFnSc1	Eva
Křístková	Křístková	k1gFnSc1	Křístková
<g/>
,	,	kIx,	,
Dějiny	dějiny	k1gFnPc1	dějiny
anglicky	anglicky	k6eAd1	anglicky
mluvících	mluvící	k2eAgInPc2d1	mluvící
národů	národ	k1gInPc2	národ
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
-	-	kIx~	-
Velké	velký	k2eAgFnSc2d1	velká
demokracie	demokracie	k1gFnSc2	demokracie
<g/>
,	,	kIx,	,
Argo	Argo	k1gMnSc1	Argo
a	a	k8xC	a
Český	český	k2eAgMnSc1d1	český
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
přeložily	přeložit	k5eAaPmAgFnP	přeložit
Radka	Radka	k1gFnSc1	Radka
Edererová	Edererová	k1gFnSc1	Edererová
a	a	k8xC	a
Eva	Eva	k1gFnSc1	Eva
Křístková	Křístková	k1gFnSc1	Křístková
<g/>
,	,	kIx,	,
Velké	velký	k2eAgFnSc2d1	velká
bitvy	bitva	k1gFnSc2	bitva
a	a	k8xC	a
osobnosti	osobnost	k1gFnSc2	osobnost
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
Knižní	knižní	k2eAgInSc1d1	knižní
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Jiří	Jiří	k1gMnSc1	Jiří
Bumbálek	bumbálek	k1gMnSc1	bumbálek
<g/>
.	.	kIx.	.
</s>
<s>
Winston	Winston	k1gInSc1	Winston
Churchill	Churchill	k1gMnSc1	Churchill
byl	být	k5eAaImAgMnS	být
držitelem	držitel	k1gMnSc7	držitel
řady	řada	k1gFnSc2	řada
vojenských	vojenský	k2eAgFnPc2d1	vojenská
hodností	hodnost	k1gFnPc2	hodnost
<g/>
,	,	kIx,	,
civilních	civilní	k2eAgNnPc2d1	civilní
i	i	k8xC	i
politických	politický	k2eAgNnPc2d1	politické
ocenění	ocenění	k1gNnPc2	ocenění
<g/>
,	,	kIx,	,
čestných	čestný	k2eAgMnPc2d1	čestný
doktorátů	doktorát	k1gInPc2	doktorát
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
největší	veliký	k2eAgFnSc4d3	veliký
poctu	pocta	k1gFnSc4	pocta
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
vykonání	vykonání	k1gNnSc1	vykonání
státního	státní	k2eAgInSc2d1	státní
pohřbu	pohřeb	k1gInSc2	pohřeb
v	v	k7c6	v
Katedrále	katedrála	k1gFnSc6	katedrála
sv.	sv.	kA	sv.
Pavla	Pavel	k1gMnSc4	Pavel
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
jeho	jeho	k3xOp3gFnSc1	jeho
rakev	rakev	k1gFnSc1	rakev
byla	být	k5eAaImAgFnS	být
po	po	k7c4	po
tři	tři	k4xCgInPc4	tři
dny	den	k1gInPc4	den
vystavena	vystaven	k2eAgFnSc1d1	vystavena
ve	v	k7c4	v
Westminster	Westminster	k1gInSc4	Westminster
Hall	Halla	k1gFnPc2	Halla
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
obřad	obřad	k1gInSc1	obřad
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
koná	konat	k5eAaImIp3nS	konat
jen	jen	k9	jen
pro	pro	k7c4	pro
členy	člen	k1gMnPc4	člen
královské	královský	k2eAgFnSc2d1	královská
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
obdržel	obdržet	k5eAaPmAgMnS	obdržet
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
také	také	k6eAd1	také
nominován	nominovat	k5eAaBmNgMnS	nominovat
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
mír	mír	k1gInSc4	mír
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
udělen	udělen	k2eAgInSc1d1	udělen
in	in	k?	in
memoriam	memoriam	k6eAd1	memoriam
Řád	řád	k1gInSc1	řád
Bílého	bílý	k2eAgInSc2d1	bílý
lva	lev	k1gInSc2	lev
civilní	civilní	k2eAgFnSc2d1	civilní
skupiny	skupina	k1gFnSc2	skupina
I.	I.	kA	I.
třídy	třída	k1gFnSc2	třída
za	za	k7c4	za
jeho	jeho	k3xOp3gFnPc4	jeho
zásluhy	zásluha	k1gFnPc4	zásluha
pro	pro	k7c4	pro
republiku	republika	k1gFnSc4	republika
<g/>
.	.	kIx.	.
</s>
