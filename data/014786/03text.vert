<s>
DAF	DAF	kA
XF	XF	kA
</s>
<s>
DAF	DAF	kA
XF	XF	kA
Výrobce	výrobce	k1gMnSc1
</s>
<s>
DAF	DAF	kA
Roky	rok	k1gInPc1
produkce	produkce	k1gFnSc2
</s>
<s>
1997	#num#	k4
<g/>
-dosud	-dosud	k1gInSc1
Motor	motor	k1gInSc1
Motor	motor	k1gInSc1
</s>
<s>
R6	R6	k4
(	(	kIx(
<g/>
410	#num#	k4
<g/>
-	-	kIx~
<g/>
530	#num#	k4
hp	hp	k?
<g/>
)	)	kIx)
Objem	objem	k1gInSc1
</s>
<s>
12,9	12,9	k4
l	l	kA
Převodovky	převodovka	k1gFnSc2
Převodovka	převodovka	k1gFnSc1
</s>
<s>
Servoshift	Servoshift	k1gInSc1
(	(	kIx(
<g/>
manuální	manuální	k2eAgFnSc6d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
AS-Tronic	AS-Tronice	k1gFnPc2
auto	auto	k1gNnSc4
(	(	kIx(
<g/>
automatická	automatický	k2eAgFnSc1d1
<g/>
)	)	kIx)
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
DAF	DAF	kA
XF	XF	kA
je	být	k5eAaImIp3nS
série	série	k1gFnSc1
nákladních	nákladní	k2eAgNnPc2d1
vozidel	vozidlo	k1gNnPc2
určená	určený	k2eAgFnSc1d1
pro	pro	k7c4
dlouhé	dlouhý	k2eAgInPc4d1
trasy	tras	k1gInPc4
a	a	k8xC
vyráběná	vyráběný	k2eAgFnSc1d1
společností	společnost	k1gFnSc7
DAF	DAF	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnes	dnes	k6eAd1
se	se	k3xPyFc4
vyrábí	vyrábět	k5eAaImIp3nS
druhá	druhý	k4xOgFnSc1
generace	generace	k1gFnSc1
vozidel	vozidlo	k1gNnPc2
XF	XF	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Verze	verze	k1gFnSc1
s	s	k7c7
pravostranným	pravostranný	k2eAgNnSc7d1
řízením	řízení	k1gNnSc7
se	se	k3xPyFc4
vyrábí	vyrábět	k5eAaImIp3nS
v	v	k7c6
závodě	závod	k1gInSc6
Leyland	Leyland	k1gInSc1
Trucks	Trucks	k1gInSc1
ve	v	k7c6
Spojeném	spojený	k2eAgNnSc6d1
království	království	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
DAF	DAF	kA
95	#num#	k4
XF	XF	kA
</s>
<s>
Poprvé	poprvé	k6eAd1
bylo	být	k5eAaImAgNnS
vozidlo	vozidlo	k1gNnSc1
této	tento	k3xDgFnSc2
série	série	k1gFnSc2
představeno	představit	k5eAaPmNgNnS
v	v	k7c6
roce	rok	k1gInSc6
1997	#num#	k4
a	a	k8xC
získalo	získat	k5eAaPmAgNnS
titul	titul	k1gInSc4
Truck	truck	k1gInSc1
of	of	k?
the	the	k?
Year	Year	k1gInSc1
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Používal	používat	k5eAaImAgMnS
nově	nově	k6eAd1
vyvinutý	vyvinutý	k2eAgInSc4d1
přeplňovaný	přeplňovaný	k2eAgInSc4d1
6	#num#	k4
-	-	kIx~
válcový	válcový	k2eAgInSc1d1
motor	motor	k1gInSc1
o	o	k7c6
objemu	objem	k1gInSc6
12,6	12,6	k4
l	l	kA
a	a	k8xC
s	s	k7c7
mezichladičem	mezichladič	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
palivovém	palivový	k2eAgInSc6d1
systému	systém	k1gInSc6
bylo	být	k5eAaImAgNnS
použito	použít	k5eAaPmNgNnS
mechanicky	mechanicky	k6eAd1
ovládané	ovládaný	k2eAgNnSc1d1
palivové	palivový	k2eAgNnSc1d1
čerpadlo	čerpadlo	k1gNnSc1
bez	bez	k7c2
elektronické	elektronický	k2eAgFnSc2d1
kontroly	kontrola	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
toho	ten	k3xDgNnSc2
motor	motor	k1gInSc1
splňoval	splňovat	k5eAaImAgInS
emisní	emisní	k2eAgFnSc4d1
normu	norma	k1gFnSc4
Euro	euro	k1gNnSc1
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
září	září	k1gNnSc6
1999	#num#	k4
byly	být	k5eAaImAgInP
modernizovány	modernizován	k2eAgInPc1d1
motory	motor	k1gInPc1
a	a	k8xC
tím	ten	k3xDgNnSc7
byla	být	k5eAaImAgFnS
splněna	splnit	k5eAaPmNgFnS
emisní	emisní	k2eAgFnSc1d1
norma	norma	k1gFnSc1
Euro	euro	k1gNnSc1
3	#num#	k4
podpořena	podpořit	k5eAaPmNgFnS
palivovým	palivový	k2eAgInSc7d1
systémem	systém	k1gInSc7
PDL	PDL	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgInS
podobný	podobný	k2eAgInSc1d1
vstřikovači	vstřikovač	k1gInSc3
a	a	k8xC
montoval	montovat	k5eAaImAgInS
se	se	k3xPyFc4
do	do	k7c2
každého	každý	k3xTgInSc2
válce	válec	k1gInSc2
zvlášť	zvlášť	k6eAd1
v	v	k7c6
kombinaci	kombinace	k1gFnSc6
s	s	k7c7
vysokým	vysoký	k2eAgInSc7d1
tlakem	tlak	k1gInSc7
vstřikování	vstřikování	k1gNnSc2
paliva	palivo	k1gNnSc2
(	(	kIx(
<g/>
až	až	k9
1	#num#	k4
500	#num#	k4
bar	bar	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Blok	blok	k1gInSc1
motoru	motor	k1gInSc2
se	se	k3xPyFc4
vyráběl	vyrábět	k5eAaImAgMnS
z	z	k7c2
litiny	litina	k1gFnSc2
CGI	CGI	kA
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
zajišťovala	zajišťovat	k5eAaImAgFnS
větší	veliký	k2eAgFnSc4d2
tuhost	tuhost	k1gFnSc4
oproti	oproti	k7c3
litinové	litinový	k2eAgFnSc3d1
konstrukci	konstrukce	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řada	řada	k1gFnSc1
motorů	motor	k1gInPc2
měla	mít	k5eAaImAgFnS
výkon	výkon	k1gInSc4
340	#num#	k4
<g/>
,	,	kIx,
381	#num#	k4
<g/>
,	,	kIx,
430	#num#	k4
,	,	kIx,
480	#num#	k4
a	a	k8xC
530	#num#	k4
koní	kůň	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Systém	systém	k1gInSc1
PDL	PDL	kA
umožnil	umožnit	k5eAaPmAgInS
zachování	zachování	k1gNnSc4
stávajících	stávající	k2eAgFnPc2d1
konstrukci	konstrukce	k1gFnSc3
kabin	kabina	k1gFnPc2
<g/>
,	,	kIx,
aniž	aniž	k8xS,k8xC
se	se	k3xPyFc4
zvýšil	zvýšit	k5eAaPmAgInS
motorový	motorový	k2eAgInSc1d1
tunel	tunel	k1gInSc1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
potřebné	potřebný	k2eAgNnSc1d1
pro	pro	k7c4
vstřikovací	vstřikovací	k2eAgFnSc4d1
jednotku	jednotka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kabiny	kabina	k1gFnPc1
se	se	k3xPyFc4
vyráběly	vyrábět	k5eAaImAgFnP
a	a	k8xC
dodnes	dodnes	k6eAd1
se	se	k3xPyFc4
vyrábějí	vyrábět	k5eAaImIp3nP
ve	v	k7c6
třech	tři	k4xCgFnPc6
verzích	verze	k1gFnPc6
:	:	kIx,
Comfort	Comfort	k1gInSc4
Cab	cabit	k5eAaImRp2nS
-	-	kIx~
spací	spací	k2eAgFnSc1d1
kabina	kabina	k1gFnSc1
s	s	k7c7
nízkou	nízký	k2eAgFnSc7d1
střechou	střecha	k1gFnSc7
a	a	k8xC
jedním	jeden	k4xCgNnSc7
lůžkem	lůžko	k1gNnSc7
<g/>
,	,	kIx,
Space	Space	k1gFnSc1
Cab	cabit	k5eAaImRp2nS
-	-	kIx~
spací	spací	k2eAgFnSc1d1
kabina	kabina	k1gFnSc1
se	s	k7c7
zvýšenou	zvýšený	k2eAgFnSc7d1
střechou	střecha	k1gFnSc7
a	a	k8xC
jedním	jeden	k4xCgNnSc7
nebo	nebo	k8xC
dvěma	dva	k4xCgNnPc7
lůžky	lůžko	k1gNnPc7
<g/>
,	,	kIx,
a	a	k8xC
vrchol	vrchol	k1gInSc1
nabídky	nabídka	k1gFnSc2
Super	super	k2eAgFnSc2d1
Space	Space	k1gFnSc2
Cab	cabit	k5eAaImRp2nS
-	-	kIx~
spací	spací	k2eAgFnSc1d1
kabina	kabina	k1gFnSc1
s	s	k7c7
vysokou	vysoký	k2eAgFnSc7d1
střechou	střecha	k1gFnSc7
a	a	k8xC
dvěma	dva	k4xCgInPc7
lůžky	lůžko	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
prošla	projít	k5eAaPmAgFnS
modelová	modelový	k2eAgFnSc1d1
řada	řada	k1gFnSc1
95	#num#	k4
XF	XF	kA
modernizací	modernizace	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
zasáhla	zasáhnout	k5eAaPmAgFnS
světlomety	světlomet	k1gInPc4
<g/>
,	,	kIx,
mřížku	mřížka	k1gFnSc4
chladiče	chladič	k1gInSc2
a	a	k8xC
vzduchové	vzduchový	k2eAgInPc4d1
deflektory	deflektor	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Modelová	modelový	k2eAgFnSc1d1
řada	řada	k1gFnSc1
po	po	k7c6
faceliftu	facelifto	k1gNnSc6
dostala	dostat	k5eAaPmAgFnS
nové	nový	k2eAgNnSc4d1
označení	označení	k1gNnSc4
jako	jako	k8xC,k8xS
XF	XF	kA
95	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
DAF	DAF	kA
XF	XF	kA
95	#num#	k4
</s>
<s>
DAF	DAF	kA
XF	XF	kA
95	#num#	k4
byl	být	k5eAaImAgInS
vytvořen	vytvořit	k5eAaPmNgInS
jako	jako	k8xS,k8xC
sedlový	sedlový	k2eAgInSc1d1
tahač	tahač	k1gInSc1
s	s	k7c7
podvozky	podvozek	k1gInPc7
s	s	k7c7
konfigurací	konfigurace	k1gFnSc7
4	#num#	k4
<g/>
x	x	k?
<g/>
2	#num#	k4
a	a	k8xC
6	#num#	k4
<g/>
x	x	k?
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podvozky	podvozek	k1gInPc1
se	se	k3xPyFc4
vyráběly	vyrábět	k5eAaImAgInP
ve	v	k7c6
dvou	dva	k4xCgFnPc6
verzích	verze	k1gFnPc6
M	M	kA
(	(	kIx(
<g/>
Medium	medium	k1gNnSc1
-	-	kIx~
Duty	Duty	k?
<g/>
)	)	kIx)
s	s	k7c7
výškou	výška	k1gFnSc7
260	#num#	k4
mm	mm	kA
a	a	k8xC
H	H	kA
(	(	kIx(
<g/>
Heavy	Heav	k1gInPc1
-	-	kIx~
Duty	Duty	k?
<g/>
)	)	kIx)
s	s	k7c7
výškou	výška	k1gFnSc7
260	#num#	k4
nebo	nebo	k8xC
310	#num#	k4
mm	mm	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
model	model	k1gInSc4
má	mít	k5eAaImIp3nS
vpředu	vpředu	k6eAd1
parabolické	parabolický	k2eAgFnPc4d1
listové	listový	k2eAgFnPc4d1
pružiny	pružina	k1gFnPc4
a	a	k8xC
vzduchové	vzduchový	k2eAgNnSc4d1
odpružení	odpružení	k1gNnSc4
skládající	skládající	k2eAgNnSc4d1
se	se	k3xPyFc4
ze	z	k7c2
čtyř	čtyři	k4xCgInPc2
vzduchových	vzduchový	k2eAgInPc2d1
měchů	měch	k1gInPc2
na	na	k7c6
zadní	zadní	k2eAgFnSc6d1
nápravě	náprava	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
veletrhu	veletrh	k1gInSc2
v	v	k7c6
Amsterdamu	Amsterdam	k1gInSc6
v	v	k7c6
říjnu	říjen	k1gInSc6
2005	#num#	k4
byl	být	k5eAaImAgMnS
představen	představen	k2eAgMnSc1d1
nástupce	nástupce	k1gMnSc1
XF	XF	kA
95	#num#	k4
pod	pod	k7c7
názvem	název	k1gInSc7
XF	XF	kA
105	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
DAF	DAF	kA
XF	XF	kA
105	#num#	k4
</s>
<s>
Tahač	tahač	k1gInSc1
extrémně	extrémně	k6eAd1
těžkých	těžký	k2eAgInPc2d1
a	a	k8xC
nadrozměrných	nadrozměrný	k2eAgInPc2d1
nákladů	náklad	k1gInPc2
DAF	DAF	kA
XF	XF	kA
105.510	105.510	k4
Space	Space	k1gFnSc2
Cab	cabit	k5eAaImRp2nS
v	v	k7c6
kofiguraci	kofigurace	k1gFnSc6
8	#num#	k4
<g/>
x	x	k?
<g/>
4	#num#	k4
(	(	kIx(
<g/>
FTM	FTM	kA
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
nastoupil	nastoupit	k5eAaPmAgMnS
do	do	k7c2
výroby	výroba	k1gFnSc2
a	a	k8xC
nahradil	nahradit	k5eAaPmAgInS
tak	tak	k9
sérii	série	k1gFnSc4
XF	XF	kA
95	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
XF	XF	kA
105	#num#	k4
má	mít	k5eAaImIp3nS
modernější	moderní	k2eAgFnSc4d2
kabinu	kabina	k1gFnSc4
a	a	k8xC
více	hodně	k6eAd2
ekonomické	ekonomický	k2eAgInPc1d1
a	a	k8xC
ekologické	ekologický	k2eAgInPc1d1
motory	motor	k1gInPc1
PACCAR	PACCAR	kA
MX	MX	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Motory	motor	k1gInPc7
mají	mít	k5eAaImIp3nP
objem	objem	k1gInSc4
12,9	12,9	k4
l	l	kA
a	a	k8xC
výkon	výkon	k1gInSc1
408	#num#	k4
<g/>
,	,	kIx,
462	#num#	k4
<g/>
,	,	kIx,
510	#num#	k4
a	a	k8xC
560	#num#	k4
koní	kůň	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomocí	pomocí	k7c2
systému	systém	k1gInSc2
selektivní	selektivní	k2eAgFnSc2d1
katalytické	katalytický	k2eAgFnSc2d1
redukce	redukce	k1gFnSc2
SCR	SCR	kA
splňují	splňovat	k5eAaImIp3nP
motory	motor	k1gInPc1
emisní	emisní	k2eAgFnSc2d1
normy	norma	k1gFnSc2
Euro	euro	k1gNnSc1
4	#num#	k4
a	a	k8xC
Euro	euro	k1gNnSc1
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobně	podobně	k6eAd1
jako	jako	k8xS,k8xC
předchozí	předchozí	k2eAgInPc1d1
motory	motor	k1gInPc1
mají	mít	k5eAaImIp3nP
4	#num#	k4
ventily	ventil	k1gInPc7
na	na	k7c4
válec	válec	k1gInSc4
přeplňování	přeplňování	k1gNnSc2
turbodmychadlem	turbodmychadlo	k1gNnSc7
s	s	k7c7
mezichladičem	mezichladič	k1gInSc7
stlačeného	stlačený	k2eAgInSc2d1
vzduchu	vzduch	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
motorech	motor	k1gInPc6
se	se	k3xPyFc4
používají	používat	k5eAaImIp3nP
vstřikovače	vstřikovač	k1gInPc1
PDL	PDL	kA
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
mají	mít	k5eAaImIp3nP
zvýšený	zvýšený	k2eAgInSc4d1
tlak	tlak	k1gInSc4
z	z	k7c2
1	#num#	k4
500	#num#	k4
na	na	k7c4
2	#num#	k4
000	#num#	k4
bar	bar	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
tomu	ten	k3xDgNnSc3
ve	v	k7c6
výfukovém	výfukový	k2eAgInSc6d1
systému	systém	k1gInSc6
není	být	k5eNaImIp3nS
potřeba	potřeba	k6eAd1
filtr	filtr	k1gInSc4
pevných	pevný	k2eAgFnPc2d1
částic	částice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Životnost	životnost	k1gFnSc1
motoru	motor	k1gInSc2
se	se	k3xPyFc4
odhaduje	odhadovat	k5eAaImIp3nS
na	na	k7c4
1,6	1,6	k4
milionu	milion	k4xCgInSc2
kilometrů	kilometr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Standardně	standardně	k6eAd1
se	se	k3xPyFc4
montuje	montovat	k5eAaImIp3nS
16	#num#	k4
-	-	kIx~
stupňová	stupňový	k2eAgFnSc1d1
manuální	manuální	k2eAgFnSc1d1
převodovka	převodovka	k1gFnSc1
a	a	k8xC
volitelná	volitelný	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
automatická	automatický	k2eAgFnSc1d1
převodovka	převodovka	k1gFnSc1
AS	as	k1gNnSc2
Tronic	Tronice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c4
podzim	podzim	k1gInSc4
2006	#num#	k4
byl	být	k5eAaImAgInS
představen	představit	k5eAaPmNgInS
společně	společně	k6eAd1
s	s	k7c7
existujícími	existující	k2eAgInPc7d1
pohony	pohon	k1gInPc7
i	i	k8xC
verze	verze	k1gFnSc1
6	#num#	k4
<g/>
x	x	k?
<g/>
4	#num#	k4
<g/>
,	,	kIx,
8	#num#	k4
<g/>
x	x	k?
<g/>
2	#num#	k4
a	a	k8xC
8	#num#	k4
<g/>
x	x	k?
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgInSc1d1
jmenovaný	jmenovaný	k2eAgInSc1d1
je	být	k5eAaImIp3nS
tahač	tahač	k1gInSc1
s	s	k7c7
pohonem	pohon	k1gInSc7
8	#num#	k4
<g/>
x	x	k?
<g/>
4	#num#	k4
(	(	kIx(
<g/>
FTM	FTM	kA
<g/>
)	)	kIx)
se	s	k7c7
třemi	tři	k4xCgFnPc7
zadními	zadní	k2eAgFnPc7d1
nápravami	náprava	k1gFnPc7
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
první	první	k4xOgFnSc1
má	mít	k5eAaImIp3nS
řiditelná	řiditelný	k2eAgNnPc4d1
kola	kolo	k1gNnPc4
a	a	k8xC
dvěma	dva	k4xCgInPc7
posledními	poslední	k2eAgInPc7d1
jsou	být	k5eAaImIp3nP
hnány	hnát	k5eAaImNgFnP
nápravy	náprava	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
tahač	tahač	k1gInSc1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
pro	pro	k7c4
přepravu	přeprava	k1gFnSc4
velmi	velmi	k6eAd1
těžkých	těžký	k2eAgNnPc2d1
zařízení	zařízení	k1gNnPc2
a	a	k8xC
nadrozměrných	nadrozměrný	k2eAgInPc2d1
nákladů	náklad	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Uspořádání	uspořádání	k1gNnSc1
náprav	náprava	k1gFnPc2
a	a	k8xC
pohonů	pohon	k1gInPc2
</s>
<s>
FT	FT	kA
(	(	kIx(
<g/>
4	#num#	k4
<g/>
x	x	k?
<g/>
2	#num#	k4
<g/>
)	)	kIx)
-	-	kIx~
přední	přední	k2eAgFnSc1d1
řízená	řízený	k2eAgFnSc1d1
náprava	náprava	k1gFnSc1
<g/>
,	,	kIx,
zadní	zadní	k2eAgFnSc1d1
hnaná	hnaný	k2eAgFnSc1d1
náprava	náprava	k1gFnSc1
</s>
<s>
FTG	FTG	kA
(	(	kIx(
<g/>
6	#num#	k4
<g/>
x	x	k?
<g/>
2	#num#	k4
<g/>
)	)	kIx)
-	-	kIx~
přední	přední	k2eAgFnSc1d1
řízená	řízený	k2eAgFnSc1d1
náprava	náprava	k1gFnSc1
<g/>
,	,	kIx,
vodicí	vodicí	k2eAgFnSc1d1
první	první	k4xOgFnSc1
zadní	zadní	k2eAgFnSc1d1
náprava	náprava	k1gFnSc1
a	a	k8xC
hnaná	hnaný	k2eAgFnSc1d1
poslední	poslední	k2eAgFnSc1d1
náprava	náprava	k1gFnSc1
</s>
<s>
FTP	FTP	kA
(	(	kIx(
<g/>
6	#num#	k4
<g/>
x	x	k?
<g/>
2	#num#	k4
<g/>
)	)	kIx)
-	-	kIx~
přední	přední	k2eAgFnSc1d1
řízená	řízený	k2eAgFnSc1d1
náprava	náprava	k1gFnSc1
<g/>
,	,	kIx,
nepoháněná	poháněný	k2eNgFnSc1d1
první	první	k4xOgFnSc1
zadní	zadní	k2eAgFnSc1d1
náprava	náprava	k1gFnSc1
a	a	k8xC
hnaná	hnaný	k2eAgFnSc1d1
poslední	poslední	k2eAgFnSc1d1
náprava	náprava	k1gFnSc1
</s>
<s>
FAN	Fana	k1gFnPc2
(	(	kIx(
<g/>
6	#num#	k4
<g/>
x	x	k?
<g/>
2	#num#	k4
<g/>
)	)	kIx)
-	-	kIx~
přední	přední	k2eAgFnSc1d1
řízená	řízený	k2eAgFnSc1d1
náprava	náprava	k1gFnSc1
<g/>
,	,	kIx,
první	první	k4xOgFnSc1
zadní	zadní	k2eAgFnSc1d1
hnaná	hnaný	k2eAgFnSc1d1
náprava	náprava	k1gFnSc1
a	a	k8xC
druhá	druhý	k4xOgFnSc1
zadní	zadní	k2eAgFnSc1d1
řízená	řízený	k2eAgFnSc1d1
vlečná	vlečný	k2eAgFnSc1d1
náprava	náprava	k1gFnSc1
</s>
<s>
FAR	fara	k1gFnPc2
(	(	kIx(
<g/>
6	#num#	k4
<g/>
x	x	k?
<g/>
2	#num#	k4
<g/>
)	)	kIx)
-	-	kIx~
přední	přední	k2eAgFnSc1d1
řízená	řízený	k2eAgFnSc1d1
náprava	náprava	k1gFnSc1
<g/>
,	,	kIx,
první	první	k4xOgFnSc1
zadní	zadní	k2eAgFnSc1d1
hnaná	hnaný	k2eAgFnSc1d1
náprava	náprava	k1gFnSc1
a	a	k8xC
druhá	druhý	k4xOgFnSc1
zadní	zadní	k2eAgFnSc1d1
nepoháněná	poháněný	k2eNgFnSc1d1
náprava	náprava	k1gFnSc1
s	s	k7c7
jednomontáží	jednomontáž	k1gFnSc7
</s>
<s>
FAS	FAS	kA
(	(	kIx(
<g/>
6	#num#	k4
<g/>
x	x	k?
<g/>
2	#num#	k4
<g/>
)	)	kIx)
-	-	kIx~
přední	přední	k2eAgFnSc1d1
řízená	řízený	k2eAgFnSc1d1
náprava	náprava	k1gFnSc1
<g/>
,	,	kIx,
první	první	k4xOgFnSc1
zadní	zadní	k2eAgFnSc1d1
hnaná	hnaný	k2eAgFnSc1d1
náprava	náprava	k1gFnSc1
a	a	k8xC
druhá	druhý	k4xOgFnSc1
zadní	zadní	k2eAgFnSc1d1
nepoháněná	poháněný	k2eNgFnSc1d1
náprava	náprava	k1gFnSc1
s	s	k7c7
dvoumontáží	dvoumontáž	k1gFnSc7
</s>
<s>
FTT	FTT	kA
(	(	kIx(
<g/>
6	#num#	k4
<g/>
x	x	k?
<g/>
4	#num#	k4
<g/>
)	)	kIx)
-	-	kIx~
přední	přední	k2eAgFnSc1d1
řízená	řízený	k2eAgFnSc1d1
náprava	náprava	k1gFnSc1
<g/>
,	,	kIx,
zadní	zadní	k2eAgFnSc1d1
hnaná	hnaný	k2eAgFnSc1d1
tandemové	tandemový	k2eAgFnPc4d1
nápravy	náprava	k1gFnPc4
</s>
<s>
FTC	FTC	kA
(	(	kIx(
<g/>
8	#num#	k4
<g/>
x	x	k?
<g/>
2	#num#	k4
<g/>
)	)	kIx)
-	-	kIx~
dvě	dva	k4xCgFnPc1
přední	přední	k2eAgFnPc1d1
řízené	řízený	k2eAgFnPc1d1
nápravy	náprava	k1gFnPc1
<g/>
,	,	kIx,
první	první	k4xOgFnSc1
zadní	zadní	k2eAgFnSc1d1
hnaná	hnaný	k2eAgFnSc1d1
náprava	náprava	k1gFnSc1
a	a	k8xC
druhá	druhý	k4xOgFnSc1
zadní	zadní	k2eAgFnSc1d1
nepoháněná	poháněný	k2eNgFnSc1d1
náprava	náprava	k1gFnSc1
s	s	k7c7
dvoumontáží	dvoumontáž	k1gFnSc7
</s>
<s>
FAX	fax	k1gInSc1
(	(	kIx(
<g/>
8	#num#	k4
<g/>
x	x	k?
<g/>
2	#num#	k4
<g/>
)	)	kIx)
-	-	kIx~
dvě	dva	k4xCgFnPc1
přední	přední	k2eAgFnPc1d1
řízené	řízený	k2eAgFnPc1d1
nápravy	náprava	k1gFnPc1
<g/>
,	,	kIx,
první	první	k4xOgFnSc1
zadní	zadní	k2eAgFnSc1d1
hnaná	hnaný	k2eAgFnSc1d1
náprava	náprava	k1gFnSc1
a	a	k8xC
druhá	druhý	k4xOgFnSc1
zadní	zadní	k2eAgFnSc1d1
řízená	řízený	k2eAgFnSc1d1
vlečná	vlečný	k2eAgFnSc1d1
náprava	náprava	k1gFnSc1
</s>
<s>
FAD	FAD	kA
(	(	kIx(
<g/>
8	#num#	k4
<g/>
x	x	k?
<g/>
4	#num#	k4
<g/>
)	)	kIx)
-	-	kIx~
dvě	dva	k4xCgFnPc1
přední	přední	k2eAgFnPc1d1
řízené	řízený	k2eAgFnPc1d1
nápravy	náprava	k1gFnPc1
<g/>
,	,	kIx,
zadní	zadní	k2eAgFnSc1d1
hnaná	hnaný	k2eAgFnSc1d1
tandemové	tandemový	k2eAgFnPc4d1
nápravy	náprava	k1gFnPc4
</s>
<s>
FAK	FAK	kA
(	(	kIx(
<g/>
8	#num#	k4
<g/>
x	x	k?
<g/>
2	#num#	k4
<g/>
)	)	kIx)
-	-	kIx~
přední	přední	k2eAgFnSc1d1
řízená	řízený	k2eAgFnSc1d1
náprava	náprava	k1gFnSc1
<g/>
,	,	kIx,
řízená	řízený	k2eAgFnSc1d1
vodící	vodící	k2eAgFnSc1d1
první	první	k4xOgFnSc1
zadní	zadní	k2eAgFnSc1d1
náprava	náprava	k1gFnSc1
<g/>
,	,	kIx,
hnaná	hnaný	k2eAgFnSc1d1
zadní	zadní	k2eAgFnSc1d1
náprava	náprava	k1gFnSc1
a	a	k8xC
NEPOHÁNĚNÁ	poháněný	k2eNgFnSc1d1
zadní	zadní	k2eAgFnSc1d1
vlečná	vlečný	k2eAgFnSc1d1
náprava	náprava	k1gFnSc1
s	s	k7c7
dvoumontáží	dvoumontáž	k1gFnSc7
</s>
<s>
FTM	FTM	kA
(	(	kIx(
<g/>
8	#num#	k4
<g/>
x	x	k?
<g/>
4	#num#	k4
<g/>
)	)	kIx)
-	-	kIx~
Přední	přední	k2eAgFnSc1d1
řízená	řízený	k2eAgFnSc1d1
náprava	náprava	k1gFnSc1
<g/>
,	,	kIx,
řízená	řízený	k2eAgFnSc1d1
vodící	vodící	k2eAgFnSc1d1
první	první	k4xOgFnSc1
zadní	zadní	k2eAgFnSc1d1
náprava	náprava	k1gFnSc1
<g/>
,	,	kIx,
zadní	zadní	k2eAgFnSc1d1
hnaná	hnaný	k2eAgFnSc1d1
tandemové	tandemový	k2eAgFnPc4d1
nápravy	náprava	k1gFnPc4
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
DAF	DAF	kA
XF	XF	kA
na	na	k7c6
slovenské	slovenský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Leyland	Leyland	k1gInSc1
Trucks	Trucks	k1gInSc1
'	'	kIx"
<g/>
Product	Product	k2eAgInSc1d1
Range	Range	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leyland	Leyland	k1gInSc1
Trucks	Trucks	k1gInSc4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
19	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
2007	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
DAF	DAF	kA
XF105	XF105	k1gMnSc1
Voted	Voted	k1gMnSc1
International	International	k1gMnSc1
Truck	truck	k1gInSc1
of	of	k?
the	the	k?
Year	Year	k1gInSc1
2007	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
19	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
16	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Archivováno	archivován	k2eAgNnSc4d1
16	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2007	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gMnSc5
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
DAF	DAF	kA
XF	XF	kA
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
CT-Finland	CT-Finland	k1gInSc1
|	|	kIx~
DAF	DAF	kA
-	-	kIx~
XF-série	XF-série	k1gFnSc1
</s>
<s>
DAF	DAF	kA
XF105	XF105	k1gFnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Automobil	automobil	k1gInSc1
</s>
