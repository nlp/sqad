<s>
Hana	Hana	k1gFnSc1	Hana
Hegerová	Hegerová	k1gFnSc1	Hegerová
<g/>
,	,	kIx,	,
rozená	rozený	k2eAgFnSc1d1	rozená
Carmen	Carmen	k1gInSc4	Carmen
Mária	Mária	k1gFnSc1	Mária
Štefánia	Štefánium	k1gNnSc2	Štefánium
(	(	kIx(	(
<g/>
Beatrix	Beatrix	k1gInSc1	Beatrix
<g/>
)	)	kIx)	)
Farkašová-Čelková	Farkašová-Čelková	k1gFnSc1	Farkašová-Čelková
(	(	kIx(	(
<g/>
*	*	kIx~	*
20	[number]	k4	20
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1931	[number]	k4	1931
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
významná	významný	k2eAgFnSc1d1	významná
česká	český	k2eAgFnSc1d1	Česká
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
a	a	k8xC	a
herečka	herečka	k1gFnSc1	herečka
slovenského	slovenský	k2eAgInSc2d1	slovenský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
nekorunovaná	korunovaný	k2eNgFnSc1d1	nekorunovaná
královna	královna	k1gFnSc1	královna
československého	československý	k2eAgInSc2d1	československý
šansonu	šanson	k1gInSc2	šanson
<g/>
.	.	kIx.	.
</s>
<s>
Zahraniční	zahraniční	k2eAgMnPc1d1	zahraniční
kritici	kritik	k1gMnPc1	kritik
ji	on	k3xPp3gFnSc4	on
nazývají	nazývat	k5eAaImIp3nP	nazývat
Velkou	velký	k2eAgFnSc7d1	velká
dámou	dáma	k1gFnSc7	dáma
šansonu	šanson	k1gInSc2	šanson
<g/>
,	,	kIx,	,
Piaf	Piaf	k1gFnPc1	Piaf
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
také	také	k9	také
Šansoniérkou	šansoniérka	k1gFnSc7	šansoniérka
se	s	k7c7	s
slovanskou	slovanský	k2eAgFnSc7d1	Slovanská
duší	duše	k1gFnSc7	duše
<g/>
.	.	kIx.	.
</s>
<s>
Prosadila	prosadit	k5eAaPmAgFnS	prosadit
se	se	k3xPyFc4	se
především	především	k9	především
jako	jako	k9	jako
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
šansonů	šanson	k1gInPc2	šanson
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nemá	mít	k5eNaImIp3nS	mít
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
bývalého	bývalý	k2eAgInSc2d1	bývalý
Československa	Československo	k1gNnSc2	Československo
žádnou	žádný	k3yNgFnSc4	žádný
srovnatelnou	srovnatelný	k2eAgFnSc4d1	srovnatelná
konkurenci	konkurence	k1gFnSc4	konkurence
<g/>
.	.	kIx.	.
</s>
<s>
Široké	Široké	k2eAgNnSc4d1	Široké
publikum	publikum	k1gNnSc4	publikum
i	i	k8xC	i
odbornou	odborný	k2eAgFnSc4d1	odborná
kritiku	kritika	k1gFnSc4	kritika
zaujala	zaujmout	k5eAaPmAgFnS	zaujmout
hlavně	hlavně	k9	hlavně
schopností	schopnost	k1gFnSc7	schopnost
dramaticky	dramaticky	k6eAd1	dramaticky
interpretovat	interpretovat	k5eAaBmF	interpretovat
text	text	k1gInSc4	text
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgNnSc4d1	velké
uznání	uznání	k1gNnSc4	uznání
sklidila	sklidit	k5eAaPmAgFnS	sklidit
i	i	k9	i
v	v	k7c6	v
cizině	cizina	k1gFnSc6	cizina
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
německy	německy	k6eAd1	německy
mluvících	mluvící	k2eAgFnPc6d1	mluvící
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Narodila	narodit	k5eAaPmAgFnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
bankovního	bankovní	k2eAgMnSc4d1	bankovní
ředitele	ředitel	k1gMnSc4	ředitel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1937	[number]	k4	1937
<g/>
–	–	k?	–
<g/>
1942	[number]	k4	1942
navštěvovala	navštěvovat	k5eAaImAgFnS	navštěvovat
baletní	baletní	k2eAgFnSc4d1	baletní
školu	škola	k1gFnSc4	škola
v	v	k7c6	v
Národním	národní	k2eAgNnSc6d1	národní
divadle	divadlo	k1gNnSc6	divadlo
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
maturitě	maturita	k1gFnSc6	maturita
na	na	k7c6	na
gymnáziu	gymnázium	k1gNnSc6	gymnázium
v	v	k7c6	v
Komárně	Komárno	k1gNnSc6	Komárno
se	se	k3xPyFc4	se
chtěla	chtít	k5eAaImAgFnS	chtít
dostat	dostat	k5eAaPmF	dostat
na	na	k7c4	na
Vysokou	vysoký	k2eAgFnSc4d1	vysoká
školu	škola	k1gFnSc4	škola
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
nemohla	moct	k5eNaImAgFnS	moct
<g/>
,	,	kIx,	,
pracovala	pracovat	k5eAaImAgFnS	pracovat
v	v	k7c6	v
komárenských	komárenský	k2eAgFnPc6d1	komárenský
strojírnách	strojírna	k1gFnPc6	strojírna
a	a	k8xC	a
přednášela	přednášet	k5eAaImAgFnS	přednášet
na	na	k7c6	na
tamní	tamní	k2eAgFnSc6d1	tamní
učňovské	učňovský	k2eAgFnSc6d1	učňovská
škole	škola	k1gFnSc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
absolvování	absolvování	k1gNnSc6	absolvování
odborného	odborný	k2eAgInSc2d1	odborný
divadelního	divadelní	k2eAgInSc2d1	divadelní
kurzu	kurz	k1gInSc2	kurz
při	při	k7c6	při
státní	státní	k2eAgFnSc6d1	státní
konzervatoři	konzervatoř	k1gFnSc6	konzervatoř
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1951	[number]	k4	1951
až	až	k8xS	až
1953	[number]	k4	1953
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
členkou	členka	k1gFnSc7	členka
Divadla	divadlo	k1gNnSc2	divadlo
Petra	Petr	k1gMnSc2	Petr
Jilemnického	jilemnický	k2eAgMnSc2d1	jilemnický
v	v	k7c6	v
Žilině	Žilina	k1gFnSc6	Žilina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
již	již	k6eAd1	již
měla	mít	k5eAaImAgFnS	mít
dítě	dítě	k1gNnSc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
hrála	hrát	k5eAaImAgFnS	hrát
titulní	titulní	k2eAgFnSc4d1	titulní
roli	role	k1gFnSc4	role
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Frona	Frono	k1gNnSc2	Frono
pod	pod	k7c7	pod
uměleckým	umělecký	k2eAgNnSc7d1	umělecké
jménem	jméno	k1gNnSc7	jméno
Hana	Hana	k1gFnSc1	Hana
Čelková	Čelková	k1gFnSc1	Čelková
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
působila	působit	k5eAaImAgFnS	působit
v	v	k7c6	v
bratislavské	bratislavský	k2eAgFnPc1d1	Bratislavská
Tatra	Tatra	k1gFnSc1	Tatra
revue	revue	k1gFnSc1	revue
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
představila	představit	k5eAaPmAgFnS	představit
jako	jako	k9	jako
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1961	[number]	k4	1961
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
v	v	k7c6	v
pražském	pražský	k2eAgNnSc6d1	Pražské
divadle	divadlo	k1gNnSc6	divadlo
Rokoko	rokoko	k1gNnSc1	rokoko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1962	[number]	k4	1962
až	až	k9	až
1966	[number]	k4	1966
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
Semafor	Semafor	k1gInSc4	Semafor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hrála	hrát	k5eAaImAgFnS	hrát
v	v	k7c6	v
pásmu	pásmo	k1gNnSc6	pásmo
písniček	písnička	k1gFnPc2	písnička
Zuzana	Zuzana	k1gFnSc1	Zuzana
je	být	k5eAaImIp3nS	být
zase	zase	k9	zase
sama	sám	k3xTgMnSc4	sám
doma	doma	k6eAd1	doma
<g/>
,	,	kIx,	,
Zuzana	Zuzana	k1gFnSc1	Zuzana
není	být	k5eNaImIp3nS	být
pro	pro	k7c4	pro
nikoho	nikdo	k3yNnSc4	nikdo
doma	doma	k6eAd1	doma
<g/>
,	,	kIx,	,
v	v	k7c6	v
Dobře	dobře	k6eAd1	dobře
placené	placený	k2eAgFnSc6d1	placená
procházce	procházka	k1gFnSc6	procházka
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
kolegy	kolega	k1gMnPc7	kolega
ze	z	k7c2	z
Semaforu	Semafor	k1gInSc2	Semafor
hrála	hrát	k5eAaImAgFnS	hrát
i	i	k9	i
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Kdyby	kdyby	k9	kdyby
tisíc	tisíc	k4xCgInPc2	tisíc
klarinetů	klarinet	k1gInPc2	klarinet
(	(	kIx(	(
<g/>
role	role	k1gFnSc1	role
Edity	Edita	k1gFnSc2	Edita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
velkých	velký	k2eAgInPc2d1	velký
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
úspěchů	úspěch	k1gInPc2	úspěch
svou	svůj	k3xOyFgFnSc4	svůj
interpretací	interpretace	k1gFnSc7	interpretace
původních	původní	k2eAgInPc2d1	původní
i	i	k8xC	i
cizích	cizí	k2eAgInPc2d1	cizí
šansonů	šanson	k1gInPc2	šanson
<g/>
.	.	kIx.	.
</s>
<s>
Absolvovala	absolvovat	k5eAaPmAgFnS	absolvovat
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
úspěšných	úspěšný	k2eAgNnPc2d1	úspěšné
vystoupení	vystoupení	k1gNnPc2	vystoupení
doma	doma	k6eAd1	doma
i	i	k9	i
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
s	s	k7c7	s
francouzsky	francouzsky	k6eAd1	francouzsky
zpívanými	zpívaný	k2eAgFnPc7d1	zpívaná
písněmi	píseň	k1gFnPc7	píseň
v	v	k7c6	v
Pařížské	pařížský	k2eAgFnSc6d1	Pařížská
Olympii	Olympia	k1gFnSc6	Olympia
a	a	k8xC	a
na	na	k7c6	na
Světové	světový	k2eAgFnSc6d1	světová
výstavě	výstava	k1gFnSc6	výstava
v	v	k7c6	v
Montrealu	Montreal	k1gInSc6	Montreal
<g/>
.	.	kIx.	.
</s>
<s>
Začali	začít	k5eAaPmAgMnP	začít
jí	on	k3xPp3gFnSc3	on
vydávat	vydávat	k5eAaImF	vydávat
gramofonové	gramofonový	k2eAgFnSc2d1	gramofonová
desky	deska	k1gFnSc2	deska
v	v	k7c6	v
NSR	NSR	kA	NSR
<g/>
,	,	kIx,	,
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
v	v	k7c6	v
televizních	televizní	k2eAgInPc6d1	televizní
pořadech	pořad	k1gInPc6	pořad
NDR	NDR	kA	NDR
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
umisťovala	umisťovat	k5eAaImAgFnS	umisťovat
pravidelně	pravidelně	k6eAd1	pravidelně
v	v	k7c6	v
první	první	k4xOgFnSc6	první
desítce	desítka	k1gFnSc6	desítka
ankety	anketa	k1gFnSc2	anketa
Zlatý	zlatý	k2eAgInSc1d1	zlatý
slavík	slavík	k1gInSc1	slavík
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
70	[number]	k4	70
<g/>
.	.	kIx.	.
a	a	k8xC	a
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
strávila	strávit	k5eAaPmAgFnS	strávit
půl	půl	k6eAd1	půl
roku	rok	k1gInSc2	rok
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
její	její	k3xOp3gInSc4	její
účet	účet	k1gInSc4	účet
omylem	omylem	k6eAd1	omylem
přišla	přijít	k5eAaPmAgFnS	přijít
vyšší	vysoký	k2eAgFnSc1d2	vyšší
finanční	finanční	k2eAgFnSc1d1	finanční
částka	částka	k1gFnSc1	částka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
patřila	patřit	k5eAaImAgFnS	patřit
Karlu	Karla	k1gFnSc4	Karla
Gottovi	Gott	k1gMnSc3	Gott
<g/>
.	.	kIx.	.
</s>
<s>
Hegerová	Hegerová	k1gFnSc1	Hegerová
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
vlastních	vlastní	k2eAgNnPc2d1	vlastní
slov	slovo	k1gNnPc2	slovo
domnívala	domnívat	k5eAaImAgFnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
honorář	honorář	k1gInSc4	honorář
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
nechala	nechat	k5eAaPmAgFnS	nechat
si	se	k3xPyFc3	se
ho	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
soud	soud	k1gInSc1	soud
to	ten	k3xDgNnSc1	ten
vyhodnotil	vyhodnotit	k5eAaPmAgInS	vyhodnotit
jako	jako	k8xS	jako
zpronevěru	zpronevěra	k1gFnSc4	zpronevěra
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
12	[number]	k4	12
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2011	[number]	k4	2011
ohlásila	ohlásit	k5eAaPmAgFnS	ohlásit
konec	konec	k1gInSc4	konec
své	svůj	k3xOyFgFnSc2	svůj
koncertní	koncertní	k2eAgFnSc2d1	koncertní
kariéry	kariéra	k1gFnSc2	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
obdržela	obdržet	k5eAaPmAgFnS	obdržet
od	od	k7c2	od
prezidenta	prezident	k1gMnSc2	prezident
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
medaili	medaile	k1gFnSc4	medaile
Za	za	k7c4	za
zásluhy	zásluha	k1gFnPc4	zásluha
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
držitelkou	držitelka	k1gFnSc7	držitelka
rytířské	rytířský	k2eAgFnSc2d1	rytířská
hvězdy	hvězda	k1gFnSc2	hvězda
francouzského	francouzský	k2eAgInSc2d1	francouzský
Řádu	řád	k1gInSc2	řád
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2012	[number]	k4	2012
obdržela	obdržet	k5eAaPmAgFnS	obdržet
na	na	k7c6	na
Staroměstské	staroměstský	k2eAgFnSc6d1	Staroměstská
radnici	radnice	k1gFnSc6	radnice
čestné	čestný	k2eAgNnSc4d1	čestné
občanství	občanství	k1gNnSc4	občanství
Prahy	Praha	k1gFnSc2	Praha
1	[number]	k4	1
a	a	k8xC	a
šek	šek	k1gInSc1	šek
na	na	k7c4	na
250.000	[number]	k4	250.000
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2013	[number]	k4	2013
byla	být	k5eAaImAgFnS	být
povýšena	povýšit	k5eAaPmNgFnS	povýšit
na	na	k7c4	na
komandéra	komandér	k1gMnSc4	komandér
francouzského	francouzský	k2eAgInSc2d1	francouzský
Řádu	řád	k1gInSc2	řád
za	za	k7c4	za
zásluhy	zásluha	k1gFnPc4	zásluha
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
rukou	ruka	k1gFnPc2	ruka
francouzského	francouzský	k2eAgMnSc2d1	francouzský
velvyslance	velvyslanec	k1gMnSc2	velvyslanec
Pierra	Pierr	k1gMnSc4	Pierr
Lévyho	Lévy	k1gMnSc4	Lévy
řád	řád	k1gInSc1	řád
převzal	převzít	k5eAaPmAgInS	převzít
v	v	k7c6	v
zastoupení	zastoupení	k1gNnSc6	zastoupení
její	její	k3xOp3gMnSc1	její
vnuk	vnuk	k1gMnSc1	vnuk
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
vyjádření	vyjádření	k1gNnSc2	vyjádření
velvyslance	velvyslanec	k1gMnSc4	velvyslanec
byl	být	k5eAaImAgInS	být
zpěvačce	zpěvačka	k1gFnSc3	zpěvačka
udělen	udělen	k2eAgMnSc1d1	udělen
za	za	k7c4	za
významné	významný	k2eAgNnSc4d1	významné
přispění	přispění	k1gNnSc4	přispění
k	k	k7c3	k
propagaci	propagace	k1gFnSc3	propagace
francouzského	francouzský	k2eAgInSc2d1	francouzský
šansonu	šanson	k1gInSc2	šanson
a	a	k8xC	a
francouzské	francouzský	k2eAgFnSc2d1	francouzská
kultury	kultura	k1gFnSc2	kultura
obecně	obecně	k6eAd1	obecně
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2014	[number]	k4	2014
jí	jíst	k5eAaImIp3nS	jíst
český	český	k2eAgMnSc1d1	český
prezident	prezident	k1gMnSc1	prezident
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
udělil	udělit	k5eAaPmAgMnS	udělit
Řád	řád	k1gInSc4	řád
Tomáše	Tomáš	k1gMnSc2	Tomáš
Garrigua	Garrigu	k1gInSc2	Garrigu
Masaryka	Masaryk	k1gMnSc2	Masaryk
I.	I.	kA	I.
třídy	třída	k1gFnSc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
Slovenský	slovenský	k2eAgMnSc1d1	slovenský
prezident	prezident	k1gMnSc1	prezident
Andrej	Andrej	k1gMnSc1	Andrej
Kiska	Kiska	k1gMnSc1	Kiska
jí	jíst	k5eAaImIp3nS	jíst
7	[number]	k4	7
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2016	[number]	k4	2016
na	na	k7c6	na
Bratislavském	bratislavský	k2eAgInSc6d1	bratislavský
hradě	hrad	k1gInSc6	hrad
udělil	udělit	k5eAaPmAgInS	udělit
Řád	řád	k1gInSc1	řád
Ľudovíta	Ľudovíto	k1gNnSc2	Ľudovíto
Štúra	Štúr	k1gInSc2	Štúr
II	II	kA	II
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
Řadová	řadový	k2eAgFnSc1d1	řadová
alba	alba	k1gFnSc1	alba
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
2016	[number]	k4	2016
Šansony	šansona	k1gFnSc2	šansona
Pozn	pozn	kA	pozn
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Každé	každý	k3xTgNnSc1	každý
vydání	vydání	k1gNnSc1	vydání
mělo	mít	k5eAaImAgNnS	mít
jinou	jiný	k2eAgFnSc4d1	jiná
obálku	obálka	k1gFnSc4	obálka
<g/>
!	!	kIx.	!
</s>
<s>
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
Recital	Recital	k1gMnSc1	Recital
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
Recital	Recital	k1gMnSc1	Recital
2	[number]	k4	2
1977	[number]	k4	1977
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
Lásko	láska	k1gFnSc5	láska
prokletá	prokletý	k2eAgFnSc1d1	prokletá
1987	[number]	k4	1987
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
Potměšilý	potměšilý	k2eAgInSc1d1	potměšilý
host	host	k1gMnSc1	host
1987	[number]	k4	1987
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
Chansons	Chansons	k1gInSc1	Chansons
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
Mlýnské	mlýnský	k2eAgInPc1d1	mlýnský
kolo	kolo	k1gNnSc4	kolo
v	v	k7c6	v
srdci	srdce	k1gNnSc6	srdce
mém	můj	k3xOp1gNnSc6	můj
Pozn	pozn	kA	pozn
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Reedice	reedice	k1gFnSc1	reedice
s	s	k7c7	s
DVD	DVD	kA	DVD
<g/>
!	!	kIx.	!
</s>
<s>
Živá	živý	k2eAgFnSc1d1	živá
alba	alba	k1gFnSc1	alba
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
Live	Liv	k1gInSc2	Liv
1998	[number]	k4	1998
Bratislava	Bratislava	k1gFnSc1	Bratislava
live	live	k1gFnSc1	live
2006	[number]	k4	2006
Hana	Hana	k1gFnSc1	Hana
Hegerová	Hegerová	k1gFnSc1	Hegerová
2015	[number]	k4	2015
Recital	Recital	k1gMnSc1	Recital
'	'	kIx"	'
<g/>
70	[number]	k4	70
Kompilace	kompilace	k1gFnSc1	kompilace
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
Ohlédnutí	ohlédnutí	k1gNnSc2	ohlédnutí
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
Paběrky	paběrka	k1gFnSc2	paběrka
Pozn	pozn	kA	pozn
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
První	první	k4xOgNnSc1	první
vydání	vydání	k1gNnSc1	vydání
2	[number]	k4	2
CD	CD	kA	CD
<g/>
,	,	kIx,	,
reedice	reedice	k1gFnSc1	reedice
ve	v	k7c6	v
zkrácené	zkrácený	k2eAgFnSc6d1	zkrácená
podobě	podoba	k1gFnSc6	podoba
na	na	k7c4	na
1	[number]	k4	1
CD	CD	kA	CD
<g/>
!	!	kIx.	!
</s>
<s>
1997	[number]	k4	1997
Rýmování	rýmování	k1gNnSc1	rýmování
o	o	k7c6	o
životě	život	k1gInSc6	život
2005	[number]	k4	2005
Můj	můj	k1gMnSc1	můj
dík	dík	k1gInSc1	dík
(	(	kIx(	(
<g/>
H.	H.	kA	H.
H.	H.	kA	H.
zpívá	zpívat	k5eAaImIp3nS	zpívat
písně	píseň	k1gFnPc4	píseň
Pavla	Pavla	k1gFnSc1	Pavla
Kopty	kopt	k1gInPc1	kopt
<g/>
)	)	kIx)	)
2006	[number]	k4	2006
Všechno	všechen	k3xTgNnSc1	všechen
nejlepší	dobrý	k2eAgInPc4d3	nejlepší
2009	[number]	k4	2009
Paběrky	paběrek	k1gInPc4	paběrek
a	a	k8xC	a
pamlsky	pamlsek	k1gInPc7	pamlsek
2013	[number]	k4	2013
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
kolekce	kolekce	k1gFnSc1	kolekce
1957	[number]	k4	1957
<g/>
–	–	k?	–
<g/>
2010	[number]	k4	2010
2016	[number]	k4	2016
Cesta	cesta	k1gFnSc1	cesta
EP	EP	kA	EP
1965	[number]	k4	1965
Prague	Pragu	k1gFnSc2	Pragu
Songs	Songs	k1gInSc4	Songs
1969	[number]	k4	1969
Hana	Hana	k1gFnSc1	Hana
Hegerová	Hegerová	k1gFnSc1	Hegerová
Zahraniční	zahraniční	k2eAgFnSc1d1	zahraniční
alba	alba	k1gFnSc1	alba
1967	[number]	k4	1967
Ich	Ich	k1gMnSc1	Ich
–	–	k?	–
Hana	Hana	k1gFnSc1	Hana
Hegerová	Hegerová	k1gFnSc1	Hegerová
1972	[number]	k4	1972
So	So	kA	So
<g />
.	.	kIx.	.
</s>
<s>
geht	geht	k1gMnSc1	geht
es	es	k1gNnPc2	es
auf	auf	k?	auf
der	drát	k5eAaImRp2nS	drát
Welt	Welt	k1gInSc4	Welt
1974	[number]	k4	1974
Fast	Fast	k1gMnSc1	Fast
ein	ein	k?	ein
Liebeslied	Liebeslied	k1gMnSc1	Liebeslied
1975	[number]	k4	1975
Wir	Wir	k1gFnSc2	Wir
für	für	k?	für
euch	euch	k1gMnSc1	euch
1954	[number]	k4	1954
Frona	Fron	k1gInSc2	Fron
1957	[number]	k4	1957
Tam	tam	k6eAd1	tam
na	na	k7c6	na
konečné	konečná	k1gFnSc6	konečná
1960	[number]	k4	1960
Policejní	policejní	k2eAgFnSc1d1	policejní
hodina	hodina	k1gFnSc1	hodina
1960	[number]	k4	1960
Přežil	přežít	k5eAaPmAgMnS	přežít
jsem	být	k5eAaImIp1nS	být
svou	svůj	k3xOyFgFnSc4	svůj
smrt	smrt	k1gFnSc4	smrt
1962	[number]	k4	1962
Neděle	neděle	k1gFnSc1	neděle
ve	v	k7c4	v
všední	všední	k2eAgInSc4d1	všední
den	den	k1gInSc4	den
1962	[number]	k4	1962
Zhasněte	zhasnout	k5eAaPmRp2nP	zhasnout
lampióny	lampión	k1gInPc7	lampión
1963	[number]	k4	1963
Konkurs	konkurs	k1gInSc1	konkurs
(	(	kIx(	(
<g/>
Režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Miloš	Miloš	k1gMnSc1	Miloš
Forman	Forman	k1gMnSc1	Forman
<g/>
)	)	kIx)	)
1963	[number]	k4	1963
Naděje	naděje	k1gFnSc1	naděje
1964	[number]	k4	1964
Kdyby	kdyby	k9	kdyby
tisíc	tisíc	k4xCgInPc2	tisíc
klarinetů	klarinet	k1gInPc2	klarinet
1966	[number]	k4	1966
Dobře	dobře	k6eAd1	dobře
placená	placený	k2eAgFnSc1d1	placená
procházka	procházka	k1gFnSc1	procházka
(	(	kIx(	(
<g/>
televizní	televizní	k2eAgInSc1d1	televizní
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
1967	[number]	k4	1967
Ta	ten	k3xDgFnSc1	ten
naše	náš	k3xOp1gFnSc1	náš
písnička	písnička	k1gFnSc1	písnička
česká	český	k2eAgFnSc1d1	Česká
1974	[number]	k4	1974
Třicet	třicet	k4xCc1	třicet
případů	případ	k1gInPc2	případ
majora	major	k1gMnSc2	major
Zemana	Zeman	k1gMnSc2	Zeman
1988	[number]	k4	1988
Lovec	lovec	k1gMnSc1	lovec
senzací	senzace	k1gFnSc7	senzace
1989	[number]	k4	1989
"	"	kIx"	"
<g/>
Fabrik	fabrika	k1gFnPc2	fabrika
der	drát	k5eAaImRp2nS	drát
Offiziere	Offizier	k1gMnSc5	Offizier
<g/>
"	"	kIx"	"
1991	[number]	k4	1991
Poslední	poslední	k2eAgMnSc1d1	poslední
motýl	motýl	k1gMnSc1	motýl
2006	[number]	k4	2006
Kde	kde	k6eAd1	kde
lampy	lampa	k1gFnSc2	lampa
bloudí	bloudit	k5eAaImIp3nS	bloudit
2008	[number]	k4	2008
Česká	český	k2eAgFnSc1d1	Česká
RAPublika	RAPublika	k1gFnSc1	RAPublika
</s>
