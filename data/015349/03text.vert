<s>
Pennsylvanian	Pennsylvanian	k1gMnSc1
(	(	kIx(
<g/>
Amtrak	Amtrak	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Pennsylvanian	Pennsylvanian	k1gInSc1
je	být	k5eAaImIp3nS
715	#num#	k4
km	km	kA
dlouhé	dlouhý	k2eAgFnSc2d1
denní	denní	k2eAgFnSc2d1
železniční	železniční	k2eAgFnSc2d1
spojení	spojení	k1gNnSc4
mezi	mezi	k7c4
New	New	k1gFnSc4
Yorkem	York	k1gInSc7
a	a	k8xC
Pittsburghem	Pittsburgh	k1gInSc7
přes	přes	k7c4
Filadelfii	Filadelfie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
nejvytíženějšího	vytížený	k2eAgInSc2d3
železničního	železniční	k2eAgInSc2d1
regionu	region	k1gInSc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
,	,	kIx,
Severovýchodního	severovýchodní	k2eAgInSc2d1
koridoru	koridor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jízdní	jízdní	k2eAgFnSc1d1
doba	doba	k1gFnSc1
činí	činit	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
9	#num#	k4
hodin	hodina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Provoz	provoz	k1gInSc1
na	na	k7c6
trati	trať	k1gFnSc6
začal	začít	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
1980	#num#	k4
po	po	k7c6
osmileté	osmiletý	k2eAgFnSc6d1
pauze	pauza	k1gFnSc6
<g/>
,	,	kIx,
do	do	k7c2
roku	rok	k1gInSc2
1972	#num#	k4
jezdil	jezdit	k5eAaImAgMnS
na	na	k7c6
stejnojmenné	stejnojmenný	k2eAgFnSc6d1
trati	trať	k1gFnSc6
vlak	vlak	k1gInSc1
Keystone	Keyston	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
167	#num#	k4
km	km	kA
úseku	úsek	k1gInSc6
z	z	k7c2
Philadelphie	Philadelphia	k1gFnSc2
do	do	k7c2
Harrisburgu	Harrisburg	k1gInSc2
byli	být	k5eAaImAgMnP
v	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
ukončeny	ukončen	k2eAgFnPc4d1
úpravy	úprava	k1gFnPc4
tratě	trať	k1gFnPc4
umožňující	umožňující	k2eAgNnSc1d1
dosahovat	dosahovat	k5eAaImF
rychlost	rychlost	k1gFnSc4
do	do	k7c2
180	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h.	h.	k?
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Pennsylvanian	Pennsylvanian	k1gInSc1
(	(	kIx(
<g/>
Amtrak	Amtrak	k1gInSc1
<g/>
)	)	kIx)
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Pennsylvanian	Pennsylvaniana	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Amtrak	Amtrak	k1gMnSc1
-	-	kIx~
Pennsylvanian	Pennsylvanian	k1gMnSc1
</s>
