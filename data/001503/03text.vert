<s>
Gabriel	Gabriel	k1gMnSc1	Gabriel
Jonas	Jonas	k1gMnSc1	Jonas
Lippmann	Lippmann	k1gMnSc1	Lippmann
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1845	[number]	k4	1845
<g/>
,	,	kIx,	,
Bonnevoie	Bonnevoie	k1gFnSc1	Bonnevoie
<g/>
,	,	kIx,	,
Lucembursko	Lucembursko	k1gNnSc1	Lucembursko
-	-	kIx~	-
13	[number]	k4	13
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1921	[number]	k4	1921
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
parníku	parník	k1gInSc2	parník
France	Franc	k1gMnPc4	Franc
<g/>
,	,	kIx,	,
Atlantský	atlantský	k2eAgInSc1d1	atlantský
oceán	oceán	k1gInSc1	oceán
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
rok	rok	k1gInSc4	rok
1908	[number]	k4	1908
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
profesorem	profesor	k1gMnSc7	profesor
matematické	matematický	k2eAgFnSc2d1	matematická
fyziky	fyzika	k1gFnSc2	fyzika
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1886	[number]	k4	1886
experimentální	experimentální	k2eAgFnSc2d1	experimentální
fyziky	fyzika	k1gFnSc2	fyzika
na	na	k7c6	na
Sorbonně	Sorbonna	k1gFnSc6	Sorbonna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1872	[number]	k4	1872
vynalezl	vynaleznout	k5eAaPmAgInS	vynaleznout
kapilární	kapilární	k2eAgInSc1d1	kapilární
elektrometr	elektrometr	k1gInSc1	elektrometr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1887	[number]	k4	1887
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
metodu	metoda	k1gFnSc4	metoda
reprodukce	reprodukce	k1gFnSc2	reprodukce
barevné	barevný	k2eAgFnSc2d1	barevná
fotografie	fotografia	k1gFnSc2	fotografia
založené	založený	k2eAgFnSc2d1	založená
na	na	k7c6	na
interferenci	interference	k1gFnSc6	interference
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
obdržel	obdržet	k5eAaPmAgMnS	obdržet
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Navázal	navázat	k5eAaPmAgMnS	navázat
na	na	k7c4	na
výzkum	výzkum	k1gInSc4	výzkum
Clauda	Claud	k1gMnSc4	Claud
Félixe	Félixe	k1gFnSc2	Félixe
de	de	k?	de
Saint-Victora	Saint-Victor	k1gMnSc2	Saint-Victor
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
společně	společně	k6eAd1	společně
s	s	k7c7	s
Alexandrem	Alexandr	k1gMnSc7	Alexandr
Becquerelem	becquerel	k1gInSc7	becquerel
pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
o	o	k7c4	o
první	první	k4xOgFnSc4	první
barevnou	barevný	k2eAgFnSc4d1	barevná
fotografii	fotografia	k1gFnSc4	fotografia
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nedokázali	dokázat	k5eNaPmAgMnP	dokázat
ji	on	k3xPp3gFnSc4	on
ještě	ještě	k6eAd1	ještě
trvale	trvale	k6eAd1	trvale
a	a	k8xC	a
spolehlivě	spolehlivě	k6eAd1	spolehlivě
ustálit	ustálit	k5eAaPmF	ustálit
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
povedlo	povést	k5eAaPmAgNnS	povést
právě	právě	k6eAd1	právě
jemu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1891	[number]	k4	1891
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
období	období	k1gNnSc6	období
byl	být	k5eAaImAgMnS	být
také	také	k9	také
prezidentem	prezident	k1gMnSc7	prezident
francouzské	francouzský	k2eAgFnSc2d1	francouzská
fotografické	fotografický	k2eAgFnSc2d1	fotografická
společnosti	společnost	k1gFnSc2	společnost
Société	Sociétý	k2eAgFnSc2d1	Sociétý
française	française	k1gFnSc2	française
de	de	k?	de
photographie	photographie	k1gFnSc2	photographie
<g/>
.	.	kIx.	.
</s>
