<p>
<s>
Our	Our	k?	Our
Airline	Airlin	k1gInSc5	Airlin
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Naše	náš	k3xOp1gFnSc1	náš
Aerolinie	aerolinie	k1gFnSc1	aerolinie
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
národní	národní	k2eAgFnSc1d1	národní
letecká	letecký	k2eAgFnSc1d1	letecká
společnost	společnost	k1gFnSc1	společnost
republiky	republika	k1gFnSc2	republika
Nauru	Naur	k1gInSc2	Naur
<g/>
.	.	kIx.	.
</s>
<s>
Letecká	letecký	k2eAgFnSc1d1	letecká
společnost	společnost	k1gFnSc1	společnost
<g/>
,	,	kIx,	,
nástupce	nástupce	k1gMnSc1	nástupce
"	"	kIx"	"
<g/>
Air	Air	k1gFnSc1	Air
Nauru	Naur	k1gInSc2	Naur
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
rychlý	rychlý	k2eAgInSc1d1	rychlý
dopravní	dopravní	k2eAgInSc4d1	dopravní
prostředek	prostředek	k1gInSc4	prostředek
ke	k	k7c3	k
spojení	spojení	k1gNnSc3	spojení
se	s	k7c7	s
sousedními	sousední	k2eAgInPc7d1	sousední
státy	stát	k1gInPc7	stát
v	v	k7c6	v
období	období	k1gNnSc6	období
růstu	růst	k1gInSc2	růst
státní	státní	k2eAgFnSc2d1	státní
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
Provozuje	provozovat	k5eAaImIp3nS	provozovat
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
pravidelnou	pravidelný	k2eAgFnSc4d1	pravidelná
dopravu	doprava	k1gFnSc4	doprava
v	v	k7c4	v
pondělí	pondělí	k1gNnSc4	pondělí
a	a	k8xC	a
sobotu	sobota	k1gFnSc4	sobota
z	z	k7c2	z
Brisbane	Brisban	k1gMnSc5	Brisban
<g/>
,	,	kIx,	,
Honairy	Honaira	k1gFnSc2	Honaira
<g/>
,	,	kIx,	,
Nauru	Naur	k1gInSc2	Naur
<g/>
,	,	kIx,	,
Nadi	Naďa	k1gFnSc2	Naďa
a	a	k8xC	a
Tarawy	Tarawa	k1gFnSc2	Tarawa
v	v	k7c6	v
jediné	jediný	k2eAgFnSc6d1	jediná
letecké	letecký	k2eAgFnSc6d1	letecká
lince	linka	k1gFnSc6	linka
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
když	když	k8xS	když
měla	mít	k5eAaImAgFnS	mít
bývalá	bývalý	k2eAgFnSc1d1	bývalá
společnost	společnost	k1gFnSc1	společnost
Air	Air	k1gFnSc2	Air
Nauru	Naur	k1gInSc2	Naur
značnou	značný	k2eAgFnSc4d1	značná
flotilu	flotila	k1gFnSc4	flotila
letadel	letadlo	k1gNnPc2	letadlo
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
provozovány	provozován	k2eAgInPc1d1	provozován
lety	let	k1gInPc1	let
do	do	k7c2	do
mnoha	mnoho	k4c2	mnoho
tichomořských	tichomořský	k2eAgInPc2d1	tichomořský
ostrovů	ostrov	k1gInPc2	ostrov
a	a	k8xC	a
tří	tři	k4xCgNnPc2	tři
australských	australský	k2eAgNnPc2d1	Australské
měst	město	k1gNnPc2	město
-	-	kIx~	-
Melbourne	Melbourne	k1gNnPc2	Melbourne
<g/>
,	,	kIx,	,
Brisbane	Brisban	k1gMnSc5	Brisban
a	a	k8xC	a
Sydney	Sydney	k1gNnSc2	Sydney
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
hlavní	hlavní	k2eAgFnSc1d1	hlavní
základna	základna	k1gFnSc1	základna
je	být	k5eAaImIp3nS	být
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
letiště	letiště	k1gNnSc4	letiště
Nauru	Naur	k1gInSc2	Naur
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Chronologie	chronologie	k1gFnSc2	chronologie
==	==	k?	==
</s>
</p>
<p>
<s>
1970	[number]	k4	1970
-	-	kIx~	-
Dne	den	k1gInSc2	den
14	[number]	k4	14
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
je	být	k5eAaImIp3nS	být
založena	založen	k2eAgFnSc1d1	založena
společnost	společnost	k1gFnSc1	společnost
Air	Air	k1gFnSc2	Air
Nauru	Naur	k1gInSc2	Naur
<g/>
,	,	kIx,	,
letadlo	letadlo	k1gNnSc1	letadlo
Dassault	Dassaulta	k1gFnPc2	Dassaulta
Falcon	Falcon	k1gNnSc1	Falcon
20	[number]	k4	20
pronajaté	pronajatý	k2eAgFnSc6d1	pronajatá
od	od	k7c2	od
jiné	jiný	k2eAgFnSc2d1	jiná
společnosti	společnost	k1gFnSc2	společnost
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
1971	[number]	k4	1971
-	-	kIx~	-
první	první	k4xOgNnSc1	první
komerční	komerční	k2eAgNnSc1d1	komerční
letadlo	letadlo	k1gNnSc1	letadlo
letecké	letecký	k2eAgFnSc2d1	letecká
společnosti	společnost	k1gFnSc2	společnost
Air	Air	k1gFnSc2	Air
Nauru	Naur	k1gInSc2	Naur
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
1976	[number]	k4	1976
-	-	kIx~	-
Dvě	dva	k4xCgNnPc1	dva
další	další	k2eAgNnPc1d1	další
letadla	letadlo	k1gNnPc1	letadlo
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
1983	[number]	k4	1983
-	-	kIx~	-
Celkový	celkový	k2eAgInSc1d1	celkový
strojový	strojový	k2eAgInSc1d1	strojový
park	park	k1gInSc1	park
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
sedmi	sedm	k4xCc2	sedm
letadel	letadlo	k1gNnPc2	letadlo
Boeing	boeing	k1gInSc4	boeing
<g/>
,	,	kIx,	,
společnost	společnost	k1gFnSc1	společnost
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
svých	svůj	k3xOyFgMnPc2	svůj
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
ekonomických	ekonomický	k2eAgMnPc2d1	ekonomický
ukazatelů	ukazatel	k1gMnPc2	ukazatel
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
1990	[number]	k4	1990
-	-	kIx~	-
Republika	republika	k1gFnSc1	republika
Nauru	Naur	k1gInSc2	Naur
čelí	čelit	k5eAaImIp3nS	čelit
vážné	vážný	k2eAgFnSc3d1	vážná
ekonomické	ekonomický	k2eAgFnSc3d1	ekonomická
krizi	krize	k1gFnSc3	krize
<g/>
,	,	kIx,	,
Air	Air	k1gFnSc3	Air
Nauru	Naur	k1gInSc2	Naur
musí	muset	k5eAaImIp3nS	muset
prodat	prodat	k5eAaPmF	prodat
šest	šest	k4xCc4	šest
ze	z	k7c2	z
svých	svůj	k3xOyFgNnPc2	svůj
letadel	letadlo	k1gNnPc2	letadlo
Boeing	boeing	k1gInSc1	boeing
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
jen	jen	k9	jen
Boeing	boeing	k1gInSc4	boeing
737-400	[number]	k4	737-400
zůstal	zůstat	k5eAaPmAgMnS	zůstat
v	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
1992	[number]	k4	1992
-	-	kIx~	-
Letecká	letecký	k2eAgFnSc1d1	letecká
společnost	společnost	k1gFnSc1	společnost
Air	Air	k1gFnSc2	Air
Nauru	Naur	k1gInSc2	Naur
v	v	k7c6	v
konkurzu	konkurz	k1gInSc6	konkurz
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
2005	[number]	k4	2005
-	-	kIx~	-
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
poslední	poslední	k2eAgNnSc4d1	poslední
letadlo	letadlo	k1gNnSc4	letadlo
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
2006	[number]	k4	2006
-	-	kIx~	-
Ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
dohodě	dohoda	k1gFnSc3	dohoda
s	s	k7c7	s
Tchaj-wanem	Tchajan	k1gInSc7	Tchaj-wan
(	(	kIx(	(
<g/>
Čína	Čína	k1gFnSc1	Čína
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
záchrana	záchrana	k1gFnSc1	záchrana
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
přejmenování	přejmenování	k1gNnSc6	přejmenování
na	na	k7c4	na
Our	Our	k1gFnSc4	Our
Airline	Airlin	k1gInSc5	Airlin
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
2008	[number]	k4	2008
-	-	kIx~	-
Our	Our	k1gFnSc1	Our
Airline	Airlin	k1gInSc5	Airlin
létá	létat	k5eAaImIp3nS	létat
mezi	mezi	k7c7	mezi
Brisbane	Brisban	k1gMnSc5	Brisban
a	a	k8xC	a
Nauru	Naur	k1gInSc2	Naur
<g/>
,	,	kIx,	,
s	s	k7c7	s
mezipřistáním	mezipřistání	k1gNnSc7	mezipřistání
v	v	k7c6	v
Honiaře	Honiara	k1gFnSc6	Honiara
<g/>
,	,	kIx,	,
s	s	k7c7	s
cestovním	cestovní	k2eAgInSc7d1	cestovní
časem	čas	k1gInSc7	čas
5	[number]	k4	5
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
45	[number]	k4	45
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2009	[number]	k4	2009
-	-	kIx~	-
Další	další	k2eAgInSc1d1	další
nový	nový	k2eAgInSc1d1	nový
Boeing	boeing	k1gInSc1	boeing
737-300	[number]	k4	737-300
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
flotily	flotila	k1gFnSc2	flotila
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Air	Air	k?	Air
Nauru	Naura	k1gFnSc4	Naura
začala	začít	k5eAaPmAgFnS	začít
létat	létat	k5eAaImF	létat
14	[number]	k4	14
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1970	[number]	k4	1970
mezi	mezi	k7c4	mezi
Nauru	Naura	k1gFnSc4	Naura
a	a	k8xC	a
Brisbane	Brisban	k1gMnSc5	Brisban
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
letadla	letadlo	k1gNnSc2	letadlo
Dassault	Dassault	k2eAgInSc4d1	Dassault
Falcon	Falcon	k1gInSc4	Falcon
20	[number]	k4	20
(	(	kIx(	(
<g/>
VH-BIZ	VH-BIZ	k1gFnSc2	VH-BIZ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
byly	být	k5eAaImAgInP	být
zahájeny	zahájen	k2eAgInPc1d1	zahájen
pravidelné	pravidelný	k2eAgInPc1d1	pravidelný
lety	let	k1gInPc1	let
s	s	k7c7	s
letadlem	letadlo	k1gNnSc7	letadlo
Fokker	Fokker	k1gMnSc1	Fokker
F-28	F-28	k1gMnSc1	F-28
(	(	kIx(	(
<g/>
C	C	kA	C
<g/>
2	[number]	k4	2
<g/>
-RN	-RN	k?	-RN
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
Boeing	boeing	k1gInSc4	boeing
737-200	[number]	k4	737-200
(	(	kIx(	(
<g/>
C	C	kA	C
<g/>
2	[number]	k4	2
<g/>
-RN	-RN	k?	-RN
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
přibyl	přibýt	k5eAaPmAgInS	přibýt
do	do	k7c2	do
letecké	letecký	k2eAgFnSc2d1	letecká
flotily	flotila	k1gFnSc2	flotila
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
a	a	k8xC	a
Boeing	boeing	k1gInSc1	boeing
727-100	[number]	k4	727-100
(	(	kIx(	(
<g/>
C	C	kA	C
<g/>
2	[number]	k4	2
<g/>
-RN	-RN	k?	-RN
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
k	k	k7c3	k
16	[number]	k4	16
<g/>
.	.	kIx.	.
červnu	červen	k1gInSc6	červen
v	v	k7c4	v
1976	[number]	k4	1976
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
na	na	k7c6	na
konci	konec	k1gInSc6	konec
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
dvě	dva	k4xCgNnPc4	dva
letadla	letadlo	k1gNnPc4	letadlo
-	-	kIx~	-
Boeing	boeing	k1gInSc4	boeing
737-727	[number]	k4	737-727
a	a	k8xC	a
727-200	[number]	k4	727-200
-	-	kIx~	-
prodána	prodán	k2eAgFnSc1d1	prodána
Air	Air	k1gFnSc7	Air
Niugini	Niugin	k1gMnPc1	Niugin
<g/>
,	,	kIx,	,
letecké	letecký	k2eAgFnPc1d1	letecká
společnosti	společnost	k1gFnPc1	společnost
z	z	k7c2	z
Papua-Nová	Papua-Nový	k2eAgFnSc1d1	Papua-Nová
Guinea	Guinea	k1gFnSc1	Guinea
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
čítal	čítat	k5eAaImAgInS	čítat
strojový	strojový	k2eAgInSc1d1	strojový
park	park	k1gInSc1	park
sedm	sedm	k4xCc4	sedm
letadel	letadlo	k1gNnPc2	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
letadla	letadlo	k1gNnSc2	letadlo
Boeing	boeing	k1gInSc4	boeing
727-100	[number]	k4	727-100
(	(	kIx(	(
<g/>
zápis	zápis	k1gInSc1	zápis
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
RN	RN	kA	RN
<g/>
7	[number]	k4	7
<g/>
-C	-C	k?	-C
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
a	a	k8xC	a
pět	pět	k4xCc1	pět
Boeing	boeing	k1gInSc1	boeing
737-200	[number]	k4	737-200
(	(	kIx(	(
<g/>
C	C	kA	C
<g/>
2	[number]	k4	2
<g/>
-RN	-RN	k?	-RN
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
RN	RN	kA	RN
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
RN	RN	kA	RN
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
RN8	RN8	k1gFnSc1	RN8
a	a	k8xC	a
RN	RN	kA	RN
<g/>
9	[number]	k4	9
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
celá	celý	k2eAgFnSc1d1	celá
populace	populace	k1gFnSc1	populace
v	v	k7c6	v
republice	republika	k1gFnSc6	republika
Nauru	Naur	k1gInSc2	Naur
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
činila	činit	k5eAaImAgFnS	činit
jen	jen	k9	jen
asi	asi	k9	asi
8	[number]	k4	8
tisíc	tisíc	k4xCgInPc2	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Společnost	společnost	k1gFnSc1	společnost
také	také	k9	také
měla	mít	k5eAaImAgFnS	mít
špatnou	špatný	k2eAgFnSc4d1	špatná
pověst	pověst	k1gFnSc4	pověst
kvůli	kvůli	k7c3	kvůli
zrušeným	zrušený	k2eAgInPc3d1	zrušený
letům	let	k1gInPc3	let
a	a	k8xC	a
zpožděním	zpoždění	k1gNnSc7	zpoždění
<g/>
.	.	kIx.	.
</s>
<s>
Letecká	letecký	k2eAgFnSc1d1	letecká
společnost	společnost	k1gFnSc1	společnost
začala	začít	k5eAaPmAgFnS	začít
na	na	k7c4	na
pronajímat	pronajímat	k5eAaImF	pronajímat
a	a	k8xC	a
prodávat	prodávat	k5eAaImF	prodávat
letadla	letadlo	k1gNnPc4	letadlo
a	a	k8xC	a
začala	začít	k5eAaPmAgNnP	začít
stagnovat	stagnovat	k5eAaImF	stagnovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
kvůli	kvůli	k7c3	kvůli
těžké	těžký	k2eAgFnSc3d1	těžká
hospodářské	hospodářský	k2eAgFnSc3d1	hospodářská
krizi	krize	k1gFnSc3	krize
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
musí	muset	k5eAaImIp3nS	muset
Air	Air	k1gMnPc3	Air
Nauru	Naura	k1gFnSc4	Naura
prodat	prodat	k5eAaPmF	prodat
několik	několik	k4yIc4	několik
letadel	letadlo	k1gNnPc2	letadlo
<g/>
,	,	kIx,	,
nechávají	nechávat	k5eAaImIp3nP	nechávat
si	se	k3xPyFc3	se
pouze	pouze	k6eAd1	pouze
Boeing	boeing	k1gInSc4	boeing
737-400	[number]	k4	737-400
a	a	k8xC	a
dostávají	dostávat	k5eAaImIp3nP	dostávat
se	se	k3xPyFc4	se
pod	pod	k7c4	pod
státní	státní	k2eAgFnSc4d1	státní
správu	správa	k1gFnSc4	správa
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
čelí	čelit	k5eAaImIp3nP	čelit
Nauru	Naura	k1gFnSc4	Naura
dalším	další	k2eAgFnPc3d1	další
vážným	vážný	k2eAgFnPc3d1	vážná
finančním	finanční	k2eAgFnPc3d1	finanční
potížím	potíž	k1gFnPc3	potíž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
Air	Air	k1gFnPc6	Air
Nauru	Naur	k1gInSc2	Naur
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
konkurz	konkurz	k1gInSc1	konkurz
s	s	k7c7	s
dluhem	dluh	k1gInSc7	dluh
kolem	kolem	k7c2	kolem
US	US	kA	US
<g/>
$	$	kIx~	$
10.000.000	[number]	k4	10.000.000
<g/>
.	.	kIx.	.
</s>
<s>
Dohoda	dohoda	k1gFnSc1	dohoda
o	o	k7c6	o
záchraně	záchrana	k1gFnSc6	záchrana
s	s	k7c7	s
americkou	americký	k2eAgFnSc7d1	americká
společností	společnost	k1gFnSc7	společnost
General	General	k1gMnSc1	General
Electric	Electric	k1gMnSc1	Electric
Capital	Capital	k1gMnSc1	Capital
Corporation	Corporation	k1gInSc4	Corporation
selhala	selhat	k5eAaPmAgFnS	selhat
<g/>
,	,	kIx,	,
majetek	majetek	k1gInSc1	majetek
firmy	firma	k1gFnSc2	firma
byl	být	k5eAaImAgInS	být
obstaven	obstavit	k5eAaPmNgInS	obstavit
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
věřitelů	věřitel	k1gMnPc2	věřitel
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
zabaven	zabavit	k5eAaPmNgInS	zabavit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Obnova	obnova	k1gFnSc1	obnova
Our	Our	k1gFnSc2	Our
Airline	Airlin	k1gInSc5	Airlin
===	===	k?	===
</s>
</p>
<p>
<s>
Díky	díky	k7c3	díky
finanční	finanční	k2eAgFnSc3d1	finanční
podpoře	podpora	k1gFnSc3	podpora
z	z	k7c2	z
Tchaj-wanu	Tchajan	k1gInSc2	Tchaj-wan
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
firma	firma	k1gFnSc1	firma
obnovena	obnovit	k5eAaPmNgFnS	obnovit
pod	pod	k7c7	pod
novým	nový	k2eAgInSc7d1	nový
názvem	název	k1gInSc7	název
Our	Our	k1gFnSc2	Our
Airline	Airlin	k1gInSc5	Airlin
s	s	k7c7	s
reklamním	reklamní	k2eAgNnSc7d1	reklamní
heslem	heslo	k1gNnSc7	heslo
"	"	kIx"	"
<g/>
Let	léto	k1gNnPc2	léto
our	our	k?	our
airline	airlin	k1gInSc5	airlin
be	be	k?	be
your	your	k1gInSc1	your
airline	airlin	k1gInSc5	airlin
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Nechť	nechť	k9	nechť
naše	náš	k3xOp1gFnSc1	náš
letecká	letecký	k2eAgFnSc1d1	letecká
společnost	společnost	k1gFnSc1	společnost
bude	být	k5eAaImBp3nS	být
váš	váš	k3xOp2gInSc1	váš
letecké	letecký	k2eAgFnPc1d1	letecká
společnosti	společnost	k1gFnPc1	společnost
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
provozuje	provozovat	k5eAaImIp3nS	provozovat
lety	léto	k1gNnPc7	léto
na	na	k7c6	na
trase	trasa	k1gFnSc6	trasa
z	z	k7c2	z
Brisbane	Brisban	k1gMnSc5	Brisban
(	(	kIx(	(
<g/>
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
)	)	kIx)	)
na	na	k7c4	na
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
letiště	letiště	k1gNnSc4	letiště
Nauru	Naur	k1gInSc2	Naur
s	s	k7c7	s
mezipřistáním	mezipřistání	k1gNnSc7	mezipřistání
v	v	k7c6	v
Honaiře	Honaira	k1gFnSc6	Honaira
(	(	kIx(	(
<g/>
Šalamounovy	Šalamounův	k2eAgInPc1d1	Šalamounův
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
)	)	kIx)	)
a	a	k8xC	a
dál	daleko	k6eAd2	daleko
do	do	k7c2	do
Tarawy	Tarawa	k1gFnSc2	Tarawa
v	v	k7c6	v
Kiribati	Kiribati	k1gFnSc6	Kiribati
a	a	k8xC	a
Nadi	Naďa	k1gFnSc6	Naďa
na	na	k7c6	na
ostrovech	ostrov	k1gInPc6	ostrov
Fidži	Fidž	k1gFnSc6	Fidž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Flotila	flotila	k1gFnSc1	flotila
==	==	k?	==
</s>
</p>
<p>
<s>
Společnost	společnost	k1gFnSc1	společnost
Naše	náš	k3xOp1gFnSc1	náš
letecká	letecký	k2eAgFnSc1d1	letecká
společnost	společnost	k1gFnSc1	společnost
má	mít	k5eAaImIp3nS	mít
pouze	pouze	k6eAd1	pouze
dvě	dva	k4xCgNnPc1	dva
letadla	letadlo	k1gNnPc1	letadlo
Boeing	boeing	k1gInSc4	boeing
737-300	[number]	k4	737-300
zapsaná	zapsaný	k2eAgFnSc1d1	zapsaná
jako	jako	k8xC	jako
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
VH-INU	VH-INU	k?	VH-INU
</s>
</p>
<p>
<s>
VH-NLK	VH-NLK	k?	VH-NLK
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
Odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Our	Our	k?	Our
Airline	Airlin	k1gInSc5	Airlin
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
