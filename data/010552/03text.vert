<p>
<s>
Donald	Donald	k1gMnSc1	Donald
John	John	k1gMnSc1	John
Trump	Trump	k1gMnSc1	Trump
(	(	kIx(	(
<g/>
*	*	kIx~	*
14	[number]	k4	14
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1946	[number]	k4	1946
New	New	k1gFnPc2	New
York	York	k1gInSc1	York
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
podnikatel	podnikatel	k1gMnSc1	podnikatel
<g/>
,	,	kIx,	,
republikánský	republikánský	k2eAgMnSc1d1	republikánský
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
45	[number]	k4	45
<g/>
.	.	kIx.	.
prezident	prezident	k1gMnSc1	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
,	,	kIx,	,
kterým	který	k3yQgMnSc7	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Bílého	bílý	k2eAgInSc2d1	bílý
domu	dům	k1gInSc2	dům
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
70	[number]	k4	70
let	léto	k1gNnPc2	léto
jako	jako	k8xS	jako
nejstarší	starý	k2eAgInSc1d3	nejstarší
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
nejbohatší	bohatý	k2eAgMnSc1d3	nejbohatší
zvolený	zvolený	k2eAgMnSc1d1	zvolený
prezident	prezident	k1gMnSc1	prezident
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
také	také	k9	také
prvním	první	k4xOgMnSc7	první
mužem	muž	k1gMnSc7	muž
v	v	k7c6	v
úřadu	úřad	k1gInSc6	úřad
bez	bez	k7c2	bez
předchozí	předchozí	k2eAgFnSc2d1	předchozí
zkušenosti	zkušenost	k1gFnSc2	zkušenost
s	s	k7c7	s
politickou	politický	k2eAgFnSc7d1	politická
či	či	k8xC	či
armádní	armádní	k2eAgFnSc7d1	armádní
funkcí	funkce	k1gFnSc7	funkce
<g/>
.	.	kIx.	.
<g/>
Kandidaturu	kandidatura	k1gFnSc4	kandidatura
do	do	k7c2	do
amerických	americký	k2eAgFnPc2d1	americká
prezidentských	prezidentský	k2eAgFnPc2d1	prezidentská
voleb	volba	k1gFnPc2	volba
2016	[number]	k4	2016
za	za	k7c4	za
Republikánskou	republikánský	k2eAgFnSc4d1	republikánská
stranu	strana	k1gFnSc4	strana
formálně	formálně	k6eAd1	formálně
oznámil	oznámit	k5eAaPmAgInS	oznámit
16	[number]	k4	16
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2015	[number]	k4	2015
a	a	k8xC	a
po	po	k7c6	po
vítězných	vítězný	k2eAgFnPc6d1	vítězná
primárkách	primárky	k1gFnPc6	primárky
získal	získat	k5eAaPmAgMnS	získat
nominaci	nominace	k1gFnSc4	nominace
na	na	k7c4	na
úřad	úřad	k1gInSc4	úřad
prezidenta	prezident	k1gMnSc2	prezident
USA	USA	kA	USA
během	během	k7c2	během
srpnového	srpnový	k2eAgInSc2d1	srpnový
stranického	stranický	k2eAgInSc2d1	stranický
sjezdu	sjezd	k1gInSc2	sjezd
v	v	k7c6	v
Clevelandu	Cleveland	k1gInSc6	Cleveland
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
dne	den	k1gInSc2	den
8	[number]	k4	8
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2016	[number]	k4	2016
získal	získat	k5eAaPmAgInS	získat
306	[number]	k4	306
hlasů	hlas	k1gInPc2	hlas
volitelů	volitel	k1gMnPc2	volitel
(	(	kIx(	(
<g/>
oproti	oproti	k7c3	oproti
potřebnému	potřebné	k1gNnSc3	potřebné
počtu	počet	k1gInSc2	počet
270	[number]	k4	270
<g/>
)	)	kIx)	)
a	a	k8xC	a
porazil	porazit	k5eAaPmAgMnS	porazit
tak	tak	k9	tak
svou	svůj	k3xOyFgFnSc4	svůj
hlavní	hlavní	k2eAgFnSc4d1	hlavní
soupeřku	soupeřka	k1gFnSc4	soupeřka
<g/>
,	,	kIx,	,
demokratickou	demokratický	k2eAgFnSc4d1	demokratická
kandidátku	kandidátka	k1gFnSc4	kandidátka
Hillary	Hillara	k1gFnSc2	Hillara
Clintonovou	Clintonová	k1gFnSc7	Clintonová
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yRgFnSc4	který
připadlo	připadnout	k5eAaPmAgNnS	připadnout
232	[number]	k4	232
hlasů	hlas	k1gInPc2	hlas
volitelů	volitel	k1gMnPc2	volitel
<g/>
.	.	kIx.	.
<g/>
Před	před	k7c7	před
vstupem	vstup	k1gInSc7	vstup
do	do	k7c2	do
úřadu	úřad	k1gInSc2	úřad
působil	působit	k5eAaImAgMnS	působit
jako	jako	k8xC	jako
předseda	předseda	k1gMnSc1	předseda
a	a	k8xC	a
generální	generální	k2eAgMnSc1d1	generální
ředitel	ředitel	k1gMnSc1	ředitel
společnosti	společnost	k1gFnSc2	společnost
The	The	k1gMnSc1	The
Trump	Trump	k1gMnSc1	Trump
Organization	Organization	k1gInSc4	Organization
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
obchodováním	obchodování	k1gNnSc7	obchodování
s	s	k7c7	s
nemovitostmi	nemovitost	k1gFnPc7	nemovitost
<g/>
.	.	kIx.	.
</s>
<s>
Provozuje	provozovat	k5eAaImIp3nS	provozovat
také	také	k9	také
mnoho	mnoho	k4c1	mnoho
hotelů	hotel	k1gInPc2	hotel
<g/>
,	,	kIx,	,
kasin	kasino	k1gNnPc2	kasino
a	a	k8xC	a
golfových	golfový	k2eAgNnPc2d1	golfové
hřišť	hřiště	k1gNnPc2	hřiště
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Díky	dík	k1gInPc1	dík
své	svůj	k3xOyFgFnSc2	svůj
reality	realita	k1gFnSc2	realita
show	show	k1gFnSc1	show
The	The	k1gFnSc1	The
Apprentice	Apprentice	k1gFnSc1	Apprentice
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
Učeň	učeň	k1gMnSc1	učeň
<g/>
)	)	kIx)	)
na	na	k7c6	na
stanici	stanice	k1gFnSc6	stanice
NBC	NBC	kA	NBC
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
i	i	k9	i
mediálně	mediálně	k6eAd1	mediálně
známým	známý	k2eAgMnPc3d1	známý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
prvních	první	k4xOgInPc6	první
měsících	měsíc	k1gInPc6	měsíc
svého	svůj	k3xOyFgNnSc2	svůj
úřadování	úřadování	k1gNnSc2	úřadování
Trump	Trump	k1gMnSc1	Trump
zvrátil	zvrátit	k5eAaPmAgMnS	zvrátit
některé	některý	k3yIgFnPc4	některý
z	z	k7c2	z
kroků	krok	k1gInPc2	krok
svého	svůj	k3xOyFgMnSc2	svůj
předchůdce	předchůdce	k1gMnSc2	předchůdce
Baracka	Baracko	k1gNnSc2	Baracko
Obamy	Obama	k1gFnSc2	Obama
<g/>
.	.	kIx.	.
</s>
<s>
Odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
od	od	k7c2	od
dohody	dohoda	k1gFnSc2	dohoda
o	o	k7c6	o
transpacifickém	transpacifický	k2eAgNnSc6d1	transpacifický
partnerství	partnerství	k1gNnSc6	partnerství
a	a	k8xC	a
od	od	k7c2	od
pařížské	pařížský	k2eAgFnSc2d1	Pařížská
dohody	dohoda	k1gFnSc2	dohoda
o	o	k7c6	o
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
klimatickým	klimatický	k2eAgFnPc3d1	klimatická
změnám	změna	k1gFnPc3	změna
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
částečně	částečně	k6eAd1	částečně
zastavil	zastavit	k5eAaPmAgMnS	zastavit
postupné	postupný	k2eAgNnSc4d1	postupné
oteplování	oteplování	k1gNnSc4	oteplování
americko-kubánských	americkoubánský	k2eAgInPc2d1	americko-kubánský
vztahů	vztah	k1gInPc2	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Trump	Trump	k1gMnSc1	Trump
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
konzervativního	konzervativní	k2eAgMnSc4d1	konzervativní
soudce	soudce	k1gMnSc4	soudce
Neila	Neil	k1gMnSc4	Neil
Gorsucha	Gorsuch	k1gMnSc4	Gorsuch
jako	jako	k9	jako
devátého	devátý	k4xOgMnSc4	devátý
člena	člen	k1gMnSc4	člen
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
<g/>
,	,	kIx,	,
Senát	senát	k1gInSc1	senát
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
jeho	on	k3xPp3gInSc2	on
nominaci	nominace	k1gFnSc4	nominace
následně	následně	k6eAd1	následně
i	i	k9	i
přes	přes	k7c4	přes
obstrukce	obstrukce	k1gFnPc4	obstrukce
demokratů	demokrat	k1gMnPc2	demokrat
schválil	schválit	k5eAaPmAgInS	schválit
<g/>
.	.	kIx.	.
</s>
<s>
Trump	Trump	k1gMnSc1	Trump
dále	daleko	k6eAd2	daleko
nastolil	nastolit	k5eAaPmAgMnS	nastolit
částečný	částečný	k2eAgMnSc1d1	částečný
travel	travel	k1gMnSc1	travel
ban	ban	k?	ban
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
měsíce	měsíc	k1gInPc4	měsíc
zabránil	zabránit	k5eAaPmAgMnS	zabránit
občanům	občan	k1gMnPc3	občan
ze	z	k7c2	z
šesti	šest	k4xCc2	šest
muslimských	muslimský	k2eAgFnPc2d1	muslimská
zemí	zem	k1gFnPc2	zem
ve	v	k7c6	v
vstupu	vstup	k1gInSc6	vstup
na	na	k7c6	na
území	území	k1gNnSc6	území
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
předmětem	předmět	k1gInSc7	předmět
vícero	vícero	k1gNnSc4	vícero
soudních	soudní	k2eAgInPc2d1	soudní
sporů	spor	k1gInPc2	spor
<g/>
.	.	kIx.	.
</s>
<s>
Trumpova	Trumpův	k2eAgFnSc1d1	Trumpova
snaha	snaha	k1gFnSc1	snaha
o	o	k7c4	o
zastavení	zastavení	k1gNnSc4	zastavení
nebo	nebo	k8xC	nebo
omezení	omezení	k1gNnSc4	omezení
programu	program	k1gInSc2	program
rozsáhlé	rozsáhlý	k2eAgFnSc2d1	rozsáhlá
zdravotní	zdravotní	k2eAgFnSc2d1	zdravotní
péče	péče	k1gFnSc2	péče
i	i	k9	i
pro	pro	k7c4	pro
nemajetné	majetný	k2eNgMnPc4d1	nemajetný
občany	občan	k1gMnPc4	občan
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Obamacare	Obamacar	k1gMnSc5	Obamacar
<g/>
)	)	kIx)	)
však	však	k9	však
narazila	narazit	k5eAaPmAgFnS	narazit
na	na	k7c4	na
velký	velký	k2eAgInSc4d1	velký
odpor	odpor	k1gInSc4	odpor
a	a	k8xC	a
prozatím	prozatím	k6eAd1	prozatím
neuspěla	uspět	k5eNaPmAgFnS	uspět
kvůli	kvůli	k7c3	kvůli
odlišnému	odlišný	k2eAgInSc3d1	odlišný
postoji	postoj	k1gInSc3	postoj
několika	několik	k4yIc2	několik
republikánských	republikánský	k2eAgMnPc2d1	republikánský
senátorů	senátor	k1gMnPc2	senátor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2017	[number]	k4	2017
propustil	propustit	k5eAaPmAgInS	propustit
Trump	Trump	k1gInSc1	Trump
z	z	k7c2	z
úřadu	úřad	k1gInSc2	úřad
ředitele	ředitel	k1gMnSc2	ředitel
FBI	FBI	kA	FBI
Jamese	Jamese	k1gFnSc2	Jamese
Comeyho	Comey	k1gMnSc2	Comey
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
byl	být	k5eAaImAgInS	být
ministerstvem	ministerstvo	k1gNnSc7	ministerstvo
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
jmenován	jmenován	k2eAgMnSc1d1	jmenován
Robert	Robert	k1gMnSc1	Robert
Mueller	Mueller	k1gMnSc1	Mueller
speciálním	speciální	k2eAgMnSc7d1	speciální
prokurátorem	prokurátor	k1gMnSc7	prokurátor
pro	pro	k7c4	pro
vyšetřování	vyšetřování	k1gNnSc4	vyšetřování
údajného	údajný	k2eAgNnSc2d1	údajné
ruského	ruský	k2eAgNnSc2d1	ruské
vměšování	vměšování	k1gNnSc2	vměšování
do	do	k7c2	do
prezidentských	prezidentský	k2eAgFnPc2d1	prezidentská
voleb	volba	k1gFnPc2	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
a	a	k8xC	a
případných	případný	k2eAgInPc2d1	případný
vztahů	vztah	k1gInPc2	vztah
mezi	mezi	k7c7	mezi
Ruskem	Rusko	k1gNnSc7	Rusko
a	a	k8xC	a
spolupracovníky	spolupracovník	k1gMnPc7	spolupracovník
Trumpovy	Trumpův	k2eAgFnSc2d1	Trumpova
prezidentské	prezidentský	k2eAgFnSc2d1	prezidentská
kampaně	kampaň	k1gFnSc2	kampaň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Původ	původ	k1gInSc1	původ
a	a	k8xC	a
příbuzní	příbuzný	k1gMnPc1	příbuzný
===	===	k?	===
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
newyorském	newyorský	k2eAgInSc6d1	newyorský
Queensu	Queens	k1gInSc6	Queens
jako	jako	k8xC	jako
čtvrté	čtvrtý	k4xOgFnPc1	čtvrtý
z	z	k7c2	z
pěti	pět	k4xCc2	pět
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc7	jeho
rodiči	rodič	k1gMnPc7	rodič
byli	být	k5eAaImAgMnP	být
Frederic	Frederic	k1gMnSc1	Frederic
C.	C.	kA	C.
Trump	Trump	k1gMnSc1	Trump
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1905	[number]	k4	1905
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
–	–	k?	–
25	[number]	k4	25
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
a	a	k8xC	a
Mary	Mary	k1gFnSc2	Mary
A.	A.	kA	A.
MacLeod	MacLeod	k1gInSc1	MacLeod
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1912	[number]	k4	1912
<g/>
,	,	kIx,	,
Skotsko	Skotsko	k1gNnSc1	Skotsko
–	–	k?	–
7	[number]	k4	7
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
velkým	velký	k2eAgMnSc7d1	velký
stavebním	stavební	k2eAgMnSc7d1	stavební
podnikatelem	podnikatel	k1gMnSc7	podnikatel
<g/>
.	.	kIx.	.
</s>
<s>
Prarodiče	prarodič	k1gMnSc2	prarodič
Donalda	Donald	k1gMnSc2	Donald
Trumpa	Trump	k1gMnSc2	Trump
z	z	k7c2	z
otcovy	otcův	k2eAgFnSc2d1	otcova
strany	strana	k1gFnSc2	strana
byli	být	k5eAaImAgMnP	být
původem	původ	k1gInSc7	původ
němečtí	německý	k2eAgMnPc1d1	německý
emigranti	emigrant	k1gMnPc1	emigrant
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
přistěhovali	přistěhovat	k5eAaPmAgMnP	přistěhovat
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1885	[number]	k4	1885
<g/>
.	.	kIx.	.
</s>
<s>
Dědeček	dědeček	k1gMnSc1	dědeček
Frederic	Frederic	k1gMnSc1	Frederic
Trump	Trump	k1gMnSc1	Trump
(	(	kIx(	(
<g/>
původním	původní	k2eAgNnSc7d1	původní
jménem	jméno	k1gNnSc7	jméno
Friedrich	Friedrich	k1gMnSc1	Friedrich
Trump	Trump	k1gMnSc1	Trump
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgInS	narodit
v	v	k7c6	v
malé	malý	k2eAgFnSc6d1	malá
vinařské	vinařský	k2eAgFnSc6d1	vinařská
obci	obec	k1gFnSc6	obec
Kallstadt	Kallstadt	k1gMnSc1	Kallstadt
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
německé	německý	k2eAgFnSc2d1	německá
spolkové	spolkový	k2eAgFnSc2d1	spolková
země	zem	k1gFnSc2	zem
Porýní-Falc	Porýní-Falc	k1gFnSc1	Porýní-Falc
<g/>
.	.	kIx.	.
</s>
<s>
Babička	babička	k1gFnSc1	babička
se	se	k3xPyFc4	se
jmenovala	jmenovat	k5eAaBmAgFnS	jmenovat
Elisabeth	Elisabeth	k1gInSc4	Elisabeth
roz	roz	k?	roz
<g/>
.	.	kIx.	.
</s>
<s>
Christ	Christ	k1gMnSc1	Christ
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k9	již
dědeček	dědeček	k1gMnSc1	dědeček
Trump	Trump	k1gMnSc1	Trump
nabyl	nabýt	k5eAaPmAgMnS	nabýt
značného	značný	k2eAgInSc2d1	značný
majetku	majetek	k1gInSc2	majetek
jako	jako	k8xC	jako
majitel	majitel	k1gMnSc1	majitel
řetězce	řetězec	k1gInSc2	řetězec
pohostinství	pohostinství	k1gNnSc2	pohostinství
mj.	mj.	kA	mj.
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Seattle	Seattle	k1gFnSc2	Seattle
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Washington	Washington	k1gInSc1	Washington
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vzdělání	vzdělání	k1gNnSc1	vzdělání
===	===	k?	===
</s>
</p>
<p>
<s>
Mladý	mladý	k2eAgMnSc1d1	mladý
Trump	Trump	k1gMnSc1	Trump
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c4	na
Kew-Forest	Kew-Forest	k1gInSc4	Kew-Forest
School	Schoola	k1gFnPc2	Schoola
ve	v	k7c4	v
Forest	Forest	k1gFnSc4	Forest
Hills	Hillsa	k1gFnPc2	Hillsa
<g/>
,	,	kIx,	,
v	v	k7c6	v
newyorské	newyorský	k2eAgFnSc6d1	newyorská
městské	městský	k2eAgFnSc6d1	městská
části	část	k1gFnSc6	část
Queens	Queensa	k1gFnPc2	Queensa
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
problémům	problém	k1gInPc3	problém
při	při	k7c6	při
studiu	studio	k1gNnSc6	studio
jej	on	k3xPp3gMnSc4	on
rodiče	rodič	k1gMnSc4	rodič
ve	v	k7c6	v
třinácti	třináct	k4xCc6	třináct
letech	let	k1gInPc6	let
poslali	poslat	k5eAaPmAgMnP	poslat
na	na	k7c4	na
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
akademii	akademie	k1gFnSc4	akademie
(	(	kIx(	(
<g/>
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
Military	Militara	k1gFnSc2	Militara
Academy	Academa	k1gFnSc2	Academa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
získal	získat	k5eAaPmAgMnS	získat
akademické	akademický	k2eAgNnSc4d1	akademické
vzdělání	vzdělání	k1gNnSc4	vzdělání
a	a	k8xC	a
také	také	k9	také
hrál	hrát	k5eAaImAgMnS	hrát
v	v	k7c6	v
týmech	tým	k1gInPc6	tým
amerického	americký	k2eAgInSc2d1	americký
fotbalu	fotbal	k1gInSc2	fotbal
a	a	k8xC	a
basketbalu	basketbal	k1gInSc2	basketbal
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
basketbalového	basketbalový	k2eAgMnSc2d1	basketbalový
trenéra	trenér	k1gMnSc2	trenér
dostal	dostat	k5eAaPmAgInS	dostat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
cenu	cena	k1gFnSc4	cena
Coach	Coacha	k1gFnPc2	Coacha
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Award	Awarda	k1gFnPc2	Awarda
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
studoval	studovat	k5eAaImAgMnS	studovat
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
na	na	k7c4	na
Fordham	Fordham	k1gInSc4	Fordham
University	universita	k1gFnSc2	universita
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
přestoupil	přestoupit	k5eAaPmAgMnS	přestoupit
na	na	k7c4	na
Wharton	Wharton	k1gInSc4	Wharton
School	Schoola	k1gFnPc2	Schoola
patřící	patřící	k2eAgMnSc1d1	patřící
k	k	k7c3	k
Pensylvánské	pensylvánský	k2eAgFnSc3d1	Pensylvánská
univerzitě	univerzita	k1gFnSc3	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
absolvování	absolvování	k1gNnSc6	absolvování
bakalářského	bakalářský	k2eAgNnSc2d1	bakalářské
studia	studio	k1gNnSc2	studio
(	(	kIx(	(
<g/>
BS	BS	kA	BS
<g/>
)	)	kIx)	)
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
ekonomie	ekonomie	k1gFnSc2	ekonomie
a	a	k8xC	a
finančnictví	finančnictví	k1gNnSc2	finančnictví
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
do	do	k7c2	do
otcovy	otcův	k2eAgFnSc2d1	otcova
firmy	firma	k1gFnSc2	firma
Trump	Trump	k1gMnSc1	Trump
Organization	Organization	k1gInSc1	Organization
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rodina	rodina	k1gFnSc1	rodina
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
se	se	k3xPyFc4	se
Donald	Donald	k1gMnSc1	Donald
Trump	Trump	k1gMnSc1	Trump
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
českou	český	k2eAgFnSc7d1	Česká
modelkou	modelka	k1gFnSc7	modelka
Ivanou	Ivana	k1gFnSc7	Ivana
Zelníčkovou	Zelníčková	k1gFnSc7	Zelníčková
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
manželství	manželství	k1gNnSc2	manželství
se	se	k3xPyFc4	se
narodily	narodit	k5eAaPmAgFnP	narodit
tři	tři	k4xCgFnPc1	tři
děti	dítě	k1gFnPc1	dítě
<g/>
:	:	kIx,	:
Donald	Donald	k1gMnSc1	Donald
Jr	Jr	k1gMnSc1	Jr
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Ivanka	Ivanka	k1gFnSc1	Ivanka
a	a	k8xC	a
Eric	Eric	k1gFnSc1	Eric
Trumpovi	Trumpovi	k1gRnPc1	Trumpovi
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgMnSc1d3	nejstarší
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
Donald	Donald	k1gMnSc1	Donald
Jr	Jr	k1gMnSc1	Jr
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
hovoří	hovořit	k5eAaImIp3nS	hovořit
plynně	plynně	k6eAd1	plynně
česky	česky	k6eAd1	česky
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
měl	mít	k5eAaImAgMnS	mít
v	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
úzký	úzký	k2eAgInSc4d1	úzký
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
prarodičům	prarodič	k1gMnPc3	prarodič
z	z	k7c2	z
matčiny	matčin	k2eAgFnSc2d1	matčina
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
také	také	k9	také
pravidelně	pravidelně	k6eAd1	pravidelně
trávil	trávit	k5eAaImAgInS	trávit
část	část	k1gFnSc4	část
letních	letní	k2eAgFnPc2d1	letní
prázdnin	prázdniny	k1gFnPc2	prázdniny
ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
<g/>
.	.	kIx.	.
<g/>
Československá	československý	k2eAgFnSc1d1	Československá
Státní	státní	k2eAgFnSc1d1	státní
bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
vedla	vést	k5eAaImAgFnS	vést
od	od	k7c2	od
dubna	duben	k1gInSc2	duben
1978	[number]	k4	1978
sledovací	sledovací	k2eAgInSc1d1	sledovací
svazek	svazek	k1gInSc1	svazek
na	na	k7c4	na
tehdejší	tehdejší	k2eAgFnSc4d1	tehdejší
Trumpovu	Trumpův	k2eAgFnSc4d1	Trumpova
manželku	manželka	k1gFnSc4	manželka
Ivanu	Ivan	k1gMnSc3	Ivan
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
té	ten	k3xDgFnSc6	ten
příležitosti	příležitost	k1gFnSc6	příležitost
získávala	získávat	k5eAaImAgFnS	získávat
také	také	k9	také
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
Trumpově	Trumpův	k2eAgNnSc6d1	Trumpovo
angažmá	angažmá	k1gNnSc6	angažmá
v	v	k7c6	v
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
americké	americký	k2eAgFnSc6d1	americká
politice	politika	k1gFnSc6	politika
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
tak	tak	k9	tak
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
informována	informovat	k5eAaBmNgFnS	informovat
o	o	k7c6	o
Trumpově	Trumpův	k2eAgInSc6d1	Trumpův
zájmu	zájem	k1gInSc6	zájem
ucházet	ucházet	k5eAaImF	ucházet
se	se	k3xPyFc4	se
o	o	k7c4	o
křeslo	křeslo	k1gNnSc4	křeslo
prezidenta	prezident	k1gMnSc2	prezident
USA	USA	kA	USA
už	už	k9	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
<g/>
Samotný	samotný	k2eAgMnSc1d1	samotný
Donald	Donald	k1gMnSc1	Donald
Trump	Trump	k1gMnSc1	Trump
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Československo	Československo	k1gNnSc1	Československo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
pohřbu	pohřeb	k1gInSc6	pohřeb
svého	svůj	k3xOyFgMnSc2	svůj
tchána	tchán	k1gMnSc2	tchán
Miloše	Miloš	k1gMnSc2	Miloš
Zelníčka	Zelníček	k1gMnSc2	Zelníček
<g/>
.	.	kIx.	.
</s>
<s>
Manželé	manžel	k1gMnPc1	manžel
Donald	Donald	k1gMnSc1	Donald
a	a	k8xC	a
Ivana	Ivana	k1gFnSc1	Ivana
Trumpovi	Trumpův	k2eAgMnPc1d1	Trumpův
se	se	k3xPyFc4	se
rozvedli	rozvést	k5eAaPmAgMnP	rozvést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
si	se	k3xPyFc3	se
Trump	Trump	k1gInSc1	Trump
vzal	vzít	k5eAaPmAgInS	vzít
herečku	herečka	k1gFnSc4	herečka
Marlu	Marl	k1gInSc2	Marl
Maplesovou	Maplesový	k2eAgFnSc7d1	Maplesová
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
má	mít	k5eAaImIp3nS	mít
dceru	dcera	k1gFnSc4	dcera
Tiffany	Tiffana	k1gFnSc2	Tiffana
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
rozvod	rozvod	k1gInSc1	rozvod
následoval	následovat	k5eAaImAgInS	následovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
šest	šest	k4xCc4	šest
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
Trump	Trump	k1gMnSc1	Trump
oženil	oženit	k5eAaPmAgMnS	oženit
potřetí	potřetí	k4xO	potřetí
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
se	s	k7c7	s
slovinskou	slovinský	k2eAgFnSc7d1	slovinská
modelkou	modelka	k1gFnSc7	modelka
a	a	k8xC	a
návrhářkou	návrhářka	k1gFnSc7	návrhářka
Melanií	Melanie	k1gFnSc7	Melanie
Knavsovou	Knavsová	k1gFnSc7	Knavsová
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
přivedla	přivést	k5eAaPmAgFnS	přivést
na	na	k7c4	na
svět	svět	k1gInSc4	svět
syna	syn	k1gMnSc2	syn
Barrona	Barron	k1gMnSc2	Barron
Williama	William	k1gMnSc2	William
Trumpa	Trump	k1gMnSc2	Trump
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Podnikatelská	podnikatelský	k2eAgFnSc1d1	podnikatelská
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Počátky	počátek	k1gInPc4	počátek
podnikatelské	podnikatelský	k2eAgFnSc2d1	podnikatelská
kariéry	kariéra	k1gFnSc2	kariéra
===	===	k?	===
</s>
</p>
<p>
<s>
Trump	Trump	k1gInSc1	Trump
začal	začít	k5eAaPmAgInS	začít
svou	svůj	k3xOyFgFnSc4	svůj
podnikatelskou	podnikatelský	k2eAgFnSc4d1	podnikatelská
kariéru	kariéra	k1gFnSc4	kariéra
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
(	(	kIx(	(
<g/>
Trump	Trump	k1gInSc1	Trump
Organization	Organization	k1gInSc1	Organization
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
začátek	začátek	k1gInSc4	začátek
jeho	on	k3xPp3gNnSc2	on
vlastního	vlastní	k2eAgNnSc2d1	vlastní
podnikání	podnikání	k1gNnSc2	podnikání
mu	on	k3xPp3gMnSc3	on
otec	otec	k1gMnSc1	otec
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
úvěr	úvěr	k1gInSc4	úvěr
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
jednoho	jeden	k4xCgInSc2	jeden
milionu	milion	k4xCgInSc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gInPc2	jeho
prvních	první	k4xOgInPc2	první
projektů	projekt	k1gInPc2	projekt
byla	být	k5eAaImAgFnS	být
revitalizace	revitalizace	k1gFnSc1	revitalizace
bytového	bytový	k2eAgInSc2d1	bytový
komplexu	komplex	k1gInSc2	komplex
Swifton	Swifton	k1gInSc1	Swifton
Village	Villag	k1gFnSc2	Villag
v	v	k7c6	v
Cincinnati	Cincinnati	k1gFnSc6	Cincinnati
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Ohio	Ohio	k1gNnSc1	Ohio
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
předal	předat	k5eAaPmAgInS	předat
Swifton	Swifton	k1gInSc1	Swifton
Village	Villag	k1gFnSc2	Villag
firmě	firma	k1gFnSc3	firma
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
<g/>
,	,	kIx,	,
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
zisku	zisk	k1gInSc2	zisk
6	[number]	k4	6
milionů	milion	k4xCgInPc2	milion
amerických	americký	k2eAgInPc2d1	americký
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
v	v	k7c6	v
New	New	k1gMnSc6	New
Yorku	York	k1gInSc2	York
začaly	začít	k5eAaPmAgFnP	začít
práce	práce	k1gFnPc1	práce
na	na	k7c4	na
obnovení	obnovení	k1gNnSc4	obnovení
Wollman	Wollman	k1gMnSc1	Wollman
Stadium	stadium	k1gNnSc1	stadium
(	(	kIx(	(
<g/>
stadionu	stadion	k1gInSc2	stadion
<g/>
)	)	kIx)	)
v	v	k7c6	v
Central	Central	k1gFnSc6	Central
Parku	park	k1gInSc2	park
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
měl	mít	k5eAaImAgInS	mít
předpokládaný	předpokládaný	k2eAgInSc1d1	předpokládaný
stavební	stavební	k2eAgInSc1d1	stavební
plán	plán	k1gInSc1	plán
na	na	k7c4	na
2	[number]	k4	2
<g/>
1⁄	1⁄	k?	1⁄
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Jenže	jenže	k8xC	jenže
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
nebyli	být	k5eNaImAgMnP	být
ani	ani	k8xC	ani
blízko	blízko	k7c2	blízko
dokončení	dokončení	k1gNnSc2	dokončení
a	a	k8xC	a
už	už	k6eAd1	už
proinvestovali	proinvestovat	k5eAaPmAgMnP	proinvestovat
12	[number]	k4	12
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Trump	Trump	k1gMnSc1	Trump
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
převzetí	převzetí	k1gNnSc4	převzetí
těchto	tento	k3xDgFnPc2	tento
prací	práce	k1gFnPc2	práce
bez	bez	k7c2	bez
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
město	město	k1gNnSc1	město
muselo	muset	k5eAaImAgNnS	muset
něco	něco	k3yInSc1	něco
zaplatit	zaplatit	k5eAaPmF	zaplatit
<g/>
.	.	kIx.	.
</s>
<s>
Nabídka	nabídka	k1gFnSc1	nabídka
byla	být	k5eAaImAgFnS	být
odmítána	odmítat	k5eAaImNgFnS	odmítat
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
se	se	k3xPyFc4	se
nedostala	dostat	k5eNaPmAgFnS	dostat
do	do	k7c2	do
pozornosti	pozornost	k1gFnSc2	pozornost
médií	médium	k1gNnPc2	médium
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
dostal	dostat	k5eAaPmAgMnS	dostat
příležitost	příležitost	k1gFnSc4	příležitost
a	a	k8xC	a
projekt	projekt	k1gInSc4	projekt
dokončil	dokončit	k5eAaPmAgMnS	dokončit
během	během	k7c2	během
tří	tři	k4xCgInPc2	tři
měsíců	měsíc	k1gInPc2	měsíc
za	za	k7c4	za
1,95	[number]	k4	1,95
milionu	milion	k4xCgInSc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
o	o	k7c4	o
750	[number]	k4	750
000	[number]	k4	000
dolarů	dolar	k1gInPc2	dolar
méně	málo	k6eAd2	málo
než	než	k8xS	než
původní	původní	k2eAgInSc1d1	původní
rozpočet	rozpočet	k1gInSc1	rozpočet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
finanční	finanční	k2eAgFnSc2d1	finanční
krize	krize	k1gFnSc2	krize
<g/>
.	.	kIx.	.
</s>
<s>
Výstavbu	výstavba	k1gFnSc4	výstavba
svého	svůj	k3xOyFgInSc2	svůj
třetího	třetí	k4xOgInSc2	třetí
kasína	kasín	k1gInSc2	kasín
<g/>
,	,	kIx,	,
Trump	Trump	k1gInSc1	Trump
Taj	taj	k1gInSc1	taj
Mahal	Mahal	k1gInSc4	Mahal
v	v	k7c4	v
Atlantic	Atlantice	k1gFnPc2	Atlantice
City	City	k1gFnSc2	City
(	(	kIx(	(
<g/>
New	New	k1gFnSc1	New
Jersey	Jersea	k1gFnSc2	Jersea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
financoval	financovat	k5eAaBmAgInS	financovat
většinou	většinou	k6eAd1	většinou
z	z	k7c2	z
půjček	půjčka	k1gFnPc2	půjčka
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
úrokem	úrok	k1gInSc7	úrok
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
následně	následně	k6eAd1	následně
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
splácet	splácet	k5eAaImF	splácet
<g/>
.	.	kIx.	.
</s>
<s>
Banky	banka	k1gFnPc1	banka
a	a	k8xC	a
držitelé	držitel	k1gMnPc1	držitel
dluhopisů	dluhopis	k1gInPc2	dluhopis
přitom	přitom	k6eAd1	přitom
ztratili	ztratit	k5eAaPmAgMnP	ztratit
stovky	stovka	k1gFnPc4	stovka
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
50	[number]	k4	50
%	%	kIx~	%
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
kasína	kasín	k1gInSc2	kasín
Taj	taj	k1gInSc1	taj
Mahal	Mahal	k1gInSc1	Mahal
musel	muset	k5eAaImAgInS	muset
převést	převést	k5eAaPmF	převést
na	na	k7c4	na
původní	původní	k2eAgMnPc4d1	původní
držitele	držitel	k1gMnPc4	držitel
dluhopisů	dluhopis	k1gInPc2	dluhopis
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
snížil	snížit	k5eAaPmAgInS	snížit
úrokové	úrokový	k2eAgNnSc4d1	úrokové
zatížení	zatížení	k1gNnSc4	zatížení
a	a	k8xC	a
dostal	dostat	k5eAaPmAgMnS	dostat
více	hodně	k6eAd2	hodně
času	čas	k1gInSc2	čas
na	na	k7c6	na
splacení	splacení	k1gNnSc6	splacení
dluhu	dluh	k1gInSc2	dluh
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
značně	značně	k6eAd1	značně
snížit	snížit	k5eAaPmF	snížit
svůj	svůj	k3xOyFgInSc4	svůj
900	[number]	k4	900
<g/>
milionový	milionový	k2eAgInSc4d1	milionový
osobní	osobní	k2eAgInSc4d1	osobní
dluh	dluh	k1gInSc4	dluh
a	a	k8xC	a
též	též	k9	též
výrazně	výrazně	k6eAd1	výrazně
snížil	snížit	k5eAaPmAgInS	snížit
svůj	svůj	k3xOyFgInSc4	svůj
3,5	[number]	k4	3,5
<g/>
miliardový	miliardový	k2eAgInSc4d1	miliardový
obchodní	obchodní	k2eAgInSc4d1	obchodní
dluh	dluh	k1gInSc4	dluh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
spojil	spojit	k5eAaPmAgMnS	spojit
svoje	své	k1gNnSc4	své
kasína	kasín	k1gMnSc2	kasín
do	do	k7c2	do
společnosti	společnost	k1gFnSc2	společnost
Trump	Trump	k1gMnSc1	Trump
Hotels	Hotelsa	k1gFnPc2	Hotelsa
&	&	k?	&
Casino	Casino	k1gNnSc4	Casino
Resorts	Resorts	k1gInSc4	Resorts
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Podnikatelské	podnikatelský	k2eAgInPc4d1	podnikatelský
úspěchy	úspěch	k1gInPc4	úspěch
===	===	k?	===
</s>
</p>
<p>
<s>
Trump	Trump	k1gMnSc1	Trump
je	být	k5eAaImIp3nS	být
předsedou	předseda	k1gMnSc7	předseda
a	a	k8xC	a
generálním	generální	k2eAgMnSc7d1	generální
ředitelem	ředitel	k1gMnSc7	ředitel
(	(	kIx(	(
<g/>
Chief	Chief	k1gMnSc1	Chief
Executive	Executiv	k1gInSc5	Executiv
Officer	Officer	k1gMnSc1	Officer
<g/>
,	,	kIx,	,
CEO	CEO	kA	CEO
<g/>
)	)	kIx)	)
společnosti	společnost	k1gFnSc2	společnost
Trump	Trump	k1gInSc1	Trump
Organization	Organization	k1gInSc1	Organization
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
obchodováním	obchodování	k1gNnSc7	obchodování
s	s	k7c7	s
nemovitostmi	nemovitost	k1gFnPc7	nemovitost
<g/>
.	.	kIx.	.
</s>
<s>
Trump	Trump	k1gInSc1	Trump
je	být	k5eAaImIp3nS	být
též	též	k9	též
zakladatelem	zakladatel	k1gMnSc7	zakladatel
společnosti	společnost	k1gFnSc2	společnost
Trump	Trump	k1gMnSc1	Trump
Entertainment	Entertainment	k1gMnSc1	Entertainment
Resorts	Resortsa	k1gFnPc2	Resortsa
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
provozuje	provozovat	k5eAaImIp3nS	provozovat
mnoho	mnoho	k4c4	mnoho
kasin	kasino	k1gNnPc2	kasino
a	a	k8xC	a
hotelů	hotel	k1gInPc2	hotel
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2016	[number]	k4	2016
byl	být	k5eAaImAgMnS	být
Donald	Donald	k1gMnSc1	Donald
Trump	Trump	k1gMnSc1	Trump
podle	podle	k7c2	podle
seznamu	seznam	k1gInSc2	seznam
časopisu	časopis	k1gInSc2	časopis
Forbes	forbes	k1gInSc1	forbes
499	[number]	k4	499
<g/>
.	.	kIx.	.
nejbohatším	bohatý	k2eAgMnSc7d3	nejbohatší
člověkem	člověk	k1gMnSc7	člověk
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgInPc4d1	další
Trumpovy	Trumpův	k2eAgInPc4d1	Trumpův
fungující	fungující	k2eAgInPc4d1	fungující
projekty	projekt	k1gInPc4	projekt
–	–	k?	–
např.	např.	kA	např.
Trump	Trump	k1gMnSc1	Trump
Financial	Financial	k1gMnSc1	Financial
(	(	kIx(	(
<g/>
hypoteční	hypoteční	k2eAgFnSc1d1	hypoteční
firma	firma	k1gFnSc1	firma
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Trump	Trump	k1gMnSc1	Trump
Sales	Sales	k1gMnSc1	Sales
and	and	k?	and
Leasing	leasing	k1gInSc1	leasing
(	(	kIx(	(
<g/>
rezidentní	rezidentní	k2eAgFnSc2d1	rezidentní
prodeje	prodej	k1gFnSc2	prodej
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Trump	Trump	k1gMnSc1	Trump
International	International	k1gFnSc2	International
Realty	Realt	k1gInPc1	Realt
(	(	kIx(	(
<g/>
rezidenční	rezidenční	k2eAgFnPc1d1	rezidenční
a	a	k8xC	a
komerční	komerční	k2eAgFnPc1d1	komerční
nemovitosti	nemovitost	k1gFnPc1	nemovitost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Trump	Trump	k1gInSc1	Trump
Restaurants	Restaurants	k1gInSc1	Restaurants
<g/>
,	,	kIx,	,
Go	Go	k1gMnSc1	Go
Trump	Trump	k1gMnSc1	Trump
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
on-line	onin	k1gInSc5	on-lin
cestovní	cestovní	k2eAgMnSc1d1	cestovní
vyhledávač	vyhledávač	k1gMnSc1	vyhledávač
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Select	Select	k1gInSc1	Select
By	by	k9	by
Trump	Trump	k1gInSc1	Trump
(	(	kIx(	(
<g/>
řada	řada	k1gFnSc1	řada
kávových	kávový	k2eAgInPc2d1	kávový
nápojů	nápoj	k1gInPc2	nápoj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Trump	Trump	k1gInSc1	Trump
Drinks	Drinks	k1gInSc1	Drinks
(	(	kIx(	(
<g/>
energetický	energetický	k2eAgInSc1d1	energetický
nápoj	nápoj	k1gInSc1	nápoj
pro	pro	k7c4	pro
izraelský	izraelský	k2eAgInSc4d1	izraelský
a	a	k8xC	a
palestinský	palestinský	k2eAgInSc4d1	palestinský
trh	trh	k1gInSc4	trh
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Donald	Donald	k1gMnSc1	Donald
J.	J.	kA	J.
Trump	Trump	k1gMnSc1	Trump
Signature	Signatur	k1gMnSc5	Signatur
Collection	Collection	k1gInSc1	Collection
(	(	kIx(	(
<g/>
řada	řada	k1gFnSc1	řada
pánského	pánský	k2eAgNnSc2d1	pánské
oblečení	oblečení	k1gNnSc2	oblečení
a	a	k8xC	a
hodinky	hodinka	k1gFnSc2	hodinka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Trump	Trump	k1gMnSc1	Trump
Ice	Ice	k1gMnSc1	Ice
(	(	kIx(	(
<g/>
balená	balený	k2eAgFnSc1d1	balená
voda	voda	k1gFnSc1	voda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Trump	Trump	k1gInSc1	Trump
Golf	golf	k1gInSc1	golf
<g/>
,	,	kIx,	,
Trump	Trump	k1gInSc1	Trump
Chocolate	Chocolat	k1gInSc5	Chocolat
<g/>
,	,	kIx,	,
Trump	Trump	k1gInSc1	Trump
Productions	Productions	k1gInSc1	Productions
(	(	kIx(	(
<g/>
televizní	televizní	k2eAgFnSc1d1	televizní
produkční	produkční	k2eAgFnSc1d1	produkční
společnost	společnost	k1gFnSc1	společnost
<g/>
)	)	kIx)	)
anebo	anebo	k8xC	anebo
Trump	Trump	k1gMnSc1	Trump
Home	Hom	k1gFnSc2	Hom
(	(	kIx(	(
<g/>
bytové	bytový	k2eAgNnSc1d1	bytové
zařízení	zařízení	k1gNnSc1	zařízení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Podnikatelské	podnikatelský	k2eAgInPc4d1	podnikatelský
neúspěchy	neúspěch	k1gInPc4	neúspěch
===	===	k?	===
</s>
</p>
<p>
<s>
Donald	Donald	k1gMnSc1	Donald
Trump	Trump	k1gMnSc1	Trump
se	se	k3xPyFc4	se
vydává	vydávat	k5eAaImIp3nS	vydávat
za	za	k7c4	za
znalého	znalý	k2eAgMnSc4d1	znalý
a	a	k8xC	a
chytrého	chytrý	k2eAgMnSc4d1	chytrý
obchodníka	obchodník	k1gMnSc4	obchodník
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
kariéru	kariéra	k1gFnSc4	kariéra
šestkrát	šestkrát	k6eAd1	šestkrát
zbankrotoval	zbankrotovat	k5eAaPmAgInS	zbankrotovat
a	a	k8xC	a
přišel	přijít	k5eAaPmAgInS	přijít
o	o	k7c4	o
miliardy	miliarda	k4xCgFnPc4	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Trump	Trump	k1gMnSc1	Trump
se	se	k3xPyFc4	se
o	o	k7c6	o
sobě	se	k3xPyFc3	se
nechává	nechávat	k5eAaImIp3nS	nechávat
slyšet	slyšet	k5eAaImF	slyšet
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vypracoval	vypracovat	k5eAaPmAgMnS	vypracovat
sám	sám	k3xTgMnSc1	sám
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
Fred	Fred	k1gMnSc1	Fred
Trump	Trump	k1gMnSc1	Trump
<g/>
,	,	kIx,	,
mu	on	k3xPp3gMnSc3	on
mezi	mezi	k7c4	mezi
3	[number]	k4	3
a	a	k8xC	a
58	[number]	k4	58
lety	léto	k1gNnPc7	léto
věku	věk	k1gInSc6	věk
dal	dát	k5eAaPmAgMnS	dát
nebo	nebo	k8xC	nebo
půjčil	půjčit	k5eAaPmAgMnS	půjčit
celkem	celek	k1gInSc7	celek
413	[number]	k4	413
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kdyby	kdyby	kYmCp3nS	kdyby
Donald	Donald	k1gMnSc1	Donald
Trump	Trump	k1gMnSc1	Trump
tyto	tento	k3xDgInPc4	tento
půjčené	půjčený	k2eAgInPc4d1	půjčený
nebo	nebo	k8xC	nebo
darované	darovaný	k2eAgInPc4d1	darovaný
peníze	peníz	k1gInPc4	peníz
vložil	vložit	k5eAaPmAgMnS	vložit
do	do	k7c2	do
bezpečných	bezpečný	k2eAgFnPc2d1	bezpečná
<g/>
,	,	kIx,	,
nízkorizikových	nízkorizikův	k2eAgFnPc2d1	nízkorizikův
investic	investice	k1gFnPc2	investice
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
by	by	kYmCp3nS	by
nyní	nyní	k6eAd1	nyní
2	[number]	k4	2
mld.	mld.	k?	mld.
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
1985	[number]	k4	1985
<g/>
–	–	k?	–
<g/>
1994	[number]	k4	1994
ztratil	ztratit	k5eAaPmAgInS	ztratit
Trump	Trump	k1gInSc1	Trump
svými	svůj	k3xOyFgNnPc7	svůj
podnikatelskými	podnikatelský	k2eAgNnPc7d1	podnikatelské
rozhodnutími	rozhodnutí	k1gNnPc7	rozhodnutí
1,17	[number]	k4	1,17
miliardy	miliarda	k4xCgFnSc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Trump	Trump	k1gMnSc1	Trump
se	se	k3xPyFc4	se
konkrétně	konkrétně	k6eAd1	konkrétně
k	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
vyjádřil	vyjádřit	k5eAaPmAgInS	vyjádřit
ve	v	k7c6	v
tweetu	tweet	k1gInSc6	tweet
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
lze	lze	k6eAd1	lze
interpretovat	interpretovat	k5eAaBmF	interpretovat
jako	jako	k8xS	jako
přiznání	přiznání	k1gNnSc4	přiznání
k	k	k7c3	k
daňovým	daňový	k2eAgInPc3d1	daňový
únikům	únik	k1gInPc3	únik
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
a	a	k8xC	a
2009	[number]	k4	2009
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
bankrot	bankrot	k1gInSc1	bankrot
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
dva	dva	k4xCgInPc1	dva
bankroty	bankrot	k1gInPc1	bankrot
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
jeho	jeho	k3xOp3gFnPc7	jeho
nemovitostmi	nemovitost	k1gFnPc7	nemovitost
<g/>
:	:	kIx,	:
Trump	Trump	k1gInSc1	Trump
Taj	taj	k1gInSc1	taj
Mahal	Mahal	k1gInSc1	Mahal
a	a	k8xC	a
Trump	Trump	k1gInSc1	Trump
Plaza	plaz	k1gMnSc4	plaz
Hotel	hotel	k1gInSc1	hotel
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
odstupem	odstup	k1gInSc7	odstup
času	čas	k1gInSc2	čas
sám	sám	k3xTgInSc1	sám
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nechal	nechat	k5eAaPmAgMnS	nechat
postavit	postavit	k5eAaPmF	postavit
ona	onen	k3xDgFnSc1	onen
luxusní	luxusní	k2eAgFnSc1d1	luxusní
kasína	kasína	k1gFnSc1	kasína
<g/>
,	,	kIx,	,
příliš	příliš	k6eAd1	příliš
zpyšněl	zpyšnět	k5eAaPmAgMnS	zpyšnět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc4	jeho
osobní	osobní	k2eAgInSc1d1	osobní
dluh	dluh	k1gInSc1	dluh
vyšplhal	vyšplhat	k5eAaPmAgInS	vyšplhat
na	na	k7c4	na
900	[number]	k4	900
milionů	milion	k4xCgInPc2	milion
a	a	k8xC	a
obchodní	obchodní	k2eAgInSc4d1	obchodní
dluh	dluh	k1gInSc4	dluh
3,5	[number]	k4	3,5
mld.	mld.	k?	mld.
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
musel	muset	k5eAaImAgMnS	muset
prodat	prodat	k5eAaPmF	prodat
část	část	k1gFnSc4	část
svých	svůj	k3xOyFgFnPc2	svůj
firem	firma	k1gFnPc2	firma
a	a	k8xC	a
nemovitostí	nemovitost	k1gFnPc2	nemovitost
a	a	k8xC	a
vzdát	vzdát	k5eAaPmF	vzdát
se	se	k3xPyFc4	se
svého	svůj	k3xOyFgInSc2	svůj
podílu	podíl	k1gInSc2	podíl
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
společnostech	společnost	k1gFnPc6	společnost
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
finančnímu	finanční	k2eAgNnSc3d1	finanční
zotavení	zotavení	k1gNnSc3	zotavení
mu	on	k3xPp3gMnSc3	on
pomohly	pomoct	k5eAaPmAgInP	pomoct
mj.	mj.	kA	mj.
výhodné	výhodný	k2eAgFnPc4d1	výhodná
smlouvy	smlouva	k1gFnPc4	smlouva
u	u	k7c2	u
nemovitostí	nemovitost	k1gFnPc2	nemovitost
za	za	k7c4	za
možnost	možnost	k1gFnSc4	možnost
zobrazit	zobrazit	k5eAaPmF	zobrazit
na	na	k7c6	na
nich	on	k3xPp3gInPc6	on
nápis	nápis	k1gInSc1	nápis
"	"	kIx"	"
<g/>
TRUMP	TRUMP	kA	TRUMP
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Bankroty	bankrot	k1gInPc1	bankrot
z	z	k7c2	z
let	léto	k1gNnPc2	léto
2004	[number]	k4	2004
a	a	k8xC	a
2009	[number]	k4	2009
byly	být	k5eAaImAgFnP	být
opět	opět	k6eAd1	opět
spojeny	spojit	k5eAaPmNgFnP	spojit
s	s	k7c7	s
jeho	jeho	k3xOp3gFnPc7	jeho
nemovitostmi	nemovitost	k1gFnPc7	nemovitost
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
jinými	jiný	k2eAgInPc7d1	jiný
Trump	Trump	k1gInSc4	Trump
Taj	taj	k1gInSc1	taj
Mahal	Mahal	k1gInSc1	Mahal
a	a	k8xC	a
Trump	Trump	k1gInSc1	Trump
Hotels	Hotelsa	k1gFnPc2	Hotelsa
&	&	k?	&
Casino	Casino	k1gNnSc4	Casino
Resorts	Resorts	k1gInSc4	Resorts
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mnohé	mnohý	k2eAgInPc4d1	mnohý
projekty	projekt	k1gInPc4	projekt
jeho	jeho	k3xOp3gNnPc2	jeho
podnikání	podnikání	k1gNnPc2	podnikání
skončily	skončit	k5eAaPmAgFnP	skončit
neúspěchem	neúspěch	k1gInSc7	neúspěch
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
již	již	k6eAd1	již
byly	být	k5eAaImAgInP	být
zrušeny	zrušit	k5eAaPmNgInP	zrušit
–	–	k?	–
např.	např.	kA	např.
Trump	Trump	k1gMnSc1	Trump
Airlines	Airlines	k1gMnSc1	Airlines
<g/>
,	,	kIx,	,
Trump	Trump	k1gMnSc1	Trump
University	universita	k1gFnSc2	universita
<g/>
,	,	kIx,	,
Trump	Trump	k1gMnSc1	Trump
Magazine	Magazin	k1gInSc5	Magazin
<g/>
,	,	kIx,	,
Trump	Trump	k1gInSc1	Trump
Vodka	vodka	k1gFnSc1	vodka
<g/>
,	,	kIx,	,
Trump	Trump	k1gInSc1	Trump
Steaks	Steaks	k1gInSc1	Steaks
<g/>
,	,	kIx,	,
Trump	Trump	k1gInSc1	Trump
Mortgage	Mortgag	k1gFnSc2	Mortgag
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
Trump	Trump	k1gInSc1	Trump
University	universita	k1gFnSc2	universita
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
-	-	kIx~	-
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
a	a	k8xC	a
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2016	[number]	k4	2016
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
zájmu	zájem	k1gInSc2	zájem
médií	médium	k1gNnPc2	médium
<g/>
,	,	kIx,	,
když	když	k8xS	když
státní	státní	k2eAgInSc4d1	státní
návladní	návladní	k2eAgInSc4d1	návladní
Eric	Eric	k1gInSc4	Eric
Schneiderman	Schneiderman	k1gMnSc1	Schneiderman
zahájil	zahájit	k5eAaPmAgMnS	zahájit
vyšetřování	vyšetřování	k1gNnSc4	vyšetřování
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
skončilo	skončit	k5eAaPmAgNnS	skončit
žalobou	žaloba	k1gFnSc7	žaloba
<g/>
,	,	kIx,	,
říkající	říkající	k2eAgFnSc7d1	říkající
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
škola	škola	k1gFnSc1	škola
"	"	kIx"	"
<g/>
angažovala	angažovat	k5eAaBmAgFnS	angažovat
ve	v	k7c6	v
specifických	specifický	k2eAgFnPc6d1	specifická
podvodných	podvodný	k2eAgFnPc6d1	podvodná
<g/>
,	,	kIx,	,
klamavých	klamavý	k2eAgFnPc6d1	klamavá
a	a	k8xC	a
nezákonných	zákonný	k2eNgFnPc6d1	nezákonná
činnostech	činnost	k1gFnPc6	činnost
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nejednalo	jednat	k5eNaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
univerzitu	univerzita	k1gFnSc4	univerzita
(	(	kIx(	(
<g/>
název	název	k1gInSc1	název
musel	muset	k5eAaImAgInS	muset
být	být	k5eAaImF	být
později	pozdě	k6eAd2	pozdě
změněn	změněn	k2eAgMnSc1d1	změněn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spíše	spíše	k9	spíše
o	o	k7c4	o
program	program	k1gInSc4	program
velmi	velmi	k6eAd1	velmi
drahých	drahý	k2eAgInPc2d1	drahý
motivačních	motivační	k2eAgInPc2d1	motivační
kurzů	kurz	k1gInPc2	kurz
s	s	k7c7	s
pochybným	pochybný	k2eAgInSc7d1	pochybný
prospěchem	prospěch	k1gInSc7	prospěch
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gMnPc4	jejich
účastníky	účastník	k1gMnPc4	účastník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nemovitosti	nemovitost	k1gFnSc6	nemovitost
===	===	k?	===
</s>
</p>
<p>
<s>
Donald	Donald	k1gMnSc1	Donald
Trump	Trump	k1gMnSc1	Trump
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
v	v	k7c6	v
USA	USA	kA	USA
několik	několik	k4yIc4	několik
nemovitostí	nemovitost	k1gFnPc2	nemovitost
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
administrativní	administrativní	k2eAgFnPc1d1	administrativní
a	a	k8xC	a
rezidenční	rezidenční	k2eAgFnPc1d1	rezidenční
budovy	budova	k1gFnPc1	budova
</s>
</p>
<p>
<s>
Trump	Trump	k1gMnSc1	Trump
Tower	Tower	k1gMnSc1	Tower
(	(	kIx(	(
<g/>
mrakodrap	mrakodrap	k1gInSc1	mrakodrap
s	s	k7c7	s
hodnotou	hodnota	k1gFnSc7	hodnota
320	[number]	k4	320
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejcennějších	cenný	k2eAgFnPc2d3	nejcennější
nemovitostí	nemovitost	k1gFnPc2	nemovitost
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Trump	Trump	k1gInSc1	Trump
World	World	k1gInSc1	World
Tower	Tower	k1gInSc4	Tower
(	(	kIx(	(
<g/>
newyorský	newyorský	k2eAgInSc4d1	newyorský
mrakodrap	mrakodrap	k1gInSc4	mrakodrap
oceněný	oceněný	k2eAgInSc4d1	oceněný
na	na	k7c4	na
290	[number]	k4	290
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
40	[number]	k4	40
Wall	Wall	k1gMnSc1	Wall
Street	Street	k1gMnSc1	Street
(	(	kIx(	(
<g/>
Trump	Trump	k1gMnSc1	Trump
Building	Building	k1gInSc4	Building
<g/>
,	,	kIx,	,
původní	původní	k2eAgFnSc4d1	původní
budovu	budova	k1gFnSc4	budova
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1930	[number]	k4	1930
Trump	Trump	k1gMnSc1	Trump
renovoval	renovovat	k5eAaBmAgMnS	renovovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Budova	budova	k1gFnSc1	budova
je	být	k5eAaImIp3nS	být
283	[number]	k4	283
m	m	kA	m
vysoká	vysoká	k1gFnSc1	vysoká
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
72	[number]	k4	72
podlaží	podlaží	k1gNnPc2	podlaží
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
hotely	hotel	k1gInPc1	hotel
</s>
</p>
<p>
<s>
Trump	Trump	k1gMnSc1	Trump
International	International	k1gFnSc2	International
Hotel	hotel	k1gInSc1	hotel
&	&	k?	&
Tower	Tower	k1gInSc1	Tower
(	(	kIx(	(
<g/>
Chicago	Chicago	k1gNnSc1	Chicago
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Trump	Trump	k1gMnSc1	Trump
International	International	k1gFnSc2	International
Hotel	hotel	k1gInSc1	hotel
&	&	k?	&
Tower	Tower	k1gInSc1	Tower
(	(	kIx(	(
<g/>
Las	laso	k1gNnPc2	laso
Vegas	Vegasa	k1gFnPc2	Vegasa
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Trump	Trump	k1gMnSc1	Trump
International	International	k1gFnSc2	International
Hotel	hotel	k1gInSc1	hotel
&	&	k?	&
Tower	Tower	k1gInSc1	Tower
(	(	kIx(	(
<g/>
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
)	)	kIx)	)
<g/>
kasína	kasína	k1gFnSc1	kasína
</s>
</p>
<p>
<s>
Trump	Trump	k1gInSc1	Trump
Taj	taj	k1gInSc1	taj
Mahal	Mahal	k1gInSc1	Mahal
(	(	kIx(	(
<g/>
pokerová	pokerový	k2eAgFnSc1d1	pokerová
místnost	místnost	k1gFnSc1	místnost
kasinové	kasinový	k2eAgFnSc2d1	kasinová
části	část	k1gFnSc2	část
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
v	v	k7c6	v
Atlantic	Atlantice	k1gFnPc2	Atlantice
City	City	k1gFnSc2	City
<g/>
.	.	kIx.	.
</s>
<s>
Hotelová	hotelový	k2eAgFnSc1d1	hotelová
část	část	k1gFnSc1	část
má	mít	k5eAaImIp3nS	mít
1250	[number]	k4	1250
pokojů	pokoj	k1gInPc2	pokoj
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Trump	Trump	k1gInSc1	Trump
Plaza	plaz	k1gMnSc2	plaz
</s>
</p>
<p>
<s>
Trump	Trump	k1gInSc1	Trump
MarinaKromě	MarinaKromě	k1gMnPc2	MarinaKromě
těchto	tento	k3xDgMnPc2	tento
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
nemovitostí	nemovitost	k1gFnPc2	nemovitost
v	v	k7c6	v
USA	USA	kA	USA
vlastní	vlastní	k2eAgInSc1d1	vlastní
Trump	Trump	k1gInSc1	Trump
-	-	kIx~	-
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
společně	společně	k6eAd1	společně
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
první	první	k4xOgFnSc7	první
manželkou	manželka	k1gFnSc7	manželka
Ivanou	Ivana	k1gFnSc7	Ivana
Trumpovou	Trumpová	k1gFnSc7	Trumpová
-	-	kIx~	-
také	také	k9	také
luxusní	luxusní	k2eAgNnSc4d1	luxusní
sídlo	sídlo	k1gNnSc4	sídlo
na	na	k7c6	na
Floridě	Florida	k1gFnSc6	Florida
<g/>
,	,	kIx,	,
zvané	zvaný	k2eAgNnSc4d1	zvané
Mar-a-Lago	Mar-Lago	k1gNnSc4	Mar-a-Lago
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
toto	tento	k3xDgNnSc4	tento
sídlo	sídlo	k1gNnSc4	sídlo
musel	muset	k5eAaImAgMnS	muset
Trump	Trump	k1gMnSc1	Trump
dne	den	k1gInSc2	den
8	[number]	k4	8
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2017	[number]	k4	2017
nařídit	nařídit	k5eAaPmF	nařídit
jeho	jeho	k3xOp3gFnSc4	jeho
evakuaci	evakuace	k1gFnSc4	evakuace
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
celé	celý	k2eAgNnSc4d1	celé
východní	východní	k2eAgNnSc4d1	východní
pobřeží	pobřeží	k1gNnSc4	pobřeží
států	stát	k1gInPc2	stát
Florida	Florida	k1gFnSc1	Florida
a	a	k8xC	a
Georgia	Georgia	k1gFnSc1	Georgia
<g/>
,	,	kIx,	,
ohroženo	ohrozit	k5eAaPmNgNnS	ohrozit
hurikánem	hurikán	k1gInSc7	hurikán
Irma	Irma	k1gFnSc1	Irma
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
nesou	nést	k5eAaImIp3nP	nést
Trumpovo	Trumpův	k2eAgNnSc4d1	Trumpovo
jméno	jméno	k1gNnSc4	jméno
následující	následující	k2eAgFnSc2d1	následující
budovy	budova	k1gFnSc2	budova
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Trump	Trump	k1gMnSc1	Trump
International	International	k1gFnSc2	International
Hotel	hotel	k1gInSc1	hotel
and	and	k?	and
Tower	Tower	k1gInSc1	Tower
(	(	kIx(	(
<g/>
Toronto	Toronto	k1gNnSc1	Toronto
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
The	The	k?	The
Palm	Palm	k1gMnSc1	Palm
Trump	Trump	k1gMnSc1	Trump
International	International	k1gMnSc1	International
Hotel	hotel	k1gInSc1	hotel
and	and	k?	and
Tower	Tower	k1gInSc1	Tower
(	(	kIx(	(
<g/>
Dubaj	Dubaj	k1gInSc1	Dubaj
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Trump	Trump	k1gInSc1	Trump
Ocean	Oceana	k1gFnPc2	Oceana
Club	club	k1gInSc1	club
International	International	k1gMnSc1	International
Hotel	hotel	k1gInSc1	hotel
and	and	k?	and
Tower	Tower	k1gInSc1	Tower
(	(	kIx(	(
<g/>
Panama	panama	k2eAgNnSc1d1	panama
City	city	k1gNnSc1	city
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Trump	Trump	k1gInSc1	Trump
Ocean	Oceana	k1gFnPc2	Oceana
Club	club	k1gInSc1	club
Baja	Baja	k1gMnSc1	Baja
Mexico	Mexico	k1gMnSc1	Mexico
(	(	kIx(	(
<g/>
nedaleko	daleko	k6eNd1	daleko
od	od	k7c2	od
San	San	k1gFnSc2	San
Diego	Diego	k6eAd1	Diego
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Mediální	mediální	k2eAgNnSc4d1	mediální
působení	působení	k1gNnSc4	působení
==	==	k?	==
</s>
</p>
<p>
<s>
Donald	Donald	k1gMnSc1	Donald
Trump	Trump	k1gMnSc1	Trump
byl	být	k5eAaImAgMnS	být
dvakrát	dvakrát	k6eAd1	dvakrát
nominován	nominovat	k5eAaBmNgMnS	nominovat
na	na	k7c4	na
Cenu	cena	k1gFnSc4	cena
Emmy	Emma	k1gFnSc2	Emma
a	a	k8xC	a
účinkoval	účinkovat	k5eAaImAgInS	účinkovat
v	v	k7c6	v
cameo	cameo	k6eAd1	cameo
rolích	role	k1gFnPc6	role
v	v	k7c6	v
několika	několik	k4yIc6	několik
filmech	film	k1gInPc6	film
(	(	kIx(	(
<g/>
Sám	sám	k3xTgMnSc1	sám
doma	doma	k6eAd1	doma
2	[number]	k4	2
<g/>
:	:	kIx,	:
Ztracen	ztracen	k2eAgInSc1d1	ztracen
v	v	k7c6	v
New	New	k1gFnPc6	New
Yorku	York	k1gInSc2	York
<g/>
)	)	kIx)	)
a	a	k8xC	a
seriálech	seriál	k1gInPc6	seriál
(	(	kIx(	(
<g/>
Chůva	chůva	k1gFnSc1	chůva
k	k	k7c3	k
pohledání	pohledání	k1gNnSc3	pohledání
<g/>
,	,	kIx,	,
Tak	tak	k6eAd1	tak
jde	jít	k5eAaImIp3nS	jít
čas	čas	k1gInSc1	čas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mnohokrát	mnohokrát	k6eAd1	mnohokrát
byl	být	k5eAaImAgInS	být
hostem	host	k1gMnSc7	host
různých	různý	k2eAgFnPc2d1	různá
talk	talka	k1gFnPc2	talka
show	show	k1gFnSc1	show
<g/>
.	.	kIx.	.
</s>
<s>
Objevil	objevit	k5eAaPmAgInS	objevit
se	se	k3xPyFc4	se
i	i	k9	i
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
zábavných	zábavný	k2eAgInPc6d1	zábavný
pořadech	pořad	k1gInPc6	pořad
<g/>
,	,	kIx,	,
např.	např.	kA	např.
ve	v	k7c4	v
World	World	k1gInSc4	World
Wrestling	Wrestling	k1gInSc1	Wrestling
Entertainment	Entertainment	k1gInSc1	Entertainment
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
v	v	k7c6	v
ringu	ring	k1gInSc6	ring
pral	prát	k5eAaImAgMnS	prát
s	s	k7c7	s
vlastníkem	vlastník	k1gMnSc7	vlastník
organizace	organizace	k1gFnSc2	organizace
<g/>
,	,	kIx,	,
Vincem	Vince	k1gMnSc7	Vince
McMahonem	McMahon	k1gMnSc7	McMahon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
výkonným	výkonný	k2eAgMnSc7d1	výkonný
producentem	producent	k1gMnSc7	producent
a	a	k8xC	a
moderátorem	moderátor	k1gMnSc7	moderátor
soutěžní	soutěžní	k2eAgFnSc2d1	soutěžní
reality	realita	k1gFnSc2	realita
show	show	k1gFnSc1	show
The	The	k1gFnSc1	The
Apprentice	Apprentice	k1gFnSc1	Apprentice
na	na	k7c6	na
kanále	kanál	k1gInSc6	kanál
NBC	NBC	kA	NBC
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
každém	každý	k3xTgInSc6	každý
vysílání	vysílání	k1gNnSc6	vysílání
této	tento	k3xDgFnSc2	tento
soutěže	soutěž	k1gFnSc2	soutěž
bojovala	bojovat	k5eAaImAgFnS	bojovat
skupina	skupina	k1gFnSc1	skupina
lidí	člověk	k1gMnPc2	člověk
o	o	k7c4	o
práci	práce	k1gFnSc4	práce
v	v	k7c6	v
Trumpově	Trumpův	k2eAgFnSc6d1	Trumpova
společnosti	společnost	k1gFnSc6	společnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
sérii	série	k1gFnSc6	série
The	The	k1gFnSc2	The
Apprentice	Apprentice	k1gFnSc2	Apprentice
dostával	dostávat	k5eAaImAgMnS	dostávat
Trump	Trump	k1gMnSc1	Trump
plat	plat	k1gInSc4	plat
50	[number]	k4	50
000	[number]	k4	000
dolarů	dolar	k1gInPc2	dolar
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
epizodu	epizoda	k1gFnSc4	epizoda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
úspěchu	úspěch	k1gInSc6	úspěch
programu	program	k1gInSc2	program
byl	být	k5eAaImAgMnS	být
odměňován	odměňován	k2eAgMnSc1d1	odměňován
3	[number]	k4	3
miliony	milion	k4xCgInPc1	milion
dolarů	dolar	k1gInPc2	dolar
za	za	k7c4	za
epizodu	epizoda	k1gFnSc4	epizoda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
10	[number]	k4	10
sériích	série	k1gFnPc6	série
přidal	přidat	k5eAaPmAgMnS	přidat
spin-off	spinff	k1gMnSc1	spin-off
The	The	k1gFnSc2	The
Celebrity	celebrita	k1gFnSc2	celebrita
Apprentice	Apprentice	k1gFnSc2	Apprentice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vystupovaly	vystupovat	k5eAaImAgInP	vystupovat
celebrity	celebrita	k1gFnPc4	celebrita
<g/>
.	.	kIx.	.
</s>
<s>
Peníze	peníz	k1gInPc1	peníz
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
vybrat	vybrat	k5eAaPmF	vybrat
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
kole	kolo	k1gNnSc6	kolo
<g/>
,	,	kIx,	,
šly	jít	k5eAaImAgInP	jít
na	na	k7c4	na
charitativní	charitativní	k2eAgInSc4d1	charitativní
projekt	projekt	k1gInSc4	projekt
dle	dle	k7c2	dle
výběru	výběr	k1gInSc2	výběr
projektového	projektový	k2eAgMnSc2d1	projektový
manažera	manažer	k1gMnSc2	manažer
vítězného	vítězný	k2eAgInSc2d1	vítězný
týmu	tým	k1gInSc2	tým
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
za	za	k7c4	za
The	The	k1gFnSc4	The
Apprentice	Apprentice	k1gFnSc2	Apprentice
dostal	dostat	k5eAaPmAgInS	dostat
Trump	Trump	k1gInSc1	Trump
hvězdu	hvězda	k1gFnSc4	hvězda
na	na	k7c6	na
hollywoodském	hollywoodský	k2eAgInSc6d1	hollywoodský
chodníku	chodník	k1gInSc6	chodník
slávy	sláva	k1gFnSc2	sláva
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
však	však	k9	však
byla	být	k5eAaImAgFnS	být
již	již	k6eAd1	již
několikrát	několikrát	k6eAd1	několikrát
vandalizována	vandalizován	k2eAgFnSc1d1	vandalizován
<g/>
.	.	kIx.	.
<g/>
Trump	Trump	k1gMnSc1	Trump
se	se	k3xPyFc4	se
po	po	k7c6	po
úspěchu	úspěch	k1gInSc6	úspěch
show	show	k1gFnSc2	show
The	The	k1gFnSc2	The
Apprentice	Apprentice	k1gFnSc2	Apprentice
stal	stát	k5eAaPmAgMnS	stát
výkonným	výkonný	k2eAgMnSc7d1	výkonný
producentem	producent	k1gMnSc7	producent
i	i	k8xC	i
dalších	další	k2eAgInPc2d1	další
projektů	projekt	k1gInPc2	projekt
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
byly	být	k5eAaImAgFnP	být
soutěže	soutěž	k1gFnPc1	soutěž
krásy	krása	k1gFnSc2	krása
Miss	miss	k1gFnSc2	miss
USA	USA	kA	USA
a	a	k8xC	a
Miss	miss	k1gFnSc1	miss
Universe	Universe	k1gFnSc2	Universe
Pageant	Pageanta	k1gFnPc2	Pageanta
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
vysílala	vysílat	k5eAaImAgFnS	vysílat
rovněž	rovněž	k9	rovněž
televizní	televizní	k2eAgFnSc1d1	televizní
stanice	stanice	k1gFnSc1	stanice
NBC	NBC	kA	NBC
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
oznámil	oznámit	k5eAaPmAgMnS	oznámit
prezidentskou	prezidentský	k2eAgFnSc4d1	prezidentská
kandidaturu	kandidatura	k1gFnSc4	kandidatura
a	a	k8xC	a
v	v	k7c6	v
preferencích	preference	k1gFnPc6	preference
šel	jít	k5eAaImAgMnS	jít
mezi	mezi	k7c7	mezi
republikány	republikán	k1gMnPc7	republikán
do	do	k7c2	do
vedení	vedení	k1gNnSc2	vedení
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
Trump	Trump	k1gInSc1	Trump
mediálně	mediálně	k6eAd1	mediálně
exponován	exponovat	k5eAaImNgInS	exponovat
nesrovnatelně	srovnatelně	k6eNd1	srovnatelně
intenzivněji	intenzivně	k6eAd2	intenzivně
<g/>
,	,	kIx,	,
typicky	typicky	k6eAd1	typicky
s	s	k7c7	s
několika	několik	k4yIc7	několik
rozhovory	rozhovor	k1gInPc7	rozhovor
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
<s>
Získával	získávat	k5eAaImAgMnS	získávat
tak	tak	k9	tak
nejvíce	nejvíce	k6eAd1	nejvíce
prostoru	prostor	k1gInSc2	prostor
a	a	k8xC	a
času	čas	k1gInSc2	čas
v	v	k7c6	v
médiích	médium	k1gNnPc6	médium
nejen	nejen	k6eAd1	nejen
mezi	mezi	k7c7	mezi
republikánskými	republikánský	k2eAgInPc7d1	republikánský
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
všemi	všecek	k3xTgMnPc7	všecek
prezidentskými	prezidentský	k2eAgMnPc7d1	prezidentský
kandidáty	kandidát	k1gMnPc7	kandidát
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
média	médium	k1gNnSc2	médium
byly	být	k5eAaImAgFnP	být
zprávy	zpráva	k1gFnPc1	zpráva
týkající	týkající	k2eAgFnPc1d1	týkající
se	se	k3xPyFc4	se
Donalda	Donald	k1gMnSc2	Donald
Trumpa	Trump	k1gMnSc2	Trump
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
sledovanosti	sledovanost	k1gFnSc2	sledovanost
velmi	velmi	k6eAd1	velmi
atraktivní	atraktivní	k2eAgMnSc1d1	atraktivní
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
na	na	k7c6	na
zpravodajství	zpravodajství	k1gNnSc6	zpravodajství
ani	ani	k8xC	ani
mediálním	mediální	k2eAgInSc6d1	mediální
času	čas	k1gInSc6	čas
jemu	on	k3xPp3gMnSc3	on
věnovanému	věnovaný	k2eAgInSc3d1	věnovaný
nešetřily	šetřit	k5eNaImAgFnP	šetřit
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
i	i	k9	i
on	on	k3xPp3gMnSc1	on
dostával	dostávat	k5eAaImAgMnS	dostávat
de	de	k?	de
facto	facto	k1gNnSc4	facto
zdarma	zdarma	k6eAd1	zdarma
prostor	prostor	k1gInSc4	prostor
pro	pro	k7c4	pro
vyjadřování	vyjadřování	k1gNnSc4	vyjadřování
svých	svůj	k3xOyFgInPc2	svůj
názorů	názor	k1gInPc2	názor
<g/>
,	,	kIx,	,
kterými	který	k3yRgInPc7	který
mohl	moct	k5eAaImAgMnS	moct
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
získat	získat	k5eAaPmF	získat
mnohé	mnohý	k2eAgMnPc4d1	mnohý
nerozhodnuté	rozhodnutý	k2eNgMnPc4d1	nerozhodnutý
voliče	volič	k1gMnPc4	volič
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
bylo	být	k5eAaImAgNnS	být
odhadováno	odhadován	k2eAgNnSc1d1	odhadováno
<g/>
,	,	kIx,	,
že	že	k8xS	že
hodnota	hodnota	k1gFnSc1	hodnota
mediálního	mediální	k2eAgInSc2d1	mediální
prostoru	prostor	k1gInSc2	prostor
jemu	on	k3xPp3gInSc3	on
"	"	kIx"	"
<g/>
zdarma	zdarma	k6eAd1	zdarma
<g/>
"	"	kIx"	"
poskytnutého	poskytnutý	k2eAgNnSc2d1	poskytnuté
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
reklam	reklama	k1gFnPc2	reklama
<g/>
,	,	kIx,	,
za	za	k7c4	za
něž	jenž	k3xRgFnPc4	jenž
by	by	kYmCp3nS	by
musel	muset	k5eAaImAgMnS	muset
platit	platit	k5eAaImF	platit
komerční	komerční	k2eAgFnSc2d1	komerční
ceny	cena	k1gFnSc2	cena
<g/>
)	)	kIx)	)
činila	činit	k5eAaImAgFnS	činit
1,9	[number]	k4	1,9
<g/>
–	–	k?	–
<g/>
2,0	[number]	k4	2,0
miliardy	miliarda	k4xCgFnSc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Politické	politický	k2eAgNnSc1d1	politické
působení	působení	k1gNnSc1	působení
do	do	k7c2	do
ledna	leden	k1gInSc2	leden
2017	[number]	k4	2017
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Politické	politický	k2eAgFnPc1d1	politická
pozice	pozice	k1gFnPc1	pozice
===	===	k?	===
</s>
</p>
<p>
<s>
Vnitropoliticky	vnitropoliticky	k6eAd1	vnitropoliticky
se	se	k3xPyFc4	se
především	především	k6eAd1	především
profiloval	profilovat	k5eAaImAgInS	profilovat
svými	svůj	k3xOyFgInPc7	svůj
požadavky	požadavek	k1gInPc7	požadavek
na	na	k7c4	na
snížení	snížení	k1gNnSc4	snížení
daní	danit	k5eAaImIp3nP	danit
firmám	firma	k1gFnPc3	firma
<g/>
,	,	kIx,	,
zjednodušení	zjednodušení	k1gNnSc3	zjednodušení
daňového	daňový	k2eAgInSc2d1	daňový
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
snížení	snížení	k1gNnSc4	snížení
federálního	federální	k2eAgInSc2d1	federální
dluhu	dluh	k1gInSc2	dluh
a	a	k8xC	a
také	také	k9	také
svými	svůj	k3xOyFgInPc7	svůj
odmítavými	odmítavý	k2eAgInPc7d1	odmítavý
postoji	postoj	k1gInPc7	postoj
vůči	vůči	k7c3	vůči
nelegálnímu	legální	k2eNgNnSc3d1	nelegální
přistěhovalectví	přistěhovalectví	k1gNnSc3	přistěhovalectví
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zahraniční	zahraniční	k2eAgFnSc6d1	zahraniční
politice	politika	k1gFnSc6	politika
je	být	k5eAaImIp3nS	být
proti	proti	k7c3	proti
smlouvám	smlouva	k1gFnPc3	smlouva
o	o	k7c6	o
svobodném	svobodný	k2eAgInSc6d1	svobodný
obchodu	obchod	k1gInSc6	obchod
a	a	k8xC	a
prosazuje	prosazovat	k5eAaImIp3nS	prosazovat
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
nemají	mít	k5eNaImIp3nP	mít
příliš	příliš	k6eAd1	příliš
intervenovat	intervenovat	k5eAaImF	intervenovat
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
<g/>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
prvního	první	k4xOgNnSc2	první
funkčního	funkční	k2eAgNnSc2d1	funkční
období	období	k1gNnSc2	období
Baracka	Baracko	k1gNnSc2	Baracko
Obamy	Obama	k1gFnSc2	Obama
se	se	k3xPyFc4	se
zviditelnil	zviditelnit	k5eAaPmAgMnS	zviditelnit
jako	jako	k9	jako
sympatizant	sympatizant	k1gMnSc1	sympatizant
tzv.	tzv.	kA	tzv.
Birthers	Birthers	k1gInSc1	Birthers
<g/>
,	,	kIx,	,
skupiny	skupina	k1gFnPc1	skupina
lidí	člověk	k1gMnPc2	člověk
vyžadující	vyžadující	k2eAgInSc4d1	vyžadující
důkaz	důkaz	k1gInSc4	důkaz
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Obama	Obama	k?	Obama
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
USA	USA	kA	USA
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
způsobilý	způsobilý	k2eAgMnSc1d1	způsobilý
být	být	k5eAaImF	být
prezidentem	prezident	k1gMnSc7	prezident
USA	USA	kA	USA
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Spory	spora	k1gFnSc2	spora
o	o	k7c4	o
volitelnost	volitelnost	k1gFnSc4	volitelnost
Baracka	Baracko	k1gNnSc2	Baracko
Obamy	Obama	k1gFnSc2	Obama
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Minimálně	minimálně	k6eAd1	minimálně
v	v	k7c6	v
jednom	jeden	k4xCgNnSc6	jeden
televizním	televizní	k2eAgNnSc6d1	televizní
vystoupení	vystoupení	k1gNnSc6	vystoupení
Trump	Trump	k1gInSc1	Trump
veřejně	veřejně	k6eAd1	veřejně
slíbil	slíbit	k5eAaPmAgInS	slíbit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
Obama	Obama	k?	Obama
poskytne	poskytnout	k5eAaPmIp3nS	poskytnout
vyžadované	vyžadovaný	k2eAgInPc4d1	vyžadovaný
dokumenty	dokument	k1gInPc4	dokument
<g/>
,	,	kIx,	,
věnuje	věnovat	k5eAaImIp3nS	věnovat
on	on	k3xPp3gInSc1	on
5	[number]	k4	5
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
na	na	k7c4	na
charitu	charita	k1gFnSc4	charita
dle	dle	k7c2	dle
prezidentova	prezidentův	k2eAgNnSc2d1	prezidentovo
uvážení	uvážení	k1gNnSc2	uvážení
<g/>
.	.	kIx.	.
</s>
<s>
Obama	Obama	k?	Obama
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
výzvu	výzva	k1gFnSc4	výzva
nereagoval	reagovat	k5eNaBmAgMnS	reagovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
Trump	Trump	k1gMnSc1	Trump
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
bývalého	bývalý	k2eAgMnSc4d1	bývalý
amerického	americký	k2eAgMnSc4d1	americký
prezidenta	prezident	k1gMnSc4	prezident
Bushe	Bush	k1gMnSc4	Bush
za	za	k7c4	za
invazi	invaze	k1gFnSc4	invaze
do	do	k7c2	do
Iráku	Irák	k1gInSc2	Irák
a	a	k8xC	a
také	také	k9	také
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
dění	dění	k1gNnSc4	dění
v	v	k7c6	v
Libyi	Libye	k1gFnSc6	Libye
<g/>
,	,	kIx,	,
Sýrii	Sýrie	k1gFnSc6	Sýrie
a	a	k8xC	a
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
,	,	kIx,	,
že	že	k8xS	že
zahraniční	zahraniční	k2eAgFnSc1d1	zahraniční
politika	politika	k1gFnSc1	politika
Baracka	Baracko	k1gNnSc2	Baracko
Obamy	Obama	k1gFnSc2	Obama
a	a	k8xC	a
bývalé	bývalý	k2eAgFnSc2d1	bývalá
ministryně	ministryně	k1gFnSc2	ministryně
zahraničí	zahraničí	k1gNnSc2	zahraničí
Hillary	Hillara	k1gFnSc2	Hillara
Clintonové	Clintonová	k1gFnSc2	Clintonová
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
a	a	k8xC	a
na	na	k7c6	na
Blízkém	blízký	k2eAgInSc6d1	blízký
východě	východ	k1gInSc6	východ
zapříčinila	zapříčinit	k5eAaPmAgFnS	zapříčinit
smrt	smrt	k1gFnSc1	smrt
"	"	kIx"	"
<g/>
stovek	stovka	k1gFnPc2	stovka
tisíc	tisíc	k4xCgInPc2	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nadto	nadto	k6eAd1	nadto
stála	stát	k5eAaImAgFnS	stát
tato	tento	k3xDgFnSc1	tento
politika	politika	k1gFnSc1	politika
velmi	velmi	k6eAd1	velmi
mnoho	mnoho	k4c1	mnoho
peněz	peníze	k1gInPc2	peníze
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mohly	moct	k5eAaImAgInP	moct
být	být	k5eAaImF	být
podle	podle	k7c2	podle
Trumpa	Trump	k1gMnSc2	Trump
vydány	vydat	k5eAaPmNgInP	vydat
pro	pro	k7c4	pro
užitečnější	užitečný	k2eAgInPc4d2	užitečnější
účely	účel	k1gInPc4	účel
na	na	k7c6	na
území	území	k1gNnSc6	území
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
označil	označit	k5eAaPmAgMnS	označit
Trump	Trump	k1gMnSc1	Trump
německou	německý	k2eAgFnSc4d1	německá
kancléřku	kancléřka	k1gFnSc4	kancléřka
Angelu	Angela	k1gFnSc4	Angela
Merkelovou	Merkelová	k1gFnSc7	Merkelová
za	za	k7c4	za
"	"	kIx"	"
<g/>
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
největšího	veliký	k2eAgMnSc2d3	veliký
světového	světový	k2eAgMnSc2d1	světový
vůdce	vůdce	k1gMnSc2	vůdce
současnosti	současnost	k1gFnSc2	současnost
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
Angelu	Angela	k1gFnSc4	Angela
Merkelovou	Merkelový	k2eAgFnSc4d1	Merkelová
ostře	ostro	k6eAd1	ostro
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
za	za	k7c4	za
její	její	k3xOp3gFnSc4	její
vstřícnou	vstřícný	k2eAgFnSc4d1	vstřícná
imigrační	imigrační	k2eAgFnSc4d1	imigrační
politiku	politika	k1gFnSc4	politika
během	během	k7c2	během
evropské	evropský	k2eAgFnSc2d1	Evropská
migrační	migrační	k2eAgFnSc2d1	migrační
krize	krize	k1gFnSc2	krize
<g/>
.	.	kIx.	.
<g/>
Trump	Trump	k1gInSc1	Trump
má	mít	k5eAaImIp3nS	mít
kladný	kladný	k2eAgInSc4d1	kladný
postoj	postoj	k1gInSc4	postoj
k	k	k7c3	k
Izraeli	Izrael	k1gInSc3	Izrael
a	a	k8xC	a
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
uznal	uznat	k5eAaPmAgInS	uznat
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
jako	jako	k8xC	jako
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Izraele	Izrael	k1gInSc2	Izrael
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
Číně	Čína	k1gFnSc3	Čína
má	mít	k5eAaImIp3nS	mít
Trump	Trump	k1gInSc1	Trump
spíše	spíše	k9	spíše
negativní	negativní	k2eAgInSc4d1	negativní
vztah	vztah	k1gInSc4	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Čínu	Čína	k1gFnSc4	Čína
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
za	za	k7c4	za
její	její	k3xOp3gFnSc4	její
obchodní	obchodní	k2eAgFnSc4d1	obchodní
politiku	politika	k1gFnSc4	politika
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
podle	podle	k7c2	podle
něho	on	k3xPp3gMnSc2	on
poškozuje	poškozovat	k5eAaImIp3nS	poškozovat
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
a	a	k8xC	a
připravuje	připravovat	k5eAaImIp3nS	připravovat
Američany	Američan	k1gMnPc4	Američan
o	o	k7c4	o
práci	práce	k1gFnSc4	práce
<g/>
,	,	kIx,	,
a	a	k8xC	a
pohrozil	pohrozit	k5eAaPmAgMnS	pohrozit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zavede	zavést	k5eAaPmIp3nS	zavést
vysoká	vysoký	k2eAgNnPc4d1	vysoké
dovozní	dovozní	k2eAgNnPc4d1	dovozní
cla	clo	k1gNnPc4	clo
na	na	k7c4	na
čínské	čínský	k2eAgNnSc4d1	čínské
zboží	zboží	k1gNnSc4	zboží
<g/>
.	.	kIx.	.
<g/>
Při	při	k7c6	při
projevu	projev	k1gInSc6	projev
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgNnSc6	který
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2015	[number]	k4	2015
ohlásil	ohlásit	k5eAaPmAgInS	ohlásit
svou	svůj	k3xOyFgFnSc4	svůj
kandidaturu	kandidatura	k1gFnSc4	kandidatura
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
USA	USA	kA	USA
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
jako	jako	k8xC	jako
prezident	prezident	k1gMnSc1	prezident
postaví	postavit	k5eAaPmIp3nS	postavit
velkou	velký	k2eAgFnSc4d1	velká
zeď	zeď	k1gFnSc4	zeď
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
s	s	k7c7	s
Mexikem	Mexiko	k1gNnSc7	Mexiko
a	a	k8xC	a
Mexičané	Mexičan	k1gMnPc1	Mexičan
ji	on	k3xPp3gFnSc4	on
zaplatí	zaplatit	k5eAaPmIp3nP	zaplatit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Stanice	stanice	k1gFnSc1	stanice
NBC	NBC	kA	NBC
Universal	Universal	k1gFnSc1	Universal
krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
základě	základ	k1gInSc6	základ
jeho	jeho	k3xOp3gNnPc2	jeho
slov	slovo	k1gNnPc2	slovo
vůči	vůči	k7c3	vůči
imigrantům	imigrant	k1gMnPc3	imigrant
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
neslučitelné	slučitelný	k2eNgFnSc3d1	neslučitelná
s	s	k7c7	s
jejími	její	k3xOp3gFnPc7	její
hodnotami	hodnota	k1gFnPc7	hodnota
respektu	respekt	k1gInSc2	respekt
a	a	k8xC	a
úcty	úcta	k1gFnSc2	úcta
vůči	vůči	k7c3	vůči
všem	všecek	k3xTgMnPc3	všecek
<g/>
,	,	kIx,	,
ruší	rušit	k5eAaImIp3nS	rušit
všechny	všechen	k3xTgInPc4	všechen
Trumpovy	Trumpův	k2eAgInPc4d1	Trumpův
televizní	televizní	k2eAgInPc4d1	televizní
pořady	pořad	k1gInPc4	pořad
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
vysílané	vysílaný	k2eAgFnSc6d1	vysílaná
<g/>
.	.	kIx.	.
</s>
<s>
Podnikatel	podnikatel	k1gMnSc1	podnikatel
se	se	k3xPyFc4	se
nechal	nechat	k5eAaPmAgMnS	nechat
slyšet	slyšet	k5eAaImF	slyšet
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
za	za	k7c7	za
svými	svůj	k3xOyFgNnPc7	svůj
slovy	slovo	k1gNnPc7	slovo
stojí	stát	k5eAaImIp3nS	stát
a	a	k8xC	a
bude	být	k5eAaImBp3nS	být
stanici	stanice	k1gFnSc4	stanice
žalovat	žalovat	k5eAaImF	žalovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
USA	USA	kA	USA
žije	žít	k5eAaImIp3nS	žít
asi	asi	k9	asi
11	[number]	k4	11
milionů	milion	k4xCgInPc2	milion
nelegálních	legální	k2eNgMnPc2d1	nelegální
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
a	a	k8xC	a
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
jich	on	k3xPp3gFnPc2	on
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Mexika	Mexiko	k1gNnSc2	Mexiko
<g/>
.	.	kIx.	.
<g/>
Trump	Trump	k1gInSc1	Trump
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
úmyslu	úmysl	k1gInSc6	úmysl
zastavit	zastavit	k5eAaPmF	zastavit
veškerou	veškerý	k3xTgFnSc4	veškerý
finanční	finanční	k2eAgFnSc4d1	finanční
podporu	podpora	k1gFnSc4	podpora
z	z	k7c2	z
federálních	federální	k2eAgInPc2d1	federální
fondů	fond	k1gInPc2	fond
směřující	směřující	k2eAgNnSc1d1	směřující
k	k	k7c3	k
"	"	kIx"	"
<g/>
azylovým	azylový	k2eAgNnPc3d1	azylové
městům	město	k1gNnPc3	město
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
útočiště	útočiště	k1gNnSc4	útočiště
přistěhovalcům	přistěhovalec	k1gMnPc3	přistěhovalec
bez	bez	k7c2	bez
dokladů	doklad	k1gInPc2	doklad
a	a	k8xC	a
porušují	porušovat	k5eAaImIp3nP	porušovat
tak	tak	k6eAd1	tak
federální	federální	k2eAgInPc1d1	federální
zákony	zákon	k1gInPc1	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Takzvaná	takzvaný	k2eAgFnSc1d1	takzvaná
"	"	kIx"	"
<g/>
sanctuary	sanctuar	k1gInPc1	sanctuar
cities	citiesa	k1gFnPc2	citiesa
<g/>
"	"	kIx"	"
ovládaná	ovládaný	k2eAgFnSc1d1	ovládaná
převážně	převážně	k6eAd1	převážně
demokraty	demokrat	k1gMnPc4	demokrat
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
něž	jenž	k3xRgFnPc4	jenž
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
Los	los	k1gInSc1	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
,	,	kIx,	,
Miami	Miami	k1gNnPc2	Miami
nebo	nebo	k8xC	nebo
San	San	k1gFnPc2	San
Francisco	Francisco	k6eAd1	Francisco
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
města	město	k1gNnPc1	město
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
místní	místní	k2eAgFnSc1d1	místní
radnice	radnice	k1gFnSc1	radnice
nehlásí	hlásit	k5eNaImIp3nS	hlásit
federálnímu	federální	k2eAgInSc3d1	federální
imigračnímu	imigrační	k2eAgInSc3d1	imigrační
úřadu	úřad	k1gInSc3	úřad
ilegální	ilegální	k2eAgFnSc2d1	ilegální
přistěhovalce	přistěhovalka	k1gFnSc3	přistěhovalka
a	a	k8xC	a
zakazují	zakazovat	k5eAaImIp3nP	zakazovat
to	ten	k3xDgNnSc1	ten
dělat	dělat	k5eAaImF	dělat
i	i	k9	i
místní	místní	k2eAgFnSc4d1	místní
policii	policie	k1gFnSc4	policie
<g/>
.	.	kIx.	.
</s>
<s>
Newyorský	newyorský	k2eAgMnSc1d1	newyorský
starosta	starosta	k1gMnSc1	starosta
Bill	Bill	k1gMnSc1	Bill
de	de	k?	de
Blasio	Blasio	k1gMnSc1	Blasio
se	se	k3xPyFc4	se
nechal	nechat	k5eAaPmAgMnS	nechat
slyšet	slyšet	k5eAaImF	slyšet
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
připraven	připravit	k5eAaPmNgMnS	připravit
jít	jít	k5eAaImF	jít
i	i	k9	i
k	k	k7c3	k
soudu	soud	k1gInSc3	soud
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bránil	bránit	k5eAaImAgMnS	bránit
své	své	k1gNnSc4	své
"	"	kIx"	"
<g/>
azylové	azylový	k2eAgNnSc1d1	azylové
město	město	k1gNnSc1	město
<g/>
"	"	kIx"	"
před	před	k7c7	před
federální	federální	k2eAgFnSc7d1	federální
vládou	vláda	k1gFnSc7	vláda
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2016	[number]	k4	2016
Trump	Trump	k1gMnSc1	Trump
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
USA	USA	kA	USA
měly	mít	k5eAaImAgInP	mít
uznat	uznat	k5eAaPmF	uznat
ruskou	ruský	k2eAgFnSc4d1	ruská
anexi	anexe	k1gFnSc4	anexe
Krymu	Krym	k1gInSc2	Krym
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
lepším	dobrý	k2eAgInPc3d2	lepší
vztahům	vztah	k1gInPc3	vztah
s	s	k7c7	s
Ruskou	Ruska	k1gFnSc7	Ruska
federací	federace	k1gFnSc7	federace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vladimir	Vladimir	k1gMnSc1	Vladimir
Putin	putin	k2eAgMnSc1d1	putin
se	se	k3xPyFc4	se
k	k	k7c3	k
americké	americký	k2eAgFnSc3d1	americká
volební	volební	k2eAgFnSc3d1	volební
kampani	kampaň	k1gFnSc3	kampaň
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
vyjadřoval	vyjadřovat	k5eAaImAgMnS	vyjadřovat
minimálně	minimálně	k6eAd1	minimálně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ruská	ruský	k2eAgNnPc4d1	ruské
média	médium	k1gNnPc4	médium
o	o	k7c6	o
Trumpovi	Trump	k1gMnSc6	Trump
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
této	tento	k3xDgFnSc2	tento
kampaně	kampaň	k1gFnSc2	kampaň
psala	psát	k5eAaImAgFnS	psát
pozitivně	pozitivně	k6eAd1	pozitivně
<g/>
.	.	kIx.	.
</s>
<s>
Trump	Trump	k1gMnSc1	Trump
také	také	k9	také
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
Putin	putin	k2eAgMnSc1d1	putin
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
silný	silný	k2eAgMnSc1d1	silný
vůdce	vůdce	k1gMnSc1	vůdce
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgInSc7	jenž
by	by	kYmCp3nS	by
"	"	kIx"	"
<g/>
zřejmě	zřejmě	k6eAd1	zřejmě
dobře	dobře	k6eAd1	dobře
vycházel	vycházet	k5eAaImAgMnS	vycházet
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
s	s	k7c7	s
ABC	ABC	kA	ABC
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
Putinovi	Putin	k1gMnSc3	Putin
nemá	mít	k5eNaImIp3nS	mít
žádný	žádný	k3yNgInSc4	žádný
vztah	vztah	k1gInSc4	vztah
a	a	k8xC	a
nikdy	nikdy	k6eAd1	nikdy
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
nesetkal	setkat	k5eNaPmAgMnS	setkat
<g/>
.	.	kIx.	.
</s>
<s>
Podpořil	podpořit	k5eAaPmAgMnS	podpořit
ruské	ruský	k2eAgInPc4d1	ruský
nálety	nálet	k1gInPc4	nálet
proti	proti	k7c3	proti
povstalcům	povstalec	k1gMnPc3	povstalec
a	a	k8xC	a
Islámskému	islámský	k2eAgInSc3d1	islámský
státu	stát	k1gInSc3	stát
(	(	kIx(	(
<g/>
IS	IS	kA	IS
<g/>
)	)	kIx)	)
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
a	a	k8xC	a
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
ochotu	ochota	k1gFnSc4	ochota
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
v	v	k7c6	v
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
IS	IS	kA	IS
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
rezignaci	rezignace	k1gFnSc6	rezignace
krátkodobého	krátkodobý	k2eAgMnSc2d1	krátkodobý
hlavního	hlavní	k2eAgMnSc2d1	hlavní
bezpečnostního	bezpečnostní	k2eAgMnSc2d1	bezpečnostní
poradce	poradce	k1gMnSc2	poradce
prezidenta	prezident	k1gMnSc2	prezident
Trumpa	Trump	k1gMnSc2	Trump
<g/>
,	,	kIx,	,
bývalého	bývalý	k2eAgMnSc2d1	bývalý
generálporučíka	generálporučík	k1gMnSc2	generálporučík
Michaela	Michael	k1gMnSc2	Michael
Flynna	Flynn	k1gMnSc2	Flynn
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
zastáncem	zastánce	k1gMnSc7	zastánce
lepších	dobrý	k2eAgInPc2d2	lepší
vztahů	vztah	k1gInPc2	vztah
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
<g/>
,	,	kIx,	,
mluvčí	mluvčí	k1gFnSc7	mluvčí
Bílého	bílý	k2eAgInSc2d1	bílý
domu	dům	k1gInSc2	dům
Sean	Sean	k1gMnSc1	Sean
Spicer	Spicer	k1gMnSc1	Spicer
14	[number]	k4	14
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2017	[number]	k4	2017
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
prezident	prezident	k1gMnSc1	prezident
Trump	Trump	k1gMnSc1	Trump
považuje	považovat	k5eAaImIp3nS	považovat
Krym	Krym	k1gInSc4	Krym
za	za	k7c4	za
součást	součást	k1gFnSc4	součást
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
a	a	k8xC	a
očekává	očekávat	k5eAaImIp3nS	očekávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Rusko	Rusko	k1gNnSc1	Rusko
vrátí	vrátit	k5eAaPmIp3nS	vrátit
poloostrov	poloostrov	k1gInSc4	poloostrov
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Spicer	Spicer	k1gInSc1	Spicer
také	také	k9	také
odkázal	odkázat	k5eAaPmAgInS	odkázat
na	na	k7c4	na
slova	slovo	k1gNnPc4	slovo
americké	americký	k2eAgFnSc2d1	americká
velvyslankyně	velvyslankyně	k1gFnSc2	velvyslankyně
při	při	k7c6	při
OSN	OSN	kA	OSN
Nikki	Nikki	k1gNnSc1	Nikki
Haleyové	Haleyové	k2eAgFnSc7d1	Haleyové
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
na	na	k7c6	na
půdě	půda	k1gFnSc6	půda
OSN	OSN	kA	OSN
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
protiruské	protiruský	k2eAgFnPc1d1	protiruská
sankce	sankce	k1gFnPc1	sankce
zůstanou	zůstat	k5eAaPmIp3nP	zůstat
v	v	k7c6	v
platnosti	platnost	k1gFnSc6	platnost
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
se	se	k3xPyFc4	se
Krym	Krym	k1gInSc1	Krym
nevrátí	vrátit	k5eNaPmIp3nS	vrátit
k	k	k7c3	k
Ukrajině	Ukrajina	k1gFnSc3	Ukrajina
<g/>
.	.	kIx.	.
<g/>
Trump	Trump	k1gMnSc1	Trump
označil	označit	k5eAaPmAgMnS	označit
syrského	syrský	k2eAgMnSc4d1	syrský
prezidenta	prezident	k1gMnSc4	prezident
Bašára	Bašár	k1gMnSc4	Bašár
al-Asada	al-Asada	k1gFnSc1	al-Asada
za	za	k7c4	za
"	"	kIx"	"
<g/>
padoucha	padouch	k1gMnSc4	padouch
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nemám	mít	k5eNaImIp1nS	mít
rád	rád	k6eAd1	rád
syrského	syrský	k2eAgMnSc4d1	syrský
prezidenta	prezident	k1gMnSc4	prezident
Bašára	Bašár	k1gMnSc4	Bašár
Asada	Asad	k1gMnSc4	Asad
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Asad	Asad	k1gMnSc1	Asad
zabíjí	zabíjet	k5eAaImIp3nP	zabíjet
bojovníky	bojovník	k1gMnPc7	bojovník
Islámského	islámský	k2eAgInSc2d1	islámský
státu	stát	k1gInSc2	stát
(	(	kIx(	(
<g/>
IS	IS	kA	IS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rusko	Rusko	k1gNnSc4	Rusko
zabíjí	zabíjet	k5eAaImIp3nP	zabíjet
bojovníky	bojovník	k1gMnPc7	bojovník
IS	IS	kA	IS
a	a	k8xC	a
Írán	Írán	k1gInSc1	Írán
zabíjí	zabíjet	k5eAaImIp3nS	zabíjet
bojovníky	bojovník	k1gMnPc4	bojovník
IS	IS	kA	IS
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
V	v	k7c6	v
pozdějším	pozdní	k2eAgInSc6d2	pozdější
rozhovoru	rozhovor	k1gInSc6	rozhovor
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
veřejnosti	veřejnost	k1gFnSc2	veřejnost
řekl	říct	k5eAaPmAgMnS	říct
Trump	Trump	k1gMnSc1	Trump
na	na	k7c4	na
adresu	adresa	k1gFnSc4	adresa
senátora	senátor	k1gMnSc2	senátor
Johna	John	k1gMnSc2	John
McCaina	McCain	k1gMnSc2	McCain
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
vojenské	vojenský	k2eAgFnSc6d1	vojenská
kariéře	kariéra	k1gFnSc6	kariéra
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
během	během	k7c2	během
války	válka	k1gFnSc2	válka
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
hrdina	hrdina	k1gMnSc1	hrdina
<g/>
.	.	kIx.	.
</s>
<s>
Ok	oko	k1gNnPc2	oko
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
hrdina	hrdina	k1gMnSc1	hrdina
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
byl	být	k5eAaImAgInS	být
chycen	chytit	k5eAaPmNgInS	chytit
<g/>
.	.	kIx.	.
</s>
<s>
Já	já	k3xPp1nSc1	já
preferuji	preferovat	k5eAaImIp1nS	preferovat
ty	ten	k3xDgMnPc4	ten
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
nebyli	být	k5eNaImAgMnP	být
chyceni	chytit	k5eAaPmNgMnP	chytit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
McCain	McCain	k1gMnSc1	McCain
totiž	totiž	k9	totiž
havaroval	havarovat	k5eAaPmAgMnS	havarovat
se	s	k7c7	s
svým	svůj	k3xOyFgNnSc7	svůj
letadlem	letadlo	k1gNnSc7	letadlo
během	během	k7c2	během
náletu	nálet	k1gInSc2	nálet
na	na	k7c4	na
Hanoj	Hanoj	k1gFnSc4	Hanoj
při	při	k7c6	při
operaci	operace	k1gFnSc6	operace
Rolling	Rolling	k1gInSc1	Rolling
Thunder	Thundra	k1gFnPc2	Thundra
<g/>
,	,	kIx,	,
zlomil	zlomit	k5eAaPmAgMnS	zlomit
si	se	k3xPyFc3	se
nohu	noha	k1gFnSc4	noha
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
chycen	chytit	k5eAaPmNgInS	chytit
vietnamskými	vietnamský	k2eAgMnPc7d1	vietnamský
bojovníky	bojovník	k1gMnPc7	bojovník
a	a	k8xC	a
vězněn	vězněn	k2eAgInSc4d1	vězněn
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Trumpův	Trumpův	k2eAgInSc1d1	Trumpův
výrok	výrok	k1gInSc1	výrok
rozvířil	rozvířit	k5eAaPmAgInS	rozvířit
debatu	debata	k1gFnSc4	debata
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
zdali	zdali	k8xS	zdali
je	být	k5eAaImIp3nS	být
McCain	McCain	k2eAgMnSc1d1	McCain
válečný	válečný	k2eAgMnSc1d1	válečný
hrdina	hrdina	k1gMnSc1	hrdina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
7	[number]	k4	7
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
po	po	k7c6	po
teroristickém	teroristický	k2eAgInSc6d1	teroristický
útoku	útok	k1gInSc6	útok
v	v	k7c6	v
San	San	k1gMnSc6	San
Bernardinu	bernardin	k1gMnSc6	bernardin
<g/>
,	,	kIx,	,
vydal	vydat	k5eAaPmAgInS	vydat
Trump	Trump	k1gInSc1	Trump
prohlášení	prohlášení	k1gNnSc2	prohlášení
začínající	začínající	k2eAgMnSc1d1	začínající
touto	tento	k3xDgFnSc7	tento
větou	věta	k1gFnSc7	věta
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Když	když	k8xS	když
byl	být	k5eAaImAgInS	být
dotázán	dotázat	k5eAaPmNgInS	dotázat
na	na	k7c6	na
rozvinutí	rozvinutí	k1gNnSc6	rozvinutí
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
co	co	k8xS	co
tím	ten	k3xDgNnSc7	ten
konkrétně	konkrétně	k6eAd1	konkrétně
myslel	myslet	k5eAaImAgMnS	myslet
<g/>
,	,	kIx,	,
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
toto	tento	k3xDgNnSc1	tento
zamezení	zamezení	k1gNnSc1	zamezení
neplatilo	platit	k5eNaImAgNnS	platit
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
radikály	radikál	k1gMnPc4	radikál
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
všechny	všechen	k3xTgMnPc4	všechen
lidi	člověk	k1gMnPc4	člověk
vyznávající	vyznávající	k2eAgInSc1d1	vyznávající
islám	islám	k1gInSc1	islám
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
i	i	k9	i
pro	pro	k7c4	pro
muslimy	muslim	k1gMnPc4	muslim
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
chtějí	chtít	k5eAaImIp3nP	chtít
přicestovat	přicestovat	k5eAaPmF	přicestovat
do	do	k7c2	do
USA	USA	kA	USA
jako	jako	k8xC	jako
turisté	turist	k1gMnPc1	turist
<g/>
,	,	kIx,	,
obchodní	obchodní	k2eAgMnPc1d1	obchodní
cestující	cestující	k1gMnPc1	cestující
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
pro	pro	k7c4	pro
muslimy	muslim	k1gMnPc4	muslim
s	s	k7c7	s
americkou	americký	k2eAgFnSc7d1	americká
státní	státní	k2eAgFnSc7d1	státní
příslušností	příslušnost	k1gFnSc7	příslušnost
<g/>
,	,	kIx,	,
vracející	vracející	k2eAgFnSc2d1	vracející
se	se	k3xPyFc4	se
do	do	k7c2	do
země	zem	k1gFnSc2	zem
např.	např.	kA	např.
z	z	k7c2	z
dovolené	dovolená	k1gFnSc2	dovolená
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
Trumpovo	Trumpův	k2eAgNnSc1d1	Trumpovo
prohlášení	prohlášení	k1gNnSc1	prohlášení
se	se	k3xPyFc4	se
setkalo	setkat	k5eAaPmAgNnS	setkat
s	s	k7c7	s
převážně	převážně	k6eAd1	převážně
negativní	negativní	k2eAgFnSc7d1	negativní
reakcí	reakce	k1gFnSc7	reakce
v	v	k7c6	v
médiích	médium	k1gNnPc6	médium
<g/>
.	.	kIx.	.
</s>
<s>
Saúdskoarabský	saúdskoarabský	k2eAgMnSc1d1	saúdskoarabský
princ	princ	k1gMnSc1	princ
Valíd	Valíd	k1gMnSc1	Valíd
bin	bin	k?	bin
Talál	Talál	k1gMnSc1	Talál
poté	poté	k6eAd1	poté
nazval	nazvat	k5eAaPmAgMnS	nazvat
Trumpa	Trump	k1gMnSc4	Trump
"	"	kIx"	"
<g/>
ostudou	ostuda	k1gFnSc7	ostuda
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Trump	Trump	k1gInSc1	Trump
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
princ	princ	k1gMnSc1	princ
"	"	kIx"	"
<g/>
chce	chtít	k5eAaImIp3nS	chtít
převzít	převzít	k5eAaPmF	převzít
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
našimi	náš	k3xOp1gMnPc7	náš
politiky	politik	k1gMnPc7	politik
pomocí	pomocí	k7c2	pomocí
tatínkových	tatínkův	k2eAgInPc2d1	tatínkův
peněz	peníze	k1gInPc2	peníze
<g/>
"	"	kIx"	"
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
tomu	ten	k3xDgInSc3	ten
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
zvolení	zvolení	k1gNnSc6	zvolení
prezidentem	prezident	k1gMnSc7	prezident
USA	USA	kA	USA
učiní	učinit	k5eAaImIp3nS	učinit
přítrž	přítrž	k1gFnSc4	přítrž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2016	[number]	k4	2016
Trump	Trump	k1gMnSc1	Trump
zmírnil	zmírnit	k5eAaPmAgMnS	zmírnit
svou	svůj	k3xOyFgFnSc4	svůj
pozici	pozice	k1gFnSc4	pozice
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
pouze	pouze	k6eAd1	pouze
dočasný	dočasný	k2eAgInSc1d1	dočasný
zákaz	zákaz	k1gInSc1	zákaz
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
vztahoval	vztahovat	k5eAaImAgInS	vztahovat
jen	jen	k9	jen
na	na	k7c4	na
osoby	osoba	k1gFnPc4	osoba
pocházející	pocházející	k2eAgFnPc4d1	pocházející
ze	z	k7c2	z
zemí	zem	k1gFnPc2	zem
"	"	kIx"	"
<g/>
usvědčených	usvědčený	k2eAgFnPc2d1	usvědčená
z	z	k7c2	z
terorismu	terorismus	k1gInSc2	terorismus
proti	proti	k7c3	proti
USA	USA	kA	USA
nebo	nebo	k8xC	nebo
jejich	jejich	k3xOp3gMnPc3	jejich
spojencům	spojenec	k1gMnPc3	spojenec
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
zemí	zem	k1gFnPc2	zem
"	"	kIx"	"
<g/>
ohrožených	ohrožený	k2eAgInPc2d1	ohrožený
terorismem	terorismus	k1gInSc7	terorismus
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Trump	Trump	k1gMnSc1	Trump
se	se	k3xPyFc4	se
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
kriticky	kriticky	k6eAd1	kriticky
na	na	k7c4	na
adresu	adresa	k1gFnSc4	adresa
Saúdské	saúdský	k2eAgFnSc2d1	Saúdská
Arábie	Arábie	k1gFnSc2	Arábie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
klíčovým	klíčový	k2eAgMnSc7d1	klíčový
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
problematickým	problematický	k2eAgMnSc7d1	problematický
americkým	americký	k2eAgMnSc7d1	americký
spojencem	spojenec	k1gMnSc7	spojenec
na	na	k7c6	na
Blízkém	blízký	k2eAgInSc6d1	blízký
východě	východ	k1gInSc6	východ
<g/>
,	,	kIx,	,
a	a	k8xC	a
naznačil	naznačit	k5eAaPmAgMnS	naznačit
<g/>
,	,	kIx,	,
že	že	k8xS	že
její	její	k3xOp3gMnPc1	její
představitelé	představitel	k1gMnPc1	představitel
mohli	moct	k5eAaImAgMnP	moct
nést	nést	k5eAaImF	nést
odpovědnost	odpovědnost	k1gFnSc4	odpovědnost
za	za	k7c4	za
teroristické	teroristický	k2eAgInPc4d1	teroristický
útoky	útok	k1gInPc4	útok
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Trump	Trump	k1gMnSc1	Trump
také	také	k9	také
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
Saúdskou	saúdský	k2eAgFnSc4d1	Saúdská
Arábii	Arábie	k1gFnSc4	Arábie
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
více	hodně	k6eAd2	hodně
zapojila	zapojit	k5eAaPmAgNnP	zapojit
do	do	k7c2	do
boje	boj	k1gInSc2	boj
proti	proti	k7c3	proti
Islámskému	islámský	k2eAgInSc3d1	islámský
státu	stát	k1gInSc3	stát
a	a	k8xC	a
hradila	hradit	k5eAaImAgFnS	hradit
náklady	náklad	k1gInPc4	náklad
za	za	k7c4	za
působení	působení	k1gNnSc4	působení
amerických	americký	k2eAgNnPc2d1	americké
vojsk	vojsko	k1gNnPc2	vojsko
na	na	k7c6	na
jejím	její	k3xOp3gNnSc6	její
území	území	k1gNnSc6	území
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Hillary	Hillara	k1gFnSc2	Hillara
Clintonové	Clintonová	k1gFnSc2	Clintonová
požadoval	požadovat	k5eAaImAgMnS	požadovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
její	její	k3xOp3gMnSc1	její
Clinton	Clinton	k1gMnSc1	Clinton
Foundation	Foundation	k1gInSc1	Foundation
vrátila	vrátit	k5eAaPmAgFnS	vrátit
dary	dar	k1gInPc4	dar
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
obdržela	obdržet	k5eAaPmAgFnS	obdržet
od	od	k7c2	od
Saúdské	saúdský	k2eAgFnSc2d1	Saúdská
Arábie	Arábie	k1gFnSc2	Arábie
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
podle	podle	k7c2	podle
něj	on	k3xPp3gInSc2	on
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
jedná	jednat	k5eAaImIp3nS	jednat
se	s	k7c7	s
ženami	žena	k1gFnPc7	žena
jako	jako	k8xC	jako
s	s	k7c7	s
otroky	otrok	k1gMnPc7	otrok
a	a	k8xC	a
zabíjí	zabíjet	k5eAaImIp3nS	zabíjet
gaye	gay	k1gMnPc4	gay
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c6	v
čtvrté	čtvrtý	k4xOgFnSc6	čtvrtý
republikánské	republikánský	k2eAgFnSc6d1	republikánská
debatě	debata	k1gFnSc6	debata
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
Trump	Trump	k1gMnSc1	Trump
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
přijatelné	přijatelný	k2eAgNnSc1d1	přijatelné
se	se	k3xPyFc4	se
vypořádat	vypořádat	k5eAaPmF	vypořádat
nejen	nejen	k6eAd1	nejen
s	s	k7c7	s
teroristy	terorista	k1gMnPc7	terorista
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
s	s	k7c7	s
jejich	jejich	k3xOp3gFnPc7	jejich
rodinami	rodina	k1gFnPc7	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Řekl	říct	k5eAaPmAgMnS	říct
také	také	k9	také
<g/>
,	,	kIx,	,
že	že	k8xS	že
teroristé	terorista	k1gMnPc1	terorista
používají	používat	k5eAaImIp3nP	používat
své	svůj	k3xOyFgFnPc4	svůj
rodiny	rodina	k1gFnPc4	rodina
jako	jako	k9	jako
"	"	kIx"	"
<g/>
živé	živý	k2eAgInPc1d1	živý
štíty	štít	k1gInPc1	štít
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
debatě	debata	k1gFnSc6	debata
volal	volat	k5eAaImAgMnS	volat
po	po	k7c6	po
tvrdém	tvrdý	k2eAgInSc6d1	tvrdý
zásahu	zásah	k1gInSc6	zásah
proti	proti	k7c3	proti
Islámskému	islámský	k2eAgInSc3d1	islámský
státu	stát	k1gInSc3	stát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
dodal	dodat	k5eAaPmAgMnS	dodat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
udělal	udělat	k5eAaPmAgMnS	udělat
vše	všechen	k3xTgNnSc4	všechen
pro	pro	k7c4	pro
minimalizaci	minimalizace	k1gFnSc4	minimalizace
počtu	počet	k1gInSc2	počet
civilních	civilní	k2eAgFnPc2d1	civilní
obětí	oběť	k1gFnPc2	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
cílení	cílení	k1gNnSc4	cílení
na	na	k7c4	na
ostatní	ostatní	k2eAgMnPc4d1	ostatní
členy	člen	k1gMnPc4	člen
rodin	rodina	k1gFnPc2	rodina
teroristů	terorista	k1gMnPc2	terorista
byl	být	k5eAaImAgMnS	být
totiž	totiž	k9	totiž
kritizován	kritizovat	k5eAaImNgMnS	kritizovat
prezident	prezident	k1gMnSc1	prezident
Barack	Barack	k1gMnSc1	Barack
Obama	Obama	k?	Obama
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
jím	jíst	k5eAaImIp1nS	jíst
nařízeným	nařízený	k2eAgInPc3d1	nařízený
náletům	nálet	k1gInPc3	nálet
a	a	k8xC	a
bombardování	bombardování	k1gNnSc3	bombardování
nejrůznějších	různý	k2eAgInPc2d3	nejrůznější
objektů	objekt	k1gInPc2	objekt
pomocí	pomocí	k7c2	pomocí
bezpilotních	bezpilotní	k2eAgNnPc2d1	bezpilotní
letadel	letadlo	k1gNnPc2	letadlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
9	[number]	k4	9
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2016	[number]	k4	2016
řekl	říct	k5eAaPmAgMnS	říct
Trump	Trump	k1gMnSc1	Trump
na	na	k7c6	na
mítinku	mítink	k1gInSc6	mítink
přenášeném	přenášený	k2eAgInSc6d1	přenášený
CNN	CNN	kA	CNN
o	o	k7c4	o
Hillary	Hillara	k1gFnPc4	Hillara
Clintonové	Clintonová	k1gFnSc2	Clintonová
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gNnPc1	jeho
slova	slovo	k1gNnPc1	slovo
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
dala	dát	k5eAaPmAgFnS	dát
vykládat	vykládat	k5eAaImF	vykládat
(	(	kIx(	(
<g/>
v	v	k7c6	v
nejzazším	zadní	k2eAgInSc6d3	nejzazší
případě	případ	k1gInSc6	případ
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
nabádání	nabádání	k1gNnSc4	nabádání
k	k	k7c3	k
zastřelení	zastřelení	k1gNnSc3	zastřelení
jeho	jeho	k3xOp3gFnSc2	jeho
protikandidátky	protikandidátka	k1gFnSc2	protikandidátka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vítězství	vítězství	k1gNnSc6	vítězství
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
Trump	Trump	k1gMnSc1	Trump
zmírnil	zmírnit	k5eAaPmAgMnS	zmírnit
svou	svůj	k3xOyFgFnSc4	svůj
ostrou	ostrý	k2eAgFnSc4d1	ostrá
rétoriku	rétorika	k1gFnSc4	rétorika
a	a	k8xC	a
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
projevu	projev	k1gInSc6	projev
dne	den	k1gInSc2	den
9	[number]	k4	9
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2016	[number]	k4	2016
pochválil	pochválit	k5eAaPmAgMnS	pochválit
Clintonovou	Clintonová	k1gFnSc4	Clintonová
za	za	k7c4	za
její	její	k3xOp3gFnSc4	její
"	"	kIx"	"
<g/>
houževnatou	houževnatý	k2eAgFnSc4d1	houževnatá
kampaň	kampaň	k1gFnSc4	kampaň
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Prezidentská	prezidentský	k2eAgFnSc1d1	prezidentská
kampaň	kampaň	k1gFnSc1	kampaň
===	===	k?	===
</s>
</p>
<p>
<s>
Trump	Trump	k1gInSc1	Trump
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
politicky	politicky	k6eAd1	politicky
výrazněji	výrazně	k6eAd2	výrazně
angažovat	angažovat	k5eAaBmF	angažovat
od	od	k7c2	od
konce	konec	k1gInSc2	konec
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
organizačně	organizačně	k6eAd1	organizačně
i	i	k8xC	i
finančně	finančně	k6eAd1	finančně
podporoval	podporovat	k5eAaImAgInS	podporovat
některé	některý	k3yIgFnPc4	některý
volební	volební	k2eAgFnPc4d1	volební
kampaně	kampaň	k1gFnPc4	kampaň
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
republikány	republikán	k1gMnPc7	republikán
poprvé	poprvé	k6eAd1	poprvé
neformálně	formálně	k6eNd1	formálně
nabídnuto	nabídnout	k5eAaPmNgNnS	nabídnout
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
potenciálního	potenciální	k2eAgMnSc2d1	potenciální
politického	politický	k2eAgMnSc2d1	politický
partnera	partner	k1gMnSc2	partner
ho	on	k3xPp3gMnSc2	on
zřejmě	zřejmě	k6eAd1	zřejmě
vnímal	vnímat	k5eAaImAgMnS	vnímat
i	i	k9	i
Michail	Michail	k1gMnSc1	Michail
Gorbačov	Gorbačov	k1gInSc1	Gorbačov
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
měl	mít	k5eAaImAgInS	mít
velký	velký	k2eAgInSc1d1	velký
zájem	zájem	k1gInSc1	zájem
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
setkat	setkat	k5eAaPmF	setkat
<g/>
.	.	kIx.	.
</s>
<s>
Trump	Trump	k1gMnSc1	Trump
nakonec	nakonec	k6eAd1	nakonec
nabídku	nabídka	k1gFnSc4	nabídka
republikánů	republikán	k1gMnPc2	republikán
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
ale	ale	k9	ale
začal	začít	k5eAaPmAgInS	začít
uvažovat	uvažovat	k5eAaImF	uvažovat
o	o	k7c6	o
vlastní	vlastní	k2eAgFnSc6d1	vlastní
kandidatuře	kandidatura	k1gFnSc6	kandidatura
pro	pro	k7c4	pro
rok	rok	k1gInSc4	rok
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
chtěl	chtít	k5eAaImAgMnS	chtít
už	už	k6eAd1	už
tehdy	tehdy	k6eAd1	tehdy
koncipovat	koncipovat	k5eAaBmF	koncipovat
jeho	jeho	k3xOp3gFnSc4	jeho
nezávislou	závislý	k2eNgFnSc4d1	nezávislá
na	na	k7c6	na
republikánech	republikán	k1gMnPc6	republikán
i	i	k8xC	i
demokratech	demokrat	k1gMnPc6	demokrat
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
show	show	k1gNnSc6	show
Oprah	Opraha	k1gFnPc2	Opraha
Winfreyové	Winfreyové	k2eAgFnPc2d1	Winfreyové
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
byl	být	k5eAaImAgInS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
poprvé	poprvé	k6eAd1	poprvé
dotázán	dotázán	k2eAgMnSc1d1	dotázán
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
přemýšlí	přemýšlet	k5eAaImIp3nS	přemýšlet
o	o	k7c6	o
kandidatuře	kandidatura	k1gFnSc6	kandidatura
na	na	k7c6	na
prezidenta	prezident	k1gMnSc2	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Trump	Trump	k1gMnSc1	Trump
sice	sice	k8xC	sice
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
"	"	kIx"	"
<g/>
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
ne	ne	k9	ne
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tuto	tento	k3xDgFnSc4	tento
možnost	možnost	k1gFnSc4	možnost
zcela	zcela	k6eAd1	zcela
nevyloučil	vyloučit	k5eNaPmAgMnS	vyloučit
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
předvolebního	předvolební	k2eAgInSc2d1	předvolební
boje	boj	k1gInSc2	boj
o	o	k7c4	o
kandidaturu	kandidatura	k1gFnSc4	kandidatura
pak	pak	k6eAd1	pak
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
se	se	k3xPyFc4	se
primárek	primárky	k1gFnPc2	primárky
Reformní	reformní	k2eAgFnSc2d1	reformní
strany	strana	k1gFnSc2	strana
USA	USA	kA	USA
a	a	k8xC	a
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
od	od	k7c2	od
kandidatury	kandidatura	k1gFnSc2	kandidatura
ustoupil	ustoupit	k5eAaPmAgMnS	ustoupit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
velmi	velmi	k6eAd1	velmi
vážně	vážně	k6eAd1	vážně
zvažuje	zvažovat	k5eAaImIp3nS	zvažovat
<g/>
"	"	kIx"	"
opětovnou	opětovný	k2eAgFnSc4d1	opětovná
kandidaturu	kandidatura	k1gFnSc4	kandidatura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
16	[number]	k4	16
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2015	[number]	k4	2015
formálně	formálně	k6eAd1	formálně
oznámil	oznámit	k5eAaPmAgMnS	oznámit
svoji	svůj	k3xOyFgFnSc4	svůj
kandidaturu	kandidatura	k1gFnSc4	kandidatura
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
za	za	k7c4	za
Republikánskou	republikánský	k2eAgFnSc4d1	republikánská
stranu	strana	k1gFnSc4	strana
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
také	také	k9	také
představil	představit	k5eAaPmAgMnS	představit
svá	svůj	k3xOyFgNnPc4	svůj
předvolební	předvolební	k2eAgNnPc4d1	předvolební
hesla	heslo	k1gNnPc4	heslo
"	"	kIx"	"
<g/>
Udělejme	udělat	k5eAaPmRp1nP	udělat
Ameriku	Amerika	k1gFnSc4	Amerika
opět	opět	k6eAd1	opět
velkou	velký	k2eAgFnSc4d1	velká
<g/>
/	/	kIx~	/
<g/>
skvělou	skvělý	k2eAgFnSc4d1	skvělá
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
v	v	k7c6	v
originále	originál	k1gInSc6	originál
anglicky	anglicky	k6eAd1	anglicky
"	"	kIx"	"
<g/>
Make	Make	k1gNnSc7	Make
America	Americ	k1gInSc2	Americ
Great	Great	k2eAgInSc4d1	Great
Again	Again	k1gInSc4	Again
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
a	a	k8xC	a
"	"	kIx"	"
<g/>
Napřed	napřed	k6eAd1	napřed
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
America	Americ	k2eAgFnSc1d1	America
first	first	k1gFnSc1	first
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
první	první	k4xOgFnSc6	první
republikánské	republikánský	k2eAgFnSc6d1	republikánská
debatě	debata	k1gFnSc6	debata
byl	být	k5eAaImAgInS	být
jediný	jediný	k2eAgInSc1d1	jediný
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
nezavázal	zavázat	k5eNaPmAgInS	zavázat
slibem	slib	k1gInSc7	slib
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
případě	případ	k1gInSc6	případ
svého	svůj	k3xOyFgInSc2	svůj
neúspěchu	neúspěch	k1gInSc2	neúspěch
bude	být	k5eAaImBp3nS	být
podporovat	podporovat	k5eAaImF	podporovat
vítěze	vítěz	k1gMnPc4	vítěz
primárek	primárky	k1gFnPc2	primárky
a	a	k8xC	a
nebude	být	k5eNaImBp3nS	být
kandidovat	kandidovat	k5eAaImF	kandidovat
za	za	k7c4	za
nějakou	nějaký	k3yIgFnSc4	nějaký
nezávislou	závislý	k2eNgFnSc4d1	nezávislá
třetí	třetí	k4xOgFnSc4	třetí
stranu	strana	k1gFnSc4	strana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
vlastního	vlastní	k2eAgNnSc2d1	vlastní
vyjádření	vyjádření	k1gNnSc2	vyjádření
byl	být	k5eAaImAgInS	být
proti	proti	k7c3	proti
americké	americký	k2eAgFnSc3d1	americká
invazi	invaze	k1gFnSc3	invaze
do	do	k7c2	do
Iráku	Irák	k1gInSc2	Irák
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
televizní	televizní	k2eAgFnSc2d1	televizní
debaty	debata	k1gFnSc2	debata
republikánských	republikánský	k2eAgMnPc2d1	republikánský
kandidátů	kandidát	k1gMnPc2	kandidát
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
v	v	k7c6	v
září	září	k1gNnSc6	září
2015	[number]	k4	2015
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
dřívějšího	dřívější	k2eAgMnSc4d1	dřívější
amerického	americký	k2eAgMnSc4d1	americký
prezidenta	prezident	k1gMnSc4	prezident
George	Georg	k1gMnSc4	Georg
W.	W.	kA	W.
Bushe	Bush	k1gMnSc4	Bush
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc4	jeho
bratra	bratr	k1gMnSc4	bratr
Jeba	Jebus	k1gMnSc4	Jebus
Bushe	Bush	k1gMnSc4	Bush
kvůli	kvůli	k7c3	kvůli
válce	válka	k1gFnSc3	válka
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
oznámení	oznámení	k1gNnSc6	oznámení
své	svůj	k3xOyFgFnSc2	svůj
kandidatury	kandidatura	k1gFnSc2	kandidatura
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
majetek	majetek	k1gInSc4	majetek
o	o	k7c6	o
výši	výše	k1gFnSc6	výše
8,7	[number]	k4	8,7
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
;	;	kIx,	;
podle	podle	k7c2	podle
časopisu	časopis	k1gInSc2	časopis
Forbes	forbes	k1gInSc1	forbes
jeho	on	k3xPp3gInSc4	on
majetek	majetek	k1gInSc4	majetek
činí	činit	k5eAaImIp3nS	činit
4,5	[number]	k4	4,5
mld.	mld.	k?	mld.
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
<g/>
Rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
vedoucím	vedoucí	k2eAgMnSc7d1	vedoucí
kandidátem	kandidát	k1gMnSc7	kandidát
Republikánské	republikánský	k2eAgFnSc2d1	republikánská
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
3	[number]	k4	3
<g/>
.	.	kIx.	.
květnu	květen	k1gInSc6	květen
2016	[number]	k4	2016
Trump	Trump	k1gMnSc1	Trump
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
ve	v	k7c6	v
28	[number]	k4	28
republikánských	republikánský	k2eAgFnPc6d1	republikánská
primárkách	primárky	k1gFnPc6	primárky
a	a	k8xC	a
shromážděních	shromáždění	k1gNnPc6	shromáždění
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
caucuses	caucuses	k1gMnSc1	caucuses
<g/>
)	)	kIx)	)
v	v	k7c6	v
unijních	unijní	k2eAgInPc6d1	unijní
státech	stát	k1gInPc6	stát
a	a	k8xC	a
teritoriích	teritorium	k1gNnPc6	teritorium
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
3	[number]	k4	3
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2016	[number]	k4	2016
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
v	v	k7c6	v
indianských	indianský	k2eAgFnPc6d1	indianská
primárkách	primárky	k1gFnPc6	primárky
<g/>
,	,	kIx,	,
odstoupili	odstoupit	k5eAaPmAgMnP	odstoupit
zbývající	zbývající	k2eAgMnPc1d1	zbývající
dva	dva	k4xCgMnPc1	dva
hlavní	hlavní	k2eAgMnPc1d1	hlavní
soupeři	soupeř	k1gMnPc1	soupeř
–	–	k?	–
texaský	texaský	k2eAgMnSc1d1	texaský
senátor	senátor	k1gMnSc1	senátor
Ted	Ted	k1gMnSc1	Ted
Cruz	Cruz	k1gMnSc1	Cruz
i	i	k8xC	i
ohijský	ohijský	k2eAgMnSc1d1	ohijský
guvernér	guvernér	k1gMnSc1	guvernér
John	John	k1gMnSc1	John
Kasich	Kasich	k1gMnSc1	Kasich
<g/>
,	,	kIx,	,
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
předpokládaným	předpokládaný	k2eAgMnSc7d1	předpokládaný
kandidátem	kandidát	k1gMnSc7	kandidát
republikánů	republikán	k1gMnPc2	republikán
pro	pro	k7c4	pro
prezidentské	prezidentský	k2eAgFnPc4d1	prezidentská
volby	volba	k1gFnPc4	volba
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
pěti	pět	k4xCc2	pět
nejdůležitějších	důležitý	k2eAgInPc2d3	nejdůležitější
průzkumů	průzkum	k1gInPc2	průzkum
veřejného	veřejný	k2eAgNnSc2d1	veřejné
mínění	mínění	k1gNnSc2	mínění
měl	mít	k5eAaImAgInS	mít
Trump	Trump	k1gInSc1	Trump
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2016	[number]	k4	2016
poprvé	poprvé	k6eAd1	poprvé
malý	malý	k2eAgInSc4d1	malý
náskok	náskok	k1gInSc4	náskok
před	před	k7c7	před
svou	svůj	k3xOyFgFnSc7	svůj
pravděpodobnou	pravděpodobný	k2eAgFnSc7d1	pravděpodobná
soupeřkou	soupeřka	k1gFnSc7	soupeřka
v	v	k7c6	v
prezidentských	prezidentský	k2eAgFnPc6d1	prezidentská
volbách	volba	k1gFnPc6	volba
<g/>
,	,	kIx,	,
kandidátkou	kandidátka	k1gFnSc7	kandidátka
Demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
Hillary	Hillara	k1gFnSc2	Hillara
Clintonovou	Clintonová	k1gFnSc7	Clintonová
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
internetové	internetový	k2eAgFnSc6d1	internetová
stránce	stránka	k1gFnSc6	stránka
Real	Real	k1gInSc1	Real
Clear	Cleara	k1gFnPc2	Cleara
Politics	Politicsa	k1gFnPc2	Politicsa
(	(	kIx(	(
<g/>
RCP	RCP	kA	RCP
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
něj	on	k3xPp3gInSc4	on
vypočten	vypočten	k2eAgInSc4d1	vypočten
náskok	náskok	k1gInSc4	náskok
0,2	[number]	k4	0,2
%	%	kIx~	%
v	v	k7c6	v
celých	celý	k2eAgInPc6d1	celý
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Průzkumy	průzkum	k1gInPc1	průzkum
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
dopadly	dopadnout	k5eAaPmAgInP	dopadnout
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
Trumpa	Trump	k1gMnSc2	Trump
(	(	kIx(	(
<g/>
s	s	k7c7	s
procentním	procentní	k2eAgInSc7d1	procentní
náskokem	náskok	k1gInSc7	náskok
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgFnPc4	tento
<g/>
:	:	kIx,	:
Rasmussen	Rasmussen	k2eAgInSc1d1	Rasmussen
Reports	Reports	k1gInSc1	Reports
-	-	kIx~	-
5	[number]	k4	5
%	%	kIx~	%
<g/>
,	,	kIx,	,
Fox	fox	k1gInSc1	fox
News	News	k1gInSc1	News
<g/>
:	:	kIx,	:
3	[number]	k4	3
%	%	kIx~	%
<g/>
,	,	kIx,	,
ABC	ABC	kA	ABC
News	News	k1gInSc1	News
<g/>
/	/	kIx~	/
<g/>
Washington	Washington	k1gInSc1	Washington
Post	posta	k1gFnPc2	posta
-	-	kIx~	-
2	[number]	k4	2
%	%	kIx~	%
<g/>
,	,	kIx,	,
USC	USC	kA	USC
Dornsife	Dornsif	k1gInSc5	Dornsif
<g/>
/	/	kIx~	/
<g/>
Los	los	k1gMnSc1	los
Angeles	Angeles	k1gMnSc1	Angeles
Times	Times	k1gMnSc1	Times
-	-	kIx~	-
3	[number]	k4	3
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
Clintonové	Clintonová	k1gFnSc3	Clintonová
dopadly	dopadnout	k5eAaPmAgInP	dopadnout
tyto	tento	k3xDgInPc1	tento
průzkumy	průzkum	k1gInPc1	průzkum
<g/>
:	:	kIx,	:
NBC	NBC	kA	NBC
News	News	k1gInSc1	News
<g/>
/	/	kIx~	/
<g/>
Wall	Wall	k1gMnSc1	Wall
Street	Street	k1gMnSc1	Street
Journal	Journal	k1gMnSc1	Journal
-	-	kIx~	-
2	[number]	k4	2
%	%	kIx~	%
<g/>
,	,	kIx,	,
CBS	CBS	kA	CBS
News	News	k1gInSc1	News
<g/>
/	/	kIx~	/
<g/>
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
Times	Times	k1gInSc1	Times
-	-	kIx~	-
6	[number]	k4	6
%	%	kIx~	%
<g/>
.	.	kIx.	.
<g/>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
května	květen	k1gInSc2	květen
2016	[number]	k4	2016
měl	mít	k5eAaImAgInS	mít
Trump	Trump	k1gInSc1	Trump
již	již	k6eAd1	již
zajištěn	zajistit	k5eAaPmNgInS	zajistit
potřebný	potřebný	k2eAgInSc1d1	potřebný
počet	počet	k1gInSc1	počet
hlasů	hlas	k1gInPc2	hlas
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
nominaci	nominace	k1gFnSc4	nominace
oficiálním	oficiální	k2eAgMnSc7d1	oficiální
kandidátem	kandidát	k1gMnSc7	kandidát
Republikánské	republikánský	k2eAgFnSc2d1	republikánská
strany	strana	k1gFnSc2	strana
pro	pro	k7c4	pro
prezidentské	prezidentský	k2eAgFnPc4d1	prezidentská
volby	volba	k1gFnPc4	volba
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tiskové	tiskový	k2eAgFnSc2d1	tisková
agentury	agentura	k1gFnSc2	agentura
Associated	Associated	k1gMnSc1	Associated
Press	Press	k1gInSc4	Press
překonal	překonat	k5eAaPmAgMnS	překonat
počtem	počet	k1gInSc7	počet
1	[number]	k4	1
238	[number]	k4	238
delegátů	delegát	k1gMnPc2	delegát
stanovenou	stanovený	k2eAgFnSc4d1	stanovená
hranici	hranice	k1gFnSc4	hranice
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
hlas	hlas	k1gInSc4	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Vyslovili	vyslovit	k5eAaPmAgMnP	vyslovit
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
dosud	dosud	k6eAd1	dosud
nerozhodnutí	rozhodnutý	k2eNgMnPc1d1	nerozhodnutý
delegáti	delegát	k1gMnPc1	delegát
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gNnPc7	on
předsedkyně	předsedkyně	k1gFnSc2	předsedkyně
republikánů	republikán	k1gMnPc2	republikán
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Oklahoma	Oklahoma	k1gFnSc1	Oklahoma
Pam	Pam	k1gFnSc1	Pam
Pollardová	Pollardová	k1gFnSc1	Pollardová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
sčítání	sčítání	k1gNnPc2	sčítání
hlasů	hlas	k1gInPc2	hlas
v	v	k7c6	v
referendu	referendum	k1gNnSc6	referendum
o	o	k7c6	o
členství	členství	k1gNnSc6	členství
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
v	v	k7c6	v
Evropské	evropský	k2eAgFnSc6d1	Evropská
unii	unie	k1gFnSc6	unie
přiletěl	přiletět	k5eAaPmAgMnS	přiletět
Trump	Trump	k1gMnSc1	Trump
dne	den	k1gInSc2	den
24	[number]	k4	24
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2016	[number]	k4	2016
do	do	k7c2	do
Skotska	Skotsko	k1gNnSc2	Skotsko
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
vrtulník	vrtulník	k1gInSc1	vrtulník
pak	pak	k6eAd1	pak
přistál	přistát	k5eAaImAgInS	přistát
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
golfového	golfový	k2eAgNnSc2d1	golfové
hřiště	hřiště	k1gNnSc2	hřiště
Trump	Trump	k1gInSc1	Trump
Turnburry	Turnburra	k1gFnSc2	Turnburra
u	u	k7c2	u
obce	obec	k1gFnSc2	obec
Turnberry	Turnberra	k1gFnSc2	Turnberra
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
země	zem	k1gFnSc2	zem
(	(	kIx(	(
<g/>
hrabství	hrabství	k1gNnSc1	hrabství
Ayrshire	Ayrshir	k1gInSc5	Ayrshir
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
ohlášení	ohlášení	k1gNnSc4	ohlášení
výsledků	výsledek	k1gInPc2	výsledek
referenda	referendum	k1gNnSc2	referendum
prohlásil	prohlásit	k5eAaPmAgInS	prohlásit
Trump	Trump	k1gInSc1	Trump
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
britští	britský	k2eAgMnPc1d1	britský
voliči	volič	k1gMnPc1	volič
si	se	k3xPyFc3	se
vzali	vzít	k5eAaPmAgMnP	vzít
svoji	svůj	k3xOyFgFnSc4	svůj
zemi	zem	k1gFnSc4	zem
zpět	zpět	k6eAd1	zpět
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
obměnou	obměna	k1gFnSc7	obměna
jeho	jeho	k3xOp3gNnSc2	jeho
vlastního	vlastní	k2eAgNnSc2d1	vlastní
hesla	heslo	k1gNnSc2	heslo
z	z	k7c2	z
předvolebního	předvolební	k2eAgInSc2d1	předvolební
boje	boj	k1gInSc2	boj
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
amerického	americký	k2eAgInSc2d1	americký
časopisu	časopis	k1gInSc2	časopis
Time	Tim	k1gFnSc2	Tim
lze	lze	k6eAd1	lze
hlasování	hlasování	k1gNnSc2	hlasování
o	o	k7c4	o
brexitu	brexita	k1gFnSc4	brexita
označit	označit	k5eAaPmF	označit
za	za	k7c4	za
nový	nový	k2eAgInSc4d1	nový
mezník	mezník	k1gInSc4	mezník
v	v	k7c4	v
globální	globální	k2eAgFnSc4d1	globální
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
celosvětové	celosvětový	k2eAgInPc1d1	celosvětový
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
válce	válec	k1gInPc1	válec
o	o	k7c4	o
elity	elita	k1gFnPc4	elita
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jistém	jistý	k2eAgInSc6d1	jistý
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
je	být	k5eAaImIp3nS	být
prý	prý	k9	prý
také	také	k9	také
vítězstvím	vítězství	k1gNnSc7	vítězství
trumpismu	trumpismus	k1gInSc2	trumpismus
ve	v	k7c6	v
světovém	světový	k2eAgNnSc6d1	světové
měřítku	měřítko	k1gNnSc6	měřítko
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2016	[number]	k4	2016
se	se	k3xPyFc4	se
Trump	Trump	k1gMnSc1	Trump
zabýval	zabývat	k5eAaImAgMnS	zabývat
přípravami	příprava	k1gFnPc7	příprava
na	na	k7c4	na
kongres	kongres	k1gInSc4	kongres
Republikánské	republikánský	k2eAgFnSc2d1	republikánská
strany	strana	k1gFnSc2	strana
v	v	k7c6	v
Clevelandu	Cleveland	k1gInSc6	Cleveland
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
toho	ten	k3xDgInSc2	ten
měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yQgNnSc6	který
byl	být	k5eAaImAgInS	být
posléze	posléze	k6eAd1	posléze
určen	určit	k5eAaPmNgInS	určit
oficiálním	oficiální	k2eAgMnSc7d1	oficiální
kandidátem	kandidát	k1gMnSc7	kandidát
strany	strana	k1gFnSc2	strana
pro	pro	k7c4	pro
prezidentské	prezidentský	k2eAgFnPc4d1	prezidentská
volby	volba	k1gFnPc4	volba
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Úvahy	úvaha	k1gFnPc1	úvaha
komentátorů	komentátor	k1gMnPc2	komentátor
se	se	k3xPyFc4	se
točily	točit	k5eAaImAgInP	točit
kolem	kolem	k7c2	kolem
dvou	dva	k4xCgNnPc2	dva
jmen	jméno	k1gNnPc2	jméno
politiků	politik	k1gMnPc2	politik
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
mohli	moct	k5eAaImAgMnP	moct
stát	stát	k5eAaPmF	stát
Trumpovými	Trumpův	k2eAgMnPc7d1	Trumpův
kandidáty	kandidát	k1gMnPc7	kandidát
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
viceprezidenta	viceprezident	k1gMnSc2	viceprezident
USA	USA	kA	USA
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
running	running	k1gInSc1	running
mate	mást	k5eAaImIp3nS	mást
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jmenovitě	jmenovitě	k6eAd1	jmenovitě
to	ten	k3xDgNnSc1	ten
byli	být	k5eAaImAgMnP	být
guvernér	guvernér	k1gMnSc1	guvernér
státu	stát	k1gInSc2	stát
New	New	k1gMnSc1	New
Jersey	Jersea	k1gFnSc2	Jersea
Chris	Chris	k1gFnSc1	Chris
Christie	Christie	k1gFnSc1	Christie
a	a	k8xC	a
guvernér	guvernér	k1gMnSc1	guvernér
státu	stát	k1gInSc2	stát
Indiana	Indiana	k1gFnSc1	Indiana
Mike	Mike	k1gFnSc1	Mike
Pence	pence	k1gFnSc1	pence
<g/>
.	.	kIx.	.
</s>
<s>
Trump	Trump	k1gMnSc1	Trump
si	se	k3xPyFc3	se
pak	pak	k6eAd1	pak
jako	jako	k8xC	jako
svého	svůj	k3xOyFgMnSc2	svůj
spolukandidáta	spolukandidát	k1gMnSc2	spolukandidát
vybral	vybrat	k5eAaPmAgMnS	vybrat
Mika	Mik	k1gMnSc4	Mik
Pence	pence	k1gFnSc1	pence
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zvolený	zvolený	k2eAgMnSc1d1	zvolený
prezident	prezident	k1gMnSc1	prezident
USA	USA	kA	USA
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Přípravy	příprava	k1gFnPc1	příprava
na	na	k7c4	na
převzetí	převzetí	k1gNnSc4	převzetí
úřadu	úřad	k1gInSc2	úřad
====	====	k?	====
</s>
</p>
<p>
<s>
Donald	Donald	k1gMnSc1	Donald
Trump	Trump	k1gMnSc1	Trump
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
dne	den	k1gInSc2	den
8	[number]	k4	8
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2016	[number]	k4	2016
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
tak	tak	k6eAd1	tak
designovaným	designovaný	k2eAgMnSc7d1	designovaný
45	[number]	k4	45
<g/>
.	.	kIx.	.
prezidentem	prezident	k1gMnSc7	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Úřadu	úřad	k1gInSc2	úřad
se	se	k3xPyFc4	se
oficiálně	oficiálně	k6eAd1	oficiálně
ujal	ujmout	k5eAaPmAgInS	ujmout
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
11	[number]	k4	11
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc6	listopad
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
Trump	Trump	k1gInSc4	Trump
budoucího	budoucí	k2eAgMnSc2d1	budoucí
viceprezidenta	viceprezident	k1gMnSc2	viceprezident
USA	USA	kA	USA
Mikea	Mikeus	k1gMnSc2	Mikeus
Pence	pence	k1gFnSc2	pence
vedoucím	vedoucí	k2eAgInPc3d1	vedoucí
týmu	tým	k1gInSc2	tým
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
poté	poté	k6eAd1	poté
připravoval	připravovat	k5eAaImAgInS	připravovat
nástup	nástup	k1gInSc1	nástup
nové	nový	k2eAgFnSc2d1	nová
administrativy	administrativa	k1gFnSc2	administrativa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
týmu	tým	k1gInSc6	tým
dále	daleko	k6eAd2	daleko
pracovali	pracovat	k5eAaImAgMnP	pracovat
Chris	Chris	k1gInSc4	Chris
Christie	Christie	k1gFnSc2	Christie
<g/>
,	,	kIx,	,
guvernér	guvernér	k1gMnSc1	guvernér
státu	stát	k1gInSc2	stát
New	New	k1gFnSc2	New
Jersey	Jersea	k1gFnSc2	Jersea
<g/>
,	,	kIx,	,
generál	generál	k1gMnSc1	generál
Michael	Michael	k1gMnSc1	Michael
T.	T.	kA	T.
Flynn	Flynn	k1gMnSc1	Flynn
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
mluvčí	mluvčí	k1gMnSc1	mluvčí
Sněmovny	sněmovna	k1gFnSc2	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
Newt	Newt	k1gMnSc1	Newt
Gingrich	Gingrich	k1gMnSc1	Gingrich
<g/>
,	,	kIx,	,
lékař	lékař	k1gMnSc1	lékař
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
Ben	Ben	k1gInSc4	Ben
Carson	Carson	k1gMnSc1	Carson
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
starosta	starosta	k1gMnSc1	starosta
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
Rudy	ruda	k1gFnSc2	ruda
Giuliani	Giulian	k1gMnPc1	Giulian
a	a	k8xC	a
senátor	senátor	k1gMnSc1	senátor
za	za	k7c2	za
Alabamu	Alabam	k1gInSc2	Alabam
Jeff	Jeff	k1gInSc1	Jeff
Sessions	Sessions	k1gInSc1	Sessions
<g/>
.	.	kIx.	.
</s>
<s>
Členy	člen	k1gMnPc7	člen
týmu	tým	k1gInSc2	tým
byli	být	k5eAaImAgMnP	být
také	také	k9	také
předseda	předseda	k1gMnSc1	předseda
Republikánské	republikánský	k2eAgFnSc2d1	republikánská
strany	strana	k1gFnSc2	strana
Reince	Reinec	k1gInSc2	Reinec
Priebus	Priebus	k1gInSc1	Priebus
<g/>
,	,	kIx,	,
šéf	šéf	k1gMnSc1	šéf
Trumpovy	Trumpův	k2eAgFnSc2d1	Trumpova
volební	volební	k2eAgFnSc2d1	volební
kampaně	kampaň	k1gFnSc2	kampaň
Stephen	Stephen	k2eAgMnSc1d1	Stephen
Bannon	Bannon	k1gMnSc1	Bannon
a	a	k8xC	a
podnikatel	podnikatel	k1gMnSc1	podnikatel
německého	německý	k2eAgInSc2d1	německý
původu	původ	k1gInSc2	původ
Peter	Peter	k1gMnSc1	Peter
Thiel	Thiel	k1gMnSc1	Thiel
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
investuje	investovat	k5eAaBmIp3nS	investovat
kapitál	kapitál	k1gInSc4	kapitál
v	v	k7c6	v
Silicon	Silicon	kA	Silicon
Valley	Valle	k2eAgFnPc4d1	Valle
<g/>
.	.	kIx.	.
</s>
<s>
Spolupracovali	spolupracovat	k5eAaImAgMnP	spolupracovat
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
rovněž	rovněž	k9	rovněž
Donald	Donald	k1gMnSc1	Donald
Trump	Trump	k1gMnSc1	Trump
Jr	Jr	k1gMnSc1	Jr
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Ivanka	Ivanka	k1gFnSc1	Ivanka
Trumpová	Trumpová	k1gFnSc1	Trumpová
<g/>
,	,	kIx,	,
Eric	Eric	k1gFnSc1	Eric
Trump	Trump	k1gMnSc1	Trump
a	a	k8xC	a
zeť	zeť	k1gMnSc1	zeť
Jared	Jared	k1gMnSc1	Jared
Kushner	Kushner	k1gMnSc1	Kushner
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
13	[number]	k4	13
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2016	[number]	k4	2016
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
Trump	Trump	k1gMnSc1	Trump
dosavadního	dosavadní	k2eAgMnSc4d1	dosavadní
předsedu	předseda	k1gMnSc4	předseda
Republikánského	republikánský	k2eAgInSc2d1	republikánský
národního	národní	k2eAgInSc2d1	národní
výboru	výbor	k1gInSc2	výbor
Reince	Reinec	k1gInSc2	Reinec
Priebuse	Priebuse	k1gFnSc2	Priebuse
do	do	k7c2	do
významné	významný	k2eAgFnSc2d1	významná
funkce	funkce	k1gFnSc2	funkce
jako	jako	k8xC	jako
příští	příští	k2eAgMnSc1d1	příští
šéf	šéf	k1gMnSc1	šéf
jeho	on	k3xPp3gInSc2	on
štábu	štáb	k1gInSc2	štáb
v	v	k7c6	v
Bílém	bílý	k2eAgInSc6d1	bílý
domě	dům	k1gInSc6	dům
<g/>
.	.	kIx.	.
</s>
<s>
Priebusovi	Priebus	k1gMnSc3	Priebus
se	se	k3xPyFc4	se
přičítaly	přičítat	k5eAaImAgInP	přičítat
k	k	k7c3	k
dobru	dobro	k1gNnSc3	dobro
jeho	jeho	k3xOp3gFnSc1	jeho
značná	značný	k2eAgFnSc1d1	značná
podpora	podpora	k1gFnSc1	podpora
Trumpa	Trumpa	k1gFnSc1	Trumpa
ve	v	k7c6	v
volebním	volební	k2eAgInSc6d1	volební
boji	boj	k1gInSc6	boj
<g/>
,	,	kIx,	,
organizační	organizační	k2eAgFnPc4d1	organizační
schopnosti	schopnost	k1gFnPc4	schopnost
a	a	k8xC	a
také	také	k9	také
jeho	jeho	k3xOp3gInPc4	jeho
dobré	dobrý	k2eAgInPc4d1	dobrý
vztahy	vztah	k1gInPc4	vztah
například	například	k6eAd1	například
s	s	k7c7	s
mluvčím	mluvčí	k1gMnSc7	mluvčí
Sněmovny	sněmovna	k1gFnSc2	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
Paulem	Paul	k1gMnSc7	Paul
Ryanem	Ryan	k1gMnSc7	Ryan
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Stephen	Stephen	k2eAgMnSc1d1	Stephen
Bannon	Bannon	k1gMnSc1	Bannon
<g/>
,	,	kIx,	,
dosavadní	dosavadní	k2eAgMnSc1d1	dosavadní
majitel	majitel	k1gMnSc1	majitel
internetového	internetový	k2eAgInSc2d1	internetový
portálu	portál	k1gInSc2	portál
Breitbart	Breitbarta	k1gFnPc2	Breitbarta
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
zaujmout	zaujmout	k5eAaPmF	zaujmout
post	post	k1gInSc4	post
hlavního	hlavní	k2eAgMnSc2d1	hlavní
poradce	poradce	k1gMnSc2	poradce
prezidenta	prezident	k1gMnSc2	prezident
(	(	kIx(	(
<g/>
senior	senior	k1gMnSc1	senior
advisor	advisor	k1gMnSc1	advisor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
vítězství	vítězství	k1gNnSc6	vítězství
Trumpa	Trumpa	k1gFnSc1	Trumpa
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
prezidenta	prezident	k1gMnSc2	prezident
<g/>
,	,	kIx,	,
a	a	k8xC	a
sice	sice	k8xC	sice
14	[number]	k4	14
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
první	první	k4xOgInSc1	první
telefonický	telefonický	k2eAgInSc1d1	telefonický
rozhovor	rozhovor	k1gInSc1	rozhovor
mezi	mezi	k7c7	mezi
ním	on	k3xPp3gMnSc7	on
a	a	k8xC	a
ruským	ruský	k2eAgMnSc7d1	ruský
prezidentem	prezident	k1gMnSc7	prezident
Vladimirem	Vladimir	k1gMnSc7	Vladimir
Putinem	Putin	k1gMnSc7	Putin
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
politikové	politik	k1gMnPc1	politik
si	se	k3xPyFc3	se
navzájem	navzájem	k6eAd1	navzájem
slíbili	slíbit	k5eAaPmAgMnP	slíbit
<g/>
,	,	kIx,	,
že	že	k8xS	že
budou	být	k5eAaImBp3nP	být
konstruktivně	konstruktivně	k6eAd1	konstruktivně
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
<g/>
.	.	kIx.	.
</s>
<s>
Putin	putin	k2eAgMnSc1d1	putin
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
Trumpovi	Trump	k1gMnSc3	Trump
partnerský	partnerský	k2eAgInSc4d1	partnerský
dialog	dialog	k1gInSc4	dialog
za	za	k7c2	za
předpokladu	předpoklad	k1gInSc2	předpoklad
vzájemného	vzájemný	k2eAgInSc2d1	vzájemný
respektu	respekt	k1gInSc2	respekt
a	a	k8xC	a
nevměšování	nevměšování	k1gNnSc2	nevměšování
se	se	k3xPyFc4	se
do	do	k7c2	do
záležitostí	záležitost	k1gFnSc7	záležitost
druhé	druhý	k4xOgFnSc2	druhý
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
se	se	k3xPyFc4	se
shodli	shodnout	k5eAaBmAgMnP	shodnout
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
dosavadní	dosavadní	k2eAgInPc1d1	dosavadní
vztahy	vztah	k1gInPc1	vztah
obou	dva	k4xCgFnPc2	dva
zemí	zem	k1gFnPc2	zem
nejsou	být	k5eNaImIp3nP	být
uspokojivé	uspokojivý	k2eAgFnPc1d1	uspokojivá
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tehdejšího	tehdejší	k2eAgNnSc2d1	tehdejší
prohlášení	prohlášení	k1gNnSc2	prohlášení
Kremlu	Kreml	k1gInSc2	Kreml
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nP	muset
obě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
"	"	kIx"	"
<g/>
vrátit	vrátit	k5eAaPmF	vrátit
k	k	k7c3	k
pragmatické	pragmatický	k2eAgFnSc3d1	pragmatická
spolupráci	spolupráce	k1gFnSc3	spolupráce
k	k	k7c3	k
oboustrannému	oboustranný	k2eAgInSc3d1	oboustranný
užitku	užitek	k1gInSc3	užitek
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zohlední	zohlednit	k5eAaPmIp3nS	zohlednit
zájmy	zájem	k1gInPc4	zájem
obou	dva	k4xCgInPc2	dva
států	stát	k1gInPc2	stát
a	a	k8xC	a
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
a	a	k8xC	a
stabilitu	stabilita	k1gFnSc4	stabilita
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
propojeno	propojen	k2eAgNnSc4d1	propojeno
úsilí	úsilí	k1gNnSc4	úsilí
obou	dva	k4xCgInPc2	dva
států	stát	k1gInPc2	stát
v	v	k7c6	v
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
mezinárodnímu	mezinárodní	k2eAgInSc3d1	mezinárodní
terorismu	terorismus	k1gInSc3	terorismus
a	a	k8xC	a
extremismu	extremismus	k1gInSc3	extremismus
<g/>
.	.	kIx.	.
</s>
<s>
Trumpův	Trumpův	k2eAgInSc1d1	Trumpův
tým	tým	k1gInSc1	tým
se	se	k3xPyFc4	se
vyjádřil	vyjádřit	k5eAaPmAgInS	vyjádřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zvolený	zvolený	k2eAgMnSc1d1	zvolený
prezident	prezident	k1gMnSc1	prezident
hovořil	hovořit	k5eAaImAgMnS	hovořit
s	s	k7c7	s
Putinem	Putin	k1gInSc7	Putin
o	o	k7c6	o
celé	celý	k2eAgFnSc6d1	celá
řadě	řada	k1gFnSc6	řada
témat	téma	k1gNnPc2	téma
<g/>
.	.	kIx.	.
</s>
<s>
Trump	Trump	k1gMnSc1	Trump
si	se	k3xPyFc3	se
přeje	přát	k5eAaImIp3nS	přát
"	"	kIx"	"
<g/>
silné	silný	k2eAgFnPc1d1	silná
a	a	k8xC	a
trvalé	trvalý	k2eAgInPc1d1	trvalý
vztahy	vztah	k1gInPc1	vztah
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Čína	Čína	k1gFnSc1	Čína
podala	podat	k5eAaPmAgFnS	podat
oficiální	oficiální	k2eAgInSc4d1	oficiální
protest	protest	k1gInSc4	protest
proti	proti	k7c3	proti
telefonátu	telefonát	k1gInSc3	telefonát
Trumpa	Trumpa	k1gFnSc1	Trumpa
s	s	k7c7	s
tchajwanskou	tchajwanský	k2eAgFnSc7d1	tchajwanská
prezidentkou	prezidentka	k1gFnSc7	prezidentka
Cchaj	Cchaj	k1gInSc4	Cchaj
Jing-wen	Jingen	k2eAgInSc4d1	Jing-wen
z	z	k7c2	z
2	[number]	k4	2
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byl	být	k5eAaImAgInS	být
první	první	k4xOgInSc4	první
oficiální	oficiální	k2eAgInSc4d1	oficiální
kontakt	kontakt	k1gInSc4	kontakt
mezi	mezi	k7c7	mezi
americkým	americký	k2eAgMnSc7d1	americký
a	a	k8xC	a
tchajwanským	tchajwanský	k2eAgMnSc7d1	tchajwanský
vůdcem	vůdce	k1gMnSc7	vůdce
od	od	k7c2	od
přerušení	přerušení	k1gNnSc2	přerušení
diplomatických	diplomatický	k2eAgInPc2d1	diplomatický
vztahů	vztah	k1gInPc2	vztah
mezi	mezi	k7c7	mezi
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
a	a	k8xC	a
Čínskou	čínský	k2eAgFnSc7d1	čínská
republikou	republika	k1gFnSc7	republika
(	(	kIx(	(
<g/>
Tchaj-wanem	Tchajan	k1gInSc7	Tchaj-wan
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
co	co	k3yQnSc4	co
již	již	k6eAd1	již
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
prezidenta	prezident	k1gMnSc2	prezident
Richarda	Richard	k1gMnSc2	Richard
Nixona	Nixon	k1gMnSc2	Nixon
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
k	k	k7c3	k
velkému	velký	k2eAgInSc3d1	velký
obratu	obrat	k1gInSc3	obrat
v	v	k7c6	v
americké	americký	k2eAgFnSc6d1	americká
politice	politika	k1gFnSc6	politika
vůči	vůči	k7c3	vůči
Číně	Čína	k1gFnSc3	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Obamova	Obamův	k2eAgFnSc1d1	Obamova
administrativa	administrativa	k1gFnSc1	administrativa
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
telefonát	telefonát	k1gInSc4	telefonát
<g/>
,	,	kIx,	,
že	že	k8xS	že
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
nadále	nadále	k6eAd1	nadále
podporují	podporovat	k5eAaImIp3nP	podporovat
politiku	politika	k1gFnSc4	politika
jedné	jeden	k4xCgFnSc2	jeden
Číny	Čína	k1gFnSc2	Čína
a	a	k8xC	a
považují	považovat	k5eAaImIp3nP	považovat
Čínskou	čínský	k2eAgFnSc4d1	čínská
republiku	republika	k1gFnSc4	republika
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Tchaj-wan	Tchajan	k1gInSc4	Tchaj-wan
za	za	k7c4	za
provincii	provincie	k1gFnSc4	provincie
pevninské	pevninský	k2eAgFnSc2d1	pevninská
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Deníky	deník	k1gInPc1	deník
New	New	k1gMnPc2	New
York	York	k1gInSc1	York
Times	Timesa	k1gFnPc2	Timesa
a	a	k8xC	a
Guardian	Guardiana	k1gFnPc2	Guardiana
označily	označit	k5eAaPmAgFnP	označit
Trumpův	Trumpův	k2eAgInSc4d1	Trumpův
telefonát	telefonát	k1gInSc4	telefonát
za	za	k7c4	za
"	"	kIx"	"
<g/>
provokaci	provokace	k1gFnSc4	provokace
<g/>
"	"	kIx"	"
vůči	vůči	k7c3	vůči
pevninské	pevninský	k2eAgFnSc3d1	pevninská
Číně	Čína	k1gFnSc3	Čína
a	a	k8xC	a
Trumpa	Trump	k1gMnSc4	Trump
kritizovali	kritizovat	k5eAaImAgMnP	kritizovat
také	také	k6eAd1	také
někteří	některý	k3yIgMnPc1	některý
američtí	americký	k2eAgMnPc1d1	americký
politici	politik	k1gMnPc1	politik
<g/>
.	.	kIx.	.
</s>
<s>
Trump	Trump	k1gInSc1	Trump
na	na	k7c4	na
kritiku	kritika	k1gFnSc4	kritika
reagoval	reagovat	k5eAaBmAgMnS	reagovat
na	na	k7c6	na
twitteru	twitter	k1gInSc6	twitter
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Je	být	k5eAaImIp3nS	být
zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
<g/>
,	,	kIx,	,
že	že	k8xS	že
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
prodávají	prodávat	k5eAaImIp3nP	prodávat
Tchaj-wanu	Tchajan	k1gInSc2	Tchaj-wan
vojenský	vojenský	k2eAgInSc4d1	vojenský
materiál	materiál	k1gInSc4	materiál
za	za	k7c4	za
miliardy	miliarda	k4xCgFnPc4	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
a	a	k8xC	a
já	já	k3xPp1nSc1	já
bych	by	kYmCp1nS	by
přitom	přitom	k6eAd1	přitom
neměl	mít	k5eNaImAgInS	mít
přijmout	přijmout	k5eAaPmF	přijmout
blahopřejný	blahopřejný	k2eAgInSc1d1	blahopřejný
telefonát	telefonát	k1gInSc1	telefonát
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
====	====	k?	====
Nová	nový	k2eAgFnSc1d1	nová
administrativa	administrativa	k1gFnSc1	administrativa
====	====	k?	====
</s>
</p>
<p>
<s>
Brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
vítězství	vítězství	k1gNnSc6	vítězství
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
začal	začít	k5eAaPmAgMnS	začít
Trump	Trump	k1gMnSc1	Trump
řešit	řešit	k5eAaImF	řešit
personální	personální	k2eAgFnPc4d1	personální
otázky	otázka	k1gFnPc4	otázka
spojené	spojený	k2eAgFnPc4d1	spojená
se	s	k7c7	s
sestavením	sestavení	k1gNnSc7	sestavení
nové	nový	k2eAgFnSc2d1	nová
vlády	vláda	k1gFnSc2	vláda
USA	USA	kA	USA
a	a	k8xC	a
obsazením	obsazení	k1gNnSc7	obsazení
dalších	další	k2eAgFnPc2d1	další
klíčových	klíčový	k2eAgFnPc2d1	klíčová
pozic	pozice	k1gFnPc2	pozice
ve	v	k7c6	v
státním	státní	k2eAgInSc6d1	státní
aparátu	aparát	k1gInSc6	aparát
<g/>
.	.	kIx.	.
</s>
<s>
Deset	deset	k4xCc1	deset
dní	den	k1gInPc2	den
po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
již	již	k9	již
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
o	o	k7c6	o
třech	tři	k4xCgInPc6	tři
důležitých	důležitý	k2eAgInPc6d1	důležitý
postech	post	k1gInPc6	post
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
nového	nový	k2eAgMnSc4d1	nový
ministra	ministr	k1gMnSc4	ministr
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
určil	určit	k5eAaPmAgInS	určit
dosavadního	dosavadní	k2eAgMnSc4d1	dosavadní
senátora	senátor	k1gMnSc4	senátor
za	za	k7c2	za
Alabamu	Alabam	k1gInSc2	Alabam
Jeffa	Jeff	k1gMnSc2	Jeff
Sessionse	Sessions	k1gMnSc2	Sessions
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
poradce	poradce	k1gMnSc1	poradce
pro	pro	k7c4	pro
národní	národní	k2eAgFnSc4d1	národní
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
byl	být	k5eAaImAgInS	být
tehdy	tehdy	k6eAd1	tehdy
nominován	nominován	k2eAgMnSc1d1	nominován
bývalý	bývalý	k2eAgMnSc1d1	bývalý
generálporučík	generálporučík	k1gMnSc1	generálporučík
a	a	k8xC	a
šéf	šéf	k1gMnSc1	šéf
vojenské	vojenský	k2eAgFnSc2d1	vojenská
kontrarozvědky	kontrarozvědka	k1gFnSc2	kontrarozvědka
DIA	DIA	kA	DIA
Michael	Michael	k1gMnSc1	Michael
T.	T.	kA	T.
Flynn	Flynn	k1gInSc1	Flynn
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
však	však	k9	však
začátkem	začátkem	k7c2	začátkem
února	únor	k1gInSc2	únor
2017	[number]	k4	2017
musel	muset	k5eAaImAgMnS	muset
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
funkce	funkce	k1gFnSc2	funkce
kvůli	kvůli	k7c3	kvůli
svým	svůj	k3xOyFgInPc3	svůj
dřívějším	dřívější	k2eAgInPc3d1	dřívější
kontaktům	kontakt	k1gInPc3	kontakt
s	s	k7c7	s
ruskými	ruský	k2eAgMnPc7d1	ruský
činiteli	činitel	k1gMnPc7	činitel
odstoupit	odstoupit	k5eAaPmF	odstoupit
<g/>
.	.	kIx.	.
</s>
<s>
Vystřídal	vystřídat	k5eAaPmAgMnS	vystřídat
jej	on	k3xPp3gNnSc4	on
aktivní	aktivní	k2eAgMnSc1d1	aktivní
generálporučík	generálporučík	k1gMnSc1	generálporučík
H.	H.	kA	H.
R.	R.	kA	R.
McMaster	McMaster	k1gMnSc1	McMaster
(	(	kIx(	(
<g/>
ten	ten	k3xDgMnSc1	ten
vykonával	vykonávat	k5eAaImAgInS	vykonávat
funkci	funkce	k1gFnSc4	funkce
do	do	k7c2	do
23	[number]	k4	23
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2018	[number]	k4	2018
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
vystřídán	vystřídat	k5eAaPmNgInS	vystřídat
Johnem	John	k1gMnSc7	John
Boltonem	Bolton	k1gInSc7	Bolton
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ředitelem	ředitel	k1gMnSc7	ředitel
zpravodajské	zpravodajský	k2eAgFnSc2d1	zpravodajská
služby	služba	k1gFnSc2	služba
CIA	CIA	kA	CIA
se	se	k3xPyFc4	se
po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
Trumpa	Trump	k1gMnSc2	Trump
do	do	k7c2	do
prezidentského	prezidentský	k2eAgInSc2d1	prezidentský
úřadu	úřad	k1gInSc2	úřad
stal	stát	k5eAaPmAgMnS	stát
dosavadní	dosavadní	k2eAgMnSc1d1	dosavadní
republikánský	republikánský	k2eAgMnSc1d1	republikánský
kongresman	kongresman	k1gMnSc1	kongresman
Mike	Mik	k1gFnSc2	Mik
Pompeo	Pompeo	k1gMnSc1	Pompeo
<g/>
.	.	kIx.	.
<g/>
Novou	Nová	k1gFnSc4	Nová
ministryní	ministryně	k1gFnPc2	ministryně
pro	pro	k7c4	pro
vzdělání	vzdělání	k1gNnSc4	vzdělání
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
miliardářka	miliardářka	k1gFnSc1	miliardářka
Betsy	Betsa	k1gFnSc2	Betsa
DeVosová	DeVosová	k1gFnSc1	DeVosová
ze	z	k7c2	z
státu	stát	k1gInSc2	stát
Michigan	Michigan	k1gInSc1	Michigan
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
prošla	projít	k5eAaPmAgFnS	projít
Senátem	senát	k1gInSc7	senát
pouze	pouze	k6eAd1	pouze
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
hlasu	hlas	k1gInSc2	hlas
viceprezidenta	viceprezident	k1gMnSc2	viceprezident
Mika	Mik	k1gMnSc2	Mik
Pence	pence	k1gFnSc1	pence
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
poměrem	poměr	k1gInSc7	poměr
51	[number]	k4	51
:	:	kIx,	:
50	[number]	k4	50
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
velvyslankyni	velvyslankyně	k1gFnSc3	velvyslankyně
při	při	k7c6	při
OSN	OSN	kA	OSN
určil	určit	k5eAaPmAgMnS	určit
Trump	Trump	k1gMnSc1	Trump
dosavadní	dosavadní	k2eAgFnSc4d1	dosavadní
republikánskou	republikánský	k2eAgFnSc4d1	republikánská
guvernérku	guvernérka	k1gFnSc4	guvernérka
Jižní	jižní	k2eAgFnSc2d1	jižní
Karolíny	Karolína	k1gFnSc2	Karolína
Nikki	Nikki	k1gNnSc2	Nikki
Haleyovou	Haleyová	k1gFnSc4	Haleyová
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gMnPc1	jejíž
rodiče	rodič	k1gMnPc1	rodič
se	se	k3xPyFc4	se
do	do	k7c2	do
USA	USA	kA	USA
přistěhovali	přistěhovat	k5eAaPmAgMnP	přistěhovat
z	z	k7c2	z
Indie	Indie	k1gFnSc2	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgNnSc7d1	další
personálním	personální	k2eAgNnSc7d1	personální
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
bylo	být	k5eAaImAgNnS	být
určení	určení	k1gNnSc1	určení
bývalého	bývalý	k2eAgMnSc2d1	bývalý
manažera	manažer	k1gMnSc2	manažer
bankovní	bankovní	k2eAgFnSc2d1	bankovní
společnosti	společnost	k1gFnSc2	společnost
Goldman	Goldman	k1gMnSc1	Goldman
Sachs	Sachs	k1gInSc1	Sachs
Stevena	Steven	k2eAgFnSc1d1	Stevena
Mnuchina	Mnuchina	k1gFnSc1	Mnuchina
jako	jako	k8xS	jako
příštího	příští	k2eAgMnSc4d1	příští
ministra	ministr	k1gMnSc4	ministr
financí	finance	k1gFnPc2	finance
USA	USA	kA	USA
<g/>
.	.	kIx.	.
<g/>
Dalším	další	k2eAgInSc7d1	další
členem	člen	k1gInSc7	člen
Trumpovy	Trumpův	k2eAgFnSc2d1	Trumpova
vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
penzionovaný	penzionovaný	k2eAgMnSc1d1	penzionovaný
66	[number]	k4	66
<g/>
letý	letý	k2eAgMnSc1d1	letý
generál	generál	k1gMnSc1	generál
James	James	k1gMnSc1	James
Mattis	Mattis	k1gFnSc1	Mattis
jako	jako	k8xC	jako
ministr	ministr	k1gMnSc1	ministr
obrany	obrana	k1gFnSc2	obrana
<g/>
.	.	kIx.	.
</s>
<s>
Mattis	Mattis	k1gInSc1	Mattis
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Senátu	senát	k1gInSc6	senát
schválen	schválen	k2eAgInSc4d1	schválen
poměrem	poměr	k1gInSc7	poměr
hlasů	hlas	k1gInPc2	hlas
98	[number]	k4	98
:	:	kIx,	:
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
své	svůj	k3xOyFgFnSc2	svůj
44	[number]	k4	44
<g/>
leté	letý	k2eAgFnSc2d1	letá
působnosti	působnost	k1gFnSc2	působnost
v	v	k7c6	v
ozbrojených	ozbrojený	k2eAgFnPc6d1	ozbrojená
silách	síla	k1gFnPc6	síla
USA	USA	kA	USA
sloužil	sloužit	k5eAaImAgMnS	sloužit
Mattis	Mattis	k1gInSc4	Mattis
převážně	převážně	k6eAd1	převážně
u	u	k7c2	u
složek	složka	k1gFnPc2	složka
námořní	námořní	k2eAgFnSc2d1	námořní
pěchoty	pěchota	k1gFnSc2	pěchota
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
v	v	k7c6	v
obou	dva	k4xCgFnPc6	dva
válkách	válka	k1gFnPc6	válka
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
a	a	k8xC	a
v	v	k7c4	v
Afghánistán	Afghánistán	k1gInSc4	Afghánistán
<g/>
.	.	kIx.	.
<g/>
Jako	jako	k8xC	jako
nový	nový	k2eAgMnSc1d1	nový
ministr	ministr	k1gMnSc1	ministr
pro	pro	k7c4	pro
bytovou	bytový	k2eAgFnSc4d1	bytová
výstavbu	výstavba	k1gFnSc4	výstavba
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc4	rozvoj
měst	město	k1gNnPc2	město
byl	být	k5eAaImAgMnS	být
potvrzen	potvrzen	k2eAgMnSc1d1	potvrzen
afroamerický	afroamerický	k2eAgMnSc1d1	afroamerický
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
bývalý	bývalý	k2eAgMnSc1d1	bývalý
Trumpův	Trumpův	k2eAgMnSc1d1	Trumpův
konkurent	konkurent	k1gMnSc1	konkurent
v	v	k7c6	v
kandidatuře	kandidatura	k1gFnSc6	kandidatura
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
Ben	Ben	k1gInSc4	Ben
Carson	Carson	k1gNnSc4	Carson
<g/>
,	,	kIx,	,
původním	původní	k2eAgNnSc7d1	původní
povoláním	povolání	k1gNnSc7	povolání
chirurg	chirurg	k1gMnSc1	chirurg
<g/>
.	.	kIx.	.
<g/>
Postupně	postupně	k6eAd1	postupně
byla	být	k5eAaImAgNnP	být
oznamována	oznamován	k2eAgNnPc1d1	oznamováno
jména	jméno	k1gNnPc1	jméno
dalších	další	k2eAgMnPc2d1	další
potenciálních	potenciální	k2eAgMnPc2d1	potenciální
členů	člen	k1gMnPc2	člen
příští	příští	k2eAgFnSc2d1	příští
Trumpovy	Trumpův	k2eAgFnSc2d1	Trumpova
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Byli	být	k5eAaImAgMnP	být
to	ten	k3xDgNnSc4	ten
mj.	mj.	kA	mj.
<g/>
:	:	kIx,	:
Wilbur	Wilbur	k1gMnSc1	Wilbur
Ross	Rossa	k1gFnPc2	Rossa
<g/>
,	,	kIx,	,
78	[number]	k4	78
<g/>
letý	letý	k2eAgMnSc1d1	letý
finančník	finančník	k1gMnSc1	finančník
jako	jako	k8xC	jako
ministr	ministr	k1gMnSc1	ministr
obchodu	obchod	k1gInSc2	obchod
<g/>
,	,	kIx,	,
Andrew	Andrew	k1gMnSc1	Andrew
Puzder	Puzder	k1gMnSc1	Puzder
jako	jako	k8xS	jako
ministr	ministr	k1gMnSc1	ministr
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
však	však	k9	však
Senátem	senát	k1gInSc7	senát
neprošel	projít	k5eNaPmAgMnS	projít
<g/>
,	,	kIx,	,
Elaine	elain	k1gInSc5	elain
Chaová	Chaová	k1gFnSc1	Chaová
<g/>
,	,	kIx,	,
manželka	manželka	k1gFnSc1	manželka
vlivného	vlivný	k2eAgMnSc2d1	vlivný
senátora	senátor	k1gMnSc2	senátor
Mitche	Mitch	k1gMnSc2	Mitch
McConnella	McConnell	k1gMnSc2	McConnell
pro	pro	k7c4	pro
post	post	k1gInSc4	post
ministryně	ministryně	k1gFnSc2	ministryně
dopravy	doprava	k1gFnSc2	doprava
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
původem	původ	k1gInSc7	původ
z	z	k7c2	z
Tchaj-wanu	Tchajan	k1gInSc2	Tchaj-wan
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
a	a	k8xC	a
donedávna	donedávna	k6eAd1	donedávna
aktivně	aktivně	k6eAd1	aktivně
činný	činný	k2eAgMnSc1d1	činný
čtyřhvězdičkový	čtyřhvězdičkový	k2eAgMnSc1d1	čtyřhvězdičkový
generál	generál	k1gMnSc1	generál
John	John	k1gMnSc1	John
Kelly	Kella	k1gFnSc2	Kella
jako	jako	k8xS	jako
ministr	ministr	k1gMnSc1	ministr
pro	pro	k7c4	pro
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
(	(	kIx(	(
<g/>
Kelly	Kell	k1gInPc1	Kell
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stal	stát	k5eAaPmAgMnS	stát
šéfem	šéf	k1gMnSc7	šéf
štábu	štáb	k1gInSc2	štáb
Bílého	bílý	k2eAgInSc2d1	bílý
domu	dům	k1gInSc2	dům
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Jednou	jednou	k9	jednou
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
pozic	pozice	k1gFnPc2	pozice
v	v	k7c6	v
americké	americký	k2eAgFnSc6d1	americká
vládě	vláda	k1gFnSc6	vláda
je	být	k5eAaImIp3nS	být
místo	místo	k1gNnSc4	místo
ministra	ministr	k1gMnSc2	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
(	(	kIx(	(
<g/>
oficiálně	oficiálně	k6eAd1	oficiálně
Secretary	Secretara	k1gFnPc1	Secretara
of	of	k?	of
State	status	k1gInSc5	status
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
5	[number]	k4	5
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2016	[number]	k4	2016
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
spekulace	spekulace	k1gFnPc1	spekulace
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
jím	on	k3xPp3gMnSc7	on
mohl	moct	k5eAaImAgMnS	moct
stát	stát	k5eAaImF	stát
Rex	Rex	k1gMnSc1	Rex
Tillerson	Tillerson	k1gMnSc1	Tillerson
<g/>
,	,	kIx,	,
dosavadní	dosavadní	k2eAgMnSc1d1	dosavadní
předseda	předseda	k1gMnSc1	předseda
představenstva	představenstvo	k1gNnSc2	představenstvo
velké	velký	k2eAgFnSc2d1	velká
olejářské	olejářský	k2eAgFnSc2d1	olejářská
společnosti	společnost	k1gFnSc2	společnost
ExxonMobil	ExxonMobil	k1gFnSc2	ExxonMobil
<g/>
.	.	kIx.	.
13	[number]	k4	13
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2016	[number]	k4	2016
potvrdil	potvrdit	k5eAaPmAgInS	potvrdit
Trump	Trump	k1gInSc1	Trump
<g/>
,	,	kIx,	,
že	že	k8xS	že
navrhne	navrhnout	k5eAaPmIp3nS	navrhnout
Tillersona	Tillersona	k1gFnSc1	Tillersona
jako	jako	k8xS	jako
svého	svůj	k3xOyFgMnSc2	svůj
ministra	ministr	k1gMnSc2	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Tillersonova	Tillersonův	k2eAgFnSc1d1	Tillersonův
nominace	nominace	k1gFnSc1	nominace
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
proceduře	procedura	k1gFnSc6	procedura
nakonec	nakonec	k6eAd1	nakonec
potvrzena	potvrdit	k5eAaPmNgFnS	potvrdit
Senátem	senát	k1gInSc7	senát
USA	USA	kA	USA
<g/>
,	,	kIx,	,
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
postu	post	k1gInSc6	post
však	však	k9	však
setrval	setrvat	k5eAaPmAgMnS	setrvat
jen	jen	k9	jen
do	do	k7c2	do
března	březen	k1gInSc2	březen
2018	[number]	k4	2018
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
prezintem	prezint	k1gMnSc7	prezint
Trumpem	Trump	k1gMnSc7	Trump
odvolán	odvolat	k5eAaPmNgMnS	odvolat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
místo	místo	k1gNnSc4	místo
má	mít	k5eAaImIp3nS	mít
přijít	přijít	k5eAaPmF	přijít
dosavadní	dosavadní	k2eAgMnSc1d1	dosavadní
ředitel	ředitel	k1gMnSc1	ředitel
zpravodajské	zpravodajský	k2eAgFnSc2d1	zpravodajská
služby	služba	k1gFnSc2	služba
CIA	CIA	kA	CIA
Mike	Mike	k1gFnSc1	Mike
Pompeo	Pompeo	k1gMnSc1	Pompeo
(	(	kIx(	(
<g/>
toho	ten	k3xDgInSc2	ten
má	mít	k5eAaImIp3nS	mít
vystřídat	vystřídat	k5eAaPmF	vystřídat
Gina	Gin	k2eAgFnSc1d1	Gina
Haspelová	Haspelová	k1gFnSc1	Haspelová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Koncem	koncem	k7c2	koncem
prosince	prosinec	k1gInSc2	prosinec
2016	[number]	k4	2016
byl	být	k5eAaImAgInS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
další	další	k2eAgMnSc1d1	další
Trumpův	Trumpův	k2eAgMnSc1d1	Trumpův
poradce	poradce	k1gMnSc1	poradce
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
miliardář	miliardář	k1gMnSc1	miliardář
a	a	k8xC	a
výrobce	výrobce	k1gMnSc1	výrobce
elektricky	elektricky	k6eAd1	elektricky
poháněných	poháněný	k2eAgInPc2d1	poháněný
automobilů	automobil	k1gInPc2	automobil
značky	značka	k1gFnSc2	značka
Tesla	Tesla	k1gFnSc1	Tesla
Elon	Elon	k1gMnSc1	Elon
Musk	Musk	k1gMnSc1	Musk
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2016	[number]	k4	2016
bylo	být	k5eAaImAgNnS	být
oznámena	oznámit	k5eAaPmNgFnS	oznámit
nominace	nominace	k1gFnSc2	nominace
Ryana	Ryan	k1gMnSc4	Ryan
Zinkeho	Zinke	k1gMnSc4	Zinke
<g/>
,	,	kIx,	,
dosavadního	dosavadní	k2eAgMnSc4d1	dosavadní
člena	člen	k1gMnSc4	člen
Sněmovny	sněmovna	k1gFnSc2	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
za	za	k7c4	za
stát	stát	k1gInSc4	stát
Montana	Montana	k1gFnSc1	Montana
<g/>
,	,	kIx,	,
na	na	k7c4	na
post	post	k1gInSc4	post
ministra	ministr	k1gMnSc2	ministr
vnitra	vnitro	k1gNnSc2	vnitro
<g/>
.	.	kIx.	.
<g/>
Trump	Trump	k1gMnSc1	Trump
také	také	k9	také
oznámil	oznámit	k5eAaPmAgMnS	oznámit
svůj	svůj	k3xOyFgInSc4	svůj
úmysl	úmysl	k1gInSc4	úmysl
vytvořit	vytvořit	k5eAaPmF	vytvořit
nový	nový	k2eAgInSc4d1	nový
post	post	k1gInSc4	post
zvláštního	zvláštní	k2eAgMnSc2d1	zvláštní
zplnomocněnce	zplnomocněnec	k1gMnSc2	zplnomocněnec
pro	pro	k7c4	pro
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
vyjednávání	vyjednávání	k1gNnSc4	vyjednávání
<g/>
.	.	kIx.	.
</s>
<s>
Stane	stanout	k5eAaPmIp3nS	stanout
se	se	k3xPyFc4	se
jím	on	k3xPp3gInSc7	on
49	[number]	k4	49
<g/>
letý	letý	k2eAgMnSc1d1	letý
hlavní	hlavní	k2eAgMnSc1d1	hlavní
právní	právní	k2eAgMnSc1d1	právní
poradce	poradce	k1gMnSc1	poradce
Trumpových	Trumpův	k2eAgFnPc2d1	Trumpova
firem	firma	k1gFnPc2	firma
Jason	Jason	k1gMnSc1	Jason
Greenblatt	Greenblatt	k1gMnSc1	Greenblatt
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
židovského	židovský	k2eAgNnSc2d1	Židovské
vyznání	vyznání	k1gNnSc2	vyznání
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
David	David	k1gMnSc1	David
Friedman	Friedman	k1gMnSc1	Friedman
<g/>
,	,	kIx,	,
budoucí	budoucí	k2eAgMnSc1d1	budoucí
velvyslanec	velvyslanec	k1gMnSc1	velvyslanec
v	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
jmenování	jmenování	k1gNnPc1	jmenování
zapadají	zapadat	k5eAaPmIp3nP	zapadat
do	do	k7c2	do
proklamovaného	proklamovaný	k2eAgNnSc2d1	proklamované
značného	značný	k2eAgNnSc2d1	značné
zlepšení	zlepšení	k1gNnSc2	zlepšení
vztahů	vztah	k1gInPc2	vztah
mezi	mezi	k7c7	mezi
Izraelem	Izrael	k1gInSc7	Izrael
a	a	k8xC	a
USA	USA	kA	USA
po	po	k7c6	po
Trumpově	Trumpův	k2eAgInSc6d1	Trumpův
nástupu	nástup	k1gInSc6	nástup
do	do	k7c2	do
prezidentského	prezidentský	k2eAgInSc2d1	prezidentský
úřadu	úřad	k1gInSc2	úřad
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
co	co	k9	co
se	se	k3xPyFc4	se
tyto	tento	k3xDgInPc1	tento
vztahy	vztah	k1gInPc1	vztah
v	v	k7c6	v
posledním	poslední	k2eAgNnSc6d1	poslední
období	období	k1gNnSc6	období
Obamovy	Obamův	k2eAgFnSc2d1	Obamova
administrativy	administrativa	k1gFnSc2	administrativa
naopak	naopak	k6eAd1	naopak
podstatně	podstatně	k6eAd1	podstatně
zhoršily	zhoršit	k5eAaPmAgFnP	zhoršit
<g/>
.	.	kIx.	.
<g/>
Jednou	jednou	k9	jednou
z	z	k7c2	z
pozdních	pozdní	k2eAgFnPc2d1	pozdní
nominací	nominace	k1gFnPc2	nominace
bylo	být	k5eAaImAgNnS	být
jmenování	jmenování	k1gNnSc1	jmenování
budoucího	budoucí	k2eAgMnSc2d1	budoucí
ředitele	ředitel	k1gMnSc2	ředitel
všech	všecek	k3xTgInPc6	všecek
17	[number]	k4	17
amerických	americký	k2eAgFnPc2d1	americká
rozvědek	rozvědka	k1gFnPc2	rozvědka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
celkem	celkem	k6eAd1	celkem
zhruba	zhruba	k6eAd1	zhruba
100	[number]	k4	100
000	[number]	k4	000
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
a	a	k8xC	a
disponují	disponovat	k5eAaBmIp3nP	disponovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
50	[number]	k4	50
miliardami	miliarda	k4xCgFnPc7	miliarda
dolarů	dolar	k1gInPc2	dolar
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
se	se	k3xPyFc4	se
jím	on	k3xPp3gMnSc7	on
stát	stát	k1gInSc1	stát
Dan	Dan	k1gMnSc1	Dan
Coats	Coats	k1gInSc1	Coats
<g/>
,	,	kIx,	,
dosavadní	dosavadní	k2eAgMnSc1d1	dosavadní
senátor	senátor	k1gMnSc1	senátor
USA	USA	kA	USA
za	za	k7c4	za
stát	stát	k1gInSc4	stát
Indiana	Indiana	k1gFnSc1	Indiana
a	a	k8xC	a
bývalý	bývalý	k2eAgMnSc1d1	bývalý
velvyslanec	velvyslanec	k1gMnSc1	velvyslanec
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc7d1	poslední
nominací	nominace	k1gFnSc7	nominace
před	před	k7c7	před
inaugurací	inaugurace	k1gFnSc7	inaugurace
Donalda	Donald	k1gMnSc2	Donald
Trumpa	Trump	k1gMnSc2	Trump
jako	jako	k8xS	jako
nového	nový	k2eAgMnSc2d1	nový
prezidenta	prezident	k1gMnSc2	prezident
USA	USA	kA	USA
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
ohlášena	ohlášen	k2eAgFnSc1d1	ohlášena
19	[number]	k4	19
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
jmenování	jmenování	k1gNnSc1	jmenování
bývalého	bývalý	k2eAgMnSc2d1	bývalý
guvernéra	guvernér	k1gMnSc2	guvernér
státu	stát	k1gInSc2	stát
Georgia	Georgius	k1gMnSc2	Georgius
<g/>
,	,	kIx,	,
70	[number]	k4	70
<g/>
letého	letý	k2eAgMnSc2d1	letý
Sonnyho	Sonny	k1gMnSc2	Sonny
Perdue	Perdue	k1gNnSc2	Perdue
<g/>
,	,	kIx,	,
ministrem	ministr	k1gMnSc7	ministr
zemědělství	zemědělství	k1gNnSc2	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgMnSc1	tento
politik	politik	k1gMnSc1	politik
však	však	k9	však
ještě	ještě	k6eAd1	ještě
čeká	čekat	k5eAaImIp3nS	čekat
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
potvrzení	potvrzení	k1gNnSc4	potvrzení
Senátem	senát	k1gInSc7	senát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ke	k	k7c3	k
dni	den	k1gInSc3	den
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
ke	k	k7c3	k
dni	den	k1gInSc3	den
inaugurace	inaugurace	k1gFnSc2	inaugurace
prezidenta	prezident	k1gMnSc2	prezident
USA	USA	kA	USA
Donalda	Donald	k1gMnSc2	Donald
Trumpa	Trump	k1gMnSc2	Trump
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
Senátem	senát	k1gInSc7	senát
USA	USA	kA	USA
potvrzeni	potvrdit	k5eAaPmNgMnP	potvrdit
pouze	pouze	k6eAd1	pouze
dva	dva	k4xCgMnPc1	dva
ministři	ministr	k1gMnPc1	ministr
<g/>
.	.	kIx.	.
</s>
<s>
Svých	svůj	k3xOyFgInPc2	svůj
úřadů	úřad	k1gInPc2	úřad
se	se	k3xPyFc4	se
mohli	moct	k5eAaImAgMnP	moct
toho	ten	k3xDgInSc2	ten
dne	den	k1gInSc2	den
ujmout	ujmout	k5eAaPmF	ujmout
James	James	k1gInSc4	James
Mattis	Mattis	k1gFnSc2	Mattis
jako	jako	k8xC	jako
ministr	ministr	k1gMnSc1	ministr
obrany	obrana	k1gFnSc2	obrana
(	(	kIx(	(
<g/>
šéf	šéf	k1gMnSc1	šéf
Pentagonu	Pentagon	k1gInSc2	Pentagon
<g/>
)	)	kIx)	)
a	a	k8xC	a
John	John	k1gMnSc1	John
Kelly	Kella	k1gFnSc2	Kella
jako	jako	k8xC	jako
ministr	ministr	k1gMnSc1	ministr
pro	pro	k7c4	pro
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Členové	člen	k1gMnPc1	člen
Trumpovy	Trumpův	k2eAgFnSc2d1	Trumpova
administrativy	administrativa	k1gFnSc2	administrativa
jsou	být	k5eAaImIp3nP	být
co	co	k9	co
do	do	k7c2	do
kumulovaného	kumulovaný	k2eAgInSc2d1	kumulovaný
majetku	majetek	k1gInSc2	majetek
"	"	kIx"	"
<g/>
nejbohatší	bohatý	k2eAgFnSc7d3	nejbohatší
<g/>
"	"	kIx"	"
vládou	vláda	k1gFnSc7	vláda
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
USA	USA	kA	USA
<g/>
,	,	kIx,	,
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
i	i	k9	i
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
administrativě	administrativa	k1gFnSc6	administrativa
je	být	k5eAaImIp3nS	být
mnoho	mnoho	k4c1	mnoho
dolarových	dolarový	k2eAgMnPc2d1	dolarový
multimilionářů	multimilionář	k1gMnPc2	multimilionář
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
dvou	dva	k4xCgMnPc2	dva
miliardářů	miliardář	k1gMnPc2	miliardář
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
prezidenta	prezident	k1gMnSc2	prezident
samotného	samotný	k2eAgMnSc2d1	samotný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
Willbura	Willbura	k1gFnSc1	Willbura
Rosse	Rosse	k1gFnSc2	Rosse
a	a	k8xC	a
Betsy	Betsa	k1gFnSc2	Betsa
DeVos	DeVosa	k1gFnPc2	DeVosa
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
jsou	být	k5eAaImIp3nP	být
nebo	nebo	k8xC	nebo
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
zastoupeni	zastoupen	k2eAgMnPc1d1	zastoupen
významní	významný	k2eAgMnPc1d1	významný
manažeři	manažer	k1gMnPc1	manažer
z	z	k7c2	z
různých	různý	k2eAgNnPc2d1	různé
odvětví	odvětví	k1gNnPc2	odvětví
průmyslu	průmysl	k1gInSc2	průmysl
(	(	kIx(	(
<g/>
energetika	energetika	k1gFnSc1	energetika
<g/>
,	,	kIx,	,
finance	finance	k1gFnPc1	finance
<g/>
,	,	kIx,	,
nemovitosti	nemovitost	k1gFnPc1	nemovitost
<g/>
)	)	kIx)	)
jako	jako	k9	jako
například	například	k6eAd1	například
ministr	ministr	k1gMnSc1	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Rex	Rex	k1gMnSc1	Rex
Tillerson	Tillerson	k1gMnSc1	Tillerson
a	a	k8xC	a
ministr	ministr	k1gMnSc1	ministr
financí	finance	k1gFnPc2	finance
Steven	Stevna	k1gFnPc2	Stevna
Mnuchin	Mnuchin	k2eAgInSc1d1	Mnuchin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Prezident	prezident	k1gMnSc1	prezident
USA	USA	kA	USA
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
2017	[number]	k4	2017
===	===	k?	===
</s>
</p>
<p>
<s>
Od	od	k7c2	od
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
strávil	strávit	k5eAaPmAgMnS	strávit
prezident	prezident	k1gMnSc1	prezident
Trump	Trump	k1gMnSc1	Trump
zhruba	zhruba	k6eAd1	zhruba
třetinu	třetina	k1gFnSc4	třetina
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
(	(	kIx(	(
<g/>
celkem	celkem	k6eAd1	celkem
111	[number]	k4	111
dní	den	k1gInPc2	den
<g/>
)	)	kIx)	)
v	v	k7c6	v
budovách	budova	k1gFnPc6	budova
a	a	k8xC	a
rezortech	rezort	k1gInPc6	rezort
nesoucích	nesoucí	k2eAgFnPc2d1	nesoucí
jeho	jeho	k3xOp3gNnSc6	jeho
jméno	jméno	k1gNnSc1	jméno
nebo	nebo	k8xC	nebo
mu	on	k3xPp3gMnSc3	on
patřících	patřící	k2eAgMnPc2d1	patřící
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
rezidenci	rezidence	k1gFnSc6	rezidence
Mar-a-Lago	Mar-Lago	k6eAd1	Mar-a-Lago
ve	v	k7c4	v
West	West	k2eAgInSc4d1	West
Palm	Palm	k1gInSc4	Palm
Beach	Beacha	k1gFnPc2	Beacha
na	na	k7c6	na
Floridě	Florida	k1gFnSc6	Florida
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtinu	čtvrtina	k1gFnSc4	čtvrtina
dní	den	k1gInPc2	den
od	od	k7c2	od
své	svůj	k3xOyFgFnSc2	svůj
inaugurace	inaugurace	k1gFnSc2	inaugurace
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
hrál	hrát	k5eAaImAgInS	hrát
Trump	Trump	k1gInSc1	Trump
golf	golf	k1gInSc4	golf
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
ovšem	ovšem	k9	ovšem
s	s	k7c7	s
politiky	politik	k1gMnPc7	politik
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
republikánští	republikánský	k2eAgMnPc1d1	republikánský
senátoři	senátor	k1gMnPc1	senátor
Lindsey	Lindsea	k1gFnSc2	Lindsea
Graham	graham	k1gInSc1	graham
a	a	k8xC	a
Rand	rand	k1gInSc1	rand
Paul	Paul	k1gMnSc1	Paul
nebo	nebo	k8xC	nebo
japonský	japonský	k2eAgMnSc1d1	japonský
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
Šinzo	Šinza	k1gFnSc5	Šinza
Abe	Abe	k1gMnSc7	Abe
<g/>
.	.	kIx.	.
</s>
<s>
Americké	americký	k2eAgFnPc1d1	americká
daňové	daňový	k2eAgFnPc1d1	daňová
poplatníky	poplatník	k1gMnPc4	poplatník
tento	tento	k3xDgInSc1	tento
Trumpův	Trumpův	k2eAgMnSc1d1	Trumpův
"	"	kIx"	"
<g/>
oddechový	oddechový	k2eAgInSc1d1	oddechový
čas	čas	k1gInSc1	čas
<g/>
"	"	kIx"	"
údajně	údajně	k6eAd1	údajně
stál	stát	k5eAaImAgInS	stát
43	[number]	k4	43
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
na	na	k7c6	na
výdajích	výdaj	k1gInPc6	výdaj
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
náklady	náklad	k1gInPc4	náklad
letů	let	k1gInPc2	let
apod.	apod.	kA	apod.
</s>
</p>
<p>
<s>
====	====	k?	====
Leden	leden	k1gInSc1	leden
====	====	k?	====
</s>
</p>
<p>
<s>
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2017	[number]	k4	2017
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
slavnostní	slavnostní	k2eAgFnSc1d1	slavnostní
inaugurace	inaugurace	k1gFnSc1	inaugurace
Donalda	Donald	k1gMnSc2	Donald
Trumpa	Trump	k1gMnSc2	Trump
jako	jako	k9	jako
45	[number]	k4	45
<g/>
.	.	kIx.	.
prezidenta	prezident	k1gMnSc4	prezident
USA	USA	kA	USA
před	před	k7c7	před
Kapitolem	Kapitol	k1gInSc7	Kapitol
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
<g/>
.	.	kIx.	.
</s>
<s>
Následovaly	následovat	k5eAaImAgInP	následovat
Trumpův	Trumpův	k2eAgInSc4d1	Trumpův
nástupní	nástupní	k2eAgInSc4d1	nástupní
projev	projev	k1gInSc4	projev
<g/>
,	,	kIx,	,
jízda	jízda	k1gFnSc1	jízda
automobilem	automobil	k1gInSc7	automobil
od	od	k7c2	od
Kapitolu	Kapitol	k1gInSc2	Kapitol
k	k	k7c3	k
Bílému	bílý	k2eAgInSc3d1	bílý
domu	dům	k1gInSc3	dům
<g/>
,	,	kIx,	,
slavnostní	slavnostní	k2eAgNnSc1d1	slavnostní
defilé	defilé	k1gNnSc1	defilé
vojenských	vojenský	k2eAgFnPc2d1	vojenská
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
formací	formace	k1gFnPc2	formace
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
oslavy	oslava	k1gFnPc1	oslava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
21	[number]	k4	21
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
vydal	vydat	k5eAaPmAgMnS	vydat
Trump	Trump	k1gMnSc1	Trump
výkonné	výkonný	k2eAgNnSc4d1	výkonné
nařízení	nařízení	k1gNnSc4	nařízení
o	o	k7c6	o
minimalizaci	minimalizace	k1gFnSc6	minimalizace
finanční	finanční	k2eAgFnSc2d1	finanční
zátěže	zátěž	k1gFnSc2	zátěž
spojenė	spojenė	k?	spojenė
se	s	k7c7	s
zdravotní	zdravotní	k2eAgFnSc7d1	zdravotní
reformou	reforma	k1gFnSc7	reforma
exprezidenta	exprezident	k1gMnSc2	exprezident
Baracka	Baracko	k1gNnSc2	Baracko
Obamy	Obama	k1gFnSc2	Obama
<g/>
,	,	kIx,	,
než	než	k8xS	než
bude	být	k5eAaImBp3nS	být
program	program	k1gInSc1	program
"	"	kIx"	"
<g/>
Obamacare	Obamacar	k1gMnSc5	Obamacar
<g/>
"	"	kIx"	"
zcela	zcela	k6eAd1	zcela
zrušen	zrušit	k5eAaPmNgInS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
navštívil	navštívit	k5eAaPmAgInS	navštívit
centrálu	centrála	k1gFnSc4	centrála
CIA	CIA	kA	CIA
v	v	k7c4	v
Langley	Langlea	k1gFnPc4	Langlea
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Virginie	Virginie	k1gFnSc2	Virginie
<g/>
,	,	kIx,	,
a	a	k8xC	a
podpořil	podpořit	k5eAaPmAgMnS	podpořit
její	její	k3xOp3gFnSc4	její
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
vydal	vydat	k5eAaPmAgMnS	vydat
vládní	vládní	k2eAgInSc4d1	vládní
příkaz	příkaz	k1gInSc4	příkaz
k	k	k7c3	k
23	[number]	k4	23
<g/>
.	.	kIx.	.
lednu	ledno	k1gNnSc6wB	ledno
obnovující	obnovující	k2eAgFnSc2d1	obnovující
tzv.	tzv.	kA	tzv.
Mexico	Mexico	k6eAd1	Mexico
City	city	k1gNnSc1	city
Policy	Polica	k1gFnSc2	Polica
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zakazuje	zakazovat	k5eAaImIp3nS	zakazovat
přidělování	přidělování	k1gNnSc4	přidělování
finanční	finanční	k2eAgFnSc2d1	finanční
podpory	podpora	k1gFnSc2	podpora
z	z	k7c2	z
federálních	federální	k2eAgInPc2d1	federální
zdrojů	zdroj	k1gInPc2	zdroj
organizacím	organizace	k1gFnPc3	organizace
podporujícím	podporující	k2eAgInSc6d1	podporující
potraty	potrat	k1gInPc4	potrat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
pak	pak	k6eAd1	pak
jí	on	k3xPp3gFnSc3	on
daná	daný	k2eAgFnSc1d1	daná
omezení	omezení	k1gNnSc4	omezení
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
i	i	k9	i
na	na	k7c4	na
zahraniční	zahraniční	k2eAgFnSc4d1	zahraniční
pomoc	pomoc	k1gFnSc4	pomoc
rozdělovanou	rozdělovaný	k2eAgFnSc4d1	rozdělovaná
přímo	přímo	k6eAd1	přímo
vládními	vládní	k2eAgInPc7d1	vládní
úřady	úřad	k1gInPc7	úřad
USA	USA	kA	USA
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
23	[number]	k4	23
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
vypověděl	vypovědět	k5eAaPmAgMnS	vypovědět
Trump	Trump	k1gInSc4	Trump
doposud	doposud	k6eAd1	doposud
neratifikovanou	ratifikovaný	k2eNgFnSc4d1	neratifikovaná
smlouvu	smlouva	k1gFnSc4	smlouva
o	o	k7c6	o
Transpacifickém	Transpacifický	k2eAgNnSc6d1	Transpacifický
partnerství	partnerství	k1gNnSc6	partnerství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
25	[number]	k4	25
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
vydal	vydat	k5eAaPmAgMnS	vydat
Trump	Trump	k1gMnSc1	Trump
výkonné	výkonný	k2eAgNnSc4d1	výkonné
nařízení	nařízení	k1gNnSc4	nařízení
k	k	k7c3	k
další	další	k2eAgFnSc3d1	další
výstavbě	výstavba	k1gFnSc3	výstavba
zdi	zeď	k1gFnSc2	zeď
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
s	s	k7c7	s
Mexikem	Mexiko	k1gNnSc7	Mexiko
a	a	k8xC	a
zahájil	zahájit	k5eAaPmAgMnS	zahájit
vyšetřování	vyšetřování	k1gNnSc4	vyšetřování
údajných	údajný	k2eAgInPc2d1	údajný
volebních	volební	k2eAgInPc2d1	volební
podvodů	podvod	k1gInPc2	podvod
<g/>
,	,	kIx,	,
týkajících	týkající	k2eAgFnPc2d1	týkající
se	se	k3xPyFc4	se
registrací	registrace	k1gFnPc2	registrace
voličů	volič	k1gMnPc2	volič
během	během	k7c2	během
prezidentských	prezidentský	k2eAgFnPc2d1	prezidentská
voleb	volba	k1gFnPc2	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
oficiální	oficiální	k2eAgFnSc4d1	oficiální
zahraniční	zahraniční	k2eAgFnSc4d1	zahraniční
návštěvu	návštěva	k1gFnSc4	návštěva
přijal	přijmout	k5eAaPmAgInS	přijmout
Trump	Trump	k1gInSc1	Trump
v	v	k7c6	v
Bílém	bílý	k2eAgInSc6d1	bílý
domě	dům	k1gInSc6	dům
dne	den	k1gInSc2	den
27	[number]	k4	27
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
a	a	k8xC	a
britská	britský	k2eAgFnSc1d1	britská
ministerská	ministerský	k2eAgFnSc1d1	ministerská
předsedkyně	předsedkyně	k1gFnSc1	předsedkyně
Theresa	Theresa	k1gFnSc1	Theresa
Mayová	Mayová	k1gFnSc1	Mayová
potvrdili	potvrdit	k5eAaPmAgMnP	potvrdit
na	na	k7c6	na
následné	následný	k2eAgFnSc6d1	následná
tiskové	tiskový	k2eAgFnSc6d1	tisková
konferenci	konference	k1gFnSc6	konference
výjimečné	výjimečný	k2eAgInPc4d1	výjimečný
vztahy	vztah	k1gInPc4	vztah
mezi	mezi	k7c7	mezi
svými	svůj	k3xOyFgFnPc7	svůj
zeměmi	zem	k1gFnPc7	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Již	již	k6eAd1	již
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
nástupu	nástup	k1gInSc6	nástup
do	do	k7c2	do
prezidentské	prezidentský	k2eAgFnSc2d1	prezidentská
funkce	funkce	k1gFnSc2	funkce
<g/>
,	,	kIx,	,
a	a	k8xC	a
sice	sice	k8xC	sice
31	[number]	k4	31
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
nominoval	nominovat	k5eAaBmAgMnS	nominovat
Trump	Trump	k1gMnSc1	Trump
kandidáta	kandidát	k1gMnSc2	kandidát
pro	pro	k7c4	pro
deváté	devátý	k4xOgNnSc4	devátý
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
Nejvyšším	vysoký	k2eAgInSc6d3	Nejvyšší
soudu	soud	k1gInSc6	soud
USA	USA	kA	USA
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
uprázdnilo	uprázdnit	k5eAaPmAgNnS	uprázdnit
16	[number]	k4	16
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2016	[number]	k4	2016
smrtí	smrtit	k5eAaImIp3nS	smrtit
soudce	soudce	k1gMnSc1	soudce
Antonina	Antonin	k2eAgFnSc1d1	Antonina
Scalii	Scalie	k1gFnSc3	Scalie
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
velmi	velmi	k6eAd1	velmi
důležitou	důležitý	k2eAgFnSc4d1	důležitá
pozici	pozice	k1gFnSc4	pozice
byl	být	k5eAaImAgInS	být
vybrán	vybrat	k5eAaPmNgInS	vybrat
49	[number]	k4	49
<g/>
letý	letý	k2eAgMnSc1d1	letý
konzervativní	konzervativní	k2eAgMnSc1d1	konzervativní
právník	právník	k1gMnSc1	právník
Neil	Neil	k1gMnSc1	Neil
Gorsuch	Gorsuch	k1gMnSc1	Gorsuch
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
působil	působit	k5eAaImAgMnS	působit
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
ze	z	k7c2	z
13	[number]	k4	13
federálních	federální	k2eAgInPc2d1	federální
odvolacích	odvolací	k2eAgInPc2d1	odvolací
soudů	soud	k1gInPc2	soud
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
druhá	druhý	k4xOgFnSc1	druhý
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
instance	instance	k1gFnSc1	instance
amerického	americký	k2eAgNnSc2d1	americké
soudnictví	soudnictví	k1gNnSc2	soudnictví
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc4	jeho
nominaci	nominace	k1gFnSc4	nominace
však	však	k9	však
musel	muset	k5eAaImAgInS	muset
ještě	ještě	k6eAd1	ještě
schválit	schválit	k5eAaPmF	schválit
Senát	senát	k1gInSc4	senát
USA	USA	kA	USA
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
odpor	odpor	k1gInSc4	odpor
většiny	většina	k1gFnSc2	většina
demokratických	demokratický	k2eAgMnPc2d1	demokratický
senátorů	senátor	k1gMnPc2	senátor
zprvu	zprvu	k6eAd1	zprvu
málo	málo	k6eAd1	málo
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
.	.	kIx.	.
</s>
<s>
Gorsuch	Gorsuch	k1gInSc1	Gorsuch
však	však	k9	však
byl	být	k5eAaImAgInS	být
7	[number]	k4	7
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2017	[number]	k4	2017
potvrzen	potvrzen	k2eAgInSc1d1	potvrzen
Senátem	senát	k1gInSc7	senát
USA	USA	kA	USA
poměrem	poměr	k1gInSc7	poměr
hlasů	hlas	k1gInPc2	hlas
55	[number]	k4	55
<g/>
:	:	kIx,	:
<g/>
45	[number]	k4	45
<g/>
.	.	kIx.	.
</s>
<s>
Posléze	posléze	k6eAd1	posléze
dne	den	k1gInSc2	den
10	[number]	k4	10
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2017	[number]	k4	2017
složil	složit	k5eAaPmAgMnS	složit
přísahu	přísaha	k1gFnSc4	přísaha
a	a	k8xC	a
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
jako	jako	k9	jako
soudce	soudce	k1gMnSc1	soudce
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
USA	USA	kA	USA
na	na	k7c4	na
doživotí	doživotí	k1gNnSc4	doživotí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Únor	únor	k1gInSc4	únor
====	====	k?	====
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2017	[number]	k4	2017
skončil	skončit	k5eAaPmAgInS	skončit
Trumpův	Trumpův	k2eAgInSc1d1	Trumpův
telefonický	telefonický	k2eAgInSc4d1	telefonický
rozhovor	rozhovor	k1gInSc4	rozhovor
s	s	k7c7	s
australským	australský	k2eAgMnSc7d1	australský
premiérem	premiér	k1gMnSc7	premiér
Malcolmem	Malcolm	k1gMnSc7	Malcolm
Turnbullem	Turnbull	k1gMnSc7	Turnbull
podle	podle	k7c2	podle
nelegálně	legálně	k6eNd1	legálně
vynesených	vynesený	k2eAgFnPc2d1	vynesená
informací	informace	k1gFnPc2	informace
vzájemnou	vzájemný	k2eAgFnSc7d1	vzájemná
roztržkou	roztržka	k1gFnSc7	roztržka
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
oba	dva	k4xCgMnPc1	dva
státníci	státník	k1gMnPc1	státník
neshodli	shodnout	k5eNaPmAgMnP	shodnout
na	na	k7c4	na
pokračování	pokračování	k1gNnSc4	pokračování
dohody	dohoda	k1gFnSc2	dohoda
o	o	k7c6	o
přesídlení	přesídlení	k1gNnSc6	přesídlení
asi	asi	k9	asi
1250	[number]	k4	1250
žadatelů	žadatel	k1gMnPc2	žadatel
o	o	k7c4	o
azyl	azyl	k1gInSc4	azyl
z	z	k7c2	z
australských	australský	k2eAgInPc2d1	australský
záchytných	záchytný	k2eAgInPc2d1	záchytný
táborů	tábor	k1gInPc2	tábor
na	na	k7c6	na
ostrovech	ostrov	k1gInPc6	ostrov
Nauru	Naur	k1gInSc2	Naur
a	a	k8xC	a
Papua-Nová	Papua-Nový	k2eAgFnSc1d1	Papua-Nová
Guinea	Guinea	k1gFnSc1	Guinea
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
Austrálie	Austrálie	k1gFnSc1	Austrálie
a	a	k8xC	a
USA	USA	kA	USA
uzavřely	uzavřít	k5eAaPmAgFnP	uzavřít
ještě	ještě	k9	ještě
v	v	k7c6	v
době	doba	k1gFnSc6	doba
vlády	vláda	k1gFnSc2	vláda
prezidenta	prezident	k1gMnSc2	prezident
Baracka	Baracko	k1gNnSc2	Baracko
Obamy	Obama	k1gFnSc2	Obama
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
test	test	k1gInSc4	test
íránské	íránský	k2eAgFnSc2d1	íránská
balistické	balistický	k2eAgFnSc2d1	balistická
rakety	raketa	k1gFnSc2	raketa
ze	z	k7c2	z
dne	den	k1gInSc2	den
29	[number]	k4	29
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2017	[number]	k4	2017
se	se	k3xPyFc4	se
Donald	Donald	k1gMnSc1	Donald
Trump	Trump	k1gMnSc1	Trump
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
3	[number]	k4	3
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
zemi	zem	k1gFnSc4	zem
uvalit	uvalit	k5eAaPmF	uvalit
určité	určitý	k2eAgFnPc4d1	určitá
sankce	sankce	k1gFnPc4	sankce
<g/>
.	.	kIx.	.
</s>
<s>
Íránská	íránský	k2eAgFnSc1d1	íránská
vláda	vláda	k1gFnSc1	vláda
odsoudila	odsoudit	k5eAaPmAgFnS	odsoudit
nové	nový	k2eAgFnPc4d1	nová
sankce	sankce	k1gFnPc4	sankce
jako	jako	k8xS	jako
nelegální	legální	k2eNgInSc1d1	nelegální
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
uvedla	uvést	k5eAaPmAgFnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
zavede	zavést	k5eAaPmIp3nS	zavést
právní	právní	k2eAgNnSc1d1	právní
omezení	omezení	k1gNnSc1	omezení
proti	proti	k7c3	proti
americkým	americký	k2eAgMnPc3d1	americký
jednotlivcům	jednotlivec	k1gMnPc3	jednotlivec
a	a	k8xC	a
jiným	jiný	k2eAgInPc3d1	jiný
subjektům	subjekt	k1gInPc3	subjekt
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
"	"	kIx"	"
<g/>
regionálním	regionální	k2eAgFnPc3d1	regionální
teroristickým	teroristický	k2eAgFnPc3d1	teroristická
skupinám	skupina	k1gFnPc3	skupina
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Trump	Trump	k1gInSc1	Trump
již	již	k6eAd1	již
2	[number]	k4	2
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
Twitteru	Twitter	k1gInSc6	Twitter
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
íránská	íránský	k2eAgFnSc1d1	íránská
vláda	vláda	k1gFnSc1	vláda
obdržela	obdržet	k5eAaPmAgFnS	obdržet
kvůli	kvůli	k7c3	kvůli
raketové	raketový	k2eAgFnSc3d1	raketová
zkoušce	zkouška	k1gFnSc3	zkouška
od	od	k7c2	od
USA	USA	kA	USA
oficiální	oficiální	k2eAgFnSc4d1	oficiální
výstrahu	výstraha	k1gFnSc4	výstraha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
4	[number]	k4	4
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
pozastavil	pozastavit	k5eAaPmAgMnS	pozastavit
federální	federální	k2eAgMnSc1d1	federální
soudce	soudce	k1gMnSc1	soudce
americkém	americký	k2eAgNnSc6d1	americké
Seattlu	Seattlo	k1gNnSc6	Seattlo
platnost	platnost	k1gFnSc4	platnost
výkonného	výkonný	k2eAgNnSc2d1	výkonné
nařízení	nařízení	k1gNnSc2	nařízení
prezidenta	prezident	k1gMnSc2	prezident
Trumpa	Trump	k1gMnSc2	Trump
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
zamezilo	zamezit	k5eAaPmAgNnS	zamezit
občanům	občan	k1gMnPc3	občan
vybraných	vybraný	k2eAgFnPc2d1	vybraná
sedmi	sedm	k4xCc2	sedm
muslimských	muslimský	k2eAgFnPc2d1	muslimská
zemí	zem	k1gFnPc2	zem
vstup	vstup	k1gInSc4	vstup
na	na	k7c4	na
americké	americký	k2eAgNnSc4d1	americké
území	území	k1gNnSc4	území
a	a	k8xC	a
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
problémy	problém	k1gInPc4	problém
na	na	k7c6	na
letištích	letiště	k1gNnPc6	letiště
<g/>
.	.	kIx.	.
</s>
<s>
Posléze	posléze	k6eAd1	posléze
bylo	být	k5eAaImAgNnS	být
toto	tento	k3xDgNnSc1	tento
soudní	soudní	k2eAgNnSc1d1	soudní
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
potvrzeno	potvrzen	k2eAgNnSc1d1	potvrzeno
třemi	tři	k4xCgFnPc7	tři
soudci	soudce	k1gMnPc1	soudce
odvolacího	odvolací	k2eAgInSc2d1	odvolací
soudu	soud	k1gInSc2	soud
v	v	k7c6	v
San	San	k1gFnSc6	San
Franciscu	Franciscus	k1gInSc2	Franciscus
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
dnech	den	k1gInPc6	den
nejistoty	nejistota	k1gFnSc2	nejistota
ohledně	ohledně	k7c2	ohledně
dalšího	další	k2eAgInSc2d1	další
postupu	postup	k1gInSc2	postup
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Trumpova	Trumpův	k2eAgFnSc1d1	Trumpova
administrativa	administrativa	k1gFnSc1	administrativa
tuto	tento	k3xDgFnSc4	tento
kauzu	kauza	k1gFnSc4	kauza
nepředloží	předložit	k5eNaPmIp3nS	předložit
Nejvyššímu	vysoký	k2eAgInSc3d3	Nejvyšší
soudu	soud	k1gInSc3	soud
USA	USA	kA	USA
ke	k	k7c3	k
konečnému	konečný	k2eAgNnSc3d1	konečné
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
<g/>
.	.	kIx.	.
</s>
<s>
Administrativa	administrativa	k1gFnSc1	administrativa
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
při	při	k7c6	při
stávající	stávající	k2eAgFnSc6d1	stávající
rovnosti	rovnost	k1gFnSc6	rovnost
hlasů	hlas	k1gInPc2	hlas
pro	pro	k7c4	pro
a	a	k8xC	a
proti	proti	k7c3	proti
(	(	kIx(	(
<g/>
čtyři	čtyři	k4xCgMnPc4	čtyři
proti	proti	k7c3	proti
čtyřem	čtyři	k4xCgInPc3	čtyři
<g/>
)	)	kIx)	)
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
v	v	k7c6	v
platnosti	platnost	k1gFnSc6	platnost
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
federálního	federální	k2eAgInSc2d1	federální
soudu	soud	k1gInSc2	soud
v	v	k7c6	v
Seattlu	Seattl	k1gInSc6	Seattl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c4	v
sobotu	sobota	k1gFnSc4	sobota
4	[number]	k4	4
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2017	[number]	k4	2017
Trump	Trump	k1gMnSc1	Trump
také	také	k9	také
hovořil	hovořit	k5eAaImAgMnS	hovořit
telefonicky	telefonicky	k6eAd1	telefonicky
s	s	k7c7	s
ruským	ruský	k2eAgMnSc7d1	ruský
prezidentem	prezident	k1gMnSc7	prezident
Vladimirem	Vladimir	k1gMnSc7	Vladimir
Putinem	Putin	k1gMnSc7	Putin
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
obsahu	obsah	k1gInSc6	obsah
tohoto	tento	k3xDgInSc2	tento
rozhovoru	rozhovor	k1gInSc2	rozhovor
není	být	k5eNaImIp3nS	být
mnoho	mnoho	k6eAd1	mnoho
známo	znám	k2eAgNnSc1d1	známo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
stejný	stejný	k2eAgInSc4d1	stejný
den	den	k1gInSc4	den
telefonoval	telefonovat	k5eAaImAgMnS	telefonovat
Trump	Trump	k1gInSc4	Trump
s	s	k7c7	s
německou	německý	k2eAgFnSc7d1	německá
kancléřkou	kancléřka	k1gFnSc7	kancléřka
Angelou	Angela	k1gFnSc7	Angela
Merkelovou	Merkelová	k1gFnSc7	Merkelová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
11	[number]	k4	11
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
Trump	Trump	k1gInSc4	Trump
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
výrokům	výrok	k1gInPc3	výrok
federálních	federální	k2eAgInPc2d1	federální
soudů	soud	k1gInPc2	soud
nepodřídí	podřídit	k5eNaPmIp3nS	podřídit
a	a	k8xC	a
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
možné	možný	k2eAgNnSc4d1	možné
vydání	vydání	k1gNnSc4	vydání
nového	nový	k2eAgInSc2d1	nový
dekretu	dekret	k1gInSc2	dekret
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nP	by
občanům	občan	k1gMnPc3	občan
některých	některý	k3yIgFnPc2	některý
muslimských	muslimský	k2eAgFnPc2d1	muslimská
zemí	zem	k1gFnPc2	zem
zapověděl	zapovědět	k5eAaPmAgMnS	zapovědět
vstup	vstup	k1gInSc4	vstup
na	na	k7c6	na
území	území	k1gNnSc6	území
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
15	[number]	k4	15
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
přijal	přijmout	k5eAaPmAgMnS	přijmout
Trump	Trump	k1gInSc4	Trump
v	v	k7c6	v
Bílém	bílý	k2eAgInSc6d1	bílý
domě	dům	k1gInSc6	dům
izraelského	izraelský	k2eAgMnSc2d1	izraelský
premiéra	premiér	k1gMnSc2	premiér
Benjamina	Benjamin	k1gMnSc2	Benjamin
Netanjahua	Netanjahuus	k1gMnSc2	Netanjahuus
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
setkání	setkání	k1gNnSc2	setkání
prezident	prezident	k1gMnSc1	prezident
Trump	Trump	k1gMnSc1	Trump
zopakoval	zopakovat	k5eAaPmAgMnS	zopakovat
svůj	svůj	k3xOyFgInSc4	svůj
souhlas	souhlas	k1gInSc4	souhlas
s	s	k7c7	s
přenesením	přenesení	k1gNnSc7	přenesení
amerického	americký	k2eAgNnSc2d1	americké
velvyslanectví	velvyslanectví	k1gNnSc2	velvyslanectví
v	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
z	z	k7c2	z
Tel	tel	kA	tel
Avivu	Aviva	k1gFnSc4	Aviva
do	do	k7c2	do
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
však	však	k9	však
Izrael	Izrael	k1gInSc4	Izrael
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
s	s	k7c7	s
výstavbou	výstavba	k1gFnSc7	výstavba
židovských	židovský	k2eAgFnPc2d1	židovská
osad	osada	k1gFnPc2	osada
na	na	k7c6	na
území	území	k1gNnSc6	území
Palestinské	palestinský	k2eAgFnSc2d1	palestinská
autonomie	autonomie	k1gFnSc2	autonomie
"	"	kIx"	"
<g/>
trochu	trochu	k6eAd1	trochu
držel	držet	k5eAaImAgInS	držet
zpátky	zpátky	k6eAd1	zpátky
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Naznačil	naznačit	k5eAaPmAgMnS	naznačit
také	také	k9	také
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
akceptovat	akceptovat	k5eAaBmF	akceptovat
jak	jak	k6eAd1	jak
řešení	řešení	k1gNnSc2	řešení
formou	forma	k1gFnSc7	forma
"	"	kIx"	"
<g/>
jednoho	jeden	k4xCgMnSc2	jeden
státu	stát	k1gInSc2	stát
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tak	tak	k9	tak
formou	forma	k1gFnSc7	forma
"	"	kIx"	"
<g/>
dvou	dva	k4xCgInPc2	dva
států	stát	k1gInPc2	stát
<g/>
"	"	kIx"	"
na	na	k7c6	na
území	území	k1gNnSc6	území
Izraele	Izrael	k1gInSc2	Izrael
a	a	k8xC	a
jím	on	k3xPp3gInSc7	on
okupovaných	okupovaný	k2eAgNnPc6d1	okupované
palestinských	palestinský	k2eAgNnPc6d1	palestinské
územích	území	k1gNnPc6	území
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rovněž	rovněž	k9	rovněž
15	[number]	k4	15
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
ohlásili	ohlásit	k5eAaPmAgMnP	ohlásit
Trump	Trump	k1gMnSc1	Trump
a	a	k8xC	a
právníci	právník	k1gMnPc1	právník
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
vypracováno	vypracován	k2eAgNnSc1d1	vypracováno
nové	nový	k2eAgNnSc1d1	nové
výkonné	výkonný	k2eAgNnSc1d1	výkonné
nařízení	nařízení	k1gNnSc1	nařízení
prezidenta	prezident	k1gMnSc2	prezident
<g/>
,	,	kIx,	,
kterým	který	k3yQgMnSc7	který
bude	být	k5eAaImBp3nS	být
odvoláno	odvolán	k2eAgNnSc1d1	odvolán
původní	původní	k2eAgNnSc1d1	původní
nařízení	nařízení	k1gNnSc1	nařízení
<g/>
,	,	kIx,	,
zapovídající	zapovídající	k2eAgInSc1d1	zapovídající
vstup	vstup	k1gInSc1	vstup
na	na	k7c6	na
území	území	k1gNnSc6	území
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
občanům	občan	k1gMnPc3	občan
sedmi	sedm	k4xCc2	sedm
muslimských	muslimský	k2eAgFnPc2d1	muslimská
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
bude	být	k5eAaImBp3nS	být
onen	onen	k3xDgInSc1	onen
dekret	dekret	k1gInSc1	dekret
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
pozastaven	pozastavit	k5eAaPmNgInS	pozastavit
federálními	federální	k2eAgInPc7d1	federální
soudy	soud	k1gInPc7	soud
<g/>
,	,	kIx,	,
nahrazen	nahrazen	k2eAgInSc1d1	nahrazen
"	"	kIx"	"
<g/>
o	o	k7c4	o
něco	něco	k3yInSc4	něco
úžeji	úzko	k6eAd2	úzko
formulovaným	formulovaný	k2eAgMnSc7d1	formulovaný
<g/>
"	"	kIx"	"
textem	text	k1gInSc7	text
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
bude	být	k5eAaImBp3nS	být
přizpůsoben	přizpůsoben	k2eAgInSc1d1	přizpůsoben
těmto	tento	k3xDgNnPc3	tento
"	"	kIx"	"
<g/>
špatným	špatný	k2eAgNnSc7d1	špatné
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
soudů	soud	k1gInPc2	soud
<g/>
"	"	kIx"	"
<g/>
.25	.25	k4	.25
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
odřekl	odřeknout	k5eAaPmAgMnS	odřeknout
Trump	Trump	k1gMnSc1	Trump
svou	svůj	k3xOyFgFnSc4	svůj
účast	účast	k1gFnSc4	účast
na	na	k7c4	na
každoroční	každoroční	k2eAgFnSc4d1	každoroční
slavnostní	slavnostní	k2eAgFnSc4d1	slavnostní
večeři	večeře	k1gFnSc4	večeře
korespondentů	korespondent	k1gMnPc2	korespondent
akreditovaných	akreditovaný	k2eAgMnPc2d1	akreditovaný
u	u	k7c2	u
Bílého	bílý	k2eAgInSc2d1	bílý
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
má	mít	k5eAaImIp3nS	mít
konat	konat	k5eAaImF	konat
29	[number]	k4	29
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
s	s	k7c7	s
poukazem	poukaz	k1gInSc7	poukaz
na	na	k7c4	na
nepřátelský	přátelský	k2eNgInSc4d1	nepřátelský
postoj	postoj	k1gInSc4	postoj
médií	médium	k1gNnPc2	médium
vůči	vůči	k7c3	vůči
němu	on	k3xPp3gMnSc3	on
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc3	jeho
politice	politika	k1gFnSc3	politika
<g/>
.	.	kIx.	.
</s>
<s>
Předseda	předseda	k1gMnSc1	předseda
spolku	spolek	k1gInSc2	spolek
korespondentů	korespondent	k1gMnPc2	korespondent
Jeff	Jeff	k1gMnSc1	Jeff
Mason	mason	k1gMnSc1	mason
předtím	předtím	k6eAd1	předtím
ohlásil	ohlásit	k5eAaPmAgMnS	ohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
večeře	večeře	k1gFnSc2	večeře
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
oslavována	oslavován	k2eAgFnSc1d1	oslavována
"	"	kIx"	"
<g/>
role	role	k1gFnSc1	role
nezávislých	závislý	k2eNgNnPc2d1	nezávislé
zpravodajských	zpravodajský	k2eAgNnPc2d1	zpravodajské
médií	médium	k1gNnPc2	médium
ve	v	k7c6	v
zdravé	zdravý	k2eAgFnSc6d1	zdravá
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Po	po	k7c6	po
Trumpově	Trumpův	k2eAgNnSc6d1	Trumpovo
odřeknutí	odřeknutí	k1gNnSc6	odřeknutí
účasti	účast	k1gFnSc2	účast
se	se	k3xPyFc4	se
spolek	spolek	k1gInSc1	spolek
korespondentů	korespondent	k1gMnPc2	korespondent
proti	proti	k7c3	proti
Trumpovu	Trumpův	k2eAgNnSc3d1	Trumpovo
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
ostře	ostro	k6eAd1	ostro
ohradil	ohradit	k5eAaPmAgMnS	ohradit
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2017	[number]	k4	2017
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
prezident	prezident	k1gMnSc1	prezident
Trump	Trump	k1gMnSc1	Trump
poprvé	poprvé	k6eAd1	poprvé
před	před	k7c7	před
oběma	dva	k4xCgFnPc7	dva
komorami	komora	k1gFnPc7	komora
Kongresu	kongres	k1gInSc2	kongres
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Přednesl	přednést	k5eAaPmAgMnS	přednést
projev	projev	k1gInSc4	projev
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
nastínil	nastínit	k5eAaPmAgMnS	nastínit
problémy	problém	k1gInPc4	problém
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yRgInPc7	který
se	se	k3xPyFc4	se
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
potýkají	potýkat	k5eAaImIp3nP	potýkat
<g/>
,	,	kIx,	,
a	a	k8xC	a
svou	svůj	k3xOyFgFnSc4	svůj
politickou	politický	k2eAgFnSc4d1	politická
linii	linie	k1gFnSc4	linie
<g/>
.	.	kIx.	.
</s>
<s>
Vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
republikány	republikán	k1gMnPc4	republikán
a	a	k8xC	a
demokraty	demokrat	k1gMnPc4	demokrat
k	k	k7c3	k
vzájemné	vzájemný	k2eAgFnSc3d1	vzájemná
spolupráci	spolupráce	k1gFnSc3	spolupráce
při	při	k7c6	při
řešení	řešení	k1gNnSc6	řešení
problémů	problém	k1gInPc2	problém
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Zopakoval	zopakovat	k5eAaPmAgMnS	zopakovat
svůj	svůj	k3xOyFgInSc4	svůj
záměr	záměr	k1gInSc4	záměr
podpořit	podpořit	k5eAaPmF	podpořit
vznik	vznik	k1gInSc4	vznik
mnoha	mnoho	k4c2	mnoho
nových	nový	k2eAgNnPc2d1	nové
pracovních	pracovní	k2eAgNnPc2d1	pracovní
míst	místo	k1gNnPc2	místo
a	a	k8xC	a
zásadním	zásadní	k2eAgInSc7d1	zásadní
způsobem	způsob	k1gInSc7	způsob
zlepšit	zlepšit	k5eAaPmF	zlepšit
infrastrukturu	infrastruktura	k1gFnSc4	infrastruktura
na	na	k7c6	na
území	území	k1gNnSc6	území
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
na	na	k7c6	na
mnoha	mnoho	k4c2	mnoho
místech	místo	k1gNnPc6	místo
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
dobrém	dobrý	k2eAgInSc6d1	dobrý
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
Trval	trvat	k5eAaImAgInS	trvat
na	na	k7c6	na
nutnosti	nutnost	k1gFnSc6	nutnost
přísné	přísný	k2eAgFnSc2d1	přísná
imigrační	imigrační	k2eAgFnSc2d1	imigrační
politiky	politika	k1gFnSc2	politika
a	a	k8xC	a
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
souvislosti	souvislost	k1gFnSc6	souvislost
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
plán	plán	k1gInSc4	plán
postavit	postavit	k5eAaPmF	postavit
další	další	k2eAgInPc4d1	další
úseky	úsek	k1gInPc4	úsek
zdi	zeď	k1gFnSc2	zeď
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
s	s	k7c7	s
Mexikem	Mexiko	k1gNnSc7	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
Ohlásil	ohlásit	k5eAaPmAgInS	ohlásit
daňovou	daňový	k2eAgFnSc4d1	daňová
reformu	reforma	k1gFnSc4	reforma
<g/>
,	,	kIx,	,
neuvedl	uvést	k5eNaPmAgMnS	uvést
ale	ale	k8xC	ale
zatím	zatím	k6eAd1	zatím
žádné	žádný	k3yNgInPc4	žádný
detaily	detail	k1gInPc4	detail
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
výdajů	výdaj	k1gInPc2	výdaj
na	na	k7c4	na
obranu	obrana	k1gFnSc4	obrana
má	mít	k5eAaImIp3nS	mít
dojít	dojít	k5eAaPmF	dojít
ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
ročního	roční	k2eAgInSc2d1	roční
rozpočtu	rozpočet	k1gInSc2	rozpočet
o	o	k7c4	o
54	[number]	k4	54
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Březen	březen	k1gInSc1	březen
a	a	k8xC	a
duben	duben	k1gInSc1	duben
====	====	k?	====
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2017	[number]	k4	2017
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
již	již	k6eAd1	již
zjevná	zjevný	k2eAgFnSc1d1	zjevná
roztržka	roztržka	k1gFnSc1	roztržka
mezi	mezi	k7c7	mezi
prezidentem	prezident	k1gMnSc7	prezident
Donaldem	Donald	k1gMnSc7	Donald
Trumpem	Trump	k1gMnSc7	Trump
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc7	jeho
předchůdcem	předchůdce	k1gMnSc7	předchůdce
Barackem	Baracko	k1gNnSc7	Baracko
Obamou	Obama	k1gFnSc7	Obama
nových	nový	k2eAgInPc2d1	nový
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
.	.	kIx.	.
</s>
<s>
Trump	Trump	k1gMnSc1	Trump
obvinil	obvinit	k5eAaPmAgMnS	obvinit
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
zpráv	zpráva	k1gFnPc2	zpráva
na	na	k7c6	na
Twitteru	Twitter	k1gInSc6	Twitter
Obamu	Obam	k1gInSc2	Obam
<g/>
,	,	kIx,	,
že	že	k8xS	že
během	během	k7c2	během
volebního	volební	k2eAgInSc2d1	volební
boje	boj	k1gInSc2	boj
nebo	nebo	k8xC	nebo
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
nechal	nechat	k5eAaPmAgMnS	nechat
odposlouchávat	odposlouchávat	k5eAaImF	odposlouchávat
jeho	jeho	k3xOp3gInPc4	jeho
telefony	telefon	k1gInPc4	telefon
<g/>
.	.	kIx.	.
</s>
<s>
Zprvu	zprvu	k6eAd1	zprvu
však	však	k9	však
nepředložil	předložit	k5eNaPmAgInS	předložit
pro	pro	k7c4	pro
toto	tento	k3xDgNnSc4	tento
tvrzení	tvrzení	k1gNnSc4	tvrzení
žádné	žádný	k3yNgInPc4	žádný
důkazy	důkaz	k1gInPc4	důkaz
<g/>
.	.	kIx.	.
</s>
<s>
Trump	Trump	k1gMnSc1	Trump
dále	daleko	k6eAd2	daleko
označil	označit	k5eAaPmAgMnS	označit
Obamu	Obama	k1gFnSc4	Obama
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
zlého	zlý	k2eAgMnSc2d1	zlý
nebo	nebo	k8xC	nebo
nemocného	mocný	k2eNgMnSc2d1	nemocný
chlapíka	chlapík	k1gMnSc2	chlapík
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Informaci	informace	k1gFnSc4	informace
o	o	k7c6	o
Trumpově	Trumpův	k2eAgInSc6d1	Trumpův
útoku	útok	k1gInSc6	útok
na	na	k7c6	na
Obamu	Obam	k1gInSc6	Obam
přinesly	přinést	k5eAaPmAgFnP	přinést
také	také	k9	také
Lidovky	Lidovky	k1gFnPc1	Lidovky
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgInSc2	ten
bylo	být	k5eAaImAgNnS	být
sděleno	sdělit	k5eAaPmNgNnS	sdělit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Trump	Trump	k1gMnSc1	Trump
vyčetl	vyčíst	k5eAaPmAgMnS	vyčíst
manželům	manžel	k1gMnPc3	manžel
Obamovým	Obamová	k1gFnPc3	Obamová
časté	častý	k2eAgFnSc2d1	častá
návštěvy	návštěva	k1gFnSc2	návštěva
ruského	ruský	k2eAgMnSc2d1	ruský
velvyslance	velvyslanec	k1gMnSc2	velvyslanec
v	v	k7c6	v
USA	USA	kA	USA
Sergeje	Sergej	k1gMnSc2	Sergej
Kisljaka	Kisljak	k1gMnSc2	Kisljak
v	v	k7c6	v
Bílém	bílý	k2eAgInSc6d1	bílý
domě	dům	k1gInSc6	dům
<g/>
.	.	kIx.	.
</s>
<s>
Trump	Trump	k1gMnSc1	Trump
uvedl	uvést	k5eAaPmAgMnS	uvést
22	[number]	k4	22
Kisljakových	Kisljakový	k2eAgFnPc2d1	Kisljakový
návštěv	návštěva	k1gFnPc2	návštěva
za	za	k7c4	za
dobu	doba	k1gFnSc4	doba
Obamovy	Obamův	k2eAgFnSc2d1	Obamova
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
čtyři	čtyři	k4xCgInPc4	čtyři
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Odposlech	odposlech	k1gInSc1	odposlech
Trumpa	Trump	k1gMnSc2	Trump
nebo	nebo	k8xC	nebo
členů	člen	k1gMnPc2	člen
jeho	on	k3xPp3gInSc2	on
týmu	tým	k1gInSc2	tým
byl	být	k5eAaImAgInS	být
poté	poté	k6eAd1	poté
zpochybněn	zpochybnit	k5eAaPmNgInS	zpochybnit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dne	den	k1gInSc2	den
22	[number]	k4	22
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2017	[number]	k4	2017
potvrzen	potvrzen	k2eAgInSc1d1	potvrzen
Devinem	Devin	k1gMnSc7	Devin
Nunesem	Nunes	k1gMnSc7	Nunes
<g/>
,	,	kIx,	,
republikánským	republikánský	k2eAgMnSc7d1	republikánský
předsedou	předseda	k1gMnSc7	předseda
vyšetřovacího	vyšetřovací	k2eAgInSc2d1	vyšetřovací
výboru	výbor	k1gInSc2	výbor
Sněmovny	sněmovna	k1gFnSc2	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jeho	jeho	k3xOp3gNnPc2	jeho
vyjádření	vyjádření	k1gNnPc2	vyjádření
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
dělo	dít	k5eAaImAgNnS	dít
po	po	k7c6	po
Trumpově	Trumpův	k2eAgNnSc6d1	Trumpovo
zvolení	zvolení	k1gNnSc6	zvolení
prezidentem	prezident	k1gMnSc7	prezident
<g/>
,	,	kIx,	,
a	a	k8xC	a
sice	sice	k8xC	sice
v	v	k7c6	v
měsících	měsíc	k1gInPc6	měsíc
listopadu	listopad	k1gInSc2	listopad
a	a	k8xC	a
prosinci	prosinec	k1gInSc6	prosinec
2016	[number]	k4	2016
a	a	k8xC	a
lednu	leden	k1gInSc3	leden
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
Trump	Trump	k1gMnSc1	Trump
sestavoval	sestavovat	k5eAaImAgMnS	sestavovat
svou	svůj	k3xOyFgFnSc4	svůj
vládu	vláda	k1gFnSc4	vláda
<g/>
,	,	kIx,	,
hovořil	hovořit	k5eAaImAgMnS	hovořit
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
spolupracovníky	spolupracovník	k1gMnPc7	spolupracovník
o	o	k7c6	o
svých	svůj	k3xOyFgInPc6	svůj
plánech	plán	k1gInPc6	plán
a	a	k8xC	a
telefonoval	telefonovat	k5eAaImAgInS	telefonovat
se	s	k7c7	s
zahraničními	zahraniční	k2eAgMnPc7d1	zahraniční
státníky	státník	k1gMnPc7	státník
<g/>
.	.	kIx.	.
</s>
<s>
Akce	akce	k1gFnSc1	akce
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
podle	podle	k7c2	podle
Nunese	Nunese	k1gFnSc2	Nunese
legální	legální	k2eAgFnSc2d1	legální
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
Nunese	Nunese	k1gFnSc2	Nunese
znepokojilo	znepokojit	k5eAaPmAgNnS	znepokojit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
písemných	písemný	k2eAgFnPc6d1	písemná
zprávách	zpráva	k1gFnPc6	zpráva
tajných	tajný	k2eAgFnPc2d1	tajná
služeb	služba	k1gFnPc2	služba
o	o	k7c6	o
odposlechu	odposlech	k1gInSc6	odposlech
jsou	být	k5eAaImIp3nP	být
přímo	přímo	k6eAd1	přímo
uváděna	uváděn	k2eAgNnPc1d1	uváděno
jména	jméno	k1gNnPc1	jméno
členů	člen	k1gMnPc2	člen
Trumpova	Trumpův	k2eAgInSc2d1	Trumpův
týmu	tým	k1gInSc2	tým
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
by	by	kYmCp3nS	by
legální	legální	k2eAgMnSc1d1	legální
nebylo	být	k5eNaImAgNnS	být
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2017	[number]	k4	2017
navštívila	navštívit	k5eAaPmAgFnS	navštívit
prezidenta	prezident	k1gMnSc4	prezident
Trumpa	Trump	k1gMnSc4	Trump
v	v	k7c6	v
Bílém	bílý	k2eAgInSc6d1	bílý
domě	dům	k1gInSc6	dům
německá	německý	k2eAgFnSc1d1	německá
kancléřka	kancléřka	k1gFnSc1	kancléřka
Angela	Angela	k1gFnSc1	Angela
Merkelová	Merkelová	k1gFnSc1	Merkelová
s	s	k7c7	s
početným	početný	k2eAgInSc7d1	početný
doprovodem	doprovod	k1gInSc7	doprovod
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
jednali	jednat	k5eAaImAgMnP	jednat
oba	dva	k4xCgMnPc1	dva
státníci	státník	k1gMnPc1	státník
mezi	mezi	k7c7	mezi
čtyřma	čtyři	k4xCgNnPc7	čtyři
očima	oko	k1gNnPc7	oko
v	v	k7c6	v
Oválné	oválný	k2eAgFnSc6d1	oválná
kanceláři	kancelář	k1gFnSc6	kancelář
a	a	k8xC	a
poté	poté	k6eAd1	poté
ve	v	k7c6	v
velkém	velký	k2eAgInSc6d1	velký
kruhu	kruh	k1gInSc6	kruh
účastníků	účastník	k1gMnPc2	účastník
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
hlavně	hlavně	k9	hlavně
o	o	k7c6	o
vzájemných	vzájemný	k2eAgInPc6d1	vzájemný
obchodních	obchodní	k2eAgInPc6d1	obchodní
stycích	styk	k1gInPc6	styk
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
byla	být	k5eAaImAgFnS	být
uspořádána	uspořádán	k2eAgFnSc1d1	uspořádána
tisková	tiskový	k2eAgFnSc1d1	tisková
konference	konference	k1gFnSc1	konference
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
navštívena	navštívit	k5eAaPmNgFnS	navštívit
velkým	velký	k2eAgInSc7d1	velký
počtem	počet	k1gInSc7	počet
zástupců	zástupce	k1gMnPc2	zástupce
médií	médium	k1gNnPc2	médium
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Den	den	k1gInSc1	den
poté	poté	k6eAd1	poté
uveřejnil	uveřejnit	k5eAaPmAgInS	uveřejnit
Trump	Trump	k1gInSc1	Trump
na	na	k7c6	na
Twitteru	Twitter	k1gInSc6	Twitter
tento	tento	k3xDgInSc4	tento
text	text	k1gInSc4	text
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Německo	Německo	k1gNnSc1	Německo
dluží	dlužit	k5eAaImIp3nS	dlužit
NATO	nato	k6eAd1	nato
obrovské	obrovský	k2eAgFnPc4d1	obrovská
sumy	suma	k1gFnPc4	suma
<g/>
,	,	kIx,	,
a	a	k8xC	a
Spojeným	spojený	k2eAgInPc3d1	spojený
státům	stát	k1gInPc3	stát
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
lépe	dobře	k6eAd2	dobře
platit	platit	k5eAaImF	platit
za	za	k7c4	za
silnou	silný	k2eAgFnSc4d1	silná
a	a	k8xC	a
nákladnou	nákladný	k2eAgFnSc4d1	nákladná
obranu	obrana	k1gFnSc4	obrana
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
Německu	Německo	k1gNnSc6	Německo
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
Jako	jako	k9	jako
odvetu	odveta	k1gFnSc4	odveta
za	za	k7c4	za
chemický	chemický	k2eAgInSc4d1	chemický
útok	útok	k1gInSc4	útok
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zabil	zabít	k5eAaPmAgMnS	zabít
desítky	desítka	k1gFnPc4	desítka
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
vyslal	vyslat	k5eAaPmAgMnS	vyslat
Trump	Trump	k1gMnSc1	Trump
7	[number]	k4	7
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2017	[number]	k4	2017
na	na	k7c4	na
leteckou	letecký	k2eAgFnSc4d1	letecká
základnu	základna	k1gFnSc4	základna
syrských	syrský	k2eAgFnPc2d1	Syrská
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
59	[number]	k4	59
střel	střela	k1gFnPc2	střela
řízeného	řízený	k2eAgInSc2d1	řízený
doletu	dolet	k1gInSc2	dolet
Tomahawk	tomahawk	k1gInSc1	tomahawk
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
útoku	útok	k1gInSc6	útok
bezprostředně	bezprostředně	k6eAd1	bezprostředně
informoval	informovat	k5eAaBmAgMnS	informovat
čínského	čínský	k2eAgMnSc4d1	čínský
prezidenta	prezident	k1gMnSc4	prezident
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
právě	právě	k6eAd1	právě
hostil	hostit	k5eAaImAgMnS	hostit
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
resortu	resort	k1gInSc6	resort
Mar-a-Lago	Mar-Lago	k6eAd1	Mar-a-Lago
na	na	k7c6	na
Floridě	Florida	k1gFnSc6	Florida
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Červenec	červenec	k1gInSc4	červenec
a	a	k8xC	a
srpen	srpen	k1gInSc4	srpen
====	====	k?	====
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
5	[number]	k4	5
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2017	[number]	k4	2017
přiletěl	přiletět	k5eAaPmAgInS	přiletět
Trump	Trump	k1gInSc1	Trump
na	na	k7c4	na
krátkou	krátký	k2eAgFnSc4d1	krátká
státní	státní	k2eAgFnSc4d1	státní
návštěvu	návštěva	k1gFnSc4	návštěva
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
je	být	k5eAaImIp3nS	být
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
účastí	účast	k1gFnSc7	účast
na	na	k7c4	na
zasedání	zasedání	k1gNnSc4	zasedání
uskupení	uskupení	k1gNnSc2	uskupení
nejdůležitějších	důležitý	k2eAgFnPc2d3	nejdůležitější
zemí	zem	k1gFnPc2	zem
světa	svět	k1gInSc2	svět
G20	G20	k1gFnSc2	G20
v	v	k7c6	v
německém	německý	k2eAgInSc6d1	německý
Hamburku	Hamburk	k1gInSc6	Hamburk
od	od	k7c2	od
7	[number]	k4	7
<g/>
.	.	kIx.	.
do	do	k7c2	do
8	[number]	k4	8
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
.	.	kIx.	.
</s>
<s>
Doprovází	doprovázet	k5eAaImIp3nS	doprovázet
jej	on	k3xPp3gMnSc4	on
mj.	mj.	kA	mj.
manželka	manželka	k1gFnSc1	manželka
Melanie	Melanie	k1gFnSc1	Melanie
Trumpová	Trumpová	k1gFnSc1	Trumpová
a	a	k8xC	a
dcera	dcera	k1gFnSc1	dcera
Ivanka	Ivanka	k1gFnSc1	Ivanka
Trumpová	Trumpová	k1gFnSc1	Trumpová
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Varšavě	Varšava	k1gFnSc6	Varšava
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
představiteli	představitel	k1gMnPc7	představitel
12	[number]	k4	12
zemí	zem	k1gFnPc2	zem
střední	střední	k2eAgFnSc2d1	střední
a	a	k8xC	a
východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
spojily	spojit	k5eAaPmAgFnP	spojit
do	do	k7c2	do
Iniciativy	iniciativa	k1gFnSc2	iniciativa
Trojmoří	Trojmoří	k1gNnSc4	Trojmoří
(	(	kIx(	(
<g/>
Baltické	baltický	k2eAgNnSc4d1	Baltické
<g/>
,	,	kIx,	,
Jaderské	jaderský	k2eAgNnSc4d1	Jaderské
a	a	k8xC	a
Černé	Černé	k2eAgNnSc4d1	Černé
moře	moře	k1gNnSc4	moře
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
zasedání	zasedání	k1gNnSc1	zasedání
tohoto	tento	k3xDgNnSc2	tento
uskupení	uskupení	k1gNnSc2	uskupení
se	se	k3xPyFc4	se
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
v	v	k7c6	v
Chorvatsku	Chorvatsko	k1gNnSc6	Chorvatsko
<g/>
.	.	kIx.	.
</s>
<s>
Následujícího	následující	k2eAgInSc2d1	následující
dne	den	k1gInSc2	den
bylo	být	k5eAaImAgNnS	být
dopoledne	dopoledne	k6eAd1	dopoledne
na	na	k7c6	na
pořadu	pořad	k1gInSc6	pořad
setkání	setkání	k1gNnSc2	setkání
s	s	k7c7	s
polským	polský	k2eAgMnSc7d1	polský
prezidentem	prezident	k1gMnSc7	prezident
Andrzejem	Andrzej	k1gMnSc7	Andrzej
Dudou	Duda	k1gMnSc7	Duda
<g/>
.	.	kIx.	.
</s>
<s>
Odpoledne	odpoledne	k6eAd1	odpoledne
promluvil	promluvit	k5eAaPmAgMnS	promluvit
Trump	Trump	k1gInSc4	Trump
před	před	k7c7	před
shromážděním	shromáždění	k1gNnSc7	shromáždění
polských	polský	k2eAgMnPc2d1	polský
představitelů	představitel	k1gMnPc2	představitel
a	a	k8xC	a
obyvatel	obyvatel	k1gMnPc2	obyvatel
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
u	u	k7c2	u
pomníku	pomník	k1gInSc2	pomník
bojovníků	bojovník	k1gMnPc2	bojovník
varšavského	varšavský	k2eAgNnSc2d1	Varšavské
povstání	povstání	k1gNnSc2	povstání
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
krvavě	krvavě	k6eAd1	krvavě
potlačeno	potlačit	k5eAaPmNgNnS	potlačit
německým	německý	k2eAgInSc7d1	německý
Wehrmachtem	wehrmacht	k1gInSc7	wehrmacht
<g/>
.	.	kIx.	.
</s>
<s>
Trump	Trump	k1gMnSc1	Trump
obšírně	obšírně	k6eAd1	obšírně
ocenil	ocenit	k5eAaPmAgMnS	ocenit
tehdejší	tehdejší	k2eAgNnSc4d1	tehdejší
hrdinství	hrdinství	k1gNnSc4	hrdinství
Poláků	Polák	k1gMnPc2	Polák
i	i	k8xC	i
jejich	jejich	k3xOp3gInSc4	jejich
dlouhodobý	dlouhodobý	k2eAgInSc4d1	dlouhodobý
boj	boj	k1gInSc4	boj
za	za	k7c4	za
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Přihlásil	přihlásit	k5eAaPmAgMnS	přihlásit
se	se	k3xPyFc4	se
k	k	k7c3	k
ustanovení	ustanovení	k1gNnSc3	ustanovení
článku	článek	k1gInSc2	článek
5	[number]	k4	5
smlouvy	smlouva	k1gFnSc2	smlouva
o	o	k7c4	o
NATO	NATO	kA	NATO
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zaručuje	zaručovat	k5eAaImIp3nS	zaručovat
vzájemnost	vzájemnost	k1gFnSc4	vzájemnost
členů	člen	k1gMnPc2	člen
aliance	aliance	k1gFnSc2	aliance
při	při	k7c6	při
obraně	obrana	k1gFnSc6	obrana
před	před	k7c7	před
útokem	útok	k1gInSc7	útok
zvenčí	zvenčí	k6eAd1	zvenčí
<g/>
.	.	kIx.	.
</s>
<s>
Okrajově	okrajově	k6eAd1	okrajově
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
Rusko	Rusko	k1gNnSc4	Rusko
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
přestalo	přestat	k5eAaPmAgNnS	přestat
"	"	kIx"	"
<g/>
destabilizovat	destabilizovat	k5eAaBmF	destabilizovat
situaci	situace	k1gFnSc4	situace
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
projevu	projev	k1gInSc6	projev
přicestoval	přicestovat	k5eAaPmAgMnS	přicestovat
Trump	Trump	k1gInSc4	Trump
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
doprovodem	doprovod	k1gInSc7	doprovod
do	do	k7c2	do
Hamburku	Hamburk	k1gInSc2	Hamburk
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
summitu	summit	k1gInSc6	summit
G20	G20	k1gFnSc2	G20
v	v	k7c6	v
Hamburku	Hamburk	k1gInSc6	Hamburk
se	se	k3xPyFc4	se
Trump	Trump	k1gMnSc1	Trump
poprvé	poprvé	k6eAd1	poprvé
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
ruským	ruský	k2eAgMnSc7d1	ruský
prezidentem	prezident	k1gMnSc7	prezident
Vladimirem	Vladimir	k1gMnSc7	Vladimir
Putinem	Putin	k1gMnSc7	Putin
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2017	[number]	k4	2017
schválil	schválit	k5eAaPmAgInS	schválit
senát	senát	k1gInSc1	senát
USA	USA	kA	USA
Christophera	Christophero	k1gNnSc2	Christophero
A.	A.	kA	A.
Wraye	Wray	k1gMnSc4	Wray
jako	jako	k8xS	jako
nového	nový	k2eAgMnSc4d1	nový
ředitele	ředitel	k1gMnSc4	ředitel
Federálního	federální	k2eAgInSc2d1	federální
úřadu	úřad	k1gInSc2	úřad
pro	pro	k7c4	pro
vyšetřování	vyšetřování	k1gNnSc4	vyšetřování
(	(	kIx(	(
<g/>
FBI	FBI	kA	FBI
<g/>
)	)	kIx)	)
po	po	k7c6	po
odvolání	odvolání	k1gNnSc6	odvolání
Jamese	Jamese	k1gFnSc2	Jamese
Comeyho	Comey	k1gMnSc2	Comey
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
vysokým	vysoký	k2eAgInSc7d1	vysoký
poměrem	poměr	k1gInSc7	poměr
hlasů	hlas	k1gInPc2	hlas
92	[number]	k4	92
:	:	kIx,	:
5	[number]	k4	5
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tuto	tento	k3xDgFnSc4	tento
nominaci	nominace	k1gFnSc4	nominace
prezidenta	prezident	k1gMnSc2	prezident
Trumpa	Trump	k1gMnSc2	Trump
podpořila	podpořit	k5eAaPmAgFnS	podpořit
také	také	k6eAd1	také
velká	velký	k2eAgFnSc1d1	velká
většina	většina	k1gFnSc1	většina
senátorů	senátor	k1gMnPc2	senátor
Demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
<g/>
Trump	Trump	k1gMnSc1	Trump
podpořil	podpořit	k5eAaPmAgMnS	podpořit
návrh	návrh	k1gInSc4	návrh
předložený	předložený	k2eAgInSc4d1	předložený
republikánskými	republikánský	k2eAgMnPc7d1	republikánský
senátory	senátor	k1gMnPc7	senátor
Tomem	Tom	k1gMnSc7	Tom
Cottonem	Cotton	k1gInSc7	Cotton
a	a	k8xC	a
Davidem	David	k1gMnSc7	David
Perduem	Perdu	k1gMnSc7	Perdu
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
snížil	snížit	k5eAaPmAgInS	snížit
legální	legální	k2eAgFnSc4d1	legální
imigraci	imigrace	k1gFnSc4	imigrace
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
nových	nový	k2eAgMnPc2d1	nový
držitelů	držitel	k1gMnPc2	držitel
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
zelené	zelený	k2eAgFnPc1d1	zelená
karty	karta	k1gFnPc1	karta
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
povolení	povolení	k1gNnSc4	povolení
k	k	k7c3	k
pobytu	pobyt	k1gInSc3	pobyt
a	a	k8xC	a
k	k	k7c3	k
zaměstnání	zaměstnání	k1gNnSc3	zaměstnání
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
snížil	snížit	k5eAaPmAgInS	snížit
z	z	k7c2	z
asi	asi	k9	asi
půldruhého	půldruhý	k2eAgInSc2d1	půldruhý
milionu	milion	k4xCgInSc2	milion
na	na	k7c4	na
půl	půl	k1xP	půl
milionu	milion	k4xCgInSc2	milion
osob	osoba	k1gFnPc2	osoba
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Demokratičtí	demokratický	k2eAgMnPc1d1	demokratický
zákonodárci	zákonodárce	k1gMnPc1	zákonodárce
chtějí	chtít	k5eAaImIp3nP	chtít
tento	tento	k3xDgInSc4	tento
návrh	návrh	k1gInSc4	návrh
zablokovat	zablokovat	k5eAaPmF	zablokovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
by	by	kYmCp3nS	by
poškodil	poškodit	k5eAaPmAgInS	poškodit
především	především	k6eAd1	především
imigranty	imigrant	k1gMnPc7	imigrant
z	z	k7c2	z
neevropských	evropský	k2eNgFnPc2d1	neevropská
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
od	od	k7c2	od
přijetí	přijetí	k1gNnSc2	přijetí
Immigration	Immigration	k1gInSc1	Immigration
and	and	k?	and
Nationality	Nationalita	k1gFnSc2	Nationalita
Act	Act	k1gFnSc2	Act
of	of	k?	of
1965	[number]	k4	1965
převládají	převládat	k5eAaImIp3nP	převládat
mezi	mezi	k7c7	mezi
legálními	legální	k2eAgMnPc7d1	legální
imigranty	imigrant	k1gMnPc7	imigrant
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Listopad	listopad	k1gInSc1	listopad
a	a	k8xC	a
prosinec	prosinec	k1gInSc1	prosinec
====	====	k?	====
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
6	[number]	k4	6
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2017	[number]	k4	2017
vydal	vydat	k5eAaPmAgMnS	vydat
prezident	prezident	k1gMnSc1	prezident
Trump	Trump	k1gMnSc1	Trump
prohlášení	prohlášení	k1gNnSc4	prohlášení
<g/>
,	,	kIx,	,
kterým	který	k3yIgNnSc7	který
definitivně	definitivně	k6eAd1	definitivně
uznal	uznat	k5eAaPmAgInS	uznat
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
za	za	k7c4	za
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
státu	stát	k1gInSc2	stát
Izrael	Izrael	k1gInSc1	Izrael
<g/>
.	.	kIx.	.
</s>
<s>
USA	USA	kA	USA
jsou	být	k5eAaImIp3nP	být
první	první	k4xOgFnPc1	první
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
tak	tak	k6eAd1	tak
učinila	učinit	k5eAaImAgFnS	učinit
<g/>
.	.	kIx.	.
</s>
<s>
De	De	k?	De
facto	facto	k1gNnSc1	facto
se	se	k3xPyFc4	se
Trumpovo	Trumpův	k2eAgNnSc1d1	Trumpovo
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
zakládá	zakládat	k5eAaImIp3nS	zakládat
na	na	k7c6	na
zákonu	zákon	k1gInSc6	zákon
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
Kongresem	kongres	k1gInSc7	kongres
USA	USA	kA	USA
schválen	schválit	k5eAaPmNgInS	schválit
již	již	k6eAd1	již
před	před	k7c7	před
22	[number]	k4	22
lety	léto	k1gNnPc7	léto
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
účinnost	účinnost	k1gFnSc1	účinnost
však	však	k9	však
byla	být	k5eAaImAgFnS	být
americkými	americký	k2eAgMnPc7d1	americký
prezidenty	prezident	k1gMnPc7	prezident
doposud	doposud	k6eAd1	doposud
v	v	k7c6	v
půlročních	půlroční	k2eAgInPc6d1	půlroční
intervalech	interval	k1gInPc6	interval
odkládána	odkládat	k5eAaImNgFnS	odkládat
<g/>
.	.	kIx.	.
</s>
<s>
Trump	Trump	k1gInSc1	Trump
zároveň	zároveň	k6eAd1	zároveň
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
americké	americký	k2eAgNnSc1d1	americké
velvyslanectví	velvyslanectví	k1gNnSc1	velvyslanectví
v	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
bude	být	k5eAaImBp3nS	být
po	po	k7c6	po
potřebných	potřebný	k2eAgFnPc6d1	potřebná
přípravách	příprava	k1gFnPc6	příprava
přemístěno	přemístěn	k2eAgNnSc1d1	přemístěno
z	z	k7c2	z
Tel	tel	kA	tel
Avivu	Aviva	k1gFnSc4	Aviva
do	do	k7c2	do
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
<g/>
.	.	kIx.	.
</s>
<s>
Trumpovo	Trumpův	k2eAgNnSc1d1	Trumpovo
prohlášení	prohlášení	k1gNnSc1	prohlášení
bylo	být	k5eAaImAgNnS	být
odsouzeno	odsouzet	k5eAaImNgNnS	odsouzet
právně	právně	k6eAd1	právně
nezávaznou	závazný	k2eNgFnSc7d1	nezávazná
rezolucí	rezoluce	k1gFnSc7	rezoluce
Valného	valný	k2eAgNnSc2d1	Valné
shromáždění	shromáždění	k1gNnSc2	shromáždění
OSN	OSN	kA	OSN
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2017	[number]	k4	2017
podpořilo	podpořit	k5eAaPmAgNnS	podpořit
128	[number]	k4	128
členských	členský	k2eAgFnPc2d1	členská
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
i	i	k9	i
Slovensko	Slovensko	k1gNnSc1	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Devět	devět	k4xCc1	devět
států	stát	k1gInPc2	stát
hlasovalo	hlasovat	k5eAaImAgNnS	hlasovat
proti	proti	k7c3	proti
této	tento	k3xDgFnSc3	tento
rezoluci	rezoluce	k1gFnSc3	rezoluce
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
USA	USA	kA	USA
a	a	k8xC	a
Izrael	Izrael	k1gInSc1	Izrael
<g/>
.	.	kIx.	.
35	[number]	k4	35
států	stát	k1gInPc2	stát
se	se	k3xPyFc4	se
zdrželo	zdržet	k5eAaPmAgNnS	zdržet
hlasování	hlasování	k1gNnSc1	hlasování
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
šest	šest	k4xCc1	šest
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
EU	EU	kA	EU
(	(	kIx(	(
<g/>
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
<g/>
,	,	kIx,	,
Lotyšsko	Lotyšsko	k1gNnSc1	Lotyšsko
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
a	a	k8xC	a
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
<g/>
)	)	kIx)	)
<g/>
.22	.22	k4	.22
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
podepsal	podepsat	k5eAaPmAgMnS	podepsat
Trump	Trump	k1gMnSc1	Trump
zákon	zákon	k1gInSc4	zákon
o	o	k7c6	o
rozsáhlé	rozsáhlý	k2eAgFnSc6d1	rozsáhlá
daňové	daňový	k2eAgFnSc6d1	daňová
reformě	reforma	k1gFnSc6	reforma
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
dob	doba	k1gFnPc2	doba
Ronalda	Ronald	k1gMnSc2	Ronald
Reagana	Reagan	k1gMnSc2	Reagan
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
prezidentem	prezident	k1gMnSc7	prezident
USA	USA	kA	USA
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
nebyla	být	k5eNaImAgFnS	být
tak	tak	k6eAd1	tak
velká	velký	k2eAgFnSc1d1	velká
změna	změna	k1gFnSc1	změna
daňových	daňový	k2eAgFnPc2d1	daňová
podmínek	podmínka	k1gFnPc2	podmínka
pro	pro	k7c4	pro
soukromé	soukromý	k2eAgFnPc4d1	soukromá
osoby	osoba	k1gFnPc4	osoba
a	a	k8xC	a
firmy	firma	k1gFnSc2	firma
schválena	schválit	k5eAaPmNgFnS	schválit
<g/>
.	.	kIx.	.
</s>
<s>
Daně	daň	k1gFnPc1	daň
z	z	k7c2	z
příjmu	příjem	k1gInSc2	příjem
právnických	právnický	k2eAgFnPc2d1	právnická
osob	osoba	k1gFnPc2	osoba
se	se	k3xPyFc4	se
snižují	snižovat	k5eAaImIp3nP	snižovat
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2018	[number]	k4	2018
z	z	k7c2	z
nynějších	nynější	k2eAgInPc2d1	nynější
35	[number]	k4	35
%	%	kIx~	%
na	na	k7c4	na
21	[number]	k4	21
%	%	kIx~	%
a	a	k8xC	a
zlepšují	zlepšovat	k5eAaImIp3nP	zlepšovat
se	se	k3xPyFc4	se
podmínky	podmínka	k1gFnPc1	podmínka
pro	pro	k7c4	pro
odpisy	odpis	k1gInPc4	odpis
investičních	investiční	k2eAgInPc2d1	investiční
nákladů	náklad	k1gInPc2	náklad
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
má	mít	k5eAaImIp3nS	mít
vést	vést	k5eAaImF	vést
k	k	k7c3	k
oživení	oživení	k1gNnSc3	oživení
investic	investice	k1gFnPc2	investice
domácích	domácí	k2eAgFnPc2d1	domácí
společností	společnost	k1gFnPc2	společnost
<g/>
,	,	kIx,	,
návratu	návrat	k1gInSc2	návrat
amerických	americký	k2eAgFnPc2d1	americká
firem	firma	k1gFnPc2	firma
ze	z	k7c2	z
zahraničí	zahraničí	k1gNnSc2	zahraničí
a	a	k8xC	a
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
k	k	k7c3	k
přilákání	přilákání	k1gNnSc3	přilákání
nových	nový	k2eAgFnPc2d1	nová
investic	investice	k1gFnPc2	investice
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
do	do	k7c2	do
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
jednotlivcům	jednotlivec	k1gMnPc3	jednotlivec
a	a	k8xC	a
zvláště	zvláště	k6eAd1	zvláště
rodinám	rodina	k1gFnPc3	rodina
s	s	k7c7	s
dětmi	dítě	k1gFnPc7	dítě
se	s	k7c7	s
středními	střední	k2eAgInPc7d1	střední
až	až	k8xS	až
vysokými	vysoký	k2eAgInPc7d1	vysoký
příjmy	příjem	k1gInPc7	příjem
přinese	přinést	k5eAaPmIp3nS	přinést
reforma	reforma	k1gFnSc1	reforma
úspory	úspora	k1gFnPc4	úspora
na	na	k7c6	na
dani	daň	k1gFnSc6	daň
z	z	k7c2	z
příjmu	příjem	k1gInSc2	příjem
<g/>
.	.	kIx.	.
</s>
<s>
Trumpovu	Trumpův	k2eAgInSc3d1	Trumpův
podpisu	podpis	k1gInSc3	podpis
zákona	zákon	k1gInSc2	zákon
předcházela	předcházet	k5eAaImAgNnP	předcházet
složitá	složitý	k2eAgNnPc1d1	složité
vyjednávání	vyjednávání	k1gNnPc1	vyjednávání
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
Kongresu	kongres	k1gInSc6	kongres
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
musely	muset	k5eAaImAgFnP	muset
být	být	k5eAaImF	být
mj.	mj.	kA	mj.
sladěny	sladěn	k2eAgInPc1d1	sladěn
texty	text	k1gInPc1	text
zákona	zákon	k1gInSc2	zákon
schválené	schválený	k2eAgNnSc4d1	schválené
Sněmovnou	sněmovna	k1gFnSc7	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
a	a	k8xC	a
Senátem	senát	k1gInSc7	senát
USA	USA	kA	USA
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Senátu	senát	k1gInSc6	senát
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
jeho	on	k3xPp3gNnSc2	on
znění	znění	k1gNnSc2	znění
zákona	zákon	k1gInSc2	zákon
postupně	postupně	k6eAd1	postupně
zapracován	zapracován	k2eAgInSc1d1	zapracován
značný	značný	k2eAgInSc1d1	značný
počet	počet	k1gInSc1	počet
pozměňovacích	pozměňovací	k2eAgInPc2d1	pozměňovací
návrhů	návrh	k1gInPc2	návrh
jednotlivých	jednotlivý	k2eAgMnPc2d1	jednotlivý
republikánských	republikánský	k2eAgMnPc2d1	republikánský
senátorů	senátor	k1gMnPc2	senátor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obou	dva	k4xCgFnPc6	dva
komorách	komora	k1gFnPc6	komora
Kongresu	kongres	k1gInSc2	kongres
kladli	klást	k5eAaImAgMnP	klást
zástupci	zástupce	k1gMnPc1	zástupce
Demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
daňové	daňový	k2eAgFnSc2d1	daňová
reformě	reforma	k1gFnSc3	reforma
tuhý	tuhý	k2eAgInSc4d1	tuhý
odpor	odpor	k1gInSc4	odpor
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
však	však	k9	však
přehlasováni	přehlasován	k2eAgMnPc1d1	přehlasován
republikánskými	republikánský	k2eAgFnPc7d1	republikánská
většinami	většina	k1gFnPc7	většina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
2018	[number]	k4	2018
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Leden	leden	k1gInSc1	leden
až	až	k8xS	až
březen	březen	k1gInSc1	březen
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2018	[number]	k4	2018
se	se	k3xPyFc4	se
Trump	Trump	k1gMnSc1	Trump
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc7	první
prezidentem	prezident	k1gMnSc7	prezident
USA	USA	kA	USA
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
promluvil	promluvit	k5eAaPmAgMnS	promluvit
k	k	k7c3	k
účastníkům	účastník	k1gMnPc3	účastník
washingtonského	washingtonský	k2eAgInSc2d1	washingtonský
Pochodu	pochod	k1gInSc2	pochod
pro	pro	k7c4	pro
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
téže	týž	k3xTgFnSc6	týž
době	doba	k1gFnSc6	doba
jeho	jeho	k3xOp3gFnSc1	jeho
vláda	vláda	k1gFnSc1	vláda
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pracuje	pracovat	k5eAaImIp3nS	pracovat
na	na	k7c4	na
zvýšení	zvýšení	k1gNnSc4	zvýšení
ochrany	ochrana	k1gFnSc2	ochrana
práva	právo	k1gNnSc2	právo
na	na	k7c4	na
výhradu	výhrada	k1gFnSc4	výhrada
svědomí	svědomí	k1gNnSc2	svědomí
u	u	k7c2	u
pracovníků	pracovník	k1gMnPc2	pracovník
ve	v	k7c6	v
zdravotnictví	zdravotnictví	k1gNnSc6	zdravotnictví
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
13	[number]	k4	13
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2018	[number]	k4	2018
oznámil	oznámit	k5eAaPmAgMnS	oznámit
Trump	Trump	k1gMnSc1	Trump
na	na	k7c6	na
Twitteru	Twitter	k1gInSc6	Twitter
<g/>
,	,	kIx,	,
že	že	k8xS	že
propustil	propustit	k5eAaPmAgMnS	propustit
z	z	k7c2	z
úřadu	úřad	k1gInSc2	úřad
ministra	ministr	k1gMnSc2	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Rexe	Rex	k1gMnSc2	Rex
Tillersona	Tillerson	k1gMnSc2	Tillerson
a	a	k8xC	a
za	za	k7c2	za
jeho	on	k3xPp3gNnSc2	on
nástupce	nástupce	k1gMnSc1	nástupce
nominoval	nominovat	k5eAaBmAgMnS	nominovat
dosavadního	dosavadní	k2eAgMnSc4d1	dosavadní
ředitele	ředitel	k1gMnSc4	ředitel
zpravodajské	zpravodajský	k2eAgFnSc2d1	zpravodajská
služby	služba	k1gFnSc2	služba
CIA	CIA	kA	CIA
Mikea	Mikea	k1gMnSc1	Mikea
Pompea	Pompea	k1gMnSc1	Pompea
<g/>
.	.	kIx.	.
</s>
<s>
Pompeo	Pompeo	k6eAd1	Pompeo
musí	muset	k5eAaImIp3nS	muset
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
funkci	funkce	k1gFnSc6	funkce
ještě	ještě	k6eAd1	ještě
být	být	k5eAaImF	být
potvrzen	potvrdit	k5eAaPmNgInS	potvrdit
Senátem	senát	k1gInSc7	senát
USA	USA	kA	USA
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
schválení	schválení	k1gNnSc4	schválení
je	být	k5eAaImIp3nS	být
však	však	k9	však
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
jisté	jistý	k2eAgNnSc4d1	jisté
<g/>
.	.	kIx.	.
</s>
<s>
Nástupkyní	nástupkyně	k1gFnSc7	nástupkyně
Pompea	Pompe	k1gInSc2	Pompe
ve	v	k7c6	v
vedení	vedení	k1gNnSc6	vedení
CIA	CIA	kA	CIA
bude	být	k5eAaImBp3nS	být
Gina	Gina	k1gFnSc1	Gina
Haspel	Haspela	k1gFnPc2	Haspela
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
první	první	k4xOgFnSc1	první
žena	žena	k1gFnSc1	žena
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
všech	všecek	k3xTgFnPc2	všecek
amerických	americký	k2eAgFnPc2d1	americká
zpravodajských	zpravodajský	k2eAgFnPc2d1	zpravodajská
služeb	služba	k1gFnPc2	služba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
22	[number]	k4	22
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2018	[number]	k4	2018
propustil	propustit	k5eAaPmAgInS	propustit
Trump	Trump	k1gInSc1	Trump
dosavadního	dosavadní	k2eAgMnSc2d1	dosavadní
poradce	poradce	k1gMnSc2	poradce
pro	pro	k7c4	pro
národní	národní	k2eAgFnSc4d1	národní
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
<g/>
,	,	kIx,	,
tříhvězdičkového	tříhvězdičkový	k2eAgMnSc2d1	tříhvězdičkový
generála	generál	k1gMnSc2	generál
H.	H.	kA	H.
R.	R.	kA	R.
McMastera	McMaster	k1gMnSc2	McMaster
<g/>
,	,	kIx,	,
a	a	k8xC	a
nahradil	nahradit	k5eAaPmAgInS	nahradit
jej	on	k3xPp3gMnSc4	on
bývalým	bývalý	k2eAgMnSc7d1	bývalý
velvyslancem	velvyslanec	k1gMnSc7	velvyslanec
USA	USA	kA	USA
u	u	k7c2	u
OSN	OSN	kA	OSN
Johnem	John	k1gMnSc7	John
Boltonem	Bolton	k1gInSc7	Bolton
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgInSc1d1	známý
jako	jako	k8xS	jako
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
jestřáb	jestřáb	k1gMnSc1	jestřáb
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
hawk	hawk	k1gInSc1	hawk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
svým	svůj	k3xOyFgInSc7	svůj
tvrdým	tvrdý	k2eAgInSc7d1	tvrdý
postojem	postoj	k1gInSc7	postoj
ohledně	ohledně	k7c2	ohledně
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
jmenování	jmenování	k1gNnSc4	jmenování
nepotřebuje	potřebovat	k5eNaImIp3nS	potřebovat
Trump	Trump	k1gMnSc1	Trump
souhlas	souhlas	k1gInSc4	souhlas
Senátu	senát	k1gInSc2	senát
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Senátoři	senátor	k1gMnPc1	senátor
za	za	k7c4	za
Republikánskou	republikánský	k2eAgFnSc4d1	republikánská
stranu	strana	k1gFnSc4	strana
Lindsay	Lindsaa	k1gFnSc2	Lindsaa
Graham	graham	k1gInSc1	graham
a	a	k8xC	a
Marco	Marco	k6eAd1	Marco
Rubio	Rubio	k6eAd1	Rubio
Boltonovo	Boltonův	k2eAgNnSc4d1	Boltonův
jmenování	jmenování	k1gNnSc4	jmenování
přivítali	přivítat	k5eAaPmAgMnP	přivítat
<g/>
,	,	kIx,	,
demokratický	demokratický	k2eAgMnSc1d1	demokratický
senátor	senátor	k1gMnSc1	senátor
Edward	Edward	k1gMnSc1	Edward
J.	J.	kA	J.
Markey	Markey	k1gInPc4	Markey
je	být	k5eAaImIp3nS	být
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
<g/>
.	.	kIx.	.
</s>
<s>
Německý	německý	k2eAgInSc1d1	německý
list	list	k1gInSc1	list
Frankfurter	Frankfurtra	k1gFnPc2	Frankfurtra
Allgemeine	Allgemein	k1gInSc5	Allgemein
Zeitung	Zeitung	k1gInSc1	Zeitung
dále	daleko	k6eAd2	daleko
cituje	citovat	k5eAaBmIp3nS	citovat
noviny	novina	k1gFnSc2	novina
The	The	k1gMnPc2	The
Washington	Washington	k1gInSc1	Washington
Post	posta	k1gFnPc2	posta
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterých	který	k3yRgFnPc2	který
může	moct	k5eAaImIp3nS	moct
Boltonovo	Boltonův	k2eAgNnSc1d1	Boltonův
působení	působení	k1gNnSc1	působení
jako	jako	k8xC	jako
poradce	poradce	k1gMnSc1	poradce
pro	pro	k7c4	pro
národní	národní	k2eAgFnSc4d1	národní
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
vést	vést	k5eAaImF	vést
k	k	k7c3	k
"	"	kIx"	"
<g/>
dramatickým	dramatický	k2eAgFnPc3d1	dramatická
změnám	změna	k1gFnPc3	změna
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
vlády	vláda	k1gFnSc2	vláda
na	na	k7c4	na
krize	krize	k1gFnPc4	krize
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
amerického	americký	k2eAgInSc2d1	americký
deníku	deník	k1gInSc2	deník
sleduje	sledovat	k5eAaImIp3nS	sledovat
Trump	Trump	k1gInSc1	Trump
jmenováním	jmenování	k1gNnSc7	jmenování
Boltona	Bolton	k1gMnSc2	Bolton
především	především	k9	především
ukázku	ukázka	k1gFnSc4	ukázka
svého	svůj	k3xOyFgNnSc2	svůj
odhodlání	odhodlání	k1gNnSc2	odhodlání
<g/>
,	,	kIx,	,
nenechat	nechat	k5eNaPmF	nechat
se	se	k3xPyFc4	se
"	"	kIx"	"
<g/>
zarámovat	zarámovat	k5eAaPmF	zarámovat
<g/>
"	"	kIx"	"
umírněnými	umírněný	k2eAgMnPc7d1	umírněný
politiky	politik	k1gMnPc7	politik
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
administraci	administrace	k1gFnSc6	administrace
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
naopak	naopak	k6eAd1	naopak
v	v	k7c6	v
úmyslu	úmysl	k1gInSc6	úmysl
přestavět	přestavět	k5eAaPmF	přestavět
svůj	svůj	k3xOyFgInSc4	svůj
tým	tým	k1gInSc4	tým
podle	podle	k7c2	podle
svých	svůj	k3xOyFgNnPc2	svůj
přání	přání	k1gNnPc2	přání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Duben	duben	k1gInSc1	duben
až	až	k8xS	až
červen	červen	k1gInSc1	červen
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
20	[number]	k4	20
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
uveřejnil	uveřejnit	k5eAaPmAgMnS	uveřejnit
prezident	prezident	k1gMnSc1	prezident
Trump	Trump	k1gMnSc1	Trump
na	na	k7c6	na
Twitteru	Twitter	k1gInSc6	Twitter
svůj	svůj	k3xOyFgInSc4	svůj
požadavek	požadavek	k1gInSc4	požadavek
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
USA	USA	kA	USA
zabývalo	zabývat	k5eAaImAgNnS	zabývat
otázkou	otázka	k1gFnSc7	otázka
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
tajná	tajný	k2eAgFnSc1d1	tajná
služba	služba	k1gFnSc1	služba
FBI	FBI	kA	FBI
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
v	v	k7c6	v
době	doba	k1gFnSc6	doba
jeho	jeho	k3xOp3gFnSc2	jeho
prezidentské	prezidentský	k2eAgFnSc2d1	prezidentská
kampaně	kampaň	k1gFnSc2	kampaň
<g/>
,	,	kIx,	,
z	z	k7c2	z
politických	politický	k2eAgInPc2d1	politický
důvodů	důvod	k1gInPc2	důvod
infiltrovala	infiltrovat	k5eAaBmAgFnS	infiltrovat
jeho	jeho	k3xOp3gInSc4	jeho
volební	volební	k2eAgInSc4d1	volební
tým	tým	k1gInSc4	tým
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
večer	večer	k6eAd1	večer
oznámilo	oznámit	k5eAaPmAgNnS	oznámit
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
pověřilo	pověřit	k5eAaPmAgNnS	pověřit
svého	svůj	k3xOyFgMnSc4	svůj
generálního	generální	k2eAgMnSc4d1	generální
inspektora	inspektor	k1gMnSc4	inspektor
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
toto	tento	k3xDgNnSc4	tento
podezření	podezření	k1gNnSc4	podezření
objasnil	objasnit	k5eAaPmAgMnS	objasnit
<g/>
.	.	kIx.	.
</s>
<s>
Trump	Trump	k1gInSc1	Trump
podezírá	podezírat	k5eAaImIp3nS	podezírat
vládu	vláda	k1gFnSc4	vláda
prezidenta	prezident	k1gMnSc2	prezident
Baracka	Baracko	k1gNnSc2	Baracko
Obamy	Obama	k1gFnSc2	Obama
<g/>
,	,	kIx,	,
že	že	k8xS	že
dosadila	dosadit	k5eAaPmAgFnS	dosadit
"	"	kIx"	"
<g/>
špiona	špion	k1gMnSc2	špion
<g/>
"	"	kIx"	"
do	do	k7c2	do
jeho	jeho	k3xOp3gInSc2	jeho
volebního	volební	k2eAgInSc2d1	volební
týmu	tým	k1gInSc2	tým
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
poškodila	poškodit	k5eAaPmAgFnS	poškodit
jeho	jeho	k3xOp3gFnSc4	jeho
kandidaturu	kandidatura	k1gFnSc4	kandidatura
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
<g/>
,	,	kIx,	,
a	a	k8xC	a
označuje	označovat	k5eAaImIp3nS	označovat
tuto	tento	k3xDgFnSc4	tento
akci	akce	k1gFnSc4	akce
za	za	k7c4	za
"	"	kIx"	"
<g/>
největší	veliký	k2eAgInSc1d3	veliký
politický	politický	k2eAgInSc1d1	politický
skandál	skandál	k1gInSc1	skandál
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Americká	americký	k2eAgNnPc4d1	americké
média	médium	k1gNnPc4	médium
informovala	informovat	k5eAaBmAgFnS	informovat
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
že	že	k8xS	že
informátorem	informátor	k1gMnSc7	informátor
FBI	FBI	kA	FBI
v	v	k7c6	v
Trumpově	Trumpův	k2eAgInSc6d1	Trumpův
volebním	volební	k2eAgInSc6d1	volební
štábu	štáb	k1gInSc6	štáb
byl	být	k5eAaImAgMnS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
agent	agent	k1gMnSc1	agent
CIA	CIA	kA	CIA
Stefan	Stefan	k1gMnSc1	Stefan
Halper	Halper	k1gMnSc1	Halper
<g/>
.	.	kIx.	.
<g/>
Nový	nový	k2eAgMnSc1d1	nový
advokát	advokát	k1gMnSc1	advokát
prezidenta	prezident	k1gMnSc2	prezident
Trumpa	Trump	k1gMnSc2	Trump
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
newyorský	newyorský	k2eAgMnSc1d1	newyorský
starosta	starosta	k1gMnSc1	starosta
Rudy	Rudy	k1gMnSc1	Rudy
Giuliani	Giulian	k1gMnPc1	Giulian
<g/>
,	,	kIx,	,
oznámil	oznámit	k5eAaPmAgMnS	oznámit
rovněž	rovněž	k9	rovněž
20	[number]	k4	20
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
,	,	kIx,	,
že	že	k8xS	že
zvláštní	zvláštní	k2eAgMnSc1d1	zvláštní
státní	státní	k2eAgMnSc1d1	státní
zástupce	zástupce	k1gMnSc1	zástupce
Robert	Robert	k1gMnSc1	Robert
Mueller	Mueller	k1gMnSc1	Mueller
dal	dát	k5eAaPmAgMnS	dát
najevo	najevo	k6eAd1	najevo
svůj	svůj	k3xOyFgInSc4	svůj
úmysl	úmysl	k1gInSc4	úmysl
ukončit	ukončit	k5eAaPmF	ukončit
vyšetřování	vyšetřování	k1gNnSc4	vyšetřování
"	"	kIx"	"
<g/>
ruských	ruský	k2eAgInPc2d1	ruský
kontaktů	kontakt	k1gInPc2	kontakt
<g/>
"	"	kIx"	"
Trumpova	Trumpův	k2eAgInSc2d1	Trumpův
týmu	tým	k1gInSc2	tým
do	do	k7c2	do
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
průtahy	průtah	k1gInPc1	průtah
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
vyšetřování	vyšetřování	k1gNnSc6	vyšetřování
by	by	kYmCp3nS	by
jinak	jinak	k6eAd1	jinak
mohly	moct	k5eAaImAgInP	moct
nežádoucím	žádoucí	k2eNgInSc7d1	nežádoucí
způsobem	způsob	k1gInSc7	způsob
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
volby	volba	k1gFnPc4	volba
do	do	k7c2	do
Kongresu	kongres	k1gInSc2	kongres
USA	USA	kA	USA
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
budou	být	k5eAaImBp3nP	být
konat	konat	k5eAaImF	konat
6	[number]	k4	6
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
<g/>
Deník	deník	k1gInSc1	deník
New	New	k1gFnPc2	New
York	York	k1gInSc1	York
Times	Times	k1gMnSc1	Times
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
informoval	informovat	k5eAaBmAgMnS	informovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejstarší	starý	k2eAgMnSc1d3	nejstarší
syn	syn	k1gMnSc1	syn
Donald	Donald	k1gMnSc1	Donald
Trump	Trump	k1gMnSc1	Trump
Jr	Jr	k1gMnSc1	Jr
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
sešel	sejít	k5eAaPmAgInS	sejít
3	[number]	k4	3
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2016	[number]	k4	2016
na	na	k7c6	na
tajné	tajný	k2eAgFnSc6d1	tajná
schůzce	schůzka	k1gFnSc6	schůzka
v	v	k7c4	v
Trump	Trump	k1gInSc4	Trump
Tower	Towero	k1gNnPc2	Towero
s	s	k7c7	s
Georgem	Georg	k1gInSc7	Georg
Naderem	Naderem	k?	Naderem
<g/>
,	,	kIx,	,
emisarem	emisar	k1gMnSc7	emisar
korunních	korunní	k2eAgMnPc2d1	korunní
princů	princ	k1gMnPc2	princ
Saúdské	saúdský	k2eAgFnSc2d1	Saúdská
Arábie	Arábie	k1gFnSc2	Arábie
a	a	k8xC	a
Spojených	spojený	k2eAgInPc2d1	spojený
arabských	arabský	k2eAgInPc2d1	arabský
emirátů	emirát	k1gInPc2	emirát
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
nabídli	nabídnout	k5eAaPmAgMnP	nabídnout
pomoc	pomoc	k1gFnSc4	pomoc
Trumpově	Trumpův	k2eAgFnSc3d1	Trumpova
volební	volební	k2eAgFnSc3d1	volební
kampani	kampaň	k1gFnSc3	kampaň
<g/>
.	.	kIx.	.
</s>
<s>
Libanonsko-americký	libanonskomerický	k2eAgMnSc1d1	libanonsko-americký
lobbista	lobbista	k1gMnSc1	lobbista
George	George	k1gFnSc1	George
Nader	nadrat	k5eAaBmRp2nS	nadrat
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
politickým	politický	k2eAgMnSc7d1	politický
poradcem	poradce	k1gMnSc7	poradce
SAE	SAE	kA	SAE
<g/>
,	,	kIx,	,
poukázal	poukázat	k5eAaPmAgMnS	poukázat
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2017	[number]	k4	2017
jednomu	jeden	k4xCgMnSc3	jeden
z	z	k7c2	z
významných	významný	k2eAgMnPc2d1	významný
Trumpových	Trumpův	k2eAgMnPc2d1	Trumpův
sponzorů	sponzor	k1gMnPc2	sponzor
2,5	[number]	k4	2,5
miliónu	milión	k4xCgInSc2	milión
dolarů	dolar	k1gInPc2	dolar
a	a	k8xC	a
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
přes	přes	k7c4	přes
něj	on	k3xPp3gNnSc4	on
ovlivňovat	ovlivňovat	k5eAaImF	ovlivňovat
Trumpovu	Trumpův	k2eAgFnSc4d1	Trumpova
administrativu	administrativa	k1gFnSc4	administrativa
k	k	k7c3	k
tvrdšímu	tvrdý	k2eAgInSc3d2	tvrdší
postoji	postoj	k1gInSc3	postoj
vůči	vůči	k7c3	vůči
Íránu	Írán	k1gInSc3	Írán
a	a	k8xC	a
Kataru	katar	k1gInSc3	katar
během	během	k7c2	během
katarské	katarský	k2eAgFnSc2d1	katarská
diplomatické	diplomatický	k2eAgFnSc2d1	diplomatická
krize	krize	k1gFnSc2	krize
a	a	k8xC	a
také	také	k9	také
k	k	k7c3	k
vypovězení	vypovězení	k1gNnSc3	vypovězení
jaderné	jaderný	k2eAgFnSc2d1	jaderná
dohody	dohoda	k1gFnSc2	dohoda
s	s	k7c7	s
Íránem	Írán	k1gInSc7	Írán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tři	tři	k4xCgFnPc1	tři
hodiny	hodina	k1gFnPc1	hodina
před	před	k7c7	před
ukončením	ukončení	k1gNnSc7	ukončení
setkání	setkání	k1gNnSc2	setkání
G	G	kA	G
7	[number]	k4	7
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
odletěl	odletět	k5eAaPmAgMnS	odletět
Trump	Trump	k1gInSc4	Trump
do	do	k7c2	do
Singapuru	Singapur	k1gInSc2	Singapur
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
paluby	paluba	k1gFnSc2	paluba
letadla	letadlo	k1gNnSc2	letadlo
Air	Air	k1gFnSc1	Air
Force	force	k1gFnSc1	force
1	[number]	k4	1
odeslal	odeslat	k5eAaPmAgMnS	odeslat
dva	dva	k4xCgInPc4	dva
tvíty	tvít	k1gInPc4	tvít
<g/>
,	,	kIx,	,
kterými	který	k3yQgInPc7	který
odvolal	odvolat	k5eAaPmAgInS	odvolat
svůj	svůj	k3xOyFgInSc4	svůj
souhlas	souhlas	k1gInSc4	souhlas
se	s	k7c7	s
závěrečným	závěrečný	k2eAgNnSc7d1	závěrečné
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
téměř	téměř	k6eAd1	téměř
bezobsažným	bezobsažný	k2eAgNnSc7d1	bezobsažné
prohlášením	prohlášení	k1gNnSc7	prohlášení
tohoto	tento	k3xDgNnSc2	tento
setkání	setkání	k1gNnSc2	setkání
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
ostře	ostro	k6eAd1	ostro
zkritizoval	zkritizovat	k5eAaPmAgMnS	zkritizovat
vystoupení	vystoupení	k1gNnSc4	vystoupení
kanadského	kanadský	k2eAgMnSc2d1	kanadský
premiéra	premiér	k1gMnSc2	premiér
Justina	Justin	k1gMnSc2	Justin
Trudeaua	Trudeauus	k1gMnSc2	Trudeauus
na	na	k7c6	na
tiskové	tiskový	k2eAgFnSc6d1	tisková
konferenci	konference	k1gFnSc6	konference
po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
odletu	odlet	k1gInSc6	odlet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
12	[number]	k4	12
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2018	[number]	k4	2018
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
singapurský	singapurský	k2eAgInSc1d1	singapurský
summit	summit	k1gInSc1	summit
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
Trump	Trump	k1gMnSc1	Trump
setkal	setkat	k5eAaPmAgMnS	setkat
se	s	k7c7	s
severokorejským	severokorejský	k2eAgMnSc7d1	severokorejský
diktátorem	diktátor	k1gMnSc7	diktátor
Kimem	Kim	k1gMnSc7	Kim
Čong-unem	Čongn	k1gMnSc7	Čong-un
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
politikové	politik	k1gMnPc1	politik
dospěli	dochvít	k5eAaPmAgMnP	dochvít
k	k	k7c3	k
závěrům	závěr	k1gInPc3	závěr
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgInP	moct
dalekosáhle	dalekosáhle	k6eAd1	dalekosáhle
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
situaci	situace	k1gFnSc4	situace
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Koreji	Korea	k1gFnSc6	Korea
<g/>
.	.	kIx.	.
</s>
<s>
USA	USA	kA	USA
budou	být	k5eAaImBp3nP	být
dohlížet	dohlížet	k5eAaImF	dohlížet
na	na	k7c6	na
plnění	plnění	k1gNnSc6	plnění
úmluvy	úmluva	k1gFnSc2	úmluva
o	o	k7c6	o
postupném	postupný	k2eAgNnSc6d1	postupné
atomárním	atomární	k2eAgNnSc6d1	atomární
a	a	k8xC	a
raketovém	raketový	k2eAgNnSc6d1	raketové
odzbrojení	odzbrojení	k1gNnSc6	odzbrojení
KLDR	KLDR	kA	KLDR
<g/>
.	.	kIx.	.
</s>
<s>
Samy	sám	k3xTgInPc1	sám
chtějí	chtít	k5eAaImIp3nP	chtít
eventuálně	eventuálně	k6eAd1	eventuálně
zastavit	zastavit	k5eAaPmF	zastavit
společné	společný	k2eAgInPc4d1	společný
manévry	manévr	k1gInPc4	manévr
vojsk	vojsko	k1gNnPc2	vojsko
USA	USA	kA	USA
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc2d1	jižní
Koreje	Korea	k1gFnSc2	Korea
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
dosavadní	dosavadní	k2eAgInSc1d1	dosavadní
stav	stav	k1gInSc1	stav
pouhého	pouhý	k2eAgNnSc2d1	pouhé
příměří	příměří	k1gNnSc2	příměří
na	na	k7c6	na
korejském	korejský	k2eAgInSc6d1	korejský
poloostrově	poloostrov	k1gInSc6	poloostrov
bude	být	k5eAaImBp3nS	být
upraven	upravit	k5eAaPmNgInS	upravit
mírovou	mírový	k2eAgFnSc7d1	mírová
smlouvou	smlouva	k1gFnSc7	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Sankce	sankce	k1gFnPc1	sankce
uvalené	uvalený	k2eAgFnPc1d1	uvalená
Radou	rada	k1gFnSc7	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
OSN	OSN	kA	OSN
vůči	vůči	k7c3	vůči
KLDR	KLDR	kA	KLDR
se	s	k7c7	s
souhlasem	souhlas	k1gInSc7	souhlas
všech	všecek	k3xTgInPc2	všecek
jejích	její	k3xOp3gNnPc2	její
pěti	pět	k4xCc2	pět
stálých	stálý	k2eAgMnPc2d1	stálý
členů	člen	k1gMnPc2	člen
však	však	k9	však
zůstanou	zůstat	k5eAaPmIp3nP	zůstat
v	v	k7c6	v
platnosti	platnost	k1gFnSc6	platnost
tak	tak	k6eAd1	tak
dlouho	dlouho	k6eAd1	dlouho
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
tato	tento	k3xDgFnSc1	tento
země	země	k1gFnSc1	země
nesplní	splnit	k5eNaPmIp3nS	splnit
všechna	všechen	k3xTgNnPc4	všechen
očekávání	očekávání	k1gNnPc4	očekávání
<g/>
.	.	kIx.	.
</s>
<s>
KLDR	KLDR	kA	KLDR
by	by	kYmCp3nS	by
ráda	rád	k2eAgFnSc1d1	ráda
navázala	navázat	k5eAaPmAgFnS	navázat
diplomatické	diplomatický	k2eAgInPc4d1	diplomatický
styky	styk	k1gInPc4	styk
s	s	k7c7	s
USA	USA	kA	USA
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
vyloučeno	vyloučit	k5eAaPmNgNnS	vyloučit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Trump	Trump	k1gMnSc1	Trump
pozve	pozvat	k5eAaPmIp3nS	pozvat
Kima	Kimum	k1gNnPc4	Kimum
na	na	k7c4	na
návštěvu	návštěva	k1gFnSc4	návštěva
Washingtonu	Washington	k1gInSc2	Washington
a	a	k8xC	a
že	že	k8xS	že
sám	sám	k3xTgMnSc1	sám
poletí	letět	k5eAaImIp3nS	letět
do	do	k7c2	do
Pchjongjangu	Pchjongjang	k1gInSc2	Pchjongjang
<g/>
.	.	kIx.	.
<g/>
Setkání	setkání	k1gNnSc1	setkání
s	s	k7c7	s
Kimem	Kim	k1gInSc7	Kim
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc7	jeho
výsledky	výsledek	k1gInPc7	výsledek
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Trumpova	Trumpův	k2eAgFnSc1d1	Trumpova
oblíbenost	oblíbenost	k1gFnSc1	oblíbenost
u	u	k7c2	u
republikánských	republikánský	k2eAgMnPc2d1	republikánský
voličů	volič	k1gMnPc2	volič
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
na	na	k7c4	na
dosud	dosud	k6eAd1	dosud
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
úroveň	úroveň	k1gFnSc4	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
televizní	televizní	k2eAgFnSc2d1	televizní
stanice	stanice	k1gFnSc2	stanice
CNN	CNN	kA	CNN
má	mít	k5eAaImIp3nS	mít
Trump	Trump	k1gInSc4	Trump
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2018	[number]	k4	2018
podporu	podpor	k1gInSc2	podpor
85	[number]	k4	85
%	%	kIx~	%
republikánů	republikán	k1gMnPc2	republikán
a	a	k8xC	a
40	[number]	k4	40
%	%	kIx~	%
všech	všecek	k3xTgMnPc2	všecek
Američanů	Američan	k1gMnPc2	Američan
<g/>
.	.	kIx.	.
</s>
<s>
Trumpovo	Trumpův	k2eAgNnSc1d1	Trumpovo
silné	silný	k2eAgNnSc1d1	silné
postavení	postavení	k1gNnSc1	postavení
v	v	k7c6	v
Republikánské	republikánský	k2eAgFnSc6d1	republikánská
straně	strana	k1gFnSc6	strana
se	se	k3xPyFc4	se
projevilo	projevit	k5eAaPmAgNnS	projevit
také	také	k9	také
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gMnSc1	jeho
vnitrostranický	vnitrostranický	k2eAgMnSc1d1	vnitrostranický
kritik	kritik	k1gMnSc1	kritik
Mark	Mark	k1gMnSc1	Mark
Sanford	Sanford	k1gMnSc1	Sanford
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
guvernér	guvernér	k1gMnSc1	guvernér
státu	stát	k1gInSc2	stát
Jižní	jižní	k2eAgNnPc1d1	jižní
Karolina	Karolinum	k1gNnPc1	Karolinum
a	a	k8xC	a
eventuální	eventuální	k2eAgMnSc1d1	eventuální
kandidát	kandidát	k1gMnSc1	kandidát
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2020	[number]	k4	2020
<g/>
,	,	kIx,	,
prohrál	prohrát	k5eAaPmAgInS	prohrát
právě	právě	k9	právě
v	v	k7c4	v
den	den	k1gInSc4	den
singapurského	singapurský	k2eAgInSc2d1	singapurský
summitu	summit	k1gInSc2	summit
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
12	[number]	k4	12
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
,	,	kIx,	,
v	v	k7c6	v
republikánském	republikánský	k2eAgNnSc6d1	republikánské
hlasování	hlasování	k1gNnSc6	hlasování
o	o	k7c6	o
kandidatuře	kandidatura	k1gFnSc6	kandidatura
do	do	k7c2	do
Sněmovny	sněmovna	k1gFnSc2	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Vítězkou	vítězka	k1gFnSc7	vítězka
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Trumpova	Trumpův	k2eAgFnSc1d1	Trumpova
stoupenkyně	stoupenkyně	k1gFnSc1	stoupenkyně
Kattie	Kattie	k1gFnSc2	Kattie
Arrington	Arrington	k1gInSc1	Arrington
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
prezident	prezident	k1gMnSc1	prezident
podpořil	podpořit	k5eAaPmAgMnS	podpořit
tvítem	tvít	k1gInSc7	tvít
ze	z	k7c2	z
Singapuru	Singapur	k1gInSc6	Singapur
tři	tři	k4xCgFnPc1	tři
hodiny	hodina	k1gFnPc1	hodina
před	před	k7c7	před
koncem	konec	k1gInSc7	konec
hlasování	hlasování	k1gNnSc2	hlasování
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
Sanfordovi	Sanforda	k1gMnSc6	Sanforda
se	se	k3xPyFc4	se
Trump	Trump	k1gMnSc1	Trump
naopak	naopak	k6eAd1	naopak
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
zdrojem	zdroj	k1gInSc7	zdroj
problémů	problém	k1gInPc2	problém
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
he	he	k0	he
is	is	k?	is
nothing	nothing	k1gInSc1	nothing
but	but	k?	but
trouble	trouble	k6eAd1	trouble
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Červenec	červenec	k1gInSc4	červenec
až	až	k9	až
září	září	k1gNnSc4	září
====	====	k?	====
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
27	[number]	k4	27
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
oznámil	oznámit	k5eAaPmAgMnS	oznámit
81	[number]	k4	81
<g/>
letý	letý	k2eAgMnSc1d1	letý
soudce	soudce	k1gMnSc1	soudce
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
Anthony	Anthona	k1gFnSc2	Anthona
Kennedy	Kenned	k1gMnPc4	Kenned
<g/>
,	,	kIx,	,
že	že	k8xS	že
ku	k	k7c3	k
dni	den	k1gInSc3	den
31	[number]	k4	31
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2018	[number]	k4	2018
odejde	odejít	k5eAaPmIp3nS	odejít
na	na	k7c4	na
odpočinek	odpočinek	k1gInSc4	odpočinek
<g/>
.	.	kIx.	.
</s>
<s>
Trump	Trump	k1gInSc1	Trump
tím	ten	k3xDgNnSc7	ten
dostal	dostat	k5eAaPmAgInS	dostat
příležitost	příležitost	k1gFnSc4	příležitost
nominovat	nominovat	k5eAaBmF	nominovat
nového	nový	k2eAgMnSc4d1	nový
soudce	soudce	k1gMnSc4	soudce
podle	podle	k7c2	podle
vlastního	vlastní	k2eAgNnSc2d1	vlastní
uvážení	uvážení	k1gNnSc2	uvážení
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
za	za	k7c2	za
dané	daný	k2eAgFnSc2d1	daná
situace	situace	k1gFnSc2	situace
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
budoucí	budoucí	k2eAgNnSc1d1	budoucí
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
tohoto	tento	k3xDgInSc2	tento
soudu	soud	k1gInSc2	soud
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
9	[number]	k4	9
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
oznámil	oznámit	k5eAaPmAgMnS	oznámit
svého	svůj	k3xOyFgMnSc4	svůj
kandidáta	kandidát	k1gMnSc4	kandidát
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
jím	on	k3xPp3gMnSc7	on
53	[number]	k4	53
<g/>
letý	letý	k2eAgMnSc1d1	letý
konzervativní	konzervativní	k2eAgMnSc1d1	konzervativní
právník	právník	k1gMnSc1	právník
a	a	k8xC	a
absolvent	absolvent	k1gMnSc1	absolvent
elitní	elitní	k2eAgFnSc2d1	elitní
Yale	Yal	k1gFnSc2	Yal
University	universita	k1gFnSc2	universita
Brett	Brett	k1gMnSc1	Brett
Kavanaugh	Kavanaugh	k1gMnSc1	Kavanaugh
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ještě	ještě	k6eAd1	ještě
musel	muset	k5eAaImAgInS	muset
být	být	k5eAaImF	být
potvrzen	potvrdit	k5eAaPmNgInS	potvrdit
Senátem	senát	k1gInSc7	senát
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Kavanaugh	Kavanaugh	k1gInSc1	Kavanaugh
byl	být	k5eAaImAgInS	být
pak	pak	k6eAd1	pak
6	[number]	k4	6
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2018	[number]	k4	2018
přes	přes	k7c4	přes
velké	velký	k2eAgInPc4d1	velký
protesty	protest	k1gInPc4	protest
Demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
části	část	k1gFnSc2	část
americké	americký	k2eAgFnSc2d1	americká
veřejnosti	veřejnost	k1gFnSc2	veřejnost
potvrzen	potvrdit	k5eAaPmNgInS	potvrdit
Senátem	senát	k1gInSc7	senát
USA	USA	kA	USA
poměrem	poměr	k1gInSc7	poměr
hlasů	hlas	k1gInPc2	hlas
50	[number]	k4	50
:	:	kIx,	:
48	[number]	k4	48
a	a	k8xC	a
doplnil	doplnit	k5eAaPmAgMnS	doplnit
soudcovské	soudcovský	k2eAgNnSc4d1	soudcovské
kolegium	kolegium	k1gNnSc4	kolegium
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
na	na	k7c4	na
plný	plný	k2eAgInSc4d1	plný
počet	počet	k1gInSc4	počet
devíti	devět	k4xCc2	devět
členů	člen	k1gInPc2	člen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
dnech	den	k1gInPc6	den
12	[number]	k4	12
<g/>
.	.	kIx.	.
a	a	k8xC	a
13	[number]	k4	13
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
se	se	k3xPyFc4	se
Trump	Trump	k1gInSc1	Trump
zúčastnil	zúčastnit	k5eAaPmAgInS	zúčastnit
summitu	summit	k1gInSc2	summit
NATO	NATO	kA	NATO
v	v	k7c6	v
Bruselu	Brusel	k1gInSc6	Brusel
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Spojené	spojený	k2eAgNnSc4d1	spojené
království	království	k1gNnSc4	království
a	a	k8xC	a
setkal	setkat	k5eAaPmAgMnS	setkat
se	se	k3xPyFc4	se
s	s	k7c7	s
královnou	královna	k1gFnSc7	královna
Alžbětou	Alžběta	k1gFnSc7	Alžběta
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
premiérkou	premiérka	k1gFnSc7	premiérka
Theresou	Theresa	k1gFnSc7	Theresa
Mayovou	Mayová	k1gFnSc7	Mayová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pondělí	pondělí	k1gNnSc6	pondělí
16	[number]	k4	16
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2018	[number]	k4	2018
se	se	k3xPyFc4	se
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
jeho	jeho	k3xOp3gNnSc1	jeho
dlouho	dlouho	k6eAd1	dlouho
očekávané	očekávaný	k2eAgNnSc1d1	očekávané
dvoustranné	dvoustranný	k2eAgNnSc1d1	dvoustranné
setkání	setkání	k1gNnSc1	setkání
s	s	k7c7	s
ruským	ruský	k2eAgMnSc7d1	ruský
prezidentem	prezident	k1gMnSc7	prezident
Vladimirem	Vladimir	k1gMnSc7	Vladimir
Putinem	Putin	k1gMnSc7	Putin
ve	v	k7c6	v
finských	finský	k2eAgFnPc6d1	finská
Helsinkách	Helsinky	k1gFnPc6	Helsinky
<g/>
.	.	kIx.	.
</s>
<s>
Mnoha	mnoho	k4c3	mnoho
médii	médium	k1gNnPc7	médium
byly	být	k5eAaImAgInP	být
před	před	k7c7	před
touto	tento	k3xDgFnSc7	tento
Trumpovou	Trumpový	k2eAgFnSc7d1	Trumpová
cestou	cesta	k1gFnSc7	cesta
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
vyjadřovány	vyjadřován	k2eAgFnPc4d1	vyjadřována
spekulativní	spekulativní	k2eAgFnPc4d1	spekulativní
obavy	obava	k1gFnPc4	obava
<g/>
,	,	kIx,	,
že	že	k8xS	že
Trump	Trump	k1gMnSc1	Trump
při	při	k7c6	při
ní	on	k3xPp3gFnSc6	on
poškodí	poškodit	k5eAaPmIp3nS	poškodit
řadu	řada	k1gFnSc4	řada
zájmových	zájmový	k2eAgFnPc2d1	zájmová
oblastí	oblast	k1gFnPc2	oblast
západních	západní	k2eAgMnPc2d1	západní
spojenců	spojenec	k1gMnPc2	spojenec
USA	USA	kA	USA
a	a	k8xC	a
krom	krom	k7c2	krom
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
příliš	příliš	k6eAd1	příliš
intenzívně	intenzívně	k6eAd1	intenzívně
politicky	politicky	k6eAd1	politicky
sblíží	sblížit	k5eAaPmIp3nS	sblížit
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
a	a	k8xC	a
osobně	osobně	k6eAd1	osobně
s	s	k7c7	s
Putinem	Putin	k1gMnSc7	Putin
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
Evropy	Evropa	k1gFnSc2	Evropa
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
Trump	Trump	k1gMnSc1	Trump
televizní	televizní	k2eAgFnSc4d1	televizní
stanici	stanice	k1gFnSc4	stanice
Fox	fox	k1gInSc1	fox
News	Newsa	k1gFnPc2	Newsa
interview	interview	k1gNnSc1	interview
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
negativně	negativně	k6eAd1	negativně
o	o	k7c6	o
imigrační	imigrační	k2eAgFnSc6d1	imigrační
politice	politika	k1gFnSc6	politika
německé	německý	k2eAgFnSc2d1	německá
kancléřky	kancléřka	k1gFnSc2	kancléřka
Angely	Angela	k1gFnSc2	Angela
Merkelové	Merkelová	k1gFnSc2	Merkelová
<g/>
.	.	kIx.	.
</s>
<s>
Doslova	doslova	k6eAd1	doslova
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Angela	Angela	k1gFnSc1	Angela
byla	být	k5eAaImAgFnS	být
superhvězda	superhvězda	k1gFnSc1	superhvězda
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
než	než	k8xS	než
dovolila	dovolit	k5eAaPmAgFnS	dovolit
milionům	milion	k4xCgInPc3	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
přišli	přijít	k5eAaPmAgMnP	přijít
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
volbách	volba	k1gFnPc6	volba
neporazitelná	porazitelný	k2eNgFnSc1d1	neporazitelná
<g/>
.	.	kIx.	.
</s>
<s>
Přistěhovalství	přistěhovalství	k1gNnSc1	přistěhovalství
jí	jíst	k5eAaImIp3nS	jíst
velmi	velmi	k6eAd1	velmi
těžce	těžce	k6eAd1	těžce
uškodilo	uškodit	k5eAaPmAgNnS	uškodit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
interview	interview	k1gNnSc6	interview
pro	pro	k7c4	pro
anglický	anglický	k2eAgInSc4d1	anglický
deník	deník	k1gInSc4	deník
The	The	k1gMnSc3	The
Sun	Sun	kA	Sun
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
dále	daleko	k6eAd2	daleko
<g/>
,	,	kIx,	,
že	že	k8xS	že
přistěhovalství	přistěhovalství	k1gNnSc1	přistěhovalství
není	být	k5eNaImIp3nS	být
dobré	dobrý	k2eAgNnSc1d1	dobré
pro	pro	k7c4	pro
Evropu	Evropa	k1gFnSc4	Evropa
jako	jako	k8xS	jako
celek	celek	k1gInSc4	celek
a	a	k8xC	a
také	také	k9	také
ne	ne	k9	ne
pro	pro	k7c4	pro
USA	USA	kA	USA
<g/>
.	.	kIx.	.
<g/>
Trumpova	Trumpův	k2eAgFnSc1d1	Trumpova
administrativa	administrativa	k1gFnSc1	administrativa
uvalila	uvalit	k5eAaPmAgFnS	uvalit
sankce	sankce	k1gFnPc4	sankce
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
turecké	turecký	k2eAgInPc4d1	turecký
ministry	ministr	k1gMnPc4	ministr
kvůli	kvůli	k7c3	kvůli
zadržování	zadržování	k1gNnSc3	zadržování
amerického	americký	k2eAgMnSc2d1	americký
pastora	pastor	k1gMnSc2	pastor
Andrewa	Andrewus	k1gMnSc2	Andrewus
Brunsona	Brunson	k1gMnSc2	Brunson
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgInS	být
zatčen	zatknout	k5eAaPmNgMnS	zatknout
během	během	k7c2	během
čistek	čistka	k1gFnPc2	čistka
v	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
po	po	k7c6	po
neúspěšném	úspěšný	k2eNgInSc6d1	neúspěšný
pokusu	pokus	k1gInSc6	pokus
o	o	k7c4	o
vojenský	vojenský	k2eAgInSc4d1	vojenský
převrat	převrat	k1gInSc4	převrat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
a	a	k8xC	a
obviněn	obviněn	k2eAgMnSc1d1	obviněn
z	z	k7c2	z
kontaktů	kontakt	k1gInPc2	kontakt
na	na	k7c4	na
hnutí	hnutí	k1gNnSc4	hnutí
kazatele	kazatel	k1gMnSc2	kazatel
Fethullaha	Fethullah	k1gMnSc2	Fethullah
Gülena	Gülen	k1gMnSc2	Gülen
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
exilu	exil	k1gInSc6	exil
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Trump	Trump	k1gMnSc1	Trump
předtím	předtím	k6eAd1	předtím
pohrozil	pohrozit	k5eAaPmAgMnS	pohrozit
Turecku	Turecko	k1gNnSc3	Turecko
"	"	kIx"	"
<g/>
rozsáhlými	rozsáhlý	k2eAgFnPc7d1	rozsáhlá
sankcemi	sankce	k1gFnPc7	sankce
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Turecko	Turecko	k1gNnSc1	Turecko
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
důležitým	důležitý	k2eAgMnSc7d1	důležitý
spojencem	spojenec	k1gMnSc7	spojenec
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
NATO	NATO	kA	NATO
<g/>
,	,	kIx,	,
v	v	k7c6	v
odvetě	odveta	k1gFnSc6	odveta
uvalilo	uvalit	k5eAaPmAgNnS	uvalit
sankce	sankce	k1gFnPc4	sankce
na	na	k7c4	na
dva	dva	k4xCgMnPc4	dva
americké	americký	k2eAgMnPc4d1	americký
ministry	ministr	k1gMnPc4	ministr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Říjen	říjen	k2eAgInSc4d1	říjen
až	až	k9	až
prosinec	prosinec	k1gInSc4	prosinec
====	====	k?	====
</s>
</p>
<p>
<s>
Trump	Trump	k1gMnSc1	Trump
se	se	k3xPyFc4	se
intenzívně	intenzívně	k6eAd1	intenzívně
zapojil	zapojit	k5eAaPmAgMnS	zapojit
do	do	k7c2	do
boje	boj	k1gInSc2	boj
republikánů	republikán	k1gMnPc2	republikán
proti	proti	k7c3	proti
demokratům	demokrat	k1gMnPc3	demokrat
před	před	k7c7	před
volbami	volba	k1gFnPc7	volba
do	do	k7c2	do
obou	dva	k4xCgFnPc2	dva
komor	komora	k1gFnPc2	komora
Kongresu	kongres	k1gInSc2	kongres
USA	USA	kA	USA
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
konaly	konat	k5eAaImAgFnP	konat
6	[number]	k4	6
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
</s>
<s>
Spadaly	spadat	k5eAaImAgFnP	spadat
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
jeho	jeho	k3xOp3gNnSc2	jeho
vlastního	vlastní	k2eAgNnSc2d1	vlastní
funkčního	funkční	k2eAgNnSc2d1	funkční
období	období	k1gNnSc2	období
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
midterm	midterm	k1gInSc1	midterm
elections	elections	k1gInSc1	elections
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
vystoupil	vystoupit	k5eAaPmAgInS	vystoupit
23	[number]	k4	23
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
na	na	k7c6	na
shromáždění	shromáždění	k1gNnSc6	shromáždění
16	[number]	k4	16
000	[number]	k4	000
přívrženců	přívrženec	k1gMnPc2	přívrženec
své	svůj	k3xOyFgFnSc2	svůj
strany	strana	k1gFnSc2	strana
v	v	k7c6	v
Houstonu	Houston	k1gInSc6	Houston
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
podpořil	podpořit	k5eAaPmAgMnS	podpořit
dosavadního	dosavadní	k2eAgMnSc2d1	dosavadní
senátora	senátor	k1gMnSc2	senátor
USA	USA	kA	USA
za	za	k7c4	za
stát	stát	k1gInSc4	stát
Texas	Texas	k1gInSc1	Texas
Teda	Ted	k1gMnSc4	Ted
Cruze	Cruze	k1gFnSc2	Cruze
<g/>
,	,	kIx,	,
svého	svůj	k3xOyFgMnSc4	svůj
rivala	rival	k1gMnSc4	rival
z	z	k7c2	z
boje	boj	k1gInSc2	boj
o	o	k7c4	o
prezidentskou	prezidentský	k2eAgFnSc4d1	prezidentská
kandidaturu	kandidatura	k1gFnSc4	kandidatura
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Cruz	Cruz	k1gMnSc1	Cruz
měl	mít	k5eAaImAgMnS	mít
silného	silný	k2eAgMnSc4d1	silný
protivníka	protivník	k1gMnSc4	protivník
v	v	k7c6	v
osobě	osoba	k1gFnSc6	osoba
podstatně	podstatně	k6eAd1	podstatně
mladšího	mladý	k2eAgMnSc2d2	mladší
demokrata	demokrat	k1gMnSc2	demokrat
Beta	beta	k1gNnSc2	beta
O	O	kA	O
<g/>
'	'	kIx"	'
<g/>
Rourkeho	Rourke	k1gMnSc2	Rourke
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
Demokratická	demokratický	k2eAgFnSc1d1	demokratická
strana	strana	k1gFnSc1	strana
vkládala	vkládat	k5eAaImAgFnS	vkládat
velké	velký	k2eAgFnPc4d1	velká
naděje	naděje	k1gFnPc4	naděje
do	do	k7c2	do
kandidatury	kandidatura	k1gFnSc2	kandidatura
Beta	beta	k1gNnSc2	beta
O	O	kA	O
<g/>
'	'	kIx"	'
<g/>
Rourkeho	Rourke	k1gMnSc2	Rourke
<g/>
,	,	kIx,	,
Trumpova	Trumpův	k2eAgFnSc1d1	Trumpova
pomoc	pomoc	k1gFnSc1	pomoc
Cruzovi	Cruz	k1gMnSc3	Cruz
výrazně	výrazně	k6eAd1	výrazně
posílila	posílit	k5eAaPmAgFnS	posílit
šance	šance	k1gFnSc1	šance
tohoto	tento	k3xDgMnSc2	tento
republikána	republikán	k1gMnSc2	republikán
na	na	k7c4	na
obhájení	obhájení	k1gNnSc4	obhájení
senátorského	senátorský	k2eAgInSc2d1	senátorský
postu	post	k1gInSc2	post
<g/>
.	.	kIx.	.
</s>
<s>
Všech	všecek	k3xTgInPc2	všecek
šest	šest	k4xCc4	šest
průzkumů	průzkum	k1gInPc2	průzkum
voličských	voličský	k2eAgFnPc2d1	voličská
preferencí	preference	k1gFnPc2	preference
z	z	k7c2	z
měsíce	měsíc	k1gInSc2	měsíc
října	říjen	k1gInSc2	říjen
vyznělo	vyznět	k5eAaImAgNnS	vyznět
v	v	k7c4	v
jeho	jeho	k3xOp3gInSc4	jeho
prospěch	prospěch	k1gInSc4	prospěch
<g/>
,	,	kIx,	,
s	s	k7c7	s
průměrem	průměr	k1gInSc7	průměr
50,8	[number]	k4	50,8
%	%	kIx~	%
:	:	kIx,	:
43,8	[number]	k4	43,8
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Trump	Trump	k1gInSc4	Trump
při	při	k7c6	při
svých	svůj	k3xOyFgNnPc6	svůj
vystoupeních	vystoupení	k1gNnPc6	vystoupení
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
v	v	k7c6	v
Houstonu	Houston	k1gInSc6	Houston
tak	tak	k6eAd1	tak
v	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
tzv.	tzv.	kA	tzv.
příhraničních	příhraniční	k2eAgInPc6d1	příhraniční
státech	stát	k1gInPc6	stát
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
border	border	k1gMnSc1	border
states	states	k1gMnSc1	states
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poukazoval	poukazovat	k5eAaImAgMnS	poukazovat
na	na	k7c4	na
solidní	solidní	k2eAgInPc4d1	solidní
ekonomické	ekonomický	k2eAgInPc4d1	ekonomický
ukazatele	ukazatel	k1gInPc4	ukazatel
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
své	svůj	k3xOyFgFnSc2	svůj
vlády	vláda	k1gFnSc2	vláda
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
<s>
Hlavně	hlavně	k6eAd1	hlavně
se	se	k3xPyFc4	se
však	však	k9	však
zaměřoval	zaměřovat	k5eAaImAgMnS	zaměřovat
na	na	k7c4	na
problematiku	problematika	k1gFnSc4	problematika
imigrace	imigrace	k1gFnSc2	imigrace
ze	z	k7c2	z
států	stát	k1gInPc2	stát
Střední	střední	k2eAgFnSc2d1	střední
Ameriky	Amerika	k1gFnSc2	Amerika
(	(	kIx(	(
<g/>
Honduras	Honduras	k1gInSc1	Honduras
<g/>
,	,	kIx,	,
El	Ela	k1gFnPc2	Ela
Salvador	Salvador	k1gInSc1	Salvador
a	a	k8xC	a
Guatemala	Guatemala	k1gFnSc1	Guatemala
<g/>
)	)	kIx)	)
přes	přes	k7c4	přes
Mexiko	Mexiko	k1gNnSc4	Mexiko
do	do	k7c2	do
USA	USA	kA	USA
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
zesílila	zesílit	k5eAaPmAgFnS	zesílit
<g/>
.	.	kIx.	.
</s>
<s>
Situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
v	v	k7c6	v
řínu	řínus	k1gInSc6	řínus
2018	[number]	k4	2018
podle	podle	k7c2	podle
Trumpa	Trump	k1gMnSc2	Trump
vyostřila	vyostřit	k5eAaPmAgFnS	vyostřit
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
poslal	poslat	k5eAaPmAgMnS	poslat
vojenské	vojenský	k2eAgFnPc4d1	vojenská
jednotky	jednotka	k1gFnPc4	jednotka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
posílily	posílit	k5eAaPmAgFnP	posílit
zabezpečení	zabezpečení	k1gNnSc4	zabezpečení
hranice	hranice	k1gFnSc2	hranice
USA	USA	kA	USA
s	s	k7c7	s
Mexikem	Mexiko	k1gNnSc7	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
již	již	k6eAd1	již
slouží	sloužit	k5eAaImIp3nS	sloužit
2	[number]	k4	2
000	[number]	k4	000
příslušníků	příslušník	k1gMnPc2	příslušník
Národní	národní	k2eAgFnSc2d1	národní
gardy	garda	k1gFnSc2	garda
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
příhraničních	příhraniční	k2eAgInPc2d1	příhraniční
států	stát	k1gInPc2	stát
dorazilo	dorazit	k5eAaPmAgNnS	dorazit
již	již	k6eAd1	již
800	[number]	k4	800
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
měli	mít	k5eAaImAgMnP	mít
Trump	Trump	k1gInSc4	Trump
a	a	k8xC	a
Pentagon	Pentagon	k1gInSc4	Pentagon
v	v	k7c6	v
úmyslu	úmysl	k1gInSc6	úmysl
poslat	poslat	k5eAaPmF	poslat
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
s	s	k7c7	s
Mexikem	Mexiko	k1gNnSc7	Mexiko
více	hodně	k6eAd2	hodně
než	než	k8xS	než
5	[number]	k4	5
200	[number]	k4	200
<g/>
,	,	kIx,	,
eventuálně	eventuálně	k6eAd1	eventuálně
až	až	k9	až
15	[number]	k4	15
tisíc	tisíc	k4xCgInPc2	tisíc
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
odstrašili	odstrašit	k5eAaPmAgMnP	odstrašit
nelegální	legální	k2eNgMnPc4d1	nelegální
imigranty	imigrant	k1gMnPc4	imigrant
od	od	k7c2	od
přechodu	přechod	k1gInSc2	přechod
hranic	hranice	k1gFnPc2	hranice
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
řekl	říct	k5eAaPmAgMnS	říct
v	v	k7c6	v
pondělí	pondělí	k1gNnSc6	pondělí
29	[number]	k4	29
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2018	[number]	k4	2018
generál	generál	k1gMnSc1	generál
Terrence	Terrenec	k1gInSc2	Terrenec
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Shaughnessy	Shaughnessa	k1gFnPc4	Shaughnessa
<g/>
.	.	kIx.	.
<g/>
Také	také	k9	také
jiné	jiný	k2eAgFnSc3d1	jiná
republikánské	republikánský	k2eAgFnSc3d1	republikánská
kandidáty	kandidát	k1gMnPc7	kandidát
do	do	k7c2	do
obou	dva	k4xCgFnPc2	dva
komor	komora	k1gFnPc2	komora
Kongresu	kongres	k1gInSc2	kongres
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc1	jejichž
zvolení	zvolení	k1gNnSc1	zvolení
bylo	být	k5eAaImAgNnS	být
nejisté	jistý	k2eNgNnSc1d1	nejisté
<g/>
,	,	kIx,	,
Trump	Trump	k1gInSc1	Trump
podporoval	podporovat	k5eAaImAgInS	podporovat
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
pozornost	pozornost	k1gFnSc4	pozornost
věnoval	věnovat	k5eAaImAgMnS	věnovat
kandidátům	kandidát	k1gMnPc3	kandidát
do	do	k7c2	do
Senátu	senát	k1gInSc2	senát
USA	USA	kA	USA
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnPc1	jejichž
znovuzvolení	znovuzvolený	k2eAgMnPc1d1	znovuzvolený
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
zisk	zisk	k1gInSc4	zisk
křesla	křeslo	k1gNnSc2	křeslo
navíc	navíc	k6eAd1	navíc
mohly	moct	k5eAaImAgFnP	moct
pomoci	pomoct	k5eAaPmF	pomoct
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
další	další	k2eAgInPc4d1	další
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
zajištěna	zajištěn	k2eAgFnSc1d1	zajištěna
pro	pro	k7c4	pro
něj	on	k3xPp3gInSc4	on
důležitá	důležitý	k2eAgFnSc1d1	důležitá
dosavadní	dosavadní	k2eAgFnSc1d1	dosavadní
republikánská	republikánský	k2eAgFnSc1d1	republikánská
většina	většina	k1gFnSc1	většina
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
komoře	komora	k1gFnSc6	komora
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
dalekosáhlé	dalekosáhlý	k2eAgFnPc4d1	dalekosáhlá
pravomoce	pravomoc	k1gFnPc4	pravomoc
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
měli	mít	k5eAaImAgMnP	mít
republikáni	republikán	k1gMnPc1	republikán
doposud	doposud	k6eAd1	doposud
těsnou	těsný	k2eAgFnSc4d1	těsná
většinu	většina	k1gFnSc4	většina
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
51	[number]	k4	51
:	:	kIx,	:
49	[number]	k4	49
<g/>
.	.	kIx.	.
</s>
<s>
Tvrdý	tvrdý	k2eAgInSc4d1	tvrdý
boj	boj	k1gInSc4	boj
se	se	k3xPyFc4	se
vedl	vést	k5eAaImAgMnS	vést
mj.	mj.	kA	mj.
o	o	k7c4	o
senátní	senátní	k2eAgNnSc4d1	senátní
místo	místo	k1gNnSc4	místo
za	za	k7c4	za
stát	stát	k1gInSc4	stát
Severní	severní	k2eAgFnSc1d1	severní
Dakota	Dakota	k1gFnSc1	Dakota
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dosavadní	dosavadní	k2eAgFnSc1d1	dosavadní
demokratická	demokratický	k2eAgFnSc1d1	demokratická
senátorka	senátorka	k1gFnSc1	senátorka
Heidi	Heid	k1gMnPc1	Heid
Heitkamp	Heitkamp	k1gMnSc1	Heitkamp
stála	stát	k5eAaImAgFnS	stát
pod	pod	k7c7	pod
silným	silný	k2eAgInSc7d1	silný
tlakem	tlak	k1gInSc7	tlak
svého	svůj	k3xOyFgMnSc2	svůj
oponenta	oponent	k1gMnSc2	oponent
Kevina	Kevin	k1gMnSc2	Kevin
Cramera	Cramer	k1gMnSc2	Cramer
<g/>
.	.	kIx.	.
</s>
<s>
Problémem	problém	k1gInSc7	problém
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
aféra	aféra	k1gFnSc1	aféra
s	s	k7c7	s
uveřejněním	uveřejnění	k1gNnSc7	uveřejnění
jmen	jméno	k1gNnPc2	jméno
údajných	údajný	k2eAgFnPc2d1	údajná
obětí	oběť	k1gFnPc2	oběť
sexuálního	sexuální	k2eAgNnSc2d1	sexuální
obtěžování	obtěžování	k1gNnSc2	obtěžování
bez	bez	k7c2	bez
jejich	jejich	k3xOp3gInSc2	jejich
souhlasu	souhlas	k1gInSc2	souhlas
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
zapříčinili	zapříčinit	k5eAaPmAgMnP	zapříčinit
spolupracovníci	spolupracovník	k1gMnPc1	spolupracovník
Heitkampové	Heitkampová	k1gFnSc2	Heitkampová
<g/>
.	.	kIx.	.
</s>
<s>
Týkalo	týkat	k5eAaImAgNnS	týkat
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Heidi	Heid	k1gMnPc1	Heid
Heitkamp	Heitkamp	k1gInSc4	Heitkamp
měla	mít	k5eAaImAgFnS	mít
proto	proto	k8xC	proto
průběžně	průběžně	k6eAd1	průběžně
ztrátu	ztráta	k1gFnSc4	ztráta
zhruba	zhruba	k6eAd1	zhruba
10-16	[number]	k4	10-16
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
proti	proti	k7c3	proti
Kevinu	Kevin	k1gMnSc3	Kevin
Cramerovi	Cramer	k1gMnSc3	Cramer
<g/>
,	,	kIx,	,
musela	muset	k5eAaImAgFnS	muset
i	i	k9	i
z	z	k7c2	z
finančních	finanční	k2eAgInPc2d1	finanční
důvodů	důvod	k1gInPc2	důvod
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
omezit	omezit	k5eAaPmF	omezit
svou	svůj	k3xOyFgFnSc4	svůj
kampaň	kampaň	k1gFnSc4	kampaň
a	a	k8xC	a
volby	volba	k1gFnPc4	volba
nakonec	nakonec	k6eAd1	nakonec
prohrála	prohrát	k5eAaPmAgFnS	prohrát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
souhrnu	souhrn	k1gInSc2	souhrn
průzkumů	průzkum	k1gInPc2	průzkum
volebních	volební	k2eAgInPc2d1	volební
preferencí	preference	k1gFnSc7	preference
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
uveřejňovala	uveřejňovat	k5eAaImAgFnS	uveřejňovat
americká	americký	k2eAgFnSc1d1	americká
organizace	organizace	k1gFnSc1	organizace
Real	Real	k1gInSc1	Real
Clear	Clear	k1gMnSc1	Clear
Politics	Politics	k1gInSc1	Politics
<g/>
,	,	kIx,	,
ztráta	ztráta	k1gFnSc1	ztráta
senátního	senátní	k2eAgNnSc2d1	senátní
křesla	křeslo	k1gNnSc2	křeslo
Heitkampové	Heitkampová	k1gFnSc2	Heitkampová
a	a	k8xC	a
ztráta	ztráta	k1gFnSc1	ztráta
nejméně	málo	k6eAd3	málo
dalšího	další	k2eAgMnSc2d1	další
jednoho	jeden	k4xCgMnSc4	jeden
postu	post	k1gInSc6	post
mohla	moct	k5eAaImAgFnS	moct
pro	pro	k7c4	pro
demokraty	demokrat	k1gMnPc4	demokrat
znamenat	znamenat	k5eAaImF	znamenat
snížení	snížení	k1gNnSc3	snížení
počtu	počet	k1gInSc2	počet
jejich	jejich	k3xOp3gNnPc2	jejich
senátních	senátní	k2eAgNnPc2d1	senátní
míst	místo	k1gNnPc2	místo
na	na	k7c4	na
47	[number]	k4	47
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
na	na	k7c4	na
46	[number]	k4	46
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
republikáni	republikán	k1gMnPc1	republikán
by	by	kYmCp3nP	by
mohli	moct	k5eAaImAgMnP	moct
od	od	k7c2	od
<g />
.	.	kIx.	.
</s>
<s>
ledna	leden	k1gInSc2	leden
2019	[number]	k4	2019
mít	mít	k5eAaImF	mít
v	v	k7c6	v
Senátu	senát	k1gInSc6	senát
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
posty	post	k1gInPc4	post
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
53	[number]	k4	53
křesel	křeslo	k1gNnPc2	křeslo
(	(	kIx(	(
<g/>
eventuálně	eventuálně	k6eAd1	eventuálně
i	i	k8xC	i
o	o	k7c4	o
tři	tři	k4xCgMnPc4	tři
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
54	[number]	k4	54
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
vraždou	vražda	k1gFnSc7	vražda
saúdskoarabského	saúdskoarabský	k2eAgMnSc2d1	saúdskoarabský
opozičního	opoziční	k2eAgMnSc2d1	opoziční
novináře	novinář	k1gMnSc2	novinář
Džamála	Džamál	k1gMnSc2	Džamál
Chášukdžího	Chášukdží	k1gMnSc2	Chášukdží
na	na	k7c6	na
saúdském	saúdský	k2eAgInSc6d1	saúdský
generálním	generální	k2eAgInSc6d1	generální
konzulátě	konzulát	k1gInSc6	konzulát
v	v	k7c6	v
Istanbulu	Istanbul	k1gInSc6	Istanbul
odmítl	odmítnout	k5eAaPmAgInS	odmítnout
Trump	Trump	k1gInSc1	Trump
pozastavit	pozastavit	k5eAaPmF	pozastavit
dodávky	dodávka	k1gFnPc4	dodávka
amerických	americký	k2eAgFnPc2d1	americká
zbraní	zbraň	k1gFnPc2	zbraň
do	do	k7c2	do
Saúdské	saúdský	k2eAgFnSc2d1	Saúdská
Arábie	Arábie	k1gFnSc2	Arábie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
dlouhodobým	dlouhodobý	k2eAgMnSc7d1	dlouhodobý
spojencem	spojenec	k1gMnSc7	spojenec
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c4	v
předvečer	předvečer	k1gInSc4	předvečer
voleb	volba	k1gFnPc2	volba
uprostřed	uprostřed	k7c2	uprostřed
volebního	volební	k2eAgNnSc2d1	volební
období	období	k1gNnSc2	období
(	(	kIx(	(
<g/>
midterm	midterm	k1gInSc1	midterm
elections	elections	k1gInSc1	elections
<g/>
)	)	kIx)	)
dne	den	k1gInSc2	den
6	[number]	k4	6
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2018	[number]	k4	2018
byly	být	k5eAaImAgFnP	být
zveřejněny	zveřejnit	k5eAaPmNgInP	zveřejnit
průběžné	průběžný	k2eAgInPc1d1	průběžný
výsledky	výsledek	k1gInPc1	výsledek
průzkumu	průzkum	k1gInSc2	průzkum
oblíbenosti	oblíbenost	k1gFnPc1	oblíbenost
pěti	pět	k4xCc2	pět
vrcholných	vrcholný	k2eAgMnPc2d1	vrcholný
představitelů	představitel	k1gMnPc2	představitel
USA	USA	kA	USA
u	u	k7c2	u
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
tito	tento	k3xDgMnPc1	tento
politikové	politik	k1gMnPc1	politik
obdrželi	obdržet	k5eAaPmAgMnP	obdržet
negativní	negativní	k2eAgNnSc4d1	negativní
hodnocení	hodnocení	k1gNnSc4	hodnocení
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
Trumpův	Trumpův	k2eAgInSc1d1	Trumpův
výsledek	výsledek	k1gInSc1	výsledek
byl	být	k5eAaImAgInS	být
relativně	relativně	k6eAd1	relativně
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
<g/>
.	.	kIx.	.
41,8	[number]	k4	41,8
%	%	kIx~	%
dotázaných	dotázaný	k2eAgMnPc2d1	dotázaný
mělo	mít	k5eAaImAgNnS	mít
o	o	k7c6	o
něm	on	k3xPp3gNnSc6	on
pozitivní	pozitivní	k2eAgNnSc1d1	pozitivní
mínění	mínění	k1gNnSc1	mínění
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
negativně	negativně	k6eAd1	negativně
jej	on	k3xPp3gMnSc4	on
vidělo	vidět	k5eAaImAgNnS	vidět
54,5	[number]	k4	54,5
%	%	kIx~	%
lidí	člověk	k1gMnPc2	člověk
<g/>
;	;	kIx,	;
rozdíl	rozdíl	k1gInSc1	rozdíl
byl	být	k5eAaImAgInS	být
tedy	tedy	k9	tedy
-12,7	-12,7	k4	-12,7
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Mnohem	mnohem	k6eAd1	mnohem
méně	málo	k6eAd2	málo
příznivý	příznivý	k2eAgMnSc1d1	příznivý
byl	být	k5eAaImAgMnS	být
úsudek	úsudek	k1gInSc4	úsudek
respondentů	respondent	k1gMnPc2	respondent
o	o	k7c4	o
mluvčí	mluvčí	k1gFnSc4	mluvčí
demokratů	demokrat	k1gMnPc2	demokrat
ve	v	k7c6	v
Sněmovně	sněmovna	k1gFnSc6	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
Nancy	Nancy	k1gFnPc2	Nancy
Pelosiové	Pelosiový	k2eAgNnSc1d1	Pelosiové
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gNnSc1	jejíž
nepříznivé	příznivý	k2eNgNnSc1d1	nepříznivé
saldo	saldo	k1gNnSc1	saldo
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
-26,0	-26,0	k4	-26,0
%	%	kIx~	%
(	(	kIx(	(
<g/>
pozitivně	pozitivně	k6eAd1	pozitivně
ji	on	k3xPp3gFnSc4	on
vidělo	vidět	k5eAaImAgNnS	vidět
jen	jen	k9	jen
27,5	[number]	k4	27,5
%	%	kIx~	%
dotázaných	dotázaný	k2eAgFnPc2d1	dotázaná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mluvčí	mluvčí	k1gFnSc1	mluvčí
republikánů	republikán	k1gMnPc2	republikán
ve	v	k7c6	v
Sněmovně	sněmovna	k1gFnSc6	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
Paul	Paul	k1gMnSc1	Paul
Ryan	Ryan	k1gMnSc1	Ryan
obdržel	obdržet	k5eAaPmAgMnS	obdržet
negativní	negativní	k2eAgInSc4d1	negativní
výsledek	výsledek	k1gInSc4	výsledek
-22,3	-22,3	k4	-22,3
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgFnPc1	druhý
relativně	relativně	k6eAd1	relativně
nejlepší	dobrý	k2eAgNnPc4d3	nejlepší
hodnocení	hodnocení	k1gNnPc4	hodnocení
připadlo	připadnout	k5eAaPmAgNnS	připadnout
na	na	k7c4	na
lídra	lídr	k1gMnSc4	lídr
demokratické	demokratický	k2eAgFnSc2d1	demokratická
menšiny	menšina	k1gFnSc2	menšina
v	v	k7c6	v
Senátě	senát	k1gInSc6	senát
USA	USA	kA	USA
Chucka	Chucka	k1gFnSc1	Chucka
Schumera	Schumera	k1gFnSc1	Schumera
s	s	k7c7	s
hodnotou	hodnota	k1gFnSc7	hodnota
-15,0	-15,0	k4	-15,0
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Nejhorší	zlý	k2eAgInSc4d3	Nejhorší
výsledek	výsledek	k1gInSc4	výsledek
obdržel	obdržet	k5eAaPmAgMnS	obdržet
republikánský	republikánský	k2eAgMnSc1d1	republikánský
mluvčí	mluvčí	k1gMnSc1	mluvčí
Senátu	senát	k1gInSc2	senát
Mitch	Mitch	k1gMnSc1	Mitch
McConnell	McConnell	k1gMnSc1	McConnell
<g/>
,	,	kIx,	,
a	a	k8xC	a
sice	sice	k8xC	sice
-27,2	-27,2	k4	-27,2
%	%	kIx~	%
<g/>
.	.	kIx.	.
<g/>
Republikáni	republikán	k1gMnPc1	republikán
ztratili	ztratit	k5eAaPmAgMnP	ztratit
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Kongresu	kongres	k1gInSc2	kongres
USA	USA	kA	USA
svou	svůj	k3xOyFgFnSc4	svůj
dosavadní	dosavadní	k2eAgFnSc4d1	dosavadní
většinu	většina	k1gFnSc4	většina
ve	v	k7c6	v
Sněmovně	sněmovna	k1gFnSc6	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
vyslovenou	vyslovený	k2eAgFnSc7d1	vyslovená
prioritou	priorita	k1gFnSc7	priorita
prezidenta	prezident	k1gMnSc2	prezident
Trumpa	Trump	k1gMnSc2	Trump
bylo	být	k5eAaImAgNnS	být
udržení	udržení	k1gNnSc1	udržení
většiny	většina	k1gFnSc2	většina
v	v	k7c6	v
Senátě	senát	k1gInSc6	senát
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
nejen	nejen	k6eAd1	nejen
podařilo	podařit	k5eAaPmAgNnS	podařit
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
byly	být	k5eAaImAgFnP	být
získány	získat	k5eAaPmNgFnP	získat
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
dva	dva	k4xCgInPc4	dva
posty	post	k1gInPc4	post
navíc	navíc	k6eAd1	navíc
proti	proti	k7c3	proti
dosavadnímu	dosavadní	k2eAgInSc3d1	dosavadní
stavu	stav	k1gInSc3	stav
<g/>
.	.	kIx.	.
</s>
<s>
Takový	takový	k3xDgInSc1	takový
výsledek	výsledek	k1gInSc1	výsledek
se	se	k3xPyFc4	se
za	za	k7c2	za
posledních	poslední	k2eAgNnPc2d1	poslední
30	[number]	k4	30
let	léto	k1gNnPc2	léto
zdařil	zdařit	k5eAaPmAgMnS	zdařit
jen	jen	k9	jen
dvěma	dva	k4xCgMnPc3	dva
prezidentům	prezident	k1gMnPc3	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Republikánský	republikánský	k2eAgMnSc1d1	republikánský
senátor	senátor	k1gMnSc1	senátor
Cory	Cora	k1gFnSc2	Cora
Gardner	Gardner	k1gMnSc1	Gardner
(	(	kIx(	(
<g/>
Colorado	Colorado	k1gNnSc1	Colorado
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
předsedou	předseda	k1gMnSc7	předseda
výboru	výbor	k1gInSc2	výbor
republikánů	republikán	k1gMnPc2	republikán
pro	pro	k7c4	pro
volby	volba	k1gFnPc4	volba
do	do	k7c2	do
Senátu	senát	k1gInSc2	senát
<g/>
,	,	kIx,	,
ocenil	ocenit	k5eAaPmAgMnS	ocenit
velký	velký	k2eAgInSc4d1	velký
Trumpův	Trumpův	k2eAgInSc4d1	Trumpův
přínos	přínos	k1gInSc4	přínos
k	k	k7c3	k
vítězství	vítězství	k1gNnSc3	vítězství
strany	strana	k1gFnSc2	strana
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
volbách	volba	k1gFnPc6	volba
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
spočíval	spočívat	k5eAaImAgInS	spočívat
v	v	k7c6	v
neúnavném	únavný	k2eNgNnSc6d1	neúnavné
Trumpově	Trumpův	k2eAgNnSc6d1	Trumpovo
osobním	osobní	k2eAgNnSc6d1	osobní
nasazení	nasazení	k1gNnSc6	nasazení
a	a	k8xC	a
velkém	velký	k2eAgInSc6d1	velký
zájmu	zájem	k1gInSc6	zájem
lidí	člověk	k1gMnPc2	člověk
o	o	k7c4	o
jeho	jeho	k3xOp3gNnPc4	jeho
početná	početný	k2eAgNnPc4d1	početné
volební	volební	k2eAgNnPc4d1	volební
shromáždění	shromáždění	k1gNnPc4	shromáždění
ve	v	k7c6	v
státech	stát	k1gInPc6	stát
USA	USA	kA	USA
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgFnPc6	který
se	se	k3xPyFc4	se
bojovalo	bojovat	k5eAaImAgNnS	bojovat
o	o	k7c4	o
klíčová	klíčový	k2eAgNnPc4d1	klíčové
místa	místo	k1gNnPc4	místo
v	v	k7c6	v
Senátě	senát	k1gInSc6	senát
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
ledna	leden	k1gInSc2	leden
2019	[number]	k4	2019
budou	být	k5eAaImBp3nP	být
mít	mít	k5eAaImF	mít
republikáni	republikán	k1gMnPc1	republikán
53	[number]	k4	53
senátorů	senátor	k1gMnPc2	senátor
ze	z	k7c2	z
100	[number]	k4	100
<g/>
.	.	kIx.	.
</s>
<s>
Odebrali	odebrat	k5eAaPmAgMnP	odebrat
demokratům	demokrat	k1gMnPc3	demokrat
senátorské	senátorský	k2eAgFnSc2d1	senátorská
posty	posta	k1gFnSc2	posta
na	na	k7c6	na
Floridě	Florida	k1gFnSc6	Florida
<g/>
,	,	kIx,	,
v	v	k7c6	v
Indianě	Indiana	k1gFnSc6	Indiana
<g/>
,	,	kIx,	,
Missouri	Missouri	k1gFnSc6	Missouri
a	a	k8xC	a
Severní	severní	k2eAgFnSc6d1	severní
Dakotě	Dakota	k1gFnSc6	Dakota
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
ztratili	ztratit	k5eAaPmAgMnP	ztratit
mandáty	mandát	k1gInPc4	mandát
v	v	k7c6	v
Arizoně	Arizona	k1gFnSc6	Arizona
a	a	k8xC	a
Nevadě	Nevada	k1gFnSc6	Nevada
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Floridě	Florida	k1gFnSc6	Florida
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
dosavadní	dosavadní	k2eAgMnSc1d1	dosavadní
guvernér	guvernér	k1gMnSc1	guvernér
Rick	Rick	k1gMnSc1	Rick
Scott	Scott	k1gMnSc1	Scott
po	po	k7c6	po
přepočítávání	přepočítávání	k1gNnSc6	přepočítávání
hlasů	hlas	k1gInPc2	hlas
velmi	velmi	k6eAd1	velmi
těsně	těsně	k6eAd1	těsně
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zhruba	zhruba	k6eAd1	zhruba
o	o	k7c4	o
10	[number]	k4	10
000	[number]	k4	000
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
jednom	jeden	k4xCgNnSc6	jeden
senátorském	senátorský	k2eAgNnSc6d1	senátorské
místě	místo	k1gNnSc6	místo
-	-	kIx~	-
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Mississippi	Mississippi	k1gFnSc2	Mississippi
-	-	kIx~	-
se	se	k3xPyFc4	se
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
v	v	k7c6	v
dodatečné	dodatečný	k2eAgFnSc6d1	dodatečná
volbě	volba	k1gFnSc6	volba
dne	den	k1gInSc2	den
27	[number]	k4	27
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
</s>
<s>
Vítězkou	vítězka	k1gFnSc7	vítězka
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
republikánka	republikánka	k1gFnSc1	republikánka
Cindy	Cinda	k1gFnSc2	Cinda
Hyde-Smith	Hyde-Smitha	k1gFnPc2	Hyde-Smitha
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
porazila	porazit	k5eAaPmAgFnS	porazit
bývalého	bývalý	k2eAgMnSc4d1	bývalý
ministra	ministr	k1gMnSc4	ministr
zemědělství	zemědělství	k1gNnSc2	zemědělství
Mikea	Mikeus	k1gMnSc2	Mikeus
Espyho	Espy	k1gMnSc2	Espy
poměrem	poměr	k1gInSc7	poměr
hlasů	hlas	k1gInPc2	hlas
55	[number]	k4	55
%	%	kIx~	%
<g/>
:	:	kIx,	:
45	[number]	k4	45
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
2019	[number]	k4	2019
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Leden	leden	k1gInSc1	leden
až	až	k8xS	až
březen	březen	k1gInSc1	březen
====	====	k?	====
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
15	[number]	k4	15
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2019	[number]	k4	2019
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
prezident	prezident	k1gMnSc1	prezident
Trump	Trump	k1gMnSc1	Trump
stav	stav	k1gInSc4	stav
národní	národní	k2eAgFnSc2d1	národní
nouze	nouze	k1gFnSc2	nouze
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
kvůli	kvůli	k7c3	kvůli
situaci	situace	k1gFnSc3	situace
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
s	s	k7c7	s
Mexikem	Mexiko	k1gNnSc7	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Trumpa	Trump	k1gMnSc2	Trump
je	být	k5eAaImIp3nS	být
neudržitelné	udržitelný	k2eNgNnSc1d1	neudržitelné
<g/>
,	,	kIx,	,
že	že	k8xS	že
přes	přes	k7c4	přes
tyto	tento	k3xDgFnPc4	tento
hranice	hranice	k1gFnPc4	hranice
proudí	proudit	k5eAaPmIp3nP	proudit
do	do	k7c2	do
USA	USA	kA	USA
desetitisíce	desetitisíce	k1gInPc4	desetitisíce
nelegálních	legální	k2eNgMnPc2d1	nelegální
imigrantů	imigrant	k1gMnPc2	imigrant
a	a	k8xC	a
velká	velký	k2eAgNnPc4d1	velké
množství	množství	k1gNnPc4	množství
drog	droga	k1gFnPc2	droga
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
údajně	údajně	k6eAd1	údajně
nutné	nutný	k2eAgNnSc1d1	nutné
posílit	posílit	k5eAaPmF	posílit
stávající	stávající	k2eAgInSc4d1	stávající
hraniční	hraniční	k2eAgInSc4d1	hraniční
plot	plot	k1gInSc4	plot
a	a	k8xC	a
na	na	k7c4	na
zhruba	zhruba	k6eAd1	zhruba
400	[number]	k4	400
km	km	kA	km
délky	délka	k1gFnSc2	délka
vybudovat	vybudovat	k5eAaPmF	vybudovat
vysokou	vysoký	k2eAgFnSc4d1	vysoká
a	a	k8xC	a
zajištěnou	zajištěný	k2eAgFnSc4d1	zajištěná
zeď	zeď	k1gFnSc4	zeď
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
Kongres	kongres	k1gInSc1	kongres
USA	USA	kA	USA
nepovolil	povolit	k5eNaPmAgInS	povolit
pro	pro	k7c4	pro
tyto	tento	k3xDgInPc4	tento
účely	účel	k1gInPc4	účel
dostatečné	dostatečný	k2eAgInPc4d1	dostatečný
prostředky	prostředek	k1gInPc7	prostředek
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
Trump	Trump	k1gMnSc1	Trump
touto	tento	k3xDgFnSc7	tento
cestou	cesta	k1gFnSc7	cesta
možnost	možnost	k1gFnSc1	možnost
uvolnit	uvolnit	k5eAaPmF	uvolnit
celkově	celkově	k6eAd1	celkově
až	až	k9	až
zhruba	zhruba	k6eAd1	zhruba
8	[number]	k4	8
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
zčásti	zčásti	k6eAd1	zčásti
z	z	k7c2	z
rozpočtu	rozpočet	k1gInSc2	rozpočet
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
obrany	obrana	k1gFnSc2	obrana
<g/>
.26	.26	k4	.26
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2019	[number]	k4	2019
přiletěl	přiletět	k5eAaPmAgMnS	přiletět
Trump	Trump	k1gMnSc1	Trump
s	s	k7c7	s
početným	početný	k2eAgInSc7d1	početný
doprovodem	doprovod	k1gInSc7	doprovod
do	do	k7c2	do
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Vietnamu	Vietnam	k1gInSc2	Vietnam
Hanoje	Hanoj	k1gFnSc2	Hanoj
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
po	po	k7c4	po
dva	dva	k4xCgInPc4	dva
následující	následující	k2eAgInPc4d1	následující
dny	den	k1gInPc4	den
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
vůdcem	vůdce	k1gMnSc7	vůdce
Severní	severní	k2eAgFnSc2d1	severní
Koreje	Korea	k1gFnSc2	Korea
Kim	Kim	k1gMnSc7	Kim
Čong-unem	Čongn	k1gMnSc7	Čong-un
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
druhé	druhý	k4xOgFnSc6	druhý
jejich	jejich	k3xOp3gNnSc2	jejich
setkání	setkání	k1gNnSc1	setkání
po	po	k7c6	po
Singapurském	singapurský	k2eAgInSc6d1	singapurský
summitu	summit	k1gInSc6	summit
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
zčásti	zčásti	k6eAd1	zčásti
přehnaná	přehnaný	k2eAgNnPc4d1	přehnané
očekávání	očekávání	k1gNnPc4	očekávání
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
však	však	k9	však
tato	tento	k3xDgFnSc1	tento
schůzka	schůzka	k1gFnSc1	schůzka
nepřinesla	přinést	k5eNaPmAgFnS	přinést
žádné	žádný	k3yNgInPc4	žádný
hmatatelné	hmatatelný	k2eAgInPc4d1	hmatatelný
výsledky	výsledek	k1gInPc4	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
Kim	Kim	k?	Kim
nebyl	být	k5eNaImAgMnS	být
ochoten	ochoten	k2eAgMnSc1d1	ochoten
přistoupit	přistoupit	k5eAaPmF	přistoupit
na	na	k7c4	na
úplné	úplný	k2eAgNnSc4d1	úplné
odzbrojení	odzbrojení	k1gNnSc4	odzbrojení
severokorejského	severokorejský	k2eAgInSc2d1	severokorejský
atomového	atomový	k2eAgInSc2d1	atomový
arzenálu	arzenál	k1gInSc2	arzenál
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
nabízel	nabízet	k5eAaImAgInS	nabízet
pouze	pouze	k6eAd1	pouze
částečné	částečný	k2eAgNnSc4d1	částečné
odzbrojení	odzbrojení	k1gNnSc4	odzbrojení
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
zrušení	zrušení	k1gNnSc4	zrušení
přísných	přísný	k2eAgFnPc2d1	přísná
hospodářských	hospodářský	k2eAgFnPc2d1	hospodářská
sankcí	sankce	k1gFnPc2	sankce
vůči	vůči	k7c3	vůči
svému	svůj	k3xOyFgInSc3	svůj
režimu	režim	k1gInSc3	režim
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
však	však	k9	však
Trump	Trump	k1gInSc1	Trump
a	a	k8xC	a
americká	americký	k2eAgFnSc1d1	americká
vláda	vláda	k1gFnSc1	vláda
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Summit	summit	k1gInSc1	summit
skončil	skončit	k5eAaPmAgInS	skončit
o	o	k7c4	o
dvě	dva	k4xCgFnPc4	dva
hodiny	hodina	k1gFnPc4	hodina
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
bylo	být	k5eAaImAgNnS	být
plánováno	plánovat	k5eAaImNgNnS	plánovat
<g/>
.	.	kIx.	.
</s>
<s>
Trumpovo	Trumpův	k2eAgNnSc1d1	Trumpovo
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
skončit	skončit	k5eAaPmF	skončit
summit	summit	k1gInSc4	summit
bez	bez	k7c2	bez
výsledku	výsledek	k1gInSc2	výsledek
zhodnotil	zhodnotit	k5eAaPmAgMnS	zhodnotit
vůdce	vůdce	k1gMnSc1	vůdce
demokratické	demokratický	k2eAgFnSc2d1	demokratická
menšiny	menšina	k1gFnSc2	menšina
v	v	k7c6	v
Senátu	senát	k1gInSc6	senát
USA	USA	kA	USA
Chuck	Chuck	k1gMnSc1	Chuck
Schumer	Schumer	k1gMnSc1	Schumer
pozitivně	pozitivně	k6eAd1	pozitivně
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Prezident	prezident	k1gMnSc1	prezident
udělal	udělat	k5eAaPmAgMnS	udělat
dobře	dobře	k6eAd1	dobře
<g/>
,	,	kIx,	,
že	že	k8xS	že
přerušil	přerušit	k5eAaPmAgMnS	přerušit
rozhovory	rozhovor	k1gInPc4	rozhovor
a	a	k8xC	a
vzdal	vzdát	k5eAaPmAgMnS	vzdát
se	se	k3xPyFc4	se
dobré	dobrý	k2eAgFnSc2d1	dobrá
fotografie	fotografia	k1gFnSc2	fotografia
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
špatný	špatný	k2eAgInSc4d1	špatný
obchod	obchod	k1gInSc4	obchod
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Při	při	k7c6	při
oficiální	oficiální	k2eAgFnSc6d1	oficiální
návštěvě	návštěva	k1gFnSc6	návštěva
českého	český	k2eAgMnSc2d1	český
premiéra	premiér	k1gMnSc2	premiér
Andreje	Andrej	k1gMnSc2	Andrej
Babiše	Babiš	k1gMnSc2	Babiš
v	v	k7c6	v
USA	USA	kA	USA
byl	být	k5eAaImAgInS	být
Babiš	Babiš	k1gInSc4	Babiš
prezidentem	prezident	k1gMnSc7	prezident
Trumpem	Trump	k1gMnSc7	Trump
přijat	přijmout	k5eAaPmNgMnS	přijmout
v	v	k7c6	v
Bílém	bílý	k2eAgInSc6d1	bílý
domě	dům	k1gInSc6	dům
<g/>
.22	.22	k4	.22
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
předal	předat	k5eAaPmAgMnS	předat
zvláštní	zvláštní	k2eAgMnSc1d1	zvláštní
poradce	poradce	k1gMnSc1	poradce
Robert	Robert	k1gMnSc1	Robert
Mueller	Mueller	k1gMnSc1	Mueller
ministrovi	ministr	k1gMnSc3	ministr
spravedlnosti	spravedlnost	k1gFnSc3	spravedlnost
Williamu	William	k1gInSc2	William
Barrovi	Barr	k1gMnSc3	Barr
dlouho	dlouho	k6eAd1	dlouho
očekávanou	očekávaný	k2eAgFnSc4d1	očekávaná
konečnou	konečný	k2eAgFnSc4d1	konečná
zprávu	zpráva	k1gFnSc4	zpráva
o	o	k7c6	o
výsledku	výsledek	k1gInSc6	výsledek
svého	svůj	k3xOyFgNnSc2	svůj
vyšetřování	vyšetřování	k1gNnSc2	vyšetřování
ve	v	k7c6	v
věci	věc	k1gFnSc6	věc
údajné	údajný	k2eAgFnPc1d1	údajná
"	"	kIx"	"
<g/>
Russian	Russian	k1gInSc1	Russian
connection	connection	k1gInSc1	connection
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
spojení	spojení	k1gNnSc1	spojení
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
<g/>
)	)	kIx)	)
volebního	volební	k2eAgInSc2d1	volební
týmu	tým	k1gInSc2	tým
Donalda	Donald	k1gMnSc2	Donald
Trumpa	Trump	k1gMnSc2	Trump
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
a	a	k8xC	a
eventuální	eventuální	k2eAgFnSc2d1	eventuální
osobní	osobní	k2eAgFnSc2d1	osobní
angažovanosti	angažovanost	k1gFnSc2	angažovanost
úřadujícího	úřadující	k2eAgMnSc2d1	úřadující
prezidenta	prezident	k1gMnSc2	prezident
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Den	den	k1gInSc4	den
poté	poté	k6eAd1	poté
ministr	ministr	k1gMnSc1	ministr
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc2	jeho
náměstek	náměstka	k1gFnPc2	náměstka
Rod	rod	k1gInSc1	rod
Rosenstein	Rosenstein	k1gMnSc1	Rosenstein
tuto	tento	k3xDgFnSc4	tento
zprávu	zpráva	k1gFnSc4	zpráva
vyhodnocovali	vyhodnocovat	k5eAaImAgMnP	vyhodnocovat
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
prosákl	prosáknout	k5eAaPmAgMnS	prosáknout
na	na	k7c4	na
veřejnost	veřejnost	k1gFnSc4	veřejnost
jen	jen	k6eAd1	jen
jeden	jeden	k4xCgInSc4	jeden
detail	detail	k1gInSc4	detail
<g/>
,	,	kIx,	,
že	že	k8xS	že
totiž	totiž	k9	totiž
Mueller	Mueller	k1gInSc1	Mueller
nemá	mít	k5eNaImIp3nS	mít
v	v	k7c6	v
úmyslu	úmysl	k1gInSc6	úmysl
pokračovat	pokračovat	k5eAaImF	pokračovat
ve	v	k7c6	v
vyšetřování	vyšetřování	k1gNnSc6	vyšetřování
a	a	k8xC	a
obžalobách	obžaloba	k1gFnPc6	obžaloba
dalších	další	k2eAgFnPc2d1	další
osob	osoba	k1gFnPc2	osoba
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
oněch	onen	k3xDgFnPc2	onen
30	[number]	k4	30
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
jeho	jeho	k3xOp3gInSc7	jeho
týmem	tým	k1gInSc7	tým
doposud	doposud	k6eAd1	doposud
podchyceny	podchytit	k5eAaPmNgFnP	podchytit
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
by	by	kYmCp3nS	by
tím	ten	k3xDgNnSc7	ten
bylo	být	k5eAaImAgNnS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
osoby	osoba	k1gFnPc1	osoba
blízké	blízký	k2eAgFnPc1d1	blízká
prezidentovi	prezident	k1gMnSc3	prezident
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gMnSc7	jeho
zeť	zeť	k1gMnSc1	zeť
Jared	Jared	k1gMnSc1	Jared
Kushner	Kushner	k1gMnSc1	Kushner
nebudou	být	k5eNaImBp3nP	být
obžalovány	obžalován	k2eAgInPc1d1	obžalován
<g/>
,	,	kIx,	,
vyvodili	vyvodit	k5eAaBmAgMnP	vyvodit
lidé	člověk	k1gMnPc1	člověk
kolem	kolem	k7c2	kolem
Trumpa	Trump	k1gMnSc2	Trump
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
detailu	detail	k1gInSc2	detail
závěr	závěr	k1gInSc1	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
zpráva	zpráva	k1gFnSc1	zpráva
přinesla	přinést	k5eAaPmAgFnS	přinést
pro	pro	k7c4	pro
Donalda	Donald	k1gMnSc4	Donald
Trumpa	Trump	k1gMnSc2	Trump
samotného	samotný	k2eAgMnSc2d1	samotný
pozitivní	pozitivní	k2eAgInSc4d1	pozitivní
výsledek	výsledek	k1gInSc4	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
Demokratická	demokratický	k2eAgFnSc1d1	demokratická
strana	strana	k1gFnSc1	strana
nemá	mít	k5eNaImIp3nS	mít
podle	podle	k7c2	podle
těchto	tento	k3xDgFnPc2	tento
úvah	úvaha	k1gFnPc2	úvaha
žádnou	žádný	k3yNgFnSc4	žádný
možnost	možnost	k1gFnSc4	možnost
<g/>
,	,	kIx,	,
prosadit	prosadit	k5eAaPmF	prosadit
zahájení	zahájení	k1gNnSc4	zahájení
procesu	proces	k1gInSc2	proces
proti	proti	k7c3	proti
prezidentovi	prezident	k1gMnSc3	prezident
zvaného	zvaný	k2eAgMnSc2d1	zvaný
impeachment	impeachment	k1gMnSc1	impeachment
<g/>
,	,	kIx,	,
natož	natož	k6eAd1	natož
aby	aby	kYmCp3nS	aby
takový	takový	k3xDgInSc1	takový
proces	proces	k1gInSc1	proces
skutečně	skutečně	k6eAd1	skutečně
vedl	vést	k5eAaImAgInS	vést
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
sesazení	sesazení	k1gNnSc3	sesazení
<g/>
.	.	kIx.	.
</s>
<s>
Chybí	chybět	k5eAaImIp3nS	chybět
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
především	především	k9	především
potřebná	potřebný	k2eAgFnSc1d1	potřebná
dvoutřetinová	dvoutřetinový	k2eAgFnSc1d1	dvoutřetinová
většina	většina	k1gFnSc1	většina
hlasů	hlas	k1gInPc2	hlas
v	v	k7c6	v
Senátu	senát	k1gInSc6	senát
USA	USA	kA	USA
<g/>
.	.	kIx.	.
<g/>
Během	během	k7c2	během
krátké	krátký	k2eAgFnSc2d1	krátká
návštěvy	návštěva	k1gFnSc2	návštěva
izraelského	izraelský	k2eAgMnSc2d1	izraelský
premiéra	premiér	k1gMnSc2	premiér
Benjamina	Benjamin	k1gMnSc2	Benjamin
Netanjahua	Netanjahuus	k1gMnSc2	Netanjahuus
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
podepsal	podepsat	k5eAaPmAgInS	podepsat
Trump	Trump	k1gInSc1	Trump
25	[number]	k4	25
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2019	[number]	k4	2019
dekret	dekret	k1gInSc4	dekret
<g/>
,	,	kIx,	,
kterým	který	k3yQgNnSc7	který
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
uznaly	uznat	k5eAaPmAgInP	uznat
izraelskou	izraelský	k2eAgFnSc4d1	izraelská
suverenitu	suverenita	k1gFnSc4	suverenita
nad	nad	k7c7	nad
Golanskými	Golanský	k2eAgFnPc7d1	Golanský
výšinami	výšina	k1gFnPc7	výšina
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
území	území	k1gNnSc4	území
Izrael	Izrael	k1gInSc4	Izrael
vojensky	vojensky	k6eAd1	vojensky
okupoval	okupovat	k5eAaBmAgMnS	okupovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
je	být	k5eAaImIp3nS	být
de	de	k?	de
facto	facto	k1gNnSc1	facto
anektoval	anektovat	k5eAaBmAgMnS	anektovat
<g/>
.	.	kIx.	.
</s>
<s>
Doposud	doposud	k6eAd1	doposud
nebyla	být	k5eNaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
anexe	anexe	k1gFnSc1	anexe
žádným	žádný	k3yNgInSc7	žádný
jiným	jiný	k2eAgInSc7d1	jiný
státem	stát	k1gInSc7	stát
uznána	uznat	k5eAaPmNgFnS	uznat
a	a	k8xC	a
Golany	Golana	k1gFnPc1	Golana
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
Organizací	organizace	k1gFnSc7	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
(	(	kIx(	(
<g/>
OSN	OSN	kA	OSN
<g/>
)	)	kIx)	)
nadále	nadále	k6eAd1	nadále
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
výsostné	výsostný	k2eAgNnSc4d1	výsostné
území	území	k1gNnSc4	území
Sýrie	Sýrie	k1gFnSc2	Sýrie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Komunikace	komunikace	k1gFnSc1	komunikace
skrze	skrze	k?	skrze
Twitter	Twitter	k1gInSc1	Twitter
==	==	k?	==
</s>
</p>
<p>
<s>
Donald	Donald	k1gMnSc1	Donald
Trump	Trump	k1gMnSc1	Trump
využívá	využívat	k5eAaImIp3nS	využívat
soukromý	soukromý	k2eAgInSc4d1	soukromý
twitterový	twitterový	k2eAgInSc4d1	twitterový
účet	účet	k1gInSc4	účet
@	@	kIx~	@
<g/>
realDonaldTrump	realDonaldTrump	k1gInSc1	realDonaldTrump
jako	jako	k8xS	jako
častý	častý	k2eAgInSc1d1	častý
prostředek	prostředek	k1gInSc1	prostředek
komunikace	komunikace	k1gFnSc2	komunikace
k	k	k7c3	k
vyjádření	vyjádření	k1gNnSc3	vyjádření
svých	svůj	k3xOyFgInPc2	svůj
pocitů	pocit	k1gInPc2	pocit
a	a	k8xC	a
politických	politický	k2eAgInPc2d1	politický
názorů	názor	k1gInPc2	názor
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
k	k	k7c3	k
oficiálním	oficiální	k2eAgFnPc3d1	oficiální
oznámením	oznámení	k1gNnSc7	oznámení
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
ke	k	k7c3	k
jmenování	jmenování	k1gNnSc3	jmenování
šéfa	šéf	k1gMnSc2	šéf
Fedu	Fedus	k1gInSc2	Fedus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
existuje	existovat	k5eAaImIp3nS	existovat
oficiální	oficiální	k2eAgInSc4d1	oficiální
prezidentský	prezidentský	k2eAgInSc4d1	prezidentský
účet	účet	k1gInSc4	účet
@	@	kIx~	@
<g/>
POTUS	POTUS	kA	POTUS
<g/>
.	.	kIx.	.
</s>
<s>
Trump	Trump	k1gMnSc1	Trump
si	se	k3xPyFc3	se
jej	on	k3xPp3gNnSc4	on
zřídil	zřídit	k5eAaPmAgInS	zřídit
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Tweety	Tweeta	k1gFnPc1	Tweeta
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
zdrojem	zdroj	k1gInSc7	zdroj
zpráv	zpráva	k1gFnPc2	zpráva
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgNnPc2	jenž
vycházejí	vycházet	k5eAaImIp3nP	vycházet
oficiální	oficiální	k2eAgNnPc1d1	oficiální
<g/>
,	,	kIx,	,
veřejnoprávní	veřejnoprávní	k2eAgNnPc1d1	veřejnoprávní
i	i	k8xC	i
komerční	komerční	k2eAgNnPc1d1	komerční
média	médium	k1gNnPc1	médium
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
přerostlo	přerůst	k5eAaPmAgNnS	přerůst
blokování	blokování	k1gNnSc1	blokování
uživatelů	uživatel	k1gMnPc2	uživatel
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
na	na	k7c6	na
Trumpově	Trumpův	k2eAgInSc6d1	Trumpův
soukromém	soukromý	k2eAgInSc6d1	soukromý
účtu	účet	k1gInSc6	účet
reagovali	reagovat	k5eAaBmAgMnP	reagovat
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
v	v	k7c4	v
civilně	civilně	k6eAd1	civilně
právní	právní	k2eAgFnPc4d1	právní
spory	spora	k1gFnPc4	spora
týkající	týkající	k2eAgFnPc4d1	týkající
se	se	k3xPyFc4	se
svobody	svoboda	k1gFnSc2	svoboda
projevu	projev	k1gInSc2	projev
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
řijnu	řijn	k1gInSc6	řijn
2017	[number]	k4	2017
k	k	k7c3	k
tomu	ten	k3xDgMnSc3	ten
argumentoval	argumentovat	k5eAaImAgMnS	argumentovat
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
Trumpův	Trumpův	k2eAgMnSc1d1	Trumpův
tiskový	tiskový	k2eAgMnSc1d1	tiskový
mluvčí	mluvčí	k1gMnSc1	mluvčí
Sean	Sean	k1gMnSc1	Sean
Spicer	Spicer	k1gMnSc1	Spicer
<g/>
,	,	kIx,	,
že	že	k8xS	že
Bílý	bílý	k2eAgInSc1d1	bílý
dům	dům	k1gInSc1	dům
považuje	považovat	k5eAaImIp3nS	považovat
všechny	všechen	k3xTgFnPc4	všechen
Trumpovy	Trumpův	k2eAgFnPc4d1	Trumpova
tweety	tweeta	k1gFnPc4	tweeta
za	za	k7c4	za
"	"	kIx"	"
<g/>
oficiální	oficiální	k2eAgNnSc4d1	oficiální
prohlášení	prohlášení	k1gNnSc4	prohlášení
prezidenta	prezident	k1gMnSc2	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Vybrané	vybraný	k2eAgFnPc1d1	vybraná
dvojice	dvojice	k1gFnPc1	dvojice
Trumpových	Trumpová	k1gFnPc2	Trumpová
názorově	názorově	k6eAd1	názorově
protichůdných	protichůdný	k2eAgInPc2d1	protichůdný
tweetů	tweet	k1gInPc2	tweet
některé	některý	k3yIgFnPc1	některý
firmy	firma	k1gFnPc1	firma
otiskly	otisknout	k5eAaPmAgFnP	otisknout
na	na	k7c6	na
pantofle	pantofle	k?	pantofle
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
flip-flops	fliplops	k1gInSc1	flip-flops
–	–	k?	–
označuje	označovat	k5eAaImIp3nS	označovat
jak	jak	k6eAd1	jak
žabky	žabka	k1gFnPc4	žabka
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
obrat	obrat	k1gInSc1	obrat
v	v	k7c6	v
názoru	názor	k1gInSc6	názor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Soukromé	soukromý	k2eAgInPc4d1	soukromý
tweety	tweet	k1gInPc4	tweet
prezidenta	prezident	k1gMnSc2	prezident
coby	coby	k?	coby
zdroj	zdroj	k1gInSc4	zdroj
žurnalistické	žurnalistický	k2eAgFnSc2d1	žurnalistická
zprávy	zpráva	k1gFnSc2	zpráva
jsou	být	k5eAaImIp3nP	být
novinka	novinka	k1gFnSc1	novinka
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
–	–	k?	–
ač	ač	k8xS	ač
technologicky	technologicky	k6eAd1	technologicky
mohl	moct	k5eAaImAgInS	moct
–	–	k?	–
nepraktikoval	praktikovat	k5eNaImAgInS	praktikovat
ani	ani	k8xC	ani
jeho	jeho	k3xOp3gMnSc1	jeho
předchůdce	předchůdce	k1gMnSc1	předchůdce
v	v	k7c6	v
úřadu	úřad	k1gInSc6	úřad
Barack	Barack	k1gMnSc1	Barack
Obama	Obama	k?	Obama
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kampaň	kampaň	k1gFnSc4	kampaň
pro	pro	k7c4	pro
znovuzvolení	znovuzvolení	k1gNnSc4	znovuzvolení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2020	[number]	k4	2020
==	==	k?	==
</s>
</p>
<p>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
úmysl	úmysl	k1gInSc4	úmysl
kandidovat	kandidovat	k5eAaImF	kandidovat
znovu	znovu	k6eAd1	znovu
v	v	k7c6	v
prezidentských	prezidentský	k2eAgFnPc6d1	prezidentská
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
2020	[number]	k4	2020
dal	dát	k5eAaPmAgInS	dát
Trump	Trump	k1gInSc1	Trump
najevo	najevo	k6eAd1	najevo
již	již	k6eAd1	již
několik	několik	k4yIc4	několik
hodin	hodina	k1gFnPc2	hodina
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgMnS	ujmout
úřadu	úřad	k1gInSc3	úřad
dne	den	k1gInSc2	den
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zapsal	zapsat	k5eAaPmAgMnS	zapsat
jako	jako	k9	jako
kandidát	kandidát	k1gMnSc1	kandidát
u	u	k7c2	u
Federální	federální	k2eAgFnSc2d1	federální
komise	komise	k1gFnSc2	komise
pro	pro	k7c4	pro
volby	volba	k1gFnPc4	volba
(	(	kIx(	(
<g/>
Federal	Federal	k1gFnSc1	Federal
Election	Election	k1gInSc1	Election
Commission	Commission	k1gInSc1	Commission
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
byl	být	k5eAaImAgInS	být
jeho	jeho	k3xOp3gInSc1	jeho
volební	volební	k2eAgInSc1d1	volební
výbor	výbor	k1gInSc1	výbor
pro	pro	k7c4	pro
rok	rok	k1gInSc4	rok
2016	[number]	k4	2016
přeměněn	přeměněn	k2eAgInSc4d1	přeměněn
na	na	k7c4	na
výbor	výbor	k1gInSc4	výbor
pro	pro	k7c4	pro
znovuzvolení	znovuzvolení	k1gNnSc4	znovuzvolení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2020	[number]	k4	2020
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1	oficiální
start	start	k1gInSc1	start
nové	nový	k2eAgFnSc2d1	nová
kampaně	kampaň	k1gFnSc2	kampaň
se	se	k3xPyFc4	se
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
na	na	k7c6	na
shromáždění	shromáždění	k1gNnSc6	shromáždění
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Melbourne	Melbourne	k1gNnSc2	Melbourne
na	na	k7c6	na
Floridě	Florida	k1gFnSc6	Florida
18	[number]	k4	18
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
než	než	k8xS	než
jeden	jeden	k4xCgInSc4	jeden
měsíc	měsíc	k1gInSc4	měsíc
po	po	k7c6	po
Trumpově	Trumpův	k2eAgFnSc6d1	Trumpova
inauguraci	inaugurace	k1gFnSc6	inaugurace
jako	jako	k9	jako
prezident	prezident	k1gMnSc1	prezident
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2018	[number]	k4	2018
měl	mít	k5eAaImAgInS	mít
Trumpův	Trumpův	k2eAgInSc1d1	Trumpův
výbor	výbor	k1gInSc1	výbor
pro	pro	k7c4	pro
znovuzvolení	znovuzvolení	k1gNnSc4	znovuzvolení
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
již	již	k6eAd1	již
22,1	[number]	k4	22,1
milionu	milion	k4xCgInSc2	milion
dolarů	dolar	k1gInPc2	dolar
a	a	k8xC	a
do	do	k7c2	do
konce	konec	k1gInSc2	konec
června	červen	k1gInSc2	červen
2018	[number]	k4	2018
vybral	vybrat	k5eAaPmAgMnS	vybrat
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
kampaň	kampaň	k1gFnSc4	kampaň
celkově	celkově	k6eAd1	celkově
50	[number]	k4	50
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kontroverze	kontroverze	k1gFnSc1	kontroverze
a	a	k8xC	a
kritika	kritika	k1gFnSc1	kritika
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Vztah	vztah	k1gInSc1	vztah
k	k	k7c3	k
ženám	žena	k1gFnPc3	žena
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
svého	svůj	k3xOyFgInSc2	svůj
mediálního	mediální	k2eAgInSc2d1	mediální
života	život	k1gInSc2	život
se	se	k3xPyFc4	se
Donald	Donald	k1gMnSc1	Donald
Trump	Trump	k1gMnSc1	Trump
dle	dle	k7c2	dle
kritiků	kritik	k1gMnPc2	kritik
několikrát	několikrát	k6eAd1	několikrát
nevhodně	vhodně	k6eNd1	vhodně
vyjadřoval	vyjadřovat	k5eAaImAgMnS	vyjadřovat
o	o	k7c6	o
ženách	žena	k1gFnPc6	žena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
Howarda	Howard	k1gMnSc2	Howard
Sterna	sternum	k1gNnSc2	sternum
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Howard	Howard	k1gInSc1	Howard
Stern	sternum	k1gNnPc2	sternum
Show	show	k1gFnSc2	show
<g/>
"	"	kIx"	"
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
hodnotil	hodnotit	k5eAaImAgInS	hodnotit
ženy	žena	k1gFnSc2	žena
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
od	od	k7c2	od
1	[number]	k4	1
do	do	k7c2	do
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
výroky	výrok	k1gInPc1	výrok
nebyly	být	k5eNaImAgInP	být
jen	jen	k9	jen
obecné	obecný	k2eAgInPc1d1	obecný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
směřovaly	směřovat	k5eAaImAgFnP	směřovat
také	také	k9	také
na	na	k7c4	na
konkrétní	konkrétní	k2eAgFnPc4d1	konkrétní
ženy	žena	k1gFnPc4	žena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
nazval	nazvat	k5eAaPmAgMnS	nazvat
bývalou	bývalý	k2eAgFnSc4d1	bývalá
Miss	miss	k1gFnSc4	miss
Universe	Universe	k1gFnSc2	Universe
Aliciu	Alicium	k1gNnSc3	Alicium
Machado	Machada	k1gFnSc5	Machada
"	"	kIx"	"
<g/>
Miss	miss	k1gFnSc1	miss
Piggy	Pigga	k1gFnSc2	Pigga
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Miss	miss	k1gFnSc1	miss
prasátko	prasátko	k1gNnSc4	prasátko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
také	také	k9	také
"	"	kIx"	"
<g/>
eating	eating	k1gInSc1	eating
machine	machinout	k5eAaPmIp3nS	machinout
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
žrací	žracet	k5eAaImIp3nS	žracet
stroj	stroj	k1gInSc1	stroj
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
Miss	miss	k1gFnSc1	miss
Housekeeping	Housekeeping	k1gInSc1	Housekeeping
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Miss	miss	k1gFnSc1	miss
uklízení	uklízení	k1gNnSc2	uklízení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Donald	Donald	k1gMnSc1	Donald
Trump	Trump	k1gMnSc1	Trump
při	při	k7c6	při
různých	různý	k2eAgFnPc6d1	různá
příležitostech	příležitost	k1gFnPc6	příležitost
slovně	slovně	k6eAd1	slovně
napadl	napadnout	k5eAaPmAgMnS	napadnout
americkou	americký	k2eAgFnSc4d1	americká
herečku	herečka	k1gFnSc4	herečka
Rosie	Rosi	k1gMnPc4	Rosi
O	O	kA	O
<g/>
'	'	kIx"	'
<g/>
Donnell	Donnell	k1gMnSc1	Donnell
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
během	během	k7c2	během
debaty	debata	k1gFnSc2	debata
Republikánské	republikánský	k2eAgFnSc2d1	republikánská
strany	strana	k1gFnSc2	strana
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
<g/>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
boje	boj	k1gInSc2	boj
o	o	k7c4	o
prezidentský	prezidentský	k2eAgInSc4d1	prezidentský
post	post	k1gInSc4	post
<g/>
,	,	kIx,	,
zveřejnil	zveřejnit	k5eAaPmAgInS	zveřejnit
deník	deník	k1gInSc4	deník
The	The	k1gMnPc2	The
Washington	Washington	k1gInSc1	Washington
Post	posta	k1gFnPc2	posta
video	video	k1gNnSc1	video
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Video	video	k1gNnSc1	video
začíná	začínat	k5eAaImIp3nS	začínat
záběrem	záběr	k1gInSc7	záběr
přijíždějícího	přijíždějící	k2eAgInSc2d1	přijíždějící
autobusu	autobus	k1gInSc2	autobus
spojeným	spojený	k2eAgNnSc7d1	spojené
se	s	k7c7	s
zvukovou	zvukový	k2eAgFnSc7d1	zvuková
stopou	stopa	k1gFnSc7	stopa
rozhovoru	rozhovor	k1gInSc2	rozhovor
Donalda	Donald	k1gMnSc2	Donald
Trumpa	Trump	k1gMnSc2	Trump
s	s	k7c7	s
tehdejším	tehdejší	k2eAgInSc7d1	tehdejší
televizním	televizní	k2eAgInSc7d1	televizní
moderátorem	moderátor	k1gInSc7	moderátor
Billym	Billym	k1gInSc1	Billym
Bushem	Bush	k1gMnSc7	Bush
uvnitř	uvnitř	k7c2	uvnitř
autobusu	autobus	k1gInSc2	autobus
a	a	k8xC	a
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
rozhovorem	rozhovor	k1gInSc7	rozhovor
Trumpa	Trump	k1gMnSc2	Trump
<g/>
,	,	kIx,	,
Bushe	Bush	k1gMnSc2	Bush
a	a	k8xC	a
herečky	herečka	k1gFnPc1	herečka
Arianne	Ariann	k1gInSc5	Ariann
Zucker	Zuckra	k1gFnPc2	Zuckra
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
společnosti	společnost	k1gFnSc2	společnost
NBC	NBC	kA	NBC
Universal	Universal	k1gMnSc2	Universal
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
s	s	k7c7	s
Bushem	Bush	k1gMnSc7	Bush
se	se	k3xPyFc4	se
Trump	Trump	k1gMnSc1	Trump
nevhodně	vhodně	k6eNd1	vhodně
vyjadřoval	vyjadřovat	k5eAaImAgMnS	vyjadřovat
o	o	k7c6	o
ženách	žena	k1gFnPc6	žena
a	a	k8xC	a
hovořil	hovořit	k5eAaImAgMnS	hovořit
také	také	k9	také
o	o	k7c6	o
sexuálních	sexuální	k2eAgInPc6d1	sexuální
útocích	útok	k1gInPc6	útok
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterým	který	k3yRgFnPc3	který
se	se	k3xPyFc4	se
přiznal	přiznat	k5eAaPmAgMnS	přiznat
<g/>
.	.	kIx.	.
</s>
<s>
Donald	Donald	k1gMnSc1	Donald
Trump	Trump	k1gMnSc1	Trump
netušil	tušit	k5eNaImAgMnS	tušit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mikrofon	mikrofon	k1gInSc1	mikrofon
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
s	s	k7c7	s
jeho	jeho	k3xOp3gNnSc7	jeho
vědomím	vědomí	k1gNnSc7	vědomí
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
umístěn	umístit	k5eAaPmNgInS	umístit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
aktivní	aktivní	k2eAgMnSc1d1	aktivní
<g/>
.	.	kIx.	.
</s>
<s>
Konkrétně	konkrétně	k6eAd1	konkrétně
promluvil	promluvit	k5eAaPmAgMnS	promluvit
o	o	k7c4	o
osahávání	osahávání	k1gNnSc4	osahávání
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
kterých	který	k3yQgMnPc2	který
se	se	k3xPyFc4	se
dopouštěl	dopouštět	k5eAaImAgMnS	dopouštět
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
mu	on	k3xPp3gInSc3	on
to	ten	k3xDgNnSc1	ten
vždy	vždy	k6eAd1	vždy
prošlo	projít	k5eAaPmAgNnS	projít
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
slavný	slavný	k2eAgMnSc1d1	slavný
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tyto	tento	k3xDgInPc4	tento
výroky	výrok	k1gInPc4	výrok
se	se	k3xPyFc4	se
Trump	Trump	k1gInSc1	Trump
po	po	k7c6	po
jejich	jejich	k3xOp3gNnSc6	jejich
zveřejnění	zveřejnění	k1gNnSc6	zveřejnění
omluvil	omluvit	k5eAaPmAgMnS	omluvit
<g/>
;	;	kIx,	;
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
od	od	k7c2	od
něj	on	k3xPp3gMnSc2	on
ale	ale	k8xC	ale
odešlo	odejít	k5eAaPmAgNnS	odejít
mnoho	mnoho	k4c1	mnoho
jeho	jeho	k3xOp3gMnPc2	jeho
příznivců	příznivec	k1gMnPc2	příznivec
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
několik	několik	k4yIc1	několik
žen	žena	k1gFnPc2	žena
potvrdilo	potvrdit	k5eAaPmAgNnS	potvrdit
jeho	jeho	k3xOp3gMnPc3	jeho
vlastní	vlastní	k2eAgNnPc4d1	vlastní
slova	slovo	k1gNnPc4	slovo
s	s	k7c7	s
tvrzením	tvrzení	k1gNnSc7	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
ony	onen	k3xDgInPc1	onen
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterých	který	k3yIgInPc6	který
Trump	Trump	k1gInSc1	Trump
na	na	k7c6	na
nahrávce	nahrávka	k1gFnSc6	nahrávka
mluví	mluvit	k5eAaImIp3nS	mluvit
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
žen	žena	k1gFnPc2	žena
byla	být	k5eAaImAgFnS	být
Natasha	Natasha	k1gFnSc1	Natasha
Stoynoff	Stoynoff	k1gInSc4	Stoynoff
z	z	k7c2	z
magazínu	magazín	k1gInSc2	magazín
People	People	k1gMnPc2	People
<g/>
,	,	kIx,	,
její	její	k3xOp3gMnPc4	její
tvrzení	tvrzení	k1gNnSc1	tvrzení
později	pozdě	k6eAd2	pozdě
potvrdilo	potvrdit	k5eAaPmAgNnS	potvrdit
šest	šest	k4xCc1	šest
jejích	její	k3xOp3gMnPc2	její
kolegů	kolega	k1gMnPc2	kolega
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
ještě	ještě	k9	ještě
další	další	k2eAgFnPc1d1	další
audio	audio	k2eAgFnPc1d1	audio
nahrávky	nahrávka	k1gFnPc1	nahrávka
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yQgInPc6	který
Trump	Trump	k1gInSc1	Trump
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
ženách	žena	k1gFnPc6	žena
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jako	jako	k8xS	jako
ředitel	ředitel	k1gMnSc1	ředitel
soutěže	soutěž	k1gFnSc2	soutěž
"	"	kIx"	"
<g/>
Miss	miss	k1gFnSc1	miss
Teen	Teen	k1gMnSc1	Teen
USA	USA	kA	USA
<g/>
"	"	kIx"	"
mohl	moct	k5eAaImAgMnS	moct
chodit	chodit	k5eAaImF	chodit
do	do	k7c2	do
šaten	šatna	k1gFnPc2	šatna
dívek	dívka	k1gFnPc2	dívka
a	a	k8xC	a
mohl	moct	k5eAaImAgMnS	moct
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
vidět	vidět	k5eAaImF	vidět
nahé	nahý	k2eAgNnSc4d1	nahé
<g/>
.	.	kIx.	.
</s>
<s>
Vždy	vždy	k6eAd1	vždy
dívkám	dívka	k1gFnPc3	dívka
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
inspekci	inspekce	k1gFnSc4	inspekce
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
původních	původní	k2eAgMnPc2d1	původní
soutěžících	soutěžící	k1gMnPc2	soutěžící
toto	tento	k3xDgNnSc1	tento
jeho	jeho	k3xOp3gNnSc1	jeho
prohlášení	prohlášení	k1gNnSc1	prohlášení
označilo	označit	k5eAaPmAgNnS	označit
za	za	k7c4	za
pravdivé	pravdivý	k2eAgFnPc4d1	pravdivá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Svědectví	svědectví	k1gNnPc2	svědectví
bývalých	bývalý	k2eAgMnPc2d1	bývalý
spolupracovníků	spolupracovník	k1gMnPc2	spolupracovník
===	===	k?	===
</s>
</p>
<p>
<s>
Z	z	k7c2	z
výslechu	výslech	k1gInSc2	výslech
bývalých	bývalý	k2eAgMnPc2d1	bývalý
Trumpových	Trumpův	k2eAgMnPc2d1	Trumpův
spolupracovníků	spolupracovník	k1gMnPc2	spolupracovník
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
byli	být	k5eAaImAgMnP	být
zvláštním	zvláštní	k2eAgNnSc7d1	zvláštní
vyšetřovatelem	vyšetřovatel	k1gMnSc7	vyšetřovatel
Robertem	Robert	k1gMnSc7	Robert
Muellerem	Mueller	k1gMnSc7	Mueller
obviněni	obvinit	k5eAaPmNgMnP	obvinit
z	z	k7c2	z
federálních	federální	k2eAgInPc2d1	federální
zločinů	zločin	k1gInPc2	zločin
<g/>
,	,	kIx,	,
vyplývají	vyplývat	k5eAaImIp3nP	vyplývat
některé	některý	k3yIgFnPc4	některý
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
Trumpově	Trumpův	k2eAgFnSc6d1	Trumpova
povaze	povaha	k1gFnSc6	povaha
a	a	k8xC	a
údajných	údajný	k2eAgInPc6d1	údajný
podvodech	podvod	k1gInPc6	podvod
<g/>
,	,	kIx,	,
kterých	který	k3yIgMnPc2	který
se	se	k3xPyFc4	se
dopustil	dopustit	k5eAaPmAgMnS	dopustit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
bývalý	bývalý	k2eAgMnSc1d1	bývalý
právník	právník	k1gMnSc1	právník
Michael	Michael	k1gMnSc1	Michael
Cohen	Cohen	k1gInSc4	Cohen
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Trump	Trump	k1gInSc1	Trump
zakoupil	zakoupit	k5eAaPmAgInS	zakoupit
celkem	celkem	k6eAd1	celkem
tři	tři	k4xCgNnPc4	tři
své	svůj	k3xOyFgInPc4	svůj
malířské	malířský	k2eAgInPc4d1	malířský
portréty	portrét	k1gInPc4	portrét
z	z	k7c2	z
peněz	peníze	k1gInPc2	peníze
vlastní	vlastní	k2eAgFnSc2d1	vlastní
charitativní	charitativní	k2eAgFnSc2d1	charitativní
nadace	nadace	k1gFnSc2	nadace
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
poslední	poslední	k2eAgFnSc4d1	poslední
koupi	koupě	k1gFnSc4	koupě
najal	najmout	k5eAaPmAgMnS	najmout
nastrčeného	nastrčený	k2eAgMnSc4d1	nastrčený
dražitele	dražitel	k1gMnSc4	dražitel
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
měl	mít	k5eAaImAgInS	mít
cenu	cena	k1gFnSc4	cena
vyšroubovat	vyšroubovat	k5eAaPmF	vyšroubovat
na	na	k7c4	na
nejvyšší	vysoký	k2eAgNnPc4d3	nejvyšší
ze	z	k7c2	z
všech	všecek	k3xTgNnPc2	všecek
dražených	dražený	k2eAgNnPc2d1	dražené
děl	dělo	k1gNnPc2	dělo
a	a	k8xC	a
pak	pak	k6eAd1	pak
mu	on	k3xPp3gNnSc3	on
celkem	celkem	k6eAd1	celkem
60	[number]	k4	60
000	[number]	k4	000
dolarů	dolar	k1gInPc2	dolar
proplatil	proplatit	k5eAaPmAgMnS	proplatit
z	z	k7c2	z
prostředků	prostředek	k1gInPc2	prostředek
nadace	nadace	k1gFnSc2	nadace
<g/>
.	.	kIx.	.
</s>
<s>
Portrét	portrét	k1gInSc1	portrét
nyní	nyní	k6eAd1	nyní
visí	viset	k5eAaImIp3nS	viset
v	v	k7c6	v
Trumpově	Trumpův	k2eAgInSc6d1	Trumpův
venkovském	venkovský	k2eAgInSc6d1	venkovský
klubu	klub	k1gInSc6	klub
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Negativní	negativní	k2eAgFnPc1d1	negativní
a	a	k8xC	a
falešné	falešný	k2eAgFnPc1d1	falešná
zprávy	zpráva	k1gFnPc1	zpráva
v	v	k7c6	v
médiích	médium	k1gNnPc6	médium
===	===	k?	===
</s>
</p>
<p>
<s>
Studie	studie	k1gFnSc1	studie
Media	medium	k1gNnSc2	medium
Research	Researcha	k1gFnPc2	Researcha
Center	centrum	k1gNnPc2	centrum
zjistila	zjistit	k5eAaPmAgFnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
v	v	k7c6	v
době	doba	k1gFnSc6	doba
předvolební	předvolební	k2eAgFnSc2d1	předvolební
kampaně	kampaň	k1gFnSc2	kampaň
věnovaly	věnovat	k5eAaPmAgFnP	věnovat
americké	americký	k2eAgFnPc1d1	americká
televizní	televizní	k2eAgFnPc1d1	televizní
stanice	stanice	k1gFnPc1	stanice
ABC	ABC	kA	ABC
<g/>
,	,	kIx,	,
CBS	CBS	kA	CBS
a	a	k8xC	a
NBC	NBC	kA	NBC
mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
prostoru	prostora	k1gFnSc4	prostora
Trumpovi	Trump	k1gMnSc3	Trump
než	než	k8xS	než
jeho	jeho	k3xOp3gFnSc3	jeho
soupeřce	soupeřka	k1gFnSc3	soupeřka
Hillary	Hillara	k1gFnSc2	Hillara
Clintové	Clintový	k2eAgFnPc1d1	Clintový
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
celých	celý	k2eAgNnPc2d1	celé
91	[number]	k4	91
%	%	kIx~	%
mediálních	mediální	k2eAgInPc2d1	mediální
výstupů	výstup	k1gInPc2	výstup
o	o	k7c6	o
Trumpovi	Trump	k1gMnSc6	Trump
bylo	být	k5eAaImAgNnS	být
negativních	negativní	k2eAgNnPc2d1	negativní
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
studie	studie	k1gFnSc2	studie
Pew	Pew	k1gMnSc1	Pew
Research	Research	k1gMnSc1	Research
Center	centrum	k1gNnPc2	centrum
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
bylo	být	k5eAaImAgNnS	být
pouze	pouze	k6eAd1	pouze
5	[number]	k4	5
%	%	kIx~	%
zpráv	zpráva	k1gFnPc2	zpráva
o	o	k7c6	o
prezidentu	prezident	k1gMnSc6	prezident
Trumpovi	Trump	k1gMnSc6	Trump
pozitivních	pozitivní	k2eAgFnPc2d1	pozitivní
<g/>
,	,	kIx,	,
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
se	s	k7c7	s
42	[number]	k4	42
%	%	kIx~	%
pozitivních	pozitivní	k2eAgFnPc2d1	pozitivní
zpráv	zpráva	k1gFnPc2	zpráva
o	o	k7c6	o
prezidentu	prezident	k1gMnSc6	prezident
Obamovi	Obam	k1gMnSc6	Obam
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
a	a	k8xC	a
média	médium	k1gNnSc2	médium
se	se	k3xPyFc4	se
zaměřovala	zaměřovat	k5eAaImAgFnS	zaměřovat
více	hodně	k6eAd2	hodně
na	na	k7c4	na
Trumpovu	Trumpův	k2eAgFnSc4d1	Trumpova
povahu	povaha	k1gFnSc4	povaha
než	než	k8xS	než
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
politiku	politika	k1gFnSc4	politika
<g/>
.	.	kIx.	.
</s>
<s>
Studie	studie	k1gFnSc1	studie
Harvardovy	Harvardův	k2eAgFnSc2d1	Harvardova
univerzity	univerzita	k1gFnSc2	univerzita
zjistila	zjistit	k5eAaPmAgFnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
během	během	k7c2	během
prvních	první	k4xOgInPc2	první
100	[number]	k4	100
dní	den	k1gInPc2	den
Trumpa	Trumpa	k1gFnSc1	Trumpa
v	v	k7c6	v
úřadu	úřad	k1gInSc6	úřad
amerického	americký	k2eAgMnSc2d1	americký
prezidenta	prezident	k1gMnSc2	prezident
bylo	být	k5eAaImAgNnS	být
o	o	k7c6	o
Trumpovi	Trump	k1gMnSc6	Trump
negativních	negativní	k2eAgInPc2d1	negativní
93	[number]	k4	93
%	%	kIx~	%
zpráv	zpráva	k1gFnPc2	zpráva
CNN	CNN	kA	CNN
a	a	k8xC	a
NBC	NBC	kA	NBC
<g/>
,	,	kIx,	,
91	[number]	k4	91
%	%	kIx~	%
zpravodajství	zpravodajství	k1gNnSc1	zpravodajství
CBS	CBS	kA	CBS
<g/>
,	,	kIx,	,
87	[number]	k4	87
%	%	kIx~	%
zpráv	zpráva	k1gFnPc2	zpráva
New	New	k1gMnPc2	New
York	York	k1gInSc1	York
Times	Timesa	k1gFnPc2	Timesa
a	a	k8xC	a
83	[number]	k4	83
%	%	kIx~	%
zpráv	zpráva	k1gFnPc2	zpráva
Washington	Washington	k1gInSc1	Washington
Post	posta	k1gFnPc2	posta
<g/>
,	,	kIx,	,
70	[number]	k4	70
%	%	kIx~	%
zpravodajství	zpravodajství	k1gNnSc2	zpravodajství
The	The	k1gFnSc2	The
Wall	Wall	k1gMnSc1	Wall
Street	Street	k1gMnSc1	Street
Journal	Journal	k1gMnSc1	Journal
a	a	k8xC	a
52	[number]	k4	52
%	%	kIx~	%
zpráv	zpráva	k1gFnPc2	zpráva
Fox	fox	k1gInSc1	fox
News	News	k1gInSc1	News
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
studie	studie	k1gFnSc2	studie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
bylo	být	k5eAaImAgNnS	být
stále	stále	k6eAd1	stále
přes	přes	k7c4	přes
90	[number]	k4	90
%	%	kIx~	%
mediálních	mediální	k2eAgFnPc2d1	mediální
zpráv	zpráva	k1gFnPc2	zpráva
o	o	k7c6	o
Trumpovi	Trump	k1gMnSc6	Trump
negativních	negativní	k2eAgInPc2d1	negativní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
a	a	k8xC	a
únoru	únor	k1gInSc6	únor
2018	[number]	k4	2018
se	s	k7c7	s
63	[number]	k4	63
%	%	kIx~	%
zpráv	zpráva	k1gFnPc2	zpráva
o	o	k7c4	o
Trumpovi	Trumpův	k2eAgMnPc1d1	Trumpův
věnovalo	věnovat	k5eAaPmAgNnS	věnovat
skandálům	skandál	k1gInPc3	skandál
<g/>
,	,	kIx,	,
např.	např.	kA	např.
vyšetřování	vyšetřování	k1gNnSc2	vyšetřování
FBI	FBI	kA	FBI
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Roberta	Robert	k1gMnSc2	Robert
Muellera	Mueller	k1gMnSc2	Mueller
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
objevit	objevit	k5eAaPmF	objevit
a	a	k8xC	a
rozkrýt	rozkrýt	k5eAaPmF	rozkrýt
možné	možný	k2eAgFnPc4d1	možná
vazby	vazba	k1gFnPc4	vazba
prezidenta	prezident	k1gMnSc2	prezident
Trumpa	Trump	k1gMnSc2	Trump
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc2	jeho
spolupracovníků	spolupracovník	k1gMnPc2	spolupracovník
na	na	k7c4	na
Rusko	Rusko	k1gNnSc4	Rusko
<g/>
,	,	kIx,	,
Izrael	Izrael	k1gInSc4	Izrael
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
země	zem	k1gFnPc4	zem
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
skandálu	skandál	k1gInSc2	skandál
kolem	kolem	k7c2	kolem
<g />
.	.	kIx.	.
</s>
<s>
údajného	údajný	k2eAgInSc2d1	údajný
sexuálního	sexuální	k2eAgInSc2d1	sexuální
vztahu	vztah	k1gInSc2	vztah
Trumpa	Trumpa	k1gFnSc1	Trumpa
s	s	k7c7	s
pornoherečkou	pornoherečka	k1gFnSc7	pornoherečka
Stormy	Storma	k1gFnSc2	Storma
Daniels	Danielsa	k1gFnPc2	Danielsa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
a	a	k8xC	a
pouze	pouze	k6eAd1	pouze
37	[number]	k4	37
%	%	kIx~	%
zpráv	zpráva	k1gFnPc2	zpráva
se	se	k3xPyFc4	se
věnovalo	věnovat	k5eAaPmAgNnS	věnovat
politice	politika	k1gFnSc3	politika
Trumpovy	Trumpův	k2eAgFnSc2d1	Trumpova
administrativy	administrativa	k1gFnSc2	administrativa
<g/>
.	.	kIx.	.
<g/>
Trump	Trump	k1gMnSc1	Trump
opakovaně	opakovaně	k6eAd1	opakovaně
obvinil	obvinit	k5eAaPmAgMnS	obvinit
některá	některý	k3yIgNnPc4	některý
americká	americký	k2eAgNnPc4d1	americké
média	médium	k1gNnPc4	médium
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
televizní	televizní	k2eAgFnSc1d1	televizní
stanice	stanice	k1gFnSc1	stanice
CNN	CNN	kA	CNN
a	a	k8xC	a
ABC	ABC	kA	ABC
nebo	nebo	k8xC	nebo
deníky	deník	k1gInPc1	deník
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
Times	Times	k1gInSc1	Times
a	a	k8xC	a
Washington	Washington	k1gInSc1	Washington
Post	posta	k1gFnPc2	posta
<g/>
,	,	kIx,	,
z	z	k7c2	z
šíření	šíření	k1gNnSc2	šíření
fake	fak	k1gInSc2	fak
news	newsa	k1gFnPc2	newsa
a	a	k8xC	a
dezinformací	dezinformace	k1gFnPc2	dezinformace
<g/>
.	.	kIx.	.
</s>
<s>
Amerického	americký	k2eAgMnSc2d1	americký
prezidenta	prezident	k1gMnSc2	prezident
podpořil	podpořit	k5eAaPmAgMnS	podpořit
polský	polský	k2eAgMnSc1d1	polský
prezident	prezident	k1gMnSc1	prezident
Andrzej	Andrzej	k1gMnSc1	Andrzej
Duda	Duda	k1gMnSc1	Duda
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
napsal	napsat	k5eAaBmAgMnS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Musíme	muset	k5eAaImIp1nP	muset
nadále	nadále	k6eAd1	nadále
bojovat	bojovat	k5eAaImF	bojovat
proti	proti	k7c3	proti
tomuto	tento	k3xDgInSc3	tento
jevu	jev	k1gInSc3	jev
<g/>
.	.	kIx.	.
</s>
<s>
Polsko	Polsko	k1gNnSc1	Polsko
zažívá	zažívat	k5eAaImIp3nS	zažívat
moc	moc	k6eAd1	moc
falešných	falešný	k2eAgFnPc2d1	falešná
zpráv	zpráva	k1gFnPc2	zpráva
z	z	k7c2	z
první	první	k4xOgFnSc2	první
ruky	ruka	k1gFnSc2	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
evropských	evropský	k2eAgFnPc2d1	Evropská
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k9	i
amerických	americký	k2eAgMnPc2d1	americký
úředníků	úředník	k1gMnPc2	úředník
utváří	utvářet	k5eAaImIp3nS	utvářet
své	svůj	k3xOyFgInPc4	svůj
názory	názor	k1gInPc4	názor
na	na	k7c4	na
Polsko	Polsko	k1gNnSc4	Polsko
na	na	k7c6	na
základě	základ	k1gInSc6	základ
neúprosného	úprosný	k2eNgInSc2d1	neúprosný
toku	tok	k1gInSc2	tok
falešných	falešný	k2eAgFnPc2d1	falešná
zpráv	zpráva	k1gFnPc2	zpráva
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2018	[number]	k4	2018
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
Trumpova	Trumpův	k2eAgFnSc1d1	Trumpova
vláda	vláda	k1gFnSc1	vláda
politiku	politika	k1gFnSc4	politika
nulové	nulový	k2eAgFnSc2d1	nulová
tolerance	tolerance	k1gFnSc2	tolerance
vůči	vůči	k7c3	vůči
ilegální	ilegální	k2eAgFnSc3d1	ilegální
migraci	migrace	k1gFnSc3	migrace
a	a	k8xC	a
došlo	dojít	k5eAaPmAgNnS	dojít
také	také	k9	také
k	k	k7c3	k
oddělování	oddělování	k1gNnSc3	oddělování
dětí	dítě	k1gFnPc2	dítě
ilegálních	ilegální	k2eAgMnPc2d1	ilegální
imigrantů	imigrant	k1gMnPc2	imigrant
od	od	k7c2	od
jejich	jejich	k3xOp3gMnPc2	jejich
rodičů	rodič	k1gMnPc2	rodič
na	na	k7c6	na
americké	americký	k2eAgFnSc6d1	americká
hranici	hranice	k1gFnSc6	hranice
s	s	k7c7	s
Mexikem	Mexiko	k1gNnSc7	Mexiko
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yQnSc4	což
se	se	k3xPyFc4	se
Trump	Trump	k1gInSc4	Trump
ocitl	ocitnout	k5eAaPmAgInS	ocitnout
pod	pod	k7c7	pod
palbou	palba	k1gFnSc7	palba
kritiky	kritika	k1gFnSc2	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
sociálních	sociální	k2eAgInPc6d1	sociální
sitích	sit	k1gInPc6	sit
se	se	k3xPyFc4	se
šířila	šířit	k5eAaImAgFnS	šířit
fotografie	fotografie	k1gFnSc1	fotografie
nedospělých	dospělý	k2eNgMnPc2d1	nedospělý
imigrantů	imigrant	k1gMnPc2	imigrant
umístěných	umístěný	k2eAgMnPc2d1	umístěný
do	do	k7c2	do
drátěné	drátěný	k2eAgFnSc2d1	drátěná
klece	klec	k1gFnSc2	klec
jako	jako	k8xS	jako
důkaz	důkaz	k1gInSc1	důkaz
nelidskosti	nelidskost	k1gFnSc2	nelidskost
Trumpovy	Trumpův	k2eAgFnSc2d1	Trumpova
imigrační	imigrační	k2eAgFnSc2d1	imigrační
politiky	politika	k1gFnSc2	politika
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
ale	ale	k8xC	ale
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
fotka	fotka	k1gFnSc1	fotka
byla	být	k5eAaImAgFnS	být
pořízena	pořídit	k5eAaPmNgFnS	pořídit
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Baracka	Baracko	k1gNnSc2	Baracko
Obamy	Obama	k1gFnSc2	Obama
<g/>
.	.	kIx.	.
</s>
<s>
Trump	Trump	k1gInSc1	Trump
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
reagoval	reagovat	k5eAaBmAgMnS	reagovat
na	na	k7c6	na
twitteru	twitter	k1gInSc6	twitter
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Demokraté	demokrat	k1gMnPc1	demokrat
chybně	chybně	k6eAd1	chybně
tweetovali	tweetovat	k5eAaPmAgMnP	tweetovat
obrázek	obrázek	k1gInSc4	obrázek
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vládl	vládnout	k5eAaImAgInS	vládnout
Obama	Obama	k?	Obama
<g/>
,	,	kIx,	,
zobrazující	zobrazující	k2eAgFnPc1d1	zobrazující
děti	dítě	k1gFnPc1	dítě
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
v	v	k7c6	v
drátěných	drátěný	k2eAgFnPc6d1	drátěná
klecích	klec	k1gFnPc6	klec
<g/>
.	.	kIx.	.
</s>
<s>
Mysleli	myslet	k5eAaImAgMnP	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
nové	nový	k2eAgFnPc1d1	nová
fotky	fotka	k1gFnPc1	fotka
a	a	k8xC	a
chtěli	chtít	k5eAaImAgMnP	chtít
nás	my	k3xPp1nPc4	my
zostudit	zostudit	k5eAaPmF	zostudit
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
obrátilo	obrátit	k5eAaPmAgNnS	obrátit
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
proti	proti	k7c3	proti
nim	on	k3xPp3gNnPc3	on
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Na	na	k7c6	na
titulní	titulní	k2eAgFnSc6d1	titulní
straně	strana	k1gFnSc6	strana
časopisu	časopis	k1gInSc2	časopis
Time	Tim	k1gInSc2	Tim
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
fotomontáž	fotomontáž	k1gFnSc1	fotomontáž
fotografie	fotografia	k1gFnSc2	fotografia
s	s	k7c7	s
Trumpem	Trump	k1gInSc7	Trump
a	a	k8xC	a
proti	proti	k7c3	proti
němu	on	k3xPp3gMnSc3	on
stojící	stojící	k2eAgFnSc1d1	stojící
plačící	plačící	k2eAgFnSc7d1	plačící
holčičkou	holčička	k1gFnSc7	holčička
z	z	k7c2	z
Hondurasu	Honduras	k1gInSc2	Honduras
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
podle	podle	k7c2	podle
zpráv	zpráva	k1gFnPc2	zpráva
v	v	k7c6	v
médiích	médium	k1gNnPc6	médium
oddělená	oddělený	k2eAgFnSc1d1	oddělená
od	od	k7c2	od
své	svůj	k3xOyFgFnSc2	svůj
matky	matka	k1gFnSc2	matka
a	a	k8xC	a
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
fotografie	fotografie	k1gFnSc1	fotografie
obletěla	obletět	k5eAaPmAgFnS	obletět
celý	celý	k2eAgInSc4d1	celý
svět	svět	k1gInSc4	svět
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
ale	ale	k8xC	ale
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
později	pozdě	k6eAd2	pozdě
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
holčička	holčička	k1gFnSc1	holčička
od	od	k7c2	od
matky	matka	k1gFnSc2	matka
oddělená	oddělený	k2eAgFnSc1d1	oddělená
nebyla	být	k5eNaImAgFnS	být
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Donald	Donald	k1gMnSc1	Donald
Trump	Trump	k1gInSc4	Trump
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Kategorie	kategorie	k1gFnSc1	kategorie
Donald	Donald	k1gMnSc1	Donald
Trump	Trump	k1gMnSc1	Trump
ve	v	k7c6	v
Wikizprávách	Wikizpráva	k1gFnPc6	Wikizpráva
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Donald	Donald	k1gMnSc1	Donald
Trump	Trump	k1gMnSc1	Trump
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Recenze	recenze	k1gFnSc1	recenze
knihy	kniha	k1gFnSc2	kniha
Donald	Donald	k1gMnSc1	Donald
Trump	Trump	k1gMnSc1	Trump
<g/>
:	:	kIx,	:
nespokojený	spokojený	k2eNgMnSc1d1	nespokojený
</s>
</p>
