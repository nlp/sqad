<s>
Patrik	Patrik	k1gMnSc1	Patrik
Vajda	vajda	k1gMnSc1	vajda
(	(	kIx(	(
<g/>
*	*	kIx~	*
20	[number]	k4	20
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
Banská	banský	k2eAgFnSc1d1	Banská
Bystrica	Bystrica	k1gFnSc1	Bystrica
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
slovenský	slovenský	k2eAgMnSc1d1	slovenský
fotbalový	fotbalový	k2eAgMnSc1d1	fotbalový
obránce	obránce	k1gMnSc1	obránce
<g/>
,	,	kIx,	,
od	od	k7c2	od
léta	léto	k1gNnSc2	léto
2015	[number]	k4	2015
působící	působící	k2eAgFnSc2d1	působící
v	v	k7c6	v
klubu	klub	k1gInSc6	klub
FO	FO	kA	FO
ŽP	ŽP	kA	ŽP
ŠPORT	ŠPORT	kA	ŠPORT
Podbrezová	Podbrezová	k1gFnSc1	Podbrezová
<g/>
.	.	kIx.	.
</s>
<s>
Svoji	svůj	k3xOyFgFnSc4	svůj
fotbalovou	fotbalový	k2eAgFnSc4d1	fotbalová
kariéru	kariéra	k1gFnSc4	kariéra
začal	začít	k5eAaPmAgInS	začít
v	v	k7c6	v
FK	FK	kA	FK
Dukla	Dukla	k1gFnSc1	Dukla
Banská	banský	k2eAgFnSc1d1	Banská
Bystrica	Bystrica	k1gFnSc1	Bystrica
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
A-týmu	Aým	k1gInSc2	A-tým
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2015	[number]	k4	2015
po	po	k7c6	po
sestupu	sestup	k1gInSc6	sestup
Dukly	Dukla	k1gFnSc2	Dukla
Banská	banský	k2eAgFnSc1d1	Banská
Bystrica	Bystrica	k1gFnSc1	Bystrica
do	do	k7c2	do
2	[number]	k4	2
<g/>
.	.	kIx.	.
ligy	liga	k1gFnSc2	liga
přestoupil	přestoupit	k5eAaPmAgMnS	přestoupit
do	do	k7c2	do
prvoligového	prvoligový	k2eAgMnSc2d1	prvoligový
FO	FO	kA	FO
ŽP	ŽP	kA	ŽP
ŠPORT	ŠPORT	kA	ŠPORT
Podbrezová	Podbrezová	k1gFnSc1	Podbrezová
<g/>
,	,	kIx,	,
dalšího	další	k2eAgInSc2d1	další
klubu	klub	k1gInSc2	klub
z	z	k7c2	z
Banskobystrického	banskobystrický	k2eAgInSc2d1	banskobystrický
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
