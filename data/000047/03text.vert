<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Garrigue	Garrigue	k1gNnSc2	Garrigue
Masaryk	Masaryk	k1gMnSc1	Masaryk
<g/>
,	,	kIx,	,
označovaný	označovaný	k2eAgMnSc1d1	označovaný
T.	T.	kA	T.
G.	G.	kA	G.
M.	M.	kA	M.
<g/>
,	,	kIx,	,
TGM	TGM	kA	TGM
nebo	nebo	k8xC	nebo
president	president	k1gMnSc1	president
Osvoboditel	osvoboditel	k1gMnSc1	osvoboditel
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1850	[number]	k4	1850
Hodonín	Hodonín	k1gInSc1	Hodonín
–	–	k?	–
14	[number]	k4	14
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1937	[number]	k4	1937
Lány	lán	k1gInPc4	lán
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
státník	státník	k1gMnSc1	státník
<g/>
,	,	kIx,	,
filozof	filozof	k1gMnSc1	filozof
a	a	k8xC	a
pedagog	pedagog	k1gMnSc1	pedagog
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
prezident	prezident	k1gMnSc1	prezident
Československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jeho	jeho	k3xOp3gInPc3	jeho
osmdesátým	osmdesátý	k4xOgInPc3	osmdesátý
narozeninám	narozeniny	k1gFnPc3	narozeniny
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1930	[number]	k4	1930
přijat	přijat	k2eAgInSc1d1	přijat
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
zásluhách	zásluha	k1gFnPc6	zásluha
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
obsahující	obsahující	k2eAgFnSc4d1	obsahující
větu	věta	k1gFnSc4	věta
"	"	kIx"	"
<g/>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Garrigue	Garrigu	k1gMnSc2	Garrigu
Masaryk	Masaryk	k1gMnSc1	Masaryk
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
se	se	k3xPyFc4	se
o	o	k7c4	o
stát	stát	k1gInSc4	stát
<g/>
"	"	kIx"	"
a	a	k8xC	a
po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
z	z	k7c2	z
funkce	funkce	k1gFnSc2	funkce
roku	rok	k1gInSc2	rok
1935	[number]	k4	1935
ho	on	k3xPp3gInSc4	on
parlament	parlament	k1gInSc1	parlament
znovu	znovu	k6eAd1	znovu
ocenil	ocenit	k5eAaPmAgInS	ocenit
a	a	k8xC	a
odměnil	odměnit	k5eAaPmAgInS	odměnit
za	za	k7c4	za
jeho	jeho	k3xOp3gNnSc4	jeho
osvoboditelské	osvoboditelský	k2eAgNnSc4d1	osvoboditelské
a	a	k8xC	a
budovatelské	budovatelský	k2eAgNnSc4d1	budovatelské
dílo	dílo	k1gNnSc4	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Masaryk	Masaryk	k1gMnSc1	Masaryk
pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
chudé	chudý	k2eAgFnSc2d1	chudá
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
původem	původ	k1gInSc7	původ
Slovák	Slovák	k1gMnSc1	Slovák
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
kočí	kočí	k1gMnSc1	kočí
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
pracovala	pracovat	k5eAaImAgFnS	pracovat
jako	jako	k9	jako
kuchařka	kuchařka	k1gFnSc1	kuchařka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
studiích	studio	k1gNnPc6	studio
ve	v	k7c6	v
Strážnici	Strážnice	k1gFnSc6	Strážnice
<g/>
,	,	kIx,	,
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
a	a	k8xC	a
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
roku	rok	k1gInSc2	rok
1876	[number]	k4	1876
promoval	promovat	k5eAaBmAgInS	promovat
filosofickou	filosofický	k2eAgFnSc7d1	filosofická
prací	práce	k1gFnSc7	práce
o	o	k7c6	o
Platónovi	Platón	k1gMnSc6	Platón
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
studijního	studijní	k2eAgInSc2d1	studijní
pobytu	pobyt	k1gInSc2	pobyt
v	v	k7c6	v
Lipsku	Lipsko	k1gNnSc6	Lipsko
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1877	[number]	k4	1877
seznámil	seznámit	k5eAaPmAgInS	seznámit
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
budoucí	budoucí	k2eAgFnSc7d1	budoucí
ženou	žena	k1gFnSc7	žena
<g/>
,	,	kIx,	,
Američankou	Američanka	k1gFnSc7	Američanka
Charlottou	Charlotta	k1gFnSc7	Charlotta
Garrigueovou	Garrigueův	k2eAgFnSc7d1	Garrigueův
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1878	[number]	k4	1878
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
v	v	k7c4	v
New	New	k1gFnSc4	New
Yorku	York	k1gInSc2	York
oženil	oženit	k5eAaPmAgInS	oženit
<g/>
.	.	kIx.	.
</s>
<s>
Následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
habilitoval	habilitovat	k5eAaBmAgMnS	habilitovat
sociologickou	sociologický	k2eAgFnSc7d1	sociologická
prací	práce	k1gFnSc7	práce
o	o	k7c6	o
sebevraždě	sebevražda	k1gFnSc6	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
české	český	k2eAgFnSc2d1	Česká
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
byl	být	k5eAaImAgInS	být
1882	[number]	k4	1882
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
profesorem	profesor	k1gMnSc7	profesor
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
.	.	kIx.	.
</s>
<s>
Založil	založit	k5eAaPmAgInS	založit
a	a	k8xC	a
redigoval	redigovat	k5eAaImAgInS	redigovat
měsíčník	měsíčník	k1gInSc1	měsíčník
Athenaeum	Athenaeum	k1gInSc4	Athenaeum
<g/>
,	,	kIx,	,
inicioval	iniciovat	k5eAaBmAgMnS	iniciovat
Ottův	Ottův	k2eAgInSc4d1	Ottův
slovník	slovník	k1gInSc4	slovník
naučný	naučný	k2eAgInSc4d1	naučný
a	a	k8xC	a
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
veřejného	veřejný	k2eAgInSc2d1	veřejný
života	život	k1gInSc2	život
kritikou	kritika	k1gFnSc7	kritika
tzv.	tzv.	kA	tzv.
Rukopisů	rukopis	k1gInPc2	rukopis
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgMnPc4	svůj
studenty	student	k1gMnPc4	student
vedl	vést	k5eAaImAgMnS	vést
ke	k	k7c3	k
kritičnosti	kritičnost	k1gFnSc3	kritičnost
a	a	k8xC	a
vědeckosti	vědeckost	k1gFnSc2	vědeckost
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
Augusta	Augusta	k1gMnSc1	Augusta
Comta	Comt	k1gInSc2	Comt
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
program	program	k1gInSc1	program
"	"	kIx"	"
<g/>
realismu	realismus	k1gInSc2	realismus
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
soustředil	soustředit	k5eAaPmAgInS	soustředit
kolem	kolem	k7c2	kolem
týdeníku	týdeník	k1gInSc2	týdeník
Čas	čas	k1gInSc1	čas
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1890	[number]	k4	1890
Masaryk	Masaryk	k1gMnSc1	Masaryk
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
s	s	k7c7	s
přáteli	přítel	k1gMnPc7	přítel
do	do	k7c2	do
mladočeské	mladočeský	k2eAgFnSc2d1	mladočeská
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
v	v	k7c6	v
dalším	další	k2eAgInSc6d1	další
roce	rok	k1gInSc6	rok
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
poslancem	poslanec	k1gMnSc7	poslanec
Říšské	říšský	k2eAgFnSc2d1	říšská
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
Hájil	hájit	k5eAaImAgMnS	hájit
jak	jak	k6eAd1	jak
větší	veliký	k2eAgFnSc4d2	veliký
autonomii	autonomie	k1gFnSc4	autonomie
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
tak	tak	k9	tak
také	také	k9	také
zájmy	zájem	k1gInPc1	zájem
jihoslovanských	jihoslovanský	k2eAgInPc2d1	jihoslovanský
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pro	pro	k7c4	pro
spory	spor	k1gInPc4	spor
s	s	k7c7	s
radikálním	radikální	k2eAgNnSc7d1	radikální
vedením	vedení	k1gNnSc7	vedení
strany	strana	k1gFnSc2	strana
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1893	[number]	k4	1893
mandátu	mandát	k1gInSc2	mandát
vzdal	vzdát	k5eAaPmAgMnS	vzdát
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
kultivovat	kultivovat	k5eAaImF	kultivovat
české	český	k2eAgNnSc4d1	české
politické	politický	k2eAgNnSc4d1	politické
myšlení	myšlení	k1gNnSc4	myšlení
se	se	k3xPyFc4	se
Masaryk	Masaryk	k1gMnSc1	Masaryk
začal	začít	k5eAaPmAgMnS	začít
zabývat	zabývat	k5eAaImF	zabývat
dějinami	dějiny	k1gFnPc7	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Navázal	navázat	k5eAaPmAgMnS	navázat
na	na	k7c4	na
koncept	koncept	k1gInSc4	koncept
Františka	František	k1gMnSc2	František
Palackého	Palacký	k1gMnSc2	Palacký
a	a	k8xC	a
přemýšlel	přemýšlet	k5eAaImAgMnS	přemýšlet
o	o	k7c6	o
historickém	historický	k2eAgNnSc6d1	historické
poslání	poslání	k1gNnSc6	poslání
českého	český	k2eAgInSc2d1	český
národa	národ	k1gInSc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
reformace	reformace	k1gFnSc1	reformace
a	a	k8xC	a
národní	národní	k2eAgNnPc1d1	národní
obrození	obrození	k1gNnPc1	obrození
jako	jako	k8xS	jako
projevy	projev	k1gInPc1	projev
humanity	humanita	k1gFnSc2	humanita
mají	mít	k5eAaImIp3nP	mít
podle	podle	k7c2	podle
něho	on	k3xPp3gNnSc2	on
širší	široký	k2eAgInSc4d2	širší
<g/>
,	,	kIx,	,
všelidský	všelidský	k2eAgInSc4d1	všelidský
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
Masaryk	Masaryk	k1gMnSc1	Masaryk
zabýval	zabývat	k5eAaImAgMnS	zabývat
i	i	k9	i
sociálními	sociální	k2eAgFnPc7d1	sociální
otázkami	otázka	k1gFnPc7	otázka
<g/>
,	,	kIx,	,
podporoval	podporovat	k5eAaImAgInS	podporovat
osmihodinovou	osmihodinový	k2eAgFnSc4d1	osmihodinová
pracovní	pracovní	k2eAgFnSc4d1	pracovní
dobu	doba	k1gFnSc4	doba
a	a	k8xC	a
všeobecné	všeobecný	k2eAgNnSc4d1	všeobecné
volební	volební	k2eAgNnSc4d1	volební
právo	právo	k1gNnSc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1899	[number]	k4	1899
vystoupil	vystoupit	k5eAaPmAgInS	vystoupit
s	s	k7c7	s
požadavkem	požadavek	k1gInSc7	požadavek
na	na	k7c4	na
revizi	revize	k1gFnSc4	revize
procesu	proces	k1gInSc2	proces
s	s	k7c7	s
Hilsnerem	Hilsner	k1gMnSc7	Hilsner
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
hilsneriáda	hilsneriáda	k1gFnSc1	hilsneriáda
<g/>
)	)	kIx)	)
a	a	k8xC	a
proti	proti	k7c3	proti
antisemitským	antisemitský	k2eAgFnPc3d1	antisemitská
pověrám	pověra	k1gFnPc3	pověra
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
založil	založit	k5eAaPmAgMnS	založit
Českou	český	k2eAgFnSc4d1	Česká
stranu	strana	k1gFnSc4	strana
pokrokovou	pokrokový	k2eAgFnSc4d1	pokroková
<g/>
,	,	kIx,	,
za	za	k7c4	za
niž	jenž	k3xRgFnSc4	jenž
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1907	[number]	k4	1907
a	a	k8xC	a
1911	[number]	k4	1911
znovu	znovu	k6eAd1	znovu
zvolen	zvolit	k5eAaPmNgInS	zvolit
(	(	kIx(	(
<g/>
jejím	její	k3xOp3gInSc7	její
jediným	jediný	k2eAgInSc7d1	jediný
<g/>
)	)	kIx)	)
říšským	říšský	k2eAgMnSc7d1	říšský
poslancem	poslanec	k1gMnSc7	poslanec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
dlouhodobého	dlouhodobý	k2eAgNnSc2d1	dlouhodobé
studia	studio	k1gNnSc2	studio
a	a	k8xC	a
četných	četný	k2eAgFnPc2d1	četná
návštěv	návštěva	k1gFnPc2	návštěva
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
vydával	vydávat	k5eAaImAgInS	vydávat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1913	[number]	k4	1913
v	v	k7c6	v
němčině	němčina	k1gFnSc6	němčina
své	svůj	k3xOyFgFnSc2	svůj
nejrozsáhlejší	rozsáhlý	k2eAgFnSc2d3	nejrozsáhlejší
a	a	k8xC	a
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
nejslavnější	slavný	k2eAgNnSc1d3	nejslavnější
dílo	dílo	k1gNnSc1	dílo
"	"	kIx"	"
<g/>
Rusko	Rusko	k1gNnSc1	Rusko
a	a	k8xC	a
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
přeložené	přeložený	k2eAgFnSc2d1	přeložená
do	do	k7c2	do
několika	několik	k4yIc2	několik
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Svých	svůj	k3xOyFgInPc2	svůj
styků	styk	k1gInPc2	styk
<g/>
,	,	kIx,	,
znalostí	znalost	k1gFnPc2	znalost
i	i	k8xC	i
zkušeností	zkušenost	k1gFnPc2	zkušenost
Masaryk	Masaryk	k1gMnSc1	Masaryk
bohatě	bohatě	k6eAd1	bohatě
využil	využít	k5eAaPmAgMnS	využít
<g/>
,	,	kIx,	,
když	když	k8xS	když
roku	rok	k1gInSc2	rok
1914	[number]	k4	1914
cestoval	cestovat	k5eAaImAgMnS	cestovat
na	na	k7c4	na
Západ	západ	k1gInSc4	západ
a	a	k8xC	a
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
R.	R.	kA	R.
W.	W.	kA	W.
Seton-Watsona	Seton-Watsona	k1gFnSc1	Seton-Watsona
a	a	k8xC	a
dalších	další	k2eAgNnPc6d1	další
seznamoval	seznamovat	k5eAaImAgMnS	seznamovat
světové	světový	k2eAgFnSc2d1	světová
politiky	politika	k1gFnSc2	politika
s	s	k7c7	s
českými	český	k2eAgInPc7d1	český
požadavky	požadavek	k1gInPc7	požadavek
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1915	[number]	k4	1915
se	se	k3xPyFc4	se
už	už	k6eAd1	už
nemohl	moct	k5eNaImAgMnS	moct
vrátit	vrátit	k5eAaPmF	vrátit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
na	na	k7c4	na
něho	on	k3xPp3gMnSc4	on
byl	být	k5eAaImAgInS	být
vydán	vydán	k2eAgInSc1d1	vydán
zatykač	zatykač	k1gInSc1	zatykač
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
žena	žena	k1gFnSc1	žena
byla	být	k5eAaImAgFnS	být
uvězněna	uvěznit	k5eAaPmNgFnS	uvěznit
<g/>
.	.	kIx.	.
</s>
<s>
Spojkou	spojka	k1gFnSc7	spojka
s	s	k7c7	s
domovem	domov	k1gInSc7	domov
pak	pak	k6eAd1	pak
byl	být	k5eAaImAgMnS	být
Edvard	Edvard	k1gMnSc1	Edvard
Beneš	Beneš	k1gMnSc1	Beneš
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
přátelé	přítel	k1gMnPc1	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Obeslal	obeslat	k5eAaPmAgMnS	obeslat
české	český	k2eAgMnPc4d1	český
krajany	krajan	k1gMnPc4	krajan
po	po	k7c6	po
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgMnS	získat
jejich	jejich	k3xOp3gFnSc4	jejich
podporu	podpora	k1gFnSc4	podpora
a	a	k8xC	a
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1915	[number]	k4	1915
v	v	k7c6	v
Ženevě	Ženeva	k1gFnSc6	Ženeva
poprvé	poprvé	k6eAd1	poprvé
veřejně	veřejně	k6eAd1	veřejně
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
požadavek	požadavek	k1gInSc1	požadavek
samostatného	samostatný	k2eAgInSc2d1	samostatný
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
odjel	odjet	k5eAaPmAgMnS	odjet
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
profesorem	profesor	k1gMnSc7	profesor
na	na	k7c6	na
King	King	k1gMnSc1	King
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
College	College	k1gFnSc7	College
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
neúnavně	únavně	k6eNd1	únavně
publikoval	publikovat	k5eAaBmAgMnS	publikovat
a	a	k8xC	a
přesvědčoval	přesvědčovat	k5eAaImAgMnS	přesvědčovat
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1916	[number]	k4	1916
mu	on	k3xPp3gMnSc3	on
M.	M.	kA	M.
R.	R.	kA	R.
Štefánik	Štefánik	k1gMnSc1	Štefánik
připravil	připravit	k5eAaPmAgMnS	připravit
přijetí	přijetí	k1gNnSc4	přijetí
u	u	k7c2	u
francouzského	francouzský	k2eAgMnSc2d1	francouzský
premiéra	premiér	k1gMnSc2	premiér
Brianda	Brianda	k1gFnSc1	Brianda
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
také	také	k9	také
Československá	československý	k2eAgFnSc1d1	Československá
národní	národní	k2eAgFnSc1d1	národní
rada	rada	k1gFnSc1	rada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1917	[number]	k4	1917
<g/>
,	,	kIx,	,
když	když	k8xS	když
velmoci	velmoc	k1gFnPc1	velmoc
uznaly	uznat	k5eAaPmAgFnP	uznat
tehdy	tehdy	k6eAd1	tehdy
formulovaný	formulovaný	k2eAgInSc4d1	formulovaný
československý	československý	k2eAgInSc4d1	československý
požadavek	požadavek	k1gInSc4	požadavek
<g/>
,	,	kIx,	,
odjel	odjet	k5eAaPmAgMnS	odjet
Masaryk	Masaryk	k1gMnSc1	Masaryk
do	do	k7c2	do
Ruska	Rusko	k1gNnSc2	Rusko
shromažďovat	shromažďovat	k5eAaImF	shromažďovat
vojsko	vojsko	k1gNnSc4	vojsko
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
svému	svůj	k3xOyFgMnSc3	svůj
programu	program	k1gInSc2	program
dodal	dodat	k5eAaPmAgMnS	dodat
větší	veliký	k2eAgFnSc4d2	veliký
váhu	váha	k1gFnSc4	váha
<g/>
.	.	kIx.	.
</s>
<s>
Československé	československý	k2eAgFnPc1d1	Československá
legie	legie	k1gFnPc1	legie
měly	mít	k5eAaImAgFnP	mít
postupně	postupně	k6eAd1	postupně
až	až	k6eAd1	až
50	[number]	k4	50
tisíc	tisíc	k4xCgInPc2	tisíc
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
operovaly	operovat	k5eAaImAgInP	operovat
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
však	však	k9	však
podřízeny	podřízen	k2eAgFnPc1d1	podřízena
Národní	národní	k2eAgFnSc2d1	národní
radě	rada	k1gFnSc3	rada
a	a	k8xC	a
zařazeny	zařazen	k2eAgMnPc4d1	zařazen
jako	jako	k8xC	jako
spojenci	spojenec	k1gMnPc7	spojenec
do	do	k7c2	do
francouzské	francouzský	k2eAgFnSc2d1	francouzská
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Masaryk	Masaryk	k1gMnSc1	Masaryk
sám	sám	k3xTgMnSc1	sám
řídil	řídit	k5eAaImAgMnS	řídit
organizování	organizování	k1gNnSc4	organizování
legií	legie	k1gFnPc2	legie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
říjnové	říjnový	k2eAgFnSc6d1	říjnová
revoluci	revoluce	k1gFnSc6	revoluce
a	a	k8xC	a
separátnímu	separátní	k2eAgInSc3d1	separátní
míru	mír	k1gInSc3	mír
Ruska	Rusko	k1gNnSc2	Rusko
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgMnS	vydat
z	z	k7c2	z
Moskvy	Moskva	k1gFnSc2	Moskva
přes	přes	k7c4	přes
Sibiř	Sibiř	k1gFnSc4	Sibiř
do	do	k7c2	do
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
připravil	připravit	k5eAaPmAgInS	připravit
přepravu	přeprava	k1gFnSc4	přeprava
legií	legie	k1gFnPc2	legie
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1918	[number]	k4	1918
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgMnS	vydat
lodí	loď	k1gFnSc7	loď
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
věděl	vědět	k5eAaImAgMnS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
prezident	prezident	k1gMnSc1	prezident
Woodrow	Woodrow	k1gMnSc1	Woodrow
Wilson	Wilson	k1gMnSc1	Wilson
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
v	v	k7c6	v
poválečném	poválečný	k2eAgNnSc6d1	poválečné
uspořádání	uspořádání	k1gNnSc6	uspořádání
světa	svět	k1gInSc2	svět
a	a	k8xC	a
zejména	zejména	k9	zejména
Střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
velmi	velmi	k6eAd1	velmi
významné	významný	k2eAgNnSc4d1	významné
slovo	slovo	k1gNnSc4	slovo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1918	[number]	k4	1918
do	do	k7c2	do
USA	USA	kA	USA
dorazil	dorazit	k5eAaPmAgMnS	dorazit
<g/>
.	.	kIx.	.
</s>
<s>
Přednášel	přednášet	k5eAaImAgMnS	přednášet
<g/>
,	,	kIx,	,
psal	psát	k5eAaImAgMnS	psát
a	a	k8xC	a
agitoval	agitovat	k5eAaImAgMnS	agitovat
zejména	zejména	k9	zejména
mezi	mezi	k7c7	mezi
krajany	krajan	k1gMnPc7	krajan
a	a	k8xC	a
dosavadní	dosavadní	k2eAgInPc1d1	dosavadní
velké	velký	k2eAgInPc1d1	velký
úspěchy	úspěch	k1gInPc1	úspěch
legionářů	legionář	k1gMnPc2	legionář
byly	být	k5eAaImAgInP	být
jeho	jeho	k3xOp3gInSc7	jeho
pádným	pádný	k2eAgInSc7d1	pádný
argumentem	argument	k1gInSc7	argument
<g/>
.	.	kIx.	.
</s>
<s>
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
kniha	kniha	k1gFnSc1	kniha
"	"	kIx"	"
<g/>
Nová	nový	k2eAgFnSc1d1	nová
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
"	"	kIx"	"
přispěla	přispět	k5eAaPmAgFnS	přispět
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
prezident	prezident	k1gMnSc1	prezident
Wilson	Wilson	k1gMnSc1	Wilson
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
rakouské	rakouský	k2eAgInPc4d1	rakouský
návrhy	návrh	k1gInPc4	návrh
na	na	k7c4	na
federalizaci	federalizace	k1gFnSc4	federalizace
monarchie	monarchie	k1gFnSc2	monarchie
a	a	k8xC	a
postavil	postavit	k5eAaPmAgMnS	postavit
se	se	k3xPyFc4	se
za	za	k7c4	za
sebeurčení	sebeurčení	k1gNnSc4	sebeurčení
slovanských	slovanský	k2eAgInPc2d1	slovanský
i	i	k8xC	i
jiných	jiný	k2eAgInPc2d1	jiný
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c4	o
revoluci	revoluce	k1gFnSc4	revoluce
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
vzniku	vznik	k1gInSc6	vznik
Československa	Československo	k1gNnSc2	Československo
dne	den	k1gInSc2	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1918	[number]	k4	1918
zastihla	zastihnout	k5eAaPmAgFnS	zastihnout
Masaryka	Masaryk	k1gMnSc2	Masaryk
ještě	ještě	k9	ještě
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
jeho	jeho	k3xOp3gNnSc6	jeho
zvolení	zvolení	k1gNnSc6	zvolení
prezidentem	prezident	k1gMnSc7	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Cestou	cestou	k7c2	cestou
domů	dům	k1gInPc2	dům
navštívil	navštívit	k5eAaPmAgMnS	navštívit
už	už	k6eAd1	už
jako	jako	k8xC	jako
prezident	prezident	k1gMnSc1	prezident
Anglii	Anglie	k1gFnSc4	Anglie
<g/>
,	,	kIx,	,
Francii	Francie	k1gFnSc4	Francie
a	a	k8xC	a
Itálii	Itálie	k1gFnSc4	Itálie
i	i	k8xC	i
české	český	k2eAgMnPc4d1	český
legionáře	legionář	k1gMnPc4	legionář
a	a	k8xC	a
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1918	[number]	k4	1918
byl	být	k5eAaImAgInS	být
triumfálně	triumfálně	k6eAd1	triumfálně
uvítán	uvítat	k5eAaPmNgInS	uvítat
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
byl	být	k5eAaImAgInS	být
znovu	znovu	k6eAd1	znovu
zvolen	zvolit	k5eAaPmNgInS	zvolit
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
získal	získat	k5eAaPmAgInS	získat
jen	jen	k9	jen
asi	asi	k9	asi
65	[number]	k4	65
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
i	i	k9	i
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1927	[number]	k4	1927
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
při	při	k7c6	při
třetí	třetí	k4xOgFnSc6	třetí
volbě	volba	k1gFnSc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1934	[number]	k4	1934
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
ústava	ústava	k1gFnSc1	ústava
presidentu	president	k1gMnSc3	president
Osvoboditeli	osvoboditel	k1gMnSc3	osvoboditel
dovolovala	dovolovat	k5eAaImAgFnS	dovolovat
a	a	k8xC	a
která	který	k3yQgFnSc1	který
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
jako	jako	k9	jako
manifestace	manifestace	k1gFnSc1	manifestace
pro	pro	k7c4	pro
demokracii	demokracie	k1gFnSc4	demokracie
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgMnS	získat
73	[number]	k4	73
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Komunisté	komunista	k1gMnPc1	komunista
a	a	k8xC	a
slovenští	slovenský	k2eAgMnPc1d1	slovenský
nacionalisté	nacionalista	k1gMnPc1	nacionalista
pro	pro	k7c4	pro
Masaryka	Masaryk	k1gMnSc4	Masaryk
nikdy	nikdy	k6eAd1	nikdy
nehlasovali	hlasovat	k5eNaImAgMnP	hlasovat
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1935	[number]	k4	1935
Masaryk	Masaryk	k1gMnSc1	Masaryk
ze	z	k7c2	z
zdravotních	zdravotní	k2eAgInPc2d1	zdravotní
důvodů	důvod	k1gInPc2	důvod
abdikoval	abdikovat	k5eAaBmAgMnS	abdikovat
a	a	k8xC	a
14	[number]	k4	14
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1937	[number]	k4	1937
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
pohřeb	pohřeb	k1gInSc1	pohřeb
byl	být	k5eAaImAgInS	být
velkou	velký	k2eAgFnSc7d1	velká
národní	národní	k2eAgFnSc7d1	národní
manifestací	manifestace	k1gFnSc7	manifestace
za	za	k7c4	za
svobodu	svoboda	k1gFnSc4	svoboda
a	a	k8xC	a
demokracii	demokracie	k1gFnSc4	demokracie
<g/>
.	.	kIx.	.
</s>
<s>
Masaryk	Masaryk	k1gMnSc1	Masaryk
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Jan	Jan	k1gMnSc1	Jan
<g/>
"	"	kIx"	"
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
bouřlivém	bouřlivý	k2eAgInSc6d1	bouřlivý
revolučním	revoluční	k2eAgInSc6d1	revoluční
roce	rok	k1gInSc6	rok
1848	[number]	k4	1848
na	na	k7c6	na
Slovácku	Slovácko	k1gNnSc6	Slovácko
<g/>
.	.	kIx.	.
</s>
<s>
Rodiče	rodič	k1gMnPc1	rodič
byli	být	k5eAaImAgMnP	být
zaměstnanci	zaměstnanec	k1gMnPc1	zaměstnanec
na	na	k7c6	na
statku	statek	k1gInSc6	statek
Nathana	Nathan	k1gMnSc2	Nathan
Redlicha	Redlich	k1gMnSc2	Redlich
<g/>
.	.	kIx.	.
</s>
<s>
Tomášova	Tomášův	k2eAgFnSc1d1	Tomášova
matka	matka	k1gFnSc1	matka
Terezie	Terezie	k1gFnSc2	Terezie
Masaryková	Masaryková	k1gFnSc1	Masaryková
roz	roz	k?	roz
<g/>
.	.	kIx.	.
Kropáčková	Kropáčková	k1gFnSc1	Kropáčková
(	(	kIx(	(
<g/>
1813	[number]	k4	1813
<g/>
–	–	k?	–
<g/>
1887	[number]	k4	1887
<g/>
)	)	kIx)	)
z	z	k7c2	z
Hustopečí	Hustopeč	k1gFnPc2	Hustopeč
<g/>
,	,	kIx,	,
pocházela	pocházet	k5eAaImAgFnS	pocházet
z	z	k7c2	z
Hané	Haná	k1gFnSc2	Haná
<g/>
.	.	kIx.	.
</s>
<s>
Mluvila	mluvit	k5eAaImAgFnS	mluvit
lépe	dobře	k6eAd2	dobře
německy	německy	k6eAd1	německy
než	než	k8xS	než
česky	česky	k6eAd1	česky
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
Jozef	Jozef	k1gMnSc1	Jozef
Maszárik	Maszárik	k1gMnSc1	Maszárik
(	(	kIx(	(
<g/>
také	také	k9	také
Masarik	Masarika	k1gFnPc2	Masarika
<g/>
,	,	kIx,	,
či	či	k8xC	či
Masaryk	Masaryk	k1gMnSc1	Masaryk
<g/>
,	,	kIx,	,
1823	[number]	k4	1823
<g/>
–	–	k?	–
<g/>
1907	[number]	k4	1907
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
Slovák	Slovák	k1gMnSc1	Slovák
a	a	k8xC	a
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
kočí	kočí	k1gMnSc1	kočí
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
dlouho	dlouho	k6eAd1	dlouho
negramotný	gramotný	k2eNgMnSc1d1	negramotný
a	a	k8xC	a
psát	psát	k5eAaImF	psát
ho	on	k3xPp3gMnSc4	on
naučil	naučit	k5eAaPmAgMnS	naučit
až	až	k9	až
syn	syn	k1gMnSc1	syn
Tomáš	Tomáš	k1gMnSc1	Tomáš
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	s	k7c7	s
25	[number]	k4	25
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1823	[number]	k4	1823
v	v	k7c6	v
Kopčanech	Kopčan	k1gMnPc6	Kopčan
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
tehdy	tehdy	k6eAd1	tehdy
patřilo	patřit	k5eAaImAgNnS	patřit
do	do	k7c2	do
Uherského	uherský	k2eAgNnSc2d1	Uherské
království	království	k1gNnSc2	království
<g/>
;	;	kIx,	;
tak	tak	k6eAd1	tak
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
úřední	úřední	k2eAgFnSc1d1	úřední
podoba	podoba	k1gFnSc1	podoba
jeho	on	k3xPp3gNnSc2	on
příjmení	příjmení	k1gNnSc2	příjmení
(	(	kIx(	(
<g/>
Maszárik	Maszárika	k1gFnPc2	Maszárika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Masaryk	Masaryk	k1gMnSc1	Masaryk
měl	mít	k5eAaImAgMnS	mít
dva	dva	k4xCgInPc4	dva
mladší	mladý	k2eAgFnPc4d2	mladší
bratry	bratr	k1gMnPc7	bratr
<g/>
,	,	kIx,	,
Martina	Martina	k1gFnSc1	Martina
(	(	kIx(	(
<g/>
1852	[number]	k4	1852
<g/>
–	–	k?	–
<g/>
1873	[number]	k4	1873
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ludvíka	Ludvík	k1gMnSc4	Ludvík
(	(	kIx(	(
<g/>
1854	[number]	k4	1854
<g/>
–	–	k?	–
<g/>
1912	[number]	k4	1912
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
hustopečská	hustopečský	k2eAgFnSc1d1	Hustopečská
tiskárna	tiskárna	k1gFnSc1	tiskárna
později	pozdě	k6eAd2	pozdě
svou	svůj	k3xOyFgFnSc7	svůj
produkcí	produkce	k1gFnSc7	produkce
výrazně	výrazně	k6eAd1	výrazně
podporovala	podporovat	k5eAaImAgFnS	podporovat
Masarykovo	Masarykův	k2eAgNnSc4d1	Masarykovo
veřejné	veřejný	k2eAgNnSc4d1	veřejné
působení	působení	k1gNnSc4	působení
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnPc1d1	další
sourozenci	sourozenec	k1gMnPc1	sourozenec
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
a	a	k8xC	a
Františka	František	k1gMnSc2	František
<g/>
,	,	kIx,	,
zemřeli	zemřít	k5eAaPmAgMnP	zemřít
ještě	ještě	k6eAd1	ještě
jako	jako	k8xS	jako
malé	malý	k2eAgFnPc4d1	malá
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
absolvování	absolvování	k1gNnSc6	absolvování
čejkovické	čejkovický	k2eAgFnSc2d1	čejkovická
školy	škola	k1gFnSc2	škola
studoval	studovat	k5eAaImAgMnS	studovat
Tomáš	Tomáš	k1gMnSc1	Tomáš
Masaryk	Masaryk	k1gMnSc1	Masaryk
na	na	k7c6	na
reálném	reálný	k2eAgNnSc6d1	reálné
gymnáziu	gymnázium	k1gNnSc6	gymnázium
v	v	k7c6	v
Hustopečích	Hustopeč	k1gFnPc6	Hustopeč
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
na	na	k7c6	na
gymnáziu	gymnázium	k1gNnSc6	gymnázium
ve	v	k7c6	v
Strážnici	Strážnice	k1gFnSc6	Strážnice
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
však	však	k9	však
nemohl	moct	k5eNaImAgInS	moct
dokončit	dokončit	k5eAaPmF	dokončit
<g/>
.	.	kIx.	.
</s>
<s>
Praktikoval	praktikovat	k5eAaImAgMnS	praktikovat
krátce	krátce	k6eAd1	krátce
v	v	k7c6	v
hodonínské	hodonínský	k2eAgFnSc6d1	hodonínská
škole	škola	k1gFnSc6	škola
<g/>
,	,	kIx,	,
učil	učit	k5eAaImAgMnS	učit
se	s	k7c7	s
zámečníkem	zámečník	k1gMnSc7	zámečník
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
kovářem	kovář	k1gMnSc7	kovář
v	v	k7c6	v
Čejči	Čejč	k1gInSc6	Čejč
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
brzy	brzy	k6eAd1	brzy
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
jako	jako	k8xC	jako
podučitel	podučitel	k1gMnSc1	podučitel
hustopečského	hustopečský	k2eAgNnSc2d1	Hustopečské
reálného	reálný	k2eAgNnSc2d1	reálné
gymnázia	gymnázium	k1gNnSc2	gymnázium
a	a	k8xC	a
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
vlasteneckého	vlastenecký	k2eAgMnSc2d1	vlastenecký
kněze	kněz	k1gMnSc2	kněz
a	a	k8xC	a
kaplana	kaplan	k1gMnSc2	kaplan
P.	P.	kA	P.
Františka	František	k1gMnSc2	František
Satory	Satora	k1gFnSc2	Satora
se	se	k3xPyFc4	se
chystal	chystat	k5eAaImAgMnS	chystat
ke	k	k7c3	k
studiu	studio	k1gNnSc3	studio
na	na	k7c6	na
německojazyčném	německojazyčný	k2eAgNnSc6d1	německojazyčné
klasickém	klasický	k2eAgNnSc6d1	klasické
gymnáziu	gymnázium	k1gNnSc6	gymnázium
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
nemajetní	majetný	k2eNgMnPc1d1	nemajetný
rodiče	rodič	k1gMnPc1	rodič
(	(	kIx(	(
<g/>
otec	otec	k1gMnSc1	otec
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
panský	panský	k2eAgMnSc1d1	panský
kočí	kočí	k1gMnSc1	kočí
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
správce	správce	k1gMnSc1	správce
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
jako	jako	k8xC	jako
kuchařka	kuchařka	k1gFnSc1	kuchařka
v	v	k7c6	v
panských	panský	k2eAgFnPc6d1	Panská
službách	služba	k1gFnPc6	služba
<g/>
)	)	kIx)	)
ho	on	k3xPp3gMnSc4	on
nemohli	moct	k5eNaImAgMnP	moct
na	na	k7c6	na
studiích	studie	k1gFnPc6	studie
vydržovat	vydržovat	k5eAaImF	vydržovat
<g/>
.	.	kIx.	.
</s>
<s>
Dával	dávat	k5eAaImAgMnS	dávat
proto	proto	k8xC	proto
kondice	kondice	k1gFnSc1	kondice
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
bylo	být	k5eAaImAgNnS	být
již	již	k6eAd1	již
tehdy	tehdy	k6eAd1	tehdy
obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
publicisté	publicista	k1gMnPc1	publicista
Masarykův	Masarykův	k2eAgInSc4d1	Masarykův
původ	původ	k1gInSc4	původ
po	po	k7c6	po
otci	otec	k1gMnSc6	otec
zpochybňovali	zpochybňovat	k5eAaImAgMnP	zpochybňovat
především	především	k9	především
kvůli	kvůli	k7c3	kvůli
věkovému	věkový	k2eAgInSc3d1	věkový
rozdílu	rozdíl	k1gInSc2	rozdíl
10	[number]	k4	10
let	léto	k1gNnPc2	léto
mezi	mezi	k7c7	mezi
jeho	jeho	k3xOp3gMnPc7	jeho
rodiči	rodič	k1gMnPc7	rodič
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnSc3	jejich
odlišnému	odlišný	k2eAgNnSc3d1	odlišné
společenskému	společenský	k2eAgNnSc3d1	společenské
postavení	postavení	k1gNnSc3	postavení
a	a	k8xC	a
vzdělanosti	vzdělanost	k1gFnSc2	vzdělanost
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytly	vyskytnout	k5eAaPmAgInP	vyskytnout
se	se	k3xPyFc4	se
i	i	k9	i
snahy	snaha	k1gFnPc1	snaha
"	"	kIx"	"
<g/>
připsat	připsat	k5eAaPmF	připsat
<g/>
"	"	kIx"	"
k	k	k7c3	k
otcovství	otcovství	k1gNnSc3	otcovství
TGM	TGM	kA	TGM
jména	jméno	k1gNnPc4	jméno
statkáře	statkář	k1gMnSc2	statkář
Redlicha	Redlich	k1gMnSc2	Redlich
či	či	k8xC	či
dokonce	dokonce	k9	dokonce
rakouského	rakouský	k2eAgMnSc2d1	rakouský
císaře	císař	k1gMnSc2	císař
Františka	František	k1gMnSc4	František
Josefa	Josef	k1gMnSc2	Josef
I.	I.	kA	I.
(	(	kIx(	(
<g/>
císaři	císař	k1gMnSc3	císař
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
narození	narození	k1gNnSc2	narození
Tomáše	Tomáš	k1gMnSc2	Tomáš
Masaryka	Masaryk	k1gMnSc2	Masaryk
20	[number]	k4	20
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
narodil	narodit	k5eAaPmAgInS	narodit
se	s	k7c7	s
1830	[number]	k4	1830
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
neprokázaná	prokázaný	k2eNgFnSc1d1	neprokázaná
hypotéza	hypotéza	k1gFnSc1	hypotéza
rozvádí	rozvádět	k5eAaImIp3nS	rozvádět
údajnou	údajný	k2eAgFnSc4d1	údajná
císařovu	císařův	k2eAgFnSc4d1	císařova
poznámku	poznámka	k1gFnSc4	poznámka
v	v	k7c6	v
osobním	osobní	k2eAgInSc6d1	osobní
deníku	deník	k1gInSc6	deník
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1849	[number]	k4	1849
"	"	kIx"	"
<g/>
Kropaczek	Kropaczek	k1gInSc1	Kropaczek
erl	erl	k?	erl
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
"	"	kIx"	"
<g/>
Kropáček	Kropáček	k1gMnSc1	Kropáček
<g/>
/	/	kIx~	/
<g/>
Kropáčková	Kropáčková	k1gFnSc1	Kropáčková
vyřízeno	vyřídit	k5eAaPmNgNnS	vyřídit
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
vztahující	vztahující	k2eAgMnSc1d1	vztahující
se	se	k3xPyFc4	se
k	k	k7c3	k
Masarykově	Masarykův	k2eAgFnSc3d1	Masarykova
matce	matka	k1gFnSc3	matka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
2016	[number]	k4	2016
a	a	k8xC	a
2017	[number]	k4	2017
měla	mít	k5eAaImAgFnS	mít
proběhnout	proběhnout	k5eAaPmF	proběhnout
analýza	analýza	k1gFnSc1	analýza
DNA	dno	k1gNnSc2	dno
Masarykových	Masarykových	k2eAgFnPc2d1	Masarykových
příbuzných	příbuzná	k1gFnPc2	příbuzná
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
vyvrátila	vyvrátit	k5eAaPmAgFnS	vyvrátit
nebo	nebo	k8xC	nebo
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
domněnku	domněnka	k1gFnSc4	domněnka
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
příbuznosti	příbuznost	k1gFnSc6	příbuznost
s	s	k7c7	s
Františkem	František	k1gMnSc7	František
Josefem	Josef	k1gMnSc7	Josef
I.	I.	kA	I.
Výzkum	výzkum	k1gInSc1	výzkum
byl	být	k5eAaImAgInS	být
však	však	k9	však
pozastaven	pozastaven	k2eAgInSc1d1	pozastaven
na	na	k7c4	na
žádost	žádost	k1gFnSc4	žádost
pravnučky	pravnučka	k1gFnSc2	pravnučka
Tomáše	Tomáš	k1gMnSc2	Tomáš
Garrigua	Garriguus	k1gMnSc2	Garriguus
Masaryka	Masaryk	k1gMnSc2	Masaryk
Charlotty	Charlotta	k1gFnSc2	Charlotta
Kotíkové	Kotíková	k1gFnSc2	Kotíková
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
15	[number]	k4	15
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
živil	živit	k5eAaImAgMnS	živit
sám	sám	k3xTgMnSc1	sám
jako	jako	k8xS	jako
domácí	domácí	k2eAgMnSc1d1	domácí
učitel	učitel	k1gMnSc1	učitel
dětí	dítě	k1gFnPc2	dítě
bohatých	bohatý	k2eAgMnPc2d1	bohatý
rodičů	rodič	k1gMnPc2	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
gymnáziu	gymnázium	k1gNnSc6	gymnázium
(	(	kIx(	(
<g/>
studia	studio	k1gNnSc2	studio
byla	být	k5eAaImAgFnS	být
přerušena	přerušit	k5eAaPmNgFnS	přerušit
prusko-rakouskou	pruskoakouský	k2eAgFnSc7d1	prusko-rakouská
válkou	válka	k1gFnSc7	válka
1866	[number]	k4	1866
<g/>
)	)	kIx)	)
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
byl	být	k5eAaImAgInS	být
podporován	podporovat	k5eAaImNgInS	podporovat
rodinou	rodina	k1gFnSc7	rodina
policejního	policejní	k2eAgMnSc2d1	policejní
ředitele	ředitel	k1gMnSc2	ředitel
Antona	Anton	k1gMnSc2	Anton
Le	Le	k1gMnSc2	Le
Monniera	Monnier	k1gMnSc2	Monnier
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	nízce	k6eAd2	nízce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1869	[number]	k4	1869
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dostudoval	dostudovat	k5eAaPmAgMnS	dostudovat
na	na	k7c6	na
Akademickém	akademický	k2eAgNnSc6d1	akademické
gymnáziu	gymnázium	k1gNnSc6	gymnázium
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
maturitě	maturita	k1gFnSc6	maturita
1872	[number]	k4	1872
začal	začít	k5eAaPmAgMnS	začít
studovat	studovat	k5eAaImF	studovat
filosofickou	filosofický	k2eAgFnSc4d1	filosofická
fakultu	fakulta	k1gFnSc4	fakulta
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ho	on	k3xPp3gMnSc4	on
silně	silně	k6eAd1	silně
ovlivnil	ovlivnit	k5eAaPmAgMnS	ovlivnit
nový	nový	k2eAgMnSc1d1	nový
profesor	profesor	k1gMnSc1	profesor
filosofie	filosofie	k1gFnSc2	filosofie
Franz	Franz	k1gMnSc1	Franz
Brentano	Brentana	k1gFnSc5	Brentana
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
přišel	přijít	k5eAaPmAgMnS	přijít
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1874	[number]	k4	1874
<g/>
.	.	kIx.	.
</s>
<s>
Brentano	Brentana	k1gFnSc5	Brentana
začínal	začínat	k5eAaImAgMnS	začínat
jako	jako	k9	jako
kněz	kněz	k1gMnSc1	kněz
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
s	s	k7c7	s
církví	církev	k1gFnSc7	církev
rozešel	rozejít	k5eAaPmAgMnS	rozejít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gNnPc1	jeho
realistická	realistický	k2eAgNnPc1d1	realistické
i	i	k9	i
když	když	k8xS	když
nikoli	nikoli	k9	nikoli
ateistická	ateistický	k2eAgFnSc1d1	ateistická
filosofie	filosofie	k1gFnSc1	filosofie
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
na	na	k7c6	na
aristotelismu	aristotelismus	k1gInSc6	aristotelismus
<g/>
,	,	kIx,	,
Masaryka	Masaryk	k1gMnSc2	Masaryk
natrvalo	natrvalo	k6eAd1	natrvalo
poznamenala	poznamenat	k5eAaPmAgFnS	poznamenat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Antona	Anton	k1gMnSc2	Anton
Le	Le	k1gMnSc2	Le
Monniera	Monnier	k1gMnSc2	Monnier
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1873	[number]	k4	1873
<g/>
,	,	kIx,	,
našel	najít	k5eAaPmAgInS	najít
nové	nový	k2eAgNnSc4d1	nové
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
Rudolfa	Rudolf	k1gMnSc2	Rudolf
Schlessingera	Schlessinger	k1gMnSc2	Schlessinger
<g/>
,	,	kIx,	,
generálního	generální	k2eAgMnSc2d1	generální
rady	rada	k1gMnSc2	rada
Anglo-rakouské	Angloakouský	k2eAgFnSc2d1	Anglo-rakouská
banky	banka	k1gFnSc2	banka
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc1	jehož
syna	syn	k1gMnSc4	syn
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
odměnu	odměna	k1gFnSc4	odměna
za	za	k7c4	za
úspěšnou	úspěšný	k2eAgFnSc4d1	úspěšná
maturitu	maturita	k1gFnSc4	maturita
mladého	mladý	k2eAgMnSc2d1	mladý
Schlessingera	Schlessinger	k1gMnSc2	Schlessinger
odjeli	odjet	k5eAaPmAgMnP	odjet
roku	rok	k1gInSc2	rok
1876	[number]	k4	1876
společně	společně	k6eAd1	společně
po	po	k7c6	po
Masarykově	Masarykův	k2eAgInSc6d1	Masarykův
doktorátu	doktorát	k1gInSc6	doktorát
(	(	kIx(	(
<g/>
disertační	disertační	k2eAgFnSc1d1	disertační
práce	práce	k1gFnSc1	práce
"	"	kIx"	"
<g/>
Podstata	podstata	k1gFnSc1	podstata
duše	duše	k1gFnSc1	duše
u	u	k7c2	u
Platóna	Platón	k1gMnSc2	Platón
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
po	po	k7c6	po
Itálii	Itálie	k1gFnSc6	Itálie
a	a	k8xC	a
Masaryk	Masaryk	k1gMnSc1	Masaryk
na	na	k7c4	na
roční	roční	k2eAgInSc4d1	roční
pobyt	pobyt	k1gInSc4	pobyt
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Lipsku	Lipsko	k1gNnSc6	Lipsko
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
Masaryk	Masaryk	k1gMnSc1	Masaryk
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
mladším	mladý	k2eAgMnSc7d2	mladší
krajanem	krajan	k1gMnSc7	krajan
E.	E.	kA	E.
Husserlem	Husserl	k1gMnSc7	Husserl
a	a	k8xC	a
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
mladou	mladý	k2eAgFnSc7d1	mladá
klavíristkou	klavíristka	k1gFnSc7	klavíristka
Charlottou	Charlotta	k1gFnSc7	Charlotta
Garrigue	Garrigue	k1gFnSc7	Garrigue
<g/>
,	,	kIx,	,
dcerou	dcera	k1gFnSc7	dcera
newyorského	newyorský	k2eAgMnSc4d1	newyorský
podnikatele	podnikatel	k1gMnSc4	podnikatel
a	a	k8xC	a
10	[number]	k4	10
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1877	[number]	k4	1877
byli	být	k5eAaImAgMnP	být
zasnoubeni	zasnouben	k2eAgMnPc1d1	zasnouben
<g/>
.	.	kIx.	.
</s>
<s>
Masaryk	Masaryk	k1gMnSc1	Masaryk
chtěl	chtít	k5eAaImAgMnS	chtít
ale	ale	k9	ale
před	před	k7c7	před
sňatkem	sňatek	k1gInSc7	sňatek
získat	získat	k5eAaPmF	získat
docenturu	docentura	k1gFnSc4	docentura
filosofie	filosofie	k1gFnSc2	filosofie
na	na	k7c6	na
vídeňské	vídeňský	k2eAgFnSc6d1	Vídeňská
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
sňatek	sňatek	k1gInSc1	sňatek
<g/>
,	,	kIx,	,
narychlo	narychlo	k6eAd1	narychlo
uspořádaný	uspořádaný	k2eAgMnSc1d1	uspořádaný
rodinou	rodina	k1gFnSc7	rodina
Charlotty	Charlotta	k1gFnSc2	Charlotta
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
15	[number]	k4	15
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1878	[number]	k4	1878
<g/>
,	,	kIx,	,
pozdrželo	pozdržet	k5eAaPmAgNnS	pozdržet
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
Masaryk	Masaryk	k1gMnSc1	Masaryk
živil	živit	k5eAaImAgMnS	živit
rodinu	rodina	k1gFnSc4	rodina
a	a	k8xC	a
sebe	sebe	k3xPyFc4	sebe
suplováním	suplování	k1gNnSc7	suplování
na	na	k7c6	na
střední	střední	k2eAgFnSc6d1	střední
škole	škola	k1gFnSc6	škola
<g/>
,	,	kIx,	,
přednáškami	přednáška	k1gFnPc7	přednáška
a	a	k8xC	a
kondicemi	kondice	k1gFnPc7	kondice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vypůjčoval	vypůjčovat	k5eAaImAgInS	vypůjčovat
si	se	k3xPyFc3	se
také	také	k6eAd1	také
od	od	k7c2	od
přátel	přítel	k1gMnPc2	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Habilitoval	habilitovat	k5eAaBmAgInS	habilitovat
se	se	k3xPyFc4	se
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1879	[number]	k4	1879
prací	práce	k1gFnPc2	práce
Der	drát	k5eAaImRp2nS	drát
Selbstmord	Selbstmord	k1gInSc1	Selbstmord
als	als	k?	als
soziale	soziale	k6eAd1	soziale
Massenerscheinung	Massenerscheinung	k1gInSc1	Massenerscheinung
der	drát	k5eAaImRp2nS	drát
Gegenwart	Gegenwart	k1gInSc1	Gegenwart
(	(	kIx(	(
<g/>
Sebevražda	sebevražda	k1gFnSc1	sebevražda
jako	jako	k8xC	jako
masový	masový	k2eAgInSc1d1	masový
sociální	sociální	k2eAgInSc1d1	sociální
jev	jev	k1gInSc1	jev
současnosti	současnost	k1gFnSc2	současnost
<g/>
)	)	kIx)	)
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
bezplatně	bezplatně	k6eAd1	bezplatně
přednášet	přednášet	k5eAaImF	přednášet
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
jako	jako	k8xS	jako
soukromý	soukromý	k2eAgMnSc1d1	soukromý
docent	docent	k1gMnSc1	docent
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
habilitační	habilitační	k2eAgFnSc1d1	habilitační
práce	práce	k1gFnSc1	práce
tam	tam	k6eAd1	tam
vyšla	vyjít	k5eAaPmAgFnS	vyjít
roku	rok	k1gInSc2	rok
1881	[number]	k4	1881
a	a	k8xC	a
vzbudila	vzbudit	k5eAaPmAgFnS	vzbudit
zájem	zájem	k1gInSc4	zájem
i	i	k9	i
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1879	[number]	k4	1879
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
dcera	dcera	k1gFnSc1	dcera
Alice	Alice	k1gFnSc1	Alice
a	a	k8xC	a
1880	[number]	k4	1880
syn	syn	k1gMnSc1	syn
Herbert	Herbert	k1gMnSc1	Herbert
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
jako	jako	k9	jako
student	student	k1gMnSc1	student
byl	být	k5eAaImAgMnS	být
Masaryk	Masaryk	k1gMnSc1	Masaryk
literárně	literárně	k6eAd1	literárně
činný	činný	k2eAgMnSc1d1	činný
<g/>
,	,	kIx,	,
napsal	napsat	k5eAaBmAgInS	napsat
řadu	řada	k1gFnSc4	řada
studií	studio	k1gNnPc2	studio
a	a	k8xC	a
odborných	odborný	k2eAgFnPc2d1	odborná
statí	stať	k1gFnPc2	stať
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1875	[number]	k4	1875
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
předsedou	předseda	k1gMnSc7	předseda
Českého	český	k2eAgInSc2d1	český
akademického	akademický	k2eAgInSc2d1	akademický
spolku	spolek	k1gInSc2	spolek
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
se	se	k3xPyFc4	se
pravidelně	pravidelně	k6eAd1	pravidelně
stýkat	stýkat	k5eAaImF	stýkat
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
Aloise	Alois	k1gMnSc2	Alois
Šembery	Šembera	k1gMnSc2	Šembera
(	(	kIx(	(
<g/>
1807	[number]	k4	1807
<g/>
–	–	k?	–
<g/>
1882	[number]	k4	1882
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
profesora	profesor	k1gMnSc4	profesor
české	český	k2eAgFnSc2d1	Česká
řeči	řeč	k1gFnSc2	řeč
a	a	k8xC	a
literatury	literatura	k1gFnSc2	literatura
na	na	k7c6	na
vídeňské	vídeňský	k2eAgFnSc6d1	Vídeňská
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Spor	spor	k1gInSc1	spor
o	o	k7c4	o
pravost	pravost	k1gFnSc4	pravost
rukopisů	rukopis	k1gInPc2	rukopis
Královédvorského	královédvorský	k2eAgNnSc2d1	královédvorské
a	a	k8xC	a
Zelenohorského	zelenohorský	k2eAgNnSc2d1	zelenohorský
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
celonárodní	celonárodní	k2eAgFnSc7d1	celonárodní
aférou	aféra	k1gFnSc7	aféra
<g/>
,	,	kIx,	,
využívanou	využívaný	k2eAgFnSc7d1	využívaná
i	i	k9	i
politicky	politicky	k6eAd1	politicky
<g/>
.	.	kIx.	.
</s>
<s>
Masaryk	Masaryk	k1gMnSc1	Masaryk
se	se	k3xPyFc4	se
postavil	postavit	k5eAaPmAgMnS	postavit
na	na	k7c6	na
samém	samý	k3xTgInSc6	samý
počátku	počátek	k1gInSc6	počátek
sporu	spor	k1gInSc2	spor
1877	[number]	k4	1877
za	za	k7c4	za
vědecké	vědecký	k2eAgNnSc4d1	vědecké
prozkoumání	prozkoumání	k1gNnSc4	prozkoumání
jejich	jejich	k3xOp3gInSc2	jejich
původu	původ	k1gInSc2	původ
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
českých	český	k2eAgMnPc2d1	český
vlastenců	vlastenec	k1gMnPc2	vlastenec
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Jan	Jan	k1gMnSc1	Jan
Neruda	Neruda	k1gMnSc1	Neruda
<g/>
,	,	kIx,	,
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
Eliška	Eliška	k1gFnSc1	Eliška
Krásnohorská	krásnohorský	k2eAgFnSc1d1	Krásnohorská
nebo	nebo	k8xC	nebo
básník	básník	k1gMnSc1	básník
Adolf	Adolf	k1gMnSc1	Adolf
Heyduk	Heyduk	k1gMnSc1	Heyduk
<g/>
)	)	kIx)	)
stal	stát	k5eAaPmAgInS	stát
dokonce	dokonce	k9	dokonce
"	"	kIx"	"
<g/>
vlastizrádcem	vlastizrádce	k1gMnSc7	vlastizrádce
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
poprvé	poprvé	k6eAd1	poprvé
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
rozporu	rozpor	k1gInSc2	rozpor
s	s	k7c7	s
obecným	obecný	k2eAgInSc7d1	obecný
názorem	názor	k1gInSc7	názor
a	a	k8xC	a
šel	jít	k5eAaImAgMnS	jít
proti	proti	k7c3	proti
názoru	názor	k1gInSc3	názor
"	"	kIx"	"
<g/>
Otce	Otka	k1gFnSc3	Otka
národa	národ	k1gInSc2	národ
<g/>
"	"	kIx"	"
Františka	František	k1gMnSc2	František
Palackého	Palacký	k1gMnSc2	Palacký
<g/>
.	.	kIx.	.
</s>
<s>
Rukopisné	rukopisný	k2eAgFnPc1d1	rukopisná
spory	spora	k1gFnPc1	spora
pak	pak	k6eAd1	pak
rušily	rušit	k5eAaImAgFnP	rušit
i	i	k9	i
relativní	relativní	k2eAgInSc4d1	relativní
klid	klid	k1gInSc4	klid
a	a	k8xC	a
pohodu	pohoda	k1gFnSc4	pohoda
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
měl	mít	k5eAaImAgMnS	mít
Masaryk	Masaryk	k1gMnSc1	Masaryk
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
příchodu	příchod	k1gInSc6	příchod
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
v	v	k7c6	v
období	období	k1gNnSc6	období
1882	[number]	k4	1882
až	až	k9	až
1884	[number]	k4	1884
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
po	po	k7c4	po
13	[number]	k4	13
<g/>
.	.	kIx.	.
únoru	únor	k1gInSc3	únor
1886	[number]	k4	1886
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
Athenaeu	Athenaeum	k1gNnSc6	Athenaeum
vyšel	vyjít	k5eAaPmAgMnS	vyjít
s	s	k7c7	s
Masarykovým	Masarykův	k2eAgNnSc7d1	Masarykovo
doporučením	doporučení	k1gNnSc7	doporučení
Gebauerův	Gebauerův	k2eAgInSc4d1	Gebauerův
článek	článek	k1gInSc4	článek
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
žádá	žádat	k5eAaImIp3nS	žádat
důkladné	důkladný	k2eAgFnPc4d1	důkladná
jazykové	jazykový	k2eAgFnPc4d1	jazyková
<g/>
,	,	kIx,	,
estetické	estetický	k2eAgFnPc4d1	estetická
<g/>
,	,	kIx,	,
historické	historický	k2eAgFnPc4d1	historická
<g/>
,	,	kIx,	,
paleografické	paleografický	k2eAgFnPc4d1	paleografická
i	i	k8xC	i
chemické	chemický	k2eAgFnPc4d1	chemická
analýzy	analýza	k1gFnPc4	analýza
rukopisů	rukopis	k1gInPc2	rukopis
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
rozdělení	rozdělení	k1gNnSc3	rozdělení
Univerzity	univerzita	k1gFnSc2	univerzita
Karlo-Ferdinandovy	Karlo-Ferdinandův	k2eAgFnSc2d1	Karlo-Ferdinandova
na	na	k7c4	na
českou	český	k2eAgFnSc4d1	Česká
a	a	k8xC	a
německou	německý	k2eAgFnSc4d1	německá
část	část	k1gFnSc4	část
dostal	dostat	k5eAaPmAgMnS	dostat
místo	místo	k7c2	místo
mimořádného	mimořádný	k2eAgMnSc2d1	mimořádný
profesora	profesor	k1gMnSc2	profesor
filosofie	filosofie	k1gFnSc2	filosofie
na	na	k7c6	na
nově	nově	k6eAd1	nově
založené	založený	k2eAgFnSc6d1	založená
české	český	k2eAgFnSc6d1	Česká
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
1882	[number]	k4	1882
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
odstěhoval	odstěhovat	k5eAaPmAgMnS	odstěhovat
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
zde	zde	k6eAd1	zde
přednášet	přednášet	k5eAaImF	přednášet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
prostředí	prostředí	k1gNnSc6	prostředí
–	–	k?	–
mnohdy	mnohdy	k6eAd1	mnohdy
ještě	ještě	k6eAd1	ještě
provinčním	provinční	k2eAgMnPc3d1	provinční
a	a	k8xC	a
politicky	politicky	k6eAd1	politicky
určovaném	určovaný	k2eAgNnSc6d1	určované
ambicemi	ambice	k1gFnPc7	ambice
českého	český	k2eAgInSc2d1	český
nacionalismu	nacionalismus	k1gInSc2	nacionalismus
–	–	k?	–
si	se	k3xPyFc3	se
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
mnoho	mnoho	k4c4	mnoho
odpůrců	odpůrce	k1gMnPc2	odpůrce
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
svým	svůj	k3xOyFgInSc7	svůj
otevřeným	otevřený	k2eAgInSc7d1	otevřený
<g/>
,	,	kIx,	,
rovnostářským	rovnostářský	k2eAgInSc7d1	rovnostářský
přístupem	přístup	k1gInSc7	přístup
ke	k	k7c3	k
studentům	student	k1gMnPc3	student
<g/>
,	,	kIx,	,
kritikou	kritika	k1gFnSc7	kritika
romantického	romantický	k2eAgNnSc2d1	romantické
vlastenčení	vlastenčení	k1gNnSc2	vlastenčení
a	a	k8xC	a
přísně	přísně	k6eAd1	přísně
vědeckým	vědecký	k2eAgInSc7d1	vědecký
přístupem	přístup	k1gInSc7	přístup
k	k	k7c3	k
vykládaným	vykládaný	k2eAgNnPc3d1	vykládané
tématům	téma	k1gNnPc3	téma
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
u	u	k7c2	u
konzervativních	konzervativní	k2eAgMnPc2d1	konzervativní
katolíků	katolík	k1gMnPc2	katolík
a	a	k8xC	a
představitelů	představitel	k1gMnPc2	představitel
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
svými	svůj	k3xOyFgMnPc7	svůj
názory	názor	k1gInPc4	názor
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Masaryk	Masaryk	k1gMnSc1	Masaryk
z	z	k7c2	z
racionálních	racionální	k2eAgFnPc2d1	racionální
pozic	pozice	k1gFnPc2	pozice
kritizoval	kritizovat	k5eAaImAgInS	kritizovat
některé	některý	k3yIgInPc4	některý
rysy	rys	k1gInPc4	rys
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
její	její	k3xOp3gInSc1	její
centralismus	centralismus	k1gInSc1	centralismus
a	a	k8xC	a
dogma	dogma	k1gNnSc1	dogma
o	o	k7c6	o
papežské	papežský	k2eAgFnSc6d1	Papežská
neomylnosti	neomylnost	k1gFnSc6	neomylnost
<g/>
.	.	kIx.	.
</s>
<s>
Viděl	vidět	k5eAaImAgMnS	vidět
nutnost	nutnost	k1gFnSc4	nutnost
náboženství	náboženství	k1gNnSc2	náboženství
pro	pro	k7c4	pro
moderní	moderní	k2eAgFnSc4d1	moderní
společnost	společnost	k1gFnSc4	společnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
náboženství	náboženství	k1gNnSc4	náboženství
chápal	chápat	k5eAaImAgMnS	chápat
jako	jako	k9	jako
vnitřní	vnitřní	k2eAgInSc4d1	vnitřní
a	a	k8xC	a
mravní	mravní	k2eAgInSc4d1	mravní
postoj	postoj	k1gInSc4	postoj
bez	bez	k7c2	bez
mocenských	mocenský	k2eAgFnPc2d1	mocenská
ambicí	ambice	k1gFnPc2	ambice
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
ho	on	k3xPp3gNnSc4	on
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
seznámil	seznámit	k5eAaPmAgMnS	seznámit
Brentano	Brentana	k1gFnSc5	Brentana
a	a	k8xC	a
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
zažil	zažít	k5eAaPmAgMnS	zažít
zejména	zejména	k9	zejména
v	v	k7c6	v
americkém	americký	k2eAgNnSc6d1	americké
prostředí	prostředí	k1gNnSc6	prostředí
své	svůj	k3xOyFgFnSc2	svůj
ženy	žena	k1gFnSc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
činorodý	činorodý	k2eAgMnSc1d1	činorodý
člověk	člověk	k1gMnSc1	člověk
se	s	k7c7	s
širokým	široký	k2eAgInSc7d1	široký
rozhledem	rozhled	k1gInSc7	rozhled
a	a	k8xC	a
světovými	světový	k2eAgFnPc7d1	světová
zkušenostmi	zkušenost	k1gFnPc7	zkušenost
si	se	k3xPyFc3	se
ale	ale	k8xC	ale
získal	získat	k5eAaPmAgMnS	získat
mnoho	mnoho	k4c4	mnoho
přátel	přítel	k1gMnPc2	přítel
mezi	mezi	k7c7	mezi
svými	svůj	k3xOyFgMnPc7	svůj
studenty	student	k1gMnPc7	student
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
v	v	k7c6	v
okruhu	okruh	k1gInSc6	okruh
lidí	člověk	k1gMnPc2	člověk
kolem	kolem	k7c2	kolem
revue	revue	k1gFnSc2	revue
Athenaeum	Athenaeum	k1gInSc1	Athenaeum
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
založil	založit	k5eAaPmAgMnS	založit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jejích	její	k3xOp3gFnPc6	její
stránkách	stránka	k1gFnPc6	stránka
poukazoval	poukazovat	k5eAaImAgInS	poukazovat
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1885	[number]	k4	1885
na	na	k7c4	na
nutnost	nutnost	k1gFnSc4	nutnost
zřízení	zřízení	k1gNnSc2	zřízení
druhé	druhý	k4xOgFnSc2	druhý
české	český	k2eAgFnSc2d1	Česká
univerzity	univerzita	k1gFnSc2	univerzita
(	(	kIx(	(
<g/>
z	z	k7c2	z
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
,	,	kIx,	,
založené	založený	k2eAgInPc4d1	založený
roku	rok	k1gInSc2	rok
1573	[number]	k4	1573
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
fungovala	fungovat	k5eAaImAgFnS	fungovat
pouze	pouze	k6eAd1	pouze
Teologická	teologický	k2eAgFnSc1d1	teologická
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Masaryk	Masaryk	k1gMnSc1	Masaryk
začal	začít	k5eAaPmAgMnS	začít
svou	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
našel	najít	k5eAaPmAgMnS	najít
spolupracovníky	spolupracovník	k1gMnPc4	spolupracovník
v	v	k7c4	v
Josefu	Josefa	k1gFnSc4	Josefa
Kaizlovi	Kaizl	k1gMnSc3	Kaizl
a	a	k8xC	a
Karlu	Karel	k1gMnSc3	Karel
Kramářovi	kramář	k1gMnSc3	kramář
a	a	k8xC	a
formuloval	formulovat	k5eAaImAgInS	formulovat
nový	nový	k2eAgInSc4d1	nový
politický	politický	k2eAgInSc4d1	politický
směr	směr	k1gInSc4	směr
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
prosazoval	prosazovat	k5eAaImAgInS	prosazovat
"	"	kIx"	"
<g/>
přesné	přesný	k2eAgNnSc4d1	přesné
vědecké	vědecký	k2eAgNnSc4d1	vědecké
poznávání	poznávání	k1gNnSc4	poznávání
věcí	věc	k1gFnPc2	věc
proti	proti	k7c3	proti
romantické	romantický	k2eAgFnSc3d1	romantická
fantastice	fantastika	k1gFnSc3	fantastika
<g/>
"	"	kIx"	"
–	–	k?	–
realismus	realismus	k1gInSc1	realismus
<g/>
.	.	kIx.	.
</s>
<s>
Revue	revue	k1gFnSc1	revue
Čas	čas	k1gInSc1	čas
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
1886	[number]	k4	1886
tribunou	tribuna	k1gFnSc7	tribuna
realistů	realista	k1gMnPc2	realista
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
předávali	předávat	k5eAaImAgMnP	předávat
své	svůj	k3xOyFgInPc4	svůj
názory	názor	k1gInPc4	názor
a	a	k8xC	a
postoje	postoj	k1gInPc4	postoj
poměrně	poměrně	k6eAd1	poměrně
úzkému	úzký	k2eAgInSc3d1	úzký
okruhu	okruh	k1gInSc3	okruh
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
podstatně	podstatně	k6eAd1	podstatně
širšímu	široký	k2eAgNnSc3d2	širší
než	než	k8xS	než
intelektuálního	intelektuální	k2eAgNnSc2d1	intelektuální
periodika	periodikum	k1gNnSc2	periodikum
Athenaeum	Athenaeum	k1gInSc1	Athenaeum
<g/>
.	.	kIx.	.
</s>
<s>
Profesoru	profesor	k1gMnSc3	profesor
Václavu	Václav	k1gMnSc3	Václav
Vlčkovi	Vlček	k1gMnSc3	Vlček
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gFnSc6	jehož
vile	vila	k1gFnSc6	vila
"	"	kIx"	"
<g/>
Osvěta	osvěta	k1gFnSc1	osvěta
<g/>
"	"	kIx"	"
nad	nad	k7c7	nad
Nuselským	nuselský	k2eAgNnSc7d1	Nuselské
údolím	údolí	k1gNnSc7	údolí
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1884	[number]	k4	1884
bydlel	bydlet	k5eAaImAgInS	bydlet
<g/>
,	,	kIx,	,
pomáhal	pomáhat	k5eAaImAgInS	pomáhat
založit	založit	k5eAaPmF	založit
a	a	k8xC	a
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
časopis	časopis	k1gInSc4	časopis
Osvěta	osvěta	k1gFnSc1	osvěta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
vile	vila	k1gFnSc6	vila
se	s	k7c7	s
14	[number]	k4	14
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1886	[number]	k4	1886
narodil	narodit	k5eAaPmAgMnS	narodit
jeho	jeho	k3xOp3gMnSc1	jeho
mladší	mladý	k2eAgMnSc1d2	mladší
syn	syn	k1gMnSc1	syn
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
však	však	k9	však
propukly	propuknout	k5eAaPmAgInP	propuknout
rukopisné	rukopisný	k2eAgInPc1d1	rukopisný
spory	spor	k1gInPc1	spor
<g/>
,	,	kIx,	,
právě	právě	k9	právě
kolem	kolem	k7c2	kolem
tohoto	tento	k3xDgInSc2	tento
měsíčníku	měsíčník	k1gInSc2	měsíčník
"	"	kIx"	"
<g/>
Osvěta	osvěta	k1gFnSc1	osvěta
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
zformovala	zformovat	k5eAaPmAgFnS	zformovat
skupina	skupina	k1gFnSc1	skupina
obránců	obránce	k1gMnPc2	obránce
rukopisů	rukopis	k1gInPc2	rukopis
vedená	vedený	k2eAgFnSc1d1	vedená
profesory	profesor	k1gMnPc7	profesor
historie	historie	k1gFnSc2	historie
V.	V.	kA	V.
V.	V.	kA	V.
Tomkem	Tomek	k1gMnSc7	Tomek
a	a	k8xC	a
J.	J.	kA	J.
Kalouskem	Kalousek	k1gMnSc7	Kalousek
a	a	k8xC	a
Masaryk	Masaryk	k1gMnSc1	Masaryk
musel	muset	k5eAaImAgMnS	muset
Vlčkovu	Vlčkův	k2eAgFnSc4d1	Vlčkova
vilu	vila	k1gFnSc4	vila
opustit	opustit	k5eAaPmF	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
vzdal	vzdát	k5eAaPmAgMnS	vzdát
funkce	funkce	k1gFnSc2	funkce
vedoucího	vedoucí	k2eAgMnSc2d1	vedoucí
redaktora	redaktor	k1gMnSc2	redaktor
Ottova	Ottův	k2eAgInSc2d1	Ottův
slovníku	slovník	k1gInSc2	slovník
naučného	naučný	k2eAgInSc2d1	naučný
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
neohrozil	ohrozit	k5eNaPmAgInS	ohrozit
jeho	jeho	k3xOp3gNnSc4	jeho
vycházení	vycházení	k1gNnSc4	vycházení
<g/>
.	.	kIx.	.
</s>
<s>
Masaryk	Masaryk	k1gMnSc1	Masaryk
hledal	hledat	k5eAaImAgMnS	hledat
politické	politický	k2eAgNnSc4d1	politické
uskupení	uskupení	k1gNnSc4	uskupení
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
prosazovat	prosazovat	k5eAaImF	prosazovat
své	svůj	k3xOyFgFnPc4	svůj
zásady	zásada	k1gFnPc4	zásada
a	a	k8xC	a
názory	názor	k1gInPc4	názor
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
neúspěšných	úspěšný	k2eNgNnPc6d1	neúspěšné
jednáních	jednání	k1gNnPc6	jednání
realistů	realista	k1gMnPc2	realista
se	s	k7c7	s
Staročechy	Staročech	k1gMnPc7	Staročech
byli	být	k5eAaImAgMnP	být
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1890	[number]	k4	1890
přijati	přijmout	k5eAaPmNgMnP	přijmout
k	k	k7c3	k
Mladočechům	mladočech	k1gMnPc3	mladočech
<g/>
,	,	kIx,	,
s	s	k7c7	s
nimiž	jenž	k3xRgInPc7	jenž
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1891	[number]	k4	1891
získali	získat	k5eAaPmAgMnP	získat
mandáty	mandát	k1gInPc4	mandát
do	do	k7c2	do
Říšské	říšský	k2eAgFnSc2d1	říšská
rady	rada	k1gFnSc2	rada
(	(	kIx(	(
<g/>
celorakouského	celorakouský	k2eAgInSc2d1	celorakouský
parlamentu	parlament	k1gInSc2	parlament
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1891	[number]	k4	1891
byl	být	k5eAaImAgMnS	být
Masaryk	Masaryk	k1gMnSc1	Masaryk
zvolen	zvolit	k5eAaPmNgMnS	zvolit
do	do	k7c2	do
českého	český	k2eAgInSc2d1	český
zemského	zemský	k2eAgInSc2d1	zemský
sněmu	sněm	k1gInSc2	sněm
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
v	v	k7c6	v
témž	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
předložil	předložit	k5eAaPmAgMnS	předložit
ve	v	k7c6	v
Vídeňské	vídeňský	k2eAgFnSc6d1	Vídeňská
říšské	říšský	k2eAgFnSc6d1	říšská
radě	rada	k1gFnSc6	rada
návrh	návrh	k1gInSc4	návrh
na	na	k7c6	na
zřízení	zřízení	k1gNnSc6	zřízení
druhé	druhý	k4xOgFnSc2	druhý
české	český	k2eAgFnSc2d1	Česká
univerzity	univerzita	k1gFnSc2	univerzita
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Této	tento	k3xDgFnSc3	tento
otázce	otázka	k1gFnSc3	otázka
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgMnS	věnovat
i	i	k9	i
v	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
letech	léto	k1gNnPc6	léto
–	–	k?	–
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
článcích	článek	k1gInPc6	článek
a	a	k8xC	a
sháněním	shánění	k1gNnSc7	shánění
podpory	podpora	k1gFnSc2	podpora
mezi	mezi	k7c7	mezi
vědeckou	vědecký	k2eAgFnSc7d1	vědecká
veřejností	veřejnost	k1gFnSc7	veřejnost
a	a	k8xC	a
studenty	student	k1gMnPc7	student
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1893	[number]	k4	1893
se	se	k3xPyFc4	se
ale	ale	k9	ale
obou	dva	k4xCgInPc2	dva
mandátů	mandát	k1gInPc2	mandát
vzdal	vzdát	k5eAaPmAgInS	vzdát
kvůli	kvůli	k7c3	kvůli
sporům	spor	k1gInPc3	spor
s	s	k7c7	s
radikálním	radikální	k2eAgNnSc7d1	radikální
vedením	vedení	k1gNnSc7	vedení
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Přispěla	přispět	k5eAaPmAgFnS	přispět
také	také	k9	také
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
rodina	rodina	k1gFnSc1	rodina
rozrostla	rozrůst	k5eAaPmAgFnS	rozrůst
o	o	k7c4	o
další	další	k2eAgFnPc4d1	další
dvě	dva	k4xCgFnPc4	dva
děti	dítě	k1gFnPc4	dítě
–	–	k?	–
Jana	Jan	k1gMnSc4	Jan
a	a	k8xC	a
Olgu	Olga	k1gFnSc4	Olga
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1897	[number]	k4	1897
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
řádným	řádný	k2eAgMnSc7d1	řádný
profesorem	profesor	k1gMnSc7	profesor
a	a	k8xC	a
i	i	k9	i
nadále	nadále	k6eAd1	nadále
byl	být	k5eAaImAgInS	být
literárně	literárně	k6eAd1	literárně
činný	činný	k2eAgMnSc1d1	činný
<g/>
.	.	kIx.	.
</s>
<s>
Napsal	napsat	k5eAaBmAgMnS	napsat
mnoho	mnoho	k4c4	mnoho
studií	studio	k1gNnPc2	studio
i	i	k8xC	i
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
statí	stať	k1gFnPc2	stať
<g/>
,	,	kIx,	,
inicioval	iniciovat	k5eAaBmAgInS	iniciovat
vznik	vznik	k1gInSc1	vznik
Ottova	Ottův	k2eAgInSc2d1	Ottův
slovníku	slovník	k1gInSc2	slovník
naučného	naučný	k2eAgInSc2d1	naučný
a	a	k8xC	a
také	také	k9	také
psal	psát	k5eAaImAgInS	psát
články	článek	k1gInPc7	článek
do	do	k7c2	do
odborných	odborný	k2eAgInPc2d1	odborný
časopisů	časopis	k1gInPc2	časopis
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
byly	být	k5eAaImAgFnP	být
Naše	náš	k3xOp1gFnPc1	náš
doba	doba	k1gFnSc1	doba
nebo	nebo	k8xC	nebo
Čas	čas	k1gInSc1	čas
(	(	kIx(	(
<g/>
který	který	k3yQgMnSc1	který
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1901-1915	[number]	k4	1901-1915
vycházel	vycházet	k5eAaImAgInS	vycházet
jako	jako	k9	jako
deník	deník	k1gInSc1	deník
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejichž	jejichž	k3xOyRp3gInSc6	jejichž
vzniku	vznik	k1gInSc6	vznik
se	se	k3xPyFc4	se
aktivně	aktivně	k6eAd1	aktivně
podílel	podílet	k5eAaImAgMnS	podílet
<g/>
.	.	kIx.	.
</s>
<s>
Masaryk	Masaryk	k1gMnSc1	Masaryk
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
omezený	omezený	k2eAgInSc4d1	omezený
rozhled	rozhled	k1gInSc4	rozhled
a	a	k8xC	a
okruh	okruh	k1gInSc4	okruh
zájmů	zájem	k1gInPc2	zájem
českých	český	k2eAgInPc2d1	český
politických	politický	k2eAgInPc2d1	politický
i	i	k8xC	i
kulturních	kulturní	k2eAgInPc2d1	kulturní
kruhů	kruh	k1gInPc2	kruh
<g/>
.	.	kIx.	.
</s>
<s>
Příčil	příčit	k5eAaImAgMnS	příčit
se	se	k3xPyFc4	se
mu	on	k3xPp3gNnSc3	on
český	český	k2eAgInSc4d1	český
nacionalismus	nacionalismus	k1gInSc4	nacionalismus
a	a	k8xC	a
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
i	i	k9	i
proti	proti	k7c3	proti
pověrám	pověra	k1gFnPc3	pověra
antisemitismu	antisemitismus	k1gInSc2	antisemitismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1899	[number]	k4	1899
se	se	k3xPyFc4	se
zasadil	zasadit	k5eAaPmAgInS	zasadit
o	o	k7c4	o
obnovení	obnovení	k1gNnSc4	obnovení
procesu	proces	k1gInSc2	proces
se	s	k7c7	s
Židem	Žid	k1gMnSc7	Žid
Leopoldem	Leopold	k1gMnSc7	Leopold
Hilsnerem	Hilsner	k1gMnSc7	Hilsner
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgInS	být
odsouzen	odsoudit	k5eAaPmNgMnS	odsoudit
za	za	k7c4	za
vraždu	vražda	k1gFnSc4	vražda
mladé	mladý	k2eAgFnSc2d1	mladá
Češky	Češka	k1gFnSc2	Češka
Anežky	Anežka	k1gFnSc2	Anežka
Hrůzové	Hrůzová	k1gFnSc2	Hrůzová
v	v	k7c6	v
Polné	Polná	k1gFnSc6	Polná
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
pověry	pověra	k1gFnSc2	pověra
<g/>
,	,	kIx,	,
že	že	k8xS	že
Židé	Žid	k1gMnPc1	Žid
užívají	užívat	k5eAaImIp3nP	užívat
krev	krev	k1gFnSc4	krev
mladých	mladý	k2eAgMnPc2d1	mladý
křesťanů	křesťan	k1gMnPc2	křesťan
při	při	k7c6	při
svých	svůj	k3xOyFgInPc6	svůj
obřadech	obřad	k1gInPc6	obřad
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
veřejnosti	veřejnost	k1gFnSc3	veřejnost
vraždu	vražda	k1gFnSc4	vražda
považovala	považovat	k5eAaImAgFnS	považovat
za	za	k7c4	za
"	"	kIx"	"
<g/>
rituální	rituální	k2eAgInSc4d1	rituální
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
za	za	k7c2	za
sporů	spor	k1gInPc2	spor
o	o	k7c4	o
Rukopisy	rukopis	k1gInPc4	rukopis
byl	být	k5eAaImAgMnS	být
opět	opět	k6eAd1	opět
vystaven	vystavit	k5eAaPmNgMnS	vystavit
ostrým	ostrý	k2eAgInPc3d1	ostrý
útokům	útok	k1gInPc3	útok
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
studentů	student	k1gMnPc2	student
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
uvažoval	uvažovat	k5eAaImAgMnS	uvažovat
o	o	k7c6	o
odchodu	odchod	k1gInSc6	odchod
do	do	k7c2	do
země	zem	k1gFnSc2	zem
své	svůj	k3xOyFgFnSc2	svůj
ženy	žena	k1gFnSc2	žena
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Charlotta	Charlotta	k1gFnSc1	Charlotta
jej	on	k3xPp3gMnSc4	on
utvrdila	utvrdit	k5eAaPmAgFnS	utvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gNnSc1	jeho
místo	místo	k1gNnSc1	místo
je	být	k5eAaImIp3nS	být
právě	právě	k9	právě
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Čapkových	Čapkových	k2eAgInPc6d1	Čapkových
"	"	kIx"	"
<g/>
Hovorech	hovor	k1gInPc6	hovor
<g/>
"	"	kIx"	"
Masaryk	Masaryk	k1gMnSc1	Masaryk
vzpomíná	vzpomínat	k5eAaImIp3nS	vzpomínat
na	na	k7c4	na
proces	proces	k1gInSc4	proces
v	v	k7c6	v
Polné	Polná	k1gFnSc6	Polná
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
hilsneriádu	hilsneriáda	k1gFnSc4	hilsneriáda
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Zlá	zlý	k2eAgFnSc1d1	zlá
kampaň	kampaň	k1gFnSc1	kampaň
byla	být	k5eAaImAgFnS	být
ta	ten	k3xDgFnSc1	ten
"	"	kIx"	"
<g/>
hilsneriáda	hilsneriáda	k1gFnSc1	hilsneriáda
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
když	když	k8xS	když
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
musel	muset	k5eAaImAgMnS	muset
rvát	rvát	k5eAaImF	rvát
s	s	k7c7	s
pověrou	pověra	k1gFnSc7	pověra
o	o	k7c6	o
rituální	rituální	k2eAgFnSc6d1	rituální
vraždě	vražda	k1gFnSc6	vražda
<g/>
.	.	kIx.	.
</s>
<s>
Já	já	k3xPp1nSc1	já
jsem	být	k5eAaImIp1nS	být
se	s	k7c7	s
zprvu	zprvu	k6eAd1	zprvu
o	o	k7c4	o
ten	ten	k3xDgInSc4	ten
Hilsnerův	Hilsnerův	k2eAgInSc4d1	Hilsnerův
proces	proces	k1gInSc4	proces
nezajímal	zajímat	k5eNaImAgMnS	zajímat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přijel	přijet	k5eAaPmAgMnS	přijet
za	za	k7c7	za
mnou	já	k3xPp1nSc7	já
můj	můj	k3xOp1gMnSc1	můj
bývalý	bývalý	k2eAgMnSc1d1	bývalý
žák	žák	k1gMnSc1	žák
z	z	k7c2	z
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
Sigismund	Sigismund	k1gMnSc1	Sigismund
Münz	Münz	k1gMnSc1	Münz
<g/>
,	,	kIx,	,
Moravan	Moravan	k1gMnSc1	Moravan
<g/>
,	,	kIx,	,
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
mě	já	k3xPp1nSc4	já
přiměl	přimět	k5eAaPmAgMnS	přimět
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsem	být	k5eAaImIp1nS	být
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
rituální	rituální	k2eAgFnSc6d1	rituální
pověře	pověra	k1gFnSc6	pověra
jsem	být	k5eAaImIp1nS	být
znal	znát	k5eAaImAgMnS	znát
knihy	kniha	k1gFnSc2	kniha
berlínského	berlínský	k2eAgMnSc2d1	berlínský
teologa	teolog	k1gMnSc2	teolog
Starcka	Starcko	k1gNnSc2	Starcko
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vypsal	vypsat	k5eAaPmAgInS	vypsat
vznik	vznik	k1gInSc4	vznik
a	a	k8xC	a
historii	historie	k1gFnSc4	historie
té	ten	k3xDgFnSc2	ten
pověry	pověra	k1gFnSc2	pověra
<g/>
.	.	kIx.	.
</s>
<s>
Řekl	říct	k5eAaPmAgMnS	říct
jsem	být	k5eAaImIp1nS	být
panu	pan	k1gMnSc3	pan
Münzovi	Münzův	k2eAgMnPc1d1	Münzův
svůj	svůj	k3xOyFgInSc4	svůj
názor	názor	k1gInSc4	názor
o	o	k7c4	o
věci	věc	k1gFnPc4	věc
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
to	ten	k3xDgNnSc4	ten
oznámil	oznámit	k5eAaPmAgMnS	oznámit
veřejnosti	veřejnost	k1gFnSc2	veřejnost
v	v	k7c6	v
Neue	Neue	k1gFnSc6	Neue
Freie	Freie	k1gFnSc2	Freie
Presse	Presse	k1gFnSc2	Presse
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
mely	mely	k?	mely
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Krom	krom	k7c2	krom
jiného	jiné	k1gNnSc2	jiné
sepsal	sepsat	k5eAaPmAgMnS	sepsat
a	a	k8xC	a
vydal	vydat	k5eAaPmAgMnS	vydat
dvě	dva	k4xCgFnPc4	dva
brožury	brožura	k1gFnPc4	brožura
o	o	k7c6	o
nutnosti	nutnost	k1gFnSc6	nutnost
revize	revize	k1gFnSc1	revize
polenského	polenský	k2eAgInSc2d1	polenský
procesu	proces	k1gInSc2	proces
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
jsou	být	k5eAaImIp3nP	být
pracemi	práce	k1gFnPc7	práce
téměř	téměř	k6eAd1	téměř
detektivními	detektivní	k2eAgFnPc7d1	detektivní
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
března	březen	k1gInSc2	březen
a	a	k8xC	a
dubna	duben	k1gInSc2	duben
1900	[number]	k4	1900
založil	založit	k5eAaPmAgMnS	založit
Masaryk	Masaryk	k1gMnSc1	Masaryk
Českou	český	k2eAgFnSc4d1	Česká
stranu	strana	k1gFnSc4	strana
lidovou	lidový	k2eAgFnSc4d1	lidová
–	–	k?	–
pokrokovou	pokrokový	k2eAgFnSc4d1	pokroková
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
známou	známý	k2eAgFnSc4d1	známá
jako	jako	k8xC	jako
Realistická	realistický	k2eAgFnSc1d1	realistická
strana	strana	k1gFnSc1	strana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
nejen	nejen	k6eAd1	nejen
usilovně	usilovně	k6eAd1	usilovně
publikoval	publikovat	k5eAaBmAgMnS	publikovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
přednášel	přednášet	k5eAaImAgMnS	přednášet
v	v	k7c6	v
různých	různý	k2eAgNnPc6d1	různé
městech	město	k1gNnPc6	město
mimo	mimo	k7c4	mimo
fakultu	fakulta	k1gFnSc4	fakulta
(	(	kIx(	(
<g/>
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
,	,	kIx,	,
Skuteč	Skuteč	k1gFnSc1	Skuteč
<g/>
,	,	kIx,	,
Sušice	Sušice	k1gFnSc1	Sušice
<g/>
,	,	kIx,	,
Strakonice	Strakonice	k1gFnPc1	Strakonice
<g/>
,	,	kIx,	,
Písek	Písek	k1gInSc1	Písek
<g/>
,	,	kIx,	,
Vsetín	Vsetín	k1gInSc1	Vsetín
<g/>
,	,	kIx,	,
Valašské	valašský	k2eAgNnSc1d1	Valašské
Meziříčí	Meziříčí	k1gNnSc1	Meziříčí
<g/>
,	,	kIx,	,
Kopřivnice	Kopřivnice	k1gFnSc1	Kopřivnice
<g/>
,	,	kIx,	,
Kroměříž	Kroměříž	k1gFnSc1	Kroměříž
<g/>
,	,	kIx,	,
Modřany	Modřany	k1gInPc1	Modřany
<g/>
,	,	kIx,	,
Kolín	Kolín	k1gInSc1	Kolín
<g/>
,	,	kIx,	,
Velim	Velim	k?	Velim
<g/>
,	,	kIx,	,
Jičín	Jičín	k1gInSc1	Jičín
<g/>
,	,	kIx,	,
Mělník	Mělník	k1gInSc1	Mělník
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
V	v	k7c6	v
letních	letní	k2eAgInPc6d1	letní
měsících	měsíc	k1gInPc6	měsíc
roku	rok	k1gInSc2	rok
1902	[number]	k4	1902
přednášel	přednášet	k5eAaImAgMnS	přednášet
na	na	k7c4	na
pozvání	pozvání	k1gNnSc4	pozvání
milionáře	milionář	k1gMnSc4	milionář
Charlese	Charles	k1gMnSc2	Charles
R.	R.	kA	R.
Cranea	Craneus	k1gMnSc2	Craneus
na	na	k7c6	na
Chicagské	chicagský	k2eAgFnSc6d1	Chicagská
univerzitě	univerzita	k1gFnSc6	univerzita
a	a	k8xC	a
uspořádal	uspořádat	k5eAaPmAgMnS	uspořádat
mnoho	mnoho	k4c4	mnoho
veřejných	veřejný	k2eAgNnPc2d1	veřejné
vystoupení	vystoupení	k1gNnPc2	vystoupení
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
americké	americký	k2eAgMnPc4d1	americký
Čechy	Čech	k1gMnPc4	Čech
a	a	k8xC	a
Slováky	Slovák	k1gMnPc4	Slovák
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1907	[number]	k4	1907
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgMnS	účastnit
kongresu	kongres	k1gInSc2	kongres
o	o	k7c6	o
náboženské	náboženský	k2eAgFnSc6d1	náboženská
svobodě	svoboda	k1gFnSc6	svoboda
v	v	k7c6	v
Bostonu	Boston	k1gInSc6	Boston
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1907	[number]	k4	1907
a	a	k8xC	a
1911	[number]	k4	1911
byl	být	k5eAaImAgMnS	být
dvakrát	dvakrát	k6eAd1	dvakrát
zvolen	zvolit	k5eAaPmNgMnS	zvolit
poslancem	poslanec	k1gMnSc7	poslanec
vídeňské	vídeňský	k2eAgFnSc2d1	Vídeňská
Říšské	říšský	k2eAgFnSc2d1	říšská
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1909	[number]	k4	1909
odhalil	odhalit	k5eAaPmAgInS	odhalit
domnělé	domnělý	k2eAgInPc4d1	domnělý
dokumenty	dokument	k1gInPc4	dokument
o	o	k7c6	o
velezradě	velezrada	k1gFnSc6	velezrada
Chorvatů	Chorvat	k1gMnPc2	Chorvat
a	a	k8xC	a
Srbů	Srb	k1gMnPc2	Srb
jako	jako	k8xS	jako
podvrhy	podvrh	k1gInPc1	podvrh
z	z	k7c2	z
dílny	dílna	k1gFnSc2	dílna
ministra	ministr	k1gMnSc2	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
von	von	k1gInSc1	von
Aehrenthala	Aehrenthal	k1gMnSc4	Aehrenthal
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
jimi	on	k3xPp3gMnPc7	on
chtěl	chtít	k5eAaImAgMnS	chtít
legitimovat	legitimovat	k5eAaBmF	legitimovat
rakouskou	rakouský	k2eAgFnSc4d1	rakouská
anexi	anexe	k1gFnSc4	anexe
Bosny	Bosna	k1gFnSc2	Bosna
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1912	[number]	k4	1912
předložil	předložit	k5eAaPmAgInS	předložit
návrh	návrh	k1gInSc4	návrh
na	na	k7c4	na
zřízení	zřízení	k1gNnSc4	zřízení
druhé	druhý	k4xOgFnSc2	druhý
české	český	k2eAgFnSc2d1	Česká
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
této	tento	k3xDgFnSc2	tento
interpelace	interpelace	k1gFnSc2	interpelace
přiložil	přiložit	k5eAaPmAgMnS	přiložit
přes	přes	k7c4	přes
sedm	sedm	k4xCc4	sedm
tisíc	tisíc	k4xCgInPc2	tisíc
petic	petice	k1gFnPc2	petice
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
česká	český	k2eAgNnPc1d1	české
<g/>
,	,	kIx,	,
moravská	moravský	k2eAgNnPc1d1	Moravské
i	i	k8xC	i
slovenská	slovenský	k2eAgNnPc1d1	slovenské
města	město	k1gNnPc1	město
a	a	k8xC	a
obce	obec	k1gFnPc1	obec
vyslovily	vyslovit	k5eAaPmAgFnP	vyslovit
pro	pro	k7c4	pro
její	její	k3xOp3gNnSc4	její
urychlené	urychlený	k2eAgNnSc4d1	urychlené
zřízení	zřízení	k1gNnSc4	zřízení
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1913	[number]	k4	1913
začal	začít	k5eAaPmAgMnS	začít
v	v	k7c6	v
němčině	němčina	k1gFnSc6	němčina
vydávat	vydávat	k5eAaPmF	vydávat
své	svůj	k3xOyFgNnSc4	svůj
nejrozsáhlejší	rozsáhlý	k2eAgNnSc4d3	nejrozsáhlejší
dílo	dílo	k1gNnSc4	dílo
Rusko	Rusko	k1gNnSc1	Rusko
a	a	k8xC	a
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
,	,	kIx,	,
hojně	hojně	k6eAd1	hojně
překládané	překládaný	k2eAgFnPc1d1	překládaná
a	a	k8xC	a
stále	stále	k6eAd1	stále
citované	citovaný	k2eAgNnSc1d1	citované
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
založeno	založit	k5eAaPmNgNnS	založit
nejen	nejen	k6eAd1	nejen
na	na	k7c6	na
důkladném	důkladný	k2eAgNnSc6d1	důkladné
studiu	studio	k1gNnSc6	studio
ruské	ruský	k2eAgFnSc2d1	ruská
odborné	odborný	k2eAgFnSc2d1	odborná
i	i	k8xC	i
krásné	krásný	k2eAgFnSc2d1	krásná
literatury	literatura	k1gFnSc2	literatura
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
na	na	k7c6	na
osobních	osobní	k2eAgInPc6d1	osobní
setkáních	setkání	k1gNnPc6	setkání
a	a	k8xC	a
vlastních	vlastní	k2eAgFnPc6d1	vlastní
zkušenostech	zkušenost	k1gFnPc6	zkušenost
z	z	k7c2	z
cest	cesta	k1gFnPc2	cesta
do	do	k7c2	do
Ruska	Rusko	k1gNnSc2	Rusko
(	(	kIx(	(
<g/>
1888	[number]	k4	1888
<g/>
,	,	kIx,	,
1889	[number]	k4	1889
<g/>
,	,	kIx,	,
1910	[number]	k4	1910
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
jej	on	k3xPp3gMnSc4	on
zastihla	zastihnout	k5eAaPmAgFnS	zastihnout
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1914	[number]	k4	1914
na	na	k7c6	na
dovolené	dovolená	k1gFnSc6	dovolená
v	v	k7c6	v
saském	saský	k2eAgMnSc6d1	saský
Bad	Bad	k1gMnSc6	Bad
Schandau	Schanda	k1gMnSc6	Schanda
<g/>
.	.	kIx.	.
</s>
<s>
Viděl	vidět	k5eAaImAgMnS	vidět
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
Němci	Němec	k1gMnPc1	Němec
rukují	rukovat	k5eAaImIp3nP	rukovat
s	s	k7c7	s
nadšením	nadšení	k1gNnSc7	nadšení
a	a	k8xC	a
"	"	kIx"	"
<g/>
spořádaně	spořádaně	k6eAd1	spořádaně
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
u	u	k7c2	u
Čechů	Čech	k1gMnPc2	Čech
nepozoroval	pozorovat	k5eNaImAgMnS	pozorovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
špatných	špatný	k2eAgFnPc6d1	špatná
zkušenostech	zkušenost	k1gFnPc6	zkušenost
s	s	k7c7	s
Rakouskou	rakouský	k2eAgFnSc7d1	rakouská
vládou	vláda	k1gFnSc7	vláda
a	a	k8xC	a
zahraniční	zahraniční	k2eAgFnSc7d1	zahraniční
politikou	politika	k1gFnSc7	politika
z	z	k7c2	z
Říšské	říšský	k2eAgFnSc2d1	říšská
rady	rada	k1gFnSc2	rada
<g/>
,	,	kIx,	,
Masaryk	Masaryk	k1gMnSc1	Masaryk
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
opustil	opustit	k5eAaPmAgMnS	opustit
svůj	svůj	k3xOyFgInSc4	svůj
původní	původní	k2eAgInSc4d1	původní
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1	Rakousko-Uhersko
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
reformovat	reformovat	k5eAaBmF	reformovat
do	do	k7c2	do
moderní	moderní	k2eAgFnSc2d1	moderní
federace	federace	k1gFnSc2	federace
<g/>
,	,	kIx,	,
svazku	svazek	k1gInSc2	svazek
autonomních	autonomní	k2eAgFnPc2d1	autonomní
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
naopak	naopak	k6eAd1	naopak
uvažovat	uvažovat	k5eAaImF	uvažovat
a	a	k8xC	a
připravovat	připravovat	k5eAaImF	připravovat
nezávislost	nezávislost	k1gFnSc4	nezávislost
národů	národ	k1gInPc2	národ
a	a	k8xC	a
vystoupení	vystoupení	k1gNnSc4	vystoupení
českého	český	k2eAgInSc2d1	český
národa	národ	k1gInSc2	národ
ze	z	k7c2	z
svazku	svazek	k1gInSc2	svazek
Rakouska-Uherska	Rakouska-Uhersk	k1gInSc2	Rakouska-Uhersk
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
"	"	kIx"	"
<g/>
Nová	nový	k2eAgFnSc1d1	nová
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
:	:	kIx,	:
Stanovisko	stanovisko	k1gNnSc1	stanovisko
slovanské	slovanský	k2eAgFnSc2d1	Slovanská
<g/>
"	"	kIx"	"
ospravedlňoval	ospravedlňovat	k5eAaImAgInS	ospravedlňovat
boj	boj	k1gInSc1	boj
proti	proti	k7c3	proti
monarchii	monarchie	k1gFnSc3	monarchie
<g/>
,	,	kIx,	,
nutnost	nutnost	k1gFnSc4	nutnost
lepšího	dobrý	k2eAgNnSc2d2	lepší
státoprávního	státoprávní	k2eAgNnSc2d1	státoprávní
uspořádání	uspořádání	k1gNnSc2	uspořádání
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
a	a	k8xC	a
navázal	navázat	k5eAaPmAgMnS	navázat
na	na	k7c4	na
tradici	tradice	k1gFnSc4	tradice
úvah	úvaha	k1gFnPc2	úvaha
o	o	k7c6	o
možném	možný	k2eAgNnSc6d1	možné
uspořádání	uspořádání	k1gNnSc6	uspořádání
Evropy	Evropa	k1gFnSc2	Evropa
jako	jako	k8xS	jako
federace	federace	k1gFnSc2	federace
demokratických	demokratický	k2eAgInPc2d1	demokratický
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
50	[number]	k4	50
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
navázali	navázat	k5eAaPmAgMnP	navázat
zakladatelé	zakladatel	k1gMnPc1	zakladatel
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dosažení	dosažení	k1gNnSc3	dosažení
politické	politický	k2eAgFnSc2d1	politická
samostatnosti	samostatnost	k1gFnSc2	samostatnost
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
hlavní	hlavní	k2eAgMnSc1d1	hlavní
činitel	činitel	k1gMnSc1	činitel
z	z	k7c2	z
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
pracovali	pracovat	k5eAaImAgMnP	pracovat
na	na	k7c6	na
zviditelnění	zviditelnění	k1gNnSc6	zviditelnění
českého	český	k2eAgInSc2d1	český
národa	národ	k1gInSc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
války	válka	k1gFnSc2	válka
přesvědčoval	přesvědčovat	k5eAaImAgInS	přesvědčovat
státníky	státník	k1gMnPc4	státník
velmocí	velmoc	k1gFnPc2	velmoc
o	o	k7c6	o
potřebnosti	potřebnost	k1gFnSc6	potřebnost
a	a	k8xC	a
užitečnosti	užitečnost	k1gFnSc6	užitečnost
samostatného	samostatný	k2eAgInSc2d1	samostatný
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1914	[number]	k4	1914
se	se	k3xPyFc4	se
Masaryk	Masaryk	k1gMnSc1	Masaryk
vydal	vydat	k5eAaPmAgMnS	vydat
na	na	k7c4	na
kratší	krátký	k2eAgFnPc4d2	kratší
zahraniční	zahraniční	k2eAgFnPc4d1	zahraniční
cesty	cesta	k1gFnPc4	cesta
do	do	k7c2	do
západní	západní	k2eAgFnSc2d1	západní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
kvůli	kvůli	k7c3	kvůli
předání	předání	k1gNnSc3	předání
tajných	tajný	k2eAgInPc2d1	tajný
dokumentů	dokument	k1gInPc2	dokument
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
aby	aby	kYmCp3nS	aby
získal	získat	k5eAaPmAgMnS	získat
doma	doma	k6eAd1	doma
těžko	těžko	k6eAd1	těžko
dostupné	dostupný	k2eAgFnPc4d1	dostupná
informace	informace	k1gFnPc4	informace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
vypravil	vypravit	k5eAaPmAgInS	vypravit
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
a	a	k8xC	a
odtud	odtud	k6eAd1	odtud
do	do	k7c2	do
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Ženevě	Ženeva	k1gFnSc6	Ženeva
jej	on	k3xPp3gNnSc2	on
pak	pak	k6eAd1	pak
zastihlo	zastihnout	k5eAaPmAgNnS	zastihnout
varování	varování	k1gNnSc1	varování
přátel	přítel	k1gMnPc2	přítel
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Rakousku-Uhersku	Rakousku-Uhersek	k1gInSc6	Rakousku-Uhersek
vydán	vydat	k5eAaPmNgInS	vydat
zatykač	zatykač	k1gInSc1	zatykač
a	a	k8xC	a
že	že	k8xS	že
se	se	k3xPyFc4	se
za	za	k7c4	za
žádnou	žádný	k3yNgFnSc4	žádný
cenu	cena	k1gFnSc4	cena
nemá	mít	k5eNaImIp3nS	mít
vracet	vracet	k5eAaImF	vracet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Reformačním	reformační	k2eAgInSc6d1	reformační
sále	sál	k1gInSc6	sál
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Ženevě	Ženeva	k1gFnSc6	Ženeva
pronesl	pronést	k5eAaPmAgMnS	pronést
Masaryk	Masaryk	k1gMnSc1	Masaryk
6	[number]	k4	6
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1915	[number]	k4	1915
svůj	svůj	k3xOyFgInSc4	svůj
slavný	slavný	k2eAgInSc4d1	slavný
projev	projev	k1gInSc4	projev
k	k	k7c3	k
pětistému	pětistý	k2eAgNnSc3d1	pětistý
výročí	výročí	k1gNnSc3	výročí
upálení	upálení	k1gNnSc2	upálení
Mistra	mistr	k1gMnSc2	mistr
Jana	Jan	k1gMnSc2	Jan
Husa	Hus	k1gMnSc2	Hus
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
boj	boj	k1gInSc1	boj
habsburské	habsburský	k2eAgFnPc1d1	habsburská
nadvládě	nadvláda	k1gFnSc3	nadvláda
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Odsuzujeme	odsuzovat	k5eAaImIp1nP	odsuzovat
násilí	násilí	k1gNnSc4	násilí
<g/>
,	,	kIx,	,
nechceme	chtít	k5eNaImIp1nP	chtít
a	a	k8xC	a
nebudeme	být	k5eNaImBp1nP	být
ho	on	k3xPp3gNnSc4	on
užívat	užívat	k5eAaImF	užívat
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
proti	proti	k7c3	proti
násilí	násilí	k1gNnSc3	násilí
budeme	být	k5eAaImBp1nP	být
se	se	k3xPyFc4	se
hájiti	hájit	k5eAaImF	hájit
třeba	třeba	k6eAd1	třeba
železem	železo	k1gNnSc7	železo
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
blízkém	blízký	k2eAgNnSc6d1	blízké
Chamonix	Chamonix	k1gNnSc6	Chamonix
pod	pod	k7c7	pod
horou	hora	k1gFnSc7	hora
Mont	Mont	k1gMnSc1	Mont
Blanc	Blanc	k1gMnSc1	Blanc
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
Milanem	Milan	k1gMnSc7	Milan
Rastislavem	Rastislav	k1gMnSc7	Rastislav
Štefánikem	Štefánik	k1gMnSc7	Štefánik
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
tam	tam	k6eAd1	tam
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
meteorolog	meteorolog	k1gMnSc1	meteorolog
a	a	k8xC	a
k	k	k7c3	k
Masarykovi	Masaryk	k1gMnSc3	Masaryk
se	se	k3xPyFc4	se
sám	sám	k3xTgMnSc1	sám
přihlásil	přihlásit	k5eAaPmAgMnS	přihlásit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
získal	získat	k5eAaPmAgMnS	získat
Masaryk	Masaryk	k1gMnSc1	Masaryk
srbský	srbský	k2eAgInSc4d1	srbský
pas	pas	k1gInSc4	pas
<g/>
.	.	kIx.	.
</s>
<s>
Obdivoval	obdivovat	k5eAaImAgMnS	obdivovat
organizovanost	organizovanost	k1gFnSc4	organizovanost
a	a	k8xC	a
dobré	dobrý	k2eAgNnSc4d1	dobré
utajení	utajení	k1gNnSc4	utajení
sicilské	sicilský	k2eAgFnSc2d1	sicilská
Mafie	mafie	k1gFnSc2	mafie
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
česká	český	k2eAgFnSc1d1	Česká
odbojová	odbojový	k2eAgFnSc1d1	odbojová
organizace	organizace	k1gFnSc1	organizace
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
tajný	tajný	k2eAgInSc1d1	tajný
spolek	spolek	k1gInSc1	spolek
<g/>
,	,	kIx,	,
nazvala	nazvat	k5eAaPmAgFnS	nazvat
Maffie	Maffie	k1gFnSc1	Maffie
<g/>
.	.	kIx.	.
</s>
<s>
Masarykovy	Masarykův	k2eAgMnPc4d1	Masarykův
přátele	přítel	k1gMnPc4	přítel
nazval	nazvat	k5eAaBmAgInS	nazvat
"	"	kIx"	"
<g/>
mafiány	mafián	k1gMnPc4	mafián
<g/>
"	"	kIx"	"
již	již	k6eAd1	již
v	v	k7c6	v
osmdesátých	osmdesátý	k4xOgNnPc6	osmdesátý
letech	léto	k1gNnPc6	léto
devatenáctého	devatenáctý	k4xOgNnSc2	devatenáctý
století	století	k1gNnSc2	století
rozzlobený	rozzlobený	k2eAgMnSc1d1	rozzlobený
Julius	Julius	k1gMnSc1	Julius
Grégr	Grégr	k1gMnSc1	Grégr
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
neustále	neustále	k6eAd1	neustále
a	a	k8xC	a
vytrvale	vytrvale	k6eAd1	vytrvale
zneklidňovali	zneklidňovat	k5eAaImAgMnP	zneklidňovat
tehdejší	tehdejší	k2eAgFnSc4d1	tehdejší
politickou	politický	k2eAgFnSc4d1	politická
elitu	elita	k1gFnSc4	elita
svojí	svojit	k5eAaImIp3nS	svojit
kritikou	kritika	k1gFnSc7	kritika
<g/>
,	,	kIx,	,
připomínkami	připomínka	k1gFnPc7	připomínka
a	a	k8xC	a
návrhy	návrh	k1gInPc7	návrh
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
oni	onen	k3xDgMnPc1	onen
se	se	k3xPyFc4	se
za	za	k7c2	za
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
stali	stát	k5eAaPmAgMnP	stát
hnací	hnací	k2eAgFnSc7d1	hnací
silou	síla	k1gFnSc7	síla
odboje	odboj	k1gInSc2	odboj
a	a	k8xC	a
profesor	profesor	k1gMnSc1	profesor
Masaryk	Masaryk	k1gMnSc1	Masaryk
je	být	k5eAaImIp3nS	být
poháněl	pohánět	k5eAaImAgMnS	pohánět
z	z	k7c2	z
exilu	exil	k1gInSc2	exil
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1915	[number]	k4	1915
emigroval	emigrovat	k5eAaBmAgInS	emigrovat
za	za	k7c7	za
Masarykem	Masaryk	k1gMnSc7	Masaryk
Edvard	Edvard	k1gMnSc1	Edvard
Beneš	Beneš	k1gMnSc1	Beneš
a	a	k8xC	a
oba	dva	k4xCgMnPc1	dva
přesídlili	přesídlit	k5eAaPmAgMnP	přesídlit
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
k	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
připojil	připojit	k5eAaPmAgInS	připojit
Milan	Milan	k1gMnSc1	Milan
Rastislav	Rastislav	k1gMnSc1	Rastislav
Štefánik	Štefánik	k1gMnSc1	Štefánik
(	(	kIx(	(
<g/>
1880	[number]	k4	1880
<g/>
–	–	k?	–
<g/>
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
naturalizovaný	naturalizovaný	k2eAgMnSc1d1	naturalizovaný
a	a	k8xC	a
s	s	k7c7	s
potřebnými	potřebný	k2eAgInPc7d1	potřebný
styky	styk	k1gInPc7	styk
<g/>
.	.	kIx.	.
</s>
<s>
Masaryk	Masaryk	k1gMnSc1	Masaryk
oba	dva	k4xCgMnPc1	dva
dobře	dobře	k6eAd1	dobře
poznal	poznat	k5eAaPmAgMnS	poznat
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
jejich	jejich	k3xOp3gNnPc2	jejich
pražských	pražský	k2eAgNnPc2d1	Pražské
studií	studio	k1gNnPc2	studio
(	(	kIx(	(
<g/>
byl	být	k5eAaImAgMnS	být
jejich	jejich	k3xOp3gMnSc7	jejich
učitelem	učitel	k1gMnSc7	učitel
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
podporoval	podporovat	k5eAaImAgMnS	podporovat
<g/>
.	.	kIx.	.
</s>
<s>
Československé	československý	k2eAgFnPc1d1	Československá
akce	akce	k1gFnPc1	akce
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
svěřil	svěřit	k5eAaPmAgMnS	svěřit
Benešovi	Beneš	k1gMnSc3	Beneš
a	a	k8xC	a
sám	sám	k3xTgMnSc1	sám
ještě	ještě	k9	ještě
v	v	k7c4	v
září	září	k1gNnSc4	září
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
přesídlil	přesídlit	k5eAaPmAgInS	přesídlit
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
londýnské	londýnský	k2eAgFnSc6d1	londýnská
univerzitě	univerzita	k1gFnSc6	univerzita
King	King	k1gMnSc1	King
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
College	College	k1gNnSc7	College
dostal	dostat	k5eAaPmAgMnS	dostat
Masaryk	Masaryk	k1gMnSc1	Masaryk
profesuru	profesura	k1gFnSc4	profesura
"	"	kIx"	"
<g/>
pro	pro	k7c4	pro
slovanské	slovanský	k2eAgFnPc4d1	Slovanská
věci	věc	k1gFnPc4	věc
<g/>
"	"	kIx"	"
a	a	k8xC	a
pozornost	pozornost	k1gFnSc4	pozornost
si	se	k3xPyFc3	se
získával	získávat	k5eAaImAgMnS	získávat
přednáškami	přednáška	k1gFnPc7	přednáška
<g/>
,	,	kIx,	,
veřejnými	veřejný	k2eAgFnPc7d1	veřejná
vystoupeními	vystoupení	k1gNnPc7	vystoupení
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
proti	proti	k7c3	proti
rakouskému	rakouský	k2eAgMnSc3d1	rakouský
ministrovi	ministr	k1gMnSc3	ministr
Aloisi	Alois	k1gMnSc3	Alois
Aehrenthalovi	Aehrenthal	k1gMnSc3	Aehrenthal
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
žurnalistickou	žurnalistický	k2eAgFnSc7d1	žurnalistická
činností	činnost	k1gFnSc7	činnost
a	a	k8xC	a
osobními	osobní	k2eAgInPc7d1	osobní
styky	styk	k1gInPc7	styk
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
svou	svůj	k3xOyFgFnSc7	svůj
literární	literární	k2eAgFnSc7d1	literární
prací	práce	k1gFnSc7	práce
o	o	k7c6	o
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
na	na	k7c6	na
které	který	k3yIgFnSc6	který
stále	stále	k6eAd1	stále
pracoval	pracovat	k5eAaImAgMnS	pracovat
a	a	k8xC	a
jejíž	jejíž	k3xOyRp3gFnSc2	jejíž
části	část	k1gFnSc2	část
zveřejňoval	zveřejňovat	k5eAaImAgMnS	zveřejňovat
–	–	k?	–
Rusko	Rusko	k1gNnSc1	Rusko
a	a	k8xC	a
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
a	a	k8xC	a
Evropa	Evropa	k1gFnSc1	Evropa
a	a	k8xC	a
svět	svět	k1gInSc1	svět
(	(	kIx(	(
<g/>
Russia	Russia	k1gFnSc1	Russia
and	and	k?	and
Europe	Europ	k1gMnSc5	Europ
<g/>
,	,	kIx,	,
The	The	k1gFnPc1	The
Spirit	Spirit	k1gInSc1	Spirit
of	of	k?	of
Russia	Russia	k1gFnSc1	Russia
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Spirit	Spirit	k1gInSc1	Spirit
of	of	k?	of
Russia	Russia	k1gFnSc1	Russia
and	and	k?	and
the	the	k?	the
World	World	k1gInSc1	World
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
předseda	předseda	k1gMnSc1	předseda
Československé	československý	k2eAgFnSc2d1	Československá
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
<g/>
,	,	kIx,	,
založené	založený	k2eAgFnSc2d1	založená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1916	[number]	k4	1916
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
budoval	budovat	k5eAaImAgMnS	budovat
vytrvale	vytrvale	k6eAd1	vytrvale
zahraniční	zahraniční	k2eAgInSc4d1	zahraniční
odboj	odboj	k1gInSc4	odboj
a	a	k8xC	a
zdaleka	zdaleka	k6eAd1	zdaleka
se	se	k3xPyFc4	se
nespokojil	spokojit	k5eNaPmAgMnS	spokojit
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
vytvořením	vytvoření	k1gNnSc7	vytvoření
politické	politický	k2eAgFnSc2d1	politická
organizace	organizace	k1gFnSc2	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Koordinoval	koordinovat	k5eAaBmAgMnS	koordinovat
také	také	k9	také
zpravodajskou	zpravodajský	k2eAgFnSc4d1	zpravodajská
činnost	činnost	k1gFnSc4	činnost
nejen	nejen	k6eAd1	nejen
nitkami	nitka	k1gFnPc7	nitka
z	z	k7c2	z
domova	domov	k1gInSc2	domov
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Dostával	dostávat	k5eAaImAgMnS	dostávat
dokonalé	dokonalý	k2eAgFnPc4d1	dokonalá
informace	informace	k1gFnPc4	informace
o	o	k7c4	o
špionážní	špionážní	k2eAgFnPc4d1	špionážní
<g/>
,	,	kIx,	,
sabotážní	sabotážní	k2eAgFnPc4d1	sabotážní
a	a	k8xC	a
záškodnické	záškodnický	k2eAgFnPc4d1	záškodnická
činnosti	činnost	k1gFnPc4	činnost
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
Rakousko-Uherska	Rakousko-Uhersko	k1gNnSc2	Rakousko-Uhersko
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mu	on	k3xPp3gMnSc3	on
informační	informační	k2eAgFnSc4d1	informační
službu	služba	k1gFnSc4	služba
zprostředkoval	zprostředkovat	k5eAaPmAgMnS	zprostředkovat
Emanuel	Emanuel	k1gMnSc1	Emanuel
Voska	Voska	k1gMnSc1	Voska
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
přehled	přehled	k1gInSc4	přehled
i	i	k9	i
o	o	k7c4	o
situaci	situace	k1gFnSc4	situace
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vybudování	vybudování	k1gNnSc6	vybudování
silných	silný	k2eAgFnPc2d1	silná
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
jednotek	jednotka	k1gFnPc2	jednotka
budoucího	budoucí	k2eAgInSc2d1	budoucí
státu	stát	k1gInSc2	stát
viděl	vidět	k5eAaImAgMnS	vidět
Masaryk	Masaryk	k1gMnSc1	Masaryk
další	další	k2eAgFnSc2d1	další
nutný	nutný	k2eAgInSc4d1	nutný
krok	krok	k1gInSc4	krok
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
uskutečnění	uskutečnění	k1gNnSc3	uskutečnění
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
únorové	únorový	k2eAgFnSc6d1	únorová
revoluci	revoluce	k1gFnSc6	revoluce
v	v	k7c6	v
Petrohradě	Petrohrad	k1gInSc6	Petrohrad
přesídlil	přesídlit	k5eAaPmAgInS	přesídlit
do	do	k7c2	do
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dal	dát	k5eAaPmAgMnS	dát
rozhodující	rozhodující	k2eAgInSc4d1	rozhodující
podnět	podnět	k1gInSc4	podnět
k	k	k7c3	k
sestavení	sestavení	k1gNnSc3	sestavení
dalších	další	k2eAgFnPc2d1	další
samostatných	samostatný	k2eAgFnPc2d1	samostatná
československých	československý	k2eAgFnPc2d1	Československá
legií	legie	k1gFnPc2	legie
z	z	k7c2	z
českých	český	k2eAgMnPc2d1	český
a	a	k8xC	a
slovenských	slovenský	k2eAgMnPc2d1	slovenský
přeběhlíků	přeběhlík	k1gMnPc2	přeběhlík
<g/>
,	,	kIx,	,
zajatců	zajatec	k1gMnPc2	zajatec
i	i	k8xC	i
Čechoslováků	Čechoslovák	k1gMnPc2	Čechoslovák
žijících	žijící	k2eAgMnPc2d1	žijící
na	na	k7c6	na
Rusi	Rus	k1gFnSc6	Rus
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Zborovské	Zborovský	k2eAgFnSc6d1	Zborovská
bitvě	bitva	k1gFnSc6	bitva
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
vybudoval	vybudovat	k5eAaPmAgMnS	vybudovat
do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
více	hodně	k6eAd2	hodně
než	než	k8xS	než
padesátitisícovou	padesátitisícový	k2eAgFnSc4d1	padesátitisícová
armádu	armáda	k1gFnSc4	armáda
a	a	k8xC	a
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
se	se	k3xPyFc4	se
o	o	k7c4	o
stejný	stejný	k2eAgInSc4d1	stejný
počet	počet	k1gInSc4	počet
dobrovolníků	dobrovolník	k1gMnPc2	dobrovolník
v	v	k7c6	v
zajateckých	zajatecký	k2eAgInPc6d1	zajatecký
spolcích	spolek	k1gInPc6	spolek
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
byli	být	k5eAaImAgMnP	být
zálohou	záloha	k1gFnSc7	záloha
pro	pro	k7c4	pro
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
čs	čs	kA	čs
<g/>
.	.	kIx.	.
legií	legie	k1gFnPc2	legie
<g/>
.	.	kIx.	.
</s>
<s>
Tehdejší	tehdejší	k2eAgInSc1d1	tehdejší
Pražský	pražský	k2eAgInSc1d1	pražský
pluk	pluk	k1gInSc1	pluk
legií	legie	k1gFnPc2	legie
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
jejich	jejich	k3xOp3gFnSc1	jejich
pátá	pátá	k1gFnSc1	pátá
divize	divize	k1gFnSc2	divize
<g/>
)	)	kIx)	)
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
T.	T.	kA	T.
G.	G.	kA	G.
Masarykovi	Masaryk	k1gMnSc3	Masaryk
symbolické	symbolický	k2eAgNnSc4d1	symbolické
"	"	kIx"	"
<g/>
vlastnictví	vlastnictví	k1gNnSc4	vlastnictví
<g/>
"	"	kIx"	"
pluku	pluk	k1gInSc2	pluk
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
TGM	TGM	kA	TGM
přijal	přijmout	k5eAaPmAgMnS	přijmout
a	a	k8xC	a
ještě	ještě	k6eAd1	ještě
za	za	k7c2	za
demokratické	demokratický	k2eAgFnSc2d1	demokratická
poúnorové	poúnorový	k2eAgFnSc2d1	poúnorová
ruské	ruský	k2eAgFnSc2d1	ruská
vlády	vláda	k1gFnSc2	vláda
bylo	být	k5eAaImAgNnS	být
realizováno	realizován	k2eAgNnSc1d1	realizováno
a	a	k8xC	a
schváleno	schválen	k2eAgNnSc1d1	schváleno
<g/>
.	.	kIx.	.
</s>
<s>
Vojskem	vojsko	k1gNnSc7	vojsko
byl	být	k5eAaImAgInS	být
přijímán	přijímat	k5eAaImNgMnS	přijímat
jako	jako	k9	jako
jeho	jeho	k3xOp3gMnSc1	jeho
nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
velitel	velitel	k1gMnSc1	velitel
<g/>
–	–	k?	–
<g/>
"	"	kIx"	"
<g/>
tatíček	tatíček	k1gMnSc1	tatíček
<g/>
"	"	kIx"	"
a	a	k8xC	a
ruská	ruský	k2eAgFnSc1d1	ruská
generalita	generalita	k1gFnSc1	generalita
mu	on	k3xPp3gMnSc3	on
prokazovala	prokazovat	k5eAaImAgFnS	prokazovat
úctu	úcta	k1gFnSc4	úcta
<g/>
,	,	kIx,	,
náležející	náležející	k2eAgFnSc4d1	náležející
prezidentovi	prezident	k1gMnSc3	prezident
ještě	ještě	k9	ještě
neexistujícího	existující	k2eNgInSc2d1	neexistující
státu	stát	k1gInSc2	stát
<g/>
:	:	kIx,	:
čestné	čestný	k2eAgFnSc2d1	čestná
stráže	stráž	k1gFnSc2	stráž
<g/>
,	,	kIx,	,
vojenské	vojenský	k2eAgFnSc2d1	vojenská
přehlídky	přehlídka	k1gFnSc2	přehlídka
a	a	k8xC	a
vítání	vítání	k1gNnSc1	vítání
místním	místní	k2eAgNnSc7d1	místní
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Masaryk	Masaryk	k1gMnSc1	Masaryk
byl	být	k5eAaImAgMnS	být
oslovován	oslovovat	k5eAaImNgMnS	oslovovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
gospodin	gospodin	k1gMnSc1	gospodin
preziděnt	preziděnt	k1gMnSc1	preziděnt
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
budování	budování	k1gNnSc3	budování
legií	legie	k1gFnPc2	legie
využíval	využívat	k5eAaImAgMnS	využívat
Masaryk	Masaryk	k1gMnSc1	Masaryk
prostřednictví	prostřednictví	k1gNnSc2	prostřednictví
českých	český	k2eAgInPc2d1	český
časopisů	časopis	k1gInPc2	časopis
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
(	(	kIx(	(
<g/>
Čechoslovák	Čechoslovák	k1gMnSc1	Čechoslovák
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
angažoval	angažovat	k5eAaBmAgMnS	angažovat
se	se	k3xPyFc4	se
i	i	k9	i
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
návštěvami	návštěva	k1gFnPc7	návštěva
zajateckých	zajatecký	k2eAgMnPc2d1	zajatecký
táborů	tábor	k1gMnPc2	tábor
<g/>
,	,	kIx,	,
nemocnic	nemocnice	k1gFnPc2	nemocnice
s	s	k7c7	s
raněnými	raněný	k2eAgMnPc7d1	raněný
vojáky	voják	k1gMnPc7	voják
i	i	k9	i
vzniklých	vzniklý	k2eAgMnPc2d1	vzniklý
osmi	osm	k4xCc2	osm
pluků	pluk	k1gInPc2	pluk
vojska	vojsko	k1gNnSc2	vojsko
pěšího	pěší	k1gMnSc2	pěší
<g/>
,	,	kIx,	,
několika	několik	k4yIc2	několik
jednotek	jednotka	k1gFnPc2	jednotka
dělostřeleckých	dělostřelecký	k2eAgFnPc2d1	dělostřelecká
a	a	k8xC	a
záložního	záložní	k2eAgInSc2d1	záložní
korpusu	korpus	k1gInSc2	korpus
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
příznivec	příznivec	k1gMnSc1	příznivec
letectví	letectví	k1gNnSc2	letectví
zřídil	zřídit	k5eAaPmAgMnS	zřídit
u	u	k7c2	u
legií	legie	k1gFnPc2	legie
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
i	i	k9	i
leteckou	letecký	k2eAgFnSc4d1	letecká
školu	škola	k1gFnSc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Podporoval	podporovat	k5eAaImAgMnS	podporovat
též	též	k9	též
kronikáře	kronikář	k1gMnPc4	kronikář
<g/>
,	,	kIx,	,
fotografy	fotograf	k1gMnPc4	fotograf
a	a	k8xC	a
filmaře	filmař	k1gMnPc4	filmař
pluků	pluk	k1gInPc2	pluk
a	a	k8xC	a
dbal	dbát	k5eAaImAgMnS	dbát
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
u	u	k7c2	u
každého	každý	k3xTgInSc2	každý
pluku	pluk	k1gInSc2	pluk
byla	být	k5eAaImAgFnS	být
i	i	k9	i
dobrá	dobrý	k2eAgFnSc1d1	dobrá
vojenská	vojenský	k2eAgFnSc1d1	vojenská
hudba	hudba	k1gFnSc1	hudba
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnPc7	jejichž
kapelníky	kapelník	k1gMnPc7	kapelník
byli	být	k5eAaImAgMnP	být
i	i	k9	i
známí	známý	k2eAgMnPc1d1	známý
hudební	hudební	k2eAgMnPc1d1	hudební
skladatelé	skladatel	k1gMnPc1	skladatel
(	(	kIx(	(
<g/>
např.	např.	kA	např.
skladatel	skladatel	k1gMnSc1	skladatel
Novotný	Novotný	k1gMnSc1	Novotný
padl	padnout	k5eAaPmAgMnS	padnout
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
proti	proti	k7c3	proti
bolševikům	bolševik	k1gMnPc3	bolševik
na	na	k7c6	na
Mechové	mechový	k2eAgFnSc6d1	mechová
hoře	hora	k1gFnSc6	hora
po	po	k7c6	po
incidentu	incident	k1gInSc6	incident
u	u	k7c2	u
Zlatoústu	Zlatoúst	k1gInSc2	Zlatoúst
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zařizoval	zařizovat	k5eAaImAgMnS	zařizovat
jim	on	k3xPp3gMnPc3	on
zimní	zimní	k2eAgNnSc1d1	zimní
ubytování	ubytování	k1gNnSc1	ubytování
<g/>
,	,	kIx,	,
promýšlel	promýšlet	k5eAaImAgMnS	promýšlet
jejich	jejich	k3xOp3gFnSc4	jejich
taktiku	taktika	k1gFnSc4	taktika
<g/>
,	,	kIx,	,
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
velitele	velitel	k1gMnSc2	velitel
<g/>
,	,	kIx,	,
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
se	se	k3xPyFc4	se
o	o	k7c4	o
uznání	uznání	k1gNnSc4	uznání
legií	legie	k1gFnPc2	legie
jako	jako	k8xS	jako
součásti	součást	k1gFnSc2	součást
francouzské	francouzský	k2eAgFnSc2d1	francouzská
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
propočítával	propočítávat	k5eAaImAgMnS	propočítávat
a	a	k8xC	a
navrhoval	navrhovat	k5eAaImAgMnS	navrhovat
cestu	cesta	k1gFnSc4	cesta
legií	legie	k1gFnPc2	legie
na	na	k7c4	na
francouzská	francouzský	k2eAgNnPc4d1	francouzské
bojiště	bojiště	k1gNnPc4	bojiště
včetně	včetně	k7c2	včetně
potřeby	potřeba	k1gFnSc2	potřeba
256	[number]	k4	256
ešalonů	ešalon	k1gInPc2	ešalon
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
"	"	kIx"	"
<g/>
kvartýrmajstrem	kvartýrmajstr	k1gInSc7	kvartýrmajstr
<g/>
"	"	kIx"	"
legií	legie	k1gFnPc2	legie
na	na	k7c6	na
transsibiřské	transsibiřský	k2eAgFnSc6d1	Transsibiřská
magistrále	magistrála	k1gFnSc6	magistrála
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
vstoupily	vstoupit	k5eAaPmAgFnP	vstoupit
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1917	[number]	k4	1917
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
západních	západní	k2eAgMnPc2d1	západní
spojenců	spojenec	k1gMnPc2	spojenec
i	i	k8xC	i
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tzv.	tzv.	kA	tzv.
Velké	velký	k2eAgFnSc6d1	velká
říjnové	říjnový	k2eAgFnSc6d1	říjnová
revoluci	revoluce	k1gFnSc6	revoluce
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1917	[number]	k4	1917
se	se	k3xPyFc4	se
zcela	zcela	k6eAd1	zcela
změnila	změnit	k5eAaPmAgFnS	změnit
celková	celkový	k2eAgFnSc1d1	celková
situace	situace	k1gFnSc1	situace
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
když	když	k8xS	když
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Brest-Litevsku	Brest-Litevsko	k1gNnSc6	Brest-Litevsko
uzavřen	uzavřít	k5eAaPmNgInS	uzavřít
separátní	separátní	k2eAgInSc1d1	separátní
mír	mír	k1gInSc1	mír
Ruska	Rusko	k1gNnSc2	Rusko
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
podmínky	podmínka	k1gFnPc1	podmínka
pro	pro	k7c4	pro
působení	působení	k1gNnSc4	působení
československých	československý	k2eAgFnPc2d1	Československá
legií	legie	k1gFnPc2	legie
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
území	území	k1gNnSc6	území
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
se	se	k3xPyFc4	se
Masaryk	Masaryk	k1gMnSc1	Masaryk
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
k	k	k7c3	k
daleké	daleký	k2eAgFnSc3d1	daleká
cestě	cesta	k1gFnSc3	cesta
přes	přes	k7c4	přes
Sibiř	Sibiř	k1gFnSc4	Sibiř
a	a	k8xC	a
Japonsko	Japonsko	k1gNnSc4	Japonsko
do	do	k7c2	do
USA	USA	kA	USA
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
tam	tam	k6eAd1	tam
působil	působit	k5eAaImAgMnS	působit
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
Woodrowa	Woodrowus	k1gMnSc4	Woodrowus
Wilsona	Wilson	k1gMnSc4	Wilson
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
tehdy	tehdy	k6eAd1	tehdy
již	již	k6eAd1	již
převzal	převzít	k5eAaPmAgMnS	převzít
evropské	evropský	k2eAgInPc4d1	evropský
názory	názor	k1gInPc4	názor
o	o	k7c6	o
možné	možný	k2eAgFnSc6d1	možná
transformaci	transformace	k1gFnSc6	transformace
rakouské	rakouský	k2eAgFnSc2d1	rakouská
monarchie	monarchie	k1gFnSc2	monarchie
do	do	k7c2	do
národnostně	národnostně	k6eAd1	národnostně
svobodného	svobodný	k2eAgInSc2d1	svobodný
svazku	svazek	k1gInSc2	svazek
autonomních	autonomní	k2eAgInPc2d1	autonomní
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
ovšem	ovšem	k9	ovšem
ještě	ještě	k6eAd1	ještě
neznamenalo	znamenat	k5eNaImAgNnS	znamenat
podporu	podpora	k1gFnSc4	podpora
vzniku	vznik	k1gInSc2	vznik
zcela	zcela	k6eAd1	zcela
samostatných	samostatný	k2eAgInPc2d1	samostatný
států	stát	k1gInPc2	stát
a	a	k8xC	a
zánik	zánik	k1gInSc1	zánik
mnohonárodnostního	mnohonárodnostní	k2eAgInSc2d1	mnohonárodnostní
Rakouska-Uherska	Rakouska-Uhersk	k1gInSc2	Rakouska-Uhersk
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
strastiplné	strastiplný	k2eAgFnSc2d1	strastiplná
cesty	cesta	k1gFnSc2	cesta
lazaretním	lazaretní	k2eAgInSc7d1	lazaretní
vlakem	vlak	k1gInSc7	vlak
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yIgFnSc4	který
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgMnS	vydat
v	v	k7c4	v
den	den	k1gInSc4	den
svých	svůj	k3xOyFgMnPc2	svůj
68	[number]	k4	68
<g/>
.	.	kIx.	.
narozenin	narozeniny	k1gFnPc2	narozeniny
7	[number]	k4	7
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
nejen	nejen	k6eAd1	nejen
zajišťoval	zajišťovat	k5eAaImAgInS	zajišťovat
místa	místo	k1gNnPc4	místo
budoucího	budoucí	k2eAgInSc2d1	budoucí
pobytu	pobyt	k1gInSc2	pobyt
legií	legie	k1gFnPc2	legie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tvořil	tvořit	k5eAaImAgInS	tvořit
též	též	k9	též
svůj	svůj	k3xOyFgInSc4	svůj
známý	známý	k2eAgInSc4d1	známý
spis	spis	k1gInSc4	spis
"	"	kIx"	"
<g/>
Nová	nový	k2eAgFnSc1d1	nová
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
z	z	k7c2	z
Tokia	Tokio	k1gNnSc2	Tokio
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
dorazil	dorazit	k5eAaPmAgMnS	dorazit
z	z	k7c2	z
Vladivostoku	Vladivostok	k1gInSc2	Vladivostok
přes	přes	k7c4	přes
tehdy	tehdy	k6eAd1	tehdy
Japonskem	Japonsko	k1gNnSc7	Japonsko
okupovanou	okupovaný	k2eAgFnSc4d1	okupovaná
Koreu	Korea	k1gFnSc4	Korea
a	a	k8xC	a
poté	poté	k6eAd1	poté
na	na	k7c6	na
lodi	loď	k1gFnSc6	loď
<g/>
,	,	kIx,	,
poslal	poslat	k5eAaPmAgInS	poslat
prezidentu	prezident	k1gMnSc3	prezident
Wilsonovi	Wilson	k1gMnSc3	Wilson
memorandum	memorandum	k1gNnSc4	memorandum
o	o	k7c6	o
stavu	stav	k1gInSc6	stav
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
zdůraznil	zdůraznit	k5eAaPmAgInS	zdůraznit
význam	význam	k1gInSc1	význam
a	a	k8xC	a
úspěchy	úspěch	k1gInPc1	úspěch
Československých	československý	k2eAgFnPc2d1	Československá
legií	legie	k1gFnPc2	legie
<g/>
.	.	kIx.	.
</s>
<s>
Dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
tak	tak	k9	tak
předpokládané	předpokládaný	k2eAgFnSc3d1	předpokládaná
pozornosti	pozornost	k1gFnSc3	pozornost
a	a	k8xC	a
účinku	účinek	k1gInSc3	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Doporučoval	doporučovat	k5eAaImAgInS	doporučovat
tehdy	tehdy	k6eAd1	tehdy
západním	západní	k2eAgMnPc3d1	západní
spojencům	spojenec	k1gMnPc3	spojenec
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
uznali	uznat	k5eAaPmAgMnP	uznat
ruskou	ruský	k2eAgFnSc4d1	ruská
bolševickou	bolševický	k2eAgFnSc4d1	bolševická
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
29	[number]	k4	29
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1918	[number]	k4	1918
Masaryk	Masaryk	k1gMnSc1	Masaryk
dorazil	dorazit	k5eAaPmAgMnS	dorazit
do	do	k7c2	do
kanadského	kanadský	k2eAgInSc2d1	kanadský
Vancouveru	Vancouver	k1gInSc2	Vancouver
a	a	k8xC	a
5	[number]	k4	5
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
byl	být	k5eAaImAgMnS	být
ke	k	k7c3	k
svému	svůj	k3xOyFgNnSc3	svůj
překvapení	překvapení	k1gNnSc3	překvapení
přijat	přijmout	k5eAaPmNgInS	přijmout
jásajícími	jásající	k2eAgInPc7d1	jásající
davy	dav	k1gInPc7	dav
u	u	k7c2	u
Michiganského	michiganský	k2eAgNnSc2d1	Michiganské
jezera	jezero	k1gNnSc2	jezero
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
a	a	k8xC	a
bezmála	bezmála	k6eAd1	bezmála
dvousettisícovým	dvousettisícový	k2eAgInSc7d1	dvousettisícový
průvodem	průvod	k1gInSc7	průvod
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
počest	počest	k1gFnSc6	počest
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
žádán	žádat	k5eAaImNgInS	žádat
o	o	k7c4	o
projevy	projev	k1gInPc4	projev
a	a	k8xC	a
zasypáván	zasypávat	k5eAaImNgMnS	zasypávat
květinami	květina	k1gFnPc7	květina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
zorganizoval	zorganizovat	k5eAaPmAgMnS	zorganizovat
velkou	velký	k2eAgFnSc4d1	velká
přesvědčovací	přesvědčovací	k2eAgFnSc4d1	přesvědčovací
kampaň	kampaň	k1gFnSc4	kampaň
mezi	mezi	k7c7	mezi
americkými	americký	k2eAgMnPc7d1	americký
Čechy	Čech	k1gMnPc7	Čech
a	a	k8xC	a
Slováky	Slovák	k1gMnPc7	Slovák
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
samostatného	samostatný	k2eAgInSc2d1	samostatný
československého	československý	k2eAgInSc2d1	československý
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
se	se	k3xPyFc4	se
připojili	připojit	k5eAaPmAgMnP	připojit
i	i	k9	i
představitelé	představitel	k1gMnPc1	představitel
Rusínů	Rusín	k1gMnPc2	Rusín
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sepsání	sepsání	k1gNnSc6	sepsání
Clevelandské	Clevelandský	k2eAgFnSc2d1	Clevelandská
dohody	dohoda	k1gFnSc2	dohoda
pak	pak	k6eAd1	pak
následně	následně	k6eAd1	následně
31	[number]	k4	31
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1918	[number]	k4	1918
podepsali	podepsat	k5eAaPmAgMnP	podepsat
zástupci	zástupce	k1gMnPc1	zástupce
krajanů	krajan	k1gMnPc2	krajan
v	v	k7c4	v
Pittsburghu	Pittsburgha	k1gFnSc4	Pittsburgha
Pittsburskou	pittsburský	k2eAgFnSc4d1	Pittsburská
dohodu	dohoda	k1gFnSc4	dohoda
o	o	k7c6	o
společném	společný	k2eAgInSc6d1	společný
státu	stát	k1gInSc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Masaryk	Masaryk	k1gMnSc1	Masaryk
jednal	jednat	k5eAaImAgMnS	jednat
s	s	k7c7	s
diplomaty	diplomat	k1gMnPc7	diplomat
řady	řada	k1gFnSc2	řada
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
organizoval	organizovat	k5eAaBmAgInS	organizovat
zpravodajství	zpravodajství	k1gNnSc4	zpravodajství
<g/>
,	,	kIx,	,
psal	psát	k5eAaImAgMnS	psát
do	do	k7c2	do
novin	novina	k1gFnPc2	novina
a	a	k8xC	a
přednášel	přednášet	k5eAaImAgMnS	přednášet
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
přesvědčení	přesvědčení	k1gNnSc3	přesvědčení
americké	americký	k2eAgFnSc2d1	americká
veřejnosti	veřejnost	k1gFnSc2	veřejnost
a	a	k8xC	a
státníků	státník	k1gMnPc2	státník
o	o	k7c6	o
československé	československý	k2eAgFnSc6d1	Československá
věci	věc	k1gFnSc6	věc
pomohly	pomoct	k5eAaPmAgInP	pomoct
i	i	k9	i
Masarykovy	Masarykův	k2eAgInPc1d1	Masarykův
dobré	dobrý	k2eAgInPc1d1	dobrý
styky	styk	k1gInPc1	styk
s	s	k7c7	s
představiteli	představitel	k1gMnPc7	představitel
krajanů	krajan	k1gMnPc2	krajan
a	a	k8xC	a
vlivnými	vlivný	k2eAgFnPc7d1	vlivná
osobnostmi	osobnost	k1gFnPc7	osobnost
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
předcházejících	předcházející	k2eAgFnPc2d1	předcházející
návštěv	návštěva	k1gFnPc2	návštěva
u	u	k7c2	u
rodiny	rodina	k1gFnSc2	rodina
jeho	jeho	k3xOp3gFnSc2	jeho
manželky	manželka	k1gFnSc2	manželka
Charlotty	Charlotta	k1gFnSc2	Charlotta
a	a	k8xC	a
z	z	k7c2	z
tehdejších	tehdejší	k2eAgNnPc2d1	tehdejší
veřejných	veřejný	k2eAgNnPc2d1	veřejné
vystoupení	vystoupení	k1gNnPc2	vystoupení
<g/>
.	.	kIx.	.
</s>
<s>
Prezidentovi	prezidentův	k2eAgMnPc1d1	prezidentův
Wilsonovi	Wilsonův	k2eAgMnPc1d1	Wilsonův
jeho	jeho	k3xOp3gMnPc1	jeho
poradci	poradce	k1gMnPc1	poradce
doporučovali	doporučovat	k5eAaImAgMnP	doporučovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
k	k	k7c3	k
poradám	porada	k1gFnPc3	porada
o	o	k7c6	o
ruské	ruský	k2eAgFnSc6d1	ruská
otázce	otázka	k1gFnSc6	otázka
přizval	přizvat	k5eAaPmAgMnS	přizvat
Masaryka	Masaryk	k1gMnSc2	Masaryk
jako	jako	k8xC	jako
znalce	znalec	k1gMnSc2	znalec
ruské	ruský	k2eAgFnSc2d1	ruská
problematiky	problematika	k1gFnSc2	problematika
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Bílého	bílý	k2eAgInSc2d1	bílý
domu	dům	k1gInSc2	dům
dorazil	dorazit	k5eAaPmAgMnS	dorazit
Masaryk	Masaryk	k1gMnSc1	Masaryk
19	[number]	k4	19
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
.	.	kIx.	.
</s>
<s>
Přesvědčoval	přesvědčovat	k5eAaImAgMnS	přesvědčovat
Wilsona	Wilson	k1gMnSc2	Wilson
<g/>
,	,	kIx,	,
že	že	k8xS	že
válku	válka	k1gFnSc4	válka
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
vyhrát	vyhrát	k5eAaPmF	vyhrát
jedině	jedině	k6eAd1	jedině
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
<g/>
-li	i	k?	-li
rozbito	rozbit	k2eAgNnSc1d1	rozbito
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1	Rakousko-Uhersko
<g/>
,	,	kIx,	,
největší	veliký	k2eAgMnSc1d3	veliký
spojenec	spojenec	k1gMnSc1	spojenec
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Žádal	žádat	k5eAaImAgInS	žádat
o	o	k7c4	o
americkou	americký	k2eAgFnSc4d1	americká
pomoc	pomoc	k1gFnSc4	pomoc
s	s	k7c7	s
dopravou	doprava	k1gFnSc7	doprava
Československých	československý	k2eAgFnPc2d1	Československá
legií	legie	k1gFnPc2	legie
z	z	k7c2	z
Ruska	Rusko	k1gNnSc2	Rusko
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
se	se	k3xPyFc4	se
ale	ale	k9	ale
proti	proti	k7c3	proti
zamýšlené	zamýšlený	k2eAgFnSc3d1	zamýšlená
západní	západní	k2eAgFnSc3d1	západní
intervenci	intervence	k1gFnSc3	intervence
v	v	k7c6	v
občanské	občanský	k2eAgFnSc6d1	občanská
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
18	[number]	k4	18
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1918	[number]	k4	1918
Wilson	Wilson	k1gInSc1	Wilson
veřejně	veřejně	k6eAd1	veřejně
změnil	změnit	k5eAaPmAgInS	změnit
svoje	svůj	k3xOyFgNnSc4	svůj
dosavadní	dosavadní	k2eAgNnSc4d1	dosavadní
stanovisko	stanovisko	k1gNnSc4	stanovisko
a	a	k8xC	a
přispěl	přispět	k5eAaPmAgMnS	přispět
tak	tak	k6eAd1	tak
rozhodujícím	rozhodující	k2eAgInSc7d1	rozhodující
způsobem	způsob	k1gInSc7	způsob
k	k	k7c3	k
rozpadu	rozpad	k1gInSc3	rozpad
Rakouska-Uherska	Rakouska-Uhersk	k1gInSc2	Rakouska-Uhersk
<g/>
.	.	kIx.	.
</s>
<s>
Podpořil	podpořit	k5eAaPmAgMnS	podpořit
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
vznik	vznik	k1gInSc1	vznik
samostatného	samostatný	k2eAgNnSc2d1	samostatné
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
Washingtonská	washingtonský	k2eAgFnSc1d1	Washingtonská
deklarace	deklarace	k1gFnSc1	deklarace
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stala	stát	k5eAaPmAgFnS	stát
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
zakládající	zakládající	k2eAgFnSc7d1	zakládající
listinou	listina	k1gFnSc7	listina
nového	nový	k2eAgInSc2d1	nový
demokratického	demokratický	k2eAgInSc2d1	demokratický
československého	československý	k2eAgInSc2d1	československý
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
Republiky	republika	k1gFnSc2	republika
československé	československý	k2eAgFnSc2d1	Československá
<g/>
,	,	kIx,	,
ke	k	k7c3	k
dni	den	k1gInSc3	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
po	po	k7c6	po
Evropě	Evropa	k1gFnSc6	Evropa
</s>
<s>
Československo	Československo	k1gNnSc1	Československo
bylo	být	k5eAaImAgNnS	být
mezinárodně	mezinárodně	k6eAd1	mezinárodně
uznáno	uznat	k5eAaPmNgNnS	uznat
za	za	k7c4	za
samostatný	samostatný	k2eAgInSc4d1	samostatný
stát	stát	k1gInSc4	stát
a	a	k8xC	a
Masaryk	Masaryk	k1gMnSc1	Masaryk
byl	být	k5eAaImAgMnS	být
14	[number]	k4	14
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc6	listopad
1918	[number]	k4	1918
zvolen	zvolit	k5eAaPmNgMnS	zvolit
prvním	první	k4xOgMnSc7	první
prezidentem	prezident	k1gMnSc7	prezident
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
dcerou	dcera	k1gFnSc7	dcera
Olgou	Olga	k1gFnSc7	Olga
se	se	k3xPyFc4	se
vrátili	vrátit	k5eAaPmAgMnP	vrátit
lodí	loď	k1gFnSc7	loď
Carmania	Carmanium	k1gNnPc1	Carmanium
<g/>
,	,	kIx,	,
po	po	k7c6	po
přistání	přistání	k1gNnSc6	přistání
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
přes	přes	k7c4	přes
Francii	Francie	k1gFnSc4	Francie
<g/>
,	,	kIx,	,
Itálii	Itálie	k1gFnSc4	Itálie
a	a	k8xC	a
Rakousko	Rakousko	k1gNnSc4	Rakousko
dále	daleko	k6eAd2	daleko
vlakem	vlak	k1gInSc7	vlak
<g/>
.	.	kIx.	.
</s>
<s>
Přehlídku	přehlídka	k1gFnSc4	přehlídka
legií	legie	k1gFnPc2	legie
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
francouzským	francouzský	k2eAgMnSc7d1	francouzský
prezidentem	prezident	k1gMnSc7	prezident
Raymondem	Raymond	k1gMnSc7	Raymond
Poincaré	Poincarý	k2eAgInPc4d1	Poincarý
<g/>
,	,	kIx,	,
provedl	provést	k5eAaPmAgInS	provést
v	v	k7c4	v
Darney	Darne	k2eAgFnPc4d1	Darne
9	[number]	k4	9
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
</s>
<s>
Citát	citát	k1gInSc1	citát
z	z	k7c2	z
jeho	jeho	k3xOp3gInSc2	jeho
tamějšího	tamější	k2eAgInSc2d1	tamější
projevu	projev	k1gInSc2	projev
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Bratři	bratr	k1gMnPc1	bratr
<g/>
,	,	kIx,	,
vy	vy	k3xPp2nPc1	vy
jste	být	k5eAaImIp2nP	být
to	ten	k3xDgNnSc4	ten
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
<g/>
,	,	kIx,	,
dejme	dát	k5eAaPmRp1nP	dát
teď	teď	k6eAd1	teď
jen	jen	k9	jen
pozor	pozor	k1gInSc4	pozor
<g/>
,	,	kIx,	,
když	když	k8xS	když
jsme	být	k5eAaImIp1nP	být
na	na	k7c6	na
kopečku	kopeček	k1gInSc6	kopeček
<g/>
,	,	kIx,	,
abychom	aby	kYmCp1nP	aby
tam	tam	k6eAd1	tam
zůstali	zůstat	k5eAaPmAgMnP	zůstat
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
ho	on	k3xPp3gMnSc4	on
se	se	k3xPyFc4	se
všemi	všecek	k3xTgFnPc7	všecek
poctami	pocta	k1gFnPc7	pocta
přijal	přijmout	k5eAaPmAgMnS	přijmout
král	král	k1gMnSc1	král
Viktor	Viktor	k1gMnSc1	Viktor
Emanuel	Emanuel	k1gMnSc1	Emanuel
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
po	po	k7c6	po
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
<g/>
,	,	kIx,	,
první	první	k4xOgInSc4	první
projev	projev	k1gInSc4	projev
ve	v	k7c6	v
vlasti	vlast	k1gFnSc6	vlast
a	a	k8xC	a
prezidentský	prezidentský	k2eAgInSc4d1	prezidentský
slib	slib	k1gInSc4	slib
Čerstvé	čerstvý	k2eAgFnSc2d1	čerstvá
československé	československý	k2eAgFnSc2d1	Československá
hranice	hranice	k1gFnSc2	hranice
překročil	překročit	k5eAaPmAgMnS	překročit
Masaryk	Masaryk	k1gMnSc1	Masaryk
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
doprovodem	doprovod	k1gInSc7	doprovod
(	(	kIx(	(
<g/>
jen	jen	k9	jen
jeho	jeho	k3xOp3gInSc4	jeho
vlak	vlak	k1gInSc4	vlak
měl	mít	k5eAaImAgMnS	mít
19	[number]	k4	19
vagonů	vagon	k1gInPc2	vagon
a	a	k8xC	a
dvě	dva	k4xCgFnPc4	dva
lokomotivy	lokomotiva	k1gFnPc4	lokomotiva
<g/>
,	,	kIx,	,
před	před	k7c7	před
ním	on	k3xPp3gMnSc7	on
jel	jet	k5eAaImAgMnS	jet
ještě	ještě	k6eAd1	ještě
vlak	vlak	k1gInSc4	vlak
s	s	k7c7	s
legionáři	legionář	k1gMnPc7	legionář
<g/>
)	)	kIx)	)
v	v	k7c4	v
pátek	pátek	k1gInSc4	pátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
v	v	k7c6	v
Horním	horní	k2eAgNnSc6d1	horní
Dvořišti	dvořiště	k1gNnSc6	dvořiště
na	na	k7c6	na
rakousko-české	rakousko-český	k2eAgFnSc6d1	rakousko-česká
hranici	hranice	k1gFnSc6	hranice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ho	on	k3xPp3gMnSc4	on
přivítalo	přivítat	k5eAaPmAgNnS	přivítat
nově	nově	k6eAd1	nově
zvolené	zvolený	k2eAgNnSc1d1	zvolené
Národní	národní	k2eAgNnSc1d1	národní
shromáždění	shromáždění	k1gNnSc1	shromáždění
Republiky	republika	k1gFnSc2	republika
československé	československý	k2eAgFnSc2d1	Československá
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
veřejné	veřejný	k2eAgNnSc1d1	veřejné
vystoupení	vystoupení	k1gNnSc1	vystoupení
měl	mít	k5eAaImAgInS	mít
v	v	k7c6	v
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
na	na	k7c6	na
zcela	zcela	k6eAd1	zcela
zaplněném	zaplněný	k2eAgNnSc6d1	zaplněné
hlavním	hlavní	k2eAgNnSc6d1	hlavní
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
pojmenováno	pojmenovat	k5eAaPmNgNnS	pojmenovat
<g/>
.	.	kIx.	.
</s>
<s>
Triumfální	triumfální	k2eAgInSc1d1	triumfální
prezidentův	prezidentův	k2eAgInSc1d1	prezidentův
návrat	návrat	k1gInSc1	návrat
do	do	k7c2	do
vlasti	vlast	k1gFnSc2	vlast
natáčel	natáčet	k5eAaImAgMnS	natáčet
pozdější	pozdní	k2eAgMnSc1d2	pozdější
filmový	filmový	k2eAgMnSc1d1	filmový
režisér	režisér	k1gMnSc1	režisér
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Innemann	Innemann	k1gMnSc1	Innemann
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc4d1	následující
den	den	k1gInSc4	den
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
Masaryk	Masaryk	k1gMnSc1	Masaryk
krátce	krátce	k6eAd1	krátce
veřejně	veřejně	k6eAd1	veřejně
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
ve	v	k7c6	v
Veselí-Mezimostí	Veselí-Mezimost	k1gFnSc7	Veselí-Mezimost
<g/>
,	,	kIx,	,
v	v	k7c6	v
Táboře	Tábor	k1gInSc6	Tábor
<g/>
,	,	kIx,	,
Benešově	Benešov	k1gInSc6	Benešov
a	a	k8xC	a
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
davy	dav	k1gInPc1	dav
občanů	občan	k1gMnPc2	občan
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
slavnostní	slavnostní	k2eAgFnSc2d1	slavnostní
cesty	cesta	k1gFnSc2	cesta
Prahou	Praha	k1gFnSc7	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Začala	začít	k5eAaPmAgFnS	začít
projevy	projev	k1gInPc4	projev
po	po	k7c4	po
vystoupení	vystoupení	k1gNnSc4	vystoupení
z	z	k7c2	z
vlaku	vlak	k1gInSc2	vlak
na	na	k7c6	na
Wilsonově	Wilsonův	k2eAgNnSc6d1	Wilsonovo
nádraží	nádraží	k1gNnSc6	nádraží
(	(	kIx(	(
<g/>
hlavní	hlavní	k2eAgInSc1d1	hlavní
uvítací	uvítací	k2eAgInSc1d1	uvítací
projev	projev	k1gInSc1	projev
přednesl	přednést	k5eAaPmAgMnS	přednést
Alois	Alois	k1gMnSc1	Alois
Jirásek	Jirásek	k1gMnSc1	Jirásek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kantátou	kantáta	k1gFnSc7	kantáta
Vítězslava	Vítězslav	k1gMnSc2	Vítězslav
Nováka	Novák	k1gMnSc2	Novák
"	"	kIx"	"
<g/>
Sláva	Sláva	k1gFnSc1	Sláva
Tobě	ty	k3xPp2nSc3	ty
<g/>
,	,	kIx,	,
Masaryku	Masaryk	k1gMnSc5	Masaryk
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
triumfální	triumfální	k2eAgFnSc7d1	triumfální
jízdou	jízda	k1gFnSc7	jízda
přes	přes	k7c4	přes
Václavské	václavský	k2eAgNnSc4d1	Václavské
náměstí	náměstí	k1gNnSc4	náměstí
<g/>
,	,	kIx,	,
slavnostními	slavnostní	k2eAgFnPc7d1	slavnostní
fanfárami	fanfára	k1gFnPc7	fanfára
Smetanovy	Smetanův	k2eAgFnSc2d1	Smetanova
Libuše	Libuše	k1gFnSc2	Libuše
z	z	k7c2	z
lodžie	lodžie	k1gFnSc2	lodžie
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
zcela	zcela	k6eAd1	zcela
zaplněné	zaplněný	k2eAgNnSc4d1	zaplněné
Staroměstské	staroměstský	k2eAgNnSc4d1	Staroměstské
náměstí	náměstí	k1gNnSc4	náměstí
a	a	k8xC	a
odtud	odtud	k6eAd1	odtud
po	po	k7c6	po
slavnostních	slavnostní	k2eAgInPc6d1	slavnostní
projevech	projev	k1gInPc6	projev
krásně	krásně	k6eAd1	krásně
vyzdobenou	vyzdobený	k2eAgFnSc4d1	vyzdobená
Mikulášskou	mikulášský	k2eAgFnSc4d1	Mikulášská
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Pařížskou	pařížský	k2eAgFnSc7d1	Pařížská
<g/>
)	)	kIx)	)
třídou	třída	k1gFnSc7	třída
na	na	k7c4	na
Malou	malý	k2eAgFnSc4d1	malá
Stranu	strana	k1gFnSc4	strana
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Sněmovní	sněmovní	k2eAgFnSc6d1	sněmovní
ulici	ulice	k1gFnSc6	ulice
pak	pak	k6eAd1	pak
složil	složit	k5eAaPmAgMnS	složit
první	první	k4xOgInSc4	první
oficiální	oficiální	k2eAgInSc4d1	oficiální
slib	slib	k1gInSc4	slib
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
Národnímu	národní	k2eAgNnSc3d1	národní
shromáždění	shromáždění	k1gNnSc3	shromáždění
<g/>
.	.	kIx.	.
</s>
<s>
Odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
Kramářův	kramářův	k2eAgInSc4d1	kramářův
návrh	návrh	k1gInSc4	návrh
ústavy	ústava	k1gFnSc2	ústava
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterého	který	k3yQgMnSc4	který
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
prezident	prezident	k1gMnSc1	prezident
československa	československo	k1gNnSc2	československo
spíše	spíše	k9	spíše
ceremoniální	ceremoniální	k2eAgFnSc1d1	ceremoniální
figura	figura	k1gFnSc1	figura
bez	bez	k7c2	bez
velkých	velký	k2eAgFnPc2d1	velká
pravomocí	pravomoc	k1gFnPc2	pravomoc
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
triumfální	triumfální	k2eAgFnSc3d1	triumfální
cestě	cesta	k1gFnSc3	cesta
rozjásanou	rozjásaný	k2eAgFnSc7d1	rozjásaná
Prahou	Praha	k1gFnSc7	Praha
použil	použít	k5eAaPmAgInS	použít
s	s	k7c7	s
předsedou	předseda	k1gMnSc7	předseda
sněmovny	sněmovna	k1gFnSc2	sněmovna
Františkem	František	k1gMnSc7	František
Tomáškem	Tomášek	k1gMnSc7	Tomášek
zcela	zcela	k6eAd1	zcela
odkrytý	odkrytý	k2eAgInSc4d1	odkrytý
automobil	automobil	k1gInSc4	automobil
Laurin	Laurin	k1gInSc1	Laurin
&	&	k?	&
Klement	Klement	k1gMnSc1	Klement
200	[number]	k4	200
řady	řada	k1gFnSc2	řada
S	s	k7c7	s
(	(	kIx(	(
<g/>
místo	místo	k7c2	místo
císařského	císařský	k2eAgInSc2d1	císařský
kočáru	kočár	k1gInSc2	kočár
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgInS	být
nabízen	nabízen	k2eAgInSc1d1	nabízen
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
jakožto	jakožto	k8xS	jakožto
dopravní	dopravní	k2eAgInSc1d1	dopravní
prostředek	prostředek	k1gInSc1	prostředek
nový	nový	k2eAgInSc1d1	nový
a	a	k8xC	a
demokratický	demokratický	k2eAgInSc1d1	demokratický
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
kočáru	kočár	k1gInSc2	kočár
rakouského	rakouský	k2eAgMnSc2d1	rakouský
císaře	císař	k1gMnSc2	císař
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
pro	pro	k7c4	pro
slavnostní	slavnostní	k2eAgFnSc4d1	slavnostní
jízdu	jízda	k1gFnSc4	jízda
opatřil	opatřit	k5eAaPmAgMnS	opatřit
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
vyslanec	vyslanec	k1gMnSc1	vyslanec
Tusar	Tusar	k1gMnSc1	Tusar
<g/>
,	,	kIx,	,
zasedli	zasednout	k5eAaPmAgMnP	zasednout
Masarykův	Masarykův	k2eAgMnSc1d1	Masarykův
syn	syn	k1gMnSc1	syn
Jan	Jan	k1gMnSc1	Jan
a	a	k8xC	a
dcera	dcera	k1gFnSc1	dcera
Olga	Olga	k1gFnSc1	Olga
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
prezidenta	prezident	k1gMnSc4	prezident
doprovázela	doprovázet	k5eAaImAgFnS	doprovázet
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
z	z	k7c2	z
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
pak	pak	k6eAd1	pak
mohl	moct	k5eAaImAgMnS	moct
vyzvednout	vyzvednout	k5eAaPmF	vyzvednout
manželku	manželka	k1gFnSc4	manželka
Charlottu	Charlotta	k1gFnSc4	Charlotta
Masarykovou	Masaryková	k1gFnSc7	Masaryková
ve	v	k7c6	v
veleslavínském	veleslavínský	k2eAgNnSc6d1	veleslavínské
sanatoriu	sanatorium	k1gNnSc6	sanatorium
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
22	[number]	k4	22
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1918	[number]	k4	1918
přednesl	přednést	k5eAaPmAgMnS	přednést
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
své	svůj	k3xOyFgFnSc2	svůj
"	"	kIx"	"
<g/>
První	první	k4xOgNnPc4	první
poselství	poselství	k1gNnPc4	poselství
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Založení	založení	k1gNnSc1	založení
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
a	a	k8xC	a
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
akademie	akademie	k1gFnSc2	akademie
práce	práce	k1gFnSc2	práce
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
podepsal	podepsat	k5eAaPmAgMnS	podepsat
jako	jako	k9	jako
prezident	prezident	k1gMnSc1	prezident
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
<g/>
50	[number]	k4	50
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
z.	z.	k?	z.
a	a	k8xC	a
n.	n.	k?	n.
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
zřízena	zřídit	k5eAaPmNgFnS	zřídit
druhá	druhý	k4xOgFnSc1	druhý
česká	český	k2eAgFnSc1d1	Česká
univerzita	univerzita	k1gFnSc1	univerzita
se	s	k7c7	s
čtyřmi	čtyři	k4xCgFnPc7	čtyři
zakládajícími	zakládající	k2eAgFnPc7d1	zakládající
fakultami	fakulta	k1gFnPc7	fakulta
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
dostala	dostat	k5eAaPmAgFnS	dostat
jméno	jméno	k1gNnSc4	jméno
po	po	k7c6	po
svém	svůj	k3xOyFgMnSc6	svůj
zakladateli	zakladatel	k1gMnSc6	zakladatel
–	–	k?	–
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1919	[number]	k4	1919
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
ustavily	ustavit	k5eAaPmAgFnP	ustavit
základní	základní	k2eAgFnPc4d1	základní
fakulty	fakulta	k1gFnPc4	fakulta
lékařská	lékařský	k2eAgFnSc1d1	lékařská
<g/>
,	,	kIx,	,
právnická	právnický	k2eAgFnSc1d1	právnická
<g/>
,	,	kIx,	,
přírodovědecká	přírodovědecký	k2eAgFnSc1d1	Přírodovědecká
a	a	k8xC	a
filozofická	filozofický	k2eAgFnSc1d1	filozofická
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
zahájila	zahájit	k5eAaPmAgFnS	zahájit
s	s	k7c7	s
Masarykovou	Masarykův	k2eAgFnSc7d1	Masarykova
podporou	podpora	k1gFnSc7	podpora
po	po	k7c6	po
několikaleté	několikaletý	k2eAgFnSc6d1	několikaletá
přípravě	příprava	k1gFnSc6	příprava
svoji	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
badatelská	badatelský	k2eAgFnSc1d1	badatelská
technicko-ekonomická	technickokonomický	k2eAgFnSc1d1	technicko-ekonomická
samosprávná	samosprávný	k2eAgFnSc1d1	samosprávná
instituce	instituce	k1gFnSc1	instituce
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
akademie	akademie	k1gFnSc2	akademie
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
jejímiž	jejíž	k3xOyRp3gMnPc7	jejíž
členy	člen	k1gMnPc7	člen
byli	být	k5eAaImAgMnP	být
většinou	většinou	k6eAd1	většinou
profesoři	profesor	k1gMnPc1	profesor
a	a	k8xC	a
vedoucí	vedoucí	k1gFnSc1	vedoucí
ústavů	ústav	k1gInPc2	ústav
technických	technický	k2eAgFnPc2d1	technická
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc1	návrh
na	na	k7c4	na
její	její	k3xOp3gNnSc4	její
zřízení	zřízení	k1gNnSc4	zřízení
byl	být	k5eAaImAgInS	být
podán	podat	k5eAaPmNgInS	podat
Národnímu	národní	k2eAgNnSc3d1	národní
shromáždění	shromáždění	k1gNnSc3	shromáždění
v	v	k7c4	v
den	den	k1gInSc4	den
příjezdu	příjezd	k1gInSc2	příjezd
prezidenta	prezident	k1gMnSc2	prezident
do	do	k7c2	do
vlasti	vlast	k1gFnSc2	vlast
20	[number]	k4	20
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
vznik	vznik	k1gInSc1	vznik
byl	být	k5eAaImAgInS	být
potvrzen	potvrdit	k5eAaPmNgInS	potvrdit
zákonem	zákon	k1gInSc7	zákon
č.	č.	k?	č.
86	[number]	k4	86
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
z.	z.	k?	z.
a	a	k8xC	a
nař	nař	k?	nař
<g/>
.	.	kIx.	.
29	[number]	k4	29
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1920	[number]	k4	1920
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
27	[number]	k4	27
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1920	[number]	k4	1920
byl	být	k5eAaImAgInS	být
zvolen	zvolen	k2eAgInSc1d1	zvolen
TGM	TGM	kA	TGM
prezidentem	prezident	k1gMnSc7	prezident
republiky	republika	k1gFnSc2	republika
podruhé	podruhé	k6eAd1	podruhé
a	a	k8xC	a
k	k	k7c3	k
volbě	volba	k1gFnSc3	volba
přijel	přijet	k5eAaPmAgMnS	přijet
s	s	k7c7	s
Františkem	František	k1gMnSc7	František
Tomáškem	Tomášek	k1gMnSc7	Tomášek
opět	opět	k6eAd1	opět
zcela	zcela	k6eAd1	zcela
otevřeným	otevřený	k2eAgInSc7d1	otevřený
automobilem	automobil	k1gInSc7	automobil
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
Praga	Praga	k1gFnSc1	Praga
Grand	grand	k1gMnSc1	grand
<g/>
,	,	kIx,	,
typu	typ	k1gInSc2	typ
G.	G.	kA	G.
Prezident	prezident	k1gMnSc1	prezident
Masaryk	Masaryk	k1gMnSc1	Masaryk
otevřeně	otevřeně	k6eAd1	otevřeně
podporoval	podporovat	k5eAaImAgMnS	podporovat
sionismus	sionismus	k1gInSc4	sionismus
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
ideový	ideový	k2eAgInSc1d1	ideový
směr	směr	k1gInSc1	směr
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc7	jeho
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
dobrovolné	dobrovolný	k2eAgNnSc1d1	dobrovolné
přesídlení	přesídlení	k1gNnSc1	přesídlení
Židů	Žid	k1gMnPc2	Žid
do	do	k7c2	do
země	zem	k1gFnSc2	zem
izraelské	izraelský	k2eAgFnSc2d1	izraelská
a	a	k8xC	a
vybudování	vybudování	k1gNnSc3	vybudování
a	a	k8xC	a
udržení	udržení	k1gNnSc3	udržení
židovského	židovský	k2eAgInSc2d1	židovský
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
díky	díky	k7c3	díky
vstřícnosti	vstřícnost	k1gFnSc3	vstřícnost
TGM	TGM	kA	TGM
a	a	k8xC	a
Československa	Československo	k1gNnSc2	Československo
jako	jako	k9	jako
takového	takový	k3xDgNnSc2	takový
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
ČSR	ČSR	kA	ČSR
konaly	konat	k5eAaImAgFnP	konat
hned	hned	k6eAd1	hned
tři	tři	k4xCgInPc4	tři
světové	světový	k2eAgInPc4d1	světový
sionistické	sionistický	k2eAgInPc4d1	sionistický
kongresy	kongres	k1gInPc4	kongres
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
Masaryk	Masaryk	k1gMnSc1	Masaryk
překonal	překonat	k5eAaPmAgMnS	překonat
nebezpečnou	bezpečný	k2eNgFnSc4d1	nebezpečná
trombózu	trombóza	k1gFnSc4	trombóza
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
vážné	vážný	k2eAgFnPc4d1	vážná
obavy	obava	k1gFnPc4	obava
o	o	k7c4	o
jeho	jeho	k3xOp3gInSc4	jeho
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Manželka	manželka	k1gFnSc1	manželka
Charlotta	Charlotta	k1gFnSc1	Charlotta
se	se	k3xPyFc4	se
ale	ale	k9	ale
již	již	k6eAd1	již
nezotavila	zotavit	k5eNaPmAgFnS	zotavit
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
těžké	těžký	k2eAgFnSc2d1	těžká
nemoci	nemoc	k1gFnSc2	nemoc
a	a	k8xC	a
13	[number]	k4	13
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1923	[number]	k4	1923
zemřela	zemřít	k5eAaPmAgFnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k9	již
za	za	k7c4	za
její	její	k3xOp3gFnPc4	její
nemoci	nemoc	k1gFnPc4	nemoc
převzala	převzít	k5eAaPmAgFnS	převzít
dcera	dcera	k1gFnSc1	dcera
Alice	Alice	k1gFnSc2	Alice
roli	role	k1gFnSc4	role
"	"	kIx"	"
<g/>
první	první	k4xOgFnPc1	první
dámy	dáma	k1gFnPc1	dáma
<g/>
"	"	kIx"	"
a	a	k8xC	a
zodpovědnost	zodpovědnost	k1gFnSc4	zodpovědnost
za	za	k7c4	za
prezidentskou	prezidentský	k2eAgFnSc4d1	prezidentská
domácnost	domácnost	k1gFnSc4	domácnost
<g/>
,	,	kIx,	,
mimo	mimo	k6eAd1	mimo
svého	svůj	k3xOyFgNnSc2	svůj
angažmá	angažmá	k1gNnSc2	angažmá
v	v	k7c6	v
Československém	československý	k2eAgInSc6d1	československý
červeném	červený	k2eAgInSc6d1	červený
kříži	kříž	k1gInSc6	kříž
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
založila	založit	k5eAaPmAgFnS	založit
(	(	kIx(	(
<g/>
pomohly	pomoct	k5eAaPmAgInP	pomoct
i	i	k9	i
otcovy	otcův	k2eAgInPc1d1	otcův
styky	styk	k1gInPc1	styk
v	v	k7c6	v
Ženevě	Ženeva	k1gFnSc6	Ženeva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dcera	dcera	k1gFnSc1	dcera
Olga	Olga	k1gFnSc1	Olga
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
provdala	provdat	k5eAaPmAgFnS	provdat
za	za	k7c4	za
švýcarského	švýcarský	k2eAgMnSc4d1	švýcarský
lékaře	lékař	k1gMnSc4	lékař
Henriho	Henri	k1gMnSc4	Henri
Revillioda	Revilliod	k1gMnSc4	Revilliod
a	a	k8xC	a
tehdy	tehdy	k6eAd1	tehdy
byl	být	k5eAaImAgInS	být
TGM	TGM	kA	TGM
zvolen	zvolit	k5eAaPmNgInS	zvolit
prezidentem	prezident	k1gMnSc7	prezident
podruhé	podruhé	k6eAd1	podruhé
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
rok	rok	k1gInSc1	rok
byl	být	k5eAaImAgInS	být
rovněž	rovněž	k9	rovněž
rokem	rok	k1gInSc7	rok
VII	VII	kA	VII
<g/>
.	.	kIx.	.
</s>
<s>
Všesokolského	všesokolský	k2eAgInSc2d1	všesokolský
sletu	slet	k1gInSc2	slet
a	a	k8xC	a
pan	pan	k1gMnSc1	pan
prezident	prezident	k1gMnSc1	prezident
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgMnS	být
členem	člen	k1gInSc7	člen
Sokola	Sokol	k1gMnSc2	Sokol
již	již	k6eAd1	již
od	od	k7c2	od
třinácti	třináct	k4xCc2	třináct
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
přijel	přijet	k5eAaPmAgMnS	přijet
na	na	k7c4	na
letenský	letenský	k2eAgInSc4d1	letenský
stadion	stadion	k1gInSc4	stadion
na	na	k7c6	na
koni	kůň	k1gMnSc6	kůň
Hektorovi	Hektor	k1gMnSc6	Hektor
<g/>
.	.	kIx.	.
</s>
<s>
Syn	syn	k1gMnSc1	syn
Jan	Jan	k1gMnSc1	Jan
Masaryk	Masaryk	k1gMnSc1	Masaryk
většinou	většinou	k6eAd1	většinou
pobýval	pobývat	k5eAaImAgMnS	pobývat
jako	jako	k9	jako
diplomat	diplomat	k1gMnSc1	diplomat
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
T.	T.	kA	T.
G.	G.	kA	G.
Masaryk	Masaryk	k1gMnSc1	Masaryk
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
pobýval	pobývat	k5eAaImAgMnS	pobývat
často	často	k6eAd1	často
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
,	,	kIx,	,
nejen	nejen	k6eAd1	nejen
o	o	k7c6	o
dovolené	dovolená	k1gFnSc6	dovolená
(	(	kIx(	(
<g/>
v	v	k7c6	v
Bystričce	Bystrička	k1gFnSc6	Bystrička
<g/>
,	,	kIx,	,
Topoľčiankách	Topoľčianka	k1gFnPc6	Topoľčianka
<g/>
,	,	kIx,	,
na	na	k7c6	na
Hrušově	Hrušov	k1gInSc6	Hrušov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
služebně	služebně	k6eAd1	služebně
(	(	kIx(	(
<g/>
např.	např.	kA	např.
21	[number]	k4	21
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1921	[number]	k4	1921
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
vojenské	vojenský	k2eAgFnSc2d1	vojenská
přehlídky	přehlídka	k1gFnSc2	přehlídka
bratislavských	bratislavský	k2eAgInPc2d1	bratislavský
pluků	pluk	k1gInPc2	pluk
v	v	k7c6	v
Petržalce	Petržalka	k1gFnSc6	Petržalka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podporoval	podporovat	k5eAaImAgMnS	podporovat
budování	budování	k1gNnSc4	budování
letectva	letectvo	k1gNnSc2	letectvo
i	i	k8xC	i
armády	armáda	k1gFnSc2	armáda
na	na	k7c6	na
základě	základ	k1gInSc6	základ
československých	československý	k2eAgFnPc2d1	Československá
legií	legie	k1gFnPc2	legie
<g/>
.	.	kIx.	.
</s>
<s>
Účastnil	účastnit	k5eAaImAgInS	účastnit
se	se	k3xPyFc4	se
manévrů	manévr	k1gInPc2	manévr
<g/>
,	,	kIx,	,
přehlídek	přehlídka	k1gFnPc2	přehlídka
<g/>
,	,	kIx,	,
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
letiště	letiště	k1gNnPc4	letiště
a	a	k8xC	a
výcvikové	výcvikový	k2eAgFnPc4d1	výcviková
prostory	prostora	k1gFnPc4	prostora
(	(	kIx(	(
<g/>
jen	jen	k9	jen
v	v	k7c6	v
letech	let	k1gInPc6	let
1920	[number]	k4	1920
<g/>
–	–	k?	–
<g/>
1922	[number]	k4	1922
navštívil	navštívit	k5eAaPmAgInS	navštívit
šestkrát	šestkrát	k6eAd1	šestkrát
vojenský	vojenský	k2eAgInSc4d1	vojenský
výcvikový	výcvikový	k2eAgInSc4d1	výcvikový
prostor	prostor	k1gInSc4	prostor
Milovice	Milovice	k1gFnPc4	Milovice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Masaryk	Masaryk	k1gMnSc1	Masaryk
představoval	představovat	k5eAaImAgMnS	představovat
světu	svět	k1gInSc3	svět
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
moderních	moderní	k2eAgInPc2d1	moderní
a	a	k8xC	a
vyspělých	vyspělý	k2eAgInPc2d1	vyspělý
států	stát	k1gInPc2	stát
meziválečné	meziválečný	k2eAgFnSc2d1	meziválečná
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jeho	jeho	k3xOp3gFnPc1	jeho
síly	síla	k1gFnPc1	síla
dovolily	dovolit	k5eAaPmAgFnP	dovolit
<g/>
,	,	kIx,	,
přispíval	přispívat	k5eAaImAgMnS	přispívat
i	i	k9	i
k	k	k7c3	k
jeho	jeho	k3xOp3gInSc3	jeho
dalšímu	další	k2eAgInSc3d1	další
rozvoji	rozvoj	k1gInSc3	rozvoj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Edvardu	Edvard	k1gMnSc6	Edvard
Benešovi	Beneš	k1gMnSc6	Beneš
měl	mít	k5eAaImAgMnS	mít
ideálního	ideální	k2eAgMnSc4d1	ideální
ministra	ministr	k1gMnSc4	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnSc1d1	další
jeho	jeho	k3xOp3gMnSc1	jeho
spojenec	spojenec	k1gMnSc1	spojenec
Milan	Milan	k1gMnSc1	Milan
Rastislav	Rastislav	k1gMnSc1	Rastislav
Štefánik	Štefánik	k1gMnSc1	Štefánik
<g/>
,	,	kIx,	,
ministr	ministr	k1gMnSc1	ministr
obrany	obrana	k1gFnSc2	obrana
<g/>
,	,	kIx,	,
zahynul	zahynout	k5eAaPmAgMnS	zahynout
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gInSc1	jeho
letoun	letoun	k1gInSc1	letoun
za	za	k7c2	za
války	válka	k1gFnSc2	válka
s	s	k7c7	s
Maďary	Maďar	k1gMnPc7	Maďar
za	za	k7c2	za
nejasných	jasný	k2eNgFnPc2d1	nejasná
okolností	okolnost	k1gFnPc2	okolnost
zřítil	zřítit	k5eAaPmAgInS	zřítit
<g/>
.	.	kIx.	.
</s>
<s>
Alois	Alois	k1gMnSc1	Alois
Rašín	Rašín	k1gMnSc1	Rašín
<g/>
,	,	kIx,	,
ministr	ministr	k1gMnSc1	ministr
financí	finance	k1gFnPc2	finance
<g/>
,	,	kIx,	,
Masarykův	Masarykův	k2eAgMnSc1d1	Masarykův
někdejší	někdejší	k2eAgMnSc1d1	někdejší
protivník	protivník	k1gMnSc1	protivník
ale	ale	k8xC	ale
nyní	nyní	k6eAd1	nyní
spolehlivý	spolehlivý	k2eAgMnSc1d1	spolehlivý
spojenec	spojenec	k1gMnSc1	spojenec
<g/>
,	,	kIx,	,
předčasně	předčasně	k6eAd1	předčasně
zemřel	zemřít	k5eAaPmAgMnS	zemřít
1923	[number]	k4	1923
na	na	k7c4	na
následky	následek	k1gInPc4	následek
atentátu	atentát	k1gInSc2	atentát
a	a	k8xC	a
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
zemřela	zemřít	k5eAaPmAgFnS	zemřít
i	i	k9	i
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
žena	žena	k1gFnSc1	žena
Charlotta	Charlotta	k1gFnSc1	Charlotta
Garrigue	Garrigue	k1gFnSc1	Garrigue
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Antonínu	Antonín	k1gMnSc6	Antonín
Švehlovi	Švehla	k1gMnSc6	Švehla
(	(	kIx(	(
<g/>
agrární	agrární	k2eAgFnSc1d1	agrární
strana	strana	k1gFnSc1	strana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
předsedovi	předsedův	k2eAgMnPc1d1	předsedův
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
dalšího	další	k2eAgMnSc4d1	další
spojence	spojenec	k1gMnSc4	spojenec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Karel	Karel	k1gMnSc1	Karel
Kramář	kramář	k1gMnSc1	kramář
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
(	(	kIx(	(
<g/>
Národně	národně	k6eAd1	národně
demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Masarykovým	Masarykův	k2eAgMnSc7d1	Masarykův
politickým	politický	k2eAgMnSc7d1	politický
odpůrcem	odpůrce	k1gMnSc7	odpůrce
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
zůstali	zůstat	k5eAaPmAgMnP	zůstat
osobními	osobní	k2eAgMnPc7d1	osobní
přáteli	přítel	k1gMnPc7	přítel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1923	[number]	k4	1923
byl	být	k5eAaImAgMnS	být
Masaryk	Masaryk	k1gMnSc1	Masaryk
jako	jako	k8xS	jako
prezident	prezident	k1gMnSc1	prezident
přivítán	přivítán	k2eAgMnSc1d1	přivítán
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
Belgii	Belgie	k1gFnSc6	Belgie
a	a	k8xC	a
Velké	velký	k2eAgFnSc3d1	velká
Británii	Británie	k1gFnSc3	Británie
<g/>
,	,	kIx,	,
když	když	k8xS	když
předtím	předtím	k6eAd1	předtím
pobýval	pobývat	k5eAaImAgMnS	pobývat
soukromě	soukromě	k6eAd1	soukromě
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
a	a	k8xC	a
Palestině	Palestina	k1gFnSc6	Palestina
<g/>
,	,	kIx,	,
na	na	k7c6	na
výroční	výroční	k2eAgFnSc6d1	výroční
cestě	cesta	k1gFnSc6	cesta
k	k	k7c3	k
pátému	pátý	k4xOgNnSc3	pátý
výročí	výročí	k1gNnSc3	výročí
vzniku	vznik	k1gInSc2	vznik
ČSR	ČSR	kA	ČSR
<g/>
.	.	kIx.	.
</s>
<s>
Překonal	překonat	k5eAaPmAgInS	překonat
též	též	k9	též
zápal	zápal	k1gInSc1	zápal
plic	plíce	k1gFnPc2	plíce
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
dostavil	dostavit	k5eAaPmAgMnS	dostavit
po	po	k7c4	po
oslabení	oslabení	k1gNnSc4	oslabení
vlivem	vliv	k1gInSc7	vliv
úmrtí	úmrtí	k1gNnSc2	úmrtí
jeho	jeho	k3xOp3gFnSc2	jeho
ženy	žena	k1gFnSc2	žena
a	a	k8xC	a
nachlazení	nachlazení	k1gNnSc1	nachlazení
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
ve	v	k7c6	v
Hluboši	Hluboš	k1gMnSc6	Hluboš
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
navštívil	navštívit	k5eAaPmAgMnS	navštívit
univerzitu	univerzita	k1gFnSc4	univerzita
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
oslav	oslava	k1gFnPc2	oslava
jejího	její	k3xOp3gNnSc2	její
pětiletého	pětiletý	k2eAgNnSc2d1	pětileté
trvání	trvání	k1gNnSc2	trvání
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ocenil	ocenit	k5eAaPmAgMnS	ocenit
tuto	tento	k3xDgFnSc4	tento
vysokou	vysoký	k2eAgFnSc4d1	vysoká
školu	škola	k1gFnSc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
též	též	k9	též
vybral	vybrat	k5eAaPmAgMnS	vybrat
sochaře	sochař	k1gMnSc4	sochař
Otakara	Otakar	k1gMnSc4	Otakar
Španiela	Španiel	k1gMnSc4	Španiel
a	a	k8xC	a
Jaroslava	Jaroslav	k1gMnSc4	Jaroslav
Bendu	Benda	k1gMnSc4	Benda
pro	pro	k7c4	pro
úkol	úkol	k1gInSc4	úkol
tvorby	tvorba	k1gFnSc2	tvorba
rektorských	rektorský	k2eAgFnPc2d1	Rektorská
insignií	insignie	k1gFnPc2	insignie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
stál	stát	k5eAaImAgInS	stát
též	též	k9	též
u	u	k7c2	u
zrodu	zrod	k1gInSc2	zrod
časopisu	časopis	k1gInSc2	časopis
Přítomnost	přítomnost	k1gFnSc1	přítomnost
novináře	novinář	k1gMnSc2	novinář
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
Peroutky	Peroutka	k1gMnSc2	Peroutka
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ministrem	ministr	k1gMnSc7	ministr
obchodu	obchod	k1gInSc2	obchod
USA	USA	kA	USA
Herbertem	Herbert	k1gMnSc7	Herbert
Hooverem	Hoover	k1gMnSc7	Hoover
<g/>
,	,	kIx,	,
pozdějším	pozdní	k2eAgMnSc7d2	pozdější
americkým	americký	k2eAgMnSc7d1	americký
prezidentem	prezident	k1gMnSc7	prezident
<g/>
,	,	kIx,	,
převzal	převzít	k5eAaPmAgMnS	převzít
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1924	[number]	k4	1924
záštitu	záštita	k1gFnSc4	záštita
nad	nad	k7c7	nad
"	"	kIx"	"
<g/>
Prvním	první	k4xOgInSc7	první
světovým	světový	k2eAgInSc7d1	světový
kongresem	kongres	k1gInSc7	kongres
o	o	k7c6	o
vědeckém	vědecký	k2eAgNnSc6d1	vědecké
řízení	řízení	k1gNnSc6	řízení
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
PIMCO	PIMCO	kA	PIMCO
–	–	k?	–
First	First	k1gInSc1	First
Prague	Pragu	k1gMnSc2	Pragu
International	International	k1gFnSc1	International
Management	management	k1gInSc1	management
Congress	Congress	k1gInSc1	Congress
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
organizovaným	organizovaný	k2eAgInSc7d1	organizovaný
Masarykovou	Masarykův	k2eAgFnSc7d1	Masarykova
akademií	akademie	k1gFnSc7	akademie
práce	práce	k1gFnSc2	práce
v	v	k7c6	v
Pantheonu	Pantheon	k1gInSc6	Pantheon
Národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
také	také	k9	také
dohlížel	dohlížet	k5eAaImAgMnS	dohlížet
na	na	k7c4	na
dostavbu	dostavba	k1gFnSc4	dostavba
nového	nový	k2eAgInSc2d1	nový
velkého	velký	k2eAgInSc2d1	velký
Sokolského	sokolský	k2eAgInSc2d1	sokolský
stadionu	stadion	k1gInSc2	stadion
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
Strahově	Strahov	k1gInSc6	Strahov
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
přijížděl	přijíždět	k5eAaImAgMnS	přijíždět
z	z	k7c2	z
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
na	na	k7c6	na
svém	svůj	k3xOyFgMnSc6	svůj
oblíbeném	oblíbený	k2eAgMnSc6d1	oblíbený
koni	kůň	k1gMnSc6	kůň
Hektorovi	Hektor	k1gMnSc6	Hektor
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
Velikonocích	Velikonoce	k1gFnPc6	Velikonoce
na	na	k7c4	na
Bílou	bílý	k2eAgFnSc4d1	bílá
sobotu	sobota	k1gFnSc4	sobota
dopoledne	dopoledne	k6eAd1	dopoledne
vyhlašoval	vyhlašovat	k5eAaImAgMnS	vyhlašovat
ze	z	k7c2	z
schodiště	schodiště	k1gNnSc2	schodiště
Parlamentu	parlament	k1gInSc2	parlament
(	(	kIx(	(
<g/>
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Rudolfina	Rudolfinum	k1gNnSc2	Rudolfinum
na	na	k7c6	na
Palachově	palachově	k6eAd1	palachově
náměstí	náměstí	k1gNnSc6	náměstí
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
)	)	kIx)	)
pravidelně	pravidelně	k6eAd1	pravidelně
"	"	kIx"	"
<g/>
Mír	mír	k1gInSc1	mír
Československého	československý	k2eAgInSc2d1	československý
červeného	červený	k2eAgInSc2d1	červený
kříže	kříž	k1gInSc2	kříž
<g/>
"	"	kIx"	"
pěti	pět	k4xCc3	pět
minutami	minuta	k1gFnPc7	minuta
ticha	ticho	k1gNnSc2	ticho
a	a	k8xC	a
dopravního	dopravní	k2eAgInSc2d1	dopravní
klidu	klid	k1gInSc2	klid
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Československé	československý	k2eAgFnSc6d1	Československá
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
1926	[number]	k4	1926
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
s	s	k7c7	s
celým	celý	k2eAgInSc7d1	celý
generálním	generální	k2eAgInSc7d1	generální
štábem	štáb	k1gInSc7	štáb
velkého	velký	k2eAgNnSc2d1	velké
vojenského	vojenský	k2eAgNnSc2d1	vojenské
cvičení	cvičení	k1gNnSc2	cvičení
v	v	k7c6	v
Milovicích	Milovice	k1gFnPc6	Milovice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1927	[number]	k4	1927
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
prezidentem	prezident	k1gMnSc7	prezident
republiky	republika	k1gFnSc2	republika
potřetí	potřetí	k4xO	potřetí
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
osobně	osobně	k6eAd1	osobně
slavnostního	slavnostní	k2eAgNnSc2d1	slavnostní
položení	položení	k1gNnSc2	položení
základního	základní	k2eAgInSc2d1	základní
kamene	kámen	k1gInSc2	kámen
k	k	k7c3	k
nové	nový	k2eAgFnSc3d1	nová
budově	budova	k1gFnSc3	budova
právnické	právnický	k2eAgFnSc2d1	právnická
fakulty	fakulta	k1gFnSc2	fakulta
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
prezident	prezident	k1gMnSc1	prezident
Československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
též	též	k9	též
cestoval	cestovat	k5eAaImAgMnS	cestovat
oficiálně	oficiálně	k6eAd1	oficiálně
po	po	k7c6	po
dalších	další	k2eAgFnPc6d1	další
českých	český	k2eAgFnPc6d1	Česká
<g/>
,	,	kIx,	,
moravských	moravský	k2eAgFnPc6d1	Moravská
a	a	k8xC	a
slovenských	slovenský	k2eAgNnPc6d1	slovenské
městech	město	k1gNnPc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
i	i	k8xC	i
zahajoval	zahajovat	k5eAaImAgMnS	zahajovat
mnohé	mnohý	k2eAgFnPc4d1	mnohá
významné	významný	k2eAgFnPc4d1	významná
výstavy	výstava	k1gFnPc4	výstava
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
republice	republika	k1gFnSc6	republika
a	a	k8xC	a
veletrhy	veletrh	k1gInPc1	veletrh
(	(	kIx(	(
<g/>
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
i	i	k8xC	i
finančně	finančně	k6eAd1	finančně
podpořil	podpořit	k5eAaPmAgMnS	podpořit
výstavbu	výstavba	k1gFnSc4	výstavba
budovy	budova	k1gFnSc2	budova
spolku	spolek	k1gInSc2	spolek
Mánes	Mánes	k1gMnSc1	Mánes
na	na	k7c6	na
dnešním	dnešní	k2eAgNnSc6d1	dnešní
Masarykově	Masarykův	k2eAgNnSc6d1	Masarykovo
nábřeží	nábřeží	k1gNnSc6	nábřeží
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Podporoval	podporovat	k5eAaImAgMnS	podporovat
též	též	k9	též
dostihový	dostihový	k2eAgInSc4d1	dostihový
sport	sport	k1gInSc4	sport
a	a	k8xC	a
na	na	k7c6	na
koni	kůň	k1gMnSc6	kůň
dojížděl	dojíždět	k5eAaImAgInS	dojíždět
i	i	k9	i
na	na	k7c4	na
závodiště	závodiště	k1gNnSc4	závodiště
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Chuchli	Chuchle	k1gFnSc6	Chuchle
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1931	[number]	k4	1931
sledoval	sledovat	k5eAaImAgInS	sledovat
s	s	k7c7	s
předsedou	předseda	k1gMnSc7	předseda
vlády	vláda	k1gFnSc2	vláda
Františkem	František	k1gMnSc7	František
Udržalem	Udržal	k1gInSc7	Udržal
celodenní	celodenní	k2eAgInSc4d1	celodenní
dostihový	dostihový	k2eAgInSc4d1	dostihový
program	program	k1gInSc4	program
včetně	včetně	k7c2	včetně
Československého	československý	k2eAgNnSc2d1	Československé
derby	derby	k1gNnSc2	derby
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
a	a	k8xC	a
1932	[number]	k4	1932
přijel	přijet	k5eAaPmAgMnS	přijet
na	na	k7c6	na
koni	kůň	k1gMnSc6	kůň
Hektorovi	Hektor	k1gMnSc6	Hektor
na	na	k7c6	na
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
a	a	k8xC	a
IX	IX	kA	IX
<g/>
.	.	kIx.	.
</s>
<s>
Všesokolský	všesokolský	k2eAgInSc1d1	všesokolský
slet	slet	k1gInSc1	slet
na	na	k7c6	na
Strahově	Strahov	k1gInSc6	Strahov
a	a	k8xC	a
15	[number]	k4	15
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc2	výročí
republiky	republika	k1gFnSc2	republika
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1933	[number]	k4	1933
oslavil	oslavit	k5eAaPmAgInS	oslavit
poslední	poslední	k2eAgFnSc7d1	poslední
projížďkou	projížďka	k1gFnSc7	projížďka
na	na	k7c6	na
novém	nový	k2eAgMnSc6d1	nový
koni	kůň	k1gMnSc6	kůň
Prahou	Praha	k1gFnSc7	Praha
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
přehlídkového	přehlídkový	k2eAgNnSc2d1	přehlídkové
jízdního	jízdní	k2eAgNnSc2d1	jízdní
vojska	vojsko	k1gNnSc2	vojsko
od	od	k7c2	od
Muzea	muzeum	k1gNnSc2	muzeum
na	na	k7c6	na
Václavském	václavský	k2eAgNnSc6d1	Václavské
náměstí	náměstí	k1gNnSc6	náměstí
až	až	k9	až
po	po	k7c4	po
Rudolfinum	Rudolfinum	k1gNnSc4	Rudolfinum
na	na	k7c6	na
dnešním	dnešní	k2eAgNnSc6d1	dnešní
náměstí	náměstí	k1gNnSc6	náměstí
Jana	Jan	k1gMnSc2	Jan
Palacha	Palach	k1gMnSc2	Palach
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sídlilo	sídlit	k5eAaImAgNnS	sídlit
Národní	národní	k2eAgNnSc1d1	národní
shromáždění	shromáždění	k1gNnSc1	shromáždění
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
zbudována	zbudován	k2eAgFnSc1d1	zbudována
slavnostní	slavnostní	k2eAgFnSc1d1	slavnostní
tribuna	tribuna	k1gFnSc1	tribuna
<g/>
.	.	kIx.	.
</s>
<s>
Prezidentem	prezident	k1gMnSc7	prezident
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
i	i	k9	i
24	[number]	k4	24
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1934	[number]	k4	1934
<g/>
,	,	kIx,	,
počtvrté	počtvrté	k4xO	počtvrté
a	a	k8xC	a
naposledy	naposledy	k6eAd1	naposledy
i	i	k9	i
přes	přes	k7c4	přes
špatný	špatný	k2eAgInSc4d1	špatný
zdravotní	zdravotní	k2eAgInSc4d1	zdravotní
stav	stav	k1gInSc4	stav
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
síni	síň	k1gFnSc6	síň
Rudolfina	Rudolfinum	k1gNnSc2	Rudolfinum
<g/>
,	,	kIx,	,
před	před	k7c7	před
svojí	svůj	k3xOyFgFnSc7	svůj
"	"	kIx"	"
<g/>
Štursovou	Štursův	k2eAgFnSc7d1	Štursova
<g/>
"	"	kIx"	"
sochou	socha	k1gFnSc7	socha
z	z	k7c2	z
bílého	bílý	k2eAgInSc2d1	bílý
mramoru	mramor	k1gInSc2	mramor
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
socha	socha	k1gFnSc1	socha
umístěna	umístit	k5eAaPmNgFnS	umístit
v	v	k7c6	v
Rotmayerově	Rotmayerův	k2eAgInSc6d1	Rotmayerův
sále	sál	k1gInSc6	sál
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
Masaryk	Masaryk	k1gMnSc1	Masaryk
osobně	osobně	k6eAd1	osobně
vykonal	vykonat	k5eAaPmAgMnS	vykonat
slib	slib	k1gInSc4	slib
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
odejel	odejet	k5eAaPmAgInS	odejet
otevřeným	otevřený	k2eAgInSc7d1	otevřený
automobilem	automobil	k1gInSc7	automobil
přes	přes	k7c4	přes
Mánesův	Mánesův	k2eAgInSc4d1	Mánesův
most	most	k1gInSc4	most
na	na	k7c4	na
oficiální	oficiální	k2eAgFnSc4d1	oficiální
oslavu	oslava	k1gFnSc4	oslava
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
již	již	k6eAd1	již
ve	v	k7c6	v
Vladislavském	vladislavský	k2eAgInSc6d1	vladislavský
sále	sál	k1gInSc6	sál
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
osobně	osobně	k6eAd1	osobně
přítomen	přítomen	k2eAgInSc1d1	přítomen
<g/>
.	.	kIx.	.
</s>
<s>
Prezidentský	prezidentský	k2eAgInSc1d1	prezidentský
slib	slib	k1gInSc1	slib
byl	být	k5eAaImAgInS	být
přenášen	přenášet	k5eAaImNgInS	přenášet
v	v	k7c6	v
přímém	přímý	k2eAgInSc6d1	přímý
přenosu	přenos	k1gInSc6	přenos
Československým	československý	k2eAgInSc7d1	československý
rozhlasem	rozhlas	k1gInSc7	rozhlas
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nemoci	nemoc	k1gFnSc6	nemoc
však	však	k9	však
již	již	k6eAd1	již
nemohl	moct	k5eNaImAgMnS	moct
jezdit	jezdit	k5eAaImF	jezdit
na	na	k7c6	na
koni	kůň	k1gMnSc6	kůň
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
"	"	kIx"	"
<g/>
sokolovat	sokolovat	k5eAaImF	sokolovat
<g/>
"	"	kIx"	"
a	a	k8xC	a
úřadovat	úřadovat	k5eAaImF	úřadovat
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
jak	jak	k6eAd1	jak
byl	být	k5eAaImAgInS	být
zvyklý	zvyklý	k2eAgMnSc1d1	zvyklý
(	(	kIx(	(
<g/>
ochromená	ochromený	k2eAgFnSc1d1	ochromená
pravá	pravý	k2eAgFnSc1d1	pravá
ruka	ruka	k1gFnSc1	ruka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
14	[number]	k4	14
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1935	[number]	k4	1935
abdikoval	abdikovat	k5eAaBmAgMnS	abdikovat
ze	z	k7c2	z
zdravotních	zdravotní	k2eAgInPc2d1	zdravotní
důvodů	důvod	k1gInPc2	důvod
a	a	k8xC	a
pobýval	pobývat	k5eAaImAgMnS	pobývat
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
v	v	k7c6	v
Lánech	lán	k1gInPc6	lán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
předal	předat	k5eAaPmAgMnS	předat
v	v	k7c6	v
Lánech	lán	k1gInPc6	lán
zlatý	zlatý	k2eAgInSc4d1	zlatý
rektorský	rektorský	k2eAgInSc4d1	rektorský
řetěz	řetěz	k1gInSc4	řetěz
<g/>
,	,	kIx,	,
vytvořený	vytvořený	k2eAgInSc4d1	vytvořený
Otakarem	Otakar	k1gMnSc7	Otakar
Španielem	Španiel	k1gMnSc7	Španiel
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Jaroslavem	Jaroslav	k1gMnSc7	Jaroslav
Bendou	Benda	k1gMnSc7	Benda
rektorovi	rektor	k1gMnSc3	rektor
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
Janu	Jan	k1gMnSc3	Jan
Krejčímu	Krejčí	k1gMnSc3	Krejčí
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
poslední	poslední	k2eAgNnSc1d1	poslední
veřejné	veřejný	k2eAgNnSc1d1	veřejné
vystoupení	vystoupení	k1gNnSc1	vystoupení
bylo	být	k5eAaImAgNnS	být
4	[number]	k4	4
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1937	[number]	k4	1937
za	za	k7c2	za
Zborovských	Zborovský	k2eAgFnPc2d1	Zborovská
oslav	oslava	k1gFnPc2	oslava
na	na	k7c6	na
zcela	zcela	k6eAd1	zcela
zaplněném	zaplněný	k2eAgInSc6d1	zaplněný
velkém	velký	k2eAgInSc6d1	velký
Masarykově	Masarykův	k2eAgInSc6d1	Masarykův
státním	státní	k2eAgInSc6d1	státní
stadionu	stadion	k1gInSc6	stadion
na	na	k7c6	na
Strahově	Strahov	k1gInSc6	Strahov
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
tehdy	tehdy	k6eAd1	tehdy
použil	použít	k5eAaPmAgInS	použít
otevřený	otevřený	k2eAgInSc1d1	otevřený
automobil	automobil	k1gInSc1	automobil
české	český	k2eAgFnSc2d1	Česká
výroby	výroba	k1gFnSc2	výroba
Tatra	Tatra	k1gFnSc1	Tatra
T80	T80	k1gMnSc1	T80
(	(	kIx(	(
<g/>
dochoval	dochovat	k5eAaPmAgInS	dochovat
se	se	k3xPyFc4	se
dodnes	dodnes	k6eAd1	dodnes
jako	jako	k9	jako
exponát	exponát	k1gInSc4	exponát
Národního	národní	k2eAgNnSc2d1	národní
technického	technický	k2eAgNnSc2d1	technické
muzea	muzeum	k1gNnSc2	muzeum
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
nadšené	nadšený	k2eAgFnSc6d1	nadšená
atmosféře	atmosféra	k1gFnSc6	atmosféra
tohoto	tento	k3xDgInSc2	tento
dne	den	k1gInSc2	den
svědčí	svědčit	k5eAaImIp3nS	svědčit
i	i	k9	i
Nezvalova	Nezvalův	k2eAgFnSc1d1	Nezvalova
báseň	báseň	k1gFnSc1	báseň
"	"	kIx"	"
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
červenec	červenec	k1gInSc1	červenec
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
osoba	osoba	k1gFnSc1	osoba
bývá	bývat	k5eAaImIp3nS	bývat
spojována	spojovat	k5eAaImNgFnS	spojovat
většinou	většinou	k6eAd1	většinou
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
obdobím	období	k1gNnSc7	období
první	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
značnou	značný	k2eAgFnSc4d1	značná
část	část	k1gFnSc4	část
svého	svůj	k3xOyFgNnSc2	svůj
díla	dílo	k1gNnSc2	dílo
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
již	již	k6eAd1	již
před	před	k7c7	před
ní	on	k3xPp3gFnSc7	on
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
filozofickou	filozofický	k2eAgFnSc4d1	filozofická
realizaci	realizace	k1gFnSc4	realizace
jeho	jeho	k3xOp3gNnSc2	jeho
díla	dílo	k1gNnSc2	dílo
knižně	knižně	k6eAd1	knižně
<g/>
,	,	kIx,	,
literární	literární	k2eAgFnSc4d1	literární
časopiseckou	časopisecký	k2eAgFnSc4d1	časopisecká
činnost	činnost	k1gFnSc4	činnost
a	a	k8xC	a
vynikající	vynikající	k2eAgFnSc4d1	vynikající
aktivitu	aktivita	k1gFnSc4	aktivita
v	v	k7c6	v
jeho	jeho	k3xOp3gNnPc6	jeho
třech	tři	k4xCgNnPc6	tři
poslaneckých	poslanecký	k2eAgNnPc6d1	poslanecké
obdobích	období	k1gNnPc6	období
ve	v	k7c6	v
vídeňské	vídeňský	k2eAgFnSc6d1	Vídeňská
říšské	říšský	k2eAgFnSc6d1	říšská
radě	rada	k1gFnSc6	rada
<g/>
.	.	kIx.	.
</s>
<s>
Masaryk	Masaryk	k1gMnSc1	Masaryk
byl	být	k5eAaImAgMnS	být
označen	označit	k5eAaPmNgMnS	označit
za	za	k7c4	za
"	"	kIx"	"
<g/>
Prezidenta	prezident	k1gMnSc4	prezident
Osvoboditele	osvoboditel	k1gMnSc4	osvoboditel
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
oficiální	oficiální	k2eAgInSc4d1	oficiální
titul	titul	k1gInSc4	titul
zákonem	zákon	k1gInSc7	zákon
č.	č.	k?	č.
232	[number]	k4	232
<g/>
/	/	kIx~	/
<g/>
1935	[number]	k4	1935
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
z.	z.	k?	z.
a	a	k8xC	a
n.	n.	k?	n.
<g/>
,	,	kIx,	,
z	z	k7c2	z
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
za	za	k7c2	za
jeho	on	k3xPp3gInSc2	on
života	život	k1gInSc2	život
a	a	k8xC	a
prezidentování	prezidentování	k1gNnSc2	prezidentování
<g/>
,	,	kIx,	,
k	k	k7c3	k
jeho	jeho	k3xOp3gInPc3	jeho
osmdesátým	osmdesátý	k4xOgInPc3	osmdesátý
narozeninám	narozeniny	k1gFnPc3	narozeniny
7.3	[number]	k4	7.3
<g/>
.1930	.1930	k4	.1930
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
národní	národní	k2eAgNnSc1d1	národní
shromáždění	shromáždění	k1gNnSc1	shromáždění
usneslo	usnést	k5eAaPmAgNnS	usnést
na	na	k7c6	na
zákoně	zákon	k1gInSc6	zákon
"	"	kIx"	"
<g/>
Masaryk	Masaryk	k1gMnSc1	Masaryk
se	se	k3xPyFc4	se
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
o	o	k7c4	o
stát	stát	k1gInSc4	stát
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Lex	Lex	k1gMnSc1	Lex
Masaryk	Masaryk	k1gMnSc1	Masaryk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1937	[number]	k4	1937
vážně	vážně	k6eAd1	vážně
onemocněl	onemocnět	k5eAaPmAgInS	onemocnět
a	a	k8xC	a
v	v	k7c6	v
pondělí	pondělí	k1gNnSc6	pondělí
13	[number]	k4	13
<g/>
.	.	kIx.	.
vzdali	vzdát	k5eAaPmAgMnP	vzdát
lékaři	lékař	k1gMnPc1	lékař
v	v	k7c6	v
pozdních	pozdní	k2eAgFnPc6d1	pozdní
nočních	noční	k2eAgFnPc6d1	noční
hodinách	hodina	k1gFnPc6	hodina
další	další	k2eAgInSc4d1	další
boj	boj	k1gInSc4	boj
o	o	k7c4	o
jeho	on	k3xPp3gInSc4	on
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
T.	T.	kA	T.
G.	G.	kA	G.
Masaryk	Masaryk	k1gMnSc1	Masaryk
14	[number]	k4	14
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1937	[number]	k4	1937
po	po	k7c6	po
zánětu	zánět	k1gInSc6	zánět
plic	plíce	k1gFnPc2	plíce
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
zemřel	zemřít	k5eAaPmAgMnS	zemřít
pro	pro	k7c4	pro
jeho	on	k3xPp3gMnSc4	on
obdivovatele	obdivovatel	k1gMnSc4	obdivovatel
symbol	symbol	k1gInSc4	symbol
morální	morální	k2eAgFnSc2d1	morální
velikosti	velikost	k1gFnSc2	velikost
a	a	k8xC	a
velké	velký	k2eAgFnSc2d1	velká
autority	autorita	k1gFnSc2	autorita
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgInSc1d1	státní
pohřeb	pohřeb	k1gInSc1	pohřeb
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
v	v	k7c6	v
úterý	úterý	k1gNnSc6	úterý
21	[number]	k4	21
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1937	[number]	k4	1937
za	za	k7c2	za
zvuku	zvuk	k1gInSc2	zvuk
Svatováclavského	svatováclavský	k2eAgInSc2d1	svatováclavský
chorálu	chorál	k1gInSc2	chorál
z	z	k7c2	z
Plečnikovy	Plečnikův	k2eAgFnSc2d1	Plečnikova
sloupové	sloupový	k2eAgFnSc2d1	sloupová
síně	síň	k1gFnSc2	síň
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
a	a	k8xC	a
končil	končit	k5eAaImAgInS	končit
po	po	k7c6	po
více	hodně	k6eAd2	hodně
než	než	k8xS	než
devíti	devět	k4xCc6	devět
hodinách	hodina	k1gFnPc6	hodina
za	za	k7c2	za
modlitby	modlitba	k1gFnSc2	modlitba
"	"	kIx"	"
<g/>
Otčenáš	otčenáš	k1gInSc1	otčenáš
<g/>
"	"	kIx"	"
uložením	uložení	k1gNnSc7	uložení
do	do	k7c2	do
hrobu	hrob	k1gInSc2	hrob
na	na	k7c6	na
Lánském	lánský	k2eAgInSc6d1	lánský
hřbitově	hřbitov	k1gInSc6	hřbitov
<g/>
.	.	kIx.	.
</s>
<s>
Popsal	popsat	k5eAaPmAgMnS	popsat
jej	on	k3xPp3gNnSc4	on
dosti	dosti	k6eAd1	dosti
podrobně	podrobně	k6eAd1	podrobně
i	i	k9	i
Karel	Karel	k1gMnSc1	Karel
Čapek	Čapek	k1gMnSc1	Čapek
v	v	k7c6	v
úvaze	úvaha	k1gFnSc6	úvaha
"	"	kIx"	"
<g/>
Cesta	cesta	k1gFnSc1	cesta
devíti	devět	k4xCc2	devět
hodin	hodina	k1gFnPc2	hodina
<g/>
"	"	kIx"	"
a	a	k8xC	a
dny	den	k1gInPc4	den
smutku	smutek	k1gInSc2	smutek
před	před	k7c7	před
pohřbem	pohřeb	k1gInSc7	pohřeb
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
dobře	dobře	k6eAd1	dobře
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Seifert	Seifert	k1gMnSc1	Seifert
impozantní	impozantní	k2eAgMnPc1d1	impozantní
básní	báseň	k1gFnPc2	báseň
"	"	kIx"	"
<g/>
Osm	osm	k4xCc1	osm
dní	den	k1gInPc2	den
<g/>
"	"	kIx"	"
známou	známý	k2eAgFnSc7d1	známá
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
To	ten	k3xDgNnSc1	ten
kalné	kalný	k2eAgNnSc1d1	kalné
ráno	ráno	k1gNnSc1	ráno
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
si	se	k3xPyFc3	se
pamatuj	pamatovat	k5eAaImRp2nS	pamatovat
<g/>
,	,	kIx,	,
mé	můj	k3xOp1gNnSc1	můj
dítě	dítě	k1gNnSc1	dítě
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Pietní	pietní	k2eAgFnPc1d1	pietní
chvíle	chvíle	k1gFnPc1	chvíle
fotograficky	fotograficky	k6eAd1	fotograficky
velmi	velmi	k6eAd1	velmi
podrobně	podrobně	k6eAd1	podrobně
dokumentoval	dokumentovat	k5eAaBmAgMnS	dokumentovat
zejména	zejména	k9	zejména
Ladislav	Ladislav	k1gMnSc1	Ladislav
Sitenský	Sitenský	k2eAgMnSc1d1	Sitenský
<g/>
.	.	kIx.	.
</s>
<s>
Pohřební	pohřební	k2eAgFnSc4d1	pohřební
řeč	řeč	k1gFnSc4	řeč
vedl	vést	k5eAaImAgMnS	vést
přítel	přítel	k1gMnSc1	přítel
Masaryka	Masaryk	k1gMnSc2	Masaryk
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
rodiny	rodina	k1gFnSc2	rodina
kazatel	kazatel	k1gMnSc1	kazatel
Jednoty	jednota	k1gFnSc2	jednota
českobratrské	českobratrský	k2eAgFnSc2d1	Českobratrská
František	František	k1gMnSc1	František
Urbánek	Urbánek	k1gMnSc1	Urbánek
<g/>
.	.	kIx.	.
</s>
<s>
Masaryk	Masaryk	k1gMnSc1	Masaryk
o	o	k7c6	o
politice	politika	k1gFnSc6	politika
<g/>
:	:	kIx,	:
Veškerá	veškerý	k3xTgFnSc1	veškerý
politika	politika	k1gFnSc1	politika
<g/>
,	,	kIx,	,
zahraniční	zahraniční	k2eAgFnSc1d1	zahraniční
i	i	k8xC	i
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
mně	já	k3xPp1nSc3	já
prováděním	provádění	k1gNnSc7	provádění
a	a	k8xC	a
upevňováním	upevňování	k1gNnSc7	upevňování
humanity	humanita	k1gFnSc2	humanita
<g/>
;	;	kIx,	;
podřizuji	podřizovat	k5eAaImIp1nS	podřizovat
politiku	politika	k1gFnSc4	politika
přikázáním	přikázání	k1gNnPc3	přikázání
ethickým	ethický	k2eAgNnPc3d1	ethický
<g/>
.	.	kIx.	.
</s>
<s>
Vím	vědět	k5eAaImIp1nS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
stanovisko	stanovisko	k1gNnSc1	stanovisko
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
politikům	politik	k1gMnPc3	politik
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
pokládají	pokládat	k5eAaImIp3nP	pokládat
za	za	k7c4	za
velmi	velmi	k6eAd1	velmi
praktické	praktický	k2eAgNnSc4d1	praktické
a	a	k8xC	a
chytré	chytrý	k2eAgNnSc4d1	chytré
<g/>
,	,	kIx,	,
nelíbí	líbit	k5eNaImIp3nS	líbit
<g/>
;	;	kIx,	;
ale	ale	k8xC	ale
zkušenost	zkušenost	k1gFnSc1	zkušenost
<g/>
,	,	kIx,	,
a	a	k8xC	a
myslím	myslet	k5eAaImIp1nS	myslet
<g/>
,	,	kIx,	,
nejen	nejen	k6eAd1	nejen
moje	můj	k3xOp1gNnSc4	můj
<g/>
,	,	kIx,	,
dokazuje	dokazovat	k5eAaImIp3nS	dokazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
politika	politika	k1gFnSc1	politika
rozumná	rozumný	k2eAgFnSc1d1	rozumná
a	a	k8xC	a
poctivá	poctivý	k2eAgFnSc1d1	poctivá
(	(	kIx(	(
<g/>
Havlíček	Havlíček	k1gMnSc1	Havlíček
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
je	on	k3xPp3gInPc4	on
nejpraktičtější	praktický	k2eAgInPc4d3	nejpraktičtější
<g/>
,	,	kIx,	,
neúčinnější	účinný	k2eNgInPc4d2	neúčinnější
a	a	k8xC	a
nejvýnosnější	výnosný	k2eAgInPc4d3	nejvýnosnější
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
t.	t.	k?	t.
<g/>
zv	zv	k?	zv
<g/>
.	.	kIx.	.
idealisté	idealista	k1gMnPc1	idealista
<g/>
,	,	kIx,	,
lidé	člověk	k1gMnPc1	člověk
slušní	slušný	k2eAgMnPc1d1	slušný
a	a	k8xC	a
čestní	čestný	k2eAgMnPc1d1	čestný
vždycky	vždycky	k6eAd1	vždycky
mívají	mívat	k5eAaImIp3nP	mívat
pravdu	pravda	k1gFnSc4	pravda
a	a	k8xC	a
udělají	udělat	k5eAaPmIp3nP	udělat
pro	pro	k7c4	pro
stát	stát	k1gInSc4	stát
<g/>
,	,	kIx,	,
národ	národ	k1gInSc4	národ
a	a	k8xC	a
lidstvo	lidstvo	k1gNnSc4	lidstvo
více	hodně	k6eAd2	hodně
než	než	k8xS	než
politikové	politik	k1gMnPc1	politik
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
reální	reálnět	k5eAaImIp3nS	reálnět
a	a	k8xC	a
chytří	chytřet	k5eAaImIp3nS	chytřet
<g/>
.	.	kIx.	.
</s>
<s>
Chytráci	chytrák	k1gMnPc1	chytrák
jsou	být	k5eAaImIp3nP	být
konec	konec	k1gInSc4	konec
konců	konec	k1gInPc2	konec
hloupí	hloupit	k5eAaImIp3nS	hloupit
<g/>
.	.	kIx.	.
</s>
<s>
Pojímám	pojímat	k5eAaImIp1nS	pojímat
politiku	politika	k1gFnSc4	politika
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
jednotlivce	jednotlivec	k1gMnSc2	jednotlivec
a	a	k8xC	a
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
sub	sub	k7c4	sub
specie	specie	k1gFnSc4	specie
aeternitatis	aeternitatis	k1gFnSc2	aeternitatis
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
odpůrcích	odpůrce	k1gMnPc6	odpůrce
"	"	kIx"	"
<g/>
hradní	hradní	k2eAgFnSc2d1	hradní
politiky	politika	k1gFnSc2	politika
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
představitelé	představitel	k1gMnPc1	představitel
výrazně	výrazně	k6eAd1	výrazně
protimasarykovské	protimasarykovský	k2eAgFnSc2d1	protimasarykovská
pravice	pravice	k1gFnSc2	pravice
i	i	k8xC	i
levice	levice	k1gFnSc2	levice
nazývali	nazývat	k5eAaImAgMnP	nazývat
politický	politický	k2eAgInSc4d1	politický
směr	směr	k1gInSc4	směr
Masaryka	Masaryk	k1gMnSc2	Masaryk
a	a	k8xC	a
Beneše	Beneš	k1gMnSc2	Beneš
<g/>
,	,	kIx,	,
měli	mít	k5eAaImAgMnP	mít
tito	tento	k3xDgMnPc1	tento
dva	dva	k4xCgMnPc1	dva
politici	politik	k1gMnPc1	politik
tvrdé	tvrdý	k2eAgFnSc2d1	tvrdá
protivníky	protivník	k1gMnPc7	protivník
<g/>
.	.	kIx.	.
</s>
<s>
Zastánci	zastánce	k1gMnPc1	zastánce
"	"	kIx"	"
<g/>
hradní	hradní	k2eAgFnSc2d1	hradní
politiky	politika	k1gFnSc2	politika
<g/>
"	"	kIx"	"
ji	on	k3xPp3gFnSc4	on
ale	ale	k8xC	ale
označují	označovat	k5eAaImIp3nP	označovat
za	za	k7c4	za
liberální	liberální	k2eAgFnSc4d1	liberální
a	a	k8xC	a
reformní	reformní	k2eAgFnSc4d1	reformní
politiku	politika	k1gFnSc4	politika
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
ještě	ještě	k6eAd1	ještě
nedostatečně	dostatečně	k6eNd1	dostatečně
vyvinutou	vyvinutý	k2eAgFnSc4d1	vyvinutá
politickou	politický	k2eAgFnSc4d1	politická
kulturu	kultura	k1gFnSc4	kultura
(	(	kIx(	(
<g/>
politického	politický	k2eAgInSc2d1	politický
konsensu	konsens	k1gInSc2	konsens
<g/>
)	)	kIx)	)
a	a	k8xC	a
poměrně	poměrně	k6eAd1	poměrně
nízkou	nízký	k2eAgFnSc4d1	nízká
míru	míra	k1gFnSc4	míra
zodpovědnosti	zodpovědnost	k1gFnSc2	zodpovědnost
politiků	politik	k1gMnPc2	politik
se	se	k3xPyFc4	se
během	během	k7c2	během
dvou	dva	k4xCgNnPc2	dva
desetiletí	desetiletí	k1gNnPc2	desetiletí
existence	existence	k1gFnSc2	existence
předválečné	předválečný	k2eAgFnSc6d1	předválečná
ČSR	ČSR	kA	ČSR
nepodařilo	podařit	k5eNaPmAgNnS	podařit
vyřešit	vyřešit	k5eAaPmF	vyřešit
konflikty	konflikt	k1gInPc4	konflikt
mezi	mezi	k7c7	mezi
jejími	její	k3xOp3gFnPc7	její
zájmovými	zájmový	k2eAgFnPc7d1	zájmová
skupinami	skupina	k1gFnPc7	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Nejen	nejen	k6eAd1	nejen
mezi	mezi	k7c7	mezi
"	"	kIx"	"
<g/>
Hradem	hrad	k1gInSc7	hrad
<g/>
"	"	kIx"	"
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc7	jeho
protivníky	protivník	k1gMnPc7	protivník
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
dokládají	dokládat	k5eAaImIp3nP	dokládat
četné	četný	k2eAgFnSc2d1	četná
vládní	vládní	k2eAgFnSc2d1	vládní
a	a	k8xC	a
volební	volební	k2eAgFnSc2d1	volební
krize	krize	k1gFnSc2	krize
<g/>
.	.	kIx.	.
</s>
<s>
Zájmy	zájem	k1gInPc1	zájem
nových	nový	k2eAgFnPc2d1	nová
i	i	k8xC	i
starých	starý	k2eAgFnPc2d1	stará
zájmových	zájmový	k2eAgFnPc2d1	zájmová
skupin	skupina	k1gFnPc2	skupina
prorůstaly	prorůstat	k5eAaImAgInP	prorůstat
se	se	k3xPyFc4	se
zájmy	zájem	k1gInPc7	zájem
četných	četný	k2eAgMnPc2d1	četný
úředníků	úředník	k1gMnPc2	úředník
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
a	a	k8xC	a
policie	policie	k1gFnSc2	policie
převzatých	převzatý	k2eAgInPc2d1	převzatý
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
monarchie	monarchie	k1gFnSc2	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
starých	starý	k2eAgFnPc2d1	stará
dob	doba	k1gFnPc2	doba
narostlá	narostlý	k2eAgFnSc1d1	narostlá
administrativa	administrativa	k1gFnSc1	administrativa
<g/>
,	,	kIx,	,
sloužící	sloužící	k2eAgMnSc1d1	sloužící
ze	z	k7c2	z
značné	značný	k2eAgFnSc2d1	značná
části	část	k1gFnSc2	část
"	"	kIx"	"
<g/>
místodržení	místodržení	k1gNnSc2	místodržení
<g/>
"	"	kIx"	"
a	a	k8xC	a
lokálním	lokální	k2eAgInPc3d1	lokální
mocenským	mocenský	k2eAgInPc3d1	mocenský
zájmům	zájem	k1gInPc3	zájem
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
teprve	teprve	k6eAd1	teprve
pozvolna	pozvolna	k6eAd1	pozvolna
začala	začít	k5eAaPmAgFnS	začít
přeměňovat	přeměňovat	k5eAaImF	přeměňovat
<g/>
.	.	kIx.	.
</s>
<s>
Masarykovi	Masaryk	k1gMnSc3	Masaryk
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc3	jeho
spojencům	spojenec	k1gMnPc3	spojenec
v	v	k7c6	v
československé	československý	k2eAgFnSc6d1	Československá
politice	politika	k1gFnSc6	politika
se	se	k3xPyFc4	se
nepodařilo	podařit	k5eNaPmAgNnS	podařit
prosadit	prosadit	k5eAaPmF	prosadit
rozluku	rozluka	k1gFnSc4	rozluka
církve	církev	k1gFnSc2	církev
a	a	k8xC	a
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
odpor	odpor	k1gInSc4	odpor
konzervativních	konzervativní	k2eAgInPc2d1	konzervativní
katolických	katolický	k2eAgInPc2d1	katolický
kruhů	kruh	k1gInPc2	kruh
proti	proti	k7c3	proti
oslabení	oslabení	k1gNnSc3	oslabení
jejich	jejich	k3xOp3gInSc2	jejich
vlivu	vliv	k1gInSc2	vliv
byl	být	k5eAaImAgInS	být
příliš	příliš	k6eAd1	příliš
silný	silný	k2eAgInSc1d1	silný
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
výtku	výtka	k1gFnSc4	výtka
spisovatele	spisovatel	k1gMnSc2	spisovatel
Emila	Emil	k1gMnSc2	Emil
Ludwiga	Ludwig	k1gMnSc2	Ludwig
<g/>
,	,	kIx,	,
že	že	k8xS	že
Masaryk	Masaryk	k1gMnSc1	Masaryk
jako	jako	k8xS	jako
prezident	prezident	k1gMnSc1	prezident
málo	málo	k6eAd1	málo
"	"	kIx"	"
<g/>
socializuje	socializovat	k5eAaBmIp3nS	socializovat
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
T.	T.	kA	T.
G.	G.	kA	G.
Masaryk	Masaryk	k1gMnSc1	Masaryk
reagoval	reagovat	k5eAaBmAgMnS	reagovat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Stát	stát	k1gInSc1	stát
nemá	mít	k5eNaImIp3nS	mít
tolik	tolik	k4xDc4	tolik
peněz	peníze	k1gInPc2	peníze
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgMnS	moct
firmy	firma	k1gFnPc4	firma
od	od	k7c2	od
jednotlivců	jednotlivec	k1gMnPc2	jednotlivec
vykupovat	vykupovat	k5eAaImF	vykupovat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
E.	E.	kA	E.
Ludwig	Ludwig	k1gMnSc1	Ludwig
<g/>
:	:	kIx,	:
Duch	duch	k1gMnSc1	duch
a	a	k8xC	a
čin	čin	k1gInSc1	čin
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Nepodařilo	podařit	k5eNaPmAgNnS	podařit
se	se	k3xPyFc4	se
vyřešit	vyřešit	k5eAaPmF	vyřešit
národnostní	národnostní	k2eAgInPc4d1	národnostní
problémy	problém	k1gInPc4	problém
ČSR	ČSR	kA	ČSR
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
federativního	federativní	k2eAgNnSc2d1	federativní
uspořádání	uspořádání	k1gNnSc2	uspořádání
podle	podle	k7c2	podle
švýcarského	švýcarský	k2eAgInSc2d1	švýcarský
vzoru	vzor	k1gInSc2	vzor
<g/>
.	.	kIx.	.
</s>
<s>
Prosadila	prosadit	k5eAaPmAgFnS	prosadit
se	se	k3xPyFc4	se
centralistická	centralistický	k2eAgFnSc1d1	centralistická
koncepce	koncepce	k1gFnSc1	koncepce
podle	podle	k7c2	podle
francouzského	francouzský	k2eAgInSc2d1	francouzský
vzoru	vzor	k1gInSc2	vzor
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
částečné	částečný	k2eAgFnSc3d1	částečná
diskriminaci	diskriminace	k1gFnSc3	diskriminace
národnostních	národnostní	k2eAgFnPc2d1	národnostní
menšin	menšina	k1gFnPc2	menšina
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
(	(	kIx(	(
<g/>
německé	německý	k2eAgFnSc2d1	německá
<g/>
,	,	kIx,	,
maďarské	maďarský	k2eAgFnSc2d1	maďarská
a	a	k8xC	a
polské	polský	k2eAgFnSc2d1	polská
<g/>
)	)	kIx)	)
a	a	k8xC	a
k	k	k7c3	k
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
čechoslovakismu	čechoslovakismus	k1gInSc2	čechoslovakismus
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
byla	být	k5eAaImAgFnS	být
oslabena	oslabit	k5eAaPmNgFnS	oslabit
pozice	pozice	k1gFnSc1	pozice
předválečné	předválečný	k2eAgFnSc2d1	předválečná
ČSR	ČSR	kA	ČSR
vůči	vůči	k7c3	vůči
rostoucímu	rostoucí	k2eAgInSc3d1	rostoucí
vlivu	vliv	k1gInSc3	vliv
nacistického	nacistický	k2eAgNnSc2d1	nacistické
Německa	Německo	k1gNnSc2	Německo
i	i	k8xC	i
dalších	další	k2eAgMnPc2d1	další
expanzivních	expanzivní	k2eAgMnPc2d1	expanzivní
sousedů	soused	k1gMnPc2	soused
(	(	kIx(	(
<g/>
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
)	)	kIx)	)
na	na	k7c6	na
československém	československý	k2eAgNnSc6d1	Československé
území	území	k1gNnSc6	území
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
dobré	dobrý	k2eAgFnPc4d1	dobrá
podmínky	podmínka	k1gFnPc4	podmínka
měli	mít	k5eAaImAgMnP	mít
naopak	naopak	k6eAd1	naopak
ruští	ruský	k2eAgMnPc1d1	ruský
imigranti	imigrant	k1gMnPc1	imigrant
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
většinou	většinou	k6eAd1	většinou
patřili	patřit	k5eAaImAgMnP	patřit
ke	k	k7c3	k
špičkové	špičkový	k2eAgFnSc3d1	špičková
inteligenci	inteligence	k1gFnSc3	inteligence
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jeho	jeho	k3xOp3gInPc3	jeho
osmdesátým	osmdesátý	k4xOgInPc3	osmdesátý
narozeninám	narozeniny	k1gFnPc3	narozeniny
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1930	[number]	k4	1930
přijat	přijat	k2eAgInSc1d1	přijat
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
zásluhách	zásluha	k1gFnPc6	zásluha
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
obsahující	obsahující	k2eAgFnSc4d1	obsahující
větu	věta	k1gFnSc4	věta
"	"	kIx"	"
<g/>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Garrigue	Garrigu	k1gMnSc2	Garrigu
Masaryk	Masaryk	k1gMnSc1	Masaryk
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
se	se	k3xPyFc4	se
o	o	k7c4	o
stát	stát	k1gInSc4	stát
<g/>
"	"	kIx"	"
a	a	k8xC	a
po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
z	z	k7c2	z
funkce	funkce	k1gFnSc2	funkce
roku	rok	k1gInSc2	rok
1935	[number]	k4	1935
ho	on	k3xPp3gInSc4	on
parlament	parlament	k1gInSc1	parlament
znovu	znovu	k6eAd1	znovu
ocenil	ocenit	k5eAaPmAgInS	ocenit
a	a	k8xC	a
odměnil	odměnit	k5eAaPmAgInS	odměnit
za	za	k7c4	za
jeho	jeho	k3xOp3gNnSc4	jeho
osvoboditelské	osvoboditelský	k2eAgNnSc4d1	osvoboditelské
a	a	k8xC	a
budovatelské	budovatelský	k2eAgNnSc4d1	budovatelské
dílo	dílo	k1gNnSc4	dílo
zákonem	zákon	k1gInSc7	zákon
č.	č.	k?	č.
232	[number]	k4	232
<g/>
/	/	kIx~	/
<g/>
1935	[number]	k4	1935
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
z	z	k7c2	z
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1935	[number]	k4	1935
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
sedmnáctkrát	sedmnáctkrát	k6eAd1	sedmnáctkrát
byl	být	k5eAaImAgMnS	být
Masaryk	Masaryk	k1gMnSc1	Masaryk
navržen	navrhnout	k5eAaPmNgMnS	navrhnout
na	na	k7c4	na
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
míru	mír	k1gInSc2	mír
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Masarykovi	Masaryk	k1gMnSc6	Masaryk
je	být	k5eAaImIp3nS	být
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
řád	řád	k1gInSc1	řád
Tomáše	Tomáš	k1gMnSc2	Tomáš
Garrigua	Garriguus	k1gMnSc2	Garriguus
Masaryka	Masaryk	k1gMnSc2	Masaryk
<g/>
,	,	kIx,	,
jedno	jeden	k4xCgNnSc1	jeden
ze	z	k7c2	z
státních	státní	k2eAgNnPc2d1	státní
vyznamenání	vyznamenání	k1gNnPc2	vyznamenání
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
bývalého	bývalý	k2eAgNnSc2d1	bývalé
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Zasadil	zasadit	k5eAaPmAgInS	zasadit
se	se	k3xPyFc4	se
o	o	k7c6	o
založení	založení	k1gNnSc6	založení
brněnské	brněnský	k2eAgFnSc2d1	brněnská
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
vydala	vydat	k5eAaPmAgFnS	vydat
americká	americký	k2eAgFnSc1d1	americká
skupina	skupina	k1gFnSc1	skupina
Faith	Faitha	k1gFnPc2	Faitha
No	no	k9	no
More	mor	k1gInSc5	mor
album	album	k1gNnSc4	album
Album	album	k1gNnSc1	album
of	of	k?	of
the	the	k?	the
Year	Year	k1gInSc1	Year
s	s	k7c7	s
fotografií	fotografia	k1gFnSc7	fotografia
Masaryka	Masaryk	k1gMnSc2	Masaryk
na	na	k7c6	na
obalu	obal	k1gInSc6	obal
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Tomáše	Tomáš	k1gMnSc2	Tomáš
Garrigua	Garriguus	k1gMnSc2	Garriguus
Masaryka	Masaryk	k1gMnSc2	Masaryk
nese	nést	k5eAaImIp3nS	nést
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
ulic	ulice	k1gFnPc2	ulice
a	a	k8xC	a
náměstí	náměstí	k1gNnSc1	náměstí
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
i	i	k8xC	i
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Gottwald	Gottwald	k1gMnSc1	Gottwald
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
hrál	hrát	k5eAaImAgMnS	hrát
Masaryka	Masaryk	k1gMnSc2	Masaryk
Gustav	Gustav	k1gMnSc1	Gustav
Opočenský	opočenský	k2eAgMnSc1d1	opočenský
<g/>
,	,	kIx,	,
ve	v	k7c6	v
filmu	film	k1gInSc6	film
o	o	k7c4	o
Karlu	Karel	k1gMnSc3	Karel
Čapkovi	Čapek	k1gMnSc3	Čapek
Člověk	člověk	k1gMnSc1	člověk
proti	proti	k7c3	proti
zkáze	zkáza	k1gFnSc3	zkáza
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Beneš	Beneš	k1gMnSc1	Beneš
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
České	český	k2eAgNnSc1d1	české
století	století	k1gNnSc1	století
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
jej	on	k3xPp3gNnSc4	on
ztvárnil	ztvárnit	k5eAaPmAgMnS	ztvárnit
Martin	Martin	k1gMnSc1	Martin
Huba	huba	k1gFnSc1	huba
<g/>
,	,	kIx,	,
v	v	k7c6	v
televizním	televizní	k2eAgInSc6d1	televizní
filmu	film	k1gInSc6	film
Zločin	zločin	k1gInSc1	zločin
v	v	k7c6	v
Polné	Polná	k1gFnSc6	Polná
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
Karel	Karel	k1gMnSc1	Karel
Roden	Roden	k2eAgMnSc1d1	Roden
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
stručný	stručný	k2eAgInSc4d1	stručný
přehled	přehled	k1gInSc4	přehled
<g/>
)	)	kIx)	)
1872	[number]	k4	1872
–	–	k?	–
maturita	maturita	k1gFnSc1	maturita
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
,	,	kIx,	,
zápis	zápis	k1gInSc1	zápis
na	na	k7c4	na
fakultu	fakulta	k1gFnSc4	fakulta
filosofickou	filosofický	k2eAgFnSc4d1	filosofická
Vídeňské	vídeňský	k2eAgFnSc2d1	Vídeňská
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
obor	obor	k1gInSc1	obor
filologie	filologie	k1gFnSc1	filologie
1876	[number]	k4	1876
–	–	k?	–
doktorát	doktorát	k1gInSc4	doktorát
vídeňské	vídeňský	k2eAgFnSc2d1	Vídeňská
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
filosofie	filosofie	k1gFnSc2	filosofie
(	(	kIx(	(
<g/>
stručný	stručný	k2eAgInSc4d1	stručný
přehled	přehled	k1gInSc4	přehled
<g/>
)	)	kIx)	)
1876	[number]	k4	1876
<g/>
–	–	k?	–
<g/>
1877	[number]	k4	1877
–	–	k?	–
roční	roční	k2eAgInSc4d1	roční
pobyt	pobyt	k1gInSc4	pobyt
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Lipsku	Lipsko	k1gNnSc6	Lipsko
1879	[number]	k4	1879
–	–	k?	–
habilitace	habilitace	k1gFnSc2	habilitace
<g/>
,	,	kIx,	,
soukromý	soukromý	k2eAgMnSc1d1	soukromý
docent	docent	k1gMnSc1	docent
na	na	k7c6	na
Vídeňské	vídeňský	k2eAgFnSc6d1	Vídeňská
univerzitě	univerzita	k1gFnSc6	univerzita
1882	[number]	k4	1882
–	–	k?	–
mimořádný	mimořádný	k2eAgMnSc1d1	mimořádný
profesor	profesor	k1gMnSc1	profesor
filozofie	filozofie	k1gFnSc2	filozofie
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g />
.	.	kIx.	.
</s>
<s>
1896	[number]	k4	1896
–	–	k?	–
řádný	řádný	k2eAgMnSc1d1	řádný
profesor	profesor	k1gMnSc1	profesor
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
univerzity	univerzita	k1gFnSc2	univerzita
(	(	kIx(	(
<g/>
ordinář	ordinář	k1gMnSc1	ordinář
<g/>
)	)	kIx)	)
1850	[number]	k4	1850
–	–	k?	–
narozen	narozen	k2eAgMnSc1d1	narozen
v	v	k7c6	v
Hodoníně	Hodonín	k1gInSc6	Hodonín
1869	[number]	k4	1869
–	–	k?	–
odchod	odchod	k1gInSc1	odchod
z	z	k7c2	z
Brna	Brno	k1gNnSc2	Brno
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
1877	[number]	k4	1877
–	–	k?	–
setkání	setkání	k1gNnSc1	setkání
s	s	k7c7	s
Charlottou	Charlotta	k1gFnSc7	Charlotta
Garrigue	Garrigu	k1gFnSc2	Garrigu
v	v	k7c6	v
Lipsku	Lipsko	k1gNnSc6	Lipsko
<g/>
,	,	kIx,	,
zasnoubení	zasnoubení	k1gNnSc1	zasnoubení
1878	[number]	k4	1878
–	–	k?	–
sňatek	sňatek	k1gInSc1	sňatek
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
1879	[number]	k4	1879
–	–	k?	–
narození	narození	k1gNnSc2	narození
dcery	dcera	k1gFnSc2	dcera
Alice	Alice	k1gFnSc1	Alice
1880	[number]	k4	1880
–	–	k?	–
narození	narození	k1gNnSc2	narození
syna	syn	k1gMnSc2	syn
Herberta	Herbert	k1gMnSc2	Herbert
<g />
.	.	kIx.	.
</s>
<s>
1882	[number]	k4	1882
–	–	k?	–
přistěhování	přistěhování	k1gNnSc4	přistěhování
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
1886	[number]	k4	1886
–	–	k?	–
narození	narození	k1gNnSc2	narození
syna	syn	k1gMnSc4	syn
Jana	Jan	k1gMnSc4	Jan
1890	[number]	k4	1890
–	–	k?	–
narození	narození	k1gNnSc2	narození
dcery	dcera	k1gFnSc2	dcera
Eleanor	Eleanor	k1gMnSc1	Eleanor
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
<g/>
)	)	kIx)	)
a	a	k8xC	a
její	její	k3xOp3gNnSc4	její
úmrtí	úmrtí	k1gNnSc4	úmrtí
18	[number]	k4	18
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1891	[number]	k4	1891
–	–	k?	–
narození	narození	k1gNnSc2	narození
dcery	dcera	k1gFnSc2	dcera
Olgy	Olga	k1gFnSc2	Olga
1914	[number]	k4	1914
–	–	k?	–
v	v	k7c6	v
exilu	exil	k1gInSc6	exil
s	s	k7c7	s
dcerou	dcera	k1gFnSc7	dcera
Olgou	Olga	k1gFnSc7	Olga
(	(	kIx(	(
<g/>
manželka	manželka	k1gFnSc1	manželka
a	a	k8xC	a
tři	tři	k4xCgFnPc4	tři
děti	dítě	k1gFnPc4	dítě
<g />
.	.	kIx.	.
</s>
<s>
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
)	)	kIx)	)
1915	[number]	k4	1915
–	–	k?	–
úmrtí	úmrtí	k1gNnSc2	úmrtí
syna	syn	k1gMnSc4	syn
Herberta	Herbert	k1gMnSc4	Herbert
1918	[number]	k4	1918
–	–	k?	–
návrat	návrat	k1gInSc1	návrat
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
z	z	k7c2	z
exilu	exil	k1gInSc2	exil
1923	[number]	k4	1923
–	–	k?	–
úmrtí	úmrtí	k1gNnSc2	úmrtí
manželky	manželka	k1gFnSc2	manželka
Charlotty	Charlotta	k1gFnSc2	Charlotta
1937	[number]	k4	1937
–	–	k?	–
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
v	v	k7c6	v
Lánech	lán	k1gInPc6	lán
(	(	kIx(	(
<g/>
v	v	k7c6	v
letech	let	k1gInPc6	let
1914	[number]	k4	1914
<g/>
–	–	k?	–
<g/>
1918	[number]	k4	1918
v	v	k7c6	v
kontextu	kontext	k1gInSc6	kontext
souvisejících	související	k2eAgFnPc2d1	související
událostí	událost	k1gFnPc2	událost
<g/>
)	)	kIx)	)
1875	[number]	k4	1875
–	–	k?	–
předseda	předseda	k1gMnSc1	předseda
Českého	český	k2eAgInSc2d1	český
akademického	akademický	k2eAgInSc2d1	akademický
spolku	spolek	k1gInSc2	spolek
<g />
.	.	kIx.	.
</s>
<s>
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
1877	[number]	k4	1877
–	–	k?	–
prosadil	prosadit	k5eAaPmAgMnS	prosadit
vědecké	vědecký	k2eAgNnSc4d1	vědecké
prozkoumání	prozkoumání	k1gNnSc4	prozkoumání
zfalšovaných	zfalšovaný	k2eAgInPc2d1	zfalšovaný
rukopisů	rukopis	k1gInPc2	rukopis
1882	[number]	k4	1882
–	–	k?	–
po	po	k7c6	po
přistěhování	přistěhování	k1gNnSc6	přistěhování
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
politická	politický	k2eAgFnSc1d1	politická
činnost	činnost	k1gFnSc1	činnost
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
i	i	k8xC	i
nový	nový	k2eAgInSc4d1	nový
politický	politický	k2eAgInSc4d1	politický
směr	směr	k1gInSc4	směr
realismus	realismus	k1gInSc1	realismus
1887	[number]	k4	1887
–	–	k?	–
první	první	k4xOgFnSc1	první
cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
Ruska	Rusko	k1gNnSc2	Rusko
1888	[number]	k4	1888
–	–	k?	–
druhá	druhý	k4xOgFnSc1	druhý
cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
Ruska	Rusko	k1gNnSc2	Rusko
1890	[number]	k4	1890
–	–	k?	–
jednání	jednání	k1gNnSc1	jednání
se	s	k7c7	s
staročechy	staročech	k1gMnPc7	staročech
a	a	k8xC	a
po	po	k7c6	po
neúspěchu	neúspěch	k1gInSc6	neúspěch
u	u	k7c2	u
nich	on	k3xPp3gNnPc2	on
s	s	k7c7	s
mladočechy	mladočech	k1gMnPc7	mladočech
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
přijat	přijat	k2eAgInSc1d1	přijat
k	k	k7c3	k
mladočechům	mladočech	k1gMnPc3	mladočech
1891	[number]	k4	1891
<g/>
–	–	k?	–
<g/>
93	[number]	k4	93
–	–	k?	–
poslanec	poslanec	k1gMnSc1	poslanec
Říšské	říšský	k2eAgFnSc2d1	říšská
rady	rada	k1gFnSc2	rada
<g/>
,	,	kIx,	,
mandát	mandát	k1gInSc4	mandát
za	za	k7c4	za
mladočechy	mladočech	k1gMnPc4	mladočech
1891	[number]	k4	1891
<g/>
–	–	k?	–
<g/>
93	[number]	k4	93
–	–	k?	–
Český	český	k2eAgInSc1d1	český
zemský	zemský	k2eAgInSc1d1	zemský
sněm	sněm	k1gInSc1	sněm
<g/>
,	,	kIx,	,
mandát	mandát	k1gInSc1	mandát
za	za	k7c7	za
mladočechy	mladočech	k1gMnPc7	mladočech
1893	[number]	k4	1893
–	–	k?	–
vzdal	vzdát	k5eAaPmAgMnS	vzdát
se	se	k3xPyFc4	se
obou	dva	k4xCgNnPc2	dva
mandátů	mandát	k1gInPc2	mandát
1899	[number]	k4	1899
–	–	k?	–
polenská	polenský	k2eAgFnSc1d1	polenská
aféra	aféra	k1gFnSc1	aféra
<g/>
,	,	kIx,	,
Hilsneriáda	hilsneriáda	k1gFnSc1	hilsneriáda
<g/>
,	,	kIx,	,
po	po	k7c6	po
odsouzení	odsouzení	k1gNnSc6	odsouzení
Leopolda	Leopold	k1gMnSc2	Leopold
<g />
.	.	kIx.	.
</s>
<s>
Hilsnera	Hilsner	k1gMnSc4	Hilsner
za	za	k7c7	za
údajnou	údajný	k2eAgFnSc7d1	údajná
židovskou	židovská	k1gFnSc7	židovská
rituální	rituální	k2eAgFnSc4d1	rituální
vraždu	vražda	k1gFnSc4	vražda
české	český	k2eAgFnSc2d1	Česká
dívky	dívka	k1gFnSc2	dívka
prosadil	prosadit	k5eAaPmAgMnS	prosadit
revizi	revize	k1gFnSc4	revize
procesu	proces	k1gInSc2	proces
1900	[number]	k4	1900
–	–	k?	–
založení	založení	k1gNnSc2	založení
české	český	k2eAgFnSc2d1	Česká
strany	strana	k1gFnSc2	strana
lidové	lidový	k2eAgFnSc2d1	lidová
(	(	kIx(	(
<g/>
od	od	k7c2	od
1905	[number]	k4	1905
Česká	český	k2eAgFnSc1d1	Česká
strana	strana	k1gFnSc1	strana
pokroková	pokrokový	k2eAgFnSc1d1	pokroková
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
realisté	realista	k1gMnPc1	realista
<g/>
"	"	kIx"	"
1902	[number]	k4	1902
–	–	k?	–
na	na	k7c4	na
pozvání	pozvání	k1gNnSc4	pozvání
Chicagské	chicagský	k2eAgFnSc2d1	Chicagská
univerzity	univerzita	k1gFnSc2	univerzita
přednášky	přednáška	k1gFnSc2	přednáška
a	a	k8xC	a
veřejná	veřejný	k2eAgNnPc4d1	veřejné
vystoupení	vystoupení	k1gNnPc4	vystoupení
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
s	s	k7c7	s
<g />
.	.	kIx.	.
</s>
<s>
americkými	americký	k2eAgMnPc7d1	americký
Čechy	Čech	k1gMnPc7	Čech
a	a	k8xC	a
Slováky	Slovák	k1gMnPc7	Slovák
(	(	kIx(	(
<g/>
po	po	k7c6	po
sňatku	sňatek	k1gInSc6	sňatek
druhá	druhý	k4xOgFnSc1	druhý
cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
USA	USA	kA	USA
<g/>
)	)	kIx)	)
1906	[number]	k4	1906
–	–	k?	–
soudní	soudní	k2eAgInSc4d1	soudní
spor	spor	k1gInSc4	spor
<g/>
,	,	kIx,	,
hájen	hájen	k2eAgInSc4d1	hájen
Václavem	Václav	k1gMnSc7	Václav
Boučkem	Bouček	k1gMnSc7	Bouček
proti	proti	k7c3	proti
žalobě	žaloba	k1gFnSc3	žaloba
306	[number]	k4	306
katechetů	katecheta	k1gMnPc2	katecheta
pro	pro	k7c4	pro
urážku	urážka	k1gFnSc4	urážka
na	na	k7c6	na
cti	čest	k1gFnSc6	čest
1907	[number]	k4	1907
<g/>
–	–	k?	–
<g/>
14	[number]	k4	14
–	–	k?	–
podruhé	podruhé	k6eAd1	podruhé
poslancem	poslanec	k1gMnSc7	poslanec
Říšské	říšský	k2eAgFnSc2d1	říšská
rady	rada	k1gFnSc2	rada
s	s	k7c7	s
mandátem	mandát	k1gInSc7	mandát
realistů	realista	k1gMnPc2	realista
1907	[number]	k4	1907
–	–	k?	–
třetí	třetí	k4xOgFnSc1	třetí
cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
USA	USA	kA	USA
<g />
.	.	kIx.	.
</s>
<s>
účast	účast	k1gFnSc1	účast
na	na	k7c6	na
sjezdu	sjezd	k1gInSc6	sjezd
pro	pro	k7c4	pro
svobodu	svoboda	k1gFnSc4	svoboda
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
,	,	kIx,	,
další	další	k2eAgNnPc4d1	další
veřejná	veřejný	k2eAgNnPc4d1	veřejné
vystoupení	vystoupení	k1gNnPc4	vystoupení
<g/>
,	,	kIx,	,
přednášky	přednáška	k1gFnPc4	přednáška
a	a	k8xC	a
setkání	setkání	k1gNnSc4	setkání
s	s	k7c7	s
osobnostmi	osobnost	k1gFnPc7	osobnost
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
s	s	k7c7	s
americkými	americký	k2eAgMnPc7d1	americký
Čechy	Čech	k1gMnPc7	Čech
a	a	k8xC	a
Slováky	Slováky	k1gInPc7	Slováky
1909	[number]	k4	1909
–	–	k?	–
protest	protest	k1gInSc4	protest
proti	proti	k7c3	proti
zinscenovanému	zinscenovaný	k2eAgInSc3d1	zinscenovaný
Záhřebskému	záhřebský	k2eAgInSc3d1	záhřebský
procesu	proces	k1gInSc3	proces
<g/>
,	,	kIx,	,
úspěšné	úspěšný	k2eAgNnSc1d1	úspěšné
odhalení	odhalení	k1gNnSc1	odhalení
komplotu	komplot	k1gInSc2	komplot
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
zahraničí	zahraničí	k1gNnSc2	zahraničí
v	v	k7c6	v
Říšském	říšský	k2eAgInSc6d1	říšský
sněmu	sněm	k1gInSc6	sněm
1910	[number]	k4	1910
–	–	k?	–
třetí	třetí	k4xOgFnSc1	třetí
cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
Ruska	Rusko	k1gNnSc2	Rusko
1914	[number]	k4	1914
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
v	v	k7c6	v
září	září	k1gNnSc6	září
a	a	k8xC	a
říjnu	říjen	k1gInSc6	říjen
cesty	cesta	k1gFnSc2	cesta
ve	v	k7c6	v
věci	věc	k1gFnSc6	věc
české	český	k2eAgFnSc2d1	Česká
samostatnosti	samostatnost	k1gFnSc2	samostatnost
do	do	k7c2	do
Holandska	Holandsko	k1gNnSc2	Holandsko
(	(	kIx(	(
<g/>
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
propukla	propuknout	k5eAaPmAgFnS	propuknout
první	první	k4xOgFnSc1	první
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
a	a	k8xC	a
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
už	už	k6eAd1	už
zůstal	zůstat	k5eAaPmAgMnS	zůstat
(	(	kIx(	(
<g/>
s	s	k7c7	s
dcerou	dcera	k1gFnSc7	dcera
Olgou	Olga	k1gFnSc7	Olga
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
psal	psát	k5eAaImAgInS	psát
velení	velení	k1gNnSc4	velení
ruské	ruský	k2eAgFnSc2d1	ruská
carské	carský	k2eAgFnSc2d1	carská
armády	armáda	k1gFnSc2	armáda
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
nestříleli	střílet	k5eNaImAgMnP	střílet
po	po	k7c6	po
československých	československý	k2eAgMnPc6d1	československý
přeběhlících	přeběhlík	k1gMnPc6	přeběhlík
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
odhodí	odhodit	k5eAaPmIp3nP	odhodit
zbraň	zbraň	k1gFnSc4	zbraň
<g/>
,	,	kIx,	,
budou	být	k5eAaImBp3nP	být
mít	mít	k5eAaImF	mít
bílý	bílý	k2eAgInSc4d1	bílý
šátek	šátek	k1gInSc4	šátek
kolem	kolem	k7c2	kolem
krku	krk	k1gInSc2	krk
a	a	k8xC	a
začnou	začít	k5eAaPmIp3nP	začít
zpívat	zpívat	k5eAaImF	zpívat
"	"	kIx"	"
<g/>
Hej	hej	k6eAd1	hej
Slované	Slovan	k1gMnPc1	Slovan
<g/>
..	..	k?	..
<g/>
"	"	kIx"	"
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
1914	[number]	k4	1914
<g/>
–	–	k?	–
<g/>
15	[number]	k4	15
ze	z	k7c2	z
Ženevy	Ženeva	k1gFnSc2	Ženeva
společně	společně	k6eAd1	společně
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
organizace	organizace	k1gFnSc2	organizace
exilového	exilový	k2eAgNnSc2d1	exilové
hnutí	hnutí	k1gNnSc2	hnutí
proti	proti	k7c3	proti
Rakousku-Uhersku	Rakousku-Uhersek	k1gInSc3	Rakousku-Uhersek
<g/>
,	,	kIx,	,
kontakty	kontakt	k1gInPc4	kontakt
k	k	k7c3	k
<g />
.	.	kIx.	.
</s>
<s>
exilu	exil	k1gInSc2	exil
a	a	k8xC	a
krajanům	krajan	k1gMnPc3	krajan
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
USA	USA	kA	USA
1915	[number]	k4	1915
1915	[number]	k4	1915
–	–	k?	–
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
první	první	k4xOgFnSc1	první
tajná	tajný	k2eAgFnSc1d1	tajná
schůze	schůze	k1gFnSc1	schůze
pozdější	pozdní	k2eAgFnSc2d2	pozdější
Maffie	Maffie	k1gFnSc2	Maffie
4	[number]	k4	4
<g/>
.	.	kIx.	.
a	a	k8xC	a
6	[number]	k4	6
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
–	–	k?	–
proslovy	proslov	k1gInPc4	proslov
v	v	k7c6	v
Curychu	Curych	k1gInSc6	Curych
a	a	k8xC	a
v	v	k7c6	v
Ženevě	Ženeva	k1gFnSc6	Ženeva
<g/>
,	,	kIx,	,
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
požadavku	požadavek	k1gInSc2	požadavek
české	český	k2eAgFnSc2d1	Česká
samostatnosti	samostatnost	k1gFnSc2	samostatnost
mimo	mimo	k7c4	mimo
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc4	Rakousko-Uhersko
(	(	kIx(	(
<g/>
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
rovnalo	rovnat	k5eAaImAgNnS	rovnat
velezradě	velezrada	k1gFnSc3	velezrada
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
3	[number]	k4	3
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
–	–	k?	–
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
veřejné	veřejný	k2eAgNnSc4d1	veřejné
prohlášení	prohlášení	k1gNnSc4	prohlášení
Maffie	Maffie	k1gFnSc1	Maffie
k	k	k7c3	k
zastupování	zastupování	k1gNnSc3	zastupování
českého	český	k2eAgInSc2d1	český
národa	národ	k1gInSc2	národ
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
,	,	kIx,	,
rozdělení	rozdělení	k1gNnSc6	rozdělení
úloh	úloha	k1gFnPc2	úloha
(	(	kIx(	(
<g/>
Masaryk	Masaryk	k1gMnSc1	Masaryk
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
na	na	k7c4	na
universitu	universita	k1gFnSc4	universita
<g/>
,	,	kIx,	,
Beneš	Beneš	k1gMnSc1	Beneš
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
Sychrava	Sychrava	k1gFnSc1	Sychrava
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
pro	pro	k7c4	pro
tajné	tajný	k2eAgNnSc4d1	tajné
spojení	spojení	k1gNnSc4	spojení
s	s	k7c7	s
Prahou	Praha	k1gFnSc7	Praha
<g/>
)	)	kIx)	)
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
1915	[number]	k4	1915
<g />
.	.	kIx.	.
</s>
<s>
září	září	k1gNnSc4	září
–	–	k?	–
odjezd	odjezd	k1gInSc1	odjezd
z	z	k7c2	z
Ženevy	Ženeva	k1gFnSc2	Ženeva
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
Paříž	Paříž	k1gFnSc4	Paříž
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
říjen	říjen	k1gInSc1	říjen
–	–	k?	–
nástupní	nástupní	k2eAgFnSc1d1	nástupní
přednáška	přednáška	k1gFnSc1	přednáška
na	na	k7c6	na
Londýnské	londýnský	k2eAgFnSc6d1	londýnská
univerzitě	univerzita	k1gFnSc6	univerzita
(	(	kIx(	(
<g/>
profesura	profesura	k1gFnSc1	profesura
slavistiky	slavistika	k1gFnSc2	slavistika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
o	o	k7c6	o
samostatnosti	samostatnost	k1gFnSc6	samostatnost
národů	národ	k1gInPc2	národ
a	a	k8xC	a
rozpuštění	rozpuštění	k1gNnSc1	rozpuštění
Rakouska-Uherska	Rakouska-Uhersk	k1gInSc2	Rakouska-Uhersk
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
založeno	založit	k5eAaPmNgNnS	založit
Comité	Comitý	k2eAgNnSc1d1	Comité
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
action	action	k1gInSc1	action
tchè	tchè	k?	tchè
à	à	k?	à
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
étranger	étrangero	k1gNnPc2	étrangero
exilovými	exilový	k2eAgInPc7d1	exilový
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
krajanskými	krajanský	k2eAgFnPc7d1	krajanská
organizacemi	organizace	k1gFnPc7	organizace
Čechů	Čech	k1gMnPc2	Čech
a	a	k8xC	a
Slováků	Slováky	k1gInPc2	Slováky
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
vyhlásilo	vyhlásit	k5eAaPmAgNnS	vyhlásit
zřízení	zřízení	k1gNnSc1	zřízení
nezávislého	závislý	k2eNgInSc2d1	nezávislý
česko-slovenského	českolovenský	k2eAgInSc2d1	česko-slovenský
státu	stát	k1gInSc2	stát
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
cíl	cíl	k1gInSc4	cíl
Masaryk	Masaryk	k1gMnSc1	Masaryk
předkládá	předkládat	k5eAaImIp3nS	předkládat
britskému	britský	k2eAgMnSc3d1	britský
ministrovi	ministr	k1gMnSc3	ministr
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
věcí	věc	k1gFnSc7	věc
plán	plán	k1gInSc4	plán
na	na	k7c4	na
vytvoření	vytvoření	k1gNnSc4	vytvoření
budoucího	budoucí	k2eAgInSc2d1	budoucí
společného	společný	k2eAgInSc2d1	společný
státu	stát	k1gInSc2	stát
Čechů	Čech	k1gMnPc2	Čech
<g/>
,	,	kIx,	,
Srbů	Srb	k1gMnPc2	Srb
a	a	k8xC	a
Chorvatů	Chorvat	k1gMnPc2	Chorvat
(	(	kIx(	(
<g/>
přesněji	přesně	k6eAd2	přesně
–	–	k?	–
předpokládané	předpokládaný	k2eAgFnPc4d1	předpokládaná
"	"	kIx"	"
<g/>
Svobodné	svobodný	k2eAgFnPc4d1	svobodná
Čechy	Čechy	k1gFnPc4	Čechy
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
měly	mít	k5eAaImAgInP	mít
<g />
.	.	kIx.	.
</s>
<s>
spojit	spojit	k5eAaPmF	spojit
s	s	k7c7	s
předpokládaným	předpokládaný	k2eAgNnSc7d1	předpokládané
"	"	kIx"	"
<g/>
Srbochorvatskem	Srbochorvatsko	k1gNnSc7	Srbochorvatsko
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
1916	[number]	k4	1916
únor	únor	k1gInSc4	únor
–	–	k?	–
Comité	Comitý	k2eAgFnSc2d1	Comitý
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
action	action	k1gInSc1	action
tchè	tchè	k?	tchè
à	à	k?	à
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
étranger	étrangero	k1gNnPc2	étrangero
přejmenováno	přejmenovat	k5eAaPmNgNnS	přejmenovat
na	na	k7c4	na
Česko-slovenskou	českolovenský	k2eAgFnSc4d1	česko-slovenská
národní	národní	k2eAgFnSc4d1	národní
radu	rada	k1gFnSc4	rada
<g/>
,	,	kIx,	,
Masaryk	Masaryk	k1gMnSc1	Masaryk
zvolen	zvolit	k5eAaPmNgMnS	zvolit
předsedou	předseda	k1gMnSc7	předseda
také	také	k9	také
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
Masarykovo	Masarykův	k2eAgNnSc4d1	Masarykovo
jednání	jednání	k1gNnSc4	jednání
s	s	k7c7	s
francouzským	francouzský	k2eAgMnSc7d1	francouzský
premiérem	premiér	k1gMnSc7	premiér
Aristidem	Aristid	k1gMnSc7	Aristid
Briandem	Briand	k1gInSc7	Briand
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
umožněné	umožněný	k2eAgNnSc1d1	umožněné
Štefánikovým	Štefánikův	k2eAgNnSc7d1	Štefánikovo
prostřednictvím	prostřednictví	k1gNnSc7	prostřednictví
<g/>
,	,	kIx,	,
zprávy	zpráva	k1gFnSc2	zpráva
o	o	k7c4	o
jednání	jednání	k1gNnSc4	jednání
a	a	k8xC	a
požadavku	požadavek	k1gInSc2	požadavek
samostatnosti	samostatnost	k1gFnSc2	samostatnost
ve	v	k7c6	v
francouzském	francouzský	k2eAgInSc6d1	francouzský
a	a	k8xC	a
anglickém	anglický	k2eAgInSc6d1	anglický
tisku	tisk	k1gInSc6	tisk
další	další	k2eAgNnSc4d1	další
jednání	jednání	k1gNnSc4	jednání
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
o	o	k7c6	o
budoucím	budoucí	k2eAgInSc6d1	budoucí
samostatném	samostatný	k2eAgInSc6d1	samostatný
státě	stát	k1gInSc6	stát
Rusko	Rusko	k1gNnSc1	Rusko
1917	[number]	k4	1917
květen	květno	k1gNnPc2	květno
–	–	k?	–
Masarykův	Masarykův	k2eAgInSc4d1	Masarykův
příjezd	příjezd	k1gInSc4	příjezd
do	do	k7c2	do
Ruska	Rusko	k1gNnSc2	Rusko
a	a	k8xC	a
jednání	jednání	k1gNnSc2	jednání
o	o	k7c4	o
založení	založení	k1gNnSc4	založení
československých	československý	k2eAgFnPc2d1	Československá
jednotek	jednotka	k1gFnPc2	jednotka
včetně	včetně	k7c2	včetně
jejich	jejich	k3xOp3gNnPc2	jejich
rozšiřování	rozšiřování	k1gNnPc2	rozšiřování
červenec	červenec	k1gInSc1	červenec
<g/>
–	–	k?	–
<g/>
srpen	srpen	k1gInSc4	srpen
–	–	k?	–
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
návštěvy	návštěva	k1gFnSc2	návštěva
v	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
lazaretech	lazaret	k1gInPc6	lazaret
<g/>
,	,	kIx,	,
zajateckých	zajatecký	k2eAgInPc6d1	zajatecký
táborech	tábor	k1gInPc6	tábor
a	a	k8xC	a
u	u	k7c2	u
legií	legie	k1gFnPc2	legie
září	září	k1gNnSc2	září
<g/>
–	–	k?	–
<g/>
říjen	říjen	k1gInSc1	říjen
–	–	k?	–
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
příprava	příprava	k1gFnSc1	příprava
přezimování	přezimování	k1gNnSc2	přezimování
legionářských	legionářský	k2eAgInPc2d1	legionářský
korpusů	korpus	k1gInPc2	korpus
15	[number]	k4	15
<g/>
.	.	kIx.	.
říjen	říjen	k1gInSc1	říjen
–	–	k?	–
první	první	k4xOgInSc4	první
transport	transport	k1gInSc4	transport
téměř	téměř	k6eAd1	téměř
1500	[number]	k4	1500
čs	čs	kA	čs
<g/>
.	.	kIx.	.
legionářů	legionář	k1gMnPc2	legionář
z	z	k7c2	z
Ruska	Rusko	k1gNnSc2	Rusko
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
lodí	loď	k1gFnPc2	loď
tzv.	tzv.	kA	tzv.
severní	severní	k2eAgFnSc7d1	severní
cestou	cesta	k1gFnSc7	cesta
(	(	kIx(	(
<g/>
lodí	loď	k1gFnSc7	loď
z	z	k7c2	z
Murmaně	Murmaň	k1gFnSc2	Murmaň
<g/>
)	)	kIx)	)
prosinec	prosinec	k1gInSc1	prosinec
–	–	k?	–
Francie	Francie	k1gFnSc2	Francie
<g />
.	.	kIx.	.
</s>
<s>
povolila	povolit	k5eAaPmAgFnS	povolit
zřízení	zřízení	k1gNnSc4	zřízení
čs	čs	kA	čs
<g/>
.	.	kIx.	.
jednotek	jednotka	k1gFnPc2	jednotka
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
území	území	k1gNnSc6	území
1918	[number]	k4	1918
únor	únor	k1gInSc4	únor
–	–	k?	–
Spojenci	spojenec	k1gMnPc7	spojenec
uznali	uznat	k5eAaPmAgMnP	uznat
čs	čs	kA	čs
<g/>
.	.	kIx.	.
jednotky	jednotka	k1gFnPc1	jednotka
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
jako	jako	k8xC	jako
část	část	k1gFnSc1	část
čs	čs	kA	čs
<g/>
.	.	kIx.	.
jednotek	jednotka	k1gFnPc2	jednotka
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
záměrem	záměr	k1gInSc7	záměr
bylo	být	k5eAaImAgNnS	být
je	on	k3xPp3gFnPc4	on
přesunout	přesunout	k5eAaPmF	přesunout
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
tam	tam	k6eAd1	tam
je	být	k5eAaImIp3nS	být
spojit	spojit	k5eAaPmF	spojit
<g/>
,	,	kIx,	,
záměr	záměr	k1gInSc1	záměr
se	se	k3xPyFc4	se
nezdařil	zdařit	k5eNaPmAgInS	zdařit
(	(	kIx(	(
<g/>
jednotky	jednotka	k1gFnPc1	jednotka
se	se	k3xPyFc4	se
z	z	k7c2	z
<g />
.	.	kIx.	.
</s>
<s>
Ruska	Rusko	k1gNnSc2	Rusko
vracely	vracet	k5eAaImAgInP	vracet
ještě	ještě	k6eAd1	ještě
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
války	válka	k1gFnSc2	válka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zprávy	zpráva	k1gFnPc1	zpráva
o	o	k7c6	o
statečnosti	statečnost	k1gFnSc6	statečnost
v	v	k7c6	v
bojích	boj	k1gInPc6	boj
s	s	k7c7	s
jak	jak	k6eAd1	jak
německými	německý	k2eAgFnPc7d1	německá
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
bolševickými	bolševický	k2eAgNnPc7d1	bolševické
vojsky	vojsko	k1gNnPc7	vojsko
měly	mít	k5eAaImAgFnP	mít
svůj	svůj	k3xOyFgInSc4	svůj
kýžený	kýžený	k2eAgInSc4d1	kýžený
propagandistický	propagandistický	k2eAgInSc4d1	propagandistický
účinek	účinek	k1gInSc4	účinek
v	v	k7c6	v
jednáních	jednání	k1gNnPc6	jednání
se	s	k7c7	s
státníky	státník	k1gMnPc7	státník
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
vychází	vycházet	k5eAaImIp3nS	vycházet
Masarykovo	Masarykův	k2eAgNnSc4d1	Masarykovo
dílo	dílo	k1gNnSc4	dílo
The	The	k1gMnSc2	The
New	New	k1gMnSc2	New
Europe	Europ	k1gInSc5	Europ
–	–	k?	–
Nová	nový	k2eAgFnSc1d1	nová
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
:	:	kIx,	:
Stanovisko	stanovisko	k1gNnSc1	stanovisko
slovanské	slovanský	k2eAgNnSc1d1	slovanské
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
politický	politický	k2eAgInSc4d1	politický
program	program	k1gInSc4	program
uspořádání	uspořádání	k1gNnSc2	uspořádání
poválečné	poválečný	k2eAgFnSc2d1	poválečná
Evropy	Evropa	k1gFnSc2	Evropa
březen	březen	k1gInSc1	březen
–	–	k?	–
Masaryk	Masaryk	k1gMnSc1	Masaryk
vyráží	vyrážet	k5eAaImIp3nS	vyrážet
z	z	k7c2	z
Moskvy	Moskva	k1gFnSc2	Moskva
do	do	k7c2	do
Vladivostoku	Vladivostok	k1gInSc2	Vladivostok
(	(	kIx(	(
<g/>
Transsibiřskou	transsibiřský	k2eAgFnSc7d1	Transsibiřská
magistrálou	magistrála	k1gFnSc7	magistrála
<g/>
)	)	kIx)	)
a	a	k8xC	a
přes	přes	k7c4	přes
Koreu	Korea	k1gFnSc4	Korea
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc4	Japonsko
a	a	k8xC	a
Kanadu	Kanada	k1gFnSc4	Kanada
do	do	k7c2	do
USA	USA	kA	USA
USA	USA	kA	USA
1918	[number]	k4	1918
květen	květen	k1gInSc4	květen
–	–	k?	–
triumfální	triumfální	k2eAgNnSc4d1	triumfální
přijetí	přijetí	k1gNnSc4	přijetí
deseti	deset	k4xCc7	deset
tisíci	tisíc	k4xCgInPc7	tisíc
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
jako	jako	k9	jako
i	i	k9	i
o	o	k7c4	o
něco	něco	k3yInSc4	něco
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
následovalo	následovat	k5eAaImAgNnS	následovat
turné	turné	k1gNnSc1	turné
po	po	k7c6	po
USA	USA	kA	USA
<g/>
,	,	kIx,	,
během	během	k7c2	během
kterého	který	k3yRgNnSc2	který
Masaryk	Masaryk	k1gMnSc1	Masaryk
loboval	lobovat	k5eAaBmAgMnS	lobovat
pro	pro	k7c4	pro
čs	čs	kA	čs
<g/>
.	.	kIx.	.
samostatnost	samostatnost	k1gFnSc4	samostatnost
<g/>
,	,	kIx,	,
i	i	k8xC	i
na	na	k7c6	na
řadě	řada	k1gFnSc6	řada
setkání	setkání	k1gNnSc2	setkání
s	s	k7c7	s
představiteli	představitel	k1gMnPc7	představitel
a	a	k8xC	a
organizacemi	organizace	k1gFnPc7	organizace
krajanů	krajan	k1gMnPc2	krajan
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
počet	počet	k1gInSc1	počet
byl	být	k5eAaImAgInS	být
odhadován	odhadovat	k5eAaImNgInS	odhadovat
na	na	k7c4	na
více	hodně	k6eAd2	hodně
než	než	k8xS	než
milion	milion	k4xCgInSc1	milion
<g/>
,	,	kIx,	,
četné	četný	k2eAgFnPc1d1	četná
zprávy	zpráva	k1gFnPc1	zpráva
v	v	k7c6	v
tisku	tisk	k1gInSc6	tisk
(	(	kIx(	(
<g/>
mj.	mj.	kA	mj.
The	The	k1gFnSc1	The
Washington	Washington	k1gInSc1	Washington
Post	post	k1gInSc1	post
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
Times	Times	k1gInSc1	Times
<g/>
)	)	kIx)	)
<g/>
Pittsburská	pittsburský	k2eAgFnSc1d1	Pittsburská
dohoda	dohoda	k1gFnSc1	dohoda
mezi	mezi	k7c7	mezi
americkými	americký	k2eAgMnPc7d1	americký
Čechy	Čech	k1gMnPc7	Čech
a	a	k8xC	a
Slováky	Slovák	k1gMnPc7	Slovák
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
Masaryk	Masaryk	k1gMnSc1	Masaryk
také	také	k9	také
podepsal	podepsat	k5eAaPmAgMnS	podepsat
<g/>
,	,	kIx,	,
Slovákům	Slovák	k1gMnPc3	Slovák
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
v	v	k7c6	v
budoucím	budoucí	k2eAgInSc6d1	budoucí
společném	společný	k2eAgInSc6d1	společný
státě	stát	k1gInSc6	stát
zaručena	zaručit	k5eAaPmNgFnS	zaručit
vlastní	vlastní	k2eAgFnSc1d1	vlastní
správa	správa	k1gFnSc1	správa
<g/>
,	,	kIx,	,
vlastní	vlastní	k2eAgInSc1d1	vlastní
parlament	parlament	k1gInSc1	parlament
<g/>
,	,	kIx,	,
vlastní	vlastní	k2eAgNnSc1d1	vlastní
soudnictví	soudnictví	k1gNnSc1	soudnictví
(	(	kIx(	(
<g/>
což	což	k3yRnSc1	což
všechno	všechen	k3xTgNnSc1	všechen
ale	ale	k8xC	ale
v	v	k7c6	v
první	první	k4xOgFnSc6	první
Ústavě	ústava	k1gFnSc6	ústava
ČSR	ČSR	kA	ČSR
<g />
.	.	kIx.	.
</s>
<s>
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
chybělo	chybět	k5eAaImAgNnS	chybět
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
pramenem	pramen	k1gInSc7	pramen
protestů	protest	k1gInPc2	protest
<g/>
,	,	kIx,	,
nespokojenosti	nespokojenost	k1gFnSc2	nespokojenost
a	a	k8xC	a
rozbrojů	rozbroj	k1gInPc2	rozbroj
až	až	k9	až
do	do	k7c2	do
rozpadu	rozpad	k1gInSc2	rozpad
ČSR	ČSR	kA	ČSR
<g/>
)	)	kIx)	)
19	[number]	k4	19
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
–	–	k?	–
Masarykovo	Masarykův	k2eAgNnSc1d1	Masarykovo
jednání	jednání	k1gNnSc1	jednání
(	(	kIx(	(
<g/>
první	první	k4xOgFnSc1	první
ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
<g/>
,	,	kIx,	,
probíhala	probíhat	k5eAaImAgNnP	probíhat
až	až	k6eAd1	až
do	do	k7c2	do
listopadu	listopad	k1gInSc2	listopad
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
prezidentem	prezident	k1gMnSc7	prezident
Wilsonem	Wilson	k1gMnSc7	Wilson
v	v	k7c6	v
Bílém	bílý	k2eAgInSc6d1	bílý
domě	dům	k1gInSc6	dům
<g/>
,	,	kIx,	,
Masaryk	Masaryk	k1gMnSc1	Masaryk
<g />
.	.	kIx.	.
</s>
<s>
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
Wilsonovy	Wilsonův	k2eAgFnSc2d1	Wilsonova
plné	plný	k2eAgFnSc2d1	plná
podpory	podpora	k1gFnSc2	podpora
28	[number]	k4	28
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
–	–	k?	–
prezident	prezident	k1gMnSc1	prezident
Wilson	Wilson	k1gMnSc1	Wilson
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
že	že	k8xS	že
USA	USA	kA	USA
sledují	sledovat	k5eAaImIp3nP	sledovat
osvobození	osvobození	k1gNnSc4	osvobození
slovanských	slovanský	k2eAgInPc2d1	slovanský
národů	národ	k1gInPc2	národ
z	z	k7c2	z
německé	německý	k2eAgFnSc2d1	německá
a	a	k8xC	a
rakouské	rakouský	k2eAgFnSc2d1	rakouská
nadvlády	nadvláda	k1gFnSc2	nadvláda
29	[number]	k4	29
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
–	–	k?	–
Francie	Francie	k1gFnSc1	Francie
uznala	uznat	k5eAaPmAgFnS	uznat
Česko-slovenskou	českolovenský	k2eAgFnSc4d1	česko-slovenská
národní	národní	k2eAgFnSc4d1	národní
radu	rada	k1gFnSc4	rada
(	(	kIx(	(
<g/>
ČSNR	ČSNR	kA	ČSNR
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
základ	základ	k1gInSc1	základ
budoucí	budoucí	k2eAgFnSc2d1	budoucí
čs	čs	kA	čs
<g/>
.	.	kIx.	.
vlády	vláda	k1gFnSc2	vláda
s	s	k7c7	s
T.	T.	kA	T.
G.	G.	kA	G.
Masarykem	Masaryk	k1gMnSc7	Masaryk
v	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
čele	čelo	k1gNnSc6	čelo
(	(	kIx(	(
<g/>
především	především	k6eAd1	především
Benešova	Benešův	k2eAgFnSc1d1	Benešova
a	a	k8xC	a
Rašínova	Rašínův	k2eAgFnSc1d1	Rašínova
práce	práce	k1gFnSc1	práce
<g/>
)	)	kIx)	)
9	[number]	k4	9
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
–	–	k?	–
Británie	Británie	k1gFnSc1	Británie
obdobně	obdobně	k6eAd1	obdobně
uznala	uznat	k5eAaPmAgFnS	uznat
ČSNR	ČSNR	kA	ČSNR
jako	jako	k8xC	jako
vládu	vláda	k1gFnSc4	vláda
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
Čecho-slováky	Čecholovák	k1gInPc4	Čecho-slovák
za	za	k7c2	za
spojence	spojenec	k1gMnSc2	spojenec
3	[number]	k4	3
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
–	–	k?	–
USA	USA	kA	USA
obdobně	obdobně	k6eAd1	obdobně
uznaly	uznat	k5eAaPmAgFnP	uznat
ČSNR	ČSNR	kA	ČSNR
jako	jako	k8xC	jako
vládu	vláda	k1gFnSc4	vláda
a	a	k8xC	a
spojence	spojenka	k1gFnSc6	spojenka
5	[number]	k4	5
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
–	–	k?	–
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1	Rakousko-Uhersko
předkládá	předkládat	k5eAaImIp3nS	předkládat
USA	USA	kA	USA
mírovou	mírový	k2eAgFnSc4d1	mírová
dohodu	dohoda	k1gFnSc4	dohoda
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
uznalo	uznat	k5eAaPmAgNnS	uznat
Čtrnáct	čtrnáct	k4xCc4	čtrnáct
bodů	bod	k1gInPc2	bod
prezidenta	prezident	k1gMnSc2	prezident
Wilsona	Wilson	k1gMnSc2	Wilson
(	(	kIx(	(
<g/>
z	z	k7c2	z
8	[number]	k4	8
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterými	který	k3yRgInPc7	který
požadoval	požadovat	k5eAaImAgInS	požadovat
autonomii	autonomie	k1gFnSc4	autonomie
národů	národ	k1gInPc2	národ
Rakouska-Uherska	Rakouska-Uhersek	k1gMnSc2	Rakouska-Uhersek
14	[number]	k4	14
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
–	–	k?	–
Beneš	Beneš	k1gMnSc1	Beneš
a	a	k8xC	a
Rašín	Rašína	k1gFnPc2	Rašína
oznámili	oznámit	k5eAaPmAgMnP	oznámit
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
založení	založení	k1gNnSc2	založení
Prozatímní	prozatímní	k2eAgFnSc2d1	prozatímní
česko-slovenské	českolovenský	k2eAgFnSc2d1	česko-slovenská
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
s	s	k7c7	s
Masarykem	Masaryk	k1gMnSc7	Masaryk
jako	jako	k8xS	jako
premiérem	premiér	k1gMnSc7	premiér
15	[number]	k4	15
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
Francie	Francie	k1gFnSc1	Francie
uznala	uznat	k5eAaPmAgFnS	uznat
Prozatímní	prozatímní	k2eAgFnSc4d1	prozatímní
československou	československý	k2eAgFnSc4d1	Československá
vládu	vláda	k1gFnSc4	vláda
16	[number]	k4	16
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
–	–	k?	–
Masaryk	Masaryk	k1gMnSc1	Masaryk
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
formuluje	formulovat	k5eAaImIp3nS	formulovat
prohlášení	prohlášení	k1gNnSc1	prohlášení
československé	československý	k2eAgFnSc2d1	Československá
samostatnosti	samostatnost	k1gFnSc2	samostatnost
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
prohlášení	prohlášení	k1gNnSc1	prohlášení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
československého	československý	k2eAgInSc2d1	československý
národa	národ	k1gInSc2	národ
prozatímní	prozatímní	k2eAgFnSc7d1	prozatímní
vládou	vláda	k1gFnSc7	vláda
československou	československý	k2eAgFnSc7d1	Československá
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
předseda	předseda	k1gMnSc1	předseda
prozatímní	prozatímní	k2eAgFnSc2d1	prozatímní
vlády	vláda	k1gFnSc2	vláda
17	[number]	k4	17
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
–	–	k?	–
rakousko-uherský	rakouskoherský	k2eAgMnSc1d1	rakousko-uherský
císař	císař	k1gMnSc1	císař
Karel	Karel	k1gMnSc1	Karel
I.	I.	kA	I.
zveřejnil	zveřejnit	k5eAaPmAgMnS	zveřejnit
svůj	svůj	k3xOyFgInSc4	svůj
manifest	manifest	k1gInSc4	manifest
o	o	k7c6	o
federativním	federativní	k2eAgInSc6d1	federativní
<g />
.	.	kIx.	.
</s>
<s>
uspořádání	uspořádání	k1gNnSc1	uspořádání
své	svůj	k3xOyFgFnSc2	svůj
říše	říš	k1gFnSc2	říš
18	[number]	k4	18
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
–	–	k?	–
Masaryk	Masaryk	k1gMnSc1	Masaryk
zveřejnil	zveřejnit	k5eAaPmAgMnS	zveřejnit
Washingtonskou	washingtonský	k2eAgFnSc4d1	Washingtonská
deklaraci	deklarace	k1gFnSc4	deklarace
(	(	kIx(	(
<g/>
na	na	k7c4	na
které	který	k3yRgFnPc4	který
v	v	k7c6	v
předcházejících	předcházející	k2eAgNnPc6d1	předcházející
dnech	dno	k1gNnPc6	dno
Beneš	Beneš	k1gMnSc1	Beneš
horečně	horečně	k6eAd1	horečně
pracoval	pracovat	k5eAaImAgMnS	pracovat
<g/>
,	,	kIx,	,
Masaryk	Masaryk	k1gMnSc1	Masaryk
mu	on	k3xPp3gMnSc3	on
její	její	k3xOp3gFnSc3	její
konečné	konečný	k2eAgNnSc1d1	konečné
znění	znění	k1gNnSc1	znění
tento	tento	k3xDgInSc4	tento
den	den	k1gInSc4	den
telegrafoval	telegrafovat	k5eAaBmAgMnS	telegrafovat
z	z	k7c2	z
Washingtonu	Washington	k1gInSc2	Washington
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
)	)	kIx)	)
a	a	k8xC	a
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
jménem	jméno	k1gNnSc7	jméno
Prozatímní	prozatímní	k2eAgFnSc2d1	prozatímní
česko-slovenské	českolovenský	k2eAgFnSc2d1	česko-slovenská
vlády	vláda	k1gFnSc2	vláda
samostatný	samostatný	k2eAgInSc1d1	samostatný
Česko-slovenský	českolovenský	k2eAgInSc1d1	česko-slovenský
státWilson	státWilson	k1gInSc1	státWilson
odmítl	odmítnout	k5eAaPmAgInS	odmítnout
návrh	návrh	k1gInSc4	návrh
mírové	mírový	k2eAgFnSc2d1	mírová
<g />
.	.	kIx.	.
</s>
<s>
dohody	dohoda	k1gFnPc1	dohoda
<g/>
,	,	kIx,	,
předložený	předložený	k2eAgInSc1d1	předložený
5	[number]	k4	5
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
Rakousko-Uherskem	Rakousko-Uhersek	k1gMnSc7	Rakousko-Uhersek
<g/>
,	,	kIx,	,
poukázal	poukázat	k5eAaPmAgMnS	poukázat
na	na	k7c4	na
snahy	snaha	k1gFnPc4	snaha
Čecho-slováků	Čecholovák	k1gMnPc2	Čecho-slovák
a	a	k8xC	a
jihoslovanů	jihoslovan	k1gMnPc2	jihoslovan
o	o	k7c4	o
samostatnost	samostatnost	k1gFnSc4	samostatnost
a	a	k8xC	a
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
nich	on	k3xPp3gInPc6	on
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
o	o	k7c6	o
nabídce	nabídka	k1gFnSc6	nabídka
Karla	Karel	k1gMnSc2	Karel
I.	I.	kA	I.
23	[number]	k4	23
<g/>
.	.	kIx.	.
–	–	k?	–
26	[number]	k4	26
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
–	–	k?	–
sjezd	sjezd	k1gInSc1	sjezd
Středoevropské	středoevropský	k2eAgFnSc2d1	středoevropská
unie	unie	k1gFnSc2	unie
ve	v	k7c6	v
Filadelfii	Filadelfie	k1gFnSc6	Filadelfie
<g/>
,	,	kIx,	,
T.	T.	kA	T.
G.	G.	kA	G.
Masaryk	Masaryk	k1gMnSc1	Masaryk
zvolen	zvolit	k5eAaPmNgMnS	zvolit
jejím	její	k3xOp3gNnSc7	její
<g />
.	.	kIx.	.
</s>
<s>
prezidentem	prezident	k1gMnSc7	prezident
<g/>
,	,	kIx,	,
přednesl	přednést	k5eAaPmAgMnS	přednést
a	a	k8xC	a
u	u	k7c2	u
zvonu	zvon	k1gInSc2	zvon
nezávislosti	nezávislost	k1gFnSc2	nezávislost
i	i	k8xC	i
podepsal	podepsat	k5eAaPmAgMnS	podepsat
její	její	k3xOp3gFnSc4	její
deklaraci	deklarace	k1gFnSc4	deklarace
<g/>
,	,	kIx,	,
jednání	jednání	k1gNnPc4	jednání
zástupců	zástupce	k1gMnPc2	zástupce
Rusínů	Rusín	k1gMnPc2	Rusín
s	s	k7c7	s
Masarykem	Masaryk	k1gMnSc7	Masaryk
<g/>
,	,	kIx,	,
podpis	podpis	k1gInSc4	podpis
dohody	dohoda	k1gFnSc2	dohoda
o	o	k7c4	o
připojení	připojení	k1gNnSc4	připojení
Podkarpatské	podkarpatský	k2eAgFnSc2d1	Podkarpatská
Rusi	Rus	k1gFnSc2	Rus
k	k	k7c3	k
budoucímu	budoucí	k2eAgInSc3d1	budoucí
státu	stát	k1gInSc3	stát
<g/>
,	,	kIx,	,
při	při	k7c6	při
zaručení	zaručení	k1gNnSc6	zaručení
autonomie	autonomie	k1gFnSc2	autonomie
(	(	kIx(	(
<g/>
nebyla	být	k5eNaImAgFnS	být
splněna	splnit	k5eAaPmNgFnS	splnit
<g/>
,	,	kIx,	,
teprve	teprve	k6eAd1	teprve
až	až	k9	až
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
rozpadem	rozpad	k1gInSc7	rozpad
ČSR	ČSR	kA	ČSR
první	první	k4xOgFnSc2	první
vlažné	vlažný	k2eAgFnSc2d1	vlažná
reakce	reakce	k1gFnSc2	reakce
<g />
.	.	kIx.	.
</s>
<s>
centralistické	centralistický	k2eAgInPc1d1	centralistický
čs	čs	kA	čs
<g/>
.	.	kIx.	.
vlády	vláda	k1gFnSc2	vláda
<g/>
)	)	kIx)	)
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
–	–	k?	–
Karel	Karel	k1gMnSc1	Karel
I.	I.	kA	I.
přijímá	přijímat	k5eAaImIp3nS	přijímat
Wilsonovo	Wilsonův	k2eAgNnSc4d1	Wilsonovo
prohlášení	prohlášení	k1gNnSc4	prohlášení
z	z	k7c2	z
18	[number]	k4	18
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
a	a	k8xC	a
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
okamžité	okamžitý	k2eAgNnSc4d1	okamžité
příměří	příměří	k1gNnSc4	příměří
(	(	kIx(	(
<g/>
tento	tento	k3xDgInSc1	tento
den	den	k1gInSc1	den
platí	platit	k5eAaImIp3nS	platit
také	také	k9	také
jako	jako	k9	jako
den	den	k1gInSc4	den
vzniku	vznik	k1gInSc2	vznik
ČSR	ČSR	kA	ČSR
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
přítomná	přítomný	k2eAgFnSc1d1	přítomná
skupina	skupina	k1gFnSc1	skupina
politiků	politik	k1gMnPc2	politik
"	"	kIx"	"
<g/>
muži	muž	k1gMnPc1	muž
28	[number]	k4	28
<g/>
<g />
.	.	kIx.	.
</s>
<s>
října	říjen	k1gInSc2	říjen
<g/>
"	"	kIx"	"
dozvěděla	dozvědět	k5eAaPmAgFnS	dozvědět
o	o	k7c6	o
kroku	krok	k1gInSc6	krok
Karla	Karel	k1gMnSc2	Karel
I.	I.	kA	I.
a	a	k8xC	a
narychlo	narychlo	k6eAd1	narychlo
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
samostatný	samostatný	k2eAgInSc4d1	samostatný
stát	stát	k1gInSc4	stát
<g/>
)	)	kIx)	)
30	[number]	k4	30
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
–	–	k?	–
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
národní	národní	k2eAgFnSc1d1	národní
rada	rada	k1gFnSc1	rada
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
v	v	k7c6	v
Martině	Martina	k1gFnSc6	Martina
Deklarací	deklarace	k1gFnPc2	deklarace
slovenského	slovenský	k2eAgInSc2d1	slovenský
národa	národ	k1gInSc2	národ
své	svůj	k3xOyFgNnSc4	svůj
založení	založení	k1gNnSc4	založení
a	a	k8xC	a
spojení	spojení	k1gNnSc4	spojení
Slovenska	Slovensko	k1gNnSc2	Slovensko
s	s	k7c7	s
českými	český	k2eAgFnPc7d1	Česká
zeměmi	zem	k1gFnPc7	zem
<g/>
,	,	kIx,	,
vycházela	vycházet	k5eAaImAgNnP	vycházet
přitom	přitom	k6eAd1	přitom
nejen	nejen	k6eAd1	nejen
z	z	k7c2	z
četných	četný	k2eAgFnPc2d1	četná
ústních	ústní	k2eAgFnPc2d1	ústní
dohod	dohoda	k1gFnPc2	dohoda
ale	ale	k8xC	ale
i	i	k9	i
písemných	písemný	k2eAgInPc2d1	písemný
závazků	závazek	k1gInPc2	závazek
mj.	mj.	kA	mj.
i	i	k8xC	i
Pittsburské	pittsburský	k2eAgFnSc2d1	Pittsburská
dohody	dohoda	k1gFnSc2	dohoda
<g/>
,	,	kIx,	,
původní	původní	k2eAgFnSc1d1	původní
Martinská	martinský	k2eAgFnSc1d1	Martinská
deklarace	deklarace	k1gFnSc1	deklarace
(	(	kIx(	(
<g/>
originál	originál	k1gInSc1	originál
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
ale	ale	k9	ale
"	"	kIx"	"
<g/>
ztratila	ztratit	k5eAaPmAgFnS	ztratit
<g/>
"	"	kIx"	"
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
13	[number]	k4	13
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
–	–	k?	–
Národní	národní	k2eAgInSc1d1	národní
výbor	výbor	k1gInSc1	výbor
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
Prozatímní	prozatímní	k2eAgFnSc4d1	prozatímní
ústavu	ústava	k1gFnSc4	ústava
14	[number]	k4	14
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
–	–	k?	–
Revoluční	revoluční	k2eAgInSc1d1	revoluční
parlament	parlament	k1gInSc1	parlament
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
právě	právě	k6eAd1	právě
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
z	z	k7c2	z
Národního	národní	k2eAgInSc2d1	národní
výboru	výbor	k1gInSc2	výbor
(	(	kIx(	(
<g/>
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
přejmenovává	přejmenovávat	k5eAaImIp3nS	přejmenovávat
na	na	k7c4	na
Revoluční	revoluční	k2eAgNnSc4d1	revoluční
národní	národní	k2eAgNnSc4d1	národní
shromáždění	shromáždění	k1gNnSc4	shromáždění
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zvolil	zvolit	k5eAaPmAgMnS	zvolit
Masaryka	Masaryk	k1gMnSc4	Masaryk
prezidentem	prezident	k1gMnSc7	prezident
samostatného	samostatný	k2eAgInSc2d1	samostatný
státu	stát	k1gInSc2	stát
(	(	kIx(	(
<g/>
v	v	k7c6	v
Revolučním	revoluční	k2eAgInSc6d1	revoluční
<g />
.	.	kIx.	.
</s>
<s>
parlamentu	parlament	k1gInSc2	parlament
ale	ale	k9	ale
nebyly	být	k5eNaImAgFnP	být
zastoupeny	zastoupit	k5eAaPmNgFnP	zastoupit
národnostní	národnostní	k2eAgFnPc1d1	národnostní
menšiny	menšina	k1gFnPc1	menšina
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
ani	ani	k9	ani
početných	početný	k2eAgFnPc2d1	početná
národnostních	národnostní	k2eAgFnPc2d1	národnostní
menšin	menšina	k1gFnPc2	menšina
německých	německý	k2eAgMnPc2d1	německý
<g/>
,	,	kIx,	,
rusínských	rusínský	k2eAgInPc2d1	rusínský
<g/>
,	,	kIx,	,
rumunských	rumunský	k2eAgInPc2d1	rumunský
a	a	k8xC	a
maďarských	maďarský	k2eAgInPc2d1	maďarský
<g/>
)	)	kIx)	)
18	[number]	k4	18
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
–	–	k?	–
vytvoření	vytvoření	k1gNnSc1	vytvoření
společného	společný	k2eAgInSc2d1	společný
státu	stát	k1gInSc2	stát
a	a	k8xC	a
autonomie	autonomie	k1gFnSc2	autonomie
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
dohodnuty	dohodnout	k5eAaPmNgFnP	dohodnout
ve	v	k7c6	v
Filadelfské	filadelfský	k2eAgFnSc6d1	Filadelfská
dohodě	dohoda	k1gFnSc6	dohoda
<g/>
,	,	kIx,	,
schváleny	schválit	k5eAaPmNgFnP	schválit
Národní	národní	k2eAgFnSc7d1	národní
radou	rada	k1gFnSc7	rada
uherských	uherský	k2eAgInPc2d1	uherský
Rusínů	rusín	k1gInPc2	rusín
v	v	k7c6	v
USA	USA	kA	USA
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
hlasování	hlasování	k1gNnSc1	hlasování
v	v	k7c6	v
kostelech	kostel	k1gInPc6	kostel
po	po	k7c6	po
bohoslužbách	bohoslužba	k1gFnPc6	bohoslužba
<g/>
,	,	kIx,	,
67	[number]	k4	67
%	%	kIx~	%
Rusínů	rusín	k1gInPc2	rusín
se	se	k3xPyFc4	se
vyslovilo	vyslovit	k5eAaPmAgNnS	vyslovit
pro	pro	k7c4	pro
4	[number]	k4	4
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
–	–	k?	–
Spojenci	spojenec	k1gMnPc1	spojenec
uznali	uznat	k5eAaPmAgMnP	uznat
Československo	Československo	k1gNnSc4	Československo
1918	[number]	k4	1918
20	[number]	k4	20
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
–	–	k?	–
Masaryk	Masaryk	k1gMnSc1	Masaryk
překročil	překročit	k5eAaPmAgMnS	překročit
z	z	k7c2	z
Rakouska	Rakousko	k1gNnSc2	Rakousko
nové	nový	k2eAgNnSc4d1	nové
čs	čs	kA	čs
<g/>
.	.	kIx.	.
hranice	hranice	k1gFnSc1	hranice
a	a	k8xC	a
po	po	k7c4	po
přivítání	přivítání	k1gNnSc4	přivítání
Národním	národní	k2eAgNnSc7d1	národní
shromážděním	shromáždění	k1gNnSc7	shromáždění
v	v	k7c6	v
Horním	horní	k2eAgNnSc6d1	horní
Dvořišti	dvořiště	k1gNnSc6	dvořiště
a	a	k8xC	a
občany	občan	k1gMnPc4	občan
na	na	k7c4	na
náměstí	náměstí	k1gNnSc4	náměstí
v	v	k7c6	v
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
<g/>
,	,	kIx,	,
veřejném	veřejný	k2eAgNnSc6d1	veřejné
vystoupení	vystoupení	k1gNnSc6	vystoupení
ve	v	k7c6	v
Veselí-Mezimostí	Veselí-Mezimost	k1gFnSc7	Veselí-Mezimost
<g/>
,	,	kIx,	,
Táboře	Tábor	k1gInSc6	Tábor
a	a	k8xC	a
Benešově	Benešov	k1gInSc6	Benešov
přijel	přijet	k5eAaPmAgInS	přijet
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
bouřlivě	bouřlivě	k6eAd1	bouřlivě
vítán	vítat	k5eAaImNgInS	vítat
nadšenými	nadšený	k2eAgInPc7d1	nadšený
davy	dav	k1gInPc7	dav
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Sněmovní	sněmovní	k2eAgFnSc6d1	sněmovní
ulici	ulice	k1gFnSc6	ulice
vykonal	vykonat	k5eAaPmAgMnS	vykonat
slib	slib	k1gInSc4	slib
prezidenta	prezident	k1gMnSc2	prezident
Republiky	republika	k1gFnSc2	republika
československé	československý	k2eAgFnSc2d1	Československá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
22	[number]	k4	22
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
pronáší	pronášet	k5eAaImIp3nS	pronášet
T.	T.	kA	T.
G.	G.	kA	G.
Masaryk	Masaryk	k1gMnSc1	Masaryk
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
"	"	kIx"	"
<g/>
První	první	k4xOgNnPc4	první
poselství	poselství	k1gNnPc4	poselství
<g/>
"	"	kIx"	"
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
podzim	podzim	k1gInSc4	podzim
1918	[number]	k4	1918
–	–	k?	–
jaro	jaro	k1gNnSc1	jaro
1919	[number]	k4	1919
–	–	k?	–
čs	čs	kA	čs
<g/>
.	.	kIx.	.
legionáři	legionář	k1gMnSc3	legionář
a	a	k8xC	a
vojenské	vojenský	k2eAgFnSc2d1	vojenská
jednotky	jednotka	k1gFnSc2	jednotka
zakročili	zakročit	k5eAaPmAgMnP	zakročit
proti	proti	k7c3	proti
snaze	snaha	k1gFnSc3	snaha
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
a	a	k8xC	a
Rakouska	Rakousko	k1gNnSc2	Rakousko
obsadit	obsadit	k5eAaPmF	obsadit
a	a	k8xC	a
anektovat	anektovat	k5eAaBmF	anektovat
části	část	k1gFnPc1	část
hraničních	hraniční	k2eAgNnPc2d1	hraniční
území	území	k1gNnPc2	území
1919	[number]	k4	1919
<g />
.	.	kIx.	.
</s>
<s>
1919	[number]	k4	1919
<g/>
–	–	k?	–
<g/>
20	[number]	k4	20
–	–	k?	–
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
jednání	jednání	k1gNnSc4	jednání
a	a	k8xC	a
dohody	dohoda	k1gFnPc4	dohoda
o	o	k7c4	o
definitivních	definitivní	k2eAgNnPc2d1	definitivní
čs	čs	kA	čs
<g/>
.	.	kIx.	.
hranicích	hranice	k1gFnPc6	hranice
(	(	kIx(	(
<g/>
mírové	mírový	k2eAgFnPc4d1	mírová
smlouvy	smlouva	k1gFnPc4	smlouva
z	z	k7c2	z
Versailles	Versailles	k1gFnSc2	Versailles
<g/>
,	,	kIx,	,
St.	st.	kA	st.
Germain	Germain	k1gInSc1	Germain
<g/>
,	,	kIx,	,
Trianonu	Trianon	k1gInSc3	Trianon
a	a	k8xC	a
Sè	Sè	k1gFnSc3	Sè
<g/>
,	,	kIx,	,
čs	čs	kA	čs
<g/>
.	.	kIx.	.
záležitosti	záležitost	k1gFnPc1	záležitost
pod	pod	k7c7	pod
Benešovým	Benešův	k2eAgNnSc7d1	Benešovo
vedením	vedení	k1gNnSc7	vedení
<g/>
)	)	kIx)	)
1919	[number]	k4	1919
–	–	k?	–
8	[number]	k4	8
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
na	na	k7c6	na
setkání	setkání	k1gNnSc6	setkání
<g />
.	.	kIx.	.
</s>
<s>
rusínských	rusínský	k2eAgFnPc2d1	rusínská
rad	rada	k1gFnPc2	rada
v	v	k7c6	v
Užhorodě	Užhorod	k1gInSc6	Užhorod
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
nově	nově	k6eAd1	nově
založená	založený	k2eAgFnSc1d1	založená
Karpatoruská	karpatoruský	k2eAgFnSc1d1	karpatoruský
ústřední	ústřední	k2eAgFnSc1d1	ústřední
národní	národní	k2eAgFnSc1d1	národní
rada	rada	k1gFnSc1	rada
připojení	připojení	k1gNnSc2	připojení
Podkarpatské	podkarpatský	k2eAgFnSc2d1	Podkarpatská
Rusi	Rus	k1gFnSc2	Rus
k	k	k7c3	k
Československu	Československo	k1gNnSc3	Československo
<g/>
,	,	kIx,	,
vycházela	vycházet	k5eAaImAgFnS	vycházet
při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
z	z	k7c2	z
Filadelfské	filadelfský	k2eAgFnSc2d1	Filadelfská
dohody	dohoda	k1gFnSc2	dohoda
zaručující	zaručující	k2eAgFnSc4d1	zaručující
autonomii	autonomie	k1gFnSc4	autonomie
<g/>
.	.	kIx.	.
1920	[number]	k4	1920
29	[number]	k4	29
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
vyhlásilo	vyhlásit	k5eAaPmAgNnS	vyhlásit
Revoluční	revoluční	k2eAgNnSc4d1	revoluční
národní	národní	k2eAgNnSc4d1	národní
shromáždění	shromáždění	k1gNnSc4	shromáždění
novou	nový	k2eAgFnSc4d1	nová
Ústavu	ústava	k1gFnSc4	ústava
(	(	kIx(	(
<g/>
platila	platit	k5eAaImAgFnS	platit
až	až	k9	až
do	do	k7c2	do
obsazení	obsazení	k1gNnSc2	obsazení
Sudet	Sudety	k1gFnPc2	Sudety
a	a	k8xC	a
nacistické	nacistický	k2eAgFnSc2d1	nacistická
okupace	okupace	k1gFnSc2	okupace
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
mj.	mj.	kA	mj.
vycházela	vycházet	k5eAaImAgFnS	vycházet
z	z	k7c2	z
doktríny	doktrína	k1gFnSc2	doktrína
<g/>
,	,	kIx,	,
ideologie	ideologie	k1gFnSc2	ideologie
jednotného	jednotný	k2eAgInSc2d1	jednotný
"	"	kIx"	"
<g/>
československého	československý	k2eAgInSc2d1	československý
národa	národ	k1gInSc2	národ
<g/>
"	"	kIx"	"
1920	[number]	k4	1920
Masaryk	Masaryk	k1gMnSc1	Masaryk
podruhé	podruhé	k6eAd1	podruhé
zvolen	zvolit	k5eAaPmNgMnS	zvolit
prezidentem	prezident	k1gMnSc7	prezident
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
i	i	k9	i
potřetí	potřetí	k4xO	potřetí
a	a	k8xC	a
počtvrté	počtvrté	k4xO	počtvrté
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1927	[number]	k4	1927
a	a	k8xC	a
1934	[number]	k4	1934
1935	[number]	k4	1935
14	[number]	k4	14
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
pro	pro	k7c4	pro
nemoc	nemoc	k1gFnSc4	nemoc
a	a	k8xC	a
stáří	stáří	k1gNnSc4	stáří
ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
pokročilých	pokročilý	k2eAgNnPc6d1	pokročilé
85	[number]	k4	85
letech	léto	k1gNnPc6	léto
abdikoval	abdikovat	k5eAaBmAgMnS	abdikovat
Masaryk	Masaryk	k1gMnSc1	Masaryk
zemřel	zemřít	k5eAaPmAgMnS	zemřít
14	[number]	k4	14
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1937	[number]	k4	1937
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
v	v	k7c6	v
Lánech	lán	k1gInPc6	lán
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
rodiny	rodina	k1gFnSc2	rodina
a	a	k8xC	a
svého	svůj	k3xOyFgMnSc2	svůj
nástupce	nástupce	k1gMnSc2	nástupce
<g/>
,	,	kIx,	,
druhého	druhý	k4xOgMnSc4	druhý
prezidenta	prezident	k1gMnSc4	prezident
Československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
Edvarda	Edvard	k1gMnSc2	Edvard
Beneše	Beneš	k1gMnSc2	Beneš
<g/>
.	.	kIx.	.
</s>
<s>
Prinzipien	Prinzipien	k2eAgInSc4d1	Prinzipien
der	drát	k5eAaImRp2nS	drát
Soziologie	Soziologie	k1gFnSc2	Soziologie
<g/>
,	,	kIx,	,
Vídeň	Vídeň	k1gFnSc1	Vídeň
1877	[number]	k4	1877
Der	drát	k5eAaImRp2nS	drát
Selbstmord	Selbstmord	k1gInSc1	Selbstmord
als	als	k?	als
soziale	soziale	k6eAd1	soziale
Massenerscheinung	Massenerscheinung	k1gInSc1	Massenerscheinung
der	drát	k5eAaImRp2nS	drát
Gegenwart	Gegenwart	k1gInSc1	Gegenwart
(	(	kIx(	(
<g/>
Sebevražda	sebevražda	k1gFnSc1	sebevražda
jako	jako	k8xC	jako
masový	masový	k2eAgInSc1d1	masový
sociální	sociální	k2eAgInSc1d1	sociální
jev	jev	k1gInSc1	jev
přítomnosti	přítomnost	k1gFnSc2	přítomnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vídeň	Vídeň	k1gFnSc1	Vídeň
1881	[number]	k4	1881
<g/>
.	.	kIx.	.
dostupné	dostupný	k2eAgInPc1d1	dostupný
onlineSebevražda	onlineSebevraždo	k1gNnSc2	onlineSebevraždo
hromadným	hromadný	k2eAgInSc7d1	hromadný
jevem	jev	k1gInSc7	jev
společenským	společenský	k2eAgInSc7d1	společenský
moderní	moderní	k2eAgFnSc2d1	moderní
osvěty	osvěta	k1gFnSc2	osvěta
<g/>
,	,	kIx,	,
ÚTGM	ÚTGM	kA	ÚTGM
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2002	[number]	k4	2002
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
4	[number]	k4	4
<g/>
.	.	kIx.	.
č.	č.	k?	č.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
O	o	k7c6	o
hypnotismu	hypnotismus	k1gInSc6	hypnotismus
<g/>
,	,	kIx,	,
magnetismu	magnetismus	k1gInSc6	magnetismus
zvířecím	zvířecí	k2eAgInSc6d1	zvířecí
<g/>
,	,	kIx,	,
Vídeň	Vídeň	k1gFnSc1	Vídeň
1880	[number]	k4	1880
dostupné	dostupný	k2eAgFnSc6d1	dostupná
online	onlinout	k5eAaPmIp3nS	onlinout
Theorie	theorie	k1gFnSc1	theorie
dějin	dějiny	k1gFnPc2	dějiny
podle	podle	k7c2	podle
zásad	zásada	k1gFnPc2	zásada
T.	T.	kA	T.
H.	H.	kA	H.
Bucklea	Bucklea	k1gMnSc1	Bucklea
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
(	(	kIx(	(
<g/>
1883	[number]	k4	1883
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dostupné	dostupný	k2eAgFnPc4d1	dostupná
online	onlinout	k5eAaPmIp3nS	onlinout
Blaise	Blaise	k1gFnSc1	Blaise
Pascal	pascal	k1gInSc1	pascal
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
život	život	k1gInSc1	život
a	a	k8xC	a
filosofie	filosofie	k1gFnSc1	filosofie
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
1883	[number]	k4	1883
<g/>
,	,	kIx,	,
dostupné	dostupný	k2eAgFnPc4d1	dostupná
online	onlinout	k5eAaPmIp3nS	onlinout
Blaise	Blaise	k1gFnSc1	Blaise
Pascal	pascal	k1gInSc1	pascal
a	a	k8xC	a
Humeova	Humeův	k2eAgFnSc1d1	Humeova
skepse	skepse	k1gFnSc1	skepse
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
(	(	kIx(	(
<g/>
1884	[number]	k4	1884
<g/>
)	)	kIx)	)
O	o	k7c6	o
studiu	studio	k1gNnSc6	studio
děl	dělo	k1gNnPc2	dělo
básnických	básnický	k2eAgInPc2d1	básnický
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1884	[number]	k4	1884
<g/>
,	,	kIx,	,
1924	[number]	k4	1924
dostupné	dostupný	k2eAgNnSc4d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
Základové	základový	k2eAgFnPc4d1	základová
konkrétné	konkrétné	k?	konkrétné
logiky	logika	k1gFnSc2	logika
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1885	[number]	k4	1885
<g/>
,	,	kIx,	,
dostupné	dostupný	k2eAgFnPc4d1	dostupná
online	onlinout	k5eAaPmIp3nS	onlinout
Spor	spor	k1gInSc1	spor
o	o	k7c4	o
Kanta	Kant	k1gMnSc4	Kant
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g />
.	.	kIx.	.
</s>
<s>
1885	[number]	k4	1885
<g/>
,	,	kIx,	,
1924	[number]	k4	1924
Slovanské	slovanský	k2eAgFnPc1d1	Slovanská
studie	studie	k1gFnPc1	studie
–	–	k?	–
Slavjanofilství	slavjanofilství	k1gNnSc1	slavjanofilství
I.V.	I.V.	k1gFnSc2	I.V.
Kirjejevského	Kirjejevský	k2eAgNnSc2d1	Kirjejevský
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1885	[number]	k4	1885
O	o	k7c6	o
studiu	studio	k1gNnSc6	studio
děl	dělo	k1gNnPc2	dělo
básnických	básnický	k2eAgFnPc2d1	básnická
II	II	kA	II
–	–	k?	–
Příspěvky	příspěvek	k1gInPc1	příspěvek
k	k	k7c3	k
estetickému	estetický	k2eAgInSc3d1	estetický
rozboru	rozbor	k1gInSc3	rozbor
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1886	[number]	k4	1886
Náčrt	náčrt	k1gInSc1	náčrt
sociologického	sociologický	k2eAgInSc2d1	sociologický
rozboru	rozbor	k1gInSc2	rozbor
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1886	[number]	k4	1886
Versuch	Versuch	k1gMnSc1	Versuch
einer	einer	k1gMnSc1	einer
concreten	concreten	k2eAgMnSc1d1	concreten
Logik	logik	k1gMnSc1	logik
(	(	kIx(	(
<g/>
Pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
concretní	concretní	k2eAgFnSc4d1	concretní
logiku	logika	k1gFnSc4	logika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g />
.	.	kIx.	.
</s>
<s>
1887	[number]	k4	1887
<g/>
Pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
konkrétní	konkrétní	k2eAgFnSc4d1	konkrétní
logiku	logika	k1gFnSc4	logika
<g/>
,	,	kIx,	,
třídění	třídění	k1gNnSc1	třídění
a	a	k8xC	a
soustava	soustava	k1gFnSc1	soustava
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
MÚ	MÚ	kA	MÚ
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2001	[number]	k4	2001
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
č.	č.	k?	č.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
Základové	základový	k2eAgFnSc2d1	základová
konkretné	konkretný	k2eAgFnSc2d1	konkretný
logiky	logika	k1gFnSc2	logika
<g/>
,	,	kIx,	,
třídění	třídění	k1gNnSc1	třídění
a	a	k8xC	a
soustava	soustava	k1gFnSc1	soustava
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
MÚ	MÚ	kA	MÚ
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2001	[number]	k4	2001
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
č.	č.	k?	č.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Několik	několik	k4yIc1	několik
myšlenek	myšlenka	k1gFnPc2	myšlenka
o	o	k7c6	o
úkolech	úkol	k1gInPc6	úkol
českého	český	k2eAgNnSc2d1	české
studentstva	studentstvo	k1gNnSc2	studentstvo
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1889	[number]	k4	1889
J.	J.	kA	J.
A.	A.	kA	A.
Komenský	Komenský	k1gMnSc1	Komenský
Praha	Praha	k1gFnSc1	Praha
1892	[number]	k4	1892
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1920	[number]	k4	1920
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
O	o	k7c6	o
Kollárovi	Kollár	k1gMnSc6	Kollár
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1893	[number]	k4	1893
Česká	český	k2eAgFnSc1d1	Česká
otázka	otázka	k1gFnSc1	otázka
<g/>
.	.	kIx.	.
</s>
<s>
Snahy	snaha	k1gFnPc1	snaha
a	a	k8xC	a
tužby	tužba	k1gFnPc1	tužba
národního	národní	k2eAgNnSc2d1	národní
obrození	obrození	k1gNnSc2	obrození
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
1895	[number]	k4	1895
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1908	[number]	k4	1908
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
a	a	k8xC	a
jako	jako	k9	jako
e-kniha	eniha	k1gFnSc1	e-kniha
<g/>
.	.	kIx.	.
</s>
<s>
Naše	náš	k3xOp1gFnSc1	náš
nynější	nynější	k2eAgFnSc1d1	nynější
krise	kris	k1gInSc5	kris
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
1895	[number]	k4	1895
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
a	a	k8xC	a
jako	jako	k9	jako
e-kniha	eniha	k1gFnSc1	e-kniha
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
<g/>
.	.	kIx.	.
</s>
<s>
Naše	náš	k3xOp1gNnSc1	náš
obrození	obrození	k1gNnSc1	obrození
a	a	k8xC	a
naše	náš	k3xOp1gFnSc1	náš
reformace	reformace	k1gFnSc1	reformace
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
1896	[number]	k4	1896
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1908	[number]	k4	1908
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
168	[number]	k4	168
s.	s.	k?	s.
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
900095	[number]	k4	900095
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Kanzelsberger	Kanzelsberger	k1gMnSc1	Kanzelsberger
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Praha	Praha	k1gFnSc1	Praha
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
a	a	k8xC	a
jako	jako	k9	jako
e-kniha	eniha	k1gFnSc1	e-kniha
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
otázka	otázka	k1gFnSc1	otázka
–	–	k?	–
Naše	náš	k3xOp1gFnSc1	náš
nynější	nynější	k2eAgFnSc1d1	nynější
krize	krize	k1gFnSc1	krize
–	–	k?	–
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
<g/>
,	,	kIx,	,
ÚTGM	ÚTGM	kA	ÚTGM
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2000	[number]	k4	2000
(	(	kIx(	(
<g/>
Česká	český	k2eAgFnSc1d1	Česká
otázka	otázka	k1gFnSc1	otázka
8	[number]	k4	8
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Naše	náš	k3xOp1gFnSc1	náš
nynější	nynější	k2eAgFnSc1d1	nynější
krize	krize	k1gFnSc1	krize
7	[number]	k4	7
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
9	[number]	k4	9
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Karel	Karel	k1gMnSc1	Karel
Havlíček	Havlíček	k1gMnSc1	Havlíček
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1896	[number]	k4	1896
<g/>
Karel	Karel	k1gMnSc1	Karel
Havlíček	Havlíček	k1gMnSc1	Havlíček
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1904	[number]	k4	1904
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
oprav	oprava	k1gFnPc2	oprava
<g/>
.	.	kIx.	.
a	a	k8xC	a
dopln	dopln	k1gInSc1	dopln
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
<g/>
Karel	Karel	k1gMnSc1	Karel
Havlíček	Havlíček	k1gMnSc1	Havlíček
<g/>
,	,	kIx,	,
snahy	snaha	k1gFnPc1	snaha
a	a	k8xC	a
tužby	tužba	k1gFnPc1	tužba
politického	politický	k2eAgNnSc2d1	politické
probuzení	probuzení	k1gNnSc2	probuzení
<g/>
,	,	kIx,	,
ÚTGM	ÚTGM	kA	ÚTGM
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1996	[number]	k4	1996
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Moderní	moderní	k2eAgMnSc1d1	moderní
člověk	člověk	k1gMnSc1	člověk
a	a	k8xC	a
náboženství	náboženství	k1gNnSc1	náboženství
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1896	[number]	k4	1896
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
a	a	k8xC	a
jako	jako	k8xC	jako
e-kniha	eniha	k1gMnSc1	e-kniha
<g/>
.	.	kIx.	.
<g/>
Moderní	moderní	k2eAgMnSc1d1	moderní
člověk	člověk	k1gMnSc1	člověk
a	a	k8xC	a
náboženství	náboženství	k1gNnSc1	náboženství
<g/>
,	,	kIx,	,
MÚ	MÚ	kA	MÚ
AV	AV	kA	AV
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2000	[number]	k4	2000
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
O	o	k7c6	o
nynější	nynější	k2eAgFnSc6d1	nynější
evoluční	evoluční	k2eAgFnSc6d1	evoluční
filosofii	filosofie	k1gFnSc6	filosofie
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1896	[number]	k4	1896
O	o	k7c6	o
národech	národ	k1gInPc6	národ
jako	jako	k8xC	jako
úvod	úvod	k1gInSc4	úvod
do	do	k7c2	do
filosofie	filosofie	k1gFnSc2	filosofie
dějin	dějiny	k1gFnPc2	dějiny
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1898	[number]	k4	1898
Jak	jak	k6eAd1	jak
pracovat	pracovat	k5eAaImF	pracovat
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
přednášky	přednáška	k1gFnPc4	přednáška
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1898	[number]	k4	1898
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1926	[number]	k4	1926
Palackého	Palackého	k2eAgFnSc1d1	Palackého
idea	idea	k1gFnSc1	idea
národa	národ	k1gInSc2	národ
českého	český	k2eAgInSc2d1	český
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
roku	rok	k1gInSc2	rok
1898	[number]	k4	1898
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
1912	[number]	k4	1912
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1926	[number]	k4	1926
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Hus	Hus	k1gMnSc1	Hus
českému	český	k2eAgNnSc3d1	české
studentstvu	studentstvo	k1gNnSc3	studentstvo
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1899	[number]	k4	1899
<g/>
,	,	kIx,	,
1919	[number]	k4	1919
Mnohoženství	mnohoženství	k1gNnSc2	mnohoženství
a	a	k8xC	a
jednoženství	jednoženství	k1gNnSc2	jednoženství
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1899	[number]	k4	1899
Nutnost	nutnost	k1gFnSc4	nutnost
revidovati	revidovat	k5eAaImF	revidovat
proces	proces	k1gInSc4	proces
polenský	polenský	k2eAgInSc4d1	polenský
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1899	[number]	k4	1899
Význam	význam	k1gInSc1	význam
procesu	proces	k1gInSc2	proces
polenského	polenský	k2eAgInSc2d1	polenský
pro	pro	k7c4	pro
pověru	pověra	k1gFnSc4	pověra
rituelní	rituelní	k2eAgFnSc1d1	rituelní
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1900	[number]	k4	1900
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Vídeň	Vídeň	k1gFnSc1	Vídeň
1900	[number]	k4	1900
dostupné	dostupný	k2eAgInPc1d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
O	o	k7c6	o
pověře	pověra	k1gFnSc6	pověra
rituelní	rituelní	k2eAgFnSc6d1	rituelní
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1900	[number]	k4	1900
Právo	právo	k1gNnSc4	právo
přirozené	přirozený	k2eAgNnSc4d1	přirozené
a	a	k8xC	a
historické	historický	k2eAgNnSc4d1	historické
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1900	[number]	k4	1900
dostupné	dostupný	k2eAgFnSc6d1	dostupná
online	onlinout	k5eAaPmIp3nS	onlinout
Hospodářský	hospodářský	k2eAgInSc1d1	hospodářský
a	a	k8xC	a
sociální	sociální	k2eAgInSc1d1	sociální
význam	význam	k1gInSc1	význam
vzdělání	vzdělání	k1gNnSc2	vzdělání
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1900	[number]	k4	1900
Osm	osm	k4xCc1	osm
hodin	hodina	k1gFnPc2	hodina
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1900	[number]	k4	1900
<g/>
,	,	kIx,	,
1905	[number]	k4	1905
O	o	k7c6	o
oprávněnosti	oprávněnost	k1gFnSc6	oprávněnost
a	a	k8xC	a
prospěšnosti	prospěšnost	k1gFnSc6	prospěšnost
osmihodinové	osmihodinový	k2eAgFnSc2d1	osmihodinová
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1900	[number]	k4	1900
<g/>
,	,	kIx,	,
1905	[number]	k4	1905
<g/>
,	,	kIx,	,
1924	[number]	k4	1924
Rukověť	rukověť	k1gFnSc1	rukověť
sociologie	sociologie	k1gFnSc1	sociologie
<g/>
.	.	kIx.	.
</s>
<s>
Podstata	podstata	k1gFnSc1	podstata
a	a	k8xC	a
methoda	methoda	k1gFnSc1	methoda
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1900	[number]	k4	1900
<g/>
,	,	kIx,	,
1901	[number]	k4	1901
Naše	náš	k3xOp1gFnSc1	náš
politická	politický	k2eAgFnSc1d1	politická
situace	situace	k1gFnSc1	situace
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1901	[number]	k4	1901
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Augustin	Augustin	k1gMnSc1	Augustin
Smetana	Smetana	k1gMnSc1	Smetana
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
filosofie	filosofie	k1gFnSc1	filosofie
sociální	sociální	k2eAgFnSc1d1	sociální
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1901	[number]	k4	1901
Ideály	ideál	k1gInPc1	ideál
humanitní	humanitní	k2eAgInPc1d1	humanitní
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1901	[number]	k4	1901
<g/>
,	,	kIx,	,
1919	[number]	k4	1919
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
a	a	k8xC	a
jako	jako	k9	jako
e-kniha	eniha	k1gFnSc1	e-kniha
<g/>
.	.	kIx.	.
</s>
<s>
Otázka	otázka	k1gFnSc1	otázka
sociální	sociální	k2eAgFnSc1d1	sociální
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1896	[number]	k4	1896
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
1898	[number]	k4	1898
<g/>
Otázka	otázka	k1gFnSc1	otázka
sociální	sociální	k2eAgFnSc1d1	sociální
<g/>
:	:	kIx,	:
základy	základ	k1gInPc1	základ
marxismu	marxismus	k1gInSc2	marxismus
filosofické	filosofický	k2eAgFnSc2d1	filosofická
a	a	k8xC	a
sociologické	sociologický	k2eAgFnSc2d1	sociologická
I.	I.	kA	I.
a	a	k8xC	a
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
MÚ	MÚ	kA	MÚ
AV	AV	kA	AV
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2000	[number]	k4	2000
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
č.	č.	k?	č.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
a	a	k8xC	a
jako	jako	k8xC	jako
e-kniha	eniha	k1gMnSc1	e-kniha
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
;	;	kIx,	;
2	[number]	k4	2
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
;	;	kIx,	;
3	[number]	k4	3
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
;	;	kIx,	;
4	[number]	k4	4
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Los	los	k1gMnSc1	los
von	von	k1gInSc4	von
Rom	Rom	k1gMnSc1	Rom
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1902	[number]	k4	1902
Desorganisace	Desorganisace	k1gFnPc4	Desorganisace
mladočeské	mladočeský	k2eAgFnSc2d1	mladočeská
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1903	[number]	k4	1903
Organisujme	organisovat	k5eAaBmRp1nP	organisovat
se	se	k3xPyFc4	se
ku	k	k7c3	k
práci	práce	k1gFnSc3	práce
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1903	[number]	k4	1903
V	v	k7c6	v
boji	boj	k1gInSc6	boj
o	o	k7c4	o
náboženství	náboženství	k1gNnSc4	náboženství
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1904	[number]	k4	1904
O	o	k7c6	o
svobodě	svoboda	k1gFnSc6	svoboda
náboženské	náboženský	k2eAgFnSc2d1	náboženská
a	a	k8xC	a
volnosti	volnost	k1gFnSc2	volnost
přesvědčení	přesvědčení	k1gNnSc2	přesvědčení
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1904	[number]	k4	1904
Národnostní	národnostní	k2eAgFnSc2d1	národnostní
filosofie	filosofie	k1gFnSc2	filosofie
doby	doba	k1gFnSc2	doba
nejnovější	nový	k2eAgInSc4d3	nejnovější
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g />
.	.	kIx.	.
</s>
<s>
1905	[number]	k4	1905
Problém	problém	k1gInSc1	problém
malého	malý	k2eAgInSc2d1	malý
národa	národ	k1gInSc2	národ
<g/>
,	,	kIx,	,
Kroměříž	Kroměříž	k1gFnSc1	Kroměříž
1905	[number]	k4	1905
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1905	[number]	k4	1905
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1937	[number]	k4	1937
(	(	kIx(	(
<g/>
první	první	k4xOgNnSc1	první
a	a	k8xC	a
druhé	druhý	k4xOgNnSc1	druhý
vydání	vydání	k1gNnSc1	vydání
knižní	knižní	k2eAgFnSc2d1	knižní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1947	[number]	k4	1947
(	(	kIx(	(
<g/>
třetí	třetí	k4xOgNnSc4	třetí
vydání	vydání	k1gNnSc4	vydání
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc6	vydání
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
900073	[number]	k4	900073
<g/>
-	-	kIx~	-
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
Vydavatelství	vydavatelství	k1gNnSc1	vydavatelství
Neutralita	neutralita	k1gFnSc1	neutralita
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1990	[number]	k4	1990
Přehled	přehled	k1gInSc1	přehled
nejnovější	nový	k2eAgFnSc2d3	nejnovější
filosofie	filosofie	k1gFnSc2	filosofie
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1905	[number]	k4	1905
dostupné	dostupný	k2eAgInPc1d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
Politická	politický	k2eAgFnSc1d1	politická
situace	situace	k1gFnSc1	situace
<g/>
,	,	kIx,	,
poznámky	poznámka	k1gFnPc1	poznámka
ku	k	k7c3	k
"	"	kIx"	"
<g/>
Poznámkám	poznámka	k1gFnPc3	poznámka
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Kramáře	kramář	k1gMnSc4	kramář
<g/>
"	"	kIx"	"
<g/>
-reforma	eformum	k1gNnSc2	-reformum
volebního	volební	k2eAgInSc2d1	volební
zákona	zákon	k1gInSc2	zákon
a	a	k8xC	a
rozpuštění	rozpuštění	k1gNnSc1	rozpuštění
říšské	říšský	k2eAgFnSc2d1	říšská
rady	rada	k1gFnSc2	rada
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1905	[number]	k4	1905
<g />
.	.	kIx.	.
</s>
<s>
Politika	politika	k1gFnSc1	politika
vědou	věda	k1gFnSc7	věda
a	a	k8xC	a
uměním	umění	k1gNnSc7	umění
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1906	[number]	k4	1906
Zrcadlo	zrcadlo	k1gNnSc4	zrcadlo
katechetům	katecheta	k1gMnPc3	katecheta
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1906	[number]	k4	1906
Inteligence	inteligence	k1gFnPc1	inteligence
a	a	k8xC	a
náboženství	náboženství	k1gNnSc1	náboženství
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1906	[number]	k4	1906
<g/>
,	,	kIx,	,
1907	[number]	k4	1907
Americké	americký	k2eAgFnPc1d1	americká
přednášky	přednáška	k1gFnPc1	přednáška
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1907	[number]	k4	1907
<g/>
,	,	kIx,	,
1922	[number]	k4	1922
Freie	Freie	k1gFnPc4	Freie
wissenschaftliche	wissenschaftlich	k1gInSc2	wissenschaftlich
und	und	k?	und
kirchlich	kirchlich	k1gMnSc1	kirchlich
gebundene	gebunden	k1gInSc5	gebunden
Weltantschaung	Weltantschaung	k1gInSc1	Weltantschaung
und	und	k?	und
Lebensauffassung	Lebensauffassung	k1gInSc4	Lebensauffassung
<g/>
,	,	kIx,	,
Wien	Wien	k1gInSc4	Wien
1908	[number]	k4	1908
Věda	věda	k1gFnSc1	věda
a	a	k8xC	a
církev	církev	k1gFnSc1	církev
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1908	[number]	k4	1908
Za	za	k7c4	za
svobodu	svoboda	k1gFnSc4	svoboda
svědomí	svědomí	k1gNnSc2	svědomí
a	a	k8xC	a
učení	učení	k1gNnSc2	učení
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1908	[number]	k4	1908
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Student	student	k1gMnSc1	student
a	a	k8xC	a
politika	politika	k1gFnSc1	politika
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1909	[number]	k4	1909
<g/>
,	,	kIx,	,
1923	[number]	k4	1923
Tak	tak	k9	tak
zvaný	zvaný	k2eAgInSc1d1	zvaný
velezrádný	velezrádný	k2eAgInSc1d1	velezrádný
proces	proces	k1gInSc1	proces
v	v	k7c6	v
Záhřebě	Záhřeb	k1gInSc6	Záhřeb
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1909	[number]	k4	1909
Žena	žena	k1gFnSc1	žena
u	u	k7c2	u
Ježíše	Ježíš	k1gMnSc2	Ježíš
a	a	k8xC	a
Pavla	Pavel	k1gMnSc2	Pavel
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1910	[number]	k4	1910
Demokratismus	demokratismus	k1gInSc1	demokratismus
v	v	k7c6	v
politice	politika	k1gFnSc6	politika
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1912	[number]	k4	1912
Nesnáze	nesnáz	k1gFnSc2	nesnáz
demokracie	demokracie	k1gFnSc1	demokracie
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1913	[number]	k4	1913
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Přechod	přechod	k1gInSc1	přechod
ze	z	k7c2	z
střední	střední	k2eAgFnSc2d1	střední
školy	škola	k1gFnSc2	škola
na	na	k7c4	na
školu	škola	k1gFnSc4	škola
vysokou	vysoká	k1gFnSc4	vysoká
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1913	[number]	k4	1913
<g/>
,	,	kIx,	,
1919	[number]	k4	1919
Válka	válka	k1gFnSc1	válka
a	a	k8xC	a
hospodářství	hospodářství	k1gNnSc1	hospodářství
<g/>
,	,	kIx,	,
hospodářský	hospodářský	k2eAgInSc1d1	hospodářský
boj	boj	k1gInSc1	boj
Anglie	Anglie	k1gFnSc2	Anglie
a	a	k8xC	a
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1914	[number]	k4	1914
Problém	problém	k1gInSc4	problém
malých	malý	k2eAgInPc2d1	malý
národů	národ	k1gInPc2	národ
a	a	k8xC	a
evropská	evropský	k2eAgFnSc1d1	Evropská
krise	kris	k1gInSc5	kris
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
1915	[number]	k4	1915
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Berlín	Berlín	k1gInSc1	Berlín
1922	[number]	k4	1922
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1926	[number]	k4	1926
Slované	Slovan	k1gMnPc1	Slovan
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
Svět	svět	k1gInSc1	svět
a	a	k8xC	a
Slované	Slovan	k1gMnPc1	Slovan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
–	–	k?	–
Londýn	Londýn	k1gInSc1	Londýn
1916	[number]	k4	1916
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
a	a	k8xC	a
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1919	[number]	k4	1919
Spomienky	Spomienka	k1gFnPc1	Spomienka
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
1917	[number]	k4	1917
The	The	k1gMnSc5	The
New	New	k1gMnSc5	New
Europe	Europ	k1gMnSc5	Europ
<g/>
,	,	kIx,	,
London	London	k1gMnSc1	London
1918	[number]	k4	1918
Rusko	Rusko	k1gNnSc1	Rusko
a	a	k8xC	a
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Rusko	Rusko	k1gNnSc1	Rusko
a	a	k8xC	a
Evropa	Evropa	k1gFnSc1	Evropa
a	a	k8xC	a
svět	svět	k1gInSc1	svět
(	(	kIx(	(
<g/>
od	od	k7c2	od
1915	[number]	k4	1915
řada	řada	k1gFnSc1	řada
vydání	vydání	k1gNnSc2	vydání
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
řečech	řeč	k1gFnPc6	řeč
<g/>
)	)	kIx)	)
<g/>
Rusko	Rusko	k1gNnSc1	Rusko
a	a	k8xC	a
Evropa	Evropa	k1gFnSc1	Evropa
I.	I.	kA	I.
–	–	k?	–
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
ÚTGM	ÚTGM	kA	ÚTGM
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1995	[number]	k4	1995
<g/>
–	–	k?	–
<g/>
96	[number]	k4	96
(	(	kIx(	(
<g/>
rev	rev	k?	rev
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Budoucí	budoucí	k2eAgFnPc1d1	budoucí
Čechy	Čechy	k1gFnPc1	Čechy
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1919	[number]	k4	1919
dostupné	dostupný	k2eAgInPc1d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
Nová	nový	k2eAgFnSc1d1	nová
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
:	:	kIx,	:
Stanovisko	stanovisko	k1gNnSc1	stanovisko
slovanské	slovanský	k2eAgNnSc1d1	slovanské
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1920	[number]	k4	1920
Dostupné	dostupný	k2eAgInPc1d1	dostupný
online	onlin	k1gMnSc5	onlin
a	a	k8xC	a
jako	jako	k9	jako
e-kniha	eniha	k1gFnSc1	e-kniha
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
bolševictví	bolševictví	k1gNnSc6	bolševictví
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1921	[number]	k4	1921
Das	Das	k1gFnSc1	Das
Neue	Neue	k1gFnSc1	Neue
Europa	Europa	k1gFnSc1	Europa
<g/>
,	,	kIx,	,
C.A.	C.A.	k1gFnSc1	C.A.
<g/>
Schwestschke	Schwestschke	k1gFnSc1	Schwestschke
uhd	uhd	k?	uhd
Sohn	Sohna	k1gFnPc2	Sohna
<g/>
/	/	kIx~	/
<g/>
Verlagsbuchhandlung	Verlagsbuchhandlunga	k1gFnPc2	Verlagsbuchhandlunga
<g/>
,	,	kIx,	,
Berlin	berlina	k1gFnPc2	berlina
1922	[number]	k4	1922
The	The	k1gFnPc2	The
Slaws	Slaws	k1gInSc1	Slaws
after	after	k1gMnSc1	after
the	the	k?	the
War	War	k1gMnSc1	War
<g/>
,	,	kIx,	,
London	London	k1gMnSc1	London
1922	[number]	k4	1922
O	o	k7c6	o
ženě	žena	k1gFnSc6	žena
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1929	[number]	k4	1929
Slované	Slovan	k1gMnPc1	Slovan
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1923	[number]	k4	1923
"	"	kIx"	"
<g/>
Les	les	k1gInSc1	les
Slaves	Slavesa	k1gFnPc2	Slavesa
aprés	aprésa	k1gFnPc2	aprésa
la	la	k1gNnSc2	la
guerre	guerr	k1gMnSc5	guerr
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Paris	Paris	k1gMnSc1	Paris
1923	[number]	k4	1923
"	"	kIx"	"
<g/>
Slowienje	Slowienj	k1gInSc2	Slowienj
po	po	k7c6	po
wójnie	wójnie	k1gFnSc1	wójnie
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Lublaň	Lublaň	k1gFnSc1	Lublaň
1923	[number]	k4	1923
"	"	kIx"	"
<g/>
Sloviane	Slovian	k1gMnSc5	Slovian
po	po	k7c6	po
wojnie	wojnie	k1gFnSc1	wojnie
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Varšava	Varšava	k1gFnSc1	Varšava
1924	[number]	k4	1924
Světová	světový	k2eAgFnSc1d1	světová
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
války	válka	k1gFnSc2	válka
a	a	k8xC	a
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
1914	[number]	k4	1914
<g/>
–	–	k?	–
<g/>
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1925	[number]	k4	1925
(	(	kIx(	(
<g/>
dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
1936	[number]	k4	1936
(	(	kIx(	(
<g/>
dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1938	[number]	k4	1938
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
a	a	k8xC	a
jako	jako	k9	jako
e-kniha	eniha	k1gFnSc1	e-kniha
<g/>
.	.	kIx.	.
<g/>
Světová	světový	k2eAgFnSc1d1	světová
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
války	válka	k1gFnSc2	válka
a	a	k8xC	a
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
1914	[number]	k4	1914
<g/>
–	–	k?	–
<g/>
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
ÚTGM	ÚTGM	kA	ÚTGM
/	/	kIx~	/
MÚ	MÚ	kA	MÚ
AV	AV	kA	AV
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-86142-17-5	[number]	k4	80-86142-17-5
/	/	kIx~	/
ISBN	ISBN	kA	ISBN
80-86495-27-2	[number]	k4	80-86495-27-2
</s>
