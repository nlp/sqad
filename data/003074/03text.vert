<p>
<s>
Prvočíslo	prvočíslo	k1gNnSc1	prvočíslo
je	být	k5eAaImIp3nS	být
přirozené	přirozený	k2eAgNnSc1d1	přirozené
číslo	číslo	k1gNnSc1	číslo
větší	veliký	k2eAgFnSc2d2	veliký
než	než	k8xS	než
1	[number]	k4	1
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
dělitelné	dělitelný	k2eAgNnSc1d1	dělitelné
jen	jen	k6eAd1	jen
dvěma	dva	k4xCgInPc7	dva
děliteli	dělitel	k1gInPc7	dělitel
<g/>
:	:	kIx,	:
jedničkou	jednička	k1gFnSc7	jednička
a	a	k8xC	a
samo	sám	k3xTgNnSc1	sám
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
</s>
<s>
Jednička	jednička	k1gFnSc1	jednička
není	být	k5eNaImIp3nS	být
prvočíslo	prvočíslo	k1gNnSc4	prvočíslo
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
nemá	mít	k5eNaImIp3nS	mít
dva	dva	k4xCgInPc4	dva
dělitele	dělitel	k1gInPc4	dělitel
<g/>
.	.	kIx.	.
</s>
<s>
Přirozená	přirozený	k2eAgNnPc1d1	přirozené
čísla	číslo	k1gNnPc1	číslo
větší	veliký	k2eAgNnPc1d2	veliký
než	než	k8xS	než
jedna	jeden	k4xCgFnSc1	jeden
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nejsou	být	k5eNaImIp3nP	být
prvočísly	prvočíslo	k1gNnPc7	prvočíslo
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
složená	složený	k2eAgNnPc1d1	složené
čísla	číslo	k1gNnPc1	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgNnSc7	první
prvočíslem	prvočíslo	k1gNnSc7	prvočíslo
je	být	k5eAaImIp3nS	být
číslo	číslo	k1gNnSc1	číslo
2	[number]	k4	2
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgNnSc7d1	jediné
sudým	sudý	k2eAgNnSc7d1	sudé
prvočíslem	prvočíslo	k1gNnSc7	prvočíslo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Příklad	příklad	k1gInSc1	příklad
==	==	k?	==
</s>
</p>
<p>
<s>
Číslo	číslo	k1gNnSc1	číslo
13	[number]	k4	13
má	mít	k5eAaImIp3nS	mít
při	při	k7c6	při
dělení	dělení	k1gNnSc6	dělení
dvěma	dva	k4xCgFnPc7	dva
zbytek	zbytek	k1gInSc1	zbytek
1	[number]	k4	1
<g/>
,	,	kIx,	,
při	při	k7c6	při
dělení	dělení	k1gNnSc6	dělení
3	[number]	k4	3
zbytek	zbytek	k1gInSc1	zbytek
1	[number]	k4	1
<g/>
,	,	kIx,	,
při	při	k7c6	při
dělení	dělení	k1gNnSc6	dělení
pěti	pět	k4xCc2	pět
zbytek	zbytek	k1gInSc4	zbytek
3	[number]	k4	3
atd.	atd.	kA	atd.
Říkáme	říkat	k5eAaImIp1nP	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
těmito	tento	k3xDgMnPc7	tento
čísly	čísnout	k5eAaPmAgFnP	čísnout
nedělitelné	dělitelný	k2eNgFnPc1d1	nedělitelná
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
při	při	k7c6	při
dělení	dělení	k1gNnSc6	dělení
1	[number]	k4	1
a	a	k8xC	a
13	[number]	k4	13
je	být	k5eAaImIp3nS	být
zbytek	zbytek	k1gInSc4	zbytek
0	[number]	k4	0
(	(	kIx(	(
<g/>
dělitelné	dělitelný	k2eAgNnSc1d1	dělitelné
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	on	k3xPp3gNnSc4	on
13	[number]	k4	13
prvočíslo	prvočíslo	k1gNnSc4	prvočíslo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Číslo	číslo	k1gNnSc1	číslo
24	[number]	k4	24
je	být	k5eAaImIp3nS	být
dělitelné	dělitelný	k2eAgNnSc1d1	dělitelné
čísly	číslo	k1gNnPc7	číslo
1	[number]	k4	1
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
,	,	kIx,	,
8	[number]	k4	8
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
,	,	kIx,	,
24	[number]	k4	24
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
proto	proto	k8xC	proto
prvočíslem	prvočíslo	k1gNnSc7	prvočíslo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
složeným	složený	k2eAgNnSc7d1	složené
číslem	číslo	k1gNnSc7	číslo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Prvočíselnost	Prvočíselnost	k1gFnSc1	Prvočíselnost
jedničky	jednička	k1gFnSc2	jednička
==	==	k?	==
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
říká	říkat	k5eAaImIp3nS	říkat
základní	základní	k2eAgFnSc1d1	základní
věta	věta	k1gFnSc1	věta
aritmetiky	aritmetika	k1gFnSc2	aritmetika
<g/>
,	,	kIx,	,
každé	každý	k3xTgNnSc1	každý
přirozené	přirozený	k2eAgNnSc1d1	přirozené
číslo	číslo	k1gNnSc1	číslo
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
rozložit	rozložit	k5eAaPmF	rozložit
na	na	k7c4	na
právě	právě	k6eAd1	právě
jeden	jeden	k4xCgInSc4	jeden
prvočíselný	prvočíselný	k2eAgInSc4d1	prvočíselný
součin	součin	k1gInSc4	součin
(	(	kIx(	(
<g/>
např.	např.	kA	např.
12	[number]	k4	12
=	=	kIx~	=
2	[number]	k4	2
<g/>
×	×	k?	×
<g/>
2	[number]	k4	2
<g/>
×	×	k?	×
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
jednička	jednička	k1gFnSc1	jednička
zahrnuta	zahrnout	k5eAaPmNgFnS	zahrnout
do	do	k7c2	do
množiny	množina	k1gFnSc2	množina
prvočísel	prvočíslo	k1gNnPc2	prvočíslo
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
by	by	kYmCp3nS	by
takových	takový	k3xDgMnPc2	takový
rozkladů	rozklad	k1gInPc2	rozklad
vždy	vždy	k6eAd1	vždy
nekonečně	konečně	k6eNd1	konečně
mnoho	mnoho	k6eAd1	mnoho
(	(	kIx(	(
<g/>
12	[number]	k4	12
=	=	kIx~	=
2	[number]	k4	2
<g/>
×	×	k?	×
<g/>
2	[number]	k4	2
<g/>
×	×	k?	×
<g/>
3	[number]	k4	3
=	=	kIx~	=
1	[number]	k4	1
<g/>
×	×	k?	×
<g/>
2	[number]	k4	2
<g/>
×	×	k?	×
<g/>
2	[number]	k4	2
<g/>
×	×	k?	×
<g/>
3	[number]	k4	3
=	=	kIx~	=
1	[number]	k4	1
<g/>
×	×	k?	×
<g/>
1	[number]	k4	1
<g/>
×	×	k?	×
<g/>
2	[number]	k4	2
<g/>
×	×	k?	×
<g/>
2	[number]	k4	2
<g/>
×	×	k?	×
<g/>
3	[number]	k4	3
=	=	kIx~	=
.	.	kIx.	.
<g/>
..	..	k?	..
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
jednička	jednička	k1gFnSc1	jednička
za	za	k7c4	za
prvočíslo	prvočíslo	k1gNnSc4	prvočíslo
nepovažuje	považovat	k5eNaImIp3nS	považovat
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
podmínku	podmínka	k1gFnSc4	podmínka
dělitelnosti	dělitelnost	k1gFnSc2	dělitelnost
pouze	pouze	k6eAd1	pouze
sebou	se	k3xPyFc7	se
samým	samý	k3xTgMnSc7	samý
a	a	k8xC	a
jedničkou	jednička	k1gFnSc7	jednička
splňuje	splňovat	k5eAaImIp3nS	splňovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
obecnějších	obecní	k2eAgFnPc2d2	obecní
teorií	teorie	k1gFnPc2	teorie
jsou	být	k5eAaImIp3nP	být
prvočísla	prvočíslo	k1gNnPc1	prvočíslo
takzvanými	takzvaný	k2eAgInPc7d1	takzvaný
prvočiniteli	prvočinitel	k1gInPc7	prvočinitel
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
jednička	jednička	k1gFnSc1	jednička
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
takzvané	takzvaný	k2eAgFnPc4d1	takzvaná
jednotky	jednotka	k1gFnPc4	jednotka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
p	p	k?	p
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
prvočíslo	prvočíslo	k1gNnSc1	prvočíslo
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
p	p	k?	p
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
dělí	dělit	k5eAaImIp3nS	dělit
součin	součin	k1gInSc1	součin
čísel	číslo	k1gNnPc2	číslo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
b	b	k?	b
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
pak	pak	k6eAd1	pak
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
p	p	k?	p
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
dělí	dělit	k5eAaImIp3nS	dělit
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
nebo	nebo	k8xC	nebo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
p	p	k?	p
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
dělí	dělit	k5eAaImIp3nS	dělit
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
b	b	k?	b
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
Každé	každý	k3xTgNnSc1	každý
složené	složený	k2eAgNnSc1d1	složené
číslo	číslo	k1gNnSc1	číslo
lze	lze	k6eAd1	lze
jednoznačně	jednoznačně	k6eAd1	jednoznačně
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
jako	jako	k8xS	jako
součin	součin	k1gInSc1	součin
prvočísel	prvočíslo	k1gNnPc2	prvočíslo
<g/>
.	.	kIx.	.
</s>
<s>
Proces	proces	k1gInSc1	proces
rozkladu	rozklad	k1gInSc2	rozklad
čísla	číslo	k1gNnSc2	číslo
na	na	k7c4	na
jeho	jeho	k3xOp3gMnPc4	jeho
prvočíselné	prvočíselný	k2eAgMnPc4d1	prvočíselný
činitele	činitel	k1gMnPc4	činitel
(	(	kIx(	(
<g/>
prvočinitele	prvočinitel	k1gMnSc4	prvočinitel
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
faktorizace	faktorizace	k1gFnSc1	faktorizace
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
24	[number]	k4	24
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
24	[number]	k4	24
<g/>
=	=	kIx~	=
<g/>
2	[number]	k4	2
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
3	[number]	k4	3
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
Okruh	okruh	k1gInSc1	okruh
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Z	z	k7c2	z
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
/	/	kIx~	/
</s>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Z	z	k7c2	z
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbb	mathbba	k1gFnPc2	mathbba
{	{	kIx(	{
<g/>
Z	z	k7c2	z
<g/>
}	}	kIx)	}
/	/	kIx~	/
<g/>
n	n	k0	n
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gInSc1	mathbb
{	{	kIx(	{
<g/>
Z	z	k7c2	z
<g/>
}	}	kIx)	}
}}	}}	k?	}}
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
množina	množina	k1gFnSc1	množina
zbytkových	zbytkový	k2eAgFnPc2d1	zbytková
tříd	třída	k1gFnPc2	třída
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
těleso	těleso	k1gNnSc1	těleso
<g/>
,	,	kIx,	,
právě	právě	k9	právě
když	když	k8xS	když
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
prvočíslo	prvočíslo	k1gNnSc1	prvočíslo
<g/>
.	.	kIx.	.
</s>
<s>
Jinak	jinak	k6eAd1	jinak
vyjádřeno	vyjádřit	k5eAaPmNgNnS	vyjádřit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
prvočíslo	prvočíslo	k1gNnSc1	prvočíslo
<g/>
,	,	kIx,	,
právě	právě	k9	právě
když	když	k8xS	když
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
φ	φ	k?	φ
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
varphi	varphi	k1gNnPc3	varphi
(	(	kIx(	(
<g/>
n	n	k0	n
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
n-1	n-1	k4	n-1
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
kde	kde	k6eAd1	kde
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
φ	φ	k?	φ
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
varphi	varphi	k1gNnPc3	varphi
(	(	kIx(	(
<g/>
n	n	k0	n
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
počet	počet	k1gInSc1	počet
invertovatelných	invertovatelný	k2eAgInPc2d1	invertovatelný
prvků	prvek	k1gInPc2	prvek
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Z	z	k7c2	z
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
/	/	kIx~	/
</s>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Z	z	k7c2	z
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbb	mathbba	k1gFnPc2	mathbba
{	{	kIx(	{
<g/>
Z	z	k7c2	z
<g/>
}	}	kIx)	}
/	/	kIx~	/
<g/>
n	n	k0	n
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gInSc1	mathbb
{	{	kIx(	{
<g/>
Z	z	k7c2	z
<g/>
}	}	kIx)	}
}}	}}	k?	}}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
p	p	k?	p
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
prvočíslo	prvočíslo	k1gNnSc4	prvočíslo
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
<s>
<	<	kIx(	<
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
<	<	kIx(	<
</s>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
0	[number]	k4	0
<g/>
<	<	kIx(	<
<g/>
a	a	k8xC	a
<g/>
<	<	kIx(	<
<g/>
p	p	k?	p
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
celé	celý	k2eAgNnSc1d1	celé
číslo	číslo	k1gNnSc1	číslo
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
p	p	k?	p
<g/>
}	}	kIx)	}
<g/>
-	-	kIx~	-
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
dělitelné	dělitelný	k2eAgNnSc1d1	dělitelné
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
p	p	k?	p
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
Malá	malý	k2eAgFnSc1d1	malá
Fermatova	Fermatův	k2eAgFnSc1d1	Fermatova
věta	věta	k1gFnSc1	věta
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
kladné	kladný	k2eAgNnSc4d1	kladné
celé	celý	k2eAgNnSc4d1	celé
číslo	číslo	k1gNnSc4	číslo
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
jedna	jeden	k4xCgFnSc1	jeden
<g/>
,	,	kIx,	,
existuje	existovat	k5eAaImIp3nS	existovat
prvočíslo	prvočíslo	k1gNnSc1	prvočíslo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
p	p	k?	p
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
tak	tak	k9	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
<s>
<	<	kIx(	<
</s>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
<s>
<	<	kIx(	<
</s>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
<	<	kIx(	<
<g/>
p	p	k?	p
<g/>
<	<	kIx(	<
<g/>
2n	[number]	k4	2n
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
Bertrandův	Bertrandův	k2eAgInSc1d1	Bertrandův
postulát	postulát	k1gInSc1	postulát
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Číslo	číslo	k1gNnSc1	číslo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
p	p	k?	p
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
jedna	jeden	k4xCgFnSc1	jeden
je	být	k5eAaImIp3nS	být
prvočíslo	prvočíslo	k1gNnSc1	prvočíslo
<g/>
,	,	kIx,	,
právě	právě	k9	právě
když	když	k8xS	když
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
</s>
</p>
<p>
</p>
<p>
<s>
≡	≡	k?	≡
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
mod	mod	k?	mod
</s>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
p-1	p-1	k4	p-1
<g/>
)	)	kIx)	)
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
\	\	kIx~	\
\	\	kIx~	\
<g/>
equiv	equiv	k6eAd1	equiv
\	\	kIx~	\
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
pmod	pmod	k1gInSc1	pmod
{	{	kIx(	{
<g/>
p	p	k?	p
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
Wilsonova	Wilsonův	k2eAgFnSc1d1	Wilsonova
věta	věta	k1gFnSc1	věta
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
G	G	kA	G
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
konečná	konečný	k2eAgFnSc1d1	konečná
grupa	grupa	k1gFnSc1	grupa
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
p	p	k?	p
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
mocnina	mocnina	k1gFnSc1	mocnina
prvočísla	prvočíslo	k1gNnSc2	prvočíslo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
p	p	k?	p
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
dělí	dělit	k5eAaImIp3nS	dělit
řád	řád	k1gInSc4	řád
grupy	grupa	k1gFnSc2	grupa
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
G	G	kA	G
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
grupa	grupa	k1gFnSc1	grupa
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
G	G	kA	G
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
podgrupu	podgrupat	k5eAaPmIp1nS	podgrupat
řádu	řád	k1gInSc2	řád
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
p	p	k?	p
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
p	p	k?	p
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
prvočíslo	prvočíslo	k1gNnSc4	prvočíslo
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
G	G	kA	G
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
grupa	grupa	k1gFnSc1	grupa
s	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
p	p	k?	p
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
prvky	prvek	k1gInPc1	prvek
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
G	G	kA	G
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
prvek	prvek	k1gInSc1	prvek
řádu	řád	k1gInSc2	řád
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
p	p	k?	p
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
Prvočísel	prvočíslo	k1gNnPc2	prvočíslo
je	být	k5eAaImIp3nS	být
nekonečně	konečně	k6eNd1	konečně
mnoho	mnoho	k4c1	mnoho
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Suma	suma	k1gFnSc1	suma
převrácených	převrácený	k2eAgFnPc2d1	převrácená
hodnot	hodnota	k1gFnPc2	hodnota
prvočísel	prvočíslo	k1gNnPc2	prvočíslo
diverguje	divergovat	k5eAaImIp3nS	divergovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hustota	hustota	k1gFnSc1	hustota
prvočísel	prvočíslo	k1gNnPc2	prvočíslo
je	být	k5eAaImIp3nS	být
asymptoticky	asymptoticky	k6eAd1	asymptoticky
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
<s>
/	/	kIx~	/
</s>
</p>
<p>
</p>
<p>
<s>
ln	ln	k?	ln
</s>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
\	\	kIx~	\
<g/>
ln	ln	k?	ln
<g/>
(	(	kIx(	(
<g/>
n	n	k0	n
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
kde	kde	k6eAd1	kde
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
ln	ln	k?	ln
</s>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
ln	ln	k?	ln
<g/>
(	(	kIx(	(
<g/>
n	n	k0	n
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
přirozený	přirozený	k2eAgInSc4d1	přirozený
logaritmus	logaritmus	k1gInSc4	logaritmus
n	n	k0	n
<g/>
.	.	kIx.	.
</s>
<s>
Přesněji	přesně	k6eAd2	přesně
<g/>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
π	π	k?	π
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
≃	≃	k?	≃
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
<s>
/	/	kIx~	/
</s>
</p>
<p>
</p>
<p>
<s>
ln	ln	k?	ln
</s>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
pi	pi	k0	pi
(	(	kIx(	(
<g/>
n	n	k0	n
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
simeq	simeq	k?	simeq
n	n	k0	n
<g/>
/	/	kIx~	/
<g/>
\	\	kIx~	\
<g/>
ln	ln	k?	ln
<g/>
(	(	kIx(	(
<g/>
n	n	k0	n
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
kde	kde	k6eAd1	kde
prvočíselná	prvočíselný	k2eAgFnSc1d1	prvočíselná
funkce	funkce	k1gFnSc1	funkce
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
π	π	k?	π
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
pi	pi	k0	pi
(	(	kIx(	(
<g/>
n	n	k0	n
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
počet	počet	k1gInSc4	počet
prvočísel	prvočíslo	k1gNnPc2	prvočíslo
menších	malý	k2eAgNnPc2d2	menší
než	než	k8xS	než
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
Největší	veliký	k2eAgNnSc1d3	veliký
dnes	dnes	k6eAd1	dnes
(	(	kIx(	(
<g/>
prosinec	prosinec	k1gInSc1	prosinec
2018	[number]	k4	2018
<g/>
)	)	kIx)	)
známé	známý	k2eAgNnSc1d1	známé
prvočíslo	prvočíslo	k1gNnSc1	prvočíslo
je	být	k5eAaImIp3nS	být
282 589 933	[number]	k4	282 589 933
−	−	k?	−
1	[number]	k4	1
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
24 862 048	[number]	k4	24 862 048
dekadických	dekadický	k2eAgFnPc2d1	dekadická
cifer	cifra	k1gFnPc2	cifra
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
51.	[number]	k4	51.
známé	známý	k2eAgNnSc1d1	známé
Mersennovo	Mersennův	k2eAgNnSc1d1	Mersennovo
prvočíslo	prvočíslo	k1gNnSc1	prvočíslo
<g/>
,	,	kIx,	,
označované	označovaný	k2eAgFnPc1d1	označovaná
jako	jako	k8xS	jako
M82589933	M82589933	k1gFnPc1	M82589933
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
nalezeno	naleznout	k5eAaPmNgNnS	naleznout
7.	[number]	k4	7.
prosince	prosinec	k1gInSc2	prosinec
2018.	[number]	k4	2018.
<g/>
Zkoumáním	zkoumání	k1gNnSc7	zkoumání
vlastností	vlastnost	k1gFnPc2	vlastnost
prvočísel	prvočíslo	k1gNnPc2	prvočíslo
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
teorie	teorie	k1gFnSc1	teorie
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Zobecněním	zobecnění	k1gNnSc7	zobecnění
prvočísel	prvočíslo	k1gNnPc2	prvočíslo
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
abstraktní	abstraktní	k2eAgFnSc6d1	abstraktní
algebře	algebra	k1gFnSc6	algebra
prvočinitelé	prvočinitel	k1gMnPc1	prvočinitel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
prvočísel	prvočíslo	k1gNnPc2	prvočíslo
==	==	k?	==
</s>
</p>
<p>
<s>
Prvočísel	prvočíslo	k1gNnPc2	prvočíslo
je	být	k5eAaImIp3nS	být
nekonečně	konečně	k6eNd1	konečně
mnoho	mnoho	k4c1	mnoho
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Důkaz	důkaz	k1gInSc1	důkaz
sporem	spor	k1gInSc7	spor
<g/>
:	:	kIx,	:
Nechť	nechť	k9	nechť
existuje	existovat	k5eAaImIp3nS	existovat
jen	jen	k9	jen
konečně	konečně	k6eAd1	konečně
mnoho	mnoho	k4c4	mnoho
prvočísel	prvočíslo	k1gNnPc2	prvočíslo
<g/>
.	.	kIx.	.
</s>
<s>
Označme	označit	k5eAaPmRp1nP	označit
je	být	k5eAaImIp3nS	být
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
<g/>
..	..	k?	..
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
p_	p_	k?	p_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
p_	p_	k?	p_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
ldots	ldots	k1gInSc1	ldots
,	,	kIx,	,
<g/>
p_	p_	k?	p_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
Potom	potom	k6eAd1	potom
číslo	číslo	k1gNnSc1	číslo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋯	⋯	k?	⋯
</s>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
=	=	kIx~	=
<g/>
p_	p_	k?	p_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
p_	p_	k?	p_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdots	cdotsit	k5eAaPmRp2nS	cdotsit
p_	p_	k?	p_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
není	být	k5eNaImIp3nS	být
dělitelné	dělitelný	k2eAgNnSc1d1	dělitelné
žádným	žádný	k3yNgInSc7	žádný
z	z	k7c2	z
těchto	tento	k3xDgNnPc2	tento
prvočísel	prvočíslo	k1gNnPc2	prvočíslo
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
při	při	k7c6	při
dělení	dělení	k1gNnSc6	dělení
dostaneme	dostat	k5eAaPmIp1nP	dostat
vždy	vždy	k6eAd1	vždy
zbytek	zbytek	k1gInSc4	zbytek
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
číslo	číslo	k1gNnSc1	číslo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
buď	buď	k8xC	buď
prvočíslo	prvočíslo	k1gNnSc4	prvočíslo
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
dělitelné	dělitelný	k2eAgNnSc1d1	dělitelné
nějakým	nějaký	k3yIgNnSc7	nějaký
jiným	jiný	k2eAgNnSc7d1	jiné
prvočíslem	prvočíslo	k1gNnSc7	prvočíslo
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
ale	ale	k9	ale
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
množina	množina	k1gFnSc1	množina
prvočísel	prvočíslo	k1gNnPc2	prvočíslo
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
důkazu	důkaz	k1gInSc2	důkaz
nebyla	být	k5eNaImAgFnS	být
úplná	úplný	k2eAgFnSc1d1	úplná
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
spor	spor	k1gInSc1	spor
s	s	k7c7	s
předpokladem	předpoklad	k1gInSc7	předpoklad
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
důkaz	důkaz	k1gInSc1	důkaz
předvedl	předvést	k5eAaPmAgInS	předvést
Eukleidés	Eukleidés	k1gInSc4	Eukleidés
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
Bertrandova	Bertrandův	k2eAgInSc2d1	Bertrandův
postulátu	postulát	k1gInSc2	postulát
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaPmF	nalézt
vždy	vždy	k6eAd1	vždy
alespoň	alespoň	k9	alespoň
jedno	jeden	k4xCgNnSc4	jeden
prvočíslo	prvočíslo	k1gNnSc4	prvočíslo
mezi	mezi	k7c7	mezi
čísly	číslo	k1gNnPc7	číslo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
2n	[number]	k4	2n
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
pro	pro	k7c4	pro
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
<s>
>	>	kIx)	>
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
>	>	kIx)	>
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
jich	on	k3xPp3gMnPc2	on
však	však	k9	však
existuje	existovat	k5eAaImIp3nS	existovat
pro	pro	k7c4	pro
vyšší	vysoký	k2eAgFnSc4d2	vyšší
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
daleko	daleko	k6eAd1	daleko
více	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
I	i	k9	i
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
věty	věta	k1gFnSc2	věta
lze	lze	k6eAd1	lze
dovodit	dovodit	k5eAaPmF	dovodit
<g/>
,	,	kIx,	,
že	že	k8xS	že
prvočísel	prvočíslo	k1gNnPc2	prvočíslo
je	být	k5eAaImIp3nS	být
nekonečně	konečně	k6eNd1	konečně
mnoho	mnoho	k4c1	mnoho
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaPmF	nalézt
libovolně	libovolně	k6eAd1	libovolně
dlouhé	dlouhý	k2eAgInPc1d1	dlouhý
intervaly	interval	k1gInPc1	interval
přirozených	přirozený	k2eAgNnPc2d1	přirozené
čísel	číslo	k1gNnPc2	číslo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
žádné	žádný	k3yNgNnSc4	žádný
prvočíslo	prvočíslo	k1gNnSc4	prvočíslo
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
interval	interval	k1gInSc4	interval
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
<g/>
..	..	k?	..
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
k	k	k7c3	k
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
k	k	k7c3	k
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
+	+	kIx~	+
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
ldots	ldots	k1gInSc1	ldots
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
k	k	k7c3	k
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
+	+	kIx~	+
<g/>
k	k	k7c3	k
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
k	k	k7c3	k
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
složených	složený	k2eAgNnPc2d1	složené
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
čísla	číslo	k1gNnPc1	číslo
jsou	být	k5eAaImIp3nP	být
totiž	totiž	k9	totiž
po	po	k7c6	po
řadě	řada	k1gFnSc6	řada
dělitelná	dělitelný	k2eAgFnSc1d1	dělitelná
dvěma	dva	k4xCgFnPc7	dva
<g/>
,	,	kIx,	,
třemi	tři	k4xCgNnPc7	tři
<g/>
,	,	kIx,	,
.	.	kIx.	.
<g/>
..	..	k?	..
<g/>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
k	k	k7c3	k
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
Mnoho	mnoho	k4c1	mnoho
hypotéz	hypotéza	k1gFnPc2	hypotéza
o	o	k7c6	o
rozložení	rozložení	k1gNnSc6	rozložení
prvočísel	prvočíslo	k1gNnPc2	prvočíslo
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
nevyřešených	vyřešený	k2eNgFnPc2d1	nevyřešená
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
otevřený	otevřený	k2eAgInSc1d1	otevřený
problém	problém	k1gInSc1	problém
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
Riemannova	Riemannův	k2eAgFnSc1d1	Riemannova
hypotéza	hypotéza	k1gFnSc1	hypotéza
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
pravidelností	pravidelnost	k1gFnSc7	pravidelnost
rozložení	rozložení	k1gNnSc2	rozložení
prvočísel	prvočíslo	k1gNnPc2	prvočíslo
a	a	k8xC	a
za	za	k7c4	za
jejíž	jejíž	k3xOyRp3gInSc4	jejíž
důkaz	důkaz	k1gInSc4	důkaz
je	být	k5eAaImIp3nS	být
vypsána	vypsán	k2eAgFnSc1d1	vypsána
odměna	odměna	k1gFnSc1	odměna
milion	milion	k4xCgInSc1	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Speciální	speciální	k2eAgNnPc1d1	speciální
prvočísla	prvočíslo	k1gNnPc1	prvočíslo
===	===	k?	===
</s>
</p>
<p>
<s>
Některá	některý	k3yIgNnPc1	některý
prvočísla	prvočíslo	k1gNnPc1	prvočíslo
lze	lze	k6eAd1	lze
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
v	v	k7c6	v
některé	některý	k3yIgFnSc6	některý
z	z	k7c2	z
několika	několik	k4yIc2	několik
matematicky	matematicky	k6eAd1	matematicky
zajímavých	zajímavý	k2eAgFnPc2d1	zajímavá
podob	podoba	k1gFnPc2	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
sem	sem	k6eAd1	sem
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
<s>
Fermatova	Fermatův	k2eAgNnPc1d1	Fermatovo
čísla	číslo	k1gNnPc1	číslo
<g/>
:	:	kIx,	:
Prvočísly	prvočíslo	k1gNnPc7	prvočíslo
je	on	k3xPp3gMnPc4	on
prvních	první	k4xOgNnPc6	první
pět	pět	k4xCc4	pět
čísel	číslo	k1gNnPc2	číslo
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
2	[number]	k4	2
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
Mersennova	Mersennův	k2eAgNnPc1d1	Mersennovo
prvočísla	prvočíslo	k1gNnPc1	prvočíslo
<g/>
:	:	kIx,	:
Prvočíslo	prvočíslo	k1gNnSc1	prvočíslo
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
2	[number]	k4	2
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
p	p	k?	p
<g/>
}	}	kIx)	}
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
kde	kde	k6eAd1	kde
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
p	p	k?	p
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
jiné	jiný	k2eAgNnSc4d1	jiné
prvočíslo	prvočíslo	k1gNnSc4	prvočíslo
<g/>
.	.	kIx.	.
</s>
<s>
Mersennovými	Mersennův	k2eAgNnPc7d1	Mersennovo
prvočísly	prvočíslo	k1gNnPc7	prvočíslo
je	být	k5eAaImIp3nS	být
mnoho	mnoho	k4c4	mnoho
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
známých	známá	k1gFnPc2	známá
prvočísel	prvočíslo	k1gNnPc2	prvočíslo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Některá	některý	k3yIgNnPc1	některý
prvočísla	prvočíslo	k1gNnPc1	prvočíslo
lze	lze	k6eAd1	lze
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
6	[number]	k4	6
</s>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
1+2	[number]	k4	1+2
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
6	[number]	k4	6
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
Velký	velký	k2eAgInSc1d1	velký
praktický	praktický	k2eAgInSc1d1	praktický
význam	význam	k1gInSc1	význam
mají	mít	k5eAaImIp3nP	mít
prvočísla	prvočíslo	k1gNnPc1	prvočíslo
v	v	k7c6	v
kryptografii	kryptografie	k1gFnSc6	kryptografie
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
šifrovacích	šifrovací	k2eAgInPc6d1	šifrovací
systémech	systém	k1gInPc6	systém
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
RSA	RSA	kA	RSA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
vytvoření	vytvoření	k1gNnSc4	vytvoření
seznamu	seznam	k1gInSc2	seznam
prvočísel	prvočíslo	k1gNnPc2	prvočíslo
existují	existovat	k5eAaImIp3nP	existovat
různé	různý	k2eAgInPc1d1	různý
algoritmy	algoritmus	k1gInPc1	algoritmus
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Eratosthenovo	Eratosthenův	k2eAgNnSc1d1	Eratosthenovo
síto	síto	k1gNnSc1	síto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Testování	testování	k1gNnSc4	testování
prvočíselnosti	prvočíselnost	k1gFnSc2	prvočíselnost
==	==	k?	==
</s>
</p>
<p>
<s>
Otestovat	otestovat	k5eAaPmF	otestovat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
číslo	číslo	k1gNnSc4	číslo
prvočíslem	prvočíslo	k1gNnSc7	prvočíslo
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
testovat	testovat	k5eAaImF	testovat
prvočíselnost	prvočíselnost	k1gFnSc4	prvočíselnost
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
asymptoticky	asymptoticky	k6eAd1	asymptoticky
v	v	k7c6	v
polynomiálním	polynomiální	k2eAgInSc6d1	polynomiální
čase	čas	k1gInSc6	čas
algoritmem	algoritmus	k1gInSc7	algoritmus
AKS	AKS	kA	AKS
<g/>
,	,	kIx,	,
nalezeným	nalezený	k2eAgInPc3d1	nalezený
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Asymptoticky	asymptoticky	k6eAd1	asymptoticky
rekordní	rekordní	k2eAgFnSc1d1	rekordní
rychlost	rychlost	k1gFnSc1	rychlost
ovšem	ovšem	k9	ovšem
neznamená	znamenat	k5eNaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
algoritmus	algoritmus	k1gInSc4	algoritmus
prakticky	prakticky	k6eAd1	prakticky
nejvýhodnější	výhodný	k2eAgMnSc1d3	nejvýhodnější
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
bývá	bývat	k5eAaImIp3nS	bývat
častější	častý	k2eAgNnSc1d2	častější
použití	použití	k1gNnSc1	použití
některého	některý	k3yIgInSc2	některý
z	z	k7c2	z
pravděpodobnostních	pravděpodobnostní	k2eAgInPc2d1	pravděpodobnostní
algoritmů	algoritmus	k1gInPc2	algoritmus
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Millerova-Rabinova	Millerova-Rabinův	k2eAgInSc2d1	Millerova-Rabinův
algoritmu	algoritmus	k1gInSc2	algoritmus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Testování	testování	k1gNnPc1	testování
prvočíselnosti	prvočíselnost	k1gFnSc2	prvočíselnost
pomocí	pomocí	k7c2	pomocí
algoritmu	algoritmus	k1gInSc2	algoritmus
využívajícího	využívající	k2eAgInSc2d1	využívající
vlastností	vlastnost	k1gFnSc7	vlastnost
eliptických	eliptický	k2eAgFnPc2d1	eliptická
křivek	křivka	k1gFnPc2	křivka
(	(	kIx(	(
<g/>
ECPP	ECPP	kA	ECPP
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejrychlejší	rychlý	k2eAgInSc1d3	nejrychlejší
známý	známý	k2eAgInSc1d1	známý
algoritmus	algoritmus	k1gInSc1	algoritmus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Příklad	příklad	k1gInSc1	příklad
testovacího	testovací	k2eAgInSc2d1	testovací
algoritmu	algoritmus	k1gInSc2	algoritmus
===	===	k?	===
</s>
</p>
<p>
<s>
Následující	následující	k2eAgInSc1d1	následující
jednoduchý	jednoduchý	k2eAgInSc1d1	jednoduchý
algoritmus	algoritmus	k1gInSc1	algoritmus
implementovaný	implementovaný	k2eAgInSc1d1	implementovaný
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
C	C	kA	C
<g/>
++	++	k?	++
zkouší	zkoušet	k5eAaImIp3nP	zkoušet
dělit	dělit	k5eAaImF	dělit
vstup	vstup	k1gInSc4	vstup
všemi	všecek	k3xTgNnPc7	všecek
menšími	malý	k2eAgNnPc7d2	menší
čísly	číslo	k1gNnPc7	číslo
od	od	k7c2	od
2	[number]	k4	2
do	do	k7c2	do
jeho	jeho	k3xOp3gFnSc2	jeho
odmocniny	odmocnina	k1gFnSc2	odmocnina
-	-	kIx~	-
pokud	pokud	k8xS	pokud
nalezne	nalézt	k5eAaBmIp3nS	nalézt
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
intervalu	interval	k1gInSc6	interval
dělitele	dělitel	k1gInSc2	dělitel
zadaného	zadaný	k2eAgNnSc2d1	zadané
čísla	číslo	k1gNnSc2	číslo
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
zadané	zadaný	k2eAgNnSc4d1	zadané
číslo	číslo	k1gNnSc4	číslo
není	být	k5eNaImIp3nS	být
prvočíslo	prvočíslo	k1gNnSc4	prvočíslo
<g/>
.	.	kIx.	.
</s>
<s>
Testovat	testovat	k5eAaImF	testovat
stačí	stačit	k5eAaBmIp3nS	stačit
pouze	pouze	k6eAd1	pouze
do	do	k7c2	do
odmocniny	odmocnina	k1gFnSc2	odmocnina
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
pokud	pokud	k8xS	pokud
n	n	k0	n
je	být	k5eAaImIp3nS	být
složené	složený	k2eAgNnSc4d1	složené
číslo	číslo	k1gNnSc4	číslo
<g/>
,	,	kIx,	,
můžeme	moct	k5eAaImIp1nP	moct
psát	psát	k5eAaImF	psát
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
a	a	k8xC	a
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k2eAgMnSc1d1	cdot
{	{	kIx(	{
<g/>
}	}	kIx)	}
<g/>
b	b	k?	b
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
pro	pro	k7c4	pro
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
<s>
∈	∈	k?	∈
</s>
</p>
<p>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
<s>
>	>	kIx)	>
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
,	,	kIx,	,
<g/>
b	b	k?	b
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
N	N	kA	N
<g/>
}	}	kIx)	}
,	,	kIx,	,
<g/>
a	a	k8xC	a
<g/>
,	,	kIx,	,
<g/>
b	b	k?	b
<g/>
>	>	kIx)	>
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
nestačilo	stačit	k5eNaBmAgNnS	stačit
testovat	testovat	k5eAaImF	testovat
do	do	k7c2	do
odmocniny	odmocnina	k1gFnSc2	odmocnina
<g/>
,	,	kIx,	,
znamenalo	znamenat	k5eAaImAgNnS	znamenat
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
>	>	kIx)	>
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
>	>	kIx)	>
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k1gInSc1	sqrt
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
a	a	k8xC	a
současně	současně	k6eAd1	současně
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
<s>
>	>	kIx)	>
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
b	b	k?	b
<g/>
>	>	kIx)	>
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k1gInSc1	sqrt
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
vynásobíme	vynásobit	k5eAaPmIp1nP	vynásobit
<g/>
-li	i	k?	-li
ale	ale	k8xC	ale
tyto	tento	k3xDgInPc4	tento
dva	dva	k4xCgInPc4	dva
vztahy	vztah	k1gInPc4	vztah
<g/>
,	,	kIx,	,
máme	mít	k5eAaImIp1nP	mít
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
<s>
>	>	kIx)	>
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k2eAgMnSc1d1	cdot
{	{	kIx(	{
<g/>
}	}	kIx)	}
<g/>
b	b	k?	b
<g/>
>	>	kIx)	>
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
spor	spor	k1gInSc4	spor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Prvočísla	prvočíslo	k1gNnPc1	prvočíslo
menší	malý	k2eAgFnSc2d2	menší
než	než	k8xS	než
1000	[number]	k4	1000
==	==	k?	==
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
,	,	kIx,	,
11	[number]	k4	11
<g/>
,	,	kIx,	,
13	[number]	k4	13
<g/>
,	,	kIx,	,
17	[number]	k4	17
<g/>
,	,	kIx,	,
19	[number]	k4	19
<g/>
,	,	kIx,	,
23	[number]	k4	23
<g/>
,	,	kIx,	,
29	[number]	k4	29
<g/>
,	,	kIx,	,
31	[number]	k4	31
<g/>
,	,	kIx,	,
37	[number]	k4	37
<g/>
,	,	kIx,	,
41	[number]	k4	41
<g/>
,	,	kIx,	,
43	[number]	k4	43
<g/>
,	,	kIx,	,
47	[number]	k4	47
<g/>
,	,	kIx,	,
53	[number]	k4	53
<g/>
,	,	kIx,	,
59	[number]	k4	59
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
61	[number]	k4	61
<g/>
,	,	kIx,	,
67	[number]	k4	67
<g/>
,	,	kIx,	,
71	[number]	k4	71
<g/>
,	,	kIx,	,
73	[number]	k4	73
<g/>
,	,	kIx,	,
79	[number]	k4	79
<g/>
,	,	kIx,	,
83	[number]	k4	83
<g/>
,	,	kIx,	,
89	[number]	k4	89
<g/>
,	,	kIx,	,
97	[number]	k4	97
<g/>
,	,	kIx,	,
101	[number]	k4	101
<g/>
,	,	kIx,	,
103	[number]	k4	103
<g/>
,	,	kIx,	,
107	[number]	k4	107
<g/>
,	,	kIx,	,
109	[number]	k4	109
<g/>
,	,	kIx,	,
113	[number]	k4	113
<g/>
,	,	kIx,	,
127	[number]	k4	127
<g/>
,	,	kIx,	,
131	[number]	k4	131
<g/>
,	,	kIx,	,
137	[number]	k4	137
<g/>
,	,	kIx,	,
139	[number]	k4	139
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
149	[number]	k4	149
<g/>
,	,	kIx,	,
151	[number]	k4	151
<g/>
,	,	kIx,	,
157	[number]	k4	157
<g/>
,	,	kIx,	,
163	[number]	k4	163
<g/>
,	,	kIx,	,
167	[number]	k4	167
<g/>
,	,	kIx,	,
173	[number]	k4	173
<g/>
,	,	kIx,	,
179	[number]	k4	179
<g/>
,	,	kIx,	,
181	[number]	k4	181
<g/>
,	,	kIx,	,
191	[number]	k4	191
<g/>
,	,	kIx,	,
193	[number]	k4	193
<g/>
,	,	kIx,	,
197	[number]	k4	197
<g/>
,	,	kIx,	,
199	[number]	k4	199
<g/>
,	,	kIx,	,
211	[number]	k4	211
<g/>
,	,	kIx,	,
223	[number]	k4	223
<g/>
,	,	kIx,	,
227	[number]	k4	227
<g/>
,	,	kIx,	,
229	[number]	k4	229
<g/>
,	,	kIx,	,
233	[number]	k4	233
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
239	[number]	k4	239
<g/>
,	,	kIx,	,
241	[number]	k4	241
<g/>
,	,	kIx,	,
251	[number]	k4	251
<g/>
,	,	kIx,	,
257	[number]	k4	257
<g/>
,	,	kIx,	,
263	[number]	k4	263
<g/>
,	,	kIx,	,
269	[number]	k4	269
<g/>
,	,	kIx,	,
271	[number]	k4	271
<g/>
,	,	kIx,	,
277	[number]	k4	277
<g/>
,	,	kIx,	,
281	[number]	k4	281
<g/>
,	,	kIx,	,
283	[number]	k4	283
<g/>
,	,	kIx,	,
293	[number]	k4	293
<g/>
,	,	kIx,	,
307	[number]	k4	307
<g/>
,	,	kIx,	,
311	[number]	k4	311
<g/>
,	,	kIx,	,
313	[number]	k4	313
<g/>
,	,	kIx,	,
317	[number]	k4	317
<g/>
,	,	kIx,	,
331	[number]	k4	331
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
337	[number]	k4	337
<g/>
,	,	kIx,	,
347	[number]	k4	347
<g/>
,	,	kIx,	,
349	[number]	k4	349
<g/>
,	,	kIx,	,
353	[number]	k4	353
<g/>
,	,	kIx,	,
359	[number]	k4	359
<g/>
,	,	kIx,	,
367	[number]	k4	367
<g/>
,	,	kIx,	,
373	[number]	k4	373
<g/>
,	,	kIx,	,
379	[number]	k4	379
<g/>
,	,	kIx,	,
383	[number]	k4	383
<g/>
,	,	kIx,	,
389	[number]	k4	389
<g/>
,	,	kIx,	,
397	[number]	k4	397
<g/>
,	,	kIx,	,
401	[number]	k4	401
<g/>
,	,	kIx,	,
409	[number]	k4	409
<g/>
,	,	kIx,	,
419	[number]	k4	419
<g/>
,	,	kIx,	,
421	[number]	k4	421
<g/>
,	,	kIx,	,
431	[number]	k4	431
<g/>
,	,	kIx,	,
433	[number]	k4	433
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
439	[number]	k4	439
<g/>
,	,	kIx,	,
443	[number]	k4	443
<g/>
,	,	kIx,	,
449	[number]	k4	449
<g/>
,	,	kIx,	,
457	[number]	k4	457
<g/>
,	,	kIx,	,
461	[number]	k4	461
<g/>
,	,	kIx,	,
463	[number]	k4	463
<g/>
,	,	kIx,	,
467	[number]	k4	467
<g/>
,	,	kIx,	,
479	[number]	k4	479
<g/>
,	,	kIx,	,
487	[number]	k4	487
<g/>
,	,	kIx,	,
491	[number]	k4	491
<g/>
,	,	kIx,	,
499	[number]	k4	499
<g/>
,	,	kIx,	,
503	[number]	k4	503
<g/>
,	,	kIx,	,
509	[number]	k4	509
<g/>
,	,	kIx,	,
521	[number]	k4	521
<g/>
,	,	kIx,	,
523	[number]	k4	523
<g/>
,	,	kIx,	,
541	[number]	k4	541
<g/>
,	,	kIx,	,
547	[number]	k4	547
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
557	[number]	k4	557
<g/>
,	,	kIx,	,
563	[number]	k4	563
<g/>
,	,	kIx,	,
569	[number]	k4	569
<g/>
,	,	kIx,	,
571	[number]	k4	571
<g/>
,	,	kIx,	,
577	[number]	k4	577
<g/>
,	,	kIx,	,
587	[number]	k4	587
<g/>
,	,	kIx,	,
593	[number]	k4	593
<g/>
,	,	kIx,	,
599	[number]	k4	599
<g/>
,	,	kIx,	,
601	[number]	k4	601
<g/>
,	,	kIx,	,
607	[number]	k4	607
<g/>
,	,	kIx,	,
613	[number]	k4	613
<g/>
,	,	kIx,	,
617	[number]	k4	617
<g/>
,	,	kIx,	,
619	[number]	k4	619
<g/>
,	,	kIx,	,
631	[number]	k4	631
<g/>
,	,	kIx,	,
641	[number]	k4	641
<g/>
,	,	kIx,	,
643	[number]	k4	643
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
647	[number]	k4	647
<g/>
,	,	kIx,	,
653	[number]	k4	653
<g/>
,	,	kIx,	,
659	[number]	k4	659
<g/>
,	,	kIx,	,
661	[number]	k4	661
<g/>
,	,	kIx,	,
673	[number]	k4	673
<g/>
,	,	kIx,	,
677	[number]	k4	677
<g/>
,	,	kIx,	,
683	[number]	k4	683
<g/>
,	,	kIx,	,
691	[number]	k4	691
<g/>
,	,	kIx,	,
701	[number]	k4	701
<g/>
,	,	kIx,	,
709	[number]	k4	709
<g/>
,	,	kIx,	,
719	[number]	k4	719
<g/>
,	,	kIx,	,
727	[number]	k4	727
<g/>
,	,	kIx,	,
733	[number]	k4	733
<g/>
,	,	kIx,	,
739	[number]	k4	739
<g/>
,	,	kIx,	,
743	[number]	k4	743
<g/>
,	,	kIx,	,
751	[number]	k4	751
<g/>
,	,	kIx,	,
757	[number]	k4	757
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
761	[number]	k4	761
<g/>
,	,	kIx,	,
769	[number]	k4	769
<g/>
,	,	kIx,	,
773	[number]	k4	773
<g/>
,	,	kIx,	,
787	[number]	k4	787
<g/>
,	,	kIx,	,
797	[number]	k4	797
<g/>
,	,	kIx,	,
809	[number]	k4	809
<g/>
,	,	kIx,	,
811	[number]	k4	811
<g/>
,	,	kIx,	,
821	[number]	k4	821
<g/>
,	,	kIx,	,
823	[number]	k4	823
<g/>
,	,	kIx,	,
827	[number]	k4	827
<g/>
,	,	kIx,	,
829	[number]	k4	829
<g/>
,	,	kIx,	,
839	[number]	k4	839
<g/>
,	,	kIx,	,
853	[number]	k4	853
<g/>
,	,	kIx,	,
857	[number]	k4	857
<g/>
,	,	kIx,	,
859	[number]	k4	859
<g/>
,	,	kIx,	,
863	[number]	k4	863
<g/>
,	,	kIx,	,
877	[number]	k4	877
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
881	[number]	k4	881
<g/>
,	,	kIx,	,
883	[number]	k4	883
<g/>
,	,	kIx,	,
887	[number]	k4	887
<g/>
,	,	kIx,	,
907	[number]	k4	907
<g/>
,	,	kIx,	,
911	[number]	k4	911
<g/>
,	,	kIx,	,
919	[number]	k4	919
<g/>
,	,	kIx,	,
929	[number]	k4	929
<g/>
,	,	kIx,	,
937	[number]	k4	937
<g/>
,	,	kIx,	,
941	[number]	k4	941
<g/>
,	,	kIx,	,
947	[number]	k4	947
<g/>
,	,	kIx,	,
953	[number]	k4	953
<g/>
,	,	kIx,	,
967	[number]	k4	967
<g/>
,	,	kIx,	,
971	[number]	k4	971
<g/>
,	,	kIx,	,
977	[number]	k4	977
<g/>
,	,	kIx,	,
983	[number]	k4	983
<g/>
,	,	kIx,	,
991	[number]	k4	991
<g/>
,	,	kIx,	,
997	[number]	k4	997
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Eratosthenovo	Eratosthenův	k2eAgNnSc1d1	Eratosthenovo
síto	síto	k1gNnSc1	síto
</s>
</p>
<p>
<s>
Mersennovo	Mersennův	k2eAgNnSc1d1	Mersennovo
prvočíslo	prvočíslo	k1gNnSc1	prvočíslo
</s>
</p>
<p>
<s>
Emirp	Emirp	k1gMnSc1	Emirp
</s>
</p>
<p>
<s>
Prvočíselný	prvočíselný	k2eAgInSc1d1	prvočíselný
rozklad	rozklad	k1gInSc1	rozklad
</s>
</p>
<p>
<s>
Prvočíselná	prvočíselný	k2eAgFnSc1d1	prvočíselná
dvojice	dvojice	k1gFnSc1	dvojice
</s>
</p>
<p>
<s>
Wieferichovo	Wieferichův	k2eAgNnSc1d1	Wieferichův
prvočíslo	prvočíslo	k1gNnSc1	prvočíslo
</s>
</p>
<p>
<s>
Ulamova	Ulamův	k2eAgFnSc1d1	Ulamův
spirála	spirála	k1gFnSc1	spirála
</s>
</p>
<p>
<s>
2147483647	[number]	k4	2147483647
</s>
</p>
<p>
<s>
Ilegální	ilegální	k2eAgNnSc1d1	ilegální
prvočíslo	prvočíslo	k1gNnSc1	prvočíslo
</s>
</p>
<p>
<s>
Poloprvočíslo	Poloprvočísnout	k5eAaPmAgNnS	Poloprvočísnout
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
prvočíslo	prvočíslo	k1gNnSc4	prvočíslo
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
prvočíslo	prvočíslo	k1gNnSc4	prvočíslo
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Výukový	výukový	k2eAgInSc1d1	výukový
kurs	kurs	k1gInSc1	kurs
Prvočísla	prvočíslo	k1gNnSc2	prvočíslo
ve	v	k7c6	v
Wikiverzitě	Wikiverzita	k1gFnSc6	Wikiverzita
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Primes	Primesa	k1gFnPc2	Primesa
Pages	Pagesa	k1gFnPc2	Pagesa
–	–	k?	–
přehledové	přehledový	k2eAgFnSc2d1	přehledová
i	i	k8xC	i
aktuální	aktuální	k2eAgFnSc2d1	aktuální
informace	informace	k1gFnSc2	informace
o	o	k7c6	o
výzkumu	výzkum	k1gInSc6	výzkum
prvočísel	prvočíslo	k1gNnPc2	prvočíslo
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
www.prime-numbers.org	www.primeumbers.org	k1gInSc1	www.prime-numbers.org
–	–	k?	–
seznam	seznam	k1gInSc4	seznam
prvočísel	prvočíslo	k1gNnPc2	prvočíslo
do	do	k7c2	do
10	[number]	k4	10
miliard	miliarda	k4xCgFnPc2	miliarda
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Prvočísla	prvočíslo	k1gNnPc4	prvočíslo
do	do	k7c2	do
jednoho	jeden	k4xCgInSc2	jeden
bilionu	bilion	k4xCgInSc2	bilion
</s>
</p>
