<s>
Vejdova	Vejdův	k2eAgFnSc1d1	Vejdova
lípa	lípa	k1gFnSc1	lípa
je	být	k5eAaImIp3nS	být
obvodem	obvod	k1gInSc7	obvod
nejmohutnější	mohutný	k2eAgInSc4d3	nejmohutnější
památný	památný	k2eAgInSc4d1	památný
strom	strom	k1gInSc4	strom
na	na	k7c6	na
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Roste	růst	k5eAaImIp3nS	růst
v	v	k7c6	v
Pastvinách	pastvina	k1gFnPc6	pastvina
nedaleko	nedaleko	k7c2	nedaleko
Klášterce	Klášterec	k1gInSc2	Klášterec
nad	nad	k7c7	nad
Orlicí	Orlice	k1gFnSc7	Orlice
<g/>
.	.	kIx.	.
</s>
<s>
Lip	lípa	k1gFnPc2	lípa
bylo	být	k5eAaImAgNnS	být
původně	původně	k6eAd1	původně
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dodnes	dodnes	k6eAd1	dodnes
se	se	k3xPyFc4	se
zachovala	zachovat	k5eAaPmAgFnS	zachovat
jediná	jediný	k2eAgFnSc1d1	jediná
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
nejstarší	starý	k2eAgFnSc1d3	nejstarší
(	(	kIx(	(
<g/>
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
statku	statek	k1gInSc2	statek
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
zničena	zničit	k5eAaPmNgFnS	zničit
vichřicí	vichřice	k1gFnSc7	vichřice
v	v	k7c4	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
stromy	strom	k1gInPc1	strom
byly	být	k5eAaImAgInP	být
chráněny	chránit	k5eAaImNgInP	chránit
již	již	k6eAd1	již
ve	v	k7c6	v
40	[number]	k4	40
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
název	název	k1gInSc1	název
<g/>
:	:	kIx,	:
Vejdova	Vejdův	k2eAgFnSc1d1	Vejdova
lípa	lípa	k1gFnSc1	lípa
výška	výška	k1gFnSc1	výška
<g/>
:	:	kIx,	:
22	[number]	k4	22
m	m	kA	m
(	(	kIx(	(
<g/>
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
25	[number]	k4	25
m	m	kA	m
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
25	[number]	k4	25
m	m	kA	m
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
výška	výška	k1gFnSc1	výška
<g />
.	.	kIx.	.
</s>
<s>
kmene	kmen	k1gInSc2	kmen
<g/>
:	:	kIx,	:
4	[number]	k4	4
m	m	kA	m
obvod	obvod	k1gInSc1	obvod
<g/>
:	:	kIx,	:
1100	[number]	k4	1100
cm	cm	kA	cm
(	(	kIx(	(
<g/>
1880	[number]	k4	1880
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
864	[number]	k4	864
cm	cm	kA	cm
(	(	kIx(	(
<g/>
1907	[number]	k4	1907
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
900	[number]	k4	900
cm	cm	kA	cm
(	(	kIx(	(
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1164	[number]	k4	1164
cm	cm	kA	cm
(	(	kIx(	(
<g/>
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1225	[number]	k4	1225
cm	cm	kA	cm
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1252	[number]	k4	1252
cm	cm	kA	cm
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1259	[number]	k4	1259
cm	cm	kA	cm
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
věk	věk	k1gInSc1	věk
<g/>
:	:	kIx,	:
850	[number]	k4	850
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
1907	[number]	k4	1907
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
850	[number]	k4	850
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
>	>	kIx)	>
<g/>
600	[number]	k4	600
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
700-800	[number]	k4	700-800
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
600	[number]	k4	600
let	léto	k1gNnPc2	léto
sanace	sanace	k1gFnSc2	sanace
<g/>
:	:	kIx,	:
1977	[number]	k4	1977
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
souřadnice	souřadnice	k1gFnPc1	souřadnice
<g/>
:	:	kIx,	:
50	[number]	k4	50
<g/>
°	°	k?	°
<g/>
5	[number]	k4	5
<g/>
'	'	kIx"	'
<g/>
42.13	[number]	k4	42.13
<g/>
"	"	kIx"	"
<g/>
N	N	kA	N
<g/>
,	,	kIx,	,
16	[number]	k4	16
<g/>
°	°	k?	°
<g/>
33	[number]	k4	33
<g/>
'	'	kIx"	'
<g/>
15.28	[number]	k4	15.28
<g/>
"	"	kIx"	"
<g/>
E	E	kA	E
Určitá	určitý	k2eAgFnSc1d1	určitá
nesrovnalost	nesrovnalost	k1gFnSc1	nesrovnalost
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
zdroji	zdroj	k1gInPc7	zdroj
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
druhového	druhový	k2eAgNnSc2d1	druhové
zařazení	zařazení	k1gNnSc2	zařazení
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
zdroje	zdroj	k1gInPc1	zdroj
chybně	chybně	k6eAd1	chybně
uvádějí	uvádět	k5eAaImIp3nP	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
lípou	lípa	k1gFnSc7	lípa
malolistou	malolistý	k2eAgFnSc4d1	malolistá
(	(	kIx(	(
<g/>
srdčitou	srdčitý	k2eAgFnSc4d1	srdčitá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vyhlašovací	vyhlašovací	k2eAgFnSc1d1	vyhlašovací
dokumentace	dokumentace	k1gFnSc1	dokumentace
<g/>
,	,	kIx,	,
registr	registr	k1gInSc1	registr
památných	památný	k2eAgInPc2d1	památný
stromů	strom	k1gInPc2	strom
(	(	kIx(	(
<g/>
který	který	k3yRgInSc1	který
přebírá	přebírat	k5eAaImIp3nS	přebírat
informace	informace	k1gFnPc4	informace
z	z	k7c2	z
vyhlašovací	vyhlašovací	k2eAgFnSc2d1	vyhlašovací
dokumentace	dokumentace	k1gFnSc2	dokumentace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odborné	odborný	k2eAgFnPc1d1	odborná
studie	studie	k1gFnPc1	studie
a	a	k8xC	a
publikace	publikace	k1gFnPc1	publikace
klasifikují	klasifikovat	k5eAaImIp3nP	klasifikovat
lípu	lípa	k1gFnSc4	lípa
jako	jako	k8xS	jako
velkolistou	velkolistý	k2eAgFnSc4d1	velkolistá
<g/>
.	.	kIx.	.
</s>
<s>
Lípa	lípa	k1gFnSc1	lípa
je	být	k5eAaImIp3nS	být
veřejně	veřejně	k6eAd1	veřejně
přístupná	přístupný	k2eAgFnSc1d1	přístupná
<g/>
,	,	kIx,	,
roste	růst	k5eAaImIp3nS	růst
u	u	k7c2	u
bývalé	bývalý	k2eAgFnSc2d1	bývalá
hájovny	hájovna	k1gFnSc2	hájovna
(	(	kIx(	(
<g/>
č.	č.	k?	č.
p.	p.	k?	p.
21	[number]	k4	21
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
severním	severní	k2eAgInSc7d1	severní
směrem	směr	k1gInSc7	směr
<g/>
.	.	kIx.	.
</s>
<s>
Strom	strom	k1gInSc1	strom
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
věk	věk	k1gInSc4	věk
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
dobrém	dobrý	k2eAgInSc6d1	dobrý
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
kmen	kmen	k1gInSc1	kmen
uzavírá	uzavírat	k5eAaPmIp3nS	uzavírat
rozměrnou	rozměrný	k2eAgFnSc4d1	rozměrná
dutinu	dutina	k1gFnSc4	dutina
shora	shora	k6eAd1	shora
krytou	krytý	k2eAgFnSc7d1	krytá
stříškou	stříška	k1gFnSc7	stříška
<g/>
.	.	kIx.	.
</s>
<s>
Otvor	otvor	k1gInSc1	otvor
do	do	k7c2	do
dutiny	dutina	k1gFnSc2	dutina
je	být	k5eAaImIp3nS	být
částečně	částečně	k6eAd1	částečně
překryt	překrýt	k5eAaPmNgInS	překrýt
a	a	k8xC	a
ponechán	ponechat	k5eAaPmNgInS	ponechat
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
průchod	průchod	k1gInSc4	průchod
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nízkého	nízký	k2eAgInSc2d1	nízký
kmene	kmen	k1gInSc2	kmen
se	se	k3xPyFc4	se
rozrůstá	rozrůstat	k5eAaImIp3nS	rozrůstat
široká	široký	k2eAgFnSc1d1	široká
vitální	vitální	k2eAgFnSc1d1	vitální
koruna	koruna	k1gFnSc1	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zápisu	zápis	k1gInSc2	zápis
z	z	k7c2	z
místní	místní	k2eAgFnSc2d1	místní
školní	školní	k2eAgFnSc2d1	školní
knihy	kniha	k1gFnSc2	kniha
z	z	k7c2	z
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
ještě	ještě	k6eAd1	ještě
nebyla	být	k5eNaImAgFnS	být
dutina	dutina	k1gFnSc1	dutina
otevřená	otevřený	k2eAgFnSc1d1	otevřená
a	a	k8xC	a
strom	strom	k1gInSc1	strom
měl	mít	k5eAaImAgInS	mít
v	v	k7c6	v
obvodu	obvod	k1gInSc6	obvod
11	[number]	k4	11
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
otevření	otevření	k1gNnSc3	otevření
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
došlo	dojít	k5eAaPmAgNnS	dojít
vylomením	vylomení	k1gNnSc7	vylomení
části	část	k1gFnSc2	část
srostlice	srostlice	k1gFnSc2	srostlice
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1880	[number]	k4	1880
a	a	k8xC	a
1907	[number]	k4	1907
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
potvrzuje	potvrzovat	k5eAaImIp3nS	potvrzovat
i	i	k9	i
pokles	pokles	k1gInSc1	pokles
celkového	celkový	k2eAgInSc2d1	celkový
obvodu	obvod	k1gInSc2	obvod
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
<g/>
.	.	kIx.	.
</s>
<s>
Lípy	lípa	k1gFnPc1	lípa
byly	být	k5eAaImAgFnP	být
odborně	odborně	k6eAd1	odborně
ošetřeny	ošetřen	k2eAgFnPc1d1	ošetřena
roku	rok	k1gInSc2	rok
1977	[number]	k4	1977
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
na	na	k7c4	na
průřez	průřez	k1gInSc4	průřez
koruny	koruna	k1gFnSc2	koruna
<g/>
,	,	kIx,	,
vyčištění	vyčištění	k1gNnSc4	vyčištění
dutin	dutina	k1gFnPc2	dutina
<g/>
,	,	kIx,	,
konzervaci	konzervace	k1gFnSc3	konzervace
skalicí	skalice	k1gFnSc7	skalice
modrou	modrý	k2eAgFnSc7d1	modrá
a	a	k8xC	a
zakrytí	zakrytí	k1gNnSc2	zakrytí
šindelovým	šindelový	k2eAgInSc7d1	šindelový
krytem	kryt	k1gInSc7	kryt
<g/>
.	.	kIx.	.
</s>
<s>
Koruny	koruna	k1gFnPc1	koruna
byly	být	k5eAaImAgFnP	být
staženy	stáhnout	k5eAaPmNgFnP	stáhnout
ocelovými	ocelový	k2eAgNnPc7d1	ocelové
lany	lano	k1gNnPc7	lano
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
musela	muset	k5eAaImAgFnS	muset
být	být	k5eAaImF	být
odstraněna	odstranit	k5eAaPmNgFnS	odstranit
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
větví	větev	k1gFnPc2	větev
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
došlo	dojít	k5eAaPmAgNnS	dojít
na	na	k7c4	na
prořez	prořez	k1gInSc4	prořez
koruny	koruna	k1gFnSc2	koruna
<g/>
,	,	kIx,	,
instalaci	instalace	k1gFnSc3	instalace
vazeb	vazba	k1gFnPc2	vazba
<g/>
,	,	kIx,	,
konzervaci	konzervace	k1gFnSc4	konzervace
dutiny	dutina	k1gFnSc2	dutina
<g/>
,	,	kIx,	,
opravu	oprava	k1gFnSc4	oprava
stříšky	stříška	k1gFnSc2	stříška
a	a	k8xC	a
ošetření	ošetření	k1gNnSc2	ošetření
odumřelých	odumřelý	k2eAgNnPc2d1	odumřelé
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
byl	být	k5eAaImAgInS	být
proveden	proveden	k2eAgInSc1d1	proveden
zdravotní	zdravotní	k2eAgInSc1d1	zdravotní
a	a	k8xC	a
redukční	redukční	k2eAgInSc1d1	redukční
řez	řez	k1gInSc1	řez
koruny	koruna	k1gFnSc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
historická	historický	k2eAgFnSc1d1	historická
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
obci	obec	k1gFnSc6	obec
Pastviny	pastvina	k1gFnSc2	pastvina
se	se	k3xPyFc4	se
datuje	datovat	k5eAaImIp3nS	datovat
do	do	k7c2	do
r.	r.	kA	r.
1513	[number]	k4	1513
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
lípy	lípa	k1gFnPc1	lípa
stály	stát	k5eAaImAgFnP	stát
již	již	k6eAd1	již
při	při	k7c6	při
založení	založení	k1gNnSc6	založení
vesnice	vesnice	k1gFnSc2	vesnice
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1200	[number]	k4	1200
stával	stávat	k5eAaImAgInS	stávat
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
současné	současný	k2eAgFnSc2d1	současná
hájovny	hájovna	k1gFnSc2	hájovna
(	(	kIx(	(
<g/>
č.	č.	k?	č.
21	[number]	k4	21
<g/>
)	)	kIx)	)
srub	srub	k1gInSc1	srub
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
užívali	užívat	k5eAaImAgMnP	užívat
pasáci	pasák	k1gMnPc1	pasák
vrchnosti	vrchnost	k1gFnSc2	vrchnost
ze	z	k7c2	z
Žampachu	Žampach	k1gInSc2	Žampach
<g/>
.	.	kIx.	.
</s>
<s>
Lípy	lípa	k1gFnPc1	lípa
údajně	údajně	k6eAd1	údajně
označovaly	označovat	k5eAaImAgFnP	označovat
roh	roh	k1gInSc4	roh
ohrady	ohrada	k1gFnSc2	ohrada
pro	pro	k7c4	pro
dobytek	dobytek	k1gInSc4	dobytek
<g/>
.	.	kIx.	.
</s>
<s>
Lípa	lípa	k1gFnSc1	lípa
označující	označující	k2eAgInSc4d1	označující
druhý	druhý	k4xOgInSc4	druhý
roh	roh	k1gInSc4	roh
<g/>
,	,	kIx,	,
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
od	od	k7c2	od
statku	statek	k1gInSc2	statek
(	(	kIx(	(
<g/>
druhá	druhý	k4xOgFnSc1	druhý
Vejdova	Vejdův	k2eAgFnSc1d1	Vejdova
lípa	lípa	k1gFnSc1	lípa
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgNnP	být
už	už	k6eAd1	už
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
popisovaná	popisovaný	k2eAgFnSc1d1	popisovaná
jako	jako	k8xS	jako
velice	velice	k6eAd1	velice
sešlá	sešlý	k2eAgFnSc1d1	sešlá
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
bez	bez	k7c2	bez
poloviny	polovina	k1gFnSc2	polovina
kmene	kmen	k1gInSc2	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgInSc4	třetí
roh	roh	k1gInSc4	roh
určovaly	určovat	k5eAaImAgInP	určovat
prý	prý	k9	prý
Bouchalovy	Bouchalův	k2eAgFnPc1d1	Bouchalova
lípy	lípa	k1gFnPc1	lípa
(	(	kIx(	(
<g/>
3	[number]	k4	3
stromy	strom	k1gInPc7	strom
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
snad	snad	k9	snad
vyrostly	vyrůst	k5eAaPmAgFnP	vyrůst
jako	jako	k8xS	jako
výmladky	výmladek	k1gInPc4	výmladek
původní	původní	k2eAgFnSc2d1	původní
lípy	lípa	k1gFnSc2	lípa
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
čtvrté	čtvrtý	k4xOgFnSc6	čtvrtý
lípě	lípa	k1gFnSc6	lípa
se	se	k3xPyFc4	se
neví	vědět	k5eNaImIp3nS	vědět
<g/>
.	.	kIx.	.
</s>
<s>
Vejdovy	Vejdův	k2eAgFnPc1d1	Vejdova
lípy	lípa	k1gFnPc1	lípa
získaly	získat	k5eAaPmAgFnP	získat
své	svůj	k3xOyFgNnSc4	svůj
přízvisko	přízvisko	k1gNnSc4	přízvisko
podle	podle	k7c2	podle
přezdívky	přezdívka	k1gFnSc2	přezdívka
jednoho	jeden	k4xCgMnSc2	jeden
z	z	k7c2	z
bývalých	bývalý	k2eAgMnPc2d1	bývalý
majitelů	majitel	k1gMnPc2	majitel
statku	statek	k1gInSc2	statek
<g/>
,	,	kIx,	,
pana	pan	k1gMnSc2	pan
Dolečka	doleček	k1gInSc2	doleček
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
sousedy	soused	k1gMnPc7	soused
proslavil	proslavit	k5eAaPmAgMnS	proslavit
větou	věta	k1gFnSc7	věta
"	"	kIx"	"
<g/>
Kdypak	kdypak	k6eAd1	kdypak
já	já	k3xPp1nSc1	já
na	na	k7c4	na
ten	ten	k3xDgInSc4	ten
kopec	kopec	k1gInSc4	kopec
vejda	vejdo	k1gNnSc2	vejdo
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
údajně	údajně	k6eAd1	údajně
pronášel	pronášet	k5eAaImAgInS	pronášet
při	při	k7c6	při
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
dolní	dolní	k2eAgFnSc2d1	dolní
hospody	hospody	k?	hospody
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Po	Po	kA	Po
"	"	kIx"	"
<g/>
Vejdovi	Vejda	k1gMnSc3	Vejda
<g/>
"	"	kIx"	"
pak	pak	k6eAd1	pak
byl	být	k5eAaImAgInS	být
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
původní	původní	k2eAgInSc1d1	původní
selský	selský	k2eAgInSc1d1	selský
dvůr	dvůr	k1gInSc1	dvůr
<g/>
,	,	kIx,	,
lípy	lípa	k1gFnPc1	lípa
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
i	i	k9	i
hájovna	hájovna	k1gFnSc1	hájovna
(	(	kIx(	(
<g/>
roku	rok	k1gInSc2	rok
1872	[number]	k4	1872
nahradila	nahradit	k5eAaPmAgFnS	nahradit
statek	statek	k1gInSc1	statek
<g/>
)	)	kIx)	)
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
i	i	k9	i
nová	nový	k2eAgFnSc1d1	nová
zástavba	zástavba	k1gFnSc1	zástavba
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Na	na	k7c6	na
Vejdovně	Vejdovna	k1gFnSc6	Vejdovna
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
po	po	k7c6	po
vysídlení	vysídlení	k1gNnSc6	vysídlení
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
stavby	stavba	k1gFnSc2	stavba
přehrady	přehrada	k1gFnSc2	přehrada
ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
40	[number]	k4	40
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vlastnil	vlastnit	k5eAaImAgMnS	vlastnit
hájovnu	hájovna	k1gFnSc4	hájovna
pan	pan	k1gMnSc1	pan
Josef	Josef	k1gMnSc1	Josef
Ježek	Ježek	k1gMnSc1	Ježek
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ji	on	k3xPp3gFnSc4	on
koupil	koupit	k5eAaPmAgMnS	koupit
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
musel	muset	k5eAaImAgMnS	muset
odstěhovat	odstěhovat	k5eAaPmF	odstěhovat
z	z	k7c2	z
usedlosti	usedlost	k1gFnSc2	usedlost
zaplavené	zaplavený	k2eAgFnSc2d1	zaplavená
přehradou	přehrada	k1gFnSc7	přehrada
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
pověsti	pověst	k1gFnSc2	pověst
do	do	k7c2	do
lípy	lípa	k1gFnSc2	lípa
kdysi	kdysi	k6eAd1	kdysi
zapadl	zapadnout	k5eAaPmAgMnS	zapadnout
pasáček	pasáček	k1gMnSc1	pasáček
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yQgInSc2	který
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
mnoha	mnoho	k4c6	mnoho
letech	léto	k1gNnPc6	léto
nalezena	nalézt	k5eAaBmNgFnS	nalézt
pouze	pouze	k6eAd1	pouze
kostra	kostra	k1gFnSc1	kostra
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
půlnoci	půlnoc	k1gFnSc2	půlnoc
je	být	k5eAaImIp3nS	být
prý	prý	k9	prý
z	z	k7c2	z
lípy	lípa	k1gFnSc2	lípa
slyšet	slyšet	k5eAaImF	slyšet
sten	sten	k1gInSc4	sten
a	a	k8xC	a
ve	v	k7c6	v
větvích	větev	k1gFnPc6	větev
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
světlo	světlo	k1gNnSc1	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Strašení	strašení	k1gNnSc1	strašení
zřejmě	zřejmě	k6eAd1	zřejmě
ustalo	ustat	k5eAaPmAgNnS	ustat
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
kmen	kmen	k1gInSc1	kmen
počátkem	počátkem	k7c2	počátkem
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
otevřel	otevřít	k5eAaPmAgMnS	otevřít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgNnPc6	tento
místech	místo	k1gNnPc6	místo
prý	prý	k9	prý
působil	působit	k5eAaImAgMnS	působit
i	i	k9	i
loupežník	loupežník	k1gMnSc1	loupežník
Ledříček	Ledříček	k1gMnSc1	Ledříček
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
si	se	k3xPyFc3	se
ukryl	ukrýt	k5eAaPmAgMnS	ukrýt
své	svůj	k3xOyFgInPc4	svůj
poklady	poklad	k1gInPc4	poklad
ve	v	k7c6	v
stráni	stráň	k1gFnSc6	stráň
pod	pod	k7c7	pod
lípou	lípa	k1gFnSc7	lípa
a	a	k8xC	a
nedalekém	daleký	k2eNgInSc6d1	nedaleký
lese	les	k1gInSc6	les
Sekyří	Sekyř	k1gFnPc2	Sekyř
poblíž	poblíž	k7c2	poblíž
tzv.	tzv.	kA	tzv.
Katova	katův	k2eAgInSc2d1	katův
dolu	dol	k1gInSc2	dol
<g/>
.	.	kIx.	.
</s>
<s>
Vejdova	Vejdův	k2eAgFnSc1d1	Vejdova
lípa	lípa	k1gFnSc1	lípa
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
(	(	kIx(	(
<g/>
poškození	poškození	k1gNnSc3	poškození
Tatrovické	Tatrovický	k2eAgFnSc2d1	Tatrovická
lípy	lípa	k1gFnSc2	lípa
<g/>
)	)	kIx)	)
vůbec	vůbec	k9	vůbec
nejmohutnějším	mohutný	k2eAgInSc7d3	nejmohutnější
stromem	strom	k1gInSc7	strom
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
lip	lípa	k1gFnPc2	lípa
bývala	bývat	k5eAaImAgFnS	bývat
krom	krom	k7c2	krom
Tatrovické	Tatrovický	k2eAgFnSc2d1	Tatrovická
(	(	kIx(	(
<g/>
1650	[number]	k4	1650
cm	cm	kA	cm
<g/>
)	)	kIx)	)
mohutnější	mohutný	k2eAgFnSc1d2	mohutnější
ještě	ještě	k6eAd1	ještě
Bzenecká	bzenecký	k2eAgFnSc1d1	Bzenecká
(	(	kIx(	(
<g/>
1400	[number]	k4	1400
cm	cm	kA	cm
<g/>
,	,	kIx,	,
do	do	k7c2	do
přelomu	přelom	k1gInSc2	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
a	a	k8xC	a
srovnatelné	srovnatelný	k2eAgNnSc1d1	srovnatelné
Běleňská	Běleňský	k2eAgFnSc1d1	Běleňský
(	(	kIx(	(
<g/>
1250	[number]	k4	1250
cm	cm	kA	cm
<g/>
,	,	kIx,	,
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
lípa	lípa	k1gFnSc1	lípa
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Losenici	Losenice	k1gFnSc6	Losenice
(	(	kIx(	(
<g/>
1256	[number]	k4	1256
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
dutiny	dutina	k1gFnSc2	dutina
stromu	strom	k1gInSc2	strom
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
dobového	dobový	k2eAgNnSc2d1	dobové
hlášení	hlášení	k1gNnSc2	hlášení
četnické	četnický	k2eAgFnSc2d1	četnická
stanice	stanice	k1gFnSc2	stanice
pohodlně	pohodlně	k6eAd1	pohodlně
vejde	vejít	k5eAaPmIp3nS	vejít
stůl	stůl	k1gInSc4	stůl
a	a	k8xC	a
čtyři	čtyři	k4xCgFnPc4	čtyři
židle	židle	k1gFnPc4	židle
<g/>
.	.	kIx.	.
</s>
<s>
Dočasně	dočasně	k6eAd1	dočasně
byla	být	k5eAaImAgFnS	být
využívána	využívat	k5eAaImNgFnS	využívat
i	i	k9	i
jako	jako	k8xC	jako
skladiště	skladiště	k1gNnSc1	skladiště
hospodářského	hospodářský	k2eAgNnSc2d1	hospodářské
nářadí	nářadí	k1gNnSc2	nářadí
<g/>
.	.	kIx.	.
</s>
<s>
Vejdově	Vejdův	k2eAgFnSc3d1	Vejdova
lípě	lípa	k1gFnSc3	lípa
byl	být	k5eAaImAgInS	být
věnován	věnovat	k5eAaImNgInS	věnovat
prostor	prostor	k1gInSc1	prostor
v	v	k7c6	v
televizním	televizní	k2eAgInSc6d1	televizní
pořadu	pořad	k1gInSc6	pořad
Paměť	paměť	k1gFnSc4	paměť
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
v	v	k7c6	v
dílu	díl	k1gInSc6	díl
č.	č.	k?	č.
4	[number]	k4	4
<g/>
:	:	kIx,	:
Stromy	strom	k1gInPc1	strom
žijí	žít	k5eAaImIp3nP	žít
s	s	k7c7	s
lidmi	člověk	k1gMnPc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Krom	krom	k7c2	krom
zmíněného	zmíněný	k2eAgNnSc2d1	zmíněné
literárního	literární	k2eAgNnSc2d1	literární
díla	dílo	k1gNnSc2	dílo
byla	být	k5eAaImAgFnS	být
lípa	lípa	k1gFnSc1	lípa
zachycena	zachycen	k2eAgFnSc1d1	zachycena
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
kresbách	kresba	k1gFnPc6	kresba
a	a	k8xC	a
malbách	malba	k1gFnPc6	malba
<g/>
.	.	kIx.	.
</s>
<s>
Prodávané	prodávaný	k2eAgFnPc1d1	prodávaná
byly	být	k5eAaImAgFnP	být
pohlednice	pohlednice	k1gFnPc1	pohlednice
s	s	k7c7	s
kresbami	kresba	k1gFnPc7	kresba
akademického	akademický	k2eAgMnSc2d1	akademický
malíře	malíř	k1gMnSc2	malíř
K.	K.	kA	K.
Rubeše	Rubeš	k1gMnSc2	Rubeš
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vlastivědné	vlastivědný	k2eAgFnSc6d1	vlastivědná
publikaci	publikace	k1gFnSc6	publikace
Žambersko	Žambersko	k1gNnSc1	Žambersko
byl	být	k5eAaImAgInS	být
uveden	uveden	k2eAgInSc1d1	uveden
dřevoryt	dřevoryt	k1gInSc1	dřevoryt
J.	J.	kA	J.
Krčmářové	Krčmářová	k1gFnSc2	Krčmářová
<g/>
.	.	kIx.	.
</s>
<s>
Zdařilé	zdařilý	k2eAgNnSc1d1	zdařilé
vyobrazení	vyobrazení	k1gNnSc1	vyobrazení
bylo	být	k5eAaImAgNnS	být
také	také	k9	také
uváděno	uvádět	k5eAaImNgNnS	uvádět
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
Krása	krása	k1gFnSc1	krása
našeho	náš	k3xOp1gInSc2	náš
domova	domov	k1gInSc2	domov
od	od	k7c2	od
Jana	Jan	k1gMnSc2	Jan
Březiny	Březina	k1gMnSc2	Březina
(	(	kIx(	(
<g/>
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lípu	lípa	k1gFnSc4	lípa
také	také	k9	také
zachytil	zachytit	k5eAaPmAgMnS	zachytit
akademický	akademický	k2eAgMnSc1d1	akademický
malíř	malíř	k1gMnSc1	malíř
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Turek	Turek	k1gMnSc1	Turek
nebo	nebo	k8xC	nebo
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Vladimír	Vladimír	k1gMnSc1	Vladimír
Lepš	Lepš	k1gMnSc1	Lepš
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bouchalovy	Bouchalův	k2eAgFnPc1d1	Bouchalova
lípy	lípa	k1gFnPc1	lípa
(	(	kIx(	(
<g/>
významné	významný	k2eAgInPc1d1	významný
stromy	strom	k1gInPc1	strom
<g/>
)	)	kIx)	)
lípy	lípa	k1gFnPc1	lípa
u	u	k7c2	u
křížku	křížek	k1gInSc2	křížek
v	v	k7c6	v
Pastvinách	pastvina	k1gFnPc6	pastvina
(	(	kIx(	(
<g/>
2	[number]	k4	2
stromy	strom	k1gInPc4	strom
<g/>
)	)	kIx)	)
Jirešův	Jirešův	k2eAgInSc4d1	Jirešův
javor	javor	k1gInSc4	javor
(	(	kIx(	(
<g/>
Klášterec	Klášterec	k1gInSc1	Klášterec
<g/>
)	)	kIx)	)
Jasany	jasan	k1gInPc1	jasan
v	v	k7c6	v
Klášterci	Klášterec	k1gInSc6	Klášterec
(	(	kIx(	(
<g/>
2	[number]	k4	2
stromy	strom	k1gInPc1	strom
<g/>
,	,	kIx,	,
Klášterec	Klášterec	k1gInSc1	Klášterec
<g/>
)	)	kIx)	)
Hloh	hloh	k1gInSc1	hloh
u	u	k7c2	u
Lhotky	Lhotka	k1gFnSc2	Lhotka
Klen	klen	k2eAgInSc4d1	klen
u	u	k7c2	u
kostela	kostel	k1gInSc2	kostel
(	(	kIx(	(
<g/>
Klášterec	Klášterec	k1gInSc1	Klášterec
<g/>
)	)	kIx)	)
Tománkova	Tománkův	k2eAgFnSc1d1	Tománkova
lípa	lípa	k1gFnSc1	lípa
(	(	kIx(	(
<g/>
Klášterec	Klášterec	k1gInSc1	Klášterec
<g/>
)	)	kIx)	)
Lípa	lípa	k1gFnSc1	lípa
u	u	k7c2	u
fary	fara	k1gFnSc2	fara
(	(	kIx(	(
<g/>
Nekoř	kořit	k5eNaImRp2nS	kořit
<g/>
)	)	kIx)	)
Nekořské	Nekořský	k2eAgFnPc1d1	Nekořský
lípy	lípa	k1gFnPc1	lípa
(	(	kIx(	(
<g/>
5	[number]	k4	5
stromů	strom	k1gInPc2	strom
<g/>
)	)	kIx)	)
Vejdovy	Vejdův	k2eAgFnPc1d1	Vejdova
lípy	lípa	k1gFnPc1	lípa
byly	být	k5eAaImAgFnP	být
podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
zdrojů	zdroj	k1gInPc2	zdroj
původně	původně	k6eAd1	původně
čtyři	čtyři	k4xCgMnPc4	čtyři
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
dochovala	dochovat	k5eAaPmAgFnS	dochovat
jediná	jediný	k2eAgFnSc1d1	jediná
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
Vejdova	Vejdův	k2eAgFnSc1d1	Vejdova
lípa	lípa	k1gFnSc1	lípa
rostla	růst	k5eAaImAgFnS	růst
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
statku	statek	k1gInSc2	statek
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
lípu	lípa	k1gFnSc4	lípa
velkolistou	velkolistý	k2eAgFnSc7d1	velkolistá
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
původní	původní	k2eAgInSc4d1	původní
kmen	kmen	k1gInSc4	kmen
se	se	k3xPyFc4	se
částečně	částečně	k6eAd1	částečně
rozpadl	rozpadnout	k5eAaPmAgMnS	rozpadnout
a	a	k8xC	a
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
zdání	zdání	k1gNnSc4	zdání
dvou	dva	k4xCgInPc2	dva
samostatných	samostatný	k2eAgInPc2d1	samostatný
stromů	strom	k1gInPc2	strom
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
z	z	k7c2	z
kmenů	kmen	k1gInPc2	kmen
visel	viset	k5eAaImAgInS	viset
obrázek	obrázek	k1gInSc1	obrázek
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc4	Václav
a	a	k8xC	a
prý	prý	k9	prý
pod	pod	k7c7	pod
ním	on	k3xPp3gNnSc7	on
sedávala	sedávat	k5eAaImAgFnS	sedávat
hospodyně	hospodyně	k1gFnSc1	hospodyně
oplakávající	oplakávající	k2eAgFnSc2d1	oplakávající
rodinné	rodinný	k2eAgFnSc2d1	rodinná
poměry	poměra	k1gFnSc2	poměra
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
téma	téma	k1gNnSc1	téma
je	být	k5eAaImIp3nS	být
literárně	literárně	k6eAd1	literárně
zpracované	zpracovaný	k2eAgInPc4d1	zpracovaný
Jožkou	Jožka	k1gMnSc7	Jožka
Žamberským	Žamberský	k2eAgMnSc7d1	Žamberský
v	v	k7c6	v
románu	román	k1gInSc6	román
Drama	dramo	k1gNnSc2	dramo
na	na	k7c6	na
Vejdově	Vejdův	k2eAgInSc6d1	Vejdův
statku	statek	k1gInSc6	statek
v	v	k7c6	v
Pastvinách	pastvina	k1gFnPc6	pastvina
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
40	[number]	k4	40
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
obvod	obvod	k1gInSc1	obvod
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
kmene	kmen	k1gInSc2	kmen
600	[number]	k4	600
cm	cm	kA	cm
a	a	k8xC	a
menší	malý	k2eAgInPc1d2	menší
340	[number]	k4	340
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc4	druhý
Vejdovu	Vejdův	k2eAgFnSc4d1	Vejdova
lípu	lípa	k1gFnSc4	lípa
zničila	zničit	k5eAaPmAgFnS	zničit
vichřice	vichřice	k1gFnSc1	vichřice
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1978	[number]	k4	1978
<g/>
.	.	kIx.	.
</s>
<s>
Lípa	lípa	k1gFnSc1	lípa
ale	ale	k9	ale
nezanikla	zaniknout	k5eNaPmAgFnS	zaniknout
úplně	úplně	k6eAd1	úplně
<g/>
,	,	kIx,	,
z	z	k7c2	z
živých	živý	k2eAgInPc2d1	živý
kořenů	kořen	k1gInPc2	kořen
vyrašil	vyrašit	k5eAaPmAgInS	vyrašit
výmladek	výmladek	k1gInSc1	výmladek
a	a	k8xC	a
časem	časem	k6eAd1	časem
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
vyrostl	vyrůst	k5eAaPmAgInS	vyrůst
mladý	mladý	k2eAgInSc1d1	mladý
stromek	stromek	k1gInSc1	stromek
<g/>
.	.	kIx.	.
</s>
