<p>
<s>
Skleníkový	skleníkový	k2eAgInSc1d1	skleníkový
efekt	efekt	k1gInSc1	efekt
<g/>
,	,	kIx,	,
také	také	k9	také
zvaný	zvaný	k2eAgInSc1d1	zvaný
skleníkový	skleníkový	k2eAgInSc1d1	skleníkový
jev	jev	k1gInSc1	jev
je	být	k5eAaImIp3nS	být
proces	proces	k1gInSc1	proces
<g/>
,	,	kIx,	,
kterým	který	k3yRgNnSc7	který
záření	záření	k1gNnSc4	záření
atmosféry	atmosféra	k1gFnSc2	atmosféra
planety	planeta	k1gFnSc2	planeta
ohřívá	ohřívat	k5eAaImIp3nS	ohřívat
povrch	povrch	k1gInSc4	povrch
planety	planeta	k1gFnSc2	planeta
na	na	k7c4	na
teplotu	teplota	k1gFnSc4	teplota
vyšší	vysoký	k2eAgFnSc4d2	vyšší
<g/>
,	,	kIx,	,
než	než	k8xS	než
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
bez	bez	k7c2	bez
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
atmosféra	atmosféra	k1gFnSc1	atmosféra
planety	planeta	k1gFnSc2	planeta
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
zářivě	zářivě	k6eAd1	zářivě
aktivní	aktivní	k2eAgInPc4d1	aktivní
plyny	plyn	k1gInPc4	plyn
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
skleníkové	skleníkový	k2eAgInPc1d1	skleníkový
plyny	plyn	k1gInPc1	plyn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
budou	být	k5eAaImBp3nP	být
vyzařovat	vyzařovat	k5eAaImF	vyzařovat
energii	energie	k1gFnSc4	energie
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
směrech	směr	k1gInPc6	směr
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
tohoto	tento	k3xDgNnSc2	tento
záření	záření	k1gNnSc2	záření
míří	mířit	k5eAaImIp3nS	mířit
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
povrchu	povrch	k1gInSc3	povrch
a	a	k8xC	a
otepluje	oteplovat	k5eAaImIp3nS	oteplovat
ho	on	k3xPp3gNnSc4	on
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Intenzita	intenzita	k1gFnSc1	intenzita
záření	záření	k1gNnSc2	záření
-	-	kIx~	-
tedy	tedy	k8xC	tedy
síla	síla	k1gFnSc1	síla
skleníkového	skleníkový	k2eAgInSc2d1	skleníkový
efektu	efekt	k1gInSc2	efekt
-	-	kIx~	-
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
teplotě	teplota	k1gFnSc6	teplota
atmosféry	atmosféra	k1gFnSc2	atmosféra
a	a	k8xC	a
na	na	k7c4	na
množství	množství	k1gNnSc4	množství
skleníkových	skleníkový	k2eAgInPc2d1	skleníkový
plynů	plyn	k1gInPc2	plyn
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
atmosféra	atmosféra	k1gFnSc1	atmosféra
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přirozený	přirozený	k2eAgInSc1d1	přirozený
skleníkový	skleníkový	k2eAgInSc1d1	skleníkový
efekt	efekt	k1gInSc1	efekt
Země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
rozhodující	rozhodující	k2eAgFnSc1d1	rozhodující
pro	pro	k7c4	pro
zachování	zachování	k1gNnSc4	zachování
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Lidská	lidský	k2eAgFnSc1d1	lidská
činnost	činnost	k1gFnSc1	činnost
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
spalování	spalování	k1gNnSc1	spalování
fosilních	fosilní	k2eAgNnPc2d1	fosilní
paliv	palivo	k1gNnPc2	palivo
a	a	k8xC	a
kácení	kácení	k1gNnSc6	kácení
lesů	les	k1gInPc2	les
<g/>
,	,	kIx,	,
zesilují	zesilovat	k5eAaImIp3nP	zesilovat
skleníkový	skleníkový	k2eAgInSc4d1	skleníkový
efekt	efekt	k1gInSc4	efekt
a	a	k8xC	a
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
globální	globální	k2eAgNnSc4d1	globální
oteplování	oteplování	k1gNnSc4	oteplování
<g/>
.	.	kIx.	.
<g/>
Pojem	pojem	k1gInSc1	pojem
"	"	kIx"	"
<g/>
skleníkový	skleníkový	k2eAgInSc1d1	skleníkový
efekt	efekt	k1gInSc1	efekt
<g/>
"	"	kIx"	"
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
z	z	k7c2	z
chybné	chybný	k2eAgFnSc2d1	chybná
analogie	analogie	k1gFnSc2	analogie
s	s	k7c7	s
účinkem	účinek	k1gInSc7	účinek
slunečního	sluneční	k2eAgNnSc2d1	sluneční
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
prochází	procházet	k5eAaImIp3nS	procházet
sklem	sklo	k1gNnSc7	sklo
a	a	k8xC	a
ohřívá	ohřívat	k5eAaImIp3nS	ohřívat
skleník	skleník	k1gInSc4	skleník
<g/>
.	.	kIx.	.
</s>
<s>
Způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
jakým	jaký	k3yIgNnSc7	jaký
skleník	skleník	k1gInSc1	skleník
zachovává	zachovávat	k5eAaImIp3nS	zachovávat
teplo	teplo	k1gNnSc4	teplo
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
však	však	k9	však
zásadně	zásadně	k6eAd1	zásadně
odlišný	odlišný	k2eAgInSc1d1	odlišný
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
skleník	skleník	k1gInSc1	skleník
pracuje	pracovat	k5eAaImIp3nS	pracovat
většinou	většinou	k6eAd1	většinou
snížením	snížení	k1gNnSc7	snížení
proudu	proud	k1gInSc2	proud
vzduchu	vzduch	k1gInSc2	vzduch
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zachoval	zachovat	k5eAaPmAgInS	zachovat
teplý	teplý	k2eAgInSc1d1	teplý
vzduch	vzduch	k1gInSc1	vzduch
uvnitř	uvnitř	k6eAd1	uvnitř
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Existenci	existence	k1gFnSc4	existence
skleníkového	skleníkový	k2eAgInSc2d1	skleníkový
efektu	efekt	k1gInSc2	efekt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1824	[number]	k4	1824
předpověděl	předpovědět	k5eAaPmAgMnS	předpovědět
francouzský	francouzský	k2eAgMnSc1d1	francouzský
fyzik	fyzik	k1gMnSc1	fyzik
a	a	k8xC	a
matematik	matematik	k1gMnSc1	matematik
Joseph	Joseph	k1gMnSc1	Joseph
Fourier	Fourier	k1gMnSc1	Fourier
<g/>
.	.	kIx.	.
</s>
<s>
Argument	argument	k1gInSc1	argument
a	a	k8xC	a
důkazy	důkaz	k1gInPc1	důkaz
byly	být	k5eAaImAgInP	být
dále	daleko	k6eAd2	daleko
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1827	[number]	k4	1827
a	a	k8xC	a
1838	[number]	k4	1838
podpořeny	podpořit	k5eAaPmNgInP	podpořit
francouzským	francouzský	k2eAgMnSc7d1	francouzský
fyzikem	fyzik	k1gMnSc7	fyzik
Claudem	Claud	k1gInSc7	Claud
Pouilletem	Pouillet	k1gInSc7	Pouillet
a	a	k8xC	a
odůvodněny	odůvodněn	k2eAgFnPc1d1	odůvodněna
experimentálními	experimentální	k2eAgNnPc7d1	experimentální
pozorováními	pozorování	k1gNnPc7	pozorování
irského	irský	k2eAgMnSc2d1	irský
fyzika	fyzik	k1gMnSc2	fyzik
Johna	John	k1gMnSc2	John
Tyndalla	Tyndall	k1gMnSc2	Tyndall
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1859	[number]	k4	1859
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
změřil	změřit	k5eAaPmAgInS	změřit
radiační	radiační	k2eAgFnPc4d1	radiační
vlastnosti	vlastnost	k1gFnPc4	vlastnost
určitých	určitý	k2eAgInPc2d1	určitý
skleníkových	skleníkový	k2eAgInPc2d1	skleníkový
plynů	plyn	k1gInPc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Účinek	účinek	k1gInSc1	účinek
byl	být	k5eAaImAgInS	být
plně	plně	k6eAd1	plně
vyčíslen	vyčíslit	k5eAaPmNgInS	vyčíslit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
švédským	švédský	k2eAgMnSc7d1	švédský
vědcem	vědec	k1gMnSc7	vědec
Svantem	Svant	k1gMnSc7	Svant
Arrheniusem	Arrhenius	k1gMnSc7	Arrhenius
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
provedl	provést	k5eAaPmAgInS	provést
první	první	k4xOgInSc1	první
kvantitativní	kvantitativní	k2eAgInSc1d1	kvantitativní
odhad	odhad	k1gInSc1	odhad
globálního	globální	k2eAgNnSc2d1	globální
oteplování	oteplování	k1gNnSc2	oteplování
následkem	následkem	k7c2	následkem
hypotetického	hypotetický	k2eAgNnSc2d1	hypotetické
zdvojnásobení	zdvojnásobení	k1gNnSc2	zdvojnásobení
atmosférického	atmosférický	k2eAgInSc2d1	atmosférický
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
<g/>
.	.	kIx.	.
</s>
<s>
Žádný	žádný	k3yNgMnSc1	žádný
z	z	k7c2	z
těchto	tento	k3xDgMnPc2	tento
vědců	vědec	k1gMnPc2	vědec
však	však	k9	však
nepoužil	použít	k5eNaPmAgMnS	použít
termín	termín	k1gInSc4	termín
"	"	kIx"	"
<g/>
skleníkový	skleníkový	k2eAgInSc4d1	skleníkový
efekt	efekt	k1gInSc4	efekt
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
ten	ten	k3xDgInSc1	ten
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
významu	význam	k1gInSc6	význam
poprvé	poprvé	k6eAd1	poprvé
použit	použít	k5eAaPmNgInS	použít
švédským	švédský	k2eAgMnSc7d1	švédský
meteorologem	meteorolog	k1gMnSc7	meteorolog
Nilsem	Nils	k1gMnSc7	Nils
Gustafem	Gustaf	k1gInSc7	Gustaf
Ekholmem	Ekholm	k1gInSc7	Ekholm
v	v	k7c4	v
1901	[number]	k4	1901
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mechanismus	mechanismus	k1gInSc1	mechanismus
==	==	k?	==
</s>
</p>
<p>
<s>
Země	země	k1gFnSc1	země
přijímá	přijímat	k5eAaImIp3nS	přijímat
energii	energie	k1gFnSc4	energie
ze	z	k7c2	z
Slunce	slunce	k1gNnSc2	slunce
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
ultrafialového	ultrafialový	k2eAgNnSc2d1	ultrafialové
<g/>
,	,	kIx,	,
viditelného	viditelný	k2eAgNnSc2d1	viditelné
a	a	k8xC	a
blízkého	blízký	k2eAgNnSc2d1	blízké
infračerveného	infračervený	k2eAgNnSc2d1	infračervené
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
26	[number]	k4	26
%	%	kIx~	%
přicházející	přicházející	k2eAgFnSc2d1	přicházející
sluneční	sluneční	k2eAgFnSc2d1	sluneční
energie	energie	k1gFnSc2	energie
se	se	k3xPyFc4	se
atmosférou	atmosféra	k1gFnSc7	atmosféra
a	a	k8xC	a
mraky	mrak	k1gInPc1	mrak
odráží	odrážet	k5eAaImIp3nP	odrážet
zpátky	zpátky	k6eAd1	zpátky
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
a	a	k8xC	a
19	[number]	k4	19
%	%	kIx~	%
energie	energie	k1gFnSc1	energie
atmosféra	atmosféra	k1gFnSc1	atmosféra
a	a	k8xC	a
mraky	mrak	k1gInPc1	mrak
absorbují	absorbovat	k5eAaBmIp3nP	absorbovat
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
zbývající	zbývající	k2eAgFnSc2d1	zbývající
energie	energie	k1gFnSc2	energie
je	být	k5eAaImIp3nS	být
absorbována	absorbovat	k5eAaBmNgFnS	absorbovat
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
povrch	povrch	k1gInSc4	povrch
Země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
chladnější	chladný	k2eAgMnSc1d2	chladnější
než	než	k8xS	než
Slunce	slunce	k1gNnSc1	slunce
<g/>
,	,	kIx,	,
vyzařuje	vyzařovat	k5eAaImIp3nS	vyzařovat
na	na	k7c6	na
vlnových	vlnový	k2eAgFnPc6d1	vlnová
délkách	délka	k1gFnPc6	délka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
mnohem	mnohem	k6eAd1	mnohem
delší	dlouhý	k2eAgFnPc4d2	delší
než	než	k8xS	než
vlnové	vlnový	k2eAgFnPc4d1	vlnová
délky	délka	k1gFnPc4	délka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
absorbovány	absorbovat	k5eAaBmNgFnP	absorbovat
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
tohoto	tento	k3xDgNnSc2	tento
tepelného	tepelný	k2eAgNnSc2d1	tepelné
záření	záření	k1gNnSc2	záření
(	(	kIx(	(
<g/>
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
absorbována	absorbován	k2eAgFnSc1d1	absorbována
atmosférou	atmosféra	k1gFnSc7	atmosféra
a	a	k8xC	a
ohřívá	ohřívat	k5eAaImIp3nS	ohřívat
ji	on	k3xPp3gFnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Atmosféra	atmosféra	k1gFnSc1	atmosféra
také	také	k9	také
získává	získávat	k5eAaImIp3nS	získávat
teplo	teplo	k1gNnSc4	teplo
na	na	k7c4	na
ohřátí	ohřátý	k2eAgMnPc1d1	ohřátý
vzduchu	vzduch	k1gInSc2	vzduch
a	a	k8xC	a
latentní	latentní	k2eAgNnSc4d1	latentní
teplo	teplo	k1gNnSc4	teplo
proudící	proudící	k2eAgNnSc4d1	proudící
z	z	k7c2	z
povrchu	povrch	k1gInSc2	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Atmosféra	atmosféra	k1gFnSc1	atmosféra
vyzařuje	vyzařovat	k5eAaImIp3nS	vyzařovat
energii	energie	k1gFnSc4	energie
jak	jak	k8xS	jak
nahoru	nahoru	k6eAd1	nahoru
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
dolů	dolů	k6eAd1	dolů
<g/>
;	;	kIx,	;
část	část	k1gFnSc1	část
vyzařovaná	vyzařovaný	k2eAgFnSc1d1	vyzařovaná
dolů	dolů	k6eAd1	dolů
je	být	k5eAaImIp3nS	být
absorbována	absorbovat	k5eAaBmNgFnS	absorbovat
povrchem	povrch	k1gInSc7	povrch
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
vyšší	vysoký	k2eAgFnSc3d2	vyšší
rovnovážné	rovnovážný	k2eAgFnSc3d1	rovnovážná
teplotě	teplota	k1gFnSc3	teplota
<g/>
,	,	kIx,	,
než	než	k8xS	než
kdyby	kdyby	kYmCp3nS	kdyby
atmosféru	atmosféra	k1gFnSc4	atmosféra
neměla	mít	k5eNaImAgFnS	mít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ideální	ideální	k2eAgNnSc1d1	ideální
tepelně	tepelně	k6eAd1	tepelně
vodivé	vodivý	k2eAgNnSc1d1	vodivé
černé	černý	k2eAgNnSc1d1	černé
těleso	těleso	k1gNnSc1	těleso
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
jako	jako	k8xC	jako
Země	zem	k1gFnSc2	zem
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
teplotu	teplota	k1gFnSc4	teplota
kolem	kolem	k7c2	kolem
5,3	[number]	k4	5,3
°	°	k?	°
<g/>
C.	C.	kA	C.
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Země	země	k1gFnSc1	země
odráží	odrážet	k5eAaImIp3nS	odrážet
přibližně	přibližně	k6eAd1	přibližně
30	[number]	k4	30
%	%	kIx~	%
přicházejícího	přicházející	k2eAgNnSc2d1	přicházející
slunečního	sluneční	k2eAgNnSc2d1	sluneční
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
by	by	kYmCp3nS	by
tato	tento	k3xDgFnSc1	tento
efektivní	efektivní	k2eAgFnSc1d1	efektivní
teplota	teplota	k1gFnSc1	teplota
ideální	ideální	k2eAgFnSc2d1	ideální
planety	planeta	k1gFnSc2	planeta
(	(	kIx(	(
<g/>
teplota	teplota	k1gFnSc1	teplota
černého	černý	k2eAgNnSc2d1	černé
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
<g />
.	.	kIx.	.
</s>
<s>
by	by	kYmCp3nS	by
vyzařovala	vyzařovat	k5eAaImAgFnS	vyzařovat
stejné	stejný	k2eAgNnSc4d1	stejné
množství	množství	k1gNnSc4	množství
záření	záření	k1gNnSc2	záření
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
asi	asi	k9	asi
-18	-18	k4	-18
°	°	k?	°
<g/>
C.	C.	kA	C.
Povrchová	povrchový	k2eAgFnSc1d1	povrchová
teplota	teplota	k1gFnSc1	teplota
této	tento	k3xDgFnSc2	tento
hypotetické	hypotetický	k2eAgFnSc2d1	hypotetická
planety	planeta	k1gFnSc2	planeta
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
33	[number]	k4	33
°	°	k?	°
<g/>
C	C	kA	C
nižší	nízký	k2eAgFnPc1d2	nižší
než	než	k8xS	než
aktuální	aktuální	k2eAgFnSc1d1	aktuální
teplota	teplota	k1gFnSc1	teplota
povrchu	povrch	k1gInSc2	povrch
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
14	[number]	k4	14
°	°	k?	°
<g/>
C.	C.	kA	C.
<g/>
Základní	základní	k2eAgInSc1d1	základní
mechanismus	mechanismus	k1gInSc1	mechanismus
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
kvalifikován	kvalifikovat	k5eAaBmNgMnS	kvalifikovat
mnoha	mnoho	k4c7	mnoho
způsoby	způsob	k1gInPc7	způsob
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
žádný	žádný	k3yNgInSc1	žádný
neovlivňuje	ovlivňovat	k5eNaImIp3nS	ovlivňovat
základní	základní	k2eAgInSc1d1	základní
proces	proces	k1gInSc1	proces
<g/>
.	.	kIx.	.
</s>
<s>
Atmosféra	atmosféra	k1gFnSc1	atmosféra
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
povrchu	povrch	k1gInSc2	povrch
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
neprůhledná	průhledný	k2eNgFnSc1d1	neprůhledná
pro	pro	k7c4	pro
tepelné	tepelný	k2eAgNnSc4d1	tepelné
záření	záření	k1gNnSc4	záření
(	(	kIx(	(
<g/>
s	s	k7c7	s
důležitými	důležitý	k2eAgFnPc7d1	důležitá
výjimkami	výjimka	k1gFnPc7	výjimka
pro	pro	k7c4	pro
"	"	kIx"	"
<g/>
průhledné	průhledný	k2eAgInPc4d1	průhledný
<g/>
"	"	kIx"	"
pásy	pás	k1gInPc1	pás
<g/>
)	)	kIx)	)
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
tepelných	tepelný	k2eAgFnPc2d1	tepelná
ztrát	ztráta	k1gFnPc2	ztráta
z	z	k7c2	z
povrchu	povrch	k1gInSc2	povrch
je	být	k5eAaImIp3nS	být
vlastním	vlastní	k2eAgNnSc7d1	vlastní
teplem	teplo	k1gNnSc7	teplo
a	a	k8xC	a
skupenským	skupenský	k2eAgInSc7d1	skupenský
přenosem	přenos	k1gInSc7	přenos
tepla	teplo	k1gNnSc2	teplo
<g/>
.	.	kIx.	.
</s>
<s>
Radiační	radiační	k2eAgFnPc1d1	radiační
energetické	energetický	k2eAgFnPc1d1	energetická
ztráty	ztráta	k1gFnPc1	ztráta
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
stále	stále	k6eAd1	stále
důležitější	důležitý	k2eAgMnSc1d2	důležitější
<g/>
,	,	kIx,	,
především	především	k9	především
kvůli	kvůli	k7c3	kvůli
klesající	klesající	k2eAgFnSc3d1	klesající
koncentraci	koncentrace	k1gFnSc3	koncentrace
vodní	vodní	k2eAgFnSc2d1	vodní
páry	pára	k1gFnSc2	pára
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
důležitým	důležitý	k2eAgInSc7d1	důležitý
skleníkovým	skleníkový	k2eAgInSc7d1	skleníkový
plynem	plyn	k1gInSc7	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
realističtější	realistický	k2eAgNnSc1d2	realističtější
myslet	myslet	k5eAaImF	myslet
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
skleníkový	skleníkový	k2eAgInSc1d1	skleníkový
efekt	efekt	k1gInSc1	efekt
se	se	k3xPyFc4	se
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
na	na	k7c4	na
"	"	kIx"	"
<g/>
povrch	povrch	k1gInSc4	povrch
<g/>
"	"	kIx"	"
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
části	část	k1gFnSc6	část
troposféry	troposféra	k1gFnSc2	troposféra
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
účinně	účinně	k6eAd1	účinně
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
povrchem	povrch	k1gInSc7	povrch
teplotním	teplotní	k2eAgInSc7d1	teplotní
gradientem	gradient	k1gInSc7	gradient
<g/>
.	.	kIx.	.
</s>
<s>
Jednoduchý	jednoduchý	k2eAgInSc1d1	jednoduchý
obraz	obraz	k1gInSc1	obraz
také	také	k9	také
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
ustálený	ustálený	k2eAgInSc4d1	ustálený
stav	stav	k1gInSc4	stav
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
reálném	reálný	k2eAgInSc6d1	reálný
světě	svět	k1gInSc6	svět
existují	existovat	k5eAaImIp3nP	existovat
odchylky	odchylka	k1gFnPc4	odchylka
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
denního	denní	k2eAgInSc2d1	denní
cyklu	cyklus	k1gInSc2	cyklus
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
sezónního	sezónní	k2eAgInSc2d1	sezónní
cyklu	cyklus	k1gInSc2	cyklus
a	a	k8xC	a
povětrnostních	povětrnostní	k2eAgFnPc2d1	povětrnostní
poruch	porucha	k1gFnPc2	porucha
<g/>
.	.	kIx.	.
</s>
<s>
Solární	solární	k2eAgInSc1d1	solární
ohřev	ohřev	k1gInSc1	ohřev
se	se	k3xPyFc4	se
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
pouze	pouze	k6eAd1	pouze
během	během	k7c2	během
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
noci	noc	k1gFnSc2	noc
se	se	k3xPyFc4	se
atmosféra	atmosféra	k1gFnSc1	atmosféra
trochu	trochu	k6eAd1	trochu
ochlazuje	ochlazovat	k5eAaImIp3nS	ochlazovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
velice	velice	k6eAd1	velice
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
její	její	k3xOp3gFnSc1	její
emisivita	emisivita	k1gFnSc1	emisivita
je	být	k5eAaImIp3nS	být
nízká	nízký	k2eAgFnSc1d1	nízká
<g/>
.	.	kIx.	.
</s>
<s>
Denní	denní	k2eAgNnSc1d1	denní
kolísání	kolísání	k1gNnSc1	kolísání
teploty	teplota	k1gFnSc2	teplota
se	se	k3xPyFc4	se
snižuje	snižovat	k5eAaImIp3nS	snižovat
s	s	k7c7	s
výškou	výška	k1gFnSc7	výška
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
důležité	důležitý	k2eAgInPc1d1	důležitý
radiační	radiační	k2eAgInPc1d1	radiační
účinky	účinek	k1gInPc1	účinek
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
popis	popis	k1gInSc1	popis
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
daný	daný	k2eAgInSc1d1	daný
idealizovaným	idealizovaný	k2eAgInSc7d1	idealizovaný
skleníkovým	skleníkový	k2eAgInSc7d1	skleníkový
modelem	model	k1gInSc7	model
<g/>
,	,	kIx,	,
stává	stávat	k5eAaImIp3nS	stávat
realistickým	realistický	k2eAgMnSc7d1	realistický
<g/>
.	.	kIx.	.
</s>
<s>
Zemský	zemský	k2eAgInSc4d1	zemský
povrch	povrch	k1gInSc4	povrch
<g/>
,	,	kIx,	,
ohřátý	ohřátý	k2eAgInSc4d1	ohřátý
na	na	k7c4	na
teplotu	teplota	k1gFnSc4	teplota
okolo	okolo	k7c2	okolo
255	[number]	k4	255
K	K	kA	K
(	(	kIx(	(
<g/>
-	-	kIx~	-
<g/>
18	[number]	k4	18
°C	°C	k?	°C
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vyzařuje	vyzařovat	k5eAaImIp3nS	vyzařovat
dlouhovlnné	dlouhovlnný	k2eAgNnSc1d1	dlouhovlnné
<g/>
,	,	kIx,	,
infračervené	infračervený	k2eAgNnSc1d1	infračervené
teplo	teplo	k1gNnSc1	teplo
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
4-100	[number]	k4	4-100
μ	μ	k?	μ
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
těchto	tento	k3xDgFnPc6	tento
vlnových	vlnový	k2eAgFnPc6d1	vlnová
délkách	délka	k1gFnPc6	délka
jsou	být	k5eAaImIp3nP	být
skleníkové	skleníkový	k2eAgInPc1d1	skleníkový
plyny	plyn	k1gInPc1	plyn
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgInP	být
převážně	převážně	k6eAd1	převážně
průhledné	průhledný	k2eAgInPc1d1	průhledný
pro	pro	k7c4	pro
příchozí	příchozí	k1gFnSc4	příchozí
sluneční	sluneční	k2eAgNnSc1d1	sluneční
záření	záření	k1gNnSc1	záření
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
absorpční	absorpční	k2eAgFnSc1d1	absorpční
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
vrstva	vrstva	k1gFnSc1	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
se	s	k7c7	s
skleníkovými	skleníkový	k2eAgInPc7d1	skleníkový
plyny	plyn	k1gInPc7	plyn
pohlcuje	pohlcovat	k5eAaImIp3nS	pohlcovat
část	část	k1gFnSc4	část
tepla	teplo	k1gNnSc2	teplo
vyzařované	vyzařovaný	k2eAgNnSc4d1	vyzařované
nahoru	nahoru	k6eAd1	nahoru
ze	z	k7c2	z
spodních	spodní	k2eAgFnPc2d1	spodní
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Probíhá	probíhat	k5eAaImIp3nS	probíhat
opakování	opakování	k1gNnSc1	opakování
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
směrech	směr	k1gInPc6	směr
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
nahoru	nahoru	k6eAd1	nahoru
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
dolů	dolů	k6eAd1	dolů
<g/>
;	;	kIx,	;
v	v	k7c6	v
rovnováze	rovnováha	k1gFnSc6	rovnováha
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
definice	definice	k1gFnSc2	definice
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
stejné	stejný	k2eAgNnSc1d1	stejné
množství	množství	k1gNnSc1	množství
<g/>
,	,	kIx,	,
jaké	jaký	k3yQgNnSc1	jaký
se	se	k3xPyFc4	se
absorbovalo	absorbovat	k5eAaBmAgNnS	absorbovat
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
větší	veliký	k2eAgFnSc4d2	veliký
teplotu	teplota	k1gFnSc4	teplota
dole	dole	k6eAd1	dole
<g/>
.	.	kIx.	.
</s>
<s>
Zvýšení	zvýšení	k1gNnSc1	zvýšení
koncentrace	koncentrace	k1gFnSc2	koncentrace
plynů	plyn	k1gInPc2	plyn
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
množství	množství	k1gNnSc1	množství
absorpce	absorpce	k1gFnSc2	absorpce
a	a	k8xC	a
opětovné	opětovný	k2eAgNnSc1d1	opětovné
záření	záření	k1gNnSc1	záření
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
dále	daleko	k6eAd2	daleko
ohřívá	ohřívat	k5eAaImIp3nS	ohřívat
vrstvy	vrstva	k1gFnPc4	vrstva
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
i	i	k9	i
povrch	povrch	k1gInSc4	povrch
pod	pod	k7c7	pod
nimi	on	k3xPp3gMnPc7	on
<g/>
.	.	kIx.	.
<g/>
Skleníkové	skleníkový	k2eAgInPc1d1	skleníkový
plyny	plyn	k1gInPc1	plyn
-	-	kIx~	-
včetně	včetně	k7c2	včetně
většiny	většina	k1gFnSc2	většina
dvoumolekulových	dvoumolekulový	k2eAgInPc2d1	dvoumolekulový
atomů	atom	k1gInPc2	atom
plynů	plyn	k1gInPc2	plyn
se	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
různými	různý	k2eAgInPc7d1	různý
atomy	atom	k1gInPc7	atom
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
oxid	oxid	k1gInSc1	oxid
uhelnatý	uhelnatý	k2eAgInSc1d1	uhelnatý
<g/>
,	,	kIx,	,
CO	co	k3yInSc1	co
<g/>
)	)	kIx)	)
a	a	k8xC	a
všechny	všechen	k3xTgInPc4	všechen
plyny	plyn	k1gInPc4	plyn
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
atomy	atom	k1gInPc1	atom
-	-	kIx~	-
jsou	být	k5eAaImIp3nP	být
schopny	schopen	k2eAgFnPc1d1	schopna
absorbovat	absorbovat	k5eAaBmF	absorbovat
a	a	k8xC	a
vyzařovat	vyzařovat	k5eAaImF	vyzařovat
infračervené	infračervený	k2eAgNnSc4d1	infračervené
záření	záření	k1gNnSc4	záření
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
více	hodně	k6eAd2	hodně
než	než	k8xS	než
99	[number]	k4	99
%	%	kIx~	%
suché	suchý	k2eAgFnSc2d1	suchá
atmosféry	atmosféra	k1gFnSc2	atmosféra
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
infračervené	infračervený	k2eAgNnSc4d1	infračervené
záření	záření	k1gNnSc4	záření
průhledné	průhledný	k2eAgFnSc2d1	průhledná
(	(	kIx(	(
<g/>
protože	protože	k8xS	protože
hlavní	hlavní	k2eAgFnPc1d1	hlavní
složky	složka	k1gFnPc1	složka
-	-	kIx~	-
N	N	kA	N
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
O2	O2	k1gFnSc1	O2
a	a	k8xC	a
argon	argon	k1gInSc1	argon
-	-	kIx~	-
nejsou	být	k5eNaImIp3nP	být
schopny	schopen	k2eAgFnPc1d1	schopna
přímo	přímo	k6eAd1	přímo
absorbovat	absorbovat	k5eAaBmF	absorbovat
nebo	nebo	k8xC	nebo
vyzařovat	vyzařovat	k5eAaImF	vyzařovat
infračervené	infračervený	k2eAgNnSc4d1	infračervené
záření	záření	k1gNnSc4	záření
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mezimolekulové	mezimolekulový	k2eAgFnPc1d1	mezimolekulový
srážky	srážka	k1gFnPc1	srážka
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
energie	energie	k1gFnSc1	energie
pohlcovaná	pohlcovaný	k2eAgFnSc1d1	pohlcovaná
a	a	k8xC	a
vyzařovaná	vyzařovaný	k2eAgFnSc1d1	vyzařovaná
skleníkovými	skleníkový	k2eAgInPc7d1	skleníkový
plyny	plyn	k1gInPc7	plyn
je	být	k5eAaImIp3nS	být
sdílena	sdílen	k2eAgFnSc1d1	sdílena
s	s	k7c7	s
ostatními	ostatní	k2eAgInPc7d1	ostatní
plyny	plyn	k1gInPc7	plyn
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
nejsou	být	k5eNaImIp3nP	být
aktivní	aktivní	k2eAgInPc1d1	aktivní
v	v	k7c6	v
infračervené	infračervený	k2eAgFnSc6d1	infračervená
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
IPCC	IPCC	kA	IPCC
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
nepravděpodobné	pravděpodobný	k2eNgNnSc1d1	nepravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
lidskou	lidský	k2eAgFnSc7d1	lidská
činností	činnost	k1gFnSc7	činnost
skleníkový	skleníkový	k2eAgInSc1d1	skleníkový
efekt	efekt	k1gInSc1	efekt
vymkl	vymknout	k5eAaPmAgInS	vymknout
kontrole	kontrola	k1gFnSc3	kontrola
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
a	a	k8xC	a
'	'	kIx"	'
<g/>
runaway	runawa	k2eAgFnPc1d1	runawa
greenhouse	greenhouse	k1gFnPc1	greenhouse
effect	effecta	k1gFnPc2	effecta
<g/>
'	'	kIx"	'
<g/>
–	–	k?	–
<g/>
analogous	analogous	k1gMnSc1	analogous
to	ten	k3xDgNnSc4	ten
[	[	kIx(	[
<g/>
that	that	k1gInSc1	that
of	of	k?	of
<g/>
]	]	kIx)	]
Venus	Venus	k1gMnSc1	Venus
<g/>
–	–	k?	–
<g/>
appears	appears	k6eAd1	appears
to	ten	k3xDgNnSc4	ten
have	hav	k1gFnPc1	hav
virtually	virtualnout	k5eAaPmAgFnP	virtualnout
no	no	k9	no
chance	chanec	k1gInSc2	chanec
of	of	k?	of
being	being	k1gMnSc1	being
induced	induced	k1gMnSc1	induced
by	by	kYmCp3nS	by
anthropogenic	anthropogenice	k1gFnPc2	anthropogenice
activities	activities	k1gInSc1	activities
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
==	==	k?	==
Skleníkové	skleníkový	k2eAgInPc1d1	skleníkový
plyny	plyn	k1gInPc1	plyn
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
jejich	jejich	k3xOp3gInSc2	jejich
procentního	procentní	k2eAgInSc2d1	procentní
podílu	podíl	k1gInSc2	podíl
na	na	k7c6	na
skleníkovém	skleníkový	k2eAgInSc6d1	skleníkový
efektu	efekt	k1gInSc6	efekt
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
jsou	být	k5eAaImIp3nP	být
čtyři	čtyři	k4xCgInPc1	čtyři
hlavní	hlavní	k2eAgInPc1d1	hlavní
plyny	plyn	k1gInPc1	plyn
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
vodní	vodní	k2eAgFnSc1d1	vodní
pára	pára	k1gFnSc1	pára
<g/>
,	,	kIx,	,
36	[number]	k4	36
<g/>
–	–	k?	–
<g/>
70	[number]	k4	70
%	%	kIx~	%
</s>
</p>
<p>
<s>
oxid	oxid	k1gInSc1	oxid
uhličitý	uhličitý	k2eAgInSc1d1	uhličitý
<g/>
,	,	kIx,	,
9	[number]	k4	9
<g/>
–	–	k?	–
<g/>
26	[number]	k4	26
%	%	kIx~	%
</s>
</p>
<p>
<s>
methan	methan	k1gInSc1	methan
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
9	[number]	k4	9
%	%	kIx~	%
</s>
</p>
<p>
<s>
ozón	ozón	k1gInSc1	ozón
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
%	%	kIx~	%
<g/>
Každému	každý	k3xTgMnSc3	každý
plynu	plyn	k1gInSc2	plyn
nelze	lze	k6eNd1	lze
přiřadit	přiřadit	k5eAaPmF	přiřadit
přesné	přesný	k2eAgNnSc4d1	přesné
procento	procento	k1gNnSc4	procento
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
absorpční	absorpční	k2eAgInPc1d1	absorpční
a	a	k8xC	a
emisní	emisní	k2eAgInPc1d1	emisní
pásy	pás	k1gInPc1	pás
plynů	plyn	k1gInPc2	plyn
se	se	k3xPyFc4	se
překrývají	překrývat	k5eAaImIp3nP	překrývat
(	(	kIx(	(
<g/>
proto	proto	k8xC	proto
jsou	být	k5eAaImIp3nP	být
výše	vysoce	k6eAd2	vysoce
uvedeny	uveden	k2eAgInPc1d1	uveden
rozsahy	rozsah	k1gInPc1	rozsah
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mraky	mrak	k1gInPc1	mrak
také	také	k9	také
absorbují	absorbovat	k5eAaBmIp3nP	absorbovat
a	a	k8xC	a
vyzařují	vyzařovat	k5eAaImIp3nP	vyzařovat
infračervené	infračervený	k2eAgNnSc4d1	infračervené
záření	záření	k1gNnSc4	záření
a	a	k8xC	a
tím	ten	k3xDgInSc7	ten
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
radiační	radiační	k2eAgFnPc1d1	radiační
vlastnosti	vlastnost	k1gFnPc1	vlastnost
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Role	role	k1gFnSc1	role
ve	v	k7c6	v
změně	změna	k1gFnSc6	změna
klimatu	klima	k1gNnSc2	klima
==	==	k?	==
</s>
</p>
<p>
<s>
Zesílení	zesílení	k1gNnSc1	zesílení
skleníkového	skleníkový	k2eAgInSc2d1	skleníkový
efektu	efekt	k1gInSc2	efekt
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
lidské	lidský	k2eAgFnSc2d1	lidská
činnosti	činnost	k1gFnSc2	činnost
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
jako	jako	k8xC	jako
zvýšený	zvýšený	k2eAgInSc1d1	zvýšený
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
antropogenní	antropogenní	k2eAgInSc4d1	antropogenní
<g/>
)	)	kIx)	)
skleníkový	skleníkový	k2eAgInSc4d1	skleníkový
efekt	efekt	k1gInSc4	efekt
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
nárůst	nárůst	k1gInSc1	nárůst
radiačního	radiační	k2eAgNnSc2d1	radiační
působení	působení	k1gNnSc2	působení
z	z	k7c2	z
lidské	lidský	k2eAgFnSc2d1	lidská
činnosti	činnost	k1gFnSc2	činnost
je	být	k5eAaImIp3nS	být
důsledkem	důsledek	k1gInSc7	důsledek
zejména	zejména	k9	zejména
zvýšení	zvýšení	k1gNnSc3	zvýšení
úrovně	úroveň	k1gFnSc2	úroveň
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
nejnovější	nový	k2eAgFnSc2d3	nejnovější
hodnotící	hodnotící	k2eAgFnSc2d1	hodnotící
zprávy	zpráva	k1gFnSc2	zpráva
Mezivládního	mezivládní	k2eAgInSc2d1	mezivládní
panelu	panel	k1gInSc2	panel
o	o	k7c6	o
změně	změna	k1gFnSc6	změna
klimatu	klima	k1gNnSc2	klima
"	"	kIx"	"
<g/>
atmosférické	atmosférický	k2eAgFnPc1d1	atmosférická
koncentrace	koncentrace	k1gFnPc1	koncentrace
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
<g/>
,	,	kIx,	,
metanu	metan	k1gInSc2	metan
a	a	k8xC	a
oxidu	oxid	k1gInSc2	oxid
dusného	dusný	k2eAgInSc2d1	dusný
jsou	být	k5eAaImIp3nP	být
za	za	k7c4	za
posledních	poslední	k2eAgInPc2d1	poslední
800	[number]	k4	800
000	[number]	k4	000
letech	let	k1gInPc6	let
bezprecedentní	bezprecedentní	k2eAgInSc1d1	bezprecedentní
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc7	jejich
účinky	účinek	k1gInPc7	účinek
společně	společně	k6eAd1	společně
s	s	k7c7	s
jinými	jiný	k2eAgFnPc7d1	jiná
antropogenními	antropogenní	k2eAgFnPc7d1	antropogenní
silami	síla	k1gFnPc7	síla
byly	být	k5eAaImAgFnP	být
zjištěny	zjistit	k5eAaPmNgFnP	zjistit
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
klimatickém	klimatický	k2eAgInSc6d1	klimatický
systému	systém	k1gInSc6	systém
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
byly	být	k5eAaImAgFnP	být
dominantní	dominantní	k2eAgFnSc7d1	dominantní
příčinou	příčina	k1gFnSc7	příčina
pozorovaného	pozorovaný	k2eAgNnSc2d1	pozorované
oteplování	oteplování	k1gNnSc2	oteplování
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
CO	co	k6eAd1	co
<g/>
2	[number]	k4	2
vzniká	vznikat	k5eAaImIp3nS	vznikat
spalováním	spalování	k1gNnSc7	spalování
fosilních	fosilní	k2eAgNnPc2d1	fosilní
paliv	palivo	k1gNnPc2	palivo
a	a	k8xC	a
dalšími	další	k2eAgFnPc7d1	další
činnostmi	činnost	k1gFnPc7	činnost
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
výroba	výroba	k1gFnSc1	výroba
cementu	cement	k1gInSc2	cement
a	a	k8xC	a
odlesňování	odlesňování	k1gNnSc2	odlesňování
tropů	trop	k1gInPc2	trop
<g/>
.	.	kIx.	.
</s>
<s>
Měření	měření	k1gNnSc1	měření
CO2	CO2	k1gFnSc2	CO2
na	na	k7c6	na
observatoři	observatoř	k1gFnSc6	observatoř
Mauna	Maun	k1gMnSc2	Maun
Loa	Loa	k1gMnSc2	Loa
na	na	k7c6	na
Havaji	Havaj	k1gFnSc6	Havaj
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
koncentrace	koncentrace	k1gFnSc1	koncentrace
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
zvýšily	zvýšit	k5eAaPmAgFnP	zvýšit
z	z	k7c2	z
přibližně	přibližně	k6eAd1	přibližně
hodnoty	hodnota	k1gFnPc1	hodnota
313	[number]	k4	313
ppm	ppm	k?	ppm
na	na	k7c4	na
na	na	k7c4	na
přibližně	přibližně	k6eAd1	přibližně
389	[number]	k4	389
ppm	ppm	k?	ppm
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
9	[number]	k4	9
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2013	[number]	k4	2013
dosáhly	dosáhnout	k5eAaPmAgFnP	dosáhnout
koncentrace	koncentrace	k1gFnPc1	koncentrace
milníku	milník	k1gInSc2	milník
400	[number]	k4	400
ppm	ppm	k?	ppm
<g/>
.	.	kIx.	.
</s>
<s>
Současné	současný	k2eAgNnSc1d1	současné
zjištěné	zjištěný	k2eAgNnSc1d1	zjištěné
množství	množství	k1gNnSc1	množství
CO2	CO2	k1gMnPc2	CO2
překračuje	překračovat	k5eAaImIp3nS	překračovat
maximální	maximální	k2eAgFnPc4d1	maximální
hodnoty	hodnota	k1gFnPc4	hodnota
geologických	geologický	k2eAgInPc2d1	geologický
záznamů	záznam	k1gInPc2	záznam
(	(	kIx(	(
<g/>
~	~	kIx~	~
300	[number]	k4	300
ppm	ppm	k?	ppm
<g/>
)	)	kIx)	)
z	z	k7c2	z
údajů	údaj	k1gInPc2	údaj
ledových	ledový	k2eAgNnPc2d1	ledové
vrtných	vrtný	k2eAgNnPc2d1	vrtné
jader	jádro	k1gNnPc2	jádro
(	(	kIx(	(
<g/>
ledové	ledový	k2eAgInPc1d1	ledový
vývrty	vývrt	k1gInPc1	vývrt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Účinek	účinek	k1gInSc1	účinek
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
vzniklého	vzniklý	k2eAgInSc2d1	vzniklý
spalováním	spalování	k1gNnSc7	spalování
na	na	k7c4	na
globální	globální	k2eAgNnSc4d1	globální
klima	klima	k1gNnSc4	klima
<g/>
,	,	kIx,	,
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
případ	případ	k1gInSc4	případ
skleníkového	skleníkový	k2eAgInSc2d1	skleníkový
efektu	efekt	k1gInSc2	efekt
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
popsaný	popsaný	k2eAgInSc1d1	popsaný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
Svantem	Svant	k1gMnSc7	Svant
Arrheniusem	Arrhenius	k1gMnSc7	Arrhenius
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
také	také	k9	také
nazývá	nazývat	k5eAaImIp3nS	nazývat
Callendarův	Callendarův	k2eAgInSc4d1	Callendarův
účinek	účinek	k1gInSc4	účinek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Údaje	údaj	k1gInPc4	údaj
z	z	k7c2	z
ledových	ledový	k2eAgNnPc2d1	ledové
vrtných	vrtný	k2eAgNnPc2d1	vrtné
jader	jádro	k1gNnPc2	jádro
z	z	k7c2	z
uplynulých	uplynulý	k2eAgInPc2d1	uplynulý
800	[number]	k4	800
000	[number]	k4	000
let	léto	k1gNnPc2	léto
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
koncentrace	koncentrace	k1gFnSc1	koncentrace
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
se	se	k3xPyFc4	se
pohybovala	pohybovat	k5eAaImAgFnS	pohybovat
od	od	k7c2	od
hodnot	hodnota	k1gFnPc2	hodnota
od	od	k7c2	od
180	[number]	k4	180
ppm	ppm	k?	ppm
do	do	k7c2	do
před	před	k7c4	před
industriální	industriální	k2eAgInSc4d1	industriální
úrovně	úroveň	k1gFnSc2	úroveň
270	[number]	k4	270
ppm	ppm	k?	ppm
<g/>
.	.	kIx.	.
</s>
<s>
Paleoklimatologové	Paleoklimatolog	k1gMnPc1	Paleoklimatolog
považují	považovat	k5eAaImIp3nP	považovat
změny	změna	k1gFnPc4	změna
v	v	k7c6	v
koncentraci	koncentrace	k1gFnSc6	koncentrace
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
za	za	k7c4	za
zásadní	zásadní	k2eAgInSc4d1	zásadní
faktor	faktor	k1gInSc4	faktor
ovlivňující	ovlivňující	k2eAgFnSc2d1	ovlivňující
změny	změna	k1gFnSc2	změna
klimatu	klima	k1gNnSc2	klima
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
časovém	časový	k2eAgNnSc6d1	časové
měřítku	měřítko	k1gNnSc6	měřítko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vědecké	vědecký	k2eAgNnSc1d1	vědecké
potvrzení	potvrzení	k1gNnSc1	potvrzení
role	role	k1gFnSc2	role
skleníkových	skleníkový	k2eAgInPc2d1	skleníkový
plynů	plyn	k1gInPc2	plyn
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
vědeckých	vědecký	k2eAgInPc2d1	vědecký
modelů	model	k1gInPc2	model
fungování	fungování	k1gNnSc2	fungování
klimatu	klima	k1gNnSc2	klima
byly	být	k5eAaImAgInP	být
definovány	definován	k2eAgInPc1d1	definován
následující	následující	k2eAgInPc1d1	následující
následky	následek	k1gInPc1	následek
zvyšování	zvyšování	k1gNnSc1	zvyšování
koncentrací	koncentrace	k1gFnPc2	koncentrace
skleníkových	skleníkový	k2eAgInPc2d1	skleníkový
plynů	plyn	k1gInPc2	plyn
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
CO	co	k8xS	co
<g/>
2	[number]	k4	2
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
ztenčování	ztenčování	k1gNnSc1	ztenčování
vrchních	vrchní	k2eAgFnPc2d1	vrchní
vrstev	vrstva	k1gFnPc2	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
pokles	pokles	k1gInSc1	pokles
teploty	teplota	k1gFnSc2	teplota
horních	horní	k2eAgFnPc2d1	horní
vrstev	vrstva	k1gFnPc2	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
rychlejší	rychlý	k2eAgInSc4d2	rychlejší
vzestup	vzestup	k1gInSc4	vzestup
nočních	noční	k2eAgFnPc2d1	noční
teplot	teplota	k1gFnPc2	teplota
proti	proti	k7c3	proti
denním	denní	k2eAgFnPc3d1	denní
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
rychlejší	rychlý	k2eAgInSc4d2	rychlejší
vzestup	vzestup	k1gInSc4	vzestup
zimních	zimní	k2eAgFnPc2d1	zimní
teplot	teplota	k1gFnPc2	teplota
proti	proti	k7c3	proti
letním	letní	k2eAgFnPc3d1	letní
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
nižší	nízký	k2eAgNnSc1d2	nižší
vyzařování	vyzařování	k1gNnSc1	vyzařování
tepla	teplo	k1gNnSc2	teplo
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
snižování	snižování	k1gNnSc1	snižování
obsahu	obsah	k1gInSc2	obsah
kyslíku	kyslík	k1gInSc2	kyslík
v	v	k7c6	v
ovzduší	ovzduší	k1gNnSc6	ovzduší
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
zvyšování	zvyšování	k1gNnSc1	zvyšování
koncentrace	koncentrace	k1gFnSc2	koncentrace
CO2	CO2	k1gFnSc2	CO2
v	v	k7c6	v
ovzduší	ovzduší	k1gNnSc6	ovzduší
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
vyšší	vysoký	k2eAgFnSc1d2	vyšší
koncentrace	koncentrace	k1gFnSc1	koncentrace
CO2	CO2	k1gFnPc2	CO2
ze	z	k7c2	z
spalovacích	spalovací	k2eAgInPc2d1	spalovací
procesů	proces	k1gInPc2	proces
ve	v	k7c6	v
stromech	strom	k1gInPc6	strom
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
větší	veliký	k2eAgNnSc4d2	veliký
množství	množství	k1gNnSc4	množství
infračerveného	infračervený	k2eAgNnSc2d1	infračervené
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
navrací	navracet	k5eAaBmIp3nS	navracet
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
zvyšování	zvyšování	k1gNnSc1	zvyšování
teploty	teplota	k1gFnSc2	teplota
oceánů	oceán	k1gInPc2	oceán
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
větší	veliký	k2eAgFnSc1d2	veliký
koncentrace	koncentrace	k1gFnSc1	koncentrace
CO2	CO2	k1gFnPc2	CO2
ze	z	k7c2	z
spalovacích	spalovací	k2eAgInPc2d1	spalovací
procesů	proces	k1gInPc2	proces
v	v	k7c6	v
oceánech	oceán	k1gInPc6	oceán
a	a	k8xC	a
korálech	korál	k1gInPc6	korál
<g/>
.	.	kIx.	.
<g/>
Všechny	všechen	k3xTgInPc4	všechen
vyjmenované	vyjmenovaný	k2eAgInPc4d1	vyjmenovaný
jevy	jev	k1gInPc4	jev
byla	být	k5eAaImAgFnS	být
potvrzeny	potvrzen	k2eAgInPc4d1	potvrzen
měřením	měření	k1gNnSc7	měření
těchto	tento	k3xDgInPc2	tento
jevů	jev	k1gInPc2	jev
při	při	k7c6	při
použití	použití	k1gNnSc6	použití
mnoha	mnoho	k4c2	mnoho
různých	různý	k2eAgFnPc2d1	různá
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
metod	metoda	k1gFnPc2	metoda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Skutečný	skutečný	k2eAgInSc4d1	skutečný
skleník	skleník	k1gInSc4	skleník
==	==	k?	==
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Skleníkový	skleníkový	k2eAgInSc1d1	skleníkový
efekt	efekt	k1gInSc1	efekt
<g/>
"	"	kIx"	"
atmosféry	atmosféra	k1gFnSc2	atmosféra
je	být	k5eAaImIp3nS	být
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
analogicky	analogicky	k6eAd1	analogicky
k	k	k7c3	k
skleníkům	skleník	k1gInPc3	skleník
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
za	za	k7c2	za
slunečního	sluneční	k2eAgNnSc2d1	sluneční
záření	záření	k1gNnSc2	záření
stávají	stávat	k5eAaImIp3nP	stávat
teplejšími	teplý	k2eAgNnPc7d2	teplejší
<g/>
.	.	kIx.	.
</s>
<s>
Skleník	skleník	k1gInSc1	skleník
však	však	k9	však
primárně	primárně	k6eAd1	primárně
neohřívá	ohřívat	k5eNaImIp3nS	ohřívat
"	"	kIx"	"
<g/>
skleníkový	skleníkový	k2eAgInSc1d1	skleníkový
efekt	efekt	k1gInSc1	efekt
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Skleníkový	skleníkový	k2eAgInSc1d1	skleníkový
efekt	efekt	k1gInSc1	efekt
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
nesprávným	správný	k2eNgInSc7d1	nesprávný
názvem	název	k1gInSc7	název
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
vytápění	vytápění	k1gNnSc4	vytápění
v	v	k7c6	v
běžném	běžný	k2eAgInSc6d1	běžný
skleníku	skleník	k1gInSc6	skleník
je	být	k5eAaImIp3nS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
snížením	snížení	k1gNnSc7	snížení
konvekce	konvekce	k1gFnSc2	konvekce
zatímco	zatímco	k8xS	zatímco
"	"	kIx"	"
<g/>
skleníkový	skleníkový	k2eAgInSc1d1	skleníkový
efekt	efekt	k1gInSc1	efekt
<g/>
"	"	kIx"	"
funguje	fungovat	k5eAaImIp3nS	fungovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
brání	bránit	k5eAaImIp3nS	bránit
absorbovanému	absorbovaný	k2eAgNnSc3d1	absorbované
teplu	teplo	k1gNnSc3	teplo
opouštět	opouštět	k5eAaImF	opouštět
strukturu	struktura	k1gFnSc4	struktura
radiačním	radiační	k2eAgInSc7d1	radiační
přenosem	přenos	k1gInSc7	přenos
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Skleník	skleník	k1gInSc1	skleník
je	být	k5eAaImIp3nS	být
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
z	z	k7c2	z
jakéhokoliv	jakýkoliv	k3yIgInSc2	jakýkoliv
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
prochází	procházet	k5eAaImIp3nS	procházet
sluneční	sluneční	k2eAgNnSc1d1	sluneční
světlo	světlo	k1gNnSc1	světlo
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
ze	z	k7c2	z
skla	sklo	k1gNnSc2	sklo
nebo	nebo	k8xC	nebo
plastu	plast	k1gInSc2	plast
<g/>
.	.	kIx.	.
</s>
<s>
Slunce	slunce	k1gNnSc1	slunce
ohřívá	ohřívat	k5eAaImIp3nS	ohřívat
zem	zem	k1gFnSc4	zem
a	a	k8xC	a
obsah	obsah	k1gInSc4	obsah
uvnitř	uvnitř	k7c2	uvnitř
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
venku	venku	k6eAd1	venku
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
pak	pak	k6eAd1	pak
ohřívá	ohřívat	k5eAaImIp3nS	ohřívat
vzduch	vzduch	k1gInSc1	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Venku	venku	k6eAd1	venku
se	se	k3xPyFc4	se
teplý	teplý	k2eAgInSc1d1	teplý
vzduch	vzduch	k1gInSc1	vzduch
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
povrchu	povrch	k1gInSc2	povrch
zvedá	zvedat	k5eAaImIp3nS	zvedat
a	a	k8xC	a
mísí	mísit	k5eAaImIp3nS	mísit
se	se	k3xPyFc4	se
s	s	k7c7	s
chladnějším	chladný	k2eAgInSc7d2	chladnější
vzduchem	vzduch	k1gInSc7	vzduch
<g/>
,	,	kIx,	,
udržuje	udržovat	k5eAaImIp3nS	udržovat
teplotu	teplota	k1gFnSc4	teplota
nižší	nízký	k2eAgFnSc4d2	nižší
než	než	k8xS	než
uvnitř	uvnitř	k6eAd1	uvnitř
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vzduch	vzduch	k1gInSc1	vzduch
nadále	nadále	k6eAd1	nadále
zahřívá	zahřívat	k5eAaImIp3nS	zahřívat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
omezen	omezit	k5eAaPmNgInS	omezit
na	na	k7c4	na
skleník	skleník	k1gInSc4	skleník
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
lze	lze	k6eAd1	lze
dokázat	dokázat	k5eAaPmF	dokázat
otevřením	otevření	k1gNnSc7	otevření
malého	malý	k2eAgNnSc2d1	malé
okna	okno	k1gNnSc2	okno
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
střechy	střecha	k1gFnSc2	střecha
skleníku	skleník	k1gInSc2	skleník
<g/>
:	:	kIx,	:
teplota	teplota	k1gFnSc1	teplota
začne	začít	k5eAaPmIp3nS	začít
výrazně	výrazně	k6eAd1	výrazně
klesat	klesat	k5eAaImF	klesat
<g/>
.	.	kIx.	.
</s>
<s>
Experimentálně	experimentálně	k6eAd1	experimentálně
bylo	být	k5eAaImAgNnS	být
demonstrováno	demonstrovat	k5eAaBmNgNnS	demonstrovat
(	(	kIx(	(
<g/>
Robertem	Robert	k1gMnSc7	Robert
Woodem	Wood	k1gMnSc7	Wood
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1909	[number]	k4	1909
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
že	že	k8xS	že
(	(	kIx(	(
<g/>
nevyhřívaný	vyhřívaný	k2eNgMnSc1d1	nevyhřívaný
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
skleník	skleník	k1gInSc1	skleník
<g/>
"	"	kIx"	"
pokrytý	pokrytý	k2eAgInSc1d1	pokrytý
horninovou	horninový	k2eAgFnSc7d1	horninová
solí	sůl	k1gFnSc7	sůl
<g/>
,	,	kIx,	,
halitem	halit	k1gInSc7	halit
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
průhledný	průhledný	k2eAgInSc1d1	průhledný
vůči	vůči	k7c3	vůči
infračervenému	infračervený	k2eAgNnSc3d1	infračervené
záření	záření	k1gNnSc3	záření
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
ohřívá	ohřívat	k5eAaImIp3nS	ohřívat
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
jiný	jiný	k2eAgMnSc1d1	jiný
<g/>
,	,	kIx,	,
zakrytý	zakrytý	k2eAgMnSc1d1	zakrytý
sklem	sklo	k1gNnSc7	sklo
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
skleníky	skleník	k1gInPc1	skleník
fungují	fungovat	k5eAaImIp3nP	fungovat
především	především	k9	především
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zabraňují	zabraňovat	k5eAaImIp3nP	zabraňovat
konvektivnímu	konvektivní	k2eAgNnSc3d1	konvektivní
ochlazování	ochlazování	k1gNnSc3	ochlazování
<g/>
.	.	kIx.	.
<g/>
Vyhřívané	vyhřívaný	k2eAgInPc1d1	vyhřívaný
skleníky	skleník	k1gInPc1	skleník
jsou	být	k5eAaImIp3nP	být
další	další	k2eAgFnSc7d1	další
záležitostí	záležitost	k1gFnSc7	záležitost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
vnitřní	vnitřní	k2eAgInSc4d1	vnitřní
zdroj	zdroj	k1gInSc4	zdroj
tepla	teplo	k1gNnSc2	teplo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
chce	chtít	k5eAaImIp3nS	chtít
uniknout	uniknout	k5eAaPmF	uniknout
ven	ven	k6eAd1	ven
<g/>
,	,	kIx,	,
čemuž	což	k3yRnSc3	což
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
zabránit	zabránit	k5eAaPmF	zabránit
<g/>
.	.	kIx.	.
</s>
<s>
Dává	dávat	k5eAaImIp3nS	dávat
tedy	tedy	k9	tedy
smysl	smysl	k1gInSc4	smysl
se	se	k3xPyFc4	se
pokusit	pokusit	k5eAaPmF	pokusit
zabránit	zabránit	k5eAaPmF	zabránit
radiačnímu	radiační	k2eAgNnSc3d1	radiační
ochlazení	ochlazení	k1gNnSc3	ochlazení
pomocí	pomocí	k7c2	pomocí
vhodného	vhodný	k2eAgNnSc2d1	vhodné
zasklení	zasklení	k1gNnSc2	zasklení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
efekty	efekt	k1gInPc1	efekt
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Protiskleníkový	Protiskleníkový	k2eAgInSc1d1	Protiskleníkový
efekt	efekt	k1gInSc1	efekt
===	===	k?	===
</s>
</p>
<p>
<s>
Protiskleníkový	Protiskleníkový	k2eAgInSc1d1	Protiskleníkový
efekt	efekt	k1gInSc1	efekt
je	být	k5eAaImIp3nS	být
mechanismus	mechanismus	k1gInSc1	mechanismus
podobný	podobný	k2eAgInSc1d1	podobný
a	a	k8xC	a
symetrický	symetrický	k2eAgMnSc1d1	symetrický
vůči	vůči	k7c3	vůči
skleníkovému	skleníkový	k2eAgInSc3d1	skleníkový
efektu	efekt	k1gInSc3	efekt
<g/>
:	:	kIx,	:
skleníkový	skleníkový	k2eAgInSc1d1	skleníkový
efekt	efekt	k1gInSc1	efekt
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
propouští	propouštět	k5eAaImIp3nS	propouštět
záření	záření	k1gNnSc4	záření
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
dovoloval	dovolovat	k5eAaImAgInS	dovolovat
tepelné	tepelný	k2eAgNnSc4d1	tepelné
vyzařování	vyzařování	k1gNnSc4	vyzařování
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
ohřívá	ohřívat	k5eAaImIp3nS	ohřívat
povrch	povrch	k1gInSc4	povrch
těla	tělo	k1gNnSc2	tělo
<g/>
;	;	kIx,	;
anti-skleníkový	antikleníkový	k2eAgInSc1d1	anti-skleníkový
efekt	efekt	k1gInSc1	efekt
je	být	k5eAaImIp3nS	být
kolem	kolem	k7c2	kolem
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
neumožňuje	umožňovat	k5eNaImIp3nS	umožňovat
záření	záření	k1gNnSc4	záření
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
tepelné	tepelný	k2eAgNnSc4d1	tepelné
vyzařování	vyzařování	k1gNnSc4	vyzařování
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
snižuje	snižovat	k5eAaImIp3nS	snižovat
rovnovážná	rovnovážný	k2eAgFnSc1d1	rovnovážná
povrchová	povrchový	k2eAgFnSc1d1	povrchová
teplota	teplota	k1gFnSc1	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Takový	takový	k3xDgInSc1	takový
účinek	účinek	k1gInSc1	účinek
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
o	o	k7c6	o
saturnově	saturnův	k2eAgInSc6d1	saturnův
měsíci	měsíc	k1gInSc6	měsíc
Titanu	titan	k1gInSc2	titan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Skrytý	skrytý	k2eAgInSc1d1	skrytý
skleníkový	skleníkový	k2eAgInSc1d1	skleníkový
efekt	efekt	k1gInSc1	efekt
===	===	k?	===
</s>
</p>
<p>
<s>
Skrytý	skrytý	k2eAgInSc1d1	skrytý
skleníkový	skleníkový	k2eAgInSc1d1	skleníkový
efekt	efekt	k1gInSc1	efekt
nastane	nastat	k5eAaPmIp3nS	nastat
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
kladná	kladný	k2eAgFnSc1d1	kladná
zpětná	zpětný	k2eAgFnSc1d1	zpětná
vazba	vazba	k1gFnSc1	vazba
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
vypařování	vypařování	k1gNnSc3	vypařování
všech	všecek	k3xTgInPc2	všecek
skleníkových	skleníkový	k2eAgInPc2d1	skleníkový
plynů	plyn	k1gInPc2	plyn
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
skrytém	skrytý	k2eAgInSc6d1	skrytý
skleníkovém	skleníkový	k2eAgInSc6d1	skleníkový
efektu	efekt	k1gInSc6	efekt
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
oxid	oxid	k1gInSc1	oxid
uhličitý	uhličitý	k2eAgInSc1d1	uhličitý
a	a	k8xC	a
vodní	vodní	k2eAgFnSc4d1	vodní
páru	pára	k1gFnSc4	pára
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
již	již	k6eAd1	již
dlouho	dlouho	k6eAd1	dlouho
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nastal	nastat	k5eAaPmAgInS	nastat
na	na	k7c4	na
Venuši	Venuše	k1gFnSc4	Venuše
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Tělesa	těleso	k1gNnPc1	těleso
jiné	jiný	k2eAgFnSc2d1	jiná
než	než	k8xS	než
Země	zem	k1gFnSc2	zem
==	==	k?	==
</s>
</p>
<p>
<s>
Skleníkový	skleníkový	k2eAgInSc1d1	skleníkový
efekt	efekt	k1gInSc1	efekt
na	na	k7c4	na
Venuši	Venuše	k1gFnSc4	Venuše
je	být	k5eAaImIp3nS	být
zvláště	zvláště	k6eAd1	zvláště
velký	velký	k2eAgInSc1d1	velký
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jeho	jeho	k3xOp3gFnSc1	jeho
hustá	hustý	k2eAgFnSc1d1	hustá
atmosféra	atmosféra	k1gFnSc1	atmosféra
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
především	především	k6eAd1	především
z	z	k7c2	z
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Venuše	Venuše	k1gFnSc2	Venuše
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
zažila	zažít	k5eAaPmAgFnS	zažít
skleníkový	skleníkový	k2eAgInSc4d1	skleníkový
efekt	efekt	k1gInSc4	efekt
a	a	k8xC	a
očekáváme	očekávat	k5eAaImIp1nP	očekávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
bude	být	k5eAaImBp3nS	být
efekt	efekt	k1gInSc1	efekt
trvat	trvat	k5eAaImF	trvat
asi	asi	k9	asi
2	[number]	k4	2
miliardy	miliarda	k4xCgFnSc2	miliarda
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
sluneční	sluneční	k2eAgNnSc1d1	sluneční
záření	záření	k1gNnSc1	záření
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Titan	titan	k1gInSc1	titan
má	mít	k5eAaImIp3nS	mít
protiskleníkový	protiskleníkový	k2eAgInSc1d1	protiskleníkový
efekt	efekt	k1gInSc1	efekt
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jeho	jeho	k3xOp3gFnSc1	jeho
atmosféra	atmosféra	k1gFnSc1	atmosféra
pohlcuje	pohlcovat	k5eAaImIp3nS	pohlcovat
sluneční	sluneční	k2eAgNnSc1d1	sluneční
záření	záření	k1gNnSc1	záření
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
průhledná	průhledný	k2eAgFnSc1d1	průhledná
pro	pro	k7c4	pro
odchozí	odchozí	k2eAgNnSc4d1	odchozí
infračervené	infračervený	k2eAgNnSc4d1	infračervené
záření	záření	k1gNnSc4	záření
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Trpasličí	trpasličí	k2eAgFnSc1d1	trpasličí
planeta	planeta	k1gFnSc1	planeta
Pluto	Pluto	k1gNnSc4	Pluto
je	být	k5eAaImIp3nS	být
také	také	k9	také
chladnější	chladný	k2eAgMnSc1d2	chladnější
<g/>
,	,	kIx,	,
než	než	k8xS	než
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
dalo	dát	k5eAaPmAgNnS	dát
očekávat	očekávat	k5eAaImF	očekávat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ho	on	k3xPp3gMnSc4	on
ochlazuje	ochlazovat	k5eAaImIp3nS	ochlazovat
odpařování	odpařování	k1gNnSc1	odpařování
dusíku	dusík	k1gInSc2	dusík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Greenhouse	Greenhouse	k1gFnSc2	Greenhouse
effect	effecta	k1gFnPc2	effecta
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Skleníkové	skleníkový	k2eAgInPc1d1	skleníkový
plyny	plyn	k1gInPc1	plyn
</s>
</p>
<p>
<s>
Kjótský	Kjótský	k2eAgInSc1d1	Kjótský
protokol	protokol	k1gInSc1	protokol
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Businger	Businger	k1gMnSc1	Businger
<g/>
,	,	kIx,	,
Joost	Joost	k1gMnSc1	Joost
Alois	Alois	k1gMnSc1	Alois
<g/>
;	;	kIx,	;
Fleagle	Fleagle	k1gInSc1	Fleagle
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
Guthrie	Guthrie	k1gFnSc2	Guthrie
<g/>
.	.	kIx.	.
</s>
<s>
An	An	k?	An
introduction	introduction	k1gInSc1	introduction
to	ten	k3xDgNnSc1	ten
atmospheric	atmospheric	k1gMnSc1	atmospheric
physics	physics	k1gInSc1	physics
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
San	San	k?	San
Diego	Diego	k1gMnSc1	Diego
<g/>
:	:	kIx,	:
Academic	Academic	k1gMnSc1	Academic
<g/>
,	,	kIx,	,
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
International	International	k1gFnSc1	International
geophysics	geophysics	k1gInSc1	geophysics
series	series	k1gInSc1	series
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
260355	[number]	k4	260355
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Henderson-Sellers	Henderson-Sellers	k1gInSc1	Henderson-Sellers
<g/>
,	,	kIx,	,
Ann	Ann	k1gFnSc1	Ann
<g/>
;	;	kIx,	;
McGuffie	McGuffie	k1gFnSc1	McGuffie
<g/>
,	,	kIx,	,
Kendal	Kendal	k1gFnSc1	Kendal
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
climate	climat	k1gInSc5	climat
modelling	modelling	k1gInSc1	modelling
primer	primera	k1gFnPc2	primera
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
Wiley	Wiley	k1gInPc1	Wiley
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
470	[number]	k4	470
<g/>
-	-	kIx~	-
<g/>
85750	[number]	k4	85750
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
VÍDEN	VÍDEN	kA	VÍDEN
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
<g/>
.	.	kIx.	.
</s>
<s>
Chemie	chemie	k1gFnSc1	chemie
ovzduší	ovzduší	k1gNnSc2	ovzduší
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
VŠCHT	VŠCHT	kA	VŠCHT
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2017	[number]	k4	2017
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
28	[number]	k4	28
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
14	[number]	k4	14
Skleníkový	skleníkový	k2eAgInSc1d1	skleníkový
efekt	efekt	k1gInSc1	efekt
<g/>
,	,	kIx,	,
s.	s.	k?	s.
90-97	[number]	k4	90-97
z	z	k7c2	z
98	[number]	k4	98
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7080	[number]	k4	7080
<g/>
-	-	kIx~	-
<g/>
571	[number]	k4	571
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Rutgers	Rutgers	k1gInSc1	Rutgers
University	universita	k1gFnSc2	universita
<g/>
:	:	kIx,	:
Earth	Earth	k1gInSc1	Earth
Radiation	Radiation	k1gInSc1	Radiation
Budget	budget	k1gInSc1	budget
(	(	kIx(	(
<g/>
bilance	bilance	k1gFnSc1	bilance
zemského	zemský	k2eAgNnSc2d1	zemské
záření	záření	k1gNnSc2	záření
<g/>
)	)	kIx)	)
</s>
</p>
