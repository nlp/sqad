<s>
Oscar	Oscar	k1gMnSc1
E.	E.	kA
Monnig	Monnig	k1gMnSc1
(	(	kIx(
<g/>
4	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1902	#num#	k4
–	–	k?
4	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1999	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
americký	americký	k2eAgMnSc1d1
amatérský	amatérský	k2eAgMnSc1d1
astronom	astronom	k1gMnSc1
<g/>
,	,	kIx,
známý	známý	k2eAgMnSc1d1
především	především	k6eAd1
pro	pro	k7c4
své	svůj	k3xOyFgInPc4
příspěvky	příspěvek	k1gInPc4
ke	k	k7c3
studiu	studio	k1gNnSc3
meteoritů	meteorit	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Založil	založit	k5eAaPmAgInS
rozsáhlou	rozsáhlý	k2eAgFnSc4d1
meteoritickou	meteoritický	k2eAgFnSc4d1
sbírku	sbírka	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
později	pozdě	k6eAd2
daroval	darovat	k5eAaPmAgMnS
Texaské	texaský	k2eAgFnSc3d1
křesťanské	křesťanský	k2eAgFnSc3d1
univerzitě	univerzita	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>