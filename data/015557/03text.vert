<s>
Oscar	Oscar	k1gMnSc1
E.	E.	kA
Monnig	Monnig	k1gMnSc1
</s>
<s>
Oscar	Oscar	k1gInSc1
E.	E.	kA
Monnig	Monnig	k1gInSc4
Narození	narození	k1gNnSc2
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1902	#num#	k4
<g/>
Fort	Fort	k?
Worth	Worth	k1gMnSc1
Úmrtí	úmrtí	k1gNnSc1
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1999	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
96	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Fort	Fort	k?
Worth	Worth	k1gInSc1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
Právní	právní	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
Texaské	texaský	k2eAgFnSc2d1
university	universita	k1gFnSc2
Povolání	povolání	k1gNnSc2
</s>
<s>
astronom	astronom	k1gMnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybět	k5eAaImIp3nS,k5eAaPmIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Oscar	Oscar	k1gMnSc1
E.	E.	kA
Monnig	Monnig	k1gMnSc1
(	(	kIx(
<g/>
4	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1902	#num#	k4
–	–	k?
4	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1999	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
americký	americký	k2eAgMnSc1d1
amatérský	amatérský	k2eAgMnSc1d1
astronom	astronom	k1gMnSc1
<g/>
,	,	kIx,
známý	známý	k2eAgMnSc1d1
především	především	k6eAd1
pro	pro	k7c4
své	svůj	k3xOyFgInPc4
příspěvky	příspěvek	k1gInPc4
ke	k	k7c3
studiu	studio	k1gNnSc3
meteoritů	meteorit	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Založil	založit	k5eAaPmAgInS
rozsáhlou	rozsáhlý	k2eAgFnSc4d1
meteoritickou	meteoritický	k2eAgFnSc4d1
sbírku	sbírka	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
později	pozdě	k6eAd2
daroval	darovat	k5eAaPmAgMnS
Texaské	texaský	k2eAgFnSc3d1
křesťanské	křesťanský	k2eAgFnSc3d1
univerzitě	univerzita	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Oscar	Oscar	k1gMnSc1
Monnig	Monnig	k1gMnSc1
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgMnS
4	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc4
1902	#num#	k4
v	v	k7c6
texaském	texaský	k2eAgNnSc6d1
městě	město	k1gNnSc6
Fort	Fort	k?
Worth	Worth	k1gInSc4
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1925	#num#	k4
ukončil	ukončit	k5eAaPmAgInS
studium	studium	k1gNnSc4
práv	právo	k1gNnPc2
na	na	k7c6
Texaské	texaský	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
v	v	k7c6
Austinu	Austin	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
získal	získat	k5eAaPmAgInS
titul	titul	k1gInSc1
LLB	LLB	kA
(	(	kIx(
<g/>
Bachelor	Bachelor	k1gMnSc1
of	of	k?
Laws	Laws	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pracoval	pracovat	k5eAaImAgMnS
v	v	k7c6
rodinném	rodinný	k2eAgInSc6d1
podniku	podnik	k1gInSc6
se	s	k7c7
střižním	střižní	k2eAgNnSc7d1
zbožím	zboží	k1gNnSc7
a	a	k8xC
od	od	k7c2
roku	rok	k1gInSc2
1974	#num#	k4
až	až	k9
do	do	k7c2
jeho	jeho	k3xOp3gInSc2,k3xPp3gInSc2
prodeje	prodej	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1981	#num#	k4
stál	stát	k5eAaImAgInS
v	v	k7c6
jeho	jeho	k3xOp3gNnSc6
čele	čelo	k1gNnSc6
jako	jako	k9
prezident	prezident	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Roku	rok	k1gInSc2
1941	#num#	k4
se	se	k3xPyFc4
oženil	oženit	k5eAaPmAgMnS
s	s	k7c7
Juanitou	Juanita	k1gFnSc7
Mickleovou	Mickleův	k2eAgFnSc7d1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
zemřela	zemřít	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1996	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neměli	mít	k5eNaImAgMnP
žádné	žádný	k3yNgFnPc4
děti	dítě	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Monnig	Monnig	k1gMnSc1
zemřel	zemřít	k5eAaPmAgMnS
ve	v	k7c6
svém	svůj	k3xOyFgNnSc6
rodném	rodný	k2eAgNnSc6d1
městě	město	k1gNnSc6
4	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1999	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Astronomie	astronomie	k1gFnSc1
</s>
<s>
Monnigův	Monnigův	k2eAgInSc1d1
zájem	zájem	k1gInSc1
o	o	k7c4
astronomii	astronomie	k1gFnSc4
se	se	k3xPyFc4
začal	začít	k5eAaPmAgInS
rozvíjet	rozvíjet	k5eAaImF
ve	v	k7c6
20	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Založil	založit	k5eAaPmAgInS
astronomický	astronomický	k2eAgInSc1d1
klub	klub	k1gInSc1
Texas	Texas	k1gInSc1
Observers	Observersa	k1gFnPc2
a	a	k8xC
v	v	k7c6
letech	léto	k1gNnPc6
1931	#num#	k4
až	až	k9
1947	#num#	k4
vydával	vydávat	k5eAaPmAgInS,k5eAaImAgInS
měsíčník	měsíčník	k1gInSc1
Texas	Texas	k1gInSc1
Observers	Observers	k1gInSc1
Bulletin	bulletin	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
se	se	k3xPyFc4
specializoval	specializovat	k5eAaBmAgMnS
na	na	k7c4
hlavní	hlavní	k2eAgInPc4d1
objekty	objekt	k1gInPc4
zájmu	zájem	k1gInSc2
amatérských	amatérský	k2eAgMnPc2d1
astronomů	astronom	k1gMnPc2
jako	jako	k8xC,k8xS
proměnné	proměnný	k2eAgFnPc4d1
hvězdy	hvězda	k1gFnPc4
<g/>
,	,	kIx,
meteory	meteor	k1gInPc4
<g/>
,	,	kIx,
komety	kometa	k1gFnPc4
a	a	k8xC
planety	planeta	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Koncem	koncem	k7c2
20	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
se	se	k3xPyFc4
Monnig	Monnig	k1gInSc1
začal	začít	k5eAaPmAgInS
zabývat	zabývat	k5eAaImF
meteority	meteorit	k1gInPc7
a	a	k8xC
možnostmi	možnost	k1gFnPc7
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
s	s	k7c7
jejich	jejich	k3xOp3gFnSc7
pomocí	pomoc	k1gFnSc7
získat	získat	k5eAaPmF
informace	informace	k1gFnPc4
o	o	k7c6
původu	původ	k1gInSc6
sluneční	sluneční	k2eAgFnSc2d1
soustavy	soustava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1933	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
jedním	jeden	k4xCgMnSc7
ze	z	k7c2
zakládajících	zakládající	k2eAgMnPc2d1
členů	člen	k1gMnPc2
Společnosti	společnost	k1gFnSc2
pro	pro	k7c4
výzkum	výzkum	k1gInSc4
meteoritů	meteorit	k1gInPc2
(	(	kIx(
<g/>
později	pozdě	k6eAd2
přejmenované	přejmenovaný	k2eAgNnSc1d1
na	na	k7c4
Meteoritickou	Meteoritický	k2eAgFnSc4d1
společnost	společnost	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Meteoritická	Meteoritický	k2eAgFnSc1d1
sbírka	sbírka	k1gFnSc1
</s>
<s>
Začátkem	začátkem	k7c2
30	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
začal	začít	k5eAaPmAgMnS
Monnig	Monnig	k1gInSc4
meteority	meteorit	k1gInPc1
též	též	k6eAd1
sbírat	sbírat	k5eAaImF
<g/>
,	,	kIx,
ovšem	ovšem	k9
své	svůj	k3xOyFgNnSc4
sběratelské	sběratelský	k2eAgNnSc4d1
úsilí	úsilí	k1gNnSc4
zvýšil	zvýšit	k5eAaPmAgInS
zejména	zejména	k9
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
v	v	k7c6
srpnu	srpen	k1gInSc6
roku	rok	k1gInSc2
1932	#num#	k4
neuspěl	uspět	k5eNaPmAgInS
se	s	k7c7
svou	svůj	k3xOyFgFnSc7
žádostí	žádost	k1gFnSc7
o	o	k7c4
přístup	přístup	k1gInSc4
k	k	k7c3
meteoritickým	meteoritický	k2eAgFnPc3d1
sbírkám	sbírka	k1gFnPc3
ve	v	k7c4
Field	Field	k1gInSc4
Museum	museum	k1gNnSc4
of	of	k?
Natural	Natural	k?
History	Histor	k1gInPc4
v	v	k7c6
Chicagu	Chicago	k1gNnSc6
a	a	k8xC
v	v	k7c6
American	Americana	k1gFnPc2
Museum	museum	k1gNnSc1
of	of	k?
Natural	Natural	k?
History	Histor	k1gInPc4
v	v	k7c6
New	New	k1gFnSc6
Yorku	York	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
snaze	snaha	k1gFnSc6
získat	získat	k5eAaPmF
další	další	k2eAgInPc4d1
exponáty	exponát	k1gInPc4
vyslýchal	vyslýchat	k5eAaImAgMnS
lidi	člověk	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
spatřili	spatřit	k5eAaPmAgMnP
bolid	bolid	k1gInSc4
nebo	nebo	k8xC
výbuch	výbuch	k1gInSc4
meteoroidu	meteoroid	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
následně	následně	k6eAd1
pořádal	pořádat	k5eAaImAgMnS
a	a	k8xC
financoval	financovat	k5eAaBmAgMnS
pátrací	pátrací	k2eAgFnPc4d1
expedice	expedice	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Platil	platit	k5eAaImAgInS
1	#num#	k4
dolar	dolar	k1gInSc1
za	za	k7c4
každou	každý	k3xTgFnSc4
libru	libra	k1gFnSc4
váhy	váha	k1gFnSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
byla	být	k5eAaImAgFnS
cena	cena	k1gFnSc1
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
žádné	žádný	k3yNgNnSc1
muzeum	muzeum	k1gNnSc1
v	v	k7c6
době	doba	k1gFnSc6
Velké	velký	k2eAgFnSc2d1
hospodářské	hospodářský	k2eAgFnSc2d1
krize	krize	k1gFnSc2
nemohlo	moct	k5eNaImAgNnS
konkurovat	konkurovat	k5eAaImF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Sbírka	sbírka	k1gFnSc1
se	se	k3xPyFc4
postupně	postupně	k6eAd1
stala	stát	k5eAaPmAgFnS
jednou	jeden	k4xCgFnSc7
z	z	k7c2
největších	veliký	k2eAgFnPc2d3
soukromých	soukromý	k2eAgFnPc2d1
sbírek	sbírka	k1gFnPc2
meteoritů	meteorit	k1gInPc2
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsahovala	obsahovat	k5eAaImAgFnS
okolo	okolo	k7c2
3000	#num#	k4
exemplářů	exemplář	k1gInPc2
pocházejících	pocházející	k2eAgFnPc2d1
asi	asi	k9
ze	z	k7c2
400	#num#	k4
různých	různý	k2eAgInPc2d1
meteoritů	meteorit	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Pravděpodobně	pravděpodobně	k6eAd1
nejvzácnějšími	vzácný	k2eAgInPc7d3
z	z	k7c2
nich	on	k3xPp3gInPc2
byly	být	k5eAaImAgInP
dva	dva	k4xCgInPc1
uhlíkaté	uhlíkatý	k2eAgInPc1d1
chondrity	chondrit	k1gInPc1
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgInPc2
jeden	jeden	k4xCgInSc1
byl	být	k5eAaImAgInS
nalezen	nalézt	k5eAaBmNgInS,k5eAaPmNgInS
roku	rok	k1gInSc2
1936	#num#	k4
u	u	k7c2
města	město	k1gNnSc2
Crescent	Crescent	k1gInSc1
v	v	k7c6
Oklahomě	Oklahomě	k1gFnSc6
a	a	k8xC
druhý	druhý	k4xOgMnSc1
roku	rok	k1gInSc2
1961	#num#	k4
u	u	k7c2
města	město	k1gNnSc2
Bells	Bellsa	k1gFnPc2
v	v	k7c6
Texasu	Texas	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Později	pozdě	k6eAd2
se	se	k3xPyFc4
Monnig	Monnig	k1gInSc1
rozhodl	rozhodnout	k5eAaPmAgInS
darovat	darovat	k5eAaPmF
svou	svůj	k3xOyFgFnSc4
sbírku	sbírka	k1gFnSc4
Texaské	texaský	k2eAgFnSc6d1
křesťanské	křesťanský	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
ve	v	k7c6
Fort	Fort	k?
Worth	Worth	k1gInSc1
<g/>
,	,	kIx,
a	a	k8xC
tento	tento	k3xDgInSc4
svůj	svůj	k3xOyFgInSc4
záměr	záměr	k1gInSc4
nakonec	nakonec	k6eAd1
realizoval	realizovat	k5eAaBmAgMnS
sérií	série	k1gFnSc7
transferů	transfer	k1gInPc2
v	v	k7c6
letech	léto	k1gNnPc6
1976	#num#	k4
až	až	k9
1986	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
sbírka	sbírka	k1gFnSc1
obsahuje	obsahovat	k5eAaImIp3nS
více	hodně	k6eAd2
než	než	k8xS
1450	#num#	k4
různých	různý	k2eAgInPc2d1
meteoritů	meteorit	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
2003	#num#	k4
<g/>
,	,	kIx,
čtyři	čtyři	k4xCgInPc4
roky	rok	k1gInPc4
po	po	k7c6
Monnigově	Monnigův	k2eAgFnSc6d1
smrti	smrt	k1gFnSc6
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
otevřena	otevřít	k5eAaPmNgFnS
Oscar	Oscar	k1gMnSc1
E.	E.	kA
Monnig	Monnig	k1gMnSc1
Meteorite	meteorit	k1gInSc5
Gallery	Galler	k1gInPc1
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
bylo	být	k5eAaImAgNnS
pro	pro	k7c4
veřejnost	veřejnost	k1gFnSc4
zpřístupněno	zpřístupnit	k5eAaPmNgNnS
asi	asi	k9
10	#num#	k4
procent	procento	k1gNnPc2
meteoritů	meteorit	k1gInPc2
této	tento	k3xDgFnSc2
sbírky	sbírka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Uznání	uznání	k1gNnSc1
</s>
<s>
Roku	rok	k1gInSc2
1984	#num#	k4
se	se	k3xPyFc4
Monnig	Monnig	k1gMnSc1
stal	stát	k5eAaPmAgMnS
prvním	první	k4xOgMnSc7
laureátem	laureát	k1gMnSc7
<g/>
,	,	kIx,
kterému	který	k3yRgNnSc3,k3yIgNnSc3,k3yQgNnSc3
Jihozápadní	jihozápadní	k2eAgInSc4d1
region	region	k1gInSc4
americké	americký	k2eAgFnSc2d1
Astronomické	astronomický	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
udělil	udělit	k5eAaPmAgMnS
ocenění	ocenění	k1gNnSc4
Texas	Texas	k1gInSc1
Lone	Lone	k1gNnSc6
Stargazer	Stargazer	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Award	Award	k1gInSc1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
za	za	k7c4
jeho	jeho	k3xOp3gFnPc4
služby	služba	k1gFnPc4
amatérským	amatérský	k2eAgMnPc3d1
astronomům	astronom	k1gMnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Roku	rok	k1gInSc2
1990	#num#	k4
obdržel	obdržet	k5eAaPmAgMnS
od	od	k7c2
Pacifické	pacifický	k2eAgFnSc2d1
astronomické	astronomický	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
ocenění	ocenění	k1gNnSc2
Amateur	Amateur	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Achievement	Achievement	k1gInSc1
Award	Award	k1gInSc4
za	za	k7c4
příspěvky	příspěvek	k1gInPc4
ke	k	k7c3
studiu	studio	k1gNnSc3
meteoritů	meteorit	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c4
jeho	jeho	k3xOp3gFnSc4
počest	počest	k1gFnSc4
byla	být	k5eAaImAgFnS
též	též	k9
pojmenována	pojmenován	k2eAgFnSc1d1
planetka	planetka	k1gFnSc1
hlavního	hlavní	k2eAgInSc2d1
pásu	pás	k1gInSc2
<g/>
,	,	kIx,
objevená	objevený	k2eAgFnSc1d1
roku	rok	k1gInSc2
1981	#num#	k4
americkým	americký	k2eAgMnSc7d1
astronomem	astronom	k1gMnSc7
Scheltem	Schelt	k1gInSc7
Busem	bus	k1gInSc7
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
nyní	nyní	k6eAd1
nese	nést	k5eAaImIp3nS
název	název	k1gInSc1
(	(	kIx(
<g/>
2780	#num#	k4
<g/>
)	)	kIx)
Monnig	Monnig	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
WILLIAMS	WILLIAMS	kA
<g/>
,	,	kIx,
Thomas	Thomas	k1gMnSc1
R.	R.	kA
Obituary	Obituar	k1gInPc1
<g/>
:	:	kIx,
Oscar	Oscar	k1gMnSc1
E.	E.	kA
Monnig	Monnig	k1gMnSc1
<g/>
,	,	kIx,
1902	#num#	k4
<g/>
–	–	k?
<g/>
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bulletin	bulletin	k1gInSc1
of	of	k?
the	the	k?
American	American	k1gInSc4
Astronomical	Astronomical	k1gMnSc4
Society	societa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
32	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
4	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1682	#num#	k4
<g/>
–	–	k?
<g/>
1683	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
7537	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Welcome	Welcom	k1gInSc5
to	ten	k3xDgNnSc1
the	the	k?
Oscar	Oscar	k1gInSc1
E.	E.	kA
Monnig	Monnig	k1gInSc1
Meteorite	meteorit	k1gInSc5
Gallery	Galler	k1gMnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Texas	Texas	k1gInSc4
Christian	Christian	k1gMnSc1
University	universita	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
EHLMANN	EHLMANN	kA
<g/>
,	,	kIx,
Arthur	Arthur	k1gMnSc1
J.	J.	kA
<g/>
;	;	kIx,
MCCOY	MCCOY	kA
<g/>
,	,	kIx,
Timothy	Timoth	k1gInPc7
J.	J.	kA
A	a	k8xC
biography	biographa	k1gFnSc2
of	of	k?
Oscar	Oscar	k1gMnSc1
Monnig	Monnig	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Texas	Texas	k1gInSc4
Christian	Christian	k1gMnSc1
University	universita	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Past	past	k1gFnSc1
Amateur	Amateur	k1gMnSc1
Achievement	Achievement	k1gMnSc1
Winners	Winners	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pacifická	pacifický	k2eAgFnSc1d1
astronomická	astronomický	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
JPL	JPL	kA
Small-Body	Small-Boda	k1gFnSc2
Database	Databas	k1gInSc6
Browser	Browser	k1gInSc4
–	–	k?
2780	#num#	k4
Monnig	Monnig	k1gInSc1
(	(	kIx(
<g/>
1981	#num#	k4
DO	do	k7c2
<g/>
2	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jet	jet	k2eAgInSc1d1
Propulsion	Propulsion	k1gInSc1
Laboratory	Laborator	k1gInPc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Astronomie	astronomie	k1gFnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
|	|	kIx~
Planetární	planetární	k2eAgFnSc2d1
vědy	věda	k1gFnSc2
</s>
