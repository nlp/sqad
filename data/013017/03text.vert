<p>
<s>
Radochlín	Radochlín	k1gInSc1	Radochlín
je	být	k5eAaImIp3nS	být
malá	malý	k2eAgFnSc1d1	malá
vesnice	vesnice	k1gFnSc1	vesnice
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
obce	obec	k1gFnSc2	obec
Lukavice	Lukavice	k1gFnSc2	Lukavice
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Chrudim	Chrudim	k1gFnSc1	Chrudim
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
asi	asi	k9	asi
2,5	[number]	k4	2,5
km	km	kA	km
na	na	k7c4	na
jih	jih	k1gInSc4	jih
od	od	k7c2	od
Lukavic	Lukavice	k1gFnPc2	Lukavice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
evidováno	evidovat	k5eAaImNgNnS	evidovat
15	[number]	k4	15
adres	adresa	k1gFnPc2	adresa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
zde	zde	k6eAd1	zde
trvale	trvale	k6eAd1	trvale
žilo	žít	k5eAaImAgNnS	žít
12	[number]	k4	12
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
<g/>
Radochlín	Radochlín	k1gInSc1	Radochlín
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
katastrálním	katastrální	k2eAgNnSc6d1	katastrální
území	území	k1gNnSc6	území
Vížky	vížka	k1gFnSc2	vížka
o	o	k7c6	o
výměře	výměra	k1gFnSc6	výměra
2,48	[number]	k4	2,48
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Katastrální	katastrální	k2eAgFnSc1d1	katastrální
mapa	mapa	k1gFnSc1	mapa
katastru	katastr	k1gInSc2	katastr
Vížky	vížka	k1gFnSc2	vížka
na	na	k7c6	na
webu	web	k1gInSc6	web
ČÚZK	ČÚZK	kA	ČÚZK
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Radochlín	Radochlína	k1gFnPc2	Radochlína
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
